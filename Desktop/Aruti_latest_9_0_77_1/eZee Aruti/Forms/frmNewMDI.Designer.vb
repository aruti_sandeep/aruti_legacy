﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmNewMDI
    Inherits eZee.Common.eZeeForm

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub


    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
Me.components = New System.ComponentModel.Container
Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmNewMDI))
Me.tblMDILayout = New System.Windows.Forms.TableLayoutPanel
Me.fpnlButtons = New System.Windows.Forms.FlowLayoutPanel
Me.fpnlAssessmentSetups = New System.Windows.Forms.FlowLayoutPanel
Me.btnAssessmentPeriod = New eZee.Common.eZeeLightButton(Me.components)
Me.btnAssessorAccess = New eZee.Common.eZeeLightButton(Me.components)
Me.btnReviewer = New eZee.Common.eZeeLightButton(Me.components)
Me.btnExternalAssessor = New eZee.Common.eZeeLightButton(Me.components)
Me.btnMigration = New eZee.Common.eZeeLightButton(Me.components)
Me.btnAssessmentRatio = New eZee.Common.eZeeLightButton(Me.components)
Me.btnCloseAssessmentPeriod = New eZee.Common.eZeeLightButton(Me.components)
Me.btnAssessGroup = New eZee.Common.eZeeLightButton(Me.components)
Me.btnAssessScale = New eZee.Common.eZeeLightButton(Me.components)
Me.btnCompetencies = New eZee.Common.eZeeLightButton(Me.components)
Me.btnAssignCompetencies = New eZee.Common.eZeeLightButton(Me.components)
Me.btnPerspective = New eZee.Common.eZeeLightButton(Me.components)
Me.btnAssessCaptionSettings = New eZee.Common.eZeeLightButton(Me.components)
Me.btnAssessmentMapping = New eZee.Common.eZeeLightButton(Me.components)
Me.btnBSCViewSetting = New eZee.Common.eZeeLightButton(Me.components)
Me.btnAssessCompanyGoals = New eZee.Common.eZeeLightButton(Me.components)
Me.btnAllocationGoals = New eZee.Common.eZeeLightButton(Me.components)
Me.btnEmployeeGoals = New eZee.Common.eZeeLightButton(Me.components)
Me.btnPerformancePlan = New eZee.Common.eZeeLightButton(Me.components)
Me.btnOpenPeriodicReview = New eZee.Common.eZeeLightButton(Me.components)
Me.btnCustomHeaders = New eZee.Common.eZeeLightButton(Me.components)
Me.btnCustomItems = New eZee.Common.eZeeLightButton(Me.components)
Me.btnComputationFormula = New eZee.Common.eZeeLightButton(Me.components)
Me.btnAssessor = New eZee.Common.eZeeLightButton(Me.components)
Me.fpnlLeaveInfo = New System.Windows.Forms.FlowLayoutPanel
Me.btnLeaveType = New eZee.Common.eZeeLightButton(Me.components)
Me.btnHolidays = New eZee.Common.eZeeLightButton(Me.components)
Me.btnEmployeeHolidays = New eZee.Common.eZeeLightButton(Me.components)
Me.btnApproverLevel = New eZee.Common.eZeeLightButton(Me.components)
Me.btnLeaveApprover = New eZee.Common.eZeeLightButton(Me.components)
Me.btnLeaveForm = New eZee.Common.eZeeLightButton(Me.components)
Me.btnLeaveProcess = New eZee.Common.eZeeLightButton(Me.components)
Me.btnLeaveAccrue = New eZee.Common.eZeeLightButton(Me.components)
Me.btnLeaveViewer = New eZee.Common.eZeeLightButton(Me.components)
Me.btnLeavePlanner = New eZee.Common.eZeeLightButton(Me.components)
Me.btnLeaveSchedular = New eZee.Common.eZeeLightButton(Me.components)
Me.btnImportAccrueLeave = New eZee.Common.eZeeLightButton(Me.components)
Me.BtnTransferApprover = New eZee.Common.eZeeLightButton(Me.components)
Me.btnEndELC = New eZee.Common.eZeeLightButton(Me.components)
Me.btnLeaveFrequency = New eZee.Common.eZeeLightButton(Me.components)
Me.btnLeaveAdjustment = New eZee.Common.eZeeLightButton(Me.components)
Me.btnLVSwapApprover = New eZee.Common.eZeeLightButton(Me.components)
Me.fpnlCommomMasters = New System.Windows.Forms.FlowLayoutPanel
Me.btnBranch = New eZee.Common.eZeeLightButton(Me.components)
Me.btnDepartmentGroup = New eZee.Common.eZeeLightButton(Me.components)
Me.btnDepartment = New eZee.Common.eZeeLightButton(Me.components)
Me.btnSectionGroup = New eZee.Common.eZeeLightButton(Me.components)
Me.btnSection = New eZee.Common.eZeeLightButton(Me.components)
Me.btnUnitGroup = New eZee.Common.eZeeLightButton(Me.components)
Me.btnUnit = New eZee.Common.eZeeLightButton(Me.components)
Me.btnTeam = New eZee.Common.eZeeLightButton(Me.components)
Me.btnJobGroup = New eZee.Common.eZeeLightButton(Me.components)
Me.btnJob = New eZee.Common.eZeeLightButton(Me.components)
Me.btnClassGroup = New eZee.Common.eZeeLightButton(Me.components)
Me.btnClasses = New eZee.Common.eZeeLightButton(Me.components)
Me.btnAllocationMapping = New eZee.Common.eZeeLightButton(Me.components)
Me.fpnlTnAInfo = New System.Windows.Forms.FlowLayoutPanel
Me.btnDeviceUserMapping = New eZee.Common.eZeeLightButton(Me.components)
Me.btnLogin = New eZee.Common.eZeeLightButton(Me.components)
Me.btnTimesheet = New eZee.Common.eZeeLightButton(Me.components)
Me.btnHoldEmployee = New eZee.Common.eZeeLightButton(Me.components)
Me.btnEmployeeAbsent = New eZee.Common.eZeeLightButton(Me.components)
Me.btnImportData = New eZee.Common.eZeeLightButton(Me.components)
Me.btnExportData = New eZee.Common.eZeeLightButton(Me.components)
Me.btnImportAttendancedata = New eZee.Common.eZeeLightButton(Me.components)
Me.btnGroupAttendance = New eZee.Common.eZeeLightButton(Me.components)
Me.btnEditTimeSheet = New eZee.Common.eZeeLightButton(Me.components)
Me.btnRecalculateTiming = New eZee.Common.eZeeLightButton(Me.components)
Me.btnRoundOff = New eZee.Common.eZeeLightButton(Me.components)
Me.btnGlobalEditDelete = New eZee.Common.eZeeLightButton(Me.components)
Me.fpnlPayrollMasters = New System.Windows.Forms.FlowLayoutPanel
Me.btnPayrollGroup = New eZee.Common.eZeeLightButton(Me.components)
Me.btnPayPoint = New eZee.Common.eZeeLightButton(Me.components)
Me.objelLine1 = New eZee.Common.eZeeLine
Me.btnBankBranch = New eZee.Common.eZeeLightButton(Me.components)
Me.btnBankAccountType = New eZee.Common.eZeeLightButton(Me.components)
Me.btnBankEDI = New eZee.Common.eZeeLightButton(Me.components)
Me.objelLine2 = New eZee.Common.eZeeLine
Me.btnCurrency = New eZee.Common.eZeeLightButton(Me.components)
Me.btnDemomination = New eZee.Common.eZeeLightButton(Me.components)
Me.btnPayslipMessage = New eZee.Common.eZeeLightButton(Me.components)
Me.btnGlobalPayslipMessage = New eZee.Common.eZeeLightButton(Me.components)
Me.objelLine8 = New eZee.Common.eZeeLine
Me.btnAccount = New eZee.Common.eZeeLightButton(Me.components)
Me.btnAccountConfiguration = New eZee.Common.eZeeLightButton(Me.components)
Me.btnEmpAccConfig = New eZee.Common.eZeeLightButton(Me.components)
Me.btnCCAccConfig = New eZee.Common.eZeeLightButton(Me.components)
Me.objLine11 = New eZee.Common.eZeeLine
Me.btnPayAppLevel = New eZee.Common.eZeeLightButton(Me.components)
Me.btnPayApproverMap = New eZee.Common.eZeeLightButton(Me.components)
Me.fpnlAssessment = New System.Windows.Forms.FlowLayoutPanel
Me.btnAssessItems = New eZee.Common.eZeeLightButton(Me.components)
Me.btnAssessSubItem = New eZee.Common.eZeeLightButton(Me.components)
Me.btnEmployeeAssessment = New eZee.Common.eZeeLightButton(Me.components)
Me.btnAssessorAssessmentlist = New eZee.Common.eZeeLightButton(Me.components)
Me.btnReviewerAssessment = New eZee.Common.eZeeLightButton(Me.components)
Me.fpnlDiscipline = New System.Windows.Forms.FlowLayoutPanel
Me.btnDisciplineType = New eZee.Common.eZeeLightButton(Me.components)
Me.btnDisciplinaryAction = New eZee.Common.eZeeLightButton(Me.components)
Me.btnDisciplineStatus = New eZee.Common.eZeeLightButton(Me.components)
Me.btnDisciplineFiling = New eZee.Common.eZeeLightButton(Me.components)
Me.btnResolutionSteps = New eZee.Common.eZeeLightButton(Me.components)
Me.btnCommittee = New eZee.Common.eZeeLightButton(Me.components)
Me.btnHearingList = New eZee.Common.eZeeLightButton(Me.components)
Me.fpnlHRMaster = New System.Windows.Forms.FlowLayoutPanel
Me.btnReasons = New eZee.Common.eZeeLightButton(Me.components)
Me.btnSkillMaster = New eZee.Common.eZeeLightButton(Me.components)
Me.btnSkillExpertise = New eZee.Common.eZeeLightButton(Me.components)
Me.btnAdvertiseMaster = New eZee.Common.eZeeLightButton(Me.components)
Me.btnQualification = New eZee.Common.eZeeLightButton(Me.components)
Me.btnResultCode = New eZee.Common.eZeeLightButton(Me.components)
Me.btnBenefitPlan = New eZee.Common.eZeeLightButton(Me.components)
Me.fpnlLoanInfo = New System.Windows.Forms.FlowLayoutPanel
Me.btnLoanScheme = New eZee.Common.eZeeLightButton(Me.components)
Me.btnLoanApproverLevel = New eZee.Common.eZeeLightButton(Me.components)
Me.btnLoanApprover = New eZee.Common.eZeeLightButton(Me.components)
Me.btnLoanApplication = New eZee.Common.eZeeLightButton(Me.components)
        Me.btnFlexcubeLoanApplication = New eZee.Common.eZeeLightButton(Me.components)
Me.btnProcessPendingLoan = New eZee.Common.eZeeLightButton(Me.components)
Me.btnLoanAdvance = New eZee.Common.eZeeLightButton(Me.components)
Me.btnImportLA = New eZee.Common.eZeeLightButton(Me.components)
Me.btnLoanApproverMigration = New eZee.Common.eZeeLightButton(Me.components)
Me.btnLoanSwapApprover = New eZee.Common.eZeeLightButton(Me.components)
        Me.btnLoanSchemeCategoryMapping = New eZee.Common.eZeeLightButton(Me.components)
Me.objelLine5 = New eZee.Common.eZeeLine
Me.btnSavingScheme = New eZee.Common.eZeeLightButton(Me.components)
Me.btnEmployeeSavings = New eZee.Common.eZeeLightButton(Me.components)
Me.btnImportSaving = New eZee.Common.eZeeLightButton(Me.components)
Me.fpnlRecruitment = New System.Windows.Forms.FlowLayoutPanel
Me.btnStaffReqAppLevel = New eZee.Common.eZeeLightButton(Me.components)
Me.btnStaffReqAppMapping = New eZee.Common.eZeeLightButton(Me.components)
Me.btnTransferStaffRequisitionApprover = New eZee.Common.eZeeLightButton(Me.components)
Me.btnStaffRequisition = New eZee.Common.eZeeLightButton(Me.components)
Me.btnVacancy = New eZee.Common.eZeeLightButton(Me.components)
Me.btnApplicantMaster = New eZee.Common.eZeeLightButton(Me.components)
Me.btnDownloadAptitudeResult = New eZee.Common.eZeeLightButton(Me.components)
Me.btnApplicantFilter = New eZee.Common.eZeeLightButton(Me.components)
Me.btnFinalShortListedApplicant = New eZee.Common.eZeeLightButton(Me.components)
Me.btnBatchScheduling = New eZee.Common.eZeeLightButton(Me.components)
Me.btnInterviewScheduling = New eZee.Common.eZeeLightButton(Me.components)
Me.btnInterviewAnalysis = New eZee.Common.eZeeLightButton(Me.components)
Me.btnGlobalInterviewAnalysis = New eZee.Common.eZeeLightButton(Me.components)
Me.btnFinalApplicants = New eZee.Common.eZeeLightButton(Me.components)
Me.btnExporttoWeb = New eZee.Common.eZeeLightButton(Me.components)
Me.btnImportfromWeb = New eZee.Common.eZeeLightButton(Me.components)
Me.btnAutoImportFromWeb = New eZee.Common.eZeeLightButton(Me.components)
Me.btnCandidateFeedback = New eZee.Common.eZeeLightButton(Me.components)
Me.fpnlEmpData = New System.Windows.Forms.FlowLayoutPanel
Me.btnEmployeeData2 = New eZee.Common.eZeeLightButton(Me.components)
Me.btnDependants2 = New eZee.Common.eZeeLightButton(Me.components)
Me.btnBenefitAllocation = New eZee.Common.eZeeLightButton(Me.components)
Me.btnEmployeeBenefit = New eZee.Common.eZeeLightButton(Me.components)
Me.btnGroupBenefit = New eZee.Common.eZeeLightButton(Me.components)
Me.btnReferee = New eZee.Common.eZeeLightButton(Me.components)
Me.btnEmployeeAssets = New eZee.Common.eZeeLightButton(Me.components)
Me.btnEmployeeSkill = New eZee.Common.eZeeLightButton(Me.components)
Me.btnEmployeeQualification = New eZee.Common.eZeeLightButton(Me.components)
Me.btnEmployeeExperence = New eZee.Common.eZeeLightButton(Me.components)
Me.btnEmployeeMovement2 = New eZee.Common.eZeeLightButton(Me.components)
Me.btnEmployeeDiary = New eZee.Common.eZeeLightButton(Me.components)
Me.btnEmployeeDataApproval = New eZee.Common.eZeeLightButton(Me.components)
Me.fpnlTrainingInfo = New System.Windows.Forms.FlowLayoutPanel
Me.btnTrainingPriority = New eZee.Common.eZeeLightButton(Me.components)
Me.btnTrainingScheduling = New eZee.Common.eZeeLightButton(Me.components)
Me.btnTrainingEnrollment = New eZee.Common.eZeeLightButton(Me.components)
        Me.btnTrainingApproverEmployeeMapping = New eZee.Common.eZeeLightButton(Me.components)
Me.btnTrainingAttendance = New eZee.Common.eZeeLightButton(Me.components)
Me.btnTrainingAnalysis = New eZee.Common.eZeeLightButton(Me.components)
Me.fpnlUtility = New System.Windows.Forms.FlowLayoutPanel
Me.btnGeneralSettings = New eZee.Common.eZeeLightButton(Me.components)
Me.btnReminder = New eZee.Common.eZeeLightButton(Me.components)
Me.btnBackup = New eZee.Common.eZeeLightButton(Me.components)
Me.btnRestore = New eZee.Common.eZeeLightButton(Me.components)
Me.objelLine6 = New eZee.Common.eZeeLine
Me.btnLetterType = New eZee.Common.eZeeLightButton(Me.components)
Me.btnLetterTemplate = New eZee.Common.eZeeLightButton(Me.components)
Me.btnMailbox = New eZee.Common.eZeeLightButton(Me.components)
Me.objelLine7 = New eZee.Common.eZeeLine
Me.btnChangeUser = New eZee.Common.eZeeLightButton(Me.components)
Me.btnChangeCompany = New eZee.Common.eZeeLightButton(Me.components)
Me.btnChangeDatabase = New eZee.Common.eZeeLightButton(Me.components)
Me.objelLine9 = New eZee.Common.eZeeLine
Me.btnCommonExportData = New eZee.Common.eZeeLightButton(Me.components)
Me.btnCommonImportData = New eZee.Common.eZeeLightButton(Me.components)
Me.btnUserLog = New eZee.Common.eZeeLightButton(Me.components)
Me.btnChangePwd = New eZee.Common.eZeeLightButton(Me.components)
Me.btnScanAttachments = New eZee.Common.eZeeLightButton(Me.components)
Me.objelLine11 = New eZee.Common.eZeeLine
Me.btnMapOrbitParams = New eZee.Common.eZeeLightButton(Me.components)
Me.btnReportLanguage = New eZee.Common.eZeeLightButton(Me.components)
Me.fpnlPayrollTransaction = New System.Windows.Forms.FlowLayoutPanel
Me.btnEmployeeCostCenter = New eZee.Common.eZeeLightButton(Me.components)
Me.btnEmployeeBanks = New eZee.Common.eZeeLightButton(Me.components)
Me.btnTransactionHeads = New eZee.Common.eZeeLightButton(Me.components)
Me.btnBatchPosting = New eZee.Common.eZeeLightButton(Me.components)
Me.btnBatchTransaction = New eZee.Common.eZeeLightButton(Me.components)
Me.btnEarningDeduction = New eZee.Common.eZeeLightButton(Me.components)
Me.btnGlobalAssignED = New eZee.Common.eZeeLightButton(Me.components)
Me.btnBatchEntry = New eZee.Common.eZeeLightButton(Me.components)
Me.btnEmployeeExemption = New eZee.Common.eZeeLightButton(Me.components)
Me.btnProcessPayroll = New eZee.Common.eZeeLightButton(Me.components)
Me.btnPayslip = New eZee.Common.eZeeLightButton(Me.components)
Me.btnBudgetPreparation = New eZee.Common.eZeeLightButton(Me.components)
Me.btnCashDenomination = New eZee.Common.eZeeLightButton(Me.components)
Me.objelLine3 = New eZee.Common.eZeeLine
Me.btnCompanyJV = New eZee.Common.eZeeLightButton(Me.components)
Me.objelLine4 = New eZee.Common.eZeeLine
Me.fpnlCoreSetups = New System.Windows.Forms.FlowLayoutPanel
Me.btnCommonMaster = New eZee.Common.eZeeLightButton(Me.components)
Me.btnMemebership = New eZee.Common.eZeeLightButton(Me.components)
Me.btnCountry = New eZee.Common.eZeeLightButton(Me.components)
Me.btnState = New eZee.Common.eZeeLightButton(Me.components)
Me.btnCity = New eZee.Common.eZeeLightButton(Me.components)
Me.btnZipcode = New eZee.Common.eZeeLightButton(Me.components)
Me.btnEmpApproverLevel = New eZee.Common.eZeeLightButton(Me.components)
Me.btnEmployeeApprover = New eZee.Common.eZeeLightButton(Me.components)
Me.btnApproveEmployee = New eZee.Common.eZeeLightButton(Me.components)
Me.btnEmployeeData = New eZee.Common.eZeeLightButton(Me.components)
Me.btnDependants = New eZee.Common.eZeeLightButton(Me.components)
        Me.btnGarnishees = New eZee.Common.eZeeLightButton(Me.components)
Me.btnEmployeeMovement = New eZee.Common.eZeeLightButton(Me.components)
Me.btnShift = New eZee.Common.eZeeLightButton(Me.components)
Me.btnTrainingInstitute = New eZee.Common.eZeeLightButton(Me.components)
Me.btnPolicyList = New eZee.Common.eZeeLightButton(Me.components)
Me.fpnlBSC = New System.Windows.Forms.FlowLayoutPanel
Me.btnWeightedSetting = New eZee.Common.eZeeLightButton(Me.components)
Me.btnObjectiveList = New eZee.Common.eZeeLightButton(Me.components)
Me.btnKPIMeasureList = New eZee.Common.eZeeLightButton(Me.components)
Me.btnTargetsList = New eZee.Common.eZeeLightButton(Me.components)
Me.btnInitiativeList = New eZee.Common.eZeeLightButton(Me.components)
Me.btnSelfAssessedBSC = New eZee.Common.eZeeLightButton(Me.components)
Me.btnAssessorBSCList = New eZee.Common.eZeeLightButton(Me.components)
Me.btnReviewerBSCList = New eZee.Common.eZeeLightButton(Me.components)
Me.fpnlTrainingNeed = New System.Windows.Forms.FlowLayoutPanel
Me.btnFinalizingTrainingPriority = New eZee.Common.eZeeLightButton(Me.components)
Me.fpnlTrainingFeedback = New System.Windows.Forms.FlowLayoutPanel
Me.btnFeedbackGroup = New eZee.Common.eZeeLightButton(Me.components)
Me.btnFeedbackItems = New eZee.Common.eZeeLightButton(Me.components)
Me.btnFeedbackSubItems = New eZee.Common.eZeeLightButton(Me.components)
Me.btnImpactItems = New eZee.Common.eZeeLightButton(Me.components)
Me.objelLine10 = New eZee.Common.eZeeLine
Me.btnFeedback = New eZee.Common.eZeeLightButton(Me.components)
Me.btnEvaluationLevel3 = New eZee.Common.eZeeLightButton(Me.components)
Me.fpnlAuditTrails = New System.Windows.Forms.FlowLayoutPanel
Me.btnApplicationEventLog = New eZee.Common.eZeeLightButton(Me.components)
Me.btnUserAuthenticationLog = New eZee.Common.eZeeLightButton(Me.components)
Me.btnUserAttempts = New eZee.Common.eZeeLightButton(Me.components)
Me.btnLogView = New eZee.Common.eZeeLightButton(Me.components)
Me.btnEmailLogView = New eZee.Common.eZeeLightButton(Me.components)
Me.btnRemovePPALogs = New eZee.Common.eZeeLightButton(Me.components)
Me.btnSystemErrorLog = New eZee.Common.eZeeLightButton(Me.components)
Me.fnlWagesSetups = New System.Windows.Forms.FlowLayoutPanel
Me.btnGradeGroup = New eZee.Common.eZeeLightButton(Me.components)
Me.btnGrade = New eZee.Common.eZeeLightButton(Me.components)
Me.btnGradeLevel = New eZee.Common.eZeeLightButton(Me.components)
Me.btnCostcenter = New eZee.Common.eZeeLightButton(Me.components)
Me.btnWagesTable = New eZee.Common.eZeeLightButton(Me.components)
Me.btnPayrollPeriods = New eZee.Common.eZeeLightButton(Me.components)
Me.btnSalaryIncrement = New eZee.Common.eZeeLightButton(Me.components)
Me.btnClosePayroll = New eZee.Common.eZeeLightButton(Me.components)
Me.fpnlMedicalInfo = New System.Windows.Forms.FlowLayoutPanel
Me.btnMedicalMasters = New eZee.Common.eZeeLightButton(Me.components)
Me.btnMedicalInstitute = New eZee.Common.eZeeLightButton(Me.components)
Me.btnCategoryAssignment = New eZee.Common.eZeeLightButton(Me.components)
Me.btnCoverAssignment = New eZee.Common.eZeeLightButton(Me.components)
Me.btnDependantException = New eZee.Common.eZeeLightButton(Me.components)
Me.btnSickSheet = New eZee.Common.eZeeLightButton(Me.components)
Me.btnMedicalClaim = New eZee.Common.eZeeLightButton(Me.components)
Me.btnEmployeeInjuries = New eZee.Common.eZeeLightButton(Me.components)
Me.fpnlPayActivity = New System.Windows.Forms.FlowLayoutPanel
Me.btnMeasureList = New eZee.Common.eZeeLightButton(Me.components)
Me.btnActivity_List = New eZee.Common.eZeeLightButton(Me.components)
Me.btnActivityRate = New eZee.Common.eZeeLightButton(Me.components)
Me.btnPayPerActivity = New eZee.Common.eZeeLightButton(Me.components)
Me.btnPayrollPosting = New eZee.Common.eZeeLightButton(Me.components)
Me.btnActivityGlobalAssign = New eZee.Common.eZeeLightButton(Me.components)
Me.fpnlAppraisal = New System.Windows.Forms.FlowLayoutPanel
Me.btnAppraisalSetups = New eZee.Common.eZeeLightButton(Me.components)
Me.btnShortListEmployee = New eZee.Common.eZeeLightButton(Me.components)
Me.btnFinalShortListedEmployee = New eZee.Common.eZeeLightButton(Me.components)
Me.fpnlClaimsExpenses = New System.Windows.Forms.FlowLayoutPanel
Me.btnExpensesList = New eZee.Common.eZeeLightButton(Me.components)
Me.btnCostingList = New eZee.Common.eZeeLightButton(Me.components)
Me.btnSecRouteAssignment = New eZee.Common.eZeeLightButton(Me.components)
Me.btnExpenseAssignment = New eZee.Common.eZeeLightButton(Me.components)
Me.btnExApprovalLevel = New eZee.Common.eZeeLightButton(Me.components)
Me.btnExApprovers = New eZee.Common.eZeeLightButton(Me.components)
Me.btnExApprMigration = New eZee.Common.eZeeLightButton(Me.components)
Me.btnClaimSwapApprover = New eZee.Common.eZeeLightButton(Me.components)
Me.btnClaims_Request = New eZee.Common.eZeeLightButton(Me.components)
Me.btnExpenseApproval = New eZee.Common.eZeeLightButton(Me.components)
Me.btnPostToPayroll = New eZee.Common.eZeeLightButton(Me.components)
Me.fpnlPerformanceEval = New System.Windows.Forms.FlowLayoutPanel
Me.btnSelfPEvaluation = New eZee.Common.eZeeLightButton(Me.components)
Me.btnAssessorPEvaluation = New eZee.Common.eZeeLightButton(Me.components)
Me.btnReviewerPEvaluation = New eZee.Common.eZeeLightButton(Me.components)
Me.btnGlobalVoidAssessment = New eZee.Common.eZeeLightButton(Me.components)
Me.btnComputeProcess = New eZee.Common.eZeeLightButton(Me.components)
Me.btnExportPrintEmplForm = New eZee.Common.eZeeLightButton(Me.components)
Me.fpnlBudgetInfo = New System.Windows.Forms.FlowLayoutPanel
Me.btnFundSources = New eZee.Common.eZeeLightButton(Me.components)
Me.btnFundProjectCode = New eZee.Common.eZeeLightButton(Me.components)
Me.btnProjectCodeAdjustments = New eZee.Common.eZeeLightButton(Me.components)
Me.btnFundActivity = New eZee.Common.eZeeLightButton(Me.components)
Me.btnFundActivityAdjustment = New eZee.Common.eZeeLightButton(Me.components)
Me.btnBgtApproverLevel = New eZee.Common.eZeeLightButton(Me.components)
Me.btnBgtApproverMapping = New eZee.Common.eZeeLightButton(Me.components)
Me.btnBudgetFormula = New eZee.Common.eZeeLightButton(Me.components)
Me.btnBudget = New eZee.Common.eZeeLightButton(Me.components)
Me.btnBudgetCodes = New eZee.Common.eZeeLightButton(Me.components)
Me.fpnlTimesheetInfo = New System.Windows.Forms.FlowLayoutPanel
Me.btnTimesheetApproverLevel = New eZee.Common.eZeeLightButton(Me.components)
Me.btnTimesheetApproverMst = New eZee.Common.eZeeLightButton(Me.components)
Me.btnEmployeeTimesheet = New eZee.Common.eZeeLightButton(Me.components)
Me.btnTimesheetApproval = New eZee.Common.eZeeLightButton(Me.components)
Me.btnTimesheetApproverMigration = New eZee.Common.eZeeLightButton(Me.components)
Me.pnlSummary = New System.Windows.Forms.Panel
Me.objspc1 = New System.Windows.Forms.SplitContainer
Me.objlblCaption = New System.Windows.Forms.Label
Me.pnlMenuItems = New System.Windows.Forms.Panel
Me.pnlMainInfo = New System.Windows.Forms.Panel
Me.mnuArutiPayroll = New System.Windows.Forms.MenuStrip
Me.mnuGeneralMaster = New System.Windows.Forms.ToolStripMenuItem
Me.mnuCoreSetups = New System.Windows.Forms.ToolStripMenuItem
Me.mnuCommonSetups = New System.Windows.Forms.ToolStripMenuItem
Me.mnuWagesSetup = New System.Windows.Forms.ToolStripMenuItem
Me.mnuRecruitment = New System.Windows.Forms.ToolStripMenuItem
Me.mnuHumanResource = New System.Windows.Forms.ToolStripMenuItem
Me.mnuPersonnel = New System.Windows.Forms.ToolStripMenuItem
Me.mnuHRMaster = New System.Windows.Forms.ToolStripMenuItem
Me.mnuEmployeeData = New System.Windows.Forms.ToolStripMenuItem
Me.mnuMedical = New System.Windows.Forms.ToolStripMenuItem
Me.mnuRecruitmentItem = New System.Windows.Forms.ToolStripMenuItem
Me.mnuDiscipline = New System.Windows.Forms.ToolStripMenuItem
Me.mnuAssessment = New System.Windows.Forms.ToolStripMenuItem
Me.mnuSetups = New System.Windows.Forms.ToolStripMenuItem
Me.mnuPerformaceEvaluation = New System.Windows.Forms.ToolStripMenuItem
Me.mnuGeneralAssessment = New System.Windows.Forms.ToolStripMenuItem
Me.mnuBalancedScoreCard = New System.Windows.Forms.ToolStripMenuItem
Me.mnuAppraisal = New System.Windows.Forms.ToolStripMenuItem
Me.mnuTrainingInformation = New System.Windows.Forms.ToolStripMenuItem
Me.mnuJobTraining = New System.Windows.Forms.ToolStripMenuItem
Me.mnuTrainingEvaluation = New System.Windows.Forms.ToolStripMenuItem
Me.mnuAssetDeclaration = New System.Windows.Forms.ToolStripMenuItem
Me.mnuPayroll = New System.Windows.Forms.ToolStripMenuItem
Me.mnuPayrollMasters = New System.Windows.Forms.ToolStripMenuItem
Me.mnuPayrollTransactions = New System.Windows.Forms.ToolStripMenuItem
Me.mnuLoan_Advance_Savings = New System.Windows.Forms.ToolStripMenuItem
Me.mnuPayActivity = New System.Windows.Forms.ToolStripMenuItem
Me.mnuBudget = New System.Windows.Forms.ToolStripMenuItem
Me.mnuLeaveAndTnA = New System.Windows.Forms.ToolStripMenuItem
Me.mnuLeaveInformation = New System.Windows.Forms.ToolStripMenuItem
Me.mnuTnAInfo = New System.Windows.Forms.ToolStripMenuItem
Me.mnuEmpBudgetTimesheet = New System.Windows.Forms.ToolStripMenuItem
Me.mnuUtilitiesMain = New System.Windows.Forms.ToolStripMenuItem
Me.mnuUtilities = New System.Windows.Forms.ToolStripMenuItem
Me.mnuAuditTrails = New System.Windows.Forms.ToolStripMenuItem
Me.mnuClaimsExpenses = New System.Windows.Forms.ToolStripMenuItem
Me.mnuAuditTrail = New System.Windows.Forms.ToolStripMenuItem
Me.mnuView = New System.Windows.Forms.ToolStripMenuItem
Me.mnuPayrollView = New System.Windows.Forms.ToolStripMenuItem
Me.mnuReportView = New System.Windows.Forms.ToolStripMenuItem
Me.mnuCustomReporting = New System.Windows.Forms.ToolStripMenuItem
Me.mnuHelp = New System.Windows.Forms.ToolStripMenuItem
Me.mnuRegister = New System.Windows.Forms.ToolStripMenuItem
Me.mnuProduct = New System.Windows.Forms.ToolStripMenuItem
Me.mnuAbout = New System.Windows.Forms.ToolStripMenuItem
Me.mnuArutiHelp = New System.Windows.Forms.ToolStripMenuItem
Me.mnuExit = New System.Windows.Forms.ToolStripMenuItem
Me.mnuTrainingFeedback = New System.Windows.Forms.ToolStripMenuItem
Me.StatusStrip1 = New System.Windows.Forms.StatusStrip
Me.lblPropertyName = New System.Windows.Forms.ToolStripStatusLabel
Me.objbtnPropertyName = New System.Windows.Forms.ToolStripStatusLabel
Me.objlblSep1 = New System.Windows.Forms.ToolStripStatusLabel
Me.lblPayrollYear = New System.Windows.Forms.ToolStripStatusLabel
Me.objbtnYearName = New System.Windows.Forms.ToolStripStatusLabel
Me.objlblSep2 = New System.Windows.Forms.ToolStripStatusLabel
Me.lblUserName = New System.Windows.Forms.ToolStripStatusLabel
Me.objbtnUserName = New System.Windows.Forms.ToolStripStatusLabel
Me.objlblSep3 = New System.Windows.Forms.ToolStripStatusLabel
Me.lblWorkingDate = New System.Windows.Forms.ToolStripStatusLabel
Me.objbtnWorkingDate = New System.Windows.Forms.ToolStripStatusLabel
Me.objlblSep4 = New System.Windows.Forms.ToolStripStatusLabel
Me.tmrReminder = New System.Windows.Forms.Timer(Me.components)
Me.dtpEmployeeAsOnDate = New System.Windows.Forms.DateTimePicker
Me.Panel1 = New System.Windows.Forms.Panel
Me.lblEmployeeAsOnDate = New System.Windows.Forms.Label
Me.mnuTroubleshoot = New System.Windows.Forms.ToolStripMenuItem
Me.tblMDILayout.SuspendLayout
Me.fpnlButtons.SuspendLayout
Me.fpnlAssessmentSetups.SuspendLayout
Me.fpnlLeaveInfo.SuspendLayout
Me.fpnlCommomMasters.SuspendLayout
Me.fpnlTnAInfo.SuspendLayout
Me.fpnlPayrollMasters.SuspendLayout
Me.fpnlAssessment.SuspendLayout
Me.fpnlDiscipline.SuspendLayout
Me.fpnlHRMaster.SuspendLayout
Me.fpnlLoanInfo.SuspendLayout
Me.fpnlRecruitment.SuspendLayout
Me.fpnlEmpData.SuspendLayout
Me.fpnlTrainingInfo.SuspendLayout
Me.fpnlUtility.SuspendLayout
Me.fpnlPayrollTransaction.SuspendLayout
Me.fpnlCoreSetups.SuspendLayout
Me.fpnlBSC.SuspendLayout
Me.fpnlTrainingNeed.SuspendLayout
Me.fpnlTrainingFeedback.SuspendLayout
Me.fpnlAuditTrails.SuspendLayout
Me.fnlWagesSetups.SuspendLayout
Me.fpnlMedicalInfo.SuspendLayout
Me.fpnlPayActivity.SuspendLayout
Me.fpnlAppraisal.SuspendLayout
Me.fpnlClaimsExpenses.SuspendLayout
Me.fpnlPerformanceEval.SuspendLayout
Me.fpnlBudgetInfo.SuspendLayout
Me.fpnlTimesheetInfo.SuspendLayout
Me.objspc1.Panel1.SuspendLayout
Me.objspc1.Panel2.SuspendLayout
Me.objspc1.SuspendLayout
Me.pnlMainInfo.SuspendLayout
Me.mnuArutiPayroll.SuspendLayout
Me.StatusStrip1.SuspendLayout
Me.Panel1.SuspendLayout
Me.SuspendLayout
'
'tblMDILayout
'
Me.tblMDILayout.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink
Me.tblMDILayout.CellBorderStyle = System.Windows.Forms.TableLayoutPanelCellBorderStyle.[Single]
Me.tblMDILayout.ColumnCount = 2
Me.tblMDILayout.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 191!))
Me.tblMDILayout.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100!))
Me.tblMDILayout.Controls.Add(Me.fpnlButtons, 1, 1)
Me.tblMDILayout.Controls.Add(Me.pnlSummary, 1, 0)
Me.tblMDILayout.Controls.Add(Me.objspc1, 0, 0)
Me.tblMDILayout.Dock = System.Windows.Forms.DockStyle.Fill
Me.tblMDILayout.Location = New System.Drawing.Point(0, 0)
Me.tblMDILayout.Name = "tblMDILayout"
Me.tblMDILayout.RowCount = 2
Me.tblMDILayout.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100!))
Me.tblMDILayout.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 0!))
Me.tblMDILayout.Size = New System.Drawing.Size(887, 521)
Me.tblMDILayout.TabIndex = 1
'
'fpnlButtons
'
Me.fpnlButtons.AutoScroll = true
Me.fpnlButtons.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
Me.fpnlButtons.Controls.Add(Me.fpnlAssessmentSetups)
Me.fpnlButtons.Controls.Add(Me.fpnlLeaveInfo)
Me.fpnlButtons.Controls.Add(Me.fpnlCommomMasters)
Me.fpnlButtons.Controls.Add(Me.fpnlTnAInfo)
Me.fpnlButtons.Controls.Add(Me.fpnlPayrollMasters)
Me.fpnlButtons.Controls.Add(Me.fpnlAssessment)
Me.fpnlButtons.Controls.Add(Me.fpnlDiscipline)
Me.fpnlButtons.Controls.Add(Me.fpnlHRMaster)
Me.fpnlButtons.Controls.Add(Me.fpnlLoanInfo)
Me.fpnlButtons.Controls.Add(Me.fpnlRecruitment)
Me.fpnlButtons.Controls.Add(Me.fpnlEmpData)
Me.fpnlButtons.Controls.Add(Me.fpnlTrainingInfo)
Me.fpnlButtons.Controls.Add(Me.fpnlUtility)
Me.fpnlButtons.Controls.Add(Me.fpnlPayrollTransaction)
Me.fpnlButtons.Controls.Add(Me.fpnlCoreSetups)
Me.fpnlButtons.Controls.Add(Me.fpnlBSC)
Me.fpnlButtons.Controls.Add(Me.fpnlTrainingNeed)
Me.fpnlButtons.Controls.Add(Me.fpnlTrainingFeedback)
Me.fpnlButtons.Controls.Add(Me.fpnlAuditTrails)
Me.fpnlButtons.Controls.Add(Me.fnlWagesSetups)
Me.fpnlButtons.Controls.Add(Me.fpnlMedicalInfo)
Me.fpnlButtons.Controls.Add(Me.fpnlPayActivity)
Me.fpnlButtons.Controls.Add(Me.fpnlAppraisal)
Me.fpnlButtons.Controls.Add(Me.fpnlClaimsExpenses)
Me.fpnlButtons.Controls.Add(Me.fpnlPerformanceEval)
Me.fpnlButtons.Controls.Add(Me.fpnlBudgetInfo)
Me.fpnlButtons.Controls.Add(Me.fpnlTimesheetInfo)
Me.fpnlButtons.Dock = System.Windows.Forms.DockStyle.Fill
Me.fpnlButtons.FlowDirection = System.Windows.Forms.FlowDirection.TopDown
Me.fpnlButtons.Location = New System.Drawing.Point(196, 523)
Me.fpnlButtons.Name = "fpnlButtons"
Me.fpnlButtons.Size = New System.Drawing.Size(687, 1)
Me.fpnlButtons.TabIndex = 24
'
'fpnlAssessmentSetups
'
Me.fpnlAssessmentSetups.AutoScroll = true
Me.fpnlAssessmentSetups.Controls.Add(Me.btnAssessmentPeriod)
Me.fpnlAssessmentSetups.Controls.Add(Me.btnAssessorAccess)
Me.fpnlAssessmentSetups.Controls.Add(Me.btnReviewer)
Me.fpnlAssessmentSetups.Controls.Add(Me.btnExternalAssessor)
Me.fpnlAssessmentSetups.Controls.Add(Me.btnMigration)
Me.fpnlAssessmentSetups.Controls.Add(Me.btnAssessmentRatio)
Me.fpnlAssessmentSetups.Controls.Add(Me.btnCloseAssessmentPeriod)
Me.fpnlAssessmentSetups.Controls.Add(Me.btnAssessGroup)
Me.fpnlAssessmentSetups.Controls.Add(Me.btnAssessScale)
Me.fpnlAssessmentSetups.Controls.Add(Me.btnCompetencies)
Me.fpnlAssessmentSetups.Controls.Add(Me.btnAssignCompetencies)
Me.fpnlAssessmentSetups.Controls.Add(Me.btnPerspective)
Me.fpnlAssessmentSetups.Controls.Add(Me.btnAssessCaptionSettings)
Me.fpnlAssessmentSetups.Controls.Add(Me.btnAssessmentMapping)
Me.fpnlAssessmentSetups.Controls.Add(Me.btnBSCViewSetting)
Me.fpnlAssessmentSetups.Controls.Add(Me.btnAssessCompanyGoals)
Me.fpnlAssessmentSetups.Controls.Add(Me.btnAllocationGoals)
Me.fpnlAssessmentSetups.Controls.Add(Me.btnEmployeeGoals)
Me.fpnlAssessmentSetups.Controls.Add(Me.btnPerformancePlan)
Me.fpnlAssessmentSetups.Controls.Add(Me.btnOpenPeriodicReview)
Me.fpnlAssessmentSetups.Controls.Add(Me.btnCustomHeaders)
Me.fpnlAssessmentSetups.Controls.Add(Me.btnCustomItems)
Me.fpnlAssessmentSetups.Controls.Add(Me.btnComputationFormula)
Me.fpnlAssessmentSetups.Controls.Add(Me.btnAssessor)
Me.fpnlAssessmentSetups.Location = New System.Drawing.Point(3, 3)
Me.fpnlAssessmentSetups.Name = "fpnlAssessmentSetups"
Me.fpnlAssessmentSetups.Size = New System.Drawing.Size(191, 39)
Me.fpnlAssessmentSetups.TabIndex = 20
'
'btnAssessmentPeriod
'
Me.btnAssessmentPeriod.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left)  _
            Or System.Windows.Forms.AnchorStyles.Right),System.Windows.Forms.AnchorStyles)
Me.btnAssessmentPeriod.BackColor = System.Drawing.Color.Transparent
Me.btnAssessmentPeriod.BackgroundImage = CType(resources.GetObject("btnAssessmentPeriod.BackgroundImage"),System.Drawing.Image)
Me.btnAssessmentPeriod.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
Me.btnAssessmentPeriod.BorderColor = System.Drawing.Color.Empty
Me.btnAssessmentPeriod.ButtonType = eZee.Common.eZeeLightButton.enButtonType.CheckButton
Me.btnAssessmentPeriod.FlatAppearance.BorderSize = 0
Me.btnAssessmentPeriod.FlatStyle = System.Windows.Forms.FlatStyle.Flat
Me.btnAssessmentPeriod.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
Me.btnAssessmentPeriod.ForeColor = System.Drawing.Color.Black
Me.btnAssessmentPeriod.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
Me.btnAssessmentPeriod.GradientForeColor = System.Drawing.Color.Black
Me.btnAssessmentPeriod.HoverGradientBackColor = System.Drawing.Color.Transparent
Me.btnAssessmentPeriod.HoverGradientForeColor = System.Drawing.Color.Black
Me.btnAssessmentPeriod.Image = Global.Aruti.Main.My.Resources.Resources.Assessment_Periods
Me.btnAssessmentPeriod.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
Me.btnAssessmentPeriod.Location = New System.Drawing.Point(3, 3)
Me.btnAssessmentPeriod.MinimumSize = New System.Drawing.Size(150, 35)
Me.btnAssessmentPeriod.Name = "btnAssessmentPeriod"
Me.btnAssessmentPeriod.PressedGradientBackColor = System.Drawing.Color.Transparent
Me.btnAssessmentPeriod.PressedGradientForeColor = System.Drawing.Color.Black
Me.btnAssessmentPeriod.Size = New System.Drawing.Size(166, 35)
Me.btnAssessmentPeriod.TabIndex = 7
Me.btnAssessmentPeriod.Text = "Assessment &Periods"
Me.btnAssessmentPeriod.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
Me.btnAssessmentPeriod.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText
Me.btnAssessmentPeriod.UseVisualStyleBackColor = false
'
'btnAssessorAccess
'
Me.btnAssessorAccess.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left)  _
            Or System.Windows.Forms.AnchorStyles.Right),System.Windows.Forms.AnchorStyles)
Me.btnAssessorAccess.BackColor = System.Drawing.Color.Transparent
Me.btnAssessorAccess.BackgroundImage = CType(resources.GetObject("btnAssessorAccess.BackgroundImage"),System.Drawing.Image)
Me.btnAssessorAccess.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
Me.btnAssessorAccess.BorderColor = System.Drawing.Color.Empty
Me.btnAssessorAccess.ButtonType = eZee.Common.eZeeLightButton.enButtonType.CheckButton
Me.btnAssessorAccess.FlatAppearance.BorderSize = 0
Me.btnAssessorAccess.FlatStyle = System.Windows.Forms.FlatStyle.Flat
Me.btnAssessorAccess.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
Me.btnAssessorAccess.ForeColor = System.Drawing.Color.Black
Me.btnAssessorAccess.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
Me.btnAssessorAccess.GradientForeColor = System.Drawing.Color.Black
Me.btnAssessorAccess.HoverGradientBackColor = System.Drawing.Color.Transparent
Me.btnAssessorAccess.HoverGradientForeColor = System.Drawing.Color.Black
Me.btnAssessorAccess.Image = Global.Aruti.Main.My.Resources.Resources.Assessor_Access
Me.btnAssessorAccess.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
Me.btnAssessorAccess.Location = New System.Drawing.Point(3, 44)
Me.btnAssessorAccess.MinimumSize = New System.Drawing.Size(150, 35)
Me.btnAssessorAccess.Name = "btnAssessorAccess"
Me.btnAssessorAccess.PressedGradientBackColor = System.Drawing.Color.Transparent
Me.btnAssessorAccess.PressedGradientForeColor = System.Drawing.Color.Black
Me.btnAssessorAccess.Size = New System.Drawing.Size(166, 35)
Me.btnAssessorAccess.TabIndex = 11
Me.btnAssessorAccess.Text = "Assessor A&ccess"
Me.btnAssessorAccess.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
Me.btnAssessorAccess.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText
Me.btnAssessorAccess.UseVisualStyleBackColor = false
'
'btnReviewer
'
Me.btnReviewer.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left)  _
            Or System.Windows.Forms.AnchorStyles.Right),System.Windows.Forms.AnchorStyles)
Me.btnReviewer.BackColor = System.Drawing.Color.Transparent
Me.btnReviewer.BackgroundImage = CType(resources.GetObject("btnReviewer.BackgroundImage"),System.Drawing.Image)
Me.btnReviewer.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
Me.btnReviewer.BorderColor = System.Drawing.Color.Empty
Me.btnReviewer.ButtonType = eZee.Common.eZeeLightButton.enButtonType.CheckButton
Me.btnReviewer.FlatAppearance.BorderSize = 0
Me.btnReviewer.FlatStyle = System.Windows.Forms.FlatStyle.Flat
Me.btnReviewer.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
Me.btnReviewer.ForeColor = System.Drawing.Color.Black
Me.btnReviewer.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
Me.btnReviewer.GradientForeColor = System.Drawing.Color.Black
Me.btnReviewer.HoverGradientBackColor = System.Drawing.Color.Transparent
Me.btnReviewer.HoverGradientForeColor = System.Drawing.Color.Black
Me.btnReviewer.Image = Global.Aruti.Main.My.Resources.Resources.Reviewer
Me.btnReviewer.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
Me.btnReviewer.Location = New System.Drawing.Point(3, 85)
Me.btnReviewer.MinimumSize = New System.Drawing.Size(150, 35)
Me.btnReviewer.Name = "btnReviewer"
Me.btnReviewer.PressedGradientBackColor = System.Drawing.Color.Transparent
Me.btnReviewer.PressedGradientForeColor = System.Drawing.Color.Black
Me.btnReviewer.Size = New System.Drawing.Size(166, 35)
Me.btnReviewer.TabIndex = 15
Me.btnReviewer.Text = "&Reviewer Access"
Me.btnReviewer.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
Me.btnReviewer.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText
Me.btnReviewer.UseVisualStyleBackColor = false
'
'btnExternalAssessor
'
Me.btnExternalAssessor.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left)  _
            Or System.Windows.Forms.AnchorStyles.Right),System.Windows.Forms.AnchorStyles)
Me.btnExternalAssessor.BackColor = System.Drawing.Color.Transparent
Me.btnExternalAssessor.BackgroundImage = CType(resources.GetObject("btnExternalAssessor.BackgroundImage"),System.Drawing.Image)
Me.btnExternalAssessor.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
Me.btnExternalAssessor.BorderColor = System.Drawing.Color.Empty
Me.btnExternalAssessor.ButtonType = eZee.Common.eZeeLightButton.enButtonType.CheckButton
Me.btnExternalAssessor.FlatAppearance.BorderSize = 0
Me.btnExternalAssessor.FlatStyle = System.Windows.Forms.FlatStyle.Flat
Me.btnExternalAssessor.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
Me.btnExternalAssessor.ForeColor = System.Drawing.Color.Black
Me.btnExternalAssessor.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
Me.btnExternalAssessor.GradientForeColor = System.Drawing.Color.Black
Me.btnExternalAssessor.HoverGradientBackColor = System.Drawing.Color.Transparent
Me.btnExternalAssessor.HoverGradientForeColor = System.Drawing.Color.Black
Me.btnExternalAssessor.Image = Global.Aruti.Main.My.Resources.Resources.ExtAssessor
Me.btnExternalAssessor.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
Me.btnExternalAssessor.Location = New System.Drawing.Point(3, 126)
Me.btnExternalAssessor.MinimumSize = New System.Drawing.Size(150, 35)
Me.btnExternalAssessor.Name = "btnExternalAssessor"
Me.btnExternalAssessor.PressedGradientBackColor = System.Drawing.Color.Transparent
Me.btnExternalAssessor.PressedGradientForeColor = System.Drawing.Color.Black
Me.btnExternalAssessor.Size = New System.Drawing.Size(166, 35)
Me.btnExternalAssessor.TabIndex = 17
Me.btnExternalAssessor.Text = "&External Assessor"
Me.btnExternalAssessor.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
Me.btnExternalAssessor.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText
Me.btnExternalAssessor.UseVisualStyleBackColor = false
Me.btnExternalAssessor.Visible = false
'
'btnMigration
'
Me.btnMigration.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left)  _
            Or System.Windows.Forms.AnchorStyles.Right),System.Windows.Forms.AnchorStyles)
Me.btnMigration.BackColor = System.Drawing.Color.Transparent
Me.btnMigration.BackgroundImage = CType(resources.GetObject("btnMigration.BackgroundImage"),System.Drawing.Image)
Me.btnMigration.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
Me.btnMigration.BorderColor = System.Drawing.Color.Empty
Me.btnMigration.ButtonType = eZee.Common.eZeeLightButton.enButtonType.CheckButton
Me.btnMigration.FlatAppearance.BorderSize = 0
Me.btnMigration.FlatStyle = System.Windows.Forms.FlatStyle.Flat
Me.btnMigration.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
Me.btnMigration.ForeColor = System.Drawing.Color.Black
Me.btnMigration.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
Me.btnMigration.GradientForeColor = System.Drawing.Color.Black
Me.btnMigration.HoverGradientBackColor = System.Drawing.Color.Transparent
Me.btnMigration.HoverGradientForeColor = System.Drawing.Color.Black
Me.btnMigration.Image = Global.Aruti.Main.My.Resources.Resources.Transfer_Employee
Me.btnMigration.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
Me.btnMigration.Location = New System.Drawing.Point(3, 167)
Me.btnMigration.MinimumSize = New System.Drawing.Size(150, 35)
Me.btnMigration.Name = "btnMigration"
Me.btnMigration.PressedGradientBackColor = System.Drawing.Color.Transparent
Me.btnMigration.PressedGradientForeColor = System.Drawing.Color.Black
Me.btnMigration.Size = New System.Drawing.Size(166, 35)
Me.btnMigration.TabIndex = 18
Me.btnMigration.Text = "Assessor/Reviewer Migration"
Me.btnMigration.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
Me.btnMigration.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText
Me.btnMigration.UseVisualStyleBackColor = false
'
'btnAssessmentRatio
'
Me.btnAssessmentRatio.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left)  _
            Or System.Windows.Forms.AnchorStyles.Right),System.Windows.Forms.AnchorStyles)
Me.btnAssessmentRatio.BackColor = System.Drawing.Color.Transparent
Me.btnAssessmentRatio.BackgroundImage = CType(resources.GetObject("btnAssessmentRatio.BackgroundImage"),System.Drawing.Image)
Me.btnAssessmentRatio.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
Me.btnAssessmentRatio.BorderColor = System.Drawing.Color.Empty
Me.btnAssessmentRatio.ButtonType = eZee.Common.eZeeLightButton.enButtonType.CheckButton
Me.btnAssessmentRatio.FlatAppearance.BorderSize = 0
Me.btnAssessmentRatio.FlatStyle = System.Windows.Forms.FlatStyle.Flat
Me.btnAssessmentRatio.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
Me.btnAssessmentRatio.ForeColor = System.Drawing.Color.Black
Me.btnAssessmentRatio.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
Me.btnAssessmentRatio.GradientForeColor = System.Drawing.Color.Black
Me.btnAssessmentRatio.HoverGradientBackColor = System.Drawing.Color.Transparent
Me.btnAssessmentRatio.HoverGradientForeColor = System.Drawing.Color.Black
Me.btnAssessmentRatio.Image = Global.Aruti.Main.My.Resources.Resources.weight_setting_24
Me.btnAssessmentRatio.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
Me.btnAssessmentRatio.Location = New System.Drawing.Point(3, 208)
Me.btnAssessmentRatio.MinimumSize = New System.Drawing.Size(150, 35)
Me.btnAssessmentRatio.Name = "btnAssessmentRatio"
Me.btnAssessmentRatio.PressedGradientBackColor = System.Drawing.Color.Transparent
Me.btnAssessmentRatio.PressedGradientForeColor = System.Drawing.Color.Black
Me.btnAssessmentRatio.Size = New System.Drawing.Size(166, 35)
Me.btnAssessmentRatio.TabIndex = 21
Me.btnAssessmentRatio.Text = "Assessment &Ratios"
Me.btnAssessmentRatio.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
Me.btnAssessmentRatio.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText
Me.btnAssessmentRatio.UseVisualStyleBackColor = false
'
'btnCloseAssessmentPeriod
'
Me.btnCloseAssessmentPeriod.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left)  _
            Or System.Windows.Forms.AnchorStyles.Right),System.Windows.Forms.AnchorStyles)
Me.btnCloseAssessmentPeriod.BackColor = System.Drawing.Color.Transparent
Me.btnCloseAssessmentPeriod.BackgroundImage = CType(resources.GetObject("btnCloseAssessmentPeriod.BackgroundImage"),System.Drawing.Image)
Me.btnCloseAssessmentPeriod.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
Me.btnCloseAssessmentPeriod.BorderColor = System.Drawing.Color.Empty
Me.btnCloseAssessmentPeriod.ButtonType = eZee.Common.eZeeLightButton.enButtonType.CheckButton
Me.btnCloseAssessmentPeriod.FlatAppearance.BorderSize = 0
Me.btnCloseAssessmentPeriod.FlatStyle = System.Windows.Forms.FlatStyle.Flat
Me.btnCloseAssessmentPeriod.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
Me.btnCloseAssessmentPeriod.ForeColor = System.Drawing.Color.Black
Me.btnCloseAssessmentPeriod.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
Me.btnCloseAssessmentPeriod.GradientForeColor = System.Drawing.Color.Black
Me.btnCloseAssessmentPeriod.HoverGradientBackColor = System.Drawing.Color.Transparent
Me.btnCloseAssessmentPeriod.HoverGradientForeColor = System.Drawing.Color.Black
Me.btnCloseAssessmentPeriod.Image = Global.Aruti.Main.My.Resources.Resources.Close_Payroll
Me.btnCloseAssessmentPeriod.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
Me.btnCloseAssessmentPeriod.Location = New System.Drawing.Point(3, 249)
Me.btnCloseAssessmentPeriod.MinimumSize = New System.Drawing.Size(150, 35)
Me.btnCloseAssessmentPeriod.Name = "btnCloseAssessmentPeriod"
Me.btnCloseAssessmentPeriod.PressedGradientBackColor = System.Drawing.Color.Transparent
Me.btnCloseAssessmentPeriod.PressedGradientForeColor = System.Drawing.Color.Black
Me.btnCloseAssessmentPeriod.Size = New System.Drawing.Size(166, 35)
Me.btnCloseAssessmentPeriod.TabIndex = 19
Me.btnCloseAssessmentPeriod.Text = "Close Assessment &Period"
Me.btnCloseAssessmentPeriod.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
Me.btnCloseAssessmentPeriod.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText
Me.btnCloseAssessmentPeriod.UseVisualStyleBackColor = false
'
'btnAssessGroup
'
Me.btnAssessGroup.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left)  _
            Or System.Windows.Forms.AnchorStyles.Right),System.Windows.Forms.AnchorStyles)
Me.btnAssessGroup.BackColor = System.Drawing.Color.Transparent
Me.btnAssessGroup.BackgroundImage = CType(resources.GetObject("btnAssessGroup.BackgroundImage"),System.Drawing.Image)
Me.btnAssessGroup.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
Me.btnAssessGroup.BorderColor = System.Drawing.Color.Empty
Me.btnAssessGroup.ButtonType = eZee.Common.eZeeLightButton.enButtonType.CheckButton
Me.btnAssessGroup.FlatAppearance.BorderSize = 0
Me.btnAssessGroup.FlatStyle = System.Windows.Forms.FlatStyle.Flat
Me.btnAssessGroup.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
Me.btnAssessGroup.ForeColor = System.Drawing.Color.Black
Me.btnAssessGroup.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
Me.btnAssessGroup.GradientForeColor = System.Drawing.Color.Black
Me.btnAssessGroup.HoverGradientBackColor = System.Drawing.Color.Transparent
Me.btnAssessGroup.HoverGradientForeColor = System.Drawing.Color.Black
Me.btnAssessGroup.Image = Global.Aruti.Main.My.Resources.Resources.Assessment_Group
Me.btnAssessGroup.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
Me.btnAssessGroup.Location = New System.Drawing.Point(3, 290)
Me.btnAssessGroup.MinimumSize = New System.Drawing.Size(150, 35)
Me.btnAssessGroup.Name = "btnAssessGroup"
Me.btnAssessGroup.PressedGradientBackColor = System.Drawing.Color.Transparent
Me.btnAssessGroup.PressedGradientForeColor = System.Drawing.Color.Black
Me.btnAssessGroup.Size = New System.Drawing.Size(166, 35)
Me.btnAssessGroup.TabIndex = 8
Me.btnAssessGroup.Text = "Assessment &Groups"
Me.btnAssessGroup.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
Me.btnAssessGroup.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText
Me.btnAssessGroup.UseVisualStyleBackColor = false
'
'btnAssessScale
'
Me.btnAssessScale.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left)  _
            Or System.Windows.Forms.AnchorStyles.Right),System.Windows.Forms.AnchorStyles)
Me.btnAssessScale.BackColor = System.Drawing.Color.Transparent
Me.btnAssessScale.BackgroundImage = CType(resources.GetObject("btnAssessScale.BackgroundImage"),System.Drawing.Image)
Me.btnAssessScale.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
Me.btnAssessScale.BorderColor = System.Drawing.Color.Empty
Me.btnAssessScale.ButtonType = eZee.Common.eZeeLightButton.enButtonType.CheckButton
Me.btnAssessScale.FlatAppearance.BorderSize = 0
Me.btnAssessScale.FlatStyle = System.Windows.Forms.FlatStyle.Flat
Me.btnAssessScale.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
Me.btnAssessScale.ForeColor = System.Drawing.Color.Black
Me.btnAssessScale.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
Me.btnAssessScale.GradientForeColor = System.Drawing.Color.Black
Me.btnAssessScale.HoverGradientBackColor = System.Drawing.Color.Transparent
Me.btnAssessScale.HoverGradientForeColor = System.Drawing.Color.Black
Me.btnAssessScale.Image = Global.Aruti.Main.My.Resources.Resources.discipline_action
Me.btnAssessScale.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
Me.btnAssessScale.Location = New System.Drawing.Point(3, 331)
Me.btnAssessScale.MinimumSize = New System.Drawing.Size(150, 35)
Me.btnAssessScale.Name = "btnAssessScale"
Me.btnAssessScale.PressedGradientBackColor = System.Drawing.Color.Transparent
Me.btnAssessScale.PressedGradientForeColor = System.Drawing.Color.Black
Me.btnAssessScale.Size = New System.Drawing.Size(166, 35)
Me.btnAssessScale.TabIndex = 23
Me.btnAssessScale.Text = "Assessment Scales"
Me.btnAssessScale.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
Me.btnAssessScale.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText
Me.btnAssessScale.UseVisualStyleBackColor = false
'
'btnCompetencies
'
Me.btnCompetencies.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left)  _
            Or System.Windows.Forms.AnchorStyles.Right),System.Windows.Forms.AnchorStyles)
Me.btnCompetencies.BackColor = System.Drawing.Color.Transparent
Me.btnCompetencies.BackgroundImage = CType(resources.GetObject("btnCompetencies.BackgroundImage"),System.Drawing.Image)
Me.btnCompetencies.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
Me.btnCompetencies.BorderColor = System.Drawing.Color.Empty
Me.btnCompetencies.ButtonType = eZee.Common.eZeeLightButton.enButtonType.CheckButton
Me.btnCompetencies.FlatAppearance.BorderSize = 0
Me.btnCompetencies.FlatStyle = System.Windows.Forms.FlatStyle.Flat
Me.btnCompetencies.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
Me.btnCompetencies.ForeColor = System.Drawing.Color.Black
Me.btnCompetencies.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
Me.btnCompetencies.GradientForeColor = System.Drawing.Color.Black
Me.btnCompetencies.HoverGradientBackColor = System.Drawing.Color.Transparent
Me.btnCompetencies.HoverGradientForeColor = System.Drawing.Color.Black
Me.btnCompetencies.Image = Global.Aruti.Main.My.Resources.Resources.Assessment_Item
Me.btnCompetencies.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
Me.btnCompetencies.Location = New System.Drawing.Point(3, 372)
Me.btnCompetencies.MinimumSize = New System.Drawing.Size(150, 35)
Me.btnCompetencies.Name = "btnCompetencies"
Me.btnCompetencies.PressedGradientBackColor = System.Drawing.Color.Transparent
Me.btnCompetencies.PressedGradientForeColor = System.Drawing.Color.Black
Me.btnCompetencies.Size = New System.Drawing.Size(166, 35)
Me.btnCompetencies.TabIndex = 28
Me.btnCompetencies.Text = "Competencies"
Me.btnCompetencies.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
Me.btnCompetencies.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText
Me.btnCompetencies.UseVisualStyleBackColor = false
'
'btnAssignCompetencies
'
Me.btnAssignCompetencies.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left)  _
            Or System.Windows.Forms.AnchorStyles.Right),System.Windows.Forms.AnchorStyles)
Me.btnAssignCompetencies.BackColor = System.Drawing.Color.Transparent
Me.btnAssignCompetencies.BackgroundImage = CType(resources.GetObject("btnAssignCompetencies.BackgroundImage"),System.Drawing.Image)
Me.btnAssignCompetencies.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
Me.btnAssignCompetencies.BorderColor = System.Drawing.Color.Empty
Me.btnAssignCompetencies.ButtonType = eZee.Common.eZeeLightButton.enButtonType.CheckButton
Me.btnAssignCompetencies.FlatAppearance.BorderSize = 0
Me.btnAssignCompetencies.FlatStyle = System.Windows.Forms.FlatStyle.Flat
Me.btnAssignCompetencies.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
Me.btnAssignCompetencies.ForeColor = System.Drawing.Color.Black
Me.btnAssignCompetencies.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
Me.btnAssignCompetencies.GradientForeColor = System.Drawing.Color.Black
Me.btnAssignCompetencies.HoverGradientBackColor = System.Drawing.Color.Transparent
Me.btnAssignCompetencies.HoverGradientForeColor = System.Drawing.Color.Black
Me.btnAssignCompetencies.Image = Global.Aruti.Main.My.Resources.Resources.Assign_Category
Me.btnAssignCompetencies.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
Me.btnAssignCompetencies.Location = New System.Drawing.Point(3, 413)
Me.btnAssignCompetencies.MinimumSize = New System.Drawing.Size(150, 35)
Me.btnAssignCompetencies.Name = "btnAssignCompetencies"
Me.btnAssignCompetencies.PressedGradientBackColor = System.Drawing.Color.Transparent
Me.btnAssignCompetencies.PressedGradientForeColor = System.Drawing.Color.Black
Me.btnAssignCompetencies.Size = New System.Drawing.Size(166, 35)
Me.btnAssignCompetencies.TabIndex = 29
Me.btnAssignCompetencies.Text = "Assign Competencies"
Me.btnAssignCompetencies.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
Me.btnAssignCompetencies.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText
Me.btnAssignCompetencies.UseVisualStyleBackColor = false
'
'btnPerspective
'
Me.btnPerspective.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left)  _
            Or System.Windows.Forms.AnchorStyles.Right),System.Windows.Forms.AnchorStyles)
Me.btnPerspective.BackColor = System.Drawing.Color.Transparent
Me.btnPerspective.BackgroundImage = CType(resources.GetObject("btnPerspective.BackgroundImage"),System.Drawing.Image)
Me.btnPerspective.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
Me.btnPerspective.BorderColor = System.Drawing.Color.Empty
Me.btnPerspective.ButtonType = eZee.Common.eZeeLightButton.enButtonType.CheckButton
Me.btnPerspective.FlatAppearance.BorderSize = 0
Me.btnPerspective.FlatStyle = System.Windows.Forms.FlatStyle.Flat
Me.btnPerspective.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
Me.btnPerspective.ForeColor = System.Drawing.Color.Black
Me.btnPerspective.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
Me.btnPerspective.GradientForeColor = System.Drawing.Color.Black
Me.btnPerspective.HoverGradientBackColor = System.Drawing.Color.Transparent
Me.btnPerspective.HoverGradientForeColor = System.Drawing.Color.Black
Me.btnPerspective.Image = Global.Aruti.Main.My.Resources.Resources.Self_assessed_bsc
Me.btnPerspective.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
Me.btnPerspective.Location = New System.Drawing.Point(3, 454)
Me.btnPerspective.MinimumSize = New System.Drawing.Size(150, 35)
Me.btnPerspective.Name = "btnPerspective"
Me.btnPerspective.PressedGradientBackColor = System.Drawing.Color.Transparent
Me.btnPerspective.PressedGradientForeColor = System.Drawing.Color.Black
Me.btnPerspective.Size = New System.Drawing.Size(166, 35)
Me.btnPerspective.TabIndex = 33
Me.btnPerspective.Text = "Perspective"
Me.btnPerspective.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
Me.btnPerspective.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText
Me.btnPerspective.UseVisualStyleBackColor = false
'
'btnAssessCaptionSettings
'
Me.btnAssessCaptionSettings.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left)  _
            Or System.Windows.Forms.AnchorStyles.Right),System.Windows.Forms.AnchorStyles)
Me.btnAssessCaptionSettings.BackColor = System.Drawing.Color.Transparent
Me.btnAssessCaptionSettings.BackgroundImage = CType(resources.GetObject("btnAssessCaptionSettings.BackgroundImage"),System.Drawing.Image)
Me.btnAssessCaptionSettings.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
Me.btnAssessCaptionSettings.BorderColor = System.Drawing.Color.Empty
Me.btnAssessCaptionSettings.ButtonType = eZee.Common.eZeeLightButton.enButtonType.CheckButton
Me.btnAssessCaptionSettings.FlatAppearance.BorderSize = 0
Me.btnAssessCaptionSettings.FlatStyle = System.Windows.Forms.FlatStyle.Flat
Me.btnAssessCaptionSettings.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
Me.btnAssessCaptionSettings.ForeColor = System.Drawing.Color.Black
Me.btnAssessCaptionSettings.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
Me.btnAssessCaptionSettings.GradientForeColor = System.Drawing.Color.Black
Me.btnAssessCaptionSettings.HoverGradientBackColor = System.Drawing.Color.Transparent
Me.btnAssessCaptionSettings.HoverGradientForeColor = System.Drawing.Color.Black
Me.btnAssessCaptionSettings.Image = Global.Aruti.Main.My.Resources.Resources.Objective
Me.btnAssessCaptionSettings.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
Me.btnAssessCaptionSettings.Location = New System.Drawing.Point(3, 495)
Me.btnAssessCaptionSettings.MinimumSize = New System.Drawing.Size(150, 35)
Me.btnAssessCaptionSettings.Name = "btnAssessCaptionSettings"
Me.btnAssessCaptionSettings.PressedGradientBackColor = System.Drawing.Color.Transparent
Me.btnAssessCaptionSettings.PressedGradientForeColor = System.Drawing.Color.Black
Me.btnAssessCaptionSettings.Size = New System.Drawing.Size(166, 35)
Me.btnAssessCaptionSettings.TabIndex = 22
Me.btnAssessCaptionSettings.Text = "BSC Titles"
Me.btnAssessCaptionSettings.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
Me.btnAssessCaptionSettings.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText
Me.btnAssessCaptionSettings.UseVisualStyleBackColor = false
'
'btnAssessmentMapping
'
Me.btnAssessmentMapping.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left)  _
            Or System.Windows.Forms.AnchorStyles.Right),System.Windows.Forms.AnchorStyles)
Me.btnAssessmentMapping.BackColor = System.Drawing.Color.Transparent
Me.btnAssessmentMapping.BackgroundImage = CType(resources.GetObject("btnAssessmentMapping.BackgroundImage"),System.Drawing.Image)
Me.btnAssessmentMapping.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
Me.btnAssessmentMapping.BorderColor = System.Drawing.Color.Empty
Me.btnAssessmentMapping.ButtonType = eZee.Common.eZeeLightButton.enButtonType.CheckButton
Me.btnAssessmentMapping.FlatAppearance.BorderSize = 0
Me.btnAssessmentMapping.FlatStyle = System.Windows.Forms.FlatStyle.Flat
Me.btnAssessmentMapping.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
Me.btnAssessmentMapping.ForeColor = System.Drawing.Color.Black
Me.btnAssessmentMapping.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
Me.btnAssessmentMapping.GradientForeColor = System.Drawing.Color.Black
Me.btnAssessmentMapping.HoverGradientBackColor = System.Drawing.Color.Transparent
Me.btnAssessmentMapping.HoverGradientForeColor = System.Drawing.Color.Black
Me.btnAssessmentMapping.Image = Global.Aruti.Main.My.Resources.Resources.targets
Me.btnAssessmentMapping.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
Me.btnAssessmentMapping.Location = New System.Drawing.Point(3, 536)
Me.btnAssessmentMapping.MinimumSize = New System.Drawing.Size(150, 35)
Me.btnAssessmentMapping.Name = "btnAssessmentMapping"
Me.btnAssessmentMapping.PressedGradientBackColor = System.Drawing.Color.Transparent
Me.btnAssessmentMapping.PressedGradientForeColor = System.Drawing.Color.Black
Me.btnAssessmentMapping.Size = New System.Drawing.Size(166, 35)
Me.btnAssessmentMapping.TabIndex = 27
Me.btnAssessmentMapping.Text = "BSC Title Mapping"
Me.btnAssessmentMapping.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
Me.btnAssessmentMapping.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText
Me.btnAssessmentMapping.UseVisualStyleBackColor = false
'
'btnBSCViewSetting
'
Me.btnBSCViewSetting.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left)  _
            Or System.Windows.Forms.AnchorStyles.Right),System.Windows.Forms.AnchorStyles)
Me.btnBSCViewSetting.BackColor = System.Drawing.Color.Transparent
Me.btnBSCViewSetting.BackgroundImage = CType(resources.GetObject("btnBSCViewSetting.BackgroundImage"),System.Drawing.Image)
Me.btnBSCViewSetting.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
Me.btnBSCViewSetting.BorderColor = System.Drawing.Color.Empty
Me.btnBSCViewSetting.ButtonType = eZee.Common.eZeeLightButton.enButtonType.CheckButton
Me.btnBSCViewSetting.FlatAppearance.BorderSize = 0
Me.btnBSCViewSetting.FlatStyle = System.Windows.Forms.FlatStyle.Flat
Me.btnBSCViewSetting.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
Me.btnBSCViewSetting.ForeColor = System.Drawing.Color.Black
Me.btnBSCViewSetting.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
Me.btnBSCViewSetting.GradientForeColor = System.Drawing.Color.Black
Me.btnBSCViewSetting.HoverGradientBackColor = System.Drawing.Color.Transparent
Me.btnBSCViewSetting.HoverGradientForeColor = System.Drawing.Color.Black
Me.btnBSCViewSetting.Image = Global.Aruti.Main.My.Resources.Resources.KPI
Me.btnBSCViewSetting.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
Me.btnBSCViewSetting.Location = New System.Drawing.Point(3, 577)
Me.btnBSCViewSetting.MinimumSize = New System.Drawing.Size(150, 35)
Me.btnBSCViewSetting.Name = "btnBSCViewSetting"
Me.btnBSCViewSetting.PressedGradientBackColor = System.Drawing.Color.Transparent
Me.btnBSCViewSetting.PressedGradientForeColor = System.Drawing.Color.Black
Me.btnBSCViewSetting.Size = New System.Drawing.Size(166, 35)
Me.btnBSCViewSetting.TabIndex = 31
Me.btnBSCViewSetting.Text = "BSC Title View Setting"
Me.btnBSCViewSetting.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
Me.btnBSCViewSetting.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText
Me.btnBSCViewSetting.UseVisualStyleBackColor = false
'
'btnAssessCompanyGoals
'
Me.btnAssessCompanyGoals.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left)  _
            Or System.Windows.Forms.AnchorStyles.Right),System.Windows.Forms.AnchorStyles)
Me.btnAssessCompanyGoals.BackColor = System.Drawing.Color.Transparent
Me.btnAssessCompanyGoals.BackgroundImage = CType(resources.GetObject("btnAssessCompanyGoals.BackgroundImage"),System.Drawing.Image)
Me.btnAssessCompanyGoals.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
Me.btnAssessCompanyGoals.BorderColor = System.Drawing.Color.Empty
Me.btnAssessCompanyGoals.ButtonType = eZee.Common.eZeeLightButton.enButtonType.CheckButton
Me.btnAssessCompanyGoals.FlatAppearance.BorderSize = 0
Me.btnAssessCompanyGoals.FlatStyle = System.Windows.Forms.FlatStyle.Flat
Me.btnAssessCompanyGoals.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
Me.btnAssessCompanyGoals.ForeColor = System.Drawing.Color.Black
Me.btnAssessCompanyGoals.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
Me.btnAssessCompanyGoals.GradientForeColor = System.Drawing.Color.Black
Me.btnAssessCompanyGoals.HoverGradientBackColor = System.Drawing.Color.Transparent
Me.btnAssessCompanyGoals.HoverGradientForeColor = System.Drawing.Color.Black
Me.btnAssessCompanyGoals.Image = Global.Aruti.Main.My.Resources.Resources.Sick_Sheet_form
Me.btnAssessCompanyGoals.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
Me.btnAssessCompanyGoals.Location = New System.Drawing.Point(3, 618)
Me.btnAssessCompanyGoals.MinimumSize = New System.Drawing.Size(150, 35)
Me.btnAssessCompanyGoals.Name = "btnAssessCompanyGoals"
Me.btnAssessCompanyGoals.PressedGradientBackColor = System.Drawing.Color.Transparent
Me.btnAssessCompanyGoals.PressedGradientForeColor = System.Drawing.Color.Black
Me.btnAssessCompanyGoals.Size = New System.Drawing.Size(166, 35)
Me.btnAssessCompanyGoals.TabIndex = 24
Me.btnAssessCompanyGoals.Text = "Company Goals"
Me.btnAssessCompanyGoals.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
Me.btnAssessCompanyGoals.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText
Me.btnAssessCompanyGoals.UseVisualStyleBackColor = false
'
'btnAllocationGoals
'
Me.btnAllocationGoals.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left)  _
            Or System.Windows.Forms.AnchorStyles.Right),System.Windows.Forms.AnchorStyles)
Me.btnAllocationGoals.BackColor = System.Drawing.Color.Transparent
Me.btnAllocationGoals.BackgroundImage = CType(resources.GetObject("btnAllocationGoals.BackgroundImage"),System.Drawing.Image)
Me.btnAllocationGoals.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
Me.btnAllocationGoals.BorderColor = System.Drawing.Color.Empty
Me.btnAllocationGoals.ButtonType = eZee.Common.eZeeLightButton.enButtonType.CheckButton
Me.btnAllocationGoals.FlatAppearance.BorderSize = 0
Me.btnAllocationGoals.FlatStyle = System.Windows.Forms.FlatStyle.Flat
Me.btnAllocationGoals.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
Me.btnAllocationGoals.ForeColor = System.Drawing.Color.Black
Me.btnAllocationGoals.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
Me.btnAllocationGoals.GradientForeColor = System.Drawing.Color.Black
Me.btnAllocationGoals.HoverGradientBackColor = System.Drawing.Color.Transparent
Me.btnAllocationGoals.HoverGradientForeColor = System.Drawing.Color.Black
Me.btnAllocationGoals.Image = Global.Aruti.Main.My.Resources.Resources.Analysis_List
Me.btnAllocationGoals.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
Me.btnAllocationGoals.Location = New System.Drawing.Point(3, 659)
Me.btnAllocationGoals.MinimumSize = New System.Drawing.Size(150, 35)
Me.btnAllocationGoals.Name = "btnAllocationGoals"
Me.btnAllocationGoals.PressedGradientBackColor = System.Drawing.Color.Transparent
Me.btnAllocationGoals.PressedGradientForeColor = System.Drawing.Color.Black
Me.btnAllocationGoals.Size = New System.Drawing.Size(166, 35)
Me.btnAllocationGoals.TabIndex = 25
Me.btnAllocationGoals.Text = "Allocation Goals"
Me.btnAllocationGoals.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
Me.btnAllocationGoals.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText
Me.btnAllocationGoals.UseVisualStyleBackColor = false
'
'btnEmployeeGoals
'
Me.btnEmployeeGoals.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left)  _
            Or System.Windows.Forms.AnchorStyles.Right),System.Windows.Forms.AnchorStyles)
Me.btnEmployeeGoals.BackColor = System.Drawing.Color.Transparent
Me.btnEmployeeGoals.BackgroundImage = CType(resources.GetObject("btnEmployeeGoals.BackgroundImage"),System.Drawing.Image)
Me.btnEmployeeGoals.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
Me.btnEmployeeGoals.BorderColor = System.Drawing.Color.Empty
Me.btnEmployeeGoals.ButtonType = eZee.Common.eZeeLightButton.enButtonType.CheckButton
Me.btnEmployeeGoals.FlatAppearance.BorderSize = 0
Me.btnEmployeeGoals.FlatStyle = System.Windows.Forms.FlatStyle.Flat
Me.btnEmployeeGoals.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
Me.btnEmployeeGoals.ForeColor = System.Drawing.Color.Black
Me.btnEmployeeGoals.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
Me.btnEmployeeGoals.GradientForeColor = System.Drawing.Color.Black
Me.btnEmployeeGoals.HoverGradientBackColor = System.Drawing.Color.Transparent
Me.btnEmployeeGoals.HoverGradientForeColor = System.Drawing.Color.Black
Me.btnEmployeeGoals.Image = Global.Aruti.Main.My.Resources.Resources.Master_Group
Me.btnEmployeeGoals.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
Me.btnEmployeeGoals.Location = New System.Drawing.Point(3, 700)
Me.btnEmployeeGoals.MinimumSize = New System.Drawing.Size(150, 35)
Me.btnEmployeeGoals.Name = "btnEmployeeGoals"
Me.btnEmployeeGoals.PressedGradientBackColor = System.Drawing.Color.Transparent
Me.btnEmployeeGoals.PressedGradientForeColor = System.Drawing.Color.Black
Me.btnEmployeeGoals.Size = New System.Drawing.Size(166, 35)
Me.btnEmployeeGoals.TabIndex = 26
Me.btnEmployeeGoals.Text = "Employee Goals"
Me.btnEmployeeGoals.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
Me.btnEmployeeGoals.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText
Me.btnEmployeeGoals.UseVisualStyleBackColor = false
'
'btnPerformancePlan
'
Me.btnPerformancePlan.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left)  _
            Or System.Windows.Forms.AnchorStyles.Right),System.Windows.Forms.AnchorStyles)
Me.btnPerformancePlan.BackColor = System.Drawing.Color.Transparent
Me.btnPerformancePlan.BackgroundImage = CType(resources.GetObject("btnPerformancePlan.BackgroundImage"),System.Drawing.Image)
Me.btnPerformancePlan.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
Me.btnPerformancePlan.BorderColor = System.Drawing.Color.Empty
Me.btnPerformancePlan.ButtonType = eZee.Common.eZeeLightButton.enButtonType.CheckButton
Me.btnPerformancePlan.FlatAppearance.BorderSize = 0
Me.btnPerformancePlan.FlatStyle = System.Windows.Forms.FlatStyle.Flat
Me.btnPerformancePlan.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
Me.btnPerformancePlan.ForeColor = System.Drawing.Color.Black
Me.btnPerformancePlan.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
Me.btnPerformancePlan.GradientForeColor = System.Drawing.Color.Black
Me.btnPerformancePlan.HoverGradientBackColor = System.Drawing.Color.Transparent
Me.btnPerformancePlan.HoverGradientForeColor = System.Drawing.Color.Black
Me.btnPerformancePlan.Image = Global.Aruti.Main.My.Resources.Resources.Self_assessed_bsc
Me.btnPerformancePlan.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
Me.btnPerformancePlan.Location = New System.Drawing.Point(3, 741)
Me.btnPerformancePlan.MinimumSize = New System.Drawing.Size(150, 35)
Me.btnPerformancePlan.Name = "btnPerformancePlan"
Me.btnPerformancePlan.PressedGradientBackColor = System.Drawing.Color.Transparent
Me.btnPerformancePlan.PressedGradientForeColor = System.Drawing.Color.Black
Me.btnPerformancePlan.Size = New System.Drawing.Size(166, 35)
Me.btnPerformancePlan.TabIndex = 35
Me.btnPerformancePlan.Text = "Approve/Reject Goals"
Me.btnPerformancePlan.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
Me.btnPerformancePlan.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText
Me.btnPerformancePlan.UseVisualStyleBackColor = false
'
'btnOpenPeriodicReview
'
Me.btnOpenPeriodicReview.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left)  _
            Or System.Windows.Forms.AnchorStyles.Right),System.Windows.Forms.AnchorStyles)
Me.btnOpenPeriodicReview.BackColor = System.Drawing.Color.Transparent
Me.btnOpenPeriodicReview.BackgroundImage = CType(resources.GetObject("btnOpenPeriodicReview.BackgroundImage"),System.Drawing.Image)
Me.btnOpenPeriodicReview.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
Me.btnOpenPeriodicReview.BorderColor = System.Drawing.Color.Empty
Me.btnOpenPeriodicReview.ButtonType = eZee.Common.eZeeLightButton.enButtonType.CheckButton
Me.btnOpenPeriodicReview.FlatAppearance.BorderSize = 0
Me.btnOpenPeriodicReview.FlatStyle = System.Windows.Forms.FlatStyle.Flat
Me.btnOpenPeriodicReview.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
Me.btnOpenPeriodicReview.ForeColor = System.Drawing.Color.Black
Me.btnOpenPeriodicReview.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
Me.btnOpenPeriodicReview.GradientForeColor = System.Drawing.Color.Black
Me.btnOpenPeriodicReview.HoverGradientBackColor = System.Drawing.Color.Transparent
Me.btnOpenPeriodicReview.HoverGradientForeColor = System.Drawing.Color.Black
Me.btnOpenPeriodicReview.Image = Global.Aruti.Main.My.Resources.Resources.Assessment
Me.btnOpenPeriodicReview.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
Me.btnOpenPeriodicReview.Location = New System.Drawing.Point(3, 782)
Me.btnOpenPeriodicReview.MinimumSize = New System.Drawing.Size(150, 35)
Me.btnOpenPeriodicReview.Name = "btnOpenPeriodicReview"
Me.btnOpenPeriodicReview.PressedGradientBackColor = System.Drawing.Color.Transparent
Me.btnOpenPeriodicReview.PressedGradientForeColor = System.Drawing.Color.Black
Me.btnOpenPeriodicReview.Size = New System.Drawing.Size(166, 35)
Me.btnOpenPeriodicReview.TabIndex = 32
Me.btnOpenPeriodicReview.Text = "Unlock for Periodic Review"
Me.btnOpenPeriodicReview.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
Me.btnOpenPeriodicReview.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText
Me.btnOpenPeriodicReview.UseVisualStyleBackColor = false
'
'btnCustomHeaders
'
Me.btnCustomHeaders.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left)  _
            Or System.Windows.Forms.AnchorStyles.Right),System.Windows.Forms.AnchorStyles)
Me.btnCustomHeaders.BackColor = System.Drawing.Color.Transparent
Me.btnCustomHeaders.BackgroundImage = CType(resources.GetObject("btnCustomHeaders.BackgroundImage"),System.Drawing.Image)
Me.btnCustomHeaders.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
Me.btnCustomHeaders.BorderColor = System.Drawing.Color.Empty
Me.btnCustomHeaders.ButtonType = eZee.Common.eZeeLightButton.enButtonType.CheckButton
Me.btnCustomHeaders.FlatAppearance.BorderSize = 0
Me.btnCustomHeaders.FlatStyle = System.Windows.Forms.FlatStyle.Flat
Me.btnCustomHeaders.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
Me.btnCustomHeaders.ForeColor = System.Drawing.Color.Black
Me.btnCustomHeaders.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
Me.btnCustomHeaders.GradientForeColor = System.Drawing.Color.Black
Me.btnCustomHeaders.HoverGradientBackColor = System.Drawing.Color.Transparent
Me.btnCustomHeaders.HoverGradientForeColor = System.Drawing.Color.Black
Me.btnCustomHeaders.Image = Global.Aruti.Main.My.Resources.Resources.Reminder
Me.btnCustomHeaders.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
Me.btnCustomHeaders.Location = New System.Drawing.Point(3, 823)
Me.btnCustomHeaders.MinimumSize = New System.Drawing.Size(150, 35)
Me.btnCustomHeaders.Name = "btnCustomHeaders"
Me.btnCustomHeaders.PressedGradientBackColor = System.Drawing.Color.Transparent
Me.btnCustomHeaders.PressedGradientForeColor = System.Drawing.Color.Black
Me.btnCustomHeaders.Size = New System.Drawing.Size(166, 35)
Me.btnCustomHeaders.TabIndex = 29
Me.btnCustomHeaders.Text = "Custom Section Titles"
Me.btnCustomHeaders.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
Me.btnCustomHeaders.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText
Me.btnCustomHeaders.UseVisualStyleBackColor = false
'
'btnCustomItems
'
Me.btnCustomItems.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left)  _
            Or System.Windows.Forms.AnchorStyles.Right),System.Windows.Forms.AnchorStyles)
Me.btnCustomItems.BackColor = System.Drawing.Color.Transparent
Me.btnCustomItems.BackgroundImage = CType(resources.GetObject("btnCustomItems.BackgroundImage"),System.Drawing.Image)
Me.btnCustomItems.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
Me.btnCustomItems.BorderColor = System.Drawing.Color.Empty
Me.btnCustomItems.ButtonType = eZee.Common.eZeeLightButton.enButtonType.CheckButton
Me.btnCustomItems.FlatAppearance.BorderSize = 0
Me.btnCustomItems.FlatStyle = System.Windows.Forms.FlatStyle.Flat
Me.btnCustomItems.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
Me.btnCustomItems.ForeColor = System.Drawing.Color.Black
Me.btnCustomItems.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
Me.btnCustomItems.GradientForeColor = System.Drawing.Color.Black
Me.btnCustomItems.HoverGradientBackColor = System.Drawing.Color.Transparent
Me.btnCustomItems.HoverGradientForeColor = System.Drawing.Color.Black
Me.btnCustomItems.Image = Global.Aruti.Main.My.Resources.Resources.Assessment_Item
Me.btnCustomItems.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
Me.btnCustomItems.Location = New System.Drawing.Point(3, 864)
Me.btnCustomItems.MinimumSize = New System.Drawing.Size(150, 35)
Me.btnCustomItems.Name = "btnCustomItems"
Me.btnCustomItems.PressedGradientBackColor = System.Drawing.Color.Transparent
Me.btnCustomItems.PressedGradientForeColor = System.Drawing.Color.Black
Me.btnCustomItems.Size = New System.Drawing.Size(166, 35)
Me.btnCustomItems.TabIndex = 30
Me.btnCustomItems.Text = "Custom Assessment Items"
Me.btnCustomItems.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
Me.btnCustomItems.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText
Me.btnCustomItems.UseVisualStyleBackColor = false
'
'btnComputationFormula
'
Me.btnComputationFormula.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left)  _
            Or System.Windows.Forms.AnchorStyles.Right),System.Windows.Forms.AnchorStyles)
Me.btnComputationFormula.BackColor = System.Drawing.Color.Transparent
Me.btnComputationFormula.BackgroundImage = CType(resources.GetObject("btnComputationFormula.BackgroundImage"),System.Drawing.Image)
Me.btnComputationFormula.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
Me.btnComputationFormula.BorderColor = System.Drawing.Color.Empty
Me.btnComputationFormula.ButtonType = eZee.Common.eZeeLightButton.enButtonType.CheckButton
Me.btnComputationFormula.FlatAppearance.BorderSize = 0
Me.btnComputationFormula.FlatStyle = System.Windows.Forms.FlatStyle.Flat
Me.btnComputationFormula.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
Me.btnComputationFormula.ForeColor = System.Drawing.Color.Black
Me.btnComputationFormula.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
Me.btnComputationFormula.GradientForeColor = System.Drawing.Color.Black
Me.btnComputationFormula.HoverGradientBackColor = System.Drawing.Color.Transparent
Me.btnComputationFormula.HoverGradientForeColor = System.Drawing.Color.Black
Me.btnComputationFormula.Image = Global.Aruti.Main.My.Resources.Resources.download_new
Me.btnComputationFormula.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
Me.btnComputationFormula.Location = New System.Drawing.Point(3, 905)
Me.btnComputationFormula.MinimumSize = New System.Drawing.Size(150, 35)
Me.btnComputationFormula.Name = "btnComputationFormula"
Me.btnComputationFormula.PressedGradientBackColor = System.Drawing.Color.Transparent
Me.btnComputationFormula.PressedGradientForeColor = System.Drawing.Color.Black
Me.btnComputationFormula.Size = New System.Drawing.Size(166, 35)
Me.btnComputationFormula.TabIndex = 34
Me.btnComputationFormula.Text = "Computation Formula"
Me.btnComputationFormula.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
Me.btnComputationFormula.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText
Me.btnComputationFormula.UseVisualStyleBackColor = false
'
'btnAssessor
'
Me.btnAssessor.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left)  _
            Or System.Windows.Forms.AnchorStyles.Right),System.Windows.Forms.AnchorStyles)
Me.btnAssessor.BackColor = System.Drawing.Color.Transparent
Me.btnAssessor.BackgroundImage = CType(resources.GetObject("btnAssessor.BackgroundImage"),System.Drawing.Image)
Me.btnAssessor.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
Me.btnAssessor.BorderColor = System.Drawing.Color.Empty
Me.btnAssessor.ButtonType = eZee.Common.eZeeLightButton.enButtonType.CheckButton
Me.btnAssessor.FlatAppearance.BorderSize = 0
Me.btnAssessor.FlatStyle = System.Windows.Forms.FlatStyle.Flat
Me.btnAssessor.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
Me.btnAssessor.ForeColor = System.Drawing.Color.Black
Me.btnAssessor.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
Me.btnAssessor.GradientForeColor = System.Drawing.Color.Black
Me.btnAssessor.HoverGradientBackColor = System.Drawing.Color.Transparent
Me.btnAssessor.HoverGradientForeColor = System.Drawing.Color.Black
Me.btnAssessor.Image = Global.Aruti.Main.My.Resources.Resources.Assessor
Me.btnAssessor.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
Me.btnAssessor.Location = New System.Drawing.Point(3, 946)
Me.btnAssessor.MinimumSize = New System.Drawing.Size(150, 35)
Me.btnAssessor.Name = "btnAssessor"
Me.btnAssessor.PressedGradientBackColor = System.Drawing.Color.Transparent
Me.btnAssessor.PressedGradientForeColor = System.Drawing.Color.Black
Me.btnAssessor.Size = New System.Drawing.Size(166, 35)
Me.btnAssessor.TabIndex = 10
Me.btnAssessor.Text = "Assess&or/Reviewer"
Me.btnAssessor.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
Me.btnAssessor.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText
Me.btnAssessor.UseVisualStyleBackColor = false
Me.btnAssessor.Visible = false
'
'fpnlLeaveInfo
'
Me.fpnlLeaveInfo.AutoScroll = true
Me.fpnlLeaveInfo.Controls.Add(Me.btnLeaveType)
Me.fpnlLeaveInfo.Controls.Add(Me.btnHolidays)
Me.fpnlLeaveInfo.Controls.Add(Me.btnEmployeeHolidays)
Me.fpnlLeaveInfo.Controls.Add(Me.btnApproverLevel)
Me.fpnlLeaveInfo.Controls.Add(Me.btnLeaveApprover)
Me.fpnlLeaveInfo.Controls.Add(Me.btnLeaveForm)
Me.fpnlLeaveInfo.Controls.Add(Me.btnLeaveProcess)
Me.fpnlLeaveInfo.Controls.Add(Me.btnLeaveAccrue)
Me.fpnlLeaveInfo.Controls.Add(Me.btnLeaveViewer)
Me.fpnlLeaveInfo.Controls.Add(Me.btnLeavePlanner)
Me.fpnlLeaveInfo.Controls.Add(Me.btnLeaveSchedular)
Me.fpnlLeaveInfo.Controls.Add(Me.btnImportAccrueLeave)
Me.fpnlLeaveInfo.Controls.Add(Me.BtnTransferApprover)
Me.fpnlLeaveInfo.Controls.Add(Me.btnEndELC)
Me.fpnlLeaveInfo.Controls.Add(Me.btnLeaveFrequency)
Me.fpnlLeaveInfo.Controls.Add(Me.btnLeaveAdjustment)
Me.fpnlLeaveInfo.Controls.Add(Me.btnLVSwapApprover)
Me.fpnlLeaveInfo.Location = New System.Drawing.Point(3, 48)
Me.fpnlLeaveInfo.Name = "fpnlLeaveInfo"
Me.fpnlLeaveInfo.Size = New System.Drawing.Size(191, 39)
Me.fpnlLeaveInfo.TabIndex = 9
Me.fpnlLeaveInfo.Visible = false
'
'btnLeaveType
'
Me.btnLeaveType.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left)  _
            Or System.Windows.Forms.AnchorStyles.Right),System.Windows.Forms.AnchorStyles)
Me.btnLeaveType.BackColor = System.Drawing.Color.Transparent
Me.btnLeaveType.BackgroundImage = CType(resources.GetObject("btnLeaveType.BackgroundImage"),System.Drawing.Image)
Me.btnLeaveType.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
Me.btnLeaveType.BorderColor = System.Drawing.Color.Empty
Me.btnLeaveType.ButtonType = eZee.Common.eZeeLightButton.enButtonType.CheckButton
Me.btnLeaveType.FlatAppearance.BorderSize = 0
Me.btnLeaveType.FlatStyle = System.Windows.Forms.FlatStyle.Flat
Me.btnLeaveType.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
Me.btnLeaveType.ForeColor = System.Drawing.Color.Black
Me.btnLeaveType.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
Me.btnLeaveType.GradientForeColor = System.Drawing.Color.Black
Me.btnLeaveType.HoverGradientBackColor = System.Drawing.Color.Transparent
Me.btnLeaveType.HoverGradientForeColor = System.Drawing.Color.Black
Me.btnLeaveType.Image = Global.Aruti.Main.My.Resources.Resources.LeaveType
Me.btnLeaveType.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
Me.btnLeaveType.Location = New System.Drawing.Point(3, 3)
Me.btnLeaveType.MinimumSize = New System.Drawing.Size(150, 35)
Me.btnLeaveType.Name = "btnLeaveType"
Me.btnLeaveType.PressedGradientBackColor = System.Drawing.Color.Transparent
Me.btnLeaveType.PressedGradientForeColor = System.Drawing.Color.Black
Me.btnLeaveType.Size = New System.Drawing.Size(166, 35)
Me.btnLeaveType.TabIndex = 7
Me.btnLeaveType.Text = "&Leave Type"
Me.btnLeaveType.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
Me.btnLeaveType.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText
Me.btnLeaveType.UseVisualStyleBackColor = false
'
'btnHolidays
'
Me.btnHolidays.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left)  _
            Or System.Windows.Forms.AnchorStyles.Right),System.Windows.Forms.AnchorStyles)
Me.btnHolidays.BackColor = System.Drawing.Color.Transparent
Me.btnHolidays.BackgroundImage = CType(resources.GetObject("btnHolidays.BackgroundImage"),System.Drawing.Image)
Me.btnHolidays.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
Me.btnHolidays.BorderColor = System.Drawing.Color.Empty
Me.btnHolidays.ButtonType = eZee.Common.eZeeLightButton.enButtonType.CheckButton
Me.btnHolidays.FlatAppearance.BorderSize = 0
Me.btnHolidays.FlatStyle = System.Windows.Forms.FlatStyle.Flat
Me.btnHolidays.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
Me.btnHolidays.ForeColor = System.Drawing.Color.Black
Me.btnHolidays.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
Me.btnHolidays.GradientForeColor = System.Drawing.Color.Black
Me.btnHolidays.HoverGradientBackColor = System.Drawing.Color.Transparent
Me.btnHolidays.HoverGradientForeColor = System.Drawing.Color.Black
Me.btnHolidays.Image = Global.Aruti.Main.My.Resources.Resources.HolidayList
Me.btnHolidays.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
Me.btnHolidays.Location = New System.Drawing.Point(3, 44)
Me.btnHolidays.MinimumSize = New System.Drawing.Size(150, 35)
Me.btnHolidays.Name = "btnHolidays"
Me.btnHolidays.PressedGradientBackColor = System.Drawing.Color.Transparent
Me.btnHolidays.PressedGradientForeColor = System.Drawing.Color.Black
Me.btnHolidays.Size = New System.Drawing.Size(166, 35)
Me.btnHolidays.TabIndex = 8
Me.btnHolidays.Text = "&Holidays"
Me.btnHolidays.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
Me.btnHolidays.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText
Me.btnHolidays.UseVisualStyleBackColor = false
'
'btnEmployeeHolidays
'
Me.btnEmployeeHolidays.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left)  _
            Or System.Windows.Forms.AnchorStyles.Right),System.Windows.Forms.AnchorStyles)
Me.btnEmployeeHolidays.BackColor = System.Drawing.Color.Transparent
Me.btnEmployeeHolidays.BackgroundImage = CType(resources.GetObject("btnEmployeeHolidays.BackgroundImage"),System.Drawing.Image)
Me.btnEmployeeHolidays.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
Me.btnEmployeeHolidays.BorderColor = System.Drawing.Color.Empty
Me.btnEmployeeHolidays.ButtonType = eZee.Common.eZeeLightButton.enButtonType.CheckButton
Me.btnEmployeeHolidays.FlatAppearance.BorderSize = 0
Me.btnEmployeeHolidays.FlatStyle = System.Windows.Forms.FlatStyle.Flat
Me.btnEmployeeHolidays.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
Me.btnEmployeeHolidays.ForeColor = System.Drawing.Color.Black
Me.btnEmployeeHolidays.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
Me.btnEmployeeHolidays.GradientForeColor = System.Drawing.Color.Black
Me.btnEmployeeHolidays.HoverGradientBackColor = System.Drawing.Color.Transparent
Me.btnEmployeeHolidays.HoverGradientForeColor = System.Drawing.Color.Black
Me.btnEmployeeHolidays.Image = Global.Aruti.Main.My.Resources.Resources.Employee_Holidays
Me.btnEmployeeHolidays.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
Me.btnEmployeeHolidays.Location = New System.Drawing.Point(3, 85)
Me.btnEmployeeHolidays.MinimumSize = New System.Drawing.Size(150, 35)
Me.btnEmployeeHolidays.Name = "btnEmployeeHolidays"
Me.btnEmployeeHolidays.PressedGradientBackColor = System.Drawing.Color.Transparent
Me.btnEmployeeHolidays.PressedGradientForeColor = System.Drawing.Color.Black
Me.btnEmployeeHolidays.Size = New System.Drawing.Size(166, 35)
Me.btnEmployeeHolidays.TabIndex = 9
Me.btnEmployeeHolidays.Text = "&Employee Holidays"
Me.btnEmployeeHolidays.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
Me.btnEmployeeHolidays.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText
Me.btnEmployeeHolidays.UseVisualStyleBackColor = false
'
'btnApproverLevel
'
Me.btnApproverLevel.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left)  _
            Or System.Windows.Forms.AnchorStyles.Right),System.Windows.Forms.AnchorStyles)
Me.btnApproverLevel.BackColor = System.Drawing.Color.Transparent
Me.btnApproverLevel.BackgroundImage = CType(resources.GetObject("btnApproverLevel.BackgroundImage"),System.Drawing.Image)
Me.btnApproverLevel.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
Me.btnApproverLevel.BorderColor = System.Drawing.Color.Empty
Me.btnApproverLevel.ButtonType = eZee.Common.eZeeLightButton.enButtonType.CheckButton
Me.btnApproverLevel.FlatAppearance.BorderSize = 0
Me.btnApproverLevel.FlatStyle = System.Windows.Forms.FlatStyle.Flat
Me.btnApproverLevel.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
Me.btnApproverLevel.ForeColor = System.Drawing.Color.Black
Me.btnApproverLevel.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
Me.btnApproverLevel.GradientForeColor = System.Drawing.Color.Black
Me.btnApproverLevel.HoverGradientBackColor = System.Drawing.Color.Transparent
Me.btnApproverLevel.HoverGradientForeColor = System.Drawing.Color.Black
Me.btnApproverLevel.Image = Global.Aruti.Main.My.Resources.Resources.Approver_Level
Me.btnApproverLevel.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
Me.btnApproverLevel.Location = New System.Drawing.Point(3, 126)
Me.btnApproverLevel.MinimumSize = New System.Drawing.Size(150, 35)
Me.btnApproverLevel.Name = "btnApproverLevel"
Me.btnApproverLevel.PressedGradientBackColor = System.Drawing.Color.Transparent
Me.btnApproverLevel.PressedGradientForeColor = System.Drawing.Color.Black
Me.btnApproverLevel.Size = New System.Drawing.Size(166, 35)
Me.btnApproverLevel.TabIndex = 10
Me.btnApproverLevel.Text = "&Approver Level"
Me.btnApproverLevel.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
Me.btnApproverLevel.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText
Me.btnApproverLevel.UseVisualStyleBackColor = false
'
'btnLeaveApprover
'
Me.btnLeaveApprover.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left)  _
            Or System.Windows.Forms.AnchorStyles.Right),System.Windows.Forms.AnchorStyles)
Me.btnLeaveApprover.BackColor = System.Drawing.Color.Transparent
Me.btnLeaveApprover.BackgroundImage = CType(resources.GetObject("btnLeaveApprover.BackgroundImage"),System.Drawing.Image)
Me.btnLeaveApprover.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
Me.btnLeaveApprover.BorderColor = System.Drawing.Color.Empty
Me.btnLeaveApprover.ButtonType = eZee.Common.eZeeLightButton.enButtonType.CheckButton
Me.btnLeaveApprover.FlatAppearance.BorderSize = 0
Me.btnLeaveApprover.FlatStyle = System.Windows.Forms.FlatStyle.Flat
Me.btnLeaveApprover.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
Me.btnLeaveApprover.ForeColor = System.Drawing.Color.Black
Me.btnLeaveApprover.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
Me.btnLeaveApprover.GradientForeColor = System.Drawing.Color.Black
Me.btnLeaveApprover.HoverGradientBackColor = System.Drawing.Color.Transparent
Me.btnLeaveApprover.HoverGradientForeColor = System.Drawing.Color.Black
Me.btnLeaveApprover.Image = Global.Aruti.Main.My.Resources.Resources.Leave_Approver
Me.btnLeaveApprover.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
Me.btnLeaveApprover.Location = New System.Drawing.Point(3, 167)
Me.btnLeaveApprover.MinimumSize = New System.Drawing.Size(150, 35)
Me.btnLeaveApprover.Name = "btnLeaveApprover"
Me.btnLeaveApprover.PressedGradientBackColor = System.Drawing.Color.Transparent
Me.btnLeaveApprover.PressedGradientForeColor = System.Drawing.Color.Black
Me.btnLeaveApprover.Size = New System.Drawing.Size(166, 35)
Me.btnLeaveApprover.TabIndex = 11
Me.btnLeaveApprover.Text = "&Leave Approver"
Me.btnLeaveApprover.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
Me.btnLeaveApprover.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText
Me.btnLeaveApprover.UseVisualStyleBackColor = false
'
'btnLeaveForm
'
Me.btnLeaveForm.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left)  _
            Or System.Windows.Forms.AnchorStyles.Right),System.Windows.Forms.AnchorStyles)
Me.btnLeaveForm.BackColor = System.Drawing.Color.Transparent
Me.btnLeaveForm.BackgroundImage = CType(resources.GetObject("btnLeaveForm.BackgroundImage"),System.Drawing.Image)
Me.btnLeaveForm.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
Me.btnLeaveForm.BorderColor = System.Drawing.Color.Empty
Me.btnLeaveForm.ButtonType = eZee.Common.eZeeLightButton.enButtonType.CheckButton
Me.btnLeaveForm.FlatAppearance.BorderSize = 0
Me.btnLeaveForm.FlatStyle = System.Windows.Forms.FlatStyle.Flat
Me.btnLeaveForm.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
Me.btnLeaveForm.ForeColor = System.Drawing.Color.Black
Me.btnLeaveForm.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
Me.btnLeaveForm.GradientForeColor = System.Drawing.Color.Black
Me.btnLeaveForm.HoverGradientBackColor = System.Drawing.Color.Transparent
Me.btnLeaveForm.HoverGradientForeColor = System.Drawing.Color.Black
Me.btnLeaveForm.Image = Global.Aruti.Main.My.Resources.Resources.Leaveform
Me.btnLeaveForm.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
Me.btnLeaveForm.Location = New System.Drawing.Point(3, 208)
Me.btnLeaveForm.MinimumSize = New System.Drawing.Size(150, 35)
Me.btnLeaveForm.Name = "btnLeaveForm"
Me.btnLeaveForm.PressedGradientBackColor = System.Drawing.Color.Transparent
Me.btnLeaveForm.PressedGradientForeColor = System.Drawing.Color.Black
Me.btnLeaveForm.Size = New System.Drawing.Size(166, 35)
Me.btnLeaveForm.TabIndex = 12
Me.btnLeaveForm.Text = "Leave &Form"
Me.btnLeaveForm.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
Me.btnLeaveForm.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText
Me.btnLeaveForm.UseVisualStyleBackColor = false
'
'btnLeaveProcess
'
Me.btnLeaveProcess.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left)  _
            Or System.Windows.Forms.AnchorStyles.Right),System.Windows.Forms.AnchorStyles)
Me.btnLeaveProcess.BackColor = System.Drawing.Color.Transparent
Me.btnLeaveProcess.BackgroundImage = CType(resources.GetObject("btnLeaveProcess.BackgroundImage"),System.Drawing.Image)
Me.btnLeaveProcess.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
Me.btnLeaveProcess.BorderColor = System.Drawing.Color.Empty
Me.btnLeaveProcess.ButtonType = eZee.Common.eZeeLightButton.enButtonType.CheckButton
Me.btnLeaveProcess.FlatAppearance.BorderSize = 0
Me.btnLeaveProcess.FlatStyle = System.Windows.Forms.FlatStyle.Flat
Me.btnLeaveProcess.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
Me.btnLeaveProcess.ForeColor = System.Drawing.Color.Black
Me.btnLeaveProcess.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
Me.btnLeaveProcess.GradientForeColor = System.Drawing.Color.Black
Me.btnLeaveProcess.HoverGradientBackColor = System.Drawing.Color.Transparent
Me.btnLeaveProcess.HoverGradientForeColor = System.Drawing.Color.Black
Me.btnLeaveProcess.Image = Global.Aruti.Main.My.Resources.Resources.Leaveprocess
Me.btnLeaveProcess.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
Me.btnLeaveProcess.Location = New System.Drawing.Point(3, 249)
Me.btnLeaveProcess.MinimumSize = New System.Drawing.Size(150, 35)
Me.btnLeaveProcess.Name = "btnLeaveProcess"
Me.btnLeaveProcess.PressedGradientBackColor = System.Drawing.Color.Transparent
Me.btnLeaveProcess.PressedGradientForeColor = System.Drawing.Color.Black
Me.btnLeaveProcess.Size = New System.Drawing.Size(166, 35)
Me.btnLeaveProcess.TabIndex = 13
Me.btnLeaveProcess.Text = "Leave &Process"
Me.btnLeaveProcess.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
Me.btnLeaveProcess.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText
Me.btnLeaveProcess.UseVisualStyleBackColor = false
'
'btnLeaveAccrue
'
Me.btnLeaveAccrue.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left)  _
            Or System.Windows.Forms.AnchorStyles.Right),System.Windows.Forms.AnchorStyles)
Me.btnLeaveAccrue.BackColor = System.Drawing.Color.Transparent
Me.btnLeaveAccrue.BackgroundImage = CType(resources.GetObject("btnLeaveAccrue.BackgroundImage"),System.Drawing.Image)
Me.btnLeaveAccrue.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
Me.btnLeaveAccrue.BorderColor = System.Drawing.Color.Empty
Me.btnLeaveAccrue.ButtonType = eZee.Common.eZeeLightButton.enButtonType.CheckButton
Me.btnLeaveAccrue.FlatAppearance.BorderSize = 0
Me.btnLeaveAccrue.FlatStyle = System.Windows.Forms.FlatStyle.Flat
Me.btnLeaveAccrue.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
Me.btnLeaveAccrue.ForeColor = System.Drawing.Color.Black
Me.btnLeaveAccrue.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
Me.btnLeaveAccrue.GradientForeColor = System.Drawing.Color.Black
Me.btnLeaveAccrue.HoverGradientBackColor = System.Drawing.Color.Transparent
Me.btnLeaveAccrue.HoverGradientForeColor = System.Drawing.Color.Black
Me.btnLeaveAccrue.Image = Global.Aruti.Main.My.Resources.Resources.LeaveAccrue
Me.btnLeaveAccrue.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
Me.btnLeaveAccrue.Location = New System.Drawing.Point(3, 290)
Me.btnLeaveAccrue.MinimumSize = New System.Drawing.Size(150, 35)
Me.btnLeaveAccrue.Name = "btnLeaveAccrue"
Me.btnLeaveAccrue.PressedGradientBackColor = System.Drawing.Color.Transparent
Me.btnLeaveAccrue.PressedGradientForeColor = System.Drawing.Color.Black
Me.btnLeaveAccrue.Size = New System.Drawing.Size(166, 35)
Me.btnLeaveAccrue.TabIndex = 14
Me.btnLeaveAccrue.Text = "Leave A&ccrue"
Me.btnLeaveAccrue.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
Me.btnLeaveAccrue.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText
Me.btnLeaveAccrue.UseVisualStyleBackColor = false
'
'btnLeaveViewer
'
Me.btnLeaveViewer.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left)  _
            Or System.Windows.Forms.AnchorStyles.Right),System.Windows.Forms.AnchorStyles)
Me.btnLeaveViewer.BackColor = System.Drawing.Color.Transparent
Me.btnLeaveViewer.BackgroundImage = CType(resources.GetObject("btnLeaveViewer.BackgroundImage"),System.Drawing.Image)
Me.btnLeaveViewer.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
Me.btnLeaveViewer.BorderColor = System.Drawing.Color.Empty
Me.btnLeaveViewer.ButtonType = eZee.Common.eZeeLightButton.enButtonType.CheckButton
Me.btnLeaveViewer.FlatAppearance.BorderSize = 0
Me.btnLeaveViewer.FlatStyle = System.Windows.Forms.FlatStyle.Flat
Me.btnLeaveViewer.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
Me.btnLeaveViewer.ForeColor = System.Drawing.Color.Black
Me.btnLeaveViewer.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
Me.btnLeaveViewer.GradientForeColor = System.Drawing.Color.Black
Me.btnLeaveViewer.HoverGradientBackColor = System.Drawing.Color.Transparent
Me.btnLeaveViewer.HoverGradientForeColor = System.Drawing.Color.Black
Me.btnLeaveViewer.Image = Global.Aruti.Main.My.Resources.Resources.LeaveViewver
Me.btnLeaveViewer.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
Me.btnLeaveViewer.Location = New System.Drawing.Point(3, 331)
Me.btnLeaveViewer.MinimumSize = New System.Drawing.Size(150, 35)
Me.btnLeaveViewer.Name = "btnLeaveViewer"
Me.btnLeaveViewer.PressedGradientBackColor = System.Drawing.Color.Transparent
Me.btnLeaveViewer.PressedGradientForeColor = System.Drawing.Color.Black
Me.btnLeaveViewer.Size = New System.Drawing.Size(166, 35)
Me.btnLeaveViewer.TabIndex = 15
Me.btnLeaveViewer.Text = "Leave &Viewer"
Me.btnLeaveViewer.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
Me.btnLeaveViewer.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText
Me.btnLeaveViewer.UseVisualStyleBackColor = false
'
'btnLeavePlanner
'
Me.btnLeavePlanner.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left)  _
            Or System.Windows.Forms.AnchorStyles.Right),System.Windows.Forms.AnchorStyles)
Me.btnLeavePlanner.BackColor = System.Drawing.Color.Transparent
Me.btnLeavePlanner.BackgroundImage = CType(resources.GetObject("btnLeavePlanner.BackgroundImage"),System.Drawing.Image)
Me.btnLeavePlanner.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
Me.btnLeavePlanner.BorderColor = System.Drawing.Color.Empty
Me.btnLeavePlanner.ButtonType = eZee.Common.eZeeLightButton.enButtonType.CheckButton
Me.btnLeavePlanner.FlatAppearance.BorderSize = 0
Me.btnLeavePlanner.FlatStyle = System.Windows.Forms.FlatStyle.Flat
Me.btnLeavePlanner.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
Me.btnLeavePlanner.ForeColor = System.Drawing.Color.Black
Me.btnLeavePlanner.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
Me.btnLeavePlanner.GradientForeColor = System.Drawing.Color.Black
Me.btnLeavePlanner.HoverGradientBackColor = System.Drawing.Color.Transparent
Me.btnLeavePlanner.HoverGradientForeColor = System.Drawing.Color.Black
Me.btnLeavePlanner.Image = Global.Aruti.Main.My.Resources.Resources.LeavePlanner
Me.btnLeavePlanner.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
Me.btnLeavePlanner.Location = New System.Drawing.Point(3, 372)
Me.btnLeavePlanner.MinimumSize = New System.Drawing.Size(150, 35)
Me.btnLeavePlanner.Name = "btnLeavePlanner"
Me.btnLeavePlanner.PressedGradientBackColor = System.Drawing.Color.Transparent
Me.btnLeavePlanner.PressedGradientForeColor = System.Drawing.Color.Black
Me.btnLeavePlanner.Size = New System.Drawing.Size(166, 35)
Me.btnLeavePlanner.TabIndex = 17
Me.btnLeavePlanner.Text = "Leave P&lanner"
Me.btnLeavePlanner.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
Me.btnLeavePlanner.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText
Me.btnLeavePlanner.UseVisualStyleBackColor = false
'
'btnLeaveSchedular
'
Me.btnLeaveSchedular.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left)  _
            Or System.Windows.Forms.AnchorStyles.Right),System.Windows.Forms.AnchorStyles)
Me.btnLeaveSchedular.BackColor = System.Drawing.Color.Transparent
Me.btnLeaveSchedular.BackgroundImage = CType(resources.GetObject("btnLeaveSchedular.BackgroundImage"),System.Drawing.Image)
Me.btnLeaveSchedular.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
Me.btnLeaveSchedular.BorderColor = System.Drawing.Color.Empty
Me.btnLeaveSchedular.ButtonType = eZee.Common.eZeeLightButton.enButtonType.CheckButton
Me.btnLeaveSchedular.FlatAppearance.BorderSize = 0
Me.btnLeaveSchedular.FlatStyle = System.Windows.Forms.FlatStyle.Flat
Me.btnLeaveSchedular.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
Me.btnLeaveSchedular.ForeColor = System.Drawing.Color.Black
Me.btnLeaveSchedular.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
Me.btnLeaveSchedular.GradientForeColor = System.Drawing.Color.Black
Me.btnLeaveSchedular.HoverGradientBackColor = System.Drawing.Color.Transparent
Me.btnLeaveSchedular.HoverGradientForeColor = System.Drawing.Color.Black
Me.btnLeaveSchedular.Image = Global.Aruti.Main.My.Resources.Resources.LeavePlannerViewver
Me.btnLeaveSchedular.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
Me.btnLeaveSchedular.Location = New System.Drawing.Point(3, 413)
Me.btnLeaveSchedular.MinimumSize = New System.Drawing.Size(150, 35)
Me.btnLeaveSchedular.Name = "btnLeaveSchedular"
Me.btnLeaveSchedular.PressedGradientBackColor = System.Drawing.Color.Transparent
Me.btnLeaveSchedular.PressedGradientForeColor = System.Drawing.Color.Black
Me.btnLeaveSchedular.Size = New System.Drawing.Size(166, 35)
Me.btnLeaveSchedular.TabIndex = 16
Me.btnLeaveSchedular.Text = "Planned Leave V&iewer"
Me.btnLeaveSchedular.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
Me.btnLeaveSchedular.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText
Me.btnLeaveSchedular.UseVisualStyleBackColor = false
'
'btnImportAccrueLeave
'
Me.btnImportAccrueLeave.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left)  _
            Or System.Windows.Forms.AnchorStyles.Right),System.Windows.Forms.AnchorStyles)
Me.btnImportAccrueLeave.BackColor = System.Drawing.Color.Transparent
Me.btnImportAccrueLeave.BackgroundImage = CType(resources.GetObject("btnImportAccrueLeave.BackgroundImage"),System.Drawing.Image)
Me.btnImportAccrueLeave.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
Me.btnImportAccrueLeave.BorderColor = System.Drawing.Color.Empty
Me.btnImportAccrueLeave.ButtonType = eZee.Common.eZeeLightButton.enButtonType.CheckButton
Me.btnImportAccrueLeave.FlatAppearance.BorderSize = 0
Me.btnImportAccrueLeave.FlatStyle = System.Windows.Forms.FlatStyle.Flat
Me.btnImportAccrueLeave.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
Me.btnImportAccrueLeave.ForeColor = System.Drawing.Color.Black
Me.btnImportAccrueLeave.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
Me.btnImportAccrueLeave.GradientForeColor = System.Drawing.Color.Black
Me.btnImportAccrueLeave.HoverGradientBackColor = System.Drawing.Color.Transparent
Me.btnImportAccrueLeave.HoverGradientForeColor = System.Drawing.Color.Black
Me.btnImportAccrueLeave.Image = Global.Aruti.Main.My.Resources.Resources.import_database
Me.btnImportAccrueLeave.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
Me.btnImportAccrueLeave.Location = New System.Drawing.Point(3, 454)
Me.btnImportAccrueLeave.MinimumSize = New System.Drawing.Size(150, 35)
Me.btnImportAccrueLeave.Name = "btnImportAccrueLeave"
Me.btnImportAccrueLeave.PressedGradientBackColor = System.Drawing.Color.Transparent
Me.btnImportAccrueLeave.PressedGradientForeColor = System.Drawing.Color.Black
Me.btnImportAccrueLeave.Size = New System.Drawing.Size(166, 35)
Me.btnImportAccrueLeave.TabIndex = 18
Me.btnImportAccrueLeave.Text = "Import Accure &Leave"
Me.btnImportAccrueLeave.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
Me.btnImportAccrueLeave.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText
Me.btnImportAccrueLeave.UseVisualStyleBackColor = false
'
'BtnTransferApprover
'
Me.BtnTransferApprover.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left)  _
            Or System.Windows.Forms.AnchorStyles.Right),System.Windows.Forms.AnchorStyles)
Me.BtnTransferApprover.BackColor = System.Drawing.Color.Transparent
Me.BtnTransferApprover.BackgroundImage = CType(resources.GetObject("BtnTransferApprover.BackgroundImage"),System.Drawing.Image)
Me.BtnTransferApprover.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
Me.BtnTransferApprover.BorderColor = System.Drawing.Color.Empty
Me.BtnTransferApprover.ButtonType = eZee.Common.eZeeLightButton.enButtonType.CheckButton
Me.BtnTransferApprover.FlatAppearance.BorderSize = 0
Me.BtnTransferApprover.FlatStyle = System.Windows.Forms.FlatStyle.Flat
Me.BtnTransferApprover.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
Me.BtnTransferApprover.ForeColor = System.Drawing.Color.Black
Me.BtnTransferApprover.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
Me.BtnTransferApprover.GradientForeColor = System.Drawing.Color.Black
Me.BtnTransferApprover.HoverGradientBackColor = System.Drawing.Color.Transparent
Me.BtnTransferApprover.HoverGradientForeColor = System.Drawing.Color.Black
Me.BtnTransferApprover.Image = Global.Aruti.Main.My.Resources.Resources.Transfer_Employee
Me.BtnTransferApprover.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
Me.BtnTransferApprover.Location = New System.Drawing.Point(3, 495)
Me.BtnTransferApprover.MinimumSize = New System.Drawing.Size(150, 35)
Me.BtnTransferApprover.Name = "BtnTransferApprover"
Me.BtnTransferApprover.PressedGradientBackColor = System.Drawing.Color.Transparent
Me.BtnTransferApprover.PressedGradientForeColor = System.Drawing.Color.Black
Me.BtnTransferApprover.Size = New System.Drawing.Size(166, 35)
Me.BtnTransferApprover.TabIndex = 19
Me.BtnTransferApprover.Text = "Approver Migration"
Me.BtnTransferApprover.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
Me.BtnTransferApprover.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText
Me.BtnTransferApprover.UseVisualStyleBackColor = false
'
'btnEndELC
'
Me.btnEndELC.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left)  _
            Or System.Windows.Forms.AnchorStyles.Right),System.Windows.Forms.AnchorStyles)
Me.btnEndELC.BackColor = System.Drawing.Color.Transparent
Me.btnEndELC.BackgroundImage = CType(resources.GetObject("btnEndELC.BackgroundImage"),System.Drawing.Image)
Me.btnEndELC.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
Me.btnEndELC.BorderColor = System.Drawing.Color.Empty
Me.btnEndELC.ButtonType = eZee.Common.eZeeLightButton.enButtonType.CheckButton
Me.btnEndELC.FlatAppearance.BorderSize = 0
Me.btnEndELC.FlatStyle = System.Windows.Forms.FlatStyle.Flat
Me.btnEndELC.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
Me.btnEndELC.ForeColor = System.Drawing.Color.Black
Me.btnEndELC.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
Me.btnEndELC.GradientForeColor = System.Drawing.Color.Black
Me.btnEndELC.HoverGradientBackColor = System.Drawing.Color.Transparent
Me.btnEndELC.HoverGradientForeColor = System.Drawing.Color.Black
Me.btnEndELC.Image = Global.Aruti.Main.My.Resources.Resources.EndofLeaveCycle
Me.btnEndELC.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
Me.btnEndELC.Location = New System.Drawing.Point(3, 536)
Me.btnEndELC.MinimumSize = New System.Drawing.Size(150, 35)
Me.btnEndELC.Name = "btnEndELC"
Me.btnEndELC.PressedGradientBackColor = System.Drawing.Color.Transparent
Me.btnEndELC.PressedGradientForeColor = System.Drawing.Color.Black
Me.btnEndELC.Size = New System.Drawing.Size(166, 35)
Me.btnEndELC.TabIndex = 20
Me.btnEndELC.Text = "&End Leave Cycle"
Me.btnEndELC.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
Me.btnEndELC.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText
Me.btnEndELC.UseVisualStyleBackColor = false
'
'btnLeaveFrequency
'
Me.btnLeaveFrequency.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left)  _
            Or System.Windows.Forms.AnchorStyles.Right),System.Windows.Forms.AnchorStyles)
Me.btnLeaveFrequency.BackColor = System.Drawing.Color.Transparent
Me.btnLeaveFrequency.BackgroundImage = CType(resources.GetObject("btnLeaveFrequency.BackgroundImage"),System.Drawing.Image)
Me.btnLeaveFrequency.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
Me.btnLeaveFrequency.BorderColor = System.Drawing.Color.Empty
Me.btnLeaveFrequency.ButtonType = eZee.Common.eZeeLightButton.enButtonType.CheckButton
Me.btnLeaveFrequency.FlatAppearance.BorderSize = 0
Me.btnLeaveFrequency.FlatStyle = System.Windows.Forms.FlatStyle.Flat
Me.btnLeaveFrequency.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
Me.btnLeaveFrequency.ForeColor = System.Drawing.Color.Black
Me.btnLeaveFrequency.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
Me.btnLeaveFrequency.GradientForeColor = System.Drawing.Color.Black
Me.btnLeaveFrequency.HoverGradientBackColor = System.Drawing.Color.Transparent
Me.btnLeaveFrequency.HoverGradientForeColor = System.Drawing.Color.Black
Me.btnLeaveFrequency.Image = Global.Aruti.Main.My.Resources.Resources.Leave_Frequency
Me.btnLeaveFrequency.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
Me.btnLeaveFrequency.Location = New System.Drawing.Point(3, 577)
Me.btnLeaveFrequency.MinimumSize = New System.Drawing.Size(150, 35)
Me.btnLeaveFrequency.Name = "btnLeaveFrequency"
Me.btnLeaveFrequency.PressedGradientBackColor = System.Drawing.Color.Transparent
Me.btnLeaveFrequency.PressedGradientForeColor = System.Drawing.Color.Black
Me.btnLeaveFrequency.Size = New System.Drawing.Size(166, 35)
Me.btnLeaveFrequency.TabIndex = 21
Me.btnLeaveFrequency.Text = "Leave &Frequency"
Me.btnLeaveFrequency.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
Me.btnLeaveFrequency.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText
Me.btnLeaveFrequency.UseVisualStyleBackColor = false
'
'btnLeaveAdjustment
'
Me.btnLeaveAdjustment.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left)  _
            Or System.Windows.Forms.AnchorStyles.Right),System.Windows.Forms.AnchorStyles)
Me.btnLeaveAdjustment.BackColor = System.Drawing.Color.Transparent
Me.btnLeaveAdjustment.BackgroundImage = CType(resources.GetObject("btnLeaveAdjustment.BackgroundImage"),System.Drawing.Image)
Me.btnLeaveAdjustment.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
Me.btnLeaveAdjustment.BorderColor = System.Drawing.Color.Empty
Me.btnLeaveAdjustment.ButtonType = eZee.Common.eZeeLightButton.enButtonType.CheckButton
Me.btnLeaveAdjustment.FlatAppearance.BorderSize = 0
Me.btnLeaveAdjustment.FlatStyle = System.Windows.Forms.FlatStyle.Flat
Me.btnLeaveAdjustment.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
Me.btnLeaveAdjustment.ForeColor = System.Drawing.Color.Black
Me.btnLeaveAdjustment.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
Me.btnLeaveAdjustment.GradientForeColor = System.Drawing.Color.Black
Me.btnLeaveAdjustment.HoverGradientBackColor = System.Drawing.Color.Transparent
Me.btnLeaveAdjustment.HoverGradientForeColor = System.Drawing.Color.Black
Me.btnLeaveAdjustment.Image = Global.Aruti.Main.My.Resources.Resources.Leave_Adjustment
Me.btnLeaveAdjustment.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
Me.btnLeaveAdjustment.Location = New System.Drawing.Point(3, 618)
Me.btnLeaveAdjustment.MinimumSize = New System.Drawing.Size(150, 35)
Me.btnLeaveAdjustment.Name = "btnLeaveAdjustment"
Me.btnLeaveAdjustment.PressedGradientBackColor = System.Drawing.Color.Transparent
Me.btnLeaveAdjustment.PressedGradientForeColor = System.Drawing.Color.Black
Me.btnLeaveAdjustment.Size = New System.Drawing.Size(166, 35)
Me.btnLeaveAdjustment.TabIndex = 22
Me.btnLeaveAdjustment.Text = "Leave Adjustment"
Me.btnLeaveAdjustment.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
Me.btnLeaveAdjustment.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText
Me.btnLeaveAdjustment.UseVisualStyleBackColor = false
'
'btnLVSwapApprover
'
Me.btnLVSwapApprover.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left)  _
            Or System.Windows.Forms.AnchorStyles.Right),System.Windows.Forms.AnchorStyles)
Me.btnLVSwapApprover.BackColor = System.Drawing.Color.Transparent
Me.btnLVSwapApprover.BackgroundImage = CType(resources.GetObject("btnLVSwapApprover.BackgroundImage"),System.Drawing.Image)
Me.btnLVSwapApprover.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
Me.btnLVSwapApprover.BorderColor = System.Drawing.Color.Empty
Me.btnLVSwapApprover.ButtonType = eZee.Common.eZeeLightButton.enButtonType.CheckButton
Me.btnLVSwapApprover.FlatAppearance.BorderSize = 0
Me.btnLVSwapApprover.FlatStyle = System.Windows.Forms.FlatStyle.Flat
Me.btnLVSwapApprover.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
Me.btnLVSwapApprover.ForeColor = System.Drawing.Color.Black
Me.btnLVSwapApprover.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
Me.btnLVSwapApprover.GradientForeColor = System.Drawing.Color.Black
Me.btnLVSwapApprover.HoverGradientBackColor = System.Drawing.Color.Transparent
Me.btnLVSwapApprover.HoverGradientForeColor = System.Drawing.Color.Black
Me.btnLVSwapApprover.Image = Global.Aruti.Main.My.Resources.Resources.Approver_Swapping
Me.btnLVSwapApprover.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
Me.btnLVSwapApprover.Location = New System.Drawing.Point(3, 659)
Me.btnLVSwapApprover.MinimumSize = New System.Drawing.Size(150, 35)
Me.btnLVSwapApprover.Name = "btnLVSwapApprover"
Me.btnLVSwapApprover.PressedGradientBackColor = System.Drawing.Color.Transparent
Me.btnLVSwapApprover.PressedGradientForeColor = System.Drawing.Color.Black
Me.btnLVSwapApprover.Size = New System.Drawing.Size(166, 35)
Me.btnLVSwapApprover.TabIndex = 23
Me.btnLVSwapApprover.Text = "Swap Approver"
Me.btnLVSwapApprover.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
Me.btnLVSwapApprover.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText
Me.btnLVSwapApprover.UseVisualStyleBackColor = false
'
'fpnlCommomMasters
'
Me.fpnlCommomMasters.AutoScroll = true
Me.fpnlCommomMasters.Controls.Add(Me.btnBranch)
Me.fpnlCommomMasters.Controls.Add(Me.btnDepartmentGroup)
Me.fpnlCommomMasters.Controls.Add(Me.btnDepartment)
Me.fpnlCommomMasters.Controls.Add(Me.btnSectionGroup)
Me.fpnlCommomMasters.Controls.Add(Me.btnSection)
Me.fpnlCommomMasters.Controls.Add(Me.btnUnitGroup)
Me.fpnlCommomMasters.Controls.Add(Me.btnUnit)
Me.fpnlCommomMasters.Controls.Add(Me.btnTeam)
Me.fpnlCommomMasters.Controls.Add(Me.btnJobGroup)
Me.fpnlCommomMasters.Controls.Add(Me.btnJob)
Me.fpnlCommomMasters.Controls.Add(Me.btnClassGroup)
Me.fpnlCommomMasters.Controls.Add(Me.btnClasses)
Me.fpnlCommomMasters.Controls.Add(Me.btnAllocationMapping)
Me.fpnlCommomMasters.Location = New System.Drawing.Point(3, 93)
Me.fpnlCommomMasters.Name = "fpnlCommomMasters"
Me.fpnlCommomMasters.Size = New System.Drawing.Size(190, 41)
Me.fpnlCommomMasters.TabIndex = 0
Me.fpnlCommomMasters.Visible = false
'
'btnBranch
'
Me.btnBranch.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left)  _
            Or System.Windows.Forms.AnchorStyles.Right),System.Windows.Forms.AnchorStyles)
Me.btnBranch.BackColor = System.Drawing.Color.Transparent
Me.btnBranch.BackgroundImage = CType(resources.GetObject("btnBranch.BackgroundImage"),System.Drawing.Image)
Me.btnBranch.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
Me.btnBranch.BorderColor = System.Drawing.Color.Empty
Me.btnBranch.ButtonType = eZee.Common.eZeeLightButton.enButtonType.CheckButton
Me.btnBranch.FlatAppearance.BorderSize = 0
Me.btnBranch.FlatStyle = System.Windows.Forms.FlatStyle.Flat
Me.btnBranch.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
Me.btnBranch.ForeColor = System.Drawing.Color.Black
Me.btnBranch.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
Me.btnBranch.GradientForeColor = System.Drawing.Color.Black
Me.btnBranch.HoverGradientBackColor = System.Drawing.Color.Transparent
Me.btnBranch.HoverGradientForeColor = System.Drawing.Color.Black
Me.btnBranch.Image = Global.Aruti.Main.My.Resources.Resources.Branch
Me.btnBranch.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
Me.btnBranch.Location = New System.Drawing.Point(3, 3)
Me.btnBranch.MinimumSize = New System.Drawing.Size(150, 35)
Me.btnBranch.Name = "btnBranch"
Me.btnBranch.PressedGradientBackColor = System.Drawing.Color.Transparent
Me.btnBranch.PressedGradientForeColor = System.Drawing.Color.Black
Me.btnBranch.Size = New System.Drawing.Size(166, 35)
Me.btnBranch.TabIndex = 4
Me.btnBranch.Tag = ""
Me.btnBranch.Text = "Branch"
Me.btnBranch.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
Me.btnBranch.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText
Me.btnBranch.UseVisualStyleBackColor = false
'
'btnDepartmentGroup
'
Me.btnDepartmentGroup.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left)  _
            Or System.Windows.Forms.AnchorStyles.Right),System.Windows.Forms.AnchorStyles)
Me.btnDepartmentGroup.BackColor = System.Drawing.Color.Transparent
Me.btnDepartmentGroup.BackgroundImage = CType(resources.GetObject("btnDepartmentGroup.BackgroundImage"),System.Drawing.Image)
Me.btnDepartmentGroup.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
Me.btnDepartmentGroup.BorderColor = System.Drawing.Color.Empty
Me.btnDepartmentGroup.ButtonType = eZee.Common.eZeeLightButton.enButtonType.CheckButton
Me.btnDepartmentGroup.FlatAppearance.BorderSize = 0
Me.btnDepartmentGroup.FlatStyle = System.Windows.Forms.FlatStyle.Flat
Me.btnDepartmentGroup.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
Me.btnDepartmentGroup.ForeColor = System.Drawing.Color.Black
Me.btnDepartmentGroup.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
Me.btnDepartmentGroup.GradientForeColor = System.Drawing.Color.Black
Me.btnDepartmentGroup.HoverGradientBackColor = System.Drawing.Color.Transparent
Me.btnDepartmentGroup.HoverGradientForeColor = System.Drawing.Color.Black
Me.btnDepartmentGroup.Image = Global.Aruti.Main.My.Resources.Resources.departmentgroup
Me.btnDepartmentGroup.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
Me.btnDepartmentGroup.Location = New System.Drawing.Point(3, 44)
Me.btnDepartmentGroup.MinimumSize = New System.Drawing.Size(150, 35)
Me.btnDepartmentGroup.Name = "btnDepartmentGroup"
Me.btnDepartmentGroup.PressedGradientBackColor = System.Drawing.Color.Transparent
Me.btnDepartmentGroup.PressedGradientForeColor = System.Drawing.Color.Black
Me.btnDepartmentGroup.Size = New System.Drawing.Size(166, 35)
Me.btnDepartmentGroup.TabIndex = 5
Me.btnDepartmentGroup.Text = "Department Group"
Me.btnDepartmentGroup.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
Me.btnDepartmentGroup.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText
Me.btnDepartmentGroup.UseVisualStyleBackColor = false
'
'btnDepartment
'
Me.btnDepartment.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left)  _
            Or System.Windows.Forms.AnchorStyles.Right),System.Windows.Forms.AnchorStyles)
Me.btnDepartment.BackColor = System.Drawing.Color.Transparent
Me.btnDepartment.BackgroundImage = CType(resources.GetObject("btnDepartment.BackgroundImage"),System.Drawing.Image)
Me.btnDepartment.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
Me.btnDepartment.BorderColor = System.Drawing.Color.Empty
Me.btnDepartment.ButtonType = eZee.Common.eZeeLightButton.enButtonType.CheckButton
Me.btnDepartment.FlatAppearance.BorderSize = 0
Me.btnDepartment.FlatStyle = System.Windows.Forms.FlatStyle.Flat
Me.btnDepartment.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
Me.btnDepartment.ForeColor = System.Drawing.Color.Black
Me.btnDepartment.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
Me.btnDepartment.GradientForeColor = System.Drawing.Color.Black
Me.btnDepartment.HoverGradientBackColor = System.Drawing.Color.Transparent
Me.btnDepartment.HoverGradientForeColor = System.Drawing.Color.Black
Me.btnDepartment.Image = Global.Aruti.Main.My.Resources.Resources.department
Me.btnDepartment.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
Me.btnDepartment.Location = New System.Drawing.Point(3, 85)
Me.btnDepartment.MinimumSize = New System.Drawing.Size(150, 35)
Me.btnDepartment.Name = "btnDepartment"
Me.btnDepartment.PressedGradientBackColor = System.Drawing.Color.Transparent
Me.btnDepartment.PressedGradientForeColor = System.Drawing.Color.Black
Me.btnDepartment.Size = New System.Drawing.Size(166, 35)
Me.btnDepartment.TabIndex = 6
Me.btnDepartment.Text = "Department"
Me.btnDepartment.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
Me.btnDepartment.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText
Me.btnDepartment.UseVisualStyleBackColor = false
'
'btnSectionGroup
'
Me.btnSectionGroup.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left)  _
            Or System.Windows.Forms.AnchorStyles.Right),System.Windows.Forms.AnchorStyles)
Me.btnSectionGroup.BackColor = System.Drawing.Color.Transparent
Me.btnSectionGroup.BackgroundImage = CType(resources.GetObject("btnSectionGroup.BackgroundImage"),System.Drawing.Image)
Me.btnSectionGroup.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
Me.btnSectionGroup.BorderColor = System.Drawing.Color.Empty
Me.btnSectionGroup.ButtonType = eZee.Common.eZeeLightButton.enButtonType.CheckButton
Me.btnSectionGroup.FlatAppearance.BorderSize = 0
Me.btnSectionGroup.FlatStyle = System.Windows.Forms.FlatStyle.Flat
Me.btnSectionGroup.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
Me.btnSectionGroup.ForeColor = System.Drawing.Color.Black
Me.btnSectionGroup.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
Me.btnSectionGroup.GradientForeColor = System.Drawing.Color.Black
Me.btnSectionGroup.HoverGradientBackColor = System.Drawing.Color.Transparent
Me.btnSectionGroup.HoverGradientForeColor = System.Drawing.Color.Black
Me.btnSectionGroup.Image = Global.Aruti.Main.My.Resources.Resources.sectiongroup
Me.btnSectionGroup.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
Me.btnSectionGroup.Location = New System.Drawing.Point(3, 126)
Me.btnSectionGroup.MinimumSize = New System.Drawing.Size(150, 35)
Me.btnSectionGroup.Name = "btnSectionGroup"
Me.btnSectionGroup.PressedGradientBackColor = System.Drawing.Color.Transparent
Me.btnSectionGroup.PressedGradientForeColor = System.Drawing.Color.Black
Me.btnSectionGroup.Size = New System.Drawing.Size(166, 35)
Me.btnSectionGroup.TabIndex = 16
Me.btnSectionGroup.Text = "Section Group"
Me.btnSectionGroup.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
Me.btnSectionGroup.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText
Me.btnSectionGroup.UseVisualStyleBackColor = false
'
'btnSection
'
Me.btnSection.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left)  _
            Or System.Windows.Forms.AnchorStyles.Right),System.Windows.Forms.AnchorStyles)
Me.btnSection.BackColor = System.Drawing.Color.Transparent
Me.btnSection.BackgroundImage = CType(resources.GetObject("btnSection.BackgroundImage"),System.Drawing.Image)
Me.btnSection.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
Me.btnSection.BorderColor = System.Drawing.Color.Empty
Me.btnSection.ButtonType = eZee.Common.eZeeLightButton.enButtonType.CheckButton
Me.btnSection.FlatAppearance.BorderSize = 0
Me.btnSection.FlatStyle = System.Windows.Forms.FlatStyle.Flat
Me.btnSection.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
Me.btnSection.ForeColor = System.Drawing.Color.Black
Me.btnSection.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
Me.btnSection.GradientForeColor = System.Drawing.Color.Black
Me.btnSection.HoverGradientBackColor = System.Drawing.Color.Transparent
Me.btnSection.HoverGradientForeColor = System.Drawing.Color.Black
Me.btnSection.Image = Global.Aruti.Main.My.Resources.Resources.section
Me.btnSection.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
Me.btnSection.Location = New System.Drawing.Point(3, 167)
Me.btnSection.MinimumSize = New System.Drawing.Size(150, 35)
Me.btnSection.Name = "btnSection"
Me.btnSection.PressedGradientBackColor = System.Drawing.Color.Transparent
Me.btnSection.PressedGradientForeColor = System.Drawing.Color.Black
Me.btnSection.Size = New System.Drawing.Size(166, 35)
Me.btnSection.TabIndex = 7
Me.btnSection.Text = "Section"
Me.btnSection.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
Me.btnSection.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText
Me.btnSection.UseVisualStyleBackColor = false
'
'btnUnitGroup
'
Me.btnUnitGroup.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left)  _
            Or System.Windows.Forms.AnchorStyles.Right),System.Windows.Forms.AnchorStyles)
Me.btnUnitGroup.BackColor = System.Drawing.Color.Transparent
Me.btnUnitGroup.BackgroundImage = CType(resources.GetObject("btnUnitGroup.BackgroundImage"),System.Drawing.Image)
Me.btnUnitGroup.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
Me.btnUnitGroup.BorderColor = System.Drawing.Color.Empty
Me.btnUnitGroup.ButtonType = eZee.Common.eZeeLightButton.enButtonType.CheckButton
Me.btnUnitGroup.FlatAppearance.BorderSize = 0
Me.btnUnitGroup.FlatStyle = System.Windows.Forms.FlatStyle.Flat
Me.btnUnitGroup.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
Me.btnUnitGroup.ForeColor = System.Drawing.Color.Black
Me.btnUnitGroup.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
Me.btnUnitGroup.GradientForeColor = System.Drawing.Color.Black
Me.btnUnitGroup.HoverGradientBackColor = System.Drawing.Color.Transparent
Me.btnUnitGroup.HoverGradientForeColor = System.Drawing.Color.Black
Me.btnUnitGroup.Image = Global.Aruti.Main.My.Resources.Resources.unitgroup
Me.btnUnitGroup.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
Me.btnUnitGroup.Location = New System.Drawing.Point(3, 208)
Me.btnUnitGroup.MinimumSize = New System.Drawing.Size(150, 35)
Me.btnUnitGroup.Name = "btnUnitGroup"
Me.btnUnitGroup.PressedGradientBackColor = System.Drawing.Color.Transparent
Me.btnUnitGroup.PressedGradientForeColor = System.Drawing.Color.Black
Me.btnUnitGroup.Size = New System.Drawing.Size(166, 35)
Me.btnUnitGroup.TabIndex = 17
Me.btnUnitGroup.Text = "Unit Group"
Me.btnUnitGroup.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
Me.btnUnitGroup.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText
Me.btnUnitGroup.UseVisualStyleBackColor = false
'
'btnUnit
'
Me.btnUnit.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left)  _
            Or System.Windows.Forms.AnchorStyles.Right),System.Windows.Forms.AnchorStyles)
Me.btnUnit.BackColor = System.Drawing.Color.Transparent
Me.btnUnit.BackgroundImage = CType(resources.GetObject("btnUnit.BackgroundImage"),System.Drawing.Image)
Me.btnUnit.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
Me.btnUnit.BorderColor = System.Drawing.Color.Empty
Me.btnUnit.ButtonType = eZee.Common.eZeeLightButton.enButtonType.CheckButton
Me.btnUnit.FlatAppearance.BorderSize = 0
Me.btnUnit.FlatStyle = System.Windows.Forms.FlatStyle.Flat
Me.btnUnit.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
Me.btnUnit.ForeColor = System.Drawing.Color.Black
Me.btnUnit.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
Me.btnUnit.GradientForeColor = System.Drawing.Color.Black
Me.btnUnit.HoverGradientBackColor = System.Drawing.Color.Transparent
Me.btnUnit.HoverGradientForeColor = System.Drawing.Color.Black
Me.btnUnit.Image = Global.Aruti.Main.My.Resources.Resources.unit
Me.btnUnit.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
Me.btnUnit.Location = New System.Drawing.Point(3, 249)
Me.btnUnit.MinimumSize = New System.Drawing.Size(150, 35)
Me.btnUnit.Name = "btnUnit"
Me.btnUnit.PressedGradientBackColor = System.Drawing.Color.Transparent
Me.btnUnit.PressedGradientForeColor = System.Drawing.Color.Black
Me.btnUnit.Size = New System.Drawing.Size(166, 35)
Me.btnUnit.TabIndex = 8
Me.btnUnit.Text = "Unit"
Me.btnUnit.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
Me.btnUnit.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText
Me.btnUnit.UseVisualStyleBackColor = false
'
'btnTeam
'
Me.btnTeam.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left)  _
            Or System.Windows.Forms.AnchorStyles.Right),System.Windows.Forms.AnchorStyles)
Me.btnTeam.BackColor = System.Drawing.Color.Transparent
Me.btnTeam.BackgroundImage = CType(resources.GetObject("btnTeam.BackgroundImage"),System.Drawing.Image)
Me.btnTeam.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
Me.btnTeam.BorderColor = System.Drawing.Color.Empty
Me.btnTeam.ButtonType = eZee.Common.eZeeLightButton.enButtonType.CheckButton
Me.btnTeam.FlatAppearance.BorderSize = 0
Me.btnTeam.FlatStyle = System.Windows.Forms.FlatStyle.Flat
Me.btnTeam.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
Me.btnTeam.ForeColor = System.Drawing.Color.Black
Me.btnTeam.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
Me.btnTeam.GradientForeColor = System.Drawing.Color.Black
Me.btnTeam.HoverGradientBackColor = System.Drawing.Color.Transparent
Me.btnTeam.HoverGradientForeColor = System.Drawing.Color.Black
Me.btnTeam.Image = Global.Aruti.Main.My.Resources.Resources.team
Me.btnTeam.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
Me.btnTeam.Location = New System.Drawing.Point(3, 290)
Me.btnTeam.MinimumSize = New System.Drawing.Size(150, 35)
Me.btnTeam.Name = "btnTeam"
Me.btnTeam.PressedGradientBackColor = System.Drawing.Color.Transparent
Me.btnTeam.PressedGradientForeColor = System.Drawing.Color.Black
Me.btnTeam.Size = New System.Drawing.Size(166, 35)
Me.btnTeam.TabIndex = 18
Me.btnTeam.Text = "Team"
Me.btnTeam.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
Me.btnTeam.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText
Me.btnTeam.UseVisualStyleBackColor = false
'
'btnJobGroup
'
Me.btnJobGroup.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left)  _
            Or System.Windows.Forms.AnchorStyles.Right),System.Windows.Forms.AnchorStyles)
Me.btnJobGroup.BackColor = System.Drawing.Color.Transparent
Me.btnJobGroup.BackgroundImage = CType(resources.GetObject("btnJobGroup.BackgroundImage"),System.Drawing.Image)
Me.btnJobGroup.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
Me.btnJobGroup.BorderColor = System.Drawing.Color.Empty
Me.btnJobGroup.ButtonType = eZee.Common.eZeeLightButton.enButtonType.CheckButton
Me.btnJobGroup.FlatAppearance.BorderSize = 0
Me.btnJobGroup.FlatStyle = System.Windows.Forms.FlatStyle.Flat
Me.btnJobGroup.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
Me.btnJobGroup.ForeColor = System.Drawing.Color.Black
Me.btnJobGroup.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
Me.btnJobGroup.GradientForeColor = System.Drawing.Color.Black
Me.btnJobGroup.HoverGradientBackColor = System.Drawing.Color.Transparent
Me.btnJobGroup.HoverGradientForeColor = System.Drawing.Color.Black
Me.btnJobGroup.Image = Global.Aruti.Main.My.Resources.Resources.Job_Group
Me.btnJobGroup.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
Me.btnJobGroup.Location = New System.Drawing.Point(3, 331)
Me.btnJobGroup.MinimumSize = New System.Drawing.Size(150, 35)
Me.btnJobGroup.Name = "btnJobGroup"
Me.btnJobGroup.PressedGradientBackColor = System.Drawing.Color.Transparent
Me.btnJobGroup.PressedGradientForeColor = System.Drawing.Color.Black
Me.btnJobGroup.Size = New System.Drawing.Size(166, 35)
Me.btnJobGroup.TabIndex = 9
Me.btnJobGroup.Text = "Job Group "
Me.btnJobGroup.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
Me.btnJobGroup.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText
Me.btnJobGroup.UseVisualStyleBackColor = false
'
'btnJob
'
Me.btnJob.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left)  _
            Or System.Windows.Forms.AnchorStyles.Right),System.Windows.Forms.AnchorStyles)
Me.btnJob.BackColor = System.Drawing.Color.Transparent
Me.btnJob.BackgroundImage = CType(resources.GetObject("btnJob.BackgroundImage"),System.Drawing.Image)
Me.btnJob.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
Me.btnJob.BorderColor = System.Drawing.Color.Empty
Me.btnJob.ButtonType = eZee.Common.eZeeLightButton.enButtonType.CheckButton
Me.btnJob.FlatAppearance.BorderSize = 0
Me.btnJob.FlatStyle = System.Windows.Forms.FlatStyle.Flat
Me.btnJob.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
Me.btnJob.ForeColor = System.Drawing.Color.Black
Me.btnJob.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
Me.btnJob.GradientForeColor = System.Drawing.Color.Black
Me.btnJob.HoverGradientBackColor = System.Drawing.Color.Transparent
Me.btnJob.HoverGradientForeColor = System.Drawing.Color.Black
Me.btnJob.Image = Global.Aruti.Main.My.Resources.Resources.Job
Me.btnJob.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
Me.btnJob.Location = New System.Drawing.Point(3, 372)
Me.btnJob.MinimumSize = New System.Drawing.Size(150, 35)
Me.btnJob.Name = "btnJob"
Me.btnJob.PressedGradientBackColor = System.Drawing.Color.Transparent
Me.btnJob.PressedGradientForeColor = System.Drawing.Color.Black
Me.btnJob.Size = New System.Drawing.Size(166, 35)
Me.btnJob.TabIndex = 10
Me.btnJob.Text = "Job"
Me.btnJob.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
Me.btnJob.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText
Me.btnJob.UseVisualStyleBackColor = false
'
'btnClassGroup
'
Me.btnClassGroup.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left)  _
            Or System.Windows.Forms.AnchorStyles.Right),System.Windows.Forms.AnchorStyles)
Me.btnClassGroup.BackColor = System.Drawing.Color.Transparent
Me.btnClassGroup.BackgroundImage = CType(resources.GetObject("btnClassGroup.BackgroundImage"),System.Drawing.Image)
Me.btnClassGroup.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
Me.btnClassGroup.BorderColor = System.Drawing.Color.Empty
Me.btnClassGroup.ButtonType = eZee.Common.eZeeLightButton.enButtonType.CheckButton
Me.btnClassGroup.FlatAppearance.BorderSize = 0
Me.btnClassGroup.FlatStyle = System.Windows.Forms.FlatStyle.Flat
Me.btnClassGroup.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
Me.btnClassGroup.ForeColor = System.Drawing.Color.Black
Me.btnClassGroup.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
Me.btnClassGroup.GradientForeColor = System.Drawing.Color.Black
Me.btnClassGroup.HoverGradientBackColor = System.Drawing.Color.Transparent
Me.btnClassGroup.HoverGradientForeColor = System.Drawing.Color.Black
Me.btnClassGroup.Image = Global.Aruti.Main.My.Resources.Resources.classgroup
Me.btnClassGroup.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
Me.btnClassGroup.Location = New System.Drawing.Point(3, 413)
Me.btnClassGroup.MinimumSize = New System.Drawing.Size(150, 35)
Me.btnClassGroup.Name = "btnClassGroup"
Me.btnClassGroup.PressedGradientBackColor = System.Drawing.Color.Transparent
Me.btnClassGroup.PressedGradientForeColor = System.Drawing.Color.Black
Me.btnClassGroup.Size = New System.Drawing.Size(166, 35)
Me.btnClassGroup.TabIndex = 11
Me.btnClassGroup.Text = "Class Group"
Me.btnClassGroup.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
Me.btnClassGroup.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText
Me.btnClassGroup.UseVisualStyleBackColor = false
'
'btnClasses
'
Me.btnClasses.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left)  _
            Or System.Windows.Forms.AnchorStyles.Right),System.Windows.Forms.AnchorStyles)
Me.btnClasses.BackColor = System.Drawing.Color.Transparent
Me.btnClasses.BackgroundImage = CType(resources.GetObject("btnClasses.BackgroundImage"),System.Drawing.Image)
Me.btnClasses.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
Me.btnClasses.BorderColor = System.Drawing.Color.Empty
Me.btnClasses.ButtonType = eZee.Common.eZeeLightButton.enButtonType.CheckButton
Me.btnClasses.FlatAppearance.BorderSize = 0
Me.btnClasses.FlatStyle = System.Windows.Forms.FlatStyle.Flat
Me.btnClasses.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
Me.btnClasses.ForeColor = System.Drawing.Color.Black
Me.btnClasses.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
Me.btnClasses.GradientForeColor = System.Drawing.Color.Black
Me.btnClasses.HoverGradientBackColor = System.Drawing.Color.Transparent
Me.btnClasses.HoverGradientForeColor = System.Drawing.Color.Black
Me.btnClasses.Image = Global.Aruti.Main.My.Resources.Resources.classes
Me.btnClasses.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
Me.btnClasses.Location = New System.Drawing.Point(3, 454)
Me.btnClasses.MinimumSize = New System.Drawing.Size(150, 35)
Me.btnClasses.Name = "btnClasses"
Me.btnClasses.PressedGradientBackColor = System.Drawing.Color.Transparent
Me.btnClasses.PressedGradientForeColor = System.Drawing.Color.Black
Me.btnClasses.Size = New System.Drawing.Size(166, 35)
Me.btnClasses.TabIndex = 12
Me.btnClasses.Text = "Classes"
Me.btnClasses.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
Me.btnClasses.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText
Me.btnClasses.UseVisualStyleBackColor = false
'
'btnAllocationMapping
'
Me.btnAllocationMapping.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left)  _
            Or System.Windows.Forms.AnchorStyles.Right),System.Windows.Forms.AnchorStyles)
Me.btnAllocationMapping.BackColor = System.Drawing.Color.Transparent
Me.btnAllocationMapping.BackgroundImage = CType(resources.GetObject("btnAllocationMapping.BackgroundImage"),System.Drawing.Image)
Me.btnAllocationMapping.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
Me.btnAllocationMapping.BorderColor = System.Drawing.Color.Empty
Me.btnAllocationMapping.ButtonType = eZee.Common.eZeeLightButton.enButtonType.CheckButton
Me.btnAllocationMapping.FlatAppearance.BorderSize = 0
Me.btnAllocationMapping.FlatStyle = System.Windows.Forms.FlatStyle.Flat
Me.btnAllocationMapping.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
Me.btnAllocationMapping.ForeColor = System.Drawing.Color.Black
Me.btnAllocationMapping.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
Me.btnAllocationMapping.GradientForeColor = System.Drawing.Color.Black
Me.btnAllocationMapping.HoverGradientBackColor = System.Drawing.Color.Transparent
Me.btnAllocationMapping.HoverGradientForeColor = System.Drawing.Color.Black
Me.btnAllocationMapping.Image = Global.Aruti.Main.My.Resources.Resources.AllocationMapping
Me.btnAllocationMapping.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
Me.btnAllocationMapping.Location = New System.Drawing.Point(3, 495)
Me.btnAllocationMapping.MinimumSize = New System.Drawing.Size(150, 35)
Me.btnAllocationMapping.Name = "btnAllocationMapping"
Me.btnAllocationMapping.PressedGradientBackColor = System.Drawing.Color.Transparent
Me.btnAllocationMapping.PressedGradientForeColor = System.Drawing.Color.Black
Me.btnAllocationMapping.Size = New System.Drawing.Size(166, 35)
Me.btnAllocationMapping.TabIndex = 19
Me.btnAllocationMapping.Tag = ""
Me.btnAllocationMapping.Text = "Allocation Mapping"
Me.btnAllocationMapping.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
Me.btnAllocationMapping.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText
Me.btnAllocationMapping.UseVisualStyleBackColor = false
'
'fpnlTnAInfo
'
Me.fpnlTnAInfo.AutoScroll = true
Me.fpnlTnAInfo.Controls.Add(Me.btnDeviceUserMapping)
Me.fpnlTnAInfo.Controls.Add(Me.btnLogin)
Me.fpnlTnAInfo.Controls.Add(Me.btnTimesheet)
Me.fpnlTnAInfo.Controls.Add(Me.btnHoldEmployee)
Me.fpnlTnAInfo.Controls.Add(Me.btnEmployeeAbsent)
Me.fpnlTnAInfo.Controls.Add(Me.btnImportData)
Me.fpnlTnAInfo.Controls.Add(Me.btnExportData)
Me.fpnlTnAInfo.Controls.Add(Me.btnImportAttendancedata)
Me.fpnlTnAInfo.Controls.Add(Me.btnGroupAttendance)
Me.fpnlTnAInfo.Controls.Add(Me.btnEditTimeSheet)
Me.fpnlTnAInfo.Controls.Add(Me.btnRecalculateTiming)
Me.fpnlTnAInfo.Controls.Add(Me.btnRoundOff)
Me.fpnlTnAInfo.Controls.Add(Me.btnGlobalEditDelete)
Me.fpnlTnAInfo.Location = New System.Drawing.Point(3, 140)
Me.fpnlTnAInfo.Name = "fpnlTnAInfo"
Me.fpnlTnAInfo.Size = New System.Drawing.Size(191, 40)
Me.fpnlTnAInfo.TabIndex = 10
Me.fpnlTnAInfo.Visible = false
'
'btnDeviceUserMapping
'
Me.btnDeviceUserMapping.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left)  _
            Or System.Windows.Forms.AnchorStyles.Right),System.Windows.Forms.AnchorStyles)
Me.btnDeviceUserMapping.BackColor = System.Drawing.Color.Transparent
Me.btnDeviceUserMapping.BackgroundImage = CType(resources.GetObject("btnDeviceUserMapping.BackgroundImage"),System.Drawing.Image)
Me.btnDeviceUserMapping.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
Me.btnDeviceUserMapping.BorderColor = System.Drawing.Color.Empty
Me.btnDeviceUserMapping.ButtonType = eZee.Common.eZeeLightButton.enButtonType.CheckButton
Me.btnDeviceUserMapping.FlatAppearance.BorderSize = 0
Me.btnDeviceUserMapping.FlatStyle = System.Windows.Forms.FlatStyle.Flat
Me.btnDeviceUserMapping.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
Me.btnDeviceUserMapping.ForeColor = System.Drawing.Color.Black
Me.btnDeviceUserMapping.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
Me.btnDeviceUserMapping.GradientForeColor = System.Drawing.Color.Black
Me.btnDeviceUserMapping.HoverGradientBackColor = System.Drawing.Color.Transparent
Me.btnDeviceUserMapping.HoverGradientForeColor = System.Drawing.Color.Black
Me.btnDeviceUserMapping.Image = Global.Aruti.Main.My.Resources.Resources.Device_user_mapping
Me.btnDeviceUserMapping.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
Me.btnDeviceUserMapping.Location = New System.Drawing.Point(3, 3)
Me.btnDeviceUserMapping.MinimumSize = New System.Drawing.Size(150, 35)
Me.btnDeviceUserMapping.Name = "btnDeviceUserMapping"
Me.btnDeviceUserMapping.PressedGradientBackColor = System.Drawing.Color.Transparent
Me.btnDeviceUserMapping.PressedGradientForeColor = System.Drawing.Color.Black
Me.btnDeviceUserMapping.Size = New System.Drawing.Size(166, 35)
Me.btnDeviceUserMapping.TabIndex = 21
Me.btnDeviceUserMapping.Text = "&Device User Mapping"
Me.btnDeviceUserMapping.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
Me.btnDeviceUserMapping.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText
Me.btnDeviceUserMapping.UseVisualStyleBackColor = false
'
'btnLogin
'
Me.btnLogin.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left)  _
            Or System.Windows.Forms.AnchorStyles.Right),System.Windows.Forms.AnchorStyles)
Me.btnLogin.BackColor = System.Drawing.Color.Transparent
Me.btnLogin.BackgroundImage = CType(resources.GetObject("btnLogin.BackgroundImage"),System.Drawing.Image)
Me.btnLogin.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
Me.btnLogin.BorderColor = System.Drawing.Color.Empty
Me.btnLogin.ButtonType = eZee.Common.eZeeLightButton.enButtonType.CheckButton
Me.btnLogin.FlatAppearance.BorderSize = 0
Me.btnLogin.FlatStyle = System.Windows.Forms.FlatStyle.Flat
Me.btnLogin.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
Me.btnLogin.ForeColor = System.Drawing.Color.Black
Me.btnLogin.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
Me.btnLogin.GradientForeColor = System.Drawing.Color.Black
Me.btnLogin.HoverGradientBackColor = System.Drawing.Color.Transparent
Me.btnLogin.HoverGradientForeColor = System.Drawing.Color.Black
Me.btnLogin.Image = Global.Aruti.Main.My.Resources.Resources.Login
Me.btnLogin.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
Me.btnLogin.Location = New System.Drawing.Point(3, 44)
Me.btnLogin.MinimumSize = New System.Drawing.Size(150, 35)
Me.btnLogin.Name = "btnLogin"
Me.btnLogin.PressedGradientBackColor = System.Drawing.Color.Transparent
Me.btnLogin.PressedGradientForeColor = System.Drawing.Color.Black
Me.btnLogin.Size = New System.Drawing.Size(166, 35)
Me.btnLogin.TabIndex = 9
Me.btnLogin.Text = "&Login"
Me.btnLogin.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
Me.btnLogin.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText
Me.btnLogin.UseVisualStyleBackColor = false
Me.btnLogin.Visible = false
'
'btnTimesheet
'
Me.btnTimesheet.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left)  _
            Or System.Windows.Forms.AnchorStyles.Right),System.Windows.Forms.AnchorStyles)
Me.btnTimesheet.BackColor = System.Drawing.Color.Transparent
Me.btnTimesheet.BackgroundImage = CType(resources.GetObject("btnTimesheet.BackgroundImage"),System.Drawing.Image)
Me.btnTimesheet.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
Me.btnTimesheet.BorderColor = System.Drawing.Color.Empty
Me.btnTimesheet.ButtonType = eZee.Common.eZeeLightButton.enButtonType.CheckButton
Me.btnTimesheet.FlatAppearance.BorderSize = 0
Me.btnTimesheet.FlatStyle = System.Windows.Forms.FlatStyle.Flat
Me.btnTimesheet.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
Me.btnTimesheet.ForeColor = System.Drawing.Color.Black
Me.btnTimesheet.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
Me.btnTimesheet.GradientForeColor = System.Drawing.Color.Black
Me.btnTimesheet.HoverGradientBackColor = System.Drawing.Color.Transparent
Me.btnTimesheet.HoverGradientForeColor = System.Drawing.Color.Black
Me.btnTimesheet.Image = Global.Aruti.Main.My.Resources.Resources.Time_Sheet
Me.btnTimesheet.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
Me.btnTimesheet.Location = New System.Drawing.Point(3, 85)
Me.btnTimesheet.MinimumSize = New System.Drawing.Size(150, 35)
Me.btnTimesheet.Name = "btnTimesheet"
Me.btnTimesheet.PressedGradientBackColor = System.Drawing.Color.Transparent
Me.btnTimesheet.PressedGradientForeColor = System.Drawing.Color.Black
Me.btnTimesheet.Size = New System.Drawing.Size(166, 35)
Me.btnTimesheet.TabIndex = 10
Me.btnTimesheet.Text = "&Timesheet"
Me.btnTimesheet.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
Me.btnTimesheet.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText
Me.btnTimesheet.UseVisualStyleBackColor = false
'
'btnHoldEmployee
'
Me.btnHoldEmployee.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left)  _
            Or System.Windows.Forms.AnchorStyles.Right),System.Windows.Forms.AnchorStyles)
Me.btnHoldEmployee.BackColor = System.Drawing.Color.Transparent
Me.btnHoldEmployee.BackgroundImage = CType(resources.GetObject("btnHoldEmployee.BackgroundImage"),System.Drawing.Image)
Me.btnHoldEmployee.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
Me.btnHoldEmployee.BorderColor = System.Drawing.Color.Empty
Me.btnHoldEmployee.ButtonType = eZee.Common.eZeeLightButton.enButtonType.CheckButton
Me.btnHoldEmployee.FlatAppearance.BorderSize = 0
Me.btnHoldEmployee.FlatStyle = System.Windows.Forms.FlatStyle.Flat
Me.btnHoldEmployee.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
Me.btnHoldEmployee.ForeColor = System.Drawing.Color.Black
Me.btnHoldEmployee.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
Me.btnHoldEmployee.GradientForeColor = System.Drawing.Color.Black
Me.btnHoldEmployee.HoverGradientBackColor = System.Drawing.Color.Transparent
Me.btnHoldEmployee.HoverGradientForeColor = System.Drawing.Color.Black
Me.btnHoldEmployee.Image = Global.Aruti.Main.My.Resources.Resources.hold_employee
Me.btnHoldEmployee.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
Me.btnHoldEmployee.Location = New System.Drawing.Point(3, 126)
Me.btnHoldEmployee.MinimumSize = New System.Drawing.Size(150, 35)
Me.btnHoldEmployee.Name = "btnHoldEmployee"
Me.btnHoldEmployee.PressedGradientBackColor = System.Drawing.Color.Transparent
Me.btnHoldEmployee.PressedGradientForeColor = System.Drawing.Color.Black
Me.btnHoldEmployee.Size = New System.Drawing.Size(166, 35)
Me.btnHoldEmployee.TabIndex = 11
Me.btnHoldEmployee.Text = "&Hold Employee"
Me.btnHoldEmployee.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
Me.btnHoldEmployee.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText
Me.btnHoldEmployee.UseVisualStyleBackColor = false
'
'btnEmployeeAbsent
'
Me.btnEmployeeAbsent.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left)  _
            Or System.Windows.Forms.AnchorStyles.Right),System.Windows.Forms.AnchorStyles)
Me.btnEmployeeAbsent.BackColor = System.Drawing.Color.Transparent
Me.btnEmployeeAbsent.BackgroundImage = CType(resources.GetObject("btnEmployeeAbsent.BackgroundImage"),System.Drawing.Image)
Me.btnEmployeeAbsent.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
Me.btnEmployeeAbsent.BorderColor = System.Drawing.Color.Empty
Me.btnEmployeeAbsent.ButtonType = eZee.Common.eZeeLightButton.enButtonType.CheckButton
Me.btnEmployeeAbsent.FlatAppearance.BorderSize = 0
Me.btnEmployeeAbsent.FlatStyle = System.Windows.Forms.FlatStyle.Flat
Me.btnEmployeeAbsent.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
Me.btnEmployeeAbsent.ForeColor = System.Drawing.Color.Black
Me.btnEmployeeAbsent.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
Me.btnEmployeeAbsent.GradientForeColor = System.Drawing.Color.Black
Me.btnEmployeeAbsent.HoverGradientBackColor = System.Drawing.Color.Transparent
Me.btnEmployeeAbsent.HoverGradientForeColor = System.Drawing.Color.Black
Me.btnEmployeeAbsent.Image = Global.Aruti.Main.My.Resources.Resources.employee_absent
Me.btnEmployeeAbsent.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
Me.btnEmployeeAbsent.Location = New System.Drawing.Point(3, 167)
Me.btnEmployeeAbsent.MinimumSize = New System.Drawing.Size(150, 35)
Me.btnEmployeeAbsent.Name = "btnEmployeeAbsent"
Me.btnEmployeeAbsent.PressedGradientBackColor = System.Drawing.Color.Transparent
Me.btnEmployeeAbsent.PressedGradientForeColor = System.Drawing.Color.Black
Me.btnEmployeeAbsent.Size = New System.Drawing.Size(166, 35)
Me.btnEmployeeAbsent.TabIndex = 13
Me.btnEmployeeAbsent.Text = "Employee &Absent"
Me.btnEmployeeAbsent.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
Me.btnEmployeeAbsent.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText
Me.btnEmployeeAbsent.UseVisualStyleBackColor = false
'
'btnImportData
'
Me.btnImportData.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left)  _
            Or System.Windows.Forms.AnchorStyles.Right),System.Windows.Forms.AnchorStyles)
Me.btnImportData.BackColor = System.Drawing.Color.Transparent
Me.btnImportData.BackgroundImage = CType(resources.GetObject("btnImportData.BackgroundImage"),System.Drawing.Image)
Me.btnImportData.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
Me.btnImportData.BorderColor = System.Drawing.Color.Empty
Me.btnImportData.ButtonType = eZee.Common.eZeeLightButton.enButtonType.CheckButton
Me.btnImportData.FlatAppearance.BorderSize = 0
Me.btnImportData.FlatStyle = System.Windows.Forms.FlatStyle.Flat
Me.btnImportData.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
Me.btnImportData.ForeColor = System.Drawing.Color.Black
Me.btnImportData.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
Me.btnImportData.GradientForeColor = System.Drawing.Color.Black
Me.btnImportData.HoverGradientBackColor = System.Drawing.Color.Transparent
Me.btnImportData.HoverGradientForeColor = System.Drawing.Color.Black
Me.btnImportData.Image = Global.Aruti.Main.My.Resources.Resources.import_database
Me.btnImportData.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
Me.btnImportData.Location = New System.Drawing.Point(3, 208)
Me.btnImportData.MinimumSize = New System.Drawing.Size(150, 35)
Me.btnImportData.Name = "btnImportData"
Me.btnImportData.PressedGradientBackColor = System.Drawing.Color.Transparent
Me.btnImportData.PressedGradientForeColor = System.Drawing.Color.Black
Me.btnImportData.Size = New System.Drawing.Size(166, 35)
Me.btnImportData.TabIndex = 14
Me.btnImportData.Text = "&Import Data"
Me.btnImportData.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
Me.btnImportData.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText
Me.btnImportData.UseVisualStyleBackColor = false
'
'btnExportData
'
Me.btnExportData.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left)  _
            Or System.Windows.Forms.AnchorStyles.Right),System.Windows.Forms.AnchorStyles)
Me.btnExportData.BackColor = System.Drawing.Color.Transparent
Me.btnExportData.BackgroundImage = CType(resources.GetObject("btnExportData.BackgroundImage"),System.Drawing.Image)
Me.btnExportData.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
Me.btnExportData.BorderColor = System.Drawing.Color.Empty
Me.btnExportData.ButtonType = eZee.Common.eZeeLightButton.enButtonType.CheckButton
Me.btnExportData.FlatAppearance.BorderSize = 0
Me.btnExportData.FlatStyle = System.Windows.Forms.FlatStyle.Flat
Me.btnExportData.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
Me.btnExportData.ForeColor = System.Drawing.Color.Black
Me.btnExportData.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
Me.btnExportData.GradientForeColor = System.Drawing.Color.Black
Me.btnExportData.HoverGradientBackColor = System.Drawing.Color.Transparent
Me.btnExportData.HoverGradientForeColor = System.Drawing.Color.Black
Me.btnExportData.Image = Global.Aruti.Main.My.Resources.Resources.export_database
Me.btnExportData.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
Me.btnExportData.Location = New System.Drawing.Point(3, 249)
Me.btnExportData.MinimumSize = New System.Drawing.Size(150, 35)
Me.btnExportData.Name = "btnExportData"
Me.btnExportData.PressedGradientBackColor = System.Drawing.Color.Transparent
Me.btnExportData.PressedGradientForeColor = System.Drawing.Color.Black
Me.btnExportData.Size = New System.Drawing.Size(166, 35)
Me.btnExportData.TabIndex = 15
Me.btnExportData.Text = "&Export Data"
Me.btnExportData.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
Me.btnExportData.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText
Me.btnExportData.UseVisualStyleBackColor = false
'
'btnImportAttendancedata
'
Me.btnImportAttendancedata.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left)  _
            Or System.Windows.Forms.AnchorStyles.Right),System.Windows.Forms.AnchorStyles)
Me.btnImportAttendancedata.BackColor = System.Drawing.Color.Transparent
Me.btnImportAttendancedata.BackgroundImage = CType(resources.GetObject("btnImportAttendancedata.BackgroundImage"),System.Drawing.Image)
Me.btnImportAttendancedata.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
Me.btnImportAttendancedata.BorderColor = System.Drawing.Color.Empty
Me.btnImportAttendancedata.ButtonType = eZee.Common.eZeeLightButton.enButtonType.CheckButton
Me.btnImportAttendancedata.FlatAppearance.BorderSize = 0
Me.btnImportAttendancedata.FlatStyle = System.Windows.Forms.FlatStyle.Flat
Me.btnImportAttendancedata.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
Me.btnImportAttendancedata.ForeColor = System.Drawing.Color.Black
Me.btnImportAttendancedata.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
Me.btnImportAttendancedata.GradientForeColor = System.Drawing.Color.Black
Me.btnImportAttendancedata.HoverGradientBackColor = System.Drawing.Color.Transparent
Me.btnImportAttendancedata.HoverGradientForeColor = System.Drawing.Color.Black
Me.btnImportAttendancedata.Image = Global.Aruti.Main.My.Resources.Resources.Import_Attendence
Me.btnImportAttendancedata.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
Me.btnImportAttendancedata.Location = New System.Drawing.Point(3, 290)
Me.btnImportAttendancedata.MinimumSize = New System.Drawing.Size(150, 35)
Me.btnImportAttendancedata.Name = "btnImportAttendancedata"
Me.btnImportAttendancedata.PressedGradientBackColor = System.Drawing.Color.Transparent
Me.btnImportAttendancedata.PressedGradientForeColor = System.Drawing.Color.Black
Me.btnImportAttendancedata.Size = New System.Drawing.Size(166, 35)
Me.btnImportAttendancedata.TabIndex = 16
Me.btnImportAttendancedata.Text = "&Import Device Attendance"
Me.btnImportAttendancedata.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
Me.btnImportAttendancedata.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText
Me.btnImportAttendancedata.UseVisualStyleBackColor = false
'
'btnGroupAttendance
'
Me.btnGroupAttendance.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left)  _
            Or System.Windows.Forms.AnchorStyles.Right),System.Windows.Forms.AnchorStyles)
Me.btnGroupAttendance.BackColor = System.Drawing.Color.Transparent
Me.btnGroupAttendance.BackgroundImage = CType(resources.GetObject("btnGroupAttendance.BackgroundImage"),System.Drawing.Image)
Me.btnGroupAttendance.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
Me.btnGroupAttendance.BorderColor = System.Drawing.Color.Empty
Me.btnGroupAttendance.ButtonType = eZee.Common.eZeeLightButton.enButtonType.CheckButton
Me.btnGroupAttendance.FlatAppearance.BorderSize = 0
Me.btnGroupAttendance.FlatStyle = System.Windows.Forms.FlatStyle.Flat
Me.btnGroupAttendance.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
Me.btnGroupAttendance.ForeColor = System.Drawing.Color.Black
Me.btnGroupAttendance.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
Me.btnGroupAttendance.GradientForeColor = System.Drawing.Color.Black
Me.btnGroupAttendance.HoverGradientBackColor = System.Drawing.Color.Transparent
Me.btnGroupAttendance.HoverGradientForeColor = System.Drawing.Color.Black
Me.btnGroupAttendance.Image = Global.Aruti.Main.My.Resources.Resources.Group_Attendence
Me.btnGroupAttendance.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
Me.btnGroupAttendance.Location = New System.Drawing.Point(3, 331)
Me.btnGroupAttendance.MinimumSize = New System.Drawing.Size(150, 35)
Me.btnGroupAttendance.Name = "btnGroupAttendance"
Me.btnGroupAttendance.PressedGradientBackColor = System.Drawing.Color.Transparent
Me.btnGroupAttendance.PressedGradientForeColor = System.Drawing.Color.Black
Me.btnGroupAttendance.Size = New System.Drawing.Size(166, 35)
Me.btnGroupAttendance.TabIndex = 17
Me.btnGroupAttendance.Text = "&Group Attendance"
Me.btnGroupAttendance.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
Me.btnGroupAttendance.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText
Me.btnGroupAttendance.UseVisualStyleBackColor = false
'
'btnEditTimeSheet
'
Me.btnEditTimeSheet.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left)  _
            Or System.Windows.Forms.AnchorStyles.Right),System.Windows.Forms.AnchorStyles)
Me.btnEditTimeSheet.BackColor = System.Drawing.Color.Transparent
Me.btnEditTimeSheet.BackgroundImage = CType(resources.GetObject("btnEditTimeSheet.BackgroundImage"),System.Drawing.Image)
Me.btnEditTimeSheet.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
Me.btnEditTimeSheet.BorderColor = System.Drawing.Color.Empty
Me.btnEditTimeSheet.ButtonType = eZee.Common.eZeeLightButton.enButtonType.CheckButton
Me.btnEditTimeSheet.FlatAppearance.BorderSize = 0
Me.btnEditTimeSheet.FlatStyle = System.Windows.Forms.FlatStyle.Flat
Me.btnEditTimeSheet.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
Me.btnEditTimeSheet.ForeColor = System.Drawing.Color.Black
Me.btnEditTimeSheet.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
Me.btnEditTimeSheet.GradientForeColor = System.Drawing.Color.Black
Me.btnEditTimeSheet.HoverGradientBackColor = System.Drawing.Color.Transparent
Me.btnEditTimeSheet.HoverGradientForeColor = System.Drawing.Color.Black
Me.btnEditTimeSheet.Image = Global.Aruti.Main.My.Resources.Resources.Edit_Timesheet_Card
Me.btnEditTimeSheet.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
Me.btnEditTimeSheet.Location = New System.Drawing.Point(3, 372)
Me.btnEditTimeSheet.MinimumSize = New System.Drawing.Size(150, 35)
Me.btnEditTimeSheet.Name = "btnEditTimeSheet"
Me.btnEditTimeSheet.PressedGradientBackColor = System.Drawing.Color.Transparent
Me.btnEditTimeSheet.PressedGradientForeColor = System.Drawing.Color.Black
Me.btnEditTimeSheet.Size = New System.Drawing.Size(166, 35)
Me.btnEditTimeSheet.TabIndex = 19
Me.btnEditTimeSheet.Text = "Edit Timesheet Card"
Me.btnEditTimeSheet.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
Me.btnEditTimeSheet.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText
Me.btnEditTimeSheet.UseVisualStyleBackColor = false
'
'btnRecalculateTiming
'
Me.btnRecalculateTiming.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left)  _
            Or System.Windows.Forms.AnchorStyles.Right),System.Windows.Forms.AnchorStyles)
Me.btnRecalculateTiming.BackColor = System.Drawing.Color.Transparent
Me.btnRecalculateTiming.BackgroundImage = CType(resources.GetObject("btnRecalculateTiming.BackgroundImage"),System.Drawing.Image)
Me.btnRecalculateTiming.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
Me.btnRecalculateTiming.BorderColor = System.Drawing.Color.Empty
Me.btnRecalculateTiming.ButtonType = eZee.Common.eZeeLightButton.enButtonType.CheckButton
Me.btnRecalculateTiming.FlatAppearance.BorderSize = 0
Me.btnRecalculateTiming.FlatStyle = System.Windows.Forms.FlatStyle.Flat
Me.btnRecalculateTiming.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
Me.btnRecalculateTiming.ForeColor = System.Drawing.Color.Black
Me.btnRecalculateTiming.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
Me.btnRecalculateTiming.GradientForeColor = System.Drawing.Color.Black
Me.btnRecalculateTiming.HoverGradientBackColor = System.Drawing.Color.Transparent
Me.btnRecalculateTiming.HoverGradientForeColor = System.Drawing.Color.Black
Me.btnRecalculateTiming.Image = Global.Aruti.Main.My.Resources.Resources.Recalculate_Timings
Me.btnRecalculateTiming.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
Me.btnRecalculateTiming.Location = New System.Drawing.Point(3, 413)
Me.btnRecalculateTiming.MinimumSize = New System.Drawing.Size(150, 35)
Me.btnRecalculateTiming.Name = "btnRecalculateTiming"
Me.btnRecalculateTiming.PressedGradientBackColor = System.Drawing.Color.Transparent
Me.btnRecalculateTiming.PressedGradientForeColor = System.Drawing.Color.Black
Me.btnRecalculateTiming.Size = New System.Drawing.Size(166, 35)
Me.btnRecalculateTiming.TabIndex = 20
Me.btnRecalculateTiming.Text = "Recalculate Timing"
Me.btnRecalculateTiming.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
Me.btnRecalculateTiming.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText
Me.btnRecalculateTiming.UseVisualStyleBackColor = false
'
'btnRoundOff
'
Me.btnRoundOff.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left)  _
            Or System.Windows.Forms.AnchorStyles.Right),System.Windows.Forms.AnchorStyles)
Me.btnRoundOff.BackColor = System.Drawing.Color.Transparent
Me.btnRoundOff.BackgroundImage = CType(resources.GetObject("btnRoundOff.BackgroundImage"),System.Drawing.Image)
Me.btnRoundOff.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
Me.btnRoundOff.BorderColor = System.Drawing.Color.Empty
Me.btnRoundOff.ButtonType = eZee.Common.eZeeLightButton.enButtonType.CheckButton
Me.btnRoundOff.FlatAppearance.BorderSize = 0
Me.btnRoundOff.FlatStyle = System.Windows.Forms.FlatStyle.Flat
Me.btnRoundOff.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
Me.btnRoundOff.ForeColor = System.Drawing.Color.Black
Me.btnRoundOff.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
Me.btnRoundOff.GradientForeColor = System.Drawing.Color.Black
Me.btnRoundOff.HoverGradientBackColor = System.Drawing.Color.Transparent
Me.btnRoundOff.HoverGradientForeColor = System.Drawing.Color.Black
Me.btnRoundOff.Image = Global.Aruti.Main.My.Resources.Resources.Auto_Rounding_24
Me.btnRoundOff.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
Me.btnRoundOff.Location = New System.Drawing.Point(3, 454)
Me.btnRoundOff.MinimumSize = New System.Drawing.Size(150, 35)
Me.btnRoundOff.Name = "btnRoundOff"
Me.btnRoundOff.PressedGradientBackColor = System.Drawing.Color.Transparent
Me.btnRoundOff.PressedGradientForeColor = System.Drawing.Color.Black
Me.btnRoundOff.Size = New System.Drawing.Size(166, 35)
Me.btnRoundOff.TabIndex = 22
Me.btnRoundOff.Text = "Round Off Timing"
Me.btnRoundOff.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
Me.btnRoundOff.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText
Me.btnRoundOff.UseVisualStyleBackColor = false
'
'btnGlobalEditDelete
'
Me.btnGlobalEditDelete.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left)  _
            Or System.Windows.Forms.AnchorStyles.Right),System.Windows.Forms.AnchorStyles)
Me.btnGlobalEditDelete.BackColor = System.Drawing.Color.Transparent
Me.btnGlobalEditDelete.BackgroundImage = CType(resources.GetObject("btnGlobalEditDelete.BackgroundImage"),System.Drawing.Image)
Me.btnGlobalEditDelete.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
Me.btnGlobalEditDelete.BorderColor = System.Drawing.Color.Empty
Me.btnGlobalEditDelete.ButtonType = eZee.Common.eZeeLightButton.enButtonType.CheckButton
Me.btnGlobalEditDelete.FlatAppearance.BorderSize = 0
Me.btnGlobalEditDelete.FlatStyle = System.Windows.Forms.FlatStyle.Flat
Me.btnGlobalEditDelete.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
Me.btnGlobalEditDelete.ForeColor = System.Drawing.Color.Black
Me.btnGlobalEditDelete.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
Me.btnGlobalEditDelete.GradientForeColor = System.Drawing.Color.Black
Me.btnGlobalEditDelete.HoverGradientBackColor = System.Drawing.Color.Transparent
Me.btnGlobalEditDelete.HoverGradientForeColor = System.Drawing.Color.Black
Me.btnGlobalEditDelete.Image = Global.Aruti.Main.My.Resources.Resources.Global_Edit_Delete_Timesheet_24
Me.btnGlobalEditDelete.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
Me.btnGlobalEditDelete.Location = New System.Drawing.Point(3, 495)
Me.btnGlobalEditDelete.MinimumSize = New System.Drawing.Size(150, 35)
Me.btnGlobalEditDelete.Name = "btnGlobalEditDelete"
Me.btnGlobalEditDelete.PressedGradientBackColor = System.Drawing.Color.Transparent
Me.btnGlobalEditDelete.PressedGradientForeColor = System.Drawing.Color.Black
Me.btnGlobalEditDelete.Size = New System.Drawing.Size(166, 35)
Me.btnGlobalEditDelete.TabIndex = 23
Me.btnGlobalEditDelete.Text = "Global Timesheet"
Me.btnGlobalEditDelete.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
Me.btnGlobalEditDelete.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText
Me.btnGlobalEditDelete.UseVisualStyleBackColor = false
'
'fpnlPayrollMasters
'
Me.fpnlPayrollMasters.AutoScroll = true
Me.fpnlPayrollMasters.Controls.Add(Me.btnPayrollGroup)
Me.fpnlPayrollMasters.Controls.Add(Me.btnPayPoint)
Me.fpnlPayrollMasters.Controls.Add(Me.objelLine1)
Me.fpnlPayrollMasters.Controls.Add(Me.btnBankBranch)
Me.fpnlPayrollMasters.Controls.Add(Me.btnBankAccountType)
Me.fpnlPayrollMasters.Controls.Add(Me.btnBankEDI)
Me.fpnlPayrollMasters.Controls.Add(Me.objelLine2)
Me.fpnlPayrollMasters.Controls.Add(Me.btnCurrency)
Me.fpnlPayrollMasters.Controls.Add(Me.btnDemomination)
Me.fpnlPayrollMasters.Controls.Add(Me.btnPayslipMessage)
Me.fpnlPayrollMasters.Controls.Add(Me.btnGlobalPayslipMessage)
Me.fpnlPayrollMasters.Controls.Add(Me.objelLine8)
Me.fpnlPayrollMasters.Controls.Add(Me.btnAccount)
Me.fpnlPayrollMasters.Controls.Add(Me.btnAccountConfiguration)
Me.fpnlPayrollMasters.Controls.Add(Me.btnEmpAccConfig)
Me.fpnlPayrollMasters.Controls.Add(Me.btnCCAccConfig)
Me.fpnlPayrollMasters.Controls.Add(Me.objLine11)
Me.fpnlPayrollMasters.Controls.Add(Me.btnPayAppLevel)
Me.fpnlPayrollMasters.Controls.Add(Me.btnPayApproverMap)
Me.fpnlPayrollMasters.Location = New System.Drawing.Point(200, 3)
Me.fpnlPayrollMasters.Name = "fpnlPayrollMasters"
Me.fpnlPayrollMasters.Size = New System.Drawing.Size(190, 41)
Me.fpnlPayrollMasters.TabIndex = 7
Me.fpnlPayrollMasters.Visible = false
'
'btnPayrollGroup
'
Me.btnPayrollGroup.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left)  _
            Or System.Windows.Forms.AnchorStyles.Right),System.Windows.Forms.AnchorStyles)
Me.btnPayrollGroup.BackColor = System.Drawing.Color.Transparent
Me.btnPayrollGroup.BackgroundImage = CType(resources.GetObject("btnPayrollGroup.BackgroundImage"),System.Drawing.Image)
Me.btnPayrollGroup.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
Me.btnPayrollGroup.BorderColor = System.Drawing.Color.Empty
Me.btnPayrollGroup.ButtonType = eZee.Common.eZeeLightButton.enButtonType.CheckButton
Me.btnPayrollGroup.FlatAppearance.BorderSize = 0
Me.btnPayrollGroup.FlatStyle = System.Windows.Forms.FlatStyle.Flat
Me.btnPayrollGroup.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
Me.btnPayrollGroup.ForeColor = System.Drawing.Color.Black
Me.btnPayrollGroup.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
Me.btnPayrollGroup.GradientForeColor = System.Drawing.Color.Black
Me.btnPayrollGroup.HoverGradientBackColor = System.Drawing.Color.Transparent
Me.btnPayrollGroup.HoverGradientForeColor = System.Drawing.Color.Black
Me.btnPayrollGroup.Image = Global.Aruti.Main.My.Resources.Resources.Master_Group
Me.btnPayrollGroup.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
Me.btnPayrollGroup.Location = New System.Drawing.Point(3, 3)
Me.btnPayrollGroup.MinimumSize = New System.Drawing.Size(150, 35)
Me.btnPayrollGroup.Name = "btnPayrollGroup"
Me.btnPayrollGroup.PressedGradientBackColor = System.Drawing.Color.Transparent
Me.btnPayrollGroup.PressedGradientForeColor = System.Drawing.Color.Black
Me.btnPayrollGroup.Size = New System.Drawing.Size(166, 35)
Me.btnPayrollGroup.TabIndex = 6
Me.btnPayrollGroup.Text = "Payroll &Group"
Me.btnPayrollGroup.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
Me.btnPayrollGroup.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText
Me.btnPayrollGroup.UseVisualStyleBackColor = false
'
'btnPayPoint
'
Me.btnPayPoint.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left)  _
            Or System.Windows.Forms.AnchorStyles.Right),System.Windows.Forms.AnchorStyles)
Me.btnPayPoint.BackColor = System.Drawing.Color.Transparent
Me.btnPayPoint.BackgroundImage = CType(resources.GetObject("btnPayPoint.BackgroundImage"),System.Drawing.Image)
Me.btnPayPoint.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
Me.btnPayPoint.BorderColor = System.Drawing.Color.Empty
Me.btnPayPoint.ButtonType = eZee.Common.eZeeLightButton.enButtonType.CheckButton
Me.btnPayPoint.FlatAppearance.BorderSize = 0
Me.btnPayPoint.FlatStyle = System.Windows.Forms.FlatStyle.Flat
Me.btnPayPoint.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
Me.btnPayPoint.ForeColor = System.Drawing.Color.Black
Me.btnPayPoint.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
Me.btnPayPoint.GradientForeColor = System.Drawing.Color.Black
Me.btnPayPoint.HoverGradientBackColor = System.Drawing.Color.Transparent
Me.btnPayPoint.HoverGradientForeColor = System.Drawing.Color.Black
Me.btnPayPoint.Image = Global.Aruti.Main.My.Resources.Resources.Pay_Point
Me.btnPayPoint.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
Me.btnPayPoint.Location = New System.Drawing.Point(3, 44)
Me.btnPayPoint.MinimumSize = New System.Drawing.Size(150, 35)
Me.btnPayPoint.Name = "btnPayPoint"
Me.btnPayPoint.PressedGradientBackColor = System.Drawing.Color.Transparent
Me.btnPayPoint.PressedGradientForeColor = System.Drawing.Color.Black
Me.btnPayPoint.Size = New System.Drawing.Size(166, 35)
Me.btnPayPoint.TabIndex = 9
Me.btnPayPoint.Text = "Pa&y Point"
Me.btnPayPoint.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
Me.btnPayPoint.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText
Me.btnPayPoint.UseVisualStyleBackColor = false
'
'objelLine1
'
Me.objelLine1.LineOrientation = eZee.Common.eZeeLine.Orientation.Horizontal
Me.objelLine1.Location = New System.Drawing.Point(3, 82)
Me.objelLine1.Name = "objelLine1"
Me.objelLine1.Size = New System.Drawing.Size(166, 17)
Me.objelLine1.TabIndex = 11
Me.objelLine1.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
'
'btnBankBranch
'
Me.btnBankBranch.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left)  _
            Or System.Windows.Forms.AnchorStyles.Right),System.Windows.Forms.AnchorStyles)
Me.btnBankBranch.BackColor = System.Drawing.Color.Transparent
Me.btnBankBranch.BackgroundImage = CType(resources.GetObject("btnBankBranch.BackgroundImage"),System.Drawing.Image)
Me.btnBankBranch.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
Me.btnBankBranch.BorderColor = System.Drawing.Color.Empty
Me.btnBankBranch.ButtonType = eZee.Common.eZeeLightButton.enButtonType.CheckButton
Me.btnBankBranch.FlatAppearance.BorderSize = 0
Me.btnBankBranch.FlatStyle = System.Windows.Forms.FlatStyle.Flat
Me.btnBankBranch.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
Me.btnBankBranch.ForeColor = System.Drawing.Color.Black
Me.btnBankBranch.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
Me.btnBankBranch.GradientForeColor = System.Drawing.Color.Black
Me.btnBankBranch.HoverGradientBackColor = System.Drawing.Color.Transparent
Me.btnBankBranch.HoverGradientForeColor = System.Drawing.Color.Black
Me.btnBankBranch.Image = Global.Aruti.Main.My.Resources.Resources.Bank_Branch
Me.btnBankBranch.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
Me.btnBankBranch.Location = New System.Drawing.Point(3, 102)
Me.btnBankBranch.MinimumSize = New System.Drawing.Size(150, 35)
Me.btnBankBranch.Name = "btnBankBranch"
Me.btnBankBranch.PressedGradientBackColor = System.Drawing.Color.Transparent
Me.btnBankBranch.PressedGradientForeColor = System.Drawing.Color.Black
Me.btnBankBranch.Size = New System.Drawing.Size(166, 35)
Me.btnBankBranch.TabIndex = 12
Me.btnBankBranch.Text = "Bank &Branch"
Me.btnBankBranch.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
Me.btnBankBranch.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText
Me.btnBankBranch.UseVisualStyleBackColor = false
'
'btnBankAccountType
'
Me.btnBankAccountType.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left)  _
            Or System.Windows.Forms.AnchorStyles.Right),System.Windows.Forms.AnchorStyles)
Me.btnBankAccountType.BackColor = System.Drawing.Color.Transparent
Me.btnBankAccountType.BackgroundImage = CType(resources.GetObject("btnBankAccountType.BackgroundImage"),System.Drawing.Image)
Me.btnBankAccountType.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
Me.btnBankAccountType.BorderColor = System.Drawing.Color.Empty
Me.btnBankAccountType.ButtonType = eZee.Common.eZeeLightButton.enButtonType.CheckButton
Me.btnBankAccountType.FlatAppearance.BorderSize = 0
Me.btnBankAccountType.FlatStyle = System.Windows.Forms.FlatStyle.Flat
Me.btnBankAccountType.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
Me.btnBankAccountType.ForeColor = System.Drawing.Color.Black
Me.btnBankAccountType.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
Me.btnBankAccountType.GradientForeColor = System.Drawing.Color.Black
Me.btnBankAccountType.HoverGradientBackColor = System.Drawing.Color.Transparent
Me.btnBankAccountType.HoverGradientForeColor = System.Drawing.Color.Black
Me.btnBankAccountType.Image = Global.Aruti.Main.My.Resources.Resources.BankAccount_Type
Me.btnBankAccountType.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
Me.btnBankAccountType.Location = New System.Drawing.Point(3, 143)
Me.btnBankAccountType.MinimumSize = New System.Drawing.Size(150, 35)
Me.btnBankAccountType.Name = "btnBankAccountType"
Me.btnBankAccountType.PressedGradientBackColor = System.Drawing.Color.Transparent
Me.btnBankAccountType.PressedGradientForeColor = System.Drawing.Color.Black
Me.btnBankAccountType.Size = New System.Drawing.Size(166, 35)
Me.btnBankAccountType.TabIndex = 13
Me.btnBankAccountType.Text = "Bank &Account Type"
Me.btnBankAccountType.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
Me.btnBankAccountType.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText
Me.btnBankAccountType.UseVisualStyleBackColor = false
'
'btnBankEDI
'
Me.btnBankEDI.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left)  _
            Or System.Windows.Forms.AnchorStyles.Right),System.Windows.Forms.AnchorStyles)
Me.btnBankEDI.BackColor = System.Drawing.Color.Transparent
Me.btnBankEDI.BackgroundImage = CType(resources.GetObject("btnBankEDI.BackgroundImage"),System.Drawing.Image)
Me.btnBankEDI.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
Me.btnBankEDI.BorderColor = System.Drawing.Color.Empty
Me.btnBankEDI.ButtonType = eZee.Common.eZeeLightButton.enButtonType.CheckButton
Me.btnBankEDI.FlatAppearance.BorderSize = 0
Me.btnBankEDI.FlatStyle = System.Windows.Forms.FlatStyle.Flat
Me.btnBankEDI.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
Me.btnBankEDI.ForeColor = System.Drawing.Color.Black
Me.btnBankEDI.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
Me.btnBankEDI.GradientForeColor = System.Drawing.Color.Black
Me.btnBankEDI.HoverGradientBackColor = System.Drawing.Color.Transparent
Me.btnBankEDI.HoverGradientForeColor = System.Drawing.Color.Black
Me.btnBankEDI.Image = Global.Aruti.Main.My.Resources.Resources.Bank_EDI
Me.btnBankEDI.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
Me.btnBankEDI.Location = New System.Drawing.Point(3, 184)
Me.btnBankEDI.MinimumSize = New System.Drawing.Size(150, 35)
Me.btnBankEDI.Name = "btnBankEDI"
Me.btnBankEDI.PressedGradientBackColor = System.Drawing.Color.Transparent
Me.btnBankEDI.PressedGradientForeColor = System.Drawing.Color.Black
Me.btnBankEDI.Size = New System.Drawing.Size(166, 35)
Me.btnBankEDI.TabIndex = 14
Me.btnBankEDI.Text = "Bank &EDI"
Me.btnBankEDI.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
Me.btnBankEDI.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText
Me.btnBankEDI.UseVisualStyleBackColor = false
'
'objelLine2
'
Me.objelLine2.LineOrientation = eZee.Common.eZeeLine.Orientation.Horizontal
Me.objelLine2.Location = New System.Drawing.Point(3, 222)
Me.objelLine2.Name = "objelLine2"
Me.objelLine2.Size = New System.Drawing.Size(166, 17)
Me.objelLine2.TabIndex = 15
Me.objelLine2.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
'
'btnCurrency
'
Me.btnCurrency.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left)  _
            Or System.Windows.Forms.AnchorStyles.Right),System.Windows.Forms.AnchorStyles)
Me.btnCurrency.BackColor = System.Drawing.Color.Transparent
Me.btnCurrency.BackgroundImage = CType(resources.GetObject("btnCurrency.BackgroundImage"),System.Drawing.Image)
Me.btnCurrency.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
Me.btnCurrency.BorderColor = System.Drawing.Color.Empty
Me.btnCurrency.ButtonType = eZee.Common.eZeeLightButton.enButtonType.CheckButton
Me.btnCurrency.FlatAppearance.BorderSize = 0
Me.btnCurrency.FlatStyle = System.Windows.Forms.FlatStyle.Flat
Me.btnCurrency.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
Me.btnCurrency.ForeColor = System.Drawing.Color.Black
Me.btnCurrency.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
Me.btnCurrency.GradientForeColor = System.Drawing.Color.Black
Me.btnCurrency.HoverGradientBackColor = System.Drawing.Color.Transparent
Me.btnCurrency.HoverGradientForeColor = System.Drawing.Color.Black
Me.btnCurrency.Image = Global.Aruti.Main.My.Resources.Resources.currency
Me.btnCurrency.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
Me.btnCurrency.Location = New System.Drawing.Point(3, 242)
Me.btnCurrency.MinimumSize = New System.Drawing.Size(150, 35)
Me.btnCurrency.Name = "btnCurrency"
Me.btnCurrency.PressedGradientBackColor = System.Drawing.Color.Transparent
Me.btnCurrency.PressedGradientForeColor = System.Drawing.Color.Black
Me.btnCurrency.Size = New System.Drawing.Size(166, 35)
Me.btnCurrency.TabIndex = 16
Me.btnCurrency.Text = "C&urrency"
Me.btnCurrency.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
Me.btnCurrency.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText
Me.btnCurrency.UseVisualStyleBackColor = false
'
'btnDemomination
'
Me.btnDemomination.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left)  _
            Or System.Windows.Forms.AnchorStyles.Right),System.Windows.Forms.AnchorStyles)
Me.btnDemomination.BackColor = System.Drawing.Color.Transparent
Me.btnDemomination.BackgroundImage = CType(resources.GetObject("btnDemomination.BackgroundImage"),System.Drawing.Image)
Me.btnDemomination.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
Me.btnDemomination.BorderColor = System.Drawing.Color.Empty
Me.btnDemomination.ButtonType = eZee.Common.eZeeLightButton.enButtonType.CheckButton
Me.btnDemomination.FlatAppearance.BorderSize = 0
Me.btnDemomination.FlatStyle = System.Windows.Forms.FlatStyle.Flat
Me.btnDemomination.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
Me.btnDemomination.ForeColor = System.Drawing.Color.Black
Me.btnDemomination.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
Me.btnDemomination.GradientForeColor = System.Drawing.Color.Black
Me.btnDemomination.HoverGradientBackColor = System.Drawing.Color.Transparent
Me.btnDemomination.HoverGradientForeColor = System.Drawing.Color.Black
Me.btnDemomination.Image = Global.Aruti.Main.My.Resources.Resources.Denomination
Me.btnDemomination.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
Me.btnDemomination.Location = New System.Drawing.Point(3, 283)
Me.btnDemomination.MinimumSize = New System.Drawing.Size(150, 35)
Me.btnDemomination.Name = "btnDemomination"
Me.btnDemomination.PressedGradientBackColor = System.Drawing.Color.Transparent
Me.btnDemomination.PressedGradientForeColor = System.Drawing.Color.Black
Me.btnDemomination.Size = New System.Drawing.Size(166, 35)
Me.btnDemomination.TabIndex = 17
Me.btnDemomination.Text = "&Denomination"
Me.btnDemomination.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
Me.btnDemomination.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText
Me.btnDemomination.UseVisualStyleBackColor = false
'
'btnPayslipMessage
'
Me.btnPayslipMessage.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left)  _
            Or System.Windows.Forms.AnchorStyles.Right),System.Windows.Forms.AnchorStyles)
Me.btnPayslipMessage.BackColor = System.Drawing.Color.Transparent
Me.btnPayslipMessage.BackgroundImage = CType(resources.GetObject("btnPayslipMessage.BackgroundImage"),System.Drawing.Image)
Me.btnPayslipMessage.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
Me.btnPayslipMessage.BorderColor = System.Drawing.Color.Empty
Me.btnPayslipMessage.ButtonType = eZee.Common.eZeeLightButton.enButtonType.CheckButton
Me.btnPayslipMessage.FlatAppearance.BorderSize = 0
Me.btnPayslipMessage.FlatStyle = System.Windows.Forms.FlatStyle.Flat
Me.btnPayslipMessage.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
Me.btnPayslipMessage.ForeColor = System.Drawing.Color.Black
Me.btnPayslipMessage.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
Me.btnPayslipMessage.GradientForeColor = System.Drawing.Color.Black
Me.btnPayslipMessage.HoverGradientBackColor = System.Drawing.Color.Transparent
Me.btnPayslipMessage.HoverGradientForeColor = System.Drawing.Color.Black
Me.btnPayslipMessage.Image = Global.Aruti.Main.My.Resources.Resources.payslip_message
Me.btnPayslipMessage.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
Me.btnPayslipMessage.Location = New System.Drawing.Point(3, 324)
Me.btnPayslipMessage.MinimumSize = New System.Drawing.Size(150, 35)
Me.btnPayslipMessage.Name = "btnPayslipMessage"
Me.btnPayslipMessage.PressedGradientBackColor = System.Drawing.Color.Transparent
Me.btnPayslipMessage.PressedGradientForeColor = System.Drawing.Color.Black
Me.btnPayslipMessage.Size = New System.Drawing.Size(166, 35)
Me.btnPayslipMessage.TabIndex = 18
Me.btnPayslipMessage.Text = "&Payslip Messages"
Me.btnPayslipMessage.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
Me.btnPayslipMessage.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText
Me.btnPayslipMessage.UseVisualStyleBackColor = false
'
'btnGlobalPayslipMessage
'
Me.btnGlobalPayslipMessage.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left)  _
            Or System.Windows.Forms.AnchorStyles.Right),System.Windows.Forms.AnchorStyles)
Me.btnGlobalPayslipMessage.BackColor = System.Drawing.Color.Transparent
Me.btnGlobalPayslipMessage.BackgroundImage = CType(resources.GetObject("btnGlobalPayslipMessage.BackgroundImage"),System.Drawing.Image)
Me.btnGlobalPayslipMessage.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
Me.btnGlobalPayslipMessage.BorderColor = System.Drawing.Color.Empty
Me.btnGlobalPayslipMessage.ButtonType = eZee.Common.eZeeLightButton.enButtonType.CheckButton
Me.btnGlobalPayslipMessage.FlatAppearance.BorderSize = 0
Me.btnGlobalPayslipMessage.FlatStyle = System.Windows.Forms.FlatStyle.Flat
Me.btnGlobalPayslipMessage.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
Me.btnGlobalPayslipMessage.ForeColor = System.Drawing.Color.Black
Me.btnGlobalPayslipMessage.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
Me.btnGlobalPayslipMessage.GradientForeColor = System.Drawing.Color.Black
Me.btnGlobalPayslipMessage.HoverGradientBackColor = System.Drawing.Color.Transparent
Me.btnGlobalPayslipMessage.HoverGradientForeColor = System.Drawing.Color.Black
Me.btnGlobalPayslipMessage.Image = Global.Aruti.Main.My.Resources.Resources.globle_payslip_message
Me.btnGlobalPayslipMessage.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
Me.btnGlobalPayslipMessage.Location = New System.Drawing.Point(3, 365)
Me.btnGlobalPayslipMessage.MinimumSize = New System.Drawing.Size(150, 35)
Me.btnGlobalPayslipMessage.Name = "btnGlobalPayslipMessage"
Me.btnGlobalPayslipMessage.PressedGradientBackColor = System.Drawing.Color.Transparent
Me.btnGlobalPayslipMessage.PressedGradientForeColor = System.Drawing.Color.Black
Me.btnGlobalPayslipMessage.Size = New System.Drawing.Size(166, 35)
Me.btnGlobalPayslipMessage.TabIndex = 19
Me.btnGlobalPayslipMessage.Text = "&Global Payslip Message"
Me.btnGlobalPayslipMessage.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
Me.btnGlobalPayslipMessage.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText
Me.btnGlobalPayslipMessage.UseVisualStyleBackColor = false
'
'objelLine8
'
Me.objelLine8.LineOrientation = eZee.Common.eZeeLine.Orientation.Horizontal
Me.objelLine8.Location = New System.Drawing.Point(3, 403)
Me.objelLine8.Name = "objelLine8"
Me.objelLine8.Size = New System.Drawing.Size(166, 17)
Me.objelLine8.TabIndex = 20
Me.objelLine8.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
'
'btnAccount
'
Me.btnAccount.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left)  _
            Or System.Windows.Forms.AnchorStyles.Right),System.Windows.Forms.AnchorStyles)
Me.btnAccount.BackColor = System.Drawing.Color.Transparent
Me.btnAccount.BackgroundImage = CType(resources.GetObject("btnAccount.BackgroundImage"),System.Drawing.Image)
Me.btnAccount.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
Me.btnAccount.BorderColor = System.Drawing.Color.Empty
Me.btnAccount.ButtonType = eZee.Common.eZeeLightButton.enButtonType.CheckButton
Me.btnAccount.FlatAppearance.BorderSize = 0
Me.btnAccount.FlatStyle = System.Windows.Forms.FlatStyle.Flat
Me.btnAccount.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
Me.btnAccount.ForeColor = System.Drawing.Color.Black
Me.btnAccount.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
Me.btnAccount.GradientForeColor = System.Drawing.Color.Black
Me.btnAccount.HoverGradientBackColor = System.Drawing.Color.Transparent
Me.btnAccount.HoverGradientForeColor = System.Drawing.Color.Black
Me.btnAccount.Image = Global.Aruti.Main.My.Resources.Resources.account
Me.btnAccount.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
Me.btnAccount.Location = New System.Drawing.Point(3, 423)
Me.btnAccount.MinimumSize = New System.Drawing.Size(150, 35)
Me.btnAccount.Name = "btnAccount"
Me.btnAccount.PressedGradientBackColor = System.Drawing.Color.Transparent
Me.btnAccount.PressedGradientForeColor = System.Drawing.Color.Black
Me.btnAccount.Size = New System.Drawing.Size(166, 35)
Me.btnAccount.TabIndex = 21
Me.btnAccount.Text = "A&ccount"
Me.btnAccount.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
Me.btnAccount.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText
Me.btnAccount.UseVisualStyleBackColor = false
'
'btnAccountConfiguration
'
Me.btnAccountConfiguration.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left)  _
            Or System.Windows.Forms.AnchorStyles.Right),System.Windows.Forms.AnchorStyles)
Me.btnAccountConfiguration.BackColor = System.Drawing.Color.Transparent
Me.btnAccountConfiguration.BackgroundImage = CType(resources.GetObject("btnAccountConfiguration.BackgroundImage"),System.Drawing.Image)
Me.btnAccountConfiguration.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
Me.btnAccountConfiguration.BorderColor = System.Drawing.Color.Empty
Me.btnAccountConfiguration.ButtonType = eZee.Common.eZeeLightButton.enButtonType.CheckButton
Me.btnAccountConfiguration.FlatAppearance.BorderSize = 0
Me.btnAccountConfiguration.FlatStyle = System.Windows.Forms.FlatStyle.Flat
Me.btnAccountConfiguration.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
Me.btnAccountConfiguration.ForeColor = System.Drawing.Color.Black
Me.btnAccountConfiguration.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
Me.btnAccountConfiguration.GradientForeColor = System.Drawing.Color.Black
Me.btnAccountConfiguration.HoverGradientBackColor = System.Drawing.Color.Transparent
Me.btnAccountConfiguration.HoverGradientForeColor = System.Drawing.Color.Black
Me.btnAccountConfiguration.Image = Global.Aruti.Main.My.Resources.Resources.account_config
Me.btnAccountConfiguration.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
Me.btnAccountConfiguration.Location = New System.Drawing.Point(3, 464)
Me.btnAccountConfiguration.MinimumSize = New System.Drawing.Size(150, 35)
Me.btnAccountConfiguration.Name = "btnAccountConfiguration"
Me.btnAccountConfiguration.PressedGradientBackColor = System.Drawing.Color.Transparent
Me.btnAccountConfiguration.PressedGradientForeColor = System.Drawing.Color.Black
Me.btnAccountConfiguration.Size = New System.Drawing.Size(166, 35)
Me.btnAccountConfiguration.TabIndex = 22
Me.btnAccountConfiguration.Text = "C&ompany Acc. Config."
Me.btnAccountConfiguration.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
Me.btnAccountConfiguration.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText
Me.btnAccountConfiguration.UseVisualStyleBackColor = false
'
'btnEmpAccConfig
'
Me.btnEmpAccConfig.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left)  _
            Or System.Windows.Forms.AnchorStyles.Right),System.Windows.Forms.AnchorStyles)
Me.btnEmpAccConfig.BackColor = System.Drawing.Color.Transparent
Me.btnEmpAccConfig.BackgroundImage = CType(resources.GetObject("btnEmpAccConfig.BackgroundImage"),System.Drawing.Image)
Me.btnEmpAccConfig.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
Me.btnEmpAccConfig.BorderColor = System.Drawing.Color.Empty
Me.btnEmpAccConfig.ButtonType = eZee.Common.eZeeLightButton.enButtonType.CheckButton
Me.btnEmpAccConfig.FlatAppearance.BorderSize = 0
Me.btnEmpAccConfig.FlatStyle = System.Windows.Forms.FlatStyle.Flat
Me.btnEmpAccConfig.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
Me.btnEmpAccConfig.ForeColor = System.Drawing.Color.Black
Me.btnEmpAccConfig.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
Me.btnEmpAccConfig.GradientForeColor = System.Drawing.Color.Black
Me.btnEmpAccConfig.HoverGradientBackColor = System.Drawing.Color.Transparent
Me.btnEmpAccConfig.HoverGradientForeColor = System.Drawing.Color.Black
Me.btnEmpAccConfig.Image = Global.Aruti.Main.My.Resources.Resources.account_config
Me.btnEmpAccConfig.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
Me.btnEmpAccConfig.Location = New System.Drawing.Point(3, 505)
Me.btnEmpAccConfig.MinimumSize = New System.Drawing.Size(150, 35)
Me.btnEmpAccConfig.Name = "btnEmpAccConfig"
Me.btnEmpAccConfig.PressedGradientBackColor = System.Drawing.Color.Transparent
Me.btnEmpAccConfig.PressedGradientForeColor = System.Drawing.Color.Black
Me.btnEmpAccConfig.Size = New System.Drawing.Size(166, 35)
Me.btnEmpAccConfig.TabIndex = 23
Me.btnEmpAccConfig.Text = "Emp&loyee  Acc. Config."
Me.btnEmpAccConfig.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
Me.btnEmpAccConfig.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText
Me.btnEmpAccConfig.UseVisualStyleBackColor = false
'
'btnCCAccConfig
'
Me.btnCCAccConfig.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left)  _
            Or System.Windows.Forms.AnchorStyles.Right),System.Windows.Forms.AnchorStyles)
Me.btnCCAccConfig.BackColor = System.Drawing.Color.Transparent
Me.btnCCAccConfig.BackgroundImage = CType(resources.GetObject("btnCCAccConfig.BackgroundImage"),System.Drawing.Image)
Me.btnCCAccConfig.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
Me.btnCCAccConfig.BorderColor = System.Drawing.Color.Empty
Me.btnCCAccConfig.ButtonType = eZee.Common.eZeeLightButton.enButtonType.CheckButton
Me.btnCCAccConfig.FlatAppearance.BorderSize = 0
Me.btnCCAccConfig.FlatStyle = System.Windows.Forms.FlatStyle.Flat
Me.btnCCAccConfig.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
Me.btnCCAccConfig.ForeColor = System.Drawing.Color.Black
Me.btnCCAccConfig.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
Me.btnCCAccConfig.GradientForeColor = System.Drawing.Color.Black
Me.btnCCAccConfig.HoverGradientBackColor = System.Drawing.Color.Transparent
Me.btnCCAccConfig.HoverGradientForeColor = System.Drawing.Color.Black
Me.btnCCAccConfig.Image = Global.Aruti.Main.My.Resources.Resources.account_config
Me.btnCCAccConfig.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
Me.btnCCAccConfig.Location = New System.Drawing.Point(3, 546)
Me.btnCCAccConfig.MinimumSize = New System.Drawing.Size(150, 35)
Me.btnCCAccConfig.Name = "btnCCAccConfig"
Me.btnCCAccConfig.PressedGradientBackColor = System.Drawing.Color.Transparent
Me.btnCCAccConfig.PressedGradientForeColor = System.Drawing.Color.Black
Me.btnCCAccConfig.Size = New System.Drawing.Size(166, 35)
Me.btnCCAccConfig.TabIndex = 24
Me.btnCCAccConfig.Text = "CCe&nter  Acc. Config."
Me.btnCCAccConfig.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
Me.btnCCAccConfig.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText
Me.btnCCAccConfig.UseVisualStyleBackColor = false
'
'objLine11
'
Me.objLine11.LineOrientation = eZee.Common.eZeeLine.Orientation.Horizontal
Me.objLine11.Location = New System.Drawing.Point(3, 584)
Me.objLine11.Name = "objLine11"
Me.objLine11.Size = New System.Drawing.Size(166, 17)
Me.objLine11.TabIndex = 25
Me.objLine11.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
'
'btnPayAppLevel
'
Me.btnPayAppLevel.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left)  _
            Or System.Windows.Forms.AnchorStyles.Right),System.Windows.Forms.AnchorStyles)
Me.btnPayAppLevel.BackColor = System.Drawing.Color.Transparent
Me.btnPayAppLevel.BackgroundImage = CType(resources.GetObject("btnPayAppLevel.BackgroundImage"),System.Drawing.Image)
Me.btnPayAppLevel.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
Me.btnPayAppLevel.BorderColor = System.Drawing.Color.Empty
Me.btnPayAppLevel.ButtonType = eZee.Common.eZeeLightButton.enButtonType.CheckButton
Me.btnPayAppLevel.FlatAppearance.BorderSize = 0
Me.btnPayAppLevel.FlatStyle = System.Windows.Forms.FlatStyle.Flat
Me.btnPayAppLevel.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
Me.btnPayAppLevel.ForeColor = System.Drawing.Color.Black
Me.btnPayAppLevel.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
Me.btnPayAppLevel.GradientForeColor = System.Drawing.Color.Black
Me.btnPayAppLevel.HoverGradientBackColor = System.Drawing.Color.Transparent
Me.btnPayAppLevel.HoverGradientForeColor = System.Drawing.Color.Black
Me.btnPayAppLevel.Image = Global.Aruti.Main.My.Resources.Resources.Approver_Level
Me.btnPayAppLevel.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
Me.btnPayAppLevel.Location = New System.Drawing.Point(3, 604)
Me.btnPayAppLevel.MinimumSize = New System.Drawing.Size(150, 35)
Me.btnPayAppLevel.Name = "btnPayAppLevel"
Me.btnPayAppLevel.PressedGradientBackColor = System.Drawing.Color.Transparent
Me.btnPayAppLevel.PressedGradientForeColor = System.Drawing.Color.Black
Me.btnPayAppLevel.Size = New System.Drawing.Size(166, 35)
Me.btnPayAppLevel.TabIndex = 26
Me.btnPayAppLevel.Text = "&Approver Level"
Me.btnPayAppLevel.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
Me.btnPayAppLevel.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText
Me.btnPayAppLevel.UseVisualStyleBackColor = false
'
'btnPayApproverMap
'
Me.btnPayApproverMap.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left)  _
            Or System.Windows.Forms.AnchorStyles.Right),System.Windows.Forms.AnchorStyles)
Me.btnPayApproverMap.BackColor = System.Drawing.Color.Transparent
Me.btnPayApproverMap.BackgroundImage = CType(resources.GetObject("btnPayApproverMap.BackgroundImage"),System.Drawing.Image)
Me.btnPayApproverMap.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
Me.btnPayApproverMap.BorderColor = System.Drawing.Color.Empty
Me.btnPayApproverMap.ButtonType = eZee.Common.eZeeLightButton.enButtonType.CheckButton
Me.btnPayApproverMap.FlatAppearance.BorderSize = 0
Me.btnPayApproverMap.FlatStyle = System.Windows.Forms.FlatStyle.Flat
Me.btnPayApproverMap.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
Me.btnPayApproverMap.ForeColor = System.Drawing.Color.Black
Me.btnPayApproverMap.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
Me.btnPayApproverMap.GradientForeColor = System.Drawing.Color.Black
Me.btnPayApproverMap.HoverGradientBackColor = System.Drawing.Color.Transparent
Me.btnPayApproverMap.HoverGradientForeColor = System.Drawing.Color.Black
Me.btnPayApproverMap.Image = Global.Aruti.Main.My.Resources.Resources.Leave_Approver
Me.btnPayApproverMap.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
Me.btnPayApproverMap.Location = New System.Drawing.Point(3, 645)
Me.btnPayApproverMap.MinimumSize = New System.Drawing.Size(150, 35)
Me.btnPayApproverMap.Name = "btnPayApproverMap"
Me.btnPayApproverMap.PressedGradientBackColor = System.Drawing.Color.Transparent
Me.btnPayApproverMap.PressedGradientForeColor = System.Drawing.Color.Black
Me.btnPayApproverMap.Size = New System.Drawing.Size(166, 35)
Me.btnPayApproverMap.TabIndex = 27
Me.btnPayApproverMap.Text = "&Approver Mapping"
Me.btnPayApproverMap.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
Me.btnPayApproverMap.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText
Me.btnPayApproverMap.UseVisualStyleBackColor = false
'
'fpnlAssessment
'
Me.fpnlAssessment.AutoScroll = true
Me.fpnlAssessment.Controls.Add(Me.btnAssessItems)
Me.fpnlAssessment.Controls.Add(Me.btnAssessSubItem)
Me.fpnlAssessment.Controls.Add(Me.btnEmployeeAssessment)
Me.fpnlAssessment.Controls.Add(Me.btnAssessorAssessmentlist)
Me.fpnlAssessment.Controls.Add(Me.btnReviewerAssessment)
Me.fpnlAssessment.Location = New System.Drawing.Point(200, 50)
Me.fpnlAssessment.Name = "fpnlAssessment"
Me.fpnlAssessment.Size = New System.Drawing.Size(191, 41)
Me.fpnlAssessment.TabIndex = 4
Me.fpnlAssessment.Visible = false
'
'btnAssessItems
'
Me.btnAssessItems.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left)  _
            Or System.Windows.Forms.AnchorStyles.Right),System.Windows.Forms.AnchorStyles)
Me.btnAssessItems.BackColor = System.Drawing.Color.Transparent
Me.btnAssessItems.BackgroundImage = CType(resources.GetObject("btnAssessItems.BackgroundImage"),System.Drawing.Image)
Me.btnAssessItems.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
Me.btnAssessItems.BorderColor = System.Drawing.Color.Empty
Me.btnAssessItems.ButtonType = eZee.Common.eZeeLightButton.enButtonType.CheckButton
Me.btnAssessItems.FlatAppearance.BorderSize = 0
Me.btnAssessItems.FlatStyle = System.Windows.Forms.FlatStyle.Flat
Me.btnAssessItems.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
Me.btnAssessItems.ForeColor = System.Drawing.Color.Black
Me.btnAssessItems.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
Me.btnAssessItems.GradientForeColor = System.Drawing.Color.Black
Me.btnAssessItems.HoverGradientBackColor = System.Drawing.Color.Transparent
Me.btnAssessItems.HoverGradientForeColor = System.Drawing.Color.Black
Me.btnAssessItems.Image = Global.Aruti.Main.My.Resources.Resources.Assessment_Item
Me.btnAssessItems.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
Me.btnAssessItems.Location = New System.Drawing.Point(3, 3)
Me.btnAssessItems.MinimumSize = New System.Drawing.Size(150, 35)
Me.btnAssessItems.Name = "btnAssessItems"
Me.btnAssessItems.PressedGradientBackColor = System.Drawing.Color.Transparent
Me.btnAssessItems.PressedGradientForeColor = System.Drawing.Color.Black
Me.btnAssessItems.Size = New System.Drawing.Size(166, 35)
Me.btnAssessItems.TabIndex = 9
Me.btnAssessItems.Text = "Assessment &Items"
Me.btnAssessItems.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
Me.btnAssessItems.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText
Me.btnAssessItems.UseVisualStyleBackColor = false
'
'btnAssessSubItem
'
Me.btnAssessSubItem.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left)  _
            Or System.Windows.Forms.AnchorStyles.Right),System.Windows.Forms.AnchorStyles)
Me.btnAssessSubItem.BackColor = System.Drawing.Color.Transparent
Me.btnAssessSubItem.BackgroundImage = CType(resources.GetObject("btnAssessSubItem.BackgroundImage"),System.Drawing.Image)
Me.btnAssessSubItem.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
Me.btnAssessSubItem.BorderColor = System.Drawing.Color.Empty
Me.btnAssessSubItem.ButtonType = eZee.Common.eZeeLightButton.enButtonType.CheckButton
Me.btnAssessSubItem.FlatAppearance.BorderSize = 0
Me.btnAssessSubItem.FlatStyle = System.Windows.Forms.FlatStyle.Flat
Me.btnAssessSubItem.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
Me.btnAssessSubItem.ForeColor = System.Drawing.Color.Black
Me.btnAssessSubItem.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
Me.btnAssessSubItem.GradientForeColor = System.Drawing.Color.Black
Me.btnAssessSubItem.HoverGradientBackColor = System.Drawing.Color.Transparent
Me.btnAssessSubItem.HoverGradientForeColor = System.Drawing.Color.Black
Me.btnAssessSubItem.Image = Global.Aruti.Main.My.Resources.Resources.assess_sub_items
Me.btnAssessSubItem.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
Me.btnAssessSubItem.Location = New System.Drawing.Point(3, 44)
Me.btnAssessSubItem.MinimumSize = New System.Drawing.Size(150, 35)
Me.btnAssessSubItem.Name = "btnAssessSubItem"
Me.btnAssessSubItem.PressedGradientBackColor = System.Drawing.Color.Transparent
Me.btnAssessSubItem.PressedGradientForeColor = System.Drawing.Color.Black
Me.btnAssessSubItem.Size = New System.Drawing.Size(166, 35)
Me.btnAssessSubItem.TabIndex = 14
Me.btnAssessSubItem.Text = "Assessment &Sub Items"
Me.btnAssessSubItem.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
Me.btnAssessSubItem.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText
Me.btnAssessSubItem.UseVisualStyleBackColor = false
'
'btnEmployeeAssessment
'
Me.btnEmployeeAssessment.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left)  _
            Or System.Windows.Forms.AnchorStyles.Right),System.Windows.Forms.AnchorStyles)
Me.btnEmployeeAssessment.BackColor = System.Drawing.Color.Transparent
Me.btnEmployeeAssessment.BackgroundImage = CType(resources.GetObject("btnEmployeeAssessment.BackgroundImage"),System.Drawing.Image)
Me.btnEmployeeAssessment.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
Me.btnEmployeeAssessment.BorderColor = System.Drawing.Color.Empty
Me.btnEmployeeAssessment.ButtonType = eZee.Common.eZeeLightButton.enButtonType.CheckButton
Me.btnEmployeeAssessment.FlatAppearance.BorderSize = 0
Me.btnEmployeeAssessment.FlatStyle = System.Windows.Forms.FlatStyle.Flat
Me.btnEmployeeAssessment.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
Me.btnEmployeeAssessment.ForeColor = System.Drawing.Color.Black
Me.btnEmployeeAssessment.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
Me.btnEmployeeAssessment.GradientForeColor = System.Drawing.Color.Black
Me.btnEmployeeAssessment.HoverGradientBackColor = System.Drawing.Color.Transparent
Me.btnEmployeeAssessment.HoverGradientForeColor = System.Drawing.Color.Black
Me.btnEmployeeAssessment.Image = Global.Aruti.Main.My.Resources.Resources.EmployeeAssessment
Me.btnEmployeeAssessment.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
Me.btnEmployeeAssessment.Location = New System.Drawing.Point(3, 85)
Me.btnEmployeeAssessment.MinimumSize = New System.Drawing.Size(150, 35)
Me.btnEmployeeAssessment.Name = "btnEmployeeAssessment"
Me.btnEmployeeAssessment.PressedGradientBackColor = System.Drawing.Color.Transparent
Me.btnEmployeeAssessment.PressedGradientForeColor = System.Drawing.Color.Black
Me.btnEmployeeAssessment.Size = New System.Drawing.Size(166, 35)
Me.btnEmployeeAssessment.TabIndex = 12
Me.btnEmployeeAssessment.Text = "Self A&ssessment"
Me.btnEmployeeAssessment.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
Me.btnEmployeeAssessment.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText
Me.btnEmployeeAssessment.UseVisualStyleBackColor = false
'
'btnAssessorAssessmentlist
'
Me.btnAssessorAssessmentlist.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left)  _
            Or System.Windows.Forms.AnchorStyles.Right),System.Windows.Forms.AnchorStyles)
Me.btnAssessorAssessmentlist.BackColor = System.Drawing.Color.Transparent
Me.btnAssessorAssessmentlist.BackgroundImage = CType(resources.GetObject("btnAssessorAssessmentlist.BackgroundImage"),System.Drawing.Image)
Me.btnAssessorAssessmentlist.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
Me.btnAssessorAssessmentlist.BorderColor = System.Drawing.Color.Empty
Me.btnAssessorAssessmentlist.ButtonType = eZee.Common.eZeeLightButton.enButtonType.CheckButton
Me.btnAssessorAssessmentlist.FlatAppearance.BorderSize = 0
Me.btnAssessorAssessmentlist.FlatStyle = System.Windows.Forms.FlatStyle.Flat
Me.btnAssessorAssessmentlist.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
Me.btnAssessorAssessmentlist.ForeColor = System.Drawing.Color.Black
Me.btnAssessorAssessmentlist.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
Me.btnAssessorAssessmentlist.GradientForeColor = System.Drawing.Color.Black
Me.btnAssessorAssessmentlist.HoverGradientBackColor = System.Drawing.Color.Transparent
Me.btnAssessorAssessmentlist.HoverGradientForeColor = System.Drawing.Color.Black
Me.btnAssessorAssessmentlist.Image = Global.Aruti.Main.My.Resources.Resources.AssessorAssessment
Me.btnAssessorAssessmentlist.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
Me.btnAssessorAssessmentlist.Location = New System.Drawing.Point(3, 126)
Me.btnAssessorAssessmentlist.MinimumSize = New System.Drawing.Size(150, 35)
Me.btnAssessorAssessmentlist.Name = "btnAssessorAssessmentlist"
Me.btnAssessorAssessmentlist.PressedGradientBackColor = System.Drawing.Color.Transparent
Me.btnAssessorAssessmentlist.PressedGradientForeColor = System.Drawing.Color.Black
Me.btnAssessorAssessmentlist.Size = New System.Drawing.Size(166, 35)
Me.btnAssessorAssessmentlist.TabIndex = 13
Me.btnAssessorAssessmentlist.Text = "Assessor Ass&essment"
Me.btnAssessorAssessmentlist.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
Me.btnAssessorAssessmentlist.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText
Me.btnAssessorAssessmentlist.UseVisualStyleBackColor = false
'
'btnReviewerAssessment
'
Me.btnReviewerAssessment.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left)  _
            Or System.Windows.Forms.AnchorStyles.Right),System.Windows.Forms.AnchorStyles)
Me.btnReviewerAssessment.BackColor = System.Drawing.Color.Transparent
Me.btnReviewerAssessment.BackgroundImage = CType(resources.GetObject("btnReviewerAssessment.BackgroundImage"),System.Drawing.Image)
Me.btnReviewerAssessment.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
Me.btnReviewerAssessment.BorderColor = System.Drawing.Color.Empty
Me.btnReviewerAssessment.ButtonType = eZee.Common.eZeeLightButton.enButtonType.CheckButton
Me.btnReviewerAssessment.FlatAppearance.BorderSize = 0
Me.btnReviewerAssessment.FlatStyle = System.Windows.Forms.FlatStyle.Flat
Me.btnReviewerAssessment.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
Me.btnReviewerAssessment.ForeColor = System.Drawing.Color.Black
Me.btnReviewerAssessment.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
Me.btnReviewerAssessment.GradientForeColor = System.Drawing.Color.Black
Me.btnReviewerAssessment.HoverGradientBackColor = System.Drawing.Color.Transparent
Me.btnReviewerAssessment.HoverGradientForeColor = System.Drawing.Color.Black
Me.btnReviewerAssessment.Image = Global.Aruti.Main.My.Resources.Resources.Reviewer_assess
Me.btnReviewerAssessment.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
Me.btnReviewerAssessment.Location = New System.Drawing.Point(3, 167)
Me.btnReviewerAssessment.MinimumSize = New System.Drawing.Size(150, 35)
Me.btnReviewerAssessment.Name = "btnReviewerAssessment"
Me.btnReviewerAssessment.PressedGradientBackColor = System.Drawing.Color.Transparent
Me.btnReviewerAssessment.PressedGradientForeColor = System.Drawing.Color.Black
Me.btnReviewerAssessment.Size = New System.Drawing.Size(166, 35)
Me.btnReviewerAssessment.TabIndex = 16
Me.btnReviewerAssessment.Text = "&Reviewer Assessment"
Me.btnReviewerAssessment.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
Me.btnReviewerAssessment.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText
Me.btnReviewerAssessment.UseVisualStyleBackColor = false
'
'fpnlDiscipline
'
Me.fpnlDiscipline.AutoScroll = true
Me.fpnlDiscipline.Controls.Add(Me.btnDisciplineType)
Me.fpnlDiscipline.Controls.Add(Me.btnDisciplinaryAction)
Me.fpnlDiscipline.Controls.Add(Me.btnDisciplineStatus)
Me.fpnlDiscipline.Controls.Add(Me.btnDisciplineFiling)
Me.fpnlDiscipline.Controls.Add(Me.btnResolutionSteps)
Me.fpnlDiscipline.Controls.Add(Me.btnCommittee)
Me.fpnlDiscipline.Controls.Add(Me.btnHearingList)
Me.fpnlDiscipline.Location = New System.Drawing.Point(200, 97)
Me.fpnlDiscipline.Name = "fpnlDiscipline"
Me.fpnlDiscipline.Size = New System.Drawing.Size(191, 41)
Me.fpnlDiscipline.TabIndex = 3
Me.fpnlDiscipline.Visible = false
'
'btnDisciplineType
'
Me.btnDisciplineType.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left)  _
            Or System.Windows.Forms.AnchorStyles.Right),System.Windows.Forms.AnchorStyles)
Me.btnDisciplineType.BackColor = System.Drawing.Color.Transparent
Me.btnDisciplineType.BackgroundImage = CType(resources.GetObject("btnDisciplineType.BackgroundImage"),System.Drawing.Image)
Me.btnDisciplineType.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
Me.btnDisciplineType.BorderColor = System.Drawing.Color.Empty
Me.btnDisciplineType.ButtonType = eZee.Common.eZeeLightButton.enButtonType.CheckButton
Me.btnDisciplineType.FlatAppearance.BorderSize = 0
Me.btnDisciplineType.FlatStyle = System.Windows.Forms.FlatStyle.Flat
Me.btnDisciplineType.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
Me.btnDisciplineType.ForeColor = System.Drawing.Color.Black
Me.btnDisciplineType.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
Me.btnDisciplineType.GradientForeColor = System.Drawing.Color.Black
Me.btnDisciplineType.HoverGradientBackColor = System.Drawing.Color.Transparent
Me.btnDisciplineType.HoverGradientForeColor = System.Drawing.Color.Black
Me.btnDisciplineType.Image = Global.Aruti.Main.My.Resources.Resources.discipline_types
Me.btnDisciplineType.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
Me.btnDisciplineType.Location = New System.Drawing.Point(3, 3)
Me.btnDisciplineType.MinimumSize = New System.Drawing.Size(150, 35)
Me.btnDisciplineType.Name = "btnDisciplineType"
Me.btnDisciplineType.PressedGradientBackColor = System.Drawing.Color.Transparent
Me.btnDisciplineType.PressedGradientForeColor = System.Drawing.Color.Black
Me.btnDisciplineType.Size = New System.Drawing.Size(166, 35)
Me.btnDisciplineType.TabIndex = 5
Me.btnDisciplineType.Text = "Disciplinary &Offences"
Me.btnDisciplineType.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
Me.btnDisciplineType.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText
Me.btnDisciplineType.UseVisualStyleBackColor = false
'
'btnDisciplinaryAction
'
Me.btnDisciplinaryAction.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left)  _
            Or System.Windows.Forms.AnchorStyles.Right),System.Windows.Forms.AnchorStyles)
Me.btnDisciplinaryAction.BackColor = System.Drawing.Color.Transparent
Me.btnDisciplinaryAction.BackgroundImage = CType(resources.GetObject("btnDisciplinaryAction.BackgroundImage"),System.Drawing.Image)
Me.btnDisciplinaryAction.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
Me.btnDisciplinaryAction.BorderColor = System.Drawing.Color.Empty
Me.btnDisciplinaryAction.ButtonType = eZee.Common.eZeeLightButton.enButtonType.CheckButton
Me.btnDisciplinaryAction.FlatAppearance.BorderSize = 0
Me.btnDisciplinaryAction.FlatStyle = System.Windows.Forms.FlatStyle.Flat
Me.btnDisciplinaryAction.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
Me.btnDisciplinaryAction.ForeColor = System.Drawing.Color.Black
Me.btnDisciplinaryAction.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
Me.btnDisciplinaryAction.GradientForeColor = System.Drawing.Color.Black
Me.btnDisciplinaryAction.HoverGradientBackColor = System.Drawing.Color.Transparent
Me.btnDisciplinaryAction.HoverGradientForeColor = System.Drawing.Color.Black
Me.btnDisciplinaryAction.Image = Global.Aruti.Main.My.Resources.Resources.discipline_action
Me.btnDisciplinaryAction.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
Me.btnDisciplinaryAction.Location = New System.Drawing.Point(3, 44)
Me.btnDisciplinaryAction.MinimumSize = New System.Drawing.Size(150, 35)
Me.btnDisciplinaryAction.Name = "btnDisciplinaryAction"
Me.btnDisciplinaryAction.PressedGradientBackColor = System.Drawing.Color.Transparent
Me.btnDisciplinaryAction.PressedGradientForeColor = System.Drawing.Color.Black
Me.btnDisciplinaryAction.Size = New System.Drawing.Size(166, 35)
Me.btnDisciplinaryAction.TabIndex = 6
Me.btnDisciplinaryAction.Text = "Disciplinary &Penalties"
Me.btnDisciplinaryAction.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
Me.btnDisciplinaryAction.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText
Me.btnDisciplinaryAction.UseVisualStyleBackColor = false
'
'btnDisciplineStatus
'
Me.btnDisciplineStatus.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left)  _
            Or System.Windows.Forms.AnchorStyles.Right),System.Windows.Forms.AnchorStyles)
Me.btnDisciplineStatus.BackColor = System.Drawing.Color.Transparent
Me.btnDisciplineStatus.BackgroundImage = CType(resources.GetObject("btnDisciplineStatus.BackgroundImage"),System.Drawing.Image)
Me.btnDisciplineStatus.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
Me.btnDisciplineStatus.BorderColor = System.Drawing.Color.Empty
Me.btnDisciplineStatus.ButtonType = eZee.Common.eZeeLightButton.enButtonType.CheckButton
Me.btnDisciplineStatus.FlatAppearance.BorderSize = 0
Me.btnDisciplineStatus.FlatStyle = System.Windows.Forms.FlatStyle.Flat
Me.btnDisciplineStatus.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
Me.btnDisciplineStatus.ForeColor = System.Drawing.Color.Black
Me.btnDisciplineStatus.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
Me.btnDisciplineStatus.GradientForeColor = System.Drawing.Color.Black
Me.btnDisciplineStatus.HoverGradientBackColor = System.Drawing.Color.Transparent
Me.btnDisciplineStatus.HoverGradientForeColor = System.Drawing.Color.Black
Me.btnDisciplineStatus.Image = Global.Aruti.Main.My.Resources.Resources.discipline_status
Me.btnDisciplineStatus.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
Me.btnDisciplineStatus.Location = New System.Drawing.Point(3, 85)
Me.btnDisciplineStatus.MinimumSize = New System.Drawing.Size(150, 35)
Me.btnDisciplineStatus.Name = "btnDisciplineStatus"
Me.btnDisciplineStatus.PressedGradientBackColor = System.Drawing.Color.Transparent
Me.btnDisciplineStatus.PressedGradientForeColor = System.Drawing.Color.Black
Me.btnDisciplineStatus.Size = New System.Drawing.Size(166, 35)
Me.btnDisciplineStatus.TabIndex = 7
Me.btnDisciplineStatus.Text = "Disciplinary &Status"
Me.btnDisciplineStatus.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
Me.btnDisciplineStatus.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText
Me.btnDisciplineStatus.UseVisualStyleBackColor = false
'
'btnDisciplineFiling
'
Me.btnDisciplineFiling.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left)  _
            Or System.Windows.Forms.AnchorStyles.Right),System.Windows.Forms.AnchorStyles)
Me.btnDisciplineFiling.BackColor = System.Drawing.Color.Transparent
Me.btnDisciplineFiling.BackgroundImage = CType(resources.GetObject("btnDisciplineFiling.BackgroundImage"),System.Drawing.Image)
Me.btnDisciplineFiling.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
Me.btnDisciplineFiling.BorderColor = System.Drawing.Color.Empty
Me.btnDisciplineFiling.ButtonType = eZee.Common.eZeeLightButton.enButtonType.CheckButton
Me.btnDisciplineFiling.FlatAppearance.BorderSize = 0
Me.btnDisciplineFiling.FlatStyle = System.Windows.Forms.FlatStyle.Flat
Me.btnDisciplineFiling.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
Me.btnDisciplineFiling.ForeColor = System.Drawing.Color.Black
Me.btnDisciplineFiling.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
Me.btnDisciplineFiling.GradientForeColor = System.Drawing.Color.Black
Me.btnDisciplineFiling.HoverGradientBackColor = System.Drawing.Color.Transparent
Me.btnDisciplineFiling.HoverGradientForeColor = System.Drawing.Color.Black
Me.btnDisciplineFiling.Image = Global.Aruti.Main.My.Resources.Resources.Discipline_list
Me.btnDisciplineFiling.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
Me.btnDisciplineFiling.Location = New System.Drawing.Point(3, 126)
Me.btnDisciplineFiling.MinimumSize = New System.Drawing.Size(150, 35)
Me.btnDisciplineFiling.Name = "btnDisciplineFiling"
Me.btnDisciplineFiling.PressedGradientBackColor = System.Drawing.Color.Transparent
Me.btnDisciplineFiling.PressedGradientForeColor = System.Drawing.Color.Black
Me.btnDisciplineFiling.Size = New System.Drawing.Size(166, 35)
Me.btnDisciplineFiling.TabIndex = 8
Me.btnDisciplineFiling.Text = "Charges and Proceeding&s"
Me.btnDisciplineFiling.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
Me.btnDisciplineFiling.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText
Me.btnDisciplineFiling.UseVisualStyleBackColor = false
'
'btnResolutionSteps
'
Me.btnResolutionSteps.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left)  _
            Or System.Windows.Forms.AnchorStyles.Right),System.Windows.Forms.AnchorStyles)
Me.btnResolutionSteps.BackColor = System.Drawing.Color.Transparent
Me.btnResolutionSteps.BackgroundImage = CType(resources.GetObject("btnResolutionSteps.BackgroundImage"),System.Drawing.Image)
Me.btnResolutionSteps.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
Me.btnResolutionSteps.BorderColor = System.Drawing.Color.Empty
Me.btnResolutionSteps.ButtonType = eZee.Common.eZeeLightButton.enButtonType.CheckButton
Me.btnResolutionSteps.FlatAppearance.BorderSize = 0
Me.btnResolutionSteps.FlatStyle = System.Windows.Forms.FlatStyle.Flat
Me.btnResolutionSteps.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
Me.btnResolutionSteps.ForeColor = System.Drawing.Color.Black
Me.btnResolutionSteps.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
Me.btnResolutionSteps.GradientForeColor = System.Drawing.Color.Black
Me.btnResolutionSteps.HoverGradientBackColor = System.Drawing.Color.Transparent
Me.btnResolutionSteps.HoverGradientForeColor = System.Drawing.Color.Black
Me.btnResolutionSteps.Image = Global.Aruti.Main.My.Resources.Resources.discipline_types
Me.btnResolutionSteps.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
Me.btnResolutionSteps.Location = New System.Drawing.Point(3, 167)
Me.btnResolutionSteps.MinimumSize = New System.Drawing.Size(150, 35)
Me.btnResolutionSteps.Name = "btnResolutionSteps"
Me.btnResolutionSteps.PressedGradientBackColor = System.Drawing.Color.Transparent
Me.btnResolutionSteps.PressedGradientForeColor = System.Drawing.Color.Black
Me.btnResolutionSteps.Size = New System.Drawing.Size(166, 35)
Me.btnResolutionSteps.TabIndex = 10
Me.btnResolutionSteps.Text = "P&roceedings Approvals"
Me.btnResolutionSteps.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
Me.btnResolutionSteps.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText
Me.btnResolutionSteps.UseVisualStyleBackColor = false
'
'btnCommittee
'
Me.btnCommittee.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left)  _
            Or System.Windows.Forms.AnchorStyles.Right),System.Windows.Forms.AnchorStyles)
Me.btnCommittee.BackColor = System.Drawing.Color.Transparent
Me.btnCommittee.BackgroundImage = CType(resources.GetObject("btnCommittee.BackgroundImage"),System.Drawing.Image)
Me.btnCommittee.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
Me.btnCommittee.BorderColor = System.Drawing.Color.Empty
Me.btnCommittee.ButtonType = eZee.Common.eZeeLightButton.enButtonType.CheckButton
Me.btnCommittee.FlatAppearance.BorderSize = 0
Me.btnCommittee.FlatStyle = System.Windows.Forms.FlatStyle.Flat
Me.btnCommittee.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
Me.btnCommittee.ForeColor = System.Drawing.Color.Black
Me.btnCommittee.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
Me.btnCommittee.GradientForeColor = System.Drawing.Color.Black
Me.btnCommittee.HoverGradientBackColor = System.Drawing.Color.Transparent
Me.btnCommittee.HoverGradientForeColor = System.Drawing.Color.Black
Me.btnCommittee.Image = Global.Aruti.Main.My.Resources.Resources.committee
Me.btnCommittee.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
Me.btnCommittee.Location = New System.Drawing.Point(3, 208)
Me.btnCommittee.MinimumSize = New System.Drawing.Size(150, 35)
Me.btnCommittee.Name = "btnCommittee"
Me.btnCommittee.PressedGradientBackColor = System.Drawing.Color.Transparent
Me.btnCommittee.PressedGradientForeColor = System.Drawing.Color.Black
Me.btnCommittee.Size = New System.Drawing.Size(166, 35)
Me.btnCommittee.TabIndex = 11
Me.btnCommittee.Text = "Disciplinary &Committee"
Me.btnCommittee.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
Me.btnCommittee.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText
Me.btnCommittee.UseVisualStyleBackColor = false
'
'btnHearingList
'
Me.btnHearingList.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left)  _
            Or System.Windows.Forms.AnchorStyles.Right),System.Windows.Forms.AnchorStyles)
Me.btnHearingList.BackColor = System.Drawing.Color.Transparent
Me.btnHearingList.BackgroundImage = CType(resources.GetObject("btnHearingList.BackgroundImage"),System.Drawing.Image)
Me.btnHearingList.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
Me.btnHearingList.BorderColor = System.Drawing.Color.Empty
Me.btnHearingList.ButtonType = eZee.Common.eZeeLightButton.enButtonType.CheckButton
Me.btnHearingList.FlatAppearance.BorderSize = 0
Me.btnHearingList.FlatStyle = System.Windows.Forms.FlatStyle.Flat
Me.btnHearingList.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
Me.btnHearingList.ForeColor = System.Drawing.Color.Black
Me.btnHearingList.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
Me.btnHearingList.GradientForeColor = System.Drawing.Color.Black
Me.btnHearingList.HoverGradientBackColor = System.Drawing.Color.Transparent
Me.btnHearingList.HoverGradientForeColor = System.Drawing.Color.Black
Me.btnHearingList.Image = Global.Aruti.Main.My.Resources.Resources.discipline_status
Me.btnHearingList.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
Me.btnHearingList.Location = New System.Drawing.Point(3, 249)
Me.btnHearingList.MinimumSize = New System.Drawing.Size(150, 35)
Me.btnHearingList.Name = "btnHearingList"
Me.btnHearingList.PressedGradientBackColor = System.Drawing.Color.Transparent
Me.btnHearingList.PressedGradientForeColor = System.Drawing.Color.Black
Me.btnHearingList.Size = New System.Drawing.Size(166, 35)
Me.btnHearingList.TabIndex = 12
Me.btnHearingList.Text = "Discipline Hearing List"
Me.btnHearingList.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
Me.btnHearingList.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText
Me.btnHearingList.UseVisualStyleBackColor = false
'
'fpnlHRMaster
'
Me.fpnlHRMaster.AutoScroll = true
Me.fpnlHRMaster.Controls.Add(Me.btnReasons)
Me.fpnlHRMaster.Controls.Add(Me.btnSkillMaster)
Me.fpnlHRMaster.Controls.Add(Me.btnSkillExpertise)
Me.fpnlHRMaster.Controls.Add(Me.btnAdvertiseMaster)
Me.fpnlHRMaster.Controls.Add(Me.btnQualification)
Me.fpnlHRMaster.Controls.Add(Me.btnResultCode)
Me.fpnlHRMaster.Controls.Add(Me.btnBenefitPlan)
Me.fpnlHRMaster.Location = New System.Drawing.Point(397, 3)
Me.fpnlHRMaster.Name = "fpnlHRMaster"
Me.fpnlHRMaster.Size = New System.Drawing.Size(191, 202)
Me.fpnlHRMaster.TabIndex = 17
Me.fpnlHRMaster.Visible = false
'
'btnReasons
'
Me.btnReasons.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left)  _
            Or System.Windows.Forms.AnchorStyles.Right),System.Windows.Forms.AnchorStyles)
Me.btnReasons.BackColor = System.Drawing.Color.Transparent
Me.btnReasons.BackgroundImage = CType(resources.GetObject("btnReasons.BackgroundImage"),System.Drawing.Image)
Me.btnReasons.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
Me.btnReasons.BorderColor = System.Drawing.Color.Empty
Me.btnReasons.ButtonType = eZee.Common.eZeeLightButton.enButtonType.CheckButton
Me.btnReasons.FlatAppearance.BorderSize = 0
Me.btnReasons.FlatStyle = System.Windows.Forms.FlatStyle.Flat
Me.btnReasons.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
Me.btnReasons.ForeColor = System.Drawing.Color.Black
Me.btnReasons.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
Me.btnReasons.GradientForeColor = System.Drawing.Color.Black
Me.btnReasons.HoverGradientBackColor = System.Drawing.Color.Transparent
Me.btnReasons.HoverGradientForeColor = System.Drawing.Color.Black
Me.btnReasons.Image = Global.Aruti.Main.My.Resources.Resources.reson_list
Me.btnReasons.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
Me.btnReasons.Location = New System.Drawing.Point(3, 3)
Me.btnReasons.MinimumSize = New System.Drawing.Size(150, 35)
Me.btnReasons.Name = "btnReasons"
Me.btnReasons.PressedGradientBackColor = System.Drawing.Color.Transparent
Me.btnReasons.PressedGradientForeColor = System.Drawing.Color.Black
Me.btnReasons.Size = New System.Drawing.Size(166, 35)
Me.btnReasons.TabIndex = 6
Me.btnReasons.Text = "&Reason Master"
Me.btnReasons.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
Me.btnReasons.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText
Me.btnReasons.UseVisualStyleBackColor = false
Me.btnReasons.Visible = false
'
'btnSkillMaster
'
Me.btnSkillMaster.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left)  _
            Or System.Windows.Forms.AnchorStyles.Right),System.Windows.Forms.AnchorStyles)
Me.btnSkillMaster.BackColor = System.Drawing.Color.Transparent
Me.btnSkillMaster.BackgroundImage = CType(resources.GetObject("btnSkillMaster.BackgroundImage"),System.Drawing.Image)
Me.btnSkillMaster.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
Me.btnSkillMaster.BorderColor = System.Drawing.Color.Empty
Me.btnSkillMaster.ButtonType = eZee.Common.eZeeLightButton.enButtonType.CheckButton
Me.btnSkillMaster.FlatAppearance.BorderSize = 0
Me.btnSkillMaster.FlatStyle = System.Windows.Forms.FlatStyle.Flat
Me.btnSkillMaster.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
Me.btnSkillMaster.ForeColor = System.Drawing.Color.Black
Me.btnSkillMaster.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
Me.btnSkillMaster.GradientForeColor = System.Drawing.Color.Black
Me.btnSkillMaster.HoverGradientBackColor = System.Drawing.Color.Transparent
Me.btnSkillMaster.HoverGradientForeColor = System.Drawing.Color.Black
Me.btnSkillMaster.Image = Global.Aruti.Main.My.Resources.Resources.skill_master
Me.btnSkillMaster.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
Me.btnSkillMaster.Location = New System.Drawing.Point(3, 44)
Me.btnSkillMaster.MinimumSize = New System.Drawing.Size(150, 35)
Me.btnSkillMaster.Name = "btnSkillMaster"
Me.btnSkillMaster.PressedGradientBackColor = System.Drawing.Color.Transparent
Me.btnSkillMaster.PressedGradientForeColor = System.Drawing.Color.Black
Me.btnSkillMaster.Size = New System.Drawing.Size(166, 35)
Me.btnSkillMaster.TabIndex = 7
Me.btnSkillMaster.Text = "&Skill Master"
Me.btnSkillMaster.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
Me.btnSkillMaster.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText
Me.btnSkillMaster.UseVisualStyleBackColor = false
'
'btnSkillExpertise
'
Me.btnSkillExpertise.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left)  _
            Or System.Windows.Forms.AnchorStyles.Right),System.Windows.Forms.AnchorStyles)
Me.btnSkillExpertise.BackColor = System.Drawing.Color.Transparent
Me.btnSkillExpertise.BackgroundImage = CType(resources.GetObject("btnSkillExpertise.BackgroundImage"),System.Drawing.Image)
Me.btnSkillExpertise.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
Me.btnSkillExpertise.BorderColor = System.Drawing.Color.Empty
Me.btnSkillExpertise.ButtonType = eZee.Common.eZeeLightButton.enButtonType.CheckButton
Me.btnSkillExpertise.FlatAppearance.BorderSize = 0
Me.btnSkillExpertise.FlatStyle = System.Windows.Forms.FlatStyle.Flat
Me.btnSkillExpertise.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
Me.btnSkillExpertise.ForeColor = System.Drawing.Color.Black
Me.btnSkillExpertise.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
Me.btnSkillExpertise.GradientForeColor = System.Drawing.Color.Black
Me.btnSkillExpertise.HoverGradientBackColor = System.Drawing.Color.Transparent
Me.btnSkillExpertise.HoverGradientForeColor = System.Drawing.Color.Black
Me.btnSkillExpertise.Image = Global.Aruti.Main.My.Resources.Resources.skill_master
Me.btnSkillExpertise.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
Me.btnSkillExpertise.Location = New System.Drawing.Point(3, 85)
Me.btnSkillExpertise.MinimumSize = New System.Drawing.Size(150, 35)
Me.btnSkillExpertise.Name = "btnSkillExpertise"
Me.btnSkillExpertise.PressedGradientBackColor = System.Drawing.Color.Transparent
Me.btnSkillExpertise.PressedGradientForeColor = System.Drawing.Color.Black
Me.btnSkillExpertise.Size = New System.Drawing.Size(166, 35)
Me.btnSkillExpertise.TabIndex = 13
Me.btnSkillExpertise.Text = "Skill &Expertise"
Me.btnSkillExpertise.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
Me.btnSkillExpertise.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText
Me.btnSkillExpertise.UseVisualStyleBackColor = false
'
'btnAdvertiseMaster
'
Me.btnAdvertiseMaster.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left)  _
            Or System.Windows.Forms.AnchorStyles.Right),System.Windows.Forms.AnchorStyles)
Me.btnAdvertiseMaster.BackColor = System.Drawing.Color.Transparent
Me.btnAdvertiseMaster.BackgroundImage = CType(resources.GetObject("btnAdvertiseMaster.BackgroundImage"),System.Drawing.Image)
Me.btnAdvertiseMaster.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
Me.btnAdvertiseMaster.BorderColor = System.Drawing.Color.Empty
Me.btnAdvertiseMaster.ButtonType = eZee.Common.eZeeLightButton.enButtonType.CheckButton
Me.btnAdvertiseMaster.FlatAppearance.BorderSize = 0
Me.btnAdvertiseMaster.FlatStyle = System.Windows.Forms.FlatStyle.Flat
Me.btnAdvertiseMaster.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
Me.btnAdvertiseMaster.ForeColor = System.Drawing.Color.Black
Me.btnAdvertiseMaster.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
Me.btnAdvertiseMaster.GradientForeColor = System.Drawing.Color.Black
Me.btnAdvertiseMaster.HoverGradientBackColor = System.Drawing.Color.Transparent
Me.btnAdvertiseMaster.HoverGradientForeColor = System.Drawing.Color.Black
Me.btnAdvertiseMaster.Image = Global.Aruti.Main.My.Resources.Resources.advertise
Me.btnAdvertiseMaster.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
Me.btnAdvertiseMaster.Location = New System.Drawing.Point(3, 126)
Me.btnAdvertiseMaster.MinimumSize = New System.Drawing.Size(150, 35)
Me.btnAdvertiseMaster.Name = "btnAdvertiseMaster"
Me.btnAdvertiseMaster.PressedGradientBackColor = System.Drawing.Color.Transparent
Me.btnAdvertiseMaster.PressedGradientForeColor = System.Drawing.Color.Black
Me.btnAdvertiseMaster.Size = New System.Drawing.Size(166, 35)
Me.btnAdvertiseMaster.TabIndex = 8
Me.btnAdvertiseMaster.Text = "&Agency Master"
Me.btnAdvertiseMaster.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
Me.btnAdvertiseMaster.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText
Me.btnAdvertiseMaster.UseVisualStyleBackColor = false
'
'btnQualification
'
Me.btnQualification.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left)  _
            Or System.Windows.Forms.AnchorStyles.Right),System.Windows.Forms.AnchorStyles)
Me.btnQualification.BackColor = System.Drawing.Color.Transparent
Me.btnQualification.BackgroundImage = CType(resources.GetObject("btnQualification.BackgroundImage"),System.Drawing.Image)
Me.btnQualification.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
Me.btnQualification.BorderColor = System.Drawing.Color.Empty
Me.btnQualification.ButtonType = eZee.Common.eZeeLightButton.enButtonType.CheckButton
Me.btnQualification.FlatAppearance.BorderSize = 0
Me.btnQualification.FlatStyle = System.Windows.Forms.FlatStyle.Flat
Me.btnQualification.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
Me.btnQualification.ForeColor = System.Drawing.Color.Black
Me.btnQualification.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
Me.btnQualification.GradientForeColor = System.Drawing.Color.Black
Me.btnQualification.HoverGradientBackColor = System.Drawing.Color.Transparent
Me.btnQualification.HoverGradientForeColor = System.Drawing.Color.Black
Me.btnQualification.Image = Global.Aruti.Main.My.Resources.Resources.qualification
Me.btnQualification.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
Me.btnQualification.Location = New System.Drawing.Point(3, 167)
Me.btnQualification.MinimumSize = New System.Drawing.Size(150, 35)
Me.btnQualification.Name = "btnQualification"
Me.btnQualification.PressedGradientBackColor = System.Drawing.Color.Transparent
Me.btnQualification.PressedGradientForeColor = System.Drawing.Color.Black
Me.btnQualification.Size = New System.Drawing.Size(166, 35)
Me.btnQualification.TabIndex = 9
Me.btnQualification.Text = "&Qualification Master"
Me.btnQualification.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
Me.btnQualification.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText
Me.btnQualification.UseVisualStyleBackColor = false
'
'btnResultCode
'
Me.btnResultCode.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left)  _
            Or System.Windows.Forms.AnchorStyles.Right),System.Windows.Forms.AnchorStyles)
Me.btnResultCode.BackColor = System.Drawing.Color.Transparent
Me.btnResultCode.BackgroundImage = CType(resources.GetObject("btnResultCode.BackgroundImage"),System.Drawing.Image)
Me.btnResultCode.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
Me.btnResultCode.BorderColor = System.Drawing.Color.Empty
Me.btnResultCode.ButtonType = eZee.Common.eZeeLightButton.enButtonType.CheckButton
Me.btnResultCode.FlatAppearance.BorderSize = 0
Me.btnResultCode.FlatStyle = System.Windows.Forms.FlatStyle.Flat
Me.btnResultCode.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
Me.btnResultCode.ForeColor = System.Drawing.Color.Black
Me.btnResultCode.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
Me.btnResultCode.GradientForeColor = System.Drawing.Color.Black
Me.btnResultCode.HoverGradientBackColor = System.Drawing.Color.Transparent
Me.btnResultCode.HoverGradientForeColor = System.Drawing.Color.Black
Me.btnResultCode.Image = Global.Aruti.Main.My.Resources.Resources.result_code
Me.btnResultCode.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
Me.btnResultCode.Location = New System.Drawing.Point(3, 208)
Me.btnResultCode.MinimumSize = New System.Drawing.Size(150, 35)
Me.btnResultCode.Name = "btnResultCode"
Me.btnResultCode.PressedGradientBackColor = System.Drawing.Color.Transparent
Me.btnResultCode.PressedGradientForeColor = System.Drawing.Color.Black
Me.btnResultCode.Size = New System.Drawing.Size(166, 35)
Me.btnResultCode.TabIndex = 10
Me.btnResultCode.Text = "&Result Code"
Me.btnResultCode.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
Me.btnResultCode.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText
Me.btnResultCode.UseVisualStyleBackColor = false
'
'btnBenefitPlan
'
Me.btnBenefitPlan.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left)  _
            Or System.Windows.Forms.AnchorStyles.Right),System.Windows.Forms.AnchorStyles)
Me.btnBenefitPlan.BackColor = System.Drawing.Color.Transparent
Me.btnBenefitPlan.BackgroundImage = CType(resources.GetObject("btnBenefitPlan.BackgroundImage"),System.Drawing.Image)
Me.btnBenefitPlan.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
Me.btnBenefitPlan.BorderColor = System.Drawing.Color.Empty
Me.btnBenefitPlan.ButtonType = eZee.Common.eZeeLightButton.enButtonType.CheckButton
Me.btnBenefitPlan.FlatAppearance.BorderSize = 0
Me.btnBenefitPlan.FlatStyle = System.Windows.Forms.FlatStyle.Flat
Me.btnBenefitPlan.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
Me.btnBenefitPlan.ForeColor = System.Drawing.Color.Black
Me.btnBenefitPlan.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
Me.btnBenefitPlan.GradientForeColor = System.Drawing.Color.Black
Me.btnBenefitPlan.HoverGradientBackColor = System.Drawing.Color.Transparent
Me.btnBenefitPlan.HoverGradientForeColor = System.Drawing.Color.Black
Me.btnBenefitPlan.Image = Global.Aruti.Main.My.Resources.Resources.benefit_plan
Me.btnBenefitPlan.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
Me.btnBenefitPlan.Location = New System.Drawing.Point(3, 249)
Me.btnBenefitPlan.MinimumSize = New System.Drawing.Size(150, 35)
Me.btnBenefitPlan.Name = "btnBenefitPlan"
Me.btnBenefitPlan.PressedGradientBackColor = System.Drawing.Color.Transparent
Me.btnBenefitPlan.PressedGradientForeColor = System.Drawing.Color.Black
Me.btnBenefitPlan.Size = New System.Drawing.Size(166, 35)
Me.btnBenefitPlan.TabIndex = 12
Me.btnBenefitPlan.Text = "&Benefit Plan"
Me.btnBenefitPlan.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
Me.btnBenefitPlan.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText
Me.btnBenefitPlan.UseVisualStyleBackColor = false
'
'fpnlLoanInfo
'
Me.fpnlLoanInfo.AutoScroll = true
Me.fpnlLoanInfo.Controls.Add(Me.btnLoanScheme)
Me.fpnlLoanInfo.Controls.Add(Me.btnLoanApproverLevel)
Me.fpnlLoanInfo.Controls.Add(Me.btnLoanApprover)
Me.fpnlLoanInfo.Controls.Add(Me.btnLoanApplication)
        Me.fpnlLoanInfo.Controls.Add(Me.btnFlexcubeLoanApplication)
Me.fpnlLoanInfo.Controls.Add(Me.btnProcessPendingLoan)
Me.fpnlLoanInfo.Controls.Add(Me.btnLoanAdvance)
Me.fpnlLoanInfo.Controls.Add(Me.btnImportLA)
Me.fpnlLoanInfo.Controls.Add(Me.btnLoanApproverMigration)
Me.fpnlLoanInfo.Controls.Add(Me.btnLoanSwapApprover)
        Me.fpnlLoanInfo.Controls.Add(Me.btnLoanSchemeCategoryMapping)
Me.fpnlLoanInfo.Controls.Add(Me.objelLine5)
Me.fpnlLoanInfo.Controls.Add(Me.btnSavingScheme)
Me.fpnlLoanInfo.Controls.Add(Me.btnEmployeeSavings)
Me.fpnlLoanInfo.Controls.Add(Me.btnImportSaving)
Me.fpnlLoanInfo.Location = New System.Drawing.Point(594, 3)
Me.fpnlLoanInfo.Name = "fpnlLoanInfo"
Me.fpnlLoanInfo.Size = New System.Drawing.Size(189, 41)
Me.fpnlLoanInfo.TabIndex = 14
Me.fpnlLoanInfo.Visible = false
'
'btnLoanScheme
'
Me.btnLoanScheme.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left)  _
            Or System.Windows.Forms.AnchorStyles.Right),System.Windows.Forms.AnchorStyles)
Me.btnLoanScheme.BackColor = System.Drawing.Color.Transparent
Me.btnLoanScheme.BackgroundImage = CType(resources.GetObject("btnLoanScheme.BackgroundImage"),System.Drawing.Image)
Me.btnLoanScheme.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
Me.btnLoanScheme.BorderColor = System.Drawing.Color.Empty
Me.btnLoanScheme.ButtonType = eZee.Common.eZeeLightButton.enButtonType.CheckButton
Me.btnLoanScheme.FlatAppearance.BorderSize = 0
Me.btnLoanScheme.FlatStyle = System.Windows.Forms.FlatStyle.Flat
Me.btnLoanScheme.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
Me.btnLoanScheme.ForeColor = System.Drawing.Color.Black
Me.btnLoanScheme.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
Me.btnLoanScheme.GradientForeColor = System.Drawing.Color.Black
Me.btnLoanScheme.HoverGradientBackColor = System.Drawing.Color.Transparent
Me.btnLoanScheme.HoverGradientForeColor = System.Drawing.Color.Black
Me.btnLoanScheme.Image = Global.Aruti.Main.My.Resources.Resources.Loan_Scheme
Me.btnLoanScheme.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
Me.btnLoanScheme.Location = New System.Drawing.Point(3, 3)
Me.btnLoanScheme.MinimumSize = New System.Drawing.Size(150, 35)
Me.btnLoanScheme.Name = "btnLoanScheme"
Me.btnLoanScheme.PressedGradientBackColor = System.Drawing.Color.Transparent
Me.btnLoanScheme.PressedGradientForeColor = System.Drawing.Color.Black
Me.btnLoanScheme.Size = New System.Drawing.Size(166, 35)
Me.btnLoanScheme.TabIndex = 6
Me.btnLoanScheme.Text = "L&oan Scheme"
Me.btnLoanScheme.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
Me.btnLoanScheme.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText
Me.btnLoanScheme.UseVisualStyleBackColor = false
'
'btnLoanApproverLevel
'
Me.btnLoanApproverLevel.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left)  _
            Or System.Windows.Forms.AnchorStyles.Right),System.Windows.Forms.AnchorStyles)
Me.btnLoanApproverLevel.BackColor = System.Drawing.Color.Transparent
Me.btnLoanApproverLevel.BackgroundImage = CType(resources.GetObject("btnLoanApproverLevel.BackgroundImage"),System.Drawing.Image)
Me.btnLoanApproverLevel.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
Me.btnLoanApproverLevel.BorderColor = System.Drawing.Color.Empty
Me.btnLoanApproverLevel.ButtonType = eZee.Common.eZeeLightButton.enButtonType.CheckButton
Me.btnLoanApproverLevel.FlatAppearance.BorderSize = 0
Me.btnLoanApproverLevel.FlatStyle = System.Windows.Forms.FlatStyle.Flat
Me.btnLoanApproverLevel.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
Me.btnLoanApproverLevel.ForeColor = System.Drawing.Color.Black
Me.btnLoanApproverLevel.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
Me.btnLoanApproverLevel.GradientForeColor = System.Drawing.Color.Black
Me.btnLoanApproverLevel.HoverGradientBackColor = System.Drawing.Color.Transparent
Me.btnLoanApproverLevel.HoverGradientForeColor = System.Drawing.Color.Black
Me.btnLoanApproverLevel.Image = Global.Aruti.Main.My.Resources.Resources.Approver_Level
Me.btnLoanApproverLevel.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
Me.btnLoanApproverLevel.Location = New System.Drawing.Point(3, 44)
Me.btnLoanApproverLevel.MinimumSize = New System.Drawing.Size(150, 35)
Me.btnLoanApproverLevel.Name = "btnLoanApproverLevel"
Me.btnLoanApproverLevel.PressedGradientBackColor = System.Drawing.Color.Transparent
Me.btnLoanApproverLevel.PressedGradientForeColor = System.Drawing.Color.Black
Me.btnLoanApproverLevel.Size = New System.Drawing.Size(166, 35)
Me.btnLoanApproverLevel.TabIndex = 11
Me.btnLoanApproverLevel.Text = "&Approver Level"
Me.btnLoanApproverLevel.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
Me.btnLoanApproverLevel.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText
Me.btnLoanApproverLevel.UseVisualStyleBackColor = false
'
'btnLoanApprover
'
Me.btnLoanApprover.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left)  _
            Or System.Windows.Forms.AnchorStyles.Right),System.Windows.Forms.AnchorStyles)
Me.btnLoanApprover.BackColor = System.Drawing.Color.Transparent
Me.btnLoanApprover.BackgroundImage = CType(resources.GetObject("btnLoanApprover.BackgroundImage"),System.Drawing.Image)
Me.btnLoanApprover.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
Me.btnLoanApprover.BorderColor = System.Drawing.Color.Empty
Me.btnLoanApprover.ButtonType = eZee.Common.eZeeLightButton.enButtonType.CheckButton
Me.btnLoanApprover.FlatAppearance.BorderSize = 0
Me.btnLoanApprover.FlatStyle = System.Windows.Forms.FlatStyle.Flat
Me.btnLoanApprover.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
Me.btnLoanApprover.ForeColor = System.Drawing.Color.Black
Me.btnLoanApprover.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
Me.btnLoanApprover.GradientForeColor = System.Drawing.Color.Black
Me.btnLoanApprover.HoverGradientBackColor = System.Drawing.Color.Transparent
Me.btnLoanApprover.HoverGradientForeColor = System.Drawing.Color.Black
Me.btnLoanApprover.Image = Global.Aruti.Main.My.Resources.Resources.Loan_Approver
Me.btnLoanApprover.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
Me.btnLoanApprover.Location = New System.Drawing.Point(3, 85)
Me.btnLoanApprover.MinimumSize = New System.Drawing.Size(150, 35)
Me.btnLoanApprover.Name = "btnLoanApprover"
Me.btnLoanApprover.PressedGradientBackColor = System.Drawing.Color.Transparent
Me.btnLoanApprover.PressedGradientForeColor = System.Drawing.Color.Black
Me.btnLoanApprover.Size = New System.Drawing.Size(166, 36)
Me.btnLoanApprover.TabIndex = 36
Me.btnLoanApprover.Text = "Loan &Approver"
Me.btnLoanApprover.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
Me.btnLoanApprover.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText
Me.btnLoanApprover.UseVisualStyleBackColor = false
'
'btnLoanApplication
'
Me.btnLoanApplication.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left)  _
            Or System.Windows.Forms.AnchorStyles.Right),System.Windows.Forms.AnchorStyles)
Me.btnLoanApplication.BackColor = System.Drawing.Color.Transparent
Me.btnLoanApplication.BackgroundImage = CType(resources.GetObject("btnLoanApplication.BackgroundImage"),System.Drawing.Image)
Me.btnLoanApplication.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
Me.btnLoanApplication.BorderColor = System.Drawing.Color.Empty
Me.btnLoanApplication.ButtonType = eZee.Common.eZeeLightButton.enButtonType.CheckButton
Me.btnLoanApplication.FlatAppearance.BorderSize = 0
Me.btnLoanApplication.FlatStyle = System.Windows.Forms.FlatStyle.Flat
Me.btnLoanApplication.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
Me.btnLoanApplication.ForeColor = System.Drawing.Color.Black
Me.btnLoanApplication.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
Me.btnLoanApplication.GradientForeColor = System.Drawing.Color.Black
Me.btnLoanApplication.HoverGradientBackColor = System.Drawing.Color.Transparent
Me.btnLoanApplication.HoverGradientForeColor = System.Drawing.Color.Black
Me.btnLoanApplication.Image = Global.Aruti.Main.My.Resources.Resources.ApproverUserMapping
Me.btnLoanApplication.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
Me.btnLoanApplication.Location = New System.Drawing.Point(3, 127)
Me.btnLoanApplication.MinimumSize = New System.Drawing.Size(150, 35)
Me.btnLoanApplication.Name = "btnLoanApplication"
Me.btnLoanApplication.PressedGradientBackColor = System.Drawing.Color.Transparent
Me.btnLoanApplication.PressedGradientForeColor = System.Drawing.Color.Black
Me.btnLoanApplication.Size = New System.Drawing.Size(166, 36)
Me.btnLoanApplication.TabIndex = 38
Me.btnLoanApplication.Text = "Loan Application"
Me.btnLoanApplication.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
Me.btnLoanApplication.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText
Me.btnLoanApplication.UseVisualStyleBackColor = false
'
        'btnFlexcubeLoanApplication
        '
        Me.btnFlexcubeLoanApplication.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
                    Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnFlexcubeLoanApplication.BackColor = System.Drawing.Color.Transparent
        Me.btnFlexcubeLoanApplication.BackgroundImage = CType(resources.GetObject("btnFlexcubeLoanApplication.BackgroundImage"), System.Drawing.Image)
        Me.btnFlexcubeLoanApplication.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.btnFlexcubeLoanApplication.BorderColor = System.Drawing.Color.Empty
        Me.btnFlexcubeLoanApplication.ButtonType = eZee.Common.eZeeLightButton.enButtonType.CheckButton
        Me.btnFlexcubeLoanApplication.FlatAppearance.BorderSize = 0
        Me.btnFlexcubeLoanApplication.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnFlexcubeLoanApplication.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnFlexcubeLoanApplication.ForeColor = System.Drawing.Color.Black
        Me.btnFlexcubeLoanApplication.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.btnFlexcubeLoanApplication.GradientForeColor = System.Drawing.Color.Black
        Me.btnFlexcubeLoanApplication.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.btnFlexcubeLoanApplication.HoverGradientForeColor = System.Drawing.Color.Black
        Me.btnFlexcubeLoanApplication.Image = Global.Aruti.Main.My.Resources.Resources.ApproverUserMapping
        Me.btnFlexcubeLoanApplication.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
Me.btnFlexcubeLoanApplication.Location = New System.Drawing.Point(3, 169)
        Me.btnFlexcubeLoanApplication.MinimumSize = New System.Drawing.Size(150, 35)
        Me.btnFlexcubeLoanApplication.Name = "btnFlexcubeLoanApplication"
        Me.btnFlexcubeLoanApplication.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.btnFlexcubeLoanApplication.PressedGradientForeColor = System.Drawing.Color.Black
        Me.btnFlexcubeLoanApplication.Size = New System.Drawing.Size(166, 36)
        Me.btnFlexcubeLoanApplication.TabIndex = 38
        Me.btnFlexcubeLoanApplication.Text = "Flexcube Loan"
        Me.btnFlexcubeLoanApplication.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.btnFlexcubeLoanApplication.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText
        Me.btnFlexcubeLoanApplication.UseVisualStyleBackColor = False
'
'btnProcessPendingLoan
'
Me.btnProcessPendingLoan.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left)  _
            Or System.Windows.Forms.AnchorStyles.Right),System.Windows.Forms.AnchorStyles)
Me.btnProcessPendingLoan.BackColor = System.Drawing.Color.Transparent
Me.btnProcessPendingLoan.BackgroundImage = CType(resources.GetObject("btnProcessPendingLoan.BackgroundImage"),System.Drawing.Image)
Me.btnProcessPendingLoan.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
Me.btnProcessPendingLoan.BorderColor = System.Drawing.Color.Empty
Me.btnProcessPendingLoan.ButtonType = eZee.Common.eZeeLightButton.enButtonType.CheckButton
Me.btnProcessPendingLoan.FlatAppearance.BorderSize = 0
Me.btnProcessPendingLoan.FlatStyle = System.Windows.Forms.FlatStyle.Flat
Me.btnProcessPendingLoan.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
Me.btnProcessPendingLoan.ForeColor = System.Drawing.Color.Black
Me.btnProcessPendingLoan.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
Me.btnProcessPendingLoan.GradientForeColor = System.Drawing.Color.Black
Me.btnProcessPendingLoan.HoverGradientBackColor = System.Drawing.Color.Transparent
Me.btnProcessPendingLoan.HoverGradientForeColor = System.Drawing.Color.Black
Me.btnProcessPendingLoan.Image = Global.Aruti.Main.My.Resources.Resources.process_pending_loan
Me.btnProcessPendingLoan.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
Me.btnProcessPendingLoan.Location = New System.Drawing.Point(3, 211)
Me.btnProcessPendingLoan.MinimumSize = New System.Drawing.Size(150, 35)
Me.btnProcessPendingLoan.Name = "btnProcessPendingLoan"
Me.btnProcessPendingLoan.PressedGradientBackColor = System.Drawing.Color.Transparent
Me.btnProcessPendingLoan.PressedGradientForeColor = System.Drawing.Color.Black
Me.btnProcessPendingLoan.Size = New System.Drawing.Size(166, 36)
Me.btnProcessPendingLoan.TabIndex = 36
Me.btnProcessPendingLoan.Text = "&Process Loan/Advance"
Me.btnProcessPendingLoan.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
Me.btnProcessPendingLoan.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText
Me.btnProcessPendingLoan.UseVisualStyleBackColor = false
'
'btnLoanAdvance
'
Me.btnLoanAdvance.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left)  _
            Or System.Windows.Forms.AnchorStyles.Right),System.Windows.Forms.AnchorStyles)
Me.btnLoanAdvance.BackColor = System.Drawing.Color.Transparent
Me.btnLoanAdvance.BackgroundImage = CType(resources.GetObject("btnLoanAdvance.BackgroundImage"),System.Drawing.Image)
Me.btnLoanAdvance.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
Me.btnLoanAdvance.BorderColor = System.Drawing.Color.Empty
Me.btnLoanAdvance.ButtonType = eZee.Common.eZeeLightButton.enButtonType.CheckButton
Me.btnLoanAdvance.FlatAppearance.BorderSize = 0
Me.btnLoanAdvance.FlatStyle = System.Windows.Forms.FlatStyle.Flat
Me.btnLoanAdvance.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
Me.btnLoanAdvance.ForeColor = System.Drawing.Color.Black
Me.btnLoanAdvance.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
Me.btnLoanAdvance.GradientForeColor = System.Drawing.Color.Black
Me.btnLoanAdvance.HoverGradientBackColor = System.Drawing.Color.Transparent
Me.btnLoanAdvance.HoverGradientForeColor = System.Drawing.Color.Black
Me.btnLoanAdvance.Image = Global.Aruti.Main.My.Resources.Resources.Loan_Advance
Me.btnLoanAdvance.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
Me.btnLoanAdvance.Location = New System.Drawing.Point(3, 253)
Me.btnLoanAdvance.MinimumSize = New System.Drawing.Size(150, 35)
Me.btnLoanAdvance.Name = "btnLoanAdvance"
Me.btnLoanAdvance.PressedGradientBackColor = System.Drawing.Color.Transparent
Me.btnLoanAdvance.PressedGradientForeColor = System.Drawing.Color.Black
Me.btnLoanAdvance.Size = New System.Drawing.Size(166, 35)
Me.btnLoanAdvance.TabIndex = 37
Me.btnLoanAdvance.Text = "&Loan/Advance"
Me.btnLoanAdvance.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
Me.btnLoanAdvance.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText
Me.btnLoanAdvance.UseVisualStyleBackColor = false
'
'btnImportLA
'
Me.btnImportLA.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left)  _
            Or System.Windows.Forms.AnchorStyles.Right),System.Windows.Forms.AnchorStyles)
Me.btnImportLA.BackColor = System.Drawing.Color.Transparent
Me.btnImportLA.BackgroundImage = CType(resources.GetObject("btnImportLA.BackgroundImage"),System.Drawing.Image)
Me.btnImportLA.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
Me.btnImportLA.BorderColor = System.Drawing.Color.Empty
Me.btnImportLA.ButtonType = eZee.Common.eZeeLightButton.enButtonType.CheckButton
Me.btnImportLA.FlatAppearance.BorderSize = 0
Me.btnImportLA.FlatStyle = System.Windows.Forms.FlatStyle.Flat
Me.btnImportLA.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
Me.btnImportLA.ForeColor = System.Drawing.Color.Black
Me.btnImportLA.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
Me.btnImportLA.GradientForeColor = System.Drawing.Color.Black
Me.btnImportLA.HoverGradientBackColor = System.Drawing.Color.Transparent
Me.btnImportLA.HoverGradientForeColor = System.Drawing.Color.Black
Me.btnImportLA.Image = Global.Aruti.Main.My.Resources.Resources.Import_Attendence
Me.btnImportLA.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
Me.btnImportLA.Location = New System.Drawing.Point(3, 294)
Me.btnImportLA.MinimumSize = New System.Drawing.Size(150, 35)
Me.btnImportLA.Name = "btnImportLA"
Me.btnImportLA.PressedGradientBackColor = System.Drawing.Color.Transparent
Me.btnImportLA.PressedGradientForeColor = System.Drawing.Color.Black
Me.btnImportLA.Size = New System.Drawing.Size(166, 35)
Me.btnImportLA.TabIndex = 39
Me.btnImportLA.Text = "&Import Loan/Advance"
Me.btnImportLA.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
Me.btnImportLA.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText
Me.btnImportLA.UseVisualStyleBackColor = false
'
'btnLoanApproverMigration
'
Me.btnLoanApproverMigration.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left)  _
            Or System.Windows.Forms.AnchorStyles.Right),System.Windows.Forms.AnchorStyles)
Me.btnLoanApproverMigration.BackColor = System.Drawing.Color.Transparent
Me.btnLoanApproverMigration.BackgroundImage = CType(resources.GetObject("btnLoanApproverMigration.BackgroundImage"),System.Drawing.Image)
Me.btnLoanApproverMigration.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
Me.btnLoanApproverMigration.BorderColor = System.Drawing.Color.Empty
Me.btnLoanApproverMigration.ButtonType = eZee.Common.eZeeLightButton.enButtonType.CheckButton
Me.btnLoanApproverMigration.FlatAppearance.BorderSize = 0
Me.btnLoanApproverMigration.FlatStyle = System.Windows.Forms.FlatStyle.Flat
Me.btnLoanApproverMigration.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
Me.btnLoanApproverMigration.ForeColor = System.Drawing.Color.Black
Me.btnLoanApproverMigration.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
Me.btnLoanApproverMigration.GradientForeColor = System.Drawing.Color.Black
Me.btnLoanApproverMigration.HoverGradientBackColor = System.Drawing.Color.Transparent
Me.btnLoanApproverMigration.HoverGradientForeColor = System.Drawing.Color.Black
Me.btnLoanApproverMigration.Image = Global.Aruti.Main.My.Resources.Resources.Import_Attendence
Me.btnLoanApproverMigration.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
Me.btnLoanApproverMigration.Location = New System.Drawing.Point(3, 335)
Me.btnLoanApproverMigration.MinimumSize = New System.Drawing.Size(150, 35)
Me.btnLoanApproverMigration.Name = "btnLoanApproverMigration"
Me.btnLoanApproverMigration.PressedGradientBackColor = System.Drawing.Color.Transparent
Me.btnLoanApproverMigration.PressedGradientForeColor = System.Drawing.Color.Black
Me.btnLoanApproverMigration.Size = New System.Drawing.Size(166, 35)
Me.btnLoanApproverMigration.TabIndex = 40
Me.btnLoanApproverMigration.Text = "Transfer Approver"
Me.btnLoanApproverMigration.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
Me.btnLoanApproverMigration.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText
Me.btnLoanApproverMigration.UseVisualStyleBackColor = false
'
'btnLoanSwapApprover
'
Me.btnLoanSwapApprover.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left)  _
            Or System.Windows.Forms.AnchorStyles.Right),System.Windows.Forms.AnchorStyles)
Me.btnLoanSwapApprover.BackColor = System.Drawing.Color.Transparent
Me.btnLoanSwapApprover.BackgroundImage = CType(resources.GetObject("btnLoanSwapApprover.BackgroundImage"),System.Drawing.Image)
Me.btnLoanSwapApprover.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
Me.btnLoanSwapApprover.BorderColor = System.Drawing.Color.Empty
Me.btnLoanSwapApprover.ButtonType = eZee.Common.eZeeLightButton.enButtonType.CheckButton
Me.btnLoanSwapApprover.FlatAppearance.BorderSize = 0
Me.btnLoanSwapApprover.FlatStyle = System.Windows.Forms.FlatStyle.Flat
Me.btnLoanSwapApprover.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
Me.btnLoanSwapApprover.ForeColor = System.Drawing.Color.Black
Me.btnLoanSwapApprover.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
Me.btnLoanSwapApprover.GradientForeColor = System.Drawing.Color.Black
Me.btnLoanSwapApprover.HoverGradientBackColor = System.Drawing.Color.Transparent
Me.btnLoanSwapApprover.HoverGradientForeColor = System.Drawing.Color.Black
Me.btnLoanSwapApprover.Image = Global.Aruti.Main.My.Resources.Resources.Approver_Swapping
Me.btnLoanSwapApprover.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
Me.btnLoanSwapApprover.Location = New System.Drawing.Point(3, 376)
Me.btnLoanSwapApprover.MinimumSize = New System.Drawing.Size(150, 35)
Me.btnLoanSwapApprover.Name = "btnLoanSwapApprover"
Me.btnLoanSwapApprover.PressedGradientBackColor = System.Drawing.Color.Transparent
Me.btnLoanSwapApprover.PressedGradientForeColor = System.Drawing.Color.Black
Me.btnLoanSwapApprover.Size = New System.Drawing.Size(166, 36)
Me.btnLoanSwapApprover.TabIndex = 41
Me.btnLoanSwapApprover.Text = "Swap Approver"
Me.btnLoanSwapApprover.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
Me.btnLoanSwapApprover.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText
Me.btnLoanSwapApprover.UseVisualStyleBackColor = false
'
        'btnLoanSchemeCategoryMapping
        '
        Me.btnLoanSchemeCategoryMapping.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
                    Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnLoanSchemeCategoryMapping.BackColor = System.Drawing.Color.Transparent
        Me.btnLoanSchemeCategoryMapping.BackgroundImage = CType(resources.GetObject("btnLoanSchemeCategoryMapping.BackgroundImage"), System.Drawing.Image)
        Me.btnLoanSchemeCategoryMapping.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.btnLoanSchemeCategoryMapping.BorderColor = System.Drawing.Color.Empty
        Me.btnLoanSchemeCategoryMapping.ButtonType = eZee.Common.eZeeLightButton.enButtonType.CheckButton
        Me.btnLoanSchemeCategoryMapping.FlatAppearance.BorderSize = 0
        Me.btnLoanSchemeCategoryMapping.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnLoanSchemeCategoryMapping.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnLoanSchemeCategoryMapping.ForeColor = System.Drawing.Color.Black
        Me.btnLoanSchemeCategoryMapping.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.btnLoanSchemeCategoryMapping.GradientForeColor = System.Drawing.Color.Black
        Me.btnLoanSchemeCategoryMapping.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.btnLoanSchemeCategoryMapping.HoverGradientForeColor = System.Drawing.Color.Black
        Me.btnLoanSchemeCategoryMapping.Image = Global.Aruti.Main.My.Resources.Resources.AllocationMapping
        Me.btnLoanSchemeCategoryMapping.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
Me.btnLoanSchemeCategoryMapping.Location = New System.Drawing.Point(3, 418)
        Me.btnLoanSchemeCategoryMapping.MinimumSize = New System.Drawing.Size(150, 35)
        Me.btnLoanSchemeCategoryMapping.Name = "btnLoanSchemeCategoryMapping"
        Me.btnLoanSchemeCategoryMapping.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.btnLoanSchemeCategoryMapping.PressedGradientForeColor = System.Drawing.Color.Black
        Me.btnLoanSchemeCategoryMapping.Size = New System.Drawing.Size(166, 36)
        Me.btnLoanSchemeCategoryMapping.TabIndex = 41
        Me.btnLoanSchemeCategoryMapping.Text = "Scheme Category Mapping"
        Me.btnLoanSchemeCategoryMapping.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.btnLoanSchemeCategoryMapping.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText
        Me.btnLoanSchemeCategoryMapping.UseVisualStyleBackColor = False
'
'objelLine5
'
Me.objelLine5.LineOrientation = eZee.Common.eZeeLine.Orientation.Horizontal
Me.objelLine5.Location = New System.Drawing.Point(3, 457)
Me.objelLine5.Name = "objelLine5"
Me.objelLine5.Size = New System.Drawing.Size(166, 17)
Me.objelLine5.TabIndex = 16
Me.objelLine5.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
'
'btnSavingScheme
'
Me.btnSavingScheme.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left)  _
            Or System.Windows.Forms.AnchorStyles.Right),System.Windows.Forms.AnchorStyles)
Me.btnSavingScheme.BackColor = System.Drawing.Color.Transparent
Me.btnSavingScheme.BackgroundImage = CType(resources.GetObject("btnSavingScheme.BackgroundImage"),System.Drawing.Image)
Me.btnSavingScheme.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
Me.btnSavingScheme.BorderColor = System.Drawing.Color.Empty
Me.btnSavingScheme.ButtonType = eZee.Common.eZeeLightButton.enButtonType.CheckButton
Me.btnSavingScheme.FlatAppearance.BorderSize = 0
Me.btnSavingScheme.FlatStyle = System.Windows.Forms.FlatStyle.Flat
Me.btnSavingScheme.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
Me.btnSavingScheme.ForeColor = System.Drawing.Color.Black
Me.btnSavingScheme.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
Me.btnSavingScheme.GradientForeColor = System.Drawing.Color.Black
Me.btnSavingScheme.HoverGradientBackColor = System.Drawing.Color.Transparent
Me.btnSavingScheme.HoverGradientForeColor = System.Drawing.Color.Black
Me.btnSavingScheme.Image = Global.Aruti.Main.My.Resources.Resources.SavingScheme
Me.btnSavingScheme.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
Me.btnSavingScheme.Location = New System.Drawing.Point(3, 477)
Me.btnSavingScheme.MinimumSize = New System.Drawing.Size(150, 35)
Me.btnSavingScheme.Name = "btnSavingScheme"
Me.btnSavingScheme.PressedGradientBackColor = System.Drawing.Color.Transparent
Me.btnSavingScheme.PressedGradientForeColor = System.Drawing.Color.Black
Me.btnSavingScheme.Size = New System.Drawing.Size(166, 35)
Me.btnSavingScheme.TabIndex = 11
Me.btnSavingScheme.Text = "&Saving Scheme"
Me.btnSavingScheme.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
Me.btnSavingScheme.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText
Me.btnSavingScheme.UseVisualStyleBackColor = false
'
'btnEmployeeSavings
'
Me.btnEmployeeSavings.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left)  _
            Or System.Windows.Forms.AnchorStyles.Right),System.Windows.Forms.AnchorStyles)
Me.btnEmployeeSavings.BackColor = System.Drawing.Color.Transparent
Me.btnEmployeeSavings.BackgroundImage = CType(resources.GetObject("btnEmployeeSavings.BackgroundImage"),System.Drawing.Image)
Me.btnEmployeeSavings.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
Me.btnEmployeeSavings.BorderColor = System.Drawing.Color.Empty
Me.btnEmployeeSavings.ButtonType = eZee.Common.eZeeLightButton.enButtonType.CheckButton
Me.btnEmployeeSavings.FlatAppearance.BorderSize = 0
Me.btnEmployeeSavings.FlatStyle = System.Windows.Forms.FlatStyle.Flat
Me.btnEmployeeSavings.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
Me.btnEmployeeSavings.ForeColor = System.Drawing.Color.Black
Me.btnEmployeeSavings.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
Me.btnEmployeeSavings.GradientForeColor = System.Drawing.Color.Black
Me.btnEmployeeSavings.HoverGradientBackColor = System.Drawing.Color.Transparent
Me.btnEmployeeSavings.HoverGradientForeColor = System.Drawing.Color.Black
Me.btnEmployeeSavings.Image = Global.Aruti.Main.My.Resources.Resources.Employee_Saving
Me.btnEmployeeSavings.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
Me.btnEmployeeSavings.Location = New System.Drawing.Point(3, 518)
Me.btnEmployeeSavings.MinimumSize = New System.Drawing.Size(150, 35)
Me.btnEmployeeSavings.Name = "btnEmployeeSavings"
Me.btnEmployeeSavings.PressedGradientBackColor = System.Drawing.Color.Transparent
Me.btnEmployeeSavings.PressedGradientForeColor = System.Drawing.Color.Black
Me.btnEmployeeSavings.Size = New System.Drawing.Size(166, 35)
Me.btnEmployeeSavings.TabIndex = 12
Me.btnEmployeeSavings.Text = "&Employee Savings"
Me.btnEmployeeSavings.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
Me.btnEmployeeSavings.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText
Me.btnEmployeeSavings.UseVisualStyleBackColor = false
'
'btnImportSaving
'
Me.btnImportSaving.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left)  _
            Or System.Windows.Forms.AnchorStyles.Right),System.Windows.Forms.AnchorStyles)
Me.btnImportSaving.BackColor = System.Drawing.Color.Transparent
Me.btnImportSaving.BackgroundImage = CType(resources.GetObject("btnImportSaving.BackgroundImage"),System.Drawing.Image)
Me.btnImportSaving.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
Me.btnImportSaving.BorderColor = System.Drawing.Color.Empty
Me.btnImportSaving.ButtonType = eZee.Common.eZeeLightButton.enButtonType.CheckButton
Me.btnImportSaving.FlatAppearance.BorderSize = 0
Me.btnImportSaving.FlatStyle = System.Windows.Forms.FlatStyle.Flat
Me.btnImportSaving.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
Me.btnImportSaving.ForeColor = System.Drawing.Color.Black
Me.btnImportSaving.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
Me.btnImportSaving.GradientForeColor = System.Drawing.Color.Black
Me.btnImportSaving.HoverGradientBackColor = System.Drawing.Color.Transparent
Me.btnImportSaving.HoverGradientForeColor = System.Drawing.Color.Black
Me.btnImportSaving.Image = Global.Aruti.Main.My.Resources.Resources.common_import
Me.btnImportSaving.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
Me.btnImportSaving.Location = New System.Drawing.Point(3, 559)
Me.btnImportSaving.MinimumSize = New System.Drawing.Size(150, 35)
Me.btnImportSaving.Name = "btnImportSaving"
Me.btnImportSaving.PressedGradientBackColor = System.Drawing.Color.Transparent
Me.btnImportSaving.PressedGradientForeColor = System.Drawing.Color.Black
Me.btnImportSaving.Size = New System.Drawing.Size(166, 35)
Me.btnImportSaving.TabIndex = 19
Me.btnImportSaving.Text = "&Import Saving"
Me.btnImportSaving.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
Me.btnImportSaving.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText
Me.btnImportSaving.UseVisualStyleBackColor = false
'
'fpnlRecruitment
'
Me.fpnlRecruitment.AutoScroll = true
Me.fpnlRecruitment.Controls.Add(Me.btnStaffReqAppLevel)
Me.fpnlRecruitment.Controls.Add(Me.btnStaffReqAppMapping)
Me.fpnlRecruitment.Controls.Add(Me.btnTransferStaffRequisitionApprover)
Me.fpnlRecruitment.Controls.Add(Me.btnStaffRequisition)
Me.fpnlRecruitment.Controls.Add(Me.btnVacancy)
Me.fpnlRecruitment.Controls.Add(Me.btnApplicantMaster)
Me.fpnlRecruitment.Controls.Add(Me.btnDownloadAptitudeResult)
Me.fpnlRecruitment.Controls.Add(Me.btnApplicantFilter)
Me.fpnlRecruitment.Controls.Add(Me.btnFinalShortListedApplicant)
Me.fpnlRecruitment.Controls.Add(Me.btnBatchScheduling)
Me.fpnlRecruitment.Controls.Add(Me.btnInterviewScheduling)
Me.fpnlRecruitment.Controls.Add(Me.btnInterviewAnalysis)
Me.fpnlRecruitment.Controls.Add(Me.btnGlobalInterviewAnalysis)
Me.fpnlRecruitment.Controls.Add(Me.btnFinalApplicants)
Me.fpnlRecruitment.Controls.Add(Me.btnExporttoWeb)
Me.fpnlRecruitment.Controls.Add(Me.btnImportfromWeb)
Me.fpnlRecruitment.Controls.Add(Me.btnAutoImportFromWeb)
Me.fpnlRecruitment.Controls.Add(Me.btnCandidateFeedback)
Me.fpnlRecruitment.Location = New System.Drawing.Point(594, 50)
Me.fpnlRecruitment.Name = "fpnlRecruitment"
Me.fpnlRecruitment.Size = New System.Drawing.Size(189, 43)
Me.fpnlRecruitment.TabIndex = 15
Me.fpnlRecruitment.Visible = false
'
'btnStaffReqAppLevel
'
Me.btnStaffReqAppLevel.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left)  _
            Or System.Windows.Forms.AnchorStyles.Right),System.Windows.Forms.AnchorStyles)
Me.btnStaffReqAppLevel.BackColor = System.Drawing.Color.Transparent
Me.btnStaffReqAppLevel.BackgroundImage = CType(resources.GetObject("btnStaffReqAppLevel.BackgroundImage"),System.Drawing.Image)
Me.btnStaffReqAppLevel.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
Me.btnStaffReqAppLevel.BorderColor = System.Drawing.Color.Empty
Me.btnStaffReqAppLevel.ButtonType = eZee.Common.eZeeLightButton.enButtonType.CheckButton
Me.btnStaffReqAppLevel.FlatAppearance.BorderSize = 0
Me.btnStaffReqAppLevel.FlatStyle = System.Windows.Forms.FlatStyle.Flat
Me.btnStaffReqAppLevel.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
Me.btnStaffReqAppLevel.ForeColor = System.Drawing.Color.Black
Me.btnStaffReqAppLevel.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
Me.btnStaffReqAppLevel.GradientForeColor = System.Drawing.Color.Black
Me.btnStaffReqAppLevel.HoverGradientBackColor = System.Drawing.Color.Transparent
Me.btnStaffReqAppLevel.HoverGradientForeColor = System.Drawing.Color.Black
Me.btnStaffReqAppLevel.Image = Global.Aruti.Main.My.Resources.Resources.rc_Requisition_App_Lvl
Me.btnStaffReqAppLevel.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
Me.btnStaffReqAppLevel.Location = New System.Drawing.Point(3, 3)
Me.btnStaffReqAppLevel.MinimumSize = New System.Drawing.Size(150, 35)
Me.btnStaffReqAppLevel.Name = "btnStaffReqAppLevel"
Me.btnStaffReqAppLevel.PressedGradientBackColor = System.Drawing.Color.Transparent
Me.btnStaffReqAppLevel.PressedGradientForeColor = System.Drawing.Color.Black
Me.btnStaffReqAppLevel.Size = New System.Drawing.Size(166, 35)
Me.btnStaffReqAppLevel.TabIndex = 18
Me.btnStaffReqAppLevel.Text = "&Staff Requisition Approver Level"
Me.btnStaffReqAppLevel.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
Me.btnStaffReqAppLevel.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText
Me.btnStaffReqAppLevel.UseVisualStyleBackColor = false
'
'btnStaffReqAppMapping
'
Me.btnStaffReqAppMapping.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left)  _
            Or System.Windows.Forms.AnchorStyles.Right),System.Windows.Forms.AnchorStyles)
Me.btnStaffReqAppMapping.BackColor = System.Drawing.Color.Transparent
Me.btnStaffReqAppMapping.BackgroundImage = CType(resources.GetObject("btnStaffReqAppMapping.BackgroundImage"),System.Drawing.Image)
Me.btnStaffReqAppMapping.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
Me.btnStaffReqAppMapping.BorderColor = System.Drawing.Color.Empty
Me.btnStaffReqAppMapping.ButtonType = eZee.Common.eZeeLightButton.enButtonType.CheckButton
Me.btnStaffReqAppMapping.FlatAppearance.BorderSize = 0
Me.btnStaffReqAppMapping.FlatStyle = System.Windows.Forms.FlatStyle.Flat
Me.btnStaffReqAppMapping.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
Me.btnStaffReqAppMapping.ForeColor = System.Drawing.Color.Black
Me.btnStaffReqAppMapping.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
Me.btnStaffReqAppMapping.GradientForeColor = System.Drawing.Color.Black
Me.btnStaffReqAppMapping.HoverGradientBackColor = System.Drawing.Color.Transparent
Me.btnStaffReqAppMapping.HoverGradientForeColor = System.Drawing.Color.Black
Me.btnStaffReqAppMapping.Image = Global.Aruti.Main.My.Resources.Resources.rc_Requisition_App_Mapping
Me.btnStaffReqAppMapping.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
Me.btnStaffReqAppMapping.Location = New System.Drawing.Point(3, 44)
Me.btnStaffReqAppMapping.MinimumSize = New System.Drawing.Size(150, 35)
Me.btnStaffReqAppMapping.Name = "btnStaffReqAppMapping"
Me.btnStaffReqAppMapping.PressedGradientBackColor = System.Drawing.Color.Transparent
Me.btnStaffReqAppMapping.PressedGradientForeColor = System.Drawing.Color.Black
Me.btnStaffReqAppMapping.Size = New System.Drawing.Size(166, 35)
Me.btnStaffReqAppMapping.TabIndex = 19
Me.btnStaffReqAppMapping.Text = "&Staff Requisition Approver Mapping"
Me.btnStaffReqAppMapping.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
Me.btnStaffReqAppMapping.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText
Me.btnStaffReqAppMapping.UseVisualStyleBackColor = false
'
'btnTransferStaffRequisitionApprover
'
Me.btnTransferStaffRequisitionApprover.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left)  _
            Or System.Windows.Forms.AnchorStyles.Right),System.Windows.Forms.AnchorStyles)
Me.btnTransferStaffRequisitionApprover.BackColor = System.Drawing.Color.Transparent
Me.btnTransferStaffRequisitionApprover.BackgroundImage = CType(resources.GetObject("btnTransferStaffRequisitionApprover.BackgroundImage"),System.Drawing.Image)
Me.btnTransferStaffRequisitionApprover.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
Me.btnTransferStaffRequisitionApprover.BorderColor = System.Drawing.Color.Empty
Me.btnTransferStaffRequisitionApprover.ButtonType = eZee.Common.eZeeLightButton.enButtonType.CheckButton
Me.btnTransferStaffRequisitionApprover.FlatAppearance.BorderSize = 0
Me.btnTransferStaffRequisitionApprover.FlatStyle = System.Windows.Forms.FlatStyle.Flat
Me.btnTransferStaffRequisitionApprover.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
Me.btnTransferStaffRequisitionApprover.ForeColor = System.Drawing.Color.Black
Me.btnTransferStaffRequisitionApprover.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
Me.btnTransferStaffRequisitionApprover.GradientForeColor = System.Drawing.Color.Black
Me.btnTransferStaffRequisitionApprover.HoverGradientBackColor = System.Drawing.Color.Transparent
Me.btnTransferStaffRequisitionApprover.HoverGradientForeColor = System.Drawing.Color.Black
Me.btnTransferStaffRequisitionApprover.Image = Global.Aruti.Main.My.Resources.Resources.Transfer_Employee
Me.btnTransferStaffRequisitionApprover.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
Me.btnTransferStaffRequisitionApprover.Location = New System.Drawing.Point(3, 85)
Me.btnTransferStaffRequisitionApprover.MinimumSize = New System.Drawing.Size(150, 35)
Me.btnTransferStaffRequisitionApprover.Name = "btnTransferStaffRequisitionApprover"
Me.btnTransferStaffRequisitionApprover.PressedGradientBackColor = System.Drawing.Color.Transparent
Me.btnTransferStaffRequisitionApprover.PressedGradientForeColor = System.Drawing.Color.Black
Me.btnTransferStaffRequisitionApprover.Size = New System.Drawing.Size(166, 35)
Me.btnTransferStaffRequisitionApprover.TabIndex = 21
Me.btnTransferStaffRequisitionApprover.Text = "&Transfer Approver"
Me.btnTransferStaffRequisitionApprover.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
Me.btnTransferStaffRequisitionApprover.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText
Me.btnTransferStaffRequisitionApprover.UseVisualStyleBackColor = false
'
'btnStaffRequisition
'
Me.btnStaffRequisition.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left)  _
            Or System.Windows.Forms.AnchorStyles.Right),System.Windows.Forms.AnchorStyles)
Me.btnStaffRequisition.BackColor = System.Drawing.Color.Transparent
Me.btnStaffRequisition.BackgroundImage = CType(resources.GetObject("btnStaffRequisition.BackgroundImage"),System.Drawing.Image)
Me.btnStaffRequisition.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
Me.btnStaffRequisition.BorderColor = System.Drawing.Color.Empty
Me.btnStaffRequisition.ButtonType = eZee.Common.eZeeLightButton.enButtonType.CheckButton
Me.btnStaffRequisition.FlatAppearance.BorderSize = 0
Me.btnStaffRequisition.FlatStyle = System.Windows.Forms.FlatStyle.Flat
Me.btnStaffRequisition.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
Me.btnStaffRequisition.ForeColor = System.Drawing.Color.Black
Me.btnStaffRequisition.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
Me.btnStaffRequisition.GradientForeColor = System.Drawing.Color.Black
Me.btnStaffRequisition.HoverGradientBackColor = System.Drawing.Color.Transparent
Me.btnStaffRequisition.HoverGradientForeColor = System.Drawing.Color.Black
Me.btnStaffRequisition.Image = Global.Aruti.Main.My.Resources.Resources.Staff_Requisition
Me.btnStaffRequisition.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
Me.btnStaffRequisition.Location = New System.Drawing.Point(3, 126)
Me.btnStaffRequisition.MinimumSize = New System.Drawing.Size(150, 35)
Me.btnStaffRequisition.Name = "btnStaffRequisition"
Me.btnStaffRequisition.PressedGradientBackColor = System.Drawing.Color.Transparent
Me.btnStaffRequisition.PressedGradientForeColor = System.Drawing.Color.Black
Me.btnStaffRequisition.Size = New System.Drawing.Size(166, 35)
Me.btnStaffRequisition.TabIndex = 20
Me.btnStaffRequisition.Text = "&Staff Requisition"
Me.btnStaffRequisition.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
Me.btnStaffRequisition.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText
Me.btnStaffRequisition.UseVisualStyleBackColor = false
'
'btnVacancy
'
Me.btnVacancy.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left)  _
            Or System.Windows.Forms.AnchorStyles.Right),System.Windows.Forms.AnchorStyles)
Me.btnVacancy.BackColor = System.Drawing.Color.Transparent
Me.btnVacancy.BackgroundImage = CType(resources.GetObject("btnVacancy.BackgroundImage"),System.Drawing.Image)
Me.btnVacancy.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
Me.btnVacancy.BorderColor = System.Drawing.Color.Empty
Me.btnVacancy.ButtonType = eZee.Common.eZeeLightButton.enButtonType.CheckButton
Me.btnVacancy.FlatAppearance.BorderSize = 0
Me.btnVacancy.FlatStyle = System.Windows.Forms.FlatStyle.Flat
Me.btnVacancy.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
Me.btnVacancy.ForeColor = System.Drawing.Color.Black
Me.btnVacancy.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
Me.btnVacancy.GradientForeColor = System.Drawing.Color.Black
Me.btnVacancy.HoverGradientBackColor = System.Drawing.Color.Transparent
Me.btnVacancy.HoverGradientForeColor = System.Drawing.Color.Black
Me.btnVacancy.Image = Global.Aruti.Main.My.Resources.Resources.vacancy
Me.btnVacancy.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
Me.btnVacancy.Location = New System.Drawing.Point(3, 167)
Me.btnVacancy.MinimumSize = New System.Drawing.Size(150, 35)
Me.btnVacancy.Name = "btnVacancy"
Me.btnVacancy.PressedGradientBackColor = System.Drawing.Color.Transparent
Me.btnVacancy.PressedGradientForeColor = System.Drawing.Color.Black
Me.btnVacancy.Size = New System.Drawing.Size(166, 35)
Me.btnVacancy.TabIndex = 8
Me.btnVacancy.Text = "&Vacancy Master"
Me.btnVacancy.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
Me.btnVacancy.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText
Me.btnVacancy.UseVisualStyleBackColor = false
'
'btnApplicantMaster
'
Me.btnApplicantMaster.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left)  _
            Or System.Windows.Forms.AnchorStyles.Right),System.Windows.Forms.AnchorStyles)
Me.btnApplicantMaster.BackColor = System.Drawing.Color.Transparent
Me.btnApplicantMaster.BackgroundImage = CType(resources.GetObject("btnApplicantMaster.BackgroundImage"),System.Drawing.Image)
Me.btnApplicantMaster.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
Me.btnApplicantMaster.BorderColor = System.Drawing.Color.Empty
Me.btnApplicantMaster.ButtonType = eZee.Common.eZeeLightButton.enButtonType.CheckButton
Me.btnApplicantMaster.FlatAppearance.BorderSize = 0
Me.btnApplicantMaster.FlatStyle = System.Windows.Forms.FlatStyle.Flat
Me.btnApplicantMaster.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
Me.btnApplicantMaster.ForeColor = System.Drawing.Color.Black
Me.btnApplicantMaster.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
Me.btnApplicantMaster.GradientForeColor = System.Drawing.Color.Black
Me.btnApplicantMaster.HoverGradientBackColor = System.Drawing.Color.Transparent
Me.btnApplicantMaster.HoverGradientForeColor = System.Drawing.Color.Black
Me.btnApplicantMaster.Image = Global.Aruti.Main.My.Resources.Resources.Applicant_Detail
Me.btnApplicantMaster.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
Me.btnApplicantMaster.Location = New System.Drawing.Point(3, 208)
Me.btnApplicantMaster.MinimumSize = New System.Drawing.Size(150, 35)
Me.btnApplicantMaster.Name = "btnApplicantMaster"
Me.btnApplicantMaster.PressedGradientBackColor = System.Drawing.Color.Transparent
Me.btnApplicantMaster.PressedGradientForeColor = System.Drawing.Color.Black
Me.btnApplicantMaster.Size = New System.Drawing.Size(166, 35)
Me.btnApplicantMaster.TabIndex = 7
Me.btnApplicantMaster.Text = "&Applicant Master"
Me.btnApplicantMaster.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
Me.btnApplicantMaster.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText
Me.btnApplicantMaster.UseVisualStyleBackColor = false
'
'btnDownloadAptitudeResult
'
Me.btnDownloadAptitudeResult.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left)  _
            Or System.Windows.Forms.AnchorStyles.Right),System.Windows.Forms.AnchorStyles)
Me.btnDownloadAptitudeResult.BackColor = System.Drawing.Color.Transparent
Me.btnDownloadAptitudeResult.BackgroundImage = CType(resources.GetObject("btnDownloadAptitudeResult.BackgroundImage"),System.Drawing.Image)
Me.btnDownloadAptitudeResult.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
Me.btnDownloadAptitudeResult.BorderColor = System.Drawing.Color.Empty
Me.btnDownloadAptitudeResult.ButtonType = eZee.Common.eZeeLightButton.enButtonType.CheckButton
Me.btnDownloadAptitudeResult.FlatAppearance.BorderSize = 0
Me.btnDownloadAptitudeResult.FlatStyle = System.Windows.Forms.FlatStyle.Flat
Me.btnDownloadAptitudeResult.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
Me.btnDownloadAptitudeResult.ForeColor = System.Drawing.Color.Black
Me.btnDownloadAptitudeResult.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
Me.btnDownloadAptitudeResult.GradientForeColor = System.Drawing.Color.Black
Me.btnDownloadAptitudeResult.HoverGradientBackColor = System.Drawing.Color.Transparent
Me.btnDownloadAptitudeResult.HoverGradientForeColor = System.Drawing.Color.Black
Me.btnDownloadAptitudeResult.Image = Global.Aruti.Main.My.Resources.Resources.result_code
Me.btnDownloadAptitudeResult.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
Me.btnDownloadAptitudeResult.Location = New System.Drawing.Point(3, 249)
Me.btnDownloadAptitudeResult.MinimumSize = New System.Drawing.Size(150, 35)
Me.btnDownloadAptitudeResult.Name = "btnDownloadAptitudeResult"
Me.btnDownloadAptitudeResult.PressedGradientBackColor = System.Drawing.Color.Transparent
Me.btnDownloadAptitudeResult.PressedGradientForeColor = System.Drawing.Color.Black
Me.btnDownloadAptitudeResult.Size = New System.Drawing.Size(166, 35)
Me.btnDownloadAptitudeResult.TabIndex = 23
Me.btnDownloadAptitudeResult.Text = "Download Aptitude Result"
Me.btnDownloadAptitudeResult.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
Me.btnDownloadAptitudeResult.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText
Me.btnDownloadAptitudeResult.UseVisualStyleBackColor = false
'
'btnApplicantFilter
'
Me.btnApplicantFilter.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left)  _
            Or System.Windows.Forms.AnchorStyles.Right),System.Windows.Forms.AnchorStyles)
Me.btnApplicantFilter.BackColor = System.Drawing.Color.Transparent
Me.btnApplicantFilter.BackgroundImage = CType(resources.GetObject("btnApplicantFilter.BackgroundImage"),System.Drawing.Image)
Me.btnApplicantFilter.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
Me.btnApplicantFilter.BorderColor = System.Drawing.Color.Empty
Me.btnApplicantFilter.ButtonType = eZee.Common.eZeeLightButton.enButtonType.CheckButton
Me.btnApplicantFilter.FlatAppearance.BorderSize = 0
Me.btnApplicantFilter.FlatStyle = System.Windows.Forms.FlatStyle.Flat
Me.btnApplicantFilter.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
Me.btnApplicantFilter.ForeColor = System.Drawing.Color.Black
Me.btnApplicantFilter.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
Me.btnApplicantFilter.GradientForeColor = System.Drawing.Color.Black
Me.btnApplicantFilter.HoverGradientBackColor = System.Drawing.Color.Transparent
Me.btnApplicantFilter.HoverGradientForeColor = System.Drawing.Color.Black
Me.btnApplicantFilter.Image = Global.Aruti.Main.My.Resources.Resources.short_listed_applicant1
Me.btnApplicantFilter.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
Me.btnApplicantFilter.Location = New System.Drawing.Point(3, 290)
Me.btnApplicantFilter.MinimumSize = New System.Drawing.Size(150, 35)
Me.btnApplicantFilter.Name = "btnApplicantFilter"
Me.btnApplicantFilter.PressedGradientBackColor = System.Drawing.Color.Transparent
Me.btnApplicantFilter.PressedGradientForeColor = System.Drawing.Color.Black
Me.btnApplicantFilter.Size = New System.Drawing.Size(166, 35)
Me.btnApplicantFilter.TabIndex = 13
Me.btnApplicantFilter.Text = "Short List Applicants"
Me.btnApplicantFilter.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
Me.btnApplicantFilter.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText
Me.btnApplicantFilter.UseVisualStyleBackColor = false
'
'btnFinalShortListedApplicant
'
Me.btnFinalShortListedApplicant.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left)  _
            Or System.Windows.Forms.AnchorStyles.Right),System.Windows.Forms.AnchorStyles)
Me.btnFinalShortListedApplicant.BackColor = System.Drawing.Color.Transparent
Me.btnFinalShortListedApplicant.BackgroundImage = CType(resources.GetObject("btnFinalShortListedApplicant.BackgroundImage"),System.Drawing.Image)
Me.btnFinalShortListedApplicant.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
Me.btnFinalShortListedApplicant.BorderColor = System.Drawing.Color.Empty
Me.btnFinalShortListedApplicant.ButtonType = eZee.Common.eZeeLightButton.enButtonType.CheckButton
Me.btnFinalShortListedApplicant.FlatAppearance.BorderSize = 0
Me.btnFinalShortListedApplicant.FlatStyle = System.Windows.Forms.FlatStyle.Flat
Me.btnFinalShortListedApplicant.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
Me.btnFinalShortListedApplicant.ForeColor = System.Drawing.Color.Black
Me.btnFinalShortListedApplicant.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
Me.btnFinalShortListedApplicant.GradientForeColor = System.Drawing.Color.Black
Me.btnFinalShortListedApplicant.HoverGradientBackColor = System.Drawing.Color.Transparent
Me.btnFinalShortListedApplicant.HoverGradientForeColor = System.Drawing.Color.Black
Me.btnFinalShortListedApplicant.Image = Global.Aruti.Main.My.Resources.Resources.final_short_listed_applicant1
Me.btnFinalShortListedApplicant.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
Me.btnFinalShortListedApplicant.Location = New System.Drawing.Point(3, 331)
Me.btnFinalShortListedApplicant.MinimumSize = New System.Drawing.Size(150, 35)
Me.btnFinalShortListedApplicant.Name = "btnFinalShortListedApplicant"
Me.btnFinalShortListedApplicant.PressedGradientBackColor = System.Drawing.Color.Transparent
Me.btnFinalShortListedApplicant.PressedGradientForeColor = System.Drawing.Color.Black
Me.btnFinalShortListedApplicant.Size = New System.Drawing.Size(166, 35)
Me.btnFinalShortListedApplicant.TabIndex = 14
Me.btnFinalShortListedApplicant.Text = "Final Short Listed Applicant "
Me.btnFinalShortListedApplicant.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
Me.btnFinalShortListedApplicant.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText
Me.btnFinalShortListedApplicant.UseVisualStyleBackColor = false
'
'btnBatchScheduling
'
Me.btnBatchScheduling.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left)  _
            Or System.Windows.Forms.AnchorStyles.Right),System.Windows.Forms.AnchorStyles)
Me.btnBatchScheduling.BackColor = System.Drawing.Color.Transparent
Me.btnBatchScheduling.BackgroundImage = CType(resources.GetObject("btnBatchScheduling.BackgroundImage"),System.Drawing.Image)
Me.btnBatchScheduling.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
Me.btnBatchScheduling.BorderColor = System.Drawing.Color.Empty
Me.btnBatchScheduling.ButtonType = eZee.Common.eZeeLightButton.enButtonType.CheckButton
Me.btnBatchScheduling.FlatAppearance.BorderSize = 0
Me.btnBatchScheduling.FlatStyle = System.Windows.Forms.FlatStyle.Flat
Me.btnBatchScheduling.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
Me.btnBatchScheduling.ForeColor = System.Drawing.Color.Black
Me.btnBatchScheduling.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
Me.btnBatchScheduling.GradientForeColor = System.Drawing.Color.Black
Me.btnBatchScheduling.HoverGradientBackColor = System.Drawing.Color.Transparent
Me.btnBatchScheduling.HoverGradientForeColor = System.Drawing.Color.Black
Me.btnBatchScheduling.Image = Global.Aruti.Main.My.Resources.Resources.BatchScheduling
Me.btnBatchScheduling.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
Me.btnBatchScheduling.Location = New System.Drawing.Point(3, 372)
Me.btnBatchScheduling.MinimumSize = New System.Drawing.Size(150, 35)
Me.btnBatchScheduling.Name = "btnBatchScheduling"
Me.btnBatchScheduling.PressedGradientBackColor = System.Drawing.Color.Transparent
Me.btnBatchScheduling.PressedGradientForeColor = System.Drawing.Color.Black
Me.btnBatchScheduling.Size = New System.Drawing.Size(166, 35)
Me.btnBatchScheduling.TabIndex = 9
Me.btnBatchScheduling.Text = "Batch &Scheduling"
Me.btnBatchScheduling.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
Me.btnBatchScheduling.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText
Me.btnBatchScheduling.UseVisualStyleBackColor = false
'
'btnInterviewScheduling
'
Me.btnInterviewScheduling.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left)  _
            Or System.Windows.Forms.AnchorStyles.Right),System.Windows.Forms.AnchorStyles)
Me.btnInterviewScheduling.BackColor = System.Drawing.Color.Transparent
Me.btnInterviewScheduling.BackgroundImage = CType(resources.GetObject("btnInterviewScheduling.BackgroundImage"),System.Drawing.Image)
Me.btnInterviewScheduling.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
Me.btnInterviewScheduling.BorderColor = System.Drawing.Color.Empty
Me.btnInterviewScheduling.ButtonType = eZee.Common.eZeeLightButton.enButtonType.CheckButton
Me.btnInterviewScheduling.FlatAppearance.BorderSize = 0
Me.btnInterviewScheduling.FlatStyle = System.Windows.Forms.FlatStyle.Flat
Me.btnInterviewScheduling.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
Me.btnInterviewScheduling.ForeColor = System.Drawing.Color.Black
Me.btnInterviewScheduling.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
Me.btnInterviewScheduling.GradientForeColor = System.Drawing.Color.Black
Me.btnInterviewScheduling.HoverGradientBackColor = System.Drawing.Color.Transparent
Me.btnInterviewScheduling.HoverGradientForeColor = System.Drawing.Color.Black
Me.btnInterviewScheduling.Image = Global.Aruti.Main.My.Resources.Resources.Interview_Scheduling
Me.btnInterviewScheduling.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
Me.btnInterviewScheduling.Location = New System.Drawing.Point(3, 413)
Me.btnInterviewScheduling.MinimumSize = New System.Drawing.Size(150, 35)
Me.btnInterviewScheduling.Name = "btnInterviewScheduling"
Me.btnInterviewScheduling.PressedGradientBackColor = System.Drawing.Color.Transparent
Me.btnInterviewScheduling.PressedGradientForeColor = System.Drawing.Color.Black
Me.btnInterviewScheduling.Size = New System.Drawing.Size(166, 35)
Me.btnInterviewScheduling.TabIndex = 10
Me.btnInterviewScheduling.Text = "&Interview Scheduling"
Me.btnInterviewScheduling.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
Me.btnInterviewScheduling.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText
Me.btnInterviewScheduling.UseVisualStyleBackColor = false
'
'btnInterviewAnalysis
'
Me.btnInterviewAnalysis.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left)  _
            Or System.Windows.Forms.AnchorStyles.Right),System.Windows.Forms.AnchorStyles)
Me.btnInterviewAnalysis.BackColor = System.Drawing.Color.Transparent
Me.btnInterviewAnalysis.BackgroundImage = CType(resources.GetObject("btnInterviewAnalysis.BackgroundImage"),System.Drawing.Image)
Me.btnInterviewAnalysis.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
Me.btnInterviewAnalysis.BorderColor = System.Drawing.Color.Empty
Me.btnInterviewAnalysis.ButtonType = eZee.Common.eZeeLightButton.enButtonType.CheckButton
Me.btnInterviewAnalysis.FlatAppearance.BorderSize = 0
Me.btnInterviewAnalysis.FlatStyle = System.Windows.Forms.FlatStyle.Flat
Me.btnInterviewAnalysis.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
Me.btnInterviewAnalysis.ForeColor = System.Drawing.Color.Black
Me.btnInterviewAnalysis.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
Me.btnInterviewAnalysis.GradientForeColor = System.Drawing.Color.Black
Me.btnInterviewAnalysis.HoverGradientBackColor = System.Drawing.Color.Transparent
Me.btnInterviewAnalysis.HoverGradientForeColor = System.Drawing.Color.Black
Me.btnInterviewAnalysis.Image = Global.Aruti.Main.My.Resources.Resources.InterviewAnalysis
Me.btnInterviewAnalysis.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
Me.btnInterviewAnalysis.Location = New System.Drawing.Point(3, 454)
Me.btnInterviewAnalysis.MinimumSize = New System.Drawing.Size(150, 35)
Me.btnInterviewAnalysis.Name = "btnInterviewAnalysis"
Me.btnInterviewAnalysis.PressedGradientBackColor = System.Drawing.Color.Transparent
Me.btnInterviewAnalysis.PressedGradientForeColor = System.Drawing.Color.Black
Me.btnInterviewAnalysis.Size = New System.Drawing.Size(166, 35)
Me.btnInterviewAnalysis.TabIndex = 11
Me.btnInterviewAnalysis.Text = "Interview Ana&lysis"
Me.btnInterviewAnalysis.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
Me.btnInterviewAnalysis.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText
Me.btnInterviewAnalysis.UseVisualStyleBackColor = false
'
'btnGlobalInterviewAnalysis
'
Me.btnGlobalInterviewAnalysis.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left)  _
            Or System.Windows.Forms.AnchorStyles.Right),System.Windows.Forms.AnchorStyles)
Me.btnGlobalInterviewAnalysis.BackColor = System.Drawing.Color.Transparent
Me.btnGlobalInterviewAnalysis.BackgroundImage = CType(resources.GetObject("btnGlobalInterviewAnalysis.BackgroundImage"),System.Drawing.Image)
Me.btnGlobalInterviewAnalysis.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
Me.btnGlobalInterviewAnalysis.BorderColor = System.Drawing.Color.Empty
Me.btnGlobalInterviewAnalysis.ButtonType = eZee.Common.eZeeLightButton.enButtonType.CheckButton
Me.btnGlobalInterviewAnalysis.FlatAppearance.BorderSize = 0
Me.btnGlobalInterviewAnalysis.FlatStyle = System.Windows.Forms.FlatStyle.Flat
Me.btnGlobalInterviewAnalysis.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
Me.btnGlobalInterviewAnalysis.ForeColor = System.Drawing.Color.Black
Me.btnGlobalInterviewAnalysis.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
Me.btnGlobalInterviewAnalysis.GradientForeColor = System.Drawing.Color.Black
Me.btnGlobalInterviewAnalysis.HoverGradientBackColor = System.Drawing.Color.Transparent
Me.btnGlobalInterviewAnalysis.HoverGradientForeColor = System.Drawing.Color.Black
Me.btnGlobalInterviewAnalysis.Image = Global.Aruti.Main.My.Resources.Resources.InterviewAnalysis
Me.btnGlobalInterviewAnalysis.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
Me.btnGlobalInterviewAnalysis.Location = New System.Drawing.Point(3, 495)
Me.btnGlobalInterviewAnalysis.MinimumSize = New System.Drawing.Size(150, 35)
Me.btnGlobalInterviewAnalysis.Name = "btnGlobalInterviewAnalysis"
Me.btnGlobalInterviewAnalysis.PressedGradientBackColor = System.Drawing.Color.Transparent
Me.btnGlobalInterviewAnalysis.PressedGradientForeColor = System.Drawing.Color.Black
Me.btnGlobalInterviewAnalysis.Size = New System.Drawing.Size(166, 35)
Me.btnGlobalInterviewAnalysis.TabIndex = 18
Me.btnGlobalInterviewAnalysis.Text = "Global Interview Ana&lysis"
Me.btnGlobalInterviewAnalysis.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
Me.btnGlobalInterviewAnalysis.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText
Me.btnGlobalInterviewAnalysis.UseVisualStyleBackColor = false
'
'btnFinalApplicants
'
Me.btnFinalApplicants.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left)  _
            Or System.Windows.Forms.AnchorStyles.Right),System.Windows.Forms.AnchorStyles)
Me.btnFinalApplicants.BackColor = System.Drawing.Color.Transparent
Me.btnFinalApplicants.BackgroundImage = CType(resources.GetObject("btnFinalApplicants.BackgroundImage"),System.Drawing.Image)
Me.btnFinalApplicants.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
Me.btnFinalApplicants.BorderColor = System.Drawing.Color.Empty
Me.btnFinalApplicants.ButtonType = eZee.Common.eZeeLightButton.enButtonType.CheckButton
Me.btnFinalApplicants.FlatAppearance.BorderSize = 0
Me.btnFinalApplicants.FlatStyle = System.Windows.Forms.FlatStyle.Flat
Me.btnFinalApplicants.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
Me.btnFinalApplicants.ForeColor = System.Drawing.Color.Black
Me.btnFinalApplicants.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
Me.btnFinalApplicants.GradientForeColor = System.Drawing.Color.Black
Me.btnFinalApplicants.HoverGradientBackColor = System.Drawing.Color.Transparent
Me.btnFinalApplicants.HoverGradientForeColor = System.Drawing.Color.Black
Me.btnFinalApplicants.Image = Global.Aruti.Main.My.Resources.Resources.FinalApplicant
Me.btnFinalApplicants.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
Me.btnFinalApplicants.Location = New System.Drawing.Point(3, 536)
Me.btnFinalApplicants.MinimumSize = New System.Drawing.Size(150, 35)
Me.btnFinalApplicants.Name = "btnFinalApplicants"
Me.btnFinalApplicants.PressedGradientBackColor = System.Drawing.Color.Transparent
Me.btnFinalApplicants.PressedGradientForeColor = System.Drawing.Color.Black
Me.btnFinalApplicants.Size = New System.Drawing.Size(166, 35)
Me.btnFinalApplicants.TabIndex = 12
Me.btnFinalApplicants.Text = "&Final Applicants"
Me.btnFinalApplicants.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
Me.btnFinalApplicants.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText
Me.btnFinalApplicants.UseVisualStyleBackColor = false
'
'btnExporttoWeb
'
Me.btnExporttoWeb.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left)  _
            Or System.Windows.Forms.AnchorStyles.Right),System.Windows.Forms.AnchorStyles)
Me.btnExporttoWeb.BackColor = System.Drawing.Color.Transparent
Me.btnExporttoWeb.BackgroundImage = CType(resources.GetObject("btnExporttoWeb.BackgroundImage"),System.Drawing.Image)
Me.btnExporttoWeb.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
Me.btnExporttoWeb.BorderColor = System.Drawing.Color.Empty
Me.btnExporttoWeb.ButtonType = eZee.Common.eZeeLightButton.enButtonType.CheckButton
Me.btnExporttoWeb.FlatAppearance.BorderSize = 0
Me.btnExporttoWeb.FlatStyle = System.Windows.Forms.FlatStyle.Flat
Me.btnExporttoWeb.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
Me.btnExporttoWeb.ForeColor = System.Drawing.Color.Black
Me.btnExporttoWeb.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
Me.btnExporttoWeb.GradientForeColor = System.Drawing.Color.Black
Me.btnExporttoWeb.HoverGradientBackColor = System.Drawing.Color.Transparent
Me.btnExporttoWeb.HoverGradientForeColor = System.Drawing.Color.Black
Me.btnExporttoWeb.Image = Global.Aruti.Main.My.Resources.Resources.export_database
Me.btnExporttoWeb.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
Me.btnExporttoWeb.Location = New System.Drawing.Point(3, 577)
Me.btnExporttoWeb.MinimumSize = New System.Drawing.Size(150, 35)
Me.btnExporttoWeb.Name = "btnExporttoWeb"
Me.btnExporttoWeb.PressedGradientBackColor = System.Drawing.Color.Transparent
Me.btnExporttoWeb.PressedGradientForeColor = System.Drawing.Color.Black
Me.btnExporttoWeb.Size = New System.Drawing.Size(166, 35)
Me.btnExporttoWeb.TabIndex = 16
Me.btnExporttoWeb.Text = "&Export Data"
Me.btnExporttoWeb.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
Me.btnExporttoWeb.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText
Me.btnExporttoWeb.UseVisualStyleBackColor = false
'
'btnImportfromWeb
'
Me.btnImportfromWeb.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left)  _
            Or System.Windows.Forms.AnchorStyles.Right),System.Windows.Forms.AnchorStyles)
Me.btnImportfromWeb.BackColor = System.Drawing.Color.Transparent
Me.btnImportfromWeb.BackgroundImage = CType(resources.GetObject("btnImportfromWeb.BackgroundImage"),System.Drawing.Image)
Me.btnImportfromWeb.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
Me.btnImportfromWeb.BorderColor = System.Drawing.Color.Empty
Me.btnImportfromWeb.ButtonType = eZee.Common.eZeeLightButton.enButtonType.CheckButton
Me.btnImportfromWeb.FlatAppearance.BorderSize = 0
Me.btnImportfromWeb.FlatStyle = System.Windows.Forms.FlatStyle.Flat
Me.btnImportfromWeb.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
Me.btnImportfromWeb.ForeColor = System.Drawing.Color.Black
Me.btnImportfromWeb.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
Me.btnImportfromWeb.GradientForeColor = System.Drawing.Color.Black
Me.btnImportfromWeb.HoverGradientBackColor = System.Drawing.Color.Transparent
Me.btnImportfromWeb.HoverGradientForeColor = System.Drawing.Color.Black
Me.btnImportfromWeb.Image = Global.Aruti.Main.My.Resources.Resources.import_database
Me.btnImportfromWeb.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
Me.btnImportfromWeb.Location = New System.Drawing.Point(3, 618)
Me.btnImportfromWeb.MinimumSize = New System.Drawing.Size(150, 35)
Me.btnImportfromWeb.Name = "btnImportfromWeb"
Me.btnImportfromWeb.PressedGradientBackColor = System.Drawing.Color.Transparent
Me.btnImportfromWeb.PressedGradientForeColor = System.Drawing.Color.Black
Me.btnImportfromWeb.Size = New System.Drawing.Size(166, 35)
Me.btnImportfromWeb.TabIndex = 15
Me.btnImportfromWeb.Text = "&Import Data"
Me.btnImportfromWeb.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
Me.btnImportfromWeb.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText
Me.btnImportfromWeb.UseVisualStyleBackColor = false
'
'btnAutoImportFromWeb
'
Me.btnAutoImportFromWeb.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left)  _
            Or System.Windows.Forms.AnchorStyles.Right),System.Windows.Forms.AnchorStyles)
Me.btnAutoImportFromWeb.BackColor = System.Drawing.Color.Transparent
Me.btnAutoImportFromWeb.BackgroundImage = CType(resources.GetObject("btnAutoImportFromWeb.BackgroundImage"),System.Drawing.Image)
Me.btnAutoImportFromWeb.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
Me.btnAutoImportFromWeb.BorderColor = System.Drawing.Color.Empty
Me.btnAutoImportFromWeb.ButtonType = eZee.Common.eZeeLightButton.enButtonType.CheckButton
Me.btnAutoImportFromWeb.FlatAppearance.BorderSize = 0
Me.btnAutoImportFromWeb.FlatStyle = System.Windows.Forms.FlatStyle.Flat
Me.btnAutoImportFromWeb.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
Me.btnAutoImportFromWeb.ForeColor = System.Drawing.Color.Black
Me.btnAutoImportFromWeb.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
Me.btnAutoImportFromWeb.GradientForeColor = System.Drawing.Color.Black
Me.btnAutoImportFromWeb.HoverGradientBackColor = System.Drawing.Color.Transparent
Me.btnAutoImportFromWeb.HoverGradientForeColor = System.Drawing.Color.Black
Me.btnAutoImportFromWeb.Image = Global.Aruti.Main.My.Resources.Resources.autoimport
Me.btnAutoImportFromWeb.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
Me.btnAutoImportFromWeb.Location = New System.Drawing.Point(3, 659)
Me.btnAutoImportFromWeb.MinimumSize = New System.Drawing.Size(150, 35)
Me.btnAutoImportFromWeb.Name = "btnAutoImportFromWeb"
Me.btnAutoImportFromWeb.PressedGradientBackColor = System.Drawing.Color.Transparent
Me.btnAutoImportFromWeb.PressedGradientForeColor = System.Drawing.Color.Black
Me.btnAutoImportFromWeb.Size = New System.Drawing.Size(166, 35)
Me.btnAutoImportFromWeb.TabIndex = 17
Me.btnAutoImportFromWeb.Text = "&Auto Import Data"
Me.btnAutoImportFromWeb.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
Me.btnAutoImportFromWeb.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText
Me.btnAutoImportFromWeb.UseVisualStyleBackColor = false
'
'btnCandidateFeedback
'
Me.btnCandidateFeedback.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left)  _
            Or System.Windows.Forms.AnchorStyles.Right),System.Windows.Forms.AnchorStyles)
Me.btnCandidateFeedback.BackColor = System.Drawing.Color.Transparent
Me.btnCandidateFeedback.BackgroundImage = CType(resources.GetObject("btnCandidateFeedback.BackgroundImage"),System.Drawing.Image)
Me.btnCandidateFeedback.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
Me.btnCandidateFeedback.BorderColor = System.Drawing.Color.Empty
Me.btnCandidateFeedback.ButtonType = eZee.Common.eZeeLightButton.enButtonType.CheckButton
Me.btnCandidateFeedback.FlatAppearance.BorderSize = 0
Me.btnCandidateFeedback.FlatStyle = System.Windows.Forms.FlatStyle.Flat
Me.btnCandidateFeedback.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
Me.btnCandidateFeedback.ForeColor = System.Drawing.Color.Black
Me.btnCandidateFeedback.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
Me.btnCandidateFeedback.GradientForeColor = System.Drawing.Color.Black
Me.btnCandidateFeedback.HoverGradientBackColor = System.Drawing.Color.Transparent
Me.btnCandidateFeedback.HoverGradientForeColor = System.Drawing.Color.Black
Me.btnCandidateFeedback.Image = Global.Aruti.Main.My.Resources.Resources.employee_feedback
Me.btnCandidateFeedback.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
Me.btnCandidateFeedback.Location = New System.Drawing.Point(3, 700)
Me.btnCandidateFeedback.MinimumSize = New System.Drawing.Size(150, 35)
Me.btnCandidateFeedback.Name = "btnCandidateFeedback"
Me.btnCandidateFeedback.PressedGradientBackColor = System.Drawing.Color.Transparent
Me.btnCandidateFeedback.PressedGradientForeColor = System.Drawing.Color.Black
Me.btnCandidateFeedback.Size = New System.Drawing.Size(166, 35)
Me.btnCandidateFeedback.TabIndex = 22
Me.btnCandidateFeedback.Text = "Send Candidate Feedback"
Me.btnCandidateFeedback.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
Me.btnCandidateFeedback.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText
Me.btnCandidateFeedback.UseVisualStyleBackColor = false
'
'fpnlEmpData
'
Me.fpnlEmpData.AutoScroll = true
Me.fpnlEmpData.Controls.Add(Me.btnEmployeeData2)
Me.fpnlEmpData.Controls.Add(Me.btnDependants2)
Me.fpnlEmpData.Controls.Add(Me.btnBenefitAllocation)
Me.fpnlEmpData.Controls.Add(Me.btnEmployeeBenefit)
Me.fpnlEmpData.Controls.Add(Me.btnGroupBenefit)
Me.fpnlEmpData.Controls.Add(Me.btnReferee)
Me.fpnlEmpData.Controls.Add(Me.btnEmployeeAssets)
Me.fpnlEmpData.Controls.Add(Me.btnEmployeeSkill)
Me.fpnlEmpData.Controls.Add(Me.btnEmployeeQualification)
Me.fpnlEmpData.Controls.Add(Me.btnEmployeeExperence)
Me.fpnlEmpData.Controls.Add(Me.btnEmployeeMovement2)
Me.fpnlEmpData.Controls.Add(Me.btnEmployeeDiary)
Me.fpnlEmpData.Controls.Add(Me.btnEmployeeDataApproval)
Me.fpnlEmpData.Location = New System.Drawing.Point(594, 99)
Me.fpnlEmpData.Name = "fpnlEmpData"
Me.fpnlEmpData.Size = New System.Drawing.Size(191, 41)
Me.fpnlEmpData.TabIndex = 2
Me.fpnlEmpData.Visible = false
'
'btnEmployeeData2
'
Me.btnEmployeeData2.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left)  _
            Or System.Windows.Forms.AnchorStyles.Right),System.Windows.Forms.AnchorStyles)
Me.btnEmployeeData2.BackColor = System.Drawing.Color.Transparent
Me.btnEmployeeData2.BackgroundImage = CType(resources.GetObject("btnEmployeeData2.BackgroundImage"),System.Drawing.Image)
Me.btnEmployeeData2.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
Me.btnEmployeeData2.BorderColor = System.Drawing.Color.Empty
Me.btnEmployeeData2.ButtonType = eZee.Common.eZeeLightButton.enButtonType.CheckButton
Me.btnEmployeeData2.FlatAppearance.BorderSize = 0
Me.btnEmployeeData2.FlatStyle = System.Windows.Forms.FlatStyle.Flat
Me.btnEmployeeData2.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
Me.btnEmployeeData2.ForeColor = System.Drawing.Color.Black
Me.btnEmployeeData2.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
Me.btnEmployeeData2.GradientForeColor = System.Drawing.Color.Black
Me.btnEmployeeData2.HoverGradientBackColor = System.Drawing.Color.Transparent
Me.btnEmployeeData2.HoverGradientForeColor = System.Drawing.Color.Black
Me.btnEmployeeData2.Image = Global.Aruti.Main.My.Resources.Resources.employee
Me.btnEmployeeData2.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
Me.btnEmployeeData2.Location = New System.Drawing.Point(3, 3)
Me.btnEmployeeData2.MinimumSize = New System.Drawing.Size(150, 35)
Me.btnEmployeeData2.Name = "btnEmployeeData2"
Me.btnEmployeeData2.PressedGradientBackColor = System.Drawing.Color.Transparent
Me.btnEmployeeData2.PressedGradientForeColor = System.Drawing.Color.Black
Me.btnEmployeeData2.Size = New System.Drawing.Size(166, 35)
Me.btnEmployeeData2.TabIndex = 6
Me.btnEmployeeData2.Text = "&Employee Master"
Me.btnEmployeeData2.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
Me.btnEmployeeData2.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText
Me.btnEmployeeData2.UseVisualStyleBackColor = false
Me.btnEmployeeData2.Visible = false
'
'btnDependants2
'
Me.btnDependants2.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left)  _
            Or System.Windows.Forms.AnchorStyles.Right),System.Windows.Forms.AnchorStyles)
Me.btnDependants2.BackColor = System.Drawing.Color.Transparent
Me.btnDependants2.BackgroundImage = CType(resources.GetObject("btnDependants2.BackgroundImage"),System.Drawing.Image)
Me.btnDependants2.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
Me.btnDependants2.BorderColor = System.Drawing.Color.Empty
Me.btnDependants2.ButtonType = eZee.Common.eZeeLightButton.enButtonType.CheckButton
Me.btnDependants2.FlatAppearance.BorderSize = 0
Me.btnDependants2.FlatStyle = System.Windows.Forms.FlatStyle.Flat
Me.btnDependants2.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
Me.btnDependants2.ForeColor = System.Drawing.Color.Black
Me.btnDependants2.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
Me.btnDependants2.GradientForeColor = System.Drawing.Color.Black
Me.btnDependants2.HoverGradientBackColor = System.Drawing.Color.Transparent
Me.btnDependants2.HoverGradientForeColor = System.Drawing.Color.Black
Me.btnDependants2.Image = Global.Aruti.Main.My.Resources.Resources.dependants
Me.btnDependants2.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
Me.btnDependants2.Location = New System.Drawing.Point(3, 44)
Me.btnDependants2.MinimumSize = New System.Drawing.Size(150, 35)
Me.btnDependants2.Name = "btnDependants2"
Me.btnDependants2.PressedGradientBackColor = System.Drawing.Color.Transparent
Me.btnDependants2.PressedGradientForeColor = System.Drawing.Color.Black
Me.btnDependants2.Size = New System.Drawing.Size(166, 35)
Me.btnDependants2.TabIndex = 7
Me.btnDependants2.Text = "&Dependants"
Me.btnDependants2.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
Me.btnDependants2.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText
Me.btnDependants2.UseVisualStyleBackColor = false
Me.btnDependants2.Visible = false
'
'btnBenefitAllocation
'
Me.btnBenefitAllocation.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left)  _
            Or System.Windows.Forms.AnchorStyles.Right),System.Windows.Forms.AnchorStyles)
Me.btnBenefitAllocation.BackColor = System.Drawing.Color.Transparent
Me.btnBenefitAllocation.BackgroundImage = CType(resources.GetObject("btnBenefitAllocation.BackgroundImage"),System.Drawing.Image)
Me.btnBenefitAllocation.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
Me.btnBenefitAllocation.BorderColor = System.Drawing.Color.Empty
Me.btnBenefitAllocation.ButtonType = eZee.Common.eZeeLightButton.enButtonType.CheckButton
Me.btnBenefitAllocation.FlatAppearance.BorderSize = 0
Me.btnBenefitAllocation.FlatStyle = System.Windows.Forms.FlatStyle.Flat
Me.btnBenefitAllocation.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
Me.btnBenefitAllocation.ForeColor = System.Drawing.Color.Black
Me.btnBenefitAllocation.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
Me.btnBenefitAllocation.GradientForeColor = System.Drawing.Color.Black
Me.btnBenefitAllocation.HoverGradientBackColor = System.Drawing.Color.Transparent
Me.btnBenefitAllocation.HoverGradientForeColor = System.Drawing.Color.Black
Me.btnBenefitAllocation.Image = Global.Aruti.Main.My.Resources.Resources.benefit_allocation
Me.btnBenefitAllocation.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
Me.btnBenefitAllocation.Location = New System.Drawing.Point(3, 85)
Me.btnBenefitAllocation.MinimumSize = New System.Drawing.Size(150, 35)
Me.btnBenefitAllocation.Name = "btnBenefitAllocation"
Me.btnBenefitAllocation.PressedGradientBackColor = System.Drawing.Color.Transparent
Me.btnBenefitAllocation.PressedGradientForeColor = System.Drawing.Color.Black
Me.btnBenefitAllocation.Size = New System.Drawing.Size(166, 35)
Me.btnBenefitAllocation.TabIndex = 8
Me.btnBenefitAllocation.Text = "&Group Benefit"
Me.btnBenefitAllocation.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
Me.btnBenefitAllocation.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText
Me.btnBenefitAllocation.UseVisualStyleBackColor = false
'
'btnEmployeeBenefit
'
Me.btnEmployeeBenefit.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left)  _
            Or System.Windows.Forms.AnchorStyles.Right),System.Windows.Forms.AnchorStyles)
Me.btnEmployeeBenefit.BackColor = System.Drawing.Color.Transparent
Me.btnEmployeeBenefit.BackgroundImage = CType(resources.GetObject("btnEmployeeBenefit.BackgroundImage"),System.Drawing.Image)
Me.btnEmployeeBenefit.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
Me.btnEmployeeBenefit.BorderColor = System.Drawing.Color.Empty
Me.btnEmployeeBenefit.ButtonType = eZee.Common.eZeeLightButton.enButtonType.CheckButton
Me.btnEmployeeBenefit.FlatAppearance.BorderSize = 0
Me.btnEmployeeBenefit.FlatStyle = System.Windows.Forms.FlatStyle.Flat
Me.btnEmployeeBenefit.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
Me.btnEmployeeBenefit.ForeColor = System.Drawing.Color.Black
Me.btnEmployeeBenefit.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
Me.btnEmployeeBenefit.GradientForeColor = System.Drawing.Color.Black
Me.btnEmployeeBenefit.HoverGradientBackColor = System.Drawing.Color.Transparent
Me.btnEmployeeBenefit.HoverGradientForeColor = System.Drawing.Color.Black
Me.btnEmployeeBenefit.Image = Global.Aruti.Main.My.Resources.Resources.employee_benefit
Me.btnEmployeeBenefit.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
Me.btnEmployeeBenefit.Location = New System.Drawing.Point(3, 126)
Me.btnEmployeeBenefit.MinimumSize = New System.Drawing.Size(150, 35)
Me.btnEmployeeBenefit.Name = "btnEmployeeBenefit"
Me.btnEmployeeBenefit.PressedGradientBackColor = System.Drawing.Color.Transparent
Me.btnEmployeeBenefit.PressedGradientForeColor = System.Drawing.Color.Black
Me.btnEmployeeBenefit.Size = New System.Drawing.Size(166, 35)
Me.btnEmployeeBenefit.TabIndex = 9
Me.btnEmployeeBenefit.Text = "Employee Be&nefit"
Me.btnEmployeeBenefit.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
Me.btnEmployeeBenefit.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText
Me.btnEmployeeBenefit.UseVisualStyleBackColor = false
'
'btnGroupBenefit
'
Me.btnGroupBenefit.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left)  _
            Or System.Windows.Forms.AnchorStyles.Right),System.Windows.Forms.AnchorStyles)
Me.btnGroupBenefit.BackColor = System.Drawing.Color.Transparent
Me.btnGroupBenefit.BackgroundImage = CType(resources.GetObject("btnGroupBenefit.BackgroundImage"),System.Drawing.Image)
Me.btnGroupBenefit.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
Me.btnGroupBenefit.BorderColor = System.Drawing.Color.Empty
Me.btnGroupBenefit.ButtonType = eZee.Common.eZeeLightButton.enButtonType.CheckButton
Me.btnGroupBenefit.FlatAppearance.BorderSize = 0
Me.btnGroupBenefit.FlatStyle = System.Windows.Forms.FlatStyle.Flat
Me.btnGroupBenefit.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
Me.btnGroupBenefit.ForeColor = System.Drawing.Color.Black
Me.btnGroupBenefit.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
Me.btnGroupBenefit.GradientForeColor = System.Drawing.Color.Black
Me.btnGroupBenefit.HoverGradientBackColor = System.Drawing.Color.Transparent
Me.btnGroupBenefit.HoverGradientForeColor = System.Drawing.Color.Black
Me.btnGroupBenefit.Image = Global.Aruti.Main.My.Resources.Resources.group_benefit
Me.btnGroupBenefit.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
Me.btnGroupBenefit.Location = New System.Drawing.Point(3, 167)
Me.btnGroupBenefit.MinimumSize = New System.Drawing.Size(150, 35)
Me.btnGroupBenefit.Name = "btnGroupBenefit"
Me.btnGroupBenefit.PressedGradientBackColor = System.Drawing.Color.Transparent
Me.btnGroupBenefit.PressedGradientForeColor = System.Drawing.Color.Black
Me.btnGroupBenefit.Size = New System.Drawing.Size(166, 35)
Me.btnGroupBenefit.TabIndex = 10
Me.btnGroupBenefit.Text = "&Group Benefit"
Me.btnGroupBenefit.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
Me.btnGroupBenefit.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText
Me.btnGroupBenefit.UseVisualStyleBackColor = false
'
'btnReferee
'
Me.btnReferee.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left)  _
            Or System.Windows.Forms.AnchorStyles.Right),System.Windows.Forms.AnchorStyles)
Me.btnReferee.BackColor = System.Drawing.Color.Transparent
Me.btnReferee.BackgroundImage = CType(resources.GetObject("btnReferee.BackgroundImage"),System.Drawing.Image)
Me.btnReferee.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
Me.btnReferee.BorderColor = System.Drawing.Color.Empty
Me.btnReferee.ButtonType = eZee.Common.eZeeLightButton.enButtonType.CheckButton
Me.btnReferee.FlatAppearance.BorderSize = 0
Me.btnReferee.FlatStyle = System.Windows.Forms.FlatStyle.Flat
Me.btnReferee.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
Me.btnReferee.ForeColor = System.Drawing.Color.Black
Me.btnReferee.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
Me.btnReferee.GradientForeColor = System.Drawing.Color.Black
Me.btnReferee.HoverGradientBackColor = System.Drawing.Color.Transparent
Me.btnReferee.HoverGradientForeColor = System.Drawing.Color.Black
Me.btnReferee.Image = Global.Aruti.Main.My.Resources.Resources.referee
Me.btnReferee.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
Me.btnReferee.Location = New System.Drawing.Point(3, 208)
Me.btnReferee.MinimumSize = New System.Drawing.Size(150, 35)
Me.btnReferee.Name = "btnReferee"
Me.btnReferee.PressedGradientBackColor = System.Drawing.Color.Transparent
Me.btnReferee.PressedGradientForeColor = System.Drawing.Color.Black
Me.btnReferee.Size = New System.Drawing.Size(166, 35)
Me.btnReferee.TabIndex = 11
Me.btnReferee.Text = "Employee &Referee"
Me.btnReferee.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
Me.btnReferee.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText
Me.btnReferee.UseVisualStyleBackColor = false
'
'btnEmployeeAssets
'
Me.btnEmployeeAssets.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left)  _
            Or System.Windows.Forms.AnchorStyles.Right),System.Windows.Forms.AnchorStyles)
Me.btnEmployeeAssets.BackColor = System.Drawing.Color.Transparent
Me.btnEmployeeAssets.BackgroundImage = CType(resources.GetObject("btnEmployeeAssets.BackgroundImage"),System.Drawing.Image)
Me.btnEmployeeAssets.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
Me.btnEmployeeAssets.BorderColor = System.Drawing.Color.Empty
Me.btnEmployeeAssets.ButtonType = eZee.Common.eZeeLightButton.enButtonType.CheckButton
Me.btnEmployeeAssets.FlatAppearance.BorderSize = 0
Me.btnEmployeeAssets.FlatStyle = System.Windows.Forms.FlatStyle.Flat
Me.btnEmployeeAssets.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
Me.btnEmployeeAssets.ForeColor = System.Drawing.Color.Black
Me.btnEmployeeAssets.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
Me.btnEmployeeAssets.GradientForeColor = System.Drawing.Color.Black
Me.btnEmployeeAssets.HoverGradientBackColor = System.Drawing.Color.Transparent
Me.btnEmployeeAssets.HoverGradientForeColor = System.Drawing.Color.Black
Me.btnEmployeeAssets.Image = Global.Aruti.Main.My.Resources.Resources.Asset
Me.btnEmployeeAssets.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
Me.btnEmployeeAssets.Location = New System.Drawing.Point(3, 249)
Me.btnEmployeeAssets.MinimumSize = New System.Drawing.Size(150, 35)
Me.btnEmployeeAssets.Name = "btnEmployeeAssets"
Me.btnEmployeeAssets.PressedGradientBackColor = System.Drawing.Color.Transparent
Me.btnEmployeeAssets.PressedGradientForeColor = System.Drawing.Color.Black
Me.btnEmployeeAssets.Size = New System.Drawing.Size(166, 35)
Me.btnEmployeeAssets.TabIndex = 12
Me.btnEmployeeAssets.Text = "Company &Assets"
Me.btnEmployeeAssets.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
Me.btnEmployeeAssets.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText
Me.btnEmployeeAssets.UseVisualStyleBackColor = false
'
'btnEmployeeSkill
'
Me.btnEmployeeSkill.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left)  _
            Or System.Windows.Forms.AnchorStyles.Right),System.Windows.Forms.AnchorStyles)
Me.btnEmployeeSkill.BackColor = System.Drawing.Color.Transparent
Me.btnEmployeeSkill.BackgroundImage = CType(resources.GetObject("btnEmployeeSkill.BackgroundImage"),System.Drawing.Image)
Me.btnEmployeeSkill.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
Me.btnEmployeeSkill.BorderColor = System.Drawing.Color.Empty
Me.btnEmployeeSkill.ButtonType = eZee.Common.eZeeLightButton.enButtonType.CheckButton
Me.btnEmployeeSkill.FlatAppearance.BorderSize = 0
Me.btnEmployeeSkill.FlatStyle = System.Windows.Forms.FlatStyle.Flat
Me.btnEmployeeSkill.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
Me.btnEmployeeSkill.ForeColor = System.Drawing.Color.Black
Me.btnEmployeeSkill.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
Me.btnEmployeeSkill.GradientForeColor = System.Drawing.Color.Black
Me.btnEmployeeSkill.HoverGradientBackColor = System.Drawing.Color.Transparent
Me.btnEmployeeSkill.HoverGradientForeColor = System.Drawing.Color.Black
Me.btnEmployeeSkill.Image = Global.Aruti.Main.My.Resources.Resources.skill_list
Me.btnEmployeeSkill.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
Me.btnEmployeeSkill.Location = New System.Drawing.Point(3, 290)
Me.btnEmployeeSkill.MinimumSize = New System.Drawing.Size(150, 35)
Me.btnEmployeeSkill.Name = "btnEmployeeSkill"
Me.btnEmployeeSkill.PressedGradientBackColor = System.Drawing.Color.Transparent
Me.btnEmployeeSkill.PressedGradientForeColor = System.Drawing.Color.Black
Me.btnEmployeeSkill.Size = New System.Drawing.Size(166, 35)
Me.btnEmployeeSkill.TabIndex = 13
Me.btnEmployeeSkill.Text = "Employee &Skill"
Me.btnEmployeeSkill.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
Me.btnEmployeeSkill.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText
Me.btnEmployeeSkill.UseVisualStyleBackColor = false
'
'btnEmployeeQualification
'
Me.btnEmployeeQualification.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left)  _
            Or System.Windows.Forms.AnchorStyles.Right),System.Windows.Forms.AnchorStyles)
Me.btnEmployeeQualification.BackColor = System.Drawing.Color.Transparent
Me.btnEmployeeQualification.BackgroundImage = CType(resources.GetObject("btnEmployeeQualification.BackgroundImage"),System.Drawing.Image)
Me.btnEmployeeQualification.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
Me.btnEmployeeQualification.BorderColor = System.Drawing.Color.Empty
Me.btnEmployeeQualification.ButtonType = eZee.Common.eZeeLightButton.enButtonType.CheckButton
Me.btnEmployeeQualification.FlatAppearance.BorderSize = 0
Me.btnEmployeeQualification.FlatStyle = System.Windows.Forms.FlatStyle.Flat
Me.btnEmployeeQualification.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
Me.btnEmployeeQualification.ForeColor = System.Drawing.Color.Black
Me.btnEmployeeQualification.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
Me.btnEmployeeQualification.GradientForeColor = System.Drawing.Color.Black
Me.btnEmployeeQualification.HoverGradientBackColor = System.Drawing.Color.Transparent
Me.btnEmployeeQualification.HoverGradientForeColor = System.Drawing.Color.Black
Me.btnEmployeeQualification.Image = Global.Aruti.Main.My.Resources.Resources.qualification_course
Me.btnEmployeeQualification.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
Me.btnEmployeeQualification.Location = New System.Drawing.Point(3, 331)
Me.btnEmployeeQualification.MinimumSize = New System.Drawing.Size(150, 35)
Me.btnEmployeeQualification.Name = "btnEmployeeQualification"
Me.btnEmployeeQualification.PressedGradientBackColor = System.Drawing.Color.Transparent
Me.btnEmployeeQualification.PressedGradientForeColor = System.Drawing.Color.Black
Me.btnEmployeeQualification.Size = New System.Drawing.Size(166, 35)
Me.btnEmployeeQualification.TabIndex = 14
Me.btnEmployeeQualification.Text = "Employee &Qualification"
Me.btnEmployeeQualification.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
Me.btnEmployeeQualification.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText
Me.btnEmployeeQualification.UseVisualStyleBackColor = false
'
'btnEmployeeExperence
'
Me.btnEmployeeExperence.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left)  _
            Or System.Windows.Forms.AnchorStyles.Right),System.Windows.Forms.AnchorStyles)
Me.btnEmployeeExperence.BackColor = System.Drawing.Color.Transparent
Me.btnEmployeeExperence.BackgroundImage = CType(resources.GetObject("btnEmployeeExperence.BackgroundImage"),System.Drawing.Image)
Me.btnEmployeeExperence.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
Me.btnEmployeeExperence.BorderColor = System.Drawing.Color.Empty
Me.btnEmployeeExperence.ButtonType = eZee.Common.eZeeLightButton.enButtonType.CheckButton
Me.btnEmployeeExperence.FlatAppearance.BorderSize = 0
Me.btnEmployeeExperence.FlatStyle = System.Windows.Forms.FlatStyle.Flat
Me.btnEmployeeExperence.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
Me.btnEmployeeExperence.ForeColor = System.Drawing.Color.Black
Me.btnEmployeeExperence.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
Me.btnEmployeeExperence.GradientForeColor = System.Drawing.Color.Black
Me.btnEmployeeExperence.HoverGradientBackColor = System.Drawing.Color.Transparent
Me.btnEmployeeExperence.HoverGradientForeColor = System.Drawing.Color.Black
Me.btnEmployeeExperence.Image = Global.Aruti.Main.My.Resources.Resources.experience
Me.btnEmployeeExperence.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
Me.btnEmployeeExperence.Location = New System.Drawing.Point(3, 372)
Me.btnEmployeeExperence.MinimumSize = New System.Drawing.Size(150, 35)
Me.btnEmployeeExperence.Name = "btnEmployeeExperence"
Me.btnEmployeeExperence.PressedGradientBackColor = System.Drawing.Color.Transparent
Me.btnEmployeeExperence.PressedGradientForeColor = System.Drawing.Color.Black
Me.btnEmployeeExperence.Size = New System.Drawing.Size(166, 35)
Me.btnEmployeeExperence.TabIndex = 15
Me.btnEmployeeExperence.Text = "Employee Ex&perience"
Me.btnEmployeeExperence.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
Me.btnEmployeeExperence.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText
Me.btnEmployeeExperence.UseVisualStyleBackColor = false
'
'btnEmployeeMovement2
'
Me.btnEmployeeMovement2.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left)  _
            Or System.Windows.Forms.AnchorStyles.Right),System.Windows.Forms.AnchorStyles)
Me.btnEmployeeMovement2.BackColor = System.Drawing.Color.Transparent
Me.btnEmployeeMovement2.BackgroundImage = CType(resources.GetObject("btnEmployeeMovement2.BackgroundImage"),System.Drawing.Image)
Me.btnEmployeeMovement2.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
Me.btnEmployeeMovement2.BorderColor = System.Drawing.Color.Empty
Me.btnEmployeeMovement2.ButtonType = eZee.Common.eZeeLightButton.enButtonType.CheckButton
Me.btnEmployeeMovement2.FlatAppearance.BorderSize = 0
Me.btnEmployeeMovement2.FlatStyle = System.Windows.Forms.FlatStyle.Flat
Me.btnEmployeeMovement2.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
Me.btnEmployeeMovement2.ForeColor = System.Drawing.Color.Black
Me.btnEmployeeMovement2.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
Me.btnEmployeeMovement2.GradientForeColor = System.Drawing.Color.Black
Me.btnEmployeeMovement2.HoverGradientBackColor = System.Drawing.Color.Transparent
Me.btnEmployeeMovement2.HoverGradientForeColor = System.Drawing.Color.Black
Me.btnEmployeeMovement2.Image = Global.Aruti.Main.My.Resources.Resources.employee_movement
Me.btnEmployeeMovement2.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
Me.btnEmployeeMovement2.Location = New System.Drawing.Point(3, 413)
Me.btnEmployeeMovement2.MinimumSize = New System.Drawing.Size(150, 35)
Me.btnEmployeeMovement2.Name = "btnEmployeeMovement2"
Me.btnEmployeeMovement2.PressedGradientBackColor = System.Drawing.Color.Transparent
Me.btnEmployeeMovement2.PressedGradientForeColor = System.Drawing.Color.Black
Me.btnEmployeeMovement2.Size = New System.Drawing.Size(166, 35)
Me.btnEmployeeMovement2.TabIndex = 16
Me.btnEmployeeMovement2.Text = "Employee &Movement"
Me.btnEmployeeMovement2.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
Me.btnEmployeeMovement2.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText
Me.btnEmployeeMovement2.UseVisualStyleBackColor = false
Me.btnEmployeeMovement2.Visible = false
'
'btnEmployeeDiary
'
Me.btnEmployeeDiary.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left)  _
            Or System.Windows.Forms.AnchorStyles.Right),System.Windows.Forms.AnchorStyles)
Me.btnEmployeeDiary.BackColor = System.Drawing.Color.Transparent
Me.btnEmployeeDiary.BackgroundImage = CType(resources.GetObject("btnEmployeeDiary.BackgroundImage"),System.Drawing.Image)
Me.btnEmployeeDiary.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
Me.btnEmployeeDiary.BorderColor = System.Drawing.Color.Empty
Me.btnEmployeeDiary.ButtonType = eZee.Common.eZeeLightButton.enButtonType.CheckButton
Me.btnEmployeeDiary.FlatAppearance.BorderSize = 0
Me.btnEmployeeDiary.FlatStyle = System.Windows.Forms.FlatStyle.Flat
Me.btnEmployeeDiary.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
Me.btnEmployeeDiary.ForeColor = System.Drawing.Color.Black
Me.btnEmployeeDiary.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
Me.btnEmployeeDiary.GradientForeColor = System.Drawing.Color.Black
Me.btnEmployeeDiary.HoverGradientBackColor = System.Drawing.Color.Transparent
Me.btnEmployeeDiary.HoverGradientForeColor = System.Drawing.Color.Black
Me.btnEmployeeDiary.Image = CType(resources.GetObject("btnEmployeeDiary.Image"),System.Drawing.Image)
Me.btnEmployeeDiary.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
Me.btnEmployeeDiary.Location = New System.Drawing.Point(3, 454)
Me.btnEmployeeDiary.MinimumSize = New System.Drawing.Size(150, 35)
Me.btnEmployeeDiary.Name = "btnEmployeeDiary"
Me.btnEmployeeDiary.PressedGradientBackColor = System.Drawing.Color.Transparent
Me.btnEmployeeDiary.PressedGradientForeColor = System.Drawing.Color.Black
Me.btnEmployeeDiary.Size = New System.Drawing.Size(166, 35)
Me.btnEmployeeDiary.TabIndex = 17
Me.btnEmployeeDiary.Text = "Employee D&iary"
Me.btnEmployeeDiary.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
Me.btnEmployeeDiary.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText
Me.btnEmployeeDiary.UseVisualStyleBackColor = false
Me.btnEmployeeDiary.Visible = false
'
'btnEmployeeDataApproval
'
Me.btnEmployeeDataApproval.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left)  _
            Or System.Windows.Forms.AnchorStyles.Right),System.Windows.Forms.AnchorStyles)
Me.btnEmployeeDataApproval.BackColor = System.Drawing.Color.Transparent
Me.btnEmployeeDataApproval.BackgroundImage = CType(resources.GetObject("btnEmployeeDataApproval.BackgroundImage"),System.Drawing.Image)
Me.btnEmployeeDataApproval.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
Me.btnEmployeeDataApproval.BorderColor = System.Drawing.Color.Empty
Me.btnEmployeeDataApproval.ButtonType = eZee.Common.eZeeLightButton.enButtonType.CheckButton
Me.btnEmployeeDataApproval.FlatAppearance.BorderSize = 0
Me.btnEmployeeDataApproval.FlatStyle = System.Windows.Forms.FlatStyle.Flat
Me.btnEmployeeDataApproval.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
Me.btnEmployeeDataApproval.ForeColor = System.Drawing.Color.Black
Me.btnEmployeeDataApproval.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
Me.btnEmployeeDataApproval.GradientForeColor = System.Drawing.Color.Black
Me.btnEmployeeDataApproval.HoverGradientBackColor = System.Drawing.Color.Transparent
Me.btnEmployeeDataApproval.HoverGradientForeColor = System.Drawing.Color.Black
Me.btnEmployeeDataApproval.Image = Global.Aruti.Main.My.Resources.Resources.employee_movement
Me.btnEmployeeDataApproval.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
Me.btnEmployeeDataApproval.Location = New System.Drawing.Point(3, 495)
Me.btnEmployeeDataApproval.MinimumSize = New System.Drawing.Size(150, 35)
Me.btnEmployeeDataApproval.Name = "btnEmployeeDataApproval"
Me.btnEmployeeDataApproval.PressedGradientBackColor = System.Drawing.Color.Transparent
Me.btnEmployeeDataApproval.PressedGradientForeColor = System.Drawing.Color.Black
Me.btnEmployeeDataApproval.Size = New System.Drawing.Size(166, 35)
Me.btnEmployeeDataApproval.TabIndex = 18
Me.btnEmployeeDataApproval.Text = "Employee &Data Approval"
Me.btnEmployeeDataApproval.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
Me.btnEmployeeDataApproval.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText
Me.btnEmployeeDataApproval.UseVisualStyleBackColor = false
'
'fpnlTrainingInfo
'
Me.fpnlTrainingInfo.AutoScroll = true
Me.fpnlTrainingInfo.Controls.Add(Me.btnTrainingPriority)
Me.fpnlTrainingInfo.Controls.Add(Me.btnTrainingScheduling)
Me.fpnlTrainingInfo.Controls.Add(Me.btnTrainingEnrollment)
        Me.fpnlTrainingInfo.Controls.Add(Me.btnTrainingApproverEmployeeMapping)
Me.fpnlTrainingInfo.Controls.Add(Me.btnTrainingAttendance)
Me.fpnlTrainingInfo.Controls.Add(Me.btnTrainingAnalysis)
Me.fpnlTrainingInfo.Location = New System.Drawing.Point(594, 146)
Me.fpnlTrainingInfo.Name = "fpnlTrainingInfo"
Me.fpnlTrainingInfo.Size = New System.Drawing.Size(191, 41)
Me.fpnlTrainingInfo.TabIndex = 5
Me.fpnlTrainingInfo.Visible = false
'
'btnTrainingPriority
'
Me.btnTrainingPriority.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left)  _
            Or System.Windows.Forms.AnchorStyles.Right),System.Windows.Forms.AnchorStyles)
Me.btnTrainingPriority.BackColor = System.Drawing.Color.Transparent
Me.btnTrainingPriority.BackgroundImage = CType(resources.GetObject("btnTrainingPriority.BackgroundImage"),System.Drawing.Image)
Me.btnTrainingPriority.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
Me.btnTrainingPriority.BorderColor = System.Drawing.Color.Empty
Me.btnTrainingPriority.ButtonType = eZee.Common.eZeeLightButton.enButtonType.CheckButton
Me.btnTrainingPriority.FlatAppearance.BorderSize = 0
Me.btnTrainingPriority.FlatStyle = System.Windows.Forms.FlatStyle.Flat
Me.btnTrainingPriority.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
Me.btnTrainingPriority.ForeColor = System.Drawing.Color.Black
Me.btnTrainingPriority.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
Me.btnTrainingPriority.GradientForeColor = System.Drawing.Color.Black
Me.btnTrainingPriority.HoverGradientBackColor = System.Drawing.Color.Transparent
Me.btnTrainingPriority.HoverGradientForeColor = System.Drawing.Color.Black
Me.btnTrainingPriority.Image = Global.Aruti.Main.My.Resources.Resources.TrainingPriority
Me.btnTrainingPriority.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
Me.btnTrainingPriority.Location = New System.Drawing.Point(3, 3)
Me.btnTrainingPriority.MinimumSize = New System.Drawing.Size(150, 35)
Me.btnTrainingPriority.Name = "btnTrainingPriority"
Me.btnTrainingPriority.PressedGradientBackColor = System.Drawing.Color.Transparent
Me.btnTrainingPriority.PressedGradientForeColor = System.Drawing.Color.Black
Me.btnTrainingPriority.Size = New System.Drawing.Size(166, 35)
Me.btnTrainingPriority.TabIndex = 13
Me.btnTrainingPriority.Text = "Training &Needs Analysis"
Me.btnTrainingPriority.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
Me.btnTrainingPriority.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText
Me.btnTrainingPriority.UseVisualStyleBackColor = false
'
'btnTrainingScheduling
'
Me.btnTrainingScheduling.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left)  _
            Or System.Windows.Forms.AnchorStyles.Right),System.Windows.Forms.AnchorStyles)
Me.btnTrainingScheduling.BackColor = System.Drawing.Color.Transparent
Me.btnTrainingScheduling.BackgroundImage = CType(resources.GetObject("btnTrainingScheduling.BackgroundImage"),System.Drawing.Image)
Me.btnTrainingScheduling.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
Me.btnTrainingScheduling.BorderColor = System.Drawing.Color.Empty
Me.btnTrainingScheduling.ButtonType = eZee.Common.eZeeLightButton.enButtonType.CheckButton
Me.btnTrainingScheduling.FlatAppearance.BorderSize = 0
Me.btnTrainingScheduling.FlatStyle = System.Windows.Forms.FlatStyle.Flat
Me.btnTrainingScheduling.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
Me.btnTrainingScheduling.ForeColor = System.Drawing.Color.Black
Me.btnTrainingScheduling.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
Me.btnTrainingScheduling.GradientForeColor = System.Drawing.Color.Black
Me.btnTrainingScheduling.HoverGradientBackColor = System.Drawing.Color.Transparent
Me.btnTrainingScheduling.HoverGradientForeColor = System.Drawing.Color.Black
Me.btnTrainingScheduling.Image = Global.Aruti.Main.My.Resources.Resources.Scheduling
Me.btnTrainingScheduling.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
Me.btnTrainingScheduling.Location = New System.Drawing.Point(3, 44)
Me.btnTrainingScheduling.MinimumSize = New System.Drawing.Size(150, 35)
Me.btnTrainingScheduling.Name = "btnTrainingScheduling"
Me.btnTrainingScheduling.PressedGradientBackColor = System.Drawing.Color.Transparent
Me.btnTrainingScheduling.PressedGradientForeColor = System.Drawing.Color.Black
Me.btnTrainingScheduling.Size = New System.Drawing.Size(166, 35)
Me.btnTrainingScheduling.TabIndex = 9
Me.btnTrainingScheduling.Text = "Training &Scheduling"
Me.btnTrainingScheduling.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
Me.btnTrainingScheduling.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText
Me.btnTrainingScheduling.UseVisualStyleBackColor = false
'
'btnTrainingEnrollment
'
Me.btnTrainingEnrollment.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left)  _
            Or System.Windows.Forms.AnchorStyles.Right),System.Windows.Forms.AnchorStyles)
Me.btnTrainingEnrollment.BackColor = System.Drawing.Color.Transparent
Me.btnTrainingEnrollment.BackgroundImage = CType(resources.GetObject("btnTrainingEnrollment.BackgroundImage"),System.Drawing.Image)
Me.btnTrainingEnrollment.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
Me.btnTrainingEnrollment.BorderColor = System.Drawing.Color.Empty
Me.btnTrainingEnrollment.ButtonType = eZee.Common.eZeeLightButton.enButtonType.CheckButton
Me.btnTrainingEnrollment.FlatAppearance.BorderSize = 0
Me.btnTrainingEnrollment.FlatStyle = System.Windows.Forms.FlatStyle.Flat
Me.btnTrainingEnrollment.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
Me.btnTrainingEnrollment.ForeColor = System.Drawing.Color.Black
Me.btnTrainingEnrollment.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
Me.btnTrainingEnrollment.GradientForeColor = System.Drawing.Color.Black
Me.btnTrainingEnrollment.HoverGradientBackColor = System.Drawing.Color.Transparent
Me.btnTrainingEnrollment.HoverGradientForeColor = System.Drawing.Color.Black
Me.btnTrainingEnrollment.Image = Global.Aruti.Main.My.Resources.Resources.Enrollment
Me.btnTrainingEnrollment.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
Me.btnTrainingEnrollment.Location = New System.Drawing.Point(3, 85)
Me.btnTrainingEnrollment.MinimumSize = New System.Drawing.Size(150, 35)
Me.btnTrainingEnrollment.Name = "btnTrainingEnrollment"
Me.btnTrainingEnrollment.PressedGradientBackColor = System.Drawing.Color.Transparent
Me.btnTrainingEnrollment.PressedGradientForeColor = System.Drawing.Color.Black
Me.btnTrainingEnrollment.Size = New System.Drawing.Size(166, 35)
Me.btnTrainingEnrollment.TabIndex = 10
Me.btnTrainingEnrollment.Text = "Training &Enrollment"
Me.btnTrainingEnrollment.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
Me.btnTrainingEnrollment.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText
Me.btnTrainingEnrollment.UseVisualStyleBackColor = false
'
        'btnTrainingApproverEmployeeMapping
        '
        Me.btnTrainingApproverEmployeeMapping.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
                    Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnTrainingApproverEmployeeMapping.BackColor = System.Drawing.Color.Transparent
        Me.btnTrainingApproverEmployeeMapping.BackgroundImage = CType(resources.GetObject("btnTrainingApproverEmployeeMapping.BackgroundImage"), System.Drawing.Image)
        Me.btnTrainingApproverEmployeeMapping.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.btnTrainingApproverEmployeeMapping.BorderColor = System.Drawing.Color.Empty
        Me.btnTrainingApproverEmployeeMapping.ButtonType = eZee.Common.eZeeLightButton.enButtonType.CheckButton
        Me.btnTrainingApproverEmployeeMapping.FlatAppearance.BorderSize = 0
        Me.btnTrainingApproverEmployeeMapping.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnTrainingApproverEmployeeMapping.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnTrainingApproverEmployeeMapping.ForeColor = System.Drawing.Color.Black
        Me.btnTrainingApproverEmployeeMapping.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.btnTrainingApproverEmployeeMapping.GradientForeColor = System.Drawing.Color.Black
        Me.btnTrainingApproverEmployeeMapping.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.btnTrainingApproverEmployeeMapping.HoverGradientForeColor = System.Drawing.Color.Black
        Me.btnTrainingApproverEmployeeMapping.Image = Global.Aruti.Main.My.Resources.Resources.import_database
        Me.btnTrainingApproverEmployeeMapping.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
Me.btnTrainingApproverEmployeeMapping.Location = New System.Drawing.Point(3, 126)
        Me.btnTrainingApproverEmployeeMapping.MinimumSize = New System.Drawing.Size(150, 35)
        Me.btnTrainingApproverEmployeeMapping.Name = "btnTrainingApproverEmployeeMapping"
        Me.btnTrainingApproverEmployeeMapping.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.btnTrainingApproverEmployeeMapping.PressedGradientForeColor = System.Drawing.Color.Black
        Me.btnTrainingApproverEmployeeMapping.Size = New System.Drawing.Size(166, 35)
        Me.btnTrainingApproverEmployeeMapping.TabIndex = 10
        Me.btnTrainingApproverEmployeeMapping.Text = "Training Approver"
        Me.btnTrainingApproverEmployeeMapping.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.btnTrainingApproverEmployeeMapping.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText
        Me.btnTrainingApproverEmployeeMapping.UseVisualStyleBackColor = False
'
'btnTrainingAttendance
'
Me.btnTrainingAttendance.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left)  _
            Or System.Windows.Forms.AnchorStyles.Right),System.Windows.Forms.AnchorStyles)
Me.btnTrainingAttendance.BackColor = System.Drawing.Color.Transparent
Me.btnTrainingAttendance.BackgroundImage = CType(resources.GetObject("btnTrainingAttendance.BackgroundImage"),System.Drawing.Image)
Me.btnTrainingAttendance.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
Me.btnTrainingAttendance.BorderColor = System.Drawing.Color.Empty
Me.btnTrainingAttendance.ButtonType = eZee.Common.eZeeLightButton.enButtonType.CheckButton
Me.btnTrainingAttendance.FlatAppearance.BorderSize = 0
Me.btnTrainingAttendance.FlatStyle = System.Windows.Forms.FlatStyle.Flat
Me.btnTrainingAttendance.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
Me.btnTrainingAttendance.ForeColor = System.Drawing.Color.Black
Me.btnTrainingAttendance.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
Me.btnTrainingAttendance.GradientForeColor = System.Drawing.Color.Black
Me.btnTrainingAttendance.HoverGradientBackColor = System.Drawing.Color.Transparent
Me.btnTrainingAttendance.HoverGradientForeColor = System.Drawing.Color.Black
Me.btnTrainingAttendance.Image = Global.Aruti.Main.My.Resources.Resources.Attendence
Me.btnTrainingAttendance.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
Me.btnTrainingAttendance.Location = New System.Drawing.Point(3, 167)
Me.btnTrainingAttendance.MinimumSize = New System.Drawing.Size(150, 35)
Me.btnTrainingAttendance.Name = "btnTrainingAttendance"
Me.btnTrainingAttendance.PressedGradientBackColor = System.Drawing.Color.Transparent
Me.btnTrainingAttendance.PressedGradientForeColor = System.Drawing.Color.Black
Me.btnTrainingAttendance.Size = New System.Drawing.Size(166, 35)
Me.btnTrainingAttendance.TabIndex = 11
Me.btnTrainingAttendance.Text = "Training Atten&dance"
Me.btnTrainingAttendance.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
Me.btnTrainingAttendance.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText
Me.btnTrainingAttendance.UseVisualStyleBackColor = false
Me.btnTrainingAttendance.Visible = false
'
'btnTrainingAnalysis
'
Me.btnTrainingAnalysis.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left)  _
            Or System.Windows.Forms.AnchorStyles.Right),System.Windows.Forms.AnchorStyles)
Me.btnTrainingAnalysis.BackColor = System.Drawing.Color.Transparent
Me.btnTrainingAnalysis.BackgroundImage = CType(resources.GetObject("btnTrainingAnalysis.BackgroundImage"),System.Drawing.Image)
Me.btnTrainingAnalysis.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
Me.btnTrainingAnalysis.BorderColor = System.Drawing.Color.Empty
Me.btnTrainingAnalysis.ButtonType = eZee.Common.eZeeLightButton.enButtonType.CheckButton
Me.btnTrainingAnalysis.FlatAppearance.BorderSize = 0
Me.btnTrainingAnalysis.FlatStyle = System.Windows.Forms.FlatStyle.Flat
Me.btnTrainingAnalysis.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
Me.btnTrainingAnalysis.ForeColor = System.Drawing.Color.Black
Me.btnTrainingAnalysis.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
Me.btnTrainingAnalysis.GradientForeColor = System.Drawing.Color.Black
Me.btnTrainingAnalysis.HoverGradientBackColor = System.Drawing.Color.Transparent
Me.btnTrainingAnalysis.HoverGradientForeColor = System.Drawing.Color.Black
Me.btnTrainingAnalysis.Image = Global.Aruti.Main.My.Resources.Resources.Analysis
Me.btnTrainingAnalysis.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
Me.btnTrainingAnalysis.Location = New System.Drawing.Point(3, 208)
Me.btnTrainingAnalysis.MinimumSize = New System.Drawing.Size(150, 35)
Me.btnTrainingAnalysis.Name = "btnTrainingAnalysis"
Me.btnTrainingAnalysis.PressedGradientBackColor = System.Drawing.Color.Transparent
Me.btnTrainingAnalysis.PressedGradientForeColor = System.Drawing.Color.Black
Me.btnTrainingAnalysis.Size = New System.Drawing.Size(166, 35)
Me.btnTrainingAnalysis.TabIndex = 12
Me.btnTrainingAnalysis.Text = "Training Ana&lysis"
Me.btnTrainingAnalysis.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
Me.btnTrainingAnalysis.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText
Me.btnTrainingAnalysis.UseVisualStyleBackColor = false
Me.btnTrainingAnalysis.Visible = false
'
'fpnlUtility
'
Me.fpnlUtility.AutoScroll = true
Me.fpnlUtility.Controls.Add(Me.btnGeneralSettings)
Me.fpnlUtility.Controls.Add(Me.btnReminder)
Me.fpnlUtility.Controls.Add(Me.btnBackup)
Me.fpnlUtility.Controls.Add(Me.btnRestore)
Me.fpnlUtility.Controls.Add(Me.objelLine6)
Me.fpnlUtility.Controls.Add(Me.btnLetterType)
Me.fpnlUtility.Controls.Add(Me.btnLetterTemplate)
Me.fpnlUtility.Controls.Add(Me.btnMailbox)
Me.fpnlUtility.Controls.Add(Me.objelLine7)
Me.fpnlUtility.Controls.Add(Me.btnChangeUser)
Me.fpnlUtility.Controls.Add(Me.btnChangeCompany)
Me.fpnlUtility.Controls.Add(Me.btnChangeDatabase)
Me.fpnlUtility.Controls.Add(Me.objelLine9)
Me.fpnlUtility.Controls.Add(Me.btnCommonExportData)
Me.fpnlUtility.Controls.Add(Me.btnCommonImportData)
Me.fpnlUtility.Controls.Add(Me.btnUserLog)
Me.fpnlUtility.Controls.Add(Me.btnChangePwd)
Me.fpnlUtility.Controls.Add(Me.btnScanAttachments)
Me.fpnlUtility.Controls.Add(Me.objelLine11)
Me.fpnlUtility.Controls.Add(Me.btnMapOrbitParams)
Me.fpnlUtility.Controls.Add(Me.btnReportLanguage)
Me.fpnlUtility.Location = New System.Drawing.Point(791, 3)
Me.fpnlUtility.Name = "fpnlUtility"
Me.fpnlUtility.Size = New System.Drawing.Size(191, 41)
Me.fpnlUtility.TabIndex = 16
Me.fpnlUtility.Visible = false
'
'btnGeneralSettings
'
Me.btnGeneralSettings.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left)  _
            Or System.Windows.Forms.AnchorStyles.Right),System.Windows.Forms.AnchorStyles)
Me.btnGeneralSettings.BackColor = System.Drawing.Color.Transparent
Me.btnGeneralSettings.BackgroundImage = CType(resources.GetObject("btnGeneralSettings.BackgroundImage"),System.Drawing.Image)
Me.btnGeneralSettings.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
Me.btnGeneralSettings.BorderColor = System.Drawing.Color.Empty
Me.btnGeneralSettings.ButtonType = eZee.Common.eZeeLightButton.enButtonType.CheckButton
Me.btnGeneralSettings.FlatAppearance.BorderSize = 0
Me.btnGeneralSettings.FlatStyle = System.Windows.Forms.FlatStyle.Flat
Me.btnGeneralSettings.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
Me.btnGeneralSettings.ForeColor = System.Drawing.Color.Black
Me.btnGeneralSettings.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
Me.btnGeneralSettings.GradientForeColor = System.Drawing.Color.Black
Me.btnGeneralSettings.HoverGradientBackColor = System.Drawing.Color.Transparent
Me.btnGeneralSettings.HoverGradientForeColor = System.Drawing.Color.Black
Me.btnGeneralSettings.Image = CType(resources.GetObject("btnGeneralSettings.Image"),System.Drawing.Image)
Me.btnGeneralSettings.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
Me.btnGeneralSettings.Location = New System.Drawing.Point(3, 3)
Me.btnGeneralSettings.MinimumSize = New System.Drawing.Size(150, 35)
Me.btnGeneralSettings.Name = "btnGeneralSettings"
Me.btnGeneralSettings.PressedGradientBackColor = System.Drawing.Color.Transparent
Me.btnGeneralSettings.PressedGradientForeColor = System.Drawing.Color.Black
Me.btnGeneralSettings.Size = New System.Drawing.Size(166, 35)
Me.btnGeneralSettings.TabIndex = 25
Me.btnGeneralSettings.Text = "General &Settings"
Me.btnGeneralSettings.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
Me.btnGeneralSettings.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText
Me.btnGeneralSettings.UseVisualStyleBackColor = false
'
'btnReminder
'
Me.btnReminder.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left)  _
            Or System.Windows.Forms.AnchorStyles.Right),System.Windows.Forms.AnchorStyles)
Me.btnReminder.BackColor = System.Drawing.Color.Transparent
Me.btnReminder.BackgroundImage = CType(resources.GetObject("btnReminder.BackgroundImage"),System.Drawing.Image)
Me.btnReminder.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
Me.btnReminder.BorderColor = System.Drawing.Color.Empty
Me.btnReminder.ButtonType = eZee.Common.eZeeLightButton.enButtonType.CheckButton
Me.btnReminder.FlatAppearance.BorderSize = 0
Me.btnReminder.FlatStyle = System.Windows.Forms.FlatStyle.Flat
Me.btnReminder.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
Me.btnReminder.ForeColor = System.Drawing.Color.Black
Me.btnReminder.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
Me.btnReminder.GradientForeColor = System.Drawing.Color.Black
Me.btnReminder.HoverGradientBackColor = System.Drawing.Color.Transparent
Me.btnReminder.HoverGradientForeColor = System.Drawing.Color.Black
Me.btnReminder.Image = Global.Aruti.Main.My.Resources.Resources.Reminder
Me.btnReminder.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
Me.btnReminder.Location = New System.Drawing.Point(3, 44)
Me.btnReminder.MinimumSize = New System.Drawing.Size(150, 35)
Me.btnReminder.Name = "btnReminder"
Me.btnReminder.PressedGradientBackColor = System.Drawing.Color.Transparent
Me.btnReminder.PressedGradientForeColor = System.Drawing.Color.Black
Me.btnReminder.Size = New System.Drawing.Size(166, 35)
Me.btnReminder.TabIndex = 18
Me.btnReminder.Text = "&Reminder"
Me.btnReminder.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
Me.btnReminder.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText
Me.btnReminder.UseVisualStyleBackColor = false
'
'btnBackup
'
Me.btnBackup.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left)  _
            Or System.Windows.Forms.AnchorStyles.Right),System.Windows.Forms.AnchorStyles)
Me.btnBackup.BackColor = System.Drawing.Color.Transparent
Me.btnBackup.BackgroundImage = CType(resources.GetObject("btnBackup.BackgroundImage"),System.Drawing.Image)
Me.btnBackup.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
Me.btnBackup.BorderColor = System.Drawing.Color.Empty
Me.btnBackup.ButtonType = eZee.Common.eZeeLightButton.enButtonType.CheckButton
Me.btnBackup.FlatAppearance.BorderSize = 0
Me.btnBackup.FlatStyle = System.Windows.Forms.FlatStyle.Flat
Me.btnBackup.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
Me.btnBackup.ForeColor = System.Drawing.Color.Black
Me.btnBackup.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
Me.btnBackup.GradientForeColor = System.Drawing.Color.Black
Me.btnBackup.HoverGradientBackColor = System.Drawing.Color.Transparent
Me.btnBackup.HoverGradientForeColor = System.Drawing.Color.Black
Me.btnBackup.Image = Global.Aruti.Main.My.Resources.Resources.Backup
Me.btnBackup.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
Me.btnBackup.Location = New System.Drawing.Point(3, 85)
Me.btnBackup.MinimumSize = New System.Drawing.Size(150, 35)
Me.btnBackup.Name = "btnBackup"
Me.btnBackup.PressedGradientBackColor = System.Drawing.Color.Transparent
Me.btnBackup.PressedGradientForeColor = System.Drawing.Color.Black
Me.btnBackup.Size = New System.Drawing.Size(166, 35)
Me.btnBackup.TabIndex = 19
Me.btnBackup.Text = "&Backup"
Me.btnBackup.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
Me.btnBackup.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText
Me.btnBackup.UseVisualStyleBackColor = false
'
'btnRestore
'
Me.btnRestore.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left)  _
            Or System.Windows.Forms.AnchorStyles.Right),System.Windows.Forms.AnchorStyles)
Me.btnRestore.BackColor = System.Drawing.Color.Transparent
Me.btnRestore.BackgroundImage = CType(resources.GetObject("btnRestore.BackgroundImage"),System.Drawing.Image)
Me.btnRestore.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
Me.btnRestore.BorderColor = System.Drawing.Color.Empty
Me.btnRestore.ButtonType = eZee.Common.eZeeLightButton.enButtonType.CheckButton
Me.btnRestore.FlatAppearance.BorderSize = 0
Me.btnRestore.FlatStyle = System.Windows.Forms.FlatStyle.Flat
Me.btnRestore.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
Me.btnRestore.ForeColor = System.Drawing.Color.Black
Me.btnRestore.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
Me.btnRestore.GradientForeColor = System.Drawing.Color.Black
Me.btnRestore.HoverGradientBackColor = System.Drawing.Color.Transparent
Me.btnRestore.HoverGradientForeColor = System.Drawing.Color.Black
Me.btnRestore.Image = Global.Aruti.Main.My.Resources.Resources.restore
Me.btnRestore.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
Me.btnRestore.Location = New System.Drawing.Point(3, 126)
Me.btnRestore.MinimumSize = New System.Drawing.Size(150, 35)
Me.btnRestore.Name = "btnRestore"
Me.btnRestore.PressedGradientBackColor = System.Drawing.Color.Transparent
Me.btnRestore.PressedGradientForeColor = System.Drawing.Color.Black
Me.btnRestore.Size = New System.Drawing.Size(166, 35)
Me.btnRestore.TabIndex = 20
Me.btnRestore.Text = "R&estore"
Me.btnRestore.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
Me.btnRestore.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText
Me.btnRestore.UseVisualStyleBackColor = false
'
'objelLine6
'
Me.objelLine6.LineOrientation = eZee.Common.eZeeLine.Orientation.Horizontal
Me.objelLine6.Location = New System.Drawing.Point(3, 164)
Me.objelLine6.Name = "objelLine6"
Me.objelLine6.Size = New System.Drawing.Size(166, 17)
Me.objelLine6.TabIndex = 17
Me.objelLine6.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
'
'btnLetterType
'
Me.btnLetterType.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left)  _
            Or System.Windows.Forms.AnchorStyles.Right),System.Windows.Forms.AnchorStyles)
Me.btnLetterType.BackColor = System.Drawing.Color.Transparent
Me.btnLetterType.BackgroundImage = CType(resources.GetObject("btnLetterType.BackgroundImage"),System.Drawing.Image)
Me.btnLetterType.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
Me.btnLetterType.BorderColor = System.Drawing.Color.Empty
Me.btnLetterType.ButtonType = eZee.Common.eZeeLightButton.enButtonType.CheckButton
Me.btnLetterType.FlatAppearance.BorderSize = 0
Me.btnLetterType.FlatStyle = System.Windows.Forms.FlatStyle.Flat
Me.btnLetterType.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
Me.btnLetterType.ForeColor = System.Drawing.Color.Black
Me.btnLetterType.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
Me.btnLetterType.GradientForeColor = System.Drawing.Color.Black
Me.btnLetterType.HoverGradientBackColor = System.Drawing.Color.Transparent
Me.btnLetterType.HoverGradientForeColor = System.Drawing.Color.Black
Me.btnLetterType.Image = Global.Aruti.Main.My.Resources.Resources.Letter_Type
Me.btnLetterType.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
Me.btnLetterType.Location = New System.Drawing.Point(3, 184)
Me.btnLetterType.MinimumSize = New System.Drawing.Size(150, 35)
Me.btnLetterType.Name = "btnLetterType"
Me.btnLetterType.PressedGradientBackColor = System.Drawing.Color.Transparent
Me.btnLetterType.PressedGradientForeColor = System.Drawing.Color.Black
Me.btnLetterType.Size = New System.Drawing.Size(166, 35)
Me.btnLetterType.TabIndex = 8
Me.btnLetterType.Text = "&Letter Type"
Me.btnLetterType.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
Me.btnLetterType.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText
Me.btnLetterType.UseVisualStyleBackColor = false
'
'btnLetterTemplate
'
Me.btnLetterTemplate.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left)  _
            Or System.Windows.Forms.AnchorStyles.Right),System.Windows.Forms.AnchorStyles)
Me.btnLetterTemplate.BackColor = System.Drawing.Color.Transparent
Me.btnLetterTemplate.BackgroundImage = CType(resources.GetObject("btnLetterTemplate.BackgroundImage"),System.Drawing.Image)
Me.btnLetterTemplate.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
Me.btnLetterTemplate.BorderColor = System.Drawing.Color.Empty
Me.btnLetterTemplate.ButtonType = eZee.Common.eZeeLightButton.enButtonType.CheckButton
Me.btnLetterTemplate.FlatAppearance.BorderSize = 0
Me.btnLetterTemplate.FlatStyle = System.Windows.Forms.FlatStyle.Flat
Me.btnLetterTemplate.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
Me.btnLetterTemplate.ForeColor = System.Drawing.Color.Black
Me.btnLetterTemplate.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
Me.btnLetterTemplate.GradientForeColor = System.Drawing.Color.Black
Me.btnLetterTemplate.HoverGradientBackColor = System.Drawing.Color.Transparent
Me.btnLetterTemplate.HoverGradientForeColor = System.Drawing.Color.Black
Me.btnLetterTemplate.Image = Global.Aruti.Main.My.Resources.Resources.Letter_Template
Me.btnLetterTemplate.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
Me.btnLetterTemplate.Location = New System.Drawing.Point(3, 225)
Me.btnLetterTemplate.MinimumSize = New System.Drawing.Size(150, 35)
Me.btnLetterTemplate.Name = "btnLetterTemplate"
Me.btnLetterTemplate.PressedGradientBackColor = System.Drawing.Color.Transparent
Me.btnLetterTemplate.PressedGradientForeColor = System.Drawing.Color.Black
Me.btnLetterTemplate.Size = New System.Drawing.Size(166, 35)
Me.btnLetterTemplate.TabIndex = 9
Me.btnLetterTemplate.Text = "Letter &Template"
Me.btnLetterTemplate.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
Me.btnLetterTemplate.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText
Me.btnLetterTemplate.UseVisualStyleBackColor = false
'
'btnMailbox
'
Me.btnMailbox.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left)  _
            Or System.Windows.Forms.AnchorStyles.Right),System.Windows.Forms.AnchorStyles)
Me.btnMailbox.BackColor = System.Drawing.Color.Transparent
Me.btnMailbox.BackgroundImage = CType(resources.GetObject("btnMailbox.BackgroundImage"),System.Drawing.Image)
Me.btnMailbox.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
Me.btnMailbox.BorderColor = System.Drawing.Color.Empty
Me.btnMailbox.ButtonType = eZee.Common.eZeeLightButton.enButtonType.CheckButton
Me.btnMailbox.FlatAppearance.BorderSize = 0
Me.btnMailbox.FlatStyle = System.Windows.Forms.FlatStyle.Flat
Me.btnMailbox.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
Me.btnMailbox.ForeColor = System.Drawing.Color.Black
Me.btnMailbox.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
Me.btnMailbox.GradientForeColor = System.Drawing.Color.Black
Me.btnMailbox.HoverGradientBackColor = System.Drawing.Color.Transparent
Me.btnMailbox.HoverGradientForeColor = System.Drawing.Color.Black
Me.btnMailbox.Image = Global.Aruti.Main.My.Resources.Resources.Mailbox
Me.btnMailbox.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
Me.btnMailbox.Location = New System.Drawing.Point(3, 266)
Me.btnMailbox.MinimumSize = New System.Drawing.Size(150, 35)
Me.btnMailbox.Name = "btnMailbox"
Me.btnMailbox.PressedGradientBackColor = System.Drawing.Color.Transparent
Me.btnMailbox.PressedGradientForeColor = System.Drawing.Color.Black
Me.btnMailbox.Size = New System.Drawing.Size(166, 35)
Me.btnMailbox.TabIndex = 10
Me.btnMailbox.Text = "&Mail Box"
Me.btnMailbox.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
Me.btnMailbox.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText
Me.btnMailbox.UseVisualStyleBackColor = false
'
'objelLine7
'
Me.objelLine7.LineOrientation = eZee.Common.eZeeLine.Orientation.Horizontal
Me.objelLine7.Location = New System.Drawing.Point(3, 304)
Me.objelLine7.Name = "objelLine7"
Me.objelLine7.Size = New System.Drawing.Size(166, 17)
Me.objelLine7.TabIndex = 21
Me.objelLine7.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
'
'btnChangeUser
'
Me.btnChangeUser.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left)  _
            Or System.Windows.Forms.AnchorStyles.Right),System.Windows.Forms.AnchorStyles)
Me.btnChangeUser.BackColor = System.Drawing.Color.Transparent
Me.btnChangeUser.BackgroundImage = CType(resources.GetObject("btnChangeUser.BackgroundImage"),System.Drawing.Image)
Me.btnChangeUser.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
Me.btnChangeUser.BorderColor = System.Drawing.Color.Empty
Me.btnChangeUser.ButtonType = eZee.Common.eZeeLightButton.enButtonType.CheckButton
Me.btnChangeUser.FlatAppearance.BorderSize = 0
Me.btnChangeUser.FlatStyle = System.Windows.Forms.FlatStyle.Flat
Me.btnChangeUser.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
Me.btnChangeUser.ForeColor = System.Drawing.Color.Black
Me.btnChangeUser.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
Me.btnChangeUser.GradientForeColor = System.Drawing.Color.Black
Me.btnChangeUser.HoverGradientBackColor = System.Drawing.Color.Transparent
Me.btnChangeUser.HoverGradientForeColor = System.Drawing.Color.Black
Me.btnChangeUser.Image = Global.Aruti.Main.My.Resources.Resources.change_user
Me.btnChangeUser.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
Me.btnChangeUser.Location = New System.Drawing.Point(3, 324)
Me.btnChangeUser.MinimumSize = New System.Drawing.Size(150, 35)
Me.btnChangeUser.Name = "btnChangeUser"
Me.btnChangeUser.PressedGradientBackColor = System.Drawing.Color.Transparent
Me.btnChangeUser.PressedGradientForeColor = System.Drawing.Color.Black
Me.btnChangeUser.Size = New System.Drawing.Size(166, 35)
Me.btnChangeUser.TabIndex = 22
Me.btnChangeUser.Text = "Change &User"
Me.btnChangeUser.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
Me.btnChangeUser.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText
Me.btnChangeUser.UseVisualStyleBackColor = false
'
'btnChangeCompany
'
Me.btnChangeCompany.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left)  _
            Or System.Windows.Forms.AnchorStyles.Right),System.Windows.Forms.AnchorStyles)
Me.btnChangeCompany.BackColor = System.Drawing.Color.Transparent
Me.btnChangeCompany.BackgroundImage = CType(resources.GetObject("btnChangeCompany.BackgroundImage"),System.Drawing.Image)
Me.btnChangeCompany.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
Me.btnChangeCompany.BorderColor = System.Drawing.Color.Empty
Me.btnChangeCompany.ButtonType = eZee.Common.eZeeLightButton.enButtonType.CheckButton
Me.btnChangeCompany.FlatAppearance.BorderSize = 0
Me.btnChangeCompany.FlatStyle = System.Windows.Forms.FlatStyle.Flat
Me.btnChangeCompany.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
Me.btnChangeCompany.ForeColor = System.Drawing.Color.Black
Me.btnChangeCompany.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
Me.btnChangeCompany.GradientForeColor = System.Drawing.Color.Black
Me.btnChangeCompany.HoverGradientBackColor = System.Drawing.Color.Transparent
Me.btnChangeCompany.HoverGradientForeColor = System.Drawing.Color.Black
Me.btnChangeCompany.Image = Global.Aruti.Main.My.Resources.Resources.change_company
Me.btnChangeCompany.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
Me.btnChangeCompany.Location = New System.Drawing.Point(3, 365)
Me.btnChangeCompany.MinimumSize = New System.Drawing.Size(150, 35)
Me.btnChangeCompany.Name = "btnChangeCompany"
Me.btnChangeCompany.PressedGradientBackColor = System.Drawing.Color.Transparent
Me.btnChangeCompany.PressedGradientForeColor = System.Drawing.Color.Black
Me.btnChangeCompany.Size = New System.Drawing.Size(166, 35)
Me.btnChangeCompany.TabIndex = 23
Me.btnChangeCompany.Text = "&Change Company"
Me.btnChangeCompany.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
Me.btnChangeCompany.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText
Me.btnChangeCompany.UseVisualStyleBackColor = false
'
'btnChangeDatabase
'
Me.btnChangeDatabase.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left)  _
            Or System.Windows.Forms.AnchorStyles.Right),System.Windows.Forms.AnchorStyles)
Me.btnChangeDatabase.BackColor = System.Drawing.Color.Transparent
Me.btnChangeDatabase.BackgroundImage = CType(resources.GetObject("btnChangeDatabase.BackgroundImage"),System.Drawing.Image)
Me.btnChangeDatabase.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
Me.btnChangeDatabase.BorderColor = System.Drawing.Color.Empty
Me.btnChangeDatabase.ButtonType = eZee.Common.eZeeLightButton.enButtonType.CheckButton
Me.btnChangeDatabase.FlatAppearance.BorderSize = 0
Me.btnChangeDatabase.FlatStyle = System.Windows.Forms.FlatStyle.Flat
Me.btnChangeDatabase.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
Me.btnChangeDatabase.ForeColor = System.Drawing.Color.Black
Me.btnChangeDatabase.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
Me.btnChangeDatabase.GradientForeColor = System.Drawing.Color.Black
Me.btnChangeDatabase.HoverGradientBackColor = System.Drawing.Color.Transparent
Me.btnChangeDatabase.HoverGradientForeColor = System.Drawing.Color.Black
Me.btnChangeDatabase.Image = Global.Aruti.Main.My.Resources.Resources.change_database
Me.btnChangeDatabase.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
Me.btnChangeDatabase.Location = New System.Drawing.Point(3, 406)
Me.btnChangeDatabase.MinimumSize = New System.Drawing.Size(150, 35)
Me.btnChangeDatabase.Name = "btnChangeDatabase"
Me.btnChangeDatabase.PressedGradientBackColor = System.Drawing.Color.Transparent
Me.btnChangeDatabase.PressedGradientForeColor = System.Drawing.Color.Black
Me.btnChangeDatabase.Size = New System.Drawing.Size(166, 35)
Me.btnChangeDatabase.TabIndex = 24
Me.btnChangeDatabase.Text = "Change &Database"
Me.btnChangeDatabase.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
Me.btnChangeDatabase.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText
Me.btnChangeDatabase.UseVisualStyleBackColor = false
'
'objelLine9
'
Me.objelLine9.LineOrientation = eZee.Common.eZeeLine.Orientation.Horizontal
Me.objelLine9.Location = New System.Drawing.Point(3, 444)
Me.objelLine9.Name = "objelLine9"
Me.objelLine9.Size = New System.Drawing.Size(166, 17)
Me.objelLine9.TabIndex = 26
Me.objelLine9.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
'
'btnCommonExportData
'
Me.btnCommonExportData.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left)  _
            Or System.Windows.Forms.AnchorStyles.Right),System.Windows.Forms.AnchorStyles)
Me.btnCommonExportData.BackColor = System.Drawing.Color.Transparent
Me.btnCommonExportData.BackgroundImage = CType(resources.GetObject("btnCommonExportData.BackgroundImage"),System.Drawing.Image)
Me.btnCommonExportData.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
Me.btnCommonExportData.BorderColor = System.Drawing.Color.Empty
Me.btnCommonExportData.ButtonType = eZee.Common.eZeeLightButton.enButtonType.CheckButton
Me.btnCommonExportData.FlatAppearance.BorderSize = 0
Me.btnCommonExportData.FlatStyle = System.Windows.Forms.FlatStyle.Flat
Me.btnCommonExportData.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
Me.btnCommonExportData.ForeColor = System.Drawing.Color.Black
Me.btnCommonExportData.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
Me.btnCommonExportData.GradientForeColor = System.Drawing.Color.Black
Me.btnCommonExportData.HoverGradientBackColor = System.Drawing.Color.Transparent
Me.btnCommonExportData.HoverGradientForeColor = System.Drawing.Color.Black
Me.btnCommonExportData.Image = Global.Aruti.Main.My.Resources.Resources.common_export
Me.btnCommonExportData.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
Me.btnCommonExportData.Location = New System.Drawing.Point(3, 464)
Me.btnCommonExportData.MinimumSize = New System.Drawing.Size(150, 35)
Me.btnCommonExportData.Name = "btnCommonExportData"
Me.btnCommonExportData.PressedGradientBackColor = System.Drawing.Color.Transparent
Me.btnCommonExportData.PressedGradientForeColor = System.Drawing.Color.Black
Me.btnCommonExportData.Size = New System.Drawing.Size(166, 35)
Me.btnCommonExportData.TabIndex = 28
Me.btnCommonExportData.Text = "Common &Export"
Me.btnCommonExportData.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
Me.btnCommonExportData.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText
Me.btnCommonExportData.UseVisualStyleBackColor = false
'
'btnCommonImportData
'
Me.btnCommonImportData.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left)  _
            Or System.Windows.Forms.AnchorStyles.Right),System.Windows.Forms.AnchorStyles)
Me.btnCommonImportData.BackColor = System.Drawing.Color.Transparent
Me.btnCommonImportData.BackgroundImage = CType(resources.GetObject("btnCommonImportData.BackgroundImage"),System.Drawing.Image)
Me.btnCommonImportData.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
Me.btnCommonImportData.BorderColor = System.Drawing.Color.Empty
Me.btnCommonImportData.ButtonType = eZee.Common.eZeeLightButton.enButtonType.CheckButton
Me.btnCommonImportData.FlatAppearance.BorderSize = 0
Me.btnCommonImportData.FlatStyle = System.Windows.Forms.FlatStyle.Flat
Me.btnCommonImportData.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
Me.btnCommonImportData.ForeColor = System.Drawing.Color.Black
Me.btnCommonImportData.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
Me.btnCommonImportData.GradientForeColor = System.Drawing.Color.Black
Me.btnCommonImportData.HoverGradientBackColor = System.Drawing.Color.Transparent
Me.btnCommonImportData.HoverGradientForeColor = System.Drawing.Color.Black
Me.btnCommonImportData.Image = Global.Aruti.Main.My.Resources.Resources.common_import
Me.btnCommonImportData.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
Me.btnCommonImportData.Location = New System.Drawing.Point(3, 505)
Me.btnCommonImportData.MinimumSize = New System.Drawing.Size(150, 35)
Me.btnCommonImportData.Name = "btnCommonImportData"
Me.btnCommonImportData.PressedGradientBackColor = System.Drawing.Color.Transparent
Me.btnCommonImportData.PressedGradientForeColor = System.Drawing.Color.Black
Me.btnCommonImportData.Size = New System.Drawing.Size(166, 35)
Me.btnCommonImportData.TabIndex = 27
Me.btnCommonImportData.Text = "Common &Import"
Me.btnCommonImportData.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
Me.btnCommonImportData.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText
Me.btnCommonImportData.UseVisualStyleBackColor = false
'
'btnUserLog
'
Me.btnUserLog.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left)  _
            Or System.Windows.Forms.AnchorStyles.Right),System.Windows.Forms.AnchorStyles)
Me.btnUserLog.BackColor = System.Drawing.Color.Transparent
Me.btnUserLog.BackgroundImage = CType(resources.GetObject("btnUserLog.BackgroundImage"),System.Drawing.Image)
Me.btnUserLog.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
Me.btnUserLog.BorderColor = System.Drawing.Color.Empty
Me.btnUserLog.ButtonType = eZee.Common.eZeeLightButton.enButtonType.CheckButton
Me.btnUserLog.FlatAppearance.BorderSize = 0
Me.btnUserLog.FlatStyle = System.Windows.Forms.FlatStyle.Flat
Me.btnUserLog.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
Me.btnUserLog.ForeColor = System.Drawing.Color.Black
Me.btnUserLog.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
Me.btnUserLog.GradientForeColor = System.Drawing.Color.Black
Me.btnUserLog.HoverGradientBackColor = System.Drawing.Color.Transparent
Me.btnUserLog.HoverGradientForeColor = System.Drawing.Color.Black
Me.btnUserLog.Image = Global.Aruti.Main.My.Resources.Resources.UserLog
Me.btnUserLog.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
Me.btnUserLog.Location = New System.Drawing.Point(3, 546)
Me.btnUserLog.MinimumSize = New System.Drawing.Size(150, 35)
Me.btnUserLog.Name = "btnUserLog"
Me.btnUserLog.PressedGradientBackColor = System.Drawing.Color.Transparent
Me.btnUserLog.PressedGradientForeColor = System.Drawing.Color.Black
Me.btnUserLog.Size = New System.Drawing.Size(166, 35)
Me.btnUserLog.TabIndex = 29
Me.btnUserLog.Text = "&User Log"
Me.btnUserLog.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
Me.btnUserLog.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText
Me.btnUserLog.UseVisualStyleBackColor = false
Me.btnUserLog.Visible = false
'
'btnChangePwd
'
Me.btnChangePwd.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left)  _
            Or System.Windows.Forms.AnchorStyles.Right),System.Windows.Forms.AnchorStyles)
Me.btnChangePwd.BackColor = System.Drawing.Color.Transparent
Me.btnChangePwd.BackgroundImage = CType(resources.GetObject("btnChangePwd.BackgroundImage"),System.Drawing.Image)
Me.btnChangePwd.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
Me.btnChangePwd.BorderColor = System.Drawing.Color.Empty
Me.btnChangePwd.ButtonType = eZee.Common.eZeeLightButton.enButtonType.CheckButton
Me.btnChangePwd.FlatAppearance.BorderSize = 0
Me.btnChangePwd.FlatStyle = System.Windows.Forms.FlatStyle.Flat
Me.btnChangePwd.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
Me.btnChangePwd.ForeColor = System.Drawing.Color.Black
Me.btnChangePwd.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
Me.btnChangePwd.GradientForeColor = System.Drawing.Color.Black
Me.btnChangePwd.HoverGradientBackColor = System.Drawing.Color.Transparent
Me.btnChangePwd.HoverGradientForeColor = System.Drawing.Color.Black
Me.btnChangePwd.Image = Global.Aruti.Main.My.Resources.Resources.UserLog
Me.btnChangePwd.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
Me.btnChangePwd.Location = New System.Drawing.Point(3, 587)
Me.btnChangePwd.MinimumSize = New System.Drawing.Size(150, 35)
Me.btnChangePwd.Name = "btnChangePwd"
Me.btnChangePwd.PressedGradientBackColor = System.Drawing.Color.Transparent
Me.btnChangePwd.PressedGradientForeColor = System.Drawing.Color.Black
Me.btnChangePwd.Size = New System.Drawing.Size(166, 35)
Me.btnChangePwd.TabIndex = 30
Me.btnChangePwd.Text = "&Change Password"
Me.btnChangePwd.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
Me.btnChangePwd.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText
Me.btnChangePwd.UseVisualStyleBackColor = false
'
'btnScanAttachments
'
Me.btnScanAttachments.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left)  _
            Or System.Windows.Forms.AnchorStyles.Right),System.Windows.Forms.AnchorStyles)
Me.btnScanAttachments.BackColor = System.Drawing.Color.Transparent
Me.btnScanAttachments.BackgroundImage = CType(resources.GetObject("btnScanAttachments.BackgroundImage"),System.Drawing.Image)
Me.btnScanAttachments.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
Me.btnScanAttachments.BorderColor = System.Drawing.Color.Empty
Me.btnScanAttachments.ButtonType = eZee.Common.eZeeLightButton.enButtonType.CheckButton
Me.btnScanAttachments.FlatAppearance.BorderSize = 0
Me.btnScanAttachments.FlatStyle = System.Windows.Forms.FlatStyle.Flat
Me.btnScanAttachments.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
Me.btnScanAttachments.ForeColor = System.Drawing.Color.Black
Me.btnScanAttachments.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
Me.btnScanAttachments.GradientForeColor = System.Drawing.Color.Black
Me.btnScanAttachments.HoverGradientBackColor = System.Drawing.Color.Transparent
Me.btnScanAttachments.HoverGradientForeColor = System.Drawing.Color.Black
Me.btnScanAttachments.Image = Global.Aruti.Main.My.Resources.Resources.Letter_Type
Me.btnScanAttachments.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
Me.btnScanAttachments.Location = New System.Drawing.Point(3, 628)
Me.btnScanAttachments.MinimumSize = New System.Drawing.Size(150, 35)
Me.btnScanAttachments.Name = "btnScanAttachments"
Me.btnScanAttachments.PressedGradientBackColor = System.Drawing.Color.Transparent
Me.btnScanAttachments.PressedGradientForeColor = System.Drawing.Color.Black
Me.btnScanAttachments.Size = New System.Drawing.Size(166, 35)
Me.btnScanAttachments.TabIndex = 32
Me.btnScanAttachments.Text = "&Scan/Attachment List"
Me.btnScanAttachments.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
Me.btnScanAttachments.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText
Me.btnScanAttachments.UseVisualStyleBackColor = false
'
'objelLine11
'
Me.objelLine11.LineOrientation = eZee.Common.eZeeLine.Orientation.Horizontal
Me.objelLine11.Location = New System.Drawing.Point(3, 666)
Me.objelLine11.Name = "objelLine11"
Me.objelLine11.Size = New System.Drawing.Size(166, 17)
Me.objelLine11.TabIndex = 33
Me.objelLine11.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
'
'btnMapOrbitParams
'
Me.btnMapOrbitParams.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left)  _
            Or System.Windows.Forms.AnchorStyles.Right),System.Windows.Forms.AnchorStyles)
Me.btnMapOrbitParams.BackColor = System.Drawing.Color.Transparent
Me.btnMapOrbitParams.BackgroundImage = CType(resources.GetObject("btnMapOrbitParams.BackgroundImage"),System.Drawing.Image)
Me.btnMapOrbitParams.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
Me.btnMapOrbitParams.BorderColor = System.Drawing.Color.Empty
Me.btnMapOrbitParams.ButtonType = eZee.Common.eZeeLightButton.enButtonType.CheckButton
Me.btnMapOrbitParams.FlatAppearance.BorderSize = 0
Me.btnMapOrbitParams.FlatStyle = System.Windows.Forms.FlatStyle.Flat
Me.btnMapOrbitParams.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
Me.btnMapOrbitParams.ForeColor = System.Drawing.Color.Black
Me.btnMapOrbitParams.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
Me.btnMapOrbitParams.GradientForeColor = System.Drawing.Color.Black
Me.btnMapOrbitParams.HoverGradientBackColor = System.Drawing.Color.Transparent
Me.btnMapOrbitParams.HoverGradientForeColor = System.Drawing.Color.Black
Me.btnMapOrbitParams.Image = Global.Aruti.Main.My.Resources.Resources.unitgroup
Me.btnMapOrbitParams.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
Me.btnMapOrbitParams.Location = New System.Drawing.Point(3, 686)
Me.btnMapOrbitParams.MinimumSize = New System.Drawing.Size(150, 35)
Me.btnMapOrbitParams.Name = "btnMapOrbitParams"
Me.btnMapOrbitParams.PressedGradientBackColor = System.Drawing.Color.Transparent
Me.btnMapOrbitParams.PressedGradientForeColor = System.Drawing.Color.Black
Me.btnMapOrbitParams.Size = New System.Drawing.Size(166, 35)
Me.btnMapOrbitParams.TabIndex = 34
Me.btnMapOrbitParams.Text = "Map &Orbit Parameters"
Me.btnMapOrbitParams.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
Me.btnMapOrbitParams.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText
Me.btnMapOrbitParams.UseVisualStyleBackColor = false
'
'btnReportLanguage
'
Me.btnReportLanguage.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left)  _
            Or System.Windows.Forms.AnchorStyles.Right),System.Windows.Forms.AnchorStyles)
Me.btnReportLanguage.BackColor = System.Drawing.Color.Transparent
Me.btnReportLanguage.BackgroundImage = CType(resources.GetObject("btnReportLanguage.BackgroundImage"),System.Drawing.Image)
Me.btnReportLanguage.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
Me.btnReportLanguage.BorderColor = System.Drawing.Color.Empty
Me.btnReportLanguage.ButtonType = eZee.Common.eZeeLightButton.enButtonType.CheckButton
Me.btnReportLanguage.FlatAppearance.BorderSize = 0
Me.btnReportLanguage.FlatStyle = System.Windows.Forms.FlatStyle.Flat
Me.btnReportLanguage.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
Me.btnReportLanguage.ForeColor = System.Drawing.Color.Black
Me.btnReportLanguage.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
Me.btnReportLanguage.GradientForeColor = System.Drawing.Color.Black
Me.btnReportLanguage.HoverGradientBackColor = System.Drawing.Color.Transparent
Me.btnReportLanguage.HoverGradientForeColor = System.Drawing.Color.Black
Me.btnReportLanguage.Image = Global.Aruti.Main.My.Resources.Resources.Letter_Type
Me.btnReportLanguage.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
Me.btnReportLanguage.Location = New System.Drawing.Point(3, 727)
Me.btnReportLanguage.MinimumSize = New System.Drawing.Size(150, 35)
Me.btnReportLanguage.Name = "btnReportLanguage"
Me.btnReportLanguage.PressedGradientBackColor = System.Drawing.Color.Transparent
Me.btnReportLanguage.PressedGradientForeColor = System.Drawing.Color.Black
Me.btnReportLanguage.Size = New System.Drawing.Size(166, 35)
Me.btnReportLanguage.TabIndex = 35
Me.btnReportLanguage.Text = "Report Language"
Me.btnReportLanguage.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
Me.btnReportLanguage.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText
Me.btnReportLanguage.UseVisualStyleBackColor = false
'
'fpnlPayrollTransaction
'
Me.fpnlPayrollTransaction.AutoScroll = true
Me.fpnlPayrollTransaction.Controls.Add(Me.btnEmployeeCostCenter)
Me.fpnlPayrollTransaction.Controls.Add(Me.btnEmployeeBanks)
Me.fpnlPayrollTransaction.Controls.Add(Me.btnTransactionHeads)
Me.fpnlPayrollTransaction.Controls.Add(Me.btnBatchPosting)
Me.fpnlPayrollTransaction.Controls.Add(Me.btnBatchTransaction)
Me.fpnlPayrollTransaction.Controls.Add(Me.btnEarningDeduction)
Me.fpnlPayrollTransaction.Controls.Add(Me.btnGlobalAssignED)
Me.fpnlPayrollTransaction.Controls.Add(Me.btnBatchEntry)
Me.fpnlPayrollTransaction.Controls.Add(Me.btnEmployeeExemption)
Me.fpnlPayrollTransaction.Controls.Add(Me.btnProcessPayroll)
Me.fpnlPayrollTransaction.Controls.Add(Me.btnPayslip)
Me.fpnlPayrollTransaction.Controls.Add(Me.btnBudgetPreparation)
Me.fpnlPayrollTransaction.Controls.Add(Me.btnCashDenomination)
Me.fpnlPayrollTransaction.Controls.Add(Me.objelLine3)
Me.fpnlPayrollTransaction.Controls.Add(Me.btnCompanyJV)
Me.fpnlPayrollTransaction.Controls.Add(Me.objelLine4)
Me.fpnlPayrollTransaction.Location = New System.Drawing.Point(791, 50)
Me.fpnlPayrollTransaction.Name = "fpnlPayrollTransaction"
Me.fpnlPayrollTransaction.Size = New System.Drawing.Size(190, 38)
Me.fpnlPayrollTransaction.TabIndex = 8
Me.fpnlPayrollTransaction.Visible = false
'
'btnEmployeeCostCenter
'
Me.btnEmployeeCostCenter.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left)  _
            Or System.Windows.Forms.AnchorStyles.Right),System.Windows.Forms.AnchorStyles)
Me.btnEmployeeCostCenter.BackColor = System.Drawing.Color.Transparent
Me.btnEmployeeCostCenter.BackgroundImage = CType(resources.GetObject("btnEmployeeCostCenter.BackgroundImage"),System.Drawing.Image)
Me.btnEmployeeCostCenter.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
Me.btnEmployeeCostCenter.BorderColor = System.Drawing.Color.Empty
Me.btnEmployeeCostCenter.ButtonType = eZee.Common.eZeeLightButton.enButtonType.CheckButton
Me.btnEmployeeCostCenter.FlatAppearance.BorderSize = 0
Me.btnEmployeeCostCenter.FlatStyle = System.Windows.Forms.FlatStyle.Flat
Me.btnEmployeeCostCenter.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
Me.btnEmployeeCostCenter.ForeColor = System.Drawing.Color.Black
Me.btnEmployeeCostCenter.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
Me.btnEmployeeCostCenter.GradientForeColor = System.Drawing.Color.Black
Me.btnEmployeeCostCenter.HoverGradientBackColor = System.Drawing.Color.Transparent
Me.btnEmployeeCostCenter.HoverGradientForeColor = System.Drawing.Color.Black
Me.btnEmployeeCostCenter.Image = Global.Aruti.Main.My.Resources.Resources.Employee_Cost_Center
Me.btnEmployeeCostCenter.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
Me.btnEmployeeCostCenter.Location = New System.Drawing.Point(3, 3)
Me.btnEmployeeCostCenter.MinimumSize = New System.Drawing.Size(150, 35)
Me.btnEmployeeCostCenter.Name = "btnEmployeeCostCenter"
Me.btnEmployeeCostCenter.PressedGradientBackColor = System.Drawing.Color.Transparent
Me.btnEmployeeCostCenter.PressedGradientForeColor = System.Drawing.Color.Black
Me.btnEmployeeCostCenter.Size = New System.Drawing.Size(166, 35)
Me.btnEmployeeCostCenter.TabIndex = 7
Me.btnEmployeeCostCenter.Text = "&Employee Cost Center"
Me.btnEmployeeCostCenter.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
Me.btnEmployeeCostCenter.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText
Me.btnEmployeeCostCenter.UseVisualStyleBackColor = false
'
'btnEmployeeBanks
'
Me.btnEmployeeBanks.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left)  _
            Or System.Windows.Forms.AnchorStyles.Right),System.Windows.Forms.AnchorStyles)
Me.btnEmployeeBanks.BackColor = System.Drawing.Color.Transparent
Me.btnEmployeeBanks.BackgroundImage = CType(resources.GetObject("btnEmployeeBanks.BackgroundImage"),System.Drawing.Image)
Me.btnEmployeeBanks.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
Me.btnEmployeeBanks.BorderColor = System.Drawing.Color.Empty
Me.btnEmployeeBanks.ButtonType = eZee.Common.eZeeLightButton.enButtonType.CheckButton
Me.btnEmployeeBanks.FlatAppearance.BorderSize = 0
Me.btnEmployeeBanks.FlatStyle = System.Windows.Forms.FlatStyle.Flat
Me.btnEmployeeBanks.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
Me.btnEmployeeBanks.ForeColor = System.Drawing.Color.Black
Me.btnEmployeeBanks.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
Me.btnEmployeeBanks.GradientForeColor = System.Drawing.Color.Black
Me.btnEmployeeBanks.HoverGradientBackColor = System.Drawing.Color.Transparent
Me.btnEmployeeBanks.HoverGradientForeColor = System.Drawing.Color.Black
Me.btnEmployeeBanks.Image = Global.Aruti.Main.My.Resources.Resources.Employee_Bank
Me.btnEmployeeBanks.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
Me.btnEmployeeBanks.Location = New System.Drawing.Point(3, 44)
Me.btnEmployeeBanks.MinimumSize = New System.Drawing.Size(150, 35)
Me.btnEmployeeBanks.Name = "btnEmployeeBanks"
Me.btnEmployeeBanks.PressedGradientBackColor = System.Drawing.Color.Transparent
Me.btnEmployeeBanks.PressedGradientForeColor = System.Drawing.Color.Black
Me.btnEmployeeBanks.Size = New System.Drawing.Size(166, 35)
Me.btnEmployeeBanks.TabIndex = 8
Me.btnEmployeeBanks.Text = "Employee &Banks"
Me.btnEmployeeBanks.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
Me.btnEmployeeBanks.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText
Me.btnEmployeeBanks.UseVisualStyleBackColor = false
'
'btnTransactionHeads
'
Me.btnTransactionHeads.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left)  _
            Or System.Windows.Forms.AnchorStyles.Right),System.Windows.Forms.AnchorStyles)
Me.btnTransactionHeads.BackColor = System.Drawing.Color.Transparent
Me.btnTransactionHeads.BackgroundImage = CType(resources.GetObject("btnTransactionHeads.BackgroundImage"),System.Drawing.Image)
Me.btnTransactionHeads.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
Me.btnTransactionHeads.BorderColor = System.Drawing.Color.Empty
Me.btnTransactionHeads.ButtonType = eZee.Common.eZeeLightButton.enButtonType.CheckButton
Me.btnTransactionHeads.FlatAppearance.BorderSize = 0
Me.btnTransactionHeads.FlatStyle = System.Windows.Forms.FlatStyle.Flat
Me.btnTransactionHeads.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
Me.btnTransactionHeads.ForeColor = System.Drawing.Color.Black
Me.btnTransactionHeads.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
Me.btnTransactionHeads.GradientForeColor = System.Drawing.Color.Black
Me.btnTransactionHeads.HoverGradientBackColor = System.Drawing.Color.Transparent
Me.btnTransactionHeads.HoverGradientForeColor = System.Drawing.Color.Black
Me.btnTransactionHeads.Image = Global.Aruti.Main.My.Resources.Resources.Trnsaction_Head
Me.btnTransactionHeads.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
Me.btnTransactionHeads.Location = New System.Drawing.Point(3, 85)
Me.btnTransactionHeads.MinimumSize = New System.Drawing.Size(150, 35)
Me.btnTransactionHeads.Name = "btnTransactionHeads"
Me.btnTransactionHeads.PressedGradientBackColor = System.Drawing.Color.Transparent
Me.btnTransactionHeads.PressedGradientForeColor = System.Drawing.Color.Black
Me.btnTransactionHeads.Size = New System.Drawing.Size(166, 35)
Me.btnTransactionHeads.TabIndex = 10
Me.btnTransactionHeads.Text = "Transaction &Heads"
Me.btnTransactionHeads.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
Me.btnTransactionHeads.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText
Me.btnTransactionHeads.UseVisualStyleBackColor = false
'
'btnBatchPosting
'
Me.btnBatchPosting.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left)  _
            Or System.Windows.Forms.AnchorStyles.Right),System.Windows.Forms.AnchorStyles)
Me.btnBatchPosting.BackColor = System.Drawing.Color.Transparent
Me.btnBatchPosting.BackgroundImage = CType(resources.GetObject("btnBatchPosting.BackgroundImage"),System.Drawing.Image)
Me.btnBatchPosting.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
Me.btnBatchPosting.BorderColor = System.Drawing.Color.Empty
Me.btnBatchPosting.ButtonType = eZee.Common.eZeeLightButton.enButtonType.CheckButton
Me.btnBatchPosting.FlatAppearance.BorderSize = 0
Me.btnBatchPosting.FlatStyle = System.Windows.Forms.FlatStyle.Flat
Me.btnBatchPosting.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
Me.btnBatchPosting.ForeColor = System.Drawing.Color.Black
Me.btnBatchPosting.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
Me.btnBatchPosting.GradientForeColor = System.Drawing.Color.Black
Me.btnBatchPosting.HoverGradientBackColor = System.Drawing.Color.Transparent
Me.btnBatchPosting.HoverGradientForeColor = System.Drawing.Color.Black
Me.btnBatchPosting.Image = Global.Aruti.Main.My.Resources.Resources._7_Batch_Posting
Me.btnBatchPosting.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
Me.btnBatchPosting.Location = New System.Drawing.Point(3, 126)
Me.btnBatchPosting.MinimumSize = New System.Drawing.Size(150, 35)
Me.btnBatchPosting.Name = "btnBatchPosting"
Me.btnBatchPosting.PressedGradientBackColor = System.Drawing.Color.Transparent
Me.btnBatchPosting.PressedGradientForeColor = System.Drawing.Color.Black
Me.btnBatchPosting.Size = New System.Drawing.Size(166, 35)
Me.btnBatchPosting.TabIndex = 25
Me.btnBatchPosting.Text = "Batch &Posting"
Me.btnBatchPosting.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
Me.btnBatchPosting.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText
Me.btnBatchPosting.UseVisualStyleBackColor = false
'
'btnBatchTransaction
'
Me.btnBatchTransaction.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left)  _
            Or System.Windows.Forms.AnchorStyles.Right),System.Windows.Forms.AnchorStyles)
Me.btnBatchTransaction.BackColor = System.Drawing.Color.Transparent
Me.btnBatchTransaction.BackgroundImage = CType(resources.GetObject("btnBatchTransaction.BackgroundImage"),System.Drawing.Image)
Me.btnBatchTransaction.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
Me.btnBatchTransaction.BorderColor = System.Drawing.Color.Empty
Me.btnBatchTransaction.ButtonType = eZee.Common.eZeeLightButton.enButtonType.CheckButton
Me.btnBatchTransaction.FlatAppearance.BorderSize = 0
Me.btnBatchTransaction.FlatStyle = System.Windows.Forms.FlatStyle.Flat
Me.btnBatchTransaction.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
Me.btnBatchTransaction.ForeColor = System.Drawing.Color.Black
Me.btnBatchTransaction.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
Me.btnBatchTransaction.GradientForeColor = System.Drawing.Color.Black
Me.btnBatchTransaction.HoverGradientBackColor = System.Drawing.Color.Transparent
Me.btnBatchTransaction.HoverGradientForeColor = System.Drawing.Color.Black
Me.btnBatchTransaction.Image = Global.Aruti.Main.My.Resources.Resources.Batch_Trnsaction
Me.btnBatchTransaction.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
Me.btnBatchTransaction.Location = New System.Drawing.Point(3, 167)
Me.btnBatchTransaction.MinimumSize = New System.Drawing.Size(150, 35)
Me.btnBatchTransaction.Name = "btnBatchTransaction"
Me.btnBatchTransaction.PressedGradientBackColor = System.Drawing.Color.Transparent
Me.btnBatchTransaction.PressedGradientForeColor = System.Drawing.Color.Black
Me.btnBatchTransaction.Size = New System.Drawing.Size(166, 35)
Me.btnBatchTransaction.TabIndex = 9
Me.btnBatchTransaction.Text = "Batch &Transaction"
Me.btnBatchTransaction.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
Me.btnBatchTransaction.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText
Me.btnBatchTransaction.UseVisualStyleBackColor = false
'
'btnEarningDeduction
'
Me.btnEarningDeduction.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left)  _
            Or System.Windows.Forms.AnchorStyles.Right),System.Windows.Forms.AnchorStyles)
Me.btnEarningDeduction.BackColor = System.Drawing.Color.Transparent
Me.btnEarningDeduction.BackgroundImage = CType(resources.GetObject("btnEarningDeduction.BackgroundImage"),System.Drawing.Image)
Me.btnEarningDeduction.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
Me.btnEarningDeduction.BorderColor = System.Drawing.Color.Empty
Me.btnEarningDeduction.ButtonType = eZee.Common.eZeeLightButton.enButtonType.CheckButton
Me.btnEarningDeduction.FlatAppearance.BorderSize = 0
Me.btnEarningDeduction.FlatStyle = System.Windows.Forms.FlatStyle.Flat
Me.btnEarningDeduction.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
Me.btnEarningDeduction.ForeColor = System.Drawing.Color.Black
Me.btnEarningDeduction.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
Me.btnEarningDeduction.GradientForeColor = System.Drawing.Color.Black
Me.btnEarningDeduction.HoverGradientBackColor = System.Drawing.Color.Transparent
Me.btnEarningDeduction.HoverGradientForeColor = System.Drawing.Color.Black
Me.btnEarningDeduction.Image = Global.Aruti.Main.My.Resources.Resources.Earning_Deduction
Me.btnEarningDeduction.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
Me.btnEarningDeduction.Location = New System.Drawing.Point(3, 208)
Me.btnEarningDeduction.MinimumSize = New System.Drawing.Size(150, 35)
Me.btnEarningDeduction.Name = "btnEarningDeduction"
Me.btnEarningDeduction.PressedGradientBackColor = System.Drawing.Color.Transparent
Me.btnEarningDeduction.PressedGradientForeColor = System.Drawing.Color.Black
Me.btnEarningDeduction.Size = New System.Drawing.Size(166, 35)
Me.btnEarningDeduction.TabIndex = 11
Me.btnEarningDeduction.Text = "Earning And &Deduction"
Me.btnEarningDeduction.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
Me.btnEarningDeduction.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText
Me.btnEarningDeduction.UseVisualStyleBackColor = false
'
'btnGlobalAssignED
'
Me.btnGlobalAssignED.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left)  _
            Or System.Windows.Forms.AnchorStyles.Right),System.Windows.Forms.AnchorStyles)
Me.btnGlobalAssignED.BackColor = System.Drawing.Color.Transparent
Me.btnGlobalAssignED.BackgroundImage = CType(resources.GetObject("btnGlobalAssignED.BackgroundImage"),System.Drawing.Image)
Me.btnGlobalAssignED.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
Me.btnGlobalAssignED.BorderColor = System.Drawing.Color.Empty
Me.btnGlobalAssignED.ButtonType = eZee.Common.eZeeLightButton.enButtonType.CheckButton
Me.btnGlobalAssignED.FlatAppearance.BorderSize = 0
Me.btnGlobalAssignED.FlatStyle = System.Windows.Forms.FlatStyle.Flat
Me.btnGlobalAssignED.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
Me.btnGlobalAssignED.ForeColor = System.Drawing.Color.Black
Me.btnGlobalAssignED.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
Me.btnGlobalAssignED.GradientForeColor = System.Drawing.Color.Black
Me.btnGlobalAssignED.HoverGradientBackColor = System.Drawing.Color.Transparent
Me.btnGlobalAssignED.HoverGradientForeColor = System.Drawing.Color.Black
Me.btnGlobalAssignED.Image = Global.Aruti.Main.My.Resources.Resources.Global_assign_ED
Me.btnGlobalAssignED.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
Me.btnGlobalAssignED.Location = New System.Drawing.Point(3, 249)
Me.btnGlobalAssignED.MinimumSize = New System.Drawing.Size(150, 35)
Me.btnGlobalAssignED.Name = "btnGlobalAssignED"
Me.btnGlobalAssignED.PressedGradientBackColor = System.Drawing.Color.Transparent
Me.btnGlobalAssignED.PressedGradientForeColor = System.Drawing.Color.Black
Me.btnGlobalAssignED.Size = New System.Drawing.Size(166, 35)
Me.btnGlobalAssignED.TabIndex = 19
Me.btnGlobalAssignED.Text = "&Global Assign ED"
Me.btnGlobalAssignED.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
Me.btnGlobalAssignED.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText
Me.btnGlobalAssignED.UseVisualStyleBackColor = false
'
'btnBatchEntry
'
Me.btnBatchEntry.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left)  _
            Or System.Windows.Forms.AnchorStyles.Right),System.Windows.Forms.AnchorStyles)
Me.btnBatchEntry.BackColor = System.Drawing.Color.Transparent
Me.btnBatchEntry.BackgroundImage = CType(resources.GetObject("btnBatchEntry.BackgroundImage"),System.Drawing.Image)
Me.btnBatchEntry.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
Me.btnBatchEntry.BorderColor = System.Drawing.Color.Empty
Me.btnBatchEntry.ButtonType = eZee.Common.eZeeLightButton.enButtonType.CheckButton
Me.btnBatchEntry.FlatAppearance.BorderSize = 0
Me.btnBatchEntry.FlatStyle = System.Windows.Forms.FlatStyle.Flat
Me.btnBatchEntry.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
Me.btnBatchEntry.ForeColor = System.Drawing.Color.Black
Me.btnBatchEntry.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
Me.btnBatchEntry.GradientForeColor = System.Drawing.Color.Black
Me.btnBatchEntry.HoverGradientBackColor = System.Drawing.Color.Transparent
Me.btnBatchEntry.HoverGradientForeColor = System.Drawing.Color.Black
Me.btnBatchEntry.Image = Global.Aruti.Main.My.Resources.Resources.batch_entry
Me.btnBatchEntry.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
Me.btnBatchEntry.Location = New System.Drawing.Point(3, 290)
Me.btnBatchEntry.MinimumSize = New System.Drawing.Size(150, 35)
Me.btnBatchEntry.Name = "btnBatchEntry"
Me.btnBatchEntry.PressedGradientBackColor = System.Drawing.Color.Transparent
Me.btnBatchEntry.PressedGradientForeColor = System.Drawing.Color.Black
Me.btnBatchEntry.Size = New System.Drawing.Size(166, 35)
Me.btnBatchEntry.TabIndex = 20
Me.btnBatchEntry.Text = "Batch E&ntry"
Me.btnBatchEntry.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
Me.btnBatchEntry.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText
Me.btnBatchEntry.UseVisualStyleBackColor = false
'
'btnEmployeeExemption
'
Me.btnEmployeeExemption.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left)  _
            Or System.Windows.Forms.AnchorStyles.Right),System.Windows.Forms.AnchorStyles)
Me.btnEmployeeExemption.BackColor = System.Drawing.Color.Transparent
Me.btnEmployeeExemption.BackgroundImage = CType(resources.GetObject("btnEmployeeExemption.BackgroundImage"),System.Drawing.Image)
Me.btnEmployeeExemption.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
Me.btnEmployeeExemption.BorderColor = System.Drawing.Color.Empty
Me.btnEmployeeExemption.ButtonType = eZee.Common.eZeeLightButton.enButtonType.CheckButton
Me.btnEmployeeExemption.FlatAppearance.BorderSize = 0
Me.btnEmployeeExemption.FlatStyle = System.Windows.Forms.FlatStyle.Flat
Me.btnEmployeeExemption.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
Me.btnEmployeeExemption.ForeColor = System.Drawing.Color.Black
Me.btnEmployeeExemption.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
Me.btnEmployeeExemption.GradientForeColor = System.Drawing.Color.Black
Me.btnEmployeeExemption.HoverGradientBackColor = System.Drawing.Color.Transparent
Me.btnEmployeeExemption.HoverGradientForeColor = System.Drawing.Color.Black
Me.btnEmployeeExemption.Image = Global.Aruti.Main.My.Resources.Resources.employee_exemption
Me.btnEmployeeExemption.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
Me.btnEmployeeExemption.Location = New System.Drawing.Point(3, 331)
Me.btnEmployeeExemption.MinimumSize = New System.Drawing.Size(150, 35)
Me.btnEmployeeExemption.Name = "btnEmployeeExemption"
Me.btnEmployeeExemption.PressedGradientBackColor = System.Drawing.Color.Transparent
Me.btnEmployeeExemption.PressedGradientForeColor = System.Drawing.Color.Black
Me.btnEmployeeExemption.Size = New System.Drawing.Size(166, 35)
Me.btnEmployeeExemption.TabIndex = 18
Me.btnEmployeeExemption.Text = "Employee Exem&ption"
Me.btnEmployeeExemption.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
Me.btnEmployeeExemption.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText
Me.btnEmployeeExemption.UseVisualStyleBackColor = false
'
'btnProcessPayroll
'
Me.btnProcessPayroll.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left)  _
            Or System.Windows.Forms.AnchorStyles.Right),System.Windows.Forms.AnchorStyles)
Me.btnProcessPayroll.BackColor = System.Drawing.Color.Transparent
Me.btnProcessPayroll.BackgroundImage = CType(resources.GetObject("btnProcessPayroll.BackgroundImage"),System.Drawing.Image)
Me.btnProcessPayroll.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
Me.btnProcessPayroll.BorderColor = System.Drawing.Color.Empty
Me.btnProcessPayroll.ButtonType = eZee.Common.eZeeLightButton.enButtonType.CheckButton
Me.btnProcessPayroll.FlatAppearance.BorderSize = 0
Me.btnProcessPayroll.FlatStyle = System.Windows.Forms.FlatStyle.Flat
Me.btnProcessPayroll.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
Me.btnProcessPayroll.ForeColor = System.Drawing.Color.Black
Me.btnProcessPayroll.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
Me.btnProcessPayroll.GradientForeColor = System.Drawing.Color.Black
Me.btnProcessPayroll.HoverGradientBackColor = System.Drawing.Color.Transparent
Me.btnProcessPayroll.HoverGradientForeColor = System.Drawing.Color.Black
Me.btnProcessPayroll.Image = Global.Aruti.Main.My.Resources.Resources.Payroll_process
Me.btnProcessPayroll.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
Me.btnProcessPayroll.Location = New System.Drawing.Point(3, 372)
Me.btnProcessPayroll.MinimumSize = New System.Drawing.Size(150, 35)
Me.btnProcessPayroll.Name = "btnProcessPayroll"
Me.btnProcessPayroll.PressedGradientBackColor = System.Drawing.Color.Transparent
Me.btnProcessPayroll.PressedGradientForeColor = System.Drawing.Color.Black
Me.btnProcessPayroll.Size = New System.Drawing.Size(166, 35)
Me.btnProcessPayroll.TabIndex = 12
Me.btnProcessPayroll.Text = "Process &Payroll"
Me.btnProcessPayroll.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
Me.btnProcessPayroll.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText
Me.btnProcessPayroll.UseVisualStyleBackColor = false
'
'btnPayslip
'
Me.btnPayslip.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left)  _
            Or System.Windows.Forms.AnchorStyles.Right),System.Windows.Forms.AnchorStyles)
Me.btnPayslip.BackColor = System.Drawing.Color.Transparent
Me.btnPayslip.BackgroundImage = CType(resources.GetObject("btnPayslip.BackgroundImage"),System.Drawing.Image)
Me.btnPayslip.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
Me.btnPayslip.BorderColor = System.Drawing.Color.Empty
Me.btnPayslip.ButtonType = eZee.Common.eZeeLightButton.enButtonType.CheckButton
Me.btnPayslip.FlatAppearance.BorderSize = 0
Me.btnPayslip.FlatStyle = System.Windows.Forms.FlatStyle.Flat
Me.btnPayslip.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
Me.btnPayslip.ForeColor = System.Drawing.Color.Black
Me.btnPayslip.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
Me.btnPayslip.GradientForeColor = System.Drawing.Color.Black
Me.btnPayslip.HoverGradientBackColor = System.Drawing.Color.Transparent
Me.btnPayslip.HoverGradientForeColor = System.Drawing.Color.Black
Me.btnPayslip.Image = Global.Aruti.Main.My.Resources.Resources.payslip
Me.btnPayslip.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
Me.btnPayslip.Location = New System.Drawing.Point(3, 413)
Me.btnPayslip.MinimumSize = New System.Drawing.Size(150, 35)
Me.btnPayslip.Name = "btnPayslip"
Me.btnPayslip.PressedGradientBackColor = System.Drawing.Color.Transparent
Me.btnPayslip.PressedGradientForeColor = System.Drawing.Color.Black
Me.btnPayslip.Size = New System.Drawing.Size(166, 35)
Me.btnPayslip.TabIndex = 13
Me.btnPayslip.Text = "Pa&yslip"
Me.btnPayslip.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
Me.btnPayslip.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText
Me.btnPayslip.UseVisualStyleBackColor = false
'
'btnBudgetPreparation
'
Me.btnBudgetPreparation.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left)  _
            Or System.Windows.Forms.AnchorStyles.Right),System.Windows.Forms.AnchorStyles)
Me.btnBudgetPreparation.BackColor = System.Drawing.Color.Transparent
Me.btnBudgetPreparation.BackgroundImage = CType(resources.GetObject("btnBudgetPreparation.BackgroundImage"),System.Drawing.Image)
Me.btnBudgetPreparation.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
Me.btnBudgetPreparation.BorderColor = System.Drawing.Color.Empty
Me.btnBudgetPreparation.ButtonType = eZee.Common.eZeeLightButton.enButtonType.CheckButton
Me.btnBudgetPreparation.FlatAppearance.BorderSize = 0
Me.btnBudgetPreparation.FlatStyle = System.Windows.Forms.FlatStyle.Flat
Me.btnBudgetPreparation.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
Me.btnBudgetPreparation.ForeColor = System.Drawing.Color.Black
Me.btnBudgetPreparation.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
Me.btnBudgetPreparation.GradientForeColor = System.Drawing.Color.Black
Me.btnBudgetPreparation.HoverGradientBackColor = System.Drawing.Color.Transparent
Me.btnBudgetPreparation.HoverGradientForeColor = System.Drawing.Color.Black
Me.btnBudgetPreparation.Image = Global.Aruti.Main.My.Resources.Resources.Budget_Preparation
Me.btnBudgetPreparation.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
Me.btnBudgetPreparation.Location = New System.Drawing.Point(3, 454)
Me.btnBudgetPreparation.MinimumSize = New System.Drawing.Size(150, 35)
Me.btnBudgetPreparation.Name = "btnBudgetPreparation"
Me.btnBudgetPreparation.PressedGradientBackColor = System.Drawing.Color.Transparent
Me.btnBudgetPreparation.PressedGradientForeColor = System.Drawing.Color.Black
Me.btnBudgetPreparation.Size = New System.Drawing.Size(166, 35)
Me.btnBudgetPreparation.TabIndex = 14
Me.btnBudgetPreparation.Text = "B&udget Preparation"
Me.btnBudgetPreparation.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
Me.btnBudgetPreparation.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText
Me.btnBudgetPreparation.UseVisualStyleBackColor = false
'
'btnCashDenomination
'
Me.btnCashDenomination.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left)  _
            Or System.Windows.Forms.AnchorStyles.Right),System.Windows.Forms.AnchorStyles)
Me.btnCashDenomination.BackColor = System.Drawing.Color.Transparent
Me.btnCashDenomination.BackgroundImage = CType(resources.GetObject("btnCashDenomination.BackgroundImage"),System.Drawing.Image)
Me.btnCashDenomination.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
Me.btnCashDenomination.BorderColor = System.Drawing.Color.Empty
Me.btnCashDenomination.ButtonType = eZee.Common.eZeeLightButton.enButtonType.CheckButton
Me.btnCashDenomination.FlatAppearance.BorderSize = 0
Me.btnCashDenomination.FlatStyle = System.Windows.Forms.FlatStyle.Flat
Me.btnCashDenomination.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
Me.btnCashDenomination.ForeColor = System.Drawing.Color.Black
Me.btnCashDenomination.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
Me.btnCashDenomination.GradientForeColor = System.Drawing.Color.Black
Me.btnCashDenomination.HoverGradientBackColor = System.Drawing.Color.Transparent
Me.btnCashDenomination.HoverGradientForeColor = System.Drawing.Color.Black
Me.btnCashDenomination.Image = Global.Aruti.Main.My.Resources.Resources.cash_denomination
Me.btnCashDenomination.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
Me.btnCashDenomination.Location = New System.Drawing.Point(3, 495)
Me.btnCashDenomination.MinimumSize = New System.Drawing.Size(150, 35)
Me.btnCashDenomination.Name = "btnCashDenomination"
Me.btnCashDenomination.PressedGradientBackColor = System.Drawing.Color.Transparent
Me.btnCashDenomination.PressedGradientForeColor = System.Drawing.Color.Black
Me.btnCashDenomination.Size = New System.Drawing.Size(166, 35)
Me.btnCashDenomination.TabIndex = 24
Me.btnCashDenomination.Text = "Cash &Denomination"
Me.btnCashDenomination.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
Me.btnCashDenomination.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText
Me.btnCashDenomination.UseVisualStyleBackColor = false
'
'objelLine3
'
Me.objelLine3.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
Me.objelLine3.LineOrientation = eZee.Common.eZeeLine.Orientation.Horizontal
Me.objelLine3.Location = New System.Drawing.Point(3, 533)
Me.objelLine3.Name = "objelLine3"
Me.objelLine3.Size = New System.Drawing.Size(166, 17)
Me.objelLine3.TabIndex = 17
Me.objelLine3.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
Me.objelLine3.Visible = false
'
'btnCompanyJV
'
Me.btnCompanyJV.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left)  _
            Or System.Windows.Forms.AnchorStyles.Right),System.Windows.Forms.AnchorStyles)
Me.btnCompanyJV.BackColor = System.Drawing.Color.Transparent
Me.btnCompanyJV.BackgroundImage = CType(resources.GetObject("btnCompanyJV.BackgroundImage"),System.Drawing.Image)
Me.btnCompanyJV.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
Me.btnCompanyJV.BorderColor = System.Drawing.Color.Empty
Me.btnCompanyJV.ButtonType = eZee.Common.eZeeLightButton.enButtonType.CheckButton
Me.btnCompanyJV.FlatAppearance.BorderSize = 0
Me.btnCompanyJV.FlatStyle = System.Windows.Forms.FlatStyle.Flat
Me.btnCompanyJV.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
Me.btnCompanyJV.ForeColor = System.Drawing.Color.Black
Me.btnCompanyJV.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
Me.btnCompanyJV.GradientForeColor = System.Drawing.Color.Black
Me.btnCompanyJV.HoverGradientBackColor = System.Drawing.Color.Transparent
Me.btnCompanyJV.HoverGradientForeColor = System.Drawing.Color.Black
Me.btnCompanyJV.Image = Global.Aruti.Main.My.Resources.Resources.journal_voucher
Me.btnCompanyJV.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
Me.btnCompanyJV.Location = New System.Drawing.Point(3, 553)
Me.btnCompanyJV.MinimumSize = New System.Drawing.Size(150, 35)
Me.btnCompanyJV.Name = "btnCompanyJV"
Me.btnCompanyJV.PressedGradientBackColor = System.Drawing.Color.Transparent
Me.btnCompanyJV.PressedGradientForeColor = System.Drawing.Color.Black
Me.btnCompanyJV.Size = New System.Drawing.Size(166, 35)
Me.btnCompanyJV.TabIndex = 23
Me.btnCompanyJV.Text = "Journal &Voucher"
Me.btnCompanyJV.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
Me.btnCompanyJV.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText
Me.btnCompanyJV.UseVisualStyleBackColor = false
Me.btnCompanyJV.Visible = false
'
'objelLine4
'
Me.objelLine4.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
Me.objelLine4.LineOrientation = eZee.Common.eZeeLine.Orientation.Horizontal
Me.objelLine4.Location = New System.Drawing.Point(3, 591)
Me.objelLine4.Name = "objelLine4"
Me.objelLine4.Size = New System.Drawing.Size(166, 17)
Me.objelLine4.TabIndex = 21
Me.objelLine4.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
'
'fpnlCoreSetups
'
Me.fpnlCoreSetups.AutoScroll = true
Me.fpnlCoreSetups.Controls.Add(Me.btnCommonMaster)
Me.fpnlCoreSetups.Controls.Add(Me.btnMemebership)
Me.fpnlCoreSetups.Controls.Add(Me.btnCountry)
Me.fpnlCoreSetups.Controls.Add(Me.btnState)
Me.fpnlCoreSetups.Controls.Add(Me.btnCity)
Me.fpnlCoreSetups.Controls.Add(Me.btnZipcode)
Me.fpnlCoreSetups.Controls.Add(Me.btnEmpApproverLevel)
Me.fpnlCoreSetups.Controls.Add(Me.btnEmployeeApprover)
Me.fpnlCoreSetups.Controls.Add(Me.btnApproveEmployee)
Me.fpnlCoreSetups.Controls.Add(Me.btnEmployeeData)
Me.fpnlCoreSetups.Controls.Add(Me.btnDependants)
        Me.fpnlCoreSetups.Controls.Add(Me.btnGarnishees)
Me.fpnlCoreSetups.Controls.Add(Me.btnEmployeeMovement)
Me.fpnlCoreSetups.Controls.Add(Me.btnShift)
Me.fpnlCoreSetups.Controls.Add(Me.btnTrainingInstitute)
Me.fpnlCoreSetups.Controls.Add(Me.btnPolicyList)
Me.fpnlCoreSetups.Location = New System.Drawing.Point(791, 94)
Me.fpnlCoreSetups.Name = "fpnlCoreSetups"
Me.fpnlCoreSetups.Size = New System.Drawing.Size(193, 44)
Me.fpnlCoreSetups.TabIndex = 18
Me.fpnlCoreSetups.Visible = false
'
'btnCommonMaster
'
Me.btnCommonMaster.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left)  _
            Or System.Windows.Forms.AnchorStyles.Right),System.Windows.Forms.AnchorStyles)
Me.btnCommonMaster.BackColor = System.Drawing.Color.Transparent
Me.btnCommonMaster.BackgroundImage = CType(resources.GetObject("btnCommonMaster.BackgroundImage"),System.Drawing.Image)
Me.btnCommonMaster.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
Me.btnCommonMaster.BorderColor = System.Drawing.Color.Empty
Me.btnCommonMaster.ButtonType = eZee.Common.eZeeLightButton.enButtonType.CheckButton
Me.btnCommonMaster.FlatAppearance.BorderSize = 0
Me.btnCommonMaster.FlatStyle = System.Windows.Forms.FlatStyle.Flat
Me.btnCommonMaster.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
Me.btnCommonMaster.ForeColor = System.Drawing.Color.Black
Me.btnCommonMaster.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
Me.btnCommonMaster.GradientForeColor = System.Drawing.Color.Black
Me.btnCommonMaster.HoverGradientBackColor = System.Drawing.Color.Transparent
Me.btnCommonMaster.HoverGradientForeColor = System.Drawing.Color.Black
Me.btnCommonMaster.Image = Global.Aruti.Main.My.Resources.Resources.Commom_Master
Me.btnCommonMaster.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
Me.btnCommonMaster.Location = New System.Drawing.Point(3, 3)
Me.btnCommonMaster.MinimumSize = New System.Drawing.Size(150, 35)
Me.btnCommonMaster.Name = "btnCommonMaster"
Me.btnCommonMaster.PressedGradientBackColor = System.Drawing.Color.Transparent
Me.btnCommonMaster.PressedGradientForeColor = System.Drawing.Color.Black
Me.btnCommonMaster.Size = New System.Drawing.Size(166, 35)
Me.btnCommonMaster.TabIndex = 5
Me.btnCommonMaster.Text = "&Common Masters"
Me.btnCommonMaster.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
Me.btnCommonMaster.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText
Me.btnCommonMaster.UseVisualStyleBackColor = false
'
'btnMemebership
'
Me.btnMemebership.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left)  _
            Or System.Windows.Forms.AnchorStyles.Right),System.Windows.Forms.AnchorStyles)
Me.btnMemebership.BackColor = System.Drawing.Color.Transparent
Me.btnMemebership.BackgroundImage = CType(resources.GetObject("btnMemebership.BackgroundImage"),System.Drawing.Image)
Me.btnMemebership.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
Me.btnMemebership.BorderColor = System.Drawing.Color.Empty
Me.btnMemebership.ButtonType = eZee.Common.eZeeLightButton.enButtonType.CheckButton
Me.btnMemebership.FlatAppearance.BorderSize = 0
Me.btnMemebership.FlatStyle = System.Windows.Forms.FlatStyle.Flat
Me.btnMemebership.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
Me.btnMemebership.ForeColor = System.Drawing.Color.Black
Me.btnMemebership.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
Me.btnMemebership.GradientForeColor = System.Drawing.Color.Black
Me.btnMemebership.HoverGradientBackColor = System.Drawing.Color.Transparent
Me.btnMemebership.HoverGradientForeColor = System.Drawing.Color.Black
Me.btnMemebership.Image = Global.Aruti.Main.My.Resources.Resources.membership
Me.btnMemebership.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
Me.btnMemebership.Location = New System.Drawing.Point(3, 44)
Me.btnMemebership.MinimumSize = New System.Drawing.Size(150, 35)
Me.btnMemebership.Name = "btnMemebership"
Me.btnMemebership.PressedGradientBackColor = System.Drawing.Color.Transparent
Me.btnMemebership.PressedGradientForeColor = System.Drawing.Color.Black
Me.btnMemebership.Size = New System.Drawing.Size(166, 35)
Me.btnMemebership.TabIndex = 11
Me.btnMemebership.Text = "&Membership Master"
Me.btnMemebership.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
Me.btnMemebership.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText
Me.btnMemebership.UseVisualStyleBackColor = false
'
'btnCountry
'
Me.btnCountry.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left)  _
            Or System.Windows.Forms.AnchorStyles.Right),System.Windows.Forms.AnchorStyles)
Me.btnCountry.BackColor = System.Drawing.Color.Transparent
Me.btnCountry.BackgroundImage = CType(resources.GetObject("btnCountry.BackgroundImage"),System.Drawing.Image)
Me.btnCountry.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
Me.btnCountry.BorderColor = System.Drawing.Color.Empty
Me.btnCountry.ButtonType = eZee.Common.eZeeLightButton.enButtonType.CheckButton
Me.btnCountry.FlatAppearance.BorderSize = 0
Me.btnCountry.FlatStyle = System.Windows.Forms.FlatStyle.Flat
Me.btnCountry.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
Me.btnCountry.ForeColor = System.Drawing.Color.Black
Me.btnCountry.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
Me.btnCountry.GradientForeColor = System.Drawing.Color.Black
Me.btnCountry.HoverGradientBackColor = System.Drawing.Color.Transparent
Me.btnCountry.HoverGradientForeColor = System.Drawing.Color.Black
Me.btnCountry.Image = Global.Aruti.Main.My.Resources.Resources.state
Me.btnCountry.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
Me.btnCountry.Location = New System.Drawing.Point(3, 85)
Me.btnCountry.MinimumSize = New System.Drawing.Size(150, 35)
Me.btnCountry.Name = "btnCountry"
Me.btnCountry.PressedGradientBackColor = System.Drawing.Color.Transparent
Me.btnCountry.PressedGradientForeColor = System.Drawing.Color.Black
Me.btnCountry.Size = New System.Drawing.Size(166, 35)
Me.btnCountry.TabIndex = 23
Me.btnCountry.Text = "Coun&try"
Me.btnCountry.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
Me.btnCountry.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText
Me.btnCountry.UseVisualStyleBackColor = false
'
'btnState
'
Me.btnState.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left)  _
            Or System.Windows.Forms.AnchorStyles.Right),System.Windows.Forms.AnchorStyles)
Me.btnState.BackColor = System.Drawing.Color.Transparent
Me.btnState.BackgroundImage = CType(resources.GetObject("btnState.BackgroundImage"),System.Drawing.Image)
Me.btnState.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
Me.btnState.BorderColor = System.Drawing.Color.Empty
Me.btnState.ButtonType = eZee.Common.eZeeLightButton.enButtonType.CheckButton
Me.btnState.FlatAppearance.BorderSize = 0
Me.btnState.FlatStyle = System.Windows.Forms.FlatStyle.Flat
Me.btnState.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
Me.btnState.ForeColor = System.Drawing.Color.Black
Me.btnState.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
Me.btnState.GradientForeColor = System.Drawing.Color.Black
Me.btnState.HoverGradientBackColor = System.Drawing.Color.Transparent
Me.btnState.HoverGradientForeColor = System.Drawing.Color.Black
Me.btnState.Image = Global.Aruti.Main.My.Resources.Resources.state
Me.btnState.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
Me.btnState.Location = New System.Drawing.Point(3, 126)
Me.btnState.MinimumSize = New System.Drawing.Size(150, 35)
Me.btnState.Name = "btnState"
Me.btnState.PressedGradientBackColor = System.Drawing.Color.Transparent
Me.btnState.PressedGradientForeColor = System.Drawing.Color.Black
Me.btnState.Size = New System.Drawing.Size(166, 35)
Me.btnState.TabIndex = 13
Me.btnState.Text = "S&tate"
Me.btnState.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
Me.btnState.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText
Me.btnState.UseVisualStyleBackColor = false
'
'btnCity
'
Me.btnCity.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left)  _
            Or System.Windows.Forms.AnchorStyles.Right),System.Windows.Forms.AnchorStyles)
Me.btnCity.BackColor = System.Drawing.Color.Transparent
Me.btnCity.BackgroundImage = CType(resources.GetObject("btnCity.BackgroundImage"),System.Drawing.Image)
Me.btnCity.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
Me.btnCity.BorderColor = System.Drawing.Color.Empty
Me.btnCity.ButtonType = eZee.Common.eZeeLightButton.enButtonType.CheckButton
Me.btnCity.FlatAppearance.BorderSize = 0
Me.btnCity.FlatStyle = System.Windows.Forms.FlatStyle.Flat
Me.btnCity.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
Me.btnCity.ForeColor = System.Drawing.Color.Black
Me.btnCity.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
Me.btnCity.GradientForeColor = System.Drawing.Color.Black
Me.btnCity.HoverGradientBackColor = System.Drawing.Color.Transparent
Me.btnCity.HoverGradientForeColor = System.Drawing.Color.Black
Me.btnCity.Image = Global.Aruti.Main.My.Resources.Resources.City
Me.btnCity.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
Me.btnCity.Location = New System.Drawing.Point(3, 167)
Me.btnCity.MinimumSize = New System.Drawing.Size(150, 35)
Me.btnCity.Name = "btnCity"
Me.btnCity.PressedGradientBackColor = System.Drawing.Color.Transparent
Me.btnCity.PressedGradientForeColor = System.Drawing.Color.Black
Me.btnCity.Size = New System.Drawing.Size(166, 35)
Me.btnCity.TabIndex = 14
Me.btnCity.Text = "&City"
Me.btnCity.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
Me.btnCity.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText
Me.btnCity.UseVisualStyleBackColor = false
'
'btnZipcode
'
Me.btnZipcode.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left)  _
            Or System.Windows.Forms.AnchorStyles.Right),System.Windows.Forms.AnchorStyles)
Me.btnZipcode.BackColor = System.Drawing.Color.Transparent
Me.btnZipcode.BackgroundImage = CType(resources.GetObject("btnZipcode.BackgroundImage"),System.Drawing.Image)
Me.btnZipcode.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
Me.btnZipcode.BorderColor = System.Drawing.Color.Empty
Me.btnZipcode.ButtonType = eZee.Common.eZeeLightButton.enButtonType.CheckButton
Me.btnZipcode.FlatAppearance.BorderSize = 0
Me.btnZipcode.FlatStyle = System.Windows.Forms.FlatStyle.Flat
Me.btnZipcode.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
Me.btnZipcode.ForeColor = System.Drawing.Color.Black
Me.btnZipcode.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
Me.btnZipcode.GradientForeColor = System.Drawing.Color.Black
Me.btnZipcode.HoverGradientBackColor = System.Drawing.Color.Transparent
Me.btnZipcode.HoverGradientForeColor = System.Drawing.Color.Black
Me.btnZipcode.Image = Global.Aruti.Main.My.Resources.Resources.zipcode
Me.btnZipcode.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
Me.btnZipcode.Location = New System.Drawing.Point(3, 208)
Me.btnZipcode.MinimumSize = New System.Drawing.Size(150, 35)
Me.btnZipcode.Name = "btnZipcode"
Me.btnZipcode.PressedGradientBackColor = System.Drawing.Color.Transparent
Me.btnZipcode.PressedGradientForeColor = System.Drawing.Color.Black
Me.btnZipcode.Size = New System.Drawing.Size(166, 35)
Me.btnZipcode.TabIndex = 15
Me.btnZipcode.Text = "&Zipcode"
Me.btnZipcode.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
Me.btnZipcode.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText
Me.btnZipcode.UseVisualStyleBackColor = false
'
'btnEmpApproverLevel
'
Me.btnEmpApproverLevel.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left)  _
            Or System.Windows.Forms.AnchorStyles.Right),System.Windows.Forms.AnchorStyles)
Me.btnEmpApproverLevel.BackColor = System.Drawing.Color.Transparent
Me.btnEmpApproverLevel.BackgroundImage = CType(resources.GetObject("btnEmpApproverLevel.BackgroundImage"),System.Drawing.Image)
Me.btnEmpApproverLevel.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
Me.btnEmpApproverLevel.BorderColor = System.Drawing.Color.Empty
Me.btnEmpApproverLevel.ButtonType = eZee.Common.eZeeLightButton.enButtonType.CheckButton
Me.btnEmpApproverLevel.FlatAppearance.BorderSize = 0
Me.btnEmpApproverLevel.FlatStyle = System.Windows.Forms.FlatStyle.Flat
Me.btnEmpApproverLevel.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
Me.btnEmpApproverLevel.ForeColor = System.Drawing.Color.Black
Me.btnEmpApproverLevel.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
Me.btnEmpApproverLevel.GradientForeColor = System.Drawing.Color.Black
Me.btnEmpApproverLevel.HoverGradientBackColor = System.Drawing.Color.Transparent
Me.btnEmpApproverLevel.HoverGradientForeColor = System.Drawing.Color.Black
Me.btnEmpApproverLevel.Image = Global.Aruti.Main.My.Resources.Resources.Approver_Level
Me.btnEmpApproverLevel.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
Me.btnEmpApproverLevel.Location = New System.Drawing.Point(3, 249)
Me.btnEmpApproverLevel.MinimumSize = New System.Drawing.Size(150, 35)
Me.btnEmpApproverLevel.Name = "btnEmpApproverLevel"
Me.btnEmpApproverLevel.PressedGradientBackColor = System.Drawing.Color.Transparent
Me.btnEmpApproverLevel.PressedGradientForeColor = System.Drawing.Color.Black
Me.btnEmpApproverLevel.Size = New System.Drawing.Size(166, 35)
Me.btnEmpApproverLevel.TabIndex = 20
Me.btnEmpApproverLevel.Text = "&Approver Level"
Me.btnEmpApproverLevel.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
Me.btnEmpApproverLevel.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText
Me.btnEmpApproverLevel.UseVisualStyleBackColor = false
'
'btnEmployeeApprover
'
Me.btnEmployeeApprover.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left)  _
            Or System.Windows.Forms.AnchorStyles.Right),System.Windows.Forms.AnchorStyles)
Me.btnEmployeeApprover.BackColor = System.Drawing.Color.Transparent
Me.btnEmployeeApprover.BackgroundImage = CType(resources.GetObject("btnEmployeeApprover.BackgroundImage"),System.Drawing.Image)
Me.btnEmployeeApprover.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
Me.btnEmployeeApprover.BorderColor = System.Drawing.Color.Empty
Me.btnEmployeeApprover.ButtonType = eZee.Common.eZeeLightButton.enButtonType.CheckButton
Me.btnEmployeeApprover.FlatAppearance.BorderSize = 0
Me.btnEmployeeApprover.FlatStyle = System.Windows.Forms.FlatStyle.Flat
Me.btnEmployeeApprover.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
Me.btnEmployeeApprover.ForeColor = System.Drawing.Color.Black
Me.btnEmployeeApprover.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
Me.btnEmployeeApprover.GradientForeColor = System.Drawing.Color.Black
Me.btnEmployeeApprover.HoverGradientBackColor = System.Drawing.Color.Transparent
Me.btnEmployeeApprover.HoverGradientForeColor = System.Drawing.Color.Black
Me.btnEmployeeApprover.Image = Global.Aruti.Main.My.Resources.Resources.Leave_Approver
Me.btnEmployeeApprover.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
Me.btnEmployeeApprover.Location = New System.Drawing.Point(3, 290)
Me.btnEmployeeApprover.MinimumSize = New System.Drawing.Size(150, 35)
Me.btnEmployeeApprover.Name = "btnEmployeeApprover"
Me.btnEmployeeApprover.PressedGradientBackColor = System.Drawing.Color.Transparent
Me.btnEmployeeApprover.PressedGradientForeColor = System.Drawing.Color.Black
Me.btnEmployeeApprover.Size = New System.Drawing.Size(166, 35)
Me.btnEmployeeApprover.TabIndex = 21
Me.btnEmployeeApprover.Text = "E&mployee Approver"
Me.btnEmployeeApprover.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
Me.btnEmployeeApprover.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText
Me.btnEmployeeApprover.UseVisualStyleBackColor = false
'
'btnApproveEmployee
'
Me.btnApproveEmployee.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left)  _
            Or System.Windows.Forms.AnchorStyles.Right),System.Windows.Forms.AnchorStyles)
Me.btnApproveEmployee.BackColor = System.Drawing.Color.Transparent
Me.btnApproveEmployee.BackgroundImage = CType(resources.GetObject("btnApproveEmployee.BackgroundImage"),System.Drawing.Image)
Me.btnApproveEmployee.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
Me.btnApproveEmployee.BorderColor = System.Drawing.Color.Empty
Me.btnApproveEmployee.ButtonType = eZee.Common.eZeeLightButton.enButtonType.CheckButton
Me.btnApproveEmployee.FlatAppearance.BorderSize = 0
Me.btnApproveEmployee.FlatStyle = System.Windows.Forms.FlatStyle.Flat
Me.btnApproveEmployee.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
Me.btnApproveEmployee.ForeColor = System.Drawing.Color.Black
Me.btnApproveEmployee.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
Me.btnApproveEmployee.GradientForeColor = System.Drawing.Color.Black
Me.btnApproveEmployee.HoverGradientBackColor = System.Drawing.Color.Transparent
Me.btnApproveEmployee.HoverGradientForeColor = System.Drawing.Color.Black
Me.btnApproveEmployee.Image = Global.Aruti.Main.My.Resources.Resources.rc_Requisition_App_Mapping
Me.btnApproveEmployee.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
Me.btnApproveEmployee.Location = New System.Drawing.Point(3, 331)
Me.btnApproveEmployee.MinimumSize = New System.Drawing.Size(150, 35)
Me.btnApproveEmployee.Name = "btnApproveEmployee"
Me.btnApproveEmployee.PressedGradientBackColor = System.Drawing.Color.Transparent
Me.btnApproveEmployee.PressedGradientForeColor = System.Drawing.Color.Black
Me.btnApproveEmployee.Size = New System.Drawing.Size(166, 35)
Me.btnApproveEmployee.TabIndex = 22
Me.btnApproveEmployee.Text = "A&pprove Employee"
Me.btnApproveEmployee.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
Me.btnApproveEmployee.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText
Me.btnApproveEmployee.UseVisualStyleBackColor = false
'
'btnEmployeeData
'
Me.btnEmployeeData.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left)  _
            Or System.Windows.Forms.AnchorStyles.Right),System.Windows.Forms.AnchorStyles)
Me.btnEmployeeData.BackColor = System.Drawing.Color.Transparent
Me.btnEmployeeData.BackgroundImage = CType(resources.GetObject("btnEmployeeData.BackgroundImage"),System.Drawing.Image)
Me.btnEmployeeData.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
Me.btnEmployeeData.BorderColor = System.Drawing.Color.Empty
Me.btnEmployeeData.ButtonType = eZee.Common.eZeeLightButton.enButtonType.CheckButton
Me.btnEmployeeData.FlatAppearance.BorderSize = 0
Me.btnEmployeeData.FlatStyle = System.Windows.Forms.FlatStyle.Flat
Me.btnEmployeeData.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
Me.btnEmployeeData.ForeColor = System.Drawing.Color.Black
Me.btnEmployeeData.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
Me.btnEmployeeData.GradientForeColor = System.Drawing.Color.Black
Me.btnEmployeeData.HoverGradientBackColor = System.Drawing.Color.Transparent
Me.btnEmployeeData.HoverGradientForeColor = System.Drawing.Color.Black
Me.btnEmployeeData.Image = Global.Aruti.Main.My.Resources.Resources.employee
Me.btnEmployeeData.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
Me.btnEmployeeData.Location = New System.Drawing.Point(3, 372)
Me.btnEmployeeData.MinimumSize = New System.Drawing.Size(150, 35)
Me.btnEmployeeData.Name = "btnEmployeeData"
Me.btnEmployeeData.PressedGradientBackColor = System.Drawing.Color.Transparent
Me.btnEmployeeData.PressedGradientForeColor = System.Drawing.Color.Black
Me.btnEmployeeData.Size = New System.Drawing.Size(166, 35)
Me.btnEmployeeData.TabIndex = 16
Me.btnEmployeeData.Text = "&Employee Master"
Me.btnEmployeeData.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
Me.btnEmployeeData.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText
Me.btnEmployeeData.UseVisualStyleBackColor = false
'
'btnDependants
'
Me.btnDependants.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left)  _
            Or System.Windows.Forms.AnchorStyles.Right),System.Windows.Forms.AnchorStyles)
Me.btnDependants.BackColor = System.Drawing.Color.Transparent
Me.btnDependants.BackgroundImage = CType(resources.GetObject("btnDependants.BackgroundImage"),System.Drawing.Image)
Me.btnDependants.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
Me.btnDependants.BorderColor = System.Drawing.Color.Empty
Me.btnDependants.ButtonType = eZee.Common.eZeeLightButton.enButtonType.CheckButton
Me.btnDependants.FlatAppearance.BorderSize = 0
Me.btnDependants.FlatStyle = System.Windows.Forms.FlatStyle.Flat
Me.btnDependants.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
Me.btnDependants.ForeColor = System.Drawing.Color.Black
Me.btnDependants.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
Me.btnDependants.GradientForeColor = System.Drawing.Color.Black
Me.btnDependants.HoverGradientBackColor = System.Drawing.Color.Transparent
Me.btnDependants.HoverGradientForeColor = System.Drawing.Color.Black
Me.btnDependants.Image = Global.Aruti.Main.My.Resources.Resources.dependants
Me.btnDependants.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
Me.btnDependants.Location = New System.Drawing.Point(3, 413)
Me.btnDependants.MinimumSize = New System.Drawing.Size(150, 35)
Me.btnDependants.Name = "btnDependants"
Me.btnDependants.PressedGradientBackColor = System.Drawing.Color.Transparent
Me.btnDependants.PressedGradientForeColor = System.Drawing.Color.Black
Me.btnDependants.Size = New System.Drawing.Size(166, 35)
Me.btnDependants.TabIndex = 17
Me.btnDependants.Text = "&Dependants"
Me.btnDependants.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
Me.btnDependants.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText
Me.btnDependants.UseVisualStyleBackColor = false
'
        'btnGarnishees
        '
        Me.btnGarnishees.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
                    Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnGarnishees.BackColor = System.Drawing.Color.Transparent
        Me.btnGarnishees.BackgroundImage = CType(resources.GetObject("btnGarnishees.BackgroundImage"), System.Drawing.Image)
        Me.btnGarnishees.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.btnGarnishees.BorderColor = System.Drawing.Color.Empty
        Me.btnGarnishees.ButtonType = eZee.Common.eZeeLightButton.enButtonType.CheckButton
        Me.btnGarnishees.FlatAppearance.BorderSize = 0
        Me.btnGarnishees.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnGarnishees.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnGarnishees.ForeColor = System.Drawing.Color.Black
        Me.btnGarnishees.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.btnGarnishees.GradientForeColor = System.Drawing.Color.Black
        Me.btnGarnishees.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.btnGarnishees.HoverGradientForeColor = System.Drawing.Color.Black
        Me.btnGarnishees.Image = Global.Aruti.Main.My.Resources.Resources.dependants
        Me.btnGarnishees.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.btnGarnishees.Location = New System.Drawing.Point(3, 413)
        Me.btnGarnishees.MinimumSize = New System.Drawing.Size(150, 35)
        Me.btnGarnishees.Name = "btnGarnishees"
        Me.btnGarnishees.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.btnGarnishees.PressedGradientForeColor = System.Drawing.Color.Black
        Me.btnGarnishees.Size = New System.Drawing.Size(166, 35)
        Me.btnGarnishees.TabIndex = 17
        Me.btnGarnishees.Text = "&Garnishees"
        Me.btnGarnishees.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.btnGarnishees.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText
        Me.btnGarnishees.UseVisualStyleBackColor = False
'
'btnEmployeeMovement
'
Me.btnEmployeeMovement.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left)  _
            Or System.Windows.Forms.AnchorStyles.Right),System.Windows.Forms.AnchorStyles)
Me.btnEmployeeMovement.BackColor = System.Drawing.Color.Transparent
Me.btnEmployeeMovement.BackgroundImage = CType(resources.GetObject("btnEmployeeMovement.BackgroundImage"),System.Drawing.Image)
Me.btnEmployeeMovement.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
Me.btnEmployeeMovement.BorderColor = System.Drawing.Color.Empty
Me.btnEmployeeMovement.ButtonType = eZee.Common.eZeeLightButton.enButtonType.CheckButton
Me.btnEmployeeMovement.FlatAppearance.BorderSize = 0
Me.btnEmployeeMovement.FlatStyle = System.Windows.Forms.FlatStyle.Flat
Me.btnEmployeeMovement.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
Me.btnEmployeeMovement.ForeColor = System.Drawing.Color.Black
Me.btnEmployeeMovement.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
Me.btnEmployeeMovement.GradientForeColor = System.Drawing.Color.Black
Me.btnEmployeeMovement.HoverGradientBackColor = System.Drawing.Color.Transparent
Me.btnEmployeeMovement.HoverGradientForeColor = System.Drawing.Color.Black
Me.btnEmployeeMovement.Image = Global.Aruti.Main.My.Resources.Resources.employee_movement
Me.btnEmployeeMovement.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
Me.btnEmployeeMovement.Location = New System.Drawing.Point(3, 454)
Me.btnEmployeeMovement.MinimumSize = New System.Drawing.Size(150, 35)
Me.btnEmployeeMovement.Name = "btnEmployeeMovement"
Me.btnEmployeeMovement.PressedGradientBackColor = System.Drawing.Color.Transparent
Me.btnEmployeeMovement.PressedGradientForeColor = System.Drawing.Color.Black
Me.btnEmployeeMovement.Size = New System.Drawing.Size(166, 35)
Me.btnEmployeeMovement.TabIndex = 18
Me.btnEmployeeMovement.Text = "Employee &Movement"
Me.btnEmployeeMovement.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
Me.btnEmployeeMovement.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText
Me.btnEmployeeMovement.UseVisualStyleBackColor = false
Me.btnEmployeeMovement.Visible = false
'
'btnShift
'
Me.btnShift.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left)  _
            Or System.Windows.Forms.AnchorStyles.Right),System.Windows.Forms.AnchorStyles)
Me.btnShift.BackColor = System.Drawing.Color.Transparent
Me.btnShift.BackgroundImage = CType(resources.GetObject("btnShift.BackgroundImage"),System.Drawing.Image)
Me.btnShift.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
Me.btnShift.BorderColor = System.Drawing.Color.Empty
Me.btnShift.ButtonType = eZee.Common.eZeeLightButton.enButtonType.CheckButton
Me.btnShift.FlatAppearance.BorderSize = 0
Me.btnShift.FlatStyle = System.Windows.Forms.FlatStyle.Flat
Me.btnShift.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
Me.btnShift.ForeColor = System.Drawing.Color.Black
Me.btnShift.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
Me.btnShift.GradientForeColor = System.Drawing.Color.Black
Me.btnShift.HoverGradientBackColor = System.Drawing.Color.Transparent
Me.btnShift.HoverGradientForeColor = System.Drawing.Color.Black
Me.btnShift.Image = Global.Aruti.Main.My.Resources.Resources.shift
Me.btnShift.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
Me.btnShift.Location = New System.Drawing.Point(3, 495)
Me.btnShift.MinimumSize = New System.Drawing.Size(150, 35)
Me.btnShift.Name = "btnShift"
Me.btnShift.PressedGradientBackColor = System.Drawing.Color.Transparent
Me.btnShift.PressedGradientForeColor = System.Drawing.Color.Black
Me.btnShift.Size = New System.Drawing.Size(166, 35)
Me.btnShift.TabIndex = 8
Me.btnShift.Text = "&Shift"
Me.btnShift.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
Me.btnShift.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText
Me.btnShift.UseVisualStyleBackColor = false
'
'btnTrainingInstitute
'
Me.btnTrainingInstitute.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left)  _
            Or System.Windows.Forms.AnchorStyles.Right),System.Windows.Forms.AnchorStyles)
Me.btnTrainingInstitute.BackColor = System.Drawing.Color.Transparent
Me.btnTrainingInstitute.BackgroundImage = CType(resources.GetObject("btnTrainingInstitute.BackgroundImage"),System.Drawing.Image)
Me.btnTrainingInstitute.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
Me.btnTrainingInstitute.BorderColor = System.Drawing.Color.Empty
Me.btnTrainingInstitute.ButtonType = eZee.Common.eZeeLightButton.enButtonType.CheckButton
Me.btnTrainingInstitute.FlatAppearance.BorderSize = 0
Me.btnTrainingInstitute.FlatStyle = System.Windows.Forms.FlatStyle.Flat
Me.btnTrainingInstitute.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
Me.btnTrainingInstitute.ForeColor = System.Drawing.Color.Black
Me.btnTrainingInstitute.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
Me.btnTrainingInstitute.GradientForeColor = System.Drawing.Color.Black
Me.btnTrainingInstitute.HoverGradientBackColor = System.Drawing.Color.Transparent
Me.btnTrainingInstitute.HoverGradientForeColor = System.Drawing.Color.Black
Me.btnTrainingInstitute.Image = Global.Aruti.Main.My.Resources.Resources.Institutes
Me.btnTrainingInstitute.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
Me.btnTrainingInstitute.Location = New System.Drawing.Point(3, 536)
Me.btnTrainingInstitute.MinimumSize = New System.Drawing.Size(150, 35)
Me.btnTrainingInstitute.Name = "btnTrainingInstitute"
Me.btnTrainingInstitute.PressedGradientBackColor = System.Drawing.Color.Transparent
Me.btnTrainingInstitute.PressedGradientForeColor = System.Drawing.Color.Black
Me.btnTrainingInstitute.Size = New System.Drawing.Size(166, 35)
Me.btnTrainingInstitute.TabIndex = 8
Me.btnTrainingInstitute.Text = "Training &Institute"
Me.btnTrainingInstitute.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
Me.btnTrainingInstitute.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText
Me.btnTrainingInstitute.UseVisualStyleBackColor = false
'
'btnPolicyList
'
Me.btnPolicyList.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left)  _
            Or System.Windows.Forms.AnchorStyles.Right),System.Windows.Forms.AnchorStyles)
Me.btnPolicyList.BackColor = System.Drawing.Color.Transparent
Me.btnPolicyList.BackgroundImage = CType(resources.GetObject("btnPolicyList.BackgroundImage"),System.Drawing.Image)
Me.btnPolicyList.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
Me.btnPolicyList.BorderColor = System.Drawing.Color.Empty
Me.btnPolicyList.ButtonType = eZee.Common.eZeeLightButton.enButtonType.CheckButton
Me.btnPolicyList.FlatAppearance.BorderSize = 0
Me.btnPolicyList.FlatStyle = System.Windows.Forms.FlatStyle.Flat
Me.btnPolicyList.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
Me.btnPolicyList.ForeColor = System.Drawing.Color.Black
Me.btnPolicyList.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
Me.btnPolicyList.GradientForeColor = System.Drawing.Color.Black
Me.btnPolicyList.HoverGradientBackColor = System.Drawing.Color.Transparent
Me.btnPolicyList.HoverGradientForeColor = System.Drawing.Color.Black
Me.btnPolicyList.Image = Global.Aruti.Main.My.Resources.Resources.policy
Me.btnPolicyList.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
Me.btnPolicyList.Location = New System.Drawing.Point(3, 577)
Me.btnPolicyList.MinimumSize = New System.Drawing.Size(150, 35)
Me.btnPolicyList.Name = "btnPolicyList"
Me.btnPolicyList.PressedGradientBackColor = System.Drawing.Color.Transparent
Me.btnPolicyList.PressedGradientForeColor = System.Drawing.Color.Black
Me.btnPolicyList.Size = New System.Drawing.Size(166, 35)
Me.btnPolicyList.TabIndex = 19
Me.btnPolicyList.Text = "Policy"
Me.btnPolicyList.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
Me.btnPolicyList.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText
Me.btnPolicyList.UseVisualStyleBackColor = false
'
'fpnlBSC
'
Me.fpnlBSC.AutoScroll = true
Me.fpnlBSC.Controls.Add(Me.btnWeightedSetting)
Me.fpnlBSC.Controls.Add(Me.btnObjectiveList)
Me.fpnlBSC.Controls.Add(Me.btnKPIMeasureList)
Me.fpnlBSC.Controls.Add(Me.btnTargetsList)
Me.fpnlBSC.Controls.Add(Me.btnInitiativeList)
Me.fpnlBSC.Controls.Add(Me.btnSelfAssessedBSC)
Me.fpnlBSC.Controls.Add(Me.btnAssessorBSCList)
Me.fpnlBSC.Controls.Add(Me.btnReviewerBSCList)
Me.fpnlBSC.Location = New System.Drawing.Point(791, 144)
Me.fpnlBSC.Name = "fpnlBSC"
Me.fpnlBSC.Size = New System.Drawing.Size(190, 40)
Me.fpnlBSC.TabIndex = 19
Me.fpnlBSC.Visible = false
'
'btnWeightedSetting
'
Me.btnWeightedSetting.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left)  _
            Or System.Windows.Forms.AnchorStyles.Right),System.Windows.Forms.AnchorStyles)
Me.btnWeightedSetting.BackColor = System.Drawing.Color.Transparent
Me.btnWeightedSetting.BackgroundImage = CType(resources.GetObject("btnWeightedSetting.BackgroundImage"),System.Drawing.Image)
Me.btnWeightedSetting.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
Me.btnWeightedSetting.BorderColor = System.Drawing.Color.Empty
Me.btnWeightedSetting.ButtonType = eZee.Common.eZeeLightButton.enButtonType.CheckButton
Me.btnWeightedSetting.FlatAppearance.BorderSize = 0
Me.btnWeightedSetting.FlatStyle = System.Windows.Forms.FlatStyle.Flat
Me.btnWeightedSetting.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
Me.btnWeightedSetting.ForeColor = System.Drawing.Color.Black
Me.btnWeightedSetting.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
Me.btnWeightedSetting.GradientForeColor = System.Drawing.Color.Black
Me.btnWeightedSetting.HoverGradientBackColor = System.Drawing.Color.Transparent
Me.btnWeightedSetting.HoverGradientForeColor = System.Drawing.Color.Black
Me.btnWeightedSetting.Image = Global.Aruti.Main.My.Resources.Resources.weight_setting_24
Me.btnWeightedSetting.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
Me.btnWeightedSetting.Location = New System.Drawing.Point(3, 3)
Me.btnWeightedSetting.MinimumSize = New System.Drawing.Size(150, 35)
Me.btnWeightedSetting.Name = "btnWeightedSetting"
Me.btnWeightedSetting.PressedGradientBackColor = System.Drawing.Color.Transparent
Me.btnWeightedSetting.PressedGradientForeColor = System.Drawing.Color.Black
Me.btnWeightedSetting.Size = New System.Drawing.Size(166, 35)
Me.btnWeightedSetting.TabIndex = 20
Me.btnWeightedSetting.Text = "&Weightage Setting"
Me.btnWeightedSetting.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
Me.btnWeightedSetting.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText
Me.btnWeightedSetting.UseVisualStyleBackColor = false
'
'btnObjectiveList
'
Me.btnObjectiveList.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left)  _
            Or System.Windows.Forms.AnchorStyles.Right),System.Windows.Forms.AnchorStyles)
Me.btnObjectiveList.BackColor = System.Drawing.Color.Transparent
Me.btnObjectiveList.BackgroundImage = CType(resources.GetObject("btnObjectiveList.BackgroundImage"),System.Drawing.Image)
Me.btnObjectiveList.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
Me.btnObjectiveList.BorderColor = System.Drawing.Color.Empty
Me.btnObjectiveList.ButtonType = eZee.Common.eZeeLightButton.enButtonType.CheckButton
Me.btnObjectiveList.FlatAppearance.BorderSize = 0
Me.btnObjectiveList.FlatStyle = System.Windows.Forms.FlatStyle.Flat
Me.btnObjectiveList.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
Me.btnObjectiveList.ForeColor = System.Drawing.Color.Black
Me.btnObjectiveList.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
Me.btnObjectiveList.GradientForeColor = System.Drawing.Color.Black
Me.btnObjectiveList.HoverGradientBackColor = System.Drawing.Color.Transparent
Me.btnObjectiveList.HoverGradientForeColor = System.Drawing.Color.Black
Me.btnObjectiveList.Image = Global.Aruti.Main.My.Resources.Resources.targets
Me.btnObjectiveList.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
Me.btnObjectiveList.Location = New System.Drawing.Point(3, 44)
Me.btnObjectiveList.MinimumSize = New System.Drawing.Size(150, 35)
Me.btnObjectiveList.Name = "btnObjectiveList"
Me.btnObjectiveList.PressedGradientBackColor = System.Drawing.Color.Transparent
Me.btnObjectiveList.PressedGradientForeColor = System.Drawing.Color.Black
Me.btnObjectiveList.Size = New System.Drawing.Size(166, 35)
Me.btnObjectiveList.TabIndex = 9
Me.btnObjectiveList.Text = "&Objective List"
Me.btnObjectiveList.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
Me.btnObjectiveList.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText
Me.btnObjectiveList.UseVisualStyleBackColor = false
'
'btnKPIMeasureList
'
Me.btnKPIMeasureList.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left)  _
            Or System.Windows.Forms.AnchorStyles.Right),System.Windows.Forms.AnchorStyles)
Me.btnKPIMeasureList.BackColor = System.Drawing.Color.Transparent
Me.btnKPIMeasureList.BackgroundImage = CType(resources.GetObject("btnKPIMeasureList.BackgroundImage"),System.Drawing.Image)
Me.btnKPIMeasureList.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
Me.btnKPIMeasureList.BorderColor = System.Drawing.Color.Empty
Me.btnKPIMeasureList.ButtonType = eZee.Common.eZeeLightButton.enButtonType.CheckButton
Me.btnKPIMeasureList.FlatAppearance.BorderSize = 0
Me.btnKPIMeasureList.FlatStyle = System.Windows.Forms.FlatStyle.Flat
Me.btnKPIMeasureList.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
Me.btnKPIMeasureList.ForeColor = System.Drawing.Color.Black
Me.btnKPIMeasureList.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
Me.btnKPIMeasureList.GradientForeColor = System.Drawing.Color.Black
Me.btnKPIMeasureList.HoverGradientBackColor = System.Drawing.Color.Transparent
Me.btnKPIMeasureList.HoverGradientForeColor = System.Drawing.Color.Black
Me.btnKPIMeasureList.Image = Global.Aruti.Main.My.Resources.Resources.KPI
Me.btnKPIMeasureList.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
Me.btnKPIMeasureList.Location = New System.Drawing.Point(3, 85)
Me.btnKPIMeasureList.MinimumSize = New System.Drawing.Size(150, 35)
Me.btnKPIMeasureList.Name = "btnKPIMeasureList"
Me.btnKPIMeasureList.PressedGradientBackColor = System.Drawing.Color.Transparent
Me.btnKPIMeasureList.PressedGradientForeColor = System.Drawing.Color.Black
Me.btnKPIMeasureList.Size = New System.Drawing.Size(166, 35)
Me.btnKPIMeasureList.TabIndex = 14
Me.btnKPIMeasureList.Text = "&KPI/Measure"
Me.btnKPIMeasureList.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
Me.btnKPIMeasureList.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText
Me.btnKPIMeasureList.UseVisualStyleBackColor = false
'
'btnTargetsList
'
Me.btnTargetsList.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left)  _
            Or System.Windows.Forms.AnchorStyles.Right),System.Windows.Forms.AnchorStyles)
Me.btnTargetsList.BackColor = System.Drawing.Color.Transparent
Me.btnTargetsList.BackgroundImage = CType(resources.GetObject("btnTargetsList.BackgroundImage"),System.Drawing.Image)
Me.btnTargetsList.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
Me.btnTargetsList.BorderColor = System.Drawing.Color.Empty
Me.btnTargetsList.ButtonType = eZee.Common.eZeeLightButton.enButtonType.CheckButton
Me.btnTargetsList.FlatAppearance.BorderSize = 0
Me.btnTargetsList.FlatStyle = System.Windows.Forms.FlatStyle.Flat
Me.btnTargetsList.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
Me.btnTargetsList.ForeColor = System.Drawing.Color.Black
Me.btnTargetsList.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
Me.btnTargetsList.GradientForeColor = System.Drawing.Color.Black
Me.btnTargetsList.HoverGradientBackColor = System.Drawing.Color.Transparent
Me.btnTargetsList.HoverGradientForeColor = System.Drawing.Color.Black
Me.btnTargetsList.Image = Global.Aruti.Main.My.Resources.Resources.Objective
Me.btnTargetsList.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
Me.btnTargetsList.Location = New System.Drawing.Point(3, 126)
Me.btnTargetsList.MinimumSize = New System.Drawing.Size(150, 35)
Me.btnTargetsList.Name = "btnTargetsList"
Me.btnTargetsList.PressedGradientBackColor = System.Drawing.Color.Transparent
Me.btnTargetsList.PressedGradientForeColor = System.Drawing.Color.Black
Me.btnTargetsList.Size = New System.Drawing.Size(166, 35)
Me.btnTargetsList.TabIndex = 15
Me.btnTargetsList.Text = "&Targets"
Me.btnTargetsList.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
Me.btnTargetsList.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText
Me.btnTargetsList.UseVisualStyleBackColor = false
'
'btnInitiativeList
'
Me.btnInitiativeList.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left)  _
            Or System.Windows.Forms.AnchorStyles.Right),System.Windows.Forms.AnchorStyles)
Me.btnInitiativeList.BackColor = System.Drawing.Color.Transparent
Me.btnInitiativeList.BackgroundImage = CType(resources.GetObject("btnInitiativeList.BackgroundImage"),System.Drawing.Image)
Me.btnInitiativeList.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
Me.btnInitiativeList.BorderColor = System.Drawing.Color.Empty
Me.btnInitiativeList.ButtonType = eZee.Common.eZeeLightButton.enButtonType.CheckButton
Me.btnInitiativeList.FlatAppearance.BorderSize = 0
Me.btnInitiativeList.FlatStyle = System.Windows.Forms.FlatStyle.Flat
Me.btnInitiativeList.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
Me.btnInitiativeList.ForeColor = System.Drawing.Color.Black
Me.btnInitiativeList.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
Me.btnInitiativeList.GradientForeColor = System.Drawing.Color.Black
Me.btnInitiativeList.HoverGradientBackColor = System.Drawing.Color.Transparent
Me.btnInitiativeList.HoverGradientForeColor = System.Drawing.Color.Black
Me.btnInitiativeList.Image = Global.Aruti.Main.My.Resources.Resources.action
Me.btnInitiativeList.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
Me.btnInitiativeList.Location = New System.Drawing.Point(3, 167)
Me.btnInitiativeList.MinimumSize = New System.Drawing.Size(150, 35)
Me.btnInitiativeList.Name = "btnInitiativeList"
Me.btnInitiativeList.PressedGradientBackColor = System.Drawing.Color.Transparent
Me.btnInitiativeList.PressedGradientForeColor = System.Drawing.Color.Black
Me.btnInitiativeList.Size = New System.Drawing.Size(166, 35)
Me.btnInitiativeList.TabIndex = 16
Me.btnInitiativeList.Text = "&Initiative/Actions"
Me.btnInitiativeList.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
Me.btnInitiativeList.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText
Me.btnInitiativeList.UseVisualStyleBackColor = false
'
'btnSelfAssessedBSC
'
Me.btnSelfAssessedBSC.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left)  _
            Or System.Windows.Forms.AnchorStyles.Right),System.Windows.Forms.AnchorStyles)
Me.btnSelfAssessedBSC.BackColor = System.Drawing.Color.Transparent
Me.btnSelfAssessedBSC.BackgroundImage = CType(resources.GetObject("btnSelfAssessedBSC.BackgroundImage"),System.Drawing.Image)
Me.btnSelfAssessedBSC.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
Me.btnSelfAssessedBSC.BorderColor = System.Drawing.Color.Empty
Me.btnSelfAssessedBSC.ButtonType = eZee.Common.eZeeLightButton.enButtonType.CheckButton
Me.btnSelfAssessedBSC.FlatAppearance.BorderSize = 0
Me.btnSelfAssessedBSC.FlatStyle = System.Windows.Forms.FlatStyle.Flat
Me.btnSelfAssessedBSC.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
Me.btnSelfAssessedBSC.ForeColor = System.Drawing.Color.Black
Me.btnSelfAssessedBSC.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
Me.btnSelfAssessedBSC.GradientForeColor = System.Drawing.Color.Black
Me.btnSelfAssessedBSC.HoverGradientBackColor = System.Drawing.Color.Transparent
Me.btnSelfAssessedBSC.HoverGradientForeColor = System.Drawing.Color.Black
Me.btnSelfAssessedBSC.Image = Global.Aruti.Main.My.Resources.Resources.Self_assessed_bsc
Me.btnSelfAssessedBSC.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
Me.btnSelfAssessedBSC.Location = New System.Drawing.Point(3, 208)
Me.btnSelfAssessedBSC.MinimumSize = New System.Drawing.Size(150, 35)
Me.btnSelfAssessedBSC.Name = "btnSelfAssessedBSC"
Me.btnSelfAssessedBSC.PressedGradientBackColor = System.Drawing.Color.Transparent
Me.btnSelfAssessedBSC.PressedGradientForeColor = System.Drawing.Color.Black
Me.btnSelfAssessedBSC.Size = New System.Drawing.Size(166, 35)
Me.btnSelfAssessedBSC.TabIndex = 17
Me.btnSelfAssessedBSC.Text = "&Self Assessed BSC"
Me.btnSelfAssessedBSC.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
Me.btnSelfAssessedBSC.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText
Me.btnSelfAssessedBSC.UseVisualStyleBackColor = false
'
'btnAssessorBSCList
'
Me.btnAssessorBSCList.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left)  _
            Or System.Windows.Forms.AnchorStyles.Right),System.Windows.Forms.AnchorStyles)
Me.btnAssessorBSCList.BackColor = System.Drawing.Color.Transparent
Me.btnAssessorBSCList.BackgroundImage = CType(resources.GetObject("btnAssessorBSCList.BackgroundImage"),System.Drawing.Image)
Me.btnAssessorBSCList.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
Me.btnAssessorBSCList.BorderColor = System.Drawing.Color.Empty
Me.btnAssessorBSCList.ButtonType = eZee.Common.eZeeLightButton.enButtonType.CheckButton
Me.btnAssessorBSCList.FlatAppearance.BorderSize = 0
Me.btnAssessorBSCList.FlatStyle = System.Windows.Forms.FlatStyle.Flat
Me.btnAssessorBSCList.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
Me.btnAssessorBSCList.ForeColor = System.Drawing.Color.Black
Me.btnAssessorBSCList.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
Me.btnAssessorBSCList.GradientForeColor = System.Drawing.Color.Black
Me.btnAssessorBSCList.HoverGradientBackColor = System.Drawing.Color.Transparent
Me.btnAssessorBSCList.HoverGradientForeColor = System.Drawing.Color.Black
Me.btnAssessorBSCList.Image = Global.Aruti.Main.My.Resources.Resources.Appraiser_assess_bsc
Me.btnAssessorBSCList.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
Me.btnAssessorBSCList.Location = New System.Drawing.Point(3, 249)
Me.btnAssessorBSCList.MinimumSize = New System.Drawing.Size(150, 35)
Me.btnAssessorBSCList.Name = "btnAssessorBSCList"
Me.btnAssessorBSCList.PressedGradientBackColor = System.Drawing.Color.Transparent
Me.btnAssessorBSCList.PressedGradientForeColor = System.Drawing.Color.Black
Me.btnAssessorBSCList.Size = New System.Drawing.Size(166, 35)
Me.btnAssessorBSCList.TabIndex = 18
Me.btnAssessorBSCList.Text = "Assessor's BSC List"
Me.btnAssessorBSCList.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
Me.btnAssessorBSCList.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText
Me.btnAssessorBSCList.UseVisualStyleBackColor = false
'
'btnReviewerBSCList
'
Me.btnReviewerBSCList.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left)  _
            Or System.Windows.Forms.AnchorStyles.Right),System.Windows.Forms.AnchorStyles)
Me.btnReviewerBSCList.BackColor = System.Drawing.Color.Transparent
Me.btnReviewerBSCList.BackgroundImage = CType(resources.GetObject("btnReviewerBSCList.BackgroundImage"),System.Drawing.Image)
Me.btnReviewerBSCList.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
Me.btnReviewerBSCList.BorderColor = System.Drawing.Color.Empty
Me.btnReviewerBSCList.ButtonType = eZee.Common.eZeeLightButton.enButtonType.CheckButton
Me.btnReviewerBSCList.FlatAppearance.BorderSize = 0
Me.btnReviewerBSCList.FlatStyle = System.Windows.Forms.FlatStyle.Flat
Me.btnReviewerBSCList.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
Me.btnReviewerBSCList.ForeColor = System.Drawing.Color.Black
Me.btnReviewerBSCList.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
Me.btnReviewerBSCList.GradientForeColor = System.Drawing.Color.Black
Me.btnReviewerBSCList.HoverGradientBackColor = System.Drawing.Color.Transparent
Me.btnReviewerBSCList.HoverGradientForeColor = System.Drawing.Color.Black
Me.btnReviewerBSCList.Image = Global.Aruti.Main.My.Resources.Resources.Reviewer_bsc
Me.btnReviewerBSCList.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
Me.btnReviewerBSCList.Location = New System.Drawing.Point(3, 290)
Me.btnReviewerBSCList.MinimumSize = New System.Drawing.Size(150, 35)
Me.btnReviewerBSCList.Name = "btnReviewerBSCList"
Me.btnReviewerBSCList.PressedGradientBackColor = System.Drawing.Color.Transparent
Me.btnReviewerBSCList.PressedGradientForeColor = System.Drawing.Color.Black
Me.btnReviewerBSCList.Size = New System.Drawing.Size(166, 35)
Me.btnReviewerBSCList.TabIndex = 19
Me.btnReviewerBSCList.Text = "Reviewer's BSC List"
Me.btnReviewerBSCList.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
Me.btnReviewerBSCList.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText
Me.btnReviewerBSCList.UseVisualStyleBackColor = false
'
'fpnlTrainingNeed
'
Me.fpnlTrainingNeed.AutoScroll = true
Me.fpnlTrainingNeed.Controls.Add(Me.btnFinalizingTrainingPriority)
Me.fpnlTrainingNeed.Location = New System.Drawing.Point(990, 3)
Me.fpnlTrainingNeed.Name = "fpnlTrainingNeed"
Me.fpnlTrainingNeed.Size = New System.Drawing.Size(172, 41)
Me.fpnlTrainingNeed.TabIndex = 22
'
'btnFinalizingTrainingPriority
'
Me.btnFinalizingTrainingPriority.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left)  _
            Or System.Windows.Forms.AnchorStyles.Right),System.Windows.Forms.AnchorStyles)
Me.btnFinalizingTrainingPriority.BackColor = System.Drawing.Color.Transparent
Me.btnFinalizingTrainingPriority.BackgroundImage = CType(resources.GetObject("btnFinalizingTrainingPriority.BackgroundImage"),System.Drawing.Image)
Me.btnFinalizingTrainingPriority.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
Me.btnFinalizingTrainingPriority.BorderColor = System.Drawing.Color.Empty
Me.btnFinalizingTrainingPriority.ButtonType = eZee.Common.eZeeLightButton.enButtonType.CheckButton
Me.btnFinalizingTrainingPriority.FlatAppearance.BorderSize = 0
Me.btnFinalizingTrainingPriority.FlatStyle = System.Windows.Forms.FlatStyle.Flat
Me.btnFinalizingTrainingPriority.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
Me.btnFinalizingTrainingPriority.ForeColor = System.Drawing.Color.Black
Me.btnFinalizingTrainingPriority.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
Me.btnFinalizingTrainingPriority.GradientForeColor = System.Drawing.Color.Black
Me.btnFinalizingTrainingPriority.HoverGradientBackColor = System.Drawing.Color.Transparent
Me.btnFinalizingTrainingPriority.HoverGradientForeColor = System.Drawing.Color.Black
Me.btnFinalizingTrainingPriority.Image = Global.Aruti.Main.My.Resources.Resources.Analysis
Me.btnFinalizingTrainingPriority.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
Me.btnFinalizingTrainingPriority.Location = New System.Drawing.Point(3, 3)
Me.btnFinalizingTrainingPriority.MinimumSize = New System.Drawing.Size(150, 35)
Me.btnFinalizingTrainingPriority.Name = "btnFinalizingTrainingPriority"
Me.btnFinalizingTrainingPriority.PressedGradientBackColor = System.Drawing.Color.Transparent
Me.btnFinalizingTrainingPriority.PressedGradientForeColor = System.Drawing.Color.Black
Me.btnFinalizingTrainingPriority.Size = New System.Drawing.Size(166, 35)
Me.btnFinalizingTrainingPriority.TabIndex = 14
Me.btnFinalizingTrainingPriority.Text = "Finalizing Training"
Me.btnFinalizingTrainingPriority.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
Me.btnFinalizingTrainingPriority.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText
Me.btnFinalizingTrainingPriority.UseVisualStyleBackColor = false
Me.btnFinalizingTrainingPriority.Visible = false
'
'fpnlTrainingFeedback
'
Me.fpnlTrainingFeedback.AutoScroll = true
Me.fpnlTrainingFeedback.Controls.Add(Me.btnFeedbackGroup)
Me.fpnlTrainingFeedback.Controls.Add(Me.btnFeedbackItems)
Me.fpnlTrainingFeedback.Controls.Add(Me.btnFeedbackSubItems)
Me.fpnlTrainingFeedback.Controls.Add(Me.btnImpactItems)
Me.fpnlTrainingFeedback.Controls.Add(Me.objelLine10)
Me.fpnlTrainingFeedback.Controls.Add(Me.btnFeedback)
Me.fpnlTrainingFeedback.Controls.Add(Me.btnEvaluationLevel3)
Me.fpnlTrainingFeedback.Location = New System.Drawing.Point(990, 50)
Me.fpnlTrainingFeedback.Name = "fpnlTrainingFeedback"
Me.fpnlTrainingFeedback.Size = New System.Drawing.Size(191, 41)
Me.fpnlTrainingFeedback.TabIndex = 23
'
'btnFeedbackGroup
'
Me.btnFeedbackGroup.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left)  _
            Or System.Windows.Forms.AnchorStyles.Right),System.Windows.Forms.AnchorStyles)
Me.btnFeedbackGroup.BackColor = System.Drawing.Color.Transparent
Me.btnFeedbackGroup.BackgroundImage = CType(resources.GetObject("btnFeedbackGroup.BackgroundImage"),System.Drawing.Image)
Me.btnFeedbackGroup.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
Me.btnFeedbackGroup.BorderColor = System.Drawing.Color.Empty
Me.btnFeedbackGroup.ButtonType = eZee.Common.eZeeLightButton.enButtonType.CheckButton
Me.btnFeedbackGroup.FlatAppearance.BorderSize = 0
Me.btnFeedbackGroup.FlatStyle = System.Windows.Forms.FlatStyle.Flat
Me.btnFeedbackGroup.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
Me.btnFeedbackGroup.ForeColor = System.Drawing.Color.Black
Me.btnFeedbackGroup.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
Me.btnFeedbackGroup.GradientForeColor = System.Drawing.Color.Black
Me.btnFeedbackGroup.HoverGradientBackColor = System.Drawing.Color.Transparent
Me.btnFeedbackGroup.HoverGradientForeColor = System.Drawing.Color.Black
Me.btnFeedbackGroup.Image = Global.Aruti.Main.My.Resources.Resources.feedback_group
Me.btnFeedbackGroup.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
Me.btnFeedbackGroup.Location = New System.Drawing.Point(3, 3)
Me.btnFeedbackGroup.MinimumSize = New System.Drawing.Size(150, 35)
Me.btnFeedbackGroup.Name = "btnFeedbackGroup"
Me.btnFeedbackGroup.PressedGradientBackColor = System.Drawing.Color.Transparent
Me.btnFeedbackGroup.PressedGradientForeColor = System.Drawing.Color.Black
Me.btnFeedbackGroup.Size = New System.Drawing.Size(166, 35)
Me.btnFeedbackGroup.TabIndex = 7
Me.btnFeedbackGroup.Text = "Evaluation &Group"
Me.btnFeedbackGroup.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
Me.btnFeedbackGroup.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText
Me.btnFeedbackGroup.UseVisualStyleBackColor = false
'
'btnFeedbackItems
'
Me.btnFeedbackItems.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left)  _
            Or System.Windows.Forms.AnchorStyles.Right),System.Windows.Forms.AnchorStyles)
Me.btnFeedbackItems.BackColor = System.Drawing.Color.Transparent
Me.btnFeedbackItems.BackgroundImage = CType(resources.GetObject("btnFeedbackItems.BackgroundImage"),System.Drawing.Image)
Me.btnFeedbackItems.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
Me.btnFeedbackItems.BorderColor = System.Drawing.Color.Empty
Me.btnFeedbackItems.ButtonType = eZee.Common.eZeeLightButton.enButtonType.CheckButton
Me.btnFeedbackItems.FlatAppearance.BorderSize = 0
Me.btnFeedbackItems.FlatStyle = System.Windows.Forms.FlatStyle.Flat
Me.btnFeedbackItems.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
Me.btnFeedbackItems.ForeColor = System.Drawing.Color.Black
Me.btnFeedbackItems.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
Me.btnFeedbackItems.GradientForeColor = System.Drawing.Color.Black
Me.btnFeedbackItems.HoverGradientBackColor = System.Drawing.Color.Transparent
Me.btnFeedbackItems.HoverGradientForeColor = System.Drawing.Color.Black
Me.btnFeedbackItems.Image = Global.Aruti.Main.My.Resources.Resources.feedback_items
Me.btnFeedbackItems.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
Me.btnFeedbackItems.Location = New System.Drawing.Point(3, 44)
Me.btnFeedbackItems.MinimumSize = New System.Drawing.Size(150, 35)
Me.btnFeedbackItems.Name = "btnFeedbackItems"
Me.btnFeedbackItems.PressedGradientBackColor = System.Drawing.Color.Transparent
Me.btnFeedbackItems.PressedGradientForeColor = System.Drawing.Color.Black
Me.btnFeedbackItems.Size = New System.Drawing.Size(166, 35)
Me.btnFeedbackItems.TabIndex = 9
Me.btnFeedbackItems.Text = "Evaluation &Items"
Me.btnFeedbackItems.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
Me.btnFeedbackItems.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText
Me.btnFeedbackItems.UseVisualStyleBackColor = false
'
'btnFeedbackSubItems
'
Me.btnFeedbackSubItems.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left)  _
            Or System.Windows.Forms.AnchorStyles.Right),System.Windows.Forms.AnchorStyles)
Me.btnFeedbackSubItems.BackColor = System.Drawing.Color.Transparent
Me.btnFeedbackSubItems.BackgroundImage = CType(resources.GetObject("btnFeedbackSubItems.BackgroundImage"),System.Drawing.Image)
Me.btnFeedbackSubItems.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
Me.btnFeedbackSubItems.BorderColor = System.Drawing.Color.Empty
Me.btnFeedbackSubItems.ButtonType = eZee.Common.eZeeLightButton.enButtonType.CheckButton
Me.btnFeedbackSubItems.FlatAppearance.BorderSize = 0
Me.btnFeedbackSubItems.FlatStyle = System.Windows.Forms.FlatStyle.Flat
Me.btnFeedbackSubItems.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
Me.btnFeedbackSubItems.ForeColor = System.Drawing.Color.Black
Me.btnFeedbackSubItems.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
Me.btnFeedbackSubItems.GradientForeColor = System.Drawing.Color.Black
Me.btnFeedbackSubItems.HoverGradientBackColor = System.Drawing.Color.Transparent
Me.btnFeedbackSubItems.HoverGradientForeColor = System.Drawing.Color.Black
Me.btnFeedbackSubItems.Image = Global.Aruti.Main.My.Resources.Resources.feedback_subitems
Me.btnFeedbackSubItems.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
Me.btnFeedbackSubItems.Location = New System.Drawing.Point(3, 85)
Me.btnFeedbackSubItems.MinimumSize = New System.Drawing.Size(150, 35)
Me.btnFeedbackSubItems.Name = "btnFeedbackSubItems"
Me.btnFeedbackSubItems.PressedGradientBackColor = System.Drawing.Color.Transparent
Me.btnFeedbackSubItems.PressedGradientForeColor = System.Drawing.Color.Black
Me.btnFeedbackSubItems.Size = New System.Drawing.Size(166, 35)
Me.btnFeedbackSubItems.TabIndex = 10
Me.btnFeedbackSubItems.Text = "Evaluation &SubItems"
Me.btnFeedbackSubItems.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
Me.btnFeedbackSubItems.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText
Me.btnFeedbackSubItems.UseVisualStyleBackColor = false
'
'btnImpactItems
'
Me.btnImpactItems.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left)  _
            Or System.Windows.Forms.AnchorStyles.Right),System.Windows.Forms.AnchorStyles)
Me.btnImpactItems.BackColor = System.Drawing.Color.Transparent
Me.btnImpactItems.BackgroundImage = CType(resources.GetObject("btnImpactItems.BackgroundImage"),System.Drawing.Image)
Me.btnImpactItems.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
Me.btnImpactItems.BorderColor = System.Drawing.Color.Empty
Me.btnImpactItems.ButtonType = eZee.Common.eZeeLightButton.enButtonType.CheckButton
Me.btnImpactItems.FlatAppearance.BorderSize = 0
Me.btnImpactItems.FlatStyle = System.Windows.Forms.FlatStyle.Flat
Me.btnImpactItems.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
Me.btnImpactItems.ForeColor = System.Drawing.Color.Black
Me.btnImpactItems.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
Me.btnImpactItems.GradientForeColor = System.Drawing.Color.Black
Me.btnImpactItems.HoverGradientBackColor = System.Drawing.Color.Transparent
Me.btnImpactItems.HoverGradientForeColor = System.Drawing.Color.Black
Me.btnImpactItems.Image = Global.Aruti.Main.My.Resources.Resources.feedback_subitems
Me.btnImpactItems.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
Me.btnImpactItems.Location = New System.Drawing.Point(3, 126)
Me.btnImpactItems.MinimumSize = New System.Drawing.Size(150, 35)
Me.btnImpactItems.Name = "btnImpactItems"
Me.btnImpactItems.PressedGradientBackColor = System.Drawing.Color.Transparent
Me.btnImpactItems.PressedGradientForeColor = System.Drawing.Color.Black
Me.btnImpactItems.Size = New System.Drawing.Size(166, 35)
Me.btnImpactItems.TabIndex = 14
Me.btnImpactItems.Text = "Evaluation III &Items"
Me.btnImpactItems.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
Me.btnImpactItems.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText
Me.btnImpactItems.UseVisualStyleBackColor = false
'
'objelLine10
'
Me.objelLine10.LineOrientation = eZee.Common.eZeeLine.Orientation.Horizontal
Me.objelLine10.Location = New System.Drawing.Point(3, 164)
Me.objelLine10.Name = "objelLine10"
Me.objelLine10.Size = New System.Drawing.Size(166, 17)
Me.objelLine10.TabIndex = 12
Me.objelLine10.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
'
'btnFeedback
'
Me.btnFeedback.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left)  _
            Or System.Windows.Forms.AnchorStyles.Right),System.Windows.Forms.AnchorStyles)
Me.btnFeedback.BackColor = System.Drawing.Color.Transparent
Me.btnFeedback.BackgroundImage = CType(resources.GetObject("btnFeedback.BackgroundImage"),System.Drawing.Image)
Me.btnFeedback.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
Me.btnFeedback.BorderColor = System.Drawing.Color.Empty
Me.btnFeedback.ButtonType = eZee.Common.eZeeLightButton.enButtonType.CheckButton
Me.btnFeedback.FlatAppearance.BorderSize = 0
Me.btnFeedback.FlatStyle = System.Windows.Forms.FlatStyle.Flat
Me.btnFeedback.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
Me.btnFeedback.ForeColor = System.Drawing.Color.Black
Me.btnFeedback.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
Me.btnFeedback.GradientForeColor = System.Drawing.Color.Black
Me.btnFeedback.HoverGradientBackColor = System.Drawing.Color.Transparent
Me.btnFeedback.HoverGradientForeColor = System.Drawing.Color.Black
Me.btnFeedback.Image = Global.Aruti.Main.My.Resources.Resources.employee_feedback
Me.btnFeedback.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
Me.btnFeedback.Location = New System.Drawing.Point(3, 184)
Me.btnFeedback.MinimumSize = New System.Drawing.Size(150, 35)
Me.btnFeedback.Name = "btnFeedback"
Me.btnFeedback.PressedGradientBackColor = System.Drawing.Color.Transparent
Me.btnFeedback.PressedGradientForeColor = System.Drawing.Color.Black
Me.btnFeedback.Size = New System.Drawing.Size(166, 35)
Me.btnFeedback.TabIndex = 11
Me.btnFeedback.Text = "Level I Evaluation"
Me.btnFeedback.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
Me.btnFeedback.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText
Me.btnFeedback.UseVisualStyleBackColor = false
'
'btnEvaluationLevel3
'
Me.btnEvaluationLevel3.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left)  _
            Or System.Windows.Forms.AnchorStyles.Right),System.Windows.Forms.AnchorStyles)
Me.btnEvaluationLevel3.BackColor = System.Drawing.Color.Transparent
Me.btnEvaluationLevel3.BackgroundImage = CType(resources.GetObject("btnEvaluationLevel3.BackgroundImage"),System.Drawing.Image)
Me.btnEvaluationLevel3.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
Me.btnEvaluationLevel3.BorderColor = System.Drawing.Color.Empty
Me.btnEvaluationLevel3.ButtonType = eZee.Common.eZeeLightButton.enButtonType.CheckButton
Me.btnEvaluationLevel3.FlatAppearance.BorderSize = 0
Me.btnEvaluationLevel3.FlatStyle = System.Windows.Forms.FlatStyle.Flat
Me.btnEvaluationLevel3.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
Me.btnEvaluationLevel3.ForeColor = System.Drawing.Color.Black
Me.btnEvaluationLevel3.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
Me.btnEvaluationLevel3.GradientForeColor = System.Drawing.Color.Black
Me.btnEvaluationLevel3.HoverGradientBackColor = System.Drawing.Color.Transparent
Me.btnEvaluationLevel3.HoverGradientForeColor = System.Drawing.Color.Black
Me.btnEvaluationLevel3.Image = Global.Aruti.Main.My.Resources.Resources.employee_feedback
Me.btnEvaluationLevel3.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
Me.btnEvaluationLevel3.Location = New System.Drawing.Point(3, 225)
Me.btnEvaluationLevel3.MinimumSize = New System.Drawing.Size(150, 35)
Me.btnEvaluationLevel3.Name = "btnEvaluationLevel3"
Me.btnEvaluationLevel3.PressedGradientBackColor = System.Drawing.Color.Transparent
Me.btnEvaluationLevel3.PressedGradientForeColor = System.Drawing.Color.Black
Me.btnEvaluationLevel3.Size = New System.Drawing.Size(166, 35)
Me.btnEvaluationLevel3.TabIndex = 13
Me.btnEvaluationLevel3.Text = "Level III Evaluation"
Me.btnEvaluationLevel3.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
Me.btnEvaluationLevel3.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText
Me.btnEvaluationLevel3.UseVisualStyleBackColor = false
'
'fpnlAuditTrails
'
Me.fpnlAuditTrails.AutoScroll = true
Me.fpnlAuditTrails.Controls.Add(Me.btnApplicationEventLog)
Me.fpnlAuditTrails.Controls.Add(Me.btnUserAuthenticationLog)
Me.fpnlAuditTrails.Controls.Add(Me.btnUserAttempts)
Me.fpnlAuditTrails.Controls.Add(Me.btnLogView)
Me.fpnlAuditTrails.Controls.Add(Me.btnEmailLogView)
Me.fpnlAuditTrails.Controls.Add(Me.btnRemovePPALogs)
Me.fpnlAuditTrails.Controls.Add(Me.btnSystemErrorLog)
Me.fpnlAuditTrails.Location = New System.Drawing.Point(990, 97)
Me.fpnlAuditTrails.Name = "fpnlAuditTrails"
Me.fpnlAuditTrails.Size = New System.Drawing.Size(191, 41)
Me.fpnlAuditTrails.TabIndex = 24
Me.fpnlAuditTrails.Visible = false
'
'btnApplicationEventLog
'
Me.btnApplicationEventLog.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left)  _
            Or System.Windows.Forms.AnchorStyles.Right),System.Windows.Forms.AnchorStyles)
Me.btnApplicationEventLog.BackColor = System.Drawing.Color.Transparent
Me.btnApplicationEventLog.BackgroundImage = CType(resources.GetObject("btnApplicationEventLog.BackgroundImage"),System.Drawing.Image)
Me.btnApplicationEventLog.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
Me.btnApplicationEventLog.BorderColor = System.Drawing.Color.Empty
Me.btnApplicationEventLog.ButtonType = eZee.Common.eZeeLightButton.enButtonType.CheckButton
Me.btnApplicationEventLog.FlatAppearance.BorderSize = 0
Me.btnApplicationEventLog.FlatStyle = System.Windows.Forms.FlatStyle.Flat
Me.btnApplicationEventLog.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
Me.btnApplicationEventLog.ForeColor = System.Drawing.Color.Black
Me.btnApplicationEventLog.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
Me.btnApplicationEventLog.GradientForeColor = System.Drawing.Color.Black
Me.btnApplicationEventLog.HoverGradientBackColor = System.Drawing.Color.Transparent
Me.btnApplicationEventLog.HoverGradientForeColor = System.Drawing.Color.Black
Me.btnApplicationEventLog.Image = Global.Aruti.Main.My.Resources.Resources.Application_Event_Log
Me.btnApplicationEventLog.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
Me.btnApplicationEventLog.Location = New System.Drawing.Point(3, 3)
Me.btnApplicationEventLog.MinimumSize = New System.Drawing.Size(150, 35)
Me.btnApplicationEventLog.Name = "btnApplicationEventLog"
Me.btnApplicationEventLog.PressedGradientBackColor = System.Drawing.Color.Transparent
Me.btnApplicationEventLog.PressedGradientForeColor = System.Drawing.Color.Black
Me.btnApplicationEventLog.Size = New System.Drawing.Size(166, 35)
Me.btnApplicationEventLog.TabIndex = 29
Me.btnApplicationEventLog.Text = "Application Event Log"
Me.btnApplicationEventLog.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
Me.btnApplicationEventLog.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText
Me.btnApplicationEventLog.UseVisualStyleBackColor = false
'
'btnUserAuthenticationLog
'
Me.btnUserAuthenticationLog.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left)  _
            Or System.Windows.Forms.AnchorStyles.Right),System.Windows.Forms.AnchorStyles)
Me.btnUserAuthenticationLog.BackColor = System.Drawing.Color.Transparent
Me.btnUserAuthenticationLog.BackgroundImage = CType(resources.GetObject("btnUserAuthenticationLog.BackgroundImage"),System.Drawing.Image)
Me.btnUserAuthenticationLog.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
Me.btnUserAuthenticationLog.BorderColor = System.Drawing.Color.Empty
Me.btnUserAuthenticationLog.ButtonType = eZee.Common.eZeeLightButton.enButtonType.CheckButton
Me.btnUserAuthenticationLog.FlatAppearance.BorderSize = 0
Me.btnUserAuthenticationLog.FlatStyle = System.Windows.Forms.FlatStyle.Flat
Me.btnUserAuthenticationLog.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
Me.btnUserAuthenticationLog.ForeColor = System.Drawing.Color.Black
Me.btnUserAuthenticationLog.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
Me.btnUserAuthenticationLog.GradientForeColor = System.Drawing.Color.Black
Me.btnUserAuthenticationLog.HoverGradientBackColor = System.Drawing.Color.Transparent
Me.btnUserAuthenticationLog.HoverGradientForeColor = System.Drawing.Color.Black
Me.btnUserAuthenticationLog.Image = Global.Aruti.Main.My.Resources.Resources.User_Authentication_Log
Me.btnUserAuthenticationLog.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
Me.btnUserAuthenticationLog.Location = New System.Drawing.Point(3, 44)
Me.btnUserAuthenticationLog.MinimumSize = New System.Drawing.Size(150, 35)
Me.btnUserAuthenticationLog.Name = "btnUserAuthenticationLog"
Me.btnUserAuthenticationLog.PressedGradientBackColor = System.Drawing.Color.Transparent
Me.btnUserAuthenticationLog.PressedGradientForeColor = System.Drawing.Color.Black
Me.btnUserAuthenticationLog.Size = New System.Drawing.Size(166, 35)
Me.btnUserAuthenticationLog.TabIndex = 31
Me.btnUserAuthenticationLog.Text = "User Authentication Log"
Me.btnUserAuthenticationLog.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
Me.btnUserAuthenticationLog.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText
Me.btnUserAuthenticationLog.UseVisualStyleBackColor = false
'
'btnUserAttempts
'
Me.btnUserAttempts.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left)  _
            Or System.Windows.Forms.AnchorStyles.Right),System.Windows.Forms.AnchorStyles)
Me.btnUserAttempts.BackColor = System.Drawing.Color.Transparent
Me.btnUserAttempts.BackgroundImage = CType(resources.GetObject("btnUserAttempts.BackgroundImage"),System.Drawing.Image)
Me.btnUserAttempts.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
Me.btnUserAttempts.BorderColor = System.Drawing.Color.Empty
Me.btnUserAttempts.ButtonType = eZee.Common.eZeeLightButton.enButtonType.CheckButton
Me.btnUserAttempts.FlatAppearance.BorderSize = 0
Me.btnUserAttempts.FlatStyle = System.Windows.Forms.FlatStyle.Flat
Me.btnUserAttempts.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
Me.btnUserAttempts.ForeColor = System.Drawing.Color.Black
Me.btnUserAttempts.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
Me.btnUserAttempts.GradientForeColor = System.Drawing.Color.Black
Me.btnUserAttempts.HoverGradientBackColor = System.Drawing.Color.Transparent
Me.btnUserAttempts.HoverGradientForeColor = System.Drawing.Color.Black
Me.btnUserAttempts.Image = Global.Aruti.Main.My.Resources.Resources.User_Attempts_Log
Me.btnUserAttempts.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
Me.btnUserAttempts.Location = New System.Drawing.Point(3, 85)
Me.btnUserAttempts.MinimumSize = New System.Drawing.Size(150, 35)
Me.btnUserAttempts.Name = "btnUserAttempts"
Me.btnUserAttempts.PressedGradientBackColor = System.Drawing.Color.Transparent
Me.btnUserAttempts.PressedGradientForeColor = System.Drawing.Color.Black
Me.btnUserAttempts.Size = New System.Drawing.Size(166, 35)
Me.btnUserAttempts.TabIndex = 30
Me.btnUserAttempts.Text = "User Attempts Log"
Me.btnUserAttempts.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
Me.btnUserAttempts.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText
Me.btnUserAttempts.UseVisualStyleBackColor = false
'
'btnLogView
'
Me.btnLogView.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left)  _
            Or System.Windows.Forms.AnchorStyles.Right),System.Windows.Forms.AnchorStyles)
Me.btnLogView.BackColor = System.Drawing.Color.Transparent
Me.btnLogView.BackgroundImage = CType(resources.GetObject("btnLogView.BackgroundImage"),System.Drawing.Image)
Me.btnLogView.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
Me.btnLogView.BorderColor = System.Drawing.Color.Empty
Me.btnLogView.ButtonType = eZee.Common.eZeeLightButton.enButtonType.CheckButton
Me.btnLogView.FlatAppearance.BorderSize = 0
Me.btnLogView.FlatStyle = System.Windows.Forms.FlatStyle.Flat
Me.btnLogView.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
Me.btnLogView.ForeColor = System.Drawing.Color.Black
Me.btnLogView.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
Me.btnLogView.GradientForeColor = System.Drawing.Color.Black
Me.btnLogView.HoverGradientBackColor = System.Drawing.Color.Transparent
Me.btnLogView.HoverGradientForeColor = System.Drawing.Color.Black
Me.btnLogView.Image = Global.Aruti.Main.My.Resources.Resources.Log_View
Me.btnLogView.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
Me.btnLogView.Location = New System.Drawing.Point(3, 126)
Me.btnLogView.MinimumSize = New System.Drawing.Size(150, 35)
Me.btnLogView.Name = "btnLogView"
Me.btnLogView.PressedGradientBackColor = System.Drawing.Color.Transparent
Me.btnLogView.PressedGradientForeColor = System.Drawing.Color.Black
Me.btnLogView.Size = New System.Drawing.Size(166, 35)
Me.btnLogView.TabIndex = 32
Me.btnLogView.Text = "Log View"
Me.btnLogView.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
Me.btnLogView.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText
Me.btnLogView.UseVisualStyleBackColor = false
'
'btnEmailLogView
'
Me.btnEmailLogView.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left)  _
            Or System.Windows.Forms.AnchorStyles.Right),System.Windows.Forms.AnchorStyles)
Me.btnEmailLogView.BackColor = System.Drawing.Color.Transparent
Me.btnEmailLogView.BackgroundImage = CType(resources.GetObject("btnEmailLogView.BackgroundImage"),System.Drawing.Image)
Me.btnEmailLogView.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
Me.btnEmailLogView.BorderColor = System.Drawing.Color.Empty
Me.btnEmailLogView.ButtonType = eZee.Common.eZeeLightButton.enButtonType.CheckButton
Me.btnEmailLogView.FlatAppearance.BorderSize = 0
Me.btnEmailLogView.FlatStyle = System.Windows.Forms.FlatStyle.Flat
Me.btnEmailLogView.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
Me.btnEmailLogView.ForeColor = System.Drawing.Color.Black
Me.btnEmailLogView.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
Me.btnEmailLogView.GradientForeColor = System.Drawing.Color.Black
Me.btnEmailLogView.HoverGradientBackColor = System.Drawing.Color.Transparent
Me.btnEmailLogView.HoverGradientForeColor = System.Drawing.Color.Black
Me.btnEmailLogView.Image = Global.Aruti.Main.My.Resources.Resources.Application_Event_Log
Me.btnEmailLogView.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
Me.btnEmailLogView.Location = New System.Drawing.Point(3, 167)
Me.btnEmailLogView.MinimumSize = New System.Drawing.Size(150, 35)
Me.btnEmailLogView.Name = "btnEmailLogView"
Me.btnEmailLogView.PressedGradientBackColor = System.Drawing.Color.Transparent
Me.btnEmailLogView.PressedGradientForeColor = System.Drawing.Color.Black
Me.btnEmailLogView.Size = New System.Drawing.Size(166, 35)
Me.btnEmailLogView.TabIndex = 33
Me.btnEmailLogView.Text = "Email Sent Log View"
Me.btnEmailLogView.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
Me.btnEmailLogView.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText
Me.btnEmailLogView.UseVisualStyleBackColor = false
'
'btnRemovePPALogs
'
Me.btnRemovePPALogs.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left)  _
            Or System.Windows.Forms.AnchorStyles.Right),System.Windows.Forms.AnchorStyles)
Me.btnRemovePPALogs.BackColor = System.Drawing.Color.Transparent
Me.btnRemovePPALogs.BackgroundImage = CType(resources.GetObject("btnRemovePPALogs.BackgroundImage"),System.Drawing.Image)
Me.btnRemovePPALogs.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
Me.btnRemovePPALogs.BorderColor = System.Drawing.Color.Empty
Me.btnRemovePPALogs.ButtonType = eZee.Common.eZeeLightButton.enButtonType.CheckButton
Me.btnRemovePPALogs.FlatAppearance.BorderSize = 0
Me.btnRemovePPALogs.FlatStyle = System.Windows.Forms.FlatStyle.Flat
Me.btnRemovePPALogs.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
Me.btnRemovePPALogs.ForeColor = System.Drawing.Color.Black
Me.btnRemovePPALogs.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
Me.btnRemovePPALogs.GradientForeColor = System.Drawing.Color.Black
Me.btnRemovePPALogs.HoverGradientBackColor = System.Drawing.Color.Transparent
Me.btnRemovePPALogs.HoverGradientForeColor = System.Drawing.Color.Black
Me.btnRemovePPALogs.Image = Global.Aruti.Main.My.Resources.Resources._4_Pay_Per_Activity
Me.btnRemovePPALogs.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
Me.btnRemovePPALogs.Location = New System.Drawing.Point(3, 208)
Me.btnRemovePPALogs.MinimumSize = New System.Drawing.Size(150, 35)
Me.btnRemovePPALogs.Name = "btnRemovePPALogs"
Me.btnRemovePPALogs.PressedGradientBackColor = System.Drawing.Color.Transparent
Me.btnRemovePPALogs.PressedGradientForeColor = System.Drawing.Color.Black
Me.btnRemovePPALogs.Size = New System.Drawing.Size(166, 35)
Me.btnRemovePPALogs.TabIndex = 34
Me.btnRemovePPALogs.Text = "Remove Pay Per Activty Audit Logs"
Me.btnRemovePPALogs.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
Me.btnRemovePPALogs.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText
Me.btnRemovePPALogs.UseVisualStyleBackColor = false
'
'btnSystemErrorLog
'
Me.btnSystemErrorLog.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left)  _
            Or System.Windows.Forms.AnchorStyles.Right),System.Windows.Forms.AnchorStyles)
Me.btnSystemErrorLog.BackColor = System.Drawing.Color.Transparent
Me.btnSystemErrorLog.BackgroundImage = CType(resources.GetObject("btnSystemErrorLog.BackgroundImage"),System.Drawing.Image)
Me.btnSystemErrorLog.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
Me.btnSystemErrorLog.BorderColor = System.Drawing.Color.Empty
Me.btnSystemErrorLog.ButtonType = eZee.Common.eZeeLightButton.enButtonType.CheckButton
Me.btnSystemErrorLog.FlatAppearance.BorderSize = 0
Me.btnSystemErrorLog.FlatStyle = System.Windows.Forms.FlatStyle.Flat
Me.btnSystemErrorLog.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
Me.btnSystemErrorLog.ForeColor = System.Drawing.Color.Black
Me.btnSystemErrorLog.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
Me.btnSystemErrorLog.GradientForeColor = System.Drawing.Color.Black
Me.btnSystemErrorLog.HoverGradientBackColor = System.Drawing.Color.Transparent
Me.btnSystemErrorLog.HoverGradientForeColor = System.Drawing.Color.Black
Me.btnSystemErrorLog.Image = Global.Aruti.Main.My.Resources.Resources.Application_Event_Log
Me.btnSystemErrorLog.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
Me.btnSystemErrorLog.Location = New System.Drawing.Point(3, 249)
Me.btnSystemErrorLog.MinimumSize = New System.Drawing.Size(150, 35)
Me.btnSystemErrorLog.Name = "btnSystemErrorLog"
Me.btnSystemErrorLog.PressedGradientBackColor = System.Drawing.Color.Transparent
Me.btnSystemErrorLog.PressedGradientForeColor = System.Drawing.Color.Black
Me.btnSystemErrorLog.Size = New System.Drawing.Size(166, 35)
Me.btnSystemErrorLog.TabIndex = 35
Me.btnSystemErrorLog.Text = "&System Error Log"
Me.btnSystemErrorLog.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
Me.btnSystemErrorLog.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText
Me.btnSystemErrorLog.UseVisualStyleBackColor = false
'
'fnlWagesSetups
'
Me.fnlWagesSetups.AutoScroll = true
Me.fnlWagesSetups.Controls.Add(Me.btnGradeGroup)
Me.fnlWagesSetups.Controls.Add(Me.btnGrade)
Me.fnlWagesSetups.Controls.Add(Me.btnGradeLevel)
Me.fnlWagesSetups.Controls.Add(Me.btnCostcenter)
Me.fnlWagesSetups.Controls.Add(Me.btnWagesTable)
Me.fnlWagesSetups.Controls.Add(Me.btnPayrollPeriods)
Me.fnlWagesSetups.Controls.Add(Me.btnSalaryIncrement)
Me.fnlWagesSetups.Controls.Add(Me.btnClosePayroll)
Me.fnlWagesSetups.Location = New System.Drawing.Point(990, 144)
Me.fnlWagesSetups.Name = "fnlWagesSetups"
Me.fnlWagesSetups.Size = New System.Drawing.Size(191, 40)
Me.fnlWagesSetups.TabIndex = 19
'
'btnGradeGroup
'
Me.btnGradeGroup.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left)  _
            Or System.Windows.Forms.AnchorStyles.Right),System.Windows.Forms.AnchorStyles)
Me.btnGradeGroup.BackColor = System.Drawing.Color.Transparent
Me.btnGradeGroup.BackgroundImage = CType(resources.GetObject("btnGradeGroup.BackgroundImage"),System.Drawing.Image)
Me.btnGradeGroup.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
Me.btnGradeGroup.BorderColor = System.Drawing.Color.Empty
Me.btnGradeGroup.ButtonType = eZee.Common.eZeeLightButton.enButtonType.CheckButton
Me.btnGradeGroup.FlatAppearance.BorderSize = 0
Me.btnGradeGroup.FlatStyle = System.Windows.Forms.FlatStyle.Flat
Me.btnGradeGroup.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
Me.btnGradeGroup.ForeColor = System.Drawing.Color.Black
Me.btnGradeGroup.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
Me.btnGradeGroup.GradientForeColor = System.Drawing.Color.Black
Me.btnGradeGroup.HoverGradientBackColor = System.Drawing.Color.Transparent
Me.btnGradeGroup.HoverGradientForeColor = System.Drawing.Color.Black
Me.btnGradeGroup.Image = Global.Aruti.Main.My.Resources.Resources.GradeGroup
Me.btnGradeGroup.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
Me.btnGradeGroup.Location = New System.Drawing.Point(3, 3)
Me.btnGradeGroup.MinimumSize = New System.Drawing.Size(150, 35)
Me.btnGradeGroup.Name = "btnGradeGroup"
Me.btnGradeGroup.PressedGradientBackColor = System.Drawing.Color.Transparent
Me.btnGradeGroup.PressedGradientForeColor = System.Drawing.Color.Black
Me.btnGradeGroup.Size = New System.Drawing.Size(166, 35)
Me.btnGradeGroup.TabIndex = 13
Me.btnGradeGroup.Text = "Grade Group"
Me.btnGradeGroup.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
Me.btnGradeGroup.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText
Me.btnGradeGroup.UseVisualStyleBackColor = false
'
'btnGrade
'
Me.btnGrade.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left)  _
            Or System.Windows.Forms.AnchorStyles.Right),System.Windows.Forms.AnchorStyles)
Me.btnGrade.BackColor = System.Drawing.Color.Transparent
Me.btnGrade.BackgroundImage = CType(resources.GetObject("btnGrade.BackgroundImage"),System.Drawing.Image)
Me.btnGrade.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
Me.btnGrade.BorderColor = System.Drawing.Color.Empty
Me.btnGrade.ButtonType = eZee.Common.eZeeLightButton.enButtonType.CheckButton
Me.btnGrade.FlatAppearance.BorderSize = 0
Me.btnGrade.FlatStyle = System.Windows.Forms.FlatStyle.Flat
Me.btnGrade.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
Me.btnGrade.ForeColor = System.Drawing.Color.Black
Me.btnGrade.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
Me.btnGrade.GradientForeColor = System.Drawing.Color.Black
Me.btnGrade.HoverGradientBackColor = System.Drawing.Color.Transparent
Me.btnGrade.HoverGradientForeColor = System.Drawing.Color.Black
Me.btnGrade.Image = Global.Aruti.Main.My.Resources.Resources.Grade
Me.btnGrade.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
Me.btnGrade.Location = New System.Drawing.Point(3, 44)
Me.btnGrade.MinimumSize = New System.Drawing.Size(150, 35)
Me.btnGrade.Name = "btnGrade"
Me.btnGrade.PressedGradientBackColor = System.Drawing.Color.Transparent
Me.btnGrade.PressedGradientForeColor = System.Drawing.Color.Black
Me.btnGrade.Size = New System.Drawing.Size(166, 35)
Me.btnGrade.TabIndex = 14
Me.btnGrade.Text = "Grade"
Me.btnGrade.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
Me.btnGrade.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText
Me.btnGrade.UseVisualStyleBackColor = false
'
'btnGradeLevel
'
Me.btnGradeLevel.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left)  _
            Or System.Windows.Forms.AnchorStyles.Right),System.Windows.Forms.AnchorStyles)
Me.btnGradeLevel.BackColor = System.Drawing.Color.Transparent
Me.btnGradeLevel.BackgroundImage = CType(resources.GetObject("btnGradeLevel.BackgroundImage"),System.Drawing.Image)
Me.btnGradeLevel.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
Me.btnGradeLevel.BorderColor = System.Drawing.Color.Empty
Me.btnGradeLevel.ButtonType = eZee.Common.eZeeLightButton.enButtonType.CheckButton
Me.btnGradeLevel.FlatAppearance.BorderSize = 0
Me.btnGradeLevel.FlatStyle = System.Windows.Forms.FlatStyle.Flat
Me.btnGradeLevel.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
Me.btnGradeLevel.ForeColor = System.Drawing.Color.Black
Me.btnGradeLevel.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
Me.btnGradeLevel.GradientForeColor = System.Drawing.Color.Black
Me.btnGradeLevel.HoverGradientBackColor = System.Drawing.Color.Transparent
Me.btnGradeLevel.HoverGradientForeColor = System.Drawing.Color.Black
Me.btnGradeLevel.Image = Global.Aruti.Main.My.Resources.Resources.GradeLevel
Me.btnGradeLevel.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
Me.btnGradeLevel.Location = New System.Drawing.Point(3, 85)
Me.btnGradeLevel.MinimumSize = New System.Drawing.Size(150, 35)
Me.btnGradeLevel.Name = "btnGradeLevel"
Me.btnGradeLevel.PressedGradientBackColor = System.Drawing.Color.Transparent
Me.btnGradeLevel.PressedGradientForeColor = System.Drawing.Color.Black
Me.btnGradeLevel.Size = New System.Drawing.Size(166, 35)
Me.btnGradeLevel.TabIndex = 15
Me.btnGradeLevel.Text = "Grade Level"
Me.btnGradeLevel.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
Me.btnGradeLevel.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText
Me.btnGradeLevel.UseVisualStyleBackColor = false
'
'btnCostcenter
'
Me.btnCostcenter.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left)  _
            Or System.Windows.Forms.AnchorStyles.Right),System.Windows.Forms.AnchorStyles)
Me.btnCostcenter.BackColor = System.Drawing.Color.Transparent
Me.btnCostcenter.BackgroundImage = CType(resources.GetObject("btnCostcenter.BackgroundImage"),System.Drawing.Image)
Me.btnCostcenter.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
Me.btnCostcenter.BorderColor = System.Drawing.Color.Empty
Me.btnCostcenter.ButtonType = eZee.Common.eZeeLightButton.enButtonType.CheckButton
Me.btnCostcenter.FlatAppearance.BorderSize = 0
Me.btnCostcenter.FlatStyle = System.Windows.Forms.FlatStyle.Flat
Me.btnCostcenter.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
Me.btnCostcenter.ForeColor = System.Drawing.Color.Black
Me.btnCostcenter.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
Me.btnCostcenter.GradientForeColor = System.Drawing.Color.Black
Me.btnCostcenter.HoverGradientBackColor = System.Drawing.Color.Transparent
Me.btnCostcenter.HoverGradientForeColor = System.Drawing.Color.Black
Me.btnCostcenter.Image = Global.Aruti.Main.My.Resources.Resources.Cost_Center
Me.btnCostcenter.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
Me.btnCostcenter.Location = New System.Drawing.Point(3, 126)
Me.btnCostcenter.MinimumSize = New System.Drawing.Size(150, 35)
Me.btnCostcenter.Name = "btnCostcenter"
Me.btnCostcenter.PressedGradientBackColor = System.Drawing.Color.Transparent
Me.btnCostcenter.PressedGradientForeColor = System.Drawing.Color.Black
Me.btnCostcenter.Size = New System.Drawing.Size(166, 35)
Me.btnCostcenter.TabIndex = 8
Me.btnCostcenter.Text = "&Cost Center"
Me.btnCostcenter.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
Me.btnCostcenter.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText
Me.btnCostcenter.UseVisualStyleBackColor = false
'
'btnWagesTable
'
Me.btnWagesTable.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left)  _
            Or System.Windows.Forms.AnchorStyles.Right),System.Windows.Forms.AnchorStyles)
Me.btnWagesTable.BackColor = System.Drawing.Color.Transparent
Me.btnWagesTable.BackgroundImage = CType(resources.GetObject("btnWagesTable.BackgroundImage"),System.Drawing.Image)
Me.btnWagesTable.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
Me.btnWagesTable.BorderColor = System.Drawing.Color.Empty
Me.btnWagesTable.ButtonType = eZee.Common.eZeeLightButton.enButtonType.CheckButton
Me.btnWagesTable.FlatAppearance.BorderSize = 0
Me.btnWagesTable.FlatStyle = System.Windows.Forms.FlatStyle.Flat
Me.btnWagesTable.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
Me.btnWagesTable.ForeColor = System.Drawing.Color.Black
Me.btnWagesTable.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
Me.btnWagesTable.GradientForeColor = System.Drawing.Color.Black
Me.btnWagesTable.HoverGradientBackColor = System.Drawing.Color.Transparent
Me.btnWagesTable.HoverGradientForeColor = System.Drawing.Color.Black
Me.btnWagesTable.Image = Global.Aruti.Main.My.Resources.Resources.wages_table
Me.btnWagesTable.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
Me.btnWagesTable.Location = New System.Drawing.Point(3, 167)
Me.btnWagesTable.MinimumSize = New System.Drawing.Size(150, 35)
Me.btnWagesTable.Name = "btnWagesTable"
Me.btnWagesTable.PressedGradientBackColor = System.Drawing.Color.Transparent
Me.btnWagesTable.PressedGradientForeColor = System.Drawing.Color.Black
Me.btnWagesTable.Size = New System.Drawing.Size(166, 35)
Me.btnWagesTable.TabIndex = 16
Me.btnWagesTable.Text = "&Wages Table"
Me.btnWagesTable.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
Me.btnWagesTable.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText
Me.btnWagesTable.UseVisualStyleBackColor = false
'
'btnPayrollPeriods
'
Me.btnPayrollPeriods.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left)  _
            Or System.Windows.Forms.AnchorStyles.Right),System.Windows.Forms.AnchorStyles)
Me.btnPayrollPeriods.BackColor = System.Drawing.Color.Transparent
Me.btnPayrollPeriods.BackgroundImage = CType(resources.GetObject("btnPayrollPeriods.BackgroundImage"),System.Drawing.Image)
Me.btnPayrollPeriods.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
Me.btnPayrollPeriods.BorderColor = System.Drawing.Color.Empty
Me.btnPayrollPeriods.ButtonType = eZee.Common.eZeeLightButton.enButtonType.CheckButton
Me.btnPayrollPeriods.FlatAppearance.BorderSize = 0
Me.btnPayrollPeriods.FlatStyle = System.Windows.Forms.FlatStyle.Flat
Me.btnPayrollPeriods.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
Me.btnPayrollPeriods.ForeColor = System.Drawing.Color.Black
Me.btnPayrollPeriods.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
Me.btnPayrollPeriods.GradientForeColor = System.Drawing.Color.Black
Me.btnPayrollPeriods.HoverGradientBackColor = System.Drawing.Color.Transparent
Me.btnPayrollPeriods.HoverGradientForeColor = System.Drawing.Color.Black
Me.btnPayrollPeriods.Image = Global.Aruti.Main.My.Resources.Resources.Periods
Me.btnPayrollPeriods.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
Me.btnPayrollPeriods.Location = New System.Drawing.Point(3, 208)
Me.btnPayrollPeriods.MinimumSize = New System.Drawing.Size(150, 35)
Me.btnPayrollPeriods.Name = "btnPayrollPeriods"
Me.btnPayrollPeriods.PressedGradientBackColor = System.Drawing.Color.Transparent
Me.btnPayrollPeriods.PressedGradientForeColor = System.Drawing.Color.Black
Me.btnPayrollPeriods.Size = New System.Drawing.Size(166, 35)
Me.btnPayrollPeriods.TabIndex = 7
Me.btnPayrollPeriods.Text = "Payroll Pe&riods"
Me.btnPayrollPeriods.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
Me.btnPayrollPeriods.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText
Me.btnPayrollPeriods.UseVisualStyleBackColor = false
'
'btnSalaryIncrement
'
Me.btnSalaryIncrement.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left)  _
            Or System.Windows.Forms.AnchorStyles.Right),System.Windows.Forms.AnchorStyles)
Me.btnSalaryIncrement.BackColor = System.Drawing.Color.Transparent
Me.btnSalaryIncrement.BackgroundImage = CType(resources.GetObject("btnSalaryIncrement.BackgroundImage"),System.Drawing.Image)
Me.btnSalaryIncrement.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
Me.btnSalaryIncrement.BorderColor = System.Drawing.Color.Empty
Me.btnSalaryIncrement.ButtonType = eZee.Common.eZeeLightButton.enButtonType.CheckButton
Me.btnSalaryIncrement.FlatAppearance.BorderSize = 0
Me.btnSalaryIncrement.FlatStyle = System.Windows.Forms.FlatStyle.Flat
Me.btnSalaryIncrement.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
Me.btnSalaryIncrement.ForeColor = System.Drawing.Color.Black
Me.btnSalaryIncrement.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
Me.btnSalaryIncrement.GradientForeColor = System.Drawing.Color.Black
Me.btnSalaryIncrement.HoverGradientBackColor = System.Drawing.Color.Transparent
Me.btnSalaryIncrement.HoverGradientForeColor = System.Drawing.Color.Black
Me.btnSalaryIncrement.Image = Global.Aruti.Main.My.Resources.Resources.salary_increment
Me.btnSalaryIncrement.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
Me.btnSalaryIncrement.Location = New System.Drawing.Point(3, 249)
Me.btnSalaryIncrement.MinimumSize = New System.Drawing.Size(150, 35)
Me.btnSalaryIncrement.Name = "btnSalaryIncrement"
Me.btnSalaryIncrement.PressedGradientBackColor = System.Drawing.Color.Transparent
Me.btnSalaryIncrement.PressedGradientForeColor = System.Drawing.Color.Black
Me.btnSalaryIncrement.Size = New System.Drawing.Size(166, 35)
Me.btnSalaryIncrement.TabIndex = 15
Me.btnSalaryIncrement.Text = "Salary &Change"
Me.btnSalaryIncrement.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
Me.btnSalaryIncrement.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText
Me.btnSalaryIncrement.UseVisualStyleBackColor = false
'
'btnClosePayroll
'
Me.btnClosePayroll.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left)  _
            Or System.Windows.Forms.AnchorStyles.Right),System.Windows.Forms.AnchorStyles)
Me.btnClosePayroll.BackColor = System.Drawing.Color.Transparent
Me.btnClosePayroll.BackgroundImage = CType(resources.GetObject("btnClosePayroll.BackgroundImage"),System.Drawing.Image)
Me.btnClosePayroll.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
Me.btnClosePayroll.BorderColor = System.Drawing.Color.Empty
Me.btnClosePayroll.ButtonType = eZee.Common.eZeeLightButton.enButtonType.CheckButton
Me.btnClosePayroll.FlatAppearance.BorderSize = 0
Me.btnClosePayroll.FlatStyle = System.Windows.Forms.FlatStyle.Flat
Me.btnClosePayroll.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
Me.btnClosePayroll.ForeColor = System.Drawing.Color.Black
Me.btnClosePayroll.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
Me.btnClosePayroll.GradientForeColor = System.Drawing.Color.Black
Me.btnClosePayroll.HoverGradientBackColor = System.Drawing.Color.Transparent
Me.btnClosePayroll.HoverGradientForeColor = System.Drawing.Color.Black
Me.btnClosePayroll.Image = Global.Aruti.Main.My.Resources.Resources.Close_Payroll
Me.btnClosePayroll.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
Me.btnClosePayroll.Location = New System.Drawing.Point(3, 290)
Me.btnClosePayroll.MinimumSize = New System.Drawing.Size(150, 35)
Me.btnClosePayroll.Name = "btnClosePayroll"
Me.btnClosePayroll.PressedGradientBackColor = System.Drawing.Color.Transparent
Me.btnClosePayroll.PressedGradientForeColor = System.Drawing.Color.Black
Me.btnClosePayroll.Size = New System.Drawing.Size(166, 35)
Me.btnClosePayroll.TabIndex = 22
Me.btnClosePayroll.Text = "Cl&ose Payroll"
Me.btnClosePayroll.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
Me.btnClosePayroll.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText
Me.btnClosePayroll.UseVisualStyleBackColor = false
'
'fpnlMedicalInfo
'
Me.fpnlMedicalInfo.AutoScroll = true
Me.fpnlMedicalInfo.Controls.Add(Me.btnMedicalMasters)
Me.fpnlMedicalInfo.Controls.Add(Me.btnMedicalInstitute)
Me.fpnlMedicalInfo.Controls.Add(Me.btnCategoryAssignment)
Me.fpnlMedicalInfo.Controls.Add(Me.btnCoverAssignment)
Me.fpnlMedicalInfo.Controls.Add(Me.btnDependantException)
Me.fpnlMedicalInfo.Controls.Add(Me.btnSickSheet)
Me.fpnlMedicalInfo.Controls.Add(Me.btnMedicalClaim)
Me.fpnlMedicalInfo.Controls.Add(Me.btnEmployeeInjuries)
Me.fpnlMedicalInfo.Location = New System.Drawing.Point(1187, 3)
Me.fpnlMedicalInfo.Name = "fpnlMedicalInfo"
Me.fpnlMedicalInfo.Size = New System.Drawing.Size(190, 41)
Me.fpnlMedicalInfo.TabIndex = 6
Me.fpnlMedicalInfo.Visible = false
'
'btnMedicalMasters
'
Me.btnMedicalMasters.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left)  _
            Or System.Windows.Forms.AnchorStyles.Right),System.Windows.Forms.AnchorStyles)
Me.btnMedicalMasters.BackColor = System.Drawing.Color.Transparent
Me.btnMedicalMasters.BackgroundImage = CType(resources.GetObject("btnMedicalMasters.BackgroundImage"),System.Drawing.Image)
Me.btnMedicalMasters.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
Me.btnMedicalMasters.BorderColor = System.Drawing.Color.Empty
Me.btnMedicalMasters.ButtonType = eZee.Common.eZeeLightButton.enButtonType.CheckButton
Me.btnMedicalMasters.FlatAppearance.BorderSize = 0
Me.btnMedicalMasters.FlatStyle = System.Windows.Forms.FlatStyle.Flat
Me.btnMedicalMasters.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
Me.btnMedicalMasters.ForeColor = System.Drawing.Color.Black
Me.btnMedicalMasters.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
Me.btnMedicalMasters.GradientForeColor = System.Drawing.Color.Black
Me.btnMedicalMasters.HoverGradientBackColor = System.Drawing.Color.Transparent
Me.btnMedicalMasters.HoverGradientForeColor = System.Drawing.Color.Black
Me.btnMedicalMasters.Image = Global.Aruti.Main.My.Resources.Resources.Master_List
Me.btnMedicalMasters.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
Me.btnMedicalMasters.Location = New System.Drawing.Point(3, 3)
Me.btnMedicalMasters.MinimumSize = New System.Drawing.Size(150, 35)
Me.btnMedicalMasters.Name = "btnMedicalMasters"
Me.btnMedicalMasters.PressedGradientBackColor = System.Drawing.Color.Transparent
Me.btnMedicalMasters.PressedGradientForeColor = System.Drawing.Color.Black
Me.btnMedicalMasters.Size = New System.Drawing.Size(166, 35)
Me.btnMedicalMasters.TabIndex = 6
Me.btnMedicalMasters.Text = "Masters &List"
Me.btnMedicalMasters.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
Me.btnMedicalMasters.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText
Me.btnMedicalMasters.UseVisualStyleBackColor = false
'
'btnMedicalInstitute
'
Me.btnMedicalInstitute.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left)  _
            Or System.Windows.Forms.AnchorStyles.Right),System.Windows.Forms.AnchorStyles)
Me.btnMedicalInstitute.BackColor = System.Drawing.Color.Transparent
Me.btnMedicalInstitute.BackgroundImage = CType(resources.GetObject("btnMedicalInstitute.BackgroundImage"),System.Drawing.Image)
Me.btnMedicalInstitute.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
Me.btnMedicalInstitute.BorderColor = System.Drawing.Color.Empty
Me.btnMedicalInstitute.ButtonType = eZee.Common.eZeeLightButton.enButtonType.CheckButton
Me.btnMedicalInstitute.FlatAppearance.BorderSize = 0
Me.btnMedicalInstitute.FlatStyle = System.Windows.Forms.FlatStyle.Flat
Me.btnMedicalInstitute.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
Me.btnMedicalInstitute.ForeColor = System.Drawing.Color.Black
Me.btnMedicalInstitute.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
Me.btnMedicalInstitute.GradientForeColor = System.Drawing.Color.Black
Me.btnMedicalInstitute.HoverGradientBackColor = System.Drawing.Color.Transparent
Me.btnMedicalInstitute.HoverGradientForeColor = System.Drawing.Color.Black
Me.btnMedicalInstitute.Image = Global.Aruti.Main.My.Resources.Resources.Medical_Institute
Me.btnMedicalInstitute.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
Me.btnMedicalInstitute.Location = New System.Drawing.Point(3, 44)
Me.btnMedicalInstitute.MinimumSize = New System.Drawing.Size(150, 35)
Me.btnMedicalInstitute.Name = "btnMedicalInstitute"
Me.btnMedicalInstitute.PressedGradientBackColor = System.Drawing.Color.Transparent
Me.btnMedicalInstitute.PressedGradientForeColor = System.Drawing.Color.Black
Me.btnMedicalInstitute.Size = New System.Drawing.Size(166, 35)
Me.btnMedicalInstitute.TabIndex = 7
Me.btnMedicalInstitute.Text = "Service &Provider"
Me.btnMedicalInstitute.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
Me.btnMedicalInstitute.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText
Me.btnMedicalInstitute.UseVisualStyleBackColor = false
'
'btnCategoryAssignment
'
Me.btnCategoryAssignment.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left)  _
            Or System.Windows.Forms.AnchorStyles.Right),System.Windows.Forms.AnchorStyles)
Me.btnCategoryAssignment.BackColor = System.Drawing.Color.Transparent
Me.btnCategoryAssignment.BackgroundImage = CType(resources.GetObject("btnCategoryAssignment.BackgroundImage"),System.Drawing.Image)
Me.btnCategoryAssignment.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
Me.btnCategoryAssignment.BorderColor = System.Drawing.Color.Empty
Me.btnCategoryAssignment.ButtonType = eZee.Common.eZeeLightButton.enButtonType.CheckButton
Me.btnCategoryAssignment.FlatAppearance.BorderSize = 0
Me.btnCategoryAssignment.FlatStyle = System.Windows.Forms.FlatStyle.Flat
Me.btnCategoryAssignment.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
Me.btnCategoryAssignment.ForeColor = System.Drawing.Color.Black
Me.btnCategoryAssignment.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
Me.btnCategoryAssignment.GradientForeColor = System.Drawing.Color.Black
Me.btnCategoryAssignment.HoverGradientBackColor = System.Drawing.Color.Transparent
Me.btnCategoryAssignment.HoverGradientForeColor = System.Drawing.Color.Black
Me.btnCategoryAssignment.Image = Global.Aruti.Main.My.Resources.Resources.Assign_Category
Me.btnCategoryAssignment.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
Me.btnCategoryAssignment.Location = New System.Drawing.Point(3, 85)
Me.btnCategoryAssignment.MinimumSize = New System.Drawing.Size(150, 35)
Me.btnCategoryAssignment.Name = "btnCategoryAssignment"
Me.btnCategoryAssignment.PressedGradientBackColor = System.Drawing.Color.Transparent
Me.btnCategoryAssignment.PressedGradientForeColor = System.Drawing.Color.Black
Me.btnCategoryAssignment.Size = New System.Drawing.Size(166, 35)
Me.btnCategoryAssignment.TabIndex = 9
Me.btnCategoryAssignment.Text = "&Assign Category "
Me.btnCategoryAssignment.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
Me.btnCategoryAssignment.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText
Me.btnCategoryAssignment.UseVisualStyleBackColor = false
'
'btnCoverAssignment
'
Me.btnCoverAssignment.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left)  _
            Or System.Windows.Forms.AnchorStyles.Right),System.Windows.Forms.AnchorStyles)
Me.btnCoverAssignment.BackColor = System.Drawing.Color.Transparent
Me.btnCoverAssignment.BackgroundImage = CType(resources.GetObject("btnCoverAssignment.BackgroundImage"),System.Drawing.Image)
Me.btnCoverAssignment.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
Me.btnCoverAssignment.BorderColor = System.Drawing.Color.Empty
Me.btnCoverAssignment.ButtonType = eZee.Common.eZeeLightButton.enButtonType.CheckButton
Me.btnCoverAssignment.FlatAppearance.BorderSize = 0
Me.btnCoverAssignment.FlatStyle = System.Windows.Forms.FlatStyle.Flat
Me.btnCoverAssignment.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
Me.btnCoverAssignment.ForeColor = System.Drawing.Color.Black
Me.btnCoverAssignment.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
Me.btnCoverAssignment.GradientForeColor = System.Drawing.Color.Black
Me.btnCoverAssignment.HoverGradientBackColor = System.Drawing.Color.Transparent
Me.btnCoverAssignment.HoverGradientForeColor = System.Drawing.Color.Black
Me.btnCoverAssignment.Image = Global.Aruti.Main.My.Resources.Resources.Assign_Cover
Me.btnCoverAssignment.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
Me.btnCoverAssignment.Location = New System.Drawing.Point(3, 126)
Me.btnCoverAssignment.MinimumSize = New System.Drawing.Size(150, 35)
Me.btnCoverAssignment.Name = "btnCoverAssignment"
Me.btnCoverAssignment.PressedGradientBackColor = System.Drawing.Color.Transparent
Me.btnCoverAssignment.PressedGradientForeColor = System.Drawing.Color.Black
Me.btnCoverAssignment.Size = New System.Drawing.Size(166, 35)
Me.btnCoverAssignment.TabIndex = 10
Me.btnCoverAssignment.Text = "Assign Co&ver"
Me.btnCoverAssignment.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
Me.btnCoverAssignment.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText
Me.btnCoverAssignment.UseVisualStyleBackColor = false
'
'btnDependantException
'
Me.btnDependantException.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left)  _
            Or System.Windows.Forms.AnchorStyles.Right),System.Windows.Forms.AnchorStyles)
Me.btnDependantException.BackColor = System.Drawing.Color.Transparent
Me.btnDependantException.BackgroundImage = CType(resources.GetObject("btnDependantException.BackgroundImage"),System.Drawing.Image)
Me.btnDependantException.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
Me.btnDependantException.BorderColor = System.Drawing.Color.Empty
Me.btnDependantException.ButtonType = eZee.Common.eZeeLightButton.enButtonType.CheckButton
Me.btnDependantException.FlatAppearance.BorderSize = 0
Me.btnDependantException.FlatStyle = System.Windows.Forms.FlatStyle.Flat
Me.btnDependantException.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
Me.btnDependantException.ForeColor = System.Drawing.Color.Black
Me.btnDependantException.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
Me.btnDependantException.GradientForeColor = System.Drawing.Color.Black
Me.btnDependantException.HoverGradientBackColor = System.Drawing.Color.Transparent
Me.btnDependantException.HoverGradientForeColor = System.Drawing.Color.Black
Me.btnDependantException.Image = Global.Aruti.Main.My.Resources.Resources.DependantException
Me.btnDependantException.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
Me.btnDependantException.Location = New System.Drawing.Point(3, 167)
Me.btnDependantException.MinimumSize = New System.Drawing.Size(150, 35)
Me.btnDependantException.Name = "btnDependantException"
Me.btnDependantException.PressedGradientBackColor = System.Drawing.Color.Transparent
Me.btnDependantException.PressedGradientForeColor = System.Drawing.Color.Black
Me.btnDependantException.Size = New System.Drawing.Size(166, 35)
Me.btnDependantException.TabIndex = 13
Me.btnDependantException.Text = "Dependant Exception"
Me.btnDependantException.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
Me.btnDependantException.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText
Me.btnDependantException.UseVisualStyleBackColor = false
Me.btnDependantException.Visible = false
'
'btnSickSheet
'
Me.btnSickSheet.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left)  _
            Or System.Windows.Forms.AnchorStyles.Right),System.Windows.Forms.AnchorStyles)
Me.btnSickSheet.BackColor = System.Drawing.Color.Transparent
Me.btnSickSheet.BackgroundImage = CType(resources.GetObject("btnSickSheet.BackgroundImage"),System.Drawing.Image)
Me.btnSickSheet.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
Me.btnSickSheet.BorderColor = System.Drawing.Color.Empty
Me.btnSickSheet.ButtonType = eZee.Common.eZeeLightButton.enButtonType.CheckButton
Me.btnSickSheet.FlatAppearance.BorderSize = 0
Me.btnSickSheet.FlatStyle = System.Windows.Forms.FlatStyle.Flat
Me.btnSickSheet.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
Me.btnSickSheet.ForeColor = System.Drawing.Color.Black
Me.btnSickSheet.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
Me.btnSickSheet.GradientForeColor = System.Drawing.Color.Black
Me.btnSickSheet.HoverGradientBackColor = System.Drawing.Color.Transparent
Me.btnSickSheet.HoverGradientForeColor = System.Drawing.Color.Black
Me.btnSickSheet.Image = Global.Aruti.Main.My.Resources.Resources.Sick_Sheet_form
Me.btnSickSheet.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
Me.btnSickSheet.Location = New System.Drawing.Point(3, 208)
Me.btnSickSheet.MinimumSize = New System.Drawing.Size(150, 35)
Me.btnSickSheet.Name = "btnSickSheet"
Me.btnSickSheet.PressedGradientBackColor = System.Drawing.Color.Transparent
Me.btnSickSheet.PressedGradientForeColor = System.Drawing.Color.Black
Me.btnSickSheet.Size = New System.Drawing.Size(166, 35)
Me.btnSickSheet.TabIndex = 12
Me.btnSickSheet.Text = "Sick Sheet"
Me.btnSickSheet.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
Me.btnSickSheet.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText
Me.btnSickSheet.UseVisualStyleBackColor = false
'
'btnMedicalClaim
'
Me.btnMedicalClaim.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left)  _
            Or System.Windows.Forms.AnchorStyles.Right),System.Windows.Forms.AnchorStyles)
Me.btnMedicalClaim.BackColor = System.Drawing.Color.Transparent
Me.btnMedicalClaim.BackgroundImage = CType(resources.GetObject("btnMedicalClaim.BackgroundImage"),System.Drawing.Image)
Me.btnMedicalClaim.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
Me.btnMedicalClaim.BorderColor = System.Drawing.Color.Empty
Me.btnMedicalClaim.ButtonType = eZee.Common.eZeeLightButton.enButtonType.CheckButton
Me.btnMedicalClaim.FlatAppearance.BorderSize = 0
Me.btnMedicalClaim.FlatStyle = System.Windows.Forms.FlatStyle.Flat
Me.btnMedicalClaim.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
Me.btnMedicalClaim.ForeColor = System.Drawing.Color.Black
Me.btnMedicalClaim.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
Me.btnMedicalClaim.GradientForeColor = System.Drawing.Color.Black
Me.btnMedicalClaim.HoverGradientBackColor = System.Drawing.Color.Transparent
Me.btnMedicalClaim.HoverGradientForeColor = System.Drawing.Color.Black
Me.btnMedicalClaim.Image = Global.Aruti.Main.My.Resources.Resources.Mediclaim
Me.btnMedicalClaim.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
Me.btnMedicalClaim.Location = New System.Drawing.Point(3, 249)
Me.btnMedicalClaim.MinimumSize = New System.Drawing.Size(150, 35)
Me.btnMedicalClaim.Name = "btnMedicalClaim"
Me.btnMedicalClaim.PressedGradientBackColor = System.Drawing.Color.Transparent
Me.btnMedicalClaim.PressedGradientForeColor = System.Drawing.Color.Black
Me.btnMedicalClaim.Size = New System.Drawing.Size(166, 35)
Me.btnMedicalClaim.TabIndex = 11
Me.btnMedicalClaim.Text = "Medical C&laim"
Me.btnMedicalClaim.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
Me.btnMedicalClaim.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText
Me.btnMedicalClaim.UseVisualStyleBackColor = false
'
'btnEmployeeInjuries
'
Me.btnEmployeeInjuries.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left)  _
            Or System.Windows.Forms.AnchorStyles.Right),System.Windows.Forms.AnchorStyles)
Me.btnEmployeeInjuries.BackColor = System.Drawing.Color.Transparent
Me.btnEmployeeInjuries.BackgroundImage = CType(resources.GetObject("btnEmployeeInjuries.BackgroundImage"),System.Drawing.Image)
Me.btnEmployeeInjuries.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
Me.btnEmployeeInjuries.BorderColor = System.Drawing.Color.Empty
Me.btnEmployeeInjuries.ButtonType = eZee.Common.eZeeLightButton.enButtonType.CheckButton
Me.btnEmployeeInjuries.FlatAppearance.BorderSize = 0
Me.btnEmployeeInjuries.FlatStyle = System.Windows.Forms.FlatStyle.Flat
Me.btnEmployeeInjuries.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
Me.btnEmployeeInjuries.ForeColor = System.Drawing.Color.Black
Me.btnEmployeeInjuries.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
Me.btnEmployeeInjuries.GradientForeColor = System.Drawing.Color.Black
Me.btnEmployeeInjuries.HoverGradientBackColor = System.Drawing.Color.Transparent
Me.btnEmployeeInjuries.HoverGradientForeColor = System.Drawing.Color.Black
Me.btnEmployeeInjuries.Image = Global.Aruti.Main.My.Resources.Resources.Employee_Injuries
Me.btnEmployeeInjuries.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
Me.btnEmployeeInjuries.Location = New System.Drawing.Point(3, 290)
Me.btnEmployeeInjuries.MinimumSize = New System.Drawing.Size(150, 35)
Me.btnEmployeeInjuries.Name = "btnEmployeeInjuries"
Me.btnEmployeeInjuries.PressedGradientBackColor = System.Drawing.Color.Transparent
Me.btnEmployeeInjuries.PressedGradientForeColor = System.Drawing.Color.Black
Me.btnEmployeeInjuries.Size = New System.Drawing.Size(166, 35)
Me.btnEmployeeInjuries.TabIndex = 8
Me.btnEmployeeInjuries.Text = "Employee In&juries"
Me.btnEmployeeInjuries.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
Me.btnEmployeeInjuries.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText
Me.btnEmployeeInjuries.UseVisualStyleBackColor = false
'
'fpnlPayActivity
'
Me.fpnlPayActivity.AutoScroll = true
Me.fpnlPayActivity.Controls.Add(Me.btnMeasureList)
Me.fpnlPayActivity.Controls.Add(Me.btnActivity_List)
Me.fpnlPayActivity.Controls.Add(Me.btnActivityRate)
Me.fpnlPayActivity.Controls.Add(Me.btnPayPerActivity)
Me.fpnlPayActivity.Controls.Add(Me.btnPayrollPosting)
Me.fpnlPayActivity.Controls.Add(Me.btnActivityGlobalAssign)
Me.fpnlPayActivity.Location = New System.Drawing.Point(1187, 50)
Me.fpnlPayActivity.Name = "fpnlPayActivity"
Me.fpnlPayActivity.Size = New System.Drawing.Size(189, 40)
Me.fpnlPayActivity.TabIndex = 25
'
'btnMeasureList
'
Me.btnMeasureList.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left)  _
            Or System.Windows.Forms.AnchorStyles.Right),System.Windows.Forms.AnchorStyles)
Me.btnMeasureList.BackColor = System.Drawing.Color.Transparent
Me.btnMeasureList.BackgroundImage = CType(resources.GetObject("btnMeasureList.BackgroundImage"),System.Drawing.Image)
Me.btnMeasureList.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
Me.btnMeasureList.BorderColor = System.Drawing.Color.Empty
Me.btnMeasureList.ButtonType = eZee.Common.eZeeLightButton.enButtonType.CheckButton
Me.btnMeasureList.FlatAppearance.BorderSize = 0
Me.btnMeasureList.FlatStyle = System.Windows.Forms.FlatStyle.Flat
Me.btnMeasureList.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
Me.btnMeasureList.ForeColor = System.Drawing.Color.Black
Me.btnMeasureList.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
Me.btnMeasureList.GradientForeColor = System.Drawing.Color.Black
Me.btnMeasureList.HoverGradientBackColor = System.Drawing.Color.Transparent
Me.btnMeasureList.HoverGradientForeColor = System.Drawing.Color.Black
Me.btnMeasureList.Image = Global.Aruti.Main.My.Resources.Resources._1_Measure_List
Me.btnMeasureList.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
Me.btnMeasureList.Location = New System.Drawing.Point(3, 3)
Me.btnMeasureList.MinimumSize = New System.Drawing.Size(150, 35)
Me.btnMeasureList.Name = "btnMeasureList"
Me.btnMeasureList.PressedGradientBackColor = System.Drawing.Color.Transparent
Me.btnMeasureList.PressedGradientForeColor = System.Drawing.Color.Black
Me.btnMeasureList.Size = New System.Drawing.Size(166, 35)
Me.btnMeasureList.TabIndex = 31
Me.btnMeasureList.Text = "&Measure List"
Me.btnMeasureList.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
Me.btnMeasureList.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText
Me.btnMeasureList.UseVisualStyleBackColor = false
'
'btnActivity_List
'
Me.btnActivity_List.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left)  _
            Or System.Windows.Forms.AnchorStyles.Right),System.Windows.Forms.AnchorStyles)
Me.btnActivity_List.BackColor = System.Drawing.Color.Transparent
Me.btnActivity_List.BackgroundImage = CType(resources.GetObject("btnActivity_List.BackgroundImage"),System.Drawing.Image)
Me.btnActivity_List.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
Me.btnActivity_List.BorderColor = System.Drawing.Color.Empty
Me.btnActivity_List.ButtonType = eZee.Common.eZeeLightButton.enButtonType.CheckButton
Me.btnActivity_List.FlatAppearance.BorderSize = 0
Me.btnActivity_List.FlatStyle = System.Windows.Forms.FlatStyle.Flat
Me.btnActivity_List.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
Me.btnActivity_List.ForeColor = System.Drawing.Color.Black
Me.btnActivity_List.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
Me.btnActivity_List.GradientForeColor = System.Drawing.Color.Black
Me.btnActivity_List.HoverGradientBackColor = System.Drawing.Color.Transparent
Me.btnActivity_List.HoverGradientForeColor = System.Drawing.Color.Black
Me.btnActivity_List.Image = Global.Aruti.Main.My.Resources.Resources._2_Activity_List
Me.btnActivity_List.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
Me.btnActivity_List.Location = New System.Drawing.Point(3, 44)
Me.btnActivity_List.MinimumSize = New System.Drawing.Size(150, 35)
Me.btnActivity_List.Name = "btnActivity_List"
Me.btnActivity_List.PressedGradientBackColor = System.Drawing.Color.Transparent
Me.btnActivity_List.PressedGradientForeColor = System.Drawing.Color.Black
Me.btnActivity_List.Size = New System.Drawing.Size(166, 35)
Me.btnActivity_List.TabIndex = 29
Me.btnActivity_List.Text = "Ac&tivity List"
Me.btnActivity_List.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
Me.btnActivity_List.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText
Me.btnActivity_List.UseVisualStyleBackColor = false
'
'btnActivityRate
'
Me.btnActivityRate.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left)  _
            Or System.Windows.Forms.AnchorStyles.Right),System.Windows.Forms.AnchorStyles)
Me.btnActivityRate.BackColor = System.Drawing.Color.Transparent
Me.btnActivityRate.BackgroundImage = CType(resources.GetObject("btnActivityRate.BackgroundImage"),System.Drawing.Image)
Me.btnActivityRate.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
Me.btnActivityRate.BorderColor = System.Drawing.Color.Empty
Me.btnActivityRate.ButtonType = eZee.Common.eZeeLightButton.enButtonType.CheckButton
Me.btnActivityRate.FlatAppearance.BorderSize = 0
Me.btnActivityRate.FlatStyle = System.Windows.Forms.FlatStyle.Flat
Me.btnActivityRate.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
Me.btnActivityRate.ForeColor = System.Drawing.Color.Black
Me.btnActivityRate.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
Me.btnActivityRate.GradientForeColor = System.Drawing.Color.Black
Me.btnActivityRate.HoverGradientBackColor = System.Drawing.Color.Transparent
Me.btnActivityRate.HoverGradientForeColor = System.Drawing.Color.Black
Me.btnActivityRate.Image = Global.Aruti.Main.My.Resources.Resources._3_Activity_Rates
Me.btnActivityRate.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
Me.btnActivityRate.Location = New System.Drawing.Point(3, 85)
Me.btnActivityRate.MinimumSize = New System.Drawing.Size(150, 35)
Me.btnActivityRate.Name = "btnActivityRate"
Me.btnActivityRate.PressedGradientBackColor = System.Drawing.Color.Transparent
Me.btnActivityRate.PressedGradientForeColor = System.Drawing.Color.Black
Me.btnActivityRate.Size = New System.Drawing.Size(166, 35)
Me.btnActivityRate.TabIndex = 32
Me.btnActivityRate.Text = "Set Activity &Rates"
Me.btnActivityRate.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
Me.btnActivityRate.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText
Me.btnActivityRate.UseVisualStyleBackColor = false
'
'btnPayPerActivity
'
Me.btnPayPerActivity.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left)  _
            Or System.Windows.Forms.AnchorStyles.Right),System.Windows.Forms.AnchorStyles)
Me.btnPayPerActivity.BackColor = System.Drawing.Color.Transparent
Me.btnPayPerActivity.BackgroundImage = CType(resources.GetObject("btnPayPerActivity.BackgroundImage"),System.Drawing.Image)
Me.btnPayPerActivity.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
Me.btnPayPerActivity.BorderColor = System.Drawing.Color.Empty
Me.btnPayPerActivity.ButtonType = eZee.Common.eZeeLightButton.enButtonType.CheckButton
Me.btnPayPerActivity.FlatAppearance.BorderSize = 0
Me.btnPayPerActivity.FlatStyle = System.Windows.Forms.FlatStyle.Flat
Me.btnPayPerActivity.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
Me.btnPayPerActivity.ForeColor = System.Drawing.Color.Black
Me.btnPayPerActivity.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
Me.btnPayPerActivity.GradientForeColor = System.Drawing.Color.Black
Me.btnPayPerActivity.HoverGradientBackColor = System.Drawing.Color.Transparent
Me.btnPayPerActivity.HoverGradientForeColor = System.Drawing.Color.Black
Me.btnPayPerActivity.Image = Global.Aruti.Main.My.Resources.Resources._4_Pay_Per_Activity
Me.btnPayPerActivity.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
Me.btnPayPerActivity.Location = New System.Drawing.Point(3, 126)
Me.btnPayPerActivity.MinimumSize = New System.Drawing.Size(150, 35)
Me.btnPayPerActivity.Name = "btnPayPerActivity"
Me.btnPayPerActivity.PressedGradientBackColor = System.Drawing.Color.Transparent
Me.btnPayPerActivity.PressedGradientForeColor = System.Drawing.Color.Black
Me.btnPayPerActivity.Size = New System.Drawing.Size(166, 35)
Me.btnPayPerActivity.TabIndex = 30
Me.btnPayPerActivity.Text = "Pa&y Per Activity"
Me.btnPayPerActivity.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
Me.btnPayPerActivity.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText
Me.btnPayPerActivity.UseVisualStyleBackColor = false
'
'btnPayrollPosting
'
Me.btnPayrollPosting.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left)  _
            Or System.Windows.Forms.AnchorStyles.Right),System.Windows.Forms.AnchorStyles)
Me.btnPayrollPosting.BackColor = System.Drawing.Color.Transparent
Me.btnPayrollPosting.BackgroundImage = CType(resources.GetObject("btnPayrollPosting.BackgroundImage"),System.Drawing.Image)
Me.btnPayrollPosting.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
Me.btnPayrollPosting.BorderColor = System.Drawing.Color.Empty
Me.btnPayrollPosting.ButtonType = eZee.Common.eZeeLightButton.enButtonType.CheckButton
Me.btnPayrollPosting.FlatAppearance.BorderSize = 0
Me.btnPayrollPosting.FlatStyle = System.Windows.Forms.FlatStyle.Flat
Me.btnPayrollPosting.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
Me.btnPayrollPosting.ForeColor = System.Drawing.Color.Black
Me.btnPayrollPosting.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
Me.btnPayrollPosting.GradientForeColor = System.Drawing.Color.Black
Me.btnPayrollPosting.HoverGradientBackColor = System.Drawing.Color.Transparent
Me.btnPayrollPosting.HoverGradientForeColor = System.Drawing.Color.Black
Me.btnPayrollPosting.Image = Global.Aruti.Main.My.Resources.Resources._5_Post_to_Payroll
Me.btnPayrollPosting.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
Me.btnPayrollPosting.Location = New System.Drawing.Point(3, 167)
Me.btnPayrollPosting.MinimumSize = New System.Drawing.Size(150, 35)
Me.btnPayrollPosting.Name = "btnPayrollPosting"
Me.btnPayrollPosting.PressedGradientBackColor = System.Drawing.Color.Transparent
Me.btnPayrollPosting.PressedGradientForeColor = System.Drawing.Color.Black
Me.btnPayrollPosting.Size = New System.Drawing.Size(166, 35)
Me.btnPayrollPosting.TabIndex = 33
Me.btnPayrollPosting.Text = "P&ost to Payroll"
Me.btnPayrollPosting.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
Me.btnPayrollPosting.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText
Me.btnPayrollPosting.UseVisualStyleBackColor = false
'
'btnActivityGlobalAssign
'
Me.btnActivityGlobalAssign.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left)  _
            Or System.Windows.Forms.AnchorStyles.Right),System.Windows.Forms.AnchorStyles)
Me.btnActivityGlobalAssign.BackColor = System.Drawing.Color.Transparent
Me.btnActivityGlobalAssign.BackgroundImage = CType(resources.GetObject("btnActivityGlobalAssign.BackgroundImage"),System.Drawing.Image)
Me.btnActivityGlobalAssign.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
Me.btnActivityGlobalAssign.BorderColor = System.Drawing.Color.Empty
Me.btnActivityGlobalAssign.ButtonType = eZee.Common.eZeeLightButton.enButtonType.CheckButton
Me.btnActivityGlobalAssign.FlatAppearance.BorderSize = 0
Me.btnActivityGlobalAssign.FlatStyle = System.Windows.Forms.FlatStyle.Flat
Me.btnActivityGlobalAssign.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
Me.btnActivityGlobalAssign.ForeColor = System.Drawing.Color.Black
Me.btnActivityGlobalAssign.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
Me.btnActivityGlobalAssign.GradientForeColor = System.Drawing.Color.Black
Me.btnActivityGlobalAssign.HoverGradientBackColor = System.Drawing.Color.Transparent
Me.btnActivityGlobalAssign.HoverGradientForeColor = System.Drawing.Color.Black
Me.btnActivityGlobalAssign.Image = Global.Aruti.Main.My.Resources.Resources._6_Global_Assign_Activity
Me.btnActivityGlobalAssign.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
Me.btnActivityGlobalAssign.Location = New System.Drawing.Point(3, 208)
Me.btnActivityGlobalAssign.MinimumSize = New System.Drawing.Size(150, 35)
Me.btnActivityGlobalAssign.Name = "btnActivityGlobalAssign"
Me.btnActivityGlobalAssign.PressedGradientBackColor = System.Drawing.Color.Transparent
Me.btnActivityGlobalAssign.PressedGradientForeColor = System.Drawing.Color.Black
Me.btnActivityGlobalAssign.Size = New System.Drawing.Size(166, 35)
Me.btnActivityGlobalAssign.TabIndex = 34
Me.btnActivityGlobalAssign.Text = "&Global Assign"
Me.btnActivityGlobalAssign.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
Me.btnActivityGlobalAssign.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText
Me.btnActivityGlobalAssign.UseVisualStyleBackColor = false
'
'fpnlAppraisal
'
Me.fpnlAppraisal.AutoScroll = true
Me.fpnlAppraisal.Controls.Add(Me.btnAppraisalSetups)
Me.fpnlAppraisal.Controls.Add(Me.btnShortListEmployee)
Me.fpnlAppraisal.Controls.Add(Me.btnFinalShortListedEmployee)
Me.fpnlAppraisal.Location = New System.Drawing.Point(1187, 96)
Me.fpnlAppraisal.Name = "fpnlAppraisal"
Me.fpnlAppraisal.Size = New System.Drawing.Size(189, 42)
Me.fpnlAppraisal.TabIndex = 21
Me.fpnlAppraisal.Visible = false
'
'btnAppraisalSetups
'
Me.btnAppraisalSetups.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left)  _
            Or System.Windows.Forms.AnchorStyles.Right),System.Windows.Forms.AnchorStyles)
Me.btnAppraisalSetups.BackColor = System.Drawing.Color.Transparent
Me.btnAppraisalSetups.BackgroundImage = CType(resources.GetObject("btnAppraisalSetups.BackgroundImage"),System.Drawing.Image)
Me.btnAppraisalSetups.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
Me.btnAppraisalSetups.BorderColor = System.Drawing.Color.Empty
Me.btnAppraisalSetups.ButtonType = eZee.Common.eZeeLightButton.enButtonType.CheckButton
Me.btnAppraisalSetups.FlatAppearance.BorderSize = 0
Me.btnAppraisalSetups.FlatStyle = System.Windows.Forms.FlatStyle.Flat
Me.btnAppraisalSetups.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
Me.btnAppraisalSetups.ForeColor = System.Drawing.Color.Black
Me.btnAppraisalSetups.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
Me.btnAppraisalSetups.GradientForeColor = System.Drawing.Color.Black
Me.btnAppraisalSetups.HoverGradientBackColor = System.Drawing.Color.Transparent
Me.btnAppraisalSetups.HoverGradientForeColor = System.Drawing.Color.Black
Me.btnAppraisalSetups.Image = Global.Aruti.Main.My.Resources.Resources.AppraisalAnalysis
Me.btnAppraisalSetups.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
Me.btnAppraisalSetups.Location = New System.Drawing.Point(3, 3)
Me.btnAppraisalSetups.MinimumSize = New System.Drawing.Size(150, 35)
Me.btnAppraisalSetups.Name = "btnAppraisalSetups"
Me.btnAppraisalSetups.PressedGradientBackColor = System.Drawing.Color.Transparent
Me.btnAppraisalSetups.PressedGradientForeColor = System.Drawing.Color.Black
Me.btnAppraisalSetups.Size = New System.Drawing.Size(166, 35)
Me.btnAppraisalSetups.TabIndex = 16
Me.btnAppraisalSetups.Text = "Appraisal Setups"
Me.btnAppraisalSetups.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
Me.btnAppraisalSetups.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText
Me.btnAppraisalSetups.UseVisualStyleBackColor = false
'
'btnShortListEmployee
'
Me.btnShortListEmployee.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left)  _
            Or System.Windows.Forms.AnchorStyles.Right),System.Windows.Forms.AnchorStyles)
Me.btnShortListEmployee.BackColor = System.Drawing.Color.Transparent
Me.btnShortListEmployee.BackgroundImage = CType(resources.GetObject("btnShortListEmployee.BackgroundImage"),System.Drawing.Image)
Me.btnShortListEmployee.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
Me.btnShortListEmployee.BorderColor = System.Drawing.Color.Empty
Me.btnShortListEmployee.ButtonType = eZee.Common.eZeeLightButton.enButtonType.CheckButton
Me.btnShortListEmployee.FlatAppearance.BorderSize = 0
Me.btnShortListEmployee.FlatStyle = System.Windows.Forms.FlatStyle.Flat
Me.btnShortListEmployee.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
Me.btnShortListEmployee.ForeColor = System.Drawing.Color.Black
Me.btnShortListEmployee.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
Me.btnShortListEmployee.GradientForeColor = System.Drawing.Color.Black
Me.btnShortListEmployee.HoverGradientBackColor = System.Drawing.Color.Transparent
Me.btnShortListEmployee.HoverGradientForeColor = System.Drawing.Color.Black
Me.btnShortListEmployee.Image = Global.Aruti.Main.My.Resources.Resources.AppraisalAnalysis
Me.btnShortListEmployee.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
Me.btnShortListEmployee.Location = New System.Drawing.Point(3, 44)
Me.btnShortListEmployee.MinimumSize = New System.Drawing.Size(150, 35)
Me.btnShortListEmployee.Name = "btnShortListEmployee"
Me.btnShortListEmployee.PressedGradientBackColor = System.Drawing.Color.Transparent
Me.btnShortListEmployee.PressedGradientForeColor = System.Drawing.Color.Black
Me.btnShortListEmployee.Size = New System.Drawing.Size(166, 35)
Me.btnShortListEmployee.TabIndex = 14
Me.btnShortListEmployee.Text = "Appraisal Analysis"
Me.btnShortListEmployee.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
Me.btnShortListEmployee.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText
Me.btnShortListEmployee.UseVisualStyleBackColor = false
'
'btnFinalShortListedEmployee
'
Me.btnFinalShortListedEmployee.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left)  _
            Or System.Windows.Forms.AnchorStyles.Right),System.Windows.Forms.AnchorStyles)
Me.btnFinalShortListedEmployee.BackColor = System.Drawing.Color.Transparent
Me.btnFinalShortListedEmployee.BackgroundImage = CType(resources.GetObject("btnFinalShortListedEmployee.BackgroundImage"),System.Drawing.Image)
Me.btnFinalShortListedEmployee.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
Me.btnFinalShortListedEmployee.BorderColor = System.Drawing.Color.Empty
Me.btnFinalShortListedEmployee.ButtonType = eZee.Common.eZeeLightButton.enButtonType.CheckButton
Me.btnFinalShortListedEmployee.FlatAppearance.BorderSize = 0
Me.btnFinalShortListedEmployee.FlatStyle = System.Windows.Forms.FlatStyle.Flat
Me.btnFinalShortListedEmployee.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
Me.btnFinalShortListedEmployee.ForeColor = System.Drawing.Color.Black
Me.btnFinalShortListedEmployee.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
Me.btnFinalShortListedEmployee.GradientForeColor = System.Drawing.Color.Black
Me.btnFinalShortListedEmployee.HoverGradientBackColor = System.Drawing.Color.Transparent
Me.btnFinalShortListedEmployee.HoverGradientForeColor = System.Drawing.Color.Black
Me.btnFinalShortListedEmployee.Image = Global.Aruti.Main.My.Resources.Resources.Appraisal
Me.btnFinalShortListedEmployee.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
Me.btnFinalShortListedEmployee.Location = New System.Drawing.Point(3, 85)
Me.btnFinalShortListedEmployee.MinimumSize = New System.Drawing.Size(150, 35)
Me.btnFinalShortListedEmployee.Name = "btnFinalShortListedEmployee"
Me.btnFinalShortListedEmployee.PressedGradientBackColor = System.Drawing.Color.Transparent
Me.btnFinalShortListedEmployee.PressedGradientForeColor = System.Drawing.Color.Black
Me.btnFinalShortListedEmployee.Size = New System.Drawing.Size(166, 35)
Me.btnFinalShortListedEmployee.TabIndex = 15
Me.btnFinalShortListedEmployee.Text = "Appraisals"
Me.btnFinalShortListedEmployee.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
Me.btnFinalShortListedEmployee.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText
Me.btnFinalShortListedEmployee.UseVisualStyleBackColor = false
'
'fpnlClaimsExpenses
'
Me.fpnlClaimsExpenses.AutoScroll = true
Me.fpnlClaimsExpenses.Controls.Add(Me.btnExpensesList)
Me.fpnlClaimsExpenses.Controls.Add(Me.btnCostingList)
Me.fpnlClaimsExpenses.Controls.Add(Me.btnSecRouteAssignment)
Me.fpnlClaimsExpenses.Controls.Add(Me.btnExpenseAssignment)
Me.fpnlClaimsExpenses.Controls.Add(Me.btnExApprovalLevel)
Me.fpnlClaimsExpenses.Controls.Add(Me.btnExApprovers)
Me.fpnlClaimsExpenses.Controls.Add(Me.btnExApprMigration)
Me.fpnlClaimsExpenses.Controls.Add(Me.btnClaimSwapApprover)
Me.fpnlClaimsExpenses.Controls.Add(Me.btnClaims_Request)
Me.fpnlClaimsExpenses.Controls.Add(Me.btnExpenseApproval)
Me.fpnlClaimsExpenses.Controls.Add(Me.btnPostToPayroll)
Me.fpnlClaimsExpenses.Location = New System.Drawing.Point(1187, 144)
Me.fpnlClaimsExpenses.Name = "fpnlClaimsExpenses"
Me.fpnlClaimsExpenses.Size = New System.Drawing.Size(191, 41)
Me.fpnlClaimsExpenses.TabIndex = 26
'
'btnExpensesList
'
Me.btnExpensesList.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left)  _
            Or System.Windows.Forms.AnchorStyles.Right),System.Windows.Forms.AnchorStyles)
Me.btnExpensesList.BackColor = System.Drawing.Color.Transparent
Me.btnExpensesList.BackgroundImage = CType(resources.GetObject("btnExpensesList.BackgroundImage"),System.Drawing.Image)
Me.btnExpensesList.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
Me.btnExpensesList.BorderColor = System.Drawing.Color.Empty
Me.btnExpensesList.ButtonType = eZee.Common.eZeeLightButton.enButtonType.CheckButton
Me.btnExpensesList.FlatAppearance.BorderSize = 0
Me.btnExpensesList.FlatStyle = System.Windows.Forms.FlatStyle.Flat
Me.btnExpensesList.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
Me.btnExpensesList.ForeColor = System.Drawing.Color.Black
Me.btnExpensesList.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
Me.btnExpensesList.GradientForeColor = System.Drawing.Color.Black
Me.btnExpensesList.HoverGradientBackColor = System.Drawing.Color.Transparent
Me.btnExpensesList.HoverGradientForeColor = System.Drawing.Color.Black
Me.btnExpensesList.Image = Global.Aruti.Main.My.Resources.Resources.Expense_List_24_x_24
Me.btnExpensesList.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
Me.btnExpensesList.Location = New System.Drawing.Point(3, 3)
Me.btnExpensesList.MinimumSize = New System.Drawing.Size(150, 35)
Me.btnExpensesList.Name = "btnExpensesList"
Me.btnExpensesList.PressedGradientBackColor = System.Drawing.Color.Transparent
Me.btnExpensesList.PressedGradientForeColor = System.Drawing.Color.Black
Me.btnExpensesList.Size = New System.Drawing.Size(166, 35)
Me.btnExpensesList.TabIndex = 17
Me.btnExpensesList.Text = "Expenses List"
Me.btnExpensesList.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
Me.btnExpensesList.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText
Me.btnExpensesList.UseVisualStyleBackColor = false
'
'btnCostingList
'
Me.btnCostingList.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left)  _
            Or System.Windows.Forms.AnchorStyles.Right),System.Windows.Forms.AnchorStyles)
Me.btnCostingList.BackColor = System.Drawing.Color.Transparent
Me.btnCostingList.BackgroundImage = CType(resources.GetObject("btnCostingList.BackgroundImage"),System.Drawing.Image)
Me.btnCostingList.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
Me.btnCostingList.BorderColor = System.Drawing.Color.Empty
Me.btnCostingList.ButtonType = eZee.Common.eZeeLightButton.enButtonType.CheckButton
Me.btnCostingList.FlatAppearance.BorderSize = 0
Me.btnCostingList.FlatStyle = System.Windows.Forms.FlatStyle.Flat
Me.btnCostingList.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
Me.btnCostingList.ForeColor = System.Drawing.Color.Black
Me.btnCostingList.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
Me.btnCostingList.GradientForeColor = System.Drawing.Color.Black
Me.btnCostingList.HoverGradientBackColor = System.Drawing.Color.Transparent
Me.btnCostingList.HoverGradientForeColor = System.Drawing.Color.Black
Me.btnCostingList.Image = Global.Aruti.Main.My.Resources.Resources.Costing_List_24_x_24
Me.btnCostingList.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
Me.btnCostingList.Location = New System.Drawing.Point(3, 44)
Me.btnCostingList.MinimumSize = New System.Drawing.Size(150, 35)
Me.btnCostingList.Name = "btnCostingList"
Me.btnCostingList.PressedGradientBackColor = System.Drawing.Color.Transparent
Me.btnCostingList.PressedGradientForeColor = System.Drawing.Color.Black
Me.btnCostingList.Size = New System.Drawing.Size(166, 35)
Me.btnCostingList.TabIndex = 18
Me.btnCostingList.Text = "Costing List"
Me.btnCostingList.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
Me.btnCostingList.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText
Me.btnCostingList.UseVisualStyleBackColor = false
'
'btnSecRouteAssignment
'
Me.btnSecRouteAssignment.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left)  _
            Or System.Windows.Forms.AnchorStyles.Right),System.Windows.Forms.AnchorStyles)
Me.btnSecRouteAssignment.BackColor = System.Drawing.Color.Transparent
Me.btnSecRouteAssignment.BackgroundImage = CType(resources.GetObject("btnSecRouteAssignment.BackgroundImage"),System.Drawing.Image)
Me.btnSecRouteAssignment.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
Me.btnSecRouteAssignment.BorderColor = System.Drawing.Color.Empty
Me.btnSecRouteAssignment.ButtonType = eZee.Common.eZeeLightButton.enButtonType.CheckButton
Me.btnSecRouteAssignment.FlatAppearance.BorderSize = 0
Me.btnSecRouteAssignment.FlatStyle = System.Windows.Forms.FlatStyle.Flat
Me.btnSecRouteAssignment.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
Me.btnSecRouteAssignment.ForeColor = System.Drawing.Color.Black
Me.btnSecRouteAssignment.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
Me.btnSecRouteAssignment.GradientForeColor = System.Drawing.Color.Black
Me.btnSecRouteAssignment.HoverGradientBackColor = System.Drawing.Color.Transparent
Me.btnSecRouteAssignment.HoverGradientForeColor = System.Drawing.Color.Black
Me.btnSecRouteAssignment.Image = Global.Aruti.Main.My.Resources.Resources._5_Post_to_Payroll
Me.btnSecRouteAssignment.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
Me.btnSecRouteAssignment.Location = New System.Drawing.Point(3, 85)
Me.btnSecRouteAssignment.MinimumSize = New System.Drawing.Size(150, 35)
Me.btnSecRouteAssignment.Name = "btnSecRouteAssignment"
Me.btnSecRouteAssignment.PressedGradientBackColor = System.Drawing.Color.Transparent
Me.btnSecRouteAssignment.PressedGradientForeColor = System.Drawing.Color.Black
Me.btnSecRouteAssignment.Size = New System.Drawing.Size(166, 35)
Me.btnSecRouteAssignment.TabIndex = 29
Me.btnSecRouteAssignment.Text = "Sector/Route Assignment"
Me.btnSecRouteAssignment.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
Me.btnSecRouteAssignment.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText
Me.btnSecRouteAssignment.UseVisualStyleBackColor = false
'
'btnExpenseAssignment
'
Me.btnExpenseAssignment.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left)  _
            Or System.Windows.Forms.AnchorStyles.Right),System.Windows.Forms.AnchorStyles)
Me.btnExpenseAssignment.BackColor = System.Drawing.Color.Transparent
Me.btnExpenseAssignment.BackgroundImage = CType(resources.GetObject("btnExpenseAssignment.BackgroundImage"),System.Drawing.Image)
Me.btnExpenseAssignment.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
Me.btnExpenseAssignment.BorderColor = System.Drawing.Color.Empty
Me.btnExpenseAssignment.ButtonType = eZee.Common.eZeeLightButton.enButtonType.CheckButton
Me.btnExpenseAssignment.FlatAppearance.BorderSize = 0
Me.btnExpenseAssignment.FlatStyle = System.Windows.Forms.FlatStyle.Flat
Me.btnExpenseAssignment.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
Me.btnExpenseAssignment.ForeColor = System.Drawing.Color.Black
Me.btnExpenseAssignment.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
Me.btnExpenseAssignment.GradientForeColor = System.Drawing.Color.Black
Me.btnExpenseAssignment.HoverGradientBackColor = System.Drawing.Color.Transparent
Me.btnExpenseAssignment.HoverGradientForeColor = System.Drawing.Color.Black
Me.btnExpenseAssignment.Image = Global.Aruti.Main.My.Resources.Resources.Expense_Assignment_24_x_24
Me.btnExpenseAssignment.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
Me.btnExpenseAssignment.Location = New System.Drawing.Point(3, 126)
Me.btnExpenseAssignment.MinimumSize = New System.Drawing.Size(150, 35)
Me.btnExpenseAssignment.Name = "btnExpenseAssignment"
Me.btnExpenseAssignment.PressedGradientBackColor = System.Drawing.Color.Transparent
Me.btnExpenseAssignment.PressedGradientForeColor = System.Drawing.Color.Black
Me.btnExpenseAssignment.Size = New System.Drawing.Size(166, 35)
Me.btnExpenseAssignment.TabIndex = 19
Me.btnExpenseAssignment.Text = "Expense Assignment"
Me.btnExpenseAssignment.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
Me.btnExpenseAssignment.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText
Me.btnExpenseAssignment.UseVisualStyleBackColor = false
'
'btnExApprovalLevel
'
Me.btnExApprovalLevel.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left)  _
            Or System.Windows.Forms.AnchorStyles.Right),System.Windows.Forms.AnchorStyles)
Me.btnExApprovalLevel.BackColor = System.Drawing.Color.Transparent
Me.btnExApprovalLevel.BackgroundImage = CType(resources.GetObject("btnExApprovalLevel.BackgroundImage"),System.Drawing.Image)
Me.btnExApprovalLevel.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
Me.btnExApprovalLevel.BorderColor = System.Drawing.Color.Empty
Me.btnExApprovalLevel.ButtonType = eZee.Common.eZeeLightButton.enButtonType.CheckButton
Me.btnExApprovalLevel.FlatAppearance.BorderSize = 0
Me.btnExApprovalLevel.FlatStyle = System.Windows.Forms.FlatStyle.Flat
Me.btnExApprovalLevel.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
Me.btnExApprovalLevel.ForeColor = System.Drawing.Color.Black
Me.btnExApprovalLevel.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
Me.btnExApprovalLevel.GradientForeColor = System.Drawing.Color.Black
Me.btnExApprovalLevel.HoverGradientBackColor = System.Drawing.Color.Transparent
Me.btnExApprovalLevel.HoverGradientForeColor = System.Drawing.Color.Black
Me.btnExApprovalLevel.Image = Global.Aruti.Main.My.Resources.Resources.Approver_Level
Me.btnExApprovalLevel.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
Me.btnExApprovalLevel.Location = New System.Drawing.Point(3, 167)
Me.btnExApprovalLevel.MinimumSize = New System.Drawing.Size(150, 35)
Me.btnExApprovalLevel.Name = "btnExApprovalLevel"
Me.btnExApprovalLevel.PressedGradientBackColor = System.Drawing.Color.Transparent
Me.btnExApprovalLevel.PressedGradientForeColor = System.Drawing.Color.Black
Me.btnExApprovalLevel.Size = New System.Drawing.Size(166, 35)
Me.btnExApprovalLevel.TabIndex = 20
Me.btnExApprovalLevel.Text = "Expense Approval Level"
Me.btnExApprovalLevel.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
Me.btnExApprovalLevel.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText
Me.btnExApprovalLevel.UseVisualStyleBackColor = false
'
'btnExApprovers
'
Me.btnExApprovers.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left)  _
            Or System.Windows.Forms.AnchorStyles.Right),System.Windows.Forms.AnchorStyles)
Me.btnExApprovers.BackColor = System.Drawing.Color.Transparent
Me.btnExApprovers.BackgroundImage = CType(resources.GetObject("btnExApprovers.BackgroundImage"),System.Drawing.Image)
Me.btnExApprovers.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
Me.btnExApprovers.BorderColor = System.Drawing.Color.Empty
Me.btnExApprovers.ButtonType = eZee.Common.eZeeLightButton.enButtonType.CheckButton
Me.btnExApprovers.FlatAppearance.BorderSize = 0
Me.btnExApprovers.FlatStyle = System.Windows.Forms.FlatStyle.Flat
Me.btnExApprovers.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
Me.btnExApprovers.ForeColor = System.Drawing.Color.Black
Me.btnExApprovers.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
Me.btnExApprovers.GradientForeColor = System.Drawing.Color.Black
Me.btnExApprovers.HoverGradientBackColor = System.Drawing.Color.Transparent
Me.btnExApprovers.HoverGradientForeColor = System.Drawing.Color.Black
Me.btnExApprovers.Image = Global.Aruti.Main.My.Resources.Resources.Leave_Approver
Me.btnExApprovers.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
Me.btnExApprovers.Location = New System.Drawing.Point(3, 208)
Me.btnExApprovers.MinimumSize = New System.Drawing.Size(150, 35)
Me.btnExApprovers.Name = "btnExApprovers"
Me.btnExApprovers.PressedGradientBackColor = System.Drawing.Color.Transparent
Me.btnExApprovers.PressedGradientForeColor = System.Drawing.Color.Black
Me.btnExApprovers.Size = New System.Drawing.Size(166, 35)
Me.btnExApprovers.TabIndex = 21
Me.btnExApprovers.Text = "Expense Approvers List"
Me.btnExApprovers.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
Me.btnExApprovers.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText
Me.btnExApprovers.UseVisualStyleBackColor = false
'
'btnExApprMigration
'
Me.btnExApprMigration.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left)  _
            Or System.Windows.Forms.AnchorStyles.Right),System.Windows.Forms.AnchorStyles)
Me.btnExApprMigration.BackColor = System.Drawing.Color.Transparent
Me.btnExApprMigration.BackgroundImage = CType(resources.GetObject("btnExApprMigration.BackgroundImage"),System.Drawing.Image)
Me.btnExApprMigration.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
Me.btnExApprMigration.BorderColor = System.Drawing.Color.Empty
Me.btnExApprMigration.ButtonType = eZee.Common.eZeeLightButton.enButtonType.CheckButton
Me.btnExApprMigration.FlatAppearance.BorderSize = 0
Me.btnExApprMigration.FlatStyle = System.Windows.Forms.FlatStyle.Flat
Me.btnExApprMigration.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
Me.btnExApprMigration.ForeColor = System.Drawing.Color.Black
Me.btnExApprMigration.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
Me.btnExApprMigration.GradientForeColor = System.Drawing.Color.Black
Me.btnExApprMigration.HoverGradientBackColor = System.Drawing.Color.Transparent
Me.btnExApprMigration.HoverGradientForeColor = System.Drawing.Color.Black
Me.btnExApprMigration.Image = Global.Aruti.Main.My.Resources.Resources.Transfer_Employee
Me.btnExApprMigration.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
Me.btnExApprMigration.Location = New System.Drawing.Point(3, 249)
Me.btnExApprMigration.MinimumSize = New System.Drawing.Size(150, 35)
Me.btnExApprMigration.Name = "btnExApprMigration"
Me.btnExApprMigration.PressedGradientBackColor = System.Drawing.Color.Transparent
Me.btnExApprMigration.PressedGradientForeColor = System.Drawing.Color.Black
Me.btnExApprMigration.Size = New System.Drawing.Size(166, 35)
Me.btnExApprMigration.TabIndex = 22
Me.btnExApprMigration.Text = "Approver Migration"
Me.btnExApprMigration.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
Me.btnExApprMigration.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText
Me.btnExApprMigration.UseVisualStyleBackColor = false
'
'btnClaimSwapApprover
'
Me.btnClaimSwapApprover.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left)  _
            Or System.Windows.Forms.AnchorStyles.Right),System.Windows.Forms.AnchorStyles)
Me.btnClaimSwapApprover.BackColor = System.Drawing.Color.Transparent
Me.btnClaimSwapApprover.BackgroundImage = CType(resources.GetObject("btnClaimSwapApprover.BackgroundImage"),System.Drawing.Image)
Me.btnClaimSwapApprover.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
Me.btnClaimSwapApprover.BorderColor = System.Drawing.Color.Empty
Me.btnClaimSwapApprover.ButtonType = eZee.Common.eZeeLightButton.enButtonType.CheckButton
Me.btnClaimSwapApprover.FlatAppearance.BorderSize = 0
Me.btnClaimSwapApprover.FlatStyle = System.Windows.Forms.FlatStyle.Flat
Me.btnClaimSwapApprover.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
Me.btnClaimSwapApprover.ForeColor = System.Drawing.Color.Black
Me.btnClaimSwapApprover.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
Me.btnClaimSwapApprover.GradientForeColor = System.Drawing.Color.Black
Me.btnClaimSwapApprover.HoverGradientBackColor = System.Drawing.Color.Transparent
Me.btnClaimSwapApprover.HoverGradientForeColor = System.Drawing.Color.Black
Me.btnClaimSwapApprover.Image = Global.Aruti.Main.My.Resources.Resources.Approver_Swapping
Me.btnClaimSwapApprover.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
Me.btnClaimSwapApprover.Location = New System.Drawing.Point(3, 290)
Me.btnClaimSwapApprover.MinimumSize = New System.Drawing.Size(150, 35)
Me.btnClaimSwapApprover.Name = "btnClaimSwapApprover"
Me.btnClaimSwapApprover.PressedGradientBackColor = System.Drawing.Color.Transparent
Me.btnClaimSwapApprover.PressedGradientForeColor = System.Drawing.Color.Black
Me.btnClaimSwapApprover.Size = New System.Drawing.Size(166, 35)
Me.btnClaimSwapApprover.TabIndex = 26
Me.btnClaimSwapApprover.Text = "Swap Approver"
Me.btnClaimSwapApprover.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
Me.btnClaimSwapApprover.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText
Me.btnClaimSwapApprover.UseVisualStyleBackColor = false
'
'btnClaims_Request
'
Me.btnClaims_Request.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left)  _
            Or System.Windows.Forms.AnchorStyles.Right),System.Windows.Forms.AnchorStyles)
Me.btnClaims_Request.BackColor = System.Drawing.Color.Transparent
Me.btnClaims_Request.BackgroundImage = CType(resources.GetObject("btnClaims_Request.BackgroundImage"),System.Drawing.Image)
Me.btnClaims_Request.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
Me.btnClaims_Request.BorderColor = System.Drawing.Color.Empty
Me.btnClaims_Request.ButtonType = eZee.Common.eZeeLightButton.enButtonType.CheckButton
Me.btnClaims_Request.FlatAppearance.BorderSize = 0
Me.btnClaims_Request.FlatStyle = System.Windows.Forms.FlatStyle.Flat
Me.btnClaims_Request.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
Me.btnClaims_Request.ForeColor = System.Drawing.Color.Black
Me.btnClaims_Request.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
Me.btnClaims_Request.GradientForeColor = System.Drawing.Color.Black
Me.btnClaims_Request.HoverGradientBackColor = System.Drawing.Color.Transparent
Me.btnClaims_Request.HoverGradientForeColor = System.Drawing.Color.Black
Me.btnClaims_Request.Image = Global.Aruti.Main.My.Resources.Resources.Leaveform
Me.btnClaims_Request.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
Me.btnClaims_Request.Location = New System.Drawing.Point(3, 331)
Me.btnClaims_Request.MinimumSize = New System.Drawing.Size(150, 35)
Me.btnClaims_Request.Name = "btnClaims_Request"
Me.btnClaims_Request.PressedGradientBackColor = System.Drawing.Color.Transparent
Me.btnClaims_Request.PressedGradientForeColor = System.Drawing.Color.Black
Me.btnClaims_Request.Size = New System.Drawing.Size(166, 35)
Me.btnClaims_Request.TabIndex = 23
Me.btnClaims_Request.Text = "Claims && Request"
Me.btnClaims_Request.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
Me.btnClaims_Request.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText
Me.btnClaims_Request.UseVisualStyleBackColor = false
'
'btnExpenseApproval
'
Me.btnExpenseApproval.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left)  _
            Or System.Windows.Forms.AnchorStyles.Right),System.Windows.Forms.AnchorStyles)
Me.btnExpenseApproval.BackColor = System.Drawing.Color.Transparent
Me.btnExpenseApproval.BackgroundImage = CType(resources.GetObject("btnExpenseApproval.BackgroundImage"),System.Drawing.Image)
Me.btnExpenseApproval.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
Me.btnExpenseApproval.BorderColor = System.Drawing.Color.Empty
Me.btnExpenseApproval.ButtonType = eZee.Common.eZeeLightButton.enButtonType.CheckButton
Me.btnExpenseApproval.FlatAppearance.BorderSize = 0
Me.btnExpenseApproval.FlatStyle = System.Windows.Forms.FlatStyle.Flat
Me.btnExpenseApproval.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
Me.btnExpenseApproval.ForeColor = System.Drawing.Color.Black
Me.btnExpenseApproval.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
Me.btnExpenseApproval.GradientForeColor = System.Drawing.Color.Black
Me.btnExpenseApproval.HoverGradientBackColor = System.Drawing.Color.Transparent
Me.btnExpenseApproval.HoverGradientForeColor = System.Drawing.Color.Black
Me.btnExpenseApproval.Image = Global.Aruti.Main.My.Resources.Resources.Leaveprocess
Me.btnExpenseApproval.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
Me.btnExpenseApproval.Location = New System.Drawing.Point(3, 372)
Me.btnExpenseApproval.MinimumSize = New System.Drawing.Size(150, 35)
Me.btnExpenseApproval.Name = "btnExpenseApproval"
Me.btnExpenseApproval.PressedGradientBackColor = System.Drawing.Color.Transparent
Me.btnExpenseApproval.PressedGradientForeColor = System.Drawing.Color.Black
Me.btnExpenseApproval.Size = New System.Drawing.Size(166, 35)
Me.btnExpenseApproval.TabIndex = 24
Me.btnExpenseApproval.Text = "Expense Approval"
Me.btnExpenseApproval.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
Me.btnExpenseApproval.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText
Me.btnExpenseApproval.UseVisualStyleBackColor = false
'
'btnPostToPayroll
'
Me.btnPostToPayroll.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left)  _
            Or System.Windows.Forms.AnchorStyles.Right),System.Windows.Forms.AnchorStyles)
Me.btnPostToPayroll.BackColor = System.Drawing.Color.Transparent
Me.btnPostToPayroll.BackgroundImage = CType(resources.GetObject("btnPostToPayroll.BackgroundImage"),System.Drawing.Image)
Me.btnPostToPayroll.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
Me.btnPostToPayroll.BorderColor = System.Drawing.Color.Empty
Me.btnPostToPayroll.ButtonType = eZee.Common.eZeeLightButton.enButtonType.CheckButton
Me.btnPostToPayroll.FlatAppearance.BorderSize = 0
Me.btnPostToPayroll.FlatStyle = System.Windows.Forms.FlatStyle.Flat
Me.btnPostToPayroll.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
Me.btnPostToPayroll.ForeColor = System.Drawing.Color.Black
Me.btnPostToPayroll.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
Me.btnPostToPayroll.GradientForeColor = System.Drawing.Color.Black
Me.btnPostToPayroll.HoverGradientBackColor = System.Drawing.Color.Transparent
Me.btnPostToPayroll.HoverGradientForeColor = System.Drawing.Color.Black
Me.btnPostToPayroll.Image = Global.Aruti.Main.My.Resources.Resources._5_Post_to_Payroll
Me.btnPostToPayroll.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
Me.btnPostToPayroll.Location = New System.Drawing.Point(3, 413)
Me.btnPostToPayroll.MinimumSize = New System.Drawing.Size(150, 35)
Me.btnPostToPayroll.Name = "btnPostToPayroll"
Me.btnPostToPayroll.PressedGradientBackColor = System.Drawing.Color.Transparent
Me.btnPostToPayroll.PressedGradientForeColor = System.Drawing.Color.Black
Me.btnPostToPayroll.Size = New System.Drawing.Size(166, 35)
Me.btnPostToPayroll.TabIndex = 25
Me.btnPostToPayroll.Text = "Post to Payroll"
Me.btnPostToPayroll.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
Me.btnPostToPayroll.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText
Me.btnPostToPayroll.UseVisualStyleBackColor = false
'
'fpnlPerformanceEval
'
Me.fpnlPerformanceEval.AutoScroll = true
Me.fpnlPerformanceEval.Controls.Add(Me.btnSelfPEvaluation)
Me.fpnlPerformanceEval.Controls.Add(Me.btnAssessorPEvaluation)
Me.fpnlPerformanceEval.Controls.Add(Me.btnReviewerPEvaluation)
Me.fpnlPerformanceEval.Controls.Add(Me.btnGlobalVoidAssessment)
Me.fpnlPerformanceEval.Controls.Add(Me.btnComputeProcess)
Me.fpnlPerformanceEval.Controls.Add(Me.btnExportPrintEmplForm)
Me.fpnlPerformanceEval.Location = New System.Drawing.Point(1384, 3)
Me.fpnlPerformanceEval.Name = "fpnlPerformanceEval"
Me.fpnlPerformanceEval.Size = New System.Drawing.Size(189, 42)
Me.fpnlPerformanceEval.TabIndex = 26
'
'btnSelfPEvaluation
'
Me.btnSelfPEvaluation.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left)  _
            Or System.Windows.Forms.AnchorStyles.Right),System.Windows.Forms.AnchorStyles)
Me.btnSelfPEvaluation.BackColor = System.Drawing.Color.Transparent
Me.btnSelfPEvaluation.BackgroundImage = CType(resources.GetObject("btnSelfPEvaluation.BackgroundImage"),System.Drawing.Image)
Me.btnSelfPEvaluation.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
Me.btnSelfPEvaluation.BorderColor = System.Drawing.Color.Empty
Me.btnSelfPEvaluation.ButtonType = eZee.Common.eZeeLightButton.enButtonType.CheckButton
Me.btnSelfPEvaluation.FlatAppearance.BorderSize = 0
Me.btnSelfPEvaluation.FlatStyle = System.Windows.Forms.FlatStyle.Flat
Me.btnSelfPEvaluation.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
Me.btnSelfPEvaluation.ForeColor = System.Drawing.Color.Black
Me.btnSelfPEvaluation.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
Me.btnSelfPEvaluation.GradientForeColor = System.Drawing.Color.Black
Me.btnSelfPEvaluation.HoverGradientBackColor = System.Drawing.Color.Transparent
Me.btnSelfPEvaluation.HoverGradientForeColor = System.Drawing.Color.Black
Me.btnSelfPEvaluation.Image = Global.Aruti.Main.My.Resources.Resources.EmployeeAssessment
Me.btnSelfPEvaluation.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
Me.btnSelfPEvaluation.Location = New System.Drawing.Point(3, 3)
Me.btnSelfPEvaluation.MinimumSize = New System.Drawing.Size(150, 35)
Me.btnSelfPEvaluation.Name = "btnSelfPEvaluation"
Me.btnSelfPEvaluation.PressedGradientBackColor = System.Drawing.Color.Transparent
Me.btnSelfPEvaluation.PressedGradientForeColor = System.Drawing.Color.Black
Me.btnSelfPEvaluation.Size = New System.Drawing.Size(166, 35)
Me.btnSelfPEvaluation.TabIndex = 17
Me.btnSelfPEvaluation.Text = "Self-Assessment"
Me.btnSelfPEvaluation.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
Me.btnSelfPEvaluation.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText
Me.btnSelfPEvaluation.UseVisualStyleBackColor = false
'
'btnAssessorPEvaluation
'
Me.btnAssessorPEvaluation.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left)  _
            Or System.Windows.Forms.AnchorStyles.Right),System.Windows.Forms.AnchorStyles)
Me.btnAssessorPEvaluation.BackColor = System.Drawing.Color.Transparent
Me.btnAssessorPEvaluation.BackgroundImage = CType(resources.GetObject("btnAssessorPEvaluation.BackgroundImage"),System.Drawing.Image)
Me.btnAssessorPEvaluation.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
Me.btnAssessorPEvaluation.BorderColor = System.Drawing.Color.Empty
Me.btnAssessorPEvaluation.ButtonType = eZee.Common.eZeeLightButton.enButtonType.CheckButton
Me.btnAssessorPEvaluation.FlatAppearance.BorderSize = 0
Me.btnAssessorPEvaluation.FlatStyle = System.Windows.Forms.FlatStyle.Flat
Me.btnAssessorPEvaluation.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
Me.btnAssessorPEvaluation.ForeColor = System.Drawing.Color.Black
Me.btnAssessorPEvaluation.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
Me.btnAssessorPEvaluation.GradientForeColor = System.Drawing.Color.Black
Me.btnAssessorPEvaluation.HoverGradientBackColor = System.Drawing.Color.Transparent
Me.btnAssessorPEvaluation.HoverGradientForeColor = System.Drawing.Color.Black
Me.btnAssessorPEvaluation.Image = Global.Aruti.Main.My.Resources.Resources.AssessorAssessment
Me.btnAssessorPEvaluation.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
Me.btnAssessorPEvaluation.Location = New System.Drawing.Point(3, 44)
Me.btnAssessorPEvaluation.MinimumSize = New System.Drawing.Size(150, 35)
Me.btnAssessorPEvaluation.Name = "btnAssessorPEvaluation"
Me.btnAssessorPEvaluation.PressedGradientBackColor = System.Drawing.Color.Transparent
Me.btnAssessorPEvaluation.PressedGradientForeColor = System.Drawing.Color.Black
Me.btnAssessorPEvaluation.Size = New System.Drawing.Size(166, 35)
Me.btnAssessorPEvaluation.TabIndex = 18
Me.btnAssessorPEvaluation.Text = "Assess Employee"
Me.btnAssessorPEvaluation.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
Me.btnAssessorPEvaluation.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText
Me.btnAssessorPEvaluation.UseVisualStyleBackColor = false
'
'btnReviewerPEvaluation
'
Me.btnReviewerPEvaluation.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left)  _
            Or System.Windows.Forms.AnchorStyles.Right),System.Windows.Forms.AnchorStyles)
Me.btnReviewerPEvaluation.BackColor = System.Drawing.Color.Transparent
Me.btnReviewerPEvaluation.BackgroundImage = CType(resources.GetObject("btnReviewerPEvaluation.BackgroundImage"),System.Drawing.Image)
Me.btnReviewerPEvaluation.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
Me.btnReviewerPEvaluation.BorderColor = System.Drawing.Color.Empty
Me.btnReviewerPEvaluation.ButtonType = eZee.Common.eZeeLightButton.enButtonType.CheckButton
Me.btnReviewerPEvaluation.FlatAppearance.BorderSize = 0
Me.btnReviewerPEvaluation.FlatStyle = System.Windows.Forms.FlatStyle.Flat
Me.btnReviewerPEvaluation.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
Me.btnReviewerPEvaluation.ForeColor = System.Drawing.Color.Black
Me.btnReviewerPEvaluation.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
Me.btnReviewerPEvaluation.GradientForeColor = System.Drawing.Color.Black
Me.btnReviewerPEvaluation.HoverGradientBackColor = System.Drawing.Color.Transparent
Me.btnReviewerPEvaluation.HoverGradientForeColor = System.Drawing.Color.Black
Me.btnReviewerPEvaluation.Image = Global.Aruti.Main.My.Resources.Resources.Reviewer_assess
Me.btnReviewerPEvaluation.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
Me.btnReviewerPEvaluation.Location = New System.Drawing.Point(3, 85)
Me.btnReviewerPEvaluation.MinimumSize = New System.Drawing.Size(150, 35)
Me.btnReviewerPEvaluation.Name = "btnReviewerPEvaluation"
Me.btnReviewerPEvaluation.PressedGradientBackColor = System.Drawing.Color.Transparent
Me.btnReviewerPEvaluation.PressedGradientForeColor = System.Drawing.Color.Black
Me.btnReviewerPEvaluation.Size = New System.Drawing.Size(166, 35)
Me.btnReviewerPEvaluation.TabIndex = 19
Me.btnReviewerPEvaluation.Text = "Review Employee Assessment"
Me.btnReviewerPEvaluation.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
Me.btnReviewerPEvaluation.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText
Me.btnReviewerPEvaluation.UseVisualStyleBackColor = false
'
'btnGlobalVoidAssessment
'
Me.btnGlobalVoidAssessment.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left)  _
            Or System.Windows.Forms.AnchorStyles.Right),System.Windows.Forms.AnchorStyles)
Me.btnGlobalVoidAssessment.BackColor = System.Drawing.Color.Transparent
Me.btnGlobalVoidAssessment.BackgroundImage = CType(resources.GetObject("btnGlobalVoidAssessment.BackgroundImage"),System.Drawing.Image)
Me.btnGlobalVoidAssessment.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
Me.btnGlobalVoidAssessment.BorderColor = System.Drawing.Color.Empty
Me.btnGlobalVoidAssessment.ButtonType = eZee.Common.eZeeLightButton.enButtonType.CheckButton
Me.btnGlobalVoidAssessment.FlatAppearance.BorderSize = 0
Me.btnGlobalVoidAssessment.FlatStyle = System.Windows.Forms.FlatStyle.Flat
Me.btnGlobalVoidAssessment.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
Me.btnGlobalVoidAssessment.ForeColor = System.Drawing.Color.Black
Me.btnGlobalVoidAssessment.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
Me.btnGlobalVoidAssessment.GradientForeColor = System.Drawing.Color.Black
Me.btnGlobalVoidAssessment.HoverGradientBackColor = System.Drawing.Color.Transparent
Me.btnGlobalVoidAssessment.HoverGradientForeColor = System.Drawing.Color.Black
Me.btnGlobalVoidAssessment.Image = Global.Aruti.Main.My.Resources.Resources.Delete_batch_wise
Me.btnGlobalVoidAssessment.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
Me.btnGlobalVoidAssessment.Location = New System.Drawing.Point(3, 126)
Me.btnGlobalVoidAssessment.MinimumSize = New System.Drawing.Size(150, 35)
Me.btnGlobalVoidAssessment.Name = "btnGlobalVoidAssessment"
Me.btnGlobalVoidAssessment.PressedGradientBackColor = System.Drawing.Color.Transparent
Me.btnGlobalVoidAssessment.PressedGradientForeColor = System.Drawing.Color.Black
Me.btnGlobalVoidAssessment.Size = New System.Drawing.Size(166, 35)
Me.btnGlobalVoidAssessment.TabIndex = 20
Me.btnGlobalVoidAssessment.Text = "Global Void Assessment"
Me.btnGlobalVoidAssessment.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
Me.btnGlobalVoidAssessment.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText
Me.btnGlobalVoidAssessment.UseVisualStyleBackColor = false
'
'btnComputeProcess
'
Me.btnComputeProcess.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left)  _
            Or System.Windows.Forms.AnchorStyles.Right),System.Windows.Forms.AnchorStyles)
Me.btnComputeProcess.BackColor = System.Drawing.Color.Transparent
Me.btnComputeProcess.BackgroundImage = CType(resources.GetObject("btnComputeProcess.BackgroundImage"),System.Drawing.Image)
Me.btnComputeProcess.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
Me.btnComputeProcess.BorderColor = System.Drawing.Color.Empty
Me.btnComputeProcess.ButtonType = eZee.Common.eZeeLightButton.enButtonType.CheckButton
Me.btnComputeProcess.FlatAppearance.BorderSize = 0
Me.btnComputeProcess.FlatStyle = System.Windows.Forms.FlatStyle.Flat
Me.btnComputeProcess.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
Me.btnComputeProcess.ForeColor = System.Drawing.Color.Black
Me.btnComputeProcess.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
Me.btnComputeProcess.GradientForeColor = System.Drawing.Color.Black
Me.btnComputeProcess.HoverGradientBackColor = System.Drawing.Color.Transparent
Me.btnComputeProcess.HoverGradientForeColor = System.Drawing.Color.Black
Me.btnComputeProcess.Image = Global.Aruti.Main.My.Resources.Resources.General_settings
Me.btnComputeProcess.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
Me.btnComputeProcess.Location = New System.Drawing.Point(3, 167)
Me.btnComputeProcess.MinimumSize = New System.Drawing.Size(150, 35)
Me.btnComputeProcess.Name = "btnComputeProcess"
Me.btnComputeProcess.PressedGradientBackColor = System.Drawing.Color.Transparent
Me.btnComputeProcess.PressedGradientForeColor = System.Drawing.Color.Black
Me.btnComputeProcess.Size = New System.Drawing.Size(166, 35)
Me.btnComputeProcess.TabIndex = 21
Me.btnComputeProcess.Text = "Compute Score Process"
Me.btnComputeProcess.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
Me.btnComputeProcess.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText
Me.btnComputeProcess.UseVisualStyleBackColor = false
'
'btnExportPrintEmplForm
'
Me.btnExportPrintEmplForm.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left)  _
            Or System.Windows.Forms.AnchorStyles.Right),System.Windows.Forms.AnchorStyles)
Me.btnExportPrintEmplForm.BackColor = System.Drawing.Color.Transparent
Me.btnExportPrintEmplForm.BackgroundImage = CType(resources.GetObject("btnExportPrintEmplForm.BackgroundImage"),System.Drawing.Image)
Me.btnExportPrintEmplForm.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
Me.btnExportPrintEmplForm.BorderColor = System.Drawing.Color.Empty
Me.btnExportPrintEmplForm.ButtonType = eZee.Common.eZeeLightButton.enButtonType.CheckButton
Me.btnExportPrintEmplForm.FlatAppearance.BorderSize = 0
Me.btnExportPrintEmplForm.FlatStyle = System.Windows.Forms.FlatStyle.Flat
Me.btnExportPrintEmplForm.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
Me.btnExportPrintEmplForm.ForeColor = System.Drawing.Color.Black
Me.btnExportPrintEmplForm.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
Me.btnExportPrintEmplForm.GradientForeColor = System.Drawing.Color.Black
Me.btnExportPrintEmplForm.HoverGradientBackColor = System.Drawing.Color.Transparent
Me.btnExportPrintEmplForm.HoverGradientForeColor = System.Drawing.Color.Black
Me.btnExportPrintEmplForm.Image = Global.Aruti.Main.My.Resources.Resources.import_export
Me.btnExportPrintEmplForm.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
Me.btnExportPrintEmplForm.Location = New System.Drawing.Point(3, 208)
Me.btnExportPrintEmplForm.MinimumSize = New System.Drawing.Size(150, 35)
Me.btnExportPrintEmplForm.Name = "btnExportPrintEmplForm"
Me.btnExportPrintEmplForm.PressedGradientBackColor = System.Drawing.Color.Transparent
Me.btnExportPrintEmplForm.PressedGradientForeColor = System.Drawing.Color.Black
Me.btnExportPrintEmplForm.Size = New System.Drawing.Size(166, 35)
Me.btnExportPrintEmplForm.TabIndex = 22
Me.btnExportPrintEmplForm.Text = "Employee Score Card"
Me.btnExportPrintEmplForm.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
Me.btnExportPrintEmplForm.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText
Me.btnExportPrintEmplForm.UseVisualStyleBackColor = false
'
'fpnlBudgetInfo
'
Me.fpnlBudgetInfo.AutoScroll = true
Me.fpnlBudgetInfo.Controls.Add(Me.btnFundSources)
Me.fpnlBudgetInfo.Controls.Add(Me.btnFundProjectCode)
Me.fpnlBudgetInfo.Controls.Add(Me.btnProjectCodeAdjustments)
Me.fpnlBudgetInfo.Controls.Add(Me.btnFundActivity)
Me.fpnlBudgetInfo.Controls.Add(Me.btnFundActivityAdjustment)
Me.fpnlBudgetInfo.Controls.Add(Me.btnBgtApproverLevel)
Me.fpnlBudgetInfo.Controls.Add(Me.btnBgtApproverMapping)
Me.fpnlBudgetInfo.Controls.Add(Me.btnBudgetFormula)
Me.fpnlBudgetInfo.Controls.Add(Me.btnBudget)
Me.fpnlBudgetInfo.Controls.Add(Me.btnBudgetCodes)
Me.fpnlBudgetInfo.Location = New System.Drawing.Point(1384, 51)
Me.fpnlBudgetInfo.Name = "fpnlBudgetInfo"
Me.fpnlBudgetInfo.Size = New System.Drawing.Size(191, 39)
Me.fpnlBudgetInfo.TabIndex = 27
Me.fpnlBudgetInfo.Visible = false
'
'btnFundSources
'
Me.btnFundSources.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left)  _
            Or System.Windows.Forms.AnchorStyles.Right),System.Windows.Forms.AnchorStyles)
Me.btnFundSources.BackColor = System.Drawing.Color.Transparent
Me.btnFundSources.BackgroundImage = CType(resources.GetObject("btnFundSources.BackgroundImage"),System.Drawing.Image)
Me.btnFundSources.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
Me.btnFundSources.BorderColor = System.Drawing.Color.Empty
Me.btnFundSources.ButtonType = eZee.Common.eZeeLightButton.enButtonType.CheckButton
Me.btnFundSources.FlatAppearance.BorderSize = 0
Me.btnFundSources.FlatStyle = System.Windows.Forms.FlatStyle.Flat
Me.btnFundSources.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
Me.btnFundSources.ForeColor = System.Drawing.Color.Black
Me.btnFundSources.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
Me.btnFundSources.GradientForeColor = System.Drawing.Color.Black
Me.btnFundSources.HoverGradientBackColor = System.Drawing.Color.Transparent
Me.btnFundSources.HoverGradientForeColor = System.Drawing.Color.Black
Me.btnFundSources.Image = Global.Aruti.Main.My.Resources.Resources.Loan_Scheme
Me.btnFundSources.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
Me.btnFundSources.Location = New System.Drawing.Point(3, 3)
Me.btnFundSources.MinimumSize = New System.Drawing.Size(150, 35)
Me.btnFundSources.Name = "btnFundSources"
Me.btnFundSources.PressedGradientBackColor = System.Drawing.Color.Transparent
Me.btnFundSources.PressedGradientForeColor = System.Drawing.Color.Black
Me.btnFundSources.Size = New System.Drawing.Size(166, 35)
Me.btnFundSources.TabIndex = 6
Me.btnFundSources.Text = "Fund Sources"
Me.btnFundSources.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
Me.btnFundSources.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText
Me.btnFundSources.UseVisualStyleBackColor = false
'
'btnFundProjectCode
'
Me.btnFundProjectCode.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left)  _
            Or System.Windows.Forms.AnchorStyles.Right),System.Windows.Forms.AnchorStyles)
Me.btnFundProjectCode.BackColor = System.Drawing.Color.Transparent
Me.btnFundProjectCode.BackgroundImage = CType(resources.GetObject("btnFundProjectCode.BackgroundImage"),System.Drawing.Image)
Me.btnFundProjectCode.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
Me.btnFundProjectCode.BorderColor = System.Drawing.Color.Empty
Me.btnFundProjectCode.ButtonType = eZee.Common.eZeeLightButton.enButtonType.CheckButton
Me.btnFundProjectCode.FlatAppearance.BorderSize = 0
Me.btnFundProjectCode.FlatStyle = System.Windows.Forms.FlatStyle.Flat
Me.btnFundProjectCode.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
Me.btnFundProjectCode.ForeColor = System.Drawing.Color.Black
Me.btnFundProjectCode.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
Me.btnFundProjectCode.GradientForeColor = System.Drawing.Color.Black
Me.btnFundProjectCode.HoverGradientBackColor = System.Drawing.Color.Transparent
Me.btnFundProjectCode.HoverGradientForeColor = System.Drawing.Color.Black
Me.btnFundProjectCode.Image = Global.Aruti.Main.My.Resources.Resources.Loan_Scheme
Me.btnFundProjectCode.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
Me.btnFundProjectCode.Location = New System.Drawing.Point(3, 44)
Me.btnFundProjectCode.MinimumSize = New System.Drawing.Size(150, 35)
Me.btnFundProjectCode.Name = "btnFundProjectCode"
Me.btnFundProjectCode.PressedGradientBackColor = System.Drawing.Color.Transparent
Me.btnFundProjectCode.PressedGradientForeColor = System.Drawing.Color.Black
Me.btnFundProjectCode.Size = New System.Drawing.Size(166, 35)
Me.btnFundProjectCode.TabIndex = 41
Me.btnFundProjectCode.Text = "Fund Project Code"
Me.btnFundProjectCode.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
Me.btnFundProjectCode.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText
Me.btnFundProjectCode.UseVisualStyleBackColor = false
'
'btnProjectCodeAdjustments
'
Me.btnProjectCodeAdjustments.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left)  _
            Or System.Windows.Forms.AnchorStyles.Right),System.Windows.Forms.AnchorStyles)
Me.btnProjectCodeAdjustments.BackColor = System.Drawing.Color.Transparent
Me.btnProjectCodeAdjustments.BackgroundImage = CType(resources.GetObject("btnProjectCodeAdjustments.BackgroundImage"),System.Drawing.Image)
Me.btnProjectCodeAdjustments.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
Me.btnProjectCodeAdjustments.BorderColor = System.Drawing.Color.Empty
Me.btnProjectCodeAdjustments.ButtonType = eZee.Common.eZeeLightButton.enButtonType.CheckButton
Me.btnProjectCodeAdjustments.FlatAppearance.BorderSize = 0
Me.btnProjectCodeAdjustments.FlatStyle = System.Windows.Forms.FlatStyle.Flat
Me.btnProjectCodeAdjustments.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
Me.btnProjectCodeAdjustments.ForeColor = System.Drawing.Color.Black
Me.btnProjectCodeAdjustments.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
Me.btnProjectCodeAdjustments.GradientForeColor = System.Drawing.Color.Black
Me.btnProjectCodeAdjustments.HoverGradientBackColor = System.Drawing.Color.Transparent
Me.btnProjectCodeAdjustments.HoverGradientForeColor = System.Drawing.Color.Black
Me.btnProjectCodeAdjustments.Image = Global.Aruti.Main.My.Resources.Resources.Approver_Level
Me.btnProjectCodeAdjustments.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
Me.btnProjectCodeAdjustments.Location = New System.Drawing.Point(3, 85)
Me.btnProjectCodeAdjustments.MinimumSize = New System.Drawing.Size(150, 35)
Me.btnProjectCodeAdjustments.Name = "btnProjectCodeAdjustments"
Me.btnProjectCodeAdjustments.PressedGradientBackColor = System.Drawing.Color.Transparent
Me.btnProjectCodeAdjustments.PressedGradientForeColor = System.Drawing.Color.Black
Me.btnProjectCodeAdjustments.Size = New System.Drawing.Size(166, 35)
Me.btnProjectCodeAdjustments.TabIndex = 11
Me.btnProjectCodeAdjustments.Text = "Project Code Adjustment"
Me.btnProjectCodeAdjustments.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
Me.btnProjectCodeAdjustments.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText
Me.btnProjectCodeAdjustments.UseVisualStyleBackColor = false
'
'btnFundActivity
'
Me.btnFundActivity.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left)  _
            Or System.Windows.Forms.AnchorStyles.Right),System.Windows.Forms.AnchorStyles)
Me.btnFundActivity.BackColor = System.Drawing.Color.Transparent
Me.btnFundActivity.BackgroundImage = CType(resources.GetObject("btnFundActivity.BackgroundImage"),System.Drawing.Image)
Me.btnFundActivity.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
Me.btnFundActivity.BorderColor = System.Drawing.Color.Empty
Me.btnFundActivity.ButtonType = eZee.Common.eZeeLightButton.enButtonType.CheckButton
Me.btnFundActivity.FlatAppearance.BorderSize = 0
Me.btnFundActivity.FlatStyle = System.Windows.Forms.FlatStyle.Flat
Me.btnFundActivity.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
Me.btnFundActivity.ForeColor = System.Drawing.Color.Black
Me.btnFundActivity.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
Me.btnFundActivity.GradientForeColor = System.Drawing.Color.Black
Me.btnFundActivity.HoverGradientBackColor = System.Drawing.Color.Transparent
Me.btnFundActivity.HoverGradientForeColor = System.Drawing.Color.Black
Me.btnFundActivity.Image = Global.Aruti.Main.My.Resources.Resources.Expense_List_24_x_24
Me.btnFundActivity.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
Me.btnFundActivity.Location = New System.Drawing.Point(3, 126)
Me.btnFundActivity.MinimumSize = New System.Drawing.Size(150, 35)
Me.btnFundActivity.Name = "btnFundActivity"
Me.btnFundActivity.PressedGradientBackColor = System.Drawing.Color.Transparent
Me.btnFundActivity.PressedGradientForeColor = System.Drawing.Color.Black
Me.btnFundActivity.Size = New System.Drawing.Size(166, 35)
Me.btnFundActivity.TabIndex = 38
Me.btnFundActivity.Text = "Fund Activity"
Me.btnFundActivity.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
Me.btnFundActivity.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText
Me.btnFundActivity.UseVisualStyleBackColor = false
'
'btnFundActivityAdjustment
'
Me.btnFundActivityAdjustment.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left)  _
            Or System.Windows.Forms.AnchorStyles.Right),System.Windows.Forms.AnchorStyles)
Me.btnFundActivityAdjustment.BackColor = System.Drawing.Color.Transparent
Me.btnFundActivityAdjustment.BackgroundImage = CType(resources.GetObject("btnFundActivityAdjustment.BackgroundImage"),System.Drawing.Image)
Me.btnFundActivityAdjustment.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
Me.btnFundActivityAdjustment.BorderColor = System.Drawing.Color.Empty
Me.btnFundActivityAdjustment.ButtonType = eZee.Common.eZeeLightButton.enButtonType.CheckButton
Me.btnFundActivityAdjustment.FlatAppearance.BorderSize = 0
Me.btnFundActivityAdjustment.FlatStyle = System.Windows.Forms.FlatStyle.Flat
Me.btnFundActivityAdjustment.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
Me.btnFundActivityAdjustment.ForeColor = System.Drawing.Color.Black
Me.btnFundActivityAdjustment.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
Me.btnFundActivityAdjustment.GradientForeColor = System.Drawing.Color.Black
Me.btnFundActivityAdjustment.HoverGradientBackColor = System.Drawing.Color.Transparent
Me.btnFundActivityAdjustment.HoverGradientForeColor = System.Drawing.Color.Black
Me.btnFundActivityAdjustment.Image = Global.Aruti.Main.My.Resources.Resources.Expense_List_24_x_24
Me.btnFundActivityAdjustment.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
Me.btnFundActivityAdjustment.Location = New System.Drawing.Point(3, 167)
Me.btnFundActivityAdjustment.MinimumSize = New System.Drawing.Size(150, 35)
Me.btnFundActivityAdjustment.Name = "btnFundActivityAdjustment"
Me.btnFundActivityAdjustment.PressedGradientBackColor = System.Drawing.Color.Transparent
Me.btnFundActivityAdjustment.PressedGradientForeColor = System.Drawing.Color.Black
Me.btnFundActivityAdjustment.Size = New System.Drawing.Size(166, 35)
Me.btnFundActivityAdjustment.TabIndex = 39
Me.btnFundActivityAdjustment.Text = "Activity Adjustment"
Me.btnFundActivityAdjustment.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
Me.btnFundActivityAdjustment.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText
Me.btnFundActivityAdjustment.UseVisualStyleBackColor = false
'
'btnBgtApproverLevel
'
Me.btnBgtApproverLevel.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left)  _
            Or System.Windows.Forms.AnchorStyles.Right),System.Windows.Forms.AnchorStyles)
Me.btnBgtApproverLevel.BackColor = System.Drawing.Color.Transparent
Me.btnBgtApproverLevel.BackgroundImage = CType(resources.GetObject("btnBgtApproverLevel.BackgroundImage"),System.Drawing.Image)
Me.btnBgtApproverLevel.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
Me.btnBgtApproverLevel.BorderColor = System.Drawing.Color.Empty
Me.btnBgtApproverLevel.ButtonType = eZee.Common.eZeeLightButton.enButtonType.CheckButton
Me.btnBgtApproverLevel.FlatAppearance.BorderSize = 0
Me.btnBgtApproverLevel.FlatStyle = System.Windows.Forms.FlatStyle.Flat
Me.btnBgtApproverLevel.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
Me.btnBgtApproverLevel.ForeColor = System.Drawing.Color.Black
Me.btnBgtApproverLevel.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
Me.btnBgtApproverLevel.GradientForeColor = System.Drawing.Color.Black
Me.btnBgtApproverLevel.HoverGradientBackColor = System.Drawing.Color.Transparent
Me.btnBgtApproverLevel.HoverGradientForeColor = System.Drawing.Color.Black
Me.btnBgtApproverLevel.Image = Global.Aruti.Main.My.Resources.Resources.Approver_Level
Me.btnBgtApproverLevel.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
Me.btnBgtApproverLevel.Location = New System.Drawing.Point(3, 208)
Me.btnBgtApproverLevel.MinimumSize = New System.Drawing.Size(150, 35)
Me.btnBgtApproverLevel.Name = "btnBgtApproverLevel"
Me.btnBgtApproverLevel.PressedGradientBackColor = System.Drawing.Color.Transparent
Me.btnBgtApproverLevel.PressedGradientForeColor = System.Drawing.Color.Black
Me.btnBgtApproverLevel.Size = New System.Drawing.Size(166, 35)
Me.btnBgtApproverLevel.TabIndex = 40
Me.btnBgtApproverLevel.Text = "&Approver Level"
Me.btnBgtApproverLevel.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
Me.btnBgtApproverLevel.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText
Me.btnBgtApproverLevel.UseVisualStyleBackColor = false
'
'btnBgtApproverMapping
'
Me.btnBgtApproverMapping.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left)  _
            Or System.Windows.Forms.AnchorStyles.Right),System.Windows.Forms.AnchorStyles)
Me.btnBgtApproverMapping.BackColor = System.Drawing.Color.Transparent
Me.btnBgtApproverMapping.BackgroundImage = CType(resources.GetObject("btnBgtApproverMapping.BackgroundImage"),System.Drawing.Image)
Me.btnBgtApproverMapping.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
Me.btnBgtApproverMapping.BorderColor = System.Drawing.Color.Empty
Me.btnBgtApproverMapping.ButtonType = eZee.Common.eZeeLightButton.enButtonType.CheckButton
Me.btnBgtApproverMapping.FlatAppearance.BorderSize = 0
Me.btnBgtApproverMapping.FlatStyle = System.Windows.Forms.FlatStyle.Flat
Me.btnBgtApproverMapping.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
Me.btnBgtApproverMapping.ForeColor = System.Drawing.Color.Black
Me.btnBgtApproverMapping.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
Me.btnBgtApproverMapping.GradientForeColor = System.Drawing.Color.Black
Me.btnBgtApproverMapping.HoverGradientBackColor = System.Drawing.Color.Transparent
Me.btnBgtApproverMapping.HoverGradientForeColor = System.Drawing.Color.Black
Me.btnBgtApproverMapping.Image = Global.Aruti.Main.My.Resources.Resources.Leave_Approver
Me.btnBgtApproverMapping.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
Me.btnBgtApproverMapping.Location = New System.Drawing.Point(3, 249)
Me.btnBgtApproverMapping.MinimumSize = New System.Drawing.Size(150, 35)
Me.btnBgtApproverMapping.Name = "btnBgtApproverMapping"
Me.btnBgtApproverMapping.PressedGradientBackColor = System.Drawing.Color.Transparent
Me.btnBgtApproverMapping.PressedGradientForeColor = System.Drawing.Color.Black
Me.btnBgtApproverMapping.Size = New System.Drawing.Size(166, 35)
Me.btnBgtApproverMapping.TabIndex = 41
Me.btnBgtApproverMapping.Text = "&Approver Level Mapping"
Me.btnBgtApproverMapping.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
Me.btnBgtApproverMapping.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText
Me.btnBgtApproverMapping.UseVisualStyleBackColor = false
'
'btnBudgetFormula
'
Me.btnBudgetFormula.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left)  _
            Or System.Windows.Forms.AnchorStyles.Right),System.Windows.Forms.AnchorStyles)
Me.btnBudgetFormula.BackColor = System.Drawing.Color.Transparent
Me.btnBudgetFormula.BackgroundImage = CType(resources.GetObject("btnBudgetFormula.BackgroundImage"),System.Drawing.Image)
Me.btnBudgetFormula.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
Me.btnBudgetFormula.BorderColor = System.Drawing.Color.Empty
Me.btnBudgetFormula.ButtonType = eZee.Common.eZeeLightButton.enButtonType.CheckButton
Me.btnBudgetFormula.FlatAppearance.BorderSize = 0
Me.btnBudgetFormula.FlatStyle = System.Windows.Forms.FlatStyle.Flat
Me.btnBudgetFormula.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
Me.btnBudgetFormula.ForeColor = System.Drawing.Color.Black
Me.btnBudgetFormula.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
Me.btnBudgetFormula.GradientForeColor = System.Drawing.Color.Black
Me.btnBudgetFormula.HoverGradientBackColor = System.Drawing.Color.Transparent
Me.btnBudgetFormula.HoverGradientForeColor = System.Drawing.Color.Black
Me.btnBudgetFormula.Image = Global.Aruti.Main.My.Resources.Resources.process_pending_loan
Me.btnBudgetFormula.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
Me.btnBudgetFormula.Location = New System.Drawing.Point(3, 290)
Me.btnBudgetFormula.MinimumSize = New System.Drawing.Size(150, 35)
Me.btnBudgetFormula.Name = "btnBudgetFormula"
Me.btnBudgetFormula.PressedGradientBackColor = System.Drawing.Color.Transparent
Me.btnBudgetFormula.PressedGradientForeColor = System.Drawing.Color.Black
Me.btnBudgetFormula.Size = New System.Drawing.Size(166, 36)
Me.btnBudgetFormula.TabIndex = 36
Me.btnBudgetFormula.Text = "Budget Formula"
Me.btnBudgetFormula.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
Me.btnBudgetFormula.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText
Me.btnBudgetFormula.UseVisualStyleBackColor = false
'
'btnBudget
'
Me.btnBudget.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left)  _
            Or System.Windows.Forms.AnchorStyles.Right),System.Windows.Forms.AnchorStyles)
Me.btnBudget.BackColor = System.Drawing.Color.Transparent
Me.btnBudget.BackgroundImage = CType(resources.GetObject("btnBudget.BackgroundImage"),System.Drawing.Image)
Me.btnBudget.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
Me.btnBudget.BorderColor = System.Drawing.Color.Empty
Me.btnBudget.ButtonType = eZee.Common.eZeeLightButton.enButtonType.CheckButton
Me.btnBudget.FlatAppearance.BorderSize = 0
Me.btnBudget.FlatStyle = System.Windows.Forms.FlatStyle.Flat
Me.btnBudget.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
Me.btnBudget.ForeColor = System.Drawing.Color.Black
Me.btnBudget.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
Me.btnBudget.GradientForeColor = System.Drawing.Color.Black
Me.btnBudget.HoverGradientBackColor = System.Drawing.Color.Transparent
Me.btnBudget.HoverGradientForeColor = System.Drawing.Color.Black
Me.btnBudget.Image = Global.Aruti.Main.My.Resources.Resources.Budget_Preparation
Me.btnBudget.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
Me.btnBudget.Location = New System.Drawing.Point(3, 332)
Me.btnBudget.MinimumSize = New System.Drawing.Size(150, 35)
Me.btnBudget.Name = "btnBudget"
Me.btnBudget.PressedGradientBackColor = System.Drawing.Color.Transparent
Me.btnBudget.PressedGradientForeColor = System.Drawing.Color.Black
Me.btnBudget.Size = New System.Drawing.Size(166, 36)
Me.btnBudget.TabIndex = 37
Me.btnBudget.Text = "Payroll Budgeting"
Me.btnBudget.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
Me.btnBudget.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText
Me.btnBudget.UseVisualStyleBackColor = false
'
'btnBudgetCodes
'
Me.btnBudgetCodes.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left)  _
            Or System.Windows.Forms.AnchorStyles.Right),System.Windows.Forms.AnchorStyles)
Me.btnBudgetCodes.BackColor = System.Drawing.Color.Transparent
Me.btnBudgetCodes.BackgroundImage = CType(resources.GetObject("btnBudgetCodes.BackgroundImage"),System.Drawing.Image)
Me.btnBudgetCodes.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
Me.btnBudgetCodes.BorderColor = System.Drawing.Color.Empty
Me.btnBudgetCodes.ButtonType = eZee.Common.eZeeLightButton.enButtonType.CheckButton
Me.btnBudgetCodes.FlatAppearance.BorderSize = 0
Me.btnBudgetCodes.FlatStyle = System.Windows.Forms.FlatStyle.Flat
Me.btnBudgetCodes.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
Me.btnBudgetCodes.ForeColor = System.Drawing.Color.Black
Me.btnBudgetCodes.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
Me.btnBudgetCodes.GradientForeColor = System.Drawing.Color.Black
Me.btnBudgetCodes.HoverGradientBackColor = System.Drawing.Color.Transparent
Me.btnBudgetCodes.HoverGradientForeColor = System.Drawing.Color.Black
Me.btnBudgetCodes.Image = Global.Aruti.Main.My.Resources.Resources.Budget_Preparation
Me.btnBudgetCodes.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
Me.btnBudgetCodes.Location = New System.Drawing.Point(3, 374)
Me.btnBudgetCodes.MinimumSize = New System.Drawing.Size(150, 35)
Me.btnBudgetCodes.Name = "btnBudgetCodes"
Me.btnBudgetCodes.PressedGradientBackColor = System.Drawing.Color.Transparent
Me.btnBudgetCodes.PressedGradientForeColor = System.Drawing.Color.Black
Me.btnBudgetCodes.Size = New System.Drawing.Size(166, 36)
Me.btnBudgetCodes.TabIndex = 40
Me.btnBudgetCodes.Text = "Budget Codes"
Me.btnBudgetCodes.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
Me.btnBudgetCodes.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText
Me.btnBudgetCodes.UseVisualStyleBackColor = false
'
'fpnlTimesheetInfo
'
Me.fpnlTimesheetInfo.AutoScroll = true
Me.fpnlTimesheetInfo.Controls.Add(Me.btnTimesheetApproverLevel)
Me.fpnlTimesheetInfo.Controls.Add(Me.btnTimesheetApproverMst)
Me.fpnlTimesheetInfo.Controls.Add(Me.btnEmployeeTimesheet)
Me.fpnlTimesheetInfo.Controls.Add(Me.btnTimesheetApproval)
Me.fpnlTimesheetInfo.Controls.Add(Me.btnTimesheetApproverMigration)
Me.fpnlTimesheetInfo.Location = New System.Drawing.Point(1384, 96)
Me.fpnlTimesheetInfo.Name = "fpnlTimesheetInfo"
Me.fpnlTimesheetInfo.Size = New System.Drawing.Size(191, 39)
Me.fpnlTimesheetInfo.TabIndex = 28
Me.fpnlTimesheetInfo.Visible = false
'
'btnTimesheetApproverLevel
'
Me.btnTimesheetApproverLevel.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left)  _
            Or System.Windows.Forms.AnchorStyles.Right),System.Windows.Forms.AnchorStyles)
Me.btnTimesheetApproverLevel.BackColor = System.Drawing.Color.Transparent
Me.btnTimesheetApproverLevel.BackgroundImage = CType(resources.GetObject("btnTimesheetApproverLevel.BackgroundImage"),System.Drawing.Image)
Me.btnTimesheetApproverLevel.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
Me.btnTimesheetApproverLevel.BorderColor = System.Drawing.Color.Empty
Me.btnTimesheetApproverLevel.ButtonType = eZee.Common.eZeeLightButton.enButtonType.CheckButton
Me.btnTimesheetApproverLevel.FlatAppearance.BorderSize = 0
Me.btnTimesheetApproverLevel.FlatStyle = System.Windows.Forms.FlatStyle.Flat
Me.btnTimesheetApproverLevel.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
Me.btnTimesheetApproverLevel.ForeColor = System.Drawing.Color.Black
Me.btnTimesheetApproverLevel.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
Me.btnTimesheetApproverLevel.GradientForeColor = System.Drawing.Color.Black
Me.btnTimesheetApproverLevel.HoverGradientBackColor = System.Drawing.Color.Transparent
Me.btnTimesheetApproverLevel.HoverGradientForeColor = System.Drawing.Color.Black
Me.btnTimesheetApproverLevel.Image = Global.Aruti.Main.My.Resources.Resources.Approver_Level
Me.btnTimesheetApproverLevel.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
Me.btnTimesheetApproverLevel.Location = New System.Drawing.Point(3, 3)
Me.btnTimesheetApproverLevel.MinimumSize = New System.Drawing.Size(150, 35)
Me.btnTimesheetApproverLevel.Name = "btnTimesheetApproverLevel"
Me.btnTimesheetApproverLevel.PressedGradientBackColor = System.Drawing.Color.Transparent
Me.btnTimesheetApproverLevel.PressedGradientForeColor = System.Drawing.Color.Black
Me.btnTimesheetApproverLevel.Size = New System.Drawing.Size(166, 35)
Me.btnTimesheetApproverLevel.TabIndex = 40
Me.btnTimesheetApproverLevel.Text = "&Approver Level"
Me.btnTimesheetApproverLevel.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
Me.btnTimesheetApproverLevel.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText
Me.btnTimesheetApproverLevel.UseVisualStyleBackColor = false
'
'btnTimesheetApproverMst
'
Me.btnTimesheetApproverMst.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left)  _
            Or System.Windows.Forms.AnchorStyles.Right),System.Windows.Forms.AnchorStyles)
Me.btnTimesheetApproverMst.BackColor = System.Drawing.Color.Transparent
Me.btnTimesheetApproverMst.BackgroundImage = CType(resources.GetObject("btnTimesheetApproverMst.BackgroundImage"),System.Drawing.Image)
Me.btnTimesheetApproverMst.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
Me.btnTimesheetApproverMst.BorderColor = System.Drawing.Color.Empty
Me.btnTimesheetApproverMst.ButtonType = eZee.Common.eZeeLightButton.enButtonType.CheckButton
Me.btnTimesheetApproverMst.FlatAppearance.BorderSize = 0
Me.btnTimesheetApproverMst.FlatStyle = System.Windows.Forms.FlatStyle.Flat
Me.btnTimesheetApproverMst.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
Me.btnTimesheetApproverMst.ForeColor = System.Drawing.Color.Black
Me.btnTimesheetApproverMst.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
Me.btnTimesheetApproverMst.GradientForeColor = System.Drawing.Color.Black
Me.btnTimesheetApproverMst.HoverGradientBackColor = System.Drawing.Color.Transparent
Me.btnTimesheetApproverMst.HoverGradientForeColor = System.Drawing.Color.Black
Me.btnTimesheetApproverMst.Image = Global.Aruti.Main.My.Resources.Resources.Leave_Approver
Me.btnTimesheetApproverMst.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
Me.btnTimesheetApproverMst.Location = New System.Drawing.Point(3, 44)
Me.btnTimesheetApproverMst.MinimumSize = New System.Drawing.Size(150, 35)
Me.btnTimesheetApproverMst.Name = "btnTimesheetApproverMst"
Me.btnTimesheetApproverMst.PressedGradientBackColor = System.Drawing.Color.Transparent
Me.btnTimesheetApproverMst.PressedGradientForeColor = System.Drawing.Color.Black
Me.btnTimesheetApproverMst.Size = New System.Drawing.Size(166, 35)
Me.btnTimesheetApproverMst.TabIndex = 41
Me.btnTimesheetApproverMst.Text = "&Approver Master"
Me.btnTimesheetApproverMst.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
Me.btnTimesheetApproverMst.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText
Me.btnTimesheetApproverMst.UseVisualStyleBackColor = false
'
'btnEmployeeTimesheet
'
Me.btnEmployeeTimesheet.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left)  _
            Or System.Windows.Forms.AnchorStyles.Right),System.Windows.Forms.AnchorStyles)
Me.btnEmployeeTimesheet.BackColor = System.Drawing.Color.Transparent
Me.btnEmployeeTimesheet.BackgroundImage = CType(resources.GetObject("btnEmployeeTimesheet.BackgroundImage"),System.Drawing.Image)
Me.btnEmployeeTimesheet.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
Me.btnEmployeeTimesheet.BorderColor = System.Drawing.Color.Empty
Me.btnEmployeeTimesheet.ButtonType = eZee.Common.eZeeLightButton.enButtonType.CheckButton
Me.btnEmployeeTimesheet.FlatAppearance.BorderSize = 0
Me.btnEmployeeTimesheet.FlatStyle = System.Windows.Forms.FlatStyle.Flat
Me.btnEmployeeTimesheet.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
Me.btnEmployeeTimesheet.ForeColor = System.Drawing.Color.Black
Me.btnEmployeeTimesheet.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
Me.btnEmployeeTimesheet.GradientForeColor = System.Drawing.Color.Black
Me.btnEmployeeTimesheet.HoverGradientBackColor = System.Drawing.Color.Transparent
Me.btnEmployeeTimesheet.HoverGradientForeColor = System.Drawing.Color.Black
Me.btnEmployeeTimesheet.Image = Global.Aruti.Main.My.Resources.Resources.process_pending_loan
Me.btnEmployeeTimesheet.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
Me.btnEmployeeTimesheet.Location = New System.Drawing.Point(3, 85)
Me.btnEmployeeTimesheet.MinimumSize = New System.Drawing.Size(150, 35)
Me.btnEmployeeTimesheet.Name = "btnEmployeeTimesheet"
Me.btnEmployeeTimesheet.PressedGradientBackColor = System.Drawing.Color.Transparent
Me.btnEmployeeTimesheet.PressedGradientForeColor = System.Drawing.Color.Black
Me.btnEmployeeTimesheet.Size = New System.Drawing.Size(166, 36)
Me.btnEmployeeTimesheet.TabIndex = 36
Me.btnEmployeeTimesheet.Text = "Employee Timesheet"
Me.btnEmployeeTimesheet.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
Me.btnEmployeeTimesheet.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText
Me.btnEmployeeTimesheet.UseVisualStyleBackColor = false
'
'btnTimesheetApproval
'
Me.btnTimesheetApproval.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left)  _
            Or System.Windows.Forms.AnchorStyles.Right),System.Windows.Forms.AnchorStyles)
Me.btnTimesheetApproval.BackColor = System.Drawing.Color.Transparent
Me.btnTimesheetApproval.BackgroundImage = CType(resources.GetObject("btnTimesheetApproval.BackgroundImage"),System.Drawing.Image)
Me.btnTimesheetApproval.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
Me.btnTimesheetApproval.BorderColor = System.Drawing.Color.Empty
Me.btnTimesheetApproval.ButtonType = eZee.Common.eZeeLightButton.enButtonType.CheckButton
Me.btnTimesheetApproval.FlatAppearance.BorderSize = 0
Me.btnTimesheetApproval.FlatStyle = System.Windows.Forms.FlatStyle.Flat
Me.btnTimesheetApproval.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
Me.btnTimesheetApproval.ForeColor = System.Drawing.Color.Black
Me.btnTimesheetApproval.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
Me.btnTimesheetApproval.GradientForeColor = System.Drawing.Color.Black
Me.btnTimesheetApproval.HoverGradientBackColor = System.Drawing.Color.Transparent
Me.btnTimesheetApproval.HoverGradientForeColor = System.Drawing.Color.Black
Me.btnTimesheetApproval.Image = Global.Aruti.Main.My.Resources.Resources.Budget_Preparation
Me.btnTimesheetApproval.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
Me.btnTimesheetApproval.Location = New System.Drawing.Point(3, 127)
Me.btnTimesheetApproval.MinimumSize = New System.Drawing.Size(150, 35)
Me.btnTimesheetApproval.Name = "btnTimesheetApproval"
Me.btnTimesheetApproval.PressedGradientBackColor = System.Drawing.Color.Transparent
Me.btnTimesheetApproval.PressedGradientForeColor = System.Drawing.Color.Black
Me.btnTimesheetApproval.Size = New System.Drawing.Size(166, 36)
Me.btnTimesheetApproval.TabIndex = 37
Me.btnTimesheetApproval.Text = "Employee Timesheet Approval"
Me.btnTimesheetApproval.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
Me.btnTimesheetApproval.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText
Me.btnTimesheetApproval.UseVisualStyleBackColor = false
'
'btnTimesheetApproverMigration
'
Me.btnTimesheetApproverMigration.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left)  _
            Or System.Windows.Forms.AnchorStyles.Right),System.Windows.Forms.AnchorStyles)
Me.btnTimesheetApproverMigration.BackColor = System.Drawing.Color.Transparent
Me.btnTimesheetApproverMigration.BackgroundImage = CType(resources.GetObject("btnTimesheetApproverMigration.BackgroundImage"),System.Drawing.Image)
Me.btnTimesheetApproverMigration.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
Me.btnTimesheetApproverMigration.BorderColor = System.Drawing.Color.Empty
Me.btnTimesheetApproverMigration.ButtonType = eZee.Common.eZeeLightButton.enButtonType.CheckButton
Me.btnTimesheetApproverMigration.FlatAppearance.BorderSize = 0
Me.btnTimesheetApproverMigration.FlatStyle = System.Windows.Forms.FlatStyle.Flat
Me.btnTimesheetApproverMigration.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
Me.btnTimesheetApproverMigration.ForeColor = System.Drawing.Color.Black
Me.btnTimesheetApproverMigration.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
Me.btnTimesheetApproverMigration.GradientForeColor = System.Drawing.Color.Black
Me.btnTimesheetApproverMigration.HoverGradientBackColor = System.Drawing.Color.Transparent
Me.btnTimesheetApproverMigration.HoverGradientForeColor = System.Drawing.Color.Black
Me.btnTimesheetApproverMigration.Image = Global.Aruti.Main.My.Resources.Resources.Budget_Preparation
Me.btnTimesheetApproverMigration.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
Me.btnTimesheetApproverMigration.Location = New System.Drawing.Point(3, 169)
Me.btnTimesheetApproverMigration.MinimumSize = New System.Drawing.Size(150, 35)
Me.btnTimesheetApproverMigration.Name = "btnTimesheetApproverMigration"
Me.btnTimesheetApproverMigration.PressedGradientBackColor = System.Drawing.Color.Transparent
Me.btnTimesheetApproverMigration.PressedGradientForeColor = System.Drawing.Color.Black
Me.btnTimesheetApproverMigration.Size = New System.Drawing.Size(166, 36)
Me.btnTimesheetApproverMigration.TabIndex = 40
Me.btnTimesheetApproverMigration.Text = "Approver Migration"
Me.btnTimesheetApproverMigration.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
Me.btnTimesheetApproverMigration.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText
Me.btnTimesheetApproverMigration.UseVisualStyleBackColor = false
'
'pnlSummary
'
Me.pnlSummary.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
Me.pnlSummary.Dock = System.Windows.Forms.DockStyle.Fill
Me.pnlSummary.Location = New System.Drawing.Point(196, 4)
Me.pnlSummary.Name = "pnlSummary"
Me.pnlSummary.Size = New System.Drawing.Size(687, 512)
Me.pnlSummary.TabIndex = 1
'
'objspc1
'
Me.objspc1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
Me.objspc1.Dock = System.Windows.Forms.DockStyle.Fill
Me.objspc1.FixedPanel = System.Windows.Forms.FixedPanel.Panel1
Me.objspc1.IsSplitterFixed = true
Me.objspc1.Location = New System.Drawing.Point(4, 4)
Me.objspc1.Name = "objspc1"
Me.objspc1.Orientation = System.Windows.Forms.Orientation.Horizontal
'
'objspc1.Panel1
'
Me.objspc1.Panel1.Controls.Add(Me.objlblCaption)
'
'objspc1.Panel2
'
Me.objspc1.Panel2.Controls.Add(Me.pnlMenuItems)
Me.objspc1.Size = New System.Drawing.Size(185, 512)
Me.objspc1.SplitterDistance = 25
Me.objspc1.SplitterWidth = 2
Me.objspc1.TabIndex = 18
'
'objlblCaption
'
Me.objlblCaption.Dock = System.Windows.Forms.DockStyle.Fill
Me.objlblCaption.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
Me.objlblCaption.Location = New System.Drawing.Point(0, 0)
Me.objlblCaption.Name = "objlblCaption"
Me.objlblCaption.Size = New System.Drawing.Size(183, 23)
Me.objlblCaption.TabIndex = 18
Me.objlblCaption.Text = "objlblCaption"
Me.objlblCaption.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
'
'pnlMenuItems
'
Me.pnlMenuItems.Dock = System.Windows.Forms.DockStyle.Fill
Me.pnlMenuItems.Location = New System.Drawing.Point(0, 0)
Me.pnlMenuItems.Name = "pnlMenuItems"
Me.pnlMenuItems.Size = New System.Drawing.Size(183, 483)
Me.pnlMenuItems.TabIndex = 0
'
'pnlMainInfo
'
Me.pnlMainInfo.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom)  _
            Or System.Windows.Forms.AnchorStyles.Left)  _
            Or System.Windows.Forms.AnchorStyles.Right),System.Windows.Forms.AnchorStyles)
Me.pnlMainInfo.BackColor = System.Drawing.SystemColors.Control
Me.pnlMainInfo.Controls.Add(Me.tblMDILayout)
Me.pnlMainInfo.Location = New System.Drawing.Point(1, 25)
Me.pnlMainInfo.Name = "pnlMainInfo"
Me.pnlMainInfo.Size = New System.Drawing.Size(887, 521)
Me.pnlMainInfo.TabIndex = 12
'
'mnuArutiPayroll
'
Me.mnuArutiPayroll.BackColor = System.Drawing.SystemColors.Control
Me.mnuArutiPayroll.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.mnuGeneralMaster, Me.mnuRecruitment, Me.mnuHumanResource, Me.mnuPayroll, Me.mnuLeaveAndTnA, Me.mnuUtilitiesMain, Me.mnuAuditTrail, Me.mnuView, Me.mnuHelp, Me.mnuExit})
Me.mnuArutiPayroll.Location = New System.Drawing.Point(0, 0)
Me.mnuArutiPayroll.Name = "mnuArutiPayroll"
Me.mnuArutiPayroll.Size = New System.Drawing.Size(889, 24)
Me.mnuArutiPayroll.TabIndex = 14
Me.mnuArutiPayroll.Text = "MenuStrip1"
'
'mnuGeneralMaster
'
Me.mnuGeneralMaster.CheckOnClick = true
Me.mnuGeneralMaster.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.mnuCoreSetups, Me.mnuCommonSetups, Me.mnuWagesSetup})
Me.mnuGeneralMaster.Name = "mnuGeneralMaster"
Me.mnuGeneralMaster.Size = New System.Drawing.Size(98, 20)
Me.mnuGeneralMaster.Text = "&General Master"
'
'mnuCoreSetups
'
Me.mnuCoreSetups.Name = "mnuCoreSetups"
Me.mnuCoreSetups.Size = New System.Drawing.Size(163, 22)
Me.mnuCoreSetups.Tag = "mnuCoreSetups"
Me.mnuCoreSetups.Text = "Co&re Setups"
'
'mnuCommonSetups
'
Me.mnuCommonSetups.Name = "mnuCommonSetups"
Me.mnuCommonSetups.Size = New System.Drawing.Size(163, 22)
Me.mnuCommonSetups.Tag = "mnuCommonSetups"
Me.mnuCommonSetups.Text = "&Common Setups"
'
'mnuWagesSetup
'
Me.mnuWagesSetup.Name = "mnuWagesSetup"
Me.mnuWagesSetup.Size = New System.Drawing.Size(163, 22)
Me.mnuWagesSetup.Text = "Wages Setup"
'
'mnuRecruitment
'
Me.mnuRecruitment.Name = "mnuRecruitment"
Me.mnuRecruitment.Size = New System.Drawing.Size(84, 20)
Me.mnuRecruitment.Text = "&Recruitment"
Me.mnuRecruitment.Visible = false
'
'mnuHumanResource
'
Me.mnuHumanResource.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.mnuPersonnel, Me.mnuMedical, Me.mnuRecruitmentItem, Me.mnuDiscipline, Me.mnuAssessment, Me.mnuTrainingInformation, Me.mnuAssetDeclaration})
Me.mnuHumanResource.Name = "mnuHumanResource"
Me.mnuHumanResource.Size = New System.Drawing.Size(110, 20)
Me.mnuHumanResource.Text = "&Human Resource"
'
'mnuPersonnel
'
Me.mnuPersonnel.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.mnuHRMaster, Me.mnuEmployeeData})
Me.mnuPersonnel.Name = "mnuPersonnel"
Me.mnuPersonnel.Size = New System.Drawing.Size(216, 22)
Me.mnuPersonnel.Text = "&Personnel"
'
'mnuHRMaster
'
Me.mnuHRMaster.Name = "mnuHRMaster"
Me.mnuHRMaster.Size = New System.Drawing.Size(153, 22)
Me.mnuHRMaster.Text = "H&R Master"
'
'mnuEmployeeData
'
Me.mnuEmployeeData.Name = "mnuEmployeeData"
Me.mnuEmployeeData.Size = New System.Drawing.Size(153, 22)
Me.mnuEmployeeData.Text = "&Employee Data"
'
'mnuMedical
'
Me.mnuMedical.Name = "mnuMedical"
Me.mnuMedical.Size = New System.Drawing.Size(216, 22)
Me.mnuMedical.Text = "&Medical"
'
'mnuRecruitmentItem
'
Me.mnuRecruitmentItem.Name = "mnuRecruitmentItem"
Me.mnuRecruitmentItem.Size = New System.Drawing.Size(216, 22)
Me.mnuRecruitmentItem.Text = "&Recruitment"
'
'mnuDiscipline
'
Me.mnuDiscipline.Name = "mnuDiscipline"
Me.mnuDiscipline.Size = New System.Drawing.Size(216, 22)
Me.mnuDiscipline.Tag = "mnuDiscipline"
Me.mnuDiscipline.Text = "&Discipline"
'
'mnuAssessment
'
Me.mnuAssessment.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.mnuSetups, Me.mnuPerformaceEvaluation, Me.mnuGeneralAssessment, Me.mnuBalancedScoreCard, Me.mnuAppraisal})
Me.mnuAssessment.Name = "mnuAssessment"
Me.mnuAssessment.Size = New System.Drawing.Size(216, 22)
Me.mnuAssessment.Tag = "mnuAssessment"
Me.mnuAssessment.Text = "Performance &Management"
'
'mnuSetups
'
Me.mnuSetups.Name = "mnuSetups"
Me.mnuSetups.Size = New System.Drawing.Size(207, 22)
Me.mnuSetups.Text = "&Setups"
'
'mnuPerformaceEvaluation
'
Me.mnuPerformaceEvaluation.Name = "mnuPerformaceEvaluation"
Me.mnuPerformaceEvaluation.Size = New System.Drawing.Size(207, 22)
Me.mnuPerformaceEvaluation.Tag = "mnuPerformaceEvaluation"
Me.mnuPerformaceEvaluation.Text = "Performance Assessment"
'
'mnuGeneralAssessment
'
Me.mnuGeneralAssessment.Name = "mnuGeneralAssessment"
Me.mnuGeneralAssessment.Size = New System.Drawing.Size(207, 22)
Me.mnuGeneralAssessment.Tag = "mnuGeneralAssessment"
Me.mnuGeneralAssessment.Text = "&General Assessment"
Me.mnuGeneralAssessment.Visible = false
'
'mnuBalancedScoreCard
'
Me.mnuBalancedScoreCard.Name = "mnuBalancedScoreCard"
Me.mnuBalancedScoreCard.Size = New System.Drawing.Size(207, 22)
Me.mnuBalancedScoreCard.Tag = "mnuBalancedScoreCard"
Me.mnuBalancedScoreCard.Text = "&Balanced Score Card"
Me.mnuBalancedScoreCard.Visible = false
'
'mnuAppraisal
'
Me.mnuAppraisal.Name = "mnuAppraisal"
Me.mnuAppraisal.Size = New System.Drawing.Size(207, 22)
Me.mnuAppraisal.Text = "A&ppraisal"
'
'mnuTrainingInformation
'
Me.mnuTrainingInformation.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.mnuJobTraining, Me.mnuTrainingEvaluation})
Me.mnuTrainingInformation.Name = "mnuTrainingInformation"
Me.mnuTrainingInformation.Size = New System.Drawing.Size(216, 22)
Me.mnuTrainingInformation.Tag = "mnuTrainingInformation"
Me.mnuTrainingInformation.Text = "Training Information"
'
'mnuJobTraining
'
Me.mnuJobTraining.Name = "mnuJobTraining"
Me.mnuJobTraining.Size = New System.Drawing.Size(174, 22)
Me.mnuJobTraining.Text = "On Job &Training"
'
'mnuTrainingEvaluation
'
Me.mnuTrainingEvaluation.Name = "mnuTrainingEvaluation"
Me.mnuTrainingEvaluation.Size = New System.Drawing.Size(174, 22)
Me.mnuTrainingEvaluation.Text = "Training Evaluation"
'
'mnuAssetDeclaration
'
Me.mnuAssetDeclaration.Name = "mnuAssetDeclaration"
Me.mnuAssetDeclaration.Size = New System.Drawing.Size(216, 22)
Me.mnuAssetDeclaration.Text = "Assets Declaration"
'
'mnuPayroll
'
Me.mnuPayroll.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.mnuPayrollMasters, Me.mnuPayrollTransactions, Me.mnuLoan_Advance_Savings, Me.mnuPayActivity, Me.mnuBudget})
Me.mnuPayroll.Name = "mnuPayroll"
Me.mnuPayroll.Size = New System.Drawing.Size(55, 20)
Me.mnuPayroll.Text = "&Payroll"
'
'mnuPayrollMasters
'
Me.mnuPayrollMasters.Name = "mnuPayrollMasters"
Me.mnuPayrollMasters.Size = New System.Drawing.Size(207, 22)
Me.mnuPayrollMasters.Text = "Payroll &Masters"
'
'mnuPayrollTransactions
'
Me.mnuPayrollTransactions.Name = "mnuPayrollTransactions"
Me.mnuPayrollTransactions.Size = New System.Drawing.Size(207, 22)
Me.mnuPayrollTransactions.Text = "Payroll &Transactions"
'
'mnuLoan_Advance_Savings
'
Me.mnuLoan_Advance_Savings.Name = "mnuLoan_Advance_Savings"
Me.mnuLoan_Advance_Savings.Size = New System.Drawing.Size(207, 22)
Me.mnuLoan_Advance_Savings.Text = "Loan/&Advance && Savings"
'
'mnuPayActivity
'
Me.mnuPayActivity.Name = "mnuPayActivity"
Me.mnuPayActivity.Size = New System.Drawing.Size(207, 22)
Me.mnuPayActivity.Tag = "mnuPayActivity"
Me.mnuPayActivity.Text = "Pay Per Activity"
'
'mnuBudget
'
Me.mnuBudget.Name = "mnuBudget"
Me.mnuBudget.Size = New System.Drawing.Size(207, 22)
Me.mnuBudget.Text = "Budget"
'
'mnuLeaveAndTnA
'
Me.mnuLeaveAndTnA.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.mnuLeaveInformation, Me.mnuTnAInfo, Me.mnuEmpBudgetTimesheet})
Me.mnuLeaveAndTnA.Name = "mnuLeaveAndTnA"
Me.mnuLeaveAndTnA.Size = New System.Drawing.Size(91, 20)
Me.mnuLeaveAndTnA.Text = "&Leave && Time"
'
'mnuLeaveInformation
'
Me.mnuLeaveInformation.Name = "mnuLeaveInformation"
Me.mnuLeaveInformation.Size = New System.Drawing.Size(233, 22)
Me.mnuLeaveInformation.Text = "Lea&ve Information"
'
'mnuTnAInfo
'
Me.mnuTnAInfo.Name = "mnuTnAInfo"
Me.mnuTnAInfo.Size = New System.Drawing.Size(233, 22)
Me.mnuTnAInfo.Text = "&Time & Attendance Information"
'
'mnuEmpBudgetTimesheet
'
Me.mnuEmpBudgetTimesheet.Name = "mnuEmpBudgetTimesheet"
Me.mnuEmpBudgetTimesheet.Size = New System.Drawing.Size(233, 22)
Me.mnuEmpBudgetTimesheet.Text = "Employee Budget Timesheet"
'
'mnuUtilitiesMain
'
Me.mnuUtilitiesMain.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.mnuUtilities, Me.mnuAuditTrails, Me.mnuClaimsExpenses})
Me.mnuUtilitiesMain.Name = "mnuUtilitiesMain"
Me.mnuUtilitiesMain.Size = New System.Drawing.Size(58, 20)
Me.mnuUtilitiesMain.Text = "&Utilities"
'
'mnuUtilities
'
Me.mnuUtilities.Name = "mnuUtilities"
Me.mnuUtilities.Size = New System.Drawing.Size(174, 22)
Me.mnuUtilities.Text = "&Utilities"
'
'mnuAuditTrails
'
Me.mnuAuditTrails.Name = "mnuAuditTrails"
Me.mnuAuditTrails.Size = New System.Drawing.Size(174, 22)
Me.mnuAuditTrails.Text = "&Audit Trails"
'
'mnuClaimsExpenses
'
Me.mnuClaimsExpenses.Name = "mnuClaimsExpenses"
Me.mnuClaimsExpenses.Size = New System.Drawing.Size(174, 22)
Me.mnuClaimsExpenses.Tag = "mnuClaimsExpenses"
Me.mnuClaimsExpenses.Text = "Claims && Expenses"
'
'mnuAuditTrail
'
Me.mnuAuditTrail.Name = "mnuAuditTrail"
Me.mnuAuditTrail.Size = New System.Drawing.Size(77, 20)
Me.mnuAuditTrail.Tag = "mnuAuditTrail"
Me.mnuAuditTrail.Text = "Audit Trails"
Me.mnuAuditTrail.Visible = false
'
'mnuView
'
Me.mnuView.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.mnuPayrollView, Me.mnuReportView, Me.mnuCustomReporting})
Me.mnuView.Name = "mnuView"
Me.mnuView.Size = New System.Drawing.Size(44, 20)
Me.mnuView.Text = "&View"
'
'mnuPayrollView
'
Me.mnuPayrollView.Name = "mnuPayrollView"
Me.mnuPayrollView.Size = New System.Drawing.Size(171, 22)
Me.mnuPayrollView.Text = "&Main View"
'
'mnuReportView
'
Me.mnuReportView.Name = "mnuReportView"
Me.mnuReportView.Size = New System.Drawing.Size(171, 22)
Me.mnuReportView.Text = "&Report View"
'
'mnuCustomReporting
'
Me.mnuCustomReporting.Name = "mnuCustomReporting"
Me.mnuCustomReporting.Size = New System.Drawing.Size(171, 22)
Me.mnuCustomReporting.Tag = "mnuCustomReporting"
Me.mnuCustomReporting.Text = "C&ustom Reporting"
'
'mnuHelp
'
Me.mnuHelp.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.mnuRegister, Me.mnuTroubleshoot, Me.mnuProduct, Me.mnuAbout, Me.mnuArutiHelp})
Me.mnuHelp.Name = "mnuHelp"
Me.mnuHelp.Size = New System.Drawing.Size(44, 20)
Me.mnuHelp.Text = "&Help"
'
'mnuRegister
'
Me.mnuRegister.Name = "mnuRegister"
Me.mnuRegister.Size = New System.Drawing.Size(143, 22)
Me.mnuRegister.Text = "Register"
'
'mnuProduct
'
Me.mnuProduct.Name = "mnuProduct"
Me.mnuProduct.Size = New System.Drawing.Size(143, 22)
Me.mnuProduct.Text = "&Product"
Me.mnuProduct.Visible = false
'
'mnuAbout
'
Me.mnuAbout.Name = "mnuAbout"
Me.mnuAbout.Size = New System.Drawing.Size(143, 22)
Me.mnuAbout.Text = "&About"
Me.mnuAbout.Visible = false
'
'mnuArutiHelp
'
Me.mnuArutiHelp.Name = "mnuArutiHelp"
Me.mnuArutiHelp.Size = New System.Drawing.Size(143, 22)
Me.mnuArutiHelp.Text = "Help"
'
'mnuExit
'
Me.mnuExit.Name = "mnuExit"
Me.mnuExit.Size = New System.Drawing.Size(38, 20)
Me.mnuExit.Text = "E&xit"
'
'mnuTrainingFeedback
'
Me.mnuTrainingFeedback.Name = "mnuTrainingFeedback"
Me.mnuTrainingFeedback.Size = New System.Drawing.Size(187, 22)
Me.mnuTrainingFeedback.Tag = "mnuTrainingFeedback"
Me.mnuTrainingFeedback.Text = "Training &Feedback"
'
'StatusStrip1
'
Me.StatusStrip1.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.lblPropertyName, Me.objbtnPropertyName, Me.objlblSep1, Me.lblPayrollYear, Me.objbtnYearName, Me.objlblSep2, Me.lblUserName, Me.objbtnUserName, Me.objlblSep3, Me.lblWorkingDate, Me.objbtnWorkingDate, Me.objlblSep4})
Me.StatusStrip1.Location = New System.Drawing.Point(0, 548)
Me.StatusStrip1.Name = "StatusStrip1"
Me.StatusStrip1.Size = New System.Drawing.Size(889, 22)
Me.StatusStrip1.TabIndex = 16
Me.StatusStrip1.Text = "StatusStrip1"
'
'lblPropertyName
'
Me.lblPropertyName.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
Me.lblPropertyName.Name = "lblPropertyName"
Me.lblPropertyName.Size = New System.Drawing.Size(89, 17)
Me.lblPropertyName.Text = "Company Name :"
'
'objbtnPropertyName
'
Me.objbtnPropertyName.AutoSize = false
Me.objbtnPropertyName.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text
Me.objbtnPropertyName.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
Me.objbtnPropertyName.Name = "objbtnPropertyName"
Me.objbtnPropertyName.Size = New System.Drawing.Size(490, 17)
Me.objbtnPropertyName.Spring = true
Me.objbtnPropertyName.Text = "Company Name"
'
'objlblSep1
'
Me.objlblSep1.Name = "objlblSep1"
Me.objlblSep1.Size = New System.Drawing.Size(10, 17)
Me.objlblSep1.Text = "|"
'
'lblPayrollYear
'
Me.lblPayrollYear.Name = "lblPayrollYear"
Me.lblPayrollYear.Size = New System.Drawing.Size(86, 17)
Me.lblPayrollYear.Text = "Database Year :"
'
'objbtnYearName
'
Me.objbtnYearName.AutoSize = false
Me.objbtnYearName.Name = "objbtnYearName"
Me.objbtnYearName.Size = New System.Drawing.Size(59, 17)
Me.objbtnYearName.Text = "Year Name"
'
'objlblSep2
'
Me.objlblSep2.Name = "objlblSep2"
Me.objlblSep2.Size = New System.Drawing.Size(10, 17)
Me.objlblSep2.Text = "|"
'
'lblUserName
'
Me.lblUserName.Name = "lblUserName"
Me.lblUserName.Size = New System.Drawing.Size(68, 17)
Me.lblUserName.Text = "UserName :"
'
'objbtnUserName
'
Me.objbtnUserName.Name = "objbtnUserName"
Me.objbtnUserName.Size = New System.Drawing.Size(62, 17)
Me.objbtnUserName.Text = "UserName"
'
'objlblSep3
'
Me.objlblSep3.Name = "objlblSep3"
Me.objlblSep3.Size = New System.Drawing.Size(10, 15)
Me.objlblSep3.Text = "|"
'
'lblWorkingDate
'
Me.lblWorkingDate.Name = "lblWorkingDate"
Me.lblWorkingDate.Size = New System.Drawing.Size(85, 15)
Me.lblWorkingDate.Text = "Working Date :"
'
'objbtnWorkingDate
'
Me.objbtnWorkingDate.Name = "objbtnWorkingDate"
Me.objbtnWorkingDate.Size = New System.Drawing.Size(79, 15)
Me.objbtnWorkingDate.Text = "Working Date"
'
'objlblSep4
'
Me.objlblSep4.Name = "objlblSep4"
Me.objlblSep4.Size = New System.Drawing.Size(10, 15)
Me.objlblSep4.Text = "|"
'
'tmrReminder
'
Me.tmrReminder.Interval = 60000
'
'dtpEmployeeAsOnDate
'
Me.dtpEmployeeAsOnDate.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right),System.Windows.Forms.AnchorStyles)
Me.dtpEmployeeAsOnDate.Checked = false
Me.dtpEmployeeAsOnDate.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
Me.dtpEmployeeAsOnDate.Format = System.Windows.Forms.DateTimePickerFormat.[Short]
Me.dtpEmployeeAsOnDate.Location = New System.Drawing.Point(139, 5)
Me.dtpEmployeeAsOnDate.Name = "dtpEmployeeAsOnDate"
Me.dtpEmployeeAsOnDate.Size = New System.Drawing.Size(108, 21)
Me.dtpEmployeeAsOnDate.TabIndex = 18
'
'Panel1
'
Me.Panel1.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right),System.Windows.Forms.AnchorStyles)
Me.Panel1.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D
Me.Panel1.Controls.Add(Me.lblEmployeeAsOnDate)
Me.Panel1.Controls.Add(Me.dtpEmployeeAsOnDate)
Me.Panel1.Location = New System.Drawing.Point(628, -4)
Me.Panel1.Name = "Panel1"
Me.Panel1.Size = New System.Drawing.Size(257, 28)
Me.Panel1.TabIndex = 19
'
'lblEmployeeAsOnDate
'
Me.lblEmployeeAsOnDate.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right),System.Windows.Forms.AnchorStyles)
Me.lblEmployeeAsOnDate.Location = New System.Drawing.Point(11, 9)
Me.lblEmployeeAsOnDate.Name = "lblEmployeeAsOnDate"
Me.lblEmployeeAsOnDate.Size = New System.Drawing.Size(116, 13)
Me.lblEmployeeAsOnDate.TabIndex = 19
Me.lblEmployeeAsOnDate.Text = "Employee As On Date"
'
'mnuTroubleshoot
'
Me.mnuTroubleshoot.Name = "mnuTroubleshoot"
Me.mnuTroubleshoot.Size = New System.Drawing.Size(143, 22)
Me.mnuTroubleshoot.Text = "Troubleshoot"
'
'frmNewMDI
'
Me.AutoScaleDimensions = New System.Drawing.SizeF(6!, 13!)
Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
Me.ClientSize = New System.Drawing.Size(889, 570)
Me.Controls.Add(Me.Panel1)
Me.Controls.Add(Me.StatusStrip1)
Me.Controls.Add(Me.mnuArutiPayroll)
Me.Controls.Add(Me.pnlMainInfo)
Me.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
Me.IsMdiContainer = true
Me.KeyPreview = true
Me.Name = "frmNewMDI"
Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
Me.Text = "Aruti"
Me.WindowState = System.Windows.Forms.FormWindowState.Maximized
Me.tblMDILayout.ResumeLayout(false)
Me.fpnlButtons.ResumeLayout(false)
Me.fpnlAssessmentSetups.ResumeLayout(false)
Me.fpnlLeaveInfo.ResumeLayout(false)
Me.fpnlCommomMasters.ResumeLayout(false)
Me.fpnlTnAInfo.ResumeLayout(false)
Me.fpnlPayrollMasters.ResumeLayout(false)
Me.fpnlAssessment.ResumeLayout(false)
Me.fpnlDiscipline.ResumeLayout(false)
Me.fpnlHRMaster.ResumeLayout(false)
Me.fpnlLoanInfo.ResumeLayout(false)
Me.fpnlRecruitment.ResumeLayout(false)
Me.fpnlEmpData.ResumeLayout(false)
Me.fpnlTrainingInfo.ResumeLayout(false)
Me.fpnlUtility.ResumeLayout(false)
Me.fpnlPayrollTransaction.ResumeLayout(false)
Me.fpnlCoreSetups.ResumeLayout(false)
Me.fpnlBSC.ResumeLayout(false)
Me.fpnlTrainingNeed.ResumeLayout(false)
Me.fpnlTrainingFeedback.ResumeLayout(false)
Me.fpnlAuditTrails.ResumeLayout(false)
Me.fnlWagesSetups.ResumeLayout(false)
Me.fpnlMedicalInfo.ResumeLayout(false)
Me.fpnlPayActivity.ResumeLayout(false)
Me.fpnlAppraisal.ResumeLayout(false)
Me.fpnlClaimsExpenses.ResumeLayout(false)
Me.fpnlPerformanceEval.ResumeLayout(false)
Me.fpnlBudgetInfo.ResumeLayout(false)
Me.fpnlTimesheetInfo.ResumeLayout(false)
Me.objspc1.Panel1.ResumeLayout(false)
Me.objspc1.Panel2.ResumeLayout(false)
Me.objspc1.ResumeLayout(false)
Me.pnlMainInfo.ResumeLayout(false)
Me.mnuArutiPayroll.ResumeLayout(false)
Me.mnuArutiPayroll.PerformLayout
Me.StatusStrip1.ResumeLayout(false)
Me.StatusStrip1.PerformLayout
Me.Panel1.ResumeLayout(false)
Me.ResumeLayout(false)
Me.PerformLayout

End Sub
    Friend WithEvents tblMDILayout As System.Windows.Forms.TableLayoutPanel
    Friend WithEvents pnlMenuItems As System.Windows.Forms.Panel
    Friend WithEvents fpnlCommomMasters As System.Windows.Forms.FlowLayoutPanel
    Friend WithEvents btnBranch As eZee.Common.eZeeLightButton
    Friend WithEvents btnDepartmentGroup As eZee.Common.eZeeLightButton
    Friend WithEvents btnDepartment As eZee.Common.eZeeLightButton
    Friend WithEvents btnSection As eZee.Common.eZeeLightButton
    Friend WithEvents btnUnit As eZee.Common.eZeeLightButton
    Friend WithEvents btnJobGroup As eZee.Common.eZeeLightButton
    Friend WithEvents btnJob As eZee.Common.eZeeLightButton
    Friend WithEvents btnClassGroup As eZee.Common.eZeeLightButton
    Friend WithEvents btnClasses As eZee.Common.eZeeLightButton
    Friend WithEvents btnGradeGroup As eZee.Common.eZeeLightButton
    Friend WithEvents btnGrade As eZee.Common.eZeeLightButton
    Friend WithEvents btnGradeLevel As eZee.Common.eZeeLightButton
    Friend WithEvents pnlSummary As System.Windows.Forms.Panel
    Friend WithEvents pnlMainInfo As System.Windows.Forms.Panel
    Friend WithEvents btnQualification As eZee.Common.eZeeLightButton
    Friend WithEvents btnAdvertiseMaster As eZee.Common.eZeeLightButton
    Friend WithEvents btnSkillMaster As eZee.Common.eZeeLightButton
    Friend WithEvents btnReasons As eZee.Common.eZeeLightButton
    Friend WithEvents btnBenefitPlan As eZee.Common.eZeeLightButton
    Friend WithEvents btnResultCode As eZee.Common.eZeeLightButton
    Friend WithEvents fpnlEmpData As System.Windows.Forms.FlowLayoutPanel
    Friend WithEvents btnEmployeeData2 As eZee.Common.eZeeLightButton
    Friend WithEvents btnDependants2 As eZee.Common.eZeeLightButton
    Friend WithEvents btnBenefitAllocation As eZee.Common.eZeeLightButton
    Friend WithEvents btnEmployeeBenefit As eZee.Common.eZeeLightButton
    Friend WithEvents btnGroupBenefit As eZee.Common.eZeeLightButton
    Friend WithEvents btnReferee As eZee.Common.eZeeLightButton
    Friend WithEvents btnEmployeeAssets As eZee.Common.eZeeLightButton
    Friend WithEvents btnEmployeeSkill As eZee.Common.eZeeLightButton
    Friend WithEvents btnEmployeeQualification As eZee.Common.eZeeLightButton
    Friend WithEvents btnEmployeeExperence As eZee.Common.eZeeLightButton
    Friend WithEvents fpnlDiscipline As System.Windows.Forms.FlowLayoutPanel
    Friend WithEvents btnDisciplineType As eZee.Common.eZeeLightButton
    Friend WithEvents btnDisciplinaryAction As eZee.Common.eZeeLightButton
    Friend WithEvents btnDisciplineStatus As eZee.Common.eZeeLightButton
    Friend WithEvents btnDisciplineFiling As eZee.Common.eZeeLightButton
    Friend WithEvents fpnlAssessment As System.Windows.Forms.FlowLayoutPanel
    Friend WithEvents btnAssessmentPeriod As eZee.Common.eZeeLightButton
    Friend WithEvents btnAssessGroup As eZee.Common.eZeeLightButton
    Friend WithEvents btnAssessItems As eZee.Common.eZeeLightButton
    Friend WithEvents btnAssessor As eZee.Common.eZeeLightButton
    Friend WithEvents btnAssessorAccess As eZee.Common.eZeeLightButton
    Friend WithEvents btnEmployeeAssessment As eZee.Common.eZeeLightButton
    Friend WithEvents fpnlTrainingInfo As System.Windows.Forms.FlowLayoutPanel
    Friend WithEvents btnTrainingInstitute As eZee.Common.eZeeLightButton
    Friend WithEvents btnTrainingScheduling As eZee.Common.eZeeLightButton
    Friend WithEvents btnTrainingEnrollment As eZee.Common.eZeeLightButton
    Friend WithEvents btnTrainingApproverEmployeeMapping As eZee.Common.eZeeLightButton
    Friend WithEvents btnTrainingAttendance As eZee.Common.eZeeLightButton
    Friend WithEvents btnTrainingAnalysis As eZee.Common.eZeeLightButton
    Friend WithEvents fpnlMedicalInfo As System.Windows.Forms.FlowLayoutPanel
    Friend WithEvents btnMedicalMasters As eZee.Common.eZeeLightButton
    Friend WithEvents btnMedicalInstitute As eZee.Common.eZeeLightButton
    Friend WithEvents btnEmployeeInjuries As eZee.Common.eZeeLightButton
    Friend WithEvents btnCategoryAssignment As eZee.Common.eZeeLightButton
    Friend WithEvents btnCoverAssignment As eZee.Common.eZeeLightButton
    Friend WithEvents btnMedicalClaim As eZee.Common.eZeeLightButton
    Friend WithEvents fpnlPayrollMasters As System.Windows.Forms.FlowLayoutPanel

    Friend WithEvents btnPayrollGroup As eZee.Common.eZeeLightButton
    Friend WithEvents btnPayrollPeriods As eZee.Common.eZeeLightButton
    Friend WithEvents btnCostcenter As eZee.Common.eZeeLightButton
    Friend WithEvents btnPayPoint As eZee.Common.eZeeLightButton
    Friend WithEvents objelLine1 As eZee.Common.eZeeLine
    Friend WithEvents btnBankBranch As eZee.Common.eZeeLightButton
    Friend WithEvents btnBankAccountType As eZee.Common.eZeeLightButton
    Friend WithEvents btnBankEDI As eZee.Common.eZeeLightButton
    Friend WithEvents objelLine2 As eZee.Common.eZeeLine
    Friend WithEvents btnCurrency As eZee.Common.eZeeLightButton
    Friend WithEvents btnDemomination As eZee.Common.eZeeLightButton
    Friend WithEvents fpnlPayrollTransaction As System.Windows.Forms.FlowLayoutPanel
    Friend WithEvents btnEmployeeCostCenter As eZee.Common.eZeeLightButton
    Friend WithEvents btnEmployeeBanks As eZee.Common.eZeeLightButton
    Friend WithEvents btnBatchTransaction As eZee.Common.eZeeLightButton
    Friend WithEvents btnTransactionHeads As eZee.Common.eZeeLightButton
    Friend WithEvents btnEarningDeduction As eZee.Common.eZeeLightButton
    Friend WithEvents btnProcessPayroll As eZee.Common.eZeeLightButton
    Friend WithEvents btnPayslip As eZee.Common.eZeeLightButton
    Friend WithEvents btnBudgetPreparation As eZee.Common.eZeeLightButton
    Friend WithEvents btnSalaryIncrement As eZee.Common.eZeeLightButton
    Friend WithEvents btnWagesTable As eZee.Common.eZeeLightButton
    Friend WithEvents objelLine3 As eZee.Common.eZeeLine
    Friend WithEvents btnEmployeeExemption As eZee.Common.eZeeLightButton
    Friend WithEvents btnGlobalAssignED As eZee.Common.eZeeLightButton
    Friend WithEvents btnBatchEntry As eZee.Common.eZeeLightButton
    Friend WithEvents objelLine4 As eZee.Common.eZeeLine
    Friend WithEvents btnClosePayroll As eZee.Common.eZeeLightButton
    Friend WithEvents fpnlLeaveInfo As System.Windows.Forms.FlowLayoutPanel
    Friend WithEvents btnLeaveType As eZee.Common.eZeeLightButton
    Friend WithEvents btnHolidays As eZee.Common.eZeeLightButton
    Friend WithEvents btnEmployeeHolidays As eZee.Common.eZeeLightButton
    Friend WithEvents btnApproverLevel As eZee.Common.eZeeLightButton
    Friend WithEvents btnLeaveApprover As eZee.Common.eZeeLightButton
    Friend WithEvents btnLeaveForm As eZee.Common.eZeeLightButton
    Friend WithEvents btnLeaveProcess As eZee.Common.eZeeLightButton
    Friend WithEvents btnLeaveAccrue As eZee.Common.eZeeLightButton
    Friend WithEvents btnLeaveViewer As eZee.Common.eZeeLightButton
    Friend WithEvents btnLeaveSchedular As eZee.Common.eZeeLightButton
    Friend WithEvents btnLeavePlanner As eZee.Common.eZeeLightButton
    Friend WithEvents fpnlTnAInfo As System.Windows.Forms.FlowLayoutPanel
    Friend WithEvents btnShift As eZee.Common.eZeeLightButton
    Friend WithEvents btnLogin As eZee.Common.eZeeLightButton
    Friend WithEvents btnTimesheet As eZee.Common.eZeeLightButton
    Friend WithEvents btnHoldEmployee As eZee.Common.eZeeLightButton
    Friend WithEvents btnEmployeeAbsent As eZee.Common.eZeeLightButton
    Friend WithEvents btnImportData As eZee.Common.eZeeLightButton
    Friend WithEvents btnExportData As eZee.Common.eZeeLightButton
    Friend WithEvents fpnlLoanInfo As System.Windows.Forms.FlowLayoutPanel
    Friend WithEvents btnLoanScheme As eZee.Common.eZeeLightButton
    Friend WithEvents fpnlRecruitment As System.Windows.Forms.FlowLayoutPanel
    Friend WithEvents btnApplicantMaster As eZee.Common.eZeeLightButton
    Friend WithEvents btnVacancy As eZee.Common.eZeeLightButton
    Friend WithEvents btnBatchScheduling As eZee.Common.eZeeLightButton
    Friend WithEvents btnInterviewScheduling As eZee.Common.eZeeLightButton
    Friend WithEvents btnInterviewAnalysis As eZee.Common.eZeeLightButton
    Friend WithEvents btnFinalApplicants As eZee.Common.eZeeLightButton
    Friend WithEvents fpnlUtility As System.Windows.Forms.FlowLayoutPanel
    Friend WithEvents btnLetterType As eZee.Common.eZeeLightButton
    Friend WithEvents btnLetterTemplate As eZee.Common.eZeeLightButton
    Friend WithEvents btnMailbox As eZee.Common.eZeeLightButton
    Friend WithEvents objelLine6 As eZee.Common.eZeeLine
    Friend WithEvents btnReminder As eZee.Common.eZeeLightButton
    Friend WithEvents btnBackup As eZee.Common.eZeeLightButton
    Friend WithEvents btnRestore As eZee.Common.eZeeLightButton
    Friend WithEvents objelLine7 As eZee.Common.eZeeLine
    Friend WithEvents btnChangeUser As eZee.Common.eZeeLightButton
    Friend WithEvents btnChangeCompany As eZee.Common.eZeeLightButton
    Friend WithEvents btnChangeDatabase As eZee.Common.eZeeLightButton
    Friend WithEvents fpnlHRMaster As System.Windows.Forms.FlowLayoutPanel
    Friend WithEvents btnGeneralSettings As eZee.Common.eZeeLightButton
    Friend WithEvents objspc1 As System.Windows.Forms.SplitContainer
    Friend WithEvents objlblCaption As System.Windows.Forms.Label
    Friend WithEvents btnPayslipMessage As eZee.Common.eZeeLightButton
    Friend WithEvents btnGlobalPayslipMessage As eZee.Common.eZeeLightButton
    Friend WithEvents btnCompanyJV As eZee.Common.eZeeLightButton
    Friend WithEvents objelLine8 As eZee.Common.eZeeLine
    Friend WithEvents btnAccount As eZee.Common.eZeeLightButton
    Friend WithEvents btnAccountConfiguration As eZee.Common.eZeeLightButton
    Friend WithEvents FlowLayoutPanel5 As System.Windows.Forms.FlowLayoutPanel
    Friend WithEvents btnEmployeeMovement2 As eZee.Common.eZeeLightButton
    Friend WithEvents btnEmployeeDiary As eZee.Common.eZeeLightButton
    Friend WithEvents btnCashDenomination As eZee.Common.eZeeLightButton
    Friend WithEvents mnuArutiPayroll As System.Windows.Forms.MenuStrip
    Friend WithEvents mnuGeneralMaster As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents mnuRecruitment As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents mnuHumanResource As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents mnuPersonnel As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents mnuHRMaster As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents mnuEmployeeData As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents mnuMedical As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents mnuPayroll As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents mnuPayrollMasters As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents mnuPayrollTransactions As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents mnuLoan_Advance_Savings As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents mnuLeaveAndTnA As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents mnuLeaveInformation As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents mnuTnAInfo As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents mnuUtilitiesMain As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents mnuView As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents mnuPayrollView As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents mnuReportView As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents mnuExit As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents mnuRecruitmentItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents mnuHelp As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents mnuProduct As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents mnuAbout As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents btnAssessorAssessmentlist As eZee.Common.eZeeLightButton
    Friend WithEvents objelLine9 As eZee.Common.eZeeLine
    Friend WithEvents btnCommonImportData As eZee.Common.eZeeLightButton
    Friend WithEvents btnCommonExportData As eZee.Common.eZeeLightButton
    Friend WithEvents StatusStrip1 As System.Windows.Forms.StatusStrip
    Friend WithEvents lblPropertyName As System.Windows.Forms.ToolStripStatusLabel
    Friend WithEvents objbtnPropertyName As System.Windows.Forms.ToolStripStatusLabel
    Friend WithEvents objlblSep1 As System.Windows.Forms.ToolStripStatusLabel
    Friend WithEvents lblPayrollYear As System.Windows.Forms.ToolStripStatusLabel
    Friend WithEvents objbtnYearName As System.Windows.Forms.ToolStripStatusLabel
    Friend WithEvents objlblSep2 As System.Windows.Forms.ToolStripStatusLabel
    Friend WithEvents lblUserName As System.Windows.Forms.ToolStripStatusLabel
    Friend WithEvents objbtnUserName As System.Windows.Forms.ToolStripStatusLabel
    Friend WithEvents objlblSep3 As System.Windows.Forms.ToolStripStatusLabel
    Friend WithEvents lblWorkingDate As System.Windows.Forms.ToolStripStatusLabel
    Friend WithEvents objbtnWorkingDate As System.Windows.Forms.ToolStripStatusLabel
    Friend WithEvents objlblSep4 As System.Windows.Forms.ToolStripStatusLabel
    Friend WithEvents mnuDiscipline As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents mnuAssessment As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents mnuTrainingInformation As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents mnuCommonSetups As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents mnuCoreSetups As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents fpnlCoreSetups As System.Windows.Forms.FlowLayoutPanel
    Friend WithEvents btnCommonMaster As eZee.Common.eZeeLightButton
    Friend WithEvents btnMemebership As eZee.Common.eZeeLightButton
    Friend WithEvents btnState As eZee.Common.eZeeLightButton
    Friend WithEvents btnCity As eZee.Common.eZeeLightButton
    Friend WithEvents btnZipcode As eZee.Common.eZeeLightButton
    Friend WithEvents btnEmployeeData As eZee.Common.eZeeLightButton
    Friend WithEvents btnDependants As eZee.Common.eZeeLightButton
    Friend WithEvents btnEmployeeMovement As eZee.Common.eZeeLightButton
    Friend WithEvents tmrReminder As System.Windows.Forms.Timer
    Friend WithEvents btnImportAttendancedata As eZee.Common.eZeeLightButton
    Friend WithEvents btnGroupAttendance As eZee.Common.eZeeLightButton
    Friend WithEvents btnImportAccrueLeave As eZee.Common.eZeeLightButton
    Friend WithEvents btnUserLog As eZee.Common.eZeeLightButton
    Friend WithEvents btnChangePwd As eZee.Common.eZeeLightButton
    Friend WithEvents btnEmpAccConfig As eZee.Common.eZeeLightButton
    Friend WithEvents btnCCAccConfig As eZee.Common.eZeeLightButton
    Friend WithEvents mnuRegister As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents btnSectionGroup As eZee.Common.eZeeLightButton
    Friend WithEvents btnUnitGroup As eZee.Common.eZeeLightButton
    Friend WithEvents btnTeam As eZee.Common.eZeeLightButton
    Friend WithEvents btnExporttoWeb As eZee.Common.eZeeLightButton
    Friend WithEvents btnImportfromWeb As eZee.Common.eZeeLightButton
    Friend WithEvents btnApplicantFilter As eZee.Common.eZeeLightButton
    Friend WithEvents btnFinalShortListedApplicant As eZee.Common.eZeeLightButton
    Friend WithEvents btnResolutionSteps As eZee.Common.eZeeLightButton
    Friend WithEvents btnSickSheet As eZee.Common.eZeeLightButton
    Friend WithEvents dtpEmployeeAsOnDate As System.Windows.Forms.DateTimePicker
    Friend WithEvents Panel1 As System.Windows.Forms.Panel
    Friend WithEvents lblEmployeeAsOnDate As System.Windows.Forms.Label
    Friend WithEvents mnuWagesSetup As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents fnlWagesSetups As System.Windows.Forms.FlowLayoutPanel
    Friend WithEvents mnuGeneralAssessment As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents mnuBalancedScoreCard As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents btnAssessSubItem As eZee.Common.eZeeLightButton
    Friend WithEvents fpnlBSC As System.Windows.Forms.FlowLayoutPanel
    Friend WithEvents btnObjectiveList As eZee.Common.eZeeLightButton
    Friend WithEvents btnKPIMeasureList As eZee.Common.eZeeLightButton
    Friend WithEvents btnTargetsList As eZee.Common.eZeeLightButton
    Friend WithEvents btnInitiativeList As eZee.Common.eZeeLightButton
    Friend WithEvents btnReviewer As eZee.Common.eZeeLightButton
    Friend WithEvents btnReviewerAssessment As eZee.Common.eZeeLightButton
    Friend WithEvents btnSelfAssessedBSC As eZee.Common.eZeeLightButton
    Friend WithEvents btnAssessorBSCList As eZee.Common.eZeeLightButton
    Friend WithEvents btnReviewerBSCList As eZee.Common.eZeeLightButton
    Friend WithEvents btnExternalAssessor As eZee.Common.eZeeLightButton
    Friend WithEvents fpnlAssessmentSetups As System.Windows.Forms.FlowLayoutPanel
    Friend WithEvents mnuSetups As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents mnuAppraisal As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents fpnlAppraisal As System.Windows.Forms.FlowLayoutPanel
    Friend WithEvents btnShortListEmployee As eZee.Common.eZeeLightButton
    Friend WithEvents btnFinalShortListedEmployee As eZee.Common.eZeeLightButton
    Friend WithEvents btnTrainingPriority As eZee.Common.eZeeLightButton
    Friend WithEvents btnFinalizingTrainingPriority As eZee.Common.eZeeLightButton
    Friend WithEvents mnuTrainingEvaluation As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents fpnlTrainingNeed As System.Windows.Forms.FlowLayoutPanel
    Friend WithEvents mnuJobTraining As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents btnScanAttachments As eZee.Common.eZeeLightButton
    Friend WithEvents mnuAssetDeclaration As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents mnuTrainingFeedback As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents fpnlTrainingFeedback As System.Windows.Forms.FlowLayoutPanel
    Friend WithEvents btnFeedbackGroup As eZee.Common.eZeeLightButton
    Friend WithEvents btnFeedbackItems As eZee.Common.eZeeLightButton
    Friend WithEvents btnFeedbackSubItems As eZee.Common.eZeeLightButton
    Friend WithEvents btnFeedback As eZee.Common.eZeeLightButton
    Friend WithEvents objelLine10 As eZee.Common.eZeeLine
    Friend WithEvents btnEvaluationLevel3 As eZee.Common.eZeeLightButton
    Friend WithEvents btnImpactItems As eZee.Common.eZeeLightButton
    Friend WithEvents btnCommittee As eZee.Common.eZeeLightButton
    Friend WithEvents btnMigration As eZee.Common.eZeeLightButton
    Friend WithEvents BtnTransferApprover As eZee.Common.eZeeLightButton
    Friend WithEvents mnuCustomReporting As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents fpnlButtons As System.Windows.Forms.FlowLayoutPanel
    Friend WithEvents fpnlAuditTrails As System.Windows.Forms.FlowLayoutPanel
    Friend WithEvents btnApplicationEventLog As eZee.Common.eZeeLightButton
    Friend WithEvents btnUserAttempts As eZee.Common.eZeeLightButton
    Friend WithEvents btnUserAuthenticationLog As eZee.Common.eZeeLightButton
    Friend WithEvents btnLogView As eZee.Common.eZeeLightButton
    Friend WithEvents mnuAuditTrail As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents mnuUtilities As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents mnuAuditTrails As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents btnAllocationMapping As eZee.Common.eZeeLightButton
    Friend WithEvents btnAutoImportFromWeb As eZee.Common.eZeeLightButton
    Friend WithEvents btnBatchPosting As eZee.Common.eZeeLightButton
    Friend WithEvents btnWeightedSetting As eZee.Common.eZeeLightButton
    Friend WithEvents btnCloseAssessmentPeriod As eZee.Common.eZeeLightButton
    Friend WithEvents objLine11 As eZee.Common.eZeeLine
    Friend WithEvents btnPayAppLevel As eZee.Common.eZeeLightButton
    Friend WithEvents btnPayApproverMap As eZee.Common.eZeeLightButton
    Friend WithEvents btnEndELC As eZee.Common.eZeeLightButton
    Friend WithEvents mnuArutiHelp As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents btnDependantException As eZee.Common.eZeeLightButton
    Friend WithEvents mnuPayActivity As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents fpnlPayActivity As System.Windows.Forms.FlowLayoutPanel
    Friend WithEvents btnActivity_List As eZee.Common.eZeeLightButton
    Friend WithEvents btnPayPerActivity As eZee.Common.eZeeLightButton
    Friend WithEvents btnMeasureList As eZee.Common.eZeeLightButton
    Friend WithEvents btnActivityRate As eZee.Common.eZeeLightButton
    Friend WithEvents btnPayrollPosting As eZee.Common.eZeeLightButton
    Friend WithEvents btnActivityGlobalAssign As eZee.Common.eZeeLightButton
    Friend WithEvents btnAssessmentRatio As eZee.Common.eZeeLightButton
    Friend WithEvents btnAppraisalSetups As eZee.Common.eZeeLightButton
    Friend WithEvents btnPolicyList As eZee.Common.eZeeLightButton
    Friend WithEvents btnEditTimeSheet As eZee.Common.eZeeLightButton
    Friend WithEvents btnRecalculateTiming As eZee.Common.eZeeLightButton
    Friend WithEvents btnLeaveFrequency As eZee.Common.eZeeLightButton
    Friend WithEvents btnDeviceUserMapping As eZee.Common.eZeeLightButton
    Friend WithEvents btnRoundOff As eZee.Common.eZeeLightButton
    Friend WithEvents btnGlobalEditDelete As eZee.Common.eZeeLightButton
    Friend WithEvents btnEmailLogView As eZee.Common.eZeeLightButton
    Friend WithEvents btnStaffReqAppLevel As eZee.Common.eZeeLightButton
    Friend WithEvents btnStaffReqAppMapping As eZee.Common.eZeeLightButton
    Friend WithEvents btnStaffRequisition As eZee.Common.eZeeLightButton
    Friend WithEvents btnRemovePPALogs As eZee.Common.eZeeLightButton
    Friend WithEvents mnuClaimsExpenses As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents fpnlClaimsExpenses As System.Windows.Forms.FlowLayoutPanel
    Friend WithEvents btnExpensesList As eZee.Common.eZeeLightButton
    Friend WithEvents btnCostingList As eZee.Common.eZeeLightButton
    Friend WithEvents btnExpenseAssignment As eZee.Common.eZeeLightButton
    Friend WithEvents btnExApprovalLevel As eZee.Common.eZeeLightButton
    Friend WithEvents btnExApprovers As eZee.Common.eZeeLightButton
    Friend WithEvents btnExApprMigration As eZee.Common.eZeeLightButton
    Friend WithEvents btnClaims_Request As eZee.Common.eZeeLightButton
    Friend WithEvents btnExpenseApproval As eZee.Common.eZeeLightButton
    Friend WithEvents btnPostToPayroll As eZee.Common.eZeeLightButton
    Friend WithEvents btnAssessCaptionSettings As eZee.Common.eZeeLightButton
    Friend WithEvents btnAssessScale As eZee.Common.eZeeLightButton
    Friend WithEvents btnAssessCompanyGoals As eZee.Common.eZeeLightButton
    Friend WithEvents btnAllocationGoals As eZee.Common.eZeeLightButton
    Friend WithEvents btnEmployeeGoals As eZee.Common.eZeeLightButton
    Friend WithEvents btnAssessmentMapping As eZee.Common.eZeeLightButton
    Friend WithEvents btnCompetencies As eZee.Common.eZeeLightButton
    Friend WithEvents btnAssignCompetencies As eZee.Common.eZeeLightButton
    Friend WithEvents btnCustomHeaders As eZee.Common.eZeeLightButton
    Friend WithEvents btnCustomItems As eZee.Common.eZeeLightButton
    Friend WithEvents mnuPerformaceEvaluation As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents fpnlPerformanceEval As System.Windows.Forms.FlowLayoutPanel
    Friend WithEvents btnSelfPEvaluation As eZee.Common.eZeeLightButton
    Friend WithEvents btnAssessorPEvaluation As eZee.Common.eZeeLightButton
    Friend WithEvents btnReviewerPEvaluation As eZee.Common.eZeeLightButton
    Friend WithEvents btnBSCViewSetting As eZee.Common.eZeeLightButton
    Friend WithEvents btnOpenPeriodicReview As eZee.Common.eZeeLightButton
    Friend WithEvents btnPerspective As eZee.Common.eZeeLightButton
    Friend WithEvents btnComputationFormula As eZee.Common.eZeeLightButton
    Friend WithEvents btnPerformancePlan As eZee.Common.eZeeLightButton
    Friend WithEvents btnLeaveAdjustment As eZee.Common.eZeeLightButton
    Friend WithEvents btnLVSwapApprover As eZee.Common.eZeeLightButton
    Friend WithEvents btnClaimSwapApprover As eZee.Common.eZeeLightButton
    Friend WithEvents btnGlobalVoidAssessment As eZee.Common.eZeeLightButton
    Friend WithEvents btnLoanApproverLevel As eZee.Common.eZeeLightButton
    Friend WithEvents btnLoanApprover As eZee.Common.eZeeLightButton
    Friend WithEvents btnLoanApplication As eZee.Common.eZeeLightButton
    Friend WithEvents btnFlexcubeLoanApplication As eZee.Common.eZeeLightButton
    Friend WithEvents btnProcessPendingLoan As eZee.Common.eZeeLightButton
    Friend WithEvents btnLoanAdvance As eZee.Common.eZeeLightButton
    Friend WithEvents btnImportLA As eZee.Common.eZeeLightButton
    Friend WithEvents btnLoanApproverMigration As eZee.Common.eZeeLightButton
    Friend WithEvents btnLoanSwapApprover As eZee.Common.eZeeLightButton
    Friend WithEvents btnLoanSchemeCategoryMapping As eZee.Common.eZeeLightButton
    Friend WithEvents objelLine5 As eZee.Common.eZeeLine
    Friend WithEvents btnImportSaving As eZee.Common.eZeeLightButton
    Friend WithEvents btnSavingScheme As eZee.Common.eZeeLightButton
    Friend WithEvents btnEmployeeSavings As eZee.Common.eZeeLightButton
    Friend WithEvents btnSecRouteAssignment As eZee.Common.eZeeLightButton
    Friend WithEvents btnComputeProcess As eZee.Common.eZeeLightButton
    Friend WithEvents mnuBudget As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents fpnlBudgetInfo As System.Windows.Forms.FlowLayoutPanel
    Friend WithEvents btnFundSources As eZee.Common.eZeeLightButton
    Friend WithEvents btnProjectCodeAdjustments As eZee.Common.eZeeLightButton
    Friend WithEvents btnBudgetFormula As eZee.Common.eZeeLightButton
    Friend WithEvents btnBudget As eZee.Common.eZeeLightButton
    Friend WithEvents btnFundActivity As eZee.Common.eZeeLightButton
    Friend WithEvents btnHearingList As eZee.Common.eZeeLightButton
    Friend WithEvents btnFundActivityAdjustment As eZee.Common.eZeeLightButton
    Friend WithEvents btnBudgetCodes As eZee.Common.eZeeLightButton
    Friend WithEvents btnFundProjectCode As eZee.Common.eZeeLightButton
    Friend WithEvents btnBgtApproverLevel As eZee.Common.eZeeLightButton
    Friend WithEvents btnBgtApproverMapping As eZee.Common.eZeeLightButton
    Friend WithEvents fpnlTimesheetInfo As System.Windows.Forms.FlowLayoutPanel
    Friend WithEvents btnTimesheetApproverLevel As eZee.Common.eZeeLightButton
    Friend WithEvents btnExportPrintEmplForm As eZee.Common.eZeeLightButton
    Friend WithEvents btnTimesheetApproverMst As eZee.Common.eZeeLightButton
    Friend WithEvents btnEmployeeTimesheet As eZee.Common.eZeeLightButton
    Friend WithEvents btnTimesheetApproval As eZee.Common.eZeeLightButton
    Friend WithEvents btnTimesheetApproverMigration As eZee.Common.eZeeLightButton
    Friend WithEvents mnuEmpBudgetTimesheet As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents btnSkillExpertise As eZee.Common.eZeeLightButton
    Friend WithEvents btnEmpApproverLevel As eZee.Common.eZeeLightButton
    Friend WithEvents btnEmployeeApprover As eZee.Common.eZeeLightButton
    Friend WithEvents btnApproveEmployee As eZee.Common.eZeeLightButton
    Friend WithEvents btnEmployeeDataApproval As eZee.Common.eZeeLightButton
    Friend WithEvents btnSystemErrorLog As eZee.Common.eZeeLightButton
    Friend WithEvents btnCountry As eZee.Common.eZeeLightButton
    Friend WithEvents objelLine11 As eZee.Common.eZeeLine
    Friend WithEvents btnMapOrbitParams As eZee.Common.eZeeLightButton
    Friend WithEvents btnGlobalInterviewAnalysis As eZee.Common.eZeeLightButton
    Friend WithEvents btnReportLanguage As eZee.Common.eZeeLightButton
    Friend WithEvents btnTransferStaffRequisitionApprover As eZee.Common.eZeeLightButton
    Friend WithEvents btnCandidateFeedback As eZee.Common.eZeeLightButton
    Friend WithEvents btnDownloadAptitudeResult As eZee.Common.eZeeLightButton
    Friend WithEvents mnuTroubleshoot As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents btnGarnishees As eZee.Common.eZeeLightButton
End Class
