﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmMembership_AddEdit
    Inherits eZee.Common.eZeeForm

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmMembership_AddEdit))
        Me.pnlMembershipInfo = New System.Windows.Forms.Panel
        Me.gbMembership = New eZee.Common.eZeeCollapsibleContainer(Me.components)
        Me.chkShowBasicSalaryOnStatutoryReport = New System.Windows.Forms.CheckBox
        Me.txtEmployerMemNo = New eZee.TextBox.AlphanumericTextBox
        Me.lblEmployerMemNo = New System.Windows.Forms.Label
        Me.chkAppearOnPayslip = New System.Windows.Forms.CheckBox
        Me.Label1 = New System.Windows.Forms.Label
        Me.lblTransactionHead = New System.Windows.Forms.Label
        Me.cboCTranHead = New System.Windows.Forms.ComboBox
        Me.cboETranHead = New System.Windows.Forms.ComboBox
        Me.objbtnAddCategory = New eZee.Common.eZeeGradientButton
        Me.objbtnOtherLanguage = New eZee.Common.eZeeGradientButton
        Me.txtDescription = New eZee.TextBox.AlphanumericTextBox
        Me.lblDescription = New System.Windows.Forms.Label
        Me.txtName = New eZee.TextBox.AlphanumericTextBox
        Me.lblName = New System.Windows.Forms.Label
        Me.txtCode = New eZee.TextBox.AlphanumericTextBox
        Me.lblCode = New System.Windows.Forms.Label
        Me.cboCategory = New System.Windows.Forms.ComboBox
        Me.lblCategory = New System.Windows.Forms.Label
        Me.objFooter = New eZee.Common.eZeeFooter
        Me.txtMappedInfo = New System.Windows.Forms.TextBox
        Me.btnClose = New eZee.Common.eZeeLightButton(Me.components)
        Me.btnSave = New eZee.Common.eZeeLightButton(Me.components)
        Me.pnlMembershipInfo.SuspendLayout()
        Me.gbMembership.SuspendLayout()
        Me.objFooter.SuspendLayout()
        Me.SuspendLayout()
        '
        'pnlMembershipInfo
        '
        Me.pnlMembershipInfo.Controls.Add(Me.gbMembership)
        Me.pnlMembershipInfo.Controls.Add(Me.objFooter)
        Me.pnlMembershipInfo.Dock = System.Windows.Forms.DockStyle.Fill
        Me.pnlMembershipInfo.Location = New System.Drawing.Point(0, 0)
        Me.pnlMembershipInfo.Name = "pnlMembershipInfo"
        Me.pnlMembershipInfo.Size = New System.Drawing.Size(439, 370)
        Me.pnlMembershipInfo.TabIndex = 0
        '
        'gbMembership
        '
        Me.gbMembership.BorderColor = System.Drawing.Color.Black
        Me.gbMembership.Checked = False
        Me.gbMembership.CollapseAllExceptThis = False
        Me.gbMembership.CollapsedHoverImage = Nothing
        Me.gbMembership.CollapsedNormalImage = Nothing
        Me.gbMembership.CollapsedPressedImage = Nothing
        Me.gbMembership.CollapseOnLoad = False
        Me.gbMembership.Controls.Add(Me.chkShowBasicSalaryOnStatutoryReport)
        Me.gbMembership.Controls.Add(Me.txtEmployerMemNo)
        Me.gbMembership.Controls.Add(Me.lblEmployerMemNo)
        Me.gbMembership.Controls.Add(Me.chkAppearOnPayslip)
        Me.gbMembership.Controls.Add(Me.Label1)
        Me.gbMembership.Controls.Add(Me.lblTransactionHead)
        Me.gbMembership.Controls.Add(Me.cboCTranHead)
        Me.gbMembership.Controls.Add(Me.cboETranHead)
        Me.gbMembership.Controls.Add(Me.objbtnAddCategory)
        Me.gbMembership.Controls.Add(Me.objbtnOtherLanguage)
        Me.gbMembership.Controls.Add(Me.txtDescription)
        Me.gbMembership.Controls.Add(Me.lblDescription)
        Me.gbMembership.Controls.Add(Me.txtName)
        Me.gbMembership.Controls.Add(Me.lblName)
        Me.gbMembership.Controls.Add(Me.txtCode)
        Me.gbMembership.Controls.Add(Me.lblCode)
        Me.gbMembership.Controls.Add(Me.cboCategory)
        Me.gbMembership.Controls.Add(Me.lblCategory)
        Me.gbMembership.ExpandedHoverImage = Nothing
        Me.gbMembership.ExpandedNormalImage = Nothing
        Me.gbMembership.ExpandedPressedImage = Nothing
        Me.gbMembership.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.gbMembership.GradientColor = System.Drawing.SystemColors.ButtonFace
        Me.gbMembership.HeaderHeight = 25
        Me.gbMembership.HeaderMessage = ""
        Me.gbMembership.HeaderMessageFont = New System.Drawing.Font("Tahoma", 8.25!)
        Me.gbMembership.HeaderMessageForeColor = System.Drawing.Color.Black
        Me.gbMembership.HeightOnCollapse = 0
        Me.gbMembership.LeftTextSpace = 0
        Me.gbMembership.Location = New System.Drawing.Point(12, 12)
        Me.gbMembership.Name = "gbMembership"
        Me.gbMembership.OpenHeight = 300
        Me.gbMembership.PanelBorderStyle = System.Windows.Forms.ButtonBorderStyle.Solid
        Me.gbMembership.ShowBorder = True
        Me.gbMembership.ShowCheckBox = False
        Me.gbMembership.ShowCollapseButton = False
        Me.gbMembership.ShowDefaultBorderColor = True
        Me.gbMembership.ShowDownButton = False
        Me.gbMembership.ShowHeader = True
        Me.gbMembership.Size = New System.Drawing.Size(415, 297)
        Me.gbMembership.TabIndex = 2
        Me.gbMembership.Temp = 0
        Me.gbMembership.Text = "Membership"
        Me.gbMembership.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'chkShowBasicSalaryOnStatutoryReport
        '
        Me.chkShowBasicSalaryOnStatutoryReport.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.chkShowBasicSalaryOnStatutoryReport.Location = New System.Drawing.Point(144, 272)
        Me.chkShowBasicSalaryOnStatutoryReport.Name = "chkShowBasicSalaryOnStatutoryReport"
        Me.chkShowBasicSalaryOnStatutoryReport.Size = New System.Drawing.Size(226, 17)
        Me.chkShowBasicSalaryOnStatutoryReport.TabIndex = 229
        Me.chkShowBasicSalaryOnStatutoryReport.Text = "Show Basic Salary On Statutory Report"
        Me.chkShowBasicSalaryOnStatutoryReport.UseVisualStyleBackColor = True
        '
        'txtEmployerMemNo
        '
        Me.txtEmployerMemNo.Flags = 0
        Me.txtEmployerMemNo.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtEmployerMemNo.InvalidChars = New Char() {Global.Microsoft.VisualBasic.ChrW(37), Global.Microsoft.VisualBasic.ChrW(39), Global.Microsoft.VisualBasic.ChrW(42), Global.Microsoft.VisualBasic.ChrW(34), Global.Microsoft.VisualBasic.ChrW(43), Global.Microsoft.VisualBasic.ChrW(63), Global.Microsoft.VisualBasic.ChrW(62), Global.Microsoft.VisualBasic.ChrW(60), Global.Microsoft.VisualBasic.ChrW(58), Global.Microsoft.VisualBasic.ChrW(92)}
        Me.txtEmployerMemNo.Location = New System.Drawing.Point(144, 169)
        Me.txtEmployerMemNo.Name = "txtEmployerMemNo"
        Me.txtEmployerMemNo.Size = New System.Drawing.Size(231, 21)
        Me.txtEmployerMemNo.TabIndex = 227
        '
        'lblEmployerMemNo
        '
        Me.lblEmployerMemNo.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblEmployerMemNo.Location = New System.Drawing.Point(8, 171)
        Me.lblEmployerMemNo.Name = "lblEmployerMemNo"
        Me.lblEmployerMemNo.Size = New System.Drawing.Size(130, 13)
        Me.lblEmployerMemNo.TabIndex = 226
        Me.lblEmployerMemNo.Text = "Employer Memb. Number"
        Me.lblEmployerMemNo.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'chkAppearOnPayslip
        '
        Me.chkAppearOnPayslip.Checked = True
        Me.chkAppearOnPayslip.CheckState = System.Windows.Forms.CheckState.Checked
        Me.chkAppearOnPayslip.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.chkAppearOnPayslip.Location = New System.Drawing.Point(144, 249)
        Me.chkAppearOnPayslip.Name = "chkAppearOnPayslip"
        Me.chkAppearOnPayslip.Size = New System.Drawing.Size(231, 17)
        Me.chkAppearOnPayslip.TabIndex = 223
        Me.chkAppearOnPayslip.Text = "Appear On Payslip"
        Me.chkAppearOnPayslip.UseVisualStyleBackColor = True
        '
        'Label1
        '
        Me.Label1.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label1.Location = New System.Drawing.Point(8, 145)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(130, 13)
        Me.Label1.TabIndex = 221
        Me.Label1.Text = "Co. Contribution"
        Me.Label1.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'lblTransactionHead
        '
        Me.lblTransactionHead.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblTransactionHead.Location = New System.Drawing.Point(8, 118)
        Me.lblTransactionHead.Name = "lblTransactionHead"
        Me.lblTransactionHead.Size = New System.Drawing.Size(130, 13)
        Me.lblTransactionHead.TabIndex = 219
        Me.lblTransactionHead.Text = "Emp. Contribution"
        Me.lblTransactionHead.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'cboCTranHead
        '
        Me.cboCTranHead.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboCTranHead.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboCTranHead.FormattingEnabled = True
        Me.cboCTranHead.Location = New System.Drawing.Point(144, 142)
        Me.cboCTranHead.Name = "cboCTranHead"
        Me.cboCTranHead.Size = New System.Drawing.Size(231, 21)
        Me.cboCTranHead.TabIndex = 217
        '
        'cboETranHead
        '
        Me.cboETranHead.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboETranHead.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboETranHead.FormattingEnabled = True
        Me.cboETranHead.Location = New System.Drawing.Point(144, 115)
        Me.cboETranHead.Name = "cboETranHead"
        Me.cboETranHead.Size = New System.Drawing.Size(231, 21)
        Me.cboETranHead.TabIndex = 217
        '
        'objbtnAddCategory
        '
        Me.objbtnAddCategory.BackColor = System.Drawing.Color.Transparent
        Me.objbtnAddCategory.BackColor1 = System.Drawing.Color.Transparent
        Me.objbtnAddCategory.BackColor2 = System.Drawing.Color.Transparent
        Me.objbtnAddCategory.BorderNormalColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.objbtnAddCategory.BorderSelected = False
        Me.objbtnAddCategory.DialogResult = System.Windows.Forms.DialogResult.None
        Me.objbtnAddCategory.Image = Global.Aruti.Main.My.Resources.Resources.Mini_Add
        Me.objbtnAddCategory.ImageAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.objbtnAddCategory.Location = New System.Drawing.Point(295, 34)
        Me.objbtnAddCategory.Name = "objbtnAddCategory"
        Me.objbtnAddCategory.Size = New System.Drawing.Size(21, 21)
        Me.objbtnAddCategory.TabIndex = 215
        '
        'objbtnOtherLanguage
        '
        Me.objbtnOtherLanguage.BackColor = System.Drawing.Color.Transparent
        Me.objbtnOtherLanguage.BackColor1 = System.Drawing.Color.Transparent
        Me.objbtnOtherLanguage.BackColor2 = System.Drawing.Color.Transparent
        Me.objbtnOtherLanguage.BorderNormalColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.objbtnOtherLanguage.BorderSelected = False
        Me.objbtnOtherLanguage.DialogResult = System.Windows.Forms.DialogResult.None
        Me.objbtnOtherLanguage.Image = Global.Aruti.Main.My.Resources.Resources.OtherLanguage_16
        Me.objbtnOtherLanguage.ImageAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.objbtnOtherLanguage.Location = New System.Drawing.Point(381, 88)
        Me.objbtnOtherLanguage.Name = "objbtnOtherLanguage"
        Me.objbtnOtherLanguage.Size = New System.Drawing.Size(21, 21)
        Me.objbtnOtherLanguage.TabIndex = 4
        '
        'txtDescription
        '
        Me.txtDescription.Flags = 0
        Me.txtDescription.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtDescription.InvalidChars = New Char() {Global.Microsoft.VisualBasic.ChrW(37), Global.Microsoft.VisualBasic.ChrW(39), Global.Microsoft.VisualBasic.ChrW(42), Global.Microsoft.VisualBasic.ChrW(34), Global.Microsoft.VisualBasic.ChrW(43), Global.Microsoft.VisualBasic.ChrW(63), Global.Microsoft.VisualBasic.ChrW(62), Global.Microsoft.VisualBasic.ChrW(60), Global.Microsoft.VisualBasic.ChrW(58), Global.Microsoft.VisualBasic.ChrW(92)}
        Me.txtDescription.Location = New System.Drawing.Point(144, 196)
        Me.txtDescription.Multiline = True
        Me.txtDescription.Name = "txtDescription"
        Me.txtDescription.ScrollBars = System.Windows.Forms.ScrollBars.Vertical
        Me.txtDescription.Size = New System.Drawing.Size(231, 47)
        Me.txtDescription.TabIndex = 5
        '
        'lblDescription
        '
        Me.lblDescription.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblDescription.Location = New System.Drawing.Point(8, 197)
        Me.lblDescription.Name = "lblDescription"
        Me.lblDescription.Size = New System.Drawing.Size(130, 13)
        Me.lblDescription.TabIndex = 99
        Me.lblDescription.Text = "Description"
        Me.lblDescription.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'txtName
        '
        Me.txtName.Flags = 0
        Me.txtName.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtName.InvalidChars = New Char() {Global.Microsoft.VisualBasic.ChrW(37), Global.Microsoft.VisualBasic.ChrW(39), Global.Microsoft.VisualBasic.ChrW(42), Global.Microsoft.VisualBasic.ChrW(34), Global.Microsoft.VisualBasic.ChrW(43), Global.Microsoft.VisualBasic.ChrW(63), Global.Microsoft.VisualBasic.ChrW(62), Global.Microsoft.VisualBasic.ChrW(60), Global.Microsoft.VisualBasic.ChrW(58), Global.Microsoft.VisualBasic.ChrW(92)}
        Me.txtName.Location = New System.Drawing.Point(144, 88)
        Me.txtName.Name = "txtName"
        Me.txtName.Size = New System.Drawing.Size(231, 21)
        Me.txtName.TabIndex = 3
        '
        'lblName
        '
        Me.lblName.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblName.Location = New System.Drawing.Point(8, 91)
        Me.lblName.Name = "lblName"
        Me.lblName.Size = New System.Drawing.Size(130, 13)
        Me.lblName.TabIndex = 97
        Me.lblName.Text = "Name"
        Me.lblName.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'txtCode
        '
        Me.txtCode.Flags = 0
        Me.txtCode.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtCode.InvalidChars = New Char() {Global.Microsoft.VisualBasic.ChrW(37), Global.Microsoft.VisualBasic.ChrW(39), Global.Microsoft.VisualBasic.ChrW(42), Global.Microsoft.VisualBasic.ChrW(34), Global.Microsoft.VisualBasic.ChrW(43), Global.Microsoft.VisualBasic.ChrW(63), Global.Microsoft.VisualBasic.ChrW(62), Global.Microsoft.VisualBasic.ChrW(60), Global.Microsoft.VisualBasic.ChrW(58), Global.Microsoft.VisualBasic.ChrW(92)}
        Me.txtCode.Location = New System.Drawing.Point(144, 61)
        Me.txtCode.Name = "txtCode"
        Me.txtCode.Size = New System.Drawing.Size(145, 21)
        Me.txtCode.TabIndex = 2
        '
        'lblCode
        '
        Me.lblCode.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblCode.Location = New System.Drawing.Point(8, 64)
        Me.lblCode.Name = "lblCode"
        Me.lblCode.Size = New System.Drawing.Size(130, 13)
        Me.lblCode.TabIndex = 95
        Me.lblCode.Text = "Code"
        Me.lblCode.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'cboCategory
        '
        Me.cboCategory.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboCategory.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboCategory.FormattingEnabled = True
        Me.cboCategory.Location = New System.Drawing.Point(144, 34)
        Me.cboCategory.Name = "cboCategory"
        Me.cboCategory.Size = New System.Drawing.Size(145, 21)
        Me.cboCategory.TabIndex = 1
        '
        'lblCategory
        '
        Me.lblCategory.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblCategory.Location = New System.Drawing.Point(8, 37)
        Me.lblCategory.Name = "lblCategory"
        Me.lblCategory.Size = New System.Drawing.Size(130, 13)
        Me.lblCategory.TabIndex = 93
        Me.lblCategory.Text = "Category"
        Me.lblCategory.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'objFooter
        '
        Me.objFooter.BorderColor = System.Drawing.Color.Silver
        Me.objFooter.Controls.Add(Me.txtMappedInfo)
        Me.objFooter.Controls.Add(Me.btnClose)
        Me.objFooter.Controls.Add(Me.btnSave)
        Me.objFooter.Dock = System.Windows.Forms.DockStyle.Bottom
        Me.objFooter.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.objFooter.GradiantStyle = eZee.Common.eZeeFooter.GradientStyle.Up
        Me.objFooter.GradientColor1 = System.Drawing.SystemColors.Control
        Me.objFooter.GradientColor2 = System.Drawing.SystemColors.Control
        Me.objFooter.Location = New System.Drawing.Point(0, 315)
        Me.objFooter.Name = "objFooter"
        Me.objFooter.Size = New System.Drawing.Size(439, 55)
        Me.objFooter.TabIndex = 10
        '
        'txtMappedInfo
        '
        Me.txtMappedInfo.BorderStyle = System.Windows.Forms.BorderStyle.None
        Me.txtMappedInfo.Cursor = System.Windows.Forms.Cursors.IBeam
        Me.txtMappedInfo.Location = New System.Drawing.Point(12, 6)
        Me.txtMappedInfo.Margin = New System.Windows.Forms.Padding(0)
        Me.txtMappedInfo.Multiline = True
        Me.txtMappedInfo.Name = "txtMappedInfo"
        Me.txtMappedInfo.ReadOnly = True
        Me.txtMappedInfo.ScrollBars = System.Windows.Forms.ScrollBars.Vertical
        Me.txtMappedInfo.Size = New System.Drawing.Size(181, 42)
        Me.txtMappedInfo.TabIndex = 8
        '
        'btnClose
        '
        Me.btnClose.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnClose.BackColor = System.Drawing.Color.White
        Me.btnClose.BackgroundImage = CType(resources.GetObject("btnClose.BackgroundImage"), System.Drawing.Image)
        Me.btnClose.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.btnClose.BorderColor = System.Drawing.Color.Empty
        Me.btnClose.ButtonType = eZee.Common.eZeeLightButton.enButtonType.NormalButton
        Me.btnClose.FlatAppearance.BorderSize = 0
        Me.btnClose.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnClose.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnClose.ForeColor = System.Drawing.Color.Black
        Me.btnClose.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.btnClose.GradientForeColor = System.Drawing.Color.Black
        Me.btnClose.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.btnClose.HoverGradientForeColor = System.Drawing.Color.Black
        Me.btnClose.Location = New System.Drawing.Point(333, 13)
        Me.btnClose.Name = "btnClose"
        Me.btnClose.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.btnClose.PressedGradientForeColor = System.Drawing.Color.Black
        Me.btnClose.Size = New System.Drawing.Size(94, 30)
        Me.btnClose.TabIndex = 7
        Me.btnClose.Text = "&Close"
        Me.btnClose.UseVisualStyleBackColor = True
        '
        'btnSave
        '
        Me.btnSave.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnSave.BackColor = System.Drawing.Color.White
        Me.btnSave.BackgroundImage = CType(resources.GetObject("btnSave.BackgroundImage"), System.Drawing.Image)
        Me.btnSave.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.btnSave.BorderColor = System.Drawing.Color.Empty
        Me.btnSave.ButtonType = eZee.Common.eZeeLightButton.enButtonType.NormalButton
        Me.btnSave.FlatAppearance.BorderSize = 0
        Me.btnSave.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnSave.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnSave.ForeColor = System.Drawing.Color.Black
        Me.btnSave.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.btnSave.GradientForeColor = System.Drawing.Color.Black
        Me.btnSave.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.btnSave.HoverGradientForeColor = System.Drawing.Color.Black
        Me.btnSave.Location = New System.Drawing.Point(233, 13)
        Me.btnSave.Name = "btnSave"
        Me.btnSave.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.btnSave.PressedGradientForeColor = System.Drawing.Color.Black
        Me.btnSave.Size = New System.Drawing.Size(94, 30)
        Me.btnSave.TabIndex = 6
        Me.btnSave.Text = "&Save"
        Me.btnSave.UseVisualStyleBackColor = True
        '
        'frmMembership_AddEdit
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(439, 370)
        Me.Controls.Add(Me.pnlMembershipInfo)
        Me.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle
        Me.KeyPreview = True
        Me.MaximizeBox = False
        Me.MinimizeBox = False
        Me.Name = "frmMembership_AddEdit"
        Me.ShowInTaskbar = False
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent
        Me.Text = "Add/Edit Membership"
        Me.pnlMembershipInfo.ResumeLayout(False)
        Me.gbMembership.ResumeLayout(False)
        Me.gbMembership.PerformLayout()
        Me.objFooter.ResumeLayout(False)
        Me.objFooter.PerformLayout()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents pnlMembershipInfo As System.Windows.Forms.Panel
    Friend WithEvents gbMembership As eZee.Common.eZeeCollapsibleContainer
    Friend WithEvents objFooter As eZee.Common.eZeeFooter
    Friend WithEvents txtDescription As eZee.TextBox.AlphanumericTextBox
    Friend WithEvents lblDescription As System.Windows.Forms.Label
    Friend WithEvents txtName As eZee.TextBox.AlphanumericTextBox
    Friend WithEvents lblName As System.Windows.Forms.Label
    Friend WithEvents txtCode As eZee.TextBox.AlphanumericTextBox
    Friend WithEvents lblCode As System.Windows.Forms.Label
    Friend WithEvents cboCategory As System.Windows.Forms.ComboBox
    Friend WithEvents lblCategory As System.Windows.Forms.Label
    Friend WithEvents btnClose As eZee.Common.eZeeLightButton
    Friend WithEvents btnSave As eZee.Common.eZeeLightButton
    Friend WithEvents objbtnOtherLanguage As eZee.Common.eZeeGradientButton
    Friend WithEvents objbtnAddCategory As eZee.Common.eZeeGradientButton
    Friend WithEvents lblTransactionHead As System.Windows.Forms.Label
    Friend WithEvents cboETranHead As System.Windows.Forms.ComboBox
    Friend WithEvents txtMappedInfo As System.Windows.Forms.TextBox
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents cboCTranHead As System.Windows.Forms.ComboBox
    Friend WithEvents chkAppearOnPayslip As System.Windows.Forms.CheckBox
    Friend WithEvents lblEmployerMemNo As System.Windows.Forms.Label
    Friend WithEvents txtEmployerMemNo As eZee.TextBox.AlphanumericTextBox
    Friend WithEvents chkShowBasicSalaryOnStatutoryReport As System.Windows.Forms.CheckBox
End Class
