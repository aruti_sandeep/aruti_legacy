﻿Option Strict On

Imports eZeeCommonLib
Imports Aruti.Data
'Last Message Index = 3

Public Class frmResultCodesList

#Region "Private Variable"

    Private objResultMaster As clsresult_master
    Private ReadOnly mstrModuleName As String = "frmResultCodesList"

#End Region

#Region "Form's Event"

    Private Sub frmResultCodesList_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        objResultMaster = New clsresult_master
        Try
            Call Set_Logo(Me, gApplicationType)
            'S.SANDEEP [ 20 AUG 2011 ] -- START
            'ENHANCEMENT : LANGUAGES IMPLEMENTATION
            Language.setLanguage(Me.Name)
            Call OtherSettings()
            'S.SANDEEP [ 20 AUG 2011 ] -- END



            'Pinkal (22-MAY-2012) -- Start
            'Enhancement : TRA Changes
            '   fillList()
            FillCombo()
            lvResultCodeList.GridLines = False
            'Pinkal (22-MAY-2012) -- End


            Call SetVisibility()


            'Pinkal (22-MAY-2012) -- Start
            'Enhancement : TRA Changes

            'If lvResultCodeList.Items.Count > 0 Then lvResultCodeList.Items(0).Selected = True
            'lvResultCodeList.Select()
            cboResultGroup.Select()

            'Pinkal (22-MAY-2012) -- End

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmResultCodesList_Load", mstrModuleName)
        End Try
    End Sub

    Private Sub frmResultCodesList_KeyUp(ByVal sender As System.Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles MyBase.KeyUp
        Try
            If e.KeyCode = Keys.Delete And lvResultCodeList.Focused = True Then
                'Sohail (28 Jun 2011) -- Start
                'Issue : Delete event fired even if there is no delete privilege
                'btnDelete_Click(sender, e)
                Call btnDelete.PerformClick()
                'Sohail (28 Jun 2011) -- End
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmResultCodesList_KeyUp", mstrModuleName)
        End Try
    End Sub

    Private Sub frmResultCodesList_FormClosed(ByVal sender As System.Object, ByVal e As System.Windows.Forms.FormClosedEventArgs) Handles MyBase.FormClosed
        objResultMaster = Nothing
    End Sub

    'S.SANDEEP [ 20 AUG 2011 ] -- START
    'ENHANCEMENT : LANGUAGES IMPLEMENTATION
    Private Sub Form_LanguageClick(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Me.LanguageClick
        Dim objfrm As New frmLanguage
        Try
            If User._Object._Isrighttoleft = True Then
                objfrm.RightToLeft = Windows.Forms.RightToLeft.Yes
                objfrm.RightToLeftLayout = True
                Call Language.ctlRightToLeftlayOut(objfrm)
            End If

            Call SetMessages()

            clsresult_master.SetMessages()
            objfrm._Other_ModuleNames = "clsresult_master"
            objfrm.displayDialog(Me)

            Call SetLanguage()

        Catch ex As System.Exception
            Call DisplayError.Show("-1", ex.Message, "Form_LanguageClick", mstrModuleName)
        Finally
            objfrm.Dispose()
            objfrm = Nothing
        End Try
    End Sub
    'S.SANDEEP [ 20 AUG 2011 ] -- END

#End Region

#Region "Button's Event"

    Private Sub btnNew_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnNew.Click
        Try
            Dim ObjFrmResultCode_AddEdit As New frmResultCode_AddEdit
            'S.SANDEEP [ 20 AUG 2011 ] -- START
            'ENHANCEMENT : LANGUAGES IMPLEMENTATION
            If User._Object._Isrighttoleft = True Then
                ObjFrmResultCode_AddEdit.RightToLeft = Windows.Forms.RightToLeft.Yes
                ObjFrmResultCode_AddEdit.RightToLeftLayout = True
                Call Language.ctlRightToLeftlayOut(ObjFrmResultCode_AddEdit)
            End If
            'S.SANDEEP [ 20 AUG 2011 ] -- END
            If ObjFrmResultCode_AddEdit.displayDialog(-1, enAction.ADD_CONTINUE) Then
                fillList()
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "btnNew_Click", mstrModuleName)
        End Try

    End Sub

    Private Sub btnEdit_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnEdit.Click
        Try
            If lvResultCodeList.SelectedItems.Count < 1 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 1, "Please select Result Code from the list to perform further operation."), enMsgBoxStyle.Information) '?1
                lvResultCodeList.Select()
                Exit Sub
            End If
            Dim objfrmResultCode_AddEdit As New frmResultCode_AddEdit
            Try
                'S.SANDEEP [ 20 AUG 2011 ] -- START
                'ENHANCEMENT : LANGUAGES IMPLEMENTATION
                If User._Object._Isrighttoleft = True Then
                    objfrmResultCode_AddEdit.RightToLeft = Windows.Forms.RightToLeft.Yes
                    objfrmResultCode_AddEdit.RightToLeftLayout = True
                    Call Language.ctlRightToLeftlayOut(objfrmResultCode_AddEdit)
                End If
                'S.SANDEEP [ 20 AUG 2011 ] -- END
                Dim intSelectedIndex As Integer
                intSelectedIndex = lvResultCodeList.SelectedItems(0).Index
                If objfrmResultCode_AddEdit.displayDialog(CInt(lvResultCodeList.SelectedItems(0).Tag), enAction.EDIT_ONE) Then
                    Call fillList()
                End If
                objfrmResultCode_AddEdit = Nothing

                lvResultCodeList.Items(intSelectedIndex).Selected = True
                lvResultCodeList.EnsureVisible(intSelectedIndex)
                lvResultCodeList.Select()
            Catch ex As Exception
                Call DisplayError.Show("-1", ex.Message, "btnEdit_Click", mstrModuleName)
            Finally
                If objfrmResultCode_AddEdit IsNot Nothing Then objfrmResultCode_AddEdit.Dispose()
            End Try
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "btnEdit_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub btnDelete_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnDelete.Click
        If lvResultCodeList.SelectedItems.Count < 1 Then
            eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 1, "Please select Result Code from the list to perform further operation."), enMsgBoxStyle.Information) '?1
            lvResultCodeList.Select()
            Exit Sub
        End If
        If objResultMaster.isUsed(CInt(lvResultCodeList.SelectedItems(0).Tag)) Then
            eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 2, "Sorry, You cannot delete this Result Code. Reason: This Result Code is in use."), enMsgBoxStyle.Information) '?2
            lvResultCodeList.Select()
            Exit Sub
        End If
        Try
            Dim intSelectedIndex As Integer
            intSelectedIndex = lvResultCodeList.SelectedItems(0).Index

            If eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 3, "Are you sure you want to delete this Result Code?"), CType(enMsgBoxStyle.Question + enMsgBoxStyle.YesNo, enMsgBoxStyle)) = Windows.Forms.DialogResult.Yes Then

                'S.SANDEEP [28-May-2018] -- START
                'ISSUE/ENHANCEMENT : {Audit Trails} 
                objResultMaster._FormName = mstrModuleName
                objResultMaster._LoginEmployeeunkid = 0
                objResultMaster._ClientIP = getIP()
                objResultMaster._HostName = getHostName()
                objResultMaster._FromWeb = False
                objResultMaster._AuditUserId = User._Object._Userunkid
objResultMaster._CompanyUnkid = Company._Object._Companyunkid
                objResultMaster._AuditDate = ConfigParameter._Object._CurrentDateAndTime
                'S.SANDEEP [28-May-2018] -- END

                objResultMaster.Delete(CInt(lvResultCodeList.SelectedItems(0).Tag))
                lvResultCodeList.SelectedItems(0).Remove()

                If lvResultCodeList.Items.Count <= 0 Then
                    Exit Try
                End If

                If lvResultCodeList.Items.Count = intSelectedIndex Then
                    intSelectedIndex = lvResultCodeList.Items.Count - 1
                    lvResultCodeList.Items(intSelectedIndex).Selected = True
                    lvResultCodeList.EnsureVisible(intSelectedIndex)
                ElseIf lvResultCodeList.Items.Count <> 0 Then
                    lvResultCodeList.Items(intSelectedIndex).Selected = True
                    lvResultCodeList.EnsureVisible(intSelectedIndex)
                End If
            End If
            lvResultCodeList.Select()
        Catch ex As Exception
            Call DisplayError.Show("-1", ex.Message, "btnDelete_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub btnClose_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnClose.Click
        Me.Close()
    End Sub

    'Pinkal (22-MAY-2012) -- Start
    'Enhancement : TRA Changes

    Private Sub objbtnSearch_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnSearch.Click
        Try
            fillList()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objbtnSearch_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub objbtnReset_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnReset.Click
        Try
            cboResultGroup.SelectedIndex = 0
            cboResultName.SelectedIndex = 0
            fillList()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objbtnReset_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub objbtnSearchQualification_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnSearchResultName.Click
        Dim objfrm As New frmCommonSearch
        Dim dtList As DataTable
        Try
            dtList = CType(cboResultName.DataSource, DataTable)
            With cboResultName
                objfrm.DataSource = dtList
                objfrm.ValueMember = .ValueMember
                objfrm.DisplayMember = .DisplayMember
                If objfrm.DisplayDialog Then
                    .SelectedValue = objfrm.SelectedValue
                End If
                .Focus()
            End With
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objbtnSearchResultName_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub objbtnSearchResultGroup_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnSearchResultGroup.Click
        Dim objfrm As New frmCommonSearch
        Dim dtList As DataTable
        Try
            dtList = CType(cboResultGroup.DataSource, DataTable)
            With cboResultGroup
                objfrm.DataSource = dtList
                objfrm.ValueMember = .ValueMember
                objfrm.DisplayMember = .DisplayMember
                If objfrm.DisplayDialog Then
                    .SelectedValue = objfrm.SelectedValue
                End If
                .Focus()
            End With
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objbtnSearchResultGroup_Click", mstrModuleName)
        End Try
    End Sub

    'Pinkal (22-MAY-2012) -- End

#End Region

#Region " Private Methods "

    'Pinkal (22-MAY-2012) -- Start
    'Enhancement : TRA Changes

    Private Sub FillCombo()
        Dim dsList As DataSet = Nothing
        Try
            Dim objResultGrp As New clsCommon_Master
            dsList = objResultGrp.getComboList(clsCommon_Master.enCommonMaster.RESULT_GROUP, True, "ResultGrp", -1, False)
            cboResultGroup.DisplayMember = "name"
            cboResultGroup.ValueMember = "masterunkid"
            cboResultGroup.DataSource = dsList.Tables("ResultGrp")

            dsList = Nothing

            'S.SANDEEP [28-May-2018] -- START
            'ISSUE/ENHANCEMENT : {Audit Trails} 
            objResultMaster._FormName = mstrModuleName
            objResultMaster._LoginEmployeeunkid = 0
            objResultMaster._ClientIP = getIP()
            objResultMaster._HostName = getHostName()
            objResultMaster._FromWeb = False
            objResultMaster._AuditUserId = User._Object._Userunkid
objResultMaster._CompanyUnkid = Company._Object._Companyunkid
            objResultMaster._AuditDate = ConfigParameter._Object._CurrentDateAndTime
            'S.SANDEEP [28-May-2018] -- END

            dsList = objResultMaster.getComboList("ResultCode", True, -1)
            cboResultName.DisplayMember = "Name"
            cboResultName.ValueMember = "Id"
            cboResultName.DataSource = dsList.Tables(0)
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "FillCombo", mstrModuleName)
        End Try
    End Sub

    'Pinkal (22-MAY-2012) -- End


    Private Sub fillList()
        Dim strSearching As String = ""
        Dim dsResultList As New DataSet
        Try

            If User._Object.Privilege._AllowToViewResultCodeList = True Then   'Pinkal (09-Jul-2012) -- Start


                'S.SANDEEP [28-May-2018] -- START
                'ISSUE/ENHANCEMENT : {Audit Trails} 
                objResultMaster._FormName = mstrModuleName
                objResultMaster._LoginEmployeeunkid = 0
                objResultMaster._ClientIP = getIP()
                objResultMaster._HostName = getHostName()
                objResultMaster._FromWeb = False
                objResultMaster._AuditUserId = User._Object._Userunkid
objResultMaster._CompanyUnkid = Company._Object._Companyunkid
                objResultMaster._AuditDate = ConfigParameter._Object._CurrentDateAndTime
                'S.SANDEEP [28-May-2018] -- END

                dsResultList = objResultMaster.GetList("List")


                If CInt(cboResultGroup.SelectedValue) > 0 Then
                    strSearching = "AND resultgroupunkid =" & CInt(cboResultGroup.SelectedValue) & " "
                End If


                If CInt(cboResultName.SelectedValue) > 0 Then
                    strSearching &= "AND resultunkid =" & CInt(cboResultName.SelectedValue) & " "
                End If

                Dim dtResultCode As New DataTable
                If strSearching.Length > 0 Then
                    strSearching = strSearching.Substring(3)
                    dtResultCode = New DataView(dsResultList.Tables("List"), strSearching, "", DataViewRowState.CurrentRows).ToTable
                Else
                    dtResultCode = dsResultList.Tables("List")
                End If


                Dim lvItem As ListViewItem

                lvResultCodeList.Items.Clear()
                For Each drRow As DataRow In dtResultCode.Rows
                    lvItem = New ListViewItem
                    lvItem.Text = drRow("name").ToString
                    lvItem.Tag = drRow("resultunkid")
                    lvItem.SubItems.Add(drRow("resultcode").ToString)
                    lvItem.SubItems.Add(drRow("resultname").ToString)
                    If CInt(drRow("result_level")) > 0 Then
                        lvItem.SubItems.Add(drRow("result_level").ToString)
                    Else
                        lvItem.SubItems.Add("")
                    End If

                    lvItem.SubItems.Add(drRow("description").ToString)

                    lvResultCodeList.Items.Add(lvItem)
                Next

                lvResultCodeList.GroupingColumn = colhGroupName
                lvResultCodeList.DisplayGroups(True)

                'Pinkal (2-Jan-2012) -- Start
                'Enhancement : TRA Changes

                If lvResultCodeList.Items.Count > 16 Then
                    colhDescription.Width = 315 - 18
                Else
                    colhDescription.Width = 315
                End If
                'Pinkal (2-Jan-2012) -- End

            End If

        Catch ex As Exception
            Call DisplayError.Show("-1", ex.Message, "fillList", mstrModuleName)
        Finally
            dsResultList.Dispose()
        End Try
    End Sub

    Private Sub SetVisibility()

        Try
            btnNew.Enabled = User._Object.Privilege._AddResultCode
            btnEdit.Enabled = User._Object.Privilege._EditResultCode
            btnDelete.Enabled = User._Object.Privilege._DeleteResultCode

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "SetVisibility", mstrModuleName)
        End Try

    End Sub

#End Region

    '<Language> This Auto Generated Text Please Do Not Modify it.
#Region " Language & UI Settings "
    Private Sub OtherSettings()
        Try
            Me.SuspendLayout()
           
            Call SetLanguage()

            Me.gbFilterCriteria.GradientColor = GUI._eZeeContainerHeaderBackColor
            Me.gbFilterCriteria.ForeColor = GUI._eZeeContainerHeaderForeColor


            Me.eZeeHeader.GradientColor1 = GUI._HeaderBackColor1
            Me.eZeeHeader.GradientColor2 = GUI._HeaderBackColor2
            Me.eZeeHeader.BorderColor = GUI._HeaderBorderColor
            Me.eZeeHeader.HeaderTextForeColor = GUI._HeaderTitleFontColor
            Me.eZeeHeader.DescriptionForeColor = GUI._HeaderMessageFontColor


            Me.btnDelete.GradientBackColor = GUI._ButttonBackColor
            Me.btnDelete.GradientForeColor = GUI._ButttonFontColor

            Me.btnClose.GradientBackColor = GUI._ButttonBackColor
            Me.btnClose.GradientForeColor = GUI._ButttonFontColor

            Me.btnNew.GradientBackColor = GUI._ButttonBackColor
            Me.btnNew.GradientForeColor = GUI._ButttonFontColor

            Me.btnEdit.GradientBackColor = GUI._ButttonBackColor
            Me.btnEdit.GradientForeColor = GUI._ButttonFontColor


            Me.ResumeLayout()
        Catch Ex As Exception
            DisplayError.Show("-1", Ex.Message, "OtherSettings", mstrModuleName)
        End Try
    End Sub


    Private Sub SetLanguage()
        Try
            Me.Text = Language._Object.getCaption(Me.Name, Me.Text)

            Me.eZeeHeader.Title = Language._Object.getCaption(Me.eZeeHeader.Name & "_Title", Me.eZeeHeader.Title)
            Me.eZeeHeader.Message = Language._Object.getCaption(Me.eZeeHeader.Name & "_Message", Me.eZeeHeader.Message)
            Me.btnDelete.Text = Language._Object.getCaption(Me.btnDelete.Name, Me.btnDelete.Text)
            Me.btnClose.Text = Language._Object.getCaption(Me.btnClose.Name, Me.btnClose.Text)
            Me.btnNew.Text = Language._Object.getCaption(Me.btnNew.Name, Me.btnNew.Text)
            Me.btnEdit.Text = Language._Object.getCaption(Me.btnEdit.Name, Me.btnEdit.Text)
            Me.colhGroupName.Text = Language._Object.getCaption(CStr(Me.colhGroupName.Tag), Me.colhGroupName.Text)
            Me.colhResultCode.Text = Language._Object.getCaption(CStr(Me.colhResultCode.Tag), Me.colhResultCode.Text)
            Me.colhResultName.Text = Language._Object.getCaption(CStr(Me.colhResultName.Tag), Me.colhResultName.Text)
            Me.colhDescription.Text = Language._Object.getCaption(CStr(Me.colhDescription.Tag), Me.colhDescription.Text)
            Me.colhResultLevel.Text = Language._Object.getCaption(CStr(Me.colhResultLevel.Tag), Me.colhResultLevel.Text)
            Me.gbFilterCriteria.Text = Language._Object.getCaption(Me.gbFilterCriteria.Name, Me.gbFilterCriteria.Text)
            Me.lblResultName.Text = Language._Object.getCaption(Me.lblResultName.Name, Me.lblResultName.Text)
            Me.lblResultGroup.Text = Language._Object.getCaption(Me.lblResultGroup.Name, Me.lblResultGroup.Text)

        Catch Ex As Exception
            DisplayError.Show("-1", Ex.Message, "SetLanguage", mstrModuleName)
        End Try
    End Sub


    Private Sub SetMessages()
        Try
            Language.setMessage(mstrModuleName, 1, "Please select Result Code from the list to perform further operation.")
            Language.setMessage(mstrModuleName, 2, "Sorry, You cannot delete this Result Code. Reason: This Result Code is in use.")
            Language.setMessage(mstrModuleName, 3, "Are you sure you want to delete this Result Code?")

        Catch Ex As Exception
            DisplayError.Show("-1", Ex.Message, "SetMessages", mstrModuleName)
        End Try
    End Sub
#End Region 'Language & UI Settings
    '</Language>
End Class