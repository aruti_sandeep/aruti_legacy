﻿Option Strict On

#Region " Imports "

Imports eZeeCommonLib
Imports Aruti.Data
Imports System.DirectoryServices
Imports System.DirectoryServices.Protocols

#End Region

Public Class frmADAttributeMapping

    Private ReadOnly mstrModuleName As String = "frmADAttributeMapping"
    Private mblnCancel As Boolean = True
    Private objAdAttribute As clsADAttribute_mapping
    Private mdtTable As DataTable = Nothing
    Private mdView As DataView = Nothing

    'Pinkal (09-Mar-2020) -- Start
    'Enhancement NMB AD -   Active Directory Integration Requirement For NMB [Ref No : 273].
    Private mdtHierarchyLevel As DataTable = Nothing
    'Pinkal (09-Mar-2020) -- End


#Region " Display Dialog "

    Public Function displayDialog() As Boolean
        Try
            Me.ShowDialog()

            Return Not mblnCancel

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure : displayDialog ; Module Name : " & mstrModuleName)
        End Try
    End Function

#End Region

#Region " Private Methods "

    Private Sub FillCombo()
        Dim dsList As DataSet = Nothing
        Dim dtCommon As DataTable = Nothing
        Try

            'Pinkal (09-Mar-2020) -- Start
            'Enhancement NMB AD -   Active Directory Integration Requirement For NMB [Ref No : 273].

            'Pinkal (18-Jun-2020) -- Start
            'ENHANCEMENT NMB:  Working on Claim Approver Migration To change the Approver Logic.
            RemoveHandler cboCompany.SelectedValueChanged, AddressOf cboCompany_SelectedValueChanged
            'Pinkal (18-Jun-2020) -- End

            Dim objCompany As New clsCompany_Master
            dsList = objCompany.GetList("List", True)
            With cboCompany
                .ValueMember = "companyunkid"
                .DisplayMember = "name"
                .DataSource = dsList.Tables("List")
            End With

            'Pinkal (18-Jun-2020) -- Start
            'ENHANCEMENT NMB:  Working on Claim Approver Migration To change the Approver Logic.
            AddHandler cboCompany.SelectedValueChanged, AddressOf cboCompany_SelectedValueChanged
            'Pinkal (18-Jun-2020) -- End

            'Pinkal (09-Mar-2020) -- End



            Dim objEmpMaster As New clsEmployee_Master
            Dim dtTable As DataTable = objEmpMaster.GetDynamicFieldColName(False, False, False, False, False, True)
            Dim mstrCompulosryIDs As String = CInt(clsEmployee_Master.EmpColEnum.Col_Display_Name) & "," & CInt(clsEmployee_Master.EmpColEnum.Col_Password) & "," & CInt(clsEmployee_Master.EmpColEnum.Col_FirstName) & "," & CInt(clsEmployee_Master.EmpColEnum.Col_Othername) & "," & CInt(clsEmployee_Master.EmpColEnum.Col_Surname) & _
                                                             "," & CInt(clsEmployee_Master.EmpColEnum.Col_Job) & "," & CInt(clsEmployee_Master.EmpColEnum.Col_Present_Email) & "," & CInt(clsEmployee_Master.EmpColEnum.Col_Domicile_Email) & _
                                                             "," & CInt(clsEmployee_Master.EmpColEnum.Col_Present_Tel_No) & "," & CInt(clsEmployee_Master.EmpColEnum.Col_Domicile_Tel_No) & "," & CInt(clsEmployee_Master.EmpColEnum.Col_Present_Post_Town) & _
                                                             ", " & CInt(clsEmployee_Master.EmpColEnum.Col_Present_Mobile) & ", " & CInt(clsEmployee_Master.EmpColEnum.Col_Domicile_Mobile) & "," & CInt(clsEmployee_Master.EmpColEnum.Col_Domicile_Post_Town) & "," & CInt(clsEmployee_Master.EmpColEnum.Col_Present_State) & _
                                                             "," & CInt(clsEmployee_Master.EmpColEnum.Col_Domicile_State) & "," & CInt(clsEmployee_Master.EmpColEnum.Col_Present_Country) & "," & CInt(clsEmployee_Master.EmpColEnum.Col_Domicile_Country) & _
                                                             "," & CInt(clsEmployee_Master.EmpColEnum.Col_Present_Address2) & "," & CInt(clsEmployee_Master.EmpColEnum.Col_Domicile_Address2) & "," & CInt(clsEmployee_Master.EmpColEnum.Col_Present_Fax) & _
                                                             "," & CInt(clsEmployee_Master.EmpColEnum.Col_Domicile_Fax) & "," & CInt(clsEmployee_Master.EmpColEnum.Col_Email)



            'Pinkal (09-Mar-2020) -- Enhancement NMB AD -   Active Directory Integration Requirement For NMB [Ref No : 273].[clsEmployee_Master.EmpColEnum.Col_Email]

            'Pinkal (22-Feb-2020) --  'Enhancement NMB AD -   Active Directory Integration Requirement For NMB [Ref No : 273].[CInt(clsEmployee_Master.EmpColEnum.Col_Present_Address2) & "," & CInt(clsEmployee_Master.EmpColEnum.Col_Domicile_Address2) & "," & CInt(clsEmployee_Master.EmpColEnum.Col_Present_Fax) "," & CInt(clsEmployee_Master.EmpColEnum.Col_Domicile_Fax)]


            dtTable = New DataView(dtTable, "ID IN (" & mstrCompulosryIDs & ")", "ID", DataViewRowState.CurrentRows).ToTable()

            Dim dRow As DataRow = dtTable.NewRow()
            dRow("ID") = 0
            dRow("Name") = Language.getMessage(mstrModuleName, 1, "Select")
            dtTable.Rows.InsertAt(dRow, 0)

            With cboLoginUserName
                .ValueMember = "ID"
                .DisplayMember = "Name"
                .DataSource = dtTable.Copy()
                .SelectedValue = CInt(clsEmployee_Master.EmpColEnum.Col_Display_Name)
            End With

            With cboPassword
                .ValueMember = "ID"
                .DisplayMember = "Name"
                .DataSource = dtTable.Copy()
                .SelectedValue = CInt(clsEmployee_Master.EmpColEnum.Col_Password)
            End With

            With cboFirstName
                .ValueMember = "ID"
                .DisplayMember = "Name"
                .DataSource = dtTable.Copy()
                .SelectedValue = CInt(clsEmployee_Master.EmpColEnum.Col_FirstName)
            End With

            With cboMiddleName
                .ValueMember = "ID"
                .DisplayMember = "Name"
                .DataSource = dtTable.Copy()
                .SelectedValue = CInt(clsEmployee_Master.EmpColEnum.Col_Othername)
            End With

            With cboLastName
                .ValueMember = "ID"
                .DisplayMember = "Name"
                .DataSource = dtTable.Copy()
                .SelectedValue = CInt(clsEmployee_Master.EmpColEnum.Col_Surname)
            End With


            'Pinkal (09-Mar-2020) -- Start
            'Enhancement NMB AD -   Active Directory Integration Requirement For NMB [Ref No : 273].
            'dtCommon = New DataView(dtTable, "ID in (0," & CInt(clsEmployee_Master.EmpColEnum.Col_Present_Email) & "," & CInt(clsEmployee_Master.EmpColEnum.Col_Domicile_Email) & ")", "", DataViewRowState.CurrentRows).ToTable()
            dtCommon = New DataView(dtTable, "ID in (0," & CInt(clsEmployee_Master.EmpColEnum.Col_Email) & "," & CInt(clsEmployee_Master.EmpColEnum.Col_Present_Email) & "," & CInt(clsEmployee_Master.EmpColEnum.Col_Domicile_Email) & ")", "", DataViewRowState.CurrentRows).ToTable()
            'Pinkal (09-Mar-2020) -- End

            With cboEmailAddress
                .ValueMember = "ID"
                .DisplayMember = "Name"
                .DataSource = dtCommon.Copy()
                .SelectedValue = 0
            End With

            dtCommon = New DataView(dtTable, "ID in (0," & CInt(clsEmployee_Master.EmpColEnum.Col_Present_Tel_No) & "," & CInt(clsEmployee_Master.EmpColEnum.Col_Domicile_Tel_No) & ")", "", DataViewRowState.CurrentRows).ToTable()
            With cboTelephone
                .ValueMember = "ID"
                .DisplayMember = "Name"
                .DataSource = dtCommon.Copy()
                .SelectedValue = 0
            End With

            dtCommon = New DataView(dtTable, "ID in (0," & CInt(clsEmployee_Master.EmpColEnum.Col_Present_Mobile) & "," & CInt(clsEmployee_Master.EmpColEnum.Col_Domicile_Mobile) & ")", "", DataViewRowState.CurrentRows).ToTable()
            With cboMobile
                .ValueMember = "ID"
                .DisplayMember = "Name"
                .DataSource = dtCommon.Copy()
                .SelectedValue = 0
            End With

            dtCommon = New DataView(dtTable, "ID in (0," & CInt(clsEmployee_Master.EmpColEnum.Col_Present_Post_Town) & "," & CInt(clsEmployee_Master.EmpColEnum.Col_Domicile_Post_Town) & ")", "", DataViewRowState.CurrentRows).ToTable()
            With cboCity
                .ValueMember = "ID"
                .DisplayMember = "Name"
                .DataSource = dtCommon.Copy()
                .SelectedValue = 0
            End With

            dtCommon = New DataView(dtTable, "ID in (0," & CInt(clsEmployee_Master.EmpColEnum.Col_Present_State) & "," & CInt(clsEmployee_Master.EmpColEnum.Col_Domicile_State) & ")", "", DataViewRowState.CurrentRows).ToTable()
            With cboRegion
                .ValueMember = "ID"
                .DisplayMember = "Name"
                .DataSource = dtCommon.Copy()
                .SelectedValue = 0
            End With

            dtCommon = New DataView(dtTable, "ID in (0," & CInt(clsEmployee_Master.EmpColEnum.Col_Present_Country) & "," & CInt(clsEmployee_Master.EmpColEnum.Col_Domicile_Country) & ")", "", DataViewRowState.CurrentRows).ToTable()
            With cboCountry
                .ValueMember = "ID"
                .DisplayMember = "Name"
                .DataSource = dtCommon.Copy()
                .SelectedValue = 0
            End With


            'Pinkal (22-Feb-2020) -- Start
            'Enhancement NMB AD -   Active Directory Integration Requirement For NMB [Ref No : 273].
            dtCommon = New DataView(dtTable, "ID in (0," & CInt(clsEmployee_Master.EmpColEnum.Col_Present_Address2) & "," & CInt(clsEmployee_Master.EmpColEnum.Col_Domicile_Address2) & ")", "", DataViewRowState.CurrentRows).ToTable()
            With cboStreet
                .ValueMember = "ID"
                .DisplayMember = "Name"
                .DataSource = dtCommon.Copy()
                .SelectedValue = 0
            End With

            dtCommon = New DataView(dtTable, "ID in (0," & CInt(clsEmployee_Master.EmpColEnum.Col_Present_Fax) & "," & CInt(clsEmployee_Master.EmpColEnum.Col_Domicile_Fax) & ")", "", DataViewRowState.CurrentRows).ToTable()
            With cboPOBoxNo
                .ValueMember = "ID"
                .DisplayMember = "Name"
                .DataSource = dtCommon.Copy()
                .SelectedValue = 0
            End With
            'Pinkal (22-Feb-2020) -- End



            Dim objMaster As New clsMasterData
            dsList = objMaster.GetEAllocation_Notification("List", "", False)


            'Pinkal (09-Mar-2020) -- Start
            'Enhancement NMB AD -   Active Directory Integration Requirement For NMB [Ref No : 273].

            dRow = dsList.Tables(0).NewRow()
            dRow("Id") = 0
            dRow("Name") = Language.getMessage(mstrModuleName, 1, "Select")
            dsList.Tables(0).Rows.InsertAt(dRow, 0)

            Dim dtAllocation As DataTable = New DataView(dsList.Tables(0), "Id <> " & enAllocation.COST_CENTER, "", DataViewRowState.CurrentRows).ToTable()

            With cboDepartment
                .ValueMember = "Id"
                .DisplayMember = "Name"
                .DataSource = dtAllocation.Copy()
            End With

            With cboJobtitle
                .ValueMember = "ID"
                .DisplayMember = "Name"
                .DataSource = dtAllocation.Copy()
                .SelectedValue = CInt(enAllocation.JOBS)
            End With


            With cboOffice
                .ValueMember = "ID"
                .DisplayMember = "Name"
                .DataSource = dtAllocation.Copy()
                .SelectedValue = CInt(enAllocation.CLASSES)
            End With

            With cboEntryPoint
                .ValueMember = "ID"
                .DisplayMember = "Name"
                .DataSource = dtAllocation.Copy()
                .SelectedValue = 0
            End With

            'Pinkal (09-Mar-2020) -- End


            'Pinkal (04-Apr-2020) -- Start
            'Enhancement NMB AD -   Active Directory Integration Requirement For NMB [Ref No : 273].
            With cboDescription
                .ValueMember = "ID"
                .DisplayMember = "Name"
                .DataSource = dtAllocation.Copy()
                .SelectedValue = 0
            End With
            'Pinkal (04-Apr-2020) -- End

            'Pinkal (18-Mar-2021) -- Start 
            'NMB Enhancmenet : AD Enhancement for NMB.
            Dim dtTransfer As DataTable = New DataView(dtAllocation, "ID not in (" & enAllocation.JOB_GROUP & "," & enAllocation.JOBS & "," & enAllocation.COST_CENTER & ")", "", DataViewRowState.CurrentRows).ToTable()
            With cboTransferAllocation
                .ValueMember = "ID"
                .DisplayMember = "Name"
                .DataSource = dtTransfer
                .SelectedValue = 0
            End With
            'Pinkal (18-Mar-2021) -- End


            dsList = Nothing
            dsList = objMaster.GetADDisplayNameSetting(True)
            With cboDisplayName
                .ValueMember = "ID"
                .DisplayMember = "Name"
                .DataSource = dsList.Tables(0).Copy()
            End With
            objMaster = Nothing

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "FillCombo", mstrModuleName)
        Finally
            dsList.Clear()
            dsList = Nothing
        End Try
    End Sub

    Private Sub SetValue()
        Try
            If mdtTable IsNot Nothing AndAlso mdtTable.Rows.Count > 0 Then
                Dim drRow As DataRow() = Nothing

                drRow = mdtTable.Select("attId = " & clsADAttribute_mapping.enAttributes.LoginUserName)
                If drRow.Length > 0 Then
                    drRow(0)("mappingunkid") = CInt(cboLoginUserName.SelectedValue)
                End If

                drRow = mdtTable.Select("attId = " & clsADAttribute_mapping.enAttributes.Password)
                If drRow.Length > 0 Then
                    drRow(0)("mappingunkid") = CInt(cboPassword.SelectedValue)
                End If

                drRow = mdtTable.Select("attId = " & clsADAttribute_mapping.enAttributes.FirstName)
                If drRow.Length > 0 Then
                    drRow(0)("mappingunkid") = CInt(cboFirstName.SelectedValue)
                End If

                drRow = mdtTable.Select("attId = " & clsADAttribute_mapping.enAttributes.MiddleName)
                If drRow.Length > 0 Then
                    drRow(0)("mappingunkid") = CInt(cboMiddleName.SelectedValue)
                End If

                drRow = mdtTable.Select("attId = " & clsADAttribute_mapping.enAttributes.LastName)
                If drRow.Length > 0 Then
                    drRow(0)("mappingunkid") = CInt(cboLastName.SelectedValue)
                End If

                drRow = mdtTable.Select("attId = " & clsADAttribute_mapping.enAttributes.DisplayName)
                If drRow.Length > 0 Then
                    drRow(0)("mappingunkid") = CInt(cboDisplayName.SelectedValue)
                End If

                drRow = mdtTable.Select("attId = " & clsADAttribute_mapping.enAttributes.JobTitle)
                If drRow.Length > 0 Then
                    drRow(0)("mappingunkid") = CInt(cboJobtitle.SelectedValue)
                End If

                drRow = mdtTable.Select("attId = " & clsADAttribute_mapping.enAttributes.Department)
                If drRow.Length > 0 Then
                    drRow(0)("mappingunkid") = CInt(cboDepartment.SelectedValue)
                    drRow(0)("isallocation") = True
                End If

                drRow = mdtTable.Select("attId = " & clsADAttribute_mapping.enAttributes.EmailAddress)
                If drRow.Length > 0 Then
                    drRow(0)("mappingunkid") = CInt(cboEmailAddress.SelectedValue)
                End If

                drRow = mdtTable.Select("attId = " & clsADAttribute_mapping.enAttributes.Telephone)
                If drRow.Length > 0 Then
                    drRow(0)("mappingunkid") = CInt(cboTelephone.SelectedValue)
                End If

                drRow = mdtTable.Select("attId = " & clsADAttribute_mapping.enAttributes.Mobile)
                If drRow.Length > 0 Then
                    drRow(0)("mappingunkid") = CInt(cboMobile.SelectedValue)
                End If

                drRow = mdtTable.Select("attId = " & clsADAttribute_mapping.enAttributes.City)
                If drRow.Length > 0 Then
                    drRow(0)("mappingunkid") = CInt(cboCity.SelectedValue)
                End If

                drRow = mdtTable.Select("attId = " & clsADAttribute_mapping.enAttributes.State)
                If drRow.Length > 0 Then
                    drRow(0)("mappingunkid") = CInt(cboRegion.SelectedValue)
                End If

                drRow = mdtTable.Select("attId = " & clsADAttribute_mapping.enAttributes.Country)
                If drRow.Length > 0 Then
                    drRow(0)("mappingunkid") = CInt(cboCountry.SelectedValue)
                End If


                'Pinkal (22-Feb-2020) -- Start
                'Enhancement NMB AD -   Active Directory Integration Requirement For NMB [Ref No : 273].

                drRow = mdtTable.Select("attId = " & clsADAttribute_mapping.enAttributes.Street)
                If drRow.Length > 0 Then
                    drRow(0)("mappingunkid") = CInt(cboStreet.SelectedValue)
                End If

                drRow = mdtTable.Select("attId = " & clsADAttribute_mapping.enAttributes.POBox)
                If drRow.Length > 0 Then
                    drRow(0)("mappingunkid") = CInt(cboPOBoxNo.SelectedValue)
                End If

                drRow = mdtTable.Select("attId = " & clsADAttribute_mapping.enAttributes.Office)
                If drRow.Length > 0 Then
                    drRow(0)("mappingunkid") = CInt(cboOffice.SelectedValue)
                End If

                'Pinkal (22-Feb-2020) -- End


                'Pinkal (09-Mar-2020) -- Start
                'Enhancement NMB AD -   Active Directory Integration Requirement For NMB [Ref No : 273].

                drRow = mdtTable.Select("attId = " & clsADAttribute_mapping.enAttributes.Company)
                If drRow.Length > 0 Then
                    drRow(0)("mappingunkid") = CInt(cboCompany.SelectedValue)
                End If


                drRow = mdtTable.Select("attId = " & clsADAttribute_mapping.enAttributes.EntryPoint)
                If drRow.Length > 0 Then
                    drRow(0)("mappingunkid") = CInt(cboEntryPoint.SelectedValue)
                End If

                drRow = mdtTable.Select("attId = " & clsADAttribute_mapping.enAttributes.HierarchyLevel)
                If drRow.Length > 0 Then
                    drRow(0)("mappingunkid") = nudHierarchyLevel.Value
                End If
                'Pinkal (09-Mar-2020) -- End


                'Pinkal (04-Apr-2020) -- Start
                'Enhancement NMB AD -   Active Directory Integration Requirement For NMB [Ref No : 273].
                drRow = mdtTable.Select("attId = " & clsADAttribute_mapping.enAttributes.Description)
                If drRow.Length > 0 Then
                    drRow(0)("mappingunkid") = CInt(cboDescription.SelectedValue)
                End If
                'Pinkal (04-Apr-2020) -- End

                'Pinkal (18-Mar-2021) -- Start 
                'NMB Enhancmenet : AD Enhancement for NMB.
                drRow = mdtTable.Select("attId = " & clsADAttribute_mapping.enAttributes.Transfer_Allocation)
                If drRow.Length > 0 Then
                    drRow(0)("mappingunkid") = CInt(cboTransferAllocation.SelectedValue)
                End If
                'Pinkal (18-Mar-2021) -- End

                mdtTable.AcceptChanges()
            End If

            'Pinkal (09-Mar-2020) -- Start
            'Enhancement NMB AD -   Active Directory Integration Requirement For NMB [Ref No : 273].
            objAdAttribute._CompanyId = CInt(cboCompany.SelectedValue)
            'Pinkal (09-Mar-2020) -- End

            objAdAttribute._Userunkid = User._Object._Userunkid
            objAdAttribute._FormName = mstrModuleName
            objAdAttribute._ClientIP = getIP()
            objAdAttribute._HostName = getHostName()
            objAdAttribute._FromWeb = False
            objAdAttribute._AuditUserId = User._Object._Userunkid
            objAdAttribute._AuditDate = ConfigParameter._Object._CurrentDateAndTime
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "SetValue", mstrModuleName)
        End Try
    End Sub

    Private Sub GetValue()
        Try

            GetOUFromAD()


            'Pinkal (09-Mar-2020) -- Start
            'Enhancement NMB AD -   Active Directory Integration Requirement For NMB [Ref No : 273].
            'Dim dtTable As DataTable = objAdAttribute.GetList("List", True)
            Dim dtTable As DataTable = objAdAttribute.GetList("List", CInt(cboCompany.SelectedValue), True)
            'Pinkal (09-Mar-2020) -- End


            If dtTable IsNot Nothing AndAlso dtTable.Rows.Count > 0 Then
                For Each dr As DataRow In dtTable.Rows


                    'Pinkal (16-Nov-2021)-- Start
                    'NMB Staff Requisition Approval Enhancements. 
                    Dim dRow() As DataRow = mdtTable.Select("attributename = '" & dr("attributename").ToString().Trim() & "'")
                    'Dim dRow() As DataRow = mdtTable.Select("attributename = '" & dr("attributename").ToString().Trim() & "' AND oupath = '" & dr("oupath").ToString().Trim() & "'")
                    'Pinkal (16-Nov-2021) -- End
                    If dRow.Length > 0 Then

                        'Pinkal (22-May-2020) -- Start
                        'ENHANCEMENT NMB:  Working on Exchange Server related changes in AD. 
                        dRow(0)("companyunkid") = CInt(cboCompany.SelectedValue)
                        'Pinkal (22-May-2020) -- End

                        dRow(0)("attributeunkid") = CInt(dr("attributeunkid"))
                        dRow(0)("mappingunkid") = CInt(dr("mappingunkid"))
                        dRow(0)("isallocation") = CBool(dr("isallocation"))
                        dRow(0)("allocationname") = dr("allocationname").ToString()
                        dRow(0).AcceptChanges()
                    End If
                Next
                'dtTable.Clear()
                'dtTable.Dispose()
            End If


            'Pinkal (22-May-2020) -- Start
            'ENHANCEMENT NMB:  Working on Exchange Server related changes in AD. 

            Dim drRow() As DataRow = Nothing
            If mdtTable IsNot Nothing AndAlso mdtTable.Rows.Count > 0 Then
                drRow = mdtTable.Select("companyunkid = " & CInt(cboCompany.SelectedValue) & "  AND mappingunkid > 0 and attId = " & clsADAttribute_mapping.enAttributes.LoginUserName)
                If drRow.Length > 0 Then
                    cboLoginUserName.SelectedValue = CInt(drRow(0)("mappingunkid"))
                End If

                drRow = mdtTable.Select("companyunkid = " & CInt(cboCompany.SelectedValue) & " AND mappingunkid > 0 and attId = " & clsADAttribute_mapping.enAttributes.Password)
                If drRow.Length > 0 Then
                    cboPassword.SelectedValue = CInt(drRow(0)("mappingunkid"))
                End If

                drRow = mdtTable.Select("companyunkid = " & CInt(cboCompany.SelectedValue) & "  AND mappingunkid > 0 and attId = " & clsADAttribute_mapping.enAttributes.FirstName)
                If drRow.Length > 0 Then
                    cboFirstName.SelectedValue = CInt(drRow(0)("mappingunkid"))
                End If

                drRow = mdtTable.Select("companyunkid = " & CInt(cboCompany.SelectedValue) & "  AND mappingunkid > 0 and attId = " & clsADAttribute_mapping.enAttributes.MiddleName)
                If drRow.Length > 0 Then
                    cboMiddleName.SelectedValue = CInt(drRow(0)("mappingunkid"))
                End If

                drRow = mdtTable.Select("companyunkid = " & CInt(cboCompany.SelectedValue) & "  AND mappingunkid > 0 and attId = " & clsADAttribute_mapping.enAttributes.LastName)
                If drRow.Length > 0 Then
                    cboLastName.SelectedValue = CInt(drRow(0)("mappingunkid"))
                End If

                drRow = mdtTable.Select("companyunkid = " & CInt(cboCompany.SelectedValue) & "  AND mappingunkid > 0 and attId = " & clsADAttribute_mapping.enAttributes.DisplayName)
                If drRow.Length > 0 Then
                    cboDisplayName.SelectedValue = CInt(drRow(0)("mappingunkid"))
                End If

                drRow = mdtTable.Select("companyunkid = " & CInt(cboCompany.SelectedValue) & "  AND mappingunkid > 0 and attId = " & clsADAttribute_mapping.enAttributes.JobTitle)
                If drRow.Length > 0 Then
                    cboJobtitle.SelectedValue = CInt(drRow(0)("mappingunkid"))
                End If

                drRow = mdtTable.Select("companyunkid = " & CInt(cboCompany.SelectedValue) & "  AND mappingunkid > 0 and attId = " & clsADAttribute_mapping.enAttributes.Department)
                If drRow.Length > 0 Then
                    cboDepartment.SelectedValue = CInt(drRow(0)("mappingunkid"))
                End If

                drRow = mdtTable.Select("companyunkid = " & CInt(cboCompany.SelectedValue) & "  AND mappingunkid > 0 and attId = " & clsADAttribute_mapping.enAttributes.EmailAddress)
                If drRow.Length > 0 Then
                    cboEmailAddress.SelectedValue = CInt(drRow(0)("mappingunkid"))
                End If

                drRow = mdtTable.Select("companyunkid = " & CInt(cboCompany.SelectedValue) & "  AND mappingunkid > 0 and attId = " & clsADAttribute_mapping.enAttributes.Telephone)
                If drRow.Length > 0 Then
                    cboTelephone.SelectedValue = CInt(drRow(0)("mappingunkid"))
                End If

                drRow = mdtTable.Select("companyunkid = " & CInt(cboCompany.SelectedValue) & "  AND mappingunkid > 0 and attId = " & clsADAttribute_mapping.enAttributes.Mobile)
                If drRow.Length > 0 Then
                    cboMobile.SelectedValue = CInt(drRow(0)("mappingunkid"))
                End If

                drRow = mdtTable.Select("companyunkid = " & CInt(cboCompany.SelectedValue) & "  AND mappingunkid > 0 and attId = " & clsADAttribute_mapping.enAttributes.City)
                If drRow.Length > 0 Then
                    cboCity.SelectedValue = CInt(drRow(0)("mappingunkid"))
                End If

                drRow = mdtTable.Select("companyunkid = " & CInt(cboCompany.SelectedValue) & "  AND mappingunkid > 0 and attId = " & clsADAttribute_mapping.enAttributes.State)
                If drRow.Length > 0 Then
                    cboRegion.SelectedValue = CInt(drRow(0)("mappingunkid"))
                End If

                drRow = mdtTable.Select("companyunkid = " & CInt(cboCompany.SelectedValue) & "  AND mappingunkid > 0 and attId = " & clsADAttribute_mapping.enAttributes.Country)
                If drRow.Length > 0 Then
                    cboCountry.SelectedValue = CInt(drRow(0)("mappingunkid"))
                End If


                'Pinkal (22-Feb-2020) -- Start
                'Enhancement NMB AD -   Active Directory Integration Requirement For NMB [Ref No : 273].
                drRow = mdtTable.Select("companyunkid = " & CInt(cboCompany.SelectedValue) & "  AND mappingunkid > 0 and attId = " & clsADAttribute_mapping.enAttributes.Office)
                If drRow.Length > 0 Then
                    cboOffice.SelectedValue = CInt(drRow(0)("mappingunkid"))
                End If

                drRow = mdtTable.Select("companyunkid = " & CInt(cboCompany.SelectedValue) & "  AND mappingunkid > 0 and attId = " & clsADAttribute_mapping.enAttributes.Street)
                If drRow.Length > 0 Then
                    cboStreet.SelectedValue = CInt(drRow(0)("mappingunkid"))
                End If

                drRow = mdtTable.Select("companyunkid = " & CInt(cboCompany.SelectedValue) & "  AND mappingunkid > 0 and attId = " & clsADAttribute_mapping.enAttributes.POBox)
                If drRow.Length > 0 Then
                    cboPOBoxNo.SelectedValue = CInt(drRow(0)("mappingunkid"))
                End If

                'Pinkal (22-Feb-2020) -- End


                'Pinkal (09-Mar-2020) -- Start
                'Enhancement NMB AD -   Active Directory Integration Requirement For NMB [Ref No : 273].
                drRow = mdtTable.Select("companyunkid = " & CInt(cboCompany.SelectedValue) & "  AND mappingunkid > 0 and attId = " & clsADAttribute_mapping.enAttributes.EntryPoint)
                If drRow.Length > 0 Then
                    cboEntryPoint.SelectedValue = CInt(drRow(0)("mappingunkid"))
                End If

                drRow = mdtTable.Select("companyunkid = " & CInt(cboCompany.SelectedValue) & "  AND mappingunkid > 0 and attId = " & clsADAttribute_mapping.enAttributes.Company)
                If drRow.Length > 0 Then
                    RemoveHandler cboCompany.SelectedValueChanged, AddressOf cboCompany_SelectedValueChanged
                    cboCompany.SelectedValue = CInt(drRow(0)("mappingunkid"))
                    AddHandler cboCompany.SelectedValueChanged, AddressOf cboCompany_SelectedValueChanged
                End If

                drRow = mdtTable.Select("companyunkid = " & CInt(cboCompany.SelectedValue) & "  AND mappingunkid > 0 and attId = " & clsADAttribute_mapping.enAttributes.HierarchyLevel)
                If drRow.Length > 0 Then
                    nudHierarchyLevel.Value = CInt(drRow(0)("mappingunkid"))
                End If
                'Pinkal (09-Mar-2020) -- End


                'Pinkal (04-Apr-2020) -- Start
                'Enhancement NMB AD -   Active Directory Integration Requirement For NMB [Ref No : 273].
                drRow = mdtTable.Select("companyunkid = " & CInt(cboCompany.SelectedValue) & "  AND mappingunkid > 0 and attId = " & clsADAttribute_mapping.enAttributes.Description)
                If drRow.Length > 0 Then
                    cboDescription.SelectedValue = CInt(drRow(0)("mappingunkid"))
                End If
                'Pinkal (04-Apr-2020) -- End

                'Pinkal (22-May-2020) -- End

                'Pinkal (18-Mar-2021) -- Start 
                'NMB Enhancmenet : AD Enhancement for NMB.
                drRow = mdtTable.Select("companyunkid = " & CInt(cboCompany.SelectedValue) & "  AND mappingunkid > 0 and attId = " & clsADAttribute_mapping.enAttributes.Transfer_Allocation)
                If drRow.Length > 0 Then
                    cboTransferAllocation.SelectedValue = CInt(drRow(0)("mappingunkid"))
                End If
                'Pinkal (18-Mar-2021) -- End

            End If

            FillList()

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "GetValue", mstrModuleName)
        End Try
    End Sub

    'Pinkal (01-Mar-2023) -- Start
    '(A1X-658) As a user, i want to have a port configuration option while configuring AD connection parameters As a user, i want to have a port configuration option while configuring AD conection parameters.

    'Private Sub GetOUFromAD()
    '    Dim objConfig As New clsConfigOptions
    '    Try
    '        Dim mstrADIPAddress As String = ""
    '        Dim mstrADDomain As String = ""
    '        Dim mstrADUserName As String = ""
    '        Dim mstrADUserPwd As String = ""

    '        objConfig.IsValue_Changed("ADIPAddress", "-999", mstrADIPAddress)
    '        objConfig.IsValue_Changed("ADDomain", "-999", mstrADDomain)
    '        objConfig.IsValue_Changed("ADDomainUser", "-999", mstrADUserName)
    '        objConfig.IsValue_Changed("ADDomainUserPwd", "-999", mstrADUserPwd)

    '        'Pinkal (01-Mar-2023) -- Start
    '        '(A1X-658) As a user, i want to have a port configuration option while configuring AD connection parameters As a user, i want to have a port configuration option while configuring AD conection parameters.
    '        Dim mstrADPortNo As String = ""
    '        objConfig.IsValue_Changed("ADPortNo", "-999", mstrADPortNo)
    '        'Pinkal (01-Mar-2023) -- End

    '        If mstrADUserPwd.Trim.Length > 0 Then
    '            mstrADUserPwd = clsSecurity.Decrypt(mstrADUserPwd, "ezee")
    '        End If

    '        Dim ar() As String = Nothing
    '        If mstrADDomain.Trim.Length > 0 AndAlso mstrADDomain.Trim.Contains(".") Then
    '            ar = mstrADDomain.Trim.Split(CChar("."))
    '            mstrADDomain = ""
    '            If ar.Length > 0 Then
    '                For i As Integer = 0 To ar.Length - 1
    '                    mstrADDomain &= ",DC=" & ar(i)
    '                Next
    '            End If
    '        End If

    '        If mstrADDomain.Trim.Length > 0 Then
    '            mstrADDomain = mstrADDomain.Trim.Substring(1)
    '        End If

    '        'Pinkal (01-Mar-2023) -- Start
    '        '(A1X-658) As a user, i want to have a port configuration option while configuring AD connection parameters As a user, i want to have a port configuration option while configuring AD conection parameters.
    '        Dim entry As DirectoryEntry = Nothing

    '        If mstrADPortNo.Trim.Length > 0 Then
    '            entry = New DirectoryEntry("LDAP://" & mstrADIPAddress.Trim & ":" & mstrADPortNo.Trim & "/" & mstrADDomain.Trim, mstrADUserName.Trim, mstrADUserPwd.Trim, AuthenticationTypes.Secure)
    '        Else
    '            entry = New DirectoryEntry("LDAP://" & mstrADIPAddress.Trim & "/" & mstrADDomain.Trim, mstrADUserName.Trim, mstrADUserPwd.Trim)
    '        End If
    '        'Pinkal (01-Mar-2023) -- End

    '        Dim objSearch As DirectorySearcher = New DirectorySearcher(entry)
    '        objSearch.Filter = ("(objectClass=organizationalUnit)")
    '        objSearch.SearchScope = SearchScope.Subtree
    '        objSearch.ReferralChasing = ReferralChasingOption.All
    '        objSearch.Sort.Direction = SortDirection.Ascending
    '        Dim srcResult As SearchResultCollection = objSearch.FindAll

    '        If srcResult.Count > 0 Then
    '            For Each sr As SearchResult In srcResult

    '                'Pinkal (15-Feb-2020) -- Start
    '                'Enhancement NMB AD -   Active Directory Integration Requirement For NMB [Ref No : 273].
    '                'Dim objEntry As New DirectoryEntry(sr.Path)
    '                Dim objEntry As New DirectoryEntry(sr.Path, mstrADUserName.Trim, mstrADUserPwd.Trim)
    '                'Pinkal (15-Feb-2020) -- End

    '                Dim dRow As DataRow = mdtTable.NewRow()
    '                dRow("attributename") = objEntry.Name
    '                dRow("oupath") = objEntry.Path
    '                mdtTable.Rows.Add(dRow)
    '                objEntry.Close()
    '            Next
    '            mdtTable.AcceptChanges()
    '        End If
    '        entry.Close()
    '    Catch ex As Exception
    '        DisplayError.Show("-1", ex.Message, "GetOUFromAD", mstrModuleName)
    '    Finally
    '        objConfig = Nothing
    '    End Try
    'End Sub


    Private Sub GetOUFromAD()
        Dim objConfig As New clsConfigOptions
        Try
            Dim mstrADIPAddress As String = ""
            Dim mstrADDomain As String = ""
            Dim mstrADUserName As String = ""
            Dim mstrADUserPwd As String = ""
            Dim mstrADPortNo As String = ""

            objConfig.IsValue_Changed("ADIPAddress", "-999", mstrADIPAddress)
            objConfig.IsValue_Changed("ADDomain", "-999", mstrADDomain)
            objConfig.IsValue_Changed("ADDomainUser", "-999", mstrADUserName)
            objConfig.IsValue_Changed("ADDomainUserPwd", "-999", mstrADUserPwd)
            objConfig.IsValue_Changed("ADPortNo", "-999", mstrADPortNo)

            If mstrADUserPwd.Trim.Length > 0 Then
                mstrADUserPwd = clsSecurity.Decrypt(mstrADUserPwd, "ezee")
            End If

            Dim ar() As String = Nothing
            If mstrADDomain.Trim.Length > 0 AndAlso mstrADDomain.Trim.Contains(".") Then
                ar = mstrADDomain.Trim.Split(CChar("."))
                mstrADDomain = ""
                If ar.Length > 0 Then
                    For i As Integer = 0 To ar.Length - 1
                        mstrADDomain &= ",DC=" & ar(i)
                    Next
                End If
            End If

            If mstrADDomain.Trim.Length > 0 Then
                mstrADDomain = mstrADDomain.Trim.Substring(1)
            End If

            Dim mstrAttributes() As String = New String() {"distinguishedname", "ou"}
            Dim objLDAP As LdapConnection = clsADIntegration.SetADConnection(mstrADIPAddress.Trim, mstrADDomain.Trim, mstrADPortNo.Trim, mstrADUserName.Trim, mstrADUserPwd.Trim)
            Dim objSSLSearch As New SearchRequest(mstrADDomain, "(objectClass=organizationalUnit)", Protocols.SearchScope.Subtree, mstrAttributes)
            Dim objResponse As SearchResponse = DirectCast(objLDAP.SendRequest(objSSLSearch), SearchResponse)
            If objResponse.ResultCode = ResultCode.Success Then
                Dim srcResult As SearchResultEntryCollection = objResponse.Entries
            If srcResult.Count > 0 Then
                    For Each sr As SearchResultEntry In srcResult
                        If sr.Attributes.Contains("ou") AndAlso sr.Attributes.Contains("distinguishedname") Then
                    Dim dRow As DataRow = mdtTable.NewRow()
                            dRow("attributename") = "OU=" & sr.Attributes("ou")(0).ToString()
                            dRow("oupath") = "LDAP://" & mstrADIPAddress.Trim & "/" & sr.Attributes("distinguishedname")(0).ToString()
                    mdtTable.Rows.Add(dRow)
            End If
                Next
                End If  '   If srcResult.Attributes.Count > 0 Then
            Else
                Throw New Exception(objResponse.ErrorMessage)
            End If  '  If objResponse.ResultCode = ResultCode.Success Then

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "GetOUFromAD", mstrModuleName)
        Finally
            objConfig = Nothing
        End Try
    End Sub

    'Pinkal (01-Mar-2023) -- End

    Private Sub FillList()
        Try

            dgOUMapping.AutoGenerateColumns = False

            Dim objMaster As New clsMasterData
            Dim dsList As DataSet = objMaster.GetEAllocation_Notification("List", "", False)

            'Pinkal (09-Mar-2020) -- Start
            'Enhancement NMB AD -   Active Directory Integration Requirement For NMB [Ref No : 273].

            Dim dtAllocation As DataTable = New DataView(dsList.Tables(0), "Id <> " & enAllocation.COST_CENTER, "", DataViewRowState.CurrentRows).ToTable()

            If dtAllocation IsNot Nothing AndAlso dtAllocation.Columns.Contains("Name") Then
                dtAllocation.Columns("Name").ColumnName = "attributename"
            End If

            If dtAllocation IsNot Nothing AndAlso dtAllocation.Columns.Contains("Id") Then
                dtAllocation.Columns("Id").ColumnName = "mappingunkid"
            End If

            Dim dRow As DataRow = dtAllocation.NewRow()
            dRow("mappingunkid") = 0
            dRow("attributename") = Language.getMessage(mstrModuleName, 1, "Select")
            dtAllocation.Rows.InsertAt(dRow, 0)

            With dgcolhAllocation
                .ValueMember = "mappingunkid"
                .DisplayMember = "attributename"
                .DataSource = dtAllocation
            End With
            objMaster = Nothing


            'Pinkal (09-Mar-2020) -- End


            mdView = mdtTable.DefaultView
            mdView.RowFilter = "oupath <> '' "

            'Pinkal (13-Aug-2020) -- Start
            'Bug NMB:  Working on IIS Freezing and Dump Issue for NMB.
            'If mdView.ToTable().Rows.Count > 10 Then
            'dgcolhAllocation.Width = 180
            'Else
            'dgcolhAllocation.Width = 195
            'End If
            'Pinkal (13-Aug-2020) -- End

            dgcolhOUName.DataPropertyName = "attributename"
            dgcolhAllocation.DataPropertyName = "mappingunkid"
            dgcolhAllocation.DropDownWidth = 150
            objdgcolhOUPath.DataPropertyName = "oupath"

            dgOUMapping.DataSource = mdView

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "FillList", mstrModuleName)
        End Try
    End Sub

#End Region

#Region " Form's Events "

    Private Sub frmADAttributeMapping_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Try
            Call Set_Logo(Me, gApplicationType)
            Language.setLanguage(Me.Name)
            Call OtherSettings()
            objAdAttribute = New clsADAttribute_mapping()
            mdtTable = objAdAttribute._dtTable().Copy()
            FillCombo()
            GetValue()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmADAttributeMapping_Load", mstrModuleName)
        End Try
    End Sub

#End Region

#Region " Button's Events "

    Private Sub btnSave_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSave.Click
        Try

            'Pinkal (09-Mar-2020) -- Start
            'Enhancement NMB AD -   Active Directory Integration Requirement For NMB [Ref No : 273].
            If CInt(cboCompany.SelectedValue) <= 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 2, "Company is compulsory information.Please Select company to do further operation on it."), enMsgBoxStyle.Information)
                cboEntryPoint.Select()
                Exit Sub
            ElseIf CInt(cboEntryPoint.SelectedValue) <= 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 4, "Entry Point is compulsory information.Please Select entry point for creating employee in specific OU."), enMsgBoxStyle.Information)
                cboEntryPoint.Select()
                Exit Sub
            End If
            'Pinkal (09-Mar-2020) -- End

            SetValue()
            If objAdAttribute.Insert(mdtTable) = False Then
                eZeeMsgBox.Show(objAdAttribute._Message, enMsgBoxStyle.Information)
            Else
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 3, "Active Directory attribute mapping saved successfully."), enMsgBoxStyle.Information)
                Me.Close()
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "btnSave_Click", mstrModuleName)
        End Try
    End Sub

    'Pinkal (09-Mar-2020) -- Start
    'Enhancement NMB AD -   Active Directory Integration Requirement For NMB [Ref No : 273].
    Private Sub btnMapHierarchy_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnMapHierarchy.Click
        Try
            If CInt(cboEntryPoint.SelectedValue) <= 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 4, "Entry Point is compulsory information.Please Select entry point for creating employee in specific OU."), enMsgBoxStyle.Information)
                cboEntryPoint.Select()
                Exit Sub
            ElseIf CInt(nudHierarchyLevel.Value) <= 1 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 5, "You can not map hierarchy.Reason : Hierarchy Level should be greater than 1 to map hierarchy."), enMsgBoxStyle.Information)
                nudHierarchyLevel.Select()
                Exit Sub
            End If

            Dim mblnIsSuccess As Boolean = False

            Dim objEntryhierarchy As New clsadentry_hierarchy
            Dim dsEntryHierarchy As DataSet = objEntryhierarchy.GetList("List", CInt(cboCompany.SelectedValue), True, 0, 0)

            If dsEntryHierarchy IsNot Nothing AndAlso dsEntryHierarchy.Tables(0).Rows.Count > 0 Then



            End If

            Dim frmMap As New frmADMapHierarchyLevel
            frmMap.displayDialog(CInt(cboCompany.SelectedValue), CInt(cboEntryPoint.SelectedValue), CInt(nudHierarchyLevel.Value), mblnIsSuccess)

            cboEntryPoint.Enabled = Not mblnIsSuccess
            nudHierarchyLevel.Enabled = Not mblnIsSuccess

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "btnMapHierarchy_Click", mstrModuleName)
        End Try
    End Sub
    'Pinkal (09-Mar-2020) -- End



    Private Sub btnClose_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnClose.Click
        Try
            Me.Close()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "btnClose_Click", mstrModuleName)
        End Try
    End Sub

#End Region

#Region "TextBox Event"

    Private Sub txtOUSearch_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txtOUSearch.TextChanged
        Try
            If mdView.Table.Rows.Count > 0 Then
                'mdView.RowFilter = "oupath <> '' AND attributename like '%" & txtOUSearch.Text.Trim & "%'"
                mdView.RowFilter = " oupath <> '' AND " & dgcolhOUName.DataPropertyName & " like '%" & txtOUSearch.Text.Trim & "%'"
                dgOUMapping.Refresh()
            End If

            If mdView.ToTable().Rows.Count > 10 Then
                dgcolhAllocation.Width = 180
            Else
                dgcolhAllocation.Width = 200
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "txtOUSearch_TextChanged", mstrModuleName)
        End Try
    End Sub

#End Region

#Region "DataGrid Events"

    Private Sub dgOUMapping_CellEnter(ByVal sender As System.Object, ByVal e As System.Windows.Forms.DataGridViewCellEventArgs) Handles dgOUMapping.CellEnter
        Try
            If dgcolhAllocation.Index = e.ColumnIndex Then
                SendKeys.Send("{F4}")
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "dgOUMapping_CellEnter", mstrModuleName)
        End Try
    End Sub

    Private Sub dgOUMapping_CellValueChanged(ByVal sender As System.Object, ByVal e As System.Windows.Forms.DataGridViewCellEventArgs) Handles dgOUMapping.CellValueChanged
        Try
            If e.RowIndex < 0 Then Exit Sub

            If e.ColumnIndex = dgcolhAllocation.Index Then
                If dgOUMapping.IsCurrentCellDirty Then
                    dgOUMapping.CommitEdit(DataGridViewDataErrorContexts.Commit)
                End If

                Dim drRow() As DataRow = mdView.Table.Select("oupath = '" & dgOUMapping.Rows(e.RowIndex).Cells(objdgcolhOUPath.Index).Value.ToString() & "'")
                If drRow.Length > 0 Then
                    If CInt(dgOUMapping.Rows(e.RowIndex).Cells(e.ColumnIndex).Value) > 0 Then
                        drRow(0)("isallocation") = True
                        dgOUMapping.Rows(e.RowIndex).ReadOnly = True
                    Else
                        drRow(0)("isallocation") = False
                        dgOUMapping.Rows(e.RowIndex).ReadOnly = False
                    End If
                    drRow(0).AcceptChanges()
                End If

                mdView.Table.AcceptChanges()
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "dgOUMapping_CellValueChanged", mstrModuleName)
        End Try
    End Sub

    Private Sub dgOUMapping_DataError(ByVal sender As Object, ByVal e As System.Windows.Forms.DataGridViewDataErrorEventArgs) Handles dgOUMapping.DataError

    End Sub

#End Region

    'Pinkal (22-May-2020) -- Start
    'ENHANCEMENT NMB:  Working on Exchange Server related changes in AD. 

#Region "ComboBox Event"

    Private Sub cboCompany_SelectedValueChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cboCompany.SelectedValueChanged
        Try

            'Pinkal (18-Jun-2020) -- Start
            'ENHANCEMENT NMB:  Working on Claim Approver Migration To change the Approver Logic.
            mdtTable = objAdAttribute._dtTable().Copy()
            'Pinkal (18-Jun-2020) -- End

            cboEmailAddress.SelectedValue = 0
            cboTelephone.SelectedValue = 0
            cboMobile.SelectedValue = 0
            cboCity.SelectedValue = 0
            cboRegion.SelectedValue = 0
            cboCountry.SelectedValue = 0
            cboStreet.SelectedValue = 0
            cboPOBoxNo.SelectedValue = 0
            cboDepartment.SelectedValue = 0
            cboEntryPoint.SelectedValue = 0
            cboDescription.SelectedValue = 0
            cboDisplayName.SelectedValue = 0
            nudHierarchyLevel.Value = 1
            GetValue()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "cboCompany_SelectedValueChanged", mstrModuleName)
        End Try
    End Sub

#End Region

    'Pinkal (22-May-2020) -- End







    '<Language> This Auto Generated Text Please Do Not Modify it.
#Region " Language & UI Settings "
    Private Sub OtherSettings()
        Try
            Me.SuspendLayout()
            Call SetLanguage()

            Me.btnSave.GradientBackColor = GUI._ButttonBackColor
            Me.btnSave.GradientForeColor = GUI._ButttonFontColor

            Me.btnClose.GradientBackColor = GUI._ButttonBackColor
            Me.btnClose.GradientForeColor = GUI._ButttonFontColor

			Me.btnMapHierarchy.GradientBackColor = GUI._ButttonBackColor 
			Me.btnMapHierarchy.GradientForeColor = GUI._ButttonFontColor


            Me.ResumeLayout()
        Catch Ex As Exception
            DisplayError.Show("-1", Ex.Message, "OtherSettings", mstrModuleName)
        End Try
    End Sub


    Private Sub SetLanguage()
        Try
            Me.Text = Language._Object.getCaption(Me.Name, Me.Text)

            Me.LblLoginUserName.Text = Language._Object.getCaption(Me.LblLoginUserName.Name, Me.LblLoginUserName.Text)
            Me.btnSave.Text = Language._Object.getCaption(Me.btnSave.Name, Me.btnSave.Text)
            Me.btnClose.Text = Language._Object.getCaption(Me.btnClose.Name, Me.btnClose.Text)
            Me.LblPassword.Text = Language._Object.getCaption(Me.LblPassword.Name, Me.LblPassword.Text)
            Me.LblFirstName.Text = Language._Object.getCaption(Me.LblFirstName.Name, Me.LblFirstName.Text)
            Me.LblMiddleName.Text = Language._Object.getCaption(Me.LblMiddleName.Name, Me.LblMiddleName.Text)
            Me.LblLastname.Text = Language._Object.getCaption(Me.LblLastname.Name, Me.LblLastname.Text)
            Me.LblDisplayName.Text = Language._Object.getCaption(Me.LblDisplayName.Name, Me.LblDisplayName.Text)
            Me.LblJobTitle.Text = Language._Object.getCaption(Me.LblJobTitle.Name, Me.LblJobTitle.Text)
            Me.LblDepartment.Text = Language._Object.getCaption(Me.LblDepartment.Name, Me.LblDepartment.Text)
            Me.LblEmailAddress.Text = Language._Object.getCaption(Me.LblEmailAddress.Name, Me.LblEmailAddress.Text)
            Me.LblTelephone.Text = Language._Object.getCaption(Me.LblTelephone.Name, Me.LblTelephone.Text)
            Me.LblCity.Text = Language._Object.getCaption(Me.LblCity.Name, Me.LblCity.Text)
            Me.LblState.Text = Language._Object.getCaption(Me.LblState.Name, Me.LblState.Text)
            Me.LblCountry.Text = Language._Object.getCaption(Me.LblCountry.Name, Me.LblCountry.Text)
            Me.lnPaymentRoundingOption.Text = Language._Object.getCaption(Me.lnPaymentRoundingOption.Name, Me.lnPaymentRoundingOption.Text)
			Me.LblMobile.Text = Language._Object.getCaption(Me.LblMobile.Name, Me.LblMobile.Text)
			Me.LblPOBox.Text = Language._Object.getCaption(Me.LblPOBox.Name, Me.LblPOBox.Text)
			Me.LblStreet.Text = Language._Object.getCaption(Me.LblStreet.Name, Me.LblStreet.Text)
			Me.LblOffice.Text = Language._Object.getCaption(Me.LblOffice.Name, Me.LblOffice.Text)
			Me.LblConsiderMainParent.Text = Language._Object.getCaption(Me.LblConsiderMainParent.Name, Me.LblConsiderMainParent.Text)
			Me.LblHierarchyLevel.Text = Language._Object.getCaption(Me.LblHierarchyLevel.Name, Me.LblHierarchyLevel.Text)
			Me.btnMapHierarchy.Text = Language._Object.getCaption(Me.btnMapHierarchy.Name, Me.btnMapHierarchy.Text)
			Me.lblCompany.Text = Language._Object.getCaption(Me.lblCompany.Name, Me.lblCompany.Text)
			Me.LblDescription.Text = Language._Object.getCaption(Me.LblDescription.Name, Me.LblDescription.Text)
			Me.dgcolhOUName.HeaderText = Language._Object.getCaption(Me.dgcolhOUName.Name, Me.dgcolhOUName.HeaderText)
			Me.dgcolhAllocation.HeaderText = Language._Object.getCaption(Me.dgcolhAllocation.Name, Me.dgcolhAllocation.HeaderText)
			Me.LblTransferAllocation.Text = Language._Object.getCaption(Me.LblTransferAllocation.Name, Me.LblTransferAllocation.Text)

        Catch Ex As Exception
            DisplayError.Show("-1", Ex.Message, "SetLanguage", mstrModuleName)
        End Try
    End Sub


    Private Sub SetMessages()
        Try
            Language.setMessage(mstrModuleName, 1, "Select")
			Language.setMessage(mstrModuleName, 2, "Company is compulsory information.Please Select company to do further operation on it.")
			Language.setMessage(mstrModuleName, 3, "Active Directory attribute mapping saved successfully.")
			Language.setMessage(mstrModuleName, 4, "Entry Point is compulsory information.Please Select entry point for creating employee in specific OU.")
			Language.setMessage(mstrModuleName, 5, "You can not map hierarchy.Reason : Hierarchy Level should be greater than 1 to map hierarchy.")

        Catch Ex As Exception
            DisplayError.Show("-1", Ex.Message, "SetMessages", mstrModuleName)
        End Try
    End Sub
#End Region 'Language & UI Settings
    '</Language>
End Class