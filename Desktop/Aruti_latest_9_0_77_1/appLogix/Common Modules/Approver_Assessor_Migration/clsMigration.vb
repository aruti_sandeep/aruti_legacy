﻿'************************************************************************************************************************************
'Class Name : clsMigration.vb
'Purpose    :
'Date       :19-04-12
'Written By :Sandeep J. Sharma
'Modified   :
'************************************************************************************************************************************

Imports eZeeCommonLib
''' <summary>
''' Purpose: 
''' Developer: Sandeep J. Sharma
''' </summary>
Public Class clsMigration
    Private Const mstrModuleName = "clsMigration"
    Dim mstrMessage As String = ""

#Region " Private variables "
    Private mintMigrationtranunkid As Integer
    Private mintMigrationfromunkid As Integer
    Private mintMigrationtounkid As Integer
    Private mdtMigrationdate As Date
    Private mstrMigratedemployees As String = String.Empty
    Private mintUsertypeid As Integer
    Private mintUserunkid As Integer
#End Region

#Region " Properties "

    'S.SANDEEP [28-May-2018] -- START 
    'ISSUE/ENHANCEMENT : {Audit Trails} 
    Private mstrFormName As String = String.Empty
    Public WriteOnly Property _FormName() As String
        Set(ByVal value As String)
            mstrFormName = value
        End Set
    End Property

    Private mstrClientIP As String = ""
    Public WriteOnly Property _ClientIP() As String
        Set(ByVal value As String)
            mstrClientIP = value
        End Set
    End Property

    Private mstrHostName As String = ""
    Public WriteOnly Property _HostName() As String
        Set(ByVal value As String)
            mstrHostName = value
        End Set
    End Property

    Private mblnIsWeb As Boolean = False
    Public WriteOnly Property _FromWeb() As Boolean
        Set(ByVal value As Boolean)
            mblnIsWeb = value
        End Set
    End Property

    Private mintAuditUserId As Integer = 0
    Public WriteOnly Property _AuditUserId() As Integer
        Set(ByVal value As Integer)
            mintAuditUserId = value
        End Set
    End Property

    Private mintCompanyUnkid As Integer = 0
    Public WriteOnly Property _CompanyUnkid() As Integer
        Set(ByVal value As Integer)
            mintCompanyUnkid = value
        End Set
    End Property

    Private mdtAuditDate As DateTime = Now
    Public WriteOnly Property _AuditDate() As DateTime
        Set(ByVal value As DateTime)
            mdtAuditDate = value
        End Set
    End Property

    Private xDataOpr As clsDataOperation = Nothing
    Public Property _xDataOpr() As clsDataOperation
        Get
            Return xDataOpr
        End Get
        Set(ByVal value As clsDataOperation)
            xDataOpr = value
        End Set
    End Property

    Private mintLoginEmployeeunkid As Integer = 0
    Public Property _LoginEmployeeunkid() As Integer
        Get
            Return mintLoginEmployeeunkid
        End Get
        Set(ByVal value As Integer)
            mintLoginEmployeeunkid = value
        End Set
    End Property

    Private mintVoidlogingemployeeunkid As Integer = 0
    Public Property _Voidloginemployeeunkid() As Integer
        Get
            Return mintVoidlogingemployeeunkid
        End Get
        Set(ByVal value As Integer)
            mintVoidlogingemployeeunkid = value
        End Set
    End Property
    'S.SANDEEP [28-May-2018] -- END 

    Public ReadOnly Property _Message() As String
        Get
            Return mstrMessage
        End Get
    End Property

    ''' <summary>
    ''' Purpose: Get or Set migrationtranunkid
    ''' Modify By: Sandeep J. Sharma
    ''' </summary>
    Public Property _Migrationtranunkid() As Integer
        Get
            Return mintMigrationtranunkid
        End Get
        Set(ByVal value As Integer)
            mintMigrationtranunkid = Value
            Call getData()
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set migrationfromunkid
    ''' Modify By: Sandeep J. Sharma
    ''' </summary>
    Public Property _Migrationfromunkid() As Integer
        Get
            Return mintMigrationfromunkid
        End Get
        Set(ByVal value As Integer)
            mintMigrationfromunkid = Value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set migrationtounkid
    ''' Modify By: Sandeep J. Sharma
    ''' </summary>
    Public Property _Migrationtounkid() As Integer
        Get
            Return mintMigrationtounkid
        End Get
        Set(ByVal value As Integer)
            mintMigrationtounkid = Value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set migrationdate
    ''' Modify By: Sandeep J. Sharma
    ''' </summary>
    Public Property _Migrationdate() As Date
        Get
            Return mdtMigrationdate
        End Get
        Set(ByVal value As Date)
            mdtMigrationdate = Value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set migratedemployees
    ''' Modify By: Sandeep J. Sharma
    ''' </summary>
    Public Property _Migratedemployees() As String
        Get
            Return mstrMigratedemployees
        End Get
        Set(ByVal value As String)
            mstrMigratedemployees = Value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set usertypeid
    ''' Modify By: Sandeep J. Sharma
    ''' </summary>
    Public Property _Usertypeid() As Integer
        Get
            Return mintUsertypeid
        End Get
        Set(ByVal value As Integer)
            mintUsertypeid = Value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set userunkid
    ''' Modify By: Sandeep J. Sharma
    ''' </summary>
    Public Property _Userunkid() As Integer
        Get
            Return mintUserunkid
        End Get
        Set(ByVal value As Integer)
            mintUserunkid = Value
        End Set
    End Property

#End Region

    ''' <summary>
    ''' Modify By: Sandeep J. Sharma
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>
    Public Sub GetData()
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        Dim objDataOperation As New clsDataOperation

        Try
            StrQ = "SELECT " & _
              "  migrationtranunkid " & _
              ", migrationfromunkid " & _
              ", migrationtounkid " & _
              ", migrationdate " & _
              ", migratedemployees " & _
              ", usertypeid " & _
              ", userunkid " & _
             "FROM hrmigration_tran " & _
             "WHERE migrationtranunkid = @migrationtranunkid "

            objDataOperation.AddParameter("@migrationtranunkid", SqlDbType.int, eZeeDataType.INT_SIZE, mintMigrationTranUnkId.ToString)

            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            For Each dtRow As DataRow In dsList.Tables(0).Rows
                mintmigrationtranunkid = CInt(dtRow.Item("migrationtranunkid"))
                mintmigrationfromunkid = CInt(dtRow.Item("migrationfromunkid"))
                mintmigrationtounkid = CInt(dtRow.Item("migrationtounkid"))
                mdtmigrationdate = dtRow.Item("migrationdate")
                mstrmigratedemployees = dtRow.Item("migratedemployees").ToString
                mintusertypeid = CInt(dtRow.Item("usertypeid"))
                mintuserunkid = CInt(dtRow.Item("userunkid"))
                Exit For
            Next
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: GetData; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
    End Sub

    ''' <summary>
    ''' Modify By: Sandeep J. Sharma
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>
    Public Function GetList(ByVal strTableName As String, Optional ByVal blnOnlyActive As Boolean = True) As DataSet
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        Dim objDataOperation As New clsDataOperation

        Try
            StrQ = "SELECT " & _
              "  migrationtranunkid " & _
              ", migrationfromunkid " & _
              ", migrationtounkid " & _
              ", migrationdate " & _
              ", migratedemployees " & _
              ", usertypeid " & _
              ", userunkid " & _
             "FROM hrmigration_tran "

            If blnOnlyActive Then
                strQ &= " WHERE isactive = 1 "
            End If

            dsList = objDataOperation.ExecQuery(strQ, strTableName)

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: GetList; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
        Return dsList
    End Function

    '' <summary>
    '' Modify By: Sandeep J. Sharma
    '' </summary>
    '' <returns>Boolean</returns>
    '' <purpose> INSERT INTO Database Table (hrmigration_tran) </purpose>

    'Nilay (05-May-2016) -- Start
    'Public Function Insert(ByVal objDataOperation As clsDataOperation) As Boolean
    Public Function Insert(ByVal objDataOperation As clsDataOperation, Optional ByVal intUserunkid As Integer = 0) As Boolean
        'Nilay (05-May-2016) -- End

        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        Try
            objDataOperation.ClearParameters()
            objDataOperation.AddParameter("@migrationfromunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintMigrationfromunkid.ToString)
            objDataOperation.AddParameter("@migrationtounkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintMigrationtounkid.ToString)
            objDataOperation.AddParameter("@migrationdate", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, mdtMigrationdate.ToString)
            objDataOperation.AddParameter("@migratedemployees", SqlDbType.Text, eZeeDataType.NAME_SIZE, mstrMigratedemployees.ToString)
            objDataOperation.AddParameter("@usertypeid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintUsertypeid.ToString)
            objDataOperation.AddParameter("@userunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintUserunkid.ToString)

            strQ = "INSERT INTO hrmigration_tran ( " & _
              "  migrationfromunkid " & _
              ", migrationtounkid " & _
              ", migrationdate " & _
              ", migratedemployees " & _
              ", usertypeid " & _
              ", userunkid" & _
            ") VALUES (" & _
              "  @migrationfromunkid " & _
              ", @migrationtounkid " & _
              ", @migrationdate " & _
              ", @migratedemployees " & _
              ", @usertypeid " & _
              ", @userunkid" & _
            "); SELECT @@identity"

            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            mintMigrationtranunkid = dsList.Tables(0).Rows(0).Item(0)

            'S.SANDEEP [28-May-2018] -- START
            'ISSUE/ENHANCEMENT : {Audit Trails} 
            Dim objCommonATLog As New clsCommonATLog
            objCommonATLog._FormName = mstrFormName
            objCommonATLog._LoginEmployeeUnkid = mintLoginEmployeeunkid
            objCommonATLog._ClientIP = mstrClientIP
            objCommonATLog._HostName = mstrHostName
            objCommonATLog._FromWeb = mblnIsWeb
            objCommonATLog._AuditUserId = mintAuditUserId
            objCommonATLog._CompanyUnkid = mintCompanyUnkid
            objCommonATLog._AuditDate = mdtAuditDate
            'S.SANDEEP [28-May-2018] -- END

            If objCommonATLog.Insert_AtLog(objDataOperation, 1, "hrmigration_tran", "migrationtranunkid", mintMigrationtranunkid, , intUserunkid) = False Then

                exForce = New Exception(objDataOperation.ErrorNumber & " : " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            'S.SANDEEP [28-May-2018] -- START
            'ISSUE/ENHANCEMENT : {Audit Trails} 
            objCommonATLog = Nothing
            'S.SANDEEP [28-May-2018] -- END


            Return True
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: Insert; Module Name: " & mstrModuleName)
            Return False
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
        End Try
    End Function

    '''' <summary>
    '''' Modify By: Sandeep J. Sharma
    '''' </summary>
    '''' <returns>Boolean</returns>
    '''' <purpose> Update Database Table (hrmigration_tran) </purpose>
    'Public Function Update() As Boolean
    '    Dim dsList As DataSet = Nothing
    '    Dim strQ As String = ""
    '    Dim exForce As Exception

    '    objDataOperation = New clsDataOperation

    '    Try
    '        objDataOperation.AddParameter("@migrationtranunkid", SqlDbType.int, eZeeDataType.INT_SIZE, mintmigrationtranunkid.ToString)
    '        objDataOperation.AddParameter("@migrationfromunkid", SqlDbType.int, eZeeDataType.INT_SIZE, mintmigrationfromunkid.ToString)
    '        objDataOperation.AddParameter("@migrationtounkid", SqlDbType.int, eZeeDataType.INT_SIZE, mintmigrationtounkid.ToString)
    '        objDataOperation.AddParameter("@migrationdate", SqlDbType.datetime, eZeeDataType.DATETIME_SIZE, mdtmigrationdate.ToString)
    '        objDataOperation.AddParameter("@migratedemployees", SqlDbType.text, eZeeDataType.NAME_SIZE, mstrmigratedemployees.ToString)
    '        objDataOperation.AddParameter("@usertypeid", SqlDbType.int, eZeeDataType.INT_SIZE, mintusertypeid.ToString)
    '        objDataOperation.AddParameter("@userunkid", SqlDbType.int, eZeeDataType.INT_SIZE, mintuserunkid.ToString)

    '        StrQ = "UPDATE hrmigration_tran SET " & _
    '          "  migrationfromunkid = @migrationfromunkid" & _
    '          ", migrationtounkid = @migrationtounkid" & _
    '          ", migrationdate = @migrationdate" & _
    '          ", migratedemployees = @migratedemployees" & _
    '          ", usertypeid = @usertypeid" & _
    '          ", userunkid = @userunkid " & _
    '        "WHERE migrationtranunkid = @migrationtranunkid "

    '        Call objDataOperation.ExecNonQuery(strQ)

    '        If objDataOperation.ErrorMessage <> "" Then
    '            exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
    '            Throw exForce
    '        End If

    '        Return True
    '    Catch ex As Exception
    '        Throw New Exception(ex.Message & "; Procedure Name: Update; Module Name: " & mstrModuleName)
    '        Return False
    '    Finally
    '        exForce = Nothing
    '        If dsList IsNot Nothing Then dsList.Dispose()
    '        objDataOperation = Nothing
    '    End Try
    'End Function

    '''' <summary>
    '''' Modify By: Sandeep J. Sharma
    '''' </summary>
    '''' <returns>Boolean</returns>
    '''' <purpose> Delete Database Table (hrmigration_tran) </purpose>
    'Public Function Delete(ByVal intUnkid As Integer) As Boolean
    '    If isUsed(intUnkid) Then
    '        mstrMessage = "<Message>"
    '        Return False
    '    End If

    '    Dim dsList As DataSet = Nothing
    '    Dim strQ As String = ""
    '    Dim exForce As Exception

    '    objDataOperation = New clsDataOperation

    '    Try
    '        StrQ = "DELETE FROM hrmigration_tran " & _
    '        "WHERE migrationtranunkid = @migrationtranunkid "

    '        objDataOperation.AddParameter("@migrationtranunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intUnkid)

    '        Call objDataOperation.ExecNonQuery(strQ)

    '        If objDataOperation.ErrorMessage <> "" Then
    '            exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
    '            Throw exForce
    '        End If

    '        Return True
    '    Catch ex As Exception
    '        Throw New Exception(ex.Message & "; Procedure Name: Update; Module Name: " & mstrModuleName)
    '        Return False
    '    Finally
    '        exForce = Nothing
    '        If dsList IsNot Nothing Then dsList.Dispose()
    '        objDataOperation = Nothing
    '    End Try
    'End Function

    '''' <summary>
    '''' Modify By: Sandeep J. Sharma
    '''' </summary>
    '''' <purpose> Assign all Property variable </purpose>
    'Public Function isUsed(ByVal intUnkid As Integer) As Boolean
    '    Dim dsList As DataSet = Nothing
    '    Dim strQ As String = ""
    '    Dim exForce As Exception

    '    objDataOperation = New clsDataOperation

    '    Try
    '        StrQ = "<Query>"

    '        objDataOperation.AddParameter("@migrationtranunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intUnkid)

    '        dsList = objDataOperation.ExecQuery(strQ, "List")

    '        If objDataOperation.ErrorMessage <> "" Then
    '            exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
    '            Throw exForce
    '        End If

    '        Return dsList.tables(0).rows.count > 0
    '    Catch ex As Exception
    '        Throw New Exception(ex.Message & "; Procedure Name: isUsed; Module Name: " & mstrModuleName)
    '    Finally
    '        exForce = Nothing
    '        If dsList IsNot Nothing Then dsList.Dispose()
    '        objDataOperation = Nothing
    '    End Try
    'End Function

    '''' <summary>
    '''' Modify By: Sandeep J. Sharma
    '''' </summary>
    '''' <purpose> Assign all Property variable </purpose>
    'Public Function isExist(ByVal strCode As String, ByVal strName As String, Optional ByVal intUnkid As Integer = -1) As Boolean
    '    Dim dsList As DataSet = Nothing
    '    Dim strQ As String = ""
    '    Dim exForce As Exception

    '    objDataOperation = New clsDataOperation

    '    Try
    '        StrQ = "SELECT " & _
    '          "  migrationtranunkid " & _
    '          ", migrationfromunkid " & _
    '          ", migrationtounkid " & _
    '          ", migrationdate " & _
    '          ", migratedemployees " & _
    '          ", usertypeid " & _
    '          ", userunkid " & _
    '         "FROM hrmigration_tran " & _
    '         "WHERE name = @name " & _
    '         "AND code = @code "

    '        If intUnkid > 0 Then
    '            strQ &= " AND migrationtranunkid <> @migrationtranunkid"
    '        End If

    '        objDataOperation.AddParameter("@code", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, strCode)
    '        objDataOperation.AddParameter("@name", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, strName)
    '        objDataOperation.AddParameter("@migrationtranunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intUnkid)

    '        dsList = objDataOperation.ExecQuery(strQ, "List")

    '        If objDataOperation.ErrorMessage <> "" Then
    '            exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
    '            Throw exForce
    '        End If

    '        Return dsList.tables(0).rows.count > 0
    '    Catch ex As Exception
    '        Throw New Exception(ex.Message & "; Procedure Name: isExist; Module Name: " & mstrModuleName)
    '    Finally
    '        exForce = Nothing
    '        If dsList IsNot Nothing Then dsList.Dispose()
    '        objDataOperation = Nothing
    '    End Try
    'End Function

End Class
