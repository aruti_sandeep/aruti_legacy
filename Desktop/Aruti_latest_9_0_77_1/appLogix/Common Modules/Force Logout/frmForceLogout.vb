﻿Option Strict On

Imports eZeeCommonLib
Imports Aruti.Data
Imports System

Public Class frmForceLogout

#Region " Private Variables "
    Private ReadOnly mstrModuleName As String = "frmForceLogout"

    Private mstrTranDatabaseName As String
    Private m_Dataview As DataView
#End Region

#Region " Private Methods "

    Private Sub FillCombo()
        Dim objCompany As New clsCompany_Master
        Dim dsCombo As DataSet
        Try
            dsCombo = objCompany.GetList("Company", True)
            Dim dr As DataRow = dsCombo.Tables("Company").NewRow

            dr.Item("companyunkid") = 0
            dr.Item("code") = Language.getMessage(mstrModuleName, 1, "Configuration")
            dr.Item("name") = Language.getMessage(mstrModuleName, 1, "Configuration")
            dsCombo.Tables("Company").Rows.InsertAt(dr, 0)
            dsCombo.Tables("Company").AcceptChanges()

            With cboCompany
                .ValueMember = "companyunkid"
                .DisplayMember = "name"
                .DataSource = dsCombo.Tables("Company")

                'Dim lItem As New ComboBoxValue(Language.getMessage(mstrModuleName, 1, "Configuration"), 0)
                '.Items.Insert(0, lItem)
                .SelectedValue = 0
            End With
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "FillCombo", mstrModuleName)
        End Try
    End Sub

    Private Sub FillList()
        Dim objUserLog As New clsUser_tracking_Tran
        Dim strFilter As String

        Try
            Cursor.Current = Cursors.WaitCursor

            strFilter = " logout_date IS NULL " & _
                        " AND login_modeid IS NOT NULL " & _
                        " AND isforcelogout = 0 " & _
                        " AND NOT (cfuser_tracking_tran.userunkid = " & User._Object._Userunkid & " AND ip = '" & getIP().ToString & "') "

            If CInt(cboCompany.SelectedValue) > 0 Then
                m_Dataview = New DataView(objUserLog.GetUserLog("List", enArutiApplicatinType.Aruti_Payroll, CInt(cboCompany.SelectedValue), mstrTranDatabaseName, (cboViewBy.SelectedIndex + 1).ToString, , strFilter).Tables(0))
            Else
                m_Dataview = New DataView(objUserLog.GetUserLog("List", enArutiApplicatinType.Aruti_Configuration, , , , , strFilter).Tables(0))
            End If

            m_Dataview.Table.Columns.Add("IsChecked", System.Type.GetType("System.Boolean")).DefaultValue = False

            If txtSearch.Text <> "" Then
                m_Dataview.RowFilter = "username LIKE '%" & txtSearch.Text & "%' "
            Else
                m_Dataview.RowFilter = ""
            End If

            With dgvData
                .AutoGenerateColumns = False
                .DataSource = m_Dataview

                dgcolhIsCheck.DataPropertyName = "IsChecked"
                dgcolhUser.DataPropertyName = "username"
                dgcolhLoginDate.DataPropertyName = "logindate"
                dgcolhLogoutDate.DataPropertyName = "logout_date"
                dgcolhIP.DataPropertyName = "ip"
                dgcolhMachine.DataPropertyName = "machine"
                dgcolhReloginDate.DataPropertyName = "relogin_date"

                .Refresh()
            End With

            Cursor.Current = Cursors.Default
        Catch ex As Exception
            Cursor.Current = Cursors.Default
            DisplayError.Show("-1", ex.Message, "FillList", mstrModuleName)
        End Try
    End Sub

    Private Function IsValid() As Boolean
        Dim dtTable As DataTable
        Try

            If txtReason.Text.Trim = "" Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 5, "Please enter Logout reason. Logout reason is mandatory information."), enMsgBoxStyle.Information)
                txtReason.Focus()
                Return False
            End If

            dtTable = New DataView(m_Dataview.ToTable, "IsChecked = 1 ", "", DataViewRowState.CurrentRows).ToTable

            If dtTable.Rows.Count <= 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 6, "Please Tick atleast one user from list to forcefully Logout."), enMsgBoxStyle.Information)
                Return False
            End If

            Return True
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "", mstrModuleName)
        End Try
    End Function
#End Region

#Region " Form's Evnets "

    Private Sub frmForceLogout_FormClosed(ByVal sender As Object, ByVal e As System.Windows.Forms.FormClosedEventArgs) Handles Me.FormClosed
        Try

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmForceLogout_FormClosed", mstrModuleName)
        End Try
    End Sub

    Private Sub frmForceLogout_KeyDown(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles Me.KeyDown
        Try

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmForceLogout_KeyDown", mstrModuleName)
        End Try
    End Sub

    Private Sub frmForceLogout_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            Call Set_Logo(Me, gApplicationType)
            Language.setLanguage(Me.Name)
            Call OtherSettings()
            Call FillCombo()
            Call FillList()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmForceLogout_Load", mstrModuleName)
        End Try
    End Sub

    'Pinkal (10-Jul-2014) -- Start
    'Enhancement - Language Settings

    Private Sub Form_LanguageClick(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Me.LanguageClick
        Dim objfrm As New frmLanguage
        Try
            If User._Object._Isrighttoleft = True Then
                objfrm.RightToLeft = Windows.Forms.RightToLeft.Yes
                objfrm.RightToLeftLayout = True
                Call Language.ctlRightToLeftlayOut(objfrm)
            End If

            Call SetMessages()
            objfrm.displayDialog(Me)
            Call SetLanguage()

        Catch ex As System.Exception
            Call DisplayError.Show("-1", ex.Message, "Form_LanguageClick", mstrModuleName)
        Finally
            objfrm.Dispose()
            objfrm = Nothing
        End Try
    End Sub

    'Pinkal (10-Jul-2014) -- End

#End Region

#Region " ComboBox's Events "
    Private Sub cboCompany_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cboCompany.SelectedIndexChanged
        Dim dsList As DataSet
        Try
            With cboViewBy
                .Items.Clear()
                .Items.Add(Language.getMessage(mstrModuleName, 2, "DeskTop"))
                Select Case CInt(cboCompany.SelectedValue)
                    Case Is > 0
                        .Items.Add(Language.getMessage(mstrModuleName, 3, "Employee Self Service"))
                        .Items.Add(Language.getMessage(mstrModuleName, 4, "Manager Self Service"))

                        Dim objMaster As New clsMasterData
                        dsList = objMaster.GetCompanyCurrentYearInfo("List", CInt(cboCompany.SelectedValue))
                        If dsList.Tables("List").Rows.Count > 0 Then
                            mstrTranDatabaseName = dsList.Tables("List").Rows(0).Item("database_name").ToString
                        End If
                End Select
                .SelectedIndex = 0
            End With

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "cboCompany_SelectedIndexChanged", mstrModuleName)
        End Try
    End Sub

    Private Sub cboViewBy_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cboViewBy.SelectedIndexChanged
        Try
            Call FillList()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "cboViewBy_SelectedIndexChanged", mstrModuleName)
        End Try
    End Sub
#End Region

#Region " Checkbox's Events "
    Private Sub objchkAll_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objchkAll.CheckedChanged
        Try
            If m_Dataview Is Nothing Then Exit Sub

            For Each dtRow As DataRowView In m_Dataview
                dtRow.Item("IsChecked") = objchkAll.Checked
            Next
            dgvData.DataSource = m_Dataview
            dgvData.Refresh()

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objchkAll_CheckedChanged", mstrModuleName)
        End Try
    End Sub
#End Region

#Region " Button's Events "
    Private Sub btnClose_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnClose.Click
        Try
            Me.Close()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "btnClose_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub btnSet_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSet.Click
        Try
            If m_Dataview Is Nothing Then Exit Sub

            If dtpReloginDate.Checked = True AndAlso dtpReloginDate.Value.Date < ConfigParameter._Object._CurrentDateAndTime.Date Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 7, "Sorry! Relogin date should be greater than or equal to current date."), enMsgBoxStyle.Information)
                dtpReloginDate.Focus()
                Exit Sub
            End If

            If dtpReloginDate.Checked = True Then
                If dtpReloginDate.Value.Date = ConfigParameter._Object._CurrentDateAndTime.Date AndAlso dtpTime.Checked = False Then
                    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 8, "Sorry! Relogin time is mandatory information Please set relogin time."), enMsgBoxStyle.Information)
                    dtpReloginDate.Focus()
                    Exit Sub
                End If
            End If

            'S.SANDEEP [ 29 JAN 2013 ] -- START
            'ENHANCEMENT : TRA CHANGES
            Dim dtDateTime As DateTime
            If dtpReloginDate.Checked = True AndAlso dtpTime.Checked = True Then
                dtDateTime = Convert.ToDateTime(dtpReloginDate.Value.Date.ToShortDateString + " " + dtpTime.Value.ToString("HH:mm"))
            ElseIf dtpReloginDate.Checked = True AndAlso dtpTime.Checked = False Then
                dtDateTime = dtpReloginDate.Value.Date
            ElseIf dtpReloginDate.Checked = False AndAlso dtpTime.Checked = True Then
                dtDateTime = Convert.ToDateTime(Now.Date.ToShortDateString + " " + dtpTime.Value.ToString("HH:mm"))
            Else
                dtDateTime = Nothing
            End If
            'S.SANDEEP [ 29 JAN 2013 ] -- END

            If radApplyAll.Checked = True Then

                For Each dtRow As DataRowView In m_Dataview
                    'S.SANDEEP [ 29 JAN 2013 ] -- START
                    'ENHANCEMENT : TRA CHANGES
                    'If dtpReloginDate.Checked = True Then
                    '    dtRow.Item("relogin_date") = dtpReloginDate.Value.Date
                    'Else
                    '    dtRow.Item("relogin_date") = DBNull.Value
                    'End If
                    If dtDateTime <> Nothing Then
                        dtRow.Item("relogin_date") = dtDateTime
                    Else
                        dtRow.Item("relogin_date") = DBNull.Value
                    End If
                    'S.SANDEEP [ 29 JAN 2013 ] -- END
                Next
                dgvData.DataSource = m_Dataview
                dgvData.Refresh()

            Else

                For Each dtRow As DataRowView In m_Dataview
                    If IsDBNull(dtRow.Item("IsChecked")) = False AndAlso CBool(dtRow.Item("IsChecked")) = True Then
                        'S.SANDEEP [ 29 JAN 2013 ] -- START
                        'ENHANCEMENT : TRA CHANGES
                        'If dtpReloginDate.Checked = True Then
                        '    dtRow.Item("relogin_date") = dtpReloginDate.Value.Date
                        'Else
                        '    dtRow.Item("relogin_date") = DBNull.Value
                        'End If
                        If dtDateTime <> Nothing Then
                            dtRow.Item("relogin_date") = dtDateTime
                        Else
                            dtRow.Item("relogin_date") = DBNull.Value
                        End If
                        'S.SANDEEP [ 29 JAN 2013 ] -- END
                    End If
                Next
                dgvData.DataSource = m_Dataview
                dgvData.Refresh()

            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "btnSet_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub btnLogout_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnLogout.Click
        Dim dtTable As DataTable
        Dim dtRow As DataRow
        Dim dicTracking_Tran As New Dictionary(Of Integer, ArrayList)
        Dim arrList As ArrayList
        Dim menLogin_Mode As enLogin_Mode
        Dim blnResult As Boolean

        Try
            If m_Dataview Is Nothing Then Exit Sub

            If IsValid() = False Then Exit Sub

            dtTable = New DataView(m_Dataview.ToTable, "IsChecked = 1 ", "", DataViewRowState.CurrentRows).ToTable

            Dim intCount As Integer = dtTable.Rows.Count
            For i As Integer = 0 To intCount - 1
                dtRow = dtTable.Rows(i)

                arrList = New ArrayList
                arrList.Add(CInt(dtRow.Item("userunkid"))) 'UserUnkID
                arrList.Add(dtRow.Item("employeecode").ToString) 'Employeecode
                arrList.Add(dtRow.Item("relogin_date")) 'relogin_date

                If dicTracking_Tran.ContainsKey(CInt(dtRow.Item("trackingunkid"))) = False Then
                    dicTracking_Tran.Add(CInt(dtRow.Item("trackingunkid")), arrList)
                End If

            Next
            Select Case CInt(cboViewBy.SelectedIndex) + 1
                Case enLogin_Mode.DESKTOP
                    menLogin_Mode = enLogin_Mode.DESKTOP
                Case enLogin_Mode.EMP_SELF_SERVICE
                    menLogin_Mode = enLogin_Mode.EMP_SELF_SERVICE
                Case enLogin_Mode.MGR_SELF_SERVICE
                    menLogin_Mode = enLogin_Mode.MGR_SELF_SERVICE
            End Select

            Dim objUserTrackin As New clsUser_tracking_Tran
            With objUserTrackin
                ._FormName = mstrModuleName
                ._LoginEmployeeunkid = 0
                ._ClientIP = getIP()
                ._HostName = getHostName()
                ._FromWeb = False
                ._AuditUserId = User._Object._Userunkid
._CompanyUnkid = Company._Object._Companyunkid
                ._AuditDate = ConfigParameter._Object._CurrentDateAndTime
            End With
            blnResult = objUserTrackin.UpdateForceLogout(menLogin_Mode, dicTracking_Tran, True, txtReason.Text.Trim, User._Object._Userunkid)

            If blnResult = False AndAlso objUserTrackin._Message <> "" Then
                eZeeMsgBox.Show(objUserTrackin._Message, enMsgBoxStyle.Information)
            Else
                Call FillList()
                txtReason.Text = ""
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 6, "Process completed successfully."), enMsgBoxStyle.Information)
            End If

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "btnLogout_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub btnSearch_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSearch.Click
        Try
            Call FillList()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "btnSearch_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub btnReset_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnReset.Click
        Try
            txtSearch.Text = ""
            If cboCompany.SelectedIndex <> 0 Then cboCompany.SelectedIndex = 0
            If cboViewBy.SelectedIndex <> 0 Then cboViewBy.SelectedIndex = 0

            Call FillList()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "btnReset_Click", mstrModuleName)
        End Try
    End Sub
#End Region

#Region " Textbox's Events "
    Private Sub txtSearch_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles txtSearch.TextChanged
        Try
            If m_Dataview Is Nothing Then Exit Sub

            If txtSearch.Text <> "" Then
                m_Dataview.RowFilter = "username LIKE '%" & txtSearch.Text & "%' "
            Else
                m_Dataview.RowFilter = ""
            End If

            dgvData.DataSource = m_Dataview
            dgvData.Refresh()

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "txtSearch_TextChanged", mstrModuleName)
        End Try
    End Sub
#End Region

	'<Language> This Auto Generated Text Please Do Not Modify it.
#Region " Language & UI Settings "
	Private Sub OtherSettings()
		Try
			Me.SuspendLayout()
			
			Call SetLanguage()
			
			Me.btnClose.GradientBackColor = GUI._ButttonBackColor 
			Me.btnClose.GradientForeColor = GUI._ButttonFontColor

			Me.btnLogout.GradientBackColor = GUI._ButttonBackColor 
			Me.btnLogout.GradientForeColor = GUI._ButttonFontColor

			Me.btnSet.GradientBackColor = GUI._ButttonBackColor 
			Me.btnSet.GradientForeColor = GUI._ButttonFontColor


			Me.ResumeLayout()
		Catch Ex As Exception
			DisplayError.Show("-1", ex.Message, "OtherSettings", mstrModuleName)
		End Try
	End Sub
			
			
	Private Sub SetLanguage()
		Try
			Me.Text = Language._Object.getCaption(Me.Name, Me.Text)
			
			Me.btnClose.Text = Language._Object.getCaption(Me.btnClose.Name, Me.btnClose.Text)
			Me.btnLogout.Text = Language._Object.getCaption(Me.btnLogout.Name, Me.btnLogout.Text)
			Me.btnSet.Text = Language._Object.getCaption(Me.btnSet.Name, Me.btnSet.Text)
			Me.radApplyToChecked.Text = Language._Object.getCaption(Me.radApplyToChecked.Name, Me.radApplyToChecked.Text)
			Me.radApplyAll.Text = Language._Object.getCaption(Me.radApplyAll.Name, Me.radApplyAll.Text)
			Me.lblCaption.Text = Language._Object.getCaption(Me.lblCaption.Name, Me.lblCaption.Text)
			Me.lblLogoutReason.Text = Language._Object.getCaption(Me.lblLogoutReason.Name, Me.lblLogoutReason.Text)
			Me.lblViewBy.Text = Language._Object.getCaption(Me.lblViewBy.Name, Me.lblViewBy.Text)
			Me.lblCompany.Text = Language._Object.getCaption(Me.lblCompany.Name, Me.lblCompany.Text)
			Me.dgcolhIsCheck.HeaderText = Language._Object.getCaption(Me.dgcolhIsCheck.Name, Me.dgcolhIsCheck.HeaderText)
			Me.dgcolhUser.HeaderText = Language._Object.getCaption(Me.dgcolhUser.Name, Me.dgcolhUser.HeaderText)
			Me.dgcolhLoginDate.HeaderText = Language._Object.getCaption(Me.dgcolhLoginDate.Name, Me.dgcolhLoginDate.HeaderText)
			Me.dgcolhLogoutDate.HeaderText = Language._Object.getCaption(Me.dgcolhLogoutDate.Name, Me.dgcolhLogoutDate.HeaderText)
			Me.dgcolhIP.HeaderText = Language._Object.getCaption(Me.dgcolhIP.Name, Me.dgcolhIP.HeaderText)
			Me.dgcolhMachine.HeaderText = Language._Object.getCaption(Me.dgcolhMachine.Name, Me.dgcolhMachine.HeaderText)
			Me.dgcolhReloginDate.HeaderText = Language._Object.getCaption(Me.dgcolhReloginDate.Name, Me.dgcolhReloginDate.HeaderText)
			Me.btnSearch.Text = Language._Object.getCaption(Me.btnSearch.Name, Me.btnSearch.Text)
			Me.btnReset.Text = Language._Object.getCaption(Me.btnReset.Name, Me.btnReset.Text)

		Catch Ex As Exception
			DisplayError.Show("-1", ex.Message, "SetLanguage", mstrModuleName)
		End Try
	End Sub
			
			
	Private Sub SetMessages()
		Try
			Language.setMessage(mstrModuleName, 1, "Configuration")
			Language.setMessage(mstrModuleName, 2, "DeskTop")
			Language.setMessage(mstrModuleName, 3, "Employee Self Service")
			Language.setMessage(mstrModuleName, 4, "Manager Self Service")
			Language.setMessage(mstrModuleName, 5, "Please enter Logout reason. Logout reason is mandatory information.")
			Language.setMessage(mstrModuleName, 6, "Please Tick atleast one user from list to forcefully Logout.")
			Language.setMessage(mstrModuleName, 7, "Sorry! Relogin date should be greater than or equal to current date.")
			Language.setMessage(mstrModuleName, 8, "Sorry! Relogin time is mandatory information Please set relogin time.")
			Language.setMessage(mstrModuleName, 6, "Process completed successfully.")

		Catch Ex As Exception
			DisplayError.Show("-1", ex.Message, "SetMessages", mstrModuleName)
		End Try
	End Sub
#End Region 'Language & UI Settings
	'</Language>
End Class