﻿Option Strict On

#Region " Imports "

Imports eZeeCommonLib
Imports Aruti.Data

#End Region

Public Class frmCommonValidationList

#Region " Private Variables "
    Private ReadOnly mstrModuleName As String = "frmCommonValidationList"
    Private mblnYesClicked As Boolean = False

    Private mblnShowYesNo As Boolean = False
    Private mstrMessage As String = ""
    Private mdtTable As DataTable = Nothing
#End Region

#Region " Display Dialog "

    Public Function displayDialog(ByVal blnShowYesNo As Boolean, ByVal strMessage As String, ByVal dtTable As DataTable) As Boolean
        Try
            mblnShowYesNo = blnShowYesNo
            mstrMessage = strMessage
            mdtTable = dtTable

            Me.ShowDialog()

            Return mblnYesClicked
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "displayDialog", mstrModuleName)
        End Try
    End Function

#End Region

#Region " Form's Events "
    Private Sub frmCommonValidationList_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            If mstrMessage.Trim <> "" Then gbValidationInformation.Text = "  " & mstrMessage

            If mblnShowYesNo = True Then
                objpnlOkCancel.Visible = False
                objpnlYesNo.Visible = True
            Else
                objpnlOkCancel.Visible = True
                'Hemant (31 July 2018) -- Start
                'Enhancement : Support Issue Id # 2410 - Changes to stop Payroll process of employees who had payment was already done 
                btnOk.Visible = False
                'Hemant (31 July 2018) -- End
                objpnlYesNo.Visible = False
            End If

            dgvList.AutoGenerateColumns = True
            dgvList.AutoSizeColumnsMode = DataGridViewAutoSizeColumnsMode.AllCells 'Sohail (25 Aug 2022)
            dgvList.DataSource = mdtTable
            dgvList.Refresh()

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmCommonValidationList_Load", mstrModuleName)
        End Try
    End Sub

    Private Sub Form_LanguageClick(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Me.LanguageClick
        Dim objfrm As New frmLanguage
        Try
            If User._Object._Isrighttoleft = True Then
                objfrm.RightToLeft = Windows.Forms.RightToLeft.Yes
                objfrm.RightToLeftLayout = True
                Call Language.ctlRightToLeftlayOut(objfrm)
            End If

            Call SetMessages()

            'clsPayrollProcessTran.SetMessages()
            'objfrm._Other_ModuleNames = "clsPayrollProcessTran"
            objfrm.displayDialog(Me)

            Call SetLanguage()

        Catch ex As System.Exception
            Call DisplayError.Show("-1", ex.Message, "Form_LanguageClick", mstrModuleName)
        Finally
            objfrm.Dispose()
            objfrm = Nothing
        End Try
    End Sub
#End Region

#Region " Button's Events "

    Private Sub btnClose_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnClose.Click
        Try
            Me.Close()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "btnClose_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub btnOk_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnOk.Click
        Try
            Me.Close()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "btnOk_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub btnNo_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnNo.Click
        Try
            Me.Close()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "btnNo_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub btnYes_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnYes.Click
        Try
            mblnYesClicked = True
            Me.Close()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "btnYes_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub btnExport_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnExport.Click, btnExport1.Click
        Try
            If mdtTable Is Nothing OrElse mdtTable.Rows.Count <= 0 Then Exit Try

            Dim dsDataSet As New DataSet
            'S.SANDEEP [12-Jan-2018] -- START
            'ISSUE/ENHANCEMENT : REF-ID # 0001843
            'Dim IExcel As New ExcelData
            'S.SANDEEP [12-Jan-2018] -- END

            Dim strFilePath As String = String.Empty
            Dim dlgSaveFile As New SaveFileDialog
            Dim ObjFile As IO.FileInfo

            dlgSaveFile.Filter = "Execl files(*.xlsx)|*.xlsx"
            dlgSaveFile.FilterIndex = 1

            If dlgSaveFile.ShowDialog = Windows.Forms.DialogResult.OK Then
                ObjFile = New IO.FileInfo(dlgSaveFile.FileName)
                If ObjFile.Exists = True Then ObjFile.Delete()

                strFilePath = dlgSaveFile.FileName

                dsDataSet.Tables.Clear()
                dsDataSet.Tables.Add(mdtTable.Copy)
                'S.SANDEEP [12-Jan-2018] -- START
                'ISSUE/ENHANCEMENT : REF-ID # 0001843
                'IExcel.Export(strFilePath, dsDataSet)
                OpenXML_Export(strFilePath, dsDataSet)
                'S.SANDEEP [12-Jan-2018] -- END

                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 1, "Data Exported Successfully"), enMsgBoxStyle.Information)
                'S.SANDEEP [12-Jan-2018] -- START
                'ISSUE/ENHANCEMENT : REF-ID # 0001843
                'IExcel = Nothing
                'S.SANDEEP [12-Jan-2018] -- END
                dsDataSet = Nothing
            End If

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "btnExport_Click", mstrModuleName)
        End Try
    End Sub
#End Region

#Region " Datagridview Events "
    Private Sub dgvList_DataBindingComplete(ByVal sender As Object, ByVal e As System.Windows.Forms.DataGridViewBindingCompleteEventArgs) Handles dgvList.DataBindingComplete
        Try
            Dim lstCols As List(Of DataColumn) = (From p In CType(dgvList.DataSource, DataTable).Columns Where (CType(p, DataColumn).DataType Is System.Type.GetType("System.Decimal") OrElse CType(p, DataColumn).DataType Is System.Type.GetType("System.Int32")) Select (CType(p, DataColumn))).ToList
            For Each col In lstCols
                dgvList.Columns(col.ColumnName).DefaultCellStyle.Alignment = DataGridViewContentAlignment.TopRight
            Next

            'Sohail (25 Aug 2022) -- Start
            'Enhancement : AC2-793 : VFT - As a user, I want to be able to stop over deduction where net pay goes below a certain percentage of employee gross or basic.
            'If dgvList.ColumnCount > 0 Then dgvList.Columns(0).AutoSizeMode = DataGridViewAutoSizeColumnMode.Fill
            'Sohail (25 Aug 2022) -- End

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "dgvList_DataBindingComplete", mstrModuleName)
        End Try
    End Sub
#End Region
    
	'<Language> This Auto Generated Text Please Do Not Modify it.
#Region " Language & UI Settings "
	Private Sub OtherSettings()
		Try
			Me.SuspendLayout()
			 
			Call SetLanguage()
			
			Me.gbValidationInformation.GradientColor = GUI._eZeeContainerHeaderBackColor 
			Me.gbValidationInformation.ForeColor = GUI._eZeeContainerHeaderForeColor 


			Me.btnOk.GradientBackColor = GUI._ButttonBackColor 
			Me.btnOk.GradientForeColor = GUI._ButttonFontColor

			Me.btnClose.GradientBackColor = GUI._ButttonBackColor 
			Me.btnClose.GradientForeColor = GUI._ButttonFontColor

			Me.btnYes.GradientBackColor = GUI._ButttonBackColor 
			Me.btnYes.GradientForeColor = GUI._ButttonFontColor

			Me.btnNo.GradientBackColor = GUI._ButttonBackColor 
			Me.btnNo.GradientForeColor = GUI._ButttonFontColor

			Me.btnExport.GradientBackColor = GUI._ButttonBackColor 
			Me.btnExport.GradientForeColor = GUI._ButttonFontColor

			Me.btnExport1.GradientBackColor = GUI._ButttonBackColor 
			Me.btnExport1.GradientForeColor = GUI._ButttonFontColor


			Me.ResumeLayout()
		Catch Ex As Exception
			DisplayError.Show("-1", ex.Message, "OtherSettings", mstrModuleName)
		End Try
	End Sub
			
			
	Private Sub SetLanguage()
		Try
			Me.Text = Language._Object.getCaption(Me.Name, Me.Text)
			
			Me.gbValidationInformation.Text = Language._Object.getCaption(Me.gbValidationInformation.Name, Me.gbValidationInformation.Text)
			Me.btnOk.Text = Language._Object.getCaption(Me.btnOk.Name, Me.btnOk.Text)
			Me.btnClose.Text = Language._Object.getCaption(Me.btnClose.Name, Me.btnClose.Text)
			Me.btnYes.Text = Language._Object.getCaption(Me.btnYes.Name, Me.btnYes.Text)
			Me.btnNo.Text = Language._Object.getCaption(Me.btnNo.Name, Me.btnNo.Text)
			Me.btnExport.Text = Language._Object.getCaption(Me.btnExport.Name, Me.btnExport.Text)
			Me.btnExport1.Text = Language._Object.getCaption(Me.btnExport1.Name, Me.btnExport1.Text)

		Catch Ex As Exception
			DisplayError.Show("-1", ex.Message, "SetLanguage", mstrModuleName)
		End Try
	End Sub
			
			
	Private Sub SetMessages()
		Try
			Language.setMessage(mstrModuleName, 1, "Data Exported Successfully")

		Catch Ex As Exception
			DisplayError.Show("-1", ex.Message, "SetMessages", mstrModuleName)
		End Try
	End Sub
#End Region 'Language & UI Settings
	'</Language>
End Class