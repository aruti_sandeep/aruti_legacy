﻿#Region " Imports "

Imports eZeeCommonLib
Imports Aruti.Data

#End Region

Public Class frmATLogView_New

#Region " Private Variables "

    Private mstrModuleName As String = "frmATLogView_New"
    Dim mSize As Size
    Dim strColumnName() As String = Nothing
    Dim StrName As String = String.Empty
    Dim StrParentNode As String = String.Empty
    Dim dtChild As New DataTable
    Dim dtDataSource As DataTable
    Dim dTable As DataTable
    Dim objCommonATLog As clsCommonATLog
    Dim dvAuditView As DataView
    Dim dvChildView As DataView
    'Sohail (17 Apr 2019) -- Start
    'Enhancement - 76.1 - AT Log for Payroll Mobule.
    Private mstrSearchGridText As String = String.Empty
    Private mstrSearchListText As String = String.Empty
    'Sohail (17 Apr 2019) -- End

#End Region

#Region " Private Methods "

    Private Sub FillCombo()
        Try
            Dim dsList As New DataSet
            'S.SANDEEP [ 13 AUG 2012 ] -- START
            'ENHANCEMENT : TRA CHANGES
            'Dim objUser As New clsUserAddEdit
            'Dim dsList As DataSet = objUser.getComboList("List", True)
            'With cboUser
            '    .DisplayMember = "name"
            '    .ValueMember = "userunkid"
            '    .DataSource = dsList.Tables(0)
            'End With
            With cboViewBy
                .Items.Clear()
                .Items.Add(Language.getMessage(mstrModuleName, 100, "Select"))
                .Items.Add(Language.getMessage(mstrModuleName, 105, "DeskTop"))
                Select Case gApplicationType
                    Case enArutiApplicatinType.Aruti_Payroll
                        .Items.Add(Language.getMessage(mstrModuleName, 106, "Employee Self Service"))
                        .Items.Add(Language.getMessage(mstrModuleName, 107, "Manager Self Service"))
                End Select
                .SelectedIndex = 0
            End With
            'S.SANDEEP [ 13 AUG 2012 ] -- END

            RemoveHandler cboEventType.SelectedIndexChanged, AddressOf cboEventType_SelectedIndexChanged

            Dim objAudit As New clsMasterData
            dsList = objAudit.GetAuditTypeList("Audit", True, True, True)
            With cboEventType
                .ValueMember = "id"
                .DisplayMember = "name"
                .DataSource = dsList.Tables(0)
            End With
            objAudit = Nothing
            AddHandler cboEventType.SelectedIndexChanged, AddressOf cboEventType_SelectedIndexChanged

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "FillCombo", mstrModuleName)
        End Try
    End Sub

    Private Function Set_Master_Filter(Optional ByVal blnIsDetail As Boolean = False) As String
        Dim StrFilter As String = String.Empty
        'S.SANDEEP [ 13 AUG 2012 ] -- START
        'ENHANCEMENT : TRA CHANGES
        Dim blnFlag As Boolean = False
        'S.SANDEEP [ 13 AUG 2012 ] -- END
        Try
            If CInt(cboFilter.SelectedValue) > 0 Then
                'S.SANDEEP [ 13 AUG 2012 ] -- START
                'ENHANCEMENT : TRA CHANGES
                'If blnIsDetail = False Then
                '    StrFilter &= "AND audituserunkid = '" & CInt(cboFilter.SelectedValue) & "' "
                'Else
                '    StrFilter &= "AND Uid = '" & CInt(cboFilter.SelectedValue) & "' "
                'End If
                Select Case cboViewBy.SelectedIndex
                    Case 1, 3   'DESKTOP,MSS
                        If blnIsDetail = False Then
                            StrFilter &= "AND audituserunkid = '" & CInt(cboFilter.SelectedValue) & "' "
                        Else
                            StrFilter &= "AND Uid = '" & CInt(cboFilter.SelectedValue) & "' "
                        End If
                    Case 2  'ESS
                        StrFilter &= "AND loginemployeeunkid = '" & CInt(cboFilter.SelectedValue) & "' "
                End Select
                'S.SANDEEP [ 13 AUG 2012 ] -- END
                blnFlag = True
            End If

            If CInt(cboEventType.SelectedValue) > 0 Then
                If blnIsDetail = False Then
                    'StrFilter &= "AND audittype = '" & CInt(cboEventType.SelectedValue) & "' "
                Else
                    StrFilter &= "AND ETYId = '" & CInt(cboEventType.SelectedValue) & "' "
                End If
            End If

            If dtpFromDate.Checked = True Then
                StrFilter &= "AND ADate >= '" & eZeeDate.convertDate(dtpFromDate.Value) & "' "
            End If

            If dtpTodate.Checked = True Then
                StrFilter &= "AND ADate <= '" & eZeeDate.convertDate(dtpTodate.Value) & "' "
            End If

            If txtIPAddress.Text.Trim.Length > 0 Then
                StrFilter &= "AND [IP Address] LIKE '%" & txtIPAddress.Text & "%' "
            End If

            If txtMachine.Text.Trim.Length > 0 Then
                StrFilter &= "AND [Machine Name] LIKE '%" & txtMachine.Text & "%' "
            End If

            If blnIsDetail = False Then
                If txtModuleName.Text.Trim.Length > 0 Then
                    StrFilter &= "AND [Screen Name] LIKE '%" & txtModuleName.Text & "%' "
                End If
            End If

            'S.SANDEEP [ 13 AUG 2012 ] -- START
            'ENHANCEMENT : TRA CHANGES
            If blnFlag = False Then
                If lvSummary.SelectedItems.Count > 0 Then
                    Select Case gApplicationType
                        Case enArutiApplicatinType.Aruti_Payroll
                            If dtDataSource.Columns.Contains("User/Employee") Then
                                StrFilter &= "AND [User/Employee] LIKE '" & lvSummary.SelectedItems(0).Text & "' "
                            ElseIf dtDataSource.Columns.Contains("User") Then
                                StrFilter &= "AND [User] LIKE '" & lvSummary.SelectedItems(0).Text & "' "
                            End If
                        Case enArutiApplicatinType.Aruti_Configuration
                            StrFilter &= "AND [User] LIKE '" & lvSummary.SelectedItems(0).Text & "' "
                    End Select
                End If
            End If

            If cboViewBy.SelectedIndex > 0 Then
                If dtDataSource IsNot Nothing Then
                    If dtDataSource.Columns.Contains("ModeId") Then
                        StrFilter &= "AND ModeId = '" & cboViewBy.SelectedIndex & "' "
                    End If
                Else
                    StrFilter &= "AND ModeId = '" & cboViewBy.SelectedIndex & "' "
                End If
            End If
            'S.SANDEEP [ 13 AUG 2012 ] -- END

            If StrFilter.Trim.Length > 0 Then
                StrFilter = StrFilter.Substring(3)
            End If

            Return StrFilter

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "Set_Master_Filter", mstrModuleName)
            Return ""
        End Try
    End Function

    Private Sub Fill_Child_Report()
        Try

            If dgvAuditData.RowCount > 0 Then
                lvData.Items.Clear()
                lvData.Columns.Clear()
            Else
                lvData.Items.Clear()
                lvData.Columns.Clear()
                Exit Sub
            End If


            Dim StrFilter As String = String.Empty

            If CInt(cboEventType.SelectedValue) > 0 Then
                StrFilter = "AND ETYId = '" & CInt(cboEventType.SelectedValue) & "' "
            End If

            dtChild = objCommonATLog.Get_Child_Data(StrName, CStr(cboChildData.SelectedValue))

            Select Case cboViewBy.SelectedIndex

                Case 0

                    If dtChild.Columns.Contains("User/Employee") Then
                        If lvSummary.SelectedItems.Count > 0 Then
                            StrFilter &= "AND [User/Employee] LIKE '" & lvSummary.SelectedItems(0).Text & "' "
                        End If
                    End If

                    If dtChild.Columns.Contains("User") Then
                        If lvSummary.SelectedItems.Count > 0 Then
                            StrFilter &= "AND [User] LIKE '" & lvSummary.SelectedItems(0).Text & "' "
                        End If
                    End If

                Case 1, 3
                    If dtChild.Columns.Contains("User") Then
                        If lvSummary.SelectedItems.Count > 0 Then
                            'StrFilter &= "AND [User] LIKE '" & lvSummary.SelectedItems(0).Text & "' "

                            'Pinkal (03-Apr-2013) -- Start
                            'Enhancement : TRA Changes

                            'If dtChild.Columns.Contains("ModeId") Then
                            '    StrFilter &= "AND ModeId = " & cboViewBy.SelectedIndex
                            'End If

                            'Pinkal (03-Apr-2013) -- End


                        End If
                    End If

                Case 2
                    If dtChild.Columns.Contains("User/Employee") Then
                        If lvSummary.SelectedItems.Count > 0 Then
                            StrFilter &= "AND [User/Employee] LIKE '" & lvSummary.SelectedItems(0).Text & "' "

                            'Pinkal (03-Apr-2013) -- Start
                            'Enhancement : TRA Changes

                            'If dtChild.Columns.Contains("ModeId") Then
                            '    StrFilter &= "  AND ModeId = " & cboViewBy.SelectedIndex
                            'End If

                            'Pinkal (03-Apr-2013) -- End


                        End If
                    End If
            End Select


            'Pinkal (03-Apr-2013) -- Start
            'Enhancement : TRA Changes
            If cboViewBy.SelectedIndex > 0 AndAlso dtChild.Columns.Contains("ModeId") Then
                StrFilter &= "AND ModeId = " & cboViewBy.SelectedIndex
            End If
            'Pinkal (03-Apr-2013) -- End


            'Varsha (23 Nov 2017) -- Start
            'Enhancement: Payroll Budget AT Log.
            If dtpFromDate.Checked = True Then
                StrFilter &= "AND ADate >= '" & eZeeDate.convertDate(dtpFromDate.Value) & "' "
            End If

            If dtpTodate.Checked = True Then
                StrFilter &= "AND ADate <= '" & eZeeDate.convertDate(dtpTodate.Value) & "' "
            End If
            'Varsha (23 Nov 2017) -- End

            If StrFilter.Trim.Length > 0 Then
                StrFilter = StrFilter.Substring(3)
            End If

            dtChild = New DataView(dtChild, StrFilter, "GName", DataViewRowState.CurrentRows).ToTable

            dtChild.Columns.Remove("Uid") : dtChild.Columns.Remove("ETYId")
            dtChild.Columns.Remove("ADate")
            dtChild.Columns("GName").ColumnName = cboChildData.Text

            If dtChild.Columns.Contains("ModeId") Then
                dtChild.Columns.Remove("ModeId")
            End If

            'If dtChild.Rows.Count > 0 Then
            '    Dim strColumns As String = String.Empty
            '    Dim iCntColumns As Integer = 0

            '    For Each dCol As DataColumn In dtChild.Columns
            '        lvData.Columns.Add(dCol.ColumnName, 100, HorizontalAlignment.Left)
            '        strColumns &= "," & dCol.ColumnName
            '        iCntColumns += 1
            '    Next
            '    strColumns = Mid(strColumns, 2)
            '    Dim strColumnName() As String = strColumns.Split(CChar(","))

            '    For Each dRow As DataRow In dtChild.Rows
            '        Dim lvItem As New ListViewItem
            '        For i As Integer = 0 To iCntColumns - 1
            '            Select Case i
            '                Case 0
            '                    lvItem.Text = dRow.Item(strColumnName(i)).ToString
            '                Case Else
            '                    lvItem.SubItems.Add(dRow.Item(strColumnName(i)).ToString)
            '            End Select
            '        Next
            '        lvData.Items.Add(lvItem)
            '    Next

            '    lvData.Columns(0).Width = 0
            '    lvData.GroupingColumn = lvData.Columns(0)
            '    lvData.DisplayGroups(True)
            'End If

            dvChildView = dtChild.DefaultView
            dgvChildDetails.DataSource = dvChildView

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "Fill_Child_Report", mstrModuleName)
        End Try
    End Sub

    Private Function Store_User_Log(ByVal blnIsSearch As Boolean) As Boolean
        Dim objViewAT As New clsView_AT_Logs
        Try
            objViewAT._Atviewmodeid = clsView_AT_Logs.enAT_View_Mode.APPLICANT_EVENT_LOG
            objViewAT._Userunkid = User._Object._Userunkid
            objViewAT._Viewdatetime = ConfigParameter._Object._CurrentDateAndTime
            If blnIsSearch = True Then
                objViewAT._Viewoperationid = clsView_AT_Logs.enAT_OperationId.SEARCH
            Else
                objViewAT._Viewoperationid = clsView_AT_Logs.enAT_OperationId.EXPORT
            End If

            Dim StrXML_Value As String = ""
            StrXML_Value = "<" & Me.Name & " xmlns:xsi=""http://www.w3.org/2001/XMLSchema-instance"">" & vbCrLf

            For Each ctrl As Control In gbGroupList.Controls
                If TypeOf ctrl Is DateTimePicker Then
                    If CType(ctrl, DateTimePicker).Checked = True Then
                        StrXML_Value &= "<" & ctrl.Name & ">" & eZeeDate.convertDate(CType(ctrl, DateTimePicker).Value) & "</" & ctrl.Name & ">" & vbCrLf
                    Else
                        StrXML_Value &= "<" & ctrl.Name & "/>" & vbCrLf
                    End If
                ElseIf TypeOf ctrl Is ComboBox Then
                    If CType(ctrl, ComboBox).SelectedIndex >= 0 Then
                        If CType(ctrl, ComboBox).DataSource IsNot Nothing Then
                            StrXML_Value &= "<" & ctrl.Name & ">" & CInt(CType(ctrl, ComboBox).SelectedValue) & "</" & ctrl.Name & ">" & vbCrLf
                        Else
                            StrXML_Value &= "<" & ctrl.Name & ">" & CType(ctrl, ComboBox).SelectedIndex & "</" & ctrl.Name & ">" & vbCrLf
                        End If
                    Else
                        StrXML_Value &= "<" & ctrl.Name & "/>" & vbCrLf
                    End If
                ElseIf TypeOf ctrl Is RadioButton Then
                    If CType(ctrl, RadioButton).Checked = True Then
                        StrXML_Value &= "<" & ctrl.Name & ">" & CType(ctrl, RadioButton).Checked & "</" & ctrl.Name & ">" & vbCrLf
                    Else
                        StrXML_Value &= "<" & ctrl.Name & "/>" & vbCrLf
                    End If
                ElseIf TypeOf ctrl Is TextBox Then
                    If CType(ctrl, TextBox).Text <> "" Then
                        StrXML_Value &= "<" & ctrl.Name & ">" & CType(ctrl, TextBox).Text & "</" & ctrl.Name & ">" & vbCrLf
                    Else
                        StrXML_Value &= "<" & ctrl.Name & "/>" & vbCrLf
                    End If
                End If
            Next

            For Each ctrl As Control In gbList.Controls
                If TypeOf ctrl Is ListView Then
                    If CType(ctrl, ListView).SelectedItems.Count > 0 Then
                        StrXML_Value &= "<" & CType(ctrl, ListView).Name & ">" & CType(ctrl, ListView).SelectedItems(0).Tag.ToString.Split("|")(0) & "</" & CType(ctrl, ListView).Name & ">" & vbCrLf
                    Else
                        StrXML_Value &= "<" & ctrl.Name & "/>" & vbCrLf
                    End If
                End If
            Next

            If CInt(cboEventType.SelectedValue) > 0 Then
                StrXML_Value &= "<" & cboEventType.Name & ">" & cboEventType.SelectedValue & "</" & cboEventType.Name & ">" & vbCrLf
            Else
                StrXML_Value &= "<" & cboEventType.Name & "/>" & vbCrLf
            End If

            If cboChildData.Visible = True Then
                StrXML_Value &= "<" & cboChildData.Name & ">" & cboChildData.Text & "</" & cboChildData.Name & ">" & vbCrLf
            End If

            StrXML_Value &= "</" & Me.Name & ">"

            objViewAT._Value_View = StrXML_Value

            If objViewAT.Insert = False Then
                eZeeMsgBox.Show(objViewAT._Message, enMsgBoxStyle.Information)
                Return False
            End If

            Return True

        Catch ex As Exception
            Call DisplayError.Show("-1", ex.Message, "Store_User_Log", mstrModuleName)
        Finally
            objViewAT = Nothing
        End Try
    End Function

    Private Function EscapeLikeValue(ByVal value As String) As String
        Dim sb As New System.Text.StringBuilder(value.Length)
        For i As Integer = 0 To value.Length - 1
            Dim c As Char = value(i)
            Select Case c
                Case "]"c, "["c, "%"c, "*"c
                    sb.Append("[").Append(c).Append("]")
                    Exit Select
                Case "'"c
                    sb.Append("''")
                    Exit Select
                Case Else
                    sb.Append(c)
                    Exit Select
            End Select
        Next
        Return sb.ToString()
    End Function

    Private Function GetFilterString(ByVal strCriteria As String, ByVal dvDataView As DataView, ByVal dgvData As DataGridView) As String
        Dim strFilter As String = String.Empty
        'Sohail (17 Apr 2019) -- Start
        'Enhancement - 76.1 - AT Log for Payroll Mobule.
        Dim strSubFilter As String = String.Empty
        'Sohail (17 Apr 2019) -- End
        Try
            'Sohail (17 Apr 2019) -- Start
            'Enhancement - 76.1 - AT Log for Payroll Mobule.
            'If dgvData.RowCount > 0 Then
            '    For Each dgCol As DataGridViewColumn In dgvData.Columns
            '        If dvDataView.Table.Columns(dgCol.DataPropertyName).DataType Is GetType(System.String) Then
            '            strFilter &= "OR [" & dgCol.DataPropertyName & "] LIKE '%" & EscapeLikeValue(strCriteria) & "%' "
            '        End If
            '    Next
            '    If strFilter.Trim.Length > 0 Then strFilter = Mid(strFilter, 3)
            'End If
            If dvDataView IsNot Nothing Then
                For Each strC As String In strCriteria.Split(CChar(" "))
                    strSubFilter = ""
                For Each dgCol As DataGridViewColumn In dgvData.Columns
                    If dvDataView.Table.Columns(dgCol.DataPropertyName).DataType Is GetType(System.String) Then
                            strSubFilter &= "OR [" & dgCol.DataPropertyName & "] LIKE '%" & EscapeLikeValue(strC) & "%' "
                    End If
                Next
                    If strSubFilter.Trim.Length > 0 Then strFilter &= "AND ( " & Mid(strSubFilter, 3) & " ) "
                Next
                If strFilter.Trim.Length > 0 Then strFilter = Mid(strFilter, 4)
            End If
            'Sohail (17 Apr 2019) -- End      
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "GetFilterString", mstrModuleName)
        Finally
        End Try
        Return strFilter
    End Function

    'Sohail (17 Apr 2019) -- Start
    'Enhancement - 76.1 - AT Log for Payroll Mobule.
    Private Sub SetDefaultSearchText(ByVal txt As TextBox)
        Try
            With txt
                .ForeColor = Color.Gray
                If txt.Name = txtSearchGrid.Name Then
                    RemoveHandler txt.TextChanged, AddressOf txtSearchGrid_TextChanged
                    .Text = mstrSearchGridText
                    AddHandler txt.TextChanged, AddressOf txtSearchGrid_TextChanged
                Else
                    RemoveHandler txt.TextChanged, AddressOf txtSearchList_TextChanged
                    .Text = mstrSearchListText
                    AddHandler txt.TextChanged, AddressOf txtSearchList_TextChanged
                End If
                .Font = New Font(Me.Font.FontFamily, Me.Font.Size, FontStyle.Italic)
            End With
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "SetDefaultSearchText", mstrModuleName)
        End Try
    End Sub

    Private Sub SetRegularFont(ByVal txt As TextBox)
        Try
            With txt
                .ForeColor = Color.Black
                .Font = New Font(Me.Font.FontFamily, Me.Font.Size, FontStyle.Regular)
                .SelectionLength = 0
            End With
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "SetRegularFont", mstrModuleName)
        End Try
    End Sub
    'Sohail (17 Apr 2019) -- End

#End Region

#Region " Form's Events "

    'Sohail (15 Jan 2018) -- Start
    'Enhancement: Payroll Budget AT Log in 70.1.
    Private Sub frmATLogView_New_KeyDown(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles Me.KeyDown
        Try
            If e.KeyCode = Keys.Return Then
                btnSearch.PerformClick()
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmATLogView_New_KeyDown", mstrModuleName)
        End Try
    End Sub
    'Sohail (15 Jan 2018) -- End

    Private Sub frmATLogView_New_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        objCommonATLog = New clsCommonATLog
        Try
            Call Set_Logo(Me, gApplicationType)

            'Pinkal (10-Jul-2014) -- Start
            'Enhancement - Language Settings
            Language.setLanguage(Me.Name)
            OtherSettings()
            'Pinkal (10-Jul-2014) -- End

            Call FillCombo()
            gbList.Collapse()
            gbDetails.Collapse()
            mSize = flpPanel1.Size
            gbDetails.Size = New Point(1012, gbDetails.Size.Height)
            gbList.Size = New Point(1012, gbList.Size.Height)
            Call btnReset_Click(sender, e)

            'Sohail (17 Apr 2019) -- Start
            'Enhancement - 76.1 - AT Log for Payroll Mobule.
            mstrSearchGridText = Language.getMessage(mstrModuleName, 1, "Type to Search")
            mstrSearchListText = Language.getMessage(mstrModuleName, 2, "Type to Search")

            Call SetDefaultSearchText(txtSearchGrid)
            Call SetDefaultSearchText(txtSearchList)
            'Sohail (17 Apr 2019) -- End

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmATLogView_New_Load", mstrModuleName)
        End Try
    End Sub


    'Pinkal (10-Jul-2014) -- Start
    'Enhancement - Language Settings

    Private Sub Form_LanguageClick(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Me.LanguageClick
        Dim objfrm As New frmLanguage
        Try
            If User._Object._Isrighttoleft = True Then
                objfrm.RightToLeft = Windows.Forms.RightToLeft.Yes
                objfrm.RightToLeftLayout = True
                Call Language.ctlRightToLeftlayOut(objfrm)
            End If

            Call SetMessages()
            objfrm.displayDialog(Me)
            Call SetLanguage()

        Catch ex As System.Exception
            Call DisplayError.Show("-1", ex.Message, "Form_LanguageClick", mstrModuleName)
        Finally
            objfrm.Dispose()
            objfrm = Nothing
        End Try
    End Sub
    'Pinkal (10-Jul-2014) -- End


#End Region

#Region " Buttons "

    Private Sub btnSearch_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSearch.Click
        Dim dsList As New DataSet
        Dim StrSearch As String = String.Empty
        Try
            RemoveHandler lvSummary.SelectedIndexChanged, AddressOf lvSummary_SelectedIndexChanged
            'Sohail (15 Jan 2018) -- Start
            'Enhancement: Payroll Budget AT Log in 70.1.
            Cursor.Current = Cursors.WaitCursor
            'Sohail (15 Jan 2018) -- End

            dsList = objCommonATLog.GetATModules_List(dtpFromDate.Value.Date, dtpTodate.Value.Date)


            'Pinkal (18-Sep-2012) -- Start
            'Enhancement : TRA Changes
            If dsList IsNot Nothing Then
                Dim drRow() As DataRow = dsList.Tables(0).Select("form_name = 'frmCommonImportData' OR form_name = 'frmCommonExportData'")
                If drRow.Length > 0 Then
                    For i As Integer = 0 To drRow.Length - 1
                        dsList.Tables(0).Rows.Remove(drRow(i))
                    Next
                    dsList.Tables(0).AcceptChanges()
                End If
            End If
            'Pinkal (18-Sep-2012) -- End

            Dim strColumns As String = String.Empty
            Dim iCntColumns As Integer = 0

            lvSummary.Items.Clear()
            lvSummary.Columns.Clear()


            'Pinkal (03-Apr-2013) -- Start
            'Enhancement : TRA Changes
            dgvAuditData.DataSource = Nothing
            lvData.Items.Clear()
            lvData.Columns.Clear()
            'Pinkal (03-Apr-2013) -- End
            'Hemant (21 Jul 2023) -- Start
            'Bug : A1X-1119 - Error when Exporting Audit Trail (Application event logs)
            If CType(dgvAuditData.DataSource, DataView) Is Nothing Then
                btnExport.Visible = False
            Else
                btnExport.Visible = True
            End If
            'Hemant (21 Jul 2023) -- End

            StrSearch = Set_Master_Filter()
            dTable = New DataView(dsList.Tables(0), StrSearch, "[Audit Date]", DataViewRowState.CurrentRows).ToTable

            'S.SANDEEP [ 13 AUG 2012 ] -- START
            'ENHANCEMENT : TRA CHANGES
            Dim iColIdx As Integer = dTable.Columns.IndexOf("form_name")
            'S.SANDEEP [ 13 AUG 2012 ] -- END

            For i As Integer = 0 To dTable.Columns.Count - 1
                If i = 0 Then
                    lvSummary.Columns.Add(dTable.Columns(i).ColumnName, 100, HorizontalAlignment.Left)
                ElseIf i = 1 Then
                    lvSummary.Columns.Add(dTable.Columns(i).ColumnName, 350, HorizontalAlignment.Left)
                    'S.SANDEEP [ 13 AUG 2012 ] -- START
                    'ENHANCEMENT : TRA CHANGES
                    'ElseIf i <= 4 Then
                    '    lvSummary.Columns.Add(dTable.Columns(i).ColumnName, 120, HorizontalAlignment.Left)
                    'ElseIf i > 4 Then
                    '    lvSummary.Columns.Add(dTable.Columns(i).ColumnName, 0, HorizontalAlignment.Left)
                ElseIf i <= iColIdx - 1 Then
                    lvSummary.Columns.Add(dTable.Columns(i).ColumnName, 120, HorizontalAlignment.Left)
                ElseIf i > iColIdx - 1 Then
                    lvSummary.Columns.Add(dTable.Columns(i).ColumnName, 0, HorizontalAlignment.Left)
                    'S.SANDEEP [ 13 AUG 2012 ] -- END
                End If
                strColumns &= "," & dTable.Columns(i).ColumnName
                iCntColumns += 1
            Next

            strColumns = Mid(strColumns, 2)
            strColumnName = strColumns.Split(CChar(","))

            Dim lvItems(dTable.Rows.Count - 1) As ListViewItem
            Dim idx As Integer = 0

            For Each dRow As DataRow In dTable.Rows
                Dim lstItem As New ListViewItem()
                For index As Integer = 0 To iCntColumns - 1
                    Select Case index
                        Case 0
                            lstItem.Text = dRow.Item(strColumnName(index)).ToString
                        Case Else
                            lstItem.SubItems.Add(dRow.Item(strColumnName(index)).ToString)
                    End Select
                Next index

                lstItem.Tag = dRow.Item("form_name")

                'If dRow.Item("module_name5").ToString.Trim.Length > 0 Then
                '    lstItem.Tag = dRow.Item("form_name") & "|" & dRow.Item("module_name5") & "|" & "module_name5"
                'ElseIf dRow.Item("module_name4").ToString.Trim.Length > 0 Then
                '    lstItem.Tag = dRow.Item("form_name") & "|" & dRow.Item("module_name4") & "|" & "module_name4"
                'ElseIf dRow.Item("module_name3").ToString.Trim.Length > 0 Then
                '    lstItem.Tag = dRow.Item("form_name") & "|" & dRow.Item("module_name3") & "|" & "module_name3"
                'ElseIf dRow.Item("module_name2").ToString.Trim.Length > 0 Then
                '    lstItem.Tag = dRow.Item("form_name") & "|" & dRow.Item("module_name2") & "|" & "module_name2"
                'ElseIf dRow.Item("module_name1").ToString.Trim.Length > 0 Then
                '    lstItem.Tag = dRow.Item("form_name") & "|" & dRow.Item("module_name1") & "|" & "module_name1"
                'End If

                lvItems(idx) = lstItem
                idx += 1
            Next

            lvSummary.BeginUpdate()
            lvSummary.Items.AddRange(lvItems)
            lvSummary.EndUpdate()

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "btnSearch_Click", mstrModuleName)
        Finally
            AddHandler lvSummary.SelectedIndexChanged, AddressOf lvSummary_SelectedIndexChanged
            'Sohail (15 Jan 2018) -- Start
            'Enhancement: Payroll Budget AT Log in 70.1.
            Cursor.Current = Cursors.Default
            'Sohail (15 Jan 2018) -- End
        End Try
    End Sub

    Private Sub btnReset_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnReset.Click
        Try

            'Pinkal (03-Apr-2013) -- Start
            'Enhancement : TRA Changes
            cboViewBy.SelectedIndex = 0
            'Pinkal (03-Apr-2013) -- End

            cboEventType.SelectedValue = 0
            cboFilter.SelectedValue = 0
            txtIPAddress.Text = ""
            txtMachine.Text = ""
            txtModuleName.Text = ""
            dtpFromDate.Checked = False
            dtpTodate.Checked = False
            lvSummary.Items.Clear()
            lvSummary.Columns.Clear()
            lvSummary.GridLines = False
            tabcData.TabPages.Remove(tabpChildDetail) : cboChildData.Visible = False

            'Varsha (30 Nov 2017) -- Start
            'Enhancement:[70.1] Employee Budget TimeSheet AT Log
            dgvAuditData.DataSource = Nothing
            lvData.Items.Clear()
            lvData.Columns.Clear()
            'Varsha (30 Nov 2017) -- End
            'Hemant (21 Jul 2023) -- Start
            'Bug : A1X-1119 - Error when Exporting Audit Trail (Application event logs)
            If CType(dgvAuditData.DataSource, DataView) Is Nothing Then
                btnExport.Visible = False
            Else
                btnExport.Visible = True
            End If
            'Hemant (21 Jul 2023) -- End
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "btnReset_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub btnClose_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnClose.Click
        Try
            Me.Close()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "btnClose_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub btnExport_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnExport.Click
        Try
            If Store_User_Log(False) = False Then Exit Sub
            'Gajanan [18-Dec-2019] -- Start   
            'Enhancement:Worked AT Audit Event logs Testing- Biodata Module Document
            
			'Dim dtReader As DataTable = CType(dgvAuditData.DataSource, DataTable)
			Dim dtReader As DataTable
            If dgvAuditData.DataSource.GetType().FullName = "System.Data.DataView" Then
                dtReader = CType(dgvAuditData.DataSource, DataView).ToTable()
            Else
                dtReader = CType(dgvAuditData.DataSource, DataTable)
            End If
            'Gajanan [18-Dec-2019] -- End


            If dtReader IsNot Nothing Then
                Dim strBuilder As New System.Text.StringBuilder
                strBuilder.Append(" <HTML> " & vbCrLf)
                strBuilder.Append(" <BODY><FONT FACE = TAHOMA FONT SIZE=2> " & vbCrLf)
                strBuilder.Append(" <TABLE BORDER = 1 BORDERCOLOR = BLACK WIDTH='50%'> " & vbCrLf)

                strBuilder.Append(" <TR ALIGN = LEFT VALIGN=TOP WIDTH='50%'> " & vbCrLf)
                strBuilder.Append(" <TD BORDER=1 BORDERCOLOR=BLACK ALIGN='CENTER' COLSPAN='2' BGCOLOR='LightBlue'><FONT SIZE=2><B>" & Language.getMessage(mstrModuleName, 500, "Application Log Events") & "</B></FONT></TD>" & vbCrLf)
                strBuilder.Append(" </TR> " & vbCrLf)


                strBuilder.Append(" <TR ALIGN = LEFT VALIGN=TOP WIDTH='50%'> " & vbCrLf)
                strBuilder.Append(" <TD BORDER=1 BORDERCOLOR=BLACK ALIGN='LEFT' WIDTH = '20%'><FONT SIZE=2><B>" & Language.getMessage(mstrModuleName, 503, "User : ") & "</B></FONT></TD> " & vbCrLf)
                strBuilder.Append(" <TD BORDER=1 BORDERCOLOR=BLACK ALIGN='LEFT' WIDTH = '20%'><FONT SIZE=2><B>" & CStr(IIf(CInt(cboFilter.SelectedValue) > 0, cboFilter.Text, "")) & "</B></FONT></TD> " & vbCrLf)
                strBuilder.Append(" </TR> " & vbCrLf)

                strBuilder.Append(" <TR ALIGN = LEFT VALIGN=TOP WIDTH='50%'> " & vbCrLf)
                strBuilder.Append(" <TD BORDER=1 BORDERCOLOR=BLACK ALIGN='LEFT' WIDTH = '20%'><FONT SIZE=2><B>" & Language.getMessage(mstrModuleName, 501, "From Date : ") & "</B></FONT></TD> " & vbCrLf)
                If dtpFromDate.Checked = True Then
                    strBuilder.Append(" <TD BORDER=1 BORDERCOLOR=BLACK ALIGN='LEFT' WIDTH = '20%'><FONT SIZE=2><B>" & dtpFromDate.Value.Date & "</B></FONT></TD> " & vbCrLf)
                Else
                    strBuilder.Append(" <TD BORDER=1 BORDERCOLOR=BLACK ALIGN='LEFT' WIDTH = '20%'><FONT SIZE=2><B>" & "&nbsp;" & "</B></FONT></TD> " & vbCrLf)
                End If
                strBuilder.Append(" </TR> " & vbCrLf)

                strBuilder.Append(" <TR ALIGN = LEFT VALIGN=TOP WIDTH='50%'> " & vbCrLf)
                strBuilder.Append(" <TD BORDER=1 BORDERCOLOR=BLACK ALIGN='LEFT' WIDTH = '20%'><FONT SIZE=2><B>" & Language.getMessage(mstrModuleName, 502, "To Date : ") & "</B></FONT></TD> " & vbCrLf)
                If dtpTodate.Checked = True Then
                    strBuilder.Append(" <TD BORDER=1 BORDERCOLOR=BLACK ALIGN='LEFT' WIDTH = '20%'><FONT SIZE=2><B>" & dtpTodate.Value.Date & "</B></FONT></TD> " & vbCrLf)
                Else
                    strBuilder.Append(" <TD BORDER=1 BORDERCOLOR=BLACK ALIGN='LEFT' WIDTH = '20%'><FONT SIZE=2><B>" & "&nbsp;" & "</B></FONT></TD> " & vbCrLf)
                End If
                strBuilder.Append(" </TR> " & vbCrLf)

                strBuilder.Append(" <TR ALIGN = LEFT VALIGN=TOP WIDTH='50%'> " & vbCrLf)
                strBuilder.Append(" <TD BORDER=1 BORDERCOLOR=BLACK ALIGN='LEFT' WIDTH = '20%'><FONT SIZE=2><B>" & Language.getMessage(mstrModuleName, 507, "IP Address :") & "</B></FONT></TD> " & vbCrLf)
                strBuilder.Append(" <TD BORDER=1 BORDERCOLOR=BLACK ALIGN='LEFT' WIDTH = '20%'><FONT SIZE=2><B>" & txtIPAddress.Text & "</B></FONT></TD> " & vbCrLf)
                strBuilder.Append(" </TR> " & vbCrLf)

                strBuilder.Append(" <TR ALIGN = LEFT VALIGN=TOP WIDTH='50%'> " & vbCrLf)
                strBuilder.Append(" <TD BORDER=1 BORDERCOLOR=BLACK ALIGN='LEFT' WIDTH = '20%'><FONT SIZE=2><B>" & Language.getMessage(mstrModuleName, 508, "Module Name :") & "</B></FONT></TD> " & vbCrLf)
                strBuilder.Append(" <TD BORDER=1 BORDERCOLOR=BLACK ALIGN='LEFT' WIDTH = '20%'><FONT SIZE=2><B>" & txtModuleName.Text & "</B></FONT></TD> " & vbCrLf)
                strBuilder.Append(" </TR> " & vbCrLf)

                strBuilder.Append(" <TR ALIGN = LEFT VALIGN=TOP WIDTH='50%'> " & vbCrLf)
                strBuilder.Append(" <TD BORDER=1 BORDERCOLOR=BLACK ALIGN='LEFT' WIDTH = '20%'><FONT SIZE=2><B>" & Language.getMessage(mstrModuleName, 509, "Machine Name :") & "</B></FONT></TD> " & vbCrLf)
                strBuilder.Append(" <TD BORDER=1 BORDERCOLOR=BLACK ALIGN='LEFT' WIDTH = '20%'><FONT SIZE=2><B>" & txtMachine.Text & "</B></FONT></TD> " & vbCrLf)
                strBuilder.Append(" </TR> " & vbCrLf)

                
                strBuilder.Append(" </TABLE> " & vbCrLf)

                strBuilder.Append(" <BR/> " & vbCrLf)

                'Pinkal (23 Nov 2017) -- Start
                'Bug - previously displayed id field which is not shown to user
                Dim xColumnIndex As Integer = dTable.Columns.IndexOf("form_name")
                'Pinkal (23 Nov 2017) -- End



                strBuilder.Append(" <TABLE BORDER = 1 BORDERCOLOR = BLACK WIDTH='100%'> " & vbCrLf)
                strBuilder.Append(" <TR ALIGN = LEFT VALIGN=TOP WIDTH='100%'> " & vbCrLf)
                'Pinkal (23 Nov 2017) -- Start
                'Bug - previously displayed id field which is not shown to user
                'strBuilder.Append(" <TD BORDER=1 BORDERCOLOR=BLACK ALIGN='LEFT' COLSPAN='" & dTable.Columns.Count - 8 & "'  BGCOLOR='LightBlue'><FONT SIZE=2><B>" & Language.getMessage(mstrModuleName, 506, "Summary List") & "</B></FONT></TD>" & vbCrLf)
                strBuilder.Append(" <TD BORDER=1 BORDERCOLOR=BLACK ALIGN='LEFT' COLSPAN='" & xColumnIndex & "'  BGCOLOR='LightBlue'><FONT SIZE=2><B>" & Language.getMessage(mstrModuleName, 506, "Summary List") & "</B></FONT></TD>" & vbCrLf)
                'Pinkal (23 Nov 2017) -- End
                strBuilder.Append(" </TR> " & vbCrLf)
                strBuilder.Append(" <TR ALIGN = LEFT VALIGN=TOP WIDTH='100%'> " & vbCrLf)


                'Pinkal (23 Nov 2017) -- Start
                'Bug - previously displayed id field which is not shown to user
                'For i As Integer = 0 To dTable.Columns.Count - 9
                For i As Integer = 0 To xColumnIndex - 1
                    'Pinkal (23 Nov 2017) -- End
                    strBuilder.Append(" <TD BORDER=1 BORDERCOLOR=BLACK ALIGN='LEFT'><FONT SIZE=2><B>" & dTable.Columns(i).Caption.ToString & "</B></FONT></TD> " & vbCrLf)
                Next
                strBuilder.Append(" </TR> " & vbCrLf)

                Dim idx As Integer = -1
                If lvSummary.SelectedItems.Count > 0 Then
                    idx = lvSummary.SelectedItems(0).Index
                End If

                strBuilder.Append(" <TR ALIGN = LEFT VALIGN=TOP WIDTH='100%'> " & vbCrLf)
                If idx >= 0 Then
                    'Pinkal (23 Nov 2017) -- Start
                    'Bug - previously displayed id field which is not shown to user
                    'For i As Integer = 0 To dTable.Columns.Count - 9
                    For i As Integer = 0 To xColumnIndex - 1
                        'Pinkal (23 Nov 2017) -- End
                        strBuilder.Append(" <TD BORDER=1 BORDERCOLOR=BLACK ALIGN='LEFT'><FONT SIZE=2>" & dTable.Rows(idx)(i).ToString & "</FONT></TD> " & vbCrLf)
                    Next
                End If
                strBuilder.Append(" </TR> " & vbCrLf)
                strBuilder.Append(" </TABLE> " & vbCrLf)

                strBuilder.Append(" <BR/> " & vbCrLf)

                strBuilder.Append(" <TABLE BORDER = 1 BORDERCOLOR = BLACK WIDTH='100%'> " & vbCrLf)
                strBuilder.Append(" <TR ALIGN = LEFT VALIGN=TOP WIDTH='100%'> " & vbCrLf)
                strBuilder.Append(" <TD BORDER=1 BORDERCOLOR=BLACK ALIGN='LEFT' COLSPAN='" & dtReader.Columns.Count & "'  BGCOLOR='LightBlue'><FONT SIZE=2><B>" & Language.getMessage(mstrModuleName, 505, "Master Detail") & "</B></FONT></TD>" & vbCrLf)
                strBuilder.Append(" </TR> " & vbCrLf)

                strBuilder.Append(" <TR ALIGN = LEFT VALIGN=TOP WIDTH='100%'> " & vbCrLf)
                strBuilder.Append(" <TD BORDER=1 BORDERCOLOR=BLACK ALIGN='LEFT' COLSPAN='" & dtReader.Columns.Count & "'><FONT SIZE=2><B>" & Language.getMessage(mstrModuleName, 504, "Event Type : ") & CStr(IIf(CInt(cboEventType.SelectedValue) > 0, cboEventType.Text, "")) & "</B></FONT></TD> " & vbCrLf)
                strBuilder.Append(" </TR> " & vbCrLf)


                strBuilder.Append(" <TR ALIGN = LEFT VALIGN=TOP WIDTH='100%'> " & vbCrLf)
                For i As Integer = 0 To dtReader.Columns.Count - 1
                    strBuilder.Append(" <TD BORDER=1 BORDERCOLOR=BLACK ALIGN='LEFT'><FONT SIZE=2><B>" & dtReader.Columns(i).Caption.ToString & "</B></FONT></TD> " & vbCrLf)
                Next
                strBuilder.Append(" </TR> " & vbCrLf)


                strBuilder.Append(" <TR ALIGN = LEFT VALIGN=TOP WIDTH='100%'> " & vbCrLf)
                For j As Integer = 0 To dtReader.Rows.Count - 1
                    For i As Integer = 0 To dtReader.Columns.Count - 1
                        strBuilder.Append(" <TD BORDER=1 BORDERCOLOR=BLACK ALIGN='LEFT'><FONT SIZE=2>" & dtReader.Rows(j)(i).ToString & "</FONT></TD> " & vbCrLf)
                    Next
                    strBuilder.Append(" </TR> " & vbCrLf)
                Next

                strBuilder.Append(" </TABLE> " & vbCrLf)

                If cboChildData.Visible = True Then
                    strBuilder.Append(" <BR/> " & vbCrLf)
                    strBuilder.Append(" <TABLE BORDER = 1 BORDERCOLOR = BLACK WIDTH='100%'> " & vbCrLf)
                    strBuilder.Append(" <TR ALIGN = LEFT VALIGN=TOP WIDTH='100%'> " & vbCrLf)
                    strBuilder.Append(" <TD BORDER = 1 BORDERCOLOR=BLACK ALIGN='LEFT' COLSPAN='" & dtChild.Columns.Count & "' BGCOLOR='LightBlue'><FONT SIZE=2><B>" & cboChildData.Text & "</B></FONT></TD>" & vbCrLf)
                    strBuilder.Append(" </TR> " & vbCrLf)

                    strBuilder.Append(" <TR ALIGN = LEFT VALIGN=TOP WIDTH='100%'> " & vbCrLf)
                    For i As Integer = 0 To dtChild.Columns.Count - 1
                        strBuilder.Append(" <TD BORDER=1 BORDERCOLOR=BLACK ALIGN='LEFT'><FONT SIZE=2><B>" & dtChild.Columns(i).Caption & "</B></FONT></TD> " & vbCrLf)
                    Next
                    strBuilder.Append(" </TR> " & vbCrLf)


                    strBuilder.Append(" <TR ALIGN = LEFT VALIGN=TOP WIDTH='100%'> " & vbCrLf)
                    For j As Integer = 0 To dtChild.Rows.Count - 1
                        For i As Integer = 0 To dtChild.Columns.Count - 1
                            strBuilder.Append(" <TD BORDER=1 BORDERCOLOR=BLACK ALIGN='LEFT'><FONT SIZE=2>" & dtChild.Rows(j)(i).ToString & "</FONT></TD> " & vbCrLf)
                        Next
                        strBuilder.Append(" </TR> " & vbCrLf)
                    Next
                    strBuilder.Append(" </TABLE> " & vbCrLf)
                End If


                strBuilder.Append(" </BODY> " & vbCrLf)
                strBuilder.Append(" </HTML> " & vbCrLf)

                Dim svDialog As New SaveFileDialog
                'Hemant (21 Jul 2023) -- Start
                'Bug : A1X-1119 - Error when Exporting Audit Trail (Application event logs)
                'svDialog.Filter = "Excel files (*.xlsx)|*.xlsx"
                svDialog.Filter = "Excel files (*.xls)|*.xls"
                'Hemant (21 Jul 2023) -- End
                If svDialog.ShowDialog = Windows.Forms.DialogResult.OK Then
                    Dim fsFile As New IO.FileStream(svDialog.FileName, IO.FileMode.Create, IO.FileAccess.Write)
                    Dim strWriter As New IO.StreamWriter(fsFile)
                    With strWriter
                        .BaseStream.Seek(0, IO.SeekOrigin.End)
                        .WriteLine(strBuilder)
                        .Close()
                    End With
                    Diagnostics.Process.Start(svDialog.FileName)
                End If
            End If




        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "btnExport_Click", mstrModuleName)
        End Try
    End Sub
    Private Sub objbtnSearch_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnSearch.Click
        Dim frm As New frmCommonSearch
        Try
            With frm
                .ValueMember = cboFilter.ValueMember
                .DisplayMember = cboFilter.DisplayMember
                Select Case gApplicationType
                    Case enArutiApplicatinType.Aruti_Payroll
                        'S.SANDEEP [10 AUG 2015] -- START
                        'ENHANCEMENT : Aruti SaaS Changes
                        '.CodeMember = "employeecode"
                        If cboViewBy.SelectedIndex = 2 Then
                        .CodeMember = "employeecode"
                        End If
                        'S.SANDEEP [10 AUG 2015] -- END
                End Select
                .DataSource = CType(cboFilter.DataSource, DataTable)
            End With
            If frm.DisplayDialog Then
                cboFilter.SelectedValue = frm.SelectedValue
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objbtnSearch_Click", mstrModuleName)
        Finally
            If frm IsNot Nothing Then frm.Dispose()
        End Try
    End Sub

#End Region

#Region " Controls "

    Private Sub gbList_HeaderClick(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles gbList.HeaderClick
        Try
            If (gbList.Size.Height + gbDetails.Size.Height) > mSize.Height Then
                gbList.Size = New Point(994, gbList.Size.Height)
                gbDetails.Size = New Point(994, gbDetails.Size.Height)
            Else
                gbList.Size = New Point(1012, gbList.Size.Height)
                gbDetails.Size = New Point(1012, gbDetails.Size.Height)
            End If
            flpPanel1.Refresh() : flpPanel1.Invalidate(True)
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "gbList_HeaderClick", mstrModuleName)
        End Try
    End Sub

    Private Sub gbDetails_HeaderClick(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles gbDetails.HeaderClick
        Try
            If (gbList.Size.Height + gbDetails.Size.Height) > mSize.Height Then
                gbDetails.Size = New Point(994, gbDetails.Size.Height)
                gbList.Size = New Point(994, gbList.Size.Height)
            Else
                gbDetails.Size = New Point(1012, gbDetails.Size.Height)
                gbList.Size = New Point(1012, gbList.Size.Height)
            End If
            flpPanel1.Refresh() : flpPanel1.Invalidate(True)
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "gbDetails_HeaderClick", mstrModuleName)
        End Try
    End Sub

    'Sohail (06 Apr 2013) -- Start
    'TRA - ENHANCEMENT
    Private Sub lvSummary_ColumnClick(ByVal sender As Object, ByVal e As System.Windows.Forms.ColumnClickEventArgs) Handles lvSummary.ColumnClick
        Try
            Cursor.Current = Cursors.WaitCursor
            If lvSummary.Columns(e.Column).Text.Contains("Date") Then
                lvSummary.SortingType(eZee.Common.ListViewColumnSorter.ComparerType.DateComparer)
            ElseIf lvSummary.Columns(e.Column).Text.Contains("Amount") Then
                lvSummary.SortingType(eZee.Common.ListViewColumnSorter.ComparerType.NumericComparer)
            Else
                lvSummary.SortingType(eZee.Common.ListViewColumnSorter.ComparerType.StringComparer)
            End If
            Cursor.Current = Cursors.Default
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "", mstrModuleName)
        End Try
    End Sub
    'Sohail (06 Apr 2013) -- End

    Private Sub lvSummary_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles lvSummary.SelectedIndexChanged
        Try
            If lvSummary.SelectedItems.Count <= 0 Then Exit Sub
            Cursor.Current = Cursors.WaitCursor
            dgvAuditData.DataSource = Nothing
            StrName = lvSummary.SelectedItems(0).Tag.ToString.Split("|")(0)
            'StrParentNode = lvSummary.SelectedItems(0).Tag.ToString.Split("|")(1)

            If StrName.Trim.Length > 0 Then
                Dim dList As New DataSet

                Select Case gApplicationType
                    Case enArutiApplicatinType.Aruti_Payroll

                If StrName.Trim.Length > 0 AndAlso StrName.StartsWith("frm") Then
                    If StrName = "frmEmployee_Exemption_AddEdit" AndAlso StrParentNode = "mnuEmployeeData" Then
                        StrName = StrName & "_1"
                    ElseIf StrName = "frmSalaryGlobalAssign" AndAlso StrParentNode = "mnuAppraisal" Then
                        StrName = StrName & "_1"
                    End If
                    If StrName = "frmSalaryGlobalAssign" AndAlso StrParentNode = "mnuJobTraining" Then
                        StrName = StrName & "_1"
                    End If
                    If StrName = "frmEmployeeMaster" AndAlso StrParentNode = "mnuJobTraining" Then
                        StrName = StrName & "_1"
                    End If
                            'Sohail (05 Apr 2013) -- Start
                            'TRA - ENHANCEMENT
                            If StrName = "frmBatchPostingList" AndAlso StrParentNode = "mnuPayroll" Then
                                StrName = StrName & "_1"
                            End If
                            'Sohail (05 Apr 2013) -- End

                            'Sohail (06 Apr 2013) -- Start
                            'TRA - ENHANCEMENT
                            If StrName = "frmApproveDisapprovePayment" AndAlso StrParentNode = "mnuPayroll" Then
                                StrName = StrName & "_1"
                            End If
                            'Sohail (06 Apr 2013) -- End


                            ''Nilay (01-Mar-2016) -- Start
                            ''ENHANCEMENT - Implementing External Approval changes 
                            'If lvSummary.SelectedItems(0).SubItems(14).Text.ToUpper = "ATLNLOANAPPROVAL_PROCESS_TRAN" AndAlso _
                            '    StrName.ToUpper = "FRMLOANAPPROVAL" Then
                            '    StrName = StrName & "_1"
                            'End If
                            ''Nilay (01-Mar-2016) -- End


                End If
                End Select

                dList = objCommonATLog.Get_Child_Details(StrName)
              
                Select Case StrName.ToUpper
                    'Shani(31-MAR-2016) -- Start
                    'Enhancement : PA Audit Trails
                    'Case "FRMALLOCATION_MAPPING", "FRMALLOC_MAPPING_LIST"
                    Case "FRMALLOCATION_MAPPING", "FRMALLOC_MAPPING_LIST", "FRMCOMPUTATIONFORMULALIST", "FRMCOMPUTATIONFORMULA_ADDEDIT"
                    'Shani(31-MAR-2016) -- End
                        dList.Tables(0).Rows.Clear()
                End Select
                If dList.Tables(0).Rows.Count > 0 Then
                    With cboChildData
                        .ValueMember = "Child"
                        .DisplayMember = "TableName"
                        .DataSource = dList.Tables(0)
                        .SelectedValue = dList.Tables(0).Rows(0)("Child")
                    End With
                    If tabcData.TabPages.Contains(tabpChildDetail) = False Then
                        tabcData.TabPages.Add(tabpChildDetail)
                    End If
                    cboChildData.Visible = True
                Else
                    tabcData.TabPages.Remove(tabpChildDetail) : cboChildData.Visible = False
                End If
            End If

            'S.SANDEEP [ 13 DEC 2013 ] -- START
            'dtDataSource = objCommonATLog.View_Applicantion_Log(StrName)
            'S.SANDEEP [ 07 JAN 2014 ] -- START
            'dtDataSource = objCommonATLog.View_Applicantion_Log(StrName, CDate(lvSummary.SelectedItems(0).SubItems(4).Text.Trim))

            'S.SANDEEP [04 JUN 2015] -- START
            'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
            'dtDataSource = objCommonATLog.View_Applicantion_Log(StrName, eZeeDate.convertDate(lvSummary.SelectedItems(0).SubItems(7).Text.Trim))

            'Nilay (01-Mar-2016) -- Start
            'ENHANCEMENT - Implementing External Approval changes 
            'dtDataSource = objCommonATLog.View_Applicantion_Log(StrName, eZeeDate.convertDate(lvSummary.SelectedItems(0).SubItems(7).Text.Trim), _
            '                                                   FinancialYear._Object._DatabaseName, _
            '                                                   User._Object._Userunkid, _
            '                                                   FinancialYear._Object._YearUnkid, _
            '                                                   Company._Object._Companyunkid, _
            '                                                   eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), _
            '                                                   eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), _
            '                                                   ConfigParameter._Object._UserAccessModeSetting, _
            '                                                   True, ConfigParameter._Object._IsIncludeInactiveEmp)

            dtDataSource = objCommonATLog.View_Applicantion_Log(StrName, eZeeDate.convertDate(lvSummary.SelectedItems(0).SubItems(7).Text.Trim), _
                                                                FinancialYear._Object._DatabaseName, _
                                                                User._Object._Userunkid, _
                                                                FinancialYear._Object._YearUnkid, _
                                                                Company._Object._Companyunkid, _
                                                                eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), _
                                                                eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), _
                                                                ConfigParameter._Object._UserAccessModeSetting, _
                                                               True, ConfigParameter._Object._IsIncludeInactiveEmp, _
                                                               lvSummary.SelectedItems(0).SubItems(9).Text.ToUpper)
            'Nilay (01-Mar-2016) -- End

           
            'S.SANDEEP [04 JUN 2015] -- END

            'S.SANDEEP [ 07 JAN 2014 ] -- END
            'S.SANDEEP [ 13 DEC 2013 ] -- END

            If dtDataSource IsNot Nothing Then

                'Sohail (17 Apr 2019) -- Start
                'Enhancement - 76.1 - AT Log for Payroll Mobule.
                RemoveHandler txtSearchGrid.TextChanged, AddressOf txtSearchGrid_TextChanged
                txtSearchGrid.Text = ""
                AddHandler txtSearchGrid.TextChanged, AddressOf txtSearchGrid_TextChanged

                RemoveHandler txtSearchList.TextChanged, AddressOf txtSearchList_TextChanged
                txtSearchList.Text = ""
                AddHandler txtSearchList.TextChanged, AddressOf txtSearchList_TextChanged
                'Sohail (17 Apr 2019) -- End

            Dim StrFilter As String = String.Empty

            StrFilter = Set_Master_Filter(True)

            dtDataSource = New DataView(dtDataSource, StrFilter, "", DataViewRowState.CurrentRows).ToTable

            dtDataSource.Columns.Remove("Uid") : dtDataSource.Columns.Remove("ETYId")
            dtDataSource.Columns.Remove("ADate")
            'S.SANDEEP [ 13 AUG 2012 ] -- START
            'ENHANCEMENT : TRA CHANGES
            If dtDataSource.Columns.Contains("loginemployeeunkid") Then
            dtDataSource.Columns.Remove("loginemployeeunkid")
            End If
                If dtDataSource.Columns.Contains("loginemplopyeeunkid") Then
                    dtDataSource.Columns.Remove("loginemplopyeeunkid")
                End If
            If dtDataSource.Columns.Contains("ModeId") Then
            dtDataSource.Columns.Remove("ModeId")
            End If
            'S.SANDEEP [ 13 AUG 2012 ] -- END

                dvAuditView = dtDataSource.DefaultView

                dgvAuditData.DataSource = dvAuditView

            If cboChildData.Visible = True Then
                Call Fill_Child_Report()
            End If

            End If

            'Hemant (21 Jul 2023) -- Start
            'Bug : A1X-1119 - Error when Exporting Audit Trail (Application event logs)
            If CType(dgvAuditData.DataSource, DataView) Is Nothing Then
                btnExport.Visible = False
            Else
                btnExport.Visible = True
            End If
            'Hemant (21 Jul 2023) -- End

            Call Store_User_Log(True)

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "lvSummary_SelectedIndexChanged", mstrModuleName)
        Finally
            Cursor.Current = Cursors.Default
        End Try
    End Sub

    Private Sub cboChildData_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cboChildData.SelectedIndexChanged
        Try
            tabpChildDetail.Text = cboChildData.Text
            lvData.Items.Clear()
            lvData.Columns.Clear()
            Call Fill_Child_Report()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "cboChildData_SelectedIndexChanged", mstrModuleName)
        End Try
    End Sub

    Private Sub cboEventType_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cboEventType.SelectedIndexChanged
        Try

            'S.SANDEEP [ 13 DEC 2013 ] -- START
            'dtDataSource = objCommonATLog.View_Applicantion_Log(StrName)

            'S.SANDEEP [ 20 JUN 2014 ] -- START
            'dtDataSource = objCommonATLog.View_Applicantion_Log(StrName, CDate(lvSummary.SelectedItems(0).SubItems(4).Text))

            'S.SANDEEP [04 JUN 2015] -- START
            'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
            'dtDataSource = objCommonATLog.View_Applicantion_Log(StrName, CDate(eZeeDate.convertDate(lvSummary.SelectedItems(0).SubItems(7).Text.Trim)))
            dtDataSource = objCommonATLog.View_Applicantion_Log(StrName, CDate(eZeeDate.convertDate(lvSummary.SelectedItems(0).SubItems(7).Text.Trim)), _
                                                                FinancialYear._Object._DatabaseName, _
                                                                User._Object._Userunkid, _
                                                                FinancialYear._Object._YearUnkid, _
                                                                Company._Object._Companyunkid, _
                                                                eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), _
                                                                eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), _
                                                                ConfigParameter._Object._UserAccessModeSetting, _
                                                                True, ConfigParameter._Object._IsIncludeInactiveEmp)
            'S.SANDEEP [04 JUN 2015] -- END

            'S.SANDEEP [ 20 JUN 2014 ] -- END

            'S.SANDEEP [ 13 DEC 2013 ] -- END



            If dtDataSource Is Nothing Then Exit Sub

            'Sohail (17 Apr 2019) -- Start
            'Enhancement - 76.1 - AT Log for Payroll Mobule.
            RemoveHandler txtSearchGrid.TextChanged, AddressOf txtSearchGrid_TextChanged
            txtSearchGrid.Text = ""
            AddHandler txtSearchGrid.TextChanged, AddressOf txtSearchGrid_TextChanged

            RemoveHandler txtSearchList.TextChanged, AddressOf txtSearchList_TextChanged
            txtSearchList.Text = ""
            AddHandler txtSearchList.TextChanged, AddressOf txtSearchList_TextChanged
            'Sohail (17 Apr 2019) -- End

            Dim StrFilter As String = String.Empty

            StrFilter = Set_Master_Filter(True)

            dtDataSource = New DataView(dtDataSource, StrFilter, "", DataViewRowState.CurrentRows).ToTable

            dtDataSource.Columns.Remove("Uid") : dtDataSource.Columns.Remove("ETYId")
            dtDataSource.Columns.Remove("ADate")
            'S.SANDEEP [ 13 AUG 2012 ] -- START
            'ENHANCEMENT : TRA CHANGES


            'Pinkal (14-Aug-2012) -- Start
            'Enhancement : TRA Changes
            Select Case gApplicationType
                Case enArutiApplicatinType.Aruti_Payroll
                    'SHANI (11 APR 2015)-START
                    'dtDataSource.Columns.Remove("loginemployeeunkid")
                    If dtDataSource.Columns.Contains("loginemployeeunkid") Then
                    dtDataSource.Columns.Remove("loginemployeeunkid")
                    End If
                    'SHANI (11 APR 2015)--END 
            End Select

            dtDataSource.Columns.Remove("ModeId")

            'Pinkal (14-Aug-2012) -- End


            'S.SANDEEP [ 13 AUG 2012 ] -- END

            dgvAuditData.DataSource = dtDataSource

            If cboChildData.Visible = True Then
                Call Fill_Child_Report()
            End If

            'Hemant (21 Jul 2023) -- Start
            'Bug : A1X-1119 - Error when Exporting Audit Trail (Application event logs)
            If CType(dgvAuditData.DataSource, DataTable) Is Nothing Then
                btnExport.Visible = False
            Else
                btnExport.Visible = True
            End If
            'Hemant (21 Jul 2023) -- End

            Call Store_User_Log(True)

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "cboEventType_SelectedIndexChanged", mstrModuleName)
        End Try
    End Sub

    'S.SANDEEP [ 13 AUG 2012 ] -- START
    'ENHANCEMENT : TRA CHANGES
    Private Sub cboViewBy_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cboViewBy.SelectedIndexChanged
        Try
            Dim dsList As New DataSet
            Select Case cboViewBy.SelectedIndex
                Case 0  'NONE
                    objlblCaption.Enabled = False
                    cboFilter.Enabled = False
                    objbtnSearch.Enabled = False
                    cboFilter.SelectedValue = 0
                Case 1, 3  'DESKTOP,MANAGER SELF SERVICE 
                    cboFilter.DataSource = Nothing
                    objlblCaption.Enabled = True
                    cboFilter.Enabled = True
                    objbtnSearch.Enabled = True
                    objlblCaption.Text = Language.getMessage(mstrModuleName, 200, "User")
                    Dim objFUser As New clsUserAddEdit

                    'S.SANDEEP [10 AUG 2015] -- START
                    'ENHANCEMENT : Aruti SaaS Changes
                    'Dim objOption As New clsPassowdOptions
                    'Dim drOption As DataRow() = objOption._DataTable.Select("passwdoptionid = " & enPasswordOption.USER_LOGIN_MODE)
                    'If drOption.Length > 0 Then
                    '    If objOption._UserLogingModeId = enAuthenticationMode.BASIC_AUTHENTICATION Then
                    '        'S.SANDEEP [ 11 DEC 2012 ] -- START
                    '        'ENHANCEMENT : TRA CHANGES
                    '        'dsList = objUserAddEdit.GetList("List", True, False)
                    '        Dim objPswd As New clsPassowdOptions
                    '        If objPswd._IsEmployeeAsUser Then
                    '            dsList = objFUser.getComboList("List", True, False, True)
                    '        Else
                    '            dsList = objFUser.getComboList("List", True, False)
                    '        End If
                    '        objPswd = Nothing
                    '        'S.SANDEEP [ 11 DEC 2012 ] -- END
                    '    ElseIf objOption._UserLogingModeId = enAuthenticationMode.AD_BASIC_AUTHENTICATION Then
                    '        dsList = objFUser.getComboList("List", True, True)
                    '    ElseIf objOption._UserLogingModeId = enAuthenticationMode.AD_SSO_AUTHENTICATION Then
                    '        dsList = objFUser.getComboList("List", True, True)
                    '    End If
                    'Else
                    '    dsList = objFUser.getComboList("List", True, False)
                    'End If

                    'Nilay (01-Mar-2016) -- Start
                    'dsList = objFUser.getNewComboList("List", , True)
                    dsList = objFUser.getNewComboList("List", , True, , , , True)
                    'Nilay (01-Mar-2016) -- End

                    'S.SANDEEP [10 AUG 2015] -- END

                    objFUser = Nothing
                    With cboFilter
                        .ValueMember = "userunkid"
                        .DisplayMember = "name"
                    End With
                    cboFilter.DataSource = dsList.Tables("List")
                    cboFilter.SelectedValue = 0
                Case 2  'EMPLOYEE SELF SERVICE
                    cboFilter.DataSource = Nothing
                    objlblCaption.Enabled = True
                    cboFilter.Enabled = True
                    objbtnSearch.Enabled = True
                    objlblCaption.Text = Language.getMessage(mstrModuleName, 201, "Employee")
                    Dim objEmp As New clsEmployee_Master
                    'Sohail (23 Nov 2012) -- Start
                    'TRA - ENHANCEMENT
                    'If ConfigParameter._Object._IsIncludeInactiveEmp = False Then
                    '    dsList = objEmp.GetEmployeeList("List", True, ConfigParameter._Object._IsIncludeInactiveEmp, , , , , , , , , , , , CDate(eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate).ToShortDateString), CDate(eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate).ToShortDateString))
                    'Else
                    '    dsList = objEmp.GetEmployeeList("List", True, Not ConfigParameter._Object._IsIncludeInactiveEmp)
                    'End If
                    'dsList = objEmp.GetEmployeeList("List", True)

                    dsList = objEmp.GetEmployeeList(FinancialYear._Object._DatabaseName, _
                                                    User._Object._Userunkid, _
                                                    FinancialYear._Object._YearUnkid, _
                                                    Company._Object._Companyunkid, _
                                                    eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), _
                                                    eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), _
                                                    ConfigParameter._Object._UserAccessModeSetting, _
                                                    True, False, "List", True)
                    'Sohail (23 Nov 2012) -- End
                    With cboFilter
                        .ValueMember = "employeeunkid"
                        .DisplayMember = "employeename"
                    End With
                    cboFilter.DataSource = dsList.Tables("List")
                    cboFilter.SelectedValue = 0
            End Select
            dtDataSource = Nothing : lvSummary.Items.Clear()


            'Pinkal (03-Apr-2013) -- Start
            'Enhancement : TRA Changes
            tabcData.TabPages.Remove(tabpChildDetail) : cboChildData.Visible = False
            dgvAuditData.DataSource = dtDataSource
            lvData.Columns.Clear()
            'Pinkal (03-Apr-2013) -- End
            'Hemant (21 Jul 2023) -- Start
            'Bug : A1X-1119 - Error when Exporting Audit Trail (Application event logs)
            If CType(dgvAuditData.DataSource, DataView) Is Nothing Then
                btnExport.Visible = False
            Else
                btnExport.Visible = True
            End If
            'Hemant (21 Jul 2023) -- End

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "cboViewBy_SelectedIndexChanged", mstrModuleName)
        End Try
    End Sub
    'S.SANDEEP [ 13 AUG 2012 ] -- END

    Private Sub txtSearchGrid_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txtSearchGrid.TextChanged
        Dim str As String = ""
        Try
            'Sohail (17 Apr 2019) -- Start
            'Enhancement - 76.1 - AT Log for Payroll Mobule.
            If txtSearchGrid.Text.Length > 0 Then
                Call SetRegularFont(txtSearchGrid)
            End If
            'Sohail (17 Apr 2019) -- End
            If dvAuditView Is Nothing Then Exit Sub
            If (txtSearchGrid.Text.Length > 0) Then
                str = GetFilterString(txtSearchGrid.Text, dvAuditView, dgvAuditData)
            End If
            dvAuditView.RowFilter = str
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "txtSearchGrid_TextChanged", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub txtSearchList_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txtSearchList.TextChanged
        Dim str As String = ""
        Try
            'Sohail (17 Apr 2019) -- Start
            'Enhancement - 76.1 - AT Log for Payroll Mobule.
            If txtSearchList.Text.Length > 0 Then
                Call SetRegularFont(txtSearchList)
            End If
            'Sohail (17 Apr 2019) -- End

            If (txtSearchList.Text.Length > 0) Then
                str = GetFilterString(txtSearchList.Text, dvChildView, dgvChildDetails)
            End If
            dvChildView.RowFilter = str
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "txtSearchList_TextChanged", mstrModuleName)
        Finally
        End Try
    End Sub

    'Sohail (17 Apr 2019) -- Start
    'Enhancement - 76.1 - AT Log for Payroll Mobule.
    Private Sub txtSearchGrid_GotFocus(ByVal sender As Object, ByVal e As System.EventArgs) Handles txtSearchGrid.GotFocus, txtSearchList.GotFocus
        Dim txt As TextBox = CType(sender, TextBox)
        Try
            With txt
                .ForeColor = Color.Black
                .Font = New Font(Me.Font.FontFamily, Me.Font.Size, FontStyle.Regular)

                If .Name = txtSearchGrid.Name AndAlso .Text = mstrSearchGridText Then
                    RemoveHandler txt.TextChanged, AddressOf txtSearchGrid_TextChanged
                    .Text = ""
                    AddHandler txt.TextChanged, AddressOf txtSearchGrid_TextChanged
                ElseIf .Name = txtSearchList.Name AndAlso .Text = mstrSearchListText Then
                    RemoveHandler txt.TextChanged, AddressOf txtSearchList_TextChanged
                    .Text = ""
                    AddHandler txt.TextChanged, AddressOf txtSearchList_TextChanged
                End If
            End With
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, txt.Name & "_GotFocus", mstrModuleName)
        End Try
    End Sub

    Private Sub txtSearchGrid_Leave(ByVal sender As Object, ByVal e As System.EventArgs) Handles txtSearchGrid.Leave, txtSearchList.Leave
        Dim txt As TextBox = CType(sender, TextBox)
        Try
            If txt.Text.Length <= 0 Then
                Call SetDefaultSearchText(txt)
            Else
                Call SetRegularFont(txt)
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, txt.Name & "_Leave", mstrModuleName)
        End Try
    End Sub
    'Sohail (17 Apr 2019) -- End

#End Region

    
	'<Language> This Auto Generated Text Please Do Not Modify it.
#Region " Language & UI Settings "
	Private Sub OtherSettings()
		Try
			Me.SuspendLayout()
			
			Call SetLanguage()
			
			Me.gbGroupList.GradientColor = GUI._eZeeContainerHeaderBackColor 
			Me.gbGroupList.ForeColor = GUI._eZeeContainerHeaderForeColor 

			Me.gbList.GradientColor = GUI._eZeeContainerHeaderBackColor 
			Me.gbList.ForeColor = GUI._eZeeContainerHeaderForeColor 

			Me.gbDetails.GradientColor = GUI._eZeeContainerHeaderBackColor 
			Me.gbDetails.ForeColor = GUI._eZeeContainerHeaderForeColor 


			Me.btnExport.GradientBackColor = GUI._ButttonBackColor 
			Me.btnExport.GradientForeColor = GUI._ButttonFontColor

			Me.btnClose.GradientBackColor = GUI._ButttonBackColor 
			Me.btnClose.GradientForeColor = GUI._ButttonFontColor


			Me.ResumeLayout()
		Catch Ex As Exception
			DisplayError.Show("-1", ex.Message, "OtherSettings", mstrModuleName)
		End Try
	End Sub
			
			
	Private Sub SetLanguage()
		Try
			Me.Text = Language._Object.getCaption(Me.Name, Me.Text)
			
			Me.gbGroupList.Text = Language._Object.getCaption(Me.gbGroupList.Name, Me.gbGroupList.Text)
			Me.btnReset.Text = Language._Object.getCaption(Me.btnReset.Name, Me.btnReset.Text)
			Me.btnSearch.Text = Language._Object.getCaption(Me.btnSearch.Name, Me.btnSearch.Text)
			Me.lblTodate.Text = Language._Object.getCaption(Me.lblTodate.Name, Me.lblTodate.Text)
			Me.lblEventType.Text = Language._Object.getCaption(Me.lblEventType.Name, Me.lblEventType.Text)
			Me.lblFromDate.Text = Language._Object.getCaption(Me.lblFromDate.Name, Me.lblFromDate.Text)
			Me.lblModuleName.Text = Language._Object.getCaption(Me.lblModuleName.Name, Me.lblModuleName.Text)
			Me.lblIPAddress.Text = Language._Object.getCaption(Me.lblIPAddress.Name, Me.lblIPAddress.Text)
			Me.lblMachine.Text = Language._Object.getCaption(Me.lblMachine.Name, Me.lblMachine.Text)
			Me.gbList.Text = Language._Object.getCaption(Me.gbList.Name, Me.gbList.Text)
			Me.gbDetails.Text = Language._Object.getCaption(Me.gbDetails.Name, Me.gbDetails.Text)
			Me.tabpMasterDetail.Text = Language._Object.getCaption(Me.tabpMasterDetail.Name, Me.tabpMasterDetail.Text)
			Me.tabpChildDetail.Text = Language._Object.getCaption(Me.tabpChildDetail.Name, Me.tabpChildDetail.Text)
			Me.btnExport.Text = Language._Object.getCaption(Me.btnExport.Name, Me.btnExport.Text)
			Me.btnClose.Text = Language._Object.getCaption(Me.btnClose.Name, Me.btnClose.Text)
			Me.lblViewBy.Text = Language._Object.getCaption(Me.lblViewBy.Name, Me.lblViewBy.Text)

		Catch Ex As Exception
			DisplayError.Show("-1", ex.Message, "SetLanguage", mstrModuleName)
		End Try
	End Sub
			
			
	Private Sub SetMessages()
		Try
			Language.setMessage(mstrModuleName, 100, "Select")
			Language.setMessage(mstrModuleName, 105, "DeskTop")
			Language.setMessage(mstrModuleName, 106, "Employee Self Service")
			Language.setMessage(mstrModuleName, 107, "Manager Self Service")
			Language.setMessage(mstrModuleName, 200, "User")
			Language.setMessage(mstrModuleName, 201, "Employee")
			Language.setMessage(mstrModuleName, 500, "Application Log Events")
			Language.setMessage(mstrModuleName, 501, "From Date :")
			Language.setMessage(mstrModuleName, 502, "To Date :")
			Language.setMessage(mstrModuleName, 503, "User :")
			Language.setMessage(mstrModuleName, 504, "Event Type :")
			Language.setMessage(mstrModuleName, 505, "Master Detail")
			Language.setMessage(mstrModuleName, 506, "Summary List")
			Language.setMessage(mstrModuleName, 507, "IP Address :")
			Language.setMessage(mstrModuleName, 508, "Module Name :")
			Language.setMessage(mstrModuleName, 509, "Machine Name :")

		Catch Ex As Exception
			DisplayError.Show("-1", ex.Message, "SetMessages", mstrModuleName)
		End Try
	End Sub
#End Region 'Language & UI Settings
	'</Language>
End Class