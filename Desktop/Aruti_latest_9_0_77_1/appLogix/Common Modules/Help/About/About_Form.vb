Option Strict On

Imports eZeeCommonLib

Public NotInheritable Class frmAbout

#Region " Form "

    Private Sub frmAbout_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles Me.KeyPress
        Try
            If Asc(e.KeyChar) = 27 Then
                Me.Close()
            End If
            If Asc(e.KeyChar) = 13 Then
                Windows.Forms.SendKeys.Send("{Tab}")
                e.Handled = True
            End If
        Catch ex As Exception
            Call DisplayError.Show("-1", ex.Message, "frmAbout_KeyPress", "frmAbout")
        End Try
    End Sub

    Private Sub frmAbout_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Call Set_Logo(Me, gApplicationType)
        ' Set the title of the form.
        Dim ApplicationTitle As String
        If My.Application.Info.Title <> "" Then
            'Jitu (21 Jun 2010) --Start
            'ApplicationTitle = My.Application.Info.Title
            ApplicationTitle = My.Application.Info.ProductName
            'Jitu (21 Jun 2010) --End
        Else
            ApplicationTitle = System.IO.Path.GetFileNameWithoutExtension(My.Application.Info.AssemblyName)
        End If
        Me.Text = String.Format("About {0}", ApplicationTitle)
        ' Initialize all of the text displayed on the About Box.
        ' TODO: Customize the application's assembly information in the "Application" pane of the project 
        '    properties dialog (under the "Project" menu).
        Me.LabelProductName.Text = My.Application.Info.ProductName
        Me.LabelVersion.Text = String.Format("Version {0}", My.Application.Info.Version.ToString)
        Me.LabelCopyright.Text = My.Application.Info.Copyright
        Me.LabelCompanyName.Text = My.Application.Info.CompanyName
        Me.TextBoxDescription.Text = "" & _
                    "This program is protected by copyright law and " & _
                    "international treaties. Unauthorized reproduction or " & _
                    "distribution of this program, or any portion of it, may " & _
                    "result in severe civil and criminal penalties, and will be " & _
                    "prosecuted to the maximum extent possible under the law. " & vbCrLf & vbCrLf

        Me.TextBoxDescription.Text &= "This Product is licensed to " & vbCrLf & _
                                      "--------------------------------------" & vbCrLf

        Dim objCompany As New clsCompany_Master
        Dim objMaster As New clsMasterData
        Dim objZipcode As New clszipcode_master
        Dim dsList As New DataSet
        objCompany._Companyunkid = 1
        objZipcode._Zipcodeunkid = objCompany._Postalunkid
        dsList = objMaster.getCountryList("List", False, objCompany._Countryunkid)
        Me.TextBoxDescription.Text &= objCompany._Name & vbCrLf
        If dsList.Tables(0).Rows.Count > 0 Then
            Me.TextBoxDescription.Text &= dsList.Tables(0).Rows(0)("country_name").ToString
        End If
        Me.TextBoxDescription.Text &= " - " & objZipcode._Zipcode_No.ToString

        '    If Not NGLic._Object.IsDemo Then
        '        Dim objHotel As New clsHotel(True)
        '        Me.TextBoxDescription.Text &= objHotel._Hotel_Name & vbCrLf & _
        '                                objHotel._Country & IIf(objHotel._Zipcode <> " ", " - " & objHotel._Zipcode, "").ToString & vbCrLf
        '    Else
        '        If NGLic._Object.IsExpire Then
        '            Dim strString As String = ""

        '            Select Case eZeeApplication._AppType
        '                Case enApplication.eZeePOS, enApplication.eZeePOSConfigartion
        '                    strString = ecore32.core.BackOffice
        '                Case Else
        '                    strString = ecore32.core.FrontDesk
        '            End Select

        '            Me.TextBoxDescription.Text &= "The evaluation period of " & strString & _
        '                    " is Over. To continue using this software you have to register this software." & vbCrLf
        '        Else
        '            Me.TextBoxDescription.Text &= "You have" & " " & NGLic._Object.DaysLeft & " " & "days left after today." & vbCrLf
        '            Me.TextBoxDescription.Text &= "At the end of the evaluation  period  the  software will  stop working.  You  can however continue using the software after the evaluation time is over by purchasing the license." & vbCrLf
        '        End If
        '    End If
    End Sub

#End Region

#Region " Buttons "

    Private Sub btnok_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnok.Click
        Me.Close()
    End Sub

#End Region

	
End Class
