Option Strict On
Imports eZeeCommonLib

Public Class frmDemoNotification
    Private mdsList As New DataSet
    Dim objAppSettings As New clsApplicationSettings
    Private ReadOnly mstrModuleName As String = "frmDemoNotification"

#Region " Form "

    Private Sub frmDemoNotification_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Try
            'If eZeeApplication._AppType = enApplication.eZeeFD Or eZeeApplication._AppType = enApplication.eZeeFDConfig Then
            '    'Issue # OEM
            '    Me.Text = ecore32.core.FrontDesk & " " & Me.Text
            'Else
            '    'Issue # OEM
            '    Me.Text = ecore32.core.Burrp & " " & Me.Text
            'End If

            'If IO.File.Exists(objAppSettings._ApplicationPath & "local.xml") = True Then
            '    mdsList.ReadXml(objAppSettings._ApplicationPath & "local.xml")
            'End If

            'Issue # OEM
            'Me.Icon = acore32.core.
            'Me.Text = acore32.core.FrontDesk & " " & Me.Text

            Call Set_Logo(Me, gApplicationType)

            If ArtLic._Object.IsExpire Then
                btnUnlock.Focus()
            Else
                btnUnlock.Focus()
            End If


            'Sohail (08 May 2013) -- Start
            'License - ENHANCEMENT
            'If ArtLic._Object.IsExpire Then
            '    lblDaysLeft.Visible = False
            '    btnContinue.Enabled = False
            '    ''Jitu (17 Jan 2011) --Start
            '    'If ecore32.core.iCore = ecore32.enCore.EZEE Or ecore32.core.iCore = ecore32.enCore.HOTEL360 Then
            '    '    btnFeedback.Visible = True
            '    '    btnContinue.Visible = False
            '    'End If
            '    ''Jitu (17 Jan 2011) --End
            '    'Issue # OEM
            '    lblDemoDescription.Text = "The evaluation period of Aruti is Over. To continue using this software you have to register this software."
            'Else
            '    If ArtLic._Object.IsDemo Then
            '        lblDaysLeft.Visible = True
            '        btnContinue.Enabled = True
            '        'Jitu (17 Jan 2011) --Start
            '        btnFeedback.Visible = False
            '        btnContinue.Visible = True
            '        'Jitu (17 Jan 2011) --End
            '        lblDaysLeft.Text = "You have" & " " & ArtLic._Object.DaysLeft & " " & "days left after today."
            '        lblDemoDescription.Text = "At the end of the evaluation  period  the  software will  stop working.  You  can however continue using the software after the evaluation time is over by purchasing the license."
            '    Else
            '        lblDaysLeft.Visible = False
            '        btnContinue.Enabled = False
            '        ''Jitu (17 Jan 2011) --Start
            '        'If ecore32.core.iCore = ecore32.enCore.EZEE Or ecore32.core.iCore = ecore32.enCore.HOTEL360 Then
            '        '    btnFeedback.Visible = True
            '        '    btnContinue.Visible = False
            '        'End If
            '        ''Jitu (17 Jan 2011) --End
            '        'Issue # OEM
            '        lblDemoDescription.Text = "The evaluation period of Aruti is Over. To continue using this software you have to register this software."
            '    End If
            'End If

            'Sohail [23 Apr 2016] -- Start
            'Issue : Demo does not get expired if system date is changed on server
            Dim objAppSetting As New clsApplicationSettings
            Dim blnIsExpired As Boolean = True
            If objAppSetting._IsClient = 0 Then 'Server
                blnIsExpired = ArtLic._Object.IsExpire
            Else
                blnIsExpired = ConfigParameter._Object._IsExpire
            End If
            'Sohail [23 Apr 2016] -- End

            'Sohail [23 Apr 2016] -- Start
            'Issue : Demo does not get expired if system date is changed on server
            'If ConfigParameter._Object._IsExpire Then
            If blnIsExpired = True Then
                'Sohail [23 Apr 2016] -- End
                lblDaysLeft.Visible = False
                btnContinue.Enabled = False
                ''Jitu (17 Jan 2011) --Start
                'If ecore32.core.iCore = ecore32.enCore.EZEE Or ecore32.core.iCore = ecore32.enCore.HOTEL360 Then
                '    btnFeedback.Visible = True
                '    btnContinue.Visible = False
                'End If
                ''Jitu (17 Jan 2011) --End
                'Issue # OEM

                Dim objAppSettings As New clsApplicationSettings
                If objAppSettings._IsClient <> 0 Then
                    btnUnlock.Visible = False
                End If
                objAppSettings = Nothing

                lblDemoDescription.Text = "The evaluation period of Aruti is Over. To continue using this software you have to register this software."
            Else

                If ConfigParameter._Object._IsArutiDemo = True Then 'IsDemo
                    lblDaysLeft.Visible = True
                    btnContinue.Enabled = True
                    'Jitu (17 Jan 2011) --Start
                    btnFeedback.Visible = False
                    btnContinue.Visible = True
                    'Jitu (17 Jan 2011) --End

                    Dim objAppSettings As New clsApplicationSettings
                    If objAppSettings._IsClient <> 0 Then
                        btnUnlock.Visible = False
                    End If
                    objAppSettings = Nothing

                    lblDaysLeft.Text = "You have" & " " & ConfigParameter._Object._DaysLeft & " " & "days left after today."
                    lblDemoDescription.Text = "At the end of the evaluation  period  the  software will  stop working.  You  can however continue using the software after the evaluation time is over by purchasing the license."
                Else
                    lblDaysLeft.Visible = False
                    btnContinue.Enabled = False
                    ''Jitu (17 Jan 2011) --Start
                    'If ecore32.core.iCore = ecore32.enCore.EZEE Or ecore32.core.iCore = ecore32.enCore.HOTEL360 Then
                    '    btnFeedback.Visible = True
                    '    btnContinue.Visible = False
                    'End If
                    ''Jitu (17 Jan 2011) --End
                    'Issue # OEM
                    lblDemoDescription.Text = "The evaluation period of Aruti is Over. To continue using this software you have to register this software."
                End If
            End If
            'Sohail (08 May 2013) -- End

            'Dim objOEM As New clsReseller
            'lblOEMInfo.Text = CStr(clsMain.GetLocalMessage(mdsList, mstrModuleName, "6", objAppSettings._LanguageId, "To obtain a commercial license contact")) & " " & objOEM._OEMCompany_Name & " " & CStr(clsMain.GetLocalMessage(mdsList, mstrModuleName, "7", objAppSettings._LanguageId, "today.")) & " " & vbCrLf & _
            '                    CStr(clsMain.GetLocalMessage(mdsList, mstrModuleName, "8", objAppSettings._LanguageId, "Phone:")) & " " & objOEM._OEMSales_phone & " " & vbCrLf & _
            '                    CStr(clsMain.GetLocalMessage(mdsList, mstrModuleName, "9", objAppSettings._LanguageId, "Email:")) & " " & objOEM._OEMSales_email
            ''"Fax : " & objOEM._OEMFax & " " & vbCrLf & _

            'Select Case eZeeApplication._AppType
            '    Case enApplication.eZeePOS, enApplication.eZeePOSConfigartion
            '        lblNoofRooms.Text = CStr(clsMain.GetLocalMessage(mdsList, mstrModuleName, "10", objAppSettings._LanguageId, "Thank you for evaluating")) & " " & ecore32.core.Burrp
            '    Case enApplication.eZeeFD, enApplication.eZeeFDConfig
            '        lblNoofRooms.Text = CStr(clsMain.GetLocalMessage(mdsList, mstrModuleName, "10", objAppSettings._LanguageId, "Thank you for evaluating")) & " " & ecore32.core.FrontDesk
            'End Select

            'btnUnlock.Text = CStr(clsMain.GetLocalMessage(mdsList, mstrModuleName, "13", objAppSettings._LanguageId, "Register"))
            'btnContinue.Text = CStr(clsMain.GetLocalMessage(mdsList, mstrModuleName, "14", objAppSettings._LanguageId, "Continue"))
            'btnCancel.Text = CStr(clsMain.GetLocalMessage(mdsList, mstrModuleName, "15", objAppSettings._LanguageId, "Cancel"))
            'btnFeedback.Text = CStr(clsMain.GetLocalMessage(mdsList, mstrModuleName, "16", objAppSettings._LanguageId, "Feedback"))

            'Panel3.BackgroundImage = ecore32.core.DemoNotification
            'PictureBox1.BackgroundImage = ecore32.core.Logo

        Catch ex As Exception
            Call DisplayError.Show("-1", ex.Message, "frmDemoNotification_Load", mstrModuleName)
        End Try
    End Sub
#End Region

#Region " Button "

    Private Sub btnUnlock_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnUnlock.Click
        Try
            Me.Hide()
            Dim frm As New frmNGLicense
            frm.ShowDialog()
            frm = Nothing
            Me.Close()
        Catch ex As Exception
            Call DisplayError.Show("-1", ex.Message, "btnUnlock_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub btnFeedback_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnFeedback.Click
        Try
            Dim strURL As String = "http://www.ezeetechnosys.com/feedback/index.php"

            If Not strURL.StartsWith("http://") Then
                strURL = "http://" & strURL
            End If

            System.Diagnostics.Process.Start(strURL)

        Catch ex As Exception
            Call DisplayError.Show("-1", ex.Message, "btnFeedback_Click", mstrModuleName)
        End Try

    End Sub
#End Region

End Class