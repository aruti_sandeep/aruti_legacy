#Region " Imports "
Imports eZeeCommonLib
Imports Vintasoft.Twain
#End Region

Public Class frmCommonImageScanner

#Region " Private Variables "
    Private mblnCancel As Boolean = True
    Private ReadOnly mstrModuleName As String = "frmCommonImageScanner"
    Private mimgScanImage As Drawing.Image
#End Region
    
    Public ReadOnly Property _Image() As Drawing.Image
        Get
            Return mimgScanImage
        End Get
    End Property

    Public Function DisplayDialog() As System.Windows.Forms.DialogResult
        Try
            Return Me.ShowDialog()
        Catch ex As Exception
            'Call DisplayError.Show("-1", ex.Message, "displayDialog", mstrModuleName)
        End Try
    End Function

    Private Sub frmCommonImageScanner_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            Call Set_Logo(Me, gApplicationType)
            Call Language.setLanguage(Me.Name)

            VSTwain1.StartDevice()
            If VSTwain1.SelectSource() Then
                VSTwain1.ShowUI = False
                VSTwain1.DisableAfterAcquire = True
                VSTwain1.OpenDataSource()
                VSTwain1.UnitOfMeasure = Vintasoft.Twain.UnitOfMeasure.Inches
                VSTwain1.PixelType = Vintasoft.Twain.PixelType.RGB
                VSTwain1.Acquire()
            End If

        Catch ex As Exception
            'Call DisplayError.Show("-1", ex.Message, "frmCommonImageScanner_Load", mstrModuleName)
            Try
                VSTwain1.CloseDataSource()
            Catch ex1 As Exception

            End Try
        End Try
    End Sub

    Private Sub VSTwain1_ImageAcquired(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles VSTwain1.ImageAcquired
        Try
            If Not (picImage.Image Is Nothing) Then
                picImage.Image.Dispose()
                picImage.Image = Nothing
            End If
            picImage.Image = VSTwain1.GetCurrentImage
        Catch ex As Exception

        End Try
    End Sub

    Private Sub VSTwain1_ScanCompleted(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles VSTwain1.ScanCompleted
        Try
            If VSTwain1.ErrorCode <> Vintasoft.Twain.ErrorCode.None Then
                eZeeMsgBox.Show(VSTwain1.ErrorString)
            End If

            'Anjan (14 Nov 2009)-Start
            VSTwain1.DespeckleImage(VSTwain1.SourceIndex, 8, 25, 30, 400)
            VSTwain1.DeskewImage(VSTwain1.SourceIndex, Vintasoft.Twain.BorderColor.AutoDetect, 5, 5)
            VSTwain1.DetectImageBorder(VSTwain1.SourceIndex, 5, 5, 5)
            picImage.Image = VSTwain1.GetImage(0)
            'Anjan (14 Nov 2009)-End
        Catch ex As Exception

        End Try
    End Sub

    Private Sub btnCancel_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnCancel.Click
        Try
            Me.Close()
        Catch ex As Exception
            Call DisplayError.Show("-1", ex.Message, "btnCancel_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub btnAccept_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnAccept.Click
        mimgScanImage = picImage.Image
    End Sub

	'<Language> This Auto Generated Text Please Do Not Modify it.
#Region " Language & UI Settings "
	Private Sub OtherSettings()
		Try
			Me.SuspendLayout()
			
			Call SetLanguage()
			
			Me.gbMain.GradientColor = GUI._eZeeContainerHeaderBackColor 
			Me.gbMain.ForeColor = GUI._eZeeContainerHeaderForeColor 


			Me.btnAccept.GradientBackColor = GUI._ButttonBackColor 
			Me.btnAccept.GradientForeColor = GUI._ButttonFontColor

			Me.btnCancel.GradientBackColor = GUI._ButttonBackColor 
			Me.btnCancel.GradientForeColor = GUI._ButttonFontColor


			Me.ResumeLayout()
		Catch Ex As Exception
			DisplayError.Show("-1", ex.Message, "OtherSettings", mstrModuleName)
		End Try
	End Sub
			
			
	Private Sub SetLanguage()
		Try
			Me.Text = Language._Object.getCaption(Me.Name, Me.Text)
			
			Me.gbMain.Text = Language._Object.getCaption(Me.gbMain.Name, Me.gbMain.Text)
			Me.btnAccept.Text = Language._Object.getCaption(Me.btnAccept.Name, Me.btnAccept.Text)
			Me.btnCancel.Text = Language._Object.getCaption(Me.btnCancel.Name, Me.btnCancel.Text)


		Catch Ex As Exception
			DisplayError.Show("-1", ex.Message, "SetLanguage", mstrModuleName)
		End Try
	End Sub
			
			
	Private Sub SetMessages()
		Try

		Catch Ex As Exception
			DisplayError.Show("-1", ex.Message, "SetMessages", mstrModuleName)
		End Try
	End Sub
#End Region 'Language & UI Settings
	'</Language>
End Class