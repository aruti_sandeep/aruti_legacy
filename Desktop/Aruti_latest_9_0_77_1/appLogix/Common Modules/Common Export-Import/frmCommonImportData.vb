﻿Option Strict On

Imports eZeeCommonLib
Imports Aruti.Data

'Last Message Index = 6

Public Class frmCommonImportData

#Region " Private Variables "

    Private ReadOnly mstrModuleName As String = "frmCommonImportData"
    Private mblnCancel As Boolean = True
    Private menAction As enAction = enAction.ADD_ONE
    Private mdtOther As DataTable = Nothing
    Dim mdsPayroll As DataSet = Nothing
    Dim dsList As DataSet = Nothing
    Dim dicComboName As New Dictionary(Of Integer, ComboBox)
    Dim objMaster As clsMasterData
    Dim m_Strarray(1, 1) As String
    Private m_ImportValue As New Dictionary(Of String, String)
    Dim num_rows As Long
    Dim num_cols As Long
    Dim dtCaption As New DataTable("Captions")
    Dim objImportExport As clsCommonImportExport
    Dim arColumn() As String


    'Pinkal (12-Oct-2011) -- Start
    'Enhancement : TRA Changes
    Dim mstrComboColName As String = ""
    'Pinkal (12-Oct-2011) -- End


    'Pinkal (03-Jul-2013) -- Start
    'Enhancement : TRA Changes
    Dim mstrShiftName As String = ""
    Dim mdtDays As DataTable = Nothing
    'Pinkal (03-Jul-2013) -- End

    'Shani(25-Sep-2015) -- Start
    'ENHANCEMENT : Add Option to Import Bank Branch Master on Common Importation(Issue No : 455)
    Private strClearingFieldName As String = ""
    'Shani(25-Sep-2015) -- End



#End Region

#Region "Form Event"

    Private Sub frmCommonImportData_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Try
            Call Set_Logo(Me, gApplicationType)
            objMaster = New clsMasterData
            objImportExport = New clsCommonImportExport
            dsList = objMaster.GetMasterName("Masters")
            FillMasters()
            btnOpenFile.Select()
            SetLableCaption()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmCommonImportData_Load", mstrModuleName)
        End Try

    End Sub

#End Region

#Region "Private Function"

    Public Sub FillMasters()
        Try

            Dim lvItem As ListViewItem
            lvMasterList.Items.Clear()

            'Shani(25-Sep-2015) -- Start
            'ENHANCEMENT : Add Option to Import Bank Branch Master on Common Importation(Issue No : 455)
            'Dim dtTable As DataTable = New DataView(dsList.Tables("Masters"), "id <= 30 or (id = 51 OR id = 60 OR id = 48)", "", DataViewRowState.CurrentRows).ToTable
            Dim dtTable As DataTable = New DataView(dsList.Tables("Masters"), "id <= 30 or (id = 51 OR id = 60 OR id = 48 OR id = 43)", "", DataViewRowState.CurrentRows).ToTable
            'Shani(25-Sep-2015) -- End

            For Each dr As DataRow In dtTable.Rows
                Select Case CInt(dr("Id"))


                    'Pinkal (27-Mar-2013) -- Start
                    'Enhancement : TRA Changes

                    'Case 4, 5, 6 '(STATE,CITY,ZIPCODE)
                    '    Continue For

                    'Case 6 '(STATE,CITY,ZIPCODE)
                    '    Continue For

                    'Pinkal (27-Mar-2013) -- End


                    Case Else
                        lvItem = New ListViewItem
                        lvItem.Tag = dr("Id")
                        lvItem.Text = dr("name").ToString
                        lvItem.SubItems.Add(dr("Totalfield").ToString)
                        lvItem.SubItems.Add(dr("IsImport").ToString)
                        lvMasterList.Items.Add(lvItem)
                End Select
            Next

            If lvMasterList.Items.Count > 29 Then
                colhMasterList.Width = 232 - 18
            Else
                colhMasterList.Width = 232
            End If

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "FillMasters", mstrModuleName)
        End Try
    End Sub

    Public Sub FillTableFields()
        Try

            If txtFilePath.Text <> "" Then

                Dim ImpFile As New IO.FileInfo(txtFilePath.Text)

                If ImpFile.Extension.ToLower = ".xml" Then
                    Dim mdsOther As New DataSet
                    mdsOther.ReadXml(txtFilePath.Text)
                    mdtOther = mdsOther.Tables(0)
                    mdsPayroll = New DataSet
                    mdsPayroll.Tables.Add()
                    mdsPayroll.Tables(0).Merge(mdtOther)

                ElseIf ImpFile.Extension.ToLower = ".xls" Or ImpFile.Extension.ToLower = ".xlsx" Then
                    'S.SANDEEP [12-Jan-2018] -- START
                    'ISSUE/ENHANCEMENT : REF-ID # 0001843
                    'Dim iExcelData As New ExcelData
                    'Dim ds As DataSet = iExcelData.Import(txtFilePath.Text)
                    'mdtOther = iExcelData.Import(txtFilePath.Text).Tables(0).Copy
                    Dim ds As DataSet = OpenXML_Import(txtFilePath.Text)
                    mdtOther = OpenXML_Import(txtFilePath.Text).Tables(0).Copy
                    'S.SANDEEP [12-Jan-2018] -- END

                ElseIf ImpFile.Extension.ToLower = ".txt" Or ImpFile.Extension.ToLower = ".csv" Then
                    ReadFileToArray(txtFilePath.Text)

                    If m_Strarray.Length <> 0 Then

                        For j As Integer = 0 To m_Strarray.GetUpperBound(1)
                            Dim key As String = Nothing
                            If m_Strarray(0, j) Is Nothing Then
                                key = m_Strarray(1, j)
                            Else
                                key = m_Strarray(0, j)
                            End If
                            If m_ImportValue.ContainsKey(key) Then
                                Dim strNewVal As String = m_ImportValue.Item(key)
                                m_ImportValue(key) = strNewVal & "," & j
                            Else
                                For Each cr As Control In pnlControl.Controls

                                    If cr.GetType.FullName.ToString = "System.Windows.Forms.ComboBox" Then
                                        CType(cr, ComboBox).Items.Add(key)
                                    End If

                                Next
                                m_ImportValue.Add(key, j.ToString)

                            End If
                        Next


                    End If
                End If

                If ImpFile.Extension.ToLower <> ".txt" And ImpFile.Extension.ToLower <> ".csv" Then

                    mstrComboColName = ""

                    For Each cr As Control In pnlControl.Controls

                        If cr.GetType.FullName.ToString = "System.Windows.Forms.ComboBox" Then
                            CType(cr, ComboBox).Items.Clear()

                            For Each dtColumns As DataColumn In mdtOther.Columns

                                'Pinkal (12-Oct-2011) -- Start
                                'Enhancement : TRA Changes

                                If dtColumns.ColumnName.Contains("#") Then
                                    dtColumns.ColumnName = dtColumns.ColumnName.ToString().Replace("#", "_")
                                End If

                                CType(cr, ComboBox).Items.Add(dtColumns.ColumnName)


                                If mstrComboColName.Contains(dtColumns.ColumnName.ToString()) = False Then
                                    mstrComboColName &= dtColumns.ColumnName.ToString() & ","
                                End If

                                'Pinkal (12-Oct-2011) -- End
                            Next

                        End If

                    Next
                End If


                'Pinkal (12-Oct-2011) -- Start
                'Enhancement : TRA Changes

                If mstrComboColName.Trim.Length > 0 Then
                    mstrComboColName = mstrComboColName.Substring(0, mstrComboColName.Length - 1)
                End If

                'Pinkal (12-Oct-2011) -- End

            End If

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "FillTableFields", mstrModuleName)
        End Try

    End Sub

    Private Function CreateDataForExcel(ByVal MasterUnkid As Integer) As Boolean
        Dim blnflag As Boolean = False

        Try
            Dim dtMasterData As DataTable = New DataTable()

            For dcol As Integer = 0 To mdtOther.Columns.Count - 1

                'Shani(25-Sep-2015) -- Start
                'ENHANCEMENT : Add Option to Import Bank Branch Master on Common Importation(Issue No : 455)
                'dtMasterData.Columns.Add(mdtOther.Columns(dcol).ColumnName, System.Type.GetType(mdtOther.Columns(dcol).DataType.ToString))
                If MasterUnkid = 43 And mdtOther.Columns(dcol).ColumnName.Contains("Clearing") = True Then
                    dtMasterData.Columns.Add(mdtOther.Columns(dcol).ColumnName, System.Type.GetType("System.Boolean"))
                Else
                    dtMasterData.Columns.Add(mdtOther.Columns(dcol).ColumnName, System.Type.GetType(mdtOther.Columns(dcol).DataType.ToString))
                End If
                'Shani(25-Sep-2015) -- End
            Next
            Dim arstrName As String() = mstrComboColName.Split(CChar(","))

            If arstrName.Length > 0 Then
                Dim mstrString As String = ""

                'Shani(25-Sep-2015) -- Start
                'ENHANCEMENT : Add Option to Import Bank Branch Master on Common Importation(Issue No : 455)
                'For i As Integer = 0 To arstrName.Length - 1
                '    mstrString &= " AND " & arstrName(i).ToString() & " IS NULL "
                'Next
                If MasterUnkid = 43 Then
                    For i As Integer = 0 To arstrName.Length - 1
                        mstrString &= " AND [" & arstrName(i).ToString() & "] IS NULL "
                    Next
                Else
                    For i As Integer = 0 To arstrName.Length - 1
                        mstrString &= " AND " & arstrName(i).ToString() & " IS NULL "
                    Next
                End If

                'Shani(25-Sep-2015) -- End

                If mstrString.Trim.Length > 0 Then
                    mstrString = mstrString.Substring(4, mstrString.Length - 4)
                End If

                Dim drRow As DataRow() = mdtOther.Select(mstrString)

                If drRow.Length > 0 Then

                    For Each dr As DataRow In drRow
                        mdtOther.Rows.Remove(dr)
                    Next
                    mdtOther.AcceptChanges()
                End If

            End If


            For i As Integer = 0 To mdtOther.Rows.Count - 1
                Dim drRow As DataRow = dtMasterData.NewRow
                Dim intCnt As Integer = 0
                For Each dCol As DataColumn In mdtOther.Columns
                    If dicComboName.ContainsKey(intCnt) Then
                        If dicComboName(intCnt).Text = dCol.ColumnName Then
                            drRow.Item(dicComboName(intCnt).Text) = mdtOther.Rows(i)(dicComboName(intCnt).Text)
                        Else
                            drRow.Item(dCol.ColumnName) = mdtOther.Rows(i)(dCol.ColumnName)
                        End If
                    Else
                        drRow.Item(dCol.ColumnName) = mdtOther.Rows(i)(dCol.ColumnName)
                    End If
                    intCnt += 1
                Next

                dtMasterData.Rows.Add(drRow)

            Next

            mdsPayroll = New DataSet
            mdsPayroll.Tables.Add(dtMasterData)

            blnflag = ImportData(mdsPayroll, MasterUnkid, arColumn)

            'Shani(25-Sep-2015) -- Start
            'ENHANCEMENT : Add Option to Import Bank Branch Master on Common Importation(Issue No : 455)
        Catch ex As System.ArgumentException
            eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 7, "Please check the selected file column '") & strClearingFieldName & Language.getMessage(mstrModuleName, 9, "' should only be in 'True' Or 'False' Format. Please modify it and try to import again."), enMsgBoxStyle.Information)
            'Shani(25-Sep-2015) -- End
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "CreateDataForExcel", mstrModuleName)
        End Try
        Return blnflag
    End Function

    Private Function CreateOtherFileData(ByVal MasterUnkid As Integer) As Boolean
        Try
            For Each cr As Control In pnlControl.Controls

                If cr.GetType.FullName.ToString = "System.Windows.Forms.ComboBox" Then

                    Dim strDisplay As String() = m_ImportValue(CType(cr, ComboBox).Text).Split(CChar(","))
                    If strDisplay.Length < 1 Then
                        eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 4, "Please the select the correct field to Import Data."), CType(enMsgBoxStyle.Information + enMsgBoxStyle.OkOnly, enMsgBoxStyle))
                        Return False
                        Exit Function
                    End If

                End If

            Next
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "CreateData_Other", mstrModuleName)
        End Try
        Return True
    End Function

    Private Function ImportData(ByVal mdsPayroll As DataSet, ByVal MasterId As Integer, ByVal arColumn() As String) As Boolean
        Dim blnflag As Boolean = False
        Dim dsFormula As DataSet = Nothing
        Dim dsSimpleSlab As DataSet = Nothing
        Dim dsInexcessof As DataSet = Nothing
        Dim dsWagesTran As DataSet = Nothing
        Dim dsEmpIdInfo As DataSet = Nothing
        Dim dsEmpMemInfo As DataSet = Nothing
        Dim dsEmpAllergies As DataSet = Nothing
        Dim dsEmpDisability As DataSet = Nothing

        Try
            If mdsPayroll.Tables(0).Rows.Count > 0 And arColumn.Length > 0 Then


                For i As Integer = 0 To mdsPayroll.Tables(0).Rows.Count - 1

                    If MasterId = 1 Then 'Common Master

                        Dim objcommon As New clsCommon_Master
                        objcommon._Code = mdsPayroll.Tables(0).Rows(i)(arColumn(0)).ToString
                        objcommon._Alias = mdsPayroll.Tables(0).Rows(i)(arColumn(1)).ToString
                        objcommon._Name = mdsPayroll.Tables(0).Rows(i)(arColumn(2)).ToString

                        If mdsPayroll.Tables(0).Rows(i)(arColumn(3)).ToString.ToUpper.Replace(" ", "_") = clsCommon_Master.enCommonMaster.ADVERTISE_CATEGORY.ToString Then
                            objcommon._Mastertype = clsCommon_Master.enCommonMaster.ADVERTISE_CATEGORY
                        ElseIf mdsPayroll.Tables(0).Rows(i)(arColumn(3)).ToString.ToUpper.Replace(" ", "_") = clsCommon_Master.enCommonMaster.ALLERGIES.ToString Then
                            objcommon._Mastertype = clsCommon_Master.enCommonMaster.ALLERGIES
                        ElseIf mdsPayroll.Tables(0).Rows(i)(arColumn(3)).ToString.ToUpper.Replace(" ", "_") = clsCommon_Master.enCommonMaster.ASSET_CONDITION.ToString Then
                            objcommon._Mastertype = clsCommon_Master.enCommonMaster.ASSET_CONDITION
                        ElseIf mdsPayroll.Tables(0).Rows(i)(arColumn(3)).ToString.ToUpper.Replace(" ", "_") = clsCommon_Master.enCommonMaster.BENEFIT_GROUP.ToString Then
                            objcommon._Mastertype = clsCommon_Master.enCommonMaster.BENEFIT_GROUP
                        ElseIf mdsPayroll.Tables(0).Rows(i)(arColumn(3)).ToString.ToUpper.Replace(" ", "_") = clsCommon_Master.enCommonMaster.BLOOD_GROUP.ToString Then
                            objcommon._Mastertype = clsCommon_Master.enCommonMaster.BLOOD_GROUP
                        ElseIf mdsPayroll.Tables(0).Rows(i)(arColumn(3)).ToString.ToUpper.Replace(" ", "_") = clsCommon_Master.enCommonMaster.COMPLEXION.ToString Then
                            objcommon._Mastertype = clsCommon_Master.enCommonMaster.COMPLEXION
                        ElseIf mdsPayroll.Tables(0).Rows(i)(arColumn(3)).ToString.ToUpper.Replace(" ", "_") = clsCommon_Master.enCommonMaster.DISABILITIES.ToString Then
                            objcommon._Mastertype = clsCommon_Master.enCommonMaster.DISABILITIES
                        ElseIf mdsPayroll.Tables(0).Rows(i)(arColumn(3)).ToString.ToUpper.Replace(" ", "_") = clsCommon_Master.enCommonMaster.EMPLOYEMENT_TYPE.ToString Then
                            objcommon._Mastertype = clsCommon_Master.enCommonMaster.EMPLOYEMENT_TYPE
                        ElseIf mdsPayroll.Tables(0).Rows(i)(arColumn(3)).ToString.ToUpper.Replace(" ", "_") = clsCommon_Master.enCommonMaster.ETHNICITY.ToString Then
                            objcommon._Mastertype = clsCommon_Master.enCommonMaster.ETHNICITY
                        ElseIf mdsPayroll.Tables(0).Rows(i)(arColumn(3)).ToString.ToUpper.Replace(" ", "_") = clsCommon_Master.enCommonMaster.EYES_COLOR.ToString Then
                            objcommon._Mastertype = clsCommon_Master.enCommonMaster.EYES_COLOR
                        ElseIf mdsPayroll.Tables(0).Rows(i)(arColumn(3)).ToString.ToUpper.Replace(" ", "_") = clsCommon_Master.enCommonMaster.HAIR_COLOR.ToString Then
                            objcommon._Mastertype = clsCommon_Master.enCommonMaster.HAIR_COLOR
                        ElseIf mdsPayroll.Tables(0).Rows(i)(arColumn(3)).ToString.ToUpper.Replace(" ", "_") = clsCommon_Master.enCommonMaster.IDENTITY_TYPES.ToString Then
                            objcommon._Mastertype = clsCommon_Master.enCommonMaster.IDENTITY_TYPES
                        ElseIf mdsPayroll.Tables(0).Rows(i)(arColumn(3)).ToString.ToUpper.Replace(" ", "_") = clsCommon_Master.enCommonMaster.INTERVIEW_TYPE.ToString Then
                            objcommon._Mastertype = clsCommon_Master.enCommonMaster.INTERVIEW_TYPE
                        ElseIf mdsPayroll.Tables(0).Rows(i)(arColumn(3)).ToString.ToUpper.Replace(" ", "_") = clsCommon_Master.enCommonMaster.LANGUAGES.ToString Then
                            objcommon._Mastertype = clsCommon_Master.enCommonMaster.LANGUAGES
                        ElseIf mdsPayroll.Tables(0).Rows(i)(arColumn(3)).ToString.ToUpper.Replace(" ", "_") = clsCommon_Master.enCommonMaster.MARRIED_STATUS.ToString Then
                            objcommon._Mastertype = clsCommon_Master.enCommonMaster.MARRIED_STATUS
                        ElseIf mdsPayroll.Tables(0).Rows(i)(arColumn(3)).ToString.ToUpper.Replace(" ", "_") = clsCommon_Master.enCommonMaster.MEMBERSHIP_CATEGORY.ToString Then
                            objcommon._Mastertype = clsCommon_Master.enCommonMaster.MEMBERSHIP_CATEGORY
                        ElseIf mdsPayroll.Tables(0).Rows(i)(arColumn(3)).ToString.ToUpper.Replace(" ", "_") = clsCommon_Master.enCommonMaster.PAY_TYPE.ToString Then
                            objcommon._Mastertype = clsCommon_Master.enCommonMaster.PAY_TYPE
                        ElseIf mdsPayroll.Tables(0).Rows(i)(arColumn(3)).ToString.ToUpper.Replace(" ", "_") = clsCommon_Master.enCommonMaster.QUALIFICATION_COURSE_GROUP.ToString Then
                            objcommon._Mastertype = clsCommon_Master.enCommonMaster.QUALIFICATION_COURSE_GROUP
                        ElseIf mdsPayroll.Tables(0).Rows(i)(arColumn(3)).ToString.ToUpper.Replace(" ", "_") = clsCommon_Master.enCommonMaster.RELATIONS.ToString Then
                            objcommon._Mastertype = clsCommon_Master.enCommonMaster.RELATIONS
                        ElseIf mdsPayroll.Tables(0).Rows(i)(arColumn(3)).ToString.ToUpper.Replace(" ", "_") = clsCommon_Master.enCommonMaster.RELIGION.ToString Then
                            objcommon._Mastertype = clsCommon_Master.enCommonMaster.RELIGION
                        ElseIf mdsPayroll.Tables(0).Rows(i)(arColumn(3)).ToString.ToUpper.Replace(" ", "_") = clsCommon_Master.enCommonMaster.RESULT_GROUP.ToString Then
                            objcommon._Mastertype = clsCommon_Master.enCommonMaster.RESULT_GROUP
                        ElseIf mdsPayroll.Tables(0).Rows(i)(arColumn(3)).ToString.ToUpper.Replace(" ", "_") = clsCommon_Master.enCommonMaster.SHIFT_TYPE.ToString Then
                            objcommon._Mastertype = clsCommon_Master.enCommonMaster.SHIFT_TYPE
                        ElseIf mdsPayroll.Tables(0).Rows(i)(arColumn(3)).ToString.ToUpper.Replace(" ", "_") = clsCommon_Master.enCommonMaster.SKILL_CATEGORY.ToString Then
                            objcommon._Mastertype = clsCommon_Master.enCommonMaster.SKILL_CATEGORY
                        ElseIf mdsPayroll.Tables(0).Rows(i)(arColumn(3)).ToString.ToUpper.Replace(" ", "_") = clsCommon_Master.enCommonMaster.TITLE.ToString Then
                            objcommon._Mastertype = clsCommon_Master.enCommonMaster.TITLE
                        ElseIf mdsPayroll.Tables(0).Rows(i)(arColumn(3)).ToString.ToUpper.Replace(" ", "_") = clsCommon_Master.enCommonMaster.TRAINING_RESOURCES.ToString Then
                            objcommon._Mastertype = clsCommon_Master.enCommonMaster.TRAINING_RESOURCES
                        ElseIf mdsPayroll.Tables(0).Rows(i)(arColumn(3)).ToString.ToUpper.Replace(" ", "_") = clsCommon_Master.enCommonMaster.ASSET_STATUS.ToString Then
                            objcommon._Mastertype = clsCommon_Master.enCommonMaster.ASSET_STATUS
                        ElseIf mdsPayroll.Tables(0).Rows(i)(arColumn(3)).ToString.ToUpper.Replace(" ", "_") = clsCommon_Master.enCommonMaster.ASSETS.ToString Then
                            objcommon._Mastertype = clsCommon_Master.enCommonMaster.ASSETS
                        ElseIf mdsPayroll.Tables(0).Rows(i)(arColumn(3)).ToString.ToUpper.Replace(" ", "_") = clsCommon_Master.enCommonMaster.LETTER_TYPE.ToString Then
                            objcommon._Mastertype = clsCommon_Master.enCommonMaster.LETTER_TYPE

                            'Shani(24-Jun-2016) -- Start
                        ElseIf mdsPayroll.Tables(0).Rows(i)(arColumn(3)).ToString.ToUpper.Replace(" ", "_") = clsCommon_Master.enCommonMaster.PROV_REGION_1.ToString Then
                            objcommon._Mastertype = clsCommon_Master.enCommonMaster.PROV_REGION_1
                        ElseIf mdsPayroll.Tables(0).Rows(i)(arColumn(3)).ToString.ToUpper.Replace(" ", "_") = clsCommon_Master.enCommonMaster.ROAD_STREET_1.ToString Then
                            objcommon._Mastertype = clsCommon_Master.enCommonMaster.ROAD_STREET_1
                        ElseIf mdsPayroll.Tables(0).Rows(i)(arColumn(3)).ToString.ToUpper.Replace(" ", "_") = clsCommon_Master.enCommonMaster.CHIEFDOM.ToString Then
                            objcommon._Mastertype = clsCommon_Master.enCommonMaster.CHIEFDOM
                        ElseIf mdsPayroll.Tables(0).Rows(i)(arColumn(3)).ToString.ToUpper.Replace(" ", "_") = clsCommon_Master.enCommonMaster.VILLAGE.ToString Then
                            objcommon._Mastertype = clsCommon_Master.enCommonMaster.VILLAGE
                        ElseIf mdsPayroll.Tables(0).Rows(i)(arColumn(3)).ToString.ToUpper.Replace(" ", "_") = clsCommon_Master.enCommonMaster.TOWN_1.ToString Then
                            objcommon._Mastertype = clsCommon_Master.enCommonMaster.TOWN_1
                            'Shani(24-Jun-2016) -- End

                        End If


                        If objcommon._Mastertype <= 0 Then
                            eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 6, "Invalid Data."), CType(enMsgBoxStyle.Information + enMsgBoxStyle.OkOnly, enMsgBoxStyle))
                            Return False
                        End If

                        If mdsPayroll.Tables(0).Columns.Contains("Name1") Then
                            objcommon._Name1 = mdsPayroll.Tables(0).Rows(i)("name1").ToString
                        End If

                        If mdsPayroll.Tables(0).Columns.Contains("Name2") Then
                            objcommon._Name2 = mdsPayroll.Tables(0).Rows(i)("name2").ToString
                        End If

                        If mdsPayroll.Tables(0).Columns.Contains("isactive") Then
                            objcommon._Isactive = CBool(mdsPayroll.Tables(0).Rows(i)("isactive").ToString)
                        Else
                            objcommon._Isactive = True
                        End If

                        With objcommon
                            ._FormName = mstrModuleName
                            ._LoginEmployeeunkid = 0
                            ._ClientIP = getIP()
                            ._HostName = getHostName()
                            ._FromWeb = False
                            ._AuditUserId = User._Object._Userunkid
._CompanyUnkid = Company._Object._Companyunkid
                            ._AuditDate = ConfigParameter._Object._CurrentDateAndTime
                        End With

                        blnflag = objcommon.Insert()

                        If objcommon._Message <> "" Then
                            'Sandeep [ 25 APRIL 2011 ] -- Start
                            'Issue : MESSAGE BOX STYLE
                            'DisplayError.Show("-1", objcommon._Message, "ImportData", mstrModuleName)
                            eZeeMsgBox.Show(objcommon._Code & " " & objcommon._Name & " : " & objcommon._Message, enMsgBoxStyle.Information)
                            'Sandeep [ 25 APRIL 2011 ] -- End 
                            Return False
                        End If

                    ElseIf MasterId = 2 Then  'Reason Master

                        Dim objReason As New clsAction_Reason
                        objReason._Code = mdsPayroll.Tables(0).Rows(i)(arColumn(0)).ToString
                        objReason._Reason_Action = mdsPayroll.Tables(0).Rows(i)(arColumn(1)).ToString
                        If mdsPayroll.Tables(0).Columns.Contains("Isreason") Then
                            objReason._Isreason = CBool(mdsPayroll.Tables(0).Rows(i)("Isreason").ToString)
                        Else
                            objReason._Isreason = True
                        End If

                        If mdsPayroll.Tables(0).Columns.Contains("isactive") Then
                            objReason._Isactive = CBool(mdsPayroll.Tables(0).Rows(i)("isactive").ToString)
                        Else
                            objReason._Isactive = True
                        End If
                        With objReason
                            ._FormName = mstrModuleName
                            ._LoginEmployeeunkid = 0
                            ._ClientIP = getIP()
                            ._HostName = getHostName()
                            ._FromWeb = False
                            ._AuditUserId = User._Object._Userunkid
._CompanyUnkid = Company._Object._Companyunkid
                            ._AuditDate = ConfigParameter._Object._CurrentDateAndTime
                        End With
                        blnflag = objReason.Insert()

                        If objReason._Message <> "" Then
                            'Sandeep [ 25 APRIL 2011 ] -- Start
                            'Issue : MESSAGE BOX STYLE
                            'DisplayError.Show("-1", objReason._Message, "ImportData", mstrModuleName)
                            eZeeMsgBox.Show(objReason._Code & " " & objReason._Reason_Action & " : " & objReason._Message, enMsgBoxStyle.Information)
                            'Sandeep [ 25 APRIL 2011 ] -- End 
                            Return False
                        End If

                    ElseIf MasterId = 3 Then  'Skill Master

                        Dim intMasterunkid As Integer = -1
                        intMasterunkid = objImportExport.GetCommonMasterUnkId(clsCommon_Master.enCommonMaster.SKILL_CATEGORY, mdsPayroll.Tables(0).Rows(i)(arColumn(2)).ToString)
                        If intMasterunkid <= 0 Then
                            If mdsPayroll.Tables(0).Rows(i)(arColumn(2)).ToString.Length > 0 Then
                                Dim objCommon As New clsCommon_Master
                                objCommon._Code = mdsPayroll.Tables(0).Rows(i)(arColumn(2)).ToString
                                objCommon._Mastertype = clsCommon_Master.enCommonMaster.SKILL_CATEGORY
                                objCommon._Name = mdsPayroll.Tables(0).Rows(i)(arColumn(2)).ToString
                                objCommon._Isactive = True

                                With objCommon
                                    ._FormName = mstrModuleName
                                    ._LoginEmployeeunkid = 0
                                    ._ClientIP = getIP()
                                    ._HostName = getHostName()
                                    ._FromWeb = False
                                    ._AuditUserId = User._Object._Userunkid
._CompanyUnkid = Company._Object._Companyunkid
                                    ._AuditDate = ConfigParameter._Object._CurrentDateAndTime
                                End With

                                objCommon.Insert()
                                intMasterunkid = objCommon._Masterunkid

                                If objCommon._Message <> "" Then
                                    'Sandeep [ 25 APRIL 2011 ] -- Start
                                    'Issue : MESSAGE BOX STYLE
                                    'DisplayError.Show("-1", objCommon._Message, "ImportData", mstrModuleName)
                                    eZeeMsgBox.Show(objCommon._Code & " " & objCommon._Name & " : " & objCommon._Message, enMsgBoxStyle.Information)
                                    'Sandeep [ 25 APRIL 2011 ] -- End 
                                    Return False
                                End If

                            End If
                        End If

                        Dim objSkill As New clsskill_master
                        objSkill._Skillcode = mdsPayroll.Tables(0).Rows(i)(arColumn(0)).ToString
                        objSkill._Skillname = mdsPayroll.Tables(0).Rows(i)(arColumn(1)).ToString
                        objSkill._Skillcategoryunkid = intMasterunkid
                        If mdsPayroll.Tables(0).Columns.Contains("skillname1") Then
                            objSkill._Skillname1 = mdsPayroll.Tables(0).Rows(i)("skillname1").ToString
                        End If
                        If mdsPayroll.Tables(0).Columns.Contains("skillname2") Then
                            objSkill._Skillname2 = mdsPayroll.Tables(0).Rows(i)("skillname2").ToString
                        End If
                        If mdsPayroll.Tables(0).Columns.Contains("description") Then
                            objSkill._Description = mdsPayroll.Tables(0).Rows(i)("description").ToString
                        End If
                        If mdsPayroll.Tables(0).Columns.Contains("isactive") Then
                            objSkill._Isactive = CBool(mdsPayroll.Tables(0).Rows(i)("isactive").ToString)
                        Else
                            objSkill._Isactive = True
                        End If

                        With objSkill
                            ._FormName = mstrModuleName
                            ._LoginEmployeeunkid = 0
                            ._ClientIP = getIP()
                            ._HostName = getHostName()
                            ._FromWeb = False
                            ._AuditUserId = User._Object._Userunkid
._CompanyUnkid = Company._Object._Companyunkid
                            ._AuditDate = ConfigParameter._Object._CurrentDateAndTime
                        End With

                        blnflag = objSkill.Insert()

                        If objSkill._Message <> "" Then
                            'Sandeep [ 25 APRIL 2011 ] -- Start
                            'Issue : MESSAGE BOX STYLE
                            'DisplayError.Show("-1", objSkill._Message, "ImportData", mstrModuleName)
                            eZeeMsgBox.Show(objSkill._Skillcode & " " & objSkill._Skillname & " : " & objSkill._Message, enMsgBoxStyle.Information)
                            'Sandeep [ 25 APRIL 2011 ] -- End 
                            Return False
                        End If

                    ElseIf MasterId = 4 Then  'State Master

                        Dim intCountryunkid As Integer = -1
                        intCountryunkid = objImportExport.GetCountryUnkid(mdsPayroll.Tables(0).Rows(i)(arColumn(2)).ToString)
                        ' If intCountryunkid <= 0 Then
                        '                            If mdsPayroll.Tables(0).Rows(i)(arColumn(2)).ToString.Length > 0 Then
                        '   Dim objMaster As New clsMasterData
                        '   intCountryunkid = objMaster.InsertCountry(mdsPayroll.Tables(0).Rows(i)(arColumn(2)).ToString)
                        'End If
                        'End If

                        Dim objState As New clsstate_master
                        objState._Countryunkid = intCountryunkid
                        objState._Code = mdsPayroll.Tables(0).Rows(i)(arColumn(0)).ToString
                        objState._Name = mdsPayroll.Tables(0).Rows(i)(arColumn(1)).ToString

                        If mdsPayroll.Tables(0).Columns.Contains("name1") Then
                            objState._Name1 = mdsPayroll.Tables(0).Rows(i)("name1").ToString
                        End If

                        If mdsPayroll.Tables(0).Columns.Contains("name2") Then
                            objState._Name2 = mdsPayroll.Tables(0).Rows(i)("name2").ToString
                        End If

                        If mdsPayroll.Tables(0).Columns.Contains("isactive") Then
                            objState._Isactive = CBool(mdsPayroll.Tables(0).Rows(i)("isactive").ToString)
                        Else
                            objState._Isactive = True
                        End If

                        With objState
                            ._FormName = mstrModuleName
                            ._LoginEmployeeunkid = 0
                            ._ClientIP = getIP()
                            ._HostName = getHostName()
                            ._FromWeb = False
                            ._AuditUserId = User._Object._Userunkid
._CompanyUnkid = Company._Object._Companyunkid
                            ._AuditDate = ConfigParameter._Object._CurrentDateAndTime
                        End With

                        blnflag = objState.Insert()

                        If objState._Message <> "" Then
                            eZeeMsgBox.Show(objState._Code & " " & objState._Name & " : " & objState._Message, enMsgBoxStyle.Information)
                            Return False
                        End If


                    ElseIf MasterId = 5 Then  'City Master

                        Dim intCountryunkid As Integer = -1
                        Dim intStateunkid As Integer = -1

                        intCountryunkid = objImportExport.GetCountryUnkid(mdsPayroll.Tables(0).Rows(i)(arColumn(2)).ToString)
                        'If intCountryunkid <= 0 Then
                        '    If mdsPayroll.Tables(0).Rows(i)(arColumn(2)).ToString.Length > 0 Then
                        '        Dim objMaster As New clsMasterData
                        '        intCountryunkid = objMaster.InsertCountry(mdsPayroll.Tables(0).Rows(i)(arColumn(2)).ToString)
                        '    End If
                        'End If

                        intStateunkid = objImportExport.GetStateUnkid(mdsPayroll.Tables(0).Rows(i)(arColumn(3)).ToString)
                        If intStateunkid <= 0 Then
                            If mdsPayroll.Tables(0).Rows(i)(arColumn(3)).ToString.Length > 0 Then
                                Dim objState As New clsstate_master
                                objState._Countryunkid = intCountryunkid
                                objState._Name = mdsPayroll.Tables(0).Rows(i)(arColumn(3)).ToString
                                With objState
                                    ._FormName = mstrModuleName
                                    ._LoginEmployeeunkid = 0
                                    ._ClientIP = getIP()
                                    ._HostName = getHostName()
                                    ._FromWeb = False
                                    ._AuditUserId = User._Object._Userunkid
._CompanyUnkid = Company._Object._Companyunkid
                                    ._AuditDate = ConfigParameter._Object._CurrentDateAndTime
                                End With
                                objState.Insert()
                                intStateunkid = objState._Stateunkid

                                If objState._Message <> "" Then
                                    eZeeMsgBox.Show(objState._Code & " " & objState._Name & " : " & objState._Message, enMsgBoxStyle.Information)
                                    Return False
                                End If

                            End If
                        End If

                        Dim objCity As New clscity_master
                        objCity._Countryunkid = intCountryunkid
                        objCity._Stateunkid = intStateunkid
                        objCity._Code = mdsPayroll.Tables(0).Rows(i)(arColumn(0)).ToString
                        objCity._Name = mdsPayroll.Tables(0).Rows(i)(arColumn(1)).ToString

                        If mdsPayroll.Tables(0).Columns.Contains("name1") Then
                            objCity._Name1 = mdsPayroll.Tables(0).Rows(i)("name1").ToString
                        End If

                        If mdsPayroll.Tables(0).Columns.Contains("name2") Then
                            objCity._Name2 = mdsPayroll.Tables(0).Rows(i)("name2").ToString
                        End If

                        If mdsPayroll.Tables(0).Columns.Contains("isactive") Then
                            objCity._Isactive = CBool(mdsPayroll.Tables(0).Rows(i)("isactive").ToString)
                        Else
                            objCity._Isactive = True
                        End If
                        With objCity
                            ._FormName = mstrModuleName
                            ._LoginEmployeeunkid = 0
                            ._ClientIP = getIP()
                            ._HostName = getHostName()
                            ._FromWeb = False
                            ._AuditUserId = User._Object._Userunkid
._CompanyUnkid = Company._Object._Companyunkid
                            ._AuditDate = ConfigParameter._Object._CurrentDateAndTime
                        End With
                        blnflag = objCity.Insert()

                        If objCity._Message <> "" Then
                            eZeeMsgBox.Show(objCity._Code & " " & objCity._Name & " : " & objCity._Message, enMsgBoxStyle.Information)
                            Return False
                        End If

                    ElseIf MasterId = 6 Then  'ZipCode Master

                        Dim intCountryunkid As Integer = -1
                        Dim intStateunkid As Integer = -1
                        Dim intCityunkid As Integer = -1

                        intCountryunkid = objImportExport.GetCountryUnkid(mdsPayroll.Tables(0).Rows(i)(arColumn(2)).ToString)
                        'If intCountryunkid <= 0 Then
                        '    If mdsPayroll.Tables(0).Rows(i)(arColumn(2)).ToString.Length > 0 Then
                        '        Dim objMaster As New clsMasterData
                        '        intCountryunkid = objMaster.InsertCountry(mdsPayroll.Tables(0).Rows(i)(arColumn(2)).ToString)
                        '    End If
                        'End If

                        intStateunkid = objImportExport.GetStateUnkid(mdsPayroll.Tables(0).Rows(i)(arColumn(3)).ToString)
                        If intStateunkid <= 0 Then
                            If mdsPayroll.Tables(0).Rows(i)(arColumn(3)).ToString.Length > 0 Then
                                Dim objState As New clsstate_master
                                objState._Countryunkid = intCountryunkid
                                objState._Code = mdsPayroll.Tables(0).Rows(i)(arColumn(3)).ToString
                                objState._Name = mdsPayroll.Tables(0).Rows(i)(arColumn(3)).ToString
                                With objState
                                    ._FormName = mstrModuleName
                                    ._LoginEmployeeunkid = 0
                                    ._ClientIP = getIP()
                                    ._HostName = getHostName()
                                    ._FromWeb = False
                                    ._AuditUserId = User._Object._Userunkid
._CompanyUnkid = Company._Object._Companyunkid
                                    ._AuditDate = ConfigParameter._Object._CurrentDateAndTime
                                End With
                                objState.Insert()
                                intStateunkid = objState._Stateunkid

                                If objState._Message <> "" Then
                                    'Sandeep [ 25 APRIL 2011 ] -- Start
                                    'Issue : MESSAGE BOX STYLE
                                    'DisplayError.Show("-1", objState._Message, "ImportData", mstrModuleName)
                                    eZeeMsgBox.Show(objState._Code & " " & objState._Name & " : " & objState._Message, enMsgBoxStyle.Information)
                                    'Sandeep [ 25 APRIL 2011 ] -- End 

                                    Return False
                                End If

                            End If
                        End If

                        intCityunkid = objImportExport.GetCityUnkid(mdsPayroll.Tables(0).Rows(i)(arColumn(4)).ToString)
                        If intCityunkid <= 0 Then
                            If mdsPayroll.Tables(0).Rows(i)(arColumn(4)).ToString.Length > 0 Then
                                Dim objCity As New clscity_master
                                objCity._Countryunkid = intCountryunkid
                                objCity._Stateunkid = intStateunkid
                                objCity._Code = mdsPayroll.Tables(0).Rows(i)(arColumn(4)).ToString
                                objCity._Name = mdsPayroll.Tables(0).Rows(i)(arColumn(4)).ToString
                                With objCity
                                    ._FormName = mstrModuleName
                                    ._LoginEmployeeunkid = 0
                                    ._ClientIP = getIP()
                                    ._HostName = getHostName()
                                    ._FromWeb = False
                                    ._AuditUserId = User._Object._Userunkid
._CompanyUnkid = Company._Object._Companyunkid
                                    ._AuditDate = ConfigParameter._Object._CurrentDateAndTime
                                End With
                                objCity.Insert()
                                intCityunkid = objCity._Cityunkid

                                If objCity._Message <> "" Then
                                    'Sandeep [ 25 APRIL 2011 ] -- Start
                                    'Issue : MESSAGE BOX STYLE
                                    'DisplayError.Show("-1", objCity._Message, "ImportData", mstrModuleName)
                                    eZeeMsgBox.Show(objCity._Code & " " & objCity._Name & " : " & objCity._Message, enMsgBoxStyle.Information)
                                    'Sandeep [ 25 APRIL 2011 ] -- End 

                                    Return False
                                End If

                            End If
                        End If

                        Dim objZipcode As New clszipcode_master
                        If objImportExport.GetZipcodeUnkid(mdsPayroll.Tables(0).Rows(i)(arColumn(1)).ToString) <= 0 Then
                            objZipcode._Countryunkid = intCountryunkid
                            objZipcode._Stateunkid = intStateunkid
                            objZipcode._Cityunkid = intCityunkid
                            objZipcode._Zipcode_Code = mdsPayroll.Tables(0).Rows(i)(arColumn(0)).ToString
                            objZipcode._Zipcode_No = mdsPayroll.Tables(0).Rows(i)(arColumn(1)).ToString
                            If mdsPayroll.Tables(0).Columns.Contains("isactive") Then
                                objZipcode._Isactive = CBool(mdsPayroll.Tables(0).Rows(i)("isactive").ToString)
                            Else
                                objZipcode._Isactive = True
                            End If
                            With objZipcode
                                ._FormName = mstrModuleName
                                ._LoginEmployeeunkid = 0
                                ._ClientIP = getIP()
                                ._HostName = getHostName()
                                ._FromWeb = False
                                ._AuditUserId = User._Object._Userunkid
._CompanyUnkid = Company._Object._Companyunkid
                                ._AuditDate = ConfigParameter._Object._CurrentDateAndTime
                            End With
                            blnflag = objZipcode.Insert()

                            If objZipcode._Message <> "" Then
                                'Sandeep [ 25 APRIL 2011 ] -- Start
                                'Issue : MESSAGE BOX STYLE
                                'DisplayError.Show("-1", objZipcode._Message, "ImportData", mstrModuleName)
                                eZeeMsgBox.Show(objZipcode._Zipcode_Code & " " & objZipcode._Zipcode_No & " : " & objZipcode._Message, enMsgBoxStyle.Information)
                                'Sandeep [ 25 APRIL 2011 ] -- End 

                                Return False
                            End If
                        End If
                        
                        'S.SANDEEP [ 04 APRIL 2012 ] -- END



                    ElseIf MasterId = 7 Then  'Advertise Master

                        Dim intMasterunkid As Integer = -1
                        Dim intCountryunkid As Integer = -1
                        Dim intStateunkid As Integer = -1
                        Dim intCityunkid As Integer = -1
                        Dim intPincodeunkid As Integer = -1

                        intMasterunkid = objImportExport.GetCommonMasterUnkId(clsCommon_Master.enCommonMaster.ADVERTISE_CATEGORY, mdsPayroll.Tables(0).Rows(i)(arColumn(1)).ToString)
                        If intMasterunkid <= 0 Then
                            If mdsPayroll.Tables(0).Rows(i)(arColumn(1)).ToString.Length > 0 Then
                                Dim objCommon As New clsCommon_Master
                                objCommon._Code = mdsPayroll.Tables(0).Rows(i)(arColumn(1)).ToString
                                objCommon._Mastertype = clsCommon_Master.enCommonMaster.ADVERTISE_CATEGORY
                                objCommon._Name = mdsPayroll.Tables(0).Rows(i)(arColumn(1)).ToString
                                objCommon._Isactive = True

                                With objCommon
                                    ._FormName = mstrModuleName
                                    ._LoginEmployeeunkid = 0
                                    ._ClientIP = getIP()
                                    ._HostName = getHostName()
                                    ._FromWeb = False
                                    ._AuditUserId = User._Object._Userunkid
._CompanyUnkid = Company._Object._Companyunkid
                                    ._AuditDate = ConfigParameter._Object._CurrentDateAndTime
                                End With

                                objCommon.Insert()
                                intMasterunkid = objCommon._Masterunkid

                                If objCommon._Message <> "" Then
                                    'Sandeep [ 25 APRIL 2011 ] -- Start
                                    'Issue : MESSAGE BOX STYLE
                                    'DisplayError.Show("-1", objCommon._Message, "ImportData", mstrModuleName)
                                    eZeeMsgBox.Show(objCommon._Code & " " & objCommon._Name & " : " & objCommon._Message, enMsgBoxStyle.Information)
                                    'Sandeep [ 25 APRIL 2011 ] -- End 
                                    Return False
                                End If

                            End If
                        End If


                        'S.SANDEEP [ 04 APRIL 2012 ] -- START
                        'ENHANCEMENT : TRA CHANGES
                        'If mdsPayroll.Tables(0).Columns.Contains("country") Then
                        '    intCountryunkid = objImportExport.GetCountryUnkid(mdsPayroll.Tables(0).Rows(i)("country").ToString)
                        '    If intCountryunkid <= 0 Then
                        '        If mdsPayroll.Tables(0).Rows(i)("country").ToString.Length > 0 Then
                        '            Dim objMaster As New clsMasterData
                        '            intCountryunkid = objMaster.InsertCountry(mdsPayroll.Tables(0).Rows(i)("country").ToString)
                        '        End If
                        '    End If
                        'End If
                        'S.SANDEEP [ 04 APRIL 2012 ] -- END


                        If mdsPayroll.Tables(0).Columns.Contains("state") Then
                            intStateunkid = objImportExport.GetStateUnkid(mdsPayroll.Tables(0).Rows(i)("state").ToString)
                            If intStateunkid <= 0 Then
                                If mdsPayroll.Tables(0).Rows(i)("state").ToString.Length > 0 Then
                                    Dim objState As New clsstate_master
                                    objState._Countryunkid = intCountryunkid
                                    objState._Name = mdsPayroll.Tables(0).Rows(i)("state").ToString
                                    With objState
                                        ._FormName = mstrModuleName
                                        ._LoginEmployeeunkid = 0
                                        ._ClientIP = getIP()
                                        ._HostName = getHostName()
                                        ._FromWeb = False
                                        ._AuditUserId = User._Object._Userunkid
._CompanyUnkid = Company._Object._Companyunkid
                                        ._AuditDate = ConfigParameter._Object._CurrentDateAndTime
                                    End With
                                    objState.Insert()
                                    intStateunkid = objState._Stateunkid

                                    If objState._Message <> "" Then
                                        'Sandeep [ 25 APRIL 2011 ] -- Start
                                        'Issue : MESSAGE BOX STYLE
                                        'DisplayError.Show("-1", objState._Message, "ImportData", mstrModuleName)
                                        eZeeMsgBox.Show(objState._Code & " " & objState._Name & " : " & objState._Message, enMsgBoxStyle.Information)
                                        'Sandeep [ 25 APRIL 2011 ] -- End 
                                        Return False
                                    End If
                                End If
                            End If
                        End If

                        If mdsPayroll.Tables(0).Columns.Contains("city") Then
                            intCityunkid = objImportExport.GetCityUnkid(mdsPayroll.Tables(0).Rows(i)("city").ToString)
                            If intCityunkid <= 0 Then
                                If mdsPayroll.Tables(0).Rows(i)("city").ToString.Length > 0 Then
                                    Dim objCity As New clscity_master
                                    objCity._Countryunkid = intCountryunkid
                                    objCity._Stateunkid = intStateunkid
                                    objCity._Name = mdsPayroll.Tables(0).Rows(i)("city").ToString
                                    With objCity
                                        ._FormName = mstrModuleName
                                        ._LoginEmployeeunkid = 0
                                        ._ClientIP = getIP()
                                        ._HostName = getHostName()
                                        ._FromWeb = False
                                        ._AuditUserId = User._Object._Userunkid
._CompanyUnkid = Company._Object._Companyunkid
                                        ._AuditDate = ConfigParameter._Object._CurrentDateAndTime
                                    End With
                                    objCity.Insert()
                                    intCityunkid = objCity._Cityunkid

                                    If objCity._Message <> "" Then
                                        'Sandeep [ 25 APRIL 2011 ] -- Start
                                        'Issue : MESSAGE BOX STYLE
                                        'DisplayError.Show("-1", objCity._Message, "ImportData", mstrModuleName)
                                        eZeeMsgBox.Show(objCity._Code & " " & objCity._Name & " : " & objCity._Message, enMsgBoxStyle.Information)
                                        'Sandeep [ 25 APRIL 2011 ] -- End 
                                        Return False
                                    End If

                                End If
                            End If
                        End If

                        If mdsPayroll.Tables(0).Columns.Contains("pincode") Then
                            intPincodeunkid = objImportExport.GetZipcodeUnkid(mdsPayroll.Tables(0).Rows(i)("pincode").ToString)
                            If intPincodeunkid <= 0 Then
                                If mdsPayroll.Tables(0).Rows(i)("pincode").ToString.Length > 0 Then
                                    Dim objZipcode As New clszipcode_master
                                    objZipcode._Countryunkid = intCountryunkid
                                    objZipcode._Stateunkid = intStateunkid
                                    objZipcode._Cityunkid = intCityunkid
                                    objZipcode._Zipcode_Code = mdsPayroll.Tables(0).Rows(i)("pincode").ToString
                                    With objZipcode
                                        ._FormName = mstrModuleName
                                        ._LoginEmployeeunkid = 0
                                        ._ClientIP = getIP()
                                        ._HostName = getHostName()
                                        ._FromWeb = False
                                        ._AuditUserId = User._Object._Userunkid
._CompanyUnkid = Company._Object._Companyunkid
                                        ._AuditDate = ConfigParameter._Object._CurrentDateAndTime
                                    End With
                                    objZipcode.Insert()
                                    intPincodeunkid = objZipcode._Zipcodeunkid

                                    If objZipcode._Message <> "" Then
                                        'Sandeep [ 25 APRIL 2011 ] -- Start
                                        'Issue : MESSAGE BOX STYLE
                                        'DisplayError.Show("-1", objZipcode._Message, "ImportData", mstrModuleName)
                                        eZeeMsgBox.Show(objZipcode._Zipcode_Code & " " & objZipcode._Zipcode_No & " : " & objZipcode._Message, enMsgBoxStyle.Information)
                                        'Sandeep [ 25 APRIL 2011 ] -- End 
                                        Return False
                                    End If

                                End If
                            End If
                        End If

                        Dim objAdvertise As New clsAdvertise_master
                        objAdvertise._Categoryunkid = intMasterunkid
                        objAdvertise._Countryunkid = intCountryunkid
                        objAdvertise._Stateunkid = intStateunkid
                        objAdvertise._Cityunkid = intCityunkid
                        objAdvertise._Pincodeunkid = intPincodeunkid
                        objAdvertise._Company = mdsPayroll.Tables(0).Rows(i)(arColumn(0)).ToString

                        If mdsPayroll.Tables(0).Columns.Contains("address1") Then
                            objAdvertise._Address1 = mdsPayroll.Tables(0).Rows(i)("address1").ToString
                        End If

                        If mdsPayroll.Tables(0).Columns.Contains("address2") Then
                            objAdvertise._Address2 = mdsPayroll.Tables(0).Rows(i)("address2").ToString
                        End If

                        If mdsPayroll.Tables(0).Columns.Contains("contactno") Then
                            objAdvertise._Contactno = mdsPayroll.Tables(0).Rows(i)("contactno").ToString
                        End If

                        If mdsPayroll.Tables(0).Columns.Contains("contactperson") Then
                            objAdvertise._Contactperson = mdsPayroll.Tables(0).Rows(i)("contactperson").ToString
                        End If

                        If mdsPayroll.Tables(0).Columns.Contains("fax") Then
                            objAdvertise._Fax = mdsPayroll.Tables(0).Rows(i)("fax").ToString
                        End If

                        If mdsPayroll.Tables(0).Columns.Contains("email") Then
                            objAdvertise._Email = mdsPayroll.Tables(0).Rows(i)("email").ToString
                        End If

                        If mdsPayroll.Tables(0).Columns.Contains("phone") Then
                            objAdvertise._Phone = mdsPayroll.Tables(0).Rows(i)("phone").ToString
                        End If

                        If mdsPayroll.Tables(0).Columns.Contains("isactive") Then
                            objAdvertise._Isactive = CBool(mdsPayroll.Tables(0).Rows(i)("isactive").ToString)
                        End If

                        With objAdvertise
                            ._FormName = mstrModuleName
                            ._LoginEmployeeunkid = 0
                            ._ClientIP = getIP()
                            ._HostName = getHostName()
                            ._FromWeb = False
                            ._AuditUserId = User._Object._Userunkid
._CompanyUnkid = Company._Object._Companyunkid
                            ._AuditDate = ConfigParameter._Object._CurrentDateAndTime
                        End With

                        blnflag = objAdvertise.Insert()

                        If objAdvertise._Message <> "" Then
                            'Sandeep [ 25 APRIL 2011 ] -- Start
                            'Issue : MESSAGE BOX STYLE
                            'DisplayError.Show("-1", objAdvertise._Message, "ImportData", mstrModuleName)
                            eZeeMsgBox.Show(objAdvertise._Company & objAdvertise._Message, enMsgBoxStyle.Information)
                            'Sandeep [ 25 APRIL 2011 ] -- End 
                            Return False
                        End If

                    ElseIf MasterId = 8 Then  'Qualification Master

                        Dim intMasterunkid As Integer = -1
                        intMasterunkid = objImportExport.GetCommonMasterUnkId(clsCommon_Master.enCommonMaster.QUALIFICATION_COURSE_GROUP, mdsPayroll.Tables(0).Rows(i)(arColumn(2)).ToString)
                        If intMasterunkid <= 0 Then
                            If mdsPayroll.Tables(0).Rows(i)(arColumn(2)).ToString.Length > 0 Then
                                Dim objCommon As New clsCommon_Master
                                objCommon._Code = mdsPayroll.Tables(0).Rows(i)(arColumn(2)).ToString
                                objCommon._Mastertype = clsCommon_Master.enCommonMaster.QUALIFICATION_COURSE_GROUP
                                objCommon._Name = mdsPayroll.Tables(0).Rows(i)(arColumn(2)).ToString
                                objCommon._Isactive = True

                                With objCommon
                                    ._FormName = mstrModuleName
                                    ._LoginEmployeeunkid = 0
                                    ._ClientIP = getIP()
                                    ._HostName = getHostName()
                                    ._FromWeb = False
                                    ._AuditUserId = User._Object._Userunkid
._CompanyUnkid = Company._Object._Companyunkid
                                    ._AuditDate = ConfigParameter._Object._CurrentDateAndTime
                                End With

                                objCommon.Insert()
                                intMasterunkid = objCommon._Masterunkid

                                If objCommon._Message <> "" Then
                                    eZeeMsgBox.Show(objCommon._Code & " " & objCommon._Name & " : " & objCommon._Message, enMsgBoxStyle.Information)
                                    Return False
                                End If

                            End If
                        End If

                        Dim intResultGroupunkid As Integer = -1
                        intResultGroupunkid = objImportExport.GetCommonMasterUnkId(clsCommon_Master.enCommonMaster.RESULT_GROUP, mdsPayroll.Tables(0).Rows(i)(arColumn(3)).ToString)
                        'S.SANDEEP [ 22 JUL 2014 ] -- START
                        If intResultGroupunkid <= 0 Then
                            'If intMasterunkid <= 0 Then
                            'S.SANDEEP [ 22 JUL 2014 ] -- END
                            If mdsPayroll.Tables(0).Rows(i)(arColumn(3)).ToString.Length > 0 Then
                                Dim objCommon As New clsCommon_Master
                                objCommon._Code = mdsPayroll.Tables(0).Rows(i)(arColumn(3)).ToString
                                objCommon._Mastertype = clsCommon_Master.enCommonMaster.RESULT_GROUP
                                objCommon._Name = mdsPayroll.Tables(0).Rows(i)(arColumn(3)).ToString
                                objCommon._Isactive = True

                                With objCommon
                                    ._FormName = mstrModuleName
                                    ._LoginEmployeeunkid = 0
                                    ._ClientIP = getIP()
                                    ._HostName = getHostName()
                                    ._FromWeb = False
                                    ._AuditUserId = User._Object._Userunkid
._CompanyUnkid = Company._Object._Companyunkid
                                    ._AuditDate = ConfigParameter._Object._CurrentDateAndTime
                                End With

                                objCommon.Insert()
                                intResultGroupunkid = objCommon._Masterunkid

                                If objCommon._Message <> "" Then
                                    eZeeMsgBox.Show(objCommon._Code & " " & objCommon._Name & " : " & objCommon._Message, enMsgBoxStyle.Information)
                                    Return False
                                End If

                            End If
                        End If

                        Dim objQualification As New clsqualification_master
                        objQualification._Qualificationgroupunkid = intMasterunkid
                        objQualification._Qualificationcode = mdsPayroll.Tables(0).Rows(i)(arColumn(0)).ToString
                        objQualification._Qualificationname = mdsPayroll.Tables(0).Rows(i)(arColumn(1)).ToString
                        objQualification._ResultGroupunkid = intResultGroupunkid
                        objQualification._IsSyncWithRecruitment = CBool(mdsPayroll.Tables(0).Rows(i)(arColumn(4)).ToString)

                        If mdsPayroll.Tables(0).Columns.Contains("description") Then
                            objQualification._Description = mdsPayroll.Tables(0).Rows(i)("description").ToString
                        End If

                        If mdsPayroll.Tables(0).Columns.Contains("qualificationname1") Then
                            objQualification._Qualificationname1 = mdsPayroll.Tables(0).Rows(i)("qualificationname1").ToString
                        End If

                        If mdsPayroll.Tables(0).Columns.Contains("qualificationname2") Then
                            objQualification._Qualificationname2 = mdsPayroll.Tables(0).Rows(i)("qualificationname2").ToString
                        End If

                        If mdsPayroll.Tables(0).Columns.Contains("isactive") Then
                            objQualification._Isactive = CBool(mdsPayroll.Tables(0).Rows(i)("isactive").ToString)
                        Else
                            objQualification._Isactive = True
                        End If

                        With objQualification
                            ._FormName = mstrModuleName
                            ._LoginEmployeeunkid = 0
                            ._ClientIP = getIP()
                            ._HostName = getHostName()
                            ._FromWeb = False
                            ._AuditUserId = User._Object._Userunkid
._CompanyUnkid = Company._Object._Companyunkid
                            ._AuditDate = ConfigParameter._Object._CurrentDateAndTime
                        End With

                        blnflag = objQualification.Insert()

                        If objQualification._Message <> "" Then
                            eZeeMsgBox.Show(objQualification._Qualificationcode & " " & objQualification._Qualificationname & " : " & objQualification._Message, enMsgBoxStyle.Information)
                            Return False
                        End If


                    ElseIf MasterId = 9 Then  'Result Master

                        Dim intMasterunkid As Integer = -1
                        intMasterunkid = objImportExport.GetCommonMasterUnkId(clsCommon_Master.enCommonMaster.RESULT_GROUP, mdsPayroll.Tables(0).Rows(i)(arColumn(2)).ToString)
                        If intMasterunkid <= 0 Then
                            If mdsPayroll.Tables(0).Rows(i)(arColumn(2)).ToString.Length > 0 Then
                                Dim objCommon As New clsCommon_Master
                                objCommon._Code = mdsPayroll.Tables(0).Rows(i)(arColumn(2)).ToString
                                objCommon._Mastertype = clsCommon_Master.enCommonMaster.RESULT_GROUP
                                objCommon._Name = mdsPayroll.Tables(0).Rows(i)(arColumn(2)).ToString
                                objCommon._Isactive = True

                                With objCommon
                                    ._FormName = mstrModuleName
                                    ._LoginEmployeeunkid = 0
                                    ._ClientIP = getIP()
                                    ._HostName = getHostName()
                                    ._FromWeb = False
                                    ._AuditUserId = User._Object._Userunkid
._CompanyUnkid = Company._Object._Companyunkid
                                    ._AuditDate = ConfigParameter._Object._CurrentDateAndTime
                                End With

                                objCommon.Insert()
                                intMasterunkid = objCommon._Masterunkid

                                If objCommon._Message <> "" Then
                                    eZeeMsgBox.Show(objCommon._Code & " " & objCommon._Name & " : " & objCommon._Message, enMsgBoxStyle.Information)
                                    Return False
                                End If

                            End If
                        End If

                        Dim objResult As New clsresult_master
                        objResult._Resultgroupunkid = intMasterunkid
                        objResult._Resultcode = mdsPayroll.Tables(0).Rows(i)(arColumn(0)).ToString
                        objResult._Resultname = mdsPayroll.Tables(0).Rows(i)(arColumn(1)).ToString

                        'Pinkal (18-Aug-2013) -- Start
                        'Enhancement : TRA Changes
                        objResult._ResultLevel = CInt(mdsPayroll.Tables(0).Rows(i)(arColumn(3)).ToString)
                        'Pinkal (18-Aug-2013) -- End


                        If mdsPayroll.Tables(0).Columns.Contains("description") Then
                            objResult._Description = mdsPayroll.Tables(0).Rows(i)("description").ToString
                        End If

                        If mdsPayroll.Tables(0).Columns.Contains("resultname1") Then
                            objResult._Resultname1 = mdsPayroll.Tables(0).Rows(i)("resultname1").ToString
                        End If

                        If mdsPayroll.Tables(0).Columns.Contains("resultname2") Then
                            objResult._Resultname2 = mdsPayroll.Tables(0).Rows(i)("resultname2").ToString
                        End If

                        If mdsPayroll.Tables(0).Columns.Contains("isactive") Then
                            objResult._Isactive = CBool(mdsPayroll.Tables(0).Rows(i)("isactive").ToString)
                        Else
                            objResult._Isactive = True
                        End If

                        With objResult
                            ._FormName = mstrModuleName
                            ._LoginEmployeeunkid = 0
                            ._ClientIP = getIP()
                            ._HostName = getHostName()
                            ._FromWeb = False
                            ._AuditUserId = User._Object._Userunkid
._CompanyUnkid = Company._Object._Companyunkid
                            ._AuditDate = ConfigParameter._Object._CurrentDateAndTime
                        End With

                        blnflag = objResult.Insert()

                        If objResult._Message <> "" Then
                            eZeeMsgBox.Show(objResult._Resultcode & " " & objResult._Resultname & " : " & objResult._Message, enMsgBoxStyle.Information)
                            Return False
                        End If


                    ElseIf MasterId = 10 Then  'Membership Master

                        Dim intMasterunkid As Integer = -1
                        intMasterunkid = objImportExport.GetCommonMasterUnkId(clsCommon_Master.enCommonMaster.MEMBERSHIP_CATEGORY, mdsPayroll.Tables(0).Rows(i)(arColumn(2)).ToString)
                        If intMasterunkid <= 0 Then
                            If mdsPayroll.Tables(0).Rows(i)(arColumn(2)).ToString.Length > 0 Then
                                Dim objCommon As New clsCommon_Master
                                objCommon._Code = mdsPayroll.Tables(0).Rows(i)(arColumn(2)).ToString
                                objCommon._Mastertype = clsCommon_Master.enCommonMaster.MEMBERSHIP_CATEGORY
                                objCommon._Name = mdsPayroll.Tables(0).Rows(i)(arColumn(2)).ToString
                                objCommon._Isactive = True

                                With objCommon
                                    ._FormName = mstrModuleName
                                    ._LoginEmployeeunkid = 0
                                    ._ClientIP = getIP()
                                    ._HostName = getHostName()
                                    ._FromWeb = False
                                    ._AuditUserId = User._Object._Userunkid
._CompanyUnkid = Company._Object._Companyunkid
                                    ._AuditDate = ConfigParameter._Object._CurrentDateAndTime
                                End With

                                objCommon.Insert()
                                intMasterunkid = objCommon._Masterunkid

                                If objCommon._Message <> "" Then
                                    'Sandeep [ 25 APRIL 2011 ] -- Start
                                    'Issue : MESSAGE BOX STYLE
                                    'DisplayError.Show("-1", objCommon._Message, "ImportData", mstrModuleName)
                                    eZeeMsgBox.Show(objCommon._Code & " " & objCommon._Name & " : " & objCommon._Message, enMsgBoxStyle.Information)
                                    'Sandeep [ 25 APRIL 2011 ] -- End 
                                    Return False
                                End If

                            End If
                        End If

                        Dim objmembership As New clsmembership_master
                        objmembership._Membershipcategoryunkid = intMasterunkid
                        objmembership._Membershipcode = mdsPayroll.Tables(0).Rows(i)(arColumn(0)).ToString
                        objmembership._Membershipname = mdsPayroll.Tables(0).Rows(i)(arColumn(1)).ToString

                        If mdsPayroll.Tables(0).Columns.Contains("description") Then
                            objmembership._Description = mdsPayroll.Tables(0).Rows(i)("description").ToString
                        End If

                        If mdsPayroll.Tables(0).Columns.Contains("membershipname1") Then
                            objmembership._Membershipname1 = mdsPayroll.Tables(0).Rows(i)("membershipname1").ToString
                        End If

                        If mdsPayroll.Tables(0).Columns.Contains("membershipname2") Then
                            objmembership._Membershipname2 = mdsPayroll.Tables(0).Rows(i)("membershipname2").ToString
                        End If

                        If mdsPayroll.Tables(0).Columns.Contains("isactive") Then
                            objmembership._Isactive = CBool(mdsPayroll.Tables(0).Rows(i)("isactive").ToString)
                        Else
                            objmembership._Isactive = True
                        End If

                        With objmembership
                            ._FormName = mstrModuleName
                            ._LoginEmployeeunkid = 0
                            ._ClientIP = getIP()
                            ._HostName = getHostName()
                            ._FromWeb = False
                            ._AuditUserId = User._Object._Userunkid
._CompanyUnkid = Company._Object._Companyunkid
                            ._AuditDate = ConfigParameter._Object._CurrentDateAndTime
                        End With

                        blnflag = objmembership.Insert()

                        If objmembership._Message <> "" Then
                            'Sandeep [ 25 APRIL 2011 ] -- Start
                            'Issue : MESSAGE BOX STYLE
                            'DisplayError.Show("-1", objmembership._Message, "ImportData", mstrModuleName)
                            eZeeMsgBox.Show(objmembership._Membershipcode & " " & objmembership._Membershipname & " : " & objmembership._Message, enMsgBoxStyle.Information)
                            'Sandeep [ 25 APRIL 2011 ] -- End 
                            Return False
                        End If


                    ElseIf MasterId = 11 Then  'Benefit Plan Master

                        Dim intMasterunkid As Integer = -1
                        intMasterunkid = objImportExport.GetCommonMasterUnkId(clsCommon_Master.enCommonMaster.BENEFIT_GROUP, mdsPayroll.Tables(0).Rows(i)(arColumn(2)).ToString)
                        If intMasterunkid <= 0 Then
                            If mdsPayroll.Tables(0).Rows(i)(arColumn(2)).ToString.Length > 0 Then
                                Dim objCommon As New clsCommon_Master
                                objCommon._Code = mdsPayroll.Tables(0).Rows(i)(arColumn(2)).ToString
                                objCommon._Mastertype = clsCommon_Master.enCommonMaster.BENEFIT_GROUP
                                objCommon._Name = mdsPayroll.Tables(0).Rows(i)(arColumn(2)).ToString
                                objCommon._Isactive = True

                                With objCommon
                                    ._FormName = mstrModuleName
                                    ._LoginEmployeeunkid = 0
                                    ._ClientIP = getIP()
                                    ._HostName = getHostName()
                                    ._FromWeb = False
                                    ._AuditUserId = User._Object._Userunkid
._CompanyUnkid = Company._Object._Companyunkid
                                    ._AuditDate = ConfigParameter._Object._CurrentDateAndTime
                                End With

                                objCommon.Insert()
                                intMasterunkid = objCommon._Masterunkid

                                If objCommon._Message <> "" Then
                                    eZeeMsgBox.Show(objCommon._Code & " " & objCommon._Name & " : " & objCommon._Message, enMsgBoxStyle.Information)
                                    Return False
                                End If

                            End If
                        End If

                        Dim objBenefitplan As New clsbenefitplan_master
                        objBenefitplan._Benefitgroupunkid = intMasterunkid
                        objBenefitplan._Benefitplancode = mdsPayroll.Tables(0).Rows(i)(arColumn(0)).ToString
                        objBenefitplan._Benefitplanname = mdsPayroll.Tables(0).Rows(i)(arColumn(1)).ToString

                        If mdsPayroll.Tables(0).Columns.Contains("description") Then
                            objBenefitplan._Description = mdsPayroll.Tables(0).Rows(i)("description").ToString
                        End If

                        If mdsPayroll.Tables(0).Columns.Contains("name1") Then
                            objBenefitplan._Benefitplanname1 = mdsPayroll.Tables(0).Rows(i)("name1").ToString
                        End If

                        If mdsPayroll.Tables(0).Columns.Contains("name2") Then
                            objBenefitplan._Benefitplanname2 = mdsPayroll.Tables(0).Rows(i)("name2").ToString
                        End If

                        If mdsPayroll.Tables(0).Columns.Contains("isactive") Then
                            objBenefitplan._Isactive = CBool(mdsPayroll.Tables(0).Rows(i)("isactive").ToString)
                        Else
                            objBenefitplan._Isactive = True
                        End If

                        With objBenefitplan
                            ._FormName = mstrModuleName
                            ._LoginEmployeeunkid = 0
                            ._ClientIP = getIP()
                            ._HostName = getHostName()
                            ._FromWeb = False
                            ._AuditUserId = User._Object._Userunkid
._CompanyUnkid = Company._Object._Companyunkid
                            ._AuditDate = ConfigParameter._Object._CurrentDateAndTime
                        End With

                        blnflag = objBenefitplan.Insert()

                        If objBenefitplan._Message <> "" Then
                            'Sandeep [ 25 APRIL 2011 ] -- Start
                            'Issue : MESSAGE BOX STYLE
                            'DisplayError.Show("-1", objBenefitplan._Message, "ImportData", mstrModuleName)
                            eZeeMsgBox.Show(objBenefitplan._Benefitplancode & " " & objBenefitplan._Benefitplanname & " : " & objBenefitplan._Message, enMsgBoxStyle.Information)
                            'Sandeep [ 25 APRIL 2011 ] -- End 
                            Return False
                        End If


                    ElseIf MasterId = 12 Then  'Branch Master

                        Dim objStation As New clsStation
                        objStation._Code = mdsPayroll.Tables(0).Rows(i)(arColumn(0)).ToString
                        objStation._Name = mdsPayroll.Tables(0).Rows(i)(arColumn(1)).ToString

                        If mdsPayroll.Tables(0).Columns.Contains("description") Then
                            objStation._Description = mdsPayroll.Tables(0).Rows(i)("description").ToString
                        End If

                        If mdsPayroll.Tables(0).Columns.Contains("address1") Then
                            objStation._Address1 = mdsPayroll.Tables(0).Rows(i)("address1").ToString
                        End If

                        If mdsPayroll.Tables(0).Columns.Contains("address2") Then
                            objStation._Address2 = mdsPayroll.Tables(0).Rows(i)("address2").ToString
                        End If

                        If mdsPayroll.Tables(0).Columns.Contains("phone1") Then
                            objStation._Phone1 = mdsPayroll.Tables(0).Rows(i)("phone1").ToString
                        End If

                        If mdsPayroll.Tables(0).Columns.Contains("phone2") Then
                            objStation._Phone2 = mdsPayroll.Tables(0).Rows(i)("phone2").ToString
                        End If

                        If mdsPayroll.Tables(0).Columns.Contains("phone3") Then
                            objStation._Phone3 = mdsPayroll.Tables(0).Rows(i)("phone3").ToString
                        End If

                        If mdsPayroll.Tables(0).Columns.Contains("fax") Then
                            objStation._Fax = mdsPayroll.Tables(0).Rows(i)("fax").ToString
                        End If

                        If mdsPayroll.Tables(0).Columns.Contains("email") Then
                            objStation._Email = mdsPayroll.Tables(0).Rows(i)("email").ToString
                        End If

                        If mdsPayroll.Tables(0).Columns.Contains("website") Then
                            objStation._Website = mdsPayroll.Tables(0).Rows(i)("website").ToString
                        End If

                        If mdsPayroll.Tables(0).Columns.Contains("isactive") Then
                            objStation._Isactive = CBool(mdsPayroll.Tables(0).Rows(i)("isactive").ToString)
                        Else
                            objStation._Isactive = True
                        End If
                        With objStation
                            ._FormName = mstrModuleName
                            ._LoginEmployeeunkid = 0
                            ._ClientIP = getIP()
                            ._HostName = getHostName()
                            ._FromWeb = False
                            ._AuditUserId = User._Object._Userunkid
._CompanyUnkid = Company._Object._Companyunkid
                            ._AuditDate = ConfigParameter._Object._CurrentDateAndTime
                        End With
                        blnflag = objStation.Insert()

                        If objStation._Message <> "" Then
                            eZeeMsgBox.Show(objStation._Code & " " & objStation._Name & " : " & objStation._Message, enMsgBoxStyle.Information)
                            Return False
                        End If

                    ElseIf MasterId = 13 Then  'Department Group Master

                        Dim objDeptGrp As New clsDepartmentGroup
                        objDeptGrp._Code = mdsPayroll.Tables(0).Rows(i)(arColumn(0)).ToString
                        objDeptGrp._Name = mdsPayroll.Tables(0).Rows(i)(arColumn(1)).ToString

                        If mdsPayroll.Tables(0).Columns.Contains("description") Then
                            objDeptGrp._Description = mdsPayroll.Tables(0).Rows(i)("description").ToString
                        End If

                        If mdsPayroll.Tables(0).Columns.Contains("name1") Then
                            objDeptGrp._Name1 = mdsPayroll.Tables(0).Rows(i)("name1").ToString
                        End If

                        If mdsPayroll.Tables(0).Columns.Contains("name2") Then
                            objDeptGrp._Name2 = mdsPayroll.Tables(0).Rows(i)("name2").ToString
                        End If

                        If mdsPayroll.Tables(0).Columns.Contains("isactive") Then
                            objDeptGrp._Isactive = CBool(mdsPayroll.Tables(0).Rows(i)("isactive").ToString)
                        Else
                            objDeptGrp._Isactive = True
                        End If

                        With objDeptGrp
                            ._FormName = mstrModuleName
                            ._LoginEmployeeunkid = 0
                            ._ClientIP = getIP()
                            ._HostName = getHostName()
                            ._FromWeb = False
                            ._AuditUserId = User._Object._Userunkid
._CompanyUnkid = Company._Object._Companyunkid
                            ._AuditDate = ConfigParameter._Object._CurrentDateAndTime
                        End With

                        blnflag = objDeptGrp.Insert()

                        If objDeptGrp._Message <> "" Then
                            eZeeMsgBox.Show(objDeptGrp._Code & " " & objDeptGrp._Name & " : " & objDeptGrp._Message, enMsgBoxStyle.Information)
                            Return False
                        End If

                    ElseIf MasterId = 14 Then  'Department Master

                        'Dim intStationunkid As Integer = -1
                        'Dim intDeptGrpunkId As Integer = -1

                        'If mdsPayroll.Tables(0).Columns.Contains("StationName") Then
                        '    intStationunkid = objImportExport.GetStationUnkid(mdsPayroll.Tables(0).Rows(i)("StationName").ToString)
                        '    If intStationunkid <= 0 Then
                        '        If mdsPayroll.Tables(0).Rows(i)("StationName").ToString.Length > 0 Then
                        '            Dim objStation As New clsStation
                        '            objStation._Code = mdsPayroll.Tables(0).Rows(i)("StationName").ToString
                        '            objStation._Name = mdsPayroll.Tables(0).Rows(i)("StationName").ToString
                        '            objStation._Isactive = True
                        '            objStation.Insert()
                        '            intStationunkid = objStation._Stationunkid

                        '            If objStation._Message <> "" Then
                        '                'Sandeep [ 25 APRIL 2011 ] -- Start
                        '                'Issue : MESSAGE BOX STYLE
                        '                'DisplayError.Show("-1", objStation._Message, "ImportData", mstrModuleName)
                        '                eZeeMsgBox.Show(objStation._Code & " " & objStation._Name & " : " & objStation._Message, enMsgBoxStyle.Information)
                        '                'Sandeep [ 25 APRIL 2011 ] -- End 
                        '                Return False
                        '            End If

                        '        End If
                        '    End If
                        'End If

                        'intDeptGrpunkId = objImportExport.GetDepartmentGroupUnkid(mdsPayroll.Tables(0).Rows(i)(arColumn(2)).ToString)
                        'If intDeptGrpunkId <= 0 Then
                        '    If mdsPayroll.Tables(0).Rows(i)(arColumn(2)).ToString.Length > 0 Then
                        '        Dim objDeptGrp As New clsDepartmentGroup
                        '        objDeptGrp._Code = mdsPayroll.Tables(0).Rows(i)(arColumn(2)).ToString
                        '        objDeptGrp._Name = mdsPayroll.Tables(0).Rows(i)(arColumn(2)).ToString
                        '        objDeptGrp._Isactive = True
                        '        objDeptGrp.Insert()
                        '        intDeptGrpunkId = objDeptGrp._Deptgroupunkid

                        '        If objDeptGrp._Message <> "" Then
                        '            'Sandeep [ 25 APRIL 2011 ] -- Start
                        '            'Issue : MESSAGE BOX STYLE
                        '            'DisplayError.Show("-1", objDeptGrp._Message, "ImportData", mstrModuleName)
                        '            eZeeMsgBox.Show(objDeptGrp._Code & " " & objDeptGrp._Name & " : " & objDeptGrp._Message, enMsgBoxStyle.Information)
                        '            'Sandeep [ 25 APRIL 2011 ] -- End 
                        '            Return False
                        '        End If

                        '    End If
                        'End If

                        Dim objDepartment As New clsDepartment
                        'objDepartment._Stationunkid = intStationunkid
                        'objDepartment._Deptgroupunkid = intDeptGrpunkId
                        objDepartment._Code = mdsPayroll.Tables(0).Rows(i)(arColumn(0)).ToString
                        objDepartment._Name = mdsPayroll.Tables(0).Rows(i)(arColumn(1)).ToString

                        If mdsPayroll.Tables(0).Columns.Contains("description") Then
                            objDepartment._Description = mdsPayroll.Tables(0).Rows(i)("description").ToString
                        End If

                        If mdsPayroll.Tables(0).Columns.Contains("name1") Then
                            objDepartment._Name1 = mdsPayroll.Tables(0).Rows(i)("name1").ToString
                        End If

                        If mdsPayroll.Tables(0).Columns.Contains("name2") Then
                            objDepartment._Name2 = mdsPayroll.Tables(0).Rows(i)("name2").ToString
                        End If

                        If mdsPayroll.Tables(0).Columns.Contains("isactive") Then
                            objDepartment._Isactive = CBool(mdsPayroll.Tables(0).Rows(i)("isactive").ToString)
                        Else
                            objDepartment._Isactive = True
                        End If

                        With objDepartment
                            ._FormName = mstrModuleName
                            ._LoginEmployeeunkid = 0
                            ._ClientIP = getIP()
                            ._HostName = getHostName()
                            ._FromWeb = False
                            ._AuditUserId = User._Object._Userunkid
._CompanyUnkid = Company._Object._Companyunkid
                            ._AuditDate = ConfigParameter._Object._CurrentDateAndTime
                        End With

                        blnflag = objDepartment.Insert()

                        If objDepartment._Message <> "" Then
                            'Sandeep [ 25 APRIL 2011 ] -- Start
                            'Issue : MESSAGE BOX STYLE
                            'DisplayError.Show("-1", objDepartment._Message, "ImportData", mstrModuleName)
                            eZeeMsgBox.Show(objDepartment._Code & " " & objDepartment._Name & " : " & objDepartment._Message, enMsgBoxStyle.Information)
                            'Sandeep [ 25 APRIL 2011 ] -- End 
                            Return False
                        End If


                        'S.SANDEEP [ 07 NOV 2011 ] -- START
                        'ENHANCEMENT : TRA CHANGES
                    ElseIf MasterId = 15 Then   'SECTION GROUP
                        Dim objSGrp As New clsSectionGroup
                        objSGrp._Code = mdsPayroll.Tables(0).Rows(i)(arColumn(0)).ToString
                        objSGrp._Name = mdsPayroll.Tables(0).Rows(i)(arColumn(1)).ToString

                        If mdsPayroll.Tables(0).Columns.Contains("description") Then
                            objSGrp._Description = mdsPayroll.Tables(0).Rows(i)("description").ToString
                        End If

                        If mdsPayroll.Tables(0).Columns.Contains("name1") Then
                            objSGrp._Name1 = mdsPayroll.Tables(0).Rows(i)("name1").ToString
                        End If

                        If mdsPayroll.Tables(0).Columns.Contains("name2") Then
                            objSGrp._Name2 = mdsPayroll.Tables(0).Rows(i)("name2").ToString
                        End If

                        If mdsPayroll.Tables(0).Columns.Contains("isactive") Then
                            objSGrp._Isactive = CBool(mdsPayroll.Tables(0).Rows(i)("isactive").ToString)
                        Else
                            objSGrp._Isactive = True
                        End If

                        With objSGrp
                            ._FormName = mstrModuleName
                            ._LoginEmployeeunkid = 0
                            ._ClientIP = getIP()
                            ._HostName = getHostName()
                            ._FromWeb = False
                            ._AuditUserId = User._Object._Userunkid
._CompanyUnkid = Company._Object._Companyunkid
                            ._AuditDate = ConfigParameter._Object._CurrentDateAndTime
                        End With

                        If objSGrp.Insert = False Then
                            eZeeMsgBox.Show(objSGrp._Code & " " & objSGrp._Name & " : " & objSGrp._Message, enMsgBoxStyle.Information)
                            Return False
                        End If
                    ElseIf MasterId = 16 Then   'SECTION MASTER
                        'Dim intDeptunkId As Integer = -1
                        'intDeptunkId = objImportExport.GetDepartmentUnkid(mdsPayroll.Tables(0).Rows(i)(arColumn(2)).ToString)
                        'If intDeptunkId <= 0 Then
                        '    If mdsPayroll.Tables(0).Rows(i)(arColumn(2)).ToString.Length > 0 Then
                        '        Dim objDept As New clsDepartment
                        '        objDept._Code = mdsPayroll.Tables(0).Rows(i)(arColumn(2)).ToString
                        '        objDept._Name = mdsPayroll.Tables(0).Rows(i)(arColumn(2)).ToString
                        '        objDept._Isactive = True
                        '        objDept.Insert()
                        '        intDeptunkId = objDept._Departmentunkid

                        '        If objDept._Message <> "" Then
                        '            'Sandeep [ 25 APRIL 2011 ] -- Start
                        '            'Issue : MESSAGE BOX STYLE
                        '            'DisplayError.Show("-1", objDept._Message, "ImportData", mstrModuleName)
                        '            eZeeMsgBox.Show(objDept._Code & " " & objDept._Name & " : " & objDept._Message, enMsgBoxStyle.Information)
                        '            'Sandeep [ 25 APRIL 2011 ] -- End 
                        '            Return False
                        '        End If

                        '    End If
                        'End If

                        'Dim intSectionGrpId As Integer = -1
                        'intSectionGrpId = objImportExport.GetSectionGroupUnkid(mdsPayroll.Tables(0).Rows(i)(arColumn(1)).ToString)
                        'If intSectionGrpId <= 0 Then
                        '    If mdsPayroll.Tables(0).Rows(i)(arColumn(1)).ToString.Trim.Length > 0 Then
                        '        Dim objSGrp As New clsSectionGroup
                        '        objSGrp._Code = mdsPayroll.Tables(0).Rows(i)(arColumn(0)).ToString
                        '        objSGrp._Isactive = True
                        '        objSGrp._Name = mdsPayroll.Tables(0).Rows(i)(arColumn(1)).ToString
                        '        If objSGrp.Insert Then
                        '            intSectionGrpId = objSGrp._Sectiongroupunkid
                        '        Else
                        '            eZeeMsgBox.Show(objSGrp._Code & " " & objSGrp._Name & " : " & objSGrp._Message, enMsgBoxStyle.Information)
                        '            Return False
                        '        End If
                        '    End If
                        'End If

                        Dim objSection As New clsSections
                        'objSection._Departmentunkid = intDeptunkId
                        objSection._Code = mdsPayroll.Tables(0).Rows(i)(arColumn(0)).ToString
                        objSection._Name = mdsPayroll.Tables(0).Rows(i)(arColumn(1)).ToString

                        If mdsPayroll.Tables(0).Columns.Contains("description") Then
                            objSection._Description = mdsPayroll.Tables(0).Rows(i)("description").ToString
                        End If

                        If mdsPayroll.Tables(0).Columns.Contains("name1") Then
                            objSection._Name1 = mdsPayroll.Tables(0).Rows(i)("name1").ToString
                        End If

                        If mdsPayroll.Tables(0).Columns.Contains("name2") Then
                            objSection._Name2 = mdsPayroll.Tables(0).Rows(i)("name2").ToString
                        End If

                        If mdsPayroll.Tables(0).Columns.Contains("isactive") Then
                            objSection._Isactive = CBool(mdsPayroll.Tables(0).Rows(i)("isactive").ToString)
                        Else
                            objSection._Isactive = True
                        End If
                        With objSection
                            ._FormName = mstrModuleName
                            ._LoginEmployeeunkid = 0
                            ._ClientIP = getIP()
                            ._HostName = getHostName()
                            ._FromWeb = False
                            ._AuditUserId = User._Object._Userunkid
._CompanyUnkid = Company._Object._Companyunkid
                            ._AuditDate = ConfigParameter._Object._CurrentDateAndTime
                        End With
                        blnflag = objSection.Insert()

                        If objSection._Message <> "" Then
                            'Sandeep [ 25 APRIL 2011 ] -- Start
                            'Issue : MESSAGE BOX STYLE
                            'DisplayError.Show("-1", objSection._Message, "ImportData", mstrModuleName)
                            eZeeMsgBox.Show(objSection._Code & " " & objSection._Name & " : " & objSection._Message, enMsgBoxStyle.Information)
                            'Sandeep [ 25 APRIL 2011 ] -- End 
                            Return False
                        End If
                    ElseIf MasterId = 17 Then   'UNIT GROUP
                        'Dim intSectionunkId As Integer = -1
                        'intSectionunkId = objImportExport.GetSectionUnkid(mdsPayroll.Tables(0).Rows(i)(arColumn(2)).ToString)
                        'If intSectionunkId <= 0 Then
                        '    If mdsPayroll.Tables(0).Rows(i)(arColumn(2)).ToString.Length > 0 Then
                        '        Dim objSection As New clsSections
                        '        objSection._Code = mdsPayroll.Tables(0).Rows(i)(arColumn(2)).ToString
                        '        objSection._Name = mdsPayroll.Tables(0).Rows(i)(arColumn(2)).ToString
                        '        objSection._Isactive = True
                        '        objSection.Insert()
                        '        intSectionunkId = objSection._Sectionunkid
                        '        If objSection._Message <> "" Then
                        '            eZeeMsgBox.Show(objSection._Code & " " & objSection._Name & " : " & objSection._Message, enMsgBoxStyle.Information)
                        '            Return False
                        '        End If
                        '    End If
                        'End If

                        Dim objUnitGrp As New clsUnitGroup
                        'objUnitGrp._Sectionunkid = intSectionunkId
                        objUnitGrp._Code = mdsPayroll.Tables(0).Rows(i)(arColumn(0)).ToString.Trim
                        objUnitGrp._Name = mdsPayroll.Tables(0).Rows(i)(arColumn(1)).ToString.Trim

                        If mdsPayroll.Tables(0).Columns.Contains("description") Then
                            objUnitGrp._Description = mdsPayroll.Tables(0).Rows(i)("description").ToString
                        End If

                        If mdsPayroll.Tables(0).Columns.Contains("name1") Then
                            objUnitGrp._Name1 = mdsPayroll.Tables(0).Rows(i)("name1").ToString
                        End If

                        If mdsPayroll.Tables(0).Columns.Contains("name2") Then
                            objUnitGrp._Name2 = mdsPayroll.Tables(0).Rows(i)("name2").ToString
                        End If

                        If mdsPayroll.Tables(0).Columns.Contains("isactive") Then
                            objUnitGrp._Isactive = CBool(mdsPayroll.Tables(0).Rows(i)("isactive").ToString)
                        Else
                            objUnitGrp._Isactive = True
                        End If

                        With objUnitGrp
                            ._FormName = mstrModuleName
                            ._LoginEmployeeunkid = 0
                            ._ClientIP = getIP()
                            ._HostName = getHostName()
                            ._FromWeb = False
                            ._AuditUserId = User._Object._Userunkid
._CompanyUnkid = Company._Object._Companyunkid
                            ._AuditDate = ConfigParameter._Object._CurrentDateAndTime
                        End With

                        blnflag = objUnitGrp.Insert()

                        If objUnitGrp._Message <> "" Then
                            eZeeMsgBox.Show(objUnitGrp._Code & " " & objUnitGrp._Name & " : " & objUnitGrp._Message, enMsgBoxStyle.Information)
                            Return False
                        End If

                    ElseIf MasterId = 18 Then   'UNIT MASTER

                        'Dim intUnitGrpId As Integer = -1
                        'intUnitGrpId = objImportExport.GetUnitGroupUnkid(mdsPayroll.Tables(0).Rows(i)(arColumn(3)).ToString)
                        'If intUnitGrpId <= 0 Then
                        '    If mdsPayroll.Tables(0).Rows(i)(arColumn(2)).ToString.Length > 0 Then
                        '        Dim objUnitGrp As New clsUnitGroup
                        '        objUnitGrp._Code = mdsPayroll.Tables(0).Rows(i)(arColumn(3)).ToString
                        '        objUnitGrp._Name = mdsPayroll.Tables(0).Rows(i)(arColumn(3)).ToString
                        '        objUnitGrp._Isactive = True
                        '        objUnitGrp.Insert()
                        '        intUnitGrpId = objUnitGrp._Sectionunkid
                        '        If objUnitGrp._Message <> "" Then
                        '            eZeeMsgBox.Show(objUnitGrp._Code & " " & objUnitGrp._Name & " : " & objUnitGrp._Message, enMsgBoxStyle.Information)
                        '            Return False
                        '        End If
                        '    End If
                        'End If

                        'Dim intSectionunkId As Integer = -1
                        'intSectionunkId = objImportExport.GetSectionUnkid(mdsPayroll.Tables(0).Rows(i)(arColumn(2)).ToString)
                        'If intSectionunkId <= 0 Then
                        '    If mdsPayroll.Tables(0).Rows(i)(arColumn(2)).ToString.Length > 0 Then
                        '        Dim objSection As New clsSections
                        '        objSection._Code = mdsPayroll.Tables(0).Rows(i)(arColumn(2)).ToString
                        '        objSection._Name = mdsPayroll.Tables(0).Rows(i)(arColumn(2)).ToString
                        '        objSection._Isactive = True
                        '        objSection.Insert()
                        '        intSectionunkId = objSection._Sectionunkid

                        '        If objSection._Message <> "" Then
                        '            'Sandeep [ 25 APRIL 2011 ] -- Start
                        '            'Issue : MESSAGE BOX STYLE
                        '            'DisplayError.Show("-1", objSection._Message, "ImportData", mstrModuleName)
                        '            eZeeMsgBox.Show(objSection._Code & " " & objSection._Name & " : " & objSection._Message, enMsgBoxStyle.Information)
                        '            'Sandeep [ 25 APRIL 2011 ] -- End 
                        '            Return False
                        '        End If

                        '    End If
                        'End If

                        Dim objUnit As New clsUnits
                        'objUnit._Sectionunkid = intSectionunkId
                        'objUnit._Unitgroupunkid = intUnitGrpId
                        objUnit._Code = mdsPayroll.Tables(0).Rows(i)(arColumn(0)).ToString
                        objUnit._Name = mdsPayroll.Tables(0).Rows(i)(arColumn(1)).ToString

                        If mdsPayroll.Tables(0).Columns.Contains("description") Then
                            objUnit._Description = mdsPayroll.Tables(0).Rows(i)("description").ToString
                        End If

                        If mdsPayroll.Tables(0).Columns.Contains("name1") Then
                            objUnit._Name1 = mdsPayroll.Tables(0).Rows(i)("name1").ToString
                        End If

                        If mdsPayroll.Tables(0).Columns.Contains("name2") Then
                            objUnit._Name2 = mdsPayroll.Tables(0).Rows(i)("name2").ToString
                        End If

                        If mdsPayroll.Tables(0).Columns.Contains("isactive") Then
                            objUnit._Isactive = CBool(mdsPayroll.Tables(0).Rows(i)("isactive").ToString)
                        Else
                            objUnit._Isactive = True
                        End If
                        With objUnit
                            ._FormName = mstrModuleName
                            ._LoginEmployeeunkid = 0
                            ._ClientIP = getIP()
                            ._HostName = getHostName()
                            ._FromWeb = False
                            ._AuditUserId = User._Object._Userunkid
._CompanyUnkid = Company._Object._Companyunkid
                            ._AuditDate = ConfigParameter._Object._CurrentDateAndTime
                        End With
                        blnflag = objUnit.Insert()

                        If objUnit._Message <> "" Then
                            'Sandeep [ 25 APRIL 2011 ] -- Start
                            'Issue : MESSAGE BOX STYLE
                            'DisplayError.Show("-1", objUnit._Message, "ImportData", mstrModuleName)
                            eZeeMsgBox.Show(objUnit._Code & " " & objUnit._Name & " : " & objUnit._Message, enMsgBoxStyle.Information)
                            'Sandeep [ 25 APRIL 2011 ] -- End 
                            Return False
                        End If

                    ElseIf MasterId = 19 Then   'TEAM MASTER
                        'Dim intUnitUnkid As Integer = -1
                        'intUnitUnkid = objImportExport.GetUnitUnkId(mdsPayroll.Tables(0).Rows(i)(arColumn(2)).ToString.Trim)
                        'If intUnitUnkid <= 0 Then
                        '    If mdsPayroll.Tables(0).Rows(i)(arColumn(2)).ToString.Trim.Length > 0 Then
                        '        Dim objUnit As New clsUnits
                        '        objUnit._Sectionunkid = 0
                        '        objUnit._Unitgroupunkid = 0
                        '        objUnit._Isactive = True
                        '        objUnit._Code = mdsPayroll.Tables(0).Rows(i)(arColumn(2)).ToString
                        '        objUnit._Name = mdsPayroll.Tables(0).Rows(i)(arColumn(2)).ToString
                        '        If objUnit.Insert() Then
                        '            intUnitUnkid = objUnit._Unitunkid
                        '        Else
                        '            eZeeMsgBox.Show(objUnit._Code & " " & objUnit._Name & " : " & objUnit._Message, enMsgBoxStyle.Information)
                        '            Return False
                        '        End If
                        '    End If
                        'End If
                        Dim objTeam As New clsTeams
                        'objTeam._Unitunkid = intUnitUnkid
                        objTeam._Code = mdsPayroll.Tables(0).Rows(i)(arColumn(0)).ToString.Trim
                        objTeam._Name = mdsPayroll.Tables(0).Rows(i)(arColumn(1)).ToString.Trim
                        If mdsPayroll.Tables(0).Columns.Contains("description") Then
                            objTeam._Description = mdsPayroll.Tables(0).Rows(i)("description").ToString
                        End If

                        If mdsPayroll.Tables(0).Columns.Contains("name1") Then
                            objTeam._Name1 = mdsPayroll.Tables(0).Rows(i)("name1").ToString
                        End If

                        If mdsPayroll.Tables(0).Columns.Contains("name2") Then
                            objTeam._Name2 = mdsPayroll.Tables(0).Rows(i)("name2").ToString
                        End If

                        If mdsPayroll.Tables(0).Columns.Contains("isactive") Then
                            objTeam._Isactive = CBool(mdsPayroll.Tables(0).Rows(i)("isactive").ToString)
                        Else
                            objTeam._Isactive = True
                        End If

                        With objTeam
                            ._FormName = mstrModuleName
                            ._LoginEmployeeunkid = 0
                            ._ClientIP = getIP()
                            ._HostName = getHostName()
                            ._FromWeb = False
                            ._AuditUserId = User._Object._Userunkid
._CompanyUnkid = Company._Object._Companyunkid
                            ._AuditDate = ConfigParameter._Object._CurrentDateAndTime
                        End With

                        blnflag = objTeam.Insert()

                        If objTeam._Message <> "" Then
                            eZeeMsgBox.Show(objTeam._Code & " " & objTeam._Name & " : " & objTeam._Message, enMsgBoxStyle.Information)
                            Return False
                        End If
                        'S.SANDEEP [ 07 NOV 2011 ] -- END

                    ElseIf MasterId = 20 Then  'Job Group Master

                        Dim objjobGrp As New clsJobGroup
                        objjobGrp._Code = mdsPayroll.Tables(0).Rows(i)(arColumn(0)).ToString
                        objjobGrp._Name = mdsPayroll.Tables(0).Rows(i)(arColumn(1)).ToString

                        If mdsPayroll.Tables(0).Columns.Contains("description") Then
                            objjobGrp._Description = mdsPayroll.Tables(0).Rows(i)("description").ToString
                        End If

                        If mdsPayroll.Tables(0).Columns.Contains("name1") Then
                            objjobGrp._Name1 = mdsPayroll.Tables(0).Rows(i)("name1").ToString
                        End If

                        If mdsPayroll.Tables(0).Columns.Contains("name2") Then
                            objjobGrp._Name2 = mdsPayroll.Tables(0).Rows(i)("name2").ToString
                        End If

                        If mdsPayroll.Tables(0).Columns.Contains("isactive") Then
                            objjobGrp._Isactive = CBool(mdsPayroll.Tables(0).Rows(i)("isactive").ToString)
                        Else
                            objjobGrp._Isactive = True
                        End If

                        With objjobGrp
                            ._FormName = mstrModuleName
                            ._LoginEmployeeunkid = 0
                            ._ClientIP = getIP()
                            ._HostName = getHostName()
                            ._FromWeb = False
                            ._AuditUserId = User._Object._Userunkid
._CompanyUnkid = Company._Object._Companyunkid
                            ._AuditDate = ConfigParameter._Object._CurrentDateAndTime
                        End With

                        blnflag = objjobGrp.Insert()

                        If objjobGrp._Message <> "" Then
                            'Sandeep [ 25 APRIL 2011 ] -- Start
                            'Issue : MESSAGE BOX STYLE
                            'DisplayError.Show("-1", objjobGrp._Message, "ImportData", mstrModuleName)
                            eZeeMsgBox.Show(objjobGrp._Code & " " & objjobGrp._Name & " : " & objjobGrp._Message, enMsgBoxStyle.Information)
                            'Sandeep [ 25 APRIL 2011 ] -- End 
                            Return False
                        End If


                    ElseIf MasterId = 21 Then  'Job Master

                        Dim intSectionunkId As Integer = -1
                        Dim intJobGrpunkid As Integer = -1
                        Dim intGradeunkid As Integer = -1
                        Dim intUnitunkid As Integer = -1

                        'S.SANDEEP [20 JUN 2016] -- START
                        Dim intBranchUnkid As Integer = -1
                        Dim intDepartmentGroupUnkid As Integer = -1
                        Dim intDepartmentUnkid As Integer = -1
                        Dim intSectionGroupUnkid As Integer = -1
                        Dim intUnitGroupUnkid As Integer = -1
                        Dim intTeamUnkid As Integer = -1
                        Dim intClassGroupUnkid As Integer = -1
                        Dim intClassUnkid As Integer = -1
                        'S.SANDEEP [20 JUN 2016] -- END


                        'Pinkal (06-Jul-2018) -- Start
                        'Bug - Solved Bug during Job Importation.

                        'If mdsPayroll.Tables(0).Columns.Contains("jobgrpoup") Then
                        '    intJobGrpunkid = objImportExport.GetJobGroupUnkId(mdsPayroll.Tables(0).Rows(i)("jobgrpoup").ToString)
                        '    If intJobGrpunkid <= 0 Then
                        '        If mdsPayroll.Tables(0).Rows(i)("jobgrpoup").ToString.Length > 0 Then
                        '            Dim objJobGrp As New clsJobGroup
                        '            objJobGrp._Code = mdsPayroll.Tables(0).Rows(i)("jobgrpoup").ToString
                        '            objJobGrp._Name = mdsPayroll.Tables(0).Rows(i)("jobgrpoup").ToString

                        If mdsPayroll.Tables(0).Columns.Contains("JobGroup") Then
                            intJobGrpunkid = objImportExport.GetJobGroupUnkId(mdsPayroll.Tables(0).Rows(i)("JobGroup").ToString)
                            If intJobGrpunkid <= 0 Then
                                If mdsPayroll.Tables(0).Rows(i)("JobGroup").ToString.Length > 0 Then
                                    Dim objJobGrp As New clsJobGroup
                                    objJobGrp._Code = mdsPayroll.Tables(0).Rows(i)("JobGroup").ToString
                                    objJobGrp._Name = mdsPayroll.Tables(0).Rows(i)("JobGroup").ToString
                                    'Pinkal (06-Jul-2018) -- End

                                    objJobGrp._Isactive = True
                                    With objJobGrp
                                        ._FormName = mstrModuleName
                                        ._LoginEmployeeunkid = 0
                                        ._ClientIP = getIP()
                                        ._HostName = getHostName()
                                        ._FromWeb = False
                                        ._AuditUserId = User._Object._Userunkid
._CompanyUnkid = Company._Object._Companyunkid
                                        ._AuditDate = ConfigParameter._Object._CurrentDateAndTime
                                    End With
                                    objJobGrp.Insert()
                                    intJobGrpunkid = objJobGrp._Jobgroupunkid

                                    If objJobGrp._Message <> "" Then
                                        'Sandeep [ 25 APRIL 2011 ] -- Start
                                        'Issue : MESSAGE BOX STYLE
                                        'DisplayError.Show("-1", objJobGrp._Message, "ImportData", mstrModuleName)
                                        eZeeMsgBox.Show(objJobGrp._Code & " " & objJobGrp._Name & " : " & objJobGrp._Message, enMsgBoxStyle.Information)
                                        'Sandeep [ 25 APRIL 2011 ] -- End 
                                        Return False
                                    End If

                                End If
                            End If
                        End If


                        If mdsPayroll.Tables(0).Columns.Contains("unit") Then
                            intUnitunkid = objImportExport.GetUnitUnkId(mdsPayroll.Tables(0).Rows(i)("unit").ToString)
                            If intUnitunkid <= 0 Then
                                If mdsPayroll.Tables(0).Rows(i)("unit").ToString.Length > 0 Then
                                    Dim objUnit As New clsUnits
                                    objUnit._Code = mdsPayroll.Tables(0).Rows(i)("unit").ToString
                                    objUnit._Name = mdsPayroll.Tables(0).Rows(i)("unit").ToString
                                    objUnit._Isactive = True
                                    objUnit._Sectionunkid = -1
                                    With objUnit
                                        ._FormName = mstrModuleName
                                        ._LoginEmployeeunkid = 0
                                        ._ClientIP = getIP()
                                        ._HostName = getHostName()
                                        ._FromWeb = False
                                        ._AuditUserId = User._Object._Userunkid
._CompanyUnkid = Company._Object._Companyunkid
                                        ._AuditDate = ConfigParameter._Object._CurrentDateAndTime
                                    End With
                                    objUnit.Insert()
                                    intUnitunkid = objUnit._Unitunkid

                                    If objUnit._Message <> "" Then
                                        'Sandeep [ 25 APRIL 2011 ] -- Start
                                        'Issue : MESSAGE BOX STYLE
                                        'DisplayError.Show("-1", objUnit._Message, "ImportData", mstrModuleName)
                                        eZeeMsgBox.Show(objUnit._Code & " " & objUnit._Name & " : " & objUnit._Message, enMsgBoxStyle.Information)
                                        'Sandeep [ 25 APRIL 2011 ] -- End 
                                        Return False
                                    End If

                                End If
                            End If
                        End If

                        If mdsPayroll.Tables(0).Columns.Contains("grade") Then
                            intGradeunkid = objImportExport.GetGradeUnkid(mdsPayroll.Tables(0).Rows(i)("grade").ToString)
                            If intGradeunkid <= 0 Then
                                If mdsPayroll.Tables(0).Rows(i)("grade").ToString.Length > 0 Then
                                    Dim objGrade As New clsGrade
                                    objGrade._Code = mdsPayroll.Tables(0).Rows(i)("grade").ToString
                                    objGrade._Name = mdsPayroll.Tables(0).Rows(i)("grade").ToString
                                    objGrade._Isactive = True
                                    objGrade._Gradegroupunkid = -1

                                    With objGrade
                                        ._FormName = mstrModuleName
                                        ._LoginEmployeeunkid = 0
                                        ._ClientIP = getIP()
                                        ._HostName = getHostName()
                                        ._FromWeb = False
                                        ._AuditUserId = User._Object._Userunkid
._CompanyUnkid = Company._Object._Companyunkid
                                        ._AuditDate = ConfigParameter._Object._CurrentDateAndTime
                                    End With

                                    objGrade.Insert()
                                    intGradeunkid = objGrade._Gradeunkid

                                    If objGrade._Message <> "" Then
                                        'Sandeep [ 25 APRIL 2011 ] -- Start
                                        'Issue : MESSAGE BOX STYLE
                                        'DisplayError.Show("-1", objGrade._Message, "ImportData", mstrModuleName)
                                        eZeeMsgBox.Show(objGrade._Code & " " & objGrade._Name & " : " & objGrade._Message, enMsgBoxStyle.Information)
                                        'Sandeep [ 25 APRIL 2011 ] -- End 
                                        Return False
                                    End If

                                End If
                            End If
                        End If

                        If mdsPayroll.Tables(0).Columns.Contains("Section") Then
                            intSectionunkId = objImportExport.GetSectionUnkid(mdsPayroll.Tables(0).Rows(i)("Section").ToString)
                            If intSectionunkId <= 0 Then
                                If mdsPayroll.Tables(0).Rows(i)("Section").ToString.Length > 0 Then
                                    Dim objSection As New clsSections
                                    objSection._Code = mdsPayroll.Tables(0).Rows(i)("Section").ToString
                                    objSection._Name = mdsPayroll.Tables(0).Rows(i)("Section").ToString
                                    objSection._Isactive = True
                                    objSection._Departmentunkid = -1
                                    With objSection
                                        ._FormName = mstrModuleName
                                        ._LoginEmployeeunkid = 0
                                        ._ClientIP = getIP()
                                        ._HostName = getHostName()
                                        ._FromWeb = False
                                        ._AuditUserId = User._Object._Userunkid
._CompanyUnkid = Company._Object._Companyunkid
                                        ._AuditDate = ConfigParameter._Object._CurrentDateAndTime
                                    End With
                                    objSection.Insert()
                                    intSectionunkId = objSection._Sectionunkid

                                    If objSection._Message <> "" Then
                                        'Sandeep [ 25 APRIL 2011 ] -- Start
                                        'Issue : MESSAGE BOX STYLE
                                        'DisplayError.Show("-1", objSection._Message, "ImportData", mstrModuleName)
                                        eZeeMsgBox.Show(objSection._Code & " " & objSection._Name & " : " & objSection._Message, enMsgBoxStyle.Information)
                                        'Sandeep [ 25 APRIL 2011 ] -- End 
                                        Return False
                                    End If

                                End If
                            End If
                        End If

                        'S.SANDEEP [20 JUN 2016] -- START
                        If mdsPayroll.Tables(0).Columns.Contains("Branch") Then
                            intBranchUnkid = objImportExport.GetStationUnkid(mdsPayroll.Tables(0).Rows(i)("Branch").ToString)
                            If intBranchUnkid <= 0 Then
                                If mdsPayroll.Tables(0).Rows(i)("Branch").ToString.Length > 0 Then
                                    Dim objBranch As New clsStation
                                    objBranch._Code = mdsPayroll.Tables(0).Rows(i)("Branch").ToString
                                    objBranch._Name = mdsPayroll.Tables(0).Rows(i)("Branch").ToString
                                    objBranch._Isactive = True
                                    With objBranch
                                        ._FormName = mstrModuleName
                                        ._LoginEmployeeunkid = 0
                                        ._ClientIP = getIP()
                                        ._HostName = getHostName()
                                        ._FromWeb = False
                                        ._AuditUserId = User._Object._Userunkid
._CompanyUnkid = Company._Object._Companyunkid
                                        ._AuditDate = ConfigParameter._Object._CurrentDateAndTime
                                    End With
                                    objBranch.Insert()
                                    intBranchUnkid = objBranch._Stationunkid

                                    If objBranch._Message <> "" Then
                                        eZeeMsgBox.Show(objBranch._Code & " " & objBranch._Name & " : " & objBranch._Message, enMsgBoxStyle.Information)
                                        Return False
                                    End If

                                End If
                            End If
                        End If

                        If mdsPayroll.Tables(0).Columns.Contains("DepartmentGroup") Then
                            intDepartmentGroupUnkid = objImportExport.GetDepartmentGroupUnkid(mdsPayroll.Tables(0).Rows(i)("DepartmentGroup").ToString)
                            If intDepartmentGroupUnkid <= 0 Then
                                If mdsPayroll.Tables(0).Rows(i)("DepartmentGroup").ToString.Length > 0 Then
                                    Dim objDeptGrp As New clsDepartmentGroup
                                    objDeptGrp._Code = mdsPayroll.Tables(0).Rows(i)("DepartmentGroup").ToString
                                    objDeptGrp._Name = mdsPayroll.Tables(0).Rows(i)("DepartmentGroup").ToString
                                    objDeptGrp._Isactive = True
                                    With objDeptGrp
                                        ._FormName = mstrModuleName
                                        ._LoginEmployeeunkid = 0
                                        ._ClientIP = getIP()
                                        ._HostName = getHostName()
                                        ._FromWeb = False
                                        ._AuditUserId = User._Object._Userunkid
._CompanyUnkid = Company._Object._Companyunkid
                                        ._AuditDate = ConfigParameter._Object._CurrentDateAndTime
                                    End With
                                    objDeptGrp.Insert()
                                    intDepartmentGroupUnkid = objDeptGrp._Deptgroupunkid

                                    If objDeptGrp._Message <> "" Then
                                        eZeeMsgBox.Show(objDeptGrp._Code & " " & objDeptGrp._Name & " : " & objDeptGrp._Message, enMsgBoxStyle.Information)
                                        Return False
                                    End If

                                End If
                            End If
                        End If

                        If mdsPayroll.Tables(0).Columns.Contains("Department") Then
                            intDepartmentUnkid = objImportExport.GetDepartmentUnkid(mdsPayroll.Tables(0).Rows(i)("Department").ToString)
                            If intDepartmentUnkid <= 0 Then
                                If mdsPayroll.Tables(0).Rows(i)("Department").ToString.Length > 0 Then
                                    Dim objDept As New clsDepartment
                                    objDept._Code = mdsPayroll.Tables(0).Rows(i)("Department").ToString
                                    objDept._Name = mdsPayroll.Tables(0).Rows(i)("Department").ToString
                                    objDept._Isactive = True
                                    With objDept
                                        ._FormName = mstrModuleName
                                        ._LoginEmployeeunkid = 0
                                        ._ClientIP = getIP()
                                        ._HostName = getHostName()
                                        ._FromWeb = False
                                        ._AuditUserId = User._Object._Userunkid
._CompanyUnkid = Company._Object._Companyunkid
                                        ._AuditDate = ConfigParameter._Object._CurrentDateAndTime
                                    End With
                                    objDept.Insert()
                                    intDepartmentUnkid = objDept._Departmentunkid

                                    If objDept._Message <> "" Then
                                        eZeeMsgBox.Show(objDept._Code & " " & objDept._Name & " : " & objDept._Message, enMsgBoxStyle.Information)
                                        Return False
                                    End If

                                End If
                            End If
                        End If

                        If mdsPayroll.Tables(0).Columns.Contains("SectionGroup") Then
                            intSectionGroupUnkid = objImportExport.GetSectionGroupUnkid(mdsPayroll.Tables(0).Rows(i)("SectionGroup").ToString)
                            If intSectionGroupUnkid <= 0 Then
                                If mdsPayroll.Tables(0).Rows(i)("SectionGroup").ToString.Length > 0 Then
                                    Dim objSecGrp As New clsSectionGroup
                                    objSecGrp._Code = mdsPayroll.Tables(0).Rows(i)("SectionGroup").ToString
                                    objSecGrp._Name = mdsPayroll.Tables(0).Rows(i)("SectionGroup").ToString
                                    objSecGrp._Isactive = True

                                    With objSecGrp
                                        ._FormName = mstrModuleName
                                        ._LoginEmployeeunkid = 0
                                        ._ClientIP = getIP()
                                        ._HostName = getHostName()
                                        ._FromWeb = False
                                        ._AuditUserId = User._Object._Userunkid
._CompanyUnkid = Company._Object._Companyunkid
                                        ._AuditDate = ConfigParameter._Object._CurrentDateAndTime
                                    End With

                                    objSecGrp.Insert()
                                    intSectionGroupUnkid = objSecGrp._Sectiongroupunkid

                                    If objSecGrp._Message <> "" Then
                                        eZeeMsgBox.Show(objSecGrp._Code & " " & objSecGrp._Name & " : " & objSecGrp._Message, enMsgBoxStyle.Information)
                                        Return False
                                    End If

                                End If
                            End If
                        End If

                        If mdsPayroll.Tables(0).Columns.Contains("UnitGroup") Then
                            intUnitGroupUnkid = objImportExport.GetUnitGroupUnkid(mdsPayroll.Tables(0).Rows(i)("UnitGroup").ToString)
                            If intUnitGroupUnkid <= 0 Then
                                If mdsPayroll.Tables(0).Rows(i)("UnitGroup").ToString.Length > 0 Then
                                    Dim objUnitGrp As New clsUnitGroup
                                    objUnitGrp._Code = mdsPayroll.Tables(0).Rows(i)("UnitGroup").ToString
                                    objUnitGrp._Name = mdsPayroll.Tables(0).Rows(i)("UnitGroup").ToString
                                    objUnitGrp._Isactive = True
                                    With objUnitGrp
                                        ._FormName = mstrModuleName
                                        ._LoginEmployeeunkid = 0
                                        ._ClientIP = getIP()
                                        ._HostName = getHostName()
                                        ._FromWeb = False
                                        ._AuditUserId = User._Object._Userunkid
._CompanyUnkid = Company._Object._Companyunkid
                                        ._AuditDate = ConfigParameter._Object._CurrentDateAndTime
                                    End With
                                    objUnitGrp.Insert()
                                    intSectionGroupUnkid = objUnitGrp._Unitgroupunkid

                                    If objUnitGrp._Message <> "" Then
                                        eZeeMsgBox.Show(objUnitGrp._Code & " " & objUnitGrp._Name & " : " & objUnitGrp._Message, enMsgBoxStyle.Information)
                                        Return False
                                    End If

                                End If
                            End If
                        End If

                        If mdsPayroll.Tables(0).Columns.Contains("Team") Then
                            intTeamUnkid = objImportExport.GetTeamUnkid(mdsPayroll.Tables(0).Rows(i)("Team").ToString)
                            If intTeamUnkid <= 0 Then
                                If mdsPayroll.Tables(0).Rows(i)("Team").ToString.Length > 0 Then
                                    Dim objTeam As New clsTeams
                                    objTeam._Code = mdsPayroll.Tables(0).Rows(i)("Team").ToString
                                    objTeam._Name = mdsPayroll.Tables(0).Rows(i)("Team").ToString
                                    objTeam._Isactive = True
                                    With objTeam
                                        ._FormName = mstrModuleName
                                        ._LoginEmployeeunkid = 0
                                        ._ClientIP = getIP()
                                        ._HostName = getHostName()
                                        ._FromWeb = False
                                        ._AuditUserId = User._Object._Userunkid
._CompanyUnkid = Company._Object._Companyunkid
                                        ._AuditDate = ConfigParameter._Object._CurrentDateAndTime
                                    End With
                                    objTeam.Insert()
                                    intTeamUnkid = objTeam._Teamunkid

                                    If objTeam._Message <> "" Then
                                        eZeeMsgBox.Show(objTeam._Code & " " & objTeam._Name & " : " & objTeam._Message, enMsgBoxStyle.Information)
                                        Return False
                                    End If

                                End If
                            End If
                        End If

                        If mdsPayroll.Tables(0).Columns.Contains("ClassGroup") Then
                            intClassGroupUnkid = objImportExport.GetClassGroupUnkid(mdsPayroll.Tables(0).Rows(i)("ClassGroup").ToString)
                            If intClassGroupUnkid <= 0 Then
                                If mdsPayroll.Tables(0).Rows(i)("ClassGroup").ToString.Length > 0 Then
                                    Dim objClassGrp As New clsClassGroup
                                    objClassGrp._Code = mdsPayroll.Tables(0).Rows(i)("ClassGroup").ToString
                                    objClassGrp._Name = mdsPayroll.Tables(0).Rows(i)("ClassGroup").ToString
                                    objClassGrp._Isactive = True

                                    With objClassGrp
                                        ._FormName = mstrModuleName
                                        ._LoginEmployeeunkid = 0
                                        ._ClientIP = getIP()
                                        ._HostName = getHostName()
                                        ._FromWeb = False
                                        ._AuditUserId = User._Object._Userunkid
._CompanyUnkid = Company._Object._Companyunkid
                                        ._AuditDate = ConfigParameter._Object._CurrentDateAndTime
                                    End With

                                    objClassGrp.Insert()
                                    intClassGroupUnkid = objClassGrp._Classgroupunkid

                                    If objClassGrp._Message <> "" Then
                                        eZeeMsgBox.Show(objClassGrp._Code & " " & objClassGrp._Name & " : " & objClassGrp._Message, enMsgBoxStyle.Information)
                                        Return False
                                    End If

                                End If
                            End If
                        End If

                        If mdsPayroll.Tables(0).Columns.Contains("Class") Then
                            intClassUnkid = objImportExport.GetClassUnkid(mdsPayroll.Tables(0).Rows(i)("Class").ToString)
                            If intClassUnkid <= 0 Then
                                If mdsPayroll.Tables(0).Rows(i)("Class").ToString.Length > 0 Then
                                    Dim objClass As New clsClass
                                    objClass._Code = mdsPayroll.Tables(0).Rows(i)("Class").ToString
                                    objClass._Name = mdsPayroll.Tables(0).Rows(i)("Class").ToString
                                    objClass._Isactive = True
                                    With objClass
                                        ._FormName = mstrModuleName
                                        ._LoginEmployeeunkid = 0
                                        ._ClientIP = getIP()
                                        ._HostName = getHostName()
                                        ._FromWeb = False
                                        ._AuditUserId = User._Object._Userunkid
._CompanyUnkid = Company._Object._Companyunkid
                                        ._AuditDate = ConfigParameter._Object._CurrentDateAndTime
                                    End With
                                    objClass.Insert()
                                    intClassUnkid = objClass._Classesunkid

                                    If objClass._Message <> "" Then
                                        eZeeMsgBox.Show(objClass._Code & " " & objClass._Name & " : " & objClass._Message, enMsgBoxStyle.Information)
                                        Return False
                                    End If

                                End If
                            End If
                        End If
                        'S.SANDEEP [20 JUN 2016] -- END

                        Dim objJob As New clsJobs

                        objJob._Jobgroupunkid = intJobGrpunkid
                        'objJob._Jobheadunkid = CInt(mdsPayroll.Tables(0).Rows(i)("jobheadunkid").ToString)
                        objJob._Jobgradeunkid = intGradeunkid
                        objJob._Jobsectionunkid = intSectionunkId
                        objJob._Jobunitunkid = intUnitunkid

                        'S.SANDEEP [20 JUN 2016] -- START
                        objJob._JobBranchUnkid = intBranchUnkid
                        objJob._DepartmentGrpUnkId = intDepartmentGroupUnkid
                        objJob._JobDepartmentunkid = intDepartmentUnkid
                        objJob._Teamunkid = intTeamUnkid
                        objJob._SectionGrpUnkId = intSectionGroupUnkid
                        objJob._UnitGrpUnkId = intUnitGroupUnkid
                        objJob._JobClassGroupunkid = intClassGroupUnkid
                        objJob._ClassUnkid = intClassUnkid
                        objJob._GradeLevelUnkId = 0
                        'S.SANDEEP [20 JUN 2016] -- END

                        objJob._Job_Code = mdsPayroll.Tables(0).Rows(i)(arColumn(0)).ToString
                        objJob._Job_Name = mdsPayroll.Tables(0).Rows(i)(arColumn(1)).ToString

                        If mdsPayroll.Tables(0).Columns.Contains("job_level") Then
                            If mdsPayroll.Tables(0).Rows(i)("job_level").ToString <> "" Then
                                objJob._Job_Level = CInt(mdsPayroll.Tables(0).Rows(i)("job_level").ToString)
                            End If
                        End If

                        If mdsPayroll.Tables(0).Columns.Contains("createdate") Then
                            If mdsPayroll.Tables(0).Rows(i)("createdate").ToString.Trim <> "" Then
                                objJob._Create_Date = eZeeDate.convertDate(mdsPayroll.Tables(0).Rows(i)("createdate").ToString)
                            End If
                        End If

                        If mdsPayroll.Tables(0).Columns.Contains("terminatedate") Then
                            If mdsPayroll.Tables(0).Rows(i)("terminatedate").ToString.Trim <> "" Then
                                objJob._Terminate_Date = eZeeDate.convertDate(mdsPayroll.Tables(0).Rows(i)("terminatedate").ToString.Trim)
                            End If
                        End If

                        If mdsPayroll.Tables(0).Columns.Contains("reportto") Then
                            If mdsPayroll.Tables(0).Rows(i)("reportto").ToString <> "" Then
                                objJob._Report_Tounkid = objImportExport.GetJobUnkId(mdsPayroll.Tables(0).Rows(i)("reportto").ToString)
                            Else
                                objJob._Report_Tounkid = -1
                            End If
                        End If

                        If mdsPayroll.Tables(0).Columns.Contains("positon") Then
                            objJob._Total_Position = CInt(mdsPayroll.Tables(0).Rows(i)("positon").ToString)
                        End If

                        If mdsPayroll.Tables(0).Columns.Contains("job_name1") Then
                            objJob._Job_Name1 = mdsPayroll.Tables(0).Rows(i)("job_name1").ToString
                        End If

                        If mdsPayroll.Tables(0).Columns.Contains("job_name2") Then
                            objJob._Job_Name2 = mdsPayroll.Tables(0).Rows(i)("job_name2").ToString
                        End If

                        If mdsPayroll.Tables(0).Columns.Contains("isactive") Then
                            objJob._Isactive = CBool(mdsPayroll.Tables(0).Rows(i)("isactive").ToString)
                        Else
                            objJob._Isactive = True
                        End If

'Sohail (18 Feb 2020) -- Start
                        'NMB Enhancement # : Pick employment type for type of job on job master.
                        objJob._Critical = 0
                        objJob._Jobtypeunkid = 0
                        'Sohail (18 Feb 2020) -- End

                        With objJob
                            ._FormName = mstrModuleName
                            ._LoginEmployeeunkid = 0
                            ._ClientIP = getIP()
                            ._HostName = getHostName()
                            ._FromWeb = False
                            ._AuditUserId = User._Object._Userunkid
._CompanyUnkid = Company._Object._Companyunkid
                            ._AuditDate = ConfigParameter._Object._CurrentDateAndTime
                        End With

                        objJob._Userunkid = User._Object._Userunkid
                        blnflag = objJob.Insert()

                        If objJob._Message <> "" Then
                            'Sandeep [ 25 APRIL 2011 ] -- Start
                            'Issue : MESSAGE BOX STYLE
                            'DisplayError.Show("-1", objJob._Message, "ImportData", mstrModuleName)
                            eZeeMsgBox.Show(objJob._Job_Code & "  " & objJob._Job_Name & " : " & objJob._Message, enMsgBoxStyle.Information)
                            'Sandeep [ 25 APRIL 2011 ] -- End 
                            Return False
                        End If

                    ElseIf MasterId = 22 Then  'Class Group Master

                        Dim objClassGrp As New clsClassGroup
                        objClassGrp._Code = mdsPayroll.Tables(0).Rows(i)(arColumn(0)).ToString
                        objClassGrp._Name = mdsPayroll.Tables(0).Rows(i)(arColumn(1)).ToString

                        If mdsPayroll.Tables(0).Columns.Contains("description") Then
                            objClassGrp._Description = mdsPayroll.Tables(0).Rows(i)("description").ToString
                        End If

                        If mdsPayroll.Tables(0).Columns.Contains("name1") Then
                            objClassGrp._Name1 = mdsPayroll.Tables(0).Rows(i)("name1").ToString
                        End If

                        If mdsPayroll.Tables(0).Columns.Contains("name2") Then
                            objClassGrp._Name2 = mdsPayroll.Tables(0).Rows(i)("name2").ToString
                        End If

                        If mdsPayroll.Tables(0).Columns.Contains("isactive") Then
                            objClassGrp._Isactive = CBool(mdsPayroll.Tables(0).Rows(i)("isactive").ToString)
                        Else
                            objClassGrp._Isactive = True
                        End If

                        With objClassGrp
                            ._FormName = mstrModuleName
                            ._LoginEmployeeunkid = 0
                            ._ClientIP = getIP()
                            ._HostName = getHostName()
                            ._FromWeb = False
                            ._AuditUserId = User._Object._Userunkid
._CompanyUnkid = Company._Object._Companyunkid
                            ._AuditDate = ConfigParameter._Object._CurrentDateAndTime
                        End With

                        blnflag = objClassGrp.Insert()

                        If objClassGrp._Message <> "" Then
                            'Sandeep [ 25 APRIL 2011 ] -- Start
                            'Issue : MESSAGE BOX STYLE
                            'DisplayError.Show("-1", objClassGrp._Message, "ImportData", mstrModuleName)
                            eZeeMsgBox.Show(objClassGrp._Code & " " & objClassGrp._Name & " : " & objClassGrp._Message, enMsgBoxStyle.Information)
                            'Sandeep [ 25 APRIL 2011 ] -- End 
                            Return False
                        End If


                    ElseIf MasterId = 23 Then  'Class Master
                        Dim intClassGrpunkid As Integer = -1
                        intClassGrpunkid = objImportExport.GetClassGroupUnkid(mdsPayroll.Tables(0).Rows(i)(arColumn(2)).ToString)
                        If intClassGrpunkid <= 0 Then
                            If mdsPayroll.Tables(0).Rows(i)(arColumn(2)).ToString.Length > 0 Then
                                Dim objClassgrp As New clsClassGroup
                                objClassgrp._Code = mdsPayroll.Tables(0).Rows(i)(arColumn(2)).ToString
                                objClassgrp._Name = mdsPayroll.Tables(0).Rows(i)(arColumn(2)).ToString
                                objClassgrp._Isactive = True

                                With objClassgrp
                                    ._FormName = mstrModuleName
                                    ._LoginEmployeeunkid = 0
                                    ._ClientIP = getIP()
                                    ._HostName = getHostName()
                                    ._FromWeb = False
                                    ._AuditUserId = User._Object._Userunkid
._CompanyUnkid = Company._Object._Companyunkid
                                    ._AuditDate = ConfigParameter._Object._CurrentDateAndTime
                                End With

                                objClassgrp.Insert()
                                intClassGrpunkid = objClassgrp._Classgroupunkid

                                If objClassgrp._Message <> "" Then
                                    'Sandeep [ 25 APRIL 2011 ] -- Start
                                    'Issue : MESSAGE BOX STYLE
                                    'DisplayError.Show("-1", objClassgrp._Message, "ImportData", mstrModuleName)
                                    eZeeMsgBox.Show(objClassgrp._Code & " " & objClassgrp._Name & " : " & objClassgrp._Message, enMsgBoxStyle.Information)
                                    'Sandeep [ 25 APRIL 2011 ] -- End 
                                    Return False
                                End If

                            End If
                        End If

                        Dim objClass As New clsClass
                        objClass._Classgroupunkid = intClassGrpunkid
                        objClass._Code = mdsPayroll.Tables(0).Rows(i)(arColumn(0)).ToString
                        objClass._Name = mdsPayroll.Tables(0).Rows(i)(arColumn(1)).ToString
                        With objClass
                            ._FormName = mstrModuleName
                            ._LoginEmployeeunkid = 0
                            ._ClientIP = getIP()
                            ._HostName = getHostName()
                            ._FromWeb = False
                            ._AuditUserId = User._Object._Userunkid
._CompanyUnkid = Company._Object._Companyunkid
                            ._AuditDate = ConfigParameter._Object._CurrentDateAndTime
                        End With
                        If mdsPayroll.Tables(0).Columns.Contains("description") Then
                            objClass._Description = mdsPayroll.Tables(0).Rows(i)("description").ToString
                        End If

                        If mdsPayroll.Tables(0).Columns.Contains("name1") Then
                            objClass._Name1 = mdsPayroll.Tables(0).Rows(i)("name1").ToString
                        End If

                        If mdsPayroll.Tables(0).Columns.Contains("name2") Then
                            objClass._Name2 = mdsPayroll.Tables(0).Rows(i)("name2").ToString
                        End If

                        If mdsPayroll.Tables(0).Columns.Contains("isactive") Then
                            objClass._Isactive = CBool(mdsPayroll.Tables(0).Rows(i)("isactive").ToString)
                        Else
                            objClass._Isactive = True
                        End If

                        blnflag = objClass.Insert()

                        If objClass._Message <> "" Then
                            'Sandeep [ 25 APRIL 2011 ] -- Start
                            'Issue : MESSAGE BOX STYLE
                            'DisplayError.Show("-1", objClass._Message, "ImportData", mstrModuleName)
                            eZeeMsgBox.Show(objClass._Code & " " & objClass._Name & " : " & objClass._Message, enMsgBoxStyle.Information)
                            'Sandeep [ 25 APRIL 2011 ] -- End 
                            Return False
                        End If


                    ElseIf MasterId = 24 Then  'Grade Group Master

                        Dim objGradeGrp As New clsGradeGroup
                        objGradeGrp._Code = mdsPayroll.Tables(0).Rows(i)(arColumn(0)).ToString
                        objGradeGrp._Name = mdsPayroll.Tables(0).Rows(i)(arColumn(1)).ToString

                        If mdsPayroll.Tables(0).Columns.Contains("description") Then
                            objGradeGrp._Description = mdsPayroll.Tables(0).Rows(i)("description").ToString
                        End If

                        If mdsPayroll.Tables(0).Columns.Contains("name1") Then
                            objGradeGrp._Name1 = mdsPayroll.Tables(0).Rows(i)("name1").ToString
                        End If

                        If mdsPayroll.Tables(0).Columns.Contains("name2") Then
                            objGradeGrp._Name2 = mdsPayroll.Tables(0).Rows(i)("name2").ToString
                        End If

                        If mdsPayroll.Tables(0).Columns.Contains("isactive") Then
                            objGradeGrp._Isactive = CBool(mdsPayroll.Tables(0).Rows(i)("isactive").ToString)
                        Else
                            objGradeGrp._Isactive = True
                        End If

                        With objGradeGrp
                            ._FormName = mstrModuleName
                            ._LoginEmployeeunkid = 0
                            ._ClientIP = getIP()
                            ._HostName = getHostName()
                            ._FromWeb = False
                            ._AuditUserId = User._Object._Userunkid
._CompanyUnkid = Company._Object._Companyunkid
                            ._AuditDate = ConfigParameter._Object._CurrentDateAndTime
                        End With

                        blnflag = objGradeGrp.Insert()

                        If objGradeGrp._Message <> "" Then
                            'Sandeep [ 25 APRIL 2011 ] -- Start
                            'Issue : MESSAGE BOX STYLE
                            'DisplayError.Show("-1", objGradeGrp._Message, "ImportData", mstrModuleName)
                            eZeeMsgBox.Show(objGradeGrp._Code & " " & objGradeGrp._Name & " : " & objGradeGrp._Message, enMsgBoxStyle.Information)
                            'Sandeep [ 25 APRIL 2011 ] -- End 
                            Return False
                        End If


                    ElseIf MasterId = 25 Then  'Grade Master

                        Dim intGradeGrpunkid As Integer = -1
                        intGradeGrpunkid = objImportExport.GetGradeGroupUnkid(mdsPayroll.Tables(0).Rows(i)(arColumn(2)).ToString)
                        If intGradeGrpunkid <= 0 Then
                            If mdsPayroll.Tables(0).Rows(i)(arColumn(2)).ToString.Length > 0 Then
                                Dim objGradegrp As New clsGradeGroup
                                objGradegrp._Code = mdsPayroll.Tables(0).Rows(i)(arColumn(2)).ToString
                                objGradegrp._Name = mdsPayroll.Tables(0).Rows(i)(arColumn(2)).ToString
                                objGradegrp._Isactive = True

                                With objGradegrp
                                    ._FormName = mstrModuleName
                                    ._LoginEmployeeunkid = 0
                                    ._ClientIP = getIP()
                                    ._HostName = getHostName()
                                    ._FromWeb = False
                                    ._AuditUserId = User._Object._Userunkid
._CompanyUnkid = Company._Object._Companyunkid
                                    ._AuditDate = ConfigParameter._Object._CurrentDateAndTime
                                End With

                                objGradegrp.Insert()
                                intGradeGrpunkid = objGradegrp._Gradegroupunkid

                                If objGradegrp._Message <> "" Then
                                    'Sandeep [ 25 APRIL 2011 ] -- Start
                                    'Issue : MESSAGE BOX STYLE
                                    'DisplayError.Show("-1", objGradegrp._Message, "ImportData", mstrModuleName)
                                    eZeeMsgBox.Show(objGradegrp._Code & " " & objGradegrp._Name & " : " & objGradegrp._Message, enMsgBoxStyle.Information)
                                    'Sandeep [ 25 APRIL 2011 ] -- End 
                                    Return False
                                End If

                            End If
                        End If

                        Dim objGrade As New clsGrade
                        objGrade._Gradegroupunkid = intGradeGrpunkid
                        objGrade._Code = mdsPayroll.Tables(0).Rows(i)(arColumn(0)).ToString
                        objGrade._Name = mdsPayroll.Tables(0).Rows(i)(arColumn(1)).ToString

                        If mdsPayroll.Tables(0).Columns.Contains("description") Then
                            objGrade._Description = mdsPayroll.Tables(0).Rows(i)("description").ToString
                        End If

                        If mdsPayroll.Tables(0).Columns.Contains("name1") Then
                            objGrade._Name1 = mdsPayroll.Tables(0).Rows(i)("name1").ToString
                        End If

                        If mdsPayroll.Tables(0).Columns.Contains("name2") Then
                            objGrade._Name2 = mdsPayroll.Tables(0).Rows(i)("name2").ToString
                        End If

                        If mdsPayroll.Tables(0).Columns.Contains("isactive") Then
                            objGrade._Isactive = CBool(mdsPayroll.Tables(0).Rows(i)("isactive").ToString)
                        Else
                            objGrade._Isactive = True
                        End If

                        With objGrade
                            ._FormName = mstrModuleName
                            ._LoginEmployeeunkid = 0
                            ._ClientIP = getIP()
                            ._HostName = getHostName()
                            ._FromWeb = False
                            ._AuditUserId = User._Object._Userunkid
._CompanyUnkid = Company._Object._Companyunkid
                            ._AuditDate = ConfigParameter._Object._CurrentDateAndTime
                        End With

                        blnflag = objGrade.Insert()

                        If objGrade._Message <> "" Then
                            'Sandeep [ 25 APRIL 2011 ] -- Start
                            'Issue : MESSAGE BOX STYLE
                            'DisplayError.Show("-1", objGrade._Message, "ImportData", mstrModuleName)
                            eZeeMsgBox.Show(objGrade._Code & " " & objGrade._Name & " : " & objGrade._Message, enMsgBoxStyle.Information)
                            'Sandeep [ 25 APRIL 2011 ] -- End 
                            Return False
                        End If


                    ElseIf MasterId = 26 Then  'Grade Level Master

                        Dim intGradeunkid As Integer = -1
                        Dim intGradeGrpunkid As Integer = -1

                        If mdsPayroll.Tables(0).Columns.Contains("GradeGrpName") Then
                            intGradeGrpunkid = objImportExport.GetGradeGroupUnkid(mdsPayroll.Tables(0).Rows(i)("GradeGrpName").ToString)
                            If intGradeGrpunkid <= 0 Then
                                If mdsPayroll.Tables(0).Rows(i)("GradeGrpName").ToString.Length > 0 Then
                                    Dim objGradegrp As New clsGradeGroup
                                    objGradegrp._Code = mdsPayroll.Tables(0).Rows(i)("GradeGrpName").ToString
                                    objGradegrp._Name = mdsPayroll.Tables(0).Rows(i)("GradeGrpName").ToString
                                    objGradegrp._Isactive = True

                                    With objGradegrp
                                        ._FormName = mstrModuleName
                                        ._LoginEmployeeunkid = 0
                                        ._ClientIP = getIP()
                                        ._HostName = getHostName()
                                        ._FromWeb = False
                                        ._AuditUserId = User._Object._Userunkid
._CompanyUnkid = Company._Object._Companyunkid
                                        ._AuditDate = ConfigParameter._Object._CurrentDateAndTime
                                    End With

                                    objGradegrp.Insert()
                                    intGradeGrpunkid = objGradegrp._Gradegroupunkid

                                    If objGradegrp._Message <> "" Then
                                        'Sandeep [ 25 APRIL 2011 ] -- Start
                                        'Issue : MESSAGE BOX STYLE
                                        'DisplayError.Show("-1", objGradegrp._Message, "ImportData", mstrModuleName)
                                        eZeeMsgBox.Show(objGradegrp._Code & " " & objGradegrp._Name & " : " & objGradegrp._Message, enMsgBoxStyle.Information)
                                        'Sandeep [ 25 APRIL 2011 ] -- End 
                                        Return False
                                    End If

                                End If
                            End If
                        End If


                        intGradeunkid = objImportExport.GetGradeUnkid(mdsPayroll.Tables(0).Rows(i)(arColumn(2)).ToString)
                        If intGradeunkid <= 0 Then
                            If mdsPayroll.Tables(0).Rows(i)(arColumn(2)).ToString.Length > 0 Then
                                Dim objGrade As New clsGrade
                                objGrade._Code = mdsPayroll.Tables(0).Rows(i)(arColumn(2)).ToString
                                objGrade._Name = mdsPayroll.Tables(0).Rows(i)(arColumn(2)).ToString
                                objGrade._Isactive = True
                                objGrade._Gradegroupunkid = intGradeGrpunkid
                                With objGrade
                                    ._FormName = mstrModuleName
                                    ._LoginEmployeeunkid = 0
                                    ._ClientIP = getIP()
                                    ._HostName = getHostName()
                                    ._FromWeb = False
                                    ._AuditUserId = User._Object._Userunkid
._CompanyUnkid = Company._Object._Companyunkid
                                    ._AuditDate = ConfigParameter._Object._CurrentDateAndTime
                                End With
                                objGrade.Insert()
                                intGradeunkid = objGrade._Gradeunkid

                                If objGrade._Message <> "" Then
                                    'Sandeep [ 25 APRIL 2011 ] -- Start
                                    'Issue : MESSAGE BOX STYLE
                                    'DisplayError.Show("-1", objGrade._Message, "ImportData", mstrModuleName)
                                    eZeeMsgBox.Show(objGrade._Code & " " & objGrade._Name & " : " & objGrade._Message, enMsgBoxStyle.Information)
                                    'Sandeep [ 25 APRIL 2011 ] -- End 
                                    Return False
                                End If

                            End If
                        End If

                        Dim objGradelvl As New clsGradeLevel
                        objGradelvl._Gradeunkid = intGradeunkid
                        objGradelvl._Code = mdsPayroll.Tables(0).Rows(i)(arColumn(0)).ToString
                        objGradelvl._Name = mdsPayroll.Tables(0).Rows(i)(arColumn(1)).ToString

                        If mdsPayroll.Tables(0).Columns.Contains("description") Then
                            objGradelvl._Description = mdsPayroll.Tables(0).Rows(i)("description").ToString
                        End If

                        If mdsPayroll.Tables(0).Columns.Contains("name1") Then
                            objGradelvl._Name1 = mdsPayroll.Tables(0).Rows(i)("name1").ToString
                        End If

                        If mdsPayroll.Tables(0).Columns.Contains("name2") Then
                            objGradelvl._Name2 = mdsPayroll.Tables(0).Rows(i)("name2").ToString
                        End If

                        If mdsPayroll.Tables(0).Columns.Contains("name2") Then
                            objGradelvl._Isactive = CBool(mdsPayroll.Tables(0).Rows(i)("isactive").ToString)
                        Else
                            objGradelvl._Isactive = True
                        End If

                        With objGradelvl
                            ._FormName = mstrModuleName
                            ._LoginEmployeeunkid = 0
                            ._ClientIP = getIP()
                            ._HostName = getHostName()
                            ._FromWeb = False
                            ._AuditUserId = User._Object._Userunkid
._CompanyUnkid = Company._Object._Companyunkid
                            ._AuditDate = ConfigParameter._Object._CurrentDateAndTime
                        End With

                        blnflag = objGradelvl.Insert()

                        If objGradelvl._Message <> "" Then
                            'Sandeep [ 25 APRIL 2011 ] -- Start
                            'Issue : MESSAGE BOX STYLE
                            'DisplayError.Show("-1", objGradelvl._Message, "ImportData", mstrModuleName)
                            eZeeMsgBox.Show(objGradelvl._Code & " " & objGradelvl._Name & " : " & objGradelvl._Message, enMsgBoxStyle.Information)
                            'Sandeep [ 25 APRIL 2011 ] -- End 
                            Return False
                        End If

                    ElseIf MasterId = 27 Then  'Shift Master

                        'Pinkal (03-Jul-2013) -- Start
                        'Enhancement : TRA Changes

                        Dim intMasterunkid As Integer = -1
                        intMasterunkid = objImportExport.GetCommonMasterUnkId(clsCommon_Master.enCommonMaster.SHIFT_TYPE, mdsPayroll.Tables(0).Rows(i)(arColumn(2)).ToString)

                        If intMasterunkid <= 0 Then
                            If mdsPayroll.Tables(0).Rows(i)(arColumn(2)).ToString.Length > 0 Then
                                Dim objCommon As New clsCommon_Master
                                objCommon._Code = mdsPayroll.Tables(0).Rows(i)(arColumn(2)).ToString
                                objCommon._Mastertype = clsCommon_Master.enCommonMaster.SHIFT_TYPE
                                objCommon._Name = mdsPayroll.Tables(0).Rows(i)(arColumn(2)).ToString
                                objCommon._Isactive = True

                                With objCommon
                                    ._FormName = mstrModuleName
                                    ._LoginEmployeeunkid = 0
                                    ._ClientIP = getIP()
                                    ._HostName = getHostName()
                                    ._FromWeb = False
                                    ._AuditUserId = User._Object._Userunkid
._CompanyUnkid = Company._Object._Companyunkid
                                    ._AuditDate = ConfigParameter._Object._CurrentDateAndTime
                                End With

                                objCommon.Insert()

                                

                                intMasterunkid = objCommon._Masterunkid

                                If objCommon._Message <> "" Then
                                    eZeeMsgBox.Show(objCommon._Code & " " & objCommon._Name & " : " & objCommon._Message, enMsgBoxStyle.Information)
                                    Return False
                                End If
                            End If
                        End If


                        Dim objshift As New clsNewshift_master

                        objshift._Shifttypeunkid = intMasterunkid
                        objshift._Shiftcode = mdsPayroll.Tables(0).Rows(i)(arColumn(0)).ToString
                        objshift._Shiftname = mdsPayroll.Tables(0).Rows(i)(arColumn(1)).ToString

                        If mdsPayroll.Tables(0).Columns.Contains("weekhours") Then
                            objshift._TotalWeekHrs = CInt(mdsPayroll.Tables(0).Rows(i)("weekhours"))
                        Else
                            objshift._TotalWeekHrs = 0
                        End If


                        'Pinkal (04-Dec-2019) -- Start
                        'Enhancement NMB - Working On Close Year Enhancement for NMB.

                        If mdsPayroll.Tables(0).Columns.Contains("isopenshift") Then
                            objshift._IsOpenShift = CBool(mdsPayroll.Tables(0).Rows(i)("isopenshift"))
                        Else
                            objshift._IsOpenShift = False
                        End If

                        If mdsPayroll.Tables(0).Columns.Contains("maxshifthrs") Then
                            objshift._MaxHrs = CInt(mdsPayroll.Tables(0).Rows(i)("maxshifthrs"))
                        Else
                            objshift._MaxHrs = 0
                        End If

                        If mdsPayroll.Tables(0).Columns.Contains("attondeviceiostatus") Then
                            objshift._AttOnDeviceInOutStatus = CBool(mdsPayroll.Tables(0).Rows(i)("attondeviceiostatus"))
                        Else
                            objshift._AttOnDeviceInOutStatus = False
                        End If

                        If mdsPayroll.Tables(0).Columns.Contains("isnormalhrs_forwkend") Then
                            objshift._IsNormalHrsForWeekend = CBool(mdsPayroll.Tables(0).Rows(i)("isnormalhrs_forwkend"))
                        Else
                            objshift._IsNormalHrsForWeekend = False
                        End If

                        If mdsPayroll.Tables(0).Columns.Contains("isnormalhrs_forph") Then
                            objshift._IsNormalHrsForHoliday = CBool(mdsPayroll.Tables(0).Rows(i)("isnormalhrs_forph"))
                        Else
                            objshift._IsNormalHrsForHoliday = False
                        End If

                        If mdsPayroll.Tables(0).Columns.Contains("isnormalhrs_fordayoff") Then
                            objshift._IsNormalHrsForDayOff = CBool(mdsPayroll.Tables(0).Rows(i)("isnormalhrs_fordayoff"))
                        Else
                            objshift._IsNormalHrsForDayOff = False
                        End If

                        If mdsPayroll.Tables(0).Columns.Contains("iscountshifttimeonly") Then
                            objshift._CountOnlyShiftTime = CBool(mdsPayroll.Tables(0).Rows(i)("iscountshifttimeonly"))
                        Else
                            objshift._CountOnlyShiftTime = False
                        End If

                        If mdsPayroll.Tables(0).Columns.Contains("iscnt_ot_ontotalworkhrs") Then
                            objshift._CountOTonTotalWorkedHrs = CBool(mdsPayroll.Tables(0).Rows(i)("iscnt_ot_ontotalworkhrs"))
                        Else
                            objshift._CountOTonTotalWorkedHrs = False
                        End If

                        'Pinkal (04-Dec-2019) -- End


                        objshift._Isactive = True

                        If mstrShiftName.Trim <> objshift._Shiftname.Trim Then
                            Dim drRow() As DataRow = mdsPayroll.Tables(0).Select("shiftcode ='" & mdsPayroll.Tables(0).Rows(i)("shiftcode").ToString() & "'")
                            Dim dtTran As DataTable = GenerateShiftDays(drRow)

                            With objshift
                                ._FormName = mstrModuleName
                                ._LoginEmployeeunkid = 0
                                ._ClientIP = getIP()
                                ._HostName = getHostName()
                                ._FromWeb = False
                                ._AuditUserId = User._Object._Userunkid
._CompanyUnkid = Company._Object._Companyunkid
                                ._AuditDate = ConfigParameter._Object._CurrentDateAndTime
                            End With
                            blnflag = objshift.Insert(dtTran)

                            If objshift._Message <> "" Then
                                eZeeMsgBox.Show(objshift._Shiftcode & " " & objshift._Shiftname & " : " & objshift._Message, enMsgBoxStyle.Information)
                                Return False
                            End If
                            mstrShiftName = objshift._Shiftname.Trim

                            'Pinkal (04-Dec-2019) -- Start
                            'Enhancement NMB - Working On Close Year Enhancement for NMB.

                            mdtDays = Nothing
                            'Pinkal (04-Dec-2019) -- End
                        End If

                        'Pinkal (03-Jul-2013) -- End

                    ElseIf MasterId = 28 Then  'Payroll Group Master

                        Dim objPayrollgrp As New clspayrollgroup_master
                        objPayrollgrp._Groupcode = mdsPayroll.Tables(0).Rows(i)(arColumn(0)).ToString
                        objPayrollgrp._Groupalias = mdsPayroll.Tables(0).Rows(i)(arColumn(1)).ToString
                        objPayrollgrp._Groupname = mdsPayroll.Tables(0).Rows(i)(arColumn(2)).ToString

                        If mdsPayroll.Tables(0).Columns.Contains("grouptype") Then
                            If mdsPayroll.Tables(0).Rows(i)("grouptype").ToString.Trim <> "" Then
                                If enPayrollGroupType.CostCenter.ToString.ToUpper = mdsPayroll.Tables(0).Rows(i)("grouptype").ToString.Trim.ToUpper.Replace(" ", "") Then
                                    objPayrollgrp._Grouptype_Id = enPayrollGroupType.CostCenter
                                    'ElseIf enPayrollGroupType.Vendor.ToString.ToUpper = mdsPayroll.Tables(0).Rows(i)("grouptype").ToString.Trim.ToUpper.Replace(" ", "") Then
                                    '   objPayrollgrp._Grouptype_Id = enPayrollGroupType.Vendor
                                ElseIf enPayrollGroupType.Bank.ToString.ToUpper = mdsPayroll.Tables(0).Rows(i)("grouptype").ToString.Trim.ToUpper.Replace(" ", "") Then
                                    objPayrollgrp._Grouptype_Id = enPayrollGroupType.Bank
                                End If
                            End If
                        End If



                        If mdsPayroll.Tables(0).Columns.Contains("description") Then
                            objPayrollgrp._Description = mdsPayroll.Tables(0).Rows(i)("description").ToString
                        End If

                        If mdsPayroll.Tables(0).Columns.Contains("groupname1") Then
                            objPayrollgrp._Groupname1 = mdsPayroll.Tables(0).Rows(i)("groupname1").ToString
                        End If

                        If mdsPayroll.Tables(0).Columns.Contains("groupname2") Then
                            objPayrollgrp._Groupname2 = mdsPayroll.Tables(0).Rows(i)("groupname2").ToString
                        End If

                        If mdsPayroll.Tables(0).Columns.Contains("isactive") Then
                            objPayrollgrp._Isactive = CBool(mdsPayroll.Tables(0).Rows(i)("isactive").ToString)
                        Else
                            objPayrollgrp._Isactive = True
                        End If

                        With objPayrollgrp
                            ._FormName = mstrModuleName
                            ._LoginEmployeeunkid = 0
                            ._ClientIP = getIP()
                            ._HostName = getHostName()
                            ._FromWeb = False
                            ._AuditUserId = User._Object._Userunkid
._CompanyUnkid = Company._Object._Companyunkid
                            ._AuditDate = ConfigParameter._Object._CurrentDateAndTime
                        End With

                        blnflag = objPayrollgrp.Insert()

                        If objPayrollgrp._Message <> "" Then
                            'Sandeep [ 25 APRIL 2011 ] -- Start
                            'Issue : MESSAGE BOX STYLE
                            'DisplayError.Show("-1", objPayrollgrp._Message, "ImportData", mstrModuleName)
                            eZeeMsgBox.Show(objPayrollgrp._Groupcode & " " & objPayrollgrp._Groupname & " : " & objPayrollgrp._Message, enMsgBoxStyle.Information)
                            'Sandeep [ 25 APRIL 2011 ] -- End 
                            Return False
                        End If

                    ElseIf MasterId = 29 Then  'Cost Center Master

                        Dim intCostCenterGroupunkid As Integer = -1

                        If mdsPayroll.Tables(0).Columns.Contains("costcentergroup") Then

                            intCostCenterGroupunkid = objImportExport.GetPayrollGrpUnkId(enPayrollGroupType.CostCenter, mdsPayroll.Tables(0).Rows(i)(arColumn(0)).ToString)
                            If intCostCenterGroupunkid <= 0 Then
                                If mdsPayroll.Tables(0).Rows(i)("costcentergroup").ToString.Length > 0 Then
                                    Dim objPayrollGrp As New clspayrollgroup_master
                                    objPayrollGrp._Groupcode = mdsPayroll.Tables(0).Rows(i)("costcentergroup").ToString
                                    objPayrollGrp._Grouptype_Id = enPayrollGroupType.CostCenter
                                    objPayrollGrp._Groupname = mdsPayroll.Tables(0).Rows(i)("costcentergroup").ToString
                                    objPayrollGrp._Isactive = True
                                    With objPayrollGrp
                                        ._FormName = mstrModuleName
                                        ._LoginEmployeeunkid = 0
                                        ._ClientIP = getIP()
                                        ._HostName = getHostName()
                                        ._FromWeb = False
                                        ._AuditUserId = User._Object._Userunkid
._CompanyUnkid = Company._Object._Companyunkid
                                        ._AuditDate = ConfigParameter._Object._CurrentDateAndTime
                                    End With
                                    objPayrollGrp.Insert()
                                    intCostCenterGroupunkid = objPayrollGrp._Groupmasterunkid

                                    If objPayrollGrp._Message <> "" Then
                                        'Sandeep [ 25 APRIL 2011 ] -- Start
                                        'Issue : MESSAGE BOX STYLE
                                        'DisplayError.Show("-1", objPayrollGrp._Message, "ImportData", mstrModuleName)
                                        eZeeMsgBox.Show(objPayrollGrp._Groupcode & " " & objPayrollGrp._Groupname & " : " & objPayrollGrp._Message, enMsgBoxStyle.Information)
                                        'Sandeep [ 25 APRIL 2011 ] -- End 
                                        Return False
                                    End If

                                End If
                            End If

                        End If

                        Dim objcostcenter As New clscostcenter_master
                        objcostcenter._Costcentergroupmasterunkid = intCostCenterGroupunkid
                        objcostcenter._Costcentercode = mdsPayroll.Tables(0).Rows(i)(arColumn(1)).ToString
                        objcostcenter._Costcentername = mdsPayroll.Tables(0).Rows(i)(arColumn(2)).ToString
                        'S.SANDEEP [09-AUG-2018] -- START
                        'ISSUE/ENHANCEMENT : {Ref#292}
                        objcostcenter._Customcode = mdsPayroll.Tables(0).Rows(i)(arColumn(3)).ToString
                        'S.SANDEEP [09-AUG-2018] -- END
                        If mdsPayroll.Tables(0).Columns.Contains("description") Then
                            objcostcenter._Description = mdsPayroll.Tables(0).Rows(i)("description").ToString
                        End If

                        If mdsPayroll.Tables(0).Columns.Contains("costcentername1") Then
                            objcostcenter._Costcentername1 = mdsPayroll.Tables(0).Rows(i)("costcentername1").ToString
                        End If

                        If mdsPayroll.Tables(0).Columns.Contains("costcentername2") Then
                            objcostcenter._Costcentername2 = mdsPayroll.Tables(0).Rows(i)("costcentername2").ToString
                        End If

                        If mdsPayroll.Tables(0).Columns.Contains("isactive") Then
                            objcostcenter._Isactive = CBool(mdsPayroll.Tables(0).Rows(i)("isactive").ToString)
                        Else
                            objcostcenter._Isactive = True
                        End If
                        With objcostcenter
                            ._FormName = mstrModuleName
                            ._LoginEmployeeunkid = 0
                            ._ClientIP = getIP()
                            ._HostName = getHostName()
                            ._FromWeb = False
                            ._AuditUserId = User._Object._Userunkid
._CompanyUnkid = Company._Object._Companyunkid
                            ._AuditDate = ConfigParameter._Object._CurrentDateAndTime
                        End With
                        blnflag = objcostcenter.Insert()

                        If objcostcenter._Message <> "" Then
                            eZeeMsgBox.Show(objcostcenter._Costcentercode & " " & objcostcenter._Costcentername & " : " & objcostcenter._Message, enMsgBoxStyle.Information)
                            Return False
                        End If

                    ElseIf MasterId = 30 Then  'Pay Point Master

                        Dim objpaypoint As New clspaypoint_master
                        objpaypoint._Paypointcode = mdsPayroll.Tables(0).Rows(i)(arColumn(0)).ToString
                        objpaypoint._Paypointalias = mdsPayroll.Tables(0).Rows(i)(arColumn(1)).ToString
                        objpaypoint._Paypointname = mdsPayroll.Tables(0).Rows(i)(arColumn(2)).ToString

                        If mdsPayroll.Tables(0).Columns.Contains("description") Then
                            objpaypoint._Description = mdsPayroll.Tables(0).Rows(i)("description").ToString
                        End If

                        If mdsPayroll.Tables(0).Columns.Contains("paypointname1") Then
                            objpaypoint._Paypointname1 = mdsPayroll.Tables(0).Rows(i)("paypointname1").ToString
                        End If

                        If mdsPayroll.Tables(0).Columns.Contains("paypointname2") Then
                            objpaypoint._Paypointname2 = mdsPayroll.Tables(0).Rows(i)("paypointname2").ToString
                        End If

                        If mdsPayroll.Tables(0).Columns.Contains("isactive") Then
                            objpaypoint._Isactive = CBool(mdsPayroll.Tables(0).Rows(i)("isactive").ToString)
                        Else
                            objpaypoint._Isactive = True
                        End If
                        With objpaypoint
                            ._FormName = mstrModuleName
                            ._LoginEmployeeunkid = 0
                            ._ClientIP = getIP()
                            ._HostName = getHostName()
                            ._FromWeb = False
                            ._AuditUserId = User._Object._Userunkid
._CompanyUnkid = Company._Object._Companyunkid
                            ._AuditDate = ConfigParameter._Object._CurrentDateAndTime
                        End With
                        blnflag = objpaypoint.Insert()

                        If objpaypoint._Message <> "" Then
                            'Sandeep [ 25 APRIL 2011 ] -- Start
                            'Issue : MESSAGE BOX STYLE
                            'DisplayError.Show("-1", objpaypoint._Message, "ImportData", mstrModuleName)
                            eZeeMsgBox.Show(objpaypoint._Paypointcode & " " & objpaypoint._Paypointname & " : " & objpaypoint._Message, enMsgBoxStyle.Information)
                            'Sandeep [ 25 APRIL 2011 ] -- End 
                            Return False
                        End If

                        'Pinkal (06-May-2011) -- Start

                    ElseIf MasterId = 51 Then  'Leave Type Master

                        Dim objLeaveType As New clsleavetype_master

                        If mdsPayroll.Tables(0).Rows(i)(arColumn(0)).ToString.Contains("*") Then
                            objLeaveType._Leavetypecode = mdsPayroll.Tables(0).Rows(i)(arColumn(0)).ToString.Replace("*", "")
                        Else
                            objLeaveType._Leavetypecode = mdsPayroll.Tables(0).Rows(i)(arColumn(0)).ToString
                        End If

                        If mdsPayroll.Tables(0).Rows(i)(arColumn(1)).ToString.Contains("*") Then
                            objLeaveType._Leavename = mdsPayroll.Tables(0).Rows(i)(arColumn(1)).ToString.Replace("*", "")
                        Else
                            objLeaveType._Leavename = mdsPayroll.Tables(0).Rows(i)(arColumn(1)).ToString
                        End If

                        If mdsPayroll.Tables(0).Columns.Contains("description") Then
                            objLeaveType._Description = mdsPayroll.Tables(0).Rows(i)("description").ToString
                        End If

                        If mdsPayroll.Tables(0).Columns.Contains("isissueonholiday") Then
                            objLeaveType._Isissueonholiday = CBool(mdsPayroll.Tables(0).Rows(i)("isissueonholiday").ToString)
                        End If

                        If mdsPayroll.Tables(0).Columns.Contains("color") Then
                            objLeaveType._Color = CInt(mdsPayroll.Tables(0).Rows(i)("color").ToString)
                        End If

                        If mdsPayroll.Tables(0).Columns.Contains("maxamount") Then
                            objLeaveType._MaxAmount = CInt(mdsPayroll.Tables(0).Rows(i)("maxamount").ToString)
                        End If

                        If mdsPayroll.Tables(0).Columns.Contains("ispaid") Then
                            objLeaveType._IsPaid = CBool(mdsPayroll.Tables(0).Rows(i)("ispaid").ToString)
                        End If

                        If mdsPayroll.Tables(0).Columns.Contains("isaccrueamount") Then
                            objLeaveType._IsAccrueAmount = CBool(mdsPayroll.Tables(0).Rows(i)("isaccrueamount").ToString)
                        End If

                        If mdsPayroll.Tables(0).Columns.Contains("leaveName1") Then
                            If mdsPayroll.Tables(0).Rows(i)("leaveName1").ToString.Contains("*") Then
                                objLeaveType._Leavename1 = mdsPayroll.Tables(0).Rows(i)("leaveName1").ToString.Replace("*", "")
                            Else
                                objLeaveType._Leavename1 = mdsPayroll.Tables(0).Rows(i)("leaveName1").ToString
                            End If
                        End If

                        If mdsPayroll.Tables(0).Columns.Contains("leaveName2") Then
                            If mdsPayroll.Tables(0).Rows(i)("leaveName2").ToString.Contains("*") Then
                                objLeaveType._Leavename2 = mdsPayroll.Tables(0).Rows(i)("leaveName2").ToString.Replace("*", "")
                            Else
                                objLeaveType._Leavename2 = mdsPayroll.Tables(0).Rows(i)("leaveName2").ToString
                            End If
                        End If


                        'Pinkal (18-Aug-2013) -- Start
                        'Enhancement : TRA Changes


                        If mdsPayroll.Tables(0).Columns.Contains("isshortleave") Then
                            objLeaveType._IsShortLeave = CBool(mdsPayroll.Tables(0).Rows(i)("isshortleave").ToString)
                        End If

                        If mdsPayroll.Tables(0).Columns.Contains("cfamount") Then
                            objLeaveType._CFAmount = CDec(mdsPayroll.Tables(0).Rows(i)("cfamount").ToString)
                        End If

                        'If mdsPayroll.Tables(0).Columns.Contains("eligibilityafter") Then
                        '    objLeaveType._EligibilityAfter = CInt(mdsPayroll.Tables(0).Rows(i)("eligibilityafter").ToString)
                        'End If

                        If mdsPayroll.Tables(0).Columns.Contains("isnoaction") Then
                            objLeaveType._IsNoAction = CBool(mdsPayroll.Tables(0).Rows(i)("isnoaction").ToString)
                        End If

                        'Pinkal (18-Aug-2013) -- End

                        'Sohail (21 Oct 2019) -- Start
                        'NMB Enhancement # : Option to set Exempt from Payroll on leave type master for It should be possible to update active employee status as Unpaid and automatically prorate payment of that employee effectively from the date selected. Also when employee is back, user should be able to update end date of the Unpaid status and automatically include that employee in payroll and prorate based on the date set
                        objLeaveType._Isexempt_from_payroll = False
                        'Sohail (21 Oct 2019) -- End

                        If mdsPayroll.Tables(0).Columns.Contains("isactive") Then
                            objLeaveType._Isactive = CBool(mdsPayroll.Tables(0).Rows(i)("isactive").ToString)
                        Else
                            objLeaveType._Isactive = True
                        End If

                        With objLeaveType
                            ._FormName = mstrModuleName
                            ._LoginEmployeeunkid = 0
                            ._ClientIP = getIP()
                            ._HostName = getHostName()
                            ._FromWeb = False
                            ._AuditUserId = User._Object._Userunkid
._CompanyUnkid = Company._Object._Companyunkid
                            ._AuditDate = ConfigParameter._Object._CurrentDateAndTime
                        End With
                        blnflag = objLeaveType.Insert()

                        If objLeaveType._Message <> "" Then
                            eZeeMsgBox.Show(objLeaveType._Leavetypecode & " " & objLeaveType._Leavename & " : " & objLeaveType._Message, enMsgBoxStyle.Information)
                            Return False
                        End If

                    ElseIf MasterId = 48 Then  'Service Provider Master

                        Dim objServiceProvider As New clsinstitute_master

                        objServiceProvider._Institute_Code = mdsPayroll.Tables(0).Rows(i)(arColumn(0)).ToString
                        objServiceProvider._Institute_Name = mdsPayroll.Tables(0).Rows(i)(arColumn(1)).ToString
                        objServiceProvider._IsFormRequire = CBool(mdsPayroll.Tables(0).Rows(i)(arColumn(2)).ToString)
                        objServiceProvider._IsLocalProvider = CBool(mdsPayroll.Tables(0).Rows(i)(arColumn(3)).ToString)

                        If mdsPayroll.Tables(0).Columns.Contains("institute_address") Then
                            objServiceProvider._Institute_Address = mdsPayroll.Tables(0).Rows(i)("institute_address").ToString
                        End If

                        If mdsPayroll.Tables(0).Columns.Contains("telephoneno") Then
                            objServiceProvider._Telephoneno = mdsPayroll.Tables(0).Rows(i)("telephoneno").ToString
                        End If

                        If mdsPayroll.Tables(0).Columns.Contains("institute_fax") Then
                            objServiceProvider._Institute_Fax = mdsPayroll.Tables(0).Rows(i)("institute_fax").ToString
                        End If

                        If mdsPayroll.Tables(0).Columns.Contains("institute_email") Then
                            objServiceProvider._Institute_Email = mdsPayroll.Tables(0).Rows(i)("institute_email").ToString
                        End If

                        If mdsPayroll.Tables(0).Columns.Contains("description") Then
                            objServiceProvider._Description = mdsPayroll.Tables(0).Rows(i)("description").ToString
                        End If

                        If mdsPayroll.Tables(0).Columns.Contains("contact_person") Then
                            objServiceProvider._Contact_Person = mdsPayroll.Tables(0).Rows(i)("contact_person").ToString
                        End If

                        If mdsPayroll.Tables(0).Columns.Contains("country") Then
                            Dim objMaster As New clsMasterData
                            Dim intCountryId As Integer = objMaster.GetCountryUnkId(mdsPayroll.Tables(0).Columns.Contains("country").ToString().Trim)
                            objServiceProvider._Countryunkid = intCountryId
                        End If

                        If mdsPayroll.Tables(0).Columns.Contains("state") Then
                            Dim objstate As New clsstate_master
                            Dim intStateId As Integer = objstate.GetStateUnkId(mdsPayroll.Tables(0).Columns.Contains("state").ToString().Trim)
                            objServiceProvider._Stateunkid = intStateId
                        End If

                        If mdsPayroll.Tables(0).Columns.Contains("city") Then
                            Dim objCity As New clscity_master
                            Dim intCityId As Integer = objCity.GetCityUnkId(mdsPayroll.Tables(0).Columns.Contains("city").ToString().Trim)
                            objServiceProvider._Stateunkid = intCityId
                        End If

                        If mdsPayroll.Tables(0).Columns.Contains("isactive") Then
                            objServiceProvider._Isactive = CBool(mdsPayroll.Tables(0).Rows(i)("isactive").ToString)
                        Else
                            objServiceProvider._Isactive = True
                        End If

                        objServiceProvider._Ishospital = True

                        With objServiceProvider
                            ._FormName = mstrModuleName
                            ._LoginEmployeeunkid = 0
                            ._ClientIP = getIP()
                            ._HostName = getHostName()
                            ._FromWeb = False
                            ._AuditUserId = User._Object._Userunkid
._CompanyUnkid = Company._Object._Companyunkid
                            ._AuditDate = ConfigParameter._Object._CurrentDateAndTime
                        End With

                        blnflag = objServiceProvider.Insert()

                        If objServiceProvider._Message <> "" Then
                            eZeeMsgBox.Show(objServiceProvider._Institute_Code & " " & objServiceProvider._Institute_Name & " : " & objServiceProvider._Message, enMsgBoxStyle.Information)
                            Return False
                        End If

                    ElseIf MasterId = 60 Then  'Training Provider Master

                        Dim objTraningProvider As New clsinstitute_master

                        objTraningProvider._Institute_Code = mdsPayroll.Tables(0).Rows(i)(arColumn(0)).ToString
                        objTraningProvider._Institute_Name = mdsPayroll.Tables(0).Rows(i)(arColumn(1)).ToString
                        objTraningProvider._Issync_Recruit = CBool(mdsPayroll.Tables(0).Rows(i)(arColumn(2)).ToString)

                        If mdsPayroll.Tables(0).Columns.Contains("institute_address") Then
                            objTraningProvider._Institute_Address = mdsPayroll.Tables(0).Rows(i)("institute_address").ToString
                        End If

                        If mdsPayroll.Tables(0).Columns.Contains("telephoneno") Then
                            objTraningProvider._Telephoneno = mdsPayroll.Tables(0).Rows(i)("telephoneno").ToString
                        End If

                        If mdsPayroll.Tables(0).Columns.Contains("institute_fax") Then
                            objTraningProvider._Institute_Fax = mdsPayroll.Tables(0).Rows(i)("institute_fax").ToString
                        End If

                        If mdsPayroll.Tables(0).Columns.Contains("institute_email") Then
                            objTraningProvider._Institute_Email = mdsPayroll.Tables(0).Rows(i)("institute_email").ToString
                        End If

                        If mdsPayroll.Tables(0).Columns.Contains("description") Then
                            objTraningProvider._Description = mdsPayroll.Tables(0).Rows(i)("description").ToString
                        End If

                        If mdsPayroll.Tables(0).Columns.Contains("contact_person") Then
                            objTraningProvider._Contact_Person = mdsPayroll.Tables(0).Rows(i)("contact_person").ToString
                        End If

                        If mdsPayroll.Tables(0).Columns.Contains("country") Then
                            Dim objMaster As New clsMasterData
                            Dim intCountryId As Integer = objMaster.GetCountryUnkId(mdsPayroll.Tables(0).Columns.Contains("country").ToString().Trim)
                            objTraningProvider._Countryunkid = intCountryId
                        End If

                        If mdsPayroll.Tables(0).Columns.Contains("state") Then
                            Dim objstate As New clsstate_master
                            Dim intStateId As Integer = objstate.GetStateUnkId(mdsPayroll.Tables(0).Columns.Contains("state").ToString().Trim)
                            objTraningProvider._Stateunkid = intStateId
                        End If

                        If mdsPayroll.Tables(0).Columns.Contains("city") Then
                            Dim objCity As New clscity_master
                            Dim intCityId As Integer = objCity.GetCityUnkId(mdsPayroll.Tables(0).Columns.Contains("city").ToString().Trim)
                            objTraningProvider._Stateunkid = intCityId
                        End If

                        If mdsPayroll.Tables(0).Columns.Contains("isactive") Then
                            objTraningProvider._Isactive = CBool(mdsPayroll.Tables(0).Rows(i)("isactive").ToString)
                        Else
                            objTraningProvider._Isactive = True
                        End If

                        objTraningProvider._Ishospital = False
                        objTraningProvider._IsLocalProvider = False

                        With objTraningProvider
                            ._FormName = mstrModuleName
                            ._LoginEmployeeunkid = 0
                            ._ClientIP = getIP()
                            ._HostName = getHostName()
                            ._FromWeb = False
                            ._AuditUserId = User._Object._Userunkid
._CompanyUnkid = Company._Object._Companyunkid
                            ._AuditDate = ConfigParameter._Object._CurrentDateAndTime
                        End With

                        blnflag = objTraningProvider.Insert()

                        If objTraningProvider._Message <> "" Then
                            eZeeMsgBox.Show(objTraningProvider._Institute_Code & " " & objTraningProvider._Institute_Name & " : " & objTraningProvider._Message, enMsgBoxStyle.Information)
                            Return False
                        End If

                        'Shani(25-Sep-2015) -- Start
                        'ENHANCEMENT : Add Option to Import Bank Branch Master on Common Importation(Issue No : 455)
                    ElseIf MasterId = 43 Then  'Bank Branch Master
                        Dim objBankBranch As New clsbankbranch_master
                        Dim objBankGroupMaster As New clspayrollgroup_master
                        Dim intCountryUnkId As Integer = 0
                        Dim intStateUnkId As Integer = 0
                        Dim intCityUnkId As Integer = 0
                        Dim intZipCodeUnkId As Integer = 0
                        Dim intBankGroupUnkid As Integer = 0

                        'Shani(28-Jan-2016) -- Start
                        'Issue -  Column Name Set Static on datatable to chnage set dynamic Column name
                        'Dim dsGroup As DataSet = objBankGroupMaster.getListForCombo(CInt(PayrollGroupType.Bank), "Bank Group")
                        'Dim dRow() As DataRow = dsGroup.Tables(0).Select("name = '" & mdsPayroll.Tables(0).Rows(i)("Bank Group").ToString & "'")

                        'If dRow.Length > 0 Then
                        '    objBankBranch._Bankgroupunkid = CInt(dRow(0).Item("groupmasterunkid"))
                        'Else
                        '    'Message Display
                        '    Return False
                        'End If
                        'objBankBranch._Branchcode = mdsPayroll.Tables(0).Rows(i)("Branch Code").ToString
                        'objBankBranch._Branchname = mdsPayroll.Tables(0).Rows(i)("Branch Name").ToString
                        'objBankBranch._Isclearing = CBool(mdsPayroll.Tables(0).Rows(i)("Clearing").ToString)

                        'If IsDBNull(mdsPayroll.Tables(0).Rows(i)("Address1").ToString) = False AndAlso mdsPayroll.Tables(0).Rows(i)("Address1").ToString.Length > 0 Then
                        '    objBankBranch._Contactno = mdsPayroll.Tables(0).Rows(i)("Address1").ToString
                        'End If

                        'If IsDBNull(mdsPayroll.Tables(0).Rows(i)("Address2").ToString) = False AndAlso mdsPayroll.Tables(0).Rows(i)("Address2").ToString.Length > 0 Then
                        '    objBankBranch._Contactno = mdsPayroll.Tables(0).Rows(i)("Address2").ToString
                        'End If

                        'If IsDBNull(mdsPayroll.Tables(0).Rows(i)("Country").ToString) = False AndAlso mdsPayroll.Tables(0).Rows(i)("Country").ToString.Length > 0 Then
                        '    Dim objMasterdata As New clsMasterData
                        '    intCountryUnkId = objMasterdata.GetCountryUnkId(mdsPayroll.Tables(0).Rows(i)("Country").ToString)
                        '    If intCountryUnkId > 0 Then
                        '        objBankBranch._Countryunkid = intCountryUnkId
                        '    End If
                        'End If

                        'If IsDBNull(mdsPayroll.Tables(0).Rows(i)("State").ToString) = False AndAlso mdsPayroll.Tables(0).Rows(i)("State").ToString.Length > 0 Then
                        '    Dim dsState As DataSet = Nothing
                        '    Dim objStatemaster As New clsstate_master
                        '    dsState = objStatemaster.GetList("State", True, False, intCountryUnkId)
                        '    dRow = dsState.Tables(0).Select("name='" & mdsPayroll.Tables(0).Rows(i)("State").ToString & "'")
                        '    If dRow.Length > 0 Then
                        '        intStateUnkId = CInt(dRow(0).Item("stateunkid"))
                        '        objBankBranch._Stateunkid = intStateUnkId
                        '    End If
                        'End If

                        'If IsDBNull(mdsPayroll.Tables(0).Rows(i)("City").ToString) = False AndAlso mdsPayroll.Tables(0).Rows(i)("City").ToString.Length > 0 Then
                        '    Dim dsCity As DataSet = Nothing
                        '    Dim objCitymaster As New clscity_master
                        '    dsCity = objCitymaster.GetList("City", True, False, intStateUnkId)
                        '    dRow = dsCity.Tables(0).Select("name='" & mdsPayroll.Tables(0).Rows(i)("City").ToString & "'")
                        '    If dRow.Length > 0 Then
                        '        If intCountryUnkId > 0 AndAlso intStateUnkId > 0 Then
                        '            intCityUnkId = CInt(dRow(0).Item("cityunkid"))
                        '        ElseIf intCountryUnkId <= 0 AndAlso intStateUnkId > 0 Then
                        '            intCityUnkId = CInt(dRow(0).Item("cityunkid"))
                        '        ElseIf intCountryUnkId <= 0 AndAlso intStateUnkId <= 0 Then
                        '            intCityUnkId = CInt(dRow(0).Item("cityunkid"))
                        '        End If
                        '        objBankBranch._Cityunkid = intCityUnkId
                        '    End If
                        'End If

                        'If IsDBNull(mdsPayroll.Tables(0).Rows(i)("Zip Code").ToString) = False AndAlso mdsPayroll.Tables(0).Rows(i)("Zip Code").ToString.Length > 0 Then
                        '    Dim dsZipcode As DataSet = Nothing
                        '    Dim objZipcodemaster As New clszipcode_master
                        '    dsZipcode = objZipcodemaster.GetList("ZipCode", True, False, intCityUnkId)
                        '    dRow = dsZipcode.Tables(0).Select("zipcode_no='" & mdsPayroll.Tables(0).Rows(i)("Zip Code").ToString & "'")
                        '    If dRow.Length > 0 Then
                        '        If intCountryUnkId > 0 AndAlso intStateUnkId > 0 AndAlso intCityUnkId > 0 Then
                        '            intZipCodeUnkId = CInt(dRow(0).Item("zipcodeunkid"))
                        '        ElseIf intCountryUnkId <= 0 AndAlso intStateUnkId > 0 AndAlso intCityUnkId > 0 Then
                        '            intZipCodeUnkId = CInt(dRow(0).Item("zipcodeunkid"))
                        '        ElseIf intCountryUnkId <= 0 AndAlso intStateUnkId <= 0 AndAlso intCityUnkId > 0 Then
                        '            intZipCodeUnkId = CInt(dRow(0).Item("zipcodeunkid"))
                        '        ElseIf intCountryUnkId <= 0 AndAlso intStateUnkId <= 0 AndAlso intCityUnkId <= 0 Then
                        '            intZipCodeUnkId = CInt(dRow(0).Item("zipcodeunkid"))
                        '        End If
                        '        objBankBranch._Pincodeunkid = intZipCodeUnkId
                        '    End If
                        'End If

                        'If IsDBNull(mdsPayroll.Tables(0).Rows(i)("Contact Person").ToString) = False AndAlso mdsPayroll.Tables(0).Rows(i)("Contact Person").ToString.Length > 0 Then
                        '    objBankBranch._Contactperson = mdsPayroll.Tables(0).Rows(i)("Contact Person").ToString
                        'End If

                        'If IsDBNull(mdsPayroll.Tables(0).Rows(i)("Contact No").ToString) = False AndAlso mdsPayroll.Tables(0).Rows(i)("Contact No").ToString.Length > 0 Then
                        '    objBankBranch._Contactno = mdsPayroll.Tables(0).Rows(i)("Contact No").ToString
                        'End If


                        Dim dsGroup As DataSet = objBankGroupMaster.getListForCombo(CInt(PayrollGroupType.Bank), "Bank Group")
                        Dim dRow() As DataRow = dsGroup.Tables(0).Select("name = '" & mdsPayroll.Tables(0).Rows(i)(arColumn(0)).ToString & "'")

                        If dRow.Length > 0 Then
                            intBankGroupUnkid = CInt(dRow(0).Item("groupmasterunkid"))
                        Else
                            objBankGroupMaster._Groupalias = ""
                            objBankGroupMaster._Groupcode = mdsPayroll.Tables(0).Rows(i)(arColumn(0)).ToString
                            objBankGroupMaster._Groupname = mdsPayroll.Tables(0).Rows(i)(arColumn(0)).ToString
                            objBankGroupMaster._Description = ""
                            objBankGroupMaster._Website = ""
                            objBankGroupMaster._Grouptype_Id = PayrollGroupType.Bank
                            With objBankGroupMaster
                                ._FormName = mstrModuleName
                                ._LoginEmployeeunkid = 0
                                ._ClientIP = getIP()
                                ._HostName = getHostName()
                                ._FromWeb = False
                                ._AuditUserId = User._Object._Userunkid
._CompanyUnkid = Company._Object._Companyunkid
                                ._AuditDate = ConfigParameter._Object._CurrentDateAndTime
                            End With
                            objBankGroupMaster.Insert()
                            If objBankGroupMaster._Message <> "" Then
                                eZeeMsgBox.Show(objBankGroupMaster._Groupcode & " " & objBankGroupMaster._Groupname & " : " & objBankGroupMaster._Message, enMsgBoxStyle.Information)
                                Return False
                            End If
                            intBankGroupUnkid = objBankGroupMaster._Groupmasterunkid
                        End If

                        objBankBranch._Bankgroupunkid = intBankGroupUnkid
                        objBankBranch._Branchcode = mdsPayroll.Tables(0).Rows(i)(arColumn(1)).ToString
                        objBankBranch._Branchname = mdsPayroll.Tables(0).Rows(i)(arColumn(2)).ToString
                        objBankBranch._Isclearing = CBool(mdsPayroll.Tables(0).Rows(i)(arColumn(3)).ToString)

                        If arColumn(4).Trim.Length > 0 AndAlso mdsPayroll.Tables(0).Columns.Contains(arColumn(4)) Then
                            objBankBranch._Address1 = mdsPayroll.Tables(0).Rows(i)(arColumn(4)).ToString
                        End If

                        If arColumn(5).Trim.Length > 0 AndAlso mdsPayroll.Tables(0).Columns.Contains(arColumn(5)) Then
                            objBankBranch._Address2 = mdsPayroll.Tables(0).Rows(i)(arColumn(5)).ToString
                        End If

                        If arColumn(6).Trim.Length > 0 AndAlso mdsPayroll.Tables(0).Columns.Contains(arColumn(6)) Then
                            Dim objMasterdata As New clsMasterData
                            intCountryUnkId = objMasterdata.GetCountryUnkId(mdsPayroll.Tables(0).Rows(i)(arColumn(6)).ToString)
                            If intCountryUnkId > 0 Then
                                objBankBranch._Countryunkid = intCountryUnkId
                            End If
                        End If

                        If arColumn(7).Trim.Length > 0 AndAlso mdsPayroll.Tables(0).Columns.Contains(arColumn(7)) Then
                            Dim dsState As DataSet = Nothing
                            Dim objStatemaster As New clsstate_master
                            dsState = objStatemaster.GetList("State", True, False, intCountryUnkId)
                            dRow = dsState.Tables(0).Select("name='" & mdsPayroll.Tables(0).Rows(i)(arColumn(7)).ToString & "'")
                            If dRow.Length > 0 Then
                                intStateUnkId = CInt(dRow(0).Item("stateunkid"))
                                objBankBranch._Stateunkid = intStateUnkId
                            End If
                        End If

                        If arColumn(8).Trim.Length > 0 AndAlso mdsPayroll.Tables(0).Columns.Contains(arColumn(8)) Then
                            Dim dsCity As DataSet = Nothing
                            Dim objCitymaster As New clscity_master
                            dsCity = objCitymaster.GetList("City", True, False, intStateUnkId)
                            dRow = dsCity.Tables(0).Select("name='" & mdsPayroll.Tables(0).Rows(i)(arColumn(8)).ToString & "'")
                            If dRow.Length > 0 Then
                                If intCountryUnkId > 0 AndAlso intStateUnkId > 0 Then
                                    intCityUnkId = CInt(dRow(0).Item("cityunkid"))
                                ElseIf intCountryUnkId <= 0 AndAlso intStateUnkId > 0 Then
                                    intCityUnkId = CInt(dRow(0).Item("cityunkid"))
                                ElseIf intCountryUnkId <= 0 AndAlso intStateUnkId <= 0 Then
                                    intCityUnkId = CInt(dRow(0).Item("cityunkid"))
                                End If
                                objBankBranch._Cityunkid = intCityUnkId
                            End If
                        End If

                        If arColumn(9).Trim.Length > 0 AndAlso mdsPayroll.Tables(0).Columns.Contains(arColumn(9)) Then
                            Dim dsZipcode As DataSet = Nothing
                            Dim objZipcodemaster As New clszipcode_master
                            dsZipcode = objZipcodemaster.GetList("ZipCode", True, False, intCityUnkId)
                            dRow = dsZipcode.Tables(0).Select("zipcode_no='" & mdsPayroll.Tables(0).Rows(i)(arColumn(9)).ToString & "'")
                            If dRow.Length > 0 Then
                                If intCountryUnkId > 0 AndAlso intStateUnkId > 0 AndAlso intCityUnkId > 0 Then
                                    intZipCodeUnkId = CInt(dRow(0).Item("zipcodeunkid"))
                                ElseIf intCountryUnkId <= 0 AndAlso intStateUnkId > 0 AndAlso intCityUnkId > 0 Then
                                    intZipCodeUnkId = CInt(dRow(0).Item("zipcodeunkid"))
                                ElseIf intCountryUnkId <= 0 AndAlso intStateUnkId <= 0 AndAlso intCityUnkId > 0 Then
                                    intZipCodeUnkId = CInt(dRow(0).Item("zipcodeunkid"))
                                ElseIf intCountryUnkId <= 0 AndAlso intStateUnkId <= 0 AndAlso intCityUnkId <= 0 Then
                                    intZipCodeUnkId = CInt(dRow(0).Item("zipcodeunkid"))
                                End If
                                objBankBranch._Pincodeunkid = intZipCodeUnkId
                            End If
                        End If

                        If arColumn(10).Trim.Length > 0 AndAlso mdsPayroll.Tables(0).Columns.Contains(arColumn(10)) Then
                            objBankBranch._Contactperson = mdsPayroll.Tables(0).Rows(i)(arColumn(10)).ToString
                        End If

                        If arColumn(11).Trim.Length > 0 AndAlso mdsPayroll.Tables(0).Columns.Contains(arColumn(11)) Then
                            objBankBranch._Contactno = mdsPayroll.Tables(0).Rows(i)(arColumn(11)).ToString
                        End If
                        'Shani(28-Jan-2016) -- End


                        If arColumn(12).Trim.Length > 0 AndAlso mdsPayroll.Tables(0).Columns.Contains(arColumn(12)) Then ' SORT CODE
                            objBankBranch._Sortcode = mdsPayroll.Tables(0).Rows(i)(arColumn(12)).ToString
                        End If

                        With objBankBranch
                            ._FormName = mstrModuleName
                            ._LoginEmployeeunkid = 0
                            ._ClientIP = getIP()
                            ._HostName = getHostName()
                            ._FromWeb = False
                            ._AuditUserId = User._Object._Userunkid
._CompanyUnkid = Company._Object._Companyunkid
                            ._AuditDate = ConfigParameter._Object._CurrentDateAndTime
                        End With

                        blnflag = objBankBranch.Insert()

                        If objBankBranch._Message <> "" Then
                            eZeeMsgBox.Show(objBankBranch._Message, enMsgBoxStyle.Information)
                            Return False
                        End If

                        'Shani(25-Sep-2015) -- End
                    End If

                Next

            End If

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "ImportData", mstrModuleName)
        End Try
        Return blnflag
    End Function

    Private Sub GenerateControls(ByVal NoofCtrl As Integer, ByVal intMasterId As Integer)
        Try
            Dim dtFilter As DataTable
            dtFilter = New DataView(dtCaption, "masterid = '" & intMasterId & "'", "", DataViewRowState.CurrentRows).ToTable
            Dim dtTemp() As DataRow = Nothing
            pnlControl.Controls.Clear()

            Dim CmbPn As New Point(150, 10)
            Dim LblPn As New Point(20, 10)
            Dim dz As New Drawing.Size(130, 20)
            ReDim arColumn(NoofCtrl - 1)
            For i As Integer = 1 To NoofCtrl

                Dim objLbl As New Label
                Dim objcmb As New ComboBox


                If i = 6 Then

                    'Shani(25-Sep-2015) -- Start
                    'ENHANCEMENT : Add Option to Import Bank Branch Master on Common Importation(Issue No : 455)
                    'CmbPn = New Point(475, 10)
                    'LblPn = New Point(275, 10)
                    CmbPn = New Point(410, 10)
                    LblPn = New Point(275, 10)
                    'Shani(25-Sep-2015) -- End
                ElseIf i = 11 Then

                    'Shani(25-Sep-2015) -- Start
                    'ENHANCEMENT : Add Option to Import Bank Branch Master on Common Importation(Issue No : 455)
                    'CmbPn = New Point(220, 150)
                    'LblPn = New Point(20, 150)
                    CmbPn = New Point(150, 150)
                    LblPn = New Point(20, 150)
                    'Shani(25-Sep-2015) -- End
                ElseIf i = 16 Then

                    CmbPn = New Point(475, 150)
                    LblPn = New Point(275, 150)

                ElseIf i = 21 Then

                    CmbPn = New Point(220, 290)
                    LblPn = New Point(20, 290)

                ElseIf i = 26 Then

                    CmbPn = New Point(475, 290)
                    LblPn = New Point(275, 290)

                End If

                objLbl.Name = "lbl" & i
                objLbl.AutoSize = False
                dtTemp = dtFilter.Select("captionunkid='" & i & "'")
                If dtTemp.Length > 0 Then
                    objLbl.Text = dtTemp(0)("caption").ToString
                End If

                objLbl.Size = dz
                objLbl.TextAlign = ContentAlignment.MiddleLeft
                objLbl.Location = LblPn
                pnlControl.Controls.Add(objLbl)
                LblPn.Y += 28

                objcmb.Name = "cmb" & i
                objcmb.Location = CmbPn
                If dicComboName.ContainsKey(i) = False Then
                    dicComboName.Add(i, objcmb)
                End If
                objcmb.DropDownStyle = ComboBoxStyle.DropDownList
                pnlControl.Controls.Add(objcmb)
                CmbPn.Y += 28
                AddHandler objcmb.SelectedIndexChanged, AddressOf Combobox_SelectedIndexChanged
            Next

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "GenerateControls", mstrModuleName)
        End Try
    End Sub

    Public Function validation() As Boolean
        Try
            For Each cr As Control In pnlControl.Controls

                If cr.GetType.FullName.ToString = "System.Windows.Forms.ComboBox" Then
                    'Shani(25-Sep-2015) -- Start
                    'ENHANCEMENT : Add Option to Import Bank Branch Master on Common Importation(Issue No : 455)
                    'If CType(cr, ComboBox).SelectedIndex = -1 Then
                    '    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 3, "Field is compulsory information.Please Select Field."), CType(enMsgBoxStyle.Information + enMsgBoxStyle.OkOnly, enMsgBoxStyle))
                    '    CType(cr, ComboBox).Select()
                    '    Return False
                    'End If
                    If CInt(lvMasterList.SelectedItems(0).Tag) = 43 Then
                        If CInt(CType(cr, ComboBox).Name.Substring(3)) <= 4 Then
                            If CType(cr, ComboBox).SelectedIndex = -1 Then
                                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 8, "Bank Group,Bank Branch Code,Bank Branch Name,Clearing. These Fields are compulsory information.Please Select Bank Group,Bank Branch Code,Bank Branch Name,Clearing."), CType(enMsgBoxStyle.Information + enMsgBoxStyle.OkOnly, enMsgBoxStyle))
                                CType(cr, ComboBox).Select()
                                Return False
                            End If
                        End If
                        'S.SANDEEP [22 JUN 2016] -- START
                        'MAKE JOB ALLOCATION COMBO OPTIONAL
                        'Else
                        '    If CType(cr, ComboBox).SelectedIndex = -1 Then
                        '        eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 3, "Field is compulsory information.Please Select Field."), CType(enMsgBoxStyle.Information + enMsgBoxStyle.OkOnly, enMsgBoxStyle))
                        '        CType(cr, ComboBox).Select()
                        '        Return False
                        '    End If
                        'End If
                    ElseIf CInt(lvMasterList.SelectedItems(0).Tag) = 21 Then
                        Select Case CType(cr, ComboBox).Name.ToUpper
                            Case "CMB1", "CMB2"
                                If CType(cr, ComboBox).SelectedIndex = -1 Then
                                    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 3, "Field is compulsory information.Please Select Field."), CType(enMsgBoxStyle.Information + enMsgBoxStyle.OkOnly, enMsgBoxStyle))
                                    CType(cr, ComboBox).Select()
                                    Return False
                                End If
                        End Select
                    Else
                        If CType(cr, ComboBox).SelectedIndex = -1 Then
                            eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 3, "Field is compulsory information.Please Select Field."), CType(enMsgBoxStyle.Information + enMsgBoxStyle.OkOnly, enMsgBoxStyle))
                            CType(cr, ComboBox).Select()
                            Return False
                        End If
                    End If
                    'S.SANDEEP [22 JUN 2016] -- END


                    'Shani(25-Sep-2015) -- End
                End If

            Next
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "validation", mstrModuleName)
        End Try
        Return True
    End Function

    Private Sub ReadFileToArray(ByVal strfilename As String)


        'Check if file exist
        If IO.File.Exists(strfilename) Then
            Dim tmpstream As IO.StreamReader = IO.File.OpenText(strfilename)
            Dim strlines() As String
            Dim strline() As String

            'Load content of file to strLines array
            strlines = tmpstream.ReadToEnd().Split(CChar(Environment.NewLine))

            ' Redimension the array.
            num_rows = UBound(strlines)

            If strlines(0).Split(CChar(",")).GetValue(0).ToString <> "" Then
                strline = strlines(0).Split(CChar(","))
            Else
                strline = strlines(1).Split(CChar(","))
            End If
            num_cols = UBound(strline)
            ReDim m_Strarray(CInt(num_rows), CInt(num_cols))

            ' Copy the data into the array.
            For x As Long = 0 To num_rows
                If strlines(CInt(x)).Split(CChar(",")).GetValue(0).ToString <> "" Then
                    strline = strlines(CInt(x)).Split(CChar(","))
                    If strline.Length - 1 <> num_cols Then Continue For
                    For y As Long = 0 To CInt(num_cols)
                        m_Strarray(CInt(x), CInt(y)) = strline(CInt(y)).Trim(CChar("""")).Trim
                    Next
                End If
            Next

        End If

    End Sub

    Private Sub SetLableCaption()
        Try

            'Pinkal (18-Aug-2013) -- Start
            'Enhancement : TRA Changes
            'Dim dtTable As DataTable = New DataView(dsList.Tables("Masters"), "id <= 30 or id = 51 ", "", DataViewRowState.CurrentRows).ToTable

            'Shani(25-Sep-2015) -- Start
            'ENHANCEMENT : Add Option to Import Bank Branch Master on Common Importation(Issue No : 455)
            'Dim dtTable As DataTable = New DataView(dsList.Tables("Masters"), "id <= 30 or (id = 51 OR id = 60 OR id = 48)", "", DataViewRowState.CurrentRows).ToTable
            Dim dtTable As DataTable = New DataView(dsList.Tables("Masters"), "id <= 30 or (id = 51 OR id = 60 OR id = 48 OR id = 43)", "", DataViewRowState.CurrentRows).ToTable
            'Shani(25-Sep-2015) -- End

            'Pinkal (18-Aug-2013) -- End
            Dim strMasterId As String = String.Empty
            Dim intCnt As Integer = 1
            Dim intLangCnt As Integer = 1
            Dim strFields() As String = Nothing
            Dim dCol As DataColumn

            dCol = New DataColumn("captionunkid")
            dCol.DataType = System.Type.GetType("System.Int32")
            dtCaption.Columns.Add(dCol)

            dCol = New DataColumn("caption")
            dCol.DataType = System.Type.GetType("System.String")
            dtCaption.Columns.Add(dCol)

            dCol = New DataColumn("masterid")
            dCol.DataType = System.Type.GetType("System.Int32")
            dtCaption.Columns.Add(dCol)

            For Each dtRow As DataRow In dtTable.Rows
                intCnt = 1
                strFields = dtRow.Item("Fields").ToString.Split(CChar("|"))
                If strFields.Length > 0 Then
                    For i As Integer = 0 To strFields.Length - 1
                        Dim dcRow As DataRow = dtCaption.NewRow
                        dcRow.Item("captionunkid") = intCnt
                        dcRow.Item("caption") = strFields(i)
                        dcRow.Item("masterid") = CInt(dtRow.Item("Id"))
                        intCnt += 1
                        dtCaption.Rows.Add(dcRow)
                        intLangCnt += 1
                    Next
                End If
            Next


        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "SetLableCaption", mstrModuleName)
        End Try
    End Sub

    'Pinkal (03-Jul-2013) -- Start
    'Enhancement : TRA Changes

    Private Function GenerateShiftDays(ByVal drRow As DataRow()) As DataTable
        Try
            Dim days() As String = Globalization.CultureInfo.CurrentCulture.DateTimeFormat.DayNames

            If mdtDays Is Nothing Then
                mdtDays = New DataTable("ShiftDays")
                mdtDays.Columns.Add("shifttranunkid", Type.GetType("System.Int32"))
                mdtDays.Columns.Add("shiftunkid", Type.GetType("System.Int32"))
                mdtDays.Columns.Add("dayId", Type.GetType("System.Int32"))
                mdtDays.Columns.Add("days", Type.GetType("System.String"))
                mdtDays.Columns.Add("isweekend", Type.GetType("System.Boolean"))
                mdtDays.Columns.Add("starttime", Type.GetType("System.DateTime"))
                mdtDays.Columns.Add("endtime", Type.GetType("System.DateTime"))
                mdtDays.Columns.Add("nightfromhrs", Type.GetType("System.DateTime"))
                mdtDays.Columns.Add("nighttohrs", Type.GetType("System.DateTime"))
                mdtDays.Columns.Add("breaktimeinsec", Type.GetType("System.Int32"))
                mdtDays.Columns.Add("workinghrsinsec", Type.GetType("System.Int32"))
                mdtDays.Columns.Add("calcovertimeafterinsec", Type.GetType("System.Int32"))
                mdtDays.Columns.Add("calcshorttimebeforeinsec", Type.GetType("System.Int32"))
                mdtDays.Columns.Add("halffromhrsinsec", Type.GetType("System.Int32"))
                mdtDays.Columns.Add("halftohrsinsec", Type.GetType("System.Int32"))
                mdtDays.Columns.Add("daystart_time", Type.GetType("System.DateTime"))
                mdtDays.Columns.Add("AUD", Type.GetType("System.String"))

                'Pinkal (03-Aug-2018) -- Start
                'Enhancement - Changes For B5 [Ref #289]

                'Pinkal (04-Dec-2019) -- Start
                'Enhancement NMB - Working On Close Year Enhancement for NMB.
                '[Annor set day off shift for 2 clients (Papaye & Natural) in which he configured all shift day as weekend but system didn't allow him to save due to 0 working hours for week]
                'mdtDays.Columns.Add("countotmins_aftersftendtime", Type.GetType("System.Int32"))
                mdtDays.Columns.Add("countotmins_aftersftendtimeinsec", Type.GetType("System.Int32"))
                'Pinkal (04-Dec-2019) -- End


                'Pinkal (03-Aug-2018) -- End


                For i As Integer = 0 To drRow.Length - 1
                    Dim dr As DataRow = mdtDays.NewRow
                    dr("shifttranunkid") = -1
                    dr("shiftunkid") = -1
                    dr("dayId") = GetWeekDayNumber(drRow(i)("DAYS").ToString())
                    dr("Days") = drRow(i)("DAYS").ToString()
                    dr("isweekend") = CBool(drRow(i)("isweekend"))

                    'Pinkal (04-Dec-2019) -- Start
                    'Enhancement NMB - Working On Close Year Enhancement for NMB.
                    '[Annor set day off shift for 2 clients (Papaye & Natural) in which he configured all shift day as weekend but system didn't allow him to save due to 0 working hours for week]
                    If IsDBNull(drRow(i)("starttime")) = False AndAlso drRow(i)("starttime").ToString().Trim().Length > 0 Then
                    dr("starttime") = CDate(drRow(i)("starttime"))
                    End If

                    If IsDBNull(drRow(i)("endtime")) = False AndAlso drRow(i)("endtime").ToString().Trim().Length > 0 Then
                    dr("endtime") = CDate(drRow(i)("endtime"))
                    End If
                    'Pinkal (04-Dec-2019) -- End

                    dr("nightfromhrs") = CDate(drRow(i)("nightfromhrs"))
                    dr("nighttohrs") = CDate(drRow(i)("nighttohrs"))
                    dr("breaktimeinsec") = drRow(i)("breaktime")
                    dr("workinghrsinsec") = drRow(i)("workinghrs")
                    dr("calcovertimeafterinsec") = drRow(i)("calcovertimeafter")
                    dr("calcshorttimebeforeinsec") = drRow(i)("calcshorttimebefore")
                    dr("halffromhrsinsec") = drRow(i)("halffromhrs")
                    dr("halftohrsinsec") = drRow(i)("halftohrs")


                    'Pinkal (04-Dec-2019) -- Start
                    'Enhancement NMB - Working On Close Year Enhancement for NMB.
                    '[Annor set day off shift for 2 clients (Papaye & Natural) in which he configured all shift day as weekend but system didn't allow him to save due to 0 working hours for week]
                    If IsDBNull(drRow(i)("daystart_time")) = False AndAlso drRow(i)("daystart_time").ToString().Trim().Length > 0 Then
                    dr("daystart_time") = CDate(drRow(i)("daystart_time"))
                    End If
                    'Pinkal (04-Dec-2019) -- End

                    dr("AUD") = "A"


                    'Pinkal (03-Aug-2018) -- Start
                    'Enhancement - Changes For B5 [Ref #289]

                    'Pinkal (04-Dec-2019) -- Start
                    'Enhancement NMB - Working On Close Year Enhancement for NMB.
                    '[Annor set day off shift for 2 clients (Papaye & Natural) in which he configured all shift day as weekend but system didn't allow him to save due to 0 working hours for week]
                    'dr("countotmins_aftersftendtime") = drRow(i)("countotmins_aftersftendtime")
                    dr("countotmins_aftersftendtimeinsec") = drRow(i)("countotmins_aftersftendtime")
                    'Pinkal (04-Dec-2019) -- End


                    'Pinkal (03-Aug-2018) -- End


                    mdtDays.Rows.Add(dr)
                Next

            End If

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "GenerateShiftDays", mstrModuleName)
        End Try
        Return mdtDays
    End Function

    'Pinkal (03-Jul-2013) -- End


#End Region

#Region "Button Event"

    Private Sub btnOpenFile_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnOpenFile.Click
        Try
            Dim objFileDialog As New OpenFileDialog

            objFileDialog.Filter = "Excel files(*.xlsx)|*.xlsx|XML files(*.xml)|*.xml"

            objFileDialog.FilterIndex = 5
            If objFileDialog.ShowDialog = Windows.Forms.DialogResult.OK Then
                txtFilePath.Text = objFileDialog.FileName
                If lvMasterList.SelectedItems.Count > 0 Then
                    GenerateControls(CInt(lvMasterList.SelectedItems(0).SubItems(colhTotalFields.Index).Text), CInt(lvMasterList.SelectedItems(0).Tag))
                    FillTableFields()
                End If

            End If
            objFileDialog = Nothing

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "btnOpenFile_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub BtnImport_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles BtnImport.Click
        Dim blnflag As Boolean = False
        Dim Index As Integer = 0
        Try
            If Not System.IO.File.Exists(txtFilePath.Text) Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 1, "Please the select the correct file to Import Data from."), CType(enMsgBoxStyle.Information + enMsgBoxStyle.OkOnly, enMsgBoxStyle))
                btnOpenFile.Focus()
                Exit Sub
            End If

            If validation() Then

                Index = CInt(lvMasterList.SelectedItems(0).Index)

                Dim ImpFile As New IO.FileInfo(txtFilePath.Text)

                Dim count As Integer = 0
                For Each cr As Control In pnlControl.Controls
                    If cr.GetType.FullName.ToString = "System.Windows.Forms.ComboBox" Then
                        arColumn(count) = cr.Text
                        count += 1
                    End If
                Next

                If ImpFile.Extension.ToLower = ".txt" Or ImpFile.Extension.ToLower = ".csv" Then
                    blnflag = CreateOtherFileData(CInt(lvMasterList.SelectedItems(0).Tag))
                ElseIf ImpFile.Extension.ToLower = ".xls" Or ImpFile.Extension.ToLower = ".xlsx" Then
                    blnflag = CreateDataForExcel(CInt(lvMasterList.SelectedItems(0).Tag))
                ElseIf ImpFile.Extension.ToLower = ".xml" Then
                    blnflag = ImportData(mdsPayroll, CInt(lvMasterList.SelectedItems(0).Tag), arColumn)
                End If


                If blnflag Then
                    Dim drRow As DataRow() = dsList.Tables(0).Select("id = " & CInt(lvMasterList.SelectedItems(0).Tag))
                    If drRow.Length > 0 Then
                        drRow(0)("IsImport") = blnflag
                        dsList.AcceptChanges()
                    End If
                    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 5, "Data successfully Imported."), CType(enMsgBoxStyle.Information + enMsgBoxStyle.OkOnly, enMsgBoxStyle))
                    txtFilePath.Text = ""
                    FillMasters()
                    lvMasterList.Select()
                    lvMasterList.Items(CInt(Index)).Selected = True
                End If

            End If

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "BtnImport_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub BtnClose_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles BtnClose.Click
        Me.Close()
    End Sub

    'Anjan [04 June 2014] -- Start
    'ENHANCEMENT : Implementing Language,requested by Andrew
    Private Sub Form_LanguageClick(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Me.LanguageClick
        Dim objfrm As New frmLanguage
        Try
            If User._Object._Isrighttoleft = True Then
                objfrm.RightToLeft = Windows.Forms.RightToLeft.Yes
                objfrm.RightToLeftLayout = True
                Call Language.ctlRightToLeftlayOut(objfrm)
            End If

            Call SetMessages()

            clsLetterType.SetMessages()
            objfrm._Other_ModuleNames = "clsCommonImportExport"

            objfrm.displayDialog(Me)

            Call SetLanguage()

        Catch ex As System.Exception
            Call DisplayError.Show("-1", ex.Message, "Form_LanguageClick", mstrModuleName)
        Finally
            objfrm.Dispose()
            objfrm = Nothing
        End Try
    End Sub
    'Anjan [04 June 2014] -- End


#End Region

#Region "ListView Event"

    Private Sub lvMasterList_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles lvMasterList.SelectedIndexChanged
        Try
            If lvMasterList.SelectedItems.Count = 0 Then Exit Sub

            'For i As Integer = 0 To lvMasterList.Items.Count - 1
            '    If lvMasterList.Items(i).Index = 0 Then
            '        If CBool(lvMasterList.Items(i).Selected) = False And CBool(lvMasterList.Items(i).SubItems(colhIsImport.Index).Text) = False Then
            '            lvMasterList.SelectedItems(0).Selected = True
            '        End If
            '    ElseIf CBool(lvMasterList.Items(lvMasterList.Items(i).Index - 1).SubItems(colhIsImport.Index).Text) = True Then
            '        If lvMasterList.Items(i).Index > lvMasterList.SelectedItems(0).Index Then
            '            lvMasterList.Items(i).Selected = False
            '        End If
            '        If CBool(lvMasterList.SelectedItems(0).Selected) = False Then lvMasterList.SelectedItems(0).Selected = True
            '    ElseIf lvMasterList.SelectedItems.Count > 0 Then
            '        If lvMasterList.SelectedItems(0).Index >= lvMasterList.Items(i).Index Then
            '            lvMasterList.Items(i).Selected = False
            '        End If
            '    End If
            'Next

            If lvMasterList.SelectedItems.Count > 0 Then

                'If CBool(lvMasterList.SelectedItems(0).SubItems(colhIsImport.Index).Text) = True Then
                '    BtnImport.Enabled = False
                'Else
                '    BtnImport.Enabled = True
                'End If

                txtFilePath.Text = ""
                pnlControl.Controls.Clear()
                dicComboName.Clear()

            End If

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "lvMasterList_SelectedIndexChanged", mstrModuleName)
        End Try
    End Sub

#End Region

#Region "ComboBox Event"

    Private Sub Combobox_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs)
        Try
            Dim cmb As ComboBox = CType(sender, ComboBox)

            If cmb.Text <> "" Then


                'Shani(25-Sep-2015) -- Start
                'ENHANCEMENT : Add Option to Import Bank Branch  Master on Common Importation(Issue No : 455)
                If lvMasterList.SelectedItems.Count > 0 Then
                    If CInt(lvMasterList.SelectedItems(0).Tag) = 43 AndAlso cmb.Name.ToUpper = "CMB4" Then
                        strClearingFieldName = cmb.Text
                    End If
                End If
                'Shani(25-Sep-2015) -- End


                cmb.Tag = mdtOther.Columns(cmb.Text).DataType.ToString

                For Each cr As Control In pnlControl.Controls
                    If cr.GetType.FullName.ToString = "System.Windows.Forms.ComboBox" Then

                        If cr.Name <> cmb.Name Then

                            If CType(cr, ComboBox).SelectedIndex = cmb.SelectedIndex Then
                                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 2, "This field is already selected.Please Select New field."), CType(enMsgBoxStyle.Information + enMsgBoxStyle.OkOnly, enMsgBoxStyle))
                                cmb.SelectedIndex = -1
                                cmb.Select()
                                Exit Sub
                            End If

                        End If

                    End If
                Next

            End If

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "Combobox_SelectedIndexChanged", mstrModuleName)
        End Try
    End Sub

#End Region

    '<Language> This Auto Generated Text Please Do Not Modify it.
#Region " Language & UI Settings "
    Private Sub OtherSettings()
        Try
            Me.SuspendLayout()
            
            Call SetLanguage()

            Me.gbMasterList.GradientColor = GUI._eZeeContainerHeaderBackColor
            Me.gbMasterList.ForeColor = GUI._eZeeContainerHeaderForeColor


           
            Me.EZeeHeader1.GradientColor1 = GUI._HeaderBackColor1
            Me.EZeeHeader1.GradientColor2 = GUI._HeaderBackColor2
            Me.EZeeHeader1.BorderColor = GUI._HeaderBorderColor
            Me.EZeeHeader1.HeaderTextForeColor = GUI._HeaderTitleFontColor
            Me.EZeeHeader1.DescriptionForeColor = GUI._HeaderMessageFontColor


            Me.btnOpenFile.GradientBackColor = GUI._ButttonBackColor
            Me.btnOpenFile.GradientForeColor = GUI._ButttonFontColor

            Me.BtnImport.GradientBackColor = GUI._ButttonBackColor
            Me.BtnImport.GradientForeColor = GUI._ButttonFontColor

            Me.BtnClose.GradientBackColor = GUI._ButttonBackColor
            Me.BtnClose.GradientForeColor = GUI._ButttonFontColor


            Me.ResumeLayout()
        Catch Ex As Exception
            DisplayError.Show("-1", Ex.Message, "OtherSettings", mstrModuleName)
        End Try
    End Sub


    Private Sub SetLanguage()
        Try
            Me.Text = Language._Object.getCaption(Me.Name, Me.Text)

            Me.btnOpenFile.Text = Language._Object.getCaption(Me.btnOpenFile.Name, Me.btnOpenFile.Text)
            Me.lblSelectfile.Text = Language._Object.getCaption(Me.lblSelectfile.Name, Me.lblSelectfile.Text)
            Me.EZeeHeader1.Title = Language._Object.getCaption(Me.EZeeHeader1.Name & "_Title", Me.EZeeHeader1.Title)
            Me.EZeeHeader1.Message = Language._Object.getCaption(Me.EZeeHeader1.Name & "_Message", Me.EZeeHeader1.Message)
            Me.gbMasterList.Text = Language._Object.getCaption(Me.gbMasterList.Name, Me.gbMasterList.Text)
            Me.colhMasterList.Text = Language._Object.getCaption(CStr(Me.colhMasterList.Tag), Me.colhMasterList.Text)
            Me.colhTotalFields.Text = Language._Object.getCaption(CStr(Me.colhTotalFields.Tag), Me.colhTotalFields.Text)
            Me.colhIsImport.Text = Language._Object.getCaption(CStr(Me.colhIsImport.Tag), Me.colhIsImport.Text)
            Me.BtnImport.Text = Language._Object.getCaption(Me.BtnImport.Name, Me.BtnImport.Text)
            Me.BtnClose.Text = Language._Object.getCaption(Me.BtnClose.Name, Me.BtnClose.Text)

        Catch Ex As Exception
            DisplayError.Show("-1", Ex.Message, "SetLanguage", mstrModuleName)
        End Try
    End Sub


    Private Sub SetMessages()
        Try
            Language.setMessage(mstrModuleName, 1, "Please the select the correct file to Import Data from.")
            Language.setMessage(mstrModuleName, 2, "This field is already selected.Please Select New field.")
            Language.setMessage(mstrModuleName, 3, "Field is compulsory information.Please Select Field.")
            Language.setMessage(mstrModuleName, 4, "Please the select the correct field to Import Data.")
            Language.setMessage(mstrModuleName, 5, "Data successfully Imported.")
            Language.setMessage(mstrModuleName, 6, "Invalid Data.")
            Language.setMessage(mstrModuleName, 7, "Please check the selected file column '")
            Language.setMessage(mstrModuleName, 8, "Bank Group,Bank Branch Code,Bank Branch Name,Clearing. These Field is compulsory information.Please Select Bank Group,Bank Branch Code,Bank Branch Name,Clearing. These Field.")
            Language.setMessage(mstrModuleName, 9, "' should only be in 'True' Or 'False' Format. Please modify it and try to import again.")

        Catch Ex As Exception
            DisplayError.Show("-1", Ex.Message, "SetMessages", mstrModuleName)
        End Try
    End Sub
#End Region 'Language & UI Settings
    '</Language>
End Class