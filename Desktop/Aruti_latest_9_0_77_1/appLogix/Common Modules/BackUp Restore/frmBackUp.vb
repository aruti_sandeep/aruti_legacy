Imports Microsoft.SqlServer
Imports Microsoft.SqlServer.Management.Smo
Imports Microsoft.SqlServer.Management.Common
Imports eZeeCommonLib
Imports Aruti.Data
Imports Aruti.Data.Language

Public Class frmBackUp

#Region " Private Variables "
    Private ReadOnly mstrModuleName As String = "frmBackUp"
    Dim objServer As Server
    Private WithEvents objBackupDatabase As New eZeeDatabase
    Private blnIsConfiguration As Boolean = False
#End Region

#Region " Properties "
    Public WriteOnly Property _IsConfiguration() As Boolean
        Set(ByVal value As Boolean)
            blnIsConfiguration = value
        End Set
    End Property
#End Region

#Region " Private Function "
    Private Sub SetColor()
        Try
            txtBackupPath.BackColor = GUI.ColorDisplay
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "SetColor", mstrModuleName)
        End Try
    End Sub
#End Region

#Region " Form's Events "
    Private Sub frmBackUp_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles Me.KeyPress
        If Asc(e.KeyChar) = 13 Then
            Windows.Forms.SendKeys.Send("{Tab}")
            e.Handled = True
        ElseIf Asc(e.KeyChar) = 27 Then
            Call btnCancel.PerformClick()
        End If
    End Sub

    Private Sub frmBackUp_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Try
            Call Set_Logo(Me, gApplicationType)
            'Anjan (02 Sep 2011)-Start
            'Issue : Including Language Settings.
            Language.setLanguage(Me.Name)
            Call OtherSettings()
            'Anjan (02 Sep 2011)-End 
            Call SetColor()
            txtBackupPath.Text = ConfigParameter._Object._DatabaseExportPath
            'My.Computer.FileSystem.SpecialDirectories.Desktop
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmBackUp_Load", mstrModuleName)
        End Try
    End Sub

    'Anjan (02 Sep 2011)-Start
    'Issue : Including Language Settings.
    Private Sub Form_LanguageClick(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Me.LanguageClick
        Dim objfrm As New frmLanguage
        Try
            If User._Object._Isrighttoleft = True Then
                objfrm.RightToLeft = Windows.Forms.RightToLeft.Yes
                objfrm.RightToLeftLayout = True
                Call Language.ctlRightToLeftlayOut(objfrm)
            End If

            Call SetMessages()

            objfrm.displayDialog(Me)

            Call SetLanguage()

        Catch ex As System.Exception
            Call DisplayError.Show("-1", ex.Message, "Form_LanguageClick", mstrModuleName)
        Finally
            objfrm.Dispose()
            objfrm = Nothing
        End Try
    End Sub
    'Anjan (02 Sep 2011)-End 
#End Region

#Region " Buttons "
    Private Sub btnBrowse_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnBrowse.Click
        If fdbBackup.ShowDialog() = Windows.Forms.DialogResult.OK Then
            txtBackupPath.Text = fdbBackup.SelectedPath
        End If
    End Sub

    Private Sub btnCancel_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnCancel.Click
        Me.Close()
    End Sub

    Private Sub btnBackup_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnBackup.Click
        Dim strFilePath As String
        Dim objBackup As New clsBackup
        Try
            strFilePath = txtBackupPath.Text

            If Not System.IO.Directory.Exists(strFilePath) Then
                eZeeMsgBox.Show(Aruti.Data.Language.getMessage(mstrModuleName, 2, "Selected backup path is invalid. Please select valid path in order to perform Database Backup."))
                Exit Sub
            End If

            pbBackup.Visible = True
            pbBackup.Maximum = 100
            pbBackup.Style = ProgressBarStyle.Blocks
            pbBackup.Value = 0
            pbBackup.Step = 5

            Me.Cursor = Cursors.WaitCursor

            strFilePath = objBackupDatabase.Backup(strFilePath)

            If System.IO.File.Exists(strFilePath) Then
                strFilePath = System.IO.Path.GetFullPath(strFilePath)

                objBackup._Backup_Date = Today
                objBackup._Backup_Path = strFilePath
                objBackup._Companyunkid = Company._Object._Companyunkid
                objBackup._Isconfiguration = blnIsConfiguration
                objBackup._Userunkid = User._Object._Userunkid
                objBackup._Yearunkid = FinancialYear._Object._YearUnkid

                Call objBackup.Insert()
            End If
            Me.Cursor = Cursors.Default
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "btnBackup_Click", mstrModuleName)
        End Try
    End Sub
#End Region

#Region " Other Control's Events "

    Private Sub objDatabase_Completed(ByVal sender As Object, ByVal e As Microsoft.SqlServer.Management.Common.ServerMessageEventArgs) Handles objBackupDatabase.Completed
        eZeeMsgBox.Show(Aruti.Data.Language.getMessage(mstrModuleName, 1, "Congratualtions!! Backup of Database has been taken successfully. Please secure Database Backup file. It is recommended to move backup file to a secure location.")) '?1
        pbBackup.Value = 0
    End Sub

    Private Sub objDatabase_PercentComplete(ByVal sender As Object, ByVal e As Microsoft.SqlServer.Management.Smo.PercentCompleteEventArgs) Handles objBackupDatabase.PercentComplete
        pbBackup.Value = e.Percent
    End Sub

#End Region

    
	'<Language> This Auto Generated Text Please Do Not Modify it.
#Region " Language & UI Settings "
	Private Sub OtherSettings()
		Try
			Me.SuspendLayout()
			
			Call SetLanguage()
			
			Me.gbBackUp.GradientColor = GUI._eZeeContainerHeaderBackColor 
			Me.gbBackUp.ForeColor = GUI._eZeeContainerHeaderForeColor 


			Me.btnBrowse.GradientBackColor = GUI._ButttonBackColor 
			Me.btnBrowse.GradientForeColor = GUI._ButttonFontColor

			Me.btnBackup.GradientBackColor = GUI._ButttonBackColor 
			Me.btnBackup.GradientForeColor = GUI._ButttonFontColor

			Me.btnCancel.GradientBackColor = GUI._ButttonBackColor 
			Me.btnCancel.GradientForeColor = GUI._ButttonFontColor


			Me.ResumeLayout()
		Catch Ex As Exception
			DisplayError.Show("-1", ex.Message, "OtherSettings", mstrModuleName)
		End Try
	End Sub
			
			
	Private Sub SetLanguage()
		Try
			Me.Text = Language._Object.getCaption(Me.Name, Me.Text)
			
			Me.lblBackUp.Text = Language._Object.getCaption(Me.lblBackUp.Name, Me.lblBackUp.Text)
			Me.gbBackUp.Text = Language._Object.getCaption(Me.gbBackUp.Name, Me.gbBackUp.Text)
			Me.btnBrowse.Text = Language._Object.getCaption(Me.btnBrowse.Name, Me.btnBrowse.Text)
			Me.btnBackup.Text = Language._Object.getCaption(Me.btnBackup.Name, Me.btnBackup.Text)
			Me.btnCancel.Text = Language._Object.getCaption(Me.btnCancel.Name, Me.btnCancel.Text)
			Me.StatusStrip1.Text = Language._Object.getCaption(Me.StatusStrip1.Name, Me.StatusStrip1.Text)
			Me.pbBackup.Text = Language._Object.getCaption(Me.pbBackup.Name, Me.pbBackup.Text)

		Catch Ex As Exception
			DisplayError.Show("-1", ex.Message, "SetLanguage", mstrModuleName)
		End Try
	End Sub
			
			
	Private Sub SetMessages()
		Try
			Language.setMessage(mstrModuleName, 1, "Congratualtions!! Backup of Database has been taken successfully. Please secure Database Backup file. It is recommended to move backup file to a secure location.")
			Language.setMessage(mstrModuleName, 2, "Selected backup path is invalid. Please select valid path in order to perform Database Backup.")

		Catch Ex As Exception
			DisplayError.Show("-1", ex.Message, "SetMessages", mstrModuleName)
		End Try
	End Sub
#End Region 'Language & UI Settings
	'</Language>
End Class