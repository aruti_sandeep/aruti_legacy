﻿Option Strict On

#Region " Imports "

Imports eZeeCommonLib
Imports Aruti.Data

#End Region

Public Class frmAdvanceSearch

#Region " Private Variables "

    Private ReadOnly mstrModuleName As String = "frmAdvanceSearch"
    Private mstrFilterString As String = String.Empty
    Private mstrSelectedButton As String = String.Empty
    Private intGroupID As Integer = 0
    Private mstrButtonName As String = String.Empty
    Private mstrEmployeeTableAlias As String = "" 'Sohail (05 Dec 2011)
    'Sohail (01 Aug 2019) -- Start
    'NMB Payroll UAT # TC006 - 76.1 - Advance search for process payroll, payslip, etc. should have search option. Currently user must scroll the entire list to select an item.
    Private dvDetail As DataView
    Private mdtFilterInfo As DataTable
    Private dvFilterInfo As DataView
    Private mintCount As Integer = 0
    Private mstrSearchText As String = ""
    'Sohail (01 Aug 2019) -- End

    'Sohail (08 May 2015) -- Start
    'Enhancement - Get Advance Filter string from new employee transfer table and employee categorization table.
    Private mdtAsOnDate As DateTime = Nothing
    'Sohail (08 May 2015) -- End
    'Sohail (14 Nov 2019) -- Start
    'NMB UAT Enhancement # : Bind Allocation to training course.
    Private mintSelectedAllocationID As Integer = 0
    Private mdicAdvAlloc As New Dictionary(Of Integer, String)
    Private mblnShowSkill As Boolean = False
    'Sohail (14 Nov 2019) -- End

#End Region

#Region " Properties "

    Public ReadOnly Property _GetFilterString() As String
        Get
            Return mstrFilterString
        End Get
    End Property

    'Sohail (05 Dec 2011) -- Start
    Public WriteOnly Property _Hr_EmployeeTable_Alias() As String
        Set(ByVal value As String)
            mstrEmployeeTableAlias = value
        End Set
    End Property
    'Sohail (05 Dec 2011) -- End

    'Sohail (14 Nov 2019) -- Start
    'NMB UAT Enhancement # : Bind Allocation to training course.
    Public WriteOnly Property _ShowSkill() As Boolean
        Set(ByVal value As Boolean)
            mblnShowSkill = value
        End Set
    End Property

    Public Property _DataTable() As DataTable
        Get
            Dim dt As DataTable = Nothing
            If mdtFilterInfo IsNot Nothing Then
                dt = New DataView(mdtFilterInfo, "IsGroup = 0 ", "", DataViewRowState.CurrentRows).ToTable
                If dt.Columns.Contains("GroupId") = True Then
                    dt.Columns("GroupId").ColumnName = "advancefilterallocationid"
                End If
                If dt.Columns.Contains("Id") = True Then
                    dt.Columns("Id").ColumnName = "advancefilterallocationtranunkid"
                End If
                If dt.Columns.Contains("UnkId") = False Then
                    Dim dtCol As New DataColumn("UnkId", System.Type.GetType("System.Int32"))
                    dtCol.AllowDBNull = False
                    dtCol.DefaultValue = -1
                    dt.Columns.Add(dtCol)
                End If
            End If
            Return dt
        End Get
        Set(ByVal value As DataTable)
            mdtFilterInfo = value
            If mdtFilterInfo IsNot Nothing Then
                If mdtFilterInfo.Columns.Contains("advancefilterallocationid") = True Then
                    mdtFilterInfo.Columns("advancefilterallocationid").ColumnName = "GroupId"
                End If
                If mdtFilterInfo.Columns.Contains("advancefilterallocationtranunkid") = True Then
                    mdtFilterInfo.Columns("advancefilterallocationtranunkid").ColumnName = "Id"
                End If
            End If
        End Set
    End Property
    'Sohail (14 Nov 2019) -- End

#End Region

#Region " Form's Events "

    Private Sub frmAdvanceSearch_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Try
            Call Set_Logo(Me, gApplicationType)
            Language.setLanguage(Me.Name)
            Call OtherSettings()

            'Sohail (01 Aug 2019) -- Start
            'NMB Payroll UAT # TC006 - 76.1 - Advance search for process payroll, payslip, etc. should have search option. Currently user must scroll the entire list to select an item.
            'lvFilterInfo.GridLines = False
            Call CreateFilterInfoTable()
            'Sohail (01 Aug 2019) -- End

            'Sohail (14 Nov 2019) -- Start
            'NMB UAT Enhancement # : Bind Allocation to training course.
            btnSkill.Visible = mblnShowSkill
            Dim objMaster As New clsMasterData
            Dim ds As DataSet = objMaster.GetAdvanceFilterAllocation("List", True)
            mdicAdvAlloc = (From p In ds.Tables(0) Select New With {Key .Id = CInt(p.Item("Id")), Key .Name = p.Item("Name").ToString}).ToDictionary(Function(x) x.Id, Function(y) y.Name)
            'Sohail (14 Nov 2019) -- End

            'Sohail (05 Dec 2011) -- Start
            If mstrEmployeeTableAlias.Trim.Length > 0 Then mstrEmployeeTableAlias = mstrEmployeeTableAlias & "."
            'Sohail (05 Dec 2011) -- End
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmAdvanceSearch_Load", mstrModuleName)
        Finally
        End Try
    End Sub

    'Anjan (02 Sep 2011)-Start
    'Issue : Including Language Settings.
    Private Sub Form_LanguageClick(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Me.LanguageClick
        Dim objfrm As New frmLanguage
        Try
            If User._Object._Isrighttoleft = True Then
                objfrm.RightToLeft = Windows.Forms.RightToLeft.Yes
                objfrm.RightToLeftLayout = True
                Call Language.ctlRightToLeftlayOut(objfrm)
            End If

            Call SetMessages()

            objfrm.displayDialog(Me)

            Call SetLanguage()

        Catch ex As System.Exception
            Call DisplayError.Show("-1", ex.Message, "Form_LanguageClick", mstrModuleName)
        Finally
            objfrm.Dispose()
            objfrm = Nothing
        End Try
    End Sub
    'Anjan (02 Sep 2011)-End 


#End Region

#Region " Private Methods "

    Private Sub FillDetailList(ByVal StrSelectedButton As String)
        Dim dsList As New DataSet
        'Sohail (01 Aug 2019) -- Start
        'NMB Payroll UAT # TC006 - 76.1 - Advance search for process payroll, payslip, etc. should have search option. Currently user must scroll the entire list to select an item.
        Dim intColType As Integer = 0
        'Sohail (01 Aug 2019) -- End
        Try
            'Sohail (01 Aug 2019) -- Start
            'NMB Payroll UAT # TC006 - 76.1 - Advance search for process payroll, payslip, etc. should have search option. Currently user must scroll the entire list to select an item.
            'RemoveHandler lvDetails.ItemChecked, AddressOf lvDetails_ItemChecked
            'RemoveHandler objchkACheck.CheckedChanged, AddressOf objchkACheck_CheckedChanged

            'objchkACheck.CheckState = CheckState.Unchecked
            mintCount = 0
            dgDetail.DataSource = Nothing
            RemoveHandler txtSearch.TextChanged, AddressOf txtSearch_TextChanged
            Call SetDefaultSearchEmpText()
            AddHandler txtSearch.TextChanged, AddressOf txtSearch_TextChanged
            'Sohail (01 Aug 2019) -- End

            Select Case StrSelectedButton.ToUpper
                Case "BTNBRANCH"
                    Dim objBranch As New clsStation
                    dsList = objBranch.GetList("List", True)
                    mstrSelectedButton = btnBranch.Text
                    mstrButtonName = btnBranch.Name
                    'Sohail (14 Nov 2019) -- Start
                    'NMB UAT Enhancement # : Bind Allocation to training course.
                    mintSelectedAllocationID = enAdvanceFilterAllocation.BRANCH
                    'Sohail (14 Nov 2019) -- End
                Case "BTNDEPTGRP"
                    Dim objDeptGrp As New clsDepartmentGroup
                    dsList = objDeptGrp.GetList("List", True)
                    mstrSelectedButton = btnDeptGrp.Text
                    mstrButtonName = btnDeptGrp.Name
                    'Sohail (14 Nov 2019) -- Start
                    'NMB UAT Enhancement # : Bind Allocation to training course.
                    mintSelectedAllocationID = enAdvanceFilterAllocation.DEPARTMENT_GROUP
                    'Sohail (14 Nov 2019) -- End
                Case "BTNDEPT"
                    Dim objDept As New clsDepartment
                    dsList = objDept.GetList("List", True)
                    mstrSelectedButton = btnDept.Text
                    mstrButtonName = btnDept.Name
                    'Sohail (14 Nov 2019) -- Start
                    'NMB UAT Enhancement # : Bind Allocation to training course.
                    mintSelectedAllocationID = enAdvanceFilterAllocation.DEPARTMENT
                    'Sohail (14 Nov 2019) -- End
                Case "BTNSECTION"
                    Dim objSection As New clsSections
                    dsList = objSection.GetList("List", True)
                    mstrSelectedButton = btnSection.Text
                    mstrButtonName = btnSection.Name
                    'Sohail (14 Nov 2019) -- Start
                    'NMB UAT Enhancement # : Bind Allocation to training course.
                    mintSelectedAllocationID = enAdvanceFilterAllocation.SECTION
                    'Sohail (14 Nov 2019) -- End
                Case "BTNUNIT"
                    Dim objUnit As New clsUnits
                    dsList = objUnit.GetList("List", True)
                    mstrSelectedButton = btnUnit.Text
                    mstrButtonName = btnUnit.Name
                    'Sohail (14 Nov 2019) -- Start
                    'NMB UAT Enhancement # : Bind Allocation to training course.
                    mintSelectedAllocationID = enAdvanceFilterAllocation.UNIT
                    'Sohail (14 Nov 2019) -- End
                Case "BTNJOB"
                    Dim objJobs As New clsJobs
                    dsList = objJobs.GetList("List", True)
                    mstrSelectedButton = btnJob.Text
                    mstrButtonName = btnJob.Name
                    'Sohail (01 Aug 2019) -- Start
                    'NMB Payroll UAT # TC006 - 76.1 - Advance search for process payroll, payslip, etc. should have search option. Currently user must scroll the entire list to select an item.
                    intColType = 1
                    'Sohail (01 Aug 2019) -- End
                    'Sohail (14 Nov 2019) -- Start
                    'NMB UAT Enhancement # : Bind Allocation to training course.
                    mintSelectedAllocationID = enAdvanceFilterAllocation.JOBS
                    'Sohail (14 Nov 2019) -- End
                Case "BTNSECTIONGROUP"
                    Dim objSG As New clsSectionGroup
                    dsList = objSG.GetList("List", True)
                    mstrSelectedButton = btnSectionGroup.Text
                    mstrButtonName = btnSectionGroup.Name
                    'Sohail (14 Nov 2019) -- Start
                    'NMB UAT Enhancement # : Bind Allocation to training course.
                    mintSelectedAllocationID = enAdvanceFilterAllocation.SECTION_GROUP
                    'Sohail (14 Nov 2019) -- End
                Case "BTNUNITGROUP"
                    Dim objUG As New clsUnitGroup
                    dsList = objUG.GetList("List", True)
                    mstrSelectedButton = btnUnitGroup.Text
                    mstrButtonName = btnUnitGroup.Name
                    'Sohail (14 Nov 2019) -- Start
                    'NMB UAT Enhancement # : Bind Allocation to training course.
                    mintSelectedAllocationID = enAdvanceFilterAllocation.UNIT_GROUP
                    'Sohail (14 Nov 2019) -- End
                Case "BTNTEAM"
                    Dim objTeam As New clsTeams
                    dsList = objTeam.GetList("List", True)
                    mstrSelectedButton = btnTeam.Text
                    mstrButtonName = btnTeam.Name
                    'Sohail (14 Nov 2019) -- Start
                    'NMB UAT Enhancement # : Bind Allocation to training course.
                    mintSelectedAllocationID = enAdvanceFilterAllocation.TEAM
                    'Sohail (14 Nov 2019) -- End
                Case "BTNJOBGROUP"
                    Dim objjobGRP As New clsJobGroup
                    dsList = objjobGRP.GetList("List", True)
                    mstrSelectedButton = btnJobGroup.Text
                    mstrButtonName = btnJobGroup.Name
                    'Sohail (14 Nov 2019) -- Start
                    'NMB UAT Enhancement # : Bind Allocation to training course.
                    mintSelectedAllocationID = enAdvanceFilterAllocation.JOB_GROUP
                    'Sohail (14 Nov 2019) -- End
                Case "BTNCLASSGROUP"
                    Dim objClassGrp As New clsClassGroup
                    dsList = objClassGrp.GetList("List", True)
                    mstrSelectedButton = btnClassGroup.Text
                    mstrButtonName = btnClassGroup.Name
                    'Sohail (14 Nov 2019) -- Start
                    'NMB UAT Enhancement # : Bind Allocation to training course.
                    mintSelectedAllocationID = enAdvanceFilterAllocation.CLASS_GROUP
                    'Sohail (14 Nov 2019) -- End
                Case "BTNCLASS"
                    Dim objClass As New clsClass
                    dsList = objClass.GetList("List", True)
                    mstrSelectedButton = btnClass.Text
                    mstrButtonName = btnClass.Name
                    'Sohail (14 Nov 2019) -- Start
                    'NMB UAT Enhancement # : Bind Allocation to training course.
                    mintSelectedAllocationID = enAdvanceFilterAllocation.CLASSES
                    'Sohail (14 Nov 2019) -- End
                Case "BTNGRADEGROUP"
                    Dim objGradeGrp As New clsGradeGroup
                    dsList = objGradeGrp.GetList("List", True)
                    mstrSelectedButton = btnGradeGroup.Text
                    mstrButtonName = btnGradeGroup.Name
                    'Sohail (14 Nov 2019) -- Start
                    'NMB UAT Enhancement # : Bind Allocation to training course.
                    mintSelectedAllocationID = enAdvanceFilterAllocation.GRADE_GROUP
                    'Sohail (14 Nov 2019) -- End
                Case "BTNGRADE"
                    Dim objGrade As New clsGrade
                    dsList = objGrade.GetList("List", True)
                    mstrSelectedButton = btnGrade.Text
                    mstrButtonName = btnGrade.Name
                    'Sohail (14 Nov 2019) -- Start
                    'NMB UAT Enhancement # : Bind Allocation to training course.
                    mintSelectedAllocationID = enAdvanceFilterAllocation.GRADE
                    'Sohail (14 Nov 2019) -- End
                Case "BTNGRADELEVEL"
                    Dim objGradeLvl As New clsGradeLevel
                    dsList = objGradeLvl.GetList("List", True)
                    mstrSelectedButton = btnGradeLevel.Text
                    mstrButtonName = btnGradeLevel.Name
                    'Sohail (14 Nov 2019) -- Start
                    'NMB UAT Enhancement # : Bind Allocation to training course.
                    mintSelectedAllocationID = enAdvanceFilterAllocation.GRADE_LEVEL
                    'Sohail (14 Nov 2019) -- End
                    'S.SANDEEP [ 22 MAR 2013 ] -- START
                    'ENHANCEMENT : TRA CHANGES
                Case "BTNCOSTCENTERGROUP"
                    Dim objCCG As New clspayrollgroup_master
                    dsList = objCCG.getListForCombo(enPayrollGroupType.CostCenter, "List")
                    mstrSelectedButton = btnCostCenterGroup.Text
                    mstrButtonName = btnCostCenterGroup.Name
                    'Sohail (14 Nov 2019) -- Start
                    'NMB UAT Enhancement # : Bind Allocation to training course.
                    mintSelectedAllocationID = enAdvanceFilterAllocation.COST_CENTER_GROUP
                    'Sohail (14 Nov 2019) -- End
                Case "BTNCOSTCENTER"
                    Dim objConstCenter As New clscostcenter_master
                    dsList = objConstCenter.GetList("List", True)
                    mstrSelectedButton = btnCostCenter.Text
                    mstrButtonName = btnCostCenter.Name
                    'S.SANDEEP [ 22 MAR 2013 ] -- END
                    'Sohail (01 Aug 2019) -- Start
                    'NMB Payroll UAT # TC006 - 76.1 - Advance search for process payroll, payslip, etc. should have search option. Currently user must scroll the entire list to select an item.
                    intColType = 2
                    'Sohail (01 Aug 2019) -- End
                    'Sohail (14 Nov 2019) -- Start
                    'NMB UAT Enhancement # : Bind Allocation to training course.
                    mintSelectedAllocationID = enAdvanceFilterAllocation.COST_CENTER
                    'Sohail (14 Nov 2019) -- End
                    'Pinkal (15-Sep-2013) -- Start
                    'Enhancement : TRA Changes

                Case "BTNEMPLOYEMENTTYPE"
                    Dim objEmpType As New clsCommon_Master
                    dsList = objEmpType.GetList(clsCommon_Master.enCommonMaster.EMPLOYEMENT_TYPE, "List", -1, True)
                    mstrSelectedButton = btnEmployementType.Text
                    mstrButtonName = btnEmployementType.Name
                    'Sohail (14 Nov 2019) -- Start
                    'NMB UAT Enhancement # : Bind Allocation to training course.
                    mintSelectedAllocationID = enAdvanceFilterAllocation.EMPLOYEMENT_TYPE
                    'Sohail (14 Nov 2019) -- End

                Case "BTNRELIGION"
                    Dim objReligion As New clsCommon_Master
                    dsList = objReligion.GetList(clsCommon_Master.enCommonMaster.RELIGION, "List", -1, True)
                    mstrSelectedButton = btnReligion.Text
                    mstrButtonName = btnReligion.Name
                    'Sohail (14 Nov 2019) -- Start
                    'NMB UAT Enhancement # : Bind Allocation to training course.
                    mintSelectedAllocationID = enAdvanceFilterAllocation.RELIGION
                    'Sohail (14 Nov 2019) -- End
                    'Pinkal (15-Sep-2013) -- End

                    'Sohail (09 Nov 2013) -- Start
                    'TRA - ENHANCEMENT
                Case btnGender.Name.ToUpper
                    Dim objGender As New clsMasterData
                    dsList = objGender.getGenderList("Gender")
                    mstrSelectedButton = btnGender.Text
                    mstrButtonName = btnGender.Name
                    'Sohail (01 Aug 2019) -- Start
                    'NMB Payroll UAT # TC006 - 76.1 - Advance search for process payroll, payslip, etc. should have search option. Currently user must scroll the entire list to select an item.
                    intColType = 3
                    'Sohail (01 Aug 2019) -- End
                    'Sohail (14 Nov 2019) -- Start
                    'NMB UAT Enhancement # : Bind Allocation to training course.
                    mintSelectedAllocationID = enAdvanceFilterAllocation.GENDER
                    'Sohail (14 Nov 2019) -- End

                Case btnNationality.Name.ToUpper
                    Dim objNationality As New clsMasterData
                    dsList = objNationality.getCountryList("Nationality", )
                    mstrSelectedButton = btnNationality.Text
                    mstrButtonName = btnNationality.Name
                    'Sohail (09 Nov 2013) -- End
                    'Sohail (01 Aug 2019) -- Start
                    'NMB Payroll UAT # TC006 - 76.1 - Advance search for process payroll, payslip, etc. should have search option. Currently user must scroll the entire list to select an item.
                    intColType = 4
                    'Sohail (01 Aug 2019) -- End
                    'Sohail (14 Nov 2019) -- Start
                    'NMB UAT Enhancement # : Bind Allocation to training course.
                    mintSelectedAllocationID = enAdvanceFilterAllocation.NATIONALITY
                    'Sohail (14 Nov 2019) -- End

                    'Sohail (02 Jul 2014) -- Start
                    'Enhancement - Pay Type and Pay Point allocation on Advance Filter.
                Case btnPayType.Name.ToUpper
                    Dim objPayType As New clsCommon_Master
                    dsList = objPayType.GetList(clsCommon_Master.enCommonMaster.PAY_TYPE, "List", -1, True)
                    mstrSelectedButton = btnPayType.Text
                    mstrButtonName = btnPayType.Name
                    'Sohail (14 Nov 2019) -- Start
                    'NMB UAT Enhancement # : Bind Allocation to training course.
                    mintSelectedAllocationID = enAdvanceFilterAllocation.PAY_TYPE
                    'Sohail (14 Nov 2019) -- End

                Case btnPayPoint.Name.ToUpper
                    Dim objPayPoint As New clspaypoint_master
                    dsList = objPayPoint.GetList("List", True)
                    mstrSelectedButton = btnPayPoint.Text
                    mstrButtonName = btnPayPoint.Name
                    'Sohail (02 Jul 2014) -- End
                    'Sohail (01 Aug 2019) -- Start
                    'NMB Payroll UAT # TC006 - 76.1 - Advance search for process payroll, payslip, etc. should have search option. Currently user must scroll the entire list to select an item.
                    intColType = 5
                    'Sohail (01 Aug 2019) -- End
                    'Sohail (14 Nov 2019) -- Start
                    'NMB UAT Enhancement # : Bind Allocation to training course.
                    mintSelectedAllocationID = enAdvanceFilterAllocation.PAY_POINT
                    'Sohail (14 Nov 2019) -- End

                    'Pinkal (01-Oct-2014) -- Start
                    'Enhancement -  Changes For FDRC Report

                Case btnMaritalStatus.Name.ToUpper
                    Dim objCommonMst As New clsCommon_Master
                    dsList = objCommonMst.GetList(clsCommon_Master.enCommonMaster.MARRIED_STATUS, "List", -1, True)
                    mstrSelectedButton = btnMaritalStatus.Text
                    mstrButtonName = btnMaritalStatus.Name
                    'Pinkal (01-Oct-2014) -- End
                    'Sohail (01 Aug 2019) -- Start
                    'NMB Payroll UAT # TC006 - 76.1 - Advance search for process payroll, payslip, etc. should have search option. Currently user must scroll the entire list to select an item.
                    intColType = 6
                    'Sohail (01 Aug 2019) -- End
                    'Sohail (14 Nov 2019) -- Start
                    'NMB UAT Enhancement # : Bind Allocation to training course.
                    mintSelectedAllocationID = enAdvanceFilterAllocation.MARITAL_STATUS
                    'Sohail (14 Nov 2019) -- End

                    'S.SANDEEP |04-JUN-2019| -- START
                    'ISSUE/ENHANCEMENT : [Relation filters on Allocation Advance search]
                Case btnRelationFromDpndt.Name.ToUpper()
                    Dim objCommonMst As New clsCommon_Master
                    dsList = objCommonMst.GetList(clsCommon_Master.enCommonMaster.RELATIONS, "List", -1, True)
                    mstrSelectedButton = btnRelationFromDpndt.Text
                    mstrButtonName = btnRelationFromDpndt.Name
                    'S.SANDEEP |04-JUN-2019| -- END
                    'Sohail (01 Aug 2019) -- Start
                    'NMB Payroll UAT # TC006 - 76.1 - Advance search for process payroll, payslip, etc. should have search option. Currently user must scroll the entire list to select an item.
                    intColType = 7
                    'Sohail (01 Aug 2019) -- End
                    'Sohail (14 Nov 2019) -- Start
                    'NMB UAT Enhancement # : Bind Allocation to training course.
                    mintSelectedAllocationID = enAdvanceFilterAllocation.RELATION_FROM_DEPENDENTS

                Case btnSkill.Name.ToUpper
                    Dim objSkill As New clsskill_master
                    dsList = objSkill.getComboList("List", False)
                    mstrSelectedButton = btnSkill.Text
                    mstrButtonName = btnSkill.Name
                    mintSelectedAllocationID = enAdvanceFilterAllocation.SKILL
                    objSkill = Nothing
                    intColType = 8
                    'Sohail (14 Nov 2019) -- End
            End Select

            'Sohail (01 Aug 2019) -- Start
            'NMB Payroll UAT # TC006 - 76.1 - Advance search for process payroll, payslip, etc. should have search option. Currently user must scroll the entire list to select an item.
            'lvDetails.Items.Clear()
            'Sohail (01 Aug 2019) -- End

            For Each dtRow As DataRow In dsList.Tables(0).Rows
                Dim lvItem As New ListViewItem

                lvItem.Text = ""

                Select Case StrSelectedButton.ToUpper
                    Case "BTNBRANCH"
                        lvItem.SubItems.Add(dtRow.Item("name").ToString)
                        lvItem.Tag = dtRow.Item("stationunkid")
                    Case "BTNDEPTGRP"
                        lvItem.SubItems.Add(dtRow.Item("name").ToString)
                        lvItem.Tag = dtRow.Item("deptgroupunkid")
                    Case "BTNDEPT"
                        lvItem.SubItems.Add(dtRow.Item("name").ToString)
                        lvItem.Tag = dtRow.Item("departmentunkid")
                    Case "BTNSECTION"
                        lvItem.SubItems.Add(dtRow.Item("name").ToString)
                        lvItem.Tag = dtRow.Item("sectionunkid")
                    Case "BTNUNIT"
                        lvItem.SubItems.Add(dtRow.Item("name").ToString)
                        lvItem.Tag = dtRow.Item("unitunkid")
                    Case "BTNJOB"
                        lvItem.SubItems.Add(dtRow.Item("JobName").ToString)
                        lvItem.Tag = dtRow.Item("jobunkid")
                    Case "BTNSECTIONGROUP"
                        lvItem.SubItems.Add(dtRow.Item("name").ToString)
                        lvItem.Tag = dtRow.Item("sectiongroupunkid")
                    Case "BTNUNITGROUP"
                        lvItem.SubItems.Add(dtRow.Item("name").ToString)
                        lvItem.Tag = dtRow.Item("unitgroupunkid")
                    Case "BTNTEAM"
                        lvItem.SubItems.Add(dtRow.Item("name").ToString)
                        lvItem.Tag = dtRow.Item("teamunkid")

                    Case "BTNJOBGROUP"
                        lvItem.SubItems.Add(dtRow.Item("name").ToString)
                        lvItem.Tag = dtRow.Item("jobgroupunkid")
                    Case "BTNCLASSGROUP"
                        lvItem.SubItems.Add(dtRow.Item("name").ToString)
                        lvItem.Tag = dtRow.Item("classgroupunkid")
                    Case "BTNCLASS"
                        lvItem.SubItems.Add(dtRow.Item("name").ToString)
                        lvItem.Tag = dtRow.Item("classesunkid")
                    Case "BTNGRADEGROUP"
                        lvItem.SubItems.Add(dtRow.Item("name").ToString)
                        lvItem.Tag = dtRow.Item("gradegroupunkid")
                    Case "BTNGRADE"
                        lvItem.SubItems.Add(dtRow.Item("name").ToString)
                        lvItem.Tag = dtRow.Item("gradeunkid")
                    Case "BTNGRADELEVEL"
                        lvItem.SubItems.Add(dtRow.Item("name").ToString)
                        lvItem.Tag = dtRow.Item("gradelevelunkid")
                        'S.SANDEEP [ 22 MAR 2013 ] -- START
                        'ENHANCEMENT : TRA CHANGES
                    Case "BTNCOSTCENTERGROUP"
                        lvItem.SubItems.Add(dtRow.Item("name").ToString)
                        lvItem.Tag = dtRow.Item("groupmasterunkid")
                    Case "BTNCOSTCENTER"
                        lvItem.SubItems.Add(dtRow.Item("costcentername").ToString)
                        lvItem.Tag = dtRow.Item("costcenterunkid")
                        'S.SANDEEP [ 22 MAR 2013 ] -- END


                        'Pinkal (15-Sep-2013) -- Start
                        'Enhancement : TRA Changes

                    Case "BTNEMPLOYEMENTTYPE", "BTNRELIGION"
                        lvItem.SubItems.Add(dtRow.Item("name").ToString)
                        lvItem.Tag = dtRow.Item("masterunkid")

                        'Pinkal (15-Sep-2013) -- End

                        'Sohail (09 Nov 2013) -- Start
                        'TRA - ENHANCEMENT
                    Case btnGender.Name.ToUpper
                        lvItem.SubItems.Add(dtRow.Item("Name").ToString)
                        lvItem.Tag = dtRow.Item("Id")

                    Case btnNationality.Name.ToUpper
                        lvItem.SubItems.Add(dtRow.Item("country_name").ToString)
                        lvItem.Tag = dtRow.Item("countryunkid")
                        'Sohail (09 Nov 2013) -- End

                        'Sohail (02 Jul 2014) -- Start
                        'Enhancement - Pay Type and Pay Point allocation on Advance Filter.
                    Case btnPayType.Name.ToUpper
                        lvItem.SubItems.Add(dtRow.Item("name").ToString)
                        lvItem.Tag = dtRow.Item("masterunkid")

                    Case btnPayPoint.Name.ToUpper
                        lvItem.SubItems.Add(dtRow.Item("paypointname").ToString)
                        lvItem.Tag = dtRow.Item("paypointunkid")
                        'Sohail (02 Jul 2014) -- End


                        'Pinkal (01-Oct-2014) -- Start
                        'Enhancement -  Changes For FDRC Report
                    Case btnMaritalStatus.Name.ToUpper
                        lvItem.SubItems.Add(dtRow.Item("name").ToString)
                        lvItem.Tag = dtRow.Item("masterunkid")
                        'Pinkal (01-Oct-2014) -- End

                        'S.SANDEEP |04-JUN-2019| -- START
                        'ISSUE/ENHANCEMENT : [Relation filters on Allocation Advance search]
                    Case btnRelationFromDpndt.Name.ToUpper
                        lvItem.SubItems.Add(dtRow.Item("name").ToString)
                        lvItem.Tag = dtRow.Item("masterunkid")
                        'S.SANDEEP |04-JUN-2019| -- END

                        'Sohail (14 Nov 2019) -- Start
                        'NMB UAT Enhancement # : Bind Allocation to training course.
                    Case btnSkill.Name.ToUpper
                        lvItem.SubItems.Add(dtRow.Item("name").ToString)
                        lvItem.Tag = dtRow.Item("skillunkid")
                        'Sohail (14 Nov 2019) -- End

                End Select


                'Sohail (04 Jan 2014) -- Start
                'Enhancement - Advance Search in Self Service
                'If lvFilterInfo.Items.Count > 0 Then
                '    Dim mItem As ListViewItem = lvFilterInfo.FindItemWithText(lvItem.SubItems(colhDescription.Index).Text)
                '    If mItem IsNot Nothing Then
                '        If mItem.SubItems(objcolhGroup.Index).Text = mstrSelectedButton Then
                '            lvItem.Checked = True
                '        End If
                '    End If

                'End If
                'lvItem.SubItems.Add(mstrSelectedButton)
                'Sohail (01 Aug 2019) -- Start
                'NMB Payroll UAT # TC006 - 76.1 - Advance search for process payroll, payslip, etc. should have search option. Currently user must scroll the entire list to select an item.
                'If lvFilterInfo.Items.Count > 0 Then
                '    Dim lstSelected As List(Of ListViewItem) = (From p In lvFilterInfo.Items.Cast(Of ListViewItem)() Where p.SubItems(colhFilterValue.Index).Text = lvItem.SubItems(colhDescription.Index).Text AndAlso p.SubItems(objcolhGroup.Index).Text = mstrSelectedButton Select (p)).ToList
                '    If lstSelected.Count > 0 Then
                '        lvItem.Checked = True
                '    End If
                'End If
                'Sohail (01 Aug 2019) -- End
                'Sohail (04 Jan 2014) -- End

                'lvDetails.Items.Add(lvItem) 'Sohail (01 Aug 2019)
            Next

            'Sohail (01 Aug 2019) -- Start
            'NMB Payroll UAT # TC006 - 76.1 - Advance search for process payroll, payslip, etc. should have search option. Currently user must scroll the entire list to select an item.
            'If lvFilterInfo.Items.Count > 0 Then
            '    If lvDetails.CheckedItems.Count = dsList.Tables(0).Rows.Count Then
            '        objchkACheck.CheckState = CheckState.Checked
            '    End If
            'End If

            'AddHandler lvDetails.ItemChecked, AddressOf lvDetails_ItemChecked
            'AddHandler objchkACheck.CheckedChanged, AddressOf objchkACheck_CheckedChanged
            If intColType = 0 Then
                dsList.Tables(0).Columns(0).ColumnName = "Id"
                dsList.Tables(0).Columns("name").ColumnName = "Name"
            ElseIf intColType = 1 Then
                dsList.Tables(0).Columns("jobunkid").ColumnName = "Id"
                dsList.Tables(0).Columns("jobname").ColumnName = "Name"
            ElseIf intColType = 2 Then
                dsList.Tables(0).Columns(0).ColumnName = "Id"
                dsList.Tables(0).Columns("costcentername").ColumnName = "Name"
            ElseIf intColType = 3 Then
                dsList.Tables(0).Columns(1).ColumnName = "Id"
                dsList.Tables(0).Columns(0).ColumnName = "Name"
            ElseIf intColType = 4 Then
                dsList.Tables(0).Columns(0).ColumnName = "Id"
                dsList.Tables(0).Columns("country_name").ColumnName = "Name"
            ElseIf intColType = 5 Then
                dsList.Tables(0).Columns(0).ColumnName = "Id"
                dsList.Tables(0).Columns("paypointname").ColumnName = "Name"

            ElseIf intColType = 6 Then
                dsList.Tables(0).Columns("masterunkid").ColumnName = "Id"
                dsList.Tables(0).Columns("name").ColumnName = "Name"
            ElseIf intColType = 7 Then
                dsList.Tables(0).Columns("masterunkid").ColumnName = "Id"
                dsList.Tables(0).Columns("name").ColumnName = "Name"
                'Sohail (14 Nov 2019) -- Start
                'NMB UAT Enhancement # : Bind Allocation to training course.
            ElseIf intColType = 8 Then
                dsList.Tables(0).Columns("skillunkid").ColumnName = "Id"
                dsList.Tables(0).Columns("name").ColumnName = "Name"
                'Sohail (14 Nov 2019) -- End
            End If

            If dsList.Tables(0).Columns.Contains("IsChecked") = False Then
                Dim dtCol As DataColumn
                dtCol = New DataColumn("IsChecked", System.Type.GetType("System.Boolean"))
                dtCol.DefaultValue = False
                dtCol.AllowDBNull = False
                dsList.Tables(0).Columns.Add(dtCol)
            End If

            If mdtFilterInfo IsNot Nothing Then
                'Sohail (14 Nov 2019) -- Start
                'NMB UAT Enhancement # : Bind Allocation to training course.
                'Dim allID As List(Of String) = (From p In mdtFilterInfo Where p.Item("GroupName").ToString = mstrSelectedButton Select (p.Item("Id").ToString)).ToList
                Dim allID As List(Of String) = (From p In mdtFilterInfo Where CInt(p.Item("GroupId")) = mintSelectedAllocationID Select (p.Item("Id").ToString)).ToList
                'Sohail (14 Nov 2019) -- End
                Dim strIDs As String = String.Join(",", allID.ToArray)
                If strIDs.Trim <> "" Then
                    Dim dRow() As DataRow = dsList.Tables(0).Select("Id IN (" & strIDs.Trim & ") ")
                    For Each drRow As DataRow In dRow
                        drRow.Item("IsChecked") = True
                    Next
                    'Sohail (14 Nov 2019) -- Start
                    'NMB UAT Enhancement # : Bind Allocation to training course.
                    'dvDetail.Table.AcceptChanges()
                    dsList.Tables(0).AcceptChanges()
                    'Sohail (14 Nov 2019) -- End
                End If

            End If

            dvDetail = dsList.Tables(0).DefaultView

            dgDetail.AutoGenerateColumns = False
            objdgcolhCheck.DataPropertyName = "IsChecked"
            objdgcolhUnkid.DataPropertyName = "Id"
            dgColhDescription.DataPropertyName = "Name"

            dgDetail.DataSource = dvDetail
            dvDetail.Sort = "IsChecked DESC, Name"
            'Sohail (01 Aug 2019) -- End

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "FillDetailList", mstrModuleName)
        Finally
            dsList.Dispose()
        End Try
    End Sub

    'Sohail (01 Aug 2019) -- Start
    'NMB Payroll UAT # TC006 - 76.1 - Advance search for process payroll, payslip, etc. should have search option. Currently user must scroll the entire list to select an item.
    'Private Sub DoItemOperation(ByVal Item As ListViewItem)
    '    If Item.Checked Then
    '        Dim lvItem As New ListViewItem
    '        Dim StrName As String = ""

    '        'Sohail (04 Jan 2014) -- Start
    '        'Enhancement - Advance Search in Self Service
    '        'lvItem = lvFilterInfo.FindItemWithText(Item.SubItems(colhDescription.Index).Text)

    '        'If lvItem IsNot Nothing Then
    '        '    If Item.SubItems(colhDescription.Index).Text.Trim <> lvItem.Text.Trim Then
    '        '        lvItem = Nothing
    '        '    End If
    '        'End If
    '        'Dim lstSelected As List(Of ListViewItem) = (From p In lvFilterInfo.Items.Cast(Of ListViewItem)() Where p.SubItems(colhFilterValue.Index).Text = Item.SubItems(colhDescription.Index).Text AndAlso p.SubItems(objcolhGroup.Index).Text = Item.SubItems(objdetailcolhGroup.Index).Text Select (p)).ToList
    '        'Sohail (04 Jan 2014) -- End

    '        'Sohail (04 Jan 2014) -- Start
    '        'Enhancement - Advance Search in Self Service
    '        'If lvItem Is Nothing Then
    '        If Item.Checked = True Then
    '            'Sohail (04 Jan 2014) -- End

    '            lvItem = New ListViewItem
    '            lvItem.Tag = Item.Tag
    '            lvItem.Text = Item.SubItems(colhDescription.Index).Text
    '            lvItem.SubItems.Add(mstrSelectedButton)

    '            Dim GrplvItem As ListViewItem = lvFilterInfo.FindItemWithText(mstrSelectedButton)

    '            If GrplvItem Is Nothing Then
    '                intGroupID += 1
    '                lvItem.SubItems.Add(intGroupID.ToString())
    '            Else

    '                If GrplvItem.SubItems(objcolhGroup.Index).Text <> mstrSelectedButton Then
    '                    intGroupID += 1
    '                    lvItem.SubItems.Add(intGroupID.ToString())
    '                Else
    '                    Dim GroupID As Integer = CInt(GrplvItem.SubItems(objcolhGroupID.Index).Text)
    '                    lvItem.SubItems.Add(GroupID.ToString())
    '                End If

    '            End If

    '            lvItem.SubItems.Add(mstrButtonName)

    '            lvFilterInfo.Items.Add(lvItem)
    '            lvFilterInfo.GroupingColumn = objcolhGroup
    '            lvFilterInfo.GridLines = False
    '            lvFilterInfo.DisplayGroups(True)
    '            lvFilterInfo.SortBy(objcolhGroup.Index, SortOrder.Ascending)

    '            If lvFilterInfo.Items.Count > 10 Then
    '                colhFilterValue.Width = 340 - 20
    '            Else
    '                colhFilterValue.Width = 340
    '            End If

    '        End If

    '    ElseIf Item.Checked = False Then

    '        'Sohail (04 Jan 2014) -- Start
    '        'Enhancement - Advance Search in Self Service
    '        'If lvFilterInfo.Items.Count > 0 Then
    '        '    Dim lvItem As New ListViewItem
    '        '    lvItem = lvFilterInfo.FindItemWithText(Item.SubItems(colhDescription.Index).Text)
    '        '    If lvItem IsNot Nothing Then
    '        '        If lvItem.SubItems(objcolhGroup.Index).Text = mstrSelectedButton Then
    '        '            lvFilterInfo.Items.Remove(lvItem)
    '        '        End If
    '        '    End If
    '        'End If
    '        If lvFilterInfo.Items.Count > 0 Then
    '            Dim lstSelected As List(Of ListViewItem) = (From p In lvFilterInfo.Items.Cast(Of ListViewItem)() Where p.SubItems(colhFilterValue.Index).Text = Item.SubItems(colhDescription.Index).Text AndAlso p.SubItems(objcolhGroup.Index).Text = mstrSelectedButton Select (p)).ToList
    '            If lstSelected.Count > 0 Then
    '                lvFilterInfo.Items.Remove(lstSelected.Item(0))
    '            End If
    '        End If
    '        'Sohail (04 Jan 2014) -- End

    '    End If

    'End Sub
    'Sohail (01 Aug 2019) -- End

    'Sohail (01 Aug 2019) -- Start
    'NMB Payroll UAT # TC006 - 76.1 - Advance search for process payroll, payslip, etc. should have search option. Currently user must scroll the entire list to select an item.
    Private Sub SetCheckBoxValue()
        Try

            RemoveHandler objchkSelectAll.CheckedChanged, AddressOf chkSelectAll_CheckedChanged

            Dim blnIsChecked As Boolean = Convert.ToBoolean(dgDetail.CurrentRow.Cells(objdgcolhCheck.Index).Value)

            Dim intID As Integer = CInt(dgDetail.CurrentRow.Cells(objdgcolhUnkid.Index).Value)
            Dim strName As String = dgDetail.CurrentRow.Cells(dgColhDescription.Index).Value.ToString

            If blnIsChecked = True Then
                mintCount += 1

                Dim dRow As DataRow = Nothing
                'Dim lvItem As New ListViewItem

                'lvItem.Tag = intID
                'lvItem.Text = strName
                'lvItem.SubItems.Add(mstrSelectedButton)

                'Dim GrplvItem As ListViewItem = lvFilterInfo.FindItemWithText(mstrSelectedButton)

                'If GrplvItem Is Nothing Then
                '    intGroupID += 1
                '    lvItem.SubItems.Add(intGroupID.ToString())
                'Else

                '    If GrplvItem.SubItems(objcolhGroup.Index).Text <> mstrSelectedButton Then
                '        intGroupID += 1
                '        lvItem.SubItems.Add(intGroupID.ToString())
                '    Else
                '        Dim GroupID As Integer = CInt(GrplvItem.SubItems(objcolhGroupID.Index).Text)
                '        lvItem.SubItems.Add(GroupID.ToString())
                '    End If

                'End If

                'lvItem.SubItems.Add(mstrButtonName)

                'lvFilterInfo.Items.Add(lvItem)
                'lvFilterInfo.GroupingColumn = objcolhGroup
                'lvFilterInfo.GridLines = False
                'lvFilterInfo.DisplayGroups(True)
                'lvFilterInfo.SortBy(objcolhGroup.Index, SortOrder.Ascending)

                'If lvFilterInfo.Items.Count > 10 Then
                '    colhFilterValue.Width = 340 - 20
                'Else
                '    colhFilterValue.Width = 340
                'End If
                'Sohail (14 Nov 2019) -- Start
                'NMB UAT Enhancement # : Bind Allocation to training course.
                ''If mdtFilterInfo.Select("GroupName = '" & mstrSelectedButton & "' AND IsGroup = 1 ").Length <= 0 Then
                'If mdtFilterInfo.Select("GroupId = " & mintSelectedAllocationID & " AND IsGroup = 1 ").Length <= 0 Then

                '    dRow = mdtFilterInfo.NewRow

                '    dRow.Item("Id") = intID
                '    dRow.Item("Name") = strName
                '    'Sohail (14 Nov 2019) -- Start
                '    'NMB UAT Enhancement # : Bind Allocation to training course.
                '    dRow.Item("GroupId") = mintSelectedAllocationID
                '    'Sohail (14 Nov 2019) -- End
                '    dRow.Item("GroupName") = mstrSelectedButton
                '    dRow.Item("IsGroup") = True

                '    mdtFilterInfo.Rows.Add(dRow)
                'End If
                'Sohail (14 Nov 2019) -- End
                dRow = mdtFilterInfo.NewRow

                dRow.Item("Id") = intID
                dRow.Item("Name") = strName
                'Sohail (14 Nov 2019) -- Start
                'NMB UAT Enhancement # : Bind Allocation to training course.
                'dRow.Item("GroupName") = mstrSelectedButton
                dRow.Item("GroupId") = mintSelectedAllocationID
                If mdicAdvAlloc.ContainsKey(mintSelectedAllocationID) = True Then
                    dRow.Item("GroupName") = mdicAdvAlloc.Item(mintSelectedAllocationID)
                Else
                    dRow.Item("GroupName") = mstrSelectedButton
                End If
                'Sohail (14 Nov 2019) -- End
                dRow.Item("IsGroup") = False

                mdtFilterInfo.Rows.Add(dRow)

            Else
                mintCount -= 1

                'If lvFilterInfo.Items.Count > 0 Then
                '    Dim lstSelected As List(Of ListViewItem) = (From p In lvFilterInfo.Items.Cast(Of ListViewItem)() Where p.SubItems(colhFilterValue.Index).Text = strName AndAlso p.SubItems(objcolhGroup.Index).Text = mstrSelectedButton Select (p)).ToList
                '    If lstSelected.Count > 0 Then
                '        lvFilterInfo.Items.Remove(lstSelected.Item(0))
                '    End If
                'End If
                'Sohail (14 Nov 2019) -- Start
                'NMB UAT Enhancement # : Bind Allocation to training course.
                'Dim dRow() As DataRow = mdtFilterInfo.Select("Id = " & intID & " AND GroupName = '" & mstrSelectedButton & "' AND IsGroup = 0 ")
                Dim dRow() As DataRow = mdtFilterInfo.Select("Id = " & intID & " AND GroupId = " & mintSelectedAllocationID & " AND IsGroup = 0 ")
                'Sohail (14 Nov 2019) -- End
                If dRow.Length > 0 Then
                    mdtFilterInfo.Rows.Remove(dRow(0))
                End If

                'Sohail (14 Nov 2019) -- Start
                'NMB UAT Enhancement # : Bind Allocation to training course.
                'If mdtFilterInfo.Select("GroupName = '" & mstrSelectedButton & "' AND IsGroup = 0 ").Length <= 0 Then
                '    dRow = mdtFilterInfo.Select("GroupName = '" & mstrSelectedButton & "' AND IsGroup = 1 ")
                If mdtFilterInfo.Select("GroupId = " & mintSelectedAllocationID & " AND IsGroup = 0 ").Length <= 0 Then
                    dRow = mdtFilterInfo.Select("GroupId = " & mintSelectedAllocationID & " AND IsGroup = 1 ")
                    'Sohail (14 Nov 2019) -- End
                    If dRow.Length > 0 Then
                        mdtFilterInfo.Rows.Remove(dRow(0))
                    End If
                End If
            End If

            mdtFilterInfo.AcceptChanges()

            'mdtFilterInfo = New DataView(mdtFilterInfo, "", "GroupName, IsGroup DESC, Name", DataViewRowState.CurrentRows).ToTable

            'gvFilterInfo.DataSource = mdtFilterInfo
            Call FillFilterInfo()
            'objlblEmpCount.Text = "( " & mintCount.ToString & " / " & mintTotalEmployee.ToString & " )"

            If mintCount <= 0 Then
                objchkSelectAll.CheckState = CheckState.Unchecked
            ElseIf mintCount < dgDetail.Rows.Count Then
                objchkSelectAll.CheckState = CheckState.Indeterminate
            ElseIf mintCount = dgDetail.Rows.Count Then
                objchkSelectAll.CheckState = CheckState.Checked
            End If

            AddHandler objchkSelectAll.CheckedChanged, AddressOf chkSelectAll_CheckedChanged
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "SetCheckBoxValue", mstrModuleName)
        End Try

    End Sub

    Private Sub SetDefaultSearchEmpText()
        Try
            mstrSearchText = Language.getMessage(mstrModuleName, 1, "Type to search")
            With txtSearch
                .ForeColor = Color.Gray
                .Text = mstrSearchText
                .Font = New Font(Me.Font.FontFamily, Me.Font.Size, FontStyle.Italic)
            End With
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "", mstrModuleName)
        End Try
    End Sub

    Private Sub CheckAll(ByVal blnCheckAll As Boolean)
        Try

            If dvDetail IsNot Nothing Then
                For Each dr As DataRowView In dvDetail
                    dr.Item("IsChecked") = blnCheckAll
                    dr.EndEdit()

                    If blnCheckAll = True Then
                        Dim dRow As DataRow = Nothing

                        'Sohail (14 Nov 2019) -- Start
                        'NMB UAT Enhancement # : Bind Allocation to training course.
                        ''If mdtFilterInfo.Select("GroupName = '" & mstrSelectedButton & "' AND IsGroup = 1 ").Length <= 0 Then
                        'If mdtFilterInfo.Select("GroupId = " & mintSelectedAllocationID & " AND IsGroup = 1 ").Length <= 0 Then

                        '    dRow = mdtFilterInfo.NewRow

                        '    dRow.Item("Id") = CInt(dr("Id"))
                        '    dRow.Item("Name") = dr("Name").ToString
                        '    'Sohail (14 Nov 2019) -- Start
                        '    'NMB UAT Enhancement # : Bind Allocation to training course.
                        '    dRow.Item("GroupId") = mintSelectedAllocationID
                        '    'Sohail (14 Nov 2019) -- End
                        '    dRow.Item("GroupName") = mstrSelectedButton
                        '    dRow.Item("IsGroup") = True

                        '    mdtFilterInfo.Rows.Add(dRow)
                        'End If
                        'Sohail (14 Nov 2019) -- End
                        dRow = mdtFilterInfo.NewRow

                        dRow.Item("Id") = CInt(dr("Id"))
                        dRow.Item("Name") = dr("Name").ToString
                        'Sohail (14 Nov 2019) -- Start
                        'NMB UAT Enhancement # : Bind Allocation to training course.
                        'dRow.Item("GroupName") = mstrSelectedButton
                        dRow.Item("GroupId") = mintSelectedAllocationID
                        If mdicAdvAlloc.ContainsKey(mintSelectedAllocationID) = True Then
                            dRow.Item("GroupName") = mdicAdvAlloc.Item(mintSelectedAllocationID)
                        Else
                            dRow.Item("GroupName") = mstrSelectedButton
                        End If
                        'Sohail (14 Nov 2019) -- End
                        dRow.Item("IsGroup") = False

                        mdtFilterInfo.Rows.Add(dRow)
                    Else
                        'Sohail (14 Nov 2019) -- Start
                        'NMB UAT Enhancement # : Bind Allocation to training course.
                        'Dim dRow() As DataRow = mdtFilterInfo.Select("Id = " & CInt(dr("Id")) & " AND GroupName = '" & mstrSelectedButton & "' AND IsGroup = 0 ")
                        Dim dRow() As DataRow = mdtFilterInfo.Select("Id = " & CInt(dr("Id")) & " AND GroupId = " & mintSelectedAllocationID & " AND IsGroup = 0 ")
                        'Sohail (14 Nov 2019) -- End
                        If dRow.Length > 0 Then
                            mdtFilterInfo.Rows.Remove(dRow(0))
                        End If

                        'Sohail (14 Nov 2019) -- Start
                        'NMB UAT Enhancement # : Bind Allocation to training course.
                        'If mdtFilterInfo.Select("GroupName = '" & mstrSelectedButton & "' AND IsGroup = 0 ").Length <= 0 Then
                        '    dRow = mdtFilterInfo.Select("GroupName = '" & mstrSelectedButton & "' AND IsGroup = 1 ")
                        If mdtFilterInfo.Select("GroupId = " & mintSelectedAllocationID & " AND IsGroup = 0 ").Length <= 0 Then
                            dRow = mdtFilterInfo.Select("GroupId = " & mintSelectedAllocationID & " AND IsGroup = 1 ")
                            'Sohail (14 Nov 2019) -- End
                            If dRow.Length > 0 Then
                                mdtFilterInfo.Rows.Remove(dRow(0))
                            End If
                        End If
                    End If
                    mdtFilterInfo.AcceptChanges()
                Next
                dvDetail.ToTable.AcceptChanges()

                'mdtFilterInfo = New DataView(mdtFilterInfo, "", "GroupName, IsGroup DESC, Name", DataViewRowState.CurrentRows).ToTable

                'gvFilterInfo.DataSource = mdtFilterInfo
                Call FillFilterInfo()

                Dim drRow As DataRow() = dvDetail.Table.Select("IsChecked = 1")
                mintCount = drRow.Length
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "CheckAll", mstrModuleName)
        End Try
    End Sub

    Private Sub CreateFilterInfoTable()
        Try
            If mdtFilterInfo Is Nothing Then 'Sohail (14 Nov 2019)
                mdtFilterInfo = New DataTable
                mdtFilterInfo.Columns.Add("Id", Type.GetType("System.Int32")).DefaultValue = 0
                mdtFilterInfo.Columns.Add("Name", Type.GetType("System.String")).DefaultValue = ""
                'Sohail (14 Nov 2019) -- Start
                'NMB UAT Enhancement # : Bind Allocation to training course.
                mdtFilterInfo.Columns.Add("GroupId", Type.GetType("System.Int32")).DefaultValue = 0
                'Sohail (14 Nov 2019) -- End
                mdtFilterInfo.Columns.Add("GroupName", Type.GetType("System.String")).DefaultValue = ""
                mdtFilterInfo.Columns.Add("IsGroup", Type.GetType("System.Boolean")).DefaultValue = False
            End If 'Sohail (14 Nov 2019)
            Call FillFilterInfo()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "CreateFilterInfoTable", mstrModuleName)
        End Try
    End Sub

    Private Sub FillFilterInfo()
        Try
            gvFilterInfo.AutoGenerateColumns = False

            objdgcolhFilterID.DataPropertyName = "Id"
            dgcolhFilterName.DataPropertyName = "Name"
            objdgcolhFilterGroupName.DataPropertyName = "GroupName"
            objdgcolhFilterIsGrp.DataPropertyName = "IsGroup"
            'Sohail (14 Nov 2019) -- Start
            'NMB UAT Enhancement # : Bind Allocation to training course.
            objdgcolhFilterGroupId.DataPropertyName = "GroupId"
            'Sohail (14 Nov 2019) -- End

            objdgcolhFilterGroupName.Visible = False
            objdgcolhFilterIsGrp.Visible = False

            'Sohail (14 Nov 2019) -- Start
            'NMB UAT Enhancement # : Bind Allocation to training course.
            'dvFilterInfo = mdtFilterInfo.DefaultView
            'dvFilterInfo.Sort = "GroupName, IsGroup DESC, Name"
            'gvFilterInfo.DataSource = mdtFilterInfo
            Dim dt As DataTable = New DataView(mdtFilterInfo).ToTable(True, "GroupName", "GroupId")
            Dim dtCol As New DataColumn("IsGroup", System.Type.GetType("System.Boolean"))
            dtCol.DefaultValue = True
            dtCol.AllowDBNull = False
            dt.Columns.Add(dtCol)
            dt.Merge(mdtFilterInfo, False)
            dvFilterInfo = dt.DefaultView
            dvFilterInfo.Sort = "GroupName, IsGroup DESC, Name"

            gvFilterInfo.DataSource = dvFilterInfo
            'Sohail (14 Nov 2019) -- End

            For Each dgvc As DataGridViewColumn In gvFilterInfo.Columns
                dgvc.SortMode = DataGridViewColumnSortMode.NotSortable
            Next

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "FillFilterInfo", mstrModuleName)
        End Try
    End Sub
    'Sohail (01 Aug 2019) -- End

#End Region

#Region " Button's Events "

    Private Sub btnBranch_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnBranch.Click, _
                                                                                                    btnDeptGrp.Click, btnDept.Click, _
                                                                                                    btnSection.Click, btnUnit.Click, btnJob.Click, _
                                                                                                    btnSectionGroup.Click, btnUnitGroup.Click, btnTeam.Click, btnGrade.Click, btnGradeGroup.Click, btnGradeLevel.Click, btnJobGroup.Click, btnClass.Click, btnClassGroup.Click, _
                                                                                                    btnCostCenterGroup.Click, btnCostCenter.Click, btnEmployementType.Click, btnReligion.Click, btnGender.Click, btnNationality.Click, btnPayType.Click, btnPayPoint.Click, _
                                                                                                    btnMaritalStatus.Click, btnRelationFromDpndt.Click, btnSkill.Click
        'Sohail (14 Nov 2019) - [btnSkill.Click]
        '                                                                                           'Sohail (02 Jul 2014) - [btnPayType.Click, btnPayPoint.Click]
        '                                                                                           'Sohail (09 Nov 2013) - [btnGender.Click, btnNationality.Click]
        '                                                                                           'S.SANDEEP [ 22 MAR 2013 ] -- START -- END
        'S.SANDEEP |04-JUN-2019| -- START
        'ISSUE/ENHANCEMENT : [Relation filters on Allocation Advance search]
        ', btnRelationFromDpndt.Click
        'S.SANDEEP |04-JUN-2019| -- END

        Try
            Call FillDetailList(CType(sender, eZee.Common.eZeeLightButton).Name.ToString)
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "", mstrModuleName)
        Finally
        End Try
    End Sub


    'Private Sub btnApply_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnApply.Click
    '    Try

    '        If lvFilterInfo.Items.Count > 0 Then
    '            Dim mintGroupID As Integer = 0
    '            Dim mstr As String = ""
    '            For i As Integer = 0 To lvFilterInfo.Items.Count - 1
    '                Select Case lvFilterInfo.Items(i).SubItems(objcolhbtnName.Index).Text
    '                    '================= BRANCH ================='
    '                    Case btnBranch.Name
    '                        If mstr.Trim.Length = 0 Then
    '                            'Sohail (05 Dec 2011) -- Start
    '                            'mstr = " stationunkid in ("
    '                            mstr = " " & mstrEmployeeTableAlias & "stationunkid in ("
    '                            'Sohail (05 Dec 2011) -- End
    '                        ElseIf mstr.Trim.Contains("stationunkid") = False Then
    '                            mstr = mstr.Substring(0, mstr.Length - 1) & ") "
    '                            'Sohail (05 Dec 2011) -- Start
    '                            'mstr &= " AND stationunkid in ("
    '                            mstr &= " AND " & mstrEmployeeTableAlias & "stationunkid in ("
    '                            'Sohail (05 Dec 2011) -- End
    '                        End If
    '                        mstr &= lvFilterInfo.Items(i).Tag.ToString() & ","

    '                        '================= DEPARTMENT ================='
    '                    Case btnDept.Name
    '                        If mstr.Trim.Length = 0 Then
    '                            'Sohail (05 Dec 2011) -- Start
    '                            'mstr = " departmentunkid in ("
    '                            mstr = " " & mstrEmployeeTableAlias & "departmentunkid in ("
    '                            'Sohail (05 Dec 2011) -- End
    '                        ElseIf mstr.Trim.Contains("departmentunkid") = False Then
    '                            mstr = mstr.Substring(0, mstr.Length - 1) & ") "
    '                            'Sohail (05 Dec 2011) -- Start
    '                            'mstr &= " AND departmentunkid in ("
    '                            mstr &= " AND " & mstrEmployeeTableAlias & "departmentunkid in ("
    '                            'Sohail (05 Dec 2011) -- End
    '                        End If
    '                        mstr &= lvFilterInfo.Items(i).Tag.ToString() & ","

    '                        '================= DEPARTMENT GROUP ================='
    '                    Case btnDeptGrp.Name
    '                        If mstr.Trim.Length = 0 Then
    '                            'Sohail (05 Dec 2011) -- Start
    '                            'mstr = " deptgroupunkid in ("
    '                            mstr = " " & mstrEmployeeTableAlias & "deptgroupunkid in ("
    '                            'Sohail (05 Dec 2011) -- End
    '                        ElseIf mstr.Trim.Contains("deptgroupunkid") = False Then
    '                            mstr = mstr.Substring(0, mstr.Length - 1) & ") "
    '                            'Sohail (05 Dec 2011) -- Start
    '                            'mstr &= "  AND deptgroupunkid in ("
    '                            mstr &= "  AND " & mstrEmployeeTableAlias & "deptgroupunkid in ("
    '                            'Sohail (05 Dec 2011) -- End
    '                        End If
    '                        mstr &= lvFilterInfo.Items(i).Tag.ToString() & ","

    '                        '================= SECTION ================='
    '                    Case btnSection.Name
    '                        If mstr.Trim.Length = 0 Then
    '                            'Sohail (05 Dec 2011) -- Start
    '                            'mstr = " sectionunkid in ("
    '                            mstr = " " & mstrEmployeeTableAlias & "sectionunkid in ("
    '                            'Sohail (05 Dec 2011) -- End
    '                        ElseIf mstr.Trim.Contains("sectionunkid") = False Then
    '                            mstr = mstr.Substring(0, mstr.Length - 1) & ") "
    '                            'Sohail (05 Dec 2011) -- Start
    '                            'mstr &= " AND sectionunkid in ("
    '                            mstr &= " AND " & mstrEmployeeTableAlias & "sectionunkid in ("
    '                            'Sohail (05 Dec 2011) -- End
    '                        End If
    '                        mstr &= lvFilterInfo.Items(i).Tag.ToString() & ","

    '                        '================= UNIT ================='
    '                    Case btnUnit.Name
    '                        If mstr.Trim.Length = 0 Then
    '                            'Sohail (05 Dec 2011) -- Start
    '                            'mstr = " unitunkid in ("
    '                            mstr = " " & mstrEmployeeTableAlias & "unitunkid in ("
    '                            'Sohail (05 Dec 2011) -- End
    '                        ElseIf mstr.Trim.Contains("unitunkid") = False Then
    '                            mstr = mstr.Substring(0, mstr.Length - 1) & ") "
    '                            'Sohail (05 Dec 2011) -- Start
    '                            'mstr &= " AND unitunkid in ("
    '                            mstr &= " AND " & mstrEmployeeTableAlias & "unitunkid in ("
    '                            'Sohail (05 Dec 2011) -- End
    '                        End If
    '                        mstr &= lvFilterInfo.Items(i).Tag.ToString() & ","

    '                        '================= JOB ================='
    '                    Case btnJob.Name
    '                        If mstr.Trim.Length = 0 Then
    '                            'Sohail (05 Dec 2011) -- Start
    '                            'mstr = " jobunkid in ("
    '                            mstr = " " & mstrEmployeeTableAlias & "jobunkid in ("
    '                            'Sohail (05 Dec 2011) -- End
    '                        ElseIf mstr.Trim.Contains("jobunkid") = False Then
    '                            mstr = mstr.Substring(0, mstr.Length - 1) & ") "
    '                            'Sohail (05 Dec 2011) -- Start
    '                            'mstr &= " AND jobunkid in ("
    '                            mstr &= " AND " & mstrEmployeeTableAlias & "jobunkid in ("
    '                            'Sohail (05 Dec 2011) -- End
    '                        End If
    '                        mstr &= lvFilterInfo.Items(i).Tag.ToString() & ","

    '                        '================= SECTION GROUP ================='
    '                    Case btnSectionGroup.Name
    '                        If mstr.Trim.Length = 0 Then
    '                            'Sohail (05 Dec 2011) -- Start
    '                            'mstr = " sectiongroupunkid in ("
    '                            mstr = " " & mstrEmployeeTableAlias & "sectiongroupunkid in ("
    '                            'Sohail (05 Dec 2011) -- End
    '                        ElseIf mstr.Trim.Contains("sectiongroupunkid") = False Then
    '                            mstr = mstr.Substring(0, mstr.Length - 1) & ") "
    '                            'Sohail (05 Dec 2011) -- Start
    '                            'mstr &= " AND sectiongroupunkid in ("
    '                            mstr &= " AND " & mstrEmployeeTableAlias & "sectiongroupunkid in ("
    '                            'Sohail (05 Dec 2011) -- End
    '                        End If
    '                        mstr &= lvFilterInfo.Items(i).Tag.ToString() & ","

    '                        '================= UNIT GROUP ================='
    '                    Case btnUnitGroup.Name
    '                        If mstr.Trim.Length = 0 Then
    '                            'Sohail (05 Dec 2011) -- Start
    '                            'mstr = " unitgroupunkid in ("
    '                            mstr = " " & mstrEmployeeTableAlias & "unitgroupunkid in ("
    '                            'Sohail (05 Dec 2011) -- End
    '                        ElseIf mstr.Trim.Contains("unitgroupunkid") = False Then
    '                            mstr = mstr.Substring(0, mstr.Length - 1) & ") "
    '                            'Sohail (05 Dec 2011) -- Start
    '                            'mstr &= " AND unitgroupunkid in ("
    '                            mstr &= " AND " & mstrEmployeeTableAlias & "unitgroupunkid in ("
    '                            'Sohail (05 Dec 2011) -- End
    '                        End If
    '                        mstr &= lvFilterInfo.Items(i).Tag.ToString() & ","

    '                        '================= TEAM ================='
    '                    Case btnTeam.Name
    '                        If mstr.Trim.Length = 0 Then
    '                            'Sohail (05 Dec 2011) -- Start
    '                            'mstr = " teamunkid in ("
    '                            mstr = " " & mstrEmployeeTableAlias & "teamunkid in ("
    '                            'Sohail (05 Dec 2011) -- End
    '                        ElseIf mstr.Trim.Contains("teamunkid") = False Then
    '                            mstr = mstr.Substring(0, mstr.Length - 1) & ") "
    '                            'Sohail (05 Dec 2011) -- Start
    '                            'mstr &= " AND teamunkid in ("
    '                            mstr &= " AND " & mstrEmployeeTableAlias & "teamunkid in ("
    '                            'Sohail (05 Dec 2011) -- End
    '                        End If
    '                        mstr &= lvFilterInfo.Items(i).Tag.ToString() & ","

    '                        '================= JOB GROUP ================='
    '                    Case btnJobGroup.Name
    '                        If mstr.Trim.Length = 0 Then
    '                            'Sohail (05 Dec 2011) -- Start
    '                            'mstr = " jobgroupunkid in ("
    '                            mstr = " " & mstrEmployeeTableAlias & "jobgroupunkid in ("
    '                            'Sohail (05 Dec 2011) -- End
    '                        ElseIf mstr.Trim.Contains("jobgroupunkid") = False Then
    '                            mstr = mstr.Substring(0, mstr.Length - 1) & ")"
    '                            'Sohail (05 Dec 2011) -- Start
    '                            'mstr &= " AND jobgroupunkid in ("
    '                            mstr &= " AND " & mstrEmployeeTableAlias & "jobgroupunkid in ("
    '                            'Sohail (05 Dec 2011) -- End
    '                        End If
    '                        mstr &= lvFilterInfo.Items(i).Tag.ToString() & ","


    '                        '================= CLASS GROUP ================='
    '                    Case btnClassGroup.Name
    '                        If mstr.Trim.Length = 0 Then
    '                            'Sohail (05 Dec 2011) -- Start
    '                            'mstr = " classgroupunkid in ("
    '                            mstr = " " & mstrEmployeeTableAlias & "classgroupunkid in ("
    '                            'Sohail (05 Dec 2011) -- End
    '                        ElseIf mstr.Trim.Contains("classgroupunkid") = False Then
    '                            mstr = mstr.Substring(0, mstr.Length - 1) & ")"
    '                            'Sohail (05 Dec 2011) -- Start
    '                            'mstr &= " AND classgroupunkid in ("
    '                            mstr &= " AND " & mstrEmployeeTableAlias & "classgroupunkid in ("
    '                            'Sohail (05 Dec 2011) -- End
    '                        End If
    '                        mstr &= lvFilterInfo.Items(i).Tag.ToString() & ","

    '                        '================= CLASSES ================='
    '                    Case btnClass.Name
    '                        If mstr.Trim.Length = 0 Then
    '                            'Sohail (05 Dec 2011) -- Start
    '                            'mstr = " classunkid in ("
    '                            mstr = " " & mstrEmployeeTableAlias & "classunkid in ("
    '                            'Sohail (05 Dec 2011) -- End
    '                        ElseIf mstr.Trim.Contains("classunkid") = False Then
    '                            mstr = mstr.Substring(0, mstr.Length - 1) & ")"
    '                            'Sohail (05 Dec 2011) -- Start
    '                            'mstr &= " AND classunkid in ("
    '                            mstr &= " AND " & mstrEmployeeTableAlias & "classunkid in ("
    '                            'Sohail (05 Dec 2011) -- End
    '                        End If
    '                        mstr &= lvFilterInfo.Items(i).Tag.ToString() & ","

    '                        '================= GRADE GROUP ================='
    '                    Case btnGradeGroup.Name
    '                        If mstr.Trim.Length = 0 Then
    '                            'Sohail (05 Dec 2011) -- Start
    '                            'mstr = " gradegroupunkid in ("
    '                            mstr = " " & mstrEmployeeTableAlias & "gradegroupunkid in ("
    '                            'Sohail (05 Dec 2011) -- End
    '                        ElseIf mstr.Trim.Contains("gradegroupunkid") = False Then
    '                            mstr = mstr.Substring(0, mstr.Length - 1) & ")"
    '                            'Sohail (05 Dec 2011) -- Start
    '                            'mstr &= " AND gradegroupunkid in ("
    '                            mstr &= " AND " & mstrEmployeeTableAlias & "gradegroupunkid in ("
    '                            'Sohail (05 Dec 2011) -- End
    '                        End If
    '                        mstr &= lvFilterInfo.Items(i).Tag.ToString() & ","

    '                        '================= GRADE ================='
    '                    Case btnGrade.Name
    '                        If mstr.Trim.Length = 0 Then
    '                            'Sohail (05 Dec 2011) -- Start
    '                            'mstr = " gradeunkid in ("
    '                            mstr = " " & mstrEmployeeTableAlias & "gradeunkid in ("
    '                            'Sohail (05 Dec 2011) -- End
    '                        ElseIf mstr.Trim.Contains("gradeunkid") = False Then
    '                            mstr = mstr.Substring(0, mstr.Length - 1) & ")"
    '                            'Sohail (05 Dec 2011) -- Start
    '                            'mstr &= " AND gradeunkid in ("
    '                            mstr &= " AND " & mstrEmployeeTableAlias & "gradeunkid in ("
    '                            'Sohail (05 Dec 2011) -- End
    '                        End If
    '                        mstr &= lvFilterInfo.Items(i).Tag.ToString() & ","

    '                        '================= GRADE LEVEL ================='
    '                    Case btnGradeLevel.Name
    '                        If mstr.Trim.Length = 0 Then
    '                            'Sohail (05 Dec 2011) -- Start
    '                            'mstr = " gradelevelunkid in ("
    '                            mstr = " " & mstrEmployeeTableAlias & "gradelevelunkid in ("
    '                            'Sohail (05 Dec 2011) -- End
    '                        ElseIf mstr.Trim.Contains("gradelevelunkid") = False Then
    '                            mstr = mstr.Substring(0, mstr.Length - 1) & ")"
    '                            'Sohail (05 Dec 2011) -- Start
    '                            'mstr &= " AND gradelevelunkid in ("
    '                            mstr &= " AND " & mstrEmployeeTableAlias & "gradelevelunkid in ("
    '                            'Sohail (05 Dec 2011) -- End
    '                        End If
    '                        mstr &= lvFilterInfo.Items(i).Tag.ToString() & ","
    '                        'S.SANDEEP [ 22 MAR 2013 ] -- START
    '                        'ENHANCEMENT : TRA CHANGES
    '                        '================= COST CENTER GROUP ================='
    '                    Case btnCostCenterGroup.Name
    '                        Dim StrIds As String = clscostcenter_master.GetCSV_CC(lvFilterInfo.Items(i).Tag.ToString())
    '                        If StrIds.Trim <> "" Then 'Sohail (04 Jan 2014)
    '                            If mstr.Trim.Length = 0 Then
    '                                mstr = " " & mstrEmployeeTableAlias & "costcenterunkid in ("
    '                            ElseIf mstr.Trim.Contains("costcenterunkid") = False Then
    '                                mstr = mstr.Substring(0, mstr.Length - 1) & ")"
    '                                mstr &= " AND " & mstrEmployeeTableAlias & "costcenterunkid in ("
    '                            End If
    '                            mstr &= StrIds & ","
    '                        End If 'Sohail (04 Jan 2014)
    '                        '================= COST CENTER ================='
    '                    Case btnCostCenter.Name
    '                        If mstr.Trim.Length = 0 Then
    '                            mstr = " " & mstrEmployeeTableAlias & "costcenterunkid in ("
    '                        ElseIf mstr.Trim.Contains("costcenterunkid") = False Then
    '                            mstr = mstr.Substring(0, mstr.Length - 1) & ")"
    '                            mstr &= " AND " & mstrEmployeeTableAlias & "costcenterunkid in ("
    '                        End If
    '                        mstr &= lvFilterInfo.Items(i).Tag.ToString() & ","
    '                        'S.SANDEEP [ 22 MAR 2013 ] -- END


    '                        'Pinkal (15-Sep-2013) -- Start
    '                        'Enhancement : TRA Changes

    '                        '================= EMPLOYEMENT TYPE ================='

    '                    Case btnEmployementType.Name
    '                        If mstr.Trim.Length = 0 Then
    '                            mstr = " " & mstrEmployeeTableAlias & "employmenttypeunkid in ("
    '                        ElseIf mstr.Trim.Contains("employmenttypeunkid") = False Then
    '                            mstr = mstr.Substring(0, mstr.Length - 1) & ")"
    '                            mstr &= " AND " & mstrEmployeeTableAlias & "employmenttypeunkid in ("
    '                        End If
    '                        mstr &= lvFilterInfo.Items(i).Tag.ToString() & ","

    '                        '================= RELIGION ================='

    '                    Case btnReligion.Name
    '                        If mstr.Trim.Length = 0 Then
    '                            mstr = " " & mstrEmployeeTableAlias & "religionunkid in ("
    '                        ElseIf mstr.Trim.Contains("religionunkid") = False Then
    '                            mstr = mstr.Substring(0, mstr.Length - 1) & ")"
    '                            mstr &= " AND " & mstrEmployeeTableAlias & "religionunkid in ("
    '                        End If
    '                        mstr &= lvFilterInfo.Items(i).Tag.ToString() & ","

    '                        'Pinkal (15-Sep-2013) -- End

    '                        'Sohail (09 Nov 2013) -- Start
    '                        'TRA - ENHANCEMENT
    '                        '================= GENDER ================='
    '                    Case btnGender.Name
    '                        If mstr.Trim.Length = 0 Then
    '                            mstr = " " & mstrEmployeeTableAlias & "gender in ("
    '                        ElseIf mstr.Trim.Contains("gender") = False Then
    '                            mstr = mstr.Substring(0, mstr.Length - 1) & ")"
    '                            mstr &= " AND " & mstrEmployeeTableAlias & "gender in ("
    '                        End If
    '                        mstr &= lvFilterInfo.Items(i).Tag.ToString() & ","

    '                        '================= NATIONALITY ================='
    '                    Case btnNationality.Name
    '                        If mstr.Trim.Length = 0 Then
    '                            mstr = " " & mstrEmployeeTableAlias & "nationalityunkid in ("
    '                        ElseIf mstr.Trim.Contains("nationality") = False Then
    '                            mstr = mstr.Substring(0, mstr.Length - 1) & ")"
    '                            mstr &= " AND " & mstrEmployeeTableAlias & "nationalityunkid in ("
    '                        End If
    '                        mstr &= lvFilterInfo.Items(i).Tag.ToString() & ","
    '                        'Sohail (09 Nov 2013) -- End

    '                        'Sohail (02 Jul 2014) -- Start
    '                        'Enhancement - Pay Type and Pay Point allocation on Advance Filter.
    '                        '================= PAY TYPE ================='
    '                    Case btnPayType.Name
    '                        If mstr.Trim.Length = 0 Then
    '                            mstr = " " & mstrEmployeeTableAlias & "paytypeunkid in ("
    '                        ElseIf mstr.Trim.Contains("paytypeunkid") = False Then
    '                            mstr = mstr.Substring(0, mstr.Length - 1) & ")"
    '                            mstr &= " AND " & mstrEmployeeTableAlias & "paytypeunkid in ("
    '                        End If
    '                        mstr &= lvFilterInfo.Items(i).Tag.ToString() & ","

    '                        '================= PAY POINT ================='
    '                    Case btnPayPoint.Name
    '                        If mstr.Trim.Length = 0 Then
    '                            mstr = " " & mstrEmployeeTableAlias & "paypointunkid in ("
    '                        ElseIf mstr.Trim.Contains("paypointunkid") = False Then
    '                            mstr = mstr.Substring(0, mstr.Length - 1) & ")"
    '                            mstr &= " AND " & mstrEmployeeTableAlias & "paypointunkid in ("
    '                        End If
    '                        mstr &= lvFilterInfo.Items(i).Tag.ToString() & ","
    '                        'Sohail (02 Jul 2014) -- End


    '                        'Pinkal (01-Oct-2014) -- Start
    '                        'Enhancement -  Changes For FDRC Report

    '                        '================= MARITAL STATUS ================='
    '                    Case btnMaritalStatus.Name
    '                        If mstr.Trim.Length = 0 Then
    '                            mstr = " " & mstrEmployeeTableAlias & "maritalstatusunkid in ("
    '                        ElseIf mstr.Trim.Contains("maritalstatusunkid") = False Then
    '                            mstr = mstr.Substring(0, mstr.Length - 1) & ")"
    '                            mstr &= " AND " & mstrEmployeeTableAlias & "maritalstatusunkid in ("
    '                        End If
    '                        mstr &= lvFilterInfo.Items(i).Tag.ToString() & ","
    '                        'Pinkal (01-Oct-2014) -- End

    '                End Select
    '            Next
    '            'Sohail (04 Jan 2014) -- Start
    '            'Enhancement - Advance Search in Self Service
    '            'mstr = mstr.Substring(0, mstr.Length - 1) & ") "
    '            If mstr.Trim <> "" Then
    '                mstr = mstr.Substring(0, mstr.Length - 1) & ") "
    '            Else
    '                mstr = " 1 = 1 "
    '            End If
    '            'Sohail (04 Jan 2014) -- End

    '            mstrFilterString = mstr
    '        End If
    '        Me.DialogResult = Windows.Forms.DialogResult.OK
    '        Me.Close()
    '    Catch ex As Exception
    '        DisplayError.Show("-1", ex.Message, "btnApply_Click", mstrModuleName)
    '    End Try
    'End Sub


    'Sohail (08 May 2015) -- Start
    'Enhancement - Get Advance Filter string from new employee transfer table and employee categorization table.
    'Private Sub btnApply_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnApply.Click
    '    Dim strQ As String = "" 'Sohail (08 May 2015)
    '    Try

    '        If lvFilterInfo.Items.Count > 0 Then
    '            Dim mintGroupID As Integer = 0
    '            Dim mstr As String = ""

    '            'Sohail (08 May 2015) -- Start
    '            'Enhancement - Get Advance Filter string from new employee transfer table and employee categorization table.
    '            If mdtAsOnDate = Nothing Then mdtAsOnDate = eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate.ToString)

    '            strQ = "AND (" & mstrEmployeeTableAlias & ".employeeunkid IN ( " & _
    '                    "SELECT  Trf.TrfEmpId " & _
    '                    "FROM    ( SELECT    ETT.employeeunkid AS TrfEmpId  " & _
    '                                      ", ISNULL(ETT.stationunkid, 0) AS stationunkid " & _
    '                                      ", ISNULL(ETT.deptgroupunkid, 0) AS deptgroupunkid " & _
    '                                      ", ISNULL(ETT.departmentunkid, 0) AS departmentunkid " & _
    '                                      ", ISNULL(ETT.sectiongroupunkid, 0) AS sectiongroupunkid " & _
    '                                      ", ISNULL(ETT.sectionunkid, 0) AS sectionunkid " & _
    '                                      ", ISNULL(ETT.unitgroupunkid, 0) AS unitgroupunkid " & _
    '                                      ", ISNULL(ETT.unitunkid, 0) AS unitunkid " & _
    '                                      ", ISNULL(ETT.teamunkid, 0) AS teamunkid " & _
    '                                      ", ISNULL(ETT.classgroupunkid, 0) AS classgroupunkid " & _
    '                                      ", ISNULL(ETT.classunkid, 0) AS classunkid " & _
    '                                      ", CONVERT(CHAR(8), ETT.effectivedate, 112) AS EfDt " & _
    '                                      ", ETT.employeeunkid " & _
    '                                      ", ROW_NUMBER() OVER ( PARTITION BY ETT.employeeunkid ORDER BY ETT.effectivedate DESC ) AS Rno " & _
    '                              "FROM      " & FinancialYear._Object._DatabaseName & "..hremployee_transfer_tran AS ETT " & _
    '                              "WHERE     isvoid = 0 " & _
    '                                        "AND CONVERT(CHAR(8), effectivedate, 112) <= '" & eZeeDate.convertDate(mdtAsOnDate).ToString & "' " & _
    '                            ") AS Trf " & _
    '                            "JOIN ( SELECT   ECT.employeeunkid AS CatEmpId  " & _
    '                                          ", ECT.jobgroupunkid " & _
    '                                          ", ECT.jobunkid " & _
    '                                          ", CONVERT(CHAR(8), ECT.effectivedate, 112) AS CEfDt " & _
    '                                          ", ECT.employeeunkid " & _
    '                                          ", ROW_NUMBER() OVER ( PARTITION BY ECT.employeeunkid ORDER BY ECT.effectivedate DESC ) AS Rno " & _
    '                                   "FROM     " & FinancialYear._Object._DatabaseName & "..hremployee_categorization_tran AS ECT " & _
    '                                   "WHERE    isvoid = 0 " & _
    '                                            "AND CONVERT(CHAR(8), effectivedate, 112) <= '" & eZeeDate.convertDate(mdtAsOnDate).ToString & "' " & _
    '                                 ") AS RUCat ON trf.employeeunkid = RUCat.employeeunkid " & _
    '                    "WHERE   Trf.Rno = 1 " & _
    '                            "AND RUCat.Rno = 1 "
    '            'Sohail (08 May 2015) -- End

    '            For i As Integer = 0 To lvFilterInfo.Items.Count - 1
    '                Select Case lvFilterInfo.Items(i).SubItems(objcolhbtnName.Index).Text
    '                    '================= BRANCH ================='
    '                    Case btnBranch.Name
    '                        If mstr.Trim.Length = 0 Then
    '                            'Sohail (05 Dec 2011) -- Start
    '                            'mstr = " stationunkid in ("
    '                            mstr = " " & mstrEmployeeTableAlias & "stationunkid in ("
    '                            'Sohail (05 Dec 2011) -- End
    '                        ElseIf mstr.Trim.Contains("stationunkid") = False Then
    '                            mstr = mstr.Substring(0, mstr.Length - 1) & ") "
    '                            'Sohail (05 Dec 2011) -- Start
    '                            'mstr &= " AND stationunkid in ("
    '                            mstr &= " AND " & mstrEmployeeTableAlias & "stationunkid in ("
    '                            'Sohail (05 Dec 2011) -- End
    '                        End If
    '                        mstr &= lvFilterInfo.Items(i).Tag.ToString() & ","

    '                        '================= DEPARTMENT ================='
    '                    Case btnDept.Name
    '                        If mstr.Trim.Length = 0 Then
    '                            'Sohail (05 Dec 2011) -- Start
    '                            'mstr = " departmentunkid in ("
    '                            mstr = " " & mstrEmployeeTableAlias & "departmentunkid in ("
    '                            'Sohail (05 Dec 2011) -- End
    '                        ElseIf mstr.Trim.Contains("departmentunkid") = False Then
    '                            mstr = mstr.Substring(0, mstr.Length - 1) & ") "
    '                            'Sohail (05 Dec 2011) -- Start
    '                            'mstr &= " AND departmentunkid in ("
    '                            mstr &= " AND " & mstrEmployeeTableAlias & "departmentunkid in ("
    '                            'Sohail (05 Dec 2011) -- End
    '                        End If
    '                        mstr &= lvFilterInfo.Items(i).Tag.ToString() & ","

    '                        '================= DEPARTMENT GROUP ================='
    '                    Case btnDeptGrp.Name
    '                        If mstr.Trim.Length = 0 Then
    '                            'Sohail (05 Dec 2011) -- Start
    '                            'mstr = " deptgroupunkid in ("
    '                            mstr = " " & mstrEmployeeTableAlias & "deptgroupunkid in ("
    '                            'Sohail (05 Dec 2011) -- End
    '                        ElseIf mstr.Trim.Contains("deptgroupunkid") = False Then
    '                            mstr = mstr.Substring(0, mstr.Length - 1) & ") "
    '                            'Sohail (05 Dec 2011) -- Start
    '                            'mstr &= "  AND deptgroupunkid in ("
    '                            mstr &= "  AND " & mstrEmployeeTableAlias & "deptgroupunkid in ("
    '                            'Sohail (05 Dec 2011) -- End
    '                        End If
    '                        mstr &= lvFilterInfo.Items(i).Tag.ToString() & ","

    '                        '================= SECTION ================='
    '                    Case btnSection.Name
    '                        If mstr.Trim.Length = 0 Then
    '                            'Sohail (05 Dec 2011) -- Start
    '                            'mstr = " sectionunkid in ("
    '                            mstr = " " & mstrEmployeeTableAlias & "sectionunkid in ("
    '                            'Sohail (05 Dec 2011) -- End
    '                        ElseIf mstr.Trim.Contains("sectionunkid") = False Then
    '                            mstr = mstr.Substring(0, mstr.Length - 1) & ") "
    '                            'Sohail (05 Dec 2011) -- Start
    '                            'mstr &= " AND sectionunkid in ("
    '                            mstr &= " AND " & mstrEmployeeTableAlias & "sectionunkid in ("
    '                            'Sohail (05 Dec 2011) -- End
    '                        End If
    '                        mstr &= lvFilterInfo.Items(i).Tag.ToString() & ","

    '                        '================= UNIT ================='
    '                    Case btnUnit.Name
    '                        If mstr.Trim.Length = 0 Then
    '                            'Sohail (05 Dec 2011) -- Start
    '                            'mstr = " unitunkid in ("
    '                            mstr = " " & mstrEmployeeTableAlias & "unitunkid in ("
    '                            'Sohail (05 Dec 2011) -- End
    '                        ElseIf mstr.Trim.Contains("unitunkid") = False Then
    '                            mstr = mstr.Substring(0, mstr.Length - 1) & ") "
    '                            'Sohail (05 Dec 2011) -- Start
    '                            'mstr &= " AND unitunkid in ("
    '                            mstr &= " AND " & mstrEmployeeTableAlias & "unitunkid in ("
    '                            'Sohail (05 Dec 2011) -- End
    '                        End If
    '                        mstr &= lvFilterInfo.Items(i).Tag.ToString() & ","

    '                        '================= JOB ================='
    '                    Case btnJob.Name
    '                        If mstr.Trim.Length = 0 Then
    '                            'Sohail (05 Dec 2011) -- Start
    '                            'mstr = " jobunkid in ("
    '                            mstr = " " & mstrEmployeeTableAlias & "jobunkid in ("
    '                            'Sohail (05 Dec 2011) -- End
    '                        ElseIf mstr.Trim.Contains("jobunkid") = False Then
    '                            mstr = mstr.Substring(0, mstr.Length - 1) & ") "
    '                            'Sohail (05 Dec 2011) -- Start
    '                            'mstr &= " AND jobunkid in ("
    '                            mstr &= " AND " & mstrEmployeeTableAlias & "jobunkid in ("
    '                            'Sohail (05 Dec 2011) -- End
    '                        End If
    '                        mstr &= lvFilterInfo.Items(i).Tag.ToString() & ","

    '                        '================= SECTION GROUP ================='
    '                    Case btnSectionGroup.Name
    '                        If mstr.Trim.Length = 0 Then
    '                            'Sohail (05 Dec 2011) -- Start
    '                            'mstr = " sectiongroupunkid in ("
    '                            mstr = " " & mstrEmployeeTableAlias & "sectiongroupunkid in ("
    '                            'Sohail (05 Dec 2011) -- End
    '                        ElseIf mstr.Trim.Contains("sectiongroupunkid") = False Then
    '                            mstr = mstr.Substring(0, mstr.Length - 1) & ") "
    '                            'Sohail (05 Dec 2011) -- Start
    '                            'mstr &= " AND sectiongroupunkid in ("
    '                            mstr &= " AND " & mstrEmployeeTableAlias & "sectiongroupunkid in ("
    '                            'Sohail (05 Dec 2011) -- End
    '                        End If
    '                        mstr &= lvFilterInfo.Items(i).Tag.ToString() & ","

    '                        '================= UNIT GROUP ================='
    '                    Case btnUnitGroup.Name
    '                        If mstr.Trim.Length = 0 Then
    '                            'Sohail (05 Dec 2011) -- Start
    '                            'mstr = " unitgroupunkid in ("
    '                            mstr = " " & mstrEmployeeTableAlias & "unitgroupunkid in ("
    '                            'Sohail (05 Dec 2011) -- End
    '                        ElseIf mstr.Trim.Contains("unitgroupunkid") = False Then
    '                            mstr = mstr.Substring(0, mstr.Length - 1) & ") "
    '                            'Sohail (05 Dec 2011) -- Start
    '                            'mstr &= " AND unitgroupunkid in ("
    '                            mstr &= " AND " & mstrEmployeeTableAlias & "unitgroupunkid in ("
    '                            'Sohail (05 Dec 2011) -- End
    '                        End If
    '                        mstr &= lvFilterInfo.Items(i).Tag.ToString() & ","

    '                        '================= TEAM ================='
    '                    Case btnTeam.Name
    '                        If mstr.Trim.Length = 0 Then
    '                            'Sohail (05 Dec 2011) -- Start
    '                            'mstr = " teamunkid in ("
    '                            mstr = " " & mstrEmployeeTableAlias & "teamunkid in ("
    '                            'Sohail (05 Dec 2011) -- End
    '                        ElseIf mstr.Trim.Contains("teamunkid") = False Then
    '                            mstr = mstr.Substring(0, mstr.Length - 1) & ") "
    '                            'Sohail (05 Dec 2011) -- Start
    '                            'mstr &= " AND teamunkid in ("
    '                            mstr &= " AND " & mstrEmployeeTableAlias & "teamunkid in ("
    '                            'Sohail (05 Dec 2011) -- End
    '                        End If
    '                        mstr &= lvFilterInfo.Items(i).Tag.ToString() & ","

    '                        '================= JOB GROUP ================='
    '                    Case btnJobGroup.Name
    '                        If mstr.Trim.Length = 0 Then
    '                            'Sohail (05 Dec 2011) -- Start
    '                            'mstr = " jobgroupunkid in ("
    '                            mstr = " " & mstrEmployeeTableAlias & "jobgroupunkid in ("
    '                            'Sohail (05 Dec 2011) -- End
    '                        ElseIf mstr.Trim.Contains("jobgroupunkid") = False Then
    '                            mstr = mstr.Substring(0, mstr.Length - 1) & ")"
    '                            'Sohail (05 Dec 2011) -- Start
    '                            'mstr &= " AND jobgroupunkid in ("
    '                            mstr &= " AND " & mstrEmployeeTableAlias & "jobgroupunkid in ("
    '                            'Sohail (05 Dec 2011) -- End
    '                        End If
    '                        mstr &= lvFilterInfo.Items(i).Tag.ToString() & ","


    '                        '================= CLASS GROUP ================='
    '                    Case btnClassGroup.Name
    '                        If mstr.Trim.Length = 0 Then
    '                            'Sohail (05 Dec 2011) -- Start
    '                            'mstr = " classgroupunkid in ("
    '                            mstr = " " & mstrEmployeeTableAlias & "classgroupunkid in ("
    '                            'Sohail (05 Dec 2011) -- End
    '                        ElseIf mstr.Trim.Contains("classgroupunkid") = False Then
    '                            mstr = mstr.Substring(0, mstr.Length - 1) & ")"
    '                            'Sohail (05 Dec 2011) -- Start
    '                            'mstr &= " AND classgroupunkid in ("
    '                            mstr &= " AND " & mstrEmployeeTableAlias & "classgroupunkid in ("
    '                            'Sohail (05 Dec 2011) -- End
    '                        End If
    '                        mstr &= lvFilterInfo.Items(i).Tag.ToString() & ","

    '                        '================= CLASSES ================='
    '                    Case btnClass.Name
    '                        If mstr.Trim.Length = 0 Then
    '                            'Sohail (05 Dec 2011) -- Start
    '                            'mstr = " classunkid in ("
    '                            mstr = " " & mstrEmployeeTableAlias & "classunkid in ("
    '                            'Sohail (05 Dec 2011) -- End
    '                        ElseIf mstr.Trim.Contains("classunkid") = False Then
    '                            mstr = mstr.Substring(0, mstr.Length - 1) & ")"
    '                            'Sohail (05 Dec 2011) -- Start
    '                            'mstr &= " AND classunkid in ("
    '                            mstr &= " AND " & mstrEmployeeTableAlias & "classunkid in ("
    '                            'Sohail (05 Dec 2011) -- End
    '                        End If
    '                        mstr &= lvFilterInfo.Items(i).Tag.ToString() & ","

    '                        '================= GRADE GROUP ================='
    '                    Case btnGradeGroup.Name
    '                        If mstr.Trim.Length = 0 Then
    '                            'Sohail (05 Dec 2011) -- Start
    '                            'mstr = " gradegroupunkid in ("
    '                            mstr = " " & mstrEmployeeTableAlias & "gradegroupunkid in ("
    '                            'Sohail (05 Dec 2011) -- End
    '                        ElseIf mstr.Trim.Contains("gradegroupunkid") = False Then
    '                            mstr = mstr.Substring(0, mstr.Length - 1) & ")"
    '                            'Sohail (05 Dec 2011) -- Start
    '                            'mstr &= " AND gradegroupunkid in ("
    '                            mstr &= " AND " & mstrEmployeeTableAlias & "gradegroupunkid in ("
    '                            'Sohail (05 Dec 2011) -- End
    '                        End If
    '                        mstr &= lvFilterInfo.Items(i).Tag.ToString() & ","

    '                        '================= GRADE ================='
    '                    Case btnGrade.Name
    '                        If mstr.Trim.Length = 0 Then
    '                            'Sohail (05 Dec 2011) -- Start
    '                            'mstr = " gradeunkid in ("
    '                            mstr = " " & mstrEmployeeTableAlias & "gradeunkid in ("
    '                            'Sohail (05 Dec 2011) -- End
    '                        ElseIf mstr.Trim.Contains("gradeunkid") = False Then
    '                            mstr = mstr.Substring(0, mstr.Length - 1) & ")"
    '                            'Sohail (05 Dec 2011) -- Start
    '                            'mstr &= " AND gradeunkid in ("
    '                            mstr &= " AND " & mstrEmployeeTableAlias & "gradeunkid in ("
    '                            'Sohail (05 Dec 2011) -- End
    '                        End If
    '                        mstr &= lvFilterInfo.Items(i).Tag.ToString() & ","

    '                        '================= GRADE LEVEL ================='
    '                    Case btnGradeLevel.Name
    '                        If mstr.Trim.Length = 0 Then
    '                            'Sohail (05 Dec 2011) -- Start
    '                            'mstr = " gradelevelunkid in ("
    '                            mstr = " " & mstrEmployeeTableAlias & "gradelevelunkid in ("
    '                            'Sohail (05 Dec 2011) -- End
    '                        ElseIf mstr.Trim.Contains("gradelevelunkid") = False Then
    '                            mstr = mstr.Substring(0, mstr.Length - 1) & ")"
    '                            'Sohail (05 Dec 2011) -- Start
    '                            'mstr &= " AND gradelevelunkid in ("
    '                            mstr &= " AND " & mstrEmployeeTableAlias & "gradelevelunkid in ("
    '                            'Sohail (05 Dec 2011) -- End
    '                        End If
    '                        mstr &= lvFilterInfo.Items(i).Tag.ToString() & ","
    '                        'S.SANDEEP [ 22 MAR 2013 ] -- START
    '                        'ENHANCEMENT : TRA CHANGES
    '                        '================= COST CENTER GROUP ================='
    '                    Case btnCostCenterGroup.Name
    '                        Dim StrIds As String = clscostcenter_master.GetCSV_CC(lvFilterInfo.Items(i).Tag.ToString())
    '                        If StrIds.Trim <> "" Then 'Sohail (04 Jan 2014)
    '                            If mstr.Trim.Length = 0 Then
    '                                mstr = " " & mstrEmployeeTableAlias & "costcenterunkid in ("
    '                            ElseIf mstr.Trim.Contains("costcenterunkid") = False Then
    '                                mstr = mstr.Substring(0, mstr.Length - 1) & ")"
    '                                mstr &= " AND " & mstrEmployeeTableAlias & "costcenterunkid in ("
    '                            End If
    '                            mstr &= StrIds & ","
    '                        End If 'Sohail (04 Jan 2014)
    '                        '================= COST CENTER ================='
    '                    Case btnCostCenter.Name
    '                        If mstr.Trim.Length = 0 Then
    '                            mstr = " " & mstrEmployeeTableAlias & "costcenterunkid in ("
    '                        ElseIf mstr.Trim.Contains("costcenterunkid") = False Then
    '                            mstr = mstr.Substring(0, mstr.Length - 1) & ")"
    '                            mstr &= " AND " & mstrEmployeeTableAlias & "costcenterunkid in ("
    '                        End If
    '                        mstr &= lvFilterInfo.Items(i).Tag.ToString() & ","
    '                        'S.SANDEEP [ 22 MAR 2013 ] -- END


    '                        'Pinkal (15-Sep-2013) -- Start
    '                        'Enhancement : TRA Changes

    '                        '================= EMPLOYEMENT TYPE ================='

    '                    Case btnEmployementType.Name
    '                        If mstr.Trim.Length = 0 Then
    '                            mstr = " " & mstrEmployeeTableAlias & "employmenttypeunkid in ("
    '                        ElseIf mstr.Trim.Contains("employmenttypeunkid") = False Then
    '                            mstr = mstr.Substring(0, mstr.Length - 1) & ")"
    '                            mstr &= " AND " & mstrEmployeeTableAlias & "employmenttypeunkid in ("
    '                        End If
    '                        mstr &= lvFilterInfo.Items(i).Tag.ToString() & ","

    '                        '================= RELIGION ================='

    '                    Case btnReligion.Name
    '                        If mstr.Trim.Length = 0 Then
    '                            mstr = " " & mstrEmployeeTableAlias & "religionunkid in ("
    '                        ElseIf mstr.Trim.Contains("religionunkid") = False Then
    '                            mstr = mstr.Substring(0, mstr.Length - 1) & ")"
    '                            mstr &= " AND " & mstrEmployeeTableAlias & "religionunkid in ("
    '                        End If
    '                        mstr &= lvFilterInfo.Items(i).Tag.ToString() & ","

    '                        'Pinkal (15-Sep-2013) -- End

    '                        'Sohail (09 Nov 2013) -- Start
    '                        'TRA - ENHANCEMENT
    '                        '================= GENDER ================='
    '                    Case btnGender.Name
    '                        If mstr.Trim.Length = 0 Then
    '                            mstr = " " & mstrEmployeeTableAlias & "gender in ("
    '                        ElseIf mstr.Trim.Contains("gender") = False Then
    '                            mstr = mstr.Substring(0, mstr.Length - 1) & ")"
    '                            mstr &= " AND " & mstrEmployeeTableAlias & "gender in ("
    '                        End If
    '                        mstr &= lvFilterInfo.Items(i).Tag.ToString() & ","

    '                        '================= NATIONALITY ================='
    '                    Case btnNationality.Name
    '                        If mstr.Trim.Length = 0 Then
    '                            mstr = " " & mstrEmployeeTableAlias & "nationalityunkid in ("
    '                        ElseIf mstr.Trim.Contains("nationality") = False Then
    '                            mstr = mstr.Substring(0, mstr.Length - 1) & ")"
    '                            mstr &= " AND " & mstrEmployeeTableAlias & "nationalityunkid in ("
    '                        End If
    '                        mstr &= lvFilterInfo.Items(i).Tag.ToString() & ","
    '                        'Sohail (09 Nov 2013) -- End

    '                        'Sohail (02 Jul 2014) -- Start
    '                        'Enhancement - Pay Type and Pay Point allocation on Advance Filter.
    '                        '================= PAY TYPE ================='
    '                    Case btnPayType.Name
    '                        If mstr.Trim.Length = 0 Then
    '                            mstr = " " & mstrEmployeeTableAlias & "paytypeunkid in ("
    '                        ElseIf mstr.Trim.Contains("paytypeunkid") = False Then
    '                            mstr = mstr.Substring(0, mstr.Length - 1) & ")"
    '                            mstr &= " AND " & mstrEmployeeTableAlias & "paytypeunkid in ("
    '                        End If
    '                        mstr &= lvFilterInfo.Items(i).Tag.ToString() & ","

    '                        '================= PAY POINT ================='
    '                    Case btnPayPoint.Name
    '                        If mstr.Trim.Length = 0 Then
    '                            mstr = " " & mstrEmployeeTableAlias & "paypointunkid in ("
    '                        ElseIf mstr.Trim.Contains("paypointunkid") = False Then
    '                            mstr = mstr.Substring(0, mstr.Length - 1) & ")"
    '                            mstr &= " AND " & mstrEmployeeTableAlias & "paypointunkid in ("
    '                        End If
    '                        mstr &= lvFilterInfo.Items(i).Tag.ToString() & ","
    '                        'Sohail (02 Jul 2014) -- End


    '                        'Pinkal (01-Oct-2014) -- Start
    '                        'Enhancement -  Changes For FDRC Report

    '                        '================= MARITAL STATUS ================='
    '                    Case btnMaritalStatus.Name
    '                        If mstr.Trim.Length = 0 Then
    '                            mstr = " " & mstrEmployeeTableAlias & "maritalstatusunkid in ("
    '                        ElseIf mstr.Trim.Contains("maritalstatusunkid") = False Then
    '                            mstr = mstr.Substring(0, mstr.Length - 1) & ")"
    '                            mstr &= " AND " & mstrEmployeeTableAlias & "maritalstatusunkid in ("
    '                        End If
    '                        mstr &= lvFilterInfo.Items(i).Tag.ToString() & ","
    '                        'Pinkal (01-Oct-2014) -- End

    '                End Select
    '            Next
    '            'Sohail (04 Jan 2014) -- Start
    '            'Enhancement - Advance Search in Self Service
    '            'mstr = mstr.Substring(0, mstr.Length - 1) & ") "
    '            If mstr.Trim <> "" Then
    '                mstr = mstr.Substring(0, mstr.Length - 1) & ") "
    '            Else
    '                mstr = " 1 = 1 "
    '            End If
    '            'Sohail (04 Jan 2014) -- End

    '            mstrFilterString = mstr
    '        End If
    '        Me.DialogResult = Windows.Forms.DialogResult.OK
    '        Me.Close()
    '    Catch ex As Exception
    '        DisplayError.Show("-1", ex.Message, "btnApply_Click", mstrModuleName)
    '    End Try
    'End Sub
    Private Sub btnApply_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnApply.Click
        Dim strQ As String = ""
        Try

            'Sohail (01 Aug 2019) -- Start
            'NMB Payroll UAT # TC006 - 76.1 - Advance search for process payroll, payslip, etc. should have search option. Currently user must scroll the entire list to select an item.
            'If lvFilterInfo.Items.Count > 0 Then
            '    Dim mintGroupID As Integer = 0
            '    Dim mstr As String = ""

            '    'If mdtAsOnDate = Nothing Then mdtAsOnDate = eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate.ToString)
            '    'strQ = " (" & mstrEmployeeTableAlias & "employeeunkid IN ( " & _
            '    '        "SELECT  Trf_AS.TrfEmpId " & _
            '    '        "FROM    ( SELECT    ETT.employeeunkid AS TrfEmpId  " & _
            '    '                          ", ISNULL(ETT.stationunkid, 0) AS stationunkid " & _
            '    '                          ", ISNULL(ETT.deptgroupunkid, 0) AS deptgroupunkid " & _
            '    '                          ", ISNULL(ETT.departmentunkid, 0) AS departmentunkid " & _
            '    '                          ", ISNULL(ETT.sectiongroupunkid, 0) AS sectiongroupunkid " & _
            '    '                          ", ISNULL(ETT.sectionunkid, 0) AS sectionunkid " & _
            '    '                          ", ISNULL(ETT.unitgroupunkid, 0) AS unitgroupunkid " & _
            '    '                          ", ISNULL(ETT.unitunkid, 0) AS unitunkid " & _
            '    '                          ", ISNULL(ETT.teamunkid, 0) AS teamunkid " & _
            '    '                          ", ISNULL(ETT.classgroupunkid, 0) AS classgroupunkid " & _
            '    '                          ", ISNULL(ETT.classunkid, 0) AS classunkid " & _
            '    '                          ", CONVERT(CHAR(8), ETT.effectivedate, 112) AS EfDt " & _
            '    '                          ", ETT.employeeunkid " & _
            '    '                          ", ROW_NUMBER() OVER ( PARTITION BY ETT.employeeunkid ORDER BY ETT.effectivedate DESC ) AS Rno " & _
            '    '                  "FROM      " & FinancialYear._Object._DatabaseName & "..hremployee_transfer_tran AS ETT " & _
            '    '                  "WHERE     isvoid = 0 " & _
            '    '                            "AND CONVERT(CHAR(8), effectivedate, 112) <= '" & eZeeDate.convertDate(mdtAsOnDate).ToString & "' " & _
            '    '                ") AS Trf_AS " & _
            '    '                "JOIN ( SELECT   ECT.employeeunkid AS CatEmpId  " & _
            '    '                              ", ECT.jobgroupunkid " & _
            '    '                              ", ECT.jobunkid " & _
            '    '                              ", CONVERT(CHAR(8), ECT.effectivedate, 112) AS CEfDt " & _
            '    '                              ", ECT.employeeunkid " & _
            '    '                              ", ROW_NUMBER() OVER ( PARTITION BY ECT.employeeunkid ORDER BY ECT.effectivedate DESC ) AS Rno " & _
            '    '                       "FROM     " & FinancialYear._Object._DatabaseName & "..hremployee_categorization_tran AS ECT " & _
            '    '                       "WHERE    isvoid = 0 " & _
            '    '                                "AND CONVERT(CHAR(8), effectivedate, 112) <= '" & eZeeDate.convertDate(mdtAsOnDate).ToString & "' " & _
            '    '                     ") AS RUCat ON Trf_AS.employeeunkid = RUCat.employeeunkid " & _
            '    '                "JOIN ( SELECT  CCT.employeeunkid AS CCTEmpId  " & _
            '    '                             ", CCT.cctranheadvalueid AS costcenterunkid " & _
            '    '                             ", CONVERT(CHAR(8), CCT.effectivedate, 112) AS CTEfDt " & _
            '    '                             ", ROW_NUMBER() OVER ( PARTITION BY CCT.employeeunkid ORDER BY CCT.effectivedate DESC ) AS Rno " & _
            '    '                             ", CASE WHEN CCT.rehiretranunkid > 0 THEN RH.name " & _
            '    '                                    "ELSE cfcommon_master.name " & _
            '    '                               "END AS cc_reason " & _
            '    '                        "FROM   " & FinancialYear._Object._DatabaseName & "..hremployee_cctranhead_tran AS CCT " & _
            '    '                               "LEFT JOIN " & FinancialYear._Object._DatabaseName & "..cfcommon_master AS RH ON RH.masterunkid = CCT.changereasonunkid " & _
            '    '                                                                  "AND RH.mastertype = " & clsCommon_Master.enCommonMaster.RE_HIRE & " " & _
            '    '                               "LEFT JOIN " & FinancialYear._Object._DatabaseName & "..cfcommon_master ON cfcommon_master.masterunkid = CCT.changereasonunkid " & _
            '    '                                                            "AND cfcommon_master.mastertype = " & clsCommon_Master.enCommonMaster.COST_CENTER & " " & _
            '    '                               "LEFT JOIN " & FinancialYear._Object._DatabaseName & "..prcostcenter_master ON CCT.cctranheadvalueid = costcenterunkid " & _
            '    '                        "WHERE  CCT.isvoid = 0 " & _
            '    '                               "AND CCT.istransactionhead = 0 " & _
            '    '                               "AND CONVERT(CHAR(8), CCT.effectivedate, 112) <= '" & eZeeDate.convertDate(mdtAsOnDate).ToString & "' " & _
            '    '                     ") AS CCT ON Trf_AS.employeeunkid = CCT.CCTEmpId " & _
            '    '                "JOIN ( SELECT  gradegroupunkid  " & _
            '    '                             ", gradeunkid " & _
            '    '                             ", gradelevelunkid " & _
            '    '                             ", employeeunkid " & _
            '    '                             ", ROW_NUMBER() OVER ( PARTITION BY employeeunkid ORDER BY incrementdate DESC, salaryincrementtranunkid DESC ) AS Rno " & _
            '    '                        "FROM   " & FinancialYear._Object._DatabaseName & "..prsalaryincrement_tran " & _
            '    '                        "WHERE  isvoid = 0 " & _
            '    '                               "AND isapproved = 1 " & _
            '    '                               "AND CONVERT(CHAR(8), incrementdate, 112) <= '" & eZeeDate.convertDate(mdtAsOnDate).ToString & "' " & _
            '    '                     ") AS GRD ON Trf_AS.employeeunkid = GRD.employeeunkid " & _
            '    '        "WHERE   Trf_AS.Rno = 1 " & _
            '    '                "AND RUCat.Rno = 1 " & _
            '    '                "AND CCT.Rno = 1 " & _
            '    '                "AND GRD.Rno = 1 "

            '    For i As Integer = 0 To lvFilterInfo.Items.Count - 1
            '        Select Case lvFilterInfo.Items(i).SubItems(objcolhbtnName.Index).Text
            '            '================= BRANCH ================='
            '            Case btnBranch.Name
            '                If mstr.Trim.Length = 0 Then
            '                    mstr = " ADF.stationunkid in ("
            '                ElseIf mstr.Trim.Contains("stationunkid") = False Then
            '                    mstr = mstr.Substring(0, mstr.Length - 1) & ") "
            '                    mstr &= " AND ADF.stationunkid in ("
            '                End If
            '                mstr &= lvFilterInfo.Items(i).Tag.ToString() & ","

            '                '================= DEPARTMENT ================='
            '            Case btnDept.Name
            '                If mstr.Trim.Length = 0 Then
            '                    mstr = " ADF.departmentunkid in ("
            '                ElseIf mstr.Trim.Contains("departmentunkid") = False Then
            '                    mstr = mstr.Substring(0, mstr.Length - 1) & ") "
            '                    mstr &= " AND ADF.departmentunkid in ("
            '                End If
            '                mstr &= lvFilterInfo.Items(i).Tag.ToString() & ","

            '                '================= DEPARTMENT GROUP ================='
            '            Case btnDeptGrp.Name
            '                If mstr.Trim.Length = 0 Then
            '                    mstr = " ADF.deptgroupunkid in ("
            '                ElseIf mstr.Trim.Contains("deptgroupunkid") = False Then
            '                    mstr = mstr.Substring(0, mstr.Length - 1) & ") "
            '                    mstr &= "  AND ADF.deptgroupunkid in ("
            '                End If
            '                mstr &= lvFilterInfo.Items(i).Tag.ToString() & ","

            '                '================= SECTION ================='
            '            Case btnSection.Name
            '                If mstr.Trim.Length = 0 Then
            '                    mstr = " ADF.sectionunkid in ("
            '                ElseIf mstr.Trim.Contains("sectionunkid") = False Then
            '                    mstr = mstr.Substring(0, mstr.Length - 1) & ") "
            '                    mstr &= " AND ADF.sectionunkid in ("
            '                End If
            '                mstr &= lvFilterInfo.Items(i).Tag.ToString() & ","

            '                '================= UNIT ================='
            '            Case btnUnit.Name
            '                If mstr.Trim.Length = 0 Then
            '                    mstr = " ADF.unitunkid in ("
            '                ElseIf mstr.Trim.Contains("unitunkid") = False Then
            '                    mstr = mstr.Substring(0, mstr.Length - 1) & ") "
            '                    mstr &= " AND ADF.unitunkid in ("
            '                End If
            '                mstr &= lvFilterInfo.Items(i).Tag.ToString() & ","

            '                '================= JOB ================='
            '            Case btnJob.Name
            '                If mstr.Trim.Length = 0 Then
            '                    mstr = " ADF.jobunkid in ("
            '                ElseIf mstr.Trim.Contains("jobunkid") = False Then
            '                    mstr = mstr.Substring(0, mstr.Length - 1) & ") "
            '                    mstr &= " AND ADF.jobunkid in ("
            '                End If
            '                mstr &= lvFilterInfo.Items(i).Tag.ToString() & ","

            '                '================= SECTION GROUP ================='
            '            Case btnSectionGroup.Name
            '                If mstr.Trim.Length = 0 Then
            '                    mstr = " ADF.sectiongroupunkid in ("
            '                ElseIf mstr.Trim.Contains("sectiongroupunkid") = False Then
            '                    mstr = mstr.Substring(0, mstr.Length - 1) & ") "
            '                    mstr &= " AND ADF.sectiongroupunkid in ("
            '                End If
            '                mstr &= lvFilterInfo.Items(i).Tag.ToString() & ","

            '                '================= UNIT GROUP ================='
            '            Case btnUnitGroup.Name
            '                If mstr.Trim.Length = 0 Then
            '                    mstr = " ADF.unitgroupunkid in ("
            '                ElseIf mstr.Trim.Contains("unitgroupunkid") = False Then
            '                    mstr = mstr.Substring(0, mstr.Length - 1) & ") "
            '                    mstr &= " AND ADF.unitgroupunkid in ("
            '                End If
            '                mstr &= lvFilterInfo.Items(i).Tag.ToString() & ","

            '                '================= TEAM ================='
            '            Case btnTeam.Name
            '                If mstr.Trim.Length = 0 Then
            '                    mstr = " ADF.teamunkid in ("
            '                ElseIf mstr.Trim.Contains("teamunkid") = False Then
            '                    mstr = mstr.Substring(0, mstr.Length - 1) & ") "
            '                    mstr &= " AND ADF.teamunkid in ("
            '                End If
            '                mstr &= lvFilterInfo.Items(i).Tag.ToString() & ","

            '                '================= JOB GROUP ================='
            '            Case btnJobGroup.Name
            '                If mstr.Trim.Length = 0 Then
            '                    mstr = " ADF.jobgroupunkid in ("
            '                ElseIf mstr.Trim.Contains("jobgroupunkid") = False Then
            '                    mstr = mstr.Substring(0, mstr.Length - 1) & ")"
            '                    mstr &= " AND ADF.jobgroupunkid in ("
            '                End If
            '                mstr &= lvFilterInfo.Items(i).Tag.ToString() & ","


            '                '================= CLASS GROUP ================='
            '            Case btnClassGroup.Name
            '                If mstr.Trim.Length = 0 Then
            '                    mstr = " ADF.classgroupunkid in ("
            '                ElseIf mstr.Trim.Contains("classgroupunkid") = False Then
            '                    mstr = mstr.Substring(0, mstr.Length - 1) & ")"
            '                    mstr &= " AND ADF.classgroupunkid in ("
            '                End If
            '                mstr &= lvFilterInfo.Items(i).Tag.ToString() & ","

            '                '================= CLASSES ================='
            '            Case btnClass.Name
            '                If mstr.Trim.Length = 0 Then
            '                    mstr = " ADF.classunkid in ("
            '                ElseIf mstr.Trim.Contains("classunkid") = False Then
            '                    mstr = mstr.Substring(0, mstr.Length - 1) & ")"
            '                    mstr &= " AND ADF.classunkid in ("
            '                End If
            '                mstr &= lvFilterInfo.Items(i).Tag.ToString() & ","

            '                '================= GRADE GROUP ================='
            '            Case btnGradeGroup.Name
            '                If mstr.Trim.Length = 0 Then
            '                    mstr = " ADF.gradegroupunkid in ("
            '                ElseIf mstr.Trim.Contains("gradegroupunkid") = False Then
            '                    mstr = mstr.Substring(0, mstr.Length - 1) & ")"
            '                    mstr &= " AND ADF.gradegroupunkid in ("
            '                End If
            '                mstr &= lvFilterInfo.Items(i).Tag.ToString() & ","

            '                '================= GRADE ================='
            '            Case btnGrade.Name
            '                If mstr.Trim.Length = 0 Then
            '                    mstr = " ADF.gradeunkid in ("
            '                ElseIf mstr.Trim.Contains("gradeunkid") = False Then
            '                    mstr = mstr.Substring(0, mstr.Length - 1) & ")"
            '                    mstr &= " AND ADF.gradeunkid in ("
            '                End If
            '                mstr &= lvFilterInfo.Items(i).Tag.ToString() & ","

            '                '================= GRADE LEVEL ================='
            '            Case btnGradeLevel.Name
            '                If mstr.Trim.Length = 0 Then
            '                    mstr = " ADF.gradelevelunkid in ("
            '                ElseIf mstr.Trim.Contains("gradelevelunkid") = False Then
            '                    mstr = mstr.Substring(0, mstr.Length - 1) & ")"
            '                    mstr &= " AND ADF.gradelevelunkid in ("
            '                End If
            '                mstr &= lvFilterInfo.Items(i).Tag.ToString() & ","

            '                '================= COST CENTER GROUP ================='
            '            Case btnCostCenterGroup.Name
            '                Dim StrIds As String = clscostcenter_master.GetCSV_CC(lvFilterInfo.Items(i).Tag.ToString())
            '                If StrIds.Trim <> "" Then
            '                    If mstr.Trim.Length = 0 Then
            '                        mstr = " ADF.costcenterunkid in ("
            '                    ElseIf mstr.Trim.Contains("costcenterunkid") = False Then
            '                        mstr = mstr.Substring(0, mstr.Length - 1) & ")"
            '                        mstr &= " AND ADF.costcenterunkid in ("
            '                    End If
            '                    mstr &= StrIds & ","
            '                End If

            '                '================= COST CENTER ================='
            '            Case btnCostCenter.Name
            '                If mstr.Trim.Length = 0 Then
            '                    mstr = " ADF.costcenterunkid in ("
            '                ElseIf mstr.Trim.Contains("costcenterunkid") = False Then
            '                    mstr = mstr.Substring(0, mstr.Length - 1) & ")"
            '                    mstr &= " AND ADF.costcenterunkid in ("
            '                End If
            '                mstr &= lvFilterInfo.Items(i).Tag.ToString() & ","

            '                '================= EMPLOYEMENT TYPE ================='
            '            Case btnEmployementType.Name
            '                If mstr.Trim.Length = 0 Then
            '                    mstr = " " & mstrEmployeeTableAlias & "employmenttypeunkid in ("
            '                ElseIf mstr.Trim.Contains("employmenttypeunkid") = False Then
            '                    mstr = mstr.Substring(0, mstr.Length - 1) & ")"
            '                    mstr &= " AND " & mstrEmployeeTableAlias & "employmenttypeunkid in ("
            '                End If
            '                mstr &= lvFilterInfo.Items(i).Tag.ToString() & ","

            '                '================= RELIGION ================='
            '            Case btnReligion.Name
            '                If mstr.Trim.Length = 0 Then
            '                    mstr = " " & mstrEmployeeTableAlias & "religionunkid in ("
            '                ElseIf mstr.Trim.Contains("religionunkid") = False Then
            '                    mstr = mstr.Substring(0, mstr.Length - 1) & ")"
            '                    mstr &= " AND " & mstrEmployeeTableAlias & "religionunkid in ("
            '                End If
            '                mstr &= lvFilterInfo.Items(i).Tag.ToString() & ","

            '                '================= GENDER ================='
            '            Case btnGender.Name
            '                If mstr.Trim.Length = 0 Then
            '                    mstr = " " & mstrEmployeeTableAlias & "gender in ("
            '                ElseIf mstr.Trim.Contains("gender") = False Then
            '                    mstr = mstr.Substring(0, mstr.Length - 1) & ")"
            '                    mstr &= " AND " & mstrEmployeeTableAlias & "gender in ("
            '                End If
            '                mstr &= lvFilterInfo.Items(i).Tag.ToString() & ","

            '                '================= NATIONALITY ================='
            '            Case btnNationality.Name
            '                If mstr.Trim.Length = 0 Then
            '                    mstr = " " & mstrEmployeeTableAlias & "nationalityunkid in ("
            '                ElseIf mstr.Trim.Contains("nationality") = False Then
            '                    mstr = mstr.Substring(0, mstr.Length - 1) & ")"
            '                    mstr &= " AND " & mstrEmployeeTableAlias & "nationalityunkid in ("
            '                End If
            '                mstr &= lvFilterInfo.Items(i).Tag.ToString() & ","

            '                '================= PAY TYPE ================='
            '            Case btnPayType.Name
            '                If mstr.Trim.Length = 0 Then
            '                    mstr = " " & mstrEmployeeTableAlias & "paytypeunkid in ("
            '                ElseIf mstr.Trim.Contains("paytypeunkid") = False Then
            '                    mstr = mstr.Substring(0, mstr.Length - 1) & ")"
            '                    mstr &= " AND " & mstrEmployeeTableAlias & "paytypeunkid in ("
            '                End If
            '                mstr &= lvFilterInfo.Items(i).Tag.ToString() & ","

            '                '================= PAY POINT ================='
            '            Case btnPayPoint.Name
            '                If mstr.Trim.Length = 0 Then
            '                    mstr = " " & mstrEmployeeTableAlias & "paypointunkid in ("
            '                ElseIf mstr.Trim.Contains("paypointunkid") = False Then
            '                    mstr = mstr.Substring(0, mstr.Length - 1) & ")"
            '                    mstr &= " AND " & mstrEmployeeTableAlias & "paypointunkid in ("
            '                End If
            '                mstr &= lvFilterInfo.Items(i).Tag.ToString() & ","

            '                '================= MARITAL STATUS ================='
            '            Case btnMaritalStatus.Name
            '                If mstr.Trim.Length = 0 Then
            '                    mstr = " " & mstrEmployeeTableAlias & "maritalstatusunkid in ("
            '                ElseIf mstr.Trim.Contains("maritalstatusunkid") = False Then
            '                    mstr = mstr.Substring(0, mstr.Length - 1) & ")"
            '                    mstr &= " AND " & mstrEmployeeTableAlias & "maritalstatusunkid in ("
            '                End If
            '                mstr &= lvFilterInfo.Items(i).Tag.ToString() & ","

            '                'S.SANDEEP |04-JUN-2019| -- START
            '                'ISSUE/ENHANCEMENT : [Relation filters on Allocation Advance search]
            '                '================= RELATION FROM DEPANDANTS ================='
            '            Case btnRelationFromDpndt.Name
            '                Dim StrIds As String = clsDependants_Beneficiary_tran.GetCSV_EmpIds(CInt(lvFilterInfo.Items(i).Tag.ToString()))
            '                If StrIds.Trim <> "" Then
            '                    If mstr.Trim.Length = 0 Then
            '                        mstr = " ADF.employeeunkid in ("
            '                    ElseIf mstr.Trim.Contains("employeeunkid") = False Then
            '                        mstr = mstr.Substring(0, mstr.Length - 1) & ")"
            '                        mstr &= " AND ADF.employeeunkid in ("
            '                    End If
            '                    mstr &= StrIds & ","
            '                Else
            '                    If mstr.Trim.Length = 0 Then
            '                        mstr = " ADF.employeeunkid in ("
            '                    ElseIf mstr.Trim.Contains("employeeunkid") = False Then
            '                        mstr = mstr.Substring(0, mstr.Length - 1) & ")"
            '                        mstr &= " AND ADF.employeeunkid in ("
            '                    End If
            '                    mstr &= "0,"
            '                End If
            '                'S.SANDEEP |04-JUN-2019| -- END
            '        End Select
            '    Next

            '    If mstr.Trim <> "" Then
            '        mstr = mstr.Substring(0, mstr.Length - 1) & ") "
            '    Else
            '        mstr = " 1 = 1 "
            '    End If

            '    'mstrFilterString = strQ & " AND " & mstr & " ) ) "
            '    mstrFilterString = mstr
            'End If
            Dim dRow() As DataRow = mdtFilterInfo.Select("IsGroup = 0")
            If dRow.Length > 0 Then
                Dim mstr As String = ""
                For Each drRow As DataRow In dRow
                    'Sohail (14 Nov 2019) -- Start
                    'NMB UAT Enhancement # : Bind Allocation to training course.
                    'Select Case drRow.Item("GroupName").ToString.ToUpper
                    '    Case btnBranch.Text.ToUpper
                    Select Case CInt(drRow.Item("GroupId"))
                        Case enAdvanceFilterAllocation.BRANCH
                            'Sohail (14 Nov 2019) -- End
                            If mstr.Trim.Length = 0 Then
                                mstr = " ADF.stationunkid in ("
                            ElseIf mstr.Trim.Contains("stationunkid") = False Then
                                mstr = mstr.Substring(0, mstr.Length - 1) & ") "
                                mstr &= " AND ADF.stationunkid in ("
                            End If
                            mstr &= drRow.Item("Id").ToString & ","

                            'Sohail (14 Nov 2019) -- Start
                            'NMB UAT Enhancement # : Bind Allocation to training course.
                            'Case btnDept.Text.ToUpper
                        Case enAdvanceFilterAllocation.DEPARTMENT
                            'Sohail (14 Nov 2019) -- End
                            If mstr.Trim.Length = 0 Then
                                mstr = " ADF.departmentunkid in ("
                            ElseIf mstr.Trim.Contains("departmentunkid") = False Then
                                mstr = mstr.Substring(0, mstr.Length - 1) & ") "
                                mstr &= " AND ADF.departmentunkid in ("
                            End If
                            mstr &= drRow.Item("Id").ToString & ","

                            'Sohail (14 Nov 2019) -- Start
                            'NMB UAT Enhancement # : Bind Allocation to training course.
                            'Case btnDeptGrp.Text.ToUpper
                        Case enAdvanceFilterAllocation.DEPARTMENT_GROUP
                            'Sohail (14 Nov 2019) -- End
                            If mstr.Trim.Length = 0 Then
                                mstr = " ADF.deptgroupunkid in ("
                            ElseIf mstr.Trim.Contains("deptgroupunkid") = False Then
                                mstr = mstr.Substring(0, mstr.Length - 1) & ") "
                                mstr &= "  AND ADF.deptgroupunkid in ("
                            End If
                            mstr &= drRow.Item("Id").ToString & ","

                            'Sohail (14 Nov 2019) -- Start
                            'NMB UAT Enhancement # : Bind Allocation to training course.
                            'Case btnSection.Text.ToUpper
                        Case enAdvanceFilterAllocation.SECTION
                            'Sohail (14 Nov 2019) -- End
                            If mstr.Trim.Length = 0 Then
                                mstr = " ADF.sectionunkid in ("
                            ElseIf mstr.Trim.Contains("sectionunkid") = False Then
                                mstr = mstr.Substring(0, mstr.Length - 1) & ") "
                                mstr &= " AND ADF.sectionunkid in ("
                            End If
                            mstr &= drRow.Item("Id").ToString & ","

                            'Sohail (14 Nov 2019) -- Start
                            'NMB UAT Enhancement # : Bind Allocation to training course.
                            'Case btnUnit.Text.ToUpper
                        Case enAdvanceFilterAllocation.UNIT
                            'Sohail (14 Nov 2019) -- End
                            If mstr.Trim.Length = 0 Then
                                mstr = " ADF.unitunkid in ("
                            ElseIf mstr.Trim.Contains("unitunkid") = False Then
                                mstr = mstr.Substring(0, mstr.Length - 1) & ") "
                                mstr &= " AND ADF.unitunkid in ("
                            End If
                            mstr &= drRow.Item("Id").ToString & ","

                            'Sohail (14 Nov 2019) -- Start
                            'NMB UAT Enhancement # : Bind Allocation to training course.
                            'Case btnJob.Text.ToUpper
                        Case enAdvanceFilterAllocation.JOBS
                            'Sohail (14 Nov 2019) -- End
                            If mstr.Trim.Length = 0 Then
                                mstr = " ADF.jobunkid in ("
                            ElseIf mstr.Trim.Contains("jobunkid") = False Then
                                mstr = mstr.Substring(0, mstr.Length - 1) & ") "
                                mstr &= " AND ADF.jobunkid in ("
                            End If
                            mstr &= drRow.Item("Id").ToString & ","

                            'Sohail (14 Nov 2019) -- Start
                            'NMB UAT Enhancement # : Bind Allocation to training course.
                            'Case btnSectionGroup.Text.ToUpper
                        Case enAdvanceFilterAllocation.SECTION_GROUP
                            'Sohail (14 Nov 2019) -- End
                            If mstr.Trim.Length = 0 Then
                                mstr = " ADF.sectiongroupunkid in ("
                            ElseIf mstr.Trim.Contains("sectiongroupunkid") = False Then
                                mstr = mstr.Substring(0, mstr.Length - 1) & ") "
                                mstr &= " AND ADF.sectiongroupunkid in ("
                            End If
                            mstr &= drRow.Item("Id").ToString & ","

                            'Sohail (14 Nov 2019) -- Start
                            'NMB UAT Enhancement # : Bind Allocation to training course.
                            'Case btnUnitGroup.Text.ToUpper
                        Case enAdvanceFilterAllocation.UNIT_GROUP
                            'Sohail (14 Nov 2019) -- End
                            If mstr.Trim.Length = 0 Then
                                mstr = " ADF.unitgroupunkid in ("
                            ElseIf mstr.Trim.Contains("unitgroupunkid") = False Then
                                mstr = mstr.Substring(0, mstr.Length - 1) & ") "
                                mstr &= " AND ADF.unitgroupunkid in ("
                            End If
                            mstr &= drRow.Item("Id").ToString & ","

                            'Sohail (14 Nov 2019) -- Start
                            'NMB UAT Enhancement # : Bind Allocation to training course.
                            'Case btnTeam.Text.ToUpper
                        Case enAdvanceFilterAllocation.TEAM
                            'Sohail (14 Nov 2019) -- End
                            If mstr.Trim.Length = 0 Then
                                mstr = " ADF.teamunkid in ("
                            ElseIf mstr.Trim.Contains("teamunkid") = False Then
                                mstr = mstr.Substring(0, mstr.Length - 1) & ") "
                                mstr &= " AND ADF.teamunkid in ("
                            End If
                            mstr &= drRow.Item("Id").ToString & ","

                            'Sohail (14 Nov 2019) -- Start
                            'NMB UAT Enhancement # : Bind Allocation to training course.
                            'Case btnJobGroup.Text.ToUpper
                        Case enAdvanceFilterAllocation.JOB_GROUP
                            'Sohail (14 Nov 2019) -- End
                            If mstr.Trim.Length = 0 Then
                                mstr = " ADF.jobgroupunkid in ("
                            ElseIf mstr.Trim.Contains("jobgroupunkid") = False Then
                                mstr = mstr.Substring(0, mstr.Length - 1) & ")"
                                mstr &= " AND ADF.jobgroupunkid in ("
                            End If
                            mstr &= drRow.Item("Id").ToString & ","

                            'Sohail (14 Nov 2019) -- Start
                            'NMB UAT Enhancement # : Bind Allocation to training course.
                            'Case btnClassGroup.Text.ToUpper
                        Case enAdvanceFilterAllocation.CLASS_GROUP
                            'Sohail (14 Nov 2019) -- End
                            If mstr.Trim.Length = 0 Then
                                mstr = " ADF.classgroupunkid in ("
                            ElseIf mstr.Trim.Contains("classgroupunkid") = False Then
                                mstr = mstr.Substring(0, mstr.Length - 1) & ")"
                                mstr &= " AND ADF.classgroupunkid in ("
                            End If
                            mstr &= drRow.Item("Id").ToString & ","

                            'Sohail (14 Nov 2019) -- Start
                            'NMB UAT Enhancement # : Bind Allocation to training course.
                            'Case btnClass.Text.ToUpper
                        Case enAdvanceFilterAllocation.CLASSES
                            'Sohail (14 Nov 2019) -- End
                            If mstr.Trim.Length = 0 Then
                                mstr = " ADF.classunkid in ("
                            ElseIf mstr.Trim.Contains("classunkid") = False Then
                                mstr = mstr.Substring(0, mstr.Length - 1) & ")"
                                mstr &= " AND ADF.classunkid in ("
                            End If
                            mstr &= drRow.Item("Id").ToString & ","

                            'Sohail (14 Nov 2019) -- Start
                            'NMB UAT Enhancement # : Bind Allocation to training course.
                            'Case btnGradeGroup.Text.ToUpper
                        Case enAdvanceFilterAllocation.GRADE_GROUP
                            'Sohail (14 Nov 2019) -- End
                            If mstr.Trim.Length = 0 Then
                                mstr = " ADF.gradegroupunkid in ("
                            ElseIf mstr.Trim.Contains("gradegroupunkid") = False Then
                                mstr = mstr.Substring(0, mstr.Length - 1) & ")"
                                mstr &= " AND ADF.gradegroupunkid in ("
                            End If
                            mstr &= drRow.Item("Id").ToString & ","

                            'Sohail (14 Nov 2019) -- Start
                            'NMB UAT Enhancement # : Bind Allocation to training course.
                            'Case btnGrade.Text.ToUpper
                        Case enAdvanceFilterAllocation.GRADE
                            'Sohail (14 Nov 2019) -- End
                            If mstr.Trim.Length = 0 Then
                                mstr = " ADF.gradeunkid in ("
                            ElseIf mstr.Trim.Contains("gradeunkid") = False Then
                                mstr = mstr.Substring(0, mstr.Length - 1) & ")"
                                mstr &= " AND ADF.gradeunkid in ("
                            End If
                            mstr &= drRow.Item("Id").ToString & ","

                            'Sohail (14 Nov 2019) -- Start
                            'NMB UAT Enhancement # : Bind Allocation to training course.
                            'Case btnGradeLevel.Text.ToUpper
                        Case enAdvanceFilterAllocation.GRADE_LEVEL
                            'Sohail (14 Nov 2019) -- End
                            If mstr.Trim.Length = 0 Then
                                mstr = " ADF.gradelevelunkid in ("
                            ElseIf mstr.Trim.Contains("gradelevelunkid") = False Then
                                mstr = mstr.Substring(0, mstr.Length - 1) & ")"
                                mstr &= " AND ADF.gradelevelunkid in ("
                            End If
                            mstr &= drRow.Item("Id").ToString & ","

                            'Sohail (14 Nov 2019) -- Start
                            'NMB UAT Enhancement # : Bind Allocation to training course.
                            'Case btnCostCenterGroup.Text.ToUpper
                        Case enAdvanceFilterAllocation.COST_CENTER_GROUP
                            'Sohail (14 Nov 2019) -- End
                            Dim StrIds As String = clscostcenter_master.GetCSV_CC(drRow.Item("Id").ToString)
                            If StrIds.Trim <> "" Then
                                If mstr.Trim.Length = 0 Then
                                    mstr = " ADF.costcenterunkid in ("
                                ElseIf mstr.Trim.Contains("costcenterunkid") = False Then
                                    mstr = mstr.Substring(0, mstr.Length - 1) & ")"
                                    mstr &= " AND ADF.costcenterunkid in ("
                                End If
                                mstr &= StrIds & ","
                            End If

                            'Sohail (14 Nov 2019) -- Start
                            'NMB UAT Enhancement # : Bind Allocation to training course.
                            'Case btnCostCenter.Text.ToUpper
                        Case enAdvanceFilterAllocation.COST_CENTER
                            'Sohail (14 Nov 2019) -- End
                            If mstr.Trim.Length = 0 Then
                                mstr = " ADF.costcenterunkid in ("
                            ElseIf mstr.Trim.Contains("costcenterunkid") = False Then
                                mstr = mstr.Substring(0, mstr.Length - 1) & ")"
                                mstr &= " AND ADF.costcenterunkid in ("
                            End If
                            mstr &= drRow.Item("Id").ToString & ","

                            'Sohail (14 Nov 2019) -- Start
                            'NMB UAT Enhancement # : Bind Allocation to training course.
                            'Case btnEmployementType.Text.ToUpper
                        Case enAdvanceFilterAllocation.EMPLOYEMENT_TYPE
                            'Sohail (14 Nov 2019) -- End
                            If mstr.Trim.Length = 0 Then
                                mstr = " " & mstrEmployeeTableAlias & "employmenttypeunkid in ("
                            ElseIf mstr.Trim.Contains("employmenttypeunkid") = False Then
                                mstr = mstr.Substring(0, mstr.Length - 1) & ")"
                                mstr &= " AND " & mstrEmployeeTableAlias & "employmenttypeunkid in ("
                            End If
                            mstr &= drRow.Item("Id").ToString & ","

                            'Sohail (14 Nov 2019) -- Start
                            'NMB UAT Enhancement # : Bind Allocation to training course.
                            'Case btnReligion.Text.ToUpper
                        Case enAdvanceFilterAllocation.RELIGION
                            'Sohail (14 Nov 2019) -- End
                            If mstr.Trim.Length = 0 Then
                                mstr = " " & mstrEmployeeTableAlias & "religionunkid in ("
                            ElseIf mstr.Trim.Contains("religionunkid") = False Then
                                mstr = mstr.Substring(0, mstr.Length - 1) & ")"
                                mstr &= " AND " & mstrEmployeeTableAlias & "religionunkid in ("
                            End If
                            mstr &= drRow.Item("Id").ToString & ","

                            'Sohail (14 Nov 2019) -- Start
                            'NMB UAT Enhancement # : Bind Allocation to training course.
                            'Case btnGender.Text.ToUpper
                        Case enAdvanceFilterAllocation.GENDER
                            'Sohail (14 Nov 2019) -- End
                            If mstr.Trim.Length = 0 Then
                                mstr = " " & mstrEmployeeTableAlias & "gender in ("
                            ElseIf mstr.Trim.Contains("gender") = False Then
                                mstr = mstr.Substring(0, mstr.Length - 1) & ")"
                                mstr &= " AND " & mstrEmployeeTableAlias & "gender in ("
                            End If
                            mstr &= drRow.Item("Id").ToString & ","

                            'Sohail (14 Nov 2019) -- Start
                            'NMB UAT Enhancement # : Bind Allocation to training course.
                            'Case btnNationality.Text.ToUpper
                        Case enAdvanceFilterAllocation.NATIONALITY
                            'Sohail (14 Nov 2019) -- End
                            If mstr.Trim.Length = 0 Then
                                mstr = " " & mstrEmployeeTableAlias & "nationalityunkid in ("
                            ElseIf mstr.Trim.Contains("nationality") = False Then
                                mstr = mstr.Substring(0, mstr.Length - 1) & ")"
                                mstr &= " AND " & mstrEmployeeTableAlias & "nationalityunkid in ("
                            End If
                            mstr &= drRow.Item("Id").ToString & ","

                            'Sohail (14 Nov 2019) -- Start
                            'NMB UAT Enhancement # : Bind Allocation to training course.
                            'Case btnPayType.Text.ToUpper
                        Case enAdvanceFilterAllocation.PAY_TYPE
                            'Sohail (14 Nov 2019) -- End
                            If mstr.Trim.Length = 0 Then
                                mstr = " " & mstrEmployeeTableAlias & "paytypeunkid in ("
                            ElseIf mstr.Trim.Contains("paytypeunkid") = False Then
                                mstr = mstr.Substring(0, mstr.Length - 1) & ")"
                                mstr &= " AND " & mstrEmployeeTableAlias & "paytypeunkid in ("
                            End If
                            mstr &= drRow.Item("Id").ToString & ","

                            'Sohail (14 Nov 2019) -- Start
                            'NMB UAT Enhancement # : Bind Allocation to training course.
                            'Case btnPayPoint.Text.ToUpper
                        Case enAdvanceFilterAllocation.PAY_POINT
                            'Sohail (14 Nov 2019) -- End
                            If mstr.Trim.Length = 0 Then
                                mstr = " " & mstrEmployeeTableAlias & "paypointunkid in ("
                            ElseIf mstr.Trim.Contains("paypointunkid") = False Then
                                mstr = mstr.Substring(0, mstr.Length - 1) & ")"
                                mstr &= " AND " & mstrEmployeeTableAlias & "paypointunkid in ("
                            End If
                            mstr &= drRow.Item("Id").ToString & ","

                            'Sohail (14 Nov 2019) -- Start
                            'NMB UAT Enhancement # : Bind Allocation to training course.
                            'Case btnMaritalStatus.Text.ToUpper
                        Case enAdvanceFilterAllocation.MARITAL_STATUS
                            'Sohail (14 Nov 2019) -- End
                            If mstr.Trim.Length = 0 Then
                                mstr = " " & mstrEmployeeTableAlias & "maritalstatusunkid in ("
                            ElseIf mstr.Trim.Contains("maritalstatusunkid") = False Then
                                mstr = mstr.Substring(0, mstr.Length - 1) & ")"
                                mstr &= " AND " & mstrEmployeeTableAlias & "maritalstatusunkid in ("
                            End If
                            mstr &= drRow.Item("Id").ToString & ","

                            'Sohail (14 Nov 2019) -- Start
                            'NMB UAT Enhancement # : Bind Allocation to training course.
                            'Case btnRelationFromDpndt.Text.ToUpper
                        Case enAdvanceFilterAllocation.RELATION_FROM_DEPENDENTS
                            'Sohail (14 Nov 2019) -- End
                            Dim StrIds As String = clsDependants_Beneficiary_tran.GetCSV_EmpIds(CInt(drRow.Item("Id").ToString))
                            If StrIds.Trim <> "" Then
                                If mstr.Trim.Length = 0 Then
                                    mstr = " ADF.employeeunkid in ("
                                ElseIf mstr.Trim.Contains("employeeunkid") = False Then
                                    mstr = mstr.Substring(0, mstr.Length - 1) & ")"
                                    mstr &= " AND ADF.employeeunkid in ("
                                End If
                                mstr &= StrIds & ","
                            Else
                                If mstr.Trim.Length = 0 Then
                                    mstr = " ADF.employeeunkid in ("
                                ElseIf mstr.Trim.Contains("employeeunkid") = False Then
                                    mstr = mstr.Substring(0, mstr.Length - 1) & ")"
                                    mstr &= " AND ADF.employeeunkid in ("
                                End If
                                mstr &= "0,"
                            End If

                            'Sohail (14 Nov 2019) -- Start
                            'NMB UAT Enhancement # : Bind Allocation to training course.
                        Case enAdvanceFilterAllocation.SKILL
                            Dim StrIds As String = clsEmployee_Skill_Tran.GetCSV_EmpIds(CInt(drRow.Item("Id").ToString))
                            If StrIds.Trim <> "" Then
                                If mstr.Trim.Length = 0 Then
                                    mstr = " ADF.employeeunkid in ("
                                ElseIf mstr.Trim.Contains("employeeunkid") = False Then
                                    mstr = mstr.Substring(0, mstr.Length - 1) & ")"
                                    mstr &= " AND ADF.employeeunkid in ("
                                End If
                                mstr &= StrIds & ","
                            Else
                                If mstr.Trim.Length = 0 Then
                                    mstr = " ADF.employeeunkid in ("
                                ElseIf mstr.Trim.Contains("employeeunkid") = False Then
                                    mstr = mstr.Substring(0, mstr.Length - 1) & ")"
                                    mstr &= " AND ADF.employeeunkid in ("
                                End If
                                mstr &= "0,"
                            End If
                            'Sohail (14 Nov 2019) -- End

                    End Select
                Next
                If mstr.Trim <> "" Then
                    mstr = mstr.Substring(0, mstr.Length - 1) & ") "
                Else
                    mstr = " 1 = 1 "
                End If
                mstrFilterString = mstr
            End If
            'Sohail (01 Aug 2019) -- End

            Me.DialogResult = Windows.Forms.DialogResult.OK
            Me.Close()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "btnApply_Click", mstrModuleName)
        End Try
    End Sub
    'Sohail (08 May 2015) -- End

    Private Sub btnClose_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnClose.Click
        Try
            Me.Close()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "btnClose_Click", mstrModuleName)
        End Try
    End Sub

#End Region

#Region " Controls "

    'Sohail (01 Aug 2019) -- Start
    'NMB Payroll UAT # TC006 - 76.1 - Advance search for process payroll, payslip, etc. should have search option. Currently user must scroll the entire list to select an item.
    'Private Sub objchkACheck_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs)
    '    Try
    '        RemoveHandler lvDetails.ItemChecked, AddressOf lvDetails_ItemChecked
    '        For Each lvItem As ListViewItem In lvDetails.Items
    '            lvItem.Checked = CBool(objchkACheck.CheckState)
    '            Call DoItemOperation(lvItem)
    '        Next
    '        AddHandler lvDetails.ItemChecked, AddressOf lvDetails_ItemChecked
    '    Catch ex As Exception
    '        DisplayError.Show("-1", ex.Message, "objchkACheck_CheckedChanged", mstrModuleName)
    '    Finally
    '    End Try
    'End Sub

    'Private Sub lvDetails_ItemChecked(ByVal sender As Object, ByVal e As System.Windows.Forms.ItemCheckedEventArgs)
    '    Try
    '        RemoveHandler objchkACheck.CheckedChanged, AddressOf objchkACheck_CheckedChanged
    '        If lvDetails.CheckedItems.Count <= 0 Then
    '            objchkACheck.CheckState = CheckState.Unchecked
    '        ElseIf lvDetails.CheckedItems.Count < lvDetails.Items.Count Then
    '            objchkACheck.CheckState = CheckState.Indeterminate
    '        ElseIf lvDetails.CheckedItems.Count = lvDetails.Items.Count Then
    '            objchkACheck.CheckState = CheckState.Checked
    '        End If

    '        Call DoItemOperation(e.Item)

    '        AddHandler objchkACheck.CheckedChanged, AddressOf objchkACheck_CheckedChanged

    '    Catch ex As Exception
    '        DisplayError.Show("-1", ex.Message, "lvDetails_ItemChecked", mstrModuleName)
    '    Finally
    '    End Try
    'End Sub
    Private Sub chkSelectAll_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objchkSelectAll.CheckedChanged
        Try
            Call CheckAll(objchkSelectAll.Checked)
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "chkSelectAll_CheckedChanged", mstrModuleName)
        End Try
    End Sub

    Private Sub txtSearch_GotFocus(ByVal sender As Object, ByVal e As System.EventArgs) Handles txtSearch.GotFocus
        Try
            With txtSearch
                .ForeColor = Color.Black
                .Font = New Font(Me.Font.FontFamily, Me.Font.Size, FontStyle.Regular)

                If .Text = mstrSearchText Then
                    .Clear()
                End If
            End With
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "txtSearch_GotFocus", mstrModuleName)
        End Try
    End Sub

    Private Sub txtSearch_Leave(ByVal sender As Object, ByVal e As System.EventArgs) Handles txtSearch.Leave
        Try
            If txtSearch.Text.Trim = "" Then
                Call SetDefaultSearchEmpText()
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "txtSearch_Leave", mstrModuleName)
        End Try
    End Sub

    Private Sub txtSearch_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txtSearch.TextChanged
        Try
            If txtSearch.Text.Trim = mstrSearchText Then Exit Sub
            If dvDetail IsNot Nothing Then
                dvDetail.RowFilter = "name LIKE '%" & txtSearch.Text.Replace("'", "''") & "%'"
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "txtSearch_TextChanged", mstrModuleName)
        End Try
    End Sub
    'Sohail (01 Aug 2019) -- End

#End Region

    'Sohail (01 Aug 2019) -- Start
    'NMB Payroll UAT # TC006 - 76.1 - Advance search for process payroll, payslip, etc. should have search option. Currently user must scroll the entire list to select an item.
#Region " GridView Events "

    Private Sub dgDetail_CurrentCellDirtyStateChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles dgDetail.CurrentCellDirtyStateChanged
        Try
            If dgDetail.IsCurrentCellDirty Then
                dgDetail.CommitEdit(DataGridViewDataErrorContexts.Commit)
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "dgDetail_CurrentCellDirtyStateChanged", mstrModuleName)
        End Try
    End Sub

    Private Sub dgDetail_CellValueChanged(ByVal sender As Object, ByVal e As System.Windows.Forms.DataGridViewCellEventArgs) Handles dgDetail.CellValueChanged
        Try
            If e.RowIndex < 0 Then Exit Sub

            If e.ColumnIndex = objdgcolhCheck.Index Then
                SetCheckBoxValue()
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "dgDetail_CellValueChanged", mstrModuleName)
        End Try
    End Sub

    Private Sub gvFilterInfo_CellPainting(ByVal sender As System.Object, ByVal e As System.Windows.Forms.DataGridViewCellPaintingEventArgs) Handles gvFilterInfo.CellPainting
        Try
            If e.RowIndex >= 0 AndAlso e.RowIndex < gvFilterInfo.RowCount - 1 AndAlso CBool(gvFilterInfo.Rows(e.RowIndex).Cells(objdgcolhFilterIsGrp.Index).Value) = True AndAlso e.ColumnIndex > 0 Then
                If (e.ColumnIndex = objdgColhBlank.Index) Then
                    Dim backColorBrush As Brush = New SolidBrush(Color.Gray)
                    Dim totWidth As Integer = 0
                    For i As Integer = 1 To gvFilterInfo.Columns.Count - 1
                        totWidth += gvFilterInfo.Columns(i).Width
                    Next
                    Dim r As New RectangleF(e.CellBounds.Left, e.CellBounds.Top, totWidth, e.CellBounds.Height)
                    e.Graphics.FillRectangle(backColorBrush, r)

                    e.Graphics.DrawString(CType(gvFilterInfo.Rows(e.RowIndex).Cells(objdgcolhFilterGroupName.Index).Value.ToString & " ", String), e.CellStyle.Font, Brushes.White, e.CellBounds.X, e.CellBounds.Y + 5)

                End If

                e.Handled = True
            End If

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "gvFilterInfo_CellPainting", mstrModuleName)
        End Try
    End Sub

#End Region
    'Sohail (01 Aug 2019) -- End


	'<Language> This Auto Generated Text Please Do Not Modify it.
#Region " Language & UI Settings "
	Private Sub OtherSettings()
		Try
			Me.SuspendLayout()
			 
			Call SetLanguage()
			
			Me.btnClose.GradientBackColor = GUI._ButttonBackColor 
			Me.btnClose.GradientForeColor = GUI._ButttonFontColor

			Me.btnApply.GradientBackColor = GUI._ButttonBackColor 
			Me.btnApply.GradientForeColor = GUI._ButttonFontColor

			Me.btnBranch.GradientBackColor = GUI._ButttonBackColor 
			Me.btnBranch.GradientForeColor = GUI._ButttonFontColor

			Me.btnDeptGrp.GradientBackColor = GUI._ButttonBackColor 
			Me.btnDeptGrp.GradientForeColor = GUI._ButttonFontColor

			Me.btnDept.GradientBackColor = GUI._ButttonBackColor 
			Me.btnDept.GradientForeColor = GUI._ButttonFontColor

			Me.btnSection.GradientBackColor = GUI._ButttonBackColor 
			Me.btnSection.GradientForeColor = GUI._ButttonFontColor

			Me.btnUnit.GradientBackColor = GUI._ButttonBackColor 
			Me.btnUnit.GradientForeColor = GUI._ButttonFontColor

			Me.btnJob.GradientBackColor = GUI._ButttonBackColor 
			Me.btnJob.GradientForeColor = GUI._ButttonFontColor

			Me.btnSectionGroup.GradientBackColor = GUI._ButttonBackColor 
			Me.btnSectionGroup.GradientForeColor = GUI._ButttonFontColor

			Me.btnUnitGroup.GradientBackColor = GUI._ButttonBackColor 
			Me.btnUnitGroup.GradientForeColor = GUI._ButttonFontColor

			Me.btnTeam.GradientBackColor = GUI._ButttonBackColor 
			Me.btnTeam.GradientForeColor = GUI._ButttonFontColor

			Me.btnClass.GradientBackColor = GUI._ButttonBackColor 
			Me.btnClass.GradientForeColor = GUI._ButttonFontColor

			Me.btnClassGroup.GradientBackColor = GUI._ButttonBackColor 
			Me.btnClassGroup.GradientForeColor = GUI._ButttonFontColor

			Me.btnJobGroup.GradientBackColor = GUI._ButttonBackColor 
			Me.btnJobGroup.GradientForeColor = GUI._ButttonFontColor

			Me.btnGradeGroup.GradientBackColor = GUI._ButttonBackColor 
			Me.btnGradeGroup.GradientForeColor = GUI._ButttonFontColor

			Me.btnGrade.GradientBackColor = GUI._ButttonBackColor 
			Me.btnGrade.GradientForeColor = GUI._ButttonFontColor

			Me.btnGradeLevel.GradientBackColor = GUI._ButttonBackColor 
			Me.btnGradeLevel.GradientForeColor = GUI._ButttonFontColor

			Me.btnCostCenterGroup.GradientBackColor = GUI._ButttonBackColor 
			Me.btnCostCenterGroup.GradientForeColor = GUI._ButttonFontColor

			Me.btnCostCenter.GradientBackColor = GUI._ButttonBackColor 
			Me.btnCostCenter.GradientForeColor = GUI._ButttonFontColor

			Me.btnEmployementType.GradientBackColor = GUI._ButttonBackColor 
			Me.btnEmployementType.GradientForeColor = GUI._ButttonFontColor

			Me.btnReligion.GradientBackColor = GUI._ButttonBackColor 
			Me.btnReligion.GradientForeColor = GUI._ButttonFontColor

			Me.btnGender.GradientBackColor = GUI._ButttonBackColor 
			Me.btnGender.GradientForeColor = GUI._ButttonFontColor

			Me.btnNationality.GradientBackColor = GUI._ButttonBackColor 
			Me.btnNationality.GradientForeColor = GUI._ButttonFontColor

			Me.btnPayType.GradientBackColor = GUI._ButttonBackColor 
			Me.btnPayType.GradientForeColor = GUI._ButttonFontColor

			Me.btnPayPoint.GradientBackColor = GUI._ButttonBackColor 
			Me.btnPayPoint.GradientForeColor = GUI._ButttonFontColor

			Me.btnMaritalStatus.GradientBackColor = GUI._ButttonBackColor 
			Me.btnMaritalStatus.GradientForeColor = GUI._ButttonFontColor

			Me.btnRelationFromDpndt.GradientBackColor = GUI._ButttonBackColor 
			Me.btnRelationFromDpndt.GradientForeColor = GUI._ButttonFontColor


			Me.ResumeLayout()
		Catch Ex As Exception
			DisplayError.Show("-1", ex.Message, "OtherSettings", mstrModuleName)
		End Try
	End Sub
			
			
	Private Sub SetLanguage()
		Try
			Me.Text = Language._Object.getCaption(Me.Name, Me.Text)
			
			Me.btnClose.Text = Language._Object.getCaption(Me.btnClose.Name, Me.btnClose.Text)
			Me.btnApply.Text = Language._Object.getCaption(Me.btnApply.Name, Me.btnApply.Text)
			Me.btnBranch.Text = Language._Object.getCaption(Me.btnBranch.Name, Me.btnBranch.Text)
			Me.btnDeptGrp.Text = Language._Object.getCaption(Me.btnDeptGrp.Name, Me.btnDeptGrp.Text)
			Me.btnDept.Text = Language._Object.getCaption(Me.btnDept.Name, Me.btnDept.Text)
			Me.btnSection.Text = Language._Object.getCaption(Me.btnSection.Name, Me.btnSection.Text)
			Me.btnUnit.Text = Language._Object.getCaption(Me.btnUnit.Name, Me.btnUnit.Text)
			Me.btnJob.Text = Language._Object.getCaption(Me.btnJob.Name, Me.btnJob.Text)
			Me.btnSectionGroup.Text = Language._Object.getCaption(Me.btnSectionGroup.Name, Me.btnSectionGroup.Text)
			Me.btnUnitGroup.Text = Language._Object.getCaption(Me.btnUnitGroup.Name, Me.btnUnitGroup.Text)
			Me.btnTeam.Text = Language._Object.getCaption(Me.btnTeam.Name, Me.btnTeam.Text)
			Me.btnClass.Text = Language._Object.getCaption(Me.btnClass.Name, Me.btnClass.Text)
			Me.btnClassGroup.Text = Language._Object.getCaption(Me.btnClassGroup.Name, Me.btnClassGroup.Text)
			Me.btnJobGroup.Text = Language._Object.getCaption(Me.btnJobGroup.Name, Me.btnJobGroup.Text)
			Me.btnGradeGroup.Text = Language._Object.getCaption(Me.btnGradeGroup.Name, Me.btnGradeGroup.Text)
			Me.btnGrade.Text = Language._Object.getCaption(Me.btnGrade.Name, Me.btnGrade.Text)
			Me.btnGradeLevel.Text = Language._Object.getCaption(Me.btnGradeLevel.Name, Me.btnGradeLevel.Text)
			Me.btnCostCenterGroup.Text = Language._Object.getCaption(Me.btnCostCenterGroup.Name, Me.btnCostCenterGroup.Text)
			Me.btnCostCenter.Text = Language._Object.getCaption(Me.btnCostCenter.Name, Me.btnCostCenter.Text)
			Me.btnEmployementType.Text = Language._Object.getCaption(Me.btnEmployementType.Name, Me.btnEmployementType.Text)
			Me.btnReligion.Text = Language._Object.getCaption(Me.btnReligion.Name, Me.btnReligion.Text)
			Me.btnGender.Text = Language._Object.getCaption(Me.btnGender.Name, Me.btnGender.Text)
			Me.btnNationality.Text = Language._Object.getCaption(Me.btnNationality.Name, Me.btnNationality.Text)
			Me.btnPayType.Text = Language._Object.getCaption(Me.btnPayType.Name, Me.btnPayType.Text)
			Me.btnPayPoint.Text = Language._Object.getCaption(Me.btnPayPoint.Name, Me.btnPayPoint.Text)
			Me.btnMaritalStatus.Text = Language._Object.getCaption(Me.btnMaritalStatus.Name, Me.btnMaritalStatus.Text)
			Me.btnRelationFromDpndt.Text = Language._Object.getCaption(Me.btnRelationFromDpndt.Name, Me.btnRelationFromDpndt.Text)
			Me.dgColhDescription.HeaderText = Language._Object.getCaption(Me.dgColhDescription.Name, Me.dgColhDescription.HeaderText)
			Me.dgcolhFilterName.HeaderText = Language._Object.getCaption(Me.dgcolhFilterName.Name, Me.dgcolhFilterName.HeaderText)

		Catch Ex As Exception
			DisplayError.Show("-1", ex.Message, "SetLanguage", mstrModuleName)
		End Try
	End Sub
			
			
	Private Sub SetMessages()
		Try
			Language.setMessage(mstrModuleName, 1, "Type to search")

		Catch Ex As Exception
			DisplayError.Show("-1", ex.Message, "SetMessages", mstrModuleName)
		End Try
	End Sub
#End Region 'Language & UI Settings
	'</Language>
End Class