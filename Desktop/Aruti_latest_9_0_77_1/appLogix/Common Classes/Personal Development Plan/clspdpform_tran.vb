﻿Imports eZeeCommonLib

Public Class clspdpform_tran
    Private Const mstrModuleName = "clsPdpitemdataTran"
    Dim objDataOperation As clsDataOperation
    Dim mstrMessage As String = ""

#Region " Private variables "
    Private mintItemtranunkid As Integer
    Private mstrItemgrpguid As String = ""
    Private mintCategoryunkid As Integer
    Private mintItemunkid As Integer
    Private mintPdpformunkid As Integer
    Private mstrFieldvalue As String = String.Empty
    Private mblnIsvoid As Boolean
    Private mblnIsCompetencySelection As Boolean = False

    Private mintVoiduserunkid As Integer = -1
    Private mdtVoiddatetime As Date
    Private mstrVoidreason As String = String.Empty

    Private mstrHostName As String = ""
    Private mstrClientIP As String = ""
    Private mintCompanyUnkid As Integer = 0
    Private mintAuditUserId As Integer = 0
    Private mintloginemployeeunkid As Integer = 0
    Private mblnIsWeb As Boolean = False
    Private mstrFormName As String = ""
    Private mstrDatabaseName As String = ""

    'S.SANDEEP |03-MAY-2021| -- START
    'ISSUE/ENHANCEMENT : PDP_PM_LINKING
    Private mintAnalysisunkid As Integer = 0
    'S.SANDEEP |03-MAY-2021| -- END

#End Region

#Region " Properties "
    
    Public ReadOnly Property _Message() As String
        Get
            Return mstrMessage
        End Get
    End Property

    Public Property _Itemtranunkid() As Integer
        Get
            Return mintItemtranunkid
        End Get
        Set(ByVal value As Integer)
            mintItemtranunkid = value
            Call GetData()
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set itemgrpguid
    ''' Modify By: Gajanan
    ''' </summary>
    Public Property _Itemgrpguid() As String
        Get
            Return mstrItemgrpguid
        End Get
        Set(ByVal value As String)
            mstrItemgrpguid = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set categoryunkid
    ''' Modify By: Gajanan
    ''' </summary>
    Public Property _Categoryunkid() As Integer
        Get
            Return mintCategoryunkid
        End Get
        Set(ByVal value As Integer)
            mintCategoryunkid = value
        End Set
    End Property

    Public Property _Itemunkid() As Integer
        Get
            Return mintItemunkid
        End Get
        Set(ByVal value As Integer)
            mintItemunkid = value
        End Set
    End Property

    Public Property _Pdpformunkid() As Integer
        Get
            Return mintPdpformunkid
        End Get
        Set(ByVal value As Integer)
            mintPdpformunkid = value
        End Set
    End Property

    Public Property _Fieldvalue() As String
        Get
            Return mstrFieldvalue
        End Get
        Set(ByVal value As String)
            mstrFieldvalue = value
        End Set
    End Property

    Public Property _Isvoid() As Boolean
        Get
            Return mblnIsvoid
        End Get
        Set(ByVal value As Boolean)
            mblnIsvoid = value
        End Set
    End Property

    Public Property _IsCompetencySelection() As Boolean
        Get
            Return mblnIsCompetencySelection
        End Get
        Set(ByVal value As Boolean)
            mblnIsCompetencySelection = value
        End Set
    End Property

    Public Property _Voiduserunkid() As Integer
        Get
            Return mintVoiduserunkid
        End Get
        Set(ByVal value As Integer)
            mintVoiduserunkid = value
        End Set
    End Property

    Public Property _Voiddatetime() As Date
        Get
            Return mdtVoiddatetime
        End Get
        Set(ByVal value As Date)
            mdtVoiddatetime = value
        End Set
    End Property

    Public Property _Voidreason() As String
        Get
            Return mstrVoidreason
        End Get
        Set(ByVal value As String)
            mstrVoidreason = value
        End Set
    End Property

    Public WriteOnly Property _FormName() As String
        Set(ByVal value As String)
            mstrFormName = value
        End Set
    End Property

    Public WriteOnly Property _ClientIP() As String
        Set(ByVal value As String)
            mstrClientIP = value
        End Set
    End Property

    Public WriteOnly Property _HostName() As String
        Set(ByVal value As String)
            mstrHostName = value
        End Set
    End Property

    Public WriteOnly Property _FromWeb() As Boolean
        Set(ByVal value As Boolean)
            mblnIsWeb = value
        End Set
    End Property

    Public WriteOnly Property _AuditUserId() As Integer
        Set(ByVal value As Integer)
            mintAuditUserId = value
        End Set
    End Property

    Public WriteOnly Property _LoginEmployeeUnkid() As Integer
        Set(ByVal value As Integer)
            mintloginemployeeunkid = value
        End Set
    End Property

    Public WriteOnly Property _DatabaseName() As String
        Set(ByVal value As String)
            mstrDatabaseName = value
        End Set
    End Property

    'S.SANDEEP |03-MAY-2021| -- START
    'ISSUE/ENHANCEMENT : PDP_PM_LINKING
    Public Property _PAnalysisunkid() As Integer
        Get
            Return mintAnalysisunkid
        End Get
        Set(ByVal value As Integer)
            mintAnalysisunkid = value
        End Set
    End Property
    'S.SANDEEP |03-MAY-2021| -- END

#End Region

    ''' <summary>
    ''' Modify By: Gajanan
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>
    Public Sub GetData()
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        objDataOperation = New clsDataOperation

        Try
            strQ = "IF OBJECT_ID('tempdb..#competencyList') IS NOT NULL " & _
                    "DROP TABLE #competencyList " & _
                          "CREATE TABLE #competencyList ( " & _
                            "itemtranunkid int " & _
                            ",fieldvalue  NVARCHAR(MAX) " & _
                            ",CompetencyValue int " & _
                          "); " & _
                    "INSERT INTO #competencyList " & _
                         "SELECT " & _
                   "itemtranunkid " & _
                            ",ISNULL(hrassess_competencies_master.name, '') AS fieldvalue " & _
                            ",ISNULL(pdpitemdatatran.fieldvalue, 0) AS CompetencyValue " & _
                         "FROM pdpitemdatatran " & _
                         "LEFT JOIN hrassess_competencies_master " & _
                              "ON CONVERT(NVARCHAR(MAX), pdpitemdatatran.fieldvalue) = hrassess_competencies_master.competenciesunkid " & _
                         "WHERE isvoid = 0 " & _
                         "AND iscompetencyselection = 1 "

            strQ &= "SELECT " & _
                   " pdpitemdatatran.itemtranunkid " & _
                   ",itemgrpguid " & _
                   ",categoryunkid " & _
                   ",itemunkid " & _
                   ",pdpformunkid " & _
                   ",CASE " & _
                       "WHEN ISNULL(pdpitemdatatran.iscompetencyselection, 0) > 0 THEN ISNULL(#competencyList.fieldvalue, '') " & _
                          "ELSE pdpitemdatatran.fieldvalue " & _
                     "END AS fieldvalue " & _
                   ",isvoid " & _
                   ",iscompetencyselection " & _
                   ",ISNULL(pdpitemdatatran.analysisunkid,0) AS analysisunkid " & _
             "FROM pdpitemdatatran " & _
                    "LEFT JOIN #competencyList " & _
                    "ON pdpitemdatatran.itemtranunkid = #competencyList.itemtranunkid " & _
             "WHERE pdpitemdatatran.itemtranunkid = @itemtranunkid "
            'S.SANDEEP |03-MAY-2021| -- START {analysisunkid} -- END

            objDataOperation.AddParameter("@itemtranunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintItemtranunkid.ToString)

            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            For Each dtRow As DataRow In dsList.Tables(0).Rows
                mintItemtranunkid = CInt(dtRow.Item("itemtranunkid"))
                mstrItemgrpguid = dtRow.Item("itemgrpguid").ToString
                mintCategoryunkid = CInt(dtRow.Item("categoryunkid"))
                mintItemunkid = CInt(dtRow.Item("itemunkid"))
                mintPdpformunkid = CInt(dtRow.Item("pdpformunkid"))
                mstrFieldvalue = dtRow.Item("fieldvalue").ToString
                mblnIsvoid = CBool(dtRow.Item("isvoid"))
                mblnIsCompetencySelection = CBool(dtRow.Item("iscompetencyselection"))

                mintAnalysisunkid = CInt(dtRow.Item("analysisunkid"))   'S.SANDEEP |03-MAY-2021| -- START {analysisunkid} -- END

                Exit For
            Next
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: GetData; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
    End Sub

    ''' <summary>
    ''' Modify By: Gajanan
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>
    Public Function GetList(ByVal strTableName As String, Optional ByVal blnOnlyActive As Boolean = True, Optional ByVal intUnkid As Integer = 0) As DataSet
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        objDataOperation = New clsDataOperation

        Try
            strQ = "SELECT " & _
              "  itemtranunkid " & _
              ", itemgrpguid " & _
              ", categoryunkid " & _
              ", itemunkid " & _
              ", pdpformunkid " & _
              ", fieldvalue " & _
              ", isvoid " & _
              ", iscompetencyselection " & _
              ", voiduserunkid " & _
              ", voiddatetime " & _
              ", voidreason " & _
              ", ISNULL(pdpitemdatatran.analysisunkid,0) AS analysisunkid " & _
             "FROM pdpitemdatatran WHERE 1=1 "
            'S.SANDEEP |03-MAY-2021| -- START {analysisunkid} -- END
            If blnOnlyActive Then
                strQ &= " and isvoid = 0 "
            End If


            If intUnkid > 0 Then
                strQ &= " and pdpitemdatatran.itemtranunkid = @itemtranunkid "
                objDataOperation.AddParameter("@itemtranunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intUnkid.ToString)
            End If

            dsList = objDataOperation.ExecQuery(strQ, strTableName)

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: GetList; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
        Return dsList
    End Function

    ''' <summary>
    ''' Modify By: Gajanan
    ''' </summary>
    ''' <returns>Boolean</returns>
    ''' <purpose> INSERT INTO Database Table (pdpitemdatatran) </purpose>
    Public Function Insert(Optional ByVal xDataOpr As clsDataOperation = Nothing) As Boolean

        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        If xDataOpr Is Nothing Then
            objDataOperation = New clsDataOperation
            objDataOperation.BindTransaction()
        Else
            objDataOperation = xDataOpr
        End If
        objDataOperation.ClearParameters()

        Try
            
            objDataOperation.AddParameter("@itemgrpguid", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrItemgrpguid.ToString)
            objDataOperation.AddParameter("@categoryunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintCategoryunkid.ToString)
            objDataOperation.AddParameter("@itemunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintItemunkid.ToString)
            objDataOperation.AddParameter("@pdpformunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintPdpformunkid.ToString)
            objDataOperation.AddParameter("@fieldvalue", SqlDbType.NText, eZeeDataType.NAME_SIZE, mstrFieldvalue.ToString)
            objDataOperation.AddParameter("@isvoid", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsvoid.ToString)
            objDataOperation.AddParameter("@iscompetencyselection", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsCompetencySelection.ToString)
            objDataOperation.AddParameter("@voiduserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintVoiduserunkid.ToString)
            If mdtVoiddatetime = Nothing Then
                objDataOperation.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, DBNull.Value)
            Else
                objDataOperation.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, mdtVoiddatetime)
            End If
            objDataOperation.AddParameter("@voidreason", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrVoidreason.ToString)

            'S.SANDEEP |03-MAY-2021| -- START
            'ISSUE/ENHANCEMENT : PDP_PM_LINKING
            objDataOperation.AddParameter("@analysisunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintAnalysisunkid.ToString)
            'S.SANDEEP |03-MAY-2021| -- END


            strQ = "INSERT INTO pdpitemdatatran ( " & _
                  "  itemgrpguid " & _
                  ", categoryunkid " & _
                  ", itemunkid " & _
                  ", pdpformunkid " & _
                  ", fieldvalue " & _
                  ", isvoid " & _
                  ", iscompetencyselection " & _
                  ", voiduserunkid " & _
                  ", voiddatetime " & _
                  ", voidreason" & _
                  ", analysisunkid " & _
                ") VALUES (" & _
                  "  @itemgrpguid " & _
                  ", @categoryunkid " & _
                  ", @itemunkid " & _
                  ", @pdpformunkid " & _
                  ", @fieldvalue " & _
                  ", @isvoid " & _
                  ", @iscompetencyselection " & _
                  ", @voiduserunkid " & _
                  ", @voiddatetime " & _
                  ", @voidreason" & _
                  ", @analysisunkid " & _
                "); SELECT @@identity"
            'S.SANDEEP |03-MAY-2021| -- START {analysisunkid} -- END

            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            mintItemtranunkid = dsList.Tables(0).Rows(0).Item(0)



            If xDataOpr Is Nothing Then objDataOperation.ReleaseTransaction(True)
            Return True
        Catch ex As Exception
            If xDataOpr Is Nothing Then objDataOperation.ReleaseTransaction(False)
            Throw New Exception(ex.Message & "; Procedure Name: Insert; Module Name: " & mstrModuleName)
            Return False
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            If xDataOpr Is Nothing Then objDataOperation = Nothing
        End Try
    End Function

    ''' <summary>
    ''' Modify By: Gajanan
    ''' </summary>
    ''' <returns>Boolean</returns>
    ''' <purpose> Update Database Table (pdpitemdatatran) </purpose>
    Public Function Update(Optional ByVal xDataOpr As clsDataOperation = Nothing) As Boolean

        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        If xDataOpr Is Nothing Then
            objDataOperation = New clsDataOperation
            objDataOperation.BindTransaction()
        Else
            objDataOperation = xDataOpr
        End If
        objDataOperation.ClearParameters()

        Try
            objDataOperation.AddParameter("@itemgrpguid", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrItemgrpguid.ToString)
            objDataOperation.AddParameter("@categoryunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintCategoryunkid.ToString)
            objDataOperation.AddParameter("@itemunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintItemunkid.ToString)
            objDataOperation.AddParameter("@pdpformunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintPdpformunkid.ToString)
            objDataOperation.AddParameter("@fieldvalue", SqlDbType.NText, eZeeDataType.NAME_SIZE, mstrFieldvalue.ToString)
            objDataOperation.AddParameter("@isvoid", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsvoid.ToString)
            objDataOperation.AddParameter("@iscompetencyselection", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsCompetencySelection.ToString)
            objDataOperation.AddParameter("@voiduserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintVoiduserunkid.ToString)
            If mdtVoiddatetime = Nothing Then
                objDataOperation.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, DBNull.Value)
            Else
                objDataOperation.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, mdtVoiddatetime)
            End If
            objDataOperation.AddParameter("@voidreason", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrVoidreason.ToString)

            'S.SANDEEP |03-MAY-2021| -- START
            'ISSUE/ENHANCEMENT : PDP_PM_LINKING
            objDataOperation.AddParameter("@analysisunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintAnalysisunkid.ToString)
            'S.SANDEEP |03-MAY-2021| -- END

            strQ = "UPDATE pdpitemdatatran SET " & _
              "  itemgrpguid = @itemgrpguid" & _
              ", categoryunkid = @categoryunkid" & _
              ", itemunkid = @itemunkid" & _
              ", pdpformunkid = @pdpformunkid" & _
              ", fieldvalue = @fieldvalue" & _
              ", isvoid = @isvoid" & _
              ", iscompetencyselection = @iscompetencyselection" & _
              ", voiduserunkid = @voiduserunkid" & _
              ", voiddatetime = @voiddatetime" & _
              ", voidreason = @voidreason " & _
              ", analysisunkid = @analysisunkid " & _
            "WHERE itemgrpguid = @itemgrpguid and categoryunkid = @categoryunkid and itemunkid = @itemunkid and pdpformunkid = @pdpformunkid  "
            'S.SANDEEP |03-MAY-2021| -- START {analysisunkid} -- END


            If mintItemtranunkid > 0 Then
                strQ &= " and itemtranunkid = @itemtranunkid "
                objDataOperation.AddParameter("@itemtranunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintItemtranunkid.ToString)
            End If


            Call objDataOperation.ExecNonQuery(strQ)

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If




            If xDataOpr Is Nothing Then objDataOperation.ReleaseTransaction(True)
            Return True
        Catch ex As Exception
            If xDataOpr Is Nothing Then objDataOperation.ReleaseTransaction(False)
            Throw New Exception(ex.Message & "; Procedure Name: Update; Module Name: " & mstrModuleName)
            Return False
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            If xDataOpr Is Nothing Then objDataOperation = Nothing

        End Try
    End Function

    ''' <summary>
    ''' Modify By: Gajanan
    ''' </summary>
    ''' <returns>Boolean</returns>
    ''' <purpose> Delete Database Table (pdpitemdatatran) </purpose>
    Public Function Delete(ByVal intTranunkid As Integer, ByVal strItemgrpguid As String, ByVal intcategoryunkid As Integer, Optional ByVal xDataOpr As clsDataOperation = Nothing) As Boolean

        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        If xDataOpr Is Nothing Then
            objDataOperation = New clsDataOperation
            objDataOperation.BindTransaction()
        Else
            objDataOperation = xDataOpr
        End If
        objDataOperation.ClearParameters()

        Try
            objDataOperation.AddParameter("@itemgrpguid", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, strItemgrpguid.ToString)
            objDataOperation.AddParameter("@categoryunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intcategoryunkid.ToString)
            objDataOperation.AddParameter("@pdpformunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintPdpformunkid.ToString)
            objDataOperation.AddParameter("@isvoid", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsvoid.ToString)
            objDataOperation.AddParameter("@voiduserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintVoiduserunkid.ToString)
            objDataOperation.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, mdtVoiddatetime.ToString)
            objDataOperation.AddParameter("@voidreason", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrVoidreason.ToString)

            strQ = "UPDATE pdpitemdatatran SET " & _
              " isvoid = @isvoid" & _
              ", voiduserunkid = @voiduserunkid" & _
              ", voiddatetime = @voiddatetime" & _
              ", voidreason = @voidreason " & _
            "WHERE itemgrpguid = @itemgrpguid and categoryunkid=@categoryunkid and pdpformunkid = @pdpformunkid "


            If intTranunkid > 0 Then
                strQ &= " and itemtranunkid = @itemtranunkid "
                objDataOperation.AddParameter("@itemtranunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intTranunkid)
            End If


            Call objDataOperation.ExecNonQuery(strQ)

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            If InsertAuditTrails(objDataOperation, enAuditType.DELETE, mintPdpformunkid, -1, strItemgrpguid, intcategoryunkid, -1) = False Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            If xDataOpr Is Nothing Then objDataOperation.ReleaseTransaction(True)
            Return True
        Catch ex As Exception
            If xDataOpr Is Nothing Then objDataOperation.ReleaseTransaction(False)
            Throw New Exception(ex.Message & "; Procedure Name: Delete; Module Name: " & mstrModuleName)
            Return False
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            If xDataOpr Is Nothing Then objDataOperation = Nothing

        End Try
    End Function

    Public Function isValueExist(ByVal strFieldValue As String, _
                                 ByVal strGuid As String, _
                                 ByVal intCategoryunkid As Integer, _
                            ByVal intItemunkid As Integer, _
                            ByVal intpdpformunkid As Integer, _
                                 Optional ByVal xDataOpr As clsDataOperation = Nothing) As Boolean
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        If xDataOpr Is Nothing Then
            objDataOperation = New clsDataOperation
        Else
            objDataOperation = xDataOpr
        End If
        objDataOperation.ClearParameters()

        Try
            strQ = "SELECT itemtranunkid from pdpitemdatatran WHERE " & _
                    "itemunkid = @Itemunkid " & _
                    "and isvoid = 0 " & _
                    "and fieldvalue = @fieldvalue " & _
                    "and categoryunkid = @categoryunkid " & _
                    "and pdpformunkid = @pdpformunkid " & _
                    "and itemgrpguid <> @itemgrpguid "

            objDataOperation.AddParameter("@itemgrpguid", SqlDbType.NVarChar, mstrItemgrpguid.Length, strGuid)
            objDataOperation.AddParameter("@fieldvalue", SqlDbType.NVarChar, mstrItemgrpguid.Length, strFieldValue)
            objDataOperation.AddParameter("@pdpformunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intpdpformunkid)
            objDataOperation.AddParameter("@itemunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intItemunkid)
            objDataOperation.AddParameter("@categoryunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intCategoryunkid)

            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            Return dsList.Tables(0).Rows.Count > 0
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: isValueExist; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            If xDataOpr Is Nothing Then objDataOperation = Nothing
        End Try
    End Function

    Public Function isExist(ByVal strItemgrpguid As String, _
                            ByVal intCategoryunkid As Integer, _
                            ByVal intItemunkid As Integer, _
                            ByVal intpdpformunkid As Integer, _
                            ByVal intItemTranunkid As Integer, _
                            Optional ByVal xDataOpr As clsDataOperation = Nothing) As Boolean
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        If xDataOpr Is Nothing Then
            objDataOperation = New clsDataOperation
        Else
            objDataOperation = xDataOpr
        End If
        objDataOperation.ClearParameters()

        Try
            strQ = "SELECT " & _
              "  itemtranunkid " & _
              ", itemgrpguid " & _
              ", categoryunkid " & _
              ", itemunkid " & _
              ", pdpformunkid " & _
              ", fieldvalue " & _
              ", isvoid " & _
              ", voiduserunkid " & _
              ", voiddatetime " & _
              ", voidreason " & _
             "FROM pdpitemdatatran " & _
             "WHERE itemgrpguid = @itemgrpguid " & _
             "AND categoryunkid = @categoryunkid " & _
             "AND pdpformunkid = @pdpformunkid " & _
             "AND itemunkid = @itemunkid "

            If intItemTranunkid > 0 Then
                strQ &= " AND itemtranunkid = @itemtranunkid "
                objDataOperation.AddParameter("@itemtranunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intItemTranunkid)

            End If


            objDataOperation.AddParameter("@itemgrpguid", SqlDbType.NVarChar, mstrItemgrpguid.Length, mstrItemgrpguid)
            objDataOperation.AddParameter("@categoryunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intCategoryunkid)
            objDataOperation.AddParameter("@pdpformunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intpdpformunkid)
            objDataOperation.AddParameter("@itemunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intItemunkid)

            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            Return dsList.Tables(0).Rows.Count > 0
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: isExist; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            If xDataOpr Is Nothing Then objDataOperation = Nothing
        End Try
    End Function

    Public Function isUsed(ByVal intUnkid As Integer, Optional ByVal strGuid As String = "", Optional ByVal isFromTable As Boolean = False) As Boolean
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        Dim objDataOperation As clsDataOperation
        objDataOperation = New clsDataOperation

        Try

            Dim Tables() As String = {"pdpgoals_master"}
            For Each value As String In Tables
                Select Case value
                    Case "pdpgoals_master"
                        If isFromTable Then
                            strQ = "SELECT pdpitemdatatran.itemtranunkid FROM " & value & " " & _
                                   "LEFT JOIN pdpitemdatatran ON " & value & ".itemunkid = pdpitemdatatran.itemtranunkid " & _
                                   "WHERE pdpitemdatatran.itemgrpguid = '" & strGuid & "' "
                        Else
                            strQ = "SELECT itemunkid FROM " & value & " WHERE itemunkid = @itemunkid "
                        End If


                End Select
                objDataOperation.ClearParameters()
                objDataOperation.AddParameter("@itemunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intUnkid)
                dsList = objDataOperation.ExecQuery(strQ, "Used")

                If objDataOperation.ErrorMessage <> "" Then
                    exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                    Throw exForce
                End If

                If dsList.Tables("Used").Rows.Count > 0 Then
                    Return True
                    Exit For
                End If

            Next
            Return False
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: isUsed; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
    End Function

    Public Sub GetDataGroupWise(ByVal intCategoryunkid As Integer, ByVal strItemgrpguid As String, Optional ByVal xDataOpr As clsDataOperation = Nothing)
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        If xDataOpr Is Nothing Then
            objDataOperation = New clsDataOperation
        Else
            objDataOperation = xDataOpr
        End If
        objDataOperation.ClearParameters()


        Try
            strQ = "SELECT " & _
              "  itemtranunkid " & _
              ", itemgrpguid " & _
              ", categoryunkid " & _
              ", itemunkid " & _
              ", pdpformunkid " & _
              ", fieldvalue " & _
              ", isvoid " & _
              ", iscompetencyselection " & _
              ", voiduserunkid " & _
              ", voiddatetime " & _
              ", voidreason " & _
             "FROM pdpitemdatatran " & _
             "WHERE itemgrpguid = @itemgrpguid " & _
             " and categoryunkid = @categoryunkid "

            objDataOperation.AddParameter("@itemtranunkid", SqlDbType.Int, strItemgrpguid.Length, strItemgrpguid)
            objDataOperation.AddParameter("@categoryunkid", SqlDbType.NVarChar, eZeeDataType.INT_SIZE, intCategoryunkid)

            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            For Each dtRow As DataRow In dsList.Tables(0).Rows
                mintItemtranunkid = CInt(dtRow.Item("itemtranunkid"))
                mstrItemgrpguid = dtRow.Item("itemgrpguid").ToString
                mintCategoryunkid = CInt(dtRow.Item("categoryunkid"))
                mintItemunkid = CInt(dtRow.Item("itemunkid"))
                mintPdpformunkid = CInt(dtRow.Item("pdpformunkid"))
                mstrFieldvalue = dtRow.Item("fieldvalue").ToString
                mblnIsvoid = CBool(dtRow.Item("isvoid"))
                mblnIsCompetencySelection = CBool(dtRow.Item("iscompetencyselection"))
                mintVoiduserunkid = CInt(dtRow.Item("voiduserunkid"))
                mdtVoiddatetime = dtRow.Item("voiddatetime")
                mstrVoidreason = dtRow.Item("voidreason").ToString
                Exit For
            Next
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: GetData; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            If xDataOpr Is Nothing Then objDataOperation = Nothing
        End Try
    End Sub

    Public Function SaveCustomItemData(Optional ByVal xDataOpr As clsDataOperation = Nothing) As Boolean
        Dim exForce As Exception

        If xDataOpr Is Nothing Then
            objDataOperation = New clsDataOperation
            objDataOperation.BindTransaction()
        Else
            objDataOperation = xDataOpr
        End If
        objDataOperation.ClearParameters()

        Try

            If isExist(mstrItemgrpguid, mintCategoryunkid, mintItemunkid, mintPdpformunkid, mintItemtranunkid, objDataOperation) Then
                If Update(objDataOperation) = False Then
                    exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                    Throw exForce
                End If

                If InsertAuditTrails(objDataOperation, enAuditType.EDIT, mintPdpformunkid, mintItemtranunkid, mstrItemgrpguid, mintCategoryunkid, mintItemunkid) = False Then
                    exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                    Throw exForce
                End If


            Else
                If Insert(objDataOperation) = False Then
                    exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                    Throw exForce
                End If

                If InsertAuditTrails(objDataOperation, enAuditType.ADD, mintPdpformunkid, mintItemtranunkid, mstrItemgrpguid, mintCategoryunkid, mintItemunkid) = False Then
                    exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                    Throw exForce
                End If
                mintItemtranunkid = -1
            End If



            If xDataOpr Is Nothing Then objDataOperation.ReleaseTransaction(True)
            Return True
        Catch ex As Exception
            If xDataOpr Is Nothing Then objDataOperation.ReleaseTransaction(False)
            Throw New Exception(ex.Message & "; Procedure Name: SaveCustomItemData; Module Name: " & mstrModuleName)
            Return False
        Finally
            exForce = Nothing
            If xDataOpr Is Nothing Then objDataOperation = Nothing
        End Try
    End Function

    Public Function InsertAuditTrails(ByVal objDataOperation As clsDataOperation, _
                                      ByVal eAuditType As enAuditType, _
                                      ByVal intPdpformunkid As Integer, _
                                      ByVal intUnkid As Integer, _
                                       ByVal strItemgrpguid As String, _
                                       ByVal intCategoryunkid As Integer, _
                                       ByVal intItemunkid As Integer) As Boolean

        Dim StrQ As String = ""
        Try
            StrQ = "INSERT INTO " & mstrDatabaseName & "..atpdpitemdatatran ( " & _
                    "  tranguid " & _
                    ", itemtranunkid " & _
                    ", itemgrpguid " & _
                    ", categoryunkid " & _
                    ", itemunkid " & _
                    ", pdpformunkid " & _
                    ", fieldvalue " & _
                    ", iscompetencyselection " & _
                    ", audittypeid " & _
                    ", audtuserunkid " & _
                    ", loginemployeeunkid " & _
                    ", auditdatetime " & _
                    ", formname " & _
                    ", ip " & _
                    ", host " & _
                    ", isweb" & _
                    ", analysisunkid " & _
                    ") select " & _
                    "  LOWER(NEWID()) " & _
                    ", itemtranunkid " & _
                    ", itemgrpguid " & _
                    ", categoryunkid " & _
                    ", itemunkid " & _
                    ", pdpformunkid " & _
                    ", fieldvalue " & _
                    ", iscompetencyselection " & _
                    ", @audittypeid " & _
                    ", @audtuserunkid " & _
                    ", @loginemployeeunkid " & _
                    ", GETDATE() " & _
                    ", @formname " & _
                    ", @ip " & _
                    ", @host " & _
                    ", @isweb " & _
                    ", analysisunkid " & _
                    "  From " & mstrDatabaseName & "..pdpitemdatatran WHERE 1=1 AND pdpitemdatatran.Categoryunkid = @Categoryunkid and pdpformunkid=@pdpformunkid AND pdpitemdatatran.itemgrpguid = @itemgrpguid "


            objDataOperation.ClearParameters()

            If intUnkid > 0 Then
                StrQ &= " and itemtranunkid = @itemtranunkid "
                objDataOperation.AddParameter("@itemtranunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intUnkid)
            End If

            If intItemunkid > 0 Then
                StrQ &= " AND pdpitemdatatran.Itemunkid = @Itemunkid "
                objDataOperation.AddParameter("@Itemunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intItemunkid)
            End If

            objDataOperation.AddParameter("@itemgrpguid", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, strItemgrpguid)
            objDataOperation.AddParameter("@Categoryunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intCategoryunkid)
            objDataOperation.AddParameter("@audittypeid", SqlDbType.Int, eZeeDataType.INT_SIZE, eAuditType)
            objDataOperation.AddParameter("@audtuserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintAuditUserId)
            objDataOperation.AddParameter("@loginemployeeunkid ", SqlDbType.Int, eZeeDataType.INT_SIZE, mintloginemployeeunkid)
            objDataOperation.AddParameter("@formname", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrFormName)
            objDataOperation.AddParameter("@ip", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrClientIP)
            objDataOperation.AddParameter("@host", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrHostName)
            objDataOperation.AddParameter("@isweb", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsWeb)
            objDataOperation.AddParameter("@pdpformunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintPdpformunkid)
            'S.SANDEEP |03-MAY-2021| -- START
            'ISSUE/ENHANCEMENT : PDP_PM_LINKING
            objDataOperation.AddParameter("@analysisunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintAnalysisunkid.ToString)
            'S.SANDEEP |03-MAY-2021| -- END

            objDataOperation.ExecNonQuery(StrQ)

            If objDataOperation.ErrorMessage <> "" Then
                Throw New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
            End If

            Return True
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: InsertAuditTrails; Module Name: " & mstrModuleName)
        Finally
        End Try
    End Function

    Public Function GetActionPlanSelectionItemValue(ByVal itemTranunkid As Integer, Optional ByVal xDataOpr As clsDataOperation = Nothing) As DataSet
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        If xDataOpr Is Nothing Then
            objDataOperation = New clsDataOperation
        Else
            objDataOperation = xDataOpr
        End If
        objDataOperation.ClearParameters()

        Try
            strQ = "IF OBJECT_ID('tempdb..#competencyList') IS NOT NULL " & _
                    "DROP TABLE #competencyList " & _
                          "CREATE TABLE #competencyList ( " & _
                            "itemtranunkid int " & _
                            ",fieldvalue  NVARCHAR(MAX) " & _
                            ",CompetencyValue int " & _
                          "); " & _
                    "INSERT INTO #competencyList " & _
                         "SELECT " & _
                   "itemtranunkid " & _
                            ",ISNULL(hrassess_competencies_master.name, '') AS fieldvalue " & _
                            ",ISNULL(pdpitemdatatran.fieldvalue, 0) AS CompetencyValue " & _
                         "FROM pdpitemdatatran " & _
                         "LEFT JOIN hrassess_competencies_master " & _
                              "ON CONVERT(NVARCHAR(MAX), pdpitemdatatran.fieldvalue) = hrassess_competencies_master.competenciesunkid " & _
                         "WHERE isvoid = 0 " & _
                         "AND iscompetencyselection = 1 "


            strQ &= "SELECT " & _
                   " pdpitemdatatran.itemtranunkid " & _
                   ",itemgrpguid " & _
                   ",pdpitemdatatran.categoryunkid " & _
                   ",pdpitemdatatran.itemunkid " & _
                   ",pdpformunkid " & _
                   ",CASE " & _
                       "WHEN ISNULL(pdpitemdatatran.iscompetencyselection, 0) > 0 THEN ISNULL(#competencyList.fieldvalue, '') " & _
                        "ELSE pdpitemdatatran.fieldvalue " & _
                   "END AS fieldvalue " & _
                   ",isvoid " & _
                   ",pdpitem_master.selectionmodeid " & _
                   ",pdpitem_master.itemtypeid " & _
                   ",pdpitem_master.item " & _
                   ",pdpitemdatatran.iscompetencyselection " & _
                   ",CASE " & _
                        "WHEN ISNULL(pdpitemdatatran.iscompetencyselection, 0) > 0 THEN #competencyList.CompetencyValue " & _
                        "ELSE -1 " & _
                   "END AS competencyid " & _
                "FROM pdpitemdatatran " & _
                "left JOIN pdpitem_master " & _
                     "ON pdpitemdatatran.itemunkid = pdpitem_master.itemunkid " & _
                "LEFT JOIN #competencyList " & _
                         "ON pdpitemdatatran.itemtranunkid = #competencyList.itemtranunkid " & _
                "WHERE pdpitemdatatran.itemtranunkid = @itemtranunkid "

            objDataOperation.AddParameter("@itemtranunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, itemTranunkid)

            dsList = objDataOperation.ExecQuery(strQ, "List")


            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            Dim fieldValue As String = ""
            If IsNothing(dsList) = False AndAlso dsList.Tables(0).Rows.Count > 0 Then
                Select Case CInt(dsList.Tables(0).Rows(0)("itemtypeid"))
                    Case clspdpitem_master.enPdpCustomType.FREE_TEXT
                        fieldValue = dsList.Tables(0).Rows(0)("fieldvalue")
                    Case clspdpitem_master.enPdpCustomType.SELECTION
                        If dsList.Tables(0).Rows(0)("fieldvalue").ToString.Trim.Length > 0 Then
                            Select Case CInt(dsList.Tables(0).Rows(0)("selectionmodeid"))

                                Case clspdpitem_master.enPdpSelectionMode.TRAINING_OBJECTIVE, clspdpitem_master.enPdpSelectionMode.JOB_CAPABILITIES_COURSES, clspdpitem_master.enPdpSelectionMode.CAREER_DEVELOPMENT_COURSES
                                    Dim objCMaster As New clsCommon_Master
                                    objCMaster._Masterunkid = CInt(dsList.Tables(0).Rows(0)("fieldvalue"))
                                    dsList.Tables(0).Rows(0)("fieldvalue") = objCMaster._Name
                                    objCMaster = Nothing
                                Case clspdpitem_master.enPdpSelectionMode.EMPLOYEE_COMPETENCIES
                                    Dim objCMaster As New clsassess_competencies_master
                                    objCMaster._Competenciesunkid = CInt(dsList.Tables(0).Rows(0)("fieldvalue"))
                                    fieldValue = objCMaster._Name
                                    objCMaster = Nothing
                                Case clspdpitem_master.enPdpSelectionMode.EMPLOYEE_GOALS
                                    Dim objEmpField1 As New clsassess_empfield1_master
                                    objEmpField1._Empfield1unkid = CInt(dsList.Tables(0).Rows(0)("fieldvalue"))
                                    dsList.Tables(0).Rows(0)("fieldvalue") = objEmpField1._Field_Data
                                    objEmpField1 = Nothing

                                Case clspdpitem_master.enPdpSelectionMode.PERFORMANCE_CUSTOM_ITEM
                                    Dim objCMaster As New clsCommon_Master
                                    objCMaster._Masterunkid = CInt(dsList.Tables(0).Rows(0)("fieldvalue"))
                                    dsList.Tables(0).Rows(0)("fieldvalue") = objCMaster._Name
                                    objCMaster = Nothing
                            End Select
                        End If
                    Case clspdpitem_master.enPdpCustomType.DATE_SELECTION
                        If dsList.Tables(0).Rows(0)("fieldvalue").ToString.Trim.Length > 0 Then
                            dsList.Tables(0).Rows(0)("fieldvalue") = eZeeDate.convertDate(dsList.Tables(0).Rows(0)("fieldvalue").ToString).ToShortDateString
                        End If
                    Case clspdpitem_master.enPdpCustomType.NUMERIC_DATA
                        If dsList.Tables(0).Rows(0)("fieldvalue").ToString.Trim.Length > 0 Then
                            If IsNumeric(dsList.Tables(0).Rows(0)("fieldvalue")) Then
                                dsList.Tables(0).Rows(0)("fieldvalue") = CDbl(dsList.Tables(0).Rows(0)("fieldvalue")).ToString
                            End If
                        End If
                End Select
            End If


            Return dsList

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: GetSelectionItemValue; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            If xDataOpr Is Nothing Then objDataOperation = Nothing

        End Try
    End Function

End Class
