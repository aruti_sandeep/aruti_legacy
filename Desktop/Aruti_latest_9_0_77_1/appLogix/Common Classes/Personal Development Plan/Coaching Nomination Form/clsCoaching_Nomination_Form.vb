﻿'************************************************************************************************************************************
'Class Name : clsCoach_Nomination_Form.vb
'Purpose    :
'Date       : 20-Sep-2023
'Written By : Hemant
'Modified   :
'************************************************************************************************************************************
Imports eZeeCommonLib
Imports System.Web
Imports System.Threading

''' <summary>
''' Purpose: 
''' Developer: Hemant
''' </summary>
Public Class clsCoaching_Nomination_Form
    Private Shared ReadOnly mstrModuleName As String = "clsCoaching_Nomination_Form"
    Dim mstrMessage As String = ""
    Dim objDataOperation As clsDataOperation

#Region " Private variables "
    Private mintCoachingNominationunkid As Integer
    Private mstrRefno As String = ""
    Private mdtApplication_Date As Date
    Private mintCoach_Employeeunkid As Integer
    Private mintCoachee_Employeeunkid As Integer
    Private mstrCoachWorkProcess As String = String.Empty
    Private mstrCoacheeWorkProcess As String = String.Empty
    Private mintApproverTranunkid As Integer
    Private mintStatusunkid As Integer
    Private mblnIsSubmitApproval As Boolean
    Private mdtEffective_Date As Date
    Private mintFormTypeId As Integer
    Private mstrNewCoachWorkProcess As String = String.Empty
    Private mstrReasonForChange As String = String.Empty
    Private mintPrev_Coach_Employeeunkid As Integer
    Private mintPrev_CoachingNominationunkid As Integer
    Private mintUserunkid As Integer
    Private mblnIsvoid As Boolean
    Private mintVoiduserunkid As Integer
    Private mdtVoiddatetime As Date
    Private mstrVoidreason As String = String.Empty
    Private mstrHostName As String = ""
    Private mstrClientIP As String = ""
    Private mintAuditUserId As Integer = 0
    Private mblnIsWeb As Boolean = False
    Private mstrFormName As String = ""
    Private mintMinApprovedPriority As Integer = -1
    Private objEmailList As New List(Of clsEmailCollection)
    Private objThread As Thread
    Private mintLoginEmployeeunkid As Integer
#End Region

#Region " Properties "
    ''' <summary>
    ''' Purpose: Get Message from Class 
    ''' Modify By: Hemant
    ''' </summary>
    Public ReadOnly Property _Message() As String
        Get
            Return mstrMessage
        End Get
    End Property

    Private mDataOp As clsDataOperation = Nothing
    Public Property _DataOp() As clsDataOperation
        Get
            Return mDataOp
        End Get
        Set(ByVal value As clsDataOperation)
            mDataOp = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set coachingnominationunkid
    ''' Modify By: Hemant
    ''' </summary>
    Public Property _CoachingNominationunkid() As Integer
        Get
            Return mintCoachingNominationunkid
        End Get
        Set(ByVal value As Integer)
            mintCoachingNominationunkid = value
            Call GetData()
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set refno
    ''' Modify By: Hemant
    ''' </summary>
    Public Property _Refno() As String
        Get
            Return mstrRefno
        End Get
        Set(ByVal value As String)
            mstrRefno = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set application_date
    ''' Modify By: Hemant
    ''' </summary>
    Public Property _Application_Date() As Date
        Get
            Return mdtApplication_Date
        End Get
        Set(ByVal value As Date)
            mdtApplication_Date = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set coach_employeeunkid
    ''' Modify By: Hemant
    ''' </summary>
    Public Property _Coach_Employeeunkid() As Integer
        Get
            Return mintCoach_Employeeunkid
        End Get
        Set(ByVal value As Integer)
            mintCoach_Employeeunkid = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set coachee_employeeunkid
    ''' Modify By: Hemant
    ''' </summary>
    Public Property _Coachee_Employeeunkid() As Integer
        Get
            Return mintCoachee_Employeeunkid
        End Get
        Set(ByVal value As Integer)
            mintCoachee_Employeeunkid = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set coach_work_process
    ''' Modify By: Hemant
    ''' </summary>
    Public Property _CoachWorkProcess() As String
        Get
            Return mstrCoachWorkProcess
        End Get
        Set(ByVal value As String)
            mstrCoachWorkProcess = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set coachee_work_process
    ''' Modify By: Hemant
    ''' </summary>
    Public Property _CoacheeWorkProcess() As String
        Get
            Return mstrCoacheeWorkProcess
        End Get
        Set(ByVal value As String)
            mstrCoacheeWorkProcess = value
        End Set
    End Property


    ''' <summary>
    ''' Purpose: Get or Set approvertranunkid
    ''' Modify By: Hemant
    ''' </summary>
    Public Property _Approvertranunkid() As Integer
        Get
            Return mintApproverTranunkid
        End Get
        Set(ByVal value As Integer)
            mintApproverTranunkid = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set statusunkid
    ''' Modify By: Hemant
    ''' </summary>
    Public Property _Statusunkid() As Integer
        Get
            Return mintStatusunkid
        End Get
        Set(ByVal value As Integer)
            mintStatusunkid = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set issubmitapproval
    ''' Modify By: Hemant
    ''' </summary>
    Public Property _IsSubmitApproval() As Boolean
        Get
            Return mblnIsSubmitApproval
        End Get
        Set(ByVal value As Boolean)
            mblnIsSubmitApproval = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set effective_date
    ''' Modify By: Hemant
    ''' </summary>
    Public Property _Effective_Date() As Date
        Get
            Return mdtEffective_Date
        End Get
        Set(ByVal value As Date)
            mdtEffective_Date = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set formtypeid
    ''' Modify By: Hemant
    ''' </summary>
    Public Property _FormTypeId() As Integer
        Get
            Return mintFormTypeId
        End Get
        Set(ByVal value As Integer)
            mintFormTypeId = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set newcoach_work_process
    ''' Modify By: Hemant
    ''' </summary>
    Public Property _NewCoachWorkProcess() As String
        Get
            Return mstrNewCoachWorkProcess
        End Get
        Set(ByVal value As String)
            mstrNewCoachWorkProcess = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set reason_for_change
    ''' Modify By: Hemant
    ''' </summary>
    Public Property _ReasonForChange() As String
        Get
            Return mstrReasonForChange
        End Get
        Set(ByVal value As String)
            mstrReasonForChange = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set prev_coach_employeeunkid
    ''' Modify By: Hemant
    ''' </summary>
    Public Property _Prev_Coach_Employeeunkid() As Integer
        Get
            Return mintPrev_Coach_Employeeunkid
        End Get
        Set(ByVal value As Integer)
            mintPrev_Coach_Employeeunkid = value
        End Set
    End Property


    ''' <summary>
    ''' Purpose: Get or Set formtypeid
    ''' Modify By: Hemant
    ''' </summary>
    Public Property _Prev_CoachingNominationunkid() As Integer
        Get
            Return mintPrev_CoachingNominationunkid
        End Get
        Set(ByVal value As Integer)
            mintPrev_CoachingNominationunkid = value
        End Set
    End Property


    ''' <summary>
    ''' Purpose: Get or Set userunkid
    ''' Modify By: Hemant
    ''' </summary>
    Public Property _Userunkid() As Integer
        Get
            Return mintUserunkid
        End Get
        Set(ByVal value As Integer)
            mintUserunkid = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set isvoid
    ''' Modify By: Hemant
    ''' </summary>
    Public Property _Isvoid() As Boolean
        Get
            Return mblnIsvoid
        End Get
        Set(ByVal value As Boolean)
            mblnIsvoid = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set voiduserunkid
    ''' Modify By: Hemant
    ''' </summary>
    Public Property _Voiduserunkid() As Integer
        Get
            Return mintVoiduserunkid
        End Get
        Set(ByVal value As Integer)
            mintVoiduserunkid = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set voiddatetime
    ''' Modify By: Hemant
    ''' </summary>
    Public Property _Voiddatetime() As Date
        Get
            Return mdtVoiddatetime
        End Get
        Set(ByVal value As Date)
            mdtVoiddatetime = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set voidreason
    ''' Modify By: Hemant
    ''' </summary>
    Public Property _Voidreason() As String
        Get
            Return mstrVoidreason
        End Get
        Set(ByVal value As String)
            mstrVoidreason = value
        End Set
    End Property

    Public WriteOnly Property _HostName() As String
        Set(ByVal value As String)
            mstrHostName = value
        End Set
    End Property

    Public WriteOnly Property _ClientIP() As String
        Set(ByVal value As String)
            mstrClientIP = value
        End Set
    End Property

    Public WriteOnly Property _AuditUserId() As Integer
        Set(ByVal value As Integer)
            mintAuditUserId = value
        End Set
    End Property

    Public WriteOnly Property _IsWeb() As Boolean
        Set(ByVal value As Boolean)
            mblnIsWeb = value
        End Set
    End Property

    Public WriteOnly Property _FormName() As String
        Set(ByVal value As String)
            mstrFormName = value
        End Set
    End Property

    Public Property _MinApprovedPriority() As Integer
        Get
            Return mintMinApprovedPriority
        End Get
        Set(ByVal value As Integer)
            mintMinApprovedPriority = value
        End Set
    End Property

    Public Property _LoginEmployeeunkid() As Integer
        Get
            Return mintLoginEmployeeunkid
        End Get
        Set(ByVal value As Integer)
            mintLoginEmployeeunkid = value
        End Set
    End Property

#End Region

    ''' <summary>
    ''' Modify By: Hemant
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>
    Public Sub GetData()
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception
        Dim objDataOperation As clsDataOperation
        objDataOperation = New clsDataOperation

        Try
            strQ = "SELECT " & _
                       "    coachingnominationunkid " & _
                       ",   ISNULL(refno, '') AS refno " & _
                       ",   application_date " & _
                       ",   coach_employeeunkid " & _
                       ",   coachee_employeeunkid " & _
                       ",   coach_work_process " & _
                       ",   coachee_work_process " & _
                       ",   approvertranunkid " & _
                       ",   statusunkid " & _
                       ",   issubmit_approval " & _
                       ",   effective_date " & _
                       ",   ISNULL(formtypeid, 0) AS formtypeid " & _
                       ",   ISNULL(newcoach_work_process, '') AS newcoach_work_process " & _
                       ",   ISNULL(reason_for_change, '') AS reason_for_change " & _
                       ",   ISNULL(prev_coach_employeeunkid, 0) AS prev_coach_employeeunkid " & _
                       ",   ISNULL(prev_coachingnominationunkid, 0) AS prev_coachingnominationunkid " & _
                       ",   userunkid " & _
                       ",   isvoid " & _
                       ",   voiduserunkid " & _
                       ",   voiddatetime " & _
                       ",   voidreason " & _
                   "FROM pdpcoaching_nomination_form " & _
             "WHERE isvoid = 0 AND coachingnominationunkid = @coachingnominationunkid "

            objDataOperation.AddParameter("@coachingnominationunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintCoachingNominationunkid.ToString)

            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            For Each dtRow As DataRow In dsList.Tables(0).Rows
                mintCoachingNominationunkid = CInt(dtRow.Item("coachingnominationunkid"))
                mstrRefno = dtRow.Item("refno").ToString
                mdtApplication_Date = CDate(dtRow.Item("application_date"))
                mintCoach_Employeeunkid = CInt(dtRow.Item("coach_employeeunkid"))
                mintCoachee_Employeeunkid = CInt(dtRow.Item("coachee_employeeunkid"))
                mstrCoachWorkProcess = dtRow.Item("coach_work_process")
                mstrCoacheeWorkProcess = dtRow.Item("coachee_work_process")
                mintApproverTranunkid = CInt(dtRow.Item("approvertranunkid"))
                mintStatusunkid = CInt(dtRow.Item("statusunkid"))
                mblnIsSubmitApproval = CBool(dtRow.Item("issubmit_approval"))
                mdtEffective_Date = dtRow.Item("effective_date")
                mintFormTypeId = CInt(dtRow.Item("formtypeid"))
                mstrNewCoachWorkProcess = dtRow.Item("newcoach_work_process")
                mstrReasonForChange = dtRow.Item("reason_for_change")
                mintPrev_Coach_Employeeunkid = CInt(dtRow.Item("prev_coach_employeeunkid"))
                mintPrev_CoachingNominationunkid = CInt(dtRow.Item("prev_coachingnominationunkid"))
                mintUserunkid = CInt(dtRow.Item("userunkid"))
                mblnIsvoid = CBool(dtRow.Item("isvoid"))
                mintVoiduserunkid = CInt(dtRow.Item("voiduserunkid"))
                If Not IsDBNull(dtRow.Item("voiddatetime")) Then
                    mdtVoiddatetime = dtRow.Item("voiddatetime")
                End If
                mstrVoidreason = dtRow.Item("voidreason").ToString
                Exit For
            Next
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: GetData; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
    End Sub

    ''' <summary>
    ''' Modify By: Hemant
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>
    Public Function GetList(ByVal xDatabaseName As String, _
                            ByVal xUserUnkid As Integer, _
                            ByVal xYearUnkid As Integer, _
                            ByVal xCompanyUnkid As Integer, _
                            ByVal xPeriodStart As DateTime, _
                            ByVal xPeriodEnd As DateTime, _
                            ByVal xUserModeSetting As String, _
                            ByVal xOnlyApproved As Boolean, _
                            ByVal xIncludeIn_ActiveEmployee As Boolean, _
                            ByVal strTableName As String, _
                            Optional ByVal strfilter As String = "", _
                            Optional ByVal blnApplyUserAccessFilter As Boolean = True, _
                            Optional ByVal IsUsedAsMSS As Boolean = True _
                            ) As DataSet
        Dim objMaster As New clsMasterData
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        Dim objDataOperation As New clsDataOperation

        Try
            Dim xDateJoinQry, xDateFilterQry, xUACQry, xUACFiltrQry, xAdvanceJoinQry As String
            xDateJoinQry = "" : xDateFilterQry = "" : xUACQry = "" : xUACFiltrQry = "" : xAdvanceJoinQry = ""
            Call GetDatesFilterString(xDateJoinQry, xDateFilterQry, xPeriodStart, xPeriodEnd, , , xDatabaseName)
            Call GetAdvanceFilterQry(xAdvanceJoinQry, xPeriodEnd, xDatabaseName)

            If blnApplyUserAccessFilter = True Then
                If IsUsedAsMSS Then Call NewAccessLevelFilterString(xUACQry, xUACFiltrQry, xPeriodEnd, xOnlyApproved, xDatabaseName, xUserUnkid, xCompanyUnkid, xYearUnkid, xUserModeSetting)
            End If

            Dim dsFormType As DataSet = objMaster.getComboListForPDPCoachingFormType("List", False)
            Dim dicFormType As Dictionary(Of Integer, String) = (From p In dsFormType.Tables("List") Select New With {.id = CInt(p.Item("Id")), .name = p.Item("Name").ToString}).ToDictionary(Function(x) x.id, Function(x) x.name)

            strQ &= "SELECT " & _
                       "    pdpcoaching_nomination_form.coachingnominationunkid " & _
                       ",   ISNULL(refno, '') AS refno " & _
                       ",   application_date " & _
                       ",   coach_employeeunkid " & _
                       ",   ISNULL(coach.firstname,'')+' '+ISNULL(coach.othername,'')+' '+ISNULL(coach.surname,'') AS coach " & _
                       ",   coachee_employeeunkid " & _
                       ",   ISNULL(hremployee_master.firstname,'')+' '+ISNULL(hremployee_master.othername,'')+' '+ISNULL(hremployee_master.surname,'') AS coachee " & _
                       ",   coach_work_process " & _
                       ",   coachee_work_process " & _
                       ",   approvertranunkid AS approvertranunkid " & _
                       ",   statusunkid " & _
                       ", CASE " & _
                       "       WHEN pdpcoaching_nomination_form.statusunkid = 1 THEN @Pending " & _
                       "       WHEN pdpcoaching_nomination_form.statusunkid = 2 THEN @Approved " & _
                       "       WHEN pdpcoaching_nomination_form.statusunkid = 3 THEN @Rejected " & _
                       "  END AS Status " & _
                       ",   issubmit_approval " & _
                       ",   effective_date " & _
                       ",   formtypeid " & _
                       ",   newcoach_work_process " & _
                       ",   reason_for_change " & _
                       ",   prev_coach_employeeunkid " & _
                       ",   prev_coachingnominationunkid " & _
                       ",   userunkid " & _
                       ",   isvoid " & _
                       ",   voiduserunkid " & _
                       ",   voiddatetime " & _
                       ",   voidreason "

            strQ &= ", CASE formtypeid "
            For Each pair In dicFormType
                strQ &= " WHEN ISNULL(" & pair.Key & ",'') THEN ISNULL('" & pair.Value & "','') "
            Next
            strQ &= " END AS formtype "

            strQ &= ", ISNULL(A.allowreplacement, 0) AS allowreplacement "

            strQ &= "FROM pdpcoaching_nomination_form " & _
                 " LEFT JOIN hremployee_master coach ON coach.employeeunkid = pdpcoaching_nomination_form.coach_employeeunkid " & _
                 " LEFT JOIN hremployee_master  ON hremployee_master.employeeunkid = pdpcoaching_nomination_form.coachee_employeeunkid " & _
                 " LEFT JOIN ( " & _
                 "              SELECT " & _
                 "                      coachingnominationunkid " & _
                 "                    , ROW_NUMBER() OVER	( PARTITION BY coachee_employeeunkid  ORDER by effective_date desc) AS Rno " & _
                 "                    , 1 AS 'allowreplacement' " & _
                 "              FROM pdpcoaching_nomination_form " & _
                 "              WHERE isvoid = 0 AND statusunkid IN ( " & enPDPCoachingApprovalStatus.PENDING & ", " & enPDPCoachingApprovalStatus.APPROVED & " ) " & _
                 "           ) AS A  ON A.coachingnominationunkid = pdpcoaching_nomination_form.coachingnominationunkid AND A.Rno = 1 "

            If xDateJoinQry.Trim.Length > 0 Then
                strQ &= xDateJoinQry
            End If

            If blnApplyUserAccessFilter = True Then
                If xUACQry.Trim.Length > 0 Then
                    strQ &= xUACQry
                End If
            End If

            If xAdvanceJoinQry.Trim.Length > 0 Then
                strQ &= xAdvanceJoinQry
            End If

            strQ &= " WHERE ISNULL(pdpcoaching_nomination_form.isvoid, 0) = 0 "

            If xUACFiltrQry.Trim.Length > 0 Then
                strQ &= " AND " & xUACFiltrQry
            End If

            If xIncludeIn_ActiveEmployee = False Then
                If xDateFilterQry.Trim.Length > 0 Then
                    strQ &= xDateFilterQry
                End If
            End If

            If strfilter <> "" Then
                strQ &= "And " & strfilter
            End If

            objDataOperation.ClearParameters()
            objDataOperation.AddParameter("@Pending", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 2, "Pending"))
            objDataOperation.AddParameter("@Approved", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 3, "Approved"))
            objDataOperation.AddParameter("@Rejected", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 4, "Rejected"))

            dsList = objDataOperation.ExecQuery(strQ, strTableName)

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: GetList; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
        Return dsList
    End Function

    ''' <summary>
    ''' Modify By: Hemant
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>
    Public Function Save(ByVal xDatabaseName As String, _
                           ByVal xUserUnkid As Integer, _
                           ByVal xYearUnkid As Integer, _
                           ByVal xCompanyUnkid As Integer, _
                           ByVal xUserAccessMode As String, _
                           ByVal xEmployeeAsOnDate As String, _
                           Optional ByVal xDataOp As clsDataOperation = Nothing _
                         ) As Boolean

        Dim objCoachingApproval As New clscoachingapproval_process_tran
        Dim exForce As Exception
        Dim strQ As String = ""

        If xDataOp Is Nothing Then
            objDataOperation = New clsDataOperation
            objDataOperation.BindTransaction()
        Else
            objDataOperation = xDataOp
        End If
        objDataOperation.ClearParameters()
        Try

            If mintCoachingNominationunkid > 0 Then
                If Update(objDataOperation) = False Then
                    If xDataOp Is Nothing Then objDataOperation.ReleaseTransaction(False)
                    Return False
                End If
            Else
                If Insert(objDataOperation) = False Then
                    If xDataOp Is Nothing Then objDataOperation.ReleaseTransaction(False)
                    Return False
                End If
            End If

            If mblnIsSubmitApproval = True Then
                Dim objCoachingRoleMapping As New clscoaching_role_mapping
                Dim dtApprover As DataTable = objCoachingRoleMapping.GetNextApprovers(xDatabaseName, _
                                                                                   xCompanyUnkid, _
                                                                                   xYearUnkid, _
                                                                                   xUserAccessMode, _
                                                                                   enUserPriviledge.AllowToApproveCoachingForm, _
                                                                                   xEmployeeAsOnDate, _
                                                                                   xUserUnkid, _
                                                                                   mintCoachee_Employeeunkid, _
                                                                                   mintFormTypeId, _
                                                                                   objDataOperation, "")

                Dim blnEnableVisibility As Boolean = False
                Dim intMinPriority As Integer = -1
                Dim intApproverID As Integer = -1

                If dtApprover IsNot Nothing AndAlso dtApprover.Rows.Count > 0 Then
                    For Each drRow As DataRow In dtApprover.Rows
                        objCoachingApproval._CoachingNominationunkid = mintCoachingNominationunkid
                        objCoachingApproval._CoacheeEmployeeunkid = mintCoachee_Employeeunkid
                        objCoachingApproval._Approvertranunkid = CInt(drRow("mappingunkid"))
                        objCoachingApproval._Approvaldate = mdtApplication_Date
                        objCoachingApproval._Priority = CInt(drRow("priority"))
                        objCoachingApproval._Statusunkid = mintStatusunkid
                        objCoachingApproval._Userunkid = mintUserunkid
                        objCoachingApproval._ClientIP = mstrClientIP
                        objCoachingApproval._FormName = mstrFormName
                        objCoachingApproval._HostName = mstrHostName
                        objCoachingApproval._IsWeb = mblnIsWeb
                        objCoachingApproval._Mapuserunkid = CInt(drRow("mapuserunkid"))

                        intMinPriority = CInt(dtApprover.Compute("MIN(priority)", "1=1"))

                        If intMinPriority = CInt(drRow("priority")) Then
                            If mintUserunkid = CInt(drRow("mapuserunkid")) Then
                                objCoachingApproval._Statusunkid = enApprovalStatus.APPROVED
                                mintMinApprovedPriority = CInt(drRow("priority"))
                                intApproverID = CInt(drRow("mappingunkid"))
                                blnEnableVisibility = True
                            End If

                            If blnEnableVisibility = True Then
                                objCoachingApproval._VisibleId = enApprovalStatus.APPROVED
                            Else
                                Dim dRow As DataRow() = dtApprover.Select("priority=" & intMinPriority & " AND mapuserunkid=" & mintUserunkid & "")
                                If dRow.Length > 0 Then
                                    objCoachingApproval._VisibleId = enApprovalStatus.APPROVED
                                Else
                                    objCoachingApproval._VisibleId = mintStatusunkid
                                End If
                            End If
                        Else

                            If blnEnableVisibility = True Then
                                Dim intNextMinPriority As Integer = CInt(dtApprover.Compute("MIN(priority)", "priority > " & intMinPriority))
                                If intNextMinPriority = CInt(drRow("priority")) Then
                                    objCoachingApproval._VisibleId = enApprovalStatus.PENDING
                                Else
                                    objCoachingApproval._VisibleId = -1
                                End If
                            Else
                                objCoachingApproval._VisibleId = -1
                            End If

                        End If

                        If objCoachingApproval.Insert(objDataOperation) = False Then
                            exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                            Throw exForce
                        End If
                    Next
                    If blnEnableVisibility = True Then
                        Dim intMaxPriority As Integer = CInt(dtApprover.Compute("MAX(priority)", "1=1"))
                        If intMaxPriority = intMinPriority Then
                            strQ = " UPDATE pdpcoaching_nomination_form SET " & _
                              "      statusunkid = " & enPDPCoachingApprovalStatus.APPROVED & " " & _
                              "     ,approvertranunkid = " & intApproverID & " " & _
                              " WHERE isvoid=0 AND coachingnominationunkid = @coachingnominationunkid "
                            objDataOperation.ClearParameters()
                            objDataOperation.AddParameter("@coachingnominationunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintCoachingNominationunkid.ToString)

                            objDataOperation.ExecNonQuery(strQ)

                            If objDataOperation.ErrorMessage <> "" Then
                                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                                Throw exForce
                            End If

                            mintStatusunkid = enPDPCoachingApprovalStatus.APPROVED

                            If InsertAuditTrails(objDataOperation, enAuditType.EDIT) = False Then
                                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                                Throw exForce
                            End If
                        End If
                    End If
                    dtApprover.Rows.Clear()
                Else
                    mstrMessage = Language.getMessage(mstrModuleName, 6, "Sorry, No approver is available for Selected Coachee. Please assign approver for approval")
                    If xDataOp Is Nothing Then objDataOperation.ReleaseTransaction(False)
                    Return False
                End If
            End If

            If xDataOp Is Nothing Then objDataOperation.ReleaseTransaction(True)

            Return True
        Catch ex As Exception
            If xDataOp Is Nothing Then objDataOperation.ReleaseTransaction(False)
            Throw New Exception(ex.Message & "; Procedure Name: Save; Module Name: " & mstrModuleName)
        Finally
            objCoachingApproval = Nothing
            exForce = Nothing
            If xDataOp Is Nothing Then objDataOperation = Nothing
        End Try
    End Function


    ''' <summary>
    ''' Modify By: Hemant 
    ''' </summary>
    ''' <returns>Boolean</returns>
    ''' <purpose> INSERT INTO Database Table (pdpcoaching_nomination_form) </purpose>
    Public Function Insert(Optional ByVal xDataOp As clsDataOperation = Nothing) As Boolean
        'If isExist(mintCoachee_Employeeunkid, -1, xDataOp) Then
        '    mstrMessage = Language.getMessage(mstrModuleName, 1, "Sorry, This Coach is already nominated for this coachee. Please assign new coachee.")
        '    Return False
        'End If

        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        If xDataOp Is Nothing Then
            objDataOperation = New clsDataOperation
            objDataOperation.BindTransaction()
        Else
            objDataOperation = xDataOp
        End If
        objDataOperation.ClearParameters()

        Try
            objDataOperation.AddParameter("@refno", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrRefno.ToString)
            objDataOperation.AddParameter("@application_date", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, mdtApplication_Date.ToString)
            objDataOperation.AddParameter("@coach_employeeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintCoach_Employeeunkid.ToString)
            objDataOperation.AddParameter("@coachee_employeeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintCoachee_Employeeunkid.ToString)
            objDataOperation.AddParameter("@coach_work_process", SqlDbType.NVarChar, mstrCoachWorkProcess.Trim.Length, mstrCoachWorkProcess.ToString)
            objDataOperation.AddParameter("@coachee_work_process", SqlDbType.NVarChar, mstrCoacheeWorkProcess.Trim.Length, mstrCoacheeWorkProcess.ToString)
            objDataOperation.AddParameter("@approvertranunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintApproverTranunkid.ToString)
            objDataOperation.AddParameter("@statusunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintStatusunkid.ToString)
            objDataOperation.AddParameter("@issubmit_approval", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsSubmitApproval.ToString)
            objDataOperation.AddParameter("@effective_date", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, mdtEffective_Date)
            objDataOperation.AddParameter("@formtypeid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintFormTypeId.ToString)
            objDataOperation.AddParameter("@newcoach_work_process", SqlDbType.NVarChar, mstrNewCoachWorkProcess.Trim.Length, mstrNewCoachWorkProcess.ToString)
            objDataOperation.AddParameter("@reason_for_change", SqlDbType.NVarChar, mstrReasonForChange.Trim.Length, mstrReasonForChange.ToString)
            objDataOperation.AddParameter("@prev_coach_employeeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintPrev_Coach_Employeeunkid.ToString)
            objDataOperation.AddParameter("@prev_coachingnominationunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintPrev_CoachingNominationunkid.ToString)
            objDataOperation.AddParameter("@userunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintUserunkid.ToString)
            objDataOperation.AddParameter("@isvoid", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsvoid.ToString)
            objDataOperation.AddParameter("@voiduserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintVoiduserunkid.ToString)
            If mdtVoiddatetime = Nothing Then
                objDataOperation.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, DBNull.Value)
            Else
                objDataOperation.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, mdtVoiddatetime.ToString)
            End If
            objDataOperation.AddParameter("@voidreason", SqlDbType.NVarChar, mstrVoidreason.Trim.Length, mstrVoidreason.ToString)

            strQ = "INSERT INTO pdpcoaching_nomination_form ( " & _
              "  refno " & _
              ", application_date " & _
              ", coach_employeeunkid " & _
              ", coachee_employeeunkid " & _
              ", coach_work_process " & _
              ", coachee_work_process " & _
              ", approvertranunkid " & _
              ", statusunkid " & _
              ", issubmit_approval " & _
              ", effective_date " & _
              ", formtypeid " & _
              ", newcoach_work_process " & _
              ", reason_for_change " & _
              ", prev_coach_employeeunkid " & _
              ", prev_coachingnominationunkid " & _
              ", userunkid " & _
              ", isvoid " & _
              ", voiduserunkid " & _
              ", voiddatetime " & _
              ", voidreason " & _
            ") VALUES (" & _
              "  @refno " & _
              ", @application_date " & _
              ", @coach_employeeunkid " & _
              ", @coachee_employeeunkid " & _
              ", @coach_work_process " & _
              ", @coachee_work_process " & _
              ", @approvertranunkid " & _
              ", @statusunkid " & _
              ", @issubmit_approval " & _
              ", @effective_date " & _
              ", @formtypeid " & _
              ", @newcoach_work_process " & _
              ", @reason_for_change " & _
              ", @prev_coach_employeeunkid " & _
              ", @prev_coachingnominationunkid " & _
              ", @userunkid " & _
              ", @isvoid " & _
              ", @voiduserunkid " & _
              ", @voiddatetime " & _
              ", @voidreason " & _
            "); SELECT @@identity"

            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            mintCoachingNominationunkid = CInt(dsList.Tables(0).Rows(0).Item(0))

            If InsertAuditTrails(objDataOperation, enAuditType.ADD) = False Then
                If xDataOp Is Nothing Then objDataOperation.ReleaseTransaction(False)
                Return False
            End If



            If xDataOp Is Nothing Then objDataOperation.ReleaseTransaction(True)

            Return True
        Catch ex As Exception
            If xDataOp Is Nothing Then objDataOperation.ReleaseTransaction(False)
            Throw New Exception(ex.Message & "; Procedure Name: Insert; Module Name: " & mstrModuleName)
            Return False
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            If xDataOp Is Nothing Then objDataOperation = Nothing
        End Try
    End Function

    ''' <summary>
    ''' Modify By: Hemant
    ''' </summary>
    ''' <returns>Boolean</returns>
    ''' <purpose> Update Database Table (pdpcoaching_nomination_form) </purpose>
    Public Function Update(Optional ByVal xDataOp As clsDataOperation = Nothing) As Boolean
        'If isExist(mintCoachee_Employeeunkid, mintCoachingNominationunkid, xDataOp) Then
        '    mstrMessage = Language.getMessage(mstrModuleName, 1, "Sorry, This Coach is already nominated from this coachee. Please assign new coachee.")
        '    Return False
        'End If

        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        If xDataOp Is Nothing Then
            objDataOperation = New clsDataOperation
            objDataOperation.BindTransaction()
        Else
            objDataOperation = xDataOp
        End If
        objDataOperation.ClearParameters()

        Try

            objDataOperation.AddParameter("@refno", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrRefno.ToString)
            objDataOperation.AddParameter("@coachingnominationunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintCoachingNominationunkid.ToString)
            objDataOperation.AddParameter("@application_date", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, mdtApplication_Date.ToString)
            objDataOperation.AddParameter("@coach_employeeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintCoach_Employeeunkid.ToString)
            objDataOperation.AddParameter("@coachee_employeeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintCoachee_Employeeunkid.ToString)
            objDataOperation.AddParameter("@coach_work_process", SqlDbType.NVarChar, mstrCoachWorkProcess.Trim.Length, mstrCoachWorkProcess.ToString)
            objDataOperation.AddParameter("@coachee_work_process", SqlDbType.NVarChar, mstrCoacheeWorkProcess.Trim.Length, mstrCoacheeWorkProcess.ToString)
            objDataOperation.AddParameter("@approvertranunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintApproverTranunkid.ToString)
            objDataOperation.AddParameter("@statusunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintStatusunkid.ToString)
            objDataOperation.AddParameter("@issubmit_approval", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsSubmitApproval.ToString)
            objDataOperation.AddParameter("@effective_date", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, mdtEffective_Date)
            objDataOperation.AddParameter("@formtypeid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintFormTypeId.ToString)
            objDataOperation.AddParameter("@newcoach_work_process", SqlDbType.NVarChar, mstrNewCoachWorkProcess.Trim.Length, mstrNewCoachWorkProcess.ToString)
            objDataOperation.AddParameter("@reason_for_change", SqlDbType.NVarChar, mstrReasonForChange.Trim.Length, mstrReasonForChange.ToString)
            objDataOperation.AddParameter("@prev_coach_employeeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintPrev_Coach_Employeeunkid.ToString)
            objDataOperation.AddParameter("@prev_coachingnominationunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintPrev_CoachingNominationunkid.ToString)
            objDataOperation.AddParameter("@userunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintUserunkid.ToString)
            objDataOperation.AddParameter("@isvoid", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsvoid.ToString)
            objDataOperation.AddParameter("@voiduserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintVoiduserunkid.ToString)
            If mdtVoiddatetime = Nothing Then
                objDataOperation.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, DBNull.Value)
            Else
                objDataOperation.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, mdtVoiddatetime.ToString)
            End If
            objDataOperation.AddParameter("@voidreason", SqlDbType.NVarChar, mstrVoidreason.Trim.Length, mstrVoidreason.ToString)

            strQ = "UPDATE pdpcoaching_nomination_form SET " & _
                   "  application_date = @application_date" & _
                   ", coach_employeeunkid = @coach_employeeunkid" & _
                   ", coachee_employeeunkid = @coachee_employeeunkid" & _
                   ", coach_work_process = @coach_work_process" & _
                   ", coachee_work_process = @coachee_work_process" & _
                   ", approvertranunkid = @approvertranunkid" & _
                   ", statusunkid = @statusunkid" & _
                   ", issubmit_approval = @issubmit_approval" & _
                   ", effective_date = @effective_date" & _
                   ", formtypeid = @formtypeid " & _
                   ", newcoach_work_process = @newcoach_work_process" & _
                   ", reason_for_change = @reason_for_change" & _
                   ", prev_coach_employeeunkid = @prev_coach_employeeunkid " & _
                   ", prev_coachingnominationunkid = @prev_coachingnominationunkid " & _
                   ", userunkid = @userunkid " & _
                   ", isvoid = @isvoid " & _
                   ", voiduserunkid = @voiduserunkid " & _
                   ", voiddatetime = @voiddatetime " & _
                   ", voidreason = @voidreason " & _
                   "WHERE coachingnominationunkid = @coachingnominationunkid "

            Call objDataOperation.ExecNonQuery(strQ)

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            If InsertAuditTrails(objDataOperation, enAuditType.EDIT) = False Then
                If xDataOp Is Nothing Then objDataOperation.ReleaseTransaction(False)
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            If xDataOp Is Nothing Then objDataOperation.ReleaseTransaction(True)

            Return True
        Catch ex As Exception
            If xDataOp Is Nothing Then objDataOperation.ReleaseTransaction(False)
            Throw New Exception(ex.Message & "; Procedure Name: Update; Module Name: " & mstrModuleName)
            Return False
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            If xDataOp Is Nothing Then objDataOperation = Nothing
        End Try
    End Function

    ''' <summary>
    ''' Modify By: Hemant
    ''' </summary>
    ''' <returns>Boolean</returns>
    ''' <purpose> Delete Database Table (pdpcoaching_nomination_form) </purpose>
    Public Function Delete(ByVal intUnkid As Integer) As Boolean
        'If isUsed(intUnkid) Then
        '    mstrMessage = "<Message>"
        '    Return False
        'End If

        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        Dim objDataOperation As clsDataOperation
        objDataOperation = New clsDataOperation
        objDataOperation.BindTransaction()

        Try

            strQ = "UPDATE pdpcoaching_nomination_form SET " & _
                    " isvoid = @isvoid" & _
                    ", voiduserunkid = @voiduserunkid" & _
                    ", voiddatetime = @voiddatetime" & _
                    ", voidreason = @voidreason " & _
                   "WHERE coachingnominationunkid = @coachingnominationunkid "

            objDataOperation.ClearParameters()
            objDataOperation.AddParameter("@coachingnominationunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intUnkid)
            objDataOperation.AddParameter("@isvoid", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsvoid.ToString)
            objDataOperation.AddParameter("@voiduserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintVoiduserunkid.ToString)
            objDataOperation.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, mdtVoiddatetime)
            objDataOperation.AddParameter("@voidreason", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrVoidreason.ToString)

            _CoachingNominationunkid = intUnkid

            Call objDataOperation.ExecNonQuery(strQ)

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            If InsertAuditTrails(objDataOperation, enAuditType.DELETE) = False Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            objDataOperation.ReleaseTransaction(True)

            Return True
        Catch ex As Exception
            objDataOperation.ReleaseTransaction(False)
            Throw New Exception(ex.Message & "; Procedure Name: Update; Module Name: " & mstrModuleName)
            Return False
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
    End Function

    ''' <summary>
    ''' Modify By: Hemant
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>
    Public Function isUsed(ByVal intUnkid As Integer) As Boolean
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        Dim objDataOperation As clsDataOperation
        objDataOperation = New clsDataOperation

        Try
            strQ = "<Query>"

            objDataOperation.AddParameter("@coachingnominationunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intUnkid)

            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            Return dsList.Tables(0).Rows.Count > 0
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: isUsed; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
    End Function

    ''' <summary>
    ''' Modify By: Hemant
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>
    Public Function isExist(ByVal iCoacheeEmployeeId As Integer, Optional ByVal intUnkid As Integer = -1, _
                            Optional ByVal xDataOpr As clsDataOperation = Nothing) As Boolean

        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        If xDataOpr Is Nothing Then
            objDataOperation = New clsDataOperation
        Else
            objDataOperation = xDataOpr
        End If
        objDataOperation.ClearParameters()

        Try
            strQ = "SELECT " & _
                       "    coachingnominationunkid " & _
                       ",   ISNULL(refno, '') AS refno " & _
                       ",   application_date " & _
                       ",   coach_employeeunkid " & _
                       ",   coachee_employeeunkid " & _
                       ",   coach_work_process " & _
                       ",   coachee_work_process " & _
                       ",   approvertranunkid " & _
                       ",   statusunkid " & _
                       ",   issubmit_approval " & _
                       ",   effective_date " & _
                       ",   formtypeid " & _
                       ",   newcoach_work_process " & _
                       ",   reason_for_change " & _
                       ",   prev_coach_employeeunkid " & _
                       ",   prev_coachingnominationunkid " & _
                       ",   userunkid " & _
                       ",   isvoid " & _
                       ",   voiduserunkid " & _
                       ",   voiddatetime " & _
                       ",   voidreason " & _
                    "FROM pdpcoaching_nomination_form " & _
                    "WHERE isvoid = 0 " & _
                    " AND coachee_employeeunkid  = @coachee_employeeunkid "

            If intUnkid > 0 Then
                strQ &= " AND coachingnominationunkid <> @coachingnominationunkid"
            End If

            strQ &= " AND statusunkid <> " & CInt(enApprovalStatus.REJECTED) & " "

            objDataOperation.AddParameter("@coachee_employeeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, iCoacheeEmployeeId)
            objDataOperation.AddParameter("@coachingnominationunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intUnkid)

            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            Return dsList.Tables(0).Rows.Count > 0
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: isExist; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            If xDataOpr Is Nothing Then objDataOperation = Nothing
        End Try
    End Function

    Public Function InsertAuditTrails(ByVal objDataOperation As clsDataOperation, ByVal eAuditType As enAuditType) As Boolean
        Dim StrQ As String = ""
        Try
            StrQ = "INSERT INTO atpdpcoaching_nomination_form ( " & _
                       "  tranguid " & _
                       ", coachingnominationunkid " & _
                       ", refno " & _
                       ", application_date " & _
                       ", coach_employeeunkid " & _
                       ", coachee_employeeunkid " & _
                       ", coach_work_process " & _
                       ", coachee_work_process " & _
                       ", approvertranunkid " & _
                       ", statusunkid " & _
                       ", issubmit_approval " & _
                       ", effective_date " & _
                       ", formtypeid " & _
                       ", newcoach_work_process " & _
                       ", reason_for_change " & _
                       ", prev_coach_employeeunkid " & _
                       ", prev_coachingnominationunkid " & _
                       ", audituserunkid " & _
                       ", audittype " & _
                       ", auditdatetime " & _
                       ", isweb " & _
                       ", ip " & _
                       ", hostname " & _
                       ", form_name " & _
                  ") VALUES (" & _
                       "  LOWER(NEWID()) " & _
                       ", @coachingnominationunkid " & _
                       ", @refno " & _
                       ", @application_date " & _
                       ", @coach_employeeunkid " & _
                       ", @coachee_employeeunkid " & _
                       ", @coach_work_process " & _
                       ", @coachee_work_process " & _
                       ", @approvertranunkid " & _
                       ", @statusunkid " & _
                       ", @issubmit_approval " & _
                       ", @effective_date " & _
                       ", @formtypeid " & _
                       ", @newcoach_work_process " & _
                       ", @reason_for_change " & _
                       ", @prev_coach_employeeunkid " & _
                       ", @prev_coachingnominationunkid " & _
                       ", @audituserunkid " & _
                       ", @audittype " & _
                       ", GETDATE() " & _
                       ", @isweb " & _
                       ", @ip " & _
                       ", @hostname " & _
                       ", @form_name " & _
                  ")"

            objDataOperation.ClearParameters()
            objDataOperation.AddParameter("@coachingnominationunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintCoachingNominationunkid.ToString)
            objDataOperation.AddParameter("@refno", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrRefno.ToString)
            objDataOperation.AddParameter("@application_date", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, mdtApplication_Date.ToString)
            objDataOperation.AddParameter("@coach_employeeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintCoach_Employeeunkid.ToString)
            objDataOperation.AddParameter("@coachee_employeeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintCoachee_Employeeunkid.ToString)
            objDataOperation.AddParameter("@coach_work_process", SqlDbType.NVarChar, mstrCoachWorkProcess.Trim.Length, mstrCoachWorkProcess.ToString)
            objDataOperation.AddParameter("@coachee_work_process", SqlDbType.NVarChar, mstrCoacheeWorkProcess.Trim.Length, mstrCoacheeWorkProcess.ToString)
            objDataOperation.AddParameter("@effective_date", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, mdtEffective_Date)
            objDataOperation.AddParameter("@formtypeid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintFormTypeId.ToString)
            objDataOperation.AddParameter("@newcoach_work_process", SqlDbType.NVarChar, mstrNewCoachWorkProcess.Trim.Length, mstrNewCoachWorkProcess.ToString)
            objDataOperation.AddParameter("@reason_for_change", SqlDbType.NVarChar, mstrReasonForChange.Trim.Length, mstrReasonForChange.ToString)
            objDataOperation.AddParameter("@prev_coach_employeeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintPrev_Coach_Employeeunkid.ToString)
            objDataOperation.AddParameter("@prev_coachingnominationunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintPrev_CoachingNominationunkid.ToString)
            objDataOperation.AddParameter("@approvertranunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintApproverTranunkid.ToString)
            objDataOperation.AddParameter("@statusunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintStatusunkid.ToString)
            objDataOperation.AddParameter("@issubmit_approval", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsSubmitApproval.ToString)
            objDataOperation.AddParameter("@audituserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintUserunkid)
            objDataOperation.AddParameter("@audittype", SqlDbType.Int, eZeeDataType.INT_SIZE, eAuditType)
            objDataOperation.AddParameter("@isweb", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsWeb)
            objDataOperation.AddParameter("@ip", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrClientIP)
            objDataOperation.AddParameter("@hostname", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrHostName)
            objDataOperation.AddParameter("@form_name", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrFormName)

            objDataOperation.ExecNonQuery(StrQ)

            If objDataOperation.ErrorMessage <> "" Then
                Throw New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
            End If

            Return True
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: InsertAuditTrails; Module Name: " & mstrModuleName)
        Finally
        End Try
    End Function

    ''' <summary>
    ''' Modify By: Hemant
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>
    Public Function getStatusComboList(ByVal strList As String, Optional ByVal blnAddSelect As Boolean = False) As DataSet
        Dim dsList As New DataSet
        Dim StrQ As String = String.Empty
        objDataOperation = New clsDataOperation
        Try
            If blnAddSelect Then
                StrQ = "SELECT 0 AS Id, @Select AS Name UNION "
            End If

            StrQ &= "SELECT '" & enApprovalStatus.PENDING & "' AS Id, @Pending AS Name UNION " & _
                    "SELECT '" & enApprovalStatus.APPROVED & "' AS Id, @Approved AS Name UNION " & _
                    "SELECT '" & enApprovalStatus.REJECTED & "' AS Id, @Rejected AS Name "

            objDataOperation.AddParameter("@Pending", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 2, "Pending"))
            objDataOperation.AddParameter("@Approved", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 3, "Approved"))
            objDataOperation.AddParameter("@Rejected", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 4, "Rejected"))
            objDataOperation.AddParameter("@Select", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 5, "Select"))

            dsList = objDataOperation.ExecQuery(StrQ, strList)

            If objDataOperation.ErrorMessage <> "" Then
                Throw New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
            End If

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: getStatusComboList; Module Name: " & mstrModuleName)
        Finally
        End Try
        Return dsList
    End Function

    Public Function getNextRefNo(Optional ByVal xDataOp As clsDataOperation = Nothing) As Integer
        Dim dsList As New DataSet
        Dim StrQ As String = String.Empty
        Dim intRefNo As Integer = 1

        If xDataOp Is Nothing Then
            objDataOperation = New clsDataOperation
        Else
            objDataOperation = xDataOp
        End If
        objDataOperation.ClearParameters()

        Try

            StrQ = "SELECT ISNULL(MAX(CAST(refno AS INT)), 0) + 1 AS NextRefNo " & _
                    "FROM pdpcoaching_nomination_form " & _
                    "WHERE pdpcoaching_nomination_form.isvoid = 0 " & _
                          "AND refno <> '' "

            dsList = objDataOperation.ExecQuery(StrQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                Throw New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
            End If

            If dsList.Tables(0).Rows.Count > 0 Then
                intRefNo = CInt(dsList.Tables(0).Rows(0).Item(0))
            End If

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: getNextRefNo; Module Name: " & mstrModuleName)
        Finally
            If xDataOp Is Nothing Then objDataOperation = Nothing
        End Try
        Return intRefNo
    End Function

    ''' <summary>
    ''' Modify By: Hemant
    ''' </summary>
    ''' <purpose> Send Approver Notification </purpose>
    ''' 
    Public Sub Send_Notification_Approver(ByVal xDatabaseName As String _
                                          , ByVal iEmployeeId As Integer _
                                          , ByVal intCurrentPriority As Integer _
                                          , ByVal intCompanyUnkid As Integer _
                                          , ByVal strCoachName As String _
                                          , ByVal strCoacheeName As String _
                                          , ByVal strNewCoachName As String _
                                          , ByVal dtApplicationDate As Date _
                                          , ByVal xPeriodStart As Date _
                                          , ByVal strArutiSelfServiceURL As String _
                                          , Optional ByVal iLoginTypeId As Integer = 0 _
                                          , Optional ByVal iLoginEmployeeId As Integer = 0 _
                                          , Optional ByVal iUserId As Integer = 0 _
                                          , Optional ByVal blnIsSendMail As Boolean = True _
                                          , Optional ByRef lstEmailList As List(Of clsEmailCollection) = Nothing _
                                          )
        Dim dtApprover As DataTable = Nothing
        Dim objApprovalProcessTran As New clscoachingapproval_process_tran
        Dim objUser As New clsUserAddEdit
        Dim blnFinalApproved As Boolean = False
        Dim strLink As String = String.Empty
        Try

            Dim objMail As New clsSendMail
            Dim strSubject As String = ""

            If mintStatusunkid = enApprovalStatus.APPROVED Then blnFinalApproved = True

            dtApprover = objApprovalProcessTran.GetCoachingApprovalData(xDatabaseName, _
                                                                        xPeriodStart, _
                                                                        xPeriodStart, _
                                                                        False, _
                                                                        iEmployeeId, _
                                                                        "  pdpcoaching_nomination_form.coachingnominationunkid = " & mintCoachingNominationunkid, _
                                                                        objDataOperation)

            If dtApprover IsNot Nothing AndAlso dtApprover.Rows.Count > 0 Then
                Dim intMinPriority As Integer

                If intCurrentPriority <= -1 Then
                    intMinPriority = dtApprover.AsEnumerable().Select(Function(x) x.Field(Of Integer)("priority")).Min()
                Else
                    Dim drPriority() As DataRow = dtApprover.Select("priority > " & intCurrentPriority & "")
                    If drPriority.Length > 0 Then
                        intMinPriority = CInt(dtApprover.Compute("MIN(priority)", "priority > " & intCurrentPriority & ""))
                    Else
                        intMinPriority = -1
                    End If

                End If

                If mintFormTypeId = enPDPCoachingFormType.NOMINANTION_FORM Then
                    strSubject = Language.getMessage(mstrModuleName, 7, "Notification for Approving Coaching Nomination Form")
                ElseIf mintFormTypeId = enPDPCoachingFormType.REPLACEMENT_FORM Then
                    strSubject = Language.getMessage(mstrModuleName, 8, "Notification for Approving Coaching Replacement Form")
                End If


                For Each dtRow As DataRow In dtApprover.Select("priority = " & intMinPriority).Distinct

                    objUser._Userunkid = CInt(dtRow.Item("mapuserunkid"))

                    If objUser._Email = "" Then Continue For
                    Dim strMessage As String = ""
                    Dim strContain As String = ""

                    objMail._Subject = strSubject

                    strMessage = "<HTML> <BODY>"
                    Dim info1 As System.Globalization.TextInfo = System.Globalization.CultureInfo.InvariantCulture.TextInfo

                    Dim strUserName As String = info1.ToTitleCase(objUser._Firstname.ToLower()) & " " & info1.ToTitleCase(objUser._Lastname.ToLower())
                    If strUserName.Trim.Length <= 0 Then strUserName = info1.ToTitleCase(objUser._Username.ToLower())

                    strMessage &= Language.getMessage(mstrModuleName, 9, "Dear") & " " & strUserName & ", <BR><BR>"

                    If mintFormTypeId = enPDPCoachingFormType.NOMINANTION_FORM Then
                        strContain &= Language.getMessage(mstrModuleName, 10, "This is the notification for approving Coaching Nomination") & _
                                                           Language.getMessage(mstrModuleName, 11, " with Coach :") & _
                                                           " " & Language.getMessage(mstrModuleName, 12, "#CoachName#")
                    ElseIf mintFormTypeId = enPDPCoachingFormType.REPLACEMENT_FORM Then
                        strContain &= Language.getMessage(mstrModuleName, 13, "This is the notification for approving Coaching Replacement") & _
                                                          Language.getMessage(mstrModuleName, 11, " with Coach :") & _
                                                          " " & Language.getMessage(mstrModuleName, 14, "#NewCoachName#") & _
                                                          " " & Language.getMessage(mstrModuleName, 15, " In place of Coach :") & _
                                                          " " & Language.getMessage(mstrModuleName, 12, "#CoachName#")
                    End If

                    strContain &= " " & Language.getMessage(mstrModuleName, 16, " of Coachee ") & _
                                                             " " & Language.getMessage(mstrModuleName, 17, "#CoacheeName#")

                    strContain &= Language.getMessage(mstrModuleName, 18, " on application date :") & _
                                                        " " & Language.getMessage(mstrModuleName, 19, "#ApplicationDate#") & "."

                    strMessage &= strContain

                    strLink = strArutiSelfServiceURL & "/PDP/wPg_CoachingNominationForm.aspx?" & HttpUtility.UrlEncode(clsCrypto.Encrypt( _
                                                                                                                               mintCoachingNominationunkid & "|" & _
                                                                                                                               intCompanyUnkid & "|" & _
                                                                                                                               CInt(dtRow.Item("mapuserunkid")) & "|" & _
                                                                                                                               CInt(dtRow.Item("pendingcoachingtranunkid")) & "|" & _
                                                                                                                               CInt(dtRow.Item("approvertranunkid")) & "|" & _
                                                                                                                               CStr(True)))

                    strMessage &= "<BR></BR><BR></BR>"

                    If mintFormTypeId = enPDPCoachingFormType.NOMINANTION_FORM Then
                        strMessage &= Language.getMessage(mstrModuleName, 20, "Please click on the following link to Confirm/Reject Coaching Nomination.")
                    ElseIf mintFormTypeId = enPDPCoachingFormType.REPLACEMENT_FORM Then
                        strMessage &= Language.getMessage(mstrModuleName, 21, "Please click on the following link to Confirm/Reject Coaching Replacement.")
                    End If

                    strMessage &= "<BR></BR><a href='" & strLink & "'>" _
                                           & strLink & "</a>"

                    strMessage &= "<p><center style='padding-top:25px;font-size:10.0pt;font-family:&quot;Verdana&quot;,&quot;Sans-Serif&quot;;color:#333399;margin-left:0px;margin-right:0px;margin-top:0px;margin-bottom:10px'><b>&quot;POWERED BY ARUTI HR &amp; PAYROLL MANAGEMENT SOFTWARE.&quot;</b></center></p>"

                    strMessage &= "</BODY></HTML>"

                    Dim strEmailContent As String
                    strEmailContent = strMessage.ToString()
                    strEmailContent = strEmailContent.Replace("#CoachName#", "<B>" & strCoachName & "</B>")
                    strEmailContent = strEmailContent.Replace("#CoacheeName#", "<B>" & strCoacheeName & "</B>")
                    strEmailContent = strEmailContent.Replace("#ApplicationDate#", "<B>" & dtApplicationDate.ToShortDateString & "</B>")
                    If mintFormTypeId = enPDPCoachingFormType.REPLACEMENT_FORM Then
                        strEmailContent = strEmailContent.Replace("#NewCoachName#", "<B>" & strNewCoachName & "</B>")
                    End If

                    objMail._Message = strEmailContent
                    objMail._ToEmail = objUser._Email
                    If iLoginTypeId <= 0 Then iLoginTypeId = enLogin_Mode.DESKTOP
                    If mstrFormName.Trim.Length > 0 Then
                        objMail._Form_Name = mstrFormName
                    End If
                    objMail._LogEmployeeUnkid = iLoginEmployeeId
                    objMail._OperationModeId = iLoginTypeId
                    objMail._UserUnkid = IIf(iUserId <= 0, User._Object._Userunkid, iUserId)
                    objMail._SenderAddress = IIf(objUser._Email = "", objUser._Email, objUser._Email)
                    objMail._ModuleRefId = clsSendMail.enAT_VIEW_TYPE.PDP_MGT

                    If iLoginTypeId = enLogin_Mode.EMP_SELF_SERVICE Then
                        Dim objEmp As New clsEmployee_Master
                        objEmp._Employeeunkid(ConfigParameter._Object._CurrentDateAndTime) = iLoginEmployeeId
                        Dim objEmailColl As New clsEmailCollection(objMail._ToEmail, objMail._Subject, objMail._Message, mstrFormName, mintLoginEmployeeunkid, _
                                                                   mstrClientIP, mstrHostName, 0, iLoginTypeId, clsSendMail.enAT_VIEW_TYPE.PDP_MGT, _
                                                                   IIf(objEmp._Email = "", objEmp._Firstname & " " & objEmp._Surname, objEmp._Email))

                        objEmailList.Add(objEmailColl)


                        objEmp = Nothing

                    Else
                        objUser._Userunkid = IIf(iUserId <= 0, User._Object._Userunkid, iUserId)
                        Dim objEmailColl As New clsEmailCollection(objMail._ToEmail, objMail._Subject, objMail._Message, mstrFormName, mintLoginEmployeeunkid, _
                                                                   mstrClientIP, mstrHostName, objUser._Userunkid, iLoginTypeId, clsSendMail.enAT_VIEW_TYPE.PDP_MGT, _
                                                                   IIf(objUser._Email = "", objUser._Firstname & " " & objUser._Lastname, objUser._Email))

                        objEmailList.Add(objEmailColl)



                    End If
                Next
            End If

            If blnIsSendMail = True Then
                If objEmailList.Count > 0 Then
                    If HttpContext.Current Is Nothing Then
                        objThread = New Thread(AddressOf Send_Notification)
                        objThread.IsBackground = True

                        Dim arr(1) As Object
                        arr(0) = intCompanyUnkid
                        objThread.Start(arr)
                    Else
                        Call Send_Notification(intCompanyUnkid)
                    End If
                End If
            Else
                lstEmailList = objEmailList
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "Send_Notification_Approver", mstrModuleName)
        Finally
            objUser = Nothing
            objApprovalProcessTran = Nothing
        End Try
    End Sub

    Private Sub Send_Notification(ByVal intCompanyUnkid As Object)
        Try
            If objEmailList.Count > 0 Then
                Dim objSendMail As New clsSendMail
                For Each objEmail In objEmailList
                    objSendMail._ToEmail = objEmail._EmailTo
                    objSendMail._Subject = objEmail._Subject
                    objSendMail._Message = objEmail._Message
                    objSendMail._Form_Name = objEmail._Form_Name
                    objSendMail._LogEmployeeUnkid = objEmail._LogEmployeeUnkid
                    objSendMail._OperationModeId = objEmail._OperationModeId
                    objSendMail._UserUnkid = objEmail._UserUnkid
                    objSendMail._SenderAddress = objEmail._SenderAddress
                    objSendMail._ModuleRefId = objEmail._ModuleRefId

                    Dim intCUnkId As Integer = 0
                    If TypeOf intCompanyUnkid Is Integer Then
                        intCUnkId = intCompanyUnkid
                    Else
                        intCUnkId = CInt(intCompanyUnkid(0))
                    End If
                    If objSendMail.SendMail(intCUnkId).ToString.Length > 0 Then
                        Continue For
                    End If
                Next
                objEmailList.Clear()
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "Send_Notification", mstrModuleName)
        End Try
    End Sub


	'<Language> This Auto Generated Text Please Do Not Modify it.
#Region " Language & UI Settings "
	Public Shared Sub SetMessages()
		Try
			Language.setMessage(mstrModuleName, 2, "Pending")
			Language.setMessage(mstrModuleName, 3, "Approved")
			Language.setMessage(mstrModuleName, 4, "Rejected")
			Language.setMessage(mstrModuleName, 5, "Select")
			Language.setMessage(mstrModuleName, 6, "Sorry, No approver is available for Selected Coachee. Please assign approver for approval")
			Language.setMessage(mstrModuleName, 7, "Notification for Approving Coaching Nomination Form")
			Language.setMessage(mstrModuleName, 8, "Notification for Approving Coaching Replacement Form")
			Language.setMessage(mstrModuleName, 9, "Dear")
			Language.setMessage(mstrModuleName, 10, "This is the notification for approving Coaching Nomination")
			Language.setMessage(mstrModuleName, 11, " with Coach :")
			Language.setMessage(mstrModuleName, 12, "#CoachName#")
			Language.setMessage(mstrModuleName, 13, "This is the notification for approving Coaching Replacement")
			Language.setMessage(mstrModuleName, 14, "#NewCoachName#")
			Language.setMessage(mstrModuleName, 15, " In place of Coach :")
			Language.setMessage(mstrModuleName, 16, " of Coachee")
			Language.setMessage(mstrModuleName, 17, "#CoacheeName#")
			Language.setMessage(mstrModuleName, 18, " on application date :")
			Language.setMessage(mstrModuleName, 19, "#ApplicationDate#")
			Language.setMessage(mstrModuleName, 20, "Please click on the following link to Confirm/Reject Coaching Nomination.")
			Language.setMessage(mstrModuleName, 21, "Please click on the following link to Confirm/Reject Coaching Replacement.")
			
		Catch Ex As Exception
            DisplayError.Show("-1", Ex.Message, "SetMessages", mstrModuleName)
		End Try
	End Sub
#End Region 'Language & UI Settings
	'</Language>
End Class
