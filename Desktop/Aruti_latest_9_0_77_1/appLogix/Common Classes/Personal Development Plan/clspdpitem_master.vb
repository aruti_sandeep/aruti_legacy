﻿'************************************************************************************************************************************
'Class Name : clspdpitem_master
'Purpose    :
'Date       : 12-Jan-2021
'Written By : Hemant
'Modified   :
'************************************************************************************************************************************

Imports eZeeCommonLib
''' <summary>
''' Purpose: 
''' Developer: Hemant
''' </summary>
Public Class clspdpitem_master
    Private Shared ReadOnly mstrModuleName As String = "clspdpitem_master"
    Dim mstrMessage As String = ""
    Dim objDataOperation As clsDataOperation

#Region " Private variables "
    Private mintItemunkid As Integer
    Private mstrItem As String = String.Empty
    Private mintCategoryunkid As Integer
    Private mintItemTypeid As Integer
    Private mintSelectionModeid As Integer
    Private mintSortOrder As Integer
    Private mblnIsactive As Boolean = True
    Private mblnIsCompetencySelectionSet As Boolean = False


    Private mstrHostName As String = ""
    Private mstrClientIP As String = ""
    Private mintCompanyUnkid As Integer = 0
    Private mintAuditUserId As Integer = 0
    Private mblnIsWeb As Boolean = False
    Private mstrFormName As String = ""
    Private mstrDatabaseName As String = ""

    'S.SANDEEP |03-MAY-2021| -- START
    'ISSUE/ENHANCEMENT : PDP_PM_LINKING
    Private mintVisibletoid As Integer = 0
    'S.SANDEEP |03-MAY-2021| -- END

#End Region

#Region " Properties "
    ''' <summary>
    ''' Purpose: Get Message from Class 
    ''' Modify By: Hemant
    ''' </summary>
    Public ReadOnly Property _Message() As String
        Get
            Return mstrMessage
        End Get
    End Property

    ''' <summary>
    ''' Purpose: Get or Set itemunkid
    ''' Modify By: Hemant
    ''' </summary>
    Public Property _Itemunkid(Optional ByVal xDataOpr As clsDataOperation = Nothing) As Integer
        Get
            Return mintItemunkid
        End Get
        Set(ByVal value As Integer)
            mintItemunkid = value
            Call GetData(xDataOpr)
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set item
    ''' Modify By: Hemant
    ''' </summary>
    Public Property _Item() As String
        Get
            Return mstrItem
        End Get
        Set(ByVal value As String)
            mstrItem = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set categoryunkid
    ''' Modify By: Hemant
    ''' </summary>
    Public Property _Categoryunkid() As Integer
        Get
            Return mintCategoryunkid
        End Get
        Set(ByVal value As Integer)
            mintCategoryunkid = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set itemtype
    ''' Modify By: Hemant
    ''' </summary>
    Public Property _ItemTypeId() As Integer
        Get
            Return mintItemTypeid
        End Get
        Set(ByVal value As Integer)
            mintItemTypeid = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set selectionmodeid
    ''' Modify By: Hemant
    ''' </summary>
    Public Property _SelectionModeId() As Integer
        Get
            Return mintSelectionModeid
        End Get
        Set(ByVal value As Integer)
            mintSelectionModeid = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set sortorder
    ''' Modify By: Hemant
    ''' </summary>
    Public Property _SortOrder() As Integer
        Get
            Return mintSortOrder
        End Get
        Set(ByVal value As Integer)
            mintSortOrder = value
        End Set
    End Property


    ''' <summary>
    ''' Purpose: Get or Set isactive
    ''' Modify By: Hemant
    ''' </summary>
    Public Property _Isactive() As Boolean
        Get
            Return mblnIsactive
        End Get
        Set(ByVal value As Boolean)
            mblnIsactive = value
        End Set
    End Property

    Public Property _IsCompetencySelectionSet() As Boolean
        Get
            Return mblnIsCompetencySelectionSet
        End Get
        Set(ByVal value As Boolean)
            mblnIsCompetencySelectionSet = value
        End Set
    End Property

    Public WriteOnly Property _FormName() As String
        Set(ByVal value As String)
            mstrFormName = value
        End Set
    End Property

    Public WriteOnly Property _ClientIP() As String
        Set(ByVal value As String)
            mstrClientIP = value
        End Set
    End Property

    Public WriteOnly Property _HostName() As String
        Set(ByVal value As String)
            mstrHostName = value
        End Set
    End Property

    Public WriteOnly Property _FromWeb() As Boolean
        Set(ByVal value As Boolean)
            mblnIsWeb = value
        End Set
    End Property

    Public WriteOnly Property _AuditUserId() As Integer
        Set(ByVal value As Integer)
            mintAuditUserId = value
        End Set
    End Property

    Public WriteOnly Property _CompanyUnkid() As Integer
        Set(ByVal value As Integer)
            mintCompanyUnkid = value
        End Set
    End Property

    Public WriteOnly Property _DatabaseName() As String
        Set(ByVal value As String)
            mstrDatabaseName = value
        End Set
    End Property

    'S.SANDEEP |03-MAY-2021| -- START
    'ISSUE/ENHANCEMENT : PDP_PM_LINKING
    Public Property _Visibletoid() As Integer
        Get
            Return mintVisibletoid
        End Get
        Set(ByVal value As Integer)
            mintVisibletoid = value
        End Set
    End Property
    'S.SANDEEP |03-MAY-2021| -- END

#End Region

#Region " ENUMS "

    Public Enum enPdpCustomType
        FREE_TEXT = 1
        SELECTION = 2
        DATE_SELECTION = 3
        NUMERIC_DATA = 4
    End Enum

    Public Enum enPdpSelectionMode
        TRAINING_OBJECTIVE = 1
        EMPLOYEE_COMPETENCIES = 2
        EMPLOYEE_GOALS = 3
        JOB_CAPABILITIES_COURSES = 4
        CAREER_DEVELOPMENT_COURSES = 5
        PERFORMANCE_CUSTOM_ITEM = 6
        'S.SANDEEP |03-MAY-2021| -- START
        'ISSUE/ENHANCEMENT : PDP_PM_LINKING
        PERFORMANCE_PERIOD = 7
        'S.SANDEEP |03-MAY-2021| -- END
    End Enum
#End Region


    ''' <summary>
    ''' Modify By: Hemant
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>
    Public Sub GetData(Optional ByVal xDataOpr As clsDataOperation = Nothing)
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        Dim objDataOperation As clsDataOperation
        If xDataOpr Is Nothing Then
            objDataOperation = New clsDataOperation
        Else
            objDataOperation = xDataOpr
        End If
        objDataOperation.ClearParameters()
        Try
            strQ = "SELECT " & _
              "  itemunkid " & _
              ", item " & _
              ", categoryunkid " & _
              ", itemtypeid " & _
              ", selectionmodeid " & _
              ", sortorder " & _
              ", isactive " & _
              ", ISNULL(iscompetencyselectionset, 0) AS iscompetencyselectionset " & _
              ", visibletoid " & _
             "FROM " & mstrDatabaseName & "..pdpitem_master " & _
             "WHERE itemunkid = @itemunkid "
            'S.SANDEEP |03-MAY-2021| -- START {visibletoid} -- END

            objDataOperation.AddParameter("@itemunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintItemunkid.ToString)

            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            For Each dtRow As DataRow In dsList.Tables(0).Rows
                mintItemunkid = CInt(dtRow.Item("itemunkid"))
                mstrItem = dtRow.Item("item").ToString
                mintCategoryunkid = CInt(dtRow.Item("categoryunkid"))
                mintItemTypeid = CInt(dtRow.Item("itemtypeid"))
                mintSelectionModeid = CInt(dtRow.Item("selectionmodeid"))
                mintSortOrder = CInt(dtRow.Item("sortorder"))
                mblnIsactive = CBool(dtRow.Item("isactive"))
                mblnIsCompetencySelectionSet = CBool(dtRow.Item("iscompetencyselectionset"))
                Exit For
            Next
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: GetData; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            If xDataOpr Is Nothing Then objDataOperation = Nothing
        End Try
    End Sub

    Public Function GetList_CustomTypes(Optional ByVal iList As String = "List", Optional ByVal iAddSelect As Boolean = False) As DataSet
        Dim StrQ As String = String.Empty
        Dim exForce As Exception
        Dim dsList As New DataSet
        Dim objDataOpr As New clsDataOperation
        Try
            If iAddSelect = True Then
                StrQ = "SELECT 0 AS Id, @Select AS Name UNION "
            End If
            StrQ &= "SELECT '" & enPdpCustomType.FREE_TEXT & "' AS Id, @FT AS Name UNION " & _
                   "SELECT '" & enPdpCustomType.SELECTION & "' AS Id, @ST AS Name UNION " & _
                   "SELECT '" & enPdpCustomType.DATE_SELECTION & "' AS Id, @DS AS Name UNION " & _
                   "SELECT '" & enPdpCustomType.NUMERIC_DATA & "' AS Id, @ND AS Name "

            objDataOpr.AddParameter("@FT", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 2, "Free Text"))
            objDataOpr.AddParameter("@ST", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 3, "Selection"))
            objDataOpr.AddParameter("@DS", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 4, "Date"))
            objDataOpr.AddParameter("@ND", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 8, "Numeric Data"))
            objDataOpr.AddParameter("@Select", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 9, "Select"))

            If iList.Trim.Length <= 0 Then iList = "List"

            dsList = objDataOpr.ExecQuery(StrQ, iList)

            If objDataOpr.ErrorMessage <> "" Then
                exForce = New Exception(objDataOpr.ErrorNumber & " : " & objDataOpr.ErrorMessage)
                Throw exForce
            End If

            Return dsList
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "GetList_CustomTypes", mstrModuleName)
            Return Nothing
        Finally
        End Try
    End Function

    Public Function GetList_SelectionMode(Optional ByVal iList As String = "List", Optional ByVal iAddSelect As Boolean = False) As DataSet
        Dim StrQ As String = String.Empty
        Dim exForce As Exception
        Dim dsList As New DataSet
        Dim objDataOpr As New clsDataOperation
        Try

            If iAddSelect = True Then
                StrQ = "SELECT 0 AS Id, @Select AS Name UNION "
            End If

            StrQ &= "SELECT '" & enPdpSelectionMode.TRAINING_OBJECTIVE & "' AS Id, @TO AS Name UNION " & _
                   "SELECT '" & enPdpSelectionMode.EMPLOYEE_COMPETENCIES & "' AS Id, @EC AS Name UNION " & _
                    "SELECT '" & enPdpSelectionMode.EMPLOYEE_GOALS & "' AS Id, @EG AS Name UNION " & _
                    "SELECT '" & enPdpSelectionMode.JOB_CAPABILITIES_COURSES & "' AS Id, @JCC AS Name UNION " & _
                    "SELECT '" & enPdpSelectionMode.CAREER_DEVELOPMENT_COURSES & "' AS Id, @CDC AS Name UNION " & _
                    "SELECT '" & enPdpSelectionMode.PERFORMANCE_CUSTOM_ITEM & "' AS Id, @PCI AS Name UNION " & _
                    "SELECT '" & enPdpSelectionMode.PERFORMANCE_PERIOD & "' AS Id, @PP AS NAME "


            objDataOpr.AddParameter("@TO", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 5, "Training or Learning Objective"))
            objDataOpr.AddParameter("@EC", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 6, "Employee Competencies"))
            objDataOpr.AddParameter("@EG", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 7, "Employee Goals"))
            objDataOpr.AddParameter("@Select", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 9, "Select"))
            objDataOpr.AddParameter("@JCC", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 14, "Job Capability Courses"))
            objDataOpr.AddParameter("@CDC", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 15, "Career Development Courses"))
            objDataOpr.AddParameter("@PCI", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 18, "Performance Custom Item"))
            objDataOpr.AddParameter("@PP", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 20, "Performance Period"))


            If iList.Trim.Length <= 0 Then iList = "List"

            dsList = objDataOpr.ExecQuery(StrQ, iList)

            If objDataOpr.ErrorMessage <> "" Then
                exForce = New Exception(objDataOpr.ErrorNumber & " : " & objDataOpr.ErrorMessage)
                Throw exForce
            End If

            Return dsList
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "GetList_SelectionMode", mstrModuleName)
            Return Nothing
        Finally
        End Try
    End Function

    ''' <summary>
    ''' Modify By: Hemant
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>
    Public Function GetList(ByVal strTableName As String, _
                            Optional ByVal intCategoryId As Integer = 0, _
                            Optional ByVal blnAddCategoryforPM As Boolean = True) As DataSet
        'S.SANDEEP |03-MAY-2021| -- START {blnAddCategoryforPM} -- END
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception
        Dim objCItems As New clsassess_custom_items
        Dim mdicItemType As New Dictionary(Of Integer, String)
        Dim mdicSelectionMode As New Dictionary(Of Integer, String)

        Dim objDataOperation As clsDataOperation
        objDataOperation = New clsDataOperation

        Dim ds As DataSet = GetList_CustomTypes("List", True)
        mdicItemType = (From p In ds.Tables("List") Select New With {Key .Id = CInt(p.Item("Id")), Key .Name = p.Item("name").ToString}).ToDictionary(Function(x) CInt(x.Id), Function(y) y.Name)

        ds = GetList_SelectionMode("List", False)
        Dim dt As DataTable = New DataView(ds.Tables(0), "ID NOT IN (" & enPdpSelectionMode.EMPLOYEE_COMPETENCIES & ", " & enPdpSelectionMode.EMPLOYEE_GOALS & ", " & enPdpSelectionMode.PERFORMANCE_CUSTOM_ITEM & " ) ", "", DataViewRowState.CurrentRows).ToTable
        mdicSelectionMode = (From p In dt Select New With {Key .Id = CInt(p.Item("Id")), Key .Name = p.Item("name").ToString}).ToDictionary(Function(x) CInt(x.Id), Function(y) y.Name)

        Try
            strQ = "SELECT " & _
              "  itemunkid " & _
              ", ISNULL(item, '') AS item " & _
              ", pdpitem_master.categoryunkid " & _
              ", itemtypeid " & _
              ", selectionmodeid " & _
              ", pdpitem_master.sortorder " & _
              ", pdpitem_master.isactive " & _
              ", ISNULL(pdpitem_master.iscompetencyselectionset, 0) AS iscompetencyselectionset " & _
              ", isnull(pdpcategory_master.category, '') as category " & _
              ", pdpitem_master.visibletoid " & _
              ", CASE WHEN pdpitem_master.visibletoid = '" & enAssessmentMode.APPRAISER_ASSESSMENT & "' THEN @A " & _
              "       WHEN pdpitem_master.visibletoid = '" & enAssessmentMode.REVIEWER_ASSESSMENT & "' THEN @R " & _
              "       WHEN pdpitem_master.visibletoid = '" & enAssessmentMode.SELF_ASSESSMENT & "' THEN @E " & _
              "  ELSE '' END AS ivisibleto " & _
              ", CASE WHEN orgitem in ('Performance Period','Current Role') THEN 1 ELSE 0 END AS idel "
            'S.SANDEEP |03-MAY-2021| -- START {visibletoid,ivisibleto} -- END

            If mdicItemType.Count > 0 Then
                strQ &= ", CASE "
                For Each key In mdicItemType
                    strQ &= " WHEN itemtypeid = " & key.Key & " THEN '" & key.Value & "' "
                Next
                strQ &= " END AS itemtype "
            End If

            If mdicSelectionMode.Count > 0 Then
                strQ &= ", CASE "
                For Each key In mdicSelectionMode
                    strQ &= " WHEN selectionmodeid = " & key.Key & " THEN '" & key.Value & "' "
                Next
                strQ &= " END AS selectionmode "
            End If

            strQ &= "FROM " & mstrDatabaseName & "..pdpitem_master " & _
             "LEFT JOIN pdpcategory_master ON pdpcategory_master.categoryunkid = pdpitem_master.categoryunkid "

            If blnAddCategoryforPM = False Then 'S.SANDEEP |03-MAY-2021| -- START {blnAddCategoryforPM} -- END
                strQ &= " AND pdpcategory_master.isincludeinpm = 0 "
            End If 'S.SANDEEP |03-MAY-2021| -- START {blnAddCategoryforPM} -- END

            strQ &= " WHERE pdpitem_master.isactive = 1 "

            If intCategoryId > 0 Then
                strQ &= " AND pdpitem_master.categoryunkid = @categoryunkid "
                objDataOperation.AddParameter("@categoryunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intCategoryId)
            End If

            strQ &= " ORDER BY  pdpitem_master.categoryunkid, pdpitem_master.sortorder, itemtypeid, selectionmodeid "


            objDataOperation.AddParameter("@E", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 11, "Employee"))
            objDataOperation.AddParameter("@A", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 12, "Assessor"))
            objDataOperation.AddParameter("@R", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 13, "Reviewer"))


            dsList = objDataOperation.ExecQuery(strQ, strTableName)

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: GetList; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
        Return dsList
    End Function


    ''' <summary>
    ''' Modify By: Hemant
    ''' </summary>
    ''' <returns>Boolean</returns>
    ''' <purpose> INSERT INTO Database Table (pdpitem_master) </purpose>
    Public Function Insert(Optional ByVal objDataOpr As clsDataOperation = Nothing, Optional ByVal dtStages As DataTable = Nothing) As Boolean
        If isExist(mstrItem, mintCategoryunkid, -1, objDataOpr) Then
            mstrMessage = Language.getMessage(mstrModuleName, 1, "Sorry, This Item is already defined. Please define new Item.")
            Return False
        End If

        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim strDtFilter As String = ""
        Dim exForce As Exception

        Dim objDataOperation As clsDataOperation
        If objDataOpr Is Nothing Then
            objDataOperation = New clsDataOperation
            objDataOperation.BindTransaction()
        Else
            objDataOperation = objDataOpr
        End If
        objDataOperation.ClearParameters()

        Try
            objDataOperation.AddParameter("@categoryunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintCategoryunkid.ToString)
            objDataOperation.AddParameter("@item", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrItem.ToString)
            objDataOperation.AddParameter("@itemtypeid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintItemTypeid.ToString)
            objDataOperation.AddParameter("@selectionmodeid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintSelectionModeid.ToString)
            objDataOperation.AddParameter("@sortorder", SqlDbType.Int, eZeeDataType.INT_SIZE, mintSortOrder.ToString)
            objDataOperation.AddParameter("@isactive", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsactive.ToString)
            objDataOperation.AddParameter("@iscompetencyselectionset", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsCompetencySelectionSet.ToString)
            'S.SANDEEP |03-MAY-2021| -- START
            'ISSUE/ENHANCEMENT : PDP_PM_LINKING
            objDataOperation.AddParameter("@visibletoid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintVisibletoid.ToString)
            'S.SANDEEP |03-MAY-2021| -- END

            strQ = "INSERT INTO " & mstrDatabaseName & "..pdpitem_master ( " & _
              "  categoryunkid " & _
              ", item " & _
              ", itemtypeid " & _
              ", selectionmodeid " & _
              ", sortorder " & _
              ", isactive" & _
              ", iscompetencyselectionset " & _
              ", visibletoid " & _
            ") VALUES (" & _
              "  @categoryunkid " & _
              ", @item " & _
              ", @itemtypeid " & _
              ", @selectionmodeid " & _
              ", @sortorder " & _
              ", @isactive" & _
              ", @iscompetencyselectionset " & _
              ", @visibletoid " & _
            "); SELECT @@identity"
            'S.SANDEEP |03-MAY-2021| -- START {visibletoid} -- END

            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            mintItemunkid = dsList.Tables(0).Rows(0).Item(0)

            If InsertAuditTrails(objDataOperation, enAuditType.ADD) = False Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            If objDataOpr Is Nothing Then objDataOperation.ReleaseTransaction(True)

            Return True
        Catch ex As Exception
            If objDataOpr Is Nothing Then objDataOperation.ReleaseTransaction(False)
            Throw New Exception(ex.Message & "; Procedure Name: Insert; Module Name: " & mstrModuleName)
            Return False
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            If objDataOpr Is Nothing Then objDataOperation = Nothing
        End Try
    End Function

    ''' <summary>
    ''' Modify By: Hemant
    ''' </summary>
    ''' <returns>Boolean</returns>
    ''' <purpose> Update Database Table (pdpitem_master) </purpose>
    Public Function Update() As Boolean
        If isExist(mstrItem, mintCategoryunkid, mintItemunkid) Then
            mstrMessage = Language.getMessage(mstrModuleName, 1, "Sorry, This Item is already defined. Please define new Item.")
            Return False
        End If

        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        Dim objDataOperation As clsDataOperation
        objDataOperation = New clsDataOperation
        objDataOperation.BindTransaction()
        objDataOperation.ClearParameters()

        Try
            objDataOperation.AddParameter("@itemunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintItemunkid.ToString)
            objDataOperation.AddParameter("@item", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrItem.ToString)
            objDataOperation.AddParameter("@categoryunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintCategoryunkid.ToString)
            objDataOperation.AddParameter("@itemtypeid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintItemTypeid.ToString)
            objDataOperation.AddParameter("@selectionmodeid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintSelectionModeid.ToString)
            objDataOperation.AddParameter("@sortorder", SqlDbType.Int, eZeeDataType.INT_SIZE, mintSortOrder.ToString)
            objDataOperation.AddParameter("@isactive", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsactive.ToString)
            objDataOperation.AddParameter("@iscompetencyselectionset", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsCompetencySelectionSet.ToString)
            'S.SANDEEP |03-MAY-2021| -- START
            'ISSUE/ENHANCEMENT : PDP_PM_LINKING
            objDataOperation.AddParameter("@visibletoid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintVisibletoid.ToString)
            'S.SANDEEP |03-MAY-2021| -- END

            strQ = "UPDATE " & mstrDatabaseName & "..pdpitem_master SET " & _
              " item = @item" & _
              ", categoryunkid = @categoryunkid" & _
              ", itemtypeid = @itemtypeid" & _
              ", selectionmodeid = @selectionmodeid" & _
              ", sortorder = @sortorder" & _
              ", isactive = @isactive " & _
              ", iscompetencyselectionset = @iscompetencyselectionset " & _
              ", visibletoid = @visibletoid " & _
            "WHERE itemunkid = @itemunkid "
            'S.SANDEEP |03-MAY-2021| -- START {visibletoid} -- END

            Call objDataOperation.ExecNonQuery(strQ)

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            If InsertAuditTrails(objDataOperation, enAuditType.EDIT) = False Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            objDataOperation.ReleaseTransaction(True)

            Return True
        Catch ex As Exception
            objDataOperation.ReleaseTransaction(False)
            Throw New Exception(ex.Message & "; Procedure Name: Update; Module Name: " & mstrModuleName)
            Return False
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
    End Function

    ''' <summary>
    ''' Modify By: Hemant
    ''' </summary>
    ''' <returns>Boolean</returns>
    ''' <purpose> Delete Database Table (pdpitem_master) </purpose>
    Public Function Delete(ByVal intUnkid As Integer, _
                           Optional ByVal xDataOpr As clsDataOperation = Nothing, _
                           Optional ByVal dtStages As DataTable = Nothing) As Boolean
        'If isUsed(intUnkid) Then
        '    mstrMessage = "<Message>"
        '    Return False
        'End If

        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        _Itemunkid(xDataOpr) = intUnkid
        mblnIsactive = False

        Dim objDataOperation As clsDataOperation
        If xDataOpr Is Nothing Then
            objDataOperation = New clsDataOperation
            objDataOperation.BindTransaction()
        Else
            objDataOperation = xDataOpr
        End If
        objDataOperation.ClearParameters()

        Try
            strQ = "UPDATE " & mstrDatabaseName & "..pdpitem_master SET " & _
                   " isactive = 0 " & _
                   "WHERE itemunkid = @itemunkid "

            objDataOperation.AddParameter("@itemunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intUnkid)

            Call objDataOperation.ExecNonQuery(strQ)

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            If InsertAuditTrails(objDataOperation, enAuditType.DELETE) = False Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            If xDataOpr Is Nothing Then objDataOperation.ReleaseTransaction(True)

            Return True
        Catch ex As Exception
            If xDataOpr Is Nothing Then objDataOperation.ReleaseTransaction(False)
            Throw New Exception(ex.Message & "; Procedure Name: Delete; Module Name: " & mstrModuleName)
            Return False
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            If xDataOpr Is Nothing Then objDataOperation = Nothing
        End Try
    End Function

    ''' <summary>
    ''' Modify By: Hemant
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>
    Public Function isUsed(ByVal intUnkid As Integer) As Boolean
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        Dim objDataOperation As clsDataOperation
        objDataOperation = New clsDataOperation

        Try

            Dim Tables() As String = {"pdpcategory_item_mapping", "pdpitemdatatran"}
            For Each value As String In Tables
                Select Case value
                    Case "pdpitemdatatran"
                        strQ = "SELECT itemunkid FROM " & value & " WHERE itemunkid = @itemunkid "

                    Case "pdpcategory_item_mapping"
                        strQ = "SELECT itemid FROM " & value & " WHERE itemid = @itemunkid "
                    Case Else
                End Select
                objDataOperation.ClearParameters()
            objDataOperation.AddParameter("@itemunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intUnkid)
                dsList = objDataOperation.ExecQuery(strQ, "Used")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

                If dsList.Tables("Used").Rows.Count > 0 Then
                    Return True
                    Exit For
                End If

            Next
            Return False
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: isUsed; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
    End Function

    ''' <summary>
    ''' Modify By: Hemant
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>
    Public Function isExist(ByVal strItem As String, ByVal intCategoryId As Integer, Optional ByVal intUnkid As Integer = -1, Optional ByVal objDataOpr As clsDataOperation = Nothing) As Boolean
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        Dim objDataOperation As clsDataOperation
        If objDataOpr Is Nothing Then
            objDataOperation = New clsDataOperation
        Else
            objDataOperation = objDataOpr
        End If
        objDataOperation.ClearParameters()
        Try
            strQ = "SELECT " & _
                   "  itemunkid " & _
                   ", item " & _
                   ", categoryunkid " & _
                   ", itemtypeid " & _
                   ", selectionmodeid " & _
                   ", sortorder " & _
                   ", isactive " & _
                   ", ISNULL(iscompetencyselectionset, 0) AS iscompetencyselectionset " & _
                   ", visibletoid " & _
                   "FROM " & mstrDatabaseName & "..pdpitem_master " & _
                   "WHERE isactive = 1 " & _
                   " AND categoryunkid = @categoryunkid " & _
                   " AND item = @item "
            'S.SANDEEP |03-MAY-2021| -- START {visibletoid} -- END

            If intUnkid > 0 Then
                strQ &= " AND itemunkid <> @itemunkid"
            End If

            objDataOperation.AddParameter("@item", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, strItem)
            objDataOperation.AddParameter("@itemunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intUnkid)
            objDataOperation.AddParameter("@categoryunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intCategoryId)

            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            Return dsList.Tables(0).Rows.Count > 0
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: isExist; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            If objDataOpr Is Nothing Then objDataOperation = Nothing
        End Try
    End Function

    Public Function InsertAuditTrails(ByVal objDataOperation As clsDataOperation, ByVal eAuditType As enAuditType) As Boolean
        Dim StrQ As String = ""
        Try

            StrQ = "INSERT INTO " & mstrDatabaseName & "..atpdpitem_master ( " & _
                    "  tranguid " & _
                    ", itemunkid " & _
                    ", item " & _
                    ", categoryunkid " & _
                    ", iscompetencyselectionset " & _
                    ", itemtypeid " & _
                    ", selectionmodeid " & _
                    ", sortorder " & _
                    ", audittypeid " & _
                    ", audtuserunkid " & _
                    ", auditdatetime " & _
                    ", formname " & _
                    ", ip " & _
                    ", host " & _
                    ", isweb" & _
                    ", visibletoid " & _
                  ") VALUES (" & _
                    "  LOWER(NEWID()) " & _
                    ", @itemunkid " & _
                    ", @item " & _
                    ", @categoryunkid " & _
                    ", @iscompetencyselectionset " & _
                    ", @itemtypeid " & _
                    ", @selectionmodeid " & _
                    ", @sortorder " & _
                    ", @audittypeid " & _
                    ", @audtuserunkid " & _
                    ", GETDATE() " & _
                    ", @formname " & _
                    ", @ip " & _
                    ", @host " & _
                    ", @isweb" & _
                    ", @visibletoid " & _
                  ")"
            'S.SANDEEP |03-MAY-2021| -- START {visibletoid} -- END

            objDataOperation.ClearParameters()
            objDataOperation.AddParameter("@itemunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintItemunkid.ToString)
            objDataOperation.AddParameter("@item", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrItem.ToString)
            objDataOperation.AddParameter("@categoryunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintCategoryunkid.ToString)
            objDataOperation.AddParameter("@itemtypeid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintItemTypeid.ToString)
            objDataOperation.AddParameter("@selectionmodeid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintSelectionModeid.ToString)
            objDataOperation.AddParameter("@sortorder", SqlDbType.Int, eZeeDataType.INT_SIZE, mintSortOrder.ToString)
            objDataOperation.AddParameter("@isactive", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsactive.ToString)
            objDataOperation.AddParameter("@iscompetencyselectionset", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsCompetencySelectionSet.ToString)
            objDataOperation.AddParameter("@audittypeid", SqlDbType.Int, eZeeDataType.INT_SIZE, eAuditType)
            objDataOperation.AddParameter("@audtuserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintAuditUserId)
            objDataOperation.AddParameter("@formname", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrFormName)
            objDataOperation.AddParameter("@ip", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrClientIP)
            objDataOperation.AddParameter("@host", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrHostName)
            objDataOperation.AddParameter("@isweb", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsWeb)
            'S.SANDEEP |03-MAY-2021| -- START
            'ISSUE/ENHANCEMENT : PDP_PM_LINKING
            objDataOperation.AddParameter("@visibletoid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintVisibletoid.ToString)
            'S.SANDEEP |03-MAY-2021| -- END

            objDataOperation.ExecNonQuery(StrQ)

            If objDataOperation.ErrorMessage <> "" Then
                Throw New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
            End If

            Return True
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: InsertAuditTrails; Module Name: " & mstrModuleName)
        Finally
        End Try
    End Function

    Public Function getComboList(ByVal strList As String, Optional ByVal blnSelect As Boolean = False, Optional ByVal intCategoryunkid As Integer = 0) As DataSet
        Dim StrQ As String = ""
        Dim objDataOperation As New clsDataOperation
        Dim dsList As New DataSet
        Dim exForce As Exception
        Try

            If blnSelect Then
                StrQ = "SELECT 0 AS Id, @SE AS Name,-1 AS sortorder,0 AS iscompetencyselectionset,0 AS visibletoid UNION ALL "
            End If
            StrQ &= "SELECT " & _
                    "  itemunkid AS Id " & _
                    ", item AS Name " & _
                    ", sortorder " & _
                    ", ISNULL(iscompetencyselectionset, 0) AS iscompetencyselectionset " & _
                    ", visibletoid " & _
                    "FROM " & mstrDatabaseName & "..pdpitem_master " & _
                    "WHERE isactive = 1 "

            If intCategoryunkid > 0 Then
                StrQ &= " AND categoryunkid = @categoryunkid"
                objDataOperation.AddParameter("@categoryunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intCategoryunkid)
            End If

            StrQ &= " ORDER BY sortorder "

            objDataOperation.AddParameter("@SE", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 19, "Select"))

            dsList = objDataOperation.ExecQuery(StrQ, strList)

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            Return dsList
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: getComboList; Module Name: " & mstrModuleName)
        Finally
        End Try
    End Function

    'S.SANDEEP |29-MAR-2021| -- START
    'ISSUE/ENHANCEMENT : Competencies Training
    Public Function GetAssesmentPeriod(ByVal strDBName As String, ByVal intCompanyId As Integer, Optional ByVal objDataOpr As clsDataOperation = Nothing) As Integer
        'Public Function GetAssesmentPeriod(Optional ByVal objDataOpr As clsDataOperation = Nothing) As Integer
        'S.SANDEEP |29-MAR-2021| -- END

        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        Dim objDataOperation As clsDataOperation
        If objDataOpr Is Nothing Then
            objDataOperation = New clsDataOperation
        Else
            objDataOperation = objDataOpr
        End If
        objDataOperation.ClearParameters()
        Try
            'S.SANDEEP |29-MAR-2021| -- START
            'ISSUE/ENHANCEMENT : Competencies Training
            'strQ = "SELECT top 1 " & _
            '             "periodunkid " & _
            '           ",yearunkid " & _
            '        "FROM cfcommon_period_tran " & _
            '        "WHERE modulerefid = " & CInt(enModuleReference.Assessment) & " " & _
            '        "AND isactive = 1 " & _
            '        "AND statusid = 1 " & _
            '        "AND yearunkid IN (SELECT TOP 1 " & _
            '                  "yearunkid " & _
            '             "FROM hrmsConfiguration..cffinancial_year_tran " & _
            '             "WHERE database_name = (SELECT " & _
            '                       "DB_NAME()) " & _
            '             "AND isclosed = 0 " & _
            '             "AND companyunkid > 0) " & _
            '        "ORDER BY end_date ASC "


            Dim strIsCmptRunningSep As String = ""
            Dim objConfig As New clsConfigOptions
            strIsCmptRunningSep = objConfig.GetKeyValue(intCompanyId, "RunCompetenceAssessmentSeparately", objDataOperation)
            If strIsCmptRunningSep Is Nothing Then strIsCmptRunningSep = "" '-- THIS CONDITION IF THIS SETTING IS NOT CONFIGURED TO MAKING IT BLANK.
            If strIsCmptRunningSep.Trim.Length <= 0 Then strIsCmptRunningSep = "0" '-- THIS WILL EXCUTE ONLY IF ABOVE ONE IS NOT CONFIGURED AND RETURN BLANK.

            strQ = "SELECT top 1 " & _
                         "periodunkid " & _
                       ",yearunkid " & _
                    "FROM cfcommon_period_tran " & _
                    "WHERE modulerefid = " & CInt(enModuleReference.Assessment) & " " & _
                    "AND isactive = 1 " & _
                    "AND statusid = 1 " & _
                    "AND iscmptperiod = " & IIf(CType(strIsCmptRunningSep, Boolean) = True, 1, 0) & " " & _
                    "AND yearunkid IN (SELECT TOP 1 " & _
                              "yearunkid " & _
                         "FROM hrmsConfiguration..cffinancial_year_tran " & _
                         "WHERE database_name = '" & strDBName & "' " & _
                         "AND isclosed = 0 " & _
                         "AND companyunkid > 0) " & _
                    "ORDER BY end_date ASC "
            'S.SANDEEP |29-MAR-2021| -- END
            


            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If


            If IsNothing(dsList) = False AndAlso dsList.Tables(0).Rows.Count > 0 Then
            Return IIf(dsList.Tables(0).Rows.Count > 0, CInt(dsList.Tables(0).Rows(0)("periodunkid")), -1)
            Else
                Return -1
            End If




        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: GetAssesmentPeriod ; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            If objDataOpr Is Nothing Then objDataOperation = Nothing
        End Try
    End Function

    Public Function isSortOrderExist(ByVal intSortOrder As Integer, ByVal intCategoryId As Integer, Optional ByVal intUnkid As Integer = -1, Optional ByVal xDataOper As clsDataOperation = Nothing) As Boolean
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception


        If xDataOper IsNot Nothing Then
            objDataOperation = xDataOper
        Else
            objDataOperation = New clsDataOperation
        End If
        objDataOperation.ClearParameters()


        Try
            strQ = "SELECT " & _
                    "  itemunkid " & _
                    ", item " & _
                    ", sortorder " & _
                    ", categoryunkid " & _
                    "FROM pdpitem_master " & _
                    "WHERE isactive = 1 " & _
                    " AND sortorder = @sortorder and categoryunkid=@categoryunkid "

            If intUnkid > 0 Then
                strQ &= " and itemunkid <> @itemunkid "
                objDataOperation.AddParameter("@itemunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intUnkid)
            End If


            objDataOperation.AddParameter("@categoryunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intCategoryId)
            objDataOperation.AddParameter("@sortorder", SqlDbType.Int, eZeeDataType.INT_SIZE, intSortOrder)
            dsList = objDataOperation.ExecQuery(strQ, "List")
            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            Return dsList.Tables(0).Rows.Count > 0
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: isSortOrderExist; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            If xDataOper Is Nothing Then objDataOperation = Nothing
        End Try
    End Function

    Public Function isPDPTransectionStarted(ByVal intUnkid As Integer) As DataSet
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        Dim objDataOperation As clsDataOperation
        objDataOperation = New clsDataOperation

        Try

            strQ = "SELECT " & _
                       "pdpitem_master.itemtypeid " & _
                      ",pdpitem_master.selectionmodeid " & _
                      ",pdpitemdatatran.itemunkid " & _
                   "FROM pdpitemdatatran " & _
                   "LEFT JOIN pdpitem_master " & _
                        "ON pdpitemdatatran.itemunkid = pdpitem_master.itemunkid " & _
                   "WHERE pdpitemdatatran.itemunkid = @itemunkid "

            objDataOperation.AddParameter("@itemunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intUnkid)

            dsList = objDataOperation.ExecQuery(strQ, "Used")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            Return dsList

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: isUsed; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
    End Function

    '<Language> This Auto Generated Text Please Do Not Modify it.
#Region " Language & UI Settings "
    Public Shared Sub SetMessages()
        Try
            Language.setMessage(mstrModuleName, 1, "Sorry, This Item is already defined. Please define new Item.")
            Language.setMessage(mstrModuleName, 2, "Free Text")
            Language.setMessage(mstrModuleName, 3, "Selection")
            Language.setMessage(mstrModuleName, 4, "Date")
            Language.setMessage(mstrModuleName, 5, "Training or Learning Objective")
            Language.setMessage(mstrModuleName, 6, "Employee Competencies")
            Language.setMessage(mstrModuleName, 7, "Employee Goals")
            Language.setMessage(mstrModuleName, 8, "Numeric Data")
            Language.setMessage(mstrModuleName, 9, "Select")
            Language.setMessage(mstrModuleName, 11, "Employee")
            Language.setMessage(mstrModuleName, 12, "Assessor")
            Language.setMessage(mstrModuleName, 13, "Reviewer")
            Language.setMessage(mstrModuleName, 14, "Job Capability Courses")
            Language.setMessage(mstrModuleName, 15, "Career Development Courses")
            Language.setMessage(mstrModuleName, 18, "Performance Custom Item")
            Language.setMessage(mstrModuleName, 19, "Select")
			Language.setMessage(mstrModuleName, 20, "Performance Period")

        Catch Ex As Exception
            DisplayError.Show("-1", Ex.Message, "SetMessages", mstrModuleName)
        End Try
    End Sub
#End Region 'Language & UI Settings
    '</Language>
End Class
