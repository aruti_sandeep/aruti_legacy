﻿
'************************************************************************************************************************************
'Class Name : clscoachingapproval_process_tran.vb
'Purpose    :
'Date       : 26-Sep-2023
'Written By : Hemant Morker
'Modified   :
'************************************************************************************************************************************
Imports eZeeCommonLib
Imports System

''' <summary>
''' Purpose: 
''' Developer: Hemant Morker
''' </summary>

Public Class clscoachingapproval_process_tran
    Private Shared ReadOnly mstrModuleName As String = "clscoachingapproval_process_tran"
    Dim objDataOperation As clsDataOperation
    Dim mstrMessage As String = ""

#Region " Private variables "

    Private mintPendingCoachingTranunkid As Integer
    Private mintCoachingNominationunkid As Integer
    Private mintCoacheeEmployeeunkid As Integer
    Private mintApproverTranunkid As Integer
    Private mintMapuserunkid As Integer
    Private mintPriority As Integer
    Private mdtApprovaldate As Date
    Private mintStatusunkid As Integer
    Private mstrRemark As String = String.Empty
    Private mintUserunkid As Integer
    Private mintVisibleId As Integer = -1
    Private mblnIsvoid As Boolean
    Private mdtVoiddatetime As Date
    Private mintVoiduserunkid As Integer
    Private mstrVoidreason As String = String.Empty
    Private mstrFormName As String = String.Empty
    Private mstrClientIP As String = String.Empty
    Private mstrHostName As String = String.Empty
    Private mblnIsWeb As Boolean = False

#End Region

#Region " Properties "
    ''' <summary>
    ''' Purpose: Get Message from Class 
    ''' Modify By: Hemant
    ''' </summary>
    Public ReadOnly Property _Message() As String
        Get
            Return mstrMessage
        End Get
    End Property

    '' <summary>
    '' Purpose: Get or Set pendingcoachingtranunkid
    '' Modify By: Hemant
    '' </summary>
    Public Property _PendingCoachingTranunkid(Optional ByVal objDataOp As clsDataOperation = Nothing) As Integer

        Get
            Return mintPendingCoachingTranunkid
        End Get
        Set(ByVal value As Integer)
            mintPendingCoachingTranunkid = value
            Call GetData(objDataOp)
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set coachingnominationunkid
    ''' Modify By: Hemant
    ''' </summary>
    Public Property _CoachingNominationunkid() As Integer
        Get
            Return mintCoachingNominationunkid
        End Get
        Set(ByVal value As Integer)
            mintCoachingNominationunkid = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set coachee_employeeunkid
    ''' Modify By: Hemant
    ''' </summary>
    Public Property _CoacheeEmployeeunkid() As Integer
        Get
            Return mintCoacheeEmployeeunkid
        End Get
        Set(ByVal value As Integer)
            mintCoacheeEmployeeunkid = Value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set approvertranunkid
    ''' Modify By: Hemant
    ''' </summary>
    Public Property _Approvertranunkid() As Integer
        Get
            Return mintApprovertranunkid
        End Get
        Set(ByVal value As Integer)
            mintApprovertranunkid = Value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set mapuserunkid
    ''' Modify By: Hemant
    ''' </summary>
    Public Property _Mapuserunkid() As Integer
        Get
            Return mintMapuserunkid
        End Get
        Set(ByVal value As Integer)
            mintMapuserunkid = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set Priority
    ''' Modify By: Hemant
    ''' </summary>
    Public Property _Priority() As Integer
        Get
            Return mintPriority
        End Get
        Set(ByVal value As Integer)
            mintPriority = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set approvaldate
    ''' Modify By: Hemant
    ''' </summary>
    Public Property _Approvaldate() As Date
        Get
            Return mdtApprovaldate
        End Get
        Set(ByVal value As Date)
            mdtApprovaldate = Value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set statusunkid
    ''' Modify By: Hemant
    ''' </summary>
    Public Property _Statusunkid() As Integer
        Get
            Return mintStatusunkid
        End Get
        Set(ByVal value As Integer)
            mintStatusunkid = Value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set remark
    ''' Modify By: Hemant
    ''' </summary>
    Public Property _Remark() As String
        Get
            Return mstrRemark
        End Get
        Set(ByVal value As String)
            mstrRemark = Value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set userunkid
    ''' Modify By: Hemant
    ''' </summary>
    Public Property _Userunkid() As Integer
        Get
            Return mintUserunkid
        End Get
        Set(ByVal value As Integer)
            mintUserunkid = Value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set visibleid
    ''' Modify By: Hemant
    ''' </summary>
    Public Property _VisibleId() As Integer
        Get
            Return mintVisibleId
        End Get
        Set(ByVal value As Integer)
            mintVisibleId = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set isvoid
    ''' Modify By: Hemant
    ''' </summary>
    Public Property _Isvoid() As Boolean
        Get
            Return mblnIsvoid
        End Get
        Set(ByVal value As Boolean)
            mblnIsvoid = Value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set voiddatetime
    ''' Modify By: Hemant
    ''' </summary>
    Public Property _Voiddatetime() As Date
        Get
            Return mdtVoiddatetime
        End Get
        Set(ByVal value As Date)
            mdtVoiddatetime = Value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set voiduserunkid
    ''' Modify By: Hemant
    ''' </summary>
    Public Property _Voiduserunkid() As Integer
        Get
            Return mintVoiduserunkid
        End Get
        Set(ByVal value As Integer)
            mintVoiduserunkid = Value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set voidreason
    ''' Modify By: Hemant
    ''' </summary>
    Public Property _Voidreason() As String
        Get
            Return mstrVoidreason
        End Get
        Set(ByVal value As String)
            mstrVoidreason = Value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set FormName
    ''' Modify By: Hemant
    ''' </summary>
    Public Property _FormName() As String
        Get
            Return mstrFormName
        End Get
        Set(ByVal value As String)
            mstrFormName = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set ClientIP
    ''' Modify By: Hemant
    ''' </summary>
    Public WriteOnly Property _ClientIP() As String
        Set(ByVal value As String)
            mstrClientIP = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set HostName
    ''' Modify By: Hemant
    ''' </summary>
    Public WriteOnly Property _HostName() As String
        Set(ByVal value As String)
            mstrHostName = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set IsWeb
    ''' Modify By: Hemant
    ''' </summary>
    Public WriteOnly Property _IsWeb() As Boolean
        Set(ByVal value As Boolean)
            mblnIsWeb = value
        End Set
    End Property


#End Region

    '' <summary>
    '' Modify By: Hemant
    '' </summary>
    '' <purpose> Assign all Property variable </purpose>
    Public Sub GetData(Optional ByVal objDataOp As clsDataOperation = Nothing)

        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        If objDataOp IsNot Nothing Then
            objDataOperation = objDataOp
        Else
            objDataOperation = New clsDataOperation
        End If

        Try
            strQ = "SELECT " & _
                      "  pendingcoachingtranunkid " & _
                      ", coachingnominationunkid " & _
                      ", coachee_employeeunkid " & _
                      ", approvertranunkid " & _
                      ", priority " & _
                      ", approvaldate " & _
                      ", statusunkid " & _
                      ", visibleid " & _
                      ", remark " & _
                      ", userunkid " & _
                      ", isvoid " & _
                      ", voiddatetime " & _
                      ", voiduserunkid " & _
                      ", voidreason " & _
                      ", mapuserunkid " & _
                     "FROM pdpcoachingapproval_process_tran " & _
                     "WHERE pendingcoachingtranunkid = @pendingcoachingtranunkid "

            objDataOperation.ClearParameters()

            objDataOperation.AddParameter("@pendingcoachingtranunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintPendingCoachingTranunkid.ToString)

            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            For Each dtRow As DataRow In dsList.Tables(0).Rows
                mintPendingCoachingTranunkid = CInt(dtRow.Item("pendingcoachingtranunkid"))
                mintCoachingNominationunkid = CInt(dtRow.Item("coachingnominationunkid"))
                mintCoacheeEmployeeunkid = CInt(dtRow.Item("coachee_employeeunkid"))
                mintApproverTranunkid = CInt(dtRow.Item("approvertranunkid"))
                mintPriority = CInt(dtRow.Item("priority"))
                mdtApprovaldate = CDate(dtRow.Item("approvaldate"))
                mintStatusunkid = CInt(dtRow.Item("statusunkid"))
                mintVisibleId = CInt(dtRow.Item("visibleid"))
                mstrRemark = dtRow.Item("remark").ToString
                mintUserunkid = CInt(dtRow.Item("userunkid"))
                mblnIsvoid = CBool(dtRow.Item("isvoid"))
                If Not IsDBNull(dtRow.Item("voiddatetime")) Then
                    mdtVoiddatetime = dtRow.Item("voiddatetime")
                End If
                mintVoiduserunkid = CInt(dtRow.Item("voiduserunkid"))
                mstrVoidreason = dtRow.Item("voidreason").ToString
                mintMapuserunkid = CInt(dtRow.Item("mapuserunkid"))
                Exit For
            Next
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: GetData; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If objDataOp Is Nothing Then objDataOperation = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
        End Try
    End Sub

    '' <summary>
    '' Modify By: Hemant
    '' </summary>
    '' <purpose> Assign all Property variable </purpose>
    Public Function GetList(ByVal xDatabaseName As String, _
                            ByVal xUserUnkid As Integer, _
                            ByVal xYearUnkid As Integer, _
                            ByVal xCompanyUnkid As Integer, _
                            ByVal xPeriodStart As DateTime, _
                            ByVal xPeriodEnd As DateTime, _
                            ByVal xUserModeSetting As String, _
                            ByVal xOnlyApproved As Boolean, _
                            ByVal xIncludeIn_ActiveEmployee As Boolean, _
                            ByVal strTableName As String, _
                            Optional ByVal mstrFilter As String = "", _
                            Optional ByVal strAdvanceFilter As String = "") As DataSet
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        objDataOperation = New clsDataOperation

        Try
            Dim StrFinalQurey As String = String.Empty
            Dim StrQCondition As String = String.Empty
            Dim StrQDtFilters As String = String.Empty
            Dim StrQDataJoin As String = String.Empty

            Dim xDateJoinQry, xDateFilterQry, xAdvanceJoinQry As String
            xDateJoinQry = "" : xDateFilterQry = "" : xAdvanceJoinQry = ""
            Call GetDatesFilterString(xDateJoinQry, xDateFilterQry, xPeriodStart, xPeriodEnd, , , xDatabaseName, "Emp")
            Call GetAdvanceFilterQry(xAdvanceJoinQry, xPeriodEnd, xDatabaseName, "Emp")

            Dim dsFormType As DataSet = (New clsMasterData).getComboListForPDPCoachingFormType("List", False)
            Dim dicFormType As Dictionary(Of Integer, String) = (From p In dsFormType.Tables("List") Select New With {.id = CInt(p.Item("Id")), .name = p.Item("Name").ToString}).ToDictionary(Function(x) x.id, Function(x) x.name)

            strQ = "SELECT " & _
                           " -1 AS pendingcoachingtranunkid " & _
                           ", pdpcoaching_nomination_form.coachingnominationunkid " & _
                           ", NULL AS application_date " & _
                           ", pdpcoachingapproval_process_tran.coachee_employeeunkid " & _
                           ", ISNULL(Emp.employeecode, '') + ' - ' + ISNULL(Emp.firstname, '') + ' ' + ISNULL(Emp.othername, '') + ' ' + ISNULL(Emp.surname, '') AS Employee " & _
                           ", ISNULL(Coach.employeecode, '') + ' - ' + ISNULL(Coach.firstname, '') + ' ' + ISNULL(Coach.othername, '') + ' ' + ISNULL(Coach.surname, '') AS Coach " & _
                           ", '' AS formtype " & _
                           ", -1 AS Approverunkid " & _
                           ", -1 AS levelunkid " & _
                           ", -1 AS priority " & _
                           ", -1 AS mapuserunkid " & _
                           ", '' AS ApproverName " & _
                           ", -1 AS coaching_statusunkid " & _
                           ", -1 AS statusunkid " & _
                           ", '' AS status " & _
                           ", '' AS remark " & _
                           ", -1 AS stationunkid " & _
                           ", -1 AS deptgroupunkid " & _
                           ", -1 AS departmentunkid " & _
                           ", -1 AS sectiongroupunkid " & _
                           ", -1 AS sectionunkid " & _
                           ", -1 AS unitgroupunkid " & _
                           ", -1 AS unitunkid " & _
                           ", -1 AS teamunkid " & _
                           ", -1 AS classgroupunkid " & _
                           ", -1 AS classunkid " & _
                           ", -1 AS jobgroupunkid " & _
                           ", -1 AS jobunkid " & _
                           ", -1 AS gradegroupunkid " & _
                           ", -1 AS gradeunkid " & _
                           ", -1 AS gradelevelunkid " & _
                           ", 0 AS isvoid " & _
                           ", NULL AS voiddatetime " & _
                           ", '' AS voidreason " & _
                           ", -1 AS voiduserunkid " & _
                           ", 0 AS visibleid " & _
                           ", 1 AS isgrp " & _
                        " FROM pdpcoaching_nomination_form " & _
                        " LEFT JOIN pdpcoachingapproval_process_tran ON pdpcoaching_nomination_form.coachingnominationunkid = pdpcoachingapproval_process_tran.coachingnominationunkid " & _
                                " AND pdpcoaching_nomination_form.isvoid = 0 " & _
                        " LEFT JOIN hremployee_master AS Emp ON Emp.employeeunkid = pdpcoachingapproval_process_tran.coachee_employeeunkid " & _
                        " LEFT JOIN hremployee_master AS Coach ON Coach.employeeunkid = pdpcoaching_nomination_form.coach_employeeunkid " & _
                        " LEFT JOIN pdpcoaching_role_mapping ON pdpcoaching_role_mapping.mappingunkid = pdpcoachingapproval_process_tran.approvertranunkid " & _
                        " LEFT JOIN pdpcoaching_approverlevel_master ON pdpcoaching_approverlevel_master.levelunkid = pdpcoaching_role_mapping.levelunkid "

            strQ &= " WHERE pdpcoaching_nomination_form.isvoid = 0 "

            If mstrFilter.Trim.Length > 0 Then
                strQ &= " AND " & mstrFilter
            End If

            strQ &= " UNION " & _
                       " SELECT " & _
                               "pdpcoachingapproval_process_tran.pendingcoachingtranunkid " & _
                               ", pdpcoaching_nomination_form.coachingnominationunkid " & _
                               ", pdpcoaching_nomination_form.application_date " & _
                               ", pdpcoachingapproval_process_tran.coachee_employeeunkid " & _
                               ", ISNULL(Emp.employeecode, '') + ' - ' + ISNULL(Emp.firstname, '') + ' ' + ISNULL(Emp.othername, '') + ' ' + ISNULL(Emp.surname, '') AS Employee " & _
                               ", ISNULL(Coach.employeecode, '') + ' - ' + ISNULL(Coach.firstname, '') + ' ' + ISNULL(Coach.othername, '') + ' ' + ISNULL(Coach.surname, '') AS Coach "

            strQ &= ", CASE pdpcoaching_nomination_form.formtypeid "
            For Each pair In dicFormType
                strQ &= " WHEN ISNULL(" & pair.Key & ",'') THEN ISNULL('" & pair.Value & "','') "
            Next
            strQ &= " END AS formtype "

            strQ &= ", pdpcoachingapproval_process_tran.approvertranunkid AS Approverunkid " & _
                               ", pdpcoaching_approverlevel_master.levelunkid " & _
                               ", pdpcoachingapproval_process_tran.priority " & _
                               ", pdpcoachingapproval_process_tran.mapuserunkid " & _
                               ", ISNULL(hrmsConfiguration..cfuser_master.username,'') AS ApproverName " & _
                               ", pdpcoaching_nomination_form.statusunkid as coaching_statusunkid " & _
                               ", pdpcoachingapproval_process_tran.statusunkid " & _
                               ", CASE " & _
                                    "WHEN pdpcoachingapproval_process_tran.statusunkid = 1 THEN @Pending " & _
                                    "WHEN pdpcoachingapproval_process_tran.statusunkid = 2 THEN @Approved " & _
                                    "WHEN pdpcoachingapproval_process_tran.statusunkid = 3 THEN @Reject " & _
                                "END AS status " & _
                               ", pdpcoachingapproval_process_tran.remark " & _
                               ", ISNULL(ETT.stationunkid, 0) AS stationunkid " & _
                               ", ISNULL(ETT.deptgroupunkid, 0) AS deptgroupunkid " & _
                               ", ISNULL(ETT.departmentunkid, 0) AS departmentunkid " & _
                               ", ISNULL(ETT.sectiongroupunkid, 0) AS sectiongroupunkid " & _
                               ", ISNULL(ETT.sectionunkid, 0) AS sectionunkid " & _
                               ", ISNULL(ETT.unitgroupunkid, 0) AS unitgroupunkid " & _
                               ", ISNULL(ETT.unitunkid, 0) AS unitunkid " & _
                               ", ISNULL(ETT.teamunkid, 0) AS teamunkid " & _
                               ", ISNULL(ETT.classgroupunkid, 0) AS classgroupunkid " & _
                               ", ISNULL(ETT.classunkid, 0) AS classunkid " & _
                               ", ISNULL(ECT.jobgroupunkid, 0) AS jobgroupunkid " & _
                               ", ISNULL(ECT.jobunkid, 0) AS jobunkid " & _
                               ", ISNULL(GRD.gradegroupunkid, 0) AS gradegroupunkid " & _
                               ", ISNULL(GRD.gradeunkid, 0) AS gradeunkid " & _
                               ", ISNULL(GRD.gradelevelunkid, 0) AS gradelevelunkid " & _
                               ", pdpcoachingapproval_process_tran.isvoid " & _
                               ", pdpcoachingapproval_process_tran.voiddatetime " & _
                               ", pdpcoachingapproval_process_tran.voidreason " & _
                               ", pdpcoachingapproval_process_tran.voiduserunkid " & _
                               ", pdpcoachingapproval_process_tran.visibleid " & _
                               ", 0 AS isgrp " & _
                        " FROM pdpcoachingapproval_process_tran " & _
                                " LEFT JOIN pdpcoaching_nomination_form ON pdpcoaching_nomination_form.coachingnominationunkid = pdpcoachingapproval_process_tran.coachingnominationunkid " & _
                                        " AND pdpcoaching_nomination_form.isvoid = 0 " & _
                                " LEFT JOIN hremployee_master AS Emp ON Emp.employeeunkid = pdpcoachingapproval_process_tran.coachee_employeeunkid " & _
                                " LEFT JOIN hremployee_master AS Coach ON Coach.employeeunkid = pdpcoaching_nomination_form.coach_employeeunkid " & _
                                " LEFT JOIN pdpcoaching_role_mapping ON pdpcoaching_role_mapping.mappingunkid = pdpcoachingapproval_process_tran.approvertranunkid " & _
                                " LEFT JOIN pdpcoaching_approverlevel_master ON pdpcoaching_approverlevel_master.levelunkid = pdpcoaching_role_mapping.levelunkid " & _
                                " LEFT JOIN hrmsConfiguration..cfuser_master ON hrmsConfiguration..cfuser_master.userunkid = pdpcoachingapproval_process_tran.mapuserunkid " & _
                            " LEFT JOIN " & _
                            "( " & _
                            "   SELECT " & _
                            "        stationunkid " & _
                            "       ,deptgroupunkid " & _
                            "       ,departmentunkid " & _
                            "       ,sectiongroupunkid " & _
                            "       ,sectionunkid " & _
                            "       ,unitgroupunkid " & _
                            "       ,unitunkid " & _
                            "       ,teamunkid " & _
                            "       ,classgroupunkid " & _
                            "       ,classunkid " & _
                            "       ,employeeunkid " & _
                            "       ,ROW_NUMBER()OVER(PARTITION BY employeeunkid ORDER BY effectivedate DESC) AS rno " & _
                            "   FROM hremployee_transfer_tran " & _
                            "   WHERE isvoid = 0 AND CONVERT(CHAR(8),effectivedate,112) <= '" & eZeeDate.convertDate(xPeriodEnd) & "' " & _
                            ") AS ETT ON ETT.employeeunkid = Emp.employeeunkid AND ETT.rno = 1 " & _
                            " LEFT JOIN " & _
                            "( " & _
                            "   SELECT " & _
                            "        jobgroupunkid " & _
                            "       ,jobunkid " & _
                            "       ,employeeunkid " & _
                            "       ,ROW_NUMBER()OVER(PARTITION BY employeeunkid ORDER BY effectivedate DESC) AS rno " & _
                            "   FROM hremployee_categorization_tran " & _
                            "   WHERE isvoid = 0 AND CONVERT(CHAR(8),effectivedate,112) <= '" & eZeeDate.convertDate(xPeriodEnd) & "' " & _
                            ") AS ECT ON ECT.employeeunkid = Emp.employeeunkid AND ECT.rno = 1 " & _
                            " LEFT JOIN " & _
                            "( " & _
                            "   SELECT " & _
                            "        gradegroupunkid " & _
                            "       ,gradeunkid " & _
                            "       ,gradelevelunkid " & _
                            "       ,employeeunkid " & _
                            "       ,ROW_NUMBER()OVER(PARTITION BY employeeunkid ORDER BY incrementdate DESC,salaryincrementtranunkid DESC) AS rno " & _
                            "   FROM prsalaryincrement_tran " & _
                            "   WHERE isvoid = 0 AND prsalaryincrement_tran.isapproved = 1 AND CONVERT(CHAR(8),incrementdate,112) <= '" & eZeeDate.convertDate(xPeriodEnd) & "' " & _
                            ") AS GRD ON GRD.employeeunkid = Emp.employeeunkid AND GRD.rno = 1 "

            StrFinalQurey = strQ

            If xDateJoinQry.Trim.Length > 0 Then
                strQ &= xDateJoinQry
            End If

            If xAdvanceJoinQry.Trim.Length > 0 Then
                strQ &= xAdvanceJoinQry
            End If

            StrQCondition &= " WHERE   pdpcoachingapproval_process_tran.isvoid = 0 "

            If xIncludeIn_ActiveEmployee = False Then
                If xDateFilterQry.Trim.Length > 0 Then
                    StrQDtFilters &= xDateFilterQry
                End If
            End If

            If mstrFilter.Trim.Length > 0 Then
                StrQCondition &= " AND " & mstrFilter
            End If

            If strAdvanceFilter.Trim.Length > 0 Then

                StrQCondition &= " AND " & strAdvanceFilter.Replace("ADF", "Emp").Replace("hremployee_master", "Emp")

            End If

            strQ &= StrQCondition
            strQ &= StrQDtFilters

            objDataOperation.ClearParameters()
            objDataOperation.AddParameter("@Pending", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage("clsCoach_Nomination_Form", 2, "Pending"))
            objDataOperation.AddParameter("@Approved", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage("clsCoach_Nomination_Form", 3, "Approved"))
            objDataOperation.AddParameter("@Reject", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage("clsCoach_Nomination_Form", 4, "Rejected"))


            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            Dim dsCompany As DataSet
            Dim objlnApprover As New clsLoanApprover_master

            dsCompany = objlnApprover.GetExternalApproverList(objDataOperation, "Company")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            Dim dsExtList As New DataSet

            For Each dRow As DataRow In dsCompany.Tables("Company").Rows
                strQ = StrFinalQurey
                StrQDtFilters = ""

                If dRow("dbname").ToString.Trim.Length <= 0 Then
                    strQ = strQ.Replace("#APPROVER_NAME#", "ISNULL(cfuser_master.username,'') ")
                    strQ = strQ.Replace("#APPROVER_JOIN#", "LEFT JOIN hrmsConfiguration..cfuser_master ON cfuser_master.userunkid  = pdpcoaching_role_mapping.approverempunkid ")

                    strQ &= StrQCondition
                Else

                    strQ = strQ.Replace("#APPROVER_NAME#", "CASE WHEN ISNULL(App.firstname, '') + ' ' + ISNULL(App.othername, '') + ' ' + ISNULL(App.surname, '') = ' ' THEN ISNULL(cfuser_master.username,'') " & _
                                                                "ELSE ISNULL(App.firstname, '') + ' ' + ISNULL(App.othername, '') + ' ' + ISNULL(App.surname, '') END ")
                    strQ = strQ.Replace("#APPROVER_JOIN#", "LEFT JOIN hrmsConfiguration..cfuser_master ON cfuser_master.userunkid = pdpcoaching_role_mapping.approverempunkid " & _
                                                           "LEFT JOIN #DB_NAME#hremployee_master AS App ON App.employeeunkid = cfuser_master.employeeunkid ")
                    strQ = strQ.Replace("#DB_NAME#", dRow("dbname").ToString & "..")

                    If xDateJoinQry.Trim.Length > 0 Then
                        strQ &= xDateJoinQry
                    End If

                    If xAdvanceJoinQry.Trim.Length > 0 Then
                        strQ &= xAdvanceJoinQry
                    End If

                    strQ &= StrQCondition

                    If xIncludeIn_ActiveEmployee = False Then
                        If xDateFilterQry.Trim.Length > 0 Then
                            strQ &= xDateFilterQry
                        End If
                    End If

                    If strAdvanceFilter.Trim.Length > 0 Then
                        strQ &= " AND " & strAdvanceFilter.Replace("ADF", "Emp")
                    End If

                End If


                strQ &= " AND cfuser_master.companyunkid = " & dRow("companyunkid")

                objDataOperation.ClearParameters()
                objDataOperation.AddParameter("@Pending", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage("clsCoach_Nomination_Form", 2, "Pending"))
                objDataOperation.AddParameter("@Approved", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage("clsCoach_Nomination_Form", 3, "Approved"))
                objDataOperation.AddParameter("@Reject", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage("clsCoach_Nomination_Form", 4, "Rejected"))


                dsExtList = objDataOperation.ExecQuery(strQ, strTableName)

                If objDataOperation.ErrorMessage <> "" Then
                    exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                    Throw exForce
                End If

                If dsList.Tables.Count <= 0 Then
                    dsList.Tables.Add(dsExtList.Tables(0).Copy)
                Else
                    Dim dtExtList As DataTable = New DataView(dsExtList.Tables("List"), "isgrp = 0", "", DataViewRowState.CurrentRows).ToTable
                    dsExtList.Tables.RemoveAt(0)
                    dsExtList.Tables.Add(dtExtList.Copy)
                    dsList.Tables(0).Merge(dsExtList.Tables(0), True)

                End If
            Next

            Dim dtTable As DataTable
            dtTable = New DataView(dsList.Tables("List"), "", "coachingnominationunkid DESC, priority ,isgrp DESC", DataViewRowState.CurrentRows).ToTable.Copy
            dsList.Tables.RemoveAt(0)
            dsList.Tables.Add(dtTable.Copy)

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: GetList; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
        Return dsList
    End Function
    ''' <summary>
    ''' Modify By: Hemant
    ''' </summary>
    ''' <returns>Boolean</returns>
    ''' <purpose> INSERT INTO Database Table (pdpcoachingapproval_process_tran) </purpose>
    Public Function Insert(Optional ByVal objDOperation As clsDataOperation = Nothing) As Boolean
        'If isExist(mstrName) Then
        '    mstrMessage = "<Message>"
        '    Return False
        'End If

        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        If objDOperation Is Nothing Then
            objDataOperation = New clsDataOperation
            objDataOperation.BindTransaction()
        Else
            objDataOperation = objDOperation
        End If

        Try
            objDataOperation.ClearParameters()
            objDataOperation.AddParameter("@coachingnominationunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintCoachingNominationunkid.ToString)
            objDataOperation.AddParameter("@coachee_employeeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintCoacheeEmployeeunkid.ToString)
            objDataOperation.AddParameter("@approvertranunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintApproverTranunkid.ToString)
            objDataOperation.AddParameter("@priority", SqlDbType.Int, eZeeDataType.INT_SIZE, mintPriority.ToString)
            objDataOperation.AddParameter("@approvaldate", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, mdtApprovaldate.ToString)
            objDataOperation.AddParameter("@statusunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintStatusunkid.ToString)
            objDataOperation.AddParameter("@remark", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrRemark.ToString)
            objDataOperation.AddParameter("@visibleid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintVisibleId.ToString)
            objDataOperation.AddParameter("@userunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintUserunkid.ToString)
            objDataOperation.AddParameter("@isvoid", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsvoid.ToString)
            objDataOperation.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, IIf(mdtVoiddatetime <> Nothing, mdtVoiddatetime, DBNull.Value))
            objDataOperation.AddParameter("@voiduserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintVoiduserunkid.ToString)
            objDataOperation.AddParameter("@voidreason", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrVoidreason.ToString)
            objDataOperation.AddParameter("@mapuserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintMapuserunkid.ToString)

            strQ = "INSERT INTO pdpcoachingapproval_process_tran ( " & _
                      "  coachingnominationunkid " & _
                      ", coachee_employeeunkid " & _
                      ", approvertranunkid " & _
                      ", priority " & _
                      ", approvaldate " & _
                      ", statusunkid " & _
                      ", remark " & _
                      ", visibleid " & _
                      ", userunkid " & _
                      ", isvoid " & _
                      ", voiddatetime " & _
                      ", voiduserunkid " & _
                      ", voidreason" & _
                      ", mapuserunkid" & _
                    ") VALUES (" & _
                      "  @coachingnominationunkid " & _
                      ", @coachee_employeeunkid " & _
                      ", @approvertranunkid " & _
                      ", @priority " & _
                      ", @approvaldate " & _
                      ", @statusunkid " & _
                      ", @remark " & _
                      ", @visibleid " & _
                      ", @userunkid " & _
                      ", @isvoid " & _
                      ", @voiddatetime " & _
                      ", @voiduserunkid " & _
                      ", @voidreason" & _
                      ", @mapuserunkid" & _
                    "); SELECT @@identity"

            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            mintPendingCoachingTranunkid = dsList.Tables(0).Rows(0).Item(0)

            If InsertAuditTrails(objDataOperation, 1) = False Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            If objDOperation Is Nothing Then
                objDataOperation.ReleaseTransaction(True)
            End If

            Return True
        Catch ex As Exception
            If objDOperation Is Nothing Then objDataOperation.ReleaseTransaction(False)
            Throw New Exception(ex.Message & "; Procedure Name: Insert; Module Name: " & mstrModuleName)
            Return False
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            If objDOperation Is Nothing Then objDataOperation = Nothing
        End Try
    End Function

    ''' <summary>
    ''' Modify By: Hemant
    ''' </summary>
    ''' <returns>Boolean</returns>
    ''' <purpose> Update Database Table (pdpcoachingapproval_process_tran) </purpose>
    Public Function Update(ByVal xDatabaseName As String _
                           , ByVal xPeriodStart As DateTime _
                           , Optional ByVal objDOperation As clsDataOperation = Nothing) As Boolean


        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        If mintStatusunkid = enPDPCoachingApprovalStatus.PENDING Then Return True

        If objDOperation Is Nothing Then
            objDataOperation = New clsDataOperation
            objDataOperation.BindTransaction()
        Else
            objDataOperation = objDOperation
        End If

        Try
            objDataOperation.ClearParameters()
            objDataOperation.AddParameter("@pendingcoachingtranunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintPendingCoachingTranunkid.ToString)
            objDataOperation.AddParameter("@coachingnominationunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintCoachingNominationunkid.ToString)
            objDataOperation.AddParameter("@coachee_employeeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintCoacheeEmployeeunkid.ToString)
            objDataOperation.AddParameter("@approvertranunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintApproverTranunkid.ToString)
            objDataOperation.AddParameter("@priority", SqlDbType.Int, eZeeDataType.INT_SIZE, mintPriority.ToString)
            objDataOperation.AddParameter("@approvaldate", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, mdtApprovaldate.ToString)
            objDataOperation.AddParameter("@statusunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintStatusunkid.ToString)
            objDataOperation.AddParameter("@remark", SqlDbType.NVarChar, mstrRemark.Length, mstrRemark.ToString)
            objDataOperation.AddParameter("@visibleid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintVisibleId.ToString)
            objDataOperation.AddParameter("@userunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintUserunkid.ToString)
            objDataOperation.AddParameter("@isvoid", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsvoid.ToString)
            objDataOperation.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, IIf(mdtVoiddatetime <> Nothing, mdtVoiddatetime, DBNull.Value))
            objDataOperation.AddParameter("@voiduserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintVoiduserunkid.ToString)
            objDataOperation.AddParameter("@voidreason", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrVoidreason.ToString)
            objDataOperation.AddParameter("@mapuserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintMapuserunkid.ToString)

            strQ = "UPDATE pdpcoachingapproval_process_tran SET " & _
                      "  coachingnominationunkid = @coachingnominationunkid" & _
                      ", coachee_employeeunkid = @coachee_employeeunkid" & _
                      ", approvertranunkid = @approvertranunkid" & _
                      ", priority = @priority " & _
                      ", approvaldate = @approvaldate" & _
                      ", statusunkid = @statusunkid" & _
                      ", remark = @remark" & _
                      ", visibleid = @visibleid " & _
                      ", userunkid = @userunkid" & _
                      ", isvoid = @isvoid" & _
                      ", voiddatetime = @voiddatetime" & _
                      ", voiduserunkid = @voiduserunkid" & _
                      ", voidreason = @voidreason " & _
                      ", mapuserunkid = @mapuserunkid " & _
                    "WHERE pendingcoachingtranunkid = @pendingcoachingtranunkid "

            Call objDataOperation.ExecNonQuery(strQ)

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            If InsertAuditTrails(objDataOperation, 2) = False Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            strQ = " SELECT pendingcoachingtranunkid,coachingnominationunkid,approvertranunkid, priority,visibleid FROM pdpcoachingapproval_process_tran WHERE isvoid = 0 AND coachingnominationunkid = @coachingnominationunkid "
            objDataOperation.ClearParameters()
            objDataOperation.AddParameter("@coachingnominationunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintCoachingNominationunkid.ToString)
            Dim dsApprover As DataSet = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            If dsApprover IsNot Nothing AndAlso dsApprover.Tables(0).Rows.Count > 0 Then

                strQ = " UPDATE pdpcoachingapproval_process_tran set " & _
                          " visibleid = " & mintStatusunkid & _
                          " WHERE  coachingnominationunkid = @coachingnominationunkid and coachee_employeeunkid = @coachee_employeeunkid AND approvertranunkid = @approvertranunkid  AND isvoid = 0   "


                Dim dtVisibility As DataTable = New DataView(dsApprover.Tables(0), "priority = " & mintPriority, "", DataViewRowState.CurrentRows).ToTable

                For i As Integer = 0 To dtVisibility.Rows.Count - 1
                    objDataOperation.ClearParameters()
                    objDataOperation.AddParameter("@coachingnominationunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, dtVisibility.Rows(i)("coachingnominationunkid").ToString)
                    objDataOperation.AddParameter("@coachee_employeeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintCoacheeEmployeeunkid)
                    objDataOperation.AddParameter("@approvertranunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, dtVisibility.Rows(i)("approvertranunkid").ToString)
                    objDataOperation.ExecNonQuery(strQ)

                    If objDataOperation.ErrorMessage <> "" Then
                        exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                        Throw exForce
                    End If

                Next

                Dim intMinPriority As Integer = IIf(IsDBNull(dsApprover.Tables(0).Compute("Min(priority)", "priority > " & mintPriority)), -1, dsApprover.Tables(0).Compute("Min(priority)", "priority > " & mintPriority))

                dtVisibility = New DataView(dsApprover.Tables(0), "priority = " & intMinPriority & " AND priority <> -1", "", DataViewRowState.CurrentRows).ToTable

                If dtVisibility IsNot Nothing AndAlso dtVisibility.Rows.Count > 0 Then

                    If mintStatusunkid = enPDPCoachingApprovalStatus.APPROVED Then
                        strQ = " UPDATE pdpcoachingapproval_process_tran set " & _
                                  " visibleid = 1 " & _
                                  " WHERE  coachingnominationunkid = @coachingnominationunkid and coachee_employeeunkid = @coachee_employeeunkid AND approvertranunkid = @approvertranunkid AND isvoid = 0   "

                    ElseIf mintStatusunkid = enPDPCoachingApprovalStatus.REJECTED Then

                        strQ = " UPDATE pdpcoachingapproval_process_tran set " & _
                                  " visibleid = -1 " & _
                                  " WHERE  coachingnominationunkid = @coachingnominationunkid and coachee_employeeunkid = @coachee_employeeunkid AND approvertranunkid = @approvertranunkid AND isvoid = 0   "
                    End If

                    For i As Integer = 0 To dtVisibility.Rows.Count - 1
                        objDataOperation.ClearParameters()
                        objDataOperation.AddParameter("@coachingnominationunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, dtVisibility.Rows(i)("coachingnominationunkid").ToString)
                        objDataOperation.AddParameter("@coachee_employeeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintCoacheeEmployeeunkid)
                        objDataOperation.AddParameter("@approvertranunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, dtVisibility.Rows(i)("approvertranunkid").ToString)
                        objDataOperation.ExecNonQuery(strQ)

                        If objDataOperation.ErrorMessage <> "" Then
                            exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                            Throw exForce
                        End If

                    Next

                End If

            End If


            If UpdateCoachingNominationStatus(xDatabaseName, xPeriodStart, objDataOperation) = False Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            If objDOperation Is Nothing Then
                objDataOperation.ReleaseTransaction(True)
            End If

            Return True
        Catch ex As Exception
            If objDOperation Is Nothing Then objDataOperation.ReleaseTransaction(False)
            Throw New Exception(ex.Message & "; Procedure Name: Update; Module Name: " & mstrModuleName)
            Return False
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            If objDOperation Is Nothing Then objDataOperation = Nothing
        End Try
    End Function

    ''' <summary>
    ''' Modify By: Hemant
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>
    ''' 
    Public Function UpdateCoachingNominationStatus(ByVal xDatabaseName As String, _
                                                   ByVal xPeriodStart As DateTime, _
                                                   ByVal objDataOperation As clsDataOperation) As Boolean

        Dim exForce As Exception
        Try
            If mintStatusunkid <> enPDPCoachingApprovalStatus.REJECTED AndAlso mintStatusunkid <> enPDPCoachingApprovalStatus.CANCELLED Then
                Dim objLoanApprover As New clsLoanApprover_master

                Dim dtApprover As DataTable = GetCoachingApprovalData(xDatabaseName, _
                                                                      xPeriodStart, _
                                                                      xPeriodStart, _
                                                                      False, _
                                                                      mintCoacheeEmployeeunkid, _
                                                                      "  pdpcoaching_nomination_form.coachingnominationunkid = " & mintCoachingNominationunkid & " AND pdpcoaching_approverlevel_master.priority > " & mintPriority, _
                                                                      objDataOperation)

                If dtApprover IsNot Nothing AndAlso dtApprover.Rows.Count <= 0 Then
                    Dim objCoachingNomination As New clsCoaching_Nomination_Form
                    objCoachingNomination._CoachingNominationunkid = mintCoachingNominationunkid
                    objCoachingNomination._Approvertranunkid = mintApproverTranunkid
                    objCoachingNomination._Statusunkid = mintStatusunkid
                    objCoachingNomination._Effective_Date = ConfigParameter._Object._CurrentDateAndTime
                    objCoachingNomination._ClientIP = mstrClientIP
                    objCoachingNomination._FormName = mstrFormName
                    objCoachingNomination._HostName = mstrHostName
                    objCoachingNomination._IsWeb = mblnIsWeb
                    If objCoachingNomination.Update(objDataOperation) = False Then
                        exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                        Throw exForce
                    End If
                    objCoachingNomination = Nothing
                End If
                dtApprover.Rows.Clear()
                dtApprover = Nothing

            ElseIf mintStatusunkid = enPDPCoachingApprovalStatus.REJECTED Then
                Dim objCoachingNomination As New clsCoaching_Nomination_Form
                objCoachingNomination._CoachingNominationunkid = mintCoachingNominationunkid
                objCoachingNomination._Approvertranunkid = mintApproverTranunkid
                objCoachingNomination._Statusunkid = mintStatusunkid
                objCoachingNomination._Effective_Date = ConfigParameter._Object._CurrentDateAndTime
                objCoachingNomination._ClientIP = mstrClientIP
                objCoachingNomination._FormName = mstrFormName
                objCoachingNomination._HostName = mstrHostName
                objCoachingNomination._IsWeb = mblnIsWeb
                If objCoachingNomination.Update(objDataOperation) = False Then
                    exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                    Throw exForce
                End If
                objCoachingNomination = Nothing
            End If
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: UpdateLoanApplicationStatus; Module Name: " & mstrModuleName)
        End Try
        Return True
    End Function

    ''' <summary>
    ''' Modify By: Hemant
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>
    Public Function GetCoachingApprovalData(ByVal xDatabaseName As String, _
                                            ByVal xPeriodStart As DateTime, _
                                            ByVal xPeriodEnd As DateTime, _
                                            ByVal xIncludeIn_ActiveEmployee As Boolean, _
                                            ByVal intEmployeeID As Integer, _
                                            Optional ByVal mstrFilter As String = "", _
                                            Optional ByVal objDataOp As clsDataOperation = Nothing) As DataTable

        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception
        Try

            Dim strQFinal As String = String.Empty
            Dim strQCondition As String = String.Empty

            If objDataOp IsNot Nothing Then
                objDataOperation = objDataOp
            Else
                objDataOperation = New clsDataOperation
            End If

            Dim xDateJoinQry, xDateFilterQry, xAdvanceJoinQry As String
            xDateJoinQry = "" : xDateFilterQry = "" : xAdvanceJoinQry = ""
            Call GetDatesFilterString(xDateJoinQry, xDateFilterQry, xPeriodStart, xPeriodEnd, , , xDatabaseName)
            Call GetAdvanceFilterQry(xAdvanceJoinQry, xPeriodEnd, xDatabaseName)

            objDataOperation.ClearParameters()

            strQ = " SELECT " & _
                      "  pdpcoaching_nomination_form.coachingnominationunkid " & _
                      ", pdpcoachingapproval_process_tran.pendingcoachingtranunkid " & _
                      ", pdpcoachingapproval_process_tran.coachee_employeeunkid " & _
                      ", #EMP_CODE# AS employeecode " & _
                      ", #EMP_NAME# AS employeename " & _
                      ", pdpcoachingapproval_process_tran.approvertranunkid " & _
                      ", pdpcoaching_approverlevel_master.levelunkid " & _
                      ", pdpcoaching_approverlevel_master.levelname " & _
                      ", pdpcoaching_approverlevel_master.priority " & _
                      ", pdpcoachingapproval_process_tran.mapuserunkid " & _
                      " FROM pdpcoaching_nomination_form " & _
                      " JOIN pdpcoachingapproval_process_tran ON pdpcoachingapproval_process_tran.coachingnominationunkid = pdpcoaching_nomination_form.coachingnominationunkid " & _
                      " JOIN pdpcoaching_role_mapping ON pdpcoachingapproval_process_tran.approvertranunkid = pdpcoaching_role_mapping.mappingunkid " & _
                      "     AND pdpcoaching_role_mapping.isvoid = 0 " & _
                      " #EMPLOYEE_JOIN# " & _
                      " JOIN pdpcoaching_approverlevel_master ON pdpcoaching_approverlevel_master.levelunkid = pdpcoaching_role_mapping.levelunkid " & _
                      " LEFT JOIN hrmsConfiguration..cfuser_master on cfuser_master.userunkid = pdpcoachingapproval_process_tran.mapuserunkid " & _
                      " LEFT JOIN hremployee_master UserEmp ON UserEmp.employeeunkid = hrmsConfiguration..cfuser_master.employeeunkid "

            If xDateJoinQry.Trim.Length > 0 Then
                strQ &= xDateJoinQry.Replace("hremployee_master", "UserEmp")
            End If

            If xAdvanceJoinQry.Trim.Length > 0 Then
                strQ &= xAdvanceJoinQry.Replace("hremployee_master", "UserEmp")
            End If

            strQFinal = strQ

            strQCondition = " WHERE pdpcoachingapproval_process_tran.isvoid = 0  AND pdpcoachingapproval_process_tran.coachee_employeeunkid = " & intEmployeeID

            If mstrFilter.Trim.Length > 0 Then
                strQCondition &= " AND " & mstrFilter.Trim
            End If

            strQ &= strQCondition

            If xIncludeIn_ActiveEmployee = False Then
                If xDateFilterQry.Trim.Length > 0 Then
                    strQ &= xDateFilterQry.Replace("hremployee_master", "UserEmp")
                End If
            End If

            strQ = strQ.Replace("#EMP_CODE#", "ISNULL(hremployee_master.employeecode,'') ")
            strQ = strQ.Replace("#EMP_NAME#", "ISNULL(hremployee_master.firstname,'') + ' ' + ISNULL(hremployee_master.othername, '') + ' ' + ISNULL(hremployee_master.surname,'') ")
            strQ = strQ.Replace("#EMPLOYEE_JOIN#", "LEFT JOIN hremployee_master ON hremployee_master.employeeunkid = pdpcoachingapproval_process_tran.coachee_employeeunkid AND hremployee_master.isapproved = 1 ")


            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            Dim dtTable As DataTable
            dtTable = New DataView(dsList.Tables("List"), "", "priority,employeename", DataViewRowState.CurrentRows).ToTable.Copy
            dsList.Tables.RemoveAt(0)
            dsList.Tables.Add(dtTable.Copy)
            If dsList IsNot Nothing AndAlso dsList.Tables.Count > 0 Then
                Return dsList.Tables("List")
            Else
                Return Nothing
            End If
        Catch ex As Exception
            If objDataOp Is Nothing Then objDataOperation.ReleaseTransaction(False)
            DisplayError.Show("-1", ex.Message, "GetCoachingApprovalData", mstrMessage)
            Return Nothing
        Finally
            exForce = Nothing
            If objDataOp Is Nothing Then objDataOperation = Nothing
        End Try
    End Function

    ''' <summary>
    ''' Modify By: Hemant
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>
    Public Function IsPendingCoachingRequest(ByVal intCoachingNominationunkid As Integer, Optional ByVal blnGetApplicationStatus As Boolean = False) As Boolean
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception
        Try
            objDataOperation = New clsDataOperation

            strQ = " SELECT " & _
                   "     coachingnominationunkid " & _
                   "    ,statusunkid " & _
                   " FROM pdpcoachingapproval_process_tran " & _
                   " WHERE isvoid = 0 " & _
                   "    AND coachingnominationunkid = @coachingnominationunkid "


            If blnGetApplicationStatus = True Then
                strQ &= "    AND statusunkid = 1 AND visibleid < 2 "
            Else
                strQ &= "    AND statusunkid <> 1 "
            End If

            objDataOperation.ClearParameters()
            objDataOperation.AddParameter("@coachingnominationunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intCoachingNominationunkid)

            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            If dsList.Tables("List").Rows.Count > 0 Then
                Return False
            Else
                Return True
            End If

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: IsPendingLoanApplication; Module Name: " & mstrModuleName)
        End Try
    End Function

    ''' <summary>
    ''' Modify By: Hemant
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>
    Public Function GetApprovalTranList(ByVal xDatabaseName As String, _
                                        ByVal xUserUnkid As Integer, _
                                        ByVal xYearUnkid As Integer, _
                                        ByVal xCompanyUnkid As Integer, _
                                        ByVal xPeriodStart As DateTime, _
                                        ByVal xPeriodEnd As DateTime, _
                                        ByVal xUserModeSetting As String, _
                                        ByVal xOnlyApproved As Boolean, _
                                        ByVal xIncludeIn_ActiveEmployee As Boolean, _
                                        ByVal strTableName As String, _
                                        Optional ByVal intEmployeeID As Integer = 0, _
                                        Optional ByVal intCoachingNominationId As Integer = 0, _
                                        Optional ByVal mstrFilter As String = "") As DataSet

        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        Dim objDataOperation As New clsDataOperation
        objDataOperation.ClearParameters()
        Try
            Dim StrFinalQurey As String = String.Empty
            Dim StrQCondition As String = String.Empty
            Dim StrQDtFilters As String = String.Empty
            Dim StrQDataJoin As String = String.Empty

            Dim xDateJoinQry, xDateFilterQry, xAdvanceJoinQry As String
            xDateJoinQry = "" : xDateFilterQry = "" : xAdvanceJoinQry = ""
            Call GetDatesFilterString(xDateJoinQry, xDateFilterQry, xPeriodStart, xPeriodEnd, , , xDatabaseName)
            Call GetAdvanceFilterQry(xAdvanceJoinQry, xPeriodEnd, xDatabaseName)

            strQ = "SELECT " & _
                       "  pdpcoaching_nomination_form.coachingnominationunkid " & _
                       ", pdpcoachingapproval_process_tran.pendingcoachingtranunkid " & _
                       ", pdpcoaching_nomination_form.application_date " & _
                       ", ISNULL(emp.firstname, '') + ' ' + ISNULL(emp.othername, '') + ' ' + ISNULL(emp.surname, '') AS Employee " & _
                       ", pdpcoachingapproval_process_tran.isvoid " & _
                       ", pdpcoachingapproval_process_tran.remark " & _
                       ", pdpcoaching_approverlevel_master.levelunkid " & _
                       ", pdpcoaching_approverlevel_master.levelname " & _
                       ", pdpcoaching_approverlevel_master.priority " & _
                       ", CASE " & _
                            " WHEN pdpcoachingapproval_process_tran.statusunkid = " & enPDPCoachingApprovalStatus.PENDING & " THEN @Pending " & _
                            " WHEN pdpcoachingapproval_process_tran.statusunkid = " & enPDPCoachingApprovalStatus.APPROVED & " THEN @Approved " & _
                            " WHEN pdpcoachingapproval_process_tran.statusunkid = " & enPDPCoachingApprovalStatus.REJECTED & " THEN @Reject " & _
                         " END AS status " & _
                       ", pdpcoachingapproval_process_tran.coachee_employeeunkid " & _
                       ", pdpcoachingapproval_process_tran.approvertranunkid " & _
                       ", pdpcoachingapproval_process_tran.userunkid " & _
                       ", pdpcoachingapproval_process_tran.statusunkid " & _
                       ", pdpcoachingapproval_process_tran.statusunkid " & _
                       ", ISNULL(pdpcoaching_nomination_form.statusunkid, 1) AS 'coaching_statusunkid' " & _
                       ", pdpcoachingapproval_process_tran.isvoid " & _
                       ", pdpcoachingapproval_process_tran.remark " & _
                       ", pdpcoachingapproval_process_tran.mapuserunkid " & _
                     " FROM pdpcoachingapproval_process_tran " & _
                     " LEFT JOIN pdpcoaching_nomination_form    ON pdpcoaching_nomination_form.coachingnominationunkid = pdpcoachingapproval_process_tran.coachingnominationunkid AND pdpcoaching_nomination_form.isvoid = 0 " & _
                     " LEFT JOIN hremployee_master emp    ON pdpcoachingapproval_process_tran.coachee_employeeunkid = emp.employeeunkid " & _
                     " LEFT JOIN pdpcoaching_role_mapping ON pdpcoaching_role_mapping.mappingunkid = pdpcoachingapproval_process_tran.approvertranunkid " & _
                     " LEFT JOIN pdpcoaching_approverlevel_master ON pdpcoaching_approverlevel_master.levelunkid = pdpcoaching_role_mapping.levelunkid " & _
                        " LEFT JOIN " & _
                            "( " & _
                            "   SELECT " & _
                            "        stationunkid " & _
                            "       ,deptgroupunkid " & _
                            "       ,departmentunkid " & _
                            "       ,sectiongroupunkid " & _
                            "       ,sectionunkid " & _
                            "       ,unitgroupunkid " & _
                            "       ,unitunkid " & _
                            "       ,teamunkid " & _
                            "       ,classgroupunkid " & _
                            "       ,classunkid " & _
                            "       ,employeeunkid " & _
                            "       ,ROW_NUMBER()OVER(PARTITION BY employeeunkid ORDER BY effectivedate DESC) AS rno " & _
                            "   FROM hremployee_transfer_tran " & _
                            "   WHERE isvoid = 0 AND CONVERT(CHAR(8),effectivedate,112) <= '" & eZeeDate.convertDate(xPeriodEnd) & "' " & _
                            ") AS ETT ON ETT.employeeunkid = emp.employeeunkid AND ETT.rno = 1 " & _
                            " LEFT JOIN " & _
                            "( " & _
                            "   SELECT " & _
                            "        jobgroupunkid " & _
                            "       ,jobunkid " & _
                            "       ,employeeunkid " & _
                            "       ,ROW_NUMBER()OVER(PARTITION BY employeeunkid ORDER BY effectivedate DESC) AS rno " & _
                            "   FROM hremployee_categorization_tran " & _
                            "   WHERE isvoid = 0 AND CONVERT(CHAR(8),effectivedate,112) <= '" & eZeeDate.convertDate(xPeriodEnd) & "' " & _
                            ") AS ECT ON ECT.employeeunkid = emp.employeeunkid AND ECT.rno = 1 " & _
                            " LEFT JOIN " & _
                            "( " & _
                            "   SELECT " & _
                            "        gradegroupunkid " & _
                            "       ,gradeunkid " & _
                            "       ,gradelevelunkid " & _
                            "       ,employeeunkid " & _
                            "       ,ROW_NUMBER()OVER(PARTITION BY employeeunkid ORDER BY incrementdate DESC,salaryincrementtranunkid DESC) AS rno " & _
                            "   FROM prsalaryincrement_tran " & _
                            "   WHERE isvoid = 0 AND prsalaryincrement_tran.isapproved = 1 AND CONVERT(CHAR(8),incrementdate,112) <= '" & eZeeDate.convertDate(xPeriodEnd) & "' " & _
                            ") AS GRD ON GRD.employeeunkid = emp.employeeunkid AND GRD.rno = 1 "

            StrFinalQurey = strQ

            If xDateJoinQry.Trim.Length > 0 Then
                strQ &= xDateJoinQry.Replace("hremployee_master", "emp")
            End If

            If xAdvanceJoinQry.Trim.Length > 0 Then
                strQ &= xAdvanceJoinQry.Replace("hremployee_master", "emp")
            End If

            StrQCondition &= " WHERE pdpcoachingapproval_process_tran.isvoid = 0 "

            If xIncludeIn_ActiveEmployee = False Then
                If xDateFilterQry.Trim.Length > 0 Then
                    StrQDtFilters &= xDateFilterQry.Replace("hremployee_master", "emp") & " "
                End If
            End If

            If intEmployeeID > 0 Then
                StrQCondition &= " AND pdpcoachingapproval_process_tran.coachee_employeeunkid = @coachee_employeeunkid "
                objDataOperation.AddParameter("@coachee_employeeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intEmployeeID)
            End If

            If intCoachingNominationId > 0 Then
                StrQCondition &= " AND pdpcoachingapproval_process_tran.coachingnominationunkid = @coachingnominationunkid "
                objDataOperation.AddParameter("@coachingnominationunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intCoachingNominationId)
            End If

            If mstrFilter.Trim.Length > 0 Then
                StrQCondition &= " AND " & mstrFilter
            End If

            strQ &= StrQCondition
            strQ &= StrQDtFilters

            objDataOperation.AddParameter("@Pending", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage("clsCoach_Nomination_Form", 2, "Pending"))
            objDataOperation.AddParameter("@Approved", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage("clsCoach_Nomination_Form", 3, "Approved"))
            objDataOperation.AddParameter("@Reject", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage("clsCoach_Nomination_Form", 4, "Rejected"))

            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            Dim dtTable As DataTable
            dtTable = New DataView(dsList.Tables("List"), "", "application_date, priority desc", DataViewRowState.CurrentRows).ToTable.Copy
            dsList.Tables.RemoveAt(0)
            dsList.Tables.Add(dtTable.Copy)
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "GetApprovalTranList", mstrMessage)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
        End Try
        Return dsList
    End Function

    ''' <summary>
    ''' Modify By: Hemant
    ''' </summary>
    ''' <returns>Boolean</returns>
    ''' <purpose> INSERT INTO Database Table (pdpcoachingapproval_process_tran) </purpose>
    Public Function InsertAuditTrails(ByVal objDataOperation As clsDataOperation, ByVal intAuditType As Integer) As Boolean
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        Try
            objDataOperation.ClearParameters()
            objDataOperation.AddParameter("@pendingcoachingtranunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintPendingCoachingTranunkid.ToString)
            objDataOperation.AddParameter("@coachingnominationunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintCoachingNominationunkid.ToString)
            objDataOperation.AddParameter("@coachee_employeeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintCoacheeEmployeeunkid.ToString)
            objDataOperation.AddParameter("@approvertranunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintApproverTranunkid.ToString)
            objDataOperation.AddParameter("@priority", SqlDbType.Int, eZeeDataType.INT_SIZE, mintPriority.ToString)
            objDataOperation.AddParameter("@approvaldate", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, mdtApprovaldate.ToString)
            objDataOperation.AddParameter("@statusunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintStatusunkid.ToString)
            objDataOperation.AddParameter("@remark", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrRemark.ToString)
            objDataOperation.AddParameter("@visibleid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintVisibleId.ToString)
            objDataOperation.AddParameter("@audittypeid", SqlDbType.SmallInt, eZeeDataType.INT_SIZE, intAuditType.ToString)
            objDataOperation.AddParameter("@audituserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintUserunkid)
            objDataOperation.AddParameter("@auditdatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, ConfigParameter._Object._CurrentDateAndTime)
            objDataOperation.AddParameter("@ip", SqlDbType.NVarChar, eZeeDataType.IP_SIZE, IIf(mstrClientIP.Trim.Length <= 0, getIP, mstrClientIP))
            objDataOperation.AddParameter("@host", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, IIf(mstrHostName.Trim.Length <= 0, getHostName, mstrHostName))
            objDataOperation.AddParameter("@formname", SqlDbType.NVarChar, 500, mstrFormName)
            objDataOperation.AddParameter("@isweb", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsWeb)
            objDataOperation.AddParameter("@mapuserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintMapuserunkid.ToString)

            strQ = "INSERT INTO atpdpcoachingapproval_process_tran ( " & _
                      "  tranguid " & _
                      ", pendingcoachingtranunkid " & _
                      ", coachingnominationunkid " & _
                      ", coachee_employeeunkid " & _
                      ", approvertranunkid " & _
                      ", priority " & _
                      ", approvaldate " & _
                      ", statusunkid " & _
                      ", remark " & _
                      ", visibleid " & _
                      ", audittypeid " & _
                      ", audituserunkid " & _
                      ", auditdatetime " & _
                      ", ip " & _
                      ", host" & _
                      ", formname " & _
                      ", isweb " & _
                      ", mapuserunkid " & _
                    ") VALUES (" & _
                      "  LOWER(NEWID()) " & _
                      ", @pendingcoachingtranunkid " & _
                      ", @coachingnominationunkid " & _
                      ", @coachee_employeeunkid " & _
                      ", @approvertranunkid " & _
                      ", @priority " & _
                      ", @approvaldate " & _
                      ", @statusunkid " & _
                      ", @remark " & _
                      ", @visibleid " & _
                      ", @audittypeid " & _
                      ", @audituserunkid " & _
                      ", @auditdatetime " & _
                      ", @ip " & _
                      ", @host" & _
                      ", @formname " & _
                      ", @isweb " & _
                      ", @mapuserunkid " & _
                    "); SELECT @@identity"

            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            Return True
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: InsertAuditTrails; Module Name: " & mstrModuleName)
            Return False
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
        End Try
    End Function

    
	'<Language> This Auto Generated Text Please Do Not Modify it.
#Region " Language & UI Settings "
	Public Shared Sub SetMessages()
		Try
			Language.setMessage("clsCoach_Nomination_Form", 2, "Pending")
			Language.setMessage("clsCoach_Nomination_Form", 3, "Approved")
			Language.setMessage("clsCoach_Nomination_Form", 4, "Rejected")
			
		Catch Ex As Exception
            DisplayError.Show("-1", Ex.Message, "SetMessages", mstrModuleName)
		End Try
	End Sub
#End Region 'Language & UI Settings
	'</Language>
End Class
