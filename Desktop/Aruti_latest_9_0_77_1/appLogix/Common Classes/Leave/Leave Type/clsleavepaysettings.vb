﻿'************************************************************************************************************************************
'Class Name : clsleavepaysettings.vb
'Purpose    :
'Date       :20-Feb-2023
'Written By :Pinkal Jariwala
'Modified   :
'************************************************************************************************************************************

Imports eZeeCommonLib
''' <summary>
''' Purpose: 
''' Developer: Pinkal Jariwala
''' </summary>
Public Class clsleavepaysettings
    Private Const mstrModuleName = "clsleavepaysettings"
    Dim objDataOperation As clsDataOperation
    Dim mstrMessage As String = ""

#Region " Private variables "
    Private mintLeavepayunkid As Integer
    Private mintLeavetypeunkid As Integer
    Private mdtPaySetting As DataTable
    Private mintTenureupto As Integer
    Private mdblDeduction_Percentage As Double
    Private mintUserunkid As Integer
    Private mblnIsvoid As Boolean
    Private mdtVoiddatatime As Date
    Private mintVoiduserunkid As Integer
    Private mstrVoidreason As String = String.Empty
    Private mstrFormName As String = ""
    Private mstrClientIP As String = ""
    Private mstrHostName As String = ""
    Private mblnIsFromWeb As Boolean = False
    Private mintAuditUserId As Integer = 0
#End Region

#Region " Constructor "

    Public Sub New()
        mdtPaySetting = New DataTable("List")
        Dim dCol As DataColumn
        Try
            dCol = New DataColumn("leavepayunkid")
            dCol.DataType = System.Type.GetType("System.Int32")
            mdtPaySetting.Columns.Add(dCol)

            dCol = New DataColumn("leavetypeunkid")
            dCol.DataType = System.Type.GetType("System.Int32")
            mdtPaySetting.Columns.Add(dCol)

            dCol = New DataColumn("LeaveCode")
            dCol.DataType = System.Type.GetType("System.String")
            mdtPaySetting.Columns.Add(dCol)

            dCol = New DataColumn("Leave")
            dCol.DataType = System.Type.GetType("System.String")
            mdtPaySetting.Columns.Add(dCol)

            dCol = New DataColumn("tenureupto")
            dCol.DataType = System.Type.GetType("System.Int32")
            mdtPaySetting.Columns.Add(dCol)

            dCol = New DataColumn("deduction_percentage")
            dCol.DataType = System.Type.GetType("System.Decimal")
            mdtPaySetting.Columns.Add(dCol)

            dCol = New DataColumn("AUD")
            dCol.DataType = System.Type.GetType("System.String")
            mdtPaySetting.Columns.Add(dCol)

            dCol = New DataColumn("isvoid")
            dCol.DataType = System.Type.GetType("System.Boolean")
            mdtPaySetting.Columns.Add(dCol)

            dCol = New DataColumn("voiduserunkid")
            dCol.DataType = System.Type.GetType("System.Int32")
            mdtPaySetting.Columns.Add(dCol)

            dCol = New DataColumn("voiddatetime")
            dCol.DataType = System.Type.GetType("System.DateTime")
            dCol.DefaultValue = DBNull.Value
            mdtPaySetting.Columns.Add(dCol)

            dCol = New DataColumn("voidreason")
            dCol.DataType = System.Type.GetType("System.String")
            mdtPaySetting.Columns.Add(dCol)

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "New", mstrModuleName)
        End Try
    End Sub

#End Region

#Region " Properties "
    ''' <summary>
    ''' Purpose: Get Message from Class 
    ''' Modify By: Pinkal Jariwala
    ''' </summary>
    Public ReadOnly Property _Message() As String
        Get
            Return mstrMessage
        End Get
    End Property

    ''' <summary>
    ''' Purpose: Get or Set Datatable
    ''' Modify By: Sandeep Sharma
    ''' </summary>
    Public Property _dtPaySetting() As DataTable
        Get
            Return mdtPaySetting
        End Get
        Set(ByVal value As DataTable)
            mdtPaySetting = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set leavepayunkid
    ''' Modify By: Pinkal Jariwala
    ''' </summary>
    Public Property _Leavepayunkid(Optional ByVal objDoOperation As clsDataOperation = Nothing) As Integer
        Get
            Return mintLeavepayunkid
        End Get
        Set(ByVal value As Integer)
            mintLeavepayunkid = value
            Call GetData(objDoOperation)
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set leavetypeunkid
    ''' Modify By: Pinkal Jariwala
    ''' </summary>
    Public Property _Leavetypeunkid() As Integer
        Get
            Return mintLeavetypeunkid
        End Get
        Set(ByVal value As Integer)
            mintLeavetypeunkid = Value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set tenureupto
    ''' Modify By: Pinkal Jariwala
    ''' </summary>
    Public Property _Tenureupto() As Integer
        Get
            Return mintTenureupto
        End Get
        Set(ByVal value As Integer)
            mintTenureupto = Value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set deduction_percentage
    ''' Modify By: Pinkal Jariwala
    ''' </summary>
    Public Property _Deduction_Percentage() As Double
        Get
            Return mdblDeduction_Percentage
        End Get
        Set(ByVal value As Double)
            mdblDeduction_Percentage = Value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set userunkid
    ''' Modify By: Pinkal Jariwala
    ''' </summary>
    Public Property _Userunkid() As Integer
        Get
            Return mintUserunkid
        End Get
        Set(ByVal value As Integer)
            mintUserunkid = Value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set isvoid
    ''' Modify By: Pinkal Jariwala
    ''' </summary>
    Public Property _Isvoid() As Boolean
        Get
            Return mblnIsvoid
        End Get
        Set(ByVal value As Boolean)
            mblnIsvoid = Value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set voiddatatime
    ''' Modify By: Pinkal Jariwala
    ''' </summary>
    Public Property _Voiddatatime() As Date
        Get
            Return mdtVoiddatatime
        End Get
        Set(ByVal value As Date)
            mdtVoiddatatime = Value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set voiduserunkid
    ''' Modify By: Pinkal Jariwala
    ''' </summary>
    Public Property _Voiduserunkid() As Integer
        Get
            Return mintVoiduserunkid
        End Get
        Set(ByVal value As Integer)
            mintVoiduserunkid = Value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set voidreason
    ''' Modify By: Pinkal Jariwala
    ''' </summary>
    Public Property _Voidreason() As String
        Get
            Return mstrVoidreason
        End Get
        Set(ByVal value As String)
            mstrVoidreason = Value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set FormName
    ''' Modify By: Pinkal
    ''' </summary>
    Public Property _FormName() As String
        Get
            Return mstrFormName
        End Get
        Set(ByVal value As String)
            mstrFormName = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set ClientIP
    ''' Modify By: Pinkal
    ''' </summary>
    Public Property _ClientIP() As String
        Get
            Return mstrClientIP
        End Get
        Set(ByVal value As String)
            mstrClientIP = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set HostName
    ''' Modify By: Pinkal
    ''' </summary>
    Public Property _HostName() As String
        Get
            Return mstrHostName
        End Get
        Set(ByVal value As String)
            mstrHostName = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set IsFromWeb
    ''' Modify By: Pinkal
    ''' </summary>
    Public Property _IsFromWeb() As Boolean
        Get
            Return mblnIsFromWeb
        End Get
        Set(ByVal value As Boolean)
            mblnIsFromWeb = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set AuditUserId
    ''' Modify By: Pinkal
    ''' </summary>
    Public Property _AuditUserId() As Integer
        Get
            Return mintAuditUserId
        End Get
        Set(ByVal value As Integer)
            mintAuditUserId = value
        End Set
    End Property

#End Region

    ''' <summary>
    ''' Modify By: Pinkal Jariwala
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>
    Public Sub GetData(Optional ByVal objDoOperation As clsDataOperation = Nothing)
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        If objDoOperation Is Nothing Then
            objDataOperation = New clsDataOperation
        Else
            objDataOperation = objDoOperation
        End If


        Try
            strQ = "SELECT " & _
                      "  leavepayunkid " & _
                      ", leavetypeunkid " & _
                      ", tenureupto " & _
                      ", deduction_percentage " & _
                      ", userunkid " & _
                      ", isvoid " & _
                      ", voiddatetime " & _
                      ", voiduserunkid " & _
                      ", voidreason " & _
                      " FROM lvleavepaysettings " & _
                      " WHERE leavepayunkid = @leavepayunkid "

            objDataOperation.ClearParameters()
            objDataOperation.AddParameter("@leavepayunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintLeavepayunkid.ToString)
            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            For Each dtRow As DataRow In dsList.Tables(0).Rows
                mintLeavepayunkid = CInt(dtRow.Item("leavepayunkid"))
                mintLeavetypeunkid = CInt(dtRow.Item("leavetypeunkid"))
                mintTenureupto = CInt(dtRow.Item("tenureupto"))
                mdblDeduction_Percentage = CDbl(dtRow.Item("deduction_percentage"))
                mintUserunkid = CInt(dtRow.Item("userunkid"))
                mblnIsvoid = CBool(dtRow.Item("isvoid"))
                If IsDBNull(dtRow.Item("voiddatetime")) = False Then
                    mdtVoiddatatime = dtRow.Item("voiddatetime")
                End If
                mintVoiduserunkid = CInt(dtRow.Item("voiduserunkid"))
                mstrVoidreason = dtRow.Item("voidreason").ToString
                Exit For
            Next
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: GetData; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            If objDoOperation Is Nothing Then objDataOperation = Nothing
        End Try
    End Sub

    ''' <summary>
    ''' Modify By: Pinkal Jariwala
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>
    Public Sub GetList(ByVal strTableName As String, ByVal xLeaveTypeId As Integer, Optional ByVal blnOnlyActive As Boolean = True, Optional ByVal objDoOperation As clsDataOperation = Nothing)
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        If objDoOperation Is Nothing Then
            objDataOperation = New clsDataOperation
        Else
            objDataOperation = objDoOperation
        End If

        Try
            strQ = "SELECT " & _
                      "  lvleavepaysettings.leavepayunkid " & _
                      ", ISNULL(lvleavepaysettings.leavetypeunkid,0) AS leavetypeunkid " & _
                      ", ISNULL(lvleavetype_master.leavetypecode,'') AS LeaveCode " & _
                      ", ISNULL(lvleavetype_master.leavename,'') AS Leave " & _
                      ", ISNULL(lvleavepaysettings.tenureupto,0) AS tenureupto  " & _
                      ", ISNULL(lvleavepaysettings.deduction_percentage,0.00) AS deduction_percentage  " & _
                      ", '' AS AUD " & _
                      ", lvleavepaysettings.userunkid " & _
                      ", lvleavepaysettings.isvoid " & _
                      ", lvleavepaysettings.voiddatetime " & _
                      ", lvleavepaysettings.voiduserunkid " & _
                      ", lvleavepaysettings.voidreason " & _
                      " FROM lvleavepaysettings " & _
                      " LEFT JOIN lvleavetype_master ON lvleavetype_master.leavetypeunkid = lvleavepaysettings.leavetypeunkid AND lvleavetype_master.isactive = 1 "

            If blnOnlyActive Then
                strQ &= " WHERE lvleavepaysettings.isvoid = 0 "
            End If

            strQ &= " AND ISNULL(lvleavepaysettings.leavetypeunkid,0) = @leavetypeunkid "

            objDataOperation.ClearParameters()
            objDataOperation.AddParameter("@leavetypeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, xLeaveTypeId)
            dsList = objDataOperation.ExecQuery(strQ, strTableName)

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            mdtPaySetting.Rows.Clear()

            For Each dRow As DataRow In dsList.Tables(0).Rows
                mdtPaySetting.ImportRow(dRow)
            Next


        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: GetList; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            If objDoOperation Is Nothing Then objDataOperation = Nothing
        End Try
    End Sub

    ''' <summary>
    ''' Modify By: Pinkal Jariwala
    ''' </summary>
    ''' <returns>Boolean</returns>
    ''' <purpose> INSERT INTO Database Table (lvleavepaysettings) </purpose>
    Public Function InsertUpdateDelete_LeavePaySetting(ByVal moldLeavePaySetting As DataTable, ByVal objDataOperation As clsDataOperation) As Boolean
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception
        Dim strPayUnkIDs As String = ""
        Try

            If mdtPaySetting Is Nothing OrElse mdtPaySetting.Rows.Count <= 0 Then Return True

            For Each drRow As DataRow In mdtPaySetting.Rows
                mintLeavepayunkid = CInt(drRow("leavepayunkid"))
                mintTenureupto = CInt(drRow("tenureupto"))
                mdblDeduction_Percentage = CDbl(drRow("deduction_percentage"))

                If mintLeavepayunkid <= 0 Then
                    If Insert(objDataOperation) = False Then
                        exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                        Throw exForce
                    End If
                ElseIf drRow.Item("AUD").ToString() = "U" AndAlso mintLeavepayunkid > 0 Then
                    If Update(objDataOperation) = False Then
                        exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                        Throw exForce
                    End If
                End If
                If strPayUnkIDs.Trim = "" Then
                    strPayUnkIDs = mintLeavepayunkid.ToString
                Else
                    strPayUnkIDs &= "," & mintLeavepayunkid.ToString
                End If
            Next

            If moldLeavePaySetting IsNot Nothing AndAlso moldLeavePaySetting.Rows.Count > 0 Then
                Dim dRow() As DataRow
                If strPayUnkIDs.Trim <> "" Then
                    dRow = moldLeavePaySetting.Select("leavepayunkid NOT IN (" & strPayUnkIDs & ") ")
                Else
                    dRow = moldLeavePaySetting.Select()
                End If

                For Each drRow In dRow
                    mintLeavepayunkid = CInt(drRow("leavepayunkid"))
                    If Delete(objDataOperation) = False Then
                        Return False
                    End If
                Next
            End If

            Return True
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: InsertUpdateDelete_LeavePaySetting; Module Name: " & mstrModuleName)
            Return False
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
        End Try
    End Function

    ''' <summary>
    ''' Modify By: Pinkal Jariwala
    ''' </summary>
    ''' <returns>Boolean</returns>
    ''' <purpose> INSERT INTO Database Table (lvleavepaysettings) </purpose>
    ''' 
    Public Function Insert(ByVal objDataOperation As clsDataOperation) As Boolean
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception
        Try

            strQ = "INSERT INTO lvleavepaysettings ( " & _
                            "  leavetypeunkid " & _
                            ", tenureupto " & _
                            ", deduction_percentage " & _
                            ", userunkid " & _
                            ", isvoid " & _
                            ", voiddatetime " & _
                            ", voiduserunkid " & _
                            ", voidreason" & _
                          ") VALUES (" & _
                            "  @leavetypeunkid " & _
                            ", @tenureupto " & _
                            ", @deduction_percentage " & _
                            ", @userunkid " & _
                            ", @isvoid " & _
                            ", @voiddatetime " & _
                            ", @voiduserunkid " & _
                            ", @voidreason" & _
                          "); SELECT @@identity"

            objDataOperation.ClearParameters()
            objDataOperation.AddParameter("@leavetypeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintLeavetypeunkid)
            objDataOperation.AddParameter("@tenureupto", SqlDbType.Int, eZeeDataType.INT_SIZE, mintTenureupto)
            objDataOperation.AddParameter("@deduction_percentage", SqlDbType.Money, eZeeDataType.MONEY_SIZE, mdblDeduction_Percentage)
            objDataOperation.AddParameter("@userunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintUserunkid.ToString)
            objDataOperation.AddParameter("@isvoid", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsvoid)
            If IsDBNull(mdtVoiddatatime) = False AndAlso mdtVoiddatatime <> Nothing Then
                objDataOperation.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, mdtVoiddatatime)
            Else
                objDataOperation.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, DBNull.Value)
            End If
            objDataOperation.AddParameter("@voiduserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintVoiduserunkid)
            objDataOperation.AddParameter("@voidreason", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrVoidreason)

            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            mintLeavepayunkid = dsList.Tables(0).Rows(0).Item(0)

            If InsertAuditTrailForLeavePaySetting(objDataOperation, enAuditType.ADD) = False Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If
            Return True
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: Insert; Module Name: " & mstrModuleName)
        End Try
    End Function


    ''' <summary>
    ''' Modify By: Pinkal Jariwala
    ''' </summary>
    ''' <returns>Boolean</returns>
    ''' <purpose> INSERT INTO Database Table (lvleavepaysettings) </purpose>
    ''' 
    Public Function Update(ByVal objDataOperation As clsDataOperation) As Boolean
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception
        Try

            strQ = "UPDATE lvleavepaysettings SET " & _
                      "  tenureupto = @tenureupto" & _
                      ", deduction_percentage = @deduction_percentage" & _
                      " WHERE isvoid = 0 AND leavetypeunkid = @leavetypeunkid AND leavepayunkid = @leavepayunkid "

            objDataOperation.ClearParameters()
            objDataOperation.AddParameter("@leavepayunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintLeavepayunkid)
            objDataOperation.AddParameter("@leavetypeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintLeavetypeunkid)
            objDataOperation.AddParameter("@tenureupto", SqlDbType.Int, eZeeDataType.INT_SIZE, mintTenureupto)
            objDataOperation.AddParameter("@deduction_percentage", SqlDbType.Money, eZeeDataType.MONEY_SIZE, mdblDeduction_Percentage)
            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            If InsertAuditTrailForLeavePaySetting(objDataOperation, enAuditType.EDIT) = False Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If
            Return True
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: Update; Module Name: " & mstrModuleName)
        End Try
    End Function

    ''' <summary>
    ''' Modify By: Pinkal Jariwala
    ''' </summary>
    ''' <returns>Boolean</returns>
    ''' <purpose> Delete Database Table (lvleavepaysettings) </purpose>
    Public Function Delete(ByVal objDataOperation As clsDataOperation) As Boolean
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception
        Try
            strQ = " UPDATE lvleavepaysettings SET " & _
                      "  isvoid = @isvoid" & _
                      ", voiduserunkid = @voiduserunkid" & _
                      ", voiddatetime = GETDATE() " & _
                      ", voidreason = @voidreason " & _
                      " WHERE isvoid = 0 AND leavetypeunkid = @leavetypeunkid AND  leavepayunkid = @leavepayunkid "

            objDataOperation.ClearParameters()
            objDataOperation.AddParameter("@leavepayunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintLeavepayunkid)
            objDataOperation.AddParameter("@leavetypeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintLeavetypeunkid)
            objDataOperation.AddParameter("@isvoid", SqlDbType.Bit, eZeeDataType.BIT_SIZE, True)
            objDataOperation.AddParameter("@voiduserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintUserunkid)
            objDataOperation.AddParameter("@voidreason", SqlDbType.NVarChar, eZeeDataType.DESC_SIZE, mstrVoidreason)

            objDataOperation.ExecNonQuery(strQ)

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            If InsertAuditTrailForLeavePaySetting(objDataOperation, enAuditType.DELETE) = False Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            Return True
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: Delete; Module Name: " & mstrModuleName)
            Return False
        End Try
    End Function

    ''' <summary>
    ''' Modify By: Pinkal Jariwala
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>
    Public Function isUsed(ByVal intUnkid As Integer) As Boolean
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        objDataOperation = New clsDataOperation

        Try
            StrQ = "<Query>"

            objDataOperation.AddParameter("@leavepayunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intUnkid)

            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            Return dsList.tables(0).rows.count > 0
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: isUsed; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
    End Function

    ''' <summary>
    ''' Modify By: Pinkal Jariwala
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>
    Public Function isExist(ByVal strCode As String, ByVal strName As String, Optional ByVal intUnkid As Integer = -1) As Boolean
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        objDataOperation = New clsDataOperation

        Try
            strQ = "SELECT " & _
                      "  leavepayunkid " & _
                      ", leavetypeunkid " & _
                      ", tenureupto " & _
                      ", deduction_percentage " & _
                      ", userunkid " & _
                      ", isvoid " & _
                      ", voiddatetime " & _
                      ", voiduserunkid " & _
                      ", voidreason " & _
                      " FROM lvleavepaysettings " & _
                      " WHERE name = @name " & _
                      " AND code = @code "

            If intUnkid > 0 Then
                strQ &= " AND leavepayunkid <> @leavepayunkid"
            End If

            objDataOperation.AddParameter("@code", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, strCode)
            objDataOperation.AddParameter("@name", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, strName)
            objDataOperation.AddParameter("@leavepayunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intUnkid)

            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            Return dsList.tables(0).rows.count > 0
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: isExist; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
    End Function

    ''' <summary>
    ''' Modify By: Pinkal Jariwala
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>
    Public Function InsertAuditTrailForLeavePaySetting(ByVal objDataOperation As clsDataOperation, ByVal intAuditType As Integer) As Boolean
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        Try
            strQ = "INSERT INTO atlvleavepaysettings ( " & _
                     "  leavepayunkid " & _
                     ", leavetypeunkid " & _
                     ", tenureupto " & _
                     ", deduction_percentage " & _
                     ", audittype " & _
                     ", audituserunkid " & _
                     ", auditdatetime " & _
                     ", ip " & _
                     ", machine_name" & _
                     ", form_name " & _
                     ", isweb " & _
                   ") VALUES (" & _
                     "  @leavepayunkid " & _
                     ", @leavetypeunkid " & _
                     ", @tenureupto " & _
                     ", @deduction_percentage " & _
                     ", @audittype " & _
                     ", @audituserunkid " & _
                     ", GETDATE() " & _
                     ", @ip " & _
                     ", @machine_name" & _
                     ", @form_name " & _
                     ", @isweb " & _
                   "); SELECT @@identity"

            objDataOperation.ClearParameters()
            objDataOperation.AddParameter("@leavepayunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintLeavepayunkid)
            objDataOperation.AddParameter("@leavetypeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintLeavetypeunkid)
            objDataOperation.AddParameter("@tenureupto", SqlDbType.Int, eZeeDataType.INT_SIZE, mintTenureupto)
            objDataOperation.AddParameter("@deduction_percentage", SqlDbType.Money, eZeeDataType.MONEY_SIZE, mdblDeduction_Percentage)
            objDataOperation.AddParameter("@audittype", SqlDbType.Int, eZeeDataType.INT_SIZE, intAuditType.ToString)
            objDataOperation.AddParameter("@audituserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintAuditUserId)
            objDataOperation.AddParameter("@ip", SqlDbType.NVarChar, eZeeDataType.IP_SIZE, mstrClientIP)
            objDataOperation.AddParameter("@machine_name", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrHostName)
            objDataOperation.AddParameter("@form_name", SqlDbType.NVarChar, 500, mstrFormName)
            objDataOperation.AddParameter("@isweb", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsFromWeb)

            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If
            Return True
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: InsertAuditTrailForLeavePaySetting; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
        End Try
    End Function

End Class