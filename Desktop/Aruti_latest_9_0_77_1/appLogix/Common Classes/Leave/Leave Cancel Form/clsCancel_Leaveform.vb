﻿'************************************************************************************************************************************
'Class Name : clsCancel_Leaveform.vb
'Purpose    :
'Date       :11/11/2011
'Written By :Pinkal
'Modified   :
'************************************************************************************************************************************

Imports eZeeCommonLib
''' <summary>
''' Purpose: 
''' Developer: Pinkal
''' </summary>
''' 

Public Class clsCancel_Leaveform

    Private Shared ReadOnly mstrModuleName As String = "clsCancel_Leaveform"
    Dim objDataOperation As clsDataOperation
    Dim mstrMessage As String = ""

#Region " Private variables "
    Private mintCancelunkid As Integer
    Private mintFormunkid As Integer
    Private mstrCancelformno As String = String.Empty
    Private mdtCancel_Leavedate As Date
    Private mstrCancel_Reason As String = String.Empty
    Private mintCanceluserunkid As Integer
    Private mdtCancel_Trandate As Date
    Private mblnIsvoid As Boolean
    Private mintVoiduserunkid As Integer
    Private mdtVoiddatetime As Date


    'Pinkal (12-Oct-2012) -- Start
    'Enhancement : TRA Changes
    Private mdclDayFraction As Decimal = 0
    Private mintEmployeeunkid As Integer = -1
    'Pinkal (12-Oct-2012) -- End

    'Pinkal (06-Mar-2014) -- Start
    'Enhancement : TRA Changes
    Private mintClaimMasterID As Integer = -1
    Private mstrCancelExpenseRemak As String = ""
    Private mdtCancelExpense As DataTable = Nothing
    Private mblnPaymentApprovalwithLeaveApproval As Boolean = False
    'Pinkal (06-Mar-2014) -- End


    'Pinkal (28-Nov-2017) -- Start
    'Enhancement - Cancel form document attachment  - (RefNo: 132) On leave cancel process there should be a place to attach document for a reference and this feature (cancel leave form) should be included in Aruti self service .
    Private mdtAttachment As DataTable = Nothing
    'Pinkal (28-Nov-2017) -- End


    'Pinkal (01-Oct-2018) -- Start
    'Enhancement - Leave Enhancement for NMB.
    Private mblnSkipForApproverFlow As Boolean = False
    Private mstrWebFrmName As String = ""
    Private mstrWebClientIP As String = ""
    Private mstrWebHostName As String = ""
    'Pinkal (01-Oct-2018) -- End


#End Region

#Region " Properties "

    'S.SANDEEP [28-May-2018] -- START 
    'ISSUE/ENHANCEMENT : {Audit Trails} 
    Private mstrFormName As String = String.Empty
    Public WriteOnly Property _FormName() As String
        Set(ByVal value As String)
            mstrFormName = value
        End Set
    End Property

    Private mstrClientIP As String = ""
    Public WriteOnly Property _ClientIP() As String
        Set(ByVal value As String)
            mstrClientIP = value
        End Set
    End Property

    Private mstrHostName As String = ""
    Public WriteOnly Property _HostName() As String
        Set(ByVal value As String)
            mstrHostName = value
        End Set
    End Property

    Private mblnIsWeb As Boolean = False
    Public WriteOnly Property _FromWeb() As Boolean
        Set(ByVal value As Boolean)
            mblnIsWeb = value
        End Set
    End Property

    Private mintAuditUserId As Integer = 0
    Public WriteOnly Property _AuditUserId() As Integer
        Set(ByVal value As Integer)
            mintAuditUserId = value
        End Set
    End Property

    Private mintCompanyUnkid As Integer = 0
    Public WriteOnly Property _CompanyUnkid() As Integer
        Set(ByVal value As Integer)
            mintCompanyUnkid = value
        End Set
    End Property

    Private mdtAuditDate As DateTime = Now
    Public WriteOnly Property _AuditDate() As DateTime
        Set(ByVal value As DateTime)
            mdtAuditDate = value
        End Set
    End Property

    Private xDataOpr As clsDataOperation = Nothing
    Public Property _xDataOpr() As clsDataOperation
        Get
            Return xDataOpr
        End Get
        Set(ByVal value As clsDataOperation)
            xDataOpr = value
        End Set
    End Property

    Private mintLoginEmployeeunkid As Integer = 0
    Public Property _LoginEmployeeunkid() As Integer
        Get
            Return mintLoginEmployeeunkid
        End Get
        Set(ByVal value As Integer)
            mintLoginEmployeeunkid = value
        End Set
    End Property

    Private mintVoidlogingemployeeunkid As Integer = 0
    Public Property _Voidloginemployeeunkid() As Integer
        Get
            Return mintVoidlogingemployeeunkid
        End Get
        Set(ByVal value As Integer)
            mintVoidlogingemployeeunkid = value
        End Set
    End Property
    'S.SANDEEP [28-May-2018] -- END 

    ''' <summary>
    ''' Purpose: Get Message from Class 
    ''' Modify By: Pinkal
    ''' </summary>
    Public ReadOnly Property _Message() As String
        Get
            Return mstrMessage
        End Get
    End Property

    ''' <summary>
    ''' Purpose: Get or Set cancelunkid
    ''' Modify By: Pinkal
    ''' </summary>
    Public Property _Cancelunkid() As Integer
        Get
            Return mintCancelunkid
        End Get
        Set(ByVal value As Integer)
            mintCancelunkid = value
            Call GetData()
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set formunkid
    ''' Modify By: Pinkal
    ''' </summary>
    Public Property _Formunkid() As Integer
        Get
            Return mintFormunkid
        End Get
        Set(ByVal value As Integer)
            mintFormunkid = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set cancelformno
    ''' Modify By: Pinkal
    ''' </summary>
    Public Property _Cancelformno() As String
        Get
            Return mstrCancelformno
        End Get
        Set(ByVal value As String)
            mstrCancelformno = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set cancel_leavedate
    ''' Modify By: Pinkal
    ''' </summary>
    Public Property _Cancel_Leavedate() As Date
        Get
            Return mdtCancel_Leavedate
        End Get
        Set(ByVal value As Date)
            mdtCancel_Leavedate = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set cancel_reason
    ''' Modify By: Pinkal
    ''' </summary>
    Public Property _Cancel_Reason() As String
        Get
            Return mstrCancel_Reason
        End Get
        Set(ByVal value As String)
            mstrCancel_Reason = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set canceluserunkid
    ''' Modify By: Pinkal
    ''' </summary>
    Public Property _Canceluserunkid() As Integer
        Get
            Return mintCanceluserunkid
        End Get
        Set(ByVal value As Integer)
            mintCanceluserunkid = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set cancel_trandate
    ''' Modify By: Pinkal
    ''' </summary>
    Public Property _Cancel_Trandate() As Date
        Get
            Return mdtCancel_Trandate
        End Get
        Set(ByVal value As Date)
            mdtCancel_Trandate = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set isvoid
    ''' Modify By: Pinkal
    ''' </summary>
    Public Property _Isvoid() As Boolean
        Get
            Return mblnIsvoid
        End Get
        Set(ByVal value As Boolean)
            mblnIsvoid = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set voiduserunkid
    ''' Modify By: Pinkal
    ''' </summary>
    Public Property _Voiduserunkid() As Integer
        Get
            Return mintVoiduserunkid
        End Get
        Set(ByVal value As Integer)
            mintVoiduserunkid = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set voiddatetime
    ''' Modify By: Pinkal
    ''' </summary>
    Public Property _Voiddatetime() As Date
        Get
            Return mdtVoiddatetime
        End Get
        Set(ByVal value As Date)
            mdtVoiddatetime = value
        End Set
    End Property


    'Pinkal (12-Oct-2012) -- Start
    'Enhancement : TRA Changes

    ''' <summary>
    ''' Purpose: Get or Set dayfraction
    ''' Modify By: Pinkal
    ''' </summary>
    Public Property _DayFraction() As Decimal
        Get
            Return mdclDayFraction
        End Get
        Set(ByVal value As Decimal)
            mdclDayFraction = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set employeeunkid
    ''' Modify By: Pinkal
    ''' </summary>
    Public Property _Employeeunkid() As Integer
        Get
            Return mintEmployeeunkid
        End Get
        Set(ByVal value As Integer)
            mintEmployeeunkid = value
        End Set
    End Property

    'Pinkal (06-Mar-2014) -- Start
    'Enhancement : TRA Changes

    ''' <summary>
    ''' Purpose: Get or Set ClaimMstId
    ''' Modify By: Pinkal
    ''' </summary>
    Public Property _ClaimMstId() As Integer
        Get
            Return mintClaimMasterID
        End Get
        Set(ByVal value As Integer)
            mintClaimMasterID = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set CancelExpenseRemark
    ''' Modify By: Pinkal
    ''' </summary>
    Public Property _CancelExpenseRemark() As String
        Get
            Return mstrCancelExpenseRemak
        End Get
        Set(ByVal value As String)
            mstrCancelExpenseRemak = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set CancelExpenseRemark
    ''' Modify By: Pinkal
    ''' </summary>
    Public Property _dtCancelExpense() As DataTable
        Get
            Return mdtCancelExpense
        End Get
        Set(ByVal value As DataTable)
            mdtCancelExpense = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Set IsPaymentApprovalwithLeaveApproval
    ''' Modify By: Pinkal Jariwala
    ''' </summary>
    Public Property _IsPaymentApprovalwithLeaveApproval() As Boolean
        Get
            Return mblnPaymentApprovalwithLeaveApproval
        End Get
        Set(ByVal value As Boolean)
            mblnPaymentApprovalwithLeaveApproval = value
        End Set
    End Property

    'Pinkal (06-Mar-2014) -- End


    'Pinkal (28-Nov-2017) -- Start
    'Enhancement - Cancel form document attachment  - (RefNo: 132) On leave cancel process there should be a place to attach document for a reference and this feature (cancel leave form) should be included in Aruti self service .

    ''' <summary>
    ''' Purpose: Get or Set _dtAttachment
    ''' Modify By: Pinkal
    ''' </summary>
    Public Property _dtAttachment() As DataTable
        Get
            Return mdtAttachment
        End Get
        Set(ByVal value As DataTable)
            mdtAttachment = value
        End Set
    End Property

    'Pinkal (28-Nov-2017) -- End


    'Pinkal (01-Oct-2018) -- Start
    'Enhancement - Leave Enhancement for NMB.

    ''' <summary>
    ''' Purpose: Get or Set WebFrmName
    ''' Modify By: Pinkal
    ''' </summary>
    Public Property _SkipForApproverFlow() As Boolean
        Get
            Return mblnSkipForApproverFlow
        End Get
        Set(ByVal value As Boolean)
            mblnSkipForApproverFlow = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set WebFrmName
    ''' Modify By: Pinkal
    ''' </summary>
    Public WriteOnly Property _WebFrmName() As String
        Set(ByVal value As String)
            mstrWebFrmName = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set WebClientIP
    ''' Modify By: Pinkal
    ''' </summary>
    Public WriteOnly Property _WebClientIP() As String
        Set(ByVal value As String)
            mstrWebClientIP = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set WebHostName
    ''' Modify By: Pinkal
    ''' </summary>
    Public WriteOnly Property _WebHostName() As String
        Set(ByVal value As String)
            mstrWebHostName = value
        End Set
    End Property

    'Pinkal (01-Oct-2018) -- End


#End Region

    ''' <summary>
    ''' Modify By: Pinkal
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>
    Public Sub GetData()
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        objDataOperation = New clsDataOperation

        Try

            strQ = "SELECT " & _
              "  cancelunkid " & _
              ", formunkid " & _
              ", cancelformno " & _
              ", cancel_leavedate " & _
              ", cancel_reason " & _
              ", canceluserunkid " & _
              ", cancel_trandate " & _
              ", isvoid " & _
              ", voiduserunkid " & _
              ", voiddatetime " & _
                 ", ISNULL(employeeunkid,0) As employeeunkid " & _
                 ", ISNULL(dayfraction,0.00) As dayfraction " & _
             "FROM lvcancelform " & _
             "WHERE cancelunkid = @cancelunkid "

            objDataOperation.AddParameter("@cancelunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintCancelunkid.ToString)

            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            For Each dtRow As DataRow In dsList.Tables(0).Rows
                mintCancelunkid = CInt(dtRow.Item("cancelunkid"))
                mintFormunkid = CInt(dtRow.Item("formunkid"))
                mstrCancelformno = dtRow.Item("cancelformno").ToString
                mdtCancel_Leavedate = dtRow.Item("cancel_leavedate")
                mstrCancel_Reason = dtRow.Item("cancel_reason").ToString
                mintCanceluserunkid = CInt(dtRow.Item("canceluserunkid"))
                mdtCancel_Trandate = dtRow.Item("cancel_trandate")
                mblnIsvoid = CBool(dtRow.Item("isvoid"))
                mintVoiduserunkid = CInt(dtRow.Item("voiduserunkid"))


                'Pinkal (11-Apr-2015) -- Start
                'Enhancement - VALIDATION ON EMPLOYEE MASTER OF LEAVE MODULE WHEN CHANGING APPOINTMENT DATE FOR TANAPA
                If Not IsDBNull(dtRow.Item("voiddatetime")) Then
                    mdtVoiddatetime = dtRow.Item("voiddatetime")
                End If
                'Pinkal (11-Apr-2015) -- End

                mintEmployeeunkid = CInt(dtRow.Item("employeeunkid"))
                mdclDayFraction = CDec(dtRow.Item("dayfraction"))

                Exit For
            Next
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: GetData; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
    End Sub

    ''' <summary>
    ''' Modify By: Pinkal
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>
    Public Function GetList(ByVal strTableName As String, Optional ByVal intFormunkid As Integer = -1, Optional ByVal mstrCancelFormNo As String = "", Optional ByVal blnOnlyActive As Boolean = True _
                                    , Optional ByVal mstrFilter As String = "", Optional ByVal xDataOpr As clsDataOperation = Nothing) As DataSet
        'Pinkal (18-Mar-2021) --NMB Enhancmenet : AD Enhancement for NMB.[Optional ByVal mstrFilter As String = "", Optional ByVal xDataOpr As clsDataOperation = Nothing]

        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        'Pinkal (18-Mar-2021) -- Start 
        'NMB Enhancmenet : AD Enhancement for NMB.
        If xDataOpr IsNot Nothing Then
            objDataOperation = xDataOpr
        Else
        objDataOperation = New clsDataOperation
        End If
        'Pinkal (18-Mar-2021) -- End

        Try

            strQ = "SELECT " & _
                      "  cancelunkid " & _
                      ", formunkid " & _
                      ", cancelformno " & _
                      ", convert(char(8),cancel_leavedate,112)  cancel_leavedate " & _
                      ", cancel_reason " & _
                      ", canceluserunkid " & _
                      ", cancel_trandate " & _
                      ", isvoid " & _
                      ", voiduserunkid " & _
                      ", voiddatetime " & _
                    ", ISNULL(employeeunkid,0) As employeeunkid " & _
                    ", ISNULL(dayfraction,0.00) As dayfraction " & _
                     " FROM lvcancelform " & _
                     " WHERE 1 = 1 "

            If blnOnlyActive Then
                strQ &= " AND isnull(isvoid,0) = 0 "
            End If

            objDataOperation.ClearParameters()

            If intFormunkid > 0 Then
                strQ &= " AND formunkid = @formunkid "
                objDataOperation.AddParameter("@formunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intFormunkid)
            End If

            If mstrCancelFormNo.Trim.Length > 0 Then
                strQ &= " AND cancelformno = @cancelformno "
                objDataOperation.AddParameter("@cancelformno", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrCancelFormNo)
            End If

            If mstrFilter.Trim.Length > 0 Then
                strQ &= " AND " & mstrFilter
            End If

            dsList = objDataOperation.ExecQuery(strQ, strTableName)

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: GetList; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
        Return dsList
    End Function

    ''' <summary>
    ''' Modify By: Pinkal
    ''' </summary>
    ''' <returns>Boolean</returns>
    ''' <purpose> INSERT INTO Database Table (lvcancelform) </purpose>
    Public Function Insert(ByVal xCompanyUnkid As Integer, ByVal mstrLeaveDates As String, ByVal intEmploeeyeunkid As Integer _
                                  , ByVal intLeaveTypeunkid As Integer, ByVal mintTotalLeaveDays As Integer _
                                  , ByVal mintTotalCanceldays As Integer, ByVal dtCurrentDateAndTime As DateTime _
                                  , Optional ByVal intYearId As Integer = -1, Optional ByVal blnLeaveApproverForLeaveType As String = "" _
                                  , Optional ByVal intLeaveBalanceSetting As Integer = -1, Optional ByVal intApproverId As Integer = -1 _
                                  , Optional ByVal blnSkipEmployeeMovementApprovalFlow As Boolean = True _
                                  , Optional ByVal blnCreateADUserFromEmpMst As Boolean = False _
                                  , Optional ByVal xDatabaseName As String = "" _
                                  , Optional ByVal xUserAccessModeSetting As String = "" _
                                  , Optional ByVal dtEmployeeAsOnDate As Date = Nothing _
                                  , Optional ByVal eMode As enLogin_Mode = enLogin_Mode.DESKTOP _
                                  , Optional ByVal xUsername As String = "" _
                                  ) As Boolean
        'Sohail (21 Oct 2019) -- [blnSkipEmployeeMovementApprovalFlow, blnCreateADUserFromEmpMst, xDatabaseName, xUserAccessModeSetting, dtEmployeeAsOnDate, eMode, xUsername]
        'Pinkal (03-May-2019) -- 'Enhancement - Working on Leave UAT Changes for NMB.[ByVal xCompanyUnkid As Integer]


        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception


        'Pinkal (02-Dec-2015) -- Start
        'Enhancement - Solving Leave bug in Self Service.
        Dim objLvForm As New clsleaveform
        'Pinkal (02-Dec-2015) -- End

        Dim objDataOperation As New clsDataOperation
        objDataOperation.BindTransaction()
        objDataOperation.ClearParameters()



        'Pinkal (03-May-2019) -- Start
        'Enhancement - Working on Leave UAT Changes for NMB.
        Dim mintLvCancelFormNoType As Integer = 0
        Dim mstrLvCancelFormNoPrefix As String = ""
        Dim mblnIsCancelFormGenerated As Boolean = False
        Dim objConfig As New clsConfigOptions
        mintLvCancelFormNoType = objConfig.GetKeyValue(xCompanyUnkid, "LeaveCancelFormNoType")
        mstrLvCancelFormNoPrefix = objConfig.GetKeyValue(xCompanyUnkid, "LeaveCancelFormNoPrifix")
        If mintLvCancelFormNoType = 0 Then
        If isExist(mstrCancelformno, -1, objDataOperation) Then
            mstrMessage = Language.getMessage(mstrModuleName, 1, "This Cancel Form No is already defined. Please define new Cancel Form No.")
            objDataOperation.ReleaseTransaction(False)
            Return False
        End If
        End If
        objConfig = Nothing
        'Pinkal (03-May-2019) -- End


        If intLeaveBalanceSetting <= 0 Then
            intLeaveBalanceSetting = ConfigParameter._Object._LeaveBalanceSetting
        End If

        If blnLeaveApproverForLeaveType.Trim = "" Then
            blnLeaveApproverForLeaveType = ConfigParameter._Object._IsLeaveApprover_ForLeaveType
        End If

        Try

            If mstrLeaveDates.Trim.Length <= 0 Then Return False

            strQ = "INSERT INTO lvcancelform ( " & _
                      "  formunkid " & _
                      ", cancelformno " & _
                      ", cancel_leavedate " & _
                      ", cancel_reason " & _
                      ", canceluserunkid " & _
                      ", cancel_trandate " & _
                      ", isvoid " & _
                      ", voiduserunkid " & _
                      ", voiddatetime" & _
                   ", employeeunkid " & _
                   ", dayfraction " & _
                    ") VALUES (" & _
                      "  @formunkid " & _
                      ", @cancelformno " & _
                      ", @cancel_leavedate " & _
                      ", @cancel_reason " & _
                      ", @canceluserunkid " & _
                      ", @cancel_trandate " & _
                      ", @isvoid " & _
                      ", @voiduserunkid " & _
                      ", @voiddatetime" & _
                   ", @employeeunkid " & _
                   ", @dayfraction " & _
                    "); SELECT @@identity"


            Dim arStrLeavedate As String() = mstrLeaveDates.Split(",")

            If arStrLeavedate.Length > 0 Then

                Dim mdclFraction As Decimal = 0

                For i As Integer = 0 To arStrLeavedate.Length - 1
                    Dim objFraction As New clsleaveday_fraction

                    'Pinkal (01-Oct-2018) -- Start
                    'Enhancement - Leave Enhancement for NMB.

                    If mblnSkipForApproverFlow Then
                        mdclDayFraction = objFraction.GetEmpLeaveDayFractionForSkipApproverFlow(arStrLeavedate(i), mintFormunkid, intEmploeeyeunkid, objDataOperation)
                    Else
                        mdclDayFraction = objFraction.GetEmployeeLeaveDay_Fraction(arStrLeavedate(i), mintFormunkid, intEmploeeyeunkid, blnLeaveApproverForLeaveType, intApproverId, objDataOperation)
                    End If
                    objFraction = Nothing
                    'Pinkal (01-Oct-2018) -- End


                    objDataOperation.ClearParameters()
                    objDataOperation.AddParameter("@formunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintFormunkid.ToString)
                    objDataOperation.AddParameter("@cancelformno", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrCancelformno.ToString)
                    objDataOperation.AddParameter("@cancel_leavedate", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, eZeeDate.convertDate(arStrLeavedate(i)))
                    objDataOperation.AddParameter("@cancel_reason", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrCancel_Reason.ToString)
                    objDataOperation.AddParameter("@canceluserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintCanceluserunkid.ToString)
                    objDataOperation.AddParameter("@cancel_trandate", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, mdtCancel_Trandate)
                    objDataOperation.AddParameter("@isvoid", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsvoid.ToString)
                    objDataOperation.AddParameter("@voiduserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintVoiduserunkid.ToString)
                    objDataOperation.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, IIf(mdtVoiddatetime <> Nothing, mdtVoiddatetime, DBNull.Value))
                    objDataOperation.AddParameter("@dayfraction", SqlDbType.Decimal, eZeeDataType.DECIMAL_SIZE, mdclDayFraction)
                    objDataOperation.AddParameter("@employeeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intEmploeeyeunkid)


                    mdclFraction += mdclDayFraction
                    dsList = objDataOperation.ExecQuery(strQ, "List")

                    If objDataOperation.ErrorMessage <> "" Then
                        exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                        Throw exForce
                    End If

                    mintCancelunkid = dsList.Tables(0).Rows(0).Item(0)

                    'Pinkal (03-May-2019) -- Start
                    'Enhancement - Working on Leave UAT Changes for NMB.
                    If mblnIsCancelFormGenerated = False AndAlso mintLvCancelFormNoType = 1 Then
                        If Set_AutoNumber(objDataOperation, mintCancelunkid, "lvcancelform", "cancelformno", "cancelunkid", "NextLeaveCancelFormNo", mstrLvCancelFormNoPrefix, xCompanyUnkid) = False Then
                            If objDataOperation.ErrorMessage <> "" Then
                                objDataOperation.ReleaseTransaction(False)
                                Return False
                            End If
                        End If
                        If Get_Saved_Number(objDataOperation, mintCancelunkid, "lvcancelform", "cancelformno", "cancelunkid", mstrCancelformno) = False Then
                            If objDataOperation.ErrorMessage <> "" Then
                                objDataOperation.ReleaseTransaction(False)
                                Return False
                            End If
                        End If
                        mblnIsCancelFormGenerated = True
                    End If
                    'Pinkal (03-May-2019) -- End

                    'S.SANDEEP [28-May-2018] -- START
                    'ISSUE/ENHANCEMENT : {Audit Trails} 
                    Dim objCommonATLog As New clsCommonATLog
                    objCommonATLog._FormName = mstrFormName
                    objCommonATLog._LoginEmployeeUnkid = mintLoginEmployeeunkid
                    objCommonATLog._ClientIP = mstrClientIP
                    objCommonATLog._HostName = mstrHostName
                    objCommonATLog._FromWeb = mblnIsWeb
                    objCommonATLog._AuditUserId = mintAuditUserId
                    objCommonATLog._CompanyUnkid = mintCompanyUnkid
                    objCommonATLog._AuditDate = mdtAuditDate
                    'S.SANDEEP [28-May-2018] -- END

                    If objCommonATLog.Insert_AtLog(objDataOperation, 1, "lvcancelform", "cancelunkid", mintCancelunkid, False, mintCanceluserunkid) = False Then
                        exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                        Throw exForce
                    End If

                    'S.SANDEEP [28-May-2018] -- START
                    'ISSUE/ENHANCEMENT : {Audit Trails} 
                    objCommonATLog = Nothing
                    'S.SANDEEP [28-May-2018] -- END


                Next

                'START FOR UPDATE ISSUE DAYS TO BE VOID  

                strQ = "Select isnull(leaveissueunkid,0) leaveissueunkid from lvleaveissue_master where formunkid = @formunkid and employeeunkid = @employeeunkid and leavetypeunkid = @leavetypeunkid and isvoid = 0"
                objDataOperation.ClearParameters()
                objDataOperation.AddParameter("@formunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintFormunkid)
                objDataOperation.AddParameter("@employeeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intEmploeeyeunkid)
                objDataOperation.AddParameter("@leavetypeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intLeaveTypeunkid)
                Dim dList As DataSet = objDataOperation.ExecQuery(strQ, "List")

                If dList IsNot Nothing AndAlso dsList.Tables(0).Rows.Count > 0 Then

                    Dim mintLeaveIssueunkid As Integer = CInt(dList.Tables(0).Rows(0)("leaveissueunkid"))

                    strQ = "Select * from lvleaveissue_tran where leaveissueunkid = @leaveissueunkid AND convert(char(8),lvleaveIssue_tran.leavedate,112)  in (" & mstrLeaveDates & ") AND isvoid = 0"
                    objDataOperation.ClearParameters()
                    objDataOperation.AddParameter("@leaveissueunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintLeaveIssueunkid)
                    Dim dsIssueList As DataSet = objDataOperation.ExecQuery(strQ, "List")

                    Dim objbalance As New clsleavebalance_tran
                    Dim mintLeavebalanceunkid As Integer = -1
                    Dim objLeaveType As New clsleavetype_master
                    objLeaveType._Leavetypeunkid = intLeaveTypeunkid

                    If dsIssueList IsNot Nothing AndAlso dsIssueList.Tables(0).Rows.Count > 0 Then

                        'START FOR GET LEAVE BALANCE UNKID 

                        'Pinkal (01-Oct-2018) -- Start
                        'Enhancement - Leave Enhancement for NMB.
                        'Dim dsBalance As DataSet = objbalance.GetEmployeeBalanceData(intLeaveTypeunkid, intEmploeeyeunkid, False, intYearId)
                        Dim dsBalance As DataSet = Nothing
                        If intLeaveBalanceSetting = enLeaveBalanceSetting.Financial_Year Then
                            dsBalance = objbalance.GetEmployeeBalanceData(intLeaveTypeunkid, intEmploeeyeunkid, False, intYearId, False, False, False, objDataOperation)
                        ElseIf intLeaveBalanceSetting = enLeaveBalanceSetting.ELC Then
                            dsBalance = objbalance.GetEmployeeBalanceData(intLeaveTypeunkid, intEmploeeyeunkid, False, intYearId, True, True, False, objDataOperation)
                        End If
                        'Pinkal (01-Oct-2018) -- End


                        If dsBalance IsNot Nothing AndAlso dsBalance.Tables(0).Rows.Count > 0 Then
                            mintLeavebalanceunkid = CInt(dsBalance.Tables(0).Rows(0)("leavebalanceunkid"))
                        End If
                        'END FOR GET LEAVE BALANCE UNKID 


                        Dim objIssue As New clsleaveissue_Tran
                        Dim objFraction As New clsleaveday_fraction

                        For i As Integer = 0 To dsIssueList.Tables(0).Rows.Count - 1

                            'START FOR UPDATE LEAVE ISSUE TRAN TO VOID

                            strQ = "Update lvleaveissue_tran set isvoid = 1,voiddatetime = @voiddatetime,voiduserunkid = @voiduserunkid,voidreason = @voidreason where leaveissuetranunkid = @leaveissuetranunkid"
                            objDataOperation.ClearParameters()
                            objDataOperation.AddParameter("@leaveissuetranunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, CInt(dsIssueList.Tables(0).Rows(i)("leaveissuetranunkid")))
                            objDataOperation.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, mdtCancel_Trandate)
                            objDataOperation.AddParameter("@voiduserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintCanceluserunkid)
                            objDataOperation.AddParameter("@voidreason", SqlDbType.NVarChar, eZeeDataType.DESC_SIZE, mstrCancel_Reason)
                            objDataOperation.ExecNonQuery(strQ)

                            If objDataOperation.ErrorMessage <> "" Then
                                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                                Throw exForce
                            End If

                            'END FOR UPDATE LEAVE ISSUE TRAN TO VOID

                            'START FOR INSERT INTO LEAVE ISSUE TRAN AT TABLE

                            objIssue._Leaveissueunkid = mintLeaveIssueunkid
                            objIssue._mObjDataoperation = objDataOperation
                            objIssue._Leaveissuetranunkid = CInt(dsIssueList.Tables(0).Rows(i)("leaveissuetranunkid"))
                            objIssue._LeaveAdjustmentunkid = CInt(dsIssueList.Tables(0).Rows(i)("leaveadjustmentunkid"))
                            If Not IsDBNull(dsIssueList.Tables(0).Rows(i)("ispaid")) Then
                                objIssue._IsPaid = CBool(dsIssueList.Tables(0).Rows(i)("ispaid"))
                            Else
                                objIssue._IsPaid = False
                            End If


                            'Pinkal (01-Oct-2018) -- Start
                            'Enhancement - Leave Enhancement for NMB.

                            With objIssue
                                ._FormName = mstrFormName
                                ._LoginEmployeeUnkid = mintLoginEmployeeunkid
                                ._ClientIP = mstrClientIP
                                ._HostName = mstrHostName
                                ._FromWeb = mblnIsWeb
                                ._AuditUserId = mintAuditUserId
                                ._CompanyUnkid = mintCompanyUnkid
                                ._AuditDate = mdtAuditDate
                            End With

                            If objIssue.InsertAuditTrailForLeaveIssue(objDataOperation, 3, eZeeDate.convertDate(arStrLeavedate(i).ToString()), mintCanceluserunkid) = False Then
                                If objDataOperation.ErrorMessage <> "" Then
                                    exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                                    Throw exForce
                                End If
                            End If

                            'Pinkal (01-Oct-2018) -- End


                            'END FOR INSERT INTO LEAVE ISSUE TRAN AT TABLE


                            'START FOR GET LEAVE DAY FRACTION FOR THAT PARTICULAR DATE

                            If blnLeaveApproverForLeaveType.Trim.Length <= 0 Then
                                blnLeaveApproverForLeaveType = ConfigParameter._Object._IsLeaveApprover_ForLeaveType.ToString()
                            End If


                            'Pinkal (01-Oct-2018) -- Start
                            'Enhancement - Leave Enhancement for NMB.
                            Dim mdbldayFraction As Double = 0
                            If mblnSkipForApproverFlow Then
                                mdbldayFraction = objFraction.GetEmpLeaveDayFractionForSkipApproverFlow(eZeeDate.convertDate(objIssue._Leavedate.Date), mintFormunkid, intEmploeeyeunkid, objDataOperation)
                            Else
                                mdbldayFraction = objFraction.GetEmployeeLeaveDay_Fraction(eZeeDate.convertDate(objIssue._Leavedate.Date), mintFormunkid, intEmploeeyeunkid, blnLeaveApproverForLeaveType, intApproverId, objDataOperation)
                            End If
                            'Pinkal (01-Oct-2018) -- End


                            'END FOR GET LEAVE DAY FRACTION FOR THAT PARTICULAR DATE


                            'START FOR UPDATE LEAVE BALANCE IN LEAVE BALANCE TRAN TABLE

                            If objLeaveType._IsPaid Then

                                strQ = " Update lvleavebalance_tran set " & _
                                          "  issue_amount = issue_amount  - @dayfraction " & _
                                          ", balance = balance + @dayfraction  " & _
                                          ", uptolstyr_issueamt =  uptolstyr_issueamt  - @dayfraction " & _
                                          ", remaining_bal = remaining_bal  + @dayfraction " & _
                                          ", days = days - 1  " & _
                                          " where employeeunkid = @employeeunkid and leavetypeunkid = @leavetypeunkid and isvoid = 0  and yearunkid = @yearunkid  "
                            Else

                                strQ = " Update lvleavebalance_tran set " & _
                                          "  issue_amount = issue_amount  - @dayfraction " & _
                                          " where employeeunkid = @employeeunkid and leavetypeunkid = @leavetypeunkid and isvoid = 0  and yearunkid = @yearunkid  "
                            End If

                            If intLeaveBalanceSetting = enLeaveBalanceSetting.ELC Then
                                strQ &= " AND isopenelc = 1 AND iselc = 1 "
                            End If

                            objDataOperation.ClearParameters()
                            objDataOperation.AddParameter("@yearunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, IIf(intYearId > 0, intYearId, FinancialYear._Object._YearUnkid))
                            objDataOperation.AddParameter("@employeeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intEmploeeyeunkid)
                            If objLeaveType._DeductFromLeaveTypeunkid > 0 Then
                                objDataOperation.AddParameter("@leavetypeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, objLeaveType._DeductFromLeaveTypeunkid)
                            Else
                                objDataOperation.AddParameter("@leavetypeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intLeaveTypeunkid)
                            End If

                            'Pinkal (01-Oct-2018) -- Start
                            'Enhancement - Leave Enhancement for NMB.
                            'objDataOperation.AddParameter("@dayfraction", SqlDbType.Float, eZeeDataType.FLOAT_SIZE, mdbldayFraction)
                            objDataOperation.AddParameter("@dayfraction", SqlDbType.Decimal, eZeeDataType.DECIMAL_SIZE, mdbldayFraction)
                            'Pinkal (01-Oct-2018) -- End

                            objDataOperation.ExecNonQuery(strQ)

                            If objDataOperation.ErrorMessage <> "" Then
                                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                                Throw exForce
                            End If
                            objbalance._mObjDataoperation = objDataOperation
                            objbalance._LeaveBalanceunkid = mintLeavebalanceunkid
                            objbalance._Userunkid = mintCanceluserunkid
                            With objbalance
                                ._FormName = mstrFormName
                                ._LoginEmployeeUnkid = mintLoginEmployeeunkid
                                ._ClientIP = mstrClientIP
                                ._HostName = mstrHostName
                                ._FromWeb = mblnIsWeb
                                ._AuditUserId = mintAuditUserId
                                ._CompanyUnkid = mintCompanyUnkid
                                ._AuditDate = mdtAuditDate
                            End With

                            'Pinkal (01-Oct-2018) -- Start
                            'Enhancement - Leave Enhancement for NMB.

                            If objbalance.InsertAudiTrailForLeaveBalance(objDataOperation, 2) = False Then
                                If objDataOperation.ErrorMessage <> "" Then
                                    exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                                    Throw exForce
                                End If
                            End If

                            'Pinkal (01-Oct-2018) -- End

                            'END FOR UPDATE LEAVE BALANCE IN LEAVE BALANCE TRAN TABLE

                        Next


                    End If


                    If mintTotalLeaveDays = (arStrLeavedate.Length + mintTotalCanceldays) Then

                        'START FOR UPDATE LEAVE ISSUE MASTER TO VOID

                        strQ = "Update lvleaveissue_master set isvoid = 1,voiddatetime = @voiddatetime,voiduserunkid = @voiduserunkid,voidreason = @voidreason where leaveissueunkid = @leaveissueunkid"
                        objDataOperation.ClearParameters()
                        objDataOperation.AddParameter("@leaveissueunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintLeaveIssueunkid)
                        objDataOperation.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, mdtCancel_Trandate)
                        objDataOperation.AddParameter("@voiduserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintCanceluserunkid)
                        objDataOperation.AddParameter("@voidreason", SqlDbType.NVarChar, eZeeDataType.DESC_SIZE, mstrCancel_Reason)
                        objDataOperation.ExecNonQuery(strQ)

                        If objDataOperation.ErrorMessage <> "" Then
                            exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                            Throw exForce
                        End If

                        'END FOR UPDATE LEAVE ISSUE MASTER TO VOID

                        If objLeaveType._IsPaid AndAlso objLeaveType._IsShortLeave = True Then

                            'Pinkal (13-Oct-2018) -- Start
                            'Enhancement - Leave Enhancement for NMB.
                            'strQ = " Update lvleavebalance_tran set " & _
                            '  "    remaining_occurrence = remaining_occurrence  + 1 " & _
                            '  " WHERE employeeunkid = '" & intEmploeeyeunkid & "' and leavetypeunkid = '" & intLeaveTypeunkid & "' and isvoid = 0  and yearunkid = '" & FinancialYear._Object._YearUnkid & "' "

                            strQ = " Update lvleavebalance_tran set " & _
                                   "    remaining_occurrence = remaining_occurrence  + 1 " & _
                                      " WHERE employeeunkid =@employeeunkid and leavetypeunkid = @leavetypeunkid and isvoid = 0  and yearunkid = @yearunkid"

                            objDataOperation.ClearParameters()
                            objDataOperation.AddParameter("@employeeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intEmploeeyeunkid)
                            objDataOperation.AddParameter("@leavetypeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intLeaveTypeunkid)
                            objDataOperation.AddParameter("@yearunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, IIf(intYearId > 0, intYearId, FinancialYear._Object._YearUnkid))

                            'Pinkal (13-Oct-2018) -- End


                            objDataOperation.ExecNonQuery(strQ)

                            If objDataOperation.ErrorMessage <> "" Then
                                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                                Throw exForce
                            End If

                            objbalance._mObjDataoperation = objDataOperation
                            objbalance._LeaveBalanceunkid = mintLeavebalanceunkid
                            objbalance._Userunkid = mintCanceluserunkid

                            With objbalance
                                ._FormName = mstrFormName
                                ._LoginEmployeeUnkid = mintLoginEmployeeunkid
                                ._ClientIP = mstrClientIP
                                ._HostName = mstrHostName
                                ._FromWeb = mblnIsWeb
                                ._AuditUserId = mintAuditUserId
                                ._CompanyUnkid = mintCompanyUnkid
                                ._AuditDate = mdtAuditDate
                            End With


                            'Pinkal (01-Oct-2018) -- Start
                            'Enhancement - Leave Enhancement for NMB.

                            If objbalance.InsertAudiTrailForLeaveBalance(objDataOperation, 2) = False Then
                                If objDataOperation.ErrorMessage <> "" Then
                                    exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                                    Throw exForce
                                End If
                            End If

                            'Pinkal (01-Oct-2018) -- End

                        End If
                    End If

                End If

                'dList = Nothing 'Sohail (21 Oct 2019)

                'END FOR UPDATE ISSUE DAYS TO VOID  


                If mintTotalLeaveDays = (arStrLeavedate.Length + mintTotalCanceldays) Then

                    'Pinkal (27-Apr-2019) -- Start
                    'Enhancement - Audit Trail changes.
                    Dim objpendingleave_Tran As New clspendingleave_Tran
                    With objpendingleave_Tran
                        ._FormName = mstrFormName
                        ._LoginEmployeeunkid = mintLoginEmployeeunkid
                        ._ClientIP = mstrClientIP
                        ._HostName = mstrHostName
                        ._FromWeb = mblnIsWeb
                        ._AuditUserId = mintAuditUserId
                        ._CompanyUnkid = mintCompanyUnkid
                        ._AuditDate = mdtAuditDate
                    End With
                    'Pinkal (27-Apr-2019) -- End

                    'START FOR UPDATE LEAVE PENDING TRAN ISCANCEL IS TRUE THAT INDICATE THIS FORM IS CANCEL

                    'Pinkal (01-Oct-2018) -- Start
                    'Enhancement - Leave Enhancement for NMB.
                    If mblnSkipForApproverFlow = False Then
                        objpendingleave_Tran.UpdateLeaveStatus(objDataOperation, mintFormunkid, mintCanceluserunkid)
                        If objDataOperation.ErrorMessage <> "" Then
                            exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                            Throw exForce
                        End If
                    End If
                    'Pinkal (01-Oct-2018) -- End

                    'END FOR UPDATE LEAVE PENDING TRAN ISCANCEL IS TRUE THAT INDICATE THIS FORM IS CANCEL

                    'START FOR UPDATE LEAVE FORM STATUS IS ISSUED TO CANCEL

                    With objLvForm
                        ._FormName = mstrFormName
                        ._LoginEmployeeunkid = mintLoginEmployeeunkid
                        ._ClientIP = mstrClientIP
                        ._HostName = mstrHostName
                        ._FromWeb = mblnIsWeb
                        ._AuditUserId = mintAuditUserId
                        ._CompanyUnkid = mintCompanyUnkid
                        ._AuditDate = mdtAuditDate
                    End With
                    objLvForm.UpdateLeaveFormStatus(objDataOperation, mintFormunkid, 6, mintCanceluserunkid)  ' 6 FOR LEAVEFORM CANCEL STATUS


                    If objDataOperation.ErrorMessage <> "" Then
                        exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                        Throw exForce
                    End If

                    'END FOR UPDATE LEAVE FORM STATUS IS ISSUED TO CANCEL

                    'Sohail (21 Oct 2019) -- Start
                    'NMB Enhancement # : if all issued days are cancelled then system will void employee exemption transaction for It should be possible to update active employee status as Unpaid and automatically prorate payment of that employee effectively from the date selected. Also when employee is back, user should be able to update end date of the Unpaid status and automatically include that employee in payroll and prorate based on the date set
                    Dim objLeaveType As New clsleavetype_master
                    objLeaveType._Leavetypeunkid = intLeaveTypeunkid
                    If objLeaveType._IsPaid = False Then
                        Dim objEDates As New clsemployee_dates_tran
                        Dim intID As Integer = objEDates.GetUnkIdByLeaveIssueUnkId(objDataOperation, CInt(dList.Tables(0).Rows(0)("leaveissueunkid")))
                        If intID > 0 Then
                            objEDates._Datestranunkid = intID
                            objEDates._Isvoid = True
                            objEDates._Voiddatetime = dtCurrentDateAndTime
                            objEDates._Voidreason = mstrCancel_Reason
                            objEDates._Voiduserunkid = mintCanceluserunkid
                            objEDates._Userunkid = mintCanceluserunkid

                            If blnSkipEmployeeMovementApprovalFlow = False Then
                                Dim objADate As New clsDates_Approval_Tran
                                objADate._Isvoid = False
                                objADate._Voiddatetime = dtCurrentDateAndTime
                                objADate._Voidreason = mstrCancel_Reason
                                objADate._Voiduserunkid = -1
                                objADate._Isweb = False
                                objADate._Ip = mstrWebClientIP
                                objADate._Hostname = mstrWebHostName
                                objADate._Form_Name = mstrWebFrmName
                                objADate._Audituserunkid = mintCanceluserunkid

                                If objADate.Delete(intID, clsEmployeeMovmentApproval.enOperationType.DELETED, xCompanyUnkid, blnCreateADUserFromEmpMst, objDataOperation) = False Then
                                    If objADate._Message <> "" Then
                                        Throw New Exception(objADate._Message)
                                    End If
                                    Exit Function
                                End If

                                Dim objPMovement As New clsEmployeeMovmentApproval
                                Dim intPrivilegeId As Integer = enUserPriviledge.AllowToApproveRejectEmployeeExemption
                                Dim eMovement As clsEmployeeMovmentApproval.enMovementType = clsEmployeeMovmentApproval.enMovementType.EXEMPTION
                                objPMovement.SendNotification(1, xDatabaseName, xUserAccessModeSetting, _
                                                      xCompanyUnkid, intYearId, _
                                                      intPrivilegeId, eMovement, eZeeDate.convertDate(dtEmployeeAsOnDate), _
                                                      mintCanceluserunkid, mstrWebFrmName, enLogin_Mode.DESKTOP, _
                                                      xUsername, clsEmployeeMovmentApproval.enOperationType.DELETED, _
                                                      False, enEmp_Dates_Transaction.DT_EXEMPTION, 0, intEmploeeyeunkid, "")

                            Else
                                If objEDates.Delete(blnCreateADUserFromEmpMst, intID, xCompanyUnkid, objDataOperation) = False Then
                                    If objEDates._Message <> "" Then
                                        Throw New Exception(objEDates._Message)
                                    End If
                                    Exit Function
                                End If
                            End If
                        End If
                    End If
                    'Sohail (21 Oct 2019) -- End

                End If

                'START FOR UPDATE LEAVE ISSUE MASTER FOR CANCEL DAY FRACTION 

                objDataOperation.ClearParameters()
                strQ = "Update lvleaveissue_master set cancelcount = ISNULL(cancelcount,0.00) +  @dayfraction where formunkid = @formunkid and employeeunkid = @employeeunkid and leavetypeunkid = @leavetypeunkid "
                objDataOperation.AddParameter("@formunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintFormunkid)
                objDataOperation.AddParameter("@employeeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intEmploeeyeunkid)
                objDataOperation.AddParameter("@leavetypeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intLeaveTypeunkid)
                objDataOperation.AddParameter("@dayfraction", SqlDbType.Decimal, eZeeDataType.DECIMAL_SIZE, mdclFraction)
                objDataOperation.ExecNonQuery(strQ)

                If objDataOperation.ErrorMessage <> "" Then
                    exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                    Throw exForce
                End If

                dList = Nothing 'Sohail (21 Oct 2019)
                'END FOR UPDATE LEAVE ISSUE MASTER FOR CANCEL DAY FRACTION 

            End If



            'Pinkal (01-Oct-2018) -- Start
            'Enhancement - Leave Enhancement for NMB.
            If mblnSkipForApproverFlow = False Then
                If mintClaimMasterID > 0 AndAlso mdtCancelExpense IsNot Nothing Then
                    Dim objExpApproverTran As New clsclaim_request_approval_tran
                    objExpApproverTran._YearId = intYearId
                    objExpApproverTran._LeaveBalanceSetting = intLeaveBalanceSetting
                    With objExpApproverTran
                        ._FormName = mstrFormName
                        ._LoginEmployeeunkid = mintLoginEmployeeunkid
                        ._ClientIP = mstrClientIP
                        ._HostName = mstrHostName
                        ._FromWeb = mblnIsWeb
                        ._AuditUserId = mintAuditUserId
                        ._CompanyUnkid = mintCompanyUnkid
                        ._AuditDate = mdtAuditDate
                    End With
                    If objExpApproverTran.CancelExpense(mdtCancelExpense, enExpenseType.EXP_LEAVE, mstrCancelExpenseRemak, mintCanceluserunkid, dtCurrentDateAndTime, objDataOperation) = False Then
                        exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                        Throw exForce
                    End If
                End If
            End If
            'Pinkal (01-Oct-2018) -- End


            'Pinkal (25-AUG-2017) -- Start
            'Enhancement - Working on B5 plus TnA Enhancement.
            If ArtLic._Object.ModuleStatus(ArutiModule.ArutiModule.Time_and_Attendance_Management) OrElse ArtLic._Object.IsDemo Then
                Dim objLogin As New clslogin_Tran
                With objLogin
                    ._FormName = mstrFormName
                    ._LoginEmployeeUnkid = mintLoginEmployeeunkid
                    ._ClientIP = mstrClientIP
                    ._HostName = mstrHostName
                    ._FromWeb = mblnIsWeb
                    ._AuditUserId = mintAuditUserId
                    ._CompanyUnkid = mintCompanyUnkid
                    ._AuditDate = mdtAuditDate
                End With
                If objLogin.UpdateLoginSummaryFromLeave(objDataOperation, arStrLeavedate, intEmploeeyeunkid) = False Then
                    exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                End If
                objLogin = Nothing
            End If
            'Pinkal (25-AUG-2017) -- End



            'Pinkal (28-Nov-2017) -- Start
            'Enhancement - Cancel form document attachment  - (RefNo: 132) On leave cancel process there should be a place to attach document for a reference and this feature (cancel leave form) should be included in Aruti self service .
            If mdtAttachment IsNot Nothing AndAlso mdtAttachment.Rows.Count > 0 Then
                Dim drRow() As DataRow = mdtAttachment.Select("AUD = 'A'")
                If drRow.Length > 0 Then
                    For i As Integer = 0 To drRow.Length - 1
                        drRow(i)("transactionunkid") = mintCancelunkid
                        drRow(i).AcceptChanges()
                    Next
                End If
                Dim objDocument As New clsScan_Attach_Documents
                objDocument._Datatable = mdtAttachment
                With objDocument
                    ._FormName = mstrFormName
                    ._LoginEmployeeunkid = mintLoginEmployeeunkid
                    ._ClientIP = mstrClientIP
                    ._HostName = mstrHostName
                    ._FromWeb = mblnIsWeb
                    ._AuditUserId = mintAuditUserId
                    ._CompanyUnkid = mintCompanyUnkid
                    ._AuditDate = mdtAuditDate
                End With
                If objDocument.InsertUpdateDelete_Documents(objDataOperation) = False Then
                    Throw New Exception(objDocument._Message)
                End If
                objDocument = Nothing
            End If
            'Pinkal (28-Nov-2017) -- End


            objDataOperation.ReleaseTransaction(True)

            Return True
        Catch ex As Exception
            objDataOperation.ReleaseTransaction(False)
            Throw New Exception(ex.Message & "; Procedure Name: Insert; Module Name: " & mstrModuleName)
            Return False
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
    End Function

    ''' <summary>
    ''' Modify By: Pinkal
    ''' </summary>
    ''' <returns>Boolean</returns>
    ''' <purpose> Update Database Table (lvcancelform) </purpose>
    Public Function Update() As Boolean
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        Dim objDataOperation As New clsDataOperation
        objDataOperation.BindTransaction()

        If isExist(mstrCancelformno, mintCancelunkid) Then
            mstrMessage = Language.getMessage(mstrModuleName, 1, "This Cancel Form No is already defined. Please define new Cancel Form No.")
            objDataOperation.ReleaseTransaction(False)
            Return False
        End If

        Try
            objDataOperation.AddParameter("@cancelunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintCancelunkid.ToString)
            objDataOperation.AddParameter("@formunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintFormunkid.ToString)
            objDataOperation.AddParameter("@cancelformno", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrCancelformno.ToString)
            objDataOperation.AddParameter("@cancel_leavedate", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, mdtCancel_Leavedate)
            objDataOperation.AddParameter("@cancel_reason", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrCancel_Reason.ToString)
            objDataOperation.AddParameter("@canceluserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintCanceluserunkid.ToString)
            objDataOperation.AddParameter("@cancel_trandate", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, mdtCancel_Trandate)
            objDataOperation.AddParameter("@isvoid", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsvoid.ToString)
            objDataOperation.AddParameter("@voiduserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintVoiduserunkid.ToString)
            objDataOperation.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, mdtVoiddatetime)
            objDataOperation.AddParameter("@dayfraction", SqlDbType.Decimal, eZeeDataType.DECIMAL_SIZE, mdclDayFraction)
            objDataOperation.AddParameter("@employeeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintEmployeeunkid)

            strQ = "UPDATE lvcancelform SET " & _
              "  formunkid = @formunkid" & _
              ", cancelformno = @cancelformno" & _
              ", cancel_leavedate = @cancel_leavedate" & _
              ", cancel_reason = @cancel_reason" & _
              ", canceluserunkid = @canceluserunkid" & _
              ", cancel_trandate = @cancel_trandate" & _
                       ", employeeunkid = @employeeunkid " & _
                       ", dayfraction = @dayfraction " & _
              ", isvoid = @isvoid" & _
              ", voiduserunkid = @voiduserunkid" & _
              ", voiddatetime = @voiddatetime " & _
            "WHERE cancelunkid = @cancelunkid "

            Call objDataOperation.ExecNonQuery(strQ)

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            'S.SANDEEP [28-May-2018] -- START
            'ISSUE/ENHANCEMENT : {Audit Trails} 
            Dim objCommonATLog As New clsCommonATLog
            objCommonATLog._FormName = mstrFormName
            objCommonATLog._LoginEmployeeUnkid = mintLoginEmployeeunkid
            objCommonATLog._ClientIP = mstrClientIP
            objCommonATLog._HostName = mstrHostName
            objCommonATLog._FromWeb = mblnIsWeb
            objCommonATLog._AuditUserId = mintAuditUserId
            objCommonATLog._CompanyUnkid = mintCompanyUnkid
            objCommonATLog._AuditDate = mdtAuditDate
            'S.SANDEEP [28-May-2018] -- END

            If objCommonATLog.IsTableDataUpdate("atcommon_log", "lvcancelform", mintCancelunkid, "", 2, objDataOperation) Then
                If objCommonATLog.Insert_AtLog(objDataOperation, 2, "lvcancelform", "cancelunkid", mintCancelunkid, False, mintCanceluserunkid) = False Then
                    exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                    Throw exForce
                End If
            End If

            'S.SANDEEP [28-May-2018] -- START
            'ISSUE/ENHANCEMENT : {Audit Trails} 
            objCommonATLog = Nothing
            'S.SANDEEP [28-May-2018] -- END



            objDataOperation.ReleaseTransaction(True)

            Return True
        Catch ex As Exception
            objDataOperation.ReleaseTransaction(False)
            Throw New Exception(ex.Message & "; Procedure Name: Update; Module Name: " & mstrModuleName)
            Return False
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
    End Function

    ''' <summary>
    ''' Modify By: Pinkal
    ''' </summary>
    ''' <returns>Boolean</returns>
    ''' <purpose> Delete Database Table (lvcancelform) </purpose>
    Public Function Delete(ByVal intUnkid As Integer) As Boolean
        If isUsed(intUnkid) Then
            mstrMessage = "<Message>"
            Return False
        End If

        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        Dim objDataOperation As New clsDataOperation
        objDataOperation.BindTransaction()

        Try
            strQ = "Update lvcancelform set isvoid = 1,voiduserunkid = @voiduserunkid,voiddatetime = @voiddatetime" & _
            "WHERE cancelunkid = @cancelunkid "

            objDataOperation.ClearParameters()
            objDataOperation.AddParameter("@cancelunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intUnkid)
            objDataOperation.AddParameter("@voiduserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintVoiduserunkid)
            objDataOperation.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, mdtVoiddatetime)

            Call objDataOperation.ExecNonQuery(strQ)

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            'S.SANDEEP [28-May-2018] -- START
            'ISSUE/ENHANCEMENT : {Audit Trails} 
            Dim objCommonATLog As New clsCommonATLog
            objCommonATLog._FormName = mstrFormName
            objCommonATLog._LoginEmployeeUnkid = mintLoginEmployeeunkid
            objCommonATLog._ClientIP = mstrClientIP
            objCommonATLog._HostName = mstrHostName
            objCommonATLog._FromWeb = mblnIsWeb
            objCommonATLog._AuditUserId = mintAuditUserId
            objCommonATLog._CompanyUnkid = mintCompanyUnkid
            objCommonATLog._AuditDate = mdtAuditDate
            'S.SANDEEP [28-May-2018] -- END

            If objCommonATLog.Insert_AtLog(objDataOperation, 3, "lvcancelform", "cancelunkid", intUnkid, False, mintVoiduserunkid) = False Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            'S.SANDEEP [28-May-2018] -- START
            'ISSUE/ENHANCEMENT : {Audit Trails} 
            objCommonATLog = Nothing
            'S.SANDEEP [28-May-2018] -- END


            objDataOperation.ReleaseTransaction(True)

            Return True
        Catch ex As Exception
            objDataOperation.ReleaseTransaction(False)
            Throw New Exception(ex.Message & "; Procedure Name: Delete; Module Name: " & mstrModuleName)
            Return False
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
    End Function

    ''' <summary>
    ''' Modify By: Pinkal
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>
    Public Function isUsed(ByVal intUnkid As Integer) As Boolean
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        objDataOperation = New clsDataOperation

        Try
            strQ = "<Query>"

            objDataOperation.AddParameter("@cancelunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intUnkid)

            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            Return dsList.Tables(0).Rows.Count > 0
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: isUsed; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
    End Function

    ''' <summary>
    ''' Modify By: Pinkal
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>
    Public Function isExist(ByVal strCancelFormNo As String, Optional ByVal intUnkid As Integer = -1, Optional ByVal objDataOperation As clsDataOperation = Nothing) As Boolean
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception
        If objDataOperation Is Nothing Then
            objDataOperation = New clsDataOperation
        End If

        Try
            strQ = "SELECT " & _
                      "  cancelunkid " & _
                      ", formunkid " & _
                      ", cancelformno " & _
                      ", cancel_leavedate " & _
                      ", cancel_reason " & _
                      ", canceluserunkid " & _
                      ", cancel_trandate " & _
                      ", isvoid " & _
                      ", voiduserunkid " & _
                      ", voiddatetime " & _
                     "FROM lvcancelform " & _
                     "WHERE cancelformno = @cancelformno "

            If intUnkid > 0 Then
                strQ &= " AND cancelunkid <> @cancelunkid"
            End If

            objDataOperation.ClearParameters()
            objDataOperation.AddParameter("@cancelformno", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, strCancelFormNo)
            objDataOperation.AddParameter("@cancelunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intUnkid)

            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            Return dsList.Tables(0).Rows.Count > 0
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: isExist; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
        End Try
    End Function


    ''' <summary>
    ''' Modify By: Pinkal
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>
    Public Function GetCancelDayExistForEmployee(ByVal xEmployeeId As Integer, ByVal xCancelDate As Date, Optional ByVal objDataOperation As clsDataOperation = Nothing) As Boolean
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception
        If objDataOperation Is Nothing Then
            objDataOperation = New clsDataOperation
        End If
        Try
            strQ = " SELECT " & _
                      "  cancelunkid " & _
                      " FROM lvcancelform " & _
                      " WHERE employeeunkid = @employeeunkid AND CONVERT(CHAR(8),cancel_leavedate,112) = @cancel_leavedate "


            objDataOperation.ClearParameters()
            objDataOperation.AddParameter("@cancel_leavedate", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, eZeeDate.convertDate(xCancelDate))
            objDataOperation.AddParameter("@employeeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, xEmployeeId)

            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            Return dsList.Tables(0).Rows.Count > 0
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: GetCancelDayExistForEmployee; Module Name: " & mstrModuleName)
        End Try
    End Function





    'Pinkal (12-Oct-2012) -- Start
    'Enhancement : TRA Changes

    '''' <summary>
    '''' Modify By: Pinkal
    '''' </summary>
    '''' <purpose> Assign all Property variable </purpose>
    'Public Sub UpdateCancelData(ByVal objDataOperation As clsDataOperation, ByVal mdtCancelDate As Date, ByVal intEmployeeunkid As Integer)
    '    Dim dsList As DataSet = Nothing
    '    Dim strQ As String = ""
    '    Dim exForce As Exception
    '    Try

    '        strQ = "Select ISNULL(dayfraction,0.00) as dayfraction, formunkid From lvcancelform WHERE employeeunkid = @employeeunkid AND Convert(CHAR(8),cancel_leavedate,112) = @canceldate AND isvoid = 0"
    '        objDataOperation.ClearParameters()
    '        objDataOperation.AddParameter("@employeeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intEmployeeunkid)
    '        objDataOperation.AddParameter("@canceldate", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, eZeeDate.convertDate(mdtCancelDate))
    '        dsList = objDataOperation.ExecQuery(strQ, "List")

    '        If dsList IsNot Nothing AndAlso dsList.Tables(0).Rows.Count > 0 Then

    '            strQ = " Update lvleaveIssue_master set cancelcount = cancelcount - @dayfraction  WHERE employeeunkid = @employeeunkid AND formunkid = @formunkid AND isvoid = 0"
    '            objDataOperation.ClearParameters()
    '            objDataOperation.AddParameter("@employeeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intEmployeeunkid)
    '            objDataOperation.AddParameter("@formunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, CInt(dsList.Tables(0).Rows(0)("formunkid")))
    '            objDataOperation.AddParameter("@dayfraction", SqlDbType.Decimal, eZeeDataType.DECIMAL_SIZE, CDec(dsList.Tables(0).Rows(0)("dayfraction")))
    '            objDataOperation.ExecNonQuery(strQ)

    '            strQ = " Update lvcancelform set dayfraction = 0  WHERE employeeunkid = @employeeunkid AND Convert(CHAR(8),cancel_leavedate,112) = @canceldate AND isvoid = 0"
    '            objDataOperation.ClearParameters()
    '            objDataOperation.AddParameter("@employeeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intEmployeeunkid)
    '            objDataOperation.AddParameter("@canceldate", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, eZeeDate.convertDate(mdtCancelDate))
    '            objDataOperation.ExecNonQuery(strQ)

    '        End If

    '    Catch ex As Exception
    '        Throw New Exception(ex.Message & "; Procedure Name: UpdateCancelData; Module Name: " & mstrModuleName)
    '    Finally
    '        exForce = Nothing
    '        If dsList IsNot Nothing Then dsList.Dispose()
    '    End Try
    'End Sub


    'Pinkal (12-Oct-2012) -- End



    '<Language> This Auto Generated Text Please Do Not Modify it.
#Region " Language & UI Settings "
    Public Shared Sub SetMessages()
        Try
            Language.setMessage(mstrModuleName, 1, "This Cancel Form No is already defined. Please define new Cancel Form No.")

        Catch Ex As Exception
            DisplayError.Show("-1", Ex.Message, "SetMessages", mstrModuleName)
        End Try
    End Sub
#End Region 'Language & UI Settings
    '</Language>
End Class