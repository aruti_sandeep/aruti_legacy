﻿'************************************************************************************************************************************
'Class Name : clsmembership_master.vb
'Purpose    : All Membership Opration like getList, Insert, Update, Delete, checkDuplicate
'Date       :26/06/2010
'Written By :Pinkal
'Modified   :
'Last Message Index = 3
'************************************************************************************************************************************

Imports eZeeCommonLib
''' <summary>
''' Purpose: 
''' Developer: Pinkal
''' </summary>
''' 
Public Class clsmembership_master
    Private Shared ReadOnly mstrModuleName As String = "clsmembership_master"
    Dim objDataOperation As clsDataOperation
    Dim mstrMessage As String = ""

#Region " Private variables "
    Private mintMembershipunkid As Integer
    Private mstrMembershipcode As String = String.Empty
    Private mstrMembershipname As String = String.Empty
    Private mintMembershipcategoryunkid As Integer
    Private mstrDescription As String = String.Empty
    Private mblnIsactive As Boolean = True
    Private mstrMembershipname1 As String = String.Empty
    Private mstrMembershipname2 As String = String.Empty
    'Sandeep [ 25 APRIL 2011 ] -- Start
    Private mintETransHeadId As Integer = 0
    Private mintCTransHeadId As Integer = 0
    'Sandeep [ 25 APRIL 2011 ] -- End 
    Private mblnIsappearonpayslip As Boolean = True 'Sohail (02 Oct 2014)
    'Nilay (05-Feb-2016) -- Start
    'Enhancement - Set Employer Membership Number Textbox option on Membership master screen and show membership wise employer membership number on EPF Period wise report in 57.2 and 58.1 SP as well - (From: moses.shembilu, Sent: Thursday, February 04, 2016 3:00 PM, To: 'Anjan' , Cc: 'Rutta anatory' ; glory.ponsian@npktechnologies.com ; 'Sohail Aruti' , Subject: GEPF Number not reflected - Picks NSSF Number only )
    Private mstrEmployerMemNo As String = String.Empty
    'Nilay (05-Feb-2016) -- End

    'Nilay (03-Oct-2016) -- Start
    'Enhancement : Membership wise option 'Show Basic Salary on Statutory Report and ignore that option on configuration
    Private mblnIsShowBasicSalary As Boolean = False
    'Nilay (03-Oct-2016) -- End

#End Region

#Region " Properties "

'S.SANDEEP [28-May-2018] -- START 
'ISSUE/ENHANCEMENT : {Audit Trails} 
Private mstrFormName As String = String.Empty
Public WriteOnly Property _FormName() As String 
Set(ByVal value As String) 
mstrFormName = value 
End Set 
End Property 
 
Private mstrClientIP As String = ""
Public WriteOnly Property _ClientIP() As String 
Set(ByVal value As String) 
mstrClientIP = value 
End Set 
End Property 
 
Private mstrHostName As String = ""
Public WriteOnly Property _HostName() As String 
Set(ByVal value As String) 
mstrHostName = value 
End Set 
End Property 
 
Private mblnIsWeb As Boolean = False
Public WriteOnly Property _FromWeb() As Boolean 
Set(ByVal value As Boolean) 
mblnIsWeb = value 
End Set 
End Property 
 
    Private mintAuditUserId As Integer = 0
    Public WriteOnly Property _AuditUserId() As Integer
        Set(ByVal value As Integer)
            mintAuditUserId = value
        End Set
    End Property

    Private mintCompanyUnkid As Integer = 0
    Public WriteOnly Property _CompanyUnkid() As Integer
        Set(ByVal value As Integer)
            mintCompanyUnkid = value
        End Set
    End Property
 
Private mdtAuditDate As DateTime = Now
Public WriteOnly Property _AuditDate() As DateTime 
Set(ByVal value As DateTime) 
mdtAuditDate = value 
End Set 
End Property 
 
Private xDataOpr As clsDataOperation = Nothing
Public Property _xDataOpr() As clsDataOperation 
Get 
Return xDataOpr 
End Get 
Set(ByVal value As clsDataOperation) 
xDataOpr = value 
End Set 
End Property 
 
Private mintLoginEmployeeunkid As Integer = 0
Public Property _LoginEmployeeunkid() As Integer 
Get 
Return mintLoginEmployeeunkid 
End Get 
Set(ByVal value As Integer) 
mintLoginEmployeeunkid = value 
End Set 
End Property 
 
Private mintVoidlogingemployeeunkid As Integer=0
Public Property _Voidloginemployeeunkid() As Integer 
Get 
Return mintVoidlogingemployeeunkid 
End Get 
Set(ByVal value As Integer) 
mintVoidlogingemployeeunkid = value 
End Set 
End Property 
'S.SANDEEP [28-May-2018] -- END 
    ''' <summary>
    ''' Purpose: Get Message from Class 
    ''' Modify By: Pinkal
    ''' </summary>
    Public ReadOnly Property _Message() As String
        Get
            Return mstrMessage
        End Get
    End Property

    ''' <summary>
    ''' Purpose: Get or Set membershipunkid
    ''' Modify By: Pinkal
    ''' </summary>
    Public Property _Membershipunkid() As Integer
        Get
            Return mintMembershipunkid
        End Get
        Set(ByVal value As Integer)
            mintMembershipunkid = Value
            Call getData()
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set membershipcode
    ''' Modify By: Pinkal
    ''' </summary>
    Public Property _Membershipcode() As String
        Get
            Return mstrMembershipcode
        End Get
        Set(ByVal value As String)
            mstrMembershipcode = Value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set membershipname
    ''' Modify By: Pinkal
    ''' </summary>
    Public Property _Membershipname() As String
        Get
            Return mstrMembershipname
        End Get
        Set(ByVal value As String)
            mstrMembershipname = Value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set membershipcategoryunkid
    ''' Modify By: Pinkal
    ''' </summary>
    Public Property _Membershipcategoryunkid() As Integer
        Get
            Return mintMembershipcategoryunkid
        End Get
        Set(ByVal value As Integer)
            mintMembershipcategoryunkid = Value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set description
    ''' Modify By: Pinkal
    ''' </summary>
    Public Property _Description() As String
        Get
            Return mstrDescription
        End Get
        Set(ByVal value As String)
            mstrDescription = Value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set isactive
    ''' Modify By: Pinkal
    ''' </summary>
    Public Property _Isactive() As Boolean
        Get
            Return mblnIsactive
        End Get
        Set(ByVal value As Boolean)
            mblnIsactive = Value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set membershipname1
    ''' Modify By: Pinkal
    ''' </summary>
    Public Property _Membershipname1() As String
        Get
            Return mstrMembershipname1
        End Get
        Set(ByVal value As String)
            mstrMembershipname1 = Value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set membershipname2
    ''' Modify By: Pinkal
    ''' </summary>
    Public Property _Membershipname2() As String
        Get
            Return mstrMembershipname2
        End Get
        Set(ByVal value As String)
            mstrMembershipname2 = Value
        End Set
    End Property

    'Sandeep [ 25 APRIL 2011 ] -- Start
    ''' <summary>
    ''' Purpose: Get or Set Employee Contribution Transaction Head
    ''' Modify By: Sandeep J Sharma
    ''' </summary>
    Public Property _ETransHead_Id() As Integer
        Get
            Return mintETransHeadId
        End Get
        Set(ByVal value As Integer)
            mintETransHeadId = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set Company Contribution Transaction Head
    ''' Modify By: Sandeep J Sharma
    ''' </summary>
    Public Property _CTransHead_Id() As Integer
        Get
            Return mintCTransHeadId
        End Get
        Set(ByVal value As Integer)
            mintCTransHeadId = value
        End Set
    End Property
    'Sandeep [ 25 APRIL 2011 ] -- End 

    'Sohail (02 Oct 2014) -- Start
    'Enhancement - Appear on Payslip option on Membership Master.
    Public Property _Isappearonpayslip() As Boolean
        Get
            Return mblnIsappearonpayslip
        End Get
        Set(ByVal value As Boolean)
            mblnIsappearonpayslip = value
        End Set
    End Property
    'Sohail (02 Oct 2014) -- End

    'Nilay (05-Feb-2016) -- Start
    'Enhancement - Set Employer Membership Number Textbox option on Membership master screen and show membership wise employer membership number on EPF Period wise report in 57.2 and 58.1 SP as well - (From: moses.shembilu, Sent: Thursday, February 04, 2016 3:00 PM, To: 'Anjan' , Cc: 'Rutta anatory' ; glory.ponsian@npktechnologies.com ; 'Sohail Aruti' , Subject: GEPF Number not reflected - Picks NSSF Number only )
    Public Property _EmployerMemNo() As String
        Get
            Return mstrEmployerMemNo
        End Get
        Set(ByVal value As String)
            mstrEmployerMemNo = value
        End Set
    End Property
    'Nilay (05-Feb-2016) -- End

    'Nilay (03-Oct-2016) -- Start
    'Enhancement : Membership wise option 'Show Basic Salary on Statutory Report and ignore that option on configuration
    Public Property _IsShowBasicSalary() As Boolean
        Get
            Return mblnIsShowBasicSalary
        End Get
        Set(ByVal value As Boolean)
            mblnIsShowBasicSalary = value
        End Set
    End Property
    'Nilay (03-Oct-2016) -- End

#End Region

    ''' <summary>
    ''' Modify By: Pinkal
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>
    Public Sub GetData()
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        objDataOperation = New clsDataOperation

        Try

            'Sandeep [ 25 APRIL 2011 ] -- Start
            'strQ = "SELECT " & _
            '              "  membershipunkid " & _
            '              ", membershipcode " & _
            '              ", membershipname " & _
            '              ", membershipcategoryunkid " & _
            '              ", description " & _
            '              ", isactive " & _
            '              ", membershipname1 " & _
            '              ", membershipname2 " & _
            '             "FROM hrmembership_master " & _
            '             "WHERE membershipunkid = @membershipunkid "
            strQ = "SELECT " & _
              "  membershipunkid " & _
              ", membershipcode " & _
              ", membershipname " & _
              ", membershipcategoryunkid " & _
              ", description " & _
              ", isactive " & _
              ", membershipname1 " & _
              ", membershipname2 " & _
                          ", ISNULL(emptranheadunkid,0) AS emptranheadunkid " & _
                          ", ISNULL(cotranheadunkid,0) AS cotranheadunkid " & _
                          ", ISNULL(isappearonpayslip, 0) AS isappearonpayslip " & _
                      ", ISNULL(employer_membershipno,'') AS employer_membershipno " & _
                      ", ISNULL(isshowbasicsalary,0) AS isshowbasicsalary " & _
             "FROM hrmembership_master " & _
             "WHERE membershipunkid = @membershipunkid "
            'Nilay (03-Oct-2016) -- [isshowbasicsalary]
            'Nilay (05-Feb-2016) -- [employer_membershipno]
            'Sohail (02 Oct 2014) - [isappearonpayslip]
            'Sandeep [ 25 APRIL 2011 ] -- End 

            objDataOperation.AddParameter("@membershipunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintMembershipunkid.ToString)

            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            For Each dtRow As DataRow In dsList.Tables(0).Rows
                mintMembershipunkid = CInt(dtRow.Item("membershipunkid"))
                mstrMembershipcode = dtRow.Item("membershipcode").ToString
                mstrMembershipname = dtRow.Item("membershipname").ToString
                mintMembershipcategoryunkid = CInt(dtRow.Item("membershipcategoryunkid"))
                mstrDescription = dtRow.Item("description").ToString
                mblnIsactive = CBool(dtRow.Item("isactive"))
                mstrMembershipname1 = dtRow.Item("membershipname1").ToString
                mstrMembershipname2 = dtRow.Item("membershipname2").ToString
                'Sandeep [ 25 APRIL 2011 ] -- Start
                mintETransHeadId = dtRow.Item("emptranheadunkid")
                mintCTransHeadId = dtRow.Item("cotranheadunkid")
                'Sandeep [ 25 APRIL 2011 ] -- End 
                mblnIsappearonpayslip = CBool(dtRow.Item("isappearonpayslip")) 'Sohail (02 Oct 2014)
                'Nilay (05-Feb-2016) -- Start
                'Enhancement - Set Employer Membership Number Textbox option on Membership master screen and show membership wise employer membership number on EPF Period wise report in 57.2 and 58.1 SP as well - (From: moses.shembilu, Sent: Thursday, February 04, 2016 3:00 PM, To: 'Anjan' , Cc: 'Rutta anatory' ; glory.ponsian@npktechnologies.com ; 'Sohail Aruti' , Subject: GEPF Number not reflected - Picks NSSF Number only )
                mstrEmployerMemNo = dtRow.Item("employer_membershipno").ToString
                'Nilay (05-Feb-2016) -- End

                'Nilay (03-Oct-2016) -- Start
                'Enhancement : Membership wise option 'Show Basic Salary on Statutory Report and ignore that option on configuration
                mblnIsShowBasicSalary = CBool(dtRow.Item("isshowbasicsalary"))
                'Nilay (03-Oct-2016) -- End

                Exit For
            Next
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: GetData; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
    End Sub


    ''' <summary>
    ''' Modify By: Pinkal
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>
    Public Function GetList(ByVal strTableName As String, Optional ByVal blnOnlyActive As Boolean = True) As DataSet
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        objDataOperation = New clsDataOperation

        Try

            'Sandeep [ 25 APRIL 2011 ] -- Start
            'strQ = "SELECT " & _
            '              "  membershipunkid " & _
            '              ", membershipcode " & _
            '              ", membershipname " & _
            '              ", cfcommon_master.name " & _
            '              ", membershipcategoryunkid " & _
            '              ", description " & _
            '              ", hrmembership_master.isactive " & _
            '              ", membershipname1 " & _
            '              ", membershipname2 " & _
            '             "FROM hrmembership_master  " & _
            '             " LEFT JOIN cfcommon_master on cfcommon_master.masterunkid = hrmembership_master.membershipcategoryunkid "

            strQ = "SELECT " & _
              "  membershipunkid " & _
              ", membershipcode " & _
              ", membershipname " & _
              ", cfcommon_master.name " & _
              ", membershipcategoryunkid " & _
              ", hrmembership_master.description " & _
              ", hrmembership_master.isactive " & _
              ", membershipname1 " & _
              ", membershipname2 " & _
                   ", emptranheadunkid " & _
                   ", cotranheadunkid " & _
                   ", ISNULL(isappearonpayslip, 0) AS isappearonpayslip " & _
                      ", ISNULL(employer_membershipno,'') AS employer_membershipno " & _
                      ", ISNULL(isshowbasicsalary,0) AS isshowbasicsalary " & _
             "FROM hrmembership_master  " & _
             " LEFT JOIN cfcommon_master on cfcommon_master.masterunkid = hrmembership_master.membershipcategoryunkid AND cfcommon_master.mastertype = " & clsCommon_Master.enCommonMaster.MEMBERSHIP_CATEGORY
            'Nilay (03-Oct-2016) -- [isshowbasicsalary]
            'Nilay (05-Feb-2016) -- [employer_membershipno]
            'Sohail (02 Oct 2014) - [isappearonpayslip]
            'Sandeep [ 25 APRIL 2011 ] -- End 

            'S.SANDEEP [04 DEC 2015] -- START {hrmembership_master.description} -- END

            If blnOnlyActive Then
                strQ &= " WHERE hrmembership_master.isactive = 1 "
            End If

            dsList = objDataOperation.ExecQuery(strQ, strTableName)

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: GetList; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
        Return dsList
    End Function


    ''' <summary>
    ''' Modify By: Pinkal
    ''' </summary>
    ''' <returns>Boolean</returns>
    ''' <purpose> INSERT INTO Database Table (hrmembership_master) </purpose>
    Public Function Insert() As Boolean
        If isExist(mstrMembershipcode) Then
            mstrMessage = Language.getMessage(mstrModuleName, 1, "This Membership Code is already defined. Please define new Membership Code.")
            Return False
        ElseIf isExist("", mstrMembershipname) Then
            mstrMessage = Language.getMessage(mstrModuleName, 2, "This Membership Name is already defined. Please define new Membership Name.")
            Return False
        End If

        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception


        'Pinkal (12-Oct-2011) -- Start
        'ENHANCEMENT : AUDIT TRAIL MAINTENANCE
        objDataOperation = New clsDataOperation
        objDataOperation.BindTransaction()
        'Pinkal (12-Oct-2011) -- End

        Try
            objDataOperation.AddParameter("@membershipcode", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrMembershipcode.ToString)
            objDataOperation.AddParameter("@membershipname", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrMembershipname.ToString)
            objDataOperation.AddParameter("@membershipcategoryunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintMembershipcategoryunkid.ToString)
            objDataOperation.AddParameter("@description", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrDescription.ToString)
            objDataOperation.AddParameter("@isactive", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsactive.ToString)
            objDataOperation.AddParameter("@membershipname1", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrMembershipname1.ToString)
            objDataOperation.AddParameter("@membershipname2", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrMembershipname2.ToString)
            'Sandeep [ 25 APRIL 2011 ] -- Start
            objDataOperation.AddParameter("@emptranheadunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintETransHeadId.ToString)
            objDataOperation.AddParameter("@cotranheadunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintCTransHeadId.ToString)
            objDataOperation.AddParameter("@isappearonpayslip", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsappearonpayslip.ToString) 'Sohail (02 Oct 2014)
            'Nilay (05-Feb-2016) -- Start
            'Enhancement - Set Employer Membership Number Textbox option on Membership master screen and show membership wise employer membership number on EPF Period wise report in 57.2 and 58.1 SP as well - (From: moses.shembilu, Sent: Thursday, February 04, 2016 3:00 PM, To: 'Anjan' , Cc: 'Rutta anatory' ; glory.ponsian@npktechnologies.com ; 'Sohail Aruti' , Subject: GEPF Number not reflected - Picks NSSF Number only )
            objDataOperation.AddParameter("@employer_membershipno", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrEmployerMemNo.ToString)
            'Nilay (05-Feb-2016) -- End

            'Nilay (03-Oct-2016) -- Start
            'Enhancement : Membership wise option 'Show Basic Salary on Statutory Report and ignore that option on configuration
            objDataOperation.AddParameter("@isshowbasicsalary", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsShowBasicSalary.ToString)
            'Nilay (03-Oct-2016) -- End

            'strQ = "INSERT INTO hrmembership_master ( " & _
            '  "  membershipcode " & _
            '  ", membershipname " & _
            '  ", membershipcategoryunkid " & _
            '  ", description " & _
            '  ", isactive " & _
            '  ", membershipname1 " & _
            '  ", membershipname2" & _
            '") VALUES (" & _
            '  "  @membershipcode " & _
            '  ", @membershipname " & _
            '  ", @membershipcategoryunkid " & _
            '  ", @description " & _
            '  ", @isactive " & _
            '  ", @membershipname1 " & _
            '  ", @membershipname2" & _
            '"); SELECT @@identity"

            strQ = "INSERT INTO hrmembership_master ( " & _
              "  membershipcode " & _
              ", membershipname " & _
              ", membershipcategoryunkid " & _
              ", description " & _
              ", isactive " & _
              ", membershipname1 " & _
              ", membershipname2" & _
                              ", emptranheadunkid " & _
                              ", cotranheadunkid " & _
                              ", isappearonpayslip " & _
                      ", employer_membershipno " & _
                      ", isshowbasicsalary  " & _
            ") VALUES (" & _
              "  @membershipcode " & _
              ", @membershipname " & _
              ", @membershipcategoryunkid " & _
              ", @description " & _
              ", @isactive " & _
              ", @membershipname1 " & _
              ", @membershipname2" & _
                              ", @emptranheadunkid " & _
                              ", @cotranheadunkid " & _
                              ", @isappearonpayslip " & _
                      ", @employer_membershipno " & _
                      ", @isshowbasicsalary  " & _
            "); SELECT @@identity"
            'Nilay (03-Oct-2016) -- [isshowbasicsalary]
            'Nilay (05-Feb-2016) -- [employer_membershipno]
            'Sohail (02 Oct 2014) - [isappearonpayslip]
            'Sandeep [ 25 APRIL 2011 ] -- End 

            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            mintMembershipunkid = dsList.Tables(0).Rows(0).Item(0)

            'Pinkal (12-Oct-2011) -- Start
            'ENHANCEMENT : AUDIT TRAIL MAINTENANCE

          'S.SANDEEP [28-May-2018] -- START
'ISSUE/ENHANCEMENT : {Audit Trails} 
Dim objCommonATLog AS New clsCommonATLog 
objCommonATLog._FormName = mstrFormName 
objCommonATLog._LoginEmployeeUnkid = mintLoginEmployeeunkid 
            objCommonATLog._ClientIP = mstrClientIP
            objCommonATLog._HostName = mstrHostName
            objCommonATLog._FromWeb = mblnIsWeb
            objCommonATLog._AuditUserId = mintAuditUserId
objCommonATLog._CompanyUnkid = mintCompanyUnkid
            objCommonATLog._AuditDate = mdtAuditDate
            'S.SANDEEP [28-May-2018] -- END

            If objCommonATLog.Insert_AtLog(objDataOperation, 1, "hrmembership_master", "membershipunkid", mintMembershipunkid) = False Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            'S.SANDEEP [28-May-2018] -- START
            'ISSUE/ENHANCEMENT : {Audit Trails} 
            objCommonATLog = Nothing
            'S.SANDEEP [28-May-2018] -- END

            objDataOperation.ReleaseTransaction(True)
            'Pinkal (12-Oct-2011) -- End


            Return True
        Catch ex As Exception
            'Pinkal (12-Oct-2011) -- Start
            'ENHANCEMENT : AUDIT TRAIL MAINTENANCE
            objDataOperation.ReleaseTransaction(False)
            'Pinkal (12-Oct-2011) -- End
            Throw New Exception(ex.Message & "; Procedure Name: Insert; Module Name: " & mstrModuleName)
            Return False
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
    End Function

    ''' <summary>
    ''' Modify By: Pinkal
    ''' </summary>
    ''' <returns>Boolean</returns>
    ''' <purpose> Update Database Table (hrmembership_master) </purpose>
    Public Function Update() As Boolean
        If isExist(mstrMembershipcode, "", mintMembershipunkid) Then
            mstrMessage = Language.getMessage(mstrModuleName, 1, "This Membership Code is already defined. Please define new Membership Code.")
            Return False
        ElseIf isExist("", mstrMembershipname, mintMembershipunkid) Then
            mstrMessage = Language.getMessage(mstrModuleName, 2, "This Membership Name is already defined. Please define new Membership Name.")
            Return False
        End If

        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        'Pinkal (12-Oct-2011) -- Start
        'ENHANCEMENT : AUDIT TRAIL MAINTENANCE
        objDataOperation = New clsDataOperation
        objDataOperation.BindTransaction()
        'Pinkal (12-Oct-2011) -- End

        Try
            objDataOperation.AddParameter("@membershipunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintMembershipunkid.ToString)
            objDataOperation.AddParameter("@membershipcode", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrMembershipcode.ToString)
            objDataOperation.AddParameter("@membershipname", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrMembershipname.ToString)
            objDataOperation.AddParameter("@membershipcategoryunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintMembershipcategoryunkid.ToString)
            objDataOperation.AddParameter("@description", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrDescription.ToString)
            objDataOperation.AddParameter("@isactive", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsactive.ToString)
            objDataOperation.AddParameter("@membershipname1", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrMembershipname1.ToString)
            objDataOperation.AddParameter("@membershipname2", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrMembershipname2.ToString)
            'Sandeep [ 25 APRIL 2011 ] -- Start
            objDataOperation.AddParameter("@emptranheadunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintETransHeadId.ToString)
            objDataOperation.AddParameter("@cotranheadunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintCTransHeadId.ToString)
            objDataOperation.AddParameter("@isappearonpayslip", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsappearonpayslip.ToString) 'Sohail (02 Oct 2014)
            'Nilay (05-Feb-2016) -- Start
            'Enhancement - Set Employer Membership Number Textbox option on Membership master screen and show membership wise employer membership number on EPF Period wise report in 57.2 and 58.1 SP as well - (From: moses.shembilu, Sent: Thursday, February 04, 2016 3:00 PM, To: 'Anjan' , Cc: 'Rutta anatory' ; glory.ponsian@npktechnologies.com ; 'Sohail Aruti' , Subject: GEPF Number not reflected - Picks NSSF Number only )
            objDataOperation.AddParameter("@employer_membershipno", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrEmployerMemNo.ToString)
            'Nilay (05-Feb-2016) -- End

            'Nilay (03-Oct-2016) -- Start
            'Enhancement : Membership wise option 'Show Basic Salary on Statutory Report and ignore that option on configuration
            objDataOperation.AddParameter("@isshowbasicsalary", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsShowBasicSalary.ToString)
            'Nilay (03-Oct-2016) -- End

            'strQ = "UPDATE hrmembership_master SET " & _
            '          "  membershipcode = @membershipcode" & _
            '          ", membershipname = @membershipname" & _
            '          ", membershipcategoryunkid = @membershipcategoryunkid" & _
            '          ", description = @description" & _
            '          ", isactive = @isactive" & _
            '          ", membershipname1 = @membershipname1" & _
            '          ", membershipname2 = @membershipname2 " & _
            '        "WHERE membershipunkid = @membershipunkid "

            strQ = "UPDATE hrmembership_master SET " & _
              "  membershipcode = @membershipcode" & _
              ", membershipname = @membershipname" & _
              ", membershipcategoryunkid = @membershipcategoryunkid" & _
              ", description = @description" & _
              ", isactive = @isactive" & _
              ", membershipname1 = @membershipname1" & _
              ", membershipname2 = @membershipname2 " & _
                      ", emptranheadunkid = @emptranheadunkid " & _
                      ", cotranheadunkid = @cotranheadunkid " & _
                      ", isappearonpayslip = @isappearonpayslip " & _
                      ", employer_membershipno = @employer_membershipno " & _
                      ", isshowbasicsalary = @isshowbasicsalary " & _
            "WHERE membershipunkid = @membershipunkid "
            'Nilay (03-Oct-2016) -- [isshowbasicsalary]
            'Nilay (05-Feb-2016) -- [employer_membershipno]
            'Sohail (02 Oct 2014) - [isappearonpayslip]
            'Sandeep [ 25 APRIL 2011 ] -- End 


            Call objDataOperation.ExecNonQuery(strQ)

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            'Pinkal (12-Oct-2011) -- Start
            'ENHANCEMENT : AUDIT TRAIL MAINTENANCE

           'S.SANDEEP [28-May-2018] -- START
            'ISSUE/ENHANCEMENT : {Audit Trails} 
            Dim objCommonATLog As New clsCommonATLog
            objCommonATLog._FormName = mstrFormName
            objCommonATLog._LoginEmployeeUnkid = mintLoginEmployeeunkid
            objCommonATLog._ClientIP = mstrClientIP
            objCommonATLog._HostName = mstrHostName
            objCommonATLog._FromWeb = mblnIsWeb
            objCommonATLog._AuditUserId = mintAuditUserId
objCommonATLog._CompanyUnkid = mintCompanyUnkid
            objCommonATLog._AuditDate = mdtAuditDate
            'S.SANDEEP [28-May-2018] -- END

            If objCommonATLog.IsTableDataUpdate("atcommon_log", "hrmembership_master", mintMembershipunkid, "membershipunkid", 2) Then

                If objCommonATLog.Insert_AtLog(objDataOperation, 2, "hrmembership_master", "membershipunkid", mintMembershipunkid) = False Then
                    exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                    Throw exForce
                End If

            End If

            'S.SANDEEP [28-May-2018] -- START
            'ISSUE/ENHANCEMENT : {Audit Trails} 
            objCommonATLog = Nothing
            'S.SANDEEP [28-May-2018] -- END

            objDataOperation.ReleaseTransaction(True)
            'Pinkal (12-Oct-2011) -- End


            Return True
        Catch ex As Exception
            'Pinkal (12-Oct-2011) -- Start
            'ENHANCEMENT : AUDIT TRAIL MAINTENANCE
            objDataOperation.ReleaseTransaction(False)
            'Pinkal (12-Oct-2011) -- End
            Throw New Exception(ex.Message & "; Procedure Name: Update; Module Name: " & mstrModuleName)
            Return False
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
    End Function

    ''' <summary>
    ''' Modify By: Pinkal
    ''' </summary>
    ''' <returns>Boolean</returns>
    ''' <purpose> Delete Database Table (hrmembership_master) </purpose>
    Public Function Delete(ByVal intUnkid As Integer) As Boolean
        If isUsed(intUnkid) Then
            mstrMessage = Language.getMessage(mstrModuleName, 3, "Sorry, You cannot delete selected Membership. Reason: This Membership is in use.")
            Return False
        End If

        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        'Pinkal (12-Oct-2011) -- Start
        'ENHANCEMENT : AUDIT TRAIL MAINTENANCE
        objDataOperation = New clsDataOperation
        objDataOperation.BindTransaction()
        'Pinkal (12-Oct-2011) -- End

        Try
            'StrQ = "DELETE FROM hrmembership_master " & _
            '"WHERE membershipunkid = @membershipunkid "

            strQ = "UPDATE hrmembership_master " & _
                    " SET isactive = 0 " & _
                    "WHERE membershipunkid = @membershipunkid "


            objDataOperation.AddParameter("@membershipunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intUnkid)

            Call objDataOperation.ExecNonQuery(strQ)

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            'Pinkal (12-Oct-2011) -- Start
            'ENHANCEMENT : AUDIT TRAIL MAINTENANCE

            'S.SANDEEP [28-May-2018] -- START
            'ISSUE/ENHANCEMENT : {Audit Trails} 
            Dim objCommonATLog As New clsCommonATLog
            objCommonATLog._FormName = mstrFormName
            objCommonATLog._LoginEmployeeUnkid = mintLoginEmployeeunkid
            objCommonATLog._ClientIP = mstrClientIP
            objCommonATLog._HostName = mstrHostName
            objCommonATLog._FromWeb = mblnIsWeb
            objCommonATLog._AuditUserId = mintAuditUserId
objCommonATLog._CompanyUnkid = mintCompanyUnkid
            objCommonATLog._AuditDate = mdtAuditDate
            'S.SANDEEP [28-May-2018] -- END

            If objCommonATLog.Insert_AtLog(objDataOperation, 3, "hrmembership_master", "membershipunkid", intUnkid) = False Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            'S.SANDEEP [28-May-2018] -- START
            'ISSUE/ENHANCEMENT : {Audit Trails} 
            objCommonATLog = Nothing
            'S.SANDEEP [28-May-2018] -- END

            objDataOperation.ReleaseTransaction(True)
            'Pinkal (12-Oct-2011) -- End


            Return True
        Catch ex As Exception
            'Pinkal (12-Oct-2011) -- Start
            'ENHANCEMENT : AUDIT TRAIL MAINTENANCE
            objDataOperation.ReleaseTransaction(False)
            'Pinkal (12-Oct-2011) -- End
            Throw New Exception(ex.Message & "; Procedure Name: Update; Module Name: " & mstrModuleName)
            Return False
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
    End Function

    ''' <summary>
    ''' Modify By: Pinkal
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>
    Public Function isUsed(ByVal intUnkid As Integer) As Boolean
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        'Sandeep [ 09 Oct 2010 ] -- Start
        'Issues Reported by Vimal
        Dim dsTables As DataSet = Nothing
        Dim blnIsUsed As Boolean = False
        'Sandeep [ 09 Oct 2010 ] -- End 

        objDataOperation = New clsDataOperation

        Try
            strQ = "SELECT TABLE_NAME AS TableName FROM INFORMATION_SCHEMA.COLUMNS " & _
                       "WHERE COLUMN_NAME='membershipunkid' "

            dsTables = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            strQ = ""
            objDataOperation.AddParameter("@membershipunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intUnkid)

            For Each dtRow As DataRow In dsTables.Tables("List").Rows
                If dtRow.Item("TableName") = "hrmembership_master" Then Continue For
                strQ = "SELECT membershipunkid FROM " & dtRow.Item("TableName").ToString & " WHERE membershipunkid = @membershipunkid "

                dsList = objDataOperation.ExecQuery(strQ, "Used")

                If objDataOperation.ErrorMessage <> "" Then
                    exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                    Throw exForce
                End If

                If dsList.Tables("Used").Rows.Count > 0 Then
                    blnIsUsed = True
                    Exit For
                End If
            Next

            If blnIsUsed = False Then
                strQ = "SELECT TABLE_NAME AS TableName FROM INFORMATION_SCHEMA.COLUMNS " & _
                       "WHERE COLUMN_NAME='vendorunkid' "

                dsTables = objDataOperation.ExecQuery(strQ, "List")

                For Each dtRow As DataRow In dsTables.Tables("List").Rows
                    If dtRow.Item("TableName") = "hrmembership_master" Then Continue For
                    strQ = "SELECT vendorunkid FROM " & dtRow.Item("TableName").ToString & " WHERE vendorunkid = @membershipunkid "

                    dsList = objDataOperation.ExecQuery(strQ, "Used")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

                    If dsList.Tables("Used").Rows.Count > 0 Then
                        blnIsUsed = True
                        Exit For
                    End If
                Next
            End If

            mstrMessage = ""

            Return blnIsUsed

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: isUsed; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
    End Function

    ''' <summary>
    ''' Modify By: Pinkal
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>
    Public Function isExist(Optional ByVal strCode As String = "", Optional ByVal strName As String = "", Optional ByVal intUnkid As Integer = -1) As Boolean
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        objDataOperation = New clsDataOperation

        Try
            strQ = "SELECT " & _
              "  membershipunkid " & _
              ", membershipcode " & _
              ", membershipname " & _
              ", membershipcategoryunkid " & _
              ", description " & _
              ", isactive " & _
              ", membershipname1 " & _
              ", membershipname2 " & _
             "FROM hrmembership_master " & _
             "WHERE  1=1"

            'Anjan (10 Feb 2012)-Start
            'ENHANCEMENT : TRA COMMENTS on Andrew sir's Request
            strQ &= " AND isactive = 1 "
            'Anjan (10 Feb 2012)-End

            If strCode.Length > 0 Then
                strQ &= " AND membershipcode = @membershipcode "
            End If

            If strName.Length > 0 Then
                strQ &= " AND membershipname = @membershipname"
            End If

            If intUnkid > 0 Then
                strQ &= " AND membershipunkid <> @membershipunkid"
            End If

            objDataOperation.AddParameter("@membershipcode", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, strCode)
            objDataOperation.AddParameter("@membershipname", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, strName)
            objDataOperation.AddParameter("@membershipunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intUnkid)

            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            Return dsList.Tables(0).Rows.Count > 0
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: isExist; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
    End Function

    ''' <summary>
    ''' Modify By: Pinkal
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>
    ''' Sandeep [ 21 Aug 2010 ] -- Start
    ''' Public Function getListForCombo(Optional ByVal strListName As String = "List", Optional ByVal mblFlag As Boolean = False) As DataSet
    ''' <param name="intMembershipType"> 0 = All Membership,    1 = Payroll Membership,    2 = Non-Payroll Membership </param>
    Public Function getListForCombo(Optional ByVal strListName As String = "List", _
                                    Optional ByVal mblFlag As Boolean = False, _
                                    Optional ByVal intMemberCatUnkid As Integer = -1, _
                                    Optional ByVal intMembershipType As Integer = 0, _
                                    Optional ByVal strDatabaseName As String = "") As DataSet
        'Sohail (26 May 2021) - [strDatabaseName]
        'Sohail (18 May 2013) - [intMembershipType]
        'Sandeep [ 21 Aug 2010 ] -- End 
        Dim dsList As New DataSet
        Dim objDataOperation As New clsDataOperation
        Dim strQ As String = String.Empty
        Dim exForce As Exception
        'Sohail (26 May 2021) -- Start
        'Netis Gabon Enhancement : OLD-357 : New Payslip  Template "Template 19".
        Dim sDBName As String = ""
        If strDatabaseName.Trim <> "" Then
            sDBName = strDatabaseName & ".."
        End If
        'Sohail (26 May 2021) -- End

        Try
            If mblFlag = True Then
                strQ = "SELECT 0 as membershipunkid, ' ' +  @name  as name UNION "
            End If

            'Sandeep [ 21 Aug 2010 ] -- Start
            'strQ &= "SELECT membershipunkid, membershipname  as name FROM hrmembership_master where isactive = 1 ORDER BY name "
            strQ &= "SELECT membershipunkid, membershipname  as name FROM " & sDBName & "hrmembership_master where isactive = 1 "
            'Sohail (26 May 2021) - [sDBName]
            If intMemberCatUnkid <> -1 Then
                strQ &= " AND membershipcategoryunkid = @MCateId "
                objDataOperation.AddParameter("@MCateId", SqlDbType.Int, eZeeDataType.INT_SIZE, intMemberCatUnkid)
            End If

            'Sohail (18 Jul 2013) -- Start
            'TRA - ENHANCEMENT
            If intMembershipType = 1 Then 'Payroll Membership
                'Sohail (12 Aug 2013) -- Start
                'TRA - ENHANCEMENT - As per Rutta's request for Thika Power, Include one head membership as well on Pension Fund Report.
                'strQ &= " AND ISNULL(emptranheadunkid, 0) > 0 AND ISNULL(cotranheadunkid, 0) > 0 "
                strQ &= " AND ( ISNULL(emptranheadunkid, 0) > 0 OR ISNULL(cotranheadunkid, 0) > 0 ) "
                'Sohail (12 Aug 2013) -- End
            ElseIf intMembershipType = 2 Then 'Non-Payroll Membership
                strQ &= " AND ISNULL(emptranheadunkid, 0) <= 0 AND ISNULL(cotranheadunkid, 0) <= 0 "
            End If
            'Sohail (18 Jul 2013) -- End

            strQ &= " ORDER BY name "
            'Sandeep [ 21 Aug 2010 ] -- End 

            objDataOperation.AddParameter("@name", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 6, "Select Membership"))
            dsList = objDataOperation.ExecQuery(strQ, strListName)
            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If
            Return dsList
        Catch ex As Exception
            DisplayError.Show(-1, ex.Message, "getListForCombo", mstrModuleName)
            Return Nothing
        Finally
            exForce = Nothing
            objDataOperation = Nothing
            dsList.Dispose()
            dsList = Nothing
        End Try

    End Function


    'S.SANDEEP [ 18 May 2011 ] -- START
    Public Function GetMembershipUnkid(ByVal StrName As String) As Integer
        Dim StrQ As String = String.Empty
        Dim dsList As New DataSet
        Dim exForce As Exception
        Try
            objDataOperation = New clsDataOperation

            StrQ = "SELECT membershipunkid FROM hrmembership_master WHERE membershipname = @name AND isactive = 1 "

            objDataOperation.AddParameter("@name", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, StrName)

            dsList = objDataOperation.ExecQuery(StrQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & " : " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            If dsList.Tables(0).Rows.Count > 0 Then
                Return dsList.Tables(0).Rows(0)("membershipunkid")
            End If

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "GetMembershipUnkid", mstrModuleName)
        End Try
    End Function
    'S.SANDEEP [ 18 May 2011 ] -- END 

  'Sandeep [ 25 APRIL 2011 ] -- Start
    Public Function GetHeadName(ByVal intMemId As Integer) As String
        Dim StrHeadName As String = String.Empty
        Dim StrQ As String = String.Empty
        Dim dsList As New DataSet
        Dim exForce As Exception
        Try

            objDataOperation = New clsDataOperation

            StrQ = "SELECT " & _
                        "    ISNULL(ETranHead.trnheadname,@None) AS ETName " & _
                        "   ,ISNULL(CTranHead.trnheadname,@None) AS CTName " & _
                        "FROM hrmembership_master " & _
                        "    LEFT JOIN prtranhead_master AS ETranHead ON hrmembership_master.emptranheadunkid = ETranHead.tranheadunkid " & _
                        "    LEFT JOIN prtranhead_master AS CTranHead ON hrmembership_master.cotranheadunkid = CTranHead.tranheadunkid " & _
                        "WHERE hrmembership_master.membershipunkid = @MemId " & _
                        "    AND hrmembership_master.isactive = 1 "

            objDataOperation.AddParameter("@MemId", SqlDbType.Int, eZeeDataType.INT_SIZE, intMemId)
            objDataOperation.AddParameter("@None", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 4, "None"))

            dsList = objDataOperation.ExecQuery(StrQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ":" & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            If dsList.Tables("List").Rows.Count > 0 Then
                StrHeadName = dsList.Tables("List").Rows(0).Item("ETName")
                StrHeadName &= Language.getMessage(mstrModuleName, 5, " and ")
                StrHeadName &= dsList.Tables("List").Rows(0).Item("CTName")
            End If
                
            Return StrHeadName

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "GetHeadName", mstrModuleName)
            Return StrHeadName
        End Try
    End Function

    Public Function GetMembershipName(ByVal intHeadId As Integer, ByVal intCHeadId As Integer, Optional ByVal intUnkid As Integer = -1) As String
        Dim StrHeadName As String = String.Empty
        Dim StrQ As String = String.Empty
        Dim dsList As New DataSet
        Dim exForce As Exception
        Try
            objDataOperation = New clsDataOperation

            'S.SANDEEP [ 17 OCT 2012 ] -- START
            'ENHANCEMENT : TRA CHANGES
            'StrQ = "SELECT " & _
            '       " ISNULL(hrmembership_master.membershipname,'') AS TName " & _
            '       "FROM hrmembership_master " & _
            '       " LEFT JOIN prtranhead_master AS ETranHead ON hrmembership_master.emptranheadunkid = ETranHead.tranheadunkid " & _
            '       " LEFT JOIN prtranhead_master AS CTranHead ON hrmembership_master.cotranheadunkid = CTranHead.tranheadunkid " & _
            '       "WHERE ETranHead.tranheadunkid = @EHeadId And CTranHead.tranheadunkid = @CHeadId " & _
            '       " AND hrmembership_master.isactive = 1 "
            StrQ = "SELECT " & _
                        "  ISNULL(hrmembership_master.membershipname,'') AS TName " & _
                        "FROM hrmembership_master " & _
                        "  LEFT JOIN prtranhead_master AS ETranHead ON hrmembership_master.emptranheadunkid = ETranHead.tranheadunkid " & _
                        "  LEFT JOIN prtranhead_master AS CTranHead ON hrmembership_master.cotranheadunkid = CTranHead.tranheadunkid " & _
                   "WHERE hrmembership_master.isactive = 1 "

            If intHeadId > 0 Then
                StrQ &= "AND ETranHead.tranheadunkid = @EHeadId "
            End If

            If intCHeadId > 0 Then
                StrQ &= "AND CTranHead.tranheadunkid = @CHeadId "
            End If
            'S.SANDEEP [ 17 OCT 2012 ] -- END


            If intUnkid > 0 Then
                StrQ &= " AND membershipunkid <> @membershipunkid"
                objDataOperation.AddParameter("@membershipunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intUnkid)
            End If

            objDataOperation.AddParameter("@EHeadId", SqlDbType.Int, eZeeDataType.INT_SIZE, intHeadId)
            objDataOperation.AddParameter("@CHeadId", SqlDbType.Int, eZeeDataType.INT_SIZE, intCHeadId)

            dsList = objDataOperation.ExecQuery(StrQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ":" & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            If dsList.Tables("List").Rows.Count > 0 Then
                StrHeadName = dsList.Tables("List").Rows(0)("TName").ToString
            End If

            Return StrHeadName

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "GetMembershipName", mstrModuleName)
            Return StrHeadName
        End Try
    End Function
    'Sandeep [ 25 APRIL 2011 ] -- End 

    'S.SANDEEP [ 17 OCT 2012 ] -- START
    'ENHANCEMENT : TRA CHANGES
    Public Function Is_Membership_Used(ByVal intUnkid As Integer) As Boolean
        Dim StrQ As String = String.Empty
        Dim exForce As Exception
        Dim iCnt As Integer = -1
        Try
            objDataOperation = New clsDataOperation

            StrQ = "SELECT * " & _
                       "FROM hremployee_meminfo_tran " & _
                       "WHERE membershipunkid = '" & intUnkid & "' AND isdeleted = 0 AND isactive = 1 "

            iCnt = objDataOperation.RecordCount(StrQ)

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ":" & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            If iCnt > 0 Then
                Return True
            Else
                Return False
            End If
        Catch ex As Exception
            Call DisplayError.Show("-1", ex.Message, "Is_Membership_Used", mstrModuleName)
        End Try
    End Function
    'S.SANDEEP [ 17 OCT 2012 ] -- END

	'<Language> This Auto Generated Text Please Do Not Modify it.
#Region " Language & UI Settings "
	Public Shared Sub SetMessages()
		Try
			Language.setMessage(mstrModuleName, 1, "This Membership Code is already defined. Please define new Membership Code.")
			Language.setMessage(mstrModuleName, 2, "This Membership Name is already defined. Please define new Membership Name.")
			Language.setMessage(mstrModuleName, 3, "Sorry, You cannot delete selected Membership. Reason: This Membership is in use.")
			Language.setMessage(mstrModuleName, 4, "None")
			Language.setMessage(mstrModuleName, 5, " and")
			Language.setMessage(mstrModuleName, 6, "Select Membership")

		Catch Ex As Exception
			DisplayError.Show("-1", ex.Message, "SetMessages", mstrModuleName)
		End Try
	End Sub
#End Region 'Language & UI Settings
	'</Language>
End Class