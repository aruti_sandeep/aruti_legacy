﻿Imports System.Runtime.Serialization

'Pinkal (22-Jul-2019) -- Start
'Enhancement - Working on P2P Document attachment in Claim Request for NMB.

Public Class clsCRP2PIntegration

    Dim mstrCategory As String = ""
    Dim mstrmainline As String = ""
    Dim mstrdept As String = ""
    Dim mstrglCode As String = ""
    Dim mstrcostCenter As String = ""
    Dim mstrccy As String = ""
    Dim mdecinvAmount As Decimal = 0
    Dim mstrinvoiceNo As String = ""
    Dim mstrmaker As String = ""
    Dim mstrremarks As String = ""
    Dim mstrstatus As String = ""
    Dim mstrsupplierId As String = ""
    Dim mstrextRefNo As String = ""
    Dim IDocumentList As New List(Of DocumentList)

    Public Property category() As String
        Get
            Return mstrCategory
        End Get
        Set(ByVal value As String)
            mstrCategory = value
        End Set
    End Property

    Public Property mainline() As String
        Get
            Return mstrmainline
        End Get
        Set(ByVal value As String)
            mstrmainline = value
        End Set
    End Property

    Public Property dept() As String
        Get
            Return mstrdept
        End Get
        Set(ByVal value As String)
            mstrdept = value
        End Set
    End Property

    Public Property glCode() As String
        Get
            Return mstrglCode
        End Get
        Set(ByVal value As String)
            mstrglCode = value
        End Set
    End Property

    Public Property costCenter() As String
        Get
            Return mstrcostCenter
        End Get
        Set(ByVal value As String)
            mstrcostCenter = value
        End Set
    End Property

    Public Property ccy() As String
        Get
            Return mstrccy
        End Get
        Set(ByVal value As String)
            mstrccy = value
        End Set
    End Property

    Public Property invAmount() As Decimal
        Get
            Return mdecinvAmount
        End Get
        Set(ByVal value As Decimal)
            mdecinvAmount = value
        End Set
    End Property

    Public Property invoiceNo() As String
        Get
            Return mstrinvoiceNo
        End Get
        Set(ByVal value As String)
            mstrinvoiceNo = value
        End Set
    End Property

    Public Property maker() As String
        Get
            Return mstrmaker
        End Get
        Set(ByVal value As String)
            mstrmaker = value
        End Set
    End Property

    Public Property remarks() As String
        Get
            Return mstrremarks
        End Get
        Set(ByVal value As String)
            mstrremarks = value
        End Set
    End Property

    Public Property status() As String
        Get
            Return mstrstatus
        End Get
        Set(ByVal value As String)
            mstrstatus = value
        End Set
    End Property
 
    Public Property supplierId() As String
        Get
            Return mstrsupplierId
        End Get
        Set(ByVal value As String)
            mstrsupplierId = value
        End Set
    End Property

    Public Property extRefNo() As String
        Get
            Return mstrextRefNo
        End Get
        Set(ByVal value As String)
            mstrextRefNo = value
        End Set
    End Property

    Public Property documentList() As List(Of DocumentList)
        Get
            Return IDocumentList
        End Get
        Set(ByVal value As List(Of DocumentList))
            IDocumentList = value
        End Set
    End Property

End Class

Public Class DocumentList

    Public mstrDocumentTitle As String = String.Empty
    Public mstrDocumentType As String = String.Empty
    Public mstrDocumentExtension As String = String.Empty
    Public mstrFileName As String = String.Empty
    Public mstrValue As String = String.Empty

#Region "Constructor"

    Public Sub New()
    End Sub
    Public Sub New(ByVal _documentTitle As String, ByVal _documentType As String, ByVal _documentExtension As String, ByVal _filename As String, ByVal _value As String)
        mstrDocumentTitle = _documentTitle
        mstrDocumentType = _documentType
        mstrDocumentExtension = _documentExtension
        mstrFileName = _filename
        mstrValue = _value
    End Sub

#End Region

#Region "Properties"

    Public Property documentTitle() As String
        Get
            Return mstrDocumentTitle
        End Get
        Set(ByVal value As String)
            mstrDocumentTitle = value
        End Set
    End Property

    Public Property documentType() As String
        Get
            Return mstrDocumentType
        End Get
        Set(ByVal value As String)
            mstrDocumentType = value
        End Set
    End Property

    Public Property documentExtension() As String
        Get
            Return mstrDocumentExtension
        End Get
        Set(ByVal value As String)
            mstrDocumentExtension = value
        End Set
    End Property

    Public Property fileName() As String
        Get
            Return mstrFileName
        End Get
        Set(ByVal value As String)
            mstrFileName = value
        End Set
    End Property

    Public Property value() As String
        Get
            Return mstrValue
        End Get
        Set(ByVal val As String)
            mstrValue = val
        End Set
    End Property

#End Region

End Class

'Pinkal (22-Jul-2019) -- End


Public Class clsCRP2PUserDetail
    Dim mstrUserName As String = "aruti"
    Dim mstrPassword As String = "123456"

    Public Property username() As String
        Get
            Return mstrUserName
        End Get
        Set(ByVal value As String)
            mstrUserName = value
        End Set
    End Property

    Public Property password() As String
        Get
            Return mstrPassword
        End Get
        Set(ByVal value As String)
            mstrPassword = value
        End Set
    End Property

End Class


'Pinkal (11-Sep-2019) -- Start
'Enhancement NMB - Working On Claim Retirement for NMB.

Public Class clsClaimRetirementP2PIntegration

    Dim mstrClaimType As String = ""
    Dim mstrCategory As String = ""
    Dim mstrmainline As String = ""
    Dim mstrdept As String = ""
    Dim mstrglCode As String = ""
    Dim mstrcostCenter As String = ""
    Dim mstrccy As String = ""
    Dim mdecinvAmount As Decimal = 0
    Dim mstrinvoiceNo As String = ""
    Dim mstrmaker As String = ""
    Dim mstrremarks As String = ""
    Dim mstrstatus As String = ""
    Dim mstrsupplierId As String = ""
    Dim mstrextRefNo As String = ""
    Dim mstrclaimRefNo As String = ""
    Dim IDocumentList As New List(Of DocumentList)

    Public Property claimType() As String
        Get
            Return mstrClaimType
        End Get
        Set(ByVal value As String)
            mstrClaimType = value
        End Set
    End Property

    Public Property category() As String
        Get
            Return mstrCategory
        End Get
        Set(ByVal value As String)
            mstrCategory = value
        End Set
    End Property

    Public Property mainline() As String
        Get
            Return mstrmainline
        End Get
        Set(ByVal value As String)
            mstrmainline = value
        End Set
    End Property

    Public Property dept() As String
        Get
            Return mstrdept
        End Get
        Set(ByVal value As String)
            mstrdept = value
        End Set
    End Property

    Public Property glCode() As String
        Get
            Return mstrglCode
        End Get
        Set(ByVal value As String)
            mstrglCode = value
        End Set
    End Property

    Public Property costCenter() As String
        Get
            Return mstrcostCenter
        End Get
        Set(ByVal value As String)
            mstrcostCenter = value
        End Set
    End Property

    Public Property ccy() As String
        Get
            Return mstrccy
        End Get
        Set(ByVal value As String)
            mstrccy = value
        End Set
    End Property

    Public Property invAmount() As Decimal
        Get
            Return mdecinvAmount
        End Get
        Set(ByVal value As Decimal)
            mdecinvAmount = value
        End Set
    End Property

    Public Property invoiceNo() As String
        Get
            Return mstrinvoiceNo
        End Get
        Set(ByVal value As String)
            mstrinvoiceNo = value
        End Set
    End Property

    Public Property maker() As String
        Get
            Return mstrmaker
        End Get
        Set(ByVal value As String)
            mstrmaker = value
        End Set
    End Property

    Public Property remarks() As String
        Get
            Return mstrremarks
        End Get
        Set(ByVal value As String)
            mstrremarks = value
        End Set
    End Property

    Public Property status() As String
        Get
            Return mstrstatus
        End Get
        Set(ByVal value As String)
            mstrstatus = value
        End Set
    End Property

    Public Property supplierId() As String
        Get
            Return mstrsupplierId
        End Get
        Set(ByVal value As String)
            mstrsupplierId = value
        End Set
    End Property

    Public Property extRefNo() As String
        Get
            Return mstrextRefNo
        End Get
        Set(ByVal value As String)
            mstrextRefNo = value
        End Set
    End Property

    Public Property claimRefNo() As String
        Get
            Return mstrclaimRefNo
        End Get
        Set(ByVal value As String)
            mstrclaimRefNo = value
        End Set
    End Property

    Public Property documentList() As List(Of DocumentList)
        Get
            Return IDocumentList
        End Get
        Set(ByVal value As List(Of DocumentList))
            IDocumentList = value
        End Set
    End Property

End Class

'Pinkal (11-Sep-2019) -- End


'Pinkal (16-Oct-2023) -- Start
'(A1X-1399) NMB - Post approved claims amounts to P2P.

Public Class clsP2PBudgetBalanceService

    Dim clsbudgetBalanceRequest As New budgetBalanceRequest

#Region "Properties"

    Public Property budgetBalanceRequest() As budgetBalanceRequest
        Get
            Return clsbudgetBalanceRequest
        End Get
        Set(ByVal value As budgetBalanceRequest)
            clsbudgetBalanceRequest = value
        End Set
    End Property


#End Region

End Class

<DataContract()> _
Public Class budgetBalanceRequest

    Dim mstrCompanyCode As String = Nothing
    Dim mstrGLCode As String = Nothing
    Dim mstrDimensionValue As String = Nothing
    Dim mstrStartingDate As String = Nothing
    Dim mstrBudgetModelCode As String = Nothing
    Dim mstrBudgetCycleCode As String = Nothing
    Dim mstrCalenderCode As String = Nothing

#Region "Properties"


    <DataMember()> _
    Public Property CompanyCode() As String
        Get
            Return mstrCompanyCode
        End Get
        Set(ByVal value As String)
            mstrCompanyCode = value
        End Set
    End Property

    <DataMember()> _
    Public Property GLCode() As String
        Get
            Return mstrGLCode
        End Get
        Set(ByVal value As String)
            mstrGLCode = value
        End Set
    End Property

    <DataMember()> _
   Public Property DimensionValue() As String
        Get
            Return mstrDimensionValue
        End Get
        Set(ByVal value As String)
            mstrDimensionValue = value
        End Set
    End Property

    <DataMember()> _
 Public Property StartingDate() As String
        Get
            Return mstrStartingDate
        End Get
        Set(ByVal value As String)
            mstrStartingDate = value
        End Set
    End Property

    <DataMember()> _
   Public Property BudgetModelCode() As String
        Get
            Return mstrBudgetModelCode
        End Get
        Set(ByVal value As String)
            mstrBudgetModelCode = value
        End Set
    End Property

    <DataMember()> _
  Public Property BudgetCycleCode() As String
        Get
            Return mstrBudgetCycleCode
        End Get
        Set(ByVal value As String)
            mstrBudgetCycleCode = value
        End Set
    End Property

    <DataMember()> _
  Public Property CalenderCode() As String
        Get
            Return mstrCalenderCode
        End Get
        Set(ByVal value As String)
            mstrCalenderCode = value
        End Set
    End Property

#End Region

End Class


Public Class clsP2PaymentJournalService

    Dim clspaymentRequest As New paymentRequest

#Region "Properties"

    Public Property paymentRequest() As paymentRequest
        Get
            Return clspaymentRequest
        End Get
        Set(ByVal value As paymentRequest)
            clspaymentRequest = value
        End Set
    End Property

#End Region

End Class


<DataContract()> _
Public Class paymentRequest

    Dim IPaymentJournalList As New List(Of PaymentJournals)

#Region "Properties"

    Private mstrPostingDate As String = String.Empty

    <DataMember()> _
    Public Property PostingDate() As String
        Get
            Return mstrPostingDate
        End Get
        Set(ByVal value As String)
            mstrPostingDate = value
        End Set
    End Property

    Private mstrBatchNumber As String = String.Empty
    <DataMember()> _
    Public Property BatchNumber() As String
        Get
            Return mstrBatchNumber
        End Get
        Set(ByVal value As String)
            mstrBatchNumber = value
        End Set
    End Property

    Private mstrCompanyCode As String
    <DataMember()> _
    Public Property CompanyCode() As String
        Get
            Return mstrCompanyCode
        End Get
        Set(ByVal value As String)
            mstrCompanyCode = value
        End Set
    End Property

    'Pinkal (23-Dec-2023) -- Start
    '(A1X-1555)  NMB - P2P Changes for Claim Request and Company Asset.
    Private mstrJournalName As String
    <DataMember()> _
    Public Property JournalName() As String
        Get
            Return mstrJournalName
        End Get
        Set(ByVal value As String)
            mstrJournalName = value
        End Set
    End Property
    'Pinkal (23-Dec-2023) -- End

    <DataMember()> _
    Public Property PaymentJournals() As List(Of PaymentJournals)
        Get
            Return IPaymentJournalList
        End Get
        Set(ByVal value As List(Of PaymentJournals))
            IPaymentJournalList = value
        End Set
    End Property

#End Region

End Class

<DataContract()> _
Public Class PaymentJournals

#Region "Constructor"

    Public Sub New()
    End Sub

    'Pinkal (23-Dec-2023) -- Start
    '(A1X-1555)  NMB - P2P Changes for Claim Request and Company Asset.
    Public Sub New(ByVal _CurrencyCode As String, ByVal _DebitAmt As Decimal, ByVal _CreditAmt As Decimal, ByVal _VendorAccount As String, ByVal _EmployeeCostCenter As String _
                         , ByVal _ClaimDate As String, ByVal _ClaimNumber As String, ByVal _ClaimType As String, ByVal _ClaimRemark As String, ByVal _OffsetAccountType As String _
                         , ByVal _OffsetAccount As String, ByVal _PaymMode As String)

        mstrCurrencyCode = _CurrencyCode
        mdecDebit = _DebitAmt
        mdecCredit = _CreditAmt
        mstrVendorAccount = _VendorAccount
        mstrEmployeeCostCenter = _EmployeeCostCenter
        mstrClaimDate = _ClaimDate
        mstrClaimNumber = _ClaimNumber
        mstrClaimType = _ClaimType
        mstrClaimRemark = _ClaimRemark
        mstrOffsetAccountType = _OffsetAccountType
        mstrOffsetAccount = _OffsetAccount
        mstrPaymMode = _PaymMode
    End Sub

    'Pinkal (23-Dec-2023) -- End

#End Region

#Region "Properties"


    Private mstrCurrencyCode As String = String.Empty

    <DataMember()> _
    Public Property CurrencyCode() As String
        Get
            Return mstrCurrencyCode
        End Get
        Set(ByVal value As String)
            mstrCurrencyCode = value
        End Set
    End Property

    'Pinkal (23-Dec-2023) -- Start
    '(A1X-1555)  NMB - P2P Changes for Claim Request and Company Asset.

    Private mdecDebit As Decimal = 0
    <DataMember()> _
    Public Property Debit() As Decimal
        Get
            Return mdecDebit
        End Get
        Set(ByVal value As Decimal)
            mdecDebit = value
        End Set
    End Property

    Private mdecCredit As Decimal = 0
    <DataMember()> _
    Public Property Credit() As Decimal
        Get
            Return mdecCredit
        End Get
        Set(ByVal value As Decimal)
            mdecCredit = value
        End Set
    End Property

    'Private mdecAmount As Decimal = 0
    '<DataMember()> _
    'Public Property Amount() As Decimal
    '    Get
    '        Return mdecAmount
    '    End Get
    '    Set(ByVal value As Decimal)
    '        mdecAmount = value
    '    End Set
    'End Property

    'Pinkal (23-Dec-2023) -- End

    Private mstrVendorAccount As String = String.Empty

    <DataMember()> _
    Public Property VendorAccount() As String
        Get
            Return mstrVendorAccount
        End Get
        Set(ByVal value As String)
            mstrVendorAccount = value
        End Set
    End Property

    'Pinkal (23-Dec-2023) -- Start
    '(A1X-1555)  NMB - P2P Changes for Claim Request and Company Asset.
    'Private mstrEmployeeName As String = String.Empty

    '<DataMember()> _
    'Public Property EmployeeName() As String
    '    Get
    '        Return mstrEmployeeName
    '    End Get
    '    Set(ByVal value As String)
    '        mstrEmployeeName = value
    '    End Set
    'End Property
    'Pinkal (23-Dec-2023) -- End

    Private mstrEmployeeCostCenter As String

    <DataMember()> _
    Public Property EmployeeCostCenter() As String
        Get
            Return mstrEmployeeCostCenter
        End Get
        Set(ByVal value As String)
            mstrEmployeeCostCenter = value
        End Set
    End Property

    Private mstrClaimDate As String = String.Empty

    <DataMember()> _
    Public Property ClaimDate() As String
        Get
            Return mstrClaimDate
        End Get
        Set(ByVal value As String)
            mstrClaimDate = value
        End Set
    End Property

    Private mstrClaimNumber As String = String.Empty

    <DataMember()> _
    Public Property ClaimNumber() As String
        Get
            Return mstrClaimNumber
        End Get
        Set(ByVal value As String)
            mstrClaimNumber = value
        End Set
    End Property

    Private mstrClaimType As String = String.Empty

    <DataMember()> _
    Public Property ClaimType() As String
        Get
            Return mstrClaimType
        End Get
        Set(ByVal value As String)
            mstrClaimType = value
        End Set
    End Property

    Private mstrClaimRemark As String = String.Empty

    <DataMember()> _
    Public Property ClaimRemark() As String
        Get
            Return mstrClaimRemark
        End Get
        Set(ByVal value As String)
            mstrClaimRemark = value
        End Set
    End Property


    'Pinkal (23-Dec-2023) -- Start
    '(A1X-1555)  NMB - P2P Changes for Claim Request and Company Asset.
    'Private mstrBankAccount As String = String.Empty

    '<DataMember()> _
    'Public Property BankAccount() As String
    '    Get
    '        Return mstrBankAccount
    '    End Get
    '    Set(ByVal value As String)
    '        mstrBankAccount = value
    '    End Set
    'End Property

    Private mstrOffsetAccountType As String = "Ledger"

    <DataMember()> _
    Public Property OffsetAccountType() As String
        Get
            Return mstrOffsetAccountType
        End Get
        Set(ByVal value As String)
            mstrOffsetAccountType = value
        End Set
    End Property

    Private mstrOffsetAccount As String = ""

    <DataMember()> _
    Public Property OffsetAccount() As String
        Get
            Return mstrOffsetAccount
        End Get
        Set(ByVal value As String)
            mstrOffsetAccount = value
        End Set
    End Property

    Private mstrPaymMode As String = ""

    <DataMember()> _
    Public Property PaymMode() As String
        Get
            Return mstrPaymMode
        End Get
        Set(ByVal value As String)
            mstrPaymMode = value
        End Set
    End Property

    'Pinkal (23-Dec-2023) -- End


#End Region

End Class

'Pinkal (16-Oct-2023) -- End
