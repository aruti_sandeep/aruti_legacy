﻿'************************************************************************************************************************************
'Class Name :clsclaim_request_approval_tran.vb
'Purpose    :
'Date       :07-Mar-2014
'Written By :Sandeep Sharma
'Modified   :
'************************************************************************************************************************************
Imports eZeeCommonLib
Imports System.Web
Imports System.Threading

''' <summary>
''' Purpose: 
''' Developer: Sandeep Sharma
''' </summary>
Public Class clsclaim_request_approval_tran
    Private Shared ReadOnly mstrModuleName As String = "clsclaim_request_approval_tran"
    Dim mstrMessage As String = ""
    Dim objDataOperation As clsDataOperation

#Region " Private Variables "

    Private mdtApprTran As DataTable
    Private mintClaimApprovalTranId As Integer = 0
    Private mintEmployeeID As Integer = 0
    Private mintYearId As Integer = 0
    Private mintLeaveBalanceSetting As Integer = 0
    Private mintVisibleId As Integer = -1
    Private objClaimProcessTran As New clsclaim_process_Tran
    Private mblnIsConsiderForPayroll As Boolean = False
    Private mintClaimPostingTranId As Integer = -1

    'Pinkal (16-Dec-2014) -- Start
    'Enhancement - Claim & Request For Web.
    Private mintVoidLoginemployeeunkid As Integer = 0
    'Pinkal (16-Dec-2014) -- End

    'Pinkal (22-Oct-2015) -- Start
    'Enhancement - WORKING ON AKFTZ LEAVE ISSUE ON WEB.
    Private mstrWebClientIP As String = ""
    Private mstrWebHostName As String = ""
    'Pinkal (22-Oct-2015) -- End
    Private objThread As Thread

    'S.SANDEEP |10-MAR-2022| -- START
    'ISSUE/ENHANCEMENT : OLD-580
    Private mintRebateId As Integer = CInt(enRebatePercent.RB_100)
    Private mdecRebatePercentage As Decimal = 100
    'S.SANDEEP |10-MAR-2022| -- END

    'Pinkal (16-Oct-2023) -- Start
    '(A1X-1399) NMB - Post approved claims amounts to P2P.
    Private mintCompanyId As Integer = 0
    Private mstrCompanyCode As String = ""
    'Pinkal (16-Oct-2023) -- End

    Private xDataOpr As clsDataOperation = Nothing
    Public Property _xDataOpr() As clsDataOperation
        Get
            Return xDataOpr
        End Get
        Set(ByVal value As clsDataOperation)
            xDataOpr = value
        End Set
    End Property

    Private mintLoginEmployeeunkid As Integer = 0
    Public Property _LoginEmployeeunkid() As Integer
        Get
            Return mintLoginEmployeeunkid
        End Get
        Set(ByVal value As Integer)
            mintLoginEmployeeunkid = value
        End Set
    End Property

    Private mintVoidlogingemployeeunkid As Integer = 0
    Public Property _Voidloginemployeeunkid() As Integer
        Get
            Return mintVoidlogingemployeeunkid
        End Get
        Set(ByVal value As Integer)
            mintVoidlogingemployeeunkid = value
        End Set
    End Property
    'S.SANDEEP [28-May-2018] -- END 

    ''' <summary>
    ''' Purpose: Get or Set Datatable
    ''' Modify By: Sandeep Sharma
    ''' </summary>
    Public Property _DataTable() As DataTable
        Get
            Return mdtApprTran
        End Get
        Set(ByVal value As DataTable)
            mdtApprTran = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set YearId
    ''' Modify By: Pinkal Jariwala.
    ''' </summary>
    Public Property _YearId() As Integer
        Get
            Return mintYearId
        End Get
        Set(ByVal value As Integer)
            mintYearId = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set EmployeeID
    ''' Modify By: Pinkal Jariwala.
    ''' </summary>
    Public Property _EmployeeID() As Integer
        Get
            Return mintEmployeeID
        End Get
        Set(ByVal value As Integer)
            mintEmployeeID = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set LeaveBalanceSetting
    ''' Modify By: Pinkal Jariwala.
    ''' </summary>
    Public Property _LeaveBalanceSetting() As Integer
        Get
            Return mintLeaveBalanceSetting
        End Get
        Set(ByVal value As Integer)
            mintLeaveBalanceSetting = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set VisibleId
    ''' Modify By: Pinkal Jariwala.
    ''' </summary>
    Public Property _VisiblelId() As Integer
        Get
            Return mintVisibleId
        End Get
        Set(ByVal value As Integer)
            mintVisibleId = value
        End Set
    End Property


    'Pinkal (16-Dec-2014) -- Start
    'Enhancement - Claim & Request For Web.

    ''' <summary>
    ''' Purpose: Set VoidLoginEmployeeID
    ''' Modify By: Pinkal Jariwala
    ''' </summary>
    Public Property _VoidLoginEmployeeID() As Integer
        Get
            Return mintVoidLoginemployeeunkid
        End Get
        Set(ByVal value As Integer)
            mintVoidLoginemployeeunkid = value
        End Set
    End Property

    'Pinkal (16-Dec-2014) -- End

    'Pinkal (22-Oct-2015) -- Start
    'Enhancement - WORKING ON AKFTZ LEAVE ISSUE ON WEB.

    'Public WriteOnly Property _WebClientIP() As String
    '    Set(ByVal value As String)
    '        mstrWebClientIP = value
    '    End Set
    'End Property
    'Public WriteOnly Property _WebHostName() As String
    '    Set(ByVal value As String)
    '        mstrWebHostName = value
    '    End Set
    'End Property

    'Pinkal (22-Oct-2015) -- End

    'S.SANDEEP |10-MAR-2022| -- START
    'ISSUE/ENHANCEMENT : OLD-580

    ''' <summary>
    ''' Purpose: Get or Set _RebateId
    ''' Modify By: Pinkal
    ''' </summary>
    ''' 
    Public Property _RebateId() As Integer
        Get
            Return mintRebateId
        End Get
        Set(ByVal value As Integer)
            mintRebateId = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set _RebatePercentage
    ''' Modify By: Pinkal
    ''' </summary>
    ''' 
    Public Property _RebatePercentage() As Decimal
        Get
            Return mdecRebatePercentage
        End Get
        Set(ByVal value As Decimal)
            mdecRebatePercentage = value
        End Set
    End Property

    'Pinkal (20-May-2022) -- Start
    'Optimize Global Claim Request for NMB.
    Private LstClaimEmailList As List(Of clsEmailCollection) = Nothing
    Public ReadOnly Property _ClaimEmailList() As List(Of clsEmailCollection)
        Get
            Return LstClaimEmailList
        End Get
    End Property
    'Pinkal (20-May-2022) -- End

    'Pinkal (16-Oct-2023) -- Start
    '(A1X-1399) NMB - Post approved claims amounts to P2P.

    Public Property _CompanyID() As Integer
        Get
            Return mintCompanyId
        End Get
        Set(ByVal value As Integer)
            mintCompanyId = value
        End Set
    End Property

    Public Property _CompanyCode() As String
        Get
            Return mstrCompanyCode
        End Get
        Set(ByVal value As String)
            mstrCompanyCode = value
        End Set
    End Property

    'Pinkal (16-Oct-2023) -- End


#End Region

#Region " Constructor "

    Public Sub New()
        mdtApprTran = New DataTable("List")
        Dim dCol As DataColumn
        Try
            dCol = New DataColumn("crapprovaltranunkid")
            dCol.DataType = System.Type.GetType("System.Int32")
            mdtApprTran.Columns.Add(dCol)

            dCol = New DataColumn("crtranunkid")
            dCol.DataType = System.Type.GetType("System.Int32")
            mdtApprTran.Columns.Add(dCol)

            dCol = New DataColumn("crmasterunkid")
            dCol.DataType = System.Type.GetType("System.Int32")
            mdtApprTran.Columns.Add(dCol)

            dCol = New DataColumn("expenseunkid")
            dCol.DataType = System.Type.GetType("System.Int32")
            mdtApprTran.Columns.Add(dCol)

            dCol = New DataColumn("secrouteunkid")
            dCol.DataType = System.Type.GetType("System.Int32")
            mdtApprTran.Columns.Add(dCol)

            dCol = New DataColumn("costingunkid")
            dCol.DataType = System.Type.GetType("System.Int32")
            mdtApprTran.Columns.Add(dCol)

            dCol = New DataColumn("unitprice")
            dCol.DataType = System.Type.GetType("System.Decimal")
            mdtApprTran.Columns.Add(dCol)

            dCol = New DataColumn("quantity")
            dCol.DataType = System.Type.GetType("System.Decimal")
            mdtApprTran.Columns.Add(dCol)

            dCol = New DataColumn("amount")
            dCol.DataType = System.Type.GetType("System.Decimal")
            mdtApprTran.Columns.Add(dCol)

            dCol = New DataColumn("expense_remark")
            dCol.DataType = System.Type.GetType("System.String")
            mdtApprTran.Columns.Add(dCol)

            dCol = New DataColumn("isvoid")
            dCol.DataType = System.Type.GetType("System.Boolean")
            mdtApprTran.Columns.Add(dCol)

            dCol = New DataColumn("voiduserunkid")
            dCol.DataType = System.Type.GetType("System.Int32")
            mdtApprTran.Columns.Add(dCol)

            dCol = New DataColumn("voiddatetime")
            dCol.DataType = System.Type.GetType("System.DateTime")
            dCol.DefaultValue = DBNull.Value
            mdtApprTran.Columns.Add(dCol)

            dCol = New DataColumn("voidreason")
            dCol.DataType = System.Type.GetType("System.String")
            mdtApprTran.Columns.Add(dCol)

            dCol = New DataColumn("cancelfrommoduleid")
            dCol.DataType = System.Type.GetType("System.Int32")
            mdtApprTran.Columns.Add(dCol)

            dCol = New DataColumn("iscancel")
            dCol.DataType = System.Type.GetType("System.Boolean")
            mdtApprTran.Columns.Add(dCol)

            dCol = New DataColumn("canceluserunkid")
            dCol.DataType = System.Type.GetType("System.Int32")
            mdtApprTran.Columns.Add(dCol)

            dCol = New DataColumn("cancel_remark")
            dCol.DataType = System.Type.GetType("System.String")
            mdtApprTran.Columns.Add(dCol)

            dCol = New DataColumn("cancel_datetime")
            dCol.DataType = System.Type.GetType("System.DateTime")
            dCol.DefaultValue = DBNull.Value
            mdtApprTran.Columns.Add(dCol)

            dCol = New DataColumn("loginemployeeunkid")
            dCol.DataType = System.Type.GetType("System.Int32")
            mdtApprTran.Columns.Add(dCol)

            dCol = New DataColumn("AUD")
            dCol.DataType = System.Type.GetType("System.String")
            mdtApprTran.Columns.Add(dCol)

            dCol = New DataColumn("GUID")
            dCol.DataType = System.Type.GetType("System.String")
            mdtApprTran.Columns.Add(dCol)

            dCol = New DataColumn("expense")
            dCol.DataType = System.Type.GetType("System.String")
            mdtApprTran.Columns.Add(dCol)

            dCol = New DataColumn("uom")
            dCol.DataType = System.Type.GetType("System.String")
            mdtApprTran.Columns.Add(dCol)

            dCol = New DataColumn("crprocesstranunkid")
            dCol.DataType = System.Type.GetType("System.Int32")
            mdtApprTran.Columns.Add(dCol)

            'S.SANDEEP |10-MAR-2022| -- START
            'ISSUE/ENHANCEMENT : OLD-580

            dCol = New DataColumn("airline")
            dCol.DataType = System.Type.GetType("System.String")
            dCol.DefaultValue = ""
            mdtApprTran.Columns.Add(dCol)

            dCol = New DataColumn("travelfrom")
            dCol.DataType = System.Type.GetType("System.String")
            dCol.DefaultValue = ""
            mdtApprTran.Columns.Add(dCol)

            dCol = New DataColumn("travelto")
            dCol.DataType = System.Type.GetType("System.String")
            dCol.DefaultValue = ""
            mdtApprTran.Columns.Add(dCol)

            dCol = New DataColumn("flightno")
            dCol.DataType = System.Type.GetType("System.String")
            dCol.DefaultValue = ""
            mdtApprTran.Columns.Add(dCol)

            dCol = New DataColumn("traveldate")
            dCol.DataType = System.Type.GetType("System.DateTime")
            dCol.DefaultValue = DBNull.Value
            mdtApprTran.Columns.Add(dCol)

            dCol = New DataColumn("dpndtbeneficetranunkid")
            dCol.DataType = System.Type.GetType("System.Int32")
            dCol.DefaultValue = 0
            mdtApprTran.Columns.Add(dCol)

            dCol = New DataColumn("other_person")
            dCol.DataType = System.Type.GetType("System.String")
            dCol.DefaultValue = ""
            mdtApprTran.Columns.Add(dCol)

            'S.SANDEEP |13-APR-2022| -- START
            'ISSUE/ENHANCEMENT : AC2-143
            dCol = New DataColumn("fromrouteid")
            dCol.DataType = System.Type.GetType("System.Int32")
            dCol.DefaultValue = 0
            mdtApprTran.Columns.Add(dCol)

            dCol = New DataColumn("torouteid")
            dCol.DataType = System.Type.GetType("System.Int32")
            dCol.DefaultValue = 0
            mdtApprTran.Columns.Add(dCol)
            'S.SANDEEP |13-APR-2022| -- END


            'S.SANDEEP |29-JUN-2022| -- START
            'ISSUE/ENHANCEMENT : AC2-390_V2
            dCol = New DataColumn("deductexpenseunkid")
            dCol.DataType = System.Type.GetType("System.Int32")
            dCol.DefaultValue = 0
            mdtApprTran.Columns.Add(dCol)
            'S.SANDEEP |29-JUN-2022| -- END


            'S.SANDEEP |10-MAR-2022| -- END


            'Pinkal (16-Oct-2023) -- Start
            '(A1X-1399) NMB - Post approved claims amounts to P2P.
            dCol = New DataColumn("claimrequestno")
            dCol.DataType = System.Type.GetType("System.String")
            dCol.DefaultValue = ""
            mdtApprTran.Columns.Add(dCol)

            dCol = New DataColumn("claim_remark")
            dCol.DataType = System.Type.GetType("System.String")
            dCol.DefaultValue = ""
            mdtApprTran.Columns.Add(dCol)
            'Pinkal (16-Oct-2023) -- End

            'Pinkal (23-Dec-2023) -- Start
            '(A1X-1555)  NMB - P2P Changes for Claim Request and Company Asset.
            dCol = New DataColumn("journal")
            dCol.DataType = System.Type.GetType("System.String")
            dCol.DefaultValue = ""
            mdtApprTran.Columns.Add(dCol)

            dCol = New DataColumn("paymentmode")
            dCol.DataType = System.Type.GetType("System.String")
            dCol.DefaultValue = ""
            mdtApprTran.Columns.Add(dCol)

            dCol = New DataColumn("isdebit")
            dCol.DataType = System.Type.GetType("System.Boolean")
            dCol.DefaultValue = False
            mdtApprTran.Columns.Add(dCol)

            dCol = New DataColumn("iscredit")
            dCol.DataType = System.Type.GetType("System.Boolean")
            dCol.DefaultValue = False
            mdtApprTran.Columns.Add(dCol)
            'Pinkal (23-Dec-2023) -- End


        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "New", mstrModuleName)
        End Try
    End Sub

#End Region

#Region " Public & Private Methods "


    'Pinkal (24-Aug-2015) -- Start
    'Enhancement - WORKING ON ACTIVE EMPLOYEE CONDITION.

    '' <summary>
    '' Modify By: Pinkal Jariwala
    '' </summary>
    '' <purpose> Assign all Property variable </purpose>
    '' 
    'Public Function GetApproverExpesneList(ByVal strTableName As String, ByVal IsDistinct As Boolean, ByVal blnPaymentApprovalwithLeaveApproval As Boolean _
    '                                                         , ByVal intExpCategroryId As Integer, ByVal isFromApprovalList As Boolean, Optional ByVal blnOnlyActive As Boolean = True, Optional ByVal intApproverID As Integer = -1 _
    '                                                        , Optional ByVal intUserID As Integer = -1, Optional ByVal mstFilter As String = "", Optional ByVal intClaimMstId As Integer = -1) As DataSet
    '    Dim dsList As DataSet = Nothing
    '    Dim strQ As String = ""
    '    Dim exForce As Exception
    '    Dim objDataOperation As New clsDataOperation

    '    Try


    '        'Pinkal (22-Jun-2015) -- Start
    '        'Enhancement - CHANGES IN CLAIM & REQUEST AS PER MR.ANDREW REQUIREMENT.

    '        'If blnPaymentApprovalwithLeaveApproval And intExpCategroryId <> enExpenseType.EXP_LEAVE Then
    '        If intExpCategroryId <> enExpenseType.EXP_LEAVE Then

    '        strQ = "SELECT  "

    '        If IsDistinct Then
    '            strQ &= " DISTINCT "
    '        Else
    '            strQ &= " '' AS AUD,'' AS GUID , "
    '        End If

    '        strQ &= " Cast (0 as bit) As Ischeck " & _
    '                    ",  cmclaim_request_master.crmasterunkid " & _
    '                    ", cmclaim_request_master.claimrequestno " & _
    '                    ", h1.employeecode " & _
    '                    ", ISNULL(h1.firstname,'') + ' ' + isnull(h1.surname,'') as employeename " & _
    '                    ", ISNULL(h2.firstname,'') + ' ' + isnull(h2.surname,'') as approvername " & _
    '                    ", cmapproverlevel_master.crlevelname " & _
    '                    ", cmapproverlevel_master.crpriority " & _
    '                    ", cmapproverlevel_master.crlevelunkid " & _
    '                    ", cmclaim_request_master.employeeunkid " & _
    '                    ", cmclaim_request_master.expensetypeid " & _
    '                    ", cmclaim_approval_tran.statusunkid " & _
    '                    ", cmclaim_approval_tran.visibleid " & _
    '                    ", cmclaim_approval_tran.crapproverunkid " & _
    '                    ", cmclaim_approval_tran.approveremployeeunkid " & _
    '                    ", ISNULL(hrapprover_usermapping.userunkid,-1) as mapuserunkid " & _
    '                    ", '' AS period " & _
    '                    ", CONVERT(CHAR(8),cmclaim_request_master.transactiondate,112) AS tdate " & _
    '                    ", CASE WHEN cmclaim_request_master.expensetypeid = '" & enExpenseType.EXP_LEAVE & "' THEN @Leave " & _
    '                    "         WHEN cmclaim_request_master.expensetypeid = '" & enExpenseType.EXP_MEDICAL & "' THEN @Medical " & _
    '                    "         WHEN cmclaim_request_master.expensetypeid = '" & enExpenseType.EXP_MISCELLANEOUS & "' THEN @Miscellaneous " & _
    '                    "         WHEN cmclaim_request_master.expensetypeid = '" & enExpenseType.EXP_TRAINING & "' THEN @Training END AS expensetype " & _
    '                    ", CASE WHEN cmclaim_approval_tran.statusunkid = 1 then @Approve " & _
    '                    "         WHEN cmclaim_approval_tran.statusunkid = 2 then @Pending  " & _
    '                    "         WHEN cmclaim_approval_tran.statusunkid = 3 then @Reject  " & _
    '                    "         WHEN cmclaim_approval_tran.statusunkid = 4 then @ReSchedule " & _
    '                    "         WHEN cmclaim_approval_tran.statusunkid = 6 then @Cancel " & _
    '                    "         WHEN cmclaim_approval_tran.statusunkid = 7 then @Issued END as status "
    '        If IsDistinct Then
    '            strQ &= ", ISNULL((SELECT SUM(ct.amount) From cmclaim_approval_tran  ct WHERE ct.crmasterunkid = cmclaim_approval_tran.crmasterunkid AND ct.approveremployeeunkid = cmclaim_approval_tran.approveremployeeunkid AND ct.crapproverunkid = cmclaim_approval_tran.crapproverunkid AND ct.isvoid = 0 AND cmclaim_approval_tran.isvoid = 0 AND ct.iscancel = 0),0.00 ) amount "
    '        Else
    '            strQ &= ", ISNULL(cmclaim_approval_tran.crapprovaltranunkid ,0) AS crapprovaltranunkid " & _
    '                        ", ISNULL(cmclaim_approval_tran.crtranunkid ,0) AS crtranunkid " & _
    '                        ", ISNULL(cmclaim_approval_tran.costingunkid ,0) AS costingunkid " & _
    '                        ", ISNULL(cmclaim_approval_tran.expenseunkid,0) AS expenseunkid " & _
    '                        ", ISNULL(cmclaim_approval_tran.secrouteunkid,0) AS secrouteunkid " & _
    '                        ", ISNULL(cmexpense_master.name,'') AS expense " & _
    '                        ", ISNULL(cmexpense_master.isaccrue,0) AS isaccrue " & _
    '                        ", ISNULL(cmexpense_master.isleaveencashment,0) AS isleaveencashment " & _
    '                        ", ISNULL(cmexpense_master.isconsiderforpayroll,0) AS isconsiderforpayroll " & _
    '                        ", ISNULL(cfcommon_master.name,'') AS sector " & _
    '                        ", ISNULL(cmexpense_costing_tran.amount,0.00) AS costing_amount " & _
    '                        ", CASE WHEN cmexpense_master.uomunkid = 1 THEN @Qty WHEN cmexpense_master.uomunkid = 2 THEN @Amt END AS uom " & _
    '                        ", ISNULL(cmclaim_approval_tran.unitprice,0.00 ) unitprice " & _
    '                        ", ISNULL(cmclaim_approval_tran.quantity,0.00 ) quantity " & _
    '                        ", ISNULL(cmclaim_approval_tran.amount,0.00 ) amount " & _
    '                        ", ISNULL(cmclaim_approval_tran.expense_remark,'') expense_remark " & _
    '                        ", ISNULL(cmclaim_approval_tran.cancel_remark,'') AS cancel_remark " & _
    '                        ", ISNULL(cmclaim_approval_tran.canceluserunkid,0) AS canceluserunkid " & _
    '                        ", cmclaim_approval_tran.cancel_datetime "
    '        End If

    '            strQ &= ", cmclaim_approval_tran.iscancel " & _
    '                        ", ISNULL(cmclaim_approval_tran.isvoid,0) AS isvoid " & _
    '                        ", ISNULL(cmclaim_approval_tran.voiduserunkid,0) AS voiduserunkid " & _
    '                        ", ISNULL(cmclaim_approval_tran.voidloginemployeeunkid,-1) AS voidloginemployeeunkid " & _
    '                        ", cmclaim_approval_tran.voiddatetime " & _
    '                        ", ISNULL(cmclaim_approval_tran.voidreason,'') AS voidreason " & _
    '                        ",ISNULL(h1.sectiongroupunkid,0) AS sectiongroupunkid " & _
    '                        ",ISNULL(h1.unitgroupunkid,0) AS unitgroupunkid " & _
    '                        ",ISNULL(h1.teamunkid,0) AS teamunkid " & _
    '                        ",ISNULL(h1.stationunkid,0) AS stationunkid " & _
    '                        ",ISNULL(h1.deptgroupunkid,0) AS deptgroupunkid " & _
    '                        ",ISNULL(h1.departmentunkid,0) AS departmentunkid " & _
    '                        ",ISNULL(h1.sectionunkid,0) AS sectionunkid " & _
    '                        ",ISNULL(h1.unitunkid,0) AS unitunkid " & _
    '                        ",ISNULL(h1.jobunkid,0) AS jobunkid " & _
    '                        ",ISNULL(h1.classgroupunkid,0) AS classgroupunkid " & _
    '                        ",ISNULL(h1.classunkid,0) AS classunkid " & _
    '                        ",ISNULL(h1.jobgroupunkid,0) AS jobgroupunkid " & _
    '                        ",ISNULL(h1.gradegroupunkid,0) AS gradegroupunkid " & _
    '                        ",ISNULL(h1.gradeunkid,0) AS gradeunkid " & _
    '                        ",ISNULL(h1.gradelevelunkid,0) AS gradelevelunkid " & _
    '                        ",CONVERT(CHAR(8),cmclaim_approval_tran.approvaldate, 112) AS approvaldate" & _
    '                        "   FROM cmclaim_approval_tran " & _
    '                        "	JOIN cmclaim_request_master ON cmclaim_request_master.crmasterunkid = cmclaim_approval_tran.crmasterunkid AND cmclaim_request_master.isvoid = 0 " & _
    '                        "   LEFT JOIN hremployee_master h1 on h1.employeeunkid = cmclaim_request_master.employeeunkid  " & _
    '                        "   LEFT JOIN hremployee_master h2 on h2.employeeunkid = cmclaim_approval_tran.approveremployeeunkid  " & _
    '                        "   LEFT JOIN cmexpapprover_master ON cmexpapprover_master.employeeunkid = cmclaim_approval_tran.approveremployeeunkid  AND cmexpapprover_master.crapproverunkid = cmclaim_approval_tran.crapproverunkid " & _
    '                        "   LEFT JOIN cmapproverlevel_master ON cmapproverlevel_master.crlevelunkid = cmexpapprover_master.crlevelunkid	" & _
    '                        "   LEFT JOIN hrapprover_usermapping ON hrapprover_usermapping.approverunkid = cmexpapprover_master.crapproverunkid " & " AND hrapprover_usermapping.usertypeid = " & enUserType.crApprover
    '            'Shani(08-Aug-2015) -- [approvaldate]
    '            If IsDistinct = False Then
    '                strQ &= " JOIN cmexpense_master ON cmexpense_master.expenseunkid = cmclaim_approval_tran.expenseunkid " & _
    '                             " LEFT JOIN cfcommon_master ON cfcommon_master.masterunkid = cmclaim_approval_tran.secrouteunkid AND cfcommon_master.mastertype = " & clsCommon_Master.enCommonMaster.SECTOR_ROUTE & _
    '                             " LEFT JOIN cmexpense_costing_tran ON cmexpense_costing_tran.secrouteunkid = cmclaim_approval_tran.secrouteunkid AND cmexpense_costing_tran.costingunkid = cmclaim_approval_tran.costingunkid  "
    '            End If

    '            If intUserID <= 0 Then
    '                If User._Object._Userunkid > 1 Then
    '                    strQ &= " AND hrapprover_usermapping.userunkid = " & User._Object._Userunkid
    '                End If
    '            Else
    '                strQ &= " AND hrapprover_usermapping.userunkid = " & intUserID
    '            End If

    '            strQ &= " WHERE cmclaim_request_master.frommoduleid =  " & enExpFromModuleID.FROM_EXPENSE

    '            If blnOnlyActive Then
    '                strQ &= " AND cmclaim_approval_tran.isvoid = 0 "
    '            End If

    '            If intApproverID > 0 Then
    '                strQ &= " AND cmclaim_approval_tran.crapproverunkid = " & intApproverID
    '            End If

    '            If mstFilter.Trim.Length > 0 Then
    '                strQ &= " AND " & mstFilter
    '            End If

    '            If intClaimMstId > 0 Then
    '                strQ &= " AND cmclaim_request_master.crmasterunkid = " & intClaimMstId
    '            End If

    '        End If

    '        Select Case intExpCategroryId

    '            Case enExpenseType.EXP_LEAVE

    '                'Pinkal (22-Jun-2015) -- Start
    '                'Enhancement - CHANGES IN CLAIM & REQUEST AS PER MR.ANDREW REQUIREMENT.

    '                If blnPaymentApprovalwithLeaveApproval AndAlso intExpCategroryId <> enExpenseType.EXP_LEAVE Then
    '                strQ &= " UNION "
    '                End If
    '                'Pinkal (22-Jun-2015) -- End

    '                strQ &= "SELECT  "

    '                If IsDistinct Then
    '                    strQ &= " DISTINCT "
    '                Else
    '                    strQ &= " '' AS AUD,'' AS GUID , "
    '                End If

    '                strQ &= " Cast (0 as bit) As Ischeck " & _
    '                            ",  cmclaim_request_master.crmasterunkid " & _
    '                            ", cmclaim_request_master.claimrequestno " & _
    '                            ", h1.employeecode " & _
    '                            ", ISNULL(h1.firstname,'') + ' ' + isnull(h1.surname,'') as employeename " & _
    '                            ", ISNULL(h2.firstname,'') + ' ' + isnull(h2.surname,'') as approvername "

    '                If blnPaymentApprovalwithLeaveApproval Then
    '                    strQ &= ", lvapproverlevel_master.levelname AS crlevelname " & _
    '                                ", lvapproverlevel_master.priority AS crpriority " & _
    '                                ", lvapproverlevel_master.levelunkid AS crlevelunkid"
    '                Else
    '                    strQ &= ", cmapproverlevel_master.crlevelname " & _
    '                                ", cmapproverlevel_master.crpriority " & _
    '                                ", cmapproverlevel_master.crlevelunkid "
    '                End If

    '                strQ &= ", cmclaim_request_master.employeeunkid " & _
    '                        ", cmclaim_request_master.expensetypeid " & _
    '                        ", cmclaim_approval_tran.statusunkid " & _
    '                        ", cmclaim_approval_tran.visibleid " & _
    '                        ", cmclaim_approval_tran.crapproverunkid " & _
    '                        ", cmclaim_approval_tran.approveremployeeunkid " & _
    '                        ", ISNULL(hrapprover_usermapping.userunkid,-1) as mapuserunkid " & _
    '                        ", '' AS period " & _
    '                        ", CONVERT(CHAR(8),cmclaim_request_master.transactiondate,112) AS tdate " & _
    '                        ", CASE WHEN cmclaim_request_master.expensetypeid = '" & enExpenseType.EXP_LEAVE & "' THEN @Leave " & _
    '                        "         WHEN cmclaim_request_master.expensetypeid = '" & enExpenseType.EXP_MEDICAL & "' THEN @Medical " & _
    '                        "         WHEN cmclaim_request_master.expensetypeid = '" & enExpenseType.EXP_MISCELLANEOUS & "' THEN @Miscellaneous " & _
    '                        "         WHEN cmclaim_request_master.expensetypeid = '" & enExpenseType.EXP_TRAINING & "' THEN @Training END AS expensetype " & _
    '                        ", CASE WHEN cmclaim_approval_tran.statusunkid = 1 then @Approve " & _
    '                        "         WHEN cmclaim_approval_tran.statusunkid = 2 then @Pending  " & _
    '                        "         WHEN cmclaim_approval_tran.statusunkid = 3 then @Reject  " & _
    '                        "         WHEN cmclaim_approval_tran.statusunkid = 4 then @ReSchedule " & _
    '                        "         WHEN cmclaim_approval_tran.statusunkid = 6 then @Cancel " & _
    '                        "         WHEN cmclaim_approval_tran.statusunkid = 7 then @Issued END as status "

    '                If IsDistinct Then
    '                    strQ &= ", ISNULL((SELECT SUM(ct.amount) From cmclaim_approval_tran  ct WHERE ct.crmasterunkid = cmclaim_approval_tran.crmasterunkid AND ct.approveremployeeunkid = approveremployeeunkid AND ct.crapproverunkid = cmclaim_approval_tran.crapproverunkid AND ct.isvoid = 0 AND cmclaim_approval_tran.isvoid = 0 AND ct.iscancel = 0),0.00 ) amount "
    '                Else
    '                    strQ &= ", ISNULL(cmclaim_approval_tran.crapprovaltranunkid ,0) AS crapprovaltranunkid " & _
    '                                ", ISNULL(cmclaim_approval_tran.crtranunkid ,0) AS crtranunkid " & _
    '                                ", ISNULL(cmclaim_approval_tran.costingunkid ,0) AS costingunkid " & _
    '                                ", ISNULL(cmclaim_approval_tran.expenseunkid,0) AS expenseunkid " & _
    '                                ", ISNULL(cmclaim_approval_tran.secrouteunkid,0) AS secrouteunkid " & _
    '                                ", ISNULL(cmexpense_master.name,'') AS expense " & _
    '                                ", ISNULL(cmexpense_master.isaccrue,0) AS isaccrue " & _
    '                                ", ISNULL(cmexpense_master.isleaveencashment,0) AS isleaveencashment " & _
    '                                ", ISNULL(cmexpense_master.isconsiderforpayroll,0) AS isconsiderforpayroll " & _
    '                                ", ISNULL(cfcommon_master.name,'') AS sector " & _
    '                                ", ISNULL(cmexpense_costing_tran.amount,0.00) AS costing_amount " & _
    '                                ", CASE WHEN cmexpense_master.uomunkid = 1 THEN @Qty WHEN cmexpense_master.uomunkid = 2 THEN @Amt END AS uom " & _
    '                                ", ISNULL(cmclaim_approval_tran.unitprice,0.00 ) unitprice " & _
    '                                ", ISNULL(cmclaim_approval_tran.quantity,0.00 ) quantity " & _
    '                                ", ISNULL(cmclaim_approval_tran.amount,0.00 ) amount " & _
    '                                ", ISNULL(cmclaim_approval_tran.expense_remark,'') expense_remark " & _
    '                                ", ISNULL(cmclaim_approval_tran.cancel_remark,'') AS cancel_remark " & _
    '                                ", ISNULL(cmclaim_approval_tran.canceluserunkid,0) AS canceluserunkid " & _
    '                                ", cmclaim_approval_tran.cancel_datetime "
    '                End If

    '                strQ &= ", cmclaim_approval_tran.iscancel " & _
    '                                ", ISNULL(cmclaim_approval_tran.isvoid,0) AS isvoid " & _
    '                                ", ISNULL(cmclaim_approval_tran.voiduserunkid,0) AS voiduserunkid " & _
    '                                ", ISNULL(cmclaim_approval_tran.voidloginemployeeunkid,-1) AS voidloginemployeeunkid " & _
    '                                ", cmclaim_approval_tran.voiddatetime " & _
    '                                ", ISNULL(cmclaim_approval_tran.voidreason,'') AS voidreason " & _
    '                                ",ISNULL(h1.sectiongroupunkid,0) AS sectiongroupunkid " & _
    '                                ",ISNULL(h1.unitgroupunkid,0) AS unitgroupunkid " & _
    '                                ",ISNULL(h1.teamunkid,0) AS teamunkid " & _
    '                                ",ISNULL(h1.stationunkid,0) AS stationunkid " & _
    '                                ",ISNULL(h1.deptgroupunkid,0) AS deptgroupunkid " & _
    '                                ",ISNULL(h1.departmentunkid,0) AS departmentunkid " & _
    '                                ",ISNULL(h1.sectionunkid,0) AS sectionunkid " & _
    '                                ",ISNULL(h1.unitunkid,0) AS unitunkid " & _
    '                                ",ISNULL(h1.jobunkid,0) AS jobunkid " & _
    '                                ",ISNULL(h1.classgroupunkid,0) AS classgroupunkid " & _
    '                                ",ISNULL(h1.classunkid,0) AS classunkid " & _
    '                                ",ISNULL(h1.jobgroupunkid,0) AS jobgroupunkid " & _
    '                                ",ISNULL(h1.gradegroupunkid,0) AS gradegroupunkid " & _
    '                                ",ISNULL(h1.gradeunkid,0) AS gradeunkid " & _
    '                                ",ISNULL(h1.gradelevelunkid,0) AS gradelevelunkid " & _
    '                                ",CONVERT(CHAR(8),cmclaim_approval_tran.approvaldate,112) AS approvaldate " & _
    '                                "   FROM cmclaim_approval_tran " & _
    '                                "	JOIN cmclaim_request_master ON cmclaim_request_master.crmasterunkid = cmclaim_approval_tran.crmasterunkid AND cmclaim_request_master.isvoid = 0 " & _
    '                                "   LEFT JOIN hremployee_master h1 on h1.employeeunkid = cmclaim_request_master.employeeunkid  " & _
    '                                "   LEFT JOIN hremployee_master h2 on h2.employeeunkid = cmclaim_approval_tran.approveremployeeunkid  "
    '                'Shani(08-Aug-2015) -- [approvaldate]
    '                If blnPaymentApprovalwithLeaveApproval Then
    '                    strQ &= "   LEFT JOIN lvleaveapprover_master ON lvleaveapprover_master.leaveapproverunkid = cmclaim_approval_tran.approveremployeeunkid  AND lvleaveapprover_master.approverunkid = cmclaim_approval_tran.crapproverunkid " & _
    '                                "   LEFT JOIN lvapproverlevel_master ON lvapproverlevel_master.levelunkid = lvleaveapprover_master.levelunkid	" & _
    '                                "   LEFT JOIN hrapprover_usermapping ON hrapprover_usermapping.approverunkid = lvleaveapprover_master.approverunkid " & " AND hrapprover_usermapping.usertypeid = " & enUserType.Approver
    '                Else
    '                    strQ &= "  LEFT JOIN cmexpapprover_master ON cmexpapprover_master.employeeunkid = cmclaim_approval_tran.approveremployeeunkid  AND cmexpapprover_master.crapproverunkid = cmclaim_approval_tran.crapproverunkid " & _
    '                                "   LEFT JOIN cmapproverlevel_master ON cmapproverlevel_master.crlevelunkid = cmexpapprover_master.crlevelunkid	" & _
    '                                "   LEFT JOIN hrapprover_usermapping ON hrapprover_usermapping.approverunkid = cmexpapprover_master.crapproverunkid " & " AND hrapprover_usermapping.usertypeid = " & enUserType.crApprover
    '                End If


    '                If IsDistinct = False Then
    '                    strQ &= " JOIN cmexpense_master ON cmexpense_master.expenseunkid = cmclaim_approval_tran.expenseunkid " & _
    '                                " LEFT JOIN cfcommon_master ON cfcommon_master.masterunkid = cmclaim_approval_tran.secrouteunkid AND cfcommon_master.mastertype = " & clsCommon_Master.enCommonMaster.SECTOR_ROUTE & _
    '                         " LEFT JOIN cmexpense_costing_tran ON cmexpense_costing_tran.secrouteunkid = cmclaim_approval_tran.secrouteunkid AND cmexpense_costing_tran.costingunkid = cmclaim_approval_tran.costingunkid  "

    '                End If

    '                If intUserID <= 0 Then
    '                    If User._Object._Userunkid > 1 Then
    '                        strQ &= " AND hrapprover_usermapping.userunkid = " & User._Object._Userunkid
    '                    End If
    '                Else
    '                    strQ &= " AND hrapprover_usermapping.userunkid = " & intUserID
    '                End If


    '                'Pinkal (22-Jun-2015) -- Start
    '                'Enhancement - CHANGES IN CLAIM & REQUEST AS PER MR.ANDREW REQUIREMENT.
    '                'strQ &= " WHERE cmclaim_request_master.frommoduleid =  " & enExpFromModuleID.FROM_LEAVE
    '                If isFromApprovalList Then
    '                    If blnPaymentApprovalwithLeaveApproval Then
    '                        strQ &= " WHERE cmclaim_request_master.frommoduleid =  " & enExpFromModuleID.FROM_EXPENSE
    '                    Else
    '                        strQ &= " WHERE cmclaim_request_master.modulerefunkid =  " & enModuleReference.Leave
    '                    End If
    '                Else
    '                    strQ &= " WHERE cmclaim_request_master.modulerefunkid =  " & enModuleReference.Leave
    '                End If
    '                'Pinkal (22-Jun-2015) -- End


    '                If blnOnlyActive Then
    '                    strQ &= " AND cmclaim_approval_tran.isvoid = 0 "
    '                End If

    '                If intApproverID > 0 Then
    '                    strQ &= " AND cmclaim_approval_tran.crapproverunkid = " & intApproverID
    '                End If

    '                If mstFilter.Trim.Length > 0 Then
    '                    strQ &= " AND " & mstFilter
    '                End If

    '                If intClaimMstId > 0 Then
    '                    strQ &= " AND cmclaim_request_master.crmasterunkid = " & intClaimMstId
    '                End If

    '        End Select

    '        If blnPaymentApprovalwithLeaveApproval Then
    '            If intExpCategroryId <> enExpenseType.EXP_LEAVE Then
    '                strQ &= " ORDER BY cmclaim_request_master.crmasterunkid,cmapproverlevel_master.crpriority "
    '            Else
    '                strQ &= " ORDER BY cmclaim_request_master.crmasterunkid, lvapproverlevel_master.priority "
    '            End If
    '        Else
    '            strQ &= " ORDER BY cmclaim_request_master.crmasterunkid,cmapproverlevel_master.crpriority "
    '        End If

    '        'Pinkal (22-Jun-2015) -- End

    '        objDataOperation.ClearParameters()
    '        objDataOperation.AddParameter("@Leave", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage("clsExpCommonMethods", 2, "Leave"))
    '        objDataOperation.AddParameter("@Medical", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage("clsExpCommonMethods", 3, "Medical"))
    '        objDataOperation.AddParameter("@Training", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage("clsExpCommonMethods", 4, "Training"))
    '        objDataOperation.AddParameter("@Miscellaneous", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage("clsExpCommonMethods", 8, "Miscellaneous"))

    '        objDataOperation.AddParameter("@Approve", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage("clsMasterData", 110, "Approved"))
    '        objDataOperation.AddParameter("@Pending", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage("clsMasterData", 111, "Pending"))
    '        objDataOperation.AddParameter("@Reject", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage("clsMasterData", 112, "Rejected"))
    '        objDataOperation.AddParameter("@ReSchedule", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage("clsMasterData", 113, "Re-Scheduled"))
    '        objDataOperation.AddParameter("@Cancel", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage("clsMasterData", 115, "Cancelled"))
    '        objDataOperation.AddParameter("@Issued", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage("clsMasterData", 277, "Issued"))

    '        If IsDistinct = False Then
    '            objDataOperation.AddParameter("@Qty", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage("clsExpCommonMethods", 6, "Quantity"))
    '            objDataOperation.AddParameter("@Amt", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage("clsExpCommonMethods", 7, "Amount"))
    '        End If

    '        dsList = objDataOperation.ExecQuery(strQ, strTableName)

    '        If objDataOperation.ErrorMessage <> "" Then
    '            exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
    '            Throw exForce
    '        End If

    '    Catch ex As Exception
    '        Throw New Exception(ex.Message & "; Procedure Name: GetApproverExpesneList; Module Name: " & mstrModuleName)
    '    Finally
    '        exForce = Nothing
    '        If dsList IsNot Nothing Then dsList.Dispose()
    '        objDataOperation = Nothing
    '    End Try
    '    Return dsList
    'End Function

    Public Function GetApproverExpesneList(ByVal strTableName As String, ByVal IsDistinct As Boolean, ByVal blnPaymentApprovalwithLeaveApproval As Boolean _
                                                              , ByVal xDatabaseName As String, ByVal xUserUnkid As Integer, ByVal strEmployeeAsOnDate As String _
                                                              , ByVal intExpCategroryId As Integer, ByVal isFromApprovalList As Boolean _
                                                              , Optional ByVal blnOnlyActive As Boolean = True, Optional ByVal intApproverID As Integer = -1 _
                                                              , Optional ByVal mstFilter As String = "", Optional ByVal intClaimMstId As Integer = -1, Optional ByVal objDoOperation As clsDataOperation = Nothing) As DataSet
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        If objDoOperation Is Nothing Then
            objDataOperation = New clsDataOperation
        Else
            objDataOperation = objDoOperation
        End If
        objDataOperation.ClearParameters()


        Try


            Dim dsExternalCompany As New DataSet
            Dim StrExternalQry As String : StrExternalQry = ""


            Dim xAdvanceJoinQry As String = ""
            Call GetAdvanceFilterQry(xAdvanceJoinQry, eZeeDate.convertDate(strEmployeeAsOnDate), xDatabaseName)

            If intExpCategroryId <> enExpenseType.EXP_LEAVE Then

                StrExternalQry = "SELECT  "

                If IsDistinct Then
                    StrExternalQry &= " DISTINCT "
                Else
                    StrExternalQry &= " '' AS AUD,'' AS GUID , "
                End If

                StrExternalQry &= " Cast (0 as bit) As Ischeck " & _
                        ",  cmclaim_request_master.crmasterunkid " & _
                        ", cmclaim_request_master.claimrequestno " & _
                        ", h1.employeecode " & _
                        ", ISNULL(h1.firstname,'') + ' ' + isnull(h1.surname,'') as employeename " & _
                        ", h1.employeecode + ' - ' + ISNULL(h1.firstname,'') + ' ' + isnull(h1.surname,'') as EmpCodeName " & _
                            ", #APPR_NAME# as approvername " & _
                        ", #APPR_EMAIL_VALUE# AS approveremail " & _
                        ", cmapproverlevel_master.crlevelname " & _
                        ", cmapproverlevel_master.crpriority " & _
                        ", cmapproverlevel_master.crlevelunkid " & _
                            ", cmexpapprover_master.isexternalapprover " & _
                        ", cmclaim_request_master.employeeunkid " & _
                        ", cmclaim_request_master.expensetypeid " & _
                        ", cmclaim_approval_tran.statusunkid " & _
                        ", cmclaim_request_master.statusunkid As crstatusunkid " & _
                        ", cmclaim_approval_tran.visibleid " & _
                        ", cmclaim_approval_tran.crapproverunkid " & _
                        ", cmclaim_approval_tran.approveremployeeunkid " & _
                        ", ISNULL(hrapprover_usermapping.userunkid,-1) as mapuserunkid " & _
                        ", '' AS period " & _
                        ", CONVERT(CHAR(8),cmclaim_request_master.transactiondate,112) AS tdate " & _
                        ", CASE WHEN cmclaim_request_master.expensetypeid = '" & enExpenseType.EXP_LEAVE & "' THEN @Leave " & _
                        "         WHEN cmclaim_request_master.expensetypeid = '" & enExpenseType.EXP_MEDICAL & "' THEN @Medical " & _
                        "         WHEN cmclaim_request_master.expensetypeid = '" & enExpenseType.EXP_MISCELLANEOUS & "' THEN @Miscellaneous " & _
                        "         WHEN cmclaim_request_master.expensetypeid = '" & enExpenseType.EXP_TRAINING & "' THEN @Training  " & _
                        "         WHEN cmclaim_request_master.expensetypeid = '" & enExpenseType.EXP_IMPREST & "' THEN @Imprest " & _
                        "         WHEN cmclaim_request_master.expensetypeid = '" & enExpenseType.EXP_REBATE_PRIVILEGE & "' THEN @EXP_REBATE_PRIVILEGE " & _
                        "         WHEN cmclaim_request_master.expensetypeid = '" & enExpenseType.EXP_REBATE_DUTY & "' THEN @EXP_REBATE_DUTY " & _
                        "         WHEN cmclaim_request_master.expensetypeid = '" & enExpenseType.EXP_REBATE_PRIVILEGE_DPNDT & "' THEN @EXP_REBATE_PRIVILEGE_DPNDT " & _
                        "  END AS expensetype " & _
                        ", CASE WHEN cmclaim_approval_tran.statusunkid = 1 then @Approve " & _
                        "         WHEN cmclaim_approval_tran.statusunkid = 2 then @Pending  " & _
                        "         WHEN cmclaim_approval_tran.statusunkid = 3 then @Reject  " & _
                        "         WHEN cmclaim_approval_tran.statusunkid = 4 then @ReSchedule " & _
                        "         WHEN cmclaim_approval_tran.statusunkid = 6 then @Cancel " & _
                        "         WHEN cmclaim_approval_tran.statusunkid = 7 then @Issued END as status "
                'S.SANDEEP |10-MAR-2022| -- START (OLD-580) {EXP_REBATE_PRIVILEGE,EXP_REBATE_DUTY} -- END

                'Pinkal (11-Sep-2019) -- 'Enhancement NMB - Working On Claim Retirement for NMB.[" WHEN cmclaim_request_master.expensetypeid = '" & enExpenseType.EXP_IMPREST & "' THEN @Imprest END AS expensetype " & _]

                If IsDistinct Then
                    StrExternalQry &= ", ISNULL((SELECT SUM(ct.amount) From cmclaim_approval_tran  ct WHERE ct.crmasterunkid = cmclaim_approval_tran.crmasterunkid AND ct.approveremployeeunkid = cmclaim_approval_tran.approveremployeeunkid AND ct.crapproverunkid = cmclaim_approval_tran.crapproverunkid AND ct.isvoid = 0 AND cmclaim_approval_tran.isvoid = 0 AND ct.iscancel = 0),0.00 ) amount " & _
                                               ", App.AppprovalStatus "
                Else
                    'S.SANDEEP |13-APR-2022| -- START
                    'ISSUE/ENHANCEMENT : AC2-143
                    'StrExternalQry &= ", ISNULL(cmclaim_approval_tran.crapprovaltranunkid ,0) AS crapprovaltranunkid " & _
                    '                  ", ISNULL(cmclaim_approval_tran.crtranunkid ,0) AS crtranunkid " & _
                    '                  ", ISNULL(cmclaim_approval_tran.costingunkid ,0) AS costingunkid " & _
                    '                  ", ISNULL(cmclaim_approval_tran.expenseunkid,0) AS expenseunkid " & _
                    '                  ", ISNULL(cmclaim_approval_tran.secrouteunkid,0) AS secrouteunkid " & _
                    '                  ", ISNULL(cmexpense_master.name,'') AS expense " & _
                    '                  ", ISNULL(cmexpense_master.isaccrue,0) AS isaccrue " & _
                    '                  ", ISNULL(cmexpense_master.isleaveencashment,0) AS isleaveencashment " & _
                    '                  ", ISNULL(cmexpense_master.isconsiderforpayroll,0) AS isconsiderforpayroll " & _
                    '                  ", ISNULL(cfcommon_master.name,'') AS sector " & _
                    '                  ", ISNULL(cmexpense_costing_tran.amount,0.00) AS costing_amount " & _
                    '                  ", CASE WHEN cmexpense_master.uomunkid = 1 THEN @Qty WHEN cmexpense_master.uomunkid = 2 THEN @Amt END AS uom " & _
                    '                  ", ISNULL(cmclaim_approval_tran.unitprice,0.00 ) unitprice " & _
                    '                  ", ISNULL(cmclaim_approval_tran.quantity,0.00 ) quantity " & _
                    '                  ", ISNULL(cmclaim_approval_tran.amount,0.00 ) amount " & _
                    '                  ", ISNULL(cmclaim_approval_tran.expense_remark,'') expense_remark " & _
                    '                  ", ISNULL(cmclaim_approval_tran.cancel_remark,'') AS cancel_remark " & _
                    '                  ", ISNULL(cmclaim_approval_tran.canceluserunkid,0) AS canceluserunkid " & _
                    '                                      ", cmclaim_approval_tran.cancel_datetime " & _
                    '                                      ", ISNULL(cmclaim_approval_tran.costcenterunkid,0) AS costcenterunkid " & _
                    '                                              ", ISNULL(prcostcenter_master.costcentercode,'') AS costcentercode " & _
                    '                                              ", ISNULL(prcostcenter_master.costcentername,'') AS costcentername " & _
                    '                                              ", ISNULL(cmexpense_master.glcodeunkid,0) AS glcodeunkid " & _
                    '                                              ", ISNULL(praccount_master.account_code,'') AS glcode " & _
                    '                                              ", ISNULL(praccount_master.account_name,'') AS glcodeName " & _
                    '                                              ", ISNULL(cmexpense_master.description,'') AS gldesc " & _
                    '                                              ", ISNULL(cmexpense_master.isbudgetmandatory,0) AS isbudgetmandatory " & _
                    '                                              ", ISNULL(cmexpense_master.ishrexpense,0) AS ishrexpense " & _
                    '                                      ", ISNULL(cmclaim_approval_tran.countryunkid,0) AS countryunkid " & _
                    '                                      ", ISNULL(cmclaim_approval_tran.base_countryunkid,0) AS base_countryunkid " & _
                    '                                      ", ISNULL(cmclaim_approval_tran.base_amount,0.00) AS base_amount " & _
                    '                                      ", ISNULL(cmclaim_approval_tran.exchangerateunkid,0) AS exchangerateunkid " & _
                    '                                      ", ISNULL(cmclaim_approval_tran.exchange_rate,0.00) AS exchange_rate " & _
                    '                                      ", ISNULL(cfexchange_rate.currency_sign,'') AS currency_sign " & _
                    '                                      ", ISNULL(cmclaim_approval_tran.airline,'') AS airline " & _
                    '                                      ", ISNULL(cmclaim_approval_tran.travelfrom,'') AS travelfrom " & _
                    '                                      ", ISNULL(cmclaim_approval_tran.travelto,'') AS travelto " & _
                    '                                      ", ISNULL(cmclaim_approval_tran.flightno,'') AS flightno " & _
                    '                                      ", cmclaim_approval_tran.traveldate " & _
                    '                                      ", ISNULL(cmclaim_approval_tran.other_person,'') AS other_person " & _
                    '                                      ", ISNULL(cmclaim_approval_tran.dpndtbeneficetranunkid,0) AS dpndtbeneficetranunkid " & _
                    '                                      ", CASE WHEN ISNULL(cmclaim_approval_tran.dpndtbeneficetranunkid,0) > 0 THEN ISNULL(hrdependants_beneficiaries_tran.first_name,'') + ' ' + ISNULL(hrdependants_beneficiaries_tran.last_name,'') " & _
                    '                                      "       WHEN ISNULL(cmclaim_approval_tran.dpndtbeneficetranunkid,0) <= 0 AND ISNULL(cmclaim_approval_tran.other_person,'') <> '' THEN ISNULL(cmclaim_approval_tran.other_person,'') " & _
                    '                                      "       WHEN ISNULL(cmclaim_approval_tran.dpndtbeneficetranunkid,0) <= 0 AND ISNULL(cmclaim_approval_tran.other_person,'') = '' THEN ISNULL(h1.firstname,'') + ' ' + ISNULL(h1.surname,'') " & _
                    '                                      "  ELSE '' END  AS name " & _
                    '                                      ", CASE WHEN ISNULL(cmclaim_approval_tran.dpndtbeneficetranunkid,0) > 0 THEN @DP " & _
                    '                                      "       WHEN ISNULL(cmclaim_approval_tran.dpndtbeneficetranunkid,0) <= 0 AND ISNULL(cmclaim_approval_tran.other_person,'') <> '' THEN @OH " & _
                    '                                      "       WHEN ISNULL(cmclaim_approval_tran.dpndtbeneficetranunkid,0) <= 0 AND ISNULL(cmclaim_approval_tran.other_person,'') = '' THEN @SF " & _
                    '                                      "  ELSE '' END  AS psgtype " & _
                    '                                      ", ISNULL(RL.name,'') AS relation " & _
                    '                                      ", ISNULL(cmclaim_request_master.rebate_percent,100) AS rebate_percent " & _
                    '                                      ", ISNULL(cmclaim_request_master.rebateid,1) AS rebateid " & _
                    '                                      ", ISNULL(cmclaim_request_master.baggagevalue,'') AS baggagevalue " & _
                    '                                      ", ISNULL(cmclaim_request_master.seattypeid,0) AS seattypeid "

                    StrExternalQry &= ", ISNULL(cmclaim_approval_tran.crapprovaltranunkid ,0) AS crapprovaltranunkid " & _
                            ", ISNULL(cmclaim_approval_tran.crtranunkid ,0) AS crtranunkid " & _
                            ", ISNULL(cmclaim_approval_tran.costingunkid ,0) AS costingunkid " & _
                            ", ISNULL(cmclaim_approval_tran.expenseunkid,0) AS expenseunkid " & _
                            ", ISNULL(cmclaim_approval_tran.secrouteunkid,0) AS secrouteunkid " & _
                            ", ISNULL(cmexpense_master.name,'') AS expense " & _
                            ", ISNULL(cmexpense_master.isaccrue,0) AS isaccrue " & _
                            ", ISNULL(cmexpense_master.isleaveencashment,0) AS isleaveencashment " & _
                            ", ISNULL(cmexpense_master.isconsiderforpayroll,0) AS isconsiderforpayroll " & _
                            ", ISNULL(cfcommon_master.name,'') AS sector " & _
                            ", ISNULL(cmexpense_costing_tran.amount,0.00) AS costing_amount " & _
                            ", CASE WHEN cmexpense_master.uomunkid = 1 THEN @Qty WHEN cmexpense_master.uomunkid = 2 THEN @Amt END AS uom " & _
                            ", ISNULL(cmclaim_approval_tran.unitprice,0.00 ) unitprice " & _
                            ", ISNULL(cmclaim_approval_tran.quantity,0.00 ) quantity " & _
                            ", ISNULL(cmclaim_approval_tran.amount,0.00 ) amount " & _
                            ", ISNULL(cmclaim_approval_tran.expense_remark,'') expense_remark " & _
                            ", ISNULL(cmclaim_approval_tran.cancel_remark,'') AS cancel_remark " & _
                            ", ISNULL(cmclaim_approval_tran.canceluserunkid,0) AS canceluserunkid " & _
                                                ", cmclaim_approval_tran.cancel_datetime " & _
                                                ", ISNULL(cmclaim_approval_tran.costcenterunkid,0) AS costcenterunkid " & _
                                                        ", ISNULL(prcostcenter_master.costcentercode,'') AS costcentercode " & _
                                                        ", ISNULL(prcostcenter_master.costcentername,'') AS costcentername " & _
                                                        ", ISNULL(cmexpense_master.glcodeunkid,0) AS glcodeunkid " & _
                                                        ", ISNULL(praccount_master.account_code,'') AS glcode " & _
                                                        ", ISNULL(praccount_master.account_name,'') AS glcodeName " & _
                                                        ", ISNULL(cmexpense_master.description,'') AS gldesc " & _
                                                        ", ISNULL(cmexpense_master.isbudgetmandatory,0) AS isbudgetmandatory " & _
                                                        ", ISNULL(cmexpense_master.ishrexpense,0) AS ishrexpense " & _
                                                ", ISNULL(cmclaim_approval_tran.countryunkid,0) AS countryunkid " & _
                                                ", ISNULL(cmclaim_approval_tran.base_countryunkid,0) AS base_countryunkid " & _
                                                ", ISNULL(cmclaim_approval_tran.base_amount,0.00) AS base_amount " & _
                                                ", ISNULL(cmclaim_approval_tran.exchangerateunkid,0) AS exchangerateunkid " & _
                                                ", ISNULL(cmclaim_approval_tran.exchange_rate,0.00) AS exchange_rate " & _
                                                ", ISNULL(cfexchange_rate.currency_sign,'') AS currency_sign " & _
                                                ", ISNULL(cmclaim_approval_tran.airline,'') AS airline " & _
                                                          ", ISNULL(cfrt.name,'') AS travelfrom " & _
                                                          ", ISNULL(ctrt.name,'') AS travelto " & _
                                                ", ISNULL(cmclaim_approval_tran.flightno,'') AS flightno " & _
                                                ", cmclaim_approval_tran.traveldate " & _
                                                ", ISNULL(cmclaim_approval_tran.other_person,'') AS other_person " & _
                                                ", ISNULL(cmclaim_approval_tran.dpndtbeneficetranunkid,0) AS dpndtbeneficetranunkid " & _
                                                          ", ISNULL(cmclaim_approval_tran.fromrouteid,0) AS fromrouteid " & _
                                                          ", ISNULL(cmclaim_approval_tran.torouteid,0) AS torouteid " & _
                                                ", CASE WHEN ISNULL(cmclaim_approval_tran.dpndtbeneficetranunkid,0) > 0 THEN ISNULL(hrdependants_beneficiaries_tran.first_name,'') + ' ' + ISNULL(hrdependants_beneficiaries_tran.last_name,'') " & _
                                                "       WHEN ISNULL(cmclaim_approval_tran.dpndtbeneficetranunkid,0) <= 0 AND ISNULL(cmclaim_approval_tran.other_person,'') <> '' THEN ISNULL(cmclaim_approval_tran.other_person,'') " & _
                                                "       WHEN ISNULL(cmclaim_approval_tran.dpndtbeneficetranunkid,0) <= 0 AND ISNULL(cmclaim_approval_tran.other_person,'') = '' THEN ISNULL(h1.firstname,'') + ' ' + ISNULL(h1.surname,'') " & _
                                                "  ELSE '' END  AS name " & _
                                                ", CASE WHEN ISNULL(cmclaim_approval_tran.dpndtbeneficetranunkid,0) > 0 THEN @DP " & _
                                                "       WHEN ISNULL(cmclaim_approval_tran.dpndtbeneficetranunkid,0) <= 0 AND ISNULL(cmclaim_approval_tran.other_person,'') <> '' THEN @OH " & _
                                                "       WHEN ISNULL(cmclaim_approval_tran.dpndtbeneficetranunkid,0) <= 0 AND ISNULL(cmclaim_approval_tran.other_person,'') = '' THEN @SF " & _
                                                "  ELSE '' END  AS psgtype " & _
                                                ", ISNULL(RL.name,'') AS relation " & _
                                                ", ISNULL(cmclaim_request_master.rebate_percent,100) AS rebate_percent " & _
                                                ", ISNULL(cmclaim_request_master.rebateid,1) AS rebateid " & _
                                                ", ISNULL(cmclaim_request_master.baggagevalue,'') AS baggagevalue " & _
                                                ", ISNULL(cmclaim_request_master.seattypeid,0) AS seattypeid " & _
                                                ", ISNULL((SELECT TOP 1 cm.expenseunkid FROM cmexpense_master cm WHERE cm.expensetypeid = 6 AND cm.isaccrue = 1 AND cm.isactive = 1),0) AS deductexpenseunkid " & _
                                                ", ISNULL(cmclaim_request_master.claim_remark,'') AS claim_remark " & _
                                                ", ISNULL(cmj.name,'') AS journal " & _
                                                ", ISNULL(cmp.name,'') AS paymentmode " & _
                                                ", cmexpense_master.isp2pdebit AS isdebit " & _
                                                ", cmexpense_master.isp2pcredit AS iscredit "

                    'Pinkal (23-Dec-2023) -- '(A1X-1555)  NMB - P2P Changes for Claim Request and Company Asset.[ISNULL(cmj.name,'') AS journal , ISNULL(cmp.name,'') AS paymentmode , ISNULL(cmexpense_master.isp2pdebit,0) AS isdebit, ISNULL(cmexpense_master.isp2pcredit,0) AS iscredit ]

                    'Pinkal (16-Oct-2023) -- (A1X-1399) NMB - Post approved claims amounts to P2P.[", ISNULL(cmclaim_request_master.claim_remark,'') AS claim_remark "]

                    'S.SANDEEP |29-JUN-2022| -- START {AC2-390_V2} [deductexpenseunkid : DEDUCTING BALANCE FROM PRIVILGE CATEGORY FOR DEPENEDANT CATEGORY] -- END

                    'S.SANDEEP |13-APR-2022| -- END

                    'S.SANDEEP |10-MAR-2022| -- ISSUE/ENHANCEMENT : OLD-580 [airline,travelfrom,travelto,flightno,traveldate,other_person,dpndtbeneficetranunkid,dependant]

                End If

                StrExternalQry &= ", cmclaim_approval_tran.iscancel " & _
                            ", ISNULL(cmclaim_approval_tran.isvoid,0) AS isvoid " & _
                            ", ISNULL(cmclaim_approval_tran.voiduserunkid,0) AS voiduserunkid " & _
                        ", ISNULL(cmclaim_approval_tran.voidloginemployeeunkid,-1) AS voidloginemployeeunkid " & _
                            ", cmclaim_approval_tran.voiddatetime " & _
                            ", ISNULL(cmclaim_approval_tran.voidreason,'') AS voidreason " & _
                            ", ISNULL(Alloc.sectiongroupunkid,0) AS sectiongroupunkid " & _
                            ", ISNULL(Alloc.unitgroupunkid,0) AS unitgroupunkid " & _
                            ", ISNULL(Alloc.teamunkid,0) AS teamunkid " & _
                            ", ISNULL(Alloc.stationunkid,0) AS stationunkid " & _
                            ", ISNULL(Alloc.deptgroupunkid,0) AS deptgroupunkid " & _
                            ", ISNULL(Alloc.departmentunkid,0) AS departmentunkid " & _
                            ", ISNULL(Alloc.sectionunkid,0) AS sectionunkid " & _
                            ", ISNULL(Alloc.unitunkid,0) AS unitunkid " & _
                            ", ISNULL(Alloc.classgroupunkid,0) AS classgroupunkid " & _
                            ", ISNULL(Alloc.classunkid,0) AS classunkid " & _
                            ", ISNULL(Jobs.jobunkid,0) AS jobunkid " & _
                            ", ISNULL(Jobs.jobgroupunkid,0) AS jobgroupunkid " & _
                            ", ISNULL(Grds.gradegroupunkid,0) AS gradegroupunkid " & _
                            ", ISNULL(Grds.gradeunkid,0) AS gradeunkid " & _
                            ", ISNULL(Grds.gradelevelunkid,0) AS gradelevelunkid " & _
                            ",CONVERT(CHAR(8),cmclaim_approval_tran.approvaldate, 112) AS approvaldate" & _
                            "   FROM cmclaim_approval_tran " & _
                            "	LEFT JOIN cmclaim_request_master ON cmclaim_request_master.crmasterunkid = cmclaim_approval_tran.crmasterunkid AND cmclaim_request_master.isvoid = 0 " & _
                            "   LEFT JOIN hremployee_master h1 on h1.employeeunkid = cmclaim_request_master.employeeunkid  " & _
                            "   #EMPL_JOIN#  " & _
                            "   JOIN cmexpapprover_master ON cmexpapprover_master.employeeunkid = cmclaim_approval_tran.approveremployeeunkid  AND cmexpapprover_master.crapproverunkid = cmclaim_approval_tran.crapproverunkid  AND cmexpapprover_master.isexternalapprover = #ExAppr#" & _
                            "   LEFT JOIN cmapproverlevel_master ON cmapproverlevel_master.crlevelunkid = cmexpapprover_master.crlevelunkid	" & _
                            "   LEFT JOIN hrapprover_usermapping ON hrapprover_usermapping.approverunkid = cmexpapprover_master.crapproverunkid " & " AND hrapprover_usermapping.usertypeid = " & enUserType.crApprover & _
                            "   LEFT JOIN " & _
                            "   ( " & _
                            "    SELECT " & _
                            "         stationunkid " & _
                            "        ,deptgroupunkid " & _
                            "        ,departmentunkid " & _
                            "        ,sectiongroupunkid " & _
                            "        ,sectionunkid " & _
                            "        ,unitgroupunkid " & _
                            "        ,unitunkid " & _
                            "        ,teamunkid " & _
                            "        ,classgroupunkid " & _
                            "        ,classunkid " & _
                            "        ,employeeunkid " & _
                            "        ,ROW_NUMBER()OVER(PARTITION BY employeeunkid ORDER BY effectivedate DESC) AS rno " & _
                            "    FROM hremployee_transfer_tran " & _
                            "    WHERE isvoid = 0 AND CONVERT(CHAR(8),effectivedate,112) <= '" & strEmployeeAsOnDate & "' " & _
                            "   ) AS Alloc ON Alloc.employeeunkid = h1.employeeunkid AND Alloc.rno = 1 " & _
                            "  LEFT JOIN " & _
                            "   ( " & _
                            "    SELECT " & _
                            "         jobunkid " & _
                            "        ,jobgroupunkid " & _
                            "        ,employeeunkid " & _
                            "        ,ROW_NUMBER()OVER(PARTITION BY employeeunkid ORDER BY effectivedate DESC) AS rno " & _
                            "    FROM hremployee_categorization_tran " & _
                            "    WHERE isvoid = 0 AND CONVERT(CHAR(8),effectivedate,112) <= '" & strEmployeeAsOnDate & "' " & _
                            "   ) AS Jobs ON Jobs.employeeunkid = h1.employeeunkid AND Jobs.rno = 1 " & _
                            "  LEFT JOIN " & _
                            "   ( " & _
                            "        SELECT " & _
                            "         gradegroupunkid " & _
                            "        ,gradeunkid " & _
                            "        ,gradelevelunkid " & _
                            "        ,employeeunkid " & _
                            "        ,ROW_NUMBER()OVER(PARTITION BY employeeunkid ORDER BY incrementdate DESC,salaryincrementtranunkid DESC) AS rno " & _
                            "    FROM prsalaryincrement_tran " & _
                            "    WHERE isvoid = 0 AND prsalaryincrement_tran.isapproved = 1 AND CONVERT(CHAR(8),incrementdate,112) <= '" & strEmployeeAsOnDate & "' " & _
                            "   ) AS Grds ON Grds.employeeunkid = h1.employeeunkid AND Grds.rno = 1 "

                'Pinkal (18-Mar-2021) -- Bug Solved For KBC For External approval Retrival ["LEFT JOIN cmexpapprover_master ON cmexpapprover_master.employeeunkid = cmclaim_approval_tran.approveremployeeunkid  AND cmexpapprover_master.crapproverunkid = cmclaim_approval_tran.crapproverunkid  AND cmexpapprover_master.isexternalapprover = #ExAppr#" & _]

                If IsDistinct = False Then
                    'S.SANDEEP |13-APR-2022| -- START
                    'ISSUE/ENHANCEMENT : AC2-143
                    'StrExternalQry &= " LEFT JOIN cmexpense_master ON cmexpense_master.expenseunkid = cmclaim_approval_tran.expenseunkid " & _
                    '                  " LEFT JOIN cfcommon_master ON cfcommon_master.masterunkid = cmclaim_approval_tran.secrouteunkid AND cfcommon_master.mastertype = " & clsCommon_Master.enCommonMaster.SECTOR_ROUTE & _
                    '                  " LEFT JOIN cmexpense_costing_tran ON cmexpense_costing_tran.secrouteunkid = cmclaim_approval_tran.secrouteunkid AND cmexpense_costing_tran.costingunkid = cmclaim_approval_tran.costingunkid  " & _
                    '                  " LEFT JOIN praccount_master ON praccount_master.accountunkid = cmexpense_master.glcodeunkid " & _
                    '                  " LEFT JOIN prcostcenter_master ON prcostcenter_master.costcenterunkid = cmclaim_approval_tran.costcenterunkid " & _
                    '                  " LEFT JOIN cfexchange_rate ON cfexchange_rate.exchangerateunkid = ISNULL(cmclaim_approval_tran.exchangerateunkid,0) " & _
                    '                  " LEFT JOIN hrdependants_beneficiaries_tran ON hrdependants_beneficiaries_tran.dpndtbeneficetranunkid = cmclaim_approval_tran.dpndtbeneficetranunkid AND hrdependants_beneficiaries_tran.isvoid = 0 " & _
                    '                  " LEFT JOIN cfcommon_master AS RL ON RL.masterunkid = hrdependants_beneficiaries_tran.relationunkid AND RL.mastertype = " & clsCommon_Master.enCommonMaster.RELATIONS & " "
                    StrExternalQry &= " LEFT JOIN cmexpense_master ON cmexpense_master.expenseunkid = cmclaim_approval_tran.expenseunkid " & _
                                 " LEFT JOIN cfcommon_master ON cfcommon_master.masterunkid = cmclaim_approval_tran.secrouteunkid AND cfcommon_master.mastertype = " & clsCommon_Master.enCommonMaster.SECTOR_ROUTE & _
                                               " LEFT JOIN cmexpense_costing_tran ON cmexpense_costing_tran.secrouteunkid = cmclaim_approval_tran.secrouteunkid AND cmexpense_costing_tran.costingunkid = cmclaim_approval_tran.costingunkid  " & _
                                               " LEFT JOIN praccount_master ON praccount_master.accountunkid = cmexpense_master.glcodeunkid " & _
                                               " LEFT JOIN prcostcenter_master ON prcostcenter_master.costcenterunkid = cmclaim_approval_tran.costcenterunkid " & _
                                               " LEFT JOIN cfexchange_rate ON cfexchange_rate.exchangerateunkid = ISNULL(cmclaim_approval_tran.exchangerateunkid,0) " & _
                                               " LEFT JOIN hrdependants_beneficiaries_tran ON hrdependants_beneficiaries_tran.dpndtbeneficetranunkid = cmclaim_approval_tran.dpndtbeneficetranunkid AND hrdependants_beneficiaries_tran.isvoid = 0 " & _
                                      " LEFT JOIN cfcommon_master AS RL ON RL.masterunkid = hrdependants_beneficiaries_tran.relationunkid AND RL.mastertype = " & clsCommon_Master.enCommonMaster.RELATIONS & " " & _
                                      " LEFT JOIN cfcommon_master AS cfrt ON cfrt.masterunkid = cmclaim_approval_tran.fromrouteid AND cfrt.mastertype = " & clsCommon_Master.enCommonMaster.SECTOR_ROUTE & " " & _
                                               " LEFT JOIN cfcommon_master AS ctrt ON ctrt.masterunkid = cmclaim_approval_tran.torouteid AND cfrt.mastertype = " & clsCommon_Master.enCommonMaster.SECTOR_ROUTE & " " & _
                                               " LEFT JOIN cfcommon_master AS cmj ON cmj.masterunkid = cmexpense_master.journalunkid AND cmj.mastertype = " & clsCommon_Master.enCommonMaster.CLAIM_JOURNAL & " " & _
                                               " LEFT JOIN cfcommon_master AS cmp ON cmp.masterunkid = cmexpense_master.paymentmodeunkid AND cmp.mastertype = " & clsCommon_Master.enCommonMaster.CLAIM_PAYMENTMODE & " "

                    'Pinkal (23-Dec-2023) -- (A1X-1555)  NMB - P2P Changes for Claim Request and Company Asset.[LEFT JOIN cfcommon_master AS cmj ON cmj.masterunkid = cmexpense_master.jounalunkid AND cmj.mastertype = " & clsCommon_Master.enCommonMaster.CLAIM_JOURNAL  LEFT JOIN cfcommon_master AS cmp ON cmp.masterunkid = cmexpense_master.paymentmodeunkid AND cmp.mastertype = " & clsCommon_Master.enCommonMaster.CLAIM_PAYMENTMODE ]

                    'S.SANDEEP |13-APR-2022| -- END


                    'S.SANDEEP |10-MAR-2022| -- SSUE/ENHANCEMENT : OLD-580 [LEFT JOIN hrdependants_beneficiaries_tran ON hrdependants_beneficiaries_tran.dpndtbeneficetranunkid = cmclaim_approval_tran.dpndtbeneficetranunkid AND hrdependants_beneficiaries_tran.isvoid = 0]

                End If


                'Pinkal (13-Aug-2020) -- Start
                'Bug NMB:  Working on IIS Freezing and Dump Issue for NMB.
                If IsDistinct Then
                    StrExternalQry &= " LEFT JOIN ( " & _
                                               "                    SELECT " & _
                                               "                            cmclaim_approval_tran.crmasterunkid " & _
                                               "                           ,cmclaim_approval_tran.crapproverunkid " & _
                                               "                           ,cmapproverlevel_master.crpriority " & _
                                               "                           ,cmclaim_approval_tran.statusunkid " & _
                                               "                           ,ROW_NUMBER()OVER(PARTITION BY cmclaim_approval_tran.crmasterunkid,cmapproverlevel_master.crpriority ORDER BY cmclaim_approval_tran.approvaldate DESC) AS rno " & _
                                               "                           ,CASE WHEN cmclaim_approval_tran.statusunkid = 1 THEN  @ApprovedBy + ' ' + ISNULL(cuser.username, '') " & _
                                               "                                     WHEN cmclaim_approval_tran.statusunkid = 2 THEN  @Pending " & _
                                               "                                     WHEN cmclaim_approval_tran.statusunkid = 3 THEN  @RejectedBy + ' ' + ISNULL(cuser.username, '') " & _
                                               "                             END AS 'AppprovalStatus' " & _
                                               "                    FROM cmclaim_approval_tran  " & _
                                               "                    LEFT JOIN cmexpapprover_master ON cmexpapprover_master.employeeunkid = cmclaim_approval_tran.approveremployeeunkid AND cmexpapprover_master.crapproverunkid = cmclaim_approval_tran.crapproverunkid  AND cmexpapprover_master.isexternalapprover = #ExAppr#" & _
                                               "                    LEFT JOIN hrapprover_usermapping ON hrapprover_usermapping.approverunkid = cmexpapprover_master.crapproverunkid AND usertypeid= " & enUserType.crApprover & _
                                               "                    LEFT JOIN hrmsConfiguration..cfuser_master AS cuser	ON cuser.userunkid = hrapprover_usermapping.userunkid " & _
                                               "                    LEFT JOIN cmapproverlevel_master ON cmapproverlevel_master.crlevelunkid = cmexpapprover_master.crlevelunkid " & _
                                               "                    WHERE cmclaim_approval_tran.isvoid= 0 " & _
                                               "                  ) AS App ON app.crmasterunkid = cmclaim_approval_tran.crmasterunkid " & _
                                               "  AND app.crpriority = cmapproverlevel_master.crpriority and app.rno = 1 "

                    'Pinkal (04-Aug-2023) -- Problem Resolved for KBC Claim Approval [AND cmexpapprover_master.isexternalapprover = #ExAppr#"  ]

                End If
                'Pinkal (13-Aug-2020) -- End



                If xAdvanceJoinQry.Trim.Length > 0 Then
                    StrExternalQry &= xAdvanceJoinQry.Replace("hremployee_master", "h1")
                End If

                'S.SANDEEP [09-OCT-2018] -- START
                'StrExternalQry &= " WHERE cmclaim_request_master.frommoduleid =  " & enExpFromModuleID.FROM_EXPENSE
                'AS ON NOW, AS TRAINING APPROVAL IS NOT GIVEN IN DESKTOP
                If intExpCategroryId = enExpenseType.EXP_TRAINING Then
                    StrExternalQry &= " WHERE cmclaim_request_master.frommoduleid =  " & enExpFromModuleID.FROM_TRAINING
                Else
                    StrExternalQry &= " WHERE cmclaim_request_master.frommoduleid =  " & enExpFromModuleID.FROM_EXPENSE
                End If
                'S.SANDEEP [09-OCT-2018] -- END


                If blnOnlyActive Then
                    StrExternalQry &= " AND cmclaim_approval_tran.isvoid = 0 "
                End If

                If intApproverID > 0 Then
                    StrExternalQry &= " AND cmclaim_approval_tran.crapproverunkid = " & intApproverID
                End If

                If mstFilter.Trim.Length > 0 Then
                    StrExternalQry &= " AND " & mstFilter
                End If

                If intClaimMstId > -1 Then
                    StrExternalQry &= " AND cmclaim_request_master.crmasterunkid = " & intClaimMstId
                End If

                strQ &= StrExternalQry

                strQ = strQ.Replace("#APPR_NAME#", "ISNULL(h2.firstname,'') + ' ' + isnull(h2.surname,'') ")
                strQ = strQ.Replace("#EMPL_JOIN#", "LEFT JOIN #DName#hremployee_master h2 on h2.employeeunkid = cmclaim_approval_tran.approveremployeeunkid ")
                strQ = strQ.Replace("#DName#", "")
                strQ = strQ.Replace("#ExAppr#", "0")
                strQ = strQ.Replace("#APPR_EMAIL_VALUE#", "ISNULL(h2.email,'') ")

                dsExternalCompany = GetClaimExternalApproverList(blnPaymentApprovalwithLeaveApproval, intExpCategroryId, objDoOperation, "List")

                If objDataOperation.ErrorMessage <> "" Then
                    exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                    Throw exForce
                End If

                For Each dr In dsExternalCompany.Tables(0).Rows
                    If strQ.Trim.Length > 0 Then strQ &= " UNION "
                    strQ &= StrExternalQry & " AND UEmp.companyunkid = " & dr("companyunkid") & " "
                    If dr("companyunkid") <= 0 AndAlso dr("DName").ToString.Trim = "" Then
                        strQ = strQ.Replace("#APPR_NAME#", " ISNULL(UEmp.username,'') ")
                        strQ = strQ.Replace("#APPR_EMAIL_VALUE#", "ISNULL(UEmp.email,'') ")
                        strQ = strQ.Replace("#EMPL_JOIN#", " LEFT JOIN hrmsConfiguration..cfuser_master AS UEmp ON UEmp.userunkid = cmclaim_approval_tran.approveremployeeunkid ")
                    Else
                        strQ = strQ.Replace("#APPR_NAME#", "CASE WHEN ISNULL(h2.firstname, '') + ' ' + ISNULL(h2.surname, '') = ' ' THEN ISNULL(UEmp.username,'') ELSE ISNULL(h2.firstname, '') + ' ' + ISNULL(h2.surname, '') END ")
                        strQ = strQ.Replace("#APPR_EMAIL_VALUE#", "ISNULL(UEmp.email,'') ")
                        strQ = strQ.Replace("#EMPL_JOIN#", " LEFT JOIN hrmsConfiguration..cfuser_master AS UEmp ON UEmp.userunkid = cmclaim_approval_tran.approveremployeeunkid " & _
                                                       " LEFT JOIN #DName#hremployee_master h2 on h2.employeeunkid = UEmp.employeeunkid AND h2.isapproved = 1")
                    End If

                    If dr("DName").ToString.Trim.Length > 0 Then
                        strQ = strQ.Replace("#DName#", dr("DName") & "..")
                    Else
                        strQ = strQ.Replace("#DName#", "")
                    End If
                    strQ = strQ.Replace("#ExAppr#", "1")
                Next


            End If

            Select Case intExpCategroryId

                Case enExpenseType.EXP_LEAVE

                    Dim dsCompany As New DataSet
                    Dim StrInnerQry As String : StrInnerQry = ""

                    If blnPaymentApprovalwithLeaveApproval AndAlso intExpCategroryId <> enExpenseType.EXP_LEAVE Then
                        strQ &= " UNION "
                    End If

                    dsCompany = GetClaimExternalApproverList(blnPaymentApprovalwithLeaveApproval, intExpCategroryId, objDoOperation, "List")

                    If objDataOperation.ErrorMessage <> "" Then
                        exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                        Throw exForce
                    End If

                    StrInnerQry &= "SELECT  "

                    If IsDistinct Then
                        StrInnerQry &= " DISTINCT "
                    Else
                        StrInnerQry &= " '' AS AUD,'' AS GUID , "
                    End If

                    StrInnerQry &= " Cast (0 as bit) As Ischeck " & _
                                ",  cmclaim_request_master.crmasterunkid " & _
                                ", cmclaim_request_master.claimrequestno " & _
                                ", h1.employeecode " & _
                                ", ISNULL(h1.firstname,'') + ' ' + isnull(h1.surname,'') as employeename " & _
                                ", #APPR_NAME# as approvername " & _
                                ", #APPR_EMAIL_VALUE# AS approveremail "

                    If blnPaymentApprovalwithLeaveApproval Then
                        StrInnerQry &= ", lvapproverlevel_master.levelname AS crlevelname " & _
                                    ", lvapproverlevel_master.priority AS crpriority " & _
                                    ", lvapproverlevel_master.levelunkid AS crlevelunkid " & _
                                    ", lvleaveapprover_master.isexternalapprover "
                    Else
                        StrInnerQry &= ", cmapproverlevel_master.crlevelname " & _
                                    ", cmapproverlevel_master.crpriority " & _
                                    ", cmapproverlevel_master.crlevelunkid " & _
                                    ", cmexpapprover_master.isexternalapprover "
                    End If

                    StrInnerQry &= ", cmclaim_request_master.employeeunkid " & _
                            ", cmclaim_request_master.expensetypeid " & _
                            ", cmclaim_approval_tran.statusunkid " & _
                            ", cmclaim_request_master.statusunkid As crstatusunkid " & _
                            ", cmclaim_approval_tran.visibleid " & _
                            ", cmclaim_approval_tran.crapproverunkid " & _
                            ", cmclaim_approval_tran.approveremployeeunkid " & _
                            ", ISNULL(hrapprover_usermapping.userunkid,-1) as mapuserunkid " & _
                            ", '' AS period " & _
                            ", CONVERT(CHAR(8),cmclaim_request_master.transactiondate,112) AS tdate " & _
                            ", CASE WHEN cmclaim_request_master.expensetypeid = '" & enExpenseType.EXP_LEAVE & "' THEN @Leave " & _
                            "         WHEN cmclaim_request_master.expensetypeid = '" & enExpenseType.EXP_MEDICAL & "' THEN @Medical " & _
                            "         WHEN cmclaim_request_master.expensetypeid = '" & enExpenseType.EXP_MISCELLANEOUS & "' THEN @Miscellaneous " & _
                            "         WHEN cmclaim_request_master.expensetypeid = '" & enExpenseType.EXP_TRAINING & "' THEN @Training  " & _
                            "         WHEN cmclaim_request_master.expensetypeid = '" & enExpenseType.EXP_IMPREST & "' THEN @Imprest " & _
                            "         WHEN cmclaim_request_master.expensetypeid = '" & enExpenseType.EXP_REBATE_PRIVILEGE & "' THEN @EXP_REBATE_PRIVILEGE " & _
                            "         WHEN cmclaim_request_master.expensetypeid = '" & enExpenseType.EXP_REBATE_DUTY & "' THEN @EXP_REBATE_DUTY " & _
                            "         WHEN cmclaim_request_master.expensetypeid = '" & enExpenseType.EXP_REBATE_PRIVILEGE_DPNDT & "' THEN @EXP_REBATE_PRIVILEGE_DPNDT END AS expensetype " & _
                            ", CASE WHEN cmclaim_approval_tran.statusunkid = 1 then @Approve " & _
                            "         WHEN cmclaim_approval_tran.statusunkid = 2 then @Pending  " & _
                            "         WHEN cmclaim_approval_tran.statusunkid = 3 then @Reject  " & _
                            "         WHEN cmclaim_approval_tran.statusunkid = 4 then @ReSchedule " & _
                            "         WHEN cmclaim_approval_tran.statusunkid = 6 then @Cancel " & _
                            "         WHEN cmclaim_approval_tran.statusunkid = 7 then @Issued END as status "
                    'S.SANDEEP |10-MAR-2022| -- START (OLD-580) {EXP_REBATE_PRIVILEGE,EXP_REBATE_DUTY} -- END

                    'Pinkal (11-Sep-2019) -- 'Enhancement NMB - Working On Claim Retirement for NMB.[" WHEN cmclaim_request_master.expensetypeid = '" & enExpenseType.EXP_IMPREST & "' THEN @Imprest END AS expensetype " & _]

                    If IsDistinct Then
                        StrInnerQry &= ", ISNULL((SELECT SUM(ct.amount) From cmclaim_approval_tran  ct WHERE ct.crmasterunkid = cmclaim_approval_tran.crmasterunkid AND ct.approveremployeeunkid = approveremployeeunkid AND ct.crapproverunkid = cmclaim_approval_tran.crapproverunkid AND ct.isvoid = 0 AND cmclaim_approval_tran.isvoid = 0 AND ct.iscancel = 0),0.00 ) amount " & _
                                               ", App.AppprovalStatus "
                    Else
                        'S.SANDEEP |13-APR-2022| -- START
                        'StrInnerQry &= ", ISNULL(cmclaim_approval_tran.crapprovaltranunkid ,0) AS crapprovaltranunkid " & _
                        '            ", ISNULL(cmclaim_approval_tran.crtranunkid ,0) AS crtranunkid " & _
                        '            ", ISNULL(cmclaim_approval_tran.costingunkid ,0) AS costingunkid " & _
                        '            ", ISNULL(cmclaim_approval_tran.expenseunkid,0) AS expenseunkid " & _
                        '            ", ISNULL(cmclaim_approval_tran.secrouteunkid,0) AS secrouteunkid " & _
                        '            ", ISNULL(cmexpense_master.name,'') AS expense " & _
                        '            ", ISNULL(cmexpense_master.isaccrue,0) AS isaccrue " & _
                        '            ", ISNULL(cmexpense_master.isleaveencashment,0) AS isleaveencashment " & _
                        '            ", ISNULL(cmexpense_master.isconsiderforpayroll,0) AS isconsiderforpayroll " & _
                        '            ", ISNULL(cfcommon_master.name,'') AS sector " & _
                        '            ", ISNULL(cmexpense_costing_tran.amount,0.00) AS costing_amount " & _
                        '            ", CASE WHEN cmexpense_master.uomunkid = 1 THEN @Qty WHEN cmexpense_master.uomunkid = 2 THEN @Amt END AS uom " & _
                        '            ", ISNULL(cmclaim_approval_tran.unitprice,0.00 ) unitprice " & _
                        '            ", ISNULL(cmclaim_approval_tran.quantity,0.00 ) quantity " & _
                        '            ", ISNULL(cmclaim_approval_tran.amount,0.00 ) amount " & _
                        '            ", ISNULL(cmclaim_approval_tran.expense_remark,'') expense_remark " & _
                        '            ", ISNULL(cmclaim_approval_tran.cancel_remark,'') AS cancel_remark " & _
                        '            ", ISNULL(cmclaim_approval_tran.canceluserunkid,0) AS canceluserunkid " & _
                        '                        ", cmclaim_approval_tran.cancel_datetime " & _
                        '                        ", ISNULL(cmclaim_approval_tran.costcenterunkid,0) AS costcenterunkid " & _
                        '                                    ", ISNULL(prcostcenter_master.costcentercode,'') AS costcentercode " & _
                        '                                    ", ISNULL(prcostcenter_master.costcentername,'') AS costcentername " & _
                        '                                    ", ISNULL(cmexpense_master.glcodeunkid,0) AS glcodeunkid " & _
                        '                                    ", ISNULL(praccount_master.account_code,'') AS glcode " & _
                        '                                    ", ISNULL(praccount_master.account_name,'') AS glcodeName " & _
                        '                                    ", ISNULL(cmexpense_master.description,'') AS gldesc " & _
                        '                                    ", ISNULL(cmexpense_master.isbudgetmandatory,0) AS isbudgetmandatory " & _
                        '                                    ", ISNULL(cmexpense_master.ishrexpense,0) AS ishrexpense " & _
                        '                                    ",ISNULL(cmclaim_approval_tran.countryunkid,0) AS countryunkid " & _
                        '                                    ",ISNULL(cmclaim_approval_tran.base_countryunkid,0) AS base_countryunkid " & _
                        '                                    ",ISNULL(cmclaim_approval_tran.base_amount,0.00) AS base_amount " & _
                        '                                    ",ISNULL(cmclaim_approval_tran.exchangerateunkid,0) AS exchangerateunkid " & _
                        '                                    ",ISNULL(cmclaim_approval_tran.exchange_rate,0.00) AS exchange_rate " & _
                        '                        ",ISNULL(cfexchange_rate.currency_sign,'') AS currency_sign " & _
                        '                        ", ISNULL(cmclaim_approval_tran.airline,'') AS airline " & _
                        '                        ", ISNULL(cmclaim_approval_tran.travelfrom,'') AS travelfrom " & _
                        '                        ", ISNULL(cmclaim_approval_tran.travelto,'') AS travelto " & _
                        '                        ", ISNULL(cmclaim_approval_tran.flightno,'') AS flightno " & _
                        '                        ", cmclaim_approval_tran.traveldate " & _
                        '                        ", ISNULL(cmclaim_approval_tran.other_person,'') AS other_person " & _
                        '                        ", ISNULL(cmclaim_approval_tran.dpndtbeneficetranunkid,0) AS dpndtbeneficetranunkid " & _
                        '                        ", ISNULL(cmclaim_approval_tran.dpndtbeneficetranunkid,0) AS dpndtbeneficetranunkid " & _
                        '                        ", CASE WHEN ISNULL(cmclaim_approval_tran.dpndtbeneficetranunkid,0) > 0 THEN ISNULL(hrdependants_beneficiaries_tran.first_name,'') + ' ' + ISNULL(hrdependants_beneficiaries_tran.last_name,'') " & _
                        '                        "       WHEN ISNULL(cmclaim_approval_tran.dpndtbeneficetranunkid,0) <= 0 AND ISNULL(cmclaim_approval_tran.other_person,'') <> '' THEN ISNULL(cmclaim_approval_tran.other_person,'') " & _
                        '                        "       WHEN ISNULL(cmclaim_approval_tran.dpndtbeneficetranunkid,0) <= 0 AND ISNULL(cmclaim_approval_tran.other_person,'') = '' THEN ISNULL(h1.firstname,'') + ' ' + ISNULL(h1.surname,'') " & _
                        '                        "  ELSE '' END  AS name " & _
                        '                        ", CASE WHEN ISNULL(cmclaim_approval_tran.dpndtbeneficetranunkid,0) > 0 THEN @DP " & _
                        '                        "       WHEN ISNULL(cmclaim_approval_tran.dpndtbeneficetranunkid,0) <= 0 AND ISNULL(cmclaim_approval_tran.other_person,'') <> '' THEN @OH " & _
                        '                        "       WHEN ISNULL(cmclaim_approval_tran.dpndtbeneficetranunkid,0) <= 0 AND ISNULL(cmclaim_approval_tran.other_person,'') = '' THEN @SF " & _
                        '                        "  ELSE '' END  AS psgtype " & _
                        '                        ", ISNULL(RL.name,'') AS relation "

                        StrInnerQry &= ", ISNULL(cmclaim_approval_tran.crapprovaltranunkid ,0) AS crapprovaltranunkid " & _
                                    ", ISNULL(cmclaim_approval_tran.crtranunkid ,0) AS crtranunkid " & _
                                    ", ISNULL(cmclaim_approval_tran.costingunkid ,0) AS costingunkid " & _
                                    ", ISNULL(cmclaim_approval_tran.expenseunkid,0) AS expenseunkid " & _
                                    ", ISNULL(cmclaim_approval_tran.secrouteunkid,0) AS secrouteunkid " & _
                                    ", ISNULL(cmexpense_master.name,'') AS expense " & _
                                    ", ISNULL(cmexpense_master.isaccrue,0) AS isaccrue " & _
                                    ", ISNULL(cmexpense_master.isleaveencashment,0) AS isleaveencashment " & _
                                    ", ISNULL(cmexpense_master.isconsiderforpayroll,0) AS isconsiderforpayroll " & _
                                    ", ISNULL(cfcommon_master.name,'') AS sector " & _
                                    ", ISNULL(cmexpense_costing_tran.amount,0.00) AS costing_amount " & _
                                    ", CASE WHEN cmexpense_master.uomunkid = 1 THEN @Qty WHEN cmexpense_master.uomunkid = 2 THEN @Amt END AS uom " & _
                                    ", ISNULL(cmclaim_approval_tran.unitprice,0.00 ) unitprice " & _
                                    ", ISNULL(cmclaim_approval_tran.quantity,0.00 ) quantity " & _
                                    ", ISNULL(cmclaim_approval_tran.amount,0.00 ) amount " & _
                                    ", ISNULL(cmclaim_approval_tran.expense_remark,'') expense_remark " & _
                                    ", ISNULL(cmclaim_approval_tran.cancel_remark,'') AS cancel_remark " & _
                                    ", ISNULL(cmclaim_approval_tran.canceluserunkid,0) AS canceluserunkid " & _
                                                ", cmclaim_approval_tran.cancel_datetime " & _
                                                ", ISNULL(cmclaim_approval_tran.costcenterunkid,0) AS costcenterunkid " & _
                                                            ", ISNULL(prcostcenter_master.costcentercode,'') AS costcentercode " & _
                                                            ", ISNULL(prcostcenter_master.costcentername,'') AS costcentername " & _
                                                            ", ISNULL(cmexpense_master.glcodeunkid,0) AS glcodeunkid " & _
                                                            ", ISNULL(praccount_master.account_code,'') AS glcode " & _
                                                            ", ISNULL(praccount_master.account_name,'') AS glcodeName " & _
                                                            ", ISNULL(cmexpense_master.description,'') AS gldesc " & _
                                                            ", ISNULL(cmexpense_master.isbudgetmandatory,0) AS isbudgetmandatory " & _
                                                            ", ISNULL(cmexpense_master.ishrexpense,0) AS ishrexpense " & _
                                                            ",ISNULL(cmclaim_approval_tran.countryunkid,0) AS countryunkid " & _
                                                            ",ISNULL(cmclaim_approval_tran.base_countryunkid,0) AS base_countryunkid " & _
                                                            ",ISNULL(cmclaim_approval_tran.base_amount,0.00) AS base_amount " & _
                                                            ",ISNULL(cmclaim_approval_tran.exchangerateunkid,0) AS exchangerateunkid " & _
                                                            ",ISNULL(cmclaim_approval_tran.exchange_rate,0.00) AS exchange_rate " & _
                                                ",ISNULL(cfexchange_rate.currency_sign,'') AS currency_sign " & _
                                                ", ISNULL(cmclaim_approval_tran.airline,'') AS airline " & _
                                                ", ISNULL(cfrt.name,'') AS travelfrom " & _
                                                ", ISNULL(ctrt.name,'') AS travelto " & _
                                                ", ISNULL(cmclaim_approval_tran.flightno,'') AS flightno " & _
                                                ", cmclaim_approval_tran.traveldate " & _
                                                ", ISNULL(cmclaim_approval_tran.other_person,'') AS other_person " & _
                                                ", ISNULL(cmclaim_approval_tran.dpndtbeneficetranunkid,0) AS dpndtbeneficetranunkid " & _
                                                ", ISNULL(cmclaim_approval_tran.fromrouteid,0) AS fromrouteid " & _
                                                ", ISNULL(cmclaim_approval_tran.torouteid,0) AS torouteid " & _
                                                ", CASE WHEN ISNULL(cmclaim_approval_tran.dpndtbeneficetranunkid,0) > 0 THEN ISNULL(hrdependants_beneficiaries_tran.first_name,'') + ' ' + ISNULL(hrdependants_beneficiaries_tran.last_name,'') " & _
                                                "       WHEN ISNULL(cmclaim_approval_tran.dpndtbeneficetranunkid,0) <= 0 AND ISNULL(cmclaim_approval_tran.other_person,'') <> '' THEN ISNULL(cmclaim_approval_tran.other_person,'') " & _
                                                "       WHEN ISNULL(cmclaim_approval_tran.dpndtbeneficetranunkid,0) <= 0 AND ISNULL(cmclaim_approval_tran.other_person,'') = '' THEN ISNULL(h1.firstname,'') + ' ' + ISNULL(h1.surname,'') " & _
                                                "  ELSE '' END  AS name " & _
                                                ", CASE WHEN ISNULL(cmclaim_approval_tran.dpndtbeneficetranunkid,0) > 0 THEN @DP " & _
                                                "       WHEN ISNULL(cmclaim_approval_tran.dpndtbeneficetranunkid,0) <= 0 AND ISNULL(cmclaim_approval_tran.other_person,'') <> '' THEN @OH " & _
                                                "       WHEN ISNULL(cmclaim_approval_tran.dpndtbeneficetranunkid,0) <= 0 AND ISNULL(cmclaim_approval_tran.other_person,'') = '' THEN @SF " & _
                                                "  ELSE '' END  AS psgtype " & _
                                                ", ISNULL(RL.name,'') AS relation " & _
                                                ", ISNULL(cmclaim_request_master.claim_remark,'') AS claim_remark " & _
                                                ", ISNULL(cmj.name,'') AS journal " & _
                                                ", ISNULL(cmp.name,'') AS paymentmode " & _
                                                ", cmexpense_master.isp2pdebit AS isdebit " & _
                                                ", cmexpense_master.isp2pcredit AS iscredit "

                        'Pinkal (23-Dec-2023) -- '(A1X-1555)  NMB - P2P Changes for Claim Request and Company Asset.[ISNULL(cmj.name,'') AS journal , ISNULL(cmp.name,'') AS paymentmode , ISNULL(cmexpense_master.isp2pdebit,0) AS isdebit, ISNULL(cmexpense_master.isp2pcredit,0) AS iscredit ]

                        'Pinkal (16-Oct-2023) -- (A1X-1399) NMB - Post approved claims amounts to P2P.[", ISNULL(cmclaim_request_master.claim_remark,'') AS claim_remark "]

                        'S.SANDEEP |13-APR-2022| -- END

                        'S.SANDEEP |10-MAR-2022| -- ISSUE/ENHANCEMENT : OLD-580 [airline,travelfrom,travelto,flightno,traveldate,other_person,dpndtbeneficetranunkid,dependant]

                    End If

                    StrInnerQry &= ", cmclaim_approval_tran.iscancel " & _
                                    ", ISNULL(cmclaim_approval_tran.isvoid,0) AS isvoid " & _
                                    ", ISNULL(cmclaim_approval_tran.voiduserunkid,0) AS voiduserunkid " & _
                                    ", ISNULL(cmclaim_approval_tran.voidloginemployeeunkid,-1) AS voidloginemployeeunkid " & _
                                    ", cmclaim_approval_tran.voiddatetime " & _
                                    ", ISNULL(cmclaim_approval_tran.voidreason,'') AS voidreason " & _
                                     ", ISNULL(Alloc.sectiongroupunkid,0) AS sectiongroupunkid " & _
                                    ", ISNULL(Alloc.unitgroupunkid,0) AS unitgroupunkid " & _
                                    ", ISNULL(Alloc.teamunkid,0) AS teamunkid " & _
                                    ", ISNULL(Alloc.stationunkid,0) AS stationunkid " & _
                                    ", ISNULL(Alloc.deptgroupunkid,0) AS deptgroupunkid " & _
                                    ", ISNULL(Alloc.departmentunkid,0) AS departmentunkid " & _
                                    ", ISNULL(Alloc.sectionunkid,0) AS sectionunkid " & _
                                    ", ISNULL(Alloc.unitunkid,0) AS unitunkid " & _
                                    ", ISNULL(Alloc.classgroupunkid,0) AS classgroupunkid " & _
                                    ", ISNULL(Alloc.classunkid,0) AS classunkid " & _
                                    ", ISNULL(Jobs.jobunkid,0) AS jobunkid " & _
                                    ", ISNULL(Jobs.jobgroupunkid,0) AS jobgroupunkid " & _
                                    ", ISNULL(Grds.gradegroupunkid,0) AS gradegroupunkid " & _
                                    ", ISNULL(Grds.gradeunkid,0) AS gradeunkid " & _
                                    ", ISNULL(Grds.gradelevelunkid,0) AS gradelevelunkid " & _
                                    ",CONVERT(CHAR(8),cmclaim_approval_tran.approvaldate,112) AS approvaldate " & _
                                    "   FROM cmclaim_approval_tran " & _
                                    "	LEFT JOIN cmclaim_request_master ON cmclaim_request_master.crmasterunkid = cmclaim_approval_tran.crmasterunkid AND cmclaim_request_master.isvoid = 0 " & _
                                    "   LEFT JOIN hremployee_master h1 on h1.employeeunkid = cmclaim_request_master.employeeunkid  " & _
                                   "   #EMPL_JOIN#  " & _
                                    "   LEFT JOIN " & _
                                    "   ( " & _
                                    "    SELECT " & _
                                    "         stationunkid " & _
                                    "        ,deptgroupunkid " & _
                                    "        ,departmentunkid " & _
                                    "        ,sectiongroupunkid " & _
                                    "        ,sectionunkid " & _
                                    "        ,unitgroupunkid " & _
                                    "        ,unitunkid " & _
                                    "        ,teamunkid " & _
                                    "        ,classgroupunkid " & _
                                    "        ,classunkid " & _
                                    "        ,employeeunkid " & _
                                    "        ,ROW_NUMBER()OVER(PARTITION BY employeeunkid ORDER BY effectivedate DESC) AS rno " & _
                                    "    FROM hremployee_transfer_tran " & _
                                    "    WHERE isvoid = 0 AND CONVERT(CHAR(8),effectivedate,112) <= '" & strEmployeeAsOnDate & "' " & _
                                    "   ) AS Alloc ON Alloc.employeeunkid = h1.employeeunkid AND Alloc.rno = 1 " & _
                                    "  LEFT JOIN " & _
                                    "   ( " & _
                                    "    SELECT " & _
                                    "         jobunkid " & _
                                    "        ,jobgroupunkid " & _
                                    "        ,employeeunkid " & _
                                    "        ,ROW_NUMBER()OVER(PARTITION BY employeeunkid ORDER BY effectivedate DESC) AS rno " & _
                                    "    FROM hremployee_categorization_tran " & _
                                    "    WHERE isvoid = 0 AND CONVERT(CHAR(8),effectivedate,112) <= '" & strEmployeeAsOnDate & "' " & _
                                    "   ) AS Jobs ON Jobs.employeeunkid = h1.employeeunkid AND Jobs.rno = 1 " & _
                                    "  LEFT JOIN " & _
                                    "   ( " & _
                                    "        SELECT " & _
                                    "         gradegroupunkid " & _
                                    "        ,gradeunkid " & _
                                    "        ,gradelevelunkid " & _
                                    "        ,employeeunkid " & _
                                    "        ,ROW_NUMBER()OVER(PARTITION BY employeeunkid ORDER BY incrementdate DESC,salaryincrementtranunkid DESC) AS rno " & _
                                    "    FROM prsalaryincrement_tran " & _
                                    "    WHERE isvoid = 0 AND prsalaryincrement_tran.isapproved = 1 AND CONVERT(CHAR(8),incrementdate,112) <= '" & strEmployeeAsOnDate & "' " & _
                                    "   ) AS Grds ON Grds.employeeunkid = h1.employeeunkid AND Grds.rno = 1 "

                    If blnPaymentApprovalwithLeaveApproval Then
                        StrInnerQry &= " JOIN lvleaveapprover_master ON lvleaveapprover_master.leaveapproverunkid = cmclaim_approval_tran.approveremployeeunkid  AND lvleaveapprover_master.approverunkid = cmclaim_approval_tran.crapproverunkid   AND lvleaveapprover_master.isexternalapprover = #ExAppr# " & _
                                    "   LEFT JOIN lvapproverlevel_master ON lvapproverlevel_master.levelunkid = lvleaveapprover_master.levelunkid	" & _
                                    "   LEFT JOIN hrapprover_usermapping ON hrapprover_usermapping.approverunkid = lvleaveapprover_master.approverunkid " & " AND hrapprover_usermapping.usertypeid = " & enUserType.Approver

                        'Pinkal (07-Dec-2021) -- Bug Solved For Voltamp For External approval Retrival [LEFT JOIN lvleaveapprover_master ON lvleaveapprover_master.leaveapproverunkid = cmclaim_approval_tran.approveremployeeunkid  AND lvleaveapprover_master.approverunkid = cmclaim_approval_tran.crapproverunkid   AND lvleaveapprover_master.isexternalapprover = #ExAppr# "]

                    Else
                        StrInnerQry &= " JOIN cmexpapprover_master ON cmexpapprover_master.employeeunkid = cmclaim_approval_tran.approveremployeeunkid  AND cmexpapprover_master.crapproverunkid = cmclaim_approval_tran.crapproverunkid AND cmexpapprover_master.isexternalapprover = #ExAppr# " & _
                                    "   LEFT JOIN cmapproverlevel_master ON cmapproverlevel_master.crlevelunkid = cmexpapprover_master.crlevelunkid	" & _
                                    "   LEFT JOIN hrapprover_usermapping ON hrapprover_usermapping.approverunkid = cmexpapprover_master.crapproverunkid " & " AND hrapprover_usermapping.usertypeid = " & enUserType.crApprover

                        'Pinkal (18-Mar-2021) -- Bug Solved For KBC For External approval Retrival [LEFT JOIN cmexpapprover_master ON cmexpapprover_master.employeeunkid = cmclaim_approval_tran.approveremployeeunkid  AND cmexpapprover_master.crapproverunkid = cmclaim_approval_tran.crapproverunkid AND cmexpapprover_master.isexternalapprover = #ExAppr# ]
                    End If


                    If IsDistinct = False Then
                        'S.SANDEEP |13-APR-2022| -- START
                        'ISSUE/ENHANCEMENT : AC2-143
                        'StrInnerQry &= " LEFT JOIN cmexpense_master ON cmexpense_master.expenseunkid = cmclaim_approval_tran.expenseunkid " & _
                        '            " LEFT JOIN cfcommon_master ON cfcommon_master.masterunkid = cmclaim_approval_tran.secrouteunkid AND cfcommon_master.mastertype = " & clsCommon_Master.enCommonMaster.SECTOR_ROUTE & _
                        '                       " LEFT JOIN cmexpense_costing_tran ON cmexpense_costing_tran.secrouteunkid = cmclaim_approval_tran.secrouteunkid AND cmexpense_costing_tran.costingunkid = cmclaim_approval_tran.costingunkid  " & _
                        '                       " LEFT JOIN praccount_master ON praccount_master.accountunkid = cmexpense_master.glcodeunkid " & _
                        '                       " LEFT JOIN prcostcenter_master ON prcostcenter_master.costcenterunkid = cmclaim_approval_tran.costcenterunkid " & _
                        '                       " LEFT JOIN cfexchange_rate ON cfexchange_rate.exchangerateunkid = ISNULL(cmclaim_approval_tran.exchangerateunkid,0) " & _
                        '                       " LEFT JOIN hrdependants_beneficiaries_tran ON hrdependants_beneficiaries_tran.dpndtbeneficetranunkid = cmclaim_approval_tran.dpndtbeneficetranunkid AND hrdependants_beneficiaries_tran.isvoid = 0 " & _
                        '                       " LEFT JOIN cfcommon_master AS RL ON RL.masterunkid = hrdependants_beneficiaries_tran.relationunkid AND RL.mastertype = " & clsCommon_Master.enCommonMaster.RELATIONS & " "

                        StrInnerQry &= " LEFT JOIN cmexpense_master ON cmexpense_master.expenseunkid = cmclaim_approval_tran.expenseunkid " & _
                                    " LEFT JOIN cfcommon_master ON cfcommon_master.masterunkid = cmclaim_approval_tran.secrouteunkid AND cfcommon_master.mastertype = " & clsCommon_Master.enCommonMaster.SECTOR_ROUTE & _
                                               " LEFT JOIN cmexpense_costing_tran ON cmexpense_costing_tran.secrouteunkid = cmclaim_approval_tran.secrouteunkid AND cmexpense_costing_tran.costingunkid = cmclaim_approval_tran.costingunkid  " & _
                                               " LEFT JOIN praccount_master ON praccount_master.accountunkid = cmexpense_master.glcodeunkid " & _
                                               " LEFT JOIN prcostcenter_master ON prcostcenter_master.costcenterunkid = cmclaim_approval_tran.costcenterunkid " & _
                                               " LEFT JOIN cfexchange_rate ON cfexchange_rate.exchangerateunkid = ISNULL(cmclaim_approval_tran.exchangerateunkid,0) " & _
                                               " LEFT JOIN hrdependants_beneficiaries_tran ON hrdependants_beneficiaries_tran.dpndtbeneficetranunkid = cmclaim_approval_tran.dpndtbeneficetranunkid AND hrdependants_beneficiaries_tran.isvoid = 0 " & _
                                               " LEFT JOIN cfcommon_master AS RL ON RL.masterunkid = hrdependants_beneficiaries_tran.relationunkid AND RL.mastertype = " & clsCommon_Master.enCommonMaster.RELATIONS & " " & _
                                               " LEFT JOIN cfcommon_master AS cfrt ON cfrt.masterunkid = cmclaim_approval_tran.fromrouteid AND cfrt.mastertype = " & clsCommon_Master.enCommonMaster.SECTOR_ROUTE & " " & _
                                               " LEFT JOIN cfcommon_master AS ctrt ON ctrt.masterunkid = cmclaim_approval_tran.torouteid AND cfrt.mastertype = " & clsCommon_Master.enCommonMaster.SECTOR_ROUTE & " " & _
                                               " LEFT JOIN cfcommon_master AS cmj ON cmj.masterunkid = cmexpense_master.journalunkid AND cmj.mastertype = " & clsCommon_Master.enCommonMaster.CLAIM_JOURNAL & " " & _
                                               " LEFT JOIN cfcommon_master AS cmp ON cmp.masterunkid = cmexpense_master.paymentmodeunkid AND cmp.mastertype = " & clsCommon_Master.enCommonMaster.CLAIM_PAYMENTMODE & " "

                        'Pinkal (23-Dec-2023) -- (A1X-1555)  NMB - P2P Changes for Claim Request and Company Asset.[LEFT JOIN cfcommon_master AS cmj ON cmj.masterunkid = cmexpense_master.jounalunkid AND cmj.mastertype = " & clsCommon_Master.enCommonMaster.CLAIM_JOURNAL  LEFT JOIN cfcommon_master AS cmp ON cmp.masterunkid = cmexpense_master.paymentmodeunkid AND cmp.mastertype = " & clsCommon_Master.enCommonMaster.CLAIM_PAYMENTMODE ]

                        'S.SANDEEP |13-APR-2022| -- END

                        'S.SANDEEP |10-MAR-2022| -- SSUE/ENHANCEMENT : OLD-580 [LEFT JOIN hrdependants_beneficiaries_tran ON hrdependants_beneficiaries_tran.dpndtbeneficetranunkid = cmclaim_approval_tran.dpndtbeneficetranunkid AND hrdependants_beneficiaries_tran.isvoid = 0]

                    End If



                    'Pinkal (13-Aug-2020) -- Start
                    'Bug NMB:  Working on IIS Freezing and Dump Issue for NMB.
                    'Pinkal (13-Aug-2020) -- Start
                    'Bug NMB:  Working on IIS Freezing and Dump Issue for NMB.
                    If IsDistinct Then
                        If blnPaymentApprovalwithLeaveApproval Then
                            StrInnerQry &= " LEFT JOIN ( " & _
                                                    "                    SELECT " & _
                                                    "                            cmclaim_approval_tran.crmasterunkid " & _
                                                    "                           ,cmclaim_approval_tran.crapproverunkid " & _
                                                    "                           ,lvapproverlevel_master.priority AS crpriority " & _
                                                    "                           ,cmclaim_approval_tran.statusunkid " & _
                                                    "                           ,ROW_NUMBER()OVER(PARTITION BY cmclaim_approval_tran.crmasterunkid,lvapproverlevel_master.priority ORDER BY cmclaim_approval_tran.approvaldate DESC) AS rno " & _
                                                    "                           ,CASE WHEN cmclaim_approval_tran.statusunkid = 1 THEN  @ApprovedBy + ' ' + ISNULL(cuser.username, '') " & _
                                                    "                                     WHEN cmclaim_approval_tran.statusunkid = 2 THEN  @Pending " & _
                                                    "                                     WHEN cmclaim_approval_tran.statusunkid = 3 THEN  @RejectedBy + ' ' + ISNULL(cuser.username, '') " & _
                                                    "                             END AS 'AppprovalStatus' " & _
                                                    "                    FROM cmclaim_approval_tran  " & _
                                                    "                    LEFT JOIN lvleaveapprover_master ON lvleaveapprover_master.leaveapproverunkid = cmclaim_approval_tran.approveremployeeunkid AND lvleaveapprover_master.approverunkid = cmclaim_approval_tran.crapproverunkid  " & _
                                                    "                    LEFT JOIN hrapprover_usermapping ON hrapprover_usermapping.approverunkid = lvleaveapprover_master.approverunkid AND usertypeid= " & enUserType.Approver & _
                                                    "                    LEFT JOIN hrmsConfiguration..cfuser_master AS cuser	ON cuser.userunkid = hrapprover_usermapping.userunkid " & _
                                                    "                    LEFT JOIN lvapproverlevel_master ON lvapproverlevel_master.levelunkid = lvleaveapprover_master.levelunkid " & _
                                                    "                    WHERE cmclaim_approval_tran.isvoid= 0 " & _
                                                    "                  ) AS App ON app.crmasterunkid = cmclaim_approval_tran.crmasterunkid " & _
                                                    "  AND app.crpriority = lvapproverlevel_master.priority and app.rno = 1 "

                            'Pinkal (07-Dec-2021) -- Bug Solved For Voltamp For External approval Retrival ["AND lvleaveapprover_master.isexternalapprover = #ExAppr# " ]

                            'Pinkal (04-Aug-2023) -- Problem Resolved for KBC Claim Approval [ AND lvleaveapprover_master.isexternalapprover = #ExAppr# "]

                        Else
                            StrInnerQry &= " LEFT JOIN ( " & _
                                                       "                    SELECT " & _
                                                       "                            cmclaim_approval_tran.crmasterunkid " & _
                                                       "                           ,cmclaim_approval_tran.crapproverunkid " & _
                                                       "                           ,cmapproverlevel_master.crpriority " & _
                                                       "                           ,cmclaim_approval_tran.statusunkid " & _
                                                       "                           ,ROW_NUMBER()OVER(PARTITION BY cmclaim_approval_tran.crmasterunkid,cmapproverlevel_master.crpriority ORDER BY cmclaim_approval_tran.approvaldate DESC) AS rno " & _
                                                       "                           ,CASE WHEN cmclaim_approval_tran.statusunkid = 1 THEN  @ApprovedBy + ' ' +  ISNULL(cuser.username, '') " & _
                                                       "                                     WHEN cmclaim_approval_tran.statusunkid = 2 THEN  @Pending " & _
                                                       "                                     WHEN cmclaim_approval_tran.statusunkid = 3 THEN  @RejectedBy + ' ' +  ISNULL(cuser.username, '') " & _
                                                       "                             END AS 'AppprovalStatus' " & _
                                                       "                    FROM cmclaim_approval_tran  " & _
                                                       "                    LEFT JOIN cmexpapprover_master ON cmexpapprover_master.employeeunkid = cmclaim_approval_tran.approveremployeeunkid AND cmexpapprover_master.crapproverunkid = cmclaim_approval_tran.crapproverunkid " & _
                                                       "                    LEFT JOIN hrapprover_usermapping ON hrapprover_usermapping.approverunkid = cmexpapprover_master.crapproverunkid AND usertypeid= " & enUserType.crApprover & _
                                                       "                    LEFT JOIN hrmsConfiguration..cfuser_master AS cuser	ON cuser.userunkid = hrapprover_usermapping.userunkid " & _
                                                       "                    LEFT JOIN cmapproverlevel_master ON cmapproverlevel_master.crlevelunkid = cmexpapprover_master.crlevelunkid " & _
                                                       "                    WHERE cmclaim_approval_tran.isvoid= 0 " & _
                                                       "                  ) AS App ON app.crmasterunkid = cmclaim_approval_tran.crmasterunkid " & _
                                                       "  AND app.crpriority = cmapproverlevel_master.crpriority and app.rno = 1 "


                            'Pinkal (07-Dec-2021) -- Bug Solved For Voltamp For External approval Retrival ["AND cmexpapprover_master.isexternalapprover = #ExAppr# " & _]

                        End If
                    End If
                    'Pinkal (13-Aug-2020) -- End


                    If xAdvanceJoinQry.Trim.Length > 0 Then
                        StrInnerQry &= xAdvanceJoinQry.Replace("hremployee_master", "h1")
                    End If

                    If isFromApprovalList Then
                        If blnPaymentApprovalwithLeaveApproval Then
                            StrInnerQry &= " WHERE cmclaim_request_master.frommoduleid =  " & enExpFromModuleID.FROM_EXPENSE
                        Else
                            StrInnerQry &= " WHERE cmclaim_request_master.modulerefunkid =  " & enModuleReference.Leave
                        End If
                    Else
                        StrInnerQry &= " WHERE cmclaim_request_master.modulerefunkid =  " & enModuleReference.Leave
                    End If

                    If blnOnlyActive Then
                        StrInnerQry &= " AND cmclaim_approval_tran.isvoid = 0 "
                    End If

                    If intApproverID > 0 Then
                        StrInnerQry &= " AND cmclaim_approval_tran.crapproverunkid = " & intApproverID
                    End If

                    If mstFilter.Trim.Length > 0 Then
                        StrInnerQry &= " AND " & mstFilter
                    End If

                    If intClaimMstId > -1 Then
                        StrInnerQry &= " AND cmclaim_request_master.crmasterunkid = " & intClaimMstId
                    End If

                    strQ &= StrInnerQry
                    strQ = strQ.Replace("#APPR_NAME#", "ISNULL(h2.firstname,'') + ' ' + isnull(h2.surname,'') ")

                    strQ = strQ.Replace("#APPR_EMAIL_VALUE#", "ISNULL(h2.email,'') ")

                    If blnPaymentApprovalwithLeaveApproval Then
                        strQ = strQ.Replace("#EMPL_JOIN#", "JOIN #DName#hremployee_master h2 on h2.employeeunkid = cmclaim_approval_tran.approveremployeeunkid ")
                    Else
                        strQ = strQ.Replace("#EMPL_JOIN#", "LEFT JOIN #DName#hremployee_master h2 on h2.employeeunkid = cmclaim_approval_tran.approveremployeeunkid ")
                    End If
                    strQ = strQ.Replace("#DName#", "")
                    strQ = strQ.Replace("#ExAppr#", "0")

                    For Each dr In dsCompany.Tables(0).Rows
                        If strQ.Trim.Length > 0 Then strQ &= " UNION "
                        strQ &= StrInnerQry & " AND UEmp.companyunkid = " & dr("companyunkid") & " "
                        If dr("companyunkid") <= 0 AndAlso dr("DName").ToString.Trim = "" Then
                            strQ = strQ.Replace("#APPR_NAME#", " ISNULL(UEmp.username,'') ")
                            strQ = strQ.Replace("#APPR_EMAIL_VALUE#", "ISNULL(UEmp.email,'') ")
                            strQ = strQ.Replace("#EMPL_JOIN#", " LEFT JOIN hrmsConfiguration..cfuser_master AS UEmp ON UEmp.userunkid = cmclaim_approval_tran.approveremployeeunkid ")
                        Else
                            strQ = strQ.Replace("#APPR_NAME#", "CASE WHEN ISNULL(h2.firstname, '') + ' ' + ISNULL(h2.surname, '') = ' ' THEN ISNULL(UEmp.username,'') ELSE ISNULL(h2.firstname, '') + ' ' + ISNULL(h2.surname, '') END ")
                            strQ = strQ.Replace("#APPR_EMAIL_VALUE#", "ISNULL(UEmp.email,'') ")
                            strQ = strQ.Replace("#EMPL_JOIN#", " LEFT JOIN hrmsConfiguration..cfuser_master AS UEmp ON UEmp.userunkid = cmclaim_approval_tran.approveremployeeunkid " & _
                                                           " JOIN #DName#hremployee_master h2 on h2.employeeunkid = UEmp.employeeunkid AND h2.isapproved = 1")
                        End If

                        If dr("DName").ToString.Trim.Length > 0 Then
                            strQ = strQ.Replace("#DName#", dr("DName") & "..")
                        Else
                            strQ = strQ.Replace("#DName#", "")
                        End If
                        strQ = strQ.Replace("#ExAppr#", "1")
                    Next

            End Select

            If blnPaymentApprovalwithLeaveApproval Then
                If intExpCategroryId <> enExpenseType.EXP_LEAVE Then
                    strQ &= " ORDER BY cmclaim_request_master.crmasterunkid,cmapproverlevel_master.crpriority "
                Else
                    strQ &= " ORDER BY cmclaim_request_master.crmasterunkid, lvapproverlevel_master.priority "
                End If
            Else
                strQ &= " ORDER BY cmclaim_request_master.crmasterunkid,cmapproverlevel_master.crpriority "
            End If


            objDataOperation.ClearParameters()
            objDataOperation.AddParameter("@Leave", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage("clsExpCommonMethods", 2, "Leave"))
            objDataOperation.AddParameter("@Medical", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage("clsExpCommonMethods", 3, "Medical"))
            objDataOperation.AddParameter("@Training", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage("clsExpCommonMethods", 4, "Training"))
            objDataOperation.AddParameter("@Miscellaneous", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage("clsExpCommonMethods", 8, "Miscellaneous"))

            'Pinkal (11-Sep-2019) -- Start
            'Enhancement NMB - Working On Claim Retirement for NMB.
            objDataOperation.AddParameter("@Imprest", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage("clsExpCommonMethods", 9, "Imprest"))
            'Pinkal (11-Sep-2019) -- End

            'S.SANDEEP |10-MAR-2022| -- START
            'ISSUE/ENHANCEMENT : OLD-580
            objDataOperation.AddParameter("@EXP_REBATE_PRIVILEGE", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage("clsExpCommonMethods", 10, "Rebate Privilege"))
            objDataOperation.AddParameter("@EXP_REBATE_DUTY", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage("clsExpCommonMethods", 11, "Rebate Duty"))
            'S.SANDEEP |10-MAR-2022| -- END


            'S.SANDEEP |27-MAY-2022| -- START
            'ISSUE/ENHANCEMENT : AC2-389
            objDataOperation.AddParameter("@EXP_REBATE_PRIVILEGE_DPNDT", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage("clsExpCommonMethods", 13, "Rebate Privilege Dependant"))
            'S.SANDEEP |27-MAY-2022| -- END

            objDataOperation.AddParameter("@Approve", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage("clsMasterData", 110, "Approved"))
            objDataOperation.AddParameter("@Pending", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage("clsMasterData", 111, "Pending"))
            objDataOperation.AddParameter("@Reject", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage("clsMasterData", 112, "Rejected"))
            objDataOperation.AddParameter("@ReSchedule", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage("clsMasterData", 113, "Re-Scheduled"))
            objDataOperation.AddParameter("@Cancel", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage("clsMasterData", 115, "Cancelled"))
            objDataOperation.AddParameter("@Issued", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage("clsMasterData", 277, "Issued"))

            If IsDistinct = False Then
                objDataOperation.AddParameter("@Qty", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage("clsExpCommonMethods", 6, "Quantity"))
                objDataOperation.AddParameter("@Amt", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage("clsExpCommonMethods", 7, "Amount"))
            End If


            'Pinkal (13-Aug-2020) -- Start
            'Bug NMB:  Working on IIS Freezing and Dump Issue for NMB.
            objDataOperation.AddParameter("@ApprovedBy", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 6, "Approved By :"))
            objDataOperation.AddParameter("@RejectedBy", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 7, "Rejected By :"))
            'Pinkal (13-Aug-2020) -- End


            'S.SANDEEP |10-MAR-2022| -- START
            'ISSUE/ENHANCEMENT : OLD-580
            objDataOperation.AddParameter("@DP", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage("clsclaim_request_tran", 100, "Dependant"))
            objDataOperation.AddParameter("@OH", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage("clsclaim_request_tran", 101, "Other"))
            objDataOperation.AddParameter("@SF", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage("clsclaim_request_tran", 102, "Self"))
            'S.SANDEEP |10-MAR-2022| -- END


            dsList = objDataOperation.ExecQuery(strQ, strTableName)

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: GetApproverExpesneList; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            If objDoOperation Is Nothing Then objDataOperation = Nothing
        End Try
        Return dsList
    End Function

    ''' <summary>
    ''' Modify By: Pinkal Jariwala
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>
    Public Function Insert_Update_ApproverData(ByVal iApproverEmpId As Integer, _
                                               ByVal iApproverTranId As Integer, _
                                               ByVal iStatusId As Integer, _
                                               ByVal iVisibleId As Integer, _
                                               ByVal iUserId As Integer, _
                                               ByVal dtCurrentDateAndTime As DateTime, _
                                               Optional ByVal iClaimMasterId As Integer = 0, _
                                               Optional ByVal iDataOpr As clsDataOperation = Nothing, _
                                               Optional ByVal mstrRejcectRemark As String = "", _
                                               Optional ByVal blnIsLastApprover As Boolean = False, _
                                               Optional ByVal mdtApprovalDate As DateTime = Nothing) As Boolean

        'Shani(24-Aug-2015) -- [dtCurrentDateAndTime]--'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
        'Shani(08-Aug-2015)[mdtApprovalDate 

        Dim i As Integer
        Dim StrQ As String = ""
        Dim exForce As Exception
        Dim dsList As New DataSet
        Dim mblnFlag As Boolean = False

        'Pinkal (10-Jan-2017) -- Start
        'Enhancement - Working on TRA C&R Module Changes with Leave Module.
        Dim objRequestTran As New clsclaim_request_tran
        'Pinkal (10-Jan-2017) -- End

        Try

            If iDataOpr Is Nothing Then
                mblnFlag = True
                iDataOpr = New clsDataOperation
                iDataOpr.BindTransaction()
            End If

            Dim objExpMst As New clsExpense_Master

            For i = 0 To mdtApprTran.Rows.Count - 1
                With mdtApprTran.Rows(i)
                    iDataOpr.ClearParameters()
                    If Not IsDBNull(.Item("AUD")) Then
                        objExpMst._Expenseunkid = CInt(.Item("expenseunkid"))
                        mblnIsConsiderForPayroll = objExpMst._IsConsiderForPayroll
                        Select Case .Item("AUD")
                            Case "A"

                                'Pinkal (10-Jan-2017) -- Start
                                'Enhancement - Working on TRA C&R Module Changes with Leave Module.
                                If CInt(.Item("crtranunkid")) <= 0 Then
                                    objRequestTran._ApproverTranID = iApproverTranId
                                    objRequestTran._ApproverID = iApproverEmpId
                                    objRequestTran._ClaimRequestMasterId = IIf(iClaimMasterId <= 0, .Item("crmasterunkid"), iClaimMasterId)
                                    If objRequestTran.Insert(mdtApprTran.Rows(i), iDataOpr, iUserId) = False Then
                                        exForce = New Exception(iDataOpr.ErrorNumber & ": " & iDataOpr.ErrorMessage)
                                        Throw exForce
                                    End If
                                    .Item("crtranunkid") = objRequestTran._ClaimRequestTranId
                                End If
                                'Pinkal (10-Jan-2017) -- End



                                StrQ = "INSERT INTO cmclaim_approval_tran ( " & _
                                           "  crtranunkid " & _
                                           ", crmasterunkid " & _
                                           ", expenseunkid " & _
                                           ", secrouteunkid " & _
                                           ", costingunkid " & _
                                           ", unitprice " & _
                                           ", quantity " & _
                                           ", amount " & _
                                           ", expense_remark " & _
                                           ", isvoid " & _
                                           ", voiduserunkid " & _
                                           ", voiddatetime " & _
                                           ", voidreason " & _
                                           ", cancelfrommoduleid " & _
                                           ", iscancel " & _
                                           ", canceluserunkid " & _
                                           ", cancel_remark " & _
                                           ", cancel_datetime " & _
                                           ", approveremployeeunkid " & _
                                           ", crapproverunkid " & _
                                           ", statusunkid" & _
                                           ", visibleid " & _
                                           ", userunkid " & _
                                           ", voidloginemployeeunkid " & _
                                           ", approvaldate " & _
                                           ", costcenterunkid " & _
                                           ",countryunkid " & _
                                           ",base_countryunkid " & _
                                           ",base_amount " & _
                                           ",exchangerateunkid " & _
                                           ",exchange_rate " & _
                                           ",airline " & _
                                           ",fromrouteid " & _
                                           ",torouteid " & _
                                           ",flightno " & _
                                           ",traveldate " & _
                                           ",other_person " & _
                                           ",dpndtbeneficetranunkid  " & _
                                       ") VALUES (" & _
                                           "  @crtranunkid " & _
                                           ", @crmasterunkid " & _
                                           ", @expenseunkid " & _
                                           ", @secrouteunkid " & _
                                           ", @costingunkid " & _
                                           ", @unitprice " & _
                                           ", @quantity " & _
                                           ", @amount " & _
                                           ", @expense_remark " & _
                                           ", @isvoid " & _
                                           ", @voiduserunkid " & _
                                           ", @voiddatetime " & _
                                           ", @voidreason " & _
                                           ", @cancelfrommoduleid " & _
                                           ", @iscancel " & _
                                           ", @canceluserunkid " & _
                                           ", @cancel_remark " & _
                                           ", @cancel_datetime " & _
                                           ", @approveremployeeunkid " & _
                                           ", @crapproverunkid " & _
                                           ", @statusunkid" & _
                                           ", @visibleid " & _
                                           ", @userunkid " & _
                                           ", @voidloginemployeeunkid " & _
                                           ", @approvaldate " & _
                                           ", @costcenterunkid " & _
                                           ", @countryunkid " & _
                                           ", @base_countryunkid " & _
                                           ", @base_amount " & _
                                           ", @exchangerateunkid " & _
                                           ", @exchange_rate " & _
                                           ", @airline " & _
                                           ", @fromrouteid " & _
                                           ", @torouteid " & _
                                           ", @flightno " & _
                                           ", @traveldate " & _
                                           ", @other_person " & _
                                           ", @dpndtbeneficetranunkid  " & _
                                       "); SELECT @@identity"

                                'S.SANDEEP |13-APR-2022| -- START 'ISSUE/ENHANCEMENT : AC2-143 {travelfrom -> fromrouteid & travelto -> torouteid} -- END
                                'S.SANDEEP |10-MAR-2022| -- ISSUE/ENHANCEMENT : OLD-580 [airline,travelfrom,travelto,flightno,traveldate,other_person,dpndtbeneficetranunkid]

                                'Pinkal (04-Feb-2019) -- 'Enhancement - Working on Leave & Claim Enhancement Phase 2 for NMB.[", @costcenterunkid, @countryunkid , @base_countryunkid , @base_amount , @exchangerateunkid , @exchange_rate "

                                iDataOpr.AddParameter("@crtranunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, .Item("crtranunkid"))
                                iDataOpr.AddParameter("@crmasterunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, IIf(iClaimMasterId <= 0, .Item("crmasterunkid"), iClaimMasterId))
                                iDataOpr.AddParameter("@expenseunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, .Item("expenseunkid"))
                                iDataOpr.AddParameter("@secrouteunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, .Item("secrouteunkid"))
                                iDataOpr.AddParameter("@costingunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, .Item("costingunkid"))
                                iDataOpr.AddParameter("@unitprice", SqlDbType.Decimal, eZeeDataType.DECIMAL_SIZE, .Item("unitprice"))
                                iDataOpr.AddParameter("@quantity", SqlDbType.Float, eZeeDataType.FLOAT_SIZE, .Item("quantity"))
                                iDataOpr.AddParameter("@amount", SqlDbType.Decimal, eZeeDataType.DECIMAL_SIZE, .Item("amount"))
                                iDataOpr.AddParameter("@expense_remark", SqlDbType.NText, eZeeDataType.NAME_SIZE, .Item("expense_remark"))
                                iDataOpr.AddParameter("@isvoid", SqlDbType.Bit, eZeeDataType.BIT_SIZE, .Item("isvoid"))
                                iDataOpr.AddParameter("@voiduserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, .Item("voiduserunkid"))
                                If IsDBNull(.Item("voiddatetime")) = False Then
                                    iDataOpr.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, .Item("voiddatetime"))
                                Else
                                    iDataOpr.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, DBNull.Value)
                                End If
                                iDataOpr.AddParameter("@voidreason", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, .Item("voidreason"))
                                iDataOpr.AddParameter("@cancelfrommoduleid", SqlDbType.Int, eZeeDataType.INT_SIZE, 0)
                                iDataOpr.AddParameter("@iscancel", SqlDbType.Bit, eZeeDataType.BIT_SIZE, False)
                                iDataOpr.AddParameter("@canceluserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, -1)
                                iDataOpr.AddParameter("@cancel_remark", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, "")
                                iDataOpr.AddParameter("@cancel_datetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, DBNull.Value)
                                iDataOpr.AddParameter("@approveremployeeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, iApproverEmpId)
                                iDataOpr.AddParameter("@crapproverunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, iApproverTranId)
                                iDataOpr.AddParameter("@statusunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, iStatusId)
                                iDataOpr.AddParameter("@visibleid", SqlDbType.Int, eZeeDataType.INT_SIZE, iVisibleId)
                                iDataOpr.AddParameter("@userunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, iUserId)
                                iDataOpr.AddParameter("@voidloginemployeeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, .Item("voidloginemployeeunkid"))

                                'Pinkal (10-Jan-2017) -- Start
                                'Enhancement - Working on TRA C&R Module Changes with Leave Module.
                                iDataOpr.AddParameter("@approvaldate", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, IIf(mdtApprovalDate <> Nothing, mdtApprovalDate, DBNull.Value))
                                'Pinkal (10-Jan-2017) -- End


                                'Pinkal (04-Feb-2019) -- Start
                                'Enhancement - Working on Leave & Claim Enhancement Phase 2 for NMB.
                                iDataOpr.AddParameter("@costcenterunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, .Item("costcenterunkid"))
                                'Pinkal (04-Feb-2019) -- End


                                'Pinkal (04-Feb-2019) -- Start
                                'Enhancement - Working on Leave & Claim Enhancement Phase 2 for NMB.
                                iDataOpr.AddParameter("@countryunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, .Item("countryunkid"))
                                iDataOpr.AddParameter("@base_countryunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, .Item("base_countryunkid"))
                                iDataOpr.AddParameter("@base_amount", SqlDbType.Decimal, eZeeDataType.DECIMAL_SIZE, .Item("base_amount"))
                                iDataOpr.AddParameter("@exchangerateunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, .Item("exchangerateunkid"))
                                iDataOpr.AddParameter("@exchange_rate", SqlDbType.Decimal, eZeeDataType.DECIMAL_SIZE, .Item("exchange_rate"))
                                'Pinkal (04-Feb-2019) -- End

                                'S.SANDEEP |10-MAR-2022| -- START
                                'ISSUE/ENHANCEMENT : OLD-580
                                iDataOpr.AddParameter("@airline", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, .Item("airline"))

                                'S.SANDEEP |13-APR-2022| -- START
                                'ISSUE/ENHANCEMENT : AC2-143
                                'iDataOpr.AddParameter("@travelfrom", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, .Item("travelfrom"))
                                'iDataOpr.AddParameter("@travelto", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, .Item("travelto"))

                                iDataOpr.AddParameter("@fromrouteid", SqlDbType.Int, eZeeDataType.INT_SIZE, .Item("fromrouteid"))
                                iDataOpr.AddParameter("@torouteid", SqlDbType.Int, eZeeDataType.INT_SIZE, .Item("torouteid"))
                                'S.SANDEEP |13-APR-2022| -- END

                                iDataOpr.AddParameter("@flightno", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, .Item("flightno"))
                                If IsDBNull(.Item("traveldate")) = False Then
                                    iDataOpr.AddParameter("@traveldate", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, .Item("traveldate"))
                                Else
                                    iDataOpr.AddParameter("@traveldate", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, DBNull.Value)
                                End If
                                iDataOpr.AddParameter("@other_person", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, .Item("other_person"))
                                iDataOpr.AddParameter("@dpndtbeneficetranunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, .Item("dpndtbeneficetranunkid"))
                                'S.SANDEEP |10-MAR-2022| -- END

                                dsList = iDataOpr.ExecQuery(StrQ, "List")

                                If iDataOpr.ErrorMessage <> "" Then
                                    exForce = New Exception(iDataOpr.ErrorNumber & ": " & iDataOpr.ErrorMessage)
                                    Throw exForce
                                End If

                                mintClaimApprovalTranId = dsList.Tables(0).Rows(0).Item(0)


                                If blnIsLastApprover AndAlso mblnIsConsiderForPayroll AndAlso iStatusId = 1 Then
                                    objClaimProcessTran._Crapprovaltranunkid = mintClaimApprovalTranId
                                    objClaimProcessTran._Employeeunkid = mintEmployeeID
                                    objClaimProcessTran._Periodunkid = -1
                                    objClaimProcessTran._Userunkid = iUserId
                                    objClaimProcessTran._Approveremployeeunkid = iApproverEmpId
                                    objClaimProcessTran._Crapproverunkid = iApproverTranId

                                    With objClaimProcessTran
                                        ._FormName = mstrFormName
                                        ._LoginEmployeeunkid = mintLoginEmployeeunkid
                                        ._ClientIP = mstrClientIP
                                        ._HostName = mstrHostName
                                        ._FromWeb = mblnIsWeb
                                        ._AuditUserId = mintAuditUserId
                                        ._CompanyUnkid = mintCompanyUnkid
                                        ._AuditDate = mdtAuditDate
                                    End With

                                    If objClaimProcessTran.Insert(mdtApprTran.Rows(i), dtCurrentDateAndTime, iDataOpr) = False Then
                                        exForce = New Exception(iDataOpr.ErrorNumber & ": " & iDataOpr.ErrorMessage)
                                        Throw exForce
                                    End If
                                End If


                                If .Item("crmasterunkid") > 0 Then
                                    'S.SANDEEP [28-May-2018] -- START
                                    'ISSUE/ENHANCEMENT : {Audit Trails} 
                                    Dim objCommonATLog As New clsCommonATLog
                                    objCommonATLog._FormName = mstrFormName
                                    objCommonATLog._LoginEmployeeUnkid = mintLoginEmployeeunkid
                                    objCommonATLog._ClientIP = mstrClientIP
                                    objCommonATLog._HostName = mstrHostName
                                    objCommonATLog._FromWeb = mblnIsWeb
                                    objCommonATLog._AuditUserId = mintAuditUserId
                                    objCommonATLog._CompanyUnkid = mintCompanyUnkid
                                    objCommonATLog._AuditDate = mdtAuditDate
                                    'S.SANDEEP [28-May-2018] -- END

                                    If objCommonATLog.Insert_TranAtLog(iDataOpr, "cmclaim_request_master", "crmasterunkid", .Item("crmasterunkid"), "cmclaim_approval_tran", "crapprovaltranunkid", mintClaimApprovalTranId, 2, 1, , iUserId) = False Then
                                        exForce = New Exception(iDataOpr.ErrorNumber & ": " & iDataOpr.ErrorMessage)
                                        Throw exForce
                                    End If

                                    'S.SANDEEP [28-May-2018] -- START
                                    'ISSUE/ENHANCEMENT : {Audit Trails} 
                                    objCommonATLog = Nothing
                                    'S.SANDEEP [28-May-2018] -- END

                                Else
                                    'S.SANDEEP [28-May-2018] -- START
                                    'ISSUE/ENHANCEMENT : {Audit Trails} 
                                    Dim objCommonATLog As New clsCommonATLog
                                    objCommonATLog._FormName = mstrFormName
                                    objCommonATLog._LoginEmployeeUnkid = mintLoginEmployeeunkid
                                    objCommonATLog._ClientIP = mstrClientIP
                                    objCommonATLog._HostName = mstrHostName
                                    objCommonATLog._FromWeb = mblnIsWeb
                                    objCommonATLog._AuditUserId = mintAuditUserId
                                    objCommonATLog._CompanyUnkid = mintCompanyUnkid
                                    objCommonATLog._AuditDate = mdtAuditDate
                                    'S.SANDEEP [28-May-2018] -- END

                                    If objCommonATLog.Insert_TranAtLog(iDataOpr, "cmclaim_request_master", "crmasterunkid", IIf(iClaimMasterId <= 0, .Item("crmasterunkid"), iClaimMasterId), "cmclaim_approval_tran", "crapprovaltranunkid", mintClaimApprovalTranId, 2, 1, , iUserId) = False Then
                                        exForce = New Exception(iDataOpr.ErrorNumber & ": " & iDataOpr.ErrorMessage)
                                        Throw exForce
                                    End If

                                    'S.SANDEEP [28-May-2018] -- START
                                    'ISSUE/ENHANCEMENT : {Audit Trails} 
                                    objCommonATLog = Nothing
                                    'S.SANDEEP [28-May-2018] -- END

                                End If

                            Case "U"


                                mintClaimApprovalTranId = isExist(iDataOpr, iApproverTranId, iClaimMasterId, .Item("crtranunkid"), .Item("expenseunkid"))

                                'Shani(08-Aug-2015) -- Start
                                'Enhancement - C&R Enhancement Given by glory(CR Revised.docx)
                                'StrQ = "UPDATE cmclaim_approval_tran SET " & _
                                '        "  crtranunkid = @crtranunkid" & _
                                '        ", crmasterunkid = @crmasterunkid" & _
                                '        ", expenseunkid = @expenseunkid" & _
                                '        ", secrouteunkid  = @secrouteunkid " & _
                                '        ", costingunkid = @costingunkid" & _
                                '        ", unitprice = @unitprice" & _
                                '        ", quantity = @quantity" & _
                                '        ", amount = @amount" & _
                                '        ", expense_remark = @expense_remark" & _
                                '        ", isvoid = @isvoid" & _
                                '        ", voiduserunkid = @voiduserunkid" & _
                                '        ", voiddatetime = @voiddatetime" & _
                                '        ", voidreason = @voidreason" & _
                                '        ", cancelfrommoduleid = @cancelfrommoduleid " & _
                                '        ", iscancel = @iscancel" & _
                                '        ", canceluserunkid = @canceluserunkid" & _
                                '        ", cancel_remark = @cancel_remark" & _
                                '        ", cancel_datetime = @cancel_datetime" & _
                                '        ", approveremployeeunkid = @approveremployeeunkid" & _
                                '        ", crapproverunkid = @crapproverunkid" & _
                                '        ", statusunkid = @statusunkid " & _
                                '        ", visibleid = @visibleid " & _
                                '        ", userunkid = @userunkid " & _
                                '        ", voidloginemployeeunkid = @voidloginemployeeunkid " & _
                                '      "WHERE crmasterunkid = @crmasterunkid AND crapproverunkid = @crapproverunkid " & _
                                '      " AND  crtranunkid = @crtranunkid "
                                StrQ = "UPDATE cmclaim_approval_tran SET " & _
                                        "  crtranunkid = @crtranunkid" & _
                                        ", crmasterunkid = @crmasterunkid" & _
                                        ", expenseunkid = @expenseunkid" & _
                                        ", secrouteunkid  = @secrouteunkid " & _
                                        ", costingunkid = @costingunkid" & _
                                        ", unitprice = @unitprice" & _
                                        ", quantity = @quantity" & _
                                        ", amount = @amount" & _
                                        ", expense_remark = @expense_remark" & _
                                        ", cancelfrommoduleid = @cancelfrommoduleid " & _
                                        ", iscancel = @iscancel" & _
                                        ", canceluserunkid = @canceluserunkid" & _
                                        ", cancel_remark = @cancel_remark" & _
                                        ", cancel_datetime = @cancel_datetime" & _
                                        ", approveremployeeunkid = @approveremployeeunkid" & _
                                        ", crapproverunkid = @crapproverunkid" & _
                                        ", statusunkid = @statusunkid " & _
                                        ", visibleid = @visibleid " & _
                                        ", userunkid = @userunkid " & _
                                        ", voidloginemployeeunkid = @voidloginemployeeunkid "

                                If mdtApprovalDate <> Nothing Then
                                    StrQ &= ", approvaldate = @approvaldate "
                                End If


                                'Pinkal (04-Feb-2019) -- Start
                                'Enhancement - Working on Leave & Claim Enhancement Phase 2 for NMB.
                                StrQ &= ", costcenterunkid = @costcenterunkid " & _
                                                ", countryunkid = @countryunkid " & _
                                                ", base_countryunkid = @base_countryunkid " & _
                                                ", base_amount = @base_amount " & _
                                                ", exchangerateunkid = @exchangerateunkid " & _
                                                ", exchange_rate = @exchange_rate " & _
                                                ", airline = @airline " & _
                                                ", torouteid = @torouteid " & _
                                                ", fromrouteid = @fromrouteid " & _
                                                ", flightno = @flightno " & _
                                                ", traveldate = @traveldate " & _
                                                ", other_person  = @other_person " & _
                                                ", dpndtbeneficetranunkid = @dpndtbeneficetranunkid "

                                'S.SANDEEP |13-APR-2022| -- START 'ISSUE/ENHANCEMENT : AC2-143 {travelfrom -> fromrouteid & travelto -> torouteid} -- END
                                'S.SANDEEP |10-MAR-2022| -- START ISSUE/ENHANCEMENT : OLD-580 [airline = @airline,travelfrom = @travelfrom,travelto = @travelto,flightno = @flightno, traveldate = @traveldate,other_person  = @other_person, dpndtbeneficetranunkid = @dpndtbeneficetranunkid]

                                StrQ &= "WHERE crmasterunkid = @crmasterunkid AND crapproverunkid = @crapproverunkid " & _
                                      " AND  crtranunkid = @crtranunkid "

                                'Pinkal (10-Jan-2017) -- Start
                                'Enhancement - Working on TRA C&R Module Changes with Leave Module.
                                StrQ &= " AND expenseunkid = @expenseunkid"
                                'Pinkal (10-Jan-2017) -- End


                                'iDataOpr.AddParameter("@crapprovaltranunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, .Item("crapprovaltranunkid"))
                                iDataOpr.AddParameter("@crtranunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, .Item("crtranunkid"))
                                iDataOpr.AddParameter("@crmasterunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, IIf(iClaimMasterId <= 0, .Item("crmasterunkid"), iClaimMasterId))
                                iDataOpr.AddParameter("@expenseunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, .Item("expenseunkid"))
                                iDataOpr.AddParameter("@secrouteunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, .Item("secrouteunkid"))
                                iDataOpr.AddParameter("@costingunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, .Item("costingunkid"))
                                iDataOpr.AddParameter("@unitprice", SqlDbType.Decimal, eZeeDataType.DECIMAL_SIZE, .Item("unitprice"))
                                iDataOpr.AddParameter("@quantity", SqlDbType.Float, eZeeDataType.FLOAT_SIZE, .Item("quantity"))
                                iDataOpr.AddParameter("@amount", SqlDbType.Decimal, eZeeDataType.DECIMAL_SIZE, .Item("amount"))
                                iDataOpr.AddParameter("@expense_remark", SqlDbType.NText, eZeeDataType.NAME_SIZE, .Item("expense_remark"))


                                iDataOpr.AddParameter("@cancelfrommoduleid", SqlDbType.Int, eZeeDataType.INT_SIZE, 0)
                                iDataOpr.AddParameter("@iscancel", SqlDbType.Bit, eZeeDataType.BIT_SIZE, False)
                                iDataOpr.AddParameter("@canceluserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, -1)
                                iDataOpr.AddParameter("@cancel_remark", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, "")
                                iDataOpr.AddParameter("@cancel_datetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, DBNull.Value)
                                iDataOpr.AddParameter("@approveremployeeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, iApproverEmpId)
                                iDataOpr.AddParameter("@crapproverunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, iApproverTranId)
                                iDataOpr.AddParameter("@statusunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, iStatusId)
                                iDataOpr.AddParameter("@visibleid", SqlDbType.Int, eZeeDataType.INT_SIZE, iVisibleId)
                                'Shani(08-Aug-2015) -- Start
                                'Enhancement - C&R Enhancement Given by glory(CR Revised.docx)
                                If mdtApprovalDate <> Nothing Then
                                    iDataOpr.AddParameter("@approvaldate", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, mdtApprovalDate)
                                End If
                                'Shani(08-Aug-2015) -- End

                                'Pinkal (16-Dec-2014) -- Start
                                'Enhancement - Claim & Request For Web.
                                iDataOpr.AddParameter("@userunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, iUserId)
                                iDataOpr.AddParameter("@voidloginemployeeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, .Item("voidloginemployeeunkid"))
                                'Pinkal (16-Dec-2014) -- End


                                'Pinkal (04-Feb-2019) -- Start
                                'Enhancement - Working on Leave & Claim Enhancement Phase 2 for NMB.
                                iDataOpr.AddParameter("@costcenterunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, .Item("costcenterunkid"))
                                iDataOpr.AddParameter("@countryunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, .Item("countryunkid"))
                                iDataOpr.AddParameter("@base_countryunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, .Item("base_countryunkid"))
                                iDataOpr.AddParameter("@base_amount", SqlDbType.Decimal, eZeeDataType.DECIMAL_SIZE, .Item("base_amount"))
                                iDataOpr.AddParameter("@exchangerateunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, .Item("exchangerateunkid"))
                                iDataOpr.AddParameter("@exchange_rate", SqlDbType.Decimal, eZeeDataType.DECIMAL_SIZE, .Item("exchange_rate"))
                                'Pinkal (04-Feb-2019) -- End

                                'S.SANDEEP |10-MAR-2022| -- START
                                'ISSUE/ENHANCEMENT : OLD-580
                                iDataOpr.AddParameter("@airline", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, .Item("airline"))

                                'S.SANDEEP |13-APR-2022| -- START
                                'ISSUE/ENHANCEMENT : AC2-143
                                'iDataOpr.AddParameter("@travelfrom", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, .Item("travelfrom"))
                                'iDataOpr.AddParameter("@travelto", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, .Item("travelto"))

                                iDataOpr.AddParameter("@fromrouteid", SqlDbType.Int, eZeeDataType.INT_SIZE, .Item("fromrouteid"))
                                iDataOpr.AddParameter("@torouteid", SqlDbType.Int, eZeeDataType.INT_SIZE, .Item("torouteid"))
                                'S.SANDEEP |13-APR-2022| -- END

                                iDataOpr.AddParameter("@flightno", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, .Item("flightno"))
                                If IsDBNull(.Item("traveldate")) = False Then
                                    iDataOpr.AddParameter("@traveldate", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, .Item("traveldate"))
                                Else
                                    iDataOpr.AddParameter("@traveldate", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, DBNull.Value)
                                End If
                                iDataOpr.AddParameter("@other_person", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, .Item("other_person"))
                                iDataOpr.AddParameter("@dpndtbeneficetranunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, .Item("dpndtbeneficetranunkid"))
                                'S.SANDEEP |10-MAR-2022| -- END

                                Call iDataOpr.ExecNonQuery(StrQ)

                                If iDataOpr.ErrorMessage <> "" Then
                                    exForce = New Exception(iDataOpr.ErrorNumber & ": " & iDataOpr.ErrorMessage)
                                    Throw exForce
                                End If

                                'S.SANDEEP [28-May-2018] -- START
                                'ISSUE/ENHANCEMENT : {Audit Trails} 
                                Dim objCommonATLog As New clsCommonATLog
                                objCommonATLog._FormName = mstrFormName
                                objCommonATLog._LoginEmployeeUnkid = mintLoginEmployeeunkid
                                objCommonATLog._ClientIP = mstrClientIP
                                objCommonATLog._HostName = mstrHostName
                                objCommonATLog._FromWeb = mblnIsWeb
                                objCommonATLog._AuditUserId = mintAuditUserId
                                objCommonATLog._CompanyUnkid = mintCompanyUnkid
                                objCommonATLog._AuditDate = mdtAuditDate
                                'S.SANDEEP [28-May-2018] -- END

                                If objCommonATLog.Insert_TranAtLog(iDataOpr, "cmclaim_request_master", "crmasterunkid", .Item("crmasterunkid"), "cmclaim_approval_tran", "crapprovaltranunkid", mintClaimApprovalTranId, 2, 2, , iUserId) = False Then
                                    exForce = New Exception(iDataOpr.ErrorNumber & ": " & iDataOpr.ErrorMessage)
                                    Throw exForce
                                End If

                                'S.SANDEEP [28-May-2018] -- START
                                'ISSUE/ENHANCEMENT : {Audit Trails} 
                                objCommonATLog = Nothing
                                'S.SANDEEP [28-May-2018] -- END



                                If blnIsLastApprover AndAlso mblnIsConsiderForPayroll AndAlso iStatusId = 1 Then
                                    objClaimProcessTran._Crapprovaltranunkid = mintClaimApprovalTranId
                                    objClaimProcessTran._Employeeunkid = mintEmployeeID
                                    objClaimProcessTran._Periodunkid = -1
                                    objClaimProcessTran._Userunkid = iUserId
                                    objClaimProcessTran._Approveremployeeunkid = iApproverEmpId
                                    objClaimProcessTran._Crapproverunkid = iApproverTranId

                                    With objClaimProcessTran
                                        ._FormName = mstrFormName
                                        ._LoginEmployeeunkid = mintLoginEmployeeunkid
                                        ._ClientIP = mstrClientIP
                                        ._HostName = mstrHostName
                                        ._FromWeb = mblnIsWeb
                                        ._AuditUserId = mintAuditUserId
                                        ._CompanyUnkid = mintCompanyUnkid
                                        ._AuditDate = mdtAuditDate
                                    End With

                                    'Shani(24-Aug-2015) -- Start
                                    'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
                                    'If objClaimProcessTran.Insert(mdtApprTran.Rows(i), iDataOpr) = False Then
                                    If objClaimProcessTran.Insert(mdtApprTran.Rows(i), dtCurrentDateAndTime, iDataOpr) = False Then
                                        'Shani(24-Aug-2015) -- End

                                        exForce = New Exception(iDataOpr.ErrorNumber & ": " & iDataOpr.ErrorMessage)
                                        Throw exForce
                                    End If
                                End If

                            Case "D"

                                If mdtApprTran.Columns.Contains("crapprovaltranunkid") Then
                                    If .Item("crapprovaltranunkid") > 0 Then
                                        mintClaimApprovalTranId = .Item("crapprovaltranunkid")
                                    Else
                                        mintClaimApprovalTranId = isExist(iDataOpr, iApproverTranId, iClaimMasterId, .Item("crtranunkid"), .Item("expenseunkid"))
                                    End If
                                Else
                                    mintClaimApprovalTranId = isExist(iDataOpr, iApproverTranId, iClaimMasterId, .Item("crtranunkid"), .Item("expenseunkid"))
                                End If

                                If mintClaimApprovalTranId > 0 Then
                                    'S.SANDEEP [28-May-2018] -- START
                                    'ISSUE/ENHANCEMENT : {Audit Trails} 
                                    Dim objCommonATLog As New clsCommonATLog
                                    objCommonATLog._FormName = mstrFormName
                                    objCommonATLog._LoginEmployeeUnkid = mintLoginEmployeeunkid
                                    objCommonATLog._ClientIP = mstrClientIP
                                    objCommonATLog._HostName = mstrHostName
                                    objCommonATLog._FromWeb = mblnIsWeb
                                    objCommonATLog._AuditUserId = mintAuditUserId
                                    objCommonATLog._CompanyUnkid = mintCompanyUnkid
                                    objCommonATLog._AuditDate = mdtAuditDate
                                    'S.SANDEEP [28-May-2018] -- END

                                    If objCommonATLog.Insert_TranAtLog(iDataOpr, "cmclaim_request_master", "crmasterunkid", .Item("crmasterunkid"), "cmclaim_approval_tran", "crapprovaltranunkid", mintClaimApprovalTranId, 2, 3, , iUserId) = False Then
                                        exForce = New Exception(iDataOpr.ErrorNumber & ": " & iDataOpr.ErrorMessage)
                                        Throw exForce
                                    End If

                                    'S.SANDEEP [28-May-2018] -- START
                                    'ISSUE/ENHANCEMENT : {Audit Trails} 
                                    objCommonATLog = Nothing
                                    'S.SANDEEP [28-May-2018] -- END

                                End If

                                StrQ = "UPDATE cmclaim_approval_tran SET " & _
                                       "  isvoid = @isvoid" & _
                                       ", voiduserunkid = @voiduserunkid" & _
                                       ", voidloginemployeeunkid = @voidloginemployeeunkid" & _
                                       ", voiddatetime = @voiddatetime" & _
                                       ", voidreason = @voidreason " & _
                                       "WHERE crapprovaltranunkid = @crapprovaltranunkid "

                                iDataOpr.AddParameter("@crapprovaltranunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintClaimApprovalTranId)
                                iDataOpr.AddParameter("@isvoid", SqlDbType.Bit, eZeeDataType.BIT_SIZE, .Item("isvoid"))

                                'Pinkal (16-Dec-2014) -- Start
                                'Enhancement - Claim & Request For Web.
                                iDataOpr.AddParameter("@voidloginemployeeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, .Item("voidloginemployeeunkid"))
                                iDataOpr.AddParameter("@voiduserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, .Item("voiduserunkid"))
                                'Pinkal (16-Dec-2014) -- End

                                iDataOpr.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, .Item("voiddatetime"))
                                iDataOpr.AddParameter("@voidreason", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, .Item("voidreason"))

                                Call iDataOpr.ExecNonQuery(StrQ)

                                If iDataOpr.ErrorMessage <> "" Then
                                    exForce = New Exception(iDataOpr.ErrorNumber & ": " & iDataOpr.ErrorMessage)
                                    Throw exForce
                                End If

                                'If blnIsLastApprover AndAlso mblnIsConsiderForPayroll AndAlso iStatusId = 1 Then
                                '    objClaimProcessTran._Crapprovaltranunkid = mintClaimApprovalTranId
                                '    objClaimProcessTran._Employeeunkid = mintEmployeeID
                                '    objClaimProcessTran._Periodunkid = -1
                                '    objClaimProcessTran._Userunkid = iUserId
                                '
                                '    With objClaimProcessTran
                                '        ._FormName = mstrFormName
                                '        ._LoginEmployeeunkid = mintLoginEmployeeunkid
                                '        ._ClientIP = mstrClientIP
                                '        ._HostName = mstrHostName
                                '        ._FromWeb = mblnIsWeb
                                '        ._AuditUserId = mintAuditUserId
                                '        ._CompanyUnkid = mintCompanyUnkid
                                '        ._AuditDate = mdtAuditDate
                                '    End With
                                '
                                '    'Shani(24-Aug-2015) -- Start
                                '    'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
                                '    'If objClaimProcessTran.Insert(mdtApprTran.Rows(i), iDataOpr) = False Then
                                '    If objClaimProcessTran.Insert(mdtApprTran.Rows(i), dtCurrentDateAndTime, iDataOpr) = False Then
                                '        'Shani(24-Aug-2015) -- End
                                '
                                '        exForce = New Exception(iDataOpr.ErrorNumber & ": " & iDataOpr.ErrorMessage)
                                '        Throw exForce
                                '    End If
                                'End If
                                'Pinkal (04-Feb-2019) -- Start

                        End Select
                    End If
                End With
            Next
            objExpMst = Nothing

            If blnIsLastApprover Then
                _FormName = mstrFormName
                _LoginEmployeeunkid = mintLoginEmployeeunkid
                _ClientIP = mstrClientIP
                _HostName = mstrHostName
                _FromWeb = mblnIsWeb
                _AuditUserId = mintAuditUserId
                _AuditDate = mdtAuditDate
                If UpdateExpenseBalance(iDataOpr, mdtApprTran, iStatusId, iUserId, iClaimMasterId, mstrRejcectRemark) = False Then
                    exForce = New Exception(iDataOpr.ErrorNumber & ": " & iDataOpr.ErrorMessage)
                    Throw exForce
                End If
            End If

            If mblnFlag Then
                iDataOpr.ReleaseTransaction(True)
            End If

            'Pinkal (16-Oct-2023) -- Start
            '(A1X-1399) NMB - Post approved claims amounts to P2P.
            If blnIsLastApprover AndAlso iStatusId = 1 Then 'Approved Only

                Dim objMasterData As New clsMasterData
                Dim objP2PaymentJournalService As New clsP2PaymentJournalService
                Dim mstrError As String = String.Empty
                Dim xPostedData As String = ""
                Dim xResponseData As String = ""
                Dim xHttpStatusCode As Integer = 0

                Dim objConfig As New clsConfigOptions
                Dim mstrNewRequisitionRequestP2PServiceURL As String = objConfig.GetKeyValue(mintCompanyId, "NewRequisitionRequestP2PServiceURL", Nothing)
                objConfig = Nothing

                If mstrNewRequisitionRequestP2PServiceURL.Trim.Length > 0 Then   '

                    Dim mstrToken As String = ""
                    mstrToken = objMasterData.GetP2PToken(mintCompanyId, Nothing)

                If mstrToken.Trim.Length > 0 Then

                    objP2PaymentJournalService.paymentRequest.BatchNumber = mdtApprTran.Rows(0)("claimrequestno").ToString()
                    objP2PaymentJournalService.paymentRequest.PostingDate = mdtApprovalDate.ToString("yyyy-MM-dd")
                    objP2PaymentJournalService.paymentRequest.CompanyCode = mstrCompanyCode
                    'objP2PaymentJournalService.paymentRequest.CompanyCode = "usmf"

                        'Pinkal (23-Dec-2023) -- Start
                        '(A1X-1555)  NMB - P2P Changes for Claim Request and Company Asset.

                        objP2PaymentJournalService.paymentRequest.JournalName = mdtApprTran.Rows(0)("journal").ToString()

                        Dim mstrDimensionValue As String = ""

                        Dim objEmployee As New clsEmployee_Master
                        objEmployee._Employeeunkid(mdtApprovalDate) = mintEmployeeID

                        Dim objUnitGroup As New clsUnitGroup
                        objUnitGroup._Unitgroupunkid = objEmployee._Unitgroupunkid
                        Dim mstrUnitGroupCode As String = objUnitGroup._Code.ToString().Trim()
                        objUnitGroup = Nothing

                        Dim objDepartment As New clsDepartment
                        objDepartment._Departmentunkid = objEmployee._Departmentunkid
                        Dim mstrDepartmentCode As String = objDepartment._Code.ToString().Trim
                        objDepartment = Nothing

                        Dim objSectionGroup As New clsSectionGroup
                        objSectionGroup._Sectiongroupunkid = objEmployee._Sectiongroupunkid
                        Dim mstrSectionGroupCode As String = objSectionGroup._Code.ToString().Trim
                        objSectionGroup = Nothing

                        Dim objClassGroup As New clsClassGroup
                        objClassGroup._Classgroupunkid = objEmployee._Classgroupunkid
                        Dim mstrClassGroupCode As String = objClassGroup._Code.ToString().Trim()
                        objClassGroup = Nothing

                        Dim objClass As New clsClass
                        objClass._Classesunkid = objEmployee._Classunkid
                        Dim mstrClassCode As String = objClass._Code.ToString().Trim()
                        objClass = Nothing

                        objEmployee = Nothing

                        'mstrDimensionValue = "NMB" & IIf(mstrUnitGroupCode.Trim.Length > 0, "-" & mstrUnitGroupCode, "") & If(mstrDepartmentCode.Trim.Length > 0, "-" & mstrDepartmentCode, "") & _
                        '                                  IIf(mstrSectionGroupCode.Trim.Length > 0, "-" & mstrSectionGroupCode, "") & IIf(mstrClassGroupCode.Trim.Length > 0, "-" & mstrClassGroupCode, "") & _
                        '                                  IIf(mstrClassCode.Trim.Length > 0, "-" & mstrClassCode, "")

                        mstrDimensionValue = mstrUnitGroupCode.Trim() & "-" & mstrDepartmentCode.Trim() & "-" & mstrSectionGroupCode.Trim() & "-" & mstrClassGroupCode.Trim() & "-" & mstrClassCode.Trim()

                        Dim mstrCurrency As String = ""
                        Dim objExchange As New clsExchangeRate
                        Dim dsCurrency As DataSet = objExchange.getComboList("List", False, True)

                        If dsCurrency IsNot Nothing AndAlso dsCurrency.Tables(0).Rows.Count > 0 Then
                            mstrCurrency = dsCurrency.Tables(0).Rows(0)("currency_sign").ToString()
                        End If
                        objExchange = Nothing

                        'Pinkal (23-Dec-2023) -- End

                    For p As Integer = 0 To mdtApprTran.Rows.Count - 1

                            'Pinkal (23-Dec-2023) -- Start
                            '(A1X-1555)  NMB - P2P Changes for Claim Request and Company Asset.

                            If mstrCurrency.Trim.Length <= 0 Then
                                mstrCurrency = mdtApprTran.Rows(p)("currency_sign").ToString()
                            End If

                            Dim mdecAmount As Decimal = 0
                            mdecAmount = CDec(mdtApprTran.Rows(p)("base_amount"))


                            objP2PaymentJournalService.paymentRequest.PaymentJournals.Add(New PaymentJournals(mstrCurrency, IIf(CBool(mdtApprTran.Rows(p)("isdebit")), mdecAmount, 0.0), IIf(CBool(mdtApprTran.Rows(p)("iscredit")), mdecAmount, 0.0) _
                                                                                                                              , mdtApprTran.Rows(p)("employeecode").ToString(), mstrDimensionValue.Trim(), eZeeDate.convertDate(mdtApprTran.Rows(p)("tdate").ToString()).ToString("yyyy-MM-dd"), mdtApprTran.Rows(p)("claimrequestno").ToString() _
                                                                                                                              , mdtApprTran.Rows(p)("expense").ToString(), mdtApprTran.Rows(p)("claim_remark").ToString(), "Ledger" _
                                                                                                                              , mdtApprTran.Rows(p)("glcode").ToString() & "-" & mstrDimensionValue.Trim(), mdtApprTran.Rows(p)("paymentmode").ToString()))

                        'objP2PaymentJournalService.paymentRequest.PaymentJournals.Add(New PaymentJournal(mdtApprTran.Rows(p)("currency_sign").ToString(), CDec(mdtApprTran.Rows(p)("amount")), "US_TX_013", mdtApprTran.Rows(p)("employeename").ToString(), mdtApprTran.Rows(p)("costcentername").ToString() _
                        '                                                                                                , eZeeDate.convertDate(mdtApprTran.Rows(p)("tdate").ToString()).ToString("yyyy-MM-dd"), mdtApprTran.Rows(p)("claimrequestno").ToString(), mdtApprTran.Rows(p)("expense").ToString(), mdtApprTran.Rows(p)("claim_remark").ToString() _
                        '                                                                                                , "USMF OPER"))

                            'Pinkal (23-Dec-2023) -- End

                    Next


                    If objMasterData.GetSetP2PWebRequest(mstrNewRequisitionRequestP2PServiceURL, True, True, "PaymentJournal", xResponseData, mstrError, objP2PaymentJournalService, xPostedData, mstrToken.Trim, xHttpStatusCode) = False Then
                        exForce = New Exception("Claim Approval P2P Error : " & mstrError)
                        Throw exForce
                    End If


                    Dim mstrRequestJournalNo As String = ""

                    Dim dtTable As DataTable = JsonStringToDataTable(xResponseData)
                    If dtTable IsNot Nothing AndAlso dtTable.Rows.Count > 0 Then
                        If dtTable.Columns.Contains("requestJournalNum") Then
                            If dtTable.Rows(0)("requestJournalNum").ToString().Trim.Length > 0 Then
                                mstrRequestJournalNo = dtTable.Rows(0)("requestJournalNum").ToString().Trim
                            End If
                        End If
                    End If


                    Dim objClaimMst As New clsclaim_request_master
                    If objClaimMst.UpdateP2PResponseToClaimMst(iClaimMasterId, xPostedData, xResponseData, mstrRequestJournalNo, xHttpStatusCode, iUserId) = False Then
                        exForce = New Exception("Update P2P Response Error : " & mstrError)
                        Throw exForce
                    End If
                    objClaimMst = Nothing

                End If  '    If mstrToken.Trim.Length > 0 Then

                End If  'If mstrNewRequisitionRequestP2PServiceURL.Trim.Length > 0 Then   '

                objMasterData = Nothing
                objP2PaymentJournalService = Nothing

            End If
            'Pinkal (16-Oct-2023) -- End


            Return True
        Catch ex As Exception
            'Pinkal (23-Dec-2023) -- Start
            '(A1X-1555)  NMB - P2P Changes for Claim Request and Company Asset.
            If mblnFlag = False Then iDataOpr.ReleaseTransaction(False)
            'DisplayError.Show("-1", ex.Message, "Insert_Update_ApproverData", mstrModuleName)
            Throw New Exception(ex.Message & "; Procedure Name: Insert_Update_ApproverData; Module Name: " & mstrModuleName)
            'Pinkal (23-Dec-2023) -- End
        Finally
        End Try
    End Function

    ''' <summary>
    ''' Modify By: Pinkal Jariwala
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>
    Private Function isExist(ByVal objDataOpr As clsDataOperation, _
                             ByVal iApproverId As Integer, _
                             ByVal iClaimMasterId As Integer, _
                             ByVal iClaimTranId As Integer, _
                             ByVal iExpenseId As Integer) As Integer
        Dim StrQ As String = String.Empty
        Dim exForce As Exception
        Dim dsList As New DataSet
        Try
            StrQ = "SELECT crapprovaltranunkid FROM cmclaim_approval_tran WHERE isvoid = 0 AND iscancel = 0 AND crapproverunkid = '" & iApproverId & "' AND crmasterunkid = '" & iClaimMasterId & "' " & _
                   "AND crtranunkid = '" & iClaimTranId & "' AND expenseunkid = '" & iExpenseId & "' "

            dsList = objDataOpr.ExecQuery(StrQ, "List")

            If objDataOpr.ErrorMessage <> "" Then
                exForce = New Exception(objDataOpr.ErrorNumber & ": " & objDataOpr.ErrorMessage)
                Throw exForce
            End If

            If dsList.Tables(0).Rows.Count > 0 Then
                Return dsList.Tables(0).Rows(0).Item(0)
            Else
                Return 0
            End If

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "Insert_Update_Approver", mstrModuleName)
            Return False
        Finally
        End Try
    End Function

    ''' <summary>
    ''' Modify By: Pinkal Jariwala
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>
    Private Function UpdateClaimFormStatusByApprover(ByVal objDataOperation As clsDataOperation, ByVal intStatusId As Integer _
                                                                               , ByVal intRequestMasterId As Integer, ByVal intUserID As Integer _
                                                                               , Optional ByVal mstrRejectRemark As String = "") As Boolean
        Dim mblnFlag As Boolean = False
        Dim strQ As String = ""
        Dim exForce As Exception
        Try

            strQ = " Update cmclaim_request_master set statusunkid = @statusunkid,isbalancededuct = @isbalancededuct "

            If mstrRejectRemark.Trim.Length > 0 Then
                strQ &= ",remark = @remark "
            End If

            strQ &= " WHERE crmasterunkid = @masterunkid AND isvoid = 0 "

            objDataOperation.ClearParameters()
            objDataOperation.AddParameter("@statusunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intStatusId)
            objDataOperation.AddParameter("@isbalancededuct", SqlDbType.Bit, eZeeDataType.BIT_SIZE, True)
            objDataOperation.AddParameter("@remark", SqlDbType.NVarChar, eZeeDataType.DESC_SIZE, mstrRejectRemark)
            objDataOperation.AddParameter("@masterunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intRequestMasterId)
            objDataOperation.ExecNonQuery(strQ)

            'S.SANDEEP [28-May-2018] -- START
            'ISSUE/ENHANCEMENT : {Audit Trails} 
            Dim objCommonATLog As New clsCommonATLog
            objCommonATLog._FormName = mstrFormName
            objCommonATLog._LoginEmployeeUnkid = mintLoginEmployeeunkid
            objCommonATLog._ClientIP = mstrClientIP
            objCommonATLog._HostName = mstrHostName
            objCommonATLog._FromWeb = mblnIsWeb
            objCommonATLog._AuditUserId = mintAuditUserId
            objCommonATLog._CompanyUnkid = mintCompanyUnkid
            objCommonATLog._AuditDate = mdtAuditDate
            'S.SANDEEP [28-May-2018] -- END

            If objCommonATLog.Insert_TranAtLog(objDataOperation, "cmclaim_request_master", "crmasterunkid", intRequestMasterId, "cmclaim_request_tran", "crtranunkid", -1, 2, -1, False, intUserID) = False Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            'S.SANDEEP [28-May-2018] -- START
            'ISSUE/ENHANCEMENT : {Audit Trails} 
            objCommonATLog = Nothing
            'S.SANDEEP [28-May-2018] -- END

            mblnFlag = True
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: UpdateClaimFormStatusByApprover; Module Name: " & mstrModuleName)
        End Try
        Return mblnFlag
    End Function

    ''' <summary>
    ''' Modify By: Pinkal Jariwala
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>
    Public Function UpdateExpenseBalance(ByVal objDataOperation As clsDataOperation, ByVal dtApprTran As DataTable _
                                                            , ByVal iStatusId As Integer, ByVal iUserId As Integer, ByVal iClaimMasterId As Integer _
                                                            , Optional ByVal mstrRejcectRemark As String = "") As Boolean
        Dim StrQ As String = String.Empty
        Dim exForce As Exception
        Dim objCommonATLog As New clsCommonATLog
        Try
            'S.SANDEEP |10-MAR-2022| -- START
            'ISSUE/ENHANCEMENT : OLD-580
            '            Dim objExpense As New clsExpense_Master

            '            'Pinkal (18-Feb-2016) -- Start
            '            'Enhancement - CR Changes for ASP as per Rutta's Request.
            '            'Dim drRow() As DataRow = dtApprTran.Select("AUD <> 'D' ")
            '            Dim drRow() As DataRow = dtApprTran.Select("AUD <> 'D'  AND AUD <> ''")
            '            'Pinkal (18-Feb-2016) -- End



            '            If drRow.Length > 0 AndAlso iStatusId = 1 Then  'iStatusId = 1 Approved Status

            '                'Pinkal (26-Feb-2019) -- Start
            '                'Enhancement - Working on Leave & Claim Enhancement Phase 2 for NMB.

            '                Dim objEmpExpBalance As New clsEmployeeExpenseBalance

            '                For Each dr As DataRow In drRow

            '                    'Pinkal (26-Feb-2019) -- Start
            '                    'Enhancement - Working on Leave & Claim Enhancement Phase 2 for NMB.
            '                    'objExpense._Expenseunkid = CInt(dr("expenseunkid"))
            '                    objExpense._Expenseunkid(objDataOperation) = CInt(dr("expenseunkid"))
            '                    'Pinkal (26-Feb-2019) -- End

            '                    'Pinkal (22-Mar-2016) -- Start
            '                    'Enhancement - WORKING ON ENHANCEMENT ON CLAIM SUMMARY REPORT FOR KBC.
            '                    'if objExpense._Isaccrue = False AndAlso objExpense._IsLeaveEncashment = False Then Continue For
            '                    If objExpense._Isaccrue = False AndAlso objExpense._IsLeaveEncashment = False Then GoTo ExpenseOccurrence
            '                    'Pinkal (22-Mar-2016) -- End

            '                    If objExpense._Isaccrue AndAlso objExpense._IsLeaveEncashment = False Then

            '                        StrQ = " Update cmexpbalance_tran set remaining_bal = remaining_bal - @quantity ,issue_amount = issue_amount + @quantity  " & _
            '                                  " WHERE isvoid = 0 AND employeeunkid=@employeeunkid AND expenseunkid = @expenseunkid AND yearunkid = @yearunkid "
            '                        objDataOperation.ClearParameters()
            '                        objDataOperation.AddParameter("@expenseunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, CInt(dr("expenseunkid")))
            '                        objDataOperation.AddParameter("@employeeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, CInt(dr("employeeunkid")))
            '                        If objExpense._Uomunkid = enExpUoM.UOM_QTY Then
            '                            objDataOperation.AddParameter("@quantity", SqlDbType.Float, eZeeDataType.FLOAT_SIZE, CDec(dr("quantity")))
            '                        ElseIf objExpense._Uomunkid = enExpUoM.UOM_AMOUNT Then
            '                            objDataOperation.AddParameter("@quantity", SqlDbType.Float, eZeeDataType.FLOAT_SIZE, CDec(dr("amount")))
            '                        End If

            '                        objDataOperation.AddParameter("@yearunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintYearId)
            '                        Call objDataOperation.ExecNonQuery(StrQ)

            '                        If objDataOperation.ErrorMessage <> "" Then
            '                            exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
            '                            Throw exForce
            '                        End If



            '                        StrQ = " SELECT  ISNULL(crexpbalanceunkid,0) AS crexpbalanceunkid FROM cmexpbalance_tran WHERE isvoid = 0 AND employeeunkid=@employeeunkid AND expenseunkid = @expenseunkid AND yearunkid = @yearunkid "
            '                        objDataOperation.ClearParameters()
            '                        objDataOperation.AddParameter("@expenseunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, CInt(dr("expenseunkid")))
            '                        objDataOperation.AddParameter("@employeeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, CInt(dr("employeeunkid")))
            '                        objDataOperation.AddParameter("@yearunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintYearId)
            '                        Dim dsBalanceList As DataSet = objDataOperation.ExecQuery(StrQ, "List")

            '                        If objDataOperation.ErrorMessage <> "" Then
            '                            exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
            '                            Throw exForce
            '                        End If

            '                        If dsBalanceList IsNot Nothing AndAlso dsBalanceList.Tables(0).Rows.Count > 0 Then
            '                            If clsCommonATLog.Insert_AtLog(objDataOperation, 2, "cmexpbalance_tran", "crexpbalanceunkid", CInt(dsBalanceList.Tables(0).Rows(0)("crexpbalanceunkid")), False, iUserId) = False Then
            '                                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
            '                                Throw exForce
            '                            End If
            '                        End If

            '                    ElseIf objExpense._Isaccrue = False AndAlso objExpense._IsLeaveEncashment Then

            '                        If mintLeaveBalanceSetting <= 0 Then
            '                            mintLeaveBalanceSetting = ConfigParameter._Object._LeaveBalanceSetting
            '                        End If

            '                        Dim objLeaveBal As New clsleavebalance_tran

            '                        StrQ = " Update lvleavebalance_tran set " & _
            '                                  "  issue_amount = issue_amount +  @issue_amount " & _
            '                                  ", uptolstyr_issueamt  =  uptolstyr_issueamt + @issue_amount " & _
            '                                  ", remaining_bal = remaining_bal - @issue_amount" & _
            '                                  " WHERE employeeunkid = @employeeunkid AND leavetypeunkid = @leavetypeunkid AND yearunkid = @yearunkid AND isvoid = 0 "

            '                        If mintLeaveBalanceSetting = enLeaveBalanceSetting.ELC Then
            '                            StrQ &= " AND isopenelc = 1 AND iselc = 1"
            '                        End If

            '                        objDataOperation.ClearParameters()
            '                        objDataOperation.AddParameter("@employeeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, CInt(dr("employeeunkid")))
            '                        objDataOperation.AddParameter("@yearunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintYearId)
            '                        objDataOperation.AddParameter("@leavetypeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, objExpense._Leavetypeunkid)
            '                        objDataOperation.AddParameter("@issue_amount", SqlDbType.Float, eZeeDataType.FLOAT_SIZE, CDec(dr("quantity")))
            '                        objDataOperation.ExecNonQuery(StrQ)

            '                        If objDataOperation.ErrorMessage <> "" Then
            '                            exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
            '                            Throw exForce
            '                        End If

            '                        StrQ = "SELECT ISNULL(leavebalanceunkid,0) As leavebalanceunkid From lvleavebalance_tran WHERE employeeunkid = @employeeunkid AND leavetypeunkid = @leavetypeunkid AND yearunkid = @yearunkid AND isvoid = 0 "

            '                        If mintLeaveBalanceSetting = enLeaveBalanceSetting.ELC Then
            '                            StrQ &= " AND isopenelc = 1 AND iselc = 1"
            '                        End If

            '                        objDataOperation.ClearParameters()
            '                        objDataOperation.AddParameter("@employeeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, CInt(dr("employeeunkid")))
            '                        objDataOperation.AddParameter("@yearunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintYearId)
            '                        objDataOperation.AddParameter("@leavetypeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, objExpense._Leavetypeunkid)
            '                        Dim dsList As DataSet = objDataOperation.ExecQuery(StrQ, "List")

            '                        If objDataOperation.ErrorMessage <> "" Then
            '                            exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
            '                            Throw exForce
            '                        End If

            '                        If dsList IsNot Nothing AndAlso dsList.Tables(0).Rows.Count > 0 Then
            '                            objLeaveBal._LeaveBalanceunkid = CInt(dsList.Tables(0).Rows(0)("leavebalanceunkid"))
            '                        End If

            '                        If objLeaveBal.InsertAudiTrailForLeaveBalance(objDataOperation, 2) = False Then
            '                            exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
            '                            Throw exForce
            '                        End If

            '                    End If

            '                    'Pinkal (23-Mar-2016) -- Start
            '                    'Enhancement - Adding Occurrence/Remaining Occurrence in Claim Expense Balance Requested By Rutta.

            'ExpenseOccurrence:

            '                    StrQ = " Update cmexpbalance_tran set remaining_occurrence = remaining_occurrence - 1  " & _
            '                             " WHERE isvoid = 0 AND occurrence > 0 AND employeeunkid=@employeeunkid AND expenseunkid = @expenseunkid AND yearunkid = @yearunkid "

            '                    objDataOperation.ClearParameters()
            '                    objDataOperation.AddParameter("@expenseunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, CInt(dr("expenseunkid")))
            '                    objDataOperation.AddParameter("@employeeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, CInt(dr("employeeunkid")))
            '                    objDataOperation.AddParameter("@yearunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintYearId)
            '                    Call objDataOperation.ExecNonQuery(StrQ)

            '                    If objDataOperation.ErrorMessage <> "" Then
            '                        exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
            '                        Throw exForce
            '                    End If

            '                    'Pinkal (23-Mar-2016) -- End

            '                    Dim xCrExpBalanceID As Integer = 0

            '                    'Pinkal (07-Mar-2019) -- Start
            '                    'Enhancement - Working on Leave & Claim Enhancement Phase 2 for NMB.
            '                    Dim xOccurrence As Integer = 0
            '                    Dim xRemaining_Occurrence As Integer = 0
            '                    Dim xEligibilityAfter As Integer = 0

            '                    StrQ = " SELECT  ISNULL(crexpbalanceunkid,0) AS crexpbalanceunkid  " & _
            '                              ", ISNULL(occurrence,0) AS occurrence " & _
            '                              ", ISNULL(remaining_occurrence,0) AS remaining_occurrence " & _
            '                              ", ISNULL(eligibilityafter,0) AS eligibilityafter " & _
            '                                 " FROM cmexpbalance_tran  " & _
            '                                 " WHERE isvoid = 0 AND employeeunkid=@employeeunkid AND expenseunkid = @expenseunkid AND yearunkid = @yearunkid "

            '                    'Pinkal (07-Mar-2019) -- End [  ", ISNULL(occurrence,0) AS occurrence , ISNULL(remaining_occurrence,0) AS remaining_occurrence " & _]

            '                    If mintLeaveBalanceSetting = enLeaveBalanceSetting.Financial_Year Then
            '                        StrQ &= " AND isclose_fy = 0 "
            '                    ElseIf mintLeaveBalanceSetting = enLeaveBalanceSetting.ELC Then
            '                        StrQ &= " AND isopenelc = 1 AND iselc = 1 "
            '                    End If

            '                    objDataOperation.ClearParameters()
            '                    objDataOperation.AddParameter("@expenseunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, CInt(dr("expenseunkid")))
            '                    objDataOperation.AddParameter("@employeeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, CInt(dr("employeeunkid")))
            '                    objDataOperation.AddParameter("@yearunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintYearId)
            '                    Dim dsExpBalList As DataSet = objDataOperation.ExecQuery(StrQ, "List")

            '                    If objDataOperation.ErrorMessage <> "" Then
            '                        exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
            '                        Throw exForce
            '                    End If

            '                    If dsExpBalList IsNot Nothing AndAlso dsExpBalList.Tables(0).Rows.Count > 0 Then
            '                        xCrExpBalanceID = CInt(dsExpBalList.Tables(0).Rows(0)("crexpbalanceunkid"))
            '                        'Pinkal (07-Mar-2019) -- Start
            '                        'Enhancement - Working on Leave & Claim Enhancement Phase 2 for NMB.
            '                        xOccurrence = CInt(dsExpBalList.Tables(0).Rows(0)("occurrence"))
            '                        xRemaining_Occurrence = CInt(dsExpBalList.Tables(0).Rows(0)("remaining_occurrence"))
            '                        xEligibilityAfter = CInt(dsExpBalList.Tables(0).Rows(0)("eligibilityafter"))
            '                        'Pinkal (07-Mar-2019) -- End
            '                    End If

            '                    If clsCommonATLog.Insert_AtLog(objDataOperation, 2, "cmexpbalance_tran", "crexpbalanceunkid", xCrExpBalanceID, False, iUserId) = False Then
            '                        exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
            '                        Throw exForce
            '                    End If
            '                    'Pinkal (07-Mar-2019) -- Start
            '                    'Enhancement - Working on Leave & Claim Enhancement Phase 2 for NMB.
            '                    If xEligibilityAfter > 0 AndAlso xOccurrence > 0 AndAlso xRemaining_Occurrence <= 0 Then
            '                        If objEmpExpBalance.UpdateEligibleYearOFEmployee(objDataOperation, CInt(dr("employeeunkid")), CInt(dr("expenseunkid")), mintYearId, mintLeaveBalanceSetting, eZeeDate.convertDate(dr("tdate").ToString()).Date, xCrExpBalanceID, iUserId, False) = False Then
            '                            exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
            '                            Throw exForce
            '                        End If
            '                    End If
            '                    'Pinkal (07-Mar-2019) -- End
            '                Next
            '                objEmpExpBalance = Nothing
            '                'Pinkal (26-Feb-2019) -- End

            '            End If


            '            If UpdateClaimFormStatusByApprover(objDataOperation, iStatusId, iClaimMasterId, iUserId, mstrRejcectRemark) = False Then
            '                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
            '                Throw exForce
            '            End If
            If mdecRebatePercentage = 100 Then
                Dim objExpense As New clsExpense_Master
                Dim drRow() As DataRow = dtApprTran.Select("AUD <> 'D'  AND AUD <> ''")
                If drRow.Length > 0 AndAlso iStatusId = 1 Then  'iStatusId = 1 Approved Status
                    Dim objEmpExpBalance As New clsEmployeeExpenseBalance
                    For Each dr As DataRow In drRow

                        'expensetypeid
                        'S.SANDEEP |29-JUN-2022| -- START
                        'ISSUE/ENHANCEMENT : AC2-390_V2
                        'objExpense._Expenseunkid(objDataOperation) = CInt(dr("expenseunkid"))
                        If CInt(dr("expensetypeid")) = enExpenseType.EXP_REBATE_PRIVILEGE_DPNDT Then
                            objExpense._Expenseunkid(objDataOperation) = CInt(dr("deductexpenseunkid"))
                        Else
                        objExpense._Expenseunkid(objDataOperation) = CInt(dr("expenseunkid"))
                        End If
                        'S.SANDEEP |29-JUN-2022| -- END


                        If objExpense._Isaccrue = False AndAlso objExpense._IsLeaveEncashment = False Then GoTo ExpenseOccurrence
                        If objExpense._Isaccrue AndAlso objExpense._IsLeaveEncashment = False Then

                            StrQ = " Update cmexpbalance_tran set remaining_bal = remaining_bal - @quantity ,issue_amount = issue_amount + @quantity  " & _
                                      " WHERE isvoid = 0 AND employeeunkid=@employeeunkid AND expenseunkid = @expenseunkid AND yearunkid = @yearunkid "
                            objDataOperation.ClearParameters()

                            'S.SANDEEP |29-JUN-2022| -- START
                            'ISSUE/ENHANCEMENT : AC2-390_V2
                            'objDataOperation.AddParameter("@expenseunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, CInt(dr("expenseunkid")))
                            If CInt(dr("expensetypeid")) = enExpenseType.EXP_REBATE_PRIVILEGE_DPNDT Then
                                objDataOperation.AddParameter("@expenseunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, CInt(dr("deductexpenseunkid")))
                            Else
                            objDataOperation.AddParameter("@expenseunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, CInt(dr("expenseunkid")))
                            End If
                            'S.SANDEEP |29-JUN-2022| -- END


                            objDataOperation.AddParameter("@employeeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, CInt(dr("employeeunkid")))
                            If objExpense._Uomunkid = enExpUoM.UOM_QTY Then
                                objDataOperation.AddParameter("@quantity", SqlDbType.Float, eZeeDataType.FLOAT_SIZE, CDec(dr("quantity")))
                            ElseIf objExpense._Uomunkid = enExpUoM.UOM_AMOUNT Then
                                objDataOperation.AddParameter("@quantity", SqlDbType.Float, eZeeDataType.FLOAT_SIZE, CDec(dr("amount")))
                            End If

                            objDataOperation.AddParameter("@yearunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintYearId)
                            Call objDataOperation.ExecNonQuery(StrQ)

                            If objDataOperation.ErrorMessage <> "" Then
                                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                                Throw exForce
                            End If



                            StrQ = " SELECT  ISNULL(crexpbalanceunkid,0) AS crexpbalanceunkid FROM cmexpbalance_tran WHERE isvoid = 0 AND employeeunkid=@employeeunkid AND expenseunkid = @expenseunkid AND yearunkid = @yearunkid "
                            objDataOperation.ClearParameters()

                            'S.SANDEEP |29-JUN-2022| -- START
                            'ISSUE/ENHANCEMENT : AC2-390_V2
                            'objDataOperation.AddParameter("@expenseunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, CInt(dr("expenseunkid")))
                            If CInt(dr("expensetypeid")) = enExpenseType.EXP_REBATE_PRIVILEGE_DPNDT Then
                                objDataOperation.AddParameter("@expenseunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, CInt(dr("deductexpenseunkid")))
                            Else
                            objDataOperation.AddParameter("@expenseunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, CInt(dr("expenseunkid")))
                            End If
                            'S.SANDEEP |29-JUN-2022| -- END

                            objDataOperation.AddParameter("@employeeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, CInt(dr("employeeunkid")))
                            objDataOperation.AddParameter("@yearunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintYearId)
                            Dim dsBalanceList As DataSet = objDataOperation.ExecQuery(StrQ, "List")

                            If objDataOperation.ErrorMessage <> "" Then
                                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                                Throw exForce
                            End If

                            If dsBalanceList IsNot Nothing AndAlso dsBalanceList.Tables(0).Rows.Count > 0 Then
                                'S.SANDEEP [28-May-2018] -- START
                                'ISSUE/ENHANCEMENT : {Audit Trails}                                 
                                objCommonATLog._FormName = mstrFormName
                                objCommonATLog._LoginEmployeeUnkid = mintLoginEmployeeunkid
                                objCommonATLog._ClientIP = mstrClientIP
                                objCommonATLog._HostName = mstrHostName
                                objCommonATLog._FromWeb = mblnIsWeb
                                objCommonATLog._AuditUserId = mintAuditUserId
                                objCommonATLog._CompanyUnkid = mintCompanyUnkid
                                objCommonATLog._AuditDate = mdtAuditDate
                                'S.SANDEEP [28-May-2018] -- END

                                If objCommonATLog.Insert_AtLog(objDataOperation, 2, "cmexpbalance_tran", "crexpbalanceunkid", CInt(dsBalanceList.Tables(0).Rows(0)("crexpbalanceunkid")), False, iUserId) = False Then
                                    exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                                    Throw exForce
                                End If

                                'S.SANDEEP [28-May-2018] -- START
                                'ISSUE/ENHANCEMENT : {Audit Trails} 
                                objCommonATLog = Nothing
                                'S.SANDEEP [28-May-2018] -- END

                            End If

                        ElseIf objExpense._Isaccrue = False AndAlso objExpense._IsLeaveEncashment Then

                            If mintLeaveBalanceSetting <= 0 Then
                                mintLeaveBalanceSetting = ConfigParameter._Object._LeaveBalanceSetting
                            End If

                            Dim objLeaveBal As New clsleavebalance_tran

                            StrQ = " Update lvleavebalance_tran set " & _
                                      "  issue_amount = issue_amount +  @issue_amount " & _
                                      ", uptolstyr_issueamt  =  uptolstyr_issueamt + @issue_amount " & _
                                      ", remaining_bal = remaining_bal - @issue_amount" & _
                                      " WHERE employeeunkid = @employeeunkid AND leavetypeunkid = @leavetypeunkid AND yearunkid = @yearunkid AND isvoid = 0 "

                            If mintLeaveBalanceSetting = enLeaveBalanceSetting.ELC Then
                                StrQ &= " AND isopenelc = 1 AND iselc = 1"
                            End If

                            objDataOperation.ClearParameters()
                            objDataOperation.AddParameter("@employeeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, CInt(dr("employeeunkid")))
                            objDataOperation.AddParameter("@yearunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintYearId)
                            objDataOperation.AddParameter("@leavetypeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, objExpense._Leavetypeunkid)
                            objDataOperation.AddParameter("@issue_amount", SqlDbType.Float, eZeeDataType.FLOAT_SIZE, CDec(dr("quantity")))
                            objDataOperation.ExecNonQuery(StrQ)

                            If objDataOperation.ErrorMessage <> "" Then
                                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                                Throw exForce
                            End If

                            StrQ = "SELECT ISNULL(leavebalanceunkid,0) As leavebalanceunkid From lvleavebalance_tran WHERE employeeunkid = @employeeunkid AND leavetypeunkid = @leavetypeunkid AND yearunkid = @yearunkid AND isvoid = 0 "

                            If mintLeaveBalanceSetting = enLeaveBalanceSetting.ELC Then
                                StrQ &= " AND isopenelc = 1 AND iselc = 1"
                            End If

                            objDataOperation.ClearParameters()
                            objDataOperation.AddParameter("@employeeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, CInt(dr("employeeunkid")))
                            objDataOperation.AddParameter("@yearunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintYearId)
                            objDataOperation.AddParameter("@leavetypeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, objExpense._Leavetypeunkid)
                            Dim dsList As DataSet = objDataOperation.ExecQuery(StrQ, "List")

                            If objDataOperation.ErrorMessage <> "" Then
                                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                                Throw exForce
                            End If

                            If dsList IsNot Nothing AndAlso dsList.Tables(0).Rows.Count > 0 Then
                                objLeaveBal._LeaveBalanceunkid = CInt(dsList.Tables(0).Rows(0)("leavebalanceunkid"))
                            End If
                            With objLeaveBal
                                ._FormName = mstrFormName
                                ._LoginEmployeeUnkid = mintLoginEmployeeunkid
                                ._ClientIP = mstrClientIP
                                ._HostName = mstrHostName
                                ._FromWeb = mblnIsWeb
                                ._AuditUserId = mintAuditUserId
                                ._CompanyUnkid = mintCompanyUnkid
                                ._AuditDate = mdtAuditDate
                            End With
                            If objLeaveBal.InsertAudiTrailForLeaveBalance(objDataOperation, 2) = False Then
                                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                                Throw exForce
                            End If

                        End If

                        'Pinkal (23-Mar-2016) -- Start
                        'Enhancement - Adding Occurrence/Remaining Occurrence in Claim Expense Balance Requested By Rutta.

ExpenseOccurrence:

                        StrQ = " Update cmexpbalance_tran set remaining_occurrence = remaining_occurrence - 1  " & _
                                 " WHERE isvoid = 0 AND occurrence > 0 AND employeeunkid=@employeeunkid AND expenseunkid = @expenseunkid AND yearunkid = @yearunkid "

                        objDataOperation.ClearParameters()

                        'S.SANDEEP |29-JUN-2022| -- START
                        'ISSUE/ENHANCEMENT : AC2-390_V2
                        'objDataOperation.AddParameter("@expenseunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, CInt(dr("expenseunkid")))
                        If CInt(dr("expensetypeid")) = enExpenseType.EXP_REBATE_PRIVILEGE_DPNDT Then
                            objDataOperation.AddParameter("@expenseunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, CInt(dr("deductexpenseunkid")))
                        Else
                        objDataOperation.AddParameter("@expenseunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, CInt(dr("expenseunkid")))
                        End If
                        'S.SANDEEP |29-JUN-2022| -- END


                        objDataOperation.AddParameter("@employeeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, CInt(dr("employeeunkid")))
                        objDataOperation.AddParameter("@yearunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintYearId)
                        Call objDataOperation.ExecNonQuery(StrQ)

                        If objDataOperation.ErrorMessage <> "" Then
                            exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                            Throw exForce
                        End If

                        'Pinkal (23-Mar-2016) -- End

                        Dim xCrExpBalanceID As Integer = 0

                        'Pinkal (07-Mar-2019) -- Start
                        'Enhancement - Working on Leave & Claim Enhancement Phase 2 for NMB.
                        Dim xOccurrence As Integer = 0
                        Dim xRemaining_Occurrence As Integer = 0
                        Dim xEligibilityAfter As Integer = 0

                        StrQ = " SELECT  ISNULL(crexpbalanceunkid,0) AS crexpbalanceunkid  " & _
                                  ", ISNULL(occurrence,0) AS occurrence " & _
                                  ", ISNULL(remaining_occurrence,0) AS remaining_occurrence " & _
                                  ", ISNULL(eligibilityafter,0) AS eligibilityafter " & _
                                     " FROM cmexpbalance_tran  " & _
                                     " WHERE isvoid = 0 AND employeeunkid=@employeeunkid AND expenseunkid = @expenseunkid AND yearunkid = @yearunkid "

                        'Pinkal (07-Mar-2019) -- End [  ", ISNULL(occurrence,0) AS occurrence , ISNULL(remaining_occurrence,0) AS remaining_occurrence " & _]

                        If mintLeaveBalanceSetting = enLeaveBalanceSetting.Financial_Year Then
                            StrQ &= " AND isclose_fy = 0 "
                        ElseIf mintLeaveBalanceSetting = enLeaveBalanceSetting.ELC Then
                            StrQ &= " AND isopenelc = 1 AND iselc = 1 "
                        End If

                        objDataOperation.ClearParameters()


                        'S.SANDEEP |29-JUN-2022| -- START
                        'ISSUE/ENHANCEMENT : AC2-390_V2
                        'objDataOperation.AddParameter("@expenseunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, CInt(dr("expenseunkid")))
                        If CInt(dr("expensetypeid")) = enExpenseType.EXP_REBATE_PRIVILEGE_DPNDT Then
                            objDataOperation.AddParameter("@expenseunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, CInt(dr("deductexpenseunkid")))
                        Else
                        objDataOperation.AddParameter("@expenseunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, CInt(dr("expenseunkid")))
                        End If
                        'S.SANDEEP |29-JUN-2022| -- END

                        objDataOperation.AddParameter("@employeeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, CInt(dr("employeeunkid")))
                        objDataOperation.AddParameter("@yearunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintYearId)
                        Dim dsExpBalList As DataSet = objDataOperation.ExecQuery(StrQ, "List")

                        If objDataOperation.ErrorMessage <> "" Then
                            exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                            Throw exForce
                        End If

                        If dsExpBalList IsNot Nothing AndAlso dsExpBalList.Tables(0).Rows.Count > 0 Then
                            xCrExpBalanceID = CInt(dsExpBalList.Tables(0).Rows(0)("crexpbalanceunkid"))
                            'Pinkal (07-Mar-2019) -- Start
                            'Enhancement - Working on Leave & Claim Enhancement Phase 2 for NMB.
                            xOccurrence = CInt(dsExpBalList.Tables(0).Rows(0)("occurrence"))
                            xRemaining_Occurrence = CInt(dsExpBalList.Tables(0).Rows(0)("remaining_occurrence"))
                            xEligibilityAfter = CInt(dsExpBalList.Tables(0).Rows(0)("eligibilityafter"))
                            'Pinkal (07-Mar-2019) -- End
                        End If

                        'S.SANDEEP [28-May-2018] -- START
                        'ISSUE/ENHANCEMENT : {Audit Trails} 
                        objCommonATLog = New clsCommonATLog
                        objCommonATLog._FormName = mstrFormName
                        objCommonATLog._LoginEmployeeUnkid = mintLoginEmployeeunkid
                        objCommonATLog._ClientIP = mstrClientIP
                        objCommonATLog._HostName = mstrHostName
                        objCommonATLog._FromWeb = mblnIsWeb
                        objCommonATLog._AuditUserId = mintAuditUserId
                        objCommonATLog._CompanyUnkid = mintCompanyUnkid
                        objCommonATLog._AuditDate = mdtAuditDate
                        'S.SANDEEP [28-May-2018] -- END
                        If objCommonATLog.Insert_AtLog(objDataOperation, 2, "cmexpbalance_tran", "crexpbalanceunkid", xCrExpBalanceID, False, iUserId) = False Then
                            exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                            Throw exForce
                        End If
                        If xEligibilityAfter > 0 AndAlso xOccurrence > 0 AndAlso xRemaining_Occurrence <= 0 Then
                            If objEmpExpBalance.UpdateEligibleYearOFEmployee(objDataOperation, CInt(dr("employeeunkid")), CInt(dr("expenseunkid")), mintYearId, mintLeaveBalanceSetting, eZeeDate.convertDate(dr("tdate").ToString()).Date, xCrExpBalanceID, iUserId, False) = False Then
                                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                                Throw exForce
                            End If
                        End If
                    Next
                    objEmpExpBalance = Nothing
                End If
            End If

            If UpdateClaimFormStatusByApprover(objDataOperation, iStatusId, iClaimMasterId, iUserId, mstrRejcectRemark) = False Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If
            'S.SANDEEP |10-MAR-2022| -- END



        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: UpdateExpenseBalance; Module Name: " & mstrModuleName)
            Return False
        End Try
        Return True
    End Function

    ''' <summary>
    ''' Modify By: Pinkal Jariwala
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>
    Public Function GetMaxApproverForClaimForm(ByVal intEmployeeId As Integer, ByVal intExpenseTypeId As Integer _
                                                                    , ByVal intRequestMstId As Integer, ByVal blnPaymentApprovalwithLeaveApproval As Boolean _
                                                                    , Optional ByVal intClaimFormStatusId As Integer = 0 _
                                                                    , Optional ByVal xLeaveFormId As Integer = -1) As DataTable

        'Pinkal (04-Feb-2019) -- 'Enhancement - Working on Leave & Claim Enhancement Phase 2 for NMB.[   , Optional ByVal xLeaveFormId As Integer = -1]

        Dim dtApprover As DataTable = Nothing
        Dim StrQ As String = String.Empty
        Dim exForce As Exception
        Dim dsList As New DataSet
        Try
            Dim mstrApproverIDs As String = ""

            If blnPaymentApprovalwithLeaveApproval AndAlso intExpenseTypeId = enExpenseType.EXP_LEAVE Then
                Dim objLeaveAppr As New clspendingleave_Tran

                'Pinkal (04-Feb-2019) -- Start
                'Enhancement - Working on Leave & Claim Enhancement Phase 2 for NMB.
                'dsList = objLeaveAppr.GetEmployeeApproverListWithPriority(intEmployeeId, -1, Nothing)
                dsList = objLeaveAppr.GetEmployeeApproverListWithPriority(intEmployeeId, xLeaveFormId, Nothing)
                'Pinkal (04-Feb-2019) -- End

                If dsList IsNot Nothing AndAlso dsList.Tables(0).Rows.Count > 0 Then
                    Dim intMaxPrioirty As Integer = dsList.Tables(0).Compute("Max(priority)", "1=1")
                    Dim drRow() As DataRow = dsList.Tables(0).Select("priority = " & intMaxPrioirty, "")
                    If drRow.Length > 0 Then
                        For Each dr As DataRow In drRow
                            mstrApproverIDs &= dr("approverunkid").ToString() & ","
                        Next
                    End If
                End If

            Else
                Dim objExpAppr As New clsExpenseApprover_Master

                'Pinkal (20-Apr-2022) -- Start
                'Enhancement Prevision Air  - Doing Leave module Enhancement.	
                'dsList = objExpAppr.GetEmployeeApprovers(intExpenseTypeId, intEmployeeId, "List", Nothing)
                dsList = objExpAppr.GetEmployeeApprovers(intExpenseTypeId, intEmployeeId, "List", Nothing, intRequestMstId)
                'Pinkal (20-Apr-2022) -- End


                If dsList IsNot Nothing AndAlso dsList.Tables(0).Rows.Count > 0 Then
                    Dim intMaxPrioirty As Integer = dsList.Tables(0).Compute("Max(crpriority)", "1=1")
                    Dim drRow() As DataRow = dsList.Tables(0).Select("crpriority = " & intMaxPrioirty, "")
                    If drRow.Length > 0 Then
                        For Each dr As DataRow In drRow
                            mstrApproverIDs &= dr("crapproverunkid").ToString() & ","
                        Next
                    End If
                End If
            End If

            If mstrApproverIDs.Trim.Length > 0 Then
                mstrApproverIDs = mstrApproverIDs.Trim.Substring(0, mstrApproverIDs.Trim.Length - 1)
            Else
                mstrApproverIDs = "0"
            End If


            Dim objDataOperation As New clsDataOperation

            StrQ = " SELECT DISTINCT cmclaim_approval_tran.crmasterunkid " & _
                        ", cmclaim_approval_tran.approveremployeeunkid " & _
                        ", cmclaim_approval_tran.crapproverunkid " & _
                        ", cmclaim_request_master.statusunkid "

            If blnPaymentApprovalwithLeaveApproval AndAlso intExpenseTypeId = enExpenseType.EXP_LEAVE Then
                StrQ &= ", lvapproverlevel_master.levelunkid AS crlevelunkid " & _
                            ", lvapproverlevel_master.priority AS crpriority "
            Else
                StrQ &= ", cmapproverlevel_master.crlevelunkid " & _
                            ", cmapproverlevel_master.crpriority "
            End If

            StrQ &= " FROM cmclaim_approval_tran " & _
                        " JOIN cmclaim_request_master ON cmclaim_request_master.crmasterunkid = cmclaim_approval_tran.crmasterunkid AND cmclaim_request_master.isvoid = 0 "

            If intRequestMstId > 0 Then
                StrQ &= " AND cmclaim_request_master. crmasterunkid = @masterunkid "
            End If

            If blnPaymentApprovalwithLeaveApproval AndAlso intExpenseTypeId = enExpenseType.EXP_LEAVE Then
                StrQ &= " JOIN lvleaveapprover_master ON cmclaim_approval_tran.crapproverunkid = lvleaveapprover_master.approverunkid AND lvleaveapprover_master.isvoid = 0 " & _
                            " JOIN lvapproverlevel_master ON lvapproverlevel_master.levelunkid = lvleaveapprover_master.levelunkid AND lvapproverlevel_master.isactive = 1 "
            Else
                StrQ &= " JOIN cmexpapprover_master ON cmclaim_approval_tran.crapproverunkid = cmexpapprover_master.crapproverunkid AND cmexpapprover_master.isvoid = 0 " & _
                            " JOIN cmapproverlevel_master ON cmexpapprover_master.crlevelunkid = cmapproverlevel_master.crlevelunkid AND cmapproverlevel_master.isactive = 1 "
            End If


            StrQ &= " WHERE cmclaim_approval_tran.crapproverunkid IN (" & mstrApproverIDs & ") AND cmclaim_approval_tran.isvoid = 0 "

            If intClaimFormStatusId > 0 Then
                StrQ &= "  AND cmclaim_approval_tran.statusunkid = 1 AND cmclaim_request_master.statusunkid = 1 "
            End If

            objDataOperation.ClearParameters()
            objDataOperation.AddParameter("@masterunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intRequestMstId)
            dsList = objDataOperation.ExecQuery(StrQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            dtApprover = dsList.Tables(0)

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: GetMaxApproverForClaimForm; Module Name: " & mstrModuleName)
        End Try
        Return dtApprover
    End Function

    ''' <summary>
    ''' Modify By: Pinkal Jariwala
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>
    ''' Shani(24-Aug-2015) -- Start
    ''' ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
    ''' Public Function CancelExpense(ByVal mdtTran As DataTable, ByVal intCancelModuleID As Integer, ByVal mstrCancelRemak As String, ByVal intUserId As Integer, Optional ByVal _ObjOperation As clsDataOperation = Nothing) As Boolean
    Public Function CancelExpense(ByVal mdtTran As DataTable, ByVal intCancelModuleID As Integer, ByVal mstrCancelRemak As String, ByVal intUserId As Integer, ByVal dtCurrentDateAndTime As DateTime, Optional ByVal _ObjOperation As clsDataOperation = Nothing) As Boolean
        'Shani(24-Aug-2015) -- End

        Dim StrQ As String = String.Empty
        Dim exForce As Exception
        Dim objDataOperation As clsDataOperation = Nothing
        Dim intCount As Integer = 0
        Dim mintMasterId As Integer = -1
        Try
            Dim objExpense As New clsExpense_Master

            If _ObjOperation Is Nothing Then
                objDataOperation = New clsDataOperation
                objDataOperation.BindTransaction()
            Else
                objDataOperation = _ObjOperation
            End If


            'Pinkal (26-Feb-2019) -- Start
            'Enhancement - Working on Leave & Claim Enhancement Phase 2 for NMB.
            Dim objEmpExpBalance As New clsEmployeeExpenseBalance
            'Pinkal (26-Feb-2019) -- End

            For Each dr As DataRow In mdtTran.Rows

                mintMasterId = CInt(dr("crmasterunkid"))

                If CBool(dr("IsCheck")) = False Then Continue For

                StrQ = " UPDATE cmclaim_approval_tran SET cancelfrommoduleid = @cancelfrommoduleid,iscancel  = @iscancel ,canceluserunkid = @canceluserunkid,cancel_remark=@cancel_remark,cancel_datetime = @cancel_datetime " & _
                    " WHERE  crmasterunkid = @masterunkid AND expenseunkid = @expenseunkid AND crtranunkid = @requesttranunkid AND crapprovaltranunkid = @crapprovaltranunkid AND isvoid = 0  "

                objDataOperation.ClearParameters()
                objDataOperation.AddParameter("@masterunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, CInt(dr("crmasterunkid")))
                objDataOperation.AddParameter("@expenseunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, CInt(dr("expenseunkid")))
                objDataOperation.AddParameter("@requesttranunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, CInt(dr("crtranunkid")))
                objDataOperation.AddParameter("@crapprovaltranunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, CInt(dr("crapprovaltranunkid")))
                objDataOperation.AddParameter("@cancelfrommoduleid", SqlDbType.Int, eZeeDataType.INT_SIZE, intCancelModuleID)
                objDataOperation.AddParameter("@iscancel", SqlDbType.Bit, eZeeDataType.BIT_SIZE, True)
                objDataOperation.AddParameter("@cancel_remark", SqlDbType.NVarChar, eZeeDataType.DESC_SIZE, mstrCancelRemak)
                objDataOperation.AddParameter("@canceluserunkid", SqlDbType.Bit, eZeeDataType.BIT_SIZE, intUserId)

                'Shani(24-Aug-2015) -- Start
                'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
                'objDataOperation.AddParameter("@cancel_datetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, ConfigParameter._Object._CurrentDateAndTime)
                objDataOperation.AddParameter("@cancel_datetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, dtCurrentDateAndTime)
                'Shani(24-Aug-2015) -- End

                objDataOperation.ExecNonQuery(StrQ)

                If objDataOperation.ErrorMessage <> "" Then
                    exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                    Throw exForce
                End If


                'StrQ = " SELECT ISNULL(crapprovaltranunkid,0) as crapprovaltranunkid FROM cmclaim_approval_tran WHERE isvoid = 0 AND crmasterunkid = @masterunkid AND expenseunkid = @expenseunkid AND crtranunkid = @requesttranunkid "
                'objDataOperation.ClearParameters()
                'objDataOperation.AddParameter("@masterunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, CInt(dr("crmasterunkid")))
                'objDataOperation.AddParameter("@expenseunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, CInt(dr("expenseunkid")))
                'objDataOperation.AddParameter("@requesttranunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, CInt(dr("crtranunkid")))
                'Dim dsList As DataSet = objDataOperation.ExecQuery(StrQ, "List")

                'If objDataOperation.ErrorMessage <> "" Then
                '    exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                '    Throw exForce
                'End If

                'S.SANDEEP [28-May-2018] -- START
                'ISSUE/ENHANCEMENT : {Audit Trails} 
                Dim objCommonATLog As New clsCommonATLog
                objCommonATLog._FormName = mstrFormName
                objCommonATLog._LoginEmployeeUnkid = mintLoginEmployeeunkid
                objCommonATLog._ClientIP = mstrClientIP
                objCommonATLog._HostName = mstrHostName
                objCommonATLog._FromWeb = mblnIsWeb
                objCommonATLog._AuditUserId = mintAuditUserId
                objCommonATLog._CompanyUnkid = mintCompanyUnkid
                objCommonATLog._AuditDate = mdtAuditDate
                'S.SANDEEP [28-May-2018] -- END

                If objCommonATLog.Insert_TranAtLog(objDataOperation, "cmclaim_request_master", "crmasterunkid", CInt(dr("crmasterunkid")), "cmclaim_approval_tran", "crapprovaltranunkid", CInt(dr("crapprovaltranunkid")), 2, 2, False, intUserId) = False Then
                    exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                    Throw exForce
                End If

                'S.SANDEEP [28-May-2018] -- START
                'ISSUE/ENHANCEMENT : {Audit Trails} 
                objCommonATLog = Nothing
                'S.SANDEEP [28-May-2018] -- END

                intCount += 1

                If CBool(dr("isconsiderforpayroll")) Then


                    StrQ = " SELECT ISNULL(crprocesstranunkid,0) as crprocesstranunkid FROM cmclaim_process_tran  " & _
                              " WHERE isvoid = 0 AND crmasterunkid = @masterunkid AND expenseunkid = @expenseunkid AND crtranunkid = @requesttranunkid AND crapprovaltranunkid = @crapprovaltranunkid "
                    objDataOperation.ClearParameters()
                    objDataOperation.AddParameter("@masterunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, CInt(dr("crmasterunkid")))
                    objDataOperation.AddParameter("@expenseunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, CInt(dr("expenseunkid")))
                    objDataOperation.AddParameter("@requesttranunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, CInt(dr("crtranunkid")))
                    objDataOperation.AddParameter("@crapprovaltranunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, CInt(dr("crapprovaltranunkid")))
                    Dim dsProcessList As DataSet = objDataOperation.ExecQuery(StrQ, "List")

                    If objDataOperation.ErrorMessage <> "" Then
                        exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                        Throw exForce
                    End If

                    StrQ = " UPDATE cmclaim_process_tran SET isvoid  = @isvoid ,voiduserunkid = @voiduserunkid,voidreason=@voidreason,voiddatetime = @voiddatetime " & _
                               " WHERE  crmasterunkid = @masterunkid AND expenseunkid = @expenseunkid AND crtranunkid = @requesttranunkid AND crapprovaltranunkid = @crapprovaltranunkid AND isvoid = 0"

                    objDataOperation.ClearParameters()
                    objDataOperation.AddParameter("@masterunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, CInt(dr("crmasterunkid")))
                    objDataOperation.AddParameter("@expenseunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, CInt(dr("expenseunkid")))
                    objDataOperation.AddParameter("@requesttranunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, CInt(dr("crtranunkid")))
                    objDataOperation.AddParameter("@crapprovaltranunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, CInt(dr("crapprovaltranunkid")))
                    objDataOperation.AddParameter("@isvoid", SqlDbType.Bit, eZeeDataType.BIT_SIZE, True)
                    objDataOperation.AddParameter("@voidreason", SqlDbType.NVarChar, eZeeDataType.DESC_SIZE, mstrCancelRemak)
                    objDataOperation.AddParameter("@voiduserunkid", SqlDbType.Bit, eZeeDataType.BIT_SIZE, intUserId)

                    'Shani(24-Aug-2015) -- Start
                    'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
                    'objDataOperation.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, ConfigParameter._Object._CurrentDateAndTime)
                    objDataOperation.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, dtCurrentDateAndTime)
                    'Shani(24-Aug-2015) -- End

                    objDataOperation.ExecNonQuery(StrQ)

                    If objDataOperation.ErrorMessage <> "" Then
                        exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                        Throw exForce
                    End If

                    If dsProcessList IsNot Nothing AndAlso dsProcessList.Tables(0).Rows.Count > 0 Then
                        Dim objClaimProcess As New clsclaim_process_Tran
                        objClaimProcess._Crprocesstranunkid = CInt(dsProcessList.Tables(0).Rows(0)("crprocesstranunkid"))

                        With objClaimProcess
                            ._FormName = mstrFormName
                            ._LoginEmployeeunkid = mintLoginEmployeeunkid
                            ._ClientIP = mstrClientIP
                            ._HostName = mstrHostName
                            ._FromWeb = mblnIsWeb
                            ._AuditUserId = mintAuditUserId
                            ._CompanyUnkid = mintCompanyUnkid
                            ._AuditDate = mdtAuditDate
                        End With

                        'Shani(24-Aug-2015) -- Start
                        'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
                        'If objClaimProcess.InsertAudiTrailForClaimProcessTran(objDataOperation, 3) = False Then
                        If objClaimProcess.InsertAudiTrailForClaimProcessTran(objDataOperation, 3, dtCurrentDateAndTime) = False Then
                            'Shani(24-Aug-2015) -- End

                            exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                            Throw exForce
                        End If
                        objClaimProcess = Nothing
                    End If

                End If



                'Pinkal (26-Feb-2019) -- Start
                'Enhancement - Working on Leave & Claim Enhancement Phase 2 for NMB.
                'objExpense._Expenseunkid = CInt(dr("expenseunkid"))
                objExpense._Expenseunkid(objDataOperation) = CInt(dr("expenseunkid"))
                'Pinkal (26-Feb-2019) -- End

                'Pinkal (07-Mar-2019) -- Start
                'Enhancement - Working on Leave & Claim Enhancement Phase 2 for NMB.
                Dim xOccurrence As Integer = 0
                Dim xRemaining_Occurrence As Integer = 0
                Dim xEligilibiltyafter As Integer = 0
                'Pinkal (07-Mar-2019) -- End

                If objExpense._Isaccrue AndAlso objExpense._IsLeaveEncashment = False Then

                    StrQ = " Update cmexpbalance_tran set remaining_bal = remaining_bal + @quantity ,issue_amount = issue_amount - @quantity WHERE isvoid = 0 AND employeeunkid=@employeeunkid AND expenseunkid = @expenseunkid "
                    objDataOperation.ClearParameters()
                    objDataOperation.AddParameter("@expenseunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, CInt(dr("expenseunkid")))
                    objDataOperation.AddParameter("@employeeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, CInt(dr("employeeunkid")))


                    'Pinkal (15-Sep-2017) -- Start
                    'Enhancement - Solving Cancel Expense Issue.
                    If objExpense._Uomunkid = enExpUoM.UOM_QTY Then
                        objDataOperation.AddParameter("@quantity", SqlDbType.Float, eZeeDataType.FLOAT_SIZE, CDec(dr("quantity")))
                    ElseIf objExpense._Uomunkid = enExpUoM.UOM_AMOUNT Then
                        objDataOperation.AddParameter("@quantity", SqlDbType.Float, eZeeDataType.FLOAT_SIZE, CDec(dr("amount")))
                    End If
                    'Pinkal (15-Sep-2017) -- End

                    Call objDataOperation.ExecNonQuery(StrQ)

                    If objDataOperation.ErrorMessage <> "" Then
                        exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                        Throw exForce
                    End If


                    StrQ = " SELECT  ISNULL(crexpbalanceunkid,0) AS crexpbalanceunkid  " & _
                               ", ISNULL(occurrence,0) AS occurrence " & _
                               ", ISNULL(remaining_occurrence,0) AS remaining_occurrence " & _
                               ", ISNULL(eligibilityafter,0) AS eligibilityafter " & _
                               " FROM cmexpbalance_tran WHERE isvoid = 0 AND employeeunkid=@employeeunkid AND expenseunkid = @expenseunkid  "

                    'Pinkal (07-Mar-2019) -- Start/End [  ", ISNULL(occurrence,0) AS occurrence , ISNULL(remaining_occurrence,0) AS remaining_occurrence " & _]


                    objDataOperation.ClearParameters()
                    objDataOperation.AddParameter("@expenseunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, CInt(dr("expenseunkid")))
                    objDataOperation.AddParameter("@employeeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, CInt(dr("employeeunkid")))
                    Dim dsBalanceList As DataSet = objDataOperation.ExecQuery(StrQ, "List")

                    If objDataOperation.ErrorMessage <> "" Then
                        exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                        Throw exForce
                    End If

                    If dsBalanceList IsNot Nothing AndAlso dsBalanceList.Tables(0).Rows.Count > 0 Then
                        'S.SANDEEP [28-May-2018] -- START
                        'ISSUE/ENHANCEMENT : {Audit Trails} 
                        objCommonATLog = New clsCommonATLog
                        objCommonATLog._FormName = mstrFormName
                        objCommonATLog._LoginEmployeeUnkid = mintLoginEmployeeunkid
                        objCommonATLog._ClientIP = mstrClientIP
                        objCommonATLog._HostName = mstrHostName
                        objCommonATLog._FromWeb = mblnIsWeb
                        objCommonATLog._AuditUserId = mintAuditUserId
                        objCommonATLog._CompanyUnkid = mintCompanyUnkid
                        objCommonATLog._AuditDate = mdtAuditDate
                        'S.SANDEEP [28-May-2018] -- END

                        If objCommonATLog.Insert_AtLog(objDataOperation, 2, "cmexpbalance_tran", "crexpbalanceunkid", CInt(dsBalanceList.Tables(0).Rows(0)("crexpbalanceunkid")), False, intUserId) = False Then
                            exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                            Throw exForce
                        End If
                        'Pinkal (07-Mar-2019) -- Start
                        'Enhancement - Working on Leave & Claim Enhancement Phase 2 for NMB.
                        xEligilibiltyafter = CInt(dsBalanceList.Tables(0).Rows(0)("eligibilityafter"))
                        xOccurrence = CInt(dsBalanceList.Tables(0).Rows(0)("occurrence"))
                        xRemaining_Occurrence = CInt(dsBalanceList.Tables(0).Rows(0)("remaining_occurrence"))
                        'Pinkal (07-Mar-2019) -- End

                        'S.SANDEEP [28-May-2018] -- START
                        'ISSUE/ENHANCEMENT : {Audit Trails} 
                        objCommonATLog = Nothing
                        'S.SANDEEP [28-May-2018] -- END

                    End If

                ElseIf objExpense._Isaccrue = False AndAlso objExpense._IsLeaveEncashment Then

                    If mintLeaveBalanceSetting <= 0 Then
                        mintLeaveBalanceSetting = ConfigParameter._Object._LeaveBalanceSetting
                    End If

                    Dim objLeaveBal As New clsleavebalance_tran

                    StrQ = " Update lvleavebalance_tran set " & _
                              "  issue_amount = issue_amount -  @issue_amount " & _
                              ", uptolstyr_issueamt  =  uptolstyr_issueamt - @issue_amount " & _
                              ", remaining_bal = remaining_bal + @issue_amount" & _
                              " WHERE employeeunkid = @employeeunkid AND leavetypeunkid = @leavetypeunkid AND yearunkid = @yearunkid AND isvoid = 0 "

                    If mintLeaveBalanceSetting = enLeaveBalanceSetting.ELC Then
                        StrQ &= " AND isopenelc = 1 AND iselc = 1"
                    End If

                    objDataOperation.ClearParameters()
                    objDataOperation.AddParameter("@employeeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, CInt(dr("employeeunkid")))
                    objDataOperation.AddParameter("@yearunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintYearId)
                    objDataOperation.AddParameter("@leavetypeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, objExpense._Leavetypeunkid)
                    objDataOperation.AddParameter("@issue_amount", SqlDbType.Float, eZeeDataType.FLOAT_SIZE, CDec(dr("quantity")))
                    objDataOperation.ExecNonQuery(StrQ)

                    If objDataOperation.ErrorMessage <> "" Then
                        exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                        Throw exForce
                    End If

                    StrQ = "SELECT ISNULL(leavebalanceunkid,0) As leavebalanceunkid From lvleavebalance_tran WHERE employeeunkid = @employeeunkid AND leavetypeunkid = @leavetypeunkid AND yearunkid = @yearunkid AND isvoid = 0 "

                    If mintLeaveBalanceSetting = enLeaveBalanceSetting.ELC Then
                        StrQ &= " AND isopenelc = 1 AND iselc = 1"
                    End If

                    objDataOperation.ClearParameters()
                    objDataOperation.AddParameter("@employeeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, CInt(dr("employeeunkid")))
                    objDataOperation.AddParameter("@yearunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintYearId)
                    objDataOperation.AddParameter("@leavetypeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, objExpense._Leavetypeunkid)
                    Dim dsBalanceList As DataSet = objDataOperation.ExecQuery(StrQ, "List")

                    If objDataOperation.ErrorMessage <> "" Then
                        exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                        Throw exForce
                    End If

                    If dsBalanceList IsNot Nothing AndAlso dsBalanceList.Tables(0).Rows.Count > 0 Then
                        objLeaveBal._LeaveBalanceunkid = CInt(dsBalanceList.Tables(0).Rows(0)("leavebalanceunkid"))
                    End If
                    With objLeaveBal
                        ._FormName = mstrFormName
                        ._LoginEmployeeUnkid = mintLoginEmployeeunkid
                        ._ClientIP = mstrClientIP
                        ._HostName = mstrHostName
                        ._FromWeb = mblnIsWeb
                        ._AuditUserId = mintAuditUserId
                        ._CompanyUnkid = mintCompanyUnkid
                        ._AuditDate = mdtAuditDate
                    End With
                    If objLeaveBal.InsertAudiTrailForLeaveBalance(objDataOperation, 2) = False Then
                        exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                        Throw exForce
                    End If

                End If


                'Pinkal (07-Mar-2019) -- Start
                'Enhancement - Working on Leave & Claim Enhancement Phase 2 for NMB.

                Dim xCrExpBalanceID As Integer = 0

                StrQ = " SELECT  ISNULL(crexpbalanceunkid,0) AS crexpbalanceunkid  " & _
                           ", ISNULL(occurrence,0) AS occurrence " & _
                           ", ISNULL(remaining_occurrence,0) AS remaining_occurrence " & _
                           ", ISNULL(eligibilityafter,0) AS eligibilityafter " & _
                             " FROM cmexpbalance_tran  " & _
                             " WHERE isvoid = 0 AND employeeunkid=@employeeunkid AND expenseunkid = @expenseunkid AND yearunkid = @yearunkid "

                If mintLeaveBalanceSetting = enLeaveBalanceSetting.Financial_Year Then
                    StrQ &= " AND isclose_fy = 0 "
                ElseIf mintLeaveBalanceSetting = enLeaveBalanceSetting.ELC Then
                    StrQ &= " AND isopenelc = 1 AND iselc = 1 "
                End If

                objDataOperation.ClearParameters()
                objDataOperation.AddParameter("@expenseunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, CInt(dr("expenseunkid")))
                objDataOperation.AddParameter("@employeeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, CInt(dr("employeeunkid")))
                objDataOperation.AddParameter("@yearunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintYearId)
                Dim dsExpBalList As DataSet = objDataOperation.ExecQuery(StrQ, "List")

                If objDataOperation.ErrorMessage <> "" Then
                    exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                    Throw exForce
                End If


                'Pinkal (27-Apr-2019) -- Start
                'Enhancement - Audit Trail changes.

                If dsExpBalList IsNot Nothing AndAlso dsExpBalList.Tables(0).Rows.Count > 0 Then
                    xCrExpBalanceID = CInt(dsExpBalList.Tables(0).Rows(0)("crexpbalanceunkid"))
                    xEligilibiltyafter = CInt(dsExpBalList.Tables(0).Rows(0)("eligibilityafter"))
                    xOccurrence = CInt(dsExpBalList.Tables(0).Rows(0)("occurrence"))
                    xRemaining_Occurrence = CInt(dsExpBalList.Tables(0).Rows(0)("remaining_occurrence"))

                    'Pinkal (07-Mar-2019) -- End


                    StrQ = " Update cmexpbalance_tran set remaining_occurrence = remaining_occurrence + 1  " & _
                             " WHERE isvoid = 0 AND occurrence > 0 AND employeeunkid=@employeeunkid AND expenseunkid = @expenseunkid AND yearunkid = @yearunkid "

                    objDataOperation.ClearParameters()
                    objDataOperation.AddParameter("@expenseunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, CInt(dr("expenseunkid")))
                    objDataOperation.AddParameter("@employeeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, CInt(dr("employeeunkid")))
                    objDataOperation.AddParameter("@yearunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintYearId)
                    Call objDataOperation.ExecNonQuery(StrQ)

                    If objDataOperation.ErrorMessage <> "" Then
                        exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                        Throw exForce
                    End If

                    objCommonATLog = New clsCommonATLog
                    objCommonATLog._FormName = mstrFormName
                    objCommonATLog._LoginEmployeeUnkid = mintLoginEmployeeunkid
                    objCommonATLog._ClientIP = mstrClientIP
                    objCommonATLog._HostName = mstrHostName
                    objCommonATLog._FromWeb = mblnIsWeb
                    objCommonATLog._AuditUserId = mintAuditUserId
                    objCommonATLog._CompanyUnkid = mintCompanyUnkid
                    objCommonATLog._AuditDate = mdtAuditDate

                    'Pinkal (07-Mar-2019) -- Start
                    'Enhancement - Working on Leave & Claim Enhancement Phase 2 for NMB.
                    If objCommonATLog.Insert_AtLog(objDataOperation, 2, "cmexpbalance_tran", "crexpbalanceunkid", xCrExpBalanceID, False, intUserId) = False Then
                        exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                        Throw exForce
                    End If

                End If
                'Pinkal (27-Apr-2019) -- End

                If xEligilibiltyafter > 0 AndAlso xOccurrence > 0 AndAlso xRemaining_Occurrence <= 0 Then
                    If objEmpExpBalance.UpdateEligibleYearOFEmployee(objDataOperation, CInt(dr("employeeunkid")), CInt(dr("expenseunkid")), mintYearId, mintLeaveBalanceSetting, eZeeDate.convertDate(dr("tdate").ToString()).Date, xCrExpBalanceID, intUserId, True) = False Then
                        exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                        Throw exForce
                    End If
                End If

                'Pinkal (07-Mar-2019) -- End

                'Pinkal (27-Apr-2019) -- Start
                'Enhancement - Audit Trail changes.
                objCommonATLog = Nothing
                'Pinkal (27-Apr-2019) -- End

            Next

            'Pinkal (26-Feb-2019) -- Start
            'Enhancement - Working on Leave & Claim Enhancement Phase 2 for NMB.
            objEmpExpBalance = Nothing
            'Pinkal (26-Feb-2019) -- End

            If mdtTran.Rows.Count = intCount Then

                StrQ = " Update cmclaim_request_master set statusunkid  = @statusunkid, cancelfrommoduleid = @cancelfrommoduleid,iscancel  = @iscancel ,canceluserunkid = @canceluserunkid,cancel_remark=@cancel_remark,cancel_datetime = @cancel_datetime " & _
                          " WHERE  crmasterunkid = @masterunkid AND cmclaim_request_master.isvoid = 0 "

                objDataOperation.ClearParameters()
                objDataOperation.AddParameter("@masterunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintMasterId)
                objDataOperation.AddParameter("@statusunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, 6) 'Cancel Status
                objDataOperation.AddParameter("@cancelfrommoduleid", SqlDbType.Int, eZeeDataType.INT_SIZE, intCancelModuleID)
                objDataOperation.AddParameter("@iscancel", SqlDbType.Bit, eZeeDataType.BIT_SIZE, True)
                objDataOperation.AddParameter("@cancel_remark", SqlDbType.NVarChar, eZeeDataType.DESC_SIZE, mstrCancelRemak)
                objDataOperation.AddParameter("@canceluserunkid", SqlDbType.Bit, eZeeDataType.BIT_SIZE, intUserId)

                'Shani(24-Aug-2015) -- Start
                'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
                'objDataOperation.AddParameter("@cancel_datetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, ConfigParameter._Object._CurrentDateAndTime)
                objDataOperation.AddParameter("@cancel_datetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, dtCurrentDateAndTime)
                'Shani(24-Aug-2015) -- End

                objDataOperation.ExecNonQuery(StrQ)

                If objDataOperation.ErrorMessage <> "" Then
                    exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                    Throw exForce
                End If

                'S.SANDEEP [28-May-2018] -- START
                'ISSUE/ENHANCEMENT : {Audit Trails} 
                Dim objCommonATLog As New clsCommonATLog
                objCommonATLog._FormName = mstrFormName
                objCommonATLog._LoginEmployeeUnkid = mintLoginEmployeeunkid
                objCommonATLog._ClientIP = mstrClientIP
                objCommonATLog._HostName = mstrHostName
                objCommonATLog._FromWeb = mblnIsWeb
                objCommonATLog._AuditUserId = mintAuditUserId
                objCommonATLog._CompanyUnkid = mintCompanyUnkid
                objCommonATLog._AuditDate = mdtAuditDate
                'S.SANDEEP [28-May-2018] -- END

                If objCommonATLog.Insert_TranAtLog(objDataOperation, "cmclaim_request_master", "crmasterunkid", mintMasterId, "cmclaim_approval_tran", "crapprovaltranunkid", -1, 2, 2, False, intUserId) = False Then
                    exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                    Throw exForce
                End If

                'S.SANDEEP [28-May-2018] -- START
                'ISSUE/ENHANCEMENT : {Audit Trails} 
                objCommonATLog = Nothing
                'S.SANDEEP [28-May-2018] -- END


            End If

            If _ObjOperation Is Nothing Then
                objDataOperation.ReleaseTransaction(True)
            End If

        Catch ex As Exception
            If _ObjOperation Is Nothing Then objDataOperation.ReleaseTransaction(False)
            Throw New Exception(ex.Message & "; Procedure Name: CancelExpense; Module Name: " & mstrModuleName)
        End Try
        Return True
    End Function

    ''' <summary>
    ''' Modify By: Pinkal Jariwala
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>
    Public Function GetEmployeeLastApprovedExpenseDetail(ByVal intClaimMstId As Integer) As DataTable
        Dim mdtTable As DataTable = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception
        Try
            Dim objDataOperation As New clsDataOperation
            objDataOperation.ClearParameters()

            strQ = " SELECT DISTINCT TOP 1 cmclaim_approval_tran.approveremployeeunkid " & _
                      ",cmclaim_approval_tran.crapproverunkid " & _
                      ",cmapproverlevel_master.crlevelunkid " & _
                      ",cmapproverlevel_master.crpriority " & _
                      " FROM cmclaim_approval_tran " & _
                      " JOIN cmclaim_request_master ON cmclaim_request_master.crmasterunkid = cmclaim_approval_tran.crmasterunkid AND cmclaim_request_master.isvoid = 0 AND cmclaim_request_master.statusunkid NOT IN (3,6) " & _
                      " JOIN cmexpapprover_master ON cmexpapprover_master.crapproverunkid = cmclaim_approval_tran.crapproverunkid AND cmexpapprover_master.isvoid = 0 " & _
                      " JOIN cmapproverlevel_master ON cmapproverlevel_master.crlevelunkid = cmexpapprover_master.crlevelunkid AND cmapproverlevel_master.isactive = 1 " & _
                      " WHERE cmclaim_approval_tran.crmasterunkid = @crmasterunkid AND cmclaim_approval_tran.isvoid = 0 AND cmclaim_approval_tran.statusunkid = 1 " & _
                      " ORDER by cmapproverlevel_master.crpriority DESC"


            objDataOperation.AddParameter("@crmasterunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intClaimMstId)
            Dim dsList As DataSet = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            If dsList IsNot Nothing Then
                If dsList.Tables(0).Rows.Count > 0 Then
                    mdtTable = dsList.Tables(0)
                Else

                    strQ = " SELECT DISTINCT TOP 1 cmclaim_approval_tran.approveremployeeunkid " & _
                               ",cmclaim_approval_tran.crapproverunkid " & _
                               ",cmapproverlevel_master.crlevelunkid " & _
                               ",cmapproverlevel_master.crpriority " & _
                               " FROM cmclaim_approval_tran " & _
                               " JOIN cmclaim_request_master ON cmclaim_request_master.crmasterunkid = cmclaim_approval_tran.crmasterunkid AND cmclaim_request_master.isvoid = 0 AND cmclaim_request_master.statusunkid NOT IN (3,6) " & _
                               " JOIN cmexpapprover_master ON cmexpapprover_master.crapproverunkid = cmclaim_approval_tran.crapproverunkid AND cmexpapprover_master.isvoid = 0 " & _
                               " JOIN cmapproverlevel_master ON cmapproverlevel_master.crlevelunkid = cmexpapprover_master.crlevelunkid AND cmapproverlevel_master.isactive = 1 " & _
                               " WHERE cmclaim_approval_tran.crmasterunkid = @crmasterunkid AND cmclaim_approval_tran.isvoid = 0 AND cmclaim_approval_tran.statusunkid = 2 " & _
                               " ORDER by cmapproverlevel_master.crpriority "

                    objDataOperation.ClearParameters()
                    objDataOperation.AddParameter("@crmasterunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intClaimMstId)
                    dsList = objDataOperation.ExecQuery(strQ, "List")

                    If objDataOperation.ErrorMessage <> "" Then
                        exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                        Throw exForce
                    End If

                    If dsList IsNot Nothing AndAlso dsList.Tables(0).Rows.Count > 0 Then
                        mdtTable = dsList.Tables(0)
                    End If

                End If

            End If

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: GetEmployeeLastApprovedExpenseDetail; Module Name: " & mstrModuleName)
        End Try
        Return mdtTable
    End Function

    'Pinkal (13-Jul-2015) -- Start
    'Enhancement - WORKING ON C & R ACCESS PRIVILEGE.


    'Pinkal (24-Aug-2015) -- Start
    'Enhancement - WORKING ON ACTIVE EMPLOYEE CONDITION.

    '' <summary>
    '' Modify By: Pinkal Jariwala
    '' </summary>
    '' <purpose> Assign all Property variable </purpose>
    'Public Sub SendMailToApprover(ByVal intExpCategoryID As Integer, ByVal blnPaymentApprovalWithLeaveApproval As Boolean, _
    '                                               ByVal intClaimFormID As Integer, ByVal mstrFormNo As String, ByVal intEmpId As Integer, _
    '                                               ByVal intPriority As Integer, ByVal intStatusID As Integer, _
    '                                               ByVal mstrPriorityFilter As String, _
    '                                               Optional ByVal intCompanyUnkId As Integer = 0, _
    '                                               Optional ByVal strArutiSelfServiceURL As String = "", _
    '                                               Optional ByVal iLoginTypeId As Integer = 0, _
    '                                               Optional ByVal iLoginEmployeeId As Integer = 0, _
    '                                               Optional ByVal iUserId As Integer = 0, _
    '                                               Optional ByVal mstrWebFrmName As String = "")

    '    Dim strLink As String
    '    Try

    '        If intCompanyUnkId <= 0 Then intCompanyUnkId = Company._Object._Companyunkid
    '        If strArutiSelfServiceURL = "" Then strArutiSelfServiceURL = ConfigParameter._Object._ArutiSelfServiceURL

    '        Dim objNet As New clsNetConnectivity
    '        If objNet._Conected = False Then Exit Sub

    '        If intStatusID <> 1 Then Exit Sub 'APPROVED STATUS

    '        Dim dsPedingList As DataSet = GetApproverExpesneList("List", False, blnPaymentApprovalWithLeaveApproval, intExpCategoryID, True, True, , , , intClaimFormID)
    '        Dim dtPendingList As DataTable = New DataView(dsPedingList.Tables(0), "employeeunkid = " & intEmpId & " AND claimrequestno = '" & mstrFormNo.Trim & "' " & IIf(mstrPriorityFilter.Trim.Length > 0, " AND " & mstrPriorityFilter, ""), "", DataViewRowState.CurrentRows).ToTable

    '        If dtPendingList.Rows.Count <= 0 Then Exit Sub

    '        Dim objEmp As New clsEmployee_Master
    '        Dim objMail As New clsSendMail
    '        Dim drRow() As DataRow = dtPendingList.Select("crpriority = " & CInt(dtPendingList.Rows(0)("crpriority")))

    '        If drRow.Length > 0 Then
    '            Dim inttempApproverID As Integer = -1
    '            Dim inttempPriority As Integer = -1
    '            For i As Integer = 0 To drRow.Length - 1
    '                If inttempApproverID <> CInt(drRow(i)("crapproverunkid")) AndAlso inttempPriority <> CInt(drRow(i)("crpriority")) Then

    '                    objEmp._Employeeunkid = CInt(drRow(i)("crapproverunkid"))
    '                    strLink = strArutiSelfServiceURL & "/Claims_And_Expenses/wPg_ExpenseApproval.aspx?" & System.Web.HttpUtility.UrlEncode(clsCrypto.Encrypt(intCompanyUnkId.ToString & "|" & drRow(i)("mapuserunkid").ToString & "|" & intEmpId.ToString & "|" & drRow(i)("crapproverunkid").ToString & "|" & drRow(i)("crmasterunkid").ToString & "|" & drRow(i)("crapprovaltranunkid").ToString & "|" & intExpCategoryID))
    '                    If objEmp._Email.Trim.Length <= 0 Then Continue For
    '                    objMail._Subject = Language.getMessage(mstrModuleName, 1, "Notification for approving Claim Application form")

    '                    Dim strMessage As String = ""

    '                    strMessage = "<HTML> <BODY>"

    '                    strMessage &= Language.getMessage(mstrModuleName, 2, "Dear") & " " & drRow(i)("approvername").ToString() & ", <BR><BR>"
    '                    strMessage &= "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;" & Language.getMessage(mstrModuleName, 3, "This is the notification for approving claim application no") & " " & mstrFormNo.Trim & _
    '                                            Language.getMessage(mstrModuleName, 4, " of ") & drRow(i)("employeename").ToString() & "."
    '                    strMessage &= "<BR></BR><BR></BR>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;" & Language.getMessage(mstrModuleName, 8, "Please click on the following link to approve claim form.")
    '                    strMessage &= "<BR></BR>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<a href='" & strLink & "'>" & strLink & "</a>"
    '                    strMessage &= "<BR></BR><BR></BR><center>""POWERED BY ARUTI HR & PAYROLL MANAGEMENT SOFTWARE""</center>"
    '                    strMessage &= "</BODY></HTML>"

    '                    objMail._Message = strMessage
    '                    objMail._ToEmail = objEmp._Email
    '                    If iLoginTypeId <= 0 Then iLoginTypeId = enLogin_Mode.DESKTOP
    '                    If mstrWebFrmName.Trim.Length > 0 Then
    '                        objMail._Form_Name = mstrWebFrmName
    '                    End If
    '                    objMail._LogEmployeeUnkid = iLoginEmployeeId
    '                    objMail._OperationModeId = iLoginTypeId
    '                    objMail._UserUnkid = iUserId
    '                    objMail._SenderAddress = IIf(objEmp._Email = "", objEmp._Firstname & " " & objEmp._Surname, objEmp._Email)
    '                    objMail._ModuleRefId = clsSendMail.enAT_VIEW_TYPE.CLAIMREQUEST_MGT
    '                    objMail.SendMail()
    '                    inttempApproverID = CInt(drRow(i)("crapproverunkid"))
    '                    inttempPriority = CInt(drRow(i)("crpriority"))
    '                Else
    '                    Exit For
    '                End If
    '            Next

    '        End If


    '    Catch ex As Exception
    '        DisplayError.Show("-1", ex.Message, "SendMailToApprover", mstrModuleName)
    '    End Try
    'End Sub

    Public Sub SendMailToApprover(ByVal intExpCategoryID As Integer, ByVal blnPaymentApprovalWithLeaveApproval As Boolean _
                                                  , ByVal intClaimFormID As Integer, ByVal mstrFormNo As String, ByVal intEmpId As Integer _
                                                  , ByVal intPriority As Integer, ByVal intStatusID As Integer _
                                                  , ByVal mstrPriorityFilter As String, ByVal xDatabaseName As String, ByVal strEmployeeAsOnDate As String _
                                                  , Optional ByVal intCompanyUnkId As Integer = 0 _
                                                  , Optional ByVal strArutiSelfServiceURL As String = "" _
                                                  , Optional ByVal iLoginTypeId As Integer = 0 _
                                                  , Optional ByVal iLoginEmployeeId As Integer = 0 _
                                                  , Optional ByVal iUserId As Integer = 0 _
                                                  , Optional ByVal mstrWebFrmName As String = "", Optional ByVal objDoOperation As clsDataOperation = Nothing _
                                                  , Optional ByVal mblnSendEmailByThreading As Boolean = False)

        'Pinkal (20-May-2022) -- Optimize Global Claim Request for NMB.[Optional ByVal mblnSendEmailByThreading As Boolean = False]

        Dim strLink As String
        Try

            If intCompanyUnkId <= 0 Then intCompanyUnkId = Company._Object._Companyunkid
            If strArutiSelfServiceURL = "" Then strArutiSelfServiceURL = ConfigParameter._Object._ArutiSelfServiceURL

            Dim objNet As New clsNetConnectivity
            If objNet._Conected = False Then Exit Sub

            If intStatusID <> 1 Then Exit Sub 'APPROVED STATUS


            'Pinkal (20-May-2022) -- Start
            'Optimize Global Claim Request for NMB.

            'Dim dsPedingList As DataSet = GetApproverExpesneList("List", False, blnPaymentApprovalWithLeaveApproval, xDatabaseName _
            '                                                                                    , iUserId, strEmployeeAsOnDate, intExpCategoryID, True, True, -1, "", intClaimFormID, objDoOperation)


            ''Pinkal (05-Sep-2020) -- Start
            ''Optimzation OT NMB:  Working on Optimzing Various modules for Garbage Colletion Issue.
            ''Dim dtPendingList As DataTable = New DataView(dsPedingList.Tables(0), "employeeunkid = " & intEmpId & " AND claimrequestno = '" & mstrFormNo.Trim & "' " & IIf(mstrPriorityFilter.Trim.Length > 0, " AND " & mstrPriorityFilter, ""), "", DataViewRowState.CurrentRows).ToTable
            'Dim dtPendingList As DataTable = New DataView(dsPedingList.Tables(0), "employeeunkid = " & intEmpId & " AND claimrequestno = '" & mstrFormNo.Trim & "' " & IIf(mstrPriorityFilter.Trim.Length > 0, " AND " & mstrPriorityFilter, ""), "crapproverunkid", DataViewRowState.CurrentRows).ToTable
            ''Pinkal (05-Sep-2020) -- End

            'Pinkal (16-Aug-2022) -- Start
            'Solving Bug Related to Sending Email to Approver Where PaymentApprovalWithLeaveApproval = True for Pacra.
            'Dim mstrSearch As String =  "cmclaim_request_master.employeeunkid = " & intEmpId & " AND  cmclaim_request_master.claimrequestno = '" & mstrFormNo.Trim & "' " & IIf(mstrPriorityFilter.Trim.Length > 0, "AND " & mstrPriorityFilter.Replace("crpriority", "cmapproverlevel_master.crpriority"), "")
            Dim mstrSearch As String = ""
            If blnPaymentApprovalWithLeaveApproval AndAlso intExpCategoryID = enExpenseType.EXP_LEAVE Then
                mstrSearch = "cmclaim_request_master.employeeunkid = " & intEmpId & " AND  cmclaim_request_master.claimrequestno = '" & mstrFormNo.Trim & "' " & IIf(mstrPriorityFilter.Trim.Length > 0, "AND " & mstrPriorityFilter.Replace("crpriority", "lvapproverlevel_master.priority"), "")
            Else
                mstrSearch = "cmclaim_request_master.employeeunkid = " & intEmpId & " AND  cmclaim_request_master.claimrequestno = '" & mstrFormNo.Trim & "' " & IIf(mstrPriorityFilter.Trim.Length > 0, "AND " & mstrPriorityFilter.Replace("crpriority", "cmapproverlevel_master.crpriority"), "")
            End If
            'Pinkal (16-Aug-2022) -- End

            'Pinkal (20-May-2022) -- Start
            'Optimize Global Claim Request for NMB.

            Dim dsPedingList As DataSet = GetApproverExpesneList("List", False, blnPaymentApprovalWithLeaveApproval, xDatabaseName _
                                                                                             , iUserId, strEmployeeAsOnDate, intExpCategoryID, True, True, -1, mstrSearch, intClaimFormID, objDoOperation)

            Dim dtPendingList As DataTable = New DataView(dsPedingList.Tables(0), "", "crapproverunkid", DataViewRowState.CurrentRows).ToTable()

            'Pinkal (20-May-2022) -- End



            If dtPendingList.Rows.Count <= 0 Then Exit Sub


            Dim objMail As New clsSendMail
            Dim drRow() As DataRow = dtPendingList.Select("crpriority = " & CInt(dtPendingList.Rows(0)("crpriority")))

            If drRow.Length > 0 Then
                Dim inttempApproverID As Integer = -1
                Dim inttempPriority As Integer = -1
                For i As Integer = 0 To drRow.Length - 1


                    If inttempApproverID <> CInt(drRow(i)("crapproverunkid")) Then

                        strLink = strArutiSelfServiceURL & "/Claims_And_Expenses/wPg_ExpenseApproval.aspx?" & System.Web.HttpUtility.UrlEncode(clsCrypto.Encrypt(intCompanyUnkId.ToString & "|" & drRow(i)("mapuserunkid").ToString & "|" & intEmpId.ToString & "|" & drRow(i)("crapproverunkid").ToString & "|" & drRow(i)("crmasterunkid").ToString & "|" & drRow(i)("crapprovaltranunkid").ToString & "|" & intExpCategoryID))

                        'objMail._Subject = Language.getMessage(mstrModuleName, 1, "Notification for approving Claim Application form")
                        Dim mstrSubject As String = Language.getMessage(mstrModuleName, 1, "Notification for approving Claim Application form")

                        Dim strMessage As String = ""

                        strMessage = "<HTML> <BODY>"


                        'Pinkal (01-Apr-2019) -- Start
                        'Enhancement - Working on Leave Changes for NMB.

                        'strMessage &= Language.getMessage(mstrModuleName, 2, "Dear") & " " & drRow(i)("approvername").ToString() & ", <BR><BR>"
                        'strMessage &= "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;" & Language.getMessage(mstrModuleName, 3, "This is the notification for approving claim application no") & " " & mstrFormNo.Trim & _
                        '                        Language.getMessage(mstrModuleName, 4, " of ") & drRow(i)("employeename").ToString() & "."
                        'strMessage &= "<BR></BR><BR></BR>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;" & Language.getMessage(mstrModuleName, 8, "Please click on the following link to approve claim form.")
                        'strMessage &= "<BR></BR>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<a href='" & strLink & "'>" & strLink & "</a>"

                        strMessage &= Language.getMessage(mstrModuleName, 2, "Dear") & " " & getTitleCase(drRow(i)("approvername").ToString()) & ", <BR><BR>"
                        'S.SANDEEP |29-MAR-2022| -- START
                        'ISSUE/ENHANCEMENT : OLD-580-V2
                        'strMessage &= Language.getMessage(mstrModuleName, 3, "This is the notification for approving claim application no") & " <B>(" & mstrFormNo.Trim() & ")</B> " & _
                        '                        Language.getMessage(mstrModuleName, 4, " of ") & " " & getTitleCase(drRow(i)("employeename").ToString()) & "."

                        strMessage &= Language.getMessage(mstrModuleName, 3, "This is the notification for approving claim application no") & " <B>(" & mstrFormNo.Trim() & ")</B> "
                        Select Case CInt(drRow(i)("expensetypeid"))
                            'S.SANDEEP |27-MAY-2022| -- START
                            'ISSUE/ENHANCEMENT : AC2-389
                            'Case enExpenseType.EXP_REBATE_PRIVILEGE, enExpenseType.EXP_REBATE_DUTY
                            Case enExpenseType.EXP_REBATE_PRIVILEGE, enExpenseType.EXP_REBATE_DUTY, enExpenseType.EXP_REBATE_PRIVILEGE_DPNDT
                                'S.SANDEEP |27-MAY-2022| -- END
                                strMessage &= Language.getMessage(mstrModuleName, 9, " with the type of expense : ") & " <B>" & drRow(i)("expensetype").ToString() & " (" & drRow(i)("rebate_percent").ToString() & "%)" & "</B> "
                        End Select
                        strMessage &= Language.getMessage(mstrModuleName, 4, " of ") & " " & getTitleCase(drRow(i)("employeename").ToString()) & "."
                        'S.SANDEEP |29-MAR-2022| -- END

                        strMessage &= Language.getMessage(mstrModuleName, 8, "Please click on the following link to approve claim form.")
                        strMessage &= "<BR></BR>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<a href='" & strLink & "'>" & strLink & "</a>"

                        'Pinkal (01-Apr-2019) -- End

                        strMessage &= "<BR></BR><BR></BR><center>""POWERED BY ARUTI HR & PAYROLL MANAGEMENT SOFTWARE""</center>"
                        strMessage &= "</BODY></HTML>"

                        'Pinkal (06-Sep-2021)-- Start
                        'KBC Bug : Claim Retirement didn't allow other currency even if claim is approved in other currency.

                        'objMail._Message = strMessage
                        'objMail._ToEmail = drRow(i)("approveremail").ToString()

                        'If iLoginTypeId <= 0 Then iLoginTypeId = enLogin_Mode.DESKTOP
                        'If mstrWebFrmName.Trim.Length > 0 Then
                        '    objMail._Form_Name = mstrWebFrmName
                        '    objMail._WebClientIP = mstrWebClientIP
                        '    objMail._WebHostName = mstrWebHostName
                        'End If
                        'objMail._LogEmployeeUnkid = iLoginEmployeeId
                        'objMail._OperationModeId = iLoginTypeId
                        'objMail._UserUnkid = iUserId
                        'objMail._SenderAddress = IIf(drRow(i)("approveremail") = "", drRow(i)("approvername"), drRow(i)("approveremail"))
                        'objMail._ModuleRefId = clsSendMail.enAT_VIEW_TYPE.CLAIMREQUEST_MGT
                        'objMail.SendMail(intCompanyUnkId)

                        'Pinkal (20-May-2022) -- Start
                        'Optimize Global Claim Request for NMB.

                        'Dim objEmailColl As New clsEmailCollection(drRow(i)("approveremail").ToString(), mstrSubject, strMessage, mstrWebFormName, _
                        '                                               iLoginEmployeeId, mstrWebClientIP, mstrWebHostName, _
                        '                                               iUserId, iLoginTypeId, clsSendMail.enAT_VIEW_TYPE.CLAIMREQUEST_MGT, _
                        '                                               IIf(drRow(i)("approveremail") = "", drRow(i)("approvername"), drRow(i)("approveremail")))

                        'gobjEmailList.Add(objEmailColl)


                        If mblnSendEmailByThreading Then

                            If LstClaimEmailList Is Nothing Then
                                LstClaimEmailList = New List(Of clsEmailCollection)
                            End If

                            Dim objEmailColl As New clsEmailCollection(drRow(i)("approveremail").ToString(), mstrSubject, strMessage, mstrFormName, _
                                                                       iLoginEmployeeId, mstrWebClientIP, mstrWebHostName, _
                                                                       iUserId, iLoginTypeId, clsSendMail.enAT_VIEW_TYPE.CLAIMREQUEST_MGT, _
                                                                       IIf(drRow(i)("approveremail") = "", drRow(i)("approvername"), drRow(i)("approveremail")))

                            LstClaimEmailList.Add(objEmailColl)
                        Else

                            objMail._Subject = mstrSubject
                            objMail._Message = strMessage
                            objMail._ToEmail = drRow(i)("approveremail").ToString()

                            If iLoginTypeId <= 0 Then iLoginTypeId = enLogin_Mode.DESKTOP
                            If mstrWebFrmName.Trim.Length > 0 Then
                                objMail._Form_Name = mstrWebFrmName
                                objMail._ClientIP = mstrWebClientIP
                                objMail._HostName = mstrWebHostName
                            End If
                            objMail._LogEmployeeUnkid = iLoginEmployeeId
                            objMail._OperationModeId = iLoginTypeId
                            objMail._UserUnkid = iUserId
                            objMail._SenderAddress = IIf(drRow(i)("approveremail") = "", drRow(i)("approvername"), drRow(i)("approveremail"))
                            objMail._ModuleRefId = clsSendMail.enAT_VIEW_TYPE.CLAIMREQUEST_MGT
                            objMail.SendMail(intCompanyUnkId)
                        End If

                        'Pinkal (20-May-2022) -- End

                        'Pinkal (06-Sep-2021)-- End

                        inttempApproverID = CInt(drRow(i)("crapproverunkid"))
                        inttempPriority = CInt(drRow(i)("crpriority"))
                    Else
                        'Pinkal (05-Sep-2020) -- Start
                        'Optimzation OT NMB:  Working on Optimzing Various modules for Garbage Colletion Issue.
                        'Exit For
                        Continue For
                        'Pinkal (05-Sep-2020) -- End
                    End If
                Next


            End If


        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: SendMailToApprover; Module Name: " & mstrModuleName)
        End Try
    End Sub

    'Pinkal (13-Jul-2015) -- End

    'Pinkal (21-Oct-2015) -- Start
    'Enhancement - Working on Problem not Showing expense in Leave Form Report. 

    Public Function GetEmployeeClaimApproverListWithPriority(ByVal intEmployeeID As Integer, Optional ByVal intFormID As Integer = -1, Optional ByVal objDataOperation As clsDataOperation = Nothing) As DataSet
        Dim dsList As DataSet = Nothing
        Dim exForce As Exception
        Dim strQ As String = ""
        Try

            If objDataOperation Is Nothing Then
                objDataOperation = New clsDataOperation
            End If

            If intEmployeeID > 0 AndAlso intFormID <= 0 Then

                strQ = " SELECT " & _
                       " cmexpapprover_master.crapproverunkid " & _
                       ",cmexpapprover_master.employeeunkid " & _
                       ",crpriority " & _
                       " FROM cmexpapprover_tran " & _
                       " JOIN cmexpapprover_master ON cmexpapprover_tran.crapproverunkid = cmexpapprover_master.crapproverunkid  AND cmexpapprover_master.isvoid = 0 " & _
                       " JOIN cmapproverlevel_master 	ON cmapproverlevel_master.crlevelunkid = cmexpapprover_master.crlevelunkid AND cmapproverlevel_master.isactive = 1" & _
                       " WHERE cmexpapprover_tran.employeeunkid = " & intEmployeeID & " AND cmexpapprover_tran.isvoid = 0"

            ElseIf intEmployeeID > 0 AndAlso intFormID > 0 Then

                strQ = " SELECT " & _
                          " cmexpapprover_master.crapproverunkid " & _
                          ", cmexpapprover_master.employeeunkid " & _
                          ", cmapproverlevel_master.crpriority " & _
                          " FROM cmclaim_approval_tran " & _
                          " JOIN cmclaim_request_master ON cmclaim_request_master.crmasterunkid = cmclaim_approval_tran.crmasterunkid AND cmclaim_request_master.isvoid =0  " & _
                          " JOIN cmexpapprover_master ON cmclaim_approval_tran.crapproverunkid = cmexpapprover_master.crapproverunkid     AND cmexpapprover_master.isvoid = 0 " & _
                          " JOIN cmapproverlevel_master ON cmapproverlevel_master.crlevelunkid = cmexpapprover_master.crlevelunkid 	AND cmexpapprover_master.isactive = 1 " & _
                          " WHERE cmclaim_request_master.employeeunkid = " & intEmployeeID & " AND cmclaim_request_master.referenceunkid = " & intFormID & "  AND cmclaim_approval_tran.isvoid = 0"

            End If
            objDataOperation.ClearParameters()
            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "GetEmployeeClaimApproverListWithPriority", mstrModuleName)
        End Try
        Return dsList
    End Function

    'Pinkal (21-Oct-2015) -- End



    'Pinkal (18-Feb-2016) -- Start
    'Enhancement - CR Changes for ASP as per Rutta's Request.


    Public Function GetGlobalApprovalData(ByVal xDatabaseName As String _
                                                              , ByVal xUserUnkid As Integer _
                                                              , ByVal xYearUnkid As Integer _
                                                              , ByVal xCompanyUnkid As Integer _
                                                              , ByVal xPeriodStart As DateTime _
                                                              , ByVal xPeriodEnd As DateTime _
                                                              , ByVal xUserModeSetting As String _
                                                              , ByVal xOnlyApproved As Boolean _
                                                              , ByVal xIncludeIn_ActiveEmployee As Boolean _
                                                              , ByVal strTableName As String _
                                                              , ByVal xLeaveBalanceSetting As Integer _
                                                              , ByVal mblnPaymentApprovalwithLeaveApproval As Boolean _
                                                              , ByVal iExpenseCategoryID As Integer _
                                                              , ByVal iApproverID As Integer _
                                                              , ByVal mstrFilter As String _
                                                              , ByVal isFromApprovalList As Boolean _
                                                              , ByVal IsExternalApprover As Boolean) As DataTable



        'Pinkal (01-Mar-2016) --  'Enhancement - Implementing External Approver in Claim Request & Leave Module.[]

        Dim dtList As DataTable = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception
        Dim dtTable As DataTable = Nothing
        Try

            Dim xDateJoinQry, xDateFilterQry, xAdvanceJoinQry As String
            xDateJoinQry = "" : xDateFilterQry = "" : xAdvanceJoinQry = ""
            Call GetDatesFilterString(xDateJoinQry, xDateFilterQry, xPeriodStart, xPeriodEnd, , , xDatabaseName)
            Call GetAdvanceFilterQry(xAdvanceJoinQry, xPeriodEnd, xDatabaseName)

            objDataOperation = New clsDataOperation


            strQ = "SELECT " & _
                      "  cmclaim_approval_tran.crmasterunkid " & _
                      ", cmclaim_approval_tran.crtranunkid " & _
                      ", cmclaim_approval_tran.crapprovaltranunkid " & _
                      ", cmclaim_request_master.employeeunkid " & _
                      ", cmclaim_approval_tran.expenseunkid " & _
                      ", cmclaim_approval_tran.secrouteunkid " & _
                      ", cmclaim_approval_tran.costingunkid " & _
                      ", ISNULL(claimrequestno, '') AS ClaimNo " & _
                      ", ISNULL(hremployee_master.employeecode, '') + ' - '  + ISNULL(hremployee_master.firstname, '') + ' ' + ISNULL(hremployee_master.othername, '') + ' ' + ISNULL(hremployee_master.surname, '') AS Employee " & _
                      ", ISNULL(cmexpense_master.name, '') AS Expense " & _
                      ", ISNULL(cms.name, '') AS 'Sector' " & _
                      ", CASE " & _
                      "     WHEN cmexpense_master.uomunkid = 1 THEN @Qty " & _
                      "     WHEN cmexpense_master.uomunkid = 2 THEN @Amt " & _
                      "  END AS UOM " & _
                      ", ISNULL(cmclaim_request_tran.quantity,0.00) AS AppliedQty " & _
                      ", ISNULL(cmclaim_request_tran.amount,0.00) AS AppliedAmount " & _
                      ", ISNULL(cmclaim_approval_tran.quantity,0.00) AS quantity " & _
                      ", ISNULL(cmclaim_approval_tran.unitprice,0.00) AS unitprice " & _
                      ", ISNULL(cmclaim_approval_tran.amount,0.00) AS amount " & _
                      ", '' AS Expense_Remark " & _
                      ", cmexpense_master.isleaveencashment AS isleaveencashment " & _
                      ", cmexpense_master.isaccrue AS isaccrue " & _
                      ", cmexpense_master.leavetypeunkid " & _
                      ", cmclaim_approval_tran.crapproverunkid " & _
                      ", cmclaim_approval_tran.approveremployeeunkid " & _
                      ", cmexpense_master.uomunkid " & _
                      ", cmclaim_approval_tran.voidloginemployeeunkid " & _
                      ", cmclaim_request_master.claim_remark " & _
                      ", cmclaim_request_master.transactiondate As ClaimDate " & _
                      ", ISNULL(cmclaim_approval_tran.costcenterunkid,0) AS costcenterunkid " & _
                      ", ISNULL(cmexpense_master.glcodeunkid,0) AS glcodeunkid " & _
                      ", ISNULL(cmexpense_master.description,'') AS gldesc " & _
                      ", ISNULL(cmexpense_master.isbudgetmandatory,0) AS isbudgetmandatory " & _
                      ", ISNULL(cmexpense_master.ishrexpense,0) AS ishrexpense " & _
                       ",ISNULL(cmclaim_approval_tran.countryunkid,0) AS countryunkid " & _
                       ",ISNULL(cmclaim_approval_tran.base_countryunkid,0) AS base_countryunkid " & _
                       ",ISNULL(cmclaim_approval_tran.base_amount,0.00) AS base_amount " & _
                       ",ISNULL(cmclaim_approval_tran.exchangerateunkid,0) AS exchangerateunkid " & _
                       ",ISNULL(cmclaim_approval_tran.exchange_rate,0.00) AS exchange_rate " & _
                      ", ISNULL(cmclaim_approval_tran.airline,'') AS airline " & _
                      ", ISNULL(cfrt.name,'') AS travelfrom " & _
                      ", ISNULL(ctrt.name,'') AS travelto " & _
                      ", ISNULL(cmclaim_approval_tran.flightno,'') AS flightno " & _
                      ", cmclaim_approval_tran.traveldate " & _
                      ", ISNULL(cmclaim_approval_tran.other_person,'') AS other_person " & _
                      ", ISNULL(cmclaim_approval_tran.dpndtbeneficetranunkid,0) AS dpndtbeneficetranunkid " & _
                      ", ISNULL(cmclaim_approval_tran.fromrouteid,0) AS fromrouteid " & _
                      ", ISNULL(cmclaim_approval_tran.torouteid,0) AS torouteid " & _
                      ", CASE WHEN ISNULL(cmclaim_request_tran.dpndtbeneficetranunkid,0) > 0 THEN ISNULL(hrdependants_beneficiaries_tran.first_name,'') + ' ' + ISNULL(hrdependants_beneficiaries_tran.last_name,'') " & _
                      "       WHEN ISNULL(cmclaim_request_tran.dpndtbeneficetranunkid,0) <= 0 AND ISNULL(cmclaim_request_tran.other_person,'') <> '' THEN ISNULL(cmclaim_request_tran.other_person,'') " & _
                      "       WHEN ISNULL(cmclaim_request_tran.dpndtbeneficetranunkid,0) <= 0 AND ISNULL(cmclaim_request_tran.other_person,'') = '' THEN ISNULL(hremployee_master.firstname,'') + ' ' + ISNULL(hremployee_master.surname,'') " & _
                      "  ELSE '' END  AS name " & _
                      ", CASE WHEN ISNULL(cmclaim_request_tran.dpndtbeneficetranunkid,0) > 0 THEN @DP " & _
                      "       WHEN ISNULL(cmclaim_request_tran.dpndtbeneficetranunkid,0) <= 0 AND ISNULL(cmclaim_request_tran.other_person,'') <> '' THEN @OH " & _
                      "       WHEN ISNULL(cmclaim_request_tran.dpndtbeneficetranunkid,0) <= 0 AND ISNULL(cmclaim_request_tran.other_person,'') = '' THEN @SF " & _
                      "  ELSE '' END  AS psgtype " & _
                      ", ISNULL(RL.name,'') AS relation " & _
                      ", ISNULL(rebate_percent,100) AS rebate_percent " & _
                      ", ISNULL(rebateid,0) AS rebateid " & _
                      ", ISNULL(seattypeid,0) AS seattypeid " & _
                      ", CASE WHEN ISNULL(seattypeid,0) = " & enRebateSeat.RB_SA & " THEN @SA " & _
                      "       WHEN ISNULL(seattypeid,0) = " & enRebateSeat.RB_FIRM & " THEN @FM " & _
                      "  ELSE '' END AS SeatType " & _
                      ", cmclaim_request_master.expensetypeid " & _
                      ", cmclaim_request_master.referenceunkid " & _
                      ", cmclaim_request_master.frommoduleid " & _
                      ", cmclaim_request_master.modulerefunkid " & _
                      ", ISNULL((SELECT TOP 1 cm.expenseunkid FROM cmexpense_master cm WHERE cm.expensetypeid = 6 AND cm.isaccrue = 1 AND cm.isactive = 1),0) AS deductexpenseunkid " & _
                      " FROM cmclaim_request_master " & _
                      " LEFT JOIN cmclaim_request_tran ON cmclaim_request_tran.crmasterunkid = cmclaim_request_master.crmasterunkid AND cmclaim_request_tran.isvoid = 0 " & _
                      " LEFT JOIN cmclaim_approval_tran ON cmclaim_approval_tran.crtranunkid = cmclaim_request_tran.crtranunkid AND cmclaim_approval_tran.crmasterunkid = cmclaim_request_master.crmasterunkid AND cmclaim_approval_tran.isvoid = 0 " & _
                      " JOIN hremployee_master ON cmclaim_request_master.employeeunkid = hremployee_master.employeeunkid " & _
                      " LEFT JOIN cmexpbalance_tran ON cmexpbalance_tran.expenseunkid = cmclaim_approval_tran.expenseunkid AND cmexpbalance_tran.employeeunkid = hremployee_master.employeeunkid " & _
                      " AND cmexpbalance_tran.isvoid = 0 AND cmexpbalance_tran.yearunkid = " & xYearUnkid & _
                      " LEFT JOIN cmexpense_master ON cmexpense_master.expenseunkid = cmclaim_approval_tran.expenseunkid  " & _
                      " LEFT JOIN cfcommon_master cms ON cms.masterunkid = cmclaim_approval_tran.secrouteunkid AND cms.mastertype = " & clsCommon_Master.enCommonMaster.SECTOR_ROUTE & _
                      " JOIN hrapprover_usermapping ON hrapprover_usermapping.approverunkid = cmclaim_approval_tran.crapproverunkid " & _
                      " LEFT JOIN hrdependants_beneficiaries_tran ON hrdependants_beneficiaries_tran.dpndtbeneficetranunkid = cmclaim_approval_tran.dpndtbeneficetranunkid AND hrdependants_beneficiaries_tran.isvoid = 0 " & _
                      " LEFT JOIN cfcommon_master AS RL ON RL.masterunkid = hrdependants_beneficiaries_tran.relationunkid AND RL.mastertype = " & clsCommon_Master.enCommonMaster.RELATIONS & " " & _
                      " LEFT JOIN cfcommon_master AS cfrt ON cfrt.masterunkid = cmclaim_approval_tran.fromrouteid AND cfrt.mastertype = " & clsCommon_Master.enCommonMaster.SECTOR_ROUTE & " " & _
                      " LEFT JOIN cfcommon_master AS ctrt ON ctrt.masterunkid = cmclaim_approval_tran.torouteid AND cfrt.mastertype = " & clsCommon_Master.enCommonMaster.SECTOR_ROUTE & " "

            'S.SANDEEP |29-JUN-2022| -- START {AC2-390_V2} [deductexpenseunkid : DEDUCTING BALANCE FROM PRIVILGE CATEGORY FOR DEPENEDANT CATEGORY] -- END

            'Pinkal (20-May-2022) -- 'Optimize Global Claim Request for NMB.[", cmclaim_request_master.referenceunkid , cmclaim_request_master.frommoduleid , cmclaim_request_master.modulerefunkid] 

            'S.SANDEEP |13-APR-2022| -- START (AC2-143) {ISNULL(cmclaim_approval_tran.travelfrom,'') -> ISNULL(cfrt.name,'') & ISNULL(cmclaim_approval_tran.travelto,'') -> ISNULL(ctrt.name,'') } -- END
            'S.SANDEEP |10-MAR-2022| -- ISSUE/ENHANCEMENT : OLD-580 [airline,travelfrom,travelto,flightno,traveldate,other_person,dpndtbeneficetranunkid,dependant  " LEFT JOIN hrdependants_beneficiaries_tran ON hrdependants_beneficiaries_tran.dpndtbeneficetranunkid = cmclaim_approval_tran.dpndtbeneficetranunkid AND hrdependants_beneficiaries_tran.isvoid = 0 "]

            'Pinkal (04-Feb-2019) -- 'Enhancement - Working on Leave & Claim Enhancement Phase 2 for NMB.[ ", ISNULL(cmclaim_approval_tran.costcenterunkid,0) AS costcenterunkid , ISNULL(cmexpense_master.glcodeunkid,0) AS glcodeunkid , ISNULL(cmexpense_master.description,'') AS gldesc , ISNULL(cmexpense_master.isbudgetmandatory,0) AS isbudgetmandatory , ISNULL(cmexpense_master.ishrexpense,0) AS ishrexpense ,ISNULL(cmclaim_approval_tran.countryunkid,0) AS countryunkid ,ISNULL(cmclaim_approval_tran.base_countryunkid,0) AS base_countryunkid ,ISNULL(cmclaim_approval_tran.base_amount,0.00) AS base_amount ,ISNULL(cmclaim_approval_tran.exchangerateunkid,0) AS exchangerateunkid ,ISNULL(cmclaim_approval_tran.exchange_rate,0.00) AS exchange_rate] 

            'Pinkal (22-Oct-2018) -- 'Enhancement - Implementing Claim & Request changes For NMB .[" LEFT JOIN cmexpense_master ON cmexpense_master.expenseunkid = cmexpbalance_tran.expenseunkid  " & _]

            'Pinkal (30-Apr-2018) - Start    'Enhancement  [Ref # 224] calculate/display the total balance & Balance As on date. [ ", cmclaim_request_master.transactiondate As ClaimDate " & _] 


            If mblnPaymentApprovalwithLeaveApproval AndAlso iExpenseCategoryID = enExpenseType.EXP_LEAVE Then
                strQ &= " AND usertypeid = " & enUserType.Approver & " "
            Else
                strQ &= " AND usertypeid = " & enUserType.crApprover & " "
            End If

            If xDateJoinQry.Trim.Length > 0 Then
                strQ &= xDateJoinQry
            End If

            If xAdvanceJoinQry.Trim.Length > 0 Then
                strQ &= xAdvanceJoinQry
            End If

            strQ &= " WHERE cmclaim_request_master.isvoid = 0 AND cmclaim_request_master.expensetypeid = " & iExpenseCategoryID & " And cmclaim_approval_tran.crapproverunkid = " & iApproverID & _
                        " AND cmclaim_approval_tran.statusunkid = 2 AND cmclaim_approval_tran.visibleid = 2  AND hrapprover_usermapping.userunkid = " & xUserUnkid


            'Pinkal (22-Mar-2016) -- Start
            'Enhancement - WORKING ON ENHANCEMENT ON CLAIM SUMMARY REPORT FOR KBC.
            'If mblnPaymentApprovalwithLeaveApproval Then
            '    strQ &= " AND cmclaim_request_master.frommoduleid =  " & enExpFromModuleID.FROM_EXPENSE
            'Else
            '    strQ &= " AND cmclaim_request_master.modulerefunkid =  " & enModuleReference.Leave
            'End If




            If mblnPaymentApprovalwithLeaveApproval Then
                'S.SANDEEP |13-APR-2022| -- START
                'ISSUE/ENHANCEMENT : AC2-143
                'strQ &= " AND cmclaim_request_master.modulerefunkid =  " & enModuleReference.Leave
                'S.SANDEEP |13-APR-2022| -- END
                Select Case CInt(iExpenseCategoryID)
                    Case enExpenseType.EXP_LEAVE
                        strQ &= " AND cmclaim_request_master.modulerefunkid =  " & enModuleReference.Leave
                        'Case enExpenseType.EXP_MEDICAL
                        '    strQ &= " AND cmclaim_request_master.modulerefunkid =  " & enModuleReference.Medical
                        'Case enExpenseType.EXP_TRAINING
                        '    strQ &= " AND cmclaim_request_master.modulerefunkid =  " & enModuleReference.Training
                        'Case enExpenseType.EXP_MISCELLANEOUS
                        '    strQ &= " AND cmclaim_request_master.modulerefunkid =  " & enModuleReference.Miscellaneous
                End Select
            Else
                'S.SANDEEP [09-OCT-2018] -- START
                'strQ &= " AND cmclaim_request_master.frommoduleid =  " & enExpFromModuleID.FROM_EXPENSE
                If iExpenseCategoryID = enExpenseType.EXP_TRAINING Then
                    strQ &= " AND cmclaim_request_master.frommoduleid =  " & enExpFromModuleID.FROM_TRAINING
                Else
                    strQ &= " AND cmclaim_request_master.frommoduleid =  " & enExpFromModuleID.FROM_EXPENSE
                End If
                'S.SANDEEP [09-OCT-2018] -- END
            End If
            'Pinkal (22-Mar-2016) -- End


            If xIncludeIn_ActiveEmployee = False Then
                If xDateFilterQry.Trim.Length > 0 Then
                    strQ &= xDateFilterQry
                End If
            End If

            If mstrFilter.Trim.Length > 0 Then
                strQ &= " AND " & mstrFilter
            End If

            objDataOperation.ClearParameters()

            'Pinkal (13-Aug-2020) -- Start
            'Bug NMB:  Working on IIS Freezing and Dump Issue for NMB.
            'objDataOperation.AddParameter("@Qty", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 1, "Quantity"))
            'objDataOperation.AddParameter("@Amt", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 2, "Amount"))
            objDataOperation.AddParameter("@Qty", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage("clsExpCommonMethods", 6, "Quantity"))
            objDataOperation.AddParameter("@Amt", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage("clsExpCommonMethods", 7, "Amount"))
            'Pinkal (13-Aug-2020) -- End

            'S.SANDEEP |10-MAR-2022| -- START
            'ISSUE/ENHANCEMENT : OLD-580
            objDataOperation.AddParameter("@DP", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage("clsclaim_request_tran", 100, "Dependant"))
            objDataOperation.AddParameter("@OH", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage("clsclaim_request_tran", 101, "Other"))
            objDataOperation.AddParameter("@SF", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage("clsclaim_request_tran", 102, "Self"))
            objDataOperation.AddParameter("@SA", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage("clsExpCommonMethods", 18, "S/A"))
            objDataOperation.AddParameter("@FM", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage("clsExpCommonMethods", 19, "FIRM"))
            'S.SANDEEP |10-MAR-2022| -- END


            Dim dsTran As DataSet = objDataOperation.ExecQuery(strQ, "List")


            'Pinkal (13-Jun-2016) -- Start
            'Enhancement - Working on Global Approval Expense Changes Required by GDS (Annor).
            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If
            'Pinkal (13-Jun-2016) -- End

            dtList = New DataTable("List")

            dtList.Columns.Add("IsChecked", System.Type.GetType("System.Boolean")).DefaultValue = False
            dtList.Columns.Add("crmasterunkid", System.Type.GetType("System.Int32")).DefaultValue = -1
            dtList.Columns.Add("crapprovaltranunkid", System.Type.GetType("System.Int32")).DefaultValue = -1
            dtList.Columns.Add("crtranunkid", System.Type.GetType("System.Int32")).DefaultValue = -1
            dtList.Columns.Add("employeeunkid", System.Type.GetType("System.Int32")).DefaultValue = -1
            dtList.Columns.Add("expenseunkid", System.Type.GetType("System.Int32")).DefaultValue = -1
            dtList.Columns.Add("secrouteunkid", System.Type.GetType("System.Int32")).DefaultValue = -1
            dtList.Columns.Add("costingunkid", System.Type.GetType("System.Int32")).DefaultValue = -1
            dtList.Columns.Add("crapproverunkid", System.Type.GetType("System.Int32")).DefaultValue = -1
            dtList.Columns.Add("approveremployeeunkid", System.Type.GetType("System.Int32")).DefaultValue = -1
            dtList.Columns.Add("ClaimNo", System.Type.GetType("System.String")).DefaultValue = ""
            dtList.Columns.Add("CLNo", System.Type.GetType("System.String")).DefaultValue = ""
            dtList.Columns.Add("Employee", System.Type.GetType("System.String")).DefaultValue = ""
            dtList.Columns.Add("Expense", System.Type.GetType("System.String"))
            dtList.Columns.Add("Sector", System.Type.GetType("System.String")).DefaultValue = ""
            dtList.Columns.Add("UOM", System.Type.GetType("System.String")).DefaultValue = ""
            dtList.Columns.Add("Balance", System.Type.GetType("System.Decimal"))
            dtList.Columns.Add("AppliedQty", System.Type.GetType("System.Decimal"))
            dtList.Columns.Add("AppliedAmount", System.Type.GetType("System.Decimal"))
            dtList.Columns.Add("quantity", System.Type.GetType("System.Decimal"))
            dtList.Columns.Add("unitprice", System.Type.GetType("System.Decimal"))
            dtList.Columns.Add("Amount", System.Type.GetType("System.Decimal"))
            dtList.Columns.Add("Expense_Remark", System.Type.GetType("System.String")).DefaultValue = ""

            'Pinkal (13-Jun-2016) -- Start
            'Enhancement - Working on Global Approval Expense Changes Required by GDS (Annor).
            dtList.Columns.Add("Claim_Remark", System.Type.GetType("System.String")).DefaultValue = ""
            'Pinkal (13-Jun-2016) -- End


            'Pinkal (04-Feb-2019) -- Start
            'Enhancement - Working on Leave & Claim Enhancement Phase 2 for NMB.
            dtList.Columns.Add("costcenterunkid", System.Type.GetType("System.Int32")).DefaultValue = 0
            dtList.Columns.Add("countryunkid", System.Type.GetType("System.Int32")).DefaultValue = 0
            dtList.Columns.Add("base_countryunkid", System.Type.GetType("System.Int32")).DefaultValue = 0
            dtList.Columns.Add("base_amount", System.Type.GetType("System.Decimal")).DefaultValue = 0
            dtList.Columns.Add("exchangerateunkid", System.Type.GetType("System.Int32")).DefaultValue = 0
            dtList.Columns.Add("exchange_rate", System.Type.GetType("System.Int32")).DefaultValue = 0
            'Pinkal (04-Feb-2019) -- End

            dtList.Columns.Add("isleaveencashment", System.Type.GetType("System.Boolean")).DefaultValue = False
            dtList.Columns.Add("isaccrue", System.Type.GetType("System.Boolean")).DefaultValue = False
            dtList.Columns.Add("leavetypeunkid", System.Type.GetType("System.Int32")).DefaultValue = -1
            dtList.Columns.Add("uomunkid", System.Type.GetType("System.Int32")).DefaultValue = -1
            dtList.Columns.Add("IsGrp", System.Type.GetType("System.Boolean")).DefaultValue = False
            dtList.Columns.Add("GrpId", System.Type.GetType("System.Int32")).DefaultValue = -1
            dtList.Columns.Add("AUD", System.Type.GetType("System.String")).DefaultValue = ""
            dtList.Columns.Add("voidloginemployeeunkid", System.Type.GetType("System.Int32")).DefaultValue = -1


            'Pinkal (30-Apr-2018) - Start
            'Enhancement  [Ref # 224] calculate/display the total balance & Balance As on date.
            dtList.Columns.Add("Claimdate", System.Type.GetType("System.DateTime")).DefaultValue = DBNull.Value
            'Pinkal (30-Apr-2018) - End

            'S.SANDEEP |10-MAR-2022| -- START
            'ISSUE/ENHANCEMENT : OLD-580
            dtList.Columns.Add("airline", System.Type.GetType("System.String")).DefaultValue = ""
            dtList.Columns.Add("travelfrom", System.Type.GetType("System.String")).DefaultValue = ""
            dtList.Columns.Add("travelto", System.Type.GetType("System.String")).DefaultValue = ""
            dtList.Columns.Add("flightno", System.Type.GetType("System.String")).DefaultValue = ""
            dtList.Columns.Add("traveldate", System.Type.GetType("System.DateTime")).DefaultValue = DBNull.Value
            dtList.Columns.Add("dpndtbeneficetranunkid", System.Type.GetType("System.Int32")).DefaultValue = 0
            dtList.Columns.Add("other_person", System.Type.GetType("System.String")).DefaultValue = ""
            dtList.Columns.Add("name", System.Type.GetType("System.String")).DefaultValue = ""
            dtList.Columns.Add("psgtype", System.Type.GetType("System.String")).DefaultValue = ""
            dtList.Columns.Add("relation", System.Type.GetType("System.String")).DefaultValue = ""
            dtList.Columns.Add("expensetypeid", System.Type.GetType("System.Int32")).DefaultValue = 0
            'S.SANDEEP |10-MAR-2022| -- END

            'S.SANDEEP |13-APR-2022| -- START
            'ISSUE/ENHANCEMENT : AC2-143
            dtList.Columns.Add("fromrouteid", System.Type.GetType("System.Int32")).DefaultValue = 0
            dtList.Columns.Add("torouteid", System.Type.GetType("System.Int32")).DefaultValue = 0
            dtList.Columns.Add("tdate", System.Type.GetType("System.String")).DefaultValue = ""
            'S.SANDEEP |13-APR-2022| -- END


            'S.SANDEEP |29-JUN-2022| -- START
            'ISSUE/ENHANCEMENT : AC2-390_V2
            dtList.Columns.Add("deductexpenseunkid", System.Type.GetType("System.Int32")).DefaultValue = 0
            'S.SANDEEP |29-JUN-2022| -- END


            'Pinkal (20-May-2022) -- Start
            'Optimize Global Claim Request for NMB.
            dtList.Columns.Add("referenceunkid", System.Type.GetType("System.Int32")).DefaultValue = 0
            dtList.Columns.Add("modulerefunkid", System.Type.GetType("System.Int32")).DefaultValue = 0
            dtList.Columns.Add("frommoduleid", System.Type.GetType("System.Int32")).DefaultValue = 0
            'Pinkal (20-May-2022) -- End


            If dsTran.Tables(0).Rows.Count > 0 Then
                Dim dtRow As DataRow = Nothing
                Dim dtFilter As DataTable = Nothing
                Dim mstrClaimNo As String = ""
                For Each dsRow As DataRow In dsTran.Tables("List").Rows

                    If mstrClaimNo.Trim <> dsRow.Item("ClaimNo").ToString().Trim Then
                        dtFilter = New DataView(dsTran.Tables("List"), "crmasterunkid = '" & dsRow.Item("crmasterunkid") & "'", "", DataViewRowState.CurrentRows).ToTable

                        If dtFilter.Rows.Count > 0 Then
                            dtRow = dtList.NewRow

                            'Pinkal (13-Aug-2020) -- Start
                            'Bug NMB:  Working on IIS Freezing and Dump Issue for NMB.
                            'dtRow.Item("ClaimNo") = Language.getMessage(mstrModuleName, 3, "Claim No : ") & dsRow.Item("ClaimNo").ToString()

                            'S.SANDEEP |10-MAR-2022| -- START
                            'ISSUE/ENHANCEMENT : OLD-580
                            'dtRow.Item("ClaimNo") = Language.getMessage(mstrModuleName, 5, "Claim No : ") & dsRow.Item("ClaimNo").ToString()
                            Select Case CInt(dsRow.Item("expensetypeid"))
                                'S.SANDEEP |27-MAY-2022| -- START
                                'ISSUE/ENHANCEMENT : AC2-389
                                'Case enExpenseType.EXP_REBATE_PRIVILEGE, enExpenseType.EXP_REBATE_DUTY
                                Case enExpenseType.EXP_REBATE_PRIVILEGE, enExpenseType.EXP_REBATE_DUTY, enExpenseType.EXP_REBATE_PRIVILEGE_DPNDT
                                    'S.SANDEEP |27-MAY-2022| -- END
                                    dtRow.Item("ClaimNo") = Language.getMessage(mstrModuleName, 5, "Claim No : ") & dsRow.Item("ClaimNo").ToString() & " | " & _
                                                            Language.getMessage(mstrModuleName, 900, "Rebate % : ") & dsRow.Item("rebate_percent").ToString() & " | " & _
                                                            Language.getMessage(mstrModuleName, 901, "Seat Status : ") & dsRow.Item("SeatType").ToString()
                                Case Else
                                    dtRow.Item("ClaimNo") = Language.getMessage(mstrModuleName, 5, "Claim No : ") & dsRow.Item("ClaimNo").ToString()
                            End Select
                            'S.SANDEEP |10-MAR-2022| -- END

                            'Pinkal (13-Aug-2020) -- End


                            dtRow.Item("CLNo") = dsRow.Item("ClaimNo").ToString()
                            dtRow.Item("IsGrp") = True
                            dtRow.Item("GrpId") = dsRow.Item("crmasterunkid")
                            dtRow.Item("employeeunkid") = dsRow.Item("employeeunkid")
                            dtRow.Item("CLNo") = dsRow.Item("ClaimNo").ToString()
                            'Pinkal (13-Jun-2016) -- Start
                            'Enhancement - Working on Global Approval Expense Changes Required by GDS (Annor).
                            dtRow.Item("Claim_Remark") = dsRow.Item("Claim_Remark").ToString()
                            'Pinkal (13-Jun-2016) -- End

                            dtRow.Item("crmasterunkid") = dsRow.Item("crmasterunkid").ToString()

                            'Pinkal (20-May-2022) -- Start
                            'Optimize Global Claim Request for NMB.
                            dtRow.Item("modulerefunkid") = dsRow.Item("modulerefunkid")
                            dtRow.Item("referenceunkid") = dsRow.Item("referenceunkid")
                            dtRow.Item("frommoduleid") = dsRow.Item("frommoduleid")
                            'Pinkal (20-May-2022) -- End

                            dtList.Rows.Add(dtRow)
                        End If
                        mstrClaimNo = dsRow.Item("ClaimNo").ToString().Trim

                        For Each dtlRow As DataRow In dtFilter.Rows
                            dtRow = dtList.NewRow
                            dtRow.Item("crmasterunkid") = CInt(dtlRow.Item("crmasterunkid"))
                            dtRow.Item("crtranunkid") = CInt(dtlRow.Item("crtranunkid"))
                            dtRow.Item("crapprovaltranunkid") = CInt(dtlRow.Item("crapprovaltranunkid"))
                            dtRow.Item("employeeunkid") = CInt(dtlRow.Item("employeeunkid"))
                            dtRow.Item("expenseunkid") = CInt(dtlRow.Item("expenseunkid"))
                            dtRow.Item("crapproverunkid") = CInt(dtlRow.Item("crapproverunkid"))
                            dtRow.Item("approveremployeeunkid") = CInt(dtlRow.Item("approveremployeeunkid"))

                            'Pinkal (22-Mar-2016) -- Start
                            'Enhancement - WORKING ON ENHANCEMENT ON CLAIM SUMMARY REPORT FOR KBC.
                            dtRow.Item("secrouteunkid") = CInt(dtlRow.Item("secrouteunkid"))
                            'Pinkal (22-Mar-2016) -- End

                            dtRow.Item("CLNo") = dsRow.Item("ClaimNo").ToString()
                            dtRow.Item("ClaimNo") = Space(5) & dtlRow.Item("Employee")
                            dtRow.Item("Employee") = dtlRow.Item("Employee").ToString()
                            dtRow.Item("Expense") = dtlRow.Item("Expense").ToString()
                            dtRow.Item("Sector") = dtlRow.Item("Sector").ToString()
                            dtRow.Item("UOM") = dtlRow.Item("UOM").ToString()
                            dtRow.Item("AppliedQty") = CDec(dtlRow.Item("AppliedQty"))
                            dtRow.Item("AppliedAmount") = dtlRow.Item("AppliedAmount")
                            dtRow.Item("Amount") = CDec(dtlRow.Item("Amount"))
                            dtRow.Item("quantity") = CDec(dtlRow.Item("quantity"))
                            dtRow.Item("unitprice") = CDec(dtlRow.Item("unitprice"))
                            dtRow.Item("amount") = CDec(dtlRow.Item("amount"))
                            dtRow.Item("Expense_Remark") = dtlRow.Item("Expense_Remark").ToString()

                            'Pinkal (13-Jun-2016) -- Start
                            'Enhancement - Working on Global Approval Expense Changes Required by GDS (Annor).
                            dtRow.Item("Claim_Remark") = ""
                            'Pinkal (13-Jun-2016) -- End


                            'Pinkal (04-Feb-2019) -- Start
                            'Enhancement - Working on Leave & Claim Enhancement Phase 2 for NMB.
                            dtRow.Item("costcenterunkid") = CInt(dtlRow.Item("costcenterunkid").ToString())
                            dtRow.Item("countryunkid") = CInt(dtlRow.Item("countryunkid").ToString())
                            dtRow.Item("base_countryunkid") = CInt(dtlRow.Item("base_countryunkid").ToString())
                            dtRow.Item("base_amount") = CDec(dtlRow.Item("base_amount").ToString())
                            dtRow.Item("exchangerateunkid") = CInt(dtlRow.Item("exchangerateunkid").ToString())
                            dtRow.Item("exchange_rate") = CDec(dtlRow.Item("exchange_rate").ToString())
                            'Pinkal (04-Feb-2019) -- End


                            dtRow.Item("isleaveencashment") = CBool(dtlRow.Item("isleaveencashment").ToString())
                            dtRow.Item("isaccrue") = CBool(dtlRow.Item("isaccrue").ToString())
                            dtRow.Item("IsGrp") = False
                            dtRow.Item("GrpId") = dsRow.Item("crmasterunkid")
                            dtRow.Item("leavetypeunkid") = CInt(dtlRow.Item("leavetypeunkid"))
                            dtRow.Item("uomunkid") = CInt(dtlRow.Item("uomunkid"))
                            dtRow.Item("AUD") = ""
                            dtRow.Item("voidloginemployeeunkid") = -1


                            'Pinkal (30-Apr-2018) - Start
                            'Enhancement  [Ref # 224] calculate/display the total balance & Balance As on date.
                            dtRow.Item("Claimdate") = CDate(dtlRow.Item("Claimdate"))
                            dtRow.Item("tdate") = eZeeDate.convertDate(CDate(dtlRow.Item("Claimdate")).Date).ToString()
                            'Pinkal (30-Apr-2018) - End

                            'S.SANDEEP |10-MAR-2022| -- START
                            'ISSUE/ENHANCEMENT : OLD-580
                            dtRow.Item("airline") = dtlRow.Item("airline").ToString()
                            dtRow.Item("travelfrom") = dtlRow.Item("travelfrom").ToString()
                            dtRow.Item("travelto") = dtlRow.Item("travelto").ToString()
                            dtRow.Item("flightno") = dtlRow.Item("flightno").ToString()
                            If IsDBNull(dtlRow.Item("traveldate")) = False Then
                                dtRow.Item("traveldate") = CDate(dtlRow.Item("traveldate"))
                            End If
                            dtRow.Item("other_person") = dtlRow.Item("other_person").ToString()
                            dtRow.Item("dpndtbeneficetranunkid") = dtlRow.Item("dpndtbeneficetranunkid").ToString()
                            dtRow.Item("name") = dtlRow.Item("name").ToString()
                            dtRow.Item("psgtype") = dtlRow.Item("psgtype").ToString()
                            dtRow.Item("relation") = dtlRow.Item("relation").ToString()
                            dtRow.Item("expensetypeid") = dtlRow.Item("expensetypeid").ToString()
                            'S.SANDEEP |10-MAR-2022| -- END

                            'S.SANDEEP |13-APR-2022| -- START
                            'ISSUE/ENHANCEMENT : AC2-143
                            dtRow.Item("fromrouteid") = dtlRow.Item("fromrouteid").ToString()
                            dtRow.Item("torouteid") = dtlRow.Item("torouteid").ToString()
                            'S.SANDEEP |13-APR-2022| -- END


                            'S.SANDEEP |29-JUN-2022| -- START
                            'ISSUE/ENHANCEMENT : AC2-390_V2
                            dtRow.Item("deductexpenseunkid") = dtlRow.Item("deductexpenseunkid")
                            'S.SANDEEP |29-JUN-2022| -- END


                            'Pinkal (20-May-2022) -- Start
                            'Optimize Global Claim Request for NMB.
                            dtRow.Item("modulerefunkid") = dtlRow.Item("modulerefunkid")
                            dtRow.Item("referenceunkid") = dtlRow.Item("referenceunkid")
                            dtRow.Item("frommoduleid") = dtlRow.Item("frommoduleid")
                            'Pinkal (20-May-2022) -- End

                            If mblnPaymentApprovalwithLeaveApproval = False AndAlso (CBool(dtRow.Item("isaccrue")) = False AndAlso CBool(dtRow.Item("isleaveencashment")) = False) OrElse (CBool(dtRow.Item("isaccrue")) = True AndAlso CBool(dtRow.Item("isleaveencashment")) = False) Then
                                Dim objEmpExpBal As New clsEmployeeExpenseBalance : Dim dsBal As New DataSet
                                'Pinkal (30-Apr-2018) - Start
                                'Enhancement  [Ref # 224] calculate/display the total balance & Balance As on date.

                                'S.SANDEEP |29-JUN-2022| -- START
                                'ISSUE/ENHANCEMENT : AC2-390_V2
                                'dsBal = objEmpExpBal.Get_Balance(CInt(dtRow.Item("employeeunkid")), CInt(dtRow.Item("expenseunkid")), xYearUnkid, CDate(dtRow("Claimdate")))
                                If CInt(dsRow.Item("expensetypeid")) = enExpenseType.EXP_REBATE_PRIVILEGE_DPNDT Then
                                    dsBal = objEmpExpBal.Get_Balance(CInt(dtRow.Item("employeeunkid")), CInt(dtRow.Item("deductexpenseunkid")), xYearUnkid, CDate(dtRow("Claimdate")))
                                Else
                                dsBal = objEmpExpBal.Get_Balance(CInt(dtRow.Item("employeeunkid")), CInt(dtRow.Item("expenseunkid")), xYearUnkid, CDate(dtRow("Claimdate")))
                                End If
                                'S.SANDEEP |29-JUN-2022| -- END


                                'Pinkal (30-Apr-2018) - End

                                If dsBal.Tables(0).Rows.Count > 0 Then
                                    dtRow.Item("Balance") = Convert.ToDecimal(dsBal.Tables(0).Rows(0).Item("bal"))
                                End If
                                dsBal = Nothing
                                objEmpExpBal = Nothing

                            ElseIf CBool(dtRow.Item("isaccrue")) = False AndAlso CBool(dtRow.Item("isleaveencashment")) = True And mblnPaymentApprovalwithLeaveApproval Then
                                Dim objLeave As New clsleavebalance_tran
                                Dim dsList As DataSet = Nothing
                                If xLeaveBalanceSetting = enLeaveBalanceSetting.Financial_Year Then

                                    'Pinkal (01-Mar-2016) -- Start
                                    'Enhancement - Implementing External Approver in Claim Request & Leave Module.
                                    'dsList = objLeave.GetList("List", xDatabaseName, xUserUnkid, xYearUnkid, xCompanyUnkid, eZeeDate.convertDate(xPeriodEnd), xUserModeSetting _
                                    ', True, xIncludeIn_ActiveEmployee, True, True, False, CInt(dtRow.Item("employeeunkid")), False, False, False, "lvleavebalance_tran.eavetypeunkid = " & CInt(dtRow.Item("leavetypeunkid")), Nothing)
                                    dsList = objLeave.GetList("List", xDatabaseName, xUserUnkid, xYearUnkid, xCompanyUnkid, eZeeDate.convertDate(xPeriodEnd), xUserModeSetting _
                                                                         , True, xIncludeIn_ActiveEmployee, True, True, False, CInt(dtRow.Item("employeeunkid")), False, False, False, "lvleavebalance_tran.leavetypeunkid = " & CInt(dtRow.Item("leavetypeunkid")), Nothing, IsExternalApprover)
                                    'Pinkal (01-Mar-2016) -- End



                                ElseIf xLeaveBalanceSetting = enLeaveBalanceSetting.ELC Then

                                    'Pinkal (01-Mar-2016) -- Start
                                    'Enhancement - Implementing External Approver in Claim Request & Leave Module.
                                    'dsList = objLeave.GetList("List", xDatabaseName, xUserUnkid, xYearUnkid, xCompanyUnkid, eZeeDate.convertDate(xPeriodEnd), xUserModeSetting _
                                    ', True, xIncludeIn_ActiveEmployee, True, True, False, CInt(dtRow.Item("employeeunkid")), True, True, False, "lvleavebalance_tran.leavetypeunkid = " & CInt(dtRow.Item("leavetypeunkid")), Nothing)
                                    dsList = objLeave.GetList("List", xDatabaseName, xUserUnkid, xYearUnkid, xCompanyUnkid, eZeeDate.convertDate(xPeriodEnd), xUserModeSetting _
                                                                         , True, xIncludeIn_ActiveEmployee, True, True, False, CInt(dtRow.Item("employeeunkid")), True, True, False, "lvleavebalance_tran.leavetypeunkid = " & CInt(dtRow.Item("leavetypeunkid")), Nothing, IsExternalApprover)
                                    'Pinkal (01-Mar-2016) -- End

                                End If

                                If dsList IsNot Nothing AndAlso dsList.Tables(0).Rows.Count > 0 Then
                                    dtRow.Item("Balance") = CDec(dsList.Tables(0).Rows(0)("accrue_amount")) - CDec(dsList.Tables(0).Rows(0)("issue_amount"))
                                End If
                                dsList = Nothing
                                objLeave = Nothing

                            End If
                            dtList.Rows.Add(dtRow)


                        Next

                    End If

                Next

            End If
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: GetGlobalApprovalData; Module Name: " & mstrModuleName)
        End Try
        Return dtList
    End Function


    'Pinkal (18-Feb-2016) -- End


    'Pinkal (01-Mar-2016) -- Start
    'Enhancement - Implementing External Approver in Claim Request & Leave Module.

    Public Function GetClaimExternalApproverList(ByVal mblnPaymentApprovalwithLeaveApproval As Boolean, ByVal intExpCategroryId As Integer _
                                                                        , Optional ByVal objDataOpr As clsDataOperation = Nothing, Optional ByVal strList As String = "List") As DataSet
        Dim objDataOperation As clsDataOperation
        Dim StrQ As String = ""
        Dim exForce As Exception
        Dim dsApprover As New DataSet

        If objDataOpr IsNot Nothing Then
            objDataOperation = objDataOpr
        Else
            objDataOperation = New clsDataOperation
        End If
        objDataOperation.ClearParameters()

        Try

            If strList.Trim.Length <= 0 Then strList = "List"

            If mblnPaymentApprovalwithLeaveApproval And intExpCategroryId = enExpenseType.EXP_LEAVE Then

                StrQ = "SELECT DISTINCT " & _
                                        "      ISNULL(cffinancial_year_tran.database_name,'') AS DName " & _
                                        "     ,ISNULL(cffinancial_year_tran.companyunkid,0) companyunkid " & _
                                        "     ,ISNULL(cffinancial_year_tran.yearunkid,0) AS yearunkid " & _
                                        "     ,ISNULL(EffDt.key_value,'') AS EDate " & _
                                        "     ,ISNULL(UC.key_value,'') AS ModeSet " & _
                                        "FROM cmclaim_approval_tran " & _
                                        " JOIN lvleaveapprover_master ON lvleaveapprover_master.leaveapproverunkid = cmclaim_approval_tran.approveremployeeunkid " & _
                                        " AND lvleaveapprover_master.approverunkid = cmclaim_approval_tran.crapproverunkid " & _
                                        " JOIN hrmsConfiguration..cfuser_master ON cfuser_master.userunkid = lvleaveapprover_master.leaveapproverunkid " & _
                                        " LEFT JOIN hrmsConfiguration..cffinancial_year_tran ON cffinancial_year_tran.companyunkid = cfuser_master.companyunkid AND cffinancial_year_tran.isclosed = 0 " & _
                                        " LEFT JOIN " & _
                                        " ( " & _
                                        "     SELECT " & _
                                        "          cfconfiguration.companyunkid " & _
                                        "         ,cfconfiguration.key_value " & _
                                        "     FROM hrmsConfiguration..cfconfiguration " & _
                                        "     WHERE UPPER(cfconfiguration.key_name) IN ('EMPLOYEEASONDATE') " & _
                                        " ) AS EffDt ON EffDt.companyunkid = cffinancial_year_tran.companyunkid " & _
                                        " LEFT JOIN " & _
                                        " ( " & _
                                        "     SELECT " & _
                                        "          cfconfiguration.companyunkid " & _
                                        "         ,cfconfiguration.key_value " & _
                                        "     FROM hrmsConfiguration..cfconfiguration " & _
                                        "     WHERE UPPER(cfconfiguration.key_name) IN ('USERACCESSMODESETTING') " & _
                                        " ) AS UC ON UC.companyunkid = cffinancial_year_tran.companyunkid " & _
                                        "WHERE cmclaim_approval_tran.isvoid = 0 AND lvleaveapprover_master.isexternalapprover = 1 "
            Else

                StrQ = " SELECT DISTINCT " & _
                           "     ISNULL(cffinancial_year_tran.database_name,'') AS DName " & _
                           "    ,ISNULL(cffinancial_year_tran.companyunkid,0) AS companyunkid " & _
                           "    ,ISNULL(cffinancial_year_tran.yearunkid,0) AS yearunkid " & _
                           "    ,ISNULL(EffDt.key_value,'') AS EDate " & _
                           "    ,ISNULL(UC.key_value,'') AS ModeSet " & _
                           " FROM cmexpapprover_master " & _
                           "    JOIN hrmsConfiguration..cfuser_master ON cfuser_master.userunkid = cmexpapprover_master.employeeunkid " & _
                           "    LEFT JOIN hrmsConfiguration..cffinancial_year_tran ON cffinancial_year_tran.companyunkid = cfuser_master.companyunkid AND cffinancial_year_tran.isclosed = 0 " & _
                           " LEFT JOIN " & _
                           " ( " & _
                           "    SELECT " & _
                           "         cfconfiguration.companyunkid " & _
                           "        ,cfconfiguration.key_value " & _
                           "    FROM hrmsConfiguration..cfconfiguration " & _
                           "    WHERE UPPER(cfconfiguration.key_name) IN ('EMPLOYEEASONDATE') " & _
                           " ) AS EffDt ON EffDt.companyunkid = cffinancial_year_tran.companyunkid " & _
                           " LEFT JOIN " & _
                           " ( " & _
                           "    SELECT " & _
                           "         cfconfiguration.companyunkid " & _
                           "        ,cfconfiguration.key_value " & _
                           "    FROM hrmsConfiguration..cfconfiguration " & _
                           "    WHERE UPPER(cfconfiguration.key_name) IN ('USERACCESSMODESETTING') " & _
                           " ) AS UC ON UC.companyunkid = cffinancial_year_tran.companyunkid " & _
                           " WHERE cmexpapprover_master.isvoid = 0 AND cmexpapprover_master.isswap = 0 " & _
                           " AND cmexpapprover_master.isexternalapprover = 1 "

            End If

            dsApprover = objDataOperation.ExecQuery(StrQ, strList)

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: GetClaimExternalApproverList; Module Name: " & mstrModuleName)
        Finally
            'Pinkal (25-Jan-2022) -- Start
            'Enhancement NMB  - Language Change in PM Module.	
            If objDataOpr Is Nothing Then objDataOperation = Nothing
            'Pinkal (25-Jan-2022) -- End
        End Try
        Return dsApprover
    End Function


    'Pinkal (01-Mar-2016) -- End



    'Pinkal (13-Jun-2016) -- Start
    'Enhancement - Working on Global Approval Expense Changes Required by GDS (Annor).

    Public Function GetClaimFormFromExpSector(ByVal iExpenseID As Integer, ByVal iSectorID As Integer) As String
        Dim mstrClaimFormID As String = "0"
        Dim exForce As Exception
        Dim strQ As String = ""
        Try

            Dim objDataOperation As New clsDataOperation
            objDataOperation.ClearParameters()

            If iExpenseID > 0 Then

                strQ = " SELECT ISNULL( STUFF(( select DISTINCT  + ',' + CAST(cmclaim_approval_tran.crmasterunkid AS NVARCHAR(50)) from cmclaim_approval_tran " & _
                           " JOIN cmclaim_request_master ON cmclaim_request_master.crmasterunkid = cmclaim_approval_tran.crmasterunkid " & _
                           " WHERE cmclaim_approval_tran.isvoid = 0 And cmclaim_request_master.isvoid = 0 AND cmclaim_approval_tran.statusunkid = 2 and cmclaim_approval_tran.visibleid = 2 " & _
                           " AND expenseunkid = @expenseId FOR XML PATH('')),1,1,''),'0') AS Ids "

                objDataOperation.AddParameter("@expenseId", SqlDbType.Int, eZeeDataType.INT_SIZE, iExpenseID)
                Dim dsList As DataSet = objDataOperation.ExecQuery(strQ, "List")

                If objDataOperation.ErrorMessage <> "" Then
                    exForce = New Exception(objDataOperation.ErrorNumber & " : " & objDataOperation.ErrorMessage)
                    Throw exForce
                End If

                If dsList.Tables(0).Rows.Count > 0 Then
                    mstrClaimFormID = dsList.Tables(0).Rows(0).Item("Ids").ToString.Trim
                End If

            End If

            If iSectorID > 0 Then

            End If
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: GetClaimFormFromExpSector; Module Name: " & mstrModuleName)
        End Try
        Return mstrClaimFormID
    End Function

    'Pinkal (13-Jun-2016) -- End

    'Hemant (22 Dec 2023) -- Start
    'ENHANCEMENT(NMB): A1X-1587 - Claim approval notification: Expense based final approved alerts
    Public Sub SendMailToExpensewiseUser(ByVal dtExpenseTran As DataTable _
                                         , Optional ByVal intCompanyUnkId As Integer = 0 _
                                         , Optional ByVal iLoginTypeId As Integer = 0 _
                                         , Optional ByVal iLoginEmployeeId As Integer = 0 _
                                         , Optional ByVal iUserId As Integer = 0 _
                                         , Optional ByVal mstrWebFrmName As String = "" _
                                         , Optional ByVal mblnSendEmailByThreading As Boolean = False)
        Try

            Dim strEmployeeName As String = dtExpenseTran.Rows(0).Item("employeecode") & " - " & dtExpenseTran.Rows(0).Item("employeename")
            Dim strCostAmount As String = ""

            If intCompanyUnkId <= 0 Then intCompanyUnkId = Company._Object._Companyunkid

            Dim mstrSubject As String = Language.getMessage(mstrModuleName, 10, "Expense Final Approval Notification")
            Dim strMessage As String = ""
            Dim objMail As New clsSendMail


            Dim dtExpense As DataTable = dtExpenseTran.DefaultView.ToTable(True, "expenseunkid")
            For Each drExpense As DataRow In dtExpense.Rows
                Dim objExpenseMaster As New clsExpense_Master
                objExpenseMaster._Expenseunkid = CInt(drExpense.Item("expenseunkid"))
                Dim decCostAmount As Decimal = dtExpenseTran.Compute("SUM(AMOUNT)", "expenseunkid = " & CInt(drExpense.Item("expenseunkid")) & "")
                If objExpenseMaster._NotifyApprovedUserIds.Trim = "" Then Continue For
                Dim strNotifiyUserIds As String = objExpenseMaster._NotifyApprovedUserIds
                Dim arrNotifyUserIds() As String = strNotifiyUserIds.Split(",")
                For Each strUserId As String In arrNotifyUserIds
                    Dim objUser As New clsUserAddEdit
                    objUser._Userunkid = CInt(strUserId)
                    If objUser._Email.Trim = "" Then Continue For
                    strMessage = "<HTML> <BODY>"

                    strMessage &= Language.getMessage(mstrModuleName, 11, "Dear") & " " & getTitleCase(objUser._Firstname & "  " & objUser._Lastname) & ", <BR><BR>"
                    strMessage &= " " & Language.getMessage(mstrModuleName, 12, "This is to inform you that <B>#EmployeeName#</B> has been Final Approved for cost <B>#ClaimAmount#</B> on <B>#ClaimExpense#</B>.")
                    strMessage = strMessage.Replace("#EmployeeName#", strEmployeeName)
                    strMessage = strMessage.Replace("#ClaimAmount#", Format(decCostAmount, GUI.fmtCurrency))
                    strMessage = strMessage.Replace("#ClaimExpense#", objExpenseMaster._Name)

                    strMessage &= "<BR><BR>" & Language.getMessage(mstrModuleName, 13, "Regards.") & "</BR>"

                    strMessage &= "<BR></BR><BR></BR><center>""POWERED BY ARUTI HR & PAYROLL MANAGEMENT SOFTWARE""</center>"
                    strMessage &= "</BODY></HTML>"

                    If mblnSendEmailByThreading Then

                        If LstClaimEmailList Is Nothing Then
                            LstClaimEmailList = New List(Of clsEmailCollection)
                        End If

                        Dim objEmailColl As New clsEmailCollection(objUser._Email, mstrSubject, strMessage, mstrWebFormName, _
                                                                       iLoginEmployeeId, mstrWebClientIP, mstrWebHostName, _
                                                                       iUserId, iLoginTypeId, clsSendMail.enAT_VIEW_TYPE.CLAIMREQUEST_MGT, _
                                                                       IIf(objUser._Email = "", objUser._Firstname & "  " & objUser._Lastname, objUser._Email))

                        LstClaimEmailList.Add(objEmailColl)
                    Else

                        objMail._Subject = mstrSubject
                        objMail._Message = strMessage
                        objMail._ToEmail = objUser._Email.ToString()

                        If iLoginTypeId <= 0 Then iLoginTypeId = enLogin_Mode.DESKTOP
                        If mstrWebFrmName.Trim.Length > 0 Then
                            objMail._Form_Name = mstrWebFrmName
                            objMail._WebClientIP = mstrWebClientIP
                            objMail._WebHostName = mstrWebHostName
                        End If
                        objMail._LogEmployeeUnkid = iLoginEmployeeId
                        objMail._OperationModeId = iLoginTypeId
                        objMail._UserUnkid = iUserId
                        objMail._SenderAddress = IIf(objUser._Email = "", objUser._Firstname & "  " & objUser._Lastname, objUser._Email)
                        objMail._ModuleRefId = clsSendMail.enAT_VIEW_TYPE.CLAIMREQUEST_MGT
                        objMail.SendMail(intCompanyUnkId)
                    End If

                    objUser = Nothing
                Next
                objExpenseMaster = Nothing
            Next



        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: SendMailToExpensewiseUser; Module Name: " & mstrModuleName)
        End Try
    End Sub
    'Hemant (22 Dec 2023) -- End



#End Region

    '<Language> This Auto Generated Text Please Do Not Modify it.
#Region " Language & UI Settings "
    Public Shared Sub SetMessages()
        Try
            Language.setMessage("clsclaim_request_tran", 100, "Dependant")
            Language.setMessage("clsclaim_request_tran", 101, "Other")
            Language.setMessage("clsclaim_request_tran", 102, "Self")
            Language.setMessage("clsExpCommonMethods", 2, "Leave")
            Language.setMessage("clsExpCommonMethods", 3, "Medical")
            Language.setMessage("clsExpCommonMethods", 4, "Training")
            Language.setMessage("clsExpCommonMethods", 6, "Quantity")
            Language.setMessage("clsExpCommonMethods", 7, "Amount")
            Language.setMessage("clsExpCommonMethods", 8, "Miscellaneous")
            Language.setMessage("clsExpCommonMethods", 9, "Imprest")
            Language.setMessage("clsExpCommonMethods", 10, "Rebate Privilege")
            Language.setMessage("clsExpCommonMethods", 11, "Rebate Duty")
            Language.setMessage("clsExpCommonMethods", 18, "S/A")
            Language.setMessage("clsExpCommonMethods", 19, "FIRM")
            Language.setMessage("clsMasterData", 110, "Approved")
            Language.setMessage("clsMasterData", 111, "Pending")
            Language.setMessage("clsMasterData", 112, "Rejected")
            Language.setMessage("clsMasterData", 113, "Re-Scheduled")
            Language.setMessage("clsMasterData", 115, "Cancelled")
            Language.setMessage("clsMasterData", 277, "Issued")
            Language.setMessage(mstrModuleName, 1, "Notification for approving Claim Application form")
            Language.setMessage(mstrModuleName, 2, "Dear")
            Language.setMessage(mstrModuleName, 3, "This is the notification for approving claim application no")
            Language.setMessage(mstrModuleName, 4, " of")
            Language.setMessage(mstrModuleName, 5, "Claim No :")
            Language.setMessage(mstrModuleName, 6, "Approved By :")
            Language.setMessage(mstrModuleName, 7, "Rejected By :")
            Language.setMessage(mstrModuleName, 8, "Please click on the following link to approve claim form.")
			Language.setMessage(mstrModuleName, 9, " with the type of expense :")
			Language.setMessage(mstrModuleName, 10, "Expense Final Approval Notification")
			Language.setMessage(mstrModuleName, 11, "Dear")
            Language.setMessage(mstrModuleName, 12, "This is to inform you that <B>#EmployeeName#</B> has been Final Approved for cost <B>#ClaimAmount#</B> on <B>#ClaimExpense#</B>.")
            Language.setMessage(mstrModuleName, 13, "Regards.")
            Language.setMessage(mstrModuleName, 900, "Rebate % :")
            Language.setMessage(mstrModuleName, 901, "Seat Status :")

        Catch Ex As Exception
            DisplayError.Show("-1", Ex.Message, "SetMessages", mstrModuleName)
        End Try
    End Sub
#End Region 'Language & UI Settings
    '</Language>
End Class
