﻿'************************************************************************************************************************************
'Class Name : clsexpadjustment_Tran.vb
'Purpose    :
'Date       :9/2/2015
'Written By :Pinkal
'Modified   :
'************************************************************************************************************************************

Imports eZeeCommonLib
Imports eZee.Common.eZeeForm

''' <summary>
''' Purpose: 
''' Developer: Pinkal
''' </summary>
Public Class clsexpadjustment_Tran
    Private Const mstrModuleName = "clsexpadjustment_Tran"
    Dim objDataOperation As clsDataOperation
    Dim mstrMessage As String = ""

#Region " Private variables "
    Private mintCradjustmentunkid As Integer
    Private mintCrexpbalanceunkid As Integer
    Private mintEmployeeunkid As Integer
    Private mintExpenseunkid As Integer
    Private mdblAdjustment_Amt As Double
    Private mstrRemarks As String = String.Empty
    Private mblnIsopenelc As Boolean
    Private mblnIselc As Boolean
    Private mblnIsclose_Fy As Boolean
    Private mintUserunkid As Integer
    Private mblnIsvoid As Boolean
    Private mintVoiduserunkid As Integer
    Private mdtVoiddatetime As Date
    Private mstrVoidreason As String = String.Empty
    Private mstrWebFormName As String = ""
    Private mstrWebClientIP As String = ""
    Private mstrWebHostName As String = ""
    Private mblnIsWeb As Boolean = False
#End Region

#Region " Properties "


    'S.SANDEEP [28-May-2018] -- START 
    'ISSUE/ENHANCEMENT : {Audit Trails} 
    Private mstrFormName As String = String.Empty
    Public WriteOnly Property _FormName() As String
        Set(ByVal value As String)
            mstrFormName = value
        End Set
    End Property

    Private mstrClientIP As String = ""
    Public WriteOnly Property _ClientIP() As String
        Set(ByVal value As String)
            mstrClientIP = value
        End Set
    End Property

    Private mstrHostName As String = ""
    Public WriteOnly Property _HostName() As String
        Set(ByVal value As String)
            mstrHostName = value
        End Set
    End Property

    

    Private mintAuditUserId As Integer = 0
    Public WriteOnly Property _AuditUserId() As Integer
        Set(ByVal value As Integer)
            mintAuditUserId = value
        End Set
    End Property

    Private mintCompanyUnkid As Integer = 0
    Public WriteOnly Property _CompanyUnkid() As Integer
        Set(ByVal value As Integer)
            mintCompanyUnkid = value
        End Set
    End Property

    Private mdtAuditDate As DateTime = Now
    Public WriteOnly Property _AuditDate() As DateTime
        Set(ByVal value As DateTime)
            mdtAuditDate = value
        End Set
    End Property

    Private xDataOpr As clsDataOperation = Nothing
    Public Property _xDataOpr() As clsDataOperation
        Get
            Return xDataOpr
        End Get
        Set(ByVal value As clsDataOperation)
            xDataOpr = value
        End Set
    End Property

    Private mintLoginEmployeeunkid As Integer = 0
    Public Property _LoginEmployeeunkid() As Integer
        Get
            Return mintLoginEmployeeunkid
        End Get
        Set(ByVal value As Integer)
            mintLoginEmployeeunkid = value
        End Set
    End Property

    Private mintVoidlogingemployeeunkid As Integer = 0
    Public Property _Voidloginemployeeunkid() As Integer
        Get
            Return mintVoidlogingemployeeunkid
        End Get
        Set(ByVal value As Integer)
            mintVoidlogingemployeeunkid = value
        End Set
    End Property
    'S.SANDEEP [28-May-2018] -- END 

    ''' <summary>
    ''' Purpose: Get Message from Class 
    ''' Modify By: Pinkal
    ''' </summary>
    Public ReadOnly Property _Message() As String
        Get
            Return mstrMessage
        End Get
    End Property

    ''' <summary>
    ''' Purpose: Get or Set cradjustmentunkid
    ''' Modify By: Pinkal
    ''' </summary>
    Public Property _Cradjustmentunkid() As Integer
        Get
            Return mintCradjustmentunkid
        End Get
        Set(ByVal value As Integer)
            mintCradjustmentunkid = Value
            Call getData()
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set crexpbalanceunkid
    ''' Modify By: Pinkal
    ''' </summary>
    Public Property _Crexpbalanceunkid() As Integer
        Get
            Return mintCrexpbalanceunkid
        End Get
        Set(ByVal value As Integer)
            mintCrexpbalanceunkid = Value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set employeeunkid
    ''' Modify By: Pinkal
    ''' </summary>
    Public Property _Employeeunkid() As Integer
        Get
            Return mintEmployeeunkid
        End Get
        Set(ByVal value As Integer)
            mintEmployeeunkid = Value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set expenseunkid
    ''' Modify By: Pinkal
    ''' </summary>
    Public Property _Expenseunkid() As Integer
        Get
            Return mintExpenseunkid
        End Get
        Set(ByVal value As Integer)
            mintExpenseunkid = Value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set adjustment_amt
    ''' Modify By: Pinkal
    ''' </summary>
    Public Property _Adjustment_Amt() As Double
        Get
            Return mdblAdjustment_Amt
        End Get
        Set(ByVal value As Double)
            mdblAdjustment_Amt = Value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set remarks
    ''' Modify By: Pinkal
    ''' </summary>
    Public Property _Remarks() As String
        Get
            Return mstrRemarks
        End Get
        Set(ByVal value As String)
            mstrRemarks = Value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set isopenelc
    ''' Modify By: Pinkal
    ''' </summary>
    Public Property _Isopenelc() As Boolean
        Get
            Return mblnIsopenelc
        End Get
        Set(ByVal value As Boolean)
            mblnIsopenelc = Value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set iselc
    ''' Modify By: Pinkal
    ''' </summary>
    Public Property _Iselc() As Boolean
        Get
            Return mblnIselc
        End Get
        Set(ByVal value As Boolean)
            mblnIselc = Value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set isclose_fy
    ''' Modify By: Pinkal
    ''' </summary>
    Public Property _Isclose_Fy() As Boolean
        Get
            Return mblnIsclose_Fy
        End Get
        Set(ByVal value As Boolean)
            mblnIsclose_Fy = Value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set userunkid
    ''' Modify By: Pinkal
    ''' </summary>
    Public Property _Userunkid() As Integer
        Get
            Return mintUserunkid
        End Get
        Set(ByVal value As Integer)
            mintUserunkid = Value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set isvoid
    ''' Modify By: Pinkal
    ''' </summary>
    Public Property _Isvoid() As Boolean
        Get
            Return mblnIsvoid
        End Get
        Set(ByVal value As Boolean)
            mblnIsvoid = Value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set voiduserunkid
    ''' Modify By: Pinkal
    ''' </summary>
    Public Property _Voiduserunkid() As Integer
        Get
            Return mintVoiduserunkid
        End Get
        Set(ByVal value As Integer)
            mintVoiduserunkid = Value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set voiddatetime
    ''' Modify By: Pinkal
    ''' </summary>
    Public Property _Voiddatetime() As Date
        Get
            Return mdtVoiddatetime
        End Get
        Set(ByVal value As Date)
            mdtVoiddatetime = Value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set voidreason
    ''' Modify By: Pinkal
    ''' </summary>
    Public Property _Voidreason() As String
        Get
            Return mstrVoidreason
        End Get
        Set(ByVal value As String)
            mstrVoidreason = Value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set WebFormName
    ''' Modify By: Pinkal
    ''' </summary>
    Public Property _WebFormName() As String
        Get
            Return mstrWebFormName
        End Get
        Set(ByVal value As String)
            mstrWebFormName = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set WebClientIP
    ''' Modify By: Pinkal
    ''' </summary>
    Public Property _WebClientIP() As String
        Get
            Return mstrWebClientIP
        End Get
        Set(ByVal value As String)
            mstrWebClientIP = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set WebHostName
    ''' Modify By: Pinkal
    ''' </summary>
    Public Property _WebHostName() As String
        Get
            Return mstrWebHostName
        End Get
        Set(ByVal value As String)
            mstrWebHostName = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set IsWeb
    ''' Modify By: Pinkal
    ''' </summary>
    Public Property _IsWeb() As Boolean
        Get
            Return mblnIsWeb
        End Get
        Set(ByVal value As Boolean)
            mblnIsWeb = value
        End Set
    End Property

#End Region

    ''' <summary>
    ''' Modify By: Pinkal
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>
    Public Sub GetData()
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        Dim objDataOperation As New clsDataOperation

        Try
            strQ = "SELECT " & _
                      "  cradjustmentunkid " & _
                       ", crexpbalanceunkid " & _
                       ", employeeunkid " & _
                       ", expenseunkid " & _
                       ", adjustment_amt " & _
                       ", remarks " & _
                       ", isopenelc " & _
                       ", iselc " & _
                       ", isclose_fy " & _
                       ", userunkid " & _
                       ", isvoid " & _
                       ", voiduserunkid " & _
                       ", voiddatetime " & _
                       ", voidreason " & _
                      " FROM crexpadjustment_tran  " & _
                      " WHERE cradjustmentunkid = @cradjustmentunkid "


            objDataOperation.AddParameter("@cradjustmentunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintCradjustmentunkid.ToString)

            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            For Each dtRow As DataRow In dsList.Tables(0).Rows
                mintCradjustmentunkid = CInt(dtRow.Item("cradjustmentunkid"))
                mintCrexpbalanceunkid = CInt(dtRow.Item("crexpbalanceunkid"))
                mintEmployeeunkid = CInt(dtRow.Item("employeeunkid"))
                mintExpenseunkid = CInt(dtRow.Item("expenseunkid"))
                mdblAdjustment_Amt = CDbl(dtRow.Item("adjustment_amt"))
                mstrRemarks = dtRow.Item("remarks").ToString
                mblnIsopenelc = CBool(dtRow.Item("isopenelc"))
                mblnIselc = CBool(dtRow.Item("iselc"))
                mblnIsclose_Fy = CBool(dtRow.Item("isclose_fy"))
                mintUserunkid = CInt(dtRow.Item("userunkid"))
                mblnIsvoid = CBool(dtRow.Item("isvoid"))
                mintVoiduserunkid = CInt(dtRow.Item("voiduserunkid"))

                If IsDBNull(dtRow.Item("voiddatetime")) = False Then
                    mdtVoiddatetime = dtRow.Item("voiddatetime")
                End If

                mstrVoidreason = dtRow.Item("voidreason").ToString
                Exit For
            Next
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: GetData; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
        End Try
    End Sub

    ''' <summary>
    ''' Modify By: Pinkal
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>
    Public Function GetList(ByVal strTableName As String, Optional ByVal blnOnlyActive As Boolean = True) As DataSet
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        objDataOperation = New clsDataOperation

        Try
            strQ = " SELECT " & _
                        "  cradjustmentunkid " & _
                        ", crexpbalanceunkid " & _
                        ", employeeunkid " & _
                        ", expenseunkid " & _
                        ", adjustment_amt " & _
                        ", remarks " & _
                        ", isopenelc " & _
                        ", iselc " & _
                        ", isclose_fy " & _
                        ", userunkid " & _
                        ", isvoid " & _
                        ", voiduserunkid " & _
                        ", voiddatetime " & _
                        ", voidreason " & _
                        " FROM crexpadjustment_tran "

            If blnOnlyActive Then
                strQ &= " WHERE isvoid = 0 "
            End If

            dsList = objDataOperation.ExecQuery(strQ, strTableName)

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: GetList; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
        Return dsList
    End Function

    ''' <summary>
    ''' Modify By: Pinkal
    ''' </summary>
    ''' <returns>Boolean</returns>
    ''' <purpose> INSERT INTO Database Table (lvleaveadjustment_tran) </purpose>
    Public Function Insert() As Boolean
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        Dim objDataOperation As New clsDataOperation
        objDataOperation.BindTransaction()

        Try
            objDataOperation.ClearParameters()
            objDataOperation.AddParameter("@crexpbalanceunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintCrexpbalanceunkid.ToString)
            objDataOperation.AddParameter("@employeeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintEmployeeunkid.ToString)
            objDataOperation.AddParameter("@expenseunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintExpenseunkid.ToString)
            objDataOperation.AddParameter("@adjustment_amt", SqlDbType.Float, eZeeDataType.FLOAT_SIZE, mdblAdjustment_Amt.ToString)
            objDataOperation.AddParameter("@remarks", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrRemarks.ToString)
            objDataOperation.AddParameter("@isopenelc", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsopenelc.ToString)
            objDataOperation.AddParameter("@iselc", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIselc.ToString)
            objDataOperation.AddParameter("@isclose_fy", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsclose_Fy.ToString)
            objDataOperation.AddParameter("@userunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintUserunkid.ToString)
            objDataOperation.AddParameter("@isvoid", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsvoid.ToString)
            objDataOperation.AddParameter("@voiduserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintVoiduserunkid.ToString)
            objDataOperation.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, IIf(mdtVoiddatetime <> Nothing, mdtVoiddatetime, DBNull.Value))
            objDataOperation.AddParameter("@voidreason", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrVoidreason.ToString)


            strQ = "INSERT INTO crexpadjustment_tran ( " & _
                          "  crexpbalanceunkid " & _
                          ", employeeunkid " & _
                          ", expenseunkid " & _
                          ", adjustment_amt " & _
                          ", remarks " & _
                          ", isopenelc " & _
                          ", iselc " & _
                          ", isclose_fy " & _
                          ", userunkid " & _
                          ", isvoid " & _
                          ", voiduserunkid " & _
                          ", voiddatetime " & _
                          ", voidreason" & _
                        ") VALUES (" & _
                          "  @crexpbalanceunkid " & _
                          ", @employeeunkid " & _
                          ", @expenseunkid " & _
                          ", @adjustment_amt " & _
                          ", @remarks " & _
                          ", @isopenelc " & _
                          ", @iselc " & _
                          ", @isclose_fy " & _
                          ", @userunkid " & _
                          ", @isvoid " & _
                          ", @voiduserunkid " & _
                          ", @voiddatetime " & _
                          ", @voidreason" & _
                        "); SELECT @@identity"


            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            mintCradjustmentunkid = dsList.Tables(0).Rows(0).Item(0)

            If InsertAudiTrailForExpenseAdjustment(objDataOperation, 1) = False Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If


            Dim mdecTotaladjustment As Decimal = GetTotalAdjustment(mintCrexpbalanceunkid, mintEmployeeunkid, mintExpenseunkid, objDataOperation)

            strQ = " UPDATE cmexpbalance_tran set " & _
                      " accrue_amount = accrue_amount + @adjustment  " & _
                      ", remaining_bal = (accrue_amount + @adjustment ) - issue_amount  " & _
                      ", adjustment_amt = @totaladjustment   " & _
                      "  WHERE isvoid = 0 AND employeeunkid =@employeeunkid AND expenseunkid = @expenseunkid AND crexpbalanceunkid = @crexpbalanceunkid "


            objDataOperation.ClearParameters()
            objDataOperation.AddParameter("@employeeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintEmployeeunkid)
            objDataOperation.AddParameter("@expenseunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintExpenseunkid)
            objDataOperation.AddParameter("@crexpbalanceunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintCrexpbalanceunkid)
            objDataOperation.AddParameter("@adjustment", SqlDbType.Float, eZeeDataType.FLOAT_SIZE, mdblAdjustment_Amt)
            objDataOperation.AddParameter("@totaladjustment", SqlDbType.Float, eZeeDataType.FLOAT_SIZE, mdecTotaladjustment)

            objDataOperation.ExecNonQuery(strQ)

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            'S.SANDEEP [28-May-2018] -- START
            'ISSUE/ENHANCEMENT : {Audit Trails} 
            Dim objCommonATLog As New clsCommonATLog
            objCommonATLog._FormName = mstrFormName
            objCommonATLog._LoginEmployeeUnkid = mintLoginEmployeeunkid
            objCommonATLog._ClientIP = mstrClientIP
            objCommonATLog._HostName = mstrHostName
            objCommonATLog._FromWeb = mblnIsWeb
            objCommonATLog._AuditUserId = mintAuditUserId
            objCommonATLog._CompanyUnkid = mintCompanyUnkid
            objCommonATLog._AuditDate = mdtAuditDate
            'S.SANDEEP [28-May-2018] -- END
            If objCommonATLog.Insert_AtLog(objDataOperation, 2, "cmexpbalance_tran", "crexpbalanceunkid", mintCrexpbalanceunkid) = False Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            objDataOperation.ReleaseTransaction(True)
            Return True
        Catch ex As Exception
            objDataOperation.ReleaseTransaction(False)
            Throw New Exception(ex.Message & "; Procedure Name: Insert; Module Name: " & mstrModuleName)
            Return False
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
    End Function

    ''' <summary>
    ''' Modify By: Pinkal
    ''' </summary>
    ''' <returns>Boolean</returns>
    ''' <purpose> Update Database Table (lvleaveadjustment_tran) </purpose>
    Public Function Update() As Boolean

        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        objDataOperation = New clsDataOperation
        objDataOperation.BindTransaction()

        Try
            objDataOperation.ClearParameters()
            objDataOperation.AddParameter("@cradjustmentunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintCradjustmentunkid.ToString)
            objDataOperation.AddParameter("@crexpbalanceunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintCrexpbalanceunkid.ToString)
            objDataOperation.AddParameter("@employeeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintEmployeeunkid.ToString)
            objDataOperation.AddParameter("@expenseunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintExpenseunkid.ToString)
            objDataOperation.AddParameter("@adjustment_amt", SqlDbType.Float, eZeeDataType.FLOAT_SIZE, mdblAdjustment_Amt.ToString)
            objDataOperation.AddParameter("@remarks", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrRemarks.ToString)
            objDataOperation.AddParameter("@isopenelc", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsopenelc.ToString)
            objDataOperation.AddParameter("@iselc", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIselc.ToString)
            objDataOperation.AddParameter("@isclose_fy", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsclose_Fy.ToString)
            objDataOperation.AddParameter("@userunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintUserunkid.ToString)
            objDataOperation.AddParameter("@isvoid", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsvoid.ToString)
            objDataOperation.AddParameter("@voiduserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintVoiduserunkid.ToString)
            objDataOperation.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, IIf(mdtVoiddatetime <> Nothing, mdtVoiddatetime, DBNull.Value))
            objDataOperation.AddParameter("@voidreason", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrVoidreason.ToString)


            strQ = strQ = "UPDATE crexpadjustment_tran SET " & _
                                "  crexpbalanceunkid = @crexpbalanceunkid" & _
                                ", employeeunkid = @employeeunkid" & _
                                ", expenseunkid = @expenseunkid" & _
                                ", adjustment_amt = @adjustment_amt" & _
                                ", remarks = @remarks" & _
                                ", isopenelc = @isopenelc" & _
                                ", iselc = @iselc" & _
                                ", isclose_fy = @isclose_fy" & _
                                ", userunkid = @userunkid" & _
                                ", isvoid = @isvoid" & _
                                ", voiduserunkid = @voiduserunkid" & _
                                ", voiddatetime = @voiddatetime" & _
                                ", voidreason = @voidreason " & _
                                " WHERE cradjustmentunkid = @cradjustmentunkid "

            Call objDataOperation.ExecNonQuery(strQ)

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            objDataOperation.ReleaseTransaction(True)
            Return True
        Catch ex As Exception
            objDataOperation.ReleaseTransaction(False)
            Throw New Exception(ex.Message & "; Procedure Name: Update; Module Name: " & mstrModuleName)
            Return False
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
    End Function

    ''' <summary>
    ''' Modify By: Pinkal
    ''' </summary>
    ''' <returns>Boolean</returns>
    ''' <purpose> Delete Database Table (lvleaveadjustment_tran) </purpose>
    Public Function Delete(ByVal intUnkid As Long, Optional ByVal objDOperation As clsDataOperation = Nothing) As Boolean

        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        If objDOperation Is Nothing Then
            objDataOperation = New clsDataOperation
        Else
            objDataOperation = objDOperation
        End If

        Try

            strQ = "SELECT cradjustmentunkid FROM crexpadjustment_tran WHERE crexpbalanceunkid = @crexpbalanceunkid AND isvoid = 0 "
            objDataOperation.ClearParameters()
            objDataOperation.AddParameter("@crexpbalanceunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intUnkid)
            dsList = objDataOperation.ExecQuery(strQ, "List")


            If dsList IsNot Nothing AndAlso dsList.Tables(0).Rows.Count > 0 Then
                strQ = "Update crexpadjustment_tran SET isvoid = 1,voiddatetime = getdate(),voiduserunkid = @voiduserunkid,voidreason = @voidreason WHERE cradjustmentunkid  = @cradjustmentunkid AND crexpbalanceunkid = @crexpbalanceunkid "

                For Each dr As DataRow In dsList.Tables(0).Rows
                    objDataOperation.ClearParameters()
                    objDataOperation.AddParameter("@adjustmentunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, CInt(dr("cradjustmentunkid")))
                    objDataOperation.AddParameter("@leavebalanceunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intUnkid)
                    objDataOperation.AddParameter("@voiduserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintUserunkid)
                    objDataOperation.AddParameter("@voidreason", SqlDbType.NVarChar, eZeeDataType.DESC_SIZE, mstrVoidreason)
                    Call objDataOperation.ExecNonQuery(strQ)

                    If objDataOperation.ErrorMessage <> "" Then
                        exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                        Throw exForce
                    End If

                    _Cradjustmentunkid = CLng(dr("cradjustmentunkid"))

                    If InsertAudiTrailForExpenseAdjustment(objDataOperation, 3) = False Then
                        exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                        Throw exForce
                    End If

                Next

            End If

            Return True
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: Delete; Module Name: " & mstrModuleName)
            Return False
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            If objDOperation Is Nothing Then objDataOperation = Nothing
        End Try
    End Function

    ''' <summary>
    ''' Modify By: Pinkal
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>
    Public Function isUsed(ByVal intUnkid As Integer) As Boolean
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        objDataOperation = New clsDataOperation

        Try
            StrQ = "<Query>"

            objDataOperation.AddParameter("@adjustmentunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intUnkid)

            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            Return dsList.tables(0).rows.count > 0
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: isUsed; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
    End Function

    ''' <summary>
    ''' Modify By: Pinkal
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>
    Public Function isExist(ByVal strCode As String, ByVal strName As String, Optional ByVal intUnkid As Integer = -1) As Boolean
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        objDataOperation = New clsDataOperation

        Try
            strQ = "SELECT " & _
                        "  cradjustmentunkid " & _
                        ", crexpbalanceunkid " & _
                        ", employeeunkid " & _
                        ", expenseunkid " & _
                        ", adjustment_amt " & _
                        ", remarks " & _
                        ", isopenelc " & _
                        ", iselc " & _
                        ", isclose_fy " & _
                        ", userunkid " & _
                        ", isvoid " & _
                        ", voiduserunkid " & _
                        ", voiddatetime " & _
                        ", voidreason " & _
                       " FROM crexpadjustment_tran " & _
                       " WHERE isvoid  = 0 "

            If intUnkid > 0 Then
                strQ &= " AND cradjustmentunkid <> @cradjustmentunkid"
            End If

            objDataOperation.AddParameter("@cradjustmentunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intUnkid)

            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            Return dsList.tables(0).rows.count > 0
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: isExist; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
    End Function

    ''' <summary>
    ''' Modify By: Pinkal
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>
    Public Function GetTotalAdjustment(ByVal xExpensebalanceId As Integer, ByVal xEmployeeId As Integer _
                                                       , ByVal xExpenseId As Integer, Optional ByVal objDOperation As clsDataOperation = Nothing) As Decimal
        Dim mdecAdjustment As Decimal = 0
        Dim strQ As String = ""
        Try

            If objDOperation Is Nothing Then
                objDataOperation = New clsDataOperation
            Else
                objDataOperation = objDOperation
            End If

            strQ = " SELECT SUM(adjustment_amt) AS adjustment FROM crexpadjustment_tran " & _
                      " WHERE crexpbalanceunkid = @crexpbalanceunkid AND employeeunkid = @employeeunkid AND expenseunkid = @expenseunkid AND isvoid = 0"
            objDataOperation.ClearParameters()
            objDataOperation.AddParameter("@crexpbalanceunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, xExpensebalanceId)
            objDataOperation.AddParameter("@employeeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, xEmployeeId)
            objDataOperation.AddParameter("@expenseunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, xExpenseId)
            Dim dsList As DataSet = objDataOperation.ExecQuery(strQ, "List")

            If dsList IsNot Nothing AndAlso dsList.Tables(0).Rows.Count > 0 Then
                mdecAdjustment = CDec(dsList.Tables(0).Rows(0)("adjustment"))
            End If

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: GetTotalAdjustment; Module Name: " & mstrModuleName)
        End Try
        Return mdecAdjustment
    End Function

    ''' <summary>
    ''' Modify By: Pinkal
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>
    Public Function GetAdustmentRemarkOnStatementReport(ByVal intEmployeeId As Integer, ByVal strExpeseeIds As String, ByVal elvSetting As enLeaveBalanceSetting) As String
        Dim strAdjRemark As String = String.Empty
        Dim strQ As String = ""
        Try
            If strExpeseeIds.Trim.Length > 0 Then
                Using objDataOpr As New clsDataOperation
                    strQ = "SELECT " & _
                           "    remarks " & _
                           "FROM " & _
                           "( " & _
                           "    SELECT " & _
                           "         employeeunkid " & _
                           "        ,expenseunkid " & _
                           "        ,remarks " & _
                           "        ,ROW_NUMBER()OVER(PARTITION BY employeeunkid,expenseunkid ORDER BY cradjustmentunkid DESC) AS rno " & _
                           "    FROM crexpadjustment_tran " & _
                           "    WHERE employeeunkid = @EmpId AND expenseunkid IN (" & strExpeseeIds & ") "
                    If elvSetting = enLeaveBalanceSetting.ELC Then
                        strQ &= " AND isopenelc = 1 AND iselc = 1 "
                    End If
                    strQ &= " ) AS ladj WHERE ladj.rno = 1 "

                    objDataOpr.AddParameter("@EmpId", SqlDbType.Int, eZeeDataType.INT_SIZE, intEmployeeId)
                    Dim ds As New DataSet
                    ds = objDataOpr.ExecQuery(strQ, "List")

                    If objDataOpr.ErrorMessage <> "" Then
                        Throw New Exception(objDataOpr.ErrorNumber & ": " & objDataOpr.ErrorMessage)
                    End If

                    If ds.Tables(0).Rows.Count > 0 Then
                        strAdjRemark = ds.Tables(0).Rows(0)("remarks").ToString()
                    End If
                End Using
            End If
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: GetAdustmentRemarkOnStatementReport; Module Name: " & mstrModuleName)
        Finally
        End Try
        Return strAdjRemark
    End Function

    ''' <summary>
    ''' Modify By: Pinkal
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>
    Public Function InsertAudiTrailForExpenseAdjustment(ByVal objDataOperation As clsDataOperation, ByVal AuditType As Integer) As Boolean
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception
        Try

            strQ = "INSERT INTO atcrexpadjustment_tran ( " & _
                      "  cradjustmentunkid " & _
                      ", crexpbalanceunkid " & _
                      ", employeeunkid " & _
                      ", expenseunkid " & _
                      ", adjustment_amt " & _
                      ", remarks " & _
                      ", isopenelc " & _
                      ", iselc " & _
                      ", isclose_fy " & _
                      ", audittype " & _
                      ", audituserunkid " & _
                      ", auditdatetime " & _
                      ", ip" & _
                      ", machine_name " & _
                      ", form_name " & _
                      ", isweb " & _
                    ") VALUES (" & _
                      "  @cradjustmentunkid " & _
                      ", @crexpbalanceunkid " & _
                      ", @employeeunkid " & _
                      ", @expenseunkid " & _
                      ", @adjustment_amt " & _
                      ", @remarks " & _
                      ", @isopenelc " & _
                      ", @iselc " & _
                      ", @isclose_fy " & _
                      ", @audittype " & _
                      ", @audituserunkid " & _
                      ", GETDATE() " & _
                      ", @ip" & _
                      ", @machine_name " & _
                      ", @form_name " & _
                      ", @isweb " & _
                    "); SELECT @@identity"


            objDataOperation.ClearParameters()
            objDataOperation.AddParameter("@cradjustmentunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintCradjustmentunkid.ToString)
            objDataOperation.AddParameter("@crexpbalanceunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintCrexpbalanceunkid.ToString)
            objDataOperation.AddParameter("@employeeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintEmployeeunkid.ToString)
            objDataOperation.AddParameter("@expenseunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintExpenseunkid.ToString)
            objDataOperation.AddParameter("@adjustment_amt", SqlDbType.Float, eZeeDataType.FLOAT_SIZE, mdblAdjustment_Amt.ToString)
            objDataOperation.AddParameter("@remarks", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrRemarks.ToString)
            objDataOperation.AddParameter("@isopenelc", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsopenelc.ToString)
            objDataOperation.AddParameter("@iselc", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIselc.ToString)
            objDataOperation.AddParameter("@isclose_fy", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsclose_Fy.ToString)
            objDataOperation.AddParameter("@audittype", SqlDbType.Int, eZeeDataType.INT_SIZE, AuditType.ToString)
            objDataOperation.AddParameter("@audituserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintUserunkid)

            If mstrWebClientIP.ToString().Length <= 0 Then
                mstrWebClientIP = getIP()
            End If

            If mstrWebHostName.ToString().Length <= 0 Then
                mstrWebHostName = getHostName()
            End If

            objDataOperation.AddParameter("@ip", SqlDbType.NVarChar, eZeeDataType.IP_SIZE, mstrWebClientIP)
            objDataOperation.AddParameter("@machine_name", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrWebHostName)
            objDataOperation.AddParameter("@form_name", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrWebFormName)
            objDataOperation.AddParameter("@isweb", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsWeb)

            objDataOperation.ExecNonQuery(strQ)

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If
            Return True
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: InsertAudiTrailForExpenseAdjustment; Module Name: " & mstrModuleName)
            Return False
        End Try
    End Function

End Class