﻿'************************************************************************************************************************************
'Class Name : clsExpCommonMethods.vb
'Purpose    :
'Date       :04-Feb-2014
'Written By :Sandeep Sharma
'Modified   :
'************************************************************************************************************************************

Imports eZeeCommonLib
''' <summary>
''' Purpose: 
''' Developer: Sandeep Sharma
''' </summary>
Public Class clsExpCommonMethods
    Private Shared ReadOnly mstrModuleName As String = "clsExpCommonMethods"

    Public Shared Function Get_ExpenseTypes(Optional ByVal IncludeMedical As Boolean = False, Optional ByVal IncludeTraining As Boolean = False, _
                                            Optional ByVal iFlag As Boolean = False, _
                                            Optional ByVal iList As String = "List", Optional ByVal IncludeMiscellenous As Boolean = False, _
                                            Optional ByVal blnIncludeImprest As Boolean = False, _
                                            Optional ByVal blnIncludeRebatePrivilege As Boolean = True, _
                                            Optional ByVal blnIncludeRebateDuty As Boolean = True, _
                                            Optional ByVal blnIncludeRebatePrivilegeDpndt As Boolean = True) As DataSet
        'S.SANDEEP |27-MAY-2022| -- START {AC2-389} (blnIncludeRebatePrivilegeDpndt) -- END
        'S.SANDEEP |10-MAR-2022| -- START {OLD-580} (blnIncludeRebatePrivilage,blnIncludeRebateDuty)-- END
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        Dim objDataOperation As New clsDataOperation
        'S.SANDEEP |10-MAR-2022| -- START
        'ISSUE/ENHANCEMENT : OLD-580
        Dim strGrpName As String = String.Empty
        Dim objGrp As New clsGroup_Master
        objGrp._Groupunkid = 1
        strGrpName = objGrp._Groupname
        objGrp = Nothing
        'S.SANDEEP |10-MAR-2022| -- END

        Try

            If iFlag Then
                strQ = "SELECT 0 AS Id, @Select AS Name UNION "
            End If


            strQ &= "  SELECT '" & enExpenseType.EXP_LEAVE & "' AS Id , @EXP_LEAVE  AS Name "
            objDataOperation.AddParameter("@EXP_LEAVE", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 2, "Leave"))



            'SHANI (06 JUN 2015) -- Start
            'Enhancement : Changes in C & R module given by Mr.Andrew.
            If IncludeMiscellenous = True Then
                strQ &= " UNION SELECT '" & enExpenseType.EXP_MISCELLANEOUS & "' AS Id, @EXP_MISCELLANEOUS  AS Name "
                objDataOperation.AddParameter("@EXP_MISCELLANEOUS", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 8, "Miscellaneous"))
            End If
            'SHANI (06 JUN 2015) -- End 

            If IncludeMedical = True Then
                strQ &= " UNION SELECT '" & enExpenseType.EXP_MEDICAL & "' AS Id, @EXP_MEDICAL  AS Name "
                objDataOperation.AddParameter("@EXP_MEDICAL", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 3, "Medical"))
            End If

            'S.SANDEEP [09-OCT-2018] -- START
            'Uncommented the Commented Part
            If IncludeTraining = True Then
                strQ &= " UNION SELECT '" & enExpenseType.EXP_TRAINING & "' AS Id, @EXP_TRAINING  AS Name "
                objDataOperation.AddParameter("@EXP_TRAINING", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 4, "Training"))
            End If
            'S.SANDEEP [09-OCT-2018] -- END


            'Pinkal (11-Sep-2019) -- Start
            'Enhancement NMB - Working On Claim Retirement for NMB.
            If blnIncludeImprest = True Then
                strQ &= " UNION SELECT '" & enExpenseType.EXP_IMPREST & "' AS Id, @EXP_IMPREST  AS Name "
                objDataOperation.AddParameter("@EXP_IMPREST", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 9, "Imprest"))
            End If
            'Pinkal (11-Sep-2019) -- End


            'S.SANDEEP |10-MAR-2022| -- START
            'ISSUE/ENHANCEMENT : OLD-580
            If strGrpName.ToUpper() = "PW" Then
                If blnIncludeRebatePrivilege Then
                    strQ &= " UNION SELECT '" & enExpenseType.EXP_REBATE_PRIVILEGE & "' AS Id, @EXP_REBATE_PRIVILEGE  AS Name "
                    objDataOperation.AddParameter("@EXP_REBATE_PRIVILEGE", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 10, "Rebate Privilege"))
                End If

                If blnIncludeRebateDuty Then
                    strQ &= " UNION SELECT '" & enExpenseType.EXP_REBATE_DUTY & "' AS Id, @EXP_REBATE_DUTY  AS Name "
                    objDataOperation.AddParameter("@EXP_REBATE_DUTY", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 11, "Rebate Duty"))
                End If

                'S.SANDEEP |27-MAY-2022| -- START
                'ISSUE/ENHANCEMENT : AC2-389
                If blnIncludeRebatePrivilegeDpndt Then
                    strQ &= " UNION SELECT '" & enExpenseType.EXP_REBATE_PRIVILEGE_DPNDT & "' AS Id, @EXP_REBATE_PRIVILEGE_DPNDT  AS Name "
                    objDataOperation.AddParameter("@EXP_REBATE_PRIVILEGE_DPNDT", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 13, "Rebate Privilege Dependant"))
                End If
                'S.SANDEEP |27-MAY-2022| -- END

            End If
            'S.SANDEEP |10-MAR-2022| -- END


            objDataOperation.AddParameter("@Select", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 1, "Select"))


            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            Return dsList

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: Get_ExpenseTypes; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
    End Function

    Public Shared Function Get_UoM(Optional ByVal iFlag As Boolean = False, Optional ByVal iList As String = "List") As DataSet
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        Dim objDataOperation As New clsDataOperation
        Try

            If iFlag Then
                strQ = "SELECT 0 AS Id, @Select AS Name UNION "
            End If
            strQ &= " SELECT '" & enExpUoM.UOM_QTY & "', @UOM_QTY  AS Name UNION " & _
                    " SELECT '" & enExpUoM.UOM_AMOUNT & "', @UOM_AMOUNT  AS Name  "

            objDataOperation.AddParameter("@Select", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 5, "Select"))
            objDataOperation.AddParameter("@UOM_QTY", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 6, "Quantity"))
            objDataOperation.AddParameter("@UOM_AMOUNT", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 7, "Amount"))

            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            Return dsList

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: Get_UoM; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
    End Function

    'S.SANDEEP |10-MAR-2022| -- START
    'ISSUE/ENHANCEMENT : OLD-580
    Public Shared Function GetRebatePercentage(Optional ByVal blnAddSelect As Boolean = False, Optional ByVal strList As String = "List") As DataSet
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception
        Dim objDataOperation As New clsDataOperation
        Try
            'S.SANDEEP |29-MAR-2022| -- START
            'ISSUE/ENHANCEMENT : OLD-580-V2
            'strQ &= "SELECT Id, Name FROM ( "
            'If blnAddSelect Then
            '    strQ &= "SELECT -999 AS Id, @Select AS Name UNION "
            'End If
            'strQ &= "SELECT " & enRebatePercent.RB_100 & " AS Id, '100' AS Name UNION " & _
            '        "SELECT " & enRebatePercent.RB_75 & " AS Id, '75' AS Name UNION " & _
            '        "SELECT " & enRebatePercent.RB_50 & " AS Id, '50' AS Name UNION " & _
            '        "SELECT " & enRebatePercent.RB_25 & " AS Id, '25' AS Name "

            'strQ &= " ) AS A ORDER BY Id ASC "

            strQ &= "SELECT Id, Name,SortId FROM ( "
            If blnAddSelect Then
                strQ &= "SELECT -999 AS Id, @Select AS Name, 0 AS SortId UNION "
            End If
            strQ &= "SELECT " & enRebatePercent.RB_100 & " AS Id, '100' AS Name, 1 AS SortId UNION " & _
                    "SELECT " & enRebatePercent.RB_90 & " AS Id, '90' AS Name, 2 AS SortId UNION " & _
                    "SELECT " & enRebatePercent.RB_75 & " AS Id, '75' AS Name, 3 AS SortId UNION " & _
                    "SELECT " & enRebatePercent.RB_50 & " AS Id, '50' AS Name, 4 AS SortId UNION " & _
                    "SELECT " & enRebatePercent.RB_25 & " AS Id, '25' AS Name, 5 AS SortId "

            strQ &= " ) AS A ORDER BY SortId ASC "
            'S.SANDEEP |29-MAR-2022| -- END
            

            objDataOperation.AddParameter("@Select", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 12, "Select"))
            'objDataOperation.AddParameter("@RB_100", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 13, "100"))
            'objDataOperation.AddParameter("@RB_75", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 14, "75"))
            'objDataOperation.AddParameter("@RB_50", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 15, "50"))
            'objDataOperation.AddParameter("@RB_25", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 16, "25"))

            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            Return dsList
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: GetRebatePercentage; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
    End Function

    Public Shared Function GetRebateSeatStatus(Optional ByVal blnAddSelect As Boolean = False, Optional ByVal strList As String = "List") As DataSet
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception
        Dim objDataOperation As New clsDataOperation
        Try
            strQ &= "SELECT Id, Name FROM ( "
            If blnAddSelect Then
                strQ &= "SELECT 0 AS Id, @Select AS Name UNION "
            End If
            strQ &= "SELECT " & enRebateSeat.RB_SA & " AS Id, @SA AS Name UNION " & _
                    "SELECT " & enRebateSeat.RB_FIRM & " AS Id, @FM AS Name "

            strQ &= " ) AS A ORDER BY Id ASC "

            objDataOperation.AddParameter("@Select", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 17, "Select"))
            objDataOperation.AddParameter("@SA", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 18, "S/A"))
            objDataOperation.AddParameter("@FM", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 19, "FIRM"))

            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            Return dsList
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: GetRebateSeatStatus; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
    End Function
    'S.SANDEEP |10-MAR-2022| -- END

    'S.SANDEEP |27-MAY-2022| -- START
    'ISSUE/ENHANCEMENT : AC2-392
    Public Shared Function Get_ExpenseCategoryName(ByVal intCategoryId As Integer) As String
        Dim StrExpCatName As String = ""
        Try
            Dim dsLst As New DataSet
            dsLst = Get_ExpenseTypes(True, True, False, "List", True, True, True, True, True)
            Dim itmp As DataRow()
            itmp = dsLst.Tables(0).Select("Id = '" & intCategoryId.ToString() & "'")
            If itmp IsNot Nothing Then
                StrExpCatName = itmp(0)("Name").ToString()
            End If
            If StrExpCatName Is Nothing Then StrExpCatName = ""
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: Get_ExpenseCategoryName; Module Name: " & mstrModuleName)
        End Try
        Return StrExpCatName
    End Function
    'S.SANDEEP |27-MAY-2022| -- END

	'<Language> This Auto Generated Text Please Do Not Modify it.
#Region " Language & UI Settings "
	Public Shared Sub SetMessages()
		Try
			Language.setMessage(mstrModuleName, 1, "Select")
			Language.setMessage(mstrModuleName, 2, "Leave")
			Language.setMessage(mstrModuleName, 3, "Medical")
			Language.setMessage(mstrModuleName, 4, "Training")
			Language.setMessage(mstrModuleName, 5, "Select")
			Language.setMessage(mstrModuleName, 6, "Quantity")
			Language.setMessage(mstrModuleName, 7, "Amount")
            Language.setMessage(mstrModuleName, 8, "Miscellaneous")
			Language.setMessage(mstrModuleName, 9, "Imprest")
            Language.setMessage(mstrModuleName, 10, "Rebate Privilege")
            Language.setMessage(mstrModuleName, 11, "Rebate Duty")
            Language.setMessage(mstrModuleName, 12, "Select")
            Language.setMessage(mstrModuleName, 13, "Rebate Privilege Dependant")
            Language.setMessage(mstrModuleName, 17, "Select")
            Language.setMessage(mstrModuleName, 18, "S/A")
            Language.setMessage(mstrModuleName, 19, "FIRM")

		Catch Ex As Exception
			DisplayError.Show("-1", ex.Message, "SetMessages", mstrModuleName)
		End Try
	End Sub
#End Region 'Language & UI Settings
	'</Language>
End Class
