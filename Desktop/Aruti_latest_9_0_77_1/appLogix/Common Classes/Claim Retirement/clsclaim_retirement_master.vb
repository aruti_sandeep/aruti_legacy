﻿'************************************************************************************************************************************
'Class Name : clsclaim_retirement_master.vb
'Purpose    :
'Date       :23-Sep-2019
'Written By :Pinkal
'Modified   :
'************************************************************************************************************************************

Imports eZeeCommonLib
Imports eZee.Common.eZeeForm
Imports Aruti.Data


''' <summary>
''' Purpose: 
''' Developer: Pinkal
''' </summary>
Public Class clsclaim_retirement_master
    Private Shared ReadOnly mstrModuleName As String = "clsclaim_retirement_master"
    Dim objDataOperation As clsDataOperation
    Dim mstrMessage As String = ""

#Region " Private variables "
    Private mintClaimretirementunkid As Integer
    Private mstrClaimRetirementNo As String = ""
    Private mintCrmasterunkid As Integer
    Private mintExpensetypeunkid As Integer
    Private mintEmployeeunkid As Integer
    Private mdtTransactiondate As Date
    Private mstrRetirement_Remark As String = String.Empty
    Private mdecClaim_amount As Decimal
    Private mdecImprest_Amount As Decimal
    Private mdecBalance As Decimal
    Private mintApproverunkid As Integer
    Private mintApproveremployeeunkid As Integer
    Private mintStatusunkid As Integer = 0
    Private mstrP2prequisitionid As String = String.Empty
    Private mstrP2pmessage As String = String.Empty
    Private mstrP2pstatuscode As String = String.Empty
    Private mdtP2ptimestamp As Date
    Private mstrP2pposteddata As String = ""
    Private mintUserunkid As Integer
    Private mintLoginemployeeunkid As Integer
    Private mblnIscancel As Boolean
    Private mintCanceluserunkid As Integer
    Private mstrCancel_Remark As String = String.Empty
    Private mdtCancel_Datetime As Date
    Private mblnIsvoid As Boolean
    Private mintVoiduserunkid As Integer
    Private mdtVoiddatetime As Date
    Private mstrVoidreason As String = String.Empty
    Private mintVoidloginemployeeunkid As Integer
    Private mstrWebClientIP As String = ""
    Private mstrWebHostName As String = ""
    Private mstrFormName As String = ""
    Private mblnIsWeb As Boolean = False
    Private mintLoginMode As enLogin_Mode


    'Pinkal (28-Apr-2020) -- Start
    'Enhancement Claim Retirement  - Claim Retirement Enhancement given by NMB .
    Private mblnIssubmit_Approval As Boolean = False
    'Pinkal (28-Apr-2020) -- End


    Dim objCRetirementTran As New clsclaim_retirement_Tran
    Dim objCRetirementApprovalTran As New clsclaim_retirement_approval_Tran

#End Region

#Region " Properties "

    ''' <summary>
    ''' Purpose: Get Message from Class 
    ''' Modify By: Pinkal
    ''' </summary>
    Public ReadOnly Property _Message() As String
        Get
            Return mstrMessage
        End Get
    End Property

    ''' <summary>
    ''' Purpose: Get or Set claimretirementunkid
    ''' Modify By: Pinkal
    ''' </summary>
    Public Property _Claimretirementunkid(Optional ByVal objDoOperation As clsDataOperation = Nothing) As Integer
        Get
            Return mintClaimretirementunkid
        End Get
        Set(ByVal value As Integer)
            mintClaimretirementunkid = value
            Call GetData(objDoOperation)
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set ClaimRetirementNo
    ''' Modify By: Pinkal
    ''' </summary>
    Public Property _ClaimRetirementNo() As String
        Get
            Return mstrClaimRetirementNo
        End Get
        Set(ByVal value As String)
            mstrClaimRetirementNo = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set crmasterunkid
    ''' Modify By: Pinkal
    ''' </summary>
    Public Property _Crmasterunkid() As Integer
        Get
            Return mintCrmasterunkid
        End Get
        Set(ByVal value As Integer)
            mintCrmasterunkid = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set expensetypeunkid
    ''' Modify By: Pinkal
    ''' </summary>
    Public Property _Expensetypeunkid() As Integer
        Get
            Return mintExpensetypeunkid
        End Get
        Set(ByVal value As Integer)
            mintExpensetypeunkid = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set employeeunkid
    ''' Modify By: Pinkal
    ''' </summary>
    Public Property _Employeeunkid() As Integer
        Get
            Return mintEmployeeunkid
        End Get
        Set(ByVal value As Integer)
            mintEmployeeunkid = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set transactiondate
    ''' Modify By: Pinkal
    ''' </summary>
    Public Property _Transactiondate() As Date
        Get
            Return mdtTransactiondate
        End Get
        Set(ByVal value As Date)
            mdtTransactiondate = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set retirement_remark
    ''' Modify By: Pinkal
    ''' </summary>
    Public Property _Retirement_Remark() As String
        Get
            Return mstrRetirement_Remark
        End Get
        Set(ByVal value As String)
            mstrRetirement_Remark = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set Claim_Amount
    ''' Modify By: Pinkal
    ''' </summary>
    Public Property _Claim_Amount() As Decimal
        Get
            Return mdecClaim_amount
        End Get
        Set(ByVal value As Decimal)
            mdecClaim_amount = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set imprest_amount
    ''' Modify By: Pinkal
    ''' </summary>
    Public Property _Imprest_Amount() As Decimal
        Get
            Return mdecImprest_Amount
        End Get
        Set(ByVal value As Decimal)
            mdecImprest_Amount = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set balance
    ''' Modify By: Pinkal
    ''' </summary>
    Public Property _Balance() As Decimal
        Get
            Return mdecBalance
        End Get
        Set(ByVal value As Decimal)
            mdecBalance = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set approverunkid
    ''' Modify By: Pinkal
    ''' </summary>
    Public Property _Approverunkid() As Integer
        Get
            Return mintApproverunkid
        End Get
        Set(ByVal value As Integer)
            mintApproverunkid = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set approveremployeeunkid
    ''' Modify By: Pinkal
    ''' </summary>
    Public Property _Approveremployeeunkid() As Integer
        Get
            Return mintApproveremployeeunkid
        End Get
        Set(ByVal value As Integer)
            mintApproveremployeeunkid = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set statusunkid
    ''' Modify By: Pinkal
    ''' </summary>
    Public Property _statusunkid() As Integer
        Get
            Return mintStatusunkid
        End Get
        Set(ByVal value As Integer)
            mintStatusunkid = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set p2prequisitionid
    ''' Modify By: Pinkal
    ''' </summary>
    Public Property _P2prequisitionid() As String
        Get
            Return mstrP2prequisitionid
        End Get
        Set(ByVal value As String)
            mstrP2prequisitionid = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set p2pmessage
    ''' Modify By: Pinkal
    ''' </summary>
    Public Property _P2pmessage() As String
        Get
            Return mstrP2pmessage
        End Get
        Set(ByVal value As String)
            mstrP2pmessage = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set p2pstatuscode
    ''' Modify By: Pinkal
    ''' </summary>
    Public Property _P2pstatuscode() As String
        Get
            Return mstrP2pstatuscode
        End Get
        Set(ByVal value As String)
            mstrP2pstatuscode = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set p2ptimestamp
    ''' Modify By: Pinkal
    ''' </summary>
    Public Property _P2ptimestamp() As Date
        Get
            Return mstrP2pposteddata
        End Get
        Set(ByVal value As Date)
            mstrP2pposteddata = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set p2pposteddata
    ''' Modify By: Pinkal
    ''' </summary>
    Public Property _P2pposteddata() As String
        Get
            Return mstrP2pposteddata
        End Get
        Set(ByVal value As String)
            mstrP2pposteddata = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set userunkid
    ''' Modify By: Pinkal
    ''' </summary>
    Public Property _Userunkid() As Integer
        Get
            Return mintUserunkid
        End Get
        Set(ByVal value As Integer)
            mintUserunkid = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set loginemployeeunkid
    ''' Modify By: Pinkal
    ''' </summary>
    Public Property _Loginemployeeunkid() As Integer
        Get
            Return mintLoginemployeeunkid
        End Get
        Set(ByVal value As Integer)
            mintLoginemployeeunkid = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set iscancel
    ''' Modify By: Pinkal
    ''' </summary>
    Public Property _Iscancel() As Boolean
        Get
            Return mblnIscancel
        End Get
        Set(ByVal value As Boolean)
            mblnIscancel = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set canceluserunkid
    ''' Modify By: Pinkal
    ''' </summary>
    Public Property _Canceluserunkid() As Integer
        Get
            Return mintCanceluserunkid
        End Get
        Set(ByVal value As Integer)
            mintCanceluserunkid = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set cancel_remark
    ''' Modify By: Pinkal
    ''' </summary>
    Public Property _Cancel_Remark() As String
        Get
            Return mstrCancel_Remark
        End Get
        Set(ByVal value As String)
            mstrCancel_Remark = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set cancel_datetime
    ''' Modify By: Pinkal
    ''' </summary>
    Public Property _Cancel_Datetime() As Date
        Get
            Return mdtCancel_Datetime
        End Get
        Set(ByVal value As Date)
            mdtCancel_Datetime = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set isvoid
    ''' Modify By: Pinkal
    ''' </summary>
    Public Property _Isvoid() As Boolean
        Get
            Return mblnIsvoid
        End Get
        Set(ByVal value As Boolean)
            mblnIsvoid = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set voiduserunkid
    ''' Modify By: Pinkal
    ''' </summary>
    Public Property _Voiduserunkid() As Integer
        Get
            Return mintVoiduserunkid
        End Get
        Set(ByVal value As Integer)
            mintVoiduserunkid = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set voiddatetime
    ''' Modify By: Pinkal
    ''' </summary>
    Public Property _Voiddatetime() As Date
        Get
            Return mdtVoiddatetime
        End Get
        Set(ByVal value As Date)
            mdtVoiddatetime = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set voidreason
    ''' Modify By: Pinkal
    ''' </summary>
    Public Property _Voidreason() As String
        Get
            Return mstrVoidreason
        End Get
        Set(ByVal value As String)
            mstrVoidreason = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set voidloginemployeeunkid
    ''' Modify By: Pinkal
    ''' </summary>
    Public Property _Voidloginemployeeunkid() As Integer
        Get
            Return mintVoidloginemployeeunkid
        End Get
        Set(ByVal value As Integer)
            mintVoidloginemployeeunkid = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set WebClientIP
    ''' Modify By: Pinkal
    ''' </summary>
    Public Property _WebClientIP() As String
        Get
            Return mstrWebClientIP
        End Get
        Set(ByVal value As String)
            mstrWebClientIP = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set WebHostName
    ''' Modify By: Pinkal
    ''' </summary>
    Public Property _WebHostName() As String
        Get
            Return mstrWebHostName
        End Get
        Set(ByVal value As String)
            mstrWebHostName = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set FormName
    ''' Modify By: Pinkal
    ''' </summary>
    Public Property _FormName() As String
        Get
            Return mstrFormName
        End Get
        Set(ByVal value As String)
            mstrFormName = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set IsWeb
    ''' Modify By: Pinkal
    ''' </summary>
    Public Property _IsWeb() As Boolean
        Get
            Return mblnIsWeb
        End Get
        Set(ByVal value As Boolean)
            mblnIsWeb = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set _LoginMode
    ''' Modify By: Pinkal
    ''' </summary>
    ''' 
    Public Property _LoginMode() As enLogin_Mode
        Get
            Return mintLoginMode
        End Get
        Set(ByVal value As enLogin_Mode)
            mintLoginMode = value
        End Set
    End Property


    'Pinkal (28-Apr-2020) -- Start
    'Enhancement Claim Retirement  - Claim Retirement Enhancement given by NMB .
    ''' <summary>
    ''' Purpose: Get or Set issubmit_approval
    ''' Modify By: Sohail
    ''' </summary>
    Public Property _Issubmit_Approval() As Boolean
        Get
            Return mblnIssubmit_Approval
        End Get
        Set(ByVal value As Boolean)
            mblnIssubmit_Approval = value
        End Set
    End Property
    'Pinkal (28-Apr-2020) -- End


#End Region

    ''' <summary>
    ''' Modify By: Pinkal
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>
    Public Sub GetData(Optional ByVal objDoOperation As clsDataOperation = Nothing)
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        If objDoOperation Is Nothing Then
            objDataOperation = New clsDataOperation
        Else
            objDataOperation = objDoOperation
        End If

        Try
            strQ = "SELECT " & _
                      "  claimretirementunkid " & _
                      ", claimretirementno " & _
                      ", crmasterunkid " & _
                      ", expensetypeunkid " & _
                      ", employeeunkid " & _
                      ", transactiondate " & _
                      ", retirement_remark " & _
                      ", claim_amount " & _
                      ", imprest_amount " & _
                      ", balance " & _
                      ", approverunkid " & _
                      ", approveremployeeunkid " & _
                      ", statusunkid " & _
                      ", p2prequisitionid " & _
                      ", p2pmessage " & _
                      ", p2pstatuscode " & _
                      ", p2ptimestamp " & _
                      ", p2pposteddata " & _
                      ", userunkid " & _
                      ", loginemployeeunkid " & _
                      ", iscancel " & _
                      ", canceluserunkid " & _
                      ", cancel_remark " & _
                      ", cancel_datetime " & _
                      ", ISNULL(issubmit_approval,0) AS issubmit_approval " & _
                      ", isvoid " & _
                      ", voiduserunkid " & _
                      ", voiddatetime " & _
                      ", voidreason " & _
                      ", voidloginemployeeunkid " & _
                      " FROM cmclaim_retirement_master " & _
                      " WHERE claimretirementunkid = @claimretirementunkid AND isvoid = 0"


            'Pinkal (28-Apr-2020) -- 'Enhancement Claim Retirement  - Claim Retirement Enhancement given by NMB .[ ", ISNULL(issubmit_approval,0) AS issubmit_approval " & _]


            objDataOperation.ClearParameters()
            objDataOperation.AddParameter("@claimretirementunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintClaimretirementunkid.ToString)
            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            For Each dtRow As DataRow In dsList.Tables(0).Rows
                mintClaimretirementunkid = CInt(dtRow.Item("claimretirementunkid"))
                mstrClaimRetirementNo = dtRow.Item("claimretirementno").ToString()
                mintCrmasterunkid = CInt(dtRow.Item("crmasterunkid"))
                mintExpensetypeunkid = CInt(dtRow.Item("expensetypeunkid"))
                mintEmployeeunkid = CInt(dtRow.Item("employeeunkid"))
                mdtTransactiondate = dtRow.Item("transactiondate")
                mstrRetirement_Remark = dtRow.Item("retirement_remark").ToString
                mdecClaim_amount = CDec(dtRow.Item("claim_amount"))
                mdecImprest_Amount = CDec(dtRow.Item("imprest_amount"))
                mdecBalance = CDec(dtRow.Item("balance"))
                mintApproverunkid = CInt(dtRow.Item("approverunkid"))
                mintApproveremployeeunkid = CInt(dtRow.Item("approveremployeeunkid"))
                mintStatusunkid = CInt(dtRow.Item("statusunkid"))
                mstrP2prequisitionid = dtRow.Item("p2prequisitionid").ToString
                mstrP2pmessage = dtRow.Item("p2pmessage").ToString
                mstrP2pstatuscode = dtRow.Item("p2pstatuscode").ToString

                If Not IsDBNull(dtRow.Item("p2ptimestamp")) Then
                    mdtP2ptimestamp = dtRow.Item("p2ptimestamp")
                End If


                If Not IsDBNull(dtRow.Item("p2pposteddata")) Then
                    mstrP2pposteddata = dtRow.Item("p2pposteddata").ToString()
                End If

                mintUserunkid = CInt(dtRow.Item("userunkid"))
                mintLoginemployeeunkid = CInt(dtRow.Item("loginemployeeunkid"))
                mblnIscancel = CBool(dtRow.Item("iscancel"))
                mintCanceluserunkid = CInt(dtRow.Item("canceluserunkid"))
                mstrCancel_Remark = dtRow.Item("cancel_remark").ToString

                If Not IsDBNull(dtRow.Item("cancel_datetime")) Then
                    mdtCancel_Datetime = dtRow.Item("cancel_datetime")
                End If

                'Pinkal (28-Apr-2020) -- Start
                'Enhancement Claim Retirement  - Claim Retirement Enhancement given by NMB .
                mblnIssubmit_Approval = CBool(dtRow.Item("issubmit_approval"))
                'Pinkal (28-Apr-2020) -- End

                mblnIsvoid = CBool(dtRow.Item("isvoid"))
                mintVoiduserunkid = CInt(dtRow.Item("voiduserunkid"))

                If Not IsDBNull(dtRow.Item("voiddatetime")) Then
                    mdtVoiddatetime = dtRow.Item("voiddatetime")
                End If
                mstrVoidreason = dtRow.Item("voidreason").ToString
                mintVoidloginemployeeunkid = CInt(dtRow.Item("voidloginemployeeunkid"))
                Exit For
            Next
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: GetData; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            If objDoOperation Is Nothing Then objDataOperation = Nothing
        End Try
    End Sub

    ''' <summary>
    ''' Modify By: Pinkal
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>
    Public Function GetList(ByVal xDatabaseName As String, ByVal xCompanyUnkid As Integer, ByVal xYearUnkid As Integer _
                                   , ByVal xUserUnkid As Integer, ByVal xUserModeSetting As String, ByVal xOnlyApproved As Boolean _
                                   , ByVal mdtEmpAsonDate As Date, Optional ByVal blnIncludeAccessFilterQry As Boolean = True _
                                   , Optional ByVal blnExcludeTermEmp_PayProcess As Boolean = False _
                                   , Optional ByVal blnAddApprovalCondition As Boolean = True, Optional ByVal mstrFiler As String = "") As DataSet

        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        objDataOperation = New clsDataOperation

        Try

            'Pinkal (10-Mar-2021) -- Start
            'Enhancement/Bug Retirement -   Working Retirement enhancement/Bug.

            Dim xDateJoinQry, xDataFilterQry, xUACQry, xUACFiltrQry, xAdvanceJoinQry As String
            xDateJoinQry = "" : xDataFilterQry = "" : xUACQry = "" : xUACFiltrQry = "" : xAdvanceJoinQry = ""
            Call GetDatesFilterString(xDateJoinQry, xDataFilterQry, mdtEmpAsonDate, mdtEmpAsonDate, , blnExcludeTermEmp_PayProcess, xDatabaseName)
            If blnIncludeAccessFilterQry Then Call NewAccessLevelFilterString(xUACQry, xUACFiltrQry, mdtEmpAsonDate, xOnlyApproved, xDatabaseName, xUserUnkid, xCompanyUnkid, xYearUnkid, xUserModeSetting, "", blnAddApprovalCondition, "")
            Call GetAdvanceFilterQry(xAdvanceJoinQry, mdtEmpAsonDate, xDatabaseName)


            strQ = " SELECT " & _
                       " cmclaim_request_master.crmasterunkid " & _
                       ",0 AS claimretirementunkid " & _
                       ",NULL claimretirementno " & _
                       ",CASE  WHEN ISNULL(cmclaim_retirement_master.employeeunkid, 0) <= 0 THEN cmclaim_request_master.employeeunkid " & _
                       " ELSE cmclaim_retirement_master.employeeunkid END AS employeeunkid " & _
                       ",cmclaim_request_master.claimrequestno " & _
                       ",ISNULL(hremployee_master.employeecode, '') AS employeecode " & _
                       ",ISNULL(hremployee_master.firstname, '') + ' ' + ISNULL(hremployee_master.surname, '') AS employee " & _
                       ",ISNULL(hremployee_master.employeecode, '') + ' - ' + ISNULL(hremployee_master.firstname, '') + ' ' + ISNULL(hremployee_master.surname, '') AS employeename " & _
                       ",cmclaim_request_master.expensetypeid " & _
                       ",CASE WHEN cmclaim_request_master.expensetypeid = '" & enExpenseType.EXP_LEAVE & "' THEN @Leave " & _
                       "          WHEN cmclaim_request_master.expensetypeid = '" & enExpenseType.EXP_MEDICAL & "' THEN @Medical " & _
                       "          WHEN cmclaim_request_master.expensetypeid = '" & enExpenseType.EXP_TRAINING & "' THEN @Training " & _
                       "          WHEN cmclaim_request_master.expensetypeid = '" & enExpenseType.EXP_MISCELLANEOUS & "' THEN @Miscellaneous " & _
                       "          WHEN cmclaim_request_master.expensetypeid = '" & enExpenseType.EXP_IMPREST & "' THEN @Imprest " & _
                       " END AS expensetype " & _
                       ", cmclaim_request_master.transactiondate " & _
                       ", App.ApprovedAmt AS approved_amount " & _
                       ",0.00 retirement_amount " & _
                       " ,ISNULL(App.ApprovedAmt, 0) AS balance " & _
                        ",@Pending AS [status] " & _
                       ",2 AS statusunkid " & _
                       ",-1 AS approverunkid " & _
                       ",-1 AS approveremployeeunkid " & _
                       ",'' AS p2prequisitionid " & _
                       ",'' AS p2pmessage " & _
                       ",'' AS p2pstatuscode " & _
                       ",NULL AS p2ptimestamp " & _
                       ",'' AS p2pposteddata " & _
                        ", 0 AS iscancel " & _
                       ",-1 AS canceluserunkid " & _
                       ",'' AS cancel_remark " & _
                       ",NULL AS cancel_datetime " & _
                       ", 0  AS issubmit_approval " & _
                       ",0 AS isvoid " & _
                       ",-1 AS voiduserunkid " & _
                       ",NULL voiddatetime " & _
                       ",'' voidreason " & _
                       ", 2 AS SrtId " & _
                       ", NULL AS Approvaldate " & _
                       " FROM  cmclaim_request_master " & _
                       " JOIN cmclaim_retirement_master ON cmclaim_request_master.crmasterunkid = cmclaim_retirement_master.crmasterunkid AND cmclaim_retirement_master.isvoid = 0 " & _
                       " LEFT JOIN hremployee_master ON hremployee_master.employeeunkid = cmclaim_request_master.employeeunkid "

            'Pinkal (03-May-2021)--KBC Retirement Issue -  Working on Reitrmentment KBC Issue.[", App.ApprovedAmt AS approved_amount  ,ISNULL(App.ApprovedAmt, 0) AS balance " & _]

            If xDateJoinQry.Trim.Length > 0 Then
                strQ &= xDateJoinQry
            End If

            If blnIncludeAccessFilterQry = True Then
                If xUACQry.Trim.Length > 0 Then
                    strQ &= xUACQry
                End If
            End If

            strQ &= " LEFT JOIN (SELECT DISTINCT " & _
                       "                        crmasterunkid " & _
                       "                 FROM cmclaim_approval_tran " & _
                       "                 LEFT JOIN cmexpense_master ON cmexpense_master.expenseunkid = cmclaim_approval_tran.expenseunkid AND cmexpense_master.isimprest = 1 " & _
                       "                 WHERE cmclaim_approval_tran.isvoid = 0 AND cmclaim_approval_tran.statusunkid = 1 AND approvaldate IS NOT NULL " & _
                       "                 AND iscancel = 0  " & _
                       "                 UNION  " & _
                       "                 SELECT DISTINCT " & _
                       "                         crmasterunkid " & _
                       "                 FROM cmclaim_request_tran " & _
                       "                 LEFT JOIN cmexpense_master ON cmexpense_master.expenseunkid = cmclaim_request_tran.expenseunkid AND cmexpense_master.isimprest = 1 " & _
                       "                 WHERE cmclaim_request_tran.isvoid = 0 " & _
                       "                 ) AS A ON a.crmasterunkid = cmclaim_request_master.crmasterunkid " & _
                       " JOIN ( " & _
                       "             SELECT " & _
                       "                 crmasterunkid,claimretirementunkid,statusunkid " & _
                       "                 ,ROW_NUMBER()OVER(PARTITION BY crmasterunkid ORDER BY transactiondate DESC) AS xno " & _
                       "             FROM cmclaim_retirement_master " & _
                       "             WHERE isvoid = 0 " & _
                       " ) AS Fn ON Fn.crmasterunkid = cmclaim_request_master.crmasterunkid  " & _
                       " AND fn.claimretirementunkid =cmclaim_retirement_master.claimretirementunkid AND Fn.xno = 1 " & _
                       " AND cmclaim_retirement_master.statusunkid = fn.statusunkid and fn.statusunkid = 3 " & _
                       " JOIN " & _
                       " ( " & _
                       "     SELECT " & _
                       "          crmasterunkid " & _
                       "         ,ISNULL(SUM(amount), 0.00) AS ApprovedAmt " & _
                       "         ,Approvaldate " & _
                       "     FROM cmclaim_approval_tran  " & _
                       "     WHERE isvoid = 0 And statusunkid = 1 And " & _
                       "     crapproverunkid IN ( " & _
                       "                                       SELECT crapproverunkid FROM  " & _
                       "                                       ( " & _
                       "                                               SELECT " & _
                       "                                                      ISNULL(cm.crapproverunkid, 0) AS crapproverunkid " & _
                       "                                                     ,cm.crmasterunkid " & _
                       "                                                     ,DENSE_RANK() OVER(PARTITION by cm.crmasterunkid ORDER BY crpriority DESC) AS rno " & _
                       "                                                     ,cm.approvaldate " & _
                       "                                                FROM cmclaim_approval_tran cm " & _
                       "                                                LEFT JOIN cmexpapprover_master ON cmexpapprover_master.crapproverunkid = cm.crapproverunkid " & _
                       "                                                LEFT JOIN cmapproverlevel_master	ON cmapproverlevel_master.crlevelunkid = cmexpapprover_master.crlevelunkid " & _
                       "										          LEFT JOIN cmexpense_master ON cmexpense_master.expenseunkid = cm.expenseunkid " & _
                       "                                                WHERE cm.isvoid = 0 And cmexpense_master.isimprest = 1 " & _
                       "                                      ) as cnt where cnt.rno= 1 AND cnt.crmasterunkid = cmclaim_approval_tran.crmasterunkid " & _
                       "                                  ) 	GROUP BY crmasterunkid,approvaldate " & _
                      " ) AS App ON App.crmasterunkid = cmclaim_request_master.crmasterunkid " & _
                       " WHERE cmclaim_request_master.isvoid = 0 AND cmclaim_request_master.statusunkid = 1 "


            'Pinkal (03-May-2021)--KBC Retirement Issue -  Working on Reitrmentment KBC Issue.

            If xUACFiltrQry.Trim.Length > 0 Then
                strQ &= " AND " & xUACFiltrQry
            End If


            If xDataFilterQry.Trim.Length > 0 Then
                strQ &= xDataFilterQry
            End If

            If mstrFiler.Trim.Length > 0 Then
                strQ &= " AND " & mstrFiler
            End If


            strQ &= " UNION ALL " & _
                       " SELECT " & _
                       " cmclaim_request_master.crmasterunkid " & _
                       " ,ISNULL(cmclaim_retirement_master.claimretirementunkid, 0) AS claimretirementunkid " & _
                       " ,cmclaim_retirement_master.claimretirementno " & _
                       " ,CASE WHEN ISNULL(cmclaim_retirement_master.employeeunkid, 0) <= 0 THEN cmclaim_request_master.employeeunkid ELSE cmclaim_retirement_master.employeeunkid END AS employeeunkid " & _
                       " ,cmclaim_request_master.claimrequestno " & _
                       " ,ISNULL(hremployee_master.employeecode,'') AS employeecode " & _
                       " ,ISNULL(hremployee_master.firstname,'') + ' ' + ISNULL(hremployee_master.surname,'') AS employee " & _
                       " ,ISNULL(hremployee_master.employeecode,'') + ' - ' + ISNULL(hremployee_master.firstname,'') + ' ' + ISNULL(hremployee_master.surname,'') AS employeename " & _
                       " ,cmclaim_request_master.expensetypeid " & _
                       " ,CASE WHEN cmclaim_request_master.expensetypeid = '" & enExpenseType.EXP_LEAVE & "' THEN @Leave " & _
                       "          WHEN cmclaim_request_master.expensetypeid = '" & enExpenseType.EXP_MEDICAL & "' THEN @Medical " & _
                       "          WHEN cmclaim_request_master.expensetypeid = '" & enExpenseType.EXP_TRAINING & "' THEN @Training " & _
                       "          WHEN cmclaim_request_master.expensetypeid = '" & enExpenseType.EXP_MISCELLANEOUS & "' THEN @Miscellaneous " & _
                       "          WHEN cmclaim_request_master.expensetypeid = '" & enExpenseType.EXP_IMPREST & "' THEN @Imprest END AS expensetype " & _
                       " ,CASE WHEN cmclaim_retirement_master.transactiondate IS NULL THEN cmclaim_request_master.transactiondate " & _
                       "            ELSE cmclaim_retirement_master.transactiondate END transactiondate " & _
                       " ,ISNULL(cmclaim_retirement_master.claim_amount,App.ApprovedAmt) AS approved_amount " & _
                       " ,CASE WHEN ISNULL(cmclaim_retirement_master.imprest_amount,0) <=0 THEN 0 ELSE CASE WHEN cmclaim_retirement_master.statusunkid = 3 THEN 0.00 ELSE cmclaim_retirement_master.imprest_amount END END AS retirement_amount " & _
                       " ,CASE WHEN  ISNULL(cmclaim_retirement_master.statusunkid,2) = 2 THEN ISNULL(App.ApprovedAmt,0.00) ELSE CASE WHEN cmclaim_retirement_master.statusunkid = 3 THEN  ISNULL(App.ApprovedAmt,0.00) ELSE ISNULL(cmclaim_retirement_master.balance,0.00) END END AS balance " & _
                       " ,CASE WHEN cmclaim_retirement_master.statusunkid IS NULL THEN @Pending ELSE " & _
                       " CASE WHEN cmclaim_retirement_master.statusunkid = 1 THEN @Approve " & _
                       "          WHEN cmclaim_retirement_master.statusunkid = 2 THEN @Pending " & _
                       "          WHEN cmclaim_retirement_master.statusunkid = 3 THEN @Reject " & _
                       "          WHEN cmclaim_retirement_master.statusunkid = 6 THEN @Cancel END END [status] " & _
                       " ,CASE WHEN ISNULL(cmclaim_retirement_master.statusunkid,0) <= 0 THEN 2 ELSE cmclaim_retirement_master.statusunkid END statusunkid " & _
                       ", cmclaim_retirement_master.approverunkid " & _
                       ", cmclaim_retirement_master.approveremployeeunkid " & _
                       ", cmclaim_retirement_master.p2prequisitionid " & _
                       ", cmclaim_retirement_master.p2pmessage " & _
                       ", cmclaim_retirement_master.p2pstatuscode " & _
                       ", cmclaim_retirement_master.p2ptimestamp " & _
                       ", cmclaim_retirement_master.p2pposteddata " & _
                       ", cmclaim_retirement_master.iscancel " & _
                       ", cmclaim_retirement_master.canceluserunkid " & _
                       ", cmclaim_retirement_master.cancel_remark " & _
                       ", cmclaim_retirement_master.cancel_datetime " & _
                       ", ISNULL(cmclaim_retirement_master.issubmit_approval,0) AS issubmit_approval " & _
                       ", cmclaim_retirement_master.isvoid " & _
                       ", cmclaim_retirement_master.voiduserunkid " & _
                       ", cmclaim_retirement_master.voiddatetime " & _
                       ", cmclaim_retirement_master.voidreason " & _
                       ", 1 AS SrtId " & _
                       ", CONVERT(CHAR(8),App.Approvaldate,112) AS Approvaldate " & _
                       " FROM cmclaim_request_master " & _
                       "  LEFT JOIN cmclaim_retirement_master ON cmclaim_request_master.crmasterunkid = cmclaim_retirement_master.crmasterunkid AND cmclaim_retirement_master.isvoid = 0 " & _
                       "  LEFT JOIN hremployee_master ON hremployee_master.employeeunkid = cmclaim_request_master.employeeunkid "

            'Pinkal (27-Apr-2021)-- KBC Enhancement  -  Working on Claim Retirement Enhancement.[  " ,CASE WHEN  ISNULL(cmclaim_retirement_master.statusunkid,2) = 2 THEN ISNULL(App.ApprovedAmt,0.00) ELSE ISNULL(cmclaim_retirement_master.balance,0.00) END AS balance " & _]

            If xDateJoinQry.Trim.Length > 0 Then
                strQ &= xDateJoinQry
            End If

            If blnIncludeAccessFilterQry = True Then
                If xUACQry.Trim.Length > 0 Then
                    strQ &= xUACQry
                End If
            End If

            strQ &= "  JOIN  ( " & _
                       "                SELECT DISTINCT " & _
                       "                            crmasterunkid " & _
                       "                FROM cmclaim_approval_tran " & _
                       "                LEFT JOIN cmexpense_master ON cmexpense_master.expenseunkid = cmclaim_approval_tran.expenseunkid AND cmexpense_master.isimprest = 1 " & _
                       "                WHERE cmclaim_approval_tran.isvoid = 0 AND cmclaim_approval_tran.statusunkid = 1 AND approvaldate IS NOT NULL AND iscancel = 0 " & _
                       "                UNION " & _
                       "                SELECT DISTINCT " & _
                       "                        crmasterunkid " & _
                       "                FROM cmclaim_request_tran " & _
                       "                LEFT JOIN cmexpense_master ON cmexpense_master.expenseunkid = cmclaim_request_tran.expenseunkid  AND cmexpense_master.isimprest = 1 " & _
                       "                WHERE cmclaim_request_tran.isvoid = 0 " & _
                       "           ) AS A ON a.crmasterunkid = cmclaim_request_master.crmasterunkid " & _
                       " JOIN " & _
                         " ( " & _
                         "     SELECT " & _
                         "          crmasterunkid " & _
                         "         ,ISNULL(SUM(amount), 0.00) AS ApprovedAmt " & _
                         "         ,Approvaldate " & _
                         "     FROM cmclaim_approval_tran  " & _
                         "     WHERE isvoid = 0 And statusunkid = 1 And " & _
                         "     crapproverunkid IN ( " & _
                         "                                       SELECT crapproverunkid FROM  " & _
                         "                                       ( " & _
                         "                                               SELECT " & _
                         "                                                      ISNULL(cm.crapproverunkid, 0) AS crapproverunkid " & _
                         "                                                     ,cm.crmasterunkid " & _
                         "                                                     ,DENSE_RANK() OVER(PARTITION by cm.crmasterunkid ORDER BY crpriority DESC) AS rno " & _
                         "                                                     ,cm.approvaldate " & _
                         "                                                FROM cmclaim_approval_tran cm " & _
                         "                                                LEFT JOIN cmexpapprover_master ON cmexpapprover_master.crapproverunkid = cm.crapproverunkid " & _
                         "                                                LEFT JOIN cmapproverlevel_master	ON cmapproverlevel_master.crlevelunkid = cmexpapprover_master.crlevelunkid " & _
                         "										          LEFT JOIN cmexpense_master ON cmexpense_master.expenseunkid = cm.expenseunkid " & _
                         "                                                WHERE cm.isvoid = 0 And cmexpense_master.isimprest = 1 " & _
                         "                                      ) as cnt where cnt.rno= 1 AND cnt.crmasterunkid = cmclaim_approval_tran.crmasterunkid " & _
                         "                                  ) 	GROUP BY crmasterunkid,approvaldate " & _
                      " ) AS App ON App.crmasterunkid = cmclaim_request_master.crmasterunkid " & _
                      "  WHERE cmclaim_request_master.isvoid = 0 AND cmclaim_request_master.statusunkid = 1 "

            If xUACFiltrQry.Trim.Length > 0 Then
                strQ &= " AND " & xUACFiltrQry
            End If

            If xDataFilterQry.Trim.Length > 0 Then
                strQ &= xDataFilterQry
            End If

            If mstrFiler.Trim.Length > 0 Then
                strQ &= " AND " & mstrFiler
            End If
            'Pinkal (10-Mar-2021) -- End



            'Pinkal (10-Jun-2022) -- Start
            'Enhancement KBC :(AC2-558) Provide setting on expense master to stop new imprest request if a user has unretired imprest or a retirement that's pending approval.
            strQ &= " AND cmclaim_request_master.crmasterunkid NOT IN (SELECT crmasterunkid  " & _
                       "                     FROM cmretire_process_tran  " & _
                       "                     WHERE isvoid = 0 AND crmasterunkid > 0 AND claimretirementunkid <= 0 AND periodunkid > 0 AND isposted = 1 ) "
            'Pinkal (10-Jun-2022) -- End


            strQ &= " Order by cmclaim_request_master.claimrequestno,SrtId"

            objDataOperation.ClearParameters()
            objDataOperation.AddParameter("@Leave", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage("clsExpCommonMethods", 2, "Leave"))
            objDataOperation.AddParameter("@Medical", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage("clsExpCommonMethods", 3, "Medical"))
            objDataOperation.AddParameter("@Training", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage("clsExpCommonMethods", 4, "Training"))
            objDataOperation.AddParameter("@Miscellaneous", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage("clsExpCommonMethods", 8, "Miscellaneous"))
            objDataOperation.AddParameter("@Imprest", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage("clsExpCommonMethods", 9, "Imprest"))

            objDataOperation.AddParameter("@Approve", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage("clsMasterData", 110, "Approved"))
            objDataOperation.AddParameter("@Pending", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage("clsMasterData", 111, "Pending"))
            objDataOperation.AddParameter("@Reject", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage("clsMasterData", 112, "Rejected"))
            objDataOperation.AddParameter("@Cancel", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage("clsMasterData", 115, "Cancelled"))

            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: GetList; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
        Return dsList
    End Function

    ''' <summary>
    ''' Modify By: Pinkal
    ''' </summary>
    ''' <returns>Boolean</returns>
    ''' <purpose> INSERT INTO Database Table (cmclaim_retirement_master) </purpose>
    Public Function Insert(ByVal xDatabaseName As String, ByVal xCompanyUnkid As Integer, _
                                   ByVal mdtClaimRetirement As DataTable, ByVal dtAttchement As DataTable) As Boolean

        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception


        Dim intVocNoType As Integer = 0
        Dim mstrEmployeeAsonDate As String = ""
        Dim mstrArutiSelfServiceURL As String = ""

        Dim objConfig As New clsConfigOptions
        objConfig._Companyunkid = IIf(xCompanyUnkid = 0, Company._Object._Companyunkid, xCompanyUnkid)
        intVocNoType = objConfig._ClaimRequestVocNoType
        mstrEmployeeAsonDate = objConfig._EmployeeAsOnDate
        mstrArutiSelfServiceURL = objConfig._ArutiSelfServiceURL

        If intVocNoType = 0 Then
            If isExist(mstrClaimRetirementNo) Then
                mstrMessage = Language.getMessage(mstrModuleName, 1, "This Claim Retirement Number already exists. Please define new Claim Retirement Number.")
                Return False
            End If
        ElseIf intVocNoType = 1 Then
            mstrClaimRetirementNo = ""
        End If


        objDataOperation = New clsDataOperation
        objDataOperation.BindTransaction()

        Try
            objDataOperation.ClearParameters()
            objDataOperation.AddParameter("@crmasterunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintCrmasterunkid.ToString)
            objDataOperation.AddParameter("@claimretirementno", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrClaimRetirementNo.ToString)
            objDataOperation.AddParameter("@expensetypeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintExpensetypeunkid.ToString)
            objDataOperation.AddParameter("@employeeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintEmployeeunkid.ToString)
            objDataOperation.AddParameter("@transactiondate", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, mdtTransactiondate.ToString)
            objDataOperation.AddParameter("@retirement_remark", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrRetirement_Remark.ToString)
            objDataOperation.AddParameter("@claim_amount", SqlDbType.Decimal, eZeeDataType.DECIMAL_SIZE, mdecClaim_amount.ToString)
            objDataOperation.AddParameter("@imprest_amount", SqlDbType.Decimal, eZeeDataType.DECIMAL_SIZE, mdecImprest_Amount.ToString)
            objDataOperation.AddParameter("@balance", SqlDbType.Decimal, eZeeDataType.DECIMAL_SIZE, mdecBalance.ToString)
            objDataOperation.AddParameter("@approverunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintApproverunkid.ToString)
            objDataOperation.AddParameter("@approveremployeeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintApproveremployeeunkid.ToString)
            objDataOperation.AddParameter("@statusunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintStatusunkid.ToString)
            objDataOperation.AddParameter("@p2prequisitionid", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrP2prequisitionid.ToString)
            objDataOperation.AddParameter("@p2pmessage", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrP2pmessage.ToString)
            objDataOperation.AddParameter("@p2pstatuscode", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrP2pstatuscode.ToString)

            If mdtP2ptimestamp <> Nothing Then
                objDataOperation.AddParameter("@p2ptimestamp", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, mdtP2ptimestamp.ToString)
            Else
                objDataOperation.AddParameter("@p2ptimestamp", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, DBNull.Value)
            End If


            objDataOperation.AddParameter("@userunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintUserunkid.ToString)
            objDataOperation.AddParameter("@loginemployeeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintLoginemployeeunkid.ToString)
            objDataOperation.AddParameter("@iscancel", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIscancel.ToString)
            objDataOperation.AddParameter("@canceluserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintCanceluserunkid.ToString)
            objDataOperation.AddParameter("@cancel_remark", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrCancel_Remark.ToString)

            If mdtCancel_Datetime <> Nothing Then
                objDataOperation.AddParameter("@cancel_datetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, mdtCancel_Datetime.ToString)
            Else
                objDataOperation.AddParameter("@cancel_datetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, DBNull.Value)
            End If


            'Pinkal (28-Apr-2020) -- Start
            'Enhancement Claim Retirement  - Claim Retirement Enhancement given by NMB .
            objDataOperation.AddParameter("@issubmit_approval", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIssubmit_Approval.ToString)
            'Pinkal (28-Apr-2020) -- End


            objDataOperation.AddParameter("@isvoid", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsvoid.ToString)
            objDataOperation.AddParameter("@voiduserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintVoiduserunkid.ToString)

            If mdtVoiddatetime <> Nothing Then
                objDataOperation.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, mdtVoiddatetime.ToString)
            Else
                objDataOperation.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, DBNull.Value)
            End If

            objDataOperation.AddParameter("@voidreason", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrVoidreason.ToString)
            objDataOperation.AddParameter("@voidloginemployeeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintVoidloginemployeeunkid.ToString)

            strQ = "INSERT INTO cmclaim_retirement_master ( " & _
                      "  crmasterunkid " & _
                      ", claimretirementno " & _
                      ", expensetypeunkid " & _
                      ", employeeunkid " & _
                      ", transactiondate " & _
                      ", retirement_remark " & _
                      ", claim_amount " & _
                      ", imprest_amount " & _
                      ", balance " & _
                      ", approverunkid " & _
                      ", approveremployeeunkid " & _
                      ", statusunkid " & _
                      ", p2prequisitionid " & _
                      ", p2pmessage " & _
                      ", p2pstatuscode " & _
                      ", p2ptimestamp " & _
                      ", userunkid " & _
                      ", loginemployeeunkid " & _
                      ", iscancel " & _
                      ", canceluserunkid " & _
                      ", cancel_remark " & _
                      ", cancel_datetime " & _
                      ", issubmit_approval " & _
                      ", isvoid " & _
                      ", voiduserunkid " & _
                      ", voiddatetime " & _
                      ", voidreason " & _
                      ", voidloginemployeeunkid" & _
                    ") VALUES (" & _
                      "  @crmasterunkid " & _
                      ", @claimretirementno " & _
                      ", @expensetypeunkid " & _
                      ", @employeeunkid " & _
                      ", @transactiondate " & _
                      ", @retirement_remark " & _
                      ", @claim_amount " & _
                      ", @imprest_amount " & _
                      ", @balance " & _
                      ", @approverunkid " & _
                      ", @approveremployeeunkid " & _
                      ", @statusunkid " & _
                      ", @p2prequisitionid " & _
                      ", @p2pmessage " & _
                      ", @p2pstatuscode " & _
                      ", @p2ptimestamp " & _
                      ", @userunkid " & _
                      ", @loginemployeeunkid " & _
                      ", @iscancel " & _
                      ", @canceluserunkid " & _
                      ", @cancel_remark " & _
                      ", @cancel_datetime " & _
                      ", @issubmit_approval " & _
                      ", @isvoid " & _
                      ", @voiduserunkid " & _
                      ", @voiddatetime " & _
                      ", @voidreason " & _
                      ", @voidloginemployeeunkid" & _
                    "); SELECT @@identity"


            'Pinkal (28-Apr-2020) -- Enhancement Claim Retirement  - Claim Retirement Enhancement given by NMB .[issubmit_approval]

            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            mintClaimretirementunkid = dsList.Tables(0).Rows(0).Item(0)

            If intVocNoType = 1 Then
                If Set_AutoNumber(objDataOperation, mintClaimretirementunkid, "cmclaim_retirement_master", "claimretirementno", "claimretirementunkid", "NextClaimRetirementFormNo", objConfig._ClaimRetirementFormNoPrifix, xCompanyUnkid) = False Then
                    If objDataOperation.ErrorMessage <> "" Then
                        objDataOperation.ReleaseTransaction(False)
                        Return False
                    End If
                End If

                If Get_Saved_Number(objDataOperation, mintClaimretirementunkid, "cmclaim_retirement_master", "claimretirementno", "claimretirementunkid", mstrClaimRetirementNo) = False Then
                    If objDataOperation.ErrorMessage <> "" Then
                        exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                        Throw exForce
                    End If
                End If
            End If


            If ATInsertClaim_Retirement(objDataOperation, 1) = False Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            objCRetirementTran._Claimretirementunkid = mintClaimretirementunkid
            objCRetirementTran._ApproverTranId = 0
            objCRetirementTran._ApproverEmpId = 0
            objCRetirementTran._DataTable = mdtClaimRetirement.Copy()
            objCRetirementTran._Userunkid = mintUserunkid
            objCRetirementTran._WebClientIP = mstrWebClientIP
            objCRetirementTran._WebHostName = mstrWebHostName
            objCRetirementTran._FormName = mstrFormName
            objCRetirementTran._IsWeb = mblnIsWeb

            If objCRetirementTran.InsertUpdateDelete_ClaimRetirement_Tran(objDataOperation) = False Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If



            'Pinkal (28-Apr-2020) -- Start
            'Enhancement Claim Retirement  - Claim Retirement Enhancement given by NMB .

            Dim mblnIsHRExpense As Boolean = True

            If mblnIssubmit_Approval Then

                mdtClaimRetirement = objCRetirementTran._DataTable.Copy()

            If mdtClaimRetirement IsNot Nothing AndAlso mdtClaimRetirement.Rows.Count > 0 Then
                If mdtClaimRetirement.AsEnumerable().Where(Function(x) x.Field(Of Boolean)("ishrexpense") = False AndAlso x.Field(Of String)("AUD") <> "D").Count > 0 Then
                    mblnIsHRExpense = False
                End If
            End If

            If mblnIsHRExpense Then

                Dim objExpAppr As New clsExpenseApprover_Master
                Dim dsApproverList As DataSet = objExpAppr.GetEmployeeApprovers(mintExpensetypeunkid, mintEmployeeunkid, "List", objDataOperation)
                If dsApproverList IsNot Nothing AndAlso dsApproverList.Tables(0).Rows.Count > 0 Then

                    Dim mintMinPriority As Integer = dsApproverList.Tables(0).AsEnumerable().Min(Function(x) x.Field(Of Integer)("crpriority"))

                    objCRetirementApprovalTran._Crmasterunkid = mintCrmasterunkid
                    objCRetirementApprovalTran._ClaimRetirementTran = objCRetirementTran._DataTable.Copy()
                    objCRetirementApprovalTran._Statusunkid = mintStatusunkid

                    objCRetirementApprovalTran._Userunkid = mintUserunkid
                    objCRetirementApprovalTran._WebClientIP = mstrWebClientIP
                    objCRetirementApprovalTran._WebHostName = mstrWebHostName
                    objCRetirementApprovalTran._FormName = mstrFormName
                    objCRetirementApprovalTran._IsWeb = mblnIsWeb

                    For Each dr As DataRow In dsApproverList.Tables(0).Rows

                        If mintMinPriority = CInt(dr("crpriority")) Then
                            objCRetirementApprovalTran._Visibleid = mintStatusunkid
                        Else
                            objCRetirementApprovalTran._Visibleid = -1
                        End If

                        objCRetirementApprovalTran._Approveremployeeunkid = CInt(dr("employeeunkid"))
                        objCRetirementApprovalTran._Approverunkid = CInt(dr("crapproverunkid"))

                        If objCRetirementApprovalTran.InsertUpdate_ClaimRetirement_Approval(objDataOperation, mstrRetirement_Remark, False, Nothing) = False Then
                            exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                            Throw exForce
                        End If
                    Next
                End If
                objExpAppr = Nothing

            End If

            End If

            'Pinkal (28-Apr-2020) -- End

            If dtAttchement IsNot Nothing AndAlso dtAttchement.Rows.Count > 0 Then
                If dtAttchement.Select("AUD = 'A'").Length > 0 Then
                    For Each dRow As DataRow In dtAttchement.Select("AUD = 'A'")
                        Dim dXRow As DataRow = dRow
                        Dim intTran = mdtClaimRetirement.AsEnumerable().Where(Function(x) x.Field(Of String)("GUID") = dXRow.Item("GUID").ToString).Select(Function(X) X.Field(Of Integer)("Claimretirementtranunkid")).First
                        dRow.Item("transactionunkid") = intTran
                        dRow.AcceptChanges()
                    Next
                End If

                Dim objDocument As New clsScan_Attach_Documents
                objDocument._Datatable = dtAttchement
                If objDocument.InsertUpdateDelete_Documents(objDataOperation) = False Then
                    Throw New Exception(objDocument._Message)
                End If
                objDocument = Nothing
            End If

            objDataOperation.ReleaseTransaction(True)



            'Pinkal (28-Apr-2020) -- Start
            'Enhancement Claim Retirement  - Claim Retirement Enhancement given by NMB .

            If mblnIssubmit_Approval Then

            '/*  START TO SEND EMAIL NOTIFICATION OF SPECIFIC EMPLOYEE FIRST APPROVER
            If mintClaimretirementunkid > 0 AndAlso mblnIsHRExpense Then
                objCRetirementApprovalTran._Userunkid = mintUserunkid
                objCRetirementApprovalTran._WebClientIP = mstrWebClientIP
                objCRetirementApprovalTran._WebHostName = mstrWebHostName
                objCRetirementApprovalTran._FormName = mstrFormName
                objCRetirementApprovalTran._IsWeb = mblnIsWeb

                Dim dsApprover As DataSet = objCRetirementApprovalTran.GetClaimRetirementApproverExpesneList("List", False, xDatabaseName, mintUserunkid, mstrEmployeeAsonDate _
                                                                                                                                                        , True, -1, "", mintClaimretirementunkid)

                Dim xPriority As Integer = -1
                xPriority = dsApprover.Tables(0).AsEnumerable().Min(Function(x) x.Field(Of Integer)("crpriority"))
                Dim mstrPriorityFilter As String = "crpriority = " & xPriority

                'PASS DEFAULT RETIREMENT STATUS TO 1
                objCRetirementApprovalTran.SendMailToApprover(xDatabaseName, mstrEmployeeAsonDate, mintClaimretirementunkid _
                                                                                        , mstrClaimRetirementNo, mintEmployeeunkid, xPriority, 1, mstrPriorityFilter _
                                                                                        , xCompanyUnkid, mstrArutiSelfServiceURL, mintLoginMode, mintLoginemployeeunkid _
                                                                                        , mintUserunkid, mstrFormName)
            End If
            '/*  END TO SEND EMAIL NOTIFICATION OF SPECIFIC EMPLOYEE FIRST APPROVER

            End If

            'Pinkal (28-Apr-2020) -- End

            Return True
        Catch ex As Exception
            objDataOperation.ReleaseTransaction(False)
            Throw New Exception(ex.Message & "; Procedure Name: Insert; Module Name: " & mstrModuleName)
            Return False
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
    End Function

    ''' <summary>
    ''' Modify By: Pinkal
    ''' </summary>
    ''' <returns>Boolean</returns>
    ''' <purpose> Update Database Table (cmclaim_retirement_master) </purpose>
    Public Function Update(ByVal mdtClaimRetirement As DataTable, ByVal dtAttchement As DataTable) As Boolean
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        objDataOperation = New clsDataOperation
        objDataOperation.BindTransaction()

        Try
            objDataOperation.ClearParameters()
            objDataOperation.AddParameter("@claimretirementunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintClaimretirementunkid.ToString)
            objDataOperation.AddParameter("@claimretirementno", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrClaimRetirementNo.ToString)
            objDataOperation.AddParameter("@crmasterunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintCrmasterunkid.ToString)
            objDataOperation.AddParameter("@expensetypeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintExpensetypeunkid.ToString)
            objDataOperation.AddParameter("@employeeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintEmployeeunkid.ToString)
            objDataOperation.AddParameter("@transactiondate", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, mdtTransactiondate.ToString)
            objDataOperation.AddParameter("@retirement_remark", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrRetirement_Remark.ToString)
            objDataOperation.AddParameter("@claim_amount", SqlDbType.Decimal, eZeeDataType.DECIMAL_SIZE, mdecClaim_amount.ToString)
            objDataOperation.AddParameter("@imprest_amount", SqlDbType.Decimal, eZeeDataType.DECIMAL_SIZE, mdecImprest_Amount.ToString)
            objDataOperation.AddParameter("@balance", SqlDbType.Decimal, eZeeDataType.DECIMAL_SIZE, mdecBalance.ToString)
            objDataOperation.AddParameter("@approverunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintApproverunkid.ToString)
            objDataOperation.AddParameter("@approveremployeeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintApproveremployeeunkid.ToString)
            objDataOperation.AddParameter("@statusunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintStatusunkid.ToString)
            objDataOperation.AddParameter("@p2prequisitionid", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrP2prequisitionid.ToString)
            objDataOperation.AddParameter("@p2pmessage", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrP2pmessage.ToString)
            objDataOperation.AddParameter("@p2pstatuscode", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrP2pstatuscode.ToString)

            If mdtP2ptimestamp <> Nothing Then
                objDataOperation.AddParameter("@p2ptimestamp", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, mdtP2ptimestamp.ToString)
            Else
                objDataOperation.AddParameter("@p2ptimestamp", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, DBNull.Value)
            End If

            objDataOperation.AddParameter("@userunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintUserunkid.ToString)
            objDataOperation.AddParameter("@loginemployeeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintLoginemployeeunkid.ToString)
            objDataOperation.AddParameter("@iscancel", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIscancel.ToString)
            objDataOperation.AddParameter("@canceluserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintCanceluserunkid.ToString)
            objDataOperation.AddParameter("@cancel_remark", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrCancel_Remark.ToString)

            If mdtCancel_Datetime <> Nothing Then
                objDataOperation.AddParameter("@cancel_datetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, mdtCancel_Datetime.ToString)
            Else
                objDataOperation.AddParameter("@cancel_datetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, DBNull.Value)
            End If


            'Pinkal (28-Apr-2020) -- Start
            'Enhancement Claim Retirement  - Claim Retirement Enhancement given by NMB .
            objDataOperation.AddParameter("@issubmit_approval", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIssubmit_Approval.ToString)
            'Pinkal (28-Apr-2020) -- End


            objDataOperation.AddParameter("@isvoid", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsvoid.ToString)
            objDataOperation.AddParameter("@voiduserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintVoiduserunkid.ToString)

            If mdtVoiddatetime <> Nothing Then
                objDataOperation.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, mdtVoiddatetime.ToString)
            Else
                objDataOperation.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, DBNull.Value)
            End If

            objDataOperation.AddParameter("@voidreason", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrVoidreason.ToString)
            objDataOperation.AddParameter("@voidloginemployeeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintVoidloginemployeeunkid.ToString)

            strQ = "UPDATE cmclaim_retirement_master SET " & _
                      "  claimretirementno = @claimretirementno " & _
                      ", crmasterunkid = @crmasterunkid" & _
                      ", expensetypeunkid = @expensetypeunkid" & _
                      ", employeeunkid = @employeeunkid" & _
                      ", transactiondate = @transactiondate" & _
                      ", retirement_remark = @retirement_remark" & _
                      ", claim_amount = @claim_amount" & _
                      ", imprest_amount = @imprest_amount" & _
                      ", balance = @balance" & _
                      ", approverunkid = @approverunkid" & _
                      ", approveremployeeunkid = @approveremployeeunkid" & _
                      ", statusunkid = @statusunkid " & _
                      ", p2prequisitionid = @p2prequisitionid" & _
                      ", p2pmessage = @p2pmessage" & _
                      ", p2pstatuscode = @p2pstatuscode" & _
                      ", p2ptimestamp = @p2ptimestamp" & _
                      ", userunkid = @userunkid" & _
                      ", loginemployeeunkid = @loginemployeeunkid" & _
                      ", iscancel = @iscancel" & _
                      ", canceluserunkid = @canceluserunkid" & _
                      ", cancel_remark = @cancel_remark" & _
                      ", cancel_datetime = @cancel_datetime" & _
                      ", issubmit_approval = @issubmit_approval" & _
                      ", isvoid = @isvoid" & _
                      ", voiduserunkid = @voiduserunkid" & _
                      ", voiddatetime = @voiddatetime" & _
                      ", voidreason = @voidreason" & _
                      ", voidloginemployeeunkid = @voidloginemployeeunkid " & _
                      " WHERE claimretirementunkid = @claimretirementunkid AND isvoid = 0"

            'Pinkal (28-Apr-2020) -- Start   'Enhancement Claim Retirement  - Claim Retirement Enhancement given by NMB .[", issubmit_approval = @issubmit_approval" & _]

            Call objDataOperation.ExecNonQuery(strQ)

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            If ATInsertClaim_Retirement(objDataOperation, 2) = False Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If


            objCRetirementTran._Claimretirementunkid = mintClaimretirementunkid
            objCRetirementTran._ApproverTranId = 0
            objCRetirementTran._ApproverEmpId = 0
            objCRetirementTran._DataTable = mdtClaimRetirement.Copy()
            objCRetirementTran._Userunkid = mintUserunkid
            objCRetirementTran._WebClientIP = mstrWebClientIP
            objCRetirementTran._WebHostName = mstrWebHostName
            objCRetirementTran._FormName = mstrFormName
            objCRetirementTran._IsWeb = mblnIsWeb

            If objCRetirementTran.InsertUpdateDelete_ClaimRetirement_Tran(objDataOperation) = False Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If



            'Pinkal (28-Apr-2020) -- Start
            'Enhancement Claim Retirement  - Claim Retirement Enhancement given by NMB .

            If mblnIssubmit_Approval Then

            mdtClaimRetirement = objCRetirementTran._DataTable.Copy()

            Dim mblnIsHRExpense As Boolean = True
            If mdtClaimRetirement IsNot Nothing AndAlso mdtClaimRetirement.Rows.Count > 0 Then
                If mdtClaimRetirement.AsEnumerable().Where(Function(x) x.Field(Of Boolean)("ishrexpense") = False AndAlso x.Field(Of String)("AUD") <> "D").Count > 0 Then
                    mblnIsHRExpense = False
                End If
            End If

            If mblnIsHRExpense Then

                    'Pinkal (28-Apr-2020) -- Start
                    'Enhancement Claim Retirement  - Claim Retirement Enhancement given by NMB .
                    ' IT IS ONLY USED IN UPDATE METHOD OF THE CLAIM  RETIREMENT MASTER NOT ANY OTHER METHODS.
                    Dim drRow() As DataRow = objCRetirementTran._DataTable.Select("ishrexpense = 1 AND (AUD = '' OR AUD = 'U')")
                    If drRow.Length > 0 Then
                        For Each dr As DataRow In drRow
                            dr("AUD") = "A"
                            dr.AcceptChanges()
                        Next
                    End If
                    'Pinkal (28-Apr-2020) -- End


                Dim objExpAppr As New clsExpenseApprover_Master
                Dim dsApproverList As DataSet = objExpAppr.GetEmployeeApprovers(mintExpensetypeunkid, mintEmployeeunkid, "List", objDataOperation)
                If dsApproverList IsNot Nothing AndAlso dsApproverList.Tables(0).Rows.Count > 0 Then

                    Dim mintMinPriority As Integer = dsApproverList.Tables(0).AsEnumerable().Min(Function(x) x.Field(Of Integer)("crpriority"))

                    objCRetirementApprovalTran._Crmasterunkid = mintCrmasterunkid
                    objCRetirementApprovalTran._ClaimRetirementTran = objCRetirementTran._DataTable.Copy()
                    objCRetirementApprovalTran._Statusunkid = mintStatusunkid

                    objCRetirementApprovalTran._Userunkid = mintUserunkid
                    objCRetirementApprovalTran._WebClientIP = mstrWebClientIP
                    objCRetirementApprovalTran._WebHostName = mstrWebHostName
                    objCRetirementApprovalTran._FormName = mstrFormName
                    objCRetirementApprovalTran._IsWeb = mblnIsWeb

                    For Each dr As DataRow In dsApproverList.Tables(0).Rows

                        If mintMinPriority = CInt(dr("crpriority")) Then
                            objCRetirementApprovalTran._Visibleid = mintStatusunkid
                        Else
                            objCRetirementApprovalTran._Visibleid = -1
                        End If

                        objCRetirementApprovalTran._Approveremployeeunkid = CInt(dr("employeeunkid"))
                        objCRetirementApprovalTran._Approverunkid = CInt(dr("crapproverunkid"))

                        If objCRetirementApprovalTran.InsertUpdate_ClaimRetirement_Approval(objDataOperation, mstrRetirement_Remark, False, Nothing) = False Then
                            exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                            Throw exForce
                        End If
                    Next
                End If
                objExpAppr = Nothing

            End If

            End If

            'Pinkal (28-Apr-2020) -- End

            If dtAttchement IsNot Nothing AndAlso dtAttchement.Rows.Count > 0 Then
                If dtAttchement.Select("AUD = 'A'").Length > 0 Then
                    For Each dtRow As DataRow In mdtClaimRetirement.Select("AUD = 'A'")
                        For Each dRow As DataRow In dtAttchement.Select("GUID = '" & dtRow.Item("GUID") & "'")
                            dRow.Item("transactionunkid") = dtRow.Item("crtranunkid")
                            dRow.AcceptChanges()
                        Next
                    Next
                End If

                Dim objDocument As New clsScan_Attach_Documents
                objDocument._Datatable = dtAttchement
                If objDocument.InsertUpdateDelete_Documents(objDataOperation) = False Then
                    Throw New Exception(objDocument._Message)
                End If
                objDocument = Nothing
            End If


            objDataOperation.ReleaseTransaction(True)
            Return True
        Catch ex As Exception
            objDataOperation.ReleaseTransaction(False)
            Throw New Exception(ex.Message & "; Procedure Name: Update; Module Name: " & mstrModuleName)
            Return False
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
    End Function

    ''' <summary>
    ''' Modify By: Pinkal
    ''' </summary>
    ''' <returns>Boolean</returns>
    ''' <purpose> Delete Database Table (cmclaim_retirement_master) </purpose>
    Public Function Delete(ByVal intUnkid As Integer, ByVal mdtImprest As DataTable, ByVal dtAttachment As DataTable) As Boolean
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        objDataOperation = New clsDataOperation
        objDataOperation.BindTransaction()

        Try


            Dim objClaimRetirementTran As New clsclaim_retirement_Tran
            objClaimRetirementTran._Claimretirementunkid(objDataOperation) = intUnkid
            objClaimRetirementTran._FormName = mstrFormName
            objClaimRetirementTran._WebClientIP = mstrWebClientIP
            objClaimRetirementTran._WebHostName = mstrWebHostName
            objClaimRetirementTran._IsWeb = True
            objClaimRetirementTran._Userunkid = mintUserunkid
            objClaimRetirementTran._voidloginemployeeunkid = mintVoidloginemployeeunkid

            Dim dTRetirement As DataTable = objClaimRetirementTran._DataTable.Copy()


            strQ = "UPDATE cmclaim_retirement_tran SET " & _
                      " isvoid = @isvoid " & _
                      ",voiduserunkid = @voiduserunkid " & _
                      ",voidloginemployeeunkid = @voidloginemployeeunkid " & _
                      ",voiddatetime = getdate() " & _
                      ",voidreason = @voidreason " & _
                      " WHERE claimretirementtranunkid = @claimretirementtranunkid "

            For Each dr As DataRow In dTRetirement.Rows
                objDataOperation.ClearParameters()
                objDataOperation.AddParameter("@isvoid", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsvoid)
                objDataOperation.AddParameter("@voiduserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintUserunkid)
                objDataOperation.AddParameter("@voidloginemployeeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintVoidloginemployeeunkid)
                objDataOperation.AddParameter("@voidreason", SqlDbType.NVarChar, eZeeDataType.DESC_SIZE, mstrVoidreason)
                objDataOperation.AddParameter("@claimretirementtranunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, CInt(dr("claimretirementtranunkid")))
                Call objDataOperation.ExecNonQuery(strQ)

                If objDataOperation.ErrorMessage <> "" Then
                    exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                    Throw exForce
                End If

                If objClaimRetirementTran.ATInsertClaimRetirement_Tran(objDataOperation, 3, dr) = False Then
                    exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                    Throw exForce
                End If

            Next
            objClaimRetirementTran = Nothing


            If dtAttachment IsNot Nothing Then
                Dim objDocument As New clsScan_Attach_Documents
                objDocument._Datatable = dtAttachment
                If objDocument.InsertUpdateDelete_Documents(objDataOperation) = False Then
                    Throw New Exception(objDocument._Message)
                End If
                objDocument = Nothing
            End If


            strQ = "UPDATE cmclaim_retirement_master SET " & _
                      " isvoid = @isvoid " & _
                      ",voiduserunkid = @voiduserunkid " & _
                      ",voidloginemployeeunkid = @voidloginemployeeunkid " & _
                      ",voiddatetime = getdate() " & _
                      ",voidreason = @voidreason " & _
                      " WHERE claimretirementunkid = @claimretirementunkid "

            objDataOperation.ClearParameters()
            objDataOperation.AddParameter("@isvoid", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsvoid)
            objDataOperation.AddParameter("@voiduserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintUserunkid)
            objDataOperation.AddParameter("@voidloginemployeeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintVoidloginemployeeunkid)
            objDataOperation.AddParameter("@voidreason", SqlDbType.NVarChar, eZeeDataType.DESC_SIZE, mstrVoidreason)
            objDataOperation.AddParameter("@claimretirementunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intUnkid)

            Call objDataOperation.ExecNonQuery(strQ)

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            If ATInsertClaim_Retirement(objDataOperation, 3) = False Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            objDataOperation.ReleaseTransaction(True)
            Return True
        Catch ex As Exception
            objDataOperation.ReleaseTransaction(False)
            Throw New Exception(ex.Message & "; Procedure Name: Delete; Module Name: " & mstrModuleName)
            Return False
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
    End Function

    ''' <summary>
    ''' Modify By: Pinkal
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>
    Public Function isUsed(ByVal intUnkid As Integer) As Boolean
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        objDataOperation = New clsDataOperation

        Try
            strQ = "<Query>"

            objDataOperation.AddParameter("@claimretirementunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intUnkid)

            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            Return dsList.Tables(0).Rows.Count > 0
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: isUsed; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
    End Function

    ''' <summary>
    ''' Modify By: Pinkal
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>
    Public Function isExist(ByVal xClaimReturementNo As String, Optional ByVal intUnkid As Integer = -1) As Boolean
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        objDataOperation = New clsDataOperation

        Try
            strQ = "SELECT " & _
                      "  claimretirementunkid " & _
                      ", crmasterunkid " & _
                      ", expensetypeunkid " & _
                      ", employeeunkid " & _
                      ", transactiondate " & _
                      ", retirement_remark " & _
                      ", claim_amount " & _
                      ", imprest_amount " & _
                      ", balance " & _
                      ", approverunkid " & _
                      ", approveremployeeunkid " & _
                      ", p2prequisitionid " & _
                      ", p2pmessage " & _
                      ", p2pstatuscode " & _
                      ", p2ptimestamp " & _
                      ", p2pposteddata " & _
                      ", userunkid " & _
                      ", loginemployeeunkid " & _
                      ", iscancel " & _
                      ", canceluserunkid " & _
                      ", cancel_remark " & _
                      ", cancel_datetime " & _
                      ", issubmit_approval " & _
                      ", isvoid " & _
                      ", voiduserunkid " & _
                      ", voiddatetime " & _
                      ", voidreason " & _
                      ", voidloginemployeeunkid " & _
                      " FROM cmclaim_retirement_master " & _
                      " WHERE isvoid = 0 AND claimretirementno = @claimretirementno "

            'Pinkal (28-Apr-2020) -- Enhancement Claim Retirement  - Claim Retirement Enhancement given by NMB .[issubmit_approval]


            If intUnkid > 0 Then
                strQ &= " AND claimretirementunkid <> @claimretirementunkid"
            End If

            objDataOperation.AddParameter("@claimretirementno", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, xClaimReturementNo)
            objDataOperation.AddParameter("@claimretirementunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intUnkid)

            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            Return dsList.Tables(0).Rows.Count > 0
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: isExist; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
    End Function

    ''' <summary>
    ''' Modify By: Pinkal
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>
    Public Function ATInsertClaim_Retirement(ByVal objDataOperation As clsDataOperation, ByVal xAuditType As Integer) As Boolean

        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        Try
            objDataOperation.ClearParameters()
            objDataOperation.AddParameter("@claimretirementunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintClaimretirementunkid.ToString)
            objDataOperation.AddParameter("@claimretirementno", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrClaimRetirementNo)
            objDataOperation.AddParameter("@crmasterunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintCrmasterunkid.ToString)
            objDataOperation.AddParameter("@expensetypeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintExpensetypeunkid.ToString)
            objDataOperation.AddParameter("@employeeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintEmployeeunkid.ToString)
            objDataOperation.AddParameter("@transactiondate", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, mdtTransactiondate.ToString)
            objDataOperation.AddParameter("@retirement_remark", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrRetirement_Remark.ToString)
            objDataOperation.AddParameter("@claim_amount", SqlDbType.Decimal, eZeeDataType.DECIMAL_SIZE, mdecClaim_amount.ToString)
            objDataOperation.AddParameter("@imprest_amount", SqlDbType.Decimal, eZeeDataType.DECIMAL_SIZE, mdecImprest_Amount.ToString)
            objDataOperation.AddParameter("@balance", SqlDbType.Decimal, eZeeDataType.DECIMAL_SIZE, mdecBalance.ToString)
            objDataOperation.AddParameter("@approverunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintApproverunkid.ToString)
            objDataOperation.AddParameter("@approveremployeeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintApproveremployeeunkid.ToString)
            objDataOperation.AddParameter("@statusunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintStatusunkid.ToString)
            objDataOperation.AddParameter("@p2prequisitionid", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrP2prequisitionid.ToString)
            objDataOperation.AddParameter("@p2pmessage", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrP2pmessage.ToString)
            objDataOperation.AddParameter("@p2pstatuscode", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrP2pstatuscode.ToString)

            If mdtP2ptimestamp <> Nothing Then
                objDataOperation.AddParameter("@p2ptimestamp", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, mdtP2ptimestamp)
            Else
                objDataOperation.AddParameter("@p2ptimestamp", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, DBNull.Value)
            End If

            If mstrP2pposteddata.Trim.Length > 0 Then
                objDataOperation.AddParameter("@p2pposteddata", SqlDbType.VarBinary, System.Text.Encoding.Unicode.GetBytes(mstrP2pposteddata).Length, System.Text.Encoding.Unicode.GetBytes(mstrP2pposteddata))
            Else
                objDataOperation.AddParameter("@p2pposteddata", SqlDbType.VarBinary, mstrP2pposteddata.Length, DBNull.Value)
            End If

            objDataOperation.AddParameter("@loginemployeeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintLoginemployeeunkid.ToString)
            objDataOperation.AddParameter("@voidloginemployeeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintVoidloginemployeeunkid.ToString)
            objDataOperation.AddParameter("@iscancel", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIscancel.ToString)
            objDataOperation.AddParameter("@cancel_remark", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrCancel_Remark.ToString)

            'Pinkal (28-Apr-2020) -- Start
            'Enhancement Claim Retirement  - Claim Retirement Enhancement given by NMB .
            objDataOperation.AddParameter("@issubmit_approval", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIssubmit_Approval.ToString)
            'Pinkal (28-Apr-2020) -- End

            objDataOperation.AddParameter("@audittype", SqlDbType.Int, eZeeDataType.INT_SIZE, xAuditType.ToString)
            objDataOperation.AddParameter("@audituserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintUserunkid)
            objDataOperation.AddParameter("@ip", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrWebClientIP)
            objDataOperation.AddParameter("@machine_name", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrWebhostName)
            objDataOperation.AddParameter("@form_name", SqlDbType.NVarChar, 500, mstrFormName)
            objDataOperation.AddParameter("@isweb", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsWeb)
            
            strQ = "INSERT INTO atcmclaim_retirement_master ( " & _
                      " claimretirementunkid " & _
                      ", claimretirementno " & _
                      ", crmasterunkid " & _
                      ", expensetypeunkid " & _
                      ", employeeunkid " & _
                      ", transactiondate " & _
                      ", retirement_remark " & _
                      ", claim_amount " & _
                      ", imprest_amount " & _
                      ", balance " & _
                      ", approverunkid " & _
                      ", approveremployeeunkid " & _
                      ", statusunkid " & _
                      ", p2prequisitionid " & _
                      ", p2pmessage " & _
                      ", p2pstatuscode " & _
                      ", p2ptimestamp " & _
                      ", p2pposteddata " & _
                      ", loginemployeeunkid " & _
                      ", voidloginemployeeunkid " & _
                      ", iscancel " & _
                      ", cancel_remark " & _
                      ", issubmit_approval " & _
                      ", audittype " & _
                      ", audituserunkid " & _
                      ", auditdatetime " & _
                      ", ip " & _
                      ", machine_name " & _
                      ", form_name " & _
                      ", isweb " & _
                    ") VALUES (" & _
                      "  @claimretirementunkid " & _
                      ", @claimretirementno " & _
                      ", @crmasterunkid " & _
                      ", @expensetypeunkid " & _
                      ", @employeeunkid " & _
                      ", @transactiondate " & _
                      ", @retirement_remark " & _
                      ", @claim_amount " & _
                      ", @imprest_amount " & _
                      ", @balance " & _
                      ", @approverunkid " & _
                      ", @approveremployeeunkid " & _
                      ", @statusunkid " & _
                      ", @p2prequisitionid " & _
                      ", @p2pmessage " & _
                      ", @p2pstatuscode " & _
                      ", @p2ptimestamp " & _
                      ", @p2pposteddata " & _
                      ", @loginemployeeunkid " & _
                      ", @voidloginemployeeunkid " & _
                      ", @iscancel " & _
                      ", @cancel_remark " & _
                      ", @issubmit_approval " & _
                      ", @audittype " & _
                      ", @audituserunkid " & _
                      ", getdate() " & _
                      ", @ip " & _
                      ", @machine_name " & _
                      ", @form_name " & _
                      ", @isweb " & _
                    "); SELECT @@identity"



            'Pinkal (28-Apr-2020) -- Enhancement Claim Retirement  - Claim Retirement Enhancement given by NMB .[issubmit_approval]


                dsList = objDataOperation.ExecQuery(strQ, "List")

                If objDataOperation.ErrorMessage <> "" Then
                    exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                    Throw exForce
                End If

                Return True
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: ATInsertClaim_Retirement; Module Name: " & mstrModuleName)
            Return False
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
        End Try
    End Function

    ''' <summary>
    ''' Modify By: Pinkal Jariwala
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>
    Public Sub SendMailToEmployee(ByVal intEmpId As Integer, _
                                                   ByVal mstrClaimRetirementFormNo As String, _
                                                   ByVal strEmployeeAsonDate As String, _
                                                   ByVal intStatusID As Integer, _
                                                   ByVal intCompanyId As Integer, _
                                                   Optional ByVal strPath As String = "", _
                                                   Optional ByVal strFileName As String = "", _
                                                   Optional ByVal iLoginTypeId As Integer = 0, _
                                                   Optional ByVal iLoginEmployeeId As Integer = 0, _
                                                   Optional ByVal iUserId As Integer = 0, _
                                                   Optional ByVal mstrRejectionRemark As String = "")
        Try
            Dim objNet As New clsNetConnectivity
            If objNet._Conected = False Then Exit Sub

            If intStatusID = 2 Then Exit Sub


            Dim objEmployee As New clsEmployee_Master
            objEmployee._Employeeunkid(eZeeDate.convertDate(strEmployeeAsonDate.ToString()).Date) = intEmpId

            Dim mstrStatus As String = ""


            Select Case intStatusID
                Case 1
                    mstrStatus = Language.getMessage("clsMasterData", 110, "Approved")
                Case 3
                    mstrStatus = Language.getMessage("clsMasterData", 112, "Rejected")
                Case 6
                    mstrStatus = Language.getMessage("clsMasterData", 115, "Cancelled")
            End Select


            Dim objMail As New clsSendMail
            objMail._Subject = Language.getMessage(mstrModuleName, 3, "Claim Retirement Status Notification")
            Dim strMessage As String = ""
            strMessage = "<HTML> <BODY>"
            strMessage &= Language.getMessage(mstrModuleName, 2, "Dear") & " " & getTitleCase(objEmployee._Firstname & "  " & objEmployee._Surname) & ", <BR><BR>"
            strMessage &= " " & Language.getMessage(mstrModuleName, 4, " This is to inform you that your claim retirement no ") & " <B>(" & mstrClaimRetirementFormNo & ")</B> " & Language.getMessage(mstrModuleName, 5, " has been ") & mstrStatus & "."

            If intStatusID <> 6 Then
                If intStatusID = 3 Then
                    strMessage &= "<BR></BR>"
                    strMessage &= " " & Language.getMessage(mstrModuleName, 6, "with below") & " " & Language.getMessage(mstrModuleName, 7, "Remarks/Comments:") & "<BR></BR> " & ChrW(34) & mstrRejectionRemark & ChrW(34)
                End If
            Else
                strMessage &= "<BR></BR>"
                strMessage &= " " & Language.getMessage(mstrModuleName, 6, "with below") & " " & Language.getMessage(mstrModuleName, 7, "Remarks/Comments:") & "<BR></BR> " & ChrW(34) & mstrRejectionRemark & ChrW(34)
            End If

            If intStatusID = 1 Then
                strMessage &= Language.getMessage(mstrModuleName, 8, "Kindly take note of it.")
            Else
                strMessage &= "."
            End If

            strMessage &= "<BR></BR><BR></BR><center>""POWERED BY ARUTI HR & PAYROLL MANAGEMENT SOFTWARE""</center>"
            strMessage &= "</BODY></HTML>"

            objMail._Message = strMessage
            objMail._ToEmail = objEmployee._Email

            If iLoginTypeId <= 0 Then iLoginTypeId = enLogin_Mode.DESKTOP
            If mstrFormName.Trim.Length > 0 Then
                objMail._Form_Name = mstrFormName
                objMail._ClientIP = mstrWebClientIP
                objMail._HostName = mstrWebHostName
            End If
            objMail._LogEmployeeUnkid = iLoginEmployeeId
            objMail._OperationModeId = iLoginTypeId
            objMail._UserUnkid = iUserId
            objMail._SenderAddress = objEmployee._Firstname & " " & objEmployee._Surname
            objMail._ModuleRefId = clsSendMail.enAT_VIEW_TYPE.CLAIM_RETIREMENT_MGT

            If intStatusID = 1 AndAlso strPath <> "" Then
                objMail._AttachedFiles = strFileName
                objMail.SendMail(intCompanyId, True, strPath)
            Else
                objMail.SendMail(intCompanyId)
            End If

            objEmployee = Nothing

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: SendMailToEmployee; Module Name: " & mstrModuleName)
        End Try
    End Sub

    ''' <summary>
    ''' Modify By: Pinkal Jariwala
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>
    Public Sub GenerateNewRequisitionRequestForP2P(ByVal xDatabaseName As String, ByVal xUserId As Integer, ByVal xYearId As Integer, ByVal xCompnayId As Integer _
                                                                                                                       , ByVal xUserModeSetting As String, ByVal xEmployeeId As Integer, ByVal xClaimFormNo As String _
                                                                                                                       , ByVal xClaimRetireNo As String, ByVal xRetirementDate As DateTime _
                                                                                                                       , ByVal xApplierName As String, ByVal mdtRetirmentDetail As DataTable _
                                                                                                                       , ByVal mdtAttachment As DataTable, ByRef objP2P As clsClaimRetirementP2PIntegration)
        Try
            If xClaimRetireNo.Trim.Length > 0 Then
                Dim xDepartment As String = ""
                Dim xEmployeeCode As String = ""

                If xEmployeeId > 0 Then
                    '/* START TO GET EMPLOYEE DEPARTMENT/COST CENTER AS ON CLAIM DATE REQUESTED BY EMPLOYEE/USER.

                    Dim objEmployee As New clsEmployee_Master
                    Dim StrCheck_Fields As String = String.Empty


                    StrCheck_Fields = clsEmployee_Master.EmpColEnum.Col_Code & "," & clsEmployee_Master.EmpColEnum.Col_Employee_Name & "," & clsEmployee_Master.EmpColEnum.Col_Department

                    Dim dsList As DataSet = objEmployee.GetListForDynamicField(StrCheck_Fields, xDatabaseName, xUserId, xYearId, xCompnayId, xRetirementDate, xRetirementDate _
                                                                                                                                                    , xUserModeSetting, True, False, "List", xEmployeeId, False, "", False, False, False, False, Nothing, False)
                    objEmployee = Nothing

                    If dsList IsNot Nothing AndAlso dsList.Tables(0).Rows.Count > 0 Then
                        xDepartment = dsList.Tables(0).Rows(0)(Language.getMessage("clsEmployee_Master", 120, "Department")).ToString()
                        xEmployeeCode = dsList.Tables(0).Rows(0)(Language.getMessage("clsEmployee_Master", 42, "Code")).ToString()
                    End If
                    dsList.Clear()
                    dsList = Nothing
                    '/* END TO GET EMPLOYEE DEPARTMENT/COST CENTER AS ON CLAIM DATE REQUESTED BY EMPLOYEE/USER.



                    If xDepartment.Trim().Length > 0 Then
                        Dim xTotalClaimAmount As Decimal = 0
                        If mdtRetirmentDetail IsNot Nothing AndAlso mdtRetirmentDetail.Rows.Count > 0 Then
                            xTotalClaimAmount = mdtRetirmentDetail.AsEnumerable().Where(Function(x) x.Field(Of String)("AUD") <> "D").Sum(Function(x) x.Field(Of Decimal)("base_amount"))
                            mdtRetirmentDetail = New DataView(mdtRetirmentDetail, "AUD <> 'D'", "", DataViewRowState.CurrentRows).ToTable()
                        End If


                        If mdtRetirmentDetail IsNot Nothing AndAlso mdtRetirmentDetail.Rows.Count > 0 Then

                            '/* START-- DON'T CHANGE ANYTHING TAKEN ONLY FIRST EMPLOYEE BANK ACCOUNT NO OF EMPLOYEE AS PER RUTT'S GUIDANCE.IF HE WANTS TO CHANGE IT DON'T CHANGE FOR NMB.
                            Dim xBankAcNo As String = ""
                            Dim objEmpBank As New clsEmployeeBanks
                            Dim dsEmpBankList As DataSet = objEmpBank.GetEmployeeBanks(xDatabaseName, xUserId, xYearId, xCompnayId, xRetirementDate, xRetirementDate, xUserModeSetting, True, False, xEmployeeId, False, "")
                            If dsEmpBankList IsNot Nothing AndAlso dsEmpBankList.Tables(0).Rows.Count > 0 Then
                                xBankAcNo = dsEmpBankList.Tables(0).Rows(0)("accountno").ToString()
                            Else

                            End If
                            objEmpBank = Nothing
                            '/* END-- DON'T CHANGE ANYTHING TAKEN ONLY FIRST EMPLOYEE BANK ACCOUNT NO OF EMPLOYEE AS PER RUTT'S GUIDANCE.IF HE WANTS TO CHANGE IT DON'T CHANGE FOR NMB.


                            objP2P.category = "General expenses"
                            objP2P.mainline = mdtRetirmentDetail.Rows(0)("gldesc").ToString().Trim()
                            objP2P.dept = xDepartment.Trim()
                            objP2P.glCode = mdtRetirmentDetail.Rows(0)("glCode").ToString().Trim()
                            objP2P.costCenter = mdtRetirmentDetail.Rows(0)("costcentercode").ToString().Trim()
                            objP2P.ccy = "TZS"
                            objP2P.invAmount = xTotalClaimAmount
                            objP2P.invoiceNo = xClaimRetireNo.Trim()
                            objP2P.maker = xApplierName.Trim()
                            objP2P.remarks = mdtRetirmentDetail.Rows(0)("expense_remark").ToString().Trim()
                            objP2P.status = "SUBMIT"
                            objP2P.supplierId = xBankAcNo.Trim()
                            objP2P.extRefNo = xClaimRetireNo.Trim()
                            objP2P.claimType = "STAFF IMPREST"
                            objP2P.claimRefNo = xClaimFormNo
                        End If

                        If mdtAttachment IsNot Nothing AndAlso mdtAttachment.Rows.Count > 0 Then
                            For Each dr In mdtAttachment.Rows
                                objP2P.documentList.Add(New DocumentList(dr("fileuniquename").ToString().Substring(0, dr("fileuniquename").ToString().IndexOf(".")), dr("Documentype").ToString(), dr("fileextension").ToString(), dr("fileuniquename").ToString(), dr("DocBase64Value").ToString()))
                            Next
                        End If

                    End If

                End If

            End If
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: GenerateNewRequisitionRequestForP2P; Module Name: " & mstrModuleName)
        End Try
    End Sub

    ''' <summary>
    ''' Modify By: Pinkal Jariwala
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>
    Public Function UpdateP2PResponseToClaimRetirement(ByVal dtTable As DataTable, ByVal xClaimRetirementId As Integer, ByVal xUserId As Integer, ByVal mstrPostedData As String) As Boolean

        Try
            If dtTable Is Nothing Then Return True
            Dim strQ As String = ""
            Dim exForce As Exception

            Dim objDataOperation As New clsDataOperation
            objDataOperation.BindTransaction()

            strQ = "UPDATE cmclaim_retirement_master  SET " & _
                     " p2pposteddata = @p2pposteddata " & _
                     ",p2prequisitionid = @p2prequisitionid " & _
                     ",p2pmessage = @p2pmessage " & _
                     ",p2pstatuscode = @p2pstatuscode " & _
                     ",p2ptimestamp = @p2ptimestamp " & _
                     " WHERE claimretirementunkid  = @claimretirementunkid "


            For Each dr As DataRow In dtTable.Rows
                objDataOperation.ClearParameters()
                objDataOperation.AddParameter("@p2prequisitionid", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, dr("PmtReferenceNo").ToString())
                objDataOperation.AddParameter("@p2pmessage", SqlDbType.NVarChar, eZeeDataType.DESC_SIZE, dr("message").ToString())
                objDataOperation.AddParameter("@p2pstatuscode", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, dr("status").ToString())

                If IsDBNull(dr("timeStamp")) OrElse dr("timeStamp").ToString().Trim.Length <= 0 Then
                    objDataOperation.AddParameter("@p2ptimestamp", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, DBNull.Value)
                Else
                    Dim dt As New DateTime(1970, 1, 1, 0, 0, 0, 0, System.DateTimeKind.Utc)
                    dt = dt.AddMilliseconds(CDbl(dr("timeStamp"))).ToLocalTime()
                    objDataOperation.AddParameter("@p2ptimestamp", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, dt)
                End If

                objDataOperation.AddParameter("@p2pposteddata", SqlDbType.VarBinary, System.Text.Encoding.Unicode.GetBytes(mstrPostedData).Length, System.Text.Encoding.Unicode.GetBytes(mstrPostedData))
                objDataOperation.AddParameter("@claimretirementunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, xClaimRetirementId)
                Call objDataOperation.ExecNonQuery(strQ)

                If objDataOperation.ErrorMessage <> "" Then
                    exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                    Throw exForce
                End If

                _Claimretirementunkid(objDataOperation) = xClaimRetirementId

                If ATInsertClaim_Retirement(objDataOperation, 2) = False Then
                    exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                    Throw exForce
                End If

            Next

            objDataOperation.ReleaseTransaction(True)
            Return True
        Catch ex As Exception
            objDataOperation.ReleaseTransaction(False)
            Throw New Exception(ex.Message & "; Procedure Name: UpdateP2PResponseToClaimMst; Module Name: " & mstrModuleName)
        End Try
    End Function


    'Pinkal (10-Feb-2021) -- Start
    'Enhancement/Bug Retirement -   Working Retirement enhancement/Bug.

    ''' <summary>
    ''' Modify By: Pinkal Jariwala
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>
    Public Sub SendSubmissionEmailToEmployee(ByVal intEmpId As Integer, _
                                                                   ByVal mstrClaimRetirementFormNo As String, _
                                                                   ByVal mstrClaimRequestFormNo As String, _
                                                                   ByVal strEmployeeAsonDate As String, _
                                                                   ByVal intCompanyId As Integer, _
                                                                   Optional ByVal iLoginTypeId As Integer = 0, _
                                                                   Optional ByVal iLoginEmployeeId As Integer = 0, _
                                                                   Optional ByVal iUserId As Integer = 0)
        Try
            Dim objNet As New clsNetConnectivity
            If objNet._Conected = False Then Exit Sub

            Dim objEmployee As New clsEmployee_Master
            objEmployee._Employeeunkid(eZeeDate.convertDate(strEmployeeAsonDate.ToString()).Date) = intEmpId


            Dim objMail As New clsSendMail
            objMail._Subject = Language.getMessage(mstrModuleName, 9, "Imprest Retirement Submission Retirement No.") & " " & mstrClaimRetirementFormNo
            Dim strMessage As String = ""
            strMessage = "<HTML> <BODY>"
            strMessage &= Language.getMessage(mstrModuleName, 2, "Dear") & " " & getTitleCase(objEmployee._Firstname & "  " & objEmployee._Surname) & ", <BR><BR>"
            strMessage &= " " & Language.getMessage(mstrModuleName, 10, "Please note that your retirement for claim application no") & " <B>(" & mstrClaimRequestFormNo & ")</B> " & Language.getMessage(mstrModuleName, 11, "has been successfully submitted for approval.")

            strMessage &= "<BR></BR><BR></BR>" & Language.getMessage(mstrModuleName, 12, "Regards.")

            strMessage &= "<BR></BR><BR></BR><center>""POWERED BY ARUTI HR & PAYROLL MANAGEMENT SOFTWARE""</center>"
            strMessage &= "</BODY></HTML>"

            objMail._Message = strMessage
            objMail._ToEmail = objEmployee._Email

            If iLoginTypeId <= 0 Then iLoginTypeId = enLogin_Mode.DESKTOP
            If mstrFormName.Trim.Length > 0 Then
                objMail._Form_Name = mstrFormName
                objMail._ClientIP = mstrWebClientIP
                objMail._HostName = mstrWebHostName
            End If
            objMail._LogEmployeeUnkid = iLoginEmployeeId
            objMail._OperationModeId = iLoginTypeId
            objMail._UserUnkid = iUserId
            objMail._SenderAddress = objEmployee._Firstname & " " & objEmployee._Surname
            objMail._ModuleRefId = clsSendMail.enAT_VIEW_TYPE.CLAIM_RETIREMENT_MGT

            objMail.SendMail(intCompanyId)

            objEmployee = Nothing


        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: SendSubmissionEmailToEmployee; Module Name: " & mstrModuleName)
        End Try
    End Sub

    'Pinkal (10-Feb-2021) -- End



    'Pinkal (10-Jun-2022) -- Start
    'Enhancement KBC :(AC2-558) Provide setting on expense master to stop new imprest request if a user has unretired imprest or a retirement that's pending approval.
    Public Function GetRetirementFromClaimRequest(ByVal xClaimRequestId As Integer, ByRef xClaimRetirementStatusId As Integer, Optional ByVal objDoOperation As clsDataOperation = Nothing) As Integer
        Dim xClaimRetirementId As Integer = 0
        Dim exForce As Exception
        Try

            If objDoOperation Is Nothing Then
                objDataOperation = New clsDataOperation
            Else
                objDataOperation = objDoOperation
            End If

            Dim strQ As String = "SELECT Top 1 ISNULL(claimretirementunkid,0) As claimretirementunkid, ISNULL(statusunkid,0) AS statusunkid " & _
                                           " FROM  cmclaim_retirement_master " & _
                                           " WHERE isvoid = 0 AND crmasterunkid = @crmasterunkid " & _
                                           " ORDER BY transactiondate DESC"


            objDataOperation.ClearParameters()
            objDataOperation.AddParameter("@crmasterunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, xClaimRequestId)
            Dim dsList As DataSet = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            If dsList IsNot Nothing AndAlso dsList.Tables(0).Rows.Count > 0 Then
                xClaimRetirementId = CInt(dsList.Tables(0).Rows(0)("claimretirementunkid"))
                xClaimRetirementStatusId = CInt(dsList.Tables(0).Rows(0)("statusunkid"))
            Else
                xClaimRetirementId = -1
            End If

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: GetRetirementFromClaimRequest; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If objDoOperation Is Nothing Then objDataOperation = Nothing
        End Try
        Return xClaimRetirementId
    End Function
    'Pinkal (10-Jun-2022) -- End



	'<Language> This Auto Generated Text Please Do Not Modify it.
#Region " Language & UI Settings "
	Public Shared Sub SetMessages()
		Try
            Language.setMessage(mstrModuleName, 1, "This Claim Retirement Number already exists. Please define new Claim Retirement Number.")
            Language.setMessage(mstrModuleName, 2, "Dear")
            Language.setMessage(mstrModuleName, 3, "Claim Retirement Status Notification")
            Language.setMessage(mstrModuleName, 4, " This is to inform you that your claim retirement no")
            Language.setMessage(mstrModuleName, 5, " has been")
            Language.setMessage(mstrModuleName, 6, "with below")
            Language.setMessage(mstrModuleName, 7, "Remarks/Comments:")
            Language.setMessage(mstrModuleName, 8, "Kindly take note of it.")
            Language.setMessage(mstrModuleName, 9, "Imprest Retirement Submission Retirement No.")
            Language.setMessage(mstrModuleName, 10, "Please note that your retirement for claim application no")
            Language.setMessage(mstrModuleName, 11, "has been successfully submitted for approval.")
            Language.setMessage(mstrModuleName, 12, "Regards.")

			Language.setMessage("clsExpCommonMethods", 2, "Leave")
			Language.setMessage("clsExpCommonMethods", 3, "Medical")
			Language.setMessage("clsExpCommonMethods", 4, "Training")
            Language.setMessage("clsExpCommonMethods", 8, "Miscellaneous")
			Language.setMessage("clsExpCommonMethods", 9, "Imprest")
		
			Language.setMessage("clsMasterData", 110, "Approved")
			Language.setMessage("clsMasterData", 111, "Pending")
			Language.setMessage("clsMasterData", 112, "Rejected")
			Language.setMessage("clsMasterData", 115, "Cancelled")
			
            Language.setMessage("clsEmployee_Master", 42, "Code")
			Language.setMessage("clsEmployee_Master", 120, "Department")

		Catch Ex As Exception
			DisplayError.Show("-1", ex.Message, "SetMessages", mstrModuleName)
		End Try
	End Sub
#End Region 'Language & UI Settings
	'</Language>
End Class