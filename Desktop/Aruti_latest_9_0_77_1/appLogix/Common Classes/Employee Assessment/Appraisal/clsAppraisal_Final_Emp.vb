﻿'************************************************************************************************************************************
'Class Name : clsApprisal_Filter.vb
'Purpose    :
'Date       :08-Feb-12
'Written By :Sandeep J. Sharma
'Modified   :
'************************************************************************************************************************************

Imports eZeeCommonLib

''' <summary>
''' Purpose: 
''' Developer: Pinkal Jariwala
''' </summary>

Public Class clsAppraisal_Final_Emp

#Region " Private Variables "

    Private Shared ReadOnly mstrModuleName As String = "clsAppraisal_Final_Emp"
    Dim mstrMessage As String = ""
    Private mintSFEmpTranUnkid As Integer = -1
    Private mintUserunkid As Integer
    Private mblnIsvoid As Boolean
    Private mdtVoiddatetime As DateTime
    Private mstrVoidreason As String = ""
    Private mintVoiduserunkid As Integer
    Private mdtTran As DataTable
    Private mintShortListUnkid As Integer = 0
    Private mintFilterUnkid As Integer = 0
    'S.SANDEEP [ 22 OCT 2013 ] -- START
    Private mintAppointmentTypeId As Integer = 0
    Private mdtDate1 As DateTime = Nothing
    Private mdtDate2 As DateTime = Nothing
    'S.SANDEEP [ 22 OCT 2013 ] -- END

#End Region

#Region " Properties "

    'S.SANDEEP [28-May-2018] -- START 
    'ISSUE/ENHANCEMENT : {Audit Trails} 
    Private mstrFormName As String = String.Empty
    Public WriteOnly Property _FormName() As String
        Set(ByVal value As String)
            mstrFormName = value
        End Set
    End Property

    Private mstrClientIP As String = ""
    Public WriteOnly Property _ClientIP() As String
        Set(ByVal value As String)
            mstrClientIP = value
        End Set
    End Property

    Private mstrHostName As String = ""
    Public WriteOnly Property _HostName() As String
        Set(ByVal value As String)
            mstrHostName = value
        End Set
    End Property

    Private mblnIsWeb As Boolean = False
    Public WriteOnly Property _FromWeb() As Boolean
        Set(ByVal value As Boolean)
            mblnIsWeb = value
        End Set
    End Property

    Private mintAuditUserId As Integer = 0
    Public WriteOnly Property _AuditUserId() As Integer
        Set(ByVal value As Integer)
            mintAuditUserId = value
        End Set
    End Property

    Private mintCompanyUnkid As Integer = 0
    Public WriteOnly Property _CompanyUnkid() As Integer
        Set(ByVal value As Integer)
            mintCompanyUnkid = value
        End Set
    End Property

    Private mdtAuditDate As DateTime = Now
    Public WriteOnly Property _AuditDate() As DateTime
        Set(ByVal value As DateTime)
            mdtAuditDate = value
        End Set
    End Property

    Private xDataOpr As clsDataOperation = Nothing
    Public Property _xDataOpr() As clsDataOperation
        Get
            Return xDataOpr
        End Get
        Set(ByVal value As clsDataOperation)
            xDataOpr = value
        End Set
    End Property

    Private mintLoginEmployeeunkid As Integer = 0
    Public Property _LoginEmployeeunkid() As Integer
        Get
            Return mintLoginEmployeeunkid
        End Get
        Set(ByVal value As Integer)
            mintLoginEmployeeunkid = value
        End Set
    End Property

    Private mintVoidlogingemployeeunkid As Integer = 0
    Public Property _Voidloginemployeeunkid() As Integer
        Get
            Return mintVoidlogingemployeeunkid
        End Get
        Set(ByVal value As Integer)
            mintVoidlogingemployeeunkid = value
        End Set
    End Property
    'S.SANDEEP [28-May-2018] -- END 


    Public Property _DataTable() As DataTable
        Get
            Return mdtTran
        End Get
        Set(ByVal value As DataTable)
            mdtTran = value
        End Set
    End Property

    Public Property _Userunkid() As Integer
        Get
            Return mintUserunkid
        End Get
        Set(ByVal value As Integer)
            mintUserunkid = value
        End Set
    End Property

    Public Property _Isvoid() As Boolean
        Get
            Return mblnIsvoid
        End Get
        Set(ByVal value As Boolean)
            mblnIsvoid = value
        End Set
    End Property

    Public Property _Voiduserunkid() As Integer
        Get
            Return mintVoiduserunkid
        End Get
        Set(ByVal value As Integer)
            mintVoiduserunkid = value
        End Set
    End Property

    Public Property _Voiddatetime() As Date
        Get
            Return mdtVoiddatetime
        End Get
        Set(ByVal value As Date)
            mdtVoiddatetime = value
        End Set
    End Property

    Public Property _Voidreason() As String
        Get
            Return mstrVoidreason
        End Get
        Set(ByVal value As String)
            mstrVoidreason = value
        End Set
    End Property

    Public Property _ShortListUnkid() As Integer
        Get
            Return mintShortListUnkid
        End Get
        Set(ByVal value As Integer)
            mintShortListUnkid = value
        End Set
    End Property

    Public Property _FilterUnkid() As Integer
        Get
            Return mintFilterUnkid
        End Get
        Set(ByVal value As Integer)
            mintFilterUnkid = value
        End Set
    End Property

    'S.SANDEEP [ 22 OCT 2013 ] -- START
    Public WriteOnly Property _AppointmentTypeId() As Integer
        Set(ByVal value As Integer)
            mintAppointmentTypeId = value
        End Set
    End Property

    Public WriteOnly Property _Date1() As DateTime
        Set(ByVal value As DateTime)
            mdtDate1 = value
        End Set
    End Property

    Public WriteOnly Property _Date2() As DateTime
        Set(ByVal value As DateTime)
            mdtDate2 = value
        End Set
    End Property
    'S.SANDEEP [ 22 OCT 2013 ] -- END

#End Region

#Region " Contructor "

    Public Sub New()
        Try
            mdtTran = New DataTable("EmpData")

            mdtTran.Columns.Add("finalemployeeunkid", System.Type.GetType("System.Int32")).DefaultValue = -1
            mdtTran.Columns.Add("shortlistunkid", System.Type.GetType("System.Int32")).DefaultValue = -1
            mdtTran.Columns.Add("filterunkid", System.Type.GetType("System.Int32")).DefaultValue = -1
            mdtTran.Columns.Add("employeeunkid", System.Type.GetType("System.Int32")).DefaultValue = -1
            mdtTran.Columns.Add("periodunkid", System.Type.GetType("System.Int32")).DefaultValue = -1
            mdtTran.Columns.Add("isshortlisted", System.Type.GetType("System.Boolean")).DefaultValue = False
            mdtTran.Columns.Add("isfinalshortlisted", System.Type.GetType("System.Boolean")).DefaultValue = True
            mdtTran.Columns.Add("employeecode", System.Type.GetType("System.String")).DefaultValue = ""
            mdtTran.Columns.Add("employeename", System.Type.GetType("System.String")).DefaultValue = ""
            mdtTran.Columns.Add("email", System.Type.GetType("System.String")).DefaultValue = ""

        Catch ex As Exception
            Throw New Exception(ex.Message & " Procedure : New " & mstrModuleName)
        Finally
        End Try
    End Sub

#End Region

#Region " Private/Public Methods "

    'Sohail (10 Feb 2012) -- Start
    'TRA - ENHANCEMENT
    Public Function GetShortListFinalEmployee(Optional ByVal intEmpUnkID As Integer = -1 _
                                              , Optional ByVal intShortListUnkid As Integer = -1 _
                                              , Optional ByVal intFilterUnkid As Integer = -1 _
                                              , Optional ByVal strReferenceNo As String = "" _
                                              , Optional ByVal blnOnlyShortlistedEmployee As Boolean = False _
                                              , Optional ByVal sFilter As String = "" _
                                              ) As DataTable
        Dim StrQ As String = ""
        Dim strErrorMessage As String = ""
        Dim dsList As New DataSet
        Dim exForce As Exception
        Try
            Dim objDataOperation As New clsDataOperation


            StrQ = "SELECT DISTINCT " & _
                      "  0 AS IsChecked " & _
                      ", hrapps_finalemployee.shortlistunkid " & _
                      ", hrapps_finalemployee.employeeunkid " & _
                      ", ISNULL(hremployee_master.employeecode, '') AS employeecode " & _
                      ", ISNULL(hremployee_master.firstname, '') + ' ' + ISNULL(hremployee_master.othername, '') + ' ' + ISNULL(hremployee_master.surname, '') AS employeename " & _
                      ", CASE hremployee_master.gender WHEN 1 THEN @Male WHEN 2 THEN @Female ELSE '' END AS Gender " & _
                      ", CONVERT(CHAR(8), ISNULL(hremployee_master.birthdate, '01-Jan-1900'), 112) AS birthdate " & _
                      ", CONVERT(CHAR(8), ISNULL(hremployee_master.appointeddate, '01-Jan-1900'), 112) AS appointeddate " & _
                      ", ISNULL(hremployee_master.present_mobile, '') AS Phone " & _
                      ", ISNULL(hremployee_master.email, '') AS email " & _
                      ", ISNULL(hrapps_finalemployee.periodunkid, 0 ) AS periodunkid " & _
                      ", hrapps_finalemployee.isshortlisted " & _
                      ", hrapps_finalemployee.isfinalshortlisted " & _
                      ", ISNULL(hrapps_finalemployee.operationmodeid,0) AS operationmodeid " & _
                      ", ISNULL(cfcommon_master.name,'') AS operationmode " & _
                      ", hrapps_finalemployee.userunkid " & _
                      ", hrapps_finalemployee.isvoid " & _
                      ", hrapps_finalemployee.voiduserunkid " & _
                      ", hrapps_finalemployee.voiddatetime " & _
                      ", hrapps_finalemployee.voidreason " & _
                      ", '' AS AUD " & _
                      ", ISNULL(hrapps_finalemployee.salaryincrementtranunkid, 0) AS salaryincrementtranunkid " & _
                      ",ISNULL(hrapps_finalemployee.edunkid,0) AS edunkid " & _
                      ", hrjob_master.job_name AS Job_Title " & _
                      ", hrdepartment_master.name AS Dept " & _
                      ", hrapps_finalemployee.appratingunkid " & _
                      ", hrapps_finalemployee.apprperiodunkid " & _
                      ", CASE WHEN ISNULL(hrapps_finalemployee.salaryincrementtranunkid, 0) > 0 THEN @SL " & _
                      "       WHEN ISNULL(hrapps_finalemployee.edunkid, 0) > 0 THEN @ED  " & _
                      "  ELSE '' END AS Operation " & _
                      ", CASE WHEN ISNULL(hrapps_finalemployee.salaryincrementtranunkid, 0) > 0 THEN " & CInt(enAppraisal_Modes.SALARY_INCREMENT) & " " & _
                      "       WHEN ISNULL(hrapps_finalemployee.edunkid, 0) > 0 THEN " & CInt(enAppraisal_Modes.BONUS) & " " & _
                      "  ELSE 0 END AS OperationId " & _
                "FROM    hrapps_finalemployee " & _
                      "LEFT JOIN cfcommon_master ON cfcommon_master.masterunkid = hrapps_finalemployee.operationmodeid AND mastertype ='" & clsCommon_Master.enCommonMaster.APPRAISAL_ACTIONS & "'  " & _
                        "JOIN hremployee_master ON hremployee_master.employeeunkid = hrapps_finalemployee.employeeunkid " & _
                      "JOIN hrdepartment_master ON hrdepartment_master.departmentunkid  = hremployee_master.departmentunkid " & _
                      "JOIN hrjob_master ON hremployee_master.jobunkid = hrjob_master.jobunkid " & _
                        "JOIN hrapps_shortlist_master ON hrapps_shortlist_master.shortlistunkid = hrapps_finalemployee.shortlistunkid " & _
                "WHERE   ISNULL(hrapps_finalemployee.isvoid, 0) = 0 "

            'S.SANDEEP [12-JUL-2018] -- START
            'ISSUE/ENHANCEMENT : {Ref#223|ARUTI-239}
            '", CASE WHEN ISNULL(hrapps_finalemployee.salaryincrementtranunkid, 0) > 0 THEN @YES ELSE '' END AS Operation " & _ -- REMOVED
            '", CASE WHEN ISNULL(hrapps_finalemployee.salaryincrementtranunkid, 0) > 0 THEN 1 ELSE 0 END AS OperationId " & _   -- REMOVED

            '-------- ADDED -- START
            ',ISNULL(hrapps_finalemployee.edunkid,0) AS edunkid

            '", CASE WHEN ISNULL(hrapps_finalemployee.salaryincrementtranunkid, 0) > 0 THEN @SL " & _
            '"       WHEN ISNULL(hrapps_finalemployee.edunkid, 0) > 0 THEN @ED  " & _
            '"  ELSE '' END AS Operation " & _
            '", CASE WHEN ISNULL(hrapps_finalemployee.salaryincrementtranunkid, 0) > 0 THEN " & CInt(enAppraisal_Modes.SALARY_INCREMENT) & " " & _
            '"       WHEN ISNULL(hrapps_finalemployee.edunkid, 0) > 0 THEN " & CInt(enAppraisal_Modes.BONUS) & " " & _
            '"  ELSE 0 END AS OperationId " & _
            '-------- ADDED -- END
            'S.SANDEEP [12-JUL-2018] -- END


            '   'Sohail (29 Dec 2012) - [salaryincrementtranunkid]
            'S.SANDEEP [ 14 AUG 2013 ] -- START {appratingunkid,apprperiodunkid} -- END


            'S.SANDEEP [ 14 AUG 2013 ] -- START
            'ENHANCEMENT : TRA CHANGES -- ADDED
            '   ", hrjob_master.job_name AS Job_Title "
            '   ", hrdepartment_master.name AS Dept "
            '   "JOIN hrdepartment_master ON hrdepartment_master.departmentunkid  = hremployee_master.departmentunkid "
            '   "JOIN hrjob_master ON hremployee_master.jobunkid = hrjob_master.jobunkid "
            '   ", CASE WHEN ISNULL(hrapps_finalemployee.salaryincrementtranunkid, 0) > 0 THEN @YES ELSE '' END AS Operation "
            '   ", CASE WHEN ISNULL(hrapps_finalemployee.salaryincrementtranunkid, 0) > 0 THEN 1 ELSE 0 END AS OperationId "
            'S.SANDEEP [ 14 AUG 2013 ] -- END


            If intEmpUnkID > 0 Then
                StrQ &= " AND hrapps_finalemployee.employeeunkid = " & intEmpUnkID & " "
            End If

            If intShortListUnkid > 0 Then
                StrQ &= " AND hrapps_shortlist_master.shortlistunkid = " & intShortListUnkid & " "
            End If

            If strReferenceNo.Trim.Length > 0 Then
                StrQ &= " AND hrapps_shortlist_master.referenceno = '" & strReferenceNo & "' "
            End If

            If intFilterUnkid > 0 Then
                StrQ &= " AND hrapps_finalemployee.filterunkid = " & intFilterUnkid & " "
            End If

            If blnOnlyShortlistedEmployee = True Then
                StrQ &= " AND hrapps_finalemployee.isshortlisted = 1 "
            End If

            'S.SANDEEP [ 14 AUG 2013 ] -- START
            'ENHANCEMENT : TRA CHANGES
            If sFilter.Trim.Length > 0 Then
                StrQ &= " AND " & sFilter
            End If

            'StrQ &= " AND hrapps_finalemployee.employeeunkid NOT IN (SELECT DISTINCT employeeunkid " & _
            '        "   FROM hrapps_finalemployee " & _
            '        " WHERE isvoid = 0 AND apprperiodunkid IN (SELECT periodunkid " & _
            '        "   FROM hrapps_shortlist_master " & _
            '        " WHERE shortlistunkid = " & intShortListUnkid & " AND isvoid = 0)) "
            'S.SANDEEP [ 14 AUG 2013 ] -- END

            objDataOperation.AddParameter("@MALE", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 1, "Male"))
            objDataOperation.AddParameter("@FEMALE", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 2, "Female"))

            'S.SANDEEP [12-JUL-2018] -- START
            'ISSUE/ENHANCEMENT : {Ref#223|ARUTI-239}
            'objDo.AddParameter("@YES", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 3, "Yes"))
            objDataOperation.AddParameter("@SL", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 4, "Salary Increment"))
            objDataOperation.AddParameter("@ED", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 5, "Bonus"))
            'S.SANDEEP [12-JUL-2018] -- END


            dsList = objDataOperation.ExecQuery(StrQ, "DataTable")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & " : " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            mdtTran.Rows.Clear()
            mdtTran = New DataView(dsList.Tables("DataTable")).ToTable

            'S.SANDEEP [ 14 AUG 2013 ] -- START
            'ENHANCEMENT : TRA CHANGES
            'mdtTran.Columns.Add("operationmode", System.Type.GetType("System.String")).DefaultValue = ""

            'Dim objMaster As New clsMasterData
            'dsList = objMaster.getComboListAppraisalMode("Mode", False)
            'Dim objDic As New Dictionary(Of Integer, String)
            'For Each dsRow As DataRow In dsList.Tables("Mode").Rows
            '    objDic.Add(CInt(dsRow.Item("Id")), dsRow.Item("Name"))
            'Next
            'objMaster = Nothing

            'For Each dtRow As DataRow In mdtTran.Select("operationmodeid > 0")
            '    dtRow.Item("operationmode") = objDic.Item(CInt(dtRow.Item("operationmodeid")))
            'Next
            'S.SANDEEP [ 14 AUG 2013 ] -- END





            Return mdtTran

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: GetShortListFinalEmployee; Module Name: " & mstrModuleName)
        Finally
        End Try
    End Function

    'S.SANDEEP [12-JUL-2018] -- START
    'ISSUE/ENHANCEMENT : {Ref#223|ARUTI-239}
    Public Function UpdateEarningDeductionId(ByVal intEmployeeId As Integer, _
                                             ByVal intShortListId As Integer, _
                                             ByVal intEDUnkid As Integer, _
                                             ByVal intPeriodId As Integer, _
                                             ByVal blnFromEarningDeduction As Boolean, _
                                             ByVal intUserId As Integer, _
                                             Optional ByVal xDataOpr As clsDataOperation = Nothing) As Boolean
        Dim StrQ As String = ""
        Dim strQuery As String = ""
        Dim exForce As Exception
        Dim objDataoperation As clsDataOperation = Nothing
        If xDataOpr Is Nothing Then
            objDataoperation = New clsDataOperation
        Else
            objDataoperation = xDataOpr
        End If
        objDataoperation.ClearParameters()
        If xDataOpr Is Nothing Then
            objDataoperation.BindTransaction()
        End If
        Try
            If blnFromEarningDeduction Then

                StrQ = "UPDATE hrapps_finalemployee SET " & _
                       "  periodunkid = 0 " & _
                       ", edunkid = 0 " & _
                       "WHERE edunkid = @edunkid "

                strQuery = "SELECT ISNULL(finalemployeeunkid,0) AS finalemployeeunkid From hrapps_finalemployee " & _
                           "WHERE edunkid = @edunkid "
            Else

                StrQ = "UPDATE hrapps_finalemployee SET " & _
                       "  isfinalshortlisted = @isfinalshortlisted " & _
                       ", periodunkid = @periodunkid " & _
                       ", edunkid = @edunkid " & _
                       "WHERE shortlistunkid = @shortlistunkid AND employeeunkid = @employeeunkid "

                strQuery = "SELECT ISNULL(finalemployeeunkid,0) AS finalemployeeunkid From hrapps_finalemployee " & _
                           "WHERE shortlistunkid = @shortlistunkid AND employeeunkid = @employeeunkid"

            End If

            objDataoperation.ClearParameters()
            objDataoperation.AddParameter("@isfinalshortlisted", SqlDbType.Bit, eZeeDataType.BIT_SIZE, True)
            objDataoperation.AddParameter("@shortlistunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intShortListId)
            objDataoperation.AddParameter("@employeeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intEmployeeId)
            objDataoperation.AddParameter("@periodunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intPeriodId)
            objDataoperation.AddParameter("@edunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intEDUnkid)

            objDataoperation.ExecNonQuery(StrQ)

            If objDataoperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataoperation.ErrorNumber & " :  " & objDataoperation.ErrorMessage)
                Throw exForce
            End If


            Dim dsList As DataSet = objDataoperation.ExecQuery(strQuery, "List")
            If dsList.Tables(0).Rows.Count > 0 Then

                For Each dr As DataRow In dsList.Tables(0).Rows
                    Dim objCommonATLog As New clsCommonATLog
                    With objCommonATLog
                        ._FormName = mstrFormName
                        ._LoginEmployeeUnkid = mintLoginEmployeeunkid
                        ._ClientIP = mstrClientIP
                        ._HostName = mstrHostName
                        ._FromWeb = mblnIsWeb
                        ._AuditUserId = mintAuditUserId
                        ._CompanyUnkid = mintCompanyUnkid
                        ._AuditDate = mdtAuditDate
                    End With
                    If objCommonATLog.Insert_AtLog(objDataoperation, 2, "hrapps_finalemployee", "finalemployeeunkid", CInt(dr("finalemployeeunkid")), False, intUserId) = False Then
                        exForce = New Exception(objDataoperation.ErrorNumber & ": " & objDataoperation.ErrorMessage)
                        Throw exForce
                    End If
                    objCommonATLog = Nothing
                Next

            End If

            If xDataOpr Is Nothing Then
                objDataoperation.ReleaseTransaction(True)
            End If

            Return True
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: UpdateEarningDeductionId; Module Name: " & mstrModuleName)
        Finally
            If xDataOpr Is Nothing Then objDataoperation = Nothing
        End Try
    End Function
    'S.SANDEEP [12-JUL-2018] -- END

    Public Function Set_FinalEmployee(ByVal mdtFinalEmployee As DataTable) As Boolean
        Dim StrQ As String = ""
        Dim exForce As Exception

        Dim objDataoperation As New clsDataOperation
        objDataoperation.BindTransaction()

        Try
            StrQ = "UPDATE hrapps_finalemployee SET " & _
                            "  isfinalshortlisted = @isfinalshortlisted " & _
                            ", operationmodeid = @operationmodeid " & _
                            ", periodunkid = @periodunkid " & _
                            ", salaryincrementtranunkid = @salaryincrementtranunkid " & _
                            "WHERE shortlistunkid = @shortlistunkid AND employeeunkid = @employeeunkid"
            '               'Sohail (29 Dec 2012) - [salaryincrementtranunkid]

            Dim strQuery = "Select ISNULL(finalemployeeunkid,0) AS finalemployeeunkid From hrapps_finalemployee " & _
                                    "WHERE shortlistunkid = @shortlistunkid AND employeeunkid = @employeeunkid"

            If mdtFinalEmployee IsNot Nothing AndAlso mdtFinalEmployee.Rows.Count > 0 Then

                For Each dtRow As DataRow In mdtFinalEmployee.Rows

                    objDataoperation.ClearParameters()
                    objDataoperation.AddParameter("@isfinalshortlisted", SqlDbType.Bit, eZeeDataType.BIT_SIZE, True)
                    objDataoperation.AddParameter("@operationmodeid", SqlDbType.Int, eZeeDataType.INT_SIZE, dtRow("operationmodeid").ToString())
                    objDataoperation.AddParameter("@shortlistunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, dtRow("shortlistunkid").ToString())
                    objDataoperation.AddParameter("@employeeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, dtRow("employeeunkid").ToString())
                    objDataoperation.AddParameter("@periodunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, dtRow("periodunkid").ToString())
                    objDataoperation.AddParameter("@salaryincrementtranunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, dtRow("salaryincrementtranunkid").ToString()) 'Sohail (29 Dec 2012)

                    objDataoperation.ExecNonQuery(StrQ)

                    If objDataoperation.ErrorMessage <> "" Then
                        exForce = New Exception(objDataoperation.ErrorNumber & " :  " & objDataoperation.ErrorMessage)
                        Throw exForce
                    End If


                    Dim dsList As DataSet = objDataoperation.ExecQuery(strQuery, "List")
                    If dsList.Tables(0).Rows.Count > 0 Then

                        For Each dr As DataRow In dsList.Tables(0).Rows

                            'S.SANDEEP [28-May-2018] -- START
                            'ISSUE/ENHANCEMENT : {Audit Trails} 
                            Dim objCommonATLog As New clsCommonATLog
                            objCommonATLog._FormName = mstrFormName
                            objCommonATLog._LoginEmployeeUnkid = mintLoginEmployeeunkid
                            objCommonATLog._ClientIP = mstrClientIP
                            objCommonATLog._HostName = mstrHostName
                            objCommonATLog._FromWeb = mblnIsWeb
                            objCommonATLog._AuditUserId = mintAuditUserId
                            objCommonATLog._CompanyUnkid = mintCompanyUnkid
                            objCommonATLog._AuditDate = mdtAuditDate
                            'S.SANDEEP [28-May-2018] -- END

                            If objCommonATLog.Insert_AtLog(objDataoperation, 2, "hrapps_finalemployee", "finalemployeeunkid", CInt(dr("finalemployeeunkid"))) = False Then
                                exForce = New Exception(objDataoperation.ErrorNumber & ": " & objDataoperation.ErrorMessage)
                                Throw exForce
                            End If

                            'S.SANDEEP [28-May-2018] -- START
                            'ISSUE/ENHANCEMENT : {Audit Trails} 
                            objCommonATLog = Nothing
                            'S.SANDEEP [28-May-2018] -- END

                        Next

                    End If

                Next

            End If


            objDataoperation.ReleaseTransaction(True)
            Return True

        Catch ex As Exception
            objDataoperation.ReleaseTransaction(False)
            Throw New Exception(ex.Message & "; Procedure Name: Set_FinalEmployee; Module Name: " & mstrModuleName)
        Finally
        End Try
    End Function
    'Sohail (10 Feb 2012) -- End

    'S.SANDEEP [ 14 AUG 2013 ] -- START
    'ENHANCEMENT : TRA CHANGES
    Public Function InsertByGrades(ByVal dTable As DataTable) As Boolean
        Dim blnFlag As Boolean = False
        Dim objDataOperation As New clsDataOperation
        Try
            objDataOperation.BindTransaction()
            Dim dView As DataView = dTable.DefaultView
            dView.RowFilter = "AUD IN('A','U')"
            mdtTran = dView.ToTable
            _FormName = mstrFormName
            _LoginEmployeeunkid = mintLoginEmployeeunkid
            _ClientIP = mstrClientIP
            _HostName = mstrHostName
            _FromWeb = mblnIsWeb
            _AuditUserId = mintAuditUserId
            _AuditDate = mdtAuditDate
            blnFlag = Insert_ShortListedEmployee(objDataOperation, True)
            If blnFlag = True Then
                objDataOperation.ReleaseTransaction(True)
            End If
            Return blnFlag
        Catch ex As Exception
            objDataOperation.ReleaseTransaction(False)
            Throw New Exception(ex.Message & " Procedure : InsertByGrades " & mstrModuleName)
        Finally
        End Try
    End Function
    'S.SANDEEP [ 14 AUG 2013 ] -- END

    Public Function Insert_ShortListedEmployee(Optional ByVal objDataOperation As clsDataOperation = Nothing, Optional ByVal blnByGrades As Boolean = False) As Boolean 'S.SANDEEP [ 14 AUG 2013 ] -- START -- END
        'Public Function Insert_ShortListedEmployee(Optional ByVal objDataOperation As clsDataOperation = Nothing) As Boolean
        Dim i As Integer
        Dim strQ As String = ""
        Dim exForce As Exception
        Try
            For i = 0 To mdtTran.Rows.Count - 1
                With mdtTran.Rows(i)
                    objDataOperation.ClearParameters()
                    Select Case .Item("AUD")
                        Case "A"
                    strQ = "INSERT INTO hrapps_finalemployee ( " & _
                                "  shortlistunkid " & _
                                ", filterunkid " & _
                                ", employeeunkid " & _
                                ", periodunkid " & _
                                ", isshortlisted " & _
                                ", isfinalshortlisted " & _
                                        ", operationmodeid " & _
                                ", userunkid " & _
                                ", isvoid " & _
                                ", voiduserunkid " & _
                                ", voiddatetime " & _
                                ", voidreason" & _
                                ", appratingunkid " & _
                                ", apprperiodunkid " & _
                             ") VALUES (" & _
                                "  @shortlistunkid " & _
                                ", @filterunkid " & _
                                ", @employeeunkid " & _
                                ", @periodunkid " & _
                                ", @isshortlisted " & _
                                ", @isfinalshortlisted " & _
                                ", @operationmodeid " & _
                                ", @userunkid " & _
                                ", @isvoid " & _
                                ", @voiduserunkid " & _
                                ", @voiddatetime " & _
                                ", @voidreason" & _
                                ", @appratingunkid " & _
                                ", @apprperiodunkid " & _
                              "); SELECT @@identity "
                            objDataOperation.ClearParameters()
                    objDataOperation.AddParameter("@shortlistunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintShortListUnkid.ToString)
                    objDataOperation.AddParameter("@filterunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintFilterUnkid.ToString)
                    objDataOperation.AddParameter("@employeeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, .Item("employeeunkid"))
                    objDataOperation.AddParameter("@periodunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, 0)
                    objDataOperation.AddParameter("@isshortlisted", SqlDbType.Bit, eZeeDataType.BIT_SIZE, True)
                            'S.SANDEEP [ 14 AUG 2013 ] -- START
                            'ENHANCEMENT : TRA CHANGES
                            'objDataOperation.AddParameter("@isfinalshortlisted", SqlDbType.Bit, eZeeDataType.BIT_SIZE, False)
                            If blnByGrades = True Then
                                objDataOperation.AddParameter("@isfinalshortlisted", SqlDbType.Bit, eZeeDataType.BIT_SIZE, True)
                                objDataOperation.AddParameter("@operationmodeid", SqlDbType.Int, eZeeDataType.INT_SIZE, .Item("operationmodeid"))
                            Else
                    objDataOperation.AddParameter("@isfinalshortlisted", SqlDbType.Bit, eZeeDataType.BIT_SIZE, False)
                                objDataOperation.AddParameter("@operationmodeid", SqlDbType.Int, eZeeDataType.INT_SIZE, DBNull.Value)
                            End If
                            'S.SANDEEP [ 14 AUG 2013 ] -- END
                    objDataOperation.AddParameter("@userunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, User._Object._Userunkid)
                    objDataOperation.AddParameter("@isvoid", SqlDbType.Bit, eZeeDataType.BIT_SIZE, False)
                    objDataOperation.AddParameter("@voiduserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, -1)
                    objDataOperation.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, DBNull.Value)
                    objDataOperation.AddParameter("@voidreason", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, "")
                            'S.SANDEEP [ 14 AUG 2013 ] -- START
                            'ENHANCEMENT : TRA CHANGES
                            If blnByGrades = True Then
                                objDataOperation.AddParameter("@appratingunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, .Item("appratingunkid"))
                                objDataOperation.AddParameter("@apprperiodunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, .Item("apprperiodunkid"))
                            Else
                                objDataOperation.AddParameter("@appratingunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, 0)
                                objDataOperation.AddParameter("@apprperiodunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, 0)
                            End If
                            'S.SANDEEP [ 14 AUG 2013 ] -- END

                    Dim dsList As New DataSet
                    dsList = objDataOperation.ExecQuery(strQ, "List")

                    If objDataOperation.ErrorMessage <> "" Then
                        exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                        Throw exForce
                    End If

                    mintSFEmpTranUnkid = dsList.Tables(0).Rows(0).Item(0)

                            'S.SANDEEP [28-May-2018] -- START
                            'ISSUE/ENHANCEMENT : {Audit Trails} 
                            Dim objCommonATLog As New clsCommonATLog
                            objCommonATLog._FormName = mstrFormName
                            objCommonATLog._LoginEmployeeUnkid = mintLoginEmployeeunkid
                            objCommonATLog._ClientIP = mstrClientIP
                            objCommonATLog._HostName = mstrHostName
                            objCommonATLog._FromWeb = mblnIsWeb
                            objCommonATLog._AuditUserId = mintAuditUserId
                            objCommonATLog._CompanyUnkid = mintCompanyUnkid
                            objCommonATLog._AuditDate = mdtAuditDate
                            'S.SANDEEP [28-May-2018] -- END

                            If objCommonATLog.Insert_TranAtLog(objDataOperation, "hrapps_shortlist_master", "shortlistunkid", mintShortListUnkid, "hrapps_finalemployee", "finalemployeeunkid", mintSFEmpTranUnkid, 1, 1) = False Then
                        exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                        Throw exForce
                    End If

                            'S.SANDEEP [28-May-2018] -- START
                            'ISSUE/ENHANCEMENT : {Audit Trails} 
                            objCommonATLog = Nothing
                            'S.SANDEEP [28-May-2018] -- END

                        Case "U"
                            Dim dtmp(0) As DataRow
                            dtmp(0) = mdtTran.Rows(i)
                            Dim mdtTable As DataTable = dtmp.CopyToDataTable
                            If Set_FinalEmployee(mdtTable) = False Then
                                If objDataOperation.ErrorMessage <> "" Then
                                    exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                                    Throw exForce
                                End If
                            End If
                    End Select
                End With
            Next

            Return True

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: Insert_ShortListedEmployee; Module Name: " & mstrModuleName)
        Finally
        End Try
    End Function

    'S.SANDEEP [ 14 AUG 2013 ] -- START
    'ENHANCEMENT : TRA CHANGES
    Public Function GetEmployee_ByAward(ByVal mDecScrFrm As Decimal, _
                                        ByVal mDecScrTo As Decimal, _
                                        ByVal iPeriodId As Integer, _
                                        ByVal iAction As String, _
                                        ByVal iActionId As Integer, _
                                        ByVal iRatingId As Integer, _
                                        ByVal sFilter As String) As DataTable
        Dim StrQ As String = ""
        Dim strErrorMessage As String = ""
        Dim dsList As New DataSet
        Dim exForce As Exception
        Try
            Dim objDataOperation As New clsDataOperation

            'S.SANDEEP [ 04 MAR 2014 ] -- START
            Dim iStringValue As String = String.Empty
            'S.SANDEEP [ 30 MAY 2014 ] -- START
            'Dim objWSetting As New clsWeight_Setting(True)
            Dim objWSetting As New clsWeight_Setting(iPeriodId)
            'S.SANDEEP [ 30 MAY 2014 ] -- END
            If objWSetting._Weight_Typeid <= 0 Then
                iStringValue = " AND kpiunkid > 0 AND targetunkid > 0 AND initiativeunkid  > 0  "
            ElseIf objWSetting._Weight_Typeid = enWeight_Types.WEIGHT_FIELD1 Then
                iStringValue = " AND kpiunkid <= 0 AND targetunkid <= 0 AND initiativeunkid <= 0 "
            ElseIf objWSetting._Weight_Typeid = enWeight_Types.WEIGHT_FIELD2 Then
                iStringValue = " AND kpiunkid > 0 "
            ElseIf objWSetting._Weight_Typeid = enWeight_Types.WEIGHT_FIELD3 Then
                iStringValue = " AND targetunkid > 0 "
            ElseIf objWSetting._Weight_Typeid = enWeight_Types.WEIGHT_FIELD5 Then
                iStringValue = " AND initiativeunkid > 0 "
            End If
            'S.SANDEEP [ 04 MAR 2014 ] -- END

            StrQ = "SELECT " & _
                    "	 0 AS IsChecked " & _
                    "	,0 AS shortlistunkid " & _
                    "	,hremployee_master.employeeunkid " & _
                    "	,ISNULL(hremployee_master.employeecode, '') AS employeecode " & _
                    "	,ISNULL(hremployee_master.firstname, '') + ' ' + ISNULL(hremployee_master.othername, '') + ' ' + ISNULL(hremployee_master.surname, '') AS employeename " & _
                    "	,CASE hremployee_master.gender WHEN 1 THEN @MALE WHEN 2 THEN @FEMALE ELSE '' END AS Gender " & _
                    "	,CONVERT(CHAR(8), ISNULL(hremployee_master.birthdate, '01-Jan-1900'), 112) AS birthdate " & _
                    "	,CONVERT(CHAR(8), ISNULL(hremployee_master.appointeddate, '01-Jan-1900'), 112) AS appointeddate " & _
                    "	,ISNULL(hremployee_master.present_mobile, '') AS Phone " & _
                    "	,ISNULL(hremployee_master.email, '') AS email " & _
                    "	,CASE WHEN ISNULL(hrapps_finalemployee.periodunkid,0) = 0 THEN 0 ELSE ISNULL(hrapps_finalemployee.periodunkid,0) END AS periodunkid " & _
                    "	,1 AS isshortlisted " & _
                    "	,ISNULL(hrapps_finalemployee.isfinalshortlisted,0) AS isfinalshortlisted " & _
                    "	,CASE WHEN ISNULL(hrapps_finalemployee.operationmodeid,0) = 0 THEN " & iActionId & " ELSE ISNULL(hrapps_finalemployee.operationmodeid,0) END AS operationmodeid " & _
                    "	,hrapps_finalemployee.userunkid " & _
                    "	,hrapps_finalemployee.isvoid " & _
                    "	,hrapps_finalemployee.voiddatetime " & _
                    "	,hrapps_finalemployee.voidreason " & _
                    "	,'' AS AUD " & _
                    "	,ISNULL(hrapps_finalemployee.salaryincrementtranunkid, 0) AS salaryincrementtranunkid " & _
                    "	,hrjob_master.job_name AS Job_Title " & _
                    "	,hrdepartment_master.name AS Dept " & _
                    "   ,CASE WHEN ISNULL(hrapps_finalemployee.operationmodeid,0) = 0 THEN '" & iAction & "' ELSE ISNULL(cfcommon_master.name,'') END AS operationmode " & _
                    "   ,CASE WHEN ISNULL(hrapps_finalemployee.appratingunkid,0) = 0 THEN '" & iRatingId & "' ELSE ISNULL(hrapps_finalemployee.appratingunkid,0) END AS appratingunkid " & _
                    "   ,CASE WHEN ISNULL(hrapps_finalemployee.apprperiodunkid,0) = 0 THEN '" & iPeriodId & "' ELSE ISNULL(hrapps_finalemployee.apprperiodunkid,0) END AS apprperiodunkid " & _
                    "   ,CASE WHEN ISNULL(hrapps_finalemployee.salaryincrementtranunkid, 0) > 0 THEN @YES ELSE '' END AS Operation " & _
                    "   ,CASE WHEN ISNULL(hrapps_finalemployee.salaryincrementtranunkid, 0) > 0 THEN 1 ELSE 0 END AS OperationId "

            Select Case ConfigParameter._Object._PerformanceComputation
                Case 1 'Weight Avg Of
                    If ConfigParameter._Object._WAvgEmpComputationType = True AndAlso ConfigParameter._Object._WAvgAseComputType = True AndAlso ConfigParameter._Object._WAvgRevComputType = True Then
                        StrQ &= ",CAST((ISNULL(S_GA.S_GA,0) + ISNULL(A_GA.A_GA,0) + ISNULL(R_GA.R_GA,0))/3 * (ge_weight/100)+(ISNULL(S_BSC.S_BSC,0)+ ISNULL(A_BSC.A_BSC,0) + ISNULL(R_BSC.R_BSC,0))/3 *(bsc_weight/100) AS DECIMAL(10,2)) AS Score "
                    ElseIf ConfigParameter._Object._WAvgEmpComputationType = True AndAlso ConfigParameter._Object._WAvgAseComputType = True AndAlso ConfigParameter._Object._WAvgRevComputType = False Then
                        StrQ &= ",CAST((ISNULL(S_GA.S_GA,0) + ISNULL(A_GA.A_GA,0))/2 * (ge_weight/100)+(ISNULL(S_BSC.S_BSC,0)+ ISNULL(A_BSC.A_BSC,0))/2 *(bsc_weight/100) AS DECIMAL(10,2)) AS Score "
                    ElseIf ConfigParameter._Object._WAvgEmpComputationType = True AndAlso ConfigParameter._Object._WAvgAseComputType = False AndAlso ConfigParameter._Object._WAvgRevComputType = True Then
                        StrQ &= ",CAST((ISNULL(S_GA.S_GA,0) + ISNULL(R_GA.R_GA,0))/2 * (ge_weight/100)+(ISNULL(S_BSC.S_BSC,0) + ISNULL(R_BSC.R_BSC,0))/2 *(bsc_weight/100) AS DECIMAL(10,2)) AS Score "
                    ElseIf ConfigParameter._Object._WAvgEmpComputationType = False AndAlso ConfigParameter._Object._WAvgAseComputType = True AndAlso ConfigParameter._Object._WAvgRevComputType = True Then
                        StrQ &= ",CAST((ISNULL(A_GA.A_GA,0) + ISNULL(R_GA.R_GA,0))/2 * (ge_weight/100)+(ISNULL(A_BSC.A_BSC,0) + ISNULL(R_BSC.R_BSC,0))/2 *(bsc_weight/100) AS DECIMAL(10,2)) AS Score "
                    ElseIf ConfigParameter._Object._WAvgEmpComputationType = True AndAlso ConfigParameter._Object._WAvgAseComputType = False AndAlso ConfigParameter._Object._WAvgRevComputType = False Then
                        StrQ &= ",CAST((ISNULL(S_GA.S_GA,0)) * (ge_weight/100) + (ISNULL(S_BSC.S_BSC,0)) * (bsc_weight/100) AS DECIMAL(10,2)) AS Score "
                    ElseIf ConfigParameter._Object._WAvgEmpComputationType = False AndAlso ConfigParameter._Object._WAvgAseComputType = True AndAlso ConfigParameter._Object._WAvgRevComputType = False Then
                        StrQ &= ",CAST((ISNULL(A_GA.A_GA,0)) * (ge_weight/100)+(ISNULL(A_BSC.A_BSC,0)) * (bsc_weight/100) AS DECIMAL(10,2)) AS Score "
                    ElseIf ConfigParameter._Object._WAvgEmpComputationType = False AndAlso ConfigParameter._Object._WAvgAseComputType = False AndAlso ConfigParameter._Object._WAvgRevComputType = True Then
                        StrQ &= ",CAST((ISNULL(R_GA.R_GA,0)) * (ge_weight/100)+(ISNULL(R_BSC.R_BSC,0))*(bsc_weight/100) AS DECIMAL(10,2)) AS Score "
                    End If
                Case 2 'Weight Score Of
                    If ConfigParameter._Object._WScrComputType = 1 Then
                        StrQ &= ",CAST((ISNULL(S_GA.S_GA,0)) * (ge_weight/100) + (ISNULL(S_BSC.S_BSC,0)) * (bsc_weight/100) AS DECIMAL(10,2)) AS Score "
                    ElseIf ConfigParameter._Object._WScrComputType = 2 Then
                        StrQ &= ",CAST((ISNULL(A_GA.A_GA,0)) * (ge_weight/100)+(ISNULL(A_BSC.A_BSC,0)) * (bsc_weight/100) AS DECIMAL(10,2)) AS Score "
                    ElseIf ConfigParameter._Object._WScrComputType = 3 Then
                        StrQ &= ",CAST((ISNULL(R_GA.R_GA,0)) * (ge_weight/100)+(ISNULL(R_BSC.R_BSC,0))*(bsc_weight/100) AS DECIMAL(10,2)) AS Score "
                    End If
            End Select

            'S.SANDEEP [ 05 NOV 2014 ] -- START
            'StrQ &= "FROM hremployee_master " & _
            '        "LEFT JOIN hrapps_finalemployee ON hremployee_master.employeeunkid = hrapps_finalemployee.employeeunkid AND shortlistunkid <= 0  AND isvoid = 0 " & _
            '        "LEFT JOIN cfcommon_master ON cfcommon_master.masterunkid = hrapps_finalemployee.operationmodeid AND mastertype ='" & clsCommon_Master.enCommonMaster.APPRAISAL_ACTIONS & "'  " & _
            '        "JOIN hrassessment_ratio ON hremployee_master.jobgroupunkid = hrassessment_ratio.jobgroupunkid AND hrassessment_ratio.periodunkid = '" & iPeriodId & "' AND hrassessment_ratio.isactive = 1 " & _
            '        "JOIN hrdepartment_master ON hremployee_master.departmentunkid = hrdepartment_master.departmentunkid " & _
            '        "JOIN hrjob_master ON hremployee_master.jobunkid = hrjob_master.jobunkid " & _
            '        "LEFT JOIN " & _
            '        "( " & _
            '             "SELECT " & _
            '                  " selfemployeeunkid AS EmpId " & _
            '                  ",ISNULL(SUM(SELF),0) AS S_GA " & _
            '             "FROM hrassess_analysis_master " & _
            '                  "JOIN " & _
            '                  "( " & _
            '                       "SELECT " & _
            '                            " hrassess_analysis_tran.analysisunkid " & _
            '                            ",CAST(ISNULL(CAST(SUM(CASE WHEN ISNUMERIC(resultname) <> 0 THEN resultname * " & IIf(ConfigParameter._Object._ConsiderItemWeightAsNumber = True, "CAST((hrassess_item_master.weight) AS DECIMAL(10,2))", "CAST((hrassess_item_master.weight/100) AS DECIMAL(10,2))") & " ELSE CAST(0 AS DECIMAL(10,2)) END) AS DECIMAL(10, 2)),0) * (hrassess_group_master.weight/100) AS DECIMAL(10,2)) AS SELF " & _
            '                       "FROM hrassess_analysis_tran " & _
            '                            "JOIN hrassess_analysis_master ON hrassess_analysis_tran.analysisunkid = hrassess_analysis_master.analysisunkid " & _
            '                            "JOIN hrassess_group_master ON hrassess_analysis_master.assessgroupunkid = hrassess_group_master.assessgroupunkid " & _
            '                            "JOIN hrresult_master ON hrassess_analysis_tran.resultunkid = hrresult_master.resultunkid " & _
            '                            "JOIN hrassess_item_master ON hrassess_item_master.assessitemunkid = hrassess_analysis_tran.assessitemunkid " & _
            '                       "WHERE ISNULL(hrassess_analysis_tran.isvoid,0) = 0 " & _
            '                       "GROUP BY hrassess_analysis_tran.analysisunkid,hrassess_group_master.weight " & _
            '                  ") AS Slf ON hrassess_analysis_master.analysisunkid = Slf.analysisunkid  AND iscommitted = 1 " & _
            '             "WHERE isvoid = 0 AND assessmodeid = '" & enAssessmentMode.SELF_ASSESSMENT & "' AND hrassess_analysis_master.periodunkid = '" & iPeriodId & "' " & _
            '             "GROUP BY selfemployeeunkid " & _
            '        ") AS S_GA ON S_GA.EmpId = hremployee_master.employeeunkid " & _
            '        "LEFT JOIN " & _
            '        "( " & _
            '        "SELECT " & _
            '                   "selfemployeeunkid " & _
            '                  ",SELF AS S_BSC " & _
            '             "FROM hrbsc_analysis_master " & _
            '             "JOIN " & _
            '             "( " & _
            '                  "SELECT " & _
            '                       "analysisunkid " & _
            '                       ",ISNULL(CAST(SUM(CASE WHEN ISNUMERIC(resultname) <> 0 THEN resultname ELSE CAST(0 AS DECIMAL(10,2)) END)AS DECIMAL(10,2)),0) AS SELF " & _
            '                  "FROM hrbsc_analysis_tran " & _
            '                  "JOIN hrresult_master ON hrbsc_analysis_tran.resultunkid = hrresult_master.resultunkid " & _
            '                  "WHERE ISNULL(hrbsc_analysis_tran.isvoid,0) = 0 " & _
            '                  iStringValue & " " & _
            '                  "GROUP BY  analysisunkid " & _
            '             ") AS Slf ON hrbsc_analysis_master.analysisunkid = Slf.analysisunkid AND iscommitted = 1 " & _
            '             "WHERE isvoid = 0 AND assessmodeid = '" & enAssessmentMode.SELF_ASSESSMENT & "' AND hrbsc_analysis_master.periodunkid = '" & iPeriodId & "' " & _
            '        ") AS S_BSC ON S_BSC.selfemployeeunkid = hremployee_master.employeeunkid " & _
            '        "LEFT JOIN " & _
            '        "( " & _
            '             "SELECT " & _
            '                   "assessedemployeeunkid " & _
            '                  ",CAST(SUM(AssessScore)/COUNT(DISTINCT assessormasterunkid) AS DECIMAL(10,2)) AS A_GA " & _
            '             "FROM hrassess_analysis_master " & _
            '             "JOIN " & _
            '             "( " & _
            '                  "SELECT " & _
            '                       " hrassess_analysis_tran.analysisunkid " & _
            '                       ",CAST(ISNULL(CAST(SUM(CASE WHEN ISNUMERIC(resultname) <> 0 THEN resultname *  " & IIf(ConfigParameter._Object._ConsiderItemWeightAsNumber = True, "CAST((hrassess_item_master.weight) AS DECIMAL(10,2))", "CAST((hrassess_item_master.weight/100) AS DECIMAL(10,2))") & "  ELSE CAST(0 AS DECIMAL(10,2)) END) AS DECIMAL(10, 2)),0) * (hrassess_group_master.weight/100) AS DECIMAL(10,2)) AS AssessScore " & _
            '                  "FROM hrassess_analysis_tran " & _
            '                       "JOIN hrassess_analysis_master ON hrassess_analysis_tran.analysisunkid = hrassess_analysis_master.analysisunkid " & _
            '                       "JOIN hrassess_group_master ON hrassess_analysis_master.assessgroupunkid = hrassess_group_master.assessgroupunkid " & _
            '                       "JOIN hrresult_master ON hrassess_analysis_tran.resultunkid = hrresult_master.resultunkid " & _
            '                       "JOIN hrassess_item_master ON hrassess_item_master.assessitemunkid = hrassess_analysis_tran.assessitemunkid " & _
            '                  "WHERE ISNULL(hrassess_analysis_tran.isvoid,0) = 0 GROUP BY hrassess_analysis_tran.analysisunkid,hrassess_group_master.weight " & _
            '             ") AS Slf ON hrassess_analysis_master.analysisunkid = Slf.analysisunkid AND iscommitted = 1 " & _
            '             "WHERE isvoid = 0 AND assessmodeid = '" & enAssessmentMode.APPRAISER_ASSESSMENT & "' AND hrassess_analysis_master.periodunkid ='" & iPeriodId & "' " & _
            '             "GROUP BY assessedemployeeunkid " & _
            '        ") AS A_GA ON A_GA.assessedemployeeunkid = hremployee_master.employeeunkid " & _
            '        "LEFT JOIN " & _
            '        "( " & _
            '             "SELECT " & _
            '                   "assessedemployeeunkid " & _
            '                  ",CAST(SUM(AssessScore)/COUNT(assessedemployeeunkid) AS DECIMAL(10,2)) AS A_BSC " & _
            '             "FROM hrbsc_analysis_master " & _
            '             "JOIN " & _
            '             "( " & _
            '                  "SELECT " & _
            '                        "analysisunkid " & _
            '                       ",ISNULL(CAST(SUM(CASE WHEN ISNUMERIC(resultname) <> 0 THEN resultname ELSE CAST(0 AS DECIMAL(10,2)) END)AS DECIMAL(10,2)),0) AS AssessScore " & _
            '                  "FROM hrbsc_analysis_tran " & _
            '                       "JOIN hrresult_master ON hrbsc_analysis_tran.resultunkid = hrresult_master.resultunkid " & _
            '                  "WHERE ISNULL(hrbsc_analysis_tran.isvoid,0) = 0 " & _
            '                       iStringValue & " " & _
            '                  "GROUP BY  analysisunkid " & _
            '             ") AS Slf ON hrbsc_analysis_master.analysisunkid = Slf.analysisunkid AND iscommitted = 1 " & _
            '             "WHERE isvoid = 0 AND assessmodeid = '" & enAssessmentMode.APPRAISER_ASSESSMENT & "' AND hrbsc_analysis_master.periodunkid = '" & iPeriodId & "' " & _
            '             "GROUP BY assessedemployeeunkid " & _
            '        ") AS A_BSC ON A_BSC.assessedemployeeunkid = hremployee_master.employeeunkid " & _
            '        "LEFT JOIN " & _
            '        "( " & _
            '             "SELECT " & _
            '                   "assessedemployeeunkid " & _
            '                  ",ISNULL(SUM(RevScore),0) AS R_GA " & _
            '             "FROM hrassess_analysis_master " & _
            '             "JOIN " & _
            '             "( " & _
            '                  "SELECT " & _
            '                       " hrassess_analysis_tran.analysisunkid " & _
            '                       ",CAST(ISNULL(CAST(SUM(CASE WHEN ISNUMERIC(resultname) <> 0 THEN resultname *  " & IIf(ConfigParameter._Object._ConsiderItemWeightAsNumber = True, "CAST((hrassess_item_master.weight) AS DECIMAL(10,2))", "CAST((hrassess_item_master.weight/100) AS DECIMAL(10,2))") & "  ELSE CAST(0 AS DECIMAL(10,2)) END) AS DECIMAL(10, 2)),0) * (hrassess_group_master.weight/100) AS DECIMAL(10,2)) AS RevScore " & _
            '                  "FROM hrassess_analysis_tran " & _
            '                       "JOIN hrassess_analysis_master ON hrassess_analysis_tran.analysisunkid = hrassess_analysis_master.analysisunkid " & _
            '                       "JOIN hrassess_group_master ON hrassess_analysis_master.assessgroupunkid = hrassess_group_master.assessgroupunkid " & _
            '                       "JOIN hrresult_master ON hrassess_analysis_tran.resultunkid = hrresult_master.resultunkid " & _
            '                       "JOIN hrassess_item_master ON hrassess_item_master.assessitemunkid = hrassess_analysis_tran.assessitemunkid " & _
            '                  "WHERE ISNULL(hrassess_analysis_tran.isvoid,0) = 0 GROUP BY hrassess_analysis_tran.analysisunkid,hrassess_group_master.weight " & _
            '             ") AS Slf ON hrassess_analysis_master.analysisunkid = Slf.analysisunkid AND iscommitted = 1 " & _
            '             "WHERE isvoid = 0 AND assessmodeid = '" & enAssessmentMode.REVIEWER_ASSESSMENT & "' AND hrassess_analysis_master.periodunkid ='" & iPeriodId & "' " & _
            '             "GROUP BY assessedemployeeunkid " & _
            '        ") AS R_GA ON R_GA.assessedemployeeunkid = hremployee_master.employeeunkid " & _
            '        "LEFT JOIN " & _
            '        "( " & _
            '             "SELECT " & _
            '                   "assessedemployeeunkid " & _
            '                  ",RevScore AS R_BSC " & _
            '             "FROM hrbsc_analysis_master " & _
            '             "JOIN " & _
            '             "( " & _
            '                  "SELECT " & _
            '                        "analysisunkid " & _
            '                       ",ISNULL(CAST(SUM(CASE WHEN ISNUMERIC(resultname) <> 0 THEN resultname ELSE CAST(0 AS DECIMAL(10,2)) END)AS DECIMAL(10,2)),0) AS RevScore " & _
            '                  "FROM hrbsc_analysis_tran " & _
            '                       "JOIN hrresult_master ON hrbsc_analysis_tran.resultunkid = hrresult_master.resultunkid " & _
            '                  "WHERE ISNULL(hrbsc_analysis_tran.isvoid,0) = 0 " & _
            '                       iStringValue & " " & _
            '                  "GROUP BY  analysisunkid " & _
            '             ") AS Slf ON hrbsc_analysis_master.analysisunkid = Slf.analysisunkid AND iscommitted = 1 " & _
            '             "WHERE isvoid = 0 AND assessmodeid = '" & enAssessmentMode.REVIEWER_ASSESSMENT & "' AND hrbsc_analysis_master.periodunkid = '" & iPeriodId & "' " & _
            '        ") AS R_BSC ON R_BSC.assessedemployeeunkid = hremployee_master.employeeunkid " & _
            '        " WHERE 1 = 1 "
            StrQ &= "FROM hremployee_master " & _
                    "LEFT JOIN hrapps_finalemployee ON hremployee_master.employeeunkid = hrapps_finalemployee.employeeunkid AND shortlistunkid <= 0  AND isvoid = 0 " & _
                    "LEFT JOIN cfcommon_master ON cfcommon_master.masterunkid = hrapps_finalemployee.operationmodeid AND mastertype ='" & clsCommon_Master.enCommonMaster.APPRAISAL_ACTIONS & "'  " & _
                    "JOIN hrassessment_ratio ON hremployee_master.jobgroupunkid = hrassessment_ratio.jobgroupunkid AND hrassessment_ratio.periodunkid = '" & iPeriodId & "' AND hrassessment_ratio.isactive = 1 " & _
                    "JOIN hrdepartment_master ON hremployee_master.departmentunkid = hrdepartment_master.departmentunkid " & _
                    "JOIN hrjob_master ON hremployee_master.jobunkid = hrjob_master.jobunkid " & _
                    "LEFT JOIN " & _
                    "( " & _
                         "SELECT " & _
                              " selfemployeeunkid AS EmpId " & _
                              ",SUM(ISNULL((hrcompetency_analysis_tran.result * (CASE WHEN @WeightAsNumber = CAST(1 AS BIT) THEN hrcompetency_analysis_tran.result ELSE (hrassess_competence_assign_tran.weight)/100 END)) * (hrassess_group_master.weight/100),0)) AS S_GA " & _
                         "FROM hrevaluation_analysis_master " & _
                              "JOIN hrcompetency_analysis_tran ON hrcompetency_analysis_tran.analysisunkid = hrevaluation_analysis_master.analysisunkid " & _
                              "JOIN hrassess_competence_assign_tran ON hrassess_competence_assign_tran.competenciesunkid = hrcompetency_analysis_tran.competenciesunkid " & _
                              "JOIN hrassess_competence_assign_master ON hrassess_competence_assign_master.assigncompetenceunkid = hrassess_competence_assign_tran.assigncompetenceunkid " & _
                                   "AND hrassess_competence_assign_master.assessgroupunkid = hrcompetency_analysis_tran.assessgroupunkid " & _
                              "JOIN hrassess_group_master ON hrassess_group_master.assessgroupunkid = hrcompetency_analysis_tran.assessgroupunkid " & _
                         "WHERE hrevaluation_analysis_master.isvoid = 0 AND hrcompetency_analysis_tran.isvoid = 0 AND hrevaluation_analysis_master.periodunkid = '" & iPeriodId & "' " & _
                         "AND assessmodeid = '" & enAssessmentMode.SELF_ASSESSMENT & "' AND iscommitted = 1 " & _
                         "GROUP BY selfemployeeunkid " & _
                    ") AS S_GA ON S_GA.EmpId = hremployee_master.employeeunkid " & _
                    "LEFT JOIN " & _
                    "( " & _
                    "SELECT " & _
                               "selfemployeeunkid AS EmpId " & _
                              ",SUM(ISNULL(hrgoals_analysis_tran.result,0)) AS S_BSC " & _
                         "FROM hrevaluation_analysis_master " & _
                              "JOIN hrgoals_analysis_tran ON hrgoals_analysis_tran.analysisunkid = hrevaluation_analysis_master.analysisunkid " & _
                         "WHERE hrevaluation_analysis_master.isvoid = 0 AND hrgoals_analysis_tran.isvoid = 0 AND hrevaluation_analysis_master.periodunkid = '" & iPeriodId & "' " & _
                         "AND assessmodeid = '" & enAssessmentMode.SELF_ASSESSMENT & "' AND iscommitted = 1 " & _
                         "GROUP BY selfemployeeunkid " & _
                    ")AS S_BSC ON S_BSC.EmpId = hremployee_master.employeeunkid " & _
                    "LEFT JOIN " & _
                    "( " & _
                         "SELECT " & _
                               "assessedemployeeunkid AS EmpId " & _
                              ",SUM(ISNULL((hrcompetency_analysis_tran.result * (CASE WHEN @WeightAsNumber = CAST(1 AS BIT) THEN hrcompetency_analysis_tran.result ELSE (hrassess_competence_assign_tran.weight)/100 END)) * (hrassess_group_master.weight/100),0)) / COUNT(DISTINCT assessormasterunkid)  AS A_GA " & _
                         "FROM hrevaluation_analysis_master " & _
                              "JOIN hrcompetency_analysis_tran ON hrcompetency_analysis_tran.analysisunkid = hrevaluation_analysis_master.analysisunkid " & _
                              "JOIN hrassess_competence_assign_tran ON hrassess_competence_assign_tran.competenciesunkid = hrcompetency_analysis_tran.competenciesunkid " & _
                              "JOIN hrassess_competence_assign_master ON hrassess_competence_assign_master.assigncompetenceunkid = hrassess_competence_assign_tran.assigncompetenceunkid " & _
                                   "AND hrassess_competence_assign_master.assessgroupunkid = hrcompetency_analysis_tran.assessgroupunkid " & _
                              "JOIN hrassess_group_master ON hrassess_group_master.assessgroupunkid = hrcompetency_analysis_tran.assessgroupunkid " & _
                         "WHERE hrevaluation_analysis_master.isvoid = 0 AND hrcompetency_analysis_tran.isvoid = 0 AND hrevaluation_analysis_master.periodunkid = '" & iPeriodId & "' " & _
                         "AND assessmodeid = '" & enAssessmentMode.APPRAISER_ASSESSMENT & "' AND iscommitted = 1 " & _
                         "GROUP BY assessedemployeeunkid " & _
                    ")AS A_GA ON A_GA.EmpId = hremployee_master.employeeunkid " & _
                    "LEFT JOIN " & _
                    "( " & _
                         "SELECT " & _
                               "assessedemployeeunkid AS EmpId " & _
                              ",SUM(ISNULL(hrgoals_analysis_tran.result,0)) / COUNT(DISTINCT assessormasterunkid)  AS A_BSC " & _
                         "FROM hrevaluation_analysis_master " & _
                              "JOIN hrgoals_analysis_tran ON hrgoals_analysis_tran.analysisunkid = hrevaluation_analysis_master.analysisunkid " & _
                         "WHERE hrevaluation_analysis_master.isvoid = 0 AND hrgoals_analysis_tran.isvoid = 0 AND hrevaluation_analysis_master.periodunkid = '" & iPeriodId & "' " & _
                         "AND assessmodeid = '" & enAssessmentMode.APPRAISER_ASSESSMENT & "' AND iscommitted = 1 " & _
                         "GROUP BY assessedemployeeunkid " & _
                    ")AS A_BSC ON A_BSC.EmpId = hremployee_master.employeeunkid " & _
                    "LEFT JOIN " & _
                    "( " & _
                         "SELECT " & _
                               "assessedemployeeunkid AS EmpId " & _
                              ",SUM(ISNULL((hrcompetency_analysis_tran.result * (CASE WHEN @WeightAsNumber = CAST(1 AS BIT) THEN hrcompetency_analysis_tran.result ELSE (hrassess_competence_assign_tran.weight)/100 END)) * (hrassess_group_master.weight/100),0)) AS R_GA " & _
                         "FROM hrevaluation_analysis_master " & _
                              "JOIN hrcompetency_analysis_tran ON hrcompetency_analysis_tran.analysisunkid = hrevaluation_analysis_master.analysisunkid " & _
                              "JOIN hrassess_competence_assign_tran ON hrassess_competence_assign_tran.competenciesunkid = hrcompetency_analysis_tran.competenciesunkid " & _
                              "JOIN hrassess_competence_assign_master ON hrassess_competence_assign_master.assigncompetenceunkid = hrassess_competence_assign_tran.assigncompetenceunkid " & _
                                   "AND hrassess_competence_assign_master.assessgroupunkid = hrcompetency_analysis_tran.assessgroupunkid " & _
                              "JOIN hrassess_group_master ON hrassess_group_master.assessgroupunkid = hrcompetency_analysis_tran.assessgroupunkid " & _
                         "WHERE hrevaluation_analysis_master.isvoid = 0 AND hrcompetency_analysis_tran.isvoid = 0 AND hrevaluation_analysis_master.periodunkid = '" & iPeriodId & "' " & _
                         "AND assessmodeid = '" & enAssessmentMode.REVIEWER_ASSESSMENT & "' AND iscommitted = 1 " & _
                         "GROUP BY assessedemployeeunkid " & _
                    ")AS R_GA ON R_GA.EmpId = hremployee_master.employeeunkid " & _
                    "LEFT JOIN " & _
                    "( " & _
                         "SELECT " & _
                               "assessedemployeeunkid AS EmpId " & _
                              ",SUM(ISNULL(hrgoals_analysis_tran.result,0)) AS R_BSC " & _
                         "FROM hrevaluation_analysis_master " & _
                              "JOIN hrgoals_analysis_tran ON hrgoals_analysis_tran.analysisunkid = hrevaluation_analysis_master.analysisunkid " & _
                         "WHERE hrevaluation_analysis_master.isvoid = 0 AND hrgoals_analysis_tran.isvoid = 0 AND hrevaluation_analysis_master.periodunkid = '" & iPeriodId & "' " & _
                         "AND assessmodeid = '" & enAssessmentMode.REVIEWER_ASSESSMENT & "' AND iscommitted = 1 " & _
                         "GROUP BY assessedemployeeunkid " & _
                    ")AS R_BSC ON R_BSC.EmpId = hremployee_master.employeeunkid WHERE 1 = 1 "

            'Shani(30-Jan-2016) -- Start
            'Old (757) :- "WHERE hrevaluation_analysis_master.isvoid = 0 AND hrgoals_analysis_tran.isvoid = 0 AND hrevaluation_analysis_master.periodunkid = '2' " & _
            'New (757) :- "WHERE hrevaluation_analysis_master.isvoid = 0 AND hrgoals_analysis_tran.isvoid = 0 AND hrevaluation_analysis_master.periodunkid = '" & iPeriodId & "' " & _
            'Shani(30-Jan-2016) -- End

            'S.SANDEEP [ 05 NOV 2014 ] -- END


            If sFilter.Trim.Length > 0 Then
                StrQ &= " AND " & sFilter
            End If

            'S.SANDEEP [ 22 OCT 2013 ] -- START
            Select Case mintAppointmentTypeId
                Case enAD_Report_Parameter.APP_DATE_FROM
                    If mdtDate1 <> Nothing AndAlso mdtDate2 <> Nothing Then
                        StrQ &= " AND ISNULL(CONVERT(CHAR(8), hremployee_master.appointeddate,112),'') BETWEEN '" & eZeeDate.convertDate(mdtDate1) & "' AND '" & eZeeDate.convertDate(mdtDate2) & "' "
                    End If
                Case enAD_Report_Parameter.APP_DATE_BEFORE
                    If mdtDate1 <> Nothing Then
                        StrQ &= " AND ISNULL(CONVERT(CHAR(8), hremployee_master.appointeddate,112),'') < '" & eZeeDate.convertDate(mdtDate1) & "' "
                    End If
                Case enAD_Report_Parameter.APP_DATE_AFTER
                    If mdtDate1 <> Nothing Then
                        StrQ &= " AND ISNULL(CONVERT(CHAR(8), hremployee_master.appointeddate,112),'') > '" & eZeeDate.convertDate(mdtDate1) & "' "
                    End If
            End Select
            'S.SANDEEP [ 22 OCT 2013 ] -- END

            'StrQ &= " AND hremployee_master.employeeunkid NOT IN (SELECT DISTINCT employeeunkid FROM hrapps_finalemployee " & _
            '        " JOIN hrapps_shortlist_master ON hrapps_finalemployee.shortlistunkid = hrapps_shortlist_master.shortlistunkid " & _
            '        " WHERE hrapps_finalemployee.isvoid = 0 AND hrapps_shortlist_master.periodunkid = '" & iPeriodId & "') "

            Select Case ConfigParameter._Object._PerformanceComputation
                Case 1 'Weight Avg Of
                    If ConfigParameter._Object._WAvgEmpComputationType = True AndAlso ConfigParameter._Object._WAvgAseComputType = True AndAlso ConfigParameter._Object._WAvgRevComputType = True Then
                        StrQ &= " AND CAST(((ISNULL(S_GA.S_GA,0) + ISNULL(A_GA.A_GA,0) + ISNULL(R_GA.R_GA,0))/3 * (ge_weight/100)+(ISNULL(S_BSC.S_BSC,0)+ ISNULL(A_BSC.A_BSC,0) + ISNULL(R_BSC.R_BSC,0))/3 *(bsc_weight/100)) AS DECIMAL(10,2)) BETWEEN " & mDecScrFrm & " AND  " & mDecScrTo & " "
                    ElseIf ConfigParameter._Object._WAvgEmpComputationType = True AndAlso ConfigParameter._Object._WAvgAseComputType = True AndAlso ConfigParameter._Object._WAvgRevComputType = False Then
                        StrQ &= " AND CAST(((ISNULL(S_GA.S_GA,0) + ISNULL(A_GA.A_GA,0))/2 * (ge_weight/100)+(ISNULL(S_BSC.S_BSC,0)+ ISNULL(A_BSC.A_BSC,0))/2 *(bsc_weight/100)) AS DECIMAL(10,2)) BETWEEN " & mDecScrFrm & " AND  " & mDecScrTo & " "
                    ElseIf ConfigParameter._Object._WAvgEmpComputationType = True AndAlso ConfigParameter._Object._WAvgAseComputType = False AndAlso ConfigParameter._Object._WAvgRevComputType = True Then
                        StrQ &= " AND CAST(((ISNULL(S_GA.S_GA,0)+ ISNULL(R_GA.R_GA,0))/2 * (ge_weight/100)+(ISNULL(S_BSC.S_BSC,0) + ISNULL(R_BSC.R_BSC,0))/2 *(bsc_weight/100)) AS DECIMAL(10,2)) BETWEEN " & mDecScrFrm & " AND  " & mDecScrTo & " "
                    ElseIf ConfigParameter._Object._WAvgEmpComputationType = False AndAlso ConfigParameter._Object._WAvgAseComputType = True AndAlso ConfigParameter._Object._WAvgRevComputType = True Then
                        StrQ &= " AND CAST(((ISNULL(A_GA.A_GA,0) + ISNULL(R_GA.R_GA,0))/2 * (ge_weight/100)+(ISNULL(A_BSC.A_BSC,0) + ISNULL(R_BSC.R_BSC,0))/2 *(bsc_weight/100)) AS DECIMAL(10,2)) BETWEEN " & mDecScrFrm & " AND  " & mDecScrTo & " "
                    ElseIf ConfigParameter._Object._WAvgEmpComputationType = True AndAlso ConfigParameter._Object._WAvgAseComputType = False AndAlso ConfigParameter._Object._WAvgRevComputType = False Then
                        StrQ &= " AND CAST(((ISNULL(S_GA.S_GA,0)) * (ge_weight/100) + (ISNULL(S_BSC.S_BSC,0)) * (bsc_weight/100)) AS DECIMAL(10,2)) BETWEEN " & mDecScrFrm & " AND  " & mDecScrTo & " "
                    ElseIf ConfigParameter._Object._WAvgEmpComputationType = False AndAlso ConfigParameter._Object._WAvgAseComputType = True AndAlso ConfigParameter._Object._WAvgRevComputType = False Then
                        StrQ &= " AND CAST(((ISNULL(A_GA.A_GA,0)) * (ge_weight/100)+(ISNULL(A_BSC.A_BSC,0)) * (bsc_weight/100)) AS DECIMAL(10,2)) BETWEEN " & mDecScrFrm & " AND  " & mDecScrTo & " "
                    ElseIf ConfigParameter._Object._WAvgEmpComputationType = False AndAlso ConfigParameter._Object._WAvgAseComputType = False AndAlso ConfigParameter._Object._WAvgRevComputType = True Then
                        StrQ &= " AND CAST(((ISNULL(R_GA.R_GA,0)) * (ge_weight/100)+(ISNULL(R_BSC.R_BSC,0))*(bsc_weight/100)) AS DECIMAL(10,2)) BETWEEN " & mDecScrFrm & " AND  " & mDecScrTo & " "
                    End If
                Case 2 'Weight Score Of
                    If ConfigParameter._Object._WScrComputType = 1 Then
                        StrQ &= " AND CAST(((ISNULL(S_GA.S_GA,0)) * (ge_weight/100) + (ISNULL(S_BSC.S_BSC,0)) * (bsc_weight/100)) AS DECIMAL(10,2)) BETWEEN " & mDecScrFrm & " AND  " & mDecScrTo & " "
                    ElseIf ConfigParameter._Object._WScrComputType = 2 Then
                        StrQ &= " AND CAST(((ISNULL(A_GA.A_GA,0)) * (ge_weight/100)+(ISNULL(A_BSC.A_BSC,0)) * (bsc_weight/100)) AS DECIMAL(10,2)) BETWEEN " & mDecScrFrm & " AND  " & mDecScrTo & " "
                    ElseIf ConfigParameter._Object._WScrComputType = 3 Then
                        StrQ &= " AND CAST(((ISNULL(R_GA.R_GA,0)) * (ge_weight/100)+(ISNULL(R_BSC.R_BSC,0))*(bsc_weight/100)) AS DECIMAL(10,2)) BETWEEN " & mDecScrFrm & " AND  " & mDecScrTo & " "
                    End If
            End Select

            'S.SANDEEP [ 10 SEPT 2013 ] -- START
            'ENHANCEMENT : TRA CHANGES
            Select Case ConfigParameter._Object._PerformanceComputation
                Case 1 'Weight Avg Of
                    If ConfigParameter._Object._WAvgEmpComputationType = True AndAlso ConfigParameter._Object._WAvgAseComputType = True AndAlso ConfigParameter._Object._WAvgRevComputType = True Then
                        StrQ &= " AND CAST(((ISNULL(S_GA.S_GA,0) + ISNULL(A_GA.A_GA,0) + ISNULL(R_GA.R_GA,0))/3 * (ge_weight/100)+(ISNULL(S_BSC.S_BSC,0)+ ISNULL(A_BSC.A_BSC,0) + ISNULL(R_BSC.R_BSC,0))/3 *(bsc_weight/100)) AS DECIMAL(10,2)) > 0 "
                    ElseIf ConfigParameter._Object._WAvgEmpComputationType = True AndAlso ConfigParameter._Object._WAvgAseComputType = True AndAlso ConfigParameter._Object._WAvgRevComputType = False Then
                        StrQ &= " AND CAST(((ISNULL(S_GA.S_GA,0) + ISNULL(A_GA.A_GA,0))/2 * (ge_weight/100)+(ISNULL(S_BSC.S_BSC,0)+ ISNULL(A_BSC.A_BSC,0))/2 *(bsc_weight/100)) AS DECIMAL(10,2)) > 0 "
                    ElseIf ConfigParameter._Object._WAvgEmpComputationType = True AndAlso ConfigParameter._Object._WAvgAseComputType = False AndAlso ConfigParameter._Object._WAvgRevComputType = True Then
                        StrQ &= " AND CAST(((ISNULL(S_GA.S_GA,0)+ ISNULL(R_GA.R_GA,0))/2 * (ge_weight/100)+(ISNULL(S_BSC.S_BSC,0) + ISNULL(R_BSC.R_BSC,0))/2 *(bsc_weight/100)) AS DECIMAL(10,2)) > 0 "
                    ElseIf ConfigParameter._Object._WAvgEmpComputationType = False AndAlso ConfigParameter._Object._WAvgAseComputType = True AndAlso ConfigParameter._Object._WAvgRevComputType = True Then
                        StrQ &= " AND CAST(((ISNULL(A_GA.A_GA,0) + ISNULL(R_GA.R_GA,0))/2 * (ge_weight/100)+(ISNULL(A_BSC.A_BSC,0) + ISNULL(R_BSC.R_BSC,0))/2 *(bsc_weight/100)) AS DECIMAL(10,2)) > 0 "
                    ElseIf ConfigParameter._Object._WAvgEmpComputationType = True AndAlso ConfigParameter._Object._WAvgAseComputType = False AndAlso ConfigParameter._Object._WAvgRevComputType = False Then
                        StrQ &= " AND CAST(((ISNULL(S_GA.S_GA,0)) * (ge_weight/100) + (ISNULL(S_BSC.S_BSC,0)) * (bsc_weight/100)) AS DECIMAL(10,2)) > 0 "
                    ElseIf ConfigParameter._Object._WAvgEmpComputationType = False AndAlso ConfigParameter._Object._WAvgAseComputType = True AndAlso ConfigParameter._Object._WAvgRevComputType = False Then
                        StrQ &= " AND CAST(((ISNULL(A_GA.A_GA,0)) * (ge_weight/100)+(ISNULL(A_BSC.A_BSC,0)) * (bsc_weight/100)) AS DECIMAL(10,2)) > 0 "
                    ElseIf ConfigParameter._Object._WAvgEmpComputationType = False AndAlso ConfigParameter._Object._WAvgAseComputType = False AndAlso ConfigParameter._Object._WAvgRevComputType = True Then
                        StrQ &= " AND CAST(((ISNULL(R_GA.R_GA,0)) * (ge_weight/100)+(ISNULL(R_BSC.R_BSC,0))*(bsc_weight/100)) AS DECIMAL(10,2)) > 0 "
                    End If
                Case 2 'Weight Score Of
                    If ConfigParameter._Object._WScrComputType = 1 Then
                        StrQ &= " AND CAST(((ISNULL(S_GA.S_GA,0)) * (ge_weight/100) + (ISNULL(S_BSC.S_BSC,0)) * (bsc_weight/100)) AS DECIMAL(10,2)) > 0 "
                    ElseIf ConfigParameter._Object._WScrComputType = 2 Then
                        StrQ &= " AND CAST(((ISNULL(A_GA.A_GA,0)) * (ge_weight/100)+(ISNULL(A_BSC.A_BSC,0)) * (bsc_weight/100)) AS DECIMAL(10,2)) > 0 "
                    ElseIf ConfigParameter._Object._WScrComputType = 3 Then
                        StrQ &= " AND CAST(((ISNULL(R_GA.R_GA,0)) * (ge_weight/100)+(ISNULL(R_BSC.R_BSC,0))*(bsc_weight/100)) AS DECIMAL(10,2)) > 0 "
                    End If
            End Select
            'S.SANDEEP [ 10 SEPT 2013 ] -- END


            'S.SANDEEP [ 22 OCT 2013 ] -- START
            'ENHANCEMENT : ENHANCEMENT
            ' CAST((hrassess_item_master.weight/100) AS DECIMAL(10,2)) -- Removed
            ' IIf(ConfigParameter._Object._ConsiderItemWeightAsNumber = True, "CAST((hrassess_item_master.weight) AS DECIMAL(10,2))", "CAST((hrassess_item_master.weight/100) AS DECIMAL(10,2))")  -- ADDED
            'S.SANDEEP [ 22 OCT 2013 ] -- END

            objDataOperation.AddParameter("@MALE", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 1, "Male"))
            objDataOperation.AddParameter("@FEMALE", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 2, "Female"))
            objDataOperation.AddParameter("@YES", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 3, "Yes"))
            'S.SANDEEP [ 05 NOV 2014 ] -- START
            objDataOperation.AddParameter("@WeightAsNumber", SqlDbType.Bit, eZeeDataType.BIT_SIZE, ConfigParameter._Object._ConsiderItemWeightAsNumber)
            'S.SANDEEP [ 05 NOV 2014 ] -- END


            dsList = objDataOperation.ExecQuery(StrQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & " : " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            mdtTran.Rows.Clear()

            mdtTran = New DataView(dsList.Tables("List")).ToTable

            Return mdtTran

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "GetEmployee_ByAward", mstrModuleName)
            Return Nothing
        Finally
        End Try
    End Function
    'S.SANDEEP [ 14 AUG 2013 ] -- END


    'S.SANDEEP [19 FEB 2015] -- START
    Public Function GetNewEmployee_ByAward(ByVal mDecScrFrm As Decimal, _
                                           ByVal mDecScrTo As Decimal, _
                                           ByVal iPeriodId As Integer, _
                                           ByVal iAction As String, _
                                           ByVal iActionId As Integer, _
                                           ByVal iRatingId As Integer, _
                                           ByVal sFilter As String, _
                                           ByVal xFilterBy As Integer, _
                                           ByVal xScrOptId As Integer, _
                                           ByVal xEmployeeAsOnDate As DateTime, _
                                           ByVal blnIsSelfAssignCompetencies As Boolean, _
                                           ByVal blnUsedAgreedScore As Boolean, _
                                           ByVal mblnIsCalibrationSettingActive As Boolean) As DataTable 'Shani (23-Nov-2016) -- [blnUsedAgreedScore]
        'S.SANDEEP |27-MAY-2019| -- START {mblnIsCalibrationSettingActive} -- END

        'Shani (23-Nov-2016) -- [blnUsedAgreedScore]
        Dim StrQ As String = ""
        Dim strErrorMessage As String = ""
        Dim dsList As New DataSet
        Dim exForce As Exception
        Dim dsAsrRevlst As New DataSet
        Try
            'S.SANDEEP [12-JUL-2018] -- START
            'ISSUE/ENHANCEMENT : {Ref#223|ARUTI-239}
            'Using objDo As New clsDataOperation
            '    StrQ = "SELECT " & _
            '           "	 0 AS IsChecked " & _
            '           "	,0 AS shortlistunkid " & _
            '           "	,hremployee_master.employeeunkid " & _
            '           "	,ISNULL(hremployee_master.employeecode, '') AS employeecode " & _
            '           "	,ISNULL(hremployee_master.firstname, '') + ' ' + ISNULL(hremployee_master.othername, '') + ' ' + ISNULL(hremployee_master.surname, '') AS employeename " & _
            '           "	,CASE hremployee_master.gender WHEN 1 THEN @MALE WHEN 2 THEN @FEMALE ELSE '' END AS Gender " & _
            '           "	,CONVERT(CHAR(8), ISNULL(hremployee_master.birthdate, '01-Jan-1900'), 112) AS birthdate " & _
            '           "	,CONVERT(CHAR(8), ISNULL(hremployee_master.appointeddate, '01-Jan-1900'), 112) AS appointeddate " & _
            '           "	,ISNULL(hremployee_master.present_mobile, '') AS Phone " & _
            '           "	,ISNULL(hremployee_master.email, '') AS email " & _
            '           "	,CASE WHEN ISNULL(hrapps_finalemployee.periodunkid,0) = 0 THEN 0 ELSE ISNULL(hrapps_finalemployee.periodunkid,0) END AS periodunkid " & _
            '           "	,1 AS isshortlisted " & _
            '           "	,ISNULL(hrapps_finalemployee.isfinalshortlisted,0) AS isfinalshortlisted " & _
            '           "	,CASE WHEN ISNULL(hrapps_finalemployee.operationmodeid,0) = 0 THEN " & iActionId & " ELSE ISNULL(hrapps_finalemployee.operationmodeid,0) END AS operationmodeid " & _
            '           "	,hrapps_finalemployee.userunkid " & _
            '           "	,hrapps_finalemployee.isvoid " & _
            '           "	,hrapps_finalemployee.voiddatetime " & _
            '           "	,hrapps_finalemployee.voidreason " & _
            '           "	,'' AS AUD " & _
            '           "	,ISNULL(hrapps_finalemployee.salaryincrementtranunkid, 0) AS salaryincrementtranunkid " & _
            '           "    ,ISNULL(hrapps_finalemployee.edunkid,0) AS edunkid " & _
            '           "	,hrjob_master.job_name AS Job_Title " & _
            '           "	,hrdepartment_master.name AS Dept " & _
            '           "    ,CASE WHEN ISNULL(hrapps_finalemployee.operationmodeid,0) = 0 THEN '" & iAction & "' ELSE ISNULL(cfcommon_master.name,'') END AS operationmode " & _
            '           "    ,CASE WHEN ISNULL(hrapps_finalemployee.appratingunkid,0) = 0 THEN '" & iRatingId & "' ELSE ISNULL(hrapps_finalemployee.appratingunkid,0) END AS appratingunkid " & _
            '           "    ,CASE WHEN ISNULL(hrapps_finalemployee.apprperiodunkid,0) = 0 THEN '" & iPeriodId & "' ELSE ISNULL(hrapps_finalemployee.apprperiodunkid,0) END AS apprperiodunkid " & _
            '           "    ,CASE WHEN ISNULL(hrapps_finalemployee.salaryincrementtranunkid, 0) > 0 THEN @SL " & _
            '           "          WHEN ISNULL(hrapps_finalemployee.edunkid, 0) > 0 THEN @ED  " & _
            '           "     ELSE '' END AS Operation " & _
            '           "    ,CASE WHEN ISNULL(hrapps_finalemployee.salaryincrementtranunkid, 0) > 0 THEN " & CInt(enAppraisal_Modes.SALARY_INCREMENT) & " " & _
            '           "          WHEN ISNULL(hrapps_finalemployee.edunkid, 0) > 0 THEN " & CInt(enAppraisal_Modes.BONUS) & " " & _
            '           "     ELSE 0 END AS OperationId " & _
            '           "    ,CAST(0 AS float) AS Score " & _
            '           "    ,CAST(0 AS BIT) AS isvalid " & _
            '           "FROM hremployee_master " & _
            '           "    LEFT JOIN hrapps_finalemployee ON hremployee_master.employeeunkid = hrapps_finalemployee.employeeunkid AND shortlistunkid <= 0  AND isvoid = 0 " & _
            '           "    LEFT JOIN cfcommon_master ON cfcommon_master.masterunkid = hrapps_finalemployee.operationmodeid AND mastertype ='" & clsCommon_Master.enCommonMaster.APPRAISAL_ACTIONS & "'  " & _
            '           "    JOIN hrdepartment_master ON hremployee_master.departmentunkid = hrdepartment_master.departmentunkid " & _
            '           "    JOIN hrjob_master ON hremployee_master.jobunkid = hrjob_master.jobunkid " & _
            '           "WHERE 1 = 1 "

            '    'S.SANDEEP [12-JUL-2018] -- START
            '    'ISSUE/ENHANCEMENT : {Ref#223|ARUTI-239}
            '    '", CASE WHEN ISNULL(hrapps_finalemployee.salaryincrementtranunkid, 0) > 0 THEN @YES ELSE '' END AS Operation " & _ -- REMOVED
            '    '", CASE WHEN ISNULL(hrapps_finalemployee.salaryincrementtranunkid, 0) > 0 THEN 1 ELSE 0 END AS OperationId " & _   -- REMOVED

            '    '-------- ADDED -- START
            '    ',ISNULL(hrapps_finalemployee.edunkid,0) AS edunkid

            '    '", CASE WHEN ISNULL(hrapps_finalemployee.salaryincrementtranunkid, 0) > 0 THEN @SL " & _
            '    '"       WHEN ISNULL(hrapps_finalemployee.edunkid, 0) > 0 THEN @ED  " & _
            '    '"  ELSE '' END AS Operation " & _
            '    '", CASE WHEN ISNULL(hrapps_finalemployee.salaryincrementtranunkid, 0) > 0 THEN " & CInt(enAppraisal_Modes.SALARY_INCREMENT) & " " & _
            '    '"       WHEN ISNULL(hrapps_finalemployee.edunkid, 0) > 0 THEN " & CInt(enAppraisal_Modes.BONUS) & " " & _
            '    '"  ELSE 0 END AS OperationId " & _
            '    '-------- ADDED -- END
            '    'S.SANDEEP [12-JUL-2018] -- END



            '    If sFilter.Trim.Length > 0 Then
            '        StrQ &= " AND " & sFilter
            '    End If
            '    Select Case mintAppointmentTypeId
            '        Case enAD_Report_Parameter.APP_DATE_FROM
            '            If mdtDate1 <> Nothing AndAlso mdtDate2 <> Nothing Then
            '                StrQ &= " AND ISNULL(CONVERT(CHAR(8), hremployee_master.appointeddate,112),'') BETWEEN '" & eZeeDate.convertDate(mdtDate1) & "' AND '" & eZeeDate.convertDate(mdtDate2) & "' "
            '            End If
            '        Case enAD_Report_Parameter.APP_DATE_BEFORE
            '            If mdtDate1 <> Nothing Then
            '                StrQ &= " AND ISNULL(CONVERT(CHAR(8), hremployee_master.appointeddate,112),'') < '" & eZeeDate.convertDate(mdtDate1) & "' "
            '            End If
            '        Case enAD_Report_Parameter.APP_DATE_AFTER
            '            If mdtDate1 <> Nothing Then
            '                StrQ &= " AND ISNULL(CONVERT(CHAR(8), hremployee_master.appointeddate,112),'') > '" & eZeeDate.convertDate(mdtDate1) & "' "
            '            End If
            '    End Select

            '    objDo.AddParameter("@MALE", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 1, "Male"))
            '    objDo.AddParameter("@FEMALE", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 2, "Female"))
            '    'S.SANDEEP [12-JUL-2018] -- START
            '    'ISSUE/ENHANCEMENT : {Ref#223|ARUTI-239}
            '    'objDo.AddParameter("@YES", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 3, "Yes"))
            '    objDo.AddParameter("@SL", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 4, "Salary Increment"))
            '    objDo.AddParameter("@ED", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 5, "Bonus"))
            '    'S.SANDEEP [12-JUL-2018] -- END

            '    dsList = objDo.ExecQuery(StrQ, "List")

            '    If objDo.ErrorMessage <> "" Then
            '        exForce = New Exception(objDo.ErrorNumber & " : " & objDo.ErrorMessage)
            '        Throw exForce
            '    End If


            '    StrQ = "SELECT " & _
            '           "     selfemployeeunkid AS EmpId " & _
            '           "    ,assessormasterunkid " & _
            '           "    ,assessmodeid " & _
            '           "FROM hrevaluation_analysis_master " & _
            '           "WHERE isvoid = 0 AND iscommitted = 1 AND periodunkid = '" & iPeriodId & "' AND assessmodeid = '" & enAssessmentMode.SELF_ASSESSMENT & "' " & _
            '           "UNION " & _
            '           "SELECT " & _
            '           "     assessedemployeeunkid AS EmpId " & _
            '           "    ,assessormasterunkid " & _
            '           "    ,assessmodeid " & _
            '           "FROM hrevaluation_analysis_master " & _
            '           "WHERE isvoid = 0 AND iscommitted = 1 AND periodunkid = '" & iPeriodId & "' AND assessmodeid = '" & enAssessmentMode.APPRAISER_ASSESSMENT & "' " & _
            '           "UNION " & _
            '           "SELECT " & _
            '           "     assessedemployeeunkid AS EmpId " & _
            '           "    ,assessormasterunkid " & _
            '           "    ,assessmodeid " & _
            '           "FROM hrevaluation_analysis_master " & _
            '           "WHERE isvoid = 0 AND iscommitted = 1 AND periodunkid = '" & iPeriodId & "' AND assessmodeid = '" & enAssessmentMode.REVIEWER_ASSESSMENT & "' "

            '    dsAsrRevlst = objDo.ExecQuery(StrQ, "List")

            '    If objDo.ErrorMessage <> "" Then
            '        Throw New Exception(objDo.ErrorNumber & " : " & objDo.ErrorMessage)
            '    End If


            'End Using

            'mdtTran.Rows.Clear()

            'If dsList.Tables(0).Rows.Count > 0 Then
            '    Dim xTotalScore As Decimal = 0
            '    Dim xEvaluation As New clsevaluation_analysis_master
            '    For Each xRow As DataRow In dsList.Tables(0).Rows
            '        xTotalScore = 0
            '        Select Case xFilterBy
            '            Case enApprFilterTypeId.APRL_OVER_ALL

            '                'Shani (23-Nov-2016) -- Start
            '                'Enhancement - Add New PA Enhancement (Agreed Score,exc) Given by Akiba,KBC,TNP exc...
            '                'xTotalScore = xEvaluation.Compute_Score(enAssessmentMode.SELF_ASSESSMENT, True, xScrOptId, enAssess_Computation_Formulas.FINAL_RESULT_SCORE, xEmployeeAsOnDate, xRow.Item("employeeunkid"), iPeriodId, , , , True, blnIsSelfAssignCompetencies) 'S.SANDEEP [08 Jan 2016] -- START{blnIsSelfAssignCompetencies} -- END
            '                xTotalScore = xEvaluation.Compute_Score(xAssessMode:=enAssessmentMode.SELF_ASSESSMENT, _
            '                                                        IsBalanceScoreCard:=True, _
            '                                                        xScoreOptId:=xScrOptId, _
            '                                                        xCompute_Formula:=enAssess_Computation_Formulas.FINAL_RESULT_SCORE, _
            '                                                        xEmployeeAsOnDate:=xEmployeeAsOnDate, _
            '                                                        xEmployeeId:=xRow.Item("employeeunkid"), _
            '                                                        xPeriodId:=iPeriodId, _
            '                                                        xUsedAgreedScore:=blnUsedAgreedScore, _
            '                                                        xOnlyCommited:=True, _
            '                                                        xSelfAssignedCompetencies:=blnIsSelfAssignCompetencies)
            '                'Shani (23-Nov-2016) -- End


            '                If dsAsrRevlst.Tables(0).Rows.Count > 0 Then
            '                    Dim xAsrRev() As DataRow = dsAsrRevlst.Tables(0).Select("EmpId = '" & xRow.Item("employeeunkid") & "' AND assessmodeid = '" & enAssessmentMode.APPRAISER_ASSESSMENT & "'")
            '                    If xAsrRev.Length > 0 Then

            '                        'Shani (23-Nov-2016) -- Start
            '                        'Enhancement - Add New PA Enhancement (Agreed Score,exc) Given by Akiba,KBC,TNP exc...
            '                        'xTotalScore += xEvaluation.Compute_Score(enAssessmentMode.APPRAISER_ASSESSMENT, True, xScrOptId, enAssess_Computation_Formulas.FINAL_RESULT_SCORE, xEmployeeAsOnDate, xRow.Item("employeeunkid"), iPeriodId, , , xAsrRev(0).Item("assessormasterunkid"), True, blnIsSelfAssignCompetencies) 'S.SANDEEP [08 Jan 2016] -- START{blnIsSelfAssignCompetencies} -- END
            '                        xTotalScore += xEvaluation.Compute_Score(xAssessMode:=enAssessmentMode.APPRAISER_ASSESSMENT, _
            '                                                                 IsBalanceScoreCard:=True, _
            '                                                                 xScoreOptId:=xScrOptId, _
            '                                                                 xCompute_Formula:=enAssess_Computation_Formulas.FINAL_RESULT_SCORE, _
            '                                                                 xEmployeeAsOnDate:=xEmployeeAsOnDate, _
            '                                                                 xEmployeeId:=xRow.Item("employeeunkid"), _
            '                                                                 xPeriodId:=iPeriodId, _
            '                                                                 xUsedAgreedScore:=blnUsedAgreedScore, _
            '                                                                 xAssessorReviewerId:=xAsrRev(0).Item("assessormasterunkid"), _
            '                                                                 xOnlyCommited:=True, _
            '                                                                 xSelfAssignedCompetencies:=blnIsSelfAssignCompetencies)
            '                        'Shani (23-Nov-2016) -- End


            '                    End If
            '                End If
            '                If dsAsrRevlst.Tables(0).Rows.Count > 0 Then
            '                    Dim xAsrRev() As DataRow = dsAsrRevlst.Tables(0).Select("EmpId = '" & xRow.Item("employeeunkid") & "' AND assessmodeid = '" & enAssessmentMode.REVIEWER_ASSESSMENT & "'")
            '                    If xAsrRev.Length > 0 Then

            '                        'Shani (23-Nov-2016) -- Start
            '                        'Enhancement - Add New PA Enhancement (Agreed Score,exc) Given by Akiba,KBC,TNP exc...
            '                        'xTotalScore += xEvaluation.Compute_Score(enAssessmentMode.REVIEWER_ASSESSMENT, True, xScrOptId, enAssess_Computation_Formulas.FINAL_RESULT_SCORE, xEmployeeAsOnDate, xRow.Item("employeeunkid"), iPeriodId, , , xAsrRev(0).Item("assessormasterunkid"), True, blnIsSelfAssignCompetencies) 'S.SANDEEP [08 Jan 2016] -- START{blnIsSelfAssignCompetencies} -- END
            '                        xTotalScore += xEvaluation.Compute_Score(xAssessMode:=enAssessmentMode.REVIEWER_ASSESSMENT, _
            '                                                                 IsBalanceScoreCard:=True, _
            '                                                                 xScoreOptId:=xScrOptId, _
            '                                                                 xCompute_Formula:=enAssess_Computation_Formulas.FINAL_RESULT_SCORE, _
            '                                                                 xEmployeeAsOnDate:=xEmployeeAsOnDate, _
            '                                                                 xEmployeeId:=xRow.Item("employeeunkid"), _
            '                                                                 xPeriodId:=iPeriodId, _
            '                                                                 xUsedAgreedScore:=blnUsedAgreedScore, _
            '                                                                 xAssessorReviewerId:=xAsrRev(0).Item("assessormasterunkid"), _
            '                                                                 xOnlyCommited:=True, _
            '                                                                 xSelfAssignedCompetencies:=blnIsSelfAssignCompetencies)
            '                        'Shani (23-Nov-2016) -- End


            '                    End If
            '                End If

            '                'Shani (23-Nov-2016) -- Start
            '                'Enhancement - Add New PA Enhancement (Agreed Score,exc) Given by Akiba,KBC,TNP exc...
            '                'xTotalScore += xEvaluation.Compute_Score(enAssessmentMode.SELF_ASSESSMENT, False, xScrOptId, enAssess_Computation_Formulas.FINAL_RESULT_SCORE, xEmployeeAsOnDate, xRow.Item("employeeunkid"), iPeriodId, , , , True, blnIsSelfAssignCompetencies) 'S.SANDEEP [08 Jan 2016] -- START{blnIsSelfAssignCompetencies} -- END
            '                xTotalScore += xEvaluation.Compute_Score(xAssessMode:=enAssessmentMode.SELF_ASSESSMENT, _
            '                                                         IsBalanceScoreCard:=False, _
            '                                                         xScoreOptId:=xScrOptId, _
            '                                                         xCompute_Formula:=enAssess_Computation_Formulas.FINAL_RESULT_SCORE, _
            '                                                         xEmployeeAsOnDate:=xEmployeeAsOnDate, _
            '                                                         xEmployeeId:=xRow.Item("employeeunkid"), _
            '                                                         xPeriodId:=iPeriodId, _
            '                                                         xUsedAgreedScore:=blnUsedAgreedScore, _
            '                                                         xOnlyCommited:=True, _
            '                                                         xSelfAssignedCompetencies:=blnIsSelfAssignCompetencies)
            '                'Shani (23-Nov-2016) -- End


            '                If dsAsrRevlst.Tables(0).Rows.Count > 0 Then
            '                    Dim xAsrRev() As DataRow = dsAsrRevlst.Tables(0).Select("EmpId = '" & xRow.Item("employeeunkid") & "' AND assessmodeid = '" & enAssessmentMode.APPRAISER_ASSESSMENT & "'")
            '                    If xAsrRev.Length > 0 Then

            '                        'Shani (23-Nov-2016) -- Start
            '                        'Enhancement - Add New PA Enhancement (Agreed Score,exc) Given by Akiba,KBC,TNP exc...
            '                        'xTotalScore += xEvaluation.Compute_Score(enAssessmentMode.APPRAISER_ASSESSMENT, False, xScrOptId, enAssess_Computation_Formulas.FINAL_RESULT_SCORE, xEmployeeAsOnDate, xRow.Item("employeeunkid"), iPeriodId, , , xAsrRev(0).Item("assessormasterunkid"), True, blnIsSelfAssignCompetencies) 'S.SANDEEP [08 Jan 2016] -- START{blnIsSelfAssignCompetencies} -- END
            '                        xTotalScore += xEvaluation.Compute_Score(xAssessMode:=enAssessmentMode.APPRAISER_ASSESSMENT, _
            '                                                                 IsBalanceScoreCard:=False, _
            '                                                                 xScoreOptId:=xScrOptId, _
            '                                                                 xCompute_Formula:=enAssess_Computation_Formulas.FINAL_RESULT_SCORE, _
            '                                                                 xEmployeeAsOnDate:=xEmployeeAsOnDate, _
            '                                                                 xEmployeeId:=xRow.Item("employeeunkid"), _
            '                                                                 xPeriodId:=iPeriodId, _
            '                                                                 xUsedAgreedScore:=blnUsedAgreedScore, _
            '                                                                 xAssessorReviewerId:=xAsrRev(0).Item("assessormasterunkid"), _
            '                                                                 xOnlyCommited:=True, _
            '                                                                 xSelfAssignedCompetencies:=blnIsSelfAssignCompetencies)
            '                        'Shani (23-Nov-2016) -- End


            '                    End If
            '                End If
            '                If dsAsrRevlst.Tables(0).Rows.Count > 0 Then
            '                    Dim xAsrRev() As DataRow = dsAsrRevlst.Tables(0).Select("EmpId = '" & xRow.Item("employeeunkid") & "' AND assessmodeid = '" & enAssessmentMode.REVIEWER_ASSESSMENT & "'")
            '                    If xAsrRev.Length > 0 Then

            '                        'Shani (23-Nov-2016) -- Start
            '                        'Enhancement - Add New PA Enhancement (Agreed Score,exc) Given by Akiba,KBC,TNP exc...
            '                        'xTotalScore += xEvaluation.Compute_Score(enAssessmentMode.REVIEWER_ASSESSMENT, False, xScrOptId, enAssess_Computation_Formulas.FINAL_RESULT_SCORE, xEmployeeAsOnDate, xRow.Item("employeeunkid"), iPeriodId, , , xAsrRev(0).Item("assessormasterunkid"), True, blnIsSelfAssignCompetencies) 'S.SANDEEP [08 Jan 2016] -- START{blnIsSelfAssignCompetencies} -- END
            '                        xTotalScore += xEvaluation.Compute_Score(xAssessMode:=enAssessmentMode.REVIEWER_ASSESSMENT, _
            '                                                                 IsBalanceScoreCard:=False, _
            '                                                                 xScoreOptId:=xScrOptId, _
            '                                                                 xCompute_Formula:=enAssess_Computation_Formulas.FINAL_RESULT_SCORE, _
            '                                                                 xEmployeeAsOnDate:=xEmployeeAsOnDate, _
            '                                                                 xEmployeeId:=xRow.Item("employeeunkid"), _
            '                                                                 xPeriodId:=iPeriodId, _
            '                                                                 xUsedAgreedScore:=blnUsedAgreedScore, _
            '                                                                 xAssessorReviewerId:=xAsrRev(0).Item("assessormasterunkid"), _
            '                                                                 xOnlyCommited:=True, _
            '                                                                 xSelfAssignedCompetencies:=blnIsSelfAssignCompetencies)
            '                        'Shani (23-Nov-2016) -- End


            '                    End If
            '                End If

            '                Call Set_Valid_Filter(xRow, xTotalScore, mDecScrFrm, mDecScrTo)

            '            Case enApprFilterTypeId.APRL_EMPLOYEE

            '                'Shani (23-Nov-2016) -- Start
            '                'Enhancement - Add New PA Enhancement (Agreed Score,exc) Given by Akiba,KBC,TNP exc...
            '                'xTotalScore = xEvaluation.Compute_Score(enAssessmentMode.SELF_ASSESSMENT, True, xScrOptId, enAssess_Computation_Formulas.EMP_OVERALL_SCORE, xEmployeeAsOnDate, xRow.Item("employeeunkid"), iPeriodId, , , , True, blnIsSelfAssignCompetencies) 'S.SANDEEP [08 Jan 2016] -- START{blnIsSelfAssignCompetencies} -- END
            '                'xTotalScore += xEvaluation.Compute_Score(enAssessmentMode.SELF_ASSESSMENT, False, xScrOptId, enAssess_Computation_Formulas.EMP_OVERALL_SCORE, xEmployeeAsOnDate, xRow.Item("employeeunkid"), iPeriodId, , , , True, blnIsSelfAssignCompetencies) 'S.SANDEEP [08 Jan 2016] -- START{blnIsSelfAssignCompetencies} -- END
            '                xTotalScore = xEvaluation.Compute_Score(xAssessMode:=enAssessmentMode.SELF_ASSESSMENT, _
            '                                                        IsBalanceScoreCard:=True, _
            '                                                        xScoreOptId:=xScrOptId, _
            '                                                        xCompute_Formula:=enAssess_Computation_Formulas.EMP_OVERALL_SCORE, _
            '                                                        xEmployeeAsOnDate:=xEmployeeAsOnDate, _
            '                                                        xEmployeeId:=xRow.Item("employeeunkid"), _
            '                                                        xPeriodId:=iPeriodId, _
            '                                                        xUsedAgreedScore:=blnUsedAgreedScore, _
            '                                                        xOnlyCommited:=True, _
            '                                                        xSelfAssignedCompetencies:=blnIsSelfAssignCompetencies)
            '                xTotalScore += xEvaluation.Compute_Score(xAssessMode:=enAssessmentMode.SELF_ASSESSMENT, _
            '                                                         IsBalanceScoreCard:=False, _
            '                                                         xScoreOptId:=xScrOptId, _
            '                                                         xCompute_Formula:=enAssess_Computation_Formulas.EMP_OVERALL_SCORE, _
            '                                                         xEmployeeAsOnDate:=xEmployeeAsOnDate, _
            '                                                         xEmployeeId:=xRow.Item("employeeunkid"), _
            '                                                         xPeriodId:=iPeriodId, _
            '                                                         xUsedAgreedScore:=blnUsedAgreedScore, _
            '                                                         xOnlyCommited:=True, _
            '                                                         xSelfAssignedCompetencies:=blnIsSelfAssignCompetencies)
            '                'Shani (23-Nov-2016) -- End

            '                Call Set_Valid_Filter(xRow, xTotalScore, mDecScrFrm, mDecScrTo)

            '            Case enApprFilterTypeId.APRL_ASSESSOR

            '                If dsAsrRevlst.Tables(0).Rows.Count > 0 Then
            '                    Dim xAsrRev() As DataRow = dsAsrRevlst.Tables(0).Select("EmpId = '" & xRow.Item("employeeunkid") & "' AND assessmodeid = '" & enAssessmentMode.APPRAISER_ASSESSMENT & "'")
            '                    If xAsrRev.Length > 0 Then

            '                        'Shani (23-Nov-2016) -- Start
            '                        'Enhancement - Add New PA Enhancement (Agreed Score,exc) Given by Akiba,KBC,TNP exc...
            '                        'xTotalScore += xEvaluation.Compute_Score(enAssessmentMode.APPRAISER_ASSESSMENT, True, xScrOptId, enAssess_Computation_Formulas.ASR_OVERALL_SCORE, xEmployeeAsOnDate, xRow.Item("employeeunkid"), iPeriodId, , , xAsrRev(0).Item("assessormasterunkid"), True, blnIsSelfAssignCompetencies) 'S.SANDEEP [08 Jan 2016] -- START{blnIsSelfAssignCompetencies} -- END
            '                        xTotalScore += xEvaluation.Compute_Score(xAssessMode:=enAssessmentMode.APPRAISER_ASSESSMENT, _
            '                                                                 IsBalanceScoreCard:=True, _
            '                                                                 xScoreOptId:=xScrOptId, _
            '                                                                 xCompute_Formula:=enAssess_Computation_Formulas.ASR_OVERALL_SCORE, _
            '                                                                 xEmployeeAsOnDate:=xEmployeeAsOnDate, _
            '                                                                 xEmployeeId:=xRow.Item("employeeunkid"), _
            '                                                                 xPeriodId:=iPeriodId, _
            '                                                                 xUsedAgreedScore:=blnUsedAgreedScore, _
            '                                                                 xAssessorReviewerId:=xAsrRev(0).Item("assessormasterunkid"), _
            '                                                                 xOnlyCommited:=True, _
            '                                                                 xSelfAssignedCompetencies:=blnIsSelfAssignCompetencies)
            '                        'Shani (23-Nov-2016) -- End


            '                    End If
            '                End If

            '                If dsAsrRevlst.Tables(0).Rows.Count > 0 Then
            '                    Dim xAsrRev() As DataRow = dsAsrRevlst.Tables(0).Select("EmpId = '" & xRow.Item("employeeunkid") & "' AND assessmodeid = '" & enAssessmentMode.APPRAISER_ASSESSMENT & "'")
            '                    If xAsrRev.Length > 0 Then

            '                        'Shani (23-Nov-2016) -- Start
            '                        'Enhancement - Add New PA Enhancement (Agreed Score,exc) Given by Akiba,KBC,TNP exc...
            '                        'xTotalScore += xEvaluation.Compute_Score(enAssessmentMode.APPRAISER_ASSESSMENT, False, xScrOptId, enAssess_Computation_Formulas.ASR_OVERALL_SCORE, xEmployeeAsOnDate, xRow.Item("employeeunkid"), iPeriodId, , , xAsrRev(0).Item("assessormasterunkid"), True, blnIsSelfAssignCompetencies) 'S.SANDEEP [08 Jan 2016] -- START{blnIsSelfAssignCompetencies} -- END
            '                        xTotalScore += xEvaluation.Compute_Score(xAssessMode:=enAssessmentMode.APPRAISER_ASSESSMENT, _
            '                                                                 IsBalanceScoreCard:=False, _
            '                                                                 xScoreOptId:=xScrOptId, _
            '                                                                 xCompute_Formula:=enAssess_Computation_Formulas.ASR_OVERALL_SCORE, _
            '                                                                 xEmployeeAsOnDate:=xEmployeeAsOnDate, _
            '                                                                 xEmployeeId:=xRow.Item("employeeunkid"), _
            '                                                                 xPeriodId:=iPeriodId, _
            '                                                                 xUsedAgreedScore:=blnUsedAgreedScore, _
            '                                                                 xAssessorReviewerId:=xAsrRev(0).Item("assessormasterunkid"), _
            '                                                                 xOnlyCommited:=True, _
            '                                                                 xSelfAssignedCompetencies:=blnIsSelfAssignCompetencies)
            '                        'Shani (23-Nov-2016) -- End


            '                    End If
            '                End If

            '                Call Set_Valid_Filter(xRow, xTotalScore, mDecScrFrm, mDecScrTo)

            '            Case enApprFilterTypeId.APRL_REVIEWER
            '                If dsAsrRevlst.Tables(0).Rows.Count > 0 Then
            '                    Dim xAsrRev() As DataRow = dsAsrRevlst.Tables(0).Select("EmpId = '" & xRow.Item("employeeunkid") & "' AND assessmodeid = '" & enAssessmentMode.REVIEWER_ASSESSMENT & "'")
            '                    If xAsrRev.Length > 0 Then

            '                        'Shani (23-Nov-2016) -- Start
            '                        'Enhancement - Add New PA Enhancement (Agreed Score,exc) Given by Akiba,KBC,TNP exc...
            '                        'xTotalScore += xEvaluation.Compute_Score(enAssessmentMode.REVIEWER_ASSESSMENT, True, xScrOptId, enAssess_Computation_Formulas.REV_OVERALL_SCORE, xEmployeeAsOnDate, xRow.Item("employeeunkid"), iPeriodId, , , xAsrRev(0).Item("assessormasterunkid"), True, blnIsSelfAssignCompetencies) 'S.SANDEEP [08 Jan 2016] -- START{blnIsSelfAssignCompetencies} -- END
            '                        xTotalScore += xEvaluation.Compute_Score(xAssessMode:=enAssessmentMode.REVIEWER_ASSESSMENT, _
            '                                                                 IsBalanceScoreCard:=True, _
            '                                                                 xScoreOptId:=xScrOptId, _
            '                                                                 xCompute_Formula:=enAssess_Computation_Formulas.REV_OVERALL_SCORE, _
            '                                                                 xEmployeeAsOnDate:=xEmployeeAsOnDate, _
            '                                                                 xEmployeeId:=xRow.Item("employeeunkid"), _
            '                                                                 xPeriodId:=iPeriodId, _
            '                                                                 xUsedAgreedScore:=blnUsedAgreedScore, _
            '                                                                 xAssessorReviewerId:=xAsrRev(0).Item("assessormasterunkid"), _
            '                                                                 xOnlyCommited:=True, _
            '                                                                 xSelfAssignedCompetencies:=blnIsSelfAssignCompetencies)
            '                        'Shani (23-Nov-2016) -- End


            '                    End If
            '                End If
            '                If dsAsrRevlst.Tables(0).Rows.Count > 0 Then
            '                    Dim xAsrRev() As DataRow = dsAsrRevlst.Tables(0).Select("EmpId = '" & xRow.Item("employeeunkid") & "' AND assessmodeid = '" & enAssessmentMode.REVIEWER_ASSESSMENT & "'")
            '                    If xAsrRev.Length > 0 Then

            '                        'Shani (23-Nov-2016) -- Start
            '                        'Enhancement - Add New PA Enhancement (Agreed Score,exc) Given by Akiba,KBC,TNP exc...
            '                        'xTotalScore += xEvaluation.Compute_Score(enAssessmentMode.REVIEWER_ASSESSMENT, False, xScrOptId, enAssess_Computation_Formulas.REV_OVERALL_SCORE, xEmployeeAsOnDate, xRow.Item("employeeunkid"), iPeriodId, , , xAsrRev(0).Item("assessormasterunkid"), True, blnIsSelfAssignCompetencies) 'S.SANDEEP [08 Jan 2016] -- START{blnIsSelfAssignCompetencies} -- END
            '                        xTotalScore += xEvaluation.Compute_Score(xAssessMode:=enAssessmentMode.REVIEWER_ASSESSMENT, _
            '                                                                 IsBalanceScoreCard:=False, _
            '                                                                 xScoreOptId:=xScrOptId, _
            '                                                                 xCompute_Formula:=enAssess_Computation_Formulas.REV_OVERALL_SCORE, _
            '                                                                 xEmployeeAsOnDate:=xEmployeeAsOnDate, _
            '                                                                 xEmployeeId:=xRow.Item("employeeunkid"), _
            '                                                                 xPeriodId:=iPeriodId, _
            '                                                                 xUsedAgreedScore:=blnUsedAgreedScore, _
            '                                                                 xAssessorReviewerId:=xAsrRev(0).Item("assessormasterunkid"), _
            '                                                                 xOnlyCommited:=True, _
            '                                                                 xSelfAssignedCompetencies:=blnIsSelfAssignCompetencies)
            '                        'Shani (23-Nov-2016) -- End


            '                    End If
            '                End If

            '                Call Set_Valid_Filter(xRow, xTotalScore, mDecScrFrm, mDecScrTo)

            '        End Select
            '    Next
            '    xEvaluation = Nothing
            'End If

            'mdtTran = New DataView(dsList.Tables(0), "isvalid = 1", "", DataViewRowState.CurrentRows).ToTable

            Using objDo As New clsDataOperation
                'S.SANDEEP |27-MAY-2019| -- START
                'ISSUE/ENHANCEMENT : [Score Calibration Process in Performance & Assessment Module]
                'StrQ = "SELECT " & _
                '       "	 0 AS IsChecked " & _
                '       "	,0 AS shortlistunkid " & _
                '       "	,hremployee_master.employeeunkid " & _
                '       "	,ISNULL(hremployee_master.employeecode, '') AS employeecode " & _
                '       "	,ISNULL(hremployee_master.firstname, '') + ' ' + ISNULL(hremployee_master.othername, '') + ' ' + ISNULL(hremployee_master.surname, '') AS employeename " & _
                '       "	,CASE hremployee_master.gender WHEN 1 THEN @MALE WHEN 2 THEN @FEMALE ELSE '' END AS Gender " & _
                '       "	,CONVERT(CHAR(8), ISNULL(hremployee_master.birthdate, '01-Jan-1900'), 112) AS birthdate " & _
                '       "	,CONVERT(CHAR(8), ISNULL(hremployee_master.appointeddate, '01-Jan-1900'), 112) AS appointeddate " & _
                '       "	,ISNULL(hremployee_master.present_mobile, '') AS Phone " & _
                '       "	,ISNULL(hremployee_master.email, '') AS email " & _
                '       "	,CASE WHEN ISNULL(hrapps_finalemployee.periodunkid,0) = 0 THEN 0 ELSE ISNULL(hrapps_finalemployee.periodunkid,0) END AS periodunkid " & _
                '       "	,1 AS isshortlisted " & _
                '       "	,ISNULL(hrapps_finalemployee.isfinalshortlisted,0) AS isfinalshortlisted " & _
                '       "	,CASE WHEN ISNULL(hrapps_finalemployee.operationmodeid,0) = 0 THEN " & iActionId & " ELSE ISNULL(hrapps_finalemployee.operationmodeid,0) END AS operationmodeid " & _
                '       "	,hrapps_finalemployee.userunkid " & _
                '       "	,hrapps_finalemployee.isvoid " & _
                '       "	,hrapps_finalemployee.voiddatetime " & _
                '       "	,hrapps_finalemployee.voidreason " & _
                '       "	,'' AS AUD " & _
                '       "	,ISNULL(hrapps_finalemployee.salaryincrementtranunkid, 0) AS salaryincrementtranunkid " & _
                '       "    ,ISNULL(hrapps_finalemployee.edunkid,0) AS edunkid " & _
                '       "	,hrjob_master.job_name AS Job_Title " & _
                '       "	,hrdepartment_master.name AS Dept " & _
                '       "    ,CASE WHEN ISNULL(hrapps_finalemployee.operationmodeid,0) = 0 THEN '" & iAction & "' ELSE ISNULL(cfcommon_master.name,'') END AS operationmode " & _
                '       "    ,CASE WHEN ISNULL(hrapps_finalemployee.appratingunkid,0) = 0 THEN '" & iRatingId & "' ELSE ISNULL(hrapps_finalemployee.appratingunkid,0) END AS appratingunkid " & _
                '       "    ,CASE WHEN ISNULL(hrapps_finalemployee.apprperiodunkid,0) = 0 THEN '" & iPeriodId & "' ELSE ISNULL(hrapps_finalemployee.apprperiodunkid,0) END AS apprperiodunkid " & _
                '       "    ,CASE WHEN ISNULL(hrapps_finalemployee.salaryincrementtranunkid, 0) > 0 THEN @SL " & _
                '       "          WHEN ISNULL(hrapps_finalemployee.edunkid, 0) > 0 THEN @ED  " & _
                '       "     ELSE '' END AS Operation " & _
                '       "    ,CASE WHEN ISNULL(hrapps_finalemployee.salaryincrementtranunkid, 0) > 0 THEN " & CInt(enAppraisal_Modes.SALARY_INCREMENT) & " " & _
                '       "          WHEN ISNULL(hrapps_finalemployee.edunkid, 0) > 0 THEN " & CInt(enAppraisal_Modes.BONUS) & " " & _
                '       "     ELSE 0 END AS OperationId " & _
                '       "    ,CAST(0 AS BIT) AS isvalid " & _
                '       "    ,CASE WHEN @FilterTypeId = '" & enApprFilterTypeId.APRL_OVER_ALL & "' THEN ISNULL(os.finaloverallscore, 0) " & _
                '       "          WHEN @FilterTypeId = '" & enApprFilterTypeId.APRL_EMPLOYEE & "' THEN ISNULL(ems.formula_value, 0) " & _
                '       "          WHEN @FilterTypeId = '" & enApprFilterTypeId.APRL_ASSESSOR & "' THEN ISNULL(ars.formula_value, 0) " & _
                '       "          WHEN @FilterTypeId = '" & enApprFilterTypeId.APRL_REVIEWER & "' THEN ISNULL(rvs.formula_value, 0) " & _
                '       "     END AS Score " & _
                '       "FROM hremployee_master " & _
                '       "    LEFT JOIN hrapps_finalemployee ON hremployee_master.employeeunkid = hrapps_finalemployee.employeeunkid AND shortlistunkid <= 0  AND isvoid = 0 " & _
                '       "    LEFT JOIN cfcommon_master ON cfcommon_master.masterunkid = hrapps_finalemployee.operationmodeid AND mastertype = '" & clsCommon_Master.enCommonMaster.APPRAISAL_ACTIONS & "' " & _
                '       "    LEFT JOIN " & _
                '       "    ( " & _
                '       "        SELECT " & _
                '       "             employeeunkid " & _
                '       "            ,jobunkid " & _
                '       "            ,ROW_NUMBER() OVER (PARTITION BY employeeunkid ORDER BY effectivedate DESC) AS xno " & _
                '       "        FROM hremployee_categorization_tran " & _
                '       "        WHERE isvoid = 0 AND CONVERT(NVARCHAR(8), effectivedate, 112) <= '" & eZeeDate.convertDate(xEmployeeAsOnDate) & "' " & _
                '       "    ) AS cc ON cc.employeeunkid = hremployee_master.employeeunkid AND cc.xno = 1 " & _
                '       "    JOIN hrjob_master ON cc.jobunkid = hrjob_master.jobunkid " & _
                '       "    LEFT JOIN " & _
                '       "    ( " & _
                '       "        SELECT " & _
                '       "             employeeunkid " & _
                '       "            ,departmentunkid " & _
                '       "            ,ROW_NUMBER() OVER (PARTITION BY employeeunkid ORDER BY effectivedate DESC) AS xno " & _
                '       "    FROM hremployee_transfer_tran " & _
                '       "    WHERE isvoid = 0 AND CONVERT(NVARCHAR(8), effectivedate, 112) <= '" & eZeeDate.convertDate(xEmployeeAsOnDate) & "' " & _
                '       "    ) AS ac ON ac.employeeunkid = hremployee_master.employeeunkid AND ac.xno = 1 " & _
                '       "    JOIN hrdepartment_master ON hrdepartment_master.departmentunkid = ac.departmentunkid " & _
                '       "    LEFT JOIN " & _
                '       "    ( " & _
                '       "        SELECT DISTINCT " & _
                '       "             OVS.periodunkid " & _
                '       "            ,OVS.analysisunkid " & _
                '       "            ,CAST(OVS.finaloverallscore AS DECIMAL(10, 2)) AS finaloverallscore " & _
                '       "            ,OVS.employeeunkid " & _
                '       "        FROM " & _
                '       "        ( " & _
                '       "            SELECT " & _
                '       "                 CSM.periodunkid " & _
                '       "                ,CSM.analysisunkid " & _
                '       "                ,CSM.finaloverallscore " & _
                '       "                ,ROW_NUMBER() OVER (PARTITION BY CSM.employeeunkid, CSM.periodunkid ORDER BY CSM.computationdate ASC) AS xNo " & _
                '       "                ,CASE WHEN EM.selfemployeeunkid <= 0 THEN EM.assessedemployeeunkid ELSE EM.selfemployeeunkid END AS employeeunkid " & _
                '       "            FROM hrassess_compute_score_master AS CSM " & _
                '       "                JOIN hrevaluation_analysis_master AS EM ON CSM.analysisunkid = EM.analysisunkid AND CSM.periodunkid = EM.periodunkid " & _
                '       "            WHERE CSM.isvoid = 0 AND CSM.periodunkid = @PeriodId " & _
                '       "        ) AS OVS WHERE OVS.xNo = 1 " & _
                '       "    ) AS os ON os.employeeunkid = hremployee_master.employeeunkid " & _
                '       "    LEFT JOIN " & _
                '       "    ( " & _
                '       "        SELECT DISTINCT " & _
                '       "             CSM.periodunkid " & _
                '       "            ,CSM.analysisunkid " & _
                '       "            ,2 AS FilterTypeId " & _
                '       "            ,CAST((SUM(CST.formula_value) / 2) AS DECIMAL(10, 2)) AS formula_value " & _
                '       "            ,CASE WHEN EM.selfemployeeunkid <= 0 THEN EM.assessedemployeeunkid ELSE EM.selfemployeeunkid END AS employeeunkid " & _
                '       "        FROM hrassess_compute_score_master AS CSM " & _
                '       "            JOIN hrassess_compute_score_tran AS CST ON CSM.computescoremasterunkid = CST.computescoremasterunkid " & _
                '       "            JOIN hrevaluation_analysis_master AS EM ON CSM.analysisunkid = EM.analysisunkid AND CSM.periodunkid = EM.periodunkid " & _
                '       "            JOIN hrassess_computation_master AS CM ON CM.computationunkid = CST.computationunkid " & _
                '       "        WHERE CSM.isvoid = 0 AND CST.isvoid = 0 AND CSM.periodunkid = @PeriodId AND EM.assessmodeid = 1 AND CM.formula_typeid IN (1, 4) " & _
                '       "        GROUP BY CSM.periodunkid ,CSM.analysisunkid,CASE WHEN EM.selfemployeeunkid <= 0 THEN EM.assessedemployeeunkid ELSE EM.selfemployeeunkid END " & _
                '       "    ) AS ems ON ems.employeeunkid = hremployee_master.employeeunkid " & _
                '       "    LEFT JOIN " & _
                '       "    ( " & _
                '       "        SELECT DISTINCT " & _
                '       "             CSM.periodunkid " & _
                '       "            ,CSM.analysisunkid " & _
                '       "            ,3 AS FilterTypeId " & _
                '       "            ,CAST((SUM(CST.formula_value) / 2) AS DECIMAL(10, 2)) AS formula_value " & _
                '       "            ,CASE WHEN EM.selfemployeeunkid <= 0 THEN EM.assessedemployeeunkid ELSE EM.selfemployeeunkid END AS employeeunkid " & _
                '       "        FROM hrassess_compute_score_master AS CSM " & _
                '       "            JOIN hrassess_compute_score_tran AS CST ON CSM.computescoremasterunkid = CST.computescoremasterunkid " & _
                '       "            JOIN hrevaluation_analysis_master AS EM ON CSM.analysisunkid = EM.analysisunkid AND CSM.periodunkid = EM.periodunkid " & _
                '       "            JOIN hrassess_computation_master AS CM ON CM.computationunkid = CST.computationunkid " & _
                '       "        WHERE CSM.isvoid = 0 AND CST.isvoid = 0 AND CSM.periodunkid = @PeriodId AND EM.assessmodeid = 2 AND CM.formula_typeid IN (2, 5) " & _
                '       "        GROUP BY CSM.periodunkid ,CSM.analysisunkid ,CASE WHEN EM.selfemployeeunkid <= 0 THEN EM.assessedemployeeunkid ELSE EM.selfemployeeunkid END " & _
                '       "    ) AS ars ON ars.employeeunkid = hremployee_master.employeeunkid " & _
                '       "    LEFT JOIN " & _
                '       "    ( " & _
                '       "        SELECT DISTINCT " & _
                '       "             CSM.periodunkid " & _
                '       "            ,CSM.analysisunkid " & _
                '       "            ,4 AS FilterTypeId " & _
                '       "            ,CAST((SUM(CST.formula_value) / 2) AS DECIMAL(10, 2)) AS formula_value " & _
                '       "            ,CASE WHEN EM.selfemployeeunkid <= 0 THEN EM.assessedemployeeunkid ELSE EM.selfemployeeunkid END AS employeeunkid " & _
                '       "        FROM hrassess_compute_score_master AS CSM " & _
                '       "            JOIN hrassess_compute_score_tran AS CST ON CSM.computescoremasterunkid = CST.computescoremasterunkid " & _
                '       "            JOIN hrevaluation_analysis_master AS EM ON CSM.analysisunkid = EM.analysisunkid AND CSM.periodunkid = EM.periodunkid " & _
                '       "            JOIN hrassess_computation_master AS CM ON CM.computationunkid = CST.computationunkid " & _
                '       "        WHERE CSM.isvoid = 0 AND CST.isvoid = 0 AND CSM.periodunkid = @PeriodId AND EM.assessmodeid = 3 AND CM.formula_typeid IN (3, 6) " & _
                '       "        GROUP BY CSM.periodunkid ,CSM.analysisunkid ,CASE WHEN EM.selfemployeeunkid <= 0 THEN EM.assessedemployeeunkid ELSE EM.selfemployeeunkid END " & _
                '       "    ) AS rvs ON rvs.employeeunkid = hremployee_master.employeeunkid " & _
                '       "WHERE 1 = 1 "

                StrQ = "DECLARE @Setting AS BIT ; SET @Setting = " & IIf(mblnIsCalibrationSettingActive = True, 1, 0) & " " & _
                       "SELECT " & _
                       "	 0 AS IsChecked " & _
                       "	,0 AS shortlistunkid " & _
                       "	,hremployee_master.employeeunkid " & _
                       "	,ISNULL(hremployee_master.employeecode, '') AS employeecode " & _
                       "	,ISNULL(hremployee_master.firstname, '') + ' ' + ISNULL(hremployee_master.othername, '') + ' ' + ISNULL(hremployee_master.surname, '') AS employeename " & _
                       "	,CASE hremployee_master.gender WHEN 1 THEN @MALE WHEN 2 THEN @FEMALE ELSE '' END AS Gender " & _
                       "	,CONVERT(CHAR(8), ISNULL(hremployee_master.birthdate, '01-Jan-1900'), 112) AS birthdate " & _
                       "	,CONVERT(CHAR(8), ISNULL(hremployee_master.appointeddate, '01-Jan-1900'), 112) AS appointeddate " & _
                       "	,ISNULL(hremployee_master.present_mobile, '') AS Phone " & _
                       "	,ISNULL(hremployee_master.email, '') AS email " & _
                       "	,CASE WHEN ISNULL(hrapps_finalemployee.periodunkid,0) = 0 THEN 0 ELSE ISNULL(hrapps_finalemployee.periodunkid,0) END AS periodunkid " & _
                       "	,1 AS isshortlisted " & _
                       "	,ISNULL(hrapps_finalemployee.isfinalshortlisted,0) AS isfinalshortlisted " & _
                       "	,CASE WHEN ISNULL(hrapps_finalemployee.operationmodeid,0) = 0 THEN " & iActionId & " ELSE ISNULL(hrapps_finalemployee.operationmodeid,0) END AS operationmodeid " & _
                       "	,hrapps_finalemployee.userunkid " & _
                       "	,hrapps_finalemployee.isvoid " & _
                       "	,hrapps_finalemployee.voiddatetime " & _
                       "	,hrapps_finalemployee.voidreason " & _
                       "	,'' AS AUD " & _
                       "	,ISNULL(hrapps_finalemployee.salaryincrementtranunkid, 0) AS salaryincrementtranunkid " & _
                       "    ,ISNULL(hrapps_finalemployee.edunkid,0) AS edunkid " & _
                       "	,hrjob_master.job_name AS Job_Title " & _
                       "	,hrdepartment_master.name AS Dept " & _
                       "    ,CASE WHEN ISNULL(hrapps_finalemployee.operationmodeid,0) = 0 THEN '" & iAction & "' ELSE ISNULL(cfcommon_master.name,'') END AS operationmode " & _
                       "    ,CASE WHEN ISNULL(hrapps_finalemployee.appratingunkid,0) = 0 THEN '" & iRatingId & "' ELSE ISNULL(hrapps_finalemployee.appratingunkid,0) END AS appratingunkid " & _
                       "    ,CASE WHEN ISNULL(hrapps_finalemployee.apprperiodunkid,0) = 0 THEN '" & iPeriodId & "' ELSE ISNULL(hrapps_finalemployee.apprperiodunkid,0) END AS apprperiodunkid " & _
                       "    ,CASE WHEN ISNULL(hrapps_finalemployee.salaryincrementtranunkid, 0) > 0 THEN @SL " & _
                       "          WHEN ISNULL(hrapps_finalemployee.edunkid, 0) > 0 THEN @ED  " & _
                       "     ELSE '' END AS Operation " & _
                       "    ,CASE WHEN ISNULL(hrapps_finalemployee.salaryincrementtranunkid, 0) > 0 THEN " & CInt(enAppraisal_Modes.SALARY_INCREMENT) & " " & _
                       "          WHEN ISNULL(hrapps_finalemployee.edunkid, 0) > 0 THEN " & CInt(enAppraisal_Modes.BONUS) & " " & _
                       "     ELSE 0 END AS OperationId " & _
                       "    ,CAST(0 AS BIT) AS isvalid " & _
                       "    ,CASE WHEN @FilterTypeId = '" & enApprFilterTypeId.APRL_OVER_ALL & "' THEN CAST(ISNULL(os.finaloverallscore, 0) AS DECIMAL(36,2)) " & _
                       "          WHEN @FilterTypeId = '" & enApprFilterTypeId.APRL_EMPLOYEE & "' THEN CAST(ISNULL(ems.formula_value, 0) AS DECIMAL(36,2)) " & _
                       "          WHEN @FilterTypeId = '" & enApprFilterTypeId.APRL_ASSESSOR & "' THEN CAST(ISNULL(ars.formula_value, 0) AS DECIMAL(36,2)) " & _
                       "          WHEN @FilterTypeId = '" & enApprFilterTypeId.APRL_REVIEWER & "' THEN CAST(ISNULL(rvs.formula_value, 0) AS DECIMAL(36,2)) " & _
                       "     END AS Score " & _
                       "FROM hremployee_master " & _
                       "    LEFT JOIN hrapps_finalemployee ON hremployee_master.employeeunkid = hrapps_finalemployee.employeeunkid AND shortlistunkid <= 0  AND isvoid = 0 " & _
                       "    LEFT JOIN cfcommon_master ON cfcommon_master.masterunkid = hrapps_finalemployee.operationmodeid AND mastertype = '" & clsCommon_Master.enCommonMaster.APPRAISAL_ACTIONS & "' " & _
                       "    LEFT JOIN " & _
                       "    ( " & _
                       "        SELECT " & _
                       "             employeeunkid " & _
                       "            ,jobunkid " & _
                       "            ,ROW_NUMBER() OVER (PARTITION BY employeeunkid ORDER BY effectivedate DESC) AS xno " & _
                       "        FROM hremployee_categorization_tran " & _
                       "        WHERE isvoid = 0 AND CONVERT(NVARCHAR(8), effectivedate, 112) <= '" & eZeeDate.convertDate(xEmployeeAsOnDate) & "' " & _
                       "    ) AS cc ON cc.employeeunkid = hremployee_master.employeeunkid AND cc.xno = 1 " & _
                       "    JOIN hrjob_master ON cc.jobunkid = hrjob_master.jobunkid " & _
                       "    LEFT JOIN " & _
                       "    ( " & _
                       "        SELECT " & _
                       "             employeeunkid " & _
                       "            ,departmentunkid " & _
                       "            ,ROW_NUMBER() OVER (PARTITION BY employeeunkid ORDER BY effectivedate DESC) AS xno " & _
                       "    FROM hremployee_transfer_tran " & _
                       "    WHERE isvoid = 0 AND CONVERT(NVARCHAR(8), effectivedate, 112) <= '" & eZeeDate.convertDate(xEmployeeAsOnDate) & "' " & _
                       "    ) AS ac ON ac.employeeunkid = hremployee_master.employeeunkid AND ac.xno = 1 " & _
                       "    JOIN hrdepartment_master ON hrdepartment_master.departmentunkid = ac.departmentunkid " & _
                       "    LEFT JOIN " & _
                       "    ( " & _
                       "        SELECT DISTINCT " & _
                       "             OVS.periodunkid " & _
                       "            ,OVS.analysisunkid " & _
                       "            ,CAST(OVS.finaloverallscore AS DECIMAL(10, 2)) AS finaloverallscore " & _
                       "            ,OVS.employeeunkid " & _
                       "        FROM " & _
                       "        ( " & _
                       "            SELECT " & _
                       "                 CSM.periodunkid " & _
                       "                ,CSM.analysisunkid " & _
                       "                ,CASE WHEN @Setting = 1 AND ISNULL(CSM.isprocessed,0) = 1 THEN CSM.calibrated_score " & _
                       "                      WHEN @Setting = 1 AND ISNULL(CSM.isprocessed,0) = 0 THEN 0 " & _
                       "                      WHEN @Setting = 0 THEN CSM.finaloverallscore " & _
                       "                 END AS finaloverallscore " & _
                       "                ,ROW_NUMBER() OVER (PARTITION BY CSM.employeeunkid, CSM.periodunkid ORDER BY CSM.computationdate ASC) AS xNo " & _
                       "                ,CASE WHEN EM.selfemployeeunkid <= 0 THEN EM.assessedemployeeunkid ELSE EM.selfemployeeunkid END AS employeeunkid " & _
                       "            FROM hrassess_compute_score_master AS CSM " & _
                       "                JOIN hrevaluation_analysis_master AS EM ON CSM.analysisunkid = EM.analysisunkid AND CSM.periodunkid = EM.periodunkid " & _
                       "            WHERE CSM.isvoid = 0 AND CSM.periodunkid = @PeriodId " & _
                       "        ) AS OVS WHERE OVS.xNo = 1 " & _
                       "    ) AS os ON os.employeeunkid = hremployee_master.employeeunkid " & _
                       "    LEFT JOIN " & _
                       "    ( " & _
                       "        SELECT DISTINCT " & _
                       "             CSM.periodunkid " & _
                       "            ,CSM.analysisunkid " & _
                       "            ,2 AS FilterTypeId " & _
                       "            ,CAST((SUM(CST.formula_value) / 2) AS DECIMAL(10, 2)) AS formula_value " & _
                       "            ,CASE WHEN EM.selfemployeeunkid <= 0 THEN EM.assessedemployeeunkid ELSE EM.selfemployeeunkid END AS employeeunkid " & _
                       "        FROM hrassess_compute_score_master AS CSM " & _
                       "            JOIN hrassess_compute_score_tran AS CST ON CSM.computescoremasterunkid = CST.computescoremasterunkid " & _
                       "            JOIN hrevaluation_analysis_master AS EM ON CSM.analysisunkid = EM.analysisunkid AND CSM.periodunkid = EM.periodunkid " & _
                       "            JOIN hrassess_computation_master AS CM ON CM.computationunkid = CST.computationunkid " & _
                       "        WHERE CSM.isvoid = 0 AND CST.isvoid = 0 AND CSM.periodunkid = @PeriodId AND EM.assessmodeid = 1 AND CM.formula_typeid IN (1, 4) " & _
                       "        GROUP BY CSM.periodunkid ,CSM.analysisunkid,CASE WHEN EM.selfemployeeunkid <= 0 THEN EM.assessedemployeeunkid ELSE EM.selfemployeeunkid END " & _
                       "    ) AS ems ON ems.employeeunkid = hremployee_master.employeeunkid " & _
                       "    LEFT JOIN " & _
                       "    ( " & _
                       "        SELECT DISTINCT " & _
                       "             CSM.periodunkid " & _
                       "            ,CSM.analysisunkid " & _
                       "            ,3 AS FilterTypeId " & _
                       "            ,CAST((SUM(CST.formula_value) / 2) AS DECIMAL(10, 2)) AS formula_value " & _
                       "            ,CASE WHEN EM.selfemployeeunkid <= 0 THEN EM.assessedemployeeunkid ELSE EM.selfemployeeunkid END AS employeeunkid " & _
                       "        FROM hrassess_compute_score_master AS CSM " & _
                       "            JOIN hrassess_compute_score_tran AS CST ON CSM.computescoremasterunkid = CST.computescoremasterunkid " & _
                       "            JOIN hrevaluation_analysis_master AS EM ON CSM.analysisunkid = EM.analysisunkid AND CSM.periodunkid = EM.periodunkid " & _
                       "            JOIN hrassess_computation_master AS CM ON CM.computationunkid = CST.computationunkid " & _
                       "        WHERE CSM.isvoid = 0 AND CST.isvoid = 0 AND CSM.periodunkid = @PeriodId AND EM.assessmodeid = 2 AND CM.formula_typeid IN (2, 5) " & _
                       "        GROUP BY CSM.periodunkid ,CSM.analysisunkid ,CASE WHEN EM.selfemployeeunkid <= 0 THEN EM.assessedemployeeunkid ELSE EM.selfemployeeunkid END " & _
                       "    ) AS ars ON ars.employeeunkid = hremployee_master.employeeunkid " & _
                       "    LEFT JOIN " & _
                       "    ( " & _
                       "        SELECT DISTINCT " & _
                       "             CSM.periodunkid " & _
                       "            ,CSM.analysisunkid " & _
                       "            ,4 AS FilterTypeId " & _
                       "            ,CAST((SUM(CST.formula_value) / 2) AS DECIMAL(10, 2)) AS formula_value " & _
                       "            ,CASE WHEN EM.selfemployeeunkid <= 0 THEN EM.assessedemployeeunkid ELSE EM.selfemployeeunkid END AS employeeunkid " & _
                       "        FROM hrassess_compute_score_master AS CSM " & _
                       "            JOIN hrassess_compute_score_tran AS CST ON CSM.computescoremasterunkid = CST.computescoremasterunkid " & _
                       "            JOIN hrevaluation_analysis_master AS EM ON CSM.analysisunkid = EM.analysisunkid AND CSM.periodunkid = EM.periodunkid " & _
                       "            JOIN hrassess_computation_master AS CM ON CM.computationunkid = CST.computationunkid " & _
                       "        WHERE CSM.isvoid = 0 AND CST.isvoid = 0 AND CSM.periodunkid = @PeriodId AND EM.assessmodeid = 3 AND CM.formula_typeid IN (3, 6) " & _
                       "        GROUP BY CSM.periodunkid ,CSM.analysisunkid ,CASE WHEN EM.selfemployeeunkid <= 0 THEN EM.assessedemployeeunkid ELSE EM.selfemployeeunkid END " & _
                       "    ) AS rvs ON rvs.employeeunkid = hremployee_master.employeeunkid " & _
                       "WHERE 1 = 1 "
                'S.SANDEEP |27-MAY-2019| -- END

                'S.SANDEEP |21-AUG-2019| -- START
                'ISSUE/ENHANCEMENT : DECIMAL PLACES ISSUES
                '--------------------------- REMOVED
                '"    ,CASE WHEN @FilterTypeId = '" & enApprFilterTypeId.APRL_OVER_ALL & "' THEN ISNULL(os.finaloverallscore, 0) " & _
                '"          WHEN @FilterTypeId = '" & enApprFilterTypeId.APRL_EMPLOYEE & "' THEN ISNULL(ems.formula_value, 0) " & _
                '"          WHEN @FilterTypeId = '" & enApprFilterTypeId.APRL_ASSESSOR & "' THEN ISNULL(ars.formula_value, 0) " & _
                '"          WHEN @FilterTypeId = '" & enApprFilterTypeId.APRL_REVIEWER & "' THEN ISNULL(rvs.formula_value, 0) " & _
                '"     END AS Score "

                '--------------------------- ADDED
                '"    ,CASE WHEN @FilterTypeId = '" & enApprFilterTypeId.APRL_OVER_ALL & "' THEN CAST(ISNULL(os.finaloverallscore, 0) AS DECIMAL(36,2)) " & _
                '"          WHEN @FilterTypeId = '" & enApprFilterTypeId.APRL_EMPLOYEE & "' THEN CAST(ISNULL(ems.formula_value, 0) AS DECIMAL(36,2)) " & _
                '"          WHEN @FilterTypeId = '" & enApprFilterTypeId.APRL_ASSESSOR & "' THEN CAST(ISNULL(ars.formula_value, 0) AS DECIMAL(36,2)) " & _
                '"          WHEN @FilterTypeId = '" & enApprFilterTypeId.APRL_REVIEWER & "' THEN CAST(ISNULL(rvs.formula_value, 0) AS DECIMAL(36,2)) " & _
                '"     END AS Score "
                'S.SANDEEP |21-AUG-2019| -- END

                objDo.AddParameter("@MALE", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 1, "Male"))
                objDo.AddParameter("@FEMALE", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 2, "Female"))
                objDo.AddParameter("@SL", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 4, "Salary Increment"))
                objDo.AddParameter("@ED", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 5, "Bonus"))
                objDo.AddParameter("@FilterTypeId", SqlDbType.Int, eZeeDataType.INT_SIZE, xFilterBy)
                objDo.AddParameter("@PeriodId", SqlDbType.Int, eZeeDataType.INT_SIZE, iPeriodId)

                If sFilter.Trim.Length > 0 Then
                    StrQ &= " AND " & sFilter
                End If

                Select Case mintAppointmentTypeId
                    Case enAD_Report_Parameter.APP_DATE_FROM
                        If mdtDate1 <> Nothing AndAlso mdtDate2 <> Nothing Then
                            StrQ &= " AND ISNULL(CONVERT(CHAR(8), hremployee_master.appointeddate,112),'') BETWEEN '" & eZeeDate.convertDate(mdtDate1) & "' AND '" & eZeeDate.convertDate(mdtDate2) & "' "
                        End If
                    Case enAD_Report_Parameter.APP_DATE_BEFORE
                        If mdtDate1 <> Nothing Then
                            StrQ &= " AND ISNULL(CONVERT(CHAR(8), hremployee_master.appointeddate,112),'') < '" & eZeeDate.convertDate(mdtDate1) & "' "
                        End If
                    Case enAD_Report_Parameter.APP_DATE_AFTER
                        If mdtDate1 <> Nothing Then
                            StrQ &= " AND ISNULL(CONVERT(CHAR(8), hremployee_master.appointeddate,112),'') > '" & eZeeDate.convertDate(mdtDate1) & "' "
                        End If
                End Select

                dsList = objDo.ExecQuery(StrQ, "List")

                If objDo.ErrorMessage <> "" Then
                    exForce = New Exception(objDo.ErrorNumber & " : " & objDo.ErrorMessage)
                    Throw exForce
                End If


                'S.SANDEEP |24-DEC-2019| -- START
                'ISSUE/ENHANCEMENT : SCORE ROUNDING OPTION
                Dim mDecRoundingFactor As Decimal = 0
                mDecRoundingFactor = GetRoundingFactor(iPeriodId)
                If dsList IsNot Nothing AndAlso dsList.Tables(0).Rows.Count > 0 Then
                    dsList.Tables(0).AsEnumerable().ToList().ForEach(Function(x) PerformingRounding(x, mDecRoundingFactor, "SCORE"))
                End If
                'S.SANDEEP |24-DEC-2019| -- END

                If dsList.Tables("List").Rows.Count > 0 Then
                    Dim frow() As DataRow = dsList.Tables("List").Select("Score >= " & mDecScrFrm & " AND Score <= " & mDecScrTo)
                    If frow.Length > 0 Then
                        mdtTran = frow.CopyToDataTable()
                    End If
                Else
                    mdtTran = dsList.Tables("List").Clone
                End If

            End Using
            'S.SANDEEP [12-JUL-2018] -- END

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "GetNewEmployee_ByAward", mstrModuleName)
        Finally
        End Try
        Return mdtTran
    End Function

    Private Sub Set_Valid_Filter(ByVal dRow As DataRow, ByVal xScore As Decimal, ByVal xScrFrm As Decimal, ByVal xScrTo As Decimal)
        Try
            If xScore > 0 Then
                dRow.Item("Score") = xScore
                If CDec(xScore) >= xScrFrm AndAlso CDec(xScore) <= xScrTo Then
                    dRow.Item("isvalid") = True
                End If
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "Set_Valid_Filter", mstrModuleName)
        Finally
        End Try
    End Sub
    'S.SANDEEP [19 FEB 2015] -- END


    'S.SANDEEP |24-DEC-2019| -- START
    'ISSUE/ENHANCEMENT : SCORE ROUNDING OPTION
    Private Function GetRoundingFactor(ByVal intPeriodId As Integer) As Decimal
        Dim StrQ As String = String.Empty
        Dim iDecRoundingFactor As Decimal = 0
        Try
            Using objDo As New clsDataOperation
                StrQ = "SELECT TOP 1 " & _
                       "    @val = CAST(c.key_value AS DECIMAL(36,4)) " & _
                       "FROM cfcommon_period_tran cpt " & _
                       "    JOIN hrmsConfiguration..cffinancial_year_tran cyt ON cpt.yearunkid = cyt.yearunkid " & _
                       "    JOIN hrmsConfiguration..cfconfiguration c ON cyt.companyunkid = c.companyunkid " & _
                       "WHERE cpt.modulerefid = 5 AND cpt.periodunkid = '" & intPeriodId & "' AND UPPER(c.[key_name]) = 'PASCORINGROUDINGFACTOR' "

                objDo.ClearParameters()
                objDo.AddParameter("@val", SqlDbType.Decimal, eZeeDataType.DECIMAL_SIZE, iDecRoundingFactor, ParameterDirection.InputOutput)

                objDo.ExecNonQuery(StrQ)

                If objDo.ErrorMessage <> "" Then
                    Throw New Exception(objDo.ErrorNumber & " : " & objDo.ErrorMessage)
                End If
                Try
                    iDecRoundingFactor = objDo.GetParameterValue("@val")
                Catch ex As Exception
                    iDecRoundingFactor = 0
                End Try
            End Using
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: GetRoundingFactor; Module Name: " & mstrModuleName)
        Finally
        End Try
        Return iDecRoundingFactor
    End Function

    Private Function PerformingRounding(ByVal x As DataRow, ByVal dblFactor As Decimal, ByVal strColName As String) As Boolean
        Try
            If IsDBNull(x(strColName)) = False Then
                x(strColName) = Rounding.BRound(CDec(x(strColName)), dblFactor)
            Else
                x(strColName) = 0
            End If
            Return True
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: PerformingRounding; Module Name: " & mstrModuleName)
        Finally
        End Try
    End Function
    'S.SANDEEP |24-DEC-2019| -- END

#End Region

    '<Language> This Auto Generated Text Please Do Not Modify it.
#Region " Language & UI Settings "
    Public Shared Sub SetMessages()
        Try
            Language.setMessage(mstrModuleName, 1, "Male")
            Language.setMessage(mstrModuleName, 2, "Female")
            Language.setMessage(mstrModuleName, 3, "Yes")

        Catch Ex As Exception
            DisplayError.Show("-1", Ex.Message, "SetMessages", mstrModuleName)
        End Try
    End Sub
#End Region 'Language & UI Settings
    '</Language>
End Class
