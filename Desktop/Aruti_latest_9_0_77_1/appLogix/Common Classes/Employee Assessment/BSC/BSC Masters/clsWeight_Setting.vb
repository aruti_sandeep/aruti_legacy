﻿'************************************************************************************************************************************
'Class Name : clsHrbscweighted_master.vb
'Purpose      :
'Date            :28-Dec-2012
'Written By :Sandeep Sharma
'Modified   :
'************************************************************************************************************************************

Imports eZeeCommonLib
''' <summary>
''' Purpose: 
''' Developer: Sandeep Sharma
''' </summary>

Public Class clsWeight_Setting
    Private Shared ReadOnly mstrModuleName As String = "clsHrbscweighted_master"
    Dim objDataOperation As clsDataOperation
    Dim mstrMessage As String = ""

#Region " Private variables "

    Private mintWsettingunkid As Integer
    Private mintWeight_Optionid As Integer
    Private mintWeight_Typeid As Integer
    'S.SANDEEP [ 30 MAY 2014 ] -- START
    Private mintPeriodUnkid As Integer
    'S.SANDEEP [ 30 MAY 2014 ] -- END

#End Region

#Region " Properties "

    ''' <summary>
    ''' Purpose: Get Message from Class 
    ''' Modify By: Sandeep Sharma
    ''' </summary>
    Public ReadOnly Property _Message() As String
        Get
            Return mstrMessage
        End Get
    End Property

    ''' <summary>
    ''' Purpose: Get or Set wsettingunkid
    ''' Modify By: Sandeep Sharma
    ''' </summary>
    Public Property _Wsettingunkid() As Integer
        Get
            Return mintWsettingunkid
        End Get
        Set(ByVal value As Integer)
            mintWsettingunkid = value
            Call GetData()
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set weight_optionid
    ''' Modify By: Sandeep Sharma
    ''' </summary>
    Public Property _Weight_Optionid() As Integer
        Get
            Return mintWeight_Optionid
        End Get
        Set(ByVal value As Integer)
            mintWeight_Optionid = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set weight_typeid
    ''' Modify By: Sandeep Sharma
    ''' </summary>
    Public Property _Weight_Typeid() As Integer
        Get
            Return mintWeight_Typeid
        End Get
        Set(ByVal value As Integer)
            mintWeight_Typeid = value
        End Set
    End Property

    'S.SANDEEP [ 30 MAY 2014 ] -- START
    ''' <summary>
    ''' Purpose: Get or Set PeriodId
    ''' Modify By: Sandeep Sharma
    ''' </summary>
    Public Property _PeriodUnkid() As Integer
        Get
            Return mintPeriodUnkid
        End Get
        Set(ByVal value As Integer)
            mintPeriodUnkid = value
            Call GetData()
        End Set
    End Property
    'S.SANDEEP [ 30 MAY 2014 ] -- END

#End Region

#Region " Contructor "

    Public Sub New(Optional ByVal iPeriodId As Integer = 0, Optional ByVal blnGetSetting As Boolean = False) 'S.SANDEEP [ 30 MAY 2014 ] -- START -- END
        If blnGetSetting = True Then
            _Wsettingunkid = 1
        End If
        'S.SANDEEP [ 30 MAY 2014 ] -- START
        If iPeriodId >= 0 Then
            _PeriodUnkid = iPeriodId
        End If
        'S.SANDEEP [ 30 MAY 2014 ]
    End Sub

#End Region

    ''' <summary>
    ''' Modify By: Sandeep Sharma
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>
    Public Sub GetData()
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        objDataOperation = New clsDataOperation

        Try
            If mintPeriodUnkid > 0 Then
                If objDataOperation.RecordCount("SELECT wsettingunkid FROM hrbscweighted_master WHERE ISNULL(periodunkid ,0) = '" & mintPeriodUnkid & "'") <= 0 Then
                    mintPeriodUnkid = 0 : mintWsettingunkid = 1
                End If
            ElseIf mintPeriodUnkid <= 0 Then
                mintWsettingunkid = 1
            End If

            strQ = "SELECT " & _
                          "  wsettingunkid " & _
                          ", weight_optionid " & _
                          ", weight_typeid " & _
                          ", ISNULL(periodunkid ,0) AS periodunkid " & _
                          "FROM hrbscweighted_master "

            If mintPeriodUnkid > 0 Then
                strQ &= "WHERE periodunkid = @periodunkid "
                objDataOperation.AddParameter("@periodunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintPeriodUnkid.ToString)
            ElseIf mintWsettingunkid > 0 Then
                strQ &= "WHERE wsettingunkid = @wsettingunkid "
                objDataOperation.AddParameter("@wsettingunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintWsettingunkid.ToString)
            End If

            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            For Each dtRow As DataRow In dsList.Tables(0).Rows
                mintWsettingunkid = CInt(dtRow.Item("wsettingunkid"))
                mintWeight_Optionid = CInt(dtRow.Item("weight_optionid"))
                mintWeight_Typeid = CInt(dtRow.Item("weight_typeid"))
                mintPeriodUnkid = CInt(dtRow.Item("periodunkid"))
                Exit For
            Next
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: GetData; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
    End Sub

    ''' <summary>
    ''' Modify By: Sandeep Sharma
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>
    Public Function GetList(ByVal strTableName As String, Optional ByVal blnOnlyActive As Boolean = True) As DataSet
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        objDataOperation = New clsDataOperation

        Try
            strQ = "SELECT " & _
                      "  wsettingunkid " & _
                      ", weight_optionid " & _
                      ", weight_typeid " & _
                     "FROM hrbscweighted_master "

            dsList = objDataOperation.ExecQuery(strQ, strTableName)

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: GetList; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
        Return dsList
    End Function

    ''' <summary>
    ''' Modify By: Sandeep Sharma
    ''' </summary>
    ''' <returns>Boolean</returns>
    ''' <purpose> INSERT INTO Database Table (hrbscweighted_master) </purpose>
    Public Function Insert() As Boolean
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        objDataOperation = New clsDataOperation
        objDataOperation.BindTransaction()
        Try
            objDataOperation.AddParameter("@wsettingunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintWsettingunkid.ToString)
            objDataOperation.AddParameter("@weight_optionid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintWeight_Optionid.ToString)
            objDataOperation.AddParameter("@weight_typeid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintWeight_Typeid.ToString)

            strQ = "INSERT INTO hrbscweighted_master ( " & _
                      "  wsettingunkid " & _
                      ", weight_optionid " & _
                      ", weight_typeid" & _
                    ") VALUES (" & _
                      "  @wsettingunkid " & _
                      ", @weight_optionid " & _
                      ", @weight_typeid" & _
                    "); "

            objDataOperation.ExecNonQuery(strQ)

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            'If clsCommonATLog.Insert_AtLog(objDataOperation, 1, "hrbscweighted_master", "wsettingunkid", 1) = False Then
            '    exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
            '    Throw exForce
            'End If

            objDataOperation.ReleaseTransaction(True)

            Return True
        Catch ex As Exception
            objDataOperation.ReleaseTransaction(False)
            Throw New Exception(ex.Message & "; Procedure Name: Insert; Module Name: " & mstrModuleName)
            Return False
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
    End Function

    ''' <summary>
    ''' Modify By: Sandeep Sharma
    ''' </summary>
    ''' <returns>Boolean</returns>
    ''' <purpose> Update Database Table (hrbscweighted_master) </purpose>
    Public Function Update() As Boolean
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        objDataOperation = New clsDataOperation
        objDataOperation.BindTransaction()
        Try
            objDataOperation.AddParameter("@wsettingunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintWsettingunkid.ToString)
            objDataOperation.AddParameter("@weight_optionid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintWeight_Optionid.ToString)
            objDataOperation.AddParameter("@weight_typeid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintWeight_Typeid.ToString)

            strQ = "UPDATE hrbscweighted_master SET " & _
                   "  weight_optionid = @weight_optionid" & _
                   ", weight_typeid = @weight_typeid " & _
                   "WHERE wsettingunkid = @wsettingunkid "

            Call objDataOperation.ExecNonQuery(strQ)

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            'If clsCommonATLog.Insert_AtLog(objDataOperation, 1, "hrbscweighted_master", "wsettingunkid", 1) = False Then
            '    exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
            '    Throw exForce
            'End If

            objDataOperation.ReleaseTransaction(True)

            Return True
        Catch ex As Exception
            objDataOperation.ReleaseTransaction(False)
            Throw New Exception(ex.Message & "; Procedure Name: Update; Module Name: " & mstrModuleName)
            Return False
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
    End Function

    ''' <summary>
    ''' Modify By: Sandeep Sharma
    ''' </summary>
    ''' <returns>Boolean</returns>
    ''' <purpose>To Check Whether Data Present</purpose>
    ''' <param name="intUnkid">Returns the Id of Row Found</param>
    Public Function Is_Setting_Present(Optional ByRef intUnkid As Integer = -1) As Boolean
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        objDataOperation = New clsDataOperation

        Try
            strQ = "SELECT " & _
                      "  wsettingunkid " & _
                      ", weight_optionid " & _
                      ", weight_typeid " & _
                     "FROM hrbscweighted_master "

            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            If dsList.Tables("List").Rows.Count > 0 Then
                intUnkid = CInt(dsList.Tables("List").Rows(0).Item("wsettingunkid"))
            Else
                intUnkid = -1
                Return False
            End If


            Return True

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: Is_Setting_Present; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
    End Function

    ''' <summary>
    ''' Modify By: Sandeep Sharma
    ''' </summary>
    ''' <returns>Boolean</returns>
    ''' <purpose>Returns Assigned Total Weight Based on Each Item</purpose>    
    Public Function Get_Each_Item_Weight(ByVal eWType As enWeight_Types, _
                                         ByVal intUnkid As Integer, _
                                         ByVal intChildUnkid As Integer, _
                                         ByRef mDecAssigned As Decimal, _
                                         ByRef mDecTotal As Decimal) As Decimal
        Dim dsList As DataSet = Nothing
        Dim StrQ As String = ""
        Dim exForce As Exception
        Dim mDecWeight As Decimal = 0
        objDataOperation = New clsDataOperation
        Try
            Select Case eWType
                Case enWeight_Types.WEIGHT_FIELD1
                    StrQ = "SELECT " & _
                           "  hrobjective_master.weight AS W_Total " & _
                           " ,SUM(ISNULL(hrkpi_master.weight,0)) AS W_Assign " & _
                           "FROM hrobjective_master " & _
                           " LEFT JOIN hrkpi_master ON hrobjective_master.objectiveunkid = hrkpi_master.objectiveunkid "
                    If intChildUnkid > 0 Then
                        StrQ &= " AND hrkpi_master.kpiunkid  <> '" & intChildUnkid & "' "
                    End If
                    StrQ &= "WHERE hrobjective_master.objectiveunkid = '" & intUnkid & "' " & _
                            "GROUP BY hrobjective_master.weight "
                Case enWeight_Types.WEIGHT_FIELD2
                    StrQ = "SELECT " & _
                           "  hrkpi_master.weight AS W_Total " & _
                           " ,SUM(ISNULL(hrtarget_master.weight,0)) AS W_Assign " & _
                           "FROM hrkpi_master " & _
                           " LEFT JOIN hrtarget_master ON hrkpi_master.kpiunkid = hrtarget_master.kpiunkid "
                    If intChildUnkid > 0 Then
                        StrQ &= " AND hrtarget_master.targetunkid  <> '" & intChildUnkid & "' "
                    End If
                    StrQ &= "WHERE hrkpi_master.kpiunkid = '" & intUnkid & "' " & _
                           "GROUP BY hrkpi_master.weight "
                Case enWeight_Types.WEIGHT_FIELD3
                    StrQ = "SELECT " & _
                           "  hrtarget_master.weight AS W_Total " & _
                           " ,SUM(ISNULL(hrinitiative_master.weight,0)) AS W_Assign " & _
                           "FROM hrtarget_master " & _
                           " LEFT JOIN hrinitiative_master ON hrtarget_master.targetunkid = hrinitiative_master.targetunkid "
                    If intChildUnkid > 0 Then
                        StrQ &= " AND hrinitiative_master.initiativeunkid  <> '" & intChildUnkid & "' "
                    End If
                    StrQ &= "WHERE hrtarget_master.targetunkid = '" & intUnkid & "' " & _
                           "GROUP BY hrtarget_master.weight "
            End Select

            dsList = objDataOperation.ExecQuery(StrQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            If dsList.Tables("List").Rows.Count > 0 Then
                mDecAssigned = dsList.Tables("List").Rows(0).Item("W_Assign")
                mDecTotal = dsList.Tables("List").Rows(0).Item("W_Total")
            End If

            Return mDecWeight

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: Get_Each_Item_Weight; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
    End Function

    ''' <summary>
    ''' Modify By: Sandeep Sharma
    ''' </summary>
    ''' <returns>Boolean</returns>
    ''' <purpose>Returns Assigned Total Weight Based on Selected Option</purpose>    
    Public Function Get_Weight_BasedOn(ByVal intRefId As Integer, _
                                     ByVal eWType As enWeight_Types, _
                                     ByVal intUnkid As Integer, _
                                     ByRef mDecAssigned As Decimal, _
                                     ByRef mDecTotal As Decimal, _
                                       Optional ByVal intEmpId As Integer = 0, _
                                       Optional ByVal iYearUnkid As Integer = 0, _
                                       Optional ByVal iPeriodId As Integer = 0) As Decimal 'S.SANDEEP [ 25 JUNE 2013 {iYearUnkid}] -- START -- END
        'S.SANDEEP [ 02 NOV 2013 ] -- START {iPeriodId} -- END



        Dim dsList As DataSet = Nothing
        Dim StrQ As String = ""
        Dim exForce As Exception
        objDataOperation = New clsDataOperation
        Try
            'S.SANDEEP [ 25 JUNE 2013 ] -- START
            'ENHANCEMENT : OTHER CHANGES
            If iYearUnkid <= 0 Then iYearUnkid = FinancialYear._Object._YearUnkid
            'S.SANDEEP [ 25 JUNE 2013 ] -- END
            Select Case eWType
                Case enWeight_Types.WEIGHT_FIELD1
                    StrQ = "SELECT " & _
                            " 100 AS W_Total " & _
                            ",ISNULL(SUM(ISNULL(weight,0)),0) AS W_Assign " & _
                           "FROM hrobjective_master WHERE isactive = 1 "
                    If intUnkid > 0 Then
                        StrQ &= " AND hrobjective_master.objectiveunkid <> '" & intUnkid & "' "
                    End If
                Case enWeight_Types.WEIGHT_FIELD2
                    StrQ = "SELECT " & _
                                  " 100 AS W_Total " & _
                                  ",ISNULL(SUM(ISNULL(hrkpi_master.weight,0)),0) AS W_Assign " & _
                              "FROM hrkpi_master " & _
                              " JOIN hrobjective_master ON hrobjective_master.objectiveunkid = hrkpi_master.objectiveunkid " & _
                              "WHERE hrkpi_master.isactive = 1 "
                    If intUnkid > 0 Then
                        StrQ &= " AND hrkpi_master.kpiunkid <> '" & intUnkid & "' "
                    End If
                Case enWeight_Types.WEIGHT_FIELD3
                    StrQ = "SELECT " & _
                                    " 100 AS W_Total " & _
                                    ",ISNULL(SUM(ISNULL(hrtarget_master.weight,0)),0) AS W_Assign " & _
                               "FROM hrtarget_master " & _
                               " JOIN hrobjective_master ON hrobjective_master.objectiveunkid = hrtarget_master.objectiveunkid " & _
                               "WHERE hrtarget_master.isactive = 1 "
                    If intUnkid > 0 Then
                        StrQ &= " AND hrtarget_master.targetunkid <> '" & intUnkid & "' "
                    End If
                Case enWeight_Types.WEIGHT_FIELD5
                    StrQ = "SELECT " & _
                            " 100 AS W_Total " & _
                            ",ISNULL(SUM(ISNULL(hrinitiative_master.weight,0)),0) AS W_Assign " & _
                           "FROM hrinitiative_master " & _
                           " JOIN hrobjective_master ON hrobjective_master.objectiveunkid = hrinitiative_master.objectiveunkid " & _
                           "WHERE hrinitiative_master.isactive = 1 "
                    If intUnkid > 0 Then
                        StrQ &= " AND hrinitiative_master.initiativeunkid <> '" & intUnkid & "' "
                    End If
            End Select
            If eWType <> enWeight_Types.WEIGHT_FIELD1 Then
                StrQ &= " AND hrobjective_master.referenceunkid = " & intRefId
            Else
                StrQ &= " AND referenceunkid = " & intRefId
            End If

            If intEmpId > 0 Then
                If eWType <> enWeight_Types.WEIGHT_FIELD1 Then
                    StrQ &= " AND hrobjective_master.employeeunkid = " & intEmpId
                Else
                    StrQ &= " AND employeeunkid = " & intEmpId
                End If
            End If

            'S.SANDEEP [ 25 JUNE 2013 ] -- START
            'ENHANCEMENT : OTHER CHANGES
            If iYearUnkid > 0 Then
                StrQ &= "AND hrobjective_master.yearunkid = '" & iYearUnkid & "' "
            End If
            'S.SANDEEP [ 25 JUNE 2013 ] -- END


            'S.SANDEEP [ 02 NOV 2013 ] -- START
            If iPeriodId > 0 Then
                StrQ &= "AND hrobjective_master.periodunkid = '" & iPeriodId & "' "
            End If
            'S.SANDEEP [ 02 NOV 2013 ] -- END



            dsList = objDataOperation.ExecQuery(StrQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            If dsList.Tables("List").Rows.Count > 0 Then
                mDecAssigned = dsList.Tables("List").Rows(0).Item("W_Assign")
                mDecTotal = dsList.Tables("List").Rows(0).Item("W_Total")
            End If

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: Get_Weight_BasedOn; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
    End Function

    ''' <summary>
    ''' Modify By: Sandeep Sharma
    ''' </summary>
    ''' <returns>Boolean</returns>
    ''' <purpose>Can User Change the Setting One the Data Inserted In Tables</purpose>   
    Public Function Is_Valid_Setting(ByVal intOptionId As Integer, Optional ByVal intTypeId As Integer = 0) As Boolean
        Dim strQ As String = ""
        Dim exForce As Exception
        Dim iCnt As Integer = 0

        objDataOperation = New clsDataOperation

        Try
            Select Case intOptionId
                Case enWeight_Options.WEIGHT_EACH_ITEM
                    strQ = "SELECT weight FROM hrobjective_master WHERE isactive = 1 AND weight > 0 UNION " & _
                              "SELECT weight FROM hrkpi_master WHERE isactive = 1 AND weight > 0 UNION " & _
                              "SELECT weight FROM hrtarget_master WHERE isactive = 1 AND weight > 0 UNION " & _
                              "SELECT weight FROM hrinitiative_master WHERE isactive = 1 AND weight > 0 "
                Case enWeight_Options.WEIGHT_BASED_ON
                    Select Case intTypeId
                        Case enWeight_Types.WEIGHT_FIELD1
                            strQ = "SELECT weight FROM hrobjective_master WHERE isactive = 1 AND weight > 0 "
                        Case enWeight_Types.WEIGHT_FIELD2
                            strQ = "SELECT weight FROM hrkpi_master WHERE isactive = 1 AND weight > 0 "
                        Case enWeight_Types.WEIGHT_FIELD3
                            strQ = "SELECT weight FROM hrtarget_master WHERE isactive = 1 AND weight > 0 "
                        Case enWeight_Types.WEIGHT_FIELD5
                            strQ = "SELECT weight FROM hrinitiative_master WHERE isactive = 1 AND weight > 0 "
                    End Select
            End Select

            If strQ.Trim.Length > 0 Then
                iCnt = objDataOperation.RecordCount(strQ)
            End If

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            If iCnt > 0 Then
                Return False
            Else
                Return True
            End If

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: Is_Setting_Present; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            objDataOperation = Nothing
        End Try
    End Function

    'S.SHARMA [ 22 JAN 2013 ] -- START
    'ENHANCEMENT : TRA CHANGES
    Public Function Is_FinalSaved(ByVal intObjectiveUnkid As Integer, _
                                  ByVal intEmployeeUnkid As Integer, _
                                  ByVal intYearId As Integer, _
                                  ByVal intPeriodId As Integer, _
                                  ByVal eType As enWeight_Types) As Boolean
        Dim dsList As New DataSet
        Dim exForce As Exception
        Dim blnFlag As Boolean = False
        Dim StrQ As String = ""
        Try
            objDataOperation = New clsDataOperation

            StrQ = "SELECT * FROM "

            'S.SANDEEP [ 19 FEB 2013 ] -- START
            'ENHANCEMENT : TRA CHANGES
            'Select Case eType
            '    Case enWeight_Types.WEIGHT_FIELD1
            '        StrQ &= " hrobjective_master WHERE isactive = 1 "
            '    Case enWeight_Types.WEIGHT_FIELD2
            '        StrQ &= " hrkpi_master " & _
            '                " JOIN hrobjective_master ON hrkpi_master.objectiveunkid = hrobjective_master.objectiveunkid " & _
            '                " WHERE isactive = 1 AND hrobjective_master.objectiveunkid = '" & intObjectiveUnkid & "' "
            '    Case enWeight_Types.WEIGHT_FIELD3
            '        StrQ &= " hrtarget_master " & _
            '                " JOIN hrobjective_master ON hrtarget_master.objectiveunkid = hrobjective_master.objectiveunkid " & _
            '                " WHERE isactive = 1 AND hrobjective_master.objectiveunkid = '" & intObjectiveUnkid & "' "
            '    Case enWeight_Types.WEIGHT_FIELD5
            '        StrQ &= " hrinitiative_master " & _
            '                " JOIN hrobjective_master ON hrinitiative_master.objectiveunkid = hrobjective_master.objectiveunkid " & _
            '                " WHERE isactive = 1 AND hrobjective_master.objectiveunkid = '" & intObjectiveUnkid & "' "
            'End Select
            Select Case eType
                Case enWeight_Types.WEIGHT_FIELD1
                    StrQ &= " hrobjective_master WHERE hrobjective_master.isactive = 1 "
                Case enWeight_Types.WEIGHT_FIELD2
                    StrQ &= " hrkpi_master " & _
                            " JOIN hrobjective_master ON hrkpi_master.objectiveunkid = hrobjective_master.objectiveunkid " & _
                            " WHERE hrkpi_master.isactive = 1 AND hrobjective_master.objectiveunkid = '" & intObjectiveUnkid & "' "
                Case enWeight_Types.WEIGHT_FIELD3
                    StrQ &= " hrtarget_master " & _
                            " JOIN hrobjective_master ON hrtarget_master.objectiveunkid = hrobjective_master.objectiveunkid " & _
                            " WHERE hrtarget_master.isactive = 1 AND hrobjective_master.objectiveunkid = '" & intObjectiveUnkid & "' "
                Case enWeight_Types.WEIGHT_FIELD5
                    StrQ &= " hrinitiative_master " & _
                            " JOIN hrobjective_master ON hrinitiative_master.objectiveunkid = hrobjective_master.objectiveunkid " & _
                            " WHERE hrinitiative_master.isactive = 1 AND hrobjective_master.objectiveunkid = '" & intObjectiveUnkid & "' "
            End Select
            'S.SANDEEP [ 19 FEB 2013 ] -- END

            
            StrQ &= "AND hrobjective_master.isfinal = 1 AND hrobjective_master.employeeunkid = '" & intEmployeeUnkid & "' " & _
                    "AND hrobjective_master.periodunkid = '" & intPeriodId & "' AND hrobjective_master.yearunkid = '" & intYearId & "' "

            dsList = objDataOperation.ExecQuery(StrQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & " : " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            If dsList.Tables(0).Rows.Count > 0 Then
                blnFlag = True
            Else
                blnFlag = False
            End If


            Return blnFlag

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: Is_FinalSaved; Module Name: " & mstrModuleName)
        End Try
    End Function

    Public Function Is_Valid_Weight(ByVal intUnkid As Integer, ByVal mDecWeight As Decimal, ByVal eType As enWeight_Types) As String
        Dim dsList As New DataSet
        Dim exForce As Exception
        Dim StrQ As String = ""
        Dim mStrMsg As String = ""
        Try
            objDataOperation = New clsDataOperation

            Select Case eType
                Case enWeight_Types.WEIGHT_FIELD1
                    'CHECK FOR KPI -- START
                    StrQ &= "SELECT ISNULL(SUM(hrkpi_master.weight),0) AS Weight FROM " & _
                            " hrkpi_master " & _
                            " JOIN hrobjective_master ON hrkpi_master.objectiveunkid = hrobjective_master.objectiveunkid " & _
                            " WHERE hrkpi_master.isactive = 1 AND hrobjective_master.objectiveunkid = '" & intUnkid & "' "
                    'CHECK FOR KPI -- END
                Case enWeight_Types.WEIGHT_FIELD2
                    'CHECK FOR TARGET -- START
                    StrQ &= "SELECT ISNULL(SUM(hrtarget_master.weight),0) AS Weight FROM " & _
                            " hrtarget_master " & _
                            " JOIN hrkpi_master ON hrkpi_master.kpiunkid = hrtarget_master.kpiunkid " & _
                            " WHERE hrtarget_master.isactive = 1 AND hrkpi_master.kpiunkid = '" & intUnkid & "' "
                    'CHECK FOR TARGET -- END
                Case enWeight_Types.WEIGHT_FIELD3
                    'CHECK FOR INITIATIVE -- START
                    StrQ &= "SELECT ISNULL(SUM(hrinitiative_master.weight),0) AS Weight FROM " & _
                            " hrinitiative_master " & _
                            " JOIN hrtarget_master ON hrtarget_master.targetunkid = hrinitiative_master.targetunkid " & _
                            " WHERE hrinitiative_master.isactive = 1 AND hrtarget_master.targetunkid = '" & intUnkid & "' "
                    'CHECK FOR INITIATIVE -- END
            End Select

            dsList = objDataOperation.ExecQuery(StrQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & " : " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            If dsList.Tables(0).Rows.Count > 0 Then
                If mDecWeight < CDec(dsList.Tables(0).Rows(0).Item("Weight")) Then
                    mStrMsg = Language.getMessage(mstrModuleName, 1, "Sorry, You cannot set the given weight [ ") & mDecWeight.ToString & _
                              Language.getMessage(mstrModuleName, 2, " ] as weight of subitems binded to this exceeds the given weight.")
                Else
                    mStrMsg = ""
                End If
            Else
                mStrMsg = ""
            End If

            Return mStrMsg

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: Is_Valid_Weight; Module Name: " & mstrModuleName)
        End Try
    End Function
    'S.SHARMA [ 22 JAN 2013 ] -- END

	'<Language> This Auto Generated Text Please Do Not Modify it.
#Region " Language & UI Settings "
	Public Shared Sub SetMessages()
		Try
			Language.setMessage(mstrModuleName, 1, "Sorry, You cannot set the given weight [")
			Language.setMessage(mstrModuleName, 2, " ] as weight of subitems binded to this exceeds the given weight.")

		Catch Ex As Exception
            DisplayError.Show("-1", Ex.Message, "SetMessages", mstrModuleName)
		End Try
	End Sub
#End Region 'Language & UI Settings
	'</Language>
End Class

'Public Class clsWeight_Setting
'    Private Shared ReadOnly mstrModuleName As String = "clsHrbscweighted_master"
'    Dim objDataOperation As clsDataOperation
'    Dim mstrMessage As String = ""

'#Region " Private variables "

'    Private mintWsettingunkid As Integer
'    Private mintWeight_Optionid As Integer
'    Private mintWeight_Typeid As Integer
'    'S.SANDEEP [ 30 MAY 2014 ] -- START
'    Private mintPeriodUnkid As Integer
'    'S.SANDEEP [ 30 MAY 2014 ] -- END

'#End Region

'#Region " Properties "

'    ''' <summary>
'    ''' Purpose: Get Message from Class 
'    ''' Modify By: Sandeep Sharma
'    ''' </summary>
'    Public ReadOnly Property _Message() As String
'        Get
'            Return mstrMessage
'        End Get
'    End Property

'    ''' <summary>
'    ''' Purpose: Get or Set wsettingunkid
'    ''' Modify By: Sandeep Sharma
'    ''' </summary>
'    Public Property _Wsettingunkid() As Integer
'        Get
'            Return mintWsettingunkid
'        End Get
'        Set(ByVal value As Integer)
'            mintWsettingunkid = value
'        End Set
'    End Property

'    ''' <summary>
'    ''' Purpose: Get or Set weight_optionid
'    ''' Modify By: Sandeep Sharma
'    ''' </summary>
'    Public Property _Weight_Optionid() As Integer
'        Get
'            Return mintWeight_Optionid
'        End Get
'        Set(ByVal value As Integer)
'            mintWeight_Optionid = value
'        End Set
'    End Property

'    ''' <summary>
'    ''' Purpose: Get or Set weight_typeid
'    ''' Modify By: Sandeep Sharma
'    ''' </summary>
'    Public Property _Weight_Typeid() As Integer
'        Get
'            Return mintWeight_Typeid
'        End Get
'        Set(ByVal value As Integer)
'            mintWeight_Typeid = value
'        End Set
'    End Property

'    'S.SANDEEP [ 30 MAY 2014 ] -- START
'    ''' <summary>
'    ''' Purpose: Get or Set PeriodId
'    ''' Modify By: Sandeep Sharma
'    ''' </summary>
'    Public Property _PeriodUnkid() As Integer
'        Get
'            Return mintPeriodUnkid
'        End Get
'        Set(ByVal value As Integer)
'            mintPeriodUnkid = value
'            Call GetData()
'        End Set
'    End Property
'    'S.SANDEEP [ 30 MAY 2014 ] -- END

'#End Region

'#Region " Contructor "

'    Public Sub New(Optional ByVal blnGetSetting As Boolean = False)
'        If blnGetSetting = True Then
'            _Wsettingunkid = 1
'        End If
'    End Sub

'#End Region

'    ''' <summary>
'    ''' Modify By: Sandeep Sharma
'    ''' </summary>
'    ''' <purpose> Assign all Property variable </purpose>
'    Public Sub GetData()
'        Dim dsList As DataSet = Nothing
'        Dim strQ As String = ""
'        Dim exForce As Exception

'        objDataOperation = New clsDataOperation

'        Try
'            strQ = "SELECT " & _
'                      "  wsettingunkid " & _
'                      ", weight_optionid " & _
'                      ", weight_typeid " & _
'                      "FROM hrbscweighted_master " & _
'                      "WHERE wsettingunkid = @wsettingunkid "

'            objDataOperation.AddParameter("@wsettingunkid", SqlDbType.int, eZeeDataType.INT_SIZE, mintWsettingUnkId.ToString)

'            dsList = objDataOperation.ExecQuery(strQ, "List")

'            If objDataOperation.ErrorMessage <> "" Then
'                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
'                Throw exForce
'            End If

'            For Each dtRow As DataRow In dsList.Tables(0).Rows
'                mintwsettingunkid = CInt(dtRow.Item("wsettingunkid"))
'                mintweight_optionid = CInt(dtRow.Item("weight_optionid"))
'                mintweight_typeid = CInt(dtRow.Item("weight_typeid"))
'                Exit For
'            Next
'        Catch ex As Exception
'            Throw New Exception(ex.Message & "; Procedure Name: GetData; Module Name: " & mstrModuleName)
'        Finally
'            exForce = Nothing
'            If dsList IsNot Nothing Then dsList.Dispose()
'            objDataOperation = Nothing
'        End Try
'    End Sub

'    ''' <summary>
'    ''' Modify By: Sandeep Sharma
'    ''' </summary>
'    ''' <purpose> Assign all Property variable </purpose>
'    Public Function GetList(ByVal strTableName As String, Optional ByVal blnOnlyActive As Boolean = True) As DataSet
'        Dim dsList As DataSet = Nothing
'        Dim strQ As String = ""
'        Dim exForce As Exception

'        objDataOperation = New clsDataOperation

'        Try
'            strQ = "SELECT " & _
'                      "  wsettingunkid " & _
'                      ", weight_optionid " & _
'                      ", weight_typeid " & _
'                     "FROM hrbscweighted_master "

'            dsList = objDataOperation.ExecQuery(strQ, strTableName)

'            If objDataOperation.ErrorMessage <> "" Then
'                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
'                Throw exForce
'            End If

'        Catch ex As Exception
'            Throw New Exception(ex.Message & "; Procedure Name: GetList; Module Name: " & mstrModuleName)
'        Finally
'            exForce = Nothing
'            If dsList IsNot Nothing Then dsList.Dispose()
'            objDataOperation = Nothing
'        End Try
'        Return dsList
'    End Function

'    ''' <summary>
'    ''' Modify By: Sandeep Sharma
'    ''' </summary>
'    ''' <returns>Boolean</returns>
'    ''' <purpose> INSERT INTO Database Table (hrbscweighted_master) </purpose>
'    Public Function Insert() As Boolean
'        Dim dsList As DataSet = Nothing
'        Dim strQ As String = ""
'        Dim exForce As Exception

'        objDataOperation = New clsDataOperation
'        objDataOperation.BindTransaction()
'        Try
'            objDataOperation.AddParameter("@wsettingunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintWsettingunkid.ToString)
'            objDataOperation.AddParameter("@weight_optionid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintWeight_Optionid.ToString)
'            objDataOperation.AddParameter("@weight_typeid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintWeight_Typeid.ToString)

'            strQ = "INSERT INTO hrbscweighted_master ( " & _
'                      "  wsettingunkid " & _
'                      ", weight_optionid " & _
'                      ", weight_typeid" & _
'                    ") VALUES (" & _
'                      "  @wsettingunkid " & _
'                      ", @weight_optionid " & _
'                      ", @weight_typeid" & _
'                    "); "

'            objDataOperation.ExecNonQuery(strQ)

'            If objDataOperation.ErrorMessage <> "" Then
'                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
'                Throw exForce
'            End If

'            If clsCommonATLog.Insert_AtLog(objDataOperation, 1, "hrbscweighted_master", "wsettingunkid", 1) = False Then
'                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
'                Throw exForce
'            End If

'            objDataOperation.ReleaseTransaction(True)

'            Return True
'        Catch ex As Exception
'            objDataOperation.ReleaseTransaction(False)
'            Throw New Exception(ex.Message & "; Procedure Name: Insert; Module Name: " & mstrModuleName)
'            Return False
'        Finally
'            exForce = Nothing
'            If dsList IsNot Nothing Then dsList.Dispose()
'            objDataOperation = Nothing
'        End Try
'    End Function

'    ''' <summary>
'    ''' Modify By: Sandeep Sharma
'    ''' </summary>
'    ''' <returns>Boolean</returns>
'    ''' <purpose> Update Database Table (hrbscweighted_master) </purpose>
'    Public Function Update() As Boolean
'        Dim dsList As DataSet = Nothing
'        Dim strQ As String = ""
'        Dim exForce As Exception

'        objDataOperation = New clsDataOperation
'        objDataOperation.BindTransaction()
'        Try
'            objDataOperation.AddParameter("@wsettingunkid", SqlDbType.int, eZeeDataType.INT_SIZE, mintwsettingunkid.ToString)
'            objDataOperation.AddParameter("@weight_optionid", SqlDbType.int, eZeeDataType.INT_SIZE, mintweight_optionid.ToString)
'            objDataOperation.AddParameter("@weight_typeid", SqlDbType.int, eZeeDataType.INT_SIZE, mintweight_typeid.ToString)

'            strQ = "UPDATE hrbscweighted_master SET " & _
'                   "  weight_optionid = @weight_optionid" & _
'                   ", weight_typeid = @weight_typeid " & _
'                   "WHERE wsettingunkid = @wsettingunkid "

'            Call objDataOperation.ExecNonQuery(strQ)

'            If objDataOperation.ErrorMessage <> "" Then
'                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
'                Throw exForce
'            End If

'            If clsCommonATLog.Insert_AtLog(objDataOperation, 1, "hrbscweighted_master", "wsettingunkid", 1) = False Then
'                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
'                Throw exForce
'            End If

'            objDataOperation.ReleaseTransaction(True)

'            Return True
'        Catch ex As Exception
'            objDataOperation.ReleaseTransaction(False)
'            Throw New Exception(ex.Message & "; Procedure Name: Update; Module Name: " & mstrModuleName)
'            Return False
'        Finally
'            exForce = Nothing
'            If dsList IsNot Nothing Then dsList.Dispose()
'            objDataOperation = Nothing
'        End Try
'    End Function

'    ''' <summary>
'    ''' Modify By: Sandeep Sharma
'    ''' </summary>
'    ''' <returns>Boolean</returns>
'    ''' <purpose>To Check Whether Data Present</purpose>
'    ''' <param name="intUnkid">Returns the Id of Row Found</param>
'    Public Function Is_Setting_Present(Optional ByRef intUnkid As Integer = -1) As Boolean
'        Dim dsList As DataSet = Nothing
'        Dim strQ As String = ""
'        Dim exForce As Exception

'        objDataOperation = New clsDataOperation

'        Try
'            strQ = "SELECT " & _
'                      "  wsettingunkid " & _
'                      ", weight_optionid " & _
'                      ", weight_typeid " & _
'                     "FROM hrbscweighted_master "

'            dsList = objDataOperation.ExecQuery(strQ, "List")

'            If objDataOperation.ErrorMessage <> "" Then
'                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
'                Throw exForce
'            End If

'            If dsList.Tables("List").Rows.Count > 0 Then
'                intUnkid = CInt(dsList.Tables("List").Rows(0).Item("wsettingunkid"))
'            Else
'                intUnkid = -1
'                Return False
'            End If


'            Return True

'        Catch ex As Exception
'            Throw New Exception(ex.Message & "; Procedure Name: Is_Setting_Present; Module Name: " & mstrModuleName)
'        Finally
'            exForce = Nothing
'            If dsList IsNot Nothing Then dsList.Dispose()
'            objDataOperation = Nothing
'        End Try
'    End Function

'    ''' <summary>
'    ''' Modify By: Sandeep Sharma
'    ''' </summary>
'    ''' <returns>Boolean</returns>
'    ''' <purpose>Returns Assigned Total Weight Based on Each Item</purpose>    
'    Public Function Get_Each_Item_Weight(ByVal eWType As enWeight_Types, _
'                                         ByVal intUnkid As Integer, _
'                                         ByVal intChildUnkid As Integer, _
'                                         ByRef mDecAssigned As Decimal, _
'                                         ByRef mDecTotal As Decimal) As Decimal
'        Dim dsList As DataSet = Nothing
'        Dim StrQ As String = ""
'        Dim exForce As Exception
'        Dim mDecWeight As Decimal = 0
'        objDataOperation = New clsDataOperation
'        Try
'            Select Case eWType
'                Case enWeight_Types.WEIGHT_FIELD1
'                    StrQ = "SELECT " & _
'                           "  hrobjective_master.weight AS W_Total " & _
'                           " ,SUM(ISNULL(hrkpi_master.weight,0)) AS W_Assign " & _
'                           "FROM hrobjective_master " & _
'                           " LEFT JOIN hrkpi_master ON hrobjective_master.objectiveunkid = hrkpi_master.objectiveunkid "
'                    If intChildUnkid > 0 Then
'                        StrQ &= " AND hrkpi_master.kpiunkid  <> '" & intChildUnkid & "' "
'                    End If
'                    StrQ &= "WHERE hrobjective_master.objectiveunkid = '" & intUnkid & "' " & _
'                            "GROUP BY hrobjective_master.weight "
'                Case enWeight_Types.WEIGHT_FIELD2
'                    StrQ = "SELECT " & _
'                           "  hrkpi_master.weight AS W_Total " & _
'                           " ,SUM(ISNULL(hrtarget_master.weight,0)) AS W_Assign " & _
'                           "FROM hrkpi_master " & _
'                           " LEFT JOIN hrtarget_master ON hrkpi_master.kpiunkid = hrtarget_master.kpiunkid "
'                    If intChildUnkid > 0 Then
'                        StrQ &= " AND hrtarget_master.targetunkid  <> '" & intChildUnkid & "' "
'                    End If
'                    StrQ &= "WHERE hrkpi_master.kpiunkid = '" & intUnkid & "' " & _
'                           "GROUP BY hrkpi_master.weight "
'                Case enWeight_Types.WEIGHT_FIELD3
'                    StrQ = "SELECT " & _
'                           "  hrtarget_master.weight AS W_Total " & _
'                           " ,SUM(ISNULL(hrinitiative_master.weight,0)) AS W_Assign " & _
'                           "FROM hrtarget_master " & _
'                           " LEFT JOIN hrinitiative_master ON hrtarget_master.targetunkid = hrinitiative_master.targetunkid "
'                    If intChildUnkid > 0 Then
'                        StrQ &= " AND hrinitiative_master.initiativeunkid  <> '" & intChildUnkid & "' "
'                    End If
'                    StrQ &= "WHERE hrtarget_master.targetunkid = '" & intUnkid & "' " & _
'                           "GROUP BY hrtarget_master.weight "
'            End Select

'            dsList = objDataOperation.ExecQuery(StrQ, "List")

'            If objDataOperation.ErrorMessage <> "" Then
'                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
'                Throw exForce
'            End If

'            If dsList.Tables("List").Rows.Count > 0 Then
'                mDecAssigned = dsList.Tables("List").Rows(0).Item("W_Assign")
'                mDecTotal = dsList.Tables("List").Rows(0).Item("W_Total")
'            End If

'            Return mDecWeight

'        Catch ex As Exception
'            Throw New Exception(ex.Message & "; Procedure Name: Get_Each_Item_Weight; Module Name: " & mstrModuleName)
'        Finally
'            exForce = Nothing
'            If dsList IsNot Nothing Then dsList.Dispose()
'            objDataOperation = Nothing
'        End Try
'    End Function

'    ''' <summary>
'    ''' Modify By: Sandeep Sharma
'    ''' </summary>
'    ''' <returns>Boolean</returns>
'    ''' <purpose>Returns Assigned Total Weight Based on Selected Option</purpose>    
'    Public Function Get_Weight_BasedOn(ByVal intRefId As Integer, _
'                                     ByVal eWType As enWeight_Types, _
'                                     ByVal intUnkid As Integer, _
'                                     ByRef mDecAssigned As Decimal, _
'                                     ByRef mDecTotal As Decimal, _
'                                       Optional ByVal intEmpId As Integer = 0, _
'                                       Optional ByVal iYearUnkid As Integer = 0, _
'                                       Optional ByVal iPeriodId As Integer = 0) As Decimal 'S.SANDEEP [ 25 JUNE 2013 {iYearUnkid}] -- START -- END
'        'S.SANDEEP [ 02 NOV 2013 ] -- START {iPeriodId} -- END



'        Dim dsList As DataSet = Nothing
'        Dim StrQ As String = ""
'        Dim exForce As Exception
'        objDataOperation = New clsDataOperation
'        Try
'            'S.SANDEEP [ 25 JUNE 2013 ] -- START
'            'ENHANCEMENT : OTHER CHANGES
'            If iYearUnkid <= 0 Then iYearUnkid = FinancialYear._Object._YearUnkid
'            'S.SANDEEP [ 25 JUNE 2013 ] -- END
'            Select Case eWType
'                Case enWeight_Types.WEIGHT_FIELD1
'                    StrQ = "SELECT " & _
'                            " 100 AS W_Total " & _
'                            ",ISNULL(SUM(ISNULL(weight,0)),0) AS W_Assign " & _
'                           "FROM hrobjective_master WHERE isactive = 1 "
'                    If intUnkid > 0 Then
'                        StrQ &= " AND hrobjective_master.objectiveunkid <> '" & intUnkid & "' "
'                    End If
'                Case enWeight_Types.WEIGHT_FIELD2
'                    StrQ = "SELECT " & _
'                                  " 100 AS W_Total " & _
'                                  ",ISNULL(SUM(ISNULL(hrkpi_master.weight,0)),0) AS W_Assign " & _
'                              "FROM hrkpi_master " & _
'                              " JOIN hrobjective_master ON hrobjective_master.objectiveunkid = hrkpi_master.objectiveunkid " & _
'                              "WHERE hrkpi_master.isactive = 1 "
'                    If intUnkid > 0 Then
'                        StrQ &= " AND hrkpi_master.kpiunkid <> '" & intUnkid & "' "
'                    End If
'                Case enWeight_Types.WEIGHT_FIELD3
'                    StrQ = "SELECT " & _
'                                    " 100 AS W_Total " & _
'                                    ",ISNULL(SUM(ISNULL(hrtarget_master.weight,0)),0) AS W_Assign " & _
'                               "FROM hrtarget_master " & _
'                               " JOIN hrobjective_master ON hrobjective_master.objectiveunkid = hrtarget_master.objectiveunkid " & _
'                               "WHERE hrtarget_master.isactive = 1 "
'                    If intUnkid > 0 Then
'                        StrQ &= " AND hrtarget_master.targetunkid <> '" & intUnkid & "' "
'                    End If
'                Case enWeight_Types.WEIGHT_FIELD5
'                    StrQ = "SELECT " & _
'                            " 100 AS W_Total " & _
'                            ",ISNULL(SUM(ISNULL(hrinitiative_master.weight,0)),0) AS W_Assign " & _
'                           "FROM hrinitiative_master " & _
'                           " JOIN hrobjective_master ON hrobjective_master.objectiveunkid = hrinitiative_master.objectiveunkid " & _
'                           "WHERE hrinitiative_master.isactive = 1 "
'                    If intUnkid > 0 Then
'                        StrQ &= " AND hrinitiative_master.initiativeunkid <> '" & intUnkid & "' "
'                    End If
'            End Select
'            If eWType <> enWeight_Types.WEIGHT_FIELD1 Then
'                StrQ &= " AND hrobjective_master.referenceunkid = " & intRefId
'            Else
'                StrQ &= " AND referenceunkid = " & intRefId
'            End If

'            If intEmpId > 0 Then
'                If eWType <> enWeight_Types.WEIGHT_FIELD1 Then
'                    StrQ &= " AND hrobjective_master.employeeunkid = " & intEmpId
'                Else
'                    StrQ &= " AND employeeunkid = " & intEmpId
'                End If
'            End If

'            'S.SANDEEP [ 25 JUNE 2013 ] -- START
'            'ENHANCEMENT : OTHER CHANGES
'            If iYearUnkid > 0 Then
'                StrQ &= "AND hrobjective_master.yearunkid = '" & iYearUnkid & "' "
'            End If
'            'S.SANDEEP [ 25 JUNE 2013 ] -- END


'            'S.SANDEEP [ 02 NOV 2013 ] -- START
'            If iPeriodId > 0 Then
'                StrQ &= "AND hrobjective_master.periodunkid = '" & iPeriodId & "' "
'            End If
'            'S.SANDEEP [ 02 NOV 2013 ] -- END



'            dsList = objDataOperation.ExecQuery(StrQ, "List")

'            If objDataOperation.ErrorMessage <> "" Then
'                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
'                Throw exForce
'            End If

'            If dsList.Tables("List").Rows.Count > 0 Then
'                mDecAssigned = dsList.Tables("List").Rows(0).Item("W_Assign")
'                mDecTotal = dsList.Tables("List").Rows(0).Item("W_Total")
'            End If

'        Catch ex As Exception
'            Throw New Exception(ex.Message & "; Procedure Name: Get_Weight_BasedOn; Module Name: " & mstrModuleName)
'        Finally
'            exForce = Nothing
'            If dsList IsNot Nothing Then dsList.Dispose()
'            objDataOperation = Nothing
'        End Try
'    End Function

'    ''' <summary>
'    ''' Modify By: Sandeep Sharma
'    ''' </summary>
'    ''' <returns>Boolean</returns>
'    ''' <purpose>Can User Change the Setting One the Data Inserted In Tables</purpose>   
'    Public Function Is_Valid_Setting(ByVal intOptionId As Integer, Optional ByVal intTypeId As Integer = 0) As Boolean
'        Dim strQ As String = ""
'        Dim exForce As Exception
'        Dim iCnt As Integer = 0

'        objDataOperation = New clsDataOperation

'        Try
'            Select Case intOptionId
'                Case enWeight_Options.WEIGHT_EACH_ITEM
'                    strQ = "SELECT weight FROM hrobjective_master WHERE isactive = 1 AND weight > 0 UNION " & _
'                              "SELECT weight FROM hrkpi_master WHERE isactive = 1 AND weight > 0 UNION " & _
'                              "SELECT weight FROM hrtarget_master WHERE isactive = 1 AND weight > 0 UNION " & _
'                              "SELECT weight FROM hrinitiative_master WHERE isactive = 1 AND weight > 0 "
'                Case enWeight_Options.WEIGHT_BASED_ON
'                    Select Case intTypeId
'                        Case enWeight_Types.WEIGHT_FIELD1
'                            strQ = "SELECT weight FROM hrobjective_master WHERE isactive = 1 AND weight > 0 "
'                        Case enWeight_Types.WEIGHT_FIELD2
'                            strQ = "SELECT weight FROM hrkpi_master WHERE isactive = 1 AND weight > 0 "
'                        Case enWeight_Types.WEIGHT_FIELD3
'                            strQ = "SELECT weight FROM hrtarget_master WHERE isactive = 1 AND weight > 0 "
'                        Case enWeight_Types.WEIGHT_FIELD5
'                            strQ = "SELECT weight FROM hrinitiative_master WHERE isactive = 1 AND weight > 0 "
'                    End Select
'            End Select

'            If strQ.Trim.Length > 0 Then
'                iCnt = objDataOperation.RecordCount(strQ)
'            End If

'            If objDataOperation.ErrorMessage <> "" Then
'                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
'                Throw exForce
'            End If

'            If iCnt > 0 Then
'                Return False
'            Else
'                Return True
'            End If

'        Catch ex As Exception
'            Throw New Exception(ex.Message & "; Procedure Name: Is_Setting_Present; Module Name: " & mstrModuleName)
'        Finally
'            exForce = Nothing
'            objDataOperation = Nothing
'        End Try
'    End Function

'    'S.SHARMA [ 22 JAN 2013 ] -- START
'    'ENHANCEMENT : TRA CHANGES
'    Public Function Is_FinalSaved(ByVal intObjectiveUnkid As Integer, _
'                                  ByVal intEmployeeUnkid As Integer, _
'                                  ByVal intYearId As Integer, _
'                                  ByVal intPeriodId As Integer, _
'                                  ByVal eType As enWeight_Types) As Boolean
'        Dim dsList As New DataSet
'        Dim exForce As Exception
'        Dim blnFlag As Boolean = False
'        Dim StrQ As String = ""
'        Try
'            objDataOperation = New clsDataOperation

'            StrQ = "SELECT * FROM "

'            'S.SANDEEP [ 19 FEB 2013 ] -- START
'            'ENHANCEMENT : TRA CHANGES
'            'Select Case eType
'            '    Case enWeight_Types.WEIGHT_FIELD1
'            '        StrQ &= " hrobjective_master WHERE isactive = 1 "
'            '    Case enWeight_Types.WEIGHT_FIELD2
'            '        StrQ &= " hrkpi_master " & _
'            '                " JOIN hrobjective_master ON hrkpi_master.objectiveunkid = hrobjective_master.objectiveunkid " & _
'            '                " WHERE isactive = 1 AND hrobjective_master.objectiveunkid = '" & intObjectiveUnkid & "' "
'            '    Case enWeight_Types.WEIGHT_FIELD3
'            '        StrQ &= " hrtarget_master " & _
'            '                " JOIN hrobjective_master ON hrtarget_master.objectiveunkid = hrobjective_master.objectiveunkid " & _
'            '                " WHERE isactive = 1 AND hrobjective_master.objectiveunkid = '" & intObjectiveUnkid & "' "
'            '    Case enWeight_Types.WEIGHT_FIELD5
'            '        StrQ &= " hrinitiative_master " & _
'            '                " JOIN hrobjective_master ON hrinitiative_master.objectiveunkid = hrobjective_master.objectiveunkid " & _
'            '                " WHERE isactive = 1 AND hrobjective_master.objectiveunkid = '" & intObjectiveUnkid & "' "
'            'End Select
'            Select Case eType
'                Case enWeight_Types.WEIGHT_FIELD1
'                    StrQ &= " hrobjective_master WHERE hrobjective_master.isactive = 1 "
'                Case enWeight_Types.WEIGHT_FIELD2
'                    StrQ &= " hrkpi_master " & _
'                            " JOIN hrobjective_master ON hrkpi_master.objectiveunkid = hrobjective_master.objectiveunkid " & _
'                            " WHERE hrkpi_master.isactive = 1 AND hrobjective_master.objectiveunkid = '" & intObjectiveUnkid & "' "
'                Case enWeight_Types.WEIGHT_FIELD3
'                    StrQ &= " hrtarget_master " & _
'                            " JOIN hrobjective_master ON hrtarget_master.objectiveunkid = hrobjective_master.objectiveunkid " & _
'                            " WHERE hrtarget_master.isactive = 1 AND hrobjective_master.objectiveunkid = '" & intObjectiveUnkid & "' "
'                Case enWeight_Types.WEIGHT_FIELD5
'                    StrQ &= " hrinitiative_master " & _
'                            " JOIN hrobjective_master ON hrinitiative_master.objectiveunkid = hrobjective_master.objectiveunkid " & _
'                            " WHERE hrinitiative_master.isactive = 1 AND hrobjective_master.objectiveunkid = '" & intObjectiveUnkid & "' "
'            End Select
'            'S.SANDEEP [ 19 FEB 2013 ] -- END


'            StrQ &= "AND hrobjective_master.isfinal = 1 AND hrobjective_master.employeeunkid = '" & intEmployeeUnkid & "' " & _
'                    "AND hrobjective_master.periodunkid = '" & intPeriodId & "' AND hrobjective_master.yearunkid = '" & intYearId & "' "

'            dsList = objDataOperation.ExecQuery(StrQ, "List")

'            If objDataOperation.ErrorMessage <> "" Then
'                exForce = New Exception(objDataOperation.ErrorNumber & " : " & objDataOperation.ErrorMessage)
'                Throw exForce
'            End If

'            If dsList.Tables(0).Rows.Count > 0 Then
'                blnFlag = True
'            Else
'                blnFlag = False
'            End If


'            Return blnFlag

'        Catch ex As Exception
'            Throw New Exception(ex.Message & "; Procedure Name: Is_FinalSaved; Module Name: " & mstrModuleName)
'        End Try
'    End Function

'    Public Function Is_Valid_Weight(ByVal intUnkid As Integer, ByVal mDecWeight As Decimal, ByVal eType As enWeight_Types) As String
'        Dim dsList As New DataSet
'        Dim exForce As Exception
'        Dim StrQ As String = ""
'        Dim mStrMsg As String = ""
'        Try
'            objDataOperation = New clsDataOperation

'            Select Case eType
'                Case enWeight_Types.WEIGHT_FIELD1
'                    'CHECK FOR KPI -- START
'                    StrQ &= "SELECT ISNULL(SUM(hrkpi_master.weight),0) AS Weight FROM " & _
'                            " hrkpi_master " & _
'                            " JOIN hrobjective_master ON hrkpi_master.objectiveunkid = hrobjective_master.objectiveunkid " & _
'                            " WHERE hrkpi_master.isactive = 1 AND hrobjective_master.objectiveunkid = '" & intUnkid & "' "
'                    'CHECK FOR KPI -- END
'                Case enWeight_Types.WEIGHT_FIELD2
'                    'CHECK FOR TARGET -- START
'                    StrQ &= "SELECT ISNULL(SUM(hrtarget_master.weight),0) AS Weight FROM " & _
'                            " hrtarget_master " & _
'                            " JOIN hrkpi_master ON hrkpi_master.kpiunkid = hrtarget_master.kpiunkid " & _
'                            " WHERE hrtarget_master.isactive = 1 AND hrkpi_master.kpiunkid = '" & intUnkid & "' "
'                    'CHECK FOR TARGET -- END
'                Case enWeight_Types.WEIGHT_FIELD3
'                    'CHECK FOR INITIATIVE -- START
'                    StrQ &= "SELECT ISNULL(SUM(hrinitiative_master.weight),0) AS Weight FROM " & _
'                            " hrinitiative_master " & _
'                            " JOIN hrtarget_master ON hrtarget_master.targetunkid = hrinitiative_master.targetunkid " & _
'                            " WHERE hrinitiative_master.isactive = 1 AND hrtarget_master.targetunkid = '" & intUnkid & "' "
'                    'CHECK FOR INITIATIVE -- END
'            End Select

'            dsList = objDataOperation.ExecQuery(StrQ, "List")

'            If objDataOperation.ErrorMessage <> "" Then
'                exForce = New Exception(objDataOperation.ErrorNumber & " : " & objDataOperation.ErrorMessage)
'                Throw exForce
'            End If

'            If dsList.Tables(0).Rows.Count > 0 Then
'                If mDecWeight < CDec(dsList.Tables(0).Rows(0).Item("Weight")) Then
'                    mStrMsg = Language.getMessage(mstrModuleName, 1, "Sorry, You cannot set the given weight [ ") & mDecWeight.ToString & _
'                              Language.getMessage(mstrModuleName, 2, " ] as weight of subitems binded to this exceeds the given weight.")
'                Else
'                    mStrMsg = ""
'                End If
'            Else
'                mStrMsg = ""
'            End If

'            Return mStrMsg

'        Catch ex As Exception
'            Throw New Exception(ex.Message & "; Procedure Name: Is_Valid_Weight; Module Name: " & mstrModuleName)
'        End Try
'    End Function
'    'S.SHARMA [ 22 JAN 2013 ] -- END

'	'<Language> This Auto Generated Text Please Do Not Modify it.
'#Region " Language & UI Settings "
'	Public Shared Sub SetMessages()
'		Try
'			Language.setMessage(mstrModuleName, 1, "Sorry, You cannot set the given weight [")
'			Language.setMessage(mstrModuleName, 2, " ] as weight of subitems binded to this exceeds the given weight.")

'		Catch Ex As Exception
'			DisplayError.Show("-1", ex.Message, "SetMessages", mstrModuleName)
'		End Try
'	End Sub
'#End Region 'Language & UI Settings
'	'</Language>
'End Class
