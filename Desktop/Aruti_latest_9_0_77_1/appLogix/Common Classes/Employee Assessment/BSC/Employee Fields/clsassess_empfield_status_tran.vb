﻿'************************************************************************************************************************************
'Class Name : clsassess_empfield_status_tran.vb
'Purpose    :
'Date       : 28-Apr-2023
'Written By : Hemant Morker
'Modified   :
'************************************************************************************************************************************
Imports eZeeCommonLib
''' <summary>
''' Purpose: 
''' Developer: Hemant Morker
''' </summary>
''' 
Public Class clsassess_empfield_status_tran

    Private Const mstrModuleName = "clsassess_empfield_status_tran"
    Dim mstrMessage As String = ""

#Region " Private variables "

    Private mstrTranunkid As String = String.Empty
    Private mintEmployeeunkid As Integer = 0
    Private mintPeriodunkid As Integer = 0
    Private mintEmpfieldunkid As Integer = 0
    Private mintStatustypeid As Integer = 0
    Private mintAssessormasterunkid As Integer = 0
    Private mintAssessoremployeeunkid As Integer = 0
    Private mdtStatus_Date As Date = Nothing
    Private mstrComments As String = String.Empty
    Private mblnIsunlock As Boolean = False
    Private mintUserunkid As Integer = 0
    Private mintLoginemployeeunkid As Integer = 0
    Private mblnSkipApprovalFlow As Boolean = False
    Private mdtEmpFieldStatusTran As DataTable = Nothing
#End Region

#Region " Properties "

    ''' <summary>
    ''' Purpose: Get Message from Class 
    ''' Modify By: Hemant Morker
    ''' </summary>
    Public ReadOnly Property _Message() As String
        Get
            Return mstrMessage
        End Get
    End Property

    Public Property _DtEmpFieldStatusTran() As DataTable
        Get
            Return mdtEmpFieldStatusTran
        End Get
        Set(ByVal value As DataTable)
            mdtEmpFieldStatusTran = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set tranunkid
    ''' Modify By: Hemant Morker
    ''' </summary>
    Public Property _Tranunkid() As String
        Get
            Return mstrTranunkid
        End Get
        Set(ByVal value As String)
            mstrTranunkid = value
            Call GetData()
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set employeeunkid
    ''' Modify By: Hemant Morker
    ''' </summary>
    Public Property _Employeeunkid() As Integer
        Get
            Return mintEmployeeunkid
        End Get
        Set(ByVal value As Integer)
            mintEmployeeunkid = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set periodunkid
    ''' Modify By: Hemant Morker
    ''' </summary>
    Public Property _Periodunkid() As Integer
        Get
            Return mintPeriodunkid
        End Get
        Set(ByVal value As Integer)
            mintPeriodunkid = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set empfieldunkid
    ''' Modify By: Hemant Morker
    ''' </summary>
    Public Property _Empfieldunkid() As Integer
        Get
            Return mintEmpfieldunkid
        End Get
        Set(ByVal value As Integer)
            mintEmpfieldunkid = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set statustypeid
    ''' Modify By: Hemant Morker
    ''' </summary>
    Public Property _Statustypeid() As Integer
        Get
            Return mintStatustypeid
        End Get
        Set(ByVal value As Integer)
            mintStatustypeid = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set assessormasterunkid
    ''' Modify By: Hemant Morker
    ''' </summary>
    Public Property _Assessormasterunkid() As Integer
        Get
            Return mintAssessormasterunkid
        End Get
        Set(ByVal value As Integer)
            mintAssessormasterunkid = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set assessoremployeeunkid
    ''' Modify By: Hemant Morker
    ''' </summary>
    Public Property _Assessoremployeeunkid() As Integer
        Get
            Return mintAssessoremployeeunkid
        End Get
        Set(ByVal value As Integer)
            mintAssessoremployeeunkid = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set status_date
    ''' Modify By: Hemant Morker
    ''' </summary>
    Public Property _Status_Date() As Date
        Get
            Return mdtStatus_Date
        End Get
        Set(ByVal value As Date)
            mdtStatus_Date = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set comments
    ''' Modify By: Hemant Morker
    ''' </summary>
    Public Property _Comments() As String
        Get
            Return mstrComments
        End Get
        Set(ByVal value As String)
            mstrComments = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set isunlock
    ''' Modify By: Hemant Morker
    ''' </summary>
    Public Property _Isunlock() As Boolean
        Get
            Return mblnIsunlock
        End Get
        Set(ByVal value As Boolean)
            mblnIsunlock = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set userunkid
    ''' Modify By: Hemant Morker
    ''' </summary>
    Public Property _Userunkid() As Integer
        Get
            Return mintUserunkid
        End Get
        Set(ByVal value As Integer)
            mintUserunkid = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set loginemployeeunkid
    ''' Modify By: Hemant Morker
    ''' </summary>
    Public Property _Loginemployeeunkid() As Integer
        Get
            Return mintLoginemployeeunkid
        End Get
        Set(ByVal value As Integer)
            mintLoginemployeeunkid = value
        End Set
    End Property


    Public WriteOnly Property _SkipApprovalFlow() As Boolean
        Set(ByVal value As Boolean)
            mblnSkipApprovalFlow = value
        End Set
    End Property

#End Region

#Region " Constuctor "
    Public Sub New()
        mdtEmpFieldStatusTran = New DataTable()
        Dim dCol As DataColumn
        Try

            dCol = New DataColumn("employeeunkid")
            dCol.DataType = System.Type.GetType("System.Int32")
            mdtEmpFieldStatusTran.Columns.Add(dCol)

            dCol = New DataColumn("periodunkid")
            dCol.DataType = System.Type.GetType("System.Int32")
            mdtEmpFieldStatusTran.Columns.Add(dCol)

            dCol = New DataColumn("empfieldunkid")
            dCol.DataType = System.Type.GetType("System.Int32")
            mdtEmpFieldStatusTran.Columns.Add(dCol)

            dCol = New DataColumn("statustypeid")
            dCol.DataType = System.Type.GetType("System.Int32")
            mdtEmpFieldStatusTran.Columns.Add(dCol)

            dCol = New DataColumn("assessormasterunkid")
            dCol.DataType = System.Type.GetType("System.Int32")
            mdtEmpFieldStatusTran.Columns.Add(dCol)

            dCol = New DataColumn("assessoremployeeunkid")
            dCol.DataType = System.Type.GetType("System.Int32")
            mdtEmpFieldStatusTran.Columns.Add(dCol)
            
            dCol = New DataColumn("comments")
            dCol.DataType = System.Type.GetType("System.String")
            mdtEmpFieldStatusTran.Columns.Add(dCol)

            dCol = New DataColumn("isunlock")
            dCol.DataType = System.Type.GetType("System.Boolean")
            mdtEmpFieldStatusTran.Columns.Add(dCol)

            dCol = New DataColumn("userunkid")
            dCol.DataType = System.Type.GetType("System.Int32")
            mdtEmpFieldStatusTran.Columns.Add(dCol)

            dCol = New DataColumn("loginemployeeunkid")
            dCol.DataType = System.Type.GetType("System.Int32")
            mdtEmpFieldStatusTran.Columns.Add(dCol)

            dCol = New DataColumn("isapprovalskipped")
            dCol.DataType = System.Type.GetType("System.Boolean")
            mdtEmpFieldStatusTran.Columns.Add(dCol)

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "New", mstrModuleName)
        End Try
    End Sub
#End Region

    ''' <summary>
    ''' Modify By: Hemant Morker
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>
    Public Sub GetData()
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        Dim objDataOperation As New clsDataOperation

        Try
            strQ = "SELECT " & _
              "  tranunkid " & _
              ", employeeunkid " & _
              ", periodunkid " & _
              ", empfieldunkid " & _
              ", statustypeid " & _
              ", assessormasterunkid " & _
              ", assessoremployeeunkid " & _
              ", status_date " & _
              ", comments " & _
              ", isunlock " & _
              ", userunkid " & _
              ", loginemployeeunkid " & _
              ", ISNULL(isapprovalskipped,0) AS isapprovalskipped " & _
             "FROM hrassess_empfield_status_tran " & _
             "WHERE tranunkid = @tranunkid "

            objDataOperation.AddParameter("@tranunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mstrTranunkid.ToString)

            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            For Each dtRow As DataRow In dsList.Tables(0).Rows
                mstrTranunkid = CStr(dtRow.Item("tranunkid"))
                mintEmployeeunkid = CInt(dtRow.Item("employeeunkid"))
                mintPeriodunkid = CInt(dtRow.Item("periodunkid"))
                mintStatustypeid = CInt(dtRow.Item("statustypeid"))
                mintEmpfieldunkid = CInt(dtRow.Item("empfieldunkid "))
                mintAssessormasterunkid = CInt(dtRow.Item("assessormasterunkid"))
                mintAssessoremployeeunkid = CInt(dtRow.Item("assessoremployeeunkid"))
                mdtStatus_Date = dtRow.Item("status_date")
                mstrComments = dtRow.Item("comments").ToString
                mblnIsunlock = CBool(dtRow.Item("isunlock"))
                mintUserunkid = CInt(dtRow.Item("userunkid"))
                mintLoginemployeeunkid = CInt(dtRow.Item("loginemployeeunkid"))
                mblnSkipApprovalFlow = CBool(dtRow.Item("isapprovalskipped"))

                Exit For
            Next
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: GetData; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
    End Sub

    ''' <summary>
    ''' Modify By: Hemant Morker
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>
    Public Function isStatusExists(ByVal iEmpUnkid As Integer _
                                   , ByVal iPeriodUnkid As Integer _
                                   , ByVal iEmpFieldUnkid As Integer _
                                   ) As Boolean
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception
        Dim objDataOperation As New clsDataOperation
        Dim blnFlag As Boolean = False
        Try
            strQ = "SELECT TOP 1 " & _
                   "  tranunkid " & _
                   ", employeeunkid " & _
                   ", yearunkid " & _
                   ", periodunkid " & _
                   ", empfieldunkid " & _
                   ", statustypeid " & _
                   ", assessormasterunkid " & _
                   ", assessoremployeeunkid " & _
                   ", status_date " & _
                   ", comments " & _
                   "FROM hrassess_empfield_status_tran WITH (NOLOCK) " & _
                   "WHERE employeeunkid = @employeeunkid " & _
                   " AND periodunkid = @periodunkid " & _
                   " AND empfieldunkid = @empfieldunkid " & _
                   "ORDER BY status_date DESC "

            objDataOperation.AddParameter("@employeeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, iEmpUnkid)
            objDataOperation.AddParameter("@periodunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, iPeriodUnkid)
            objDataOperation.AddParameter("@empfieldunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, iEmpFieldUnkid)

            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            If dsList.Tables(0).Rows.Count > 0 Then
                Select Case CInt(dsList.Tables(0).Rows(0).Item("statustypeid"))
                    Case enObjective_Status.SUBMIT_APPROVAL, enObjective_Status.FINAL_SAVE
                        blnFlag = True
                End Select
            End If

            Return blnFlag

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "isStatusExists", mstrModuleName)
            Return True
        End Try
    End Function

    Public Function isExist(ByVal iEmpUnkid As Integer _
                            , ByVal iPeriodUnkid As Integer _
                            , ByVal iEmpFieldUnkid As Integer _
                            , ByVal iStatusId As Integer _
                            , Optional ByVal iDataOpr As clsDataOperation = Nothing _
                            ) As Boolean
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception
        Dim blnFlag As Boolean = False
        Dim objDataOperation As clsDataOperation
        If iDataOpr Is Nothing Then
            objDataOperation = New clsDataOperation
        Else
            objDataOperation = iDataOpr
        End If
        Try
            strQ = "SELECT TOP 1 " & _
                   "  tranunkid " & _
                   ", employeeunkid " & _
                   ", periodunkid " & _
                   ", empfieldunkid " & _
                   ", statustypeid " & _
                   ", assessormasterunkid " & _
                   ", assessoremployeeunkid " & _
                   ", status_date " & _
                   ", comments " & _
                   "FROM hrassess_empfield_status_tran WITH (NOLOCK) " & _
                   "WHERE employeeunkid = @employeeunkid " & _
                   " AND periodunkid = @periodunkid " & _
                   " AND empfieldunkid = @empfieldunkid " & _
                   " ORDER BY status_date DESC "

            objDataOperation.ClearParameters()
            objDataOperation.AddParameter("@employeeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, iEmpUnkid)
            objDataOperation.AddParameter("@periodunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, iPeriodUnkid)
            objDataOperation.AddParameter("@empfieldunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, iEmpFieldUnkid)

            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            If dsList.Tables(0).Rows.Count > 0 Then
                If dsList.Tables(0).Rows(0).Item("statustypeid") = iStatusId Then
                    blnFlag = True
                Else
                    blnFlag = False
                End If
            Else
                blnFlag = False
            End If

            Return blnFlag

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: isExist; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
        End Try
    End Function

    ''' <summary>
    ''' Modify By: Hemant Morker
    ''' </summary>
    ''' <returns>Boolean</returns>
    ''' <purpose> INSERT INTO Database Table (hrassess_empfield_status_tran) </purpose>
    Public Function Insert(Optional ByVal iDataOpr As clsDataOperation = Nothing _
                           ) As Boolean

        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception
        Dim objDataOperation As clsDataOperation = Nothing

        If iDataOpr Is Nothing Then
            objDataOperation = New clsDataOperation
        Else
            objDataOperation = iDataOpr
            objDataOperation.ClearParameters()
        End If

        Dim strQuery1 As String = String.Empty
        Dim strQuery2 As String = String.Empty
        Try
            strQuery1 = "INSERT INTO hrassess_empfield_status_tran ( " & _
                                          "  tranunkid " & _
                                          ", status_date " & _
                                          ", employeeunkid " & _
                                          ", periodunkid " & _
                                          ", empfieldunkid " & _
                                          ", statustypeid " & _
                                          ", assessormasterunkid " & _
                                          ", assessoremployeeunkid " & _
                                          ", comments " & _
                                          ", isunlock " & _
                                          ", userunkid " & _
                                          ", loginemployeeunkid" & _
                                          ", isapprovalskipped " & _
                                      ") VALUES "

            For Each drRow As DataRow In mdtEmpFieldStatusTran.Rows
                
                Dim fields As String() = drRow.ItemArray.Select(Function(field) String.Concat("'", field.ToString(), "'")).ToArray()
                Dim strCommaSeparatedValues As String = String.Join(",", fields)

                strQuery2 = strQuery2 & ",( NEWID(), GETDATE(), " & strCommaSeparatedValues & ")"
               
            Next

            strQuery2 = strQuery2.Substring(1)

            strQ = strQuery1 & strQuery2

            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            Return True

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: Insert; Module Name: " & mstrModuleName)
            Return False
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
        End Try
    End Function

    Public Function Get_Last_StatusId(ByVal xEmployeeId As Integer _
                                      , ByVal xPeriodId As Integer _
                                      , ByVal xEmpField1Id As Integer _
                                      , ByRef xStatusTranId As Integer _
                                      , Optional ByVal xDataOpr As clsDataOperation = Nothing _
                                      ) As Integer
        Dim StrQ As String = String.Empty
        Dim xLastStatusId As Integer = 0
        Dim objDataOperation As clsDataOperation
        If xDataOpr Is Nothing Then
            objDataOperation = New clsDataOperation
        Else
            objDataOperation = xDataOpr
            objDataOperation.ClearParameters()
        End If
        Try
            StrQ = "SELECT TOP 1 " & _
                   "  @LstId = statustypeid " & _
                   ", @TranId = tranunkid " & _
                   " FROM hrassess_empfield_status_tran WITH (NOLOCK) " & _
                   " WHERE employeeunkid = '" & xEmployeeId & "' " & _
                   " AND periodunkid = '" & xPeriodId & "' " & _
                   " AND empfieldunkid = '" & xEmpField1Id & "' " & _
                   " ORDER BY status_date DESC "

            objDataOperation.AddParameter("@LstId", SqlDbType.Int, eZeeDataType.INT_SIZE, xLastStatusId, ParameterDirection.InputOutput)
            objDataOperation.AddParameter("@TranId", SqlDbType.Int, eZeeDataType.INT_SIZE, xStatusTranId, ParameterDirection.InputOutput)

            objDataOperation.ExecNonQuery(StrQ)

            If objDataOperation.ErrorMessage <> "" Then
                Throw New Exception(objDataOperation.ErrorNumber & " : " & objDataOperation.ErrorMessage)
            End If

            xLastStatusId = objDataOperation.GetParameterValue("@LstId")
            xStatusTranId = objDataOperation.GetParameterValue("@StatTranId")

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "Get_Last_StatusId", mstrModuleName)
        Finally
        End Try
        Return xLastStatusId
    End Function

End Class
