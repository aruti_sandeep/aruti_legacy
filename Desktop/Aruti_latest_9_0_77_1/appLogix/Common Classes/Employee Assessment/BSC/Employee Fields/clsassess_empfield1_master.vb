﻿'************************************************************************************************************************************
'Class Name : clsassess_empfield1_master.vb
'Purpose    :
'Date       :13-May-2014
'Written By :Sandeep Sharma
'Modified   :
'************************************************************************************************************************************
Imports eZeeCommonLib
Imports eZee.Common.eZeeForm
Imports System.Globalization

''' <summary>
''' Purpose: 
''' Developer: Sandeep Sharma
''' </summary>
Public Class clsassess_empfield1_master
    Private Shared ReadOnly mstrModuleName As String = "clsassess_empfield1_master"
    Dim objDataOperation As clsDataOperation
    Dim mstrMessage As String = ""

#Region " Private variables "

    Private mintEmpfield1unkid As Integer = 0
    Private mintOwrfield1unkid As Integer = 0
    Private mintEmployeeunkid As Integer = 0
    Private mintFieldunkid As Integer = 0
    Private mstrField_Data As String = String.Empty
    Private mintYearunkid As Integer = 0
    Private mintPeriodunkid As Integer = 0
    Private mdblWeight As Double = 0
    Private mblnIsfinal As Boolean = False
    Private mdtStartdate As Date = Nothing
    Private mdtEnddate As Date = Nothing
    Private mintStatusunkid As Integer = 0
    Private mintUserunkid As Integer = 0
    Private mblnIsvoid As Boolean = False
    Private mintVoiduserunkid As Integer = 0
    Private mstrVoidreason As String = String.Empty
    Private mdtVoiddatetime As Date = Nothing
    Private mintEmpFieldTypeId As Integer = 0
    Private mDecPct_Completed As Decimal = 0
    Private mstrPeriodName As String = String.Empty
    Private mstrEmployeeName As String = String.Empty
    Private mstrWebFrmName As String = String.Empty
    Private mintPerspectiveunkid As Integer = 0
    Private dtOldValue As DataTable = Nothing
    Private mintLoginemployeeunkid As Integer = 0

    'S.SANDEEP [05 DEC 2015] -- START
    Private mintAssessorMasterId As Integer = 0
    Private mintAssessorEmployeeId As Integer = 0
    Private mintStatusTypeId As Integer = 0
    Private mblnSkipApprovalFlow As Boolean = False
    'S.SANDEEP [05 DEC 2015] -- END

    'S.SANDEEP [01-OCT-2018] -- START
    'ISSUE/ENHANCEMENT : {Ref#2585|ARUTI-}
    Private mintGoalTypeid As Integer = CInt(enGoalType.GT_QUALITATIVE)
    Private mdblGoalValue As Decimal = 0
    'S.SANDEEP [01-OCT-2018] -- END

    'S.SANDEEP |12-FEB-2019| -- START
    'ISSUE/ENHANCEMENT : {Performance Assessment Changes}
    Private mintUnitOfMeasure As Integer = 0
    'S.SANDEEP |12-FEB-2019| -- END

#End Region

#Region " Properties "


    'S.SANDEEP [28-May-2018] -- START 
    'ISSUE/ENHANCEMENT : {Audit Trails} 
    Private mstrFormName As String = String.Empty
    Public WriteOnly Property _FormName() As String
        Set(ByVal value As String)
            mstrFormName = value
        End Set
    End Property

    Private mstrClientIP As String = ""
    Public WriteOnly Property _ClientIP() As String
        Set(ByVal value As String)
            mstrClientIP = value
        End Set
    End Property

    Private mstrHostName As String = ""
    Public WriteOnly Property _HostName() As String
        Set(ByVal value As String)
            mstrHostName = value
        End Set
    End Property

    Private mblnIsWeb As Boolean = False
    Public WriteOnly Property _FromWeb() As Boolean
        Set(ByVal value As Boolean)
            mblnIsWeb = value
        End Set
    End Property

    Private mintAuditUserId As Integer = 0
    Public WriteOnly Property _AuditUserId() As Integer
        Set(ByVal value As Integer)
            mintAuditUserId = value
        End Set
    End Property

    Private mintCompanyUnkid As Integer = 0
    Public WriteOnly Property _CompanyUnkid() As Integer
        Set(ByVal value As Integer)
            mintCompanyUnkid = value
        End Set
    End Property

    Private mdtAuditDate As DateTime = Now
    Public WriteOnly Property _AuditDate() As DateTime
        Set(ByVal value As DateTime)
            mdtAuditDate = value
        End Set
    End Property

    Private xDataOpr As clsDataOperation = Nothing
    Public Property _xDataOpr() As clsDataOperation
        Get
            Return xDataOpr
        End Get
        Set(ByVal value As clsDataOperation)
            xDataOpr = value
        End Set
    End Property

    Private mintVoidlogingemployeeunkid As Integer = 0
    Public Property _Voidloginemployeeunkid() As Integer
        Get
            Return mintVoidlogingemployeeunkid
        End Get
        Set(ByVal value As Integer)
            mintVoidlogingemployeeunkid = value
        End Set
    End Property
    'S.SANDEEP [28-May-2018] -- END 

    ''' <summary>
    ''' Purpose: Get Message from Class 
    ''' Modify By: Sandeep Sharma
    ''' </summary>
    Public ReadOnly Property _Message() As String
        Get
            Return mstrMessage
        End Get
    End Property

    ''' <summary>
    ''' Purpose: Get or Set empfield1unkid
    ''' Modify By: Sandeep Sharma
    ''' </summary>
    Public Property _Empfield1unkid() As Integer
        Get
            Return mintEmpfield1unkid
        End Get
        Set(ByVal value As Integer)
            mintEmpfield1unkid = value
            Call GetData()
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set Owrfield1unkid
    ''' Modify By: Sandeep Sharma
    ''' </summary>
    Public Property _Owrfield1unkid() As Integer
        Get
            Return mintOwrfield1unkid
        End Get
        Set(ByVal value As Integer)
            mintOwrfield1unkid = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set employeeunkid
    ''' Modify By: Sandeep Sharma
    ''' </summary>
    Public Property _Employeeunkid() As Integer
        Get
            Return mintEmployeeunkid
        End Get
        Set(ByVal value As Integer)
            mintEmployeeunkid = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set fieldunkid
    ''' Modify By: Sandeep Sharma
    ''' </summary>
    Public Property _Fieldunkid() As Integer
        Get
            Return mintFieldunkid
        End Get
        Set(ByVal value As Integer)
            mintFieldunkid = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set field_data
    ''' Modify By: Sandeep Sharma
    ''' </summary>
    Public Property _Field_Data() As String
        Get
            Return mstrField_Data
        End Get
        Set(ByVal value As String)
            mstrField_Data = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set yearunkid
    ''' Modify By: Sandeep Sharma
    ''' </summary>
    Public Property _Yearunkid() As Integer
        Get
            Return mintYearunkid
        End Get
        Set(ByVal value As Integer)
            mintYearunkid = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set periodunkid
    ''' Modify By: Sandeep Sharma
    ''' </summary>
    Public Property _Periodunkid() As Integer
        Get
            Return mintPeriodunkid
        End Get
        Set(ByVal value As Integer)
            mintPeriodunkid = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set weight
    ''' Modify By: Sandeep Sharma
    ''' </summary>
    Public Property _Weight() As Double
        Get
            Return mdblWeight
        End Get
        Set(ByVal value As Double)
            mdblWeight = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set pct_completed
    ''' Modify By: Sandeep Sharma
    ''' </summary>
    Public Property _Pct_Completed() As Decimal
        Get
            Return mDecPct_Completed
        End Get
        Set(ByVal value As Decimal)
            mDecPct_Completed = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set isfinal
    ''' Modify By: Sandeep Sharma
    ''' </summary>
    Public Property _Isfinal() As Boolean
        Get
            Return mblnIsfinal
        End Get
        Set(ByVal value As Boolean)
            mblnIsfinal = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set startdate
    ''' Modify By: Sandeep Sharma
    ''' </summary>
    Public Property _Startdate() As Date
        Get
            Return mdtStartdate
        End Get
        Set(ByVal value As Date)
            mdtStartdate = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set enddate
    ''' Modify By: Sandeep Sharma
    ''' </summary>
    Public Property _Enddate() As Date
        Get
            Return mdtEnddate
        End Get
        Set(ByVal value As Date)
            mdtEnddate = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set statusunkid
    ''' Modify By: Sandeep Sharma
    ''' </summary>
    Public Property _Statusunkid() As Integer
        Get
            Return mintStatusunkid
        End Get
        Set(ByVal value As Integer)
            mintStatusunkid = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set userunkid
    ''' Modify By: Sandeep Sharma
    ''' </summary>
    Public Property _Userunkid() As Integer
        Get
            Return mintUserunkid
        End Get
        Set(ByVal value As Integer)
            mintUserunkid = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set isvoid
    ''' Modify By: Sandeep Sharma
    ''' </summary>
    Public Property _Isvoid() As Boolean
        Get
            Return mblnIsvoid
        End Get
        Set(ByVal value As Boolean)
            mblnIsvoid = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set voiduserunkid
    ''' Modify By: Sandeep Sharma
    ''' </summary>
    Public Property _Voiduserunkid() As Integer
        Get
            Return mintVoiduserunkid
        End Get
        Set(ByVal value As Integer)
            mintVoiduserunkid = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set voidreason
    ''' Modify By: Sandeep Sharma
    ''' </summary>
    Public Property _Voidreason() As String
        Get
            Return mstrVoidreason
        End Get
        Set(ByVal value As String)
            mstrVoidreason = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set voiddatetime
    ''' Modify By: Sandeep Sharma
    ''' </summary>
    Public Property _Voiddatetime() As Date
        Get
            Return mdtVoiddatetime
        End Get
        Set(ByVal value As Date)
            mdtVoiddatetime = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Set EmpFieldTypeId
    ''' Modify By: Sandeep Sharma
    ''' </summary>
    Public WriteOnly Property _EmpFieldTypeId() As Integer
        Set(ByVal value As Integer)
            mintEmpFieldTypeId = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get Periodname
    ''' Modify By: Sandeep Sharma
    ''' </summary>
    Public ReadOnly Property _PeriodName() As String
        Get
            Return mstrPeriodName
        End Get
    End Property

    ''' <summary>
    ''' Purpose: Get Employee Name
    ''' Modify By: Sandeep Sharma
    ''' </summary>
    Public ReadOnly Property _EmployeeName() As String
        Get
            Return mstrEmployeeName
        End Get
    End Property

    ''' <summary>
    ''' Purpose: Set WebFormName
    ''' Modify By: Sandeep Sharma
    ''' </summary>
    Public WriteOnly Property _WebFrmName() As String
        Set(ByVal value As String)
            mstrWebFrmName = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set perspectiveunkid
    ''' Modify By: Sandeep Sharma
    ''' </summary>
    Public Property _Perspectiveunkid() As Integer
        Get
            Return mintPerspectiveunkid
        End Get
        Set(ByVal value As Integer)
            mintPerspectiveunkid = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Set loginemployeeunkid
    ''' Modify By: Sandeep Sharma
    ''' </summary>
    Public WriteOnly Property _Loginemployeeunkid() As Integer
        Set(ByVal value As Integer)
            mintLoginemployeeunkid = value
        End Set
    End Property


    'S.SANDEEP [05 DEC 2015] -- START
    Public WriteOnly Property _AssessorMasterId() As Integer
        Set(ByVal value As Integer)
            mintAssessorMasterId = value
        End Set
    End Property

    Public WriteOnly Property _AssessorEmployeeId() As Integer
        Set(ByVal value As Integer)
            mintAssessorEmployeeId = value
        End Set
    End Property

    Public WriteOnly Property _StatusTypeId() As Integer
        Set(ByVal value As Integer)
            mintStatusTypeId = value
        End Set
    End Property

    Public WriteOnly Property _SkipApprovalFlow() As Boolean
        Set(ByVal value As Boolean)
            mblnSkipApprovalFlow = value
        End Set
    End Property
    'S.SANDEEP [05 DEC 2015] -- END

    'S.SANDEEP [01-OCT-2018] -- START
    'ISSUE/ENHANCEMENT : {Ref#2585|ARUTI-}
    Public Property _GoalTypeid() As Integer
        Get
            Return mintGoalTypeid
        End Get
        Set(ByVal value As Integer)
            mintGoalTypeid = value
        End Set
    End Property

    Public Property _GoalValue() As Decimal
        Get
            Return mdblGoalValue
        End Get
        Set(ByVal value As Decimal)
            mdblGoalValue = value
        End Set
    End Property
    'S.SANDEEP [01-OCT-2018] -- END

    'S.SANDEEP |12-FEB-2019| -- START
    'ISSUE/ENHANCEMENT : {Performance Assessment Changes}
    Public Property _UnitOfMeasure() As Integer
        Get
            Return mintUnitOfMeasure
        End Get
        Set(ByVal value As Integer)
            mintUnitOfMeasure = value
        End Set
    End Property
    'S.SANDEEP |12-FEB-2019| -- END

#End Region

    ''' <summary>
    ''' Modify By: Sandeep Sharma
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>
    Public Sub GetData(Optional ByVal iDataOpr As clsDataOperation = Nothing)
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception
        Dim objDataOperation As clsDataOperation
        If iDataOpr IsNot Nothing Then
            objDataOperation = iDataOpr
            objDataOperation.ClearParameters()
        Else
            objDataOperation = New clsDataOperation
        End If
        Try
            strQ = "SELECT " & _
                    "  hrassess_empfield1_master.empfield1unkid " & _
                    ", hrassess_empfield1_master.owrfield1unkid " & _
                    ", hrassess_empfield1_master.employeeunkid " & _
                    ", hrassess_empfield1_master.fieldunkid " & _
                    ", hrassess_empfield1_master.field_data " & _
                    ", hrassess_empfield1_master.yearunkid " & _
                    ", hrassess_empfield1_master.periodunkid " & _
                    ", hrassess_empfield1_master.weight " & _
                    ", hrassess_empfield1_master.pct_completed " & _
                    ", hrassess_empfield1_master.isfinal " & _
                    ", hrassess_empfield1_master.startdate " & _
                    ", hrassess_empfield1_master.enddate " & _
                    ", hrassess_empfield1_master.statusunkid " & _
                    ", hrassess_empfield1_master.userunkid " & _
                    ", hrassess_empfield1_master.isvoid " & _
                    ", hrassess_empfield1_master.voiduserunkid " & _
                    ", hrassess_empfield1_master.voidreason " & _
                    ", hrassess_empfield1_master.voiddatetime " & _
                    ", ISNULL(hremployee_master.employeecode,'')+' - '+ISNULL(hremployee_master.firstname,'')+' '+ISNULL(hremployee_master.surname,'') AS employee " & _
                    ", ISNULL(cfcommon_period_tran.period_name,'') AS period_name " & _
                    ", hrassess_empfield1_master.perspectiveunkid " & _
                    ", ISNULL(goaltypeid," & CInt(enGoalType.GT_QUALITATIVE) & ") AS goaltypeid " & _
                    ", ISNULL(goalvalue,0) AS goalvalue " & _
                    ", ISNULL(uomtypeid,0) AS uomtypeid " & _
                "FROM hrassess_empfield1_master " & _
                    " JOIN hremployee_master ON hrassess_empfield1_master.employeeunkid = hremployee_master.employeeunkid " & _
                    " JOIN cfcommon_period_tran ON hrassess_empfield1_master.periodunkid = cfcommon_period_tran.periodunkid " & _
                "WHERE hrassess_empfield1_master.empfield1unkid = @empfield1unkid "
            'S.SANDEEP |12-FEB-2019| -- START {uomtypeid} -- END

            objDataOperation.AddParameter("@empfield1unkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintEmpfield1unkid.ToString)

            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            If dtOldValue Is Nothing Then
                dtOldValue = dsList.Tables(0).Copy
            End If

            For Each dtRow As DataRow In dsList.Tables(0).Rows
                mintEmpfield1unkid = CInt(dtRow.Item("empfield1unkid"))
                mintOwrfield1unkid = CInt(dtRow.Item("owrfield1unkid"))
                mintEmployeeunkid = CInt(dtRow.Item("employeeunkid"))
                mintFieldunkid = CInt(dtRow.Item("fieldunkid"))
                mstrField_Data = dtRow.Item("field_data").ToString
                mintYearunkid = CInt(dtRow.Item("yearunkid"))
                mintPeriodunkid = CInt(dtRow.Item("periodunkid"))
                mdblWeight = CDbl(dtRow.Item("weight"))
                mDecPct_Completed = CDec(dtRow.Item("pct_completed"))
                mblnIsfinal = CBool(dtRow.Item("isfinal"))
                If IsDBNull(dtRow.Item("startdate")) = False Then
                    mdtStartdate = dtRow.Item("startdate")
                Else
                    mdtStartdate = Nothing
                End If
                If IsDBNull(dtRow.Item("enddate")) = False Then
                    mdtEnddate = dtRow.Item("enddate")
                Else
                    mdtEnddate = Nothing
                End If
                mintStatusunkid = CInt(dtRow.Item("statusunkid"))
                mintUserunkid = CInt(dtRow.Item("userunkid"))
                mblnIsvoid = CBool(dtRow.Item("isvoid"))
                mintVoiduserunkid = CInt(dtRow.Item("voiduserunkid"))
                mstrVoidreason = dtRow.Item("voidreason").ToString
                If IsDBNull(dtRow.Item("voiddatetime")) = False Then
                    mdtVoiddatetime = dtRow.Item("voiddatetime")
                Else
                    mdtVoiddatetime = Nothing
                End If
                mstrPeriodName = dtRow.Item("period_name")
                mstrEmployeeName = dtRow.Item("employee")
                mintPerspectiveunkid = CInt(dtRow.Item("perspectiveunkid"))

                'S.SANDEEP [01-OCT-2018] -- START
                'ISSUE/ENHANCEMENT : {Ref#2585|ARUTI-}
                mintGoalTypeid = CInt(dtRow("goaltypeid"))
                mdblGoalValue = CDec(dtRow("goalvalue"))
                'S.SANDEEP [01-OCT-2018] -- END

                'S.SANDEEP |12-FEB-2019| -- START
                'ISSUE/ENHANCEMENT : {Performance Assessment Changes}
                mintUnitOfMeasure = CInt(dtRow("uomtypeid"))
                'S.SANDEEP |12-FEB-2019| -- END

                Exit For
            Next

            'S.SANDEEP [16 JUN 2015] -- START
            Dim dsUpdate As DataSet
            Dim objProgress As New clsassess_empupdate_tran
            objProgress._DataOperation = objDataOperation
            dsUpdate = objProgress.GetLatestProgress(ConfigParameter._Object._CurrentDateAndTime, mintEmployeeunkid, mintPeriodunkid)
            If dsUpdate IsNot Nothing Then
                Dim pRow() As DataRow = dsUpdate.Tables(0).Select("empfieldtypeid = '" & enWeight_Types.WEIGHT_FIELD1 & "' AND empfieldunkid = '" & mintEmpfield1unkid & "'")
                If pRow.Length > 0 Then
                    mDecPct_Completed = pRow(0).Item("pct_completed")
                    mintStatusunkid = pRow(0).Item("statusunkid")
                End If
            End If
            objProgress = Nothing
            'S.SANDEEP [16 JUN 2015] -- END

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: GetData; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            If iDataOpr Is Nothing Then
                objDataOperation = Nothing
            End If
        End Try
    End Sub

    ''' <summary>
    ''' Modify By: Sandeep Sharma
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>
    Public Function GetList(ByVal strTableName As String, Optional ByVal blnOnlyActive As Boolean = True) As DataSet
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        objDataOperation = New clsDataOperation

        Try
            strQ = "SELECT " & _
              "  empfield1unkid " & _
              ", owrfield1unkid " & _
              ", employeeunkid " & _
              ", fieldunkid " & _
              ", field_data " & _
              ", yearunkid " & _
              ", periodunkid " & _
              ", weight " & _
              ", pct_completed " & _
              ", isfinal " & _
              ", startdate " & _
              ", enddate " & _
              ", statusunkid " & _
              ", userunkid " & _
              ", isvoid " & _
              ", voiduserunkid " & _
              ", voidreason " & _
              ", voiddatetime " & _
              ", perspectiveunkid " & _
              ", ISNULL(goaltypeid," & CInt(enGoalType.GT_QUALITATIVE) & ") AS goaltypeid " & _
              ", ISNULL(goalvalue,0) AS goalvalue " & _
              ", ISNULL(uomtypeid,0) AS uomtypeid " & _
             "FROM hrassess_empfield1_master "
            'S.SANDEEP |12-FEB-2019| -- START {uomtypeid} -- END

            If blnOnlyActive Then
                strQ &= " WHERE isvoid = 0 "
            End If

            dsList = objDataOperation.ExecQuery(strQ, strTableName)

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: GetList; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
        Return dsList
    End Function

    ''' <summary>
    ''' Modify By: Sandeep Sharma
    ''' </summary>
    ''' <returns>Boolean</returns>
    ''' <purpose> INSERT INTO Database Table (hrassess_empfield1_master) </purpose>
    Public Function Insert(Optional ByVal mdicInfoFieldData As Dictionary(Of Integer, String) = Nothing, Optional ByVal mdtOwner As DataTable = Nothing) As Boolean
        If isExist(mstrField_Data, , mintEmployeeunkid, mintPeriodunkid) Then
            mstrMessage = Language.getMessage(mstrModuleName, 1, "Sorry, this data is already defined. Please define new data for the selected employee.")
            Return False
        End If

        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        objDataOperation = New clsDataOperation
        objDataOperation.BindTransaction()

        Try
            objDataOperation.AddParameter("@owrfield1unkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintOwrfield1unkid.ToString)
            objDataOperation.AddParameter("@employeeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintEmployeeunkid.ToString)
            objDataOperation.AddParameter("@fieldunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintFieldunkid.ToString)
            objDataOperation.AddParameter("@field_data", SqlDbType.NVarChar, mstrField_Data.Length, mstrField_Data.ToString)
            objDataOperation.AddParameter("@yearunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintYearunkid.ToString)
            objDataOperation.AddParameter("@periodunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintPeriodunkid.ToString)
            objDataOperation.AddParameter("@weight", SqlDbType.Float, eZeeDataType.FLOAT_SIZE, mdblWeight.ToString)
            objDataOperation.AddParameter("@pct_completed", SqlDbType.Decimal, eZeeDataType.DECIMAL_SIZE, mDecPct_Completed)
            objDataOperation.AddParameter("@isfinal", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsfinal.ToString)
            If mdtStartdate <> Nothing Then
                objDataOperation.AddParameter("@startdate", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, mdtStartdate)
            Else
                objDataOperation.AddParameter("@startdate", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, DBNull.Value)
            End If
            If mdtEnddate <> Nothing Then
                objDataOperation.AddParameter("@enddate", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, mdtEnddate)
            Else
                objDataOperation.AddParameter("@enddate", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, DBNull.Value)
            End If
            objDataOperation.AddParameter("@statusunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintStatusunkid.ToString)
            objDataOperation.AddParameter("@userunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintUserunkid.ToString)
            objDataOperation.AddParameter("@isvoid", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsvoid.ToString)
            objDataOperation.AddParameter("@voiduserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintVoiduserunkid.ToString)
            objDataOperation.AddParameter("@voidreason", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrVoidreason.ToString)
            If mdtVoiddatetime <> Nothing Then
                objDataOperation.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, mdtVoiddatetime)
            Else
                objDataOperation.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, DBNull.Value)
            End If
            objDataOperation.AddParameter("@perspectiveunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintPerspectiveunkid.ToString)

            'S.SANDEEP [01-OCT-2018] -- START
            'ISSUE/ENHANCEMENT : {Ref#2585|ARUTI-}
            objDataOperation.AddParameter("@goaltypeid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintGoalTypeid.ToString)
            objDataOperation.AddParameter("@goalvalue", SqlDbType.Decimal, eZeeDataType.DECIMAL_SIZE, mdblGoalValue)
            'S.SANDEEP [01-OCT-2018] -- END

            'S.SANDEEP |12-FEB-2019| -- START
            'ISSUE/ENHANCEMENT : {Performance Assessment Changes}
            objDataOperation.AddParameter("@uomtypeid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintUnitOfMeasure)
            'S.SANDEEP |12-FEB-2019| -- END

            strQ = "INSERT INTO hrassess_empfield1_master ( " & _
              "  owrfield1unkid " & _
              ", employeeunkid " & _
              ", fieldunkid " & _
              ", field_data " & _
              ", yearunkid " & _
              ", periodunkid " & _
              ", weight " & _
              ", pct_completed " & _
              ", isfinal " & _
              ", startdate " & _
              ", enddate " & _
              ", statusunkid " & _
              ", userunkid " & _
              ", isvoid " & _
              ", voiduserunkid " & _
              ", voidreason " & _
              ", voiddatetime " & _
              ", perspectiveunkid" & _
              ", goaltypeid " & _
              ", goalvalue " & _
              ", uomtypeid " & _
            ") VALUES (" & _
              "  @owrfield1unkid " & _
              ", @employeeunkid " & _
              ", @fieldunkid " & _
              ", @field_data " & _
              ", @yearunkid " & _
              ", @periodunkid " & _
              ", @weight " & _
              ", @pct_completed " & _
              ", @isfinal " & _
              ", @startdate " & _
              ", @enddate " & _
              ", @statusunkid " & _
              ", @userunkid " & _
              ", @isvoid " & _
              ", @voiduserunkid " & _
              ", @voidreason " & _
              ", @voiddatetime " & _
              ", @perspectiveunkid" & _
              ", @goaltypeid " & _
              ", @goalvalue " & _
              ", @uomtypeid " & _
            "); SELECT @@identity"
            'S.SANDEEP [01-OCT-2018] -- START {Ref#2585} [goaltypeid,goalvalue] -- END
            'S.SANDEEP |12-FEB-2019| -- START {uomtypeid} -- END

            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            mintEmpfield1unkid = dsList.Tables(0).Rows(0).Item(0)

            Dim objEStatTran As New clsassess_empstatus_tran

            'S.SANDEEP [05 DEC 2015] -- START
            'objEStatTran._Assessoremployeeunkid = 0
            'objEStatTran._Assessormasterunkid = 0
            'objEStatTran._Commtents = ""
            'objEStatTran._Employeeunkid = mintEmployeeunkid
            'objEStatTran._Isunlock = False
            'objEStatTran._Loginemployeeunkid = 0
            'objEStatTran._Periodunkid = mintPeriodunkid
            'objEStatTran._Status_Date = ConfigParameter._Object._CurrentDateAndTime
            'objEStatTran._Statustypeid = enObjective_Status.NOT_SUBMIT
            'objEStatTran._Userunkid = mintUserunkid
            If mblnSkipApprovalFlow = True Then
                objEStatTran._Assessoremployeeunkid = mintAssessorEmployeeId
                objEStatTran._Assessormasterunkid = mintAssessorMasterId
            Else
            objEStatTran._Assessoremployeeunkid = 0
            objEStatTran._Assessormasterunkid = 0
            End If
            
            objEStatTran._Commtents = ""
            objEStatTran._Employeeunkid = mintEmployeeunkid
            objEStatTran._Isunlock = False
            objEStatTran._Loginemployeeunkid = 0
            objEStatTran._Periodunkid = mintPeriodunkid
            objEStatTran._Status_Date = ConfigParameter._Object._CurrentDateAndTime

            'S.SANDEEP [28-May-2018] -- START
            'ISSUE/ENHANCEMENT : {Audit Trails} 
            objEStatTran._FormName = mstrFormName
            objEStatTran._Loginemployeeunkid = mintLoginemployeeunkid
            objEStatTran._ClientIP = mstrClientIP
            objEStatTran._HostName = mstrHostName
            objEStatTran._FromWeb = mblnIsWeb
            objEStatTran._AuditUserId = mintAuditUserId
            objEStatTran._CompanyUnkid = mintCompanyUnkid
            objEStatTran._AuditDate = mdtAuditDate
            'S.SANDEEP [28-May-2018] -- END

            'S.SANDEEP [23 DEC 2015] -- START

            'Shani(16-Feb-2016) -- Start
            '
            If mblnSkipApprovalFlow = False Then
                objEStatTran._Statustypeid = enObjective_Status.NOT_SUBMIT
            End If
            objEStatTran._SkipApprovalFlow = mblnSkipApprovalFlow
            'Shani(16-Feb-2016) -- End

            'S.SANDEEP [23 DEC 2015] -- END

            
            'S.SANDEEP [05 DEC 2015] -- END

            objEStatTran._Userunkid = mintUserunkid
            'S.SANDEEP [05 DEC 2015] -- END

            If objEStatTran.Insert(objDataOperation) = False Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If
            objEStatTran = Nothing

            'S.SANDEEP [28-May-2018] -- START
            'ISSUE/ENHANCEMENT : {Audit Trails} 
            Dim objCommonATLog As New clsCommonATLog
            objCommonATLog._FormName = mstrFormName
            objCommonATLog._LoginEmployeeUnkid = mintLoginemployeeunkid
            objCommonATLog._ClientIP = mstrClientIP
            objCommonATLog._HostName = mstrHostName
            objCommonATLog._FromWeb = mblnIsWeb
            objCommonATLog._AuditUserId = mintAuditUserId
            objCommonATLog._CompanyUnkid = mintCompanyUnkid
            objCommonATLog._AuditDate = mdtAuditDate
            'S.SANDEEP [28-May-2018] -- END

            If objCommonATLog.Insert_AtLog(objDataOperation, 1, "hrassess_empfield1_master", "empfield1unkid", mintEmpfield1unkid, , mintUserunkid) = False Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            'S.SANDEEP [28-May-2018] -- START
            'ISSUE/ENHANCEMENT : {Audit Trails} 
            objCommonATLog = Nothing
            'S.SANDEEP [28-May-2018] -- END


            If mdtOwner IsNot Nothing Then
                Dim objOwner As New clsassess_empowner_tran
                objOwner._DatTable = mdtOwner.Copy
                With objOwner
                    ._FormName = mstrFormName
                    ._LoginEmployeeunkid = mintLoginemployeeunkid
                    ._ClientIP = mstrClientIP
                    ._HostName = mstrHostName
                    ._FromWeb = mblnIsWeb
                    ._AuditUserId = mintAuditUserId
                    ._CompanyUnkid = mintCompanyUnkid
                    ._AuditDate = mdtAuditDate
                End With
                If objOwner.InsertDelete_Owners(objDataOperation, mintUserunkid, mintEmpfield1unkid, mintEmpFieldTypeId) = False Then
                    exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                    Throw exForce
                End If
                objOwner = Nothing
            End If

            If mdicInfoFieldData IsNot Nothing Then
                Dim objInfoField As New clsassess_empinfofield_tran
                objInfoField._dicInfoField = mdicInfoFieldData
                With objInfoField
                    ._FormName = mstrFormName
                    ._LoginEmployeeunkid = mintLoginemployeeunkid
                    ._ClientIP = mstrClientIP
                    ._HostName = mstrHostName
                    ._FromWeb = mblnIsWeb
                    ._AuditUserId = mintAuditUserId
                    ._CompanyUnkid = mintCompanyUnkid
                    ._AuditDate = mdtAuditDate
                End With
                If objInfoField.InsertDelete_InfoField(objDataOperation, mintUserunkid, mintEmpfield1unkid, mintEmpFieldTypeId) = False Then
                    exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                    Throw exForce
                End If
                objInfoField = Nothing
            End If

            objDataOperation.ReleaseTransaction(True)

            Return True
        Catch ex As Exception
            objDataOperation.ReleaseTransaction(False)
            Throw New Exception(ex.Message & "; Procedure Name: Insert; Module Name: " & mstrModuleName)
            Return False
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
    End Function

    ''' <summary>
    ''' Modify By: Sandeep Sharma
    ''' </summary>
    ''' <returns>Boolean</returns>
    ''' <purpose> Update Database Table (hrassess_empfield1_master) </purpose>
    Public Function Update(Optional ByVal mdicInfoFieldData As Dictionary(Of Integer, String) = Nothing, Optional ByVal mdtOwner As DataTable = Nothing) As Boolean
        If isExist(mstrField_Data, mintEmpfield1unkid, mintEmployeeunkid, mintPeriodunkid) Then
            mstrMessage = Language.getMessage(mstrModuleName, 1, "Sorry, this data is already defined. Please define new data for the selected employee.")
            Return False
        End If

        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        objDataOperation = New clsDataOperation
        objDataOperation.BindTransaction()

        Try
            objDataOperation.AddParameter("@empfield1unkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintEmpfield1unkid.ToString)
            objDataOperation.AddParameter("@owrfield1unkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintOwrfield1unkid.ToString)
            objDataOperation.AddParameter("@employeeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintEmployeeunkid.ToString)
            objDataOperation.AddParameter("@fieldunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintFieldunkid.ToString)
            objDataOperation.AddParameter("@field_data", SqlDbType.NVarChar, mstrField_Data.Length, mstrField_Data.ToString)
            objDataOperation.AddParameter("@yearunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintYearunkid.ToString)
            objDataOperation.AddParameter("@periodunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintPeriodunkid.ToString)
            objDataOperation.AddParameter("@weight", SqlDbType.Float, eZeeDataType.FLOAT_SIZE, mdblWeight.ToString)
            objDataOperation.AddParameter("@pct_completed", SqlDbType.Decimal, eZeeDataType.DECIMAL_SIZE, mDecPct_Completed)
            objDataOperation.AddParameter("@isfinal", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsfinal.ToString)
            If mdtStartdate <> Nothing Then
                objDataOperation.AddParameter("@startdate", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, mdtStartdate)
            Else
                objDataOperation.AddParameter("@startdate", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, DBNull.Value)
            End If
            If mdtEnddate <> Nothing Then
                objDataOperation.AddParameter("@enddate", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, mdtEnddate)
            Else
                objDataOperation.AddParameter("@enddate", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, DBNull.Value)
            End If
            objDataOperation.AddParameter("@statusunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintStatusunkid.ToString)
            objDataOperation.AddParameter("@userunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintUserunkid.ToString)
            objDataOperation.AddParameter("@isvoid", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsvoid.ToString)
            objDataOperation.AddParameter("@voiduserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintVoiduserunkid.ToString)
            objDataOperation.AddParameter("@voidreason", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrVoidreason.ToString)
            If mdtVoiddatetime <> Nothing Then
                objDataOperation.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, mdtVoiddatetime)
            Else
                objDataOperation.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, DBNull.Value)
            End If
            objDataOperation.AddParameter("@perspectiveunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintPerspectiveunkid.ToString)

            'S.SANDEEP [01-OCT-2018] -- START
            'ISSUE/ENHANCEMENT : {Ref#2585|ARUTI-}
            objDataOperation.AddParameter("@goaltypeid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintGoalTypeid.ToString)
            objDataOperation.AddParameter("@goalvalue", SqlDbType.Decimal, eZeeDataType.DECIMAL_SIZE, mdblGoalValue)
            'S.SANDEEP [01-OCT-2018] -- END

            'S.SANDEEP |12-FEB-2019| -- START
            'ISSUE/ENHANCEMENT : {Performance Assessment Changes}
            objDataOperation.AddParameter("@uomtypeid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintUnitOfMeasure)
            'S.SANDEEP |12-FEB-2019| -- END

            strQ = "UPDATE hrassess_empfield1_master SET " & _
                    "  owrfield1unkid = @owrfield1unkid" & _
                    ", employeeunkid = @employeeunkid" & _
                    ", fieldunkid = @fieldunkid" & _
                    ", field_data = @field_data" & _
                    ", yearunkid = @yearunkid" & _
                    ", periodunkid = @periodunkid" & _
                    ", weight = @weight" & _
                    ", pct_completed = @pct_completed" & _
                    ", isfinal = @isfinal" & _
                    ", startdate = @startdate" & _
                    ", enddate = @enddate" & _
                    ", statusunkid = @statusunkid" & _
                    ", userunkid = @userunkid" & _
                    ", isvoid = @isvoid" & _
                    ", voiduserunkid = @voiduserunkid" & _
                    ", voidreason = @voidreason" & _
                    ", voiddatetime = @voiddatetime " & _
                    ", perspectiveunkid = @perspectiveunkid " & _
                    ", goaltypeid = @goaltypeid " & _
                    ", goalvalue = @goalvalue " & _
                    ", uomtypeid = @uomtypeid " & _
                   "WHERE empfield1unkid = @empfield1unkid "
            'S.SANDEEP [01-OCT-2018] -- START {Ref#2585} [goaltypeid,goalvalue] -- END
            'S.SANDEEP |12-FEB-2019| -- START {uomtypeid} -- END

            Call objDataOperation.ExecNonQuery(strQ)

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            'S.SANDEEP [28-May-2018] -- START
            'ISSUE/ENHANCEMENT : {Audit Trails} 
            Dim objCommonATLog As New clsCommonATLog
            objCommonATLog._FormName = mstrFormName
            objCommonATLog._LoginEmployeeUnkid = mintLoginemployeeunkid
            objCommonATLog._ClientIP = mstrClientIP
            objCommonATLog._HostName = mstrHostName
            objCommonATLog._FromWeb = mblnIsWeb
            objCommonATLog._AuditUserId = mintAuditUserId
            objCommonATLog._CompanyUnkid = mintCompanyUnkid
            objCommonATLog._AuditDate = mdtAuditDate
            'S.SANDEEP [28-May-2018] -- END

            If objCommonATLog.IsTableDataUpdate("atcommon_log", "hrassess_empfield1_master", mintEmpfield1unkid, "empfield1unkid", 2, objDataOperation) Then
                If objCommonATLog.Insert_AtLog(objDataOperation, 2, "hrassess_empfield1_master", "empfield1unkid", mintEmpfield1unkid, , mintUserunkid) = False Then
                    exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                    Throw exForce
                End If
            End If

            'S.SANDEEP [28-May-2018] -- START
            'ISSUE/ENHANCEMENT : {Audit Trails} 
            objCommonATLog = Nothing
            'S.SANDEEP [28-May-2018] -- END

            _FormName = mstrFormName
            _Loginemployeeunkid = mintLoginemployeeunkid
            _ClientIP = mstrClientIP
            _HostName = mstrHostName
            _FromWeb = mblnIsWeb
            _AuditUserId = mintAuditUserId
            _AuditDate = mdtAuditDate
            'S.SANDEEP |11-MAY-2019| -- START
            'ISSUE/ENHANCEMENT : {Object Reference Error}
            'If Periodic_Review(enAuditType.EDIT, objDataOperation, dtOldValue) = False Then
            '    exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
            '    Throw exForce
            'End If
            If dtOldValue IsNot Nothing Then
                If Periodic_Review(enAuditType.EDIT, objDataOperation, dtOldValue) = False Then
                    exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                    Throw exForce
                End If
            End If
            'S.SANDEEP |11-MAY-2019| -- END
            If mdtOwner IsNot Nothing Then
                Dim objOwner As New clsassess_empowner_tran
                objOwner._DatTable = mdtOwner.Copy
                With objOwner
                    ._FormName = mstrFormName
                    ._LoginEmployeeunkid = mintLoginemployeeunkid
                    ._ClientIP = mstrClientIP
                    ._HostName = mstrHostName
                    ._FromWeb = mblnIsWeb
                    ._AuditUserId = mintAuditUserId
                    ._CompanyUnkid = mintCompanyUnkid
                    ._AuditDate = mdtAuditDate
                End With
                If objOwner.InsertDelete_Owners(objDataOperation, mintUserunkid, mintEmpfield1unkid, mintEmpFieldTypeId) = False Then
                    exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                    Throw exForce
                End If
                objOwner = Nothing
            End If

            If mdicInfoFieldData IsNot Nothing Then
                Dim objInfoField As New clsassess_empinfofield_tran
                objInfoField._dicInfoField = mdicInfoFieldData
                With objInfoField
                    ._FormName = mstrFormName
                    ._LoginEmployeeunkid = mintLoginemployeeunkid
                    ._ClientIP = mstrClientIP
                    ._HostName = mstrHostName
                    ._FromWeb = mblnIsWeb
                    ._AuditUserId = mintAuditUserId
                    ._CompanyUnkid = mintCompanyUnkid
                    ._AuditDate = mdtAuditDate
                End With
                If objInfoField.InsertDelete_InfoField(objDataOperation, mintUserunkid, mintEmpfield1unkid, mintEmpFieldTypeId) = False Then
                    exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                    Throw exForce
                End If
                objInfoField = Nothing
            End If

            'S.SANDEEP |09-JUL-2019| -- START
            'ISSUE/ENHANCEMENT : PA CHANGES
            '==================|START  VOIDING PROGRESS FIELDS IF MAPPED|==============='
            Dim dsChild As New DataSet : Dim dtmp As DataRow()
            If objCommonATLog Is Nothing Then objCommonATLog = New clsCommonATLog
            dsChild = objCommonATLog.GetChildList(objDataOperation, "hrassess_empupdate_tran", "empfieldunkid", mintEmpfield1unkid)
            dtmp = dsChild.Tables(0).Select("empfieldtypeid = '" & enWeight_Types.WEIGHT_FIELD1 & "'")
            If dtmp.Length > 0 Then
                For Each dr As DataRow In dtmp
                    Dim objProgress As New clsassess_empupdate_tran
                    objProgress._DataOperation = objDataOperation
                    objProgress._Isvoid = True
                    objProgress._Voiddatetime = ConfigParameter._Object._CurrentDateAndTime
                    objProgress._Voidreason = Language.getMessage(mstrModuleName, 999, "Voided due to update")
                    objProgress._Voiduserunkid = IIf(mintLoginemployeeunkid <= 0, mintUserunkid, mintLoginemployeeunkid)
                    If objProgress.Delete(dr.Item("empupdatetranunkid")) = False Then
                        exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                        Throw exForce
                    End If
                    objProgress = Nothing
                Next
            End If
            '==================|ENDING  VOIDING PROGRESS FIELDS IF MAPPED|==============='
            'S.SANDEEP |09-JUL-2019| -- END

            objDataOperation.ReleaseTransaction(True)

            Return True
        Catch ex As Exception
            objDataOperation.ReleaseTransaction(False)
            Throw New Exception(ex.Message & "; Procedure Name: Update; Module Name: " & mstrModuleName)
            Return False
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
    End Function

    ''' <summary>
    ''' Modify By: Sandeep Sharma
    ''' </summary>
    ''' <returns>Boolean</returns>
    ''' <purpose> Delete Database Table (hrassess_coyfield1_master) </purpose>
    Public Function Delete(ByVal intUnkid As Integer) As Boolean
        mstrMessage = ""
        If isUsed(intUnkid) Then
            mstrMessage = Language.getMessage(mstrModuleName, 26, "Sorry, you cannot do delete operation. Reason : selected goal is already linked with transactions.")
            Return False
        End If

        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception
        Dim objEmpField2 As New clsassess_empfield2_master

        objDataOperation = New clsDataOperation
        objDataOperation.BindTransaction()
        Try
            strQ = "UPDATE hrassess_empfield1_master SET " & _
                   "  isvoid = @isvoid " & _
                   ", voiduserunkid = @voiduserunkid " & _
                   ", voidreason = @voidreason " & _
                   ", voiddatetime = @voiddatetime " & _
                   "WHERE empfield1unkid = @empfield1unkid "

            objDataOperation.AddParameter("@empfield1unkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intUnkid)
            objDataOperation.AddParameter("@isvoid", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsvoid.ToString)
            objDataOperation.AddParameter("@voiduserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintVoiduserunkid.ToString)
            objDataOperation.AddParameter("@voidreason", SqlDbType.NVarChar, mstrVoidreason.Length, mstrVoidreason.ToString)
            objDataOperation.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, mdtVoiddatetime)

            Call objDataOperation.ExecNonQuery(strQ)

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            'S.SANDEEP [28-May-2018] -- START
            'ISSUE/ENHANCEMENT : {Audit Trails} 
            Dim objCommonATLog As New clsCommonATLog
            objCommonATLog._FormName = mstrFormName
            objCommonATLog._LoginEmployeeUnkid = mintLoginemployeeunkid
            objCommonATLog._ClientIP = mstrClientIP
            objCommonATLog._HostName = mstrHostName
            objCommonATLog._FromWeb = mblnIsWeb
            objCommonATLog._AuditUserId = mintAuditUserId
            objCommonATLog._CompanyUnkid = mintCompanyUnkid
            objCommonATLog._AuditDate = mdtAuditDate
            'S.SANDEEP [28-May-2018] -- END

            If objCommonATLog.Insert_AtLog(objDataOperation, 3, "hrassess_empfield1_master", "empfield1unkid", intUnkid, , mintVoiduserunkid) = False Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            'S.SANDEEP [28-May-2018] -- START
            'ISSUE/ENHANCEMENT : {Audit Trails} 
            objCommonATLog = Nothing
            'S.SANDEEP [28-May-2018] -- END

            objCommonATLog = New clsCommonATLog
            Dim dsChild As New DataSet : Dim dtmp As DataRow() = Nothing
            dsChild = objCommonATLog.GetChildList(objDataOperation, "hrassess_empfield2_master", "empfield1unkid", intUnkid)
            objCommonATLog = Nothing
            dtmp = dsChild.Tables(0).Select("isvoid = 0")
            If dtmp.Length > 0 Then
                For Each dr As DataRow In dtmp
                    objEmpField2._Isvoid = mblnIsvoid
                    objEmpField2._Voiddatetime = mdtVoiddatetime
                    objEmpField2._Voidreason = mstrVoidreason
                    objEmpField2._Voiduserunkid = mintVoiduserunkid

                    objEmpField2._FormName = mstrFormName
                    objEmpField2._Loginemployeeunkid = mintLoginemployeeunkid
                    objEmpField2._ClientIP = mstrClientIP
                    objEmpField2._HostName = mstrHostName
                    objEmpField2._FromWeb = mblnIsWeb
                    objEmpField2._AuditUserId = mintAuditUserId
                    objEmpField2._CompanyUnkid = mintCompanyUnkid
                    objEmpField2._AuditDate = mdtAuditDate

                    If objEmpField2.Delete(dr.Item("empfield2unkid"), objDataOperation) = False Then
                        exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                        Throw exForce
                    End If
                Next
            End If
            objCommonATLog = New clsCommonATLog
            dsChild = objCommonATLog.GetChildList(objDataOperation, "hrassess_empinfofield_tran", "empfieldunkid", intUnkid)
            objCommonATLog = Nothing
            dtmp = dsChild.Tables(0).Select("empfieldtypeid = '" & enWeight_Types.WEIGHT_FIELD1 & "'")
            If dtmp.Length > 0 Then
                For Each dr As DataRow In dtmp
                    'S.SANDEEP [28-May-2018] -- START
                    'ISSUE/ENHANCEMENT : {Audit Trails} 
                    objCommonATLog = New clsCommonATLog
                    objCommonATLog._FormName = mstrFormName
                    objCommonATLog._LoginEmployeeUnkid = mintLoginemployeeunkid
                    objCommonATLog._ClientIP = mstrClientIP
                    objCommonATLog._HostName = mstrHostName
                    objCommonATLog._FromWeb = mblnIsWeb
                    objCommonATLog._AuditUserId = mintAuditUserId
                    objCommonATLog._CompanyUnkid = mintCompanyUnkid
                    objCommonATLog._AuditDate = mdtAuditDate
                    'S.SANDEEP [28-May-2018] -- END

                    If objCommonATLog.Insert_AtLog(objDataOperation, 3, "hrassess_empinfofield_tran", "empinfofieldunkid", dr.Item("empinfofieldunkid"), , mintVoiduserunkid) = False Then
                        exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                        Throw exForce
                    End If

                    'S.SANDEEP [28-May-2018] -- START
                    'ISSUE/ENHANCEMENT : {Audit Trails} 
                    objCommonATLog = Nothing
                    'S.SANDEEP [28-May-2018] -- END

                Next
                strQ = "DELETE FROM hrassess_empinfofield_tran WHERE empfieldunkid = '" & intUnkid & "' AND empfieldtypeid = '" & enWeight_Types.WEIGHT_FIELD1 & "' "

                Call objDataOperation.ExecNonQuery(strQ)

                If objDataOperation.ErrorMessage <> "" Then
                    exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                    Throw exForce
                End If
            End If
            objCommonATLog = New clsCommonATLog
            dsChild = objCommonATLog.GetChildList(objDataOperation, "hrassess_empupdate_tran", "empfieldunkid", intUnkid)
            objCommonATLog = Nothing
            dtmp = dsChild.Tables(0).Select("empfieldtypeid = '" & enWeight_Types.WEIGHT_FIELD1 & "'")
            If dtmp.Length > 0 Then
                For Each dr As DataRow In dtmp
                    Dim objProgress As New clsassess_empupdate_tran

                    objProgress._DataOperation = objDataOperation
                    objProgress._Isvoid = mblnIsvoid
                    objProgress._Voiddatetime = mdtVoiddatetime
                    objProgress._Voidreason = mstrVoidreason
                    objProgress._Voiduserunkid = mintVoiduserunkid

                    objProgress._FormName = mstrFormName
                    objProgress._LoginEmployeeunkid = mintLoginemployeeunkid
                    objProgress._ClientIP = mstrClientIP
                    objProgress._HostName = mstrHostName
                    objProgress._FromWeb = mblnIsWeb
                    objProgress._AuditUserId = mintAuditUserId
                    objProgress._CompanyUnkid = mintCompanyUnkid
                    objProgress._AuditDate = mdtAuditDate

                    If objProgress.Delete(dr.Item("empupdatetranunkid")) = False Then
                        exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                        Throw exForce
                    End If

                    objProgress = Nothing
                Next
            End If

            mintEmpfield1unkid = intUnkid
            Call GetData(objDataOperation)
            _FormName = mstrFormName
            _Loginemployeeunkid = mintLoginemployeeunkid
            _ClientIP = mstrClientIP
            _HostName = mstrHostName
            _FromWeb = mblnIsWeb
            _AuditUserId = mintAuditUserId
            _AuditDate = mdtAuditDate

            'S.SANDEEP |11-MAY-2019| -- START
            'ISSUE/ENHANCEMENT : {Object Reference Error}
            'If Periodic_Review(enAuditType.DELETE, objDataOperation, dtOldValue) = False Then
            '    exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
            '    Throw exForce
            'End If
            If dtOldValue IsNot Nothing Then
            If Periodic_Review(enAuditType.DELETE, objDataOperation, dtOldValue) = False Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If
            End If
            'S.SANDEEP |11-MAY-2019| -- END

            objDataOperation.ReleaseTransaction(True)

            Return True
        Catch ex As Exception
            objDataOperation.ReleaseTransaction(False)
            Throw New Exception(ex.Message & "; Procedure Name: Delete; Module Name: " & mstrModuleName)
            Return False
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
    End Function

    Public Function isUsed(ByVal intUnkid As Integer) As Boolean
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        objDataOperation = New clsDataOperation

        Try
            strQ = "SELECT 1 FROM hrgoals_analysis_tran WITH (NOLOCK) WHERE empfield1unkid = @empfield1unkid AND isvoid = 0"

            objDataOperation.AddParameter("@empfield1unkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intUnkid)

            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            Return dsList.Tables(0).Rows.Count > 0
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: isUsed; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
    End Function

    ''' <summary>
    ''' Modify By: Sandeep Sharma
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>
    Public Function isExist(ByVal strName As String, _
                            Optional ByVal intUnkid As Integer = -1, _
                            Optional ByVal iEmpId As Integer = 0, _
                            Optional ByVal iPeriodId As Integer = 0) As Boolean
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        objDataOperation = New clsDataOperation

        Try
            strQ = "SELECT " & _
              "  empfield1unkid " & _
              ", owrfield1unkid " & _
              ", employeeunkid " & _
              ", fieldunkid " & _
              ", field_data " & _
              ", yearunkid " & _
              ", periodunkid " & _
              ", weight " & _
              ", pct_completed " & _
              ", isfinal " & _
              ", startdate " & _
              ", enddate " & _
              ", statusunkid " & _
              ", userunkid " & _
              ", isvoid " & _
              ", voiduserunkid " & _
              ", voidreason " & _
              ", voiddatetime " & _
              ", perspectiveunkid " & _
              ", ISNULL(goaltypeid," & CInt(enGoalType.GT_QUALITATIVE) & ") AS goaltypeid " & _
              ", ISNULL(goalvalue,0) AS goalvalue " & _
             "FROM hrassess_empfield1_master WITH (NOLOCK) " & _
             "WHERE field_data = @field_data " & _
             "AND isvoid = 0 "

            If intUnkid > 0 Then
                strQ &= " AND empfield1unkid <> @empfield1unkid"
            End If

            If iEmpId > 0 Then
                strQ &= " AND employeeunkid = @employeeunkid "
            End If

            If iPeriodId > 0 Then
                strQ &= " AND periodunkid = @periodunkid "
            End If


            'Shani(06-Feb-2016) -- Start
            'PA Changes Given By Glory for CCBRT
            'objDataOperation.AddParameter("@field_data", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, strName)
            objDataOperation.AddParameter("@field_data", SqlDbType.NVarChar, strName.Length, strName)
            'Shani(06-Feb-2016) -- End

            objDataOperation.AddParameter("@empfield1unkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intUnkid)
            objDataOperation.AddParameter("@employeeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, iEmpId)
            objDataOperation.AddParameter("@periodunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, iPeriodId)

            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            Return dsList.Tables(0).Rows.Count > 0
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: isExist; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
    End Function

    ''' <summary>
    ''' Modify By: Sandeep Sharma
    ''' </summary>
    ''' <purpose> Get Data Set for Combo </purpose>sss
    Public Function getComboList(ByVal iEmpId As Integer, ByVal iPeriodId As Integer, Optional ByVal iList As String = "List", _
                                 Optional ByVal iAddSelect As Boolean = False, Optional ByVal iOnlyCommitted As Boolean = False, Optional ByVal intPerspectiveId As Integer = 0) As DataSet
        'S.SANDEEP |22-MAR-2019| -- START
        'ISSUE/ENHANCEMENT : ISSUE ON PA (TC0007)
        'intPerspectiveId -  ADDED
        'S.SANDEEP |22-MAR-2019| -- END
        Dim dsList As New DataSet
        Dim exForce As Exception
        Dim StrQ As String = String.Empty
        objDataOperation = New clsDataOperation
        Try
            If iAddSelect = True Then
                StrQ = "SELECT 0 AS Id, @Select AS Name UNION "
            End If
            StrQ &= "SELECT empfield1unkid AS Id, field_data AS Name FROM hrassess_empfield1_master WITH (NOLOCK) WHERE isvoid = 0 " & _
                    "AND hrassess_empfield1_master.employeeunkid = '" & iEmpId & "' AND hrassess_empfield1_master.periodunkid = '" & iPeriodId & "' "

            If iOnlyCommitted = True Then
                StrQ &= "AND hrassess_empfield1_master.isfinal  = 1 "
            End If

            'S.SANDEEP |22-MAR-2019| -- START
            'ISSUE/ENHANCEMENT : ISSUE ON PA (TC0007)
            If intPerspectiveId > 0 Then
                StrQ &= "AND hrassess_empfield1_master.perspectiveunkid  = '" & intPerspectiveId & "' "
            End If
            'S.SANDEEP |22-MAR-2019| -- END

            objDataOperation.AddParameter("@Select", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 2, "Select"))

            dsList = objDataOperation.ExecQuery(StrQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            Return dsList
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "getComboList", mstrModuleName)
            Return Nothing
        Finally
            objDataOperation = Nothing
            dsList.Dispose()
        End Try
    End Function

    'Shani (26-Sep-2016) -- Start
    'Enhancement -Changes PA Flow And Add New screen Given By (Andrew)
    'Public Function GetDisplayList(ByVal iEmployeeId As Integer, _
    '                               ByVal iPeriodId As Integer, _
    '                               Optional ByVal iList As String = "", _
    '                               Optional ByVal iCascadingTypeId As Integer = 0) As DataTable
    Public Function GetDisplayList(ByVal iEmployeeId As Integer, _
                                   ByVal iPeriodId As Integer, _
                                   Optional ByVal iList As String = "", _
                                   Optional ByVal iCascadingTypeId As Integer = 0, _
                                   Optional ByVal iIsGoalAccomplishementScreen As Boolean = False) As DataTable
        'Shani (26-Sep-2016) -- End
        'S.SANDEEP [25 MAR 2015] -- START
        'Optional ByVal iCascadingTypeId As Integer = enPACascading.NEITHER_CASCADING_NOR_ALIGNMENT) As DataTable
        If iCascadingTypeId <= 0 Then
            iCascadingTypeId = ConfigParameter._Object._CascadingTypeId
        End If
        'S.SANDEEP [25 MAR 2015] -- END


        Dim StrQ As String = ""
        Dim exForce As Exception
        Dim dsList As New DataSet
        Dim mdtFinal As DataTable = Nothing
        Dim dCol As DataColumn = Nothing
        Dim iCaptionName As String = String.Empty
        'S.SANDEEP [04 MAR 2015] -- START
        Dim iMappedLinkedFieldId As Integer = 0
        Dim xFieldColName As String = String.Empty
        'S.SANDEEP [04 MAR 2015] -- END
        objDataOperation = New clsDataOperation
        Try
            If iList.Trim.Length <= 0 Then iList = "List"
            mdtFinal = New DataTable(iList)

            Dim objFMaster As New clsAssess_Field_Master(True)

            'S.SANDEEP [25 MAR 2015] -- START
                dCol = New DataColumn
                dCol.ColumnName = "perspectiveunkid"
                dCol.Caption = ""
                dCol.DataType = System.Type.GetType("System.Int32")
                dCol.DefaultValue = 0
                mdtFinal.Columns.Add(dCol)

                dCol = New DataColumn
                dCol.ColumnName = "Perspective"
            dCol.Caption = Language.getMessage(mstrModuleName, 27, "Perspective")
                dCol.DataType = System.Type.GetType("System.String")
                dCol.DefaultValue = ""
                dCol.ExtendedProperties.Add(dCol.ColumnName, CInt(clsAssess_Field_Master.enOtherInfoField.PERSPECTIVE))
                mdtFinal.Columns.Add(dCol)
            'S.SANDEEP [25 MAR 2015] -- END

            dCol = New DataColumn
            dCol.ColumnName = "OwrField1"
            dCol.Caption = Language.getMessage(mstrModuleName, 3, "Owner") & " " & objFMaster._Field1_Caption
            dCol.DataType = System.Type.GetType("System.String")
            dCol.DefaultValue = ""
            dCol.ExtendedProperties.Add(dCol.ColumnName, CInt(objFMaster._Field1Unkid))
            mdtFinal.Columns.Add(dCol)

            'S.SANDEEP [25 MAR 2015] -- START
            'If iCascadingTypeId = enPACascading.NEITHER_CASCADING_NOR_ALIGNMENT Then
            '    dCol = New DataColumn
            '    dCol.ColumnName = "perspectiveunkid"
            '    dCol.Caption = ""
            '    dCol.DataType = System.Type.GetType("System.Int32")
            '    dCol.DefaultValue = 0
            '    mdtFinal.Columns.Add(dCol)

            '    dCol = New DataColumn
            '    dCol.ColumnName = "Perspective"
            '    dCol.Caption = Language.getMessage(mstrModuleName, 13, "Perspective")
            '    dCol.DataType = System.Type.GetType("System.String")
            '    dCol.DefaultValue = ""
            '    dCol.ExtendedProperties.Add(dCol.ColumnName, CInt(clsAssess_Field_Master.enOtherInfoField.PERSPECTIVE))
            '    mdtFinal.Columns.Add(dCol)
            'End If
            'S.SANDEEP [25 MAR 2015] -- END

            dCol = New DataColumn
            dCol.ColumnName = "Emp"
            dCol.Caption = Language.getMessage(mstrModuleName, 17, "Employee")
            dCol.DataType = System.Type.GetType("System.String")
            dCol.DefaultValue = ""
            mdtFinal.Columns.Add(dCol)

            Dim objMap As New clsAssess_Field_Mapping
            iCaptionName = objMap.Get_Map_FieldName(iPeriodId)
            'S.SANDEEP [25 MAR 2015] -- START
            iMappedLinkedFieldId = objMap.Get_Map_FieldId(iPeriodId)
            Dim iExOrder As Integer = objFMaster.Get_Field_ExOrder(iMappedLinkedFieldId, True)
            Select Case iExOrder
                Case enWeight_Types.WEIGHT_FIELD1
                    xFieldColName = "Field1"
                Case enWeight_Types.WEIGHT_FIELD2
                    xFieldColName = "Field2"
                Case enWeight_Types.WEIGHT_FIELD3
                    xFieldColName = "Field3"
                Case enWeight_Types.WEIGHT_FIELD4
                    xFieldColName = "Field4"
                Case enWeight_Types.WEIGHT_FIELD5
                    xFieldColName = "Field5"
            End Select
            'S.SANDEEP [25 MAR 2015] -- END
            objMap = Nothing

            dsList = objFMaster.Get_Field_Mapping(iList)

            'S.SANDEEP |08-FEB-2019| -- START
            'ISSUE/ENHANCEMENT : INFO FIELD NOT DISPLAYED IN LIST (PA)
            'Dim i As Integer = 1
            'If dsList.Tables(iList).Rows.Count > 0 Then
            '    For Each dRow As DataRow In dsList.Tables(iList).Rows
            '        dCol = New DataColumn
            '        If i <= 5 Then
            '            dCol.ColumnName = "Field" & i.ToString
            '        Else
            '            dCol.ColumnName = "Field" & dRow.Item("fieldunkid").ToString
            '        End If
            '        dCol.Caption = dRow.Item("fieldcaption").ToString
            '        dCol.DataType = System.Type.GetType("System.String")
            '        dCol.DefaultValue = ""
            '        dCol.ExtendedProperties.Add(dCol.ColumnName, CInt(dRow.Item("fieldunkid")))
            '        mdtFinal.Columns.Add(dCol)

            '        dCol = New DataColumn
            '        If i <= 5 Then
            '            dCol.ColumnName = "Field" & i.ToString & "Id"
            '        Else
            '            dCol.ColumnName = "Field" & dRow.Item("fieldunkid").ToString & "Id"
            '        End If
            '        dCol.DataType = System.Type.GetType("System.Int32")
            '        dCol.Caption = ""
            '        dCol.DefaultValue = 0
            '        mdtFinal.Columns.Add(dCol)
            '        i += 1
            '    Next
            'End If
            Dim i As Integer = 1
            If dsList.Tables(iList).Rows.Count > 0 Then
                For Each dRow As DataRow In dsList.Tables(iList).Rows
                    dCol = New DataColumn
                    If i <= 5 AndAlso CBool(dRow("isinformational")) = False Then
                        dCol.ColumnName = "Field" & i.ToString
                    Else
                        dCol.ColumnName = "Field" & dRow.Item("fieldunkid").ToString
                    End If
                    dCol.Caption = dRow.Item("fieldcaption").ToString
                    dCol.DataType = System.Type.GetType("System.String")
                    dCol.DefaultValue = ""
                    dCol.ExtendedProperties.Add(dCol.ColumnName, CInt(dRow.Item("fieldunkid")))
                    mdtFinal.Columns.Add(dCol)

                    dCol = New DataColumn
                    If i <= 5 AndAlso CBool(dRow("isinformational")) = False Then
                        dCol.ColumnName = "Field" & i.ToString & "Id"
                    Else
                        dCol.ColumnName = "Field" & dRow.Item("fieldunkid").ToString & "Id"
                    End If
                    dCol.DataType = System.Type.GetType("System.Int32")
                    dCol.Caption = ""
                    dCol.DefaultValue = 0
                    mdtFinal.Columns.Add(dCol)
                    i += 1
                Next
            End If
            'S.SANDEEP |08-FEB-2019| -- END

            dCol = New DataColumn
            dCol.ColumnName = "St_Date"
            dCol.Caption = Language.getMessage(mstrModuleName, 18, "Start Date")
            dCol.DataType = System.Type.GetType("System.String")
            dCol.DefaultValue = ""
            dCol.ExtendedProperties.Add(dCol.ColumnName, CInt(clsAssess_Field_Master.enOtherInfoField.ST_DATE))
            mdtFinal.Columns.Add(dCol)

            dCol = New DataColumn
            dCol.ColumnName = "Ed_Date"
            dCol.Caption = Language.getMessage(mstrModuleName, 19, "End Date")
            dCol.DataType = System.Type.GetType("System.String")
            dCol.DefaultValue = ""
            dCol.ExtendedProperties.Add(dCol.ColumnName, CInt(clsAssess_Field_Master.enOtherInfoField.ED_DATE))
            mdtFinal.Columns.Add(dCol)

            dCol = New DataColumn
            dCol.ColumnName = "CStatus"
            dCol.Caption = Language.getMessage(mstrModuleName, 20, "Status")
            dCol.DataType = System.Type.GetType("System.String")
            dCol.DefaultValue = ""
            dCol.ExtendedProperties.Add(dCol.ColumnName, CInt(clsAssess_Field_Master.enOtherInfoField.STATUS))
            mdtFinal.Columns.Add(dCol)

            dCol = New DataColumn
            dCol.ColumnName = "OPeriod"
            dCol.Caption = Language.getMessage(mstrModuleName, 21, "Period")
            dCol.DataType = System.Type.GetType("System.String")
            dCol.DefaultValue = ""
            mdtFinal.Columns.Add(dCol)

            dCol = New DataColumn
            dCol.ColumnName = "CStatusId"
            dCol.DataType = System.Type.GetType("System.Int32")
            dCol.Caption = ""
            dCol.DefaultValue = 0
            mdtFinal.Columns.Add(dCol)

            dCol = New DataColumn
            dCol.ColumnName = "employeeunkid"
            dCol.DataType = System.Type.GetType("System.Int32")
            dCol.Caption = ""
            dCol.DefaultValue = 0
            mdtFinal.Columns.Add(dCol)

            dCol = New DataColumn
            dCol.ColumnName = "periodunkid"
            dCol.DataType = System.Type.GetType("System.Int32")
            dCol.Caption = ""
            dCol.DefaultValue = 0
            mdtFinal.Columns.Add(dCol)

            dCol = New DataColumn
            dCol.ColumnName = "Weight"
            'S.SANDEEP [ 16 JAN 2015 ] -- START
            'dCol.Caption = iCaptionName & " " & Language.getMessage(mstrModuleName, 10, "Weight")
            dCol.Caption = Language.getMessage(mstrModuleName, 22, "Weight")
            'S.SANDEEP [ 16 JAN 2015 ] -- END
            dCol.DataType = System.Type.GetType("System.String")
            dCol.DefaultValue = ""
            dCol.ExtendedProperties.Add(dCol.ColumnName, CInt(clsAssess_Field_Master.enOtherInfoField.WEIGHT))
            mdtFinal.Columns.Add(dCol)

            dCol = New DataColumn
            dCol.ColumnName = "pct_complete"
            dCol.Caption = iCaptionName & " " & Language.getMessage(mstrModuleName, 23, "% Completed")
            dCol.DataType = System.Type.GetType("System.String")
            dCol.DefaultValue = ""
            dCol.ExtendedProperties.Add(dCol.ColumnName, CInt(clsAssess_Field_Master.enOtherInfoField.PCT_COMPLETE))
            mdtFinal.Columns.Add(dCol)

            dCol = New DataColumn
            dCol.ColumnName = "owrfield1unkid"
            dCol.DataType = System.Type.GetType("System.Int32")
            dCol.DefaultValue = 0
            dCol.Caption = ""
            mdtFinal.Columns.Add(dCol)

            dCol = New DataColumn
            dCol.ColumnName = "empfield1unkid"
            dCol.DataType = System.Type.GetType("System.Int32")
            dCol.DefaultValue = 0
            dCol.Caption = ""
            mdtFinal.Columns.Add(dCol)

            dCol = New DataColumn
            dCol.ColumnName = "empfield2unkid"
            dCol.DataType = System.Type.GetType("System.Int32")
            dCol.DefaultValue = 0
            dCol.Caption = ""
            mdtFinal.Columns.Add(dCol)

            dCol = New DataColumn
            dCol.ColumnName = "empfield3unkid"
            dCol.DataType = System.Type.GetType("System.Int32")
            dCol.DefaultValue = 0
            dCol.Caption = ""
            mdtFinal.Columns.Add(dCol)

            dCol = New DataColumn
            dCol.ColumnName = "empfield4unkid"
            dCol.DataType = System.Type.GetType("System.Int32")
            dCol.DefaultValue = 0
            dCol.Caption = ""
            mdtFinal.Columns.Add(dCol)

            dCol = New DataColumn
            dCol.ColumnName = "empfield5unkid"
            dCol.DataType = System.Type.GetType("System.Int32")
            dCol.DefaultValue = 0
            dCol.Caption = ""
            mdtFinal.Columns.Add(dCol)

            dCol = New DataColumn
            dCol.ColumnName = "sdate"
            dCol.Caption = ""
            dCol.DataType = System.Type.GetType("System.String")
            dCol.DefaultValue = ""
            mdtFinal.Columns.Add(dCol)

            dCol = New DataColumn
            dCol.ColumnName = "edate"
            dCol.Caption = ""
            dCol.DataType = System.Type.GetType("System.String")
            dCol.DefaultValue = ""
            mdtFinal.Columns.Add(dCol)

            dCol = New DataColumn
            dCol.ColumnName = "opstatusid"
            dCol.DataType = System.Type.GetType("System.Int32")
            dCol.DefaultValue = 0
            dCol.Caption = ""
            mdtFinal.Columns.Add(dCol)

            dCol = New DataColumn
            dCol.ColumnName = "isfinal"
            dCol.DataType = System.Type.GetType("System.Boolean")
            dCol.DefaultValue = 0
            dCol.Caption = ""
            mdtFinal.Columns.Add(dCol)

            dCol = New DataColumn
            dCol.ColumnName = "OwnerIds"
            dCol.Caption = ""
            dCol.DataType = System.Type.GetType("System.String")
            dCol.DefaultValue = ""
            mdtFinal.Columns.Add(dCol)

            dCol = New DataColumn
            dCol.ColumnName = "CFieldTypeId"
            dCol.Caption = ""
            dCol.DataType = System.Type.GetType("System.Int32")
            dCol.DefaultValue = 0
            mdtFinal.Columns.Add(dCol)

            'S.SANDEEP [16 JUN 2015] -- START
            dCol = New DataColumn
            dCol.ColumnName = "vuRemark"
            dCol.Caption = Language.getMessage(mstrModuleName, 29, "Progress Remark")
            dCol.DataType = System.Type.GetType("System.String")
            dCol.DefaultValue = ""
            mdtFinal.Columns.Add(dCol)

            dCol = New DataColumn
            dCol.ColumnName = "vuProgress"
            dCol.Caption = Language.getMessage(mstrModuleName, 24, "View/Update Progress")
            dCol.DataType = System.Type.GetType("System.String")
            dCol.DefaultValue = Language.getMessage(mstrModuleName, 25, "Update Progress")
            mdtFinal.Columns.Add(dCol)
            'S.SANDEEP [16 JUN 2015] -- END

            'Shani (26-Sep-2016) -- Start
            'Enhancement -Changes PA Flow And Add New screen Given By (Andrew)
            dCol = New DataColumn
            dCol.ColumnName = "vuGoalAccomplishmentStatusId"
            dCol.Caption = ""
            dCol.DataType = System.Type.GetType("System.Int32")
            dCol.DefaultValue = 0
            mdtFinal.Columns.Add(dCol)

            dCol = New DataColumn
            dCol.ColumnName = "vuGoalAccomplishmentStatus"
            dCol.Caption = Language.getMessage(mstrModuleName, 28, "Goal Accomplishment Status")
            dCol.DataType = System.Type.GetType("System.String")
            dCol.DefaultValue = ""
            mdtFinal.Columns.Add(dCol)

            dCol = New DataColumn
            dCol.ColumnName = "empupdatetranunkid"
            dCol.Caption = ""
            dCol.DataType = System.Type.GetType("System.Int32")
            dCol.DefaultValue = 0
            mdtFinal.Columns.Add(dCol)
            'Shani (26-Sep-2016) -- End

            'S.SANDEEP [01-OCT-2018] -- START
            'ISSUE/ENHANCEMENT : {Ref#2585|ARUTI-}
            dCol = New DataColumn
            dCol.ColumnName = "goaltypeid"
            dCol.Caption = ""
            dCol.DataType = GetType(System.Int32)
            dCol.DefaultValue = 0
            mdtFinal.Columns.Add(dCol)

            dCol = New DataColumn
            dCol.ColumnName = "goalvalue"
            dCol.Caption = ""
            dCol.DataType = GetType(System.Decimal)
            dCol.DefaultValue = 0
            mdtFinal.Columns.Add(dCol)
            'S.SANDEEP [01-OCT-2018] -- END


            'S.SANDEEP [11-OCT-2018] -- START
            dCol = New DataColumn
            dCol.ColumnName = "dgoaltype"
            dCol.Caption = iCaptionName & " " & Language.getMessage("clsAssess_Field_Master", 14, "Goal Type")
            dCol.DataType = GetType(System.String)
            dCol.DefaultValue = 0
            dCol.ExtendedProperties.Add(dCol.ColumnName, CInt(clsAssess_Field_Master.enOtherInfoField.GOAL_TYPE))
            mdtFinal.Columns.Add(dCol)

            dCol = New DataColumn
            dCol.ColumnName = "dgoalvalue"
            dCol.Caption = iCaptionName & " " & Language.getMessage("clsAssess_Field_Master", 15, "Goal Value")
            'S.SANDEEP |12-FEB-2019| -- START
            'ISSUE/ENHANCEMENT : {Performance Assessment Changes}
            'dCol.DataType = GetType(System.Double)
            dCol.DataType = GetType(System.String)
            'S.SANDEEP |12-FEB-2019| -- END
            dCol.ExtendedProperties.Add(dCol.ColumnName, CInt(clsAssess_Field_Master.enOtherInfoField.GOAL_VALUE))
            dCol.DefaultValue = 0
            mdtFinal.Columns.Add(dCol)
            'S.SANDEEP [11-OCT-2018] -- END

            'S.SANDEEP |12-FEB-2019| -- START
            'ISSUE/ENHANCEMENT : {Performance Assessment Changes}
            dCol = New DataColumn
            dCol.ColumnName = "STypeName"
            dCol.Caption = "" 'Language.getMessage(mstrModuleName, 32, "Goal Status")
            dCol.DataType = GetType(System.String)
            dCol.DefaultValue = ""
            mdtFinal.Columns.Add(dCol)

            dCol = New DataColumn
            dCol.ColumnName = "UoMTypeId"
            dCol.Caption = ""
            dCol.DataType = GetType(System.Int32)
            dCol.DefaultValue = 0
            mdtFinal.Columns.Add(dCol)

            dCol = New DataColumn
            dCol.ColumnName = "UoMType"
            dCol.Caption = ""
            dCol.DataType = GetType(System.String)
            dCol.DefaultValue = ""
            mdtFinal.Columns.Add(dCol)
            'S.SANDEEP |12-FEB-2019| -- END

            'S.SANDEEP |04-DEC-2019| -- START
            'ISSUE/ENHANCEMENT : REPORT TEMPLATE 17 -- NMB
            dCol = New DataColumn
            dCol.ColumnName = "xcomments"
            dCol.Caption = ""
            dCol.DataType = GetType(System.String)
            dCol.DefaultValue = ""
            mdtFinal.Columns.Add(dCol)
            'S.SANDEEP |04-DEC-2019| -- END

            'Hemant (28 Apr 2023) -- Start
            'ENHANCEMENT(NMB) : A1X-667 - As a user, i want to have the ability to approve/decline some objectives in a score card instead of approving/declining it as a whole
            dCol = New DataColumn
            dCol.ColumnName = "GoalStatusTypeId"
            dCol.Caption = ""
            dCol.DataType = GetType(System.Int32)
            dCol.DefaultValue = 0
            mdtFinal.Columns.Add(dCol)
            'Hemant (28 Apr 2023) -- End


            StrQ = "DECLARE @LinkFieldId AS INT " & _
                   "SET @LinkFieldId = ISNULL((SELECT fieldunkid FROM hrassess_field_mapping WITH (NOLOCK) WHERE periodunkid = '" & iPeriodId & "' AND isactive = 1),0) " & _
                   "SELECT DISTINCT " & _
                          "  Perspective " & _
                          ", Emp " & _
                          ", OwrField1 AS OwrField1 " & _
                          ", Field1 " & _
                          ", Field2 " & _
                          ", Field3 " & _
                          ", Field4 " & _
                          ", Field5 " & _
                          ", perspectiveunkid " & _
                          ", ISNULL(CASE WHEN @LinkFieldId = Field1Id THEN f1_SDate " & _
                                        "WHEN @LinkFieldId = Field2Id THEN f2_SDate " & _
                                        "WHEN @LinkFieldId = Field3Id THEN f3_SDate " & _
                                        "WHEN @LinkFieldId = Field4Id THEN f4_SDate " & _
                                        "WHEN @LinkFieldId = Field5Id THEN f5_SDate " & _
                                   "END, '') AS St_Date " & _
                          ", ISNULL(CASE WHEN @LinkFieldId = Field1Id THEN f1_EDate " & _
                                        "WHEN @LinkFieldId = Field2Id THEN f2_EDate " & _
                                        "WHEN @LinkFieldId = Field3Id THEN f3_EDate " & _
                                        "WHEN @LinkFieldId = Field4Id THEN f4_EDate " & _
                                        "WHEN @LinkFieldId = Field5Id THEN f5_EDate " & _
                                   "END, '') AS Ed_Date " & _
                          ", ISNULL(CASE WHEN ISNULL(CASE WHEN @LinkFieldId = Field1Id THEN f1StatId " & _
                                                         "WHEN @LinkFieldId = Field2Id THEN f2StatId " & _
                                                         "WHEN @LinkFieldId = Field3Id THEN f3StatId " & _
                                                         "WHEN @LinkFieldId = Field4Id THEN f4StatId " & _
                                                         "WHEN @LinkFieldId = Field5Id THEN f5StatId " & _
                                                    "END, 0) = 1 THEN @ST_PENDING " & _
                                        "WHEN ISNULL(CASE WHEN @LinkFieldId = Field1Id THEN f1StatId " & _
                                                         "WHEN @LinkFieldId = Field2Id THEN f2StatId " & _
                                                         "WHEN @LinkFieldId = Field3Id THEN f3StatId " & _
                                                         "WHEN @LinkFieldId = Field4Id THEN f4StatId " & _
                                                         "WHEN @LinkFieldId = Field5Id THEN f5StatId " & _
                                                    "END, 0) = 2 THEN @ST_INPROGRESS " & _
                                        "WHEN ISNULL(CASE WHEN @LinkFieldId = Field1Id THEN f1StatId " & _
                                                         "WHEN @LinkFieldId = Field2Id THEN f2StatId " & _
                                                         "WHEN @LinkFieldId = Field3Id THEN f3StatId " & _
                                                         "WHEN @LinkFieldId = Field4Id THEN f4StatId " & _
                                                         "WHEN @LinkFieldId = Field5Id THEN f5StatId " & _
                                                    "END, 0) = 3 THEN @ST_COMPLETE " & _
                                        "WHEN ISNULL(CASE WHEN @LinkFieldId = Field1Id THEN f1StatId " & _
                                                         "WHEN @LinkFieldId = Field2Id THEN f2StatId " & _
                                                         "WHEN @LinkFieldId = Field3Id THEN f3StatId " & _
                                                         "WHEN @LinkFieldId = Field4Id THEN f4StatId " & _
                                                         "WHEN @LinkFieldId = Field5Id THEN f5StatId " & _
                                                    "END, 0) = 4 THEN @ST_CLOSED " & _
                                        "WHEN ISNULL(CASE WHEN @LinkFieldId = Field1Id THEN f1StatId " & _
                                                         "WHEN @LinkFieldId = Field2Id THEN f2StatId " & _
                                                         "WHEN @LinkFieldId = Field3Id THEN f3StatId " & _
                                                         "WHEN @LinkFieldId = Field4Id THEN f4StatId " & _
                                                         "WHEN @LinkFieldId = Field5Id THEN f5StatId " & _
                                                    "END, 0) = 5 THEN @ST_ONTRACK " & _
                                        "WHEN ISNULL(CASE WHEN @LinkFieldId = Field1Id THEN f1StatId " & _
                                                         "WHEN @LinkFieldId = Field2Id THEN f2StatId " & _
                                                         "WHEN @LinkFieldId = Field3Id THEN f3StatId " & _
                                                         "WHEN @LinkFieldId = Field4Id THEN f4StatId " & _
                                                         "WHEN @LinkFieldId = Field5Id THEN f5StatId " & _
                                                    "END, 0) = 6 THEN @ST_ATRISK " & _
                                        "WHEN ISNULL(CASE WHEN @LinkFieldId = Field1Id THEN f1StatId " & _
                                                         "WHEN @LinkFieldId = Field2Id THEN f2StatId " & _
                                                         "WHEN @LinkFieldId = Field3Id THEN f3StatId " & _
                                                         "WHEN @LinkFieldId = Field4Id THEN f4StatId " & _
                                                         "WHEN @LinkFieldId = Field5Id THEN f5StatId " & _
                                                    "END, 0) = 7 THEN @ST_NOTAPPLICABLE " & _
                                   "END, '') AS CStatus " & _
                          ", ISNULL(CASE WHEN @LinkFieldId = Field1Id THEN f1StatId " & _
                                        "WHEN @LinkFieldId = Field2Id THEN f2StatId " & _
                                        "WHEN @LinkFieldId = Field3Id THEN f3StatId " & _
                                        "WHEN @LinkFieldId = Field4Id THEN f4StatId " & _
                                        "WHEN @LinkFieldId = Field5Id THEN f5StatId " & _
                                   "END, 0) AS CStatusId " & _
                          ", ISNULL(CASE WHEN @LinkFieldId = Field1Id THEN CASE WHEN f1weight <= 0 THEN '' ELSE CAST(f1weight AS NVARCHAR(MAX)) END " & _
                                        "WHEN @LinkFieldId = Field2Id THEN CASE WHEN f2weight <= 0 THEN '' ELSE CAST(f2weight AS NVARCHAR(MAX)) END " & _
                                        "WHEN @LinkFieldId = Field3Id THEN CASE WHEN f3weight <= 0 THEN '' ELSE CAST(f3weight AS NVARCHAR(MAX)) END " & _
                                        "WHEN @LinkFieldId = Field4Id THEN CASE WHEN f4weight <= 0 THEN '' ELSE CAST(f4weight AS NVARCHAR(MAX)) END " & _
                                        "WHEN @LinkFieldId = Field5Id THEN CASE WHEN f5weight <= 0 THEN '' ELSE CAST(f5weight AS NVARCHAR(MAX)) END " & _
                                   "END, '') AS Weight " & _
                           ", ISNULL(CASE WHEN @LinkFieldId = Field1Id THEN CASE WHEN f1pct <= 0 THEN '' ELSE CAST(f1pct AS NVARCHAR(MAX)) END " & _
                                        " WHEN @LinkFieldId = Field2Id THEN CASE WHEN f2pct <= 0 THEN '' ELSE CAST(f2pct AS NVARCHAR(MAX)) END " & _
                                        " WHEN @LinkFieldId = Field3Id THEN CASE WHEN f3pct <= 0 THEN '' ELSE CAST(f3pct AS NVARCHAR(MAX)) END " & _
                                        " WHEN @LinkFieldId = Field4Id THEN CASE WHEN f4pct <= 0 THEN '' ELSE CAST(f4pct AS NVARCHAR(MAX)) END " & _
                                        " WHEN @LinkFieldId = Field5Id THEN CASE WHEN f5pct <= 0 THEN '' ELSE CAST(f5pct AS NVARCHAR(MAX)) END " & _
                                   "END, '') AS pct_complete " & _
                          ", ISNULL(empfield1unkid, 0) AS empfield1unkid " & _
                          ", ISNULL(empfield2unkid, 0) AS empfield2unkid " & _
                          ", ISNULL(empfield3unkid, 0) AS empfield3unkid " & _
                          ", ISNULL(empfield4unkid, 0) AS empfield4unkid " & _
                          ", ISNULL(empfield5unkid, 0) AS empfield5unkid " & _
                          ", Field1Id " & _
                          ", Field2Id " & _
                          ", Field3Id " & _
                          ", Field4Id " & _
                          ", Field5Id " & _
                          ", periodunkid " & _
                          ", OPeriod " & _
                          ", owrfield1unkid " & _
                          ", employeeunkid " & _
                          ", ISNULL(isfinal,0) AS isfinal " & _
                          ", ISNULL(STypId,0) AS opstatusid " & _
                          ", ISNULL(CASE WHEN @LinkFieldId = Field1Id THEN '" & enWeight_Types.WEIGHT_FIELD1 & "' " & _
                                        "WHEN @LinkFieldId = Field2Id THEN '" & enWeight_Types.WEIGHT_FIELD2 & "' " & _
                                        "WHEN @LinkFieldId = Field3Id THEN '" & enWeight_Types.WEIGHT_FIELD3 & "' " & _
                                        "WHEN @LinkFieldId = Field4Id THEN '" & enWeight_Types.WEIGHT_FIELD4 & "' " & _
                                        "WHEN @LinkFieldId = Field5Id THEN '" & enWeight_Types.WEIGHT_FIELD5 & "' " & _
                                   "END, 0) AS CFieldTypeId " & _
                          ",'' AS OwnerIds " & _
                          ", ISNULL(STypeName,'') AS STypeName " & _
                          ", ISNULL(xcomments,'') AS xcomments "
            'S.SANDEEP [23 APR 2015] -- START

            'S.SANDEEP [19 MAY 2015] -- START
            'StrQ &= ",CAST(ISNULL(empfield1unkid, 0) AS NVARCHAR(MAX))+'|'+ " & _
            '        " CAST(ISNULL(empfield2unkid, 0) AS NVARCHAR(MAX))+'|'+ " & _
            '        " CAST(ISNULL(empfield3unkid, 0) AS NVARCHAR(MAX))+'|'+ " & _
            '        " CAST(ISNULL(empfield4unkid, 0) AS NVARCHAR(MAX))+'|'+ " & _
            '        " CAST(ISNULL(empfield5unkid, 0) AS NVARCHAR(MAX)) AS Caption "

            'S.SANDEEP [16 JUN 2015] -- START
            'StrQ &= ",CASE WHEN @LinkFieldId  = Field1Id THEN CAST(ISNULL(empfield1unkid, 0) AS NVARCHAR(MAX)) " & _
            '        "      WHEN @LinkFieldId  = Field2Id THEN CAST(ISNULL(empfield2unkid, 0) AS NVARCHAR(MAX)) " & _
            '        "      WHEN @LinkFieldId  = Field3Id THEN CAST(ISNULL(empfield3unkid, 0) AS NVARCHAR(MAX)) " & _
            '        "      WHEN @LinkFieldId  = Field4Id THEN CAST(ISNULL(empfield4unkid, 0) AS NVARCHAR(MAX)) " & _
            '        "      WHEN @LinkFieldId  = Field5Id THEN CAST(ISNULL(empfield5unkid, 0) AS NVARCHAR(MAX)) " & _
            '        "END AS Caption "
            StrQ &= ",ISNULL(CASE WHEN @LinkFieldId  = Field1Id THEN CAST(ISNULL(empfield1unkid, 0) AS NVARCHAR(MAX)) " & _
                    "      WHEN @LinkFieldId  = Field2Id THEN CAST(ISNULL(empfield2unkid, 0) AS NVARCHAR(MAX)) " & _
                    "      WHEN @LinkFieldId  = Field3Id THEN CAST(ISNULL(empfield3unkid, 0) AS NVARCHAR(MAX)) " & _
                    "      WHEN @LinkFieldId  = Field4Id THEN CAST(ISNULL(empfield4unkid, 0) AS NVARCHAR(MAX)) " & _
                    "      WHEN @LinkFieldId  = Field5Id THEN CAST(ISNULL(empfield5unkid, 0) AS NVARCHAR(MAX)) " & _
                    "END,0) AS Caption "
            'S.SANDEEP [16 JUN 2015] -- END


            'S.SANDEEP [19 MAY 2015] -- END

            'S.SANDEEP [23 APR 2015] -- END

            'Shani (26-Sep-2016) -- Start
            'Enhancement -Changes PA Flow And Add New screen Given By (Andrew)
            StrQ &= ",ISNULL(Obj_GoalAccomplishment.GoalsAccomplishmentids,0) AS vuGoalAccomplishmentStatusId " & _
                    ",ISNULL(Obj_GoalAccomplishment.GoalsAccomplishmentStatus,'') AS vuGoalAccomplishmentStatus " & _
                    ",ISNULL(Obj_GoalAccomplishment.empupdatetranunkid,0) AS empupdatetranunkid " 
            'Shani (26-Sep-2016) -- End


            'S.SANDEEP [01-OCT-2018] -- START
            'ISSUE/ENHANCEMENT : {Ref#2585|ARUTI-}
            StrQ &= ", ISNULL(CASE WHEN @LinkFieldId = Field1Id THEN f1GoalTypeId " & _
                    "              WHEN @LinkFieldId = Field2Id THEN f2GoalTypeId " & _
                    "              WHEN @LinkFieldId = Field3Id THEN f3GoalTypeId " & _
                    "              WHEN @LinkFieldId = Field4Id THEN f4GoalTypeId " & _
                    "              WHEN @LinkFieldId = Field5Id THEN f5GoalTypeId " & _
                    "   END, " & CInt(enGoalType.GT_QUALITATIVE) & ") AS goaltypeid " & _
                    ", ISNULL(CASE WHEN @LinkFieldId = Field1Id THEN f1GoalValue " & _
                    "              WHEN @LinkFieldId = Field2Id THEN f2GoalValue " & _
                    "              WHEN @LinkFieldId = Field3Id THEN f3GoalValue " & _
                    "              WHEN @LinkFieldId = Field4Id THEN f4GoalValue " & _
                    "              WHEN @LinkFieldId = Field5Id THEN f5GoalValue " & _
                    "   END, 0) AS goalvalue "
            'S.SANDEEP [01-OCT-2018] -- END

            'S.SANDEEP [11-OCT-2018] -- START
            StrQ &= ", CASE WHEN ISNULL(CASE WHEN @LinkFieldId = Field1Id THEN f1GoalTypeId " & _
                    "                        WHEN @LinkFieldId = Field2Id THEN f2GoalTypeId " & _
                    "                        WHEN @LinkFieldId = Field3Id THEN f3GoalTypeId " & _
                    "                        WHEN @LinkFieldId = Field4Id THEN f4GoalTypeId " & _
                    "                        WHEN @LinkFieldId = Field5Id THEN f5GoalTypeId " & _
                    "                   END, " & CInt(enGoalType.GT_QUALITATIVE) & ") = " & CInt(enGoalType.GT_QUALITATIVE) & " THEN @GT_QUALITATIVE " & _
                    " ELSE @GT_QUANTITATIVE END AS dgoaltype "
            'S.SANDEEP [11-OCT-2018] -- END

            'S.SANDEEP |12-FEB-2019| -- START
            'ISSUE/ENHANCEMENT : {Performance Assessment Changes}
            StrQ &= ",ISNULL(UoM.name,'') AS UoMType " & _
                    ",ISNULL(CASE WHEN @LinkFieldId = Field1Id THEN f1UoMTypeId " & _
                    "             WHEN @LinkFieldId = Field2Id THEN f2UoMTypeId " & _
                    "             WHEN @LinkFieldId = Field3Id THEN f3UoMTypeId " & _
                    "             WHEN @LinkFieldId = Field4Id THEN f4UoMTypeId " & _
                    "             WHEN @LinkFieldId = Field5Id THEN f5UoMTypeId END,0) AS UoMTypeId "
            'S.SANDEEP |12-FEB-2019| -- END

            'Hemant (28 Apr 2023) -- Start
            'ENHANCEMENT(NMB) : A1X-667 - As a user, i want to have the ability to approve/decline some objectives in a score card instead of approving/declining it as a whole
            StrQ &= ", ISNULL(Obj_GoalStatus.statustypeid, 0) AS GoalStatusTypeId "
            'Hemant (28 Apr 2023) -- End

            StrQ &= "FROM    ( " & _
                                   "SELECT " & _
                                        " ISNULL(hremployee_master.employeecode,'')+ ' - ' + ISNULL(hremployee_master.firstname,'')+' '+ISNULL(hremployee_master.surname,'') AS Emp "
            If iCascadingTypeId <> enPACascading.NEITHER_CASCADING_NOR_ALIGNMENT Then
                StrQ &= ",ISNULL(hrassess_owrfield1_master.field_data, '') AS OwrField1 "
            Else
                StrQ &= ",'' AS OwrField1 "
            End If
            StrQ &= ",ISNULL(hrassess_empfield1_master.field_data, '') AS Field1 " & _
                                        ",ISNULL(hrassess_empfield2_master.field_data, '') AS Field2 " & _
                                        ",ISNULL(hrassess_empfield3_master.field_data, '') AS Field3 " & _
                                        ",ISNULL(hrassess_empfield4_master.field_data, '') AS Field4 " & _
                                        ",ISNULL(hrassess_empfield5_master.field_data, '') AS Field5 " & _
                                        ",ISNULL(hrassess_empfield1_master.fieldunkid, 0) AS Field1Id " & _
                                        ",ISNULL(hrassess_empfield2_master.fieldunkid, 0) AS Field2Id " & _
                                        ",ISNULL(hrassess_empfield3_master.fieldunkid, 0) AS Field3Id " & _
                                        ",ISNULL(hrassess_empfield4_master.fieldunkid, 0) AS Field4Id " & _
                                        ",ISNULL(hrassess_empfield5_master.fieldunkid, 0) AS Field5Id " & _
                                        ",ISNULL(CONVERT(CHAR(8), hrassess_empfield1_master.startdate, 112),'') AS f1_SDate " & _
                                        ",ISNULL(CONVERT(CHAR(8), hrassess_empfield1_master.enddate, 112),'') AS f1_EDate " & _
                                        ",ISNULL(hrassess_empfield1_master.statusunkid, 0) AS f1StatId " & _
                                        ",ISNULL(hrassess_empfield1_master.weight, 0) AS f1weight " & _
                                        ",ISNULL(hrassess_empfield1_master.pct_completed, 0) AS f1pct " & _
                                        ",ISNULL(CONVERT(CHAR(8), hrassess_empfield2_master.startdate, 112),'') AS f2_SDate " & _
                                        ",ISNULL(CONVERT(CHAR(8), hrassess_empfield2_master.enddate, 112),'') AS f2_EDate " & _
                                        ",ISNULL(hrassess_empfield2_master.statusunkid, 0) AS f2StatId " & _
                                        ",ISNULL(hrassess_empfield2_master.weight, 0) AS f2weight " & _
                                        ",ISNULL(hrassess_empfield2_master.pct_completed, 0) AS f2pct " & _
                                        ",ISNULL(CONVERT(CHAR(8), hrassess_empfield3_master.startdate, 112),'') AS f3_SDate " & _
                                        ",ISNULL(CONVERT(CHAR(8), hrassess_empfield3_master.enddate, 112),'') AS f3_EDate " & _
                                        ",ISNULL(hrassess_empfield3_master.statusunkid, 0) AS f3StatId " & _
                                        ",ISNULL(hrassess_empfield3_master.weight, 0) AS f3weight " & _
                                        ",ISNULL(hrassess_empfield3_master.pct_completed, 0) AS f3pct " & _
                                        ",ISNULL(CONVERT(CHAR(8), hrassess_empfield4_master.startdate, 112),'') AS f4_SDate " & _
                                        ",ISNULL(CONVERT(CHAR(8), hrassess_empfield4_master.enddate, 112),'') AS f4_EDate " & _
                                        ",ISNULL(hrassess_empfield4_master.statusunkid, 0) AS f4StatId " & _
                                        ",ISNULL(hrassess_empfield4_master.weight, 0) AS f4weight " & _
                                        ",ISNULL(hrassess_empfield4_master.pct_completed, 0) AS f4pct " & _
                                        ",ISNULL(CONVERT(CHAR(8), hrassess_empfield5_master.startdate, 112),'') AS f5_SDate " & _
                                        ",ISNULL(CONVERT(CHAR(8), hrassess_empfield5_master.enddate, 112),'') AS f5_EDate " & _
                                        ",ISNULL(hrassess_empfield5_master.statusunkid, 0) AS f5StatId " & _
                                        ",ISNULL(hrassess_empfield5_master.weight, 0) AS f5weight " & _
                                        ",ISNULL(hrassess_empfield5_master.pct_completed, 0) AS f5pct " & _
                                        ",hrassess_empfield1_master.empfield1unkid " & _
                                        ",hrassess_empfield2_master.empfield2unkid " & _
                                        ",hrassess_empfield3_master.empfield3unkid " & _
                                        ",hrassess_empfield4_master.empfield4unkid " & _
                                        ",hrassess_empfield5_master.empfield5unkid " & _
                                        ",ISNULL(hrassess_empfield1_master.periodunkid, 0) AS periodunkid " & _
                                        ",ISNULL(period_name, '') AS OPeriod " & _
                                        ",ISNULL(hrassess_empfield1_master.owrfield1unkid, 0) AS owrfield1unkid " & _
                                        ",hremployee_master.employeeunkid " & _
                                        ",hrassess_empfield1_master.isfinal " & _
                                        ",hrassess_perspective_master.perspectiveunkid " & _
                                        ",ISNULL(hrassess_perspective_master.name,'') AS Perspective " & _
                                        ",ISNULL(hrassess_empfield1_master.goaltypeid," & CInt(enGoalType.GT_QUALITATIVE) & ") AS f1GoalTypeId" & _
                                        ",ISNULL(hrassess_empfield1_master.goalvalue, 0) AS f1GoalValue " & _
                                        ",ISNULL(hrassess_empfield2_master.goaltypeid," & CInt(enGoalType.GT_QUALITATIVE) & ") AS f2GoalTypeId" & _
                                        ",ISNULL(hrassess_empfield2_master.goalvalue, 0) AS f2GoalValue " & _
                                        ",ISNULL(hrassess_empfield3_master.goaltypeid," & CInt(enGoalType.GT_QUALITATIVE) & ") AS f3GoalTypeId" & _
                                        ",ISNULL(hrassess_empfield3_master.goalvalue, 0) AS f3GoalValue " & _
                                        ",ISNULL(hrassess_empfield4_master.goaltypeid," & CInt(enGoalType.GT_QUALITATIVE) & ") AS f4GoalTypeId" & _
                                        ",ISNULL(hrassess_empfield4_master.goalvalue, 0) AS f4GoalValue " & _
                                        ",ISNULL(hrassess_empfield5_master.goaltypeid," & CInt(enGoalType.GT_QUALITATIVE) & ") AS f5GoalTypeId" & _
                                        ",ISNULL(hrassess_empfield5_master.goalvalue, 0) AS f5GoalValue " & _
                                        ",ISNULL(hrassess_empfield1_master.uomtypeid, 0) AS f1UoMTypeId " & _
                                        ",ISNULL(hrassess_empfield2_master.uomtypeid, 0) AS f2UoMTypeId " & _
                                        ",ISNULL(hrassess_empfield3_master.uomtypeid, 0) AS f3UoMTypeId " & _
                                        ",ISNULL(hrassess_empfield4_master.uomtypeid, 0) AS f4UoMTypeId " & _
                                        ",ISNULL(hrassess_empfield5_master.uomtypeid, 0) AS f5UoMTypeId " & _
                                   "FROM hrassess_empfield1_master WITH (NOLOCK) "
            'S.SANDEEP [01-OCT-2018] -- START {Ref#2585} [goaltypeid,goalvalue] -- END
            'S.SANDEEP |12-FEB-2019| -- START {uomtypeid} -- END

            'S.SANDEEP [25 MAR 2015] -- START
            If iCascadingTypeId <> enPACascading.NEITHER_CASCADING_NOR_ALIGNMENT Then
                StrQ &= " LEFT JOIN hrassess_owrfield1_master WITH (NOLOCK) ON hrassess_owrfield1_master.owrfield1unkid = hrassess_empfield1_master.owrfield1unkid AND hrassess_owrfield1_master.isvoid = 0 " & _
                        " LEFT JOIN hrassess_coyfield1_master WITH (NOLOCK) ON hrassess_coyfield1_master.coyfield1unkid = hrassess_owrfield1_master.coyfield1unkid AND hrassess_coyfield1_master.isvoid = 0 " & _
                        " LEFT JOIN hrassess_perspective_master WITH (NOLOCK) ON hrassess_perspective_master.perspectiveunkid = hrassess_coyfield1_master.perspectiveunkid AND hrassess_perspective_master.isactive = 1 "
            Else
                StrQ &= "LEFT JOIN hrassess_perspective_master WITH (NOLOCK) ON hrassess_perspective_master.perspectiveunkid = hrassess_empfield1_master.perspectiveunkid AND hrassess_perspective_master.isactive = 1 "
            End If
            'S.SANDEEP [25 MAR 2015] -- END

            StrQ &= "JOIN hremployee_master WITH (NOLOCK) ON hrassess_empfield1_master.employeeunkid = hremployee_master.employeeunkid " & _
                                        "JOIN cfcommon_period_tran WITH (NOLOCK) ON hrassess_empfield1_master.periodunkid = cfcommon_period_tran.periodunkid " & _
                                        "LEFT JOIN hrassess_empfield2_master WITH (NOLOCK) ON hrassess_empfield1_master.empfield1unkid = hrassess_empfield2_master.empfield1unkid AND hrassess_empfield2_master.isvoid = 0 " & _
                                        "LEFT JOIN hrassess_empfield3_master WITH (NOLOCK) ON hrassess_empfield2_master.empfield2unkid = hrassess_empfield3_master.empfield2unkid AND hrassess_empfield3_master.isvoid = 0 " & _
                                        "LEFT JOIN hrassess_empfield4_master WITH (NOLOCK) ON hrassess_empfield3_master.empfield3unkid = hrassess_empfield4_master.empfield3unkid AND hrassess_empfield4_master.isvoid = 0 " & _
                                        "LEFT JOIN hrassess_empfield5_master WITH (NOLOCK) ON hrassess_empfield4_master.empfield4unkid = hrassess_empfield5_master.empfield4unkid AND hrassess_empfield5_master.isvoid = 0 " & _
                                   "WHERE hrassess_empfield1_master.isvoid = 0 AND hremployee_master.employeeunkid = '" & iEmployeeId & "' AND cfcommon_period_tran.periodunkid = '" & iPeriodId & "' " & _
                    "       " & _
                              "UNION " & _
                                   "SELECT " & _
                                         "ISNULL(hremployee_master.employeecode,'')+ ' - ' + ISNULL(hremployee_master.firstname,'')+' '+ISNULL(hremployee_master.surname,'') AS Emp "

            'Shani (01-Dec-2016) -- ADD [AND hrassess_perspective_master.isactive = 1] 


            If iCascadingTypeId <> enPACascading.NEITHER_CASCADING_NOR_ALIGNMENT Then
                StrQ &= ",ISNULL(hrassess_owrfield1_master.field_data, '') AS OwrField1 "
            Else
                StrQ &= ",'' AS OwrField1 "
            End If
            StrQ &= ",ISNULL(hrassess_empfield1_master.field_data, '') AS Field1 " & _
                                        ",ISNULL(hrassess_empfield2_master.field_data, '') AS Field2 " & _
                                        ",ISNULL(hrassess_empfield3_master.field_data, '') AS Field3 " & _
                                        ",ISNULL(hrassess_empfield4_master.field_data, '') AS Field4 " & _
                                        ",ISNULL(hrassess_empfield5_master.field_data, '') AS Field5 " & _
                                        ",ISNULL(hrassess_empfield1_master.fieldunkid, 0) AS Field1Id " & _
                                        ",ISNULL(hrassess_empfield2_master.fieldunkid, 0) AS Field2Id " & _
                                        ",ISNULL(hrassess_empfield3_master.fieldunkid, 0) AS Field3Id " & _
                                        ",ISNULL(hrassess_empfield4_master.fieldunkid, 0) AS Field4Id " & _
                                        ",ISNULL(hrassess_empfield5_master.fieldunkid, 0) AS Field5Id " & _
                                        ",ISNULL(CONVERT(CHAR(8), hrassess_empfield1_master.startdate, 112),'') AS f1_SDate " & _
                                        ",ISNULL(CONVERT(CHAR(8), hrassess_empfield1_master.enddate, 112),'') AS f1_EDate " & _
                                        ",ISNULL(hrassess_empfield1_master.statusunkid, 0) AS f1StatId " & _
                                        ",ISNULL(hrassess_empfield1_master.weight, 0) AS f1weight " & _
                                        ",ISNULL(hrassess_empfield1_master.pct_completed, 0) AS f1pct " & _
                                        ",ISNULL(CONVERT(CHAR(8), hrassess_empfield2_master.startdate, 112),'') AS f2_SDate " & _
                                        ",ISNULL(CONVERT(CHAR(8), hrassess_empfield2_master.enddate, 112),'') AS f2_EDate " & _
                                        ",ISNULL(hrassess_empfield2_master.statusunkid, 0) AS f2StatId " & _
                                        ",ISNULL(hrassess_empfield2_master.weight, 0) AS f2weight " & _
                                        ",ISNULL(hrassess_empfield2_master.pct_completed, 0) AS f2pct " & _
                                        ",ISNULL(CONVERT(CHAR(8), hrassess_empfield3_master.startdate, 112),'') AS f3_SDate " & _
                                        ",ISNULL(CONVERT(CHAR(8), hrassess_empfield3_master.enddate, 112),'') AS f3_EDate " & _
                                        ",ISNULL(hrassess_empfield3_master.statusunkid, 0) AS f3StatId " & _
                                        ",ISNULL(hrassess_empfield3_master.weight, 0) AS f3weight " & _
                                        ",ISNULL(hrassess_empfield3_master.pct_completed, 0) AS f3pct " & _
                                        ",ISNULL(CONVERT(CHAR(8), hrassess_empfield4_master.startdate, 112),'') AS f4_SDate " & _
                                        ",ISNULL(CONVERT(CHAR(8), hrassess_empfield4_master.enddate, 112),'') AS f4_EDate " & _
                                        ",ISNULL(hrassess_empfield4_master.statusunkid, 0) AS f4StatId " & _
                                        ",ISNULL(hrassess_empfield4_master.weight, 0) AS f4weight " & _
                                        ",ISNULL(hrassess_empfield4_master.pct_completed, 0) AS f4pct " & _
                                        ",ISNULL(CONVERT(CHAR(8), hrassess_empfield5_master.startdate, 112),'') AS f5_SDate " & _
                                        ",ISNULL(CONVERT(CHAR(8), hrassess_empfield5_master.enddate, 112),'') AS f5_EDate " & _
                                        ",ISNULL(hrassess_empfield5_master.statusunkid, 0) AS f5StatId " & _
                                        ",ISNULL(hrassess_empfield5_master.weight, 0) AS f5weight " & _
                                        ",ISNULL(hrassess_empfield5_master.pct_completed, 0) AS f5pct " & _
                                        ",hrassess_empfield1_master.empfield1unkid " & _
                                        ",hrassess_empfield2_master.empfield2unkid " & _
                                        ",hrassess_empfield3_master.empfield3unkid " & _
                                        ",hrassess_empfield4_master.empfield4unkid " & _
                                        ",hrassess_empfield5_master.empfield5unkid " & _
                                        ",ISNULL(hrassess_empfield2_master.periodunkid, 0) AS periodunkid " & _
                                        ",ISNULL(period_name, '') AS OPeriod " & _
                                        ",ISNULL(hrassess_empfield1_master.owrfield1unkid, 0) AS owrfield1unkid " & _
                                        ",hremployee_master.employeeunkid " & _
                                        ",hrassess_empfield1_master.isfinal " & _
                                        ",hrassess_perspective_master.perspectiveunkid " & _
                                        ",ISNULL(hrassess_perspective_master.name,'') AS Perspective " & _
                                        ",ISNULL(hrassess_empfield1_master.goaltypeid," & CInt(enGoalType.GT_QUALITATIVE) & ") AS f1GoalTypeId" & _
                                        ",ISNULL(hrassess_empfield1_master.goalvalue, 0) AS f1GoalValue " & _
                                        ",ISNULL(hrassess_empfield2_master.goaltypeid," & CInt(enGoalType.GT_QUALITATIVE) & ") AS f2GoalTypeId" & _
                                        ",ISNULL(hrassess_empfield2_master.goalvalue, 0) AS f2GoalValue " & _
                                        ",ISNULL(hrassess_empfield3_master.goaltypeid," & CInt(enGoalType.GT_QUALITATIVE) & ") AS f3GoalTypeId" & _
                                        ",ISNULL(hrassess_empfield3_master.goalvalue, 0) AS f3GoalValue " & _
                                        ",ISNULL(hrassess_empfield4_master.goaltypeid," & CInt(enGoalType.GT_QUALITATIVE) & ") AS f4GoalTypeId" & _
                                        ",ISNULL(hrassess_empfield4_master.goalvalue, 0) AS f4GoalValue " & _
                                        ",ISNULL(hrassess_empfield5_master.goaltypeid," & CInt(enGoalType.GT_QUALITATIVE) & ") AS f5GoalTypeId" & _
                                        ",ISNULL(hrassess_empfield5_master.goalvalue, 0) AS f5GoalValue " & _
                                        ",ISNULL(hrassess_empfield1_master.uomtypeid, 0) AS f1UoMTypeId " & _
                                        ",ISNULL(hrassess_empfield2_master.uomtypeid, 0) AS f2UoMTypeId " & _
                                        ",ISNULL(hrassess_empfield3_master.uomtypeid, 0) AS f3UoMTypeId " & _
                                        ",ISNULL(hrassess_empfield4_master.uomtypeid, 0) AS f4UoMTypeId " & _
                                        ",ISNULL(hrassess_empfield5_master.uomtypeid, 0) AS f5UoMTypeId " & _
                                   "FROM hrassess_empfield2_master WITH (NOLOCK) " & _
                                        "LEFT JOIN hrassess_empfield1_master WITH (NOLOCK) ON hrassess_empfield2_master.empfield1unkid = hrassess_empfield1_master.empfield1unkid AND hrassess_empfield1_master.isvoid = 0 "
            'S.SANDEEP [25 MAR 2015] -- START
            If iCascadingTypeId <> enPACascading.NEITHER_CASCADING_NOR_ALIGNMENT Then
                StrQ &= " LEFT JOIN hrassess_owrfield1_master WITH (NOLOCK) ON hrassess_owrfield1_master.owrfield1unkid = hrassess_empfield1_master.owrfield1unkid AND hrassess_owrfield1_master.isvoid = 0 " & _
                        " LEFT JOIN hrassess_coyfield1_master WITH (NOLOCK) ON hrassess_coyfield1_master.coyfield1unkid = hrassess_owrfield1_master.coyfield1unkid AND hrassess_coyfield1_master.isvoid = 0 " & _
                        " LEFT JOIN hrassess_perspective_master WITH (NOLOCK) ON hrassess_perspective_master.perspectiveunkid = hrassess_coyfield1_master.perspectiveunkid AND hrassess_perspective_master.isactive = 1 "
            Else
                StrQ &= "LEFT JOIN hrassess_perspective_master WITH (NOLOCK) ON hrassess_perspective_master.perspectiveunkid = hrassess_empfield1_master.perspectiveunkid AND hrassess_perspective_master.isactive = 1 "
            End If
            'S.SANDEEP [25 MAR 2015] -- END
            StrQ &= "LEFT JOIN hremployee_master WITH (NOLOCK) ON hrassess_empfield2_master.employeeunkid = hremployee_master.employeeunkid " & _
                                        "LEFT JOIN cfcommon_period_tran WITH (NOLOCK) ON hrassess_empfield2_master.periodunkid = cfcommon_period_tran.periodunkid " & _
                                        "LEFT JOIN hrassess_empfield3_master WITH (NOLOCK) ON hrassess_empfield2_master.empfield2unkid = hrassess_empfield3_master.empfield2unkid AND hrassess_empfield3_master.isvoid = 0 " & _
                                        "LEFT JOIN hrassess_empfield4_master WITH (NOLOCK) ON hrassess_empfield3_master.empfield3unkid = hrassess_empfield4_master.empfield3unkid AND hrassess_empfield4_master.isvoid = 0 " & _
                                        "LEFT JOIN hrassess_empfield5_master WITH (NOLOCK) ON hrassess_empfield4_master.empfield4unkid = hrassess_empfield5_master.empfield4unkid AND hrassess_empfield5_master.isvoid = 0 " & _
                                   "WHERE hrassess_empfield2_master.isvoid = 0 AND hremployee_master.employeeunkid = '" & iEmployeeId & "' AND cfcommon_period_tran.periodunkid = '" & iPeriodId & "' " & _
                    "        " & _
                              "UNION " & _
                                   "SELECT " & _
                                         "ISNULL(hremployee_master.employeecode,'')+ ' - ' + ISNULL(hremployee_master.firstname,'')+' '+ISNULL(hremployee_master.surname,'') AS Emp "
            'Shani (01-Dec-2016) -- ADD [AND hrassess_perspective_master.isactive = 1] 
            If iCascadingTypeId <> enPACascading.NEITHER_CASCADING_NOR_ALIGNMENT Then
                StrQ &= ",ISNULL(hrassess_owrfield1_master.field_data, '') AS OwrField1 "
            Else
                StrQ &= ",'' AS OwrField1 "
            End If
            StrQ &= ",ISNULL(hrassess_empfield1_master.field_data, '') AS Field1 " & _
                                        ",ISNULL(hrassess_empfield2_master.field_data, '') AS Field2 " & _
                                        ",ISNULL(hrassess_empfield3_master.field_data, '') AS Field3 " & _
                                        ",ISNULL(hrassess_empfield4_master.field_data, '') AS Field4 " & _
                                        ",ISNULL(hrassess_empfield5_master.field_data, '') AS Field5 " & _
                                        ",ISNULL(hrassess_empfield1_master.fieldunkid, 0) AS Field1Id " & _
                                        ",ISNULL(hrassess_empfield2_master.fieldunkid, 0) AS Field2Id " & _
                                        ",ISNULL(hrassess_empfield3_master.fieldunkid, 0) AS Field3Id " & _
                                        ",ISNULL(hrassess_empfield4_master.fieldunkid, 0) AS Field4Id " & _
                                        ",ISNULL(hrassess_empfield5_master.fieldunkid, 0) AS Field5Id " & _
                                        ",ISNULL(CONVERT(CHAR(8), hrassess_empfield1_master.startdate, 112),'') AS f1_SDate " & _
                                        ",ISNULL(CONVERT(CHAR(8), hrassess_empfield1_master.enddate, 112),'') AS f1_EDate " & _
                                        ",ISNULL(hrassess_empfield1_master.statusunkid, 0) AS f1StatId " & _
                                        ",ISNULL(hrassess_empfield1_master.weight, 0) AS f1weight " & _
                                        ",ISNULL(hrassess_empfield1_master.pct_completed, 0) AS f1pct " & _
                                        ",ISNULL(CONVERT(CHAR(8), hrassess_empfield2_master.startdate, 112),'') AS f2_SDate " & _
                                        ",ISNULL(CONVERT(CHAR(8), hrassess_empfield2_master.enddate, 112),'') AS f2_EDate " & _
                                        ",ISNULL(hrassess_empfield2_master.statusunkid, 0) AS f2StatId " & _
                                        ",ISNULL(hrassess_empfield2_master.weight, 0) AS f2weight " & _
                                        ",ISNULL(hrassess_empfield2_master.pct_completed, 0) AS f2pct " & _
                                        ",ISNULL(CONVERT(CHAR(8), hrassess_empfield3_master.startdate, 112),'') AS f3_SDate " & _
                                        ",ISNULL(CONVERT(CHAR(8), hrassess_empfield3_master.enddate, 112),'') AS f3_EDate " & _
                                        ",ISNULL(hrassess_empfield3_master.statusunkid, 0) AS f3StatId " & _
                                        ",ISNULL(hrassess_empfield3_master.weight, 0) AS f3weight " & _
                                        ",ISNULL(hrassess_empfield3_master.pct_completed, 0) AS f3pct " & _
                                        ",ISNULL(CONVERT(CHAR(8), hrassess_empfield4_master.startdate, 112),'') AS f4_SDate " & _
                                        ",ISNULL(CONVERT(CHAR(8), hrassess_empfield4_master.enddate, 112),'') AS f4_EDate " & _
                                        ",ISNULL(hrassess_empfield4_master.statusunkid, 0) AS f4StatId " & _
                                        ",ISNULL(hrassess_empfield4_master.weight, 0) AS f4weight " & _
                                        ",ISNULL(hrassess_empfield4_master.pct_completed, 0) AS f4pct " & _
                                        ",ISNULL(CONVERT(CHAR(8), hrassess_empfield5_master.startdate, 112),'') AS f5_SDate " & _
                                        ",ISNULL(CONVERT(CHAR(8), hrassess_empfield5_master.enddate, 112),'') AS f5_EDate " & _
                                        ",ISNULL(hrassess_empfield5_master.statusunkid, 0) AS f5StatId " & _
                                        ",ISNULL(hrassess_empfield5_master.weight, 0) AS f5weight " & _
                                        ",ISNULL(hrassess_empfield5_master.pct_completed, 0) AS f5pct " & _
                                        ",hrassess_empfield1_master.empfield1unkid " & _
                                        ",hrassess_empfield2_master.empfield2unkid " & _
                                        ",hrassess_empfield3_master.empfield3unkid " & _
                                        ",hrassess_empfield4_master.empfield4unkid " & _
                                        ",hrassess_empfield5_master.empfield5unkid " & _
                                        ",ISNULL(hrassess_empfield3_master.periodunkid, 0) AS periodunkid " & _
                                        ",ISNULL(period_name, '') AS OPeriod " & _
                                        ",ISNULL(hrassess_empfield1_master.owrfield1unkid, 0) AS owrfield1unkid " & _
                                        ",hremployee_master.employeeunkid " & _
                                        ",hrassess_empfield1_master.isfinal " & _
                                        ",hrassess_perspective_master.perspectiveunkid " & _
                                        ",ISNULL(hrassess_perspective_master.name,'') AS Perspective " & _
                                        ",ISNULL(hrassess_empfield1_master.goaltypeid," & CInt(enGoalType.GT_QUALITATIVE) & ") AS f1GoalTypeId" & _
                                        ",ISNULL(hrassess_empfield1_master.goalvalue, 0) AS f1GoalValue " & _
                                        ",ISNULL(hrassess_empfield2_master.goaltypeid," & CInt(enGoalType.GT_QUALITATIVE) & ") AS f2GoalTypeId" & _
                                        ",ISNULL(hrassess_empfield2_master.goalvalue, 0) AS f2GoalValue " & _
                                        ",ISNULL(hrassess_empfield3_master.goaltypeid," & CInt(enGoalType.GT_QUALITATIVE) & ") AS f3GoalTypeId" & _
                                        ",ISNULL(hrassess_empfield3_master.goalvalue, 0) AS f3GoalValue " & _
                                        ",ISNULL(hrassess_empfield4_master.goaltypeid," & CInt(enGoalType.GT_QUALITATIVE) & ") AS f4GoalTypeId" & _
                                        ",ISNULL(hrassess_empfield4_master.goalvalue, 0) AS f4GoalValue " & _
                                        ",ISNULL(hrassess_empfield5_master.goaltypeid," & CInt(enGoalType.GT_QUALITATIVE) & ") AS f5GoalTypeId" & _
                                        ",ISNULL(hrassess_empfield5_master.goalvalue, 0) AS f5GoalValue " & _
                                        ",ISNULL(hrassess_empfield1_master.uomtypeid, 0) AS f1UoMTypeId " & _
                                        ",ISNULL(hrassess_empfield2_master.uomtypeid, 0) AS f2UoMTypeId " & _
                                        ",ISNULL(hrassess_empfield3_master.uomtypeid, 0) AS f3UoMTypeId " & _
                                        ",ISNULL(hrassess_empfield4_master.uomtypeid, 0) AS f4UoMTypeId " & _
                                        ",ISNULL(hrassess_empfield5_master.uomtypeid, 0) AS f5UoMTypeId " & _
                                   "FROM hrassess_empfield3_master WITH (NOLOCK) " & _
                                        "LEFT JOIN hrassess_empfield2_master WITH (NOLOCK) ON hrassess_empfield3_master.empfield2unkid = hrassess_empfield2_master.empfield2unkid AND hrassess_empfield2_master.isvoid = 0 " & _
                                        "LEFT JOIN hrassess_empfield1_master WITH (NOLOCK) ON hrassess_empfield2_master.empfield1unkid = hrassess_empfield1_master.empfield1unkid AND hrassess_empfield1_master.isvoid = 0 "
            'S.SANDEEP [25 MAR 2015] -- START
            If iCascadingTypeId <> enPACascading.NEITHER_CASCADING_NOR_ALIGNMENT Then
                StrQ &= " LEFT JOIN hrassess_owrfield1_master WITH (NOLOCK) ON hrassess_owrfield1_master.owrfield1unkid = hrassess_empfield1_master.owrfield1unkid AND hrassess_owrfield1_master.isvoid = 0 " & _
                        " LEFT JOIN hrassess_coyfield1_master WITH (NOLOCK) ON hrassess_coyfield1_master.coyfield1unkid = hrassess_owrfield1_master.coyfield1unkid AND hrassess_coyfield1_master.isvoid = 0 " & _
                        " LEFT JOIN hrassess_perspective_master WITH (NOLOCK) ON hrassess_perspective_master.perspectiveunkid = hrassess_coyfield1_master.perspectiveunkid AND hrassess_perspective_master.isactive = 1 "
            Else
                StrQ &= "LEFT JOIN hrassess_perspective_master WITH (NOLOCK) ON hrassess_perspective_master.perspectiveunkid = hrassess_empfield1_master.perspectiveunkid AND hrassess_perspective_master.isactive = 1 "
            End If
            'S.SANDEEP [25 MAR 2015] -- END
            StrQ &= "LEFT JOIN hremployee_master WITH (NOLOCK) ON hrassess_empfield3_master.employeeunkid = hremployee_master.employeeunkid " & _
                                        "LEFT JOIN cfcommon_period_tran WITH (NOLOCK) ON hrassess_empfield3_master.periodunkid = cfcommon_period_tran.periodunkid " & _
                                        "LEFT JOIN hrassess_empfield4_master WITH (NOLOCK) ON hrassess_empfield3_master.empfield3unkid = hrassess_empfield4_master.empfield3unkid AND hrassess_empfield4_master.isvoid = 0 " & _
                                        "LEFT JOIN hrassess_empfield5_master WITH (NOLOCK) ON hrassess_empfield4_master.empfield4unkid = hrassess_empfield5_master.empfield4unkid AND hrassess_empfield5_master.isvoid = 0 " & _
                                   "WHERE hrassess_empfield3_master.isvoid = 0 AND hremployee_master.employeeunkid = '" & iEmployeeId & "' AND cfcommon_period_tran.periodunkid = '" & iPeriodId & "' " & _
                    "        " & _
                              "UNION " & _
                                   "SELECT " & _
                                         "ISNULL(hremployee_master.employeecode,'')+ ' - ' + ISNULL(hremployee_master.firstname,'')+' '+ISNULL(hremployee_master.surname,'') AS Emp "
            'Shani (01-Dec-2016) -- ADD [AND hrassess_perspective_master.isactive = 1] 
            If iCascadingTypeId <> enPACascading.NEITHER_CASCADING_NOR_ALIGNMENT Then
                StrQ &= ",ISNULL(hrassess_owrfield1_master.field_data, '') AS OwrField1 "
            Else
                StrQ &= ",'' AS OwrField1 "
            End If
            StrQ &= ",ISNULL(hrassess_empfield1_master.field_data, '') AS Field1 " & _
                                        ",ISNULL(hrassess_empfield2_master.field_data, '') AS Field2 " & _
                                        ",ISNULL(hrassess_empfield3_master.field_data, '') AS Field3 " & _
                                        ",ISNULL(hrassess_empfield4_master.field_data, '') AS Field4 " & _
                                        ",ISNULL(hrassess_empfield5_master.field_data, '') AS Field5 " & _
                                        ",ISNULL(hrassess_empfield1_master.fieldunkid, 0) AS Field1Id " & _
                                        ",ISNULL(hrassess_empfield2_master.fieldunkid, 0) AS Field2Id " & _
                                        ",ISNULL(hrassess_empfield3_master.fieldunkid, 0) AS Field3Id " & _
                                        ",ISNULL(hrassess_empfield4_master.fieldunkid, 0) AS Field4Id " & _
                                        ",ISNULL(hrassess_empfield5_master.fieldunkid, 0) AS Field5Id " & _
                                        ",ISNULL(CONVERT(CHAR(8), hrassess_empfield1_master.startdate, 112),'') AS f1_SDate " & _
                                        ",ISNULL(CONVERT(CHAR(8), hrassess_empfield1_master.enddate, 112),'') AS f1_EDate " & _
                                        ",ISNULL(hrassess_empfield1_master.statusunkid, 0) AS f1StatId " & _
                                        ",ISNULL(hrassess_empfield1_master.weight, 0) AS f1weight " & _
                                        ",ISNULL(hrassess_empfield1_master.pct_completed, 0) AS f1pct " & _
                                        ",ISNULL(CONVERT(CHAR(8), hrassess_empfield2_master.startdate, 112),'') AS f2_SDate " & _
                                        ",ISNULL(CONVERT(CHAR(8), hrassess_empfield2_master.enddate, 112),'') AS f2_EDate " & _
                                        ",ISNULL(hrassess_empfield2_master.statusunkid, 0) AS f2StatId " & _
                                        ",ISNULL(hrassess_empfield2_master.weight, 0) AS f2weight " & _
                                        ",ISNULL(hrassess_empfield2_master.pct_completed, 0) AS f2pct " & _
                                        ",ISNULL(CONVERT(CHAR(8), hrassess_empfield3_master.startdate, 112),'') AS f3_SDate " & _
                                        ",ISNULL(CONVERT(CHAR(8), hrassess_empfield3_master.enddate, 112),'') AS f3_EDate " & _
                                        ",ISNULL(hrassess_empfield3_master.statusunkid, 0) AS f3StatId " & _
                                        ",ISNULL(hrassess_empfield3_master.weight, 0) AS f3weight " & _
                                        ",ISNULL(hrassess_empfield3_master.pct_completed, 0) AS f3pct " & _
                                        ",ISNULL(CONVERT(CHAR(8), hrassess_empfield4_master.startdate, 112),'') AS f4_SDate " & _
                                        ",ISNULL(CONVERT(CHAR(8), hrassess_empfield4_master.enddate, 112),'') AS f4_EDate " & _
                                        ",ISNULL(hrassess_empfield4_master.statusunkid, 0) AS f4StatId " & _
                                        ",ISNULL(hrassess_empfield4_master.weight, 0) AS f4weight " & _
                                        ",ISNULL(hrassess_empfield4_master.pct_completed, 0) AS f4pct " & _
                                        ",ISNULL(CONVERT(CHAR(8), hrassess_empfield5_master.startdate, 112),'') AS f5_SDate " & _
                                        ",ISNULL(CONVERT(CHAR(8), hrassess_empfield5_master.enddate, 112),'') AS f5_EDate " & _
                                        ",ISNULL(hrassess_empfield5_master.statusunkid, 0) AS f5StatId " & _
                                        ",ISNULL(hrassess_empfield5_master.weight, 0) AS f5weight " & _
                                        ",ISNULL(hrassess_empfield5_master.pct_completed, 0) AS f5pct " & _
                                        ",hrassess_empfield1_master.empfield1unkid " & _
                                        ",hrassess_empfield2_master.empfield2unkid " & _
                                        ",hrassess_empfield3_master.empfield3unkid " & _
                                        ",hrassess_empfield4_master.empfield4unkid " & _
                                        ",hrassess_empfield5_master.empfield5unkid " & _
                                        ",ISNULL(hrassess_empfield4_master.periodunkid, 0) AS periodunkid " & _
                                        ",ISNULL(period_name, '') AS OPeriod " & _
                                        ",ISNULL(hrassess_empfield1_master.owrfield1unkid, 0) AS owrfield1unkid " & _
                                        ",hremployee_master.employeeunkid " & _
                                        ",hrassess_empfield1_master.isfinal " & _
                                        ",hrassess_perspective_master.perspectiveunkid " & _
                                        ",ISNULL(hrassess_perspective_master.name,'') AS Perspective " & _
                                        ",ISNULL(hrassess_empfield1_master.goaltypeid," & CInt(enGoalType.GT_QUALITATIVE) & ") AS f1GoalTypeId" & _
                                        ",ISNULL(hrassess_empfield1_master.goalvalue, 0) AS f1GoalValue " & _
                                        ",ISNULL(hrassess_empfield2_master.goaltypeid," & CInt(enGoalType.GT_QUALITATIVE) & ") AS f2GoalTypeId" & _
                                        ",ISNULL(hrassess_empfield2_master.goalvalue, 0) AS f2GoalValue " & _
                                        ",ISNULL(hrassess_empfield3_master.goaltypeid," & CInt(enGoalType.GT_QUALITATIVE) & ") AS f3GoalTypeId" & _
                                        ",ISNULL(hrassess_empfield3_master.goalvalue, 0) AS f3GoalValue " & _
                                        ",ISNULL(hrassess_empfield4_master.goaltypeid," & CInt(enGoalType.GT_QUALITATIVE) & ") AS f4GoalTypeId" & _
                                        ",ISNULL(hrassess_empfield4_master.goalvalue, 0) AS f4GoalValue " & _
                                        ",ISNULL(hrassess_empfield5_master.goaltypeid," & CInt(enGoalType.GT_QUALITATIVE) & ") AS f5GoalTypeId" & _
                                        ",ISNULL(hrassess_empfield5_master.goalvalue, 0) AS f5GoalValue " & _
                                        ",ISNULL(hrassess_empfield1_master.uomtypeid, 0) AS f1UoMTypeId " & _
                                        ",ISNULL(hrassess_empfield2_master.uomtypeid, 0) AS f2UoMTypeId " & _
                                        ",ISNULL(hrassess_empfield3_master.uomtypeid, 0) AS f3UoMTypeId " & _
                                        ",ISNULL(hrassess_empfield4_master.uomtypeid, 0) AS f4UoMTypeId " & _
                                        ",ISNULL(hrassess_empfield5_master.uomtypeid, 0) AS f5UoMTypeId " & _
                                   "FROM hrassess_empfield4_master WITH (NOLOCK) " & _
                                        "LEFT JOIN hrassess_empfield3_master WITH (NOLOCK) ON hrassess_empfield4_master.empfield3unkid = hrassess_empfield3_master.empfield3unkid AND hrassess_empfield3_master.isvoid = 0 " & _
                                        "LEFT JOIN hrassess_empfield2_master WITH (NOLOCK) ON hrassess_empfield3_master.empfield2unkid = hrassess_empfield2_master.empfield2unkid AND hrassess_empfield2_master.isvoid = 0 " & _
                                        "LEFT JOIN hrassess_empfield1_master WITH (NOLOCK) ON hrassess_empfield2_master.empfield1unkid = hrassess_empfield1_master.empfield1unkid AND hrassess_empfield1_master.isvoid = 0 "
            'S.SANDEEP [25 MAR 2015] -- START
            If iCascadingTypeId <> enPACascading.NEITHER_CASCADING_NOR_ALIGNMENT Then
                StrQ &= " LEFT JOIN hrassess_owrfield1_master WITH (NOLOCK) ON hrassess_owrfield1_master.owrfield1unkid = hrassess_empfield1_master.owrfield1unkid AND hrassess_owrfield1_master.isvoid = 0 " & _
                        " LEFT JOIN hrassess_coyfield1_master WITH (NOLOCK) ON hrassess_coyfield1_master.coyfield1unkid = hrassess_owrfield1_master.coyfield1unkid AND hrassess_coyfield1_master.isvoid = 0 " & _
                        " LEFT JOIN hrassess_perspective_master WITH (NOLOCK) ON hrassess_perspective_master.perspectiveunkid = hrassess_coyfield1_master.perspectiveunkid AND hrassess_perspective_master.isactive = 1 "
            Else
                StrQ &= "LEFT JOIN hrassess_perspective_master WITH (NOLOCK) ON hrassess_perspective_master.perspectiveunkid = hrassess_empfield1_master.perspectiveunkid AND hrassess_perspective_master.isactive = 1 "
            End If
            'S.SANDEEP [25 MAR 2015] -- END
            StrQ &= "LEFT JOIN hremployee_master WITH (NOLOCK) ON hrassess_empfield4_master.employeeunkid = hremployee_master.employeeunkid " & _
                                        "LEFT JOIN cfcommon_period_tran WITH (NOLOCK) ON hrassess_empfield4_master.periodunkid = cfcommon_period_tran.periodunkid " & _
                                        "LEFT JOIN hrassess_empfield5_master WITH (NOLOCK) ON hrassess_empfield4_master.empfield4unkid = hrassess_empfield5_master.empfield4unkid AND hrassess_empfield5_master.isvoid = 0 " & _
                                   "WHERE hrassess_empfield4_master.isvoid = 0 AND hremployee_master.employeeunkid = '" & iEmployeeId & "' AND cfcommon_period_tran.periodunkid = '" & iPeriodId & "' " & _
                    "        " & _
                              "UNION " & _
                                   "SELECT " & _
                                         "ISNULL(hremployee_master.employeecode,'')+ ' - ' + ISNULL(hremployee_master.firstname,'')+' '+ISNULL(hremployee_master.surname,'') AS Emp "
            'Shani (01-Dec-2016) -- ADD [AND hrassess_perspective_master.isactive = 1] 
            If iCascadingTypeId <> enPACascading.NEITHER_CASCADING_NOR_ALIGNMENT Then
                StrQ &= ",ISNULL(hrassess_owrfield1_master.field_data, '') AS OwrField1 "
            Else
                StrQ &= ",'' AS OwrField1 "
            End If
            StrQ &= ",ISNULL(hrassess_empfield1_master.field_data, '') AS Field1 " & _
                                        ",ISNULL(hrassess_empfield2_master.field_data, '') AS Field2 " & _
                                        ",ISNULL(hrassess_empfield3_master.field_data, '') AS Field3 " & _
                                        ",ISNULL(hrassess_empfield4_master.field_data, '') AS Field4 " & _
                                        ",ISNULL(hrassess_empfield5_master.field_data, '') AS Field5 " & _
                                        ",ISNULL(hrassess_empfield1_master.fieldunkid, 0) AS Field1Id " & _
                                        ",ISNULL(hrassess_empfield2_master.fieldunkid, 0) AS Field2Id " & _
                                        ",ISNULL(hrassess_empfield3_master.fieldunkid, 0) AS Field3Id " & _
                                        ",ISNULL(hrassess_empfield4_master.fieldunkid, 0) AS Field4Id " & _
                                        ",ISNULL(hrassess_empfield5_master.fieldunkid, 0) AS Field5Id " & _
                                        ",ISNULL(CONVERT(CHAR(8), hrassess_empfield1_master.startdate, 112),'') AS f1_SDate " & _
                                        ",ISNULL(CONVERT(CHAR(8), hrassess_empfield1_master.enddate, 112),'') AS f1_EDate " & _
                                        ",ISNULL(hrassess_empfield1_master.statusunkid, 0) AS f1StatId " & _
                                        ",ISNULL(hrassess_empfield1_master.weight, 0) AS f1weight " & _
                                        ",ISNULL(hrassess_empfield1_master.pct_completed, 0) AS f1pct " & _
                                        ",ISNULL(CONVERT(CHAR(8), hrassess_empfield2_master.startdate, 112),'') AS f2_SDate " & _
                                        ",ISNULL(CONVERT(CHAR(8), hrassess_empfield2_master.enddate, 112),'') AS f2_EDate " & _
                                        ",ISNULL(hrassess_empfield2_master.statusunkid, 0) AS f2StatId " & _
                                        ",ISNULL(hrassess_empfield2_master.weight, 0) AS f2weight " & _
                                        ",ISNULL(hrassess_empfield2_master.pct_completed, 0) AS f2pct " & _
                                        ",ISNULL(CONVERT(CHAR(8), hrassess_empfield3_master.startdate, 112),'') AS f3_SDate " & _
                                        ",ISNULL(CONVERT(CHAR(8), hrassess_empfield3_master.enddate, 112),'') AS f3_EDate " & _
                                        ",ISNULL(hrassess_empfield3_master.statusunkid, 0) AS f3StatId " & _
                                        ",ISNULL(hrassess_empfield3_master.weight, 0) AS f3weight " & _
                                        ",ISNULL(hrassess_empfield3_master.pct_completed, 0) AS f3pct " & _
                                        ",ISNULL(CONVERT(CHAR(8), hrassess_empfield4_master.startdate, 112),'') AS f4_SDate " & _
                                        ",ISNULL(CONVERT(CHAR(8), hrassess_empfield4_master.enddate, 112),'') AS f4_EDate " & _
                                        ",ISNULL(hrassess_empfield4_master.statusunkid, 0) AS f4StatId " & _
                                        ",ISNULL(hrassess_empfield4_master.weight, 0) AS f4weight " & _
                                        ",ISNULL(hrassess_empfield4_master.pct_completed, 0) AS f4pct " & _
                                        ",ISNULL(CONVERT(CHAR(8), hrassess_empfield5_master.startdate, 112),'') AS f5_SDate " & _
                                        ",ISNULL(CONVERT(CHAR(8), hrassess_empfield5_master.enddate, 112),'') AS f5_EDate " & _
                                        ",ISNULL(hrassess_empfield5_master.statusunkid, 0) AS f5StatId " & _
                                        ",ISNULL(hrassess_empfield5_master.weight, 0) AS f5weight " & _
                                        ",ISNULL(hrassess_empfield5_master.pct_completed, 0) AS f5pct " & _
                                        ",hrassess_empfield1_master.empfield1unkid " & _
                                        ",hrassess_empfield2_master.empfield2unkid " & _
                                        ",hrassess_empfield3_master.empfield3unkid " & _
                                        ",hrassess_empfield4_master.empfield4unkid " & _
                                        ",hrassess_empfield5_master.empfield5unkid " & _
                                        ",ISNULL(hrassess_empfield5_master.periodunkid, 0) AS periodunkid " & _
                                        ",ISNULL(period_name, '') AS OPeriod " & _
                                        ",ISNULL(hrassess_empfield1_master.owrfield1unkid, 0) AS owrfield1unkid " & _
                                        ",hremployee_master.employeeunkid " & _
                                        ",hrassess_empfield1_master.isfinal " & _
                                        ",hrassess_perspective_master.perspectiveunkid " & _
                                        ",ISNULL(hrassess_perspective_master.name,'') AS Perspective " & _
                                        ",ISNULL(hrassess_empfield1_master.goaltypeid," & CInt(enGoalType.GT_QUALITATIVE) & ") AS f1GoalTypeId" & _
                                        ",ISNULL(hrassess_empfield1_master.goalvalue, 0) AS f1GoalValue " & _
                                        ",ISNULL(hrassess_empfield2_master.goaltypeid," & CInt(enGoalType.GT_QUALITATIVE) & ") AS f2GoalTypeId" & _
                                        ",ISNULL(hrassess_empfield2_master.goalvalue, 0) AS f2GoalValue " & _
                                        ",ISNULL(hrassess_empfield3_master.goaltypeid," & CInt(enGoalType.GT_QUALITATIVE) & ") AS f3GoalTypeId" & _
                                        ",ISNULL(hrassess_empfield3_master.goalvalue, 0) AS f3GoalValue " & _
                                        ",ISNULL(hrassess_empfield4_master.goaltypeid," & CInt(enGoalType.GT_QUALITATIVE) & ") AS f4GoalTypeId" & _
                                        ",ISNULL(hrassess_empfield4_master.goalvalue, 0) AS f4GoalValue " & _
                                        ",ISNULL(hrassess_empfield5_master.goaltypeid," & CInt(enGoalType.GT_QUALITATIVE) & ") AS f5GoalTypeId" & _
                                        ",ISNULL(hrassess_empfield5_master.goalvalue, 0) AS f5GoalValue " & _
                                        ",ISNULL(hrassess_empfield1_master.uomtypeid, 0) AS f1UoMTypeId " & _
                                        ",ISNULL(hrassess_empfield2_master.uomtypeid, 0) AS f2UoMTypeId " & _
                                        ",ISNULL(hrassess_empfield3_master.uomtypeid, 0) AS f3UoMTypeId " & _
                                        ",ISNULL(hrassess_empfield4_master.uomtypeid, 0) AS f4UoMTypeId " & _
                                        ",ISNULL(hrassess_empfield5_master.uomtypeid, 0) AS f5UoMTypeId " & _
                                   "FROM hrassess_empfield5_master WITH (NOLOCK) " & _
                                        "LEFT JOIN hrassess_empfield4_master WITH (NOLOCK) ON hrassess_empfield5_master.empfield4unkid = hrassess_empfield4_master.empfield4unkid AND hrassess_empfield4_master.isvoid = 0 " & _
                                        "LEFT JOIN hrassess_empfield3_master WITH (NOLOCK) ON hrassess_empfield4_master.empfield3unkid = hrassess_empfield3_master.empfield3unkid AND hrassess_empfield3_master.isvoid = 0 " & _
                                        "LEFT JOIN hrassess_empfield2_master WITH (NOLOCK) ON hrassess_empfield3_master.empfield2unkid = hrassess_empfield2_master.empfield2unkid AND hrassess_empfield2_master.isvoid = 0 " & _
                                        "LEFT JOIN hrassess_empfield1_master WITH (NOLOCK) ON hrassess_empfield2_master.empfield1unkid = hrassess_empfield1_master.empfield1unkid AND hrassess_empfield1_master.isvoid = 0 "
            'S.SANDEEP [25 MAR 2015] -- START
            If iCascadingTypeId <> enPACascading.NEITHER_CASCADING_NOR_ALIGNMENT Then
                StrQ &= " LEFT JOIN hrassess_owrfield1_master WITH (NOLOCK) ON hrassess_owrfield1_master.owrfield1unkid = hrassess_empfield1_master.owrfield1unkid AND hrassess_owrfield1_master.isvoid = 0 " & _
                        " LEFT JOIN hrassess_coyfield1_master WITH (NOLOCK) ON hrassess_coyfield1_master.coyfield1unkid = hrassess_owrfield1_master.coyfield1unkid AND hrassess_coyfield1_master.isvoid = 0 " & _
                        " LEFT JOIN hrassess_perspective_master WITH (NOLOCK) ON hrassess_perspective_master.perspectiveunkid = hrassess_coyfield1_master.perspectiveunkid AND hrassess_perspective_master.isactive = 1 "
            Else
                StrQ &= "LEFT JOIN hrassess_perspective_master WITH (NOLOCK) ON hrassess_perspective_master.perspectiveunkid = hrassess_empfield1_master.perspectiveunkid AND hrassess_perspective_master.isactive = 1 "
            End If
            'S.SANDEEP [25 MAR 2015] -- END

            'Shani (26-Sep-2016) -- Start
            'Enhancement -Changes PA Flow And Add New screen Given By (Andrew)
            'StrQ &= "LEFT JOIN hremployee_master ON hrassess_empfield5_master.employeeunkid = hremployee_master.employeeunkid " & _
            '                            "LEFT JOIN cfcommon_period_tran ON hrassess_empfield5_master.periodunkid = cfcommon_period_tran.periodunkid " & _
            '                       "WHERE hrassess_empfield5_master.isvoid = 0 AND hremployee_master.employeeunkid = '" & iEmployeeId & "' AND cfcommon_period_tran.periodunkid = '" & iPeriodId & "' " & _
            '                ") AS iLst " & _
            '                "LEFT JOIN " & _
            '                "( " & _
            '                    "SELECT EId,PId,STypId FROM " & _
            '                    "( " & _
            '                        "SELECT " & _
            '                            " employeeunkid AS EId " & _
            '                            ",periodunkid AS PId " & _
            '                            ",statustypeid AS STypId " & _
            '                            ",CONVERT(CHAR(8),status_date,112) AS Status_Date " & _
            '                            ",ROW_NUMBER() OVER(PARTITION BY employeeunkid,periodunkid ORDER BY statustranunkid DESC) AS RNo " & _
            '                        "FROM hrassess_empstatus_tran " & _
            '                    ")AS A WHERE RNo = 1 " & _
            '                ") AS Obj_Status ON Obj_Status.EId = iLst.employeeunkid AND Obj_Status.PId = iLst.periodunkid " & _
            '                "WHERE   1 = 1 " & _
            '                "ORDER BY perspectiveunkid ASC,empfield1unkid ASC, Field1 ASC "

            'Shani (01-Dec-2016) -- ADD [AND hrassess_perspective_master.isactive = 1] 
            StrQ &= "LEFT JOIN hremployee_master WITH (NOLOCK) ON hrassess_empfield5_master.employeeunkid = hremployee_master.employeeunkid " & _
                                        "LEFT JOIN cfcommon_period_tran WITH (NOLOCK) ON hrassess_empfield5_master.periodunkid = cfcommon_period_tran.periodunkid " & _
                                   "WHERE hrassess_empfield5_master.isvoid = 0 AND hremployee_master.employeeunkid = '" & iEmployeeId & "' AND cfcommon_period_tran.periodunkid = '" & iPeriodId & "' " & _
                    "        " & _
                            ") AS iLst " & _
                            "LEFT JOIN " & _
                            "( " & _
                                "SELECT EId,PId,STypId,STypeName,xcomments FROM " & _
                                "( " & _
                                    "SELECT " & _
                                        " employeeunkid AS EId " & _
                                        ",periodunkid AS PId " & _
                                        ",statustypeid AS STypId " & _
                                        ",CONVERT(CHAR(8),status_date,112) AS Status_Date " & _
                                        ",CASE WHEN statustypeid = " & enObjective_Status.SUBMIT_APPROVAL & " THEN @SUBMIT_APPROVAL " & _
                                        "      WHEN statustypeid = " & enObjective_Status.FINAL_SAVE & " THEN @FINAL_SAVE " & _
                                        "      WHEN statustypeid = " & enObjective_Status.OPEN_CHANGES & " THEN @OPEN_CHANGES " & _
                                        "      WHEN statustypeid = " & enObjective_Status.NOT_SUBMIT & " THEN @NOT_SUBMIT " & _
                                        "      WHEN statustypeid = " & enObjective_Status.NOT_COMMITTED & " THEN @NOT_COMMITTED " & _
                                        "      WHEN statustypeid = " & enObjective_Status.FINAL_COMMITTED & " THEN @FINAL_COMMITTED " & _
                                        "      WHEN statustypeid = " & enObjective_Status.PERIODIC_REVIEW & " THEN @PERIODIC_REVIEW " & _
                                        " END AS STypeName " & _
                                        ",commtents AS xcomments " & _
                                        ",ROW_NUMBER() OVER(PARTITION BY employeeunkid,periodunkid ORDER BY statustranunkid DESC) AS RNo " & _
                                    "FROM hrassess_empstatus_tran WITH (NOLOCK) " & _
                                    "WHERE hrassess_empstatus_tran.employeeunkid = '" & iEmployeeId & "' AND hrassess_empstatus_tran.periodunkid = '" & iPeriodId & "' " & _
                                ")AS A WHERE RNo = 1 " & _
                            ") AS Obj_Status ON Obj_Status.EId = iLst.employeeunkid AND Obj_Status.PId = iLst.periodunkid " & _
                            "LEFT JOIN " & _
                            "( " & _
                            "   SELECT " & _
                            "        Statuid AS GoalsAccomplishmentids " & _
                            "       ,CASE Statuid WHEN " & clsassess_empupdate_tran.enGoalAccomplished_Status.GA_Pending & " THEN @GA_Pending " & _
                            "                     WHEN " & clsassess_empupdate_tran.enGoalAccomplished_Status.GA_Approved & " THEN @GA_Approved " & _
                            "                     WHEN " & clsassess_empupdate_tran.enGoalAccomplished_Status.GA_Rejected & " THEN @GA_Rejected " & _
                            "        ELSE '' END AS GoalsAccomplishmentStatus " & _
                            "       ,A.empfieldunkid " & _
                            "       ,A.empupdatetranunkid " & _
                            "   FROM " & _
                            "   ( " & _
                            "       SELECT " & _
                            "           hrassess_empupdate_tran.approvalstatusunkid AS Statuid " & _
                            "           ,hrassess_empupdate_tran.empfieldunkid " & _
                            "           ,hrassess_empupdate_tran.empfieldtypeid " & _
                            "           ,hrassess_empupdate_tran.empupdatetranunkid " & _
                            "           ,ROW_NUMBER() OVER (PARTITION BY empfieldunkid ORDER BY approval_date DESC,hrassess_empupdate_tran.empupdatetranunkid DESC) AS RNo " & _
                            "       FROM hrassess_empupdate_tran WITH (NOLOCK) " & _
                            "       WHERE hrassess_empupdate_tran.employeeunkid = '" & iEmployeeId & "' AND hrassess_empupdate_tran.periodunkid = '" & iPeriodId & "' " & _
                            "       AND hrassess_empupdate_tran.isvoid = 0 " & _
                            "   ) AS A " & _
                            "   WHERE RNo = 1 " & _
                            ") AS Obj_GoalAccomplishment ON Obj_GoalAccomplishment.empfieldunkid = CASE @LinkFieldId WHEN Field1Id THEN iLst.empfield1unkid " & _
                            "                                                                                        WHEN Field2Id THEN iLst.empfield2unkid " & _
                            "                                                                                        WHEN Field3Id THEN iLst.empfield3unkid " & _
                            "                                                                                        WHEN Field4Id THEN iLst.empfield4unkid " & _
                            "                                                                                        WHEN Field5Id THEN iLst.empfield5unkid END " & _
                            "LEFT JOIN cfcommon_master AS UoM WITH (NOLOCK) ON UoM.masterunkid = (CASE WHEN @LinkFieldId = Field1Id THEN f1UoMTypeId " & _
                            "                                                            WHEN @LinkFieldId = Field2Id THEN f2UoMTypeId " & _
                            "                                                            WHEN @LinkFieldId = Field3Id THEN f3UoMTypeId " & _
                            "                                                            WHEN @LinkFieldId = Field4Id THEN f4UoMTypeId " & _
                            "                                                            WHEN @LinkFieldId = Field5Id THEN f5UoMTypeId END) "
            'Hemant (28 Apr 2023) -- Start
            'ENHANCEMENT(NMB) : A1X-667 - As a user, i want to have the ability to approve/decline some objectives in a score card instead of approving/declining it as a whole
            StrQ &= "LEFT JOIN " & _
                            "( " & _
                            "   SELECT " & _
                            "        B.empfieldunkid " & _
                            "       ,B.statustypeid " & _
                            "   FROM " & _
                            "   ( " & _
                            "       SELECT " & _
                            "           hrassess_empfield_status_tran.empfieldunkid " & _
                            "           ,hrassess_empfield_status_tran.statustypeid " & _
                            "           ,ROW_NUMBER() OVER (PARTITION BY empfieldunkid ORDER BY status_date DESC) AS RNo " & _
                            "       FROM hrassess_empfield_status_tran WITH (NOLOCK) " & _
                            "       WHERE hrassess_empfield_status_tran.employeeunkid = '" & iEmployeeId & "' AND hrassess_empfield_status_tran.periodunkid = '" & iPeriodId & "' " & _
                            "   ) AS B " & _
                            "   WHERE RNo = 1 " & _
                            ") AS Obj_GoalStatus ON Obj_GoalStatus.empfieldunkid = CASE @LinkFieldId WHEN Field1Id THEN iLst.empfield1unkid " & _
                            "                                                                        WHEN Field2Id THEN iLst.empfield2unkid " & _
                            "                                                                        WHEN Field3Id THEN iLst.empfield3unkid " & _
                            "                                                                        WHEN Field4Id THEN iLst.empfield4unkid " & _
                            "                                                                        WHEN Field5Id THEN iLst.empfield5unkid END "
            'Hemant (28 Apr 2023) -- End

            StrQ &= "        WHERE   1 = 1 " & _
                            "ORDER BY perspectiveunkid ASC,empfield1unkid ASC, Field1 ASC "

            'S.SANDEEP |04-DEC-2019| -- START
            'ISSUE/ENHANCEMENT : REPORT TEMPLATE 17 -- NMB
            '",commtents AS xcomments " --------------- ADDED
            'S.SANDEEP |04-DEC-2019| -- END

            'S.SANDEEP |20-SEP-2019| -- START {Ref#0004155} {AND hrassess_empupdate_tran.isvoid = 0} -- END
            'Shani (26-Sep-2016) -- End
            'S.SANDEEP [06 NOV 2015] -- START {REMOVED "ORDER BY Field1Id DESC, Field1 ASC "} -- END

            'S.SANDEEP |16-AUG-2019| -- START
            'ISSUE/ENHANCEMENT : ZRA {Language & Custom Item} & NMB Email Notification {Calibration}
            '---------- REMOVED {hrassess_empfield1_master.perspectiveunkid}
            '---------- ADDED   {hrassess_perspective_master.perspectiveunkid}
            '---------- ADDED   {WHERE hrassess_empupdate_tran.employeeunkid = '" & iEmployeeId & "' AND hrassess_empupdate_tran.periodunkid = '" & iPeriodId & "'}
            'S.SANDEEP |16-AUG-2019| -- END


            'objDataOperation.AddParameter("@FINANCIAL", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage("clsMasterData", 315, "Financial"))
            'objDataOperation.AddParameter("@CUSTOMER", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage("clsMasterData", 316, "Customer"))
            'objDataOperation.AddParameter("@BUSINESS_PROCESS", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage("clsMasterData", 317, "Business Process"))
            'objDataOperation.AddParameter("@ORGANIZATION_CAPACITY", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage("clsMasterData", 318, "Organization Capacity"))

            objDataOperation.AddParameter("@ST_PENDING", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage("clsMasterData", 518, "Pending"))
            objDataOperation.AddParameter("@ST_INPROGRESS", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage("clsMasterData", 519, "In progress"))
            objDataOperation.AddParameter("@ST_COMPLETE", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage("clsMasterData", 520, "Complete"))
            objDataOperation.AddParameter("@ST_CLOSED", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage("clsMasterData", 521, "Closed"))
            objDataOperation.AddParameter("@ST_ONTRACK", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage("clsMasterData", 522, "On Track"))
            objDataOperation.AddParameter("@ST_ATRISK", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage("clsMasterData", 523, "At Risk"))
            objDataOperation.AddParameter("@ST_NOTAPPLICABLE", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage("clsMasterData", 524, "Not Applicable"))

            'Shani (26-Sep-2016) -- Start
            'Enhancement -Changes PA Flow And Add New screen Given By (Andrew)

            'S.SANDEEP [10-Mar-2018] -- START
            'objDataOperation.AddParameter("@GA_Pending", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage("clsMasterData", 757, "Pending"))
            'objDataOperation.AddParameter("@GA_Approved", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage("clsMasterData", 758, "Approved"))
            'objDataOperation.AddParameter("@GA_Rejected", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage("clsMasterData", 759, "Disapprove"))

            objDataOperation.AddParameter("@GA_Pending", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage("clsMasterData", 759, "Pending"))
            objDataOperation.AddParameter("@GA_Approved", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage("clsMasterData", 760, "Approved"))
            objDataOperation.AddParameter("@GA_Rejected", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage("clsMasterData", 761, "Disapprove"))
            'S.SANDEEP [10-Mar-2018] -- END

            'Shani (26-Sep-2016) -- End

            'S.SANDEEP [11-OCT-2018] -- START
            objDataOperation.AddParameter("@GT_QUALITATIVE", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage("clsMasterData", 843, "Qualitative"))
            objDataOperation.AddParameter("@GT_QUANTITATIVE", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage("clsMasterData", 844, "Quantitative"))
            'S.SANDEEP [11-OCT-2018] -- END


            'S.SANDEEP |12-FEB-2019| -- START
            'ISSUE/ENHANCEMENT : {Performance Assessment Changes}
            objDataOperation.AddParameter("@Submit_Approval", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage("clsBSC_Planning_Report", 1, "Submitted For Approval"))
            'S.SANDEEP |12-MAR-2019| -- START
            'ISSUE/ENHANCEMENT : {Action List Phase 2 - 61}
            'objDataOperation.AddParameter("@Final_Save", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage("clsBSC_Planning_Report", 2, "Final Saved"))


            'Pinkal (22-Mar-2019) -- Start
            'Enhancement - Working on Leave & Claim Enhancement Phase 2 for NMB.
            'objDataOperation.AddParameter("@Final_Save", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage("clsBSC_Planning_Report", 2, "Final Approved"))
            objDataOperation.AddParameter("@Final_Save", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage("clsBSC_Planning_Report", 101, "Approved"))
            'Pinkal (22-Mar-2019) -- End


            'S.SANDEEP |12-MAR-2019| -- END
            objDataOperation.AddParameter("@Open_Changes", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage("clsBSC_Planning_Report", 3, "Opened For Changes"))
            objDataOperation.AddParameter("@Not_Submit", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage("clsBSC_Planning_Report", 4, "Not Submitted For Approval"))
            objDataOperation.AddParameter("@NOT_COMMITTED", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage("clsBSC_Planning_Report", 49, "Not Committed"))
            objDataOperation.AddParameter("@FINAL_COMMITTED", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage("clsBSC_Planning_Report", 50, "Final Committed"))
            objDataOperation.AddParameter("@PERIODIC_REVIEW", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage("clsBSC_Planning_Report", 51, "Periodic Review"))
            'S.SANDEEP |12-FEB-2019| -- END

            dsList = objDataOperation.ExecQuery(StrQ, iList)

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & " : " & objDataOperation.ErrorMessage)
                Throw exForce
            End If


            'S.SANDEEP [16 JUN 2015] -- START
            Dim dsProgress As New DataSet
            Dim objProgress As New clsassess_empupdate_tran

            'Shani (26-Sep-2016) -- Start
            'Enhancement -Changes PA Flow And Add New screen Given By (Andrew)
            'dsProgress = objProgress.GetLatestProgress(ConfigParameter._Object._CurrentDateAndTime.Date, iEmployeeId, iPeriodId)

            'S.SANDEEP |20-SEP-2019| -- START
            'ISSUE/ENHANCEMENT : {Ref#0004155}
            'dsProgress = objProgress.GetLatestProgress(ConfigParameter._Object._CurrentDateAndTime.Date, iEmployeeId, iPeriodId, iIsGoalAccomplishementScreen)
            dsProgress = objProgress.GetLatestProgress(ConfigParameter._Object._CurrentDateAndTime.Date, iEmployeeId, iPeriodId, True)
            'S.SANDEEP |20-SEP-2019| -- END

            'Shani (26-Sep-2016) -- End


            objProgress = Nothing
            'S.SANDEEP [16 JUN 2015] -- END



            'S.SANDEEP [25 MAR 2015] -- START
            Dim xCaption As String = String.Empty
            'S.SANDEEP [25 MAR 2015] -- END


            Dim objInfoField As New clsassess_empinfofield_tran
            Dim mdicFieldData As New Dictionary(Of Integer, String)
            For Each dRow As DataRow In dsList.Tables(iList).Rows
                mdicFieldData = New Dictionary(Of Integer, String)

                If dRow.Item("St_Date").ToString.Trim.Length > 0 Then
                    dRow.Item("St_Date") = eZeeDate.convertDate(dRow.Item("St_Date").ToString).ToShortDateString
                End If
                If dRow.Item("Ed_Date").ToString.Trim.Length > 0 Then
                    dRow.Item("Ed_Date") = eZeeDate.convertDate(dRow.Item("Ed_Date").ToString).ToShortDateString
                End If

                Dim objOwnerTran As New clsassess_empowner_tran
                Select Case CInt(dRow.Item("CFieldTypeId"))
                    Case enWeight_Types.WEIGHT_FIELD1
                        dRow.Item("OwnerIds") = objOwnerTran.GetCSV_OwnerIds(dRow.Item("empfield1unkid"), dRow.Item("CFieldTypeId"))
                        mdicFieldData = objInfoField.Get_Data(dRow.Item("empfield1unkid"), dRow.Item("CFieldTypeId"))
                    Case enWeight_Types.WEIGHT_FIELD2
                        dRow.Item("OwnerIds") = objOwnerTran.GetCSV_OwnerIds(dRow.Item("empfield2unkid"), dRow.Item("CFieldTypeId"))
                        mdicFieldData = objInfoField.Get_Data(dRow.Item("empfield2unkid"), dRow.Item("CFieldTypeId"))
                    Case enWeight_Types.WEIGHT_FIELD3
                        dRow.Item("OwnerIds") = objOwnerTran.GetCSV_OwnerIds(dRow.Item("empfield3unkid"), dRow.Item("CFieldTypeId"))
                        mdicFieldData = objInfoField.Get_Data(dRow.Item("empfield3unkid"), dRow.Item("CFieldTypeId"))
                    Case enWeight_Types.WEIGHT_FIELD4
                        dRow.Item("OwnerIds") = objOwnerTran.GetCSV_OwnerIds(dRow.Item("empfield4unkid"), dRow.Item("CFieldTypeId"))
                        mdicFieldData = objInfoField.Get_Data(dRow.Item("empfield4unkid"), dRow.Item("CFieldTypeId"))
                    Case enWeight_Types.WEIGHT_FIELD5
                        dRow.Item("OwnerIds") = objOwnerTran.GetCSV_OwnerIds(dRow.Item("empfield5unkid"), dRow.Item("CFieldTypeId"))
                        mdicFieldData = objInfoField.Get_Data(dRow.Item("empfield5unkid"), dRow.Item("CFieldTypeId"))
                End Select

                'S.SANDEEP [25 MAR 2015] -- START

                'S.SANDEEP [23 APR 2015] -- START
                'If xCaption <> dRow.Item(xFieldColName) Then
                '    xCaption = dRow.Item(xFieldColName)
                'Else
                '    dRow.Item("Weight") = ""
                'End If

                If xCaption <> dRow.Item("Caption") Then
                    xCaption = dRow.Item("Caption")
                Else
                    dRow.Item("Weight") = ""
                End If
                'S.SANDEEP [23 APR 2015] -- END

                
                'S.SANDEEP [25 MAR 2015] -- END


                mdtFinal.ImportRow(dRow)

                If dRow.Item("St_Date").ToString.Trim.Length > 0 Then
                    mdtFinal.Rows(mdtFinal.Rows.Count - 1).Item("sdate") = eZeeDate.convertDate(CDate(dRow.Item("St_Date")))
                End If
                If dRow.Item("Ed_Date").ToString.Trim.Length > 0 Then
                    mdtFinal.Rows(mdtFinal.Rows.Count - 1).Item("edate") = eZeeDate.convertDate(CDate(dRow.Item("Ed_Date")))
                End If

                'S.SANDEEP [11-OCT-2018] -- START
                mdtFinal.Rows(mdtFinal.Rows.Count - 1).Item("dgoalvalue") = CDbl(dRow.Item("goalvalue"))
                'S.SANDEEP [11-OCT-2018] -- END

                'opstatusid

                'S.SANDEEP [16 JUN 2015] -- START
                If dsProgress IsNot Nothing AndAlso dsProgress.Tables(0).Rows.Count > 0 Then
                    Dim intTableUnkid As Integer = 0
                    Select Case CInt(dRow.Item("CFieldTypeId"))
                        Case enWeight_Types.WEIGHT_FIELD1
                            intTableUnkid = dRow.Item("empfield1unkid")
                        Case enWeight_Types.WEIGHT_FIELD2
                            intTableUnkid = dRow.Item("empfield2unkid")
                        Case enWeight_Types.WEIGHT_FIELD3
                            intTableUnkid = dRow.Item("empfield3unkid")
                        Case enWeight_Types.WEIGHT_FIELD4
                            intTableUnkid = dRow.Item("empfield4unkid")
                        Case enWeight_Types.WEIGHT_FIELD5
                            intTableUnkid = dRow.Item("empfield5unkid")
                    End Select

                    Dim pRow() As DataRow = dsProgress.Tables(0).Select("empfieldtypeid = '" & dRow.Item("CFieldTypeId") & "' AND empfieldunkid = '" & intTableUnkid & "'")
                    If pRow.Length > 0 Then
                        mdtFinal.Rows(mdtFinal.Rows.Count - 1).Item("pct_complete") = pRow(0).Item("pct_completed")
                        mdtFinal.Rows(mdtFinal.Rows.Count - 1).Item("CStatusId") = pRow(0).Item("statusunkid")
                        mdtFinal.Rows(mdtFinal.Rows.Count - 1).Item("CStatus") = pRow(0).Item("dstatus")
                        mdtFinal.Rows(mdtFinal.Rows.Count - 1).Item("vuRemark") = pRow(0).Item("remark")
                    Else
                        mdtFinal.Rows(mdtFinal.Rows.Count - 1).Item("pct_complete") = 0
                        mdtFinal.Rows(mdtFinal.Rows.Count - 1).Item("CStatusId") = 0
                        mdtFinal.Rows(mdtFinal.Rows.Count - 1).Item("CStatus") = ""
                        mdtFinal.Rows(mdtFinal.Rows.Count - 1).Item("vuRemark") = ""
                    End If
                    'S.SANDEEP |21-AUG-2019| -- START
                    'ISSUE/ENHANCEMENT : DECIMAL PLACES ISSUES
                Else
                    mdtFinal.Rows(mdtFinal.Rows.Count - 1).Item("pct_complete") = 0
                    mdtFinal.Rows(mdtFinal.Rows.Count - 1).Item("CStatusId") = 0
                    mdtFinal.Rows(mdtFinal.Rows.Count - 1).Item("CStatus") = ""
                    mdtFinal.Rows(mdtFinal.Rows.Count - 1).Item("vuRemark") = ""
                    'S.SANDEEP |21-AUG-2019| -- END
                End If
                'S.SANDEEP [16 JUN 2015] -- END
                

                If mdicFieldData.Keys.Count > 0 Then
                    For Each iKey As Integer In mdicFieldData.Keys
                        If mdtFinal.Columns.Contains("Field" & iKey.ToString) Then
                            mdtFinal.Rows(mdtFinal.Rows.Count - 1).Item("Field" & iKey.ToString) = mdicFieldData(iKey)
                            mdtFinal.Rows(mdtFinal.Rows.Count - 1).Item("Field" & iKey.ToString & "Id") = iKey
                        End If
                    Next
                End If
            Next
            objInfoField = Nothing

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "GetDisplayList", mstrModuleName)
        Finally
        End Try
        Return mdtFinal
    End Function

    Public Function Get_EmployeeField1Unkid(ByVal iFieldData As String, ByVal iEmployeeId As Integer, ByVal iPeriodId As Integer) As Integer
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception
        objDataOperation = New clsDataOperation
        Try
            strQ = "SELECT " & _
                   "  empfield1unkid " & _
                   "FROM hrassess_empfield1_master WITH (NOLOCK) " & _
                   "WHERE field_data = @field_data AND isvoid = 0 AND employeeunkid = @employeeunkid AND periodunkid = @periodunkid "

            objDataOperation.AddParameter("@field_data", SqlDbType.NVarChar, mstrField_Data.Length, iFieldData)
            objDataOperation.AddParameter("@employeeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, iEmployeeId)
            objDataOperation.AddParameter("@periodunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, iPeriodId)

            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            If dsList.Tables(0).Rows.Count > 0 Then
                Return dsList.Tables(0).Rows(0).Item("empfield1unkid")
            Else
                Return 0
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "Get_EmployeeField1Unkid", mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
    End Function

    'S.SANDEEP |18-FEB-2019| -- START
    'ISSUE/ENHANCEMENT : {Performance Assessment Changes}
    'Public Sub Send_Notification_Assessor(ByVal iEmployeeId As Integer, _
    '                                      ByVal iPeriodId As Integer, _
    '                                      ByVal iYearId As Integer, _
    '                                      ByVal sYearName As String, _
    '                                      ByVal strDatabaseName As String, _
    '                                      Optional ByVal iCompanyId As Integer = 0, _
    '                                      Optional ByVal sArutiSSURL As String = "", _
    '                                      Optional ByVal iLoginTypeId As Integer = 0, _
    '                                      Optional ByVal iLoginEmployeeId As Integer = 0, _
    '                                      Optional ByVal iUserId As Integer = 0)
    '    'Sohail (21 Aug 2015) - [strDatabaseName]

    '    Dim dsList As DataSet = Nothing
    '    Dim strLink As String = String.Empty
    '    Dim StrQ As String = String.Empty

    '    objDataOperation = New clsDataOperation

    '    Try
    '        If iCompanyId <= 0 Then iCompanyId = Company._Object._Companyunkid
    '        If sArutiSSURL = "" Then sArutiSSURL = ConfigParameter._Object._ArutiSelfServiceURL
    '        Dim objNet As New clsNetConnectivity : If objNet._Conected = False Then Exit Sub

    '        'Shani(01-MAR-2016) -- Start
    '        'Enhancement :PA External Approver Flow
    '        'StrQ = "SELECT " & _
    '        '       "  ISNULL(hremployee_master.firstname,'')+' '+ISNULL(hremployee_master.surname,'') AS Ename " & _
    '        '       " ,ISNULL(email,'') AS Email " & _
    '        '       " ,hrassessor_master.assessormasterunkid " & _
    '        '       " ,hrapprover_usermapping.userunkid " & _
    '        '       "FROM hrapprover_usermapping " & _
    '        '       " JOIN hrassessor_master ON hrapprover_usermapping.approverunkid = hrassessor_master.assessormasterunkid " & _
    '        '       " JOIN hremployee_master ON hrassessor_master.employeeunkid = hremployee_master.employeeunkid " & _
    '        '       " JOIN hrassessor_tran ON hrassessor_tran.assessormasterunkid = hrassessor_master.assessormasterunkid " & _
    '        '       "WHERE usertypeid = '" & enUserType.Assessor & "' AND hrassessor_tran.employeeunkid = '" & iEmployeeId & "' AND isreviewer = 0 "

    '        ''S.SANDEEP [01 OCT 2015] -- START
    '        'StrQ &= " AND  hrassessor_master.isvoid = 0 AND hrassessor_tran.isvoid = 0 "
    '        ''S.SANDEEP [01 OCT 2015] -- END
    '        'dsList = objDataOperation.ExecQuery(StrQ, "List")

    '        'If objDataOperation.ErrorMessage <> "" Then
    '        '    mstrMessage = objDataOperation.ErrorNumber & " : " & objDataOperation.ErrorMessage
    '        'End If

    '        ' ''StrQ = "SELECT DISTINCT " & _
    '        ' ''       "     cfuser_master.companyunkid" & _
    '        ' ''       "    ,ISNULL(cffinancial_year_tran.database_name,'') AS DBName " & _
    '        ' ''       "    ,hrassessor_master.isexternalapprover " & _
    '        ' ''       "FROM hrassessor_tran " & _
    '        ' ''       "    JOIN hrassessor_master ON hrassessor_master.assessormasterunkid = hrassessor_tran.assessormasterunkid AND hrassessor_master.isvoid = 0 " & _
    '        ' ''       "    JOIN hrmsConfiguration..cfuser_master ON hrassessor_master.employeeunkid = cfuser_master.userunkid " & _
    '        ' ''       "    LEFT JOIN hrmsConfiguration..cffinancial_year_tran ON cffinancial_year_tran.companyunkid = cfuser_master.companyunkid AND cffinancial_year_tran.isclosed = 0 " & _
    '        ' ''       "WHERE hrassessor_tran.isvoid = 0 AND hrassessor_tran.employeeunkid = '" & iEmployeeId & "' AND hrassessor_master.isreviewer = 0 "

    '        ' ''Dim dsTemp As DataSet = objDataOperation.ExecQuery(StrQ, "List")

    '        dsList = (New clsAssessor).GetEmailNotificationList(iEmployeeId, False, objDataOperation, "List")

    '        'Shani(01-MAR-2016) -- End


    '        Dim objEmp As New clsEmployee_Master
    '        Dim objMail As New clsSendMail
    '        Dim objPeriod As New clscommom_period_Tran

    '        'Sohail (21 Aug 2015) -- Start
    '        'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
    '        'objEmp._Employeeunkid = iEmployeeId : objPeriod._Periodunkid = iPeriodId

    '        'S.SANDEEP [04 JUN 2015] -- START
    '        'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
    '        'objEmp._Employeeunkid = iEmployeeId : objPeriod._Periodunkid(strDatabaseName) = iPeriodId
    '        objPeriod._Periodunkid(strDatabaseName) = iPeriodId
    '        objEmp._Employeeunkid(objPeriod._End_Date) = iEmployeeId
    '        'S.SANDEEP [04 JUN 2015] -- END

    '        'Sohail (21 Aug 2015) -- End

    '        Dim dsYear As New DataSet
    '        Dim objFY As New clsCompany_Master
    '        dsYear = objFY.GetFinancialYearList(iCompanyId, , "List", objPeriod._Yearunkid)
    '        If dsYear.Tables("List").Rows.Count > 0 Then
    '            sYearName = dsYear.Tables("List").Rows(0).Item("financialyear_name")
    '        End If
    '        objFY = Nothing

    '        For Each dtRow As DataRow In dsList.Tables("List").Rows
    '            If dtRow.Item("Email") = "" Then Continue For
    '            Dim strMessage As String = ""
    '            objMail._Subject = Language.getMessage(mstrModuleName, 4, "Notification for Approval/Rejection BSC Planning.")
    '            strMessage = "<HTML> <BODY>"
    '            strMessage &= Language.getMessage(mstrModuleName, 5, "Dear") & " " & dtRow.Item("Ename").ToString() & ", <BR><BR>"
    '            strMessage &= "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;" & Language.getMessage(mstrModuleName, 6, "This is to inform you that i have completed my Balanced Score Card Planning for ") & _
    '            objPeriod._Period_Name & ", " & sYearName & Language.getMessage(mstrModuleName, 7, ". Please click the link below to review it and thereafter approve/reject it.")
    '            strMessage &= "<BR><BR>"
    '            strMessage &= objEmp._Firstname & " " & objEmp._Surname
    '            'S.SANDEEP [29 JAN 2015] -- START
    '            'strLink = sArutiSSURL & "/Assessment/BSC Masters/wPgApprove_Reject_BSC.aspx?" & System.Web.HttpUtility.UrlEncode(clsCrypto.Encrypt(iCompanyId.ToString & "|" & dtRow.Item("userunkid").ToString & "|" & iEmployeeId.ToString & "|" & dtRow.Item("assessormasterunkid").ToString & "|" & iYearId.ToString & "|" & sYearName.ToString & "|" & iPeriodId.ToString))
    '            strLink = sArutiSSURL & "/Assessment New/Common/wPgPerformance_Planning.aspx?" & System.Web.HttpUtility.UrlEncode(clsCrypto.Encrypt(iCompanyId.ToString & "|" & dtRow.Item("userunkid").ToString & "|" & iEmployeeId.ToString & "|" & dtRow.Item("assessormasterunkid").ToString & "|" & iPeriodId.ToString))
    '            'S.SANDEEP [29 JAN 2015] -- END
    '            strMessage &= "<BR></BR>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<a href='" & strLink & "'>" & strLink & "</a>"
    '            strMessage &= "<BR></BR><BR></BR><center>""POWERED BY ARUTI HR & PAYROLL MANAGEMENT SOFTWARE""</center>"
    '            strMessage &= "</BODY></HTML>"
    '            objMail._Message = strMessage
    '            objMail._ToEmail = dtRow.Item("Email")
    '            If iLoginTypeId <= 0 Then iLoginTypeId = enLogin_Mode.DESKTOP
    '            If mstrWebFrmName.Trim.Length > 0 Then
    '                objMail._Form_Name = mstrWebFrmName
    '            End If
    '            objMail._LogEmployeeUnkid = iLoginEmployeeId
    '            objMail._OperationModeId = iLoginTypeId
    '            objMail._UserUnkid = IIf(iUserId <= 0, User._Object._Userunkid, iUserId)
    '            objMail._SenderAddress = IIf(objEmp._Email = "", objEmp._Firstname & " " & objEmp._Surname, objEmp._Email)
    '            objMail._ModuleRefId = clsSendMail.enAT_VIEW_TYPE.ASSESSMENT_MGT
    '            'Sohail (30 Nov 2017) -- Start
    '            'SUMATRA Enhancement – SUMATRA – issue # 0001669: Additional feature for Exchange Server configuration setting in 70.1.
    '            'objMail.SendMail()
    '            objMail.SendMail(iCompanyId)
    '            'Sohail (30 Nov 2017) -- End
    '        Next

    '    Catch ex As Exception
    '        DisplayError.Show("-1", ex.Message, "Send_Notification_Assessor", mstrModuleName)
    '    Finally
    '    End Try
    'End Sub
    'Public Sub Send_Notification_Employee(ByVal iEmployeeId As Integer, _
    '                                      ByVal sAssessorName As String, _
    '                                      ByVal sComments As String, _
    '                                      ByVal isFinalOpr As Boolean, _
    '                                      ByVal isUnlock As Boolean, _
    '                                      ByVal intCompanyUnkId As Integer, _
    '                                      Optional ByVal iLoginTypeId As Integer = 0, _
    '                                      Optional ByVal iLoginEmployeeId As Integer = 0, _
    '                                      Optional ByVal iUserId As Integer = 0, _
    '                                      Optional ByVal strCsvUserIds As String = "") 'S.SANDEEP [29-NOV-2017] -- START {REF-ID # (38,41)} -- END
    '    Try
    '        Dim objEmp As New clsEmployee_Master
    '        Dim objMail As New clsSendMail

    '        'S.SANDEEP [04 JUN 2015] -- START
    '        'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
    '        'objEmp._Employeeunkid = iEmployeeId
    '        objEmp._Employeeunkid(Now.Date) = iEmployeeId
    '        'S.SANDEEP [04 JUN 2015] -- END


    '        If objEmp._Email.Trim.Length > 0 Then
    '            Dim strMessage As String = ""

    '            strMessage = "<HTML> <BODY>"
    '            strMessage &= Language.getMessage(mstrModuleName, 5, "Dear") & " " & objEmp._Firstname & " " & objEmp._Surname & ", <BR><BR>"

    '            If isFinalOpr = True Then
    '                objMail._Subject = Language.getMessage(mstrModuleName, 8, "Notification for Approval of BSC Planning")
    '                strMessage &= "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;" & Language.getMessage(mstrModuleName, 10, "This is to inform you that your Balanced Score Card Planning has been approved and final saved by ") & "<b>" & sAssessorName & "</b>" & "."
    '            Else
    '                If isUnlock = True Then
    '                    objMail._Subject = Language.getMessage(mstrModuleName, 15, "Notification for Unlocked of BSC Planning")
    '                    strMessage &= "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;" & Language.getMessage(mstrModuleName, 16, "This is to inform you that your Balanced Score Card Planning has been Unlocked by ") & "<b>" & sAssessorName & "</b>" & Language.getMessage(mstrModuleName, 12, " and it is now open for editing.") & "<br>"
    '                Else
    '                    objMail._Subject = Language.getMessage(mstrModuleName, 9, "Notification for Rejection of BSC Planning")
    '                    strMessage &= "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;" & Language.getMessage(mstrModuleName, 11, "This is to inform you that your Balanced Score Card Planning has been rejected by ") & "<b>" & sAssessorName & "</b>" & Language.getMessage(mstrModuleName, 12, " and it is now open for editing.") & "<br>"
    '                End If
    '                strMessage &= "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;" & Language.getMessage(mstrModuleName, 13, "Your advised to make the changes recommended by your assessor as per remarks below.") & "<br><br>" & _
    '                              "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; <b>" & Language.getMessage(mstrModuleName, 14, "Comments :") & "<b>" & sComments

    '            End If
    '            strMessage &= "<BR></BR><BR></BR><center>""POWERED BY ARUTI HR & PAYROLL MANAGEMENT SOFTWARE""</center>"
    '            strMessage &= "</BODY></HTML>"
    '            objMail._ToEmail = objEmp._Email
    '            objMail._Message = strMessage
    '            If iLoginTypeId <= 0 Then iLoginTypeId = enLogin_Mode.DESKTOP
    '            If mstrWebFrmName.Trim.Length > 0 Then
    '                objMail._Form_Name = mstrWebFrmName
    '            End If
    '            objMail._LogEmployeeUnkid = iLoginEmployeeId
    '            objMail._OperationModeId = iLoginTypeId
    '            objMail._UserUnkid = iUserId
    '            objMail._SenderAddress = IIf(objEmp._Email = "", objEmp._Firstname & " " & objEmp._Surname, objEmp._Email)
    '            objMail._ModuleRefId = clsSendMail.enAT_VIEW_TYPE.ASSESSMENT_MGT
    '            'Sohail (30 Nov 2017) -- Start
    '            'SUMATRA Enhancement – SUMATRA – issue # 0001669: Additional feature for Exchange Server configuration setting in 70.1.
    '            'objMail.SendMail()
    '            objMail.SendMail(intCompanyUnkId)
    '            'Sohail (30 Nov 2017) -- End

    '            'S.SANDEEP [29-NOV-2017] -- START
    '            'ISSUE/ENHANCEMENT : REF-ID # (38,41)
    '            If isUnlock = True AndAlso strCsvUserIds.Trim.Length > 0 Then
    '                Dim objUsr As New clsUserAddEdit
    '                Dim dsLst As New DataSet
    '                dsLst = objUsr.GetList("List", "hrmsConfiguration..cfuser_master.userunkid IN (" & strCsvUserIds & ")")
    '                objUsr = Nothing
    '                If dsLst.Tables("List").Rows.Count > 0 Then
    '                    For Each row As DataRow In dsLst.Tables("List").Rows
    '                        strMessage = ""
    '                        If row.Item("email").ToString.Trim.Length > 0 Then
    '                            Dim strName As String = row.Item("firstname") & " " & row.Item("lastname")
    '                            strMessage = "<HTML> <BODY>" & vbCrLf
    '                            strMessage &= Language.getMessage(mstrModuleName, 5, "Dear") & " <B>" & IIf(strName.Trim.Length > 0, strName, row.Item("username")) & "</B>, <BR><BR>" & vbCrLf
    '                            'S.SANDEEP [10-Mar-2018] -- START
    '                            'strMessage &= "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;" & Language.getMessage(mstrModuleName, 16, "This is to inform you that your Balanced Score Card Planning has been Unlocked by ") & "<b>" & sAssessorName & "</b>" & Language.getMessage(mstrModuleName, 12, " and it is now open for editing.") & "<br>" & vbCrLf
    '                            'strMessage &= "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;" & Language.getMessage(mstrModuleName, 300, "For employee ") & "<b>" & objEmp._Firstname & " " & objEmp._Surname & "</b>" & vbCrLf
    '                            strMessage &= "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;" & Language.getMessage(mstrModuleName, 30, "This is to inform you that Balanced Score Card Planning for employee") & "&nbsp;<b>" & objEmp._Firstname & " " & objEmp._Surname & "</b>&nbsp;" & _
    '                                          Language.getMessage(mstrModuleName, 31, "has been Unlocked by") & "<b>" & sAssessorName & "</b>" & vbCrLf & Language.getMessage(mstrModuleName, 12, " and it is now open for editing.") & "<br>" & vbCrLf
    '                            'S.SANDEEP [10-Mar-2018] -- END
    '                            strMessage &= "<BR></BR><BR></BR><center>""POWERED BY ARUTI HR & PAYROLL MANAGEMENT SOFTWARE""</center>"
    '                            strMessage &= "</BODY></HTML>"
    '                            If strMessage.ToString.Trim.Length > 0 Then
    '                                Dim objSendMail As New clsSendMail
    '                                objSendMail._ToEmail = row.Item("email").ToString.Trim
    '                                objSendMail._Subject = Language.getMessage(mstrModuleName, 15, "Notification for Unlocked of BSC Planning")
    '                                objSendMail._Message = strMessage.ToString
    '                                objSendMail._SenderAddress = row.Item("email")
    '                                objMail._ModuleRefId = clsSendMail.enAT_VIEW_TYPE.ASSESSMENT_MGT
    '                                Try
    '                                    objSendMail.SendMail(intCompanyUnkId)
    '                                Catch ex As Exception
    '                                End Try
    '                                objSendMail = Nothing
    '                            End If
    '                        End If
    '                    Next
    '                End If
    '            End If
    '            'S.SANDEEP [29-NOV-2017] -- END

    '        End If


    '    Catch ex As Exception
    '        DisplayError.Show("-1", ex.Message, "Send_Notification_Employee", mstrModuleName)
    '    End Try
    'End Sub

    Public Sub Send_Notification_Assessor(ByVal iEmployeeId As Integer, _
                                          ByVal iPeriodId As Integer, _
                                          ByVal iYearId As Integer, _
                                          ByVal sYearName As String, _
                                          ByVal strDatabaseName As String, _
                                          Optional ByVal iCompanyId As Integer = 0, _
                                          Optional ByVal sArutiSSURL As String = "", _
                                          Optional ByVal iLoginTypeId As Integer = 0, _
                                          Optional ByVal iLoginEmployeeId As Integer = 0, _
                                          Optional ByVal iUserId As Integer = 0)
        Dim dsList As DataSet = Nothing
        Dim strLink As String = String.Empty
        Dim StrQ As String = String.Empty
        objDataOperation = New clsDataOperation

        Try
            If iCompanyId <= 0 Then iCompanyId = Company._Object._Companyunkid
            If sArutiSSURL = "" Then sArutiSSURL = ConfigParameter._Object._ArutiSelfServiceURL
            Dim objNet As New clsNetConnectivity : If objNet._Conected = False Then Exit Sub

            dsList = (New clsAssessor).GetEmailNotificationList(iEmployeeId, False, objDataOperation, "List")

            Dim objEmp As New clsEmployee_Master
            Dim objMail As New clsSendMail
            Dim objPeriod As New clscommom_period_Tran

            objPeriod._Periodunkid(strDatabaseName) = iPeriodId
            objEmp._Employeeunkid(objPeriod._End_Date) = iEmployeeId

            Dim dsYear As New DataSet
            Dim objFY As New clsCompany_Master
            dsYear = objFY.GetFinancialYearList(iCompanyId, , "List", objPeriod._Yearunkid)
            If dsYear.Tables("List").Rows.Count > 0 Then
                sYearName = dsYear.Tables("List").Rows(0).Item("financialyear_name")
            End If
            objFY = Nothing
                'Gajanan [26-Feb-2019] -- Start
                'Enhancement - Email Language Changes For NMB.
            Dim info1 As TextInfo = CultureInfo.InvariantCulture.TextInfo
                'Gajanan [26-Feb-2019] -- End
            For Each dtRow As DataRow In dsList.Tables("List").Rows
                If dtRow.Item("Email") = "" Then Continue For
                Dim strMessage As String = ""
                objMail._Subject = Language.getMessage(mstrModuleName, 4, "Notification for Approval/Rejection BSC Planning.")
                strMessage = "<HTML> <BODY>"

                'Gajanan [26-Feb-2019] -- Start
                'Enhancement - Email Language Changes For NMB.
                strMessage &= Language.getMessage(mstrModuleName, 5, "Dear") & " " & info1.ToTitleCase(dtRow.Item("Ename").ToString().ToLower()) & ", <BR><BR>"
                'strMessage &= Language.getMessage(mstrModuleName, 5, "Dear") & " " & CStr(dtRow.Item("Ename")).TitleCase() & ", <BR><BR>"

                'strMessage &= "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;" & Language.getMessage(mstrModuleName, 6, "This is to inform you that i have completed my Balanced Score Card Planning for ") & _


                'Pinkal (22-Mar-2019) -- Start
                'Enhancement - Working on PA Changes FOR NMB.
                'strMessage &= "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;" & Language.getMessage(mstrModuleName, 33, "Please note that, I have completed my Balanced Score Card for the ")
                strMessage &= Language.getMessage(mstrModuleName, 33, "Please note that, I have completed my Balanced Score Card for the ") & " "
                'Pinkal (22-Mar-2019) -- End


                'objPeriod._Period_Name & ", " & sYearName & Language.getMessage(mstrModuleName, 7, ". Please click the link below to review it and thereafter approve/reject it.")

                'Pinkal (22-Mar-2019) -- Start
                'Enhancement - Working on PA Changes FOR NMB.
                'strMessage &= "<b>" & objPeriod._Period_Name & ", " & sYearName & "</b>" & Language.getMessage(mstrModuleName, 34, ". Kindly click the link below to review it.")

                'S.SANDEEP |30-MAR-2019| -- START
                'strMessage &= Language.getMessage(mstrModuleName, 50, "period") & " (<b>" & objPeriod._Period_Name & " </b>) " & Language.getMessage(mstrModuleName, 34, ". Kindly click the link below to review it.")
                'S.SANDEEP |2-APRIL-2019| -- START
                'strMessage &= Language.getMessage(mstrModuleName, 50, "period") & " <b>(" & objPeriod._Period_Name & ")</b>." & Language.getMessage(mstrModuleName, 51, "Kindly click the link below to review it.")
                strMessage &= Language.getMessage(mstrModuleName, 50, "period") & " <b>(" & objPeriod._Period_Name & ")</b>.&nbsp;" & Language.getMessage(mstrModuleName, 51, "Kindly click the link below to review it.")
                'S.SANDEEP |2-APRIL-2019| -- START
                'S.SANDEEP |30-MAR-2019| -- END
                'Pinkal (22-Mar-2019) -- End




                'strMessage &= "<BR><BR>"
                'strMessage &= "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;" & Language.getMessage(mstrModuleName, 32, "Regards,")
                'strMessage &= "<BR>"
                'strMessage &= "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;" & objEmp._Firstname & " " & objEmp._Surname
                'Gajanan [26-Feb-2019] -- End
                strMessage &= "<BR>"
                strLink = sArutiSSURL & "/Assessment New/Common/wPgPerformance_Planning.aspx?" & System.Web.HttpUtility.UrlEncode(clsCrypto.Encrypt(iCompanyId.ToString & "|" & dtRow.Item("userunkid").ToString & "|" & iEmployeeId.ToString & "|" & dtRow.Item("assessormasterunkid").ToString & "|" & iPeriodId.ToString))


                'Pinkal (22-Mar-2019) -- Start
                'Enhancement - Working on PA Changes FOR NMB.
                'strMessage &= "<BR></BR>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<a href='" & strLink & "'>" & strLink & "</a>"
                strMessage &= "<BR></BR><a href='" & strLink & "'>" & strLink & "</a>"
                'Pinkal (22-Mar-2019) -- End



                'Gajanan [26-Feb-2019] -- Start
                'Enhancement - Email Language Changes For NMB.
                strMessage &= "<BR><BR>"
                strMessage &= Language.getMessage(mstrModuleName, 35, "Regards,")
                strMessage &= "<BR>"
                strMessage &= info1.ToTitleCase((objEmp._Firstname & " " & objEmp._Surname).ToLower()).ToString()
                'Gajanan [26-Feb-2019] -- End

                strMessage &= "<BR></BR><BR></BR><center>""POWERED BY ARUTI HR & PAYROLL MANAGEMENT SOFTWARE""</center>"
                strMessage &= "</BODY></HTML>"
                objMail._Message = strMessage
                objMail._ToEmail = dtRow.Item("Email")
                If iLoginTypeId <= 0 Then iLoginTypeId = enLogin_Mode.DESKTOP
                If mstrWebFrmName.Trim.Length > 0 Then
                    objMail._Form_Name = mstrWebFrmName
                End If
                objMail._LogEmployeeUnkid = iLoginEmployeeId
                objMail._OperationModeId = iLoginTypeId
                objMail._UserUnkid = IIf(iUserId <= 0, User._Object._Userunkid, iUserId)
                objMail._SenderAddress = IIf(objEmp._Email = "", objEmp._Firstname & " " & objEmp._Surname, objEmp._Email)
                objMail._ModuleRefId = clsSendMail.enAT_VIEW_TYPE.ASSESSMENT_MGT
                With objMail
                    ._FormName = mstrFormName
                    ._ClientIP = mstrClientIP
                    ._HostName = mstrHostName
                    ._FromWeb = mblnIsWeb
                    ._AuditUserId = mintAuditUserId
                    ._CompanyUnkid = mintCompanyUnkid
                    ._AuditDate = mdtAuditDate
                End With
                objMail.SendMail(iCompanyId)
                'Sohail (30 Nov 2017) -- End
            Next


            'Pinkal (22-Mar-2019) -- Start
            'Enhancement - Working on Leave & Claim Enhancement Phase 2 for NMB.
            Dim objCompany As New clsCompany_Master
            objCompany._Companyunkid = iCompanyId

            Dim strEmpMessage As String = ""
            objMail._Subject = Language.getMessage(mstrModuleName, 46, "BSC notification")
            strEmpMessage = "<HTML> <BODY>"
            strEmpMessage &= Language.getMessage(mstrModuleName, 5, "Dear") & " " & info1.ToTitleCase(objEmp._Firstname.ToLower()) & " " & info1.ToTitleCase(objEmp._Surname.ToLower()) & ", <BR><BR>"

            'Pinkal (22-Mar-2019) -- Start
            'Enhancement - Working on PA Changes FOR NMB.
            'strEmpMessage &= "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;" & Language.getMessage(mstrModuleName, 47, "Please note that, your Balance Score card for") & " " & objPeriod._Period_Name & " " & Language.getMessage(mstrModuleName, 48, "has been submitted to your line manager for approval.")

            'S.SANDEEP |25-MAR-2019| -- START
            'strEmpMessage &= Language.getMessage(mstrModuleName, 47, "Please note that, your Balance Score card for") & " " & Language.getMessage(mstrMessage, 50, "period") & " <b>(" & objPeriod._Period_Name & ") </b> " & Language.getMessage(mstrModuleName, 48, "has been submitted to your line manager for approval.")
            'strEmpMessage &= "<BR>" & Language.getMessage(mstrModuleName, 49, "Regards.") & "</BR>"

            'S.SANDEEP |2-APRIL-2019| -- START

            'strEmpMessage &= Language.getMessage(mstrModuleName, 147, "Please note that, your Balance Score card for the") & " " & Language.getMessage(mstrMessage, 50, "period") & " <b>(" & objPeriod._Period_Name & ")</b>." & Language.getMessage(mstrModuleName, 48, "has been submitted to your line manager for approval.")
            strEmpMessage &= Language.getMessage(mstrModuleName, 147, "Please note that, your Balance Score card for the") & " " & Language.getMessage(mstrModuleName, 50, "period") & " <b>(" & objPeriod._Period_Name & ")</b>&nbsp;" & Language.getMessage(mstrModuleName, 48, "has been submitted to your line manager for approval.")
            'S.SANDEEP |2-APRIL-2019| -- END
            strEmpMessage &= "<BR><BR>" & Language.getMessage(mstrModuleName, 49, "Regards.") & "</BR>"
            'S.SANDEEP |25-MAR-2019| -- END

            
            'Pinkal (22-Mar-2019) -- End

            strEmpMessage &= "<BR></BR><BR></BR><center>""POWERED BY ARUTI HR & PAYROLL MANAGEMENT SOFTWARE""</center>"
            strEmpMessage &= "</BODY></HTML>"
            objMail._Message = strEmpMessage
            objMail._ToEmail = objEmp._Email
            If iLoginTypeId <= 0 Then iLoginTypeId = enLogin_Mode.DESKTOP
            If mstrWebFrmName.Trim.Length > 0 Then
                objMail._Form_Name = mstrWebFrmName
            End If
            objMail._LogEmployeeUnkid = iLoginEmployeeId
            objMail._OperationModeId = iLoginTypeId
            objMail._UserUnkid = IIf(iUserId <= 0, User._Object._Userunkid, iUserId)
            objMail._SenderAddress = objCompany._Email
            objMail._ModuleRefId = clsSendMail.enAT_VIEW_TYPE.ASSESSMENT_MGT
            objMail.SendMail(iCompanyId)
            objCompany = Nothing
            'Pinkal (22-Mar-2019) -- End


        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "Send_Notification_Assessor", mstrModuleName)
        Finally
        End Try
    End Sub

    Public Sub Send_Notification_Employee(ByVal iEmployeeId As Integer, _
                                          ByVal sAssessorName As String, _
                                          ByVal sComments As String, _
                                          ByVal isFinalOpr As Boolean, _
                                          ByVal isUnlock As Boolean, _
                                          ByVal intCompanyUnkId As Integer, _
                                          Optional ByVal iLoginTypeId As Integer = 0, _
                                          Optional ByVal iLoginEmployeeId As Integer = 0, _
                                          Optional ByVal iUserId As Integer = 0, _
                                          Optional ByVal strCsvUserIds As String = "", _
                                          Optional ByVal blnVoidProgress As Boolean = False) 'S.SANDEEP |09-JUL-2019| -- START {blnVoidProgress} -- END
        Try
            Dim objEmp As New clsEmployee_Master
            Dim objMail As New clsSendMail

            objEmp._Employeeunkid(Now.Date) = iEmployeeId


            'Gajanan [26-Feb-2019] -- Start
            'Enhancement - Email Language Changes For NMB.
            Dim info1 As TextInfo = CultureInfo.InvariantCulture.TextInfo
            'Gajanan [26-Feb-2019] -- End

            If objEmp._Email.Trim.Length > 0 Then
                Dim strMessage As String = ""

                strMessage = "<HTML> <BODY>"

                'Gajanan [26-Feb-2019] -- Start
                'Enhancement - Email Language Changes For NMB.
                strMessage &= Language.getMessage(mstrModuleName, 5, "Dear") & " " & info1.ToTitleCase(CStr(objEmp._Firstname & " " & objEmp._Surname).ToLower()) & ", <BR><BR>"
                'strMessage &= Language.getMessage(mstrModuleName, 5, "Dear") & " " & (objEmp._Firstname & " " & objEmp._Surname).TitleCase() & ", <BR><BR>"
                'Gajanan [26-Feb-2019] -- End

                If isFinalOpr = True Then

                    'Gajanan [26-Feb-2019] -- Start
                    'Enhancement - Email Language Changes For NMB.
                    'objMail._Subject = Language.getMessage(mstrModuleName, 8, "Notification for Approval of BSC Planning")
                    objMail._Subject = Language.getMessage(mstrModuleName, 38, "Notification for Approval of Balanced Scorecard")


                    'strMessage &= "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;" & Language.getMessage(mstrModuleName, 10, "This is to inform you that your Balanced Score Card Planning has been approved and final saved by ") & info1.ToTitleCase(sAssessorName) & "."


                    'Pinkal (22-Mar-2019) -- Start
                    'Enhancement - Working on PA Changes FOR NMB.
                    'strMessage &= "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;" & Language.getMessage(mstrModuleName, 39, "Please note that, your Balanced Scorecard has been approved") & "."
                    strMessage &= Language.getMessage(mstrModuleName, 39, "Please note that, your Balanced Scorecard has been approved") & "."
                    'Pinkal (22-Mar-2019) -- End


                    'Gajanan [26-Feb-2019] -- End

                Else
                    If isUnlock = True Then

                        'Gajanan [26-Feb-2019] -- Start
                        'Enhancement - Email Language Changes For NMB.
                        'objMail._Subject = Language.getMessage(mstrModuleName, 15, "Notification for Unlocked of BSC Planning")
                        objMail._Subject = Language.getMessage(mstrModuleName, 40, "Notification for Reopened of Balanced Scorecard")

                        'strMessage &= "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;" & Language.getMessage(mstrModuleName, 16, "This is to inform you that your Balanced Score Card Planning has been Unlocked by ") & info1.ToTitleCase(sAssessorName) & Language.getMessage(mstrModuleName, 12, " and it is now open for editing.") & "<br>"


                        'S.SANDEEP |09-JUL-2019| -- START
                        'ISSUE/ENHANCEMENT : PA CHANGES
                        ''Pinkal (22-Mar-2019) -- Start
                        ''Enhancement - Working on PA Changes FOR NMB.
                        ''strMessage &= "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;" & Language.getMessage(mstrModuleName, 41, "Please note that, your Balanced Scorecard has been reopened for editing.") & "<br>"
                        'strMessage &= Language.getMessage(mstrModuleName, 41, "Please note that, your Balanced Scorecard has been reopened for editing.") & "<br>"
                        ''Pinkal (22-Mar-2019) -- End
                        If blnVoidProgress Then
                            strMessage &= Language.getMessage(mstrModuleName, 751, "Please note that, your Balanced Scorecard has been reopened for editing along with Progress Update(s).") & "<br>"
                        Else
                            strMessage &= Language.getMessage(mstrModuleName, 44, "Please note that, your Balanced Scorecard has been reopened for editing.") & "<br>"
                        End If
                        'S.SANDEEP |09-JUL-2019| -- END

                        


                        'Gajanan [26-Feb-2019] -- End
                    Else
                        objMail._Subject = Language.getMessage(mstrModuleName, 9, "Notification for Rejection of BSC Planning")
                        'Gajanan [26-Feb-2019] -- Start
                        'Enhancement - Email Language Changes For NMB.
                        'strMessage &= "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;" & Language.getMessage(mstrModuleName, 11, "This is to inform you that your Balanced Score Card Planning has been rejected by ") & info1.ToTitleCase(sAssessorName) & Language.getMessage(mstrModuleName, 12, " and it is now open for editing.") & "<br>"


                        'Pinkal (22-Mar-2019) -- Start
                        'Enhancement - Working on PA Changes FOR NMB.
                        'strMessage &= "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;" & Language.getMessage(mstrModuleName, 36, "Please note that, your Balanced Score Card has been rejected ") & Language.getMessage(mstrModuleName, 12, " and it is now open for editing.") & "<br>"
                        strMessage &= Language.getMessage(mstrModuleName, 36, "Please note that, your Balanced Score Card has been rejected ") & Language.getMessage(mstrModuleName, 12, " and it is now open for editing.") & "<br>"
                        'Pinkal (22-Mar-2019) -- End


                        'Gajanan [26-Feb-2019] -- End
                    End If
                    'Gajanan [26-Feb-2019] -- Start
                    'Enhancement - Email Language Changes For NMB.
                    'strMessage &= "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;" & Language.getMessage(mstrModuleName, 13, "Your advised to make the changes recommended by your assessor as per remarks below.") & "<br><br>" & _


                    'Pinkal (22-Mar-2019) -- Start
                    'Enhancement - Working on PA Changes FOR NMB.
                    'If isUnlock = True Then
                    '    strMessage &= "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;" & Language.getMessage(mstrModuleName, 42, "You are advised to make changes as recommended.") & "<br><br>"
                    'Else
                    '    strMessage &= "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;" & Language.getMessage(mstrModuleName, 37, "You are therefore advised to make changes as recommended.") & "<br><br>"
                    'End If

                    If isUnlock = True Then
                        strMessage &= Language.getMessage(mstrModuleName, 42, "You are advised to make changes as recommended.") & "<br><br>"
                    Else
                        strMessage &= Language.getMessage(mstrModuleName, 37, "You are therefore advised to make changes as recommended.") & "<br><br>"
                    End If
                    'Pinkal (22-Mar-2019) -- End

                    '"&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;" & Language.getMessage(mstrModuleName, 14, "Comments :") & sComments

                    'Pinkal (22-Mar-2019) -- Start
                    'Enhancement - Working on PA Changes FOR NMB.
                    'strMessage &= "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;" & Language.getMessage(mstrModuleName, 14, "Comments :") & sComments
                    strMessage &= Language.getMessage(mstrModuleName, 14, "Comments :") & sComments
                    'Pinkal (22-Mar-2019) -- End


                    'Gajanan [26-Feb-2019] -- End

                End If

                'Gajanan [26-Feb-2019] -- Start
                'Enhancement - Email Language Changes For NMB.
                strMessage &= "<BR><BR>"
                strMessage &= Language.getMessage(mstrModuleName, 35, "Regards,")
                strMessage &= "<BR>"

                'S.SANDEEP |25-MAR-2019| -- START
                'strMessage &= sAssessorName
                strMessage &= info1.ToTitleCase(sAssessorName.ToLower())
                'S.SANDEEP |25-MAR-2019| -- END

                'Gajanan [26-Feb-2019] -- End

                strMessage &= "<BR></BR><BR></BR><center>""POWERED BY ARUTI HR & PAYROLL MANAGEMENT SOFTWARE""</center>"
                strMessage &= "</BODY></HTML>"
                objMail._ToEmail = objEmp._Email
                objMail._Message = strMessage
                If iLoginTypeId <= 0 Then iLoginTypeId = enLogin_Mode.DESKTOP
                If mstrWebFrmName.Trim.Length > 0 Then
                    objMail._Form_Name = mstrWebFrmName
                End If
                objMail._LoginEmployeeunkid = mintLoginemployeeunkid
                objMail._OperationModeId = iLoginTypeId
                objMail._UserUnkid = iUserId
                objMail._SenderAddress = IIf(objEmp._Email = "", objEmp._Firstname & " " & objEmp._Surname, objEmp._Email)
                objMail._ModuleRefId = clsSendMail.enAT_VIEW_TYPE.ASSESSMENT_MGT
                With objMail
                    ._FormName = mstrFormName
                    ._ClientIP = mstrClientIP
                    ._HostName = mstrHostName
                    ._FromWeb = mblnIsWeb
                    ._AuditUserId = mintAuditUserId
                    ._CompanyUnkid = mintCompanyUnkid
                    ._AuditDate = mdtAuditDate
                End With
                objMail.SendMail(intCompanyUnkId)
                'Sohail (30 Nov 2017) -- End

                'S.SANDEEP [29-NOV-2017] -- START
                'ISSUE/ENHANCEMENT : REF-ID # (38,41)
                If isUnlock = True AndAlso strCsvUserIds.Trim.Length > 0 Then
                    Dim objUsr As New clsUserAddEdit
                    Dim dsLst As New DataSet
                    dsLst = objUsr.GetList("List", "hrmsConfiguration..cfuser_master.userunkid IN (" & strCsvUserIds & ")")
                    objUsr = Nothing
                    If dsLst.Tables("List").Rows.Count > 0 Then
                        For Each row As DataRow In dsLst.Tables("List").Rows
                            strMessage = ""
                            If row.Item("email").ToString.Trim.Length > 0 Then
                                Dim strName As String = row.Item("firstname") & " " & row.Item("lastname")
                                strMessage = "<HTML> <BODY>" & vbCrLf

                                'Gajanan [26-Feb-2019] -- Start
                                'Enhancement - Email Language Changes For NMB.
                                strMessage &= Language.getMessage(mstrModuleName, 5, "Dear") & " " & info1.ToTitleCase(IIf(strName.Trim.Length > 0, strName, row.Item("username")).ToString().ToLower()) & ", <BR><BR>" & vbCrLf
                                'strMessage &= Language.getMessage(mstrModuleName, 5, "Dear") & " " & IIf(strName.Trim.Length > 0, strName, row.Item("username")).ToString().TitleCase() & ", <BR><BR>" & vbCrLf


                                'strMessage &= "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;" & Language.getMessage(mstrModuleName, 30, "This is to inform you that Balanced Score Card Planning for employee") & "&nbsp; " & info1.ToTitleCase(objEmp._Firstname & " " & objEmp._Surname) & " &nbsp;" & _


                                'S.SANDEEP |09-JUL-2019| -- START
                                'ISSUE/ENHANCEMENT : PA CHANGES
                                ''Pinkal (22-Mar-2019) -- Start
                                ''Enhancement - Working on PA Changes FOR NMB.
                                ''strMessage &= "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;" & Language.getMessage(mstrModuleName, 44, "Please note that, your Balanced Scorecard has been reopened for editing.") & "<br>"
                                'strMessage &= Language.getMessage(mstrModuleName, 44, "Please note that, your Balanced Scorecard has been reopened for editing.") & "<br>"
                                ''Pinkal (22-Mar-2019) -- End
                                If blnVoidProgress Then
                                    strMessage &= Language.getMessage(mstrModuleName, 751, "Please note that, your Balanced Scorecard has been reopened for editing along with Progress Update(s).") & "<br>"
                                Else
                                strMessage &= Language.getMessage(mstrModuleName, 44, "Please note that, your Balanced Scorecard has been reopened for editing.") & "<br>"
                                End If
                                'S.SANDEEP |09-JUL-2019| -- END


                                'Language.getMessage(mstrModuleName, 31, "has been Unlocked by") & " " & info1.ToTitleCase(sAssessorName) & " " & vbCrLf & Language.getMessage(mstrModuleName, 12, " and it is now open for editing.") & "<br>" & vbCrLf


                                'Pinkal (22-Mar-2019) -- Start
                                'Enhancement - Working on PA Changes FOR NMB.
                                'strMessage &= "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;" & Language.getMessage(mstrModuleName, 45, "You are advised to make changes as recommended.") & "<br><br>"
                                strMessage &= Language.getMessage(mstrModuleName, 45, "You are advised to make changes as recommended.") & "<br><br>"
                                'Pinkal (22-Mar-2019) -- End



                                'Gajanan [26-Feb-2019] -- Start
                                'Enhancement - Email Language Changes For NMB.

                                'Pinkal (22-Mar-2019) -- Start
                                'Enhancement - Working on PA Changes FOR NMB.
                                'strMessage &= "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; <b>" & Language.getMessage(mstrModuleName, 14, "Comments :") & "<b>" & sComments
                                strMessage &= "<b>" & Language.getMessage(mstrModuleName, 14, "Comments :") & "<b>" & sComments
                                'Pinkal (22-Mar-2019) -- End


                                strMessage &= "<BR><BR>"
                                strMessage &= Language.getMessage(mstrModuleName, 43, "Regards,")
                                strMessage &= "<BR>"

                                'S.SANDEEP |25-MAR-2019| -- START
                                'strMessage &= sAssessorName
                                strMessage &= info1.ToTitleCase(sAssessorName.ToLower())
                                'S.SANDEEP |25-MAR-2019| -- END

                                'Gajanan [26-Feb-2019] -- End

                                strMessage &= "<BR></BR><BR></BR><center>""POWERED BY ARUTI HR & PAYROLL MANAGEMENT SOFTWARE""</center>"
                                strMessage &= "</BODY></HTML>"
                                If strMessage.ToString.Trim.Length > 0 Then
                                    Dim objSendMail As New clsSendMail
                                    objSendMail._ToEmail = row.Item("email").ToString.Trim

                                    'Gajanan [26-Feb-2019] -- Start
                                    'Enhancement - Email Language Changes For NMB.
                                    'objSendMail._Subject = Language.getMessage(mstrModuleName, 15, "Notification for Unlocked of BSC Planning")
                                    objSendMail._Subject = Language.getMessage(mstrModuleName, 40, "Notification for Reopened of Balanced Scorecard")
                                    'Gajanan [26-Feb-2019] -- End

                                    objSendMail._Message = strMessage.ToString                                    
                                    objSendMail._SenderAddress = row.Item("email")
                                    With objSendMail
                                        ._FormName = mstrFormName
                                        ._LoginEmployeeunkid = mintLoginemployeeunkid
                                        ._ClientIP = mstrClientIP
                                        ._HostName = mstrHostName
                                        ._FromWeb = mblnIsWeb
                                        ._AuditUserId = mintAuditUserId
                                        ._CompanyUnkid = mintCompanyUnkid
                                        ._AuditDate = mdtAuditDate
                                    End With
                                    objMail._ModuleRefId = clsSendMail.enAT_VIEW_TYPE.ASSESSMENT_MGT
                                    Try
                                        objSendMail.SendMail(intCompanyUnkId)
                                    Catch ex As Exception
                                    End Try
                                    objSendMail = Nothing
                                End If
                            End If
                        Next
                    End If
                End If
                'S.SANDEEP [29-NOV-2017] -- END

            End If


        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "Send_Notification_Employee", mstrModuleName)
        End Try
    End Sub
    'S.SANDEEP |18-FEB-2019| -- END


    'S.SANDEEP [04 JUN 2015] -- START
    'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
    Public Function GetEmployee_ForPeriodicReview(ByVal xDatabaseName As String, _
                                                  ByVal xUserUnkid As Integer, _
                                                  ByVal xYearUnkid As Integer, _
                                                  ByVal xCompanyUnkid As Integer, _
                                                  ByVal xUserModeSetting As String, _
                                                  ByVal xOnlyApproved As Boolean, _
                                                  ByVal xIncludeIn_ActiveEmployee As Boolean, _
                                                  ByVal xPeriodId As Integer, _
                                                  Optional ByVal xAdvanceFilter As String = "") As DataTable
        Dim StrQ As String = String.Empty
        Dim exForce As Exception
        Dim dtTable As DataTable = Nothing
        Dim dsList As New DataSet
        Try
            If xPeriodId > 0 Then
                Dim objPrd As New clscommom_period_Tran
                'Sohail (21 Aug 2015) -- Start
                'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
                'objPrd._Periodunkid = xPeriodId
                objPrd._Periodunkid(xDatabaseName) = xPeriodId
                'Sohail (21 Aug 2015) -- End
                Dim xDateJoinQry, xDateFilterQry, xUACQry, xUACFiltrQry, xAdvanceJoinQry As String
                xDateJoinQry = "" : xDateFilterQry = "" : xUACQry = "" : xUACFiltrQry = "" : xAdvanceJoinQry = ""
                Call GetDatesFilterString(xDateJoinQry, xDateFilterQry, objPrd._Start_Date, objPrd._End_Date, , , xDatabaseName)
                Call NewAccessLevelFilterString(xUACQry, xUACFiltrQry, objPrd._End_Date, xOnlyApproved, xDatabaseName, xUserUnkid, xCompanyUnkid, xYearUnkid, xUserModeSetting)
                Call GetAdvanceFilterQry(xAdvanceJoinQry, objPrd._End_Date, xDatabaseName)

                Using objData As New clsDataOperation
                    StrQ = "SELECT DISTINCT " & _
                           "   CAST(0 AS BIT) AS ischeck " & _
                           "  ,employeecode " & _
                           "  ,firstname+' '+surname AS employeename " & _
                           "  ,email " & _
                           "  ,hremployee_master.employeeunkid " & _
                           "FROM hremployee_master WITH (NOLOCK) " & _
                           "  JOIN hrassess_empfield1_master WITH (NOLOCK) ON hrassess_empfield1_master.employeeunkid = hremployee_master.employeeunkid "

                    If xDateJoinQry.Trim.Length > 0 Then
                        StrQ &= xDateJoinQry
                    End If

                    If xUACQry.Trim.Length > 0 Then
                        StrQ &= xUACQry
                    End If

                    If xAdvanceJoinQry.Trim.Length > 0 Then
                        StrQ &= xAdvanceJoinQry
                    End If

                    StrQ &= " WHERE isapproved = 1 AND isfinal = 1 AND periodunkid = '" & xPeriodId & "' AND isvoid = 0 " & _
                            "    AND hremployee_master.employeeunkid NOT IN (SELECT selfemployeeunkid FROM hrevaluation_analysis_master WITH (NOLOCK) WHERE isvoid = 0 AND periodunkid = '" & xPeriodId & "' UNION " & _
                            " SELECT assessedemployeeunkid FROM hrevaluation_analysis_master WITH (NOLOCK) WHERE isvoid = 0 AND periodunkid = '" & xPeriodId & "') "
                    'Shani(01-MAR-2016) -- [assessoremployeeunkid-->assessedemployeeunkid]

                    If xUACFiltrQry.Trim.Length > 0 Then
                        StrQ &= " AND " & xUACFiltrQry & " "
                    End If

                    If xIncludeIn_ActiveEmployee = False Then
                        If xDateFilterQry.Trim.Length > 0 Then
                            StrQ &= xDateFilterQry & " "
                        End If
                    End If

                    If xAdvanceFilter.Trim.Length > 0 Then
                        StrQ &= "AND " & xAdvanceFilter
                    End If

                    dsList = objData.ExecQuery(StrQ, "List")

                    If objData.ErrorMessage <> "" Then
                        exForce = New Exception(objData.ErrorNumber & " : " & objData.ErrorMessage)
                        Throw exForce
                    End If

                    dtTable = dsList.Tables(0).Copy

                End Using
            End If
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: GetEmployee_ForPeriodicReview; Module Name: " & mstrModuleName)
        Finally
        End Try
        Return dtTable
    End Function

    'Public Function GetEmployee_ForPeriodicReview(ByVal xPeriodId As Integer, _
    '                                              Optional ByVal xAdvanceFilter As String = "", _
    '                                              Optional ByVal xAccessLevelIds As String = "", _
    '                                              Optional ByVal xIncludeInActiveEmployee As String = "") As DataTable
    '    Dim StrQ As String = String.Empty
    '    Dim exForce As Exception
    '    Dim dtTable As DataTable = Nothing
    '    Dim dsList As New DataSet
    '    Try
    '        If xPeriodId > 0 Then
    '            Dim objPrd As New clscommom_period_Tran
    '            objPrd._Periodunkid = xPeriodId
    '            Using objData As New clsDataOperation
    '                StrQ = "SELECT DISTINCT " & _
    '                       "     CAST(0 AS BIT) AS ischeck " & _
    '                       "    ,employeecode " & _
    '                       "    ,firstname+' '+surname AS employeename " & _
    '                       "    ,email " & _
    '                       "    ,hremployee_master.employeeunkid " & _
    '                       "FROM hremployee_master " & _
    '                       "    JOIN hrassess_empfield1_master ON hrassess_empfield1_master.employeeunkid = hremployee_master.employeeunkid " & _
    '                       "WHERE isapproved = 1 AND isfinal = 1 AND periodunkid = '" & xPeriodId & "' AND isvoid = 0 " & _
    '                       "    AND hremployee_master.employeeunkid NOT IN (SELECT selfemployeeunkid FROM hrevaluation_analysis_master WHERE isvoid = 0 AND periodunkid = '" & xPeriodId & "' UNION " & _
    '                       "SELECT assessoremployeeunkid FROM hrevaluation_analysis_master WHERE isvoid = 0 AND periodunkid = '" & xPeriodId & "') "

    '                If xAdvanceFilter.Trim.Length > 0 Then
    '                    StrQ &= " AND " & xAdvanceFilter
    '                End If

    '                If xIncludeInActiveEmployee.Trim.Length <= 0 Then
    '                    xIncludeInActiveEmployee = ConfigParameter._Object._IsIncludeInactiveEmp.ToString
    '                End If
    '                If CBool(xIncludeInActiveEmployee) = False Then
    '                    StrQ &= " AND CONVERT(CHAR(8),hremployee_master.appointeddate,112) <= @enddate " & _
    '                            " AND ISNULL(CONVERT(CHAR(8),hremployee_master.termination_from_date,112),@startdate) >= @startdate " & _
    '                            " AND ISNULL(CONVERT(CHAR(8),hremployee_master.termination_to_date,112),@startdate) >= @startdate " & _
    '                            " AND ISNULL(CONVERT(CHAR(8),hremployee_master.empl_enddate,112), @startdate) >= @startdate "

    '                    objData.AddParameter("@startdate", SqlDbType.NVarChar, eZeeDataType.DATETIME_SIZE, eZeeDate.convertDate(objPrd._Start_Date))
    '                    objData.AddParameter("@enddate", SqlDbType.NVarChar, eZeeDataType.DATETIME_SIZE, eZeeDate.convertDate(objPrd._End_Date))
    '                End If


    '                If xAccessLevelIds.Trim.Length <= 0 Then
    '                    'Sohail (08 May 2015) -- Start
    '                    'Enhancement - Get User Access Filter string from new employee transfer table and employee categorization table.
    '                    'xAccessLevelIds = UserAccessLevel._AccessLevelFilterString
    '                    'xAccessLevelIds &= NewAccessLevelFilterString()
    '                    'Sohail (08 May 2015) -- End
    '                End If

    '                StrQ &= xAccessLevelIds

    '                dsList = objData.ExecQuery(StrQ, "List")

    '                If objData.ErrorMessage <> "" Then
    '                    exForce = New Exception(objData.ErrorNumber & " : " & objData.ErrorMessage)
    '                    Throw exForce
    '                End If

    '                dtTable = dsList.Tables(0).Copy

    '            End Using
    '        End If
    '    Catch ex As Exception
    '        DisplayError.Show("-1", ex.Message, "GetEmployee_ForPeriodicReview", mstrModuleName)
    '    Finally
    '        dsList.Dispose()
    '    End Try
    '    Return dtTable
    'End Function

    'S.SANDEEP [04 JUN 2015] -- END


    Private Function Periodic_Review(ByVal eAudit As enAuditType, ByVal objDataOperation As clsDataOperation, ByVal xDataTable As DataTable) As Boolean
        Try
            If xDataTable.Rows.Count <= 0 Then Return True

            Dim xLastStatusId As Integer = -1
            Dim objEStatusTran As New clsassess_empstatus_tran
            Dim xStatusTranId As Integer = -1
            xLastStatusId = objEStatusTran.Get_Last_StatusId(mintEmployeeunkid, mintPeriodunkid, xStatusTranId, objDataOperation)
            If xLastStatusId = enObjective_Status.PERIODIC_REVIEW Then
                Dim objPriodicReview As New clsassess_periodic_review
                If IsDBNull(xDataTable.Rows(0).Item("enddate")) = True Then
                    objPriodicReview._Enddate = Nothing
                Else
                    objPriodicReview._Enddate = xDataTable.Rows(0).Item("enddate")
                End If
                objPriodicReview._Field_Data = xDataTable.Rows(0).Item("field_data")
                objPriodicReview._Fieldtranunkid = xDataTable.Rows(0).Item("empfield1unkid")
                objPriodicReview._Fieldtypeid = enWeight_Types.WEIGHT_FIELD1
                objPriodicReview._Fieldunkid = xDataTable.Rows(0).Item("fieldunkid")
                objPriodicReview._Isfinal = xDataTable.Rows(0).Item("isfinal")
                objPriodicReview._Loginemployeeunkid = mintLoginemployeeunkid
                objPriodicReview._Owrfield1unkid = xDataTable.Rows(0).Item("owrfield1unkid")
                objPriodicReview._Pct_Complete = xDataTable.Rows(0).Item("pct_completed")
                objPriodicReview._Periodunkid = xDataTable.Rows(0).Item("periodunkid")
                objPriodicReview._Perspectiveunkid = xDataTable.Rows(0).Item("perspectiveunkid")
                objPriodicReview._Reviewdate = ConfigParameter._Object._CurrentDateAndTime
                objPriodicReview._Reviewtypeid = eAudit
                If IsDBNull(xDataTable.Rows(0).Item("startdate")) = True Then
                    objPriodicReview._Startdate = Nothing
                Else
                    objPriodicReview._Startdate = xDataTable.Rows(0).Item("startdate")
                End If
                objPriodicReview._Statusunkid = xDataTable.Rows(0).Item("statusunkid")
                objPriodicReview._Userunkid = xDataTable.Rows(0).Item("userunkid")
                objPriodicReview._FormName = mstrFormName
                objPriodicReview._Loginemployeeunkid = mintLoginemployeeunkid
                objPriodicReview._ClientIP = mstrClientIP
                objPriodicReview._HostName = mstrHostName
                objPriodicReview._FromWeb = mblnIsWeb
                objPriodicReview._AuditUserId = mintAuditUserId
                objPriodicReview._CompanyUnkid = mintCompanyUnkid
                objPriodicReview._AuditDate = mdtAuditDate
                objPriodicReview._Weight = xDataTable.Rows(0).Item("weight")
                objPriodicReview._Employeeunkid = xDataTable.Rows(0).Item("employeeunkid")
                objPriodicReview._Statustranunkid = xStatusTranId
                'S.SANDEEP [01-OCT-2018] -- START
                'ISSUE/ENHANCEMENT : {Ref#2585|ARUTI-}
                objPriodicReview._GoalTypeid = xDataTable.Rows(0).Item("goaltypeid")
                objPriodicReview._GoalValue = xDataTable.Rows(0).Item("goalvalue")
                'S.SANDEEP [01-OCT-2018] -- END
                If objPriodicReview.Insert(objDataOperation) = False Then
                    Return False
                End If
                objPriodicReview = Nothing
            End If
            objEStatusTran = Nothing
            Return True
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "Periodic_Review", mstrModuleName)
        Finally
        End Try
    End Function


    'Pinkal (07-Dec-2020) -- Start
    'Enhancement NMB - Cascading PM UAT Comments.
    'Public Function Transfer_Goals_Subordinate_Employee(ByVal xRow() As DataRow) As Boolean
    Public Function Transfer_Goals_Subordinate_Employee(ByVal xRow() As DataRow, ByVal xCompanyId As Integer _
                                                                                   , Optional ByVal iLoginTypeId As Integer = 0, Optional ByVal iLoginEmployeeId As Integer = 0 _
                                                                                   , Optional ByVal iUserId As Integer = 0 _
                                                                                   , Optional ByVal blnOnlySendMail As Boolean = False) As Boolean
        'Pinkal (07-Dec-2020) -- End
        Try
            If xRow.Length > 0 Then
                'S.SANDEEP |29-NOV-2021| -- START
                'ISSUE : SENDING SINGLE EMAIL
                Dim strOwrJoin As String = String.Join(",", xRow.AsEnumerable().Select(Function(x) x.Field(Of String)("OwnerIds")).ToArray())
                Dim iList As List(Of String) = strOwrJoin.Split(",").Distinct().ToList()
                'S.SANDEEP |29-NOV-2021| -- END

                For iRowIdx As Integer = 0 To xRow.Length - 1
                    If blnOnlySendMail = False Then
                    Dim xEmp() As String = xRow(iRowIdx).Item("OwnerIds").ToString.Split(",")
                    If xEmp.Length > 0 Then
                        For iEmpId As Integer = 0 To xEmp.Length - 1
                            'S.SANDEEP [23 APR 2015] -- START
                            Dim xLastStatusId As Integer = 0
                            xLastStatusId = GetLastStatus(xRow(iRowIdx).Item("periodunkid"), xEmp(iEmpId))
                            If xLastStatusId = enObjective_Status.FINAL_SAVE Then
                                Continue For
                            End If
                            'S.SANDEEP [23 APR 2015] -- END

                            Dim objEmpField2, objOField2 As New clsassess_empfield2_master : Dim objEmpField3, objOField3 As New clsassess_empfield3_master
                            Dim objEmpField4, objOField4 As New clsassess_empfield4_master : Dim objEmpField5, objOField5 As New clsassess_empfield5_master

                            Dim iEmpField1Unkid, iEmpField2Unkid, iEmpField3Unkid, iEmpField4Unkid, iEmpField5Unkid As Integer
                            iEmpField1Unkid = 0 : iEmpField2Unkid = 0 : iEmpField3Unkid = 0 : iEmpField4Unkid = 0 : iEmpField5Unkid = 0
                            Dim mDicFieldInfo As New Dictionary(Of Integer, String) : Dim objInfoField As New clsassess_empinfofield_tran
                            _Empfield1unkid = xRow(iRowIdx).Item("empfield1unkid")
                            If mstrField_Data.Trim.Length > 0 Then
                                iEmpField1Unkid = Get_EmployeeField1Unkid(mstrField_Data, xEmp(iEmpId), xRow(iRowIdx).Item("periodunkid"))
                                If iEmpField1Unkid <= 0 Then
                                    _EmpFieldTypeId = enWeight_Types.WEIGHT_FIELD1
                                    _Employeeunkid = xEmp(iEmpId)
                                    _Enddate = mdtEnddate
                                    _Field_Data = mstrField_Data
                                    _Fieldunkid = mintFieldunkid
                                    _Isfinal = False
                                    _Isvoid = False
                                    _Owrfield1unkid = mintOwrfield1unkid
                                    _Pct_Completed = mDecPct_Completed
                                    _Periodunkid = xRow(iRowIdx).Item("periodunkid")
                                    _Startdate = mdtStartdate
                                    _Statusunkid = mintStatusunkid
                                    _Userunkid = mintUserunkid
                                    _Voiddatetime = mdtVoiddatetime
                                    _Voidreason = mstrVoidreason
                                    _Weight = mdblWeight
                                    _Yearunkid = mintYearunkid
'S.SANDEEP |27-NOV-2020| -- START
                                    'ISSUE/ENHANCEMENT : VALID GOAL OPERATION
                                    _GoalTypeid = mintGoalTypeid
                                    _GoalValue = mdblGoalValue
                                    _UnitOfMeasure = mintUnitOfMeasure
                                    'S.SANDEEP |27-NOV-2020| -- END
                                    _FormName = mstrFormName
                                    _Loginemployeeunkid = mintLoginemployeeunkid
                                    _ClientIP = mstrClientIP
                                    _HostName = mstrHostName
                                    _FromWeb = mblnIsWeb
                                    _AuditUserId = mintAuditUserId
                                    _AuditDate = mdtAuditDate
                                    mDicFieldInfo = objInfoField.Get_Data(xRow(iRowIdx).Item("empfield1unkid"), enWeight_Types.WEIGHT_FIELD1)
                                    If Insert(mDicFieldInfo) = True Then
                                        iEmpField1Unkid = _Empfield1unkid
                                    End If
                                End If
                            End If
                            objOField2._Empfield2unkid = xRow(iRowIdx).Item("empfield2unkid")
                            If objOField2._Field_Data.Trim.Length > 0 Then
                                iEmpField2Unkid = objEmpField2.Get_EmployeeField2Unkid(objOField2._Field_Data, xEmp(iEmpId), xRow(iRowIdx).Item("periodunkid"))
                                If iEmpField2Unkid <= 0 Then
                                    objEmpField2._Empfield1unkid = iEmpField1Unkid
                                    objEmpField2._EmpFieldTypeId = enWeight_Types.WEIGHT_FIELD2
                                    objEmpField2._Employeeunkid = xEmp(iEmpId)
                                    objEmpField2._Enddate = objOField2._Enddate
                                    objEmpField2._Field_Data = objOField2._Field_Data
                                    objEmpField2._Fieldunkid = objOField2._Fieldunkid
                                    objEmpField2._Isvoid = objOField2._Isvoid
                                    objEmpField2._Pct_Completed = objOField2._Pct_Completed
                                    objEmpField2._Periodunkid = xRow(iRowIdx).Item("periodunkid")
                                    objEmpField2._Startdate = objOField2._Startdate
                                    objEmpField2._Statusunkid = objOField2._Statusunkid
                                    objEmpField2._Userunkid = objOField2._Userunkid
                                    objEmpField2._Voiddatetime = objOField2._Voiddatetime
                                    objEmpField2._Voidreason = objOField2._Voidreason
                                    objEmpField2._Voiduserunkid = objOField2._Voiduserunkid
                                    objEmpField2._Weight = objOField2._Weight
'S.SANDEEP |27-NOV-2020| -- START
                                    'ISSUE/ENHANCEMENT : VALID GOAL OPERATION
                                    objEmpField2._GoalTypeid = objOField2._GoalTypeid
                                    objEmpField2._GoalValue = objOField2._GoalValue
                                    objEmpField2._UnitOfMeasure = objOField2._UnitOfMeasure
                                    'S.SANDEEP |27-NOV-2020| -- END
                                    With objEmpField2
                                        ._FormName = mstrFormName
                                        ._Loginemployeeunkid = mintLoginemployeeunkid
                                        ._ClientIP = mstrClientIP
                                        ._HostName = mstrHostName
                                        ._FromWeb = mblnIsWeb
                                        ._AuditUserId = mintAuditUserId
                                        ._CompanyUnkid = mintCompanyUnkid
                                        ._AuditDate = mdtAuditDate
                                    End With
                                    mDicFieldInfo = objInfoField.Get_Data(objOField2._Empfield2unkid, enWeight_Types.WEIGHT_FIELD2)
                                    If objEmpField2.Insert(mDicFieldInfo) = True Then
                                        iEmpField2Unkid = objEmpField2._Empfield2unkid
                                    End If
                                End If
                            End If
                            objOField3._Empfield3unkid = xRow(iRowIdx).Item("empfield3unkid")
                            If objOField3._Field_Data.Trim.Length > 0 Then
                                iEmpField3Unkid = objEmpField3.Get_EmployeeField3Unkid(objOField3._Field_Data, xEmp(iEmpId), xRow(iRowIdx).Item("periodunkid"))
                                If iEmpField3Unkid <= 0 Then
                                    objEmpField3._Empfield2unkid = iEmpField2Unkid
                                    objEmpField3._EmpFieldTypeId = enWeight_Types.WEIGHT_FIELD3
                                    objEmpField3._Employeeunkid = xEmp(iEmpId)
                                    objEmpField3._Periodunkid = xRow(iRowIdx).Item("periodunkid")
                                    objEmpField3._Enddate = objOField3._Enddate
                                    objEmpField3._Field_Data = objOField3._Field_Data
                                    objEmpField3._Fieldunkid = objOField3._Fieldunkid
                                    objEmpField3._Isvoid = objOField3._Isvoid
                                    objEmpField3._Pct_Completed = objOField3._Pct_Completed
                                    objEmpField3._Startdate = objOField3._Startdate
                                    objEmpField3._Statusunkid = objOField3._Statusunkid
                                    objEmpField3._Userunkid = objOField3._Userunkid
                                    objEmpField3._Voiddatetime = objOField3._Voiddatetime
                                    objEmpField3._Voidreason = objOField3._Voidreason
                                    objEmpField3._Voiduserunkid = objOField3._Voiduserunkid
                                    objEmpField3._Weight = objOField3._Weight
                                    With objEmpField3
                                        ._FormName = mstrFormName
                                        ._Loginemployeeunkid = mintLoginemployeeunkid
                                        ._ClientIP = mstrClientIP
                                        ._HostName = mstrHostName
                                        ._FromWeb = mblnIsWeb
                                        ._AuditUserId = mintAuditUserId
                                        ._CompanyUnkid = mintCompanyUnkid
                                        ._AuditDate = mdtAuditDate
                                    End With
                                    'S.SANDEEP |27-NOV-2020| -- START
                                    'ISSUE/ENHANCEMENT : VALID GOAL OPERATION
                                    objEmpField3._GoalTypeid = objOField3._GoalTypeid
                                    objEmpField3._GoalValue = objOField3._GoalValue
                                    objEmpField3._UnitOfMeasure = objOField3._UnitOfMeasure
                                    'S.SANDEEP |27-NOV-2020| -- END                                    
                                    mDicFieldInfo = objInfoField.Get_Data(objOField3._Empfield3unkid, enWeight_Types.WEIGHT_FIELD3)
                                    If objEmpField3.Insert(mDicFieldInfo) = True Then
                                        iEmpField3Unkid = objEmpField3._Empfield3unkid
                                    End If
                                End If
                            End If


                            objOField4._Empfield4unkid = xRow(iRowIdx).Item("empfield4unkid")
                            If objOField4._Field_Data.Trim.Length > 0 Then
                                iEmpField4Unkid = objEmpField4.Get_EmployeeField4Unkid(objOField4._Field_Data, xEmp(iEmpId), xRow(iRowIdx).Item("periodunkid"))
                                If iEmpField4Unkid <= 0 Then
                                    objEmpField4._Empfield3unkid = iEmpField3Unkid
                                    objEmpField4._EmpFieldTypeId = enWeight_Types.WEIGHT_FIELD4
                                    objEmpField4._Employeeunkid = xEmp(iEmpId)
                                    objEmpField4._Periodunkid = xRow(iRowIdx).Item("periodunkid")
                                    objEmpField4._Enddate = objOField4._Enddate
                                    objEmpField4._Field_Data = objOField4._Field_Data
                                    objEmpField4._Fieldunkid = objOField4._Fieldunkid
                                    objEmpField4._Isvoid = objOField4._Isvoid
                                    objEmpField4._Pct_Completed = objOField4._Pct_Completed
                                    objEmpField4._Startdate = objOField4._Startdate
                                    objEmpField4._Statusunkid = objOField4._Statusunkid
                                    objEmpField4._Userunkid = objOField4._Userunkid
                                    objEmpField4._Voiddatetime = objOField4._Voiddatetime
                                    objEmpField4._Voidreason = objOField4._Voidreason
                                    objEmpField4._Voiduserunkid = objOField4._Voiduserunkid
                                    objEmpField4._Weight = objOField4._Weight
                                    With objEmpField4
                                        ._FormName = mstrFormName
                                        ._Loginemployeeunkid = mintLoginemployeeunkid
                                        ._ClientIP = mstrClientIP
                                        ._HostName = mstrHostName
                                        ._FromWeb = mblnIsWeb
                                        ._AuditUserId = mintAuditUserId
                                        ._CompanyUnkid = mintCompanyUnkid
                                        ._AuditDate = mdtAuditDate
                                    End With
'S.SANDEEP |27-NOV-2020| -- START
                                    'ISSUE/ENHANCEMENT : VALID GOAL OPERATION
                                    objEmpField4._GoalTypeid = objOField4._GoalTypeid
                                    objEmpField4._GoalValue = objOField4._GoalValue
                                    objEmpField4._UnitOfMeasure = objOField4._UnitOfMeasure
                                    'S.SANDEEP |27-NOV-2020| -- END
                                    mDicFieldInfo = objInfoField.Get_Data(objOField4._Empfield4unkid, enWeight_Types.WEIGHT_FIELD4)
                                    If objEmpField4.Insert(mDicFieldInfo) = True Then
                                        iEmpField4Unkid = objEmpField4._Empfield4unkid
                                    End If
                                End If
                            End If

                            objOField5._Empfield5unkid = xRow(iRowIdx).Item("empfield5unkid")
                            If objOField5._Field_Data.Trim.Length > 0 Then
                                iEmpField5Unkid = objEmpField5.Get_EmployeeField5Unkid(objOField5._Field_Data, xEmp(iEmpId), xRow(iRowIdx).Item("periodunkid"))
                                If iEmpField5Unkid <= 0 Then
                                    objEmpField5._Empfield4unkid = iEmpField4Unkid
                                    objEmpField5._EmpFieldTypeId = enWeight_Types.WEIGHT_FIELD5
                                    objEmpField5._Employeeunkid = xEmp(iEmpId)
                                    objEmpField5._Periodunkid = xRow(iRowIdx).Item("periodunkid")
                                    objEmpField5._Enddate = objOField5._Enddate
                                    objEmpField5._Field_Data = objOField5._Field_Data
                                    objEmpField5._Fieldunkid = objOField5._Fieldunkid
                                    objEmpField5._Isvoid = objOField5._Isvoid
                                    objEmpField5._Pct_Completed = objOField5._Pct_Completed
                                    objEmpField5._Startdate = objOField5._Startdate
                                    objEmpField5._Statusunkid = objOField5._Statusunkid
                                    objEmpField5._Userunkid = objOField5._Userunkid
                                    objEmpField5._Voiddatetime = objOField5._Voiddatetime
                                    objEmpField5._Voidreason = objOField5._Voidreason
                                    objEmpField5._Voiduserunkid = objOField5._Voiduserunkid
                                    objEmpField5._Weight = objOField5._Weight
                                    With objEmpField5
                                        ._FormName = mstrFormName
                                        ._Loginemployeeunkid = mintLoginemployeeunkid
                                        ._ClientIP = mstrClientIP
                                        ._HostName = mstrHostName
                                        ._FromWeb = mblnIsWeb
                                        ._AuditUserId = mintAuditUserId
                                        ._CompanyUnkid = mintCompanyUnkid
                                        ._AuditDate = mdtAuditDate
                                    End With
'S.SANDEEP |27-NOV-2020| -- START
                                    'ISSUE/ENHANCEMENT : VALID GOAL OPERATION
                                    objEmpField5._GoalTypeid = objOField5._GoalTypeid
                                    objEmpField5._GoalValue = objOField5._GoalValue
                                    objEmpField5._UnitOfMeasure = objOField5._UnitOfMeasure
                                    'S.SANDEEP |27-NOV-2020| -- END
                                    mDicFieldInfo = objInfoField.Get_Data(objOField5._Empfield5unkid, enWeight_Types.WEIGHT_FIELD5)
                                    If objEmpField5.Insert(mDicFieldInfo) = True Then
                                        iEmpField5Unkid = objEmpField5._Empfield5unkid
                                    End If
                                End If
                            End If
                            'Pinkal (07-Dec-2020) -- Start
                            'Enhancement NMB - Cascading PM UAT Comments.

                                'S.SANDEEP |29-NOV-2021| -- START
                                'ISSUE : SENDING SINGLE EMAIL
                                'SendNotification_TransferGoalsSubordinateEmployee(xCompanyId, xEmp(iEmpId), iLoginTypeId, iLoginEmployeeId, iUserId)
                                'S.SANDEEP |29-NOV-2021| -- END

                            'Pinkal (07-Dec-2020) -- End
                        Next
                    End If
                    Else
                        'Pinkal (07-Dec-2020) -- Start
                        'Enhancement NMB - Cascading PM UAT Comments.

                        'S.SANDEEP |29-NOV-2021| -- START
                        'ISSUE : SENDING SINGLE EMAIL
                        'SendNotification_TransferGoalsSubordinateEmployee(xCompanyId, xRow(iRowIdx).Item("employeeunkid"), iLoginTypeId, iLoginEmployeeId, iUserId)
                        'S.SANDEEP |29-NOV-2021| -- END

                        'Pinkal (07-Dec-2020) -- End
                    End If
                Next
                'S.SANDEEP |29-NOV-2021| -- START
                'ISSUE : SENDING SINGLE EMAIL
                If iList IsNot Nothing AndAlso iList.Count > 0 Then
                    For Each iNum As String In iList
                        SendNotification_TransferGoalsSubordinateEmployee(xCompanyId, CInt(iNum), iLoginTypeId, iLoginEmployeeId, iUserId)
                    Next
                End If
                'S.SANDEEP |29-NOV-2021| -- END
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "Transfer_Goals_Subordinate_Employee", mstrModuleName)
        Finally
        End Try
        'S.SANDEEP [23 APR 2015] -- START
        Return True
        'S.SANDEEP [23 APR 2015] -- END
    End Function

    'S.SANDEEP [23 APR 2015] -- START
    Public Function GetLastStatus(ByVal iPeriodId As Integer, ByVal iEmployeeId As Integer) As Integer
        Dim StrQ As String = String.Empty
        Dim iStatusId As Integer = -1
        Dim exForce As Exception
        Dim dsList As New DataSet
        Try
            Using objDataOperation As New clsDataOperation

                StrQ = "SELECT TOP 1 " & _
                       "  statustypeid " & _
                       "FROM hrassess_empstatus_tran WITH (NOLOCK) " & _
                       "WHERE periodunkid = '" & iPeriodId & "' " & _
                       "  AND employeeunkid = '" & iEmployeeId & "' " & _
                       "ORDER BY status_date DESC "

                dsList = objDataOperation.ExecQuery(StrQ, "List")

                If objDataOperation.ErrorMessage <> "" Then
                    exForce = New Exception(objDataOperation.ErrorNumber & " : " & objDataOperation.ErrorMessage)
                    Throw exForce
                End If

                If dsList.Tables(0).Rows.Count > 0 Then
                    iStatusId = dsList.Tables(0).Rows(0).Item("statustypeid")
                Else
                    iStatusId = 0
                End If
            End Using
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "GetLastStatus", mstrModuleName)
        Finally
        End Try
        Return iStatusId
    End Function
    'S.SANDEEP [23 APR 2015] -- END

    'S.SANDEEP [ 10 JAN 2015 ] -- START
    Public Function GetListForTansfer(ByVal xPeriodId As Integer, ByVal xEmployeeId As Integer) As DataSet
        Dim StrQ As String = String.Empty
        Dim dsList As New DataSet
        Try
            Using objDo As New clsDataOperation
                StrQ = "SELECT " & _
                       "     empfield1unkid " & _
                       "    ,owrfield1unkid " & _
                       "    ,perspectiveunkid " & _
                       "    ,employeeunkid " & _
                       "    ,fieldunkid " & _
                       "    ,field_data " & _
                       "    ,yearunkid " & _
                       "    ,periodunkid " & _
                       "    ,weight " & _
                       "    ,pct_completed " & _
                       "    ,isfinal " & _
                       "    ,startdate " & _
                       "    ,enddate " & _
                       "    ,statusunkid " & _
                       "    ,userunkid " & _
                       "    ,isvoid " & _
                       "    ,voiduserunkid " & _
                       "    ,voidreason " & _
                       "    ,voiddatetime " & _
                       "    ,ISNULL(goaltypeid," & CInt(enGoalType.GT_QUALITATIVE) & ") AS goaltypeid " & _
                       "    ,ISNULL(goalvalue,0) AS goalvalue " & _
                       "FROM hrassess_empfield1_master WITH (NOLOCK) " & _
                       "WHERE isvoid = 0 AND periodunkid = '" & xPeriodId & "' AND employeeunkid = '" & xEmployeeId & "' ORDER BY empfield1unkid "

                dsList = objDo.ExecQuery(StrQ, "List")

                If objDo.ErrorMessage <> "" Then
                    Throw New Exception(objDo.ErrorNumber & " : " & objDo.ErrorMessage)
                End If

            End Using
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "GetListForTansfer", mstrModuleName)
        Finally
        End Try
        Return dsList
    End Function

    Public Function GetEmpFieldUnkid(ByVal xFieldData As String, ByVal xPeriodId As Integer, ByVal xEmployeeUnkid As Integer) As Integer
        Dim StrQ As String = String.Empty
        Dim xUnkid As Integer = -1
        Try
            Using objDo As New clsDataOperation

                StrQ = "SELECT @xUnkid = empfield1unkid FROM hrassess_empfield1_master WITH (NOLOCK) WHERE isvoid = 0 " & _
                       " AND periodunkid = @xPeriodId AND field_data = @xFieldData AND employeeunkid = @xEmployeeUnkid "

                objDo.AddParameter("@xUnkid", SqlDbType.Int, eZeeDataType.INT_SIZE, xUnkid, ParameterDirection.InputOutput)
                objDo.AddParameter("@xPeriodId", SqlDbType.Int, eZeeDataType.INT_SIZE, xPeriodId)
                objDo.AddParameter("@xFieldData", SqlDbType.NVarChar, xFieldData.Length, xFieldData)
                objDo.AddParameter("@xEmployeeUnkid", SqlDbType.Int, eZeeDataType.INT_SIZE, xEmployeeUnkid)

                objDo.ExecNonQuery(StrQ)

                If objDo.ErrorMessage <> "" Then
                    Throw New Exception(objDo.ErrorNumber & " : " & objDo.ErrorMessage)
                End If

                xUnkid = objDo.GetParameterValue("@xUnkid")

            End Using
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "GetEmpFieldUnkid", mstrModuleName)
        Finally
        End Try
        Return xUnkid
    End Function
    'S.SANDEEP [ 10 JAN 2015 ] -- END

    'S.SANDEEP [10 DEC 2015] -- START
    Public Function GetGoalsDataTableFormatForPlanningWizard(ByVal strName As String, ByVal intPeriodId As Integer) As DataTable
        Dim dtFinalTab As DataTable
        Dim iMappedLinkedFieldId As Integer = 0
        Dim dsList As New DataSet
        Try
            If strName.Trim.Length <= 0 Then strName = "List"
            dtFinalTab = New DataTable(strName)
            Dim dCol As DataColumn

            With dtFinalTab

                dCol = New DataColumn
                With dCol
                    .ColumnName = "assign"
                    .Caption = ""
                    .DataType = System.Type.GetType("System.Boolean")
                    .DefaultValue = False
                End With
                .Columns.Add(dCol)

                dCol = New DataColumn
                With dCol
                    .ColumnName = "owrfieldunkid"
                    .Caption = ""
                    .DataType = System.Type.GetType("System.Int32")
                    .DefaultValue = 0
                End With
                .Columns.Add(dCol)

                dCol = New DataColumn
                With dCol
                    .ColumnName = "perspectiveunkid"
                    .Caption = ""
                    .DataType = System.Type.GetType("System.Int32")
                    .DefaultValue = 0
                End With
                .Columns.Add(dCol)

                dCol = New DataColumn
                With dCol
                    .ColumnName = "periodunkid"
                    .Caption = ""
                    .DataType = System.Type.GetType("System.Int32")
                    .DefaultValue = 0
                End With
                .Columns.Add(dCol)

                dCol = New DataColumn
                With dCol
                    .ColumnName = "userunkid"
                    .Caption = ""
                    .DataType = System.Type.GetType("System.Int32")
                    .DefaultValue = 0
                End With
                .Columns.Add(dCol)

                dCol = New DataColumn
                With dCol
                    .ColumnName = "perspective"
                    .Caption = Language.getMessage(mstrModuleName, 27, "Perspective")
                    .DataType = System.Type.GetType("System.String")
                    .DefaultValue = ""
                End With
                .Columns.Add(dCol)

                Dim objFMaster As New clsAssess_Field_Master(True)
                Dim objMap As New clsAssess_Field_Mapping
                iMappedLinkedFieldId = objMap.Get_Map_FieldId(intPeriodId)
                Dim iExOrder As Integer = objFMaster.Get_Field_ExOrder(iMappedLinkedFieldId, True)

                dCol = New DataColumn
                With dCol
                    .ColumnName = "linkfieldid"
                    .Caption = ""
                    .DataType = System.Type.GetType("System.Int32")
                    .DefaultValue = iMappedLinkedFieldId
                End With
                .Columns.Add(dCol)

                dsList = objFMaster.Get_Field_Mapping(strName)
                Dim i As Integer = 1
                If dsList.Tables(strName).Rows.Count > 0 Then
                    For Each dRow As DataRow In dsList.Tables(strName).Rows
                        'For iR As Integer = 0 To 8
                        dCol = New DataColumn
                        With dCol
                            .ColumnName = "f" & i.ToString & "fieldunkid"
                            .Caption = ""
                            .DataType = System.Type.GetType("System.Int32")
                            .DefaultValue = 0
                        End With
                        .Columns.Add(dCol)

                        dCol = New DataColumn
                        With dCol
                            .ColumnName = "f" & i.ToString & "field_data"
                            .Caption = dRow.Item("fieldcaption").ToString
                            .DataType = System.Type.GetType("System.String")
                            .DefaultValue = ""
                        End With
                        .Columns.Add(dCol)

                        dCol = New DataColumn
                        With dCol
                            .ColumnName = "f" & i.ToString & "weight"
                            .Caption = Language.getMessage(mstrModuleName, 22, "Weight")
                            .DataType = System.Type.GetType("System.Decimal")
                            .DefaultValue = 0
                        End With
                        .Columns.Add(dCol)

                        dCol = New DataColumn
                        With dCol
                            .ColumnName = "f" & i.ToString & "pct_completed"
                            .Caption = Language.getMessage(mstrModuleName, 23, "% Completed")
                            .DataType = System.Type.GetType("System.Decimal")
                            .DefaultValue = 0
                        End With
                        .Columns.Add(dCol)

                        dCol = New DataColumn
                        With dCol
                            .ColumnName = "f" & i.ToString & "startdate"
                            .Caption = Language.getMessage(mstrModuleName, 18, "Start Date")
                            .DataType = System.Type.GetType("System.DateTime")
                            .DefaultValue = DBNull.Value
                        End With
                        .Columns.Add(dCol)

                        dCol = New DataColumn
                        With dCol
                            .ColumnName = "f" & i.ToString & "enddate"
                            .Caption = Language.getMessage(mstrModuleName, 19, "End Date")
                            .DataType = System.Type.GetType("System.DateTime")
                            .DefaultValue = DBNull.Value
                        End With
                        .Columns.Add(dCol)

                        dCol = New DataColumn
                        With dCol
                            .ColumnName = "f" & i.ToString & "statusunkid"
                            .Caption = ""
                            .DataType = System.Type.GetType("System.Int32")
                            .DefaultValue = 0
                        End With
                        .Columns.Add(dCol)

                        dCol = New DataColumn
                        With dCol
                            .ColumnName = "f" & i.ToString & "status"
                            .Caption = Language.getMessage(mstrModuleName, 20, "Status")
                            .DataType = System.Type.GetType("System.String")
                            .DefaultValue = ""
                        End With
                        .Columns.Add(dCol)

                        dCol = New DataColumn
                        With dCol
                            .ColumnName = "f" & i.ToString & "fieldtypeid"
                            .Caption = ""
                            .DataType = System.Type.GetType("System.Int32")
                            .DefaultValue = 0
                        End With
                        .Columns.Add(dCol)
                        'Next
                        i += 1
                    Next
                End If
                objMap = Nothing
                objFMaster = Nothing
            End With
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: ; Module Name: " & mstrModuleName)
        Finally
        End Try
        Return dtFinalTab
    End Function
    'S.SANDEEP [10 DEC 2015] -- END



    'Pinkal (19-Sep-2016) -- Start
    'Enhancement - Global Operation for all Organization.

    'S.SANDEEP |05-MAY-2019| -- START
    'ISSUE/ENHANCEMENT : TRA ENHANCEMENT FOR ANALYSIS BY
    'Public Function GetGlobalEmployeeeList(ByVal xPeriodID As Integer, ByVal xOperationType As Integer, ByVal xUserID As Integer) As DataSet
    '    Dim dsList As DataSet = Nothing
    '    Dim strQ As String = ""
    '    Dim exForce As Exception
    '    Try
    '        Dim objDataOperation As New clsDataOperation
    '        'S.SANDEEP [27-SEP-2017] -- START
    '        'ISSUE/ENHANCEMENT : REF-ID # 75
    '        'strQ = "SELECT  0 AS ischeck " & _
    '        '          ", hrassess_empfield1_master.employeeunkid  " & _
    '        '          ", RTRIM(LTRIM(ISNULL(hremployee_master.employeecode, ''))) + ' - ' + ISNULL(hremployee_master.firstname, '') + ' ' + ISNULL(hremployee_master.othername, '') + ' ' + ISNULL(hremployee_master.surname, '') AS Employee  " & _
    '        '          ", ISNULL(a.statustypeid, 3) AS statustypeid  " & _
    '        '          ", SUM(ISNULL(hrassess_empfield1_master.weight, 0.00)) AS Weight "

    '        ''S.SANDEEP [17-AUG-2017] -- START
    '        ''ISSUE/ENHANCEMENT : ERROR FOR VARIABLE DECLARATION
    '        ''If xOperationType = enObjective_Status.SUBMIT_APPROVAL AndAlso xUserID > 0 Then
    '        'If (xOperationType = enObjective_Status.SUBMIT_APPROVAL Or xOperationType = enObjective_Status.FINAL_SAVE) AndAlso xUserID > 0 Then
    '        '    strQ &= ", hrassessor_master.assessormasterunkid " & _
    '        '                ", hrassessor_master.employeeunkid AS assessorEmpid " & _
    '        '                ", ISNULL(assessor.firstname, '') + ' ' + ISNULL(assessor.othername, '') + ' ' + ISNULL(assessor.surname, '') AS AssessorName "
    '        'End If
    '        ''S.SANDEEP [17-AUG-2017] -- END


    '        'strQ &= " FROM  hrassess_empfield1_master " & _
    '        '            " LEFT JOIN hremployee_master ON hremployee_master.employeeunkid = hrassess_empfield1_master.employeeunkid " & _
    '        '            " LEFT JOIN ( " & _
    '        '            "     SELECT  ROW_NUMBER() OVER ( PARTITION BY hrassess_empstatus_tran.employeeunkid ORDER BY hrassess_empstatus_tran.status_date DESC ) AS Rno  " & _
    '        '            "         ,hrassess_empstatus_tran.periodunkid  " & _
    '        '            "         ,hrassess_empstatus_tran.statustypeid  " & _
    '        '            "         ,hrassess_empstatus_tran.employeeunkid " & _
    '        '            "     FROM hrassess_empstatus_tran " & _
    '        '            "  ) AS A ON A.periodunkid = hrassess_empfield1_master.periodunkid AND A.employeeunkid = hrassess_empfield1_master.employeeunkid AND A.Rno = 1 "

    '        ''S.SANDEEP [17-AUG-2017] -- START
    '        ''ISSUE/ENHANCEMENT : ERROR FOR VARIABLE DECLARATION
    '        ''If xOperationType = enObjective_Status.SUBMIT_APPROVAL AndAlso xUserID > 0 Then
    '        'If (xOperationType = enObjective_Status.SUBMIT_APPROVAL Or xOperationType = enObjective_Status.FINAL_SAVE) AndAlso xUserID > 0 Then
    '        '    'S.SANDEEP [17-AUG-2017] -- END
    '        '    strQ &= " LEFT JOIN hrassessor_tran ON hrassessor_tran.employeeunkid = hrassess_empfield1_master.employeeunkid AND hrassessor_tran.isvoid = 0 AND hrassessor_tran.visibletypeid = '" & clsAssessor.enARVisibilityTypeId.VISIBLE & "' " & _
    '        '                " JOIN hrassessor_master ON hrassessor_master.assessormasterunkid = hrassessor_tran.assessormasterunkid AND hrassessor_master.isvoid = 0  AND hrassessor_master.isreviewer = 0 " & _
    '        '                " JOIN hrapprover_usermapping on hrapprover_usermapping.approverunkid = hrassessor_master.assessormasterunkid AND hrapprover_usermapping.usertypeid = " & enUserType.Assessor & _
    '        '                " LEFT JOIN hremployee_master assessor ON assessor.employeeunkid = hrassessor_master.employeeunkid "
    '        'End If

    '        'strQ &= " WHERE  hrassess_empfield1_master.isvoid = 0  AND hrassess_empfield1_master.periodunkid = @periodunkid AND statustypeid = @status "
    '        ''S.SANDEEP [27 DEC 2016] -- START {visibletypeid} -- END

    '        'If xOperationType = enObjective_Status.FINAL_SAVE Then
    '        '    strQ &= " AND hrassess_empfield1_master.isfinal = 1 "
    '        'ElseIf xOperationType = enObjective_Status.SUBMIT_APPROVAL Then
    '        '    strQ &= " AND hrapprover_usermapping.userunkid = @userunkid  "
    '        'End If

    '        'strQ &= " GROUP BY a.statustypeid " & _
    '        '         ", RTRIM(LTRIM(ISNULL(hremployee_master.employeecode, ''))) + ' - ' + ISNULL(hremployee_master.firstname, '') + ' ' + ISNULL(hremployee_master.othername, '') + ' ' + ISNULL(hremployee_master.surname, '')  " & _
    '        '         ", hrassess_empfield1_master.employeeunkid "

    '        ''S.SANDEEP [17-AUG-2017] -- START
    '        ''ISSUE/ENHANCEMENT : ERROR FOR VARIABLE DECLARATION
    '        ''If xOperationType = enObjective_Status.SUBMIT_APPROVAL AndAlso xUserID > 0 Then
    '        'If (xOperationType = enObjective_Status.SUBMIT_APPROVAL Or xOperationType = enObjective_Status.FINAL_SAVE) AndAlso xUserID > 0 Then
    '        '    'S.SANDEEP [17-AUG-2017] -- END
    '        '    strQ &= ", ISNULL(assessor.firstname, '') + ' ' + ISNULL(assessor.othername, '') + ' ' + ISNULL(assessor.surname, '') " & _
    '        '                ", hrassessor_master.assessormasterunkid " & _
    '        '                ", hrassessor_master.employeeunkid  "
    '        'End If

    '        strQ = "DECLARE @fldid INT " & _
    '               "SET @fldid = (SELECT HFM.fieldunkid FROM hrassess_field_mapping HFM WHERE HFM.periodunkid = @periodunkid AND HFM.isactive = 1) " & _
    '               "SELECT DISTINCT " & _
    '               "     ischeck " & _
    '               "    ,employeeunkid " & _
    '               "    ,Employee " & _
    '               "    ,statustypeid " & _
    '               "    ,SUM(GEL.Weight) AS Weight " & _
    '               "    ,assessormasterunkid " & _
    '               "    ,assessorEmpid " & _
    '               "    ,AssessorName " & _
    '               "    ,mappinguserid " & _
    '               "FROM " & _
    '               "( " & _
    '               "    SELECT DISTINCT " & _
    '               "         0 AS ischeck " & _
    '               "        ,hrassess_empfield1_master.employeeunkid " & _
    '               "        ,RTRIM(LTRIM(ISNULL(hremployee_master.employeecode, ''))) + ' - ' + ISNULL(hremployee_master.firstname, '') + ' ' + ISNULL(hremployee_master.othername, '') + ' ' + ISNULL(hremployee_master.surname, '') AS Employee " & _
    '               "        ,ISNULL(a.statustypeid, 3) AS statustypeid " & _
    '               "        ,CASE WHEN @fldid = 1 THEN ISNULL(hrassess_empfield1_master.weight, 0.00) " & _
    '               "              WHEN @fldid = 2 THEN ISNULL(hrassess_empfield2_master.weight, 0.00) " & _
    '               "              WHEN @fldid = 3 THEN ISNULL(hrassess_empfield3_master.weight, 0.00) " & _
    '               "              WHEN @fldid = 4 THEN ISNULL(hrassess_empfield4_master.weight, 0.00) " & _
    '               "              WHEN @fldid = 5 THEN ISNULL(hrassess_empfield5_master.weight, 0.00) " & _
    '               "         END AS Weight " & _
    '               "        ,CASE WHEN @fldid = 1 THEN ISNULL(hrassess_empfield1_master.empfield1unkid, 0) " & _
    '               "              WHEN @fldid = 2 THEN ISNULL(hrassess_empfield2_master.empfield2unkid, 0) " & _
    '               "              WHEN @fldid = 3 THEN ISNULL(hrassess_empfield3_master.empfield3unkid, 0) " & _
    '               "              WHEN @fldid = 4 THEN ISNULL(hrassess_empfield4_master.empfield4unkid, 0) " & _
    '               "              WHEN @fldid = 5 THEN ISNULL(hrassess_empfield5_master.empfield5unkid, 0) " & _
    '               "         END AS empfldid " & _
    '               "        ,hrassessor_master.assessormasterunkid " & _
    '               "        ,hrassessor_master.employeeunkid AS assessorEmpid " & _
    '               "        ,ISNULL(assessor.firstname, '') + ' ' /*+ ISNULL(assessor.othername, '') + ' '*/ + ISNULL(assessor.surname, '') AS AssessorName " & _
    '               "        ,hrapprover_usermapping.userunkid AS mappinguserid " & _
    '               "    FROM hrassess_empfield1_master " & _
    '               "        LEFT JOIN hrassess_empfield2_master ON hrassess_empfield1_master.empfield1unkid = hrassess_empfield2_master.empfield1unkid AND hrassess_empfield2_master.isvoid = 0 " & _
    '               "        LEFT JOIN hrassess_empfield3_master ON hrassess_empfield3_master.empfield2unkid = hrassess_empfield2_master.empfield2unkid AND hrassess_empfield3_master.isvoid = 0 " & _
    '               "        LEFT JOIN hrassess_empfield4_master ON hrassess_empfield4_master.empfield3unkid = hrassess_empfield3_master.empfield3unkid AND hrassess_empfield4_master.isvoid = 0 " & _
    '               "        LEFT JOIN hrassess_empfield5_master ON hrassess_empfield5_master.empfield4unkid = hrassess_empfield4_master.empfield4unkid AND hrassess_empfield5_master.isvoid = 0 " & _
    '                    " LEFT JOIN hremployee_master ON hremployee_master.employeeunkid = hrassess_empfield1_master.employeeunkid " & _
    '               "        LEFT JOIN " & _
    '               "        ( " & _
    '               "            SELECT " & _
    '               "                 ROW_NUMBER() OVER ( PARTITION BY hrassess_empstatus_tran.employeeunkid ORDER BY hrassess_empstatus_tran.status_date DESC ) AS Rno " & _
    '                    "         ,hrassess_empstatus_tran.periodunkid  " & _
    '                    "         ,hrassess_empstatus_tran.statustypeid  " & _
    '                    "         ,hrassess_empstatus_tran.employeeunkid " & _
    '                    "     FROM hrassess_empstatus_tran " & _
    '               "        ) AS A ON A.periodunkid = hrassess_empfield1_master.periodunkid AND A.employeeunkid = hrassess_empfield1_master.employeeunkid AND A.Rno = 1 " & _
    '               "        LEFT JOIN hrassessor_tran ON hrassessor_tran.employeeunkid = hrassess_empfield1_master.employeeunkid AND hrassessor_tran.isvoid = 0 AND hrassessor_tran.visibletypeid = '" & clsAssessor.enARVisibilityTypeId.VISIBLE & "' " & _
    '                        " JOIN hrassessor_master ON hrassessor_master.assessormasterunkid = hrassessor_tran.assessormasterunkid AND hrassessor_master.isvoid = 0  AND hrassessor_master.isreviewer = 0 " & _
    '               "        LEFT JOIN hrapprover_usermapping on hrapprover_usermapping.approverunkid = hrassessor_master.assessormasterunkid AND hrapprover_usermapping.usertypeid = " & enUserType.Assessor & _
    '               "        LEFT JOIN hremployee_master assessor ON assessor.employeeunkid = hrassessor_master.employeeunkid " & _
    '               "    WHERE  hrassess_empfield1_master.isvoid = 0  AND hrassess_empfield1_master.periodunkid = @periodunkid AND statustypeid = @status "
    '        'S.SANDEEP |28-JAN-2019| -- START {-- ADDED : HFM.isactive = 1}  -- END

    '        'S.SANDEEP |25-MAR-2019| -- START
    '        'REMOVED : ,ISNULL(assessor.firstname, '') + ' ' + ISNULL(assessor.othername, '') + ' ' + ISNULL(assessor.surname, '') AS AssessorName
    '        'ADDED   : ,ISNULL(assessor.firstname, '') + ' ' /*+ ISNULL(assessor.othername, '') + ' '*/ + ISNULL(assessor.surname, '') AS AssessorName
    '        'S.SANDEEP |25-MAR-2019| -- END

    '        If xOperationType = enObjective_Status.FINAL_SAVE Then
    '            strQ &= " AND hrassess_empfield1_master.isfinal = 1 "
    '        End If

    '        strQ &= ") AS GEL "
    '        'S.SANDEEP [27-SEP-2017] -- END

    '        'S.SANDEEP [19-OCT-2017] -- START
    '        'ISSUE/ENHANCEMENT : SQL SUPPORT FOR SUM()OVER()
    '        '"    ,SUM(GEL.Weight) OVER(PARTITION BY GEL.Employee ORDER BY GEL.employeeunkid) AS Weight " & _
    '        strQ &= "GROUP by " & _
    '                "ischeck, employeeunkid, Employee, statustypeid, assessormasterunkid, " & _
    '                "assessorEmpid, AssessorName, mappinguserid "
    '        'S.SANDEEP [19-OCT-2017] -- END



    '        objDataOperation.ClearParameters()
    '        objDataOperation.AddParameter("@periodunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, xPeriodID)
    '        objDataOperation.AddParameter("@status", SqlDbType.Int, eZeeDataType.INT_SIZE, xOperationType)
    '        'objDataOperation.AddParameter("@userunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, xUserID) 'S.SANDEEP [27-SEP-2017]-- START {REF-ID # 75} -- END

    '        dsList = objDataOperation.ExecQuery(strQ, "List")

    '        If objDataOperation.ErrorMessage <> "" Then
    '            exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
    '            Throw exForce
    '        End If

    '    Catch ex As Exception
    '        Throw New Exception(ex.Message & "; Procedure Name: GetGlobalEmployeeeList; Module Name: " & mstrModuleName)
    '    End Try
    '    Return dsList
    'End Function
    Public Function GetGlobalEmployeeeList(ByVal xPeriodID As Integer, ByVal xOperationType As Integer, ByVal xUserID As Integer, ByVal xDatabaseName As String, ByVal xDate As Date, Optional ByVal strAdvFilter As String = "") As DataSet
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception
        Try
            Dim objDataOperation As New clsDataOperation
            
            Dim xAdvanceJoinQry As String = ""
            Call GetAdvanceFilterQry(xAdvanceJoinQry, xDate, xDatabaseName)

            strQ = "DECLARE @fldid INT " & _
                   "SET @fldid = (SELECT HFM.fieldunkid FROM hrassess_field_mapping HFM WHERE HFM.periodunkid = @periodunkid AND HFM.isactive = 1) " & _
                   "SELECT DISTINCT " & _
                   "     ischeck " & _
                   "    ,employeeunkid " & _
                   "    ,Employee " & _
                   "    ,statustypeid " & _
                   "    ,SUM(GEL.Weight) AS Weight " & _
                   "    ,assessormasterunkid " & _
                   "    ,assessorEmpid " & _
                   "    ,AssessorName " & _
                   "    ,mappinguserid " & _
                   "FROM " & _
                   "( " & _
                   "    SELECT DISTINCT " & _
                   "         0 AS ischeck " & _
                   "        ,hrassess_empfield1_master.employeeunkid " & _
                   "        ,RTRIM(LTRIM(ISNULL(hremployee_master.employeecode, ''))) + ' - ' + ISNULL(hremployee_master.firstname, '') + ' ' + ISNULL(hremployee_master.othername, '') + ' ' + ISNULL(hremployee_master.surname, '') AS Employee " & _
                   "        ,ISNULL(a.statustypeid, 3) AS statustypeid " & _
                   "        ,CASE WHEN @fldid = 1 THEN ISNULL(hrassess_empfield1_master.weight, 0.00) " & _
                   "              WHEN @fldid = 2 THEN ISNULL(hrassess_empfield2_master.weight, 0.00) " & _
                   "              WHEN @fldid = 3 THEN ISNULL(hrassess_empfield3_master.weight, 0.00) " & _
                   "              WHEN @fldid = 4 THEN ISNULL(hrassess_empfield4_master.weight, 0.00) " & _
                   "              WHEN @fldid = 5 THEN ISNULL(hrassess_empfield5_master.weight, 0.00) " & _
                   "         END AS Weight " & _
                   "        ,CASE WHEN @fldid = 1 THEN ISNULL(hrassess_empfield1_master.empfield1unkid, 0) " & _
                   "              WHEN @fldid = 2 THEN ISNULL(hrassess_empfield2_master.empfield2unkid, 0) " & _
                   "              WHEN @fldid = 3 THEN ISNULL(hrassess_empfield3_master.empfield3unkid, 0) " & _
                   "              WHEN @fldid = 4 THEN ISNULL(hrassess_empfield4_master.empfield4unkid, 0) " & _
                   "              WHEN @fldid = 5 THEN ISNULL(hrassess_empfield5_master.empfield5unkid, 0) " & _
                   "         END AS empfldid " & _
                   "        ,hrassessor_master.assessormasterunkid " & _
                   "        ,hrassessor_master.employeeunkid AS assessorEmpid " & _
                   "        ,ISNULL(assessor.firstname, '') + ' ' /*+ ISNULL(assessor.othername, '') + ' '*/ + ISNULL(assessor.surname, '') AS AssessorName " & _
                   "        ,hrapprover_usermapping.userunkid AS mappinguserid " & _
                   "    FROM hrassess_empfield1_master " & _
                   "        LEFT JOIN hrassess_empfield2_master ON hrassess_empfield1_master.empfield1unkid = hrassess_empfield2_master.empfield1unkid AND hrassess_empfield2_master.isvoid = 0 " & _
                   "        LEFT JOIN hrassess_empfield3_master ON hrassess_empfield3_master.empfield2unkid = hrassess_empfield2_master.empfield2unkid AND hrassess_empfield3_master.isvoid = 0 " & _
                   "        LEFT JOIN hrassess_empfield4_master ON hrassess_empfield4_master.empfield3unkid = hrassess_empfield3_master.empfield3unkid AND hrassess_empfield4_master.isvoid = 0 " & _
                   "        LEFT JOIN hrassess_empfield5_master ON hrassess_empfield5_master.empfield4unkid = hrassess_empfield4_master.empfield4unkid AND hrassess_empfield5_master.isvoid = 0 " & _
                   "        LEFT JOIN hremployee_master ON hremployee_master.employeeunkid = hrassess_empfield1_master.employeeunkid "
            If xAdvanceJoinQry.Trim.Length > 0 Then
                strQ &= xAdvanceJoinQry & " "
            End If
            strQ &= "        LEFT JOIN " & _
                   "        ( " & _
                   "            SELECT " & _
                   "                 ROW_NUMBER() OVER ( PARTITION BY hrassess_empstatus_tran.employeeunkid ORDER BY hrassess_empstatus_tran.status_date DESC ) AS Rno " & _
                    "                ,hrassess_empstatus_tran.periodunkid  " & _
                    "                ,hrassess_empstatus_tran.statustypeid  " & _
                    "                ,hrassess_empstatus_tran.employeeunkid " & _
                    "            FROM hrassess_empstatus_tran " & _
                    "            WHERE hrassess_empstatus_tran.periodunkid = @periodunkid " & _
                   "        ) AS A ON A.periodunkid = hrassess_empfield1_master.periodunkid AND A.employeeunkid = hrassess_empfield1_master.employeeunkid AND A.Rno = 1 " & _
                   "        LEFT JOIN hrassessor_tran ON hrassessor_tran.employeeunkid = hrassess_empfield1_master.employeeunkid AND hrassessor_tran.isvoid = 0 AND hrassessor_tran.visibletypeid = '" & clsAssessor.enARVisibilityTypeId.VISIBLE & "' " & _
                    "        JOIN hrassessor_master ON hrassessor_master.assessormasterunkid = hrassessor_tran.assessormasterunkid AND hrassessor_master.isvoid = 0  AND hrassessor_master.isreviewer = 0 " & _
                   "        LEFT JOIN hrapprover_usermapping on hrapprover_usermapping.approverunkid = hrassessor_master.assessormasterunkid AND hrapprover_usermapping.usertypeid = " & enUserType.Assessor & _
                   "        LEFT JOIN hremployee_master assessor ON assessor.employeeunkid = hrassessor_master.employeeunkid " & _
                   "    WHERE  hrassess_empfield1_master.isvoid = 0  AND hrassess_empfield1_master.periodunkid = @periodunkid AND statustypeid = @status "

            If xOperationType = enObjective_Status.FINAL_SAVE Then
                strQ &= " AND hrassess_empfield1_master.isfinal = 1 "
            End If

            If strAdvFilter.Trim.Length > 0 Then
                strQ &= " AND " & strAdvFilter
            End If

            strQ &= ") AS GEL "

            strQ &= "GROUP by " & _
                    "ischeck, employeeunkid, Employee, statustypeid, assessormasterunkid, " & _
                    "assessorEmpid, AssessorName, mappinguserid "

            objDataOperation.ClearParameters()
            objDataOperation.AddParameter("@periodunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, xPeriodID)
            objDataOperation.AddParameter("@status", SqlDbType.Int, eZeeDataType.INT_SIZE, xOperationType)
            'objDataOperation.AddParameter("@userunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, xUserID) 'S.SANDEEP [27-SEP-2017]-- START {REF-ID # 75} -- END

            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: GetGlobalEmployeeeList; Module Name: " & mstrModuleName)
        End Try
        Return dsList
    End Function
    'S.SANDEEP |05-MAY-2019| -- END

    'Pinkal (19-Sep-2016) -- End


    'Shani(24-JAN-2017) -- Start
    'Enhancement : 
    Public Function GetAccomplishemtn_DisplayList(ByVal iEmployeeId As Integer, _
                                                  ByVal iPeriodId As Integer, _
                                                  ByVal iStatusId As Integer, _
                                                  Optional ByVal iIncludeAllGoal As Boolean = False, _
                                                  Optional ByVal iList As String = "List", _
                                                  Optional ByVal strFmtCurrency As String = "") As DataSet 'S.SANDEEP |24-JUL-2019| -- START {strFmtCurrency} -- END
        Dim StrQ As String = ""
        Dim exForce As Exception
        Dim dsList As New DataSet
        Dim strMappeedTableName As String = ""
        Dim strMappedFiledName As String = ""
        objDataOperation = New clsDataOperation
        Try
            'S.SANDEEP |12-FEB-2019| -- START
            'ISSUE/ENHANCEMENT : {Performance Assessment Changes}
            'If iStatusId <= 0 Then iStatusId = clsassess_empupdate_tran.enGoalAccomplished_Status.GA_Pending
            'S.SANDEEP |12-FEB-2019| -- END

            Dim intLinkedFld As Integer = (New clsAssess_Field_Mapping).Get_Map_FieldId(iPeriodId)

            Select Case intLinkedFld
                Case enWeight_Types.WEIGHT_FIELD1
                    strMappedFiledName = "empfield1unkid"
                    strMappeedTableName = "hrassess_empfield1_master"
                Case enWeight_Types.WEIGHT_FIELD2
                    strMappedFiledName = "empfield2unkid"
                    strMappeedTableName = "hrassess_empfield2_master"
                Case enWeight_Types.WEIGHT_FIELD3
                    strMappedFiledName = "empfield3unkid"
                    strMappeedTableName = "hrassess_empfield3_master"
                Case enWeight_Types.WEIGHT_FIELD4
                    strMappedFiledName = "empfield4unkid"
                    strMappeedTableName = "hrassess_empfield4_master"
                Case enWeight_Types.WEIGHT_FIELD5
                    strMappedFiledName = "empfield5unkid"
                    strMappeedTableName = "hrassess_empfield5_master"
            End Select

            'S.SANDEEP |24-JUL-2019| -- START
            'ISSUE/ENHANCEMENT : UPDATE PROGRESS APPROVAL SCREEN CHANGES
            'StrQ = "SELECT " & _
            '       "     ischeck " & _
            '       "    ,field_data " & _
            '       "    ,Goal_status " & _
            '       "    ,per_comp " & _
            '       "    ,org_per_comp " & _
            '       "    ,Goal_Remark " & _
            '       "    ,accomplished_status " & _
            '       "    ,IsGrp " & _
            '       "    ,empfieldunkid " & _
            '       "    ,empupdatetranunkid " & _
            '       "    ,Accomplished_statusId " & _
            '       "    ,B.fieldcaption " & _
            '       "FROM " & _
            '       "( " & _
            '       "    SELECT DISTINCT " & _
            '       "         CAST(0 AS BIT) AS ischeck " & _
            '       "        ,field_data " & _
            '       "        ,'' AS Goal_status " & _
            '       "        ,-1 AS per_comp " & _
            '       "        ,-1 AS org_per_comp " & _
            '       "        ,'' AS Goal_Remark " & _
            '       "        ,'' AS accomplished_status " & _
            '       "        ,1 AS IsGrp " & _
            '       "        ," & strMappedFiledName & " AS empfieldunkid " & _
            '       "        ,0 AS empupdatetranunkid " & _
            '       "        ,-1 AS Accomplished_statusId " & _
            '       "        ," & strMappeedTableName & ".periodunkid " & _
            '       "    FROM " & strMappeedTableName & " " & _
            '       "        LEFT JOIN hrassess_empupdate_tran ON " & strMappeedTableName & "." & strMappedFiledName & " = hrassess_empupdate_tran.empfieldunkid AND hrassess_empupdate_tran.isvoid = 0 " & _
            '       "    WHERE " & strMappeedTableName & ".isvoid = 0 " & _
            '       "        AND " & strMappeedTableName & ".employeeunkid = '" & iEmployeeId & "' " & _
            '       "        AND " & strMappeedTableName & ".periodunkid = '" & iPeriodId & "' "
            'If iIncludeAllGoal = False Then
            '    StrQ &= "   AND " & strMappedFiledName & " IN (SELECT DISTINCT empfieldunkid FROM hrassess_empupdate_tran where isvoid = 0 AND employeeunkid = '" & iEmployeeId & "' and periodunkid = '" & iPeriodId & "' "
            '    StrQ &= " ) "
            '    If iStatusId > 0 Then StrQ &= " AND hrassess_empupdate_tran.approvalstatusunkid = '" & iStatusId & "' "
            'End If

            'StrQ &= "    UNION ALL " & _
            '        "    SELECT " & _
            '        "        CAST(0 AS BIT) AS ischeck " & _
            '        "        ,'' AS field_data " & _
            '        "        ,CASE hrassess_empupdate_tran.statusunkid " & _
            '        "            WHEN " & enCompGoalStatus.ST_PENDING & " THEN @ST_PENDING " & _
            '        "            WHEN " & enCompGoalStatus.ST_INPROGRESS & " THEN @ST_INPROGRESS " & _
            '        "            WHEN " & enCompGoalStatus.ST_COMPLETE & " THEN @ST_COMPLETE " & _
            '        "            WHEN " & enCompGoalStatus.ST_CLOSED & " THEN @ST_CLOSED " & _
            '        "            WHEN " & enCompGoalStatus.ST_ONTRACK & " THEN @ST_ONTRACK " & _
            '        "            WHEN " & enCompGoalStatus.ST_ATRISK & " THEN @ST_ATRISK " & _
            '        "            WHEN " & enCompGoalStatus.ST_NOTAPPLICABLE & " THEN @ST_NOTAPPLICABLE " & _
            '        "         END AS Goal_status " & _
            '        "        ,CAST(hrassess_empupdate_tran.pct_completed AS DECIMAL(36,2)) AS per_comp " & _
            '        "        ,CAST(hrassess_empupdate_tran.pct_completed AS DECIMAL(36,2)) AS org_per_comp " & _
            '        "        ,hrassess_empupdate_tran.remark AS Goal_Remark " & _
            '        "        ,CASE hrassess_empupdate_tran.approvalstatusunkid " & _
            '        "            WHEN " & clsassess_empupdate_tran.enGoalAccomplished_Status.GA_Pending & " THEN @GA_Pending " & _
            '        "            WHEN " & clsassess_empupdate_tran.enGoalAccomplished_Status.GA_Approved & " THEN @GA_Approved " & _
            '        "            WHEN " & clsassess_empupdate_tran.enGoalAccomplished_Status.GA_Rejected & " THEN @GA_Rejected " & _
            '        "         END AS accomplished_status " & _
            '        "        ,0 AS IsGrp " & _
            '        "        ,hrassess_empupdate_tran.empfieldunkid " & _
            '        "        ,hrassess_empupdate_tran.empupdatetranunkid " & _
            '        "        ,hrassess_empupdate_tran.approvalstatusunkid AS Accomplished_statusId " & _
            '        "        ," & strMappeedTableName & ".periodunkid " & _
            '        "    FROM " & strMappeedTableName & " " & _
            '        "        LEFT JOIN hrassess_empupdate_tran ON " & strMappeedTableName & "." & strMappedFiledName & " = hrassess_empupdate_tran.empfieldunkid AND hrassess_empupdate_tran.isvoid = 0 " & _
            '        "    WHERE " & strMappeedTableName & ".isvoid = 0 " & _
            '        "        AND hrassess_empupdate_tran.isvoid = 0 " & _
            '        "        AND " & strMappeedTableName & ".employeeunkid = '" & iEmployeeId & "' " & _
            '        "        AND " & strMappeedTableName & ".periodunkid = '" & iPeriodId & "' "


            StrQ = "SELECT " & _
                   "     ischeck " & _
                   "    ,field_data " & _
                   "    ,Goal_status " & _
                   "    ,per_comp " & _
                   "    ,org_per_comp " & _
                   "    ,Goal_Remark " & _
                   "    ,accomplished_status " & _
                   "    ,IsGrp " & _
                   "    ,empfieldunkid " & _
                   "    ,empupdatetranunkid " & _
                   "    ,Accomplished_statusId " & _
                   "    ,B.fieldcaption " & _
                   "    ,odate " & _
                   "    ,ddate " & _
                   "    ,changebyid " & _
                   "    ,finalvalue " & _
                   "    ,dfinalvalue " & _
                   "    ,uom " & _
                   "    ,changeby " & _
                   "    ,ofinalvalue " & _
                   "FROM " & _
                   "( " & _
                   "    SELECT DISTINCT " & _
                   "         CAST(0 AS BIT) AS ischeck " & _
                   "        ,field_data " & _
                   "        ,'' AS Goal_status " & _
                   "        ,-1 AS per_comp " & _
                   "        ,-1 AS org_per_comp " & _
                   "        ,'' AS Goal_Remark " & _
                   "        ,'' AS accomplished_status " & _
                   "        ,1 AS IsGrp " & _
                   "        ," & strMappedFiledName & " AS empfieldunkid " & _
                   "        ,0 AS empupdatetranunkid " & _
                   "        ,-1 AS Accomplished_statusId " & _
                   "        ," & strMappeedTableName & ".periodunkid " & _
                   "        ,'' AS odate " & _
                   "        ,'' AS ddate " & _
                   "        ,0 AS changebyid " & _
                   "        ,0 AS finalvalue " & _
                   "        ,'' AS dfinalvalue " & _
                   "        ,'' AS uom " & _
                   "        ,'' AS changeby " & _
                   "        ,0 AS ofinalvalue " & _
                   "    FROM " & strMappeedTableName & " WITH (NOLOCK) " & _
                   "        LEFT JOIN hrassess_empupdate_tran WITH (NOLOCK) ON " & strMappeedTableName & "." & strMappedFiledName & " = hrassess_empupdate_tran.empfieldunkid AND hrassess_empupdate_tran.isvoid = 0 " & _
                   "    WHERE " & strMappeedTableName & ".isvoid = 0 " & _
                   "        AND " & strMappeedTableName & ".employeeunkid = '" & iEmployeeId & "' " & _
                   "        AND " & strMappeedTableName & ".periodunkid = '" & iPeriodId & "' "
            If iIncludeAllGoal = False Then
                StrQ &= "   AND " & strMappedFiledName & " IN (SELECT DISTINCT empfieldunkid FROM hrassess_empupdate_tran WITH (NOLOCK) where isvoid = 0 AND employeeunkid = '" & iEmployeeId & "' and periodunkid = '" & iPeriodId & "' "
                StrQ &= " ) "
                If iStatusId > 0 Then StrQ &= " AND hrassess_empupdate_tran.approvalstatusunkid = '" & iStatusId & "' "
            End If

            StrQ &= "    UNION ALL " & _
                    "    SELECT " & _
                    "        CAST(0 AS BIT) AS ischeck " & _
                    "        ,'' AS field_data " & _
                    "        ,CASE hrassess_empupdate_tran.statusunkid " & _
                    "            WHEN " & enCompGoalStatus.ST_PENDING & " THEN @ST_PENDING " & _
                    "            WHEN " & enCompGoalStatus.ST_INPROGRESS & " THEN @ST_INPROGRESS " & _
                    "            WHEN " & enCompGoalStatus.ST_COMPLETE & " THEN @ST_COMPLETE " & _
                    "            WHEN " & enCompGoalStatus.ST_CLOSED & " THEN @ST_CLOSED " & _
                    "            WHEN " & enCompGoalStatus.ST_ONTRACK & " THEN @ST_ONTRACK " & _
                    "            WHEN " & enCompGoalStatus.ST_ATRISK & " THEN @ST_ATRISK " & _
                    "            WHEN " & enCompGoalStatus.ST_NOTAPPLICABLE & " THEN @ST_NOTAPPLICABLE " & _
                    "         END AS Goal_status " & _
                    "        ,CAST(hrassess_empupdate_tran.pct_completed AS DECIMAL(36,2)) AS per_comp " & _
                    "        ,CAST(hrassess_empupdate_tran.pct_completed AS DECIMAL(36,2)) AS org_per_comp " & _
                    "        ,hrassess_empupdate_tran.remark AS Goal_Remark " & _
                    "        ,CASE hrassess_empupdate_tran.approvalstatusunkid " & _
                    "            WHEN " & clsassess_empupdate_tran.enGoalAccomplished_Status.GA_Pending & " THEN @GA_Pending " & _
                    "            WHEN " & clsassess_empupdate_tran.enGoalAccomplished_Status.GA_Approved & " THEN @GA_Approved " & _
                    "            WHEN " & clsassess_empupdate_tran.enGoalAccomplished_Status.GA_Rejected & " THEN @GA_Rejected " & _
                    "         END AS accomplished_status " & _
                    "        ,0 AS IsGrp " & _
                    "        ,ISNULL(hrassess_empupdate_tran.empfieldunkid," & strMappeedTableName & "." & strMappedFiledName & ") AS empfieldunkid" & _
                    "        ,hrassess_empupdate_tran.empupdatetranunkid " & _
                    "        ,hrassess_empupdate_tran.approvalstatusunkid AS Accomplished_statusId " & _
                    "        ," & strMappeedTableName & ".periodunkid " & _
                    "        ,CONVERT(CHAR(8),hrassess_empupdate_tran.updatedate,112) AS odate " & _
                    "        ,'' AS ddate " & _
                    "        ,hrassess_empupdate_tran.changebyid " & _
                    "        ,hrassess_empupdate_tran.finalvalue " & _
                    "        ,'' AS dfinalvalue " & _
                    "        ,ISNULL(cfcommon_master.name,'') AS uom " & _
                    "        ,CASE WHEN hrassess_empupdate_tran.changebyid = 1 THEN @P " & _
                    "              WHEN hrassess_empupdate_tran.changebyid = 2 THEN @V " & _
                    "         END AS changeby " & _
                    "        ,CASE WHEN goaltypeid = 2 AND changebyid = 1 THEN CAST(goalvalue*(finalvalue/100) AS DECIMAL(36,2)) ELSE finalvalue END AS ofinalvalue " & _
                    "    FROM " & strMappeedTableName & " WITH (NOLOCK) " & _
                    "        LEFT JOIN hrassess_empupdate_tran WITH (NOLOCK) ON " & strMappeedTableName & "." & strMappedFiledName & " = hrassess_empupdate_tran.empfieldunkid AND hrassess_empupdate_tran.isvoid = 0 " & _
                    "        LEFT JOIN cfcommon_master WITH (NOLOCK) ON cfcommon_master.masterunkid = " & strMappeedTableName & ".uomtypeid " & _
                    "    WHERE " & strMappeedTableName & ".isvoid = 0 " & _
                    "        AND hrassess_empupdate_tran.isvoid = 0 " & _
                    "        AND " & strMappeedTableName & ".employeeunkid = '" & iEmployeeId & "' " & _
                    "        AND " & strMappeedTableName & ".periodunkid = '" & iPeriodId & "' "
            'S.SANDEEP |24-JUL-2019| -- END



            If iStatusId > 0 Then StrQ &= " AND hrassess_empupdate_tran.approvalstatusunkid = '" & iStatusId & "' "

            StrQ &= ") AS A " & _
                    "LEFT JOIN " & _
                    "(" & _
                    "   SELECT DISTINCT TOP 1 " & _
                    "        fieldcaption " & _
                    "       ,periodunkid " & _
                    "   FROM hrassess_field_master WITH (NOLOCK) " & _
                    "       JOIN hrassess_empupdate_tran WITH (NOLOCK) on empfieldtypeid = hrassess_field_master.fieldunkid " & _
                    "   WHERE isvoid = 0 AND periodunkid = '" & iPeriodId & "' " & _
                    ") AS B ON B.periodunkid = A.periodunkid " & _
                    "ORDER BY A.empfieldunkid,A.IsGrp DESC "



            'S.SANDEEP |25-MAR-2019| -- START
            'ADDED 
            '(1). B.fieldcaption
            '(2). hrassess_empfield1_master.periodunkid
            '(3). "LEFT JOIN " & _
            '     "(" & _
            '     "   SELECT DISTINCT " & _
            '     "        fieldcaption " & _
            '     "       ,periodunkid " & _
            '     "   FROM hrassess_field_master " & _
            '     "       JOIN hrassess_empupdate_tran on empfieldtypeid = hrassess_field_master.fieldunkid " & _
            '     "   WHERE isvoid = 0 AND periodunkid = '" & iPeriodId & "' " & _
            '     ") AS B ON B.periodunkid = A.periodunkid " & _
            'S.SANDEEP |25-MAR-2019| -- END


            'S.SANDEEP |12-FEB-2019| -- START
            'ISSUE/ENHANCEMENT : {Performance Assessment Changes}
            'objDataOperation.AddParameter("@ST_PENDING", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage("clsMasterData", 518, "Pending"))
            'objDataOperation.AddParameter("@ST_INPROGRESS", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage("clsMasterData", 519, "In progress"))
            'objDataOperation.AddParameter("@ST_COMPLETE", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage("clsMasterData", 520, "Complete"))
            'objDataOperation.AddParameter("@ST_CLOSED", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage("clsMasterData", 521, "Closed"))
            'objDataOperation.AddParameter("@ST_ONTRACK", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage("clsMasterData", 522, "On Track"))
            'objDataOperation.AddParameter("@ST_ATRISK", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage("clsMasterData", 523, "At Risk"))
            'objDataOperation.AddParameter("@ST_NOTAPPLICABLE", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage("clsMasterData", 524, "Not Applicable"))
            ''S.SANDEEP [15-Feb-2018] -- START
            ''objDataOperation.AddParameter("@GA_Pending", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage("clsMasterData", 757, "Pending"))
            ''objDataOperation.AddParameter("@GA_Approved", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage("clsMasterData", 758, "Approved"))
            ''objDataOperation.AddParameter("@GA_Rejected", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage("clsMasterData", 759, "Disapprove"))

            'objDataOperation.AddParameter("@GA_Pending", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage("clsMasterData", 759, "Pending"))
            'objDataOperation.AddParameter("@GA_Approved", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage("clsMasterData", 760, "Approved"))
            'objDataOperation.AddParameter("@GA_Rejected", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage("clsMasterData", 761, "Disapprove"))
            ''S.SANDEEP [15-Feb-2018] -- END            

            objDataOperation.AddParameter("@ST_PENDING", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 579, "Pending"))
            objDataOperation.AddParameter("@ST_INPROGRESS", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 580, "In progress"))
            objDataOperation.AddParameter("@ST_COMPLETE", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 581, "Complete"))
            objDataOperation.AddParameter("@ST_CLOSED", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 582, "Closed"))
            objDataOperation.AddParameter("@ST_ONTRACK", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 583, "On Track"))
            objDataOperation.AddParameter("@ST_ATRISK", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 584, "At Risk"))
            objDataOperation.AddParameter("@ST_NOTAPPLICABLE", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 585, "Not Applicable"))
            objDataOperation.AddParameter("@ST_POSTPONED", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 748, "Postponed"))
            objDataOperation.AddParameter("@ST_BEHIND_SCHEDULE", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 749, "Behind Schedule"))
            objDataOperation.AddParameter("@ST_AHEAD_SCHEDULE", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 750, "Ahead of Schedule"))
            objDataOperation.AddParameter("@GA_Pending", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage("clsMasterData", 759, "Pending"))
            objDataOperation.AddParameter("@GA_Approved", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage("clsMasterData", 760, "Approved"))
            objDataOperation.AddParameter("@GA_Rejected", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage("clsMasterData", 761, "Disapprove"))
            'S.SANDEEP |12-FEB-2019| -- END

            'S.SANDEEP |24-JUL-2019| -- START
            'ISSUE/ENHANCEMENT : UPDATE PROGRESS APPROVAL SCREEN CHANGES
            objDataOperation.AddParameter("@P", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage("frmUpdateFieldValue", 101, "Percentage"))
            objDataOperation.AddParameter("@V", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage("frmUpdateFieldValue", 102, "Value"))
            'S.SANDEEP |24-JUL-2019| -- END

            dsList = objDataOperation.ExecQuery(StrQ, iList)

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & " : " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            'S.SANDEEP |24-JUL-2019| -- START
            'ISSUE/ENHANCEMENT : UPDATE PROGRESS APPROVAL SCREEN CHANGES
            For Each row As DataRow In dsList.Tables(0).Rows
                If CBool(row("IsGrp")) = False Then
                    row.Item("ddate") = eZeeDate.convertDate(row.Item("odate").ToString).ToShortDateString
                    row.Item("dfinalvalue") = Format(CDec(row("ofinalvalue")), strFmtCurrency) & " " & row("uom").ToString()
                End If
            Next
            'S.SANDEEP |24-JUL-2019| -- END

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "GetDisplayList", mstrModuleName)
        Finally
        End Try
        Return dsList
    End Function
    'Shani(24-JAN-2017) -- End


    'S.SANDEEP [07-OCT-2017] -- START
    'ISSUE/ENHANCEMENT : IMPORT GOALS
    Public Function GetFieldDataId(ByVal strData As String, ByVal intPeriodId As Integer, ByVal intEmployeeId As Integer, ByVal intFieldTypeId As enWeight_Types, ByVal intRefFieldId As Integer) As Integer
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception
        Dim objDataOperation As New clsDataOperation
        Try
            Dim strTabName, strColName, strParentColName As String
            strTabName = "" : strColName = "" : strParentColName = ""
            Select Case intFieldTypeId
                Case enWeight_Types.WEIGHT_FIELD1
                    strColName = "empfield1unkid" : strTabName = "hrassess_empfield1_master"
                Case enWeight_Types.WEIGHT_FIELD2
                    strParentColName = "empfield1unkid"
                    strColName = "empfield2unkid" : strTabName = "hrassess_empfield2_master"
                Case enWeight_Types.WEIGHT_FIELD3
                    strParentColName = "empfield2unkid"
                    strColName = "empfield3unkid" : strTabName = "hrassess_empfield3_master"
                Case enWeight_Types.WEIGHT_FIELD4
                    strParentColName = "empfield3unkid"
                    strColName = "empfield4unkid" : strTabName = "hrassess_empfield4_master"
                Case enWeight_Types.WEIGHT_FIELD5
                    strParentColName = "empfield4unkid"
                    strColName = "empfield5unkid" : strTabName = "hrassess_empfield5_master"
            End Select

            strQ = "SELECT " & _
                   strColName & " " & _
                   "FROM " & strTabName & " WITH (NOLOCK) " & _
                   "WHERE LTRIM(RTRIM(field_data)) = LTRIM(RTRIM(@field_data)) AND isvoid = 0 " & _
                   " AND employeeunkid = @employeeunkid AND periodunkid = @periodunkid "

            If strParentColName.Trim.Length > 0 Then
                strQ &= " AND " & strParentColName & " = @prefid "
                objDataOperation.AddParameter("@prefid", SqlDbType.Int, eZeeDataType.INT_SIZE, intRefFieldId)
            End If

            objDataOperation.AddParameter("@field_data", SqlDbType.NVarChar, strData.Length, strData)
            objDataOperation.AddParameter("@employeeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intEmployeeId)
            objDataOperation.AddParameter("@periodunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intPeriodId)

            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            For Each dt As DataRow In dsList.Tables(0).Rows
                Return dt(strColName)
            Next
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: GetPerspectiveId; Module Name: " & mstrModuleName)
        Finally
        End Try
    End Function
    'S.SANDEEP [07-OCT-2017] -- END

    'S.SANDEEP |24-JUL-2019| -- START
    'ISSUE/ENHANCEMENT : UPDATE PROGRESS APPROVAL SCREEN CHANGES
    Public Function GetNewAccomplishemtn_DisplayList(ByVal iEmployeeId As Integer, _
                                                     ByVal iPeriodId As Integer, _
                                                     ByVal iStatusId As Integer, _
                                                     ByVal iCascadingTypeId As Integer, _
                                                     Optional ByVal iIncludeAllGoal As Boolean = False, _
                                                     Optional ByVal iList As String = "List", _
                                                     Optional ByVal strFmtCurrency As String = "", _
                                                     Optional ByVal intPerspectiveId As Integer = 0, _
                                                     Optional ByVal intFieldTranId As Integer = 0) As DataSet 'S.SANDEEP |29-JUL-2019| -- START {intPerspectiveId,intFieldTranId} -- END
        Dim StrQ As String = ""
        Dim exForce As Exception
        Dim dsList As New DataSet
        Dim strMappeedTableName As String = ""
        Dim strMappedFiledName As String = ""
        objDataOperation = New clsDataOperation
        Try
            'S.SANDEEP |29-JUL-2019| -- START
            'ISSUE/ENHANCEMENT : UPDATE PROGRESS APPROVAL SCREEN CHANGES
            Dim strFilter As String = String.Empty
            If intPerspectiveId > 0 Then
                strFilter &= " AND hrassess_perspective_master.perspectiveunkid = '" & intPerspectiveId & "' "
            End If
            'S.SANDEEP |29-JUL-2019| -- END

            Dim intLinkedFld As Integer = (New clsAssess_Field_Mapping).Get_Map_FieldId(iPeriodId)
            Select Case intLinkedFld
                Case enWeight_Types.WEIGHT_FIELD1
                    strMappedFiledName = "empfield1unkid"
                    strMappeedTableName = "hrassess_empfield1_master"
                    If intFieldTranId > 0 Then strFilter &= " AND hrassess_empfield1_master.empfield1unkid = '" & intFieldTranId & "' " 'S.SANDEEP |29-JUL-2019| -- START {intFieldTranId} -- END

                Case enWeight_Types.WEIGHT_FIELD2
                    strMappedFiledName = "empfield2unkid"
                    strMappeedTableName = "hrassess_empfield2_master"
                    If intFieldTranId > 0 Then strFilter &= " AND hrassess_empfield2_master.empfield2unkid = '" & intFieldTranId & "' " 'S.SANDEEP |29-JUL-2019| -- START {intFieldTranId} -- END
                Case enWeight_Types.WEIGHT_FIELD3
                    strMappedFiledName = "empfield3unkid"
                    strMappeedTableName = "hrassess_empfield3_master"
                    If intFieldTranId > 0 Then strFilter &= " AND hrassess_empfield3_master.empfield3unkid = '" & intFieldTranId & "' " 'S.SANDEEP |29-JUL-2019| -- START {intFieldTranId} -- END
                Case enWeight_Types.WEIGHT_FIELD4
                    strMappedFiledName = "empfield4unkid"
                    strMappeedTableName = "hrassess_empfield4_master"
                    If intFieldTranId > 0 Then strFilter &= " AND hrassess_empfield4_master.empfield4unkid = '" & intFieldTranId & "' " 'S.SANDEEP |29-JUL-2019| -- START {intFieldTranId} -- END
                Case enWeight_Types.WEIGHT_FIELD5
                    strMappedFiledName = "empfield5unkid"
                    strMappeedTableName = "hrassess_empfield5_master"
                    If intFieldTranId > 0 Then strFilter &= " AND hrassess_empfield5_master.empfield5unkid = '" & intFieldTranId & "' " 'S.SANDEEP |29-JUL-2019| -- START {intFieldTranId} -- END
            End Select

            StrQ = "SELECT " & _
                   "      ischeck " & _
                   "     ,pName " & _
                   "     ,A.field_data " & _
                   "     ,ISNULL(C.cfieldunkid,0) AS cfieldunkid " & _
                   "     ,ISNULL(C.cFCaption,'') AS cFCaption " & _
                   "     ,ISNULL(C.cData,'') AS cData " & _
                   "     ,ISNULL(D.dfieldunkid,0) AS dfieldunkid " & _
                   "     ,ISNULL(D.dFCaption,'') AS dFCaption " & _
                   "     ,ISNULL(D.dData,'') AS dData " & _
                   "     ,ISNULL(E.efieldunkid,0) AS efieldunkid " & _
                   "     ,ISNULL(E.eFCaption,'') AS eFCaption " & _
                   "     ,ISNULL(E.eData,'') AS eData " & _
                   "     ,Goal_status " & _
                   "     ,per_comp " & _
                   "     ,org_per_comp " & _
                   "     ,Goal_Remark " & _
                   "     ,accomplished_status " & _
                   "     ,A.empfieldunkid " & _
                   "     ,empupdatetranunkid " & _
                   "     ,Accomplished_statusId " & _
                   "     ,B.fieldcaption " & _
                   "     ,odate " & _
                   "     ,ddate " & _
                   "     ,changebyid " & _
                   "     ,finalvalue " & _
                   "     ,dfinalvalue " & _
                   "     ,uom " & _
                   "     ,changeby " & _
                   "     ,ofinalvalue " & _
                   "     ,goalvalue " & _
                   "     ,dgoalvalue " & _
                   "     ,perspectiveunkid " & _
                   "     ,@pCaption AS pCaption " & _
                   "     ,empfieldtypeid " & _
                   "     ,approval_remark " & _
                   "FROM " & _
                   "( " & _
                   "    SELECT " & _
                   "         CAST(0 AS BIT) AS ischeck " & _
                   "        ," & strMappeedTableName & ".field_data AS field_data " & _
                   "        ,hrassess_perspective_master.name AS pName " & _
                   "        ,CASE hrassess_empupdate_tran.statusunkid " & _
                   "            WHEN " & enCompGoalStatus.ST_PENDING & " THEN @ST_PENDING " & _
                   "            WHEN " & enCompGoalStatus.ST_INPROGRESS & " THEN @ST_INPROGRESS " & _
                   "            WHEN " & enCompGoalStatus.ST_COMPLETE & " THEN @ST_COMPLETE " & _
                   "            WHEN " & enCompGoalStatus.ST_CLOSED & " THEN @ST_CLOSED " & _
                   "            WHEN " & enCompGoalStatus.ST_ONTRACK & " THEN @ST_ONTRACK " & _
                   "            WHEN " & enCompGoalStatus.ST_ATRISK & " THEN @ST_ATRISK " & _
                   "            WHEN " & enCompGoalStatus.ST_NOTAPPLICABLE & " THEN @ST_NOTAPPLICABLE " & _
                   "         END AS Goal_status " & _
                   "        ,CAST(hrassess_empupdate_tran.pct_completed AS DECIMAL(36,2)) AS per_comp " & _
                   "        ,CAST(hrassess_empupdate_tran.pct_completed AS DECIMAL(36,2)) AS org_per_comp " & _
                   "        ,hrassess_empupdate_tran.remark AS Goal_Remark " & _
                   "        ,CASE hrassess_empupdate_tran.approvalstatusunkid " & _
                   "            WHEN " & clsassess_empupdate_tran.enGoalAccomplished_Status.GA_Pending & " THEN @GA_Pending " & _
                   "            WHEN " & clsassess_empupdate_tran.enGoalAccomplished_Status.GA_Approved & " THEN @GA_Approved " & _
                   "            WHEN " & clsassess_empupdate_tran.enGoalAccomplished_Status.GA_Rejected & " THEN @GA_Rejected " & _
                   "         END AS accomplished_status " & _
                   "        ,0 AS IsGrp " & _
                   "        ,ISNULL(hrassess_empupdate_tran.empfieldunkid," & strMappeedTableName & "." & strMappedFiledName & ") AS empfieldunkid " & _
                   "        ,hrassess_empupdate_tran.empupdatetranunkid " & _
                   "        ,hrassess_empupdate_tran.approvalstatusunkid AS Accomplished_statusId " & _
                   "        ," & strMappeedTableName & ".periodunkid " & _
                   "        ,CONVERT(CHAR(8),hrassess_empupdate_tran.updatedate,112) AS odate " & _
                   "        ,'' AS ddate " & _
                   "        ,hrassess_empupdate_tran.changebyid " & _
                   "        ,hrassess_empupdate_tran.finalvalue " & _
                   "        ,'' AS dfinalvalue " & _
                   "        ,ISNULL(cfcommon_master.name,'') AS uom " & _
                   "        ,CASE WHEN hrassess_empupdate_tran.changebyid = 1 THEN @P " & _
                   "              WHEN hrassess_empupdate_tran.changebyid = 2 THEN @V " & _
                   "         END AS changeby " & _
                   "        ,CASE WHEN " & strMappeedTableName & ".goaltypeid = 2 AND changebyid = 1 THEN CAST(" & strMappeedTableName & ".goalvalue*(finalvalue/100) AS DECIMAL(36,2)) ELSE finalvalue END AS ofinalvalue " & _
                   "        ," & strMappeedTableName & ".goalvalue " & _
                   "        ,'' AS dgoalvalue " & _
                   "        ,hrassess_perspective_master.perspectiveunkid " & _
                   "        ,ISNULL(hrassess_empupdate_tran.empfieldtypeid," & intLinkedFld & ") AS empfieldtypeid " & _
                   "        ,hrassess_empupdate_tran.approval_remark " & _
                   "    FROM " & strMappeedTableName & " WITH (NOLOCK) "

            Select Case intLinkedFld
                Case enWeight_Types.WEIGHT_FIELD2
                    StrQ &= " LEFT JOIN hrassess_empfield1_master WITH (NOLOCK) ON hrassess_empfield1_master.empfield1unkid = hrassess_empfield2_master.empfield1unkid "
                Case enWeight_Types.WEIGHT_FIELD3
                    StrQ &= " LEFT JOIN hrassess_empfield2_master WITH (NOLOCK) ON hrassess_empfield2_master.empfield2unkid = hrassess_empfield3_master.empfield2unkid " & _
                            " LEFT JOIN hrassess_empfield1_master WITH (NOLOCK) ON hrassess_empfield1_master.empfield1unkid = hrassess_empfield2_master.empfield1unkid "
                Case enWeight_Types.WEIGHT_FIELD4
                    StrQ &= " LEFT JOIN hrassess_empfield3_master WITH (NOLOCK) ON hrassess_empfield3_master.empfield3unkid = hrassess_empfield4_master.empfield3unkid " & _
                            " LEFT JOIN hrassess_empfield2_master WITH (NOLOCK) ON hrassess_empfield2_master.empfield2unkid = hrassess_empfield3_master.empfield2unkid " & _
                            " LEFT JOIN hrassess_empfield1_master WITH (NOLOCK) ON hrassess_empfield1_master.empfield1unkid = hrassess_empfield2_master.empfield1unkid "

                Case enWeight_Types.WEIGHT_FIELD5
                    StrQ &= " LEFT JOIN hrassess_empfield4_master WITH (NOLOCK) ON hrassess_empfield4_master.empfield4unkid = hrassess_empfield5_master.empfield4unkid " & _
                            " LEFT JOIN hrassess_empfield3_master WITH (NOLOCK) ON hrassess_empfield3_master.empfield3unkid = hrassess_empfield4_master.empfield3unkid " & _
                            " LEFT JOIN hrassess_empfield2_master WITH (NOLOCK) ON hrassess_empfield2_master.empfield2unkid = hrassess_empfield3_master.empfield2unkid " & _
                            " LEFT JOIN hrassess_empfield1_master WITH (NOLOCK) ON hrassess_empfield1_master.empfield1unkid = hrassess_empfield2_master.empfield1unkid "

            End Select

            If iCascadingTypeId <> enPACascading.NEITHER_CASCADING_NOR_ALIGNMENT Then
                StrQ &= " LEFT JOIN hrassess_owrfield1_master WITH (NOLOCK) ON hrassess_owrfield1_master.owrfield1unkid = hrassess_empfield1_master.owrfield1unkid AND hrassess_owrfield1_master.isvoid = 0 " & _
                        " LEFT JOIN hrassess_coyfield1_master WITH (NOLOCK) ON hrassess_coyfield1_master.coyfield1unkid = hrassess_owrfield1_master.coyfield1unkid AND hrassess_coyfield1_master.isvoid = 0 " & _
                        " LEFT JOIN hrassess_perspective_master WITH (NOLOCK) ON hrassess_perspective_master.perspectiveunkid = hrassess_coyfield1_master.perspectiveunkid AND hrassess_perspective_master.isactive = 1 "
            Else
                StrQ &= " LEFT JOIN hrassess_perspective_master WITH (NOLOCK) ON hrassess_perspective_master.perspectiveunkid = hrassess_empfield1_master.perspectiveunkid AND hrassess_perspective_master.isactive = 1 "
            End If

            StrQ &= "        LEFT JOIN hrassess_empupdate_tran WITH (NOLOCK) ON " & strMappeedTableName & "." & strMappedFiledName & " = hrassess_empupdate_tran.empfieldunkid AND hrassess_empupdate_tran.isvoid = 0 AND hrassess_empupdate_tran.employeeunkid = '" & iEmployeeId & "' AND hrassess_empupdate_tran.periodunkid = '" & iPeriodId & "' AND hrassess_empupdate_tran.isvoid = 0  " & _
                    "        LEFT JOIN cfcommon_master WITH (NOLOCK) ON cfcommon_master.masterunkid = " & strMappeedTableName & ".uomtypeid " & _
                    "    WHERE " & strMappeedTableName & ".isvoid = 0 " & _
                    "        " & _
                    "        AND " & strMappeedTableName & ".employeeunkid = '" & iEmployeeId & "' " & _
                    "        AND " & strMappeedTableName & ".periodunkid = '" & iPeriodId & "' "

            'S.SANDEEP |04-SEP-2019| -- START {AND hrassess_empupdate_tran.employeeunkid = '" & iEmployeeId & "' AND hrassess_empupdate_tran.periodunkid = '" & iPeriodId & "'} -- END
            If iStatusId > 0 Then StrQ &= " AND hrassess_empupdate_tran.approvalstatusunkid = '" & iStatusId & "' "

            'S.SANDEEP |29-JUL-2019| -- START
            'ISSUE/ENHANCEMENT : UPDATE PROGRESS APPROVAL SCREEN CHANGES
            If strFilter.Trim.Length > 0 Then StrQ &= strFilter
            'S.SANDEEP |29-JUL-2019| -- END


            StrQ &= ") AS A " & _
                    "LEFT JOIN " & _
                    "(" & _
                    "   SELECT DISTINCT TOP 1 " & _
                    "        fieldcaption " & _
                    "       ,periodunkid " & _
                    "   FROM hrassess_field_master WITH (NOLOCK) " & _
                    "       JOIN hrassess_empupdate_tran WITH (NOLOCK) on empfieldtypeid = hrassess_field_master.fieldunkid " & _
                    "   WHERE isvoid = 0 AND periodunkid = '" & iPeriodId & "' " & _
                    ") AS B ON B.periodunkid = A.periodunkid " & _
                    "LEFT JOIN " & _
                    "( " & _
                    "   SELECT DISTINCT " & _
                    "        hrassess_field_master.fieldcaption AS cFCaption " & _
                    "       ,hrassess_empupdate_tran.periodunkid " & _
                    "       ,hrassess_empinfofield_tran.field_data AS cData " & _
                    "       ,hrassess_empinfofield_tran.empfieldunkid " & _
                    "       ,hrassess_field_master.fieldunkid AS cfieldunkid " & _
                    "   FROM hrassess_field_master WITH (NOLOCK) " & _
                    "       JOIN hrassess_empinfofield_tran WITH (NOLOCK) ON hrassess_empinfofield_tran.fieldunkid = hrassess_field_master.fieldunkid " & _
                    "       JOIN hrassess_empupdate_tran WITH (NOLOCK) ON hrassess_empupdate_tran.empfieldunkid = hrassess_empinfofield_tran.empfieldunkid " & _
                    "   WHERE isvoid = 0 AND isinformational = 1 AND hrassess_empinfofield_tran.fieldunkid = 6 AND isused = 1 " & _
                    "   AND periodunkid = '" & iPeriodId & "' AND hrassess_empupdate_tran.employeeunkid = '" & iEmployeeId & "' " & _
                    ") AS C ON A.periodunkid = C.periodunkid AND A.empfieldunkid = C.empfieldunkid " & _
                    "LEFT JOIN " & _
                    "( " & _
                    "   SELECT DISTINCT " & _
                    "        hrassess_field_master.fieldcaption AS dFCaption " & _
                    "       ,hrassess_empupdate_tran.periodunkid " & _
                    "       ,hrassess_empinfofield_tran.field_data AS dData " & _
                    "       ,hrassess_empinfofield_tran.empfieldunkid " & _
                    "       ,hrassess_field_master.fieldunkid AS dfieldunkid " & _
                    "   FROM hrassess_field_master WITH (NOLOCK) " & _
                    "       JOIN hrassess_empinfofield_tran WITH (NOLOCK) ON hrassess_empinfofield_tran.fieldunkid = hrassess_field_master.fieldunkid " & _
                    "       JOIN hrassess_empupdate_tran WITH (NOLOCK) ON hrassess_empupdate_tran.empfieldunkid = hrassess_empinfofield_tran.empfieldunkid " & _
                    "   WHERE isvoid = 0 AND isinformational = 1 AND hrassess_empinfofield_tran.fieldunkid = 7 AND isused = 1 " & _
                    "   AND periodunkid = '" & iPeriodId & "' AND hrassess_empupdate_tran.employeeunkid = '" & iEmployeeId & "' " & _
                    ") AS D ON A.periodunkid = D.periodunkid AND A.empfieldunkid = D.empfieldunkid " & _
                    "LEFT JOIN " & _
                    "( " & _
                    "   SELECT DISTINCT " & _
                    "        hrassess_field_master.fieldcaption AS eFCaption " & _
                    "       ,hrassess_empupdate_tran.periodunkid " & _
                    "       ,hrassess_empinfofield_tran.field_data AS eData " & _
                    "       ,hrassess_empinfofield_tran.empfieldunkid " & _
                    "       ,hrassess_field_master.fieldunkid AS efieldunkid " & _
                    "   FROM hrassess_field_master WITH (NOLOCK) " & _
                    "       JOIN hrassess_empinfofield_tran WITH (NOLOCK) ON hrassess_empinfofield_tran.fieldunkid = hrassess_field_master.fieldunkid " & _
                    "       JOIN hrassess_empupdate_tran WITH (NOLOCK) ON hrassess_empupdate_tran.empfieldunkid = hrassess_empinfofield_tran.empfieldunkid " & _
                    "   WHERE isvoid = 0 AND isinformational = 1 AND hrassess_empinfofield_tran.fieldunkid = 8 AND isused = 1 " & _
                    "   AND periodunkid = '" & iPeriodId & "' AND hrassess_empupdate_tran.employeeunkid = '" & iEmployeeId & "' " & _
                    ") AS E ON A.periodunkid = E.periodunkid AND A.empfieldunkid = E.empfieldunkid " & _
                    "ORDER BY A.perspectiveunkid "

            objDataOperation.AddParameter("@ST_PENDING", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 579, "Pending"))
            objDataOperation.AddParameter("@ST_INPROGRESS", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 580, "In progress"))
            objDataOperation.AddParameter("@ST_COMPLETE", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 581, "Complete"))
            objDataOperation.AddParameter("@ST_CLOSED", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 582, "Closed"))
            objDataOperation.AddParameter("@ST_ONTRACK", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 583, "On Track"))
            objDataOperation.AddParameter("@ST_ATRISK", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 584, "At Risk"))
            objDataOperation.AddParameter("@ST_NOTAPPLICABLE", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 585, "Not Applicable"))
            objDataOperation.AddParameter("@ST_POSTPONED", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 748, "Postponed"))
            objDataOperation.AddParameter("@ST_BEHIND_SCHEDULE", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 749, "Behind Schedule"))
            objDataOperation.AddParameter("@ST_AHEAD_SCHEDULE", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 750, "Ahead of Schedule"))
            objDataOperation.AddParameter("@GA_Pending", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage("clsMasterData", 759, "Pending"))
            objDataOperation.AddParameter("@GA_Approved", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage("clsMasterData", 760, "Approved"))
            objDataOperation.AddParameter("@GA_Rejected", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage("clsMasterData", 761, "Disapprove"))
            objDataOperation.AddParameter("@P", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage("frmUpdateFieldValue", 101, "Percentage"))
            objDataOperation.AddParameter("@V", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage("frmUpdateFieldValue", 102, "Value"))
            objDataOperation.AddParameter("@pCaption", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 27, "Perspective"))

            dsList = objDataOperation.ExecQuery(StrQ, iList)

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & " : " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            'S.SANDEEP |04-DEC-2019| -- START
            'ISSUE/ENHANCEMENT : REPORT TEMPLATE 17 -- NMB
            '----------------- ADDED
            '-----------------> hrassess_empupdate_tran.empfieldtypeid
            '-----------------> empfieldtypeid
            '-----------------> approval_remark
            'S.SANDEEP |04-DEC-2019| -- END


            'S.SANDEEP |04-SEP-2019| -- START
            'ISSUE/ENHANCEMENT : Performance Of Process
            'For Each row As DataRow In dsList.Tables(0).Rows
            '    row.Item("ddate") = eZeeDate.convertDate(row.Item("odate").ToString).ToShortDateString
            '    row.Item("dfinalvalue") = Format(CDec(row("ofinalvalue")), strFmtCurrency) & " " & row("uom").ToString()
            '    row.Item("dgoalvalue") = Format(CDec(row("goalvalue")), strFmtCurrency) & " " & row("uom").ToString()
            'Next
            dsList.Tables(0).AsEnumerable().ToList().ForEach(Function(x) UpdateDisplayValue(x, strFmtCurrency))
            'S.SANDEEP |04-SEP-2019| -- END

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: GetNewAccomplishemtn_DisplayList; Module Name: " & mstrModuleName)
        Finally
        End Try
        Return dsList
    End Function
    'S.SANDEEP |24-JUL-2019| -- END

    'S.SANDEEP |04-SEP-2019| -- START
    'ISSUE/ENHANCEMENT : Performance Of Process
    Private Function UpdateDisplayValue(ByVal x As DataRow, ByVal strFmtCurrency As String) As Boolean
        Try
            'S.SANDEEP |04-DEC-2019| -- START
            'ISSUE/ENHANCEMENT : REPORT TEMPLATE 17 -- NMB
            'x.Item("ddate") = eZeeDate.convertDate(x.Item("odate").ToString).ToShortDateString
            'x.Item("dfinalvalue") = Format(CDec(x("ofinalvalue")), strFmtCurrency) & " " & x("uom").ToString()
            'x.Item("dgoalvalue") = Format(CDec(x("goalvalue")), strFmtCurrency) & " " & x("uom").ToString()
            If x.Item("odate").ToString.Trim.Length > 0 Then
                x.Item("ddate") = eZeeDate.convertDate(x.Item("odate").ToString).ToShortDateString
            End If
            If IsDBNull(x.Item("ofinalvalue")) = False Then
            x.Item("dfinalvalue") = Format(CDec(x("ofinalvalue")), strFmtCurrency) & " " & x("uom").ToString()
            Else
                x.Item("dfinalvalue") = Format(CDec(0), strFmtCurrency) & " " & x("uom").ToString()
            End If
            If IsDBNull(x.Item("goalvalue")) = False Then
            x.Item("dgoalvalue") = Format(CDec(x("goalvalue")), strFmtCurrency) & " " & x("uom").ToString()
            Else
                x.Item("dgoalvalue") = Format(CDec(0), strFmtCurrency) & " " & x("uom").ToString()
            End If
            'S.SANDEEP |04-DEC-2019| -- END
            Return True
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: UpdateDisplayValue; Module Name: " & mstrModuleName)
        Finally
        End Try
    End Function
    'S.SANDEEP |04-SEP-2019| -- END

    'S.SANDEEP |08-DEC-2020| -- START
    'ISSUE/ENHANCEMENT : NMB CASCADING CHANGES
    Public Function IsValidGoalOperation(ByVal intPeriodId As Integer, _
                                         ByVal dtEmpAsOnDate As DateTime, _
                                         ByVal intEmployeeId As Integer, _
                                         ByVal strDataBaseName As String) As String
        Dim StrMsg As String = ""
        Dim objEval As New clsevaluation_analysis_master
        Dim dsList As New DataSet
        Dim objGrp As New clsGroup_Master
        Try
            objGrp._Groupunkid = 1
            If objGrp._Groupname.ToUpper = "NMB PLC" Then
                Using objDataOpr As New clsDataOperation
                    dsList = objEval.GetEmployee_AssessorReviewerDetails(intPeriodId:=intPeriodId, _
                                                                         dtEmpAsOnDate:=dtEmpAsOnDate, _
                                                                         intEmployeeId:=intEmployeeId, _
                                                                         objDataOpr:=objDataOpr, _
                                                                         strDataBaseName:=strDataBaseName)
                    Dim iAssessorEmpId As Integer = 0
                    If dsList.Tables(0).Rows.Count > 0 Then
                        Dim dtmp() As DataRow = Nothing
                        If iAssessorEmpId <= 0 Then
                            dtmp = dsList.Tables(0).Select("isreviewer = 0 AND isfound = 1")
                            If dtmp.Length > 0 Then
                                iAssessorEmpId = dtmp(0).Item("employeeunkid")
                            Else
                                dtmp = dsList.Tables(0).Select("isreviewer = 0 AND isfound = 0 AND visibletypeid = 1")
                                If dtmp.Length > 0 Then
                                    iAssessorEmpId = dtmp(0).Item("employeeunkid")
                                End If
                            End If
                        End If
                    Else
                        strMSg = Language.getMessage(mstrModuleName, 600, "Sorry, you cannot do add/edit/delete operation on the goals assigned to you, As you do not have any assessor assigned to you. Please contact HR manager.")
                    End If

                    If iAssessorEmpId > 0 Then
                        Dim xLastStatusId As Integer = 0
                        xLastStatusId = GetLastStatus(intPeriodId, iAssessorEmpId)
                        If xLastStatusId <> enObjective_Status.FINAL_SAVE Then
                            strMSg = Language.getMessage(mstrModuleName, 601, "Sorry, you cannot do add/edit/delete operation on the goals assigned to you, As your assessor's goals are yet not approved.")
                End If
                End If
                End Using
            End If
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: IsValidGoalOperation; Module Name: " & mstrModuleName)
        Finally
            objEval = Nothing
            objGrp = Nothing
        End Try
        Return StrMsg
    End Function
    'S.SANDEEP |08-DEC-2020| -- END

'Pinkal (07-Dec-2020) -- Start
    'Enhancement NMB - Cascading PM UAT Comments.
    Public Sub SendNotification_TransferGoalsSubordinateEmployee(ByVal xCompanyId As Integer, ByVal xEmployeeId As Integer, Optional ByVal iLoginTypeId As Integer = 0, _
                                              Optional ByVal iLoginEmployeeId As Integer = 0, Optional ByVal iUserId As Integer = 0)
        Dim objMail As New clsSendMail
        Dim strMessage As String = ""
        Dim objGrp As New clsGroup_Master
        Dim StrQ As String = ""
        Dim objEmp As New clsEmployee_Master
        Try
            objGrp._Groupunkid = 1
            If objGrp._Groupname.ToUpper = "NMB PLC" Then
                Dim objNet As New clsNetConnectivity : If objNet._Conected = False Then Exit Sub
                Dim info1 As TextInfo = CultureInfo.InvariantCulture.TextInfo
                Dim dsLst As New DataSet
                Using objDo As New clsDataOperation
                    'StrQ = "SELECT " & _
                    '       "     E.email " & _
                    '       "    ,E.employeecode " & _
                    '       "    ,E.firstname+' ' + E.surname AS ename " & _
                    '       "FROM hrassessor_master AS M " & _
                    '       "    JOIN hrassessor_tran AS T ON M.assessormasterunkid = T.assessormasterunkid " & _
                    '       "    JOIN hremployee_master E ON E.employeeunkid = T.employeeunkid " & _
                    '       "WHERE M.employeeunkid = '" & xEmployeeId & "' AND M.isvoid = 0 AND M.visibletypeid = 1 " & _
                    '       "AND T.isvoid = 0 AND T.visibletypeid = 1 AND M.isreviewer = 0 "


                    StrQ = "SELECT " & _
                           "     E.email " & _
                           "    ,E.employeecode " & _
                           "    ,E.firstname+' ' + E.surname AS ename " & _
                           "FROM hremployee_master E " & _
                           "WHERE E.employeeunkid = '" & xEmployeeId & "' "

                    dsLst = objDo.ExecQuery(StrQ, "List")

                    If objDo.ErrorMessage <> "" Then
                        Throw New Exception(objDo.ErrorNumber & ": " & objDo.ErrorMessage)
                    End If
                End Using

                If dsLst IsNot Nothing AndAlso dsLst.Tables.Count > 0 Then
                    objEmp._Employeeunkid(Now.Date) = xEmployeeId
                    For Each row As DataRow In dsLst.Tables("List").Rows
                        If CStr(row("email")).Trim.Length > 0 Then
                    objMail._Subject = Language.getMessage(mstrModuleName, 52, "Set Your Goals")
                    strMessage = "<HTML> <BODY>"
                        strMessage &= Language.getMessage(mstrModuleName, 5, "Dear") & " " & info1.ToTitleCase(CStr(row("ename")).ToLower()) & ", <BR><BR>"
                    strMessage &= Language.getMessage(mstrModuleName, 53, "This is to notify you that your supervisor’s balance score card has been approved. You may proceed setting yours.")
                    strMessage &= "<BR><BR>"
                    strMessage &= Language.getMessage(mstrModuleName, 43, "Regards,")
                    strMessage &= "<BR>"
                    strMessage &= "<BR></BR><BR></BR><center>""POWERED BY ARUTI HR & PAYROLL MANAGEMENT SOFTWARE""</center>"
                    strMessage &= "</BODY></HTML>"
                            objMail._ToEmail = CStr(row("email"))
                    objMail._Message = strMessage
                    If iLoginTypeId <= 0 Then iLoginTypeId = enLogin_Mode.DESKTOP
                    If mstrWebFrmName.Trim.Length > 0 Then
                        objMail._Form_Name = mstrWebFrmName
                    End If
                    objMail._LogEmployeeUnkid = iLoginEmployeeId
                    objMail._OperationModeId = iLoginTypeId
                    objMail._UserUnkid = iUserId
                    objMail._SenderAddress = IIf(objEmp._Email = "", objEmp._Firstname & " " & objEmp._Surname, objEmp._Email)
                    objMail._ModuleRefId = clsSendMail.enAT_VIEW_TYPE.ASSESSMENT_MGT
                    objMail.SendMail(xCompanyId)
                        End If
                    Next
                End If ' If objEmp._Email.Trim.Length > 0 Then
            End If   ' If objGrp._Groupname.ToUpper = "NMB PLC" Then
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: SendNotification_TransferGoalsSubordinateEmployee; Module Name: " & mstrModuleName)
        Finally
            objGrp = Nothing
            objMail = Nothing
        End Try
    End Sub
    'Pinkal (07-Dec-2020) -- End


    'S.SANDEEP |27-NOV-2020| -- START
    'ISSUE/ENHANCEMENT : VALID GOAL OPERATION
    'Public Function AllowedToAddGoals(ByVal iPeriodId As Integer, ByVal iEmpolyeeId As Integer) As String
    '    Dim StrQ As String = ""
    '    Dim exForce As Exception
    '    Dim dsList As New DataSet
    '    Dim strMappeedTableName As String = ""
    '    Dim strMappedFiledName As String = ""
    '    Dim objDataOperation As New clsDataOperation
    '    Dim StrMsg As String = ""
    '    Dim objGrp As New clsGroup_Master
    '    Try
    '        objGrp._Groupunkid = 1
    '        If objGrp._Groupname.ToUpper = "NMB PLC" Then
    '            Dim intLinkedFld As Integer = (New clsAssess_Field_Mapping).Get_Map_FieldId(iPeriodId)

    '            Select Case intLinkedFld
    '                Case enWeight_Types.WEIGHT_FIELD1
    '                    strMappedFiledName = "empfield1unkid"
    '                    strMappeedTableName = "hrassess_empfield1_master"
    '                Case enWeight_Types.WEIGHT_FIELD2
    '                    strMappedFiledName = "empfield2unkid"
    '                    strMappeedTableName = "hrassess_empfield2_master"
    '                Case enWeight_Types.WEIGHT_FIELD3
    '                    strMappedFiledName = "empfield3unkid"
    '                    strMappeedTableName = "hrassess_empfield3_master"
    '                Case enWeight_Types.WEIGHT_FIELD4
    '                    strMappedFiledName = "empfield4unkid"
    '                    strMappeedTableName = "hrassess_empfield4_master"
    '                Case enWeight_Types.WEIGHT_FIELD5
    '                    strMappedFiledName = "empfield5unkid"
    '                    strMappeedTableName = "hrassess_empfield5_master"
    '            End Select

    '            StrQ = "SELECT " & _
    '                   "     EF.owrfield1unkid AS Id " & _
    '                   "    ,EF.field_data AS Name " & _
    '                   "FROM " & strMappeedTableName & " AS EF " & _
    '                   "    JOIN hrassess_empowner_tran AS EO ON EF." & strMappedFiledName & " = EO.empfieldunkid " & _
    '                   "WHERE EF.isvoid = 0 AND EF.periodunkid = '" & iPeriodId & "' AND EO.employeeunkid = '" & iEmpolyeeId & "' " & _
    '                   "AND EF.isfinal = 1 "

    '            dsList = objDataOperation.ExecQuery(StrQ, "List")

    '            If objDataOperation.ErrorMessage <> "" Then
    '                exForce = New Exception(objDataOperation.ErrorNumber & " : " & objDataOperation.ErrorMessage)
    '                Throw exForce
    '            End If

    '            If dsList.Tables(0).Rows.Count <= 0 Then
    '                StrMsg = Language.getMessage(mstrModuleName, 991, "Sorry, you cannot add goals for the selected period. Reason, Due to cascading setting once your assessor/line manager's goals are approved, you cannot add the new goals.")
    '            End If
    '        End If
    '    Catch ex As Exception
    '        Throw New Exception(ex.Message & "; Procedure Name: AllowedToAddGoals; Module Name: " & mstrModuleName)
    '    Finally
    '    End Try
    '    Return StrMsg
    'End Function

    'Public Function GetOwnerAssignedGoals(ByVal iPeriodId As Integer, ByVal iEmpolyeeId As Integer) As DataSet
    '    Dim StrQ As String = ""
    '    Dim exForce As Exception
    '    Dim dsList As New DataSet
    '    Dim strMappeedTableName As String = ""
    '    Dim strMappedFiledName As String = ""
    '    Dim objDataOperation As New clsDataOperation
    '    Try
    '        Dim intLinkedFld As Integer = (New clsAssess_Field_Mapping).Get_Map_FieldId(iPeriodId)

    '        Select Case intLinkedFld
    '            Case enWeight_Types.WEIGHT_FIELD1
    '                strMappedFiledName = "empfield1unkid"
    '                strMappeedTableName = "hrassess_empfield1_master"
    '            Case enWeight_Types.WEIGHT_FIELD2
    '                strMappedFiledName = "empfield2unkid"
    '                strMappeedTableName = "hrassess_empfield2_master"
    '            Case enWeight_Types.WEIGHT_FIELD3
    '                strMappedFiledName = "empfield3unkid"
    '                strMappeedTableName = "hrassess_empfield3_master"
    '            Case enWeight_Types.WEIGHT_FIELD4
    '                strMappedFiledName = "empfield4unkid"
    '                strMappeedTableName = "hrassess_empfield4_master"
    '            Case enWeight_Types.WEIGHT_FIELD5
    '                strMappedFiledName = "empfield5unkid"
    '                strMappeedTableName = "hrassess_empfield5_master"
    '        End Select

    '        StrQ = "SELECT 0 AS Id,@Select AS Name UNION ALL " & _
    '               "SELECT " & _
    '               "     EF.owrfield1unkid AS Id " & _
    '               "    ,EF.field_data AS Name " & _
    '               "FROM " & strMappeedTableName & " AS EF " & _
    '               "    JOIN hrassess_empowner_tran AS EO ON EF." & strMappedFiledName & " = EO.empfieldunkid " & _
    '               "WHERE EF.isvoid = 0 AND EF.periodunkid = '" & iPeriodId & "' AND EO.employeeunkid = '" & iEmpolyeeId & "' " & _
    '               "AND EF.isfinal = 1 "

    '        objDataOperation.AddParameter("@Select", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 992, "Select"))

    '        dsList = objDataOperation.ExecQuery(StrQ, "List")

    '        If objDataOperation.ErrorMessage <> "" Then
    '            exForce = New Exception(objDataOperation.ErrorNumber & " : " & objDataOperation.ErrorMessage)
    '            Throw exForce
    '        End If

    '    Catch ex As Exception
    '        Throw New Exception(ex.Message & "; Procedure Name: GetOwnerAssignedGoals; Module Name: " & mstrModuleName)
    '    Finally
    '    End Try
    '    Return dsList
    'End Function

    'Public Function IsGoalsAssignedSubEmployee(ByVal iPeriodId As Integer, ByVal iEmpolyeeId As Integer) As String
    '    Dim StrQ As String = ""
    '    Dim exForce As Exception
    '    Dim dsList As New DataSet
    '    Dim strMappeedTableName As String = ""
    '    Dim strMappedFiledName As String = ""
    '    Dim objDataOperation As New clsDataOperation
    '    Dim StrMsg As String = ""
    '    Dim objGrp As New clsGroup_Master
    '    Try
    '        objGrp._Groupunkid = 1
    '        If objGrp._Groupname.ToUpper = "NMB PLC" Then
    '            Dim intLinkedFld As Integer = (New clsAssess_Field_Mapping).Get_Map_FieldId(iPeriodId)

    '            Select Case intLinkedFld
    '                Case enWeight_Types.WEIGHT_FIELD1
    '                    strMappedFiledName = "empfield1unkid"
    '                    strMappeedTableName = "hrassess_empfield1_master"
    '                Case enWeight_Types.WEIGHT_FIELD2
    '                    strMappedFiledName = "empfield2unkid"
    '                    strMappeedTableName = "hrassess_empfield2_master"
    '                Case enWeight_Types.WEIGHT_FIELD3
    '                    strMappedFiledName = "empfield3unkid"
    '                    strMappeedTableName = "hrassess_empfield3_master"
    '                Case enWeight_Types.WEIGHT_FIELD4
    '                    strMappedFiledName = "empfield4unkid"
    '                    strMappeedTableName = "hrassess_empfield4_master"
    '                Case enWeight_Types.WEIGHT_FIELD5
    '                    strMappedFiledName = "empfield5unkid"
    '                    strMappeedTableName = "hrassess_empfield5_master"
    '            End Select

    '            StrQ = "SELECT DISTINCT " & _
    '                   "    EM.employeecode " & _
    '                   "FROM " & strMappeedTableName & " AS EF " & _
    '                   "    JOIN hremployee_master EM ON EF.employeeunkid = EM.employeeunkid " & _
    '                   "WHERE EF.employeeunkid IN " & _
    '                   "( " & _
    '                   "    SELECT " & _
    '                   "        O.employeeunkid " & _
    '                   "    FROM " & strMappeedTableName & " AS E " & _
    '                   "        JOIN hrassess_empowner_tran AS O ON E." & strMappedFiledName & " = O.empfieldunkid " & _
    '                   "    WHERE E.isvoid = 0 AND E.periodunkid = '" & iPeriodId & "' AND E.employeeunkid = '" & iEmpolyeeId & "' " & _
    '                   ") AND EF.isvoid = 0 AND EF.periodunkid = '" & iPeriodId & "' "

    '            dsList = objDataOperation.ExecQuery(StrQ, "List")

    '            If objDataOperation.ErrorMessage <> "" Then
    '                exForce = New Exception(objDataOperation.ErrorNumber & " : " & objDataOperation.ErrorMessage)
    '                Throw exForce
    '            End If

    '            If dsList.Tables(0).Rows.Count > 0 Then
    '                Dim eCodes As String = ""
    '                eCodes = String.Join(",", dsList.Tables(0).AsEnumerable().Select(Function(x) x.Field(Of String)("employeecode")).ToArray)
    '                StrMsg = Language.getMessage(mstrModuleName, 993, "Sorry, you cannot unlock goals for the selected period and employee. Reason, Following employee(s) [") & eCodes & _
    '                         Language.getMessage(mstrModuleName, 994, "], goals are already assigned to them. Please void then first.")
    '            End If
    '        End If
    '    Catch ex As Exception
    '        Throw New Exception(ex.Message & "; Procedure Name: AllowedToAddGoals; Module Name: " & mstrModuleName)
    '    Finally
    '    End Try
    '    Return StrMsg
    'End Function

    'Public Function IsGoalOwner(ByVal iPeriodId As Integer, ByVal iEmpolyeeId As Integer) As Boolean
    '    Dim StrQ As String = ""
    '    Dim exForce As Exception
    '    Dim dsList As New DataSet
    '    Dim strMappeedTableName As String = ""
    '    Dim strMappedFiledName As String = ""
    '    Dim objDataOperation As New clsDataOperation
    '    Dim StrMsg As String = ""
    '    Dim objGrp As New clsGroup_Master
    '    Dim blnFlag As Boolean = False
    '    Try
    '        objGrp._Groupunkid = 1
    '        If objGrp._Groupname.ToUpper = "NMB PLC" Then
    '            Dim intLinkedFld As Integer = (New clsAssess_Field_Mapping).Get_Map_FieldId(iPeriodId)

    '            Select Case intLinkedFld
    '                Case enWeight_Types.WEIGHT_FIELD1
    '                    strMappedFiledName = "owrfield1unkid"
    '                    strMappeedTableName = "hrassess_owrfield1_master"
    '                Case enWeight_Types.WEIGHT_FIELD2
    '                    strMappedFiledName = "owrfield2unkid"
    '                    strMappeedTableName = "hrassess_owrfield2_master"
    '                Case enWeight_Types.WEIGHT_FIELD3
    '                    strMappedFiledName = "owrfield3unkid"
    '                    strMappeedTableName = "hrassess_owrfield3_master"
    '                Case enWeight_Types.WEIGHT_FIELD4
    '                    strMappedFiledName = "owrfield4unkid"
    '                    strMappeedTableName = "hrassess_owrfield4_master"
    '                Case enWeight_Types.WEIGHT_FIELD5
    '                    strMappedFiledName = "owrfield5unkid"
    '                    strMappeedTableName = "hrassess_owrfield5_master"
    '            End Select

    '            StrQ = "SELECT 1 " & _
    '                   "FROM " & strMappeedTableName & " AS EF " & _
    '                   "    JOIN hrassess_owrowner_tran AS EO ON EF." & strMappedFiledName & " = EO.owrfieldunkid " & _
    '                   "WHERE EF.isvoid = 0 AND EF.periodunkid = '" & iPeriodId & "' AND EO.employeeunkid = '" & iEmpolyeeId & "' "

    '            dsList = objDataOperation.ExecQuery(StrQ, "List")

    '            If objDataOperation.ErrorMessage <> "" Then
    '                exForce = New Exception(objDataOperation.ErrorNumber & " : " & objDataOperation.ErrorMessage)
    '                Throw exForce
    '            End If

    '            If dsList.Tables(0).Rows.Count > 0 Then
    '                blnFlag = True
    '            End If
    '        Else
    '            blnFlag = True
    '        End If
    '    Catch ex As Exception
    '        Throw New Exception(ex.Message & "; Procedure Name: AllowedToAddGoals; Module Name: " & mstrModuleName)
    '    Finally
    '    End Try
    '    Return blnFlag
    'End Function
    'S.SANDEEP |27-NOV-2020| -- END

    '<Language> This Auto Generated Text Please Do Not Modify it.
#Region " Language & UI Settings "
    Public Shared Sub SetMessages()
        Try
            Language.setMessage("clsAssess_Field_Master", 14, "Goal Type")
            Language.setMessage("clsAssess_Field_Master", 15, "Goal Value")
            Language.setMessage("clsBSC_Planning_Report", 1, "Submitted For Approval")
            Language.setMessage("clsBSC_Planning_Report", 3, "Opened For Changes")
            Language.setMessage("clsBSC_Planning_Report", 4, "Not Submitted For Approval")
            Language.setMessage("clsBSC_Planning_Report", 49, "Not Committed")
            Language.setMessage("clsBSC_Planning_Report", 50, "Final Committed")
            Language.setMessage("clsBSC_Planning_Report", 51, "Periodic Review")
            Language.setMessage("clsBSC_Planning_Report", 101, "Approved")
            Language.setMessage("clsMasterData", 518, "Pending")
            Language.setMessage("clsMasterData", 519, "In progress")
            Language.setMessage("clsMasterData", 520, "Complete")
            Language.setMessage("clsMasterData", 521, "Closed")
            Language.setMessage("clsMasterData", 522, "On Track")
            Language.setMessage("clsMasterData", 523, "At Risk")
            Language.setMessage("clsMasterData", 524, "Not Applicable")
            Language.setMessage("clsMasterData", 759, "Pending")
            Language.setMessage("clsMasterData", 760, "Approved")
            Language.setMessage("clsMasterData", 761, "Disapprove")
            Language.setMessage("clsMasterData", 843, "Qualitative")
            Language.setMessage("clsMasterData", 844, "Quantitative")
            Language.setMessage("frmUpdateFieldValue", 101, "Percentage")
            Language.setMessage("frmUpdateFieldValue", 102, "Value")
            Language.setMessage(mstrModuleName, 1, "Sorry, this data is already defined. Please define new data for the selected employee.")
            Language.setMessage(mstrModuleName, 2, "Select")
            Language.setMessage(mstrModuleName, 3, "Owner")
            Language.setMessage(mstrModuleName, 4, "Notification for Approval/Rejection BSC Planning.")
            Language.setMessage(mstrModuleName, 5, "Dear")
            Language.setMessage(mstrModuleName, 9, "Notification for Rejection of BSC Planning")
            Language.setMessage(mstrModuleName, 12, " and it is now open for editing.")
            Language.setMessage(mstrModuleName, 14, "Comments :")
            Language.setMessage(mstrModuleName, 17, "Employee")
            Language.setMessage(mstrModuleName, 18, "Start Date")
            Language.setMessage(mstrModuleName, 19, "End Date")
            Language.setMessage(mstrModuleName, 20, "Status")
            Language.setMessage(mstrModuleName, 21, "Period")
            Language.setMessage(mstrModuleName, 22, "Weight")
            Language.setMessage(mstrModuleName, 23, "% Completed")
            Language.setMessage(mstrModuleName, 24, "View/Update Progress")
            Language.setMessage(mstrModuleName, 25, "Update Progress")
            Language.setMessage(mstrModuleName, 26, "Sorry, you cannot do delete operation. Reason : selected goal is already linked with transactions.")
            Language.setMessage(mstrModuleName, 27, "Perspective")
            Language.setMessage(mstrModuleName, 28, "Goal Accomplishment Status")
            Language.setMessage(mstrModuleName, 29, "Progress Remark")
			Language.setMessage(mstrModuleName, 32, "Goal Status")
			Language.setMessage(mstrModuleName, 33, "Please note that, I have completed my Balanced Score Card for the")
			Language.setMessage(mstrModuleName, 35, "Regards,")
			Language.setMessage(mstrModuleName, 36, "Please note that, your Balanced Score Card has been rejected")
			Language.setMessage(mstrModuleName, 37, "You are therefore advised to make changes as recommended.")
			Language.setMessage(mstrModuleName, 38, "Notification for Approval of Balanced Scorecard")
			Language.setMessage(mstrModuleName, 39, "Please note that, your Balanced Scorecard has been approved")
			Language.setMessage(mstrModuleName, 40, "Notification for Reopened of Balanced Scorecard")
			Language.setMessage(mstrModuleName, 42, "You are advised to make changes as recommended.")
			Language.setMessage(mstrModuleName, 43, "Regards,")
			Language.setMessage(mstrModuleName, 44, "Please note that, your Balanced Scorecard has been reopened for editing.")
			Language.setMessage(mstrModuleName, 45, "You are advised to make changes as recommended.")
			Language.setMessage(mstrModuleName, 46, "BSC notification")
			Language.setMessage(mstrModuleName, 48, "has been submitted to your line manager for approval.")
            Language.setMessage(mstrModuleName, 49, "Regards.")
            Language.setMessage(mstrModuleName, 50, "period")
            Language.setMessage(mstrModuleName, 51, "Kindly click the link below to review it.")
			Language.setMessage(mstrModuleName, 147, "Please note that, your Balance Score card for the")
			Language.setMessage(mstrModuleName, 579, "Pending")
			Language.setMessage(mstrModuleName, 580, "In progress")
			Language.setMessage(mstrModuleName, 581, "Complete")
			Language.setMessage(mstrModuleName, 582, "Closed")
			Language.setMessage(mstrModuleName, 583, "On Track")
			Language.setMessage(mstrModuleName, 584, "At Risk")
			Language.setMessage(mstrModuleName, 585, "Not Applicable")
			Language.setMessage(mstrModuleName, 748, "Postponed")
			Language.setMessage(mstrModuleName, 749, "Behind Schedule")
			Language.setMessage(mstrModuleName, 750, "Ahead of Schedule")
            Language.setMessage(mstrModuleName, 751, "Please note that, your Balanced Scorecard has been reopened for editing along with Progress Update(s).")
            Language.setMessage(mstrModuleName, 991, "Sorry, you cannot add goals for the selected period. Reason, Due to cascading setting once your assessor/line manager's goals are approved, you cannot add the new goals.")
            Language.setMessage(mstrModuleName, 992, "Select")
            Language.setMessage(mstrModuleName, 993, "Sorry, you cannot unlock goals for the selected period and employee. Reason, Following employee(s) [")
            Language.setMessage(mstrModuleName, 994, "], goals are already assigned to them. Please void then first.")
            Language.setMessage(mstrModuleName, 999, "Voided due to update")

        Catch Ex As Exception
            DisplayError.Show("-1", Ex.Message, "SetMessages", mstrModuleName)
        End Try
    End Sub
#End Region 'Language & UI Settings
    '</Language>
End Class