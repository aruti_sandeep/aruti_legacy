﻿'************************************************************************************************************************************
'Class Name : clsassess_analysis_master.vb
'Purpose    :
'Date       :03/01/2011
'Written By :Sandeep J. Sharma
'Modified   : ENHANCEMENT : ANY CHANGES IN DESKTOP SHOULD ALSO BE IMPLEMENTED IN WEB WITHOUT FAIL.
'************************************************************************************************************************************

Imports eZeeCommonLib
''' <summary>
''' Purpose: ENHANCEMENT : ANY CHANGES IN DESKTOP SHOULD ALSO BE IMPLEMENTED IN WEB WITHOUT FAIL.
''' Developer: Sandeep J. Sharma
''' </summary>

Public Class clsassess_analysis_master
    Private Shared Readonly mstrModuleName As String = "clsassess_analysis_master"
    Dim objDataOperation As clsDataOperation
    Dim mstrMessage As String = ""
    Dim objAnalysisTran As New clsassess_analysis_tran
    Dim objRemarkTran As New clsassess_remarks_tran

#Region " Private variables "

    Private mintAnalysisunkid As Integer
    Private mintYearunkid As Integer
    Private mintPeriodunkid As Integer
    Private mintSelfemployeeunkid As Integer
    Private mintAssessormasterunkid As Integer
    Private mintAssessoremployeeunkid As Integer
    Private mintAssessedemployeeunkid As Integer
    Private mintAssessgroupunkid As Integer
    Private mdtAssessmentdate As Date
    Private mintUserunkid As Integer
    Private mblnIsvoid As Boolean
    Private mintVoiduserunkid As Integer
    Private mdtVoiddatetime As Date
    Private mstrVoidreason As String = String.Empty
    Private mblnIscommitted As Boolean
    Private mintReviewerunkid As Integer
    Private mintAssessmodeid As Integer
    Private mstrReviewer_Remark1 As String = String.Empty
    Private mstrReviewer_Remark2 As String = String.Empty
    Private mintExt_Assessorunkid As Integer
    'S.SANDEEP [ 14 JUNE 2012 ] -- START
    'ENHANCEMENT : TRA CHANGES
    Private mdtCommitteddatetime As Date
    'S.SANDEEP [ 14 JUNE 2012 ] -- END

    'S.SANDEEP [ 28 JAN 2014 ] -- START
    Private mstrWebFrmName As String = String.Empty
    'S.SANDEEP [ 28 JAN 2014 ] -- END

#End Region

#Region " Properties "

    ''' <summary>
    ''' Purpose: Get Message from Class 
    ''' Modify By: Sandeep J. Sharma
    ''' </summary>
    Public ReadOnly Property _Message() As String
        Get
            Return mstrMessage
        End Get
    End Property

    ''' <summary>
    ''' Purpose: Get or Set analysisunkid
    ''' Modify By: Sandeep J. Sharma
    ''' </summary>
    Public Property _Analysisunkid() As Integer
        Get
            Return mintAnalysisunkid
        End Get
        Set(ByVal value As Integer)
            mintAnalysisunkid = value
            Call GetData()
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set yearunkid
    ''' Modify By: Sandeep J. Sharma
    ''' </summary>
    Public Property _Yearunkid() As Integer
        Get
            Return mintYearunkid
        End Get
        Set(ByVal value As Integer)
            mintYearunkid = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set periodunkid
    ''' Modify By: Sandeep J. Sharma
    ''' </summary>
    Public Property _Periodunkid() As Integer
        Get
            Return mintPeriodunkid
        End Get
        Set(ByVal value As Integer)
            mintPeriodunkid = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set selfemployeeunkid
    ''' Modify By: Sandeep J. Sharma
    ''' </summary>
    Public Property _Selfemployeeunkid() As Integer
        Get
            Return mintSelfemployeeunkid
        End Get
        Set(ByVal value As Integer)
            mintSelfemployeeunkid = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set assessormasterunkid
    ''' Modify By: Sandeep J. Sharma
    ''' </summary>
    Public Property _Assessormasterunkid() As Integer
        Get
            Return mintAssessormasterunkid
        End Get
        Set(ByVal value As Integer)
            mintAssessormasterunkid = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set assessoremployeeunkid
    ''' Modify By: Sandeep J. Sharma
    ''' </summary>
    Public Property _Assessoremployeeunkid() As Integer
        Get
            Return mintAssessoremployeeunkid
        End Get
        Set(ByVal value As Integer)
            mintAssessoremployeeunkid = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set assessedemployeeunkid
    ''' Modify By: Sandeep J. Sharma
    ''' </summary>
    Public Property _Assessedemployeeunkid() As Integer
        Get
            Return mintAssessedemployeeunkid
        End Get
        Set(ByVal value As Integer)
            mintAssessedemployeeunkid = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set assessgroupunkid
    ''' Modify By: Sandeep J. Sharma
    ''' </summary>
    Public Property _Assessgroupunkid() As Integer
        Get
            Return mintAssessgroupunkid
        End Get
        Set(ByVal value As Integer)
            mintAssessgroupunkid = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set assessmentdate
    ''' Modify By: Sandeep J. Sharma
    ''' </summary>
    Public Property _Assessmentdate() As Date
        Get
            Return mdtAssessmentdate
        End Get
        Set(ByVal value As Date)
            mdtAssessmentdate = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set userunkid
    ''' Modify By: Sandeep J. Sharma
    ''' </summary>
    Public Property _Userunkid() As Integer
        Get
            Return mintUserunkid
        End Get
        Set(ByVal value As Integer)
            mintUserunkid = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set isvoid
    ''' Modify By: Sandeep J. Sharma
    ''' </summary>
    Public Property _Isvoid() As Boolean
        Get
            Return mblnIsvoid
        End Get
        Set(ByVal value As Boolean)
            mblnIsvoid = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set voiduserunkid
    ''' Modify By: Sandeep J. Sharma
    ''' </summary>
    Public Property _Voiduserunkid() As Integer
        Get
            Return mintVoiduserunkid
        End Get
        Set(ByVal value As Integer)
            mintVoiduserunkid = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set voiddatetime
    ''' Modify By: Sandeep J. Sharma
    ''' </summary>
    Public Property _Voiddatetime() As Date
        Get
            Return mdtVoiddatetime
        End Get
        Set(ByVal value As Date)
            mdtVoiddatetime = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set voidreason
    ''' Modify By: Sandeep J. Sharma
    ''' </summary>
    Public Property _Voidreason() As String
        Get
            Return mstrVoidreason
        End Get
        Set(ByVal value As String)
            mstrVoidreason = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set iscommitted
    ''' Modify By: Sandeep J. Sharma
    ''' </summary>
    Public Property _Iscommitted() As Boolean
        Get
            Return mblnIscommitted
        End Get
        Set(ByVal value As Boolean)
            mblnIscommitted = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set reviewerunkid
    ''' Modify By: Sandeep J. Sharma
    ''' </summary>
    Public Property _Reviewerunkid() As Integer
        Get
            Return mintReviewerunkid
        End Get
        Set(ByVal value As Integer)
            mintReviewerunkid = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set assessmodeid
    ''' Modify By: Sandeep J. Sharma
    ''' </summary>
    Public Property _Assessmodeid() As Integer
        Get
            Return mintAssessmodeid
        End Get
        Set(ByVal value As Integer)
            mintAssessmodeid = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set reviewer_remark1
    ''' Modify By: Sandeep J. Sharma
    ''' </summary>
    Public Property _Reviewer_Remark1() As String
        Get
            Return mstrReviewer_Remark1
        End Get
        Set(ByVal value As String)
            mstrReviewer_Remark1 = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set reviewer_remark2
    ''' Modify By: Sandeep J. Sharma
    ''' </summary>
    Public Property _Reviewer_Remark2() As String
        Get
            Return mstrReviewer_Remark2
        End Get
        Set(ByVal value As String)
            mstrReviewer_Remark2 = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set ext_assessorunkid
    ''' Modify By: Sandeep J. Sharma
    ''' </summary>
    Public Property _Ext_Assessorunkid() As Integer
        Get
            Return mintExt_Assessorunkid
        End Get
        Set(ByVal value As Integer)
            mintExt_Assessorunkid = Value
        End Set
    End Property

    'S.SANDEEP [ 14 JUNE 2012 ] -- START
    'ENHANCEMENT : TRA CHANGES
    ''' <summary>
    ''' Purpose: Get or Set committeddatetime
    ''' Modify By: Sandeep Sharma
    ''' </summary>
    Public Property _Committeddatetime() As Date
        Get
            Return mdtCommitteddatetime
        End Get
        Set(ByVal value As Date)
            mdtCommitteddatetime = Value
        End Set
    End Property
    'S.SANDEEP [ 14 JUNE 2012 ] -- END

    'S.SANDEEP [ 28 JAN 2014 ] -- START
    Public WriteOnly Property _WebFrmName() As String
        Set(ByVal value As String)
            mstrWebFrmName = value
        End Set
    End Property
    'S.SANDEEP [ 28 JAN 2014 ] -- END

#End Region

    ''' <summary>
    ''' Modify By: Sandeep J. Sharma
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>
    Public Sub GetData()
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        objDataOperation = New clsDataOperation

        Try
            strQ = "SELECT " & _
              "  analysisunkid " & _
              ", yearunkid " & _
              ", periodunkid " & _
              ", selfemployeeunkid " & _
              ", assessormasterunkid " & _
              ", assessoremployeeunkid " & _
              ", assessedemployeeunkid " & _
              ", assessgroupunkid " & _
              ", assessmentdate " & _
              ", userunkid " & _
              ", isvoid " & _
              ", voiduserunkid " & _
              ", voiddatetime " & _
              ", voidreason " & _
              ", ISNULL(iscommitted,0) AS iscommitted " & _
              ", ISNULL(reviewerunkid,-1) AS reviewerunkid " & _
              ", ISNULL(assessmodeid,0) AS assessmodeid " & _
              ", ISNULL(reviewer_remark1,'') AS reviewer_remark1 " & _
              ", ISNULL(reviewer_remark2,'') AS reviewer_remark2 " & _
              ", ext_assessorunkid " & _
                    ", committeddatetime " & _
             "FROM hrassess_analysis_master " & _
             "WHERE analysisunkid = @analysisunkid "

            'S.SANDEEP [ 14 JUNE 2012 committeddatetime ] -- START -- END

            objDataOperation.AddParameter("@analysisunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintAnalysisunkid.ToString)

            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            For Each dtRow As DataRow In dsList.Tables(0).Rows
                mintanalysisunkid = CInt(dtRow.Item("analysisunkid"))
                mintyearunkid = CInt(dtRow.Item("yearunkid"))
                mintperiodunkid = CInt(dtRow.Item("periodunkid"))
                mintselfemployeeunkid = CInt(dtRow.Item("selfemployeeunkid"))
                mintassessormasterunkid = CInt(dtRow.Item("assessormasterunkid"))
                mintassessoremployeeunkid = CInt(dtRow.Item("assessoremployeeunkid"))
                mintassessedemployeeunkid = CInt(dtRow.Item("assessedemployeeunkid"))
                mintassessgroupunkid = CInt(dtRow.Item("assessgroupunkid"))
                mdtAssessmentdate = dtRow.Item("assessmentdate")
                mintuserunkid = CInt(dtRow.Item("userunkid"))
                mblnisvoid = CBool(dtRow.Item("isvoid"))
                mintvoiduserunkid = CInt(dtRow.Item("voiduserunkid"))
                If IsDBNull(dtRow.Item("voiddatetime")) Then
                    mdtVoiddatetime = Nothing
                Else
                    mdtVoiddatetime = dtRow.Item("voiddatetime")
                End If
                mstrvoidreason = dtRow.Item("voidreason").ToString
                mblniscommitted = CBool(dtRow.Item("iscommitted"))
                mintreviewerunkid = CInt(dtRow.Item("reviewerunkid"))
                mintassessmodeid = CInt(dtRow.Item("assessmodeid"))
                mstrreviewer_remark1 = dtRow.Item("reviewer_remark1").ToString
                mstrReviewer_Remark2 = dtRow.Item("reviewer_remark2").ToString
                mintExt_Assessorunkid = CInt(dtRow.Item("ext_assessorunkid"))

                'S.SANDEEP [ 14 JUNE 2012 ] -- START
                'ENHANCEMENT : TRA CHANGES
                If IsDBNull(dtRow.Item("committeddatetime")) = False Then
                    mdtCommitteddatetime = dtRow.Item("committeddatetime")
                Else
                    mdtCommitteddatetime = Nothing
                End If
                'S.SANDEEP [ 14 JUNE 2012 ] -- END

                Exit For
            Next
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: GetData; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
    End Sub

    ''' <summary>
    ''' Modify By: Sandeep J. Sharma
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>
    Public Function GetList(ByVal strTableName As String, _
                            ByVal enAssessMode As enAssessmentMode, _
                            Optional ByVal strIncludeInactiveEmployee As String = "", _
                            Optional ByVal strEmployeeAsOnDate As String = "", _
                            Optional ByVal strUserAccessLevelFilterString As String = "", _
                            Optional ByVal intUserUnkId As Integer = 0) As DataSet 'S.SANDEEP [ 27 APRIL 2012 ] -- START-- END
        'Public Function GetList(ByVal strTableName As String, ByVal enAssessMode As enAssessmentMode) As DataSet
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        objDataOperation = New clsDataOperation

        Try
            strQ = "SELECT " & _
                   "  hrassess_analysis_master.analysisunkid " & _
                   ", hrassess_analysis_master.yearunkid " & _
                   ", hrassess_analysis_master.periodunkid " & _
                   ", hrassess_analysis_master.selfemployeeunkid " & _
                   ", hrassess_analysis_master.assessormasterunkid " & _
                   ", hrassess_analysis_master.assessoremployeeunkid " & _
                   ", hrassess_analysis_master.assessedemployeeunkid " & _
                   ", hrassess_analysis_master.assessgroupunkid " & _
                   ", Convert(Char(8),hrassess_analysis_master.assessmentdate,112) AS assessmentdate " & _
                   ", hrassess_analysis_master.userunkid " & _
                   ", hrassess_analysis_master.isvoid " & _
                   ", hrassess_analysis_master.voiduserunkid " & _
                   ", hrassess_analysis_master.voiddatetime " & _
                   ", hrassess_analysis_master.voidreason " & _
                   ", ISNULL(hremployee_master.firstname,'')+' '+ISNULL(hremployee_master.othername,'')+' '+ISNULL(hremployee_master.surname,'') As EmpName " & _
                   ", ISNULL(hrmsConfiguration..cffinancial_year_tran.financialyear_name,'') AS YearName " & _
                   ", ISNULL(cfcommon_period_tran.period_name,'') AS PName " & _
                   ", ISNULL(iscommitted,0) AS iscommitted " & _
                   ", ISNULL(hremployee_master.employeecode,'') AS Ecode " & _
                   ", cfcommon_period_tran.statusid AS Sid "
            'S.SANDEEP [ 05 MARCH 2012 ] -- START
            'ENHANCEMENT : TRA CHANGES
            strQ &= ",hremployee_master.stationunkid " & _
                    ",hremployee_master.deptgroupunkid " & _
                    ",hremployee_master.departmentunkid " & _
                    ",hremployee_master.sectionunkid " & _
                    ",hremployee_master.unitunkid " & _
                    ",hremployee_master.jobgroupunkid " & _
                    ",hremployee_master.jobunkid " & _
                    ",hremployee_master.gradegroupunkid " & _
                    ",hremployee_master.gradeunkid " & _
                    ",hremployee_master.gradelevelunkid " & _
                    ",hremployee_master.classgroupunkid " & _
                    ",hremployee_master.classunkid " & _
                    ",hremployee_master.sectiongroupunkid " & _
                    ",hremployee_master.unitgroupunkid " & _
                    ",hremployee_master.teamunkid "

            'S.SANDEEP [ 14 JUNE 2012 ] -- START
            'ENHANCEMENT : TRA CHANGES
            strQ &= ",committeddatetime "
            'S.SANDEEP [ 14 JUNE 2012 ] -- END

            'S.SANDEEP [ 05 MARCH 2012 ] -- END
            If enAssessMode = enAssessmentMode.SELF_ASSESSMENT Then
                strQ &= ", ' ' as Assessor "
            ElseIf enAssessMode = enAssessmentMode.APPRAISER_ASSESSMENT Then
                strQ &= ", CASE WHEN hrexternal_assessor_master.ext_assessorunkid > 0 THEN hrexternal_assessor_master.displayname ELSE ISNULL(AEMP.firstname,'')+' '+ISNULL(AEMP.othername,'')+' '+ISNULL(AEMP.surname,'') END AS Assessor "
            ElseIf enAssessMode = enAssessmentMode.REVIEWER_ASSESSMENT Then
                strQ &= ", ISNULL(Reviewer.firstname,'')+' '+ISNULL(Reviewer.othername,'')+' '+ISNULL(Reviewer.surname,'') AS Reviewer "
            End If

            'S.SANDEEP [ 14 AUG 2013 ] -- START
            'ENHANCEMENT : TRA CHANGES
            strQ &= ", hrassess_group_master.assessgroup_name AS Assessment_Group "
            'S.SANDEEP [ 14 AUG 2013 ] -- END


            strQ &= "FROM hrassess_analysis_master " & _
                    " JOIN hrassess_group_master ON hrassess_analysis_master.assessgroupunkid = hrassess_group_master.assessgroupunkid " & _
                    " JOIN hrmsConfiguration..cffinancial_year_tran on hrmsConfiguration..cffinancial_year_tran.yearunkid = hrassess_analysis_master.yearunkid " & _
                    " JOIN cfcommon_period_tran on cfcommon_period_tran.periodunkid = hrassess_analysis_master.periodunkid "

            If enAssessMode = enAssessmentMode.SELF_ASSESSMENT Then
                strQ &= " JOIN hremployee_master ON hrassess_analysis_master.selfemployeeunkid = hremployee_master.employeeunkid "
            ElseIf enAssessMode = enAssessmentMode.APPRAISER_ASSESSMENT Then
                strQ &= " JOIN hremployee_master ON hrassess_analysis_master.assessedemployeeunkid = hremployee_master.employeeunkid " & _
                                        " LEFT JOIN hrexternal_assessor_master ON hrexternal_assessor_master.ext_assessorunkid = hrassess_analysis_master.ext_assessorunkid " & _
                                        " LEFT JOIN hremployee_master AS AEMP ON AEMP.employeeunkid = hrassess_analysis_master.assessoremployeeunkid AND hrassess_analysis_master.ext_assessorunkid = 0 "


                'S.SANDEEP [ 14 AUG 2013 ] -- START
                'ENHANCEMENT : TRA CHANGES
                '   " JOIN hrassess_group_master ON hrassess_analysis_master.assessgroupunkid = hrassess_group_master.assessgroupunkid " ADDED
                'S.SANDEEP [ 14 AUG 2013 ] -- END



                'S.SANDEEP [ 27 APRIL 2012 ] -- START
                'ENHANCEMENT : TRA CHANGES
                'If ConfigParameter._Object._IsIncludeInactiveEmp = False Then
                '    strQ &= " AND CONVERT(CHAR(8),AEMP.appointeddate,112) <= @enddate " & _
                '                   " AND ISNULL(CONVERT(CHAR(8),AEMP.termination_from_date,112),@startdate) >= @startdate " & _
                '                   " AND ISNULL(CONVERT(CHAR(8),AEMP.termination_to_date,112),@startdate) >= @startdate "
                'End If

                ''S.SANDEEP [ 04 FEB 2012 ] -- START
                ''ENHANCEMENT : TRA CHANGES

                ''S.SANDEEP [ 23 JAN 2012 ] -- START
                ''ENHANCEMENT : TRA CHANGES : AUTOMATIC ASSIGNMENT OF BENEFITS        
                ''If UserAccessLevel._AccessLevel.Length > 0 Then
                ''    strQ &= " AND AEMP.jobunkid IN (" & UserAccessLevel._AccessLevel & ") "
                ''End If
                ''S.SANDEEP [ 23 JAN 2012 ] -- END
                'Select Case ConfigParameter._Object._UserAccessModeSetting
                '    Case enAllocation.BRANCH
                '        If UserAccessLevel._AccessLevel.Length > 0 Then
                '            strQ &= " AND  AEMP.stationunkid IN (" & UserAccessLevel._AccessLevel & ") "
                '        End If
                '    Case enAllocation.DEPARTMENT_GROUP
                '        If UserAccessLevel._AccessLevel.Length > 0 Then
                '            strQ &= " AND  AEMP.deptgroupunkid IN (" & UserAccessLevel._AccessLevel & ") "
                '        End If
                '    Case enAllocation.DEPARTMENT
                '        If UserAccessLevel._AccessLevel.Length > 0 Then
                '            strQ &= " AND  AEMP.departmentunkid IN (" & UserAccessLevel._AccessLevel & ") "
                '        End If
                '    Case enAllocation.SECTION_GROUP
                '        If UserAccessLevel._AccessLevel.Length > 0 Then
                '            strQ &= " AND  AEMP.sectiongroupunkid IN (" & UserAccessLevel._AccessLevel & ") "
                '        End If
                '    Case enAllocation.SECTION
                '        If UserAccessLevel._AccessLevel.Length > 0 Then
                '            strQ &= " AND  AEMP.sectionunkid IN (" & UserAccessLevel._AccessLevel & ") "
                '        End If
                '    Case enAllocation.UNIT_GROUP
                '        If UserAccessLevel._AccessLevel.Length > 0 Then
                '            strQ &= " AND  AEMP.unitgroupunkid IN (" & UserAccessLevel._AccessLevel & ") "
                '        End If
                '    Case enAllocation.UNIT
                '        If UserAccessLevel._AccessLevel.Length > 0 Then
                '            strQ &= " AND  AEMP.unitunkid IN (" & UserAccessLevel._AccessLevel & ") "
                '        End If
                '    Case enAllocation.TEAM
                '        If UserAccessLevel._AccessLevel.Length > 0 Then
                '            strQ &= " AND  AEMP.teamunkid IN (" & UserAccessLevel._AccessLevel & ") "
                '        End If
                '    Case enAllocation.JOB_GROUP
                '        If UserAccessLevel._AccessLevel.Length > 0 Then
                '            strQ &= " AND  AEMP.jobgroupunkid IN (" & UserAccessLevel._AccessLevel & ") "
                '        End If
                '    Case enAllocation.JOBS
                'If UserAccessLevel._AccessLevel.Length > 0 Then
                '    strQ &= " AND AEMP.jobunkid IN (" & UserAccessLevel._AccessLevel & ") "
                'End If
                'End Select

                'strQ &= " JOIN hrassessor_master ON hrassessor_master.assessormasterunkid = hrassess_analysis_master.assessormasterunkid " & _
                '                " JOIN hrapprover_usermapping ON hrapprover_usermapping.approverunkid = hrassessor_master.assessormasterunkid " & _
                '                " AND hrapprover_usermapping.userunkid = " & User._Object._Userunkid & " " & _
                '        " AND hrapprover_usermapping.usertypeid = " & enUserType.Assessor & " AND hrassessor_master.isvoid = 0 "

                If strIncludeInactiveEmployee.Trim.Length <= 0 Then
                    strIncludeInactiveEmployee = ConfigParameter._Object._IsIncludeInactiveEmp.ToString
                        End If

                If strUserAccessLevelFilterString = "" Then
                    'Sohail (08 May 2015) -- Start
                    'Enhancement - Get User Access Filter string from new employee transfer table and employee categorization table.
                    'strQ &= UserAccessLevel._AccessLevelFilterString.Replace("hremployee_master", "AEMP")
                    'strQ &= NewAccessLevelFilterString().Replace("hremployee_master", "AEMP")
                    'Sohail (08 May 2015) -- End
                Else
                    strQ &= strUserAccessLevelFilterString.Replace("hremployee_master", "AEMP")
                        End If


                If CBool(strIncludeInactiveEmployee) = False Then
                    strQ &= " AND CONVERT(CHAR(8),AEMP.appointeddate,112) <= @enddate " & _
                                   " AND ISNULL(CONVERT(CHAR(8),AEMP.termination_from_date,112),@startdate) >= @startdate " & _
                                   " AND ISNULL(CONVERT(CHAR(8),AEMP.termination_to_date,112),@startdate) >= @startdate " & _
                                   " AND ISNULL(CONVERT(CHAR(8),AEMP.empl_enddate,112), @startdate) >= @startdate " 'Sohail (12 May 2012) - [empl_enddate]
                        End If


                strQ &= " JOIN hrassessor_master ON hrassessor_master.assessormasterunkid = hrassess_analysis_master.assessormasterunkid " & _
                                " JOIN hrapprover_usermapping ON hrapprover_usermapping.approverunkid = hrassessor_master.assessormasterunkid " & _
                                " AND hrapprover_usermapping.userunkid = " & IIf(intUserUnkId = 0, User._Object._Userunkid, intUserUnkId) & " " & _
                        " AND hrapprover_usermapping.usertypeid = " & enUserType.Assessor & " AND hrassessor_master.isvoid = 0 "
                'S.SANDEEP [ 27 APRIL 2012 ] -- END


                
                'S.SANDEEP [ 04 FEB 2012 ] -- END


                
            ElseIf enAssessMode = enAssessmentMode.REVIEWER_ASSESSMENT Then
                'S.SANDEEP [ 27 APRIL 2012 ] -- START
                'ENHANCEMENT : TRA CHANGES
                'strQ &= " JOIN hremployee_master ON hrassess_analysis_master.assessedemployeeunkid = hremployee_master.employeeunkid " & _
                '            " JOIN hremployee_master AS Reviewer ON hrassess_analysis_master.reviewerunkid = Reviewer.employeeunkid " & _
                '            " JOIN hrassessor_master ON hrassessor_master.assessormasterunkid = hrassess_analysis_master.assessormasterunkid " & _
                '            " JOIN hrapprover_usermapping ON hrapprover_usermapping.approverunkid = hrassessor_master.assessormasterunkid " & _
                '            " AND hrapprover_usermapping.userunkid = " & User._Object._Userunkid & " " & _
                '            " AND hrapprover_usermapping.usertypeid = " & enUserType.Assessor & " AND hrassessor_master.isvoid = 0 "

            strQ &= " JOIN hremployee_master ON hrassess_analysis_master.assessedemployeeunkid = hremployee_master.employeeunkid " & _
                        " JOIN hremployee_master AS Reviewer ON hrassess_analysis_master.reviewerunkid = Reviewer.employeeunkid " & _
                        " JOIN hrassessor_master ON hrassessor_master.assessormasterunkid = hrassess_analysis_master.assessormasterunkid " & _
                        " JOIN hrapprover_usermapping ON hrapprover_usermapping.approverunkid = hrassessor_master.assessormasterunkid " & _
                            " AND hrapprover_usermapping.userunkid = " & IIf(intUserUnkId = 0, User._Object._Userunkid, intUserUnkId) & " " & _
                        " AND hrapprover_usermapping.usertypeid = " & enUserType.Assessor & " AND hrassessor_master.isvoid = 0 "
                'S.SANDEEP [ 27 APRIL 2012 ] -- END
            End If

            strQ &= "WHERE hrassess_analysis_master.isvoid = 0 "



            'S.SANDEEP [ 27 APRIL 2012 ] -- START
            'ENHANCEMENT : TRA CHANGES
            'If ConfigParameter._Object._IsIncludeInactiveEmp = False Then
            '    Select Case enAssessMode
            '        Case enAssessmentMode.SELF_ASSESSMENT
            '            strQ &= " AND CONVERT(CHAR(8),hremployee_master.appointeddate,112) <= @enddate " & _
            '                   " AND ISNULL(CONVERT(CHAR(8),hremployee_master.termination_from_date,112),@startdate) >= @startdate " & _
            '                   " AND ISNULL(CONVERT(CHAR(8),hremployee_master.termination_to_date,112),@startdate) >= @startdate "

            '            objDataOperation.AddParameter("@startdate", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, ConfigParameter._Object._EmployeeAsOnDate)
            '            objDataOperation.AddParameter("@enddate", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, ConfigParameter._Object._EmployeeAsOnDate)
            '        Case enAssessmentMode.APPRAISER_ASSESSMENT
            '            strQ &= " AND CONVERT(CHAR(8),hremployee_master.appointeddate,112) <= @enddate " & _
            '                    " AND ISNULL(CONVERT(CHAR(8),hremployee_master.termination_from_date,112),@startdate) >= @startdate " & _
            '                    " AND ISNULL(CONVERT(CHAR(8),hremployee_master.termination_to_date,112),@startdate) >= @startdate "

            '            objDataOperation.AddParameter("@startdate", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, ConfigParameter._Object._EmployeeAsOnDate)
            '            objDataOperation.AddParameter("@enddate", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, ConfigParameter._Object._EmployeeAsOnDate)
            '        Case enAssessmentMode.REVIEWER_ASSESSMENT
            '            strQ &= " AND CONVERT(CHAR(8),hremployee_master.appointeddate,112) <= @enddate " & _
            '             " AND ISNULL(CONVERT(CHAR(8),hremployee_master.termination_from_date,112),@startdate) >= @startdate " & _
            '             " AND ISNULL(CONVERT(CHAR(8),hremployee_master.termination_to_date,112),@startdate) >= @startdate " & _
            '             " AND CONVERT(CHAR(8),Reviewer.appointeddate,112) <= @enddate " & _
            '             " AND ISNULL(CONVERT(CHAR(8),Reviewer.termination_from_date,112),@startdate) >= @startdate " & _
            '             " AND ISNULL(CONVERT(CHAR(8),Reviewer.termination_to_date,112),@startdate) >= @startdate "

            '            objDataOperation.AddParameter("@startdate", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, ConfigParameter._Object._EmployeeAsOnDate)
            '            objDataOperation.AddParameter("@enddate", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, ConfigParameter._Object._EmployeeAsOnDate)
            '    End Select
            'End If

            If strIncludeInactiveEmployee.Trim.Length <= 0 Then
                strIncludeInactiveEmployee = ConfigParameter._Object._IsIncludeInactiveEmp.ToString
            End If

            If CBool(strIncludeInactiveEmployee) = False Then
                Select Case enAssessMode
                    Case enAssessmentMode.SELF_ASSESSMENT
                        strQ &= " AND CONVERT(CHAR(8),hremployee_master.appointeddate,112) <= @enddate " & _
                               " AND ISNULL(CONVERT(CHAR(8),hremployee_master.termination_from_date,112),@startdate) >= @startdate " & _
                               " AND ISNULL(CONVERT(CHAR(8),hremployee_master.termination_to_date,112),@startdate) >= @startdate " & _
                               " AND ISNULL(CONVERT(CHAR(8),hremployee_master.empl_enddate,112), @startdate) >= @startdate " 'Sohail (12 May 2012) - [empl_enddate]

                        'S.SANDEEP [ 27 APRIL 2012 ] -- START
                        'ENHANCEMENT : TRA CHANGES
                        'objDataOperation.AddParameter("@startdate", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, ConfigParameter._Object._EmployeeAsOnDate)
                        'objDataOperation.AddParameter("@enddate", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, ConfigParameter._Object._EmployeeAsOnDate)

                        objDataOperation.AddParameter("@startdate", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, IIf(strEmployeeAsOnDate = "", ConfigParameter._Object._EmployeeAsOnDate, strEmployeeAsOnDate))
                        objDataOperation.AddParameter("@enddate", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, IIf(strEmployeeAsOnDate = "", ConfigParameter._Object._EmployeeAsOnDate, strEmployeeAsOnDate))
                        'S.SANDEEP [ 27 APRIL 2012 ] -- END

                    Case enAssessmentMode.APPRAISER_ASSESSMENT
                        strQ &= " AND CONVERT(CHAR(8),hremployee_master.appointeddate,112) <= @enddate " & _
                                " AND ISNULL(CONVERT(CHAR(8),hremployee_master.termination_from_date,112),@startdate) >= @startdate " & _
                                " AND ISNULL(CONVERT(CHAR(8),hremployee_master.termination_to_date,112),@startdate) >= @startdate " & _
                                " AND ISNULL(CONVERT(CHAR(8),hremployee_master.empl_enddate,112), @startdate) >= @startdate " 'Sohail (12 May 2012) - [empl_enddate]
                               
                        'S.SANDEEP [ 27 APRIL 2012 ] -- START
                        'ENHANCEMENT : TRA CHANGES
                        'objDataOperation.AddParameter("@startdate", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, ConfigParameter._Object._EmployeeAsOnDate)
                        'objDataOperation.AddParameter("@enddate", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, ConfigParameter._Object._EmployeeAsOnDate)

                        objDataOperation.AddParameter("@startdate", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, IIf(strEmployeeAsOnDate = "", ConfigParameter._Object._EmployeeAsOnDate, strEmployeeAsOnDate))
                        objDataOperation.AddParameter("@enddate", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, IIf(strEmployeeAsOnDate = "", ConfigParameter._Object._EmployeeAsOnDate, strEmployeeAsOnDate))
                        'S.SANDEEP [ 27 APRIL 2012 ] -- END

                    Case enAssessmentMode.REVIEWER_ASSESSMENT
                        strQ &= " AND CONVERT(CHAR(8),hremployee_master.appointeddate,112) <= @enddate " & _
                         " AND ISNULL(CONVERT(CHAR(8),hremployee_master.termination_from_date,112),@startdate) >= @startdate " & _
                         " AND ISNULL(CONVERT(CHAR(8),hremployee_master.termination_to_date,112),@startdate) >= @startdate " & _
                         " AND ISNULL(CONVERT(CHAR(8),hremployee_master.empl_enddate,112), @startdate) >= @startdate " & _
                         " AND CONVERT(CHAR(8),Reviewer.appointeddate,112) <= @enddate " & _
                         " AND ISNULL(CONVERT(CHAR(8),Reviewer.termination_from_date,112),@startdate) >= @startdate " & _
                         " AND ISNULL(CONVERT(CHAR(8),Reviewer.termination_to_date,112),@startdate) >= @startdate " & _
                         " AND ISNULL(CONVERT(CHAR(8),Reviewer.empl_enddate,112), @startdate) >= @startdate " 'Sohail (12 May 2012) - [empl_enddate]

                        'S.SANDEEP [ 27 APRIL 2012 ] -- START
                        'ENHANCEMENT : TRA CHANGES
                        'objDataOperation.AddParameter("@startdate", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, ConfigParameter._Object._EmployeeAsOnDate)
                        'objDataOperation.AddParameter("@enddate", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, ConfigParameter._Object._EmployeeAsOnDate)

                        objDataOperation.AddParameter("@startdate", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, IIf(strEmployeeAsOnDate = "", ConfigParameter._Object._EmployeeAsOnDate, strEmployeeAsOnDate))
                        objDataOperation.AddParameter("@enddate", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, IIf(strEmployeeAsOnDate = "", ConfigParameter._Object._EmployeeAsOnDate, strEmployeeAsOnDate))
                        'S.SANDEEP [ 27 APRIL 2012 ] -- END
                End Select
            End If

            'S.SANDEEP [ 27 APRIL 2012 ] -- END


            

            'S.SANDEEP [ 04 FEB 2012 ] -- START
            'ENHANCEMENT : TRA CHANGES
            'If UserAccessLevel._AccessLevel.Length > 0 Then
            '    If enAssessMode = enAssessmentMode.SELF_ASSESSMENT Then
            '        strQ &= " AND hremployee_master.jobunkid IN (" & UserAccessLevel._AccessLevel & ") "
            '    ElseIf enAssessMode = enAssessmentMode.APPRAISER_ASSESSMENT Then
            '        strQ &= " AND hremployee_master.jobunkid IN (" & UserAccessLevel._AccessLevel & ") AND reviewerunkid < = 0 "
            '    ElseIf enAssessMode = enAssessmentMode.REVIEWER_ASSESSMENT Then
            '        strQ &= " AND hremployee_master.jobunkid IN (" & UserAccessLevel._AccessLevel & ") AND Reviewer.jobunkid IN (" & UserAccessLevel._AccessLevel & ") "
            '    End If
            'End If



            'S.SANDEEP [ 27 APRIL 2012 ] -- START
            'ENHANCEMENT : TRA CHANGES
            'If UserAccessLevel._AccessLevel.Length > 0 Then
            '    If enAssessMode = enAssessmentMode.SELF_ASSESSMENT Then
            '        Select Case ConfigParameter._Object._UserAccessModeSetting
            '            Case enAllocation.BRANCH
            '                If UserAccessLevel._AccessLevel.Length > 0 Then
            '                    strQ &= " AND  hremployee_master.stationunkid IN (" & UserAccessLevel._AccessLevel & ") "
            '                End If
            '            Case enAllocation.DEPARTMENT_GROUP
            '                If UserAccessLevel._AccessLevel.Length > 0 Then
            '                    strQ &= " AND  hremployee_master.deptgroupunkid IN (" & UserAccessLevel._AccessLevel & ") "
            '                End If
            '            Case enAllocation.DEPARTMENT
            '                If UserAccessLevel._AccessLevel.Length > 0 Then
            '                    strQ &= " AND  hremployee_master.departmentunkid IN (" & UserAccessLevel._AccessLevel & ") "
            '                End If
            '            Case enAllocation.SECTION_GROUP
            '                If UserAccessLevel._AccessLevel.Length > 0 Then
            '                    strQ &= " AND  hremployee_master.sectiongroupunkid IN (" & UserAccessLevel._AccessLevel & ") "
            '                End If
            '            Case enAllocation.SECTION
            '                If UserAccessLevel._AccessLevel.Length > 0 Then
            '                    strQ &= " AND  hremployee_master.sectionunkid IN (" & UserAccessLevel._AccessLevel & ") "
            '                End If
            '            Case enAllocation.UNIT_GROUP
            '                If UserAccessLevel._AccessLevel.Length > 0 Then
            '                    strQ &= " AND  hremployee_master.unitgroupunkid IN (" & UserAccessLevel._AccessLevel & ") "
            '                End If
            '            Case enAllocation.UNIT
            '                If UserAccessLevel._AccessLevel.Length > 0 Then
            '                    strQ &= " AND  hremployee_master.unitunkid IN (" & UserAccessLevel._AccessLevel & ") "
            '                End If
            '            Case enAllocation.TEAM
            '                If UserAccessLevel._AccessLevel.Length > 0 Then
            '                    strQ &= " AND  hremployee_master.teamunkid IN (" & UserAccessLevel._AccessLevel & ") "
            '                End If
            '            Case enAllocation.JOB_GROUP
            '                If UserAccessLevel._AccessLevel.Length > 0 Then
            '                    strQ &= " AND  hremployee_master.jobgroupunkid IN (" & UserAccessLevel._AccessLevel & ") "
            '                End If
            '            Case enAllocation.JOBS
            '                If UserAccessLevel._AccessLevel.Length > 0 Then
            '                    strQ &= " AND hremployee_master.jobunkid IN (" & UserAccessLevel._AccessLevel & ") "
            '                End If
            '        End Select
            '    ElseIf enAssessMode = enAssessmentMode.APPRAISER_ASSESSMENT Then
            '        Select Case ConfigParameter._Object._UserAccessModeSetting
            '            Case enAllocation.BRANCH
            '                If UserAccessLevel._AccessLevel.Length > 0 Then
            '                    strQ &= " AND  hremployee_master.stationunkid IN (" & UserAccessLevel._AccessLevel & ") AND reviewerunkid < = 0 "
            '                End If
            '            Case enAllocation.DEPARTMENT_GROUP
            '                If UserAccessLevel._AccessLevel.Length > 0 Then
            '                    strQ &= " AND  hremployee_master.deptgroupunkid IN (" & UserAccessLevel._AccessLevel & ") AND reviewerunkid < = 0 "
            '                End If
            '            Case enAllocation.DEPARTMENT
            '                If UserAccessLevel._AccessLevel.Length > 0 Then
            '                    strQ &= " AND  hremployee_master.departmentunkid IN (" & UserAccessLevel._AccessLevel & ") AND reviewerunkid < = 0 "
            '                End If
            '            Case enAllocation.SECTION_GROUP
            '                If UserAccessLevel._AccessLevel.Length > 0 Then
            '                    strQ &= " AND  hremployee_master.sectiongroupunkid IN (" & UserAccessLevel._AccessLevel & ") AND reviewerunkid < = 0 "
            '                End If
            '            Case enAllocation.SECTION
            '                If UserAccessLevel._AccessLevel.Length > 0 Then
            '                    strQ &= " AND  hremployee_master.sectionunkid IN (" & UserAccessLevel._AccessLevel & ") AND reviewerunkid < = 0 "
            '                End If
            '            Case enAllocation.UNIT_GROUP
            '                If UserAccessLevel._AccessLevel.Length > 0 Then
            '                    strQ &= " AND  hremployee_master.unitgroupunkid IN (" & UserAccessLevel._AccessLevel & ") AND reviewerunkid < = 0 "
            '                End If
            '            Case enAllocation.UNIT
            '                If UserAccessLevel._AccessLevel.Length > 0 Then
            '                    strQ &= " AND  hremployee_master.unitunkid IN (" & UserAccessLevel._AccessLevel & ") AND reviewerunkid < = 0 "
            '                End If
            '            Case enAllocation.TEAM
            '                If UserAccessLevel._AccessLevel.Length > 0 Then
            '                    strQ &= " AND  hremployee_master.teamunkid IN (" & UserAccessLevel._AccessLevel & ") AND reviewerunkid < = 0 "
            '                End If
            '            Case enAllocation.JOB_GROUP
            '                If UserAccessLevel._AccessLevel.Length > 0 Then
            '                    strQ &= " AND  hremployee_master.jobgroupunkid IN (" & UserAccessLevel._AccessLevel & ") AND reviewerunkid < = 0 "
            '                End If
            '            Case enAllocation.JOBS
            '                If UserAccessLevel._AccessLevel.Length > 0 Then
            '                    strQ &= " AND hremployee_master.jobunkid IN (" & UserAccessLevel._AccessLevel & ") AND reviewerunkid < = 0 "
            '                End If
            '        End Select
            '    ElseIf enAssessMode = enAssessmentMode.REVIEWER_ASSESSMENT Then
            '        Select Case ConfigParameter._Object._UserAccessModeSetting
            '            Case enAllocation.BRANCH
            '                If UserAccessLevel._AccessLevel.Length > 0 Then
            '                    strQ &= " AND  hremployee_master.stationunkid IN (" & UserAccessLevel._AccessLevel & ") AND Reviewer.stationunkid IN (" & UserAccessLevel._AccessLevel & ") "
            '                End If
            '            Case enAllocation.DEPARTMENT_GROUP
            '                If UserAccessLevel._AccessLevel.Length > 0 Then
            '                    strQ &= " AND  hremployee_master.deptgroupunkid IN (" & UserAccessLevel._AccessLevel & ") AND Reviewer.deptgroupunkid IN (" & UserAccessLevel._AccessLevel & ") "
            '                End If
            '            Case enAllocation.DEPARTMENT
            '                If UserAccessLevel._AccessLevel.Length > 0 Then
            '                    strQ &= " AND  hremployee_master.departmentunkid IN (" & UserAccessLevel._AccessLevel & ") AND Reviewer.departmentunkid IN (" & UserAccessLevel._AccessLevel & ") "
            '                End If
            '            Case enAllocation.SECTION_GROUP
            '                If UserAccessLevel._AccessLevel.Length > 0 Then
            '                    strQ &= " AND  hremployee_master.sectiongroupunkid IN (" & UserAccessLevel._AccessLevel & ") AND Reviewer.sectiongroupunkid IN (" & UserAccessLevel._AccessLevel & ") "
            '                End If
            '            Case enAllocation.SECTION
            '                If UserAccessLevel._AccessLevel.Length > 0 Then
            '                    strQ &= " AND  hremployee_master.sectionunkid IN (" & UserAccessLevel._AccessLevel & ") AND Reviewer.sectionunkid IN (" & UserAccessLevel._AccessLevel & ") "
            '                End If
            '            Case enAllocation.UNIT_GROUP
            '                If UserAccessLevel._AccessLevel.Length > 0 Then
            '                    strQ &= " AND  hremployee_master.unitgroupunkid IN (" & UserAccessLevel._AccessLevel & ") AND Reviewer.unitgroupunkid IN (" & UserAccessLevel._AccessLevel & ") "
            '                End If
            '            Case enAllocation.UNIT
            '                If UserAccessLevel._AccessLevel.Length > 0 Then
            '                    strQ &= " AND  hremployee_master.unitunkid IN (" & UserAccessLevel._AccessLevel & ") AND Reviewer.unitunkid IN (" & UserAccessLevel._AccessLevel & ") "
            '                End If
            '            Case enAllocation.TEAM
            '                If UserAccessLevel._AccessLevel.Length > 0 Then
            '                    strQ &= " AND  hremployee_master.teamunkid IN (" & UserAccessLevel._AccessLevel & ") AND Reviewer.teamunkid IN (" & UserAccessLevel._AccessLevel & ") "
            '                End If
            '            Case enAllocation.JOB_GROUP
            '                If UserAccessLevel._AccessLevel.Length > 0 Then
            '                    strQ &= " AND  hremployee_master.jobgroupunkid IN (" & UserAccessLevel._AccessLevel & ") AND Reviewer.jobgroupunkid IN (" & UserAccessLevel._AccessLevel & ") "
            '                End If
            '            Case enAllocation.JOBS
            '                If UserAccessLevel._AccessLevel.Length > 0 Then
            '                    strQ &= " AND hremployee_master.jobunkid IN (" & UserAccessLevel._AccessLevel & ") AND Reviewer.jobunkid IN (" & UserAccessLevel._AccessLevel & ") "
            '                End If
            '        End Select
            '    End If
            'End If

                If enAssessMode = enAssessmentMode.SELF_ASSESSMENT Then
                If strUserAccessLevelFilterString = "" Then
                    'Sohail (08 May 2015) -- Start
                    'Enhancement - Get User Access Filter string from new employee transfer table and employee categorization table.
                    'strQ &= UserAccessLevel._AccessLevelFilterString
                    'strQ &= NewAccessLevelFilterString()
                    'Sohail (08 May 2015) -- End
                Else
                    strQ &= strUserAccessLevelFilterString
                            End If
                ElseIf enAssessMode = enAssessmentMode.APPRAISER_ASSESSMENT Then
                If strUserAccessLevelFilterString = "" Then
                    'Sohail (08 May 2015) -- Start
                    'Enhancement - Get User Access Filter string from new employee transfer table and employee categorization table.
                    'strQ &= UserAccessLevel._AccessLevelFilterString & " AND reviewerunkid < = 0 "
                    'strQ &= NewAccessLevelFilterString() & " AND reviewerunkid < = 0 "
                    'Sohail (08 May 2015) -- End
                Else
                    strQ &= strUserAccessLevelFilterString & " AND reviewerunkid < = 0 "
                            End If
                ElseIf enAssessMode = enAssessmentMode.REVIEWER_ASSESSMENT Then
                If strUserAccessLevelFilterString = "" Then
                    'Sohail (08 May 2015) -- Start
                    'Enhancement - Get User Access Filter string from new employee transfer table and employee categorization table.
                    'strQ &= UserAccessLevel._AccessLevelFilterString
                    'strQ &= NewAccessLevelFilterString()
                    'Sohail (08 May 2015) -- End
                Else
                    strQ &= strUserAccessLevelFilterString
                            End If

                If strUserAccessLevelFilterString = "" Then
                    'Sohail (08 May 2015) -- Start
                    'Enhancement - Get User Access Filter string from new employee transfer table and employee categorization table.
                    'strQ &= UserAccessLevel._AccessLevelFilterString.Replace("hremployee_master", "Reviewer")
                    'strQ &= NewAccessLevelFilterString().Replace("hremployee_master", "Reviewer")
                    'Sohail (08 May 2015) -- End
                Else
                    strQ &= strUserAccessLevelFilterString.Replace("hremployee_master", "Reviewer")
                End If

                
            End If
            'S.SANDEEP [ 27 APRIL 2012 ] -- END



            'S.SANDEEP [ 04 FEB 2012 ] -- END

            dsList = objDataOperation.ExecQuery(strQ, strTableName)

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            Return dsList

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: GetList; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
    End Function

    ''' <summary>
    ''' Modify By: Sandeep J. Sharma
    ''' </summary>
    ''' <returns>Boolean</returns>
    ''' <purpose> INSERT INTO Database Table (hrassess_analysis_master) </purpose>
    Public Function Insert(Optional ByVal mdtEvaluation As DataTable = Nothing, Optional ByVal mdtImprovement As DataTable = Nothing) As Boolean
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        'S.SANDEEP [ 10 SEPT 2013 ] -- START
        'ENHANCEMENT : TRA CHANGES
        Select Case mintAssessmodeid
            Case enAssessmentMode.SELF_ASSESSMENT
                If isExist(mintAssessmodeid, mintSelfemployeeunkid, mintYearunkid, mintPeriodunkid, mintAssessgroupunkid) = True Then
                    mstrMessage = Language.getMessage(mstrModuleName, 30, "Sorry, assessment already present for the selected period and employee. Please define new assessment.")
                    Return False
                End If
            Case enAssessmentMode.APPRAISER_ASSESSMENT
                If mintExt_Assessorunkid > 0 Then
                    If isExist(mintAssessmodeid, mintAssessedemployeeunkid, mintYearunkid, mintPeriodunkid, mintAssessgroupunkid, , mintExt_Assessorunkid, , , True) = True Then
                        mstrMessage = Language.getMessage(mstrModuleName, 30, "Sorry, assessment already present for the selected period and employee. Please define new assessment.")
                        Return False
                    End If
                Else
                    If isExist(mintAssessmodeid, mintAssessedemployeeunkid, mintYearunkid, mintPeriodunkid, mintAssessgroupunkid, , mintAssessormasterunkid) = True Then
                        mstrMessage = Language.getMessage(mstrModuleName, 30, "Sorry, assessment already present for the selected period and employee. Please define new assessment.")
                        Return False
                    End If
                End If
            Case enAssessmentMode.REVIEWER_ASSESSMENT
                If isExist(mintAssessmodeid, mintAssessedemployeeunkid, mintYearunkid, mintPeriodunkid, mintAssessgroupunkid, , , mintAssessormasterunkid) = True Then
                    mstrMessage = Language.getMessage(mstrModuleName, 30, "Sorry, assessment already present for the selected period and employee. Please define new assessment.")
                    Return False
                End If
        End Select
        'S.SANDEEP [ 10 SEPT 2013 ] -- END

        objDataOperation = New clsDataOperation

        objDataOperation.BindTransaction()

        Try
            objDataOperation.AddParameter("@yearunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintYearunkid.ToString)
            objDataOperation.AddParameter("@periodunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintPeriodunkid.ToString)
            objDataOperation.AddParameter("@selfemployeeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintSelfemployeeunkid.ToString)
            objDataOperation.AddParameter("@assessormasterunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintAssessormasterunkid.ToString)
            objDataOperation.AddParameter("@assessoremployeeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintAssessoremployeeunkid.ToString)
            objDataOperation.AddParameter("@assessedemployeeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintAssessedemployeeunkid.ToString)
            objDataOperation.AddParameter("@assessgroupunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintAssessgroupunkid.ToString)
            objDataOperation.AddParameter("@assessmentdate", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, mdtAssessmentdate)
            objDataOperation.AddParameter("@userunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintUserunkid.ToString)
            objDataOperation.AddParameter("@isvoid", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsvoid.ToString)
            objDataOperation.AddParameter("@voiduserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintVoiduserunkid.ToString)
            If mdtVoiddatetime = Nothing Then
                objDataOperation.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, DBNull.Value)
            Else
                objDataOperation.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, mdtVoiddatetime)
            End If
            objDataOperation.AddParameter("@voidreason", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrVoidreason.ToString)
            objDataOperation.AddParameter("@iscommitted", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIscommitted.ToString)
            objDataOperation.AddParameter("@reviewerunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintReviewerunkid.ToString)
            objDataOperation.AddParameter("@assessmodeid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintAssessmodeid.ToString)
            objDataOperation.AddParameter("@reviewer_remark1", SqlDbType.Text, eZeeDataType.NAME_SIZE, mstrReviewer_Remark1.ToString)
            objDataOperation.AddParameter("@reviewer_remark2", SqlDbType.Text, eZeeDataType.NAME_SIZE, mstrReviewer_Remark2.ToString)
            objDataOperation.AddParameter("@ext_assessorunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintExt_Assessorunkid.ToString)

            'S.SANDEEP [ 14 JUNE 2012 ] -- START
            'ENHANCEMENT : TRA CHANGES
            If mdtCommitteddatetime <> Nothing Then
                objDataOperation.AddParameter("@committeddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, mdtCommitteddatetime)
            Else
                objDataOperation.AddParameter("@committeddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, DBNull.Value)
            End If
            'strQ = "INSERT INTO hrassess_analysis_master ( " & _
            '  "  yearunkid " & _
            '  ", periodunkid " & _
            '  ", selfemployeeunkid " & _
            '  ", assessormasterunkid " & _
            '  ", assessoremployeeunkid " & _
            '  ", assessedemployeeunkid " & _
            '  ", assessgroupunkid " & _
            '  ", assessmentdate " & _
            '  ", userunkid " & _
            '  ", isvoid " & _
            '  ", voiduserunkid " & _
            '  ", voiddatetime " & _
            '  ", voidreason " & _
            '  ", iscommitted " & _
            '  ", reviewerunkid " & _
            '  ", assessmodeid " & _
            '  ", reviewer_remark1 " & _
            '  ", reviewer_remark2 " & _
            '  ", ext_assessorunkid " & _            
            '") VALUES (" & _
            '  "  @yearunkid " & _
            '  ", @periodunkid " & _
            '  ", @selfemployeeunkid " & _
            '  ", @assessormasterunkid " & _
            '  ", @assessoremployeeunkid " & _
            '  ", @assessedemployeeunkid " & _
            '  ", @assessgroupunkid " & _
            '  ", @assessmentdate " & _
            '  ", @userunkid " & _
            '  ", @isvoid " & _
            '  ", @voiduserunkid " & _
            '  ", @voiddatetime " & _
            '  ", @voidreason " & _
            '  ", @iscommitted " & _
            '  ", @reviewerunkid " & _
            '  ", @assessmodeid " & _
            '  ", @reviewer_remark1 " & _
            '  ", @reviewer_remark2 " & _
            '  ", @ext_assessorunkid " & _            
            '"); SELECT @@identity "
            strQ = "INSERT INTO hrassess_analysis_master ( " & _
              "  yearunkid " & _
              ", periodunkid " & _
              ", selfemployeeunkid " & _
              ", assessormasterunkid " & _
              ", assessoremployeeunkid " & _
              ", assessedemployeeunkid " & _
              ", assessgroupunkid " & _
              ", assessmentdate " & _
              ", userunkid " & _
              ", isvoid " & _
              ", voiduserunkid " & _
              ", voiddatetime " & _
              ", voidreason " & _
              ", iscommitted " & _
              ", reviewerunkid " & _
              ", assessmodeid " & _
              ", reviewer_remark1 " & _
              ", reviewer_remark2 " & _
              ", ext_assessorunkid" & _
                      ", committeddatetime " & _
            ") VALUES (" & _
              "  @yearunkid " & _
              ", @periodunkid " & _
              ", @selfemployeeunkid " & _
              ", @assessormasterunkid " & _
              ", @assessoremployeeunkid " & _
              ", @assessedemployeeunkid " & _
              ", @assessgroupunkid " & _
              ", @assessmentdate " & _
              ", @userunkid " & _
              ", @isvoid " & _
              ", @voiduserunkid " & _
              ", @voiddatetime " & _
              ", @voidreason " & _
              ", @iscommitted " & _
              ", @reviewerunkid " & _
              ", @assessmodeid " & _
              ", @reviewer_remark1 " & _
              ", @reviewer_remark2 " & _
              ", @ext_assessorunkid " & _
              ", @committeddatetime " & _
            "); SELECT @@identity"
            'S.SANDEEP [ 14 JUNE 2012 ] -- END
            

            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            mintAnalysisunkid = dsList.Tables(0).Rows(0).Item(0)

            If mdtEvaluation IsNot Nothing Then
                If mdtEvaluation.Rows.Count > 0 Then

                    'S.SANDEEP [ 27 APRIL 2012 ] -- START
                    'ENHANCEMENT : TRA CHANGES
                    objAnalysisTran._DataOperation = objDataOperation
                    'S.SANDEEP [ 27 APRIL 2012 ] -- END

                    objAnalysisTran._AnalysisUnkid = mintAnalysisunkid
                    objAnalysisTran._DataTable = mdtEvaluation

                    'S.SANDEEP [ 27 APRIL 2012 ] -- START
                    'ENHANCEMENT : TRA CHANGES
                    If objAnalysisTran.InsertUpdateDelete_AnalysisTran(mintUserunkid) = False Then
                        'If objAnalysisTran.InsertUpdateDelete_AnalysisTran() = False Then
                        'S.SANDEEP [ 27 APRIL 2012 ] -- END
                        objDataOperation.ReleaseTransaction(False)
                        Return False
                    End If
                End If
            End If

            If mdtImprovement IsNot Nothing Then
                If mdtImprovement.Rows.Count > 0 Then

                    'S.SANDEEP [ 27 APRIL 2012 ] -- START
                    'ENHANCEMENT : TRA CHANGES
                    objRemarkTran._DataOperation = objDataOperation
                    'S.SANDEEP [ 27 APRIL 2012 ] -- END

                    objRemarkTran._AnalysisUnkid = mintAnalysisunkid
                    objRemarkTran._DataTable = mdtImprovement

                    'S.SANDEEP [ 27 APRIL 2012 ] -- START
                    'ENHANCEMENT : TRA CHANGES
                    If objRemarkTran.InsertUpdateDelete_RemarksTran(mintUserunkid) = False Then
                        'If objRemarkTran.InsertUpdateDelete_RemarksTran() = False Then
                        'S.SANDEEP [ 27 APRIL 2012 ] -- END
                        objDataOperation.ReleaseTransaction(False)
                        Return False
                    End If
                End If
            End If

            
            objDataOperation.ReleaseTransaction(True)
            Return True
        Catch ex As Exception
            objDataOperation.ReleaseTransaction(False)
            Throw New Exception(ex.Message & "; Procedure Name: Insert; Module Name: " & mstrModuleName)
            Return False
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
    End Function

    ''' <summary>
    ''' Modify By: Sandeep J. Sharma
    ''' </summary>
    ''' <returns>Boolean</returns>
    ''' <purpose> Update Database Table (hrassess_analysis_master) </purpose>
    Public Function Update(Optional ByVal mdtEvaluation As DataTable = Nothing, Optional ByVal mdtImprovement As DataTable = Nothing) As Boolean
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        'S.SANDEEP [ 10 SEPT 2013 ] -- START
        'ENHANCEMENT : TRA CHANGES
        Select Case mintAssessmodeid
            Case enAssessmentMode.SELF_ASSESSMENT
                If isExist(mintAssessmodeid, mintSelfemployeeunkid, mintYearunkid, mintPeriodunkid, mintAssessgroupunkid, , , , mintAnalysisunkid) = True Then
                    mstrMessage = Language.getMessage(mstrModuleName, 30, "Sorry, assessment already present for the selected period and employee. Please define new assessment.")
                    Return False
                End If
            Case enAssessmentMode.APPRAISER_ASSESSMENT
                If mintExt_Assessorunkid > 0 Then
                    If isExist(mintAssessmodeid, mintAssessedemployeeunkid, mintYearunkid, mintPeriodunkid, mintAssessgroupunkid, , mintExt_Assessorunkid, , mintAnalysisunkid, True) = True Then
                        mstrMessage = Language.getMessage(mstrModuleName, 30, "Sorry, assessment already present for the selected period and employee. Please define new assessment.")
                        Return False
                    End If
                Else
                    If isExist(mintAssessmodeid, mintAssessedemployeeunkid, mintYearunkid, mintPeriodunkid, mintAssessgroupunkid, , mintAssessormasterunkid, , mintAnalysisunkid) = True Then
                        mstrMessage = Language.getMessage(mstrModuleName, 30, "Sorry, assessment already present for the selected period and employee. Please define new assessment.")
                        Return False
                    End If
                End If
            Case enAssessmentMode.REVIEWER_ASSESSMENT
                If isExist(mintAssessmodeid, mintAssessedemployeeunkid, mintYearunkid, mintPeriodunkid, mintAssessgroupunkid, , , mintAssessormasterunkid, mintAnalysisunkid) = True Then
                    mstrMessage = Language.getMessage(mstrModuleName, 30, "Sorry, assessment already present for the selected period and employee. Please define new assessment.")
                    Return False
                End If
        End Select
        'S.SANDEEP [ 10 SEPT 2013 ] -- END

        objDataOperation = New clsDataOperation

        Try
            objDataOperation.BindTransaction()

            objDataOperation.AddParameter("@analysisunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintAnalysisunkid.ToString)
            objDataOperation.AddParameter("@yearunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintYearunkid.ToString)
            objDataOperation.AddParameter("@periodunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintPeriodunkid.ToString)
            objDataOperation.AddParameter("@selfemployeeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintSelfemployeeunkid.ToString)
            objDataOperation.AddParameter("@assessormasterunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintAssessormasterunkid.ToString)
            objDataOperation.AddParameter("@assessoremployeeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintAssessoremployeeunkid.ToString)
            objDataOperation.AddParameter("@assessedemployeeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintAssessedemployeeunkid.ToString)
            objDataOperation.AddParameter("@assessgroupunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintAssessgroupunkid.ToString)
            objDataOperation.AddParameter("@assessmentdate", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, mdtAssessmentdate)
            objDataOperation.AddParameter("@userunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintUserunkid.ToString)
            objDataOperation.AddParameter("@isvoid", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsvoid.ToString)
            objDataOperation.AddParameter("@voiduserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintVoiduserunkid.ToString)
            If mdtVoiddatetime = Nothing Then
                objDataOperation.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, DBNull.Value)
            Else
                objDataOperation.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, mdtVoiddatetime)
            End If
            objDataOperation.AddParameter("@voidreason", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrVoidreason.ToString)
            objDataOperation.AddParameter("@iscommitted", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIscommitted.ToString)
            objDataOperation.AddParameter("@reviewerunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintReviewerunkid.ToString)
            objDataOperation.AddParameter("@assessmodeid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintAssessmodeid.ToString)
            objDataOperation.AddParameter("@reviewer_remark1", SqlDbType.Text, eZeeDataType.NAME_SIZE, mstrReviewer_Remark1.ToString)
            objDataOperation.AddParameter("@reviewer_remark2", SqlDbType.Text, eZeeDataType.NAME_SIZE, mstrReviewer_Remark2.ToString)
            objDataOperation.AddParameter("@ext_assessorunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintExt_Assessorunkid.ToString)

            'S.SANDEEP [ 14 JUNE 2012 ] -- START
            'ENHANCEMENT : TRA CHANGES
            If mdtCommitteddatetime <> Nothing Then
                objDataOperation.AddParameter("@committeddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, mdtCommitteddatetime)
            Else
                objDataOperation.AddParameter("@committeddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, DBNull.Value)
            End If
            'strQ = "UPDATE hrassess_analysis_master SET " & _
            '  "  yearunkid = @yearunkid" & _
            '  ", periodunkid = @periodunkid" & _
            '  ", selfemployeeunkid = @selfemployeeunkid" & _
            '  ", assessormasterunkid = @assessormasterunkid" & _
            '  ", assessoremployeeunkid = @assessoremployeeunkid" & _
            '  ", assessedemployeeunkid = @assessedemployeeunkid" & _
            '  ", assessgroupunkid = @assessgroupunkid" & _
            '  ", assessmentdate = @assessmentdate" & _
            '  ", userunkid = @userunkid" & _
            '  ", isvoid = @isvoid" & _
            '  ", voiduserunkid = @voiduserunkid" & _
            '  ", voiddatetime = @voiddatetime" & _
            '  ", voidreason = @voidreason" & _
            '  ", iscommitted = @iscommitted" & _
            '  ", reviewerunkid = @reviewerunkid" & _
            '  ", assessmodeid = @assessmodeid" & _
            '  ", reviewer_remark1 = @reviewer_remark1" & _
            '  ", reviewer_remark2 = @reviewer_remark2 " & _
            '  ", ext_assessorunkid = @ext_assessorunkid " & _
            '"WHERE analysisunkid = @analysisunkid "
            strQ = "UPDATE hrassess_analysis_master SET " & _
              "  yearunkid = @yearunkid" & _
              ", periodunkid = @periodunkid" & _
              ", selfemployeeunkid = @selfemployeeunkid" & _
              ", assessormasterunkid = @assessormasterunkid" & _
              ", assessoremployeeunkid = @assessoremployeeunkid" & _
              ", assessedemployeeunkid = @assessedemployeeunkid" & _
              ", assessgroupunkid = @assessgroupunkid" & _
              ", assessmentdate = @assessmentdate" & _
              ", userunkid = @userunkid" & _
              ", isvoid = @isvoid" & _
              ", voiduserunkid = @voiduserunkid" & _
              ", voiddatetime = @voiddatetime" & _
              ", voidreason = @voidreason" & _
              ", iscommitted = @iscommitted" & _
              ", reviewerunkid = @reviewerunkid" & _
              ", assessmodeid = @assessmodeid" & _
              ", reviewer_remark1 = @reviewer_remark1" & _
              ", reviewer_remark2 = @reviewer_remark2 " & _
              ", ext_assessorunkid = @ext_assessorunkid " & _
              ", committeddatetime = @committeddatetime " & _
            "WHERE analysisunkid = @analysisunkid "
            'S.SANDEEP [ 14 JUNE 2012 ] -- END

            Call objDataOperation.ExecNonQuery(strQ)

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            If mdtEvaluation IsNot Nothing Then
                If mdtEvaluation.Rows.Count > 0 Then

                    'S.SANDEEP [ 27 APRIL 2012 ] -- START
                    'ENHANCEMENT : TRA CHANGES
                    objAnalysisTran._DataOperation = objDataOperation
                    'S.SANDEEP [ 27 APRIL 2012 ] -- END

                    objAnalysisTran._AnalysisUnkid = mintAnalysisunkid
                    objAnalysisTran._DataTable = mdtEvaluation.Copy

                    'S.SANDEEP [ 27 APRIL 2012 ] -- START
                    'ENHANCEMENT : TRA CHANGES
                    If objAnalysisTran.InsertUpdateDelete_AnalysisTran(mintUserunkid) = False Then
                        'If objAnalysisTran.InsertUpdateDelete_AnalysisTran() = False Then
                        'S.SANDEEP [ 27 APRIL 2012 ] -- END
                        objDataOperation.ReleaseTransaction(False)
                        Return False
                    End If
                End If
            End If

            If mdtImprovement IsNot Nothing Then
                If mdtImprovement.Rows.Count > 0 Then

                    'S.SANDEEP [ 27 APRIL 2012 ] -- START
                    'ENHANCEMENT : TRA CHANGES
                    objRemarkTran._DataOperation = objDataOperation
                    'S.SANDEEP [ 27 APRIL 2012 ] -- END

                    objRemarkTran._AnalysisUnkid = mintAnalysisunkid
                    objRemarkTran._DataTable = mdtImprovement

                    'S.SANDEEP [ 27 APRIL 2012 ] -- START
                    'ENHANCEMENT : TRA CHANGES
                    If objRemarkTran.InsertUpdateDelete_RemarksTran(mintUserunkid) = False Then
                        'If objRemarkTran.InsertUpdateDelete_RemarksTran() = False Then
                        'S.SANDEEP [ 27 APRIL 2012 ] -- END
                        objDataOperation.ReleaseTransaction(False)
                        Return False
                    End If
                End If
            End If

            objDataOperation.ReleaseTransaction(True)

            Return True
        Catch ex As Exception
            objDataOperation.ReleaseTransaction(False)
            Throw New Exception(ex.Message & "; Procedure Name: Update; Module Name: " & mstrModuleName)
            Return False
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
    End Function

    ''' <summary>
    ''' Modify By: Sandeep J. Sharma
    ''' </summary>
    ''' <returns>Boolean</returns>
    ''' <purpose> Delete Database Table (hrassess_analysis_master) </purpose>
    Public Function Delete(ByVal intUnkid As Integer, ByVal iPeriodId As Integer) As Boolean 'S.SANDEEP [ 01 JUL 2014 ] -- START {iPeriodId} -- END
        'Public Function Delete(ByVal intUnkid As Integer) As Boolean
        If isUsed(intUnkid, iPeriodId) Then 'S.SANDEEP [ 01 JUL 2014 ] -- START {iPeriodId} -- END
            'If isUsed(intUnkid) Then
            mstrMessage = Language.getMessage(mstrModuleName, 2, "Sorry, You cannot delete this Assessment. Reason : This Assessor has already assessed this employee.")
            Return False
        End If

        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        objDataOperation = New clsDataOperation
        objDataOperation.BindTransaction()

        Try
            strQ = "UPDATE hrassess_analysis_master SET " & _
                    "  isvoid = @isvoid" & _
                    ", voiduserunkid = @voiduserunkid" & _
                    ", voiddatetime = @voiddatetime" & _
                    ", voidreason = @voidreason " & _
                  "WHERE analysisunkid = @analysisunkid "

            objDataOperation.AddParameter("@isvoid", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsvoid.ToString)
            objDataOperation.AddParameter("@voiduserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintVoiduserunkid.ToString)
            objDataOperation.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, mdtVoiddatetime)
            objDataOperation.AddParameter("@voidreason", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrVoidreason.ToString)

            objDataOperation.AddParameter("@analysisunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intUnkid)

            Call objDataOperation.ExecNonQuery(strQ)

            If objDataOperation.ErrorMessage <> "" Then
                objDataOperation.ReleaseTransaction(False)
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            'dsList = clsCommonATLog.GetChildList(objDataOperation, "hrassess_analysis_tran", "analysisunkid", intUnkid)

            If dsList.Tables(0).Rows.Count > 0 Then
                For Each dtRow As DataRow In dsList.Tables(0).Rows
                    strQ = "UPDATE hrassess_analysis_tran SET " & _
                           "  isvoid = @isvoid" & _
                           ", voiduserunkid = @voiduserunkid " & _
                           ", voiddatetime = @voiddatetime " & _
                           ", voidreason = @voidreason " & _
                           "WHERE analysistranunkid = '" & dtRow.Item("analysistranunkid") & "' "

                    objDataOperation.ClearParameters()
                    objDataOperation.AddParameter("@isvoid", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsvoid.ToString)
                    objDataOperation.AddParameter("@voiduserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintVoiduserunkid.ToString)
                    objDataOperation.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, mdtVoiddatetime)
                    objDataOperation.AddParameter("@voidreason", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrVoidreason.ToString)

                    Call objDataOperation.ExecNonQuery(strQ)

                    If objDataOperation.ErrorMessage <> "" Then
                        objDataOperation.ReleaseTransaction(False)
                        exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                        Throw exForce
                    End If

                    'S.SANDEEP [ 27 APRIL 2012 ] -- START
                    'ENHANCEMENT : TRA CHANGES
                    'If clsCommonATLog.Insert_TranAtLog(objDataOperation, "hrassess_analysis_master", "analysisunkid", intUnkid, "hrassess_analysis_tran", "analysistranunkid", dtRow.Item("analysistranunkid"), 3, 3) = False Then
                    '    If objDataOperation.ErrorMessage <> "" Then
                    '        objDataOperation.ReleaseTransaction(False)
                    '        exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                    '        Throw exForce
                    '    End If
                    'End If

                    'If clsCommonATLog.Insert_TranAtLog(objDataOperation, "hrassess_analysis_master", "analysisunkid", intUnkid, "hrassess_analysis_tran", "analysistranunkid", dtRow.Item("analysistranunkid"), 3, 3, , mintVoiduserunkid) = False Then
                    '    If objDataOperation.ErrorMessage <> "" Then
                    '        objDataOperation.ReleaseTransaction(False)
                    '        exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                    '        Throw exForce
                    '    End If
                    'End If
                    'S.SANDEEP [ 27 APRIL 2012 ] -- END
                Next
            End If

            'dsList = clsCommonATLog.GetChildList(objDataOperation, "hrassess_remarks_tran", "analysisunkid", intUnkid)
            If dsList.Tables(0).Rows.Count > 0 Then
                For Each dtRow As DataRow In dsList.Tables(0).Rows
                    strQ = "UPDATE hrassess_remarks_tran SET " & _
                           "  isvoid = @isvoid" & _
                           ", voiduserunkid = @voiduserunkid " & _
                           ", voiddatetime = @voiddatetime " & _
                           ", voidreason = @voidreason " & _
                           "WHERE remarkstranunkid = '" & dtRow.Item("remarkstranunkid") & "' "


                    objDataOperation.ClearParameters()
                    objDataOperation.AddParameter("@isvoid", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsvoid.ToString)
                    objDataOperation.AddParameter("@voiduserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintVoiduserunkid.ToString)
                    objDataOperation.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, mdtVoiddatetime)
                    objDataOperation.AddParameter("@voidreason", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrVoidreason.ToString)
                    Call objDataOperation.ExecNonQuery(strQ)

                    If objDataOperation.ErrorMessage <> "" Then
                        objDataOperation.ReleaseTransaction(False)
                        exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                        Throw exForce
                    End If

                    'S.SANDEEP [ 27 APRIL 2012 ] -- START
                    'ENHANCEMENT : TRA CHANGES
                    'If clsCommonATLog.Insert_TranAtLog(objDataOperation, "hrassess_analysis_master", "analysisunkid", intUnkid, "hrassess_remarks_tran", "remarkstranunkid", dtRow.Item("remarkstranunkid"), 3, 3) = False Then
                    '    If objDataOperation.ErrorMessage <> "" Then
                    '        objDataOperation.ReleaseTransaction(False)
                    '        exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                    '        Throw exForce
                    '    End If
                    'End If

                    'If clsCommonATLog.Insert_TranAtLog(objDataOperation, "hrassess_analysis_master", "analysisunkid", intUnkid, "hrassess_remarks_tran", "remarkstranunkid", dtRow.Item("remarkstranunkid"), 3, 3, , mintVoiduserunkid) = False Then
                    '    If objDataOperation.ErrorMessage <> "" Then
                    '        objDataOperation.ReleaseTransaction(False)
                    '        exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                    '        Throw exForce
                    '    End If
                    'End If
                    'S.SANDEEP [ 27 APRIL 2012 ] -- END
                Next
            End If

            objDataOperation.ReleaseTransaction(True)

            Return True
        Catch ex As Exception
            objDataOperation.ReleaseTransaction(False)
            Throw New Exception(ex.Message & "; Procedure Name: Update; Module Name: " & mstrModuleName)
            Return False
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
    End Function

    ''' <summary>
    ''' Modify By: Sandeep J. Sharma
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>
    Public Function isUsed(ByVal intUnkid As Integer, ByVal iPrdId As Integer) As Boolean 'S.SANDEEP [ 01 JUL 2014 ] -- START {iPrdId} -- END
        'Public Function isUsed(ByVal intUnkid As Integer) As Boolean
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        objDataOperation = New clsDataOperation

        Try
            strQ = "SELECT  assessedemployeeunkid FROM hrassess_analysis_master " & _
                   "WHERE assessedemployeeunkid IN (SELECT selfemployeeunkid FROM hrassess_analysis_master WHERE analysisunkid = @analysisunkid AND periodunkid = @PrdId) AND isVoid = 0 AND assessedemployeeunkid > 0 AND periodunkid = @PrdId  "

            'S.SANDEEP [ 01 JUL 2014 ] -- START
            'AND periodunkid = @PrdId -- ADDED
            'S.SANDEEP [ 01 JUL 2014 ] -- END

            objDataOperation.AddParameter("@analysisunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intUnkid)
            'S.SANDEEP [ 01 JUL 2014 ] -- START
            objDataOperation.AddParameter("@PrdId", SqlDbType.Int, eZeeDataType.INT_SIZE, iPrdId)
            'S.SANDEEP [ 01 JUL 2014 ] -- END

            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            Return dsList.tables(0).rows.count > 0
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: isUsed; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
    End Function

    ''' <summary>
    ''' Modify By: Sandeep J. Sharma
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>
    Public Function isExist(ByVal enAssessMode As enAssessmentMode, _
                            ByVal intEmpId As Integer, _
                            ByVal intYearId As Integer, _
                            ByVal intPeriodId As Integer, _
                            ByVal intAssessGrpId As Integer, _
                            Optional ByVal intSubItemId As Integer = -1, _
                            Optional ByVal intAssessorId As Integer = -1, _
                            Optional ByVal intReviewerId As Integer = -1, _
                            Optional ByVal intUnkid As Integer = -1, Optional ByVal blnIsExAssessor As Boolean = False) As Boolean
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        objDataOperation = New clsDataOperation

        Try
            strQ = "SELECT " & _
              "  hrassess_analysis_master.analysisunkid " & _
              ", hrassess_analysis_master.yearunkid " & _
              ", hrassess_analysis_master.periodunkid " & _
              ", hrassess_analysis_master.selfemployeeunkid " & _
              ", hrassess_analysis_master.assessormasterunkid " & _
              ", hrassess_analysis_master.assessoremployeeunkid " & _
              ", hrassess_analysis_master.assessedemployeeunkid " & _
              ", hrassess_analysis_master.assessgroupunkid " & _
              ", hrassess_analysis_master.assessmentdate " & _
              ", hrassess_analysis_master.userunkid " & _
              ", hrassess_analysis_master.isvoid " & _
              ", hrassess_analysis_master.voiduserunkid " & _
              ", hrassess_analysis_master.voiddatetime " & _
              ", hrassess_analysis_master.voidreason " & _
              ", hrassess_analysis_master.iscommitted " & _
              ", hrassess_analysis_master.reviewerunkid " & _
              ", hrassess_analysis_master.assessmodeid " & _
              ", hrassess_analysis_master.reviewer_remark1 " & _
              ", hrassess_analysis_master.reviewer_remark2 " & _
              ", hrassess_analysis_tran.assess_subitemunkid " & _
             "FROM hrassess_analysis_master " & _
             " JOIN hrassess_analysis_tran ON hrassess_analysis_tran.analysisunkid = hrassess_analysis_master.analysisunkid " & _
             "WHERE ISNULL(hrassess_analysis_master.isvoid,0)= 0 AND hrassess_analysis_master.yearunkid  = @YearId AND hrassess_analysis_master.periodunkid  = @PeriodId " & _
             " AND hrassess_analysis_master.assessgroupunkid = @GrpId"


            If intSubItemId > 0 Then
                strQ &= " AND hrassess_analysis_tran.assess_subitemunkid = @ItemId "
            End If

            Select Case enAssessMode
                Case enAssessmentMode.SELF_ASSESSMENT
                    strQ &= " AND hrassess_analysis_master.selfemployeeunkid = @EmpId "
                Case Else
                    strQ &= " AND hrassess_analysis_master.assessedemployeeunkid = @EmpId "
                    'S.SANDEEP [ 14 AUG 2013 ] -- START
                    'ENHANCEMENT : TRA CHANGES
                    strQ &= " AND assessmodeid = " & enAssessMode
                    'S.SANDEEP [ 14 AUG 2013 ] -- END
            End Select

            If intAssessorId > 0 Then
                If blnIsExAssessor = False Then
                strQ &= "AND hrassess_analysis_master.assessormasterunkid = @AssessorId "
                Else
                    strQ &= "AND hrassess_analysis_master.ext_assessorunkid = @AssessorId "
                End If
                objDataOperation.AddParameter("@AssessorId", SqlDbType.Int, eZeeDataType.INT_SIZE, intAssessorId)
            End If

            If intReviewerId > 0 Then
                strQ &= "AND hrassess_analysis_master.assessormasterunkid = @reviewerunkid "
                objDataOperation.AddParameter("@reviewerunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intReviewerId)
            End If

            If intUnkid > 0 Then
                strQ &= " AND hrassess_analysis_master.analysisunkid <> @analysisunkid"
                objDataOperation.AddParameter("@analysisunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intUnkid)
            End If

            objDataOperation.AddParameter("@EmpId", SqlDbType.Int, eZeeDataType.INT_SIZE, intEmpId)
            objDataOperation.AddParameter("@YearId", SqlDbType.Int, eZeeDataType.INT_SIZE, intYearId)
            objDataOperation.AddParameter("@PeriodId", SqlDbType.Int, eZeeDataType.INT_SIZE, intPeriodId)
            objDataOperation.AddParameter("@ItemId", SqlDbType.Int, eZeeDataType.INT_SIZE, intSubItemId)


            'S.SANDEEP [ 04 FEB 2012 ] -- START
            'ENHANCEMENT : TRA CHANGES
            objDataOperation.AddParameter("@GrpId", SqlDbType.Int, eZeeDataType.INT_SIZE, intAssessGrpId)
            'S.SANDEEP [ 04 FEB 2012 ] -- END

            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            Return dsList.Tables(0).Rows.Count > 0
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: isExist; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
    End Function

    Public Function GetAssessorEmpId(ByVal intAssessorId As Integer) As Integer
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception
        Dim intEmpId As Integer = -1
        objDataOperation = New clsDataOperation
        Try

            strQ = "SELECT employeeunkid AS EId FROM hrassessor_master WHERE assessormasterunkid = @AssessorId AND hrassessor_master.isvoid = 0 "

            objDataOperation.AddParameter("@AssessorId", SqlDbType.Int, eZeeDataType.INT_SIZE, intAssessorId)

            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            If dsList.Tables(0).Rows.Count > 0 Then
                intEmpId = dsList.Tables(0).Rows(0)(0)
            End If

            Return intEmpId

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "", mstrModuleName)
        End Try
    End Function

    Public Function getAssessorComboList(Optional ByVal strList As String = "List", _
                                         Optional ByVal blnFlag As Boolean = False, _
                                         Optional ByVal blnIsReviewer As Boolean = False, _
                                         Optional ByVal intUserUnkid As Integer = 0, _
                                         Optional ByVal strIncludeInactiveEmployee As String = "", _
                                         Optional ByVal strEmployeeAsOnDate As String = "") As DataSet 'S.SANDEEP [ 27 APRIL 2012 ] -- START -- END
        'Public Function getAssessorComboList(Optional ByVal strList As String = "List", Optional ByVal blnFlag As Boolean = False, Optional ByVal blnIsReviewer As Boolean = False) As DataSet
        Dim dsList As DataSet = Nothing
        Dim StrQ As String = ""
        Dim exForce As Exception

        objDataOperation = New clsDataOperation

        Try
            If blnFlag = True Then
                StrQ = "SELECT 0 AS Id, @Select AS Name,'' AS Code UNION "
            End If

            StrQ &= "SELECT " & _
                    "	 assessormasterunkid AS Id " & _
                    "	,ISNULL(hremployee_master.firstname,'')+' '+ISNULL(hremployee_master.othername,'')+' '+ISNULL(hremployee_master.surname,'') AS Name " & _
                    "   ,ISNULL(hremployee_master.employeecode,'') AS Code " & _
                    "FROM hrassessor_master " & _
                    "	JOIN hrapprover_usermapping ON hrassessor_master.assessormasterunkid = hrapprover_usermapping.approverunkid " & _
                    "	JOIN hremployee_master ON hrassessor_master.employeeunkid = hremployee_master.employeeunkid " & _
                    "WHERE hrapprover_usermapping.usertypeid = " & enUserType.Assessor & " " & _
                    "	AND hrapprover_usermapping.userunkid = " & IIf(intUserUnkid <= 0, User._Object._Userunkid, intUserUnkid) & " AND hrassessor_master.isvoid = 0 "
            'S.SANDEEP [ 14 JUNE 2012 ] -- START -- END
            If blnIsReviewer Then
                StrQ &= "	AND hrassessor_master.isreviewer = 1 "
            Else
                StrQ &= "	AND hrassessor_master.isreviewer = 0 "
            End If

            'S.SANDEEP [ 27 APRIL 2012 ] -- START
            'ENHANCEMENT : TRA CHANGES
            'If ConfigParameter._Object._IsIncludeInactiveEmp = False Then
            '    StrQ &= " AND CONVERT(CHAR(8),hremployee_master.appointeddate,112) <= @enddate " & _
            '               " AND ISNULL(CONVERT(CHAR(8),hremployee_master.termination_from_date,112),@startdate) >= @startdate " & _
            '               " AND ISNULL(CONVERT(CHAR(8),hremployee_master.termination_to_date,112),@startdate) >= @startdate "

            '    objDataOperation.AddParameter("@startdate", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, ConfigParameter._Object._EmployeeAsOnDate)
            '    objDataOperation.AddParameter("@enddate", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, ConfigParameter._Object._EmployeeAsOnDate)
            'End If

            If strIncludeInactiveEmployee.Trim.Length <= 0 Then
                strIncludeInactiveEmployee = ConfigParameter._Object._IsIncludeInactiveEmp.ToString
            End If

            If CBool(strIncludeInactiveEmployee) = False Then
                StrQ &= " AND CONVERT(CHAR(8),hremployee_master.appointeddate,112) <= @enddate " & _
                           " AND ISNULL(CONVERT(CHAR(8),hremployee_master.termination_from_date,112),@startdate) >= @startdate " & _
                           " AND ISNULL(CONVERT(CHAR(8),hremployee_master.termination_to_date,112),@startdate) >= @startdate " & _
                           " AND ISNULL(CONVERT(CHAR(8),hremployee_master.empl_enddate,112), @startdate) >= @startdate " 'Sohail (12 May 2012) - [empl_enddate]

                objDataOperation.AddParameter("@startdate", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, IIf(strEmployeeAsOnDate = "", ConfigParameter._Object._EmployeeAsOnDate, strEmployeeAsOnDate))
                objDataOperation.AddParameter("@enddate", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, IIf(strEmployeeAsOnDate = "", ConfigParameter._Object._EmployeeAsOnDate, strEmployeeAsOnDate))
            End If
            'S.SANDEEP [ 27 APRIL 2012 ] -- END



            objDataOperation.AddParameter("@Select", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 1, "Select"))

            dsList = objDataOperation.ExecQuery(StrQ, strList)

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & " : " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            Return dsList

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "getAssessorComboList", mstrModuleName)
            Return Nothing
        End Try
    End Function

    Public Function getEmployeeBasedAssessor(ByVal intAssessorId As Integer, _
                                             Optional ByVal strList As String = "List", _
                                             Optional ByVal blnFlag As Boolean = False, _
                                             Optional ByVal strIncludeInactiveEmployee As String = "", _
                                             Optional ByVal strEmployeeAsOnDate As String = "", _
                                             Optional ByVal intUserUnkId As Integer = 0) As DataSet 'S.SANDEEP [ 27 APRIL 2012 ] -- START-- END

        'Public Function getEmployeeBasedAssessor(ByVal intAssessorId As Integer, _
        '                                     Optional ByVal strList As String = "List", _
        '                                     Optional ByVal blnFlag As Boolean = False) As DataSet
        Dim dsList As DataSet = Nothing
        Dim StrQ As String = ""
        Dim exForce As Exception

        objDataOperation = New clsDataOperation

        Try
            If blnFlag = True Then
                StrQ = "SELECT  0 AS Id,@Select AS NAME,'' AS Code UNION "
            End If

            StrQ &= "SELECT " & _
                    "	 employeeunkid AS Id " & _
                    "	,ISNULL(hremployee_master.firstname,'')+' '+ISNULL(hremployee_master.othername,'')+' '+ISNULL(hremployee_master.surname,'') AS NAME " & _
                    "	,ISNULL(hremployee_master.employeecode,'') AS Code " & _
                    "FROM hremployee_master " & _
                    "WHERE employeeunkid " & _
                    "	IN " & _
                    "	( " & _
                    "		SELECT " & _
                    "			hrassessor_tran.employeeunkid " & _
                    "		FROM hrassessor_tran " & _
                    "			JOIN hrassessor_master ON hrassessor_tran.assessormasterunkid = hrassessor_master.assessormasterunkid " & _
                    "			JOIN hrapprover_usermapping ON hrapprover_usermapping.approverunkid = hrassessor_master.assessormasterunkid " & _
                    "		WHERE hrassessor_master.assessormasterunkid = @AssessorId " & _
                    "			AND hrapprover_usermapping.userunkid = " & IIf(intUserUnkId <= 0, User._Object._Userunkid, intUserUnkId) & " " & _
                    "           AND hrapprover_usermapping.usertypeid = " & enUserType.Assessor & " AND hrassessor_tran.isvoid = 0 AND hrassessor_master.isvoid = 0 " & _
                    "	) "

            'S.SANDEEP [ 27 APRIL 2012 ] -- START
            'ENHANCEMENT : TRA CHANGES
            'If ConfigParameter._Object._IsIncludeInactiveEmp = False Then
            '    StrQ &= " AND CONVERT(CHAR(8),hremployee_master.appointeddate,112) <= @enddate " & _
            '            " AND ISNULL(CONVERT(CHAR(8),hremployee_master.termination_from_date,112),@startdate) >= @startdate " & _
            '            " AND ISNULL(CONVERT(CHAR(8),hremployee_master.termination_to_date,112),@startdate) >= @startdate "

            '    objDataOperation.AddParameter("@startdate", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, ConfigParameter._Object._EmployeeAsOnDate)
            '    objDataOperation.AddParameter("@enddate", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, ConfigParameter._Object._EmployeeAsOnDate)
            'End If

            If strIncludeInactiveEmployee.Trim.Length <= 0 Then
                strIncludeInactiveEmployee = ConfigParameter._Object._IsIncludeInactiveEmp.ToString
            End If

            If CBool(strIncludeInactiveEmployee) = False Then
                StrQ &= " AND CONVERT(CHAR(8),hremployee_master.appointeddate,112) <= @enddate " & _
                        " AND ISNULL(CONVERT(CHAR(8),hremployee_master.termination_from_date,112),@startdate) >= @startdate " & _
                        " AND ISNULL(CONVERT(CHAR(8),hremployee_master.termination_to_date,112),@startdate) >= @startdate " & _
                        " AND ISNULL(CONVERT(CHAR(8),hremployee_master.empl_enddate,112), @startdate) >= @startdate " 'Sohail (12 May 2012) - [empl_enddate]

                objDataOperation.AddParameter("@startdate", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, IIf(strEmployeeAsOnDate = "", ConfigParameter._Object._EmployeeAsOnDate, strEmployeeAsOnDate))
                objDataOperation.AddParameter("@enddate", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, IIf(strEmployeeAsOnDate = "", ConfigParameter._Object._EmployeeAsOnDate, strEmployeeAsOnDate))
            End If
            'S.SANDEEP [ 27 APRIL 2012 ] -- END




            'S.SANDEEP [ 29 JUNE 2011 ] -- END 

            objDataOperation.AddParameter("@AssessorId", SqlDbType.Int, eZeeDataType.INT_SIZE, intAssessorId)
            objDataOperation.AddParameter("@Select", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 1, "Select"))

            dsList = objDataOperation.ExecQuery(StrQ, strList)

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & " : " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            Return dsList

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "getEmployeeBasedAssessor", mstrModuleName)
            Return Nothing
        End Try
    End Function

    Public Function GetUncommitedInfo(ByVal intPeriodId As Integer) As Integer
        Dim intUCnt As Integer = -1
        Dim exForce As Exception
        Dim StrQ As String = String.Empty
        Try
            objDataOperation = New clsDataOperation
            StrQ = "SELECT " & _
                   "	analysisunkid " & _
                   "FROM hrassess_analysis_master " & _
                   "WHERE ISNULL(isvoid,0) = 0 " & _
                   "	AND ISNULL(iscommitted,0) = 0 " & _
                   "    AND periodunkid = @Pid "

            objDataOperation.AddParameter("@Pid", SqlDbType.Int, eZeeDataType.INT_SIZE, intPeriodId)

            intUCnt = objDataOperation.RecordCount(StrQ)

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & " : " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            Return intUCnt

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "GetUncommitedInfo", mstrModuleName)
        Finally
        End Try
    End Function


    'S.SANDEEP [ 18 AUG 2012 ] -- START
    'ENHANCEMENT : TRA CHANGES
    'Public Function GetAssessmentResultView(ByVal intEmployeeId As Integer, _
    '                                        ByVal intAssessGroupId As Integer, _
    '                                        ByVal intYearId As Integer, _
    '                                        ByVal intPeriodId As Integer) As DataSet

    Public Function GetAssessmentResultView(ByVal intEmployeeId As Integer, _
                                            ByVal intAssessGroupId As Integer, _
                                            ByVal intYearId As Integer, _
                                        ByVal intPeriodId As Integer, Optional ByVal StrDataBaseName As String = "") As DataSet

        If StrDataBaseName.Trim.Length <= 0 Then StrDataBaseName = FinancialYear._Object._DatabaseName
        'S.SANDEEP [ 18 AUG 2012 ] -- END

        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception
        Dim dtTable As DataTable
        objDataOperation = New clsDataOperation
        Try
            dtTable = New DataTable("AssessView")

            dtTable.Columns.Add("MainGrp", System.Type.GetType("System.String")).Caption = "Main Group"
            dtTable.Columns.Add("Items", System.Type.GetType("System.String")).Caption = "Items"
            dtTable.Columns.Add("Self", System.Type.GetType("System.String")).Caption = "Self"
            dtTable.Columns.Add("SelfRemark", System.Type.GetType("System.String")).Caption = "Self Remark"
            dtTable.Columns.Add("assessitemunkid", System.Type.GetType("System.Int32")).DefaultValue = -1
            dtTable.Columns.Add("assess_subitemunkid", System.Type.GetType("System.Int32")).DefaultValue = -1


            'S.SANDEEP [ 18 AUG 2012 ] -- START
            'ENHANCEMENT : TRA CHANGES
            'strQ = "SELECT " & _
            '          " CAST(Id AS NVARCHAR(50))+'.0' +SPACE(3) + group_name +SPACE(2)+'[ '+ CAST(weight AS NVARCHAR(50)) +' ]' AS MainGrp " & _
            '          ",CASE WHEN ISNULL(sub_name,'') <> '' THEN sub_name ELSE group_name END  AS Items " & _
            '          ",ISNULL(assessitemunkid,0) AS assessitemunkid " & _
            '          ",ISNULL(assess_subitemunkid,0) AS assess_subitemunkid " & _
            '        "FROM " & _
            '        "( " & _
            '             "SELECT " & _
            '                   "CAST(hrassess_item_master.assessitemunkid AS NVARCHAR(50))+'.0'  AS GrpId " & _
            '                  ",ISNULL(hrassess_item_master.name,'') AS group_name " & _
            '                  ",ISNULL(hrassess_subitem_master.name,'') AS sub_name " & _
            '                  ",ISNULL(weight,0) AS weight " & _
            '                  ",hrassess_item_master.assessitemunkid " & _
            '                  ",hrassess_subitem_master.assess_subitemunkid " & _
            '             "FROM hrassess_item_master " & _
            '                  "LEFT JOIN hrassess_subitem_master ON hrassess_item_master.assessitemunkid = hrassess_subitem_master.assessitemunkid " & _
            '                       "AND hrassess_subitem_master.isactive = 1 " & _
            '             "WHERE hrassess_item_master.isactive=1 AND assessgroupunkid = '" & intAssessGroupId & "' " & _
            '        ") AS Main " & _
            '        "JOIN " & _
            '        "( " & _
            '             "SELECT " & _
            '                   "ROW_NUMBER() OVER(ORDER BY assessitemunkid) AS Id " & _
            '                  ",assessitemunkid AS RAId " & _
            '             "FROM hrassess_item_master WHERE hrassess_item_master.isactive=1 " & _
            '        ") AS A ON A.RAId = Main.assessitemunkid " & _
            '        "ORDER BY Main.assessitemunkid "

            strQ = "SELECT " & _
                      " CAST(Id AS NVARCHAR(50))+'.0' +SPACE(3) + group_name +SPACE(2)+'[ '+ CAST(weight AS NVARCHAR(50)) +' ]' AS MainGrp " & _
                      ",CASE WHEN ISNULL(sub_name,'') <> '' THEN sub_name ELSE group_name END  AS Items " & _
                      ",ISNULL(assessitemunkid,0) AS assessitemunkid " & _
                      ",ISNULL(assess_subitemunkid,0) AS assess_subitemunkid " & _
                    "FROM " & _
                    "( " & _
                         "SELECT " & _
                               "CAST(" & StrDataBaseName & "..hrassess_item_master.assessitemunkid AS NVARCHAR(50))+'.0'  AS GrpId " & _
                              ",ISNULL(" & StrDataBaseName & "..hrassess_item_master.name,'') AS group_name " & _
                              ",ISNULL(" & StrDataBaseName & "..hrassess_subitem_master.name,'') AS sub_name " & _
                              ",ISNULL(weight,0) AS weight " & _
                              "," & StrDataBaseName & "..hrassess_item_master.assessitemunkid " & _
                              "," & StrDataBaseName & "..hrassess_subitem_master.assess_subitemunkid " & _
                         "FROM " & StrDataBaseName & "..hrassess_item_master " & _
                              "LEFT JOIN  " & StrDataBaseName & "..hrassess_subitem_master ON  " & StrDataBaseName & "..hrassess_item_master.assessitemunkid =  " & StrDataBaseName & "..hrassess_subitem_master.assessitemunkid " & _
                                   "AND  " & StrDataBaseName & "..hrassess_subitem_master.isactive = 1 " & _
                         "WHERE  " & StrDataBaseName & "..hrassess_item_master.isactive=1 AND assessgroupunkid = '" & intAssessGroupId & "' " & _
                    " AND periodunkid = '" & intPeriodId & "') AS Main " & _
                    "JOIN " & _
                    "( " & _
                         "SELECT " & _
                               "ROW_NUMBER() OVER(ORDER BY assessitemunkid) AS Id " & _
                              ",assessitemunkid AS RAId " & _
                         "FROM  " & StrDataBaseName & "..hrassess_item_master WHERE  " & StrDataBaseName & "..hrassess_item_master.isactive=1 " & _
                    " AND periodunkid = '" & intPeriodId & "') AS A ON A.RAId = Main.assessitemunkid " & _
                    "ORDER BY Main.assessitemunkid "
            'S.SANDEEP [ 18 AUG 2012 ] -- END

            'S.SANDEEP [ 21 JUL 2014 ] -- START
            ' AND periodunkid = '" & intPeriodId & "' ============== ADDED
            'S.SANDEEP [ 21 JUL 2014 ] -- END
            

            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & " : " & objDataOperation.ErrorMessage)
                Throw exForce
            End If


            For Each dtRow As DataRow In dsList.Tables(0).Rows
                dtTable.ImportRow(dtRow)
            Next

            'S.SANDEEP [ 14 JUNE 2012 ] -- START
            'ENHANCEMENT : TRA CHANGES

            'S.SANDEEP [ 18 AUG 2012 ] -- START
            'ENHANCEMENT : TRA CHANGES
            'strQ = "SELECT " & _
            '                " hrresult_master.resultname AS selfresult " & _
            '                ",hrassess_item_master.assessitemunkid AS SelfassessId " & _
            '                ",ISNULL(hrassess_subitem_master.assess_subitemunkid,0) AS SelfsubassessId " & _
            '                ",ISNULL(hremployee_master.firstname,'')+' '+ISNULL(hremployee_master.othername,'')+' '+ISNULL(hremployee_master.surname,'') AS Emp " & _
            '                ",hrassess_analysis_tran.remark AS SelfRemark " & _
            '           "FROM hrassess_analysis_master " & _
            '                "JOIN hremployee_master ON hremployee_master.employeeunkid = hrassess_analysis_master.selfemployeeunkid " & _
            '                "JOIN hrassess_analysis_tran ON hrassess_analysis_master.analysisunkid = hrassess_analysis_tran.analysisunkid " & _
            '                "JOIN hrresult_master ON hrassess_analysis_tran.resultunkid = hrresult_master.resultunkid " & _
            '                "JOIN hrassess_item_master ON hrassess_analysis_tran.assessitemunkid = hrassess_item_master.assessitemunkid " & _
            '                "LEFT JOIN hrassess_subitem_master ON hrassess_subitem_master.assess_subitemunkid = hrassess_analysis_tran.assess_subitemunkid " & _
            '            "WHERE selfemployeeunkid = '" & intEmployeeId & "' AND hrassess_analysis_master.assessgroupunkid = '" & intAssessGroupId & "' " & _
            '                "AND yearunkid = '" & intYearId & "' AND periodunkid = '" & intPeriodId & "' " & _
            '       " AND hrassess_analysis_master.isvoid = 0  AND hrassess_analysis_tran.isvoid = 0 AND hrassess_analysis_master.iscommitted = 1 "


            'S.SANDEEP [ 22 OCT 2013 ] -- START
            'ENHANCEMENT : ENHANCEMENT
            'strQ = "SELECT " & _
            '       "  " & StrDataBaseName & "..hrresult_master.resultname AS selfresult " & _
            '       " ," & StrDataBaseName & "..hrassess_item_master.assessitemunkid AS SelfassessId " & _
            '       " ,ISNULL(" & StrDataBaseName & "..hrassess_subitem_master.assess_subitemunkid,0) AS SelfsubassessId " & _
            '       " ,ISNULL(" & StrDataBaseName & "..hremployee_master.firstname,'')+' '+ISNULL(" & StrDataBaseName & "..hremployee_master.othername,'')+' '+ISNULL(" & StrDataBaseName & "..hremployee_master.surname,'') AS Emp " & _
            '       " ," & StrDataBaseName & "..hrassess_analysis_tran.remark AS SelfRemark " & _
            '       "FROM  " & StrDataBaseName & "..hrassess_analysis_master " & _
            '       "    JOIN " & StrDataBaseName & "..hremployee_master ON " & StrDataBaseName & "..hremployee_master.employeeunkid =  " & StrDataBaseName & "..hrassess_analysis_master.selfemployeeunkid " & _
            '       "    JOIN " & StrDataBaseName & "..hrassess_analysis_tran ON  " & StrDataBaseName & "..hrassess_analysis_master.analysisunkid =  " & StrDataBaseName & "..hrassess_analysis_tran.analysisunkid " & _
            '       "    JOIN " & StrDataBaseName & "..hrresult_master ON  " & StrDataBaseName & "..hrassess_analysis_tran.resultunkid =  " & StrDataBaseName & "..hrresult_master.resultunkid " & _
            '       "    JOIN " & StrDataBaseName & "..hrassess_item_master ON  " & StrDataBaseName & "..hrassess_analysis_tran.assessitemunkid =  " & StrDataBaseName & "..hrassess_item_master.assessitemunkid " & _
            '       "    LEFT JOIN " & StrDataBaseName & "..hrassess_subitem_master ON  " & StrDataBaseName & "..hrassess_subitem_master.assess_subitemunkid =  " & StrDataBaseName & "..hrassess_analysis_tran.assess_subitemunkid " & _
            '       "WHERE selfemployeeunkid = '" & intEmployeeId & "' AND  " & StrDataBaseName & "..hrassess_analysis_master.assessgroupunkid = '" & intAssessGroupId & "' " & _
            '       "    AND " & StrDataBaseName & "..hrassess_analysis_master.yearunkid = '" & intYearId & "' AND " & StrDataBaseName & "..hrassess_analysis_master.periodunkid = '" & intPeriodId & "' " & _
            '       "    AND  " & StrDataBaseName & "..hrassess_analysis_master.isvoid = 0  AND  " & StrDataBaseName & "..hrassess_analysis_tran.isvoid = 0 AND  " & StrDataBaseName & "..hrassess_analysis_master.iscommitted = 1 "

            strQ = "SELECT " & _
                   "  CAST(CASE WHEN ISNUMERIC(" & StrDataBaseName & "..hrresult_master.resultname) <> 0 THEN " & StrDataBaseName & "..hrresult_master.resultname * " & IIf(ConfigParameter._Object._ConsiderItemWeightAsNumber = True, "CAST((" & StrDataBaseName & "..hrassess_item_master.weight) AS DECIMAL(10,2))", "1") & " ELSE CAST(0 AS DECIMAL(10,2)) END AS DECIMAL(10,2)) AS selfresult " & _
                   " ," & StrDataBaseName & "..hrassess_item_master.assessitemunkid AS SelfassessId " & _
                   " ,ISNULL(" & StrDataBaseName & "..hrassess_subitem_master.assess_subitemunkid,0) AS SelfsubassessId " & _
                   " ,ISNULL(" & StrDataBaseName & "..hremployee_master.firstname,'')+' '+ISNULL(" & StrDataBaseName & "..hremployee_master.othername,'')+' '+ISNULL(" & StrDataBaseName & "..hremployee_master.surname,'') AS Emp " & _
                   " ," & StrDataBaseName & "..hrassess_analysis_tran.remark AS SelfRemark " & _
                   "FROM  " & StrDataBaseName & "..hrassess_analysis_master " & _
                   "    JOIN " & StrDataBaseName & "..hremployee_master ON " & StrDataBaseName & "..hremployee_master.employeeunkid =  " & StrDataBaseName & "..hrassess_analysis_master.selfemployeeunkid " & _
                   "    JOIN " & StrDataBaseName & "..hrassess_analysis_tran ON  " & StrDataBaseName & "..hrassess_analysis_master.analysisunkid =  " & StrDataBaseName & "..hrassess_analysis_tran.analysisunkid " & _
                   "    JOIN " & StrDataBaseName & "..hrresult_master ON  " & StrDataBaseName & "..hrassess_analysis_tran.resultunkid =  " & StrDataBaseName & "..hrresult_master.resultunkid " & _
                   "    JOIN " & StrDataBaseName & "..hrassess_item_master ON  " & StrDataBaseName & "..hrassess_analysis_tran.assessitemunkid =  " & StrDataBaseName & "..hrassess_item_master.assessitemunkid " & _
                   "    LEFT JOIN " & StrDataBaseName & "..hrassess_subitem_master ON  " & StrDataBaseName & "..hrassess_subitem_master.assess_subitemunkid =  " & StrDataBaseName & "..hrassess_analysis_tran.assess_subitemunkid " & _
                   "WHERE selfemployeeunkid = '" & intEmployeeId & "' AND  " & StrDataBaseName & "..hrassess_analysis_master.assessgroupunkid = '" & intAssessGroupId & "' " & _
                            "AND " & StrDataBaseName & "..hrassess_analysis_master.yearunkid = '" & intYearId & "' AND " & StrDataBaseName & "..hrassess_analysis_master.periodunkid = '" & intPeriodId & "' " & _
                   " AND  " & StrDataBaseName & "..hrassess_analysis_master.isvoid = 0  AND  " & StrDataBaseName & "..hrassess_analysis_tran.isvoid = 0 AND  " & StrDataBaseName & "..hrassess_analysis_master.iscommitted = 1 "
            'S.SANDEEP [ 22 OCT 2013 ] -- END

            
            'S.SANDEEP [ 18 AUG 2012 ] -- END

            'S.SANDEEP [ 14 JUNE 2012 ] -- END


            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & " : " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            For Each dRow As DataRow In dsList.Tables(0).Rows
                Dim dTemp() As DataRow = dtTable.Select("assessitemunkid='" & dRow.Item("SelfassessId") & "' AND assess_subitemunkid ='" & dRow.Item("SelfsubassessId") & "'")
                If dTemp.Length > 0 Then
                    dTemp(0)("Self") = dRow.Item("selfresult")
                    dTemp(0)("SelfRemark") = dRow.Item("SelfRemark")
                End If
            Next

            'S.SANDEEP [ 18 AUG 2012 ] -- START
            'ENHANCEMENT : TRA CHANGES
            'strQ = "SELECT " & _
            '       " ext_assessorunkid " & _
            '       ",assessoremployeeunkid " & _
            '       "FROM hrassess_analysis_master " & _
            '       "WHERE assessedemployeeunkid = '" & intEmployeeId & "' AND hrassess_analysis_master.assessgroupunkid = '" & intAssessGroupId & "' " & _
            '       "AND isvoid = 0 and reviewerunkid <= 0 "

            strQ = "SELECT " & _
                   " ext_assessorunkid " & _
                   ",assessoremployeeunkid " & _
                   "FROM " & StrDataBaseName & "..hrassess_analysis_master " & _
                   "WHERE assessedemployeeunkid = '" & intEmployeeId & "' AND " & StrDataBaseName & "..hrassess_analysis_master.assessgroupunkid = '" & intAssessGroupId & "' " & _
                   "AND isvoid = 0 and reviewerunkid <= 0 "
            'S.SANDEEP [ 18 AUG 2012 ] -- END
            

            Dim dsAssessor As DataSet = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & " : " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            For Each dtRow As DataRow In dsAssessor.Tables(0).Rows

                'S.SANDEEP [ 14 JUNE 2012 ] -- START
                'ENHANCEMENT : TRA CHANGES
                'strQ = "SELECT " & _
                '       " ISNULL(hrassess_item_master.name,'') AS assessitem " & _
                '       ",ISNULL(hrassess_subitem_master.name,'') AS assess_subitem " & _
                '       ",hrresult_master.resultname AS Aresult " & _
                '       ",ISNULL(hrassess_item_master.weight,0) AS weight " & _
                '       ",CASE WHEN hrexternal_assessor_master.ext_assessorunkid >0 THEN hrexternal_assessor_master.displayname ELSE ISNULL(AEMP.firstname,'')+' '+ISNULL(AEMP.othername,'')+' '+ISNULL(AEMP.surname,'') END AS Appraiser " & _
                '       ",hrassess_subitem_master.assess_subitemunkid " & _
                '       ",assessoremployeeunkid " & _
                '       ",hrassess_item_master.assessitemunkid " & _
                '       ",hrassess_analysis_tran.remark AS Remark " & _
                '       ",hrexternal_assessor_master.ext_assessorunkid " & _
                '   "FROM hrassess_analysis_master " & _
                '       "LEFT JOIN hrexternal_assessor_master ON hrexternal_assessor_master.ext_assessorunkid = hrassess_analysis_master.ext_assessorunkid " & _
                '       "LEFT JOIN hremployee_master AS AEmp ON AEmp.employeeunkid = hrassess_analysis_master.assessoremployeeunkid " & _
                '       "JOIN hremployee_master ON hremployee_master.employeeunkid = hrassess_analysis_master.assessedemployeeunkid " & _
                '       "JOIN hrassess_analysis_tran ON hrassess_analysis_master.analysisunkid = hrassess_analysis_tran.analysisunkid " & _
                '       "JOIN hrresult_master ON hrassess_analysis_tran.resultunkid = hrresult_master.resultunkid " & _
                '       "JOIN hrassess_item_master ON hrassess_analysis_tran.assessitemunkid = hrassess_item_master.assessitemunkid " & _
                '       "LEFT JOIN hrassess_subitem_master ON hrassess_subitem_master.assess_subitemunkid = hrassess_analysis_tran.assess_subitemunkid " & _
                '   "WHERE assessedemployeeunkid = '" & intEmployeeId & "' AND hrassess_analysis_master.assessgroupunkid = '" & intAssessGroupId & "' " & _
                '       "AND yearunkid = '" & intYearId & "' AND periodunkid = '" & intPeriodId & "' " & _
                '       "AND hrassess_analysis_master.isvoid = 0  AND hrassess_analysis_tran.isvoid = 0 "

                'S.SANDEEP [ 18 AUG 2012 ] -- START
                'ENHANCEMENT : TRA CHANGES
                'strQ = "SELECT " & _
                '       " ISNULL(hrassess_item_master.name,'') AS assessitem " & _
                '       ",ISNULL(hrassess_subitem_master.name,'') AS assess_subitem " & _
                '       ",hrresult_master.resultname AS Aresult " & _
                '       ",ISNULL(hrassess_item_master.weight,0) AS weight " & _
                '       ",CASE WHEN hrexternal_assessor_master.ext_assessorunkid >0 THEN hrexternal_assessor_master.displayname ELSE ISNULL(AEMP.firstname,'')+' '+ISNULL(AEMP.othername,'')+' '+ISNULL(AEMP.surname,'') END AS Appraiser " & _
                '       ",hrassess_subitem_master.assess_subitemunkid " & _
                '       ",assessoremployeeunkid " & _
                '       ",hrassess_item_master.assessitemunkid " & _
                '       ",hrassess_analysis_tran.remark AS Remark " & _
                '       ",hrexternal_assessor_master.ext_assessorunkid " & _
                '   "FROM hrassess_analysis_master " & _
                '       "LEFT JOIN hrexternal_assessor_master ON hrexternal_assessor_master.ext_assessorunkid = hrassess_analysis_master.ext_assessorunkid " & _
                '       "LEFT JOIN hremployee_master AS AEmp ON AEmp.employeeunkid = hrassess_analysis_master.assessoremployeeunkid " & _
                '       "JOIN hremployee_master ON hremployee_master.employeeunkid = hrassess_analysis_master.assessedemployeeunkid " & _
                '       "JOIN hrassess_analysis_tran ON hrassess_analysis_master.analysisunkid = hrassess_analysis_tran.analysisunkid " & _
                '       "JOIN hrresult_master ON hrassess_analysis_tran.resultunkid = hrresult_master.resultunkid " & _
                '       "JOIN hrassess_item_master ON hrassess_analysis_tran.assessitemunkid = hrassess_item_master.assessitemunkid " & _
                '       "LEFT JOIN hrassess_subitem_master ON hrassess_subitem_master.assess_subitemunkid = hrassess_analysis_tran.assess_subitemunkid " & _
                '   "WHERE assessedemployeeunkid = '" & intEmployeeId & "' AND hrassess_analysis_master.assessgroupunkid = '" & intAssessGroupId & "' " & _
                '       "AND yearunkid = '" & intYearId & "' AND periodunkid = '" & intPeriodId & "' " & _
                '       "AND hrassess_analysis_master.isvoid = 0  AND hrassess_analysis_tran.isvoid = 0 AND hrassess_analysis_master.iscommitted = 1 "

                'S.SANDEEP [ 22 OCT 2013 ] -- START
                'ENHANCEMENT : ENHANCEMENT
                'strQ = "SELECT " & _
                '       " ISNULL(" & StrDataBaseName & "..hrassess_item_master.name,'') AS assessitem " & _
                '       ",ISNULL(" & StrDataBaseName & "..hrassess_subitem_master.name,'') AS assess_subitem " & _
                '       "," & StrDataBaseName & "..hrresult_master.resultname AS Aresult " & _
                '       ",ISNULL(" & StrDataBaseName & "..hrassess_item_master.weight,0) AS weight " & _
                '       ",CASE WHEN " & StrDataBaseName & "..hrexternal_assessor_master.ext_assessorunkid > 0 THEN " & StrDataBaseName & "..hrexternal_assessor_master.displayname ELSE ISNULL(AEMP.firstname,'')+' '+ISNULL(AEMP.othername,'')+' '+ISNULL(AEMP.surname,'') END AS Appraiser " & _
                '       "," & StrDataBaseName & "..hrassess_subitem_master.assess_subitemunkid " & _
                '       ",assessoremployeeunkid " & _
                '       "," & StrDataBaseName & "..hrassess_item_master.assessitemunkid " & _
                '       "," & StrDataBaseName & "..hrassess_analysis_tran.remark AS Remark " & _
                '       "," & StrDataBaseName & "..hrexternal_assessor_master.ext_assessorunkid " & _
                '   "FROM " & StrDataBaseName & "..hrassess_analysis_master " & _
                '       "LEFT JOIN " & StrDataBaseName & "..hrexternal_assessor_master ON " & StrDataBaseName & "..hrexternal_assessor_master.ext_assessorunkid = " & StrDataBaseName & "..hrassess_analysis_master.ext_assessorunkid " & _
                '       "LEFT JOIN " & StrDataBaseName & "..hremployee_master AS AEmp ON AEmp.employeeunkid = " & StrDataBaseName & "..hrassess_analysis_master.assessoremployeeunkid " & _
                '       "JOIN " & StrDataBaseName & "..hremployee_master ON " & StrDataBaseName & "..hremployee_master.employeeunkid = " & StrDataBaseName & "..hrassess_analysis_master.assessedemployeeunkid " & _
                '       "JOIN " & StrDataBaseName & "..hrassess_analysis_tran ON " & StrDataBaseName & "..hrassess_analysis_master.analysisunkid = " & StrDataBaseName & "..hrassess_analysis_tran.analysisunkid " & _
                '       "JOIN " & StrDataBaseName & "..hrresult_master ON " & StrDataBaseName & "..hrassess_analysis_tran.resultunkid = " & StrDataBaseName & "..hrresult_master.resultunkid " & _
                '       "JOIN " & StrDataBaseName & "..hrassess_item_master ON " & StrDataBaseName & "..hrassess_analysis_tran.assessitemunkid = " & StrDataBaseName & "..hrassess_item_master.assessitemunkid " & _
                '       "LEFT JOIN " & StrDataBaseName & "..hrassess_subitem_master ON " & StrDataBaseName & "..hrassess_subitem_master.assess_subitemunkid = " & StrDataBaseName & "..hrassess_analysis_tran.assess_subitemunkid " & _
                '   "WHERE assessedemployeeunkid = '" & intEmployeeId & "' AND " & StrDataBaseName & "..hrassess_analysis_master.assessgroupunkid = '" & intAssessGroupId & "' " & _
                '       "AND " & StrDataBaseName & "..hrassess_analysis_master.yearunkid = '" & intYearId & "' AND " & StrDataBaseName & "..hrassess_analysis_master.periodunkid = '" & intPeriodId & "' " & _
                '       "AND " & StrDataBaseName & "..hrassess_analysis_master.isvoid = 0  AND " & StrDataBaseName & "..hrassess_analysis_tran.isvoid = 0 AND " & StrDataBaseName & "..hrassess_analysis_master.iscommitted = 1 "

                strQ = "SELECT " & _
                       " ISNULL(" & StrDataBaseName & "..hrassess_item_master.name,'') AS assessitem " & _
                       ",ISNULL(" & StrDataBaseName & "..hrassess_subitem_master.name,'') AS assess_subitem " & _
                       ",CAST(CASE WHEN ISNUMERIC(" & StrDataBaseName & "..hrresult_master.resultname) <> 0 THEN " & StrDataBaseName & "..hrresult_master.resultname * " & IIf(ConfigParameter._Object._ConsiderItemWeightAsNumber = True, "CAST((" & StrDataBaseName & "..hrassess_item_master.weight) AS DECIMAL(10,2))", "1") & " ELSE CAST(0 AS DECIMAL(10,2)) END AS DECIMAL(10,2)) AS Aresult " & _
                       ",ISNULL(" & StrDataBaseName & "..hrassess_item_master.weight,0) AS weight " & _
                       ",CASE WHEN " & StrDataBaseName & "..hrexternal_assessor_master.ext_assessorunkid >0 THEN " & StrDataBaseName & "..hrexternal_assessor_master.displayname ELSE ISNULL(AEMP.firstname,'')+' '+ISNULL(AEMP.othername,'')+' '+ISNULL(AEMP.surname,'') END AS Appraiser " & _
                       "," & StrDataBaseName & "..hrassess_subitem_master.assess_subitemunkid " & _
                       ",assessoremployeeunkid " & _
                       "," & StrDataBaseName & "..hrassess_item_master.assessitemunkid " & _
                       "," & StrDataBaseName & "..hrassess_analysis_tran.remark AS Remark " & _
                       "," & StrDataBaseName & "..hrexternal_assessor_master.ext_assessorunkid " & _
                   "FROM " & StrDataBaseName & "..hrassess_analysis_master " & _
                       "LEFT JOIN " & StrDataBaseName & "..hrexternal_assessor_master ON " & StrDataBaseName & "..hrexternal_assessor_master.ext_assessorunkid = " & StrDataBaseName & "..hrassess_analysis_master.ext_assessorunkid " & _
                       "LEFT JOIN " & StrDataBaseName & "..hremployee_master AS AEmp ON AEmp.employeeunkid = " & StrDataBaseName & "..hrassess_analysis_master.assessoremployeeunkid " & _
                       "JOIN " & StrDataBaseName & "..hremployee_master ON " & StrDataBaseName & "..hremployee_master.employeeunkid = " & StrDataBaseName & "..hrassess_analysis_master.assessedemployeeunkid " & _
                       "JOIN " & StrDataBaseName & "..hrassess_analysis_tran ON " & StrDataBaseName & "..hrassess_analysis_master.analysisunkid = " & StrDataBaseName & "..hrassess_analysis_tran.analysisunkid " & _
                       "JOIN " & StrDataBaseName & "..hrresult_master ON " & StrDataBaseName & "..hrassess_analysis_tran.resultunkid = " & StrDataBaseName & "..hrresult_master.resultunkid " & _
                       "JOIN " & StrDataBaseName & "..hrassess_item_master ON " & StrDataBaseName & "..hrassess_analysis_tran.assessitemunkid = " & StrDataBaseName & "..hrassess_item_master.assessitemunkid " & _
                       "LEFT JOIN " & StrDataBaseName & "..hrassess_subitem_master ON " & StrDataBaseName & "..hrassess_subitem_master.assess_subitemunkid = " & StrDataBaseName & "..hrassess_analysis_tran.assess_subitemunkid " & _
                   "WHERE assessedemployeeunkid = '" & intEmployeeId & "' AND " & StrDataBaseName & "..hrassess_analysis_master.assessgroupunkid = '" & intAssessGroupId & "' " & _
                       "AND " & StrDataBaseName & "..hrassess_analysis_master.yearunkid = '" & intYearId & "' AND " & StrDataBaseName & "..hrassess_analysis_master.periodunkid = '" & intPeriodId & "' " & _
                       "AND " & StrDataBaseName & "..hrassess_analysis_master.isvoid = 0  AND " & StrDataBaseName & "..hrassess_analysis_tran.isvoid = 0 AND " & StrDataBaseName & "..hrassess_analysis_master.iscommitted = 1 "
                'S.SANDEEP [ 22 OCT 2013 ] -- END



                'S.SANDEEP [ 18 AUG 2012 ] -- END

                
                'S.SANDEEP [ 14 JUNE 2012 ] -- END

                

                If CInt(dtRow.Item("assessoremployeeunkid")) <= -1 Then
                    strQ &= " AND hrassess_analysis_master.ext_assessorunkid = '" & dtRow.Item("ext_assessorunkid") & "' "
                Else
                    strQ &= " AND assessoremployeeunkid = '" & dtRow.Item("assessoremployeeunkid") & "' "
                End If

                Dim ds As DataSet = objDataOperation.ExecQuery(strQ, "List")

                For Each dRow As DataRow In ds.Tables(0).Rows
                    If dRow.Item("assessoremployeeunkid") <= 0 Then
                        If dtTable.Columns.Contains("Column" & dRow.Item("ext_assessorunkid").ToString & "ExId") = False Then
                            dtTable.Columns.Add("Column" & dRow.Item("ext_assessorunkid").ToString & "ExId", System.Type.GetType("System.String")).Caption = "Assessor [ " & dRow.Item("Appraiser") & " ]"
                            dtTable.Columns.Add("Column" & dRow.Item("ext_assessorunkid").ToString & "ExIdRemark", System.Type.GetType("System.String")).Caption = dRow.Item("Appraiser") & "  " & "Remark"
                        End If
                    Else
                        If dtTable.Columns.Contains("Column" & dRow.Item("assessoremployeeunkid").ToString & "IntId") = False Then
                            dtTable.Columns.Add("Column" & dRow.Item("assessoremployeeunkid").ToString & "IntId", System.Type.GetType("System.String")).Caption = "Assessor [ " & dRow.Item("Appraiser") & " ]"
                            dtTable.Columns.Add("Column" & dRow.Item("assessoremployeeunkid").ToString & "IntIdRemark", System.Type.GetType("System.String")).Caption = dRow.Item("Appraiser") & "  " & "Remark"
                        End If
                    End If

                    Dim dtTemp() As DataRow = Nothing
                    If IsDBNull(dRow.Item("assess_subitemunkid")) = False Then
                        dtTemp = dtTable.Select("assess_subitemunkid = '" & dRow.Item("assess_subitemunkid") & "'")
                    Else
                        dtTemp = dtTable.Select("assessitemunkid = '" & dRow.Item("assessitemunkid") & "'")
                    End If
                    If dtTemp.Length > 0 Then
                        If dRow.Item("assessoremployeeunkid") <= 0 Then
                            dtTemp(0)("Column" & dRow.Item("ext_assessorunkid").ToString & "ExId") = dRow.Item("Aresult")
                            dtTemp(0)("Column" & dRow.Item("ext_assessorunkid").ToString & "ExIdRemark") = dRow.Item("Remark")
                        Else
                            dtTemp(0)("Column" & dtRow.Item("assessoremployeeunkid").ToString & "IntId") = dRow.Item("Aresult")
                            dtTemp(0)("Column" & dtRow.Item("assessoremployeeunkid").ToString & "IntIdRemark") = dRow.Item("Remark")
                        End If
                    End If
                Next
                dtTable.AcceptChanges()
            Next

            'S.SANDEEP [ 28 DEC 2012 ] -- START
            'ENHANCEMENT : TRA CHANGES

            'S.SANDEEP [ 22 OCT 2013 ] -- START
            'ENHANCEMENT : ENHANCEMENT
            'strQ = "SELECT " & _
            '                            " ISNULL(" & StrDataBaseName & "..hrassess_item_master.name,'') AS assessitem " & _
            '                            ",ISNULL(" & StrDataBaseName & "..hrassess_subitem_master.name,'') AS assess_subitem " & _
            '                            "," & StrDataBaseName & "..hrresult_master.resultname AS Aresult " & _
            '                            ",ISNULL(" & StrDataBaseName & "..hrassess_item_master.weight,0) AS weight " & _
            '                            ",ISNULL(REmp.firstname,'')+' '+ISNULL(REmp.othername,'')+' '+ISNULL(REmp.surname,'') AS Reviewer " & _
            '                            "," & StrDataBaseName & "..hrassess_subitem_master.assess_subitemunkid " & _
            '                            ",reviewerunkid " & _
            '                            "," & StrDataBaseName & "..hrassess_item_master.assessitemunkid " & _
            '                            "," & StrDataBaseName & "..hrassess_analysis_tran.remark AS Remark " & _
            '                          "FROM " & StrDataBaseName & "..hrassess_analysis_master " & _
            '                              "LEFT JOIN " & StrDataBaseName & "..hremployee_master AS REmp ON REmp.employeeunkid = " & StrDataBaseName & "..hrassess_analysis_master.reviewerunkid " & _
            '                              "JOIN " & StrDataBaseName & "..hremployee_master ON " & StrDataBaseName & "..hremployee_master.employeeunkid = " & StrDataBaseName & "..hrassess_analysis_master.assessedemployeeunkid " & _
            '                              "JOIN " & StrDataBaseName & "..hrassess_analysis_tran ON " & StrDataBaseName & "..hrassess_analysis_master.analysisunkid = " & StrDataBaseName & "..hrassess_analysis_tran.analysisunkid " & _
            '                              "JOIN " & StrDataBaseName & "..hrresult_master ON " & StrDataBaseName & "..hrassess_analysis_tran.resultunkid = " & StrDataBaseName & "..hrresult_master.resultunkid " & _
            '                              "JOIN " & StrDataBaseName & "..hrassess_item_master ON " & StrDataBaseName & "..hrassess_analysis_tran.assessitemunkid = " & StrDataBaseName & "..hrassess_item_master.assessitemunkid " & _
            '                              "LEFT JOIN " & StrDataBaseName & "..hrassess_subitem_master ON " & StrDataBaseName & "..hrassess_subitem_master.assess_subitemunkid = " & StrDataBaseName & "..hrassess_analysis_tran.assess_subitemunkid " & _
            '                          " WHERE assessedemployeeunkid = '" & intEmployeeId & "' AND " & StrDataBaseName & "..hrassess_analysis_master.assessgroupunkid = '" & intAssessGroupId & "' " & _
            '                          "AND " & StrDataBaseName & "..hrassess_analysis_master.yearunkid = '" & intYearId & "' AND " & StrDataBaseName & "..hrassess_analysis_master.periodunkid = '" & intPeriodId & "' " & _
            '                          "AND " & StrDataBaseName & "..hrassess_analysis_master.isvoid = 0  AND " & StrDataBaseName & "..hrassess_analysis_tran.isvoid = 0 AND " & StrDataBaseName & "..hrassess_analysis_master.iscommitted = 1  AND assessmodeid = '" & enAssessmentMode.REVIEWER_ASSESSMENT & "'"

            strQ = "SELECT " & _
                            " ISNULL(" & StrDataBaseName & "..hrassess_item_master.name,'') AS assessitem " & _
                            ",ISNULL(" & StrDataBaseName & "..hrassess_subitem_master.name,'') AS assess_subitem " & _
                        ",CAST(CASE WHEN ISNUMERIC(" & StrDataBaseName & "..hrresult_master.resultname) <> 0 THEN " & StrDataBaseName & "..hrresult_master.resultname * " & IIf(ConfigParameter._Object._ConsiderItemWeightAsNumber = True, "CAST((" & StrDataBaseName & "..hrassess_item_master.weight) AS DECIMAL(10,2))", "1") & " ELSE CAST(0 AS DECIMAL(10,2)) END AS DECIMAL(10,2)) AS Aresult " & _
                            ",ISNULL(" & StrDataBaseName & "..hrassess_item_master.weight,0) AS weight " & _
                            ",ISNULL(REmp.firstname,'')+' '+ISNULL(REmp.othername,'')+' '+ISNULL(REmp.surname,'') AS Reviewer " & _
                            "," & StrDataBaseName & "..hrassess_subitem_master.assess_subitemunkid " & _
                            ",reviewerunkid " & _
                            "," & StrDataBaseName & "..hrassess_item_master.assessitemunkid " & _
                            "," & StrDataBaseName & "..hrassess_analysis_tran.remark AS Remark " & _
                          "FROM " & StrDataBaseName & "..hrassess_analysis_master " & _
                              "LEFT JOIN " & StrDataBaseName & "..hremployee_master AS REmp ON REmp.employeeunkid = " & StrDataBaseName & "..hrassess_analysis_master.reviewerunkid " & _
                              "JOIN " & StrDataBaseName & "..hremployee_master ON " & StrDataBaseName & "..hremployee_master.employeeunkid = " & StrDataBaseName & "..hrassess_analysis_master.assessedemployeeunkid " & _
                              "JOIN " & StrDataBaseName & "..hrassess_analysis_tran ON " & StrDataBaseName & "..hrassess_analysis_master.analysisunkid = " & StrDataBaseName & "..hrassess_analysis_tran.analysisunkid " & _
                              "JOIN " & StrDataBaseName & "..hrresult_master ON " & StrDataBaseName & "..hrassess_analysis_tran.resultunkid = " & StrDataBaseName & "..hrresult_master.resultunkid " & _
                              "JOIN " & StrDataBaseName & "..hrassess_item_master ON " & StrDataBaseName & "..hrassess_analysis_tran.assessitemunkid = " & StrDataBaseName & "..hrassess_item_master.assessitemunkid " & _
                              "LEFT JOIN " & StrDataBaseName & "..hrassess_subitem_master ON " & StrDataBaseName & "..hrassess_subitem_master.assess_subitemunkid = " & StrDataBaseName & "..hrassess_analysis_tran.assess_subitemunkid " & _
                          " WHERE assessedemployeeunkid = '" & intEmployeeId & "' AND " & StrDataBaseName & "..hrassess_analysis_master.assessgroupunkid = '" & intAssessGroupId & "' " & _
                          "AND " & StrDataBaseName & "..hrassess_analysis_master.yearunkid = '" & intYearId & "' AND " & StrDataBaseName & "..hrassess_analysis_master.periodunkid = '" & intPeriodId & "' " & _
                          "AND " & StrDataBaseName & "..hrassess_analysis_master.isvoid = 0  AND " & StrDataBaseName & "..hrassess_analysis_tran.isvoid = 0 AND " & StrDataBaseName & "..hrassess_analysis_master.iscommitted = 1  AND assessmodeid = '" & enAssessmentMode.REVIEWER_ASSESSMENT & "'"
            'S.SANDEEP [ 22 OCT 2013 ] -- END


            Dim dsReviewer As DataSet = objDataOperation.ExecQuery(strQ, "List")

            If dsReviewer IsNot Nothing Then
                For Each dRow As DataRow In dsReviewer.Tables(0).Rows
                    If dtTable.Columns.Contains("Column" & dRow.Item("reviewerunkid").ToString & "RewId") = False Then
                        dtTable.Columns.Add("Column" & dRow.Item("reviewerunkid").ToString & "RewId", System.Type.GetType("System.String")).Caption = "Reviewer [ " & dRow.Item("Reviewer") & " ]"
                        dtTable.Columns.Add("Column" & dRow.Item("reviewerunkid").ToString & "RewIdIdRemark", System.Type.GetType("System.String")).Caption = dRow.Item("Reviewer") & "  " & "Remark"
                    End If

                    Dim dtTemp() As DataRow = Nothing
                    If IsDBNull(dRow.Item("assess_subitemunkid")) = False Then
                        dtTemp = dtTable.Select("assess_subitemunkid = '" & dRow.Item("assess_subitemunkid") & "'")
                    Else
                        dtTemp = dtTable.Select("assessitemunkid = '" & dRow.Item("assessitemunkid") & "'")
                    End If
                    If dtTemp.Length > 0 Then
                        dtTemp(0)("Column" & dRow.Item("reviewerunkid").ToString & "RewId") = dRow.Item("Aresult")
                        dtTemp(0)("Column" & dRow.Item("reviewerunkid").ToString & "RewIdIdRemark") = dRow.Item("Remark")
                    End If
                Next
                dtTable.AcceptChanges()
            End If

            'S.SANDEEP [ 28 DEC 2012 ] -- END

            dsList.Tables.Clear()
            dsList.Tables.Add(dtTable)
            Return dsList

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: GetAssessmentResultView; Module Name: " & mstrModuleName)
        Finally
        End Try
    End Function


    'S.SANDEEP [ 18 AUG 2012 ] -- START
    'ENHANCEMENT : TRA CHANGES
    'Public Function GetRemarksTran(ByVal blnIsImprovement As Boolean, _
    '                               ByVal intEmployeeId As Integer, _
    '                               ByVal intAssessGroupId As Integer, _
    '                               ByVal intYearId As Integer, _
    '                               ByVal intPeriodId As Integer) As DataSet

    Public Function GetRemarksTran(ByVal blnIsImprovement As Boolean, _
                                   ByVal intEmployeeId As Integer, _
                                   ByVal intAssessGroupId As Integer, _
                                   ByVal intYearId As Integer, _
                                   ByVal intPeriodId As Integer, _
                                   Optional ByVal StrDataBaseName As String = "", _
                                   Optional ByVal IsReviewer As Boolean = False) As DataSet

        If StrDataBaseName.Trim.Length <= 0 Then StrDataBaseName = FinancialYear._Object._DatabaseName
        'S.SANDEEP [ 18 AUG 2012 ] -- END


        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception
        objDataOperation = New clsDataOperation
        Try
            'S.SANDEEP [ 18 AUG 2012 ] -- START
            'ENHANCEMENT : TRA CHANGES
            'If blnIsImprovement = True Then
            '    strQ = "SELECT " & _
            '               " ISNULL(major_area,'') AS Major_Area " & _
            '               ",Convert(char(8),timeframe_date,112) AS TimeFrame " & _
            '               ",ISNULL(activity,'') AS Action_Activity " & _
            '               ",ISNULL(support_required,'') AS Support " & _
            '               ",ISNULL(other_training,'') Other " & _
            '               ",ISNULL(cfcommon_master.name,'') AS Course_Master " & _
            '           "FROM hrassess_remarks_tran " & _
            '               "LEFT JOIN cfcommon_master ON cfcommon_master.masterunkid = hrassess_remarks_tran.coursemasterunkid AND dbo.cfcommon_master.mastertype = " & clsCommon_Master.enCommonMaster.TRAINING_COURSEMASTER & "  " & _
            '               "JOIN hrassess_analysis_master ON hrassess_remarks_tran.analysisunkid = hrassess_analysis_master.analysisunkid " & _
            '           "WHERE selfemployeeunkid = '" & intEmployeeId & "' AND assessgroupunkid = '" & intAssessGroupId & "' AND hrassess_analysis_master.isvoid = 0 AND hrassess_remarks_tran.isvoid = 0 " & _
            '           "AND yearunkid = '" & intYearId & "' AND periodunkid = '" & intPeriodId & "' "
            'Else
            '    strQ = "SELECT " & _
            '               " ISNULL(major_area,'') AS Major_Area " & _
            '               ",Convert(char(8),timeframe_date,112) AS TimeFrame " & _
            '               ",ISNULL(activity,'') AS Action_Activity " & _
            '               ",ISNULL(support_required,'') AS Support " & _
            '               ",ISNULL(other_training,'') Other " & _
            '               ",ISNULL(cfcommon_master.name,'') AS Course_Master " & _
            '               ",ISNULL(hremployee_master.firstname,'')+' '+ISNULL(hremployee_master.othername,'')+' '+ISNULL(hremployee_master.surname,'') AS Appraiser " & _
            '           "FROM hrassess_remarks_tran " & _
            '               "JOIN hrassess_analysis_master ON hrassess_remarks_tran.analysisunkid = hrassess_analysis_master.analysisunkid " & _
            '               "JOIN hremployee_master ON hremployee_master.employeeunkid = hrassess_analysis_master.assessoremployeeunkid " & _
            '               "LEFT JOIN cfcommon_master ON cfcommon_master.masterunkid = hrassess_remarks_tran.coursemasterunkid AND dbo.cfcommon_master.mastertype = " & clsCommon_Master.enCommonMaster.TRAINING_COURSEMASTER & " " & _
            '           "WHERE assessedemployeeunkid = '" & intEmployeeId & "' AND assessgroupunkid = '" & intAssessGroupId & "' AND hrassess_analysis_master.isvoid = 0 AND hrassess_remarks_tran.isvoid = 0 " & _
            '           "AND yearunkid = '" & intYearId & "' AND periodunkid = '" & intPeriodId & "' "
            'End If

            If blnIsImprovement = True Then
                'S.SANDEEP [ 14 AUG 2013 ] -- START
                'ENHANCEMENT : TRA CHANGES
                If IsReviewer = False Then
                strQ = "SELECT " & _
                           " ISNULL(major_area,'') AS Major_Area " & _
                           ",Convert(char(8),timeframe_date,112) AS TimeFrame " & _
                           ",ISNULL(activity,'') AS Action_Activity " & _
                           ",ISNULL(support_required,'') AS Support " & _
                           ",ISNULL(other_training,'') Other " & _
                           ",ISNULL(cfcommon_master.name,'') AS Course_Master " & _
                       "FROM " & StrDataBaseName & "..hrassess_remarks_tran " & _
                           "LEFT JOIN " & StrDataBaseName & "..cfcommon_master ON cfcommon_master.masterunkid = " & StrDataBaseName & "..hrassess_remarks_tran.coursemasterunkid AND " & StrDataBaseName & "..cfcommon_master.mastertype = " & clsCommon_Master.enCommonMaster.TRAINING_COURSEMASTER & "  " & _
                           "JOIN " & StrDataBaseName & "..hrassess_analysis_master ON " & StrDataBaseName & "..hrassess_remarks_tran.analysisunkid = " & StrDataBaseName & "..hrassess_analysis_master.analysisunkid " & _
                       "WHERE selfemployeeunkid = '" & intEmployeeId & "' AND assessgroupunkid = '" & intAssessGroupId & "' AND " & StrDataBaseName & "..hrassess_analysis_master.isvoid = 0 AND " & StrDataBaseName & "..hrassess_remarks_tran.isvoid = 0 " & _
                       "AND yearunkid = '" & intYearId & "' AND periodunkid = '" & intPeriodId & "' AND isimporvement = 1 "
                Else
                    strQ = "SELECT " & _
                               " ISNULL(major_area,'') AS Major_Area " & _
                               ",Convert(char(8),timeframe_date,112) AS TimeFrame " & _
                               ",ISNULL(activity,'') AS Action_Activity " & _
                               ",ISNULL(support_required,'') AS Support " & _
                               ",ISNULL(other_training,'') Other " & _
                               ",ISNULL(cfcommon_master.name,'') AS Course_Master " & _
                               ",ISNULL(hremployee_master.firstname,'')+' '+ISNULL(hremployee_master.othername,'')+' '+ISNULL(hremployee_master.surname,'') AS Reviewer " & _
                           "FROM " & StrDataBaseName & "..hrassess_remarks_tran " & _
                               "JOIN " & StrDataBaseName & "..hrassess_analysis_master ON " & StrDataBaseName & "..hrassess_remarks_tran.analysisunkid = " & StrDataBaseName & "..hrassess_analysis_master.analysisunkid " & _
                               "JOIN " & StrDataBaseName & "..hremployee_master ON " & StrDataBaseName & "..hremployee_master.employeeunkid = " & StrDataBaseName & "..hrassess_analysis_master.reviewerunkid " & _
                               "LEFT JOIN " & StrDataBaseName & "..cfcommon_master ON " & StrDataBaseName & "..cfcommon_master.masterunkid = " & StrDataBaseName & "..hrassess_remarks_tran.coursemasterunkid AND " & StrDataBaseName & "..cfcommon_master.mastertype = " & clsCommon_Master.enCommonMaster.TRAINING_COURSEMASTER & " " & _
                           "WHERE assessedemployeeunkid = '" & intEmployeeId & "' AND assessgroupunkid = '" & intAssessGroupId & "' AND " & StrDataBaseName & "..hrassess_analysis_master.isvoid = 0 AND " & StrDataBaseName & "..hrassess_remarks_tran.isvoid = 0 " & _
                           "AND yearunkid = '" & intYearId & "' AND periodunkid = '" & intPeriodId & "' AND isimporvement = 1 "
                End If
                'S.SANDEEP [ 14 AUG 2013 ] -- END
            Else

                'S.SANDEEP [ 14 AUG 2013 ] -- START
                'ENHANCEMENT : TRA CHANGES
                If IsReviewer = False Then
                strQ = "SELECT " & _
                           " ISNULL(major_area,'') AS Major_Area " & _
                           ",Convert(char(8),timeframe_date,112) AS TimeFrame " & _
                           ",ISNULL(activity,'') AS Action_Activity " & _
                           ",ISNULL(support_required,'') AS Support " & _
                           ",ISNULL(other_training,'') Other " & _
                           ",ISNULL(cfcommon_master.name,'') AS Course_Master " & _
                           ",ISNULL(hremployee_master.firstname,'')+' '+ISNULL(hremployee_master.othername,'')+' '+ISNULL(hremployee_master.surname,'') AS Appraiser " & _
                       "FROM " & StrDataBaseName & "..hrassess_remarks_tran " & _
                           "JOIN " & StrDataBaseName & "..hrassess_analysis_master ON " & StrDataBaseName & "..hrassess_remarks_tran.analysisunkid = " & StrDataBaseName & "..hrassess_analysis_master.analysisunkid " & _
                           "JOIN " & StrDataBaseName & "..hremployee_master ON " & StrDataBaseName & "..hremployee_master.employeeunkid = " & StrDataBaseName & "..hrassess_analysis_master.assessoremployeeunkid " & _
                           "LEFT JOIN " & StrDataBaseName & "..cfcommon_master ON " & StrDataBaseName & "..cfcommon_master.masterunkid = " & StrDataBaseName & "..hrassess_remarks_tran.coursemasterunkid AND " & StrDataBaseName & "..cfcommon_master.mastertype = " & clsCommon_Master.enCommonMaster.TRAINING_COURSEMASTER & " " & _
                       "WHERE assessedemployeeunkid = '" & intEmployeeId & "' AND assessgroupunkid = '" & intAssessGroupId & "' AND " & StrDataBaseName & "..hrassess_analysis_master.isvoid = 0 AND " & StrDataBaseName & "..hrassess_remarks_tran.isvoid = 0 " & _
                           "AND yearunkid = '" & intYearId & "' AND periodunkid = '" & intPeriodId & "' AND isimporvement = 0 "
                Else
                    strQ = "SELECT " & _
                               " ISNULL(major_area,'') AS Major_Area " & _
                               ",Convert(char(8),timeframe_date,112) AS TimeFrame " & _
                               ",ISNULL(activity,'') AS Action_Activity " & _
                               ",ISNULL(support_required,'') AS Support " & _
                               ",ISNULL(other_training,'') Other " & _
                               ",ISNULL(cfcommon_master.name,'') AS Course_Master " & _
                               ",ISNULL(hremployee_master.firstname,'')+' '+ISNULL(hremployee_master.othername,'')+' '+ISNULL(hremployee_master.surname,'') AS Reviewer " & _
                           "FROM " & StrDataBaseName & "..hrassess_remarks_tran " & _
                               "JOIN " & StrDataBaseName & "..hrassess_analysis_master ON " & StrDataBaseName & "..hrassess_remarks_tran.analysisunkid = " & StrDataBaseName & "..hrassess_analysis_master.analysisunkid " & _
                               "JOIN " & StrDataBaseName & "..hremployee_master ON " & StrDataBaseName & "..hremployee_master.employeeunkid = " & StrDataBaseName & "..hrassess_analysis_master.reviewerunkid " & _
                               "LEFT JOIN " & StrDataBaseName & "..cfcommon_master ON " & StrDataBaseName & "..cfcommon_master.masterunkid = " & StrDataBaseName & "..hrassess_remarks_tran.coursemasterunkid AND " & StrDataBaseName & "..cfcommon_master.mastertype = " & clsCommon_Master.enCommonMaster.TRAINING_COURSEMASTER & " " & _
                           "WHERE assessedemployeeunkid = '" & intEmployeeId & "' AND assessgroupunkid = '" & intAssessGroupId & "' AND " & StrDataBaseName & "..hrassess_analysis_master.isvoid = 0 AND " & StrDataBaseName & "..hrassess_remarks_tran.isvoid = 0 " & _
                           "AND yearunkid = '" & intYearId & "' AND periodunkid = '" & intPeriodId & "' AND isimporvement = 0 "
                End If
                'S.SANDEEP [ 14 AUG 2013 ] -- END

            End If
            'S.SANDEEP [ 18 AUG 2012 ] -- END

            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & " : " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            Return dsList

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: GetRemarksTran; Module Name: " & mstrModuleName)
        Finally
        End Try
    End Function

    Public Function GetItems_BaseGroup(ByVal intAssessGrpId As Integer) As Integer
        Dim StrQ As String = String.Empty
        Dim exForce As Exception
        Dim iCnt As Integer = -1
        Try
            objDataOperation = New clsDataOperation

            StrQ = "SELECT " & _
                   " assessitemunkid  " & _
                   "FROM hrassess_item_master " & _
                   "WHERE assessgroupunkid ='" & intAssessGrpId & "' " & _
                   "AND isactive = 1 "


            iCnt = objDataOperation.RecordCount(StrQ)

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & " : " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            Return iCnt

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: GetItems_BaseGroup; Module Name: " & mstrModuleName)
        Finally
        End Try
    End Function

    'S.SANDEEP [ 28 DEC 2012 ] -- START
    'ENHANCEMENT : TRA CHANGES
    'Public Function GetSubItemCount(ByVal intAssessGrpId As Integer, Optional ByVal StrList As String = "List") As DataSet
    Public Function GetSubItemCount(ByVal intAssessGrpId As Integer, Optional ByVal StrList As String = "List", Optional ByVal intPeriodId As Integer = 0) As DataSet
        'S.SANDEEP [ 28 DEC 2012 ] -- END
        Dim StrQ As String = String.Empty
        Dim exForce As Exception
        Dim dsList As New DataSet
        Try
            objDataOperation = New clsDataOperation

            StrQ = "SELECT " & _
                    " hrassess_item_master.assessitemunkid AS Id " & _
                    ",COUNT(assess_subitemunkid) AS Cnt " & _
                   "FROM hrassess_item_master " & _
                    "LEFT JOIN hrassess_subitem_master ON hrassess_item_master.assessitemunkid = hrassess_subitem_master.assessitemunkid AND hrassess_subitem_master.isactive = 1 " & _
                   "WHERE hrassess_item_master.isactive = 1 AND assessgroupunkid = '" & intAssessGrpId & "'  AND periodunkid = '" & intPeriodId & "' " & _
                   "GROUP BY hrassess_item_master.assessitemunkid "

            dsList = objDataOperation.ExecQuery(StrQ, StrList)

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & " : " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            Return dsList
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: GetSubItemCount; Module Name: " & mstrModuleName)
        Finally
        End Try
    End Function

    'S.SANDEEP [ 04 FEB 2012 ] -- START
    'ENHANCEMENT : TRA CHANGES
    Public Function Get_Percentage(ByVal menAssessMode As enAssessmentMode, ByVal mblnConsider_Weight_As_Number As Boolean) As DataSet 'S.SANDEEP [ 22 OCT 2013 ] -- START -- END
        'Public Function Get_Percentage(ByVal menAssessMode As enAssessmentMode) As DataSet
        Dim StrQ As String = String.Empty
        Dim exForce As Exception
        Dim dsList As New DataSet
        Dim objDataOperation As New clsDataOperation
        Dim dTable As DataTable
        'S.SANDEEP [ 05 MARCH 2012 ] -- START
        'ENHANCEMENT : TRA CHANGES
        'Dim mDicIsAdded As New Dictionary(Of Integer, Integer)
        Dim mDicIsAdded As New Dictionary(Of String, Integer)
        'S.SANDEEP [ 05 MARCH 2012 ] -- END
        Try
            dTable = New DataTable("Total")
            dTable.Columns.Add("EmpId", System.Type.GetType("System.Int32"))
            dTable.Columns.Add("Pid", System.Type.GetType("System.Int32"))
            dTable.Columns.Add("TotalPercent", System.Type.GetType("System.Double"))
            'S.SANDEEP [ 08 APR 2013 ] -- START
            'ENHANCEMENT : TRA CHANGES
            dTable.Columns.Add("analysisunkid", System.Type.GetType("System.Int32"))
            'S.SANDEEP [ 08 APR 2013 ] -- END

            StrQ = "SELECT "
            'S.SANDEEP [ 08 APR 2013 ] -- START
            'ENHANCEMENT : TRA CHANGES
            StrQ &= " hrassess_analysis_master.analysisunkid "
            'S.SANDEEP [ 08 APR 2013 ] -- END   
            Select Case menAssessMode
                Case enAssessmentMode.SELF_ASSESSMENT
                    StrQ &= ", hrassess_analysis_master.selfemployeeunkid AS EmpId "
                Case enAssessmentMode.APPRAISER_ASSESSMENT
                    StrQ &= ", hrassess_analysis_master.assessedemployeeunkid AS EmpId ,CASE WHEN assessoremployeeunkid >0 THEN 'I_'+CAST(assessoremployeeunkid AS NVARCHAR(10)) ELSE 'E_'+CAST(ext_assessorunkid as NVARCHAR(10)) END AS AEmpId "
                Case enAssessmentMode.REVIEWER_ASSESSMENT
                    StrQ &= ", hrassess_analysis_master.assessedemployeeunkid AS EmpId "
            End Select

            'S.SANDEEP [ 22 OCT 2013 ] -- START
            'ENHANCEMENT : ENHANCEMENT
            'StrQ &= " ,periodunkid AS Pid " & _
            '        " ,ISNULL(hrresult_master.resultname,'') AS resultname " & _
            '        "FROM hrassess_analysis_master " & _
            '        " JOIN hrassess_analysis_tran ON dbo.hrassess_analysis_master.analysisunkid = hrassess_analysis_tran.analysisunkid " & _
            '        " JOIN hrresult_master ON hrassess_analysis_tran.resultunkid = hrresult_master.resultunkid " & _
            '        "WHERE hrassess_analysis_master.isvoid = 0 AND hrassess_analysis_tran.isvoid = 0 AND assessmodeid = '" & menAssessMode & "' "

            StrQ &= " ,hrassess_analysis_master.periodunkid AS Pid "
            If mblnConsider_Weight_As_Number = True Then
                StrQ &= " ,ISNULL(hrresult_master.resultname,0) * hrassess_item_master.weight AS resultname "
            Else
                StrQ &= " ,ISNULL(hrresult_master.resultname,'') AS resultname "
            End If
            StrQ &= "FROM hrassess_analysis_master " & _
                    " JOIN hrassess_analysis_tran ON dbo.hrassess_analysis_master.analysisunkid = hrassess_analysis_tran.analysisunkid " & _
                    " JOIN hrassess_item_master ON hrassess_analysis_tran.assessitemunkid = hrassess_item_master.assessitemunkid " & _
                    " JOIN hrresult_master ON hrassess_analysis_tran.resultunkid = hrresult_master.resultunkid " & _
                    "WHERE hrassess_analysis_master.isvoid = 0 AND hrassess_analysis_tran.isvoid = 0 AND assessmodeid = '" & menAssessMode & "' "
            'S.SANDEEP [ 22 OCT 2013 ] -- END

            dsList = objDataOperation.ExecQuery(StrQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & " : " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            For Each dtRow As DataRow In dsList.Tables(0).Rows

                'S.SANDEEP [ 05 MARCH 2012 ] -- START
                'ENHANCEMENT : TRA CHANGES
                'If mDicIsAdded.ContainsKey(dtRow.Item("EmpId")) Then Continue For
                Select Case menAssessMode

                    'S.SANDEEP [ 14 AUG 2013 ] -- START
                    'ENHANCEMENT : TRA CHANGES
                    'Case enAssessmentMode.SELF_ASSESSMENT
                    '    If mDicIsAdded.ContainsKey(dtRow.Item("EmpId") & "|" & dtRow.Item("Pid")) Then Continue For
                    '    mDicIsAdded.Add(dtRow.Item("EmpId") & "|" & dtRow.Item("Pid"), dtRow.Item("EmpId"))
                    'Case enAssessmentMode.APPRAISER_ASSESSMENT
                    '    If mDicIsAdded.ContainsKey(dtRow.Item("EmpId") & "|" & dtRow.Item("Pid") & "|" & dtRow.Item("AEmpId")) Then Continue For
                    '    mDicIsAdded.Add(dtRow.Item("EmpId") & "|" & dtRow.Item("Pid") & "|" & dtRow.Item("AEmpId"), dtRow.Item("EmpId"))
                    'Case enAssessmentMode.REVIEWER_ASSESSMENT
                    '    If mDicIsAdded.ContainsKey(dtRow.Item("EmpId") & "|" & dtRow.Item("Pid")) Then Continue For
                    '    mDicIsAdded.Add(dtRow.Item("EmpId") & "|" & dtRow.Item("Pid"), dtRow.Item("EmpId"))

                    Case enAssessmentMode.SELF_ASSESSMENT
                        If mDicIsAdded.ContainsKey(dtRow.Item("EmpId") & "|" & dtRow.Item("Pid") & "|" & dtRow.Item("analysisunkid")) Then Continue For
                        mDicIsAdded.Add(dtRow.Item("EmpId") & "|" & dtRow.Item("Pid") & "|" & dtRow.Item("analysisunkid"), dtRow.Item("EmpId"))
                    Case enAssessmentMode.APPRAISER_ASSESSMENT
                        If mDicIsAdded.ContainsKey(dtRow.Item("EmpId") & "|" & dtRow.Item("Pid") & "|" & dtRow.Item("AEmpId") & "|" & dtRow.Item("analysisunkid")) Then Continue For
                        mDicIsAdded.Add(dtRow.Item("EmpId") & "|" & dtRow.Item("Pid") & "|" & dtRow.Item("AEmpId") & "|" & dtRow.Item("analysisunkid"), dtRow.Item("EmpId"))
                    Case enAssessmentMode.REVIEWER_ASSESSMENT
                        If mDicIsAdded.ContainsKey(dtRow.Item("EmpId") & "|" & dtRow.Item("Pid") & "|" & dtRow.Item("analysisunkid")) Then Continue For
                        mDicIsAdded.Add(dtRow.Item("EmpId") & "|" & dtRow.Item("Pid") & "|" & dtRow.Item("analysisunkid"), dtRow.Item("EmpId"))
                        'S.SANDEEP [ 14 AUG 2013 ] -- END

                End Select
                'S.SANDEEP [ 05 MARCH 2012 ] -- END


                Dim dRow As DataRow = dTable.NewRow
                dRow.Item("EmpId") = dtRow.Item("EmpId")
                dRow.Item("Pid") = dtRow.Item("Pid")
                'S.SANDEEP [ 08 APR 2013 ] -- START
                'ENHANCEMENT : TRA CHANGES
                dRow.Item("analysisunkid") = dtRow.Item("analysisunkid")
                'S.SANDEEP [ 08 APR 2013 ] -- END

                Dim dMRow() As DataRow = Nothing

                Select Case menAssessMode
                    'S.SANDEEP [ 14 AUG 2013 ] -- START
                    'ENHANCEMENT : TRA CHANGES
                    'Case enAssessmentMode.SELF_ASSESSMENT
                    '    dMRow = dsList.Tables(0).Select("EmpId = '" & dtRow.Item("EmpId") & "' AND  Pid = '" & dtRow.Item("Pid") & "'")
                    'Case enAssessmentMode.APPRAISER_ASSESSMENT
                    '    dMRow = dsList.Tables(0).Select("EmpId = '" & dtRow.Item("EmpId") & "' AND  Pid = '" & dtRow.Item("Pid") & "' AND AEmpId = '" & dtRow.Item("AEmpId") & "'")
                    'Case enAssessmentMode.REVIEWER_ASSESSMENT
                    '    dMRow = dsList.Tables(0).Select("EmpId = '" & dtRow.Item("EmpId") & "' AND  Pid = '" & dtRow.Item("Pid") & "'")

                    Case enAssessmentMode.SELF_ASSESSMENT
                        dMRow = dsList.Tables(0).Select("EmpId = '" & dtRow.Item("EmpId") & "' AND  Pid = '" & dtRow.Item("Pid") & "' AND analysisunkid = '" & dtRow.Item("analysisunkid") & "'")
                    Case enAssessmentMode.APPRAISER_ASSESSMENT
                        dMRow = dsList.Tables(0).Select("EmpId = '" & dtRow.Item("EmpId") & "' AND  Pid = '" & dtRow.Item("Pid") & "' AND AEmpId = '" & dtRow.Item("AEmpId") & "' AND analysisunkid = '" & dtRow.Item("analysisunkid") & "'")
                    Case enAssessmentMode.REVIEWER_ASSESSMENT
                        dMRow = dsList.Tables(0).Select("EmpId = '" & dtRow.Item("EmpId") & "' AND  Pid = '" & dtRow.Item("Pid") & "' AND analysisunkid = '" & dtRow.Item("analysisunkid") & "'")
                        'S.SANDEEP [ 14 AUG 2013 ] -- END
                End Select

                If dMRow.Length > 0 Then
                    Dim dblTotal As Double = 0
                    For i As Integer = 0 To dMRow.Length - 1
                        If IsNumeric(dMRow(i)("resultname")) = True Then
                            dblTotal = dblTotal + CDbl(dMRow(i)("resultname"))
                        End If
                    Next
                    dRow.Item("TotalPercent") = dblTotal
                End If

                dTable.Rows.Add(dRow)
            Next

            dsList.Tables.RemoveAt(0)

            dsList.Tables.Add(dTable)

            Return dsList

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: Get_Percentage; Module Name: " & mstrModuleName)
        Finally
        End Try
    End Function
    'S.SANDEEP [ 04 FEB 2012 ] -- END

    'S.SANDEEP [ 05 MARCH 2012 ] -- START
    'ENHANCEMENT : TRA CHANGES
    Public Function Unlock_Commit(ByVal intPeriodUnkid As Integer, ByVal iEmployeeId As Integer) As Boolean 'S.SANDEEP [ 10 SEPT 2013 ] -- START -- END
        'Public Function Unlock_Commit(ByVal intPeriodUnkid As Integer) As Boolean
        Dim StrQ As String = String.Empty
        Dim iCnt As Integer = -1
        Dim exForce As Exception
        Dim objDataOperation As New clsDataOperation
        Dim blnFlag As Boolean = True
        Try

            'S.SANDEEP [ 10 SEPT 2013 ] -- START
            'ENHANCEMENT : TRA CHANGES
            'StrQ &= "SELECT * FROM hrapps_shortlist_master WHERE periodunkid = '" & intPeriodUnkid & "' AND isvoid = 0"
            StrQ = "SELECT 1 FROM hrapps_finalemployee " & _
                   "	JOIN hrapps_shortlist_master ON hrapps_finalemployee.shortlistunkid = hrapps_shortlist_master.shortlistunkid " & _
                   "WHERE hrapps_finalemployee.isvoid = 0 AND employeeunkid = '" & iEmployeeId & "' AND apprperiodunkid <= 0 AND (ISNULL(operationmodeid,0) > 0 OR isfinalshortlisted = 1) " & _
                   "AND hrapps_shortlist_master.periodunkid = '" & intPeriodUnkid & "' "
            'S.SANDEEP [ 10 SEPT 2013 ] -- END

            iCnt = objDataOperation.RecordCount(StrQ)

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & " : " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            If iCnt > 0 Then
                blnFlag = False
            End If

            'S.SANDEEP [ 10 SEPT 2013 ] -- START
            'ENHANCEMENT : TRA CHANGES
            iCnt = -1
            StrQ = "SELECT 1 " & _
                   "FROM hrapps_finalemployee " & _
                   "WHERE isvoid = 0 AND employeeunkid = '" & iEmployeeId & "' AND apprperiodunkid > 0 AND (ISNULL(operationmodeid,0) > 0 OR isfinalshortlisted = 1) " & _
                   "AND apprperiodunkid = '" & intPeriodUnkid & "' "

            iCnt = objDataOperation.RecordCount(StrQ)

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & " : " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            If iCnt > 0 Then
                blnFlag = False
            End If
            'S.SANDEEP [ 10 SEPT 2013 ] -- END

            Return blnFlag

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: Unlock_Commit; Module Name: " & mstrModuleName)
        Finally
        End Try
    End Function
    'S.SANDEEP [ 05 MARCH 2012 ] -- END

'Pinkal (12-Jun-2012) -- Start
    'Enhancement : TRA Changes
    Public Function IsBSC_Pending(ByVal intEmployeeId As Integer, ByVal eAssMode As enAssessmentMode) As Boolean
        Dim mblnFlag As Boolean = False
        Dim StrQ As String = ""
        Try
            objDataOperation = New clsDataOperation

            If eAssMode = enAssessmentMode.SELF_ASSESSMENT Then
                StrQ = "SELECT * FROM hrbsc_analysis_master WHERE selfemployeeunkid = '" & intEmployeeId & "' AND isvoid = 0 AND iscommitted = 0 "
            Else
                StrQ = "SELECT * FROM hrbsc_analysis_master WHERE assessedemployeeunkid = '" & intEmployeeId & "' AND isvoid = 0 AND iscommitted = 0 "
            End If

            StrQ &= " AND assessmodeid = " & eAssMode

            Dim intCount As Integer = objDataOperation.RecordCount(StrQ)

            If intCount > 0 Then
                mblnFlag = True
            Else
                mblnFlag = False
            End If
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: IsBSC_Pending; Module Name: " & mstrModuleName)
        End Try
        Return mblnFlag
    End Function
    'Pinkal (12-Jun-2012) -- End

    'S.SANDEEP [ 28 DEC 2012 ] -- START
    'ENHANCEMENT : TRA CHANGES
    Public Function Get_Unassessed_Emp(ByVal intCurrPid As Integer) As DataSet
        Dim dsList As New DataSet
        Dim StrQ As String = String.Empty
        Dim exForce As Exception
        Try
            objDataOperation = New clsDataOperation

            StrQ = "SELECT " & _
                           " 1 AS Id " & _
                           ",@GE AS Assess_Type " & _
                           ",employeecode AS Code " & _
                           ",CASE WHEN ISNULL(othername,'') = '' THEN ISNULL(firstname,'')+ ' ' + ISNULL(surname,'') " & _
                           "         WHEN ISNULL(othername,'') <> '' THEN ISNULL(firstname,'')+ ' ' + ISNULL(othername,'')+' '+ISNULL(surname,'') " & _
                           " END AS Ename " & _
                       "FROM hremployee_master " & _
                   " WHERE employeeunkid NOT IN (SELECT selfemployeeunkid FROM hrassess_analysis_master  WHERE selfemployeeunkid > 0 AND isvoid = 0 AND periodunkid = '" & intCurrPid & "')" & _
                       "UNION ALL " & _
                       "SELECT " & _
                           " 2 AS Id " & _
                           ",@BSC AS Assess_Type " & _
                           ",employeecode AS Code " & _
                           ",CASE WHEN ISNULL(othername,'') = '' THEN ISNULL(firstname,'')+ ' ' + ISNULL(surname,'') " & _
                           "         WHEN ISNULL(othername,'') <> '' THEN ISNULL(firstname,'')+ ' ' + ISNULL(othername,'')+' '+ISNULL(surname,'') " & _
                           " END AS Ename " & _
                       "FROM hremployee_master " & _
                   " WHERE employeeunkid NOT IN (SELECT selfemployeeunkid FROM hrbsc_analysis_master  WHERE selfemployeeunkid > 0 AND isvoid = 0 AND periodunkid = '" & intCurrPid & "') "

            objDataOperation.AddParameter("@GE", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 3, "General Evaluation"))
            objDataOperation.AddParameter("@BSC", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 4, "Balanced Score Card"))

            dsList = objDataOperation.ExecQuery(StrQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & " : " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            Return dsList

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: Get_Unassessed_Emp; Module Name: " & mstrModuleName)
        End Try
    End Function
    'S.SANDEEP [ 28 DEC 2012 ] -- END

    'S.SANDEEP [ 13 AUG 2013 ] -- START
    'ENHANCEMENT : TRA CHANGES
    Public Sub Email_Notification(ByVal eAssessMode As enAssessmentMode, _
                                  ByVal iEmployeeId As Integer, _
                                  ByVal iPeriodId As Integer, _
                                  ByVal iYearId As Integer, _
                                  ByVal isReviewerMandatory As Boolean, _
                                  ByVal strDatabaseName As String, _
                                  Optional ByVal iCompanyId As Integer = 0, _
                                  Optional ByVal sArutiSSURL As String = "", _
                                  Optional ByVal sName As String = "", _
                                  Optional ByVal iGroupUnkid As Integer = 0, _
                                  Optional ByVal iWeightAsNumber As Boolean = False, _
                                  Optional ByVal iLoginTypeId As Integer = 0, _
                                  Optional ByVal iLoginEmployeeId As Integer = 0) 'S.SANDEEP [ 22 OCT 2013 ] -- START {iGroupUnkid} -- END
        'Sohail (21 Aug 2015) - [strDatabaseName]
        'S.SANDEEP [ 13 NOV 2013 ] -- START {iWeightAsNumber} --END
        'S.SANDEEP [ 28 JAN 2014 ] -- START {iLoginTypeId,iLoginEmployeeId} -- END

        Dim dsList As DataSet = Nothing
        Dim strLink As String = String.Empty
        Dim StrQ As String = String.Empty
        Dim sYearName As String = String.Empty

        objDataOperation = New clsDataOperation

        Try
            If iCompanyId <= 0 Then iCompanyId = Company._Object._Companyunkid
            If sArutiSSURL = "" Then sArutiSSURL = ConfigParameter._Object._ArutiSelfServiceURL
            Dim objNet As New clsNetConnectivity : If objNet._Conected = False Then Exit Sub

            Select Case eAssessMode
                Case enAssessmentMode.SELF_ASSESSMENT
                    StrQ = "SELECT " & _
                           "  ISNULL(hremployee_master.firstname,'')+' '+ISNULL(hremployee_master.surname,'') AS Ename " & _
                           " ,ISNULL(email,'') AS Email " & _
                           " ,hrassessor_master.assessormasterunkid " & _
                           " ,hrapprover_usermapping.userunkid " & _
                           "FROM hrapprover_usermapping " & _
                           " JOIN hrassessor_master ON hrapprover_usermapping.approverunkid = hrassessor_master.assessormasterunkid " & _
                           " JOIN hremployee_master ON hrassessor_master.employeeunkid = hremployee_master.employeeunkid " & _
                           " JOIN hrassessor_tran ON hrassessor_tran.assessormasterunkid = hrassessor_master.assessormasterunkid " & _
                           "WHERE usertypeid = '" & enUserType.Assessor & "' AND hrassessor_tran.employeeunkid = '" & iEmployeeId & "' AND isreviewer = 0 AND hrassessor_master.isvoid = 0 AND hrassessor_tran.isvoid = 0 "

                Case enAssessmentMode.APPRAISER_ASSESSMENT
                    StrQ = "SELECT " & _
                           "  ISNULL(hremployee_master.firstname,'')+' '+ISNULL(hremployee_master.surname,'') AS Ename " & _
                           " ,ISNULL(email,'') AS Email " & _
                           " ,hrassessor_master.assessormasterunkid " & _
                           " ,hrapprover_usermapping.userunkid " & _
                           "FROM hrapprover_usermapping " & _
                           " JOIN hrassessor_master ON hrapprover_usermapping.approverunkid = hrassessor_master.assessormasterunkid " & _
                           " JOIN hremployee_master ON hrassessor_master.employeeunkid = hremployee_master.employeeunkid " & _
                           " JOIN hrassessor_tran ON hrassessor_tran.assessormasterunkid = hrassessor_master.assessormasterunkid " & _
                           "WHERE usertypeid = '" & enUserType.Assessor & "' AND hrassessor_tran.employeeunkid = '" & iEmployeeId & "' AND isreviewer = 1 AND hrassessor_master.isvoid = 0 AND hrassessor_tran.isvoid = 0 "

                Case enAssessmentMode.REVIEWER_ASSESSMENT
                    StrQ = "SELECT " & _
                           "  ISNULL(hremployee_master.firstname,'')+' '+ISNULL(hremployee_master.surname,'') AS Ename " & _
                           " ,ISNULL(email,'') AS Email " & _
                           " ,employeeunkid AS userunkid " & _
                           "FROM hremployee_master WHERE hremployee_master.employeeunkid = '" & iEmployeeId & "' "
            End Select

            dsList = objDataOperation.ExecQuery(StrQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                mstrMessage = objDataOperation.ErrorNumber & " : " & objDataOperation.ErrorMessage
            End If

            Dim objEmp As New clsEmployee_Master
            Dim objMail As New clsSendMail
            Dim objPeriod As New clscommom_period_Tran

            'Sohail (21 Aug 2015) -- Start
            'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
            'objEmp._Employeeunkid = iEmployeeId : objPeriod._Periodunkid = iPeriodId

            'S.SANDEEP [04 JUN 2015] -- START
            'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
            'objEmp._Employeeunkid = iEmployeeId : objPeriod._Periodunkid(strDatabaseName) = iPeriodId
            objPeriod._Periodunkid(strDatabaseName) = iPeriodId
            objEmp._Employeeunkid(objPeriod._End_Date) = iEmployeeId
            'S.SANDEEP [04 JUN 2015] -- END

            'Sohail (21 Aug 2015) -- End

            Dim dsYear As New DataSet
            Dim objFY As New clsCompany_Master
            dsYear = objFY.GetFinancialYearList(iCompanyId, , "List", objPeriod._Yearunkid)
            If dsYear.Tables("List").Rows.Count > 0 Then
                sYearName = dsYear.Tables("List").Rows(0).Item("financialyear_name")
            End If
            objFY = Nothing

            Select Case eAssessMode
                Case enAssessmentMode.SELF_ASSESSMENT
                    For Each dtRow As DataRow In dsList.Tables("List").Rows
                        If dtRow.Item("Email") = "" Then Continue For
                        Dim strMessage As String = ""
                        objMail._Subject = Language.getMessage(mstrModuleName, 10, "Notifications for General Assessment.")
                        strMessage = "<HTML> <BODY>"
                        strMessage &= Language.getMessage(mstrModuleName, 5, "Dear") & " <B>" & dtRow.Item("Ename").ToString() & "</B>, <BR><BR>"
                        strMessage &= "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;" & Language.getMessage(mstrModuleName, 9, "This is to inform you that, I have completed my General Self-Assessment for the period of  ")
                        strMessage &= "<B>" & objPeriod._Period_Name & "</B> and Year <B>" & sYearName & "</B>" & Language.getMessage(mstrModuleName, 7, ". Please click the link below to assess me as my supervisor.")
                        strMessage &= "<BR><BR>"
                        strMessage &= "<B>" & objEmp._Firstname & " " & objEmp._Surname & "</B>"
                        'S.SANDEEP [ 24 APR 2014 ] -- START
                        'strLink = sArutiSSURL & "/Assessment/General/wPgAssessorAssess_AddEdit.aspx?" & System.Web.HttpUtility.UrlEncode(clsCrypto.Encrypt(iCompanyId.ToString & "|" & dtRow.Item("userunkid").ToString & "|" & iEmployeeId.ToString & "|" & dtRow.Item("assessormasterunkid").ToString & "|" & iYearId.ToString & "|" & sYearName.ToString & "|" & iPeriodId.ToString & "|" & iGroupUnkid.ToString))
                        strLink = sArutiSSURL & "/Assessment/General/wPgTabularAssessor_GE_AddEdit.aspx?" & System.Web.HttpUtility.UrlEncode(clsCrypto.Encrypt(iCompanyId.ToString & "|" & dtRow.Item("userunkid").ToString & "|" & iEmployeeId.ToString & "|" & dtRow.Item("assessormasterunkid").ToString & "|" & iYearId.ToString & "|" & sYearName.ToString & "|" & iPeriodId.ToString & "|" & iGroupUnkid.ToString))
                        'S.SANDEEP [ 24 APR 2014 ] -- END
                        strMessage &= "<BR></BR>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<a href='" & strLink & "'>" & strLink & "</a>"
                        strMessage &= "<BR><BR>"
                        strMessage &= "<B>" & objEmp._Firstname & " " & objEmp._Surname & "</B>"
                        strMessage &= "<BR></BR><BR></BR><center>""POWERED BY ARUTI HR & PAYROLL MANAGEMENT SOFTWARE""</center>"
                        strMessage &= "</BODY></HTML>"
                        objMail._Message = strMessage
                        objMail._ToEmail = dtRow.Item("Email")
                        'S.SANDEEP [ 28 JAN 2014 ] -- START
                        If iLoginTypeId <= 0 Then iLoginTypeId = enLogin_Mode.DESKTOP
                        If mstrWebFrmName.Trim.Length > 0 Then
                            objMail._Form_Name = mstrWebFrmName
                        End If
                        objMail._LogEmployeeUnkid = iLoginEmployeeId
                        objMail._OperationModeId = iLoginTypeId
                        objMail._UserUnkid = mintUserunkid
                        objMail._SenderAddress = IIf(objEmp._Email = "", objEmp._Firstname & " " & objEmp._Surname, objEmp._Email)
                        objMail._ModuleRefId = clsSendMail.enAT_VIEW_TYPE.ASSESSMENT_MGT
                        'S.SANDEEP [ 28 JAN 2014 ] -- END
                        'Sohail (30 Nov 2017) -- Start
                        'SUMATRA Enhancement – SUMATRA – issue # 0001669: Additional feature for Exchange Server configuration setting in 70.1.
                        'objMail.SendMail()
                        objMail.SendMail(iCompanyId)
                        'Sohail (30 Nov 2017) -- End
                    Next
                Case enAssessmentMode.APPRAISER_ASSESSMENT
                    For Each dtRow As DataRow In dsList.Tables("List").Rows
                        If dtRow.Item("Email") = "" Then Continue For
                        Dim strMessage As String = ""
                        objMail._Subject = Language.getMessage(mstrModuleName, 10, "Notifications for General Assessment.")
                        strMessage = "<HTML> <BODY>"
                        strMessage &= Language.getMessage(mstrModuleName, 5, "Dear") & " <B>" & dtRow.Item("Ename").ToString() & "</B>, <BR><BR>"
                        strMessage &= "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;" & Language.getMessage(mstrModuleName, 6, "This is to inform you that, the General Assessment for Employee ")
                        strMessage &= "<B>" & objEmp._Firstname & " " & objEmp._Surname & "</B>" & Language.getMessage(mstrModuleName, 12, " for ") & "<B>" & objPeriod._Period_Name & "</B> and Year <B>" & sYearName & "</B>" & Language.getMessage(mstrModuleName, 8, "is complete. Please click the link below to review it.")
                        strMessage &= "<BR><BR>"
                        strMessage &= "<B>" & objEmp._Firstname & " " & objEmp._Surname & "</B>"
                        'S.SANDEEP [ 24 APR 2014 ] -- START
                        'strLink = sArutiSSURL & "/Assessment/General/wPgReviewerAssess_AddEdit.aspx?" & System.Web.HttpUtility.UrlEncode(clsCrypto.Encrypt(iCompanyId.ToString & "|" & dtRow.Item("userunkid").ToString & "|" & iEmployeeId.ToString & "|" & dtRow.Item("assessormasterunkid").ToString & "|" & iYearId.ToString & "|" & sYearName.ToString & "|" & iPeriodId.ToString & "|" & iGroupUnkid.ToString))
                        strLink = sArutiSSURL & "/Assessment/General/wPgTabularReviewer_GE_AddEdit.aspx?" & System.Web.HttpUtility.UrlEncode(clsCrypto.Encrypt(iCompanyId.ToString & "|" & dtRow.Item("userunkid").ToString & "|" & iEmployeeId.ToString & "|" & dtRow.Item("assessormasterunkid").ToString & "|" & iYearId.ToString & "|" & sYearName.ToString & "|" & iPeriodId.ToString & "|" & iGroupUnkid.ToString))
                        'S.SANDEEP [ 24 APR 2014 ] -- END
                        strMessage &= "<BR></BR>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<a href='" & strLink & "'>" & strLink & "</a>"
                        strMessage &= "<BR><BR>"
                        strMessage &= "<B>" & sName & "</B>"
                        strMessage &= "<BR></BR><BR></BR><center>""POWERED BY ARUTI HR & PAYROLL MANAGEMENT SOFTWARE""</center>"
                        strMessage &= "</BODY></HTML>"
                        objMail._Message = strMessage
                        objMail._ToEmail = dtRow.Item("Email")
                        'S.SANDEEP [ 28 JAN 2014 ] -- START
                        If iLoginTypeId <= 0 Then iLoginTypeId = enLogin_Mode.DESKTOP
                        If mstrWebFrmName.Trim.Length > 0 Then
                            objMail._Form_Name = mstrWebFrmName
                        End If
                        objMail._LogEmployeeUnkid = iLoginEmployeeId
                        objMail._OperationModeId = iLoginTypeId
                        objMail._UserUnkid = mintUserunkid
                        objMail._SenderAddress = IIf(objEmp._Email = "", objEmp._Firstname & " " & objEmp._Surname, objEmp._Email)
                        objMail._ModuleRefId = clsSendMail.enAT_VIEW_TYPE.ASSESSMENT_MGT
                        'S.SANDEEP [ 28 JAN 2014 ] -- END
                        'Sohail (30 Nov 2017) -- Start
                        'SUMATRA Enhancement – SUMATRA – issue # 0001669: Additional feature for Exchange Server configuration setting in 70.1.
                        'objMail.SendMail()
                        objMail.SendMail(iCompanyId)
                        'Sohail (30 Nov 2017) -- End
                    Next
            End Select

            Dim sMsg As String = ""
            If isReviewerMandatory = True Then
                Select Case eAssessMode
                    Case enAssessmentMode.REVIEWER_ASSESSMENT
                        If objEmp._Email.ToString.Trim.Length <= 0 Then Exit Sub
                        'S.SANDEEP [ 13 NOV 2013 ] -- START
                        'sMsg = Notify_Employee(objEmp._Firstname & " " & objEmp._Surname, objPeriod._Period_Name, sYearName, sName)
                        sMsg = Notify_Employee(objEmp._Firstname & " " & objEmp._Surname, objPeriod._Period_Name, sYearName, sName, iEmployeeId, iPeriodId, iWeightAsNumber, iGroupUnkid)
                        'S.SANDEEP [ 13 NOV 2013 ] -- END
                        objMail._Subject = Language.getMessage(mstrModuleName, 10, "Notifications for General Assessment.")
                        objMail._Message = sMsg
                        objMail._ToEmail = objEmp._Email
                        'S.SANDEEP [ 28 JAN 2014 ] -- START
                        If iLoginTypeId <= 0 Then iLoginTypeId = enLogin_Mode.DESKTOP
                        If mstrWebFrmName.Trim.Length > 0 Then
                            objMail._Form_Name = mstrWebFrmName
                        End If
                        objMail._LogEmployeeUnkid = iLoginEmployeeId
                        objMail._OperationModeId = iLoginTypeId
                        objMail._UserUnkid = mintUserunkid
                        objMail._SenderAddress = IIf(objEmp._Email = "", objEmp._Firstname & " " & objEmp._Surname, objEmp._Email)
                        objMail._ModuleRefId = clsSendMail.enAT_VIEW_TYPE.ASSESSMENT_MGT
                        'S.SANDEEP [ 28 JAN 2014 ] -- END
                        'Sohail (30 Nov 2017) -- Start
                        'SUMATRA Enhancement – SUMATRA – issue # 0001669: Additional feature for Exchange Server configuration setting in 70.1.
                        'objMail.SendMail()
                        objMail.SendMail(iCompanyId)
                        'Sohail (30 Nov 2017) -- End
                End Select
            Else
                Select Case eAssessMode
                    Case enAssessmentMode.APPRAISER_ASSESSMENT
                        If objEmp._Email.ToString.Trim.Length <= 0 Then Exit Sub
                        'S.SANDEEP [ 13 NOV 2013 ] -- START
                        'sMsg = Notify_Employee(objEmp._Firstname & " " & objEmp._Surname, objPeriod._Period_Name, sYearName, sName)
                        sMsg = Notify_Employee(objEmp._Firstname & " " & objEmp._Surname, objPeriod._Period_Name, sYearName, sName, iEmployeeId, iPeriodId, iWeightAsNumber, iGroupUnkid)
                        'S.SANDEEP [ 13 NOV 2013 ] -- END
                        objMail._Subject = Language.getMessage(mstrModuleName, 10, "Notifications for General Assessment.")
                        objMail._Message = sMsg
                        objMail._ToEmail = objEmp._Email
                        'S.SANDEEP [ 28 JAN 2014 ] -- START
                        If iLoginTypeId <= 0 Then iLoginTypeId = enLogin_Mode.DESKTOP
                        If mstrWebFrmName.Trim.Length > 0 Then
                            objMail._Form_Name = mstrWebFrmName
                        End If
                        objMail._LogEmployeeUnkid = iLoginEmployeeId
                        objMail._OperationModeId = iLoginTypeId
                        objMail._UserUnkid = mintUserunkid
                        objMail._SenderAddress = IIf(objEmp._Email = "", objEmp._Firstname & " " & objEmp._Surname, objEmp._Email)
                        objMail._ModuleRefId = clsSendMail.enAT_VIEW_TYPE.ASSESSMENT_MGT
                        'S.SANDEEP [ 28 JAN 2014 ] -- END
                        'Sohail (30 Nov 2017) -- Start
                        'SUMATRA Enhancement – SUMATRA – issue # 0001669: Additional feature for Exchange Server configuration setting in 70.1.
                        'objMail.SendMail()
                        objMail.SendMail(iCompanyId)
                        'Sohail (30 Nov 2017) -- End
                End Select
            End If

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "Email_Notification", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Function Notify_Employee(ByVal eName As String, ByVal ePeriodName As String, ByVal eYearName As String, ByVal eARName As String, ByVal iEmpId As Integer, ByVal iPeriodId As Integer, ByVal iWeightNum As Boolean, ByVal iGrpId As Integer) As String 'S.SANDEEP [ 13 NOV 2013 ] -- START {iEmpId,iPeriodId,iWeightNum} -- END
        Dim strMessage As String = ""
        Try
            strMessage = "<HTML> <BODY>"
            strMessage &= Language.getMessage(mstrModuleName, 5, "Dear") & " <B>" & eName & "</B>, <BR><BR>"
            strMessage &= "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;" & Language.getMessage(mstrModuleName, 11, "This is to inform you that, Your General Assessment for the Period of ")
            strMessage &= "<B>" & ePeriodName & "</B> and Year <B>" & eYearName & "</B>" & Language.getMessage(mstrModuleName, 13, " is complete. Please login to Aruti Employee Self Service to view it.")
            strMessage &= "<BR><BR>"
            strMessage &= "<B>" & eARName & "</B>"
            'S.SANDEEP [ 13 NOV 2013 ] -- START
            strMessage &= "<BR><BR>"
            strMessage &= GetNotification_Score(iEmpId, iPeriodId, iWeightNum, iGrpId)
            'S.SANDEEP [ 13 NOV 2013 ] -- END

            'S.SANDEEP [ 09 OCT 2014 ] -- START
            strMessage &= "<BR><BR>"
            strMessage &= "<B>" & Language.getMessage(mstrModuleName, 37, "Note :") & "</B>" & Language.getMessage(mstrModuleName, 38, "This is not final Score you will get final Score upon completion both BSC and General Assessment")
            'S.SANDEEP [ 09 OCT 2014 ] -- END

            strMessage &= "<BR></BR><BR></BR><center>""POWERED BY ARUTI HR & PAYROLL MANAGEMENT SOFTWARE""</center>"
            strMessage &= "</BODY></HTML>"

            Return strMessage

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "Notify_Employee", mstrModuleName)
            Return ""
        Finally
        End Try
    End Function
    'S.SANDEEP [ 13 AUG 2013 ] -- END

    'S.SANDEEP [ 13 NOV 2013 ] -- START
    Private Function GetNotification_Score(ByVal iEmpId As Integer, ByVal iPrdId As Integer, ByVal iWeightNum As Boolean, ByVal iGrpId As Integer) As String
        Dim iHTML_Msg As String = String.Empty
        Dim dsAsr As New DataSet
        Dim dsRev As New DataSet
        Dim StrQ As String = String.Empty
        Try
            Using iDataOpr As New clsDataOperation
                StrQ = "SELECT " & _
                       "     employeecode AS [Code] " & _
                       "    ,firstname+' '+surname AS [Name] "
                If iWeightNum = True Then
                    StrQ &= "    ,CAST(ISNULL(SUM(CAST(ISNULL(resultname,0) AS DECIMAL(10,2))* hrassess_item_master.weight),0) AS DECIMAL(10,2)) AS [Score] "
                Else
                    StrQ &= "    ,ISNULL(SUM(CAST(ISNULL(resultname,0) AS DECIMAL(10,2))),0) AS [Score] "
                End If
                StrQ &= "FROM hrassess_analysis_master " & _
                        "    JOIN hremployee_master ON hrassess_analysis_master.assessoremployeeunkid = hremployee_master.employeeunkid " & _
                        "    JOIN hrassess_analysis_tran ON hrassess_analysis_master.analysisunkid = hrassess_analysis_tran.analysisunkid " & _
                        "    JOIN hrassess_item_master ON hrassess_analysis_tran.assessitemunkid = hrassess_item_master.assessitemunkid " & _
                        "    JOIN hrresult_master ON hrassess_analysis_tran.resultunkid = hrresult_master.resultunkid " & _
                        "WHERE assessedemployeeunkid = '" & iEmpId & "' AND assessmodeid = '" & enAssessmentMode.APPRAISER_ASSESSMENT & "' AND hrassess_analysis_master.periodunkid = '" & iPrdId & "' AND hrassess_analysis_master.assessgroupunkid = '" & iGrpId & "' " & _
                        "GROUP BY employeecode,firstname+' '+surname "

                dsAsr = iDataOpr.ExecQuery(StrQ, "List")

                If iDataOpr.ErrorMessage <> "" Then
                    Throw New Exception(iDataOpr.ErrorNumber & " : " & iDataOpr.ErrorMessage)
                End If

                StrQ = "SELECT " & _
                       "     employeecode AS [Code] " & _
                       "    ,firstname+' '+surname AS [Name] "
                If iWeightNum = True Then
                    StrQ &= "    ,CAST(ISNULL(SUM(CAST(ISNULL(resultname,0) AS DECIMAL(10,2))* hrassess_item_master.weight),0) AS DECIMAL(10,2)) AS [Score] "
                Else
                    StrQ &= "    ,ISNULL(SUM(CAST(ISNULL(resultname,0) AS DECIMAL(10,2))),0) AS [Score] "
                End If
                StrQ &= "FROM hrassess_analysis_master " & _
                        "    JOIN hremployee_master ON hrassess_analysis_master.reviewerunkid = hremployee_master.employeeunkid " & _
                        "    JOIN hrassess_analysis_tran ON hrassess_analysis_master.analysisunkid = hrassess_analysis_tran.analysisunkid " & _
                        "    JOIN hrassess_item_master ON hrassess_analysis_tran.assessitemunkid = hrassess_item_master.assessitemunkid " & _
                        "    JOIN hrresult_master ON hrassess_analysis_tran.resultunkid = hrresult_master.resultunkid " & _
                        "WHERE assessedemployeeunkid = '" & iEmpId & "' AND assessmodeid = '" & enAssessmentMode.REVIEWER_ASSESSMENT & "'  AND hrassess_analysis_master.periodunkid = '" & iPrdId & "' AND hrassess_analysis_master.assessgroupunkid = '" & iGrpId & "' " & _
                        "GROUP BY employeecode,firstname+' '+surname "

                dsRev = iDataOpr.ExecQuery(StrQ, "List")

                If iDataOpr.ErrorMessage <> "" Then
                    Throw New Exception(iDataOpr.ErrorNumber & " : " & iDataOpr.ErrorMessage)
                End If

                If dsAsr.Tables(0).Rows.Count > 0 Or dsRev.Tables(0).Rows.Count > 0 Then
                    iHTML_Msg &= "<TABLE border = '0' WIDTH = '100%'>" & vbCrLf
                    iHTML_Msg &= "<TR WIDTH = '100%'>" & vbCrLf
                    iHTML_Msg &= "<TD align = 'LEFT' STYLE='WIDTH:50%'>" & vbCrLf
                    iHTML_Msg &= "<TABLE border = '1' WIDTH = '50%'>" & vbCrLf
                    iHTML_Msg &= "<TR WIDTH = '50%' bgcolor= 'SteelBlue'>" & vbCrLf
                    iHTML_Msg &= "<TD align = 'LEFT' STYLE='WIDTH:2%'><B><span style='font-size:8.0pt; font-family:" & Chr(34) & "Verdana" & Chr(34) & "," & Chr(34) & "Sans-Serif" & Chr(34) & "; color:#FFFFFF; margin-left:0px;margin-right:0px; display:block;'>" & Language.getMessage(mstrModuleName, 31, "Assessor Code") & "</span></B></TD>" & vbCrLf
                    iHTML_Msg &= "<TD align = 'LEFT' STYLE='WIDTH:5%'><B><span style='font-size:8.0pt; font-family:" & Chr(34) & "Verdana" & Chr(34) & "," & Chr(34) & "Sans-Serif" & Chr(34) & "; color:#FFFFFF; margin-left:0px;margin-right:0px; display:block;'>" & Language.getMessage(mstrModuleName, 32, "Assessor Name") & "</span></B></TD>" & vbCrLf
                    iHTML_Msg &= "<TD align = 'RIGHT' STYLE='WIDTH:1%'><B><span style='font-size:8.0pt; font-family:" & Chr(34) & "Verdana" & Chr(34) & "," & Chr(34) & "Sans-Serif" & Chr(34) & "; color:#FFFFFF; margin-left:0px;margin-right:0px; display:block;'>" & Language.getMessage(mstrModuleName, 33, "Score") & "</span></B></TD>" & vbCrLf
                    iHTML_Msg &= "</TR>" & vbCrLf
                    For Each aRow As DataRow In dsAsr.Tables(0).Rows
                        iHTML_Msg &= "<TR WIDTH = '50%'>" & vbCrLf
                        iHTML_Msg &= "<TD align = 'LEFT' WIDTH = '2%'><span style='font-size:8.0pt; font-family:" & Chr(34) & "Verdana" & Chr(34) & "," & Chr(34) & "Sans-Serif" & Chr(34) & "; color:#000000; margin-left:0px;margin-right:0px; display:block;'>" & aRow.Item("Code").ToString & "</span></TD>" & vbCrLf
                        iHTML_Msg &= "<TD align = 'LEFT' WIDTH = '5%'><span style='font-size:8.0pt; font-family:" & Chr(34) & "Verdana" & Chr(34) & "," & Chr(34) & "Sans-Serif" & Chr(34) & "; color:#000000; margin-left:0px;margin-right:0px; display:block;'>" & aRow.Item("Name").ToString & "</span></TD>" & vbCrLf
                        iHTML_Msg &= "<TD align = 'RIGHT' WIDTH = '1%'><span style='font-size:8.0pt; font-family:" & Chr(34) & "Verdana" & Chr(34) & "," & Chr(34) & "Sans-Serif" & Chr(34) & "; color:#000000; margin-left:0px;margin-right:0px; display:block;'>" & aRow.Item("Score").ToString & "</span></TD>" & vbCrLf
                        iHTML_Msg &= "</TR>" & vbCrLf
                    Next
                    iHTML_Msg &= "</TABLE>" & vbCrLf
                    iHTML_Msg &= "<BR>" & vbCrLf
                    iHTML_Msg &= "<TABLE border = '1' WIDTH = '50%'>" & vbCrLf
                    iHTML_Msg &= "<TR WIDTH = '50%' bgcolor= 'SteelBlue'>" & vbCrLf
                    iHTML_Msg &= "<TD align = 'LEFT' STYLE='WIDTH:2%'><B><span style='font-size:8.0pt; font-family:" & Chr(34) & "Verdana" & Chr(34) & "," & Chr(34) & "Sans-Serif" & Chr(34) & "; color:#FFFFFF; margin-left:0px;margin-right:0px; display:block;'>" & Language.getMessage(mstrModuleName, 34, "Reviewer Code") & "</span></B></TD>" & vbCrLf
                    iHTML_Msg &= "<TD align = 'LEFT' STYLE='WIDTH:5%'><B><span style='font-size:8.0pt; font-family:" & Chr(34) & "Verdana" & Chr(34) & "," & Chr(34) & "Sans-Serif" & Chr(34) & "; color:#FFFFFF; margin-left:0px;margin-right:0px; display:block;'>" & Language.getMessage(mstrModuleName, 35, "Reviewer Name") & "</span></B></TD>" & vbCrLf
                    iHTML_Msg &= "<TD align = 'RIGHT' STYLE='WIDTH:1%'><B><span style='font-size:8.0pt; font-family:" & Chr(34) & "Verdana" & Chr(34) & "," & Chr(34) & "Sans-Serif" & Chr(34) & "; color:#FFFFFF; margin-left:0px;margin-right:0px; display:block;'>" & Language.getMessage(mstrModuleName, 36, "Score") & "</span></B></TD>" & vbCrLf
                    iHTML_Msg &= "</TR>" & vbCrLf
                    For Each aRow As DataRow In dsRev.Tables(0).Rows
                        iHTML_Msg &= "<TR WIDTH = '50%'>" & vbCrLf
                        iHTML_Msg &= "<TD align = 'LEFT' WIDTH = '2%'><span style='font-size:8.0pt; font-family:" & Chr(34) & "Verdana" & Chr(34) & "," & Chr(34) & "Sans-Serif" & Chr(34) & "; color:#000000; margin-left:0px;margin-right:0px; display:block;'>" & aRow.Item("Code").ToString & "</span></TD>" & vbCrLf
                        iHTML_Msg &= "<TD align = 'LEFT' WIDTH = '5%'><span style='font-size:8.0pt; font-family:" & Chr(34) & "Verdana" & Chr(34) & "," & Chr(34) & "Sans-Serif" & Chr(34) & "; color:#000000; margin-left:0px;margin-right:0px; display:block;'>" & aRow.Item("Name").ToString & "</span></TD>" & vbCrLf
                        iHTML_Msg &= "<TD align = 'RIGHT' WIDTH = '1%'><span style='font-size:8.0pt; font-family:" & Chr(34) & "Verdana" & Chr(34) & "," & Chr(34) & "Sans-Serif" & Chr(34) & "; color:#000000; margin-left:0px;margin-right:0px; display:block;'>" & aRow.Item("Score").ToString & "</span></TD>" & vbCrLf
                        iHTML_Msg &= "</TR>" & vbCrLf
                    Next
                    iHTML_Msg &= "</TABLE>" & vbCrLf
                    iHTML_Msg &= "</TD>" & vbCrLf
                    iHTML_Msg &= "</TR>" & vbCrLf
                    iHTML_Msg &= "</TABLE>"
                End If

            End Using

            Return iHTML_Msg
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "GetNotification_Score", mstrModuleName)
            Return iHTML_Msg
        Finally
        End Try
    End Function
    'S.SANDEEP [ 13 NOV 2013 ] -- END


    'S.SANDEEP [ 14 AUG 2013 ] -- START
    'ENHANCEMENT : TRA CHANGES
    Public Function Export_Excel(ByVal intEmployeeId As Integer, _
                                     ByVal intAssessGroupId As Integer, _
                                     ByVal intYearId As Integer, _
                                     ByVal intPeriodId As Integer, Optional ByVal StrDataBaseName As String = "") As System.Text.StringBuilder
        Dim StrQ As String = String.Empty
        Dim dsList As New DataSet
        Dim iBuilder As New System.Text.StringBuilder
        Try

            dsList = GetAssessmentResultView(intEmployeeId, intAssessGroupId, intYearId, intPeriodId, StrDataBaseName)
            dsList.Tables(0).Columns.Remove("assessitemunkid")
            dsList.Tables(0).Columns.Remove("assess_subitemunkid")

            iBuilder.Append("<HTML> " & vbCrLf)
            iBuilder.Append("<TITLE> " & "" & Language.getMessage(mstrModuleName, 21, "General Assessment") & "</TITLE> " & vbCrLf)
            iBuilder.Append("<BODY><FONT FACE =VERDANA FONT SIZE=2> " & vbCrLf)
            iBuilder.Append("<BR> " & vbCrLf)

            iBuilder.Append("<TABLE BORDER=1 WIDTH=100%> " & vbCrLf)
            iBuilder.Append("<TR ALIGN = CENTER VALIGN=TOP> " & vbCrLf)
            iBuilder.Append("<TD COLSPAN = '" & dsList.Tables(0).Columns.Count & "'  align='CENTER' bgcolor='LightGrey'><FONT SIZE=2><B>" & Language.getMessage(mstrModuleName, 21, "General Assessment") & "</B></FONT></TD>" & vbCrLf)
            iBuilder.Append("</TR> " & vbCrLf)

            iBuilder.Append("<TR ALIGN = LEFT VALIGN=TOP> " & vbCrLf)
            For j As Integer = 0 To dsList.Tables(0).Columns.Count - 1
                iBuilder.Append("<TD BORDER=1 WIDTH='30%' ALIGN='MIDDLE'><FONT SIZE=2><B>" & dsList.Tables(0).Columns(j).Caption.ToUpper & "</B></FONT></TD>" & vbCrLf)
            Next
            iBuilder.Append("</TR> " & vbCrLf)
            For i As Integer = 0 To dsList.Tables(0).Rows.Count - 1
                For k As Integer = 0 To dsList.Tables(0).Columns.Count - 1
                    If k Mod 2 = 0 AndAlso k > 0 Then
                        iBuilder.Append("<TD BORDER=1 BORDERCOLOR=BLACK ALIGN='RIGHT'><FONT SIZE=2>" & dsList.Tables(0).Rows(i)(k) & "</FONT></TD>" & vbCrLf)
                    Else
                        iBuilder.Append("<TD BORDER=1 BORDERCOLOR=BLACK ALIGN='LEFT'><FONT SIZE=2>" & dsList.Tables(0).Rows(i)(k) & "</FONT></TD>" & vbCrLf)
                    End If
                Next
                iBuilder.Append(" </TR> " & vbCrLf)
            Next

            iBuilder.Append("<TR ALIGN = LEFT VALIGN=TOP> " & vbCrLf)
            iBuilder.Append("<TD BORDER=1 BORDERCOLOR=BLACK ALIGN='RIGHT' COLSPAN = '2'><FONT SIZE=2><B>" & Language.getMessage(mstrModuleName, 22, "TOTAL") & "</B></FONT></TD>" & vbCrLf)
            Dim dTotal As Decimal = 0
            For k As Integer = 2 To dsList.Tables(0).Columns.Count - 1
                If k Mod 2 = 0 Then
                    For i As Integer = 0 To dsList.Tables(0).Rows.Count - 1
                        'S.SANDEEP [ 22 OCT 2013 ] -- START
                        'dTotal += CDec(dsList.Tables(0).Rows(i)(k))
                        If IsDBNull(dsList.Tables(0).Rows(i)(k)) = True Then
                            dTotal += 0
                        Else
                        dTotal += CDec(dsList.Tables(0).Rows(i)(k))
                        End If
                        'S.SANDEEP [ 22 OCT 2013 ] -- END
                    Next
                    iBuilder.Append("<TD BORDER=1 BORDERCOLOR=BLACK ALIGN='RIGHT'><FONT SIZE=2><B>" & dTotal.ToString("###.#0") & "</B></FONT></TD>" & vbCrLf)
                    dTotal = 0
                Else
                    iBuilder.Append("<TD BORDER=1 BORDERCOLOR=BLACK ALIGN='RIGHT'><FONT SIZE=2><B>&nbsp;</B></FONT></TD>" & vbCrLf)
                End If
            Next
            iBuilder.Append("</TR> " & vbCrLf)
            iBuilder.Append("</TABLE> " & vbCrLf)
            iBuilder.Append("<BR> " & vbCrLf)

            objDataOperation = New clsDataOperation

            StrQ = "SELECT " & _
                       " ISNULL(major_area,'') AS [Major Area for Improvement] " & _
                       ",ISNULL(activity,'') AS [Activity/Action Required to Meet Need] " & _
                       ",ISNULL(support_required,'') AS [Support required from Appraiser or the Management] " & _
                       ",ISNULL(cfcommon_master.name,'') AS [Training or Learning Objectives] " & _
                       ",Convert(char(8),timeframe_date,112) AS [Timeframe for Completion (When)] " & _
                       ",ISNULL(other_training,'') [Other Training] " & _
                   "FROM " & StrDataBaseName & "..hrassess_remarks_tran " & _
                       "LEFT JOIN " & StrDataBaseName & "..cfcommon_master ON cfcommon_master.masterunkid = " & StrDataBaseName & "..hrassess_remarks_tran.coursemasterunkid AND " & StrDataBaseName & "..cfcommon_master.mastertype = " & clsCommon_Master.enCommonMaster.TRAINING_COURSEMASTER & "  " & _
                       "JOIN " & StrDataBaseName & "..hrassess_analysis_master ON " & StrDataBaseName & "..hrassess_remarks_tran.analysisunkid = " & StrDataBaseName & "..hrassess_analysis_master.analysisunkid " & _
                   "WHERE selfemployeeunkid = '" & intEmployeeId & "' AND assessgroupunkid = '" & intAssessGroupId & "' AND " & StrDataBaseName & "..hrassess_analysis_master.isvoid = 0 AND " & StrDataBaseName & "..hrassess_remarks_tran.isvoid = 0 " & _
                   "AND yearunkid = '" & intYearId & "' AND periodunkid = '" & intPeriodId & "' AND isimporvement = 1 "

            dsList = objDataOperation.ExecQuery(StrQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                Throw New Exception(objDataOperation.ErrorNumber & " : " & objDataOperation.ErrorMessage)
            End If

            iBuilder.Append("<TABLE BORDER=1 WIDTH=100%> " & vbCrLf)
            iBuilder.Append("<TR ALIGN = CENTER VALIGN=TOP> " & vbCrLf)
            iBuilder.Append("<TD COLSPAN = '" & dsList.Tables(0).Columns.Count & "'  align='CENTER' bgcolor='LightGrey'><FONT SIZE=2><B>" & Language.getMessage(mstrModuleName, 23, "AREAS FOR IMPROVEMENT: TO BE COMPLETED BY THE APPRAISEE") & "</B></FONT></TD>" & vbCrLf)
            iBuilder.Append("</TR> " & vbCrLf)

            iBuilder.Append("<TR ALIGN = LEFT VALIGN=TOP> " & vbCrLf)
            For j As Integer = 0 To dsList.Tables(0).Columns.Count - 1
                iBuilder.Append("<TD BORDER=1 WIDTH='20%' ALIGN='MIDDLE'><FONT SIZE=2><B>" & dsList.Tables(0).Columns(j).ColumnName.ToUpper & "</B></FONT></TD>" & vbCrLf)
            Next
            iBuilder.Append("</TR> " & vbCrLf)
            For i As Integer = 0 To dsList.Tables(0).Rows.Count - 1
                For k As Integer = 0 To dsList.Tables(0).Columns.Count - 1
                    If dsList.Tables(0).Columns(k).ColumnName = "Timeframe for Completion (When)" Then
                        iBuilder.Append("<TD BORDER=1 BORDERCOLOR=BLACK ALIGN='LEFT'><FONT SIZE=2>" & eZeeDate.convertDate(dsList.Tables(0).Rows(i)(k).ToString).ToShortDateString & "</FONT></TD>" & vbCrLf)
                    Else
                        iBuilder.Append("<TD BORDER=1 BORDERCOLOR=BLACK ALIGN='LEFT'><FONT SIZE=2>" & dsList.Tables(0).Rows(i)(k) & "</FONT></TD>" & vbCrLf)
                    End If
                Next
                iBuilder.Append(" </TR> " & vbCrLf)
            Next
            iBuilder.Append("<BR> " & vbCrLf)

            StrQ = "SELECT " & _
                       " ISNULL(hremployee_master.firstname,'')+' '+ISNULL(hremployee_master.othername,'')+' '+ISNULL(hremployee_master.surname,'') AS [Assessor] " & _
                       ",ISNULL(major_area,'') AS [Major Area for Improvement] " & _
                       ",ISNULL(activity,'') AS [Activity/Action Required to Meet Need] " & _
                       ",ISNULL(support_required,'') AS [Support required from Appraiser or the Management] " & _
                       ",ISNULL(cfcommon_master.name,'') AS [Training or Learning Objectives] " & _
                       ",Convert(char(8),timeframe_date,112) AS [Timeframe for Completion (When)] " & _
                       ",ISNULL(other_training,'') [Other Training] " & _
                   "FROM " & StrDataBaseName & "..hrassess_remarks_tran " & _
                       "JOIN " & StrDataBaseName & "..hrassess_analysis_master ON " & StrDataBaseName & "..hrassess_remarks_tran.analysisunkid = " & StrDataBaseName & "..hrassess_analysis_master.analysisunkid " & _
                       "JOIN " & StrDataBaseName & "..hremployee_master ON " & StrDataBaseName & "..hremployee_master.employeeunkid = " & StrDataBaseName & "..hrassess_analysis_master.assessoremployeeunkid " & _
                       "LEFT JOIN " & StrDataBaseName & "..cfcommon_master ON " & StrDataBaseName & "..cfcommon_master.masterunkid = " & StrDataBaseName & "..hrassess_remarks_tran.coursemasterunkid AND " & StrDataBaseName & "..cfcommon_master.mastertype = " & clsCommon_Master.enCommonMaster.TRAINING_COURSEMASTER & " " & _
                   "WHERE assessedemployeeunkid = '" & intEmployeeId & "' AND assessgroupunkid = '" & intAssessGroupId & "' AND " & StrDataBaseName & "..hrassess_analysis_master.isvoid = 0 AND " & StrDataBaseName & "..hrassess_remarks_tran.isvoid = 0 " & _
                   "AND yearunkid = '" & intYearId & "' AND periodunkid = '" & intPeriodId & "' AND isimporvement = 0 " & _
                   "ORDER BY ISNULL(hremployee_master.firstname,'')+' '+ISNULL(hremployee_master.othername,'')+' '+ISNULL(hremployee_master.surname,'') "

            dsList = objDataOperation.ExecQuery(StrQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                Throw New Exception(objDataOperation.ErrorNumber & " : " & objDataOperation.ErrorMessage)
            End If

            iBuilder.Append("<TR ALIGN = CENTER VALIGN=TOP> " & vbCrLf)
            iBuilder.Append("<TD COLSPAN = '" & dsList.Tables(0).Columns.Count - 1 & "'  align='CENTER' bgcolor='LightGrey'><FONT SIZE=2><B>" & Language.getMessage(mstrModuleName, 24, "PERSONAL DEVELOPMENT PLAN: TO BE JONITLY COMPLETED BY APPRAISEE AND APPRAISER") & "</B></FONT></TD>" & vbCrLf)
            iBuilder.Append("</TR> " & vbCrLf)

            iBuilder.Append("<TR ALIGN = LEFT VALIGN=TOP> " & vbCrLf)
            For j As Integer = 0 To dsList.Tables(0).Columns.Count - 1
                If j <= 0 Then Continue For
                iBuilder.Append("<TD BORDER=1 WIDTH='20%' ALIGN='MIDDLE'><FONT SIZE=2><B>" & dsList.Tables(0).Columns(j).ColumnName.ToUpper & "</B></FONT></TD>" & vbCrLf)
            Next
            iBuilder.Append("</TR> " & vbCrLf)
            iBuilder.Append("<TR> " & vbCrLf)
            Dim sGrp As String = String.Empty
            For i As Integer = 0 To dsList.Tables(0).Rows.Count - 1
                If sGrp <> dsList.Tables(0).Rows(i)("Assessor") Then
                    iBuilder.Append("<TD BORDER=1 BORDERCOLOR=BLACK ALIGN='LEFT'COLSPAN = '" & dsList.Tables(0).Columns.Count & "'><FONT SIZE=2><B>" & Language.getMessage(mstrModuleName, 29, "Assessor") & " : " & dsList.Tables(0).Rows(i)("Assessor") & "</B></FONT></TD>" & vbCrLf)
                    sGrp = dsList.Tables(0).Rows(i)("Assessor")
                    iBuilder.Append("</TR> " & vbCrLf)
                    iBuilder.Append("<TR> " & vbCrLf)
                End If
                For k As Integer = 0 To dsList.Tables(0).Columns.Count - 1
                    If k <= 0 Then Continue For
                    If dsList.Tables(0).Columns(k).ColumnName = "Timeframe for Completion (When)" Then
                        iBuilder.Append("<TD BORDER=1 BORDERCOLOR=BLACK ALIGN='LEFT'><FONT SIZE=2>" & eZeeDate.convertDate(dsList.Tables(0).Rows(i)(k).ToString).ToShortDateString & "</FONT></TD>" & vbCrLf)
                    Else
                        iBuilder.Append("<TD BORDER=1 BORDERCOLOR=BLACK ALIGN='LEFT'><FONT SIZE=2>" & dsList.Tables(0).Rows(i)(k) & "</FONT></TD>" & vbCrLf)
                    End If
                Next
                iBuilder.Append(" </TR> " & vbCrLf)
            Next
            iBuilder.Append("<BR> " & vbCrLf)


            StrQ = "SELECT " & _
                       " ISNULL(hremployee_master.firstname,'')+' '+ISNULL(hremployee_master.othername,'')+' '+ISNULL(hremployee_master.surname,'') AS [Reviewer] " & _
                       ",ISNULL(major_area,'') AS [Major Area for Improvement] " & _
                       ",ISNULL(activity,'') AS [Activity/Action Required to Meet Need] " & _
                       ",ISNULL(support_required,'') AS [Support required from Appraiser or the Management] " & _
                       ",ISNULL(cfcommon_master.name,'') AS [Training or Learning Objectives] " & _
                       ",Convert(char(8),timeframe_date,112) AS [Timeframe for Completion (When)] " & _
                       ",ISNULL(other_training,'') [Other Training] " & _
                   "FROM " & StrDataBaseName & "..hrassess_remarks_tran " & _
                       "JOIN " & StrDataBaseName & "..hrassess_analysis_master ON " & StrDataBaseName & "..hrassess_remarks_tran.analysisunkid = " & StrDataBaseName & "..hrassess_analysis_master.analysisunkid " & _
                       "JOIN " & StrDataBaseName & "..hremployee_master ON " & StrDataBaseName & "..hremployee_master.employeeunkid = " & StrDataBaseName & "..hrassess_analysis_master.reviewerunkid " & _
                       "LEFT JOIN " & StrDataBaseName & "..cfcommon_master ON " & StrDataBaseName & "..cfcommon_master.masterunkid = " & StrDataBaseName & "..hrassess_remarks_tran.coursemasterunkid AND " & StrDataBaseName & "..cfcommon_master.mastertype = " & clsCommon_Master.enCommonMaster.TRAINING_COURSEMASTER & " " & _
                   "WHERE assessedemployeeunkid = '" & intEmployeeId & "' AND assessgroupunkid = '" & intAssessGroupId & "' AND " & StrDataBaseName & "..hrassess_analysis_master.isvoid = 0 AND " & StrDataBaseName & "..hrassess_remarks_tran.isvoid = 0 " & _
                        "AND yearunkid = '" & intYearId & "' AND periodunkid = '" & intPeriodId & "' AND isimporvement = 1 " & _
                   " ORDER BY ISNULL(hremployee_master.firstname,'')+' '+ISNULL(hremployee_master.othername,'')+' '+ISNULL(hremployee_master.surname,'') "

            dsList = objDataOperation.ExecQuery(StrQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                Throw New Exception(objDataOperation.ErrorNumber & " : " & objDataOperation.ErrorMessage)
            End If

            iBuilder.Append("<TR ALIGN = CENTER VALIGN=TOP> " & vbCrLf)
            iBuilder.Append("<TD COLSPAN = '" & dsList.Tables(0).Columns.Count - 1 & "'  align='CENTER' bgcolor='LightGrey'><FONT SIZE=2><B>" & Language.getMessage(mstrModuleName, 25, "AREAS FOR IMPROVEMENT: [REVIEWER TO EMPLOYEE]") & "</B></FONT></TD>" & vbCrLf)
            iBuilder.Append("</TR> " & vbCrLf)

            iBuilder.Append("<TR ALIGN = LEFT VALIGN=TOP> " & vbCrLf)
            For j As Integer = 0 To dsList.Tables(0).Columns.Count - 1
                If j <= 0 Then Continue For
                iBuilder.Append("<TD BORDER=1 WIDTH='20%' ALIGN='MIDDLE'><FONT SIZE=2><B>" & dsList.Tables(0).Columns(j).ColumnName.ToUpper & "</B></FONT></TD>" & vbCrLf)
            Next
            iBuilder.Append("</TR> " & vbCrLf)
            iBuilder.Append("<TR> " & vbCrLf)
            sGrp = String.Empty
            For i As Integer = 0 To dsList.Tables(0).Rows.Count - 1
                If sGrp <> dsList.Tables(0).Rows(i)("Reviewer") Then
                    iBuilder.Append("<TD BORDER=1 BORDERCOLOR=BLACK ALIGN='LEFT'COLSPAN = '" & dsList.Tables(0).Columns.Count & "'><FONT SIZE=2><B>" & Language.getMessage(mstrModuleName, 26, "Reviewer") & " : " & dsList.Tables(0).Rows(i)("Reviewer") & "</B></FONT></TD>" & vbCrLf)
                    sGrp = dsList.Tables(0).Rows(i)("Reviewer")
                    iBuilder.Append("</TR> " & vbCrLf)
                    iBuilder.Append("<TR> " & vbCrLf)
                End If
                For k As Integer = 0 To dsList.Tables(0).Columns.Count - 1
                    If k <= 0 Then Continue For
                    If dsList.Tables(0).Columns(k).ColumnName = "Timeframe for Completion (When)" Then
                        iBuilder.Append("<TD BORDER=1 BORDERCOLOR=BLACK ALIGN='LEFT'><FONT SIZE=2>" & eZeeDate.convertDate(dsList.Tables(0).Rows(i)(k).ToString).ToShortDateString & "</FONT></TD>" & vbCrLf)
                    Else
                        iBuilder.Append("<TD BORDER=1 BORDERCOLOR=BLACK ALIGN='LEFT'><FONT SIZE=2>" & dsList.Tables(0).Rows(i)(k) & "</FONT></TD>" & vbCrLf)
                    End If
                Next
                iBuilder.Append(" </TR> " & vbCrLf)
            Next
            iBuilder.Append("<BR> " & vbCrLf)

            StrQ = "SELECT " & _
                       " ISNULL(hremployee_master.firstname,'')+' '+ISNULL(hremployee_master.othername,'')+' '+ISNULL(hremployee_master.surname,'') AS [Reviewer] " & _
                       ",ISNULL(major_area,'') AS [Major Area for Improvement] " & _
                       ",ISNULL(activity,'') AS [Activity/Action Required to Meet Need] " & _
                       ",ISNULL(support_required,'') AS [Support required from Appraiser or the Management] " & _
                       ",ISNULL(cfcommon_master.name,'') AS [Training or Learning Objectives] " & _
                       ",Convert(char(8),timeframe_date,112) AS [Timeframe for Completion (When)] " & _
                       ",ISNULL(other_training,'') [Other Training] " & _
                   "FROM " & StrDataBaseName & "..hrassess_remarks_tran " & _
                       "JOIN " & StrDataBaseName & "..hrassess_analysis_master ON " & StrDataBaseName & "..hrassess_remarks_tran.analysisunkid = " & StrDataBaseName & "..hrassess_analysis_master.analysisunkid " & _
                       "JOIN " & StrDataBaseName & "..hremployee_master ON " & StrDataBaseName & "..hremployee_master.employeeunkid = " & StrDataBaseName & "..hrassess_analysis_master.reviewerunkid " & _
                       "LEFT JOIN " & StrDataBaseName & "..cfcommon_master ON " & StrDataBaseName & "..cfcommon_master.masterunkid = " & StrDataBaseName & "..hrassess_remarks_tran.coursemasterunkid AND " & StrDataBaseName & "..cfcommon_master.mastertype = " & clsCommon_Master.enCommonMaster.TRAINING_COURSEMASTER & " " & _
                   "WHERE assessedemployeeunkid = '" & intEmployeeId & "' AND assessgroupunkid = '" & intAssessGroupId & "' AND " & StrDataBaseName & "..hrassess_analysis_master.isvoid = 0 AND " & StrDataBaseName & "..hrassess_remarks_tran.isvoid = 0 " & _
                        "AND yearunkid = '" & intYearId & "' AND periodunkid = '" & intPeriodId & "' AND isimporvement = 0 " & _
                   " ORDER BY ISNULL(hremployee_master.firstname,'')+' '+ISNULL(hremployee_master.othername,'')+' '+ISNULL(hremployee_master.surname,'') "

            dsList = objDataOperation.ExecQuery(StrQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                Throw New Exception(objDataOperation.ErrorNumber & " : " & objDataOperation.ErrorMessage)
            End If

            iBuilder.Append("<TR ALIGN = CENTER VALIGN=TOP> " & vbCrLf)
            iBuilder.Append("<TD COLSPAN = '" & dsList.Tables(0).Columns.Count - 1 & "'  align='CENTER' bgcolor='LightGrey'><FONT SIZE=2><B>" & Language.getMessage(mstrModuleName, 27, "PERSONAL DEVELOPMENT PLAN: [REVIEWER TO EMPLOYEE]") & "</B></FONT></TD>" & vbCrLf)
            iBuilder.Append("</TR> " & vbCrLf)

            iBuilder.Append("<TR ALIGN = LEFT VALIGN=TOP> " & vbCrLf)
            For j As Integer = 0 To dsList.Tables(0).Columns.Count - 1
                If j <= 0 Then Continue For
                iBuilder.Append("<TD BORDER=1 WIDTH='20%' ALIGN='MIDDLE'><FONT SIZE=2><B>" & dsList.Tables(0).Columns(j).ColumnName.ToUpper & "</B></FONT></TD>" & vbCrLf)
            Next
            iBuilder.Append("</TR> " & vbCrLf)
            iBuilder.Append("<TR> " & vbCrLf)
            sGrp = String.Empty
            For i As Integer = 0 To dsList.Tables(0).Rows.Count - 1
                If sGrp <> dsList.Tables(0).Rows(i)("Reviewer") Then
                    iBuilder.Append("<TD BORDER=1 BORDERCOLOR=BLACK ALIGN='LEFT'COLSPAN = '" & dsList.Tables(0).Columns.Count & "'><FONT SIZE=2><B>" & Language.getMessage(mstrModuleName, 26, "Reviewer") & " : " & dsList.Tables(0).Rows(i)("Reviewer") & "</B></FONT></TD>" & vbCrLf)
                    sGrp = dsList.Tables(0).Rows(i)("Reviewer")
                    iBuilder.Append("</TR> " & vbCrLf)
                    iBuilder.Append("<TR> " & vbCrLf)
                End If
                For k As Integer = 0 To dsList.Tables(0).Columns.Count - 1
                    If k <= 0 Then Continue For
                    If dsList.Tables(0).Columns(k).ColumnName = "Timeframe for Completion (When)" Then
                        iBuilder.Append("<TD BORDER=1 BORDERCOLOR=BLACK ALIGN='LEFT'><FONT SIZE=2>" & eZeeDate.convertDate(dsList.Tables(0).Rows(i)(k).ToString).ToShortDateString & "</FONT></TD>" & vbCrLf)
                    Else
                        iBuilder.Append("<TD BORDER=1 BORDERCOLOR=BLACK ALIGN='LEFT'><FONT SIZE=2>" & dsList.Tables(0).Rows(i)(k) & "</FONT></TD>" & vbCrLf)
                    End If
                Next
                iBuilder.Append(" </TR> " & vbCrLf)
            Next
            iBuilder.Append("</TABLE> " & vbCrLf)
            iBuilder.Append("<BR> " & vbCrLf)
            iBuilder.Append("<TABLE BORDER=0 WIDTH=100%> " & vbCrLf)
            iBuilder.Append("<TR> " & vbCrLf)
            iBuilder.Append("<TD COLSPAN = '4' align='LEFT'><FONT SIZE=1><B>" & Language.getMessage(mstrModuleName, 28, "Appraisee's Signature:..............") & "</B></FONT></TD>" & vbCrLf)
            iBuilder.Append("<TD COLSPAN = '4' align='LEFT'><FONT SIZE=1><B>" & Language.getMessage(mstrModuleName, 14, "Appraiser's Signature:..............") & "</B></FONT></TD>" & vbCrLf)
            iBuilder.Append("</TR> " & vbCrLf)
            iBuilder.Append("<TR> " & vbCrLf)
            iBuilder.Append("<TD COLSPAN = '4' align='LEFT'><FONT SIZE=1><B>" & Language.getMessage(mstrModuleName, 15, "Date:..............") & "</B></FONT></TD>" & vbCrLf)
            iBuilder.Append("<TD COLSPAN = '4' align='LEFT'><FONT SIZE=1><B>" & Language.getMessage(mstrModuleName, 15, "Date:..............") & "</B></FONT></TD>" & vbCrLf)
            iBuilder.Append("</TR> " & vbCrLf)
            iBuilder.Append("</TABLE> " & vbCrLf)
            iBuilder.Append("<BR> " & vbCrLf)

            StrQ = "SELECT ISNULL(reviewer_remark1,'') AS R1 ,ISNULL(reviewer_remark2,'') AS R2 FROM " & StrDataBaseName & "..hrassess_analysis_master " & _
                   "WHERE assessmodeid = 3 AND isvoid= 0 AND iscommitted = 1 " & _
                   " AND assessedemployeeunkid = '" & intEmployeeId & "' AND assessgroupunkid = '" & intAssessGroupId & "' " & _
                   " AND yearunkid = '" & intYearId & "' AND periodunkid = '" & intPeriodId & "'"

            dsList = objDataOperation.ExecQuery(StrQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                Throw New Exception(objDataOperation.ErrorNumber & " : " & objDataOperation.ErrorMessage)
            End If

            iBuilder.Append("<TABLE BORDER=1 WIDTH=100%> " & vbCrLf)
            iBuilder.Append("<TR> " & vbCrLf)
            iBuilder.Append("<TD COLSPAN = '6' align='LEFT'><FONT SIZE=2><B>" & Language.getMessage(mstrModuleName, 16, "Comments and Recommendations by the Deputy Commissioner General/Head of Department") & "</B></FONT></TD>" & vbCrLf)
            iBuilder.Append("</TR> " & vbCrLf)
            iBuilder.Append("<TR> " & vbCrLf)
            If dsList.Tables(0).Rows.Count > 0 Then
                iBuilder.Append("<TD COLSPAN = '6' align='LEFT' ROWSPAN = '4' valign='Top'><FONT SIZE=1>" & dsList.Tables(0).Rows(0).Item("R1") & "</FONT></TD>" & vbCrLf)
            Else
                iBuilder.Append("<TD COLSPAN = '6' align='LEFT' ROWSPAN = '4'><FONT SIZE=1>&nbsp;</FONT></TD>" & vbCrLf)
            End If
            iBuilder.Append("</TR> " & vbCrLf)
            iBuilder.Append("</TABLE> " & vbCrLf)
            iBuilder.Append("<BR> " & vbCrLf)

            iBuilder.Append("<TABLE BORDER=0 WIDTH=100%> " & vbCrLf)
            iBuilder.Append("<TR> " & vbCrLf)
            iBuilder.Append("<TD COLSPAN = '2' align='LEFT'><FONT SIZE=2><B>" & Language.getMessage(mstrModuleName, 17, "Name:..............") & "</B></FONT></TD>" & vbCrLf)
            iBuilder.Append("<TD COLSPAN = '2' align='LEFT'><FONT SIZE=2><B>" & Language.getMessage(mstrModuleName, 18, "Signature:..............") & "</B></FONT></TD>" & vbCrLf)
            iBuilder.Append("<TD COLSPAN = '2' align='LEFT'><FONT SIZE=2><B>" & Language.getMessage(mstrModuleName, 19, "Date:..............") & "</B></FONT></TD>" & vbCrLf)
            iBuilder.Append("</TR> " & vbCrLf)
            iBuilder.Append("</TABLE> " & vbCrLf)
            iBuilder.Append("<BR> " & vbCrLf)

            iBuilder.Append("<TABLE BORDER=1 WIDTH=100%> " & vbCrLf)
            iBuilder.Append("<TR> " & vbCrLf)
            iBuilder.Append("<TD COLSPAN = '6' align='LEFT'><FONT SIZE=2><B>" & Language.getMessage(mstrModuleName, 20, "Comments and Recommendations by the Commissioner General/Deputy Commissioner General") & "</B></FONT></TD>" & vbCrLf)
            iBuilder.Append("</TR> " & vbCrLf)
            iBuilder.Append("<TR> " & vbCrLf)
            If dsList.Tables(0).Rows.Count > 0 Then
                iBuilder.Append("<TD COLSPAN = '6' align='LEFT' ROWSPAN = '4' valign='Top'><FONT SIZE=1>" & dsList.Tables(0).Rows(0).Item("R1") & "</FONT></TD>" & vbCrLf)
            Else
                iBuilder.Append("<TD COLSPAN = '6' align='LEFT' ROWSPAN = '4'>&nbsp;</TD>" & vbCrLf)
            End If
            iBuilder.Append("</TR> " & vbCrLf)
            iBuilder.Append("</TABLE> " & vbCrLf)
            iBuilder.Append("<BR> " & vbCrLf)

            iBuilder.Append("<TABLE BORDER=0 WIDTH=100%> " & vbCrLf)
            iBuilder.Append("</TR> " & vbCrLf)
            iBuilder.Append("<TD COLSPAN = '2' align='LEFT'><FONT SIZE=2><B>" & Language.getMessage(mstrModuleName, 17, "Name:..............") & "</B></FONT></TD>" & vbCrLf)
            iBuilder.Append("<TD COLSPAN = '2' align='LEFT'><FONT SIZE=2><B>" & Language.getMessage(mstrModuleName, 18, "Signature:..............") & "</B></FONT></TD>" & vbCrLf)
            iBuilder.Append("<TD COLSPAN = '2' align='LEFT'><FONT SIZE=2><B>" & Language.getMessage(mstrModuleName, 19, "Date:..............") & "</B></FONT></TD>" & vbCrLf)
            iBuilder.Append("<TR> " & vbCrLf)
            iBuilder.Append("</TABLE> " & vbCrLf)

            iBuilder.Append(" </BODY> " & vbCrLf)
            iBuilder.Append(" </HTML> " & vbCrLf)

            Return iBuilder

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "Export_Excel", mstrModuleName)
            Return iBuilder
        Finally
        End Try
    End Function

    Public Function Get_Analysis_Id(ByVal eMode As enAssessmentMode, _
                                    ByVal iEmpId As Integer, _
                                    ByVal iPeriod As Integer, _
                                    ByVal iGroupId As Integer, _
                                    Optional ByVal iAEmpId As Integer = 0, _
                                    Optional ByVal iREmpId As Integer = 0) As Integer
        objDataOperation = New clsDataOperation
        Dim iValueId As Integer = 0
        Try
            Dim StrQ As String = String.Empty
            Dim dsList As New DataSet

            StrQ = "SELECT analysisunkid AS Id FROM hrassess_analysis_master WHERE isvoid = 0 "
            Select Case eMode
                Case enAssessmentMode.SELF_ASSESSMENT
                    StrQ &= " AND selfemployeeunkid = '" & iEmpId & "' "
                Case enAssessmentMode.APPRAISER_ASSESSMENT
                    StrQ &= " AND assessedemployeeunkid = '" & iEmpId & "' AND assessoremployeeunkid = '" & iAEmpId & "' "
                Case enAssessmentMode.REVIEWER_ASSESSMENT
                    StrQ &= " AND assessedemployeeunkid = '" & iEmpId & "' AND reviewerunkid = '" & iREmpId & "' "
            End Select
            StrQ &= " AND assessgroupunkid = '" & iGroupId & "' AND periodunkid = '" & iPeriod & "' "

            dsList = objDataOperation.ExecQuery(StrQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                Throw New Exception(objDataOperation.ErrorNumber & " : " & objDataOperation.ErrorMessage)
            End If

            If dsList.Tables("List").Rows.Count > 0 Then
                iValueId = dsList.Tables("List").Rows(0).Item("Id")
            End If

            Return iValueId

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: Get_Analysis_Id; Module Name: " & mstrModuleName)
        Finally
        End Try
    End Function
    'S.SANDEEP [ 14 AUG 2013 ] -- END

    'S.SANDEEP [ 24 APR 2014 ] -- START
    Public Function GetTabularData(ByVal iEmployeeId As Integer, ByVal iPeriodId As Integer, Optional ByVal iAssessGrpId As Integer = -1) As DataSet
        Dim StrQ As String = String.Empty
        Dim exForce As Exception
        Dim dtTable As DataTable
        Dim dCol As DataColumn
        Dim dsList As New DataSet
        objDataOperation = New clsDataOperation
        Try
            dtTable = New DataTable("View")
            dCol = New DataColumn
            With dCol
                .ColumnName = "Items"
                .DefaultValue = ""
                .DataType = System.Type.GetType("System.String")                
            End With
            dtTable.Columns.Add(dCol)

            dCol = New DataColumn
            With dCol
                .ColumnName = "Weight"
                .DefaultValue = ""
                .DataType = System.Type.GetType("System.String")
            End With
            dtTable.Columns.Add(dCol)

            dCol = New DataColumn
            With dCol
                .ColumnName = "ScoreGuide"
                .DefaultValue = Language.getMessage(mstrModuleName, 37, "View Guide")
                .DataType = System.Type.GetType("System.String")
            End With
            dtTable.Columns.Add(dCol)

            dCol = New DataColumn
            With dCol
                .ColumnName = "Result"
                .DefaultValue = ""
                .DataType = System.Type.GetType("System.String")
            End With
            dtTable.Columns.Add(dCol)

            dCol = New DataColumn
            With dCol
                .ColumnName = "Remark"
                .DefaultValue = ""
                .DataType = System.Type.GetType("System.String")
            End With
            dtTable.Columns.Add(dCol)

            dCol = New DataColumn
            With dCol
                .ColumnName = "assessgroupunkid"
                .DefaultValue = 0
                .DataType = System.Type.GetType("System.Int32")
            End With
            dtTable.Columns.Add(dCol)

            dCol = New DataColumn
            With dCol
                .ColumnName = "assessitemunkid"
                .DefaultValue = 0
                .DataType = System.Type.GetType("System.Int32")
            End With
            dtTable.Columns.Add(dCol)

            dCol = New DataColumn
            With dCol
                .ColumnName = "assesssubitemunkid"
                .DefaultValue = 0
                .DataType = System.Type.GetType("System.Int32")
            End With
            dtTable.Columns.Add(dCol)

            dCol = New DataColumn
            With dCol
                .ColumnName = "IsGrp"
                .DefaultValue = False
                .DataType = System.Type.GetType("System.Boolean")
            End With
            dtTable.Columns.Add(dCol)

            dCol = New DataColumn
            With dCol
                .ColumnName = "GrpId"
                .DefaultValue = 0
                .DataType = System.Type.GetType("System.Int32")
            End With
            dtTable.Columns.Add(dCol)

            dCol = New DataColumn
            With dCol
                .ColumnName = "iEdit"
                .DefaultValue = 0
                .DataType = System.Type.GetType("System.Int32")
            End With
            dtTable.Columns.Add(dCol)

            dCol = New DataColumn
            With dCol
                .ColumnName = "RGrpId"
                .DefaultValue = 0
                .DataType = System.Type.GetType("System.Int32")
            End With
            dtTable.Columns.Add(dCol)
            If iEmployeeId > 0 Then


                If iAssessGrpId <= 0 Then
                    Dim objAssessGrp As New clsassess_group_master
                    dsList = objAssessGrp.getListForCombo(iEmployeeId, "List", False)
                    objAssessGrp = Nothing
                Else
                    StrQ = "SELECT " & _
                           "     assessgroupunkid " & _
                           "    ,assessgroup_name AS name " & _
                           "FROM hrassess_group_master " & _
                           "WHERE assessgroupunkid IN ('" & iAssessGrpId & "') "

                    dsList = objDataOperation.ExecQuery(StrQ, "List")

                    If objDataOperation.ErrorMessage <> "" Then
                        exForce = New Exception(objDataOperation.ErrorNumber & " : " & objDataOperation.ErrorMessage)
                        Throw exForce
                    End If
                End If

                If dsList.Tables(0).Rows.Count > 0 Then
                    Dim dsTran As New DataSet

                    StrQ = "SELECT " & _
                            "     SGrpName aItems " & _
                            "    ,CAST(iWeight AS NVARCHAR(MAX)) AS aWeight " & _
                            "    ,SGrpId " & _
                            "    ,MGrpId " & _
                            "    ,Sub_ItemId " & _
                            "    ,RGrpId " & _
                            "    ,ISNULL(iCnt,0) AS iCnt " & _
                            "    ,CASE WHEN ISNULL(iCnt,0)> 0 AND Sub_ItemId <= 0 THEN 0 " & _
                            "          WHEN ISNULL(iCnt,0)<=0 AND Sub_ItemId <= 0 THEN 1 " & _
                            "		   WHEN ISNULL(iCnt,0)> 0 AND Sub_ItemId >  0 THEN 1 " & _
                            "     END AS iEdit " & _
                            "FROM " & _
                            "( " & _
                            "    SELECT " & _
                            "         assessgroup_name AS MGrpName " & _
                            "        ,hrassess_item_master.name AS SGrpName " & _
                            "        ,hrassess_group_master.assessgroupunkid AS MGrpId " & _
                            "        ,hrassess_item_master.assessitemunkid AS SGrpId " & _
                            "        ,hrassess_item_master.weight AS [iWeight] " & _
                            "        ,0 AS SortId " & _
                            "        ,CAST(1 AS BIT) AS IsMainGrp " & _
                            "        ,ROW_NUMBER() OVER (PARTITION BY hrassess_group_master.assessgroupunkid ORDER BY hrassess_group_master.assessgroupunkid ASC) AS rNo " & _
                            "        ,0 AS Sub_ItemId " & _
                            "        ,hrassess_item_master.resultgroupunkid AS RGrpId " & _
                            "    FROM hrassess_item_master " & _
                            "        JOIN hrassess_group_master ON hrassess_item_master.assessgroupunkid = hrassess_group_master.assessgroupunkid " & _
                            "    WHERE hrassess_item_master.isactive = 1 AND periodunkid = '" & iPeriodId & "' " & _
                            "UNION ALL " & _
                            "    SELECT " & _
                            "         assessgroup_name AS MGrpName " & _
                            "        ,Space(5) + CAST(ROW_NUMBER()OVER(PARTITION BY hrassess_item_master.assessitemunkid ORDER BY assess_subitemunkid) AS NVARCHAR(MAX))+'. '+ISNULL(hrassess_subitem_master.name,'') AS SGrpName " & _
                            "        ,hrassess_group_master.assessgroupunkid AS MGrpId " & _
                            "        ,hrassess_item_master.assessitemunkid AS SGrpId " & _
                            "        ,hrassess_item_master.weight AS [iWeight] " & _
                            "        ,1 AS SortId " & _
                            "        ,CAST(0 AS BIT) AS IsMainGrp " & _
                            "        ,0 " & _
                            "        ,assess_subitemunkid AS Sub_ItemId " & _
                            "        ,hrassess_item_master.resultgroupunkid AS RGrpId " & _
                            "    FROM hrassess_item_master " & _
                            "        JOIN hrassess_subitem_master ON hrassess_item_master.assessitemunkid = hrassess_subitem_master.assessitemunkid " & _
                            "        JOIN hrassess_group_master ON hrassess_item_master.assessgroupunkid = hrassess_group_master.assessgroupunkid " & _
                            "    WHERE hrassess_item_master.isactive = 1 AND periodunkid = '" & iPeriodId & "' " & _
                            ") AS A " & _
                            "LEFT JOIN " & _
                            "( " & _
                            "	SELECT " & _
                            "		 assessitemunkid AS iCntId " & _
                            "		,COUNT(assess_subitemunkid) AS iCnt " & _
                            "	FROM hrassess_subitem_master WHERE isactive = 1 " & _
                            "	GROUP BY assessitemunkid " & _
                            ")AS B ON B.iCntId = A.SGrpId " & _
                            "WHERE 1 = 1 ORDER BY A.MGrpName,SGrpId,SortId "

                    dsTran = objDataOperation.ExecQuery(StrQ, "List")

                    If objDataOperation.ErrorMessage <> "" Then
                        exForce = New Exception(objDataOperation.ErrorNumber & " : " & objDataOperation.ErrorMessage)
                        Throw exForce
                    End If

                    For Each dRow As DataRow In dsList.Tables(0).Rows
                        Dim dtRow As DataRow = dtTable.NewRow
                        dtRow.Item("Items") = dRow.Item("name")
                        dtRow.Item("assessgroupunkid") = dRow.Item("assessgroupunkid")
                        dtRow.Item("IsGrp") = True
                        dtRow.Item("GrpId") = dRow.Item("assessgroupunkid")
                        dtTable.Rows.Add(dtRow)

                        Dim dtFilter As DataTable = New DataView(dsTran.Tables(0), "MGrpId = '" & dRow.Item("assessgroupunkid") & "'", "", DataViewRowState.CurrentRows).ToTable
                        For Each dfRow As DataRow In dtFilter.Rows
                            dtRow = dtTable.NewRow
                            dtRow.Item("Items") = Space(5) & dfRow.Item("aItems")
                            dtRow.Item("Weight") = dfRow.Item("aWeight")
                            dtRow.Item("assessgroupunkid") = dRow.Item("assessgroupunkid")
                            dtRow.Item("assessitemunkid") = dfRow.Item("SGrpId")
                            dtRow.Item("assesssubitemunkid") = dfRow.Item("Sub_ItemId")
                            dtRow.Item("IsGrp") = False
                            dtRow.Item("GrpId") = dRow.Item("assessgroupunkid")
                            dtRow.Item("RGrpId") = dfRow.Item("RGrpId")
                            dtRow.Item("iEdit") = dfRow.Item("iEdit")
                            dtTable.Rows.Add(dtRow)
                        Next
                    Next
                End If
                dsList.Tables.RemoveAt(0)
            End If

            dsList.Tables.Add(dtTable.Copy)

            Return dsList

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "GetTabularData", mstrModuleName)
            Return Nothing
        Finally
        End Try
    End Function
    'S.SANDEEP [ 24 APR 2014 ] -- END


	'<Language> This Auto Generated Text Please Do Not Modify it.
#Region " Language & UI Settings "
	Public Shared Sub SetMessages()
		Try
			Language.setMessage(mstrModuleName, 1, "Select")
			Language.setMessage(mstrModuleName, 2, "Sorry, You cannot delete this Assessment. Reason : This Assessor has already assessed this employee.")
			Language.setMessage(mstrModuleName, 3, "General Evaluation")
			Language.setMessage(mstrModuleName, 4, "Balanced Score Card")
			Language.setMessage(mstrModuleName, 5, "Dear")
			Language.setMessage(mstrModuleName, 6, "This is to inform you that, the General Assessment for Employee")
			Language.setMessage(mstrModuleName, 7, ". Please click the link below to assess me as my supervisor.")
			Language.setMessage(mstrModuleName, 8, "is complete. Please click the link below to review it.")
			Language.setMessage(mstrModuleName, 9, "This is to inform you that, I have completed my General Self-Assessment for the period of")
			Language.setMessage(mstrModuleName, 10, "Notifications for General Assessment.")
			Language.setMessage(mstrModuleName, 11, "This is to inform you that, Your General Assessment for the Period of")
			Language.setMessage(mstrModuleName, 12, " for")
			Language.setMessage(mstrModuleName, 13, " is complete. Please login to Aruti Employee Self Service to view it.")
			Language.setMessage(mstrModuleName, 14, "Appraiser's Signature:..............")
			Language.setMessage(mstrModuleName, 15, "Date:..............")
			Language.setMessage(mstrModuleName, 16, "Comments and Recommendations by the Deputy Commissioner General/Head of Department")
			Language.setMessage(mstrModuleName, 17, "Name:..............")
			Language.setMessage(mstrModuleName, 18, "Signature:..............")
			Language.setMessage(mstrModuleName, 19, "Date:..............")
			Language.setMessage(mstrModuleName, 20, "Comments and Recommendations by the Commissioner General/Deputy Commissioner General")
			Language.setMessage(mstrModuleName, 21, "General Assessment")
			Language.setMessage(mstrModuleName, 22, "TOTAL")
			Language.setMessage(mstrModuleName, 23, "AREAS FOR IMPROVEMENT: TO BE COMPLETED BY THE APPRAISEE")
			Language.setMessage(mstrModuleName, 24, "PERSONAL DEVELOPMENT PLAN: TO BE JONITLY COMPLETED BY APPRAISEE AND APPRAISER")
			Language.setMessage(mstrModuleName, 25, "AREAS FOR IMPROVEMENT: [REVIEWER TO EMPLOYEE]")
			Language.setMessage(mstrModuleName, 26, "Reviewer")
			Language.setMessage(mstrModuleName, 27, "PERSONAL DEVELOPMENT PLAN: [REVIEWER TO EMPLOYEE]")
			Language.setMessage(mstrModuleName, 28, "Appraisee's Signature:..............")
			Language.setMessage(mstrModuleName, 29, "Assessor")
			Language.setMessage(mstrModuleName, 30, "Sorry, assessment already present for the selected period and employee. Please define new assessment.")
			Language.setMessage(mstrModuleName, 31, "Assessor Code")
			Language.setMessage(mstrModuleName, 32, "Assessor Name")
			Language.setMessage(mstrModuleName, 33, "Score")
			Language.setMessage(mstrModuleName, 34, "Reviewer Code")
			Language.setMessage(mstrModuleName, 35, "Reviewer Name")
			Language.setMessage(mstrModuleName, 36, "Score")

		Catch Ex As Exception
			DisplayError.Show("-1", ex.Message, "SetMessages", mstrModuleName)
		End Try
	End Sub
#End Region 'Language & UI Settings
	'</Language>
End Class

'Public Class clsassess_analysis_master1
'    Private Shared ReadOnly mstrModuleName As String = "clsassess_analysis_master"
'    Dim objDataOperation As clsDataOperation
'    Dim mstrMessage As String = ""

'#Region " Private variables "
'    Private mintAnalysisunkid As Integer
'    Private mintYearunkid As Integer
'    Private mintPeriodunkid As Integer
'    Private mintSelfemployeeunkid As Integer
'    Private mintAssessormasterunkid As Integer
'    Private mintAssessoremployeeunkid As Integer
'    Private mintAssessedemployeeunkid As Integer
'    Private mintAssessgroupunkid As Integer
'    Private mintAssessitemunkid As Integer
'    Private mintResultcodeunkid As Integer
'    Private mstrRemark As String = String.Empty
'    Private mstrStrength As String = String.Empty
'    Private mstrImprovement As String = String.Empty
'    Private mstrTrainingneed As String = String.Empty
'    Private mblnIsselfassessed As Boolean
'    Private mintUserunkid As Integer
'    Private mblnIsvoid As Boolean
'    Private mintVoiduserunkid As Integer
'    Private mdtVoiddatetime As Date
'    Private mstrVoidreason As String = String.Empty
'    Private mdtAssessmentdate As Date
'    'S.SANDEEP [ 22 JULY 2011 ] -- START
'    'ENHANCEMENT : WEB & DESKTOP COMMIT OPTION TO BE INCLUDED
'    Private mblnIscommitted As Boolean = False
'    'S.SANDEEP [ 22 JULY 2011 ] -- END 
'#End Region

'#Region " Properties "

'    ''' <summary>
'    ''' Purpose: Get Message from Class 
'    ''' Modify By: Sandeep J. Sharma
'    ''' </summary>
'    Public ReadOnly Property _Message() As String
'        Get
'            Return mstrMessage
'        End Get
'    End Property

'    ''' <summary>
'    ''' Purpose: Get or Set analysisunkid
'    ''' Modify By: Sandeep J. Sharma
'    ''' </summary>
'    Public Property _Analysisunkid() As Integer
'        Get
'            Return mintAnalysisunkid
'        End Get
'        Set(ByVal value As Integer)
'            mintAnalysisunkid = value
'            Call GetData()
'        End Set
'    End Property

'    ''' <summary>
'    ''' Purpose: Get or Set yearunkid
'    ''' Modify By: Sandeep J. Sharma
'    ''' </summary>
'    Public Property _Yearunkid() As Integer
'        Get
'            Return mintYearunkid
'        End Get
'        Set(ByVal value As Integer)
'            mintYearunkid = value
'        End Set
'    End Property

'    ''' <summary>
'    ''' Purpose: Get or Set periodunkid
'    ''' Modify By: Sandeep J. Sharma
'    ''' </summary>
'    Public Property _Periodunkid() As Integer
'        Get
'            Return mintPeriodunkid
'        End Get
'        Set(ByVal value As Integer)
'            mintPeriodunkid = value
'        End Set
'    End Property

'    ''' <summary>
'    ''' Purpose: Get or Set selfemployeeunkid
'    ''' Modify By: Sandeep J. Sharma
'    ''' </summary>
'    Public Property _Selfemployeeunkid() As Integer
'        Get
'            Return mintSelfemployeeunkid
'        End Get
'        Set(ByVal value As Integer)
'            mintSelfemployeeunkid = value
'        End Set
'    End Property

'    ''' <summary>
'    ''' Purpose: Get or Set assessormasterunkid
'    ''' Modify By: Sandeep J. Sharma
'    ''' </summary>
'    Public Property _Assessormasterunkid() As Integer
'        Get
'            Return mintAssessormasterunkid
'        End Get
'        Set(ByVal value As Integer)
'            mintAssessormasterunkid = value
'        End Set
'    End Property

'    ''' <summary>
'    ''' Purpose: Get or Set assessoremployeeunkid
'    ''' Modify By: Sandeep J. Sharma
'    ''' </summary>
'    Public Property _Assessoremployeeunkid() As Integer
'        Get
'            Return mintAssessoremployeeunkid
'        End Get
'        Set(ByVal value As Integer)
'            mintAssessoremployeeunkid = value
'        End Set
'    End Property

'    ''' <summary>
'    ''' Purpose: Get or Set assessedemployeeunkid
'    ''' Modify By: Sandeep J. Sharma
'    ''' </summary>
'    Public Property _Assessedemployeeunkid() As Integer
'        Get
'            Return mintAssessedemployeeunkid
'        End Get
'        Set(ByVal value As Integer)
'            mintAssessedemployeeunkid = value
'        End Set
'    End Property

'    ''' <summary>
'    ''' Purpose: Get or Set assessgroupunkid
'    ''' Modify By: Sandeep J. Sharma
'    ''' </summary>
'    Public Property _Assessgroupunkid() As Integer
'        Get
'            Return mintAssessgroupunkid
'        End Get
'        Set(ByVal value As Integer)
'            mintAssessgroupunkid = value
'        End Set
'    End Property

'    ''' <summary>
'    ''' Purpose: Get or Set assessitemunkid
'    ''' Modify By: Sandeep J. Sharma
'    ''' </summary>
'    Public Property _Assessitemunkid() As Integer
'        Get
'            Return mintAssessitemunkid
'        End Get
'        Set(ByVal value As Integer)
'            mintAssessitemunkid = value
'        End Set
'    End Property

'    ''' <summary>
'    ''' Purpose: Get or Set resultcodeunkid
'    ''' Modify By: Sandeep J. Sharma
'    ''' </summary>
'    Public Property _Resultcodeunkid() As Integer
'        Get
'            Return mintResultcodeunkid
'        End Get
'        Set(ByVal value As Integer)
'            mintResultcodeunkid = value
'        End Set
'    End Property

'    ''' <summary>
'    ''' Purpose: Get or Set assessmentdate
'    ''' Modify By: Sandeep J. Sharma
'    ''' </summary>
'    Public Property _Assessmentdate() As Date
'        Get
'            Return mdtAssessmentdate
'        End Get
'        Set(ByVal value As Date)
'            mdtAssessmentdate = value
'        End Set
'    End Property

'    ''' <summary>
'    ''' Purpose: Get or Set remark
'    ''' Modify By: Sandeep J. Sharma
'    ''' </summary>
'    Public Property _Remark() As String
'        Get
'            Return mstrRemark
'        End Get
'        Set(ByVal value As String)
'            mstrRemark = value
'        End Set
'    End Property

'    ''' <summary>
'    ''' Purpose: Get or Set strength
'    ''' Modify By: Sandeep J. Sharma
'    ''' </summary>
'    Public Property _Strength() As String
'        Get
'            Return mstrStrength
'        End Get
'        Set(ByVal value As String)
'            mstrStrength = value
'        End Set
'    End Property

'    ''' <summary>
'    ''' Purpose: Get or Set improvement
'    ''' Modify By: Sandeep J. Sharma
'    ''' </summary>
'    Public Property _Improvement() As String
'        Get
'            Return mstrImprovement
'        End Get
'        Set(ByVal value As String)
'            mstrImprovement = value
'        End Set
'    End Property

'    ''' <summary>
'    ''' Purpose: Get or Set trainingneed
'    ''' Modify By: Sandeep J. Sharma
'    ''' </summary>
'    Public Property _Trainingneed() As String
'        Get
'            Return mstrTrainingneed
'        End Get
'        Set(ByVal value As String)
'            mstrTrainingneed = value
'        End Set
'    End Property

'    ''' <summary>
'    ''' Purpose: Get or Set isselfassessed
'    ''' Modify By: Sandeep J. Sharma
'    ''' </summary>
'    Public Property _Isselfassessed() As Boolean
'        Get
'            Return mblnIsselfassessed
'        End Get
'        Set(ByVal value As Boolean)
'            mblnIsselfassessed = value
'        End Set
'    End Property

'    ''' <summary>
'    ''' Purpose: Get or Set userunkid
'    ''' Modify By: Sandeep J. Sharma
'    ''' </summary>
'    Public Property _Userunkid() As Integer
'        Get
'            Return mintUserunkid
'        End Get
'        Set(ByVal value As Integer)
'            mintUserunkid = value
'        End Set
'    End Property

'    ''' <summary>
'    ''' Purpose: Get or Set isvoid
'    ''' Modify By: Sandeep J. Sharma
'    ''' </summary>
'    Public Property _Isvoid() As Boolean
'        Get
'            Return mblnIsvoid
'        End Get
'        Set(ByVal value As Boolean)
'            mblnIsvoid = value
'        End Set
'    End Property

'    ''' <summary>
'    ''' Purpose: Get or Set voiduserunkid
'    ''' Modify By: Sandeep J. Sharma
'    ''' </summary>
'    Public Property _Voiduserunkid() As Integer
'        Get
'            Return mintVoiduserunkid
'        End Get
'        Set(ByVal value As Integer)
'            mintVoiduserunkid = value
'        End Set
'    End Property

'    ''' <summary>
'    ''' Purpose: Get or Set voiddatetime
'    ''' Modify By: Sandeep J. Sharma
'    ''' </summary>
'    Public Property _Voiddatetime() As Date
'        Get
'            Return mdtVoiddatetime
'        End Get
'        Set(ByVal value As Date)
'            mdtVoiddatetime = value
'        End Set
'    End Property

'    ''' <summary>
'    ''' Purpose: Get or Set voidreason
'    ''' Modify By: Sandeep J. Sharma
'    ''' </summary>
'    Public Property _Voidreason() As String
'        Get
'            Return mstrVoidreason
'        End Get
'        Set(ByVal value As String)
'            mstrVoidreason = value
'        End Set
'    End Property

'    'S.SANDEEP [ 22 JULY 2011 ] -- START
'    'ENHANCEMENT : WEB & DESKTOP COMMIT OPTION TO BE INCLUDED
'    ''' <summary>
'    ''' Purpose: Get or Set iscommitted
'    ''' Modify By: Sandeep J. Sharma
'    ''' </summary>
'    Public Property _IsCommitted() As Boolean
'        Get
'            Return mblnIscommitted
'        End Get
'        Set(ByVal value As Boolean)
'            mblnIscommitted = value
'        End Set
'    End Property
'    'S.SANDEEP [ 22 JULY 2011 ] -- END 

'#End Region

'    ''' <summary>
'    ''' Modify By: Sandeep J. Sharma
'    ''' </summary>
'    ''' <purpose> Assign all Property variable </purpose>
'    Public Sub GetData()
'        Dim dsList As DataSet = Nothing
'        Dim strQ As String = ""
'        Dim exForce As Exception

'        objDataOperation = New clsDataOperation

'        Try

'            'S.SANDEEP [ 22 JULY 2011 ] -- START
'            'ENHANCEMENT : WEB & DESKTOP COMMIT OPTION TO BE INCLUDED
'            'strQ = "SELECT " & _
'            '              "  analysisunkid " & _
'            '              ", yearunkid " & _
'            '              ", periodunkid " & _
'            '              ", selfemployeeunkid " & _
'            '              ", assessormasterunkid " & _
'            '              ", assessoremployeeunkid " & _
'            '              ", assessedemployeeunkid " & _
'            '              ", assessgroupunkid " & _
'            '              ", assessitemunkid " & _
'            '              ", resultcodeunkid " & _
'            '              ", assessmentdate " & _
'            '              ", remark " & _
'            '              ", strength " & _
'            '              ", improvement " & _
'            '              ", trainingneed " & _
'            '              ", isselfassessed " & _
'            '              ", userunkid " & _
'            '              ", isvoid " & _
'            '              ", voiduserunkid " & _
'            '              ", voiddatetime " & _
'            '              ", voidreason " & _
'            '             "FROM hrassess_analysis_master " & _
'            '             "WHERE analysisunkid = @analysisunkid "

'            strQ = "SELECT " & _
'              "  analysisunkid " & _
'              ", yearunkid " & _
'              ", periodunkid " & _
'              ", selfemployeeunkid " & _
'              ", assessormasterunkid " & _
'              ", assessoremployeeunkid " & _
'              ", assessedemployeeunkid " & _
'              ", assessgroupunkid " & _
'              ", assessitemunkid " & _
'              ", resultcodeunkid " & _
'              ", assessmentdate " & _
'              ", remark " & _
'              ", strength " & _
'              ", improvement " & _
'              ", trainingneed " & _
'              ", isselfassessed " & _
'              ", userunkid " & _
'              ", isvoid " & _
'              ", voiduserunkid " & _
'              ", voiddatetime " & _
'              ", voidreason " & _
'                         ", ISNULL(iscommitted,0) As iscommitted " & _
'             "FROM hrassess_analysis_master " & _
'             "WHERE analysisunkid = @analysisunkid "
'            'S.SANDEEP [ 22 JULY 2011 ] -- END 

'            objDataOperation.AddParameter("@analysisunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintAnalysisunkid.ToString)

'            dsList = objDataOperation.ExecQuery(strQ, "List")

'            If objDataOperation.ErrorMessage <> "" Then
'                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
'                Throw exForce
'            End If

'            For Each dtRow As DataRow In dsList.Tables(0).Rows
'                mintAnalysisunkid = CInt(dtRow.Item("analysisunkid"))
'                mintYearunkid = CInt(dtRow.Item("yearunkid"))
'                mintPeriodunkid = CInt(dtRow.Item("periodunkid"))
'                mintSelfemployeeunkid = CInt(dtRow.Item("selfemployeeunkid"))
'                mintAssessormasterunkid = CInt(dtRow.Item("assessormasterunkid"))
'                mintAssessoremployeeunkid = CInt(dtRow.Item("assessoremployeeunkid"))
'                mintAssessedemployeeunkid = CInt(dtRow.Item("assessedemployeeunkid"))
'                mintAssessgroupunkid = CInt(dtRow.Item("assessgroupunkid"))
'                mintAssessitemunkid = CInt(dtRow.Item("assessitemunkid"))
'                mintResultcodeunkid = CInt(dtRow.Item("resultcodeunkid"))
'                mdtAssessmentdate = dtRow.Item("assessmentdate")
'                mstrRemark = dtRow.Item("remark").ToString
'                mstrStrength = dtRow.Item("strength").ToString
'                mstrImprovement = dtRow.Item("improvement").ToString
'                mstrTrainingneed = dtRow.Item("trainingneed").ToString
'                mblnIsselfassessed = CBool(dtRow.Item("isselfassessed"))
'                mintUserunkid = CInt(dtRow.Item("userunkid"))
'                mblnIsvoid = CBool(dtRow.Item("isvoid"))
'                mintVoiduserunkid = CInt(dtRow.Item("voiduserunkid"))
'                If IsDBNull(dtRow.Item("voiddatetime")) Then
'                    mdtVoiddatetime = Nothing
'                Else
'                    mdtVoiddatetime = dtRow.Item("voiddatetime")
'                End If
'                mstrVoidreason = dtRow.Item("voidreason").ToString
'                'S.SANDEEP [ 22 JULY 2011 ] -- START
'                'ENHANCEMENT : WEB & DESKTOP COMMIT OPTION TO BE INCLUDED
'                mblnIscommitted = dtRow.Item("iscommitted")
'                'S.SANDEEP [ 22 JULY 2011 ] -- END 
'                Exit For
'            Next
'        Catch ex As Exception
'            Throw New Exception(ex.Message & "; Procedure Name: GetData; Module Name: " & mstrModuleName)
'        Finally
'            exForce = Nothing
'            If dsList IsNot Nothing Then dsList.Dispose()
'            objDataOperation = Nothing
'        End Try
'    End Sub

'    ''' <summary>
'    ''' Modify By: Sandeep J. Sharma
'    ''' </summary>
'    ''' <purpose> Assign all Property variable </purpose>
'    Public Function GetList(ByVal strTableName As String, ByVal blnIsSelf As Boolean) As DataSet
'        Dim dsList As DataSet = Nothing
'        Dim strQ As String = ""
'        Dim exForce As Exception

'        objDataOperation = New clsDataOperation

'        Try

'            'S.SANDEEP [ 22 JULY 2011 ] -- START
'            'ENHANCEMENT : WEB & DESKTOP COMMIT OPTION TO BE INCLUDED
'            'strQ = "SELECT " & _
'            '              "  hrassess_analysis_master.analysisunkid " & _
'            '              ", hrassess_analysis_master.yearunkid " & _
'            '              ", hrassess_analysis_master.periodunkid " & _
'            '              ", hrassess_analysis_master.selfemployeeunkid " & _
'            '              ", hrassess_analysis_master.assessormasterunkid " & _
'            '              ", hrassess_analysis_master.assessoremployeeunkid " & _
'            '              ", hrassess_analysis_master.assessedemployeeunkid " & _
'            '              ", hrassess_analysis_master.assessgroupunkid " & _
'            '              ", hrassess_analysis_master.assessitemunkid " & _
'            '              ", hrassess_analysis_master.resultcodeunkid " & _
'            '              ", Convert(Char(8),hrassess_analysis_master.assessmentdate,112) AS assessmentdate " & _
'            '              ", hrassess_analysis_master.remark " & _
'            '              ", hrassess_analysis_master.strength " & _
'            '              ", hrassess_analysis_master.improvement " & _
'            '              ", hrassess_analysis_master.trainingneed " & _
'            '              ", hrassess_analysis_master.isselfassessed " & _
'            '              ", hrassess_analysis_master.userunkid " & _
'            '              ", hrassess_analysis_master.isvoid " & _
'            '              ", hrassess_analysis_master.voiduserunkid " & _
'            '              ", hrassess_analysis_master.voiddatetime " & _
'            '              ", hrassess_analysis_master.voidreason " & _
'            '              ", ISNULL(hremployee_master.firstname,'')+' '+ISNULL(hremployee_master.othername,'')+' '+ISNULL(hremployee_master.surname,'') As EmpName " & _
'            '              ", ISNULL(hrmsConfiguration..cffinancial_year_tran.financialyear_name,'') AS YearName " & _
'            '              ", ISNULL(cfcommon_period_tran.period_name,'') AS PName "

'            strQ = "SELECT " & _
'              "  hrassess_analysis_master.analysisunkid " & _
'              ", hrassess_analysis_master.yearunkid " & _
'              ", hrassess_analysis_master.periodunkid " & _
'              ", hrassess_analysis_master.selfemployeeunkid " & _
'              ", hrassess_analysis_master.assessormasterunkid " & _
'              ", hrassess_analysis_master.assessoremployeeunkid " & _
'              ", hrassess_analysis_master.assessedemployeeunkid " & _
'              ", hrassess_analysis_master.assessgroupunkid " & _
'              ", hrassess_analysis_master.assessitemunkid " & _
'              ", hrassess_analysis_master.resultcodeunkid " & _
'              ", Convert(Char(8),hrassess_analysis_master.assessmentdate,112) AS assessmentdate " & _
'              ", hrassess_analysis_master.remark " & _
'              ", hrassess_analysis_master.strength " & _
'              ", hrassess_analysis_master.improvement " & _
'              ", hrassess_analysis_master.trainingneed " & _
'              ", hrassess_analysis_master.isselfassessed " & _
'              ", hrassess_analysis_master.userunkid " & _
'              ", hrassess_analysis_master.isvoid " & _
'              ", hrassess_analysis_master.voiduserunkid " & _
'              ", hrassess_analysis_master.voiddatetime " & _
'              ", hrassess_analysis_master.voidreason " & _
'              ", ISNULL(hremployee_master.firstname,'')+' '+ISNULL(hremployee_master.othername,'')+' '+ISNULL(hremployee_master.surname,'') As EmpName " & _
'              ", ISNULL(hrmsConfiguration..cffinancial_year_tran.financialyear_name,'') AS YearName " & _
'                      ", ISNULL(cfcommon_period_tran.period_name,'') AS PName " & _
'                      ", ISNULL(iscommitted,0) AS iscommitted " & _
'                      ", ISNULL(hremployee_master.employeecode,'') AS Ecode " & _
'                      ", cfcommon_period_tran.statusid AS Sid "
'            'S.SANDEEP [ 22 JULY 2011 ] -- END 

'            If blnIsSelf = False Then
'                strQ &= ", ISNULL(AEMP.firstname,'')+' '+ISNULL(AEMP.othername,'')+' '+ISNULL(AEMP.surname,'') AS Assessor "
'            Else
'                strQ &= ", ' ' as Assessor "
'            End If
'            strQ &= "FROM hrassess_analysis_master " & _
'                    "   JOIN hrmsConfiguration..cffinancial_year_tran on hrmsConfiguration..cffinancial_year_tran.yearunkid = hrassess_analysis_master.yearunkid " & _
'                    "   JOIN cfcommon_period_tran on cfcommon_period_tran.periodunkid = hrassess_analysis_master.periodunkid "

'            If blnIsSelf = True Then

'                strQ &= " JOIN hremployee_master ON hrassess_analysis_master.selfemployeeunkid = hremployee_master.employeeunkid "

'            Else
'                strQ &= " JOIN hremployee_master ON hrassess_analysis_master.assessedemployeeunkid = hremployee_master.employeeunkid " & _
'                                        " LEFT JOIN hremployee_master AS AEMP ON AEMP.employeeunkid = hrassess_analysis_master.assessoremployeeunkid " & _
'                                        " LEFT JOIN hrassessor_master ON hrassessor_master.assessormasterunkid = hrassess_analysis_master.assessormasterunkid " & _
'                                        " LEFT JOIN hrapprover_usermapping ON hrapprover_usermapping.approverunkid = hrassessor_master.assessormasterunkid " & _
'                                        " AND hrapprover_usermapping.userunkid = " & User._Object._Userunkid & " " & _
'                                        " AND hrapprover_usermapping.usertypeid = " & enUserType.Assessor & " "
'            End If

'            strQ &= "WHERE isvoid = 0 "


'            'S.SANDEEP [ 29 JUNE 2011 ] -- START
'            'ISSUE : INACTIVE EMPLOYEE(S) COMING ON LIST
'            If ConfigParameter._Object._IsIncludeInactiveEmp = False Then
'                If blnIsSelf = True Then
'                    strQ &= " AND ISNULL(hremployee_master.isactive,0) = 1 "
'                Else
'                    strQ &= " AND ISNULL(hremployee_master.isactive,0) = 1 AND ISNULL(AEMP.isactive,0) = 1 "
'                End If
'            End If
'            'S.SANDEEP [ 29 JUNE 2011 ] -- END 



'            'Anjan (24 Jun 2011)-Start
'            'Issue : According to prvilege that lower level user should not see superior level employees.
'            If UserAccessLevel._AccessLevel.Length > 0 Then
'                'S.SANDEEP [ 29 JUNE 2011 ] -- START
'                'ISSUE : INACTIVE EMPLOYEE(S) COMING ON LIST
'                If blnIsSelf = True Then
'                    'If mblnIsselfassessed = True Then
'                    'S.SANDEEP [ 29 JUNE 2011 ] -- END 
'                    strQ &= "  AND hremployee_master.jobunkid IN (" & UserAccessLevel._AccessLevel & ") "
'                Else
'                    strQ &= " AND hremployee_master.jobunkid IN (" & UserAccessLevel._AccessLevel & ") AND AEMP.jobunkid IN (" & UserAccessLevel._AccessLevel & ")  "
'                End If
'            End If
'            'Anjan (24 Jun 2011)-End 


'            dsList = objDataOperation.ExecQuery(strQ, strTableName)

'            If objDataOperation.ErrorMessage <> "" Then
'                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
'                Throw exForce
'            End If

'        Catch ex As Exception
'            Throw New Exception(ex.Message & "; Procedure Name: GetList; Module Name: " & mstrModuleName)
'        Finally
'            exForce = Nothing
'            If dsList IsNot Nothing Then dsList.Dispose()
'            objDataOperation = Nothing
'        End Try
'        Return dsList
'    End Function

'    ''' <summary>
'    ''' Modify By: Sandeep J. Sharma
'    ''' </summary>
'    ''' <returns>Boolean</returns>
'    ''' <purpose> INSERT INTO Database Table (hrassess_analysis_master) </purpose>
'    Public Function Insert() As Boolean
'        If mblnIsselfassessed = True Then
'            If isExist(mintSelfemployeeunkid, mintYearunkid, mintPeriodunkid, mintAssessitemunkid) Then
'                mstrMessage = Language.getMessage(mstrModuleName, 1, "This Self Assessment is already defined for this period. Please define new Self Assessment.")
'                Return False
'            End If
'        Else

'            'Sandeep [ 16 MAY 2011 ] -- Start
'            'If isExist(mintSelfemployeeunkid, mintYearunkid, mintPeriodunkid, mintAssessitemunkid, mintAssessormasterunkid) Then
'            If isExist(mintAssessedemployeeunkid, mintYearunkid, mintPeriodunkid, mintAssessitemunkid, mintAssessormasterunkid) Then
'                'Sandeep [ 16 MAY 2011 ] -- End 
'                mstrMessage = Language.getMessage(mstrModuleName, 2, "This Assessor Assessment is already defined for this period. Please define new Assessment.")
'                Return False
'            End If
'        End If

'        Dim dsList As DataSet = Nothing
'        Dim strQ As String = ""
'        Dim exForce As Exception

'        'Pinkal (12-Oct-2011) -- Start
'        'ENHANCEMENT : AUDIT TRAIL MAINTENANCE
'        objDataOperation = New clsDataOperation
'        objDataOperation.BindTransaction()
'        'Pinkal (12-Oct-2011) -- End

'        Try
'            objDataOperation.AddParameter("@yearunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintYearunkid.ToString)
'            objDataOperation.AddParameter("@periodunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintPeriodunkid.ToString)
'            objDataOperation.AddParameter("@selfemployeeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintSelfemployeeunkid.ToString)
'            objDataOperation.AddParameter("@assessormasterunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintAssessormasterunkid.ToString)
'            objDataOperation.AddParameter("@assessoremployeeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintAssessoremployeeunkid.ToString)
'            objDataOperation.AddParameter("@assessedemployeeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintAssessedemployeeunkid.ToString)
'            objDataOperation.AddParameter("@assessgroupunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintAssessgroupunkid.ToString)
'            objDataOperation.AddParameter("@assessitemunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintAssessitemunkid.ToString)
'            objDataOperation.AddParameter("@resultcodeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintResultcodeunkid.ToString)
'            objDataOperation.AddParameter("@assessmentdate", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, mdtAssessmentdate)
'            objDataOperation.AddParameter("@remark", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrRemark.ToString)
'            objDataOperation.AddParameter("@strength", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrStrength.ToString)
'            objDataOperation.AddParameter("@improvement", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrImprovement.ToString)
'            objDataOperation.AddParameter("@trainingneed", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrTrainingneed.ToString)
'            objDataOperation.AddParameter("@isselfassessed", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsselfassessed.ToString)
'            objDataOperation.AddParameter("@userunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintUserunkid.ToString)
'            objDataOperation.AddParameter("@isvoid", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsvoid.ToString)
'            objDataOperation.AddParameter("@voiduserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintVoiduserunkid.ToString)
'            If mdtVoiddatetime = Nothing Then
'                objDataOperation.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, DBNull.Value)
'            Else
'                objDataOperation.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, mdtVoiddatetime)
'            End If
'            objDataOperation.AddParameter("@voidreason", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrVoidreason.ToString)

'            'S.SANDEEP [ 22 JULY 2011 ] -- START
'            'ENHANCEMENT : WEB & DESKTOP COMMIT OPTION TO BE INCLUDED
'            objDataOperation.AddParameter("@iscommitted", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIscommitted.ToString)
'            'strQ = "INSERT INTO hrassess_analysis_master ( " & _
'            '                    "  yearunkid " & _
'            '                    ", periodunkid " & _
'            '                    ", selfemployeeunkid " & _
'            '                    ", assessormasterunkid " & _
'            '                    ", assessoremployeeunkid " & _
'            '                    ", assessedemployeeunkid " & _
'            '                    ", assessgroupunkid " & _
'            '                    ", assessitemunkid " & _
'            '                    ", resultcodeunkid " & _
'            '                    ", assessmentdate " & _
'            '                    ", remark " & _
'            '                    ", strength " & _
'            '                    ", improvement " & _
'            '                    ", trainingneed " & _
'            '                    ", isselfassessed " & _
'            '                    ", userunkid " & _
'            '                    ", isvoid " & _
'            '                    ", voiduserunkid " & _
'            '                    ", voiddatetime " & _
'            '                    ", voidreason" & _
'            '                  ") VALUES (" & _
'            '                    "  @yearunkid " & _
'            '                    ", @periodunkid " & _
'            '                    ", @selfemployeeunkid " & _
'            '                    ", @assessormasterunkid " & _
'            '                    ", @assessoremployeeunkid " & _
'            '                    ", @assessedemployeeunkid " & _
'            '                    ", @assessgroupunkid " & _
'            '                    ", @assessitemunkid " & _
'            '                    ", @resultcodeunkid " & _
'            '                    ", @assessmentdate " & _
'            '                    ", @remark " & _
'            '                    ", @strength " & _
'            '                    ", @improvement " & _
'            '                    ", @trainingneed " & _
'            '                    ", @isselfassessed " & _
'            '                    ", @userunkid " & _
'            '                    ", @isvoid " & _
'            '                    ", @voiduserunkid " & _
'            '                    ", @voiddatetime " & _
'            '                    ", @voidreason" & _
'            '                  "); SELECT @@identity"
'            strQ = "INSERT INTO hrassess_analysis_master ( " & _
'                    "  yearunkid " & _
'                    ", periodunkid " & _
'                    ", selfemployeeunkid " & _
'                    ", assessormasterunkid " & _
'                    ", assessoremployeeunkid " & _
'                    ", assessedemployeeunkid " & _
'                    ", assessgroupunkid " & _
'                    ", assessitemunkid " & _
'                    ", resultcodeunkid " & _
'                    ", assessmentdate " & _
'                    ", remark " & _
'                    ", strength " & _
'                    ", improvement " & _
'                    ", trainingneed " & _
'                    ", isselfassessed " & _
'                    ", userunkid " & _
'                    ", isvoid " & _
'                    ", voiduserunkid " & _
'                    ", voiddatetime " & _
'                    ", voidreason" & _
'                                ", iscommitted " & _
'                  ") VALUES (" & _
'                    "  @yearunkid " & _
'                    ", @periodunkid " & _
'                    ", @selfemployeeunkid " & _
'                    ", @assessormasterunkid " & _
'                    ", @assessoremployeeunkid " & _
'                    ", @assessedemployeeunkid " & _
'                    ", @assessgroupunkid " & _
'                    ", @assessitemunkid " & _
'                    ", @resultcodeunkid " & _
'                    ", @assessmentdate " & _
'                    ", @remark " & _
'                    ", @strength " & _
'                    ", @improvement " & _
'                    ", @trainingneed " & _
'                    ", @isselfassessed " & _
'                    ", @userunkid " & _
'                    ", @isvoid " & _
'                    ", @voiduserunkid " & _
'                    ", @voiddatetime " & _
'                    ", @voidreason" & _
'                                ", @iscommitted " & _
'                  "); SELECT @@identity"
'            'S.SANDEEP [ 22 JULY 2011 ] -- END 

'            dsList = objDataOperation.ExecQuery(strQ, "List")

'            If objDataOperation.ErrorMessage <> "" Then
'                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
'                Throw exForce
'            End If

'            mintAnalysisunkid = dsList.Tables(0).Rows(0).Item(0)

'            'Pinkal (12-Oct-2011) -- Start
'            'ENHANCEMENT : AUDIT TRAIL MAINTENANCE

'            If clsCommonATLog.Insert_AtLog(objDataOperation, 1, "hrassess_analysis_master", "analysisunkid", mintAnalysisunkid) = False Then
'                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
'                Throw exForce
'            End If
'            objDataOperation.ReleaseTransaction(True)
'            'Pinkal (12-Oct-2011) -- End

'            Return True
'        Catch ex As Exception
'            'Pinkal (12-Oct-2011) -- Start
'            'ENHANCEMENT : AUDIT TRAIL MAINTENANCE
'            objDataOperation.ReleaseTransaction(False)
'            'Pinkal (12-Oct-2011) -- End
'            Throw New Exception(ex.Message & "; Procedure Name: Insert; Module Name: " & mstrModuleName)
'            Return False
'        Finally
'            exForce = Nothing
'            If dsList IsNot Nothing Then dsList.Dispose()
'            objDataOperation = Nothing
'        End Try
'    End Function

'    ''' <summary>
'    ''' Modify By: Sandeep J. Sharma
'    ''' </summary>
'    ''' <returns>Boolean</returns>
'    ''' <purpose> Update Database Table (hrassess_analysis_master) </purpose>
'    Public Function Update() As Boolean
'        If mblnIsselfassessed = True Then
'            If isExist(mintSelfemployeeunkid, mintYearunkid, mintPeriodunkid, mintAssessitemunkid, , mintAnalysisunkid) Then
'                mstrMessage = Language.getMessage(mstrModuleName, 1, "This Self Assessment is already defined for this period. Please define new Self Assessment.")
'                Return False
'            End If
'        Else

'            'Sandeep [ 16 MAY 2011 ] -- Start
'            ' If isExist(mintSelfemployeeunkid, mintYearunkid, mintPeriodunkid, mintAssessitemunkid, mintAssessormasterunkid, mintAnalysisunkid) Then
'            If isExist(mintAssessedemployeeunkid, mintYearunkid, mintPeriodunkid, mintAssessitemunkid, mintAssessormasterunkid, mintAnalysisunkid) Then
'                'Sandeep [ 16 MAY 2011 ] -- End 
'                mstrMessage = Language.getMessage(mstrModuleName, 2, "This Assessor Assessment is already defined for this period. Please define new Assessment.")
'                Return False
'            End If
'        End If

'        Dim dsList As DataSet = Nothing
'        Dim strQ As String = ""
'        Dim exForce As Exception

'        'Pinkal (12-Oct-2011) -- Start
'        'ENHANCEMENT : AUDIT TRAIL MAINTENANCE
'        objDataOperation = New clsDataOperation
'        objDataOperation.BindTransaction()
'        'Pinkal (12-Oct-2011) -- End

'        Try
'            objDataOperation.AddParameter("@analysisunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintAnalysisunkid.ToString)
'            objDataOperation.AddParameter("@yearunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintYearunkid.ToString)
'            objDataOperation.AddParameter("@periodunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintPeriodunkid.ToString)
'            objDataOperation.AddParameter("@selfemployeeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintSelfemployeeunkid.ToString)
'            objDataOperation.AddParameter("@assessormasterunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintAssessormasterunkid.ToString)
'            objDataOperation.AddParameter("@assessoremployeeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintAssessoremployeeunkid.ToString)
'            objDataOperation.AddParameter("@assessedemployeeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintAssessedemployeeunkid.ToString)
'            objDataOperation.AddParameter("@assessgroupunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintAssessgroupunkid.ToString)
'            objDataOperation.AddParameter("@assessitemunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintAssessitemunkid.ToString)
'            objDataOperation.AddParameter("@resultcodeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintResultcodeunkid.ToString)
'            objDataOperation.AddParameter("@assessmentdate", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, mdtAssessmentdate)
'            objDataOperation.AddParameter("@remark", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrRemark.ToString)
'            objDataOperation.AddParameter("@strength", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrStrength.ToString)
'            objDataOperation.AddParameter("@improvement", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrImprovement.ToString)
'            objDataOperation.AddParameter("@trainingneed", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrTrainingneed.ToString)
'            objDataOperation.AddParameter("@isselfassessed", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsselfassessed.ToString)
'            objDataOperation.AddParameter("@userunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintUserunkid.ToString)
'            objDataOperation.AddParameter("@isvoid", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsvoid.ToString)
'            objDataOperation.AddParameter("@voiduserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintVoiduserunkid.ToString)
'            If mdtVoiddatetime = Nothing Then
'                objDataOperation.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, DBNull.Value)
'            Else
'                objDataOperation.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, mdtVoiddatetime)
'            End If
'            objDataOperation.AddParameter("@voidreason", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrVoidreason.ToString)

'            'S.SANDEEP [ 22 JULY 2011 ] -- START
'            'ENHANCEMENT : WEB & DESKTOP COMMIT OPTION TO BE INCLUDED
'            objDataOperation.AddParameter("@iscommitted", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIscommitted.ToString)
'            'strQ = "UPDATE hrassess_analysis_master SET " & _
'            '        "  yearunkid = @yearunkid" & _
'            '        ", periodunkid = @periodunkid" & _
'            '        ", selfemployeeunkid = @selfemployeeunkid" & _
'            '        ", assessormasterunkid = @assessormasterunkid" & _
'            '        ", assessoremployeeunkid = @assessoremployeeunkid" & _
'            '        ", assessedemployeeunkid = @assessedemployeeunkid" & _
'            '        ", assessgroupunkid = @assessgroupunkid" & _
'            '        ", assessitemunkid = @assessitemunkid" & _
'            '        ", resultcodeunkid = @resultcodeunkid" & _
'            '        ", assessmentdate = @assessmentdate" & _
'            '        ", remark = @remark" & _
'            '        ", strength = @strength" & _
'            '        ", improvement = @improvement" & _
'            '        ", trainingneed = @trainingneed" & _
'            '        ", isselfassessed = @isselfassessed" & _
'            '        ", userunkid = @userunkid" & _
'            '        ", isvoid = @isvoid" & _
'            '        ", voiduserunkid = @voiduserunkid" & _
'            '        ", voiddatetime = @voiddatetime" & _
'            '        ", voidreason = @voidreason " & _
'            '      "WHERE analysisunkid = @analysisunkid "

'            strQ = "UPDATE hrassess_analysis_master SET " & _
'                    "  yearunkid = @yearunkid" & _
'                    ", periodunkid = @periodunkid" & _
'                    ", selfemployeeunkid = @selfemployeeunkid" & _
'                    ", assessormasterunkid = @assessormasterunkid" & _
'                    ", assessoremployeeunkid = @assessoremployeeunkid" & _
'                    ", assessedemployeeunkid = @assessedemployeeunkid" & _
'                    ", assessgroupunkid = @assessgroupunkid" & _
'                    ", assessitemunkid = @assessitemunkid" & _
'                    ", resultcodeunkid = @resultcodeunkid" & _
'                    ", assessmentdate = @assessmentdate" & _
'                    ", remark = @remark" & _
'                    ", strength = @strength" & _
'                    ", improvement = @improvement" & _
'                    ", trainingneed = @trainingneed" & _
'                    ", isselfassessed = @isselfassessed" & _
'                    ", userunkid = @userunkid" & _
'                    ", isvoid = @isvoid" & _
'                    ", voiduserunkid = @voiduserunkid" & _
'                    ", voiddatetime = @voiddatetime" & _
'                    ", voidreason = @voidreason " & _
'                    ", iscommitted = @iscommitted " & _
'                  "WHERE analysisunkid = @analysisunkid "
'            'S.SANDEEP [ 22 JULY 2011 ] -- END 

'            Call objDataOperation.ExecNonQuery(strQ)

'            If objDataOperation.ErrorMessage <> "" Then
'                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
'                Throw exForce
'            End If

'            'Pinkal (12-Oct-2011) -- Start
'            'ENHANCEMENT : AUDIT TRAIL MAINTENANCE

'            If clsCommonATLog.IsTableDataUpdate("atcommon_log", "hrassess_analysis_master", mintAnalysisunkid, "analysisunkid", 2) Then

'                If clsCommonATLog.Insert_AtLog(objDataOperation, 2, "hrassess_analysis_master", "analysisunkid", mintAnalysisunkid) = False Then
'                    exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
'                    Throw exForce
'                End If

'            End If

'            objDataOperation.ReleaseTransaction(True)
'            'Pinkal (12-Oct-2011) -- End

'            Return True
'        Catch ex As Exception
'            'Pinkal (12-Oct-2011) -- Start
'            'ENHANCEMENT : AUDIT TRAIL MAINTENANCE
'            objDataOperation.ReleaseTransaction(False)
'            'Pinkal (12-Oct-2011) -- End
'            Throw New Exception(ex.Message & "; Procedure Name: Update; Module Name: " & mstrModuleName)
'            Return False
'        Finally
'            exForce = Nothing
'            If dsList IsNot Nothing Then dsList.Dispose()
'            objDataOperation = Nothing
'        End Try
'    End Function

'    ''' <summary>
'    ''' Modify By: Sandeep J. Sharma
'    ''' </summary>
'    ''' <returns>Boolean</returns>
'    ''' <purpose> Delete Database Table (hrassess_analysis_master) </purpose>
'    Public Function Delete(ByVal intUnkid As Integer) As Boolean
'        If isUsed(intUnkid) Then
'            mstrMessage = Language.getMessage(mstrModuleName, 5, "Sorry, You cannot delete this Assessment. Reason : This Assessor has already assessed this employee.")
'            Return False
'        End If

'        Dim dsList As DataSet = Nothing
'        Dim strQ As String = ""
'        Dim exForce As Exception

'        'Pinkal (12-Oct-2011) -- Start
'        'ENHANCEMENT : AUDIT TRAIL MAINTENANCE
'        objDataOperation = New clsDataOperation
'        objDataOperation.BindTransaction()
'        'Pinkal (12-Oct-2011) -- End

'        Try
'            strQ = "UPDATE hrassess_analysis_master SET " & _
'                    "  isvoid = @isvoid" & _
'                    ", voiduserunkid = @voiduserunkid" & _
'                    ", voiddatetime = @voiddatetime" & _
'                    ", voidreason = @voidreason " & _
'                  "WHERE analysisunkid = @analysisunkid "

'            objDataOperation.AddParameter("@isvoid", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsvoid.ToString)
'            objDataOperation.AddParameter("@voiduserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintVoiduserunkid.ToString)
'            objDataOperation.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, mdtVoiddatetime)
'            objDataOperation.AddParameter("@voidreason", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrVoidreason.ToString)

'            objDataOperation.AddParameter("@analysisunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intUnkid)

'            Call objDataOperation.ExecNonQuery(strQ)

'            If objDataOperation.ErrorMessage <> "" Then
'                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
'                Throw exForce
'            End If

'            'Pinkal (12-Oct-2011) -- Start
'            'ENHANCEMENT : AUDIT TRAIL MAINTENANCE

'            If clsCommonATLog.Insert_AtLog(objDataOperation, 3, "hrassess_analysis_master", "analysisunkid", intUnkid) = False Then
'                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
'                Throw exForce
'            End If
'            objDataOperation.ReleaseTransaction(True)
'            'Pinkal (12-Oct-2011) -- End

'            Return True
'        Catch ex As Exception
'            'Pinkal (12-Oct-2011) -- Start
'            'ENHANCEMENT : AUDIT TRAIL MAINTENANCE
'            objDataOperation.ReleaseTransaction(False)
'            'Pinkal (12-Oct-2011) -- End
'            Throw New Exception(ex.Message & "; Procedure Name: Update; Module Name: " & mstrModuleName)
'            Return False
'        Finally
'            exForce = Nothing
'            If dsList IsNot Nothing Then dsList.Dispose()
'            objDataOperation = Nothing
'        End Try
'    End Function

'    ''' <summary>
'    ''' Modify By: Sandeep J. Sharma
'    ''' </summary>
'    ''' <purpose> Assign all Property variable </purpose>
'    Public Function isUsed(ByVal intUnkid As Integer) As Boolean
'        Dim dsList As DataSet = Nothing
'        Dim strQ As String = ""
'        Dim exForce As Exception

'        objDataOperation = New clsDataOperation

'        Try
'            mstrMessage = ""
'            strQ = "SELECT  assessedemployeeunkid FROM hrassess_analysis_master " & _
'                   "WHERE assessedemployeeunkid IN (SELECT selfemployeeunkid FROM hrassess_analysis_master WHERE analysisunkid = @analysisunkid) AND isVoid = 0 AND assessedemployeeunkid > 0"

'            objDataOperation.AddParameter("@analysisunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intUnkid)

'            dsList = objDataOperation.ExecQuery(strQ, "List")

'            If objDataOperation.ErrorMessage <> "" Then
'                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
'                Throw exForce
'            End If

'            Return dsList.Tables(0).Rows.Count > 0

'        Catch ex As Exception
'            Throw New Exception(ex.Message & "; Procedure Name: isUsed; Module Name: " & mstrModuleName)
'        Finally
'            exForce = Nothing
'            If dsList IsNot Nothing Then dsList.Dispose()
'            objDataOperation = Nothing
'        End Try
'    End Function

'    ''' <summary>
'    ''' Modify By: Sandeep J. Sharma
'    ''' </summary>
'    ''' <purpose> Assign all Property variable </purpose>
'    Public Function isExist(ByVal intEmpId As Integer, _
'                            ByVal intYearId As Integer, _
'                            ByVal intPeriodId As Integer, _
'                            ByVal intItemId As Integer, _
'                            Optional ByVal intAssessorId As Integer = -1, _
'                            Optional ByVal intUnkid As Integer = -1) As Boolean
'        Dim dsList As DataSet = Nothing
'        Dim strQ As String = ""
'        Dim exForce As Exception

'        objDataOperation = New clsDataOperation

'        Try
'            strQ = "SELECT " & _
'                  "  analysisunkid " & _
'                  ", yearunkid " & _
'                  ", periodunkid " & _
'                  ", selfemployeeunkid " & _
'                  ", assessormasterunkid " & _
'                  ", assessoremployeeunkid " & _
'                  ", assessedemployeeunkid " & _
'                  ", assessgroupunkid " & _
'                  ", assessitemunkid " & _
'                  ", resultcodeunkid " & _
'                  ", assessmentdate " & _
'                  ", remark " & _
'                  ", strength " & _
'                  ", improvement " & _
'                  ", trainingneed " & _
'                  ", isselfassessed " & _
'                  ", userunkid " & _
'                  ", isvoid " & _
'                  ", voiduserunkid " & _
'                  ", voiddatetime " & _
'                  ", voidreason " & _
'                 "FROM hrassess_analysis_master " & _
'                 "WHERE selfemployeeunkid = @EmpId " & _
'                 "AND yearunkid = @YearId " & _
'                 "AND periodunkid = @PeriodId " & _
'                 "AND assessitemunkid = @ItemId " & _
'                 "AND isvoid = 0 "

'            If intAssessorId > 0 Then
'                strQ &= "AND assessormasterunkid = @AssessorId "
'                objDataOperation.AddParameter("@AssessorId", SqlDbType.Int, eZeeDataType.INT_SIZE, intAssessorId)
'            End If

'            If intUnkid > 0 Then
'                strQ &= " AND analysisunkid <> @analysisunkid"
'                objDataOperation.AddParameter("@analysisunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intUnkid)
'            End If

'            objDataOperation.AddParameter("@EmpId", SqlDbType.Int, eZeeDataType.INT_SIZE, intEmpId)
'            objDataOperation.AddParameter("@YearId", SqlDbType.Int, eZeeDataType.INT_SIZE, intYearId)
'            objDataOperation.AddParameter("@PeriodId", SqlDbType.Int, eZeeDataType.INT_SIZE, intPeriodId)
'            objDataOperation.AddParameter("@ItemId", SqlDbType.Int, eZeeDataType.INT_SIZE, intItemId)

'            dsList = objDataOperation.ExecQuery(strQ, "List")

'            If objDataOperation.ErrorMessage <> "" Then
'                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
'                Throw exForce
'            End If

'            Return dsList.Tables(0).Rows.Count > 0
'        Catch ex As Exception
'            Throw New Exception(ex.Message & "; Procedure Name: isExist; Module Name: " & mstrModuleName)
'        Finally
'            exForce = Nothing
'            If dsList IsNot Nothing Then dsList.Dispose()
'            objDataOperation = Nothing
'        End Try
'    End Function


'    Public Function GetAssessorEmpId(ByVal intAssessorId As Integer) As Integer
'        Dim dsList As DataSet = Nothing
'        Dim strQ As String = ""
'        Dim exForce As Exception
'        Dim intEmpId As Integer = -1
'        objDataOperation = New clsDataOperation
'        Try

'            strQ = "SELECT employeeunkid AS EId FROM hrassessor_master WHERE assessormasterunkid = @AssessorId "

'            objDataOperation.AddParameter("@AssessorId", SqlDbType.Int, eZeeDataType.INT_SIZE, intAssessorId)

'            dsList = objDataOperation.ExecQuery(strQ, "List")

'            If objDataOperation.ErrorMessage <> "" Then
'                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
'                Throw exForce
'            End If

'            If dsList.Tables(0).Rows.Count > 0 Then
'                intEmpId = dsList.Tables(0).Rows(0)(0)
'            End If

'            Return intEmpId

'        Catch ex As Exception
'            DisplayError.Show("-1", ex.Message, "", mstrModuleName)
'        End Try
'    End Function

'    Public Function getAssessorComboList(Optional ByVal strList As String = "List", Optional ByVal blnFlag As Boolean = False) As DataSet
'        Dim dsList As DataSet = Nothing
'        Dim StrQ As String = ""
'        Dim exForce As Exception

'        objDataOperation = New clsDataOperation

'        Try
'            If blnFlag = True Then
'                StrQ = "SELECT 0 AS Id, @Select AS Name,'' AS Code UNION "
'            End If

'            'S.SANDEEP [ 29 JUNE 2011 ] -- START
'            'ISSUE : INACTIVE EMPLOYEE(S) COMING ON LIST
'            'StrQ &= "SELECT " & _
'            '        "	 assessormasterunkid AS Id " & _
'            '        "	,ISNULL(hremployee_master.firstname,'')+' '+ISNULL(hremployee_master.othername,'')+' '+ISNULL(hremployee_master.surname,'') AS Name " & _
'            '        "   ,ISNULL(hremployee_master.employeecode,'') AS Code " & _
'            '        "FROM hrassessor_master " & _
'            '        "	JOIN hrapprover_usermapping ON hrassessor_master.assessormasterunkid = hrapprover_usermapping.approverunkid " & _
'            '        "	JOIN hremployee_master ON hrassessor_master.employeeunkid = hremployee_master.employeeunkid " & _
'            '        "WHERE hrapprover_usermapping.usertypeid = " & enUserType.Assessor & " " & _
'            '        "	AND hremployee_master.isactive=1 " & _
'            '        "	AND hrapprover_usermapping.userunkid = " & User._Object._Userunkid

'            StrQ &= "SELECT " & _
'                    "	 assessormasterunkid AS Id " & _
'                    "	,ISNULL(hremployee_master.firstname,'')+' '+ISNULL(hremployee_master.othername,'')+' '+ISNULL(hremployee_master.surname,'') AS Name " & _
'                    "   ,ISNULL(hremployee_master.employeecode,'') AS Code " & _
'                    "FROM hrassessor_master " & _
'                    "	JOIN hrapprover_usermapping ON hrassessor_master.assessormasterunkid = hrapprover_usermapping.approverunkid " & _
'                    "	JOIN hremployee_master ON hrassessor_master.employeeunkid = hremployee_master.employeeunkid " & _
'                    "WHERE hrapprover_usermapping.usertypeid = " & enUserType.Assessor & " " & _
'                    "	AND hrapprover_usermapping.userunkid = " & User._Object._Userunkid

'            If ConfigParameter._Object._IsIncludeInactiveEmp = False Then
'                StrQ &= "	AND hremployee_master.isactive=1 "
'            End If

'            'S.SANDEEP [ 29 JUNE 2011 ] -- END 

'            objDataOperation.AddParameter("@Select", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 4, "Select"))

'            dsList = objDataOperation.ExecQuery(StrQ, strList)

'            If objDataOperation.ErrorMessage <> "" Then
'                exForce = New Exception(objDataOperation.ErrorNumber & " : " & objDataOperation.ErrorMessage)
'                Throw exForce
'            End If

'            Return dsList

'        Catch ex As Exception
'            DisplayError.Show("-1", ex.Message, "getAssessorComboList", mstrModuleName)
'            Return Nothing
'        End Try
'    End Function

'    Public Function getEmployeeBasedAssessor(ByVal intAssessorId As Integer, Optional ByVal strList As String = "List", Optional ByVal blnFlag As Boolean = False) As DataSet
'        Dim dsList As DataSet = Nothing
'        Dim StrQ As String = ""
'        Dim exForce As Exception

'        objDataOperation = New clsDataOperation

'        Try
'            If blnFlag = True Then
'                StrQ = "SELECT  0 AS Id,@Select AS NAME,'' AS Code UNION "
'            End If


'            'S.SANDEEP [ 29 JUNE 2011 ] -- START
'            'ISSUE : INACTIVE EMPLOYEE(S) COMING ON LIST
'            'StrQ &= "SELECT " & _
'            '        "	 employeeunkid AS Id " & _
'            '        "	,ISNULL(hremployee_master.firstname,'')+' '+ISNULL(hremployee_master.othername,'')+' '+ISNULL(hremployee_master.surname,'') AS NAME " & _
'            '        "	,ISNULL(hremployee_master.employeecode,'') AS Code " & _
'            '        "FROM hremployee_master " & _
'            '        "WHERE employeeunkid " & _
'            '        "	IN " & _
'            '        "	( " & _
'            '        "		SELECT " & _
'            '        "			hrassessor_tran.employeeunkid " & _
'            '        "		FROM hrassessor_tran " & _
'            '        "			JOIN hrassessor_master ON hrassessor_tran.assessormasterunkid = hrassessor_master.assessormasterunkid " & _
'            '        "			JOIN hrapprover_usermapping ON hrapprover_usermapping.approverunkid = hrassessor_master.assessormasterunkid " & _
'            '        "		WHERE hrassessor_master.assessormasterunkid = @AssessorId " & _
'            '        "			AND hrapprover_usermapping.userunkid = " & User._Object._Userunkid & " " & _
'            '        "           AND hrapprover_usermapping.usertypeid = " & enUserType.Assessor & " " & _
'            '        "	) AND isactive = 1 "

'            StrQ &= "SELECT " & _
'                    "	 employeeunkid AS Id " & _
'                    "	,ISNULL(hremployee_master.firstname,'')+' '+ISNULL(hremployee_master.othername,'')+' '+ISNULL(hremployee_master.surname,'') AS NAME " & _
'                    "	,ISNULL(hremployee_master.employeecode,'') AS Code " & _
'                    "FROM hremployee_master " & _
'                    "WHERE employeeunkid " & _
'                    "	IN " & _
'                    "	( " & _
'                    "		SELECT " & _
'                    "			hrassessor_tran.employeeunkid " & _
'                    "		FROM hrassessor_tran " & _
'                    "			JOIN hrassessor_master ON hrassessor_tran.assessormasterunkid = hrassessor_master.assessormasterunkid " & _
'                    "			JOIN hrapprover_usermapping ON hrapprover_usermapping.approverunkid = hrassessor_master.assessormasterunkid " & _
'                    "		WHERE hrassessor_master.assessormasterunkid = @AssessorId " & _
'                    "			AND hrapprover_usermapping.userunkid = " & User._Object._Userunkid & " " & _
'                    "           AND hrapprover_usermapping.usertypeid = " & enUserType.Assessor & " " & _
'                    "	) "
'            If ConfigParameter._Object._IsIncludeInactiveEmp = False Then
'                StrQ &= " AND isactive = 1 "
'            End If

'            'S.SANDEEP [ 29 JUNE 2011 ] -- END 

'            objDataOperation.AddParameter("@AssessorId", SqlDbType.Int, eZeeDataType.INT_SIZE, intAssessorId)
'            objDataOperation.AddParameter("@Select", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 4, "Select"))

'            dsList = objDataOperation.ExecQuery(StrQ, strList)

'            If objDataOperation.ErrorMessage <> "" Then
'                exForce = New Exception(objDataOperation.ErrorNumber & " : " & objDataOperation.ErrorMessage)
'                Throw exForce
'            End If

'            Return dsList

'        Catch ex As Exception
'            DisplayError.Show("-1", ex.Message, "getEmployeeBasedAssessor", mstrModuleName)
'            Return Nothing
'        End Try
'    End Function

'    'S.SANDEEP [ 22 JULY 2011 ] -- START
'    'ENHANCEMENT : WEB & DESKTOP COMMIT OPTION TO BE INCLUDED
'    Public Function GetUncommitedInfo(ByVal intPeriodId As Integer) As Integer
'        Dim intUCnt As Integer = -1
'        Dim exForce As Exception
'        Dim StrQ As String = String.Empty
'        Try
'            objDataOperation = New clsDataOperation
'            StrQ = "SELECT " & _
'                   "	analysisunkid " & _
'                   "FROM hrassess_analysis_master " & _
'                   "WHERE ISNULL(isvoid,0) = 0 " & _
'                   "	AND ISNULL(iscommitted,0) = 0 " & _
'                   "    AND periodunkid = @Pid "

'            objDataOperation.AddParameter("@Pid", SqlDbType.Int, eZeeDataType.INT_SIZE, intPeriodId)

'            intUCnt = objDataOperation.RecordCount(StrQ)

'            If objDataOperation.ErrorMessage <> "" Then
'                exForce = New Exception(objDataOperation.ErrorNumber & " : " & objDataOperation.ErrorMessage)
'                Throw exForce
'            End If

'            Return intUCnt

'        Catch ex As Exception
'            DisplayError.Show("-1", ex.Message, "GetUncommitedInfo", mstrModuleName)
'        Finally
'        End Try
'    End Function
'    'S.SANDEEP [ 22 JULY 2011 ] -- END 

'End Class