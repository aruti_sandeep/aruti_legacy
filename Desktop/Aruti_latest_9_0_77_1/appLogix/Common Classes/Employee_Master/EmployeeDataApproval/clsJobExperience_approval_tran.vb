﻿Imports eZeeCommonLib

Public Class clsJobExperience_approval_tran
    Private Shared ReadOnly mstrModuleName As String = "clsJobExperience_approval_tran"
    Dim objDataOperation As clsDataOperation
    Dim mstrMessage As String = ""

#Region " Private variables "
    Private mstrTranguid As String = String.Empty
    Private mdtTransactiondate As Date
    Private mintMappingunkid As Integer
    Private mstrApprovalremark As String = String.Empty
    Private mblnIsfinal As Boolean
    Private mintStatusunkid As Integer
    Private mintExperiencetranunkid As Integer
    Private mintEmployeeunkid As Integer
    Private mstrCompany As String = String.Empty
    Private mstrAddress As String = String.Empty
    Private mintJobunkid As Integer
    Private mdtStart_Date As Date
    Private mdtEnd_Date As Date
    Private mstrSupervisor As String = String.Empty
    Private mstrRemark As String = String.Empty
    Private msinLast_Pay As Decimal
    Private mblnIscontact_Previous As Boolean
    Private mstrContact_Person As String = String.Empty
    Private mstrContact_No As String = String.Empty
    Private mstrMemo As String = String.Empty
    Private mstrLeave_Reason As String = String.Empty
    Private mstrOld_Job As String = String.Empty
    Private mstrOtherbenefit As String = String.Empty
    Private mstrCurrencysign As String = String.Empty
    Private mstrPreviousBenefitPlan As String = String.Empty
    Private mblnIsvoid As Boolean
    Private mdtAuditdatetime As Date
    Private mintAudittype As Integer
    Private mintAudituserunkid As Integer
    Private mstrIp As String = String.Empty
    Private mstrHost As String = String.Empty
    Private mstrForm_Name As String = String.Empty
    Private mblnIsweb As Boolean
    Private mblnIsprocessed As Boolean
    Private mintLoginEmployeeUnkid As Integer = 0
    Private mintOperationTypeId As Integer = 0



    'Gajanan [5-Dec-2019] -- Start   
    'Enhancement:Worked On ADD Attachment In Bio Data Experience    
    Private mstrnewattachdocumnetid As String = String.Empty
    Private mstrdeleteattachdocumnetid As String = String.Empty
    'Gajanan [5-Dec-2019] -- End

#End Region

#Region " Properties "
    ''' <summary>
    ''' Purpose: Get Message from Class 
    ''' Modify By: Gajanan
    ''' </summary>
    Public ReadOnly Property _Message() As String
        Get
            Return mstrMessage
        End Get
    End Property

    ''' <summary>
    ''' Purpose: Get or Set tranguid
    ''' Modify By: Gajanan
    ''' </summary>
    Public Property _Tranguid() As String
        Get
            Return mstrTranguid
        End Get
        Set(ByVal value As String)
            mstrTranguid = Value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set transactiondate
    ''' Modify By: Gajanan
    ''' </summary>
    Public Property _Transactiondate() As Date
        Get
            Return mdtTransactiondate
        End Get
        Set(ByVal value As Date)
            mdtTransactiondate = Value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set mappingunkid
    ''' Modify By: Gajanan
    ''' </summary>
    Public Property _Mappingunkid() As Integer
        Get
            Return mintMappingunkid
        End Get
        Set(ByVal value As Integer)
            mintMappingunkid = Value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set approvalremark
    ''' Modify By: Gajanan
    ''' </summary>
    Public Property _Approvalremark() As String
        Get
            Return mstrApprovalremark
        End Get
        Set(ByVal value As String)
            mstrApprovalremark = Value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set isfinal
    ''' Modify By: Gajanan
    ''' </summary>
    Public Property _Isfinal() As Boolean
        Get
            Return mblnIsfinal
        End Get
        Set(ByVal value As Boolean)
            mblnIsfinal = Value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set statusunkid
    ''' Modify By: Gajanan
    ''' </summary>
    Public Property _Statusunkid() As Integer
        Get
            Return mintStatusunkid
        End Get
        Set(ByVal value As Integer)
            mintStatusunkid = Value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set experiencetranunkid
    ''' Modify By: Gajanan
    ''' </summary>
    Public Property _Experiencetranunkid() As Integer
        Get
            Return mintExperiencetranunkid
        End Get
        Set(ByVal value As Integer)
            mintExperiencetranunkid = Value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set employeeunkid
    ''' Modify By: Gajanan
    ''' </summary>
    Public Property _Employeeunkid() As Integer
        Get
            Return mintEmployeeunkid
        End Get
        Set(ByVal value As Integer)
            mintEmployeeunkid = Value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set company
    ''' Modify By: Gajanan
    ''' </summary>
    Public Property _Company() As String
        Get
            Return mstrCompany
        End Get
        Set(ByVal value As String)
            mstrCompany = Value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set address
    ''' Modify By: Gajanan
    ''' </summary>
    Public Property _Address() As String
        Get
            Return mstrAddress
        End Get
        Set(ByVal value As String)
            mstrAddress = Value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set jobunkid
    ''' Modify By: Gajanan
    ''' </summary>
    Public Property _Jobunkid() As Integer
        Get
            Return mintJobunkid
        End Get
        Set(ByVal value As Integer)
            mintJobunkid = Value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set start_date
    ''' Modify By: Gajanan
    ''' </summary>
    Public Property _Start_Date() As Date
        Get
            Return mdtStart_Date
        End Get
        Set(ByVal value As Date)
            mdtStart_Date = Value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set end_date
    ''' Modify By: Gajanan
    ''' </summary>
    Public Property _End_Date() As Date
        Get
            Return mdtEnd_Date
        End Get
        Set(ByVal value As Date)
            mdtEnd_Date = Value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set supervisor
    ''' Modify By: Gajanan
    ''' </summary>
    Public Property _Supervisor() As String
        Get
            Return mstrSupervisor
        End Get
        Set(ByVal value As String)
            mstrSupervisor = Value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set remark
    ''' Modify By: Gajanan
    ''' </summary>
    Public Property _Remark() As String
        Get
            Return mstrRemark
        End Get
        Set(ByVal value As String)
            mstrRemark = Value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set last_pay
    ''' Modify By: Gajanan
    ''' </summary>
    Public Property _Last_Pay() As Decimal
        Get
            Return msinLast_Pay
        End Get
        Set(ByVal value As Decimal)
            msinLast_Pay = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set iscontact_previous
    ''' Modify By: Gajanan
    ''' </summary>
    Public Property _Iscontact_Previous() As Boolean
        Get
            Return mblnIscontact_Previous
        End Get
        Set(ByVal value As Boolean)
            mblnIscontact_Previous = Value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set contact_person
    ''' Modify By: Gajanan
    ''' </summary>
    Public Property _Contact_Person() As String
        Get
            Return mstrContact_Person
        End Get
        Set(ByVal value As String)
            mstrContact_Person = Value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set contact_no
    ''' Modify By: Gajanan
    ''' </summary>
    Public Property _Contact_No() As String
        Get
            Return mstrContact_No
        End Get
        Set(ByVal value As String)
            mstrContact_No = Value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set memo
    ''' Modify By: Gajanan
    ''' </summary>
    Public Property _Memo() As String
        Get
            Return mstrMemo
        End Get
        Set(ByVal value As String)
            mstrMemo = Value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set leave_reason
    ''' Modify By: Gajanan
    ''' </summary>
    Public Property _Leave_Reason() As String
        Get
            Return mstrLeave_Reason
        End Get
        Set(ByVal value As String)
            mstrLeave_Reason = Value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set old_job
    ''' Modify By: Gajanan
    ''' </summary>
    Public Property _Old_Job() As String
        Get
            Return mstrOld_Job
        End Get
        Set(ByVal value As String)
            mstrOld_Job = Value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set otherbenefit
    ''' Modify By: Gajanan
    ''' </summary>
    Public Property _Otherbenefit() As String
        Get
            Return mstrOtherbenefit
        End Get
        Set(ByVal value As String)
            mstrOtherbenefit = Value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set currencysign
    ''' Modify By: Gajanan
    ''' </summary>
    Public Property _Currencysign() As String
        Get
            Return mstrCurrencysign
        End Get
        Set(ByVal value As String)
            mstrCurrencysign = Value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set benifitplanunkids
    ''' Modify By: Gajanan
    ''' </summary>
    Public Property _PreviousBenefitPlan() As String
        Get
            Return mstrPreviousBenefitPlan
        End Get
        Set(ByVal value As String)
            mstrPreviousBenefitPlan = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set isvoid
    ''' Modify By: Gajanan
    ''' </summary>
    Public Property _Isvoid() As Boolean
        Get
            Return mblnIsvoid
        End Get
        Set(ByVal value As Boolean)
            mblnIsvoid = Value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set auditdatetime
    ''' Modify By: Gajanan
    ''' </summary>
    Public Property _Auditdatetime() As Date
        Get
            Return mdtAuditdatetime
        End Get
        Set(ByVal value As Date)
            mdtAuditdatetime = Value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set audittype
    ''' Modify By: Gajanan
    ''' </summary>
    Public Property _Audittype() As Integer
        Get
            Return mintAudittype
        End Get
        Set(ByVal value As Integer)
            mintAudittype = Value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set audituserunkid
    ''' Modify By: Gajanan
    ''' </summary>
    Public Property _Audituserunkid() As Integer
        Get
            Return mintAudituserunkid
        End Get
        Set(ByVal value As Integer)
            mintAudituserunkid = Value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set ip
    ''' Modify By: Gajanan
    ''' </summary>
    Public Property _Ip() As String
        Get
            Return mstrIp
        End Get
        Set(ByVal value As String)
            mstrIp = Value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set host
    ''' Modify By: Gajanan
    ''' </summary>
    Public Property _Host() As String
        Get
            Return mstrHost
        End Get
        Set(ByVal value As String)
            mstrHost = Value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set form_name
    ''' Modify By: Gajanan
    ''' </summary>
    Public Property _Form_Name() As String
        Get
            Return mstrForm_Name
        End Get
        Set(ByVal value As String)
            mstrForm_Name = Value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set isweb
    ''' Modify By: Gajanan
    ''' </summary>
    Public Property _Isweb() As Boolean
        Get
            Return mblnIsweb
        End Get
        Set(ByVal value As Boolean)
            mblnIsweb = Value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set isprocessed
    ''' Modify By: Gajanan
    ''' </summary>
    Public Property _Isprocessed() As Boolean
        Get
            Return mblnIsprocessed
        End Get
        Set(ByVal value As Boolean)
            mblnIsprocessed = Value
        End Set
    End Property

    Public Property _LoginEmployeeUnkid() As Integer
        Get
            Return mintLoginEmployeeUnkid
        End Get
        Set(ByVal value As Integer)
            mintLoginEmployeeUnkid = value
        End Set
    End Property

    Public Property _OperationTypeId() As Integer
        Get
            Return mintOperationTypeId
        End Get
        Set(ByVal value As Integer)
            mintOperationTypeId = value
        End Set
    End Property



    'Gajanan [5-Dec-2019] -- Start   
    'Enhancement:Worked On ADD Attachment In Bio Data Experience    
    Public Property _Newattacheddocumnetid() As String
        Get
            Return mstrnewattachdocumnetid
        End Get
        Set(ByVal value As String)
            mstrnewattachdocumnetid = value
        End Set
    End Property

    Public Property _Deletedattachdocumnetid() As String
        Get
            Return mstrdeleteattachdocumnetid
        End Get
        Set(ByVal value As String)
            mstrdeleteattachdocumnetid = value
        End Set
    End Property
    'Gajanan [5-Dec-2019] -- End

#End Region

    Public Function GetList(ByVal xDatabaseName As String, _
                            ByVal xUserUnkid As Integer, _
                            ByVal xYearUnkid As Integer, _
                            ByVal xCompanyUnkid As Integer, _
                            ByVal xPeriodStart As DateTime, _
                            ByVal xPeriodEnd As DateTime, _
                            ByVal xUserModeSetting As String, _
                            ByVal xOnlyApproved As Boolean, _
                            ByVal xIncludeIn_ActiveEmployee As Boolean, _
                            ByVal strTableName As String, _
                            Optional ByVal blnApplyUserAccessFilter As Boolean = True, _
                            Optional ByVal strFilerString As String = "", _
                            Optional ByVal IsUsedAsMSS As Boolean = True, _
                            Optional ByVal mblnAddApprovalCondition As Boolean = True) As DataSet

        Dim dsList As DataSet = Nothing
        Dim StrQ As String = ""
        Dim exForce As Exception
        Try
            Dim xDateJoinQry, xDateFilterQry, xUACQry, xUACFiltrQry, xAdvanceJoinQry As String
            xDateJoinQry = "" : xDateFilterQry = "" : xUACQry = "" : xUACFiltrQry = "" : xAdvanceJoinQry = ""
            Call GetDatesFilterString(xDateJoinQry, xDateFilterQry, xPeriodStart, xPeriodEnd, , , xDatabaseName)

            If IsUsedAsMSS Then Call NewAccessLevelFilterString(xUACQry, xUACFiltrQry, xPeriodEnd, xOnlyApproved, xDatabaseName, xUserUnkid, xCompanyUnkid, xYearUnkid, xUserModeSetting, , mblnAddApprovalCondition)

            Call GetAdvanceFilterQry(xAdvanceJoinQry, xPeriodEnd, xDatabaseName)

            Using objDo As New clsDataOperation
                StrQ = "SELECT " & _
                       "     ISNULL(hremployee_master.firstname,'')+' '+ISNULL(hremployee_master.othername,'')+' '+ISNULL(hremployee_master.surname,'') AS EmpName " & _
                       "    ,ISNULL(hremp_experience_approval_tran.company,'') AS Company " & _
                       "    ,CONVERT(CHAR(8),hremp_experience_approval_tran.start_date,112) AS StartDate " & _
                       "    ,CONVERT(CHAR(8),hremp_experience_approval_tran.end_date,112) AS EndDate " & _
                       "    ,ISNULL(hremp_experience_approval_tran.supervisor,'') AS Supervisor " & _
                       "    ,ISNULL(hremp_experience_approval_tran.remark,'') AS Remark " & _
                       "    ,hremployee_master.employeeunkid AS EmpId " & _
                       "    ,hremp_experience_approval_tran.experiencetranunkid AS ExpId " & _
                       "    ,old_job AS Jobs " & _
                       "    ,otherbenefit AS OtherBenefit " & _
                       "    ,currencysign AS CurrencySign " & _
                       "    ,ISNULL(hremployee_master.sectiongroupunkid,0) AS sectiongroupunkid " & _
                       "    ,ISNULL(hremployee_master.unitgroupunkid,0) AS unitgroupunkid " & _
                       "    ,ISNULL(hremployee_master.teamunkid,0) AS teamunkid " & _
                       "    ,ISNULL(hremployee_master.stationunkid,0) AS stationunkid " & _
                       "    ,ISNULL(hremployee_master.deptgroupunkid,0) AS deptgroupunkid " & _
                       "    ,ISNULL(hremployee_master.departmentunkid,0) AS departmentunkid " & _
                       "    ,ISNULL(hremployee_master.sectionunkid,0) AS sectionunkid " & _
                       "    ,ISNULL(hremployee_master.unitunkid,0) AS unitunkid " & _
                       "    ,ISNULL(hremployee_master.jobunkid,0) AS jobunkid " & _
                       "    ,ISNULL(hremployee_master.classgroupunkid,0) AS classgroupunkid " & _
                       "    ,ISNULL(hremployee_master.classunkid,0) AS classunkid " & _
                       "    ,ISNULL(hremployee_master.jobgroupunkid,0) AS jobgroupunkid " & _
                       "    ,ISNULL(hremployee_master.gradegroupunkid,0) AS gradegroupunkid " & _
                       "    ,ISNULL(hremployee_master.gradeunkid,0) AS gradeunkid " & _
                       "    ,ISNULL(hremployee_master.gradelevelunkid,0) AS gradelevelunkid " & _
                       "    ,hremp_experience_approval_tran.tranguid As tranguid " & _
                       "    ,ISNULL(hremp_experience_approval_tran.operationtypeid,0) AS operationtypeid " & _
                       "    ,CASE WHEN ISNULL(hremp_experience_approval_tran.operationtypeid,0) = '" & CInt(clsEmployeeDataApproval.enOperationType.ADDED) & "' THEN @ADDED " & _
                       "          WHEN ISNULL(hremp_experience_approval_tran.operationtypeid,0) = '" & CInt(clsEmployeeDataApproval.enOperationType.EDITED) & "' THEN @EDITED " & _
                       "          WHEN ISNULL(hremp_experience_approval_tran.operationtypeid,0) = '" & CInt(clsEmployeeDataApproval.enOperationType.DELETED) & "' THEN @DELETED " & _
                       "     END AS OperationType " & _
                       "FROM hremp_experience_approval_tran " & _
                       "LEFT JOIN hremployee_master ON hremp_experience_approval_tran.employeeunkid = hremployee_master.employeeunkid "

                If xDateJoinQry.Trim.Length > 0 Then
                    StrQ &= xDateJoinQry
                End If

                
                If blnApplyUserAccessFilter = True Then
                    If xUACQry.Trim.Length > 0 Then
                        'Gajanan [27-May-2019] -- Start              
                        'StrQ &= xUACQry
                        StrQ &= "LEFT " & xUACQry
                        'Gajanan [27-Ma y-2019] -- End
                    End If
                End If
               
                If xAdvanceJoinQry.Trim.Length > 0 Then
                    StrQ &= xAdvanceJoinQry
                End If

                StrQ &= "WHERE ISNULL(hremp_experience_approval_tran.isvoid,0) = 0 AND ISNULL(hremp_experience_approval_tran.isprocessed,0) = 0 "

                If blnApplyUserAccessFilter = True Then
                    If xUACFiltrQry.Trim.Length > 0 Then
                        StrQ &= " AND " & xUACFiltrQry
                    End If
                End If

                If xIncludeIn_ActiveEmployee = False Then
                    If xDateFilterQry.Trim.Length > 0 Then
                        StrQ &= xDateFilterQry
                    End If
                End If

                If strFilerString.Trim.Length > 0 Then
                    StrQ &= "AND " & strFilerString
                End If

                StrQ &= " ORDER BY CONVERT(CHAR(8),hremp_experience_approval_tran.start_date,112)  DESC "


                objDo.AddParameter("@ADDED", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage("clsEmployeeDataApproval", 62, "Newly Added Information"))
                objDo.AddParameter("@EDITED", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage("clsEmployeeDataApproval", 63, "Information Edited"))
                objDo.AddParameter("@DELETED", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage("clsEmployeeDataApproval", 64, "Information Deleted"))

                dsList = objDo.ExecQuery(StrQ, strTableName)

                If objDo.ErrorMessage <> "" Then
                    exForce = New Exception(objDo.ErrorNumber & ": " & objDo.ErrorMessage)
                    Throw exForce
                End If

            End Using
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: GetList; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
        End Try
        Return dsList
    End Function

    Public Function Insert(ByVal intCompanyId As Integer, _
                           Optional ByVal xDataOpr As clsDataOperation = Nothing, _
                           Optional ByVal blnFromApproval As Boolean = False, _
                           Optional ByVal mdtTable As DataTable = Nothing) As Boolean
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        If xDataOpr IsNot Nothing Then
            objDataOperation = xDataOpr
        Else
            objDataOperation = New clsDataOperation
            objDataOperation.BindTransaction()
        End If
        objDataOperation.ClearParameters()

        If blnFromApproval = False Then


            'Gajanan [17-April-2019] -- Start
            'Enhancement - Implementing Employee Approver Flow On Employee Data.

            'If isExist(mintEmployeeunkid, mstrCompany, mdtEnd_Date.Date, mdtStart_Date.Date, mstrOld_Job, mintExperiencetranunkid, "", objDataOperation, True) Then
            '    mstrMessage = Language.getMessage(mstrModuleName, 1, "Particular Job History is already entered for selected employee. Please enter new information.")
            '    Return False
            'End If


            If isExist(mintEmployeeunkid, mstrCompany, mdtStart_Date.Date, mdtEnd_Date.Date, mstrOld_Job, mintExperiencetranunkid, "", objDataOperation, True) Then
                mstrMessage = Language.getMessage(mstrModuleName, 1, "Particular Job History is already entered for selected employee. Please enter new information.")
                Return False
            End If
            'Gajanan [17-April-2019] -- End

        End If

        Try


            'Gajanan [5-Dec-2019] -- Start   
            'Enhancement:Worked On ADD Attachment In Bio Data Experience    
            Dim objDocument As New clsScan_Attach_Documents
            Dim dtTran As DataTable = objDocument._Datatable
            Dim docLocalPath As String = ConfigParameter._Object._Document_Path & "\"

            Dim strFolderName As String = (New clsScan_Attach_Documents).GetDocFolderName("Docs", enScanAttactRefId.JOBHISTORYS).Tables(0).Rows(0)("Name").ToString()
            If strFolderName IsNot Nothing Then strFolderName = strFolderName & "\"

            Dim dr As DataRow

            If IsNothing(mdtTable) = False Then
                For Each drow As DataRow In mdtTable.Rows
                    dr = dtTran.NewRow
                    dr("scanattachtranunkid") = drow("scanattachtranunkid")
                    dr("documentunkid") = drow("documentunkid")
                    dr("employeeunkid") = drow("employeeunkid")
                    dr("filename") = drow("filename")
                    dr("scanattachrefid") = drow("scanattachrefid")
                    dr("modulerefid") = drow("modulerefid")
                    dr("form_name") = drow("form_name")
                    dr("userunkid") = drow("userunkid")
                    dr("transactionunkid") = drow("transactionunkid")
                    dr("userunkid") = User._Object._Userunkid
                    dr("attached_date") = drow("attached_date")
                    dr("orgfilepath") = drow("localpath")
                    dr("destfilepath") = docLocalPath & strFolderName & CStr(drow.Item("filename"))
                    dr("AUD") = drow("AUD")
                    dr("fileuniquename") = drow("fileuniquename")
                    dr("filepath") = drow("filepath")
                    dr("filesize") = drow("filesize_kb")

                    dtTran.Rows.Add(dr)

                Next
                objDocument._Datatable = dtTran
                objDocument.InsertUpdateDelete_Documents(objDataOperation)
            End If
            'Gajanan [5-Dec-2019] -- End




            objDataOperation.ClearParameters()
            objDataOperation.AddParameter("@tranguid", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrTranguid.ToString)
            If mdtTransactiondate = Nothing Then
                objDataOperation.AddParameter("@transactiondate", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, DBNull.Value)
            Else
                objDataOperation.AddParameter("@transactiondate", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, mdtTransactiondate)
            End If
            objDataOperation.AddParameter("@mappingunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintMappingunkid.ToString)
            objDataOperation.AddParameter("@approvalremark", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrApprovalremark.ToString)
            objDataOperation.AddParameter("@isfinal", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsfinal.ToString)
            objDataOperation.AddParameter("@statusunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintStatusunkid.ToString)
            objDataOperation.AddParameter("@experiencetranunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintExperiencetranunkid.ToString)
            objDataOperation.AddParameter("@employeeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintEmployeeunkid.ToString)
            objDataOperation.AddParameter("@company", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrCompany.ToString)
            objDataOperation.AddParameter("@address", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrAddress.ToString)
            objDataOperation.AddParameter("@jobunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintJobunkid.ToString)
            If mdtStart_Date = Nothing Then
                objDataOperation.AddParameter("@start_date", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, DBNull.Value)
            Else
                objDataOperation.AddParameter("@start_date", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, mdtStart_Date)
            End If

            If mdtEnd_Date = Nothing Then
                objDataOperation.AddParameter("@end_date", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, DBNull.Value)
            Else
                objDataOperation.AddParameter("@end_date", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, mdtEnd_Date)
            End If
            objDataOperation.AddParameter("@supervisor", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrSupervisor.ToString)
            objDataOperation.AddParameter("@remark", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrRemark.ToString)
            objDataOperation.AddParameter("@last_pay", SqlDbType.Decimal, eZeeDataType.DECIMAL_SIZE, msinLast_Pay.ToString)
            objDataOperation.AddParameter("@iscontact_previous", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIscontact_Previous.ToString)
            objDataOperation.AddParameter("@contact_person", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrContact_Person.ToString)
            objDataOperation.AddParameter("@contact_no", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrContact_No.ToString)
            objDataOperation.AddParameter("@memo", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrMemo.ToString)
            objDataOperation.AddParameter("@leave_reason", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrLeave_Reason.ToString)
            objDataOperation.AddParameter("@old_job", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrOld_Job.ToString)
            objDataOperation.AddParameter("@otherbenefit", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrOtherbenefit.ToString)
            objDataOperation.AddParameter("@currencysign", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrCurrencysign.ToString)
            objDataOperation.AddParameter("@benifitplanunkids", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrPreviousBenefitPlan.ToString)
            objDataOperation.AddParameter("@isvoid", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsvoid.ToString)
            objDataOperation.AddParameter("@audittype", SqlDbType.Int, eZeeDataType.INT_SIZE, mintAudittype.ToString)
            objDataOperation.AddParameter("@audituserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintAudituserunkid.ToString)
            objDataOperation.AddParameter("@ip", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrIp.ToString)
            objDataOperation.AddParameter("@host", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrHost.ToString)
            objDataOperation.AddParameter("@form_name", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrForm_Name.ToString)
            objDataOperation.AddParameter("@isweb", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsweb.ToString)
            objDataOperation.AddParameter("@isprocessed", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsprocessed.ToString)
            objDataOperation.AddParameter("@loginemployeeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintLoginEmployeeUnkid)
            objDataOperation.AddParameter("@operationtypeid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintOperationTypeId)

            'Gajanan [5-Dec-2019] -- Start   
            'Enhancement:Worked On ADD Attachment In Bio Data Experience    
            If objDocument._Newattachdocumentids.Length > 0 Then
                objDataOperation.AddParameter("@newattachdocumentid", SqlDbType.NVarChar, objDocument._Newattachdocumentids.Length, objDocument._Newattachdocumentids)
            Else
                objDataOperation.AddParameter("@newattachdocumentid", SqlDbType.NVarChar, mstrnewattachdocumnetid.Length, mstrnewattachdocumnetid)
            End If

            If objDocument._Deleteattachdocumentids.Length > 0 Then
                objDataOperation.AddParameter("@deleteattachdocumentid", SqlDbType.NVarChar, objDocument._Deleteattachdocumentids.Length, objDocument._Deleteattachdocumentids)
            Else
                objDataOperation.AddParameter("@deleteattachdocumentid", SqlDbType.NVarChar, mstrdeleteattachdocumnetid.Length, mstrdeleteattachdocumnetid)
            End If
            'Gajanan [5-Dec-2019] -- End

            strQ = "INSERT INTO hremp_experience_approval_tran ( " & _
              "  tranguid " & _
              ", transactiondate " & _
              ", mappingunkid " & _
              ", approvalremark " & _
              ", isfinal " & _
              ", statusunkid " & _
              ", experiencetranunkid " & _
              ", employeeunkid " & _
              ", company " & _
              ", address " & _
              ", jobunkid " & _
              ", start_date " & _
              ", end_date " & _
              ", supervisor " & _
              ", remark " & _
              ", last_pay " & _
              ", iscontact_previous " & _
              ", contact_person " & _
              ", contact_no " & _
              ", memo " & _
              ", leave_reason " & _
              ", old_job " & _
              ", otherbenefit " & _
              ", currencysign " & _
              ", benifitplanunkids " & _
              ", isvoid " & _
              ", auditdatetime " & _
              ", audittype " & _
              ", audituserunkid " & _
              ", ip " & _
              ", host " & _
              ", form_name " & _
              ", isweb " & _
              ", isprocessed" & _
              ", loginemployeeunkid " & _
              ", operationtypeid " & _
              ", newattachdocumentid " & _
              ", deleteattachdocumentid " & _
            ") VALUES (" & _
              "  @tranguid " & _
              ", @transactiondate " & _
              ", @mappingunkid " & _
              ", @approvalremark " & _
              ", @isfinal " & _
              ", @statusunkid " & _
              ", @experiencetranunkid " & _
              ", @employeeunkid " & _
              ", @company " & _
              ", @address " & _
              ", @jobunkid " & _
              ", @start_date " & _
              ", @end_date " & _
              ", @supervisor " & _
              ", @remark " & _
              ", @last_pay " & _
              ", @iscontact_previous " & _
              ", @contact_person " & _
              ", @contact_no " & _
              ", @memo " & _
              ", @leave_reason " & _
              ", @old_job " & _
              ", @otherbenefit " & _
              ", @currencysign " & _
              ", @benifitplanunkids " & _
              ", @isvoid " & _
              ", GETDATE() " & _
              ", @audittype " & _
              ", @audituserunkid " & _
              ", @ip " & _
              ", @host " & _
              ", @form_name " & _
              ", @isweb " & _
              ", @isprocessed" & _
              ", @loginemployeeunkid " & _
              ", @operationtypeid " & _
              ", @newattachdocumentid " & _
              ", @deleteattachdocumentid " & _
              "); SELECT @@identity"

            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            If xDataOpr Is Nothing Then
                objDataOperation.ReleaseTransaction(True)
            End If
            Return True
        Catch ex As Exception
            If xDataOpr Is Nothing Then
                objDataOperation.ReleaseTransaction(False)
            End If
            Throw New Exception(ex.Message & "; Procedure Name: Insert; Module Name: " & mstrModuleName)
            Return False
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            If xDataOpr Is Nothing Then objDataOperation = Nothing
        End Try
    End Function

    Public Function Delete(ByVal intUnkid As Integer, ByVal xVoidReason As String, ByVal intCompanyId As Integer, Optional ByVal xDataOpr As clsDataOperation = Nothing) As Boolean
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception
        Dim blnFlag As Boolean = False

        If xDataOpr IsNot Nothing Then
            objDataOperation = xDataOpr
        Else
            objDataOperation = New clsDataOperation
            objDataOperation.BindTransaction()
        End If
        objDataOperation.ClearParameters()

        Try
            Dim objJobExperience_tran As clsJobExperience_tran
            objJobExperience_tran = New clsJobExperience_tran
            objJobExperience_tran._Experiencetranunkid() = intUnkid

            Me._Address = objJobExperience_tran._Address
            Me._Company = objJobExperience_tran._Company
            Me._Contact_No = objJobExperience_tran._Contact_No
            Me._Contact_Person = objJobExperience_tran._Contact_Person
            Me._Employeeunkid = objJobExperience_tran._Employeeunkid
            Me._End_Date = objJobExperience_tran._End_Date
            Me._Iscontact_Previous = objJobExperience_tran._Iscontact_Previous
            Me._Old_Job = objJobExperience_tran._Job
            Me._Otherbenefit = objJobExperience_tran._OtherBenefit
            Me._Currencysign = objJobExperience_tran._CurrencySign
            Me._Last_Pay = objJobExperience_tran._Last_Pay
            Me._Leave_Reason = objJobExperience_tran._Leave_Reason
            Me._Memo = objJobExperience_tran._Memo
            Me._Remark = objJobExperience_tran._Remark
            Me._Start_Date = objJobExperience_tran._Start_Date
            Me._Supervisor = objJobExperience_tran._Supervisor

            Me._Audittype = enAuditType.ADD
            Me._Experiencetranunkid = intUnkid
            Me._Isprocessed = False
            Me._Isvoid = False
            Me._Tranguid = Guid.NewGuid.ToString()
            Me._Transactiondate = Now
            Me._Approvalremark = ""
            Me._Mappingunkid = 0
            Me._Isfinal = False
            Me._OperationTypeId = clsEmployeeDataApproval.enOperationType.DELETED
            Me._Statusunkid = clsEmployee_Master.EmpApprovalStatus.SubmitForApproval

            blnFlag = Insert(intCompanyId, objDataOperation)

            If blnFlag = True AndAlso mstrMessage.Trim.Length <= 0 Then

                objJobExperience_tran._VoidLoginEmployeeunkid = -1
                objJobExperience_tran._Voidreason = xVoidReason
                objJobExperience_tran._Voiduserunkid = Me._Audituserunkid
                objJobExperience_tran._Voidatetime = Now
                objJobExperience_tran._Isvoid = True


                'Gajanan [9-July-2019] -- Start      
                'ISSUE/ENHANCEMENT : AT Changes For Display Missing Column


                'objJobExperience_tran._WebClientIP = Me._Ip
                'objJobExperience_tran._WebFormName = Me._Form_Name
                'objJobExperience_tran._WebHostName = Me._Host

                objJobExperience_tran._ClientIP = Me._Ip
                objJobExperience_tran._FormName = Me._Form_Name
                objJobExperience_tran._HostName = Me._Host
                objJobExperience_tran._AuditUserId = Me._Audituserunkid
                'Gajanan [9-July-2019] -- End

                If objJobExperience_tran.Delete(intUnkid, objDataOperation, False) = False Then
                    If objDataOperation.ErrorMessage <> "" Then
                        exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                        Throw exForce
                    End If
                End If
            End If

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            If xDataOpr Is Nothing Then
                objDataOperation.ReleaseTransaction(True)
            End If
            Return True
        Catch ex As Exception
            If xDataOpr Is Nothing Then objDataOperation.ReleaseTransaction(False)
            Throw New Exception(ex.Message & "; Procedure Name: Update; Module Name: " & mstrModuleName)
            Return False
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            If xDataOpr Is Nothing Then objDataOperation = Nothing
        End Try
    End Function

    Public Function isExist(ByVal intEmpId As Integer, _
                            ByVal strCompanyName As String, _
                            ByVal dtDate As DateTime, _
                            ByVal dtEDate As DateTime, _
                            ByVal StrJob As String, _
                            Optional ByVal intUnkid As Integer = -1, _
                            Optional ByVal strtrangUid As String = "", Optional ByVal xDataOpr As clsDataOperation = Nothing, _
                            Optional ByVal blnPreventNewEntry As Boolean = True, _
                            Optional ByRef intOperationTypeId As Integer = 0) As Boolean


        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        'Gajanan [17-DEC-2018] -- Start
        'Enhancement - Implementing Employee Approver Flow On Employee Data.
        If xDataOpr IsNot Nothing Then
            objDataOperation = xDataOpr
            objDataOperation.ClearParameters()
        Else
            objDataOperation = New clsDataOperation
        End If
        objDataOperation.ClearParameters()

        'Gajanan [17-DEC-2018] -- End

        Try
            strQ = "SELECT " & _
                    " tranguid " & _
                    ", transactiondate " & _
                    ", mappingunkid " & _
                    ", approvalremark " & _
                    ", isfinal " & _
                    ", statusunkid " & _
                    ", experiencetranunkid " & _
                    ", employeeunkid " & _
                    ", Company " & _
                    ", address " & _
                    ", jobunkid " & _
                    ", start_date " & _
                    ", end_date " & _
                    ", supervisor " & _
                    ", remark " & _
                    ", last_pay " & _
                    ", iscontact_previous " & _
                    ", contact_person " & _
                    ", contact_no " & _
                    ", memo " & _
                    ", leave_reason " & _
                    ", old_job " & _
                    ", otherbenefit " & _
                    ", currencysign " & _
                    ", benifitplanunkids " & _
                    ", isvoid " & _
                    ", isprocessed " & _
                    ", operationtypeid " & _
                    "FROM hremp_experience_approval_tran WHERE isvoid = 0 " & _
                    "and employeeunkid = @EmpId " & _
                    "AND company = @CompName " & _
                    "AND old_job = @Job "



            'Gajanan [17-April-2019] -- Start
            'Enhancement - Implementing Employee Approver Flow On Employee Data.

            'If dtEDate <> Nothing Then
            '    strQ &= " AND CONVERT(CHAR(8),start_date,112) = @EDate "
            'End If

            If dtDate <> Nothing Then
                strQ &= " AND CONVERT(CHAR(8),start_date,112) = @Date "
            End If

            'Gajanan [17-April-2019] -- End

            If strtrangUid.Trim.Length > 0 Then
                strQ &= " AND tranguid = @tranguid"
            End If


            'Gajanan [10-June-2019] -- Start      


            'If blnPreventNewEntry Then
            '    strQ &= " AND ISNULL(hremp_experience_approval_tran.isprocessed,0)= 0 "
            'End If
                strQ &= " AND ISNULL(hremp_experience_approval_tran.isprocessed,0)= 0 "
            'Gajanan [10-June-2019] -- End


            objDataOperation.AddParameter("@EmpId", SqlDbType.Int, eZeeDataType.INT_SIZE, intEmpId)
            objDataOperation.AddParameter("@CompName", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, strCompanyName)
            objDataOperation.AddParameter("@experiencetranunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intUnkid)
            objDataOperation.AddParameter("@tranguid", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, strtrangUid)

            objDataOperation.AddParameter("@Job", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, StrJob)
            If dtDate <> Nothing Then
                objDataOperation.AddParameter("@Date", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, eZeeDate.convertDate(dtDate))
            End If

            If dtEDate <> Nothing Then
                objDataOperation.AddParameter("@EDate", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, eZeeDate.convertDate(dtEDate))
            End If

            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            If dsList.Tables(0).Rows.Count > 0 Then
                intOperationTypeId = CInt(dsList.Tables(0).Rows(0)("operationtypeid"))
            End If

            Return dsList.Tables(0).Rows.Count > 0
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: isExist; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            If xDataOpr Is Nothing Then objDataOperation = Nothing
        End Try
    End Function

    Public Function Update(ByVal xDataOpr As clsDataOperation, ByVal intEmployeeId As Integer, ByVal strCompanyName As String, _
                            ByVal dtDate As DateTime, ByVal dtEDate As DateTime, ByVal StrJob As String, ByVal intExperienceUnkid As Integer, _
                            ByVal strColName As String) As Boolean
        Dim strQ As String = ""
        Dim exForce As Exception


        If xDataOpr IsNot Nothing Then
            objDataOperation = xDataOpr
        Else
            objDataOperation = New clsDataOperation
            objDataOperation.BindTransaction()
        End If
        objDataOperation.ClearParameters()

        Try
            strQ = "UPDATE hremp_experience_approval_tran SET " & _
                   " " & strColName & " = '" & intExperienceUnkid & "' " & _
                   " WHERE isprocessed= 0 and company= @company AND employeeunkid = @employeeunkid " & _
                   " AND old_job = '" & StrJob & "' "

            If dtDate <> Nothing Then
                strQ &= " AND CONVERT(CHAR(8),start_date,112) = '" & eZeeDate.convertDate(dtDate).ToString & "' "
            End If

            If dtEDate <> Nothing Then
                strQ &= " AND CONVERT(CHAR(8),end_date,112) = '" & eZeeDate.convertDate(dtEDate).ToString & "' "
            End If



            'Gajanan [31-May-2020] -- Start
            objDataOperation.AddParameter("@employeeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intEmployeeId)
            objDataOperation.AddParameter("@company", SqlDbType.NVarChar, eZeeDataType.DESC_SIZE, strCompanyName)
            'Gajanan [31-May-2020] -- End

            Call xDataOpr.ExecNonQuery(strQ)

            If xDataOpr.ErrorMessage <> "" Then
                exForce = New Exception(xDataOpr.ErrorNumber & ": " & xDataOpr.ErrorMessage)
                Throw exForce
            End If

            If xDataOpr Is Nothing Then
                objDataOperation.ReleaseTransaction(True)
            End If
            Return True
        Catch ex As Exception
            If xDataOpr Is Nothing Then
                objDataOperation.ReleaseTransaction(False)
            End If
            Throw New Exception(ex.Message & "; Procedure Name: ; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If xDataOpr Is Nothing Then objDataOperation = Nothing

        End Try
    End Function


    'Gajanan [5-Dec-2019] -- Start   
    'Enhancement:Worked On ADD Attachment In Bio Data Experience    
    Public Function UpdateDeleteDocument(ByVal xDataOpr As clsDataOperation, ByVal intEmployeeId As Integer, ByVal intlinkedUnkid As String, _
                                         ByVal scanattachrefid As Integer, ByVal dtTransectionDate As DateTime, _
                                         ByVal intQualificationTranUnkid As Integer, ByVal strColName As String, _
                                         ByVal blnisDelete As Boolean) As Boolean
        Dim strQ As String = ""
        Dim exForce As Exception

        If xDataOpr IsNot Nothing Then
            objDataOperation = xDataOpr
        Else
            objDataOperation = New clsDataOperation
            objDataOperation.BindTransaction()
        End If
        objDataOperation.ClearParameters()

        Try
            If intlinkedUnkid.Length > 0 Then
                strQ = "UPDATE hrdocuments_tran SET " & _
                   " " & strColName & " = '" & intQualificationTranUnkid & "'"

                If blnisDelete Then
                    strQ &= ", isactive = 0 "
                Else
                    strQ &= ", isactive = 1 "
                End If
                strQ &= "WHERE employeeunkid = '" & intEmployeeId & "' AND scanattachtranunkid in (" & intlinkedUnkid & " )"

                Call objDataOperation.ExecNonQuery(strQ)

                If objDataOperation.ErrorMessage <> "" Then
                    exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                    Throw exForce
                End If
            End If
            If xDataOpr Is Nothing Then
                objDataOperation.ReleaseTransaction(True)
            End If
            Return True
        Catch ex As Exception
            If xDataOpr Is Nothing Then
                objDataOperation.ReleaseTransaction(False)
            End If
            Throw New Exception(ex.Message & "; Procedure Name: ; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If xDataOpr Is Nothing Then objDataOperation = Nothing
        End Try
    End Function
    'Gajanan [5-Dec-2019] -- End

End Class
