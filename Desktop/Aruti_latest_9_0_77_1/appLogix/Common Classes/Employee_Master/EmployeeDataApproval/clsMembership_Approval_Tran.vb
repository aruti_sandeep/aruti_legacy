﻿'************************************************************************************************************************************
'Class Name :clsMembership_Approval_Tran.vb
'Purpose    :
'Date       :16-Apr-2019
'Written By :Sandeep Sharma
'Modified   :
'************************************************************************************************************************************
Imports eZeeCommonLib
''' <summary>
''' Purpose: 
''' Developer: Sandeep Sharma
''' </summary>
Public Class clsMembership_Approval_Tran
    Private Const mstrModuleName = "clsMembership_Approval_Tran"
    Dim objDataOperation As clsDataOperation
    Dim mstrMessage As String = ""

#Region " Private variables "

    Private mintEmployeeunkid As Integer = 0
    Private mdtTran As DataTable = Nothing

#End Region

#Region " Properties "

    Public Property _Employeeunkid() As Integer
        Get
            Return mintEmployeeunkid
        End Get
        Set(ByVal value As Integer)
            mintEmployeeunkid = value
            GetMembership_tran()
        End Set
    End Property

    Public Property _xObjDataOperation() As clsDataOperation
        Get
            Return objDataOperation
        End Get
        Set(ByVal value As clsDataOperation)
            objDataOperation = value
        End Set
    End Property

    Public Property _DataTable() As DataTable
        Get
            Return mdtTran
        End Get
        Set(ByVal value As DataTable)
            mdtTran = value
        End Set
    End Property

#End Region

#Region " Contructor "

    Public Sub New()
        mdtTran = New DataTable("memtran")
        Dim dCol As DataColumn
        Try
            dCol = New DataColumn
            With dCol
                .ColumnName = "tranguid"
                .DataType = GetType(System.String)
                .DefaultValue = ""
            End With
            mdtTran.Columns.Add(dCol)

            dCol = New DataColumn
            With dCol
                .ColumnName = "transactiondate"
                .DataType = GetType(System.DateTime)
                .DefaultValue = DBNull.Value
            End With
            mdtTran.Columns.Add(dCol)

            dCol = New DataColumn
            With dCol
                .ColumnName = "mappingunkid"
                .DataType = GetType(System.Int32)
                .DefaultValue = 0
            End With
            mdtTran.Columns.Add(dCol)

            dCol = New DataColumn
            With dCol
                .ColumnName = "approvalremark"
                .DataType = GetType(System.String)
                .DefaultValue = ""
            End With
            mdtTran.Columns.Add(dCol)

            dCol = New DataColumn
            With dCol
                .ColumnName = "isfinal"
                .DataType = GetType(System.Boolean)
                .DefaultValue = False
            End With
            mdtTran.Columns.Add(dCol)

            dCol = New DataColumn
            With dCol
                .ColumnName = "statusunkid"
                .DataType = GetType(System.Int32)
                .DefaultValue = 0
            End With
            mdtTran.Columns.Add(dCol)

            dCol = New DataColumn
            With dCol
                .ColumnName = "membershiptranunkid"
                .DataType = GetType(System.Int32)
                .DefaultValue = 0
            End With
            mdtTran.Columns.Add(dCol)

            dCol = New DataColumn
            With dCol
                .ColumnName = "employeeunkid"
                .DataType = GetType(System.Int32)
                .DefaultValue = 0
            End With
            mdtTran.Columns.Add(dCol)

            dCol = New DataColumn
            With dCol
                .ColumnName = "membership_categoryunkid"
                .DataType = GetType(System.Int32)
                .DefaultValue = 0
            End With
            mdtTran.Columns.Add(dCol)

            dCol = New DataColumn
            With dCol
                .ColumnName = "membershipunkid"
                .DataType = GetType(System.Int32)
                .DefaultValue = 0
            End With
            mdtTran.Columns.Add(dCol)

            dCol = New DataColumn
            With dCol
                .ColumnName = "membershipno"
                .DataType = GetType(System.String)
                .DefaultValue = ""
            End With
            mdtTran.Columns.Add(dCol)

            dCol = New DataColumn
            With dCol
                .ColumnName = "issue_date"
                .DataType = GetType(System.DateTime)
                .DefaultValue = DBNull.Value
            End With
            mdtTran.Columns.Add(dCol)

            dCol = New DataColumn
            With dCol
                .ColumnName = "start_date"
                .DataType = GetType(System.DateTime)
                .DefaultValue = DBNull.Value
            End With
            mdtTran.Columns.Add(dCol)

            dCol = New DataColumn
            With dCol
                .ColumnName = "expiry_date"
                .DataType = GetType(System.DateTime)
                .DefaultValue = DBNull.Value
            End With
            mdtTran.Columns.Add(dCol)

            dCol = New DataColumn
            With dCol
                .ColumnName = "remark"
                .DataType = GetType(System.String)
                .DefaultValue = ""
            End With
            mdtTran.Columns.Add(dCol)

            dCol = New DataColumn
            With dCol
                .ColumnName = "isactive"
                .DataType = GetType(System.Boolean)
                .DefaultValue = False
            End With
            mdtTran.Columns.Add(dCol)

            dCol = New DataColumn
            With dCol
                .ColumnName = "isdeleted"
                .DataType = GetType(System.Boolean)
                .DefaultValue = False
            End With
            mdtTran.Columns.Add(dCol)

            dCol = New DataColumn
            With dCol
                .ColumnName = "rehiretranunkid"
                .DataType = GetType(System.Int32)
                .DefaultValue = 0
            End With
            mdtTran.Columns.Add(dCol)

            dCol = New DataColumn
            With dCol
                .ColumnName = "effetiveperiodid"
                .DataType = GetType(System.Int32)
                .DefaultValue = 0
            End With
            mdtTran.Columns.Add(dCol)

            dCol = New DataColumn
            With dCol
                .ColumnName = "copyedslab"
                .DataType = GetType(System.Boolean)
                .DefaultValue = False
            End With
            mdtTran.Columns.Add(dCol)

            dCol = New DataColumn
            With dCol
                .ColumnName = "overwritehead"
                .DataType = GetType(System.Boolean)
                .DefaultValue = False
            End With
            mdtTran.Columns.Add(dCol)

            dCol = New DataColumn
            With dCol
                .ColumnName = "overwriteslab"
                .DataType = GetType(System.Boolean)
                .DefaultValue = False
            End With
            mdtTran.Columns.Add(dCol)

            dCol = New DataColumn
            With dCol
                .ColumnName = "loginemployeeunkid"
                .DataType = GetType(System.Int32)
                .DefaultValue = 0
            End With
            mdtTran.Columns.Add(dCol)

            dCol = New DataColumn
            With dCol
                .ColumnName = "isvoid"
                .DataType = GetType(System.Boolean)
                .DefaultValue = False
            End With
            mdtTran.Columns.Add(dCol)

            dCol = New DataColumn
            With dCol
                .ColumnName = "voidreason"
                .DataType = GetType(System.String)
                .DefaultValue = ""
            End With
            mdtTran.Columns.Add(dCol)

            dCol = New DataColumn
            With dCol
                .ColumnName = "auditdatetime"
                .DataType = GetType(System.DateTime)
                .DefaultValue = DBNull.Value
            End With
            mdtTran.Columns.Add(dCol)

            dCol = New DataColumn
            With dCol
                .ColumnName = "audittype"
                .DataType = GetType(System.Int32)
                .DefaultValue = 0
            End With
            mdtTran.Columns.Add(dCol)

            dCol = New DataColumn
            With dCol
                .ColumnName = "audituserunkid"
                .DataType = GetType(System.Int32)
                .DefaultValue = 0
            End With
            mdtTran.Columns.Add(dCol)

            dCol = New DataColumn
            With dCol
                .ColumnName = "ip"
                .DataType = GetType(System.String)
                .DefaultValue = ""
            End With
            mdtTran.Columns.Add(dCol)

            dCol = New DataColumn
            With dCol
                .ColumnName = "host"
                .DataType = GetType(System.String)
                .DefaultValue = ""
            End With
            mdtTran.Columns.Add(dCol)

            dCol = New DataColumn
            With dCol
                .ColumnName = "form_name"
                .DataType = GetType(System.String)
                .DefaultValue = ""
            End With
            mdtTran.Columns.Add(dCol)

            dCol = New DataColumn
            With dCol
                .ColumnName = "isweb"
                .DataType = GetType(System.Boolean)
                .DefaultValue = False
            End With
            mdtTran.Columns.Add(dCol)

            dCol = New DataColumn
            With dCol
                .ColumnName = "isprocessed"
                .DataType = GetType(System.Boolean)
                .DefaultValue = False
            End With
            mdtTran.Columns.Add(dCol)

            dCol = New DataColumn
            With dCol
                .ColumnName = "operationtypeid"
                .DataType = GetType(System.Int32)
                .DefaultValue = 0
            End With
            mdtTran.Columns.Add(dCol)

            dCol = New DataColumn
            With dCol
                .ColumnName = "newattachdocumentid"
                .DataType = GetType(System.String)
                .DefaultValue = ""
            End With
            mdtTran.Columns.Add(dCol)

            dCol = New DataColumn
            With dCol
                .ColumnName = "deleteattachdocumentid"
                .DataType = GetType(System.String)
                .DefaultValue = ""
            End With
            mdtTran.Columns.Add(dCol)

            dCol = New DataColumn
            With dCol
                .ColumnName = "AUD"
                .DataType = GetType(System.String)
                .DefaultValue = ""
            End With
            mdtTran.Columns.Add(dCol)

            dCol = New DataColumn
            With dCol
                .ColumnName = "GUID"
                .DataType = GetType(System.String)
                .DefaultValue = ""
            End With
            mdtTran.Columns.Add(dCol)

            dCol = New DataColumn
            With dCol
                .ColumnName = "operationtype"
                .DataType = GetType(System.String)
                .DefaultValue = ""
            End With
            mdtTran.Columns.Add(dCol)

            dCol = New DataColumn
            With dCol
                .ColumnName = "membership_category"
                .DataType = GetType(System.String)
                .DefaultValue = ""
            End With
            mdtTran.Columns.Add(dCol)

            dCol = New DataColumn
            With dCol
                .ColumnName = "membershipname"
                .DataType = GetType(System.String)
                .DefaultValue = ""
            End With
            mdtTran.Columns.Add(dCol)

            dCol = New DataColumn
            With dCol
                .ColumnName = "ccategory"
                .DefaultValue = ""
                .DataType = GetType(System.String)
            End With
            mdtTran.Columns.Add(dCol)

            dCol = New DataColumn
            With dCol
                .ColumnName = "ccategoryid"
                .DefaultValue = -1
                .DataType = GetType(System.Int32)
            End With
            mdtTran.Columns.Add(dCol)

            dCol = New DataColumn
            With dCol
                .ColumnName = "emptrnheadid"
                .DefaultValue = -1
                .DataType = GetType(System.Int32)
            End With
            mdtTran.Columns.Add(dCol)

            dCol = New DataColumn
            With dCol
                .ColumnName = "cotrnheadid"
                .DefaultValue = -1
                .DataType = GetType(System.Int32)
            End With
            mdtTran.Columns.Add(dCol)

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: New; Module Name: " & mstrModuleName)
        Finally
        End Try
    End Sub

#End Region

#Region " Private/Public Methods "

    Private Sub GetMembership_tran(Optional ByVal xDataOperation As clsDataOperation = Nothing)
        Dim strQ As String = ""
        Dim dsList As New DataSet
        Dim exForce As Exception
        Try
            If xDataOperation IsNot Nothing Then
                objDataOperation = xDataOperation
            Else
                objDataOperation = New clsDataOperation
            End If
            objDataOperation.ClearParameters()

            strQ = "SELECT " & _
                   "     MAT.tranguid " & _
                   "    ,MAT.transactiondate " & _
                   "    ,MAT.mappingunkid " & _
                   "    ,MAT.approvalremark " & _
                   "    ,MAT.isfinal " & _
                   "    ,MAT.statusunkid " & _
                   "    ,MAT.membershiptranunkid " & _
                   "    ,MAT.employeeunkid " & _
                   "    ,MAT.membership_categoryunkid " & _
                   "    ,MAT.membershipunkid " & _
                   "    ,MAT.membershipno " & _
                   "    ,MAT.issue_date " & _
                   "    ,MAT.start_date " & _
                   "    ,MAT.expiry_date " & _
                   "    ,MAT.remark " & _
                   "    ,MAT.isactive " & _
                   "    ,MAT.isdeleted " & _
                   "    ,MAT.rehiretranunkid " & _
                   "    ,MAT.periodunkid As effetiveperiodid " & _
                   "    ,MAT.iscopyprevious_slab AS copyedslab " & _
                   "    ,MAT.isoverwriteheads AS overwritehead " & _
                   "    ,MAT.isoverwriteslab AS overwriteslab " & _
                   "    ,MAT.loginemployeeunkid " & _
                   "    ,MAT.isvoid " & _
                   "    ,MAT.voidreason " & _
                   "    ,MAT.auditdatetime " & _
                   "    ,MAT.audittype " & _
                   "    ,MAT.audituserunkid " & _
                   "    ,MAT.ip " & _
                   "    ,MAT.host " & _
                   "    ,MAT.form_name " & _
                   "    ,MAT.isweb " & _
                   "    ,MAT.isprocessed " & _
                   "    ,MAT.operationtypeid " & _
                   "    ,MAT.newattachdocumentid " & _
                   "    ,MAT.deleteattachdocumentid " & _
                   "    ,ISNULL(CM.name,'') AS membership_category " & _
                   "    ,ISNULL(MM.membershipname,'') AS membershipname " & _
                   "    ,CASE WHEN ISNULL(MAT.operationtypeid,0) = '" & CInt(clsEmployeeDataApproval.enOperationType.ADDED) & "' THEN @ADDED " & _
                   "          WHEN ISNULL(MAT.operationtypeid,0) = '" & CInt(clsEmployeeDataApproval.enOperationType.EDITED) & "' THEN @EDITED " & _
                   "          WHEN ISNULL(MAT.operationtypeid,0) = '" & CInt(clsEmployeeDataApproval.enOperationType.DELETED) & "' THEN @DELETED " & _
                   "     END AS operationtype " & _
                   "    ,CASE WHEN (MAT.isactive = 1 AND MAT.isdeleted = 0) THEN @Active " & _
                   "          WHEN (MAT.isactive = 0 AND MAT.isdeleted = 0) THEN @Inactive " & _
                   "          WHEN (MAT.isactive = 0 AND MAT.isdeleted = 1) THEN @Inactive " & _
                   "     END AS ccategory " & _
                   "    ,CASE WHEN (MAT.isactive = 1 AND MAT.isdeleted = 0) THEN 1 " & _
                   "          WHEN (MAT.isactive = 0 AND MAT.isdeleted = 0) THEN 2 " & _
                   "          WHEN (MAT.isactive = 0 AND MAT.isdeleted = 1) THEN 2 " & _
                   "     END AS ccategoryid " & _
                   "    ,ISNULL(MM.emptranheadunkid,0) as emptrnheadid  " & _
                   "    ,ISNULL(MM.cotranheadunkid,0) as cotrnheadid " & _
                   "FROM hremployee_meminfo_approval_tran MAT " & _
                   "    LEFT JOIN cfcommon_master AS CM ON CM.masterunkid = MAT.membership_categoryunkid " & _
                   "    LEFT JOIN hrmembership_master AS MM ON MM.membershipunkid = MAT.membershipunkid " & _
                   "WHERE MAT.employeeunkid = @employeeunkid AND MAT.isprocessed = 0 AND MAT.statusunkid = 1  "

            objDataOperation.AddParameter("@employeeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintEmployeeunkid.ToString)

            objDataOperation.AddParameter("@ADDED", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage("clsEmployeeDataApproval", 62, "Newly Added Information"))
            objDataOperation.AddParameter("@EDITED", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage("clsEmployeeDataApproval", 63, "Information Edited"))
            objDataOperation.AddParameter("@DELETED", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage("clsEmployeeDataApproval", 64, "Information Deleted"))

            objDataOperation.AddParameter("@Active", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage("clsMembershipTran", 1, "Active"))
            objDataOperation.AddParameter("@Inactive", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage("clsMembershipTran", 2, "Inactive"))

            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            For Each irow As DataRow In dsList.Tables(0).Rows
                mdtTran.ImportRow(irow)
            Next

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: GetMembership_tran; Module Name: " & mstrModuleName)
        Finally
        End Try
    End Sub

    Public Function GetEmployeeMembershipUnkid(ByVal intEmployeeId As Integer, ByVal StrMName As String) As Integer
        Dim StrQ As String = String.Empty
        Dim dsList As New DataSet
        Dim exForce As Exception
        Try
            objDataOperation = New clsDataOperation

            StrQ = "SELECT " & _
                   "	hremployee_meminfo_approval_tran.membershipunkid " & _
                   "FROM hremployee_meminfo_approval_tran " & _
                   "	JOIN hrmembership_master ON hremployee_meminfo_approval_tran.membershipunkid = hrmembership_master.membershipunkid " & _
                   "WHERE 1 = 1 AND employeeunkid = @EmpId AND membershipname = @name "

            objDataOperation.AddParameter("@EmpId", SqlDbType.Int, eZeeDataType.INT_SIZE, intEmployeeId)
            objDataOperation.AddParameter("@name", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, StrMName)

            dsList = objDataOperation.ExecQuery(StrQ, StrMName)

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & " : " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            If dsList.Tables(0).Rows.Count > 0 Then
                Return dsList.Tables(0).Rows(0)("membershipunkid")
            End If

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "GetEmployeeMembershipUnkid", mstrModuleName)
        End Try
    End Function

    Public Function Can_Assign_Membership(ByVal mintEmplrHeadId As Integer, ByVal intEmpId As Integer) As Boolean
        Dim StrQ As String = String.Empty
        Dim exForce As Exception
        Dim iCnt As Integer = 0
        Try
            objDataOperation = New clsDataOperation

            StrQ = "SELECT " & _
                   " * " & _
                   "FROM hremployee_meminfo_approval_tran " & _
                        "JOIN hrmembership_master ON hremployee_meminfo_approval_tran.membershipunkid = hrmembership_master.membershipunkid " & _
                   "WHERE employeeunkid = '" & intEmpId & "' AND isdeleted= 0 AND hremployee_meminfo_approval_tran.isactive = 1 " & _
                   "AND hrmembership_master.cotranheadunkid = '" & mintEmplrHeadId & "' "

            iCnt = objDataOperation.RecordCount(StrQ)

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & " : " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            If iCnt > 0 Then
                Return True
            Else
                Return False
            End If

        Catch ex As Exception
            Call DisplayError.Show("-1", ex.Message, "Can_Assign_Membership", mstrModuleName)
        End Try
    End Function

    Public Function InsertUpdateDelete_MembershipTran(ByVal mdtTran As DataTable, _
                                                      ByVal xDataOperation As clsDataOperation, _
                                                      ByVal intUserUnkId As Integer, _
                                                      ByVal strDatabaseName As String, _
                                                      ByVal xYearUnkid As Integer, _
                                                      ByVal xCompanyUnkid As Integer, _
                                                      ByVal xPeriodStart As DateTime, _
                                                      ByVal xPeriodEnd As DateTime, _
                                                      ByVal xUserModeSetting As String, _
                                                      ByVal xOnlyApproved As Boolean, _
                                                      ByVal xIncludeIn_ActiveEmployee As Boolean _
                                                      ) As Boolean
        Dim i As Integer
        Dim strQ As String = ""
        Dim exForce As Exception
        Try
            If xDataOperation Is Nothing Then
                objDataOperation = New clsDataOperation
                objDataOperation.BindTransaction()
            Else
                objDataOperation = xDataOperation
            End If
            objDataOperation.ClearParameters()
            For i = 0 To mdtTran.Rows.Count - 1
                With mdtTran.Rows(i)
                    objDataOperation.ClearParameters()
                    If Not IsDBNull(.Item("AUD")) Then
                        If .Item("AUD") = "A" Then
                            strQ = "INSERT INTO hremployee_meminfo_approval_tran " & _
                                   "( " & _
                                   "     tranguid " & _
                                   "    ,transactiondate " & _
                                   "    ,mappingunkid " & _
                                   "    ,approvalremark " & _
                                   "    ,isfinal " & _
                                   "    ,statusunkid " & _
                                   "    ,membershiptranunkid " & _
                                   "    ,employeeunkid " & _
                                   "    ,membership_categoryunkid " & _
                                   "    ,membershipunkid " & _
                                   "    ,membershipno " & _
                                   "    ,issue_date " & _
                                   "    ,start_date " & _
                                   "    ,expiry_date " & _
                                   "    ,remark " & _
                                   "    ,isactive " & _
                                   "    ,isdeleted " & _
                                   "    ,rehiretranunkid " & _
                                   "    ,periodunkid " & _
                                   "    ,iscopyprevious_slab " & _
                                   "    ,isoverwriteheads " & _
                                   "    ,isoverwriteslab " & _
                                   "    ,loginemployeeunkid " & _
                                   "    ,isvoid " & _
                                   "    ,voidreason " & _
                                   "    ,auditdatetime " & _
                                   "    ,audittype " & _
                                   "    ,audituserunkid " & _
                                   "    ,ip " & _
                                   "    ,host " & _
                                   "    ,form_name " & _
                                   "    ,isweb " & _
                                   "    ,isprocessed " & _
                                   "    ,operationtypeid " & _
                                   "    ,newattachdocumentid " & _
                                   "    ,deleteattachdocumentid " & _
                                   ") " & _
                                   "VALUES " & _
                                   "( " & _
                                   "     @tranguid " & _
                                   "    ,@transactiondate " & _
                                   "    ,@mappingunkid " & _
                                   "    ,@approvalremark " & _
                                   "    ,@isfinal " & _
                                   "    ,@statusunkid " & _
                                   "    ,@membershiptranunkid " & _
                                   "    ,@employeeunkid " & _
                                   "    ,@membership_categoryunkid " & _
                                   "    ,@membershipunkid " & _
                                   "    ,@membershipno " & _
                                   "    ,@issue_date " & _
                                   "    ,@start_date " & _
                                   "    ,@expiry_date " & _
                                   "    ,@remark " & _
                                   "    ,@isactive " & _
                                   "    ,@isdeleted " & _
                                   "    ,@rehiretranunkid " & _
                                   "    ,@periodunkid " & _
                                   "    ,@iscopyprevious_slab " & _
                                   "    ,@isoverwriteheads " & _
                                   "    ,@isoverwriteslab " & _
                                   "    ,@loginemployeeunkid " & _
                                   "    ,@isvoid " & _
                                   "    ,@voidreason " & _
                                   "    ,@auditdatetime " & _
                                   "    ,@audittype " & _
                                   "    ,@audituserunkid " & _
                                   "    ,@ip " & _
                                   "    ,@host " & _
                                   "    ,@form_name " & _
                                   "    ,@isweb " & _
                                   "    ,@isprocessed " & _
                                   "    ,@operationtypeid " & _
                                   "    ,@newattachdocumentid " & _
                                   "    ,@deleteattachdocumentid " & _
                                   "); "

                            objDataOperation.AddParameter("@tranguid", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, .Item("tranguid"))
                            objDataOperation.AddParameter("@transactiondate", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, .Item("transactiondate"))
                            objDataOperation.AddParameter("@mappingunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, .Item("mappingunkid"))
                            objDataOperation.AddParameter("@approvalremark", SqlDbType.NVarChar, eZeeDataType.DESC_SIZE, .Item("approvalremark"))
                            objDataOperation.AddParameter("@isfinal", SqlDbType.Bit, eZeeDataType.BIT_SIZE, .Item("isfinal"))
                            objDataOperation.AddParameter("@statusunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, .Item("statusunkid"))
                            objDataOperation.AddParameter("@membershiptranunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, .Item("membershiptranunkid"))
                            objDataOperation.AddParameter("@employeeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, .Item("employeeunkid"))
                            objDataOperation.AddParameter("@membership_categoryunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, .Item("membership_categoryunkid"))
                            objDataOperation.AddParameter("@membershipunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, .Item("membershipunkid"))
                            objDataOperation.AddParameter("@membershipno", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, .Item("membershipno"))

                            If .Item("issue_date").ToString = Nothing Then
                                objDataOperation.AddParameter("@issue_date", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, DBNull.Value)
                            Else
                                objDataOperation.AddParameter("@issue_date", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, .Item("issue_date"))
                            End If
                            If .Item("start_date").ToString = Nothing Then
                                objDataOperation.AddParameter("@start_date", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, DBNull.Value)
                            Else
                                objDataOperation.AddParameter("@start_date", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, .Item("start_date"))
                            End If
                            If .Item("expiry_date").ToString = Nothing Then
                                objDataOperation.AddParameter("@expiry_date", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, DBNull.Value)
                            Else
                                objDataOperation.AddParameter("@expiry_date", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, .Item("expiry_date"))
                            End If

                            objDataOperation.AddParameter("@remark", SqlDbType.NVarChar, eZeeDataType.DESC_SIZE, .Item("remark"))
                            objDataOperation.AddParameter("@isactive", SqlDbType.Bit, eZeeDataType.BIT_SIZE, .Item("isactive"))
                            objDataOperation.AddParameter("@isdeleted", SqlDbType.Bit, eZeeDataType.BIT_SIZE, .Item("isdeleted"))
                            objDataOperation.AddParameter("@rehiretranunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, .Item("rehiretranunkid"))
                            objDataOperation.AddParameter("@periodunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, .Item("effetiveperiodid"))
                            objDataOperation.AddParameter("@iscopyprevious_slab", SqlDbType.Bit, eZeeDataType.BIT_SIZE, .Item("copyedslab"))
                            objDataOperation.AddParameter("@isoverwriteheads", SqlDbType.Bit, eZeeDataType.BIT_SIZE, .Item("overwritehead"))
                            objDataOperation.AddParameter("@isoverwriteslab", SqlDbType.Bit, eZeeDataType.BIT_SIZE, .Item("overwriteslab"))
                            objDataOperation.AddParameter("@loginemployeeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, .Item("loginemployeeunkid"))
                            objDataOperation.AddParameter("@isvoid", SqlDbType.Bit, eZeeDataType.BIT_SIZE, .Item("isvoid"))
                            objDataOperation.AddParameter("@voidreason", SqlDbType.NVarChar, eZeeDataType.DESC_SIZE, .Item("voidreason"))
                            objDataOperation.AddParameter("@auditdatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, .Item("auditdatetime"))
                            objDataOperation.AddParameter("@audittype", SqlDbType.Int, eZeeDataType.INT_SIZE, .Item("audittype"))
                            objDataOperation.AddParameter("@audituserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, .Item("audituserunkid"))
                            objDataOperation.AddParameter("@ip", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, .Item("ip"))
                            objDataOperation.AddParameter("@host", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, .Item("host"))
                            objDataOperation.AddParameter("@form_name", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, .Item("form_name"))
                            objDataOperation.AddParameter("@isweb", SqlDbType.Bit, eZeeDataType.BIT_SIZE, .Item("isweb"))
                            objDataOperation.AddParameter("@isprocessed", SqlDbType.Bit, eZeeDataType.BIT_SIZE, .Item("isprocessed"))
                            objDataOperation.AddParameter("@operationtypeid", SqlDbType.Int, eZeeDataType.BIT_SIZE, .Item("operationtypeid"))
                            objDataOperation.AddParameter("@newattachdocumentid", SqlDbType.NVarChar, eZeeDataType.DESC_SIZE, .Item("newattachdocumentid"))
                            objDataOperation.AddParameter("@deleteattachdocumentid", SqlDbType.NVarChar, eZeeDataType.DESC_SIZE, .Item("deleteattachdocumentid"))

                            Dim dsList As DataSet = objDataOperation.ExecQuery(strQ, "List")

                            If objDataOperation.ErrorMessage <> "" Then
                                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                                Throw exForce
                            End If


                            If .Item("operationtypeid") = clsEmployeeDataApproval.enOperationType.DELETED Then
                                Dim objCommonATLog As New clsCommonATLog
                                If .Item("membershiptranunkid") > 0 Then

                                    objCommonATLog._FormName = .Item("form_name")
                                    objCommonATLog._LoginEmployeeUnkid = .Item("loginemployeeunkid")
                                    objCommonATLog._ClientIP = .Item("ip")
                                    objCommonATLog._HostName = .Item("host")
                                    objCommonATLog._FromWeb = .Item("isweb")
                                    objCommonATLog._AuditUserId = .Item("audituserunkid")
                                    objCommonATLog._CompanyUnkid = xCompanyUnkid
                                    objCommonATLog._AuditDate = .Item("auditdatetime")

                                    If objCommonATLog.Insert_TranAtLog(objDataOperation, "hremployee_master", "employeeunkid", .Item("employeeunkid"), "hremployee_meminfo_tran", "membershiptranunkid", .Item("membershiptranunkid"), 2, 3, , intUserUnkId) = False Then
                                        exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                                        Throw exForce
                                    End If
                                    objCommonATLog = Nothing


                                End If

                                strQ = "UPDATE hremployee_meminfo_tran SET " & _
                                       " isactive = 0 " & _
                                       ",isdeleted = @isdeleted " & _
                                       "WHERE membershiptranunkid = @membershiptranunkid "

                                If .Item("membershiptranunkid") > 0 Then
                                    If .Item("emptrnheadid") > 0 Or .Item("cotrnheadid") > 0 Then
                                        Dim iCurrPeriod As Integer = -1
                                        Dim objMData As New clsMasterData
                                        Dim objPdata As New clscommom_period_Tran
                                        Dim objED As New clsEarningDeduction

                                        iCurrPeriod = objMData.getFirstPeriodID(enModuleReference.Payroll, xYearUnkid, 1)
                                        objPdata._Periodunkid(strDatabaseName) = iCurrPeriod
                                        Dim dList As New DataSet
                                        dList = objED.GetList(strDatabaseName, intUserUnkId, xYearUnkid, xCompanyUnkid, xPeriodStart, xPeriodEnd, xUserModeSetting, xOnlyApproved, xIncludeIn_ActiveEmployee, "List", True, mintEmployeeunkid, , iCurrPeriod, , , , objPdata._End_Date.Date, , objDataOperation)
                                        If dList.Tables("List").Rows.Count > 0 Then
                                            Dim dRow() As DataRow = dList.Tables("List").Select("membershiptranunkid = '" & .Item("membershiptranunkid") & "'")
                                            If dRow.Length > 0 Then
                                                For j As Integer = 0 To dRow.Length - 1
                                                    If objED.Void(objDataOperation, dRow(j)("edunkid"), intUserUnkId, ConfigParameter._Object._CurrentDateAndTime, Language.getMessage(mstrModuleName, 6, "voided when membership deactivated."), strDatabaseName) = False Then
                                                        exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                                                        Throw exForce
                                                    End If
                                                    objDataOperation.ClearParameters()
                                                Next
                                            End If
                                            .Item("isdeleted") = False
                                        End If
                                        objMData = Nothing : objPdata = Nothing : objED = Nothing
                                    Else
                                        .Item("isdeleted") = True
                                    End If
                                End If

                                objDataOperation.ClearParameters()
                                objDataOperation.AddParameter("@isdeleted", SqlDbType.Bit, eZeeDataType.BIT_SIZE, CBool(.Item("isdeleted")))
                                objDataOperation.AddParameter("@membershiptranunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, .Item("membershiptranunkid").ToString)

                                Call objDataOperation.ExecNonQuery(strQ)

                                If objDataOperation.ErrorMessage <> "" Then
                                    exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                                    Throw exForce
                                End If

                                objCommonATLog = New clsCommonATLog

                                objCommonATLog._FormName = .Item("form_name")
                                objCommonATLog._LoginEmployeeUnkid = .Item("loginemployeeunkid")
                                objCommonATLog._ClientIP = .Item("ip")
                                objCommonATLog._HostName = .Item("host")
                                objCommonATLog._FromWeb = .Item("isweb")
                                objCommonATLog._AuditUserId = .Item("audituserunkid")
                                objCommonATLog._CompanyUnkid = xCompanyUnkid
                                objCommonATLog._AuditDate = .Item("auditdatetime")

                                If objCommonATLog.Insert_TranAtLog(objDataOperation, "hremployee_master", "employeeunkid", .Item("employeeunkid"), "hremployee_meminfo_tran", "membershiptranunkid", .Item("membershiptranunkid"), 2, 3, , intUserUnkId) = False Then
                                    exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                                    Throw exForce
                                End If

                                objCommonATLog = Nothing
                            End If
                        End If
                    End If
                End With
            Next

            If xDataOperation Is Nothing Then objDataOperation.ReleaseTransaction(True)
            Return True
        Catch ex As Exception
            If xDataOperation Is Nothing Then
                objDataOperation.ReleaseTransaction(False)
            End If
            Throw New Exception(ex.Message & "; Procedure Name: InsertUpdateDelete_MembershipTran; Module Name: " & mstrModuleName)
        Finally
        End Try
    End Function

    Public Function Update(ByVal xDataOpr As clsDataOperation, _
                           ByVal intEmployeeId As Integer, _
                           ByVal intMembershipUnkid As Integer, _
                           ByVal intMembershipTranId As Integer, _
                           ByVal strColName As String) As Boolean
        Dim strQ As String = ""
        Dim exForce As Exception

        If xDataOpr IsNot Nothing Then
            objDataOperation = xDataOpr
        Else
            objDataOperation = New clsDataOperation
        End If
        objDataOperation.ClearParameters()
        Try
            strQ = "UPDATE hremployee_meminfo_approval_tran SET " & _
                   " " & strColName & " = '" & intMembershipTranId & "' " & _
                   "WHERE isprocessed= 0 and membershipunkid = @membershipunkid AND employeeunkid = @employeeunkid "


            'Gajanan [31-May-2020] -- Start
            objDataOperation.AddParameter("@employeeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intEmployeeId)
            objDataOperation.AddParameter("@membershipunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intMembershipUnkid)
            'Gajanan [31-May-2020] -- End


            Call objDataOperation.ExecNonQuery(strQ)

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            Return True
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: Update; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If xDataOpr Is Nothing Then objDataOperation = Nothing
        End Try
    End Function

#End Region

End Class
