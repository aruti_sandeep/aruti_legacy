﻿Imports eZeeCommonLib
''' <summary>
''' Purpose: 
''' Developer: Gajanan
''' </summary>
Public Class clsIdentity_Approval_tran
    Private Const mstrModuleName = "clsIdentity_Approval_tran"
    Dim objDataOperation As clsDataOperation
    Dim mstrMessage As String = ""

#Region " Private variables "

    Private mintEmployeeunkid As Integer = 0
    Private mdtTran As DataTable = Nothing

#End Region

#Region " Private Methods "

    Public Property _Employeeunkid() As Integer
        Get
            Return mintEmployeeunkid
        End Get
        Set(ByVal value As Integer)
            mintEmployeeunkid = value
            GetIdentity_tran()
        End Set
    End Property

    Public Property _xObjDataOperation() As clsDataOperation
        Get
            Return objDataOperation
        End Get
        Set(ByVal value As clsDataOperation)
            objDataOperation = value
        End Set
    End Property

    Public Property _DataTable() As DataTable
        Get
            Return mdtTran
        End Get
        Set(ByVal value As DataTable)
            mdtTran = value
        End Set
    End Property


#End Region

#Region " Contructor "

    Public Sub New()
        mdtTran = New DataTable("IdentityTran")
        Dim dCol As DataColumn
        Try
            dCol = New DataColumn
            With dCol
                .ColumnName = "tranguid"
                .DataType = GetType(System.String)
                .DefaultValue = ""
            End With
            mdtTran.Columns.Add(dCol)

            dCol = New DataColumn
            With dCol
                .ColumnName = "transactiondate"
                .DataType = GetType(System.DateTime)
                .DefaultValue = DBNull.Value
            End With
            mdtTran.Columns.Add(dCol)

            dCol = New DataColumn
            With dCol
                .ColumnName = "mappingunkid"
                .DataType = GetType(System.Int32)
                .DefaultValue = 0
            End With
            mdtTran.Columns.Add(dCol)

            dCol = New DataColumn
            With dCol
                .ColumnName = "approvalremark"
                .DataType = GetType(System.String)
                .DefaultValue = ""
            End With
            mdtTran.Columns.Add(dCol)

            dCol = New DataColumn
            With dCol
                .ColumnName = "isfinal"
                .DataType = GetType(System.Boolean)
                .DefaultValue = False
            End With
            mdtTran.Columns.Add(dCol)

            dCol = New DataColumn
            With dCol
                .ColumnName = "statusunkid"
                .DataType = GetType(System.Int32)
                .DefaultValue = 0
            End With
            mdtTran.Columns.Add(dCol)

            dCol = New DataColumn
            With dCol
                .ColumnName = "identitytranunkid"
                .DataType = GetType(System.Int32)
                .DefaultValue = 0
            End With
            mdtTran.Columns.Add(dCol)

            dCol = New DataColumn
            With dCol
                .ColumnName = "employeeunkid"
                .DataType = GetType(System.Int32)
                .DefaultValue = 0
            End With
            mdtTran.Columns.Add(dCol)

            dCol = New DataColumn
            With dCol
                .ColumnName = "idtypeunkid"
                .DataType = GetType(System.Int32)
                .DefaultValue = 0
            End With
            mdtTran.Columns.Add(dCol)

            dCol = New DataColumn
            With dCol
                .ColumnName = "identity_no"
                .DataType = GetType(System.String)
                .DefaultValue = ""
            End With
            mdtTran.Columns.Add(dCol)

            dCol = New DataColumn
            With dCol
                .ColumnName = "countryunkid"
                .DataType = GetType(System.Int32)
                .DefaultValue = 0
            End With
            mdtTran.Columns.Add(dCol)

            dCol = New DataColumn
            With dCol
                .ColumnName = "serial_no"
                .DataType = GetType(System.String)
                .DefaultValue = ""
            End With
            mdtTran.Columns.Add(dCol)

            dCol = New DataColumn
            With dCol
                .ColumnName = "issued_place"
                .DataType = GetType(System.String)
                .DefaultValue = ""
            End With
            mdtTran.Columns.Add(dCol)

            dCol = New DataColumn
            With dCol
                .ColumnName = "dl_class"
                .DataType = GetType(System.String)
                .DefaultValue = ""
            End With
            mdtTran.Columns.Add(dCol)

            dCol = New DataColumn
            With dCol
                .ColumnName = "issue_date"
                .DataType = GetType(System.DateTime)
                .DefaultValue = DBNull.Value
            End With
            mdtTran.Columns.Add(dCol)

            dCol = New DataColumn
            With dCol
                .ColumnName = "expiry_date"
                .DataType = GetType(System.DateTime)
                .DefaultValue = DBNull.Value
            End With
            mdtTran.Columns.Add(dCol)

            dCol = New DataColumn
            With dCol
                .ColumnName = "isdefault"
                .DataType = GetType(System.Boolean)
                .DefaultValue = False
            End With
            mdtTran.Columns.Add(dCol)

            dCol = New DataColumn
            With dCol
                .ColumnName = "loginemployeeunkid"
                .DataType = GetType(System.Int32)
                .DefaultValue = 0
            End With
            mdtTran.Columns.Add(dCol)

            dCol = New DataColumn
            With dCol
                .ColumnName = "isvoid"
                .DataType = GetType(System.Boolean)
                .DefaultValue = False
            End With
            mdtTran.Columns.Add(dCol)

            dCol = New DataColumn
            With dCol
                .ColumnName = "voidreason"
                .DataType = GetType(System.String)
                .DefaultValue = ""
            End With
            mdtTran.Columns.Add(dCol)

            dCol = New DataColumn
            With dCol
                .ColumnName = "auditdatetime"
                .DataType = GetType(System.DateTime)
                .DefaultValue = DBNull.Value
            End With
            mdtTran.Columns.Add(dCol)

            dCol = New DataColumn
            With dCol
                .ColumnName = "audittype"
                .DataType = GetType(System.Int32)
                .DefaultValue = 0
            End With
            mdtTran.Columns.Add(dCol)

            dCol = New DataColumn
            With dCol
                .ColumnName = "audituserunkid"
                .DataType = GetType(System.Int32)
                .DefaultValue = 0
            End With
            mdtTran.Columns.Add(dCol)

            dCol = New DataColumn
            With dCol
                .ColumnName = "ip"
                .DataType = GetType(System.String)
                .DefaultValue = ""
            End With
            mdtTran.Columns.Add(dCol)

            dCol = New DataColumn
            With dCol
                .ColumnName = "host"
                .DataType = GetType(System.String)
                .DefaultValue = ""
            End With
            mdtTran.Columns.Add(dCol)

            dCol = New DataColumn
            With dCol
                .ColumnName = "form_name"
                .DataType = GetType(System.String)
                .DefaultValue = ""
            End With
            mdtTran.Columns.Add(dCol)

            dCol = New DataColumn
            With dCol
                .ColumnName = "isweb"
                .DataType = GetType(System.Boolean)
                .DefaultValue = False
            End With
            mdtTran.Columns.Add(dCol)

            dCol = New DataColumn
            With dCol
                .ColumnName = "isprocessed"
                .DataType = GetType(System.Boolean)
                .DefaultValue = False
            End With
            mdtTran.Columns.Add(dCol)

            dCol = New DataColumn
            With dCol
                .ColumnName = "operationtypeid"
                .DataType = GetType(System.Int32)
                .DefaultValue = 0
            End With
            mdtTran.Columns.Add(dCol)

            dCol = New DataColumn
            With dCol
                .ColumnName = "AUD"
                .DataType = GetType(System.String)
                .DefaultValue = ""
            End With
            mdtTran.Columns.Add(dCol)

            dCol = New DataColumn
            With dCol
                .ColumnName = "GUID"
                .DataType = GetType(System.String)
                .DefaultValue = ""
            End With
            mdtTran.Columns.Add(dCol)

            dCol = New DataColumn
            With dCol
                .ColumnName = "identities"
                .DataType = GetType(System.String)
                .DefaultValue = ""
            End With
            mdtTran.Columns.Add(dCol)

            dCol = New DataColumn
            With dCol
                .ColumnName = "country"
                .DataType = GetType(System.String)
                .DefaultValue = ""
            End With
            mdtTran.Columns.Add(dCol)

            dCol = New DataColumn
            With dCol
                .ColumnName = "operationtype"
                .DataType = GetType(System.String)
                .DefaultValue = ""
            End With
            mdtTran.Columns.Add(dCol)

            'S.SANDEEP |26-APR-2019| -- START
            dCol = New DataColumn
            With dCol
                .ColumnName = "newattachdocumentid"
                .DataType = GetType(System.String)
                .DefaultValue = ""
            End With
            mdtTran.Columns.Add(dCol)

            dCol = New DataColumn
            With dCol
                .ColumnName = "deleteattachdocumentid"
                .DataType = GetType(System.String)
                .DefaultValue = ""
            End With
            mdtTran.Columns.Add(dCol)
            'S.SANDEEP |26-APR-2019| -- END

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: New; Module Name: " & mstrModuleName)
        Finally
        End Try
    End Sub

#End Region

#Region " Private Methods "

    Private Sub GetIdentity_tran(Optional ByVal xDataOperation As clsDataOperation = Nothing)
        Dim strQ As String = ""
        Dim dsList As New DataSet
        Dim exForce As Exception
        Try
            If xDataOperation IsNot Nothing Then
                objDataOperation = xDataOperation
            Else
                objDataOperation = New clsDataOperation
            End If
            objDataOperation.ClearParameters()

            strQ = "SELECT " & _
                   "     hremployee_idinfo_approval_tran.tranguid " & _
                   "    ,hremployee_idinfo_approval_tran.transactiondate " & _
                   "    ,hremployee_idinfo_approval_tran.mappingunkid " & _
                   "    ,hremployee_idinfo_approval_tran.approvalremark " & _
                   "    ,hremployee_idinfo_approval_tran.isfinal " & _
                   "    ,hremployee_idinfo_approval_tran.statusunkid " & _
                   "    ,hremployee_idinfo_approval_tran.identitytranunkid " & _
                   ",hremployee_idinfo_approval_tran.employeeunkid " & _
                   ",hremployee_idinfo_approval_tran.idtypeunkid " & _
                   ",hremployee_idinfo_approval_tran.identity_no " & _
                   ",hremployee_idinfo_approval_tran.countryunkid " & _
                   ",hremployee_idinfo_approval_tran.serial_no " & _
                   ",hremployee_idinfo_approval_tran.issued_place " & _
                   ",hremployee_idinfo_approval_tran.dl_class " & _
                   ",hremployee_idinfo_approval_tran.issue_date " & _
                   ",hremployee_idinfo_approval_tran.expiry_date " & _
                   ",hremployee_idinfo_approval_tran.isdefault " & _
                   "    ,loginemployeeunkid " & _
                   "    ,isvoid " & _
                   "    ,voidreason " & _
                   "    ,auditdatetime " & _
                   "    ,audittype " & _
                   "    ,audituserunkid " & _
                   "    ,ip " & _
                   "    ,host " & _
                   "    ,form_name " & _
                   "    ,isweb " & _
                   "    ,isprocessed " & _
                   "    ,hremployee_idinfo_approval_tran.operationtypeid " & _
                   ",'' As AUD " & _
                   "    ,'' AS GUID " & _
                   ",ISNULL(cfcommon_master.name,'') AS identities " & _
                   ",ISNULL(hrmsConfiguration..cfcountry_master.country_name,'') AS country " & _
                   "    ,CASE WHEN ISNULL(hremployee_idinfo_approval_tran.operationtypeid,0) = '" & CInt(clsEmployeeDataApproval.enOperationType.ADDED) & "' THEN @ADDED " & _
                   "          WHEN ISNULL(hremployee_idinfo_approval_tran.operationtypeid,0) = '" & CInt(clsEmployeeDataApproval.enOperationType.EDITED) & "' THEN @EDITED " & _
                   "          WHEN ISNULL(hremployee_idinfo_approval_tran.operationtypeid,0) = '" & CInt(clsEmployeeDataApproval.enOperationType.DELETED) & "' THEN @DELETED " & _
                   "     END AS operationtype " & _
                   "    ,newattachdocumentid " & _
                   "    ,deleteattachdocumentid " & _
                   "FROM hremployee_idinfo_approval_tran " & _
                   "LEFT JOIN hrmsConfiguration..cfcountry_master ON hremployee_idinfo_approval_tran.countryunkid = hrmsConfiguration..cfcountry_master.countryunkid " & _
                   "LEFT JOIN cfcommon_master ON cfcommon_master.masterunkid = hremployee_idinfo_approval_tran.idtypeunkid AND mastertype ='" & clsCommon_Master.enCommonMaster.IDENTITY_TYPES & "' " & _
                   "WHERE employeeunkid = @employeeunkid AND hremployee_idinfo_approval_tran.isprocessed = 0 AND ISNULL(hremployee_idinfo_approval_tran.isvoid,0) = 0 AND ISNULL(hremployee_idinfo_approval_tran.statusunkid,0) = 1 " 'Gajanan [17-April-2019] -ADD ISVOID=0 AND statusunkid=0



            objDataOperation.AddParameter("@employeeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintEmployeeunkid.ToString)

            objDataOperation.AddParameter("@ADDED", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage("clsEmployeeDataApproval", 62, "Newly Added Information"))
            objDataOperation.AddParameter("@EDITED", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage("clsEmployeeDataApproval", 63, "Information Edited"))
            objDataOperation.AddParameter("@DELETED", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage("clsEmployeeDataApproval", 64, "Information Deleted"))

            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            For Each irow As DataRow In dsList.Tables(0).Rows
                mdtTran.ImportRow(irow)
            Next

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: GetIdentity_tran; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            If xDataOperation Is Nothing Then objDataOperation = Nothing
        End Try
    End Sub

    Public Function ImportEmployeeIdentity(ByVal mdtTran As DataTable, ByVal intUserUnkId As Integer, ByVal intCompanyId As Integer) As Boolean 'S.SANDEEP |26-APR-2019| -- START {intCompanyId} -- END
        Dim blnFlag As Boolean = False
        Dim objDataOperation As New clsDataOperation
        Try
            objDataOperation.BindTransaction()

            'S.SANDEEP |26-APR-2019| -- START
            'blnFlag = InsertUpdateDelete_IdentityTran(mdtTran, objDataOperation, intUserUnkId)
            blnFlag = InsertUpdateDelete_IdentityTran(mdtTran, objDataOperation, intUserUnkId, intCompanyId)
            'S.SANDEEP |26-APR-2019| -- END

            objDataOperation.ReleaseTransaction(True)
        Catch ex As Exception
            objDataOperation.ReleaseTransaction(False)
            Throw New Exception(ex.Message & "; Procedure Name: ImportEmployeeIdentity; Module Name: " & mstrModuleName)
        Finally
            objDataOperation = Nothing
        End Try
        Return blnFlag
    End Function

    'S.SANDEEP |26-APR-2019| -- START
    Public Function InsertUpdateDelete_IdentityTran(ByVal mdtTran As DataTable, _
                                                    ByVal xDataOperation As clsDataOperation, _
                                                    ByVal intUserUnkId As Integer, _
                                                    ByVal intCompanyId As Integer, _
                                                    Optional ByVal dtDocument As DataTable = Nothing) As Boolean
        'Public Function InsertUpdateDelete_IdentityTran(ByVal mdtTran As DataTable, ByVal xDataOperation As clsDataOperation, ByVal intUserUnkId As Integer) As Boolean
        'S.SANDEEP |26-APR-2019| -- END
        Dim i As Integer
        Dim strQ As String = ""
        Dim exForce As Exception
        Try
            'S.SANDEEP |26-APR-2019| -- START
            Dim objDocument As New clsScan_Attach_Documents
            Dim strLocalPath As String = ""
            Dim dtTran As DataTable = Nothing
            Dim strFolderName As String = ""
            If dtDocument IsNot Nothing Then
                If dtDocument.Rows.Count > 0 Then
                    Dim objConfig As New clsConfigOptions
                    strLocalPath = objConfig.GetKeyValue(intCompanyId, "DocumentPath")
                    If strLocalPath Is Nothing Then strLocalPath = ""
                    objConfig = Nothing
                    strFolderName = (New clsScan_Attach_Documents).GetDocFolderName("Docs", enScanAttactRefId.IDENTITYS).Tables(0).Rows(0)("Name").ToString
                    If strFolderName IsNot Nothing Then strFolderName = strFolderName & "\"
                    dtTran = objDocument._Datatable
                End If
            End If
            'S.SANDEEP |26-APR-2019| -- END

            If xDataOperation Is Nothing Then
                objDataOperation = New clsDataOperation
                objDataOperation.BindTransaction()
            Else
                objDataOperation = xDataOperation
            End If
            objDataOperation.ClearParameters()

            For i = 0 To mdtTran.Rows.Count - 1
                With mdtTran.Rows(i)
                    Dim blnMakeDeafault As Boolean = True
                    If IsDefaultAssigned(mintEmployeeunkid, objDataOperation) Then
                        blnMakeDeafault = False
                    End If
                    objDataOperation.ClearParameters()

                    'S.SANDEEP |26-APR-2019| -- START
                    If dtDocument IsNot Nothing Then
                        Dim ftab As DataTable = New DataView(dtDocument, "pguid = '" & .Item("guid") & "'", "", DataViewRowState.CurrentRows).ToTable
                        For Each drow As DataRow In ftab.Rows
                            Dim dr As DataRow = dtTran.NewRow
                            dr("scanattachtranunkid") = drow("scanattachtranunkid")
                            dr("documentunkid") = drow("documentunkid")
                            dr("employeeunkid") = drow("employeeunkid")
                            dr("filename") = drow("filename")
                            dr("scanattachrefid") = drow("scanattachrefid")
                            dr("modulerefid") = drow("modulerefid")
                            dr("form_name") = drow("form_name")
                            dr("userunkid") = drow("userunkid")
                            dr("transactionunkid") = drow("transactionunkid")
                            dr("userunkid") = intUserUnkId
                            dr("attached_date") = drow("attached_date")
                            dr("orgfilepath") = drow("orgfilepath")
                            dr("destfilepath") = strLocalPath & strFolderName & CStr(drow.Item("filename"))
                            dr("AUD") = drow("AUD")
                            dr("filesize") = drow("filesize")
                            dr("fileuniquename") = drow("fileuniquename")
                            dr("filepath") = drow("filepath")

                            dtTran.Rows.Add(dr)
                        Next
                        objDocument._Datatable = ftab
                        objDocument.InsertUpdateDelete_Documents(objDataOperation)
                    End If
                    objDataOperation.ClearParameters()

                    If .Item("operationtypeid") = clsEmployeeDataApproval.enOperationType.DELETED Then
                        Dim objScanAttachment As New clsScan_Attach_Documents
                        Dim objConfig As New clsConfigOptions
                        Dim strDocumentPath As String = ""
                        strDocumentPath = objConfig.GetKeyValue(intCompanyId, "DocumentPath", objDataOperation)
                        objConfig = Nothing
                        'S.SANDEEP |04-SEP-2021| -- START
                        'objScanAttachment.GetList(strDocumentPath, "List", "", CInt(.Item("employeeunkid")), False, objDataOperation)
                        objScanAttachment.GetList(strDocumentPath, "List", "", CInt(.Item("employeeunkid")), False, objDataOperation, , , CBool(IIf(CInt(.Item("employeeunkid")) <= 0, True, False)))
                        'S.SANDEEP |04-SEP-2021| -- END
                        Dim oDoc As DataTable = New DataView(objScanAttachment._Datatable, "transactionunkid = '" & .Item("identitytranunkid").ToString & "' AND scanattachrefid = '" & CInt(enScanAttactRefId.IDENTITYS) & "'", "", DataViewRowState.CurrentRows).ToTable()
                        objScanAttachment = Nothing
                        For Each row As DataRow In oDoc.Rows
                            row("PGUID") = .Item("guid")
                            row("AUD") = "D"
                        Next
                        oDoc.AcceptChanges()
                        objDocument._Datatable = oDoc
                        objDocument.InsertUpdateDelete_Documents(objDataOperation)
                    End If
                    'S.SANDEEP |26-APR-2019| -- END

                    If Not IsDBNull(.Item("AUD")) Then
                        If .Item("AUD") = "A" Then
                        strQ = "INSERT INTO hremployee_idinfo_approval_tran ( " & _
                                    "  tranguid " & _
                                    ", transactiondate " & _
                                    ", mappingunkid " & _
                                    ", approvalremark " & _
                                    ", isfinal " & _
                                    ", statusunkid " & _
                                    ", identitytranunkid " & _
                                    ", employeeunkid " & _
                                    ", idtypeunkid " & _
                                    ", identity_no " & _
                                    ", countryunkid " & _
                                    ", serial_no " & _
                                    ", issued_place " & _
                                    ", dl_class " & _
                                    ", issue_date " & _
                                    ", expiry_date " & _
                                    ", isdefault " & _
                                    ", loginemployeeunkid " & _
                                    ", isvoid " & _
                                            ", voidreason " & _
                                    ", auditdatetime " & _
                                    ", audittype " & _
                                    ", audituserunkid " & _
                                    ", ip " & _
                                    ", host " & _
                                    ", form_name " & _
                                    ", isweb " & _
                                    ", isprocessed " & _
                                    ", operationtypeid" & _
                                        ", newattachdocumentid " & _
                                        ", deleteattachdocumentid " & _
                              ") VALUES (" & _
                                    "  @tranguid " & _
                                    ", @transactiondate " & _
                                    ", @mappingunkid " & _
                                    ", @approvalremark " & _
                                    ", @isfinal " & _
                                    ", @statusunkid " & _
                                    ", @identitytranunkid " & _
                                    ", @employeeunkid " & _
                                    ", @idtypeunkid " & _
                                    ", @identity_no " & _
                                    ", @countryunkid " & _
                                    ", @serial_no " & _
                                    ", @issued_place " & _
                                    ", @dl_class " & _
                                    ", @issue_date " & _
                                    ", @expiry_date " & _
                                    ", @isdefault " & _
                                    ", @loginemployeeunkid " & _
                                    ", @isvoid " & _
                                            ", @voidreason " & _
                                    ", @auditdatetime " & _
                                    ", @audittype " & _
                                    ", @audituserunkid " & _
                                    ", @ip " & _
                                    ", @host " & _
                                    ", @form_name " & _
                                    ", @isweb " & _
                                    ", @isprocessed " & _
                                    ", @operationtypeid" & _
                                        ", @newattachdocumentid " & _
                                        ", @deleteattachdocumentid " & _
                                      "); "
                            'S.SANDEEP |26-APR-2019| -- START {newattachdocumentid,deleteattachdocumentid} -- END

                            objDataOperation.AddParameter("@tranguid", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, .Item("tranguid"))
                            objDataOperation.AddParameter("@transactiondate", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, .Item("transactiondate"))
                            objDataOperation.AddParameter("@mappingunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, .Item("mappingunkid"))
                            objDataOperation.AddParameter("@approvalremark", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, .Item("approvalremark"))
                            objDataOperation.AddParameter("@isfinal", SqlDbType.Bit, eZeeDataType.BIT_SIZE, .Item("isfinal"))
                            objDataOperation.AddParameter("@statusunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, .Item("statusunkid"))
                            objDataOperation.AddParameter("@identitytranunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, .Item("identitytranunkid"))
                            objDataOperation.AddParameter("@employeeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, .Item("employeeunkid"))
                        objDataOperation.AddParameter("@idtypeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, .Item("idtypeunkid").ToString)
                        objDataOperation.AddParameter("@identity_no", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, .Item("identity_no").ToString)
                        objDataOperation.AddParameter("@countryunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, .Item("countryunkid").ToString)
                        objDataOperation.AddParameter("@serial_no", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, .Item("serial_no").ToString)
                        objDataOperation.AddParameter("@issued_place", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, .Item("issued_place").ToString)
                        objDataOperation.AddParameter("@dl_class", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, .Item("dl_class").ToString)
                        If .Item("issue_date").ToString = Nothing Then
                            objDataOperation.AddParameter("@issue_date", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, DBNull.Value)
                        Else
                            objDataOperation.AddParameter("@issue_date", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, .Item("issue_date").ToString)
                        End If
                        If .Item("expiry_date").ToString = Nothing Then
                            objDataOperation.AddParameter("@expiry_date", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, DBNull.Value)
                        Else
                            objDataOperation.AddParameter("@expiry_date", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, .Item("expiry_date").ToString)
                        End If
                        objDataOperation.AddParameter("@isdefault", SqlDbType.Bit, eZeeDataType.BIT_SIZE, blnMakeDeafault)
                            objDataOperation.AddParameter("@loginemployeeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, .Item("loginemployeeunkid"))
                            objDataOperation.AddParameter("@isvoid", SqlDbType.Bit, eZeeDataType.BIT_SIZE, .Item("isvoid"))
                            objDataOperation.AddParameter("@voidreason", SqlDbType.NVarChar, eZeeDataType.DESC_SIZE, .Item("voidreason"))
                            objDataOperation.AddParameter("@auditdatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, .Item("auditdatetime"))
                            objDataOperation.AddParameter("@audittype", SqlDbType.Int, eZeeDataType.INT_SIZE, .Item("audittype"))
                            objDataOperation.AddParameter("@audituserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, .Item("audituserunkid"))
                            objDataOperation.AddParameter("@ip", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, .Item("ip"))
                            objDataOperation.AddParameter("@host", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, .Item("host"))
                            objDataOperation.AddParameter("@form_name", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, .Item("form_name"))
                            objDataOperation.AddParameter("@isweb", SqlDbType.Bit, eZeeDataType.BIT_SIZE, .Item("isweb"))
                            objDataOperation.AddParameter("@isprocessed", SqlDbType.Bit, eZeeDataType.BIT_SIZE, .Item("isprocessed"))
                            objDataOperation.AddParameter("@operationtypeid", SqlDbType.Int, eZeeDataType.INT_SIZE, .Item("operationtypeid"))

                            'S.SANDEEP |26-APR-2019| -- START
                            If objDocument._Newattachdocumentids.Length > 0 Then
                                objDataOperation.AddParameter("@newattachdocumentid", SqlDbType.NVarChar, objDocument._Newattachdocumentids.Length, objDocument._Newattachdocumentids)
                            Else
                                objDataOperation.AddParameter("@newattachdocumentid", SqlDbType.NVarChar, .Item("newattachdocumentid").ToString().Length, .Item("newattachdocumentid").ToString())
                            End If

                            If objDocument._Deleteattachdocumentids.Length > 0 Then
                                objDataOperation.AddParameter("@deleteattachdocumentid", SqlDbType.NVarChar, objDocument._Deleteattachdocumentids.Length, objDocument._Deleteattachdocumentids)
                            Else
                                objDataOperation.AddParameter("@deleteattachdocumentid", SqlDbType.NVarChar, .Item("deleteattachdocumentid").ToString().Length, .Item("deleteattachdocumentid").ToString())
                            End If
                            'S.SANDEEP |26-APR-2019| -- END

                        Dim dsList As DataSet = objDataOperation.ExecQuery(strQ, "List")

                        If objDataOperation.ErrorMessage <> "" Then
                            exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                            Throw exForce
                        End If
                            If objDocument._Newattachdocumentids.Trim.Length > 0 Then
                                .Item("newattachdocumentid") = objDocument._Newattachdocumentids
                            End If
                            If objDocument._Deleteattachdocumentids.Trim.Length > 0 Then
                                .Item("deleteattachdocumentid") = objDocument._Deleteattachdocumentids
                            End If

                            If .Item("operationtypeid") = clsEmployeeDataApproval.enOperationType.DELETED Then
                                Dim objCommonATLog As New clsCommonATLog
                            If .Item("identitytranunkid") > 0 Then



                                    objCommonATLog._FormName = .Item("form_name")
                                    objCommonATLog._LoginEmployeeUnkid = .Item("loginemployeeunkid")
                                    objCommonATLog._ClientIP = .Item("ip")
                                    objCommonATLog._HostName = .Item("host")
                                    objCommonATLog._FromWeb = .Item("isweb")
                                    objCommonATLog._AuditUserId = .Item("audituserunkid")
                                    objCommonATLog._CompanyUnkid = intCompanyId
                                    objCommonATLog._AuditDate = .Item("auditdatetime")

                                    If objCommonATLog.Insert_TranAtLog(objDataOperation, "hremployee_master", "employeeunkid", .Item("employeeunkid"), "hremployee_idinfo_tran", "identitytranunkid", .Item("identitytranunkid"), 2, 3, , intUserUnkId) = False Then
                                        exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                                        Throw exForce
                                    End If

                                    objCommonATLog = Nothing

                                End If

                                objCommonATLog = New clsCommonATLog

                                objCommonATLog._FormName = .Item("form_name")
                                objCommonATLog._LoginEmployeeUnkid = .Item("loginemployeeunkid")
                                objCommonATLog._ClientIP = .Item("ip")
                                objCommonATLog._HostName = .Item("host")
                                objCommonATLog._FromWeb = .Item("isweb")
                                objCommonATLog._AuditUserId = .Item("audituserunkid")
                                objCommonATLog._CompanyUnkid = intCompanyId
                                objCommonATLog._AuditDate = .Item("auditdatetime")

                                If objCommonATLog.Insert_TranAtLog(objDataOperation, "hremployee_master", "employeeunkid", .Item("employeeunkid"), "hremployee_idinfo_tran", "identitytranunkid", .Item("identitytranunkid"), 2, 3, , intUserUnkId) = False Then
                                    exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                                    Throw exForce
                            End If

                                objCommonATLog = Nothing

                                strQ = "DELETE FROM hremployee_idinfo_tran WHERE identitytranunkid = @identitytranunkid "

                            objDataOperation.AddParameter("@identitytranunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, .Item("identitytranunkid").ToString)

                            Call objDataOperation.ExecNonQuery(strQ)

                            If objDataOperation.ErrorMessage <> "" Then
                                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                                Throw exForce
                            End If

                        End If
                        End If
                    End If
                End With
            Next

            If xDataOperation Is Nothing Then
                objDataOperation.ReleaseTransaction(True)
            End If

            Return True

        Catch ex As Exception
            If xDataOperation Is Nothing Then
                objDataOperation.ReleaseTransaction(False)
            End If
            Throw New Exception(ex.Message & "; Procedure Name: InsertUpdateDelete_IdentityTran; Module Name: " & mstrModuleName)
            Return False
        Finally
            exForce = Nothing
            If xDataOperation Is Nothing Then objDataOperation = Nothing
        End Try
    End Function

    Public Function IsDefaultAssigned(ByVal intEmployeeId As Integer, ByVal objDo As clsDataOperation) As Boolean
        Dim blnFlag As Boolean = False
        Dim StrQ As String = String.Empty
        Try
            StrQ = "SELECT 1 FROM hremployee_idinfo_approval_tran WHERE employeeunkid = '" & intEmployeeId & "' AND isdefault = 1 AND isvoid = 0 "

            If objDo.RecordCount(StrQ) > 0 Then
                blnFlag = True
            End If
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: IsDefaultAssigned; Module Name: " & mstrModuleName)
        Finally
        End Try
        Return blnFlag
    End Function

    Public Function Update(ByVal xDataOpr As clsDataOperation, _
                           ByVal intEmployeeId As Integer, _
                           ByVal intIdTypeId As Integer, _
                           ByVal intIdentityTranId As Integer, _
                           ByVal strColName As String) As Boolean
        Dim strQ As String = ""
        Dim exForce As Exception

        If xDataOpr IsNot Nothing Then
            objDataOperation = xDataOpr
        Else
            objDataOperation = New clsDataOperation
        End If
        objDataOperation.ClearParameters()
        Try
            strQ = "UPDATE hremployee_idinfo_approval_tran SET " & _
                   " " & strColName & " = '" & intIdentityTranId & "' " & _
                   "WHERE isprocessed= 0 and idtypeunkid = @idtypeunkid AND employeeunkid = @employeeunkid "

            'Gajanan [31-May-2020] -- Start
            objDataOperation.AddParameter("@employeeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intEmployeeId)
            objDataOperation.AddParameter("@idtypeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intIdTypeId)
            'Gajanan [31-May-2020] -- End

            Call objDataOperation.ExecNonQuery(strQ)

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            Return True
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: Update; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If xDataOpr Is Nothing Then objDataOperation = Nothing
        End Try
    End Function

    Public Function GetEmployeeIdentityUnkid(ByVal intEmployeeId As Integer, ByVal intIdentyTypeID As Integer) As Integer
        Dim StrQ As String = String.Empty
        Dim dsList As New DataSet
        Dim exForce As Exception
        Try
            Dim objDataOperation As New clsDataOperation

            StrQ = "SELECT " & _
                   "	hremployee_idinfo_approval_tran.employeeunkid " & _
                   "FROM hremployee_idinfo_approval_tran " & _
                   "WHERE employeeunkid = @EmpId AND idtypeunkid = @idtypeunkid " & _
                   "AND hremployee_idinfo_approval_tran.isprocessed = 0 "

            objDataOperation.ClearParameters()
            objDataOperation.AddParameter("@EmpId", SqlDbType.Int, eZeeDataType.INT_SIZE, intEmployeeId)
            objDataOperation.AddParameter("@idtypeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intIdentyTypeID)

            dsList = objDataOperation.ExecQuery(StrQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & " : " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            If dsList.Tables(0).Rows.Count > 0 Then

                'Gajanan [27-May-2019] -- Start              
                'Return dsList.Tables(0).Rows(0)("identitytranunkid")
                Return dsList.Tables(0).Rows(0)("employeeunkid")
                'Gajanan [27-May-2019] -- End


            End If

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "GetEmployeeIdentityUnkid", mstrModuleName)
        End Try
    End Function

    'S.SANDEEP |26-APR-2019| -- START
    Public Function UpdateDeleteDocument(ByVal xDataOperation As clsDataOperation, _
                                         ByVal intEmployeeId As Integer, _
                                         ByVal intTransactionId As Integer, _
                                         ByVal strScanRefIds As String, _
                                         ByVal strFormName As String, _
                                         ByVal strColName As String, _
                                         ByVal blnIsDelete As Boolean) As Boolean
        Dim strQ As String = ""
        Dim exForce As Exception

        If xDataOperation IsNot Nothing Then
            objDataOperation = xDataOperation
        Else
            objDataOperation = New clsDataOperation
            objDataOperation.BindTransaction()
        End If
        objDataOperation.ClearParameters()
        Try
            If strScanRefIds.Trim.Length > 0 Then
                strQ = "UPDATE hrdocuments_tran SET " & strColName & " = '" & intTransactionId & "',isinapproval = 0 "
                If blnIsDelete Then
                    strQ &= ", isactive = 0 "
                Else
                    strQ &= ", isactive = 1 "
                End If
                strQ &= "WHERE employeeunkid = '" & intEmployeeId & "' AND scanattachtranunkid IN (" & strScanRefIds & ") "

                Call objDataOperation.ExecNonQuery(strQ)

                If objDataOperation.ErrorMessage <> "" Then
                    exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                    Throw exForce
                End If
                If xDataOperation Is Nothing Then
                    objDataOperation.ReleaseTransaction(True)
                End If
            End If
            Return True
        Catch ex As Exception
            If xDataOperation Is Nothing Then
                objDataOperation.ReleaseTransaction(False)
            End If
            Throw New Exception(ex.Message & "; Procedure Name: ; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If xDataOperation Is Nothing Then objDataOperation = Nothing
        End Try
    End Function
    'S.SANDEEP |26-APR-2019| -- END

#End Region

End Class


