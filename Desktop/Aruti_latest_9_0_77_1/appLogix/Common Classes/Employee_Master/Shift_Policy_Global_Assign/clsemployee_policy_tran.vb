﻿'************************************************************************************************************************************
'Class Name : clsemployee_policy_tran.vb
'Purpose    :
'Date       :27-Sep-2013
'Written By :Sandeep Sharma
'Modified   :
'************************************************************************************************************************************

Imports eZeeCommonLib
''' <summary>
''' Purpose: 
''' Developer: Sandeep Sharma
''' </summary>
Public Class clsemployee_policy_tran
    Private Shared ReadOnly mstrModuleName As String = "clsemployee_policy_tran"
    Dim objDataOperation As clsDataOperation
    Dim mstrMessage As String = ""

#Region " Private variables "
    Private mintEmppolicytranunkid As Integer
    Private mintEmployeeunkid As Integer
    Private mintPolicyunkid As Integer
    Private mdtEffectivedate As Date
    Private mintUserunkid As Integer
    Private mblnIsvoid As Boolean
    Private mintVoiduserunkid As Integer
    Private mdtVoiddatetime As Date
    Private mstrVoidreason As String = String.Empty
#End Region

#Region " Properties "
'S.SANDEEP [28-May-2018] -- START 
'ISSUE/ENHANCEMENT : {Audit Trails} 
Private mstrFormName As String = String.Empty
Public WriteOnly Property _FormName() As String 
Set(ByVal value As String) 
mstrFormName = value 
End Set 
End Property 
 
Private mstrClientIP As String = ""
Public WriteOnly Property _ClientIP() As String 
Set(ByVal value As String) 
mstrClientIP = value 
End Set 
End Property 
 
Private mstrHostName As String = ""
Public WriteOnly Property _HostName() As String 
Set(ByVal value As String) 
mstrHostName = value 
End Set 
End Property 
 
Private mblnIsWeb As Boolean = False
Public WriteOnly Property _FromWeb() As Boolean 
Set(ByVal value As Boolean) 
mblnIsWeb = value 
End Set 
End Property 
 
    Private mintAuditUserId As Integer = 0
    Public WriteOnly Property _AuditUserId() As Integer
        Set(ByVal value As Integer)
            mintAuditUserId = value
        End Set
    End Property

    Private mintCompanyUnkid As Integer = 0
    Public WriteOnly Property _CompanyUnkid() As Integer
        Set(ByVal value As Integer)
            mintCompanyUnkid = value
        End Set
    End Property
 
Private mdtAuditDate As DateTime = Now
Public WriteOnly Property _AuditDate() As DateTime 
Set(ByVal value As DateTime) 
mdtAuditDate = value 
End Set 
End Property 
 
Private xDataOpr As clsDataOperation = Nothing
Public Property _xDataOpr() As clsDataOperation 
Get 
Return xDataOpr 
End Get 
Set(ByVal value As clsDataOperation) 
xDataOpr = value 
End Set 
End Property 
 
Private mintLoginEmployeeunkid As Integer = 0
Public Property _LoginEmployeeunkid() As Integer 
Get 
Return mintLoginEmployeeunkid 
End Get 
Set(ByVal value As Integer) 
mintLoginEmployeeunkid = value 
End Set 
End Property 
 
Private mintVoidlogingemployeeunkid As Integer=0
Public Property _Voidloginemployeeunkid() As Integer 
Get 
Return mintVoidlogingemployeeunkid 
End Get 
Set(ByVal value As Integer) 
mintVoidlogingemployeeunkid = value 
End Set 
End Property 
'S.SANDEEP [28-May-2018] -- END 
    ''' <summary>
    ''' Purpose: Get Message from Class 
    ''' Modify By: Sandeep Sharma
    ''' </summary>
    Public ReadOnly Property _Message() As String
        Get
            Return mstrMessage
        End Get
    End Property

    ''' <summary>
    ''' Purpose: Get or Set emppolicytranunkid
    ''' Modify By: Sandeep Sharma
    ''' </summary>
    Public Property _Emppolicytranunkid() As Integer
        Get
            Return mintEmppolicytranunkid
        End Get
        Set(ByVal value As Integer)
            mintEmppolicytranunkid = Value
            Call getData()
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set employeeunkid
    ''' Modify By: Sandeep Sharma
    ''' </summary>
    Public Property _Employeeunkid() As Integer
        Get
            Return mintEmployeeunkid
        End Get
        Set(ByVal value As Integer)
            mintEmployeeunkid = Value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set policyunkid
    ''' Modify By: Sandeep Sharma
    ''' </summary>
    Public Property _Policyunkid() As Integer
        Get
            Return mintPolicyunkid
        End Get
        Set(ByVal value As Integer)
            mintPolicyunkid = Value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set effectivedate
    ''' Modify By: Sandeep Sharma
    ''' </summary>
    Public Property _Effectivedate() As Date
        Get
            Return mdtEffectivedate
        End Get
        Set(ByVal value As Date)
            mdtEffectivedate = Value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set userunkid
    ''' Modify By: Sandeep Sharma
    ''' </summary>
    Public Property _Userunkid() As Integer
        Get
            Return mintUserunkid
        End Get
        Set(ByVal value As Integer)
            mintUserunkid = Value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set isvoid
    ''' Modify By: Sandeep Sharma
    ''' </summary>
    Public Property _Isvoid() As Boolean
        Get
            Return mblnIsvoid
        End Get
        Set(ByVal value As Boolean)
            mblnIsvoid = Value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set voiduserunkid
    ''' Modify By: Sandeep Sharma
    ''' </summary>
    Public Property _Voiduserunkid() As Integer
        Get
            Return mintVoiduserunkid
        End Get
        Set(ByVal value As Integer)
            mintVoiduserunkid = Value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set voiddatetime
    ''' Modify By: Sandeep Sharma
    ''' </summary>
    Public Property _Voiddatetime() As Date
        Get
            Return mdtVoiddatetime
        End Get
        Set(ByVal value As Date)
            mdtVoiddatetime = Value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set voidreason
    ''' Modify By: Sandeep Sharma
    ''' </summary>
    Public Property _Voidreason() As String
        Get
            Return mstrVoidreason
        End Get
        Set(ByVal value As String)
            mstrVoidreason = Value
        End Set
    End Property

#End Region

    ''' <summary>
    ''' Modify By: Sandeep Sharma
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>
    Public Sub GetData()
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        objDataOperation = New clsDataOperation

        Try
            StrQ = "SELECT " & _
              "  emppolicytranunkid " & _
              ", employeeunkid " & _
              ", policyunkid " & _
              ", effectivedate " & _
              ", userunkid " & _
              ", isvoid " & _
              ", voiduserunkid " & _
              ", voiddatetime " & _
              ", voidreason " & _
             "FROM hremployee_policy_tran " & _
             "WHERE emppolicytranunkid = @emppolicytranunkid "

            objDataOperation.AddParameter("@emppolicytranunkid", SqlDbType.int, eZeeDataType.INT_SIZE, mintEmppolicyTranUnkId.ToString)

            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            For Each dtRow As DataRow In dsList.Tables(0).Rows
                mintemppolicytranunkid = CInt(dtRow.Item("emppolicytranunkid"))
                mintemployeeunkid = CInt(dtRow.Item("employeeunkid"))
                mintpolicyunkid = CInt(dtRow.Item("policyunkid"))
                mdteffectivedate = dtRow.Item("effectivedate")
                mintuserunkid = CInt(dtRow.Item("userunkid"))
                mblnisvoid = CBool(dtRow.Item("isvoid"))
                mintVoiduserunkid = CInt(dtRow.Item("voiduserunkid"))

                If Not IsDBNull(dtRow.Item("voiddatetime")) Then
                mdtVoiddatetime = dtRow.Item("voiddatetime")
                End If

                mstrVoidreason = dtRow.Item("voidreason").ToString
                Exit For
            Next
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: GetData; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
    End Sub

    ''' <summary>
    ''' Modify By: Sandeep Sharma
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>
    Public Function GetList(ByVal strTableName As String, Optional ByVal blnOnlyActive As Boolean = True) As DataSet
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        objDataOperation = New clsDataOperation

        Try
            strQ = "SELECT " & _
              "  emppolicytranunkid " & _
              ", employeeunkid " & _
              ", policyunkid " & _
              ", effectivedate " & _
              ", userunkid " & _
              ", isvoid " & _
              ", voiduserunkid " & _
              ", voiddatetime " & _
              ", voidreason " & _
             "FROM hremployee_policy_tran "

            If blnOnlyActive Then
                strQ &= " WHERE isvoid = 0 "
            End If

            dsList = objDataOperation.ExecQuery(strQ, strTableName)

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: GetList; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
        Return dsList
    End Function

    ''' <summary>
    ''' Modify By: Sandeep Sharma
    ''' </summary>
    ''' <returns>Boolean</returns>
    ''' <purpose> INSERT INTO Database Table (hremployee_policy_tran) </purpose>
    Public Function Insert(Optional ByVal objDataOpr As clsDataOperation = Nothing) As Boolean 'S.SANDEEP [ 05 DEC 2013 ] -- START -- END

        'Public Function Insert() As Boolean
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        'S.SANDEEP [ 05 DEC 2013 ] -- START
        If objDataOpr Is Nothing Then
        objDataOperation = New clsDataOperation
            objDataOperation.BindTransaction()
        Else
            objDataOperation = objDataOpr
            objDataOperation.ClearParameters()
        End If
        'S.SANDEEP [ 05 DEC 2013 ] -- END
        Try
            objDataOperation.AddParameter("@employeeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintEmployeeunkid.ToString)
            objDataOperation.AddParameter("@policyunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintPolicyunkid.ToString)
            objDataOperation.AddParameter("@effectivedate", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, mdtEffectivedate)
            objDataOperation.AddParameter("@userunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintUserunkid.ToString)
            objDataOperation.AddParameter("@isvoid", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsvoid.ToString)
            objDataOperation.AddParameter("@voiduserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintVoiduserunkid.ToString)
            If mdtVoiddatetime = Nothing Then
                objDataOperation.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, DBNull.Value)
            Else
                objDataOperation.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, mdtVoiddatetime)
            End If
            objDataOperation.AddParameter("@voidreason", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrVoidreason.ToString)

            strQ = "INSERT INTO hremployee_policy_tran ( " & _
                      "  employeeunkid " & _
                      ", policyunkid " & _
                      ", effectivedate " & _
                      ", userunkid " & _
                      ", isvoid " & _
                      ", voiduserunkid " & _
                      ", voiddatetime " & _
                      ", voidreason" & _
                   ") VALUES (" & _
                      "  @employeeunkid " & _
                      ", @policyunkid " & _
                      ", @effectivedate " & _
                      ", @userunkid " & _
                      ", @isvoid " & _
                      ", @voiduserunkid " & _
                      ", @voiddatetime " & _
                      ", @voidreason" & _
                   "); SELECT @@identity"

            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            mintEmppolicytranunkid = dsList.Tables(0).Rows(0).Item(0)

'S.SANDEEP [28-May-2018] -- START
'ISSUE/ENHANCEMENT : {Audit Trails} 
Dim objCommonATLog AS New clsCommonATLog 
objCommonATLog._FormName = mstrFormName 
objCommonATLog._LoginEmployeeUnkid = mintLoginEmployeeunkid 
            objCommonATLog._ClientIP = mstrClientIP
            objCommonATLog._HostName = mstrHostName
            objCommonATLog._FromWeb = mblnIsWeb
            objCommonATLog._AuditUserId = mintAuditUserId
objCommonATLog._CompanyUnkid = mintCompanyUnkid
            objCommonATLog._AuditDate = mdtAuditDate
            'S.SANDEEP [28-May-2018] -- END

            If objCommonATLog.Insert_TranAtLog(objDataOperation, "hremployee_master", "employeeunkid", mintEmployeeunkid, "hremployee_policy_tran", "emppolicytranunkid", mintEmppolicytranunkid, 2, 1) = False Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            'S.SANDEEP [28-May-2018] -- START
            'ISSUE/ENHANCEMENT : {Audit Trails} 
            objCommonATLog = Nothing
            'S.SANDEEP [28-May-2018] -- END


            If objDataOpr Is Nothing Then
            objDataOperation.ReleaseTransaction(True)
            End If
            'S.SANDEEP [ 05 DEC 2013 ] -- END

            Return True
        Catch ex As Exception
            objDataOperation.ReleaseTransaction(False)
            Throw New Exception(ex.Message & "; Procedure Name: Insert; Module Name: " & mstrModuleName)
            Return False
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            'S.SANDEEP [ 05 DEC 2013 ] -- START
            'objDataOperation = Nothing
            If objDataOpr Is Nothing Then
            objDataOperation = Nothing
            End If
            'S.SANDEEP [ 05 DEC 2013 ] -- END
        End Try
    End Function

    ''' <summary>
    ''' Modify By: Sandeep Sharma
    ''' </summary>
    ''' <returns>Boolean</returns>
    ''' <purpose> Update Database Table (hremployee_policy_tran) </purpose>
    Public Function Update() As Boolean
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        objDataOperation = New clsDataOperation

        Try
            objDataOperation.BindTransaction()
            objDataOperation.AddParameter("@emppolicytranunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintEmppolicytranunkid.ToString)
            objDataOperation.AddParameter("@employeeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintEmployeeunkid.ToString)
            objDataOperation.AddParameter("@policyunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintPolicyunkid.ToString)
            objDataOperation.AddParameter("@effectivedate", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, mdtEffectivedate)
            objDataOperation.AddParameter("@userunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintUserunkid.ToString)
            objDataOperation.AddParameter("@isvoid", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsvoid.ToString)
            objDataOperation.AddParameter("@voiduserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintVoiduserunkid.ToString)
            If mdtVoiddatetime <> Nothing Then
                objDataOperation.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, mdtVoiddatetime)
            Else
                objDataOperation.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, DBNull.Value)
            End If
            objDataOperation.AddParameter("@voidreason", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrVoidreason.ToString)

            strQ = "UPDATE hremployee_policy_tran SET " & _
              "  employeeunkid = @employeeunkid" & _
              ", policyunkid = @policyunkid" & _
              ", effectivedate = @effectivedate" & _
              ", userunkid = @userunkid" & _
              ", isvoid = @isvoid" & _
              ", voiduserunkid = @voiduserunkid" & _
              ", voiddatetime = @voiddatetime" & _
              ", voidreason = @voidreason " & _
            "WHERE emppolicytranunkid = @emppolicytranunkid "

            Call objDataOperation.ExecNonQuery(strQ)

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            'S.SANDEEP [28-May-2018] -- START
            'ISSUE/ENHANCEMENT : {Audit Trails} 
            Dim objCommonATLog As New clsCommonATLog
            objCommonATLog._FormName = mstrFormName
            objCommonATLog._LoginEmployeeUnkid = mintLoginEmployeeunkid
            objCommonATLog._ClientIP = mstrClientIP
            objCommonATLog._HostName = mstrHostName
            objCommonATLog._FromWeb = mblnIsWeb
            objCommonATLog._AuditUserId = mintAuditUserId
objCommonATLog._CompanyUnkid = mintCompanyUnkid
            objCommonATLog._AuditDate = mdtAuditDate
            'S.SANDEEP [28-May-2018] -- END

            If objCommonATLog.Insert_TranAtLog(objDataOperation, "hremployee_master", "employeeunkid", mintEmployeeunkid, "hremployee_policy_tran", "emppolicytranunkid", mintEmppolicytranunkid, 2, 2) = False Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            'S.SANDEEP [28-May-2018] -- START
            'ISSUE/ENHANCEMENT : {Audit Trails} 
            objCommonATLog = Nothing
            'S.SANDEEP [28-May-2018] -- END


            objDataOperation.ReleaseTransaction(True)
            Return True
        Catch ex As Exception
            objDataOperation.ReleaseTransaction(False)
            Throw New Exception(ex.Message & "; Procedure Name: Update; Module Name: " & mstrModuleName)
            Return False
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
    End Function

    ''' <summary>
    ''' Modify By: Sandeep Sharma
    ''' </summary>
    ''' <returns>Boolean</returns>
    ''' <purpose> Delete Database Table (hremployee_policy_tran) </purpose>
    Public Function Delete(ByVal intUnkid As Integer) As Boolean
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        objDataOperation = New clsDataOperation

        Try
            objDataOperation.BindTransaction()
            strQ = "UPDATE hremployee_policy_tran SET " & _
                   "  isvoid = @isvoid" & _
                   ", voiduserunkid = @voiduserunkid" & _
                   ", voiddatetime = @voiddatetime" & _
                   ", voidreason = @voidreason " & _
                   "WHERE emppolicytranunkid = @emppolicytranunkid "

            objDataOperation.AddParameter("@emppolicytranunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intUnkid)
            objDataOperation.AddParameter("@isvoid", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsvoid.ToString)
            objDataOperation.AddParameter("@voiduserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintVoiduserunkid.ToString)
            objDataOperation.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, mdtVoiddatetime)
            objDataOperation.AddParameter("@voidreason", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrVoidreason.ToString)

            Call objDataOperation.ExecNonQuery(strQ)

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            'S.SANDEEP [ 31 OCT 2013 ] -- START
            Dim iReturnDt As Date = Nothing
            iReturnDt = GetCurrPolicyEndDate(mintEmployeeunkid, mintPolicyunkid, mdtEffectivedate, objDataOperation)
            If iReturnDt <> Nothing Then
                strQ = " UPDATE tnalogin_summary SET isabsentprocess = 0 WHERE employeeunkid = '" & mintEmployeeunkid & "' AND policyunkid = '" & mintPolicyunkid & "' AND CONVERT(CHAR(8),login_date,112) BETWEEN '" & eZeeDate.convertDate(mdtEffectivedate) & "' AND '" & eZeeDate.convertDate(iReturnDt) & "' "
            ElseIf iReturnDt = Nothing Then
                strQ = " UPDATE tnalogin_summary SET isabsentprocess = 0 WHERE employeeunkid = '" & mintEmployeeunkid & "' AND policyunkid = '" & mintPolicyunkid & "' AND CONVERT(CHAR(8),login_date,112) >= '" & eZeeDate.convertDate(mdtEffectivedate) & "' "
            End If

            Call objDataOperation.ExecNonQuery(strQ)

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If
            'S.SANDEEP [ 31 OCT 2013 ] -- END

            'S.SANDEEP [28-May-2018] -- START
            'ISSUE/ENHANCEMENT : {Audit Trails} 
            Dim objCommonATLog As New clsCommonATLog
            objCommonATLog._FormName = mstrFormName
            objCommonATLog._LoginEmployeeUnkid = mintLoginEmployeeunkid
            objCommonATLog._ClientIP = mstrClientIP
            objCommonATLog._HostName = mstrHostName
            objCommonATLog._FromWeb = mblnIsWeb
            objCommonATLog._AuditUserId = mintAuditUserId
objCommonATLog._CompanyUnkid = mintCompanyUnkid
            objCommonATLog._AuditDate = mdtAuditDate
            'S.SANDEEP [28-May-2018] -- END

            If objCommonATLog.Insert_TranAtLog(objDataOperation, "hremployee_master", "employeeunkid", mintEmployeeunkid, "hremployee_policy_tran", "emppolicytranunkid", intUnkid, 2, 3) = False Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            'S.SANDEEP [28-May-2018] -- START
            'ISSUE/ENHANCEMENT : {Audit Trails} 
            objCommonATLog = Nothing
            'S.SANDEEP [28-May-2018] -- END


            objDataOperation.ReleaseTransaction(True)

            Return True
        Catch ex As Exception
            objDataOperation.ReleaseTransaction(False)
            Throw New Exception(ex.Message & "; Procedure Name: Update; Module Name: " & mstrModuleName)
            Return False
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
    End Function

    ''' <summary>
    ''' Modify By: Sandeep Sharma
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>
    Public Function isUsed(ByVal iEmpId As Integer, ByVal iDate As DateTime, ByRef iMessage As String, ByVal strDatabaseName As String) As String
        'Sohail (21 Aug 2015) - [strDatabaseName]

        Dim dsList As DataSet = Nothing
        Dim StrQ As String = ""
        Dim exForce As Exception
        objDataOperation = New clsDataOperation

        Try
            StrQ = "SELECT 1 FROM hremployee_policy_tran WHERE isvoid = 0 AND employeeunkid = '" & iEmpId & "' "

            If objDataOperation.RecordCount(StrQ) = 1 Then
                iMessage = Language.getMessage(mstrModuleName, 1, "Sorry, you cannot delete this policy." & vbCrLf & _
                                                   "Reasons : " & vbCrLf & _
                                                   "1]. There is only one policy assigned to the employee." & vbCrLf & _
                                                   "2]. Period is already closed." & vbCrLf & _
                                                   "3]. There is a transaction in Time and Attendance for the selected date." & vbCrLf & _
                                                   "Due to this checked employee will be skipped from the operation.")
                GoTo iMsg
            End If

            'S.SANDEEP [ 31 OCT 2013 ] -- START
            If ArtLic._Object.ModuleStatus(ArutiModule.ArutiModule.Payroll_Management) OrElse ConfigParameter._Object._IsArutiDemo Then
                Dim objMaster As New clsMasterData
                Dim iPeriod As Integer = 0

                'Pinkal (20-Jan-2014) -- Start
                'Enhancement : Oman Changes
                'iPeriod = objMaster.getCurrentPeriodID(enModuleReference.Payroll, iDate, , FinancialYear._Object._YearUnkid, True)
                iPeriod = objMaster.getCurrentTnAPeriodID(enModuleReference.Payroll, iDate)
                'Pinkal (20-Jan-2014) -- End


                If iPeriod > 0 Then
                    Dim objPrd As New clscommom_period_Tran
                    'Sohail (21 Aug 2015) -- Start
                    'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
                    'objPrd._Periodunkid = iPeriod
                    objPrd._Periodunkid(strDatabaseName) = iPeriod
                    'Sohail (21 Aug 2015) -- End
                    If objPrd._Statusid = 2 Then
                        iMessage = Language.getMessage(mstrModuleName, 1, "Sorry, you cannot delete this policy." & vbCrLf & _
                                                       "Reasons : " & vbCrLf & _
                                                       "1]. There is only one policy assigned to the employee." & vbCrLf & _
                                                       "2]. Period is already closed." & vbCrLf & _
                                                       "3]. There is a transaction in Time and Attendance for the selected date." & vbCrLf & _
                                                       "Due to this checked employee will be skipped from the operation.")
                        GoTo iMsg
                    End If

                    'S.SANDEEP [ 31 OCT 2013 ] -- START
                    Dim objLeaveTran As New clsTnALeaveTran


                    'Pinkal (03-Jan-2014) -- Start
                    'Enhancement : Oman Changes

                    'Pinkal (03-Jan-2014) -- End

                    'If objLeaveTran.IsPayrollProcessDone(objPrd._Periodunkid, iEmpId.ToString(), objPrd._End_Date.Date) Then
                    '    iMessage = Language.getMessage(mstrModuleName, 3, "Sorry, you cannot remove assiged policy from checked employee(s).Reason:Payroll Process already done for checked employee(s) for last date of current open period.")
                    '    GoTo iMsg
                    'End If

                    Dim mdtTnADate As DateTime = Nothing
                    If objPrd._TnA_EndDate.Date < iDate.Date Then
                        mdtTnADate = iDate.Date
                    Else
                        mdtTnADate = objPrd._TnA_EndDate.Date
                    End If

                    'Sohail (21 Aug 2015) -- Start
                    'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
                    'If objLeaveTran.IsPayrollProcessDone(objPrd._Periodunkid, iEmpId.ToString(), mdtTnADate.Date, enModuleReference.TnA) Then
                    If objLeaveTran.IsPayrollProcessDone(objPrd._Periodunkid(strDatabaseName), iEmpId.ToString(), mdtTnADate.Date, enModuleReference.TnA) Then
                        'Sohail (21 Aug 2015) -- End
                        iMessage = Language.getMessage(mstrModuleName, 3, "Sorry, you cannot remove assiged policy from checked employee(s).Reason:Payroll Process already done for checked employee(s) for last date of current open period.")
                        GoTo iMsg
                    End If

                    'S.SANDEEP [ 31 OCT 2013 ] -- END

                    objPrd = Nothing
                End If
                objMaster = Nothing
            End If
            'S.SANDEEP [ 31 OCT 2013 ] -- END




            'S.SANDEEP [ 31 OCT 2013 ] -- START
            'StrQ = "SELECT 1 FROM tnalogin_summary WHERE employeeunkid = '" & iEmpId & "' AND CONVERT(CHAR(8),login_date,112) = '" & eZeeDate.convertDate(iDate).ToString & "' "

            'dsList = objDataOperation.ExecQuery(StrQ, "List")

            'If objDataOperation.ErrorMessage <> "" Then
            '    exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
            '    Throw exForce
            'End If

            'If dsList.Tables("List").Rows.Count > 0 Then
            '    iMessage = Language.getMessage(mstrModuleName, 1, "Sorry, you cannot delete this policy." & vbCrLf & _
            '                                       "Reasons : " & vbCrLf & _
            '                                       "1]. There is only one policy assigned to the employee." & vbCrLf & _
            '                                       "2]. Period is already closed." & vbCrLf & _
            '                                       "3]. There is a transaction in Time and Attendance for the selected date." & vbCrLf & _
            '                                       "Due to this checked employee will be skipped from the operation.")
            '    GoTo iMsg
            'End If
            'S.SANDEEP [ 31 OCT 2013 ] -- END




iMsg:       Return iMessage

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: isUsed; Module Name: " & mstrModuleName)
            Return iMessage
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
    End Function

    ''' <summary>
    ''' Modify By: Sandeep Sharma
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>
    Public Function isExist(ByVal iPolicyId As Integer, ByVal iDate As DateTime, ByVal iEmployeeId As Integer, Optional ByVal intUnkid As Integer = -1) As Boolean
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        objDataOperation = New clsDataOperation

        Try
            strQ = "SELECT " & _
                   "  emppolicytranunkid " & _
                   ", employeeunkid " & _
                   ", policyunkid " & _
                   ", effectivedate " & _
                   ", userunkid " & _
                   ", isvoid " & _
                   ", voiduserunkid " & _
                   ", voiddatetime " & _
                   ", voidreason " & _
                   "FROM hremployee_policy_tran " & _
                   "WHERE employeeunkid = @employeeunkid " & _
                   "AND policyunkid = @policyunkid " & _
                   "AND CONVERT(CHAR(8),effectivedate,112) >= @effectivedate " & _
                   "AND isvoid = 0 "

            If intUnkid > 0 Then
                strQ &= " AND emppolicytranunkid <> @emppolicytranunkid"
            End If

            objDataOperation.AddParameter("@policyunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, iPolicyId)
            objDataOperation.AddParameter("@effectivedate", SqlDbType.NVarChar, eZeeDataType.DATETIME_SIZE, eZeeDate.convertDate(iDate))
            objDataOperation.AddParameter("@employeeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, iEmployeeId)
            objDataOperation.AddParameter("@emppolicytranunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intUnkid)

            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            Return dsList.Tables(0).Rows.Count > 0
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: isExist; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
    End Function

    ''' <summary>
    ''' Modify By: Sandeep Sharma
    ''' </summary>
    ''' <purpose> Get List of Employee Whome Policy is Assigned </purpose>
    Public Function Assigned_EmpIds() As String
        Dim StrQ As String = String.Empty
        Dim dsList As New DataSet
        Dim iValue As String = String.Empty
        Try
            objDataOperation = New clsDataOperation

            StrQ = "SELECT ISNULL(STUFF((SELECT DISTINCT ',' +  CAST(employeeunkid AS NVARCHAR(50)) FROM hremployee_policy_tran WHERE isvoid = 0 FOR XML PATH('')),1,1,''),'') AS iData "

            dsList = objDataOperation.ExecQuery(StrQ, "List")

            If dsList.Tables(0).Rows.Count > 0 Then
                iValue = dsList.Tables(0).Rows(0).Item("iData")
            End If
            Return iValue
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "Assigned_EmpIds", mstrModuleName)
            Return iValue
        Finally
        End Try
    End Function

    'Shani(24-Aug-2015) -- Start
    'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
    'To Change GetEmployeeShift_PolicyList Method set Active Employee Query wise.
    'Public Function Get_List(ByVal sListName As String, ByVal dStDate As DateTime, ByVal dEdDate As DateTime, _
    '                         Optional ByVal iEmpId As Integer = 0, _
    '                         Optional ByVal iPolicyId As Integer = 0, _
    '                         Optional ByVal mStrFilter As String = "") As DataTable
    Public Function Get_List(ByVal xDatabaseName As String, _
                             ByVal xUserUnkid As Integer, _
                             ByVal xYearUnkid As Integer, _
                             ByVal xCompanyUnkid As Integer, _
                             ByVal xUserModeSetting As String, _
                             ByVal xIncludeIn_ActiveEmployee As Boolean, _
                             ByVal sListName As String, _
                             ByVal dStDate As DateTime, _
                             ByVal dEdDate As DateTime, _
                             Optional ByVal iEmpId As Integer = 0, _
                             Optional ByVal iPolicyId As Integer = 0, _
                             Optional ByVal mStrFilter As String = "") As DataTable
        'Shani(24-Aug-2015) -- End
        Dim StrQ As String = String.Empty
        Dim exForce As Exception
        Dim dsList As New DataSet : Dim dTable As DataTable = Nothing
        Try
            objDataOperation = New clsDataOperation

            'Shani(24-Aug-2015) -- Start
            'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
            'To Change GetEmployeeShift_PolicyList Method set Active Employee Query wise.
            'StrQ = "SELECT " & _
            '       "	 CAST(0 AS BIT) AS ischeck " & _
            '       "	,employeecode AS ecode " & _
            '       "	,firstname+' '+surname AS ename " & _
            '       "	,CONVERT(CHAR(8),effectivedate,112) AS effectivedate " & _
            '       "	,tnapolicy_master.policyname AS policyname " & _
            '       "	,hremployee_policy_tran.emppolicytranunkid " & _
            '       "	,hremployee_master.employeeunkid " & _
            '       "    ,hremployee_policy_tran.policyunkid " & _
            '       "FROM hremployee_policy_tran " & _
            '       "	JOIN tnapolicy_master ON hremployee_policy_tran.policyunkid = tnapolicy_master.policyunkid " & _
            '       "	JOIN hremployee_master ON hremployee_policy_tran.employeeunkid = hremployee_master.employeeunkid " & _
            '       "WHERE hremployee_policy_tran.isvoid = 0 "

            Dim xDateJoinQry, xDateFilterQry, xUACQry, xUACFiltrQry, xAdvanceJoinQry As String
            xDateJoinQry = "" : xDateFilterQry = "" : xUACQry = "" : xUACFiltrQry = "" : xAdvanceJoinQry = ""
            Call GetDatesFilterString(xDateJoinQry, xDateFilterQry, dStDate, dEdDate, , , xDatabaseName)
            Call NewAccessLevelFilterString(xUACQry, xUACFiltrQry, dEdDate, , xDatabaseName, xUserUnkid, xCompanyUnkid, xYearUnkid, xUserModeSetting)
            Call GetAdvanceFilterQry(xAdvanceJoinQry, dEdDate, xDatabaseName)

            StrQ = "SELECT " & _
                   "	 CAST(0 AS BIT) AS ischeck " & _
                   "	,employeecode AS ecode " & _
                   "	,firstname+' '+surname AS ename " & _
                   "	,CONVERT(CHAR(8),effectivedate,112) AS effectivedate " & _
                   "	,tnapolicy_master.policyname AS policyname " & _
                   "	,hremployee_policy_tran.emppolicytranunkid " & _
                   "	,hremployee_master.employeeunkid " & _
                   "    ,hremployee_policy_tran.policyunkid " & _
                   "FROM hremployee_policy_tran " & _
                   "	JOIN tnapolicy_master ON hremployee_policy_tran.policyunkid = tnapolicy_master.policyunkid " & _
                    "	JOIN hremployee_master ON hremployee_policy_tran.employeeunkid = hremployee_master.employeeunkid "

            If xDateJoinQry.Trim.Length > 0 Then
                StrQ &= xDateJoinQry
            End If

            'S.SANDEEP [15 NOV 2016] -- START
            If xUACQry.Trim.Length > 0 Then
                StrQ &= xUACQry
            End If
            'S.SANDEEP [15 NOV 2016] -- END


            'Pinkal (06-Jan-2016) -- Start
            'Enhancement - Working on Changes in SS for Leave Module.
            If xAdvanceJoinQry.Trim.Length > 0 Then
                StrQ &= xAdvanceJoinQry
            End If
            'Pinkal (06-Jan-2016) -- End


            StrQ &= "WHERE hremployee_policy_tran.isvoid = 0 AND hremployee_master.isapproved = 1"
            'Shani(24-Aug-2015) -- End

            'Pinkal (03-Jan-2014) -- Start
            'Enhancement : Oman Changes

            'AND CONVERT(CHAR(8),effectivedate,112) BETWEEN '" & eZeeDate.convertDate(dStDate) & "' AND '" & eZeeDate.convertDate(dEdDate) & "' "

            If dStDate <> Nothing Then

                'Shani(24-Aug-2015) -- Start
                'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
                'To Change GetEmployeeShift_PolicyList Method set Active Employee Query wise.
                'StrQ &= " AND CONVERT(CHAR(8),effectivedate,112) >= '" & eZeeDate.convertDate(dStDate) & "'"
                StrQ &= " AND CONVERT(CHAR(8),hremployee_policy_tran.effectivedate,112) >= '" & eZeeDate.convertDate(dStDate) & "'"
                'Shani(24-Aug-2015) -- End
            End If

            If dEdDate <> Nothing Then
                'Shani(24-Aug-2015) -- Start
                'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
                'To Change GetEmployeeShift_PolicyList Method set Active Employee Query wise.
                'StrQ &= " AND CONVERT(CHAR(8),effectivedate,112) <= '" & eZeeDate.convertDate(dEdDate) & "'"
                StrQ &= " AND CONVERT(CHAR(8),hremployee_policy_tran.effectivedate,112) <= '" & eZeeDate.convertDate(dEdDate) & "'"
            End If

            'Pinkal (03-Jan-2014) -- End

            If iEmpId > 0 Then
                StrQ &= "AND hremployee_master.employeeunkid = '" & iEmpId & "' "
            End If

            If iPolicyId > 0 Then
                StrQ &= "AND tnapolicy_master.policyunkid = '" & iPolicyId & "' "
            End If

            If mStrFilter.Trim.Length > 0 Then
                StrQ &= " AND " & mStrFilter
            End If

            'Shani(24-Aug-2015) -- Start
            'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
            'To Change GetEmployeeShift_PolicyList Method set Active Employee Query wise.
            'If dStDate <> Nothing AndAlso dEdDate <> Nothing Then
            '    StrQ &= " AND CONVERT(CHAR(8),effectivedate,112) BETWEEN '" & eZeeDate.convertDate(dStDate) & "' AND '" & eZeeDate.convertDate(dEdDate) & "' "
            'End If
            'Shani(24-Aug-2015) -- End

            'Shani(24-Aug-2015) -- Start
            'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
            'To Change GetEmployeeShift_PolicyList Method set Active Employee Query wise.
            If xIncludeIn_ActiveEmployee = False Then
                If xDateFilterQry.Trim.Length > 0 Then
                    StrQ &= xDateFilterQry
                End If
            End If
            
            'S.SANDEEP [15 NOV 2016] -- START
            'If xUACFiltrQry.Trim.Length > 0 Then
            '    StrQ &= " AND " & xUACFiltrQry
            'End If
            'S.SANDEEP [15 NOV 2016] -- END
            
            'Shani(24-Aug-2015) -- End


            'Pinkal (16-Nov-2021)-- Start
            'Enhancement :  Zuri want to sorting by effective date descending.
            If iEmpId > 0 Then
                StrQ &= " ORDER BY CONVERT(CHAR(8),effectivedate,112) DESC "
            Else
            StrQ &= " ORDER BY firstname+' '+surname "
            End If
            'Pinkal (16-Nov-2021)-- End

            dsList = objDataOperation.ExecQuery(StrQ, IIf(sListName = "", "List", sListName))

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & " : " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            dTable = dsList.Tables(0).Clone
            dTable.Rows.Clear()

            For Each dRow As DataRow In dsList.Tables(0).Rows
                dRow.Item("effectivedate") = eZeeDate.convertDate(dRow.Item("effectivedate").ToString).ToShortDateString
                dTable.ImportRow(dRow)
            Next

            Return dTable

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "Get_List", mstrModuleName)
            Return Nothing
        Finally
        End Try
    End Function

Public Function GetList(ByVal iEmpId As Integer) As DataTable
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception
        Dim dTable As DataTable = Nothing
        objDataOperation = New clsDataOperation

        Try
            strQ = "SELECT " & _
                   "  hremployee_policy_tran.emppolicytranunkid " & _
                   ", hremployee_policy_tran.policyunkid " & _
                   ", hremployee_policy_tran.effectivedate " & _
                   ", CONVERT(CHAR(8),hremployee_policy_tran.effectivedate,112) AS effdate " & _
                   ",tnapolicy_master.policyname AS policyname " & _
                   "FROM hremployee_policy_tran " & _
                   " JOIN tnapolicy_master ON hremployee_policy_tran.policyunkid = tnapolicy_master.policyunkid " & _
                   "WHERE hremployee_policy_tran.isvoid = 0 " & _
                   "AND hremployee_policy_tran.employeeunkid = '" & iEmpId & "' "


            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If


            dTable = dsList.Tables(0).Clone
            dTable.Rows.Clear()

            For Each dRow As DataRow In dsList.Tables(0).Rows
                dRow.Item("effdate") = eZeeDate.convertDate(dRow.Item("effdate").ToString).ToShortDateString
                dTable.ImportRow(dRow)
            Next

            Return dTable

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: GetList; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try

    End Function


    Public Function GetPolicyTranID(ByVal Effectivedate As DateTime, ByVal intEmployeeID As Integer, Optional ByVal objDooperation As clsDataOperation = Nothing) As Integer
        'Pinkal (25-Jan-2018) -- 'Bug - 0001937 Error when recalculating timing Description: Error when recalculating timing and importing device attendance.[Optional ByVal objDooperation As clsDataOperation = Nothing]
        Dim mintPolicyTranId As Integer = -1
        Dim objDataOperation As clsDataOperation

        'Pinkal (25-Jan-2018) -- Start
        'Bug - 0001937 Error when recalculating timing Description: Error when recalculating timing and importing device attendance.
        If objDooperation Is Nothing Then
        objDataOperation = New clsDataOperation
        Else
            objDataOperation = objDooperation
        End If
        'Pinkal (25-Jan-2018) -- End
        

        Dim strQ As String = ""
        Dim exForce As Exception
        Dim dsList As DataSet = Nothing
        Try
            strQ = "SELECT " & _
                       " top 1 " & _
                       "  emppolicytranunkid " & _
                       ", employeeunkid " & _
                       ", policyunkid " & _
                       ", effectivedate " & _
                       ", userunkid " & _
                       ", isvoid " & _
                       ", voiduserunkid " & _
                       ", voiddatetime " & _
                       ", voidreason " & _
                       "FROM hremployee_policy_tran " & _
                       "WHERE employeeunkid = @employeeunkid " & _
                       "AND CONVERT(CHAR(8),effectivedate,112) <= @effectivedate " & _
                       "AND isvoid = 0 ORder by effectivedate desc "

            objDataOperation.ClearParameters()
            objDataOperation.AddParameter("@effectivedate", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, eZeeDate.convertDate(Effectivedate))
            objDataOperation.AddParameter("@employeeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intEmployeeID)

            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            If dsList IsNot Nothing AndAlso dsList.Tables(0).Rows.Count > 0 Then
                mintPolicyTranId = CInt(dsList.Tables(0).Rows(0)("policyunkid"))
            End If

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "GetPolicyTranID", mstrModuleName)
        End Try
        Return mintPolicyTranId
    End Function


    'S.SANDEEP [ 31 OCT 2013 ] -- START
    Public Function GetCurrPolicyEndDate(ByVal iEmployeeId As Integer, ByVal iPolicyId As Integer, ByVal iDate As DateTime, ByVal objDataOperation As clsDataOperation) As Date
        Dim iRetDate As Date = Nothing
        Dim StrQ As String = String.Empty
        Dim exForce As Exception
        Dim dsList As New DataSet
        Try
            StrQ = "SELECT TOP 1 login_date FROM tnalogin_summary WHERE employeeunkid = '" & iEmployeeId & "' AND CONVERT(CHAR(8),login_date, 112) > '" & eZeeDate.convertDate(iDate).ToString & "' AND policyunkid <> '" & iPolicyId & "'  ORDER BY login_date ASC "

            dsList = objDataOperation.ExecQuery(StrQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & " : " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            If dsList.Tables("List").Rows.Count > 0 Then
                iRetDate = CDate(dsList.Tables("List").Rows(0).Item("login_date")).Date.AddDays(-1)
            End If

            Return iRetDate
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "GetCurrShiftEndDate", mstrModuleName)
            Return Nothing
        Finally
        End Try
    End Function
    'S.SANDEEP [ 31 OCT 2013 ] -- END

    'S.SANDEEP [ 04 DEC 2013 ] -- START
    Public Function isExist(ByVal iDate As Date, ByVal iEmployeeId As Integer) As Integer
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        objDataOperation = New clsDataOperation

        Try
            strQ = "SELECT " & _
                   "  emppolicytranunkid " & _
                   "FROM hremployee_policy_tran " & _
                   "WHERE employeeunkid = @employeeunkid " & _
                   "AND CONVERT(CHAR(8),effectivedate,112) = @effectivedate " & _
                   "AND isvoid = 0 "

            objDataOperation.AddParameter("@effectivedate", SqlDbType.NVarChar, eZeeDataType.DATETIME_SIZE, eZeeDate.convertDate(iDate))
            objDataOperation.AddParameter("@employeeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, iEmployeeId)

            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

             'S.SANDEEP [ 05 DEC 2013 ] -- START
            If dsList.Tables(0).Rows.Count > 0 Then
                Return dsList.Tables(0).Rows(0).Item("emppolicytranunkid")
            Else
                Return 0
            End If
            'S.SANDEEP [ 05 DEC 2013 ] -- END

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: isExist; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
    End Function
    'S.SANDEEP [ 04 DEC 2013 ] -- END


	'<Language> This Auto Generated Text Please Do Not Modify it.
#Region " Language & UI Settings "
	Public Shared Sub SetMessages()
		Try
			Language.setMessage(mstrModuleName, 1, "Sorry, you cannot delete this policy." & vbCrLf & _
                                                   "Reasons : " & vbCrLf & _
                                                   "1]. There is only one policy assigned to the employee." & vbCrLf & _
                                                   "2]. Period is already closed." & vbCrLf & _
                                                   "3]. There is a transaction in Time and Attendance for the selected date." & vbCrLf & _
                                                   "Due to this checked employee will be skipped from the operation.")
			Language.setMessage(mstrModuleName, 3, "Sorry, you cannot remove assiged policy from checked employee(s).Reason:Payroll Process already done for checked employee(s) for last date of current open period.")
			

		Catch Ex As Exception
			DisplayError.Show("-1", ex.Message, "SetMessages", mstrModuleName)
		End Try
	End Sub
#End Region 'Language & UI Settings
	'</Language>
End Class
