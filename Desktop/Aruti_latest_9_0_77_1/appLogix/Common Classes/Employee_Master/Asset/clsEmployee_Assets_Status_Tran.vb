﻿'************************************************************************************************************************************
'Class Name : clsEmployee_Assets_Status_Tran.vb
'Purpose    :
'Date       :06/01/2012
'Written By :Sohail
'Modified   :
'************************************************************************************************************************************

Imports eZeeCommonLib
''' <summary>
''' Purpose: 
''' Developer: Sohail
''' </summary>
Public Class clsEmployee_Assets_Status_Tran
    Private Const mstrModuleName = "clsemployee_assets_status_Tran"
    Dim objDataOperation As clsDataOperation
    Dim mstrMessage As String = ""

#Region " Private variables "
    Private mintEmployeeassetstatustranunkid As Integer
    Private mintAssetstranunkid As Integer
    Private mdtStatus_Date As Date
    Private mintStatusunkid As Integer
    Private mstrRemark As String = String.Empty
    Private mintUserunkid As Integer
    Private mblnIsvoid As Boolean
    Private mdtVoiddatetime As Date
    Private mintVoiduserunkid As Integer
#End Region

#Region " Properties "
'S.SANDEEP [28-May-2018] -- START 
'ISSUE/ENHANCEMENT : {Audit Trails} 
Private mstrFormName As String = String.Empty
Public WriteOnly Property _FormName() As String 
Set(ByVal value As String) 
mstrFormName = value 
End Set 
End Property 
 
Private mstrClientIP As String = ""
Public WriteOnly Property _ClientIP() As String 
Set(ByVal value As String) 
mstrClientIP = value 
End Set 
End Property 
 
Private mstrHostName As String = ""
Public WriteOnly Property _HostName() As String 
Set(ByVal value As String) 
mstrHostName = value 
End Set 
End Property 
 
Private mblnIsWeb As Boolean = False
Public WriteOnly Property _FromWeb() As Boolean 
Set(ByVal value As Boolean) 
mblnIsWeb = value 
End Set 
End Property 
 
    Private mintAuditUserId As Integer = 0
    Public WriteOnly Property _AuditUserId() As Integer
        Set(ByVal value As Integer)
            mintAuditUserId = value
        End Set
    End Property

    Private mintCompanyUnkid As Integer = 0
    Public WriteOnly Property _CompanyUnkid() As Integer
        Set(ByVal value As Integer)
            mintCompanyUnkid = value
        End Set
    End Property
 
Private mdtAuditDate As DateTime = Now
Public WriteOnly Property _AuditDate() As DateTime 
Set(ByVal value As DateTime) 
mdtAuditDate = value 
End Set 
End Property 
 
Private xDataOpr As clsDataOperation = Nothing
Public Property _xDataOpr() As clsDataOperation 
Get 
Return xDataOpr 
End Get 
Set(ByVal value As clsDataOperation) 
xDataOpr = value 
End Set 
End Property 
 
Private mintLoginEmployeeunkid As Integer = 0
Public Property _LoginEmployeeunkid() As Integer 
Get 
Return mintLoginEmployeeunkid 
End Get 
Set(ByVal value As Integer) 
mintLoginEmployeeunkid = value 
End Set 
End Property 
 
Private mintVoidlogingemployeeunkid As Integer=0
Public Property _Voidloginemployeeunkid() As Integer 
Get 
Return mintVoidlogingemployeeunkid 
End Get 
Set(ByVal value As Integer) 
mintVoidlogingemployeeunkid = value 
End Set 
End Property 
'S.SANDEEP [28-May-2018] -- END 

    ''' <summary>
    ''' Purpose: Get Message from Class 
    ''' Modify By: Sohail
    ''' </summary>
    Public ReadOnly Property _Message() As String
        Get
            Return mstrMessage
        End Get
    End Property

    ''' <summary>
    ''' Purpose: Get or Set employeeassetstatustranunkid
    ''' Modify By: Sohail
    ''' </summary>
    Public Property _Employeeassetstatustranunkid() As Integer
        Get
            Return mintEmployeeassetstatustranunkid
        End Get
        Set(ByVal value As Integer)
            mintEmployeeassetstatustranunkid = Value
            Call getData()
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set assetstranunkid
    ''' Modify By: Sohail
    ''' </summary>
    Public Property _Assetstranunkid() As Integer
        Get
            Return mintAssetstranunkid
        End Get
        Set(ByVal value As Integer)
            mintAssetstranunkid = Value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set status_date
    ''' Modify By: Sohail
    ''' </summary>
    Public Property _Status_Date() As Date
        Get
            Return mdtStatus_Date
        End Get
        Set(ByVal value As Date)
            mdtStatus_Date = Value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set statusunkid
    ''' Modify By: Sohail
    ''' </summary>
    Public Property _Statusunkid() As Integer
        Get
            Return mintStatusunkid
        End Get
        Set(ByVal value As Integer)
            mintStatusunkid = Value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set remark
    ''' Modify By: Sohail
    ''' </summary>
    Public Property _Remark() As String
        Get
            Return mstrRemark
        End Get
        Set(ByVal value As String)
            mstrRemark = Value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set userunkid
    ''' Modify By: Sohail
    ''' </summary>
    Public Property _Userunkid() As Integer
        Get
            Return mintUserunkid
        End Get
        Set(ByVal value As Integer)
            mintUserunkid = Value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set isvoid
    ''' Modify By: Sohail
    ''' </summary>
    Public Property _Isvoid() As Boolean
        Get
            Return mblnIsvoid
        End Get
        Set(ByVal value As Boolean)
            mblnIsvoid = Value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set voiddatetime
    ''' Modify By: Sohail
    ''' </summary>
    Public Property _Voiddatetime() As Date
        Get
            Return mdtVoiddatetime
        End Get
        Set(ByVal value As Date)
            mdtVoiddatetime = Value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set voiduserunkid
    ''' Modify By: Sohail
    ''' </summary>
    Public Property _Voiduserunkid() As Integer
        Get
            Return mintVoiduserunkid
        End Get
        Set(ByVal value As Integer)
            mintVoiduserunkid = Value
        End Set
    End Property

#End Region

    ''' <summary>
    ''' Modify By: Sohail
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>
    Public Sub GetData()
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        objDataOperation = New clsDataOperation

        Try
            StrQ = "SELECT " & _
              "  employeeassetstatustranunkid " & _
              ", assetstranunkid " & _
              ", status_date " & _
              ", statusunkid " & _
              ", remark " & _
              ", userunkid " & _
              ", isvoid " & _
              ", voiddatetime " & _
              ", voiduserunkid " & _
             "FROM hremployee_assets_status_tran " & _
             "WHERE employeeassetstatustranunkid = @employeeassetstatustranunkid "

            objDataOperation.AddParameter("@employeeassetstatustranunkid", SqlDbType.int, eZeeDataType.INT_SIZE, mintEmployeeassetstatusTranUnkId.ToString)

            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            For Each dtRow As DataRow In dsList.Tables(0).Rows
                mintemployeeassetstatustranunkid = CInt(dtRow.Item("employeeassetstatustranunkid"))
                mintassetstranunkid = CInt(dtRow.Item("assetstranunkid"))
                mdtstatus_date = dtRow.Item("status_date")
                mintstatusunkid = CInt(dtRow.Item("statusunkid"))
                mstrremark = dtRow.Item("remark").ToString
                mintuserunkid = CInt(dtRow.Item("userunkid"))
                mblnisvoid = CBool(dtRow.Item("isvoid"))
                mdtvoiddatetime = dtRow.Item("voiddatetime")
                mintvoiduserunkid = CInt(dtRow.Item("voiduserunkid"))
                Exit For
            Next
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: GetData; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
    End Sub


    ''' <summary>
    ''' Modify By: Sohail
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>
    Public Function GetList(ByVal strTableName As String, Optional ByVal blnOnlyActive As Boolean = True) As DataSet
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        objDataOperation = New clsDataOperation

        Try
            StrQ = "SELECT " & _
              "  employeeassetstatustranunkid " & _
              ", assetstranunkid " & _
              ", status_date " & _
              ", statusunkid " & _
              ", remark " & _
              ", userunkid " & _
              ", isvoid " & _
              ", voiddatetime " & _
              ", voiduserunkid " & _
             "FROM hremployee_assets_status_tran "

            If blnOnlyActive Then
                strQ &= " WHERE isactive = 1 "
            End If

            dsList = objDataOperation.ExecQuery(strQ, strTableName)

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: GetList; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
        Return dsList
    End Function


    ''' <summary>
    ''' Modify By: Sohail
    ''' </summary>
    ''' <returns>Boolean</returns>
    ''' <purpose> INSERT INTO Database Table (hremployee_assets_status_tran) </purpose>
    Public Function Insert(ByVal xCompanyId As Integer, Optional ByVal blnFromEmpAssetInsert As Boolean = False, Optional ByVal objDataOpr As clsDataOperation = Nothing) As Boolean  'S.SANDEEP [ 04 DEC 2013 ] -- START -- END
        'Pinkal (23-Dec-2023) -- (A1X-1555)  NMB - P2P Changes for Claim Request and Company Asset.[ByVal xCompanyId As Integer]

        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception
        Dim blnFlag As Boolean = False
        Dim objEmpAsset As New clsEmployee_Assets_Tran

        'S.SANDEEP [ 04 DEC 2013 ] -- START
        'objDataOperation = New clsDataOperation
        'objDataOperation.BindTransaction()
        If objDataOpr Is Nothing Then
        objDataOperation = New clsDataOperation
        objDataOperation.BindTransaction()
        Else
            objDataOperation = objDataOpr
            objDataOperation.ClearParameters()
        End If
        'S.SANDEEP [ 04 DEC 2013 ] -- END

        Try
            objDataOperation.AddParameter("@assetstranunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintAssetstranunkid.ToString)
            objDataOperation.AddParameter("@status_date", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, mdtStatus_Date)
            objDataOperation.AddParameter("@statusunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintStatusunkid.ToString)
            objDataOperation.AddParameter("@remark", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrRemark.ToString)
            objDataOperation.AddParameter("@userunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintUserunkid.ToString)
            objDataOperation.AddParameter("@isvoid", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsvoid.ToString)
            If mdtVoiddatetime = Nothing Then
                objDataOperation.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, DBNull.Value)
            Else
                objDataOperation.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, mdtVoiddatetime)
            End If
            objDataOperation.AddParameter("@voiduserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintVoiduserunkid.ToString)

            strQ = "INSERT INTO hremployee_assets_status_tran ( " & _
              "  assetstranunkid " & _
              ", status_date " & _
              ", statusunkid " & _
              ", remark " & _
              ", userunkid " & _
              ", isvoid " & _
              ", voiddatetime " & _
              ", voiduserunkid" & _
            ") VALUES (" & _
              "  @assetstranunkid " & _
              ", @status_date " & _
              ", @statusunkid " & _
              ", @remark " & _
              ", @userunkid " & _
              ", @isvoid " & _
              ", @voiddatetime " & _
              ", @voiduserunkid" & _
            "); SELECT @@identity"

            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            mintEmployeeassetstatustranunkid = dsList.Tables(0).Rows(0).Item(0)

            If blnFromEmpAssetInsert = False Then
                objEmpAsset._Assetstranunkid = mintAssetstranunkid
                objEmpAsset._Statusunkid = mintStatusunkid
                With objEmpAsset
                    ._FormName = mstrFormName
                    ._LoginEmployeeUnkid = mintLoginEmployeeunkid
                    ._ClientIP = mstrClientIP
                    ._HostName = mstrHostName
                    ._FromWeb = mblnIsWeb
                    ._AuditUserId = mintAuditUserId
._CompanyUnkid = mintCompanyUnkid
                    ._AuditDate = mdtAuditDate
                End With
                 'Pinkal (23-Dec-2023) -- Start
                '(A1X-1555)  NMB - P2P Changes for Claim Request and Company Asset.
                'blnFlag = objEmpAsset.Update(objDataOperation)
                blnFlag = objEmpAsset.Update(xCompanyId, objDataOperation)
                'Pinkal (23-Dec-2023) -- Start
            Else
                blnFlag = True
            End If

            'S.SANDEEP [ 04 DEC 2013 ] -- START
            'If blnFlag = True Then
            '    objDataOperation.ReleaseTransaction(True)
            'Else
            '    objDataOperation.ReleaseTransaction(False)
            'End If
            If objDataOpr Is Nothing Then
            If blnFlag = True Then
                objDataOperation.ReleaseTransaction(True)
            Else
                objDataOperation.ReleaseTransaction(False)
            End If
            End If
            'S.SANDEEP [ 04 DEC 2013 ] -- END

            Return True
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: Insert; Module Name: " & mstrModuleName)
            Return False
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            'S.SANDEEP [ 04 DEC 2013 ] -- START
            'objDataOperation = Nothing
            'S.SANDEEP [ 04 DEC 2013 ] -- END
        End Try
    End Function

    ''' <summary>
    ''' Modify By: Sohail
    ''' </summary>
    ''' <returns>Boolean</returns>
    ''' <purpose> Update Database Table (hremployee_assets_status_tran) </purpose>
    Public Function Update() As Boolean

        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        objDataOperation = New clsDataOperation

        Try
            objDataOperation.AddParameter("@employeeassetstatustranunkid", SqlDbType.int, eZeeDataType.INT_SIZE, mintemployeeassetstatustranunkid.ToString)
            objDataOperation.AddParameter("@assetstranunkid", SqlDbType.int, eZeeDataType.INT_SIZE, mintassetstranunkid.ToString)
            objDataOperation.AddParameter("@status_date", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, mdtStatus_Date)
            objDataOperation.AddParameter("@statusunkid", SqlDbType.int, eZeeDataType.INT_SIZE, mintstatusunkid.ToString)
            objDataOperation.AddParameter("@remark", SqlDbType.nvarchar, eZeeDataType.NAME_SIZE, mstrremark.ToString)
            objDataOperation.AddParameter("@userunkid", SqlDbType.int, eZeeDataType.INT_SIZE, mintuserunkid.ToString)
            objDataOperation.AddParameter("@isvoid", SqlDbType.bit, eZeeDataType.BIT_SIZE, mblnisvoid.ToString)
            If mdtVoiddatetime = Nothing Then
                objDataOperation.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, DBNull.Value)
            Else
                objDataOperation.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, mdtVoiddatetime)
            End If
            objDataOperation.AddParameter("@voiduserunkid", SqlDbType.int, eZeeDataType.INT_SIZE, mintvoiduserunkid.ToString)

            StrQ = "UPDATE hremployee_assets_status_tran SET " & _
              "  assetstranunkid = @assetstranunkid" & _
              ", status_date = @status_date" & _
              ", statusunkid = @statusunkid" & _
              ", remark = @remark" & _
              ", userunkid = @userunkid" & _
              ", isvoid = @isvoid" & _
              ", voiddatetime = @voiddatetime" & _
              ", voiduserunkid = @voiduserunkid " & _
            "WHERE employeeassetstatustranunkid = @employeeassetstatustranunkid "

            Call objDataOperation.ExecNonQuery(strQ)

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            Return True
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: Update; Module Name: " & mstrModuleName)
            Return False
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
    End Function


    ''' <summary>
    ''' Get Last Status of given Employee Saving
    ''' Modify By : Sohail
    ''' </summary>
    ''' <param name="strTableName"></param>
    ''' <param name="intAssetTranID"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Function GetLastStatus(ByVal strTableName As String, ByVal intAssetTranID As Integer) As DataSet
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        objDataOperation = New clsDataOperation

        Try
            strQ = "SELECT " & _
              "  employeeassetstatustranunkid " & _
              ", assetstranunkid " & _
              ", status_date " & _
              ", statusunkid " & _
              ", remark " & _
              ", userunkid " & _
              ", isvoid " & _
              ", voiddatetime " & _
              ", voiduserunkid " & _
             "FROM hremployee_assets_status_tran " & _
             "WHERE assetstranunkid = @assetstranunkid " & _
             "AND isvoid = 0 " & _
             "ORDER BY status_date DESC "

            objDataOperation.AddParameter("@assetstranunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intAssetTranID.ToString)

            dsList = objDataOperation.ExecQuery(strQ, strTableName)

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: GetLastStatus; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
        Return dsList
    End Function


    '''' <summary>
    '''' Modify By: Sohail
    '''' </summary>
    '''' <returns>Boolean</returns>
    '''' <purpose> Delete Database Table (hremployee_assets_status_tran) </purpose>
    'Public Function Delete(ByVal intUnkid As Integer) As Boolean
    '    If isUsed(intUnkid) Then
    '        mstrMessage = "<Message>"
    '        Return False
    '    End If

    '    Dim dsList As DataSet = Nothing
    '    Dim strQ As String = ""
    '    Dim exForce As Exception

    '    objDataOperation = New clsDataOperation

    '    Try
    '        StrQ = "DELETE FROM hremployee_assets_status_tran " & _
    '        "WHERE employeeassetstatustranunkid = @employeeassetstatustranunkid "

    '        objDataOperation.AddParameter("@employeeassetstatustranunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intUnkid)

    '        Call objDataOperation.ExecNonQuery(strQ)

    '        If objDataOperation.ErrorMessage <> "" Then
    '            exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
    '            Throw exForce
    '        End If

    '        Return True
    '    Catch ex As Exception
    '        Throw New Exception(ex.Message & "; Procedure Name: Update; Module Name: " & mstrModuleName)
    '        Return False
    '    Finally
    '        exForce = Nothing
    '        If dsList IsNot Nothing Then dsList.Dispose()
    '        objDataOperation = Nothing
    '    End Try
    'End Function

    '''' <summary>
    '''' Modify By: Sohail
    '''' </summary>
    '''' <purpose> Assign all Property variable </purpose>
    'Public Function isUsed(ByVal intUnkid As Integer) As Boolean
    '    Dim dsList As DataSet = Nothing
    '    Dim strQ As String = ""
    '    Dim exForce As Exception

    '    objDataOperation = New clsDataOperation

    '    Try
    '        StrQ = "<Query>"

    '        objDataOperation.AddParameter("@employeeassetstatustranunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intUnkid)

    '        dsList = objDataOperation.ExecQuery(strQ, "List")

    '        If objDataOperation.ErrorMessage <> "" Then
    '            exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
    '            Throw exForce
    '        End If

    '        Return dsList.tables(0).rows.count > 0
    '    Catch ex As Exception
    '        Throw New Exception(ex.Message & "; Procedure Name: isUsed; Module Name: " & mstrModuleName)
    '    Finally
    '        exForce = Nothing
    '        If dsList IsNot Nothing Then dsList.Dispose()
    '        objDataOperation = Nothing
    '    End Try
    'End Function

    '''' <summary>
    '''' Modify By: Sohail
    '''' </summary>
    '''' <purpose> Assign all Property variable </purpose>
    'Public Function isExist(ByVal strCode As String, ByVal strName As String, Optional ByVal intUnkid As Integer = -1) As Boolean
    '    Dim dsList As DataSet = Nothing
    '    Dim strQ As String = ""
    '    Dim exForce As Exception

    '    objDataOperation = New clsDataOperation

    '    Try
    '        StrQ = "SELECT " & _
    '          "  employeeassetstatustranunkid " & _
    '          ", assetstranunkid " & _
    '          ", status_date " & _
    '          ", statusunkid " & _
    '          ", remark " & _
    '          ", userunkid " & _
    '          ", isvoid " & _
    '          ", voiddatetime " & _
    '          ", voiduserunkid " & _
    '         "FROM hremployee_assets_status_tran " & _
    '         "WHERE name = @name " & _
    '         "AND code = @code "

    '        If intUnkid > 0 Then
    '            strQ &= " AND employeeassetstatustranunkid <> @employeeassetstatustranunkid"
    '        End If

    '        objDataOperation.AddParameter("@code", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, strCode)
    '        objDataOperation.AddParameter("@name", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, strName)
    '        objDataOperation.AddParameter("@employeeassetstatustranunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intUnkid)

    '        dsList = objDataOperation.ExecQuery(strQ, "List")

    '        If objDataOperation.ErrorMessage <> "" Then
    '            exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
    '            Throw exForce
    '        End If

    '        Return dsList.tables(0).rows.count > 0
    '    Catch ex As Exception
    '        Throw New Exception(ex.Message & "; Procedure Name: isExist; Module Name: " & mstrModuleName)
    '    Finally
    '        exForce = Nothing
    '        If dsList IsNot Nothing Then dsList.Dispose()
    '        objDataOperation = Nothing
    '    End Try
    'End Function

End Class
