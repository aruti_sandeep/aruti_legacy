﻿#Region " Imports "

Imports System
Imports System.Data
Imports System.Data.Sql
Imports System.Data.SqlClient
Imports Microsoft.Win32
Imports System.Text
Imports System.IO
Imports System.ComponentModel
Imports Aruti.Data
Imports eZeeCommonLib

#End Region

Public Class clsPeoplesoftImport

#Region " Private Variables "

    Private strFilename As String
    Private Const conREG_NODE = "Software\NPK\Aruti"
    Private mstrAppPath As String = ""
    Private mstrServerName As String = ""
    Dim sqlCn As SqlConnection
    Dim sqlCmd As SqlCommand
    Dim Trn As SqlTransaction
    Public gintTotalEmployees As Integer = 0
    Public gblnIsImported As Boolean = False
    Public gstrFileName As String = ""

    Private mintShiftUnkId As Integer = 0
    Private mstrShiftName As String = String.Empty
    Private mintIdentityTypeUnkId As Integer = 0
    Private mstrIdentityTypeName As String = String.Empty
    Private mintTZSSalaryUnkId As Integer = 0
    Private mstrTZSSalaryName As String = String.Empty
    Private mintTZSSalaryHeadTypeId As Integer = 0
    Private mintUSDSalaryUnkId As Integer = 0
    Private mstrUSDSalaryName As String = String.Empty
    Private mintUSDSalaryHeadTypeId As Integer = 0
    Private mintGradeGroupUnkId As Integer = 0
    Private mstrGradeGroupName As String = String.Empty
    Private mintGradeUnkId As Integer = 0
    Private mstrGradeName As String = String.Empty
    Private mintGradeLevelUnkId As Integer = 0
    Private mstrGradeLevelName As String = String.Empty
    Private mintSalaryChangeUnkId As Integer = 0
    Private mstrSalaryChangeName As String = String.Empty
    'Sohail (18 Feb 2019) -- Start
    'NMB Enhancement - 76.1 - Provide setting on transaction head to mark as default heads.
    Private mblnAssignDefaulTranHeads As Boolean = False
    'Sohail (18 Feb 2019) -- End

    Private mdtTable As New DataTable
    Private mdtAllocation As New DataTable
    Private mdtDependant As New DataTable
#End Region

#Region "API Functions"

    <System.Runtime.InteropServices.DllImport("kernel32.dll", EntryPoint:="GetPrivateProfileStringA", ExactSpelling:=True, CharSet:=System.Runtime.InteropServices.CharSet.Ansi, SetLastError:=True)> _
    Private Shared Function GetPrivateProfileString(ByVal lpApplicationName As String, ByVal lpKeyName As String, ByVal lpDefault As String, ByVal lpReturnedString As System.Text.StringBuilder, ByVal nSize As Integer, ByVal lpFileName As String) As Integer
    End Function

    <System.Runtime.InteropServices.DllImport("kernel32.dll", EntryPoint:="WritePrivateProfileStringA", ExactSpelling:=True, CharSet:=System.Runtime.InteropServices.CharSet.Ansi, SetLastError:=True)> _
    Private Shared Function WritePrivateProfileString(ByVal lpApplicationName As String, ByVal lpKeyName As String, ByVal lpString As String, ByVal lpFileName As String) As Integer
    End Function

    <System.Runtime.InteropServices.DllImport("kernel32.dll", EntryPoint:="GetPrivateProfileIntA", ExactSpelling:=True, CharSet:=System.Runtime.InteropServices.CharSet.Ansi, SetLastError:=True)> _
    Private Shared Function GetPrivateProfileInt(ByVal lpApplicationName As String, ByVal lpKeyName As String, ByVal nDefault As Integer, ByVal lpFileName As String) As Integer
    End Function

    <System.Runtime.InteropServices.DllImport("kernel32.dll", EntryPoint:="WritePrivateProfileStringA", ExactSpelling:=True, CharSet:=System.Runtime.InteropServices.CharSet.Ansi, SetLastError:=True)> _
    Private Shared Function FlushPrivateProfileString(ByVal lpApplicationName As Integer, ByVal lpKeyName As Integer, ByVal lpString As Integer, ByVal lpFileName As String) As Integer
    End Function

#End Region

#Region " Structure "

    Private Structure strctTable
        Dim i As Integer
        Const PARENT_PERSON As String = "PARENT:PERSON"
        Const CHILD_NAMES As String = "CHILD:NAMES"
        Const CHILD_PERS_DATA_EFFDT As String = "CHILD:PERS_DATA_EFFDT"
        Const PARENT_JOB As String = "PARENT:JOB"
        Const CHILD_COMPENSATION As String = "CHILD:COMPENSATION"
        Const CHILD_DEPENDENT_BENEF As String = "CHILD:DEPENDENT_BENEF"
    End Structure

#End Region

#Region " Enum "

    Private Enum enPARENT_PERSON
        IDENTITY_NO = 1
        EMPLOYEE_CODE = 2
        BIRTH_VILLAGE = 3
        BIRTH_DATE = 4
        LAST_UPDATE_TIME = 5
        COUNTRY_ALIAS = 6
    End Enum

    Private Enum enCHILD_NAMES
        IDENTITY_NO = 1
        EMPLOYEE_CODE = 2
        NAME_TYPE = 3
        APPOINTMENT_DATE = 4
        ACTIVE = 5
        COUNTRY_NAME_FORMAT = 6
        TITLE = 7
        SURNAME = 8
        FIRST_NAME = 9
        OTHER_NAME = 10
        LAST_UPDATE_TIME = 11
    End Enum

    Private Enum enCHILD_PERS_DATA_EFFDT
        IDENTITY_NO = 1
        EMPLOYEE_CODE = 2
        APPOINTMENT_DATE = 3
        MARITAL_STATUS = 4
        ANNIVERSARY_DATE = 5
        GENDER = 6
        HIGH_EDU_LEVEL = 7
        FULL_TIME = 8
        LANGUAGE = 9
        LAST_UPDATE_TIME = 10
    End Enum

    Private Enum enPARENT_JOB
        IDENTITY_NO = 1
        EMPLOYEE_CODE = 2
        ALLOCATION_CHANGE_DATE = 3
        EFFECTIVE_SEQUENCE = 4
        DEPT_DESCRIPTION = 5
        JOB_CODE = 6
        DEPT_UNKID = 7
        JOB_UNKID = 8
        HR_STATUS = 9
        PAYROLL_STATUS = 10
        ACTION = 11
        ACTION_REASON = 12
        BRANCH = 13
        EOC_DATE = 14
        REGULAR_SHIFT = 15
        REG_TEMP = 16
        'Sohail (16 Jun 2015) -- Start
        'Enhancement - Now pick employment type from Fulltime Parttime column and pick class group from employmenttype column as per rutta's request to anjan sir on skype.
        'FULLTIME_PARTTIME = 17
        'EMPLOYMENT_TYPE = 18
        EMPLOYMENT_TYPE = 17
        CLASS_GROUP_CODE = 18
        'Sohail (16 Jun 2015) -- End
        COMPANY = 19
        JOB_GROUP_CODE = 20
        REG_REGION = 21
        LEAVING_DATE = 22
        LAST_UPDATE_TIME = 23
    End Enum

    Private Enum enCHILD_COMPENSATION
        IDENTITY_NO = 1
        EMPLOYEE_CODE = 2
        INCREMENT_DATE = 3
        EFFECTIVE_SEQUENCE = 4
        COMP_EFFECTIVE_SEQUENCE = 5
        BASIC_PAY = 6
        NEW_SCALE = 7
        COMP_FREQUENCY = 8
        CURRENCY = 9
        LAST_UPDATE_TIME = 10
    End Enum

    Private Enum enCHILD_DEPENDENT_BENEF
        IDENTITY_NO = 1
        EMPLOYEE_CODE = 2
        APPOINTMENT_DATE = 3
        SR_NO = 4
        SURNAME = 5
        FIRST_NAME = 6
        OTHER_NAME = 7
        RELATION = 8
        BENEFICIARY_TYPE = 9
        COUNTRY_ALIAS = 10
        ADDRESS_1 = 11
        CITY = 12
        STATE = 13
        ZIP_CODE = 14
        BIRTH_DATE = 15
        LAST_UPDATE_TIME = 16
        GENDER = 17
        MARITAL_STATUS = 18
        MOBILE_NO = 19
        EMAIL_TYPE = 20
        EMAIL = 21
    End Enum

#End Region

#Region " Properties "

'S.SANDEEP [28-May-2018] -- START 
'ISSUE/ENHANCEMENT : {Audit Trails} 
Private mstrFormName As String = String.Empty
Public WriteOnly Property _FormName() As String 
Set(ByVal value As String) 
mstrFormName = value 
End Set 
End Property 
 
Private mstrClientIP As String = ""
Public WriteOnly Property _ClientIP() As String 
Set(ByVal value As String) 
mstrClientIP = value 
End Set 
End Property 
 
Private mstrHostName As String = ""
Public WriteOnly Property _HostName() As String 
Set(ByVal value As String) 
mstrHostName = value 
End Set 
End Property 
 
Private mblnIsWeb As Boolean = False
Public WriteOnly Property _FromWeb() As Boolean 
Set(ByVal value As Boolean) 
mblnIsWeb = value 
End Set 
End Property 
 
    Private mintAuditUserId As Integer = 0
    Public WriteOnly Property _AuditUserId() As Integer
        Set(ByVal value As Integer)
            mintAuditUserId = value
        End Set
    End Property

    Private mintCompanyUnkid As Integer = 0
    Public WriteOnly Property _CompanyUnkid() As Integer
        Set(ByVal value As Integer)
            mintCompanyUnkid = value
        End Set
    End Property
 
Private mdtAuditDate As DateTime = Now
Public WriteOnly Property _AuditDate() As DateTime 
Set(ByVal value As DateTime) 
mdtAuditDate = value 
End Set 
End Property 
 
Private xDataOpr As clsDataOperation = Nothing
Public Property _xDataOpr() As clsDataOperation 
Get 
Return xDataOpr 
End Get 
Set(ByVal value As clsDataOperation) 
xDataOpr = value 
End Set 
End Property 
 
Private mintLoginEmployeeunkid As Integer = 0
Public Property _LoginEmployeeunkid() As Integer 
Get 
Return mintLoginEmployeeunkid 
End Get 
Set(ByVal value As Integer) 
mintLoginEmployeeunkid = value 
End Set 
End Property 
 
Private mintVoidlogingemployeeunkid As Integer=0
Public Property _Voidloginemployeeunkid() As Integer 
Get 
Return mintVoidlogingemployeeunkid 
End Get 
Set(ByVal value As Integer) 
mintVoidlogingemployeeunkid = value 
End Set 
End Property 
'S.SANDEEP [28-May-2018] -- END 


    Public ReadOnly Property _Data_Table() As DataTable
        Get
            Return mdtTable
        End Get
    End Property

    Public ReadOnly Property _Dependant_Table() As DataTable
        Get
            Return mdtDependant
        End Get
    End Property

    Public Property _ShiftUnkID() As Integer
        Get
            Return mintShiftUnkId
        End Get
        Set(ByVal value As Integer)
            mintShiftUnkId = value
        End Set
    End Property

    Public Property _ShiftName() As String
        Get
            Return mstrShiftName
        End Get
        Set(ByVal value As String)
            mstrShiftName = value
        End Set
    End Property

    Public Property _IdentityTypeUnkID() As Integer
        Get
            Return mintIdentityTypeUnkId
        End Get
        Set(ByVal value As Integer)
            mintIdentityTypeUnkId = value
        End Set
    End Property

    Public Property _IdentityTypeName() As String
        Get
            Return mstrIdentityTypeName
        End Get
        Set(ByVal value As String)
            mstrIdentityTypeName = value
        End Set
    End Property

    Public Property _TZSSalaryUnkID() As Integer
        Get
            Return mintTZSSalaryUnkId
        End Get
        Set(ByVal value As Integer)
            mintTZSSalaryUnkId = value
        End Set
    End Property

    Public Property _TZSSalaryHeadTypeID() As Integer
        Get
            Return mintTZSSalaryHeadTypeId
        End Get
        Set(ByVal value As Integer)
            mintTZSSalaryHeadTypeId = value
        End Set
    End Property

    Public Property _TZSSalaryName() As String
        Get
            Return mstrTZSSalaryName
        End Get
        Set(ByVal value As String)
            mstrTZSSalaryName = value
        End Set
    End Property

    Public Property _USDSalaryUnkID() As Integer
        Get
            Return mintUSDSalaryUnkId
        End Get
        Set(ByVal value As Integer)
            mintUSDSalaryUnkId = value
        End Set
    End Property

    Public Property _USDSalaryName() As String
        Get
            Return mstrUSDSalaryName
        End Get
        Set(ByVal value As String)
            mstrUSDSalaryName = value
        End Set
    End Property

    Public Property _USDSalaryHeadTypeID() As Integer
        Get
            Return mintUSDSalaryHeadTypeId
        End Get
        Set(ByVal value As Integer)
            mintUSDSalaryHeadTypeId = value
        End Set
    End Property

    Public Property _GradeGroupUnkID() As Integer
        Get
            Return mintGradeGroupUnkId
        End Get
        Set(ByVal value As Integer)
            mintGradeGroupUnkId = value
        End Set
    End Property

    Public Property _GradeGroupName() As String
        Get
            Return mstrGradeGroupName
        End Get
        Set(ByVal value As String)
            mstrGradeGroupName = value
        End Set
    End Property

    Public Property _GradeUnkID() As Integer
        Get
            Return mintGradeUnkId
        End Get
        Set(ByVal value As Integer)
            mintGradeUnkId = value
        End Set
    End Property

    Public Property _GradeName() As String
        Get
            Return mstrGradeName
        End Get
        Set(ByVal value As String)
            mstrGradeName = value
        End Set
    End Property

    Public Property _GradeLevelUnkID() As Integer
        Get
            Return mintGradeLevelUnkId
        End Get
        Set(ByVal value As Integer)
            mintGradeLevelUnkId = value
        End Set
    End Property

    Public Property _GradeLevelName() As String
        Get
            Return mstrGradeLevelName
        End Get
        Set(ByVal value As String)
            mstrGradeLevelName = value
        End Set
    End Property

    'Sohail (18 Feb 2019) -- Start
    'NMB Enhancement - 76.1 - Provide setting on transaction head to mark as default heads.
    Public Property _AssignDefaulTranHeads() As Boolean
        Get
            Return mblnAssignDefaulTranHeads
        End Get
        Set(ByVal value As Boolean)
            mblnAssignDefaulTranHeads = value
        End Set
    End Property
    'Sohail (18 Feb 2019) -- End

#End Region

#Region " Private Methods "

    Public Function GetString(ByVal Section As String, ByVal Key As String) As String
        Return GetString(Section, Key, "")
    End Function

    Public Function getValue() As String
        Dim Key As RegistryKey = Nothing
        Dim Value As String = String.Empty
        Try
            'opens the given subkey
            Key = Registry.LocalMachine.OpenSubKey(conREG_NODE, True)
            If Key Is Nothing Then
                'it Key doesn't exist then throw an exception
                Throw New Exception("Given Subkey not exist.")
            End If

            'Gets the value
            Value = Key.GetValue("AppPath")

            Return Value
        Catch e As Exception
            e.ToString()
            Return False
        End Try
    End Function

    Public Function GetString(ByVal Section As String, ByVal Key As String, ByVal [Default] As String) As String

        ' Returns a string from your INI file
        Dim intCharCount As Integer = 0
        Dim strResult As String = ""
        Dim objResult As New System.Text.StringBuilder(256)

        'intCharCount = GetPrivateProfileString(Section, Key, _
        '   [Default], objResult, objResult.Capacity, strFilename)
        intCharCount = GetPrivateProfileString(Section, Key, "", objResult, objResult.Capacity, mstrAppPath & "aruti.ini")

        If intCharCount > 0 Then
            strResult = objResult.ToString().Substring(0, intCharCount)
        Else
            WriteString(Section, Key, [Default])
            strResult = [Default]
        End If

        Return strResult

    End Function

    Public Sub WriteString(ByVal Section As String, ByVal Key As String, ByVal Value As String)

        ' Writes a string to your INI file
        WritePrivateProfileString(Section, Key, Value, strFilename)
        Flush()

    End Sub

    Private Sub Flush()
        FlushPrivateProfileString(0, 0, 0, strFilename)
    End Sub

    Public Function IsDBConnected() As Boolean
        Try
            mstrAppPath = getValue()
            mstrServerName = GetString("Aruti_Payroll", "Server")
            mstrServerName = IIf(mstrServerName = "", "(LOCAL)", mstrServerName)
            sqlCn = New SqlConnection("Data Source=" & mstrServerName & "\APAYROLL;Initial Catalog=hrmsConfiguration;User ID=sa;Password=pRofessionalaRuti999")
            sqlCn.Open()
            sqlCmd = New SqlCommand
            sqlCmd.CommandTimeout = 0
            sqlCmd.Connection = sqlCn
            Return True
        Catch ex As Exception
            Return False
        End Try

    End Function

    Public Function CompanyList() As DataSet
        Dim strQ As String = "SELECT companyunkid As Id,code AS Code,name AS Name FROM hrmsConfiguration..cfcompany_master where isactive = 1 "
        sqlCmd.CommandText = strQ
        Dim da As New SqlDataAdapter(sqlCmd)
        Dim ds As New DataSet
        da.Fill(ds)
        Return ds
    End Function

    Public Function GetCurrentDBName(ByVal intCompId As Integer) As String
        Dim StrDBName As String = String.Empty
        Try
            Dim StrQ As String = "SELECT yearunkid,financialyear_name,database_name AS DName FROM hrmsConfiguration..cffinancial_year_tran WHERE companyunkid = @id AND isclosed = 0 ORDER BY yearunkid DESC"
            sqlCmd.CommandText = StrQ
            sqlCmd.Parameters.Clear()
            sqlCmd.Parameters.AddWithValue("@id", intCompId)
            Dim dr As SqlDataReader = sqlCmd.ExecuteReader()
            If dr.Read Then
                StrDBName = dr("DName")
                dr.Close()
            End If
        Catch ex As Exception
            MsgBox(ex.Message)
        End Try
        Return StrDBName
    End Function

    Public Function getTranHeadList(ByVal strDatabaseName As String, _
                                   Optional ByVal mblnAddSelect As Boolean = False, _
                                   Optional ByVal intTranHeadTypeID As Integer = 0, _
                                   Optional ByVal intCalcTypeID As Integer = 0, _
                                   Optional ByVal intTypeOfID As Integer = -1, _
                                   Optional ByVal blnAddEmpContribPayable As Boolean = False, _
                                   Optional ByVal blnExcludeMembershipHeads As Boolean = False, _
                                   Optional ByVal strFilter As String = "", _
                                   Optional ByVal blnAddRoundingAdjustment As Boolean = False _
                                   ) As DataSet


        Dim strQ As String = ""

        Try


            If mblnAddSelect = True Then
                strQ = "SELECT 0 AS tranheadunkid , ' ' AS code, ' ' + 'Select' AS name, 0 AS trnheadtype_id, 0 AS calctype_id, 0 AS typeof_id " & _
               " UNION "
            End If


            strQ &= " SELECT tranheadunkid, trnheadcode as code, trnheadname as name, trnheadtype_id, calctype_id, typeof_id " & _
             " FROM " & strDatabaseName & "..prtranhead_master "

            If blnExcludeMembershipHeads = True Then
                strQ += "LEFT JOIN " & strDatabaseName & "..hrmembership_master AS EmpContrib ON EmpContrib.emptranheadunkid = prtranhead_master.tranheadunkid AND EmpContrib.isactive = 1 " & _
                        "LEFT JOIN " & strDatabaseName & "..hrmembership_master AS CoContrib ON CoContrib.cotranheadunkid = prtranhead_master.tranheadunkid AND CoContrib.isactive = 1 "
            End If

            strQ += " WHERE ISNULL(isvoid,0) = 0"
            strQ &= " AND ISNULL(prtranhead_master.isactive, 1) = 1 "

            If intTranHeadTypeID > 0 Then
                strQ &= " AND trnheadtype_id = " & intTranHeadTypeID & " "
            End If

            If intCalcTypeID > 0 Then
                strQ += " AND calctype_id = " & intCalcTypeID & " "
            End If

            If intTypeOfID >= 0 Then
                strQ &= " AND typeof_id = " & intTypeOfID & " "
            End If

            If blnAddEmpContribPayable = False Then
                strQ &= " AND calctype_id <> 13  "
            End If

            If blnExcludeMembershipHeads = True Then
                strQ += " AND EmpContrib.membershipunkid IS NULL " & _
                        " AND CoContrib.membershipunkid IS NULL "
            End If

            If blnAddRoundingAdjustment = False Then
                strQ += " AND calctype_id <> 17 "
            End If

            If strFilter.Trim <> "" Then
                strQ &= " AND " & strFilter
            End If

            strQ &= " ORDER BY name "

            sqlCmd.CommandText = strQ
            Dim da As New SqlDataAdapter(sqlCmd)
            Dim ds As New DataSet
            da.Fill(ds)
            Return ds

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: getTranHeadList")
            Return Nothing
        End Try
    End Function

    'Sohail (24 Nov 2014) -- Start
    Private Sub CreateDataTable()
        Try
            mdtTable.Columns.Add("IsChecked", System.Type.GetType("System.Boolean")).DefaultValue = False
            mdtTable.Columns.Add("idtypeunkid", System.Type.GetType("System.Int32")).DefaultValue = -1
            mdtTable.Columns.Add("identity_no", System.Type.GetType("System.String")).DefaultValue = ""
            mdtTable.Columns.Add("isdefault", System.Type.GetType("System.Boolean")).DefaultValue = False
            mdtTable.Columns.Add("identity_aud", System.Type.GetType("System.String")).DefaultValue = ""
            mdtTable.Columns.Add("employeeunkid", System.Type.GetType("System.Int32")).DefaultValue = -1
            mdtTable.Columns.Add("employeecode", System.Type.GetType("System.String")).DefaultValue = ""
            mdtTable.Columns.Add("displayname", System.Type.GetType("System.String")).DefaultValue = ""
            mdtTable.Columns.Add("password", System.Type.GetType("System.String")).DefaultValue = ""
            mdtTable.Columns.Add("titleunkid", System.Type.GetType("System.Int32")).DefaultValue = -1
            mdtTable.Columns.Add("title", System.Type.GetType("System.String")).DefaultValue = ""
            mdtTable.Columns.Add("firstname", System.Type.GetType("System.String")).DefaultValue = ""
            mdtTable.Columns.Add("othername", System.Type.GetType("System.String")).DefaultValue = ""
            mdtTable.Columns.Add("surname", System.Type.GetType("System.String")).DefaultValue = ""
            mdtTable.Columns.Add("appointeddate", System.Type.GetType("System.DateTime"))
            mdtTable.Columns.Add("gender", System.Type.GetType("System.Int32")).DefaultValue = -1
            mdtTable.Columns.Add("gendername", System.Type.GetType("System.String")).DefaultValue = ""
            mdtTable.Columns.Add("employmenttypeunkid", System.Type.GetType("System.Int32")).DefaultValue = -1
            mdtTable.Columns.Add("employmenttype", System.Type.GetType("System.String")).DefaultValue = ""
            mdtTable.Columns.Add("shiftunkid", System.Type.GetType("System.Int32")).DefaultValue = -1
            mdtTable.Columns.Add("shift", System.Type.GetType("System.String")).DefaultValue = ""
            mdtTable.Columns.Add("birthdate", System.Type.GetType("System.DateTime"))
            mdtTable.Columns.Add("birthcountryunkid", System.Type.GetType("System.Int32")).DefaultValue = -1
            mdtTable.Columns.Add("birthcountry", System.Type.GetType("System.String")).DefaultValue = ""
            mdtTable.Columns.Add("birth_village", System.Type.GetType("System.String")).DefaultValue = ""
            mdtTable.Columns.Add("maritalstatusunkid", System.Type.GetType("System.Int32")).DefaultValue = -1
            mdtTable.Columns.Add("maritalstatus", System.Type.GetType("System.String")).DefaultValue = ""
            mdtTable.Columns.Add("anniversary_date", System.Type.GetType("System.DateTime"))
            mdtTable.Columns.Add("departmentunkid", System.Type.GetType("System.Int32")).DefaultValue = -1
            mdtTable.Columns.Add("department", System.Type.GetType("System.String")).DefaultValue = ""
            mdtTable.Columns.Add("jobunkid", System.Type.GetType("System.Int32")).DefaultValue = -1
            mdtTable.Columns.Add("job", System.Type.GetType("System.String")).DefaultValue = ""
            mdtTable.Columns.Add("stationunkid", System.Type.GetType("System.Int32")).DefaultValue = -1
            mdtTable.Columns.Add("station", System.Type.GetType("System.String")).DefaultValue = ""
            mdtTable.Columns.Add("jobgroupunkid", System.Type.GetType("System.Int32")).DefaultValue = -1
            mdtTable.Columns.Add("jobgroup", System.Type.GetType("System.String")).DefaultValue = ""
            'Sohail (16 Jun 2015) -- Start
            'Enhancement - Now pick employment type from Fulltime Parttime column and pick class group from employmenttype column as per rutta's request to anjan sir on skype.
            mdtTable.Columns.Add("classgroupunkid", System.Type.GetType("System.Int32")).DefaultValue = -1
            mdtTable.Columns.Add("classgroup", System.Type.GetType("System.String")).DefaultValue = ""
            'Sohail (16 Jun 2015) -- End
            mdtTable.Columns.Add("gradegroupunkid", System.Type.GetType("System.Int32")).DefaultValue = -1
            mdtTable.Columns.Add("gradegroup", System.Type.GetType("System.String")).DefaultValue = ""
            mdtTable.Columns.Add("gradeunkid", System.Type.GetType("System.Int32")).DefaultValue = -1
            mdtTable.Columns.Add("grade", System.Type.GetType("System.String")).DefaultValue = ""
            mdtTable.Columns.Add("gradelevelunkid", System.Type.GetType("System.Int32")).DefaultValue = -1
            mdtTable.Columns.Add("gradelevel", System.Type.GetType("System.String")).DefaultValue = ""
            mdtTable.Columns.Add("incrementdate", System.Type.GetType("System.DateTime"))
            mdtTable.Columns.Add("scale", System.Type.GetType("System.Decimal")).DefaultValue = 0
            mdtTable.Columns.Add("costcenterunkid", System.Type.GetType("System.Int32")).DefaultValue = -1
            mdtTable.Columns.Add("costcenter", System.Type.GetType("System.String")).DefaultValue = ""
            mdtTable.Columns.Add("tranhedunkid", System.Type.GetType("System.Int32")).DefaultValue = -1
            mdtTable.Columns.Add("tranhed", System.Type.GetType("System.String")).DefaultValue = ""
            mdtTable.Columns.Add("trnheadtype_id", System.Type.GetType("System.Int32")).DefaultValue = -1
            mdtTable.Columns.Add("termination_from_date", System.Type.GetType("System.DateTime"))
            mdtTable.Columns.Add("termination_to_date", System.Type.GetType("System.DateTime"))
            mdtTable.Columns.Add("empl_enddate", System.Type.GetType("System.DateTime"))
            mdtTable.Columns.Add("allocation_change_date", System.Type.GetType("System.DateTime"))
            mdtTable.Columns.Add("last_update_time", System.Type.GetType("System.DateTime"))
            mdtTable.Columns.Add("reinstatement_date", System.Type.GetType("System.DateTime"))

            mdtTable.Columns.Add("periodunkid", System.Type.GetType("System.Decimal")).DefaultValue = 0
            mdtTable.Columns.Add("currentscale", System.Type.GetType("System.Decimal")).DefaultValue = 0
            mdtTable.Columns.Add("increment", System.Type.GetType("System.Decimal")).DefaultValue = 0
            mdtTable.Columns.Add("newscale", System.Type.GetType("System.Decimal")).DefaultValue = 0
            mdtTable.Columns.Add("isgradechange", System.Type.GetType("System.Boolean")).DefaultValue = False
            mdtTable.Columns.Add("isfromemployee", System.Type.GetType("System.Boolean")).DefaultValue = False
            mdtTable.Columns.Add("reason_id", System.Type.GetType("System.Decimal")).DefaultValue = 0
            mdtTable.Columns.Add("reason", System.Type.GetType("System.String")).DefaultValue = ""
            mdtTable.Columns.Add("increment_mode", System.Type.GetType("System.Decimal")).DefaultValue = 0
            mdtTable.Columns.Add("rowtypeid", System.Type.GetType("System.Int32")).DefaultValue = 0
            mdtTable.Columns.Add("remark", System.Type.GetType("System.String")).DefaultValue = ""
            'Sohail (21 Aug 2015) -- Start
            'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
            mdtTable.Columns.Add("start_date", System.Type.GetType("System.String")).DefaultValue = ""
            mdtTable.Columns.Add("end_date", System.Type.GetType("System.String")).DefaultValue = ""
            'Sohail (21 Aug 2015) -- End

            'Allocation
            mdtAllocation.Columns.Add("employeeunkid", System.Type.GetType("System.Int32")).DefaultValue = -1
            mdtAllocation.Columns.Add("employeecode", System.Type.GetType("System.String")).DefaultValue = ""
            mdtAllocation.Columns.Add("departmentunkid", System.Type.GetType("System.Int32")).DefaultValue = -1
            mdtAllocation.Columns.Add("department", System.Type.GetType("System.String")).DefaultValue = ""
            mdtAllocation.Columns.Add("jobunkid", System.Type.GetType("System.Int32")).DefaultValue = -1
            mdtAllocation.Columns.Add("job", System.Type.GetType("System.String")).DefaultValue = ""
            mdtAllocation.Columns.Add("stationunkid", System.Type.GetType("System.Int32")).DefaultValue = -1
            mdtAllocation.Columns.Add("station", System.Type.GetType("System.String")).DefaultValue = ""
            mdtAllocation.Columns.Add("jobgroupunkid", System.Type.GetType("System.Int32")).DefaultValue = -1
            mdtAllocation.Columns.Add("jobgroup", System.Type.GetType("System.String")).DefaultValue = ""
            'Sohail (16 Jun 2015) -- Start
            'Enhancement - Now pick employment type from Fulltime Parttime column and pick class group from employmenttype column as per rutta's request to anjan sir on skype.
            mdtAllocation.Columns.Add("classgroupunkid", System.Type.GetType("System.Int32")).DefaultValue = -1
            mdtAllocation.Columns.Add("classgroup", System.Type.GetType("System.String")).DefaultValue = ""
            'Sohail (16 Jun 2015) -- End
            mdtAllocation.Columns.Add("costcenterunkid", System.Type.GetType("System.Int32")).DefaultValue = -1
            mdtAllocation.Columns.Add("costcenter", System.Type.GetType("System.String")).DefaultValue = ""
            mdtAllocation.Columns.Add("empl_enddate", System.Type.GetType("System.DateTime"))
            mdtAllocation.Columns.Add("employmenttypeunkid", System.Type.GetType("System.Int32")).DefaultValue = -1
            mdtAllocation.Columns.Add("employmenttype", System.Type.GetType("System.String")).DefaultValue = ""
            mdtAllocation.Columns.Add("termination_from_date", System.Type.GetType("System.DateTime"))
            mdtAllocation.Columns.Add("allocation_change_date", System.Type.GetType("System.DateTime"))
            mdtAllocation.Columns.Add("reinstatement_date", System.Type.GetType("System.DateTime"))

            'Dependant
            mdtDependant.Columns.Add("employeeunkid", System.Type.GetType("System.Int32")).DefaultValue = -1
            mdtDependant.Columns.Add("employeecode", System.Type.GetType("System.String")).DefaultValue = ""
            mdtDependant.Columns.Add("identity_no", System.Type.GetType("System.String")).DefaultValue = ""
            mdtDependant.Columns.Add("first_name", System.Type.GetType("System.String")).DefaultValue = ""
            mdtDependant.Columns.Add("middle_name", System.Type.GetType("System.String")).DefaultValue = ""
            mdtDependant.Columns.Add("last_name", System.Type.GetType("System.String")).DefaultValue = ""
            mdtDependant.Columns.Add("relationunkid", System.Type.GetType("System.Int32")).DefaultValue = -1
            mdtDependant.Columns.Add("relation", System.Type.GetType("System.String")).DefaultValue = ""
            mdtDependant.Columns.Add("birthdate", System.Type.GetType("System.DateTime"))
            mdtDependant.Columns.Add("address", System.Type.GetType("System.String")).DefaultValue = ""
            mdtDependant.Columns.Add("mobile_no", System.Type.GetType("System.String")).DefaultValue = ""
            mdtDependant.Columns.Add("post_box", System.Type.GetType("System.String")).DefaultValue = ""
            mdtDependant.Columns.Add("countryunkid", System.Type.GetType("System.Int32")).DefaultValue = -1
            mdtDependant.Columns.Add("country", System.Type.GetType("System.String")).DefaultValue = ""
            mdtDependant.Columns.Add("stateunkid", System.Type.GetType("System.Int32")).DefaultValue = -1
            mdtDependant.Columns.Add("state", System.Type.GetType("System.String")).DefaultValue = ""
            mdtDependant.Columns.Add("cityunkid", System.Type.GetType("System.Int32")).DefaultValue = -1
            mdtDependant.Columns.Add("city", System.Type.GetType("System.String")).DefaultValue = ""
            mdtDependant.Columns.Add("email", System.Type.GetType("System.String")).DefaultValue = ""
            mdtDependant.Columns.Add("gender", System.Type.GetType("System.Int32")).DefaultValue = -1
            mdtDependant.Columns.Add("gendername", System.Type.GetType("System.String")).DefaultValue = ""
            mdtDependant.Columns.Add("rowtypeid", System.Type.GetType("System.Int32")).DefaultValue = 0
            mdtDependant.Columns.Add("remark", System.Type.GetType("System.String")).DefaultValue = ""
            'Sohail (18 Feb 2019) -- Start
            'NMB Enhancement - 76.1 - Provide setting on transaction head to mark as default heads.
            mdtTable.Columns.Add("AssignDefaulTranHeads", System.Type.GetType("System.Boolean")).DefaultValue = False
            'Sohail (18 Feb 2019) -- End

        Catch ex As Exception
            Throw New Exception("CreateDataTable : " & ex.Message)
        End Try
    End Sub
    Public Function Import_Inbound(ByVal intCompanyUnkId As Integer, ByVal intTZSHeadID As Integer, ByVal intUSDHead As Integer, ByVal strExportPath As String, ByVal bw As BackgroundWorker, ByVal strEmployeeAsOnDate As String, ByVal intYearUnkId As Integer, ByVal dtDatabase_Start_Date As Date, ByVal strDatabaseName As String) As Boolean
        'Sohail (21 Aug 2015) - [strEmployeeAsOnDate, intYearUnkId, dtDatabase_Start_Date, strDatabaseName]
        Dim strQ As String = ""

        Dim objEmp As New clsEmployee_Master
        Dim objId_Tran As New clsIdentity_tran
        Dim objSalary As New clsSalaryIncrement
        Dim objDependant As New clsDependants_Beneficiary_tran
        Dim objMaster As New clsMasterData
        Dim objTitle As New clsCommon_Master
        Dim objDept As New clsDepartment
        Dim objCCenter As New clscostcenter_master
        Dim objJob As New clsJobs
        Dim objBranch As New clsStation
        Dim objJobGroup As New clsJobGroup
        Dim objClassGroup As New clsClassGroup 'Sohail (16 Jun 2015)
        Dim objCity As New clscity_master
        Dim objState As New clsstate_master
        Dim objZipCode As New clszipcode_master
        Dim objPeriod As New clscommom_period_Tran

        Dim dsEmployee As DataSet
        Dim dsCountry As DataSet
        Dim dsTitle As DataSet
        Dim dsMarital As DataSet
        Dim dsDept As DataSet
        Dim dsCCenter As DataSet
        Dim dsJob As DataSet
        Dim dsBranch As DataSet
        Dim dsEmplType As DataSet
        Dim dsJobGroup As DataSet
        Dim dsClassGroup As DataSet 'Sohail (16 Jun 2015)
        Dim dsRelation As DataSet
        Dim dsCity As DataSet
        Dim dsState As DataSet
        Dim dsZipCode As DataSet
        Dim dsPeriod As DataSet

        Try
            gstrFileName = ""

            Dim di As New IO.DirectoryInfo(AppDomain.CurrentDomain.BaseDirectory)
            Dim fi As FileInfo() = di.GetFiles("Z_O_DATA_DARHR_*.txt")
            Dim lstLastFile As FileInfo = (From p In fi.AsEnumerable() Order By p.LastWriteTimeUtc Descending Select (p)).FirstOrDefault()
            If lstLastFile IsNot Nothing AndAlso lstLastFile.Length > 0 Then

                gstrFileName = lstLastFile.FullName

                Call CreateDataTable()


                Dim mdtIdTran As DataTable = Nothing


'S.SANDEEP [28-May-2018] -- START
'ISSUE/ENHANCEMENT : {Audit Trails} 
objEmp._FormName = mstrFormName 
objEmp._LoginEmployeeUnkid = mintLoginEmployeeunkid 
                objEmp._ClientIP = mstrClientIP
                objEmp._HostName = mstrHostName
objEmp._FromWeb = mblnIsWeb 
objEmp._AuditUserId = mintAuditUserId 
objEmp._CompanyUnkid = mintCompanyUnkid
objEmp._AuditDate = mdtAuditDate 
'S.SANDEEP [28-May-2018] -- END

                'S.SANDEEP [04 JUN 2015] -- START
                'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
                'dsEmployee = objEmp.GetList("Emp", , True)
                dsEmployee = objEmp.GetList(FinancialYear._Object._DatabaseName, _
                                         User._Object._Userunkid, _
                                         FinancialYear._Object._YearUnkid, _
                                         Company._Object._Companyunkid, _
                                         eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), _
                                         eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), _
                                         ConfigParameter._Object._UserAccessModeSetting, False, True, _
                                         "Emp", ConfigParameter._Object._ShowFirstAppointmentDate)
                'S.SANDEEP [04 JUN 2015] -- END
                dsCountry = objMaster.getCountryList("Country", False)
                dsTitle = objTitle.GetList(clsCommon_Master.enCommonMaster.TITLE, "List", , True)
                dsMarital = objTitle.GetList(clsCommon_Master.enCommonMaster.MARRIED_STATUS, "List", , True)
                dsDept = objDept.GetList("Dept", True)
                dsCCenter = objCCenter.GetList("CC", True)
                dsJob = objJob.GetList("Job", True)
                dsBranch = objBranch.GetList("Branch", True)
                dsEmplType = objTitle.GetList(clsCommon_Master.enCommonMaster.EMPLOYEMENT_TYPE, "List", , True)
                dsJobGroup = objJobGroup.GetList("JobGroup", True)
                dsClassGroup = objClassGroup.GetList("ClassGroup", True) 'Sohail (16 Jun 2015)
                dsRelation = objTitle.GetList(clsCommon_Master.enCommonMaster.RELATIONS, "List", , True)
                dsCity = objCity.GetList("City", True)
                dsState = objState.GetList("State", True)
                dsZipCode = objZipCode.GetList("ZipCode", True)
                'Sohail (21 Aug 2015) -- Start
                'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
                'dsPeriod = objPeriod.GetList("Period", enModuleReference.Payroll, True, , , True)
                dsPeriod = objPeriod.GetList("Period", enModuleReference.Payroll, intYearUnkId, dtDatabase_Start_Date, True, , , True)
                'Sohail (21 Aug 2015) -- End

                Dim strPrevEmpCode As String = ""
                Dim strPrevGlobalID As String = ""
                Dim intSalaryChangeCount As Integer = 0
                Dim intEmployeeUnkId As Integer = 0
                Dim strEmpCode As String = ""
                Dim strIdentityNo As String = ""
                Dim strIdentityAUD As String = ""
                Dim dtBirthDate As Date = Nothing
                Dim dtTerminationTo As Date = Nothing
                Dim intBirthCountry As Integer = 0
                Dim strBirthCountry As String = ""
                Dim strBirthVillage As String = ""
                Dim blnIsDefault As Boolean = False
                Dim dtAppointed As Date = Nothing
                Dim intTitle As Integer = 0
                Dim strTitle As String = ""
                Dim strFirstName As String = ""
                Dim strOtherName As String = ""
                Dim strSurname As String = ""
                Dim intMaritalStatus As Integer = 0
                Dim strMaritalStatus As String = ""
                Dim dtAnniversary As Date = Nothing
                Dim intGender As Integer = 0
                Dim strGender As String = ""
                Dim strAction As String = ""

                Dim reader As StreamReader = lstLastFile.OpenText
                gintTotalEmployees = IO.File.ReadAllLines(lstLastFile.FullName).Length
                Dim i As Integer = -1


                Dim strLine As String = reader.ReadLine

                Dim dRow As DataRow = Nothing
                Dim dDRow As DataRow = Nothing



                Do While (strLine IsNot Nothing)

                    i += 1

                    Select Case strLine.Trim.Split("|")(0).ToUpper


                        Case strctTable.PARENT_PERSON

                            If strPrevGlobalID <> strLine.Split("|")(enPARENT_PERSON.IDENTITY_NO).Trim Then
                                intSalaryChangeCount = 0
                                intEmployeeUnkId = 0
                                strEmpCode = ""
                                strIdentityNo = ""
                                strIdentityAUD = ""
                                dtBirthDate = Nothing
                                dtTerminationTo = Nothing
                                intBirthCountry = 0
                                strBirthCountry = ""
                                strBirthVillage = ""
                                blnIsDefault = False
                                dtAppointed = Nothing
                                intTitle = 0
                                strTitle = ""
                                strFirstName = ""
                                strOtherName = ""
                                strSurname = ""
                                intMaritalStatus = 0
                                strMaritalStatus = ""
                                dtAnniversary = Nothing
                                intGender = 0
                                strGender = ""
                                strAction = ""

                                objEmp = New clsEmployee_Master
                                objId_Tran = New clsIdentity_tran
                                objDependant = New clsDependants_Beneficiary_tran

                                mdtAllocation.Rows.Clear()


                            End If

                            Dim lstEmp As List(Of DataRow) = (From p In dsEmployee.Tables("Emp") Where (p.Item("employeecode").ToString = strLine.Split("|")(enPARENT_PERSON.EMPLOYEE_CODE).Trim) Select (p)).ToList
                            If lstEmp.Count > 0 Then
                                intEmployeeUnkId = CInt(lstEmp(0).Item("employeeunkid"))

                                strIdentityNo = strLine.Split("|")(enPARENT_PERSON.IDENTITY_NO).Trim
                                strEmpCode = strLine.Split("|")(enPARENT_PERSON.EMPLOYEE_CODE).Trim

                            Else
                                intEmployeeUnkId = 0

                                strIdentityNo = strLine.Split("|")(enPARENT_PERSON.IDENTITY_NO).Trim
                                strEmpCode = strLine.Split("|")(enPARENT_PERSON.EMPLOYEE_CODE).Trim
                            End If



                            If strLine.Split("|")(enPARENT_PERSON.BIRTH_DATE).Trim <> "" Then
                                dtBirthDate = DateTime.ParseExact(strLine.Split("|")(enPARENT_PERSON.BIRTH_DATE).Trim, "MM/dd/yyyy", Nothing)
                                dtTerminationTo = DateTime.ParseExact(strLine.Split("|")(enPARENT_PERSON.BIRTH_DATE).Trim, "MM/dd/yyyy", Nothing).AddYears(60)
                            Else
                                dtBirthDate = Nothing
                                dtTerminationTo = DateAndTime.Today.Date.AddYears(60)
                            End If

                            Dim lstCountry As List(Of DataRow) = (From p In dsCountry.Tables("Country") Where (p.Item("alias").ToString = strLine.Split("|")(enPARENT_PERSON.COUNTRY_ALIAS).Trim) Select (p)).ToList
                            If lstCountry.Count > 0 Then
                                intBirthCountry = CInt(lstCountry(0).Item("countryunkid"))
                                strBirthCountry = lstCountry(0).Item("country_name").ToString
                            Else
                                intBirthCountry = 0
                                strBirthCountry = ""
                            End If
                            strBirthVillage = strLine.Split("|")(enPARENT_PERSON.BIRTH_VILLAGE).Trim

                            objId_Tran._EmployeeUnkid = intEmployeeUnkId
                            mdtIdTran = objId_Tran._DataList


                            If (From p In mdtIdTran Where (CInt(p.Item("employeeunkid")) = intEmployeeUnkId AndAlso CInt(p.Item("idtypeunkid")) = mintIdentityTypeUnkId AndAlso p.Item("identity_no").ToString = strLine.Split("|")(enPARENT_PERSON.IDENTITY_NO).Trim AndAlso p.Item("AUD").ToString <> "D") Select (p)).ToList.Count <= 0 Then
                                Dim dr As DataRow = mdtIdTran.NewRow


                                If (From q In mdtIdTran Where (CInt(q.Item("employeeunkid")) = intEmployeeUnkId AndAlso q.Item("AUD").ToString <> "D") Select (q)).ToList.Count <= 0 Then
                                    blnIsDefault = True
                                Else
                                    blnIsDefault = False
                                End If
                                strIdentityAUD = "A"

                            End If

                            strPrevGlobalID = strLine.Split("|")(enPARENT_PERSON.IDENTITY_NO).Trim

                        Case strctTable.CHILD_NAMES

                            'If objEmp._PSoft_SyncDateTime = DateTime.ParseExact(strLine.Split("|")(enCHILD_NAMES.LAST_UPDATE_TIME).Trim, "MM/dd/yyyy hh:mm", Nothing) Then
                            '    GoTo CONTINUE_WHILE
                            'End If

                            dtAppointed = DateTime.ParseExact(strLine.Split("|")(enCHILD_NAMES.APPOINTMENT_DATE).Trim, "MM/dd/yyyy", Nothing)
                            Dim lstTitle As List(Of DataRow) = (From p In dsTitle.Tables("List") Where (p.Item("code").ToString = strLine.Split("|")(enCHILD_NAMES.TITLE).Trim) Select (p)).ToList
                            If lstTitle.Count > 0 Then
                                intTitle = CInt(lstTitle(0).Item("masterunkid"))
                                strTitle = lstTitle(0).Item("name")
                            Else
                                intTitle = -1
                                strTitle = ""
                            End If
                            strFirstName = strLine.Split("|")(enCHILD_NAMES.FIRST_NAME).Trim
                            strOtherName = strLine.Split("|")(enCHILD_NAMES.OTHER_NAME).Trim
                            strSurname = strLine.Split("|")(enCHILD_NAMES.SURNAME).Trim

                        Case strctTable.CHILD_PERS_DATA_EFFDT

                            'If objEmp._PSoft_SyncDateTime = DateTime.ParseExact(strLine.Split("|")(enCHILD_PERS_DATA_EFFDT.LAST_UPDATE_TIME).Trim, "MM/dd/yyyy hh:mm", Nothing) Then
                            '    GoTo CONTINUE_WHILE
                            'End If

                            Dim lstMarital As List(Of DataRow) = (From p In dsMarital.Tables("List") Where (p.Item("code").ToString = strLine.Split("|")(enCHILD_PERS_DATA_EFFDT.MARITAL_STATUS).Trim) Select (p)).ToList
                            If lstMarital.Count > 0 Then
                                intMaritalStatus = CInt(lstMarital(0).Item("masterunkid"))
                                strMaritalStatus = lstMarital(0).Item("name")
                            Else
                                intMaritalStatus = -1
                                strMaritalStatus = ""
                            End If

                            If strLine.Split("|")(enCHILD_PERS_DATA_EFFDT.ANNIVERSARY_DATE).Trim <> "" Then
                                dtAnniversary = DateTime.ParseExact(strLine.Split("|")(enCHILD_PERS_DATA_EFFDT.ANNIVERSARY_DATE).Trim, "MM/dd/yyyy", Nothing)
                            Else
                                dtAnniversary = Nothing
                            End If

                            If strLine.Split("|")(enCHILD_PERS_DATA_EFFDT.GENDER).Trim = "M" Then
                                intGender = 1
                            ElseIf strLine.Split("|")(enCHILD_PERS_DATA_EFFDT.GENDER).Trim = "F" Then
                                intGender = 2
                            Else
                                intGender = 0
                            End If
                            strGender = strLine.Split("|")(enCHILD_PERS_DATA_EFFDT.GENDER).Trim


                        Case strctTable.PARENT_JOB

                            'If objEmp._PSoft_SyncDateTime = DateTime.ParseExact(strLine.Split("|")(enPARENT_JOB.LAST_UPDATE_TIME).Trim, "MM/dd/yyyy hh:mm", Nothing) Then
                            '    GoTo CONTINUE_WHILE
                            'End If

                            Dim drJob As DataRow = Nothing
                            drJob = mdtAllocation.NewRow

                            drJob.Item("employeeunkid") = intEmployeeUnkId


                            Dim strDeptCode As String = ""
                            Dim lstDept As List(Of DataRow) = (From p In dsDept.Tables("Dept") Where (p.Item("description").ToString = strLine.Split("|")(enPARENT_JOB.DEPT_DESCRIPTION).Trim) Select (p)).ToList
                            If lstDept.Count > 0 Then
                                drJob.Item("departmentunkid") = CInt(lstDept(0).Item("departmentunkid"))
                                strDeptCode = lstDept(0).Item("code").ToString
                                drJob.Item("department") = strDeptCode
                            Else
                                drJob.Item("departmentunkid") = 0
                                drJob.Item("department") = strLine.Split("|")(enPARENT_JOB.DEPT_DESCRIPTION).Trim
                            End If


                            Dim lstCCenter As List(Of DataRow) = (From p In dsCCenter.Tables("CC") Where (p.Item("costcentercode").ToString = strDeptCode) Select (p)).ToList
                            If lstCCenter.Count > 0 Then
                                drJob.Item("costcenterunkid") = CInt(lstCCenter(0).Item("costcenterunkid"))
                                drJob.Item("costcenter") = lstCCenter(0).Item("costcentername")
                            Else
                                drJob.Item("costcenterunkid") = 0
                                drJob.Item("costcenter") = strDeptCode
                            End If

                            Dim lstJob As List(Of DataRow) = (From p In dsJob.Tables("Job") Where (p.Item("Code").ToString = strLine.Split("|")(enPARENT_JOB.JOB_CODE).Trim) Select (p)).ToList
                            If lstJob.Count > 0 Then
                                drJob.Item("jobunkid") = CInt(lstJob(0).Item("jobunkid"))
                                drJob.Item("job") = lstJob(0).Item("jobname")
                            Else
                                drJob.Item("jobunkid") = 0
                                drJob.Item("job") = strLine.Split("|")(enPARENT_JOB.JOB_CODE).Trim
                            End If

                            Dim lstBranch As List(Of DataRow) = (From p In dsBranch.Tables("Branch") Where (p.Item("code").ToString = strLine.Split("|")(enPARENT_JOB.BRANCH).Trim) Select (p)).ToList
                            If lstBranch.Count > 0 Then
                                drJob.Item("stationunkid") = CInt(lstBranch(0).Item("stationunkid"))
                                drJob.Item("station") = lstBranch(0).Item("name")
                            Else
                                drJob.Item("stationunkid") = 0
                                drJob.Item("station") = ""
                            End If

                            If strLine.Split("|")(enPARENT_JOB.EOC_DATE).Trim <> "" Then
                                drJob.Item("empl_enddate") = DateTime.ParseExact(strLine.Split("|")(enPARENT_JOB.EOC_DATE).Trim, "MM/dd/yyyy", Nothing)
                            End If

                            Dim lstEmplType As List(Of DataRow) = (From p In dsEmplType.Tables("List") Where (p.Item("code").ToString = strLine.Split("|")(enPARENT_JOB.EMPLOYMENT_TYPE).Trim) Select (p)).ToList
                            If lstEmplType.Count > 0 Then
                                drJob.Item("employmenttypeunkid") = CInt(lstEmplType(0).Item("masterunkid"))
                                drJob.Item("employmenttype") = lstEmplType(0).Item("name")
                            Else
                                drJob.Item("employmenttypeunkid") = 0
                                drJob.Item("employmenttype") = strLine.Split("|")(enPARENT_JOB.EMPLOYMENT_TYPE).Trim
                            End If

                            Dim lstJobGroup As List(Of DataRow) = (From p In dsJobGroup.Tables("JobGroup") Where (p.Item("code").ToString = strLine.Split("|")(enPARENT_JOB.JOB_GROUP_CODE).Trim) Select (p)).ToList
                            If lstJobGroup.Count > 0 Then
                                drJob.Item("jobgroupunkid") = CInt(lstJobGroup(0).Item("jobgroupunkid"))
                                drJob.Item("jobgroup") = lstJobGroup(0).Item("name")
                            Else
                                drJob.Item("jobgroupunkid") = 0
                                drJob.Item("jobgroup") = ""
                            End If

                            'Sohail (16 Jun 2015) -- Start
                            'Enhancement - Now pick employment type from Fulltime Parttime column and pick class group from employmenttype column as per rutta's request to anjan sir on skype.
                            Dim lstClassGroup As List(Of DataRow) = (From p In dsClassGroup.Tables("ClassGroup") Where (p.Item("code").ToString = strLine.Split("|")(enPARENT_JOB.CLASS_GROUP_CODE).Trim) Select (p)).ToList
                            If lstClassGroup.Count > 0 Then
                                drJob.Item("classgroupunkid") = CInt(lstClassGroup(0).Item("classgroupunkid"))
                                drJob.Item("classgroup") = lstClassGroup(0).Item("name")
                            Else
                                drJob.Item("classgroupunkid") = 0
                                drJob.Item("classgroup") = ""
                            End If
                            'Sohail (16 Jun 2015) -- End

                            If strLine.Split("|")(enPARENT_JOB.LEAVING_DATE).Trim <> "" Then
                                drJob.Item("termination_from_date") = DateTime.ParseExact(strLine.Split("|")(enPARENT_JOB.LEAVING_DATE).Trim, "MM/dd/yyyy", Nothing)
                            End If
                            If strLine.Split("|")(enPARENT_JOB.ACTION).Trim = "TER" Then
                                drJob.Item("termination_from_date") = DateTime.ParseExact(strLine.Split("|")(enPARENT_JOB.ALLOCATION_CHANGE_DATE).Trim, "MM/dd/yyyy", Nothing)
                            End If

                            drJob.Item("allocation_change_date") = DateTime.ParseExact(strLine.Split("|")(enPARENT_JOB.ALLOCATION_CHANGE_DATE).Trim, "MM/dd/yyyy", Nothing)

                            If strLine.Split("|")(enPARENT_JOB.ACTION).Trim = "REH" Then
                                drJob.Item("reinstatement_date") = DateTime.ParseExact(strLine.Split("|")(enPARENT_JOB.ALLOCATION_CHANGE_DATE).Trim, "MM/dd/yyyy", Nothing)
                            End If

                            mdtAllocation.Rows.Add(drJob)

                            strAction = strLine.Split("|")(enPARENT_JOB.ACTION).Trim

                        Case strctTable.CHILD_COMPENSATION

                            'If objEmp._PSoft_SyncDateTime = DateTime.ParseExact(strLine.Split("|")(enCHILD_COMPENSATION.LAST_UPDATE_TIME).Trim, "MM/dd/yyyy hh:mm", Nothing) Then
                            '    GoTo CONTINUE_WHILE
                            'End If

                            'If strPrevEmpCode = strLine.Split("|")(enCHILD_COMPENSATION.EMPLOYEE_CODE).Trim Then
                            '    intSalaryChangeCount += 1
                            'End If
                            If strPrevGlobalID = strLine.Split("|")(enCHILD_COMPENSATION.IDENTITY_NO).Trim Then
                                intSalaryChangeCount += 1
                            End If

                            dRow = mdtTable.NewRow

                            If strLine.Split("|")(enCHILD_COMPENSATION.LAST_UPDATE_TIME).Trim = "" Then
                                dRow.Item("rowtypeid") = 1
                                dRow.Item("remark") = "Last Update time is required for COMPENSATION." & " (Line no. " & i.ToString & ")"
                            End If

                            'S.SANDEEP [04 JUN 2015] -- START
                            'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
                            'objEmp._Employeeunkid = intEmployeeUnkId
                            objEmp._Employeeunkid(eZeeDate.convertDate(strEmployeeAsOnDate)) = intEmployeeUnkId
                            'S.SANDEEP [04 JUN 2015] -- END


                            If dRow.Item("remark").ToString.Trim = "" Then
                                If intEmployeeUnkId > 0 AndAlso strLine.Split("|")(enCHILD_COMPENSATION.LAST_UPDATE_TIME).Trim <> "" AndAlso objEmp._PSoft_SyncDateTime = DateTime.ParseExact(strLine.Split("|")(enCHILD_COMPENSATION.LAST_UPDATE_TIME).Trim, "MM/dd/yyyy hh:mm", Nothing) Then
                                    dRow.Item("rowtypeid") = 2
                                    dRow.Item("remark") = "Transaction is already Imported." & " (Line no. " & i.ToString & ")"

                                ElseIf strIdentityNo.Trim = "" Then
                                    dRow.Item("rowtypeid") = 3
                                    dRow.Item("remark") = "GLOBAL ID is required." & " (Line no. " & i.ToString & ")"

                                ElseIf strEmpCode.Trim = "" Then

                                    If ConfigParameter._Object._EmployeeCodeNotype = 0 Then
                                        dRow.Item("rowtypeid") = 4
                                        dRow.Item("remark") = "LOCAL ID is required." & " (Line no. " & i.ToString & ")"
                                    Else
                                        Dim obj As New clsDataOperation
                                        strQ = "SELECT 1 FROM hremployee_idinfo_tran WHERE identity_no = '" & strIdentityNo & "' "
                                        If obj.RecordCount(strQ) > 0 Then
                                            dRow.Item("rowtypeid") = 4
                                            dRow.Item("remark") = "LOCAL ID is required." & " (Line no. " & i.ToString & ")"
                                        End If
                                        obj = Nothing
                                    End If


                                ElseIf strFirstName.Trim = "" Then
                                    dRow.Item("rowtypeid") = 5
                                    dRow.Item("remark") = "First Name is required." & " (Line no. " & i.ToString & ")"

                                ElseIf strSurname.Trim = "" Then
                                    dRow.Item("rowtypeid") = 6
                                    dRow.Item("remark") = "Surname is required." & " (Line no. " & i.ToString & ")"


                                ElseIf CDec(strLine.Split("|")(enCHILD_COMPENSATION.NEW_SCALE).Trim) < 0 Then
                                    dRow.Item("rowtypeid") = 7
                                    dRow.Item("remark") = "Scale should be greater than Zero." & " (Line no. " & i.ToString & ")"

                                End If
                            End If

                            If dRow.Item("remark").ToString.Trim = "" Then
                                Dim obj As New clsDataOperation
                                strQ = "SELECT 1 FROM hremployee_idinfo_tran WHERE identity_no = '" & strIdentityNo & "' AND employeeunkid <> " & intEmployeeUnkId & " "
                                If obj.RecordCount(strQ) > 0 Then
                                    dRow.Item("rowtypeid") = 18
                                    dRow.Item("remark") = "GLOBAL ID is assigned to different employee in Aruti." & " (Line no. " & i.ToString & ")"
                                End If
                                obj = Nothing
                            End If

                            If dRow.Item("remark").ToString.Trim = "" Then

                                If mdtAllocation.Rows.Count >= intSalaryChangeCount Then

                                    If CInt(mdtAllocation.Rows(intSalaryChangeCount - 1).Item("employmenttypeunkid")) <= 0 Then
                                        dRow.Item("rowtypeid") = 8
                                        dRow.Item("remark") = "Employment Type is required." & " (Line no. " & i.ToString & ")"

                                    ElseIf CInt(mdtAllocation.Rows(intSalaryChangeCount - 1).Item("departmentunkid")) <= 0 Then
                                        dRow.Item("rowtypeid") = 9
                                        dRow.Item("remark") = "Department is required." & " (Line no. " & i.ToString & ")"

                                    ElseIf CInt(mdtAllocation.Rows(intSalaryChangeCount - 1).Item("jobunkid")) <= 0 Then
                                        dRow.Item("rowtypeid") = 10
                                        dRow.Item("remark") = "Job is required." & " (Line no. " & i.ToString & ")"

                                    ElseIf CInt(mdtAllocation.Rows(intSalaryChangeCount - 1).Item("costcenterunkid")) <= 0 Then
                                        dRow.Item("rowtypeid") = 11
                                        dRow.Item("remark") = "Cost Center is required." & " (Line no. " & i.ToString & ")"

                                        'Sohail (16 Jun 2015) -- Start
                                        'Enhancement - Now pick employment type from Fulltime Parttime column and pick class group from employmenttype column as per rutta's request to anjan sir on skype.
                                    ElseIf CInt(mdtAllocation.Rows(intSalaryChangeCount - 1).Item("classgroupunkid")) <= 0 Then
                                        dRow.Item("rowtypeid") = 20
                                        dRow.Item("remark") = "Either Class Group is not given or does not exist in Aruti." & " (Line no. " & i.ToString & ")"
                                        'Sohail (16 Jun 2015) -- End
                                    End If

                                End If

                            End If

                            If dRow.Item("remark").ToString.Trim = "" Then

                                If mdtAllocation.Rows.Count >= intSalaryChangeCount Then
                                    If CInt(mdtAllocation.Rows(intSalaryChangeCount - 1).Item("jobgroupunkid")) > 0 Then
                                        If (From p In dsJob.Tables(0) Where (CInt(p.Item("jobgroupunkid")) = CInt(mdtAllocation.Rows(intSalaryChangeCount - 1).Item("jobgroupunkid")) AndAlso CInt(p.Item("jobunkid")) = CInt(mdtAllocation.Rows(intSalaryChangeCount - 1).Item("jobunkid"))) Select (p)).ToList.Count <= 0 Then
                                            dRow.Item("rowtypeid") = 12
                                            dRow.Item("remark") = "Job is not linked with given Job Group." & " (Line no. " & i.ToString & ")"
                                        End If
                                    End If
                                End If

                            End If


                            If dRow.Item("remark").ToString.Trim = "" Then

                                Dim decMinScale As Decimal
                                Dim decMaxScale As Decimal
                                Dim objWagesTran As New clsWagesTran
                                Dim dsList As DataSet = objWagesTran.getScaleInfo(mintGradeUnkId, mintGradeLevelUnkId, "Scale")
                                If dsList.Tables("Scale").Rows.Count > 0 Then
                                    decMinScale = CDec(dsList.Tables("Scale").Rows(0).Item("salary").ToString)
                                    decMaxScale = CDec(dsList.Tables("Scale").Rows(0).Item("maximum").ToString)
                                    If CDec(strLine.Split("|")(enCHILD_COMPENSATION.NEW_SCALE).Trim) < decMinScale OrElse CDec(strLine.Split("|")(enCHILD_COMPENSATION.NEW_SCALE).Trim) > decMaxScale Then

                                        dRow.Item("rowtypeid") = 13
                                        dRow.Item("remark") = "Sorry, Scale should be in between  " & decMinScale & " And " & decMaxScale & " (Line no. " & i.ToString & ")"

                                    End If
                                Else
                                    dRow.Item("rowtypeid") = 14
                                    dRow.Item("remark") = "Scale is not defined for this grade and level" & " (Line no. " & i.ToString & ")"
                                End If

                            End If

                            'S.SANDEEP [04 JUN 2015] -- START
                            'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
                            'Dim intPeriod As Integer = objMaster.getCurrentPeriodID(enModuleReference.Payroll, DateTime.ParseExact(strLine.Split("|")(enCHILD_COMPENSATION.INCREMENT_DATE).Trim, "MM/dd/yyyy", Nothing), 0, , True)
                            Dim intPeriod As Integer = objMaster.getCurrentPeriodID(enModuleReference.Payroll, DateTime.ParseExact(strLine.Split("|")(enCHILD_COMPENSATION.INCREMENT_DATE).Trim, "MM/dd/yyyy", Nothing), intYearUnkId, 0, True)
                            'S.SANDEEP [04 JUN 2015] -- END

                            If dRow.Item("remark").ToString.Trim = "" Then 'AndAlso strAction = "PAY"
                                If objSalary.isExistOnSameDate(DateTime.ParseExact(strLine.Split("|")(enCHILD_COMPENSATION.INCREMENT_DATE).Trim, "MM/dd/yyyy", Nothing).Date, intEmployeeUnkId) = True Then
                                    'dRow.Item("rowtypeid") = 17 'Please do no change 17# as it is used in filter of btnImport click event
                                    'dRow.Item("remark") = "This transaction is already exist on given date." & " (Line no. " & i.ToString & ")"
                                Else
                                    Dim ds As DataSet = objSalary.getLastIncrement("Incr", intEmployeeUnkId, True)
                                    If ds.Tables("Incr").Rows.Count > 0 Then
                                        If eZeeDate.convertDate(DateTime.ParseExact(strLine.Split("|")(enCHILD_COMPENSATION.INCREMENT_DATE).Trim, "MM/dd/yyyy", Nothing).Date) < ds.Tables("Incr").Rows(0).Item("incrementdate").ToString Then
                                            dRow.Item("rowtypeid") = 15
                                            dRow.Item("remark") = "Increment date should be greater than last increment date." & " (Line no. " & i.ToString & ")"

                                        ElseIf CBool(ds.Tables("Incr").Rows(0).Item("isapproved")) = False Then
                                            dRow.Item("rowtypeid") = 16
                                            dRow.Item("remark") = "Last Salary Change is not Approved." & " (Line no. " & i.ToString & ")"

                                        End If
                                    End If


                                    If dRow.Item("remark").ToString.Trim = "" Then
                                        If intPeriod > 0 Then
                                            If (From p In dsPeriod.Tables(0) Where (CInt(p.Item("periodunkid")) = intPeriod AndAlso CInt(p.Item("statusid")) = StatusType.Open) Select (p)).ToList.Count <= 0 Then
                                                dRow.Item("rowtypeid") = 19
                                                dRow.Item("remark") = "Period of the transaction date is already closed." & " (Line no. " & i.ToString & ")"
                                            End If
                                        End If
                                    End If
                                End If
                            End If

                            'If dRow.Item("remark").ToString.Trim = "" Then
                            '    Dim ds As DataSet = objSalary.getLastIncrement("Incr", intEmployeeUnkId, True)
                            '    If ds.Tables("Incr").Rows.Count > 0 Then
                            '        If eZeeDate.convertDate(DateTime.ParseExact(strLine.Split("|")(enCHILD_COMPENSATION.INCREMENT_DATE).Trim, "MM/dd/yyyy", Nothing).Date) < ds.Tables("Incr").Rows(0).Item("incrementdate").ToString Then
                            '            dRow.Item("rowtypeid") = 15
                            '            dRow.Item("remark") = "Increment date should be greater than last increment date." & " (Line no. " & i.ToString & ")"

                            '        ElseIf CBool(ds.Tables("Incr").Rows(0).Item("isapproved")) = False Then
                            '            dRow.Item("rowtypeid") = 16
                            '            dRow.Item("remark") = "Last Salary Change is not Approved." & " (Line no. " & i.ToString & ")"

                            '        End If
                            '    End If
                            'End If

                            'Dim intPeriod As Integer = objMaster.getCurrentPeriodID(enModuleReference.Payroll, DateTime.ParseExact(strLine.Split("|")(enCHILD_COMPENSATION.INCREMENT_DATE).Trim, "MM/dd/yyyy", Nothing), 0, , True)
                            'If dRow.Item("remark").ToString.Trim = "" Then
                            '    If intPeriod > 0 Then
                            '        If (From p In dsPeriod.Tables(0) Where (CInt(p.Item("periodunkid")) = intPeriod AndAlso CInt(p.Item("statusid")) = StatusType.Open) Select (p)).ToList.Count <= 0 Then
                            '            dRow.Item("rowtypeid") = 19
                            '            dRow.Item("remark") = "Period of the transaction date is already closed." & " (Line no. " & i.ToString & ")"
                            '        End If
                            '    End If
                            'End If

                            If dRow.Item("remark").ToString.Trim = "" AndAlso intEmployeeUnkId <= 0 Then
                                Dim lstRow As DataRow = (From p In mdtTable Where (p.Item("identity_no").ToString = strIdentityNo AndAlso p.Item("remark").ToString <> "") Select (p)).FirstOrDefault
                                If lstRow IsNot Nothing Then
                                    dRow.Item("rowtypeid") = lstRow.Item("rowtypeid")
                                    dRow.Item("remark") = lstRow.Item("remark")
                                Else
                                    'dRow.Item("remark") = "New Employee"
                                    dRow.Item("remark") = strAction
                                End If

                            End If

                            dRow.Item("employeeunkid") = intEmployeeUnkId
                            dRow.Item("employeecode") = strEmpCode
                            dRow.Item("displayname") = strEmpCode
                            dRow.Item("password") = strEmpCode
                            dRow.Item("shiftunkid") = mintShiftUnkId
                            dRow.Item("shift") = mstrShiftName
                            dRow.Item("birthdate") = dtBirthDate
                            dRow.Item("termination_to_date") = dtTerminationTo
                            dRow.Item("birthcountryunkid") = intBirthCountry
                            dRow.Item("birthcountry") = strBirthCountry
                            dRow.Item("birth_village") = strBirthVillage
                            dRow.Item("appointeddate") = dtAppointed
                            dRow.Item("titleunkid") = intTitle
                            dRow.Item("title") = strTitle
                            dRow.Item("firstname") = strFirstName
                            dRow.Item("othername") = strOtherName
                            dRow.Item("surname") = strSurname
                            dRow.Item("maritalstatusunkid") = intMaritalStatus
                            dRow.Item("maritalstatus") = strMaritalStatus
                            dRow.Item("anniversary_date") = dtAnniversary
                            dRow.Item("gender") = intGender
                            dRow.Item("gendername") = strGender

                            If mdtAllocation.Rows.Count >= intSalaryChangeCount Then
                                dRow.Item("departmentunkid") = mdtAllocation.Rows(intSalaryChangeCount - 1).Item("departmentunkid")
                                dRow.Item("department") = mdtAllocation.Rows(intSalaryChangeCount - 1).Item("department")
                                dRow.Item("costcenterunkid") = mdtAllocation.Rows(intSalaryChangeCount - 1).Item("costcenterunkid")
                                dRow.Item("costcenter") = mdtAllocation.Rows(intSalaryChangeCount - 1).Item("costcenter")
                                dRow.Item("jobunkid") = mdtAllocation.Rows(intSalaryChangeCount - 1).Item("jobunkid")
                                dRow.Item("job") = mdtAllocation.Rows(intSalaryChangeCount - 1).Item("job")
                                dRow.Item("stationunkid") = mdtAllocation.Rows(intSalaryChangeCount - 1).Item("stationunkid")
                                dRow.Item("station") = mdtAllocation.Rows(intSalaryChangeCount - 1).Item("station")
                                dRow.Item("empl_enddate") = mdtAllocation.Rows(intSalaryChangeCount - 1).Item("empl_enddate")
                                dRow.Item("employmenttypeunkid") = mdtAllocation.Rows(intSalaryChangeCount - 1).Item("employmenttypeunkid")
                                dRow.Item("employmenttype") = mdtAllocation.Rows(intSalaryChangeCount - 1).Item("employmenttype")
                                dRow.Item("jobgroupunkid") = mdtAllocation.Rows(intSalaryChangeCount - 1).Item("jobgroupunkid")
                                dRow.Item("jobgroup") = mdtAllocation.Rows(intSalaryChangeCount - 1).Item("jobgroup")
                                'Sohail (16 Jun 2015) -- Start
                                'Enhancement - Now pick employment type from Fulltime Parttime column and pick class group from employmenttype column as per rutta's request to anjan sir on skype.
                                dRow.Item("classgroupunkid") = mdtAllocation.Rows(intSalaryChangeCount - 1).Item("classgroupunkid")
                                dRow.Item("classgroup") = mdtAllocation.Rows(intSalaryChangeCount - 1).Item("classgroup")
                                'Sohail (16 Jun 2015) -- End
                                dRow.Item("termination_from_date") = mdtAllocation.Rows(intSalaryChangeCount - 1).Item("termination_from_date")
                                dRow.Item("allocation_change_date") = mdtAllocation.Rows(intSalaryChangeCount - 1).Item("allocation_change_date")
                                dRow.Item("reinstatement_date") = mdtAllocation.Rows(intSalaryChangeCount - 1).Item("reinstatement_date")
                            End If
                            If strLine.Split("|")(enCHILD_COMPENSATION.LAST_UPDATE_TIME).Trim <> "" Then
                                dRow.Item("last_update_time") = DateTime.ParseExact(strLine.Split("|")(enCHILD_COMPENSATION.LAST_UPDATE_TIME).Trim, "MM/dd/yyyy hh:mm", Nothing)
                            Else
                                dRow.Item("last_update_time") = DBNull.Value
                            End If



                            If intSalaryChangeCount = 1 Then
                                dRow.Item("identity_no") = strIdentityNo
                                dRow.Item("idtypeunkid") = mintIdentityTypeUnkId
                                dRow.Item("isdefault") = blnIsDefault
                                dRow.Item("identity_aud") = strIdentityAUD
                            Else
                                dRow.Item("identity_no") = strIdentityNo '""
                                dRow.Item("idtypeunkid") = 0
                                dRow.Item("isdefault") = False
                                dRow.Item("identity_aud") = ""
                            End If


                            dRow.Item("scale") = CDec(strLine.Split("|")(enCHILD_COMPENSATION.NEW_SCALE).Trim)
                            If strLine.Split("|")(enCHILD_COMPENSATION.CURRENCY).Trim = "USD" Then
                                dRow.Item("tranhedunkid") = mintUSDSalaryUnkId
                                dRow.Item("tranhed") = mstrUSDSalaryName
                                dRow.Item("trnheadtype_id") = mintUSDSalaryHeadTypeId
                            Else
                                dRow.Item("tranhedunkid") = mintTZSSalaryUnkId
                                dRow.Item("tranhed") = mstrTZSSalaryName
                                dRow.Item("trnheadtype_id") = mintTZSSalaryHeadTypeId
                            End If

                            'Sohail (18 Feb 2019) -- Start
                            'NMB Enhancement - 76.1 - Provide setting on transaction head to mark as default heads.
                            dRow.Item("AssignDefaulTranHeads") = mblnAssignDefaulTranHeads
                            'Sohail (18 Feb 2019) -- End

                            dRow.Item("incrementdate") = DateTime.ParseExact(strLine.Split("|")(enCHILD_COMPENSATION.INCREMENT_DATE).Trim, "MM/dd/yyyy", Nothing)

                            Dim dsScale As DataSet = objMaster.Get_Current_Scale("Scale", intEmployeeUnkId, DateTime.ParseExact(strLine.Split("|")(enCHILD_COMPENSATION.INCREMENT_DATE).Trim, "MM/dd/yyyy", Nothing))
                            If dsScale.Tables(0).Rows.Count > 0 Then

                                dRow.Item("gradegroupunkid") = CInt(dsScale.Tables(0).Rows(0).Item("gradegroupunkid"))
                                dRow.Item("gradegroup") = dsScale.Tables(0).Rows(0).Item("GradeGroup")
                                dRow.Item("gradeunkid") = CInt(dsScale.Tables(0).Rows(0).Item("gradeunkid"))
                                dRow.Item("grade") = dsScale.Tables(0).Rows(0).Item("Grade")
                                dRow.Item("gradelevelunkid") = CInt(dsScale.Tables(0).Rows(0).Item("gradelevelunkid"))
                                dRow.Item("gradelevel") = dsScale.Tables(0).Rows(0).Item("GradeLevel")

                                dRow.Item("periodunkid") = intPeriod
                                'Sohail (21 Aug 2015) -- Start
                                'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
                                If intPeriod > 0 Then
                                    objPeriod = New clscommom_period_Tran
                                    objPeriod._Periodunkid(strDatabaseName) = intPeriod
                                    dRow.Item("start_date") = eZeeDate.convertDate(objPeriod._Start_Date)
                                    dRow.Item("end_date") = eZeeDate.convertDate(objPeriod._End_Date)
                                Else
                                    dRow.Item("start_date") = strEmployeeAsOnDate
                                    dRow.Item("end_date") = strEmployeeAsOnDate
                                End If
                                'Sohail (21 Aug 2015) -- End
                                dRow.Item("currentscale") = CDec(dsScale.Tables(0).Rows(0).Item("newscale"))
                                dRow.Item("newscale") = CDec(strLine.Split("|")(enCHILD_COMPENSATION.NEW_SCALE).Trim)
                                dRow.Item("increment") = CDec(strLine.Split("|")(enCHILD_COMPENSATION.NEW_SCALE).Trim) - CDec(dsScale.Tables(0).Rows(0).Item("newscale"))
                                dRow.Item("isgradechange") = False
                                dRow.Item("isfromemployee") = False
                                dRow.Item("reason_id") = mintSalaryChangeUnkId
                                dRow.Item("reason") = mstrSalaryChangeName
                                dRow.Item("increment_mode") = enSalaryIncrementBy.Amount

                            Else
                                dRow.Item("gradegroupunkid") = mintGradeGroupUnkId
                                dRow.Item("gradegroup") = mstrGradeGroupName
                                dRow.Item("gradeunkid") = mintGradeUnkId
                                dRow.Item("grade") = mstrGradeName
                                dRow.Item("gradelevelunkid") = mintGradeLevelUnkId
                                dRow.Item("gradelevel") = mstrGradeLevelName

                                If intSalaryChangeCount = 1 Then
                                    dRow.Item("periodunkid") = 0
                                    'Sohail (21 Aug 2015) -- Start
                                    'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
                                    dRow.Item("start_date") = strEmployeeAsOnDate
                                    dRow.Item("end_date") = strEmployeeAsOnDate
                                    'Sohail (21 Aug 2015) -- End
                                Else
                                    dRow.Item("periodunkid") = intPeriod
                                    'Sohail (21 Aug 2015) -- Start
                                    'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
                                    If intPeriod > 0 Then
                                        objPeriod = New clscommom_period_Tran
                                        'Sohail (21 Aug 2015) -- Start
                                        'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
                                        'objPeriod._Periodunkid = intPeriod
                                        objPeriod._Periodunkid(strDatabaseName) = intPeriod
                                        'Sohail (21 Aug 2015) -- End
                                        dRow.Item("start_date") = eZeeDate.convertDate(objPeriod._Start_Date)
                                        dRow.Item("end_date") = eZeeDate.convertDate(objPeriod._End_Date)
                                    Else
                                        dRow.Item("start_date") = strEmployeeAsOnDate
                                        dRow.Item("end_date") = strEmployeeAsOnDate
                                    End If
                                End If
                                dRow.Item("currentscale") = CDec(strLine.Split("|")(enCHILD_COMPENSATION.NEW_SCALE).Trim)
                                dRow.Item("newscale") = CDec(strLine.Split("|")(enCHILD_COMPENSATION.NEW_SCALE).Trim)
                                dRow.Item("increment") = 0
                                dRow.Item("isgradechange") = False
                                If intSalaryChangeCount = 1 Then
                                    dRow.Item("isfromemployee") = True
                                    dRow.Item("reason_id") = 0
                                    dRow.Item("reason") = ""
                                    dRow.Item("increment_mode") = 0
                                Else
                                    dRow.Item("isfromemployee") = False
                                    dRow.Item("reason_id") = mintSalaryChangeUnkId
                                    dRow.Item("reason") = mstrSalaryChangeName
                                    dRow.Item("increment_mode") = enSalaryIncrementBy.Amount
                                End If

                            End If

                            mdtTable.Rows.Add(dRow)

                        Case strctTable.CHILD_DEPENDENT_BENEF


                            dDRow = mdtDependant.NewRow

                            If strLine.Split("|")(enCHILD_DEPENDENT_BENEF.LAST_UPDATE_TIME).Trim = "" Then
                                dDRow.Item("rowtypeid") = 6
                                dDRow.Item("remark") = "Last Update time is required for DEPENDENT_BENEF." & " (Line no. " & i.ToString & ")"
                            End If

                            If strLine.Split("|")(enCHILD_DEPENDENT_BENEF.LAST_UPDATE_TIME).Trim <> "" AndAlso objEmp._PSoft_SyncDateTime = DateTime.ParseExact(strLine.Split("|")(enCHILD_DEPENDENT_BENEF.LAST_UPDATE_TIME).Trim, "MM/dd/yyyy hh:mm", Nothing) Then
                                GoTo CONTINUE_WHILE
                            End If

                            If dDRow.Item("remark").ToString.Trim = "" Then
                                If strLine.Split("|")(enCHILD_DEPENDENT_BENEF.EMPLOYEE_CODE).Trim = "" AndAlso ConfigParameter._Object._EmployeeCodeNotype = 0 Then
                                    dDRow.Item("rowtypeid") = 2
                                    dDRow.Item("remark") = "Local ID is required." & " (Line no. " & i.ToString & ")"

                                ElseIf strLine.Split("|")(enCHILD_DEPENDENT_BENEF.IDENTITY_NO).Trim() = "" Then
                                    dDRow.Item("rowtypeid") = 1
                                    dDRow.Item("remark") = "Global ID is required." & " (Line no. " & i.ToString & ")"

                                ElseIf strLine.Split("|")(enCHILD_DEPENDENT_BENEF.FIRST_NAME).Trim() = "" Then
                                    dDRow.Item("rowtypeid") = 3
                                    dDRow.Item("remark") = "First Name is required." & " (Line no. " & i.ToString & ")"

                                ElseIf strLine.Split("|")(enCHILD_DEPENDENT_BENEF.SURNAME).Trim() = "" Then
                                    dDRow.Item("rowtypeid") = 4
                                    dDRow.Item("remark") = "Surname is required." & " (Line no. " & i.ToString & ")"

                                ElseIf ConfigParameter._Object._IsDependant_AgeLimit_Set = True Then

                                    If strLine.Split("|")(enCHILD_DEPENDENT_BENEF.BIRTH_DATE).Trim = "" Then
                                        dDRow.Item("rowtypeid") = 5
                                        dDRow.Item("remark") = "Dependent Birth Date is required as Age Limit set on configuration." & " (Line no. " & i.ToString & ")"

                                    Else
                                        If DateTime.ParseExact(strLine.Split("|")(enCHILD_DEPENDENT_BENEF.BIRTH_DATE).Trim, "MM/dd/yyyy", Nothing).Date >= ConfigParameter._Object._CurrentDateAndTime.Date Then
                                            'Date of birth cannot be greater than or equal to today date
                                            dDRow.Item("rowtypeid") = 7
                                            dDRow.Item("remark") = "Dependent Birth Date cannot be greater than or equal to today date." & " (Line no. " & i.ToString & ")"
                                        End If
                                    End If


                                Else

                                    Dim Expression As New System.Text.RegularExpressions.Regex(iEmailRegxExpression)

                                    If strLine.Split("|")(enCHILD_DEPENDENT_BENEF.EMAIL).Trim.Length > 0 Then
                                        If Expression.IsMatch(strLine.Split("|")(enCHILD_DEPENDENT_BENEF.EMAIL).Trim) = False Then
                                            dDRow.Item("rowtypeid") = 8
                                            dDRow.Item("remark") = "Invalid Email." & " (Line no. " & i.ToString & ")"
                                        End If
                                    End If

                                End If
                            End If

                            dDRow.Item("employeeunkid") = intEmployeeUnkId
                            dDRow.Item("employeecode") = strLine.Split("|")(enCHILD_DEPENDENT_BENEF.EMPLOYEE_CODE).Trim
                            dDRow.Item("identity_no") = strLine.Split("|")(enCHILD_DEPENDENT_BENEF.IDENTITY_NO).Trim
                            dDRow.Item("first_name") = strLine.Split("|")(enCHILD_DEPENDENT_BENEF.FIRST_NAME).Trim
                            dDRow.Item("middle_name") = strLine.Split("|")(enCHILD_DEPENDENT_BENEF.OTHER_NAME).Trim
                            dDRow.Item("last_name") = strLine.Split("|")(enCHILD_DEPENDENT_BENEF.SURNAME).Trim


                            Dim lstRelation As List(Of DataRow) = (From p In dsRelation.Tables("List") Where (p.Item("code").ToString = strLine.Split("|")(enCHILD_DEPENDENT_BENEF.RELATION).Trim) Select (p)).ToList
                            If lstRelation.Count > 0 Then
                                dDRow.Item("relationunkid") = CInt(lstRelation(0).Item("masterunkid"))
                                dDRow.Item("relation") = lstRelation(0).Item("name")
                            Else
                                dDRow.Item("relationunkid") = 0
                                dDRow.Item("relation") = ""
                            End If

                            If strLine.Split("|")(enCHILD_DEPENDENT_BENEF.BIRTH_DATE).Trim <> "" Then
                                dDRow.Item("birthdate") = DateTime.ParseExact(strLine.Split("|")(enCHILD_DEPENDENT_BENEF.BIRTH_DATE).Trim, "MM/dd/yyyy", Nothing)
                            Else
                                dDRow.Item("birthdate") = DBNull.Value
                            End If

                            dDRow.Item("address") = strLine.Split("|")(enCHILD_DEPENDENT_BENEF.ADDRESS_1).Trim
                            dDRow.Item("mobile_no") = strLine.Split("|")(enCHILD_DEPENDENT_BENEF.MOBILE_NO).Trim
                            dDRow.Item("post_box") = ""

                            Dim lstCountry As List(Of DataRow) = (From p In dsCountry.Tables("Country") Where (p.Item("alias").ToString = strLine.Split("|")(enCHILD_DEPENDENT_BENEF.COUNTRY_ALIAS).Trim) Select (p)).ToList
                            If lstCountry.Count > 0 Then
                                dDRow.Item("countryunkid") = CInt(lstCountry(0).Item("countryunkid"))
                                dDRow.Item("country") = lstCountry(0).Item("country_name")
                            Else
                                dDRow.Item("countryunkid") = 0
                                dDRow.Item("country") = ""
                            End If


                            Dim lstCity As List(Of DataRow) = (From p In dsCity.Tables("City") Where (p.Item("code").ToString = strLine.Split("|")(enCHILD_DEPENDENT_BENEF.CITY).Trim) Select (p)).ToList
                            If lstCity.Count > 0 Then
                                dDRow.Item("cityunkid") = CInt(lstCity(0).Item("cityunkid"))
                                dDRow.Item("city") = lstCity(0).Item("city")
                            Else
                                dDRow.Item("cityunkid") = 0
                                dDRow.Item("city") = ""
                            End If

                            Dim lstState As List(Of DataRow) = (From p In dsState.Tables("State") Where (p.Item("code").ToString = strLine.Split("|")(enCHILD_DEPENDENT_BENEF.STATE).Trim) Select (p)).ToList
                            If lstState.Count > 0 Then
                                dDRow.Item("stateunkid") = CInt(lstState(0).Item("stateunkid"))
                                dDRow.Item("state") = lstState(0).Item("state")
                            Else
                                dDRow.Item("stateunkid") = 0
                                dDRow.Item("state") = ""
                            End If

                            dDRow.Item("email") = strLine.Split("|")(enCHILD_DEPENDENT_BENEF.EMAIL).Trim
                            Dim lstZipCode As List(Of DataRow) = (From p In dsZipCode.Tables("ZipCode") Where (p.Item("zipcode_code").ToString = strLine.Split("|")(enCHILD_DEPENDENT_BENEF.STATE).Trim) Select (p)).ToList
                            If lstZipCode.Count > 0 Then
                                objDependant._Zipcodeunkid = CInt(lstZipCode(0).Item("zipcodeunkid"))
                            End If

                            If strLine.Split("|")(enCHILD_DEPENDENT_BENEF.GENDER).Trim = "M" Then
                                dDRow.Item("gender") = 1
                            ElseIf strLine.Split("|")(enCHILD_DEPENDENT_BENEF.GENDER).Trim = "F" Then
                                dDRow.Item("gender") = 2
                            Else
                                dDRow.Item("gender") = 0
                            End If
                            dDRow.Item("gendername") = strLine.Split("|")(enCHILD_DEPENDENT_BENEF.GENDER).Trim

                            mdtDependant.Rows.Add(dDRow)


                    End Select

CONTINUE_WHILE:
                    strLine = reader.ReadLine

                    bw.ReportProgress(i + 1)
                Loop

            End If


            If mdtTable.Rows.Count > 0 Then
                gblnIsImported = True
            Else
                eZeeMsgBox.Show("Sorry, You cannot import this file. Reason : There is no transaction to import in this file.", enMsgBoxStyle.Exclamation)
            End If

        Catch ex As Exception
            Throw New Exception("Import_Inbound : " & ex.Message)
        Finally
            objEmp = Nothing
            objId_Tran = Nothing
            objSalary = Nothing
            objDependant = Nothing
            objMaster = Nothing
            objTitle = Nothing
            objDept = Nothing
            objCCenter = Nothing
            objJob = Nothing
            objBranch = Nothing
            objJobGroup = Nothing
            objClassGroup = Nothing 'Sohail (16 Jun 2015)
            objCity = Nothing
            objState = Nothing
            objZipCode = Nothing
            objPeriod = Nothing
        End Try
    End Function
    'Sohail (24 Nov 2014) -- End

    ''Sohail (13 Aug 2014) -- Start
    ''Enhancement - New EPF Period Wise Report.
    'Public Function Export_Inbound(ByVal intCompanyUnkId As Integer, ByVal intTZSHeadID As Integer, ByVal intUSDHead As Integer, ByVal strExportPath As String, ByVal bw As BackgroundWorker) As Boolean
    '    Dim strQ As String
    '    Dim ds As New DataSet
    '    Dim strDatabaseName As String = ""
    '    Dim strDBName As String = ""
    '    Dim strBuilder As New StringBuilder
    '    Dim da1 As SqlDataAdapter
    '    Dim ds1 As New DataSet
    '    Dim da2 As SqlDataAdapter
    '    Dim ds2 As New DataSet
    '    Dim dsYear As New DataSet
    '    Dim dsSalary As New DataSet
    '    Dim dtEnd As Date = Nothing
    '    Dim strStartDate As String = "20140101"
    '    Dim strEndDate As String = "99000101"
    '    Try

    '        strDatabaseName = GetCurrentDBName(intCompanyUnkId)


    '        strQ = "SELECT  database_name AS dname " & _
    '                         ", cffinancial_year_tran.companyunkid AS companyunkid " & _
    '                         ", hrmsConfiguration..cffinancial_year_tran.yearunkid " & _
    '                         ", hrmsConfiguration..cffinancial_year_tran.isclosed " & _
    '                       "FROM    hrmsConfiguration..cffinancial_year_tran " & _
    '                               "JOIN sys.databases ON sys.databases.NAME = hrmsConfiguration..cffinancial_year_tran.database_name " & _
    '                               "JOIN hrmsConfiguration..cfcompany_master ON cfcompany_master.companyunkid = cffinancial_year_tran.companyunkid " & _
    '                                   "AND cfcompany_master.companyunkid = " & intCompanyUnkId & " " & _
    '                       "ORDER BY hrmsConfiguration..cffinancial_year_tran.yearunkid "


    '        sqlCmd.CommandText = strQ
    '        da2 = New SqlDataAdapter(sqlCmd)
    '        dsYear.Tables.Clear()
    '        da2.Fill(dsYear)


    '        strQ = "SELECT  hremployee_master.employeeunkid  " & _
    '                      ", identity_no AS Global_ID " & _
    '                      ", employeecode AS LOCAL_EMPLID " & _
    '                      ", birthdate AS BIRTHDATE " & _
    '                      ", ISNULL(cfcountry_master.alias, '') AS BIRTHCOUNTRY " & _
    '                      ", 'PRI' AS NAME_TYPE " & _
    '                      ", appointeddate AS EFFDT " & _
    '                      ", 'A' AS EFF_STATUS " & _
    '                      ", '001' AS COUNTRY_NM_FORMAT " & _
    '                      ", ISNULL(title.code, '') AS NAME_PREFIX " & _
    '                      ", surname AS LAST_NAME " & _
    '                      ", firstname AS FIRST_NAME " & _
    '                      ", ISNULL(othername, '') AS MIDDLE_NAME " & _
    '                      ", ISNULL(marstatus.code, 'U') AS MAR_STATUS " & _
    '                      ", anniversary_date AS MAR_STATUS_DT " & _
    '                      ", CASE gender WHEN 1 THEN 'M' WHEN 2 THEN 'F' ELSE 'U' END AS SEX " & _
    '                      ", 'A' AS HIGHEST_EDUC_LVL " & _
    '                      ", 'N' AS FT_STUDENT " & _
    '                      ", 'ENG' AS LANG_CD " & _
    '                      ", ISNULL(hrdepartment_master.description, '') AS DEPTID " & _
    '                      ", ISNULL(hrjob_master.job_code, '') AS JOBCODE " & _
    '                      ", hremployee_master.departmentunkid AS Z_LOCAL_DEPT_ID " & _
    '                      ", hremployee_master.jobunkid AS Z_LOCAL_JOBCODE_ID " & _
    '                      ", termination_from_date " & _
    '                      ", empl_enddate " & _
    '                      ", termination_to_date " & _
    '                      ", CASE WHEN termination_to_date <= GETDATE() THEN termination_to_date " & _
    '                             "WHEN ISNULL(termination_from_date, '88880101') <= GETDATE() " & _
    '                             "THEN termination_from_date " & _
    '                             "WHEN ISNULL(empl_enddate, '88880101') <= GETDATE() THEN empl_enddate " & _
    '                             "ELSE '' " & _
    '                        "END AS TERMINATIONDATE  " & _
    '                      ", CASE WHEN termination_to_date <= GETDATE() THEN 'I' " & _
    '                             "WHEN ISNULL(termination_from_date, '88880101') <= GETDATE() " & _
    '                             "THEN 'I' " & _
    '                             "WHEN ISNULL(empl_enddate, '88880101') <= GETDATE() THEN 'I' " & _
    '                             "ELSE 'A' " & _
    '                        "END AS HR_STATUS  " & _
    '                      ", CASE WHEN termination_to_date <= GETDATE() THEN 'I' " & _
    '                             "WHEN ISNULL(termination_from_date, '88880101') <= GETDATE() " & _
    '                             "THEN 'I' " & _
    '                             "WHEN ISNULL(empl_enddate, '88880101') <= GETDATE() THEN 'I' " & _
    '                             "ELSE 'A' " & _
    '                        "END AS EMPL_STATUS  " & _
    '                      ", 'HIR' AS ACTION " & _
    '                      ", '' AS ACTION_REASON " & _
    '                      ", 'DARHR' AS LOCATION " & _
    '                      ", empl_enddate AS EXPECTED_END_DATE " & _
    '                      ", 'N' AS SHIFT " & _
    '                      ", 'R' AS REG_TEMP " & _
    '                      ", 'F' AS FULL_PART_TIME " & _
    '                      ", ISNULL(emptype.code, '') AS EMPL_CLASS " & _
    '                      ", 'HHC' AS COMPANY " & _
    '                      ", ISNULL(hrjobgroup_master.code, '1') AS OFFICER_CD " & _
    '                      ", 'TZA' AS REG_REGION " & _
    '                      ", termination_from_date AS LAST_DATE_WORKED " & _
    '                      ", 0 AS COMP_EFFSEQ " & _
    '                      ", 'BPAYG' AS COMP_RATECD " & _
    '                      ", hremployee_master.scale AS COMPRATE " & _
    '                      ", 'M' AS COMP_FREQUENCY " & _
    '                      ", CASE hremployee_master.tranhedunkid WHEN " & intUSDHead & " THEN 'USD' ELSE 'TZS' END AS CURRENCY_CD " & _
    '                "FROM    " & strDatabaseName & "..hremployee_master " & _
    '                        "LEFT JOIN " & strDatabaseName & "..hremployee_idinfo_tran ON hremployee_idinfo_tran.employeeunkid = hremployee_master.employeeunkid " & _
    '                        "LEFT JOIN " & strDatabaseName & "..hrdepartment_master ON hrdepartment_master.departmentunkid = hremployee_master.departmentunkid " & _
    '                        "LEFT JOIN " & strDatabaseName & "..hrjob_master ON hrjob_master.jobunkid = hremployee_master.jobunkid " & _
    '                        "LEFT JOIN " & strDatabaseName & "..hrjobgroup_master ON hrjobgroup_master.jobgroupunkid = hremployee_master.jobgroupunkid " & _
    '                        "LEFT JOIN " & strDatabaseName & "..cfcommon_master AS title ON title.masterunkid = hremployee_master.titleunkid AND title.mastertype = 24 " & _
    '                        "LEFT JOIN " & strDatabaseName & "..cfcommon_master AS emptype ON emptype.masterunkid = hremployee_master.employmenttypeunkid AND emptype.mastertype = 8 " & _
    '                        "LEFT JOIN " & strDatabaseName & "..cfcommon_master AS marstatus ON marstatus.masterunkid = hremployee_master.maritalstatusunkid AND marstatus.mastertype = 15 " & _
    '                        "LEFT JOIN hrmsConfiguration..cfcountry_master ON cfcountry_master.countryunkid = hremployee_master.birthcountryunkid " & _
    '                "WHERE   isdefault = 1 " & _
    '                        " AND CONVERT(CHAR(8),appointeddate,112) <= '" & strEndDate & "' " & _
    '                        " AND ISNULL(CONVERT(CHAR(8),termination_from_date,112), '" & strStartDate & "') >= '" & strStartDate & "' " & _
    '                        " AND ISNULL(CONVERT(CHAR(8),termination_to_date,112), '" & strStartDate & "') >= '" & strStartDate & "' " & _
    '                        " AND ISNULL(CONVERT(CHAR(8), empl_enddate,112), '" & strStartDate & "') >= '" & strStartDate & "' " & _
    '                "ORDER BY hremployee_idinfo_tran.identity_no "

    '        sqlCmd.CommandText = strQ
    '        Dim da As New SqlDataAdapter(sqlCmd)

    '        da.Fill(ds)

    '        If ds.Tables(0).Rows.Count > 0 Then
    '            gintTotalEmployees = ds.Tables(0).Rows.Count

    '            Dim dsRow As DataRow

    '            For i As Integer = 0 To ds.Tables(0).Rows.Count - 1
    '                dsRow = ds.Tables(0).Rows(i)

    '                dtEnd = CDate(dsRow.Item("termination_to_date"))
    '                If IsDBNull(dsRow.Item("termination_from_date")) = False Then
    '                    dtEnd = IIf(CDate(dsRow.Item("termination_from_date")) < dtEnd, CDate(dsRow.Item("termination_from_date")), dtEnd)
    '                End If
    '                If IsDBNull(dsRow.Item("empl_enddate")) = False Then
    '                    dtEnd = IIf(CDate(dsRow.Item("empl_enddate")) < dtEnd, CDate(dsRow.Item("empl_enddate")), dtEnd)
    '                End If

    '                '***    PARENT:PERSON   ***
    '                strBuilder.Append("PARENT:PERSON|")
    '                strBuilder.Append(dsRow.Item("Global_ID").ToString & "|")
    '                strBuilder.Append(dsRow.Item("LOCAL_EMPLID").ToString & "|")
    '                strBuilder.Append("|") 'BIRTH PLACE
    '                If IsDBNull(dsRow.Item("BIRTHDATE")) = True Then
    '                    strBuilder.Append("|") 'BIRTH DATE
    '                Else
    '                    strBuilder.Append(CDate(dsRow.Item("BIRTHDATE")).ToString("MM/dd/yyyy") & "|")
    '                End If
    '                strBuilder.Append("|") 'LASTUPDDTTM
    '                strBuilder.Append(dsRow.Item("BIRTHCOUNTRY").ToString & "|")

    '                strBuilder.Append(vbCrLf)


    '                '***    CHILD:NAMES   ***
    '                strBuilder.Append("CHILD:NAMES|")
    '                strBuilder.Append(dsRow.Item("Global_ID").ToString & "|")
    '                strBuilder.Append(dsRow.Item("LOCAL_EMPLID").ToString & "|")
    '                strBuilder.Append(dsRow.Item("NAME_TYPE").ToString & "|")
    '                strBuilder.Append(CDate(dsRow.Item("EFFDT")).ToString("MM/dd/yyyy") & "|")
    '                strBuilder.Append(dsRow.Item("EFF_STATUS").ToString & "|")
    '                strBuilder.Append(dsRow.Item("COUNTRY_NM_FORMAT").ToString & "|")
    '                strBuilder.Append(dsRow.Item("NAME_PREFIX").ToString & "|")
    '                strBuilder.Append(dsRow.Item("LAST_NAME").ToString & "|")
    '                strBuilder.Append(dsRow.Item("FIRST_NAME").ToString & "|")
    '                strBuilder.Append(dsRow.Item("MIDDLE_NAME").ToString & "|")
    '                strBuilder.Append("|") 'LASTUPDDTTM

    '                strBuilder.Append(vbCrLf)


    '                '***    CHILD:PERS_DATA_EFFDT   ***
    '                strBuilder.Append("CHILD:PERS_DATA_EFFDT|")
    '                strBuilder.Append(dsRow.Item("Global_ID").ToString & "|")
    '                strBuilder.Append(dsRow.Item("LOCAL_EMPLID").ToString & "|")
    '                strBuilder.Append(CDate(dsRow.Item("EFFDT")).ToString("MM/dd/yyyy") & "|")
    '                strBuilder.Append(dsRow.Item("MAR_STATUS").ToString & "|")
    '                If IsDBNull(dsRow.Item("MAR_STATUS_DT")) = True Then
    '                    strBuilder.Append("|") 'MAR_STATUS_DT
    '                Else
    '                    strBuilder.Append(CDate(dsRow.Item("MAR_STATUS_DT")).ToString("MM/dd/yyyy") & "|")
    '                End If
    '                strBuilder.Append(dsRow.Item("SEX").ToString & "|")
    '                strBuilder.Append(dsRow.Item("HIGHEST_EDUC_LVL").ToString & "|")
    '                strBuilder.Append(dsRow.Item("FT_STUDENT").ToString & "|")
    '                strBuilder.Append(dsRow.Item("LANG_CD").ToString & "|")
    '                strBuilder.Append("|") 'LASTUPDDTTM

    '                strBuilder.Append(vbCrLf)


    '                '***    PARENT:JOB   ***
    '                strBuilder.Append("PARENT:JOB|")
    '                strBuilder.Append(dsRow.Item("Global_ID").ToString & "|")
    '                strBuilder.Append(dsRow.Item("LOCAL_EMPLID").ToString & "|")
    '                strBuilder.Append(CDate(dsRow.Item("EFFDT")).ToString("MM/dd/yyyy") & "|")
    '                strBuilder.Append("0|") 'EFFSEQ
    '                strBuilder.Append(dsRow.Item("DEPTID").ToString & "|")
    '                strBuilder.Append(dsRow.Item("JOBCODE").ToString.Replace(".", "") & "|")
    '                strBuilder.Append(dsRow.Item("Z_LOCAL_DEPT_ID").ToString & "|")
    '                strBuilder.Append(dsRow.Item("Z_LOCAL_JOBCODE_ID").ToString & "|")
    '                strBuilder.Append("A|") 'HR_STATUS
    '                strBuilder.Append("A|") 'EMPL_STATUS
    '                strBuilder.Append(dsRow.Item("ACTION").ToString & "|")
    '                strBuilder.Append(dsRow.Item("ACTION_REASON").ToString & "|")
    '                strBuilder.Append(dsRow.Item("LOCATION").ToString & "|")
    '                If IsDBNull(dsRow.Item("EXPECTED_END_DATE")) = True Then
    '                    strBuilder.Append("|") 'EXPECTED_END_DATE
    '                Else
    '                    strBuilder.Append(CDate(dsRow.Item("EXPECTED_END_DATE")).ToString("MM/dd/yyyy") & "|")
    '                End If
    '                strBuilder.Append(dsRow.Item("SHIFT").ToString & "|")
    '                strBuilder.Append(dsRow.Item("REG_TEMP").ToString & "|")
    '                strBuilder.Append(dsRow.Item("FULL_PART_TIME").ToString & "|")
    '                strBuilder.Append(dsRow.Item("EMPL_CLASS").ToString & "|")
    '                strBuilder.Append(dsRow.Item("COMPANY").ToString & "|")
    '                strBuilder.Append(dsRow.Item("OFFICER_CD").ToString & "|")
    '                strBuilder.Append(dsRow.Item("REG_REGION").ToString & "|")
    '                If IsDBNull(dsRow.Item("LAST_DATE_WORKED")) = True Then
    '                    strBuilder.Append("|") 'LAST_DATE_WORKED
    '                Else
    '                    strBuilder.Append(CDate(dsRow.Item("LAST_DATE_WORKED")).ToString("MM/dd/yyyy") & "|")
    '                End If
    '                strBuilder.Append("|") 'LASTUPDDTTM

    '                strBuilder.Append(vbCrLf)


    '                '***    PARENT:JOB (2nd record)  ***
    '                'strQ = "SELECT  database_name AS dname " & _
    '                '          ", cffinancial_year_tran.companyunkid AS companyunkid " & _
    '                '          ", hrmsConfiguration..cffinancial_year_tran.yearunkid " & _
    '                '          ", hrmsConfiguration..cffinancial_year_tran.isclosed " & _
    '                '        "FROM    hrmsConfiguration..cffinancial_year_tran " & _
    '                '                "JOIN sys.databases ON sys.databases.NAME = hrmsConfiguration..cffinancial_year_tran.database_name " & _
    '                '                "JOIN hrmsConfiguration..cfcompany_master ON cfcompany_master.companyunkid = cffinancial_year_tran.companyunkid " & _
    '                '                    "AND cfcompany_master.companyunkid = " & intCompanyUnkId & " " & _
    '                '        "ORDER BY hrmsConfiguration..cffinancial_year_tran.yearunkid "


    '                'sqlCmd.CommandText = strQ
    '                'da2 = New SqlDataAdapter(sqlCmd)
    '                'ds2.Tables.Clear()
    '                'da2.Fill(ds2)
    '                Dim blnRecordAdded As Boolean = False
    '                strQ = ""

    '                'For Each dRow As DataRow In ds2.Tables(0).Rows
    '                For Each dRow As DataRow In dsYear.Tables(0).Rows
    '                    strDBName = dRow.Item("dname").ToString

    '                    strQ &= "UNION ALL SELECT  incrementdate AS EFFDT  " & _
    '                                  ", 0 AS COMP_EFFSEQ " & _
    '                                  ", 'BPAYG' AS COMP_RATECD " & _
    '                                  ", prsalaryincrement_tran.newscale AS COMPRATE " & _
    '                                  ", 'M' AS COMP_FREQUENCY " & _
    '                                  ", 'TZS' AS CURRENCY_CD " & _
    '                                  ", isfromemployee " & _
    '                                  ", salaryincrementtranunkid " & _
    '                            "FROM    " & strDBName & "..prsalaryincrement_tran " & _
    '                            "WHERE   ISNULL(isvoid, 0) = 0 " & _
    '                                    "AND isapproved = 1 " & _
    '                                    "AND employeeunkid = " & CInt(dsRow.Item("employeeunkid")) & " "

    '                    If blnRecordAdded = True Then
    '                        'strQ &= " AND isfromemployee = 0 "
    '                    End If
    '                Next

    '                strQ &= "ORDER BY incrementdate, salaryincrementtranunkid  "
    '                strQ = strQ.Substring(9)

    '                sqlCmd.CommandText = strQ
    '                da1 = New SqlDataAdapter(sqlCmd)
    '                dsSalary.Tables.Clear()
    '                da1.Fill(dsSalary)

    '                For Each dtRow As DataRow In dsSalary.Tables(0).Rows

    '                    If CDate(dsRow.Item("EFFDT")) >= CDate(dtRow.Item("EFFDT")) Then Continue For 'Skip Appointment date Increment, [> Skip increment before latest appointment date] 
    '                    If blnRecordAdded = True AndAlso CBool(dtRow.Item("isfromemployee")) = True Then Continue For 'Skip CF  Increment

    '                    strBuilder.Append("PARENT:JOB|")
    '                    strBuilder.Append(dsRow.Item("Global_ID").ToString & "|")
    '                    strBuilder.Append(dsRow.Item("LOCAL_EMPLID").ToString & "|")
    '                    strBuilder.Append(CDate(dtRow.Item("EFFDT")).ToString("MM/dd/yyyy") & "|") 'EFFDT
    '                    strBuilder.Append("0|") 'EFFSEQ
    '                    strBuilder.Append(dsRow.Item("DEPTID").ToString & "|")
    '                    strBuilder.Append(dsRow.Item("JOBCODE").ToString.Replace(".", "") & "|")
    '                    strBuilder.Append(dsRow.Item("Z_LOCAL_DEPT_ID").ToString & "|")
    '                    strBuilder.Append(dsRow.Item("Z_LOCAL_JOBCODE_ID").ToString & "|")
    '                    strBuilder.Append("A|") 'HR_STATUS
    '                    strBuilder.Append("A|") 'EMPL_STATUS
    '                    strBuilder.Append("DTA|") 'ACTION
    '                    strBuilder.Append(dsRow.Item("ACTION_REASON").ToString & "|")
    '                    strBuilder.Append(dsRow.Item("LOCATION").ToString & "|")
    '                    If IsDBNull(dsRow.Item("EXPECTED_END_DATE")) = True Then
    '                        strBuilder.Append("|") 'EXPECTED_END_DATE
    '                    Else
    '                        strBuilder.Append(CDate(dsRow.Item("EXPECTED_END_DATE")).ToString("MM/dd/yyyy") & "|")
    '                    End If
    '                    strBuilder.Append(dsRow.Item("SHIFT").ToString & "|")
    '                    strBuilder.Append(dsRow.Item("REG_TEMP").ToString & "|")
    '                    strBuilder.Append(dsRow.Item("FULL_PART_TIME").ToString & "|")
    '                    strBuilder.Append(dsRow.Item("EMPL_CLASS").ToString & "|")
    '                    strBuilder.Append(dsRow.Item("COMPANY").ToString & "|")
    '                    strBuilder.Append(dsRow.Item("OFFICER_CD").ToString & "|")
    '                    strBuilder.Append(dsRow.Item("REG_REGION").ToString & "|")
    '                    If IsDBNull(dsRow.Item("LAST_DATE_WORKED")) = True Then
    '                        strBuilder.Append("|") 'LAST_DATE_WORKED
    '                    Else
    '                        strBuilder.Append(CDate(dsRow.Item("LAST_DATE_WORKED")).ToString("MM/dd/yyyy") & "|")
    '                    End If
    '                    strBuilder.Append("|") 'LASTUPDDTTM

    '                    strBuilder.Append(vbCrLf)

    '                    blnRecordAdded = True
    '                Next
    '                'Next

    '                If blnRecordAdded = False OrElse dtEnd <= DateAndTime.Today.Date Then
    '                    strBuilder.Append("PARENT:JOB|")
    '                    strBuilder.Append(dsRow.Item("Global_ID").ToString & "|")
    '                    strBuilder.Append(dsRow.Item("LOCAL_EMPLID").ToString & "|")
    '                    'If dsRow.Item("HR_STATUS").ToString = "I" Then
    '                    '    strBuilder.Append(CDate(dsRow.Item("TERMINATIONDATE")).ToString("MM/dd/yyyy") & "|") 'EFFDT
    '                    'Else
    '                    '    strBuilder.Append(DateAndTime.Today.Date.ToString("MM/dd/yyyy") & "|") 'EFFDT
    '                    'End If
    '                    If dtEnd <= DateAndTime.Today.Date Then
    '                        strBuilder.Append(dtEnd.ToString("MM/dd/yyyy") & "|") 'EFFDT
    '                    Else
    '                        strBuilder.Append(DateAndTime.Today.Date.ToString("MM/dd/yyyy") & "|") 'EFFDT
    '                    End If
    '                    strBuilder.Append("0|") 'EFFSEQ
    '                    strBuilder.Append(dsRow.Item("DEPTID").ToString & "|")
    '                    strBuilder.Append(dsRow.Item("JOBCODE").ToString.Replace(".", "") & "|")
    '                    strBuilder.Append(dsRow.Item("Z_LOCAL_DEPT_ID").ToString & "|")
    '                    strBuilder.Append(dsRow.Item("Z_LOCAL_JOBCODE_ID").ToString & "|")
    '                    'If dsRow.Item("HR_STATUS").ToString = "I" Then
    '                    '    strBuilder.Append("I|") 'HR_STATUS
    '                    '    strBuilder.Append("T|") 'EMPL_STATUS
    '                    '    strBuilder.Append("TER|") 'ACTION
    '                    'Else
    '                    '    strBuilder.Append("A|") 'HR_STATUS
    '                    '    strBuilder.Append("A|") 'EMPL_STATUS
    '                    '    strBuilder.Append("DTA|") 'ACTION
    '                    'End If
    '                    If dtEnd <= DateAndTime.Today.Date Then
    '                        strBuilder.Append("I|") 'HR_STATUS
    '                        strBuilder.Append("T|") 'EMPL_STATUS
    '                        strBuilder.Append("TER|") 'ACTION
    '                    Else
    '                        strBuilder.Append("A|") 'HR_STATUS
    '                        strBuilder.Append("A|") 'EMPL_STATUS
    '                        strBuilder.Append("DTA|") 'ACTION
    '                    End If
    '                    strBuilder.Append(dsRow.Item("ACTION_REASON").ToString & "|")
    '                    strBuilder.Append(dsRow.Item("LOCATION").ToString & "|")
    '                    If IsDBNull(dsRow.Item("EXPECTED_END_DATE")) = True Then
    '                        strBuilder.Append("|") 'EXPECTED_END_DATE
    '                    Else
    '                        'strBuilder.Append(CDate(dsRow.Item("EXPECTED_END_DATE")).ToString("MM/dd/yyyy") & "|")
    '                        strBuilder.Append("|") 'EXPECTED_END_DATE
    '                    End If
    '                    strBuilder.Append(dsRow.Item("SHIFT").ToString & "|")
    '                    strBuilder.Append(dsRow.Item("REG_TEMP").ToString & "|")
    '                    strBuilder.Append(dsRow.Item("FULL_PART_TIME").ToString & "|")
    '                    strBuilder.Append(dsRow.Item("EMPL_CLASS").ToString & "|")
    '                    strBuilder.Append(dsRow.Item("COMPANY").ToString & "|")
    '                    strBuilder.Append(dsRow.Item("OFFICER_CD").ToString & "|")
    '                    strBuilder.Append(dsRow.Item("REG_REGION").ToString & "|")
    '                    If IsDBNull(dsRow.Item("LAST_DATE_WORKED")) = True Then
    '                        strBuilder.Append("|") 'LAST_DATE_WORKED
    '                    Else
    '                        strBuilder.Append(CDate(dsRow.Item("LAST_DATE_WORKED")).ToString("MM/dd/yyyy") & "|")
    '                    End If
    '                    strBuilder.Append("|") 'LASTUPDDTTM

    '                    strBuilder.Append(vbCrLf)
    '                End If



    '                'strQ = "SELECT  database_name AS dname " & _
    '                '          ", cffinancial_year_tran.companyunkid AS companyunkid " & _
    '                '          ", hrmsConfiguration..cffinancial_year_tran.yearunkid " & _
    '                '          ", hrmsConfiguration..cffinancial_year_tran.isclosed " & _
    '                '        "FROM    hrmsConfiguration..cffinancial_year_tran " & _
    '                '                "JOIN sys.databases ON sys.databases.NAME = hrmsConfiguration..cffinancial_year_tran.database_name " & _
    '                '                "JOIN hrmsConfiguration..cfcompany_master ON cfcompany_master.companyunkid = cffinancial_year_tran.companyunkid " & _
    '                '                    "AND cfcompany_master.companyunkid = " & intCompanyUnkId & " " & _
    '                '        "ORDER BY hrmsConfiguration..cffinancial_year_tran.yearunkid "


    '                'sqlCmd.CommandText = strQ
    '                'da2 = New SqlDataAdapter(sqlCmd)
    '                'ds2.Tables.Clear()
    '                'da2.Fill(ds2)
    '                blnRecordAdded = False

    '                'For Each dRow As DataRow In ds2.Tables(0).Rows
    '                'For Each dRow As DataRow In dsYear.Tables(0).Rows
    '                '    strDBName = dRow.Item("dname").ToString

    '                '    strQ = "SELECT  incrementdate AS EFFDT  " & _
    '                '                  ", 0 AS COMP_EFFSEQ " & _
    '                '                  ", 'BPAYG' AS COMP_RATECD " & _
    '                '                  ", prsalaryincrement_tran.newscale AS COMPRATE " & _
    '                '                  ", 'M' AS COMP_FREQUENCY " & _
    '                '                  ", 'TZS' AS CURRENCY_CD " & _
    '                '            "FROM    " & strDBName & "..prsalaryincrement_tran " & _
    '                '            "WHERE   ISNULL(isvoid, 0) = 0 " & _
    '                '                    "AND isapproved = 1 " & _
    '                '                    "AND employeeunkid = " & CInt(dsRow.Item("employeeunkid")) & " "

    '                '    If blnRecordAdded = True Then
    '                '        strQ &= " AND isfromemployee = 0 "
    '                '    End If

    '                '    strQ &= "ORDER BY incrementdate "

    '                '    sqlCmd.CommandText = strQ
    '                '    da1 = New SqlDataAdapter(sqlCmd)
    '                '    ds1.Tables.Clear()
    '                '    da1.Fill(ds1)


    '                '***    CHILD:COMPENSATION   ***
    '                'If ds1.Tables(0).Rows.Count > 0 Then
    '                For Each dtRow As DataRow In dsSalary.Tables(0).Rows
    '                    If CDate(dsRow.Item("EFFDT")) > CDate(dtRow.Item("EFFDT")) Then Continue For '[> Skip increment before latest appointment date] 
    '                    If blnRecordAdded = True AndAlso CBool(dtRow.Item("isfromemployee")) = True Then Continue For 'Skip CF  Increment

    '                    strBuilder.Append("CHILD:COMPENSATION|")
    '                    strBuilder.Append(dsRow.Item("Global_ID").ToString & "|")
    '                    strBuilder.Append(dsRow.Item("LOCAL_EMPLID").ToString & "|")
    '                    strBuilder.Append(CDate(dtRow.Item("EFFDT")).ToString("MM/dd/yyyy") & "|")
    '                    strBuilder.Append("0|")
    '                    strBuilder.Append(dtRow.Item("COMP_EFFSEQ").ToString & "|")
    '                    strBuilder.Append(dtRow.Item("COMP_RATECD").ToString & "|")
    '                    strBuilder.Append(Format(CDec(dtRow.Item("COMPRATE")), "##########0.00") & "|")
    '                    'strBuilder.Append("0|")
    '                    strBuilder.Append(dtRow.Item("COMP_FREQUENCY").ToString & "|")
    '                    strBuilder.Append(dtRow.Item("CURRENCY_CD").ToString & "|")
    '                    strBuilder.Append("|") 'LASTUPDDTTM

    '                    strBuilder.Append(vbCrLf)
    '                    blnRecordAdded = True
    '                Next
    '                'Else
    '                '    strBuilder.Append("CHILD:COMPENSATION|")
    '                '    strBuilder.Append(dsRow.Item("Global_ID").ToString & "|")
    '                '    strBuilder.Append(dsRow.Item("LOCAL_EMPLID").ToString & "|")
    '                '    strBuilder.Append(CDate(dsRow.Item("EFFDT")).ToString("MM/dd/yyyy") & "|")
    '                '    strBuilder.Append("0|")
    '                '    strBuilder.Append(dsRow.Item("COMP_EFFSEQ").ToString & "|")
    '                '    strBuilder.Append(dsRow.Item("COMP_RATECD").ToString & "|")
    '                '    strBuilder.Append(Format(CDec(dsRow.Item("COMPRATE")), "##########0.00") & "|")
    '                '    'strBuilder.Append("0|")
    '                '    strBuilder.Append(dsRow.Item("COMP_FREQUENCY").ToString & "|")
    '                '    strBuilder.Append(dsRow.Item("CURRENCY_CD").ToString & "|")
    '                '    strBuilder.Append("|") 'LASTUPDDTTM

    '                '    strBuilder.Append(vbCrLf)
    '                'End If
    '                'Next

    '                If blnRecordAdded = False Then
    '                    '***    CHILD:COMPENSATION   ***
    '                    strBuilder.Append("CHILD:COMPENSATION|")
    '                    strBuilder.Append(dsRow.Item("Global_ID").ToString & "|")
    '                    strBuilder.Append(dsRow.Item("LOCAL_EMPLID").ToString & "|")
    '                    strBuilder.Append(CDate(dsRow.Item("EFFDT")).ToString("MM/dd/yyyy") & "|")
    '                    strBuilder.Append("0|")
    '                    strBuilder.Append(dsRow.Item("COMP_EFFSEQ").ToString & "|")
    '                    strBuilder.Append(dsRow.Item("COMP_RATECD").ToString & "|")
    '                    strBuilder.Append(Format(CDec(dsRow.Item("COMPRATE")), "##########0.00") & "|")
    '                    'strBuilder.Append("0|")
    '                    strBuilder.Append(dsRow.Item("COMP_FREQUENCY").ToString & "|")
    '                    strBuilder.Append(dsRow.Item("CURRENCY_CD").ToString & "|")
    '                    strBuilder.Append("|") 'LASTUPDDTTM

    '                    strBuilder.Append(vbCrLf)
    '                End If

    '                bw.ReportProgress(i + 1)

    '            Next
    '        End If

    '        If SaveTextFile(strExportPath, strBuilder) = False Then
    '            Return False
    '        End If
    '        Return True
    '    Catch ex As Exception
    '        Throw New Exception(ex.Message)
    '    End Try
    'End Function

    'Private Function SaveTextFile(ByVal fpath As String, ByVal sb As System.Text.StringBuilder) As Boolean
    '    Dim fsFile As New FileStream(fpath, FileMode.Create, FileAccess.Write)
    '    Dim strWriter As New StreamWriter(fsFile)
    '    Try

    '        With strWriter
    '            .BaseStream.Seek(0, SeekOrigin.End)
    '            .WriteLine(sb)
    '            .Close()
    '        End With
    '        Return True
    '    Catch ex As Exception
    '        Throw New Exception(ex.Message)
    '        Return False
    '    Finally
    '        sb = Nothing
    '        strWriter = Nothing
    '        fsFile = Nothing
    '    End Try
    'End Function
    ''Sohail (13 Aug 2014) -- End

    ''Sohail (07 Aug 2012) -- Start
    ''TRA - ENHANCEMENT

    'Public Function UpdateLoan_BfAmount(ByVal intCompanyUnkId As Integer) As Boolean
    '    Dim strQ As String
    '    Dim ds As New DataSet
    '    Dim strPrevDatabaseName As String = ""
    '    Dim strNextDatabaseName As String = ""
    '    Try
    '        strQ = "SELECT  database_name " & _
    '           "FROM    hrmsConfiguration..cffinancial_year_tran " & _
    '           "WHERE   companyunkid = " & intCompanyUnkId & " " & _
    '           "ORDER BY start_date "

    '        sqlCmd.CommandText = strQ
    '        Dim da As New SqlDataAdapter(sqlCmd)

    '        da.Fill(ds)

    '        If ds.Tables(0).Rows.Count > 1 Then ' Minimum 2 database, 1 open and other closed
    '            Dim dsRow As DataRow
    '            For i As Integer = 0 To ds.Tables(0).Rows.Count - 1
    '                dsRow = ds.Tables(0).Rows(i)

    '                If i = 0 Then 'First Closed Company
    '                    strPrevDatabaseName = dsRow.Item("database_name").ToString
    '                Else
    '                    strNextDatabaseName = dsRow.Item("database_name").ToString

    '                    strQ = "UPDATE  " & strNextDatabaseName & "..lnloan_advance_tran " & _
    '                            "SET     bf_amount = prevloan.balance_amount " & _
    '                            "FROM    " & strPrevDatabaseName & "..lnloan_advance_tran AS prevloan " & _
    '                            "WHERE   prevloan.loanadvancetranunkid = lnloan_advance_tran.loanadvancetranunkid " & _
    '                                    "AND lnloan_advance_tran.isbrought_forward = 1 " & _
    '                                    "AND lnloan_advance_tran.bf_amount IS NULL "


    '                    sqlCmd.CommandText = strQ
    '                    sqlCmd.Transaction = Trn
    '                    sqlCmd.ExecuteNonQuery()


    '                    strQ = "UPDATE  " & strNextDatabaseName & "..atlnloan_advance_tran " & _
    '                           "SET     bf_amount = prevloan.balance_amount " & _
    '                           "FROM    " & strPrevDatabaseName & "..atlnloan_advance_tran AS prevloan " & _
    '                           "WHERE   prevloan.loanadvancetranunkid = atlnloan_advance_tran.loanadvancetranunkid " & _
    '                                   "AND atlnloan_advance_tran.isbrought_forward = 1 " & _
    '                                   "AND atlnloan_advance_tran.bf_amount IS NULL "


    '                    sqlCmd.CommandText = strQ
    '                    sqlCmd.Transaction = Trn
    '                    sqlCmd.ExecuteNonQuery()


    '                    strPrevDatabaseName = dsRow.Item("database_name").ToString
    '                End If
    '            Next
    '        End If

    '        Return True
    '    Catch ex As Exception
    '        Throw New Exception(ex.Message)
    '    End Try



    'End Function
    ''Sohail (07 Aug 2012) -- End

    ''Anjan [ 30 Sep 2013 ] -- Start
    ''ENHANCEMENT : Currency Changes requested by Rutta
    'Public Function UpdateCurrencyTZS(ByVal intCompanyUnkId As Integer) As Boolean
    '    Dim strQ As String
    '    Dim ds As New DataSet
    '    Dim strPrevDatabaseName As String = ""
    '    Dim strNextDatabaseName As String = ""
    '    Try
    '        strQ = "SELECT  database_name " & _
    '           "FROM    hrmsConfiguration..cffinancial_year_tran " & _
    '           "WHERE   companyunkid = " & intCompanyUnkId & " " & _
    '           "ORDER BY start_date "


    '        sqlCmd.CommandText = strQ
    '        Dim da As New SqlDataAdapter(sqlCmd)

    '        da.Fill(ds)

    '        strQ = ""

    '        strQ = "UPDATE hrmsConfiguration..cfconfiguration " & _
    '                       "SET    key_value = 'TZS' " & _
    '                       "WHERE  key_value = 'TSh' " & _
    '                              "AND key_name = 'CurrencySign' " & _
    '                       "UPDATE hrmsConfiguration..cfcountry_master " & _
    '                       "SET    currency = 'TZS' " & _
    '                       "WHERE  currency = 'TSh' " & _
    '                              "AND countryunkid = 208 "

    '        sqlCmd.CommandText = strQ
    '        sqlCmd.Transaction = Trn
    '        sqlCmd.ExecuteNonQuery()

    '        If ds.Tables(0).Rows.Count > 0 Then
    '            Dim dsRow As DataRow
    '            For i As Integer = 0 To ds.Tables(0).Rows.Count - 1
    '                dsRow = ds.Tables(0).Rows(i)

    '                strNextDatabaseName = dsRow.Item("database_name").ToString

    '                strQ = "UPDATE  " & strNextDatabaseName & "..cfexchange_rate " & _
    '                         "SET    currency_name = 'TZS' , " & _
    '                                "currency_sign = 'TZS' " & _
    '                         "WHERE  countryunkid = 208 " & _
    '                                "AND  (currency_name = 'TSh' " & _
    '                                "OR  currency_sign = 'TSh') "


    '                sqlCmd.CommandText = strQ
    '                sqlCmd.Transaction = Trn
    '                sqlCmd.ExecuteNonQuery()

    '            Next
    '        End If

    '        Return True
    '    Catch ex As Exception
    '        Throw New Exception(ex.Message)
    '    End Try



    'End Function
    ''Anjan [ 30 Sep 2013 ] -- End

    ''Sohail (09 Nov 2013) -- Start
    ''TRA - ENHANCEMENT
    'Public Function CF_Cumulative_Heads_Transactions(ByVal intCompanyUnkId As Integer) As Boolean
    '    Dim strQ As String
    '    Dim ds As New DataSet
    '    Dim strCurrentDatabaseName As String = ""
    '    Dim strPrevDatabaseName As String = ""
    '    Dim strNextDatabaseName As String = ""
    '    Dim strCumulativeHeadIDs As String = ""

    '    Try

    '        strCurrentDatabaseName = GetCurrentDBName(intCompanyUnkId)

    '        strQ = "DECLARE @listStr VARCHAR(MAX) " & _
    '                "SELECT  @listStr = COALESCE(@listStr + ',', '') + CAST(tranheadunkid AS VARCHAR(MAX)) " & _
    '                "FROM    " & strCurrentDatabaseName & "..prtranhead_formula_cumulative_tran " & _
    '                "WHERE   ISNULL(isvoid, 0) = 0 " & _
    '                "SELECT  ISNULL(@listStr, '') AS IDS "

    '        sqlCmd.CommandText = strQ
    '        Dim rd As SqlDataReader = sqlCmd.ExecuteReader

    '        If rd.HasRows = True Then
    '            While rd.Read
    '                strCumulativeHeadIDs = rd.Item("IDS").ToString()
    '            End While
    '        End If
    '        rd.Close()

    '        If strCumulativeHeadIDs.Trim <> "" Then

    '            strQ = "SELECT  database_name " & _
    '                  "FROM    hrmsConfiguration..cffinancial_year_tran " & _
    '                  "WHERE   companyunkid = " & intCompanyUnkId & " " & _
    '                  "ORDER BY start_date "

    '            sqlCmd.CommandText = strQ
    '            Dim da As New SqlDataAdapter(sqlCmd)

    '            da.Fill(ds)

    '            If ds.Tables(0).Rows.Count > 1 Then ' Minimum 2 database, 1 open and other closed
    '                Dim dsRow As DataRow
    '                For i As Integer = 0 To ds.Tables(0).Rows.Count - 1
    '                    dsRow = ds.Tables(0).Rows(i)

    '                    If i = 0 Then 'First Closed Company
    '                        strPrevDatabaseName = dsRow.Item("database_name").ToString
    '                    Else
    '                        strNextDatabaseName = dsRow.Item("database_name").ToString

    '                        strQ = "SELECT  a.payrollprocesstranunkid " & _
    '                                      ", a.tnaleavetranunkid " & _
    '                                      ", a.payperiodunkid " & _
    '                                      ", a.employeeunkid " & _
    '                                      ", a.tranheadunkid " & _
    '                                "FROM    ( SELECT    prevPayroll.payrollprocesstranunkid " & _
    '                                                  ", prevPayroll.tnaleavetranunkid " & _
    '                                                  ", prevTnA.payperiodunkid " & _
    '                                                  ", prevPayroll.employeeunkid " & _
    '                                                  ", prevPayroll.tranheadunkid " & _
    '                                          "FROM      " & strPrevDatabaseName & "..prpayrollprocess_tran AS prevPayroll " & _
    '                                                    "LEFT JOIN " & strPrevDatabaseName & "..prtnaleave_tran AS prevTnA ON prevTnA.tnaleavetranunkid = prevPayroll.tnaleavetranunkid " & _
    '                                          "WHERE     prevPayroll.isvoid = 0 " & _
    '                                                    "AND prevTnA.isvoid = 0 " & _
    '                                                    "AND tranheadunkid IN ( " & strCumulativeHeadIDs & " ) " & _
    '                                        ") AS a " & _
    '                                        "LEFT JOIN " & _
    '                                        "( SELECT    prevPayroll.payrollprocesstranunkid " & _
    '                                                  ", prevPayroll.tnaleavetranunkid " & _
    '                                                  ", prevTnA.payperiodunkid " & _
    '                                                  ", prevPayroll.employeeunkid " & _
    '                                                  ", prevPayroll.tranheadunkid " & _
    '                                          "FROM      " & strNextDatabaseName & "..prpayrollprocess_tran AS prevPayroll " & _
    '                                                    "LEFT JOIN " & strNextDatabaseName & "..prtnaleave_tran AS prevTnA ON prevTnA.tnaleavetranunkid = prevPayroll.tnaleavetranunkid " & _
    '                                          "WHERE     prevPayroll.isvoid = 0 " & _
    '                                                    "AND prevTnA.isvoid = 0 " & _
    '                                                    "AND tranheadunkid IN ( " & strCumulativeHeadIDs & " ) " & _
    '                                        ") AS b ON a.payperiodunkid = b.payperiodunkid " & _
    '                                                  "AND a.tranheadunkid = b.tranheadunkid " & _
    '                                                  "AND a.employeeunkid = b.employeeunkid " & _
    '                                "WHERE   a.tranheadunkid IN ( " & strCumulativeHeadIDs & " ) " & _
    '                                "AND b.payrollprocesstranunkid IS NULL "

    '                        sqlCmd.CommandText = strQ
    '                        Dim da1 As New SqlDataAdapter(sqlCmd)
    '                        Dim ds1 As New DataSet

    '                        da1.Fill(ds1)

    '                        If ds1.Tables(0).Rows.Count > 1 Then
    '                            Dim intTnAUnkID As Integer = 0
    '                            Dim intRowsAffected As Integer = 0

    '                            Dim allTnAIDs As List(Of String) = (From p In ds1.Tables(0) Select (p.Item("tnaleavetranunkid").ToString)).Distinct().ToList
    '                            Dim strTnAIDs As String = String.Join(",", allTnAIDs.ToArray)

    '                            Dim allPayrollIDs As List(Of String) = (From p In ds1.Tables(0) Select (p.Item("payrollprocesstranunkid").ToString)).Distinct().ToList
    '                            Dim strPayrollIDs As String = String.Join(",", allPayrollIDs.ToArray)

    '                            If allTnAIDs.Count > 0 Then
    '                                For Each strID As String In allTnAIDs
    '                                    strQ = "INSERT INTO " & strNextDatabaseName & "..prtnaleave_tran " & _
    '                                                     "( payperiodunkid, employeeunkid, voucherno, processdate, daysinperiod, paidholidays, totalpayabledays, totalpayablehours, totaldaysworked, totalhoursworked, totalabsentorleave, totalextrahours, totalshorthours, total_amount, balanceamount, userunkid, isvoid, voiduserunkid, voiddatetime, voidreason, totalonholddays, openingbalance, basicsalary, auditdatetime, ip, host, void_ip, void_host, form_name, module_name1, module_name2, module_name3, module_name4, module_name5, isweb ) " & _
    '                                          "SELECT       payperiodunkid, employeeunkid, voucherno, processdate, daysinperiod, paidholidays, totalpayabledays, totalpayablehours, totaldaysworked, totalhoursworked, totalabsentorleave, totalextrahours, totalshorthours, total_amount, balanceamount, userunkid, isvoid, voiduserunkid, voiddatetime, voidreason, totalonholddays, openingbalance, basicsalary, auditdatetime, ip, host, void_ip, void_host, form_name, module_name1, module_name2, module_name3, module_name4, module_name5, isweb " & _
    '                                          "FROM     " & strPrevDatabaseName & "..prtnaleave_tran " & _
    '                                          "WHERE    tnaleavetranunkid IN (" & strID & ") " & _
    '                                          ";SELECT   @@IDENTITY "

    '                                    sqlCmd.CommandText = strQ
    '                                    sqlCmd.Transaction = Trn

    '                                    ds1 = New DataSet
    '                                    da1.Fill(ds1)

    '                                    If ds1.Tables(0).Rows.Count > 0 Then
    '                                        intTnAUnkID = CInt(ds1.Tables(0).Rows(0).Item(0))

    '                                        strQ = "INSERT INTO " & strNextDatabaseName & "..prpayrollprocess_tran " & _
    '                                                        "( tnaleavetranunkid, employeeunkid, tranheadunkid, loanadvancetranunkid, savingtranunkid, amount, add_deduct, currencyunkid, costcenterunkid, vendorunkid, broughtforward, userunkid, isvoid, voiduserunkid, voiddatetime, voidreason, membershiptranunkid, activityunkid ) " & _
    '                                              "SELECT       " & intTnAUnkID & ", employeeunkid, tranheadunkid, loanadvancetranunkid, savingtranunkid, amount, add_deduct, currencyunkid, costcenterunkid, vendorunkid, broughtforward, userunkid, isvoid, voiduserunkid, voiddatetime, voidreason, membershiptranunkid, activityunkid " & _
    '                                              "FROM      " & strPrevDatabaseName & "..prpayrollprocess_tran " & _
    '                                              "WHERE     tnaleavetranunkid IN (" & strID & ") " & _
    '                                              "AND       payrollprocesstranunkid IN (" & strPayrollIDs & ") "

    '                                        sqlCmd.CommandText = strQ
    '                                        sqlCmd.Transaction = Trn
    '                                        intRowsAffected = sqlCmd.ExecuteNonQuery()

    '                                    End If


    '                                Next
    '                            End If

    '                        End If

    '                        strPrevDatabaseName = dsRow.Item("database_name").ToString
    '                    End If
    '                Next
    '            End If
    '        End If


    '        Return True
    '    Catch ex As Exception
    '        Throw New Exception(ex.Message)
    '    End Try
    'End Function
    ''Sohail (09 Nov 2013) -- End

    ''Sohail (21 Jan 2014) -- Start
    ''Enhancement - Oman
    'Public Function UpdateCompletedLoan_BfAmount(ByVal intCompanyUnkId As Integer) As Boolean
    '    Dim strQ As String
    '    Dim ds As New DataSet
    '    Dim ds1 As New DataSet
    '    Dim ds2 As New DataSet
    '    Dim strDatabaseName As String = ""
    '    Dim decCf_Amt As Decimal = 0
    '    Dim decBf_Amt As Decimal = 0
    '    Try
    '        strQ = "SELECT  database_name " & _
    '           "FROM    hrmsConfiguration..cffinancial_year_tran " & _
    '           "WHERE   companyunkid = " & intCompanyUnkId & " " & _
    '           "ORDER BY start_date "

    '        sqlCmd.CommandText = strQ
    '        Dim da As New SqlDataAdapter(sqlCmd)

    '        da.Fill(ds)

    '        If ds.Tables(0).Rows.Count > 0 Then
    '            Dim dsRow As DataRow

    '            For i As Integer = 0 To ds.Tables(0).Rows.Count - 1
    '                dsRow = ds.Tables(0).Rows(i)

    '                strDatabaseName = dsRow.Item("database_name").ToString

    '                If i <> ds.Tables(0).Rows.Count - 1 Then 'Closed Year

    '                    '*** Checking wrong bf_amount entries in closed fy year
    '                    strQ = "SELECT  lnloan_balance_tran.loanadvancetranunkid " & _
    '                                  ", lnloan_balance_tran.employeeunkid " & _
    '                                  ", MIN(cf_amount) AS last_balance_amount " & _
    '                                  ", balance_amount " & _
    '                                  ", emi_amount " & _
    '                                  ", ( MIN(cf_amount) - balance_amount ) AS Diff " & _
    '                            "FROM    " & strDatabaseName & "..lnloan_balance_tran " & _
    '                                    "LEFT JOIN " & strDatabaseName & "..lnloan_advance_tran ON lnloan_balance_tran.loanadvancetranunkid = lnloan_advance_tran.loanadvancetranunkid " & _
    '                            "WHERE   lnloan_balance_tran.isvoid = 0 " & _
    '                                    "AND lnloan_advance_tran.isvoid = 0 " & _
    '                                    "AND lnloan_advance_tran.balance_amount = 0 " & _
    '                            "GROUP BY lnloan_balance_tran.loanadvancetranunkid " & _
    '                                  ", lnloan_balance_tran.employeeunkid " & _
    '                                  ", balance_amount " & _
    '                                  ", emi_amount " & _
    '                            "HAVING  MIN(cf_amount) <> balance_amount " & _
    '                            "ORDER BY lnloan_balance_tran.employeeunkid " & _
    '                                  ", lnloan_balance_tran.loanadvancetranunkid "

    '                    sqlCmd.CommandText = strQ
    '                    da = New SqlDataAdapter(sqlCmd)
    '                    ds1 = New DataSet

    '                    da.Fill(ds1)

    '                    If ds1.Tables(0).Rows.Count > 0 Then

    '                        For Each dtRow As DataRow In ds1.Tables(0).Rows

    '                            strQ = "SELECT  loanbalancetranunkid " & _
    '                                          ", end_date " & _
    '                                          ", bf_amount " & _
    '                                          ", amount " & _
    '                                          ", cf_amount " & _
    '                                    "FROM    " & strDatabaseName & "..lnloan_balance_tran " & _
    '                                    "WHERE   isvoid = 0 " & _
    '                                            "AND loanadvancetranunkid = " & CInt(dtRow.Item("loanadvancetranunkid")) & " " & _
    '                                    "ORDER BY loanbalancetranunkid DESC "


    '                            sqlCmd.CommandText = strQ
    '                            da = New SqlDataAdapter(sqlCmd)
    '                            ds2 = New DataSet

    '                            da.Fill(ds2)

    '                            decCf_Amt = CDec(dtRow.Item("balance_amount"))

    '                            If ds2.Tables(0).Rows.Count > 0 Then
    '                                For Each dRow As DataRow In ds2.Tables(0).Rows

    '                                    '*** Updating lnloan_balance_tran starting with last cf_amount = current balance_amount and updating others entries acordingly
    '                                    strQ = "UPDATE  " & strDatabaseName & "..lnloan_balance_tran " & _
    '                                            "SET     cf_amount = " & decCf_Amt & " " & _
    '                                                  ", bf_amount = " & decCf_Amt & " + amount " & _
    '                                            "WHERE   loanbalancetranunkid = " & CInt(dRow.Item("loanbalancetranunkid")) & " "

    '                                    sqlCmd.CommandText = strQ
    '                                    sqlCmd.Transaction = Trn
    '                                    sqlCmd.ExecuteNonQuery()

    '                                    decCf_Amt += CDec(dRow.Item("amount"))


    '                                Next
    '                            End If

    '                            '*** Finaly Updating actual bf_amount in lnloan_advance_tran
    '                            strQ = "UPDATE  " & strDatabaseName & "..lnloan_advance_tran " & _
    '                                    "SET     bf_amount = " & decCf_Amt & " " & _
    '                                    "WHERE   loanadvancetranunkid = " & CInt(dtRow.Item("loanadvancetranunkid")) & " "

    '                            sqlCmd.CommandText = strQ
    '                            sqlCmd.Transaction = Trn
    '                            sqlCmd.ExecuteNonQuery()

    '                        Next

    '                    End If

    '                Else 'Current Open Year

    '                    '*** Checking wrong bf_amount entries in open fy year
    '                    strQ = "SELECT  lnloan_advance_tran.loanadvancetranunkid " & _
    '                                  ", atlnloan_advance_tran.balance_amount " & _
    '                            "FROM    " & strDatabaseName & "..lnloan_advance_tran " & _
    '                                    "LEFT JOIN " & strDatabaseName & "..atlnloan_advance_tran ON lnloan_advance_tran.loanadvancetranunkid = atlnloan_advance_tran.loanadvancetranunkid " & _
    '                                                                       "AND atlnloan_advance_tran.audittype = 1 " & _
    '                            "WHERE   isvoid = 0 " & _
    '                                    "AND lnloan_advance_tran.isbrought_forward = 0 " & _
    '                                    "AND lnloan_advance_tran.isloan = 1 " & _
    '                                    "AND form_name = 'frmImportLoan_AdvanceWizard' " & _
    '                                    "AND lnloan_advance_tran.balance_amount = 0 " & _
    '                                    "AND lnloan_advance_tran.bf_amount <> atlnloan_advance_tran.balance_amount "

    '                    sqlCmd.CommandText = strQ
    '                    da = New SqlDataAdapter(sqlCmd)
    '                    ds1 = New DataSet

    '                    da.Fill(ds1)


    '                    If ds1.Tables(0).Rows.Count > 0 Then

    '                        For Each dtRow As DataRow In ds1.Tables(0).Rows

    '                            strQ = "SELECT  loanbalancetranunkid " & _
    '                                          ", end_date " & _
    '                                          ", bf_amount " & _
    '                                          ", amount " & _
    '                                          ", cf_amount " & _
    '                                    "FROM    " & strDatabaseName & "..lnloan_balance_tran " & _
    '                                    "WHERE   isvoid = 0 " & _
    '                                            "AND loanadvancetranunkid = " & CInt(dtRow.Item("loanadvancetranunkid")) & " " & _
    '                                    "ORDER BY loanbalancetranunkid "

    '                            sqlCmd.CommandText = strQ
    '                            da = New SqlDataAdapter(sqlCmd)
    '                            ds2 = New DataSet

    '                            da.Fill(ds2)

    '                            decBf_Amt = CDec(dtRow.Item("balance_amount"))

    '                            If ds2.Tables(0).Rows.Count > 0 Then
    '                                For Each dRow As DataRow In ds2.Tables(0).Rows

    '                                    '*** Updating lnloan_balance_tran starting with first bf_amount = balance_amount from first entry in AT table with audit type = 1 and updating others entries acordingly
    '                                    strQ = "UPDATE  " & strDatabaseName & "..lnloan_balance_tran " & _
    '                                            "SET     bf_amount = " & decBf_Amt & " " & _
    '                                                  ", cf_amount = " & decBf_Amt & " - amount " & _
    '                                            "WHERE   loanbalancetranunkid = " & CInt(dRow.Item("loanbalancetranunkid")) & " "

    '                                    sqlCmd.CommandText = strQ
    '                                    sqlCmd.Transaction = Trn
    '                                    sqlCmd.ExecuteNonQuery()

    '                                    decBf_Amt -= CDec(dRow.Item("amount"))

    '                                Next
    '                            End If

    '                            '*** Finaly Updating actual bf_amount in lnloan_advance_tran
    '                            strQ = "UPDATE  " & strDatabaseName & "..lnloan_advance_tran " & _
    '                                    "SET     bf_amount = " & CDec(dtRow.Item("balance_amount")) & " " & _
    '                                    "WHERE   loanadvancetranunkid = " & CInt(dtRow.Item("loanadvancetranunkid")) & " "

    '                            sqlCmd.CommandText = strQ
    '                            sqlCmd.Transaction = Trn
    '                            sqlCmd.ExecuteNonQuery()

    '                        Next

    '                    End If
    '                End If
    '            Next
    '        End If

    '        Return True
    '    Catch ex As Exception
    '        Throw New Exception(ex.Message)
    '    End Try



    'End Function
    'Sohail (21 Jan 2014) -- End


#End Region

End Class