﻿'************************************************************************************************************************************
'Class Name : clsCCTranhead_Approval_Tran.vb
'Purpose    :
'Date       :31-Jul-2018
'Written By :Sandeep Sharma
'Modified   :
'************************************************************************************************************************************
Imports eZeeCommonLib
''' <summary>
''' Purpose: 
''' Developer: Sandeep Sharma
''' </summary>
Public Class clsCCTranhead_Approval_Tran
    Private Const mstrModuleName = "clsCCTranhead_Approval_Tran"
    Dim mstrMessage As String = ""
'S.SANDEEP |17-JAN-2019| -- START
    Dim objDataOperation As clsDataOperation
'S.SANDEEP |17-JAN-2019| -- END
#Region " Private variables "
    Private mstrTranguid As String = String.Empty
    Private mdtTransactiondate As Date
    Private mintMappingunkid As Integer
    Private mstrRemark As String = String.Empty
    Private mblnIsfinal As Boolean
    Private mintStatusunkid As Integer
    Private mintCctranheadunkid As Integer
    Private mdtEffectivedate As Date
    Private mintEmployeeunkid As Integer
    Private mintRehiretranunkid As Integer
    Private mintCctranheadvalueid As Integer
    Private mblnIstransactionhead As Boolean
    Private mintChangereasonunkid As Integer
    Private mblnIsvoid As Boolean
    Private mdtVoiddatetime As Date
    Private mstrVoidreason As String = String.Empty
    Private mintVoiduserunkid As Integer
    Private mintAudittype As Integer
    Private mintAudituserunkid As Integer
    Private mstrIp As String = String.Empty
    Private mstrHostname As String = String.Empty
    Private mstrForm_Name As String = String.Empty
    Private mblnIsweb As Boolean
    Private mblnIsProcessed As Boolean = False
    'S.SANDEEP |17-JAN-2019| -- START
    Private mintOperationTypeId As Integer = 0
    'S.SANDEEP |17-JAN-2019| -- END
#End Region

#Region " Properties "
    ''' <summary>
    ''' Purpose: Get Message from Class 
    ''' Modify By: Sandeep Sharma
    ''' </summary>
    Public ReadOnly Property _Message() As String
        Get
            Return mstrMessage
        End Get
    End Property

    ''' <summary>
    ''' Purpose: Get or Set tranguid
    ''' Modify By: Sandeep Sharma
    ''' </summary>
    Public Property _Tranguid() As String
        Get
            Return mstrTranguid
        End Get
        Set(ByVal value As String)
            mstrTranguid = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set transactiondate
    ''' Modify By: Sandeep Sharma
    ''' </summary>
    Public Property _Transactiondate() As Date
        Get
            Return mdtTransactiondate
        End Get
        Set(ByVal value As Date)
            mdtTransactiondate = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set mappingunkid
    ''' Modify By: Sandeep Sharma
    ''' </summary>
    Public Property _Mappingunkid() As Integer
        Get
            Return mintMappingunkid
        End Get
        Set(ByVal value As Integer)
            mintMappingunkid = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set remark
    ''' Modify By: Sandeep Sharma
    ''' </summary>
    Public Property _Remark() As String
        Get
            Return mstrRemark
        End Get
        Set(ByVal value As String)
            mstrRemark = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set isfinal
    ''' Modify By: Sandeep Sharma
    ''' </summary>
    Public Property _Isfinal() As Boolean
        Get
            Return mblnIsfinal
        End Get
        Set(ByVal value As Boolean)
            mblnIsfinal = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set statusunkid
    ''' Modify By: Sandeep Sharma
    ''' </summary>
    Public Property _Statusunkid() As Integer
        Get
            Return mintStatusunkid
        End Get
        Set(ByVal value As Integer)
            mintStatusunkid = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set cctranheadunkid
    ''' Modify By: Sandeep Sharma
    ''' </summary>
    Public Property _Cctranheadunkid() As Integer
        Get
            Return mintCctranheadunkid
        End Get
        Set(ByVal value As Integer)
            mintCctranheadunkid = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set effectivedate
    ''' Modify By: Sandeep Sharma
    ''' </summary>
    Public Property _Effectivedate() As Date
        Get
            Return mdtEffectivedate
        End Get
        Set(ByVal value As Date)
            mdtEffectivedate = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set employeeunkid
    ''' Modify By: Sandeep Sharma
    ''' </summary>
    Public Property _Employeeunkid() As Integer
        Get
            Return mintEmployeeunkid
        End Get
        Set(ByVal value As Integer)
            mintEmployeeunkid = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set rehiretranunkid
    ''' Modify By: Sandeep Sharma
    ''' </summary>
    Public Property _Rehiretranunkid() As Integer
        Get
            Return mintRehiretranunkid
        End Get
        Set(ByVal value As Integer)
            mintRehiretranunkid = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set cctranheadvalueid
    ''' Modify By: Sandeep Sharma
    ''' </summary>
    Public Property _Cctranheadvalueid() As Integer
        Get
            Return mintCctranheadvalueid
        End Get
        Set(ByVal value As Integer)
            mintCctranheadvalueid = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set istransactionhead
    ''' Modify By: Sandeep Sharma
    ''' </summary>
    Public Property _Istransactionhead() As Boolean
        Get
            Return mblnIstransactionhead
        End Get
        Set(ByVal value As Boolean)
            mblnIstransactionhead = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set changereasonunkid
    ''' Modify By: Sandeep Sharma
    ''' </summary>
    Public Property _Changereasonunkid() As Integer
        Get
            Return mintChangereasonunkid
        End Get
        Set(ByVal value As Integer)
            mintChangereasonunkid = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set isvoid
    ''' Modify By: Sandeep Sharma
    ''' </summary>
    Public Property _Isvoid() As Boolean
        Get
            Return mblnIsvoid
        End Get
        Set(ByVal value As Boolean)
            mblnIsvoid = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set voiddatetime
    ''' Modify By: Sandeep Sharma
    ''' </summary>
    Public Property _Voiddatetime() As Date
        Get
            Return mdtVoiddatetime
        End Get
        Set(ByVal value As Date)
            mdtVoiddatetime = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set voidreason
    ''' Modify By: Sandeep Sharma
    ''' </summary>
    Public Property _Voidreason() As String
        Get
            Return mstrVoidreason
        End Get
        Set(ByVal value As String)
            mstrVoidreason = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set voiduserunkid
    ''' Modify By: Sandeep Sharma
    ''' </summary>
    Public Property _Voiduserunkid() As Integer
        Get
            Return mintVoiduserunkid
        End Get
        Set(ByVal value As Integer)
            mintVoiduserunkid = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set audittype
    ''' Modify By: Sandeep Sharma
    ''' </summary>
    Public Property _Audittype() As Integer
        Get
            Return mintAudittype
        End Get
        Set(ByVal value As Integer)
            mintAudittype = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set audituserunkid
    ''' Modify By: Sandeep Sharma
    ''' </summary>
    Public Property _Audituserunkid() As Integer
        Get
            Return mintAudituserunkid
        End Get
        Set(ByVal value As Integer)
            mintAudituserunkid = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set ip
    ''' Modify By: Sandeep Sharma
    ''' </summary>
    Public Property _Ip() As String
        Get
            Return mstrIp
        End Get
        Set(ByVal value As String)
            mstrIp = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set hostname
    ''' Modify By: Sandeep Sharma
    ''' </summary>
    Public Property _Hostname() As String
        Get
            Return mstrHostname
        End Get
        Set(ByVal value As String)
            mstrHostname = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set form_name
    ''' Modify By: Sandeep Sharma
    ''' </summary>
    Public Property _Form_Name() As String
        Get
            Return mstrForm_Name
        End Get
        Set(ByVal value As String)
            mstrForm_Name = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set isweb
    ''' Modify By: Sandeep Sharma
    ''' </summary>
    Public Property _Isweb() As Boolean
        Get
            Return mblnIsweb
        End Get
        Set(ByVal value As Boolean)
            mblnIsweb = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set mblnIsProcessed
    ''' Modify By: Sandeep Sharma
    ''' </summary>
    Public Property _IsProcessed() As Boolean
        Get
            Return mblnIsProcessed
        End Get
        Set(ByVal value As Boolean)
            mblnIsProcessed = value
        End Set
    End Property

    'S.SANDEEP |17-JAN-2019| -- START
    Public Property _OperationTypeId() As Integer
        Get
            Return mintOperationTypeId
        End Get
        Set(ByVal value As Integer)
            mintOperationTypeId = value
        End Set
    End Property
    'S.SANDEEP |17-JAN-2019| -- END
#End Region

    ''' <summary>
    ''' Modify By: Sandeep Sharma
    ''' <purpose> Assign all Property variable </purpose>
    ''' ''' </summary>
    Public Function GetList(ByVal strTableName As String, ByVal blnIsCostCenter As Boolean, Optional ByVal xEmployeeUnkid As Integer = 0, Optional ByVal blnOnlyPending As Boolean = True) As DataSet 'S.SANDEEP [14-AUG-2018] -- START {Ref#244} [blnOnlyPending] -- END
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception
        Dim mdtAppDate As String = ""
        Dim objDataOperation As New clsDataOperation

        Try

            strQ = "SELECT " & _
                   "    @ADate = CONVERT(CHAR(8),hremployee_master.appointeddate,112) " & _
                   "FROM hremployee_master " & _
                   "WHERE employeeunkid = '" & xEmployeeUnkid & "' "

            objDataOperation.AddParameter("@ADate", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mdtAppDate, ParameterDirection.InputOutput)

            objDataOperation.ExecNonQuery(strQ)

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            mdtAppDate = objDataOperation.GetParameterValue("@ADate")

            strQ = "SELECT " & _
                    "  hremployee_cctranhead_approval_tran.tranguid " & _
                    ", hremployee_cctranhead_approval_tran.transactiondate " & _
                    ", hremployee_cctranhead_approval_tran.mappingunkid " & _
                    ", hremployee_cctranhead_approval_tran.remark " & _
                    ", hremployee_cctranhead_approval_tran.isfinal " & _
                    ", hremployee_cctranhead_approval_tran.statusunkid " & _
                   ", hremployee_cctranhead_approval_tran.cctranheadunkid " & _
                   ", hremployee_cctranhead_approval_tran.effectivedate " & _
                   ", hremployee_cctranhead_approval_tran.employeeunkid " & _
                   ", hremployee_cctranhead_approval_tran.rehiretranunkid " & _
                   ", hremployee_cctranhead_approval_tran.cctranheadvalueid " & _
                   ", hremployee_cctranhead_approval_tran.istransactionhead " & _
                   ", hremployee_cctranhead_approval_tran.changereasonunkid " & _
                   ", hremployee_cctranhead_approval_tran.isvoid " & _
                   ", hremployee_cctranhead_approval_tran.voiduserunkid " & _
                   ", hremployee_cctranhead_approval_tran.voiddatetime " & _
                   ", hremployee_cctranhead_approval_tran.voidreason " & _
                   ", hremployee_master.employeecode as ecode " & _
                   ", hremployee_master.firstname+' '+ISNULL(hremployee_master.othername,'')+' '+hremployee_master.surname AS ename " & _
                   ", CONVERT(CHAR(8),hremployee_cctranhead_approval_tran.effectivedate,112) AS edate " & _
                   ", CONVERT(CHAR(8),hremployee_master.appointeddate,112) AS adate " & _
                   ", '' AS EffDate " & _
                   ", CASE WHEN rehiretranunkid > 0 THEN ISNULL(RH.name,'') ELSE ISNULL(cfcommon_master.name,'') END AS CReason " & _
                   ", CASE WHEN hremployee_cctranhead_approval_tran.istransactionhead = 1 THEN ISNULL(prtranhead_master.trnheadname,'') " & _
                   "    ELSE ISNULL(prcostcenter_master.costcentername,'') END AS DispValue " & _
                   ", ISNULL(prcostcenter_master.costcentername,'') AS costcentername " & _
                   ", ISNULL(prtranhead_master.trnheadname,'') AS trnheadname " & _
                   ", hremployee_cctranhead_approval_tran.isvoid " & _
                    ", hremployee_cctranhead_approval_tran.voiddatetime " & _
                    ", hremployee_cctranhead_approval_tran.voidreason " & _
                    ", hremployee_cctranhead_approval_tran.voiduserunkid " & _
                    ", hremployee_cctranhead_approval_tran.audittype " & _
                    ", hremployee_cctranhead_approval_tran.audituserunkid " & _
                    ", hremployee_cctranhead_approval_tran.ip " & _
                    ", hremployee_cctranhead_approval_tran.hostname " & _
                    ", hremployee_cctranhead_approval_tran.form_name " & _
                    ", hremployee_cctranhead_approval_tran.isweb " & _
                     ", ISNULL(hremployee_cctranhead_approval_tran.operationtypeid,0) AS operationtypeid " & _
                   ", CASE WHEN ISNULL(hremployee_cctranhead_approval_tran.operationtypeid,0) = '" & CInt(clsEmployeeMovmentApproval.enOperationType.ADDED) & "' THEN @ADDED " & _
                   "       WHEN ISNULL(hremployee_cctranhead_approval_tran.operationtypeid,0) = '" & CInt(clsEmployeeMovmentApproval.enOperationType.EDITED) & "' THEN @EDITED " & _
                   "       WHEN ISNULL(hremployee_cctranhead_approval_tran.operationtypeid,0) = '" & CInt(clsEmployeeMovmentApproval.enOperationType.DELETED) & "' THEN @DELETED " & _
                   "  END AS OperationType " & _
                 "FROM hremployee_cctranhead_approval_tran " & _
                   "  JOIN hremployee_master ON hremployee_master.employeeunkid = hremployee_cctranhead_approval_tran.employeeunkid " & _
                   "  LEFT JOIN cfcommon_master ON cfcommon_master.masterunkid = hremployee_cctranhead_approval_tran.changereasonunkid AND cfcommon_master.mastertype = " & clsCommon_Master.enCommonMaster.COST_CENTER & _
                   "  LEFT JOIN cfcommon_master AS RH ON RH.masterunkid = hremployee_cctranhead_approval_tran.changereasonunkid AND RH.mastertype = " & clsCommon_Master.enCommonMaster.RE_HIRE & _
                   "  LEFT JOIN prcostcenter_master ON cctranheadvalueid = prcostcenter_master.costcenterunkid " & _
                   "  LEFT JOIN prtranhead_master ON prtranhead_master.tranheadunkid = hremployee_cctranhead_approval_tran.cctranheadvalueid " & _
                   "WHERE hremployee_cctranhead_approval_tran.isvoid = 0 AND hremployee_cctranhead_approval_tran.cctranheadunkid >= 0 AND ISNULL(hremployee_cctranhead_approval_tran.isprocessed,0) = 0 "
            'S.SANDEEP |17-JAN-2019| -- START {operationtypeid} -- END
            If blnIsCostCenter = True Then
                strQ &= " AND hremployee_cctranhead_approval_tran.istransactionhead = 0 "
            Else
                strQ &= " AND hremployee_cctranhead_approval_tran.istransactionhead = 1 "
            End If

            If xEmployeeUnkid > 0 Then
                strQ &= " AND hremployee_cctranhead_approval_tran.employeeunkid = '" & xEmployeeUnkid & "' "
            End If

            If mdtAppDate.Trim.Length > 0 Then
                strQ &= " AND CONVERT(CHAR(8),hremployee_cctranhead_approval_tran.effectivedate,112) >= '" & mdtAppDate & "' "
            End If

            strQ &= " ORDER BY effectivedate DESC "
            'S.SANDEEP |17-JAN-2019| -- START
            objDataOperation.AddParameter("@ADDED", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage("clsEmployeeMovmentApproval", 62, "Newly Added Information"))
            objDataOperation.AddParameter("@EDITED", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage("clsEmployeeMovmentApproval", 63, "Information Edited"))
            objDataOperation.AddParameter("@DELETED", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage("clsEmployeeMovmentApproval", 64, "Information Deleted"))
            'S.SANDEEP |17-JAN-2019| -- END
            dsList = objDataOperation.ExecQuery(strQ, strTableName)

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            'S.SANDEEP [14-AUG-2018] -- START
            'ISSUE/ENHANCEMENT : {Ref#244}
            Dim strValue As String = String.Join(",", dsList.Tables(0).AsEnumerable().GroupBy(Function(x) x.Field(Of Integer)("employeeunkid")).[Select](Function(x) New With {Key .empid = x.Key, Key .empstat = x.[Select](Function(z) z.Field(Of Integer)("statusunkid")).Max}).Where(Function(x) x.empstat = 2 Or x.empstat = 3).Select(Function(x) x.empstat.ToString()).ToArray())
            'S.SANDEEP [09-AUG-2018] -- START {Added : x.empstat | Removed : x.empid} -- END
            If strValue.Trim.Length > 0 Then
                'S.SANDEEP [09-AUG-2018] -- START
                'Dim dtTbl As DataTable = New DataView(dsList.Tables(0), "employeeunkid NOT IN (" & strValue & ")", "", DataViewRowState.CurrentRows).ToTable().Copy()
                Dim dtTbl As DataTable = Nothing
                If strValue = clsEmployee_Master.EmpApprovalStatus.Rejected Then
                    strValue = String.Join(",", dsList.Tables(0).AsEnumerable().GroupBy(Function(x) x.Field(Of Integer)("employeeunkid")).[Select](Function(x) New With {Key .empid = x.Key, Key .empstat = x.[Select](Function(z) z.Field(Of Integer)("statusunkid")).Max}).Where(Function(x) x.empstat = 2 Or x.empstat = 3).Select(Function(x) x.empid.ToString()).ToArray())
                    If strValue.Trim.Length > 0 Then
                        dtTbl = New DataView(dsList.Tables(0), "employeeunkid NOT IN (" & strValue & ")", "", DataViewRowState.CurrentRows).ToTable().Copy()
                    End If
                Else
                    dtTbl = New DataView(dsList.Tables(0), "statusunkid NOT IN (" & strValue & ")", "", DataViewRowState.CurrentRows).ToTable().Copy()
                End If
                'S.SANDEEP [09-AUG-2018] -- END
                dsList.Tables.Remove(strTableName)
                dsList.Tables.Add(dtTbl)
            End If
            'S.SANDEEP [14-AUG-2018] -- END

            For Each xRow As DataRow In dsList.Tables(0).Rows
                xRow("EffDate") = eZeeDate.convertDate(xRow.Item("edate").ToString).ToShortDateString
            Next

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: GetList; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
        Return dsList
    End Function

    ''' <summary>
    ''' Modify By: Sandeep Sharma
    ''' </summary>
    ''' <returns>Boolean</returns>
    ''' <purpose> INSERT INTO Database Table (hremployee_cctranhead_approval_tran) </purpose>
    Public Function Insert(ByVal eOprType As clsEmployeeMovmentApproval.enOperationType, Optional ByVal xDataOpr As clsDataOperation = Nothing, Optional ByVal blnFromApproval As Boolean = False) As Boolean
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception
'S.SANDEEP |17-JAN-2019| -- START

        'Dim objDataOperation As New clsDataOperation

        'If xDataOpr IsNot Nothing Then
        '    objDataOperation = xDataOpr
        '    objDataOperation.ClearParameters()
        'Else
        '    objDataOperation = New clsDataOperation
        'End If

        If xDataOpr IsNot Nothing Then
            objDataOperation = xDataOpr
        Else
            objDataOperation = New clsDataOperation
            objDataOperation.BindTransaction()
        End If
        objDataOperation.ClearParameters()

        'S.SANDEEP |17-JAN-2019| -- END

        Dim blnIsCostCenter As Boolean = False
        If mblnIstransactionhead = True Then
            blnIsCostCenter = False
        Else
            blnIsCostCenter = True
        End If

        If blnFromApproval = False Then
'S.SANDEEP |17-JAN-2019| -- START Add Opration Type

            If isExist(Nothing, -1, blnIsCostCenter, mintEmployeeunkid, eOprType, , , objDataOperation, True) Then
                mstrMessage = Language.getMessage(mstrModuleName, 4, "Sorry, CostCenter information is already present in pending state for the selected employee.")
                Return False
            End If

            If isExist(mdtEffectivedate, -1, blnIsCostCenter, mintEmployeeunkid, eOprType, , , objDataOperation) Then
                If mblnIstransactionhead = True Then
                    mstrMessage = Language.getMessage(mstrModuleName, 1, "Sorry, transaction head information is already present for the selected effective date.")
                Else
                    mstrMessage = Language.getMessage(mstrModuleName, 2, "Sorry, cost center information is already present for the selected effective date.")
                End If
                Return False
            End If

            If isExist(mdtEffectivedate, mintCctranheadvalueid, blnIsCostCenter, mintEmployeeunkid, eOprType, , , objDataOperation) Then
                If mblnIstransactionhead = True Then
                    mstrMessage = Language.getMessage(mstrModuleName, 1, "Sorry, transaction head information is already present for the selected effective date.")
                Else
                    mstrMessage = Language.getMessage(mstrModuleName, 2, "Sorry, cost center information is already present for the selected effective date.")
                End If
                Return False
            End If
        End If
'S.SANDEEP |17-JAN-2019| -- END
        objDataOperation.ClearParameters()
        Try
            objDataOperation.AddParameter("@tranguid", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrTranguid.ToString)
            objDataOperation.AddParameter("@transactiondate", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, mdtTransactiondate.ToString)
            objDataOperation.AddParameter("@mappingunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintMappingunkid.ToString)
            objDataOperation.AddParameter("@remark", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrRemark.ToString)
            objDataOperation.AddParameter("@isfinal", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsfinal.ToString)
            objDataOperation.AddParameter("@statusunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintStatusunkid.ToString)
            objDataOperation.AddParameter("@cctranheadunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintCctranheadunkid.ToString)
            objDataOperation.AddParameter("@effectivedate", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, mdtEffectivedate.ToString)
            objDataOperation.AddParameter("@employeeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintEmployeeunkid.ToString)
            objDataOperation.AddParameter("@rehiretranunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintRehiretranunkid.ToString)
            objDataOperation.AddParameter("@cctranheadvalueid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintCctranheadvalueid.ToString)
            objDataOperation.AddParameter("@istransactionhead", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIstransactionhead.ToString)
            objDataOperation.AddParameter("@changereasonunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintChangereasonunkid.ToString)
            objDataOperation.AddParameter("@isvoid", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsvoid.ToString)
            If mdtVoiddatetime <> Nothing Then
                objDataOperation.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, mdtVoiddatetime)
            Else
                objDataOperation.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, DBNull.Value)
            End If
            objDataOperation.AddParameter("@voidreason", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrVoidreason.ToString)
            objDataOperation.AddParameter("@voiduserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintVoiduserunkid.ToString)
            objDataOperation.AddParameter("@audittype", SqlDbType.Int, eZeeDataType.INT_SIZE, mintAudittype.ToString)
            objDataOperation.AddParameter("@audituserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintAudituserunkid.ToString)
            objDataOperation.AddParameter("@ip", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrIp.ToString)
            objDataOperation.AddParameter("@hostname", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrHostname.ToString)
            objDataOperation.AddParameter("@form_name", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrForm_Name.ToString)
            objDataOperation.AddParameter("@isweb", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsweb.ToString)
            objDataOperation.AddParameter("@isprocessed", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsProcessed)
            'S.SANDEEP |17-JAN-2019| -- START
            objDataOperation.AddParameter("@operationtypeid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintOperationTypeId)
            'S.SANDEEP |17-JAN-2019| -- END
            strQ = "INSERT INTO hremployee_cctranhead_approval_tran ( " & _
              "  tranguid " & _
              ", transactiondate " & _
              ", mappingunkid " & _
              ", remark " & _
              ", isfinal " & _
              ", statusunkid " & _
              ", cctranheadunkid " & _
              ", effectivedate " & _
              ", employeeunkid " & _
              ", rehiretranunkid " & _
              ", cctranheadvalueid " & _
              ", istransactionhead " & _
              ", changereasonunkid " & _
              ", isvoid " & _
              ", voiddatetime " & _
              ", voidreason " & _
              ", voiduserunkid " & _
              ", audittype " & _
              ", audituserunkid " & _
              ", ip " & _
              ", hostname " & _
              ", form_name " & _
              ", isweb" & _
              ", isprocessed " & _
               ", operationtypeid " & _
            ") VALUES (" & _
              "  @tranguid " & _
              ", @transactiondate " & _
              ", @mappingunkid " & _
              ", @remark " & _
              ", @isfinal " & _
              ", @statusunkid " & _
              ", @cctranheadunkid " & _
              ", @effectivedate " & _
              ", @employeeunkid " & _
              ", @rehiretranunkid " & _
              ", @cctranheadvalueid " & _
              ", @istransactionhead " & _
              ", @changereasonunkid " & _
              ", @isvoid " & _
              ", @voiddatetime " & _
              ", @voidreason " & _
              ", @voiduserunkid " & _
              ", @audittype " & _
              ", @audituserunkid " & _
              ", @ip " & _
              ", @hostname " & _
              ", @form_name " & _
              ", @isweb" & _
              ", @isprocessed " & _
              ", @operationtypeid " & _
            "); SELECT @@identity"

            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            'mstrTranguid = dsList.Tables(0).Rows(0).Item(0)
            'S.SANDEEP |17-JAN-2019| -- START


            If xDataOpr Is Nothing Then
                objDataOperation.ReleaseTransaction(True)
            End If
            'S.SANDEEP |17-JAN-2019| -- END
            Return True
        Catch ex As Exception
            'S.SANDEEP |17-JAN-2019| -- START
            If xDataOpr Is Nothing Then objDataOperation.ReleaseTransaction(False)
            'S.SANDEEP |17-JAN-2019| -- END
            Throw New Exception(ex.Message & "; Procedure Name: Insert; Module Name: " & mstrModuleName)
            Return False
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            If xDataOpr Is Nothing Then objDataOperation = Nothing
        End Try
    End Function

    Public Function Update(ByVal xDataOpr As clsDataOperation, ByVal intEmployeeId As Integer, ByVal intTranUnkid As Integer, ByVal dtEffDate As Date, ByVal blnIstransactionhead As Boolean, ByVal strColName As String) As Boolean
        Dim strQ As String = ""
        Dim exForce As Exception
        'S.SANDEEP |17-JAN-2019| -- START
        If xDataOpr IsNot Nothing Then
            objDataOperation = xDataOpr
        Else
            objDataOperation = New clsDataOperation
            objDataOperation.BindTransaction()
        End If
        objDataOperation.ClearParameters()
'S.SANDEEP |17-JAN-2019| -- END

        Try
            'S.SANDEEP [15-NOV-2018] -- START
            strQ = "UPDATE hremployee_cctranhead_approval_tran SET " & _
                   " " & strColName & " = '" & intTranUnkid & "' " & _
                   "WHERE isprocessed = 0 and CONVERT(CHAR(8),effectivedate,112) = '" & eZeeDate.convertDate(dtEffDate).ToString & "' AND employeeunkid = '" & intEmployeeId & "' "
            '----REMOVED (effectivedate)
            '----ADDED (CONVERT(CHAR(8),effectivedate,112))
            'S.SANDEEP [15-NOV-2018] -- End
            If blnIstransactionhead Then
                strQ &= " AND istransactionhead = 1 "
            Else
                strQ &= " AND istransactionhead = 0 "
            End If

            Call xDataOpr.ExecNonQuery(strQ)

            If xDataOpr.ErrorMessage <> "" Then
                exForce = New Exception(xDataOpr.ErrorNumber & ": " & xDataOpr.ErrorMessage)
                Throw exForce
            End If
            'S.SANDEEP |17-JAN-2019| -- START


            If xDataOpr Is Nothing Then
                objDataOperation.ReleaseTransaction(True)
            End If
            'S.SANDEEP |17-JAN-2019| -- END
            Return True
        Catch ex As Exception
            'S.SANDEEP |17-JAN-2019| -- START
            If xDataOpr Is Nothing Then objDataOperation.ReleaseTransaction(False)
            'S.SANDEEP |17-JAN-2019| -- END
            Throw New Exception(ex.Message & "; Procedure Name: Update; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If xDataOpr Is Nothing Then objDataOperation = Nothing
        End Try
    End Function

    ''' <summary>
    ''' Modify By: Sandeep Sharma
    ''' </summary>
    ''' <returns>Boolean</returns>
    ''' <purpose> Update Database Table (hremployee_cctranhead_approval_tran) </purpose>
    Public Function Update(ByVal eOprType As clsEmployeeMovmentApproval.enOperationType, Optional ByVal xDataOpr As clsDataOperation = Nothing) As Boolean
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception
            'S.SANDEEP |17-JAN-2019| -- START


'Dim objDataOperation As New clsDataOperation


       


        Dim blnIsCostCenter As Boolean = False
        If mblnIstransactionhead = True Then
            blnIsCostCenter = False
        Else
            blnIsCostCenter = True
        End If

 'If xDataOpr IsNot Nothing Then
 '           objDataOperation = xDataOpr
 '           objDataOperation.ClearParameters()
 '       Else
 '           objDataOperation = New clsDataOperation
 '       End If

        If xDataOpr IsNot Nothing Then
            objDataOperation = xDataOpr
        Else
            objDataOperation = New clsDataOperation
            objDataOperation.BindTransaction()
        End If
        objDataOperation.ClearParameters()
            'S.SANDEEP |17-JAN-2019| -- END

        If isExist(mdtEffectivedate, -1, blnIsCostCenter, mintEmployeeunkid, eOprType, mstrTranguid, "", objDataOperation) Then
            If mblnIstransactionhead = True Then
                mstrMessage = Language.getMessage(mstrModuleName, 1, "Sorry, transaction head information is already present for the selected effective date.")
            Else
                mstrMessage = Language.getMessage(mstrModuleName, 2, "Sorry, cost center information is already present for the selected effective date.")
            End If
            Return False
        End If

        If isExist(mdtEffectivedate, mintCctranheadvalueid, blnIsCostCenter, mstrTranguid, mintCctranheadunkid, mstrTranguid, "", objDataOperation) Then
            If mblnIstransactionhead = True Then
                mstrMessage = Language.getMessage(mstrModuleName, 1, "Sorry, transaction head information is already present for the selected effective date.")
            Else
                mstrMessage = Language.getMessage(mstrModuleName, 2, "Sorry, cost center information is already present for the selected effective date.")
            End If
            Return False
        End If

        Try
            objDataOperation.AddParameter("@tranguid", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrTranguid.ToString)
            objDataOperation.AddParameter("@transactiondate", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, mdtTransactiondate.ToString)
            objDataOperation.AddParameter("@mappingunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintMappingunkid.ToString)
            objDataOperation.AddParameter("@remark", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrRemark.ToString)
            objDataOperation.AddParameter("@isfinal", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsfinal.ToString)
            objDataOperation.AddParameter("@statusunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintStatusunkid.ToString)
            objDataOperation.AddParameter("@cctranheadunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintCctranheadunkid.ToString)
            objDataOperation.AddParameter("@effectivedate", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, mdtEffectivedate.ToString)
            objDataOperation.AddParameter("@employeeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintEmployeeunkid.ToString)
            objDataOperation.AddParameter("@rehiretranunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintRehiretranunkid.ToString)
            objDataOperation.AddParameter("@cctranheadvalueid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintCctranheadvalueid.ToString)
            objDataOperation.AddParameter("@istransactionhead", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIstransactionhead.ToString)
            objDataOperation.AddParameter("@changereasonunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintChangereasonunkid.ToString)
            objDataOperation.AddParameter("@isvoid", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsvoid.ToString)
            If mdtVoiddatetime <> Nothing Then
                objDataOperation.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, mdtVoiddatetime)
            Else
                objDataOperation.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, DBNull.Value)
            End If
            objDataOperation.AddParameter("@voidreason", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrVoidreason.ToString)
            objDataOperation.AddParameter("@voiduserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintVoiduserunkid.ToString)
            objDataOperation.AddParameter("@audittype", SqlDbType.Int, eZeeDataType.INT_SIZE, mintAudittype.ToString)
            objDataOperation.AddParameter("@audituserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintAudituserunkid.ToString)
            objDataOperation.AddParameter("@ip", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrIp.ToString)
            objDataOperation.AddParameter("@hostname", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrHostname.ToString)
            objDataOperation.AddParameter("@form_name", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrForm_Name.ToString)
            objDataOperation.AddParameter("@isweb", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsweb.ToString)
            'S.SANDEEP |17-JAN-2019| -- START
            objDataOperation.AddParameter("@operationtypeid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintOperationTypeId)
            'S.SANDEEP |17-JAN-2019| -- END

            strQ = "UPDATE hremployee_cctranhead_approval_tran SET " & _
              "  transactiondate = @transactiondate" & _
              ", mappingunkid = @mappingunkid" & _
              ", remark = @remark" & _
              ", isfinal = @isfinal" & _
              ", statusunkid = @statusunkid" & _
              ", cctranheadunkid = @cctranheadunkid" & _
              ", effectivedate = @effectivedate" & _
              ", employeeunkid = @employeeunkid" & _
              ", rehiretranunkid = @rehiretranunkid" & _
              ", cctranheadvalueid = @cctranheadvalueid" & _
              ", istransactionhead = @istransactionhead" & _
              ", changereasonunkid = @changereasonunkid" & _
              ", isvoid = @isvoid" & _
              ", voiddatetime = @voiddatetime" & _
              ", voidreason = @voidreason" & _
              ", voiduserunkid = @voiduserunkid" & _
              ", audittype = @audittype" & _
              ", audituserunkid = @audituserunkid" & _
              ", ip = @ip" & _
              ", hostname = @hostname" & _
              ", form_name = @form_name" & _
              ", isweb = @isweb " & _
              ", operationtypeid = @operationtypeid " & _
            "WHERE tranguid = @tranguid "
            'S.SANDEEP |17-JAN-2019| -- START {operationtypeid} -- END

            Call objDataOperation.ExecNonQuery(strQ)

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If
            'S.SANDEEP |17-JAN-2019| -- START


            If xDataOpr Is Nothing Then
                objDataOperation.ReleaseTransaction(True)
            End If
            'S.SANDEEP |17-JAN-2019| -- END
            Return True
        Catch ex As Exception
            'S.SANDEEP |17-JAN-2019| -- START

            If xDataOpr Is Nothing Then objDataOperation.ReleaseTransaction(False)
           
            'S.SANDEEP |17-JAN-2019| -- END
            Throw New Exception(ex.Message & "; Procedure Name: Update; Module Name: " & mstrModuleName)
            Return False
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            If xDataOpr Is Nothing Then objDataOperation = Nothing
        End Try
    End Function

    Public Function Delete(ByVal intUnkid As Integer, ByVal eOprType As clsEmployeeMovmentApproval.enOperationType, Optional ByVal xDataOpr As clsDataOperation = Nothing) As Boolean
        'If isUsed(intUnkid) Then
        '    mstrMessage = "<Message>"
        '    Return False
        'End If

        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception
        Dim blnFlag As Boolean = False

        If xDataOpr IsNot Nothing Then
            objDataOperation = xDataOpr
        Else
            objDataOperation = New clsDataOperation
            objDataOperation.BindTransaction()
        End If
        objDataOperation.ClearParameters()

        mstrMessage = ""
        Try
            Dim objemployee_cctranhead_tran As New clsemployee_cctranhead_tran

            objemployee_cctranhead_tran._Cctranheadunkid(objDataOperation) = intUnkid

            Me._Audittype = enAuditType.ADD
            Me._Cctranheadvalueid = objemployee_cctranhead_tran._Cctranheadvalueid
            Me._Changereasonunkid = objemployee_cctranhead_tran._Changereasonunkid
            Me._Effectivedate = objemployee_cctranhead_tran._Effectivedate
            Me._Employeeunkid = objemployee_cctranhead_tran._Employeeunkid
            Me._Istransactionhead = objemployee_cctranhead_tran._Istransactionhead
            Me._Rehiretranunkid = 0
            Me._Statusunkid = clsEmployee_Master.EmpApprovalStatus.SubmitForApproval
            Me._Tranguid = Guid.NewGuid.ToString()
            Me._Transactiondate = Now
            Me._Remark = ""
            Me._Rehiretranunkid = 0
            Me._Mappingunkid = 0
            Me._Isfinal = False
            Me._Cctranheadunkid = intUnkid
            Me._IsProcessed = False
            Me._OperationTypeId = clsEmployeeMovmentApproval.enOperationType.DELETED
            Me._Statusunkid = clsEmployee_Master.EmpApprovalStatus.SubmitForApproval

            'Gajanan [9-July-2019] -- Start      
            'ISSUE/ENHANCEMENT : AT Changes For Display Missing Column
            Me._Isvoid = False
            'Gajanan [9-July-2019] -- End

            objemployee_cctranhead_tran._Voidreason = Me._Voidreason
            objemployee_cctranhead_tran._Voiduserunkid = Me._Audituserunkid
            objemployee_cctranhead_tran._Voiddatetime = Now
            objemployee_cctranhead_tran._Isvoid = True
           

            'Gajanan [9-July-2019] -- Start      
            'ISSUE/ENHANCEMENT : AT Changes For Display Missing Column
            objemployee_cctranhead_tran._ClientIP = Me._Ip
            objemployee_cctranhead_tran._FormName = Me._Form_Name
            objemployee_cctranhead_tran._HostName = Me._Hostname
            objemployee_cctranhead_tran._AuditUserId = Me._Audituserunkid
            'Gajanan [9-July-2019] -- End

            blnFlag = Insert(clsEmployeeMovmentApproval.enOperationType.DELETED, objDataOperation)

            If blnFlag = True AndAlso mstrMessage.Trim.Length <= 0 Then
                If objemployee_cctranhead_tran.Delete(intUnkid, objDataOperation) = False Then
            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If
                End If
            End If

            If xDataOpr Is Nothing Then
                objDataOperation.ReleaseTransaction(True)
            End If
            Return blnFlag
        Catch ex As Exception
            If xDataOpr Is Nothing Then objDataOperation.ReleaseTransaction(False)
            Throw New Exception(ex.Message & "; Procedure Name: Delete; Module Name: " & mstrModuleName)
            Return False
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            If xDataOpr Is Nothing Then objDataOperation = Nothing
        End Try
    End Function

    ''' <summary>
    ''' Modify By: Sandeep Sharma
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>
    Public Function isUsed(ByVal intUnkid As Integer) As Boolean
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        Dim objDataOperation As New clsDataOperation

        Try
            strQ = "<Query>"

            objDataOperation.AddParameter("@tranguid", SqlDbType.Int, eZeeDataType.INT_SIZE, intUnkid)

            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            Return dsList.Tables(0).Rows.Count > 0
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: isUsed; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
    End Function

    ''' <summary>
    ''' Modify By: Sandeep Sharma
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>
    Public Function isExist(ByVal xEffDate As DateTime, _
                            ByVal xCCTrnHeadId As Integer, _
                            ByVal xIsCostCenter As Boolean, _
                            ByVal xEmployeeId As Integer, _
                            ByVal xeOprType As clsEmployeeMovmentApproval.enOperationType, _
                            Optional ByVal strtrangUid As String = "", _
                            Optional ByVal strUnkid As String = "", Optional ByVal xDataOpr As clsDataOperation = Nothing, _
                            Optional ByVal blnPreventNewEntry As Boolean = False, Optional ByVal xTranferUnkid As Integer = 0) As Boolean
        'S.SANDEEP |17-JAN-2019| -- START {xTranferUnkid} -- END
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception


            'S.SANDEEP |17-JAN-2019| -- START


        If xDataOpr IsNot Nothing Then
            objDataOperation = xDataOpr
        Else
            objDataOperation = New clsDataOperation
        End If
        objDataOperation.ClearParameters()
            'S.SANDEEP |17-JAN-2019| -- END
        Try
            strQ = "SELECT " & _
              "  tranguid " & _
              ", transactiondate " & _
              ", mappingunkid " & _
              ", remark " & _
              ", isfinal " & _
              ", statusunkid " & _
              ", cctranheadunkid " & _
              ", effectivedate " & _
              ", employeeunkid " & _
              ", rehiretranunkid " & _
              ", cctranheadvalueid " & _
              ", istransactionhead " & _
              ", changereasonunkid " & _
              ", isvoid " & _
              ", voiddatetime " & _
              ", voidreason " & _
              ", voiduserunkid " & _
              ", audittype " & _
              ", audituserunkid " & _
              ", ip " & _
              ", hostname " & _
              ", form_name " & _
              ", isweb " & _
             "FROM hremployee_cctranhead_approval_tran " & _
             "WHERE isvoid = 0 and operationtypeid = @operationtypeid " & _
             "AND statusunkid <> 3 "

            If blnPreventNewEntry Then
                'S.SANDEEP [09-AUG-2018] -- START
                'strQ &= " AND hremployee_cctranhead_approval_tran.isfinal = 0 "
                'strQ &= " AND hremployee_cctranhead_approval_tran.isfinal = 0 AND cctranheadunkid <= 0 AND ISNULL(isprocessed,0) = 0 "
                strQ &= " AND hremployee_cctranhead_approval_tran.isfinal = 0 AND ISNULL(isprocessed,0) = 0 "
                'S.SANDEEP [09-AUG-2018] -- END
            End If

            If xEffDate <> Nothing Then
                strQ &= " AND CONVERT(CHAR(8),effectivedate,112) = @effectivedate "
            End If

            If strtrangUid.Trim.Length > 0 Then
                strQ &= " AND tranguid <> @tranguid "
            End If

            If xEmployeeId > 0 Then
                strQ &= " AND employeeunkid = @employeeunkid "
            End If

            If xCCTrnHeadId > 0 Then
                strQ &= " AND cctranheadvalueid = @cctranheadvalueid "
                objDataOperation.AddParameter("@cctranheadvalueid", SqlDbType.Int, eZeeDataType.INT_SIZE, xCCTrnHeadId)
            End If

            If xIsCostCenter = True Then
                strQ &= " AND istransactionhead = 0 "
            Else
                strQ &= " AND istransactionhead = 1 "
            End If

            'S.SANDEEP |17-JAN-2019| -- START
            If xeOprType = clsEmployeeMovmentApproval.enOperationType.EDITED Or mintOperationTypeId = clsEmployeeMovmentApproval.enOperationType.DELETED Then
                If xTranferUnkid > 0 Then
                    strQ &= " AND cctranheadunkid = @cctranheadunkid "
                    objDataOperation.AddParameter("@cctranheadunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, xTranferUnkid)
                End If
            End If
            'S.SANDEEP |17-JAN-2019| -- END
            objDataOperation.AddParameter("@effectivedate", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, eZeeDate.convertDate(xEffDate))
            objDataOperation.AddParameter("@employeeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, xEmployeeId)
            objDataOperation.AddParameter("@tranguid", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, strtranguid)
            objDataOperation.AddParameter("@operationtypeid", SqlDbType.Int, eZeeDataType.INT_SIZE, xeOprType)

            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            Return dsList.Tables(0).Rows.Count > 0
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: isExist; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            If xDataOpr Is Nothing Then objDataOperation = Nothing
        End Try
    End Function

End Class