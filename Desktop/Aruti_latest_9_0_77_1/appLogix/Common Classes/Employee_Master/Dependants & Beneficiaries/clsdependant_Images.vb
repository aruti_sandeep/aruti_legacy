﻿'************************************************************************************************************************************
'Class Name : clsdependant_Images.vb
'Purpose    :
'Date       :29/03/2013
'Written By :Pinkal
'Modified   :
'************************************************************************************************************************************

Imports eZeeCommonLib
Imports eZee.Common.eZeeForm


''' <summary>
''' Purpose: 
''' Developer: Pinkal
''' </summary>
Public Class clsdependant_Images
    Private Shared ReadOnly mstrModuleName As String = "clsdependant_Images"
    Dim objDataOperation As clsDataOperation
    Dim mstrMessage As String = ""

#Region " Private variables "
    Private mintDependantimgunkid As Integer
    Private mintCompanyunkid As Integer
    Private mintEmployeeunkid As Integer
    Private mintDependantunkid As Integer
    Private mbytPhoto As Byte()
    Private mintUserunkid As Integer
    Private mblnIsvoid As Boolean
    Private mintVoiduserunkid As Integer
    Private mdtVoiddatetime As Date
    Private mintLoginemployeeunkid As Integer
    Private mintVoidloginemployeeunkid As Integer
    Private mintAudittype As Integer
    Private mintAudituserunkid As Integer
    Private mdtAuditdatetime As Date
    Private mstrIp As String = String.Empty
    Private mstrMachine_Name As String = String.Empty
    Private mstrWebForm_Name As String = String.Empty
    Private mblnIsweb As Boolean
#End Region

#Region " Properties "
    'S.SANDEEP [28-May-2018] -- START 
    'ISSUE/ENHANCEMENT : {Audit Trails} 
    Private mstrFormName As String = String.Empty
    Public WriteOnly Property _FormName() As String
        Set(ByVal value As String)
            mstrFormName = value
        End Set
    End Property

    Private mstrClientIP As String = ""
    Public WriteOnly Property _ClientIP() As String
        Set(ByVal value As String)
            mstrClientIP = value
        End Set
    End Property

    Private mstrHostName As String = ""
    Public WriteOnly Property _HostName() As String
        Set(ByVal value As String)
            mstrHostName = value
        End Set
    End Property

    Private mintAuditUserId As Integer = 0
    Public WriteOnly Property _AuditUserId() As Integer
        Set(ByVal value As Integer)
            mintAuditUserId = value
        End Set
    End Property

    Private mdtAuditDate As DateTime = Now
    Public WriteOnly Property _AuditDate() As DateTime
        Set(ByVal value As DateTime)
            mdtAuditDate = value
        End Set
    End Property

    Private xDataOpr As clsDataOperation = Nothing
    Public Property _xDataOpr() As clsDataOperation
        Get
            Return xDataOpr
        End Get
        Set(ByVal value As clsDataOperation)
            xDataOpr = value
        End Set
    End Property
    'S.SANDEEP [28-May-2018] -- END 
    ''' <summary>
    ''' Purpose: Get Message from Class 
    ''' Modify By: Pinkal
    ''' </summary>
    Public ReadOnly Property _Message() As String
        Get
            Return mstrMessage
        End Get
    End Property

    ''' <summary>
    ''' Purpose: Get or Set dependantimgunkid
    ''' Modify By: Pinkal
    ''' </summary>
    Public Property _Dependantimgunkid() As Integer
        Get
            Return mintDependantimgunkid
        End Get
        Set(ByVal value As Integer)
            mintDependantimgunkid = Value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set companyunkid
    ''' Modify By: Pinkal
    ''' </summary>
    Public Property _Companyunkid() As Integer
        Get
            Return mintCompanyunkid
        End Get
        Set(ByVal value As Integer)
            mintCompanyunkid = Value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set employeeunkid
    ''' Modify By: Pinkal
    ''' </summary>
    Public Property _Employeeunkid() As Integer
        Get
            Return mintEmployeeunkid
        End Get
        Set(ByVal value As Integer)
            mintEmployeeunkid = Value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set dependantunkid
    ''' Modify By: Pinkal
    ''' </summary>
    Public Property _Dependantunkid() As Integer
        Get
            Return mintDependantunkid
        End Get
        Set(ByVal value As Integer)
            mintDependantunkid = Value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set photo
    ''' Modify By: Pinkal
    ''' </summary>
    Public Property _Photo() As Byte()
        Get
            Return mbytPhoto
        End Get
        Set(ByVal value As Byte())
            mbytPhoto = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set userunkid
    ''' Modify By: Pinkal
    ''' </summary>
    Public Property _Userunkid() As Integer
        Get
            Return mintUserunkid
        End Get
        Set(ByVal value As Integer)
            mintUserunkid = Value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set isvoid
    ''' Modify By: Pinkal
    ''' </summary>
    Public Property _Isvoid() As Boolean
        Get
            Return mblnIsvoid
        End Get
        Set(ByVal value As Boolean)
            mblnIsvoid = Value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set voiduserunkid
    ''' Modify By: Pinkal
    ''' </summary>
    Public Property _Voiduserunkid() As Integer
        Get
            Return mintVoiduserunkid
        End Get
        Set(ByVal value As Integer)
            mintVoiduserunkid = Value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set voiddatetime
    ''' Modify By: Pinkal
    ''' </summary>
    Public Property _Voiddatetime() As Date
        Get
            Return mdtVoiddatetime
        End Get
        Set(ByVal value As Date)
            mdtVoiddatetime = Value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set loginemployeeunkid
    ''' Modify By: Pinkal
    ''' </summary>
    Public Property _Loginemployeeunkid() As Integer
        Get
            Return mintLoginemployeeunkid
        End Get
        Set(ByVal value As Integer)
            mintLoginemployeeunkid = Value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set voidloginemployeeunkid
    ''' Modify By: Pinkal
    ''' </summary>
    Public Property _Voidloginemployeeunkid() As Integer
        Get
            Return mintVoidloginemployeeunkid
        End Get
        Set(ByVal value As Integer)
            mintVoidloginemployeeunkid = Value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set audittype
    ''' Modify By: Pinkal
    ''' </summary>
    Public Property _Audittype() As Integer
        Get
            Return mintAudittype
        End Get
        Set(ByVal value As Integer)
            mintAudittype = Value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set audituserunkid
    ''' Modify By: Pinkal
    ''' </summary>
    Public Property _Audituserunkid() As Integer
        Get
            Return mintAudituserunkid
        End Get
        Set(ByVal value As Integer)
            mintAudituserunkid = Value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set auditdatetime
    ''' Modify By: Pinkal
    ''' </summary>
    Public Property _Auditdatetime() As Date
        Get
            Return mdtAuditdatetime
        End Get
        Set(ByVal value As Date)
            mdtAuditdatetime = Value
        End Set
    End Property

    'Public Property _Ip() As String
    '    Get
    '        Return mstrIp
    '    End Get
    '    Set(ByVal value As String)
    '        mstrIp = value
    '    End Set
    'End Property

    'Public Property _Machine_Name() As String
    '    Get
    '        Return mstrMachine_Name
    '    End Get
    '    Set(ByVal value As String)
    '        mstrMachine_Name = value
    '    End Set
    'End Property

    'Public Property _WebForm_Name() As String
    '    Get
    '        Return mstrWebForm_Name
    '    End Get
    '    Set(ByVal value As String)
    '        mstrWebForm_Name = value
    '    End Set
    'End Property

    ''' <summary>
    ''' Purpose: Get or Set isweb
    ''' Modify By: Pinkal
    ''' </summary>
    Public Property _Isweb() As Boolean
        Get
            Return mblnIsweb
        End Get
        Set(ByVal value As Boolean)
            mblnIsweb = Value
        End Set
    End Property


#End Region

    ''' <summary>
    ''' Modify By: Pinkal
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>
    Public Sub GetData(ByVal intCompanyId As Integer, ByVal intEmployeeId As Integer, ByVal intDependantId As Integer, Optional ByVal blnOnlyActive As Boolean = True, Optional ByVal objDataOperation As clsDataOperation = Nothing, Optional ByVal mintdependantimgunkid As Integer = 0)
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        If objDataOperation Is Nothing Then
            objDataOperation = New clsDataOperation
        End If

        Try
            strQ = "SELECT " & _
                      "  dependantimgunkid " & _
                      ", companyunkid " & _
                      ", employeeunkid " & _
                      ", dependantunkid " & _
                      ", photo " & _
                      ", userunkid " & _
                      ", isvoid " & _
                      ", voiduserunkid " & _
                      ", voiddatetime " & _
                      ", loginemployeeunkid " & _
                      ", voidloginemployeeunkid " & _
                     "FROM arutiimages..hrdependant_images " & _
                     "WHERE companyunkid = @companyunkid AND employeeunkid = @employeeunkid   "

            If blnOnlyActive Then
                strQ &= " AND isvoid = 0 "
            End If
            objDataOperation.ClearParameters()
            objDataOperation.AddParameter("@companyunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intCompanyId)
            objDataOperation.AddParameter("@employeeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intEmployeeId)


            'Gajanan [22-Feb-2019] -- Start
            'Enhancement - Implementing Employee Approver Flow On Employee Data.

            If intDependantId > 0 Then
                strQ &= " AND dependantunkid = @dependantunkid"
            objDataOperation.AddParameter("@dependantunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intDependantId)
            End If


            If mintdependantimgunkid > 0 Then
                strQ &= " AND dependantimgunkid = @dependantimgunkid"
                objDataOperation.AddParameter("@dependantimgunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintdependantimgunkid)
            End If

            'Gajanan [22-Feb-2019] -- End



            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            For Each dtRow As DataRow In dsList.Tables(0).Rows
                mintDependantimgunkid = CInt(dtRow.Item("dependantimgunkid"))
                mintCompanyunkid = CInt(dtRow.Item("companyunkid"))
                mintEmployeeunkid = CInt(dtRow.Item("employeeunkid"))
                mintDependantunkid = CInt(dtRow.Item("dependantunkid"))
                mbytPhoto = dtRow.Item("photo")
                mintUserunkid = CInt(dtRow.Item("userunkid"))
                mblnIsvoid = CBool(dtRow.Item("isvoid"))
                mintVoiduserunkid = CInt(dtRow.Item("voiduserunkid"))

                If Not IsDBNull(dtRow.Item("voiddatetime")) Then
                    mdtVoiddatetime = dtRow.Item("voiddatetime")
                End If

                mintLoginemployeeunkid = CInt(dtRow.Item("loginemployeeunkid"))
                mintVoidloginemployeeunkid = CInt(dtRow.Item("voidloginemployeeunkid"))
                Exit For
            Next
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: GetData; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
        End Try
    End Sub

    ''' <summary>
    ''' Modify By: Pinkal
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>
    Public Function GetList(ByVal strTableName As String, Optional ByVal blnOnlyActive As Boolean = True) As DataSet
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        objDataOperation = New clsDataOperation

        Try
            strQ = "SELECT " & _
                      "  dependantimgunkid " & _
                      ", companyunkid " & _
                      ", employeeunkid " & _
                      ", dependantunkid " & _
                      ", photo " & _
                      ", userunkid " & _
                      ", isvoid " & _
                      ", voiduserunkid " & _
                      ", voiddatetime " & _
                      ", loginemployeeunkid " & _
                      ", voidloginemployeeunkid " & _
                     "FROM arutiimages..hrdependant_images "

            If blnOnlyActive Then
                strQ &= " WHERE isactive = 1 "
            End If

            dsList = objDataOperation.ExecQuery(strQ, strTableName)

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: GetList; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
        Return dsList
    End Function

    ''' <summary>
    ''' Modify By: Pinkal
    ''' </summary>
    ''' <returns>Boolean</returns>
    ''' <purpose> INSERT INTO Database Table (hrdependant_images) </purpose>
    Public Function Insert(ByVal objDataOperation As clsDataOperation, Optional ByVal Isfromapprover As Boolean = False) As Boolean
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        Try

            If isExist(objDataOperation, mintCompanyunkid, mintEmployeeunkid, mintDependantunkid) = True Then

                'Gajanan [22-Feb-2019] -- Start
                'Enhancement - Implementing Employee Approver Flow On Employee Data.

                'If Update(objDataOperation) = False Then
                '    exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                '    Throw exForce
                'End If
                'Return True
                If Isfromapprover = False Then
                If Update(objDataOperation) = False Then
                    exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                    Throw exForce
                End If
                Return True
            End If
                'Gajanan [22-Feb-2019] -- End

            End If

            objDataOperation.ClearParameters()
            objDataOperation.AddParameter("@companyunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintCompanyunkid.ToString)
            objDataOperation.AddParameter("@employeeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintEmployeeunkid.ToString)
            objDataOperation.AddParameter("@dependantunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintDependantunkid.ToString)
            objDataOperation.AddParameter("@photo", SqlDbType.Image, mbytPhoto.Length, mbytPhoto)
            objDataOperation.AddParameter("@userunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintUserunkid.ToString)
            objDataOperation.AddParameter("@isvoid", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsvoid.ToString)
            objDataOperation.AddParameter("@voiduserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintVoiduserunkid.ToString)
            objDataOperation.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, IIf(mdtVoiddatetime <> Nothing, mdtVoiddatetime, DBNull.Value))
            objDataOperation.AddParameter("@loginemployeeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintLoginemployeeunkid.ToString)
            objDataOperation.AddParameter("@voidloginemployeeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintVoidloginemployeeunkid.ToString)

            strQ = "INSERT INTO arutiimages..hrdependant_images ( " & _
                      "  companyunkid " & _
                      ", employeeunkid " & _
                      ", dependantunkid " & _
                      ", photo " & _
                      ", userunkid " & _
                      ", isvoid " & _
                      ", voiduserunkid " & _
                      ", voiddatetime " & _
                      ", loginemployeeunkid " & _
                      ", voidloginemployeeunkid" & _
                    ") VALUES (" & _
                      "  @companyunkid " & _
                      ", @employeeunkid " & _
                      ", @dependantunkid " & _
                      ", @photo " & _
                      ", @userunkid " & _
                      ", @isvoid " & _
                      ", @voiduserunkid " & _
                      ", @voiddatetime " & _
                      ", @loginemployeeunkid " & _
                      ", @voidloginemployeeunkid" & _
                    "); SELECT @@identity"

            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            mintDependantimgunkid = dsList.Tables(0).Rows(0).Item(0)

            If InsertAtDependant_Images(objDataOperation, 1) = False Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            Return True
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: Insert; Module Name: " & mstrModuleName)
            Return False
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
        End Try
    End Function

    ''' <summary>
    ''' Modify By: Pinkal
    ''' </summary>
    ''' <returns>Boolean</returns>
    ''' <purpose> Update Database Table (hrdependant_images) </purpose>
    Public Function Update(ByVal objDataOperation As clsDataOperation) As Boolean
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        Try
            objDataOperation.ClearParameters()
            objDataOperation.AddParameter("@dependantimgunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintDependantimgunkid.ToString)
            objDataOperation.AddParameter("@companyunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintCompanyunkid.ToString)
            objDataOperation.AddParameter("@employeeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintEmployeeunkid.ToString)
            objDataOperation.AddParameter("@dependantunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintDependantunkid.ToString)
            objDataOperation.AddParameter("@photo", SqlDbType.Image, mbytPhoto.Length, mbytPhoto)
            objDataOperation.AddParameter("@userunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintUserunkid.ToString)
            objDataOperation.AddParameter("@isvoid", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsvoid.ToString)
            objDataOperation.AddParameter("@voiduserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintVoiduserunkid.ToString)
            objDataOperation.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, IIf(mdtVoiddatetime <> Nothing, mdtVoiddatetime, DBNull.Value))
            objDataOperation.AddParameter("@loginemployeeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintLoginemployeeunkid.ToString)
            objDataOperation.AddParameter("@voidloginemployeeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintVoidloginemployeeunkid.ToString)

            strQ = "UPDATE arutiimages..hrdependant_images SET " & _
                      "  companyunkid = @companyunkid" & _
                      ", employeeunkid = @employeeunkid" & _
                      ", dependantunkid = @dependantunkid" & _
                      ", photo = @photo" & _
                      ", userunkid = @userunkid" & _
                      ", isvoid = @isvoid" & _
                      ", voiduserunkid = @voiduserunkid" & _
                      ", voiddatetime = @voiddatetime" & _
                      ", loginemployeeunkid = @loginemployeeunkid" & _
                      ", voidloginemployeeunkid = @voidloginemployeeunkid " & _
                    "WHERE dependantimgunkid = @dependantimgunkid "

            Call objDataOperation.ExecNonQuery(strQ)

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            If InsertAtDependant_Images(objDataOperation, 2) = False Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            Return True
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: Update; Module Name: " & mstrModuleName)
            Return False
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
        End Try
    End Function

    ''' <summary>
    ''' Modify By: Pinkal
    ''' </summary>
    ''' <returns>Boolean</returns>
    ''' <purpose> Delete Database Table (hrdependant_images) </purpose>
    Public Function Delete(ByVal objDataOperation As clsDataOperation, ByVal intUnkid As Integer) As Boolean
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        Try
            objDataOperation.ClearParameters()

            strQ = "UPDATE arutiimages..hrdependant_images  set " & _
                      " isvoid = 1 " & _
                       ", voiddatetime = @voiddatetime "

            If mintLoginemployeeunkid > 0 Then
                strQ &= ", voidloginemployeeunkid = @voidloginemployeeunkid "
                objDataOperation.AddParameter("@voidloginemployeeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintVoidloginemployeeunkid)

            ElseIf mintUserunkid > 0 Then
                strQ &= ", voiduserunkid  = @voiduserunkid "
                objDataOperation.AddParameter("@voiduserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintUserunkid)

            End If

            strQ &= " WHERE dependantimgunkid = @dependantimgunkid"

            objDataOperation.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, ConfigParameter._Object._CurrentDateAndTime)
            objDataOperation.AddParameter("@dependantimgunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intUnkid)

            Call objDataOperation.ExecNonQuery(strQ)

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            mintDependantimgunkid = intUnkid
            If InsertAtDependant_Images(objDataOperation, 3) = False Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            Return True
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: Delete; Module Name: " & mstrModuleName)
            Return False
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
        End Try
    End Function

    ''' <summary>
    ''' Modify By: Pinkal
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>
    Public Function isUsed(ByVal intUnkid As Integer) As Boolean
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        objDataOperation = New clsDataOperation

        Try
            StrQ = "<Query>"

            objDataOperation.AddParameter("@dependantimgunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intUnkid)

            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            Return dsList.tables(0).rows.count > 0
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: isUsed; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
    End Function

    ''' <summary>
    ''' Modify By: Pinkal
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>
    Public Function isExist(ByVal objdataoperation As clsDataOperation, ByVal intCompanyId As Integer, ByVal intEmployeeId As Integer, ByVal intDependantId As Integer, Optional ByVal intUnkid As Integer = -1) As Boolean
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        Try
            strQ = "SELECT " & _
                      "  dependantimgunkid " & _
                      ", companyunkid " & _
                      ", employeeunkid " & _
                      ", dependantunkid " & _
                      ", photo " & _
                      ", userunkid " & _
                      ", isvoid " & _
                      ", voiduserunkid " & _
                      ", voiddatetime " & _
                      ", loginemployeeunkid " & _
                      ", voidloginemployeeunkid " & _
                     " FROM arutiimages..hrdependant_images " & _
                     " WHERE companyunkid = @companyunkid " & _
                     " AND employeeunkid = @employeeunkid " & _
                     " AND dependantunkid = @dependantunkid AND isvoid = 0"

            If intUnkid > 0 Then
                strQ &= " AND dependantimgunkid <> @dependantimgunkid"
            End If

            objdataoperation.AddParameter("@companyunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intCompanyId)
            objdataoperation.AddParameter("@employeeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intEmployeeId)
            objdataoperation.AddParameter("@dependantunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intDependantId)
            objdataoperation.AddParameter("@dependantimgunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intUnkid)

            dsList = objdataoperation.ExecQuery(strQ, "List")

            If objdataoperation.ErrorMessage <> "" Then
                exForce = New Exception(objdataoperation.ErrorNumber & ": " & objdataoperation.ErrorMessage)
                Throw exForce
            End If

            'S.SANDEEP [ 28 MAR 2013 ] -- START
            'ENHANCEMENT : TRA CHANGES
            If dsList.Tables(0).Rows.Count > 0 Then
                mintDependantimgunkid = dsList.Tables(0).Rows(0)("dependantimgunkid")
            End If
            'S.SANDEEP [ 28 MAR 2013 ] -- END

            Return dsList.Tables(0).Rows.Count > 0
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: isExist; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
        End Try
    End Function

    ''' <summary>
    ''' Modify By: Pinkal
    ''' </summary>
    ''' <returns>Boolean</returns>
    ''' <purpose> INSERT INTO Database Table (athrdependant_images) </purpose>
    Public Function InsertAtDependant_Images(ByVal objDataOperation As clsDataOperation, ByVal intAuditType As Integer) As Boolean
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        Try
            objDataOperation.ClearParameters()
            objDataOperation.AddParameter("@dependantimgunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintDependantimgunkid.ToString)
            objDataOperation.AddParameter("@companyunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintCompanyunkid.ToString)
            objDataOperation.AddParameter("@employeeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintEmployeeunkid.ToString)
            objDataOperation.AddParameter("@dependantunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintDependantunkid.ToString)
            objDataOperation.AddParameter("@audittype", SqlDbType.Int, eZeeDataType.INT_SIZE, intAuditType.ToString)
            objDataOperation.AddParameter("@audituserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintAuditUserId)
            objDataOperation.AddParameter("@auditdatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, mdtAuditDate)
            objDataOperation.AddParameter("@ip", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrClientIP)
            objDataOperation.AddParameter("@machine_name", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrHostName)
            objDataOperation.AddParameter("@form_name", SqlDbType.NVarChar, 500, mstrFormName)
            objDataOperation.AddParameter("@isweb", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsweb)
            objDataOperation.AddParameter("@loginemployeeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintLoginemployeeunkid.ToString)

            strQ = "INSERT INTO arutiimages..athrdependant_images ( " & _
                      "  dependantimgunkid " & _
                      ", companyunkid " & _
                      ", employeeunkid " & _
                      ", dependantunkid " & _
                      ", audittype " & _
                      ", audituserunkid " & _
                      ", auditdatetime " & _
                      ", ip " & _
                      ", machine_name " & _
                      ", form_name " & _
                      " " & _
                      " " & _
                      " " & _
                      " " & _
                      " " & _
                      ", isweb " & _
                      ", loginemployeeunkid" & _
                    ") VALUES (" & _
                      "  @dependantimgunkid " & _
                      ", @companyunkid " & _
                      ", @employeeunkid " & _
                      ", @dependantunkid " & _
                      ", @audittype " & _
                      ", @audituserunkid " & _
                      ", @auditdatetime " & _
                      ", @ip " & _
                      ", @machine_name " & _
                      ", @form_name " & _
                      " " & _
                      " " & _
                      " " & _
                      " " & _
                      " " & _
                      ", @isweb " & _
                      ", @loginemployeeunkid" & _
                    "); SELECT @@identity"

            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If


            Return True
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: InsertAtDependant_Images; Module Name: " & mstrModuleName)
            Return False
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
        End Try
    End Function


    'S.SANDEEP [ 28 MAR 2013 ] -- START
    'ENHANCEMENT : TRA CHANGES
    Public Function Import_Image() As Boolean
        Try
            objDataOperation = New clsDataOperation
            objDataOperation.BindTransaction()

            If Insert(objDataOperation) = False Then
                objDataOperation.ReleaseTransaction(False)
                If objDataOperation.ErrorMessage <> "" Then
                    Throw New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                End If
            End If

            objDataOperation.ReleaseTransaction(True)

            Return True
        Catch ex As Exception
            objDataOperation.ReleaseTransaction(False)
            DisplayError.Show("-1", ex.Message, "Import_Image", mstrModuleName)
        End Try
    End Function

    Public Function Import_IsExists() As Boolean
        Try
            Dim blnFlag As Boolean = False
            objDataOperation = New clsDataOperation
            blnFlag = isExist(objDataOperation, mintCompanyunkid, mintEmployeeunkid, mintDependantunkid)
            If objDataOperation.ErrorMessage <> "" Then
                Throw New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
            End If
            Return blnFlag
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "Import_IsExists", mstrModuleName)
        End Try
    End Function
    'S.SANDEEP [ 28 MAR 2013 ] -- END


	'<Language> This Auto Generated Text Please Do Not Modify it.
#Region " Language & UI Settings "
	Public Shared Sub SetMessages()
		Try
			Language.setMessage(mstrModuleName, 1, "WEB")

		Catch Ex As Exception
			DisplayError.Show("-1", ex.Message, "SetMessages", mstrModuleName)
		End Try
	End Sub
#End Region 'Language & UI Settings
	'</Language>
End Class