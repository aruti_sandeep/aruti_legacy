﻿'************************************************************************************************************************************
'Class Name : clsGarnishees_Bank_Tran.vb
'Purpose    :
'Date       : 30/06/2023
'Written By : Hemant Morker
'Modified   :
'************************************************************************************************************************************
Imports eZeeCommonLib
''' <summary>
''' Purpose: 
''' Developer: Hemant Morker
''' </summary>
Public Class clsGarnishees_Bank_Tran

#Region " Private Variables "
    Private Const mstrModuleName As String = "clsGarnishees_Bank_Tran"
    Dim objDataOperation As clsDataOperation
    Dim mstrMessage As String = ""
    Private mintGarnisheeBankTranUnkid As Integer
    Private mintDependantTranUnkid As Integer = -1
    Private mintBranchunkid As Integer
    Private mintAccounttypeunkid As Integer
    Private mstrAccountno As String = String.Empty
    Private mdblPercentage As Double
    Private mintModeid As Integer = 2 'Percentage
    Private mdecAmount As Decimal = 0
    Private mintPriority As Integer = 1
    Private mintPeriodunkid As Integer = 0
    Private mintUserunkid As Integer
    Private mblnIsvoid As Boolean
    Private mintVoiduserunkid As Integer
    Private mintLogEmployeeUnkid As Integer
    Private mdtVoiddatetime As Date
    Private mstrVoidreason As String = String.Empty
    Private mdtAuditDate As DateTime = Now
    Private mstrFormName As String = String.Empty
    Private mblnIsWeb As Boolean = False

    Private mdtTran As DataTable
#End Region

#Region " Properties "

    Public Property _GarnisheeBankTranUnkid(ByVal xDataOp As clsDataOperation) As Integer
        Get
            Return mintGarnisheeBankTranUnkid
        End Get
        Set(ByVal value As Integer)
            mintGarnisheeBankTranUnkid = value
            Call GetData(xDataOp)
        End Set
    End Property

    Public Property _DependantTranUnkid() As Integer
        Get
            Return mintDependantTranUnkid
        End Get
        Set(ByVal value As Integer)
            mintDependantTranUnkid = value
            Call Get_Bank_Tran()
        End Set
    End Property

    Public Property _Branchunkid() As Integer
        Get
            Return mintBranchunkid
        End Get
        Set(ByVal value As Integer)
            mintBranchunkid = value
        End Set
    End Property

    Public Property _Accounttypeunkid() As Integer
        Get
            Return mintAccounttypeunkid
        End Get
        Set(ByVal value As Integer)
            mintAccounttypeunkid = value
        End Set
    End Property

    Public Property _Accountno() As String
        Get
            Return mstrAccountno
        End Get
        Set(ByVal value As String)
            mstrAccountno = value
        End Set
    End Property

    Public Property _Percentage() As Double
        Get
            Return mdblPercentage
        End Get
        Set(ByVal value As Double)
            mdblPercentage = value
        End Set
    End Property

    Public Property _Modeid() As Integer
        Get
            Return mintModeid
        End Get
        Set(ByVal value As Integer)
            mintModeid = value
        End Set
    End Property

    Public Property _Amount() As Decimal
        Get
            Return mdecAmount
        End Get
        Set(ByVal value As Decimal)
            mdecAmount = value
        End Set
    End Property

    Public Property _Priority() As Integer
        Get
            Return mintPriority
        End Get
        Set(ByVal value As Integer)
            mintPriority = value
        End Set
    End Property

    Public Property _Periodunkid() As Integer
        Get
            Return mintPeriodunkid
        End Get
        Set(ByVal value As Integer)
            mintPeriodunkid = value
        End Set
    End Property

    Public Property _Userunkid() As Integer
        Get
            Return mintUserunkid
        End Get
        Set(ByVal value As Integer)
            mintUserunkid = value
        End Set
    End Property

    Public Property _Isvoid() As Boolean
        Get
            Return mblnIsvoid
        End Get
        Set(ByVal value As Boolean)
            mblnIsvoid = Value
        End Set
    End Property

    Public Property _Voiduserunkid() As Integer
        Get
            Return mintVoiduserunkid
        End Get
        Set(ByVal value As Integer)
            mintVoiduserunkid = value
        End Set
    End Property

    Public Property _Loginemployeeunkid() As Integer
        Get
            Return mintLogEmployeeUnkid
        End Get
        Set(ByVal value As Integer)
            mintLogEmployeeUnkid = value
        End Set
    End Property

    Public Property _Voiddatetime() As Date
        Get
            Return mdtVoiddatetime
        End Get
        Set(ByVal value As Date)
            mdtVoiddatetime = value
        End Set
    End Property

    Public Property _Voidreason() As String
        Get
            Return mstrVoidreason
        End Get
        Set(ByVal value As String)
            mstrVoidreason = value
        End Set
    End Property

    Public WriteOnly Property _AuditDate() As DateTime
        Set(ByVal value As DateTime)
            mdtAuditDate = value
        End Set
    End Property

    Public WriteOnly Property _FormName() As String
        Set(ByVal value As String)
            mstrFormName = value
        End Set
    End Property

    Public Property _Isweb() As Boolean
        Get
            Return mblnIsWeb
        End Get
        Set(ByVal value As Boolean)
            mblnIsWeb = value
        End Set
    End Property

    Private mstrHostName As String = ""
    Public WriteOnly Property _HostName() As String
        Set(ByVal value As String)
            mstrHostName = value
        End Set
    End Property

    Private mstrClientIP As String = ""
    Public WriteOnly Property _ClientIP() As String
        Set(ByVal value As String)
            mstrClientIP = value
        End Set
    End Property

    Private mintAuditUserId As Integer = 0
    Public WriteOnly Property _AuditUserId() As Integer
        Set(ByVal value As Integer)
            mintAuditUserId = value
        End Set
    End Property

    Public Property _DataTable() As DataTable
        Get
            Return mdtTran
        End Get
        Set(ByVal value As DataTable)
            mdtTran = value
        End Set
    End Property

#End Region

#Region " Constuctor "
    Public Sub New()
        mdtTran = New DataTable("EmpBanks")
        Dim dCol As DataColumn
        Try

            dCol = New DataColumn("UnkId")
            dCol.DataType = System.Type.GetType("System.Int32")
            dCol.AutoIncrementSeed = 1
            dCol.AutoIncrement = True
            mdtTran.Columns.Add(dCol)

            dCol = New DataColumn("garnisheebanktranunkid")
            dCol.DataType = System.Type.GetType("System.Int32")
            mdtTran.Columns.Add(dCol)

            dCol = New DataColumn("dpndtbeneficetranunkid")
            dCol.DataType = System.Type.GetType("System.Int32")
            mdtTran.Columns.Add(dCol)

            dCol = New DataColumn("garnisheename")
            dCol.DataType = System.Type.GetType("System.String")
            mdtTran.Columns.Add(dCol)

            dCol = New DataColumn("bankgrpunkid")
            dCol.DataType = System.Type.GetType("System.Int32")
            dCol.DefaultValue = 0
            mdtTran.Columns.Add(dCol)

            dCol = New DataColumn("BankGrp")
            dCol.DataType = System.Type.GetType("System.String")
            mdtTran.Columns.Add(dCol)

            dCol = New DataColumn("branchunkid")
            dCol.DataType = System.Type.GetType("System.Int32")
            mdtTran.Columns.Add(dCol)

            dCol = New DataColumn("branchname")
            dCol.DataType = System.Type.GetType("System.String")
            mdtTran.Columns.Add(dCol)

            dCol = New DataColumn("accounttypeunkid")
            dCol.DataType = System.Type.GetType("System.Int32")
            mdtTran.Columns.Add(dCol)

            dCol = New DataColumn("acctypename")
            dCol.DataType = System.Type.GetType("System.String")
            mdtTran.Columns.Add(dCol)

            dCol = New DataColumn("accountno")
            dCol.DataType = System.Type.GetType("System.String")
            mdtTran.Columns.Add(dCol)

            dCol = New DataColumn("percentage")
            dCol.DataType = System.Type.GetType("System.Decimal")
            mdtTran.Columns.Add(dCol)

            dCol = New DataColumn("modeid")
            dCol.DataType = System.Type.GetType("System.Int32")
            mdtTran.Columns.Add(dCol)

            dCol = New DataColumn("amount")
            dCol.DataType = System.Type.GetType("System.Decimal")
            mdtTran.Columns.Add(dCol)

            dCol = New DataColumn("priority")
            dCol.DataType = System.Type.GetType("System.Int32")
            mdtTran.Columns.Add(dCol)

            dCol = New DataColumn("periodunkid")
            dCol.DataType = System.Type.GetType("System.Int32")
            mdtTran.Columns.Add(dCol)

            dCol = New DataColumn("period_name")
            dCol.DataType = System.Type.GetType("System.String")
            mdtTran.Columns.Add(dCol)

            dCol = New DataColumn("end_date")
            dCol.DataType = System.Type.GetType("System.String")
            mdtTran.Columns.Add(dCol)

            dCol = New DataColumn("AUD")
            dCol.DataType = System.Type.GetType("System.String")
            mdtTran.Columns.Add(dCol)

            dCol = New DataColumn("GUID")
            dCol.DataType = System.Type.GetType("System.String")
            mdtTran.Columns.Add(dCol)

            mdtTran.Columns.Add("isvoid", System.Type.GetType("System.Boolean")).DefaultValue = False
            mdtTran.Columns.Add("voiduserunkid", System.Type.GetType("System.Int32")).DefaultValue = -1
            mdtTran.Columns.Add("voiddatetime", System.Type.GetType("System.DateTime")).DefaultValue = Nothing
            mdtTran.Columns.Add("voidreason", System.Type.GetType("System.String")).DefaultValue = ""

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "New", mstrModuleName)
        End Try
    End Sub
#End Region

#Region " Private Functions "

    Public Sub GetData(ByVal xDataOp As clsDataOperation)
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        If xDataOp IsNot Nothing Then
            objDataOperation = xDataOp
        Else
            objDataOperation = New clsDataOperation
        End If
        objDataOperation.ClearParameters()

        Try

            strQ = "SELECT " & _
              "  garnisheebanktranunkid " & _
              ", dpndtbeneficetranunkid " & _
              ", branchunkid " & _
              ", accounttypeunkid " & _
              ", accountno " & _
              ", userunkid " & _
              ", isvoid " & _
              ", voiduserunkid " & _
              ", voiddatetime " & _
              ", voidreason " & _
              ", percentage " & _
              ", ISNULL(modeid, 2) AS modeid " & _
              ", ISNULL(amount, 0) AS amount " & _
              ", ISNULL(priority, 1) AS priority " & _
              ", ISNULL(periodunkid, 1) AS periodunkid " & _
             "FROM hrgarnishee_bank_tran " & _
             "WHERE garnisheebanktranunkid = @garnisheebanktranunkid "

            objDataOperation.AddParameter("@garnisheebanktranunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintGarnisheeBankTranUnkid.ToString)

            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            For Each dtRow As DataRow In dsList.Tables(0).Rows
                mintGarnisheeBankTranUnkid = CInt(dtRow.Item("garnisheebanktranunkid"))
                mintDependantTranUnkid = CInt(dtRow.Item("dpndtbeneficetranunkid"))
                mintBranchunkid = CInt(dtRow.Item("branchunkid"))
                mintAccounttypeunkid = CInt(dtRow.Item("accounttypeunkid"))
                mstrAccountno = dtRow.Item("accountno").ToString
                mintUserunkid = CInt(dtRow.Item("userunkid"))
                mblnIsvoid = CBool(dtRow.Item("isvoid"))
                mintVoiduserunkid = CInt(dtRow.Item("voiduserunkid"))
                If IsDBNull(dtRow.Item("voiddatetime")) = True Then
                    mdtVoiddatetime = Nothing
                Else
                    mdtVoiddatetime = dtRow.Item("voiddatetime")
                End If
                mstrVoidreason = dtRow.Item("voidreason").ToString
                mdblPercentage = CDec(dtRow.Item("percentage"))
                mdecAmount = CDec(dtRow.Item("amount"))
                mintModeid = CInt(dtRow.Item("modeid"))
                mintPriority = CInt(dtRow.Item("priority"))
                mintPeriodunkid = CInt(dtRow.Item("periodunkid"))
                Exit For
            Next
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: GetData; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            If xDataOp Is Nothing Then objDataOperation = Nothing
        End Try
    End Sub

    Public Function GetList(ByVal xPeriodEnd As DateTime, Optional ByVal dtPeriodEndDate As DateTime = Nothing) As DataSet
        Dim strQ As String = ""
        Dim strErrorMessage As String = ""
        Dim dsList As New DataSet
        Dim dRowB_Tran As DataRow
        Dim exForce As Exception
        Try
            objDataOperation = New clsDataOperation


            strQ = "SELECT  garnisheebanktranunkid " & _
                         ", dpndtbeneficetranunkid " & _
                         ", branchunkid " & _
                         ", accounttypeunkid " & _
                         ", BankGrpUnkid " & _
                         ", accountno " & _
                         ", BranchName " & _
                         ", BankGrp " & _
                         ", GarnisheeName " & _
                         ", AccName " & _
                         ", percentage " & _
                         ", modeid " & _
                         ", amount " & _
                         ", priority " & _
                         ", periodunkid " & _
                         ", period_name " & _
                         ", start_date " & _
                         ", end_date " & _
                         ", '' As AUD " & _
                         ", '' As GUID " & _
                   "FROM    (  SELECT " & _
                       " hrgarnishee_bank_tran.garnisheebanktranunkid " & _
                       ",hrgarnishee_bank_tran.dpndtbeneficetranunkid " & _
                       ",hrgarnishee_bank_tran.branchunkid " & _
                       ",hrgarnishee_bank_tran.accounttypeunkid " & _
                       ",hrmsConfiguration..cfpayrollgroup_master.groupmasterunkid AS BankGrpUnkid " & _
                       ",hrgarnishee_bank_tran.accountno " & _
                       ",hrmsConfiguration..cfbankbranch_master.branchname AS BranchName " & _
                       ",hrmsConfiguration..cfpayrollgroup_master.groupname AS BankGrp " & _
                       ",ISNULL(hrdependants_beneficiaries_tran.first_name,'')+' '+ISNULL(hrdependants_beneficiaries_tran.middle_name,'')+' '+ISNULL(hrdependants_beneficiaries_tran.last_name,'') AS GarnisheeName " & _
                       ",hrmsConfiguration..cfbankacctype_master.accounttype_name AS AccName " & _
                       ",hrgarnishee_bank_tran.percentage " & _
                       ",ISNULL(hrgarnishee_bank_tran.modeid, 2) AS modeid " & _
                       ",ISNULL(hrgarnishee_bank_tran.amount, 0) AS amount " & _
                       ",ISNULL(hrgarnishee_bank_tran.priority, 1) AS priority " & _
                       ", ISNULL(hrgarnishee_bank_tran.periodunkid, 0) AS periodunkid " & _
                       ", cfcommon_period_tran.period_name " & _
                       ", CONVERT(CHAR(8), cfcommon_period_tran.start_date,112) AS start_date " & _
                       ", CONVERT(CHAR(8), cfcommon_period_tran.end_date,112) AS end_date " & _
                       ", DENSE_RANK() OVER ( PARTITION  BY hrgarnishee_bank_tran.dpndtbeneficetranunkid ORDER BY cfcommon_period_tran.end_date DESC ) AS ROWNO " & _
                   "FROM hrgarnishee_bank_tran " & _
                       "LEFT JOIN hrmsConfiguration..cfbankbranch_master ON hrgarnishee_bank_tran.branchunkid = hrmsConfiguration..cfbankbranch_master.branchunkid " & _
                       "LEFT JOIN hrmsConfiguration..cfpayrollgroup_master ON hrmsConfiguration..cfbankbranch_master.bankgroupunkid = hrmsConfiguration..cfpayrollgroup_master.groupmasterunkid AND grouptype_id =   " & enPayrollGroupType.Bank & _
                       "LEFT JOIN hrdependants_beneficiaries_tran ON hrgarnishee_bank_tran.dpndtbeneficetranunkid = hrdependants_beneficiaries_tran.dpndtbeneficetranunkid " & _
                       "LEFT JOIN hrmsConfiguration..cfbankacctype_master ON hrgarnishee_bank_tran.accounttypeunkid = hrmsConfiguration..cfbankacctype_master.accounttypeunkid " & _
                       "LEFT JOIN dbo.cfcommon_period_tran ON hrgarnishee_bank_tran.periodunkid = cfcommon_period_tran.periodunkid "

            strQ &= "WHERE ISNULL(hrgarnishee_bank_tran.isvoid,0) = 0 " & _
                    "AND cfcommon_period_tran.modulerefid = " & enModuleReference.Payroll & " "

            If dtPeriodEndDate <> Nothing Then
                strQ &= "AND CONVERT(CHAR(8), cfcommon_period_tran.end_date, 112) <= @end_date "
                objDataOperation.AddParameter("@end_date", SqlDbType.VarChar, eZeeDataType.NAME_SIZE, eZeeDate.convertDate(xPeriodEnd))
            End If

            strQ &= " ) AS A "

            strQ &= "WHERE   1 = 1 "

            If dtPeriodEndDate <> Nothing Then
                strQ &= " AND   ROWNO = 1 "
            End If



            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If
            Return dsList
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "Get_Bank_Tran", mstrModuleName)
        End Try
    End Function

    Private Sub Get_Bank_Tran()
        Dim strQ As String = ""
        Dim strErrorMessage As String = ""
        Dim dsList As New DataSet
        Dim dRowB_Tran As DataRow
        Dim exForce As Exception
        Try
            objDataOperation = New clsDataOperation


            strQ = "SELECT " & _
                       " hrgarnishee_bank_tran.garnisheebanktranunkid " & _
                       ",hrgarnishee_bank_tran.dpndtbeneficetranunkid " & _
                       ",hrgarnishee_bank_tran.branchunkid " & _
                       ",hrgarnishee_bank_tran.accounttypeunkid " & _
                       ",hrmsConfiguration..cfpayrollgroup_master.groupmasterunkid AS BankGrpUnkid " & _
                       ",hrgarnishee_bank_tran.accountno " & _
                       ",hrmsConfiguration..cfbankbranch_master.branchname AS BranchName " & _
                       ",hrmsConfiguration..cfpayrollgroup_master.groupname AS BankGrp " & _
                       ",ISNULL(hrdependants_beneficiaries_tran.first_name,'')+' '+ISNULL(hrdependants_beneficiaries_tran.middle_name,'')+' '+ISNULL(hrdependants_beneficiaries_tran.last_name,'') AS GarnisheeName " & _
                       ",hrmsConfiguration..cfbankacctype_master.accounttype_name AS AccName " & _
                       ",hrgarnishee_bank_tran.percentage " & _
                       ",ISNULL(hrgarnishee_bank_tran.modeid, 2) AS modeid " & _
                       ",ISNULL(hrgarnishee_bank_tran.amount, 0) AS amount " & _
                       ",ISNULL(hrgarnishee_bank_tran.priority, 1) AS priority " & _
                       ", ISNULL(hrgarnishee_bank_tran.periodunkid, 0) AS periodunkid " & _
                       ", cfcommon_period_tran.period_name " & _
                       ", CONVERT(CHAR(8), cfcommon_period_tran.start_date,112) AS start_date " & _
                       ", CONVERT(CHAR(8), cfcommon_period_tran.end_date,112) AS end_date " & _
                       ",'' As AUD " & _
                   "FROM hrgarnishee_bank_tran " & _
                       "LEFT JOIN hrmsConfiguration..cfbankbranch_master ON hrgarnishee_bank_tran.branchunkid = hrmsConfiguration..cfbankbranch_master.branchunkid " & _
                       "LEFT JOIN hrmsConfiguration..cfpayrollgroup_master ON hrmsConfiguration..cfbankbranch_master.bankgroupunkid = hrmsConfiguration..cfpayrollgroup_master.groupmasterunkid AND grouptype_id =   " & enPayrollGroupType.Bank & _
                       "LEFT JOIN hrdependants_beneficiaries_tran ON hrgarnishee_bank_tran.dpndtbeneficetranunkid = hrdependants_beneficiaries_tran.dpndtbeneficetranunkid " & _
                       "LEFT JOIN hrmsConfiguration..cfbankacctype_master ON hrgarnishee_bank_tran.accounttypeunkid = hrmsConfiguration..cfbankacctype_master.accounttypeunkid " & _
                       "LEFT JOIN dbo.cfcommon_period_tran ON hrgarnishee_bank_tran.periodunkid = cfcommon_period_tran.periodunkid "

            strQ &= "WHERE ISNULL(hrgarnishee_bank_tran.isvoid,0) = 0 " & _
                    "AND cfcommon_period_tran.modulerefid = " & enModuleReference.Payroll & " " & _
                    "AND hrgarnishee_bank_tran.dpndtbeneficetranunkid = @dpndtbeneficetranunkid "

            objDataOperation.AddParameter("@dpndtbeneficetranunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintDependantTranUnkid.ToString)

            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            mdtTran.Clear()

            For i As Integer = 0 To dsList.Tables(0).Rows.Count - 1
                With dsList.Tables("List").Rows(i)
                    dRowB_Tran = mdtTran.NewRow()

                    dRowB_Tran.Item("garnisheebanktranunkid") = .Item("garnisheebanktranunkid")
                    dRowB_Tran.Item("dpndtbeneficetranunkid") = .Item("dpndtbeneficetranunkid")
                    dRowB_Tran.Item("garnisheename") = .Item("garnisheename")
                    dRowB_Tran.Item("BankGrpUnkid") = .Item("bankgrpunkid")
                    dRowB_Tran.Item("BankGrp") = .Item("bankgrp")
                    dRowB_Tran.Item("branchunkid") = .Item("branchunkid")
                    dRowB_Tran.Item("branchname") = .Item("branchname")
                    dRowB_Tran.Item("accounttypeunkid") = .Item("accounttypeunkid")
                    dRowB_Tran.Item("acctypename") = .Item("accname")
                    dRowB_Tran.Item("accountno") = .Item("accountno")
                    dRowB_Tran.Item("percentage") = .Item("percentage")
                    dRowB_Tran.Item("modeid") = .Item("modeid")
                    dRowB_Tran.Item("amount") = .Item("amount")
                    dRowB_Tran.Item("priority") = .Item("priority")
                    dRowB_Tran.Item("periodunkid") = .Item("periodunkid")
                    dRowB_Tran.Item("period_name") = .Item("period_name").ToString
                    dRowB_Tran.Item("end_date") = .Item("end_date").ToString
                    dRowB_Tran.Item("AUD") = .Item("AUD")

                    mdtTran.Rows.Add(dRowB_Tran)
                End With
            Next

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "Get_Bank_Tran", mstrModuleName)
        End Try
    End Sub

    Public Function InsertUpdateDelete_Tran(Optional ByVal objDo As clsDataOperation = Nothing, Optional ByVal intUserUnkid As Integer = 0) As Boolean

        Dim i As Integer
        Dim strQ As String = ""
        Dim strErrorMessage As String = ""
        Dim exForce As Exception
        Try

            If objDo Is Nothing Then
                objDataOperation = New clsDataOperation
                objDataOperation.BindTransaction()
            Else
                objDataOperation = objDo
            End If
            For i = 0 To mdtTran.Rows.Count - 1
                With mdtTran.Rows(i)
                    objDataOperation.ClearParameters()
                    If Not IsDBNull(.Item("AUD")) Then
                        mintGarnisheeBankTranUnkid = CInt(.Item("garnisheebanktranunkid").ToString)
                        mintBranchunkid = CInt(.Item("branchunkid").ToString)
                        mintAccounttypeunkid = CInt(.Item("accounttypeunkid").ToString)
                        mstrAccountno = .Item("accountno").ToString
                        mdblPercentage = CDec(.Item("percentage").ToString)
                        mintModeid = CInt(.Item("modeid").ToString)
                        mdecAmount = CDec(.Item("amount").ToString)
                        mintPriority = CInt(.Item("priority").ToString)
                        mblnIsvoid = False
                        mintVoiduserunkid = 0
                        mdtVoiddatetime = Nothing
                        mstrVoidreason = ""
                        mintPeriodunkid = CInt(.Item("periodunkid").ToString)
                        Select Case .Item("AUD")
                            Case "A"

                                strQ = "INSERT INTO hrgarnishee_bank_tran ( " & _
                                            "  dpndtbeneficetranunkid " & _
                                            ", branchunkid " & _
                                            ", accounttypeunkid " & _
                                            ", accountno " & _
                                            ", percentage " & _
                                            ", modeid " & _
                                            ", amount " & _
                                            ", priority " & _
                                            ", periodunkid " & _
                                            ", userunkid " & _
                                            ", isvoid " & _
                                            ", voiduserunkid " & _
                                            ", voiddatetime " & _
                                            ", voidreason" & _
                                        ") VALUES (" & _
                                            "  @dpndtbeneficetranunkid " & _
                                            ", @branchunkid " & _
                                            ", @accounttypeunkid " & _
                                            ", @accountno " & _
                                            ", @percentage " & _
                                            ", @modeid " & _
                                            ", @amount " & _
                                            ", @priority " & _
                                            ", @periodunkid " & _
                                            ", @userunkid " & _
                                            ", @isvoid " & _
                                            ", @voiduserunkid " & _
                                            ", @voiddatetime " & _
                                            ", @voidreason" & _
                                        "); SELECT @@identity"

                                objDataOperation.AddParameter("@dpndtbeneficetranunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintDependantTranUnkid.ToString)
                                objDataOperation.AddParameter("@branchunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintBranchunkid.ToString)
                                objDataOperation.AddParameter("@accounttypeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintAccounttypeunkid.ToString)
                                objDataOperation.AddParameter("@accountno", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrAccountno)
                                objDataOperation.AddParameter("@percentage", SqlDbType.Decimal, eZeeDataType.FLOAT_SIZE, mdblPercentage.ToString)
                                objDataOperation.AddParameter("@modeid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintModeid.ToString)
                                objDataOperation.AddParameter("@amount", SqlDbType.Decimal, eZeeDataType.FLOAT_SIZE, mdecAmount.ToString)
                                objDataOperation.AddParameter("@priority", SqlDbType.Int, eZeeDataType.INT_SIZE, mintPriority.ToString)
                                objDataOperation.AddParameter("@periodunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintPeriodunkid.ToString)
                                objDataOperation.AddParameter("@userunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintUserunkid)

                                objDataOperation.AddParameter("@isvoid", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsvoid)
                                objDataOperation.AddParameter("@voiduserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintVoiduserunkid)

                                If IsDBNull(.Item("voiddatetime")) = True Then
                                    objDataOperation.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, DBNull.Value)
                                Else
                                    objDataOperation.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, mdtVoiddatetime)
                                End If

                                objDataOperation.AddParameter("@voidreason", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrVoidreason)

                                Dim dsList As DataSet = objDataOperation.ExecQuery(strQ, "List")

                                If objDataOperation.ErrorMessage <> "" Then
                                    exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                                    Throw exForce
                                End If

                                mintGarnisheeBankTranUnkid = dsList.Tables(0).Rows(0).Item(0)

                                If InsertAuditTrails(objDataOperation, enAuditType.ADD) = False Then
                                    objDataOperation.ReleaseTransaction(False)
                                    Return False
                                End If

                            Case "U"

                                strQ = "UPDATE hrgarnishee_bank_tran SET " & _
                                        "  dpndtbeneficetranunkid = @dpndtbeneficetranunkid" & _
                                        ", branchunkid = @branchunkid " & _
                                        ", accounttypeunkid = @accounttypeunkid " & _
                                        ", accountno = @accountno " & _
                                        ", percentage = @percentage " & _
                                        ", modeid = @modeid " & _
                                        ", amount = @amount " & _
                                        ", priority = @priority " & _
                                        ", periodunkid = @periodunkid " & _
                                        ", userunkid = @userunkid " & _
                                        ", isvoid = @isvoid" & _
                                        ", voiduserunkid = @voiduserunkid" & _
                                        ", voiddatetime = @voiddatetime" & _
                                        ", voidreason = @voidreason " & _
                                      "WHERE garnisheebanktranunkid = @garnisheebanktranunkid "
                                objDataOperation.ClearParameters()
                                objDataOperation.AddParameter("@garnisheebanktranunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, .Item("garnisheebanktranunkid").ToString)
                                objDataOperation.AddParameter("@dpndtbeneficetranunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintDependantTranUnkid.ToString)
                                objDataOperation.AddParameter("@branchunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintBranchunkid.ToString)
                                objDataOperation.AddParameter("@accounttypeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintAccounttypeunkid.ToString)
                                objDataOperation.AddParameter("@accountno", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrAccountno)
                                objDataOperation.AddParameter("@percentage", SqlDbType.Decimal, eZeeDataType.FLOAT_SIZE, mdblPercentage.ToString)
                                objDataOperation.AddParameter("@modeid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintModeid.ToString)
                                objDataOperation.AddParameter("@amount", SqlDbType.Decimal, eZeeDataType.FLOAT_SIZE, mdecAmount.ToString)
                                objDataOperation.AddParameter("@priority", SqlDbType.Int, eZeeDataType.INT_SIZE, mintPriority.ToString)
                                objDataOperation.AddParameter("@periodunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintPeriodunkid.ToString)
                                objDataOperation.AddParameter("@userunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintUserunkid)

                                objDataOperation.AddParameter("@isvoid", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsvoid)
                                objDataOperation.AddParameter("@voiduserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintVoiduserunkid)

                                If IsDBNull(.Item("voiddatetime")) = True Then
                                    objDataOperation.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, DBNull.Value)
                                Else
                                    objDataOperation.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, mdtVoiddatetime)
                                End If

                                objDataOperation.AddParameter("@voidreason", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrVoidreason)

                                objDataOperation.ExecNonQuery(strQ)

                                If objDataOperation.ErrorMessage <> "" Then
                                    exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                                    Throw exForce
                                End If

                                If InsertAuditTrails(objDataOperation, enAuditType.EDIT) = False Then
                                    objDataOperation.ReleaseTransaction(False)
                                    Return False
                                End If

                            Case "D"

                                strQ = "UPDATE hrgarnishee_bank_tran SET " & _
                                     "  isvoid = 1 " & _
                                     " ,voiduserunkid = @voiduserunkid" & _
                                     " ,voiddatetime = @voiddatetime" & _
                                     " ,voidreason = @voidreason " & _
                                     "WHERE garnisheebanktranunkid = @garnisheebanktranunkid "
                                objDataOperation.AddParameter("@garnisheebanktranunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintGarnisheeBankTranUnkid.ToString)
                                objDataOperation.AddParameter("@voiduserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, .Item("voiduserunkid").ToString)
                                objDataOperation.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, .Item("Voiddatetime").ToString)
                                objDataOperation.AddParameter("@voidreason", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, .Item("Voidreason").ToString)

                                Call objDataOperation.ExecNonQuery(strQ)

                                If objDataOperation.ErrorMessage <> "" Then
                                    exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                                    Throw exForce
                                End If

                                Me._GarnisheeBankTranUnkid(objDataOperation) = mintGarnisheeBankTranUnkid

                                If InsertAuditTrails(objDataOperation, enAuditType.DELETE) = False Then
                                    objDataOperation.ReleaseTransaction(False)
                                    Return False
                                End If

                        End Select
                    End If
                End With
            Next

            If objDo Is Nothing Then
                objDataOperation.ReleaseTransaction(True)
            End If

            Return True
        Catch ex As Exception

            If objDo Is Nothing Then
                objDataOperation.ReleaseTransaction(False)
            End If
            DisplayError.Show("-1", ex.Message, "InsertUpdateDelete_Tran", mstrModuleName)
            Return False
        End Try
    End Function

    Public Function Update(ByVal dtCurrentDateAndTime As Date, Optional ByVal xDataOp As clsDataOperation = Nothing) As Boolean
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

       
        If xDataOp Is Nothing Then
            objDataOperation = New clsDataOperation
            objDataOperation.BindTransaction()
        Else
            objDataOperation = xDataOp
        End If

        Try
            strQ = "UPDATE hrgarnishee_bank_tran SET " & _
                                       "  dpndtbeneficetranunkid = @dpndtbeneficetranunkid" & _
                                       ", branchunkid = @branchunkid " & _
                                       ", accounttypeunkid = @accounttypeunkid " & _
                                       ", accountno = @accountno " & _
                                       ", percentage = @percentage " & _
                                       ", modeid = @modeid " & _
                                       ", amount = @amount " & _
                                       ", priority = @priority " & _
                                       ", periodunkid = @periodunkid " & _
                                       ", userunkid = @userunkid " & _
                                       ", isvoid = @isvoid" & _
                                       ", voiduserunkid = @voiduserunkid" & _
                                       ", voiddatetime = @voiddatetime" & _
                                       ", voidreason = @voidreason " & _
                                     "WHERE garnisheebanktranunkid = @garnisheebanktranunkid "
            objDataOperation.ClearParameters()
            objDataOperation.AddParameter("@garnisheebanktranunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintGarnisheeBankTranUnkid)
            objDataOperation.AddParameter("@dpndtbeneficetranunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintDependantTranUnkid.ToString)
            objDataOperation.AddParameter("@branchunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintBranchunkid.ToString)
            objDataOperation.AddParameter("@accounttypeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintAccounttypeunkid.ToString)
            objDataOperation.AddParameter("@accountno", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrAccountno)
            objDataOperation.AddParameter("@percentage", SqlDbType.Decimal, eZeeDataType.FLOAT_SIZE, mdblPercentage.ToString)
            objDataOperation.AddParameter("@modeid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintModeid.ToString)
            objDataOperation.AddParameter("@amount", SqlDbType.Decimal, eZeeDataType.FLOAT_SIZE, mdecAmount.ToString)
            objDataOperation.AddParameter("@priority", SqlDbType.Int, eZeeDataType.INT_SIZE, mintPriority.ToString)
            objDataOperation.AddParameter("@periodunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintPeriodunkid.ToString)
            objDataOperation.AddParameter("@userunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintUserunkid)

            objDataOperation.AddParameter("@isvoid", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsvoid)
            objDataOperation.AddParameter("@voiduserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintVoiduserunkid)

            If mdtVoiddatetime = Nothing Then
                objDataOperation.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, DBNull.Value)
            Else
                objDataOperation.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, mdtVoiddatetime)
            End If

            objDataOperation.AddParameter("@voidreason", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrVoidreason)

            objDataOperation.ExecNonQuery(strQ)

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            If InsertAuditTrails(objDataOperation, enAuditType.EDIT) = False Then
                objDataOperation.ReleaseTransaction(False)
                Return False
            End If

            If xDataOp Is Nothing Then objDataOperation.ReleaseTransaction(True)

            Return True
        Catch ex As Exception
            objDataOperation.ReleaseTransaction(False)
            Throw New Exception(ex.Message & "; Procedure Name: Update; Module Name: " & mstrModuleName)
            Return False
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            If xDataOp Is Nothing Then objDataOperation = Nothing
        End Try
    End Function

    Public Function Void_All(ByVal isVoid As Boolean, _
                             ByVal dtVoidDate As DateTime, _
                             ByVal strVoidRemark As String, _
                             ByVal intVoidUserId As Integer, _
                             Optional ByVal xDataOpr As clsDataOperation = Nothing _
                             ) As Boolean

        Dim strQ As String = ""
        Dim strErrorMessage As String = ""
        Dim exForce As Exception


        If xDataOpr IsNot Nothing Then
            objDataOperation = xDataOpr
        Else
            objDataOperation = New clsDataOperation
            objDataOperation.BindTransaction()
        End If
        objDataOperation.ClearParameters()

        Try
            strQ = "INSERT INTO athrgarnishee_bank_tran ( " & _
              "  tranguid " & _
              ", garnisheebanktranunkid " & _
              ", dpndtbeneficetranunkid " & _
              ", branchunkid " & _
              ", accounttypeunkid " & _
              ", accountno " & _
              ", percentage " & _
              ", modeid " & _
              ", amount " & _
              ", priority " & _
              ", periodunkid " & _
              ", auditdatetime " & _
              ", audittype " & _
              ", audituserunkid " & _
              ", loginemployeeunkid " & _
              ", ip " & _
              ", host " & _
              ", form_name " & _
              ", isweb " & _
              " )" & _
             "SELECT " & _
              "  LOWER(NEWID()) " & _
              ", garnisheebanktranunkid " & _
              ", dpndtbeneficetranunkid " & _
              ", branchunkid " & _
              ", accounttypeunkid " & _
              ", accountno " & _
              ", percentage " & _
              ", modeid " & _
              ", amount " & _
              ", priority " & _
              ", periodunkid " & _
              ", GETDATE() " & _
              ", 3 " & _
              ", @audituserunkid " & _
              ", @loginemployeeunkid " & _
              ", @ip " & _
              ", @host " & _
              ", @form_name " & _
              ", @isweb " & _
             "FROM hrgarnishee_bank_tran " & _
              "WHERE isvoid = 0 " & _
               "AND dpndtbeneficetranunkid = @dpndtbeneficetranunkid "
            objDataOperation.ClearParameters()
            objDataOperation.AddParameter("@dpndtbeneficetranunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintDependantTranUnkid)
            objDataOperation.AddParameter("@audituserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intVoidUserId)
            objDataOperation.AddParameter("@loginemployeeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintLogEmployeeUnkid)
            objDataOperation.AddParameter("@ip", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrClientIP)
            objDataOperation.AddParameter("@host", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrHostName)
            objDataOperation.AddParameter("@form_name", SqlDbType.NVarChar, eZeeDataType.DESC_SIZE, mstrFormName)
            objDataOperation.AddParameter("@isweb", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsWeb)

            Call objDataOperation.ExecNonQuery(strQ)
            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If
            objDataOperation.ClearParameters()

            strQ = "UPDATE hrgarnishee_bank_tran SET " & _
                     "  isvoid = @isvoid" & _
                     ", voiddatetime = @voiddatetime" & _
                     ", voiduserunkid = @voiduserunkid" & _
                     ", voidreason = @voidreason " & _
                    "WHERE isvoid = 0 " & _
                    " AND dpndtbeneficetranunkid = @dpndtbeneficetranunkid "


            xDataOpr.AddParameter("@isvoid", SqlDbType.Bit, eZeeDataType.BIT_SIZE, isVoid.ToString)
            xDataOpr.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, dtVoidDate)
            xDataOpr.AddParameter("@voidreason", SqlDbType.NVarChar, eZeeDataType.DESC_SIZE, strVoidRemark.ToString)
            xDataOpr.AddParameter("@voiduserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intVoidUserId)
            xDataOpr.AddParameter("@dpndtbeneficetranunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintDependantTranUnkid.ToString)

            Call xDataOpr.ExecNonQuery(strQ)

            If xDataOpr.ErrorMessage <> "" Then
                exForce = New Exception(xDataOpr.ErrorNumber & ": " & xDataOpr.ErrorMessage)
                Throw exForce
            End If

            If xDataOpr Is Nothing Then
                objDataOperation.ReleaseTransaction(True)
            End If

            Return True
        Catch ex As Exception
            If xDataOpr Is Nothing Then objDataOperation.ReleaseTransaction(False)
            DisplayError.Show("-1", ex.Message, "Void_All", mstrModuleName)
            Return False
        Finally
            exForce = Nothing
            If xDataOpr Is Nothing Then objDataOperation = Nothing
        End Try
    End Function

    Public Function InsertAuditTrails(ByVal objDataOperation As clsDataOperation, ByVal AuditType As Integer) As Boolean

        Dim strQ As String = ""
        Dim exForce As Exception

        objDataOperation = New clsDataOperation

        Try

            strQ = "INSERT INTO athrgarnishee_bank_tran ( " & _
                          "  tranguid " & _
                          ", garnisheebanktranunkid " & _
                          ", dpndtbeneficetranunkid " & _
                          ", branchunkid " & _
                          ", accounttypeunkid " & _
                          ", accountno " & _
                          ", percentage " & _
                          ", modeid " & _
                          ", amount " & _
                          ", priority " & _
                          ", periodunkid " & _
                          ", audittype " & _
                          ", audituserunkid " & _
                          ", loginemployeeunkid " & _
                          ", auditdatetime " & _
                          ", ip " & _
                          ", host" & _
                          ", form_name " & _
                          ", isweb " & _
                    ") VALUES (" & _
                          "  LOWER(NEWID()) " & _
                          ", @garnisheebanktranunkid " & _
                          ", @dpndtbeneficetranunkid " & _
                          ", @branchunkid " & _
                          ", @accounttypeunkid " & _
                          ", @accountno " & _
                          ", @percentage " & _
                          ", @modeid " & _
                          ", @amount " & _
                          ", @priority " & _
                          ", @periodunkid " & _
                          ", @audittype " & _
                          ", @audituserunkid " & _
                          ", @loginemployeeunkid " & _
                          ", @auditdatetime " & _
                          ", @ip " & _
                          ", @host" & _
                          ", @form_name " & _
                          ", @isweb " & _
                    "); SELECT @@identity"

            objDataOperation.ClearParameters()
            objDataOperation.AddParameter("@garnisheebanktranunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintGarnisheeBankTranUnkid.ToString)
            objDataOperation.AddParameter("@dpndtbeneficetranunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintDependantTranUnkid.ToString)
            objDataOperation.AddParameter("@branchunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintBranchunkid.ToString)
            objDataOperation.AddParameter("@accounttypeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintAccounttypeunkid.ToString)
            objDataOperation.AddParameter("@accountno", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrAccountno.ToString)
            objDataOperation.AddParameter("@percentage", SqlDbType.Money, eZeeDataType.MONEY_SIZE, mdblPercentage.ToString)
            objDataOperation.AddParameter("@audittype", SqlDbType.Int, eZeeDataType.INT_SIZE, AuditType.ToString)
            objDataOperation.AddParameter("@audituserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintUserunkid)
            objDataOperation.AddParameter("@loginemployeeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintLogEmployeeUnkid)
            objDataOperation.AddParameter("@auditdatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, mdtAuditDate)
            objDataOperation.AddParameter("@ip", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrClientIP)
            objDataOperation.AddParameter("@host", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrHostName)
            objDataOperation.AddParameter("@modeid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintModeid.ToString)
            objDataOperation.AddParameter("@amount", SqlDbType.Decimal, eZeeDataType.DECIMAL_SIZE, mdecAmount.ToString)
            objDataOperation.AddParameter("@priority", SqlDbType.Int, eZeeDataType.INT_SIZE, mintPriority.ToString)
            objDataOperation.AddParameter("@periodunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintPeriodunkid.ToString)
            objDataOperation.AddParameter("@form_name", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrFormName)
            objDataOperation.AddParameter("@isweb", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsWeb)

            objDataOperation.ExecNonQuery(strQ)

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            Return True

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "InsertAuditTrails", mstrModuleName)
            Return False
        Finally
            objDataOperation = Nothing
        End Try
    End Function
#End Region
End Class
