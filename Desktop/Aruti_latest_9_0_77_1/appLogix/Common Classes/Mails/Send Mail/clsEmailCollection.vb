﻿'************************************************************************************************************************************
'Class Name : clsEmailCollection.vb
'Purpose    : Send email from email collection through backgroud worker or thread.
'Date       :11/03/2014
'Written By :Sohail
'Modified   :
'************************************************************************************************************************************

Public Class clsEmailCollection

#Region " Private Variables "

    Private mstrEmailTo As String = String.Empty
    Private mstrSubject As String = String.Empty
    Private mstrMessage As String = String.Empty

    'Sohail (21 Apr 2014) -- Start
    'Enhancement - Salary Distribution by Amount and bank priority.
    'Private mstrFormName As String = String.Empty
    Private mintLogEmployeeUnkid As Integer = -1
    'Private mstrWebClientIP As String = String.Empty
    'Private mstrWebHostName As String = String.Empty
    Private mintUserUnkid As Integer = 0
    Private mintOperationModeId As Integer = 0
    Private mintModuleRefId As Integer = 0
    Private mstrSenderAddress As String = String.Empty
    'Sohail (21 Apr 2014) -- End

    'Nilay (01 Apr 2017) -- Start
    'ISSUE #23: Enhancements: Budget Employee TimeSheet Changes
    Private mstrFileName As String = String.Empty
    Private mstrExportReportPath As String = String.Empty
    'Nilay (01 Apr 2017) -- End

    'Sohail (12 Apr 2019) -- Start
    'NMB Enhancement - 76.1 - Store System Errors in database and allow to resend and archieve system errors.
    Private mintTranUnkId As Integer = 0
    'Sohail (12 Apr 2019) -- End

    'Hemant (12 Nov 2020) -- Start
    'Enhancement : NMB Final Recruitment UAT v1 (Point - 5)- On interview scheduling screen, the notification sent to internal applicants should be cc to the employee reporting to
    Private mstrCCAddress As String = String.Empty
    'Hemant (12 Nov 2020) -- End

#End Region

#Region " Public Properties "

    'S.SANDEEP [28-May-2018] -- START 
    'ISSUE/ENHANCEMENT : {Audit Trails} 
    Private mstrFormName As String = String.Empty
    Public Property _FormName() As String
        Get
            Return mstrFormName
        End Get
        Set(ByVal value As String)
            mstrFormName = value
        End Set
    End Property

    Private mstrClientIP As String = ""
    Public Property _ClientIP() As String
        Get
            Return mstrClientIP
        End Get
        Set(ByVal value As String)
            mstrClientIP = value
        End Set
    End Property

    Private mstrHostName As String = ""
    Public Property _HostName() As String
        Get
            Return mstrHostName
        End Get
        Set(ByVal value As String)
            mstrHostName = value
        End Set
    End Property

    Private mblnIsWeb As Boolean = False
    Public Property _FromWeb() As Boolean
        Get
            Return mblnIsWeb
        End Get
        Set(ByVal value As Boolean)
            mblnIsWeb = value
        End Set
    End Property

    Private mintAuditUserId As Integer = 0
    Public Property _AuditUserId() As Integer
        Get
            Return mintAuditUserId
        End Get
        Set(ByVal value As Integer)
            mintAuditUserId = value
        End Set
    End Property

    Private mdtAuditDate As DateTime = Now
    Public Property _AuditDate() As DateTime
        Get
            Return mdtAuditDate
        End Get
        Set(ByVal value As DateTime)
            mdtAuditDate = value
        End Set
    End Property
    'S.SANDEEP [28-May-2018] -- END 

    Public Property _EmailTo() As String
        Get
            Return mstrEmailTo
        End Get
        Set(ByVal value As String)
            mstrEmailTo = value
        End Set

    End Property

    Public Property _Subject() As String
        Get
            Return mstrSubject
        End Get
        Set(ByVal value As String)
            mstrSubject = value
        End Set
    End Property

    Public Property _Message() As String
        Get
            Return mstrMessage
        End Get
        Set(ByVal value As String)
            mstrMessage = value
        End Set
    End Property

    'Public Property _Form_Name() As String
    '    Get
    '        Return mstrFormName
    '    End Get
    '    Set(ByVal value As String)
    '        mstrFormName = value
    '    End Set
    'End Property

    Public Property _LogEmployeeUnkid() As Integer
        Get
            Return mintLogEmployeeUnkid
        End Get
        Set(ByVal value As Integer)
            mintLogEmployeeUnkid = value
        End Set
    End Property

    'Public WriteOnly Property _WebClientIP() As String
    '    Set(ByVal value As String)
    '        mstrWebClientIP = value
    '    End Set
    'End Property

    'Public Property _WebHostName() As String
    '    Get
    '        Return mstrWebHostName
    '    End Get
    '    Set(ByVal value As String)
    '        mstrWebHostName = value
    '    End Set
    'End Property

    Public Property _UserUnkid() As Integer
        Get
            Return mintUserUnkid
        End Get
        Set(ByVal value As Integer)
            mintUserUnkid = value
        End Set
    End Property

    Public Property _OperationModeId() As Integer
        Get
            Return mintOperationModeId
        End Get
        Set(ByVal value As Integer)
            mintOperationModeId = value
        End Set
    End Property

    Public Property _ModuleRefId() As Integer
        Get
            Return mintModuleRefId
        End Get
        Set(ByVal value As Integer)
            mintModuleRefId = value
        End Set
    End Property

    Public Property _SenderAddress() As String
        Get
            Return mstrSenderAddress
        End Get
        Set(ByVal value As String)
            mstrSenderAddress = value
        End Set
    End Property
    'Sohail (21 Apr 2014) -- End

    'Nilay (01 Apr 2017) -- Start
    'ISSUE #23: Enhancements: Budget Employee TimeSheet Changes
    Public Property _FileName() As String
        Get
            Return mstrFileName
        End Get
        Set(ByVal value As String)
            mstrFileName = value
        End Set
    End Property

    Public Property _ExportReportPath() As String
        Get
            Return mstrExportReportPath
        End Get
        Set(ByVal value As String)
            mstrExportReportPath = value
        End Set
    End Property
    'Nilay (01 Apr 2017) -- End

    'Sohail (12 Apr 2019) -- Start
    'NMB Enhancement - 76.1 - Store System Errors in database and allow to resend and archieve system errors.
    Public Property _TranUnkId() As Integer
        Get
            Return mintTranUnkId
        End Get
        Set(ByVal value As Integer)
            mintTranUnkId = value
        End Set
    End Property
    'Sohail (12 Apr 2019) -- End

    'Hemant (12 Nov 2020) -- Start
    'Enhancement : NMB Final Recruitment UAT v1 (Point - 5)- On interview scheduling screen, the notification sent to internal applicants should be cc to the employee reporting to
    Public Property _CCAddress() As String
        Get
            Return mstrCCAddress
        End Get
        Set(ByVal value As String)
            mstrCCAddress = value
        End Set
    End Property
    'Hemant (12 Nov 2020) -- End


#End Region

#Region " Constructor "

    Public Sub New()

    End Sub

    Public Sub New(ByVal strEmailTo As String, ByVal strSubject As String, ByVal strMessage As String, ByVal strFormName As String, _
                   ByVal intLogEmployeeUnkid As Integer, ByVal strWebClientIP As String, ByVal strWebHostName As String, ByVal intUserUnkid As Integer, _
                   ByVal intOperationModeId As Integer, ByVal intModuleRefId As Integer, ByVal strSenderAddress As String, _
                   Optional ByVal strFileName As String = "", Optional ByVal strExportReportPath As String = "", Optional ByVal intTranUnkId As Integer = 0, _
                   Optional ByVal strCCAddress As String = "")
        'Hemant (12 Nov 2020) -- [strCCAddress]
        'Sohail (12 Apr 2019) - [intTranUnkId]
        'Nilay (01 Apr 2017) -- [strFileName, strExportReportPath]

        mstrEmailTo = strEmailTo
        mstrSubject = strSubject
        mstrMessage = strMessage
        'Sohail (21 Apr 2014) -- Start
        'Enhancement - Salary Distribution by Amount and bank priority.
        mstrFormName = strFormName
        mintLogEmployeeUnkid = intLogEmployeeUnkid
        mstrClientIP = strWebClientIP
        mstrHostName = strWebHostName
        mintUserUnkid = intUserUnkid
        mintOperationModeId = intOperationModeId
        mintModuleRefId = intModuleRefId
        mstrSenderAddress = strSenderAddress
        'Sohail (21 Apr 2014) -- End

        'Nilay (01 Apr 2017) -- Start
        'ISSUE #23: Enhancements: Budget Employee TimeSheet Changes
        mstrFileName = strFileName
        mstrExportReportPath = strExportReportPath
        'Nilay (01 Apr 2017) -- End
        'Sohail (12 Apr 2019) -- Start
        'NMB Enhancement - 76.1 - Store System Errors in database and allow to resend and archieve system errors.
        mintTranUnkId = intTranUnkId
        'Sohail (12 Apr 2019) -- End

        'Hemant (12 Nov 2020) -- Start
        'Enhancement : NMB Final Recruitment UAT v1 (Point - 5)- On interview scheduling screen, the notification sent to internal applicants should be cc to the employee reporting to
        mstrCCAddress = strCCAddress
        'Hemant (12 Nov 2020) -- End

    End Sub

#End Region

End Class
