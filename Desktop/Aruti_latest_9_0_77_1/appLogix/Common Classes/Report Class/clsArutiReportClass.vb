﻿''************************************************************************************************************************************
''Class Name : clscfreport_master.vb
''Purpose    :
''Date       :9/10/2010
''Written By :Vimal M. Gohil
''Modified   :
''************************************************************************************************************************************

'Imports eZeeCommonLib
'''' <summary>
'''' Purpose: 
'''' Developer: Vimal M. Gohil
'''' </summary>
'Public Class clscfreport_master
'    Private Const mstrModuleName = "clscfreport_master"
'    Dim objDataOperation As clsDataOperation
'    Dim mstrMessage As String = ""

'#Region " Private variables "
'    Private mintReportunkid As Integer
'    Private mstrName As String = String.Empty
'    Private mstrReportfilename As String = String.Empty
'    Private mintReportcategoryunkid As Integer
'    Private mintSort_Key As Integer
'    Private mstrShort_Description As String = String.Empty
'    Private mstrDescription As String = String.Empty
'    Private mstrName1 As String = String.Empty
'    Private mstrName2 As String = String.Empty
'    Private mblnIsactive As Boolean = True
'#End Region

'#Region " Properties "
'    ''' <summary>
'    ''' Purpose: Get Message from Class 
'    ''' Modify By: Vimal M. Gohil
'    ''' </summary>
'    Public ReadOnly Property _Message() As String
'        Get
'            Return mstrMessage
'        End Get
'    End Property

'    ''' <summary>
'    ''' Purpose: Get or Set reportunkid
'    ''' Modify By: Vimal M. Gohil
'    ''' </summary>
'    Public Property _Reportunkid() As Integer
'        Get
'            Return mintReportunkid
'        End Get
'        Set(ByVal value As Integer)
'            mintReportunkid = Value
'            Call getData()
'        End Set
'    End Property

'    ''' <summary>
'    ''' Purpose: Get or Set name
'    ''' Modify By: Vimal M. Gohil
'    ''' </summary>
'    Public Property _Name() As String
'        Get
'            Return mstrName
'        End Get
'        Set(ByVal value As String)
'            mstrName = Value
'        End Set
'    End Property

'    ''' <summary>
'    ''' Purpose: Get or Set reportfilename
'    ''' Modify By: Vimal M. Gohil
'    ''' </summary>
'    Public Property _Reportfilename() As String
'        Get
'            Return mstrReportfilename
'        End Get
'        Set(ByVal value As String)
'            mstrReportfilename = Value
'        End Set
'    End Property

'    ''' <summary>
'    ''' Purpose: Get or Set reportcategoryunkid
'    ''' Modify By: Vimal M. Gohil
'    ''' </summary>
'    Public Property _Reportcategoryunkid() As Integer
'        Get
'            Return mintReportcategoryunkid
'        End Get
'        Set(ByVal value As Integer)
'            mintReportcategoryunkid = Value
'        End Set
'    End Property

'    ''' <summary>
'    ''' Purpose: Get or Set sort_key
'    ''' Modify By: Vimal M. Gohil
'    ''' </summary>
'    Public Property _Sort_Key() As Integer
'        Get
'            Return mintSort_Key
'        End Get
'        Set(ByVal value As Integer)
'            mintSort_Key = Value
'        End Set
'    End Property

'    ''' <summary>
'    ''' Purpose: Get or Set short_description
'    ''' Modify By: Vimal M. Gohil
'    ''' </summary>
'    Public Property _Short_Description() As String
'        Get
'            Return mstrShort_Description
'        End Get
'        Set(ByVal value As String)
'            mstrShort_Description = Value
'        End Set
'    End Property

'    ''' <summary>
'    ''' Purpose: Get or Set description
'    ''' Modify By: Vimal M. Gohil
'    ''' </summary>
'    Public Property _Description() As String
'        Get
'            Return mstrDescription
'        End Get
'        Set(ByVal value As String)
'            mstrDescription = Value
'        End Set
'    End Property

'    ''' <summary>
'    ''' Purpose: Get or Set name1
'    ''' Modify By: Vimal M. Gohil
'    ''' </summary>
'    Public Property _Name1() As String
'        Get
'            Return mstrName1
'        End Get
'        Set(ByVal value As String)
'            mstrName1 = Value
'        End Set
'    End Property

'    ''' <summary>
'    ''' Purpose: Get or Set name2
'    ''' Modify By: Vimal M. Gohil
'    ''' </summary>
'    Public Property _Name2() As String
'        Get
'            Return mstrName2
'        End Get
'        Set(ByVal value As String)
'            mstrName2 = Value
'        End Set
'    End Property

'    ''' <summary>
'    ''' Purpose: Get or Set isactive
'    ''' Modify By: Vimal M. Gohil
'    ''' </summary>
'    Public Property _Isactive() As Boolean
'        Get
'            Return mblnIsactive
'        End Get
'        Set(ByVal value As Boolean)
'            mblnIsactive = Value
'        End Set
'    End Property

'#End Region

'    ''' <summary>
'    ''' Modify By: Vimal M. Gohil
'    ''' </summary>
'    ''' <purpose> Assign all Property variable </purpose>
'    Public Sub GetData()
'        Dim dsList As DataSet = Nothing
'        Dim strQ As String = ""
'        Dim exForce As Exception

'        objDataOperation = New clsDataOperation

'        Try
'            StrQ = "SELECT " & _
'              "  reportunkid " & _
'              ", name " & _
'              ", reportfilename " & _
'              ", reportcategoryunkid " & _
'              ", sort_key " & _
'              ", short_description " & _
'              ", description " & _
'              ", name1 " & _
'              ", name2 " & _
'              ", isactive " & _
'             "FROM cfreport_master " & _
'             "WHERE reportunkid = @reportunkid "

'            objDataOperation.AddParameter("@reportunkid", SqlDbType.int, eZeeDataType.INT_SIZE, mintReportUnkId.ToString)

'            dsList = objDataOperation.ExecQuery(strQ, "List")

'            If objDataOperation.ErrorMessage <> "" Then
'                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
'                Throw exForce
'            End If

'            For Each dtRow As DataRow In dsList.Tables(0).Rows
'                mintreportunkid = CInt(dtRow.Item("reportunkid"))
'                mstrname = dtRow.Item("name").ToString
'                mstrreportfilename = dtRow.Item("reportfilename").ToString
'                mintreportcategoryunkid = CInt(dtRow.Item("reportcategoryunkid"))
'                mintsort_key = CInt(dtRow.Item("sort_key"))
'                mstrshort_description = dtRow.Item("short_description").ToString
'                mstrdescription = dtRow.Item("description").ToString
'                mstrname1 = dtRow.Item("name1").ToString
'                mstrname2 = dtRow.Item("name2").ToString
'                mblnisactive = CBool(dtRow.Item("isactive"))
'                Exit For
'            Next
'        Catch ex As Exception
'            Throw New Exception(ex.Message & "; Procedure Name: GetData; Module Name: " & mstrModuleName)
'        Finally
'            exForce = Nothing
'            If dsList IsNot Nothing Then dsList.Dispose()
'            objDataOperation = Nothing
'        End Try
'    End Sub


'    ''' <summary>
'    ''' Modify By: Vimal M. Gohil
'    ''' </summary>
'    ''' <purpose> Assign all Property variable </purpose>
'    Public Function GetList(ByVal strTableName As String, Optional ByVal blnOnlyActive As Boolean = True) As DataSet
'        Dim dsList As DataSet = Nothing
'        Dim strQ As String = ""
'        Dim exForce As Exception

'        objDataOperation = New clsDataOperation

'        Try
'            StrQ = "SELECT " & _
'              "  reportunkid " & _
'              ", name " & _
'              ", reportfilename " & _
'              ", reportcategoryunkid " & _
'              ", sort_key " & _
'              ", short_description " & _
'              ", description " & _
'              ", name1 " & _
'              ", name2 " & _
'              ", isactive " & _
'             "FROM cfreport_master "

'            If blnOnlyActive Then
'                strQ &= " WHERE isactive = 1 "
'            End If

'            dsList = objDataOperation.ExecQuery(strQ, strTableName)

'            If objDataOperation.ErrorMessage <> "" Then
'                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
'                Throw exForce
'            End If

'        Catch ex As Exception
'            Throw New Exception(ex.Message & "; Procedure Name: GetList; Module Name: " & mstrModuleName)
'        Finally
'            exForce = Nothing
'            If dsList IsNot Nothing Then dsList.Dispose()
'            objDataOperation = Nothing
'        End Try
'        Return dsList
'    End Function


'    ''' <summary>
'    ''' Modify By: Vimal M. Gohil
'    ''' </summary>
'    ''' <returns>Boolean</returns>
'    ''' <purpose> INSERT INTO Database Table (cfreport_master) </purpose>
'    Public Function Insert() As Boolean
'        If isExist(mstrName) Then
'            mstrMessage = "<Message>"
'            Return False
'        End If

'        Dim dsList As DataSet = Nothing
'        Dim strQ As String = ""
'        Dim exForce As Exception

'        objDataOperation = New clsDataOperation

'        Try
'            objDataOperation.AddParameter("@name", SqlDbType.nvarchar, eZeeDataType.NAME_SIZE, mstrname.ToString)
'            objDataOperation.AddParameter("@reportfilename", SqlDbType.nvarchar, eZeeDataType.NAME_SIZE, mstrreportfilename.ToString)
'            objDataOperation.AddParameter("@reportcategoryunkid", SqlDbType.int, eZeeDataType.INT_SIZE, mintreportcategoryunkid.ToString)
'            objDataOperation.AddParameter("@sort_key", SqlDbType.int, eZeeDataType.INT_SIZE, mintsort_key.ToString)
'            objDataOperation.AddParameter("@short_description", SqlDbType.nvarchar, eZeeDataType.NAME_SIZE, mstrshort_description.ToString)
'            objDataOperation.AddParameter("@description", SqlDbType.nvarchar, eZeeDataType.NAME_SIZE, mstrdescription.ToString)
'            objDataOperation.AddParameter("@name1", SqlDbType.nvarchar, eZeeDataType.NAME_SIZE, mstrname1.ToString)
'            objDataOperation.AddParameter("@name2", SqlDbType.nvarchar, eZeeDataType.NAME_SIZE, mstrname2.ToString)
'            objDataOperation.AddParameter("@isactive", SqlDbType.bit, eZeeDataType.BIT_SIZE, mblnisactive.ToString)

'            StrQ = "INSERT INTO cfreport_master ( " & _
'              "  name " & _
'              ", reportfilename " & _
'              ", reportcategoryunkid " & _
'              ", sort_key " & _
'              ", short_description " & _
'              ", description " & _
'              ", name1 " & _
'              ", name2 " & _
'              ", isactive" & _
'            ") VALUES (" & _
'              "  @name " & _
'              ", @reportfilename " & _
'              ", @reportcategoryunkid " & _
'              ", @sort_key " & _
'              ", @short_description " & _
'              ", @description " & _
'              ", @name1 " & _
'              ", @name2 " & _
'              ", @isactive" & _
'            "); SELECT @@identity"

'            dsList = objDataOperation.ExecQuery(strQ, "List")

'            If objDataOperation.ErrorMessage <> "" Then
'                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
'                Throw exForce
'            End If

'            mintReportUnkId = dsList.Tables(0).Rows(0).Item(0)

'            Return True
'        Catch ex As Exception
'            Throw New Exception(ex.Message & "; Procedure Name: Insert; Module Name: " & mstrModuleName)
'            Return False
'        Finally
'            exForce = Nothing
'            If dsList IsNot Nothing Then dsList.Dispose()
'            objDataOperation = Nothing
'        End Try
'    End Function

'    ''' <summary>
'    ''' Modify By: Vimal M. Gohil
'    ''' </summary>
'    ''' <returns>Boolean</returns>
'    ''' <purpose> Update Database Table (cfreport_master) </purpose>
'    Public Function Update() As Boolean
'        If isExist(mstrName, mintReportunkid) Then
'            mstrMessage = "<Message>"
'            Return False
'        End If

'        Dim dsList As DataSet = Nothing
'        Dim strQ As String = ""
'        Dim exForce As Exception

'        objDataOperation = New clsDataOperation

'        Try
'            objDataOperation.AddParameter("@reportunkid", SqlDbType.int, eZeeDataType.INT_SIZE, mintreportunkid.ToString)
'            objDataOperation.AddParameter("@name", SqlDbType.nvarchar, eZeeDataType.NAME_SIZE, mstrname.ToString)
'            objDataOperation.AddParameter("@reportfilename", SqlDbType.nvarchar, eZeeDataType.NAME_SIZE, mstrreportfilename.ToString)
'            objDataOperation.AddParameter("@reportcategoryunkid", SqlDbType.int, eZeeDataType.INT_SIZE, mintreportcategoryunkid.ToString)
'            objDataOperation.AddParameter("@sort_key", SqlDbType.int, eZeeDataType.INT_SIZE, mintsort_key.ToString)
'            objDataOperation.AddParameter("@short_description", SqlDbType.nvarchar, eZeeDataType.NAME_SIZE, mstrshort_description.ToString)
'            objDataOperation.AddParameter("@description", SqlDbType.nvarchar, eZeeDataType.NAME_SIZE, mstrdescription.ToString)
'            objDataOperation.AddParameter("@name1", SqlDbType.nvarchar, eZeeDataType.NAME_SIZE, mstrname1.ToString)
'            objDataOperation.AddParameter("@name2", SqlDbType.nvarchar, eZeeDataType.NAME_SIZE, mstrname2.ToString)
'            objDataOperation.AddParameter("@isactive", SqlDbType.bit, eZeeDataType.BIT_SIZE, mblnisactive.ToString)

'            StrQ = "UPDATE cfreport_master SET " & _
'              "  name = @name" & _
'              ", reportfilename = @reportfilename" & _
'              ", reportcategoryunkid = @reportcategoryunkid" & _
'              ", sort_key = @sort_key" & _
'              ", short_description = @short_description" & _
'              ", description = @description" & _
'              ", name1 = @name1" & _
'              ", name2 = @name2" & _
'              ", isactive = @isactive " & _
'            "WHERE reportunkid = @reportunkid "

'            Call objDataOperation.ExecNonQuery(strQ)

'            If objDataOperation.ErrorMessage <> "" Then
'                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
'                Throw exForce
'            End If

'            Return True
'        Catch ex As Exception
'            Throw New Exception(ex.Message & "; Procedure Name: Update; Module Name: " & mstrModuleName)
'            Return False
'        Finally
'            exForce = Nothing
'            If dsList IsNot Nothing Then dsList.Dispose()
'            objDataOperation = Nothing
'        End Try
'    End Function

'    ''' <summary>
'    ''' Modify By: Vimal M. Gohil
'    ''' </summary>
'    ''' <returns>Boolean</returns>
'    ''' <purpose> Delete Database Table (cfreport_master) </purpose>
'    Public Function Delete(ByVal intUnkid As Integer) As Boolean
'        If isUsed(intUnkid) Then
'            mstrMessage = "<Message>"
'            Return False
'        End If

'        Dim dsList As DataSet = Nothing
'        Dim strQ As String = ""
'        Dim exForce As Exception

'        objDataOperation = New clsDataOperation

'        Try
'            StrQ = "DELETE FROM cfreport_master " & _
'            "WHERE reportunkid = @reportunkid "

'            objDataOperation.AddParameter("@reportunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intUnkid)

'            Call objDataOperation.ExecNonQuery(strQ)

'            If objDataOperation.ErrorMessage <> "" Then
'                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
'                Throw exForce
'            End If

'            Return True
'        Catch ex As Exception
'            Throw New Exception(ex.Message & "; Procedure Name: Update; Module Name: " & mstrModuleName)
'            Return False
'        Finally
'            exForce = Nothing
'            If dsList IsNot Nothing Then dsList.Dispose()
'            objDataOperation = Nothing
'        End Try
'    End Function

'    ''' <summary>
'    ''' Modify By: Vimal M. Gohil
'    ''' </summary>
'    ''' <purpose> Assign all Property variable </purpose>
'    Public Function isUsed(ByVal intUnkid As Integer) As Boolean
'        Dim dsList As DataSet = Nothing
'        Dim strQ As String = ""
'        Dim exForce As Exception

'        objDataOperation = New clsDataOperation

'        Try
'            StrQ = "<Query>"

'            objDataOperation.AddParameter("@reportunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intUnkid)

'            dsList = objDataOperation.ExecQuery(strQ, "List")

'            If objDataOperation.ErrorMessage <> "" Then
'                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
'                Throw exForce
'            End If

'            Return dsList.tables(0).rows.count > 0
'        Catch ex As Exception
'            Throw New Exception(ex.Message & "; Procedure Name: isUsed; Module Name: " & mstrModuleName)
'        Finally
'            exForce = Nothing
'            If dsList IsNot Nothing Then dsList.Dispose()
'            objDataOperation = Nothing
'        End Try
'    End Function

'    ''' <summary>
'    ''' Modify By: Vimal M. Gohil
'    ''' </summary>
'    ''' <purpose> Assign all Property variable </purpose>
'    Public Function isExist(ByVal strCode As String, ByVal strName As String, Optional ByVal intUnkid As Integer = -1) As Boolean
'        Dim dsList As DataSet = Nothing
'        Dim strQ As String = ""
'        Dim exForce As Exception

'        objDataOperation = New clsDataOperation

'        Try
'            StrQ = "SELECT " & _
'              "  reportunkid " & _
'              ", name " & _
'              ", reportfilename " & _
'              ", reportcategoryunkid " & _
'              ", sort_key " & _
'              ", short_description " & _
'              ", description " & _
'              ", name1 " & _
'              ", name2 " & _
'              ", isactive " & _
'             "FROM cfreport_master " & _
'             "WHERE name = @name " & _
'             "AND code = @code "

'            If intUnkid > 0 Then
'                strQ &= " AND reportunkid <> @reportunkid"
'            End If

'            objDataOperation.AddParameter("@code", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, strCode)
'            objDataOperation.AddParameter("@name", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, strName)
'            objDataOperation.AddParameter("@reportunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intUnkid)

'            dsList = objDataOperation.ExecQuery(strQ, "List")

'            If objDataOperation.ErrorMessage <> "" Then
'                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
'                Throw exForce
'            End If

'            Return dsList.tables(0).rows.count > 0
'        Catch ex As Exception
'            Throw New Exception(ex.Message & "; Procedure Name: isExist; Module Name: " & mstrModuleName)
'        Finally
'            exForce = Nothing
'            If dsList IsNot Nothing Then dsList.Dispose()
'            objDataOperation = Nothing
'        End Try
'    End Function

'End Class


'************************************************************************************************************************************
'Class Name : clscfreport_master.vb
'Purpose    :
'Date       :9/10/2010
'Written By :Vimal M. Gohil
'Modified   :
'************************************************************************************************************************************

'Imports eZeeCommonLib
'Imports Aruti
'Imports ArutiReport
'Imports Aruti.Data

'''' <summary>
'''' Purpose: 
'''' Developer: Vimal M. Gohil
'''' </summary>
'Public Class clsArutiReportClass
'    Private Shared ReadOnly mstrModuleName As String = "clsArutiReportClass"
'    Dim objDataOperation As clsDataOperation
'    Dim mstrMessage As String = ""

'#Region " Private variables "
'    Private mintReportunkid As Integer
'    Private mstrName As String
'    Private mstrReportfilename As String
'    Private mintReportcategoryunkid As Integer
'    Private mintSort_Key As Integer
'    Private mstrDescription As String

'#End Region

'#Region " Properties "
'    ''' <summary>
'    ''' Purpose: Get Message from Class 
'    ''' Modify By: Vimal M. Gohil
'    ''' </summary>
'    Public ReadOnly Property _Message() As String
'        Get
'            Return mstrMessage
'        End Get
'    End Property

'    ''' <summary>
'    ''' Purpose: Get or Set reportunkid
'    ''' Modify By: Vimal M. Gohil
'    ''' </summary>
'    Public Property _Reportunkid() As Integer
'        Get
'            Return mintReportunkid
'        End Get
'        Set(ByVal value As Integer)
'            mintReportunkid = value
'            ' Call getData()
'        End Set
'    End Property

'    ''' <summary>
'    ''' Purpose: Get or Set name
'    ''' Modify By: Vimal M. Gohil
'    ''' </summary>
'    Public Property _Name() As String
'        Get
'            Return mstrName
'        End Get
'        Set(ByVal value As String)
'            mstrName = value
'        End Set
'    End Property

'    ''' <summary>
'    ''' Purpose: Get or Set reportfilename
'    ''' Modify By: Vimal M. Gohil
'    ''' </summary>
'    Public Property _Reportfilename() As String
'        Get
'            Return mstrReportfilename
'        End Get
'        Set(ByVal value As String)
'            mstrReportfilename = value
'        End Set
'    End Property

'    ''' <summary>
'    ''' Purpose: Get or Set reportcategoryunkid
'    ''' Modify By: Vimal M. Gohil
'    ''' </summary>
'    Public Property _Reportcategoryunkid() As Integer
'        Get
'            Return mintReportcategoryunkid
'        End Get
'        Set(ByVal value As Integer)
'            mintReportcategoryunkid = value
'        End Set
'    End Property

'    ''' <summary>
'    ''' Purpose: Get or Set sort_key
'    ''' Modify By: Vimal M. Gohil
'    ''' </summary>
'    Public Property _Sort_Key() As Integer
'        Get
'            Return mintSort_Key
'        End Get
'        Set(ByVal value As Integer)
'            mintSort_Key = value
'        End Set
'    End Property

'    '''' <summary>
'    '''' Purpose: Get or Set short_description
'    '''' Modify By: Vimal M. Gohil
'    '''' </summary>
'    'Public Property _Short_Description() As String
'    '    Get
'    '        Return mstrShort_Description
'    '    End Get
'    '    Set(ByVal value As String)
'    '        mstrShort_Description = Value
'    '    End Set
'    'End Property

'    ''' <summary>
'    ''' Purpose: Get or Set description
'    ''' Modify By: Vimal M. Gohil
'    ''' </summary>
'    Public Property _Description() As String
'        Get
'            Return mstrDescription
'        End Get
'        Set(ByVal value As String)
'            mstrDescription = value
'        End Set
'    End Property

'    '''' <summary>
'    '''' Purpose: Get or Set name1
'    '''' Modify By: Vimal M. Gohil
'    '''' </summary>
'    'Public Property _Name1() As String
'    '    Get
'    '        Return mstrName1
'    '    End Get
'    '    Set(ByVal value As String)
'    '        mstrName1 = Value
'    '    End Set
'    'End Property

'    '''' <summary>
'    '''' Purpose: Get or Set name2
'    '''' Modify By: Vimal M. Gohil
'    '''' </summary>
'    'Public Property _Name2() As String
'    '    Get
'    '        Return mstrName2
'    '    End Get
'    '    Set(ByVal value As String)
'    '        mstrName2 = Value
'    '    End Set
'    'End Property

'    '''' <summary>
'    '''' Purpose: Get or Set isactive
'    '''' Modify By: Vimal M. Gohil
'    '''' </summary>
'    'Public Property _Isactive() As Boolean
'    '    Get
'    '        Return mblnIsactive
'    '    End Get
'    '    Set(ByVal value As Boolean)
'    '        mblnIsactive = Value
'    '    End Set
'    'End Property

'#End Region

'    ''' <summary>
'    ''' Modify By: Vimal M. Gohil
'    ''' </summary>
'    ''' <purpose> Assign all Property variable </purpose>
'    Public Sub GetData()
'        Dim dsList As DataSet = Nothing
'        Dim strQ As String = ""
'        Dim exForce As Exception

'        objDataOperation = New clsDataOperation

'        Try
'            strQ = "SELECT " & _
'              "  reportunkid " & _
'              ", name " & _
'              ", reportfilename " & _
'              ", reportcategoryunkid " & _
'              ", sort_key " & _
'              ", description " & _
'             "FROM cfreport_master " & _
'             "WHERE reportunkid = @reportunkid "

'            objDataOperation.AddParameter("@reportunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintReportunkid.ToString)

'            dsList = objDataOperation.ExecQuery(strQ, "List")

'            If objDataOperation.ErrorMessage <> "" Then
'                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
'                Throw exForce
'            End If

'            For Each dtRow As DataRow In dsList.Tables(0).Rows
'                mintReportunkid = CInt(dtRow.Item("reportunkid"))
'                mstrName = dtRow.Item("name").ToString
'                mstrReportfilename = dtRow.Item("reportfilename").ToString
'                mintReportcategoryunkid = CInt(dtRow.Item("reportcategoryunkid"))
'                mintSort_Key = CInt(dtRow.Item("sort_key"))
'                mstrDescription = dtRow.Item("description").ToString
'                Exit For
'            Next
'        Catch ex As Exception
'            Throw New Exception(ex.Message & "; Procedure Name: GetData; Module Name: " & mstrModuleName)
'        Finally
'            exForce = Nothing
'            If dsList IsNot Nothing Then dsList.Dispose()
'            objDataOperation = Nothing
'        End Try
'    End Sub


'    ''' <summary>
'    ''' Modify By: Vimal M. Gohil
'    ''' </summary>
'    ''' <purpose> Assign all Property variable </purpose>
'    ''' 

'    Public Function getList(ByVal intUserUnkid As Integer, ByVal strKeyWord As String, ByVal blnSearchFullString As Boolean) As DataSet
'        Dim dsList As New DataSet
'        Dim strQ As String = ""
'        Dim exForce As Exception

'        Try
'            Dim objDataOperation As New clsDataOperation

'            Dim strCriteria As String = ""
'            If strKeyWord <> "" Then
'                If blnSearchFullString = True Then
'                    strCriteria += "AND (cfreport_master.description LIKE @KeyWord OR cfreport_master.name like @KeyWord) "
'                    objDataOperation.AddParameter("@KeyWord", SqlDbType.NVarChar, eZeeDataType.DESC_SIZE, "%" & strKeyWord & "%")
'                Else
'                    Dim strKey() As String
'                    strKey = Split(strKeyWord, " ")
'                    If strKey.Length > 0 Then
'                        strCriteria += " AND ( cfreport_master.description LIKE @KeyWord OR cfreport_master.name like @KeyWord "
'                        objDataOperation.AddParameter("@KeyWord", SqlDbType.NVarChar, eZeeDataType.DESC_SIZE, "%" & strKey(0) & "%")
'                        For i As Integer = 1 To strKey.Length - 1
'                            If strKey(i).Length > 0 Then
'                                strCriteria += " OR cfreport_master.description LIKE @KeyWord" & i & " OR cfreport_master.name like @KeyWord" & i & " "
'                                objDataOperation.AddParameter("@KeyWord" & i, SqlDbType.NVarChar, eZeeDataType.DESC_SIZE, "%" & strKey(i) & "%")
'                            End If
'                        Next
'                        strCriteria += " ) "
'                    End If
'                End If
'            End If

'            strQ = "SELECT cfreport_category.reportcategoryunkid, cfreport_master.name as DisplayName, " & _
'                                "cfreport_category.name as CategoryName, cfreport_master.reportunkid, sort_key " & _
'                           "FROM cfreport_master " & _
'                           "LEFT JOIN cfreport_category " & _
'                            "ON cfreport_master.reportcategoryunkid = cfreport_category.reportcategoryunkid "

'            strQ += strCriteria
'            strQ += " ORDER BY cfreport_category.reportcategoryunkid, sort_key"
'            objDataOperation.AddParameter("@userunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intUserUnkid)
'            dsList = objDataOperation.ExecQuery(strQ, "List")
'            If objDataOperation.ErrorMessage <> "" Then
'                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
'                Throw exForce
'            End If
'            Return dsList
'        Catch ex As Exception
'            DisplayError.Show(-1, ex.Message, "getList", mstrModuleName)
'            Return Nothing
'        Finally
'            dsList.Dispose()
'            dsList = Nothing
'        End Try
'    End Function

'    'Public Function GetList(ByVal strTableName As String, Optional ByVal blnOnlyActive As Boolean = True) As DataSet
'    '    Dim dsList As DataSet = Nothing
'    '    Dim strQ As String = ""
'    '    Dim exForce As Exception

'    '    objDataOperation = New clsDataOperation

'    '    Try
'    '        strQ = "SELECT " & _
'    '          "  reportunkid " & _
'    '          ", name " & _
'    '          ", reportfilename " & _
'    '          ", reportcategoryunkid " & _
'    '          ", sort_key " & _
'    '          ", short_description " & _
'    '          ", description " & _
'    '          ", name1 " & _
'    '          ", name2 " & _
'    '          ", isactive " & _
'    '         "FROM cfreport_master "

'    '        If blnOnlyActive Then
'    '            strQ &= " WHERE isactive = 1 "
'    '        End If

'    '        dsList = objDataOperation.ExecQuery(strQ, strTableName)

'    '        If objDataOperation.ErrorMessage <> "" Then
'    '            exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
'    '            Throw exForce
'    '        End If

'    '    Catch ex As Exception
'    '        Throw New Exception(ex.Message & "; Procedure Name: GetList; Module Name: " & mstrModuleName)
'    '    Finally
'    '        exForce = Nothing
'    '        If dsList IsNot Nothing Then dsList.Dispose()
'    '        objDataOperation = Nothing
'    '    End Try
'    '    Return dsList
'    'End Function


'    ''' <summary>
'    ''' Modify By: Vimal M. Gohil
'    ''' </summary>
'    ''' <returns>Boolean</returns>
'    ''' <purpose> INSERT INTO Database Table (cfreport_master) </purpose>
'    Public Function Insert() As Boolean
'        'If isExist(mstrName) Then
'        '    mstrMessage = "<Message>"
'        '    Return False
'        'End If

'        Dim dsList As DataSet = Nothing
'        Dim strQ As String = ""
'        Dim exForce As Exception

'        objDataOperation = New clsDataOperation

'        Try
'            objDataOperation.AddParameter("@name", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrName.ToString)
'            objDataOperation.AddParameter("@reportfilename", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrReportfilename.ToString)
'            objDataOperation.AddParameter("@reportcategoryunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintReportcategoryunkid.ToString)
'            objDataOperation.AddParameter("@sort_key", SqlDbType.Int, eZeeDataType.INT_SIZE, mintSort_Key.ToString)
'            objDataOperation.AddParameter("@description", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrDescription.ToString)


'            strQ = "INSERT INTO cfreport_master ( " & _
'              "  name " & _
'              ", reportfilename " & _
'              ", reportcategoryunkid " & _
'              ", sort_key " & _
'              ", description " & _
'            ") VALUES (" & _
'              "  @name " & _
'              ", @reportfilename " & _
'              ", @reportcategoryunkid " & _
'              ", @sort_key " & _
'              ", @description " & _
'            "); SELECT @@identity"

'            dsList = objDataOperation.ExecQuery(strQ, "List")

'            If objDataOperation.ErrorMessage <> "" Then
'                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
'                Throw exForce
'            End If

'            mintReportunkid = dsList.Tables(0).Rows(0).Item(0)

'            Return True
'        Catch ex As Exception
'            Throw New Exception(ex.Message & "; Procedure Name: Insert; Module Name: " & mstrModuleName)
'            Return False
'        Finally
'            exForce = Nothing
'            If dsList IsNot Nothing Then dsList.Dispose()
'            objDataOperation = Nothing
'        End Try
'    End Function

'    ''' <summary>
'    ''' Modify By: Vimal M. Gohil
'    ''' </summary>
'    ''' <returns>Boolean</returns>
'    ''' <purpose> Update Database Table (cfreport_master) </purpose>
'    Public Function Update() As Boolean
'        'If isExist(mstrName, mintReportunkid) Then
'        '    mstrMessage = "<Message>"
'        '    Return False
'        'End If

'        Dim dsList As DataSet = Nothing
'        Dim strQ As String = ""
'        Dim exForce As Exception

'        objDataOperation = New clsDataOperation

'        Try
'            objDataOperation.AddParameter("@reportunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintReportunkid.ToString)
'            objDataOperation.AddParameter("@name", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrName.ToString)
'            objDataOperation.AddParameter("@reportfilename", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrReportfilename.ToString)
'            objDataOperation.AddParameter("@reportcategoryunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintReportcategoryunkid.ToString)
'            objDataOperation.AddParameter("@sort_key", SqlDbType.Int, eZeeDataType.INT_SIZE, mintSort_Key.ToString)
'            objDataOperation.AddParameter("@description", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrDescription.ToString)


'            strQ = "UPDATE cfreport_master SET " & _
'              "  name = @name" & _
'              ", reportfilename = @reportfilename" & _
'              ", reportcategoryunkid = @reportcategoryunkid" & _
'              ", sort_key = @sort_key" & _
'              ", description = @description" & _
'             "WHERE reportunkid = @reportunkid "

'            Call objDataOperation.ExecNonQuery(strQ)

'            If objDataOperation.ErrorMessage <> "" Then
'                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
'                Throw exForce
'            End If

'            Return True
'        Catch ex As Exception
'            Throw New Exception(ex.Message & "; Procedure Name: Update; Module Name: " & mstrModuleName)
'            Return False
'        Finally
'            exForce = Nothing
'            If dsList IsNot Nothing Then dsList.Dispose()
'            objDataOperation = Nothing
'        End Try
'    End Function

'    ''' <summary>
'    ''' Modify By: Vimal M. Gohil
'    ''' </summary>
'    ''' <returns>Boolean</returns>
'    ''' <purpose> Delete Database Table (cfreport_master) </purpose>
'    Public Function Delete(ByVal intUnkid As Integer) As Boolean
'        If isUsed(intUnkid) Then
'            mstrMessage = "<Message>"
'            Return False
'        End If

'        Dim dsList As DataSet = Nothing
'        Dim strQ As String = ""
'        Dim exForce As Exception

'        objDataOperation = New clsDataOperation

'        Try
'            strQ = "DELETE FROM cfreport_master " & _
'            "WHERE reportunkid = @reportunkid "

'            objDataOperation.AddParameter("@reportunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intUnkid)

'            Call objDataOperation.ExecNonQuery(strQ)

'            If objDataOperation.ErrorMessage <> "" Then
'                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
'                Throw exForce
'            End If

'            Return True
'        Catch ex As Exception
'            Throw New Exception(ex.Message & "; Procedure Name: Update; Module Name: " & mstrModuleName)
'            Return False
'        Finally
'            exForce = Nothing
'            If dsList IsNot Nothing Then dsList.Dispose()
'            objDataOperation = Nothing
'        End Try
'    End Function

'    ''' <summary>
'    ''' Modify By: Vimal M. Gohil
'    ''' </summary>
'    ''' <purpose> Assign all Property variable </purpose>
'    Public Function isUsed(ByVal intUnkid As Integer) As Boolean
'        Dim dsList As DataSet = Nothing
'        Dim strQ As String = ""
'        Dim exForce As Exception

'        objDataOperation = New clsDataOperation

'        Try
'            strQ = "<Query>"

'            objDataOperation.AddParameter("@reportunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intUnkid)

'            dsList = objDataOperation.ExecQuery(strQ, "List")

'            If objDataOperation.ErrorMessage <> "" Then
'                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
'                Throw exForce
'            End If

'            Return dsList.Tables(0).Rows.Count > 0
'        Catch ex As Exception
'            Throw New Exception(ex.Message & "; Procedure Name: isUsed; Module Name: " & mstrModuleName)
'        Finally
'            exForce = Nothing
'            If dsList IsNot Nothing Then dsList.Dispose()
'            objDataOperation = Nothing
'        End Try
'    End Function

'    ''' <summary>
'    ''' Modify By: Vimal M. Gohil
'    ''' </summary>
'    ''' <purpose> Assign all Property variable </purpose>
'    Public Function isExist(ByVal strCode As String, ByVal strName As String, Optional ByVal intUnkid As Integer = -1) As Boolean
'        Dim dsList As DataSet = Nothing
'        Dim strQ As String = ""
'        Dim exForce As Exception

'        objDataOperation = New clsDataOperation

'        Try
'            strQ = "SELECT " & _
'              "  reportunkid " & _
'              ", name " & _
'              ", reportfilename " & _
'              ", reportcategoryunkid " & _
'              ", sort_key " & _
'              ", short_description " & _
'              ", description " & _
'              ", name1 " & _
'              ", name2 " & _
'              ", isactive " & _
'             "FROM cfreport_master " & _
'             "WHERE name = @name " & _
'             "AND code = @code "

'            If intUnkid > 0 Then
'                strQ &= " AND reportunkid <> @reportunkid"
'            End If

'            objDataOperation.AddParameter("@code", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, strCode)
'            objDataOperation.AddParameter("@name", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, strName)
'            objDataOperation.AddParameter("@reportunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intUnkid)

'            dsList = objDataOperation.ExecQuery(strQ, "List")

'            If objDataOperation.ErrorMessage <> "" Then
'                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
'                Throw exForce
'            End If

'            Return dsList.Tables(0).Rows.Count > 0
'        Catch ex As Exception
'            Throw New Exception(ex.Message & "; Procedure Name: isExist; Module Name: " & mstrModuleName)
'        Finally
'            exForce = Nothing
'            If dsList IsNot Nothing Then dsList.Dispose()
'            objDataOperation = Nothing
'        End Try
'    End Function

'End Class
'************************************************************************************************************************************
'Class Name : clscfreport_master.vb
'Purpose    :
'Date       :9/10/2010
'Written By :Vimal M. Gohil
'Modified   :
'************************************************************************************************************************************

Imports eZeeCommonLib
Imports Aruti.Data

''' <summary>
''' Purpose: 
''' Developer: Vimal M. Gohil
''' </summary>
Public Class clsArutiReportClass
    Private Shared ReadOnly mstrModuleName As String = "clsReport"
    Dim objDataOperation As clsDataOperation
    Dim mstrMessage As String = ""

#Region " Private variables "
    Private mintReportunkid As Integer
    Private mstrName As String
    Private mstrReportfilename As String
    Private mintReportcategoryunkid As Integer
    Private mintSort_Key As Integer
    Private mstrDescription As String
    Private mstrName1 As String
    Private mstrName2 As String
#End Region

#Region " Properties "
    'S.SANDEEP [28-May-2018] -- START 
    'ISSUE/ENHANCEMENT : {Audit Trails} 
    Private mstrFormName As String = String.Empty
    Public WriteOnly Property _FormName() As String
        Set(ByVal value As String)
            mstrFormName = value
        End Set
    End Property

    Private mstrClientIP As String = ""
    Public WriteOnly Property _ClientIP() As String
        Set(ByVal value As String)
            mstrClientIP = value
        End Set
    End Property

    Private mstrHostName As String = ""
    Public WriteOnly Property _HostName() As String
        Set(ByVal value As String)
            mstrHostName = value
        End Set
    End Property

    Private mblnIsWeb As Boolean = False
    Public WriteOnly Property _FromWeb() As Boolean
        Set(ByVal value As Boolean)
            mblnIsWeb = value
        End Set
    End Property

    Private mintAuditUserId As Integer = 0
    Public WriteOnly Property _AuditUserId() As Integer
        Set(ByVal value As Integer)
            mintAuditUserId = value
        End Set
    End Property

    Private mintCompanyUnkid As Integer = 0
    Public WriteOnly Property _CompanyUnkid() As Integer
        Set(ByVal value As Integer)
            mintCompanyUnkid = value
        End Set
    End Property

    Private mdtAuditDate As DateTime = Now
    Public WriteOnly Property _AuditDate() As DateTime
        Set(ByVal value As DateTime)
            mdtAuditDate = value
        End Set
    End Property

    Private xDataOpr As clsDataOperation = Nothing
    Public Property _xDataOpr() As clsDataOperation
        Get
            Return xDataOpr
        End Get
        Set(ByVal value As clsDataOperation)
            xDataOpr = value
        End Set
    End Property

    Private mintLoginEmployeeunkid As Integer = 0
    Public Property _LoginEmployeeunkid() As Integer
        Get
            Return mintLoginEmployeeunkid
        End Get
        Set(ByVal value As Integer)
            mintLoginEmployeeunkid = value
        End Set
    End Property

    Private mintVoidlogingemployeeunkid As Integer = 0
    Public Property _Voidloginemployeeunkid() As Integer
        Get
            Return mintVoidlogingemployeeunkid
        End Get
        Set(ByVal value As Integer)
            mintVoidlogingemployeeunkid = value
        End Set
    End Property
    'S.SANDEEP [28-May-2018] -- END 
    ''' <summary>
    ''' Purpose: Get Message from Class 
    ''' Modify By: Vimal M. Gohil
    ''' </summary>
    Public ReadOnly Property _Message() As String
        Get
            Return mstrMessage
        End Get
    End Property

    ''' <summary>
    ''' Purpose: Get or Set reportunkid
    ''' Modify By: Vimal M. Gohil
    ''' </summary>
    Public Property _Reportunkid() As Integer
        Get
            Return mintReportunkid
        End Get
        Set(ByVal value As Integer)
            mintReportunkid = value
            ' Call getData()
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set name
    ''' Modify By: Vimal M. Gohil
    ''' </summary>
    Public Property _Name() As String
        Get
            Return mstrName
        End Get
        Set(ByVal value As String)
            mstrName = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set reportfilename
    ''' Modify By: Vimal M. Gohil
    ''' </summary>
    Public Property _Reportfilename() As String
        Get
            Return mstrReportfilename
        End Get
        Set(ByVal value As String)
            mstrReportfilename = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set reportcategoryunkid
    ''' Modify By: Vimal M. Gohil
    ''' </summary>
    Public Property _Reportcategoryunkid() As Integer
        Get
            Return mintReportcategoryunkid
        End Get
        Set(ByVal value As Integer)
            mintReportcategoryunkid = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set sort_key
    ''' Modify By: Vimal M. Gohil
    ''' </summary>
    Public Property _Sort_Key() As Integer
        Get
            Return mintSort_Key
        End Get
        Set(ByVal value As Integer)
            mintSort_Key = value
        End Set
    End Property

    '''' <summary>
    '''' Purpose: Get or Set short_description
    '''' Modify By: Vimal M. Gohil
    '''' </summary>
    'Public Property _Short_Description() As String
    '    Get
    '        Return mstrShort_Description
    '    End Get
    '    Set(ByVal value As String)
    '        mstrShort_Description = Value
    '    End Set
    'End Property

    ''' <summary>
    ''' Purpose: Get or Set description
    ''' Modify By: Vimal M. Gohil
    ''' </summary>
    Public Property _Description() As String
        Get
            Return mstrDescription
        End Get
        Set(ByVal value As String)
            mstrDescription = value
        End Set
    End Property

    '''' <summary>
    '''' Purpose: Get or Set name1
    '''' Modify By: Vimal M. Gohil
    '''' </summary>
    'Public Property _Name1() As String
    '    Get
    '        Return mstrName1
    '    End Get
    '    Set(ByVal value As String)
    '        mstrName1 = Value
    '    End Set
    'End Property

    '''' <summary>
    '''' Purpose: Get or Set name2
    '''' Modify By: Vimal M. Gohil
    '''' </summary>
    'Public Property _Name2() As String
    '    Get
    '        Return mstrName2
    '    End Get
    '    Set(ByVal value As String)
    '        mstrName2 = Value
    '    End Set
    'End Property

    '''' <summary>
    '''' Purpose: Get or Set isactive
    '''' Modify By: Vimal M. Gohil
    '''' </summary>
    'Public Property _Isactive() As Boolean
    '    Get
    '        Return mblnIsactive
    '    End Get
    '    Set(ByVal value As Boolean)
    '        mblnIsactive = Value
    '    End Set
    'End Property

    ''' <summary>
    ''' Purpose: Get or Set name
    ''' Modify By: Sandeep
    ''' </summary>
    Public Property _Name1() As String
        Get
            Return mstrName1
        End Get
        Set(ByVal value As String)
            mstrName1 = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set name
    ''' Modify By: Sandeep
    ''' </summary>
    Public Property _Name2() As String
        Get
            Return mstrName2
        End Get
        Set(ByVal value As String)
            mstrName2 = value
        End Set
    End Property
#End Region

    ''' <summary>
    ''' Modify By: Vimal M. Gohil
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>
    Public Sub GetData()
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        objDataOperation = New clsDataOperation

        Try
            strQ = "SELECT " & _
              "  reportunkid " & _
              ", name " & _
              ", reportfilename " & _
              ", reportcategoryunkid " & _
              ", sort_key " & _
              ", description " & _
              ", name1 " & _
              ", name2 " & _
             "FROM hrmsConfiguration..cfreport_master " & _
             "WHERE reportunkid = @reportunkid "

            objDataOperation.AddParameter("@reportunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintReportunkid.ToString)

            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            For Each dtRow As DataRow In dsList.Tables(0).Rows
                mintReportunkid = CInt(dtRow.Item("reportunkid"))
                mstrName = dtRow.Item("name").ToString
                mstrReportfilename = dtRow.Item("reportfilename").ToString
                mintReportcategoryunkid = CInt(dtRow.Item("reportcategoryunkid"))
                mintSort_Key = CInt(dtRow.Item("sort_key"))
                mstrDescription = dtRow.Item("description").ToString

                mstrName1 = dtRow.Item("name1").ToString
                mstrName2 = dtRow.Item("name2").ToString

                Exit For
            Next
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: GetData; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
    End Sub

    'Nilay (12-Aug-2015) -- Start
    'User Access Report Privilge
    Public Function getReportListGroupedByCategory(ByVal strDBName As String, _
                                                   ByVal intUserUnkid As Integer, _
                                                   ByVal intCompanyUnkid As Integer, _
                                                   ByVal blnIsNotConfig As Boolean, _
                                                   Optional ByVal iEditUserId As Integer = 0) As DataSet
        Dim dsList As New DataSet
        Dim exForce As Exception
        Dim strQ As String = ""
        Dim intCountryUnkid As Integer = -1
        Try
            Dim objCompany As New clsCompany_Master
            objCompany._Companyunkid = intCompanyUnkid
            intCountryUnkid = objCompany._Countryunkid
            objCompany = Nothing

            Dim objDataOperation As New clsDataOperation

            strQ = "SELECT " & _
                   " " & strDBName & "..cfreport_category.reportcategoryunkid " & _
                   " ," & strDBName & "..cfreport_category.name AS DisplayName " & _
                   " ,'' AS CategoryName " & _
                   " ,-1 as reportunkid " & _
                   " ,-1 as sort_key " & _
                   " ,'' AS AUD " & _
                   " ,CAST(0 AS BIT) AS IsRcheck " & _
                   " ,0 AS reportprivilegeunkid " & _
                   " FROM " & strDBName & "..cfreport_category " & _
                   "    LEFT JOIN " & strDBName & "..cfreport_master ON " & strDBName & "..cfreport_master.reportcategoryunkid = " & strDBName & "..cfreport_category.reportcategoryunkid " & _
                   " WHERE 1=1 " & _
                   "    AND " & strDBName & "..cfreport_master.reportunkid NOT IN (SELECT reportunkid FROM " & strDBName & "..cfreport_master " & _
                   " WHERE " & strDBName & "..cfreport_master.name IN ('Employee Assessment Report', 'Discipline Analysis Report')) "

            strQ &= "UNION " & _
                    " SELECT " & _
                    " " & strDBName & "..cfreport_category.reportcategoryunkid " & _
                    " ,SPACE(5) + " & strDBName & "..cfreport_master.name AS DisplayName " & _
                    " ," & strDBName & "..cfreport_category.name AS CategoryName " & _
                    " ," & strDBName & "..cfreport_master.reportunkid " & _
                    " ," & strDBName & "..cfreport_master.sort_key " & _
                    " ,'' AS AUD "
            If iEditUserId > 0 Then
                strQ &= " ,CASE WHEN ISNULL(reportprivilegeunkid,0) > 0 THEN CAST(1 AS BIT) ELSE CAST(0 AS BIT) END AS IsRcheck " & _
                        " ,ISNULL(reportprivilegeunkid,0) AS reportprivilegeunkid "
            Else
                strQ &= " ,CAST(0 AS BIT) AS IsRcheck " & _
                        " ,0 AS reportprivilegeunkid "
            End If
            strQ &= " FROM " & strDBName & "..cfreport_category " & _
                    "   LEFT JOIN " & strDBName & "..cfreport_master ON " & strDBName & "..cfreport_master.reportcategoryunkid = " & strDBName & "..cfreport_category.reportcategoryunkid "
            If iEditUserId > 0 Then
                strQ &= "   LEFT JOIN " & strDBName & "..cfuser_reportprivilege ON " & strDBName & "..cfreport_master.reportunkid = " & strDBName & "..cfuser_reportprivilege.reportunkid AND " & strDBName & "..cfuser_reportprivilege.userunkid = " & iEditUserId & " AND " & strDBName & "..cfuser_reportprivilege.companyunkid = " & intCompanyUnkid & " "
            End If
            strQ &= " WHERE 1=1 " & _
                    "   AND " & strDBName & "..cfreport_master.reportunkid NOT IN (SELECT reportunkid FROM " & strDBName & "..cfreport_master " & _
                    " WHERE " & strDBName & "..cfreport_master.name IN ('Employee Assessment Report', 'Discipline Analysis Report')) "

            strQ &= GetReportFilterStringCountryWise(strDBName, intCountryUnkid, intUserUnkid, intCompanyUnkid, blnIsNotConfig)

            strQ &= ")"

            strQ += " ORDER BY " & strDBName & "..cfreport_category.reportcategoryunkid, CategoryName, sort_key"

            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: getReportListGroupedByCategory; Module Name: " & mstrModuleName)
        End Try
        Return dsList
    End Function

    'Nilay (12-Aug-2015) -- End


    'S.SANDEEP [10 AUG 2015] -- START
    'ENHANCEMENT : Aruti SaaS Changes
    ''' <summary>
    ''' Modify By: Vimal M. Gohil
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>

    Public Function getList(ByVal strDBName As String, ByVal intUserUnkid As Integer, ByVal intCompanyUnkid As Integer, ByVal blnIsNotConfig As Boolean, ByVal strKeyWord As String, ByVal blnSearchFullString As Boolean, ByVal intLangId As Integer) As DataSet
        Dim dsList As New DataSet
        Dim StrQ As String = ""
        Dim exForce As Exception
        Dim intCountryUnkid As Integer = -1
        Try
            Dim objCompany As New clsCompany_Master
            objCompany._Companyunkid = intCompanyUnkid
            intCountryUnkid = objCompany._Countryunkid
            objCompany = Nothing

            Using objDataOpr As New clsDataOperation
                Dim strCriteria As String = ""
                If strKeyWord <> "" Then
                    If blnSearchFullString = True Then
                        strCriteria += "AND (" & strDBName & "..cfreport_master.description LIKE @KeyWord OR " & strDBName & "..cfreport_master.name like @KeyWord) "
                        objDataOpr.AddParameter("@KeyWord", SqlDbType.NVarChar, eZeeDataType.DESC_SIZE, "%" & strKeyWord & "%")
                    Else
                        Dim strKey() As String
                        strKey = Split(strKeyWord, " ")
                        If strKey.Length > 0 Then
                            strCriteria += " AND ( " & strDBName & "..cfreport_master.description LIKE @KeyWord OR " & strDBName & "..cfreport_master.name like @KeyWord "
                            objDataOpr.AddParameter("@KeyWord", SqlDbType.NVarChar, eZeeDataType.DESC_SIZE, "%" & strKey(0) & "%")
                            For i As Integer = 1 To strKey.Length - 1
                                If strKey(i).Length > 0 Then
                                    strCriteria += " OR " & strDBName & "..cfreport_master.description LIKE @KeyWord" & i & " OR " & strDBName & "..cfreport_master.name like @KeyWord" & i & " "
                                    objDataOpr.AddParameter("@KeyWord" & i, SqlDbType.NVarChar, eZeeDataType.DESC_SIZE, "%" & strKey(i) & "%")
                                End If
                            Next
                            strCriteria += " ) "
                        End If
                    End If
                End If

                StrQ = "SELECT  "

                If blnIsNotConfig = True Then StrQ &= " DISTINCT "

                'StrQ &= " " & strDBName & "..cfreport_category.reportcategoryunkid, " & strDBName & "..cfreport_master.name as DisplayName, " & _
                '        " " & strDBName & "..cfreport_category.name as CategoryName, " & strDBName & "..cfreport_master.reportunkid, " & strDBName & "..cfreport_master.sort_key "


                StrQ &= " " & strDBName & "..cfreport_category.reportcategoryunkid, " & _
                        "CASE WHEN @LangId <= 0 THEN " & strDBName & "..cfreport_master.name " & _
                        "     WHEN @LangId = 1 THEN  " & strDBName & "..cfreport_master.name1 " & _
                        "     WHEN @LangId = 2 THEN " & strDBName & "..cfreport_master.name2 " & _
                        " END as DisplayName, " & _
                        " CASE WHEN @LangId <= 0 THEN " & strDBName & "..cfreport_category.name " & _
                        "      WHEN @LangId = 1 THEN " & strDBName & "..cfreport_category.name1 " & _
                        "      WHEN @LangId = 2 THEN " & strDBName & "..cfreport_category.name2 " & _
                        "END as CategoryName, " & strDBName & "..cfreport_master.reportunkid, " & strDBName & "..cfreport_master.sort_key "


                If blnIsNotConfig = True Then
                    StrQ &= ",(CASE WHEN ISNULL(" & strDBName & "..cfuser_reportprivilege.reportunkid, 0) = 0 THEN 0 ELSE 1 END ) AS Assign " & _
                            ",ISNULL(" & strDBName & "..cfuser_reportprivilege.isfavorite, 0) AS isfavorite "
                End If

                StrQ &= "FROM " & strDBName & "..cfreport_master " & _
                        "   JOIN " & strDBName & "..cfreport_category ON " & strDBName & "..cfreport_master.reportcategoryunkid = " & strDBName & "..cfreport_category.reportcategoryunkid "
                If blnIsNotConfig = True Then
                    StrQ &= "JOIN " & strDBName & "..cfuser_reportprivilege ON " & strDBName & "..cfuser_reportprivilege.reportunkid = " & strDBName & "..cfreport_master.reportunkid " & _
                            "   WHERE " & strDBName & "..cfuser_reportprivilege.userunkid = " & intUserUnkid & " " & _
                            "AND " & strDBName & "..cfuser_reportprivilege.companyunkid = " & intCompanyUnkid & " "
                Else
                    StrQ &= " WHERE 1 = 1 "
                End If

                StrQ &= " AND " & strDBName & "..cfreport_master.reportunkid NOT IN (SELECT reportunkid FROM " & strDBName & "..cfreport_master WHERE " & strDBName & "..cfreport_master.name IN ('Employee Assessment Report','Discipline Analysis Report','External Disciplinary Cases','Discipline Cases Details','Discipline Case Status','Performance Appraisal Report')) " 'S.SANDEEP [25 JUL 2016] -- START {'External Disciplinary Cases','Discipline Cases Details','Discipline Case Status'} -- START
                'S.SANDEEP [15 NOV 2016] -- START {Performance Appraisal Report} -- END


                'S.SANDEEP |19-MAY-2021| -- START
                'ISSUE/ENHANCEMENT : REPORTS WHICH ARE NOT SHOWN IN DESKTOP (FORM REPORT IS NOT CREATED)
                Dim strRptFilter As String = ""
                Dim arr() As String = {enArutiReport.Staff_Cmpts_Parameter_Report, _
                                       enArutiReport.Job_Family_Competencies_Report, _
                                       enArutiReport.Competence_Assessment_Form_Report, _
                                       enArutiReport.Competence_Score_Analysis_Report, _
                                       enArutiReport.Competence_Training_Intervension_Report}
               
                strRptFilter = String.Join(",", arr)
                If strRptFilter.Trim.Length > 0 Then
                    StrQ &= " AND " & strDBName & "..cfreport_master.reportunkid NOT IN (" & strRptFilter & ") "
                End If
                'S.SANDEEP |19-MAY-2021| -- END


                'NOTE : PLEASE DO NOT REMOVE THIS FILTER -> THIS IS FOR THE REPORT QUERY WE WROTE !!!            
                StrQ &= strCriteria & " "


                'S.SANDEEP |12-AUG-2019| -- START
                'ISSUE/ENHANCEMENT : ERROR IF REPORT DATA IS NOT SET FOR COMPANY
                'StrQ &= GetReportFilterStringCountryWise(strDBName, intCountryUnkid, intUserUnkid, intCompanyUnkid, blnIsNotConfig)
                Dim strFilter As String = String.Empty
                strFilter = GetReportFilterStringCountryWise(strDBName, intCountryUnkid, intUserUnkid, intCompanyUnkid, blnIsNotConfig)
                StrQ &= strFilter
                'S.SANDEEP |12-AUG-2019| -- END



                'NOTE : PLEASE DO NOT REMOVE THIS FILTER -> THIS IS FOR THE FILTER STRING WE GET FROM (GetReportFilterStringCountryWise)
                '       FUNCTION BASED ON COUNTRY !!!!!
                StrQ &= strCriteria & " "

                'S.SANDEEP |12-AUG-2019| -- START
                'ISSUE/ENHANCEMENT : ERROR IF REPORT DATA IS NOT SET FOR COMPANY
                'StrQ &= ")"
                If strFilter.Trim.Length > 0 Then
                    StrQ &= ")"
                End If
                'S.SANDEEP |12-AUG-2019| -- END

                


                StrQ &= " ORDER BY " & strDBName & "..cfreport_category.reportcategoryunkid, sort_key"


                objDataOpr.AddParameter("@LangId", SqlDbType.Int, eZeeDataType.INT_SIZE, IIf(intLangId < 0, 0, intLangId))

                dsList = objDataOpr.ExecQuery(StrQ, "List")

                If objDataOpr.ErrorMessage <> "" Then
                    exForce = New Exception(objDataOpr.ErrorNumber & ": " & objDataOpr.ErrorMessage)
                    Throw exForce
                End If

            End Using
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: getList; Module Name: " & mstrModuleName)
        Finally
        End Try
        Return dsList
    End Function

    'Public Function getList(ByVal strKeyWord As String, ByVal blnSearchFullString As Boolean, Optional ByVal blnIsNotConfig As Boolean = True) As DataSet
    '    Dim dsList As New DataSet
    '    Dim strQ As String = ""
    '    Dim exForce As Exception
    '    Dim strCommonStatutoryReports As String 'Sohail (12 Aug 2013)

    '    Try
    '        Dim objDataOperation As New clsDataOperation

    '        'Sohail (12 Aug 2013) -- Start
    '        'TRA - ENHANCEMENT
    '        strCommonStatutoryReports = enArutiReport.PensionFundReport & _
    '                                   "," & enArutiReport.Tax_Report

    '        'Sohail (12 Aug 2013) -- End

    '        Dim strCriteria As String = ""
    '        If strKeyWord <> "" Then
    '            If blnSearchFullString = True Then
    '                strCriteria += "AND (hrmsConfiguration..cfreport_master.description LIKE @KeyWord OR hrmsConfiguration..cfreport_master.name like @KeyWord) "
    '                objDataOperation.AddParameter("@KeyWord", SqlDbType.NVarChar, eZeeDataType.DESC_SIZE, "%" & strKeyWord & "%")
    '            Else
    '                Dim strKey() As String
    '                strKey = Split(strKeyWord, " ")
    '                If strKey.Length > 0 Then
    '                    strCriteria += " AND ( hrmsConfiguration..cfreport_master.description LIKE @KeyWord OR hrmsConfiguration..cfreport_master.name like @KeyWord "
    '                    objDataOperation.AddParameter("@KeyWord", SqlDbType.NVarChar, eZeeDataType.DESC_SIZE, "%" & strKey(0) & "%")
    '                    For i As Integer = 1 To strKey.Length - 1
    '                        If strKey(i).Length > 0 Then
    '                            strCriteria += " OR hrmsConfiguration..cfreport_master.description LIKE @KeyWord" & i & " OR hrmsConfiguration..cfreport_master.name like @KeyWord" & i & " "
    '                            objDataOperation.AddParameter("@KeyWord" & i, SqlDbType.NVarChar, eZeeDataType.DESC_SIZE, "%" & strKey(i) & "%")
    '                        End If
    '                    Next
    '                    strCriteria += " ) "
    '                End If
    '            End If
    '        End If


    '        'Pinkal (27-Jun-2013) -- Start
    '        'Enhancement : TRA Changes

    '        'strQ = "SELECT  hrmsConfiguration..cfreport_category.reportcategoryunkid, hrmsConfiguration..cfreport_master.name as DisplayName, " & _
    '        '                  " hrmsConfiguration..cfreport_category.name as CategoryName, hrmsConfiguration..cfreport_master.reportunkid, hrmsConfiguration..cfreport_master.sort_key "

    '        strQ = "SELECT  "

    '        If blnIsNotConfig = True Then strQ &= " DISTINCT "

    '        strQ &= " hrmsConfiguration..cfreport_category.reportcategoryunkid, hrmsConfiguration..cfreport_master.name as DisplayName, " & _
    '               " hrmsConfiguration..cfreport_category.name as CategoryName, hrmsConfiguration..cfreport_master.reportunkid, hrmsConfiguration..cfreport_master.sort_key "

    '        'Pinkal (27-Jun-2013) -- End


    '        If blnIsNotConfig = True Then
    '            strQ &= ",(CASE WHEN ISNULL(hrmsConfiguration..cfuser_reportprivilege.reportunkid, 0) = 0 THEN 0 ELSE 1 END ) AS Assign " & _
    '                    ",ISNULL(hrmsConfiguration..cfuser_reportprivilege.isfavorite, 0) AS isfavorite "
    '        End If

    '        strQ &= "FROM hrmsConfiguration..cfreport_master " & _
    '               "JOIN hrmsConfiguration..cfreport_category ON hrmsConfiguration..cfreport_master.reportcategoryunkid = hrmsConfiguration..cfreport_category.reportcategoryunkid "
    '        If blnIsNotConfig = True Then
    '            strQ &= "JOIN hrmsConfiguration..cfuser_reportprivilege ON hrmsConfiguration..cfuser_reportprivilege.reportunkid = hrmsConfiguration..cfreport_master.reportunkid " & _
    '                    " WHERE hrmsConfiguration..cfuser_reportprivilege.userunkid = " & User._Object._Userunkid & " " & _
    '                    "AND hrmsConfiguration..cfuser_reportprivilege.companyunkid = " & Company._Object._Companyunkid & " "
    '        Else
    '            strQ &= " WHERE 1= 1 "
    '        End If
    '        'Sandeep [ 29 NOV 2010 ] -- End 


    '        'S.SANDEEP [ 01 OCT 2012 ] -- START
    '        'ENHANCEMENT : TRA CHANGES

    '        'S.SANDEEP [ 05 NOV 2014 ] -- START
    '        'strQ &= " AND hrmsConfiguration..cfreport_master.reportunkid NOT IN (SELECT reportunkid FROM hrmsConfiguration..cfreport_master WHERE hrmsConfiguration..cfreport_master.name IN ('Employee Assessment Report','Discipline Analysis Report','Employee Assessment Form')) "
    '        strQ &= " AND hrmsConfiguration..cfreport_master.reportunkid NOT IN (SELECT reportunkid FROM hrmsConfiguration..cfreport_master WHERE hrmsConfiguration..cfreport_master.name IN ('Employee Assessment Report','Discipline Analysis Report')) "
    '        'S.SANDEEP [ 05 NOV 2014 ] -- END

    '        'Nilay (17-Mar-2015) -- Start
    '        'ENHANCEMENT - For searching full string
    '        strQ += strCriteria
    '        'Nilay (17-Mar-2015) -- End


    '        ','Employee Distribution Report'
    '        'S.SANDEEP [ 01 OCT 2012 ] -- END

    '        'Pinkal (27-Jun-2013) -- Start
    '        'Enhancement : TRA Changes

    'Select Case Company._Object._Countryunkid

    '    Case 51  ' CONGO

    '        'Sohail (12 Aug 2013) -- Start
    '        'TRA - ENHANCEMENT
    '    Case 112 'KENYA
    '        strQ &= "  AND cfreport_master.reportcategoryunkid <> 2 OR (hrmsConfiguration..cfreport_master.reportunkid IN " & _
    '                    " (SELECT hrmsConfiguration..cfreport_master.reportunkid FROM hrmsConfiguration..cfreport_master "
    '        If blnIsNotConfig Then
    '            strQ &= " JOIN hrmsConfiguration..cfuser_reportprivilege ON hrmsConfiguration..cfuser_reportprivilege.reportunkid = hrmsConfiguration..cfreport_master.reportunkid " & _
    '                     " AND hrmsConfiguration..cfuser_reportprivilege.userunkid = " & User._Object._Userunkid & " AND hrmsConfiguration..cfuser_reportprivilege.companyunkid = " & Company._Object._Companyunkid
    '        End If

    '        strQ &= " WHERE hrmsConfiguration..cfreport_master.reportunkid IN " & _
    '                     " (" & strCommonStatutoryReports & "," & enArutiReport.PAYE_P11_Report & " ," & enArutiReport.KN_NHIFReport & "," & enArutiReport.NSSF_FORM_5 & "," & enArutiReport.P9A_Report & "," & enArutiReport.P10_Report & "," & enArutiReport.ITax_Form_B_Report & "," & enArutiReport.HELB_REPORT & " ) ) AND cfreport_master.reportcategoryunkid = 2 "
    '        'Sohail (06 Apr 2015) - [HELB_REPORT]
    '        'Sohail (26 Jul 2014) - [ITax_Form_B_Report]
    '        'Sohail (13 Mar 2014) - [P10_Report]
    '        'Sohail (30 Jan 2014) - [P9A_Report]
    '        If blnIsNotConfig = True Then
    '            strQ &= "AND hrmsConfiguration..cfuser_reportprivilege.userunkid = " & User._Object._Userunkid & " AND hrmsConfiguration..cfuser_reportprivilege.companyunkid = " & Company._Object._Companyunkid & " "
    '        End If
    '        'Nilay (17-Mar-2015) -- Start
    '        'Enhancement - Searching Reports
    '        'strQ &= " ) "
    '        'Nilay (17-Mar-2015) -- End

    '        'Sohail (13 Mar 2014) - [P10_Report]
    '        'Sohail (30 Jan 2014) - [P9A_Report]
    '        'Sohail (12 Aug 2013) -- End

    '    Case 208 'TANZANIA

    '        strQ &= "  AND cfreport_master.reportcategoryunkid <> 2 OR (hrmsConfiguration..cfreport_master.reportunkid IN " & _
    '                     " (SELECT hrmsConfiguration..cfreport_master.reportunkid FROM hrmsConfiguration..cfreport_master  "

    '        If blnIsNotConfig Then
    '            strQ &= " JOIN hrmsConfiguration..cfuser_reportprivilege ON hrmsConfiguration..cfuser_reportprivilege.reportunkid = hrmsConfiguration..cfreport_master.reportunkid " & _
    '                        " AND hrmsConfiguration..cfuser_reportprivilege.userunkid = " & User._Object._Userunkid & " AND hrmsConfiguration..cfuser_reportprivilege.companyunkid = " & Company._Object._Companyunkid
    '        End If


    '        'Sohail (12 Aug 2013) -- Start
    '        'TRA - ENHANCEMENT
    '        'strQ &= "  WHERE hrmsConfiguration..cfreport_master.reportunkid IN " & _
    '        '             " (" & enArutiReport.PensionFundReport & "," & enArutiReport.IncomeTaxDeductionP9 & "," & enArutiReport.NSSF_FORM_5 & "," & _
    '        '               enArutiReport.IncomeTaxFormP10 & "," & enArutiReport.PPFContribution & "," & enArutiReport.SDL_Statutory_Report & ", " & _
    '        '               enArutiReport.LAPFContribution & "," & enArutiReport.PSPFContribution & "," & enArutiReport.GEPFContribution & "," & _
    '        '               enArutiReport.ZSSF_FORM & "," & enArutiReport.Tax_Report & "  ) ) AND cfreport_master.reportcategoryunkid = 2) "
    '        strQ &= "  WHERE hrmsConfiguration..cfreport_master.reportunkid IN " & _
    '                     " (" & strCommonStatutoryReports & "," & enArutiReport.IncomeTaxDeductionP9 & "," & enArutiReport.ZSSF_FORM & "," & _
    '                       enArutiReport.IncomeTaxFormP10 & "," & enArutiReport.PPFContribution & "," & enArutiReport.SDL_Statutory_Report & ", " & _
    '                       enArutiReport.LAPFContribution & "," & enArutiReport.PSPFContribution & "," & enArutiReport.GEPFContribution & "," & enArutiReport.NSSF_FORM_5 & "," & enArutiReport.EPF_PeriodWise_Report & ", " & _
    '                       enArutiReport.WCF_REPORT & _
    '                       "  ) ) AND cfreport_master.reportcategoryunkid = 2 "
    '        '           'Sohail (12 Aug 2014) - [EPF_PeriodWise_Report]

    '        'Pinkal (21 Jul 2015) -- [enArutiReport.WCF_REPORT] 

    '        If blnIsNotConfig = True Then
    '            strQ &= "AND hrmsConfiguration..cfuser_reportprivilege.userunkid = " & User._Object._Userunkid & " AND hrmsConfiguration..cfuser_reportprivilege.companyunkid = " & Company._Object._Companyunkid & " "
    '        End If
    '        'Nilay (17-Mar-2015) -- Start
    '        'Enhancement - Searching Reports
    '        'strQ &= " ) "
    '        'Nilay (17-Mar-2015) -- End

    '        'Sohail (12 Aug 2013) -- End

    '    Case 219  'UGANDA

    '        strQ &= "  AND cfreport_master.reportcategoryunkid <> 2 OR (hrmsConfiguration..cfreport_master.reportunkid IN " & _
    '                     " (SELECT hrmsConfiguration..cfreport_master.reportunkid FROM hrmsConfiguration..cfreport_master "

    '        If blnIsNotConfig Then
    '            strQ &= " JOIN hrmsConfiguration..cfuser_reportprivilege ON hrmsConfiguration..cfuser_reportprivilege.reportunkid = hrmsConfiguration..cfreport_master.reportunkid " & _
    '                     " AND hrmsConfiguration..cfuser_reportprivilege.userunkid = " & User._Object._Userunkid & " AND hrmsConfiguration..cfuser_reportprivilege.companyunkid = " & Company._Object._Companyunkid
    '        End If

    '        'Sohail (12 Aug 2013) -- Start
    '        'TRA - ENHANCEMENT
    '        'strQ &= " WHERE hrmsConfiguration..cfreport_master.reportunkid IN " & _
    '        '             " (" & enArutiReport.NSSF_FORM_5 & "," & enArutiReport.Tax_Report & "  ) ) AND cfreport_master.reportcategoryunkid = 2) "
    '        strQ &= " WHERE hrmsConfiguration..cfreport_master.reportunkid IN " & _
    '                     " (" & strCommonStatutoryReports & "," & enArutiReport.NSSF_FORM_5 & "  ) ) AND cfreport_master.reportcategoryunkid = 2  "
    '        'Sohail (12 Aug 2013) -- End
    '        If blnIsNotConfig = True Then
    '            strQ &= "AND hrmsConfiguration..cfuser_reportprivilege.userunkid = " & User._Object._Userunkid & " AND hrmsConfiguration..cfuser_reportprivilege.companyunkid = " & Company._Object._Companyunkid & " "
    '        End If
    '        'Nilay (17-Mar-2015) -- Start
    '        'Enhancement - Searching Reports
    '        'strQ &= " ) "
    '        'Nilay (17-Mar-2015) -- End

    '    Case 236  'ZAMBIA


    '        strQ &= "  AND cfreport_master.reportcategoryunkid <> 2 OR (hrmsConfiguration..cfreport_master.reportunkid IN " & _
    '                   " (SELECT hrmsConfiguration..cfreport_master.reportunkid FROM hrmsConfiguration..cfreport_master "

    '        If blnIsNotConfig Then
    '            strQ &= " JOIN hrmsConfiguration..cfuser_reportprivilege ON hrmsConfiguration..cfuser_reportprivilege.reportunkid = hrmsConfiguration..cfreport_master.reportunkid " & _
    '                     " AND hrmsConfiguration..cfuser_reportprivilege.userunkid = " & User._Object._Userunkid & " AND hrmsConfiguration..cfuser_reportprivilege.companyunkid = " & Company._Object._Companyunkid
    '        End If
    '        strQ &= " WHERE hrmsConfiguration..cfreport_master.reportunkid IN " & _
    '                     " (" & strCommonStatutoryReports & "," & enArutiReport.Za_ITF_P16_Report & "," & enArutiReport.Za_Monthly_AMD_Report & "," _
    '                       & enArutiReport.Za_AnnualPayeReturn & "," & enArutiReport.Za_RemittanceForm & " ) ) AND cfreport_master.reportcategoryunkid = 2  "
    '        If blnIsNotConfig = True Then
    '            strQ &= "AND hrmsConfiguration..cfuser_reportprivilege.userunkid = " & User._Object._Userunkid & " AND hrmsConfiguration..cfuser_reportprivilege.companyunkid = " & Company._Object._Companyunkid & " "
    '        End If
    '        'Nilay (17-Mar-2015) -- Start
    '        'Enhancement - Searching Reports
    '        'strQ &= " ) "
    '        'Nilay (17-Mar-2015) -- End

    '        'Sohail (10 May 2014) -- Start
    '        'Enhancement - EPF Contribution Report for Sri Lanka 
    '    Case 198  'SRI LANKA


    '        strQ &= "  AND cfreport_master.reportcategoryunkid <> 2 OR (hrmsConfiguration..cfreport_master.reportunkid IN " & _
    '                   " (SELECT hrmsConfiguration..cfreport_master.reportunkid FROM hrmsConfiguration..cfreport_master "

    '        If blnIsNotConfig Then
    '            strQ &= " JOIN hrmsConfiguration..cfuser_reportprivilege ON hrmsConfiguration..cfuser_reportprivilege.reportunkid = hrmsConfiguration..cfreport_master.reportunkid " & _
    '                     " AND hrmsConfiguration..cfuser_reportprivilege.userunkid = " & User._Object._Userunkid & " AND hrmsConfiguration..cfuser_reportprivilege.companyunkid = " & Company._Object._Companyunkid
    '        End If
    '        strQ &= " WHERE hrmsConfiguration..cfreport_master.reportunkid IN " & _
    '                     " (" & strCommonStatutoryReports & "," & enArutiReport.EPF_Contribution_Report & " ) ) AND cfreport_master.reportcategoryunkid = 2  "
    '        'Sohail (10 May 2014) -- End
    '        If blnIsNotConfig = True Then
    '            strQ &= "AND hrmsConfiguration..cfuser_reportprivilege.userunkid = " & User._Object._Userunkid & " AND hrmsConfiguration..cfuser_reportprivilege.companyunkid = " & Company._Object._Companyunkid & " "
    '        End If
    '        'Nilay (17-Mar-2015) -- Start
    '        'Enhancement - Searching Reports
    '        'strQ &= " ) "
    '        'Nilay (17-Mar-2015) -- End

    '        'Sohail (28 Aug 2014) -- Start
    '        'Enhancement - New Statutory Report Monthly Tax Income Report.
    '    Case 84  'GHANA

    '        strQ &= "  AND cfreport_master.reportcategoryunkid <> 2 OR (hrmsConfiguration..cfreport_master.reportunkid IN " & _
    '                   " (SELECT hrmsConfiguration..cfreport_master.reportunkid FROM hrmsConfiguration..cfreport_master "

    '        If blnIsNotConfig Then
    '            strQ &= " JOIN hrmsConfiguration..cfuser_reportprivilege ON hrmsConfiguration..cfuser_reportprivilege.reportunkid = hrmsConfiguration..cfreport_master.reportunkid " & _
    '                     " AND hrmsConfiguration..cfuser_reportprivilege.userunkid = " & User._Object._Userunkid & " AND hrmsConfiguration..cfuser_reportprivilege.companyunkid = " & Company._Object._Companyunkid
    '        End If
    '        strQ &= " WHERE hrmsConfiguration..cfreport_master.reportunkid IN " & _
    '                     " (" & strCommonStatutoryReports & "," & enArutiReport.Monthly_Tax_Income_Report & "," & enArutiReport.MONTHLY_TAX_DEDUCATION_SCHEDULE_REPORT & " ) ) AND cfreport_master.reportcategoryunkid = 2  "

    '        If blnIsNotConfig = True Then
    '            strQ &= "AND hrmsConfiguration..cfuser_reportprivilege.userunkid = " & User._Object._Userunkid & " AND hrmsConfiguration..cfuser_reportprivilege.companyunkid = " & Company._Object._Companyunkid & " "
    '        End If
    '        'Nilay (17-Mar-2015) -- Start
    '        'Enhancement - Searching Reports
    '        'strQ &= " ) "
    '        'Nilay (17-Mar-2015) -- End

    '        'Sohail (28 Aug 2014) -- End

    '        'Sohail (05 Jun 2015) -- Start
    '        'Enhancement - New Statutory Report Income Tax Division P12 for Malawi.
    '    Case 129 'MALAWI

    '        strQ &= "  AND cfreport_master.reportcategoryunkid <> 2 OR (hrmsConfiguration..cfreport_master.reportunkid IN " & _
    '                   " (SELECT hrmsConfiguration..cfreport_master.reportunkid FROM hrmsConfiguration..cfreport_master "

    '        If blnIsNotConfig Then
    '            strQ &= " JOIN hrmsConfiguration..cfuser_reportprivilege ON hrmsConfiguration..cfuser_reportprivilege.reportunkid = hrmsConfiguration..cfreport_master.reportunkid " & _
    '                     " AND hrmsConfiguration..cfuser_reportprivilege.userunkid = " & User._Object._Userunkid & " AND hrmsConfiguration..cfuser_reportprivilege.companyunkid = " & Company._Object._Companyunkid
    '        End If
    '        strQ &= " WHERE hrmsConfiguration..cfreport_master.reportunkid IN " & _
    '                     " (" & strCommonStatutoryReports & "," & enArutiReport.INCOME_TAX_DIVISION_P12_REPORT & " ) ) AND cfreport_master.reportcategoryunkid = 2  "
    '        'Sohail (10 May 2014) -- End
    '        If blnIsNotConfig = True Then
    '            strQ &= "AND hrmsConfiguration..cfuser_reportprivilege.userunkid = " & User._Object._Userunkid & " AND hrmsConfiguration..cfuser_reportprivilege.companyunkid = " & Company._Object._Companyunkid & " "
    '        End If
    '        'Sohail (05 Jun 2015) -- End

    '    Case Else

    '        strQ &= "  AND cfreport_master.reportcategoryunkid <> 2 OR (hrmsConfiguration..cfreport_master.reportunkid IN " & _
    '                   " (SELECT hrmsConfiguration..cfreport_master.reportunkid FROM hrmsConfiguration..cfreport_master "

    '        If blnIsNotConfig Then
    '            strQ &= " JOIN hrmsConfiguration..cfuser_reportprivilege ON hrmsConfiguration..cfuser_reportprivilege.reportunkid = hrmsConfiguration..cfreport_master.reportunkid " & _
    '                     " AND hrmsConfiguration..cfuser_reportprivilege.userunkid = " & User._Object._Userunkid & " AND hrmsConfiguration..cfuser_reportprivilege.companyunkid = " & Company._Object._Companyunkid
    '        End If

    '        strQ &= " WHERE hrmsConfiguration..cfreport_master.reportunkid IN " & _
    '                    " (" & strCommonStatutoryReports & " ) ) AND cfreport_master.reportcategoryunkid = 2  "

    '        If blnIsNotConfig = True Then
    '            strQ &= "AND hrmsConfiguration..cfuser_reportprivilege.userunkid = " & User._Object._Userunkid & " AND hrmsConfiguration..cfuser_reportprivilege.companyunkid = " & Company._Object._Companyunkid & " "
    '        End If
    '        'Nilay (17-Mar-2015) -- Start
    '        'Enhancement - Searching Reports
    '        'strQ &= " ) "
    '        'Nilay (17-Mar-2015) -- End

    'End Select

    ''Pinkal (27-Jun-2013) -- End

    'strQ += strCriteria

    ''Nilay (17-Mar-2015) -- Start
    ''Enhancement - Searching Reports
    'strQ &= ")"
    '        'Nilay (17-Mar-2015) -- End

    '        strQ += " ORDER BY hrmsConfiguration..cfreport_category.reportcategoryunkid, sort_key"
    '        dsList = objDataOperation.ExecQuery(strQ, "List")
    '        If objDataOperation.ErrorMessage <> "" Then
    '            exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
    '            Throw exForce
    '        End If
    '        Return dsList
    '    Catch ex As Exception
    '        DisplayError.Show(-1, ex.Message, "getList", mstrModuleName)
    '        Return Nothing
    '    Finally
    '        dsList.Dispose()
    '        dsList = Nothing
    '    End Try
    'End Function


    'S.SANDEEP [10 AUG 2015] -- END


    ''' <summary>
    ''' Modify By: Vimal M. Gohil
    ''' </summary>
    ''' <returns>Boolean</returns>
    ''' <purpose> INSERT INTO Database Table (cfreport_master) </purpose>
    Public Function Insert() As Boolean
        'If isExist(mstrName) Then
        '    mstrMessage = "<Message>"
        '    Return False
        'End If

        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        objDataOperation = New clsDataOperation

        Try
            objDataOperation.AddParameter("@name", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrName.ToString)
            objDataOperation.AddParameter("@reportfilename", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrReportfilename.ToString)
            objDataOperation.AddParameter("@reportcategoryunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintReportcategoryunkid.ToString)
            objDataOperation.AddParameter("@sort_key", SqlDbType.Int, eZeeDataType.INT_SIZE, mintSort_Key.ToString)
            objDataOperation.AddParameter("@description", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrDescription.ToString)


            strQ = "INSERT INTO hrmsConfiguration..cfreport_master ( " & _
              "  name " & _
              ", reportfilename " & _
              ", reportcategoryunkid " & _
              ", sort_key " & _
              ", description " & _
            ") VALUES (" & _
              "  @name " & _
              ", @reportfilename " & _
              ", @reportcategoryunkid " & _
              ", @sort_key " & _
              ", @description " & _
            "); SELECT @@identity"

            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            mintReportunkid = dsList.Tables(0).Rows(0).Item(0)

            Return True
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: Insert; Module Name: " & mstrModuleName)
            Return False
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
    End Function

    ''' <summary>
    ''' Modify By: Vimal M. Gohil
    ''' </summary>
    ''' <returns>Boolean</returns>
    ''' <purpose> Update Database Table (cfreport_master) </purpose>
    Public Function Update(ByVal intUserId As Integer, ByVal intCompanyId As Integer, _
                                        ByVal strIP As String, _
                                        ByVal strHost As String, _
                                        ByVal blnIsWeb As Boolean, _
                                        ByVal strScreenName As String) As Boolean
        'If isExist(mstrName, mintReportunkid) Then
        '    mstrMessage = "<Message>"
        '    Return False
        'End If

        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        objDataOperation = New clsDataOperation
        objDataOperation.BindTransaction()
        Try
            objDataOperation.AddParameter("@reportunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintReportunkid.ToString)
            'objDataOperation.AddParameter("@name", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrName.ToString)
            'objDataOperation.AddParameter("@reportfilename", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrReportfilename.ToString)
            'objDataOperation.AddParameter("@reportcategoryunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintReportcategoryunkid.ToString)
            'objDataOperation.AddParameter("@sort_key", SqlDbType.Int, eZeeDataType.INT_SIZE, mintSort_Key.ToString)
            'objDataOperation.AddParameter("@description", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrDescription.ToString)
            objDataOperation.AddParameter("@name1", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrName1.ToString)
            objDataOperation.AddParameter("@name2", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrName2.ToString)

            strQ = "UPDATE hrmsConfiguration..cfreport_master SET " & _
                   " name1 = @name1 " & _
                   ",name2 = @name2 " & _
             "WHERE reportunkid = @reportunkid "

            Call objDataOperation.ExecNonQuery(strQ)

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            Dim objCommonATLog As New clsCommonATLog
            With objCommonATLog
                ._AuditDate = Now
                ._AuditUserId = intUserId
                ._ClientIP = strIP
                ._CompanyUnkid = intCompanyId
                ._FormName = strScreenName
                ._FromWeb = blnIsWeb
                ._HostName = strHost
                ._LoginEmployeeUnkid = 0
            End With
            If objCommonATLog.Insert_AtLog(objDataOperation, enAuditType.EDIT, "cfreport_master", "reportunkid", mintReportunkid, True, intUserId, intCompanyId) = False Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            objDataOperation.ReleaseTransaction(True)

            Return True
        Catch ex As Exception
            objDataOperation.ReleaseTransaction(False)
            Throw New Exception(ex.Message & "; Procedure Name: Update; Module Name: " & mstrModuleName)
            Return False
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
    End Function

    ''' <summary>
    ''' Modify By: Vimal M. Gohil
    ''' </summary>
    ''' <returns>Boolean</returns>
    ''' <purpose> Delete Database Table (cfreport_master) </purpose>
    Public Function Delete(ByVal intUnkid As Integer) As Boolean
        If isUsed(intUnkid) Then
            mstrMessage = "<Message>"
            Return False
        End If

        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        objDataOperation = New clsDataOperation

        Try
            strQ = "DELETE FROM hrmsConfiguration..cfreport_master " & _
            "WHERE reportunkid = @reportunkid "

            objDataOperation.AddParameter("@reportunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intUnkid)

            Call objDataOperation.ExecNonQuery(strQ)

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            Return True
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: Update; Module Name: " & mstrModuleName)
            Return False
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
    End Function

    ''' <summary>
    ''' Modify By: Vimal M. Gohil
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>
    Public Function isUsed(ByVal intUnkid As Integer) As Boolean
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        objDataOperation = New clsDataOperation

        Try
            strQ = "<Query>"

            objDataOperation.AddParameter("@reportunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intUnkid)

            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            Return dsList.Tables(0).Rows.Count > 0
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: isUsed; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
    End Function

    ''' <summary>
    ''' Modify By: Vimal M. Gohil
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>
    Public Function isExist(ByVal strCode As String, ByVal strName As String, Optional ByVal intUnkid As Integer = -1) As Boolean
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        objDataOperation = New clsDataOperation

        Try
            strQ = "SELECT " & _
              "  reportunkid " & _
              ", name " & _
              ", reportfilename " & _
              ", reportcategoryunkid " & _
              ", sort_key " & _
              ", short_description " & _
              ", description " & _
              ", name1 " & _
              ", name2 " & _
              ", isactive " & _
             "FROM hrmsConfiguration..cfreport_master " & _
             "WHERE name = @name " & _
             "AND code = @code "

            If intUnkid > 0 Then
                strQ &= " AND reportunkid <> @reportunkid"
            End If

            objDataOperation.AddParameter("@code", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, strCode)
            objDataOperation.AddParameter("@name", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, strName)
            objDataOperation.AddParameter("@reportunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intUnkid)

            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            Return dsList.Tables(0).Rows.Count > 0
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: isExist; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
    End Function

    'Sandeep [ 29 NOV 2010 ] -- Start
    Public Function getReportList(ByVal intUserId As Integer, Optional ByVal intCompanyId As Integer = 0, Optional ByVal IsStatutoryGrpReq As Boolean = True) As DataSet
        Dim dsList As New DataSet
        Dim StrQ As String = String.Empty
        Dim exForce As Exception
        Dim dsAbilityReport As New DataSet
        objDataOperation = New clsDataOperation
        Try
            StrQ = "SELECT " & _
                       "		hrmsConfiguration..cfreport_category.reportcategoryunkid As ReportCatId " & _
                       "       ,hrmsConfiguration..cfreport_master.name AS ReportName " & _
                       "       ,hrmsConfiguration..cfreport_category.name AS ReportCateName " & _
                       "       ,hrmsConfiguration..cfreport_master.reportunkid As ReportId " & _
                       "       ,hrmsConfiguration..cfreport_master.sort_key " & _
                       "       ,(CASE WHEN ISNULL(hrmsConfiguration..cfuser_reportprivilege.reportunkid, 0) = 0 THEN 0 ELSE 1 END ) AS Assign " & _
                       "       ,ISNULL(hrmsConfiguration..cfuser_reportprivilege.isfavorite, 0) AS isfavorite " & _
                       "       ,hrmsConfiguration..cfuser_reportprivilege.companyunkid As CompanyId " & _
                       "       ,cfuser_reportprivilege.userunkid As UserId "

            If intCompanyId > 0 Then
                StrQ &= "   ,hrmsConfiguration..cfcompany_master.name As CompanyName "
            End If

            StrQ &= "FROM hrmsConfiguration..cfreport_master " & _
                       "    JOIN hrmsConfiguration..cfuser_reportprivilege ON hrmsConfiguration..cfreport_master.reportunkid = hrmsConfiguration..cfuser_reportprivilege.reportunkid " & _
                       "                                                  AND hrmsConfiguration..cfuser_reportprivilege.userunkid = @userunkid "
            If intCompanyId > 0 Then
                StrQ &= "  AND hrmsConfiguration..cfuser_reportprivilege.companyunkid = @companyunkid " & _
                        "   JOIN hrmsConfiguration..cfcompany_master ON hrmsConfiguration..cfcompany_master.companyunkid  = hrmsConfiguration..cfuser_reportprivilege.companyunkid "
            End If
            StrQ &= "       ,hrmsConfiguration..cfreport_category " & _
                         " WHERE   hrmsConfiguration..cfreport_master.reportcategoryunkid = hrmsConfiguration..cfreport_category.reportcategoryunkid "

            If IsStatutoryGrpReq = False Then
                StrQ &= " AND hrmsConfiguration..cfreport_category.reportcategoryunkid NOT IN (2) "
            End If

            StrQ &= "ORDER BY hrmsConfiguration..cfreport_category.reportcategoryunkid " & _
                        "  ,hrmsConfiguration..cfreport_master.reportunkid "

            'objDataOperation.AddParameter("@mainuserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, 1)
            objDataOperation.AddParameter("@userunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intUserId.ToString)
            objDataOperation.AddParameter("@companyunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intCompanyId.ToString)

            dsList = objDataOperation.ExecQuery(StrQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            Return dsList

            '"    AND hrmsConfiguration..cfreport_master.reportunkid IN ( SELECT  hrmsConfiguration..cfreport_master.reportunkid FROM hrmsConfiguration..cfreport_master WHERE isactive = 1 ) " & _

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "getReportList", mstrModuleName)
            Return Nothing
        End Try
    End Function

    Public Function RemovePrivilege(ByVal intUserId As Integer, ByVal intReportId As Integer, ByVal intCompanyId As Integer) As Boolean
        Dim StrQ As String = String.Empty
        Dim exForce As Exception
        Try
            objDataOperation = New clsDataOperation
            StrQ = "DELETE FROM hrmsConfiguration..cfuser_reportprivilege " & _
                   "WHERE userunkid = @UserId " & _
                   "AND reportunkid = @ReportId " & _
                   "AND companyunkid = @CompanyId "
            objDataOperation.AddParameter("@UserId", SqlDbType.Int, eZeeDataType.INT_SIZE, intUserId)
            objDataOperation.AddParameter("@ReportId", SqlDbType.Int, eZeeDataType.INT_SIZE, intReportId)
            objDataOperation.AddParameter("@CompanyId", SqlDbType.Int, eZeeDataType.INT_SIZE, intCompanyId)

            objDataOperation.ExecNonQuery(StrQ)

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & " : " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            Return True

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "RemovePrivilege", mstrModuleName)
            Return False
        End Try
    End Function

    'S.SANDEEP [10 AUG 2015] -- START
    'ENHANCEMENT : Aruti SaaS Changes
    Public Function InsertReportPrivilegeByRole_User(ByVal StrDataBaseName As String, _
                                                     ByVal iUsrRole As List(Of Integer), _
                                                     ByVal iDictionary As Dictionary(Of Integer, String), _
                                                     ByVal intCompanyId As Integer, _
                                                     ByVal blnIsByRole As Boolean, _
                                                     ByVal iOperationUserId As Integer, _
                                                     Optional ByVal iLableCtrl As Label = Nothing) As Boolean
        Dim dtUsr As DataTable = Nothing
        Dim blnFlag As Boolean = True
        Try
            Dim dUsr As List(Of Integer) = Nothing
            If blnIsByRole = True Then
                Dim objUser As New clsUserAddEdit
                Dim strValue As String = String.Empty
                strValue = String.Join(",", iUsrRole.[Select](Function(n) n.ToString()).ToArray())
                dtUsr = objUser.GetGroupedUserList("List", clsUserAddEdit.enViewUserType.SHOW_ACTIVE_USER, Now, Now, StrDataBaseName & "..cfuser_master.roleunkid IN (" & strValue & ")")
                dUsr = dtUsr.AsEnumerable().Where(Function(ir) ir.Field(Of Integer)("IsGrp") = False).Select(Function(row) row.Field(Of Integer)("UserUnkid")).ToList()
                objUser = Nothing
            Else
                dUsr = iUsrRole
            End If

            '******************************* ASSIGN / UNASSIGN REPORT PRIVILEGE(S) *******************************| START |
            Dim lblMainText As String = "Total User(s) : " & CStr(dUsr.Count) & " | Total Report(s) : " & CStr(iDictionary.Keys.Count)
            Dim strUserValue As String = String.Empty
            Dim strReportValue As String = String.Empty
            For iUserId As Integer = 0 To dUsr.Count - 1    'FOR EACH USER ID LOOP
                Application.DoEvents()
                Dim iRportCnt As Integer = 1
                strUserValue = " | Processing User(s) : " & CStr(iUserId + 1) & "/" & CStr(dUsr.Count)
                For Each iReportId As Integer In iDictionary.Keys   'FOR EACH REPORT ID TICKED/UNTICKED [AUD MAINTAINED]
                    mstrMessage = ""
                    Application.DoEvents()
                    strReportValue = " | Processing Report(s) : " & CStr(iRportCnt) & "/" & CStr(iDictionary.Keys.Count)
                    Select Case CStr(iDictionary(iReportId)).ToUpper
                        Case "A"    'ADD NEW REPORT PRIVILEGE
                            mstrMessage = InsertReportPrivilege(StrDataBaseName, CInt(dUsr(iUserId)), iReportId, intCompanyId, iOperationUserId)
                            If mstrMessage.Trim.Length > 0 Then
                                blnFlag = False
                            End If
                        Case "D"    'REMOVE EXISTING REPORT PRIVILEGE
                            mstrMessage = DeleteReportPrivilege(StrDataBaseName, dUsr(iUserId), iReportId, intCompanyId, iOperationUserId)
                            If mstrMessage.Trim.Length > 0 Then
                                blnFlag = False
                            End If
                    End Select
                    If iLableCtrl IsNot Nothing Then
                        iRportCnt += 1
                        iLableCtrl.Text = lblMainText & strUserValue & strReportValue
                    End If
                Next
                If iLableCtrl IsNot Nothing Then
                    iLableCtrl.Text = lblMainText & strUserValue & strReportValue
                End If
            Next
            '******************************* ASSIGN / UNASSIGN REPORT PRIVILEGE(S) *******************************| END   |
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: ; Module Name: InsertReportPrivilegeByRole_User" & mstrModuleName)
        Finally
            If dtUsr IsNot Nothing Then dtUsr.Dispose()
        End Try
        Return blnFlag
    End Function

    Public Function DeleteReportPrivilege(ByVal StrDataBaseName As String, _
                                          ByVal intUserUnkid As Integer, _
                                          ByVal intReportUnkid As Integer, _
                                          ByVal intCompanyId As Integer, _
                                          ByVal intOperationUserId As Integer) As String
        Dim StrQ As String = String.Empty
        Dim intReportPrivilegeUnkid As Integer = 0
        Dim strMsg As String = ""
        Try
            Using objDataOpr As New clsDataOperation
                objDataOpr.BindTransaction()

                StrQ = "SELECT " & _
                       "    @rprivilegeid = reportprivilegeunkid " & _
                       "FROM " & StrDataBaseName & "..cfuser_reportprivilege " & _
                       "WHERE userunkid = " & intUserUnkid & " AND reportunkid = " & intReportUnkid & " AND companyunkid = " & intCompanyId & " "

                objDataOpr.AddParameter("@rprivilegeid", SqlDbType.Int, eZeeDataType.INT_SIZE, intReportPrivilegeUnkid, ParameterDirection.InputOutput)

                objDataOpr.ExecNonQuery(StrQ)

                If objDataOpr.ErrorMessage <> "" Then
                    objDataOpr.ReleaseTransaction(False)
                    strMsg = objDataOpr.ErrorNumber & ": " & objDataOpr.ErrorMessage
                    Return strMsg
                End If

                intReportPrivilegeUnkid = objDataOpr.GetParameterValue("@rprivilegeid")

                If intReportPrivilegeUnkid > 0 Then

                    'S.SANDEEP [28-May-2018] -- START
                    'ISSUE/ENHANCEMENT : {Audit Trails} 
                    Dim objCommonATLog As New clsCommonATLog
                    objCommonATLog._FormName = mstrFormName
                    objCommonATLog._LoginEmployeeUnkid = mintLoginEmployeeunkid
                    objCommonATLog._ClientIP = mstrClientIP
                    objCommonATLog._HostName = mstrHostName
                    objCommonATLog._FromWeb = mblnIsWeb
                    objCommonATLog._AuditUserId = mintAuditUserId
                    objCommonATLog._CompanyUnkid = mintCompanyUnkid
                    objCommonATLog._AuditDate = mdtAuditDate
                    'S.SANDEEP [28-May-2018] -- END

                    If objCommonATLog.Insert_AtLog(objDataOpr, enAuditType.DELETE, "cfuser_reportprivilege", "reportprivilegeunkid", intReportPrivilegeUnkid, True, intOperationUserId, intCompanyId) = False Then
                        objDataOpr.ReleaseTransaction(False)
                        strMsg = objDataOpr.ErrorNumber & ": " & objDataOpr.ErrorMessage
                        Return strMsg
                    End If

                    'S.SANDEEP [28-May-2018] -- START
                    'ISSUE/ENHANCEMENT : {Audit Trails} 
                    objCommonATLog = Nothing
                    'S.SANDEEP [28-May-2018] -- END


                    StrQ = "DELETE FROM " & StrDataBaseName & "..cfuser_reportprivilege WHERE reportprivilegeunkid = " & intReportPrivilegeUnkid & " "

                    objDataOpr.ExecNonQuery(StrQ)

                    If objDataOpr.ErrorMessage <> "" Then
                        objDataOpr.ReleaseTransaction(False)
                        strMsg = objDataOpr.ErrorNumber & ": " & objDataOpr.ErrorMessage
                        Return strMsg
                    End If

                End If

                objDataOpr.ReleaseTransaction(True)

            End Using
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: DeleteReportPrivilege; Module Name: " & mstrModuleName)
        Finally
        End Try
        Return strMsg
    End Function

    Public Function InsertReportPrivilege(ByVal StrDataBaseName As String, _
                                          ByVal intUserUnkid As Integer, _
                                          ByVal intReportUnkid As Integer, _
                                          ByVal intCompanyId As Integer, _
                                          ByVal intOperationUserId As Integer) As String
        Dim StrQ As String = String.Empty
        Dim intReportPrivilegeUnkid As Integer = 0
        Dim dsList As New DataSet
        Dim strMsg As String = ""
        Try
            Using objDataOpr As New clsDataOperation

                objDataOpr.BindTransaction()

                If objDataOpr.RecordCount("SELECT * FROM cfuser_reportprivilege WHERE userunkid = " & intUserUnkid & " AND reportunkid = " & intReportUnkid & " AND companyunkid = " & intCompanyId & " ") <= 0 Then

                    StrQ = "INSERT INTO " & StrDataBaseName & "..cfuser_reportprivilege " & _
                                           "( " & _
                                           "  userunkid " & _
                                           " ,reportunkid " & _
                                           " ,companyunkid " & _
                                           " ,isfavorite " & _
                                           ") " & _
                                           "VALUES " & _
                                           "( " & _
                                           "  @userunkid " & _
                                           " ,@reportunkid " & _
                                           " ,@companyunkid " & _
                                           " ,@isfavorite " & _
                                           "); SELECT @@identity "

                    objDataOpr.AddParameter("@userunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intUserUnkid)
                    objDataOpr.AddParameter("@reportunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intReportUnkid)
                    objDataOpr.AddParameter("@companyunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intCompanyId)
                    objDataOpr.AddParameter("@isfavorite", SqlDbType.Bit, eZeeDataType.BIT_SIZE, False)

                    dsList = objDataOpr.ExecQuery(StrQ, "List")

                    If objDataOpr.ErrorMessage <> "" Then
                        objDataOpr.ReleaseTransaction(False)
                        strMsg = objDataOpr.ErrorNumber & ": " & objDataOpr.ErrorMessage
                        Return strMsg
                    End If

                    intReportPrivilegeUnkid = dsList.Tables(0).Rows(0)(0)

                    'S.SANDEEP [28-May-2018] -- START
                    'ISSUE/ENHANCEMENT : {Audit Trails} 
                    Dim objCommonATLog As New clsCommonATLog
                    objCommonATLog._FormName = mstrFormName
                    objCommonATLog._LoginEmployeeUnkid = mintLoginEmployeeunkid
                    objCommonATLog._ClientIP = mstrClientIP
                    objCommonATLog._HostName = mstrHostName
                    objCommonATLog._FromWeb = mblnIsWeb
                    objCommonATLog._AuditUserId = mintAuditUserId
                    objCommonATLog._CompanyUnkid = mintCompanyUnkid
                    objCommonATLog._AuditDate = mdtAuditDate
                    'S.SANDEEP [28-May-2018] -- END

                    If objCommonATLog.Insert_AtLog(objDataOpr, enAuditType.ADD, "cfuser_reportprivilege", "reportprivilegeunkid", intReportPrivilegeUnkid, True, intOperationUserId, intCompanyId) = False Then
                        objDataOpr.ReleaseTransaction(False)
                        strMsg = objDataOpr.ErrorNumber & ": " & objDataOpr.ErrorMessage
                        Return strMsg
                    End If

                    'S.SANDEEP [28-May-2018] -- START
                    'ISSUE/ENHANCEMENT : {Audit Trails} 
                    objCommonATLog = Nothing
                    'S.SANDEEP [28-May-2018] -- END

                End If

                objDataOpr.ReleaseTransaction(True)

            End Using
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: InsertReportPrivilege; Module Name: " & mstrModuleName)
        Finally
            dsList.Dispose()
        End Try
        Return strMsg
    End Function
    'S.SANDEEP [10 AUG 2015] -- END

    Public Function getcategoryList(ByVal blnAddSelect As Boolean, ByVal intLangId As Integer) As DataSet
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception
        Try
            Using objDo As New clsDataOperation
                If blnAddSelect Then
                    strQ = "SELECT 0 AS Id, @Select AS Name UNION ALL "
                End If
                strQ &= "SELECT reportcategoryunkid AS Id " & _
                        ",CASE WHEN @LangId <=0 THEN name " & _
                        "     WHEN @LangId = 1 THEN name1 " & _
                        "     WHEN @LangId = 2 THEN name2 END AS Name FROM hrmsConfiguration..cfreport_category "


                objDo.AddParameter("@LangId", SqlDbType.Int, eZeeDataType.INT_SIZE, intLangId)
                objDo.AddParameter("@Select", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 200, "Select"))

                dsList = objDo.ExecQuery(strQ, "List")

                If objDo.ErrorMessage <> "" Then
                    exForce = New Exception(objDo.ErrorNumber & ": " & objDo.ErrorMessage)
                    Throw exForce
                End If
            End Using
            Return dsList
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: ; Module Name: " & mstrModuleName)
        Finally
        End Try
    End Function

    Public Function GetList(ByVal intChangeLangForId As Integer, ByVal intCategoryId As Integer) As DataSet
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        objDataOperation = New clsDataOperation
        Try
            Select Case intChangeLangForId
                Case 0
                    strQ = "SELECT name,name1,name2,reportcategoryunkid AS reportunkid FROM hrmsConfiguration..cfreport_category " & _
                           " ORDER BY name "
                Case 1
                    strQ = "SELECT name,name1,name2,reportunkid FROM hrmsConfiguration..cfreport_master " & _
                                   " WHERE hrmsConfiguration..cfreport_master.reportunkid NOT IN (SELECT reportunkid FROM hrmsConfiguration..cfreport_master WHERE hrmsConfiguration..cfreport_master.name IN ('Employee Assessment Report','Discipline Analysis Report','External Disciplinary Cases','Discipline Cases Details','Discipline Case Status','Performance Appraisal Report')) "

                    If intCategoryId > 0 Then
                        strQ &= " AND reportcategoryunkid  = '" & intCategoryId & "'  "
                    End If

                    strQ &= " ORDER BY name "
            End Select



            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            Return dsList

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: GetList; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
    End Function

    Public Function InsertReportPrivilege(ByVal dtTable As DataTable) As Boolean
        Dim i As Integer
        Dim StrQ As String = String.Empty
        Dim strErrorMessage As String = ""
        Dim exForce As Exception
        Try
            objDataOperation = New clsDataOperation
            objDataOperation.BindTransaction()
            For i = 0 To dtTable.Rows.Count - 1
                With dtTable.Rows(i)
                    objDataOperation.ClearParameters()

                    If CBool(.Item("Assign")) = True Then Continue For

                    StrQ = "INSERT INTO hrmsConfiguration..cfuser_reportprivilege " & _
                           "( userunkid " & _
                           " ,reportunkid " & _
                           " ,companyunkid " & _
                           " ,isfavorite " & _
                           ")VALUES " & _
                           "( @userunkid " & _
                           " ,@reportunkid " & _
                           " ,@companyunkid " & _
                           " ,@isfavorite " & _
                           ") "

                    objDataOperation.AddParameter("@userunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, .Item("UserId").ToString)
                    objDataOperation.AddParameter("@reportunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, .Item("ReportId").ToString)
                    objDataOperation.AddParameter("@companyunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, .Item("CompanyId").ToString)
                    objDataOperation.AddParameter("@isfavorite", SqlDbType.Bit, eZeeDataType.BIT_SIZE, False)

                    objDataOperation.ExecNonQuery(StrQ)

                    If objDataOperation.ErrorMessage <> "" Then
                        exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                        Throw exForce
                    End If
                End With
            Next
            objDataOperation.ReleaseTransaction(True)
            Return True
        Catch ex As Exception
            objDataOperation.ReleaseTransaction(False)
            DisplayError.Show("-1", ex.Message, "InsertReportPrivilege", mstrModuleName)
            Return False
        End Try
    End Function

    Public Sub AddToFavorite(ByVal intUserUnkid As Integer, ByVal intReportUnkid As Integer, ByVal intCompanyId As Integer)
        Dim strQ As String = ""
        Dim exForce As Exception

        Dim objDataOperation As New clsDataOperation
        Try
            objDataOperation.AddParameter("@userunkid", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, intUserUnkid)
            objDataOperation.AddParameter("@reportunkid", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, intReportUnkid)
            objDataOperation.AddParameter("@CompanyId", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, intCompanyId)
            strQ = "UPDATE hrmsConfiguration..cfuser_reportprivilege SET isfavorite = 1 " & _
                   "WHERE userunkid = @userunkid AND reportunkid = @reportunkid AND companyunkid = @CompanyId "
            Call objDataOperation.ExecNonQuery(strQ)
            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If
        Catch ex As Exception
            DisplayError.Show(-1, ex.Message, "AddToFavorite", mstrModuleName)
        Finally
            exForce = Nothing
            objDataOperation = Nothing
        End Try
    End Sub

    Public Sub RemoveFromFavorite(ByVal intUserUnkid As Integer, ByVal intReportUnkid As Integer, ByVal intCompanyId As Integer)
        Dim strQ As String = ""
        Dim exForce As Exception

        Dim objDataOperation As New clsDataOperation
        Try
            objDataOperation.AddParameter("@userunkid", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, intUserUnkid)
            objDataOperation.AddParameter("@reportunkid", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, intReportUnkid)
            objDataOperation.AddParameter("@CompanyId", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, intCompanyId)
            strQ = "UPDATE hrmsConfiguration..cfuser_reportprivilege SET isfavorite = 0 " & _
                   "WHERE userunkid = @userunkid AND reportunkid = @reportunkid AND companyunkid = @CompanyId "
            Call objDataOperation.ExecNonQuery(strQ)
            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If
        Catch ex As Exception
            DisplayError.Show(-1, ex.Message, "RemoveFromFavorite", mstrModuleName)
        Finally
            exForce = Nothing
            objDataOperation = Nothing
        End Try
    End Sub
    'Sandeep [ 29 NOV 2010 ] -- End 

    'S.SANDEEP [ 29 OCT 2012 ] -- START
    'ENHANCEMENT : TRA CHANGES
    Public Shared Function Is_Report_Assigned(ByVal intReportId As Integer, Optional ByVal iUserId As Integer = 0, Optional ByVal iCompanyId As Integer = 0) As Boolean 'S.SANDEEP [ 29 May 2013 ] -- START -- END
        'Public Shared Function Is_Report_Assigned(ByVal intReportId As Integer) As Boolean
        Dim StrQ As String = String.Empty
        Dim blnFlag As Boolean = False
        Dim dsList As New DataSet
        Dim exForce As Exception
        Try
            Dim objDataOperation As New clsDataOperation

            'S.SANDEEP [ 29 May 2013 ] -- START
            'ENHANCEMENT : ATLAS COPCO WEB CHANGES
            'StrQ = "SELECT " & _
            '       "    reportprivilegeunkid " & _
            '       "FROM hrmsConfiguration..cfuser_reportprivilege " & _
            '       "WHERE reportunkid = '" & intReportId & "' " & _
            '       "AND userunkid = '" & User._Object._Userunkid & "' " & _
            '       "AND companyunkid = '" & Company._Object._Companyunkid & "' "

            If iUserId <= 0 Then iUserId = User._Object._Userunkid
            If iCompanyId <= 0 Then iCompanyId = Company._Object._Companyunkid

            StrQ = "SELECT " & _
                   "    reportprivilegeunkid " & _
                   "FROM hrmsConfiguration..cfuser_reportprivilege " & _
                   "WHERE reportunkid = '" & intReportId & "' " & _
                   "AND userunkid = '" & iUserId & "' " & _
                   "AND companyunkid = '" & iCompanyId & "' "
            'S.SANDEEP [ 29 May 2013 ] -- END

            dsList = objDataOperation.ExecQuery(StrQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & " : " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            If dsList.Tables("List").Rows.Count > 0 Then
                blnFlag = True
            Else
                blnFlag = False
            End If
            Return blnFlag
        Catch ex As Exception
            Call DisplayError.Show("-1", ex.Message, "Is_Report_Assigned", mstrModuleName)
            Return False
        End Try
    End Function
    'S.SANDEEP [ 29 OCT 2012 ] -- END


    'S.SANDEEP [10 AUG 2015] -- START
    'ENHANCEMENT : Aruti SaaS Changes
    Private Function GetReportFilterStringCountryWise(ByVal strDataBaseName As String, ByVal intCountryUnkid As Integer, ByVal intUserUnkid As Integer, ByVal intCompanyUnkid As Integer, ByVal blnIsNotConfig As Boolean) As String
        Dim StrFilterString As String = String.Empty
        Dim strCommonStatutoryReports As String = String.Empty
        Try
            strCommonStatutoryReports = enArutiReport.PensionFundReport & "," & enArutiReport.Tax_Report

            Select Case intCountryUnkid
                Case 51  'CONGO

                Case 112 'KENYA

                    StrFilterString &= "  AND cfreport_master.reportcategoryunkid <> 2 OR (hrmsConfiguration..cfreport_master.reportunkid IN " & _
                            " (SELECT " & strDataBaseName & "..cfreport_master.reportunkid FROM " & strDataBaseName & "..cfreport_master "
                    If blnIsNotConfig Then
                        StrFilterString &= " JOIN " & strDataBaseName & "..cfuser_reportprivilege ON " & strDataBaseName & "..cfuser_reportprivilege.reportunkid = " & strDataBaseName & "..cfreport_master.reportunkid " & _
                                 " AND " & strDataBaseName & "..cfuser_reportprivilege.userunkid = " & intUserUnkid & " AND " & strDataBaseName & "..cfuser_reportprivilege.companyunkid = " & intCompanyUnkid
                    End If

                    'Nilay (15-Dec-2015) -- Start
                    'Enhancement - NEW Statutory Reports P10A, P10B and P10D Reports for Kenya.
                    'StrFilterString &= " WHERE " & strDataBaseName & "..cfreport_master.reportunkid IN " & _
                    '             " (" & strCommonStatutoryReports & "," & enArutiReport.PAYE_P11_Report & " ," & enArutiReport.KN_NHIFReport & "," & enArutiReport.NSSF_FORM_5 & "," & enArutiReport.P9A_Report & "," & enArutiReport.P10_Report & "," & enArutiReport.ITax_Form_B_Report & "," & enArutiReport.HELB_REPORT & "," & enArutiReport.ITax_Form_J_Report & " ) ) AND cfreport_master.reportcategoryunkid = 2 "
                    StrFilterString &= " WHERE " & strDataBaseName & "..cfreport_master.reportunkid IN " & _
                                 " (" & strCommonStatutoryReports & "," & enArutiReport.PAYE_P11_Report & " ," & enArutiReport.KN_NHIFReport & "," & _
                                  enArutiReport.NSSF_FORM_5 & "," & enArutiReport.P9A_Report & "," & enArutiReport.P10_Report & "," & _
                                  enArutiReport.ITax_Form_B_Report & "," & enArutiReport.HELB_REPORT & "," & _
                                  enArutiReport.ITax_Form_J_Report & "," & enArutiReport.P10A_P10D_Report & "," & _
                                  enArutiReport.ITax_Form_C_Report & "," & enArutiReport.NITA_Report & "," & enArutiReport.Affordable_Housing_Levy_Form_M_Report & " ) ) AND cfreport_master.reportcategoryunkid = 2 "

                    'Hemant (18 Aug 2023) -- [enArutiReport.Affordable_Housing_Levy_Form_M]
                    'Sohail (29 Nov 2021) - [NITA_Report]
                    'Nilay (11 Apr 2017) -- [enArutiReport.ITax_Form_C_Report]
                    'Nilay (15-Dec-2015) -- End

                    If blnIsNotConfig = True Then
                        StrFilterString &= "AND " & strDataBaseName & "..cfuser_reportprivilege.userunkid = " & intUserUnkid & " AND " & strDataBaseName & "..cfuser_reportprivilege.companyunkid = " & intCompanyUnkid & " "
                    End If


                Case 208 'TANZANIA

                    StrFilterString &= "  AND cfreport_master.reportcategoryunkid <> 2 OR (" & strDataBaseName & "..cfreport_master.reportunkid IN " & _
                                 " (SELECT " & strDataBaseName & "..cfreport_master.reportunkid FROM " & strDataBaseName & "..cfreport_master  "

                    If blnIsNotConfig Then
                        StrFilterString &= " JOIN " & strDataBaseName & "..cfuser_reportprivilege ON " & strDataBaseName & "..cfuser_reportprivilege.reportunkid = " & strDataBaseName & "..cfreport_master.reportunkid " & _
                                    " AND " & strDataBaseName & "..cfuser_reportprivilege.userunkid = " & intUserUnkid & " AND " & strDataBaseName & "..cfuser_reportprivilege.companyunkid = " & intCompanyUnkid
                    End If
                    'Sohail (20 Dec 2017) -- Start
                    'Ref. # 138 - Changes in PSPF and NHIF report make available for Tanzania with some changes as per format given in email in 70.1.
                    'StrFilterString &= "  WHERE " & strDataBaseName & "..cfreport_master.reportunkid IN " & _
                    '                 " (" & strCommonStatutoryReports & "," & enArutiReport.IncomeTaxDeductionP9 & "," & enArutiReport.ZSSF_FORM & "," & _
                    '                        enArutiReport.IncomeTaxFormP10 & "," & enArutiReport.PPFContribution & "," & enArutiReport.SDL_Statutory_Report & ", " & _
                    '                        enArutiReport.LAPFContribution & "," & enArutiReport.PSPFContribution & "," & enArutiReport.GEPFContribution & "," & enArutiReport.NSSF_FORM_5 & "," & enArutiReport.EPF_PeriodWise_Report & ", " & _
                    '                        enArutiReport.WCF_REPORT & "," & enArutiReport.PE_Form_Report & _
                    '                 " )) AND cfreport_master.reportcategoryunkid = 2 "
                    StrFilterString &= "  WHERE " & strDataBaseName & "..cfreport_master.reportunkid IN " & _
                                 " (" & strCommonStatutoryReports & "," & enArutiReport.IncomeTaxDeductionP9 & "," & enArutiReport.ZSSF_FORM & "," & _
                                        enArutiReport.IncomeTaxFormP10 & "," & enArutiReport.PPFContribution & "," & enArutiReport.SDL_Statutory_Report & ", " & _
                                        enArutiReport.LAPFContribution & "," & enArutiReport.PSPFContribution & "," & enArutiReport.GEPFContribution & "," & enArutiReport.NSSF_FORM_5 & "," & enArutiReport.EPF_PeriodWise_Report & ", " & _
                                      enArutiReport.WCF_REPORT & "," & enArutiReport.PE_Form_Report & "," & enArutiReport.NHIF_Report_TZ & ", " & enArutiReport.PSSSF_Report & ", " & _
                                      enArutiReport.Skills_And_Development_Levy_Monthly_Return & "," & enArutiReport.Statement_And_Payment_Of_Tax_Withheld_For_Employees & _
                                      "," & enArutiReport.WCF_Detail_Report & _
                                 " )) AND cfreport_master.reportcategoryunkid = 2 "

                    'Hemant (07 Jul 2023) -- [enArutiReport.WCF_Detail_Report]
                    'Hemant (28 Aug 2020) -- [enArutiReport.Skills_And_Development_Levy_Monthly_Return & "," & enArutiReport.Statement_And_Payment_Of_Tax_Withheld_For_Employees]
                    'Sohail (20 Dec 2017) -- End


                    'Hemant (21 Nov 2018) -- [PSSSF_Report]
                    'S.SANDEEP [22-MAR-2017] -- START
                    'ISSUE/ENHANCEMENT : Tanapa PE Forms Statutory Report
                    'enArutiReport.PE_Form_Report -- ADDED
                    'S.SANDEEP [22-MAR-2017] -- END


                    If blnIsNotConfig = True Then
                        StrFilterString &= "AND " & strDataBaseName & "..cfuser_reportprivilege.userunkid = " & intUserUnkid & " AND " & strDataBaseName & "..cfuser_reportprivilege.companyunkid = " & intCompanyUnkid & " "
                    End If


                Case 219 'UGANDA

                    StrFilterString &= "  AND cfreport_master.reportcategoryunkid <> 2 OR (" & strDataBaseName & "..cfreport_master.reportunkid IN " & _
                                       " (SELECT " & strDataBaseName & "..cfreport_master.reportunkid FROM " & strDataBaseName & "..cfreport_master "

                    If blnIsNotConfig Then
                        StrFilterString &= " JOIN " & strDataBaseName & "..cfuser_reportprivilege ON " & strDataBaseName & "..cfuser_reportprivilege.reportunkid = " & strDataBaseName & "..cfreport_master.reportunkid " & _
                                           " AND " & strDataBaseName & "..cfuser_reportprivilege.userunkid = " & intUserUnkid & " AND " & strDataBaseName & "..cfuser_reportprivilege.companyunkid = " & intCompanyUnkid
                    End If

                    StrFilterString &= " WHERE " & strDataBaseName & "..cfreport_master.reportunkid IN " & _
                                       " (" & strCommonStatutoryReports & "," & enArutiReport.NSSF_FORM_5 & "  ) ) AND cfreport_master.reportcategoryunkid = 2  "

                    If blnIsNotConfig = True Then
                        StrFilterString &= " AND " & strDataBaseName & "..cfuser_reportprivilege.userunkid = " & intUserUnkid & " AND " & strDataBaseName & "..cfuser_reportprivilege.companyunkid = " & intCompanyUnkid & " "
                    End If


                Case 236 'ZAMBIA

                    StrFilterString &= " AND cfreport_master.reportcategoryunkid <> 2 OR (" & strDataBaseName & "..cfreport_master.reportunkid IN " & _
                                       " (SELECT " & strDataBaseName & "..cfreport_master.reportunkid FROM " & strDataBaseName & "..cfreport_master "

                    If blnIsNotConfig Then
                        StrFilterString &= " JOIN " & strDataBaseName & "..cfuser_reportprivilege ON " & strDataBaseName & "..cfuser_reportprivilege.reportunkid = " & strDataBaseName & "..cfreport_master.reportunkid " & _
                                           " AND " & strDataBaseName & "..cfuser_reportprivilege.userunkid = " & intUserUnkid & " AND " & strDataBaseName & "..cfuser_reportprivilege.companyunkid = " & intCompanyUnkid
                    End If
                    StrFilterString &= " WHERE " & strDataBaseName & "..cfreport_master.reportunkid IN " & _
                                       " (" & strCommonStatutoryReports & "," & enArutiReport.Za_ITF_P16_Report & "," & enArutiReport.Za_Monthly_AMD_Report & "," _
                                   & enArutiReport.Za_AnnualPayeReturn & "," & enArutiReport.Za_RemittanceForm & "," & enArutiReport.National_Health_Insurance_Report & "," & enArutiReport.NAPSA_Year_End_Report & "," & enArutiReport.PAYE_Year_End_Report & "," & _
                                   enArutiReport.PAYE_Certification_Statement & " ) ) AND cfreport_master.reportcategoryunkid = 2  "
                    'Hemant (28 Mar 2022) -- [PAYE_Certification_Statement]
                    'Sohail (28 Mar 2022) - [PAYE_Year_End_Report]
                    'Sohail (17 Mar 2022) - [NAPSA_Year_End_Report]
                    'Sohail (30 Jun 2021) - [National_Health_Insurance_Report]
                    If blnIsNotConfig = True Then
                        StrFilterString &= " AND " & strDataBaseName & "..cfuser_reportprivilege.userunkid = " & intUserUnkid & " AND " & strDataBaseName & "..cfuser_reportprivilege.companyunkid = " & intCompanyUnkid & " "
                    End If


                Case 198 'SRI LANKA

                    StrFilterString &= "  AND cfreport_master.reportcategoryunkid <> 2 OR (" & strDataBaseName & "..cfreport_master.reportunkid IN " & _
                                       " (SELECT " & strDataBaseName & "..cfreport_master.reportunkid FROM " & strDataBaseName & "..cfreport_master "

                    If blnIsNotConfig Then
                        StrFilterString &= " JOIN " & strDataBaseName & "..cfuser_reportprivilege ON " & strDataBaseName & "..cfuser_reportprivilege.reportunkid = " & strDataBaseName & "..cfreport_master.reportunkid " & _
                                           " AND " & strDataBaseName & "..cfuser_reportprivilege.userunkid = " & intUserUnkid & " AND " & strDataBaseName & "..cfuser_reportprivilege.companyunkid = " & intCompanyUnkid
                    End If
                    StrFilterString &= " WHERE " & strDataBaseName & "..cfreport_master.reportunkid IN " & _
                                       " (" & strCommonStatutoryReports & "," & enArutiReport.EPF_Contribution_Report & " ) ) AND cfreport_master.reportcategoryunkid = 2  "

                    If blnIsNotConfig = True Then
                        StrFilterString &= " AND " & strDataBaseName & "..cfuser_reportprivilege.userunkid = " & intUserUnkid & " AND " & strDataBaseName & "..cfuser_reportprivilege.companyunkid = " & intCompanyUnkid & " "
                    End If


                Case 84  'GHANA

                    StrFilterString &= "  AND cfreport_master.reportcategoryunkid <> 2 OR (" & strDataBaseName & "..cfreport_master.reportunkid IN " & _
                                       " (SELECT " & strDataBaseName & "..cfreport_master.reportunkid FROM " & strDataBaseName & "..cfreport_master "

                    If blnIsNotConfig Then
                        StrFilterString &= " JOIN " & strDataBaseName & "..cfuser_reportprivilege ON " & strDataBaseName & "..cfuser_reportprivilege.reportunkid = " & strDataBaseName & "..cfreport_master.reportunkid " & _
                                           " AND " & strDataBaseName & "..cfuser_reportprivilege.userunkid = " & intUserUnkid & " AND " & strDataBaseName & "..cfuser_reportprivilege.companyunkid = " & intCompanyUnkid
                    End If
                    StrFilterString &= " WHERE " & strDataBaseName & "..cfreport_master.reportunkid IN " & _
                                       " (" & strCommonStatutoryReports & "," & enArutiReport.Monthly_Tax_Income_Report & "," & enArutiReport.MONTHLY_TAX_DEDUCATION_SCHEDULE_REPORT & " ) ) AND cfreport_master.reportcategoryunkid = 2  "

                    If blnIsNotConfig = True Then
                        StrFilterString &= " AND " & strDataBaseName & "..cfuser_reportprivilege.userunkid = " & intUserUnkid & " AND " & strDataBaseName & "..cfuser_reportprivilege.companyunkid = " & intCompanyUnkid & " "
                    End If


                Case 129 'MALAWI

                    StrFilterString &= "  AND cfreport_master.reportcategoryunkid <> 2 OR (" & strDataBaseName & "..cfreport_master.reportunkid IN " & _
                                       " (SELECT " & strDataBaseName & "..cfreport_master.reportunkid FROM " & strDataBaseName & "..cfreport_master "

                    If blnIsNotConfig Then
                        StrFilterString &= " JOIN " & strDataBaseName & "..cfuser_reportprivilege ON " & strDataBaseName & "..cfuser_reportprivilege.reportunkid = " & strDataBaseName & "..cfreport_master.reportunkid " & _
                                           " AND " & strDataBaseName & "..cfuser_reportprivilege.userunkid = " & intUserUnkid & " AND " & strDataBaseName & "..cfuser_reportprivilege.companyunkid = " & intCompanyUnkid
                    End If
                    StrFilterString &= " WHERE " & strDataBaseName & "..cfreport_master.reportunkid IN " & _
                                       " (" & strCommonStatutoryReports & "," & enArutiReport.INCOME_TAX_DIVISION_P12_REPORT & "," & enArutiReport.PAYE_DEDUCTION_FORM_P9_Report & "," & enArutiReport.TEVET_LEVY_REPORT & "," & enArutiReport.Employee_Leaving_Certificate & "  ) ) AND cfreport_master.reportcategoryunkid = 2  "

                    'Pinkal (12-May-2018) -- Enhancement - Ref # 213 [Employee_Leaving_Certificate]
                    'Gajanan (04 May 2018) - [TEVET_LEVY_REPORT]
                    'Sohail (18 Apr 2018) - [PAYE_DEDUCTION_FORM_P9_Report]

                    If blnIsNotConfig = True Then
                        StrFilterString &= " AND " & strDataBaseName & "..cfuser_reportprivilege.userunkid = " & intUserUnkid & " AND " & strDataBaseName & "..cfuser_reportprivilege.companyunkid = " & intCompanyUnkid & " "
                    End If

                    'S.SANDEEP [13-OCT-2017] -- START
                    'ISSUE/ENHANCEMENT : REF-ID # 81, 82
                Case 137 'MAURITIUS

                    StrFilterString &= "  AND cfreport_master.reportcategoryunkid <> 2 OR (" & strDataBaseName & "..cfreport_master.reportunkid IN " & _
                                       " (SELECT " & strDataBaseName & "..cfreport_master.reportunkid FROM " & strDataBaseName & "..cfreport_master "

                    If blnIsNotConfig Then
                        StrFilterString &= " JOIN " & strDataBaseName & "..cfuser_reportprivilege ON " & strDataBaseName & "..cfuser_reportprivilege.reportunkid = " & strDataBaseName & "..cfreport_master.reportunkid " & _
                                           " AND " & strDataBaseName & "..cfuser_reportprivilege.userunkid = " & intUserUnkid & " AND " & strDataBaseName & "..cfuser_reportprivilege.companyunkid = " & intCompanyUnkid
                    End If
                    StrFilterString &= " WHERE " & strDataBaseName & "..cfreport_master.reportunkid IN " & _
                                       " (" & strCommonStatutoryReports & "," & enArutiReport.Mauritius_PAYE_Report & "," & enArutiReport.Mauritius_NPF_Report & "," & enArutiReport.PAYE_NPF_NSF_Report & " ) ) AND cfreport_master.reportcategoryunkid = 2  "
                    'Sohail (29 Mar 2018) -- [PAYE_NPF_NSF_Report]

                    If blnIsNotConfig = True Then
                        StrFilterString &= " AND " & strDataBaseName & "..cfuser_reportprivilege.userunkid = " & intUserUnkid & " AND " & strDataBaseName & "..cfuser_reportprivilege.companyunkid = " & intCompanyUnkid & " "
                    End If
                    'S.SANDEEP [13-OCT-2017] -- END


                    'Sohail (23 Jan 2021) -- Start
                    'Netis Gabon Enhancement : - New statutory report CFP Report.
                Case 80 'GABON

                    StrFilterString &= "  AND cfreport_master.reportcategoryunkid <> 2 OR (" & strDataBaseName & "..cfreport_master.reportunkid IN " & _
                                      " (SELECT " & strDataBaseName & "..cfreport_master.reportunkid FROM " & strDataBaseName & "..cfreport_master "

                    If blnIsNotConfig Then
                        StrFilterString &= " JOIN " & strDataBaseName & "..cfuser_reportprivilege ON " & strDataBaseName & "..cfuser_reportprivilege.reportunkid = " & strDataBaseName & "..cfreport_master.reportunkid " & _
                                           " AND " & strDataBaseName & "..cfuser_reportprivilege.userunkid = " & intUserUnkid & " AND " & strDataBaseName & "..cfuser_reportprivilege.companyunkid = " & intCompanyUnkid
                    End If
                    StrFilterString &= " WHERE " & strDataBaseName & "..cfreport_master.reportunkid IN " & _
                                       " (" & strCommonStatutoryReports & ", " & enArutiReport.CFP_Report & ", " & enArutiReport.IRPP_FNH_CFP_Report & ", " & enArutiReport.Rapport_Trimestriel_CNAMGS & "," & enArutiReport.Gabon_Declaration_Trimestrielle_Salaires & "," & _
                                            enArutiReport.ID20_ETAT_DE_LA_MASSE_SALARIALE_Report & "," & enArutiReport.ID21_DECLARATION_ET_TRAITEMENT_DES_SALAIRES_Report & "," & enArutiReport.ID22_DECLARATION_ET_TRAITEMENT_DES_SALAIRES_Report & ") ) AND cfreport_master.reportcategoryunkid = 2  "
                    'Pinkal (10-Dec-2021)-- [ID22_DECLARATION_ET_TRAITEMENT_DES_SALAIRES_Report]
                    'Sohail (09 Dec 2021) - [ID21_DECLARATION_ET_TRAITEMENT_DES_SALAIRES_Report]
                    'Sohail (03 Dec 2021) - [ID20_ETAT_DE_LA_MASSE_SALARIALE_Report]
                    'Pinkal (29-Nov-2021)-- Netis Gabon Statutory Report.[Gabon_Declaration_Trimestrielle_Salaires]
                    'Sohail (27 Oct 2021) - [Rapport_Trimestriel_CNAMGS]

                    If blnIsNotConfig = True Then
                        StrFilterString &= " AND " & strDataBaseName & "..cfuser_reportprivilege.userunkid = " & intUserUnkid & " AND " & strDataBaseName & "..cfuser_reportprivilege.companyunkid = " & intCompanyUnkid & " "
                    End If
                    'Sohail (23 Jan 2021) -- End

                Case Else

                    StrFilterString &= "  AND cfreport_master.reportcategoryunkid <> 2 OR (" & strDataBaseName & "..cfreport_master.reportunkid IN " & _
                                       " (SELECT " & strDataBaseName & "..cfreport_master.reportunkid FROM " & strDataBaseName & "..cfreport_master "

                    If blnIsNotConfig Then
                        StrFilterString &= " JOIN " & strDataBaseName & "..cfuser_reportprivilege ON " & strDataBaseName & "..cfuser_reportprivilege.reportunkid = " & strDataBaseName & "..cfreport_master.reportunkid " & _
                                           " AND " & strDataBaseName & "..cfuser_reportprivilege.userunkid = " & intUserUnkid & " AND " & strDataBaseName & "..cfuser_reportprivilege.companyunkid = " & intCompanyUnkid
                    End If

                    StrFilterString &= " WHERE " & strDataBaseName & "..cfreport_master.reportunkid IN " & _
                                       " (" & strCommonStatutoryReports & " ) ) AND cfreport_master.reportcategoryunkid = 2  "

                    If blnIsNotConfig = True Then
                        StrFilterString &= " AND " & strDataBaseName & "..cfuser_reportprivilege.userunkid = " & intUserUnkid & " AND " & strDataBaseName & "..cfuser_reportprivilege.companyunkid = " & intCompanyUnkid & " "
                    End If


            End Select
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: GetReportFilterStringCountryWise; Module Name: " & mstrModuleName)
        Finally
        End Try
        Return StrFilterString
    End Function
    'S.SANDEEP [10 AUG 2015] -- END


    Public Function UpdateCategoryValue(ByVal intCategoryId As Integer, _
                                        ByVal strvalue1 As String, _
                                        ByVal strvalue2 As String, _
                                        ByVal intUserId As Integer, _
                                        ByVal intCompanyId As Integer, _
                                        ByVal strIP As String, _
                                        ByVal strHost As String, _
                                        ByVal blnIsWeb As Boolean, _
                                        ByVal strScreenName As String) As Boolean
        Dim StrQ As String = ""
        Dim exForce As Exception = Nothing
        Try
            Using objDataOp As New clsDataOperation
                objDataOp.BindTransaction()

                objDataOp.AddParameter("@reportcategoryunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intCategoryId.ToString)
                objDataOp.AddParameter("@name1", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, strvalue1.ToString)
                objDataOp.AddParameter("@name2", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, strvalue2.ToString)

                StrQ = "UPDATE hrmsConfiguration..cfreport_category SET " & _
                       " name1 = @name1 " & _
                       ",name2 = @name2 " & _
                       "WHERE reportcategoryunkid = @reportcategoryunkid "

                Call objDataOp.ExecNonQuery(StrQ)

                If objDataOp.ErrorMessage <> "" Then
                    objDataOp.ReleaseTransaction(False)
                    exForce = New Exception(objDataOp.ErrorNumber & ": " & objDataOp.ErrorMessage)
                    Throw exForce
                End If
                Dim objCommonATLog As New clsCommonATLog
                With objCommonATLog
                    ._AuditDate = Now
                    ._AuditUserId = intUserId
                    ._ClientIP = strIP
                    ._CompanyUnkid = intCompanyId
                    ._FormName = strScreenName
                    ._FromWeb = blnIsWeb
                    ._HostName = strHost
                    ._LoginEmployeeUnkid = 0
                End With
                If objCommonATLog.Insert_AtLog(objDataOp, enAuditType.EDIT, "cfreport_category", "reportcategoryunkid", intCategoryId, True, intUserId, intCompanyId) = False Then
                    objDataOp.ReleaseTransaction(False)
                    exForce = New Exception(objDataOp.ErrorNumber & ": " & objDataOp.ErrorMessage)
                    Throw exForce
                End If

                objDataOp.ReleaseTransaction(True)
            End Using
            Return True
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: UpdateCategoryValue; Module Name: " & mstrModuleName)
        Finally
        End Try
    End Function

End Class