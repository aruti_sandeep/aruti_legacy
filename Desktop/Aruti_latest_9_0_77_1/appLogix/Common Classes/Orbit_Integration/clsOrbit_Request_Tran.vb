﻿Option Strict On

#Region " Import "

Imports eZeeCommonLib
Imports System
Imports System.IO
Imports System.Net
Imports System.Text
Imports Newtonsoft.Json
Imports Newtonsoft.Json.Linq
Imports System.Globalization
Imports System.Drawing.Imaging
Imports Newtonsoft.Json.Serialization
Imports System.Reflection

#End Region

Public Class clsOrbit_Request_Tran

#Region " Private Variables "

    Private Shared ReadOnly mstrModuleName As String = "clsOrbit_Request_Tran"
    Private mstrtranguid As String = String.Empty
    Private mintEmployeeunkid As Integer = 0
    Private mdtRequestdate As DateTime = Nothing
    Private mintRequesttypeid As Integer = 0
    'Sohail (25 Apr 2020) -- Start
    'FDRC Enhancement # : Orbit Integrtion for Bulk Payment Process.
    Private mintPeriodunkid As Integer = 0
    'Sohail (25 Apr 2020) -- End
    Private mstrReferenceno As String = String.Empty
    Private mstrCustomer_Number As String = String.Empty
    Private mstrCustomerStatus As String = String.Empty
    Private mstrAccountStatus As String = String.Empty
    Private mstrPrimaryAccountNumber As String = String.Empty
    Private mstrPostedData As String = String.Empty
    Private mstrResponseData As String = String.Empty
    Private mblnIsError As Boolean = False
    Private mstrErrorDescription As String = String.Empty
    Private mintReqestModuleId As Integer = 0
    Private mstrRequestMst_Number As String = String.Empty
    Private mblnIssrvrequest As Boolean = False
    Private mstrIPAddress As String = String.Empty
    Private mblnIsProcessed As Boolean = False
    Private mstrMessage As String = String.Empty
    Private mstrProductCode As String = ""

#End Region

#Region " Enum "

    Public Enum enViewType
        VW_NEW = 1
        VW_FAILED = 2
        VW_SUCCESS = 3
    End Enum

#End Region

#Region " Properties "

    Public Property _Tranguid() As String
        Get
            Return mstrtranguid
        End Get
        Set(ByVal value As String)
            mstrtranguid = value
        End Set
    End Property

    Public Property _Employeeunkid() As Integer
        Get
            Return mintEmployeeunkid
        End Get
        Set(ByVal value As Integer)
            mintEmployeeunkid = value
        End Set
    End Property

    Public Property _Requestdate() As DateTime
        Get
            Return mdtRequestdate
        End Get
        Set(ByVal value As DateTime)
            mdtRequestdate = value
        End Set
    End Property

    Public Property _Requesttypeid() As Integer
        Get
            Return mintRequesttypeid
        End Get
        Set(ByVal value As Integer)
            mintRequesttypeid = value
        End Set
    End Property

    'Sohail (25 Apr 2020) -- Start
    'FDRC Enhancement # : Orbit Integrtion for Bulk Payment Process.
    Public Property _Periodunkid() As Integer
        Get
            Return mintPeriodunkid
        End Get
        Set(ByVal value As Integer)
            mintPeriodunkid = value
        End Set
    End Property
    'Sohail (25 Apr 2020) -- End

    Public Property _Referenceno() As String
        Get
            Return mstrReferenceno
        End Get
        Set(ByVal value As String)
            mstrReferenceno = value
        End Set
    End Property

    Public Property _Customer_Number() As String
        Get
            Return mstrCustomer_Number
        End Get
        Set(ByVal value As String)
            mstrCustomer_Number = value
        End Set
    End Property

    Public Property _CustomerStatus() As String
        Get
            Return mstrCustomerStatus
        End Get
        Set(ByVal value As String)
            mstrCustomerStatus = value
        End Set
    End Property

    Public Property _AccountStatus() As String
        Get
            Return mstrAccountStatus
        End Get
        Set(ByVal value As String)
            mstrAccountStatus = value
        End Set
    End Property

    Public Property _PrimaryAccountNumber() As String
        Get
            Return mstrPrimaryAccountNumber
        End Get
        Set(ByVal value As String)
            mstrPrimaryAccountNumber = value
        End Set
    End Property

    Public Property _PostedData() As String
        Get
            Return mstrPostedData
        End Get
        Set(ByVal value As String)
            mstrPostedData = value
        End Set
    End Property

    Public Property _ResponseData() As String
        Get
            Return mstrResponseData
        End Get
        Set(ByVal value As String)
            mstrResponseData = value
        End Set
    End Property

    Public Property _IsError() As Boolean
        Get
            Return mblnIsError
        End Get
        Set(ByVal value As Boolean)
            mblnIsError = value
        End Set
    End Property

    Public Property _ErrorDescription() As String
        Get
            Return mstrErrorDescription
        End Get
        Set(ByVal value As String)
            mstrErrorDescription = value
        End Set
    End Property

    Public Property _ReqestModuleId() As Integer
        Get
            Return mintReqestModuleId
        End Get
        Set(ByVal value As Integer)
            mintReqestModuleId = value
        End Set
    End Property

    Public Property _RequestMst_Number() As String
        Get
            Return mstrRequestMst_Number
        End Get
        Set(ByVal value As String)
            mstrRequestMst_Number = value
        End Set
    End Property

    Public Property _IsServiceRequest() As Boolean
        Get
            Return mblnIssrvrequest
        End Get
        Set(ByVal value As Boolean)
            mblnIssrvrequest = value
        End Set
    End Property

    Public Property _IPAddress() As String
        Get
            Return mstrIPAddress
        End Get
        Set(ByVal value As String)
            mstrIPAddress = value
        End Set
    End Property

    Public Property _ProductCode() As String
        Get
            Return mstrProductCode
        End Get
        Set(ByVal value As String)
            mstrProductCode = value
        End Set
    End Property

#End Region

#Region " Private/Public Methods "

    Public Function Insert(Optional ByVal objDataOpr As clsDataOperation = Nothing) As Boolean
        Dim StrQ As String = String.Empty
        Dim exForce As Exception = Nothing
        Dim objDataOperation As clsDataOperation
        If objDataOpr IsNot Nothing Then
            objDataOperation = objDataOpr
        Else
            objDataOperation = New clsDataOperation
        End If
        objDataOperation.ClearParameters()
        Try
            StrQ = "INSERT INTO hrorbit_request_tran " & _
                   "( " & _
                   "     tranguid " & _
                   "    ,employeeunkid " & _
                   "    ,requestdate " & _
                   "    ,requesttypeid " & _
                   "    ,periodunkid " & _
                   "    ,referenceno " & _
                   "    ,customer_number " & _
                   "    ,customerstatus " & _
                   "    ,accountstatus " & _
                   "    ,primaryactnumber " & _
                   "    ,posteddata " & _
                   "    ,responsedata " & _
                   "    ,iserror " & _
                   "    ,error_description " & _
                   "    ,reqmoduleid " & _
                   "    ,reqmst_number " & _
                   "    ,issrvrequest " & _
                   "    ,ipaddress " & _
                   "    ,isprocessed " & _
                   "    ,productcode " & _
                   "    ,isassigned " & _
                   ") " & _
                   "VALUES " & _
                   "( " & _
                   "     @tranguid " & _
                   "    ,@employeeunkid " & _
                   "    ,@requestdate " & _
                   "    ,@requesttypeid " & _
                   "    ,@periodunkid " & _
                   "    ,@referenceno " & _
                   "    ,@customer_number " & _
                   "    ,@customerstatus " & _
                   "    ,@accountstatus " & _
                   "    ,@primaryactnumber " & _
                   "    ,@posteddata " & _
                   "    ,@responsedata " & _
                   "    ,@iserror " & _
                   "    ,@error_description " & _
                   "    ,@reqmoduleid " & _
                   "    ,@reqmst_number " & _
                   "    ,@issrvrequest " & _
                   "    ,@ipaddress " & _
                   "    ,@isprocessed " & _
                   "    ,@productcode " & _
                   "    ,CAST(0 AS BIT) " & _
                   ") "

            objDataOperation.AddParameter("@tranguid", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrtranguid)
            objDataOperation.AddParameter("@employeeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintEmployeeunkid)
            objDataOperation.AddParameter("@requestdate", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, mdtRequestdate)
            objDataOperation.AddParameter("@requesttypeid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintRequesttypeid)
            'Sohail (25 Apr 2020) -- Start
            'FDRC Enhancement # : Orbit Integrtion for Bulk Payment Process.
            objDataOperation.AddParameter("@periodunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintPeriodunkid)
            'Sohail (25 Apr 2020) -- End
            objDataOperation.AddParameter("@referenceno", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrReferenceno)
            objDataOperation.AddParameter("@customer_number", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrCustomer_Number)
            objDataOperation.AddParameter("@customerstatus", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrCustomerStatus)
            objDataOperation.AddParameter("@accountstatus", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrAccountStatus)
            objDataOperation.AddParameter("@primaryactnumber", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrPrimaryAccountNumber)
            objDataOperation.AddParameter("@posteddata", SqlDbType.VarBinary, System.Text.Encoding.Unicode.GetBytes(mstrPostedData).Length, System.Text.Encoding.Unicode.GetBytes(mstrPostedData))
            objDataOperation.AddParameter("@responsedata", SqlDbType.VarBinary, System.Text.Encoding.Unicode.GetBytes(mstrResponseData).Length, System.Text.Encoding.Unicode.GetBytes(mstrResponseData))
            objDataOperation.AddParameter("@iserror", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsError)
            objDataOperation.AddParameter("@error_description", SqlDbType.NVarChar, eZeeDataType.DESC_SIZE, mstrErrorDescription)
            objDataOperation.AddParameter("@reqmoduleid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintReqestModuleId)
            objDataOperation.AddParameter("@reqmst_number", SqlDbType.NVarChar, mstrRequestMst_Number.Length, mstrRequestMst_Number)
            objDataOperation.AddParameter("@issrvrequest", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIssrvrequest)
            objDataOperation.AddParameter("@ipaddress", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrIPAddress)
            objDataOperation.AddParameter("@isprocessed", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsProcessed)
            objDataOperation.AddParameter("@productcode", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrProductCode)

            Call objDataOperation.ExecNonQuery(StrQ)

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            If mstrCustomer_Number.Trim.Length > 0 Then
                StrQ = "SELECT 1 FROM hrorbit_request_tran WHERE customer_number = '" & mstrCustomer_Number & "' AND iserror = 0 AND  requesttypeid IN (1,2) "
                If objDataOperation.RecordCount(StrQ) = 3 Then

                    StrQ = "UPDATE hrorbit_request_tran SET isprocessed = 1 WHERE employeeunkid = '" & mintEmployeeunkid & "' AND requesttypeid IN (1,2) "

                    Call objDataOperation.ExecNonQuery(StrQ)

                    If objDataOperation.ErrorMessage <> "" Then
                        exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                        Throw exForce
                    End If
                End If
            End If

            Return True
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: Insert; Module Name: " & mstrModuleName)
        Finally
        End Try
    End Function

    Public Shared Sub ConvertJsonToDataTable(ByVal jsonString As String, ByRef iData As DataTable)
        Dim jsonLinq = JObject.Parse(jsonString)

        ' Find the first array using Linq
        Dim linqArray = jsonLinq.Descendants().Where(Function(x) TypeOf x Is JArray).First()
        Dim jsonArray = New JArray()
        For Each row As JObject In linqArray.Children(Of JObject)()
            Dim createRow = New JObject()
            For Each column As JProperty In row.Properties()
                ' Only include JValue types
                If TypeOf column.Value Is JValue Then
                    createRow.Add(column.Name, column.Value)
                End If
            Next column
            jsonArray.Add(createRow)
        Next row
        iData = JsonConvert.DeserializeObject(Of DataTable)(jsonArray.ToString())
    End Sub

    Public Shared Sub ConvertJsonToDataset(ByVal jsonString As String, ByRef iData As DataSet)
        Dim jsonLinq = JObject.Parse(jsonString)
        Dim iKeyvlue As IEnumerable(Of KeyValuePair(Of String, Newtonsoft.Json.Linq.JToken))
        iKeyvlue = jsonLinq
        Dim iEnumrator = iKeyvlue.GetEnumerator()
        While (iEnumrator.MoveNext)
            Dim createRow = New JObject()
            Dim iCurr = iEnumrator.Current : Dim strValue As String = ""

            Select Case iCurr.Value.GetType().FullName.ToUpper()
                Case GetType(JArray).FullName.ToString().ToUpper()

                    Dim jsonArray = New JArray()
                    'jsonArray.Add(iCurr.Key.ToString())
                    If DirectCast(DirectCast(iCurr.Value, Newtonsoft.Json.Linq.JToken), Newtonsoft.Json.Linq.JArray).Count > 0 Then
                        Dim linqArray = DirectCast(DirectCast(iCurr.Value, Newtonsoft.Json.Linq.JToken), Newtonsoft.Json.Linq.JArray)
                        For Each row As JObject In linqArray.Children(Of JObject)()
                            createRow = New JObject()
                            For Each column As JProperty In row.Properties()
                                ' Only include JValue types
                                If TypeOf column.Value Is JValue Then
                                    createRow.Add(column.Name, column.Value)
                                End If
                            Next column
                            jsonArray.Add(createRow)
                        Next row
                    End If
                    Dim dt As New DataTable()
                    dt = JsonConvert.DeserializeObject(Of DataTable)(jsonArray.ToString())
                    dt.TableName = iCurr.Key
                    iData.Tables.Add(dt.Copy)

                Case GetType(JValue).FullName.ToString().ToUpper()

                    strValue = CStr(DirectCast(DirectCast(iCurr.Value, Newtonsoft.Json.Linq.JToken), Newtonsoft.Json.Linq.JValue).Value)
                    Dim jsonArray = New JArray()
                    createRow.Add(iCurr.Key, strValue)
                    jsonArray.Add(createRow)
                    Dim dt As New DataTable()
                    dt = JsonConvert.DeserializeObject(Of DataTable)(jsonArray.ToString())
                    dt.TableName = iCurr.Key.ToString()
                    iData.Tables.Add(dt.Copy)

            End Select
        End While
    End Sub

    Private Function GetOrbitParameters(ByVal eOrbitSrvInfo As enOrbitServiceInfo, ByVal intCompanyId As Integer) As Dictionary(Of String, String)
        Dim mDicKeyValues As New Dictionary(Of String, String)
        Dim objCParam As New clsConfigOptions
        Try
            Dim strParamKeys() As String = { _
                                            "IsOrbitIntegrated", _
                                            "_OrbSrv_" & CInt(eOrbitSrvInfo), _
                                            "OrbitUsername", _
                                            "OrbitPassword", _
                                            "OrbitAgentUsername" _
                                            }

            mDicKeyValues = objCParam.GetKeyValue(intCompanyId, strParamKeys)
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: ; Module Name: " & mstrModuleName)
        Finally
            objCParam = Nothing
        End Try
        Return mDicKeyValues
    End Function

    Private Function PostData(ByVal eOrbitSrvInfoURL As String, ByVal objclsObjct As Object, ByRef blnIsError As Boolean) As String
        Dim strResponse As String = String.Empty
        Try
            
            Dim request As HttpWebRequest = CType(WebRequest.Create(eOrbitSrvInfoURL), HttpWebRequest)
            request.Method = "POST"
            request.ContentType = "application/json; charset=utf-8"
            request.Accept = "application/json"
            Using streamWriter = New StreamWriter(request.GetRequestStream())
                streamWriter.Write(JsonConvert.SerializeObject(objclsObjct))
                streamWriter.Flush()
            End Using
            Dim httpResponse = CType(request.GetResponse(), HttpWebResponse)
            Using streamReader = New StreamReader(httpResponse.GetResponseStream())
                strResponse = streamReader.ReadToEnd()
            End Using
            
        Catch wx As WebException
            blnIsError = True
            strResponse = wx.Message
            
        Catch ex As Exception
            blnIsError = True
            strResponse = ex.Message & "; Procedure Name: PostData; Module Name: " & mstrModuleName
            Throw New Exception(ex.Message & "; Procedure Name: PostData; Module Name: " & mstrModuleName)
        Finally
        End Try
        Return strResponse
    End Function

    'Public Function FindCustomers(ByVal intCompanyId As Integer, ByRef o As findCustomersInformation) As String
    '    Dim strResponse As String = String.Empty
    '    Dim mDicKeyValues As New Dictionary(Of String, String)
    '    Try
    '        mDicKeyValues = GetOrbitParameters(enOrbitServiceInfo.ORB_FINDCUSTOMERSINFORMATION, intCompanyId)
    '        If mDicKeyValues.Keys.Count > 0 Then
    '            If CBool(mDicKeyValues("IsOrbitIntegrated")) Then
    '                If mDicKeyValues.ContainsKey("_OrbSrv_" & CInt(enOrbitServiceInfo.ORB_FINDCUSTOMERSINFORMATION)) = False Then Exit Try
    '                Dim strMessageId As String = ""
    '                Dim iCount As Integer = 1
    '                Dim StrQ As String : Dim dsList As New DataSet
    '                'Using objDataOpr As New clsDataOperation
    '                '    While iCount > 0
    '                '        StrQ = "SELECT CONVERT(NVarChar(5), right(replace(convert(varchar, getdate(),114),':',''),5)) + CONVERT(NVarChar(5), right(newid(),5)) "
    '                '        dsList = objDataOpr.ExecQuery(StrQ, "List")
    '                '        If objDataOpr.ErrorMessage <> "" Then
    '                '            Throw New Exception(objDataOpr.ErrorNumber & " : " & objDataOpr.ErrorMessage)
    '                '        End If
    '                '        strMessageId = CStr(dsList.Tables(0).Rows(0)(0))
    '                '        StrQ = "SELECT 1 FROM hrorbit_request_tran WHERE referenceno = '" & strMessageId & "' "
    '                '        iCount = objDataOpr.RecordCount(StrQ)
    '                '        If objDataOpr.ErrorMessage <> "" Then
    '                '            Throw New Exception(objDataOpr.ErrorNumber & " : " & objDataOpr.ErrorMessage)
    '                '        End If
    '                '        If iCount <= 0 Then Exit While
    '                '    End While
    '                'End Using
    '                Dim random As New Random
    '                strMessageId = random.Next(10000, 99999).ToString()
    '                Dim fc As New findCustomersInformation
    '                fc.referenceNo = strMessageId
    '                fc.credentials.username = mDicKeyValues("OrbitUsername")
    '                fc.credentials.password = mDicKeyValues("OrbitPassword")
    '                fc.credentials.agentUsername = mDicKeyValues("OrbitAgentUsername")
    '                fc.firstName = ""
    '                fc.lastName = ""
    '                fc.customerNo = ""
    '                strResponse = JsonConvert.SerializeObject(fc, Formatting.Indented)
    '                o = fc
    '                strResponse = PostData(mDicKeyValues("_OrbSrv_" & CInt(enOrbitServiceInfo.ORB_FINDCUSTOMERSINFORMATION)), fc)
    '                If strResponse.Trim.Length > 0 Then
    '                    'DO SOMETHING
    '                End If
    '            End If
    '        End If
    '    Catch ex As Exception
    '        Throw New Exception(ex.Message & "; Procedure Name: FindCustomers; Module Name: " & mstrModuleName)
    '    Finally
    '    End Try
    '    Return strResponse
    'End Function

    Public Function FindCustomersInformation(ByVal intCompanyId As Integer, ByVal strCustNo As String, ByVal strFName As String, ByVal strLName As String, ByVal intEmployeeId As Integer) As DataTable
        Dim strResponse As String = String.Empty
        Dim mDicKeyValues As New Dictionary(Of String, String)
        Dim dsResponse As New DataSet
        Try
            mDicKeyValues = GetOrbitParameters(enOrbitServiceInfo.ORB_FINDCUSTOMERSINFORMATION, intCompanyId)
            If mDicKeyValues.Keys.Count > 0 Then
                If CBool(mDicKeyValues("IsOrbitIntegrated")) Then
                    If mDicKeyValues.ContainsKey("_OrbSrv_" & CInt(enOrbitServiceInfo.ORB_FINDCUSTOMERSINFORMATION)) = False Then Exit Try
                    Dim strMessageId As String = ""
                    Dim iCount As Integer = 1
                    Dim StrQ As String : Dim dsList As New DataSet
                    Using objDataOpr As New clsDataOperation
                        While iCount > 0
                            StrQ = "SELECT CONVERT(NVarChar(5), right(replace(convert(varchar, getdate(),114),':',''),5)) + CONVERT(NVarChar(5), right(newid(),5)) "
                            dsList = objDataOpr.ExecQuery(StrQ, "List")
                            If objDataOpr.ErrorMessage <> "" Then
                                Throw New Exception(objDataOpr.ErrorNumber & " : " & objDataOpr.ErrorMessage)
                            End If
                            strMessageId = CStr(dsList.Tables(0).Rows(0)(0))
                            StrQ = "SELECT 1 FROM hrorbit_request_tran WHERE referenceno = '" & strMessageId & "' "
                            iCount = objDataOpr.RecordCount(StrQ)
                            If objDataOpr.ErrorMessage <> "" Then
                                Throw New Exception(objDataOpr.ErrorNumber & " : " & objDataOpr.ErrorMessage)
                            End If
                            If iCount <= 0 Then Exit While
                        End While
                    End Using
                    'Dim random As New Random
                    'strMessageId = random.Next(10000, 99999).ToString()
                    Dim fc As New findCustomersInformation
                    fc.referenceNo = strMessageId
                    fc.firstName = strFName
                    fc.lastName = strLName
                    fc.customerNo = strCustNo
                    fc.credentials.username = mDicKeyValues("OrbitUsername")
                    fc.credentials.password = mDicKeyValues("OrbitPassword")
                    fc.credentials.agentUsername = mDicKeyValues("OrbitAgentUsername")

                    'Pinkal (15-Sep-2023) -- Start
                    '(A1X-1292) TRA - Security fixes
                    'strResponse = JsonConvert.SerializeObject(fc, Formatting.Indented, New JsonSerializerSettings With {.DefaultValueHandling = DefaultValueHandling.IgnoreAndPopulate})
                    strResponse = JsonConvert.SerializeObject(fc, Formatting.Indented, New JsonSerializerSettings With {.DefaultValueHandling = DefaultValueHandling.IgnoreAndPopulate, .MaxDepth = 128})
                    'Pinkal (15-Sep-2023) -- End

                    Dim blnIsError As Boolean = False
                    strResponse = PostData(mDicKeyValues("_OrbSrv_" & CInt(enOrbitServiceInfo.ORB_FINDCUSTOMERSINFORMATION)), fc, blnIsError)
                    If blnIsError Then
                        mstrtranguid = Guid.NewGuid.ToString()
                        mintEmployeeunkid = intEmployeeId
                        mdtRequestdate = Now
                        mintRequesttypeid = enOrbitServiceInfo.ORB_FINDCUSTOMERSINFORMATION
                        mstrReferenceno = strMessageId
                        mstrCustomer_Number = ""
                        mstrCustomerStatus = ""
                        mstrAccountStatus = ""
                        mstrPrimaryAccountNumber = ""
                        'Pinkal (15-Sep-2023) -- Start
                        '(A1X-1292) TRA - Security fixes
                        'mstrPostedData = JsonConvert.SerializeObject(fc, Formatting.Indented, New JsonSerializerSettings With {.DefaultValueHandling = DefaultValueHandling.IgnoreAndPopulate})
                        mstrPostedData = JsonConvert.SerializeObject(fc, Formatting.Indented, New JsonSerializerSettings With {.DefaultValueHandling = DefaultValueHandling.IgnoreAndPopulate, .MaxDepth = 128})
                        'Pinkal (15-Sep-2023) -- End
                        mstrResponseData = ""
                        mblnIsError = blnIsError
                        mstrErrorDescription = strResponse
                        mintReqestModuleId = 0
                        mstrRequestMst_Number = ""
                        mblnIssrvrequest = False
                        mstrIPAddress = getIP()
                        mblnIsProcessed = False
                        If Insert() = False Then
                            Throw New Exception(strResponse)
                        End If
                    Else
                        If strResponse.Trim.Length > 0 Then
                            Dim strErrMsg As String = ""
                            ConvertJsonToDataset(strResponse, dsResponse)

                            If dsResponse IsNot Nothing AndAlso dsResponse.Tables.Count > 0 Then
                                If dsResponse.Tables.Contains("SystemErrorCode") Then
                                    strErrMsg = dsResponse.Tables("SystemErrorCode").Rows(0)("SystemErrorCode").ToString()
                                    blnIsError = True
                                End If
                                If dsResponse.Tables.Contains("SystemErrorMessage") Then
                                    If strErrMsg.Trim.Length > 0 Then
                                        strErrMsg &= " : " & dsResponse.Tables("SystemErrorMessage").Rows(0)("SystemErrorMessage").ToString()
                                    Else
                                        strErrMsg = dsResponse.Tables("SystemErrorMessage").Rows(0)("SystemErrorMessage").ToString()
                                    End If
                                    blnIsError = True
                                End If
                            End If
                            mstrtranguid = Guid.NewGuid.ToString()
                            mintEmployeeunkid = intEmployeeId
                            mdtRequestdate = Now
                            mintRequesttypeid = enOrbitServiceInfo.ORB_CREATECUSTOMER
                            mstrReferenceno = strMessageId
                            mstrCustomer_Number = ""
                            mstrCustomerStatus = ""
                            mstrAccountStatus = ""
                            mstrPrimaryAccountNumber = ""
                            'Pinkal (15-Sep-2023) -- Start
                            '(A1X-1292) TRA - Security fixes
                            'mstrPostedData = JsonConvert.SerializeObject(fc, Formatting.Indented, New JsonSerializerSettings With {.DefaultValueHandling = DefaultValueHandling.IgnoreAndPopulate})
                            mstrPostedData = JsonConvert.SerializeObject(fc, Formatting.Indented, New JsonSerializerSettings With {.DefaultValueHandling = DefaultValueHandling.IgnoreAndPopulate, .MaxDepth = 128})
                            'Pinkal (15-Sep-2023) -- End

                            mstrResponseData = strResponse
                            mblnIsError = blnIsError
                            mstrErrorDescription = strErrMsg
                            mintReqestModuleId = 0
                            mstrRequestMst_Number = ""
                            mblnIssrvrequest = False
                            mstrIPAddress = getIP()
                            mblnIsProcessed = False

                            If Insert() = False Then
                                Throw New Exception(strResponse)
                            End If
                        End If
                    End If
                End If
            End If
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: FindCustomers; Module Name: " & mstrModuleName)
        Finally
        End Try
        Return dsResponse.Tables(0)
    End Function

    Public Function CreateCustomer(ByVal intCompanyId As Integer, _
                                   ByVal intEmployeeId As Integer, _
                                   ByVal blnImgInDB As Boolean, _
                                   ByVal ArutiSelfServiceURL As String, _
                                   ByVal PhotoPath As String, _
                                   ByRef xMsg As String, _
                                   ByRef blnIsErrorOnRequest As Boolean) As String 'S.SANDEEP |30-MAY-2020| -- START {mblnIsErrorOnRequest} -- END
        Dim strResponse As String = String.Empty
        Dim mDicKeyValues As New Dictionary(Of String, String)
        Try
            mstrMessage = ""
            mDicKeyValues = GetOrbitParameters(enOrbitServiceInfo.ORB_CREATECUSTOMER, intCompanyId)
            If mDicKeyValues.Keys.Count > 0 Then
                If CBool(mDicKeyValues("IsOrbitIntegrated")) Then
                    If mDicKeyValues.ContainsKey("_OrbSrv_" & CInt(enOrbitServiceInfo.ORB_CREATECUSTOMER)) = False Then Exit Try

                    Dim objEmp As New clsEmployee_Master

                    objEmp._Companyunkid = intCompanyId
                    objEmp._blnImgInDb = blnImgInDB
                    objEmp._Employeeunkid(Now.Date) = intEmployeeId

                    Dim objIdTran As New clsIdentity_tran
                    Dim mdtTran As New DataTable
                    objIdTran._EmployeeUnkid = intEmployeeId
                    mdtTran = objIdTran._DataList
                    objIdTran = Nothing

                    If mdtTran.Rows.Count <= 0 Then
                        mstrMessage = Language.getMessage(mstrModuleName, 2, "Sorry, No identity is defined for this employee. Please define identity in order to create customer")
                        Exit Try
                    End If
                    If mdtTran.Rows.Count > 0 Then
                        Dim dr As DataRow() = mdtTran.Select("isdefault = 1")
                        If dr.Length <= 0 Then dr = mdtTran.Select("isdefault = 0")
                        If CStr(dr(0)("identity_no")).Trim().Length < 10 Then
                            mstrMessage = Language.getMessage(mstrModuleName, 3, "Sorry, Identification number cannot be less than 10 digits.")
                            Exit Try
                        End If
                        If IsDBNull(dr(0)("expiry_date")) = True Then
                            mstrMessage = Language.getMessage(mstrModuleName, 4, "Sorry, Expiry date is mandatory information. Please provide expiry date.")
                            Exit Try
                        End If
                    End If

                    If objEmp._Present_Mobile.Trim().Length < 12 Then
                        mstrMessage = Language.getMessage(mstrModuleName, 5, "Sorry, Present Moblie number cannot be less than 12 digits.")
                        Exit Try
                    End If

                    If blnImgInDB Then
                        If objEmp._Photo Is Nothing Then
                            mstrMessage = Language.getMessage(mstrModuleName, 6, "Sorry, Emplyoee photo is mandatory information. Please provide employee photo.")
                            Exit Try
                        End If
                    Else
                        If objEmp._ImagePath = "" Then
                            mstrMessage = Language.getMessage(mstrModuleName, 6, "Sorry, Emplyoee photo is mandatory information. Please provide employee photo.")
                            Exit Try
                        End If
                    End If

                    If objEmp._Signature Is Nothing Then
                        mstrMessage = Language.getMessage(mstrModuleName, 7, "Sorry, Emplyoee signature is mandatory information. Please provide employee signature.")
                        Exit Try
                    End If

                    If objEmp._Stationunkid <= 0 Then
                        mstrMessage = Language.getMessage(mstrModuleName, 8, "Sorry, Employee branch is mandatory information. Please provide employee branch to continue.")
                        Exit Try
                    End If
                    If objEmp._Present_Address1.Trim().Length <= 0 Then
                        mstrMessage = Language.getMessage(mstrModuleName, 9, "Sorry, Present address is mandatory information. Please provide present address to continue.")
                        Exit Try
                    End If
                    If objEmp._Present_Stateunkid <= 0 Then
                        mstrMessage = Language.getMessage(mstrModuleName, 10, "Sorry, Present state is mandatory information. Please provide presemt state to continue.")
                        Exit Try
                    End If
                    If objEmp._Present_Post_Townunkid <= 0 Then
                        mstrMessage = Language.getMessage(mstrModuleName, 11, "Sorry, Present town is mandatory information. Please provide presemt town to continue.")
                        Exit Try
                    End If
                    If objEmp._Present_Countryunkid <= 0 Then
                        mstrMessage = Language.getMessage(mstrModuleName, 12, "Sorry, Present country is mandatory information. Please provide presemt country to continue.")
                        Exit Try
                    End If
                    If objEmp._Nationalityunkid <= 0 Then
                        mstrMessage = Language.getMessage(mstrModuleName, 13, "Sorry, Nationality is mandatory information. Please provide nationality to continue.")
                        Exit Try
                    End If
                    If objEmp._Birthdate = Nothing Then
                        mstrMessage = Language.getMessage(mstrModuleName, 14, "Sorry, Birthdate is mandatory information. Please provide birthdate to continue.")
                        Exit Try
                    End If
                    If objEmp._Birthcountryunkid <= 0 Then
                        mstrMessage = Language.getMessage(mstrModuleName, 19, "Sorry, Birth country is mandatory information. Please provide birthdate to continue.")
                        Exit Try
                    End If
                    If objEmp._Titalunkid <= 0 Then
                        mstrMessage = Language.getMessage(mstrModuleName, 15, "Sorry, Title is mandatory information. Please provide title to continue.")
                        Exit Try
                    End If

                    Dim strMessageId As String = ""
                    Dim iCount As Integer = 1
                    Dim StrQ As String : Dim dsList As New DataSet
                    Using objDataOpr As New clsDataOperation
                        While iCount > 0
                            StrQ = "SELECT CONVERT(NVarChar(5), right(replace(convert(varchar, getdate(),114),':',''),5)) + CONVERT(NVarChar(5), right(newid(),5)) "
                            dsList = objDataOpr.ExecQuery(StrQ, "List")
                            If objDataOpr.ErrorMessage <> "" Then
                                Throw New Exception(objDataOpr.ErrorNumber & " : " & objDataOpr.ErrorMessage)
                            End If
                            strMessageId = CStr(dsList.Tables(0).Rows(0)(0))
                            StrQ = "SELECT 1 FROM hrorbit_request_tran WHERE referenceno = '" & strMessageId & "' "
                            iCount = objDataOpr.RecordCount(StrQ)
                            If objDataOpr.ErrorMessage <> "" Then
                                Throw New Exception(objDataOpr.ErrorNumber & " : " & objDataOpr.ErrorMessage)
                            End If
                            If iCount <= 0 Then Exit While
                        End While
                    End Using
                    'Dim random As New Random
                    'strMessageId = random.Next(10000, 99999).ToString()



                    Dim cc As New clscreateCustomer
                    'cc.channelId = "104"
                    'cc.serviceChannelCode = "HRS"
                    'cc.transmissionTime = "1542931200"
                    cc.referenceNo = strMessageId
                    cc.credentials.username = mDicKeyValues("OrbitUsername")
                    cc.credentials.password = mDicKeyValues("OrbitPassword")
                    cc.credentials.agentUsername = mDicKeyValues("OrbitAgentUsername")
                    Dim strOrbKey As String = ""
                    strOrbKey = GetOrbitMappedKey(clsMapArutiOrbitParams.enArutiParameter.BRANCH, objEmp._Stationunkid)
                    cc.businessUnitId = strOrbKey
                    cc.addressLine1 = objEmp._Present_Address1.Trim()
                    Dim objState As New clsstate_master
                    Dim objCity As New clscity_master
                    objState._Stateunkid = objEmp._Present_Stateunkid
                    objCity._Cityunkid = objEmp._Present_Post_Townunkid
                    cc.addressState = objState._Name.Trim()
                    cc.addressCity = objCity._Name.Trim()
                    cc.addressPropertyTypeId = "354" 'Use default Property type Other. Will be same for all staff. Its ID is 371
                    strOrbKey = ""
                    strOrbKey = GetOrbitMappedValue(clsMapArutiOrbitParams.enArutiParameter.COUNTRY, objEmp._Birthcountryunkid)
                    cc.countryOfBirthCd = "COD" 'strOrbKey
                    strOrbKey = ""
                    strOrbKey = GetOrbitMappedValue(clsMapArutiOrbitParams.enArutiParameter.COUNTRY, objEmp._Present_Countryunkid)
                    cc.custCountryCd = "COD" 'strOrbKey
                    cc.customerCategory = "PER"
                    cc.customerSegmentCd = "OTHER"
                    cc.customerType = "701" 'Customer type is Staff for all employees. ID for Staff in Orbit is 710
                    cc.firstName = objEmp._Firstname.Trim()
                    cc.lastName = objEmp._Surname.Trim()
                    strOrbKey = ""
                    strOrbKey = GetOrbitMappedKey(clsMapArutiOrbitParams.enArutiParameter.GENDER, objEmp._Gender)
                    cc.gender = strOrbKey
                    strOrbKey = ""
                    strOrbKey = GetOrbitMappedKey(clsMapArutiOrbitParams.enArutiParameter.MARITALSTATUS, objEmp._Maritalstatusunkid)
                    cc.maritalStatus = CStr(IIf(strOrbKey.Trim.Length <= 0, "S", strOrbKey))
                    strOrbKey = ""
                    strOrbKey = GetOrbitMappedKey(clsMapArutiOrbitParams.enArutiParameter.BRANCH, objEmp._Stationunkid)
                    cc.mainBusinessUnitId = strOrbKey
                    cc.openingReasonCode = "503" 'Refers to Reason for opening account i.e. For Savings. Use a default opening reason for all staff. ID for this Reason in Orbit is 491
                    strOrbKey = ""
                    strOrbKey = GetOrbitMappedKey(clsMapArutiOrbitParams.enArutiParameter.NATIONALITY, objEmp._Nationalityunkid)
                    cc.nationalityId = strOrbKey
                    cc.operationCurrencyCd = "USD"
                    strOrbKey = ""
                    strOrbKey = GetOrbitMappedValue(clsMapArutiOrbitParams.enArutiParameter.COUNTRY, objEmp._Present_Countryunkid)
                    cc.residentCountryCd = "COD" 'strOrbKey
                    cc.residentFlag = CStr(True).ToLower
                    If objEmp._Birthdate <> Nothing Then
                        cc.strDateOfBirth = objEmp._Birthdate.ToString("dd/MM/yyyy").Replace("-", "/")
                    End If
                    cc.strFromDate = objEmp._Appointeddate.ToString("dd/MM/yyyy").Replace("-", "/")
                    strOrbKey = ""
                    strOrbKey = GetOrbitMappedKey(clsMapArutiOrbitParams.enArutiParameter.COUNTRY, objEmp._Birthcountryunkid)
                    cc.countryOfBirthId = strOrbKey
                    strOrbKey = ""
                    strOrbKey = GetOrbitMappedKey(clsMapArutiOrbitParams.enArutiParameter.COUNTRY, objEmp._Present_Countryunkid)
                    cc.countryId = strOrbKey
                    cc.countryOfResidenceId = strOrbKey
                    cc.addressTypeId = "42" '<!-- Defualt to 42 -->
                    cc.addressCountryId = strOrbKey
                    strOrbKey = ""
                    strOrbKey = GetOrbitMappedKey(clsMapArutiOrbitParams.enArutiParameter.TITLE, objEmp._Titalunkid)
                    cc.titleId = strOrbKey
                    cc.supplementaryRelationshipOfficerId = "1210" 'PASSING AS OF NOW ONCE THEY WILL CREATE THE USER NEED TO FETCH IT FROM THAT

                    If mdtTran.Rows.Count > 0 Then
                        Dim dr As DataRow() = mdtTran.Select("isdefault = 1")
                        If dr.Length <= 0 Then dr = mdtTran.Select("isdefault = 0")
                        If dr.Length > 0 Then
                            cc.identificationsList.identityTypeId = "3980"  'NO VALUE PROVIDED YET
                            strOrbKey = ""
                            strOrbKey = GetOrbitMappedKey(clsMapArutiOrbitParams.enArutiParameter.IDTYPE, CInt(dr(0)("idtypeunkid")))
                            cc.identificationsList.identityType = strOrbKey
                            cc.identificationsList.identityNumber = CStr(dr(0)("identity_no")).Trim()
                            strOrbKey = ""
                            strOrbKey = GetOrbitMappedValue(clsMapArutiOrbitParams.enArutiParameter.COUNTRY, CInt(dr(0)("countryunkid")))
                            cc.identificationsList.countryOfIssue = "COD" 'strOrbKey
                            strOrbKey = ""
                            'strOrbKey = GetOrbiMappedKeyByName(clsMapArutiOrbitParams.enOrbitParameter.CITY, CStr(dr(0)("issued_place")))
                            cc.identificationsList.cityOfIssue = "655" 'strOrbKey
                            If IsDBNull(dr(0)("issue_date")) = False Then
                                cc.identificationsList.strIssueDate = "01/11/2018" 'Format(CDate(dr(0)("issue_date")), "dd/MM/yyyy").Replace("-", "/")
                            End If
                            If IsDBNull(dr(0)("expiry_date")) = False Then
                                cc.identificationsList.strExpiryDate = Format(CDate(dr(0)("expiry_date")), "dd/MM/yyyy").Replace("-", "/").Trim()
                            End If
                            cc.identificationsList.verifiedFlag = CStr(True).ToLower
                        End If
                    End If
                    cc.contactsList.contactModeTypeId = "237" 'Contact mode will be Mobile No. for all staff. ID for Mobile No. in Orbit is 237 
                    cc.contactsList.contactMode = "700"
                    cc.contactsList.contactDetails = objEmp._Present_Mobile.Trim()
                    cc.riskCode = "100" '"566"
                    cc.industryId = "1970" '"1960"
                    Dim base64String As String = ""
                    If objEmp._Photo IsNot Nothing Then
                        If objEmp._Photo.Length > 15000 Then 'FILE SIZE SHOULD BE 15KB
                            mstrMessage = Language.getMessage(mstrModuleName, 1, "Sorry, employee photo size cannot be greater than 15 kb.")
                            Exit Try
                        End If
                        base64String = Convert.ToBase64String(objEmp._Photo)
                        cc.imageList.Add(New imageList("PHO", base64String))
                    Else
                        If objEmp._ImagePath <> "" Then
                            Dim blnIsIISInstalled As Boolean = IsSelfServiceExist()
                            If blnIsIISInstalled Then
                                Dim strPath As String = ArutiSelfServiceURL + "/UploadImage/" + StrEmployeeProfileFolder
                                Dim strError As String = ""
                                Dim strLocalpath As String = ""
                                Dim imagebyte() As Byte = clsFileUploadDownload.DownloadFile(strPath, objEmp._ImagePath, StrEmployeeProfileFolder, strError, ArutiSelfServiceURL)
                                If imagebyte IsNot Nothing Then
                                    If imagebyte.Length > 15000 Then 'FILE SIZE SHOULD BE 15KB
                                        mstrMessage = Language.getMessage(mstrModuleName, 1, "Sorry, employee photo size cannot be greater than 15 kb.")
                                        Exit Try
                                    End If
                                    base64String = Convert.ToBase64String(imagebyte)
                                    cc.imageList.Add(New imageList("PHO", base64String))
                                Else
                                    If cc.imageList.Count <= 0 Then cc.imageList.Add(New imageList("PHO", ""))
                                End If
                            Else
                                If PhotoPath.Trim.Length > 0 Then
                                    If System.IO.File.Exists(PhotoPath & "\" & objEmp._ImagePath) Then
                                        If File.ReadAllBytes(PhotoPath & "\" & objEmp._ImagePath).Length > 15000 Then 'FILE SIZE SHOULD BE 15KB
                                            mstrMessage = Language.getMessage(mstrModuleName, 1, "Sorry, employee photo size cannot be greater than 15 kb.")
                                            Exit Try
                                        End If
                                        base64String = Convert.ToBase64String(File.ReadAllBytes(PhotoPath & "\" & objEmp._ImagePath))
                                        cc.imageList.Add(New imageList("PHO", base64String))
                                    Else
                                        If cc.imageList.Count <= 0 Then cc.imageList.Add(New imageList("PHO", ""))
                                    End If
                                End If
                            End If
                        Else
                            If cc.imageList.Count <= 0 Then cc.imageList.Add(New imageList("PHO", ""))
                        End If
                    End If
                    If objEmp._Signature IsNot Nothing Then
                        If objEmp._Signature.Length > 15000 Then 'FILE SIZE SHOULD BE 15KB
                            mstrMessage = Language.getMessage(mstrModuleName, 1, "Sorry, employee photo size cannot be greater than 15 kb.")
                            Exit Try
                        End If
                        base64String = Convert.ToBase64String(objEmp._Signature)
                        cc.imageList.Add(New imageList("SIG", base64String))
                    Else
                        cc.imageList.Add(New imageList("SIG", ""))
                    End If


                    'strResponse = JsonConvert.SerializeObject(cc, Formatting.Indented, New JsonSerializerSettings With {.NullValueHandling = NullValueHandling.Ignore, .DefaultValueHandling = DefaultValueHandling.Ignore})    
                    strResponse = JsonConvert.SerializeObject(cc, Formatting.Indented)

                    objCity = Nothing : objState = Nothing
                    objEmp = Nothing

                    Dim blnIsError As Boolean = False
                    strResponse = PostData(mDicKeyValues("_OrbSrv_" & CInt(enOrbitServiceInfo.ORB_CREATECUSTOMER)), cc, blnIsError)
                    blnIsErrorOnRequest = blnIsError 'S.SANDEEP |30-MAY-2020| -- START -- END
                    If blnIsError Then
                        mstrtranguid = Guid.NewGuid.ToString()
                        mintEmployeeunkid = intEmployeeId
                        mdtRequestdate = Now
                        mintRequesttypeid = enOrbitServiceInfo.ORB_CREATECUSTOMER
                        mstrReferenceno = strMessageId
                        mstrCustomer_Number = ""
                        mstrCustomerStatus = ""
                        mstrAccountStatus = ""
                        mstrPrimaryAccountNumber = ""
                        mstrPostedData = JsonConvert.SerializeObject(cc, Formatting.Indented) ' JsonConvert.SerializeObject(cc, Formatting.Indented, New JsonSerializerSettings With {.NullValueHandling = NullValueHandling.Ignore, .DefaultValueHandling = DefaultValueHandling.Ignore, .ContractResolver = New DefaultContractResolver()})
                        mstrResponseData = ""
                        mblnIsError = blnIsError
                        mstrErrorDescription = strResponse
                        mintReqestModuleId = 0
                        mstrRequestMst_Number = ""
                        mblnIssrvrequest = False
                        mstrIPAddress = getIP()
                        mblnIsProcessed = False
                        If Insert() = False Then
                            Throw New Exception(strResponse)
                        End If
                    Else
                        If strResponse.Trim.Length > 0 Then
                            Dim dsResponse As New DataSet
                            Dim strErrMsg As String = ""
                            ConvertJsonToDataset(strResponse, dsResponse)
                            If dsResponse IsNot Nothing AndAlso dsResponse.Tables.Count > 0 Then
                                If dsResponse.Tables.Contains("SystemErrorCode") Then
                                    strErrMsg = dsResponse.Tables("SystemErrorCode").Rows(0)("SystemErrorCode").ToString()
                                    blnIsError = True
                                    blnIsErrorOnRequest = blnIsError
                                End If
                                If dsResponse.Tables.Contains("SystemErrorMessage") Then
                                    If strErrMsg.Trim.Length > 0 Then
                                        strErrMsg &= " : " & dsResponse.Tables("SystemErrorMessage").Rows(0)("SystemErrorMessage").ToString()
                                    Else
                                        strErrMsg = dsResponse.Tables("SystemErrorMessage").Rows(0)("SystemErrorMessage").ToString()
                                    End If
                                    blnIsError = True
                                    blnIsErrorOnRequest = blnIsError
                                End If
                            End If

                            mstrtranguid = Guid.NewGuid.ToString()
                            mintEmployeeunkid = intEmployeeId
                            mdtRequestdate = Now
                            mintRequesttypeid = enOrbitServiceInfo.ORB_CREATECUSTOMER
                            mstrReferenceno = strMessageId

                            If dsResponse IsNot Nothing AndAlso dsResponse.Tables.Count > 0 Then
                                If dsResponse.Tables.Contains("customerNumber") Then
                                    mstrCustomer_Number = dsResponse.Tables("customerNumber").Rows(0)("customerNumber").ToString()
                                End If
                                If dsResponse.Tables.Contains("status") Then
                                    mstrCustomerStatus = dsResponse.Tables("status").Rows(0)("status").ToString()
                                End If
                            End If
                            mstrAccountStatus = ""
                            mstrPrimaryAccountNumber = ""
                            mstrPostedData = JsonConvert.SerializeObject(cc, Formatting.Indented) 'JsonConvert.SerializeObject(cc, Formatting.Indented, New JsonSerializerSettings With {.NullValueHandling = NullValueHandling.Ignore})
                            mstrResponseData = strResponse
                            mblnIsError = blnIsError
                            mstrErrorDescription = strErrMsg
                            mintReqestModuleId = 0
                            mstrRequestMst_Number = ""
                            mblnIssrvrequest = False
                            mstrIPAddress = getIP()
                            mblnIsProcessed = False
                            If Insert() = False Then
                                Throw New Exception(strResponse)
                            End If
                        End If
                    End If
                End If

            End If
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: CreateCustomer; Module Name: " & mstrModuleName)
        Finally
        End Try
        xMsg = mstrMessage
        Return strResponse
    End Function

    Public Function CreateDepositAccount(ByVal intCompanyId As Integer, _
                                         ByVal strName As String, _
                                         ByVal strCustNo As String, _
                                         ByVal strOpeningDate As String, _
                                         ByVal intEmployeeId As Integer, _
                                         ByRef xMsg As String, _
                                         ByRef blnIsErrorOnRequest As Boolean, _
                                         Optional ByVal strPrdctCode As String = "") As String 'S.SANDEEP |30-MAY-2020| -- START {blnIsErrorOnRequest} -- END

        Dim strResponse As String = String.Empty
        Dim mDicKeyValues As New Dictionary(Of String, String)
        Try
            mDicKeyValues = GetOrbitParameters(enOrbitServiceInfo.ORB_CREATEDEPOSITACCOUNT, intCompanyId)
            If mDicKeyValues.Keys.Count > 0 Then
                If CBool(mDicKeyValues("IsOrbitIntegrated")) Then
                    If mDicKeyValues.ContainsKey("_OrbSrv_" & CInt(enOrbitServiceInfo.ORB_CREATEDEPOSITACCOUNT)) = False Then Exit Try
                    Dim strproductCode As String = ""
                    If strPrdctCode.Trim.Length <= 0 Then
                        For i As Integer = 0 To 1 'AS WE NEED TO CREATE THE DEPOSIT ACCOUNT 2 TIMES (USD AND CDF) FOR EACH EMPLYOEE
                            Select Case i
                                Case 0
                                    strproductCode = "207" 'USD
                                Case 1
                                    strproductCode = "101" 'CDF
                            End Select
                            Dim strMessageId As String = ""
                            Dim iCount As Integer = 1
                            Dim StrQ As String : Dim dsList As New DataSet
                            Using objDataOpr As New clsDataOperation
                                While iCount > 0
                                    StrQ = "SELECT CONVERT(NVarChar(5), right(replace(convert(varchar, getdate(),114),':',''),5)) + CONVERT(NVarChar(5), right(newid(),5)) "
                                    dsList = objDataOpr.ExecQuery(StrQ, "List")
                                    If objDataOpr.ErrorMessage <> "" Then
                                        Throw New Exception(objDataOpr.ErrorNumber & " : " & objDataOpr.ErrorMessage)
                                    End If
                                    strMessageId = CStr(dsList.Tables(0).Rows(0)(0))
                                    StrQ = "SELECT 1 FROM hrorbit_request_tran WHERE referenceno = '" & strMessageId & "' "
                                    iCount = objDataOpr.RecordCount(StrQ)
                                    If objDataOpr.ErrorMessage <> "" Then
                                        Throw New Exception(objDataOpr.ErrorNumber & " : " & objDataOpr.ErrorMessage)
                                    End If
                                    If iCount <= 0 Then Exit While
                                End While
                            End Using

                            Dim cd As New clscreateDepositAccount
                            cd.referenceNo = strMessageId
                            cd.credentials.username = mDicKeyValues("OrbitUsername")
                            cd.credentials.password = mDicKeyValues("OrbitPassword")
                            cd.credentials.agentUsername = mDicKeyValues("OrbitAgentUsername")
                            cd.accountTitle = strName
                            cd.primaryCustomerNumber = strCustNo
                            cd.productCode = strproductCode '"207" '"308" 'They have created ID for Other in Orbit and This will be Same for all staff. ID for Other in Orbit is 308 
                            cd.strOpeningDate = Format(CDate(eZeeDate.convertDate(strOpeningDate)), "dd/MM/yyyy").Replace("-", "/")

                            'strResponse = JsonConvert.SerializeObject(cd, Formatting.Indented, New JsonSerializerSettings With {.NullValueHandling = NullValueHandling.Ignore})
                            strResponse = JsonConvert.SerializeObject(cd, Formatting.Indented)

                            Dim blnIsError As Boolean = False
                            strResponse = PostData(mDicKeyValues("_OrbSrv_" & CInt(enOrbitServiceInfo.ORB_CREATEDEPOSITACCOUNT)), cd, blnIsError)
                            blnIsErrorOnRequest = blnIsError 'S.SANDEEP |30-MAY-2020| -- START -- END
                            If blnIsError Then
                                mstrtranguid = Guid.NewGuid.ToString()
                                mintEmployeeunkid = intEmployeeId
                                mdtRequestdate = Now
                                mintRequesttypeid = enOrbitServiceInfo.ORB_CREATEDEPOSITACCOUNT
                                mstrReferenceno = strMessageId
                                mstrCustomer_Number = strCustNo
                                mstrCustomerStatus = ""
                                mstrAccountStatus = ""
                                mstrPrimaryAccountNumber = ""
                                mstrPostedData = JsonConvert.SerializeObject(cd, Formatting.Indented) 'JsonConvert.SerializeObject(cd, Formatting.Indented, New JsonSerializerSettings With {.NullValueHandling = NullValueHandling.Ignore})
                                mstrResponseData = ""
                                mblnIsError = blnIsError
                                mstrErrorDescription = strResponse
                                mintReqestModuleId = 0
                                mstrRequestMst_Number = ""
                                mblnIssrvrequest = False
                                mstrIPAddress = getIP()
                                mblnIsProcessed = False
                                mstrProductCode = strproductCode
                                If Insert() = False Then
                                    Throw New Exception(strResponse)
                                End If
                            Else
                                If strResponse.Trim.Length > 0 Then
                                    Dim dsResponse As New DataSet
                                    Dim strErrMsg As String = ""
                                    ConvertJsonToDataset(strResponse, dsResponse)
                                    If dsResponse IsNot Nothing AndAlso dsResponse.Tables.Count > 0 Then
                                        If dsResponse.Tables.Contains("SystemErrorCode") Then
                                            strErrMsg = dsResponse.Tables("SystemErrorCode").Rows(0)("SystemErrorCode").ToString()
                                            blnIsError = True
                                            blnIsErrorOnRequest = blnIsError
                                        End If
                                    End If
                                    If dsResponse.Tables.Contains("SystemErrorMessage") Then
                                        If strErrMsg.Trim.Length > 0 Then
                                            strErrMsg &= " : " & dsResponse.Tables("SystemErrorMessage").Rows(0)("SystemErrorMessage").ToString()
                                        Else
                                            strErrMsg = dsResponse.Tables("SystemErrorMessage").Rows(0)("SystemErrorMessage").ToString()
                                        End If
                                        blnIsError = True
                                        blnIsErrorOnRequest = blnIsError
                                    End If
                                    mstrtranguid = Guid.NewGuid.ToString()
                                    mintEmployeeunkid = intEmployeeId
                                    mdtRequestdate = Now
                                    mintRequesttypeid = enOrbitServiceInfo.ORB_CREATEDEPOSITACCOUNT
                                    mstrReferenceno = strMessageId
                                    If dsResponse IsNot Nothing AndAlso dsResponse.Tables.Count > 0 Then
                                        If dsResponse.Tables.Contains("accountStatus") Then
                                            mstrAccountStatus = dsResponse.Tables("accountStatus").Rows(0)("accountStatus").ToString()
                                        End If
                                        If dsResponse.Tables.Contains("primaryAccountNumber") Then
                                            mstrPrimaryAccountNumber = dsResponse.Tables("primaryAccountNumber").Rows(0)("primaryAccountNumber").ToString()
                                        End If
                                    End If
                                    mstrPostedData = JsonConvert.SerializeObject(cd, Formatting.Indented) 'JsonConvert.SerializeObject(cd, Formatting.Indented, New JsonSerializerSettings With {.NullValueHandling = NullValueHandling.Ignore})
                                    mstrResponseData = strResponse
                                    mblnIsError = blnIsError
                                    mstrErrorDescription = strErrMsg
                                    mintReqestModuleId = 0
                                    mstrRequestMst_Number = ""
                                    mblnIssrvrequest = False
                                    mstrIPAddress = getIP()
                                    mblnIsProcessed = False
                                    mstrProductCode = strproductCode
                                    If Insert() = False Then
                                        Throw New Exception(strResponse)
                                    End If
                                End If
                            End If
                        Next
                    Else

                    End If
                End If
            End If
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: CreateDepositAccount; Module Name: " & mstrModuleName)
        Finally
        End Try
        Return strResponse
    End Function

    Public Function ProcessBulkPayments(ByVal intCompanyId As Integer, ByVal strPaymentVocNo As String, ByVal strChequeNo As String, ByVal dtAuthorization As DataTable, ByVal strDescription As String, ByRef strMsg As String, ByVal strCompanyBankGLAccountName As String) As String
        Dim strResponse As String = String.Empty
        Dim mDicKeyValues As New Dictionary(Of String, String)
        Try
            mDicKeyValues = GetOrbitParameters(enOrbitServiceInfo.ORB_PROCESSBULKPAYMENTS, intCompanyId)
            If mDicKeyValues.Keys.Count > 0 Then
                If CBool(mDicKeyValues("IsOrbitIntegrated")) Then
                    If mDicKeyValues.ContainsKey("_OrbSrv_" & CInt(enOrbitServiceInfo.ORB_PROCESSBULKPAYMENTS)) = False Then Exit Try
                    Dim strMessageId As String = ""
                    Dim iCount As Integer = 1
                    Dim StrQ As String : Dim dsList As New DataSet
                    Using objDataOpr As New clsDataOperation
                        While iCount > 0
                            StrQ = "SELECT CONVERT(NVarChar(3), right(replace(convert(varchar, getdate(),114),':',''),3)) + CONVERT(NVarChar(3), right(newid(),3)) "
                            dsList = objDataOpr.ExecQuery(StrQ, "List")
                            If objDataOpr.ErrorMessage <> "" Then
                                Throw New Exception(objDataOpr.ErrorNumber & " : " & objDataOpr.ErrorMessage)
                            End If
                            'strMessageId = CStr(dsList.Tables(0).Rows(0)(0))
                            strMessageId = GenerateOTP(8, True, False, False)
                            StrQ = "SELECT 1 FROM hrorbit_request_tran WHERE referenceno = '" & strMessageId & "' "
                            iCount = objDataOpr.RecordCount(StrQ)
                            If objDataOpr.ErrorMessage <> "" Then
                                Throw New Exception(objDataOpr.ErrorNumber & " : " & objDataOpr.ErrorMessage)
                            End If
                            If iCount <= 0 Then Exit While
                        End While
                    End Using

                    Dim pp As New processBulkPayments
                    pp.referenceNo = dtAuthorization.Rows(0).Item("voucherno").ToString 'strMessageId
                    pp.credentials.username = mDicKeyValues("OrbitUsername")
                    pp.credentials.password = mDicKeyValues("OrbitPassword")
                    pp.credentials.agentUsername = mDicKeyValues("OrbitAgentUsername")
                    pp.businessUnitId = "-99"
                    pp.recurringFlag = CStr(False).ToLower
                    pp.txnBatchCode = strMessageId 'dtAuthorization.Rows(0).Item("voucherno").ToString
                    'pp.bulkPaymentDesc = dtAuthorization.Rows(0).Item("remarks").ToString
                    If dtAuthorization.Rows(0).Item("remarks").ToString.Trim = "" Then
                        pp.bulkPaymentDesc = Language.getMessage(mstrModuleName, 20, "Payment Authorozed")
                    Else
                    pp.bulkPaymentDesc = dtAuthorization.Rows(0).Item("remarks").ToString
                    End If

                    Dim decTotalAmt As Decimal = (From p In dtAuthorization Select (CDec(Format(CDec(p.Item("Amount")), "##########0.00")))).Sum

                    For Each dtRow As DataRow In dtAuthorization.Rows
                        pp.bulkItemArray.Add(New bulkItemArray(dtRow.Item("EMP_AccountNo").ToString _
                                                              , dtRow.Item("currency_name").ToString _
                                                              , CDec(Format(CDec(dtRow.Item("Amount")), "##########0.00")) _
                                                              , dtRow.Item("chequeno").ToString _
                                                              , dtAuthorization.Rows(0).Item("voucherno").ToString _
                                                              , CDec(dtRow.Item("expaidrate")).ToString("##########0.00") _
                                                              , Language.getMessage(mstrModuleName, 17, "410") _
                                                              ))
                    Next

                    'pp.bulkItemArray.Add(New bulkItemArray(dtAuthorization.Rows(0).Item("CMP_Account").ToString _
                    '                                          , dtAuthorization.Rows(0).Item("currency_name").ToString _
                    '                                          , CDec(Format(decTotalAmt, "##########0")) _
                    '                                          , dtAuthorization.Rows(0).Item("chequeno").ToString _
                    '                                          , dtAuthorization.Rows(0).Item("voucherno").ToString _
                    '                                          , CDec(dtAuthorization.Rows(0).Item("expaidrate")).ToString("##########0") _
                    '                                          , Language.getMessage(mstrModuleName, 18, "410") _
                    '                                          ))
                    pp.bulkItemArray.Add(New bulkItemArray(strCompanyBankGLAccountName _
                                                              , dtAuthorization.Rows(0).Item("currency_name").ToString _
                                                              , CDec(Format(decTotalAmt, "##########0.00")) _
                                                              , dtAuthorization.Rows(0).Item("chequeno").ToString _
                                                              , dtAuthorization.Rows(0).Item("voucherno").ToString _
                                                              , CDec(dtAuthorization.Rows(0).Item("expaidrate")).ToString("##########0.00") _
                                                              , Language.getMessage(mstrModuleName, 18, "455") _
                                                              ))

                    strResponse = JsonConvert.SerializeObject(pp, Formatting.Indented)
                    strResponse = JValue.Parse(strResponse).ToString(Formatting.Indented)

                    Dim blnIsError As Boolean = False
                    strResponse = PostData(mDicKeyValues("_OrbSrv_" & CInt(enOrbitServiceInfo.ORB_PROCESSBULKPAYMENTS)), pp, blnIsError)
                    If blnIsError Then
                        mstrtranguid = Guid.NewGuid.ToString()
                        mintEmployeeunkid = 0
                        mdtRequestdate = Now
                        mintRequesttypeid = enOrbitServiceInfo.ORB_PROCESSBULKPAYMENTS
                        mintPeriodunkid = CInt(dtAuthorization.Rows(0).Item("periodunkid").ToString)
                        mstrReferenceno = strMessageId
                        mstrCustomer_Number = ""
                        mstrCustomerStatus = ""
                        mstrAccountStatus = ""
                        mstrPrimaryAccountNumber = ""
                        mstrPostedData = JsonConvert.SerializeObject(pp, Formatting.Indented) ' JsonConvert.SerializeObject(cc, Formatting.Indented, New JsonSerializerSettings With {.NullValueHandling = NullValueHandling.Ignore, .DefaultValueHandling = DefaultValueHandling.Ignore, .ContractResolver = New DefaultContractResolver()})
                        mstrResponseData = ""
                        mblnIsError = blnIsError
                        mstrErrorDescription = strResponse
                        mintReqestModuleId = 0
                        mstrRequestMst_Number = dtAuthorization.Rows(0).Item("voucherno").ToString
                        mblnIssrvrequest = False
                        mstrIPAddress = getIP()
                        mblnIsProcessed = False
                        If Insert() = False Then
                            Throw New Exception(strResponse)
                        End If
                    Else
                        If strResponse.Trim.Length > 0 Then
                            Dim dsResponse As New DataSet
                            Dim strErrMsg As String = ""
                            ConvertJsonToDataset(strResponse, dsResponse)
                            If dsResponse IsNot Nothing AndAlso dsResponse.Tables.Count > 0 Then
                                If dsResponse.Tables.Contains("SystemErrorCode") Then
                                    strErrMsg = dsResponse.Tables("SystemErrorCode").Rows(0)("SystemErrorCode").ToString()
                                    blnIsError = True
                                End If
                            End If
                                If dsResponse.Tables.Contains("SystemErrorMessage") Then
                                    If strErrMsg.Trim.Length > 0 Then
                                        strErrMsg &= " : " & dsResponse.Tables("SystemErrorMessage").Rows(0)("SystemErrorMessage").ToString()
                                    Else
                                        strErrMsg = dsResponse.Tables("SystemErrorMessage").Rows(0)("SystemErrorMessage").ToString()
                                    End If
                                    blnIsError = True
                                End If

                            mstrtranguid = Guid.NewGuid.ToString()
                            mintEmployeeunkid = 0
                            mdtRequestdate = Now
                            mintRequesttypeid = enOrbitServiceInfo.ORB_PROCESSBULKPAYMENTS
                            mintPeriodunkid = CInt(dtAuthorization.Rows(0).Item("periodunkid").ToString)
                            mstrReferenceno = strMessageId

                            If dsResponse IsNot Nothing AndAlso dsResponse.Tables.Count > 0 Then
                                If dsResponse.Tables.Contains("customerNumber") Then
                                    mstrCustomer_Number = dsResponse.Tables("customerNumber").Rows(0)("customerNumber").ToString()
                                End If
                                If dsResponse.Tables.Contains("status") Then
                                    mstrCustomerStatus = dsResponse.Tables("status").Rows(0)("status").ToString()
                                End If
                            End If
                            mstrAccountStatus = ""
                            mstrPrimaryAccountNumber = ""
                            mstrPostedData = JsonConvert.SerializeObject(pp, Formatting.Indented) 'JsonConvert.SerializeObject(cc, Formatting.Indented, New JsonSerializerSettings With {.NullValueHandling = NullValueHandling.Ignore})
                            mstrResponseData = strResponse
                            mblnIsError = blnIsError
                            mstrErrorDescription = strErrMsg
                            mintReqestModuleId = 0
                            mstrRequestMst_Number = dtAuthorization.Rows(0).Item("voucherno").ToString
                            mblnIssrvrequest = False
                            mstrIPAddress = getIP()
                            mblnIsProcessed = False
                            If Insert() = False Then
                                Throw New Exception(strResponse)
                            End If
                        End If
                    End If



                End If
            End If

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: ProcessBulkPayments; Module Name: " & mstrModuleName)
        Finally
        End Try
        Return strResponse
    End Function

    Public Function GetOrbitMappedKeyValue(ByVal eArutiParam As clsMapArutiOrbitParams.enArutiParameter, ByVal intAllocationId As Integer) As Dictionary(Of String, String)
        Dim StrQ As String = String.Empty
        Dim exForce As Exception = Nothing
        Dim dsList As New DataSet
        Dim mDicOrbKeyVal As New Dictionary(Of String, String)
        Try
            Using objData As New clsDataOperation
                StrQ = "SELECT " & _
                       "     OMT.orbitfldkey " & _
                       "    ,OMT.orbitfldvalue " & _
                       "FROM hrorbitparam_mapping_tran AS OMT " & _
                       "WHERE OMT.isactive = 1 " & _
                       "AND OMT.allocationunkid = @allocationunkid AND OMT.allocationrefid = @allocationrefid "
                objData.AddParameter("@allocationrefid", SqlDbType.Int, eZeeDataType.INT_SIZE, eArutiParam)
                objData.AddParameter("@allocationunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intAllocationId)

                dsList = objData.ExecQuery(StrQ, "List")

                If objData.ErrorMessage <> "" Then
                    exForce = New Exception(objData.ErrorNumber & " : " & objData.ErrorMessage)
                    Throw exForce
                End If

                If dsList.Tables("List").Rows.Count > 0 Then
                    mDicOrbKeyVal = dsList.Tables("List").AsEnumerable().ToDictionary(Function(x) x.Field(Of String)("orbitfldkey"), _
                                                                                      Function(y) y.Field(Of String)("orbitfldvalue"))
                End If
            End Using
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: GetOrbitMappedKeyValue; Module Name: " & mstrModuleName)
        Finally
        End Try
        Return mDicOrbKeyVal
    End Function

    Public Function GetOrbitMappedKey(ByVal eArutiParam As clsMapArutiOrbitParams.enArutiParameter, ByVal intAllocationId As Integer) As String
        Dim StrQ As String = String.Empty
        Dim exForce As Exception = Nothing
        Dim dsList As New DataSet
        Dim StrOrbKey As String = ""
        Try
            Using objData As New clsDataOperation
                StrQ = "SELECT " & _
                       "     OMT.orbitfldkey " & _
                       "FROM hrorbitparam_mapping_tran AS OMT " & _
                       "WHERE OMT.isactive = 1 " & _
                       "AND OMT.allocationunkid = @allocationunkid AND OMT.allocationrefid = @allocationrefid "

                objData.AddParameter("@allocationrefid", SqlDbType.Int, eZeeDataType.INT_SIZE, eArutiParam)
                objData.AddParameter("@allocationunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intAllocationId)

                dsList = objData.ExecQuery(StrQ, "List")

                If objData.ErrorMessage <> "" Then
                    exForce = New Exception(objData.ErrorNumber & " : " & objData.ErrorMessage)
                    Throw exForce
                End If

                If dsList.Tables("List").Rows.Count > 0 Then
                    StrOrbKey = CStr(dsList.Tables("List").Rows(0)("orbitfldkey"))
                End If
            End Using
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: GetOrbitMappedKey; Module Name: " & mstrModuleName)
        Finally
        End Try
        Return StrOrbKey
    End Function

    Public Function GetOrbitMappedValue(ByVal eArutiParam As clsMapArutiOrbitParams.enArutiParameter, ByVal intAllocationId As Integer) As String
        Dim StrQ As String = String.Empty
        Dim exForce As Exception = Nothing
        Dim dsList As New DataSet
        Dim StrOrbValue As String = ""
        Try
            Using objData As New clsDataOperation
                StrQ = "SELECT " & _
                       "     OMT.orbitfldvalue " & _
                       "FROM hrorbitparam_mapping_tran AS OMT " & _
                       "WHERE OMT.isactive = 1 " & _
                       "AND OMT.allocationunkid = @allocationunkid AND OMT.allocationrefid = @allocationrefid "

                objData.AddParameter("@allocationrefid", SqlDbType.Int, eZeeDataType.INT_SIZE, eArutiParam)
                objData.AddParameter("@allocationunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intAllocationId)

                dsList = objData.ExecQuery(StrQ, "List")

                If objData.ErrorMessage <> "" Then
                    exForce = New Exception(objData.ErrorNumber & " : " & objData.ErrorMessage)
                    Throw exForce
                End If

                If dsList.Tables("List").Rows.Count > 0 Then
                    StrOrbValue = CStr(dsList.Tables("List").Rows(0)("orbitfldvalue"))
                End If
            End Using
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: GetOrbitMappedValue; Module Name: " & mstrModuleName)
        Finally
        End Try
        Return StrOrbValue
    End Function

    Public Function GetOrbiMappedKeyByName(ByVal eOrbParam As clsMapArutiOrbitParams.enOrbitParameter, ByVal strName As String) As String
        Dim StrQ As String = String.Empty
        Dim exForce As Exception = Nothing
        Dim dsList As New DataSet
        Dim StrOrbValue As String = ""
        Try
            Using objData As New clsDataOperation
                StrQ = "SELECT " & _
                       "     OMT.orbitfldkey " & _
                       "FROM hrorbitparam_mapping_tran AS OMT " & _
                       "WHERE OMT.isactive = 1 " & _
                       "AND OMT.orbitfldvalue = @orbitfldvalue AND OMT.arutiorbitrefid = @arutiorbitrefid "

                objData.AddParameter("@arutiorbitrefid", SqlDbType.Int, eZeeDataType.INT_SIZE, eOrbParam)
                objData.AddParameter("@orbitfldvalue", SqlDbType.NVarChar, eZeeDataType.DESC_SIZE, strName)

                dsList = objData.ExecQuery(StrQ, "List")

                If objData.ErrorMessage <> "" Then
                    exForce = New Exception(objData.ErrorNumber & " : " & objData.ErrorMessage)
                    Throw exForce
                End If

                If dsList.Tables("List").Rows.Count > 0 Then
                    StrOrbValue = CStr(dsList.Tables("List").Rows(0)("orbitfldvalue"))
                End If
            End Using
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: GetOrbiMappedValueByName; Module Name: " & mstrModuleName)
        Finally
        End Try
        Return StrOrbValue
    End Function

    Public Function GetDisplayList(ByVal xDatabaseName As String, _
                                   ByVal xUserUnkid As Integer, _
                                   ByVal xYearUnkid As Integer, _
                                   ByVal xCompanyUnkid As Integer, _
                                   ByVal xPeriodStart As DateTime, _
                                   ByVal xPeriodEnd As DateTime, _
                                   ByVal xUserModeSetting As String, _
                                   ByVal xOnlyApproved As Boolean, _
                                   ByVal eType As enViewType, _
                                   ByVal eSrvType As enOrbitServiceInfo, _
                                   ByVal strFilter As String) As DataTable
        Dim StrQ As String = String.Empty
        Dim dsList As New DataSet
        Dim exForce As Exception = Nothing
        Try
            Dim xDateJoinQry, xDataFilterQry, xUACQry, xUACFiltrQry, xAdvanceJoinQry As String
            xDateJoinQry = "" : xDataFilterQry = "" : xUACQry = "" : xUACFiltrQry = "" : xAdvanceJoinQry = ""
            Call GetDatesFilterString(xDateJoinQry, xDataFilterQry, xPeriodStart, xPeriodEnd, True, False, xDatabaseName)
            Call NewAccessLevelFilterString(xUACQry, xUACFiltrQry, xPeriodEnd, xOnlyApproved, xDatabaseName, xUserUnkid, xCompanyUnkid, xYearUnkid, xUserModeSetting)
            Call GetAdvanceFilterQry(xAdvanceJoinQry, xPeriodEnd, xDatabaseName)

            Using objDo As New clsDataOperation
                Select Case CInt(eSrvType)
                    Case 999 'UPDATE EMPLOYEE BANK ACCOUNT
                        Select Case eType
                            Case enViewType.VW_NEW
                                StrQ = "SELECT " & _
                                       "     hremployee_master.employeeunkid " & _
                                       "    ,CAST(0 AS BIT) AS iCheck " & _
                                       "    ,hremployee_master.employeecode + ' - ' + hremployee_master.firstname + ' ' + hremployee_master.surname AS employee " & _
                                       "    ,'' AS requestdate " & _
                                       "    ,'' AS referenceno " & _
                                       "    ,customer_number " & _
                                       "    ,'' AS error_description " & _
                                       "    ,primaryactnumber " & _
                                       "FROM hrorbit_request_tran " & _
                                       "    JOIN hremployee_master ON hrorbit_request_tran.employeeunkid = hremployee_master.employeeunkid "
                                If xAdvanceJoinQry.Trim.Length > 0 Then
                                    StrQ &= xAdvanceJoinQry
                                End If
                                If xDateJoinQry.Trim.Length > 0 Then
                                    StrQ &= xDateJoinQry
                                End If
                                If xUACQry.Trim.Length > 0 Then
                                    StrQ &= xUACQry
                                End If
                                StrQ &= "WHERE error_description = '' AND productcode = 207 AND isassigned = 0 AND requesttypeid = '" & CInt(enOrbitServiceInfo.ORB_CREATEDEPOSITACCOUNT) & "' "
                                If xDataFilterQry.Trim.Length > 0 Then
                                    StrQ &= xDataFilterQry
                                End If
                                If strFilter.Trim <> "" Then
                                    StrQ &= " AND " & strFilter & " "
                                End If
                        End Select
                    Case enOrbitServiceInfo.ORB_CREATECUSTOMER
                        Select Case eType
                            Case enViewType.VW_NEW
                                StrQ = "SELECT " & _
                                       "     hremployee_master.employeeunkid " & _
                                       "    ,CAST(0 AS BIT) AS iCheck " & _
                                       "    ,hremployee_master.employeecode + ' - ' + hremployee_master.firstname + ' ' + hremployee_master.surname AS employee " & _
                                       "    ,'' AS requestdate " & _
                                       "    ,'' AS referenceno " & _
                                       "    ,'' AS customer_number " & _
                                       "    ,'' AS error_description " & _
                                       "    ,'' AS primaryactnumber " & _
                                       "FROM hremployee_master "
                                If xAdvanceJoinQry.Trim.Length > 0 Then
                                    StrQ &= xAdvanceJoinQry
                                End If
                                If xDateJoinQry.Trim.Length > 0 Then
                                    StrQ &= xDateJoinQry
                                End If
                                If xUACQry.Trim.Length > 0 Then
                                    StrQ &= xUACQry
                                End If
                                StrQ &= "WHERE hremployee_master.employeeunkid NOT IN (SELECT employeeunkid FROM hrorbit_request_tran) "
                                If xDataFilterQry.Trim.Length > 0 Then
                                    StrQ &= xDataFilterQry
                                End If
                                If strFilter.Trim <> "" Then
                                    StrQ &= " AND " & strFilter & " "
                                End If
                            Case enViewType.VW_FAILED
                                StrQ = "SELECT " & _
                                       "     hremployee_master.employeeunkid " & _
                                       "    ,CAST(0 AS BIT) AS iCheck " & _
                                       "    ,hremployee_master.employeecode + ' - ' + hremployee_master.firstname + ' ' + hremployee_master.surname AS employee " & _
                                       "    ,requestdate " & _
                                       "    ,referenceno " & _
                                       "    ,customer_number " & _
                                       "    ,error_description " & _
                                       "    ,'' AS primaryactnumber " & _
                                       "FROM hrorbit_request_tran " & _
                                       "    JOIN hremployee_master ON hrorbit_request_tran.employeeunkid = hremployee_master.employeeunkid "
                                If xAdvanceJoinQry.Trim.Length > 0 Then
                                    StrQ &= xAdvanceJoinQry
                                End If
                                If xDateJoinQry.Trim.Length > 0 Then
                                    StrQ &= xDateJoinQry
                                End If
                                If xUACQry.Trim.Length > 0 Then
                                    StrQ &= xUACQry
                                End If
                                StrQ &= "WHERE error_description <> '' AND isprocessed = 0 AND requesttypeid = @requesttypeid "
                                If xDataFilterQry.Trim.Length > 0 Then
                                    StrQ &= xDataFilterQry
                                End If
                                If strFilter.Trim <> "" Then
                                    StrQ &= " AND " & strFilter & " "
                                End If
                            Case enViewType.VW_SUCCESS
                                StrQ = "SELECT " & _
                                       "     hremployee_master.employeeunkid " & _
                                       "    ,CAST(0 AS BIT) AS iCheck " & _
                                       "    ,hremployee_master.employeecode + ' - ' + hremployee_master.firstname + ' ' + hremployee_master.surname AS employee " & _
                                       "    ,requestdate " & _
                                       "    ,referenceno " & _
                                       "    ,customer_number " & _
                                       "    ,error_description " & _
                                       "    ,'' AS primaryactnumber " & _
                                       "FROM hrorbit_request_tran " & _
                                       "    JOIN hremployee_master ON hrorbit_request_tran.employeeunkid = hremployee_master.employeeunkid "
                                If xAdvanceJoinQry.Trim.Length > 0 Then
                                    StrQ &= xAdvanceJoinQry
                                End If
                                If xDateJoinQry.Trim.Length > 0 Then
                                    StrQ &= xDateJoinQry
                                End If
                                If xUACQry.Trim.Length > 0 Then
                                    StrQ &= xUACQry
                                End If
                                StrQ &= "WHERE error_description = '' AND requesttypeid = @requesttypeid "
                                If xDataFilterQry.Trim.Length > 0 Then
                                    StrQ &= xDataFilterQry
                                End If
                                If strFilter.Trim <> "" Then
                                    StrQ &= " AND " & strFilter & " "
                                End If
                        End Select
                    Case enOrbitServiceInfo.ORB_CREATEDEPOSITACCOUNT
                        Select Case eType
                            Case enViewType.VW_NEW
                                StrQ = "SELECT " & _
                                       "     hremployee_master.employeeunkid " & _
                                       "    ,CAST(0 AS BIT) AS iCheck " & _
                                       "    ,hremployee_master.employeecode + ' - ' + hremployee_master.firstname + ' ' + hremployee_master.surname AS employee " & _
                                       "    ,'' AS requestdate " & _
                                       "    ,'' AS referenceno " & _
                                       "    ,customer_number " & _
                                       "    ,'' AS error_description " & _
                                       "    ,'' AS primaryactnumber " & _
                                       "FROM hrorbit_request_tran " & _
                                       "    JOIN hremployee_master ON hrorbit_request_tran.employeeunkid = hremployee_master.employeeunkid "
                                If xAdvanceJoinQry.Trim.Length > 0 Then
                                    StrQ &= xAdvanceJoinQry
                                End If
                                If xDateJoinQry.Trim.Length > 0 Then
                                    StrQ &= xDateJoinQry
                                End If
                                If xUACQry.Trim.Length > 0 Then
                                    StrQ &= xUACQry
                                End If
                                StrQ &= "WHERE error_description = '' AND primaryactnumber = '' AND isprocessed = 0 AND requesttypeid = '" & CInt(enOrbitServiceInfo.ORB_CREATECUSTOMER) & "' "
                                If xDataFilterQry.Trim.Length > 0 Then
                                    StrQ &= xDataFilterQry
                                End If
                                If strFilter.Trim <> "" Then
                                    StrQ &= " AND " & strFilter & " "
                                End If

                            Case enViewType.VW_FAILED
                                StrQ = "SELECT " & _
                                       "     hremployee_master.employeeunkid " & _
                                       "    ,CAST(0 AS BIT) AS iCheck " & _
                                       "    ,hremployee_master.employeecode + ' - ' + hremployee_master.firstname + ' ' + hremployee_master.surname AS employee " & _
                                       "    ,requestdate " & _
                                       "    ,referenceno " & _
                                       "    ,customer_number " & _
                                       "    ,error_description " & _
                                       "    ,primaryactnumber " & _
                                       "FROM hrorbit_request_tran " & _
                                       "    JOIN hremployee_master ON hrorbit_request_tran.employeeunkid = hremployee_master.employeeunkid "
                                If xAdvanceJoinQry.Trim.Length > 0 Then
                                    StrQ &= xAdvanceJoinQry
                                End If
                                If xDateJoinQry.Trim.Length > 0 Then
                                    StrQ &= xDateJoinQry
                                End If
                                If xUACQry.Trim.Length > 0 Then
                                    StrQ &= xUACQry
                                End If
                                StrQ &= "WHERE error_description <> '' AND isprocessed = 0 AND requesttypeid = @requesttypeid "
                                If xDataFilterQry.Trim.Length > 0 Then
                                    StrQ &= xDataFilterQry
                                End If
                                If strFilter.Trim <> "" Then
                                    StrQ &= " AND " & strFilter & " "
                                End If
                            Case enViewType.VW_SUCCESS
                                StrQ = "SELECT " & _
                                       "     hremployee_master.employeeunkid " & _
                                       "    ,CAST(0 AS BIT) AS iCheck " & _
                                       "    ,hremployee_master.employeecode + ' - ' + hremployee_master.firstname + ' ' + hremployee_master.surname AS employee " & _
                                       "    ,requestdate " & _
                                       "    ,referenceno " & _
                                       "    ,customer_number " & _
                                       "    ,error_description " & _
                                       "    ,primaryactnumber " & _
                                       "FROM hrorbit_request_tran " & _
                                       "    JOIN hremployee_master ON hrorbit_request_tran.employeeunkid = hremployee_master.employeeunkid "
                                If xAdvanceJoinQry.Trim.Length > 0 Then
                                    StrQ &= xAdvanceJoinQry
                                End If
                                If xDateJoinQry.Trim.Length > 0 Then
                                    StrQ &= xDateJoinQry
                                End If
                                If xUACQry.Trim.Length > 0 Then
                                    StrQ &= xUACQry
                                End If
                                StrQ &= "WHERE error_description = '' AND requesttypeid = @requesttypeid "
                                If xDataFilterQry.Trim.Length > 0 Then
                                    StrQ &= xDataFilterQry
                                End If
                                If strFilter.Trim <> "" Then
                                    StrQ &= " AND " & strFilter & " "
                                End If
                        End Select

                        'Sohail (25 Apr 2020) -- Start
                        'FDRC Enhancement # : Orbit Integrtion for Bulk Payment Process.
                    Case enOrbitServiceInfo.ORB_PROCESSBULKPAYMENTS
                        Select Case eType
                            Case enViewType.VW_NEW
                                StrQ = "SELECT  hremployee_master.employeeunkid " & _
                                               ", CAST(0 AS BIT) AS IsChecked " & _
                                               ", CAST(0 AS BIT) AS IsGrp " & _
                                               ", hremployee_master.employeecode + ' - ' + hremployee_master.firstname + ' ' + hremployee_master.surname AS employee " & _
                                               ", ISNULL(prglobalvoc_master.globalvocno, prpayment_tran.voucherno) AS voucherno " & _
                                               ", ISNULL(hrmsConfiguration..cfbankbranch_master.branchcode, '') AS EMP_BRCode " & _
                                               ", ISNULL(premployee_bank_tran.accountno, '') AS EMP_AccountNo  " & _
                                               ", cfexchange_rate.currency_name " & _
                                               ", ISNULL(prempsalary_tran.expaidamt, 0 ) AS Amount  " & _
                                               ", ISNULL(prpayment_tran.expaidrate, 0 ) AS expaidrate  " & _
                                               ", ISNULL(prpayment_tran.chequeno,'') AS chequeno " & _
                                               ", ISNULL(prpayment_tran.accountno, '') AS CMP_Account " & _
                                               ", ISNULL(cfcompanybank_tran.companybanktranunkid, 0) AS companybanktranunkid " & _
                                               ", prpayment_authorize_tran.remarks " & _
                                               ", prpayment_tran.periodunkid " & _
                                               ", '' AS requestdate " & _
                                               ", '' AS referenceno " & _
                                               ", '' AS customer_number " & _
                                               ", '' AS error_description " & _
                                               ", '' AS primaryactnumber " & _
                                       "FROM prpayment_tran " & _
                                            "LEFT JOIN prglobalvoc_master ON prglobalvoc_master.globalvocunkid = prpayment_tran.globalvocno " & _
                                            "LEFT JOIN prempsalary_tran ON prempsalary_tran.paymenttranunkid = prpayment_tran.paymenttranunkid " & _
                                                "AND CONVERT(CHAR(8), prempsalary_tran.paymentdate, 112) = CONVERT(CHAR(8), prpayment_tran.paymentdate, 112) " & _
                                            "LEFT JOIN prpayment_authorize_tran ON prpayment_authorize_tran.paymenttranunkid = prpayment_tran.paymenttranunkid " & _
                                             "JOIN cfcommon_period_tran ON prpayment_tran.periodunkid = cfcommon_period_tran.periodunkid " & _
                                             "JOIN hremployee_master ON prempsalary_tran.employeeunkid = hremployee_master.employeeunkid " & _
                                             "JOIN premployee_bank_tran ON prempsalary_tran.empbanktranid = premployee_bank_tran.empbanktranunkid " & _
                                             "JOIN hrmsConfiguration..cfbankbranch_master ON premployee_bank_tran.branchunkid = hrmsConfiguration..cfbankbranch_master.branchunkid " & _
                                             "LEFT JOIN cfexchange_rate ON cfexchange_rate.exchangerateunkid = prpayment_tran.paidcurrencyid " & _
                                             "LEFT JOIN hrmsConfiguration..cfcompanybank_tran ON cfcompanybank_tran.branchunkid = prpayment_tran.branchunkid AND cfcompanybank_tran.account_no = prpayment_tran.accountno "

                                If xAdvanceJoinQry.Trim.Length > 0 Then
                                    StrQ &= xAdvanceJoinQry
                                End If
                                If xDateJoinQry.Trim.Length > 0 Then
                                    StrQ &= xDateJoinQry
                                End If
                                If xUACQry.Trim.Length > 0 Then
                                    StrQ &= xUACQry
                                End If

                                StrQ &= "WHERE   prpayment_tran.isvoid = 0 " & _
                                              "AND prempsalary_tran.isvoid = 0 " & _
                                              "AND premployee_bank_tran.isvoid = 0  " & _
                                              "AND prpayment_tran.isauthorized = 1  " & _
                                              "AND prpayment_authorize_tran.isactive = 1 " & _
                                              "AND hrmsConfiguration..cfbankbranch_master.isactive = 1  " & _
                                              "AND cfcompanybank_tran.companyunkid = @companyunkid " & _
                                              "AND prpayment_tran.referenceid = " & clsPayment_tran.enPaymentRefId.PAYSLIP & " " & _
                                              "AND prpayment_tran.paytypeid = " & clsPayment_tran.enPayTypeId.PAYMENT & " " & _
                                              "AND prpayment_tran.paymentmode IN ( " & enPaymentMode.CHEQUE & ", " & enPaymentMode.TRANSFER & " ) " & _
                                              "AND ISNULL(prglobalvoc_master.globalvocno, prpayment_tran.voucherno) NOT IN (SELECT reqmst_number FROM hrorbit_request_tran WHERE hrorbit_request_tran.iserror = 0 AND hrorbit_request_tran.error_description = '') "
                                If xDataFilterQry.Trim.Length > 0 Then
                                    StrQ &= xDataFilterQry
                                End If
                                If strFilter.Trim <> "" Then
                                    StrQ &= " AND " & strFilter & " "
                                End If
                            Case enViewType.VW_FAILED
                                StrQ = "SELECT  hremployee_master.employeeunkid " & _
                                               ", CAST(0 AS BIT) AS IsChecked " & _
                                               ", CAST(0 AS BIT) AS IsGrp " & _
                                               ", hremployee_master.employeecode + ' - ' + hremployee_master.firstname + ' ' + hremployee_master.surname AS employee " & _
                                               ", ISNULL(prglobalvoc_master.globalvocno, prpayment_tran.voucherno) AS voucherno " & _
                                               ", ISNULL(hrmsConfiguration..cfbankbranch_master.branchcode, '') AS EMP_BRCode " & _
                                               ", ISNULL(premployee_bank_tran.accountno, '') AS EMP_AccountNo  " & _
                                               ", cfexchange_rate.currency_name " & _
                                               ", ISNULL(prempsalary_tran.expaidamt, 0 ) AS Amount  " & _
                                               ", ISNULL(prpayment_tran.expaidrate, 0 ) AS expaidrate  " & _
                                               ", ISNULL(prpayment_tran.chequeno,'') AS chequeno " & _
                                               ", ISNULL(prpayment_tran.accountno, '') AS CMP_Account " & _
                                               ", ISNULL(cfcompanybank_tran.companybanktranunkid, 0) AS companybanktranunkid " & _
                                               ", prpayment_authorize_tran.remarks " & _
                                               ", prpayment_tran.periodunkid " & _
                                               ", requestdate " & _
                                               ", hrorbit_request_tran.referenceno " & _
                                               ", hrorbit_request_tran.customer_number " & _
                                               ", hrorbit_request_tran.error_description " & _
                                               ", '' AS primaryactnumber " & _
                                       "FROM prpayment_tran " & _
                                            "LEFT JOIN prglobalvoc_master ON prglobalvoc_master.globalvocunkid = prpayment_tran.globalvocno " & _
                                            "JOIN hrorbit_request_tran ON hrorbit_request_tran.reqmst_number = ISNULL(prglobalvoc_master.globalvocno, prpayment_tran.voucherno) " & _
                                            "LEFT JOIN prempsalary_tran ON prempsalary_tran.paymenttranunkid = prpayment_tran.paymenttranunkid " & _
                                                "AND CONVERT(CHAR(8), prempsalary_tran.paymentdate, 112) = CONVERT(CHAR(8), prpayment_tran.paymentdate, 112) " & _
                                            "LEFT JOIN prpayment_authorize_tran ON prpayment_authorize_tran.paymenttranunkid = prpayment_tran.paymenttranunkid " & _
                                             "JOIN cfcommon_period_tran ON prpayment_tran.periodunkid = cfcommon_period_tran.periodunkid " & _
                                             "JOIN hremployee_master ON prempsalary_tran.employeeunkid = hremployee_master.employeeunkid " & _
                                             "JOIN premployee_bank_tran ON prempsalary_tran.empbanktranid = premployee_bank_tran.empbanktranunkid " & _
                                             "JOIN hrmsConfiguration..cfbankbranch_master ON premployee_bank_tran.branchunkid = hrmsConfiguration..cfbankbranch_master.branchunkid " & _
                                             "LEFT JOIN cfexchange_rate ON cfexchange_rate.exchangerateunkid = prpayment_tran.paidcurrencyid " & _
                                             "LEFT JOIN hrmsConfiguration..cfcompanybank_tran ON cfcompanybank_tran.branchunkid = prpayment_tran.branchunkid AND cfcompanybank_tran.account_no = prpayment_tran.accountno "

                                If xAdvanceJoinQry.Trim.Length > 0 Then
                                    StrQ &= xAdvanceJoinQry
                                End If
                                If xDateJoinQry.Trim.Length > 0 Then
                                    StrQ &= xDateJoinQry
                                End If
                                If xUACQry.Trim.Length > 0 Then
                                    StrQ &= xUACQry
                                End If

                                StrQ &= "WHERE   prpayment_tran.isvoid = 0 " & _
                                              "AND prempsalary_tran.isvoid = 0 " & _
                                              "AND premployee_bank_tran.isvoid = 0  " & _
                                              "AND prpayment_tran.isauthorized = 1  " & _
                                              "AND prpayment_authorize_tran.isactive = 1 " & _
                                              "AND hrmsConfiguration..cfbankbranch_master.isactive = 1  " & _
                                              "AND cfcompanybank_tran.companyunkid = @companyunkid " & _
                                              "AND prpayment_tran.referenceid = " & clsPayment_tran.enPaymentRefId.PAYSLIP & " " & _
                                              "AND prpayment_tran.paytypeid = " & clsPayment_tran.enPayTypeId.PAYMENT & " " & _
                                              "AND prpayment_tran.paymentmode IN ( " & enPaymentMode.CHEQUE & ", " & enPaymentMode.TRANSFER & " ) " & _
                                              "AND  hrorbit_request_tran.error_description <> '' AND hrorbit_request_tran.isprocessed = 0 AND hrorbit_request_tran.requesttypeid = @requesttypeid "
                                If xDataFilterQry.Trim.Length > 0 Then
                                    StrQ &= xDataFilterQry
                                End If
                                If strFilter.Trim <> "" Then
                                    StrQ &= " AND " & strFilter & " "
                                End If
                            Case enViewType.VW_SUCCESS
                                StrQ = "SELECT  hremployee_master.employeeunkid " & _
                                               ", CAST(0 AS BIT) AS IsChecked " & _
                                               ", CAST(0 AS BIT) AS IsGrp " & _
                                               ", hremployee_master.employeecode + ' - ' + hremployee_master.firstname + ' ' + hremployee_master.surname AS employee " & _
                                               ", ISNULL(prglobalvoc_master.globalvocno, prpayment_tran.voucherno) AS voucherno " & _
                                               ", ISNULL(hrmsConfiguration..cfbankbranch_master.branchcode, '') AS EMP_BRCode " & _
                                               ", ISNULL(premployee_bank_tran.accountno, '') AS EMP_AccountNo  " & _
                                               ", cfexchange_rate.currency_name " & _
                                               ", ISNULL(prempsalary_tran.expaidamt, 0 ) AS Amount  " & _
                                               ", ISNULL(prpayment_tran.expaidrate, 0 ) AS expaidrate  " & _
                                               ", ISNULL(prpayment_tran.chequeno,'') AS chequeno " & _
                                               ", ISNULL(prpayment_tran.accountno, '') AS CMP_Account " & _
                                               ", ISNULL(cfcompanybank_tran.companybanktranunkid, 0) AS companybanktranunkid " & _
                                               ", prpayment_authorize_tran.remarks " & _
                                               ", prpayment_tran.periodunkid " & _
                                               ", requestdate " & _
                                               ", hrorbit_request_tran.referenceno " & _
                                               ", hrorbit_request_tran.customer_number " & _
                                               ", hrorbit_request_tran.error_description " & _
                                               ", '' AS primaryactnumber " & _
                                       "FROM prpayment_tran " & _
                                            "LEFT JOIN prglobalvoc_master ON prglobalvoc_master.globalvocunkid = prpayment_tran.globalvocno " & _
                                            "JOIN hrorbit_request_tran ON hrorbit_request_tran.reqmst_number = ISNULL(prglobalvoc_master.globalvocno, prpayment_tran.voucherno) " & _
                                            "LEFT JOIN prempsalary_tran ON prempsalary_tran.paymenttranunkid = prpayment_tran.paymenttranunkid " & _
                                                "AND CONVERT(CHAR(8), prempsalary_tran.paymentdate, 112) = CONVERT(CHAR(8), prpayment_tran.paymentdate, 112) " & _
                                            "LEFT JOIN prpayment_authorize_tran ON prpayment_authorize_tran.paymenttranunkid = prpayment_tran.paymenttranunkid " & _
                                             "JOIN cfcommon_period_tran ON prpayment_tran.periodunkid = cfcommon_period_tran.periodunkid " & _
                                             "JOIN hremployee_master ON prempsalary_tran.employeeunkid = hremployee_master.employeeunkid " & _
                                             "JOIN premployee_bank_tran ON prempsalary_tran.empbanktranid = premployee_bank_tran.empbanktranunkid " & _
                                             "JOIN hrmsConfiguration..cfbankbranch_master ON premployee_bank_tran.branchunkid = hrmsConfiguration..cfbankbranch_master.branchunkid " & _
                                             "LEFT JOIN cfexchange_rate ON cfexchange_rate.exchangerateunkid = prpayment_tran.paidcurrencyid " & _
                                             "LEFT JOIN hrmsConfiguration..cfcompanybank_tran ON cfcompanybank_tran.branchunkid = prpayment_tran.branchunkid AND cfcompanybank_tran.account_no = prpayment_tran.accountno "

                                If xAdvanceJoinQry.Trim.Length > 0 Then
                                    StrQ &= xAdvanceJoinQry
                                End If
                                If xDateJoinQry.Trim.Length > 0 Then
                                    StrQ &= xDateJoinQry
                                End If
                                If xUACQry.Trim.Length > 0 Then
                                    StrQ &= xUACQry
                                End If

                                StrQ &= "WHERE   prpayment_tran.isvoid = 0 " & _
                                              "AND prempsalary_tran.isvoid = 0 " & _
                                              "AND premployee_bank_tran.isvoid = 0  " & _
                                              "AND prpayment_tran.isauthorized = 1  " & _
                                              "AND prpayment_authorize_tran.isactive = 1 " & _
                                              "AND hrmsConfiguration..cfbankbranch_master.isactive = 1  " & _
                                              "AND cfcompanybank_tran.companyunkid = @companyunkid " & _
                                              "AND prpayment_tran.referenceid = " & clsPayment_tran.enPaymentRefId.PAYSLIP & " " & _
                                              "AND prpayment_tran.paytypeid = " & clsPayment_tran.enPayTypeId.PAYMENT & " " & _
                                              "AND prpayment_tran.paymentmode IN ( " & enPaymentMode.CHEQUE & ", " & enPaymentMode.TRANSFER & " ) " & _
                                              "AND  hrorbit_request_tran.error_description = '' AND hrorbit_request_tran.requesttypeid = @requesttypeid "
                                If xDataFilterQry.Trim.Length > 0 Then
                                    StrQ &= xDataFilterQry
                                End If
                                If strFilter.Trim <> "" Then
                                    StrQ &= " AND " & strFilter & " "
                                End If
                        End Select
                        'Sohail (25 Apr 2020) -- End

                End Select
                objDo.AddParameter("@requesttypeid", SqlDbType.Int, eZeeDataType.INT_SIZE, eSrvType)
                objDo.AddParameter("@companyunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, xCompanyUnkid)

                If StrQ.Trim.Length > 0 Then
                    dsList = objDo.ExecQuery(StrQ, "List")
                    If objDo.ErrorMessage <> "" Then
                        exForce = New Exception(objDo.ErrorNumber & ": " & objDo.ErrorMessage)
                        Throw exForce
                    End If

                    'Sohail (25 Apr 2020) -- Start
                    'FDRC Enhancement # : Orbit Integrtion for Bulk Payment Process.
                    If CInt(eSrvType) = enOrbitServiceInfo.ORB_PROCESSBULKPAYMENTS Then
                        Dim dt As DataTable = New DataView(dsList.Tables(0)).ToTable(True, "voucherno")
                        Dim dtCol As New DataColumn("IsChecked", System.Type.GetType("System.Boolean"))
                        dtCol.DefaultValue = False
                        dtCol.AllowDBNull = False
                        dt.Columns.Add(dtCol)

                        dtCol = New DataColumn("IsGrp", System.Type.GetType("System.Boolean"))
                        dtCol.DefaultValue = True
                        dtCol.AllowDBNull = False
                        dt.Columns.Add(dtCol)

                        dt.Merge(dsList.Tables(0), False)
                        dt.DefaultView.Sort = "voucherno, employee, employeeunkid, IsGrp DESC"

                        dsList.Tables.Clear()
                        dsList.Tables.Add(dt.DefaultView.ToTable)
                    End If
                    'Sohail (25 Apr 2020) -- End

                End If

            End Using
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: GetDisplayList; Module Name: " & mstrModuleName)
        Finally
        End Try
        If dsList IsNot Nothing AndAlso dsList.Tables.Count > 0 Then Return dsList.Tables(0)
    End Function

    Public Function AssignBankAccount(ByVal intEmployeeId As Integer, _
                                      ByVal intEffectivePeriodId As Integer, _
                                      ByVal intBankGroupId As Integer, _
                                      ByVal intBranchId As Integer, _
                                      ByVal intAccountTypeId As Integer, _
                                      ByVal intDistributionModeId As Integer, _
                                      ByVal decDistributionValue As Decimal, _
                                      ByVal strAccountNumber As String, _
                                      ByRef xMsg As String, _
                                      ByRef blnIsErrorOnRequest As Boolean) As Boolean 'S.SANDEEP |30-MAY-2020| -- START {blnIsErrorOnRequest} -- END
        Try
            mstrMessage = ""
            Dim ds As DataSet : Dim objEmpBanks As New clsEmployeeBanks
            Dim objPrd As New clscommom_period_Tran
            objPrd._Periodunkid(FinancialYear._Object._DatabaseName) = intEffectivePeriodId
            ds = objEmpBanks.GetList(FinancialYear._Object._DatabaseName, User._Object._Userunkid, _
                                     FinancialYear._Object._YearUnkid, Company._Object._Companyunkid, _
                                     objPrd._Start_Date, objPrd._End_Date, ConfigParameter._Object._UserAccessModeSetting, True, _
                                     ConfigParameter._Object._IsIncludeInactiveEmp, "Banks", True, , intEmployeeId.ToString, objPrd._End_Date, _
                                     "end_date DESC, EmpName, priority")

            If ds.Tables(0).Rows.Count > 0 Then
                If ds.Tables(0).Select("periodunkid=" & intEffectivePeriodId).Length > 0 Then
                    mstrMessage = Language.getMessage(mstrModuleName, 16, "Sorry, Employee bank already present for the selected effective period.")
                    Exit Try
                End If
            End If

            Dim mdtTran As New DataTable
            Dim dtCol As DataColumn
            dtCol = New DataColumn("UnkId", System.Type.GetType("System.Int32"))
            dtCol.AutoIncrement = True
            dtCol.AutoIncrementSeed = 1
            mdtTran.Columns.Add(dtCol)
            mdtTran.Merge(ds.Tables("Banks"))

            Dim dtEBRow As DataRow
            dtEBRow = mdtTran.NewRow

            dtEBRow.Item("empbanktranunkid") = -1
            dtEBRow.Item("employeeunkid") = intEmployeeId
            dtEBRow.Item("BankGrpUnkid") = intBankGroupId
            dtEBRow.Item("branchunkid") = intBranchId
            dtEBRow.Item("accounttypeunkid") = intAccountTypeId
            dtEBRow.Item("accountno") = strAccountNumber
            If intDistributionModeId = enPaymentBy.Percentage Then
                dtEBRow.Item("percentage") = Format(decDistributionValue, "00.00")
                dtEBRow.Item("amount") = Format(0, "00.00")
                dtEBRow.Item("priority") = 1
            Else
                dtEBRow.Item("percentage") = Format(0, "00.00")
                dtEBRow.Item("amount") = Format(decDistributionValue, GUI.fmtCurrency)
                dtEBRow.Item("priority") = 1
            End If
            dtEBRow.Item("modeid") = intDistributionModeId
            dtEBRow.Item("periodunkid") = intEffectivePeriodId
            dtEBRow.Item("end_date") = eZeeDate.convertDate(objPrd._End_Date)
            dtEBRow.Item("AUD") = "A"
            dtEBRow.Item("GUID") = Guid.NewGuid().ToString

            mdtTran.Rows.Add(dtEBRow)

            objEmpBanks._DataTable = mdtTran
            objEmpBanks._Userunkid = User._Object._Userunkid
            objEmpBanks.InsertUpdateDelete_EmpBanks(ConfigParameter._Object._CurrentDateAndTime)

            If objEmpBanks._Message <> "" Then
                blnIsErrorOnRequest = True 'S.SANDEEP |30-MAY-2020| -- START -- END
                mstrMessage = objEmpBanks._Message
                Exit Try
            Else
                Using objDo As New clsDataOperation
                    objDo.ExecNonQuery("UPDATE hrorbit_request_tran set isassigned = 1 WHERE primaryactnumber = '" & strAccountNumber & "' ")

                    If objDo.ErrorMessage <> "" Then
                        mstrMessage = objDo.ErrorNumber & ": " & objDo.ErrorMessage
                        Exit Try
                    End If

                End Using
            End If

            Return True
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: AssignBankAccount; Module Name: " & mstrModuleName)
        Finally
        End Try
        xMsg = mstrMessage
    End Function

#End Region

    '<Language> This Auto Generated Text Please Do Not Modify it.
#Region " Language & UI Settings "
    Public Shared Sub SetMessages()
        Try
            Language.setMessage(mstrModuleName, 1, "Sorry, employee photo size cannot be greater than 15 kb.")
            Language.setMessage(mstrModuleName, 2, "Sorry, No identity is defined for this employee. Please define identity in order to create customer")
            Language.setMessage(mstrModuleName, 3, "Sorry, Identification number cannot be less than 10 digits.")
            Language.setMessage(mstrModuleName, 4, "Sorry, Expiry date is mandatory information. Please provide expiry date.")
            Language.setMessage(mstrModuleName, 5, "Sorry, Present Moblie number cannot be less than 12 digits.")
            Language.setMessage(mstrModuleName, 6, "Sorry, Emplyoee photo is mandatory information. Please provide employee photo.")
            Language.setMessage(mstrModuleName, 7, "Sorry, Emplyoee signature is mandatory information. Please provide employee signature.")
            Language.setMessage(mstrModuleName, 8, "Sorry, Employee branch is mandatory information. Please provide employee branch to continue.")
            Language.setMessage(mstrModuleName, 9, "Sorry, Present address is mandatory information. Please provide present address to continue.")
            Language.setMessage(mstrModuleName, 10, "Sorry, Present state is mandatory information. Please provide presemt state to continue.")
            Language.setMessage(mstrModuleName, 11, "Sorry, Present town is mandatory information. Please provide presemt town to continue.")
            Language.setMessage(mstrModuleName, 12, "Sorry, Present country is mandatory information. Please provide presemt country to continue.")
            Language.setMessage(mstrModuleName, 13, "Sorry, Nationality is mandatory information. Please provide nationality to continue.")
            Language.setMessage(mstrModuleName, 14, "Sorry, Birthdate is mandatory information. Please provide birthdate to continue.")
            Language.setMessage(mstrModuleName, 15, "Sorry, Title is mandatory information. Please provide title to continue.")
            Language.setMessage(mstrModuleName, 16, "Sorry, Employee bank already present for the selected effective period.")
            Language.setMessage(mstrModuleName, 17, "410")
            Language.setMessage(mstrModuleName, 18, "455")
            Language.setMessage(mstrModuleName, 19, "Sorry, Birth country is mandatory information. Please provide birthdate to continue.")
Language.setMessage(mstrModuleName, 20, "Payment Authorozed")

        Catch Ex As Exception
            DisplayError.Show("-1", Ex.Message, "SetMessages", mstrModuleName)
        End Try
    End Sub
#End Region 'Language & UI Settings
    '</Language>
End Class