﻿Public Class processBulkPayments
    Private _referenceNo As String = String.Empty
    Private _transmissionTime As String = String.Empty
    Private _credentials As New credentials
    Private _txnBatchCode As String = String.Empty
    Private _bulkPaymentDesc As String = String.Empty
    Private _businessUnitId As String = String.Empty
    Private _recurringFlag As String = String.Empty
    Private _serviceChannelCode As String = String.Empty
    Private _channelId As String = String.Empty
    Private _bulkItemArray As New List(Of bulkItemArray)

    Public Property bulkPaymentDesc() As String
        Get
            Return _bulkPaymentDesc
        End Get
        Set(ByVal value As String)
            _bulkPaymentDesc = value
        End Set
    End Property

    'Public Property transmissionTime() As String
    '    Get
    '        Return _transmissionTime
    '    End Get
    '    Set(ByVal value As String)
    '        _transmissionTime = value
    '    End Set
    'End Property

    Public Property referenceNo() As String
        Get
            Return _referenceNo
        End Get
        Set(ByVal value As String)
            _referenceNo = value
        End Set
    End Property

    Public Property credentials() As credentials
        Get
            Return _credentials
        End Get
        Set(ByVal value As credentials)
            _credentials = value
        End Set
    End Property

    Public Property bulkItemArray() As List(Of bulkItemArray)
        Get
            Return _bulkItemArray
        End Get
        Set(ByVal value As List(Of bulkItemArray))
            _bulkItemArray = value
        End Set
    End Property

    Public Property txnBatchCode() As String
        Get
            Return _txnBatchCode
        End Get
        Set(ByVal value As String)
            _txnBatchCode = value
        End Set
    End Property

    Public Property recurringFlag() As String
        Get
            Return _recurringFlag
        End Get
        Set(ByVal value As String)
            _recurringFlag = value
        End Set
    End Property

    'Public Property serviceChannelCode() As String
    '    Get
    '        Return _serviceChannelCode
    '    End Get
    '    Set(ByVal value As String)
    '        _serviceChannelCode = value
    '    End Set
    'End Property

    'Public Property channelId() As String
    '    Get
    '        Return _channelId
    '    End Get
    '    Set(ByVal value As String)
    '        _channelId = value
    '    End Set
    'End Property

    Public Property businessUnitId() As String
        Get
            Return _businessUnitId
        End Get
        Set(ByVal value As String)
            _businessUnitId = value
        End Set
    End Property


End Class

Public Class bulkItemArray
    Private _itemAccountNo As String = String.Empty
    Private _currencyCode As String = String.Empty
    Private _txnAmount As Decimal = 0
    Private _narrative As String = String.Empty
    Private _referenceNo As String = String.Empty
    Private _exchangeRate As String = String.Empty
    Private _transferType As String = String.Empty

    Public Property itemAccountNo() As String
        Get
            Return _itemAccountNo
        End Get
        Set(ByVal value As String)
            _itemAccountNo = value
        End Set
    End Property

    Public Property referenceNo() As String
        Get
            Return _referenceNo
        End Get
        Set(ByVal value As String)
            _referenceNo = value
        End Set
    End Property

    Public Property exchangeRate() As String
        Get
            Return _exchangeRate
        End Get
        Set(ByVal value As String)
            _exchangeRate = value
        End Set
    End Property

    Public Property narrative() As String
        Get
            Return _narrative
        End Get
        Set(ByVal value As String)
            _narrative = value
        End Set
    End Property

    Public Property transferType() As String
        Get
            Return _transferType
        End Get
        Set(ByVal value As String)
            _transferType = value
        End Set
    End Property


    Public Property currencyCode() As String
        Get
            Return _currencyCode
        End Get
        Set(ByVal value As String)
            _currencyCode = value
        End Set
    End Property

    Public Property txnAmount() As Decimal
        Get
            Return _txnAmount
        End Get
        Set(ByVal value As Decimal)
            _txnAmount = value
        End Set
    End Property

    Public Sub New()

    End Sub

    Public Sub New(ByVal itemAccountNo As String, ByVal currencyCode As String, ByVal txnAmount As Decimal, ByVal narrative As String, ByVal referenceNo As String, ByVal exchangeRate As String, ByVal transferType As String)
        _itemAccountNo = itemAccountNo
        _currencyCode = currencyCode
        _txnAmount = txnAmount
        _narrative = narrative
        _referenceNo = referenceNo
        _exchangeRate = exchangeRate
        _transferType = transferType
    End Sub

End Class
