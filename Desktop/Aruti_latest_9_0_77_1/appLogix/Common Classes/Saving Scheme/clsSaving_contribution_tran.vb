﻿'************************************************************************************************************************************
'Class Name : clsSaving_contribution_tran.vb
'Purpose    :
'Date       :13/01/2015
'Written By :Sohail
'Modified   :
'************************************************************************************************************************************

Imports eZeeCommonLib
Imports eZee.Common.eZeeForm
''' <summary>
''' Purpose: 
''' Developer: Sohail
''' </summary>

Public Class clsSaving_contribution_tran
    Private Const mstrModuleName = "clsSaving_contribution_tran"
    Dim objDataOperation As clsDataOperation
    Dim mstrMessage As String = ""

#Region " Private variables "
    Private mintSavingcontributiontranunkid As Integer
    Private mintSavingtranunkid As Integer
    Private mintPeriodunkid As Integer
    Private msinContribution As Single
    Private mintUserunkid As Integer
    Private mblnIsvoid As Boolean
    Private mintVoiduserunkid As Integer
    Private mdtVoiddatetime As Date
    Private mstrVoidreason As String = String.Empty

    Private mdtTran As DataTable
    Private mstrWebFormName As String = String.Empty
    Private mstrWebIP As String = ""
    Private mstrWebHost As String = ""
#End Region

#Region " Properties "

'S.SANDEEP [28-May-2018] -- START 
'ISSUE/ENHANCEMENT : {Audit Trails} 
Private mstrFormName As String = String.Empty
Public WriteOnly Property _FormName() As String 
Set(ByVal value As String) 
mstrFormName = value 
End Set 
End Property 
 
Private mstrClientIP As String = ""
Public WriteOnly Property _ClientIP() As String 
Set(ByVal value As String) 
mstrClientIP = value 
End Set 
End Property 
 
Private mstrHostName As String = ""
Public WriteOnly Property _HostName() As String 
Set(ByVal value As String) 
mstrHostName = value 
End Set 
End Property 
 
Private mblnIsWeb As Boolean = False
Public WriteOnly Property _FromWeb() As Boolean 
Set(ByVal value As Boolean) 
mblnIsWeb = value 
End Set 
End Property 
 
    Private mintAuditUserId As Integer = 0
    Public WriteOnly Property _AuditUserId() As Integer
        Set(ByVal value As Integer)
            mintAuditUserId = value
        End Set
    End Property

    Private mintCompanyUnkid As Integer = 0
    Public WriteOnly Property _CompanyUnkid() As Integer
        Set(ByVal value As Integer)
            mintCompanyUnkid = value
        End Set
    End Property
 
Private mdtAuditDate As DateTime = Now
Public WriteOnly Property _AuditDate() As DateTime 
Set(ByVal value As DateTime) 
mdtAuditDate = value 
End Set 
End Property 
 
Private xDataOpr As clsDataOperation = Nothing
Public Property _xDataOpr() As clsDataOperation 
Get 
Return xDataOpr 
End Get 
Set(ByVal value As clsDataOperation) 
xDataOpr = value 
End Set 
End Property 
 
Private mintLoginEmployeeunkid As Integer = 0
Public Property _LoginEmployeeunkid() As Integer 
Get 
Return mintLoginEmployeeunkid 
End Get 
Set(ByVal value As Integer) 
mintLoginEmployeeunkid = value 
End Set 
End Property 
 
Private mintVoidlogingemployeeunkid As Integer=0
Public Property _Voidloginemployeeunkid() As Integer 
Get 
Return mintVoidlogingemployeeunkid 
End Get 
Set(ByVal value As Integer) 
mintVoidlogingemployeeunkid = value 
End Set 
End Property 
'S.SANDEEP [28-May-2018] -- END 

    Public ReadOnly Property _Message() As String
        Get
            Return mstrMessage
        End Get
    End Property

    'Public Property _Savingcontributiontranunkid() As Integer
    '    Get
    '        Return mintSavingcontributiontranunkid
    '    End Get
    '    Set(ByVal value As Integer)
    '        mintSavingcontributiontranunkid = Value
    '        Call getData()
    '    End Set
    'End Property

    ''' <summary>
    ''' Purpose: Get or Set savingtranunkid
    ''' Modify By: Sohail
    ''' </summary>
    Public Property _Savingtranunkid() As Integer
        Get
            Return mintSavingtranunkid
        End Get
        Set(ByVal value As Integer)
            mintSavingtranunkid = value
            Call GetData()
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set periodunkid
    ''' Modify By: Sohail
    ''' </summary>
    Public Property _Periodunkid() As Integer
        Get
            Return mintPeriodunkid
        End Get
        Set(ByVal value As Integer)
            mintPeriodunkid = Value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set contribution
    ''' Modify By: Sohail
    ''' </summary>
    Public Property _Contribution() As Single
        Get
            Return msinContribution
        End Get
        Set(ByVal value As Single)
            msinContribution = Value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set userunkid
    ''' Modify By: Sohail
    ''' </summary>
    Public Property _Userunkid() As Integer
        Get
            Return mintUserunkid
        End Get
        Set(ByVal value As Integer)
            mintUserunkid = Value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set isvoid
    ''' Modify By: Sohail
    ''' </summary>
    Public Property _Isvoid() As Boolean
        Get
            Return mblnIsvoid
        End Get
        Set(ByVal value As Boolean)
            mblnIsvoid = Value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set voiduserunkid
    ''' Modify By: Sohail
    ''' </summary>
    Public Property _Voiduserunkid() As Integer
        Get
            Return mintVoiduserunkid
        End Get
        Set(ByVal value As Integer)
            mintVoiduserunkid = Value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set voiddatetime
    ''' Modify By: Sohail
    ''' </summary>
    Public Property _Voiddatetime() As Date
        Get
            Return mdtVoiddatetime
        End Get
        Set(ByVal value As Date)
            mdtVoiddatetime = Value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set voidreason
    ''' Modify By: Sohail
    ''' </summary>
    Public Property _Voidreason() As String
        Get
            Return mstrVoidreason
        End Get
        Set(ByVal value As String)
            mstrVoidreason = Value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set Tran Table
    ''' Modify By: SHANI
    ''' </summary>
    Public Property _DataTable() As DataTable
        Get
            Return mdtTran
        End Get
        Set(ByVal value As DataTable)
            mdtTran = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set WebFormName
    ''' Modify By: SHANI
    ''' </summary>
    Public Property _WebFormName() As String
        Get
            Return mstrWebFormName
        End Get
        Set(ByVal value As String)
            mstrWebFormName = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set WebIP
    ''' Modify By: SHANI
    ''' </summary>
    Public Property _WebIP() As String
        Get
            Return mstrWebIP
        End Get
        Set(ByVal value As String)
            mstrWebIP = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set WebHOST
    ''' Modify By: SHANI
    ''' </summary>
    Public Property _WebHost() As String
        Get
            Return mstrWebHost
        End Get
        Set(ByVal value As String)
            mstrWebHost = value
        End Set
    End Property


#End Region

    ''' <summary>
    ''' Modify By: Sohail
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>
    Public Sub GetData()
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        objDataOperation = New clsDataOperation

        Try
            strQ = "SELECT " & _
                    "cfcommon_period_tran.period_name " & _
                    ",svsaving_contribution_tran.periodunkid " & _
                    ",svsaving_contribution_tran.contribution " & _
                    ",svsaving_contribution_tran.userunkid " & _
                    ",svsaving_contribution_tran.savingcontributiontranunkid " & _
                    ",svsaving_contribution_tran.savingtranunkid " & _
                    ",svsaving_contribution_tran.isvoid " & _
                    ",svsaving_contribution_tran.voiduserunkid " & _
                    ",svsaving_contribution_tran.voiddatetime " & _
                    ",svsaving_contribution_tran.voidreason " & _
                    ",'' AS AUD " & _
                    ",'' AS GUID " & _
                    ",cfcommon_period_tran.end_date AS contributiondate " & _
             "FROM svsaving_contribution_tran " & _
                    "JOIN cfcommon_period_tran ON svsaving_contribution_tran.periodunkid=cfcommon_period_tran.periodunkid " & _
                    "WHERE svsaving_contribution_tran.isvoid=0 AND svsaving_contribution_tran.savingtranunkid=@savingtranunkid "

            objDataOperation.AddParameter("@savingtranunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintSavingtranunkid.ToString)

            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If
            mdtTran = dsList.Tables(0)

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: GetData; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
    End Sub


    ''' <summary>
    ''' Modify By: Sohail
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>
    Public Function GetList(ByVal strTableName As String, Optional ByVal blnOnlyActive As Boolean = True) As DataSet
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        objDataOperation = New clsDataOperation

        Try
            StrQ = "SELECT " & _
              "  savingcontributiontranunkid " & _
              ", savingtranunkid " & _
              ", periodunkid " & _
              ", contribution " & _
              ", userunkid " & _
              ", isvoid " & _
              ", voiduserunkid " & _
              ", voiddatetime " & _
              ", voidreason " & _
             "FROM svsaving_contribution_tran "

            If blnOnlyActive Then
                strQ &= " WHERE isactive = 1 "
            End If

            dsList = objDataOperation.ExecQuery(strQ, strTableName)

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: GetList; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
        Return dsList
    End Function


    ''' <summary>
    ''' Modify By: Sohail
    ''' </summary>
    ''' <returns>Boolean</returns>
    ''' <purpose> INSERT INTO Database Table (svsaving_contribution_tran) </purpose>
    ''' Shani(24-Aug-2015) -- Start
    ''' ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
    ''' Public Function Insert(ByVal Item As DataRow, ByVal objData As clsDataOperation) As Boolean
    Public Function Insert(ByVal Item As DataRow, ByVal objData As clsDataOperation, ByVal xCurrentDateAndTime As DateTime) As Boolean
        'Shani(24-Aug-2015) -- End

        'If isExist(mstrName) Then
        '    mstrMessage = "<Message>"
        '    Return False
        'End If

        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        objDataOperation = objData

        Try
            objDataOperation.ClearParameters()
            objDataOperation.AddParameter("@savingtranunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintSavingtranunkid.ToString)
            objDataOperation.AddParameter("@periodunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, Item("periodunkid").ToString)
            objDataOperation.AddParameter("@contribution", SqlDbType.Decimal, eZeeDataType.FLOAT_SIZE, Item("contribution").ToString)
            objDataOperation.AddParameter("@userunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, Item("userunkid").ToString)
            objDataOperation.AddParameter("@isvoid", SqlDbType.Bit, eZeeDataType.BIT_SIZE, False)
            objDataOperation.AddParameter("@voiduserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, -1)
            objDataOperation.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, DBNull.Value)
            objDataOperation.AddParameter("@voidreason", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Item("voidreason").ToString)

            strQ = "INSERT INTO svsaving_contribution_tran ( " & _
              "  savingtranunkid " & _
              ", periodunkid " & _
              ", contribution " & _
              ", userunkid " & _
              ", isvoid " & _
              ", voiduserunkid " & _
              ", voiddatetime " & _
              ", voidreason" & _
            ") VALUES (" & _
              "  @savingtranunkid " & _
              ", @periodunkid " & _
              ", @contribution " & _
              ", @userunkid " & _
              ", @isvoid " & _
              ", @voiduserunkid " & _
              ", @voiddatetime " & _
              ", @voidreason" & _
            "); SELECT @@identity"

            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            mintSavingcontributiontranunkid = dsList.Tables(0).Rows(0).Item(0)


            'Shani(24-Aug-2015) -- Start
            'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
            'If InsertAuditSavingContribution(objDataOperation, Item, 1) = False Then
            If InsertAuditSavingContribution(objDataOperation, Item, 1, xCurrentDateAndTime) = False Then
                'Shani(24-Aug-2015) -- End
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            Return True
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: Insert; Module Name: " & mstrModuleName)
            Return False
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
        End Try
    End Function

    ''' <summary>
    ''' Modify By: Sohail
    ''' </summary>
    ''' <returns>Boolean</returns>
    ''' <purpose> Update Database Table (svsaving_contribution_tran) </purpose>
    ''' Shani(24-Aug-2015) -- Start
    ''' ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
    ''' Public Function Update(ByVal Item As DataRow, ByVal objData As clsDataOperation) As Boolean
    Public Function Update(ByVal Item As DataRow, ByVal objData As clsDataOperation, ByVal xCurrentDateAndTime As DateTime) As Boolean
        'Shani(24-Aug-2015) -- End

        'If isExist(mstrName, mintSavingcontributiontranunkid) Then
        '    mstrMessage = "<Message>"
        '    Return False
        'End If

        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        objDataOperation = objData

        Try
            objDataOperation.ClearParameters()
            objDataOperation.AddParameter("@savingcontributiontranunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, Item("savingcontributiontranunkid").ToString)
            objDataOperation.AddParameter("@savingtranunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, Item("savingtranunkid").ToString)
            objDataOperation.AddParameter("@periodunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, Item("periodunkid").ToString)
            objDataOperation.AddParameter("@contribution", SqlDbType.Decimal, eZeeDataType.FLOAT_SIZE, Item("contribution").ToString)
            objDataOperation.AddParameter("@userunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, Item("userunkid").ToString)
            objDataOperation.AddParameter("@isvoid", SqlDbType.Bit, eZeeDataType.BIT_SIZE, False)
            objDataOperation.AddParameter("@voiduserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, -1)
            objDataOperation.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, DBNull.Value)
            objDataOperation.AddParameter("@voidreason", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Item("voidreason").ToString)

            strQ = "UPDATE svsaving_contribution_tran SET " & _
              "  savingtranunkid = @savingtranunkid" & _
              ", periodunkid = @periodunkid" & _
              ", contribution = @contribution" & _
              ", userunkid = @userunkid" & _
              ", isvoid = @isvoid" & _
              ", voiduserunkid = @voiduserunkid" & _
              ", voiddatetime = @voiddatetime" & _
              ", voidreason = @voidreason " & _
            "WHERE savingcontributiontranunkid = @savingcontributiontranunkid "

            Call objDataOperation.ExecNonQuery(strQ)

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            mintSavingcontributiontranunkid = Item("savingcontributiontranunkid")


            'Shani(24-Aug-2015) -- Start
            'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
            'If InsertAuditSavingContribution(objDataOperation, Item, 2) = False Then
            If InsertAuditSavingContribution(objDataOperation, Item, 2, xCurrentDateAndTime) = False Then
                'Shani(24-Aug-2015) -- End

                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            Return True
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: Update; Module Name: " & mstrModuleName)
            Return False
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
        End Try
    End Function

    ''' <summary>
    ''' Modify By: Sohail
    ''' </summary>
    ''' <returns>Boolean</returns>
    ''' <purpose> Delete Database Table (svsaving_contribution_tran) </purpose>
    ''' Shani(24-Aug-2015) -- Start
    ''' ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
    ''' Public Function Delete(ByVal Item As DataRow, ByVal objData As clsDataOperation) As Boolean
    Public Function Delete(ByVal Item As DataRow, ByVal objData As clsDataOperation, ByVal xCurrentDateAndTime As DateTime) As Boolean
        'Shani(24-Aug-2015) -- End

        'If isUsed(intUnkid) Then
        '    mstrMessage = "<Message>"
        '    Return False
        'End If

        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        objDataOperation = objData

        Try
            objDataOperation.ClearParameters()
            strQ = "UPDATE svsaving_contribution_tran SET " & _
                     " isvoid = @isvoid" & _
                     ", voiduserunkid = @voiduserunkid" & _
                     ", voiddatetime = @voiddatetime" & _
                     ", voidreason = @voidreason " & _
            "WHERE savingcontributiontranunkid = @savingcontributiontranunkid "

            objDataOperation.AddParameter("@savingcontributiontranunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, Item("savingcontributiontranunkid").ToString)
            objDataOperation.AddParameter("@isvoid", SqlDbType.Bit, eZeeDataType.BIT_SIZE, Item("isvoid").ToString)
            objDataOperation.AddParameter("@voiduserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, Item("voiduserunkid").ToString)
            If IsDBNull(Item("Voiddatetime")) = False Then
                objDataOperation.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, CDate(Item("voiddatetime")))
            Else
                objDataOperation.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, DBNull.Value)
            End If

            objDataOperation.AddParameter("@voidreason", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Item("voidreason").ToString)

            Call objDataOperation.ExecNonQuery(strQ)

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            mintSavingcontributiontranunkid = Item("savingcontributiontranunkid")


            'Shani(24-Aug-2015) -- Start
            'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
            'If InsertAuditSavingContribution(objDataOperation, Item, 3) = False Then
            If InsertAuditSavingContribution(objDataOperation, Item, 3, xCurrentDateAndTime) = False Then
                'Shani(24-Aug-2015) -- End

                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            Return True
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: Delete; Module Name: " & mstrModuleName)
            Return False
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
        End Try
    End Function

    ''' <summary>
    ''' Modify By: Sohail
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>
    Public Function isUsed(ByVal intUnkid As Integer) As Boolean
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        objDataOperation = New clsDataOperation

        Try
            StrQ = "<Query>"

            objDataOperation.AddParameter("@savingcontributiontranunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intUnkid)

            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            Return dsList.Tables(0).Rows.Count > 0
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: isUsed; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
    End Function

    ''' <summary>
    ''' Modify By: Sohail
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>
    Public Function isExist(ByVal strCode As String, ByVal strName As String, Optional ByVal intUnkid As Integer = -1) As Boolean
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        objDataOperation = New clsDataOperation

        Try
            StrQ = "SELECT " & _
              "  savingcontributiontranunkid " & _
              ", savingtranunkid " & _
              ", periodunkid " & _
              ", contribution " & _
              ", userunkid " & _
              ", isvoid " & _
              ", voiduserunkid " & _
              ", voiddatetime " & _
              ", voidreason " & _
             "FROM svsaving_contribution_tran " & _
             "WHERE name = @name " & _
             "AND code = @code "

            If intUnkid > 0 Then
                strQ &= " AND savingcontributiontranunkid <> @savingcontributiontranunkid"
            End If

            objDataOperation.AddParameter("@code", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, strCode)
            objDataOperation.AddParameter("@name", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, strName)
            objDataOperation.AddParameter("@savingcontributiontranunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intUnkid)

            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            Return dsList.Tables(0).Rows.Count > 0
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: isExist; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
    End Function

    ''' <summary>
    ''' Modify By: Shani
    ''' </summary>
    ''' <purpose> Insert_Update_Delete Method </purpose>
    ''' Shani(24-Aug-2015) -- Start
    ''' ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
    ''' Public Function Insert_Update_Delete(ByVal objData As clsDataOperation) As Boolean
    Public Function Insert_Update_Delete(ByVal objData As clsDataOperation, ByVal xCurrentDateAndTime As DateTime) As Boolean
        'Shani(24-Aug-2015) -- End

        Dim i As Integer
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception
        Try
            For i = 0 To mdtTran.Rows.Count - 1
                With mdtTran.Rows(i)
                    objData.ClearParameters()
                    If Not IsDBNull(.Item("AUD")) Then
                        Select Case .Item("AUD")
                            Case "A"

                                'Shani(24-Aug-2015) -- Start
                                'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
                                'If Insert(mdtTran.Rows(i), objData) = False Then
                                If Insert(mdtTran.Rows(i), objData, xCurrentDateAndTime) = False Then
                                    'Shani(24-Aug-2015) -- End

                                    exForce = New Exception(objData.ErrorNumber & ": " & objData.ErrorMessage)
                                    Throw exForce
                                End If
                            Case "U"

                                'Shani(24-Aug-2015) -- Start
                                'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
                                'If Update(mdtTran.Rows(i), objData) = False Then
                                If Update(mdtTran.Rows(i), objData, xCurrentDateAndTime) = False Then
                                    'Shani(24-Aug-2015) -- End

                                    exForce = New Exception(objData.ErrorNumber & ": " & objData.ErrorMessage)
                                    Throw exForce
                                End If
                            Case "D"

                                'Shani(24-Aug-2015) -- Start
                                'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
                                'If Delete(mdtTran.Rows(i), objData) = False Then
                                If Delete(mdtTran.Rows(i), objData, xCurrentDateAndTime) = False Then
                                    'Shani(24-Aug-2015) -- End

                                    exForce = New Exception(objData.ErrorNumber & ": " & objData.ErrorMessage)
                                    Throw exForce
                                End If
                        End Select
                    End If
                End With
            Next
            Return True
        Catch ex As Exception
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
        End Try
    End Function

    ''' <summary>
    ''' Modify By: SHANI
    ''' </summary>
    ''' <returns>Boolean</returns>
    ''' <purpose> Delete Database Table (svsaving_tran) </purpose>
    Public Function Master_Delete(ByVal savingtranunkid As Integer, ByVal objData As clsDataOperation) As Boolean
        'If isUsed(intUnkid) Then
        '    mstrMessage = "<Message>"
        '    Return False
        'End If

        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        objDataOperation = objData

        Try
            objDataOperation.ClearParameters()
            strQ = "UPDATE svsaving_contribution_tran SET " & _
                     " isvoid = @isvoid" & _
                     ",voiduserunkid = @voiduserunkid" & _
                     ",voiddatetime = @voiddatetime" & _
                     ",voidreason = @voidreason " & _
                     "WHERE savingtranunkid = @savingtranunkid AND isvoid = 0 "

            objDataOperation.AddParameter("@savingtranunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, savingtranunkid)
            objDataOperation.AddParameter("@isvoid", SqlDbType.Bit, eZeeDataType.BIT_SIZE, True)
            objDataOperation.AddParameter("@voiduserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintVoiduserunkid)
            If mdtVoiddatetime = Nothing Then
                objDataOperation.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, DBNull.Value)
            Else
                objDataOperation.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, mdtVoiddatetime)
            End If
            objDataOperation.AddParameter("@voidreason", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrVoidreason)


            Call objDataOperation.ExecNonQuery(strQ)

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            Return True
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: Delete; Module Name: " & mstrModuleName)
            Return False
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
        End Try
    End Function

    ''' <summary>
    ''' Modify By: SHANI
    ''' </summary>
    ''' <returns>Boolean</returns>
    ''' <purpose> At Log Table (svsaving_tran) </purpose>
    ''' Shani(24-Aug-2015) -- Start
    ''' ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
    ''' Public Function InsertAuditSavingContribution(ByVal objDataOperation As clsDataOperation, ByVal dr As DataRow, ByVal AuditType As Integer) As Boolean
    Public Function InsertAuditSavingContribution(ByVal objDataOperation As clsDataOperation, ByVal dr As DataRow, ByVal AuditType As Integer, ByVal xCurrentDateAndTime As DateTime) As Boolean
        'Shani(24-Aug-2015) -- End

        Dim strQ As String = ""
        Dim strErrorMessage As String = ""
        Dim exForce As Exception = Nothing
        Try
            strQ = "INSERT INTO atsvsaving_contribution_tran ( " & _
                                          " savingcontributiontranunkid " & _
                                          ",savingtranunkid " & _
                                          ", periodunkid " & _
                                          ", contribution " & _
                                          ", audittype " & _
                                          ", audituserunkid " & _
                                          ", auditdatetime " & _
                                          ", ip" & _
                                          ", machine_name " & _
                                          ", form_name " & _
                                          " " & _
                                          " " & _
                                          " " & _
                                          " " & _
                                          " " & _
                                          ", isweb " & _
                                        ") VALUES (" & _
                                          "  @savingcontributiontranunkid " & _
                                          ", @savingtranunkid " & _
                                          ", @periodunkid " & _
                                          ", @contribution " & _
                                          ", @audittype " & _
                                          ", @audituserunkid " & _
                                          ", @auditdatetime " & _
                                          ", @ip" & _
                                          ", @machine_name " & _
                                          ", @form_name " & _
                                          " " & _
                                          " " & _
                                          " " & _
                                          " " & _
                                          " " & _
                                          ", @isweb " & _
                                        "); SELECT @@identity"


            objDataOperation.ClearParameters()
            objDataOperation.AddParameter("@savingcontributiontranunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintSavingcontributiontranunkid)
            objDataOperation.AddParameter("@savingtranunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintSavingtranunkid.ToString)
            objDataOperation.AddParameter("@periodunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, dr("periodunkid").ToString)
            objDataOperation.AddParameter("@contribution", SqlDbType.Float, eZeeDataType.MONEY_SIZE, dr("contribution").ToString)
            objDataOperation.AddParameter("@audittype", SqlDbType.Int, eZeeDataType.INT_SIZE, AuditType.ToString)
            objDataOperation.AddParameter("@audituserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, dr("userunkid"))

            'Shani(24-Aug-2015) -- Start
            'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
            'objDataOperation.AddParameter("@auditdatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, ConfigParameter._Object._CurrentDateAndTime)
            objDataOperation.AddParameter("@auditdatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, xCurrentDateAndTime)
            'Shani(24-Aug-2015) -- End

            If mstrWebIP.ToString().Trim.Length <= 0 Then
                mstrWebIP = getIP()
            End If
            objDataOperation.AddParameter("@ip", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrWebIP)

            If mstrWebHost.ToString().Length <= 0 Then
                mstrWebHost = getHostName()
            End If
            objDataOperation.AddParameter("@machine_name", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrWebHost)

            If mstrWebFormName.Trim.Length <= 0 Then
                objDataOperation.AddParameter("@form_name", SqlDbType.NVarChar, 500, mstrFormName)
                objDataOperation.AddParameter("@isweb", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsWeb)

            Else
                objDataOperation.AddParameter("@form_name", SqlDbType.NVarChar, 500, mstrWebFormName)
                objDataOperation.AddParameter("@isweb", SqlDbType.Bit, eZeeDataType.BIT_SIZE, True)
                objDataOperation.AddParameter("@module_name1", SqlDbType.NVarChar, 500, Language.getMessage(mstrModuleName, 1, "WEB"))
            End If





            objDataOperation.ExecNonQuery(strQ)

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If
            Return True
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "InsertAuditSavingInterest", mstrModuleName)
            Return False
        End Try
    End Function
End Class
