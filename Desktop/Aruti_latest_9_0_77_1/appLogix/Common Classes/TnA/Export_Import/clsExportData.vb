﻿'************************************************************************************************************************************
'Class Name : clsExportData.vb
'Purpose    :
'Date       :01/09/2010
'Written By : Pinkal
'Modified   :
'************************************************************************************************************************************

Imports eZeeCommonLib
Imports System.Xml
Imports System.IO
Imports System.Data

'Last Message Index = 1

''' <summary>
''' Purpose: 
''' Developer: Pinkal
''' </summary>

Public Class clsExportData

    Private Const mstrModuleName = "clsExportData"
    Dim objDataOperation As clsDataOperation
    Dim mstrMessage As String = ""

#Region " Properties "
'S.SANDEEP [28-May-2018] -- START 
'ISSUE/ENHANCEMENT : {Audit Trails} 
Private mstrFormName As String = String.Empty
Public WriteOnly Property _FormName() As String 
Set(ByVal value As String) 
mstrFormName = value 
End Set 
End Property 
 
Private mstrClientIP As String = ""
Public WriteOnly Property _ClientIP() As String 
Set(ByVal value As String) 
mstrClientIP = value 
End Set 
End Property 
 
Private mstrHostName As String = ""
Public WriteOnly Property _HostName() As String 
Set(ByVal value As String) 
mstrHostName = value 
End Set 
End Property 
 
Private mblnIsWeb As Boolean = False
Public WriteOnly Property _FromWeb() As Boolean 
Set(ByVal value As Boolean) 
mblnIsWeb = value 
End Set 
End Property 
 
    Private mintAuditUserId As Integer = 0
    Public WriteOnly Property _AuditUserId() As Integer
        Set(ByVal value As Integer)
            mintAuditUserId = value
        End Set
    End Property

    Private mintCompanyUnkid As Integer = 0
    Public WriteOnly Property _CompanyUnkid() As Integer
        Set(ByVal value As Integer)
            mintCompanyUnkid = value
        End Set
    End Property
 
Private mdtAuditDate As DateTime = Now
Public WriteOnly Property _AuditDate() As DateTime 
Set(ByVal value As DateTime) 
mdtAuditDate = value 
End Set 
End Property 
 
Private xDataOpr As clsDataOperation = Nothing
Public Property _xDataOpr() As clsDataOperation 
Get 
Return xDataOpr 
End Get 
Set(ByVal value As clsDataOperation) 
xDataOpr = value 
End Set 
End Property 
 
Private mintLoginEmployeeunkid As Integer = 0
Public Property _LoginEmployeeunkid() As Integer 
Get 
Return mintLoginEmployeeunkid 
End Get 
Set(ByVal value As Integer) 
mintLoginEmployeeunkid = value 
End Set 
End Property 
 
Private mintVoidlogingemployeeunkid As Integer=0
Public Property _Voidloginemployeeunkid() As Integer 
Get 
Return mintVoidlogingemployeeunkid 
End Get 
Set(ByVal value As Integer) 
mintVoidlogingemployeeunkid = value 
End Set 
End Property 
'S.SANDEEP [28-May-2018] -- END 

    ''' <summary>
    ''' Purpose: Get Message from Class 
    ''' Modify By: Pinkal
    ''' </summary>
    Public ReadOnly Property _Message() As String
        Get
            Return mstrMessage
        End Get
    End Property

#End Region

#Region "Methods"

    Public Function ExportData(ByVal strFileName As String, ByVal strEmployeeId As String, ByVal FileType As Integer, Optional ByVal dtDateFrom As Date = Nothing, Optional ByVal dtDateTo As Date = Nothing _
                            , Optional ByVal blnTimeSheet As Boolean = False, Optional ByVal blnHoliday As Boolean = False, Optional ByVal blnLeaves As Boolean = False) As Boolean

        Dim strQ As String
        Dim exForce As Exception

        Dim dsList As New DataSet
        Dim dsExportData As New DataSet
        Try

            objDataOperation = New clsDataOperation

            'START FOR EMPLOYEE TIMESHEET



            If blnTimeSheet Then



                'Pinkal (29-Aug-2012) -- Start
                'Enhancement : TRA Changes

                'strQ = "SELECT " & _
                '          " hremployee_master.employeeunkid " & _
                '          ", isnull(hremployee_master.firstname,'') + ' ' + isnull(hremployee_master.surname,'') as employeename " & _
                '          ", isnull(hremployee_master.displayname,'') as name " & _
                '          ", hremployee_master.shiftunkid " & _
                '          ", logindate as date" & _
                '          ", checkintime " & _
                '          ", checkouttime " & _
                '          ", workhour " & _
                '          ", breakhr " & _
                '          ", holdunkid " & _
                '          ", tnalogin_tran.remark " & _
                '          ", inouttype " & _
                '          ", tnalogin_tran.isvoid " & _
                '          ", tnalogin_tran.userunkid " & _
                '          ", tnalogin_tran.voiduserunkid " & _
                '          ", tnalogin_tran.voiddatetime " & _
                '          ", tnalogin_tran.voidreason " & _
                '      "FROM tnalogin_tran " & _
                '            "LEFT JOIN hremployee_master ON hremployee_master.employeeunkid = tnalogin_tran.employeeunkid " & _
                '      "WHERE  hremployee_master.employeeunkid IN (" & strEmployeeId & ") "



                strQ = "SELECT " & _
                        " hremployee_master.Employeecode " & _
                        ", isnull(hremployee_master.firstname,'') + ' ' + isnull(hremployee_master.surname,'') as 'Employeename' " & _
                        ", Logindate" & _
                        ", Checkintime " & _
                        ", Checkouttime " & _
                        ", Workhour " & _
                        ", Breakhr " & _
                      "FROM tnalogin_tran " & _
                            "LEFT JOIN hremployee_master ON hremployee_master.employeeunkid = tnalogin_tran.employeeunkid " & _
                      "WHERE  hremployee_master.employeeunkid IN (" & strEmployeeId & ") "

                'Pinkal (29-Aug-2012) -- End


                If Not dtDateFrom = Nothing And Not dtDateTo = Nothing Then

                    strQ &= "AND CONVERT(CHAR(8),logindate,112) >= @fromdate " & _
                            "AND CONVERT(CHAR(8),logindate,112) <= @todate "
                    objDataOperation.AddParameter("@fromdate", SqlDbType.NVarChar, eZeeDataType.DATETIME_SIZE, eZeeDate.convertDate(dtDateFrom))
                    objDataOperation.AddParameter("@todate", SqlDbType.NVarChar, eZeeDataType.DATETIME_SIZE, eZeeDate.convertDate(dtDateTo))

                End If

                strQ &= "ORDER BY isnull(hremployee_master.displayname,''), logindate "
                dsList = objDataOperation.ExecQuery(strQ, "Timesheet")

                If objDataOperation.ErrorMessage <> "" Then
                    exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                    Throw exForce
                End If

                If Not dsList Is Nothing Then
                    If dsList.Tables("Timesheet").Rows.Count > 0 Then
                        dsExportData.Tables.Add(dsList.Tables("Timesheet").Copy)
                    End If
                End If

            End If

            'END FOR EMPLOYEE TIMESHEET

            'START FOR EMPLOYEE HOLIDAY

            If blnHoliday Then

                strQ = "SELECT " & _
                       " lvemployee_holiday.yearunkid,lvemployee_holiday.employeeunkid, " & _
                       " ISNULL(hremployee_master.firstname,'') + ' ' + ISNULL(hremployee_master.surname,'') AS 'empname', " & _
                       " ISNULL(hremployee_master.displayname,'') AS 'empdisplayname', " & _
                       " lvholiday_master.holidayname as name ,CONVERT(CHAR(8),holidaydate,112) as date,holidaytypeid,description,color,lvholiday_master.isactive, " & _
                       " holidayname1,holidayname2,lvemployee_holiday.remarks,lvemployee_holiday.userunkid " & _
                       " FROM lvemployee_holiday " & _
                       " LEFT JOIN hremployee_master ON hremployee_master.employeeunkid = lvemployee_holiday.employeeunkid " & _
                       " LEFT JOIN lvholiday_master ON lvholiday_master.holidayunkid = lvemployee_holiday.holidayunkid " & _
                      " WHERE lvemployee_holiday.employeeunkid IN (" & strEmployeeId & ") "

                If Not dtDateFrom = Nothing And Not dtDateTo = Nothing Then

                    strQ &= "AND CONVERT(CHAR(8),holidaydate,112) between @fromdate AND @todate "

                    objDataOperation.AddParameter("@fromdate", SqlDbType.NVarChar, eZeeDataType.DATETIME_SIZE, eZeeDate.convertDate(dtDateFrom))
                    objDataOperation.AddParameter("@todate", SqlDbType.NVarChar, eZeeDataType.DATETIME_SIZE, eZeeDate.convertDate(dtDateTo))

                End If

                'strQ &= "ORDER BY isnull(hremployee_master.firstname,'') + ' ' + isnull(hremployee_master.surname,'') " & _
                '       ", holidaydate "
                dsList = objDataOperation.ExecQuery(strQ, "Holiday")

                If objDataOperation.ErrorMessage <> "" Then
                    exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                    Throw exForce
                End If

                If Not dsList Is Nothing Then
                    If dsList.Tables("Holiday").Rows.Count > 0 Then
                        dsExportData.Tables.Add(dsList.Tables("Holiday").Copy)
                    End If
                End If

            End If

            'END FOR EMPLOYEE HOLIDAY


            'START FOR EMPLOYEE LEAVE
            If blnLeaves Then

                'strQ = "SELECT " & _
                '       " leaveissuetranunkid,lvleaveIssue_tran.leaveissueunkid,leaveyearunkid,lvleaveIssue_master.formunkid,formno , " & _
                '       " lvleaveIssue_master.leavetypeunkid,leavename,lvleaveIssue_master.employeeunkid, " & _
                '       " ISNULL(hremployee_master.firstname,'') + ' ' + ISNULL(hremployee_master.surname,'') AS employeename, " & _
                '       " convert(char(8),leavedate,112) as date,lvleaveIssue_tran.userunkid,lvleaveIssue_tran.isvoid,lvleaveIssue_tran.voiddatetime, " & _
                '       " lvleaveIssue_tran.voiduserunkid, lvleaveIssue_tran.voidreason " & _
                '       " FROM lvleaveIssue_tran " & _
                '       " LEFT JOIN lvleaveIssue_master ON lvleaveIssue_master.leaveissueunkid = lvleaveIssue_tran.leaveissueunkid " & _
                '       " LEFT JOIN lvleaveform ON lvleaveform.formunkid = lvleaveIssue_master.formunkid  " & _
                '       " LEFT JOIN lvleavetype_master ON lvleavetype_master.leavetypeunkid = lvleaveIssue_master.leavetypeunkid " & _
                '       " LEFT JOIN hremployee_master ON hremployee_master.employeeunkid = lvleaveIssue_master.employeeunkid " & _
                '       " WHERE lvleaveIssue_master.employeeunkid IN (" & strEmployeeId & ") "

                strQ = "SELECT  leaveissuetranunkid ," & _
                       " lvleaveIssue_tran.leaveissueunkid , " & _
                       " leaveyearunkid , " & _
                       " lvleaveIssue_master.formunkid , " & _
                       " formno as name , " & _
                       " lvleavetype_master.leavetypeunkid , " & _
                       " leavename , " & _
                       " lvleavetype_master.ispaid , " & _
                       " CONVERT(CHAR(8),lvleaveform.applydate,112) as date, " & _
                       " CONVERT(CHAR(8),lvleaveform.startdate,112) as startdate, " & _
                       " CONVERT(CHAR(8),lvleaveform.returndate,112) as returndate, " & _
                       " lvleaveIssue_master.employeeunkid , " & _
                       " ISNULL(hremployee_master.firstname, '') + ' ' + ISNULL(hremployee_master.surname, '') AS employeename , " & _
                       " ISNULL(hremployee_master.displayname, '') AS displayname , " & _
                       " lvleaveform.addressonleave, " & _
                       " lvleaveform.remark, " & _
                       " lvleaveform.statusunkid, " & _
                       " lvpendingleave_tran.approverunkid, " & _
                       " CONVERT(CHAR(8),lvpendingleave_tran.approvaldate,112) as approvaldate, " & _
                       " lvpendingleave_tran.transactionheadunkid, " & _
                       " lvpendingleave_tran.amount, " & _
                       " lvpendingleave_tran.remark AS pendingremark, " & _
                       " lvleaveIssue_master.leaveyearunkid, " & _
                       " CONVERT(CHAR(8), leavedate, 112) AS leavedate , " & _
                       " lvleaveIssue_tran.userunkid , " & _
                       " lvleaveIssue_tran.isvoid, " & _
                       " lvleaveIssue_tran.voiddatetime , " & _
                       " lvleaveIssue_tran.voiduserunkid , " & _
                       " lvleaveIssue_tran.voidreason " & _
                       " FROM lvleaveIssue_tran " & _
                       " LEFT JOIN lvleaveIssue_master ON lvleaveIssue_master.leaveissueunkid = lvleaveIssue_tran.leaveissueunkid " & _
                       " LEFT JOIN lvleaveform ON lvleaveform.formunkid = lvleaveIssue_master.formunkid " & _
                       " LEFT JOIN lvleavetype_master ON lvleavetype_master.leavetypeunkid = lvleaveIssue_master.leavetypeunkid " & _
                       " LEFT JOIN hremployee_master ON hremployee_master.employeeunkid = lvleaveIssue_master.employeeunkid " & _
                       " JOIN lvpendingleave_tran ON lvpendingleave_tran.formunkid = lvleaveform.formunkid " & _
                       " WHERE lvleaveIssue_master.employeeunkid IN (" & strEmployeeId & " ) "


                If Not dtDateFrom = Nothing And Not dtDateTo = Nothing Then

                    strQ &= "AND CONVERT(CHAR(8),leavedate,112) between @fromdate AND @todate "

                    objDataOperation.AddParameter("@fromdate", SqlDbType.NVarChar, eZeeDataType.DATETIME_SIZE, eZeeDate.convertDate(dtDateFrom))
                    objDataOperation.AddParameter("@todate", SqlDbType.NVarChar, eZeeDataType.DATETIME_SIZE, eZeeDate.convertDate(dtDateTo))

                End If

                strQ &= "ORDER BY isnull(hremployee_master.firstname,'') + ' ' + isnull(hremployee_master.surname,'')"

                dsList = objDataOperation.ExecQuery(strQ, "Leave")

                If objDataOperation.ErrorMessage <> "" Then
                    exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                    Throw exForce
                End If

                If Not dsList Is Nothing Then
                    If dsList.Tables("Leave").Rows.Count > 0 Then
                        dsExportData.Tables.Add(dsList.Tables("Leave").Copy)
                    End If
                End If

            End If
            'END FOR EMPLOYEE LEAVE

            If Not dsExportData.Tables.Count > 0 Then Return True

            Dim ObjFile As New FileInfo(strFileName)
       

            If FileType = 1 Then
                dsExportData.WriteXml(strFileName)
                ObjFile.IsReadOnly = True
                ObjFile.Attributes = FileAttributes.ReadOnly

            ElseIf FileType = 2 Then
                'ExportToExcel(dsExportData, strFileName, blnTimeSheet, blnHoliday, blnLeaves)
                'S.SANDEEP [12-Jan-2018] -- START
                'ISSUE/ENHANCEMENT : REF-ID # 0001843
                'Dim export As New ExcelData
                'export.Export(strFileName, dsExportData)
                OpenXML_Export(strFileName, dsExportData)
                'S.SANDEEP [12-Jan-2018] -- END

                ObjFile.IsReadOnly = True
                ObjFile.Attributes = FileAttributes.ReadOnly

            ElseIf FileType = 3 Then
                Dim filename As String = ""

                If blnTimeSheet And dsExportData.Tables.Contains("Timesheet") Then
                    filename = strFileName
                    filename &= "\" & "Timesheet_" & Date.Now.ToString("yyyyMMdd") & ".csv"
                    ExportToCSV(dsExportData.Tables("Timesheet"), filename)
                    filename = ""
                End If

                If blnHoliday And dsExportData.Tables.Contains("Holiday") Then
                    filename = strFileName
                    filename &= "\" & "Holiday_" & Date.Now.ToString("yyyyMMdd") & ".csv"
                    ExportToCSV(dsExportData.Tables("Holiday"), filename)
                    filename = ""
                End If

                If blnLeaves And dsExportData.Tables.Contains("Leave") Then
                    filename = strFileName
                    filename &= "\" & "Leave_" & Date.Now.ToString("yyyyMMdd") & ".csv"
                    ExportToCSV(dsExportData.Tables("Leave"), filename)
                    filename = ""
                End If

            End If

            Return True
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "ExportData", mstrModuleName)
            Return False
        End Try


    End Function

    Public Function getEmployeeID(ByVal strEmployeeCode As String) As Integer
        Dim strQ As String
        Dim exForce As Exception
        Dim dsList As New DataSet
        Dim intEmployeeId As Integer = -1
        Try
            objDataOperation = New clsDataOperation
            strQ = "SELECT " & _
                       " employeeunkid " & _
                   "FROM hremployee_master  " & _
                   "WHERE  employeecode = @employeecode "

            objDataOperation.AddParameter("@employeecode", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, strEmployeeCode)

            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If
            For Each dtrow As DataRow In dsList.Tables(0).Rows
                intEmployeeId = dtrow.Item("employeeunkid")
                Exit For
            Next
            Return intEmployeeId
        Catch ex As Exception
            DisplayError.Show(-1, ex.Message, "getEmployeeID", mstrModuleName)
            Return Nothing
        Finally
            If dsList IsNot Nothing Then dsList.Dispose()
            dsList = Nothing
            If objDataOperation IsNot Nothing Then objDataOperation.Dispose()
            objDataOperation = Nothing

            exForce = Nothing
        End Try
    End Function

    'Private Sub ExportToExcel(ByVal dsData As DataSet, ByVal strPath As String, Optional ByVal blnTimeSheet As Boolean = False, Optional ByVal blnHoliday As Boolean = False, Optional ByVal blnLeaves As Boolean = False)
    '    Dim Excel As Object = CreateObject("Excel.Application")
    '    Dim intCol, intRow As Integer

    '    If dsData.Tables.Count <= 0 Then Exit Sub

    '    If Excel Is Nothing Then
    '        mstrMessage = Language.getMessage(mstrModuleName, 1, "MS Excel is not installed on this machine. This operation requires MS Excel to be installed on this machine.")
    '        Exit Sub
    '    End If
    '    Try
    '        With Excel

    '            .Workbooks.Add()
    '            For i As Integer = 0 To dsData.Tables.Count - 1
    '                .SheetsInNewWorkbook = i + 1
    '                .Worksheets(i + 1).Select()
    '                .Worksheets(i + 1).Name = dsData.Tables(i).TableName

    '                '.cells(2, 4).value = dsData.Tables(i).TableName & " Information of " & Now.Date.ToShortDateString
    '                '.cells(2, 4).EntireRow.Font.Bold = True


    '                Dim intI As Integer = 1
    '                For intCol = 0 To dsData.Tables(i).Columns.Count - 1
    '                    .cells(1, intI).value = dsData.Tables(i).Columns(intCol).ColumnName
    '                    ' .cells(1, intI).EntireRow.Font.Bold = True
    '                    intI += 1
    '                Next

    '                intI = 2
    '                Dim intK As Integer = 1
    '                For intCol = 0 To dsData.Tables(i).Columns.Count - 1
    '                    intI = 2

    '                    For intRow = 0 To dsData.Tables(i).Rows.Count - 1

    '                        If dsData.Tables(i).Columns(intCol).DataType.ToString = "System.DateTime" Then
    '                            If dsData.Tables(i).Rows(intRow)(intCol) IsNot DBNull.Value Then
    '                                .Cells(intI, intK).Value = CDate(dsData.Tables(i).Rows(intRow)(intCol)).ToString("dd/MM/yyyy HH:mm:ss tt")
    '                            End If
    '                        Else
    '                            .Cells(intI, intK).Value = dsData.Tables(i).Rows(intRow)(intCol).ToString()
    '                        End If

    '                        intI += 1
    '                    Next


    '                    intK += 1
    '                Next
    '            Next
    '            .Worksheets(dsData.Tables.Count - (dsData.Tables.Count - 1)).Select()
    '            .ActiveCell.Worksheet.SaveAs(strPath)
    '        End With
    '        System.Runtime.InteropServices.Marshal.ReleaseComObject(Excel)
    '        Excel = Nothing

    '    Catch cex As System.Runtime.InteropServices.COMException
    '        Exit Sub
    '    Catch ex As Exception
    '        DisplayError.Show("-1", ex.Message, "ExportToExcel", mstrModuleName)
    '    End Try
    '    Dim pro() As Process = System.Diagnostics.Process.GetProcessesByName("EXCEL")
    '    For Each i As Process In pro
    '        i.Kill()
    '    Next
    'End Sub

    Public Sub ExportToCSV(ByVal dtData As System.Data.DataTable, ByVal strPath As String)
        Try
            Dim strExport As String = String.Empty
            'For Each dc As DataColumn In dtData.Columns
            '    strExport &= dc.ColumnName & "   "
            'Next

            'strExport = strExport.Substring(0, strExport.Length - 3) & Environment.NewLine.ToString()

            For Each dr As DataRow In dtData.Rows
                For c As Integer = 0 To dtData.Columns.Count - 1
                    strExport &= dr(dtData.Columns(c).ColumnName.ToString).ToString & ","
                Next
                strExport = strExport.Substring(0, strExport.Length - 1)
                strExport &= Environment.NewLine.ToString
            Next

            strExport = strExport.Substring(0, strExport.Length - 1) + Environment.NewLine.ToString()
            Dim tw As New System.IO.StreamWriter(strPath)
            tw.Write(strExport)
            tw.Close()

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "ExportToCSV", mstrModuleName)
        End Try
    End Sub

#End Region


End Class
