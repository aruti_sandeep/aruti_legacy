﻿'************************************************************************************************************************************
'Class Name : clspaymentbatchposting_master.vb
'Purpose    :
'Date       : 01-06-2023
'Written By : Hemant
'Modified   :
'************************************************************************************************************************************

Imports eZeeCommonLib
''' <summary>
''' Purpose: 
''' Developer: Hemant
''' </summary>
Public Class clspaymentbatchposting_master
    Private Shared ReadOnly mstrModuleName As String = "clspaymentbatchposting_master"
    Dim objDataOperation As clsDataOperation
    Dim mstrMessage As String = ""

#Region " ENUM "
    Public Enum enBatchPostingStatus
        Pending = 1
        Success = 2
        Failed = 3
        Unknown = 4
        Submitted = 5
    End Enum

    Public Enum enApprovalStatus
        SubmitForApproval = 1
        Approved = 2
        Rejected = 3
    End Enum

#End Region

#Region " Private variables "

    Private mintPaymentBatchPostingunkid As Integer
    Private mstrBatchName As String = String.Empty
    Private mintPeriodunkid As Integer
    Private mstrVoucherNo As String = String.Empty
    Private mintStatusunkid As Integer
    Private mstrRefno As String = ""
    Private mintUserunkid As Integer
    Private mblnIsweb As Boolean
    Private mblnIsvoid As Boolean
    Private mintVoiduserunkid As Integer
    Private mdtVoiddatetime As Date
    Private mstrVoidreason As String = String.Empty
    Private mdtBatchPostingEmpTran As DataTable = Nothing

#End Region

#Region " Properties "
    ''' <summary>
    ''' Purpose: Get Message from Class 
    ''' Modify By: Hemant
    ''' </summary>
    Public ReadOnly Property _Message() As String
        Get
            Return mstrMessage
        End Get
    End Property

    ''' <summary>
    ''' Purpose: Get or Set paymentbatchpostingunkid
    ''' Modify By: Hemant
    ''' </summary>
    Public Property _PaymentBatchPostingunkid(Optional ByVal xDataOp As clsDataOperation = Nothing) As Integer
        Get
            Return mintPaymentBatchPostingunkid
        End Get
        Set(ByVal value As Integer)
            mintPaymentBatchPostingunkid = value
            Call GetData(xDataOp)
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set batchname
    ''' Modify By: Hemant
    ''' </summary>
    Public Property _BatchName() As String
        Get
            Return mstrBatchName
        End Get
        Set(ByVal value As String)
            mstrBatchName = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set periodunkid
    ''' Modify By: Hemant
    ''' </summary>
    Public Property _Periodunkid() As Integer
        Get
            Return mintPeriodunkid
        End Get
        Set(ByVal value As Integer)
            mintPeriodunkid = Value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set voucherno
    ''' Modify By: Hemant
    ''' </summary>
    Public Property _VoucherNo() As String
        Get
            Return mstrVoucherNo
        End Get
        Set(ByVal value As String)
            mstrVoucherNo = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set statusunkid
    ''' Modify By: Hemant
    ''' </summary>
    Public Property _Statusunkid() As Integer
        Get
            Return mintStatusunkid
        End Get
        Set(ByVal value As Integer)
            mintStatusunkid = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set refno
    ''' Modify By: Hemant
    ''' </summary>
    Public Property _Refno() As String
        Get
            Return mstrRefno
        End Get
        Set(ByVal value As String)
            mstrRefno = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set userunkid
    ''' Modify By: Hemant
    ''' </summary>
    Public Property _Userunkid() As Integer
        Get
            Return mintUserunkid
        End Get
        Set(ByVal value As Integer)
            mintUserunkid = Value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set isweb
    ''' Modify By: Hemant
    ''' </summary>
    Public Property _Isweb() As Boolean
        Get
            Return mblnIsweb
        End Get
        Set(ByVal value As Boolean)
            mblnIsweb = Value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set isvoid
    ''' Modify By: Hemant
    ''' </summary>
    Public Property _Isvoid() As Boolean
        Get
            Return mblnIsvoid
        End Get
        Set(ByVal value As Boolean)
            mblnIsvoid = Value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set voiduserunkid
    ''' Modify By: Hemant
    ''' </summary>
    Public Property _Voiduserunkid() As Integer
        Get
            Return mintVoiduserunkid
        End Get
        Set(ByVal value As Integer)
            mintVoiduserunkid = Value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set voiddatetime
    ''' Modify By: Hemant
    ''' </summary>
    Public Property _Voiddatetime() As Date
        Get
            Return mdtVoiddatetime
        End Get
        Set(ByVal value As Date)
            mdtVoiddatetime = Value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set voidreason
    ''' Modify By: Hemant
    ''' </summary>
    Public Property _Voidreason() As String
        Get
            Return mstrVoidreason
        End Get
        Set(ByVal value As String)
            mstrVoidreason = Value
        End Set
    End Property

    Private mstrFormName As String = String.Empty
    Public WriteOnly Property _FormName() As String
        Set(ByVal value As String)
            mstrFormName = value
        End Set
    End Property

    Private mstrClientIP As String = ""
    Public WriteOnly Property _ClientIP() As String
        Set(ByVal value As String)
            mstrClientIP = value
        End Set
    End Property

    Private mstrHostName As String = ""
    Public WriteOnly Property _HostName() As String
        Set(ByVal value As String)
            mstrHostName = value
        End Set
    End Property

    Private mintAuditUserId As Integer = 0
    Public WriteOnly Property _AuditUserId() As Integer
        Set(ByVal value As Integer)
            mintAuditUserId = value
        End Set
    End Property

    Private mintCompanyUnkid As Integer = 0
    Public WriteOnly Property _CompanyUnkid() As Integer
        Set(ByVal value As Integer)
            mintCompanyUnkid = value
        End Set
    End Property

    Private mdtAuditDate As DateTime = Now
    Public WriteOnly Property _AuditDate() As DateTime
        Set(ByVal value As DateTime)
            mdtAuditDate = value
        End Set
    End Property

    Public Property _BatchPostingEmpTran() As DataTable
        Get
            Return mdtBatchPostingEmpTran
        End Get
        Set(ByVal value As DataTable)
            mdtBatchPostingEmpTran = value
        End Set
    End Property
#End Region

    ''' <summary>
    ''' Modify By: Hemant
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>
    Public Sub GetData(Optional ByVal xDataOp As clsDataOperation = Nothing)
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        If xDataOp Is Nothing Then
            objDataOperation = New clsDataOperation
        Else
            objDataOperation = xDataOp
        End If
        objDataOperation.ClearParameters()

        Try
            strQ = "SELECT " & _
              "  paymentbatchpostingunkid " & _
              ", batchname " & _
              ", periodunkid " & _
              ", voucherno " & _
              ", statusunkid " & _
              ", refno " & _
              ", userunkid " & _
              ", isvoid " & _
              ", voiduserunkid " & _
              ", voiddatetime " & _
              ", voidreason " & _
             "FROM prpayment_batchposting_master " & _
             "WHERE paymentbatchpostingunkid = @paymentbatchpostingunkid "

            objDataOperation.AddParameter("@paymentbatchpostingunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintPaymentBatchPostingunkid.ToString)

            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            For Each dtRow As DataRow In dsList.Tables(0).Rows
                mintPaymentBatchPostingunkid = CInt(dtRow.Item("paymentbatchpostingunkid"))
                mstrBatchName = dtRow.Item("batchname").ToString
                mintPeriodunkid = CInt(dtRow.Item("periodunkid"))
                mstrVoucherNo = dtRow.Item("voucherno").ToString
                mintStatusunkid = CInt(dtRow.Item("statusunkid"))
                mstrRefno = CStr(dtRow.Item("refno"))
                mintUserunkid = CInt(dtRow.Item("userunkid"))
                mblnIsvoid = CBool(dtRow.Item("isvoid"))
                mintVoiduserunkid = CInt(dtRow.Item("voiduserunkid"))
                If IsDBNull(dtRow.Item("voiddatetime")) = False Then
                    mdtVoiddatetime = dtRow.Item("voiddatetime")
                Else
                    mdtVoiddatetime = Nothing
                End If
                mstrVoidreason = dtRow.Item("voidreason").ToString

                Exit For
            Next
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: GetData; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            If xDataOp Is Nothing Then objDataOperation = Nothing
        End Try
    End Sub


    ''' <summary>
    ''' Modify By: Hemant
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>
    Public Function GetList(ByVal strTableName As String, Optional ByVal intPeriodId As Integer = 0, Optional ByVal xDataOp As clsDataOperation = Nothing) As DataSet
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        If xDataOp Is Nothing Then
            objDataOperation = New clsDataOperation
        Else
            objDataOperation = xDataOp
        End If
        objDataOperation.ClearParameters()

        Try

            strQ = "SELECT " & _
                      "  prpayment_batchposting_master.paymentbatchpostingunkid " & _
                      ", prpayment_batchposting_master.batchname " & _
                      ", prpayment_batchposting_master.periodunkid " & _
                      ", cfcommon_period_tran.period_code " & _
                      ", cfcommon_period_tran.period_name " & _
                      ", prpayment_batchposting_master.voucherno " & _
                      ", prpayment_batchposting_master.statusunkid " & _
                      ", prpayment_batchposting_master.refno " & _
                      ", prpayment_batchposting_master.userunkid " & _
                      ", prpayment_batchposting_master.isvoid " & _
                      ", prpayment_batchposting_master.voiduserunkid " & _
                      ", prpayment_batchposting_master.voiddatetime " & _
                      ", prpayment_batchposting_master.voidreason "

            strQ &= "FROM prpayment_batchposting_master " & _
                    "LEFT JOIN cfcommon_period_tran ON cfcommon_period_tran.periodunkid = prpayment_batchposting_master.periodunkid " & _
                 "WHERE prpayment_batchposting_master.isvoid = 0 "

            If intPeriodId > 0 Then
                strQ &= " AND prpayment_batchposting_master.periodunkid = @periodunkid "
                objDataOperation.AddParameter("@periodunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intPeriodId)
            End If

            dsList = objDataOperation.ExecQuery(strQ, strTableName)

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: GetList; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            If xDataOp Is Nothing Then objDataOperation = Nothing
        End Try
        Return dsList
    End Function


    ''' <summary>
    ''' Modify By: Hemant
    ''' </summary>
    ''' <returns>Boolean</returns>
    ''' <purpose> INSERT INTO Database Table (prpayment_batchposting_master) </purpose>
    Public Function Insert(Optional ByVal xDataOp As clsDataOperation = Nothing) As Boolean
        If isExist(mintPeriodunkid, -1, mstrBatchName, xDataOp) Then
            mstrMessage = Language.getMessage(mstrModuleName, 9, "Sorry, This Batch is already Generated. Please Generate new Batch.")
            Return False
        End If
        Dim objPaymentBatchPostingTran As New clspaymentbatchposting_tran
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        If xDataOp Is Nothing Then
            objDataOperation = New clsDataOperation
            objDataOperation.BindTransaction()
        Else
            objDataOperation = xDataOp
        End If
        objDataOperation.ClearParameters()

        Try
            objDataOperation.AddParameter("@batchname", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrBatchName.ToString)
            objDataOperation.AddParameter("@periodunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintPeriodunkid.ToString)
            objDataOperation.AddParameter("@voucherno", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrVoucherNo.ToString)
            objDataOperation.AddParameter("@statusunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintStatusunkid.ToString)
            objDataOperation.AddParameter("@refno", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrRefno.ToString)
            objDataOperation.AddParameter("@userunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintUserunkid.ToString)
            objDataOperation.AddParameter("@isvoid", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsvoid.ToString)
            objDataOperation.AddParameter("@voiduserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintVoiduserunkid.ToString)
            If mdtVoiddatetime <> Nothing Then
                objDataOperation.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, mdtVoiddatetime.ToString)
            Else
                objDataOperation.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, DBNull.Value)
            End If
            objDataOperation.AddParameter("@voidreason", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrVoidreason.ToString)

            strQ = "INSERT INTO prpayment_batchposting_master ( " & _
              "  batchname " & _
              ", periodunkid " & _
              ", voucherno " & _
              ", statusunkid " & _
              ", refno " & _
              ", userunkid " & _
              ", isvoid " & _
              ", voiduserunkid " & _
              ", voiddatetime " & _
              ", voidreason" & _
            ") VALUES (" & _
              "  @batchname " & _
              ", @periodunkid " & _
              ", @voucherno " & _
              ", @statusunkid " & _
              ", @refno " & _
              ", @userunkid " & _
              ", @isvoid " & _
              ", @voiduserunkid " & _
              ", @voiddatetime " & _
              ", @voidreason" & _
            "); SELECT @@identity"

            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            mintPaymentBatchPostingunkid = dsList.Tables(0).Rows(0).Item(0)

            If InsertAuditTrail(objDataOperation, enAuditType.ADD) = False Then
                objDataOperation.ReleaseTransaction(False)
                Return False
            End If

            If mdtBatchPostingEmpTran IsNot Nothing AndAlso mdtBatchPostingEmpTran.Rows.Count > 0 Then
                With objPaymentBatchPostingTran
                    ._xDataOp = objDataOperation
                    ._PaymentBatchPostingunkid = mintPaymentBatchPostingunkid
                    ._TranDataTable = mdtBatchPostingEmpTran
                    ._Isvoid = mblnIsvoid
                    ._Voiduserunkid = mintVoiduserunkid
                    ._Voiddatetime = mdtVoiddatetime
                    ._Voidreason = mstrVoidreason
                    ._Isweb = mblnIsweb
                    ._ClientIP = mstrClientIP
                    ._FormName = mstrFormName
                    ._HostName = mstrHostName
                    ._AuditUserId = mintAuditUserId
                    If .InsertAll() = False Then
                        exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                        Throw exForce
                    End If
                End With
            End If

            If xDataOp Is Nothing Then objDataOperation.ReleaseTransaction(True)

            Return True
        Catch ex As Exception
            objDataOperation.ReleaseTransaction(False)
            Throw New Exception(ex.Message & "; Procedure Name: Insert; Module Name: " & mstrModuleName)
            Return False
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            If xDataOp Is Nothing Then objDataOperation = Nothing
            objPaymentBatchPostingTran = Nothing
        End Try
    End Function

    ''' <summary>
    ''' Modify By: Hemant
    ''' </summary>
    ''' <returns>Boolean</returns>
    ''' <purpose> Update Database Table (prpayment_batchposting_master) </purpose>
    Public Function Update(Optional ByVal xDataOp As clsDataOperation = Nothing) As Boolean
        'If isExist(mstrName, mintPaymentBatchPostingunkid) Then
        '    mstrMessage = "<Message>"
        '    Return False
        'End If

        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        If xDataOp Is Nothing Then
            objDataOperation = New clsDataOperation
            objDataOperation.BindTransaction()
        Else
            objDataOperation = xDataOp
        End If
        objDataOperation.ClearParameters()

        Try
            objDataOperation.AddParameter("@paymentbatchpostingunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintPaymentBatchPostingunkid.ToString)
            objDataOperation.AddParameter("@batchname", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrBatchName.ToString)
            objDataOperation.AddParameter("@periodunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintPeriodunkid.ToString)
            objDataOperation.AddParameter("@voucherno", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrVoucherNo.ToString)
            objDataOperation.AddParameter("@statusunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintStatusunkid.ToString)
            objDataOperation.AddParameter("@refno", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrRefno.ToString)
            objDataOperation.AddParameter("@userunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintUserunkid.ToString)
            objDataOperation.AddParameter("@isvoid", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsvoid.ToString)
            objDataOperation.AddParameter("@voiduserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintVoiduserunkid.ToString)
            If mdtVoiddatetime <> Nothing Then
                objDataOperation.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, mdtVoiddatetime.ToString)
            Else
                objDataOperation.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, DBNull.Value)
            End If
            objDataOperation.AddParameter("@voidreason", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrVoidreason.ToString)

            strQ = "UPDATE prpayment_batchposting_master SET " & _
              "  batchname = @batchname" & _
              ", periodunkid = @periodunkid" & _
              ", voucherno = @voucherno" & _
              ", statusunkid = @statusunkid" & _
              ", refno = @refno" & _
              ", userunkid = @userunkid" & _
              ", isvoid = @isvoid" & _
              ", voiduserunkid = @voiduserunkid" & _
              ", voiddatetime = @voiddatetime" & _
              ", voidreason = @voidreason " & _
            "WHERE paymentbatchpostingunkid = @paymentbatchpostingunkid "

            Call objDataOperation.ExecNonQuery(strQ)

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            If InsertAuditTrail(objDataOperation, enAuditType.EDIT) = False Then
                objDataOperation.ReleaseTransaction(False)
                Return False
            End If

            If xDataOp Is Nothing Then objDataOperation.ReleaseTransaction(True)

            Return True
        Catch ex As Exception
            objDataOperation.ReleaseTransaction(False)
            Throw New Exception(ex.Message & "; Procedure Name: Update; Module Name: " & mstrModuleName)
            Return False
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            If xDataOp Is Nothing Then objDataOperation = Nothing
        End Try
    End Function

    ''' <summary>
    ''' Modify By: Hemant
    ''' </summary>
    ''' <returns>Boolean</returns>
    ''' <purpose> Delete Database Table (prpayment_batchposting_master) </purpose>
    Public Function Delete(Optional ByVal xDataOp As clsDataOperation = Nothing) As Boolean
        'If isUsed(intUnkid) Then
        '    mstrMessage = "<Message>"
        '    Return False
        'End If

        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        If xDataOp Is Nothing Then
            objDataOperation = New clsDataOperation
        Else
            objDataOperation = xDataOp
        End If
        objDataOperation.ClearParameters()

        Try
            strQ = "UPDATE prpayment_batchposting_master SET " & _
             "  isvoid = 1 " & _
             ", voiduserunkid = @voiduserunkid" & _
             ", voiddatetime = @voiddatetime" & _
             ", voidreason = @voidreason " & _
           "WHERE paymentbatchpostingunkid = @paymentbatchpostingunkid "


            objDataOperation.AddParameter("@paymentbatchpostingunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintPaymentBatchPostingunkid)
            objDataOperation.AddParameter("@isvoid", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsvoid.ToString)
            objDataOperation.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, mdtVoiddatetime)
            objDataOperation.AddParameter("@voidreason", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrVoidreason.ToString)
            objDataOperation.AddParameter("@voiduserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintVoiduserunkid.ToString)

            Call objDataOperation.ExecNonQuery(strQ)

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If


            _PaymentBatchPostingunkid(objDataOperation) = mintPaymentBatchPostingunkid

            If InsertAuditTrail(objDataOperation, enAuditType.DELETE) = False Then
                objDataOperation.ReleaseTransaction(False)
                Return False
            End If

            If xDataOp Is Nothing Then objDataOperation.ReleaseTransaction(True)

            Return True
        Catch ex As Exception
            objDataOperation.ReleaseTransaction(False)
            Throw New Exception(ex.Message & "; Procedure Name: Update; Module Name: " & mstrModuleName)
            Return False
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            If xDataOp Is Nothing Then objDataOperation = Nothing
        End Try
    End Function

    ''' <summary>
    ''' Modify By: Hemant
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>
    Public Function isUsed(ByVal intUnkid As Integer, Optional ByVal xDataOp As clsDataOperation = Nothing) As Boolean
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        If xDataOp Is Nothing Then
            objDataOperation = New clsDataOperation
        Else
            objDataOperation = xDataOp
        End If
        objDataOperation.ClearParameters()

        Try
            strQ = "<Query>"

            objDataOperation.AddParameter("@paymentbatchpostingunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intUnkid)

            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            Return dsList.Tables(0).Rows.Count > 0
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: isUsed; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            If xDataOp Is Nothing Then objDataOperation = Nothing
        End Try
    End Function

    ''' <summary>
    ''' Modify By: Hemant
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>
    Public Function isExist(ByVal intPeriodUnkId As Integer, Optional ByVal intUnkid As Integer = -1, Optional ByVal strBatchName As String = "", Optional ByVal xDataOp As clsDataOperation = Nothing) As Boolean
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        If xDataOp Is Nothing Then
            objDataOperation = New clsDataOperation
        Else
            objDataOperation = xDataOp
        End If
        objDataOperation.ClearParameters()

        Try

            strQ = "SELECT " & _
              "  paymentbatchpostingunkid " & _
              ", batchname " & _
              ", periodunkid " & _
              ", voucherno " & _
              ", statusunkid " & _
              ", refno " & _
              ", userunkid " & _
              ", isvoid " & _
              ", voiduserunkid " & _
              ", voiddatetime " & _
              ", voidreason " & _
             "FROM prpayment_batchposting_master " & _
             "WHERE isvoid = 0 " & _
             "AND periodunkid = @periodunkid "

            If strBatchName.Trim.Length > 0 Then
                strQ &= " AND batchname = @BatchName"
                objDataOperation.AddParameter("@BatchName", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, strBatchName)
            End If

            If intUnkid > 0 Then
                strQ &= " AND paymentbatchpostingunkid <> @paymentbatchpostingunkid"
                objDataOperation.AddParameter("@paymentbatchpostingunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intUnkid)
            End If

            objDataOperation.AddParameter("@periodunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intPeriodUnkId)

            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            If dsList.Tables(0).Rows.Count > 0 Then
                Return True
            End If

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: isExist; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            If xDataOp Is Nothing Then objDataOperation = Nothing
        End Try
    End Function

    Public Function InsertAuditTrail(ByVal objDataOp As clsDataOperation, ByVal AuditType As Integer) As Boolean
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception


        Try
            objDataOp.ClearParameters()
            objDataOp.AddParameter("@paymentbatchpostingunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintpaymentbatchpostingunkid.ToString)
            objDataOp.AddParameter("@batchname", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrBatchName.ToString)
            objDataOp.AddParameter("@periodunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintPeriodunkid.ToString)
            objDataOp.AddParameter("@voucherno", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrVoucherNo.ToString)
            objDataOp.AddParameter("@statusunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintStatusunkid.ToString)
            objDataOp.AddParameter("@refno", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrRefno.ToString)
            objDataOp.AddParameter("@audituserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintUserunkid)
            objDataOp.AddParameter("@audittype", SqlDbType.Int, eZeeDataType.INT_SIZE, AuditType.ToString)
            objDataOp.AddParameter("@auditdatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, mdtAuditDate)
            objDataOp.AddParameter("@isweb", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsweb.ToString)
            objDataOp.AddParameter("@ip", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrClientIP)
            objDataOp.AddParameter("@host", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrHostName)
            objDataOp.AddParameter("@form_name", SqlDbType.NVarChar, 500, mstrFormName)

            strQ = "INSERT INTO atprpayment_batchposting_master ( " & _
              "  tranguid " & _
              ", paymentbatchpostingunkid " & _
              ", batchname " & _
              ", periodunkid " & _
              ", voucherno " & _
              ", statusunkid " & _
              ", refno " & _
              ", audituserunkid " & _
              ", audittype " & _
              ", auditdatetime " & _
              ", isweb " & _
              ", ip " & _
              ", host " & _
              ", form_name " & _
            ") VALUES (" & _
              "  LOWER(NEWID()) " & _
              ", @paymentbatchpostingunkid " & _
              ", @batchname " & _
              ", @periodunkid " & _
              ", @voucherno " & _
              ", @statusunkid " & _
              ", @refno " & _
              ", @audituserunkid " & _
              ", @audittype " & _
              ", @auditdatetime " & _
              ", @isweb " & _
              ", @ip " & _
              ", @host " & _
              ", @form_name " & _
            "); SELECT @@identity"


            dsList = objDataOp.ExecQuery(strQ, "List")

            If objDataOp.ErrorMessage <> "" Then
                exForce = New Exception(objDataOp.ErrorNumber & ": " & objDataOp.ErrorMessage)
                Throw exForce
            End If


            Return True
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: InsertAuditTrail; Module Name: " & mstrModuleName)
            Return False
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
        End Try
    End Function

    Public Function getStatusComboList(ByVal strTableName As String, Optional ByVal blnAddSelect As Boolean = False) As DataSet
        Dim dsList As New DataSet
        Dim StrQ As String = String.Empty
        objDataOperation = New clsDataOperation
        Try
            StrQ = "SELECT Id, Name FROM ( "

            If blnAddSelect Then
                StrQ &= "SELECT -1 AS Id, ' ' + @Select AS Name UNION "
                objDataOperation.AddParameter("@Select", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 1, "Select"))
            End If

            StrQ &= "SELECT '" & enBatchPostingStatus.Pending & "' AS Id, @Pending AS Name UNION " & _
                    "SELECT '" & enBatchPostingStatus.Success & "' AS Id, @Success AS Name UNION " & _
                    "SELECT '" & enBatchPostingStatus.Failed & "' AS Id, @Failed AS Name UNION " & _
                    "SELECT '" & enBatchPostingStatus.Unknown & "' AS Id, @Unknown AS Name UNION " & _
                    "SELECT '" & enBatchPostingStatus.Submitted & "' AS Id, @Submitted AS Name  "

            StrQ &= " ) AS A " & _
                    " WHERE 1 = 1 "

            objDataOperation.AddParameter("@Pending", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 2, "Pending"))
            objDataOperation.AddParameter("@Success", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 3, "Success"))
            objDataOperation.AddParameter("@Failed", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 4, "Failed"))
            objDataOperation.AddParameter("@Unknown", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 5, "Unknown"))
            objDataOperation.AddParameter("@Submitted", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 11, "Submitted"))

            dsList = objDataOperation.ExecQuery(StrQ, strTableName)

            If objDataOperation.ErrorMessage <> "" Then
                Throw New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
            End If

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: getStatusComboList; Module Name: " & mstrModuleName)
        Finally
        End Try
        Return dsList
    End Function

    Public Function getNextRefNo(ByVal strVoucherNo As String, Optional ByVal xDataOp As clsDataOperation = Nothing) As Integer
        Dim dsList As New DataSet
        Dim StrQ As String = String.Empty
        Dim intRefNo As Integer = 1

        If xDataOp Is Nothing Then
            objDataOperation = New clsDataOperation
        Else
            objDataOperation = xDataOp
        End If
        objDataOperation.ClearParameters()

        Try

            StrQ = "SELECT ISNULL(MAX(CAST(refno AS INT)), 0) + 1 AS NextRefNo " & _
                    "FROM prpayment_batchposting_master " & _
                    "WHERE prpayment_batchposting_master.isvoid = 0 " & _
                          "AND refno <> '' " & _
                          "AND voucherno = '" & strVoucherNo & "' "



            dsList = objDataOperation.ExecQuery(StrQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                Throw New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
            End If

            If dsList.Tables(0).Rows.Count > 0 Then
                intRefNo = CInt(dsList.Tables(0).Rows(0).Item(0))
            End If

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: getNextRefNo; Module Name: " & mstrModuleName)
        Finally
            If xDataOp Is Nothing Then objDataOperation = Nothing
        End Try
        Return intRefNo
    End Function

    Public Function IsBatchPostingPendingForApproval(ByVal strVoucherNo As String, Optional ByVal xDataOpr As clsDataOperation = Nothing, Optional ByVal strDatabaseName As String = "") As Boolean
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception


        If xDataOpr Is Nothing Then
            objDataOperation = New clsDataOperation
        Else
            objDataOperation = xDataOpr
        End If
        objDataOperation.ClearParameters()

        Try
            Dim strDBName As String = If(strDatabaseName.Trim <> "", strDatabaseName & "..", "")

            strQ = "SELECT * " & _
                   " FROM " & strDBName & "prpayment_batchposting_master where " & _
                   "   isvoid = 0 " & _
                   "   AND voucherno = '" & strVoucherNo & "' " & _
                   "   AND statusunkid = " & enApprovalStatus.SubmitForApproval & " "

            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            If dsList.Tables(0).Rows.Count > 0 Then
                Return True
            Else
                Return False
            End If

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: IsBatchPostingPendingForApproval; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            If xDataOpr Is Nothing Then objDataOperation = Nothing
        End Try

    End Function

    Public Function getApprovalStatusComboList(ByVal strList As String, Optional ByVal blnAddSelect As Boolean = False) As DataSet
        Dim dsList As New DataSet
        Dim StrQ As String = String.Empty
        objDataOperation = New clsDataOperation
        Try
            If blnAddSelect Then
                StrQ = "SELECT 0 AS Id, @Select AS Name UNION "
            End If

            StrQ &= "SELECT '" & enApprovalStatus.SubmitForApproval & "' AS Id, @Pending AS Name UNION " & _
                    "SELECT '" & enApprovalStatus.Approved & "' AS Id, @Approved AS Name UNION " & _
                    "SELECT '" & enApprovalStatus.Rejected & "' AS Id, @Reject AS Name "

            objDataOperation.AddParameter("@Select", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 6, "Select"))
            objDataOperation.AddParameter("@Pending", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 7, "Pending"))
            objDataOperation.AddParameter("@Approved", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 8, "Approved"))
            objDataOperation.AddParameter("@Reject", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 9, "Rejected"))


            dsList = objDataOperation.ExecQuery(StrQ, strList)

            If objDataOperation.ErrorMessage <> "" Then
                Throw New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
            End If

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: getApprovalStatusComboList; Module Name: " & mstrModuleName)
        Finally
        End Try
        Return dsList
    End Function

    Public Function GetBatchNameList(ByVal strList As String, ByVal intPeriodId As Integer, ByVal intStatusId As Integer, Optional ByVal blnAddMainBatch As Boolean = False, Optional ByVal blnAddSelect As Boolean = False, Optional ByVal xDataOpr As clsDataOperation = Nothing, Optional ByVal strDatabaseName As String = "") As DataSet
        Dim objPaymentTran As New clsPayment_tran
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception


        If xDataOpr Is Nothing Then
            objDataOperation = New clsDataOperation
        Else
            objDataOperation = xDataOpr
        End If
        objDataOperation.ClearParameters()

        Try

            If blnAddSelect = True Then
                strQ = " SELECT 0 As Id,@Select As name UNION "
            End If

            If blnAddMainBatch = True AndAlso intPeriodId > 0 Then
                Dim dsMainBatch As DataSet = objPaymentTran.Get_DIST_VoucherNo("Voucher", False, clsPayment_tran.enPaymentRefId.PAYSLIP, clsPayment_tran.enPayTypeId.PAYMENT, strDatabaseName, intPeriodId)
                Dim intId As Integer = 1
                For Each drMainBatch As DataRow In dsMainBatch.Tables(0).Rows
                    strQ &= " SELECT " & intId * -1 & " As Id, '" & drMainBatch.Item("voucherno") & "' As name UNION "
                    intId = intId + 1
                Next
            End If

            strQ &= "SELECT " & _
                   "  paymentbatchpostingunkid As Id " & _
                   " ,batchname As name " & _
                   " FROM " & strDatabaseName & "..prpayment_batchposting_master " & _
                   " where  isvoid = 0 " & _
                   "   AND periodunkid = '" & intPeriodId & "' "

            If intStatusId > 0 Then
                strQ &= " AND statusunkid = '" & intStatusId & "'"
            End If

            objDataOperation.AddParameter("@Select", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 10, "Select"))
            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: GetBatchNameList; Module Name: " & mstrModuleName)
        Finally
            objPaymentTran = Nothing
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            If xDataOpr Is Nothing Then objDataOperation = Nothing
        End Try
        Return dsList
    End Function

    Public Function UpdateApprovalStatus(ByVal dtEmployeeList As DataTable, ByVal dsEmpBankList As DataSet, ByVal dsEmpSalaryList As DataSet, ByVal dsGarnisheeBankList As DataSet, ByVal intPeriodId As Integer, ByVal dtCurrentDateAndTime As Date, Optional ByVal xDataOp As clsDataOperation = Nothing) As Boolean
        Dim exForce As Exception
        If xDataOp Is Nothing Then
            objDataOperation = New clsDataOperation
            objDataOperation.BindTransaction()
        Else
            objDataOperation = xDataOp
        End If
        Try
            If Update(objDataOperation) = False Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            For Each drEmployee As DataRow In dtEmployeeList.Rows
                If CInt(drEmployee.Item("dpndtbeneficetranunkid")) > 0 Then
                    Dim objGarnisheesBankTran As New clsGarnishees_Bank_Tran
                    Dim drGarnisheesBank() As DataRow = dsGarnisheeBankList.Tables(0).Select("dpndtbeneficetranunkid = " & drEmployee.Item("dpndtbeneficetranunkid") & " AND periodunkid = " & intPeriodId & " ")
                    If drGarnisheesBank.Length > 0 Then
                        If CInt(drGarnisheesBank(0).Item("garnisheebanktranunkid")) > 0 Then
                            objGarnisheesBankTran._GarnisheeBankTranUnkid(objDataOperation) = CInt(drGarnisheesBank(0).Item("garnisheebanktranunkid"))
                            objGarnisheesBankTran._Branchunkid = CInt(drEmployee.Item("branchunkid"))
                            objGarnisheesBankTran._Accountno = CStr(drEmployee.Item("accountno"))
                            objGarnisheesBankTran._Userunkid = mintUserunkid
                            If objGarnisheesBankTran.Update(dtCurrentDateAndTime, objDataOperation) = False Then
                                objDataOperation.ReleaseTransaction(False)
                                Return False
                            End If
                        End If
                    End If
                    objGarnisheesBankTran = Nothing
                Else
                    If CInt(drEmployee.Item("employeeunkid")) = -1 Then 'Mobile Money Account
                        Dim objBranch As New clsbankbranch_master
                        objBranch._Branchunkid = CInt(drEmployee.Item("branchunkid"))

                        Dim objConfig As New clsConfigOptions
                        objConfig._Companyunkid = mintCompanyUnkid
                        objConfig._EFTMobileMoneyBank = CInt(objBranch._Bankgroupunkid)
                        objConfig._EFTMobileMoneyBranch = CInt(drEmployee.Item("branchunkid"))
                        objConfig._EFTMobileMoneyAccountNo = CStr(drEmployee.Item("accountno"))
                        objConfig.updateParam()
                        ConfigParameter._Object.Refresh()

                        objConfig = Nothing
                        objBranch = Nothing
                    Else
                        Dim intEmpbanktranunkid As Integer = -1
                Dim objEmpBankTran As New clsEmployeeBanks
                    Dim drEmpBank() As DataRow = dsEmpBankList.Tables(0).Select("employeeunkid = " & drEmployee.Item("employeeunkid") & " AND periodunkid = " & intPeriodId & " AND payment_typeid = " & CInt(enBankPaymentType.BankAccount) & " ")
                If drEmpBank.Length > 0 Then
                    objEmpBankTran._Empbanktranunkid = CInt(drEmpBank(0).Item("empbanktranunkid"))
                    objEmpBankTran._Branchunkid = CInt(drEmployee.Item("branchunkid"))
                    objEmpBankTran._Accountno = CStr(drEmployee.Item("accountno"))
                    objEmpBankTran._Userunkid = mintUserunkid
                    If objEmpBankTran._Empbanktranunkid > 0 Then
                        intEmpbanktranunkid = objEmpBankTran._Empbanktranunkid
                        If objEmpBankTran.Update(dtCurrentDateAndTime, objDataOperation) = False Then
                            objDataOperation.ReleaseTransaction(False)
                            Return False
                        End If
                            End If

                    Else
                            objEmpBankTran._Empbanktranunkid = -1
                            objEmpBankTran._Branchunkid = CInt(drEmployee.Item("branchunkid"))
                            objEmpBankTran._Accountno = CStr(drEmployee.Item("accountno"))
                            objEmpBankTran._Userunkid = mintUserunkid
                        objEmpBankTran._Employeeunkid = CInt(drEmployee.Item("employeeunkid"))
                        objEmpBankTran._Accounttypeunkid = -1

                        objEmpBankTran._Isvoid = False
                        objEmpBankTran._Voiduserunkid = 0
                        objEmpBankTran._Voiddatetime = Nothing
                        objEmpBankTran._Voidreason = ""
                        objEmpBankTran._Percetage = 100.0
                        objEmpBankTran._Priority = 1
                        objEmpBankTran._Periodunkid = intPeriodId
                            objEmpBankTran._Payment_typeid = CInt(enBankPaymentType.BankAccount)
                            objEmpBankTran._Mobileno = ""
                        If objEmpBankTran.Insert(dtCurrentDateAndTime, intEmpbanktranunkid, objDataOperation) = False Then
                            objDataOperation.ReleaseTransaction(False)
                            Return False
                        End If
                    End If
                    If intEmpbanktranunkid > 0 Then
                            Dim drEmpSalary() As DataRow = dsEmpSalaryList.Tables(0).Select("employeeunkid = " & drEmployee.Item("employeeunkid") & " AND Payment_typeid  = " & CInt(enBankPaymentType.BankAccount) & "  ")
                        If drEmpSalary.Length > 0 Then
                            Dim objEmpSalaryTran As New clsEmpSalaryTran
                            objEmpSalaryTran._Empsalarytranunkid = CInt(drEmpSalary(0).Item("empsalarytranunkid"))
                            objEmpSalaryTran._Empbanktranid = intEmpbanktranunkid
                            If objEmpSalaryTran.Update(objDataOperation) = False Then
                                objDataOperation.ReleaseTransaction(False)
                                Return False
                            End If
                            objEmpSalaryTran = Nothing
                        End If
                    End If
                objEmpBankTran = Nothing
                End If

                End If

            Next
            If xDataOp Is Nothing Then objDataOperation.ReleaseTransaction(True)
            Return True
        Catch ex As Exception
            objDataOperation.ReleaseTransaction(False)
            Throw New Exception(ex.Message & "; Procedure Name: UpdateApprovalStatus; Module Name: " & mstrModuleName)
            Return False
        Finally
            exForce = Nothing
            If xDataOp Is Nothing Then objDataOperation = Nothing
        End Try

    End Function

	'<Language> This Auto Generated Text Please Do Not Modify it.
#Region " Language & UI Settings "
	Public Shared Sub SetMessages()
		Try
			Language.setMessage(mstrModuleName, 1, "Select")
			Language.setMessage(mstrModuleName, 2, "Pending")
			Language.setMessage(mstrModuleName, 3, "Success")
			Language.setMessage(mstrModuleName, 4, "Failed")
			Language.setMessage(mstrModuleName, 5, "Unknown")
			Language.setMessage(mstrModuleName, 6, "Select")
			Language.setMessage(mstrModuleName, 7, "Pending")
			Language.setMessage(mstrModuleName, 8, "Approved")
            Language.setMessage(mstrModuleName, 9, "Rejected")
            Language.setMessage(mstrModuleName, 10, "Select")
            Language.setMessage(mstrModuleName, 11, "Submitted")
			
		Catch Ex As Exception
            Throw New Exception(Ex.Message & "; Procedure Name: SetMessages; Module Name: " & mstrModuleName)
		End Try
	End Sub
#End Region 'Language & UI Settings
	'</Language>
End Class
