﻿'************************************************************************************************************************************
'Class Name : clsStatutorypayment_master.vb
'Purpose    :
'Date       :07/08/2014
'Written By :Sohail
'Modified   :
'************************************************************************************************************************************

Imports eZeeCommonLib
Imports eZee.Common.eZeeForm
Imports System.ComponentModel

''' <summary>
''' Purpose: 
''' Developer: Sohail
''' </summary>
Public Class clsStatutorypayment_master
    Private Shared ReadOnly mstrModuleName As String = "clsStatutorypayment_master"
    Dim objDataOperation As clsDataOperation
    Dim mstrMessage As String = ""

#Region " Private variables "
    Private mintStatutorypaymentmasterunkid As Integer
    Private mintFromperiodunkid As Integer
    Private mintToperiodunkid As Integer
    Private mintMembershipunkid As Integer
    Private mstrReceiptno As String = String.Empty
    Private mdtReceiptdate As Date
    Private mdecTotal_Payableamount As Decimal
    Private mdecTotal_Receiptamount As Decimal
    Private mstrRemark As String = String.Empty
    Private mintUserunkid As Integer
    Private mblnIsvoid As Boolean
    Private mintVoiduserunkid As Integer
    Private mdtVoiddatetime As Date
    Private mstrVoidreason As String = String.Empty

    Private mstrWebIP As String = ""
    Private mstrWebhostName As String = ""
    Private mstrWebFormName As String = String.Empty
    Private mintLogEmployeeUnkid As Integer = -1
#End Region

#Region " Properties "
    'S.SANDEEP [28-May-2018] -- START 
    'ISSUE/ENHANCEMENT : {Audit Trails} 
    Private mstrFormName As String = String.Empty
    Public WriteOnly Property _FormName() As String
        Set(ByVal value As String)
            mstrFormName = value
        End Set
    End Property

    Private mstrClientIP As String = ""
    Public WriteOnly Property _ClientIP() As String
        Set(ByVal value As String)
            mstrClientIP = value
        End Set
    End Property

    Private mstrHostName As String = ""
    Public WriteOnly Property _HostName() As String
        Set(ByVal value As String)
            mstrHostName = value
        End Set
    End Property

    Private mblnIsWeb As Boolean = False
    Public WriteOnly Property _FromWeb() As Boolean
        Set(ByVal value As Boolean)
            mblnIsWeb = value
        End Set
    End Property

    Private mintAuditUserId As Integer = 0
    Public WriteOnly Property _AuditUserId() As Integer
        Set(ByVal value As Integer)
            mintAuditUserId = value
        End Set
    End Property

    Private mintCompanyUnkid As Integer = 0
    Public WriteOnly Property _CompanyUnkid() As Integer
        Set(ByVal value As Integer)
            mintCompanyUnkid = value
        End Set
    End Property

    Private mdtAuditDate As DateTime = Now
    Public WriteOnly Property _AuditDate() As DateTime
        Set(ByVal value As DateTime)
            mdtAuditDate = value
        End Set
    End Property

    Private xDataOpr As clsDataOperation = Nothing
    Public Property _xDataOpr() As clsDataOperation
        Get
            Return xDataOpr
        End Get
        Set(ByVal value As clsDataOperation)
            xDataOpr = value
        End Set
    End Property

    Private mintVoidlogingemployeeunkid As Integer = 0
    Public Property _Voidloginemployeeunkid() As Integer
        Get
            Return mintVoidlogingemployeeunkid
        End Get
        Set(ByVal value As Integer)
            mintVoidlogingemployeeunkid = value
        End Set
    End Property
    'S.SANDEEP [28-May-2018] -- END 
    ''' <summary>
    ''' Purpose: Get Message from Class 
    ''' Modify By: Sohail
    ''' </summary>
    Public ReadOnly Property _Message() As String
        Get
            Return mstrMessage
        End Get
    End Property

    ''' <summary>
    ''' Purpose: Get or Set statutorypaymentmasterunkid
    ''' Modify By: Sohail
    ''' </summary>
    Public Property _Statutorypaymentmasterunkid() As Integer
        Get
            Return mintStatutorypaymentmasterunkid
        End Get
        Set(ByVal value As Integer)
            mintStatutorypaymentmasterunkid = value
            Call GetData()
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set fromperiodunkid
    ''' Modify By: Sohail
    ''' </summary>
    Public Property _Fromperiodunkid() As Integer
        Get
            Return mintFromperiodunkid
        End Get
        Set(ByVal value As Integer)
            mintFromperiodunkid = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set toperiodunkid
    ''' Modify By: Sohail
    ''' </summary>
    Public Property _Toperiodunkid() As Integer
        Get
            Return mintToperiodunkid
        End Get
        Set(ByVal value As Integer)
            mintToperiodunkid = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set membershipunkid
    ''' Modify By: Sohail
    ''' </summary>
    Public Property _Membershipunkid() As Integer
        Get
            Return mintMembershipunkid
        End Get
        Set(ByVal value As Integer)
            mintMembershipunkid = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set receiptno
    ''' Modify By: Sohail
    ''' </summary>
    Public Property _Receiptno() As String
        Get
            Return mstrReceiptno
        End Get
        Set(ByVal value As String)
            mstrReceiptno = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set receiptdate
    ''' Modify By: Sohail
    ''' </summary>
    Public Property _Receiptdate() As Date
        Get
            Return mdtReceiptdate
        End Get
        Set(ByVal value As Date)
            mdtReceiptdate = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set total_payableamount
    ''' Modify By: Sohail
    ''' </summary>
    Public Property _Total_Payableamount() As Decimal
        Get
            Return mdecTotal_Payableamount
        End Get
        Set(ByVal value As Decimal)
            mdecTotal_Payableamount = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set total_receiptamount
    ''' Modify By: Sohail
    ''' </summary>
    Public Property _Total_Receiptamount() As Decimal
        Get
            Return mdecTotal_Receiptamount
        End Get
        Set(ByVal value As Decimal)
            mdecTotal_Receiptamount = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set remark
    ''' Modify By: Sohail
    ''' </summary>
    Public Property _Remark() As String
        Get
            Return mstrRemark
        End Get
        Set(ByVal value As String)
            mstrRemark = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set userunkid
    ''' Modify By: Sohail
    ''' </summary>
    Public Property _Userunkid() As Integer
        Get
            Return mintUserunkid
        End Get
        Set(ByVal value As Integer)
            mintUserunkid = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set isvoid
    ''' Modify By: Sohail
    ''' </summary>
    Public Property _Isvoid() As Boolean
        Get
            Return mblnIsvoid
        End Get
        Set(ByVal value As Boolean)
            mblnIsvoid = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set voiduserunkid
    ''' Modify By: Sohail
    ''' </summary>
    Public Property _Voiduserunkid() As Integer
        Get
            Return mintVoiduserunkid
        End Get
        Set(ByVal value As Integer)
            mintVoiduserunkid = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set voiddatetime
    ''' Modify By: Sohail
    ''' </summary>
    Public Property _Voiddatetime() As Date
        Get
            Return mdtVoiddatetime
        End Get
        Set(ByVal value As Date)
            mdtVoiddatetime = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set voidreason
    ''' Modify By: Sohail
    ''' </summary>
    Public Property _Voidreason() As String
        Get
            Return mstrVoidreason
        End Get
        Set(ByVal value As String)
            mstrVoidreason = value
        End Set
    End Property



    Public WriteOnly Property _DataOperation() As clsDataOperation
        Set(ByVal value As clsDataOperation)
            objDataOperation = value
        End Set
    End Property

    Public WriteOnly Property _WebIP() As String
        Set(ByVal value As String)
            mstrWebIP = value
        End Set
    End Property

    Public WriteOnly Property _WebHostName() As String
        Set(ByVal value As String)
            mstrWebhostName = value
        End Set
    End Property

    Public Property _WebFormName() As String
        Get
            Return mstrWebFormName
        End Get
        Set(ByVal value As String)
            mstrWebFormName = value
        End Set
    End Property

    Public WriteOnly Property _LoginEmployeeUnkid() As Integer
        Set(ByVal value As Integer)
            mintLogEmployeeUnkid = value
        End Set
    End Property
#End Region

    ''' <summary>
    ''' Modify By: Sohail
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>
    Public Sub GetData()
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        objDataOperation = New clsDataOperation

        Try
            strQ = "SELECT " & _
              "  statutorypaymentmasterunkid " & _
              ", fromperiodunkid " & _
              ", toperiodunkid " & _
              ", membershipunkid " & _
              ", receiptno " & _
              ", receiptdate " & _
              ", total_payableamount " & _
              ", total_receiptamount " & _
              ", remark " & _
              ", userunkid " & _
              ", isvoid " & _
              ", voiduserunkid " & _
              ", voiddatetime " & _
              ", voidreason " & _
             "FROM prstatutorypayment_master " & _
             "WHERE statutorypaymentmasterunkid = @statutorypaymentmasterunkid "

            objDataOperation.AddParameter("@statutorypaymentmasterunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintStatutorypaymentmasterunkid.ToString)

            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            For Each dtRow As DataRow In dsList.Tables(0).Rows
                mintStatutorypaymentmasterunkid = CInt(dtRow.Item("statutorypaymentmasterunkid"))
                mintFromperiodunkid = CInt(dtRow.Item("fromperiodunkid"))
                mintToperiodunkid = CInt(dtRow.Item("toperiodunkid"))
                mintMembershipunkid = CInt(dtRow.Item("membershipunkid"))
                mstrReceiptno = dtRow.Item("receiptno").ToString
                mdtReceiptdate = dtRow.Item("receiptdate")
                mdecTotal_Payableamount = dtRow.Item("total_payableamount")
                mdecTotal_Receiptamount = dtRow.Item("total_receiptamount")
                mstrRemark = dtRow.Item("remark").ToString
                mintUserunkid = CInt(dtRow.Item("userunkid"))
                mblnIsvoid = CBool(dtRow.Item("isvoid"))
                mintVoiduserunkid = CInt(dtRow.Item("voiduserunkid"))
                If IsDBNull(dtRow.Item("voiddatetime")) Then
                    mdtVoiddatetime = Nothing
                Else
                    mdtVoiddatetime = dtRow.Item("voiddatetime")
                End If
                mstrVoidreason = dtRow.Item("voidreason").ToString
                Exit For
            Next
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: GetData; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
    End Sub


    ''' <summary>
    ''' Modify By: Sohail
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>
    Public Function GetList(ByVal strTableName As String _
                          , Optional ByVal intMembershipUnkId As Integer = 0 _
                          , Optional ByVal strReceiptNo As String = "" _
                          , Optional ByVal dtReceiptDate As Date = Nothing _
                          , Optional ByVal strFilter As String = "" _
                            ) As DataSet

        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        objDataOperation = New clsDataOperation

        Try
            strQ = "SELECT  prstatutorypayment_master.statutorypaymentmasterunkid  " & _
                          ", prstatutorypayment_master.fromperiodunkid " & _
                          ", ISNULL(FPeriod.period_name, '') AS FromPeriodName " & _
                          ", prstatutorypayment_master.toperiodunkid " & _
                          ", ISNULL(TPeriod.period_name, '') AS ToPeriodName " & _
                          ", prstatutorypayment_master.membershipunkid " & _
                          ", hrmembership_master.membershipcode " & _
                          ", hrmembership_master.membershipname " & _
                          ", prstatutorypayment_master.receiptno " & _
                          ", CONVERT(CHAR(8), prstatutorypayment_master.receiptdate, 112) AS receiptdate " & _
                          ", prstatutorypayment_master.total_payableamount " & _
                          ", prstatutorypayment_master.total_receiptamount " & _
                          ", prstatutorypayment_master.remark " & _
                          ", prstatutorypayment_master.userunkid " & _
                          ", prstatutorypayment_master.isvoid " & _
                          ", prstatutorypayment_master.voiduserunkid " & _
                          ", prstatutorypayment_master.voiddatetime " & _
                          ", prstatutorypayment_master.voidreason " & _
                    "FROM    prstatutorypayment_master " & _
                            "LEFT JOIN hrmembership_master ON hrmembership_master.membershipunkid = prstatutorypayment_master.membershipunkid " & _
                            "LEFT JOIN cfcommon_period_tran AS FPeriod ON FPeriod.periodunkid = prstatutorypayment_master.fromperiodunkid " & _
                            "LEFT JOIN cfcommon_period_tran AS TPeriod ON TPeriod.periodunkid = prstatutorypayment_master.toperiodunkid " & _
                    "WHERE   ISNULL(prstatutorypayment_master.isvoid, 0) = 0 " & _
                            "AND hrmembership_master.isactive = 1 "

            If intMembershipUnkId > 0 Then
                strQ &= " AND hrmembership_master.membershipunkid = @membershipunkid "
                objDataOperation.AddParameter("@membershipunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intMembershipUnkId)
            End If

            If strReceiptNo.Trim <> "" Then
                strQ &= " AND prstatutorypayment_master.receiptno = @membershipunkid "
                objDataOperation.AddParameter("@membershipunkid", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, strReceiptNo)
            End If

            If dtReceiptDate <> Nothing Then
                strQ &= " AND CONVERT(CHAR(8), prstatutorypayment_master.receiptdate, 112) = @receiptdate "
                objDataOperation.AddParameter("@receiptdate", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, eZeeDate.convertDate(dtReceiptDate))
            End If

            If strFilter.Trim <> "" Then
                strQ &= " AND " & strFilter
            End If

            dsList = objDataOperation.ExecQuery(strQ, strTableName)

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: GetList; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
        Return dsList
    End Function


    Public Function Get_DIST_ReceiptNo(ByVal strDatabaseName As String _
                                       , ByVal strTableName As String _
                                     , ByVal blnAddSelect As Boolean _
                                     , Optional ByVal intPeriodId As Integer = 0 _
                                     , Optional ByVal intEmployeeId As Integer = 0 _
                                     , Optional ByVal intMembershipId As Integer = 0 _
                                     , Optional ByVal strFilterQuery As String = "" _
                                     ) As DataSet

        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        objDataOperation = New clsDataOperation

        Try

            'Sohail (21 Aug 2015) -- Start
            'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
            'If strDatabaseName.Trim = "" Then strDatabaseName = FinancialYear._Object._DatabaseName
            'Sohail (21 Aug 2015) -- End

            If blnAddSelect = True Then
                strQ = "SELECT ' ' + @Select AS receiptno UNION "
                objDataOperation.AddParameter("@Select", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 3, "Select"))
            End If

            strQ &= "SELECT DISTINCT " & _
                            " ISNULL(prstatutorypayment_master.receiptno,'') AS receiptno " & _
                    "FROM    " & strDatabaseName & "..prstatutorypayment_master " & _
                             "LEFT JOIN " & strDatabaseName & "..prstatutorypayment_tran ON prstatutorypayment_tran.statutorypaymentmasterunkid = prstatutorypayment_tran.statutorypaymentmasterunkid " & _
                             "LEFT JOIN " & strDatabaseName & "..hremployee_meminfo_tran ON hremployee_meminfo_tran.membershiptranunkid = prstatutorypayment_tran.membershiptranunkid " & _
                    "WHERE   ISNULL(prstatutorypayment_master.isvoid, 0) = 0 " & _
                            "AND ISNULL(prstatutorypayment_tran.isvoid, 0) = 0 "

            If intPeriodId > 0 Then
                strQ &= " AND prstatutorypayment_tran.periodunkid = @periodunkid "
                objDataOperation.AddParameter("@periodunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intPeriodId.ToString)
            End If

            If intEmployeeId > 0 Then
                strQ &= " AND prstatutorypayment_tran.employeeunkid = @employeeunkid "
                objDataOperation.AddParameter("@employeeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intEmployeeId.ToString)
            End If

            If intMembershipId > 0 Then
                strQ &= " AND prstatutorypayment_master.membershipunkid = @membershipunkid "
                objDataOperation.AddParameter("@membershipunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intMembershipId.ToString)
            End If

            If strFilterQuery.Trim <> "" Then
                strQ &= " AND " & strFilterQuery
            End If

            dsList = objDataOperation.ExecQuery(strQ, strTableName)

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If


        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: Get_DIST_ReceiptNo; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
        Return dsList
    End Function

    ''' <summary>
    ''' Modify By: Sohail
    ''' </summary>
    ''' <returns>Boolean</returns>
    ''' <purpose> INSERT INTO Database Table (prstatutorypayment_master) </purpose>
    Public Function Insert(ByVal dtTable As DataTable, ByVal dtCurrentDateAndTime As Date) As Boolean
        'Sohail (21 Aug 2015) - [dtCurrentDateAndTime]

        If isExist(mstrReceiptno) Then
            mstrMessage = Language.getMessage(mstrModuleName, 1, "Sorry, This Receipt No. already Exists.")
            Return False
        End If

        Dim objDataOperation As clsDataOperation

        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        objDataOperation = New clsDataOperation
        objDataOperation.BindTransaction()

        Try
            objDataOperation.AddParameter("@fromperiodunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintFromperiodunkid.ToString)
            objDataOperation.AddParameter("@toperiodunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintToperiodunkid.ToString)
            objDataOperation.AddParameter("@membershipunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintMembershipunkid.ToString)
            objDataOperation.AddParameter("@receiptno", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrReceiptno.ToString)
            objDataOperation.AddParameter("@receiptdate", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, mdtReceiptdate.ToString)
            objDataOperation.AddParameter("@total_payableamount", SqlDbType.Decimal, eZeeDataType.DECIMAL_SIZE, mdecTotal_Payableamount.ToString)
            objDataOperation.AddParameter("@total_receiptamount", SqlDbType.Decimal, eZeeDataType.DECIMAL_SIZE, mdecTotal_Receiptamount.ToString)
            objDataOperation.AddParameter("@remark", SqlDbType.NVarChar, eZeeDataType.DESC_SIZE, mstrRemark.ToString)
            objDataOperation.AddParameter("@userunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintUserunkid.ToString)
            objDataOperation.AddParameter("@isvoid", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsvoid.ToString)
            objDataOperation.AddParameter("@voiduserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintVoiduserunkid.ToString)
            If mdtVoiddatetime = Nothing Then
                objDataOperation.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, DBNull.Value)
            Else
                objDataOperation.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, mdtVoiddatetime)
            End If
            objDataOperation.AddParameter("@voidreason", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrVoidreason.ToString)

            strQ = "INSERT INTO prstatutorypayment_master ( " & _
              "  fromperiodunkid " & _
              ", toperiodunkid " & _
              ", membershipunkid " & _
              ", receiptno " & _
              ", receiptdate " & _
              ", total_payableamount " & _
              ", total_receiptamount " & _
              ", remark " & _
              ", userunkid " & _
              ", isvoid " & _
              ", voiduserunkid " & _
              ", voiddatetime " & _
              ", voidreason" & _
            ") VALUES (" & _
              " @fromperiodunkid " & _
              ", @toperiodunkid " & _
              ", @membershipunkid " & _
              ", @receiptno " & _
              ", @receiptdate " & _
              ", @total_payableamount " & _
              ", @total_receiptamount " & _
              ", @remark " & _
              ", @userunkid " & _
              ", @isvoid " & _
              ", @voiduserunkid " & _
              ", @voiddatetime " & _
              ", @voidreason" & _
            "); SELECT @@identity"

            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            mintStatutorypaymentmasterunkid = dsList.Tables(0).Rows(0).Item(0)

            'Sohail (21 Aug 2015) -- Start
            'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
            'If InsertAuditTrailForStatutoryPaymentMaster(objDataOperation, 1) = False Then
            If InsertAuditTrailForStatutoryPaymentMaster(objDataOperation, 1, dtCurrentDateAndTime) = False Then
                'Sohail (21 Aug 2015) -- End
                objDataOperation.ReleaseTransaction(False)
                Return False
            End If

            If dtTable IsNot Nothing Then
                Dim objStatPayTran As clsStatutoryPayment_Tran
                For Each dtRow As DataRow In dtTable.Rows
                    objStatPayTran = New clsStatutoryPayment_Tran

                    With objStatPayTran
                        ._Statutorypaymentmasterunkid = mintStatutorypaymentmasterunkid
                        ._Periodunkid = dtRow.Item("periodunkid").ToString
                        ._Employeeunkid = dtRow.Item("employeeunkid").ToString
                        ._Membershiptranunkid = dtRow.Item("membershiptranunkid").ToString
                        ._Emp_Contribution = CDec(dtRow.Item("emp_contribution").ToString)
                        ._Empl_Contribution = CDec(dtRow.Item("empl_contribution").ToString)
                        ._Total_Contribution = CDec(dtRow.Item("total_contribution").ToString)

                        ._Userunkid = dtRow.Item("userunkid").ToString
                        ._Isvoid = dtRow.Item("isvoid").ToString
                        ._Voiduserunkid = dtRow.Item("voiduserunkid").ToString
                        If dtRow.Item("Voiddatetime").ToString = Nothing Then
                            ._Voiddatetime = Nothing
                        Else
                            ._Voiddatetime = dtRow.Item("Voiddatetime").ToString
                        End If
                        ._Voidreason = dtRow.Item("Voidreason").ToString
                        '._WebFormName = mstrWebFormName
                        '._HostName = mstrWebhostName
                        '._WebIP = mstrWebIP
                        ._FormName = mstrFormName
                        ._LoginEmployeeUnkid = mintLogEmployeeUnkid
                        ._ClientIP = mstrClientIP
                        ._HostName = mstrHostName
                        ._FromWeb = mblnIsWeb
                        ._AuditUserId = mintAuditUserId
._CompanyUnkid = mintCompanyUnkid
                        ._AuditDate = mdtAuditDate
                        ._LoginEmployeeUnkid = mintLogEmployeeUnkid

                        'Sohail (21 Aug 2015) -- Start
                        'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
                        'If .Insert(objDataOperation) = False Then
                        If .Insert(objDataOperation, dtCurrentDateAndTime) = False Then
                            'Sohail (21 Aug 2015) -- End
                            objDataOperation.ReleaseTransaction(False)
                            Return False
                        End If

                    End With
                Next
            End If

            objDataOperation.ReleaseTransaction(True)

            Return True

        Catch ex As Exception
            objDataOperation.ReleaseTransaction(False)
            Throw New Exception(ex.Message & "; Procedure Name: Insert; Module Name: " & mstrModuleName)
            Return False
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
    End Function

    ''' <summary>
    ''' Modify By: Sohail
    ''' </summary>
    ''' <returns>Boolean</returns>
    ''' <purpose> Update Database Table (prstatutorypayment_master) </purpose>
    Public Function Update(ByVal dtTable As DataTable, ByVal strVoidTranIDs As String, ByVal dtCurrentDateAndTime As Date) As Boolean
        'Sohail (21 Aug 2015) - [dtCurrentDateAndTime]

        If isExist(mstrReceiptno, mintStatutorypaymentmasterunkid) Then
            mstrMessage = Language.getMessage(mstrModuleName, 1, "Sorry, This Receipt No. already Exists.")
            Return False
        End If

        Dim objDataOperation As clsDataOperation

        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        objDataOperation = New clsDataOperation
        objDataOperation.BindTransaction()

        Try
            objDataOperation.AddParameter("@statutorypaymentmasterunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintStatutorypaymentmasterunkid.ToString)
            objDataOperation.AddParameter("@fromperiodunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintFromperiodunkid.ToString)
            objDataOperation.AddParameter("@toperiodunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintToperiodunkid.ToString)
            objDataOperation.AddParameter("@membershipunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintMembershipunkid.ToString)
            objDataOperation.AddParameter("@receiptno", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrReceiptno.ToString)
            objDataOperation.AddParameter("@receiptdate", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, mdtReceiptdate.ToString)
            objDataOperation.AddParameter("@total_payableamount", SqlDbType.Decimal, eZeeDataType.DECIMAL_SIZE, mdecTotal_Payableamount.ToString)
            objDataOperation.AddParameter("@total_receiptamount", SqlDbType.Decimal, eZeeDataType.DECIMAL_SIZE, mdecTotal_Receiptamount.ToString)
            objDataOperation.AddParameter("@remark", SqlDbType.NVarChar, eZeeDataType.DESC_SIZE, mstrRemark.ToString)
            objDataOperation.AddParameter("@userunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintUserunkid.ToString)
            objDataOperation.AddParameter("@isvoid", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsvoid.ToString)
            objDataOperation.AddParameter("@voiduserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintVoiduserunkid.ToString)
            If mdtVoiddatetime = Nothing Then
                objDataOperation.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, DBNull.Value)
            Else
                objDataOperation.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, mdtVoiddatetime)
            End If
            objDataOperation.AddParameter("@voidreason", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrVoidreason.ToString)

            strQ = "UPDATE prstatutorypayment_master SET " & _
              "  fromperiodunkid = @fromperiodunkid " & _
              ", toperiodunkid = @toperiodunkid " & _
              ", membershipunkid = @membershipunkid" & _
              ", receiptno = @receiptno" & _
              ", receiptdate = @receiptdate" & _
              ", total_payableamount = @total_payableamount" & _
              ", total_receiptamount = @total_receiptamount" & _
              ", remark = @remark" & _
              ", userunkid = @userunkid" & _
              ", isvoid = @isvoid" & _
              ", voiduserunkid = @voiduserunkid" & _
              ", voiddatetime = @voiddatetime" & _
              ", voidreason = @voidreason " & _
            "WHERE statutorypaymentmasterunkid = @statutorypaymentmasterunkid "

            Call objDataOperation.ExecNonQuery(strQ)

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            'Sohail (21 Aug 2015) -- Start
            'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
            'If InsertAuditTrailForStatutoryPaymentMaster(objDataOperation, 2) = False Then
            If InsertAuditTrailForStatutoryPaymentMaster(objDataOperation, 2, dtCurrentDateAndTime) = False Then
                'Sohail (21 Aug 2015) -- End
                objDataOperation.ReleaseTransaction(False)
                Return False
            End If

            If dtTable IsNot Nothing Then
                Dim objStatPayTran As clsStatutoryPayment_Tran
                Dim intStatutorypaymenttranunkid As Integer

                For Each dtRow As DataRow In dtTable.Rows
                    objStatPayTran = New clsStatutoryPayment_Tran
                    intStatutorypaymenttranunkid = 0

                    With objStatPayTran
                        ._Statutorypaymenttranunkid = intStatutorypaymenttranunkid
                        ._Statutorypaymentmasterunkid = mintStatutorypaymentmasterunkid
                        ._Periodunkid = CInt(dtRow.Item("periodunkid"))
                        ._Employeeunkid = dtRow.Item("employeeunkid").ToString
                        ._Membershiptranunkid = dtRow.Item("membershiptranunkid").ToString
                        ._Emp_Contribution = CDec(dtRow.Item("emp_contribution").ToString)
                        ._Empl_Contribution = CDec(dtRow.Item("empl_contribution").ToString)
                        ._Total_Contribution = CDec(dtRow.Item("total_contribution").ToString)

                        ._Userunkid = dtRow.Item("userunkid").ToString
                        ._Isvoid = dtRow.Item("isvoid").ToString
                        ._Voiduserunkid = dtRow.Item("voiduserunkid").ToString
                        If dtRow.Item("Voiddatetime").ToString = Nothing Then
                            ._Voiddatetime = Nothing
                        Else
                            ._Voiddatetime = dtRow.Item("Voiddatetime").ToString
                        End If
                        ._Voidreason = dtRow.Item("Voidreason").ToString
                        '._WebFormName = mstrWebFormName
                        '._HostName = mstrWebhostName
                        '._WebIP = mstrWebIP
                        ._FormName = mstrFormName
                        ._LoginEmployeeUnkid = mintLogEmployeeUnkid
                        ._ClientIP = mstrClientIP
                        ._HostName = mstrHostName
                        ._FromWeb = mblnIsWeb
                        ._AuditUserId = mintAuditUserId
._CompanyUnkid = mintCompanyUnkid
                        ._AuditDate = mdtAuditDate
                        ._LoginEmployeeUnkid = mintLogEmployeeUnkid

                        If .isExist(CInt(dtRow.Item("periodunkid")), CInt(dtRow.Item("employeeunkid")), CInt(dtRow.Item("membershiptranunkid")), , intStatutorypaymenttranunkid) = True Then
                            ._Statutorypaymenttranunkid = intStatutorypaymenttranunkid
                            'Sohail (21 Aug 2015) -- Start
                            'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
                            'If .Update(objDataOperation) = False Then
                            If .Update(objDataOperation, dtCurrentDateAndTime) = False Then
                                'Sohail (21 Aug 2015) -- End
                                mstrMessage = ._Message
                                objDataOperation.ReleaseTransaction(False)
                                Return False
                            End If
                            'Sohail (21 Aug 2015) -- Start
                            'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
                            'ElseIf .Insert(objDataOperation) = False Then
                        ElseIf .Insert(objDataOperation, dtCurrentDateAndTime) = False Then
                            'Sohail (21 Aug 2015) -- End
                            objDataOperation.ReleaseTransaction(False)
                            Return False
                        End If

                    End With
                Next

                If strVoidTranIDs.Trim <> "" Then
                    For Each strID In strVoidTranIDs.Split(",")
                        objStatPayTran = New clsStatutoryPayment_Tran

                        'S.SANDEEP [28-May-2018] -- START
                        'ISSUE/ENHANCEMENT : {Audit Trails} 
                        objStatPayTran._FormName = mstrFormName
                        objStatPayTran._LoginEmployeeUnkid = mintLogEmployeeUnkid
                        objStatPayTran._ClientIP = mstrClientIP
                        objStatPayTran._HostName = mstrHostName
                        objStatPayTran._FromWeb = mblnIsWeb
                        objStatPayTran._AuditUserId = mintAuditUserId
objStatPayTran._CompanyUnkid = mintCompanyUnkid
                        objStatPayTran._AuditDate = mdtAuditDate
                        'S.SANDEEP [28-May-2018] -- END

                        'Sohail (21 Aug 2015) -- Start
                        'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
                        'If objStatPayTran.Delete(CInt(strID), objDataOperation) = False Then
                        If objStatPayTran.Delete(CInt(strID), objDataOperation, dtCurrentDateAndTime) = False Then
                            'Sohail (21 Aug 2015) -- End
                            objDataOperation.ReleaseTransaction(False)
                            Return False
                        End If
                    Next
                End If
            End If

            objDataOperation.ReleaseTransaction(True)

            Return True
        Catch ex As Exception
            objDataOperation.ReleaseTransaction(False)
            Throw New Exception(ex.Message & "; Procedure Name: Update; Module Name: " & mstrModuleName)
            Return False
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
    End Function

    ''' <summary>
    ''' Modify By: Sohail
    ''' </summary>
    ''' <returns>Boolean</returns>
    ''' <purpose> Delete Database Table (prstatutorypayment_master) </purpose>
    Public Function Delete(ByVal intUnkid As Integer, ByVal objDataOp As clsDataOperation, ByVal dtCurrentDateAndTime As Date) As Boolean
        'Sohail (21 Aug 2015) - [dtCurrentDateAndTime]

        'If isUsed(intUnkid) Then
        '    mstrMessage = "<Message>"
        '    Return False
        'End If

        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        objDataOperation = New clsDataOperation
        If objDataOp IsNot Nothing Then
            objDataOperation = objDataOp
            objDataOperation.ClearParameters()
        Else
            objDataOperation.BindTransaction()
        End If

        Try

            strQ = "UPDATE prstatutorypayment_master SET " & _
                         "  isvoid = @isvoid" & _
                         ", voiduserunkid = @voiduserunkid" & _
                         ", voiddatetime = @voiddatetime " & _
                         ", voidreason = @voidreason " & _
                    "WHERE statutorypaymentmasterunkid = @statutorypaymentmasterunkid "

            objDataOperation.AddParameter("@statutorypaymentmasterunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intUnkid)
            objDataOperation.AddParameter("@isvoid", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsvoid.ToString)
            objDataOperation.AddParameter("@voiduserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintVoiduserunkid.ToString)
            If mdtVoiddatetime = Nothing Then
                objDataOperation.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, DBNull.Value)
            Else
                objDataOperation.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, mdtVoiddatetime)
            End If
            objDataOperation.AddParameter("@voidreason", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrVoidreason.ToString)

            Call objDataOperation.ExecNonQuery(strQ)

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            _DataOperation = objDataOperation
            Me._Statutorypaymentmasterunkid = intUnkid

            'Sohail (21 Aug 2015) -- Start
            'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
            'If InsertAuditTrailForStatutoryPaymentMaster(objDataOperation, 3) = False Then
            If InsertAuditTrailForStatutoryPaymentMaster(objDataOperation, 3, dtCurrentDateAndTime) = False Then
                'Sohail (21 Aug 2015) -- End
                objDataOperation.ReleaseTransaction(False)
                Return False
            End If

            If objDataOp Is Nothing Then
                objDataOperation.ReleaseTransaction(True)
            End If

            Return True
        Catch ex As Exception
            objDataOperation.ReleaseTransaction(False)
            Throw New Exception(ex.Message & "; Procedure Name: Delete; Module Name: " & mstrModuleName)
            Return False
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
    End Function

    Public Function VoidAll(ByVal strUnkIdList As String, ByVal UserId As Integer, ByVal VoidDateTime As DateTime, ByVal strVoidReason As String, ByVal dtCurrentDateAndTime As Date, Optional ByVal bw As BackgroundWorker = Nothing) As Boolean
        'Sohail (21 Aug 2015) - [dtCurrentDateAndTime]

        Dim objDataOperation As clsDataOperation

        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim strQP As String = ""
        Dim strQS As String = ""
        Dim exForce As Exception
        Dim intUnkid As Integer
        Dim arrIDs() As String

        objDataOperation = New clsDataOperation
        objDataOperation.BindTransaction()

        Try
            arrIDs = strUnkIdList.Split(",")

            If bw IsNot Nothing Then
                bw.ReportProgress(0)
            End If
            Dim intCount As Integer = 0

            For i = 0 To arrIDs.Count - 1
                intUnkid = CInt(arrIDs(i))
                mblnIsvoid = True
                mintVoiduserunkid = UserId
                mdtVoiddatetime = VoidDateTime
                mstrVoidreason = strVoidReason

                'Sohail (21 Aug 2015) -- Start
                'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
                'If Delete(intUnkid, objDataOperation) = False Then
                If Delete(intUnkid, objDataOperation, dtCurrentDateAndTime) = False Then
                    'Sohail (21 Aug 2015) -- End
                    objDataOperation.ReleaseTransaction(False)
                    Return False
                End If

                Dim objStatPayTran As New clsStatutoryPayment_Tran

                'S.SANDEEP [28-May-2018] -- START
                'ISSUE/ENHANCEMENT : {Audit Trails} 
                objStatPayTran._FormName = mstrFormName
                objStatPayTran._LoginEmployeeUnkid = mintLogEmployeeUnkid
                objStatPayTran._ClientIP = mstrClientIP
                objStatPayTran._HostName = mstrHostName
                objStatPayTran._FromWeb = mblnIsWeb
                objStatPayTran._AuditUserId = mintAuditUserId
objStatPayTran._CompanyUnkid = mintCompanyUnkid
                objStatPayTran._AuditDate = mdtAuditDate
                'S.SANDEEP [28-May-2018] -- END

                If objStatPayTran.VoidByMasterUnkID(intUnkid, UserId, VoidDateTime, strVoidReason, objDataOperation) = False Then
                    objDataOperation.ReleaseTransaction(False)
                End If

                If bw IsNot Nothing Then
                    intCount = intCount + 1
                    bw.ReportProgress(intCount)
                End If
            Next

            objDataOperation.ReleaseTransaction(True)

            Return True
        Catch ex As Exception
            objDataOperation.ReleaseTransaction(False)
            Throw New Exception(ex.Message & "; Procedure Name: VoidAll; Module Name: " & mstrModuleName)
            Return False
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
    End Function

    ''' <summary>
    ''' Modify By: Sohail
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>
    Public Function isUsed(ByVal intUnkid As Integer) As Boolean
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        objDataOperation = New clsDataOperation

        Try
            strQ = "<Query>"

            objDataOperation.AddParameter("@statutorypaymentmasterunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intUnkid)

            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            Return dsList.Tables(0).Rows.Count > 0
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: isUsed; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
    End Function

    ''' <summary>
    ''' Modify By: Sohail
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>
    Public Function isExist(ByVal strReceiptNo As String, Optional ByVal intUnkid As Integer = -1) As Boolean
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        objDataOperation = New clsDataOperation

        Try
            strQ = "SELECT " & _
              "  statutorypaymentmasterunkid " & _
             "FROM prstatutorypayment_master " & _
             "WHERE ISNULL(isvoid, 0) = 0 " & _
             "AND receiptno = @receiptno "

            If intUnkid > 0 Then
                strQ &= " AND statutorypaymentmasterunkid <> @statutorypaymentmasterunkid"
                objDataOperation.AddParameter("@statutorypaymentmasterunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intUnkid)
            End If

            objDataOperation.AddParameter("@receiptno", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, strReceiptNo)


            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            Return dsList.Tables(0).Rows.Count > 0
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: isExist; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
    End Function

    Public Function InsertAuditTrailForStatutoryPaymentMaster(ByVal objDataOperation As clsDataOperation, ByVal AuditType As Integer, ByVal dtCurrentDateAndTime As Date, Optional ByVal blnIsGlobal As Boolean = False) As Boolean
        'Sohail (21 Aug 2015) - [dtCurrentDateAndTime]

        Dim strQ As String = ""
        Dim exForce As Exception

        objDataOperation = New clsDataOperation

        Try

            If mstrWebIP.Trim = "" Then mstrWebIP = getIP()
            If mstrWebhostName.Trim = "" Then mstrWebhostName = getHostName()

            strQ = "INSERT INTO atprstatutorypayment_master ( " & _
                              "  statutorypaymentmasterunkid " & _
                              ", fromperiodunkid " & _
                              ", toperiodunkid " & _
                              ", membershipunkid " & _
                              ", receiptno " & _
                              ", receiptdate " & _
                              ", total_payableamount " & _
                              ", total_receiptamount " & _
                              ", remark " & _
                              ", audittype " & _
                              ", audituserunkid " & _
                              ", auditdatetime " & _
                              ", ip " & _
                              ", machine_name" & _
                              ", form_name " & _
                              " " & _
                              " " & _
                              " " & _
                              " " & _
                              " " & _
                              ", isweb " & _
                              ", loginemployeeunkid " & _
                    ") VALUES (" & _
                              "  @statutorypaymentmasterunkid " & _
                              ", @fromperiodunkid " & _
                              ", @toperiodunkid " & _
                              ", @membershipunkid " & _
                              ", @receiptno " & _
                              ", @receiptdate " & _
                              ", @total_payableamount " & _
                              ", @total_receiptamount " & _
                              ", @remark " & _
                              ", @audittype " & _
                              ", @audituserunkid " & _
                              ", @auditdatetime " & _
                              ", @ip " & _
                              ", @machine_name" & _
                              ", @form_name " & _
                              " " & _
                              " " & _
                              " " & _
                              " " & _
                              " " & _
                              ", @isweb " & _
                              ", @loginemployeeunkid " & _
                    "); SELECT @@identity"




            objDataOperation.ClearParameters()
            objDataOperation.AddParameter("@statutorypaymentmasterunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintStatutorypaymentmasterunkid.ToString)
            objDataOperation.AddParameter("@fromperiodunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintFromperiodunkid.ToString)
            objDataOperation.AddParameter("@toperiodunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintToperiodunkid.ToString)
            objDataOperation.AddParameter("@membershipunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintMembershipunkid.ToString)
            objDataOperation.AddParameter("@receiptno", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrReceiptno.ToString)
            objDataOperation.AddParameter("@receiptdate", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, mdtReceiptdate.ToString)
            objDataOperation.AddParameter("@total_payableamount", SqlDbType.Decimal, eZeeDataType.DECIMAL_SIZE, mdecTotal_Payableamount.ToString)
            objDataOperation.AddParameter("@total_receiptamount", SqlDbType.Decimal, eZeeDataType.DECIMAL_SIZE, mdecTotal_Receiptamount.ToString)
            objDataOperation.AddParameter("@remark", SqlDbType.NVarChar, eZeeDataType.DESC_SIZE, mstrRemark.ToString)

            objDataOperation.AddParameter("@audittype", SqlDbType.Int, eZeeDataType.INT_SIZE, AuditType.ToString)
            objDataOperation.AddParameter("@audituserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintUserunkid)
            'Sohail (21 Aug 2015) -- Start
            'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
            'objDataOperation.AddParameter("@auditdatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, ConfigParameter._Object._CurrentDateAndTime)
            objDataOperation.AddParameter("@auditdatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, dtCurrentDateAndTime)
            'Sohail (21 Aug 2015) -- End
            objDataOperation.AddParameter("@ip", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrWebIP)
            objDataOperation.AddParameter("@machine_name", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrWebhostName)

            If mstrWebFormName.Trim.Length <= 0 Then
                objDataOperation.AddParameter("@form_name", SqlDbType.NVarChar, 500, mstrFormName)
                objDataOperation.AddParameter("@isweb", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsWeb)

                objDataOperation.AddParameter("@loginemployeeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, -1)
            Else
                objDataOperation.AddParameter("@form_name", SqlDbType.NVarChar, 500, mstrWebFormName)
                objDataOperation.AddParameter("@isweb", SqlDbType.Bit, eZeeDataType.BIT_SIZE, True)
                objDataOperation.AddParameter("@module_name1", SqlDbType.NVarChar, 500, Language.getMessage(mstrModuleName, 2, "WEB"))
                objDataOperation.AddParameter("@loginemployeeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintLogEmployeeUnkid)
            End If






            objDataOperation.ExecNonQuery(strQ)

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            Return True

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "InsertAuditTrailForStatutoryPaymentMaster", mstrModuleName)
            Return False
        Finally
            'objDataOperation = Nothing
        End Try
    End Function

    '<Language> This Auto Generated Text Please Do Not Modify it.
#Region " Language & UI Settings "
    Public Shared Sub SetMessages()
        Try
            Language.setMessage(mstrModuleName, 1, "Sorry, This Receipt No. already Exists.")
            Language.setMessage(mstrModuleName, 2, "WEB")
            Language.setMessage(mstrModuleName, 3, "Select")

        Catch Ex As Exception
            DisplayError.Show("-1", Ex.Message, "SetMessages", mstrModuleName)
        End Try
    End Sub
#End Region 'Language & UI Settings
    '</Language>
End Class