﻿'************************************************************************************************************************************
'Class Name : clsTranheadFormulaTran.vb
'Purpose    :
'Date       :12/07/2010
'Written By :Suhail
'Modified   :
'************************************************************************************************************************************

Imports eZeeCommonLib
''' <summary>
''' Purpose: 
''' Developer: Suhail
''' </summary>
Public Class clsTranheadFormulaTran

#Region " Private variables "
    Private Const mstrModuleName = "clsTranheadFormulaTran"
    Dim objDataOperation As clsDataOperation
    Dim mstrMessage As String = ""

    Private mintTranheadformulatranunkid As Integer
    Private mintFormulatranheadunkid As Integer
    Private mintTranheadunkid As Integer
    'Sohail (15 Feb 2012) -- Start
    'TRA - ENHANCEMENT
    Private mblnIsvoid As Boolean
    Private mintVoiduserunkid As Integer
    Private mdtVoiddatetime As Date
    Private mstrVoidreason As String = String.Empty
    'Sohail (15 Feb 2012) -- End
    Private mintDefaultvalue_id As Integer = 0
    Private mintTranheadslabtranunkid As Integer = 0 'Sohail (24 Sep 2013)

    Private mdtFormula As DataTable
#End Region

#Region " Constructor "
    Public Sub New()
        mdtFormula = New DataTable("Formula")
        Dim dCol As New DataColumn

        Try
            dCol = New DataColumn("formulatranheadunkid")
            dCol.DataType = System.Type.GetType("System.Int32")
            mdtFormula.Columns.Add(dCol)

            dCol = New DataColumn("tranheadunkid")
            dCol.DataType = System.Type.GetType("System.Int32")
            mdtFormula.Columns.Add(dCol)

            'Sohail (15 Feb 2012) -- Start
            'TRA - ENHANCEMENT
            mdtFormula.Columns.Add("isvoid", System.Type.GetType("System.Boolean")).DefaultValue = False
            mdtFormula.Columns.Add("voiduserunkid", System.Type.GetType("System.Int32")).DefaultValue = -1
            mdtFormula.Columns.Add("voiddatetime", System.Type.GetType("System.DateTime")).DefaultValue = Nothing
            mdtFormula.Columns.Add("voidreason", System.Type.GetType("System.String")).DefaultValue = String.Empty
            'Sohail (15 Feb 2012) -- End
            mdtFormula.Columns.Add("defaultvalue_id", System.Type.GetType("System.Int32")).DefaultValue = 0
            mdtFormula.Columns.Add("periodunkid", System.Type.GetType("System.Int32")).DefaultValue = 0 'Sohail (12 Apr 2013)
            mdtFormula.Columns.Add("tranheadslabtranunkid", System.Type.GetType("System.Int32")).DefaultValue = 0 'Sohail (24 Sep 2013)

            dCol = New DataColumn("AUD")
            dCol.DataType = System.Type.GetType("System.String")
            mdtFormula.Columns.Add(dCol)

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "New", mstrModuleName)
        End Try
    End Sub
#End Region

#Region " Properties "

    'S.SANDEEP [28-May-2018] -- START 
    'ISSUE/ENHANCEMENT : {Audit Trails} 
    Private mstrFormName As String = String.Empty
    Public WriteOnly Property _FormName() As String
        Set(ByVal value As String)
            mstrFormName = value
        End Set
    End Property

    Private mstrClientIP As String = ""
    Public WriteOnly Property _ClientIP() As String
        Set(ByVal value As String)
            mstrClientIP = value
        End Set
    End Property

    Private mstrHostName As String = ""
    Public WriteOnly Property _HostName() As String
        Set(ByVal value As String)
            mstrHostName = value
        End Set
    End Property

    Private mblnIsWeb As Boolean = False
    Public WriteOnly Property _FromWeb() As Boolean
        Set(ByVal value As Boolean)
            mblnIsWeb = value
        End Set
    End Property

    Private mintAuditUserId As Integer = 0
    Public WriteOnly Property _AuditUserId() As Integer
        Set(ByVal value As Integer)
            mintAuditUserId = value
        End Set
    End Property

    Private mintCompanyUnkid As Integer = 0
    Public WriteOnly Property _CompanyUnkid() As Integer
        Set(ByVal value As Integer)
            mintCompanyUnkid = value
        End Set
    End Property

    Private mdtAuditDate As DateTime = Now
    Public WriteOnly Property _AuditDate() As DateTime
        Set(ByVal value As DateTime)
            mdtAuditDate = value
        End Set
    End Property


    Private mintLoginEmployeeunkid As Integer = 0
    Public Property _LoginEmployeeunkid() As Integer
        Get
            Return mintLoginEmployeeunkid
        End Get
        Set(ByVal value As Integer)
            mintLoginEmployeeunkid = value
        End Set
    End Property

    Private mintVoidlogingemployeeunkid As Integer = 0
    Public Property _Voidloginemployeeunkid() As Integer
        Get
            Return mintVoidlogingemployeeunkid
        End Get
        Set(ByVal value As Integer)
            mintVoidlogingemployeeunkid = value
        End Set
    End Property
    'S.SANDEEP [28-May-2018] -- END 


    Public Property _Datasource() As DataTable
        Get
            Return mdtFormula
        End Get
        Set(ByVal value As DataTable)
            mdtFormula = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set formulatranheadunkid
    ''' Modify By: Suhail
    ''' </summary>
    Public Property _Formulatranheadunkid() As Integer
        Get
            Return mintFormulatranheadunkid
        End Get
        Set(ByVal value As Integer)
            mintFormulatranheadunkid = value
            Call GetData()
        End Set
    End Property





    ''' <summary>
    ''' Purpose: Get or Set tranheadformulatranunkid
    ''' Modify By: Suhail
    ''' </summary>
    Public Property _Tranheadformulatranunkid() As Integer
        Get
            Return mintTranheadformulatranunkid
        End Get
        Set(ByVal value As Integer)
            mintTranheadformulatranunkid = value
            'Call getData()
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get Message from Class 
    ''' Modify By: Suhail
    ''' </summary>
    Public ReadOnly Property _Message() As String
        Get
            Return mstrMessage
        End Get
    End Property

    ''' <summary>
    ''' Purpose: Get or Set tranheadunkid
    ''' Modify By: Suhail
    ''' </summary>
    Public Property _Tranheadunkid() As Integer
        Get
            Return mintTranheadunkid
        End Get
        Set(ByVal value As Integer)
            mintTranheadunkid = value
        End Set
    End Property

    'Sohail (28 Jan 2019) -- Start
    'NMB Enhancement - 76.1 - Option "Prorate Flat Rate head" on Transaction Head for Recurrent Head.
    Private xDataOpr As clsDataOperation = Nothing
    Public WriteOnly Property _DataOperation() As clsDataOperation
        Set(ByVal value As clsDataOperation)
            xDataOpr = value
        End Set
    End Property
    'Sohail (28 Jan 2019) -- End

#End Region

    ''' <summary>
    ''' Modify By: Suhail
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>
    Public Sub GetData()
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        'Sohail (28 Jan 2019) -- Start
        'NMB Enhancement - 76.1 - Option "Prorate Flat Rate head" on Transaction Head for Recurrent Head.
        'objDataOperation = New clsDataOperation
        If xDataOpr Is Nothing Then
            objDataOperation = New clsDataOperation
        Else
            objDataOperation = xDataOpr
        End If
        objDataOperation.ClearParameters()
        'Sohail (28 Jan 2019) -- End

        Try
            strQ = "SELECT " & _
              "  tranheadformulatranunkid " & _
              ", formulatranheadunkid " & _
              ", tranheadunkid " & _
              ", ISNULL(isvoid, 0) AS isvoid " & _
              ", ISNULL(voiduserunkid, -1) AS voiduserunkid " & _
              ", voiddatetime " & _
              ", ISNULL(voidreason, '') AS voidreason " & _
              ", ISNULL(defaultvalue_id, 0) AS defaultvalue_id " & _
              ", ISNULL(periodunkid, 0) AS periodunkid " & _
              ", ISNULL(tranheadslabtranunkid, 0) AS tranheadslabtranunkid " & _
              ", '' AS AUD " & _
             "FROM prtranhead_formula_tran " & _
             "WHERE formulatranheadunkid = @formulatranheadunkid " & _
             "AND ISNULL(isvoid,0) = 0 " 'Sohail (15 Feb 2012) - [isvoid], 'Sohail (16 May 2012) - [defaultvalue_id]
            'Sohail (24 Sep 2013) - [tranheadslabtranunkid]
            'Sohail (12 Apr 2013) - [periodunkid]

            objDataOperation.AddParameter("@formulatranheadunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintFormulatranheadunkid.ToString)

            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            mdtFormula.Rows.Clear()
            Dim dRow As DataRow

            For Each dtRow As DataRow In dsList.Tables(0).Rows
                dRow = mdtFormula.NewRow
                'minttranheadformulatranunkid = CInt(dtRow.Item("tranheadformulatranunkid"))
                dRow.Item("formulatranheadunkid") = CInt(dtRow.Item("formulatranheadunkid"))
                dRow.Item("tranheadunkid") = CInt(dtRow.Item("tranheadunkid"))
                'Sohail (15 Feb 2012) -- Start
                'TRA - ENHANCEMENT
                mblnIsvoid = CBool(dtRow.Item("isvoid"))
                mintVoiduserunkid = CInt(dtRow.Item("voiduserunkid"))
                If IsDBNull(dtRow.Item("voiddatetime")) = True Then
                    mdtVoiddatetime = Nothing
                Else
                    mdtVoiddatetime = dtRow.Item("voiddatetime").ToString
                End If
                mstrVoidreason = dtRow.Item("voidreason").ToString
                'Sohail (15 Feb 2012) -- End
                dRow.Item("defaultvalue_id") = CInt(dtRow.Item("defaultvalue_id")) 'Sohail (16 May 2012)
                dRow.Item("periodunkid") = CInt(dtRow.Item("periodunkid")) 'Sohail (12 Apr 2013)
                dRow.Item("tranheadslabtranunkid") = CInt(dtRow.Item("tranheadslabtranunkid")) 'Sohail (24 Sep 2013)

                dRow.Item("AUD") = dtRow.Item("AUD").ToString
                mdtFormula.Rows.Add(dRow)
            Next

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: GetData; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            'Sohail (28 Jan 2019) -- Start
            'NMB Enhancement - 76.1 - Option "Prorate Flat Rate head" on Transaction Head for Recurrent Head.
            'objDataOperation = Nothing
            If xDataOpr Is Nothing Then objDataOperation = Nothing
            'Sohail (28 Jan 2019) -- End
        End Try
    End Sub

    ''' <summary>
    ''' Modify By: Suhail
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>
    Public Function GetList(ByVal intTranHeadUnkID As Integer, Optional ByVal strTableName As String = "List") As DataSet
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        'Sohail (28 Jan 2019) -- Start
        'NMB Enhancement - 76.1 - Option "Prorate Flat Rate head" on Transaction Head for Recurrent Head.
        'objDataOperation = New clsDataOperation
        If xDataOpr Is Nothing Then
            objDataOperation = New clsDataOperation
        Else
            objDataOperation = xDataOpr
        End If
        objDataOperation.ClearParameters()
        'Sohail (28 Jan 2019) -- End

        Try
            strQ = "SELECT " & _
              "  tranheadformulatranunkid " & _
              ", formulatranheadunkid " & _
              ", tranheadunkid " & _
              ", ISNULL(isvoid, 0) AS isvoid " & _
              ", ISNULL(voiduserunkid, -1) AS voiduserunkid " & _
              ", voiddatetime " & _
              ", ISNULL(voidreason, '') AS voidreason " & _
              ", ISNULL(defaultvalue_id, 0) AS defaultvalue_id " & _
              ", ISNULL(prtranhead_formula_tran.periodunkid, 0) AS periodunkid " & _
              ", ISNULL(cfcommon_period_tran.period_name, '') AS period_name " & _
              ", ISNULL(prtranhead_formula_tran.tranheadslabtranunkid, 0) AS tranheadslabtranunkid " & _
             "FROM prtranhead_formula_tran " & _
             "LEFT JOIN cfcommon_period_tran ON cfcommon_period_tran.periodunkid = prtranhead_formula_tran.periodunkid " & _
             "AND cfcommon_period_tran.modulerefid = " & enModuleReference.Payroll & " " & _
             "WHERE formulatranheadunkid = @formulatranheadunkid " & _
             " AND ISNULL(isvoid, 0) = 0 " 'Sohail (15 Feb 2012) - [isvoid], 'Sohail (16 May 2012) - [defaultvalue_id]
            'Sohail (24 Sep 2013) - [tranheadslabtranunkid]
            'Sohail (12 Apr 2013) - [periodunkid, period_name]

            objDataOperation.AddParameter("@formulatranheadunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intTranHeadUnkID.ToString)

            dsList = objDataOperation.ExecQuery(strQ, strTableName)

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: GetList; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            'Sohail (28 Jan 2019) -- Start
            'NMB Enhancement - 76.1 - Option "Prorate Flat Rate head" on Transaction Head for Recurrent Head.
            'objDataOperation = Nothing
            If xDataOpr Is Nothing Then objDataOperation = Nothing
            'Sohail (28 Jan 2019) -- End
        End Try
        Return dsList
    End Function

    Public Function InsertDelete(ByVal mintOldTranheadslabtranunkid As Integer, ByVal mintNewTranheadslabtranunkid As Integer, ByVal intUserunkid As Integer, ByVal dtCurrentDateAndTime As Date) As Boolean
        'Sohail (21 Aug 2015) - [intUserunkid, dtCurrentDateAndTime]

        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        'Sohail (28 Jan 2019) -- Start
        'NMB Enhancement - 76.1 - Option "Prorate Flat Rate head" on Transaction Head for Recurrent Head.
        'objDataOperation = New clsDataOperation
        If xDataOpr Is Nothing Then
            objDataOperation = New clsDataOperation
        Else
            objDataOperation = xDataOpr
        End If
        objDataOperation.ClearParameters()
        'Sohail (28 Jan 2019) -- End

        Try
            For i = 0 To mdtFormula.Rows.Count - 1
                With mdtFormula.Rows(i)
                    objDataOperation.ClearParameters()
                    If Not IsDBNull(.Item("AUD")) Then

                        'Sohail (24 Sep 2013) -- Start
                        'TRA - ENHANCEMENT
                        If CInt(.Item("tranheadslabtranunkid")) <> mintOldTranheadslabtranunkid Then Continue For
                        'Sohail (24 Sep 2013) -- End

                        Select Case .Item("AUD")
                            Case "A"

                                strQ = "INSERT INTO prtranhead_formula_tran ( " & _
                                    "  formulatranheadunkid " & _
                                    ", tranheadunkid" & _
                                    ", isvoid " & _
                                    ", voiduserunkid " & _
                                    ", voiddatetime " & _
                                    ", voidreason " & _
                                    ", defaultvalue_id " & _
                                    ", periodunkid " & _
                                    ", tranheadslabtranunkid " & _
                                ") VALUES (" & _
                                    "  @formulatranheadunkid " & _
                                    ", @tranheadunkid" & _
                                    ", @isvoid " & _
                                    ", @voiduserunkid " & _
                                    ", @voiddatetime " & _
                                    ", @voidreason " & _
                                    ", @defaultvalue_id " & _
                                    ", @periodunkid " & _
                                    ", @tranheadslabtranunkid " & _
                                "); SELECT @@identity" 'Sohail (16 May 2012) - [defaultvalue_id]
                                'Sohail (24 Sep 2013) - [tranheadslabtranunkid]
                                'Sohail (12 Apr 2013) - [periodunkid]

                                objDataOperation.AddParameter("@formulatranheadunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintFormulatranheadunkid.ToString)
                                objDataOperation.AddParameter("@tranheadunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, .Item("tranheadunkid").ToString)
                                'Sohail (15 Feb 2012) -- Start
                                'TRA - ENHANCEMENT
                                objDataOperation.AddParameter("@isvoid", SqlDbType.Bit, eZeeDataType.BIT_SIZE, CBool(.Item("isvoid")))
                                objDataOperation.AddParameter("@voiduserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, .Item("voiduserunkid").ToString)
                                If .Item("voiddatetime").ToString = Nothing Then
                                    objDataOperation.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, DBNull.Value)
                                Else
                                    objDataOperation.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, .Item("voiddatetime").ToString)
                                End If
                                objDataOperation.AddParameter("@voidreason", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, .Item("voidreason").ToString)
                                'Sohail (15 Feb 2012) -- End
                                objDataOperation.AddParameter("@defaultvalue_id", SqlDbType.Int, eZeeDataType.INT_SIZE, .Item("defaultvalue_id").ToString) 'Sohail (16 May 2012)
                                objDataOperation.AddParameter("@periodunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, .Item("periodunkid").ToString) 'Sohail (12 Apr 2013)
                                objDataOperation.AddParameter("@tranheadslabtranunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintNewTranheadslabtranunkid) 'Sohail (24 Sep 2013)

                                dsList = objDataOperation.ExecQuery(strQ, "List")

                                If objDataOperation.ErrorMessage <> "" Then
                                    exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                                    Throw exForce
                                End If

                                'Sohail (12 Oct 2011) -- Start
                                mintTranheadformulatranunkid = dsList.Tables(0).Rows(0)(0)
                                If .Item("formulatranheadunkid") > 0 Then
                                    'S.SANDEEP [28-May-2018] -- START
                                    'ISSUE/ENHANCEMENT : {Audit Trails} 
                                    Dim objCommonATLog As New clsCommonATLog
                                    objCommonATLog._FormName = mstrFormName
                                    objCommonATLog._LoginEmployeeUnkid = mintLoginEmployeeunkid
                                    objCommonATLog._ClientIP = mstrClientIP
                                    objCommonATLog._HostName = mstrHostName
                                    objCommonATLog._FromWeb = mblnIsWeb
                                    objCommonATLog._AuditUserId = mintAuditUserId
                                    objCommonATLog._CompanyUnkid = mintCompanyUnkid
                                    objCommonATLog._AuditDate = mdtAuditDate
                                    'S.SANDEEP [28-May-2018] -- END

                                    If objCommonATLog.Insert_TranAtLog(objDataOperation, "prtranhead_master", "tranheadunkid", mintFormulatranheadunkid, "prtranhead_formula_tran", "tranheadformulatranunkid", mintTranheadformulatranunkid, 2, 1) = False Then
                                        Return False
                                    End If

                                    'S.SANDEEP [28-May-2018] -- START
                                    'ISSUE/ENHANCEMENT : {Audit Trails} 
                                    objCommonATLog = Nothing
                                    'S.SANDEEP [28-May-2018] -- END

                                Else
                                    'S.SANDEEP [28-May-2018] -- START
                                    'ISSUE/ENHANCEMENT : {Audit Trails} 
                                    Dim objCommonATLog As New clsCommonATLog
                                    objCommonATLog._FormName = mstrFormName
                                    objCommonATLog._LoginEmployeeUnkid = mintLoginEmployeeunkid
                                    objCommonATLog._ClientIP = mstrClientIP
                                    objCommonATLog._HostName = mstrHostName
                                    objCommonATLog._FromWeb = mblnIsWeb
                                    objCommonATLog._AuditUserId = mintAuditUserId
                                    objCommonATLog._CompanyUnkid = mintCompanyUnkid
                                    objCommonATLog._AuditDate = mdtAuditDate
                                    'S.SANDEEP [28-May-2018] -- END

                                    If objCommonATLog.Insert_TranAtLog(objDataOperation, "prtranhead_master", "tranheadunkid", mintFormulatranheadunkid, "prtranhead_formula_tran", "tranheadformulatranunkid", mintTranheadformulatranunkid, 1, 1) = False Then
                                        Return False
                                    End If

                                    'S.SANDEEP [28-May-2018] -- START
                                    'ISSUE/ENHANCEMENT : {Audit Trails} 
                                    objCommonATLog = Nothing
                                    'S.SANDEEP [28-May-2018] -- END

                                End If

                            Case "D"

                                'Sohail (12 Oct 2011) -- Start
                                If .Item("formulatranheadunkid") > 0 Then
                                    'S.SANDEEP [28-May-2018] -- START
                                    'ISSUE/ENHANCEMENT : {Audit Trails} 
                                    Dim objCommonATLog As New clsCommonATLog
                                    objCommonATLog._FormName = mstrFormName
                                    objCommonATLog._LoginEmployeeUnkid = mintLoginEmployeeunkid
                                    objCommonATLog._ClientIP = mstrClientIP
                                    objCommonATLog._HostName = mstrHostName
                                    objCommonATLog._FromWeb = mblnIsWeb
                                    objCommonATLog._AuditUserId = mintAuditUserId
                                    objCommonATLog._CompanyUnkid = mintCompanyUnkid
                                    objCommonATLog._AuditDate = mdtAuditDate
                                    'S.SANDEEP [28-May-2018] -- END

                                    If objCommonATLog.VoidAtTranAtLog(objDataOperation, "prtranhead_master", "tranheadunkid", mintFormulatranheadunkid, "prtranhead_formula_tran", "tranheadformulatranunkid", 2, 3, "formulatranheadunkid", "tranheadunkid = " & .Item("tranheadunkid") & " AND tranheadslabtranunkid = " & .Item("tranheadslabtranunkid") & "") = False Then
                                        Return False
                                    End If

                                    'S.SANDEEP [28-May-2018] -- START
                                    'ISSUE/ENHANCEMENT : {Audit Trails} 
                                    objCommonATLog = Nothing
                                    'S.SANDEEP [28-May-2018] -- END



                                    'Sohail (15 Feb 2012) -- Start
                                    'TRA - ENHANCEMENT
                                    'strQ = "DELETE FROM prtranhead_formula_tran " & _
                                    '    "WHERE formulatranheadunkid = @formulatranheadunkid "
                                    'Sohail (22 Feb 2012) -- Start
                                    'TRA - ENHANCEMENT
                                    'strQ = "UPDATE prtranhead_formula_tran SET " & _
                                    '        " isvoid = 1 " & _
                                    '        ", voiduserunkid = @voiduserunkid " & _
                                    '        ", voiddatetime = @voiddatetime " & _
                                    '        ", voidreason = @voidreason " & _
                                    '    "WHERE formulatranheadunkid = @formulatranheadunkid "
                                    strQ = "UPDATE prtranhead_formula_tran SET " & _
                                            " isvoid = 1 " & _
                                            ", voiduserunkid = @voiduserunkid " & _
                                            ", voiddatetime = @voiddatetime " & _
                                            ", voidreason = @voidreason " & _
                                        "WHERE formulatranheadunkid = @formulatranheadunkid " & _
                                        "AND tranheadunkid = @tranheadunkid " & _
                                        "AND tranheadslabtranunkid = @tranheadslabtranunkid "
                                    'Sohail (24 Sep 2013) - [tranheadslabtranunkid]
                                    'Sohail (22 Feb 2012) -- End
                                    'Sohail (15 Feb 2012) -- End

                                    objDataOperation.AddParameter("@formulatranheadunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintFormulatranheadunkid.ToString)
                                    objDataOperation.AddParameter("@tranheadunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, .Item("tranheadunkid").ToString) 'Sohail (22 Feb 2012)
                                    objDataOperation.AddParameter("@tranheadslabtranunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, .Item("tranheadslabtranunkid").ToString) 'Sohail (24 Sep 2013)
                                    'Sohail (15 Feb 2012) -- Start
                                    'TRA - ENHANCEMENT
                                    'Sohail (21 Aug 2015) -- Start
                                    'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
                                    'objDataOperation.AddParameter("@voiduserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, User._Object._Userunkid)
                                    'objDataOperation.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, ConfigParameter._Object._CurrentDateAndTime)
                                    objDataOperation.AddParameter("@voiduserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intUserunkid)
                                    objDataOperation.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, dtCurrentDateAndTime)
                                    'Sohail (21 Aug 2015) -- End
                                    objDataOperation.AddParameter("@voidreason", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, .Item("voidreason").ToString)
                                    'Sohail (15 Feb 2012) -- End

                                    dsList = objDataOperation.ExecQuery(strQ, "List")

                                    If objDataOperation.ErrorMessage <> "" Then
                                        exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                                        Throw exForce
                                    End If
                                End If
                                'Sohail (12 Oct 2011) -- End

                        End Select
                    End If
                End With
            Next

            Return True
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: InsertDelete; Module Name: " & mstrModuleName)
            Return False
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            'Sohail (28 Jan 2019) -- Start
            'NMB Enhancement - 76.1 - Option "Prorate Flat Rate head" on Transaction Head for Recurrent Head.
            'objDataOperation = Nothing
            If xDataOpr Is Nothing Then objDataOperation = Nothing
            'Sohail (28 Jan 2019) -- End
        End Try
    End Function

    'Sohail (22 Feb 2012) -- Start
    'TRA - ENHANCEMENT
    Public Function VoidByTranHeadUnkID(ByVal intTranHeadUnkid As Integer, _
                                        ByVal intVoidUserID As Integer, ByVal dtVoidDateTime As DateTime, ByVal strVoidReason As String) As Boolean

        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        'Sohail (28 Jan 2019) -- Start
        'NMB Enhancement - 76.1 - Option "Prorate Flat Rate head" on Transaction Head for Recurrent Head.
        'objDataOperation = New clsDataOperation
        If xDataOpr Is Nothing Then
            objDataOperation = New clsDataOperation
        Else
            objDataOperation = xDataOpr
        End If
        objDataOperation.ClearParameters()
        'Sohail (28 Jan 2019) -- End

        Try
            'S.SANDEEP [28-May-2018] -- START
            'ISSUE/ENHANCEMENT : {Audit Trails} 
            Dim objCommonATLog As New clsCommonATLog
            objCommonATLog._FormName = mstrFormName
            objCommonATLog._LoginEmployeeUnkid = mintLoginEmployeeunkid
            objCommonATLog._ClientIP = mstrClientIP
            objCommonATLog._HostName = mstrHostName
            objCommonATLog._FromWeb = mblnIsWeb
            objCommonATLog._AuditUserId = mintAuditUserId
            objCommonATLog._CompanyUnkid = mintCompanyUnkid
            objCommonATLog._AuditDate = mdtAuditDate
            'S.SANDEEP [28-May-2018] -- END

            If objCommonATLog.VoidAtTranAtLog(objDataOperation, "prtranhead_master", "tranheadunkid", intTranHeadUnkid, "prtranhead_formula_tran", "tranheadformulatranunkid", 3, 3, "formulatranheadunkid") = False Then
                Return False
            End If

            'S.SANDEEP [28-May-2018] -- START
            'ISSUE/ENHANCEMENT : {Audit Trails} 
            objCommonATLog = Nothing
            'S.SANDEEP [28-May-2018] -- END


            strQ = "UPDATE prtranhead_formula_tran SET " & _
                        " isvoid = 1 " & _
                        ", voiduserunkid = @voiduserunkid " & _
                        ", voiddatetime = @voiddatetime " & _
                        ", voidreason = @voidreason " & _
                    "WHERE formulatranheadunkid = @formulatranheadunkid "

            objDataOperation.AddParameter("@formulatranheadunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intTranHeadUnkid)
            objDataOperation.AddParameter("@voiduserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intVoidUserID)
            objDataOperation.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, dtVoidDateTime)
            objDataOperation.AddParameter("@voidreason", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, strVoidReason)

            Call objDataOperation.ExecNonQuery(strQ)

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            Return True
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: VoidByTranHeadUnkID; Module Name: " & mstrModuleName)
            Return False
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            'Sohail (28 Jan 2019) -- Start
            'NMB Enhancement - 76.1 - Option "Prorate Flat Rate head" on Transaction Head for Recurrent Head.
            'objDataOperation = Nothing
            If xDataOpr Is Nothing Then objDataOperation = Nothing
            'Sohail (28 Jan 2019) -- End
        End Try
    End Function
    'Sohail (22 Feb 2012) -- End

    'Sohail (10 Jul 2013) -- Start
    'TRA - ENHANCEMENT
    Public Function GetCurrentSlabForProcessPayroll(ByVal strList As String, ByVal intTranHeadUnkID As Integer, ByVal dtPeriodEndDate As DateTime, Optional ByVal intSlabTranUnkId As Integer = -1, Optional ByVal xDataOp As clsDataOperation = Nothing) As DataSet
        'Sohail (03 May 2018) - [xDataOp]
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        'Sohail (03 May 2018) -- Start
        'Enhancement : Removing global objects and passing objDataOperation in process payroll in 72.1.
        'objDataOperation = New clsDataOperation
        If xDataOp Is Nothing Then
            objDataOperation = New clsDataOperation
        Else
            objDataOperation = xDataOp
        End If
        objDataOperation.ClearParameters()
        'Sohail (03 May 2018) -- End

        Try
            'Sohail (26 Aug 2016) -- Start
            'Enhancement - 57.4 & 63.1 - Process Payroll Performance enhancement for TRA.
            'strQ = "SELECT " & _
            '  " tranheadunkid " & _
            '  ", ISNULL(defaultvalue_id, 0) AS defaultvalue_id " & _
            '  ", ISNULL(tranheadslabtranunkid, 0) AS tranheadslabtranunkid " & _
            ' "FROM prtranhead_formula_tran " & _
            ' "LEFT JOIN cfcommon_period_tran ON cfcommon_period_tran.periodunkid = prtranhead_formula_tran.periodunkid " & _
            ' "AND cfcommon_period_tran.modulerefid = " & enModuleReference.Payroll & " " & _
            ' "WHERE formulatranheadunkid = @formulatranheadunkid " & _
            ' " AND ISNULL(isvoid, 0) = 0 " & _
            ' "AND cfcommon_period_tran.end_date = ( SELECT    MAX(end_date) " & _
            '                                      "FROM      prtranhead_formula_tran " & _
            '                                                "LEFT JOIN cfcommon_period_tran ON prtranhead_formula_tran.periodunkid = cfcommon_period_tran.periodunkid " & _
            '                                      "WHERE     ISNULL(prtranhead_formula_tran.isvoid, 0) = 0 " & _
            '                                                "AND prtranhead_formula_tran.formulatranheadunkid = @formulatranheadunkid " & _
            '                                                "AND CONVERT(CHAR(8), cfcommon_period_tran.end_date,112) <= @end_date " & _
            '                                    ") "
            strQ = "SELECT  tranheadunkid  " & _
                          ", defaultvalue_id " & _
                          ", tranheadslabtranunkid " & _
                          ", formulatranheadunkid " & _
                          ", calctype_id_head_in_formula " & _
                    "FROM    ( SELECT    prtranhead_formula_tran.tranheadunkid  " & _
              ", ISNULL(defaultvalue_id, 0) AS defaultvalue_id " & _
              ", ISNULL(tranheadslabtranunkid, 0) AS tranheadslabtranunkid " & _
                                      ", formulatranheadunkid " & _
                                        ", ISNULL(prtranhead_master.calctype_id, 0) AS calctype_id_head_in_formula " & _
                                      ", DENSE_RANK() OVER ( PARTITION BY prtranhead_formula_tran.formulatranheadunkid ORDER BY cfcommon_period_tran.end_date DESC ) AS ROWNO " & _
             "FROM prtranhead_formula_tran " & _
             "LEFT JOIN cfcommon_period_tran ON cfcommon_period_tran.periodunkid = prtranhead_formula_tran.periodunkid " & _
             "LEFT JOIN prtranhead_master ON prtranhead_formula_tran.tranheadunkid = prtranhead_master.tranheadunkid " & _
             "AND cfcommon_period_tran.modulerefid = " & enModuleReference.Payroll & " " & _
                              "WHERE     prtranhead_formula_tran.isvoid = 0 " & _
                                        "AND CONVERT(CHAR(8), cfcommon_period_tran.end_date, 112) <= @end_date "
            'Sohail (25 Sep 2021) - [calctype_id, LEFT JOIN prtranhead_master]

            If intTranHeadUnkID > 0 Then
                strQ &= "AND formulatranheadunkid = @formulatranheadunkid "
            End If

            strQ &= "   ) AS A " & _
                    "WHERE   A.ROWNO = 1 "
            'Sohail (26 Aug 2016) -- End
            'Sohail (24 Sep 2013) - [tranheadslabtranunkid]

            'Sohail (24 Sep 2013) -- Start
            'TRA - ENHANCEMENT
            If intSlabTranUnkId >= 0 Then
                strQ &= " AND ISNULL(tranheadslabtranunkid, 0) = @tranheadslabtranunkid "
                objDataOperation.AddParameter("@tranheadslabtranunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intSlabTranUnkId)
            End If
            'Sohail (24 Sep 2013) -- End

            objDataOperation.AddParameter("@formulatranheadunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intTranHeadUnkID)
            objDataOperation.AddParameter("@end_date", SqlDbType.VarChar, eZeeDataType.NAME_SIZE, eZeeDate.convertDate(dtPeriodEndDate))

            dsList = objDataOperation.ExecQuery(strQ, strList)

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: GetCurrentSlabForProcessPayroll; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            'Sohail (03 May 2018) -- Start
            'Enhancement : Removing global objects and passing objDataOperation in process payroll in 72.1.
            'objDataOperation = Nothing
            If xDataOp Is Nothing Then objDataOperation = Nothing
            'Sohail (03 May 2018) -- End
        End Try
        Return dsList
    End Function
    'Sohail (10 Jul 2013) -- End

    ''' <summary>
    ''' Return the Dataset of Flate Rate Transaction Heads UnkID which is used in the formula of specified transaction head.
    ''' Modify By : Sohail
    ''' </summary>
    ''' <param name="intTranHeadUnkID"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Function getFlateRateHeadFromFormula(ByVal intTranHeadUnkID As Integer, Optional ByVal strListName As String = "List") As DataSet
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        'Sohail (28 Jan 2019) -- Start
        'NMB Enhancement - 76.1 - Option "Prorate Flat Rate head" on Transaction Head for Recurrent Head.
        'objDataOperation = New clsDataOperation
        If xDataOpr Is Nothing Then
            objDataOperation = New clsDataOperation
        Else
            objDataOperation = xDataOpr
        End If
        objDataOperation.ClearParameters()
        'Sohail (28 Jan 2019) -- End

        Try
            strQ = "SELECT DISTINCT prtranhead_master.tranheadunkid " & _
                                ", ISNULL(prtranhead_formula_tran.defaultvalue_id, 0 ) AS defaultvalue_id " & _
                    "FROM prtranhead_formula_tran " & _
                    "INNER JOIN prtranhead_master " & _
                    "ON prtranhead_formula_tran.tranheadunkid = prtranhead_master.tranheadunkid " & _
                    "WHERE prtranhead_formula_tran.formulatranheadunkid = @tranheadunkid " & _
                    "AND ISNULL(prtranhead_formula_tran.isvoid, 0) = 0 " & _
                    "AND prtranhead_master.calctype_id IN (" & enCalcType.FlatRate_Others & ") " 'Flate Rate Salary, Flate Rate All, 'Sohail (16 May 2012) - [defaultvalue_id]

            objDataOperation.AddParameter("@tranheadunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intTranHeadUnkID.ToString)

            dsList = objDataOperation.ExecQuery(strQ, strListName)

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            Return dsList
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: getFlateRateHeadFromFormula; Module Name: " & mstrModuleName)
            Return dsList
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            'Sohail (28 Jan 2019) -- Start
            'NMB Enhancement - 76.1 - Option "Prorate Flat Rate head" on Transaction Head for Recurrent Head.
            'objDataOperation = Nothing
            If xDataOpr Is Nothing Then objDataOperation = Nothing
            'Sohail (28 Jan 2019) -- End
        End Try

    End Function

    'Sohail (03 Sep 2012) -- Start
    'TRA - ENHANCEMENT
    Public Function IsFormulaHeadRecursive(ByVal strDatabaseName As String, _
                                           ByVal intTranHeadUnkId As Integer, _
                                           ByVal intHeadTypeID As Integer, _
                                           ByVal intTypeOfID As Integer, _
                                           ByVal intFormulaTranHeadUnkId As Integer, _
                                           ByRef strMessageHeadName As String, _
                                           ByVal dtPeriodEnd As Date _
                                           ) As Boolean
        'Sohail (13 Jul 2018) - [dtPeriodEnd]
        'Sohail (21 Aug 2015) - [strDatabaseName]
        '                                  'Sohail (21 Sep 2012) - [intHeadTypeID, strMessageHeadName]

        Dim objDataOperation As clsDataOperation

        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        'Sohail (28 Jan 2019) -- Start
        'NMB Enhancement - 76.1 - Option "Prorate Flat Rate head" on Transaction Head for Recurrent Head.
        'objDataOperation = New clsDataOperation
        If xDataOpr Is Nothing Then
            objDataOperation = New clsDataOperation
        Else
            objDataOperation = xDataOpr
        End If
        objDataOperation.ClearParameters()
        'Sohail (28 Jan 2019) -- End

        Try

            'Sohail (21 Sep 2012) -- Start
            'TRA - ENHANCEMENT
            'If intTranHeadUnkId <= 0 AndAlso intTypeOfID <> enTypeOf.Salary Then Return False
            If intTranHeadUnkId <= 0 AndAlso intTypeOfID <> enTypeOf.Salary AndAlso intHeadTypeID <> enTranHeadType.EarningForEmployees AndAlso intHeadTypeID <> enTranHeadType.DeductionForEmployee AndAlso intHeadTypeID <> enTranHeadType.EmployeesStatutoryDeductions Then Return False
            'Sohail (21 Sep 2012) -- End

            If intTypeOfID = enTypeOf.Salary Then
                Dim objTranHead As New clsTransactionHead
                'Sohail (28 Jan 2019) -- Start
                'NMB Enhancement - 76.1 - Option "Prorate Flat Rate head" on Transaction Head for Recurrent Head.
                objTranHead._xDataOperation = objDataOperation
                'Sohail (28 Jan 2019) -- End
                'Sohail (21 Aug 2015) -- Start
                'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
                'objTranHead._Tranheadunkid = intFormulaTranHeadUnkId
                objTranHead._Tranheadunkid(strDatabaseName) = intFormulaTranHeadUnkId
                'Sohail (21 Aug 2015) -- End

                If objTranHead._Formulaid.Contains("#BSL#") = True Then
                    strMessageHeadName = " Function 'BASIC SALARY' "
                    objTranHead = Nothing
                    Return True
                    'Sohail (21 Sep 2012) -- Start
                    'TRA - ENHANCEMENT
                ElseIf objTranHead._Formulaid.Contains("#DBS#") = True Then
                    strMessageHeadName = " Function 'DAILY BASIC SALARY WEEKENDS EXCLUDED' "
                    objTranHead = Nothing
                    Return True
                ElseIf objTranHead._Formulaid.Contains("#HBS#") = True Then
                    strMessageHeadName = " Function 'HOURLY BASIC SALARY WEEKENDS EXCLUDED' "
                    objTranHead = Nothing
                    Return True
                ElseIf objTranHead._Formulaid.Contains("#DWI#") = True Then
                    strMessageHeadName = " Function 'DAILY BASIC SALARY WEEKENDS INCLUDED' "
                    objTranHead = Nothing
                    Return True
                ElseIf objTranHead._Formulaid.Contains("#HWI#") = True Then
                    strMessageHeadName = " Function 'HOURLY BASIC SALARY WEEKENDS INCLUDED' "
                    objTranHead = Nothing
                    Return True
                    'Sohail (21 Sep 2012) -- End
                Else
                    objTranHead = Nothing
                End If
            End If

            'Sohail (13 Jul 2018) -- Start
            'RESD Zanzibar Issue - Support Issue Id # 0002382 : Recursive Error during creation of Taxable income Transaction in 72.1.
            'strQ = "SELECT  prtranhead_formula_tran.tranheadunkid " & _
            '              ", prtranhead_master.trnheadname " & _
            '              ", prtranhead_master.typeof_id " & _
            '              ", prtranhead_master.calctype_id " & _
            '        "FROM    prtranhead_formula_tran " & _
            '                "JOIN prtranhead_master ON prtranhead_master.tranheadunkid = prtranhead_formula_tran.tranheadunkid " & _
            '        "WHERE   ISNULL(prtranhead_formula_tran.isvoid, 0) = 0 " & _
            '                "AND formulatranheadunkid = @formulatranheadunkid "
            '               'Sohail (21 Sep 2012) - [trnheadname, calctype_id]
            strQ = "SELECT A.tranheadunkid " & _
                         ", A.trnheadname " & _
                         ", A.typeof_id " & _
                         ", A.calctype_id " & _
                         ", A.periodunkid " & _
                         ", A.MainHeadName " & _
                    "FROM " & _
                    "( " & _
                        "SELECT prtranhead_formula_tran.tranheadunkid " & _
                          ", prtranhead_master.trnheadname " & _
                          ", prtranhead_master.typeof_id " & _
                          ", prtranhead_master.calctype_id " & _
                             ", prtranhead_formula_tran.periodunkid " & _
                             ", MainHead.trnheadname AS MainHeadName " & _
                             ", DENSE_RANK() OVER (PARTITION BY prtranhead_formula_tran.formulatranheadunkid ORDER BY CONVERT(CHAR(8), cfcommon_period_tran.end_date, 112) DESC ) AS ROWNO " & _
                        "FROM prtranhead_formula_tran " & _
                            "LEFT JOIN prtranhead_master ON prtranhead_master.tranheadunkid = prtranhead_formula_tran.tranheadunkid " & _
                            "LEFT JOIN prtranhead_master AS MainHead ON MainHead.tranheadunkid = prtranhead_formula_tran.formulatranheadunkid " & _
                            "LEFT JOIN cfcommon_period_tran ON cfcommon_period_tran.periodunkid = prtranhead_formula_tran.periodunkid " & _
                        "WHERE prtranhead_formula_tran.isvoid = 0 " & _
                              "AND prtranhead_master.isvoid = 0 " & _
                              "AND cfcommon_period_tran.isactive = 1 " & _
                              "AND cfcommon_period_tran.modulerefid = " & CInt(enModuleReference.Payroll) & " " & _
                              "AND CONVERT(CHAR(8), cfcommon_period_tran.end_date, 112) <= @end_date " & _
                              "AND formulatranheadunkid = @formulatranheadunkid " & _
                    ") AS A " & _
                    "WHERE A.ROWNO = 1 "
            'Sohail (07 May 2021) - [MainHead]
            'Sohail (06 May 2019) - [DENSE_RANK() OVER (PARTITION BY prtranhead_formula_tran.formulatranheadunkid ORDER BY CONVERT(CHAR(8), cfcommon_period_tran.end_date, 112) DESC ) AS ROWNO] = [DENSE_RANK() OVER (PARTITION BY prtranhead_formula_tran.formulatranheadunkid ORDER BY CONVERT(CHAR(8), cfcommon_period_tran.end_date, 112) DESC ) AS ROWNO]
            'Sohail (13 Jul 2018) -- End

            objDataOperation.AddParameter("@formulatranheadunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intFormulaTranHeadUnkId)
            objDataOperation.AddParameter("@end_date", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, eZeeDate.convertDate(dtPeriodEnd)) 'Sohail (13 Jul 2018)

            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            If dsList.Tables("List").Rows.Count <= 0 Then
                Return False
            Else

                For Each dsRow As DataRow In dsList.Tables("List").Rows

                    'Sohail (21 Sep 2012) -- Start
                    'TRA - ENHANCEMENT
                    'If intTranHeadUnkId = CInt(dsRow.Item("tranheadunkid")) Then Return True

                    'If intTypeOfID = enTypeOf.Salary AndAlso CInt(dsRow.Item("typeof_id")) = enTypeOf.Salary Then Return True
                    If intTranHeadUnkId = CInt(dsRow.Item("tranheadunkid")) Then
                        strMessageHeadName = " Transaction Head '" & dsRow.Item("trnheadname").ToString & "' Itself "
                        strMessageHeadName &= " " & " Which is used in " & dsRow.Item("MainHeadName").ToString 'Sohail (07 May 2021)
                        Return True
                    End If

                    If intTypeOfID = enTypeOf.Salary AndAlso CInt(dsRow.Item("typeof_id")) = enTypeOf.Salary Then
                        strMessageHeadName = " Salary Head '" & dsRow.Item("trnheadname").ToString & "' "
                        strMessageHeadName &= " " & " Which is used in " & dsRow.Item("MainHeadName").ToString 'Sohail (07 May 2021)
                        Return True
                    End If

                    If intHeadTypeID = enTranHeadType.EarningForEmployees AndAlso (CInt(dsRow.Item("calctype_id")) = enCalcType.NET_PAY OrElse CInt(dsRow.Item("calctype_id")) = enCalcType.TOTAL_EARNING OrElse CInt(dsRow.Item("calctype_id")) = enCalcType.TAXABLE_EARNING_TOTAL OrElse CInt(dsRow.Item("calctype_id")) = enCalcType.NON_TAXABLE_EARNING_TOTAL) Then
                        strMessageHeadName = " Transaction Head '" & dsRow.Item("trnheadname").ToString & "' calculation Type of NET PAY or TOTAL EARNING "
                        strMessageHeadName &= " " & " Which is used in " & dsRow.Item("MainHeadName").ToString 'Sohail (07 May 2021)
                        Return True
                    End If

                    If intHeadTypeID = enTranHeadType.DeductionForEmployee AndAlso (CInt(dsRow.Item("calctype_id")) = enCalcType.NET_PAY OrElse CInt(dsRow.Item("calctype_id")) = enCalcType.TOTAL_DEDUCTION) Then
                        strMessageHeadName = " Transaction Head '" & dsRow.Item("trnheadname").ToString & "' calculation Type of NET PAY or TOTAL DEDUCTION "
                        strMessageHeadName &= " " & " Which is used in " & dsRow.Item("MainHeadName").ToString 'Sohail (07 May 2021)
                        Return True
                    End If

                    If intHeadTypeID = enTranHeadType.EmployeesStatutoryDeductions AndAlso (CInt(dsRow.Item("calctype_id")) = enCalcType.NET_PAY OrElse CInt(dsRow.Item("calctype_id")) = enCalcType.TOTAL_DEDUCTION) Then
                        strMessageHeadName = " Transaction Head '" & dsRow.Item("trnheadname").ToString & "' calculation Type of NET PAY or TOTAL DEDUCTION "
                        strMessageHeadName &= " " & " Which is used in " & dsRow.Item("MainHeadName").ToString 'Sohail (07 May 2021)
                        Return True
                    End If

                    'If intHeadTypeID = enTranHeadType.EmployersStatutoryContributions AndAlso CInt(dsRow.Item("calctype_id")) = enCalcType.NET_PAY Then
                    '    strMessageHeadName = " Transaction Head '" & dsRow.Item("trnheadname").ToString & "' calculation Type of NET PAY "
                    '    Return True
                    'End If
                    'Sohail (21 Sep 2012) -- End

                    'Sohail (14 Jun 2013) -- Start
                    'TRA - ENHANCEMENT
                    If intHeadTypeID = enTranHeadType.Informational AndAlso (CInt(dsRow.Item("calctype_id")) = enCalcType.NON_CASH_BENEFIT_TOTAL) Then
                        strMessageHeadName = " Transaction Head '" & dsRow.Item("trnheadname").ToString & "' calculation Type of NON CASH BENEFIT TOTAL "
                        strMessageHeadName &= " " & " Which is used in " & dsRow.Item("MainHeadName").ToString 'Sohail (07 May 2021)
                        Return True
                    End If
                    'Sohail (14 Jun 2013) -- End

                    'Sohail (21 Aug 2015) -- Start
                    'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
                    'If IsFormulaHeadRecursive(intTranHeadUnkId, intHeadTypeID, intTypeOfID, CInt(dsRow.Item("tranheadunkid")), strMessageHeadName) = True Then
                    'Sohail (13 Jul 2018) -- Start
                    'RESD Zanzibar Issue - Support Issue Id # 0002382 : Recursive Error during creation of Taxable income Transaction in 72.1.
                    'If IsFormulaHeadRecursive(strDatabaseName, intTranHeadUnkId, intHeadTypeID, intTypeOfID, CInt(dsRow.Item("tranheadunkid")), strMessageHeadName) = True Then
                    If IsFormulaHeadRecursive(strDatabaseName, intTranHeadUnkId, intHeadTypeID, intTypeOfID, CInt(dsRow.Item("tranheadunkid")), strMessageHeadName, dtPeriodEnd) = True Then
                        'Sohail (13 Jul 2018) -- End
                        'Sohail (21 Aug 2015) -- End
                        Return True
                    End If

                Next

            End If


            Return False

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: IsFormulaHeadRecursive; Module Name: " & mstrModuleName)
            Return False
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            'Sohail (28 Jan 2019) -- Start
            'NMB Enhancement - 76.1 - Option "Prorate Flat Rate head" on Transaction Head for Recurrent Head.
            'objDataOperation = Nothing
            If xDataOpr Is Nothing Then objDataOperation = Nothing
            'Sohail (28 Jan 2019) -- End
        End Try
    End Function
    'Sohail (03 Sep 2012) -- End





    '''' <summary>
    '''' Modify By: Suhail
    '''' </summary>
    '''' <purpose> Assign all Property variable </purpose>
    'Public Function GetList(ByVal strTableName As String, Optional ByVal blnOnlyActive As Boolean = True) As DataSet
    '    Dim dsList As DataSet = Nothing
    '    Dim strQ As String = ""
    '    Dim exForce As Exception

    '    objDataOperation = New clsDataOperation

    '    Try
    '        StrQ = "SELECT " & _
    '          "  tranheadformulatranunkid " & _
    '          ", formulatranheadunkid " & _
    '          ", tranheadunkid " & _
    '         "FROM prtranhead_formula_tran "

    '        If blnOnlyActive Then
    '            strQ &= " WHERE isactive = 1 "
    '        End If

    '        dsList = objDataOperation.ExecQuery(strQ, strTableName)

    '        If objDataOperation.ErrorMessage <> "" Then
    '            exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
    '            Throw exForce
    '        End If

    '    Catch ex As Exception
    '        Throw New Exception(ex.Message & "; Procedure Name: GetList; Module Name: " & mstrModuleName)
    '    Finally
    '        exForce = Nothing
    '        If dsList IsNot Nothing Then dsList.Dispose()
    '        objDataOperation = Nothing
    '    End Try
    '    Return dsList
    'End Function

    '''' <summary>
    '''' Modify By: Suhail
    '''' </summary>
    '''' <returns>Boolean</returns>
    '''' <purpose> INSERT INTO Database Table (prtranhead_formula_tran) </purpose>
    'Public Function Insert() As Boolean
    '    'If isExist(mstrName) Then
    '    '    mstrMessage = "<Message>"
    '    '    Return False
    '    'End If

    '    Dim dsList As DataSet = Nothing
    '    Dim strQ As String = ""
    '    Dim exForce As Exception

    '    objDataOperation = New clsDataOperation

    '    Try
    '        objDataOperation.AddParameter("@formulatranheadunkid", SqlDbType.int, eZeeDataType.INT_SIZE, mintformulatranheadunkid.ToString)
    '        objDataOperation.AddParameter("@tranheadunkid", SqlDbType.int, eZeeDataType.INT_SIZE, minttranheadunkid.ToString)

    '        StrQ = "INSERT INTO prtranhead_formula_tran ( " & _
    '          "  formulatranheadunkid " & _
    '          ", tranheadunkid" & _
    '        ") VALUES (" & _
    '          "  @formulatranheadunkid " & _
    '          ", @tranheadunkid" & _
    '        "); SELECT @@identity"

    '        dsList = objDataOperation.ExecQuery(strQ, "List")

    '        If objDataOperation.ErrorMessage <> "" Then
    '            exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
    '            Throw exForce
    '        End If

    '        mintTranheadformulaTranUnkId = dsList.Tables(0).Rows(0).Item(0)

    '        Return True
    '    Catch ex As Exception
    '        Throw New Exception(ex.Message & "; Procedure Name: Insert; Module Name: " & mstrModuleName)
    '        Return False
    '    Finally
    '        exForce = Nothing
    '        If dsList IsNot Nothing Then dsList.Dispose()
    '        objDataOperation = Nothing
    '    End Try
    'End Function

    '''' <summary>
    '''' Modify By: Suhail
    '''' </summary>
    '''' <returns>Boolean</returns>
    '''' <purpose> Update Database Table (prtranhead_formula_tran) </purpose>
    'Public Function Update() As Boolean
    '    'If isExist(mstrName, mintTranheadformulatranunkid) Then
    '    '    mstrMessage = "<Message>"
    '    '    Return False
    '    'End If

    '    Dim dsList As DataSet = Nothing
    '    Dim strQ As String = ""
    '    Dim exForce As Exception

    '    objDataOperation = New clsDataOperation

    '    Try
    '        objDataOperation.AddParameter("@tranheadformulatranunkid", SqlDbType.int, eZeeDataType.INT_SIZE, minttranheadformulatranunkid.ToString)
    '        objDataOperation.AddParameter("@formulatranheadunkid", SqlDbType.int, eZeeDataType.INT_SIZE, mintformulatranheadunkid.ToString)
    '        objDataOperation.AddParameter("@tranheadunkid", SqlDbType.int, eZeeDataType.INT_SIZE, minttranheadunkid.ToString)

    '        StrQ = "UPDATE prtranhead_formula_tran SET " & _
    '          "  formulatranheadunkid = @formulatranheadunkid" & _
    '          ", tranheadunkid = @tranheadunkid " & _
    '        "WHERE tranheadformulatranunkid = @tranheadformulatranunkid "

    '        Call objDataOperation.ExecNonQuery(strQ)

    '        If objDataOperation.ErrorMessage <> "" Then
    '            exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
    '            Throw exForce
    '        End If

    '        Return True
    '    Catch ex As Exception
    '        Throw New Exception(ex.Message & "; Procedure Name: Update; Module Name: " & mstrModuleName)
    '        Return False
    '    Finally
    '        exForce = Nothing
    '        If dsList IsNot Nothing Then dsList.Dispose()
    '        objDataOperation = Nothing
    '    End Try
    'End Function

    '''' <summary>
    '''' Modify By: Suhail
    '''' </summary>
    '''' <returns>Boolean</returns>
    '''' <purpose> Delete Database Table (prtranhead_formula_tran) </purpose>
    'Public Function Delete(ByVal intUnkid As Integer) As Boolean
    '    If isUsed(intUnkid) Then
    '        mstrMessage = "<Message>"
    '        Return False
    '    End If

    '    Dim dsList As DataSet = Nothing
    '    Dim strQ As String = ""
    '    Dim exForce As Exception

    '    objDataOperation = New clsDataOperation

    '    Try
    '        StrQ = "DELETE FROM prtranhead_formula_tran " & _
    '        "WHERE tranheadformulatranunkid = @tranheadformulatranunkid "

    '        objDataOperation.AddParameter("@tranheadformulatranunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intUnkid)

    '        Call objDataOperation.ExecNonQuery(strQ)

    '        If objDataOperation.ErrorMessage <> "" Then
    '            exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
    '            Throw exForce
    '        End If

    '        Return True
    '    Catch ex As Exception
    '        Throw New Exception(ex.Message & "; Procedure Name: Update; Module Name: " & mstrModuleName)
    '        Return False
    '    Finally
    '        exForce = Nothing
    '        If dsList IsNot Nothing Then dsList.Dispose()
    '        objDataOperation = Nothing
    '    End Try
    'End Function

    '''' <summary>
    '''' Modify By: Suhail
    '''' </summary>
    '''' <purpose> Assign all Property variable </purpose>
    'Public Function isUsed(ByVal intUnkid As Integer) As Boolean
    '    Dim dsList As DataSet = Nothing
    '    Dim strQ As String = ""
    '    Dim exForce As Exception

    '    objDataOperation = New clsDataOperation

    '    Try
    '        StrQ = "<Query>"

    '        objDataOperation.AddParameter("@tranheadformulatranunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intUnkid)

    '        dsList = objDataOperation.ExecQuery(strQ, "List")

    '        If objDataOperation.ErrorMessage <> "" Then
    '            exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
    '            Throw exForce
    '        End If

    '        Return dsList.tables(0).rows.count > 0
    '    Catch ex As Exception
    '        Throw New Exception(ex.Message & "; Procedure Name: isUsed; Module Name: " & mstrModuleName)
    '    Finally
    '        exForce = Nothing
    '        If dsList IsNot Nothing Then dsList.Dispose()
    '        objDataOperation = Nothing
    '    End Try
    'End Function

    '''' <summary>
    '''' Modify By: Suhail
    '''' </summary>
    '''' <purpose> Assign all Property variable </purpose>
    'Public Function isExist(ByVal strCode As String, ByVal strName As String, Optional ByVal intUnkid As Integer = -1) As Boolean
    '    Dim dsList As DataSet = Nothing
    '    Dim strQ As String = ""
    '    Dim exForce As Exception

    '    objDataOperation = New clsDataOperation

    '    Try
    '        StrQ = "SELECT " & _
    '          "  tranheadformulatranunkid " & _
    '          ", formulatranheadunkid " & _
    '          ", tranheadunkid " & _
    '         "FROM prtranhead_formula_tran " & _
    '         "WHERE name = @name " & _
    '         "AND code = @code "

    '        If intUnkid > 0 Then
    '            strQ &= " AND tranheadformulatranunkid <> @tranheadformulatranunkid"
    '        End If

    '        objDataOperation.AddParameter("@code", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, strCode)
    '        objDataOperation.AddParameter("@name", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, strName)
    '        objDataOperation.AddParameter("@tranheadformulatranunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intUnkid)

    '        dsList = objDataOperation.ExecQuery(strQ, "List")

    '        If objDataOperation.ErrorMessage <> "" Then
    '            exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
    '            Throw exForce
    '        End If

    '        Return dsList.tables(0).rows.count > 0
    '    Catch ex As Exception
    '        Throw New Exception(ex.Message & "; Procedure Name: isExist; Module Name: " & mstrModuleName)
    '    Finally
    '        exForce = Nothing
    '        If dsList IsNot Nothing Then dsList.Dispose()
    '        objDataOperation = Nothing
    '    End Try
    'End Function

End Class