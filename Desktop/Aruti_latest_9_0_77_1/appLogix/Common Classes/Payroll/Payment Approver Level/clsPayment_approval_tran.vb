﻿'************************************************************************************************************************************
'Class Name : clsPayment_approval_tran.vb
'Purpose    :
'Date       :13/02/2013
'Written By :Sohail
'Modified   :
'************************************************************************************************************************************

Imports eZeeCommonLib
Imports eZee.Common.eZeeForm

''' <summary>
''' Purpose: 
''' Developer: Sohail
''' </summary>
Public Class clsPayment_approval_tran
    Private Shared ReadOnly mstrModuleName As String = "clsPayment_approval_tran"
    Dim objDataOperation As clsDataOperation
    Dim mstrMessage As String = ""


#Region " Private variables "
    Private mintPaymentapprovaltranunkid As Integer
    Private mintPaymenttranunkid As Integer
    Private mintLevelunkid As Integer
    Private mintPriority As Integer
    Private mdtApproval_Date As Date
    Private mintStatusunkid As Integer
    Private mintUserunkid As Integer
    Private mblnIsvoid As Boolean = True
    Private mstrDisapprovalreason As String = String.Empty
    Private mstrRemarks As String = String.Empty 'Sohail (27 Feb 2013)

    Private mintVoiduserunkid As Integer = 0
    Private mdtVoiddatetime As Date = Nothing
    Private mstrVoidreason As String = String.Empty
    Private mstrWebFormName As String = String.Empty
    Private mintLogEmployeeUnkid As Integer = -1

    'Sohail (18 May 2013) -- Start
    'TRA - ENHANCEMENT
    Private mstrWebIP As String = ""
    Private mstrWebhostName As String = ""
    'Sohail (18 May 2013) -- End

    Private mdtTable As DataTable
#End Region

#Region " Constructor "

    Public Sub New()
        Try
            mdtTable = New DataTable

            mdtTable.Columns.Add("paymentapprovaltranunkid", System.Type.GetType("System.Int32")).DefaultValue = -1
            mdtTable.Columns.Add("paymenttranunkid", System.Type.GetType("System.Int32")).DefaultValue = -1
            mdtTable.Columns.Add("levelunkid", System.Type.GetType("System.Int32")).DefaultValue = -1
            mdtTable.Columns.Add("priority", System.Type.GetType("System.Int32")).DefaultValue = -1
            mdtTable.Columns.Add("approval_date", System.Type.GetType("System.DateTime"))
            mdtTable.Columns.Add("statusunkid", System.Type.GetType("System.Int32")).DefaultValue = -1
            mdtTable.Columns.Add("userunkid", System.Type.GetType("System.Int32")).DefaultValue = -1
            mdtTable.Columns.Add("disapprovalreason", System.Type.GetType("System.String")).DefaultValue = String.Empty
            mdtTable.Columns.Add("isvoid", System.Type.GetType("System.Boolean")).DefaultValue = False
            mdtTable.Columns.Add("voiduserunkid", System.Type.GetType("System.Int32")).DefaultValue = -1
            mdtTable.Columns.Add("voiddatetime", System.Type.GetType("System.DateTime"))
            mdtTable.Columns.Add("voidreason", System.Type.GetType("System.String")).DefaultValue = String.Empty
            mdtTable.Columns.Add("remarks", System.Type.GetType("System.String")).DefaultValue = String.Empty 'Sohail (27 Feb 2013)
            'Sohail (18 Feb 2014) -- Start
            'Enhancement - AGKN
            mdtTable.Columns.Add("employeeunkid", System.Type.GetType("System.Int32")).DefaultValue = -1
            mdtTable.Columns.Add("Paymentmodeid", System.Type.GetType("System.Int32")).DefaultValue = -1
            mdtTable.Columns.Add("expaidamt", System.Type.GetType("System.Decimal")).DefaultValue = 0
            'Sohail (18 Feb 2014) -- End

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: New; Module Name: " & mstrModuleName)
        End Try
    End Sub

#End Region

#Region " Properties "

    'S.SANDEEP [28-May-2018] -- START 
    'ISSUE/ENHANCEMENT : {Audit Trails} 
    Private mstrFormName As String = String.Empty
    Public WriteOnly Property _FormName() As String
        Set(ByVal value As String)
            mstrFormName = value
        End Set
    End Property

    Private mstrClientIP As String = ""
    Public WriteOnly Property _ClientIP() As String
        Set(ByVal value As String)
            mstrClientIP = value
        End Set
    End Property

    Private mstrHostName As String = ""
    Public WriteOnly Property _HostName() As String
        Set(ByVal value As String)
            mstrHostName = value
        End Set
    End Property

    Private mblnIsWeb As Boolean = False
    Public WriteOnly Property _FromWeb() As Boolean
        Set(ByVal value As Boolean)
            mblnIsWeb = value
        End Set
    End Property

    Private mintAuditUserId As Integer = 0
    Public WriteOnly Property _AuditUserId() As Integer
        Set(ByVal value As Integer)
            mintAuditUserId = value
        End Set
    End Property

    Private mintCompanyUnkid As Integer = 0
    Public WriteOnly Property _CompanyUnkid() As Integer
        Set(ByVal value As Integer)
            mintCompanyUnkid = value
        End Set
    End Property

    Private mdtAuditDate As DateTime = Now
    Public WriteOnly Property _AuditDate() As DateTime
        Set(ByVal value As DateTime)
            mdtAuditDate = value
        End Set
    End Property

    Private xDataOpr As clsDataOperation = Nothing
    Public Property _xDataOpr() As clsDataOperation
        Get
            Return xDataOpr
        End Get
        Set(ByVal value As clsDataOperation)
            xDataOpr = value
        End Set
    End Property

    Private mintVoidlogingemployeeunkid As Integer = 0
    Public Property _Voidloginemployeeunkid() As Integer
        Get
            Return mintVoidlogingemployeeunkid
        End Get
        Set(ByVal value As Integer)
            mintVoidlogingemployeeunkid = value
        End Set
    End Property
    'S.SANDEEP [28-May-2018] -- END 

    Public ReadOnly Property _Message() As String
        Get
            Return mstrMessage
        End Get
    End Property

    ''' <summary>
    ''' Purpose: Get or Set paymentapprovaltranunkid
    ''' Modify By: Sohail
    ''' </summary>
    Public Property _Paymentapprovaltranunkid() As Integer
        Get
            Return mintPaymentapprovaltranunkid
        End Get
        Set(ByVal value As Integer)
            mintPaymentapprovaltranunkid = value
            Call GetData()
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set paymenttranunkid
    ''' Modify By: Sohail
    ''' </summary>
    Public Property _Paymenttranunkid() As Integer
        Get
            Return mintPaymenttranunkid
        End Get
        Set(ByVal value As Integer)
            mintPaymenttranunkid = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set levelunkid
    ''' Modify By: Sohail
    ''' </summary>
    Public Property _Levelunkid() As Integer
        Get
            Return mintLevelunkid
        End Get
        Set(ByVal value As Integer)
            mintLevelunkid = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set priority
    ''' Modify By: Sohail
    ''' </summary>
    Public Property _Priority() As Integer
        Get
            Return mintPriority
        End Get
        Set(ByVal value As Integer)
            mintPriority = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set approval_date
    ''' Modify By: Sohail
    ''' </summary>
    Public Property _Approval_Date() As Date
        Get
            Return mdtApproval_Date
        End Get
        Set(ByVal value As Date)
            mdtApproval_Date = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set statusunkid
    ''' Modify By: Sohail
    ''' </summary>
    Public Property _Statusunkid() As Integer
        Get
            Return mintStatusunkid
        End Get
        Set(ByVal value As Integer)
            mintStatusunkid = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set userunkid
    ''' Modify By: Sohail
    ''' </summary>
    Public Property _Userunkid() As Integer
        Get
            Return mintUserunkid
        End Get
        Set(ByVal value As Integer)
            mintUserunkid = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set isvoid
    ''' Modify By: Sohail
    ''' </summary>
    Public Property _Isvoid() As Boolean
        Get
            Return mblnIsvoid
        End Get
        Set(ByVal value As Boolean)
            mblnIsvoid = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set voiduserunkid
    ''' Modify By: Suhail
    ''' </summary>
    Public Property _Voiduserunkid() As Integer
        Get
            Return mintVoiduserunkid
        End Get
        Set(ByVal value As Integer)
            mintVoiduserunkid = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set voiddatetime
    ''' Modify By: Suhail
    ''' </summary>
    Public Property _Voiddatetime() As Date
        Get
            Return mdtVoiddatetime
        End Get
        Set(ByVal value As Date)
            mdtVoiddatetime = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set voidreason
    ''' Modify By: Suhail
    ''' </summary>
    Public Property _Voidreason() As String
        Get
            Return mstrVoidreason
        End Get
        Set(ByVal value As String)
            mstrVoidreason = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set disapprovalreason
    ''' Modify By: Sohail
    ''' </summary>
    Public Property _Disapprovalreason() As String
        Get
            Return mstrDisapprovalreason
        End Get
        Set(ByVal value As String)
            mstrDisapprovalreason = value
        End Set
    End Property

    'Sohail (27 Feb 2013) -- Start
    'TRA - ENHANCEMENT
    ''' <summary>
    ''' Purpose: Get or Set remark
    ''' Modify By: Sohail
    ''' </summary>
    Public Property _Remarks() As String
        Get
            Return mstrRemarks
        End Get
        Set(ByVal value As String)
            mstrRemarks = value
        End Set
    End Property
    'Sohail (27 Feb 2013) -- End


    Public Property _WebFormName() As String
        Get
            Return mstrWebFormName
        End Get
        Set(ByVal value As String)
            mstrWebFormName = value
        End Set
    End Property

    Public WriteOnly Property _LoginEmployeeUnkid() As Integer
        Set(ByVal value As Integer)
            mintLogEmployeeUnkid = value
        End Set
    End Property

    Public ReadOnly Property _DataTable() As DataTable
        Get
            Return mdtTable
        End Get
    End Property

    'Sohail (18 May 2013) -- Start
    'TRA - ENHANCEMENT
    Public WriteOnly Property _WebIP() As String
        Set(ByVal value As String)
            mstrWebIP = value
        End Set
    End Property

    Public WriteOnly Property _WebHostName() As String
        Set(ByVal value As String)
            mstrWebhostName = value
        End Set
    End Property
    'Sohail (18 May 2013) -- End
#End Region

    ''' <summary>
    ''' Modify By: Sohail
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>
    Public Sub GetData()
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        objDataOperation = New clsDataOperation

        Try
            strQ = "SELECT " & _
              "  paymentapprovaltranunkid " & _
              ", paymenttranunkid " & _
              ", levelunkid " & _
              ", priority " & _
              ", approval_date " & _
              ", statusunkid " & _
              ", userunkid " & _
              ", isvoid " & _
              ", voiduserunkid " & _
              ", voiddatetime " & _
              ", voidreason " & _
              ", disapprovalreason " & _
              ", ISNULL(remarks, '') AS remarks " & _
             "FROM prpayment_approval_tran " & _
             "WHERE paymentapprovaltranunkid = @paymentapprovaltranunkid "
            'Sohail (27 Feb 2013) - [remarks]

            objDataOperation.AddParameter("@paymentapprovaltranunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintPaymentapprovaltranunkid.ToString)

            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            For Each dtRow As DataRow In dsList.Tables(0).Rows
                mintPaymentapprovaltranunkid = CInt(dtRow.Item("paymentapprovaltranunkid"))
                mintPaymenttranunkid = CInt(dtRow.Item("paymenttranunkid"))
                mintLevelunkid = CInt(dtRow.Item("levelunkid"))
                mintPriority = CInt(dtRow.Item("priority"))
                mdtApproval_Date = dtRow.Item("approval_date")
                mintStatusunkid = CInt(dtRow.Item("statusunkid"))
                mintUserunkid = CInt(dtRow.Item("userunkid"))
                mblnIsvoid = CBool(dtRow.Item("isvoid"))
                mintVoiduserunkid = CInt(dtRow.Item("voiduserunkid"))
                If IsDBNull(dtRow.Item("voiddatetime")) = True Then
                    mdtVoiddatetime = Nothing
                Else
                    mdtVoiddatetime = dtRow.Item("voiddatetime")
                End If
                mstrVoidreason = dtRow.Item("voidreason").ToString
                mstrDisapprovalreason = dtRow.Item("disapprovalreason").ToString
                mstrRemarks = dtRow.Item("remarks").ToString 'Sohail (27 Feb 2013)
                Exit For
            Next
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: GetData; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
    End Sub

    ''' <summary>
    ''' Modify By: Sohail
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>
    Public Function GetList(ByVal strTableName As String, Optional ByVal blnOnlyActive As Boolean = True _
                            , Optional ByVal strPaymentTranUnkIDs As String = "" _
                            , Optional ByVal intLevelUnkID As Integer = 0 _
                            , Optional ByVal intPriority As Integer = 0 _
                            , Optional ByVal strFilter As String = "" _
                            ) As DataSet
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        objDataOperation = New clsDataOperation

        Try
            strQ = "SELECT " & _
              "  paymentapprovaltranunkid " & _
              ", paymenttranunkid " & _
              ", levelunkid " & _
              ", priority " & _
              ", approval_date " & _
              ", statusunkid " & _
              ", userunkid " & _
              ", isvoid " & _
              ", voiduserunkid " & _
              ", voiddatetime " & _
              ", voidreason " & _
              ", disapprovalreason " & _
              ", ISNULL(remarks, '') AS remarks " & _
             "FROM prpayment_approval_tran " & _
             "WHERE 1 = 1 "
            'Sohail (27 Feb 2013) - [remarks]

            If blnOnlyActive Then
                strQ &= " AND ISNULL(isvoid, 0) = 0 "
            End If

            If strPaymentTranUnkIDs.Trim <> "" Then
                strQ &= " AND paymenttranunkid IN ( " & strPaymentTranUnkIDs & " ) "
            End If

            If intLevelUnkID > 0 Then
                strQ &= " AND levelunkid = @levelunkid "
                objDataOperation.AddParameter("@levelunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intLevelUnkID)
            End If

            If intPriority > 0 Then
                strQ &= " AND priority = @priority "
                objDataOperation.AddParameter("@priority", SqlDbType.Int, eZeeDataType.INT_SIZE, intPriority)
            End If

            If strFilter.Trim <> "" Then
                strQ &= " AND " & strFilter
            End If

            dsList = objDataOperation.ExecQuery(strQ, strTableName)

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: GetList; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
        Return dsList
    End Function

    '''' <summary>
    '''' Modify By: Sohail
    '''' </summary>    

    'Nilay (10-Feb-2016) -- Start
    'Active Employee condition changes according to 58.1
    Public Function GetListForApproval(ByVal xDatabaseName As String, _
                                       ByVal xUserUnkid As Integer, _
                                       ByVal xYearUnkid As Integer, _
                                       ByVal xCompanyUnkid As Integer, _
                                       ByVal xPeriodStart As DateTime, _
                                       ByVal xPeriodEnd As DateTime, _
                                       ByVal xUserModeSetting As String, _
                                       ByVal xOnlyApproved As Boolean, _
                                       ByVal xIncludeIn_ActiveEmployee As Boolean, _
                                       ByVal strTableName As String, _
                                       ByVal intPayRefId As Integer, _
                                       ByVal intPaymentTypeId As Integer, _
                                       ByVal blnFromApprove As Boolean, _
                                       ByVal intCurrLevelPriority As Integer, _
                                       ByVal intLowerLevelPriority As Integer, _
                                       ByVal intMaxPriority As Integer, _
                                       Optional ByVal intPeriodID As Integer = 0, _
                                       Optional ByVal intCountryID As Integer = 0, _
                                       Optional ByVal intIsApproved As Integer = -1, _
                                       Optional ByVal strFilterQuery As String = "" _
                                       ) As DataSet
        'Sohail (17 Dec 2014) - [strAccessLevelFilterString]

        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim strMainQ As String = ""
        Dim exForce As Exception

        objDataOperation = New clsDataOperation

        Try
            Dim xUACQry, xUACFiltrQry, xAdvanceJoinQry As String
            xUACQry = "" : xUACFiltrQry = "" : xAdvanceJoinQry = ""

            Call NewAccessLevelFilterString(xUACQry, xUACFiltrQry, xPeriodEnd, xOnlyApproved, xDatabaseName, xUserUnkid, xCompanyUnkid, xYearUnkid, xUserModeSetting)
            Call GetAdvanceFilterQry(xAdvanceJoinQry, xPeriodEnd, xDatabaseName)

            strQ = "SELECT  ISNULL(prpayment_approval_tran.paymentapprovaltranunkid, -999) AS paymentapprovaltranunkid " & _
                               ", prpayment_tran.paymenttranunkid " & _
                               ", CASE WHEN ISNULL(isglobalpayment,0) = 0 THEN ISNULL(prpayment_tran.voucherno,'') " & _
                               "       WHEN ISNULL(isglobalpayment,0) = 1 THEN ISNULL(prglobalvoc_master.globalvocno,'') END AS voucherno " & _
                               ", prpayment_tran.periodunkid " & _
                               ", prpayment_tran.paymentdate " & _
                               ", prpayment_tran.employeeunkid " & _
                               ", ISNULL(hremployee_master.employeecode,'') AS employeecode " & _
                               ", ISNULL(hremployee_master.firstname,'')+' '+ISNULL(hremployee_master.othername,'')+' '+ISNULL(hremployee_master.surname,'') AS EmpName " & _
                               ", prpayment_tran.referenceid " & _
                               ", prpayment_tran.paymentmode " & _
                               ", prpayment_tran.branchunkid " & _
                               ", prpayment_tran.chequeno " & _
                               ", prpayment_tran.paymentby " & _
                               ", prpayment_tran.percentage " & _
                               ", prpayment_tran.amount " & _
                               ", prpayment_tran.voucherref " & _
                               ", prpayment_tran.referencetranunkid " & _
                               ", prpayment_tran.paytypeid " & _
                               ", prpayment_tran.isreceipt " & _
                               ", prpayment_tran.voidreason " & _
                               ", prpayment_tran.isglobalpayment " & _
                               ", prpayment_tran.basecurrencyid " & _
                               ", prpayment_tran.baseexchangerate " & _
                               ", prpayment_tran.paidcurrencyid " & _
                               ", prpayment_tran.expaidrate " & _
                               ", prpayment_tran.expaidamt " & _
                               ", prpayment_tran.globalvocno " & _
                               ", prpayment_tran.isauthorized " & _
                               ", prpayment_tran.accountno " & _
                               ", prpayment_tran.countryunkid " & _
                               ", prpayment_tran.isapproved " & _
                               ", prpayment_approval_tran.levelunkid " & _
                               ", prpaymentapproverlevel_master.levelcode " & _
                               ", prpaymentapproverlevel_master.levelname " & _
                               ", prpayment_approval_tran.priority " & _
                               ", prpayment_approval_tran.approval_date " & _
                               ", prpayment_approval_tran.statusunkid " & _
                               ", prpayment_approval_tran.userunkid " & _
                               ", prpayment_approval_tran.isvoid " & _
                               ", prpayment_approval_tran.voiddatetime " & _
                               ", prpayment_approval_tran.voiduserunkid " & _
                               ", prpayment_approval_tran.voidreason " & _
                               ", prpayment_approval_tran.disapprovalreason " & _
                               ", ISNULL(prpayment_approval_tran.remarks, '') AS remarks " & _
                         "FROM    prpayment_tran " & _
                                 "LEFT JOIN prpayment_approval_tran ON prpayment_tran.paymenttranunkid = prpayment_approval_tran.paymenttranunkid " & _
                                             "AND ISNULL(prpayment_approval_tran.isvoid, 0) = 0 " & _
                                 "LEFT JOIN prpaymentapproverlevel_master ON prpayment_approval_tran.levelunkid = prpaymentapproverlevel_master.levelunkid " & _
                                 "LEFT JOIN hremployee_master ON prpayment_tran.employeeunkid = hremployee_master.employeeunkid " & _
                                 "LEFT JOIN prglobalvoc_master ON prpayment_tran.globalvocno = prglobalvoc_master.globalvocunkid "

            If xUACQry.Trim.Length > 0 Then
                strQ &= xUACQry
            End If

            If xAdvanceJoinQry.Trim.Length > 0 Then
                strQ &= xAdvanceJoinQry
            End If

            strQ &= "     WHERE   ISNULL(prpayment_tran.isvoid, 0) = 0 " & _
                                 "AND ISNULL(prpaymentapproverlevel_master.isactive, 1) = 1 " & _
                                 "AND prpayment_tran.referenceid = @referenceid " & _
                                 "AND ISNULL(isauthorized, 0) = 0 " & _
                                 "AND prpayment_tran.paytypeid = @paytypeid "

            If xUACFiltrQry.Trim.Length > 0 Then
                strQ &= " AND " & xUACFiltrQry
            End If

            If intPeriodID > 0 Then
                strQ &= " AND prpayment_tran.periodunkid = @periodunkid "
                objDataOperation.AddParameter("@periodunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intPeriodID)
            End If

            If intCountryID > 0 Then
                strQ &= " AND prpayment_tran.countryunkid = @countryunkid "
                objDataOperation.AddParameter("@countryunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intCountryID)
            End If

            'Sohail (20 Feb 2013) -- Start
            'TRA - ENHANCEMENT
            'If blnFromApprove = True Then
            '    strQ &= " AND prpayment_approval_tran.priority = @lowerpriority AND prpayment_approval_tran.statusunkid IN (0, 1) "

            '    objDataOperation.AddParameter("@lowerpriority", SqlDbType.Int, eZeeDataType.INT_SIZE, intLowerLevelPriority)
            'Else
            '    strQ &= " AND prpayment_approval_tran.statusunkid = 1 AND prpayment_approval_tran.priority = @currpriority  "
            '    objDataOperation.AddParameter("@currpriority", SqlDbType.Int, eZeeDataType.INT_SIZE, intCurrLevelPriority)
            'End If

            If blnFromApprove = False AndAlso intMaxPriority = intCurrLevelPriority Then
                strQ &= " AND (prpayment_approval_tran.priority = @lowerpriority OR prpayment_approval_tran.priority = @currpriority) AND prpayment_approval_tran.statusunkid IN (0, 1) "
                objDataOperation.AddParameter("@currpriority", SqlDbType.Int, eZeeDataType.INT_SIZE, intCurrLevelPriority)
            Else
                strQ &= " AND prpayment_approval_tran.priority = @lowerpriority AND prpayment_approval_tran.statusunkid IN (0, 1) "
            End If
            objDataOperation.AddParameter("@lowerpriority", SqlDbType.Int, eZeeDataType.INT_SIZE, intLowerLevelPriority)
            'Sohail (20 Feb 2013) -- End

            If intIsApproved = 0 OrElse intIsApproved = 1 Then
                strQ &= " AND ISNULL(prpayment_tran.isapproved, 0) = @isapproved "
                objDataOperation.AddParameter("@isapproved", SqlDbType.Bit, eZeeDataType.BIT_SIZE, intIsApproved)
            End If

            If strFilterQuery.Trim <> "" Then
                strQ &= " AND " & strFilterQuery
            End If

            strQ &= " ORDER BY ISNULL(hremployee_master.firstname,'')+' '+ISNULL(hremployee_master.othername,'')+' '+ISNULL(hremployee_master.surname,'') " 'Sohail (18 Aug 2015)

            objDataOperation.AddParameter("@referenceid", SqlDbType.Int, eZeeDataType.INT_SIZE, intPayRefId)
            objDataOperation.AddParameter("@paytypeid", SqlDbType.Int, eZeeDataType.INT_SIZE, intPaymentTypeId)
            'If blnFromApprove = True Then
            'objDataOperation.AddParameter("@priority", SqlDbType.Int, eZeeDataType.INT_SIZE, intCurrLevelPriority)
            'Else
            'objDataOperation.AddParameter("@priority", SqlDbType.Int, eZeeDataType.INT_SIZE, intLowerLevelPriority)
            'End If

            dsList = objDataOperation.ExecQuery(strQ, strTableName)

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: GetListForApproval; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
        Return dsList
    End Function

    'Public Function GetListForApproval(ByVal strTableName As String _
    '                                   , ByVal intPayRefId As Integer _
    '                                   , ByVal intPaymentTypeId As Integer _
    '                                   , ByVal blnFromApprove As Boolean _
    '                                   , ByVal intCurrLevelPriority As Integer _
    '                                   , ByVal intLowerLevelPriority As Integer _
    '                                   , ByVal intMaxPriority As Integer _
    '                                   , Optional ByVal intPeriodID As Integer = 0 _
    '                                   , Optional ByVal intCountryID As Integer = 0 _
    '                                   , Optional ByVal intIsApproved As Integer = -1 _
    '                                   , Optional ByVal strFilterQuery As String = "" _
    '                                   , Optional ByVal strAccessLevelFilterString As String = "" _
    '                                   ) As DataSet
    '    Sohail (17 Dec 2014) - [strAccessLevelFilterString]

    '    Dim dsList As DataSet = Nothing
    '    Dim strQ As String = ""
    '    Dim strMainQ As String = ""
    '    Dim exForce As Exception

    '    objDataOperation = New clsDataOperation

    '    Try

    '        Sohail (17 Dec 2014) -- Start
    '        Enhancement - Provide Link for Payroll approval/Authorize Payslip Payment.
    '        If strAccessLevelFilterString.Trim = "" Then strAccessLevelFilterString = UserAccessLevel._AccessLevelFilterString
    '        Sohail (17 Dec 2014) -- End

    '        strQ = "SELECT  ISNULL(prpayment_approval_tran.paymentapprovaltranunkid, -999) AS paymentapprovaltranunkid " & _
    '                           ", prpayment_tran.paymenttranunkid " & _
    '                           ", CASE WHEN ISNULL(isglobalpayment,0) = 0 THEN ISNULL(prpayment_tran.voucherno,'') " & _
    '                           "       WHEN ISNULL(isglobalpayment,0) = 1 THEN ISNULL(prglobalvoc_master.globalvocno,'') END AS voucherno " & _
    '                           ", prpayment_tran.periodunkid " & _
    '                           ", prpayment_tran.paymentdate " & _
    '                           ", prpayment_tran.employeeunkid " & _
    '                           ", ISNULL(hremployee_master.employeecode,'') AS employeecode " & _
    '                           ", ISNULL(hremployee_master.firstname,'')+' '+ISNULL(hremployee_master.othername,'')+' '+ISNULL(hremployee_master.surname,'') AS EmpName " & _
    '                           ", prpayment_tran.referenceid " & _
    '                           ", prpayment_tran.paymentmode " & _
    '                           ", prpayment_tran.branchunkid " & _
    '                           ", prpayment_tran.chequeno " & _
    '                           ", prpayment_tran.paymentby " & _
    '                           ", prpayment_tran.percentage " & _
    '                           ", prpayment_tran.amount " & _
    '                           ", prpayment_tran.voucherref " & _
    '                           ", prpayment_tran.referencetranunkid " & _
    '                           ", prpayment_tran.paytypeid " & _
    '                           ", prpayment_tran.isreceipt " & _
    '                           ", prpayment_tran.voidreason " & _
    '                           ", prpayment_tran.isglobalpayment " & _
    '                           ", prpayment_tran.basecurrencyid " & _
    '                           ", prpayment_tran.baseexchangerate " & _
    '                           ", prpayment_tran.paidcurrencyid " & _
    '                           ", prpayment_tran.expaidrate " & _
    '                           ", prpayment_tran.expaidamt " & _
    '                           ", prpayment_tran.globalvocno " & _
    '                           ", prpayment_tran.isauthorized " & _
    '                           ", prpayment_tran.accountno " & _
    '                           ", prpayment_tran.countryunkid " & _
    '                           ", prpayment_tran.isapproved " & _
    '                           ", prpayment_approval_tran.levelunkid " & _
    '                           ", prpaymentapproverlevel_master.levelcode " & _
    '                           ", prpaymentapproverlevel_master.levelname " & _
    '                           ", prpayment_approval_tran.priority " & _
    '                           ", prpayment_approval_tran.approval_date " & _
    '                           ", prpayment_approval_tran.statusunkid " & _
    '                           ", prpayment_approval_tran.userunkid " & _
    '                           ", prpayment_approval_tran.isvoid " & _
    '                           ", prpayment_approval_tran.voiddatetime " & _
    '                           ", prpayment_approval_tran.voiduserunkid " & _
    '                           ", prpayment_approval_tran.voidreason " & _
    '                           ", prpayment_approval_tran.disapprovalreason " & _
    '                           ", ISNULL(prpayment_approval_tran.remarks, '') AS remarks " & _
    '                     "FROM    prpayment_tran " & _
    '                             "LEFT JOIN prpayment_approval_tran ON prpayment_tran.paymenttranunkid = prpayment_approval_tran.paymenttranunkid " & _
    '                                         "AND ISNULL(prpayment_approval_tran.isvoid, 0) = 0 " & _
    '                             "LEFT JOIN prpaymentapproverlevel_master ON prpayment_approval_tran.levelunkid = prpaymentapproverlevel_master.levelunkid " & _
    '                             "LEFT JOIN hremployee_master ON prpayment_tran.employeeunkid = hremployee_master.employeeunkid " & _
    '                             "LEFT JOIN prglobalvoc_master ON prpayment_tran.globalvocno = prglobalvoc_master.globalvocunkid " & _
    '                     "WHERE   ISNULL(prpayment_tran.isvoid, 0) = 0 " & _
    '                             "AND ISNULL(prpaymentapproverlevel_master.isactive, 1) = 1 " & _
    '                             "AND prpayment_tran.referenceid = @referenceid " & _
    '                             "AND ISNULL(isauthorized, 0) = 0 " & _
    '                             "AND prpayment_tran.paytypeid = @paytypeid "
    '        Sohail (27 Feb 2013) - [remarks]
    '        "AND prpayment_approval_tran.paymenttranunkid NOT IN ( " & _
    '                                                                "SELECT  paymenttranunkid " & _
    '                                                                "FROM    prpayment_approval_tran " & _
    '                                                                "WHERE   ISNULL(isvoid, 0) = 0 " & _
    '                                                                        "AND priority > @priority ) "

    '        Sohail (27 Feb 2013) -- Start
    '        TRA - ENHANCEMENT - Show All employee list to approver without giving checkbox selection
    '        Sohail (17 Dec 2014) -- Start
    '        Enhancement - Provide Link for Payroll approval/Authorize Payslip Payment.
    '        strQ &= UserAccessLevel._AccessLevelFilterString
    '        strQ &= strAccessLevelFilterString
    '        Sohail (17 Dec 2014) -- End
    '        Sohail (27 Feb 2013) -- End


    '        If intPeriodID > 0 Then
    '            strQ &= " AND prpayment_tran.periodunkid = @periodunkid "
    '            objDataOperation.AddParameter("@periodunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intPeriodID)
    '        End If

    '        If intCountryID > 0 Then
    '            strQ &= " AND prpayment_tran.countryunkid = @countryunkid "
    '            objDataOperation.AddParameter("@countryunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intCountryID)
    '        End If

    '        Sohail (20 Feb 2013) -- Start
    '        TRA(-ENHANCEMENT)
    '        If blnFromApprove = True Then
    '            strQ &= " AND prpayment_approval_tran.priority = @lowerpriority AND prpayment_approval_tran.statusunkid IN (0, 1) "

    '            objDataOperation.AddParameter("@lowerpriority", SqlDbType.Int, eZeeDataType.INT_SIZE, intLowerLevelPriority)
    '        Else
    '            strQ &= " AND prpayment_approval_tran.statusunkid = 1 AND prpayment_approval_tran.priority = @currpriority  "
    '            objDataOperation.AddParameter("@currpriority", SqlDbType.Int, eZeeDataType.INT_SIZE, intCurrLevelPriority)
    '        End If
    '        If blnFromApprove = False AndAlso intMaxPriority = intCurrLevelPriority Then
    '            strQ &= " AND (prpayment_approval_tran.priority = @lowerpriority OR prpayment_approval_tran.priority = @currpriority) AND prpayment_approval_tran.statusunkid IN (0, 1) "
    '            objDataOperation.AddParameter("@currpriority", SqlDbType.Int, eZeeDataType.INT_SIZE, intCurrLevelPriority)
    '        Else
    '            strQ &= " AND prpayment_approval_tran.priority = @lowerpriority AND prpayment_approval_tran.statusunkid IN (0, 1) "
    '        End If
    '        objDataOperation.AddParameter("@lowerpriority", SqlDbType.Int, eZeeDataType.INT_SIZE, intLowerLevelPriority)
    '        Sohail (20 Feb 2013) -- End


    '        If intIsApproved = 0 OrElse intIsApproved = 1 Then
    '            strQ &= " AND ISNULL(prpayment_tran.isapproved, 0) = @isapproved "
    '            objDataOperation.AddParameter("@isapproved", SqlDbType.Bit, eZeeDataType.BIT_SIZE, intIsApproved)
    '        End If

    '        If strFilterQuery.Trim <> "" Then
    '            strQ &= " AND " & strFilterQuery
    '        End If

    '        strQ &= " ORDER BY ISNULL(hremployee_master.firstname,'')+' '+ISNULL(hremployee_master.othername,'')+' '+ISNULL(hremployee_master.surname,'') " 'Sohail (18 Aug 2015)

    '        objDataOperation.AddParameter("@referenceid", SqlDbType.Int, eZeeDataType.INT_SIZE, intPayRefId)
    '        objDataOperation.AddParameter("@paytypeid", SqlDbType.Int, eZeeDataType.INT_SIZE, intPaymentTypeId)
    '        If blnFromApprove = True Then
    '            objDataOperation.AddParameter("@priority", SqlDbType.Int, eZeeDataType.INT_SIZE, intCurrLevelPriority)
    '        Else
    '            objDataOperation.AddParameter("@priority", SqlDbType.Int, eZeeDataType.INT_SIZE, intLowerLevelPriority)
    '        End If


    '        dsList = objDataOperation.ExecQuery(strQ, strTableName)

    '        If objDataOperation.ErrorMessage <> "" Then
    '            exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
    '            Throw exForce
    '        End If

    '    Catch ex As Exception
    '        Throw New Exception(ex.Message & "; Procedure Name: GetListForApproval; Module Name: " & mstrModuleName)
    '    Finally
    '        exForce = Nothing
    '        If dsList IsNot Nothing Then dsList.Dispose()
    '        objDataOperation = Nothing
    '    End Try
    '    Return dsList
    'End Function
    'Nilay (10-Feb-2016) -- End

    'Sohail (27 Feb 2013) -- Start
    'TRA - ENHANCEMENT

    Public Function GetApproverStatus(ByVal strTableName As String _
                                      , ByVal intPeriodId As Integer _
                                      , Optional ByVal intPriorityUpto As Integer = -1 _
                                      , Optional ByVal strFilter As String = "" _
                                      ) As DataSet

        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        objDataOperation = New clsDataOperation

        Try
            strQ = "SELECT  a.rowno " & _
                          ", a.atpaymentapprovaltranunkid " & _
                          ", a.paymenttranunkid " & _
                          ", a.levelunkid " & _
                          ", a.levelname " & _
                          ", a.priority " & _
                          ", a.statusunkid " & _
                          ", a.audituserunkid " & _
                          ", a.auditusername " & _
                          ", a.approval_date " & _
                          ", a.remarks " & _
                    "FROM    ( SELECT    RANK() OVER ( PARTITION BY atprpayment_approval_tran.paymenttranunkid, audituserunkid ORDER BY atpaymentapprovaltranunkid DESC ) AS rowno " & _
                                      ", atprpayment_approval_tran.atpaymentapprovaltranunkid " & _
                                      ", atprpayment_approval_tran.paymenttranunkid " & _
                                      ", atprpayment_approval_tran.levelunkid " & _
                                      ", prpaymentapproverlevel_master.levelname " & _
                                      ", atprpayment_approval_tran.priority " & _
                                      ", atprpayment_approval_tran.statusunkid " & _
                                      ", atprpayment_approval_tran.audituserunkid " & _
                                      ", ISNULL(cfuser_master.username, '') AS auditusername " & _
                                      ", atprpayment_approval_tran.approval_date " & _
                                      ", atprpayment_approval_tran.remarks " & _
                              "FROM      atprpayment_approval_tran " & _
                                        "JOIN prpayment_tran ON atprpayment_approval_tran.paymenttranunkid = prpayment_tran.paymenttranunkid " & _
                                        "LEFT JOIN hrmsConfiguration..cfuser_master ON cfuser_master.userunkid = atprpayment_approval_tran.audituserunkid " & _
                                        "LEFT JOIN prpaymentapproverlevel_master ON prpaymentapproverlevel_master.levelunkid = atprpayment_approval_tran.levelunkid " & _
                              "WHERE     ISNULL(prpayment_tran.isvoid, 0) = 0 " & _
                                        "AND prpayment_tran.periodunkid = @periodunkid " & _
                                        "AND atprpayment_approval_tran.levelunkid <> -1 " & _
                                        "AND atprpayment_approval_tran.priority <> -1 "
            '               'Sohail (06 Mar 2013) - [approval_date, remarks]
            '                           "AND prpayment_tran.isauthorized = 0 " 


            'If intApproverUserUnkId > 0 Then
            '    strQ &= " AND audituserunkid = @audituserunkid "
            '    objDataOperation.AddParameter("@audituserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intApproverUserUnkId)
            'End If

            'If intLevelUnkID > 0 Then
            '    strQ &= " AND levelunkid = @levelunkid "
            '    objDataOperation.AddParameter("@levelunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intLevelUnkID)
            'End If

            'If intPriority >= 0 Then
            '    strQ &= " AND priority = @priority "
            '    objDataOperation.AddParameter("@priority", SqlDbType.Int, eZeeDataType.INT_SIZE, intPriority)
            'End If

            If intPriorityUpto >= 0 Then
                strQ &= " AND atprpayment_approval_tran.priority <= @priorityupto "
                objDataOperation.AddParameter("@priorityupto", SqlDbType.Int, eZeeDataType.INT_SIZE, intPriorityUpto)
            End If

            If strFilter.Trim <> "" Then
                strQ &= " AND " & strFilter
            End If

            strQ &= "       ) AS a " & _
                    "WHERE   a.rowno = 1 " & _
                    "ORDER BY a.paymenttranunkid " & _
                          ", a.atpaymentapprovaltranunkid DESC "


            objDataOperation.AddParameter("@periodunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intPeriodId)

            dsList = objDataOperation.ExecQuery(strQ, strTableName)

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: GetApproverStatus; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
        Return dsList
    End Function
    'Sohail (27 Feb 2013) -- End

    'Sohail (18 Aug 2015) -- Start
    'Enhancement - List all approvals with their status on Payment Approval List screen.
    Public Function GetApprovalStatus(ByVal strTableName As String _
                                      , ByVal intPeriodId As Integer _
                                      , ByVal intPriorityUpto As Integer _
                                      , Optional ByVal intUserId As Integer = -1 _
                                      , Optional ByVal intEmployeeId As Integer = -1 _
                                      , Optional ByVal strFilter As String = "" _
                                      ) As DataSet

        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim strQFilter As String = ""
        Dim exForce As Exception

        objDataOperation = New clsDataOperation

        Try

            Dim intMaxPriority = (New clsPaymentApproverlevel_master).GetMaxPriority(True)

            If intEmployeeId > 0 Then
                strQFilter &= " AND prpayment_tran.employeeunkid  = @employeeunkid "
                objDataOperation.AddParameter("@employeeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intEmployeeId)
            End If

            strQ = "SELECT  prpayment_tran.paymenttranunkid  " & _
                          ", cfcommon_period_tran.periodunkid " & _
                          ", cfcommon_period_tran.period_code " & _
                          ", cfcommon_period_tran.period_name " & _
                          ", prpayment_tran.employeeunkid " & _
                          ", hremployee_master.firstname + ' ' + ISNULL(hremployee_master.othername, '') + ' ' + hremployee_master.surname AS employeename  " & _
                          ", CASE WHEN ISNULL(isglobalpayment, 0) = 0 " & _
                                 "THEN ISNULL(prpayment_tran.voucherno, '') " & _
                                 "WHEN ISNULL(isglobalpayment, 0) = 1 " & _
                                 "THEN ISNULL(prglobalvoc_master.globalvocno, '') " & _
                            "END AS voucherno  " & _
                          ", atprpayment_approval_tran.levelunkid " & _
                          ", prpaymentapproverlevel_master.levelcode " & _
                          ", prpaymentapproverlevel_master.levelname " & _
                          ", atprpayment_approval_tran.priority " & _
                          ", atprpayment_approval_tran.approval_date " & _
                          ", atprpayment_approval_tran.statusunkid " & _
                          ", CASE atprpayment_approval_tran.statusunkid " & _
                              "WHEN 1 THEN 'Approved' " & _
                              "WHEN 2 THEN 'Disapproved' " & _
                              "ELSE 'Pending' " & _
                            "END AS statusname  " & _
                          ", atprpayment_approval_tran.audituserunkid AS userunkid " & _
                          ", CASE cfuser_master.firstname " & _
                              "WHEN '' THEN cfuser_master.username " & _
                              "ELSE cfuser_master.firstname + ' ' + cfuser_master.lastname " & _
                            "END AS username  " & _
                          ", atprpayment_approval_tran.audituserunkid AS approvedbyuserunkid " & _
                          ", ISNULL(cfuser_master.username, '') AS approvedbyusername " & _
                          ", atprpayment_approval_tran.remarks " & _
                    "FROM    atprpayment_approval_tran " & _
                            "LEFT JOIN prpayment_tran ON atprpayment_approval_tran.paymenttranunkid = prpayment_tran.paymenttranunkid " & _
                            "LEFT JOIN cfcommon_period_tran ON prpayment_tran.periodunkid = cfcommon_period_tran.periodunkid " & _
                            "LEFT JOIN hremployee_master ON hremployee_master.employeeunkid = prpayment_tran.employeeunkid " & _
                            "LEFT JOIN prglobalvoc_master ON prglobalvoc_master.globalvocunkid = prpayment_tran.globalvocno " & _
                            "LEFT JOIN prpaymentapproverlevel_master ON atprpayment_approval_tran.levelunkid = prpaymentapproverlevel_master.levelunkid " & _
                            "LEFT JOIN hrmsConfiguration..cfuser_master ON cfuser_master.userunkid = atprpayment_approval_tran.audituserunkid " & _
                    "WHERE   ISNULL(prpayment_tran.isvoid, 0) = 0 " & _
                            "AND cfcommon_period_tran.isactive = 1 " & _
                            "AND cfcommon_period_tran.modulerefid = " & enModuleReference.Payroll & " " & _
                            "AND prpayment_tran.referenceid = " & clsPayment_tran.enPaymentRefId.PAYSLIP & " " & _
                            "AND prpayment_tran.paytypeid = " & clsPayment_tran.enPayTypeId.PAYMENT & " " & _
                            "AND prpayment_tran.periodunkid = @periodunkid " & _
                            "AND atprpayment_approval_tran.priority > 0 " & _
                            "AND atprpayment_approval_tran.priority <= @priorityupto " & _
                            strQFilter

            '<TODO: Need to test>
            If strFilter.Trim <> "" Then
                strQ &= " AND " & strFilter
            End If

            strQ &= "UNION ALL " & _
                    "SELECT  prpayment_tran.paymenttranunkid  " & _
                          ", prpayment_tran.periodunkid " & _
                          ", cfcommon_period_tran.period_code " & _
                          ", cfcommon_period_tran.period_name " & _
                          ", prpayment_tran.employeeunkid " & _
                          ", hremployee_master.firstname + ' ' + ISNULL(hremployee_master.othername, '') + ' ' + hremployee_master.surname AS employeename  " & _
                          ", CASE WHEN ISNULL(isglobalpayment, 0) = 0 " & _
                                 "THEN ISNULL(prpayment_tran.voucherno, '') " & _
                                 "WHEN ISNULL(isglobalpayment, 0) = 1 " & _
                                 "THEN ISNULL(prglobalvoc_master.globalvocno, '') " & _
                            "END AS voucherno  " & _
                          ", ISNULL(B.levelunkid, 0) AS levelunkid " & _
                          ", ISNULL(B.levelcode, '') AS levelcode " & _
                          ", ISNULL(B.levelname, '') AS levelname " & _
                          ", ISNULL(B.priority, 0) AS priority " & _
                          ", '88880808' AS approval_date " & _
                          ", 0 AS statusunkid " & _
                          ", 'Pending' AS statusname " & _
                          ", B.userapproverunkid AS userunkid " & _
                          ", CASE cfuser_master.firstname " & _
                              "WHEN '' THEN cfuser_master.username " & _
                              "ELSE cfuser_master.firstname + ' ' + cfuser_master.lastname " & _
                            "END AS username  " & _
                          ", 0 AS approvedbyuserunkid " & _
                          ", '' AS approvedusername " & _
                          ", '' AS remarks " & _
                    "FROM    ( SELECT    atprpayment_approval_tran.*  " & _
                                      ", DENSE_RANK() OVER ( PARTITION BY atprpayment_approval_tran.paymenttranunkid ORDER BY atprpayment_approval_tran.approval_date DESC, atprpayment_approval_tran.atpaymentapprovaltranunkid DESC ) AS ROW_NO " & _
                              "FROM      atprpayment_approval_tran " & _
                                        "LEFT JOIN prpayment_tran ON prpayment_tran.paymenttranunkid = atprpayment_approval_tran.paymenttranunkid " & _
                              "WHERE     ISNULL(prpayment_tran.isvoid, 0) = 0 " & _
                                        "AND prpayment_tran.referenceid = " & clsPayment_tran.enPaymentRefId.PAYSLIP & " " & _
                                        "AND prpayment_tran.paytypeid = " & clsPayment_tran.enPayTypeId.PAYMENT & " " & _
                                        "AND prpayment_tran.periodunkid = @periodunkid " & _
                                        strQFilter & _
                            ") AS A " & _
                            "LEFT JOIN ( SELECT  prpayment_approver_mapping.levelunkid  " & _
                                              ", prpaymentapproverlevel_master.levelcode " & _
                                              ", prpaymentapproverlevel_master.levelname " & _
                                              ", prpaymentapproverlevel_master.priority " & _
                                              ", prpayment_approver_mapping.userapproverunkid " & _
                                        "FROM    prpayment_approver_mapping " & _
                                                "LEFT JOIN prpaymentapproverlevel_master ON prpayment_approver_mapping.levelunkid = prpaymentapproverlevel_master.levelunkid " & _
                                        "WHERE   prpayment_approver_mapping.isvoid = 0 " & _
                                                "AND prpaymentapproverlevel_master.isactive = 1 " & _
                                                "AND prpaymentapproverlevel_master.isinactive = 0 " & _
                                      ") AS B ON 1 = 1 " & _
                            "LEFT JOIN prpayment_tran ON A.paymenttranunkid = prpayment_tran.paymenttranunkid " & _
                            "LEFT JOIN cfcommon_period_tran ON cfcommon_period_tran.periodunkid = prpayment_tran.periodunkid " & _
                            "LEFT JOIN hremployee_master ON hremployee_master.employeeunkid = prpayment_tran.employeeunkid " & _
                            "LEFT JOIN prglobalvoc_master ON prglobalvoc_master.globalvocunkid = prpayment_tran.globalvocno " & _
                            "LEFT JOIN hrmsConfiguration..cfuser_master ON cfuser_master.userunkid = B.userapproverunkid " & _
                    "WHERE   A.ROW_NO = 1 " & _
                            "AND ISNULL(prpayment_tran.isvoid, 0) = 0 " & _
                            "AND cfcommon_period_tran.modulerefid = " & enModuleReference.Payroll & " " & _
                            "AND prpayment_tran.referenceid = " & clsPayment_tran.enPaymentRefId.PAYSLIP & " " & _
                            "AND prpayment_tran.paytypeid = " & clsPayment_tran.enPayTypeId.PAYMENT & " " & _
                            "AND prpayment_tran.periodunkid = @periodunkid " & _
                            "AND B.priority <= @priorityupto "
            'Hemant (12 May 2023) -- Start
            'ISSUE : GNTZ - Payment approved at 1st level  but shows pending for 1st level on Next levels
            'strQ &= " AND NOT ( A.priority = @MaxPriority " & _
            '                 "AND A.statusunkid = 1 " & _
            '               ") " & _
            '       "AND A.priority <> B.priority  " & _
            '       strQFilter
            strQ &= "/*AND NOT ( A.priority = @MaxPriority " & _
                                      "AND A.statusunkid = 1 " & _
                                    ") " & _
                   "AND A.priority <> B.priority */ AND B.priority > A.priority " & _
                            strQFilter
            'Hemant (12 May 2023) -- End
          

            '<TODO: Need to test>
            If intUserId > 0 Then
                strQ &= " AND ((a.priority = @pririty AND A.userunkid = @userunkid) OR (a.priority_A < @priorityupto)) "
                objDataOperation.AddParameter("@userunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intUserId)
            End If

            '<TODO: Need to test>
            If strFilter.Trim <> "" Then
                strQ &= " AND " & strFilter
            End If

            If intUserId > 0 Then
                strQ &= " AND ((a.priority_A = @pririty AND A.userunkid_a = @userunkid) OR (a.priority_A < @priorityupto)) "
                objDataOperation.AddParameter("@userunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intUserId)
            End If

            If strFilter.Trim <> "" Then
                strQ &= " AND " & strFilter
            End If

            strQ &= " ORDER BY employeename, approval_date, priority "


            objDataOperation.AddParameter("@periodunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intPeriodId)
            objDataOperation.AddParameter("@priorityupto", SqlDbType.Int, eZeeDataType.INT_SIZE, intPriorityUpto)
            objDataOperation.AddParameter("@MaxPriority", SqlDbType.Int, eZeeDataType.INT_SIZE, intMaxPriority)

            dsList = objDataOperation.ExecQuery(strQ, strTableName)

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: GetApproverStatus; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
        Return dsList
    End Function
    'Sohail (18 Aug 2015) -- End

    'Sohail (11 Mar 2013) -- Start
    'TRA - ENHANCEMENT
    Public Function GetTotalApprovedCount(ByVal intPeriodId As Integer _
                                          , Optional ByVal intLevelUnkID As Integer = 0 _
                                          , Optional ByVal intPriority As Integer = -1 _
                                          , Optional ByVal strFilter As String = "" _
                                          ) As Integer
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        objDataOperation = New clsDataOperation

        Try
            strQ = "SELECT  COUNT(a.rowno) AS TotalCount " & _
                     "FROM    ( SELECT    RANK() OVER ( PARTITION BY atprpayment_approval_tran.paymenttranunkid, audituserunkid ORDER BY atpaymentapprovaltranunkid DESC ) AS rowno " & _
                               "FROM      atprpayment_approval_tran " & _
                                         "JOIN prpayment_tran ON atprpayment_approval_tran.paymenttranunkid = prpayment_tran.paymenttranunkid " & _
                               "WHERE     ISNULL(prpayment_tran.isvoid, 0) = 0 " & _
                                         "AND prpayment_tran.periodunkid = @periodunkid " & _
                                         "AND atprpayment_approval_tran.levelunkid <> -1 " & _
                                         "AND atprpayment_approval_tran.priority <> -1 " & _
                                         "AND atprpayment_approval_tran.statusunkid = 1 "


            If intLevelUnkID > 0 Then
                strQ &= " AND atprpayment_approval_tran.levelunkid = @levelunkid "
                objDataOperation.AddParameter("@levelunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intLevelUnkID)
            End If

            If intPriority > -1 Then
                strQ &= " AND atprpayment_approval_tran.priority = @priority "
                objDataOperation.AddParameter("@priority", SqlDbType.Int, eZeeDataType.INT_SIZE, intPriority)
            End If

            If strFilter.Trim <> "" Then
                strQ &= " AND " & strFilter
            End If

            strQ &= "       ) AS a " & _
                  "WHERE   a.rowno = 1 "

            objDataOperation.AddParameter("@periodunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intPeriodId)

            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            Return dsList.Tables("List").Rows(0).Item("TotalCount")

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: GetTotalApprovedCount; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
    End Function

    Public Function GetLastApprovedDetail(ByVal strTableName As String _
                                          , ByVal intPeriodId As Integer _
                                          , Optional ByVal intLevelUnkID As Integer = 0 _
                                          , Optional ByVal intPriority As Integer = -1 _
                                          , Optional ByVal intApprovedUserUnkId As Integer = 0 _
                                          , Optional ByVal strFilter As String = "" _
                                          ) As DataSet
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        objDataOperation = New clsDataOperation

        Try
            strQ = "SELECT a.rowno " & _
                          ", MAX(a.approval_date) AS approval_date " & _
                          ", a.audituserunkid " & _
                          ", ISNULL(cfuser_master.username, '') AS approvername " & _
                          ", ISNULL(cfuser_master.firstname, '') + ' ' + ISNULL(cfuser_master.lastname, '') AS approverfullname " & _
                          ", a.levelname " & _
                     "FROM    ( SELECT    RANK() OVER ( PARTITION BY atprpayment_approval_tran.paymenttranunkid, audituserunkid ORDER BY atpaymentapprovaltranunkid DESC ) AS rowno " & _
                                       ", atprpayment_approval_tran.approval_date " & _
                                       ", atprpayment_approval_tran.audituserunkid " & _
                                       ", prpaymentapproverlevel_master.levelname " & _
                               "FROM      atprpayment_approval_tran " & _
                                         "JOIN prpayment_tran ON atprpayment_approval_tran.paymenttranunkid = prpayment_tran.paymenttranunkid " & _
                                         "LEFT JOIN prpaymentapproverlevel_master ON prpaymentapproverlevel_master.levelunkid = atprpayment_approval_tran.levelunkid " & _
                               "WHERE     ISNULL(prpayment_tran.isvoid, 0) = 0 " & _
                                         "AND prpayment_tran.periodunkid = @periodunkid " & _
                                         "AND atprpayment_approval_tran.levelunkid <> -1 " & _
                                         "AND atprpayment_approval_tran.priority <> -1 " & _
                                         "AND atprpayment_approval_tran.statusunkid = 1 "
            '       'Sohail (16 Mar 2013) - [approverfullname]


            If intLevelUnkID > 0 Then
                strQ &= " AND atprpayment_approval_tran.levelunkid = @levelunkid "
                objDataOperation.AddParameter("@levelunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intLevelUnkID)
            End If

            If intPriority > -1 Then
                strQ &= " AND atprpayment_approval_tran.priority = @priority "
                objDataOperation.AddParameter("@priority", SqlDbType.Int, eZeeDataType.INT_SIZE, intPriority)
            End If

            If intApprovedUserUnkId > 0 Then
                strQ &= " AND atprpayment_approval_tran.audituserunkid = @audituserunkid "
                objDataOperation.AddParameter("@audituserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intApprovedUserUnkId)
            End If

            If strFilter.Trim <> "" Then
                strQ &= " AND " & strFilter
            End If

            strQ &= ") AS a " & _
                   "LEFT JOIN hrmsConfiguration..cfuser_master ON cfuser_master.userunkid = a.audituserunkid " & _
           "WHERE   a.rowno = 1 " & _
           "GROUP BY a.rowno " & _
                  ", a.audituserunkid " & _
                  ", ISNULL(cfuser_master.username, '') " & _
                  ", ISNULL(cfuser_master.firstname, '') + ' ' + ISNULL(cfuser_master.lastname, '') " & _
                  ", a.levelname "
            '   'Sohail (16 Mar 2013) - [ISNULL(cfuser_master.firstname, '') + ' ' + ISNULL(cfuser_master.lastname, '')]

            objDataOperation.AddParameter("@periodunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intPeriodId)

            dsList = objDataOperation.ExecQuery(strQ, strTableName)

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: GetTotalApprovedCount; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
        Return dsList
    End Function
    'Sohail (11 Mar 2013) -- End


    ''' <summary>
    ''' Modify By: Sohail
    ''' </summary>
    ''' <returns>Boolean</returns>
    ''' <purpose> INSERT INTO Database Table (prpayment_approval_tran) </purpose>
    Public Function Insert() As Boolean
        'If isExist(mstrName) Then
        '    mstrMessage = "<Message>"
        '    Return False
        'End If

        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        objDataOperation = New clsDataOperation

        Try
            objDataOperation.AddParameter("@paymenttranunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintPaymenttranunkid.ToString)
            objDataOperation.AddParameter("@levelunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintLevelunkid.ToString)
            objDataOperation.AddParameter("@priority", SqlDbType.Int, eZeeDataType.INT_SIZE, mintPriority.ToString)
            objDataOperation.AddParameter("@approval_date", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, mdtApproval_Date.ToString)
            objDataOperation.AddParameter("@statusunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintStatusunkid.ToString)
            objDataOperation.AddParameter("@userunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintUserunkid.ToString)
            objDataOperation.AddParameter("@isvoid", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsvoid.ToString)
            objDataOperation.AddParameter("@voiduserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintVoiduserunkid.ToString)
            If mdtVoiddatetime = Nothing Then
                objDataOperation.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, DBNull.Value)
            Else
                objDataOperation.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, mdtVoiddatetime)
            End If
            objDataOperation.AddParameter("@voidreason", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrVoidreason.ToString)
            objDataOperation.AddParameter("@disapprovalreason", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrDisapprovalreason.ToString)
            objDataOperation.AddParameter("@remarks", SqlDbType.NVarChar, eZeeDataType.DESC_SIZE, mstrRemarks.ToString) 'Sohail (27 Feb 2013)

            strQ = "INSERT INTO prpayment_approval_tran ( " & _
              "  paymenttranunkid " & _
              ", levelunkid " & _
              ", priority " & _
              ", approval_date " & _
              ", statusunkid " & _
              ", userunkid " & _
              ", isvoid" & _
              ", voiduserunkid " & _
              ", voiddatetime " & _
              ", voidreason " & _
              ", disapprovalreason " & _
              ", remarks " & _
            ") VALUES (" & _
              "  @paymenttranunkid " & _
              ", @levelunkid " & _
              ", @priority " & _
              ", @approval_date " & _
              ", @statusunkid " & _
              ", @userunkid " & _
              ", @isvoid" & _
              ", @voiduserunkid " & _
              ", @voiddatetime " & _
              ", @voidreason " & _
              ", @disapprovalreason " & _
              ", @remarks " & _
            "); SELECT @@identity"
            'Sohail (27 Feb 2013) - [remarks]

            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            mintPaymentapprovaltranunkid = dsList.Tables(0).Rows(0).Item(0)

            If InsertAuditTrailForPaymentApprovalTran(objDataOperation, 1) = False Then
                Return False
            End If

            Return True
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: Insert; Module Name: " & mstrModuleName)
            Return False
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
    End Function

    ''' <summary>
    ''' Modify By: Sohail
    ''' </summary>
    ''' <returns>Boolean</returns>
    ''' <purpose> Update Database Table (prpayment_approval_tran) </purpose>
    Public Function Update() As Boolean
        'If isExist(mstrName, mintPaymentapprovaltranunkid) Then
        '    mstrMessage = "<Message>"
        '    Return False
        'End If

        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        objDataOperation = New clsDataOperation

        Try
            objDataOperation.AddParameter("@paymentapprovaltranunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintPaymentapprovaltranunkid.ToString)
            objDataOperation.AddParameter("@paymenttranunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintPaymenttranunkid.ToString)
            objDataOperation.AddParameter("@levelunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintLevelunkid.ToString)
            objDataOperation.AddParameter("@priority", SqlDbType.Int, eZeeDataType.INT_SIZE, mintPriority.ToString)
            objDataOperation.AddParameter("@approval_date", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, mdtApproval_Date.ToString)
            objDataOperation.AddParameter("@statusunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintStatusunkid.ToString)
            objDataOperation.AddParameter("@userunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintUserunkid.ToString)
            objDataOperation.AddParameter("@isvoid", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsvoid.ToString)
            If mdtVoiddatetime = Nothing Then
                objDataOperation.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, DBNull.Value)
            Else
                objDataOperation.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, mdtVoiddatetime)
            End If
            objDataOperation.AddParameter("@voiduserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintVoiduserunkid.ToString)
            objDataOperation.AddParameter("@voidreason", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrVoidreason.ToString)
            objDataOperation.AddParameter("@disapprovalreason", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrDisapprovalreason.ToString)
            objDataOperation.AddParameter("@remarks", SqlDbType.NVarChar, eZeeDataType.DESC_SIZE, mstrRemarks.ToString) 'Sohail (27 Feb 2013)

            strQ = "UPDATE prpayment_approval_tran SET " & _
                        "  paymenttranunkid = @paymenttranunkid" & _
                        ", levelunkid = @levelunkid" & _
                        ", priority = @priority" & _
                        ", approval_date = @approval_date" & _
                        ", statusunkid = @statusunkid" & _
                        ", userunkid = @userunkid" & _
                        ", isvoid = @isvoid" & _
                        ", voiddatetime = @voiddatetime" & _
                        ", voiduserunkid = @voiduserunkid" & _
                        ", voidreason = @voidreason" & _
                        ", disapprovalreason = @disapprovalreason " & _
                        ", remarks = @remarks " & _
            "WHERE paymentapprovaltranunkid = @paymentapprovaltranunkid "
            'Sohail (27 Feb 2013) - [remarks]

            Call objDataOperation.ExecNonQuery(strQ)

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            If InsertAuditTrailForPaymentApprovalTran(objDataOperation, 2) = False Then
                Return False
            End If

            Return True
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: Update; Module Name: " & mstrModuleName)
            Return False
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
    End Function

    ' <summary>
    ' Modify By: Sohail
    ' </summary>
    ' <param name="mdtTable"></param>
    ' <param name="blnIsApproved">0 = Ignore,  1 = Update in prpayment_tran SET IsApproved = 1,   2 = Update in prpayment_tran SET IsApproved = 0 </param>
    ' <returns></returns>
    ' <remarks></remarks>

    'Nilay (25-Mar-2016) -- Start
    'Public Function UpdatePaymentApproval(ByVal strDatabaseName As String, ByVal intYearUnkid As Integer, _
    '                                      ByVal mdtTable As DataTable _
    '                                      , ByVal dtCurrentDateAndTime As Date _
    '                                      , Optional ByVal blnIsApproved As Integer = 0 _
    '                                      ) As Boolean

    Public Function UpdatePaymentApproval(ByVal xDatabaseName As String, _
                                          ByVal xUserUnkid As Integer, _
                                          ByVal xYearUnkid As Integer, _
                                          ByVal xCompanyUnkid As Integer, _
                                          ByVal xPeriodStart As Date, _
                                          ByVal xPeriodEnd As Date, _
                                          ByVal xUserModeSetting As String, _
                                          ByVal xOnlyApproved As Boolean, _
                                          ByVal mdtTable As DataTable, _
                                          ByVal dtCurrentDateAndTime As Date, _
                                          Optional ByVal blnIsApproved As Integer = 0) As Boolean
        'Nilay (25-Mar-2016) -- End

        'Sohail (15 Dec 2015) -- [strDatabaseName, intYearUnkid]
        'Sohail (21 Aug 2015) - [dtCurrentDateAndTime]

        Dim objDataOperation As clsDataOperation

        Dim StrQ As String = ""
        Dim dsList As New DataSet

        objDataOperation = New clsDataOperation
        objDataOperation.BindTransaction()

        Try
            For Each dtRow As DataRow In mdtTable.Rows
                mintPaymentapprovaltranunkid = CInt(dtRow.Item("paymentapprovaltranunkid"))
                mintPaymenttranunkid = CInt(dtRow.Item("paymenttranunkid"))
                mintLevelunkid = CInt(dtRow.Item("levelunkid"))
                mintPriority = CInt(dtRow.Item("priority"))
                mdtApproval_Date = dtRow.Item("approval_date")
                mintStatusunkid = CInt(dtRow.Item("statusunkid"))
                mintUserunkid = CInt(dtRow.Item("userunkid"))
                mstrDisapprovalreason = dtRow.Item("disapprovalreason").ToString
                mblnIsvoid = CBool(dtRow.Item("isvoid"))
                If IsDBNull(dtRow.Item("voiddatetime")) = True Then
                    mdtVoiddatetime = Nothing
                Else
                    mdtVoiddatetime = dtRow.Item("voiddatetime")
                End If
                mintVoiduserunkid = CInt(dtRow.Item("voiduserunkid"))
                mstrVoidreason = dtRow.Item("voidreason").ToString
                mstrRemarks = dtRow.Item("remarks").ToString 'Sohail (27 Feb 2013)

                'Sohail (17 Dec 2014) -- Start
                'Enhancement - Provide Link for Payroll approval/Authorize Payslip Payment.
                'Sohail (21 Jan 2015) -- Start
                'Saving Redesign - Allow to disapprove payment to last level approvers.
                'If isExist(mintPaymenttranunkid, mintLevelunkid, mintPriority) = True Then
                If isExist(mintPaymenttranunkid, mintLevelunkid, mintPriority, , blnIsApproved) = True Then
                    'Sohail (21 Jan 2015) -- End
                    mstrMessage = Language.getMessage(mstrModuleName, 2, "Sorry, Payment Approval is already done for this level for some of the selected payments. Please Re-select Period and process again.")
                    objDataOperation.ReleaseTransaction(False)
                    Return False
                End If
                'Sohail (17 Dec 2014) -- End

                If Update() = False Then
                    objDataOperation.ReleaseTransaction(False)
                    Return False
                End If
                If mintStatusunkid = 2 AndAlso mblnIsvoid = True Then
                    mintLevelunkid = -1
                    mintPriority = -1
                    mintStatusunkid = 0
                    mblnIsvoid = False
                    mstrDisapprovalreason = ""
                    mstrRemarks = "" 'Sohail (27 Feb 2013)
                    If Insert() = False Then
                        objDataOperation.ReleaseTransaction(False)
                        Return False
                    End If
                    If blnIsApproved = 2 Then
                        Dim objPayment As New clsPayment_tran
                        'Sohail (21 Aug 2015) -- Start
                        'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
                        'objPayment._Paymenttranunkid = mintPaymenttranunkid
                        objPayment.GetData(objDataOperation, mintPaymenttranunkid)
                        'Sohail (21 Aug 2015) -- End
                        objPayment._Isapproved = False

                        With objPayment
                            ._FormName = mstrFormName
                            ._LoginEmployeeUnkid = mintLogEmployeeUnkid
                            ._ClientIP = mstrClientIP
                            ._HostName = mstrHostName
                            ._FromWeb = mblnIsWeb
                            ._AuditUserId = mintAuditUserId
._CompanyUnkid = mintCompanyUnkid
                            ._AuditDate = mdtAuditDate
                        End With
                        'Sohail (21 Aug 2015) -- Start
                        'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
                        'If objPayment.Update(objDataOperation) = False Then

                        'Sohail (15 Dec 2015) -- Start
                        'Enhancement - New Loan Calculation type Reducing balance with Fixed Principal EMI and new interest calculation Monthly apart from Daily for KBC.
                        'If objPayment.Update(dtCurrentDateAndTime, objDataOperation) = False Then

                        'Nilay (25-Mar-2016) -- Start
                        'If objPayment.Update(strDatabaseName, intYearUnkid, dtCurrentDateAndTime, objDataOperation) = False Then
                        If objPayment.Update(xDatabaseName, xUserUnkid, xYearUnkid, xCompanyUnkid, xPeriodStart, xPeriodEnd, _
                                             xUserModeSetting, xOnlyApproved, dtCurrentDateAndTime, objDataOperation) = False Then
                            'Nilay (25-Mar-2016) -- End


                            'Sohail (15 Dec 2015) -- End


                            'Sohail (21 Aug 2015) -- End
                            objDataOperation.ReleaseTransaction(False)
                            Return False
                        End If
                    End If
                Else
                    If blnIsApproved = 1 Then
                        Dim objPayment As New clsPayment_tran
                        'Sohail (21 Aug 2015) -- Start
                        'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
                        'objPayment._Paymenttranunkid = mintPaymenttranunkid
                        objPayment.GetData(objDataOperation, mintPaymenttranunkid)
                        'Sohail (21 Aug 2015) -- End
                        objPayment._Isapproved = True
                        With objPayment
                            ._FormName = mstrFormName
                            ._LoginEmployeeUnkid = mintLogEmployeeUnkid
                            ._ClientIP = mstrClientIP
                            ._HostName = mstrHostName
                            ._FromWeb = mblnIsWeb
                            ._AuditUserId = mintAuditUserId
._CompanyUnkid = mintCompanyUnkid
                            ._AuditDate = mdtAuditDate
                        End With
                        'Sohail (21 Aug 2015) -- Start
                        'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
                        'If objPayment.Update(objDataOperation) = False Then

                        'Sohail (15 Dec 2015) -- Start
                        'Enhancement - New Loan Calculation type Reducing balance with Fixed Principal EMI and new interest calculation Monthly apart from Daily for KBC.
                        'If objPayment.Update(dtCurrentDateAndTime, objDataOperation) = False Then

                        'Nilay (25-Mar-2016) -- Start
                        'If objPayment.Update(strDatabaseName, intYearUnkid, dtCurrentDateAndTime, objDataOperation) = False Then
                        If objPayment.Update(xDatabaseName, xUserUnkid, xYearUnkid, xCompanyUnkid, xPeriodStart, xPeriodEnd, _
                                             xUserModeSetting, xOnlyApproved, dtCurrentDateAndTime, objDataOperation) = False Then
                            'Nilay (25-Mar-2016) -- End

                            'Sohail (15 Dec 2015) -- End

                            'Sohail (21 Aug 2015) -- End
                            objDataOperation.ReleaseTransaction(False)
                            Return False
                        End If
                    End If
                End If
            Next

            objDataOperation.ReleaseTransaction(True)

            Return True

        Catch ex As Exception
            objDataOperation.ReleaseTransaction(False)
            Throw New Exception(ex.Message & "; Procedure Name: UpdatePaymentApproval; Module Name: " & mstrModuleName)
        Finally
        End Try
    End Function

    Public Function InsertAuditTrailForPaymentApprovalTran(ByVal objDataOperation As clsDataOperation, ByVal AuditType As Integer) As Boolean

        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        objDataOperation = New clsDataOperation

        Try
            'Sohail (18 May 2013) -- Start
            'TRA - ENHANCEMENT
            If mstrWebIP.Trim = "" Then mstrWebIP = getIP()
            If mstrWebhostName.Trim = "" Then mstrWebhostName = getHostName()
            'Sohail (18 May 2013) -- End

            objDataOperation.ClearParameters()
            objDataOperation.AddParameter("@paymentapprovaltranunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintPaymentapprovaltranunkid.ToString)
            objDataOperation.AddParameter("@paymenttranunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintPaymenttranunkid.ToString)
            objDataOperation.AddParameter("@levelunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintLevelunkid.ToString)
            objDataOperation.AddParameter("@priority", SqlDbType.Int, eZeeDataType.INT_SIZE, mintPriority.ToString)
            objDataOperation.AddParameter("@approval_date", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, mdtApproval_Date.ToString)
            objDataOperation.AddParameter("@statusunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintStatusunkid.ToString)
            objDataOperation.AddParameter("@disapprovalreason", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrDisapprovalreason.ToString)
            objDataOperation.AddParameter("@audittype", SqlDbType.Int, eZeeDataType.INT_SIZE, AuditType.ToString)
            objDataOperation.AddParameter("@audituserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintUserunkid)
            objDataOperation.AddParameter("@auditdatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, mdtAuditDate)
            objDataOperation.AddParameter("@ip", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrWebIP)
            objDataOperation.AddParameter("@machine_name", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrWebhostName)
            If mstrWebFormName.Trim.Length <= 0 Then
                objDataOperation.AddParameter("@form_name", SqlDbType.NVarChar, 500, mstrFormName)
                objDataOperation.AddParameter("@isweb", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsWeb)

                objDataOperation.AddParameter("@loginemployeeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, -1) 'mintLogEmployeeUnkid
            Else
                objDataOperation.AddParameter("@form_name", SqlDbType.NVarChar, 500, mstrWebFormName)
                objDataOperation.AddParameter("@isweb", SqlDbType.Bit, eZeeDataType.BIT_SIZE, True)
                objDataOperation.AddParameter("@module_name1", SqlDbType.NVarChar, 500, Language.getMessage(mstrModuleName, 1, "WEB"))
                objDataOperation.AddParameter("@loginemployeeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintLogEmployeeUnkid)
            End If




            objDataOperation.AddParameter("@remarks", SqlDbType.NVarChar, eZeeDataType.DESC_SIZE, mstrRemarks.ToString) 'Sohail (27 Feb 2013)

            strQ = "INSERT INTO atprpayment_approval_tran ( " & _
                "  paymentapprovaltranunkid " & _
                ", paymenttranunkid " & _
                ", levelunkid " & _
                ", priority " & _
                ", approval_date " & _
                ", statusunkid " & _
                ", disapprovalreason " & _
                ", remarks " & _
                ", audittype " & _
                ", audituserunkid " & _
                ", auditdatetime " & _
                ", ip " & _
                ", machine_name" & _
                ", form_name " & _
                " " & _
                " " & _
                " " & _
                " " & _
                " " & _
                ", isweb " & _
                ", loginemployeeunkid " & _
            ") VALUES (" & _
                "  @paymentapprovaltranunkid " & _
                ", @paymenttranunkid " & _
                ", @levelunkid " & _
                ", @priority " & _
                ", @approval_date " & _
                ", @statusunkid " & _
                ", @disapprovalreason " & _
                ", @remarks " & _
                ", @audittype " & _
                ", @audituserunkid " & _
                ", @auditdatetime " & _
                ", @ip " & _
                ", @machine_name" & _
                ", @form_name " & _
                " " & _
                " " & _
                " " & _
                " " & _
                " " & _
                ", @isweb " & _
                ", @loginemployeeunkid " & _
            "); SELECT @@identity"
            'Sohail (27 Feb 2013) - [remarks]

            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            mintPaymentapprovaltranunkid = dsList.Tables(0).Rows(0).Item(0)

            Return True
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: InsertAuditTrailForPaymentApprovalTran; Module Name: " & mstrModuleName)
            Return False
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
    End Function



    '''' <summary>
    '''' Modify By: Sohail
    '''' </summary>
    '''' <returns>Boolean</returns>
    '''' <purpose> Delete Database Table (prpayment_approval_tran) </purpose>
    'Public Function Delete(ByVal intUnkid As Integer) As Boolean
    '    If isUsed(intUnkid) Then
    '        mstrMessage = "<Message>"
    '        Return False
    '    End If

    '    Dim dsList As DataSet = Nothing
    '    Dim strQ As String = ""
    '    Dim exForce As Exception

    '    objDataOperation = New clsDataOperation

    '    Try
    '        StrQ = "DELETE FROM prpayment_approval_tran " & _
    '        "WHERE paymentapprovaltranunkid = @paymentapprovaltranunkid "

    '        objDataOperation.AddParameter("@paymentapprovaltranunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intUnkid)

    '        Call objDataOperation.ExecNonQuery(strQ)

    '        If objDataOperation.ErrorMessage <> "" Then
    '            exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
    '            Throw exForce
    '        End If

    '        If InsertAuditTrailForPaymentapprovalTran(objDataOperation, 3) = False Then
    '            Return False
    '        End If

    '        Return True
    '    Catch ex As Exception
    '        Throw New Exception(ex.Message & "; Procedure Name: Update; Module Name: " & mstrModuleName)
    '        Return False
    '    Finally
    '        exForce = Nothing
    '        If dsList IsNot Nothing Then dsList.Dispose()
    '        objDataOperation = Nothing
    '    End Try
    'End Function

    '''' <summary>
    '''' Modify By: Sohail
    '''' </summary>
    '''' <purpose> Assign all Property variable </purpose>
    'Public Function isUsed(ByVal intUnkid As Integer) As Boolean
    '    Dim dsList As DataSet = Nothing
    '    Dim strQ As String = ""
    '    Dim exForce As Exception

    '    objDataOperation = New clsDataOperation

    '    Try
    '        StrQ = "<Query>"

    '        objDataOperation.AddParameter("@paymentapprovaltranunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intUnkid)

    '        dsList = objDataOperation.ExecQuery(strQ, "List")

    '        If objDataOperation.ErrorMessage <> "" Then
    '            exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
    '            Throw exForce
    '        End If

    '        Return dsList.tables(0).rows.count > 0
    '    Catch ex As Exception
    '        Throw New Exception(ex.Message & "; Procedure Name: isUsed; Module Name: " & mstrModuleName)
    '    Finally
    '        exForce = Nothing
    '        If dsList IsNot Nothing Then dsList.Dispose()
    '        objDataOperation = Nothing
    '    End Try
    'End Function

    ''' <summary>
    ''' Modify By: Sohail
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>
    Public Function isExist(ByVal intPaymentTranUnkId As Integer _
                            , Optional ByVal intLevelUnkId As Integer = 0 _
                            , Optional ByVal intPriority As Integer = 0 _
                            , Optional ByVal intUnkid As Integer = -1 _
                            , Optional ByVal blnIsApproved As Integer = 0 _
                            ) As Boolean
        'Sohail (21 Jan 2015) - [blnIsApproved]

        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        objDataOperation = New clsDataOperation

        Try
            strQ = "SELECT " & _
              "  1 " & _
             "FROM prpayment_approval_tran " & _
             "WHERE ISNULL(isvoid, 0) = 0 " & _
             "AND paymenttranunkid = @paymenttranunkid "


            If intLevelUnkId > 0 Then
                strQ &= "AND levelunkid = @levelunkid "
                objDataOperation.AddParameter("@levelunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intLevelUnkId)
            End If

            If intPriority > 0 Then
                strQ &= "AND priority = @priority "
                objDataOperation.AddParameter("@priority", SqlDbType.Int, eZeeDataType.INT_SIZE, intPriority)
            End If

            'Sohail (21 Jan 2015) -- Start
            'Saving Redesign - Allow to disapprove payment to last level approvers.
            If blnIsApproved = 1 Then 'Approved
                strQ &= "AND statusunkid = " & blnIsApproved & " "

            ElseIf blnIsApproved = 2 Then 'Disapproved
                strQ &= "AND statusunkid = " & blnIsApproved & " "

            End If
            'Sohail (21 Jan 2015) -- End

            If intUnkid > 0 Then
                strQ &= " AND paymentapprovaltranunkid <> @paymentapprovaltranunkid"
                objDataOperation.AddParameter("@paymentapprovaltranunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intUnkid)
            End If

            objDataOperation.AddParameter("@paymenttranunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intPaymentTranUnkId)

            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            Return dsList.Tables(0).Rows.Count > 0
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: isExist; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
    End Function

    '<Language> This Auto Generated Text Please Do Not Modify it.
#Region " Language & UI Settings "
    Public Shared Sub SetMessages()
        Try
            Language.setMessage(mstrModuleName, 1, "WEB")
            Language.setMessage(mstrModuleName, 2, "Sorry, Payment Approval is already done for this level for some of the selected payments. Please Re-select Period and process again.")

        Catch Ex As Exception
            DisplayError.Show("-1", Ex.Message, "SetMessages", mstrModuleName)
        End Try
    End Sub
#End Region 'Language & UI Settings
    '</Language>
End Class
