﻿'************************************************************************************************************************************
'Class Name : clspaypoint_master.vb
'Purpose    : All Pay Point Opration like getList, Insert, Update, Delete, checkDuplicate
'Date       :01/07/2010
'Written By :Pinkal
'Modified   :
'Last Message Index = 3
'************************************************************************************************************************************

Imports eZeeCommonLib
''' <summary>
''' Purpose: 
''' Developer: Pinkal
''' </summary>
Public Class clspaypoint_master
    Private Shared ReadOnly mstrModuleName As String = "clspaypoint_master"
    Dim objDataOperation As clsDataOperation
    Dim mstrMessage As String = ""

#Region " Private variables "
    Private mintPaypointunkid As Integer
    Private mstrPaypointalias As String = String.Empty
    Private mstrPaypointcode As String = String.Empty
    Private mstrPaypointname As String = String.Empty
    Private mstrDescription As String = String.Empty
    Private mblnIsactive As Boolean = True
    Private mstrPaypointname1 As String = String.Empty
    Private mstrPaypointname2 As String = String.Empty
#End Region

#Region " Properties "

'S.SANDEEP [28-May-2018] -- START 
'ISSUE/ENHANCEMENT : {Audit Trails} 
Private mstrFormName As String = String.Empty
Public WriteOnly Property _FormName() As String 
Set(ByVal value As String) 
mstrFormName = value 
End Set 
End Property 
 
Private mstrClientIP As String = ""
Public WriteOnly Property _ClientIP() As String 
Set(ByVal value As String) 
mstrClientIP = value 
End Set 
End Property 
 
Private mstrHostName As String = ""
Public WriteOnly Property _HostName() As String 
Set(ByVal value As String) 
mstrHostName = value 
End Set 
End Property 
 
Private mblnIsWeb As Boolean = False
Public WriteOnly Property _FromWeb() As Boolean 
Set(ByVal value As Boolean) 
mblnIsWeb = value 
End Set 
End Property 
 
    Private mintAuditUserId As Integer = 0
    Public WriteOnly Property _AuditUserId() As Integer
        Set(ByVal value As Integer)
            mintAuditUserId = value
        End Set
    End Property

    Private mintCompanyUnkid As Integer = 0
    Public WriteOnly Property _CompanyUnkid() As Integer
        Set(ByVal value As Integer)
            mintCompanyUnkid = value
        End Set
    End Property
 
Private mdtAuditDate As DateTime = Now
Public WriteOnly Property _AuditDate() As DateTime 
Set(ByVal value As DateTime) 
mdtAuditDate = value 
End Set 
End Property 
 
Private xDataOpr As clsDataOperation = Nothing
Public Property _xDataOpr() As clsDataOperation 
Get 
Return xDataOpr 
End Get 
Set(ByVal value As clsDataOperation) 
xDataOpr = value 
End Set 
End Property 
 
Private mintLoginEmployeeunkid As Integer = 0
Public Property _LoginEmployeeunkid() As Integer 
Get 
Return mintLoginEmployeeunkid 
End Get 
Set(ByVal value As Integer) 
mintLoginEmployeeunkid = value 
End Set 
End Property 
 
Private mintVoidlogingemployeeunkid As Integer=0
Public Property _Voidloginemployeeunkid() As Integer 
Get 
Return mintVoidlogingemployeeunkid 
End Get 
Set(ByVal value As Integer) 
mintVoidlogingemployeeunkid = value 
End Set 
End Property 
'S.SANDEEP [28-May-2018] -- END 

    Public ReadOnly Property _Message() As String
        Get
            Return mstrMessage
        End Get
    End Property

    ''' <summary>
    ''' Purpose: Get or Set paypointunkid
    ''' Modify By: Pinkal
    ''' </summary>
    Public Property _Paypointunkid() As Integer
        Get
            Return mintPaypointunkid
        End Get
        Set(ByVal value As Integer)
            mintPaypointunkid = Value
            Call getData()
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set paypointalias
    ''' Modify By: Pinkal
    ''' </summary>
    Public Property _Paypointalias() As String
        Get
            Return mstrPaypointalias
        End Get
        Set(ByVal value As String)
            mstrPaypointalias = Value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set paypointcode
    ''' Modify By: Pinkal
    ''' </summary>
    Public Property _Paypointcode() As String
        Get
            Return mstrPaypointcode
        End Get
        Set(ByVal value As String)
            mstrPaypointcode = Value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set paypointname
    ''' Modify By: Pinkal
    ''' </summary>
    Public Property _Paypointname() As String
        Get
            Return mstrPaypointname
        End Get
        Set(ByVal value As String)
            mstrPaypointname = Value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set description
    ''' Modify By: Pinkal
    ''' </summary>
    Public Property _Description() As String
        Get
            Return mstrDescription
        End Get
        Set(ByVal value As String)
            mstrDescription = Value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set isactive
    ''' Modify By: Pinkal
    ''' </summary>
    Public Property _Isactive() As Boolean
        Get
            Return mblnIsactive
        End Get
        Set(ByVal value As Boolean)
            mblnIsactive = Value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set paypointname1
    ''' Modify By: Pinkal
    ''' </summary>
    Public Property _Paypointname1() As String
        Get
            Return mstrPaypointname1
        End Get
        Set(ByVal value As String)
            mstrPaypointname1 = Value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set paypointname2
    ''' Modify By: Pinkal
    ''' </summary>
    Public Property _Paypointname2() As String
        Get
            Return mstrPaypointname2
        End Get
        Set(ByVal value As String)
            mstrPaypointname2 = Value
        End Set
    End Property

#End Region

    ''' <summary>
    ''' Modify By: Pinkal
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>
    Public Sub GetData()
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        objDataOperation = New clsDataOperation

        Try
            StrQ = "SELECT " & _
              "  paypointunkid " & _
              ", paypointalias " & _
              ", paypointcode " & _
              ", paypointname " & _
              ", description " & _
              ", isactive " & _
              ", paypointname1 " & _
              ", paypointname2 " & _
             "FROM prpaypoint_master " & _
             "WHERE paypointunkid = @paypointunkid "

            objDataOperation.AddParameter("@paypointunkid", SqlDbType.int, eZeeDataType.INT_SIZE, mintPaypointUnkId.ToString)

            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            For Each dtRow As DataRow In dsList.Tables(0).Rows
                mintpaypointunkid = CInt(dtRow.Item("paypointunkid"))
                mstrpaypointalias = dtRow.Item("paypointalias").ToString
                mstrpaypointcode = dtRow.Item("paypointcode").ToString
                mstrpaypointname = dtRow.Item("paypointname").ToString
                mstrdescription = dtRow.Item("description").ToString
                mblnisactive = CBool(dtRow.Item("isactive"))
                mstrpaypointname1 = dtRow.Item("paypointname1").ToString
                mstrpaypointname2 = dtRow.Item("paypointname2").ToString
                Exit For
            Next
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: GetData; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
    End Sub


    ''' <summary>
    ''' Modify By: Pinkal
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>
    Public Function GetList(ByVal strTableName As String, Optional ByVal blnOnlyActive As Boolean = True) As DataSet
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        objDataOperation = New clsDataOperation

        Try
            StrQ = "SELECT " & _
              "  paypointunkid " & _
              ", paypointalias " & _
              ", paypointcode " & _
              ", paypointname " & _
              ", description " & _
              ", isactive " & _
              ", paypointname1 " & _
              ", paypointname2 " & _
             "FROM prpaypoint_master "

            If blnOnlyActive Then
                strQ &= " WHERE isactive = 1 "
            End If

            dsList = objDataOperation.ExecQuery(strQ, strTableName)

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: GetList; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
        Return dsList
    End Function


    ''' <summary>
    ''' Modify By: Pinkal
    ''' </summary>
    ''' <returns>Boolean</returns>
    ''' <purpose> INSERT INTO Database Table (prpaypoint_master) </purpose>
    Public Function Insert() As Boolean
        If isExist(mstrPaypointcode) Then
            mstrMessage = Language.getMessage(mstrModuleName, 1, "This Pay Point Code is already defined. Please define new Pay Point Code.")
            Return False
        ElseIf isExist("", mstrPaypointname) Then
            mstrMessage = Language.getMessage(mstrModuleName, 2, "This Pay Point Name is already defined. Please define new Pay Point Name.")
            Return False
        End If

        Dim objDataOperation As clsDataOperation 'Sohail (12 Oct 2011)
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        objDataOperation = New clsDataOperation
        objDataOperation.BindTransaction() 'Sohail (12 Oct 2011)

        Try
            objDataOperation.AddParameter("@paypointalias", SqlDbType.nvarchar, eZeeDataType.NAME_SIZE, mstrpaypointalias.ToString)
            objDataOperation.AddParameter("@paypointcode", SqlDbType.nvarchar, eZeeDataType.NAME_SIZE, mstrpaypointcode.ToString)
            objDataOperation.AddParameter("@paypointname", SqlDbType.nvarchar, eZeeDataType.NAME_SIZE, mstrpaypointname.ToString)
            objDataOperation.AddParameter("@description", SqlDbType.nvarchar, eZeeDataType.NAME_SIZE, mstrdescription.ToString)
            objDataOperation.AddParameter("@isactive", SqlDbType.bit, eZeeDataType.BIT_SIZE, mblnisactive.ToString)
            objDataOperation.AddParameter("@paypointname1", SqlDbType.nvarchar, eZeeDataType.NAME_SIZE, mstrpaypointname1.ToString)
            objDataOperation.AddParameter("@paypointname2", SqlDbType.nvarchar, eZeeDataType.NAME_SIZE, mstrpaypointname2.ToString)

            StrQ = "INSERT INTO prpaypoint_master ( " & _
              "  paypointalias " & _
              ", paypointcode " & _
              ", paypointname " & _
              ", description " & _
              ", isactive " & _
              ", paypointname1 " & _
              ", paypointname2" & _
            ") VALUES (" & _
              "  @paypointalias " & _
              ", @paypointcode " & _
              ", @paypointname " & _
              ", @description " & _
              ", @isactive " & _
              ", @paypointname1 " & _
              ", @paypointname2" & _
            "); SELECT @@identity"

            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            mintPaypointUnkId = dsList.Tables(0).Rows(0).Item(0)

'S.SANDEEP [28-May-2018] -- START
'ISSUE/ENHANCEMENT : {Audit Trails} 
Dim objCommonATLog AS New clsCommonATLog 
objCommonATLog._FormName = mstrFormName 
objCommonATLog._LoginEmployeeUnkid = mintLoginEmployeeunkid 
            objCommonATLog._ClientIP = mstrClientIP
            objCommonATLog._HostName = mstrHostName
            objCommonATLog._FromWeb = mblnIsWeb
            objCommonATLog._AuditUserId = mintAuditUserId
objCommonATLog._CompanyUnkid = mintCompanyUnkid
            objCommonATLog._AuditDate = mdtAuditDate
            'S.SANDEEP [28-May-2018] -- END

            If objCommonATLog.Insert_AtLog(objDataOperation, 1, "prpaypoint_master", "paypointunkid", mintPaypointunkid) = False Then
                objDataOperation.ReleaseTransaction(False)
                Return False
            Else
                objDataOperation.ReleaseTransaction(True)
            Return True
            End If

            'S.SANDEEP [28-May-2018] -- START
            'ISSUE/ENHANCEMENT : {Audit Trails} 
            objCommonATLog = Nothing
            'S.SANDEEP [28-May-2018] -- END

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: Insert; Module Name: " & mstrModuleName)
            Return False
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
    End Function

    ''' <summary>
    ''' Modify By: Pinkal
    ''' </summary>
    ''' <returns>Boolean</returns>
    ''' <purpose> Update Database Table (prpaypoint_master) </purpose>
    Public Function Update() As Boolean
        If isExist(mstrPaypointcode, "", mintPaypointunkid) Then
            mstrMessage = Language.getMessage(mstrModuleName, 1, "This Pay Point Code is already defined. Please define new Pay Point Code.")
            Return False
        ElseIf isExist("", mstrPaypointname, mintPaypointunkid) Then
            mstrMessage = Language.getMessage(mstrModuleName, 2, "This Pay Point Name is already defined. Please define new Pay Point Name.")
            Return False
        End If

        Dim objDataOperation As clsDataOperation 'Sohail (12 Oct 2011)
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        objDataOperation = New clsDataOperation
        objDataOperation.BindTransaction() 'Sohail (12 Oct 2011)

        Try
            objDataOperation.AddParameter("@paypointunkid", SqlDbType.int, eZeeDataType.INT_SIZE, mintpaypointunkid.ToString)
            objDataOperation.AddParameter("@paypointalias", SqlDbType.nvarchar, eZeeDataType.NAME_SIZE, mstrpaypointalias.ToString)
            objDataOperation.AddParameter("@paypointcode", SqlDbType.nvarchar, eZeeDataType.NAME_SIZE, mstrpaypointcode.ToString)
            objDataOperation.AddParameter("@paypointname", SqlDbType.nvarchar, eZeeDataType.NAME_SIZE, mstrpaypointname.ToString)
            objDataOperation.AddParameter("@description", SqlDbType.nvarchar, eZeeDataType.NAME_SIZE, mstrdescription.ToString)
            objDataOperation.AddParameter("@isactive", SqlDbType.bit, eZeeDataType.BIT_SIZE, mblnisactive.ToString)
            objDataOperation.AddParameter("@paypointname1", SqlDbType.nvarchar, eZeeDataType.NAME_SIZE, mstrpaypointname1.ToString)
            objDataOperation.AddParameter("@paypointname2", SqlDbType.nvarchar, eZeeDataType.NAME_SIZE, mstrpaypointname2.ToString)

            StrQ = "UPDATE prpaypoint_master SET " & _
              "  paypointalias = @paypointalias" & _
              ", paypointcode = @paypointcode" & _
              ", paypointname = @paypointname" & _
              ", description = @description" & _
              ", isactive = @isactive" & _
              ", paypointname1 = @paypointname1" & _
              ", paypointname2 = @paypointname2 " & _
            "WHERE paypointunkid = @paypointunkid "

            Call objDataOperation.ExecNonQuery(strQ)

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            'Sohail (12 Oct 2011) -- Start
            Dim blnToInsert As Boolean
            'S.SANDEEP [28-May-2018] -- START
            'ISSUE/ENHANCEMENT : {Audit Trails} 
            Dim objCommonATLog As New clsCommonATLog
            objCommonATLog._FormName = mstrFormName
            objCommonATLog._LoginEmployeeUnkid = mintLoginEmployeeunkid
            objCommonATLog._ClientIP = mstrClientIP
            objCommonATLog._HostName = mstrHostName
            objCommonATLog._FromWeb = mblnIsWeb
            objCommonATLog._AuditUserId = mintAuditUserId
objCommonATLog._CompanyUnkid = mintCompanyUnkid
            objCommonATLog._AuditDate = mdtAuditDate
            'S.SANDEEP [28-May-2018] -- END

            If objCommonATLog.IsTableDataUpdate("atcommon_log", "prpaypoint_master", mintPaypointunkid, "paypointunkid", 2) Then
                blnToInsert = True
            End If
            If blnToInsert = True AndAlso objCommonATLog.Insert_AtLog(objDataOperation, 2, "prpaypoint_master", "paypointunkid", mintPaypointunkid) = False Then
                objDataOperation.ReleaseTransaction(False)
                Return False
            Else
                objDataOperation.ReleaseTransaction(True)
            Return True
            End If

            'S.SANDEEP [28-May-2018] -- START
            'ISSUE/ENHANCEMENT : {Audit Trails} 
            objCommonATLog = Nothing
            'S.SANDEEP [28-May-2018] -- END

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: Update; Module Name: " & mstrModuleName)
            Return False
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
    End Function

    ''' <summary>
    ''' Modify By: Pinkal
    ''' </summary>
    ''' <returns>Boolean</returns>
    ''' <purpose> Delete Database Table (prpaypoint_master) </purpose>
    Public Function Delete(ByVal intUnkid As Integer) As Boolean
        'If isUsed(intUnkid) Then
        '    mstrMessage = Language.getMessage(mstrModuleName, 3, "Sorry, You cannot delete selected Pay Point. Reason: This Pay Point is in use.")
        '    Return False
        'End If

        Dim objDataOperation As clsDataOperation 'Sohail (12 Oct 2011)
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        objDataOperation = New clsDataOperation
        objDataOperation.BindTransaction() 'Sohail (12 Oct 2011)

        Try
            'Sohail (28 Dec 2010) -- Start
            'strQ = "DELETE FROM prpaypoint_master " & _
            '"WHERE paypointunkid = @paypointunkid "
            strQ = "UPDATE prpaypoint_master SET " & _
              " isactive = 0" & _
            " WHERE paypointunkid = @paypointunkid "
            'Sohail (28 Dec 2010) -- End

            objDataOperation.AddParameter("@paypointunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intUnkid)

            Call objDataOperation.ExecNonQuery(strQ)

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            'S.SANDEEP [28-May-2018] -- START
            'ISSUE/ENHANCEMENT : {Audit Trails} 
            Dim objCommonATLog As New clsCommonATLog
            objCommonATLog._FormName = mstrFormName
            objCommonATLog._LoginEmployeeUnkid = mintLoginEmployeeunkid
            objCommonATLog._ClientIP = mstrClientIP
            objCommonATLog._HostName = mstrHostName
            objCommonATLog._FromWeb = mblnIsWeb
            objCommonATLog._AuditUserId = mintAuditUserId
objCommonATLog._CompanyUnkid = mintCompanyUnkid
            objCommonATLog._AuditDate = mdtAuditDate
            'S.SANDEEP [28-May-2018] -- END

            If objCommonATLog.Insert_AtLog(objDataOperation, 3, "prpaypoint_master", "paypointunkid", intUnkid) = False Then
                objDataOperation.ReleaseTransaction(False)
                Return False
            Else
                objDataOperation.ReleaseTransaction(True)
            Return True
            End If

            'S.SANDEEP [28-May-2018] -- START
            'ISSUE/ENHANCEMENT : {Audit Trails} 
            objCommonATLog = Nothing
            'S.SANDEEP [28-May-2018] -- END

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: Update; Module Name: " & mstrModuleName)
            Return False
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
    End Function

    ''' <summary>
    ''' Modify By: Pinkal
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>
    Public Function isUsed(ByVal intUnkid As Integer) As Boolean
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        objDataOperation = New clsDataOperation

        Try
            'Sohail (19 Nov 2010) -- Start
            'strQ = "<Query>"
            strQ = "SELECT  employeeunkid " & _
                   "FROM    hremployee_master " & _
                   "WHERE   isactive = 1 " & _
                            "AND paypointunkid = @paypointunkid "
            'Sohail (19 Nov 2010) -- End

            objDataOperation.AddParameter("@paypointunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intUnkid)

            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            Return dsList.Tables(0).Rows.Count > 0
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: isUsed; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
    End Function

    ''' <summary>
    ''' Modify By: Pinkal
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>
    Public Function isExist(Optional ByVal strCode As String = "", Optional ByVal strName As String = "", Optional ByVal intUnkid As Integer = -1) As Boolean
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        objDataOperation = New clsDataOperation

        Try
            strQ = "SELECT " & _
              "  paypointunkid " & _
              ", paypointalias " & _
              ", paypointcode " & _
              ", paypointname " & _
              ", description " & _
              ", isactive " & _
              ", paypointname1 " & _
              ", paypointname2 " & _
             "FROM prpaypoint_master " & _
             "WHERE 1=1"


            If strCode.Length > 0 Then
                strQ &= " AND paypointcode = @paypointcode "
            End If

            If strName.Length > 0 Then
                strQ &= " AND paypointname = @paypointname"
            End If

            If intUnkid > 0 Then
                strQ &= " AND paypointunkid <> @paypointunkid"
            End If

            objDataOperation.AddParameter("@paypointcode", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, strCode)
            objDataOperation.AddParameter("@paypointname", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, strName)
            objDataOperation.AddParameter("@paypointunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intUnkid)

            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            Return dsList.Tables(0).Rows.Count > 0
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: isExist; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
    End Function

    ''' <summary>
    ''' Modify By: Pinkal
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>
    Public Function getListForCombo(Optional ByVal strListName As String = "List", Optional ByVal mblFlag As Boolean = False) As DataSet
        Dim dsList As New DataSet
        Dim objDataOperation As New clsDataOperation
        Dim strQ As String = String.Empty
        Dim exForce As Exception
        Try
            If mblFlag = True Then
                strQ = "SELECT 0 as paypointunkid, ' ' +  @name  as name UNION "
            End If
            strQ &= "SELECT paypointunkid, paypointname as name FROM prpaypoint_master where isactive = 1 ORDER BY name "

            objDataOperation.AddParameter("@name", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 2, "Select Pay Point"))
            dsList = objDataOperation.ExecQuery(strQ, strListName)
            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If
            Return dsList
        Catch ex As Exception
            DisplayError.Show(-1, ex.Message, "getListForCombo", mstrModuleName)
            Return Nothing
        Finally
            exForce = Nothing
            objDataOperation = Nothing
            dsList.Dispose()
            dsList = Nothing
        End Try

    End Function

    'Nilay (17 Feb 2017) -- Start
    'ISSUE #44: ENHANCEMENT: I-Tax Form B report changes
    Public Function GetPayPointUnkidByCode(ByVal strPayPointCode As String) As Integer
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception
        Dim intPayPointUnkid As Integer = 0
        objDataOperation = New clsDataOperation
        Try
            strQ = " SELECT " & _
                        " paypointunkid " & _
                   " FROM prpaypoint_master " & _
                   " WHERE paypointcode=@paypointcode AND isactive=1"

            objDataOperation.ClearParameters()
            objDataOperation.AddParameter("@paypointcode", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, strPayPointCode)

            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            If dsList.Tables("List").Rows.Count > 0 Then
                intPayPointUnkid = CInt(dsList.Tables("List").Rows(0).Item(0))
            End If

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: GetPayPointUnkidByCode; Module Name: " & mstrModuleName)
        End Try
        Return intPayPointUnkid
    End Function
    'Nilay (17 Feb 2017) -- End


	'<Language> This Auto Generated Text Please Do Not Modify it.
#Region " Language & UI Settings "
	Public Shared Sub SetMessages()
		Try
			Language.setMessage(mstrModuleName, 1, "This Pay Point Code is already defined. Please define new Pay Point Code.")
			Language.setMessage(mstrModuleName, 2, "This Pay Point Name is already defined. Please define new Pay Point Name.")
			Language.setMessage(mstrModuleName, 2, "Select Pay Point")

		Catch Ex As Exception
			DisplayError.Show("-1", ex.Message, "SetMessages", mstrModuleName)
		End Try
	End Sub
#End Region 'Language & UI Settings
	'</Language>
End Class