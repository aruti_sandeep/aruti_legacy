﻿'************************************************************************************************************************************
'Class Name : clsbudget_approver_mapping.vb
'Purpose    :
'Date       :8/8/2016
'Written By :Pinkal
'Modified   :
'************************************************************************************************************************************

Imports eZeeCommonLib
Imports eZee.Common.eZeeForm

''' <summary>
''' Purpose: 
''' Developer: Pinkal
''' </summary>
Public Class clsbudget_approver_mapping
    Private Shared ReadOnly mstrModuleName As String = "clsbudget_approver_mapping"
    Dim objDataOperation As clsDataOperation
    Dim mstrMessage As String = ""

#Region " Private variables "
    Private mintBudgetapproverunkid As Integer
    Private mintLevelunkid As Integer
    Private mintUserapproverunkid As Integer
    Private mintUserunkid As Integer
    Private mblnIsvoid As Boolean
    Private mintVoiduserunkid As Integer
    Private mdtVoiddatetime As Date
    Private mstrVoidreason As String = String.Empty
    'Private mstrWebFormName As String = String.Empty
    'Private mstrWebClientIP As String = String.Empty
    'Private mstrWebHostName As String = String.Empty

#End Region

#Region " Properties "

'S.SANDEEP [28-May-2018] -- START 
'ISSUE/ENHANCEMENT : {Audit Trails} 
Private mstrFormName As String = String.Empty
Public WriteOnly Property _FormName() As String 
Set(ByVal value As String) 
mstrFormName = value 
End Set 
End Property 
 
Private mstrClientIP As String = ""
Public WriteOnly Property _ClientIP() As String 
Set(ByVal value As String) 
mstrClientIP = value 
End Set 
End Property 
 
Private mstrHostName As String = ""
Public WriteOnly Property _HostName() As String 
Set(ByVal value As String) 
mstrHostName = value 
End Set 
End Property 
 
Private mblnIsWeb As Boolean = False
Public WriteOnly Property _FromWeb() As Boolean 
Set(ByVal value As Boolean) 
mblnIsWeb = value 
End Set 
End Property 
 
    Private mintAuditUserId As Integer = 0
    Public WriteOnly Property _AuditUserId() As Integer
        Set(ByVal value As Integer)
            mintAuditUserId = value
        End Set
    End Property

    Private mintCompanyUnkid As Integer = 0
    Public WriteOnly Property _CompanyUnkid() As Integer
        Set(ByVal value As Integer)
            mintCompanyUnkid = value
        End Set
    End Property
 
Private mdtAuditDate As DateTime = Now
Public WriteOnly Property _AuditDate() As DateTime 
Set(ByVal value As DateTime) 
mdtAuditDate = value 
End Set 
End Property 
 
Private xDataOpr As clsDataOperation = Nothing
Public Property _xDataOpr() As clsDataOperation 
Get 
Return xDataOpr 
End Get 
Set(ByVal value As clsDataOperation) 
xDataOpr = value 
End Set 
End Property 
 
Private mintLoginEmployeeunkid As Integer = 0
Public Property _LoginEmployeeunkid() As Integer 
Get 
Return mintLoginEmployeeunkid 
End Get 
Set(ByVal value As Integer) 
mintLoginEmployeeunkid = value 
End Set 
End Property 
 
Private mintVoidlogingemployeeunkid As Integer=0
Public Property _Voidloginemployeeunkid() As Integer 
Get 
Return mintVoidlogingemployeeunkid 
End Get 
Set(ByVal value As Integer) 
mintVoidlogingemployeeunkid = value 
End Set 
End Property 
'S.SANDEEP [28-May-2018] -- END 

    Public ReadOnly Property _Message() As String
        Get
            Return mstrMessage
        End Get
    End Property

    ''' <summary>
    ''' Purpose: Get or Set budgetapproverunkid
    ''' Modify By: Pinkal
    ''' </summary>
    Public Property _Budgetapproverunkid() As Integer
        Get
            Return mintBudgetapproverunkid
        End Get
        Set(ByVal value As Integer)
            mintBudgetapproverunkid = Value
            Call getData()
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set levelunkid
    ''' Modify By: Pinkal
    ''' </summary>
    Public Property _Levelunkid() As Integer
        Get
            Return mintLevelunkid
        End Get
        Set(ByVal value As Integer)
            mintLevelunkid = Value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set userapproverunkid
    ''' Modify By: Pinkal
    ''' </summary>
    Public Property _Userapproverunkid() As Integer
        Get
            Return mintUserapproverunkid
        End Get
        Set(ByVal value As Integer)
            mintUserapproverunkid = Value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set userunkid
    ''' Modify By: Pinkal
    ''' </summary>
    Public Property _Userunkid() As Integer
        Get
            Return mintUserunkid
        End Get
        Set(ByVal value As Integer)
            mintUserunkid = Value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set isvoid
    ''' Modify By: Pinkal
    ''' </summary>
    Public Property _Isvoid() As Boolean
        Get
            Return mblnIsvoid
        End Get
        Set(ByVal value As Boolean)
            mblnIsvoid = Value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set voiduserunkid
    ''' Modify By: Pinkal
    ''' </summary>
    Public Property _Voiduserunkid() As Integer
        Get
            Return mintVoiduserunkid
        End Get
        Set(ByVal value As Integer)
            mintVoiduserunkid = Value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set voiddatetime
    ''' Modify By: Pinkal
    ''' </summary>
    Public Property _Voiddatetime() As Date
        Get
            Return mdtVoiddatetime
        End Get
        Set(ByVal value As Date)
            mdtVoiddatetime = Value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set voidreason
    ''' Modify By: Pinkal
    ''' </summary>
    Public Property _Voidreason() As String
        Get
            Return mstrVoidreason
        End Get
        Set(ByVal value As String)
            mstrVoidreason = Value
        End Set
    End Property

#End Region

    ''' <summary>
    ''' Modify By: Pinkal
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>
    Public Sub GetData()
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        objDataOperation = New clsDataOperation

        Try
            strQ = "SELECT " & _
                      "  budgetapproverunkid " & _
                      ", levelunkid " & _
                      ", userapproverunkid " & _
                      ", userunkid " & _
                      ", isvoid " & _
                      ", voiduserunkid " & _
                      ", voiddatetime " & _
                      ", voidreason " & _
                     "FROM bgbudget_approver_mapping " & _
                     "WHERE budgetapproverunkid = @budgetapproverunkid  "

            objDataOperation.AddParameter("@budgetapproverunkid", SqlDbType.int, eZeeDataType.INT_SIZE, mintBudgetapproverUnkId.ToString)

            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            For Each dtRow As DataRow In dsList.Tables(0).Rows
                mintbudgetapproverunkid = CInt(dtRow.Item("budgetapproverunkid"))
                mintlevelunkid = CInt(dtRow.Item("levelunkid"))
                mintuserapproverunkid = CInt(dtRow.Item("userapproverunkid"))
                mintuserunkid = CInt(dtRow.Item("userunkid"))
                mblnisvoid = CBool(dtRow.Item("isvoid"))
                mintvoiduserunkid = CInt(dtRow.Item("voiduserunkid"))
                If Not IsDBNull(dtRow.Item("voiddatetime")) Then mdtVoiddatetime = dtRow.Item("voiddatetime")
                mstrvoidreason = dtRow.Item("voidreason").ToString
                Exit For
            Next
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: GetData; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
    End Sub

    ''' <summary>
    ''' Modify By: Pinkal
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>
    Public Function GetList(ByVal strTableName As String, Optional ByVal intApproverUserUnkId As Integer = 0 _
                            , Optional ByVal strFilter As String = "") As DataSet

        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        Dim objDataOperation As New clsDataOperation

        Try
            strQ = "SELECT " & _
                      "  bgbudget_approver_mapping.budgetapproverunkid " & _
                      ", bgbudget_approver_mapping.levelunkid " & _
                      ", bgbudgetapproverlevel_master.levelcode " & _
                      ", bgbudgetapproverlevel_master.levelname  " & _
                      ", bgbudgetapproverlevel_master.levelname1 " & _
                      ", bgbudgetapproverlevel_master.levelname2 " & _
                      ", bgbudgetapproverlevel_master.priority " & _
                      ", bgbudget_approver_mapping.userapproverunkid " & _
                      ", bgbudget_approver_mapping.userunkid " & _
                      ", hrmsConfiguration..cfuser_master.username AS approver " & _
                      ", bgbudget_approver_mapping.isvoid " & _
                      ", bgbudget_approver_mapping.voiduserunkid " & _
                      ", bgbudget_approver_mapping.voiddatetime " & _
                      ", bgbudget_approver_mapping.voidreason " & _
                      " FROM bgbudget_approver_mapping " & _
                      " JOIN hrmsConfiguration..cfuser_master ON bgbudget_approver_mapping.userapproverunkid = hrmsConfiguration..cfuser_master.userunkid " & _
                      " LEFT JOIN bgbudgetapproverlevel_master ON bgbudget_approver_mapping.levelunkid = bgbudgetapproverlevel_master.levelunkid " & _
                      " WHERE bgbudget_approver_mapping.isvoid = 0 " & _
                      " AND bgbudgetapproverlevel_master.isvoid = 0 "


            If intApproverUserUnkId > 0 Then
                strQ &= " AND bgbudget_approver_mapping.userapproverunkid = @userapproverunkid "
                objDataOperation.AddParameter("@userapproverunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intApproverUserUnkId)
            End If

            If strFilter.Trim <> "" Then
                strQ &= " AND " & strFilter
            End If

            dsList = objDataOperation.ExecQuery(strQ, strTableName)

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: GetList; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
        Return dsList
    End Function

    ''' <summary>
    ''' Modify By: Pinkal
    ''' </summary>
    ''' <returns>Boolean</returns>
    ''' <purpose> INSERT INTO Database Table (bgbudget_approver_mapping) </purpose>
    Public Function Insert() As Boolean
        If isExist(mintUserapproverunkid, -1) Then
            mstrMessage = Language.getMessage(mstrModuleName, 1, "Sorry, This approver already mapped with some level.")
            Return False
        End If

        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        objDataOperation = New clsDataOperation
        objDataOperation.BindTransaction()

        Try
            objDataOperation.ClearParameters()
            objDataOperation.AddParameter("@levelunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintLevelunkid.ToString)
            objDataOperation.AddParameter("@userapproverunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintUserapproverunkid.ToString)
            objDataOperation.AddParameter("@userunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintUserunkid.ToString)
            objDataOperation.AddParameter("@isvoid", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsvoid.ToString)
            objDataOperation.AddParameter("@voiduserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintVoiduserunkid.ToString)
            objDataOperation.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, IIf(mdtVoiddatetime <> Nothing, mdtVoiddatetime, DBNull.Value))
            objDataOperation.AddParameter("@voidreason", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrVoidreason.ToString)

            strQ = "INSERT INTO bgbudget_approver_mapping ( " & _
                      "  levelunkid " & _
                      ", userapproverunkid " & _
                      ", userunkid " & _
                      ", isvoid " & _
                      ", voiduserunkid " & _
                      ", voiddatetime " & _
                      ", voidreason" & _
                    ") VALUES (" & _
                      "  @levelunkid " & _
                      ", @userapproverunkid " & _
                      ", @userunkid " & _
                      ", @isvoid " & _
                      ", @voiduserunkid " & _
                      ", @voiddatetime " & _
                      ", @voidreason" & _
                    "); SELECT @@identity"

            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            mintBudgetapproverunkid = dsList.Tables(0).Rows(0).Item(0)

            If ATInsertApproverLevel_Mapping(objDataOperation, 1) = False Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            objDataOperation.ReleaseTransaction(True)

            Return True
        Catch ex As Exception
            objDataOperation.ReleaseTransaction(False)
            Throw New Exception(ex.Message & "; Procedure Name: Insert; Module Name: " & mstrModuleName)
            Return False
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
    End Function

    ''' <summary>
    ''' Modify By: Pinkal
    ''' </summary>
    ''' <returns>Boolean</returns>
    ''' <purpose> Update Database Table (bgbudget_approver_mapping) </purpose>
    Public Function Update() As Boolean
        If isExist(mintUserapproverunkid, mintBudgetapproverunkid) Then
            mstrMessage = Language.getMessage(mstrModuleName, 1, "Sorry, This approver already mapped with some level.")
            Return False
        End If

        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        objDataOperation = New clsDataOperation
        objDataOperation.BindTransaction()

        Try
            objDataOperation.ClearParameters()
            objDataOperation.AddParameter("@budgetapproverunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintBudgetapproverunkid.ToString)
            objDataOperation.AddParameter("@levelunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintLevelunkid.ToString)
            objDataOperation.AddParameter("@userapproverunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintUserapproverunkid.ToString)
            objDataOperation.AddParameter("@userunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintUserunkid.ToString)
            objDataOperation.AddParameter("@isvoid", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsvoid.ToString)
            objDataOperation.AddParameter("@voiduserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintVoiduserunkid.ToString)
            objDataOperation.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, IIf(mdtVoiddatetime <> Nothing, mdtVoiddatetime, DBNull.Value))
            objDataOperation.AddParameter("@voidreason", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrVoidreason.ToString)

            strQ = "UPDATE bgbudget_approver_mapping SET " & _
                      "  levelunkid = @levelunkid" & _
                      ", userapproverunkid = @userapproverunkid" & _
                      ", userunkid = @userunkid" & _
                      ", isvoid = @isvoid" & _
                      ", voiduserunkid = @voiduserunkid" & _
                      ", voiddatetime = @voiddatetime" & _
                      ", voidreason = @voidreason " & _
                    "WHERE budgetapproverunkid = @budgetapproverunkid "

            Call objDataOperation.ExecNonQuery(strQ)

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            If ATInsertApproverLevel_Mapping(objDataOperation, 2) = False Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            objDataOperation.ReleaseTransaction(True)

            Return True
        Catch ex As Exception
            objDataOperation.ReleaseTransaction(False)
            Throw New Exception(ex.Message & "; Procedure Name: Update; Module Name: " & mstrModuleName)
            Return False
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
    End Function

    ''' <summary>
    ''' Modify By: Pinkal
    ''' </summary>
    ''' <returns>Boolean</returns>
    ''' <purpose> Delete Database Table (bgbudget_approver_mapping) </purpose>
    Public Function Delete(ByVal intUnkid As Integer) As Boolean
        'If isUsed(intUnkid) Then
        '    mstrMessage = "<Message>"
        '    Return False
        'End If

        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        objDataOperation = New clsDataOperation
        objDataOperation.BindTransaction()

        Try
            strQ = "UPDATE bgbudget_approver_mapping SET " & _
                     " isvoid = 1" & _
                     ", voiduserunkid = @voiduserunkid" & _
                     ", voiddatetime = GetDate()" & _
                     ", voidreason = @voidreason " & _
                     " WHERE budgetapproverunkid = @budgetapproverunkid "

            objDataOperation.ClearParameters()
            objDataOperation.AddParameter("@budgetapproverunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intUnkid)
            objDataOperation.AddParameter("@voiduserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintVoiduserunkid)
            objDataOperation.AddParameter("@voidreason", SqlDbType.NVarChar, eZeeDataType.DESC_SIZE, mstrVoidreason)

            Call objDataOperation.ExecNonQuery(strQ)

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            If ATInsertApproverLevel_Mapping(objDataOperation, 3) = False Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            objDataOperation.ReleaseTransaction(True)

            Return True
        Catch ex As Exception
            objDataOperation.ReleaseTransaction(False)
            Throw New Exception(ex.Message & "; Procedure Name: Delete; Module Name: " & mstrModuleName)
            Return False
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
    End Function

    ''' <summary>
    ''' Modify By: Pinkal
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>
    Public Function isUsed(ByVal intUnkid As Integer) As Boolean
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        objDataOperation = New clsDataOperation

        Try
            StrQ = "<Query>"

            objDataOperation.AddParameter("@budgetapproverunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intUnkid)

            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            Return dsList.tables(0).rows.count > 0
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: isUsed; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
    End Function

    ''' <summary>
    ''' Modify By: Pinkal
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>
    Public Function isExist(ByVal intUserApproverID As Integer, Optional ByVal intUnkid As Integer = -1) As Boolean
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        Dim objDataOperation As New clsDataOperation

        Try
            strQ = "SELECT " & _
                      "  budgetapproverunkid " & _
                      ", levelunkid " & _
                      ", userapproverunkid " & _
                      ", userunkid " & _
                      ", isvoid " & _
                      ", voiduserunkid " & _
                      ", voiddatetime " & _
                      ", voidreason " & _
                      " FROM bgbudget_approver_mapping " & _
                      " WHERE isvoid = 0 " & _
                      " AND userapproverunkid = @userapproverunkid"

            If intUnkid > 0 Then
                strQ &= " AND budgetapproverunkid <> @budgetapproverunkid"
            End If

            objDataOperation.ClearParameters()
            objDataOperation.AddParameter("@userapproverunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intUserApproverID)
            objDataOperation.AddParameter("@budgetapproverunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intUnkid)

            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            Return dsList.Tables(0).Rows.Count > 0
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: isExist; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
    End Function

    Public Function GetApproverUnkIDs(Optional ByVal intPriority As Integer = -1 _
                                      , Optional ByVal intLevelUnkId As Integer = 0 _
                                      , Optional ByVal strLevelCode As String = "" _
                                      ) As String

        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        objDataOperation = New clsDataOperation
        objDataOperation.ClearParameters()

        Try

            strQ = "SELECT  ISNULL(STUFF(( SELECT   ',' + CAST(userapproverunkid AS NVARCHAR(50)) " & _
                                           "FROM     bgbudget_approver_mapping " & _
                                                    "LEFT JOIN bgbudgetapproverlevel_master ON bgbudgetapproverlevel_master.levelunkid = bgbudget_approver_mapping.levelunkid " & _
                                           "WHERE   bgbudget_approver_mapping.isvoid = 0 " & _
                                                    "AND bgbudgetapproverlevel_master.isvoid = 0 "


            If intPriority > -1 Then
                strQ &= " AND bgbudgetapproverlevel_master.priority = @priority "
                objDataOperation.AddParameter("@priority", SqlDbType.Int, eZeeDataType.INT_SIZE, intPriority)
            End If

            If intLevelUnkId > 0 Then
                strQ &= " AND bgbudget_approver_mapping.levelunkid  = @levelunkid "
                objDataOperation.AddParameter("@levelunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intLevelUnkId)
            End If

            If strLevelCode.Trim <> "" Then
                strQ &= " AND bgbudgetapproverlevel_master.levelcode = @levelcode "
                objDataOperation.AddParameter("@levelcode", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, strLevelCode)
            End If

            strQ &= "                   FOR " & _
                                           "XML PATH('') " & _
                                         "), 1, 1, ''), '') AS ApproverIDs "


            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            Return dsList.Tables(0).Rows(0)("ApproverIDs")

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: GetApproverUnkIDs; Module Name: " & mstrModuleName)
        End Try
        Return ""
    End Function

    Public Function GetLowerApproverUnkIDs(ByVal intCurrLevelPriority As Integer _
                                           ) As String

        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        objDataOperation = New clsDataOperation
        objDataOperation.ClearParameters()

        Try

            strQ = "SELECT  ISNULL(STUFF(( SELECT   ',' + CAST(userapproverunkid AS NVARCHAR(50)) " & _
                                           "FROM     bgbudget_approver_mapping " & _
                                                    "LEFT JOIN bgbudgetapproverlevel_master ON bgbudgetapproverlevel_master.levelunkid = bgbudget_approver_mapping.levelunkid " & _
                                           "WHERE    ISNULL(bgbudget_approver_mapping.isvoid, 0) = 0 " & _
                                                    "AND ISNULL(bgbudgetapproverlevel_master.isvoid, 0) = 0 " & _
                                                    "AND bgbudgetapproverlevel_master.priority < @priority "

            strQ &= "                   FOR " & _
                                           "XML PATH('') " & _
                                         "), 1, 1, ''), '') AS ApproverIDs "


            objDataOperation.AddParameter("@priority", SqlDbType.Int, eZeeDataType.INT_SIZE, intCurrLevelPriority)

            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            Return dsList.Tables(0).Rows(0)("ApproverIDs")

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: GetLowerApproverUnkIDs; Module Name: " & mstrModuleName)
        End Try
        Return ""
    End Function

    Public Function GetCurrentLevelPriority(ByVal intCurrLevelPriority As Integer _
                                            ) As String

        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        objDataOperation = New clsDataOperation
        objDataOperation.ClearParameters()

        Try

            strQ = "SELECT  ISNULL(STUFF(( SELECT   ',' + CAST(userapproverunkid AS NVARCHAR(50)) " & _
                                           "FROM     bgbudget_approver_mapping " & _
                                                    "LEFT JOIN bgbudgetapproverlevel_master ON bgbudgetapproverlevel_master.levelunkid = bgbudget_approver_mapping.levelunkid " & _
                                           "WHERE    ISNULL(bgbudget_approver_mapping.isvoid, 0) = 0 " & _
                                                    "AND ISNULL(bgbudgetapproverlevel_master.isvoid, 0) = 0 " & _
                                                    "AND bgbudgetapproverlevel_master.priority = @priority "

            strQ &= "                   FOR " & _
                                           "XML PATH('') " & _
                                         "), 1, 1, ''), '') AS ApproverIDs "


            objDataOperation.AddParameter("@priority", SqlDbType.Int, eZeeDataType.INT_SIZE, intCurrLevelPriority)

            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            Return dsList.Tables(0).Rows(0)("ApproverIDs")

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: GetCurrentLevelPriority; Module Name: " & mstrModuleName)
        End Try
        Return ""
    End Function

    ''' <summary>
    ''' Modify By: Pinkal
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>
    Private Function ATInsertApproverLevel_Mapping(ByVal xDataOpr As clsDataOperation, ByVal xAuditType As enAuditType) As Boolean
        Dim strQ As String = ""
        Dim strErrorMessage As String = ""
        Dim exForce As Exception = Nothing
        Try
            strQ = "INSERT INTO atbgbudget_approver_mapping ( " & _
                     "  budgetapproverunkid " & _
                     ", levelunkid " & _
                     ", userapproverunkid " & _
                     ", audittype " & _
                     ", audituserunkid " & _
                     ", auditdatetime " & _
                     ", ip " & _
                     ", machine_name " & _
                     ", form_name " & _
                     " " & _
                     " " & _
                     " " & _
                     " " & _
                     " " & _
                     ", isweb " & _
                 ") VALUES (" & _
                     "  @budgetapproverunkid " & _
                     ", @levelunkid " & _
                     ", @userapproverunkid " & _
                     ", @audittype " & _
                     ", @audituserunkid " & _
                     ", GetDate() " & _
                     ", @ip " & _
                     ", @machine_name " & _
                     ", @form_name " & _
                     " " & _
                     " " & _
                     " " & _
                     " " & _
                     " " & _
                     ", @isweb " & _
                 ") "


            xDataOpr.ClearParameters()
            xDataOpr.AddParameter("@budgetapproverunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintBudgetapproverunkid.ToString)
            xDataOpr.AddParameter("@levelunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintLevelunkid.ToString)
            xDataOpr.AddParameter("@userapproverunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintUserapproverunkid.ToString)
            xDataOpr.AddParameter("@userunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintUserunkid.ToString)
            xDataOpr.AddParameter("@audittype", SqlDbType.Int, eZeeDataType.INT_SIZE, xAuditType)
            xDataOpr.AddParameter("@audituserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintAuditUserId)
            xDataOpr.AddParameter("@ip", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrClientIP)
            xDataOpr.AddParameter("@machine_name", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrHostName)
            xDataOpr.AddParameter("@form_name", SqlDbType.NVarChar, 500, mstrFormName)
            xDataOpr.AddParameter("@isweb", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsWeb)

            xDataOpr.ExecNonQuery(strQ)

            If xDataOpr.ErrorMessage <> "" Then
                exForce = New Exception(xDataOpr.ErrorNumber & ": " & xDataOpr.ErrorMessage)
                Throw exForce
            End If

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: ATInsertApproverLevel_Mapping; Module Name: " & mstrModuleName)
            Return False
        Finally
        End Try
        Return True
    End Function

    '<Language> This Auto Generated Text Please Do Not Modify it.
#Region " Language & UI Settings "
    Public Shared Sub SetMessages()
        Try
            Language.setMessage(mstrModuleName, 1, "Sorry, This approver already mapped with some level.")
            Language.setMessage(mstrModuleName, 4, "WEB")

        Catch Ex As Exception
            DisplayError.Show("-1", Ex.Message, "SetMessages", mstrModuleName)
        End Try
    End Sub
#End Region 'Language & UI Settings
    '</Language>
End Class