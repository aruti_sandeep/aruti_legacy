﻿'************************************************************************************************************************************
'Class Name : clsEmpSalaryTran.vb
'Purpose    :
'Date       :26/10/2010
'Written By :Sohail
'Modified   :
'************************************************************************************************************************************

Imports eZeeCommonLib
'S.SANDEEP [ 11 AUG 2012 ] -- START
'ENHANCEMENT : TRA CHANGES
Imports eZee.Common.eZeeForm
'S.SANDEEP [ 11 AUG 2012 ] -- END

''' <summary>
''' Purpose: 
''' Developer: Sohail
''' </summary>
Public Class clsEmpSalaryTran
    Private Shared ReadOnly mstrModuleName As String = "clsEmpSalaryTran"
    Dim objDataOperation As clsDataOperation
    Dim mstrMessage As String = ""

#Region " Private variables "
    Private mintEmpsalarytranunkid As Integer
    Private mintPaymenttranunkid As Integer
    Private mdtPaymentdate As Date
    Private mintEmployeeunkid As Integer
    Private mintEmpbanktranid As Integer
    Private mdecAmount As Decimal 'Sohail (11 May 2011)
    Private mintUserunkid As Integer
    Private mblnIsvoid As Boolean
    Private mintVoiduserunkid As Integer
    Private mdtVoiddatetime As Date
    Private mstrVoidreason As String = String.Empty

    'Sohail (25 May 2012) -- Start
    'TRA - ENHANCEMENT
    Private mintBasecurrencyid As Integer
    Private mdecBaseexchangerate As Decimal
    Private mintPaidcurrencyid As Integer
    Private mdecExpaidrate As Decimal
    Private mdecExpaidamt As Decimal
    'Sohail (25 May 2012) -- End
    'Hemant (07 July 2023) -- Start
    'Enhancement : A1X-961 : Mobile money account numbers - Create a new screen to capture mobile money account numbers.
    Private mintPayment_typeid As Integer
    Private mstrMobileno As String
    'Hemant (07 July 2023) -- End
    'S.SANDEEP [ 19 JULY 2012 ] -- START
    'Enhancement : TRA Changes
    Private mstrWebFormName As String = String.Empty
    'S.SANDEEP [ 19 JULY 2012 ] -- END

    'Sohail (18 May 2013) -- Start
    'TRA - ENHANCEMENT
    Private mstrWebIP As String = ""
    Private mstrWebhostName As String = ""
    'Sohail (18 May 2013) -- End


    'S.SANDEEP [ 13 AUG 2012 ] -- START
    'ENHANCEMENT : TRA CHANGES
    Private mintLogEmployeeUnkid As Integer = -1
    'S.SANDEEP [ 13 AUG 2012 ] -- END

#End Region

#Region " Properties "

    'S.SANDEEP [28-May-2018] -- START 
    'ISSUE/ENHANCEMENT : {Audit Trails} 
    Private mstrFormName As String = String.Empty
    Public WriteOnly Property _FormName() As String
        Set(ByVal value As String)
            mstrFormName = value
        End Set
    End Property

    Private mstrClientIP As String = ""
    Public WriteOnly Property _ClientIP() As String
        Set(ByVal value As String)
            mstrClientIP = value
        End Set
    End Property

    Private mstrHostName As String = ""
    Public WriteOnly Property _HostName() As String
        Set(ByVal value As String)
            mstrHostName = value
        End Set
    End Property

    Private mblnIsWeb As Boolean = False
    Public WriteOnly Property _FromWeb() As Boolean
        Set(ByVal value As Boolean)
            mblnIsWeb = value
        End Set
    End Property

    Private mintAuditUserId As Integer = 0
    Public WriteOnly Property _AuditUserId() As Integer
        Set(ByVal value As Integer)
            mintAuditUserId = value
        End Set
    End Property

    Private mintCompanyUnkid As Integer = 0
    Public WriteOnly Property _CompanyUnkid() As Integer
        Set(ByVal value As Integer)
            mintCompanyUnkid = value
        End Set
    End Property

    Private mdtAuditDate As DateTime = Now
    Public WriteOnly Property _AuditDate() As DateTime
        Set(ByVal value As DateTime)
            mdtAuditDate = value
        End Set
    End Property

    Private xDataOpr As clsDataOperation = Nothing
    Public Property _xDataOpr() As clsDataOperation
        Get
            Return xDataOpr
        End Get
        Set(ByVal value As clsDataOperation)
            xDataOpr = value
        End Set
    End Property

    Private mintVoidlogingemployeeunkid As Integer = 0
    Public Property _Voidloginemployeeunkid() As Integer
        Get
            Return mintVoidlogingemployeeunkid
        End Get
        Set(ByVal value As Integer)
            mintVoidlogingemployeeunkid = value
        End Set
    End Property
    'S.SANDEEP [28-May-2018] -- END 

    Public ReadOnly Property _Message() As String
        Get
            Return mstrMessage
        End Get
    End Property

    ''' <summary>
    ''' Purpose: Get or Set empsalarytranunkid
    ''' Modify By: Sohail
    ''' </summary>
    Public Property _Empsalarytranunkid() As Integer
        Get
            Return mintEmpsalarytranunkid
        End Get
        Set(ByVal value As Integer)
            mintEmpsalarytranunkid = Value
            Call getData()
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set paymenttranunkid
    ''' Modify By: Sohail
    ''' </summary>
    Public Property _Paymenttranunkid() As Integer
        Get
            Return mintPaymenttranunkid
        End Get
        Set(ByVal value As Integer)
            mintPaymenttranunkid = Value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set paymentdate
    ''' Modify By: Sohail
    ''' </summary>
    Public Property _Paymentdate() As Date
        Get
            Return mdtPaymentdate
        End Get
        Set(ByVal value As Date)
            mdtPaymentdate = Value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set employeeunkid
    ''' Modify By: Sohail
    ''' </summary>
    Public Property _Employeeunkid() As Integer
        Get
            Return mintEmployeeunkid
        End Get
        Set(ByVal value As Integer)
            mintEmployeeunkid = Value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set empbanktranid
    ''' Modify By: Sohail
    ''' </summary>
    Public Property _Empbanktranid() As Integer
        Get
            Return mintEmpbanktranid
        End Get
        Set(ByVal value As Integer)
            mintEmpbanktranid = Value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set amount
    ''' Modify By: Sohail
    ''' </summary>
    Public Property _Amount() As Decimal 'Sohail (11 May 2011)
        Get
            Return mdecAmount
        End Get
        Set(ByVal value As Decimal) 'Sohail (11 May 2011)
            mdecAmount = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set userunkid
    ''' Modify By: Sohail
    ''' </summary>
    Public Property _Userunkid() As Integer
        Get
            Return mintUserunkid
        End Get
        Set(ByVal value As Integer)
            mintUserunkid = Value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set isvoid
    ''' Modify By: Sohail
    ''' </summary>
    Public Property _Isvoid() As Boolean
        Get
            Return mblnIsvoid
        End Get
        Set(ByVal value As Boolean)
            mblnIsvoid = Value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set voiduserunkid
    ''' Modify By: Sohail
    ''' </summary>
    Public Property _Voiduserunkid() As Integer
        Get
            Return mintVoiduserunkid
        End Get
        Set(ByVal value As Integer)
            mintVoiduserunkid = Value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set voiddatetime
    ''' Modify By: Sohail
    ''' </summary>
    Public Property _Voiddatetime() As Date
        Get
            Return mdtVoiddatetime
        End Get
        Set(ByVal value As Date)
            mdtVoiddatetime = Value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set voidreason
    ''' Modify By: Sohail
    ''' </summary>
    Public Property _Voidreason() As String
        Get
            Return mstrVoidreason
        End Get
        Set(ByVal value As String)
            mstrVoidreason = Value
        End Set
    End Property

    'Sohail (25 May 2012) -- Start
    'TRA - ENHANCEMENT
    Public Property _Basecurrencyid() As Integer
        Get
            Return mintBasecurrencyid
        End Get
        Set(ByVal value As Integer)
            mintBasecurrencyid = value
        End Set
    End Property

    Public Property _Baseexchangerate() As Decimal
        Get
            Return mdecBaseexchangerate
        End Get
        Set(ByVal value As Decimal)
            mdecBaseexchangerate = value
        End Set
    End Property

    Public Property _Paidcurrencyid() As Integer
        Get
            Return mintPaidcurrencyid
        End Get
        Set(ByVal value As Integer)
            mintPaidcurrencyid = value
        End Set
    End Property

    Public Property _Expaidrate() As Decimal
        Get
            Return mdecExpaidrate
        End Get
        Set(ByVal value As Decimal)
            mdecExpaidrate = value
        End Set
    End Property

    Public Property _Expaidamt() As Decimal
        Get
            Return mdecExpaidamt
        End Get
        Set(ByVal value As Decimal)
            mdecExpaidamt = value
        End Set
    End Property
    'Sohail (25 May 2012) -- End

    'S.SANDEEP [ 19 JULY 2012 ] -- START
    'Enhancement : TRA Changes

    'Public Property _WebFormName() As String
    '    Get
    '        Return mstrWebFormName
    '    End Get
    '    Set(ByVal value As String)
    '        mstrWebFormName = value
    '    End Set
    'End Property

    'S.SANDEEP [ 19 JULY 2012 ] -- END

    'Public WriteOnly Property _WebIP() As String
    '    Set(ByVal value As String)
    '        mstrWebIP = value
    '    End Set
    'End Property

    'Public WriteOnly Property _WebHostName() As String
    '    Set(ByVal value As String)
    '        mstrWebhostName = value
    '    End Set
    'End Property

    'S.SANDEEP [ 13 AUG 2012 ] -- START
    'ENHANCEMENT : TRA CHANGES
    Public WriteOnly Property _LoginEmployeeUnkid() As Integer
        Set(ByVal value As Integer)
            mintLogEmployeeUnkid = value
        End Set
    End Property
    'S.SANDEEP [ 13 AUG 2012 ] -- END
    'Hemant (07 July 2023) -- Start
    'Enhancement : A1X-961 : Mobile money account numbers - Create a new screen to capture mobile money account numbers.
    Public Property _Payment_typeid() As Integer
        Get
            Return mintPayment_typeid
        End Get
        Set(ByVal value As Integer)
            mintPayment_typeid = value
        End Set
    End Property

    Public Property _Mobileno() As String
        Get
            Return mstrMobileno
        End Get
        Set(ByVal value As String)
            mstrMobileno = value
        End Set
    End Property
    'Hemant (07 July 2023) -- End
#End Region

    ''' <summary>
    ''' Modify By: Sohail
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>
    Public Sub GetData()
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        objDataOperation = New clsDataOperation

        Try
            strQ = "SELECT " & _
              "  empsalarytranunkid " & _
              ", paymenttranunkid " & _
              ", paymentdate " & _
              ", employeeunkid " & _
              ", empbanktranid " & _
              ", amount " & _
              ", userunkid " & _
              ", isvoid " & _
              ", voiduserunkid " & _
              ", voiddatetime " & _
              ", voidreason " & _
              ", basecurrencyid " & _
              ", baseexchangerate " & _
              ", paidcurrencyid " & _
              ", expaidrate " & _
              ", expaidamt " & _
              ", payment_typeid " & _
              ", mobileno " & _
             "FROM prempsalary_tran " & _
             "WHERE empsalarytranunkid = @empsalarytranunkid "
            'Hemant (07 July 2023) -- [payment_typeid,mobileno]
            'Sohail (25 May 2012) - [basecurrencyid,baseexchangerate,paidcurrencyid,expaidrate,expaidamt]


            objDataOperation.AddParameter("@empsalarytranunkid", SqlDbType.int, eZeeDataType.INT_SIZE, mintEmpsalaryTranUnkId.ToString)

            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            For Each dtRow As DataRow In dsList.Tables(0).Rows
                mintempsalarytranunkid = CInt(dtRow.Item("empsalarytranunkid"))
                mintpaymenttranunkid = CInt(dtRow.Item("paymenttranunkid"))
                mdtpaymentdate = dtRow.Item("paymentdate")
                mintemployeeunkid = CInt(dtRow.Item("employeeunkid"))
                mintempbanktranid = CInt(dtRow.Item("empbanktranid"))
                mdecAmount = CDec(dtRow.Item("amount"))
                mintuserunkid = CInt(dtRow.Item("userunkid"))
                mblnisvoid = CBool(dtRow.Item("isvoid"))
                mintvoiduserunkid = CInt(dtRow.Item("voiduserunkid"))
                If IsDBNull(dtRow.Item("voiddatetime")) = True Then
                    mdtVoiddatetime = Nothing
                Else
                mdtvoiddatetime = dtRow.Item("voiddatetime")
                End If
                mstrVoidreason = dtRow.Item("voidreason").ToString
                'Sohail (25 May 2012) -- Start
                'TRA - ENHANCEMENT
                mintBasecurrencyid = CInt(dtRow.Item("basecurrencyid"))
                mdecBaseexchangerate = CDec(dtRow.Item("baseexchangerate"))
                mintPaidcurrencyid = CInt(dtRow.Item("paidcurrencyid"))
                mdecExpaidrate = CDec(dtRow.Item("expaidrate"))
                mdecExpaidamt = CDec(dtRow.Item("expaidamt"))
                'Sohail (25 May 2012) -- End
                'Hemant (07 July 2023) -- Start
                'Enhancement : A1X-961 : Mobile money account numbers - Create a new screen to capture mobile money account numbers.
                mintPayment_typeid = CInt(dtRow.Item("payment_typeid"))
                mstrMobileno = dtRow.Item("mobileno").ToString
                'Hemant (07 July 2023) -- End
                Exit For
            Next
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: GetData; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
    End Sub


    ''' <summary>
    ''' Modify By: Sohail
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>
    Public Function GetList(ByVal strTableName As String) As DataSet 'Sohail (16 Oct 2010)
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        objDataOperation = New clsDataOperation

        Try
            strQ = "SELECT " & _
              "  empsalarytranunkid " & _
              ", paymenttranunkid " & _
              ", paymentdate " & _
              ", employeeunkid " & _
              ", empbanktranid " & _
              ", amount " & _
              ", userunkid " & _
              ", isvoid " & _
              ", voiduserunkid " & _
              ", voiddatetime " & _
              ", voidreason " & _
              ", basecurrencyid " & _
              ", baseexchangerate " & _
              ", paidcurrencyid " & _
              ", expaidrate " & _
              ", expaidamt " & _
              ", payment_typeid " & _
              ", mobileno " & _
             "FROM prempsalary_tran " & _
             "WHERE ISNULL(isvoid,0) = 0 "
            'Hemant (07 July 2023) -- [payment_typeid,mobileno]
            'Sohail (03 Nov 2010), 'Sohail (25 May 2012) - [basecurrencyid,baseexchangerate,paidcurrencyid,expaidrate,expaidamt]


            dsList = objDataOperation.ExecQuery(strQ, strTableName)

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: GetList; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
        Return dsList
    End Function


    ''' <summary>
    ''' Modify By: Sohail
    ''' </summary>
    ''' <returns>Boolean</returns>
    ''' <purpose> INSERT INTO Database Table (prempsalary_tran) </purpose>
    Public Function Insert() As Boolean
        'If isExist(mstrName) Then
        '    mstrMessage = "<Message>"
        '    Return False
        'End If

        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        objDataOperation = New clsDataOperation

        Try
            objDataOperation.AddParameter("@paymenttranunkid", SqlDbType.int, eZeeDataType.INT_SIZE, mintpaymenttranunkid.ToString)
            objDataOperation.AddParameter("@paymentdate", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, mdtPaymentdate)
            objDataOperation.AddParameter("@employeeunkid", SqlDbType.int, eZeeDataType.INT_SIZE, mintemployeeunkid.ToString)
            objDataOperation.AddParameter("@empbanktranid", SqlDbType.int, eZeeDataType.INT_SIZE, mintempbanktranid.ToString)
            objDataOperation.AddParameter("@amount", SqlDbType.Decimal, eZeeDataType.DECIMAL_SIZE, mdecAmount.ToString) 'Sohail (11 May 2011)
            objDataOperation.AddParameter("@userunkid", SqlDbType.int, eZeeDataType.INT_SIZE, mintuserunkid.ToString)
            objDataOperation.AddParameter("@isvoid", SqlDbType.bit, eZeeDataType.BIT_SIZE, mblnisvoid.ToString)
            objDataOperation.AddParameter("@voiduserunkid", SqlDbType.int, eZeeDataType.INT_SIZE, mintvoiduserunkid.ToString)
            If mdtVoiddatetime = Nothing Then
                objDataOperation.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, DBNull.Value)
            Else
                objDataOperation.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, mdtVoiddatetime)
            End If
            objDataOperation.AddParameter("@voidreason", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrVoidreason.ToString)
            'Sohail (25 May 2012) -- Start
            'TRA - ENHANCEMENT
            objDataOperation.AddParameter("@basecurrencyid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintBasecurrencyid.ToString)
            objDataOperation.AddParameter("@baseexchangerate", SqlDbType.Decimal, eZeeDataType.DECIMAL_SIZE, mdecBaseexchangerate.ToString)
            objDataOperation.AddParameter("@paidcurrencyid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintPaidcurrencyid.ToString)
            objDataOperation.AddParameter("@expaidrate", SqlDbType.Decimal, eZeeDataType.DECIMAL_SIZE, mdecExpaidrate.ToString)
            objDataOperation.AddParameter("@expaidamt", SqlDbType.Decimal, eZeeDataType.DECIMAL_SIZE, mdecExpaidamt.ToString)
            'Sohail (25 May 2012) -- End
            'Hemant (07 July 2023) -- Start
            'Enhancement : A1X-961 : Mobile money account numbers - Create a new screen to capture mobile money account numbers.
            objDataOperation.AddParameter("@payment_typeid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintPayment_typeid.ToString)
            objDataOperation.AddParameter("@mobileno", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrMobileno.ToString)
            'Hemant (07 July 2023) -- End


            strQ = "INSERT INTO prempsalary_tran ( " & _
              "  paymenttranunkid " & _
              ", paymentdate " & _
              ", employeeunkid " & _
              ", empbanktranid " & _
              ", amount " & _
              ", userunkid " & _
              ", isvoid " & _
              ", voiduserunkid " & _
              ", voiddatetime " & _
              ", voidreason" & _
              ", basecurrencyid " & _
              ", baseexchangerate " & _
              ", paidcurrencyid " & _
              ", expaidrate " & _
              ", expaidamt " & _
              ", payment_typeid " & _
              ", mobileno " & _
            ") VALUES (" & _
              "  @paymenttranunkid " & _
              ", @paymentdate " & _
              ", @employeeunkid " & _
              ", @empbanktranid " & _
              ", @amount " & _
              ", @userunkid " & _
              ", @isvoid " & _
              ", @voiduserunkid " & _
              ", @voiddatetime " & _
              ", @voidreason" & _
              ", @basecurrencyid " & _
              ", @baseexchangerate " & _
              ", @paidcurrencyid " & _
              ", @expaidrate " & _
              ", @expaidamt " & _
              ", @payment_typeid " & _
              ", @mobileno " & _
            "); SELECT @@identity"
            'Hemant (07 July 2023) -- [payment_typeid,mobileno]
            'Sohail (25 May 2012) - [basecurrencyid,baseexchangerate,paidcurrencyid,expaidrate,expaidamt]

            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            mintEmpsalaryTranUnkId = dsList.Tables(0).Rows(0).Item(0)

            'Sohail (11 Nov 2010) -- Start
            'Anjan (11 Jun 2011)-Start
            'Issue : In main table entry was get inserted if there any error occurs at time of insertion in AT table.
            'Call InsertAuditTrailForEmpSalary(objDataOperation, 1)
            If InsertAuditTrailForEmpSalary(objDataOperation, 1) = False Then
                objDataOperation.ReleaseTransaction(False)
                Return False
            End If
            'Anjan (11 Jun 2011)-End

            'Sohail (11 Nov 2010) -- End

            Return True
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: Insert; Module Name: " & mstrModuleName)
            Return False
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
    End Function

    'Sohail (12 Oct 2011) -- Start
    '''' <summary>
    '''' Modify By: Sohail
    '''' </summary>
    '''' <returns>Boolean</returns>
    '''' <purpose> Update Database Table (prempsalary_tran) </purpose>
    'Public Function Update() As Boolean
    '    'If isExist(mstrName, mintEmpsalarytranunkid) Then
    '    '    mstrMessage = "<Message>"
    '    '    Return False
    '    'End If

    '    Dim dsList As DataSet = Nothing
    '    Dim strQ As String = ""
    '    Dim exForce As Exception

    '    objDataOperation = New clsDataOperation

    '    Try
    '        objDataOperation.AddParameter("@empsalarytranunkid", SqlDbType.int, eZeeDataType.INT_SIZE, mintempsalarytranunkid.ToString)
    '        objDataOperation.AddParameter("@paymenttranunkid", SqlDbType.int, eZeeDataType.INT_SIZE, mintpaymenttranunkid.ToString)
    '        objDataOperation.AddParameter("@paymentdate", SqlDbType.datetime, eZeeDataType.DATETIME_SIZE, mdtpaymentdate.ToString)
    '        objDataOperation.AddParameter("@employeeunkid", SqlDbType.int, eZeeDataType.INT_SIZE, mintemployeeunkid.ToString)
    '        objDataOperation.AddParameter("@empbanktranid", SqlDbType.int, eZeeDataType.INT_SIZE, mintempbanktranid.ToString)
    '        objDataOperation.AddParameter("@amount", SqlDbType.Decimal, eZeeDataType.DECIMAL_SIZE, mdecAmount.ToString) 'Sohail (11 May 2011)
    '        objDataOperation.AddParameter("@userunkid", SqlDbType.int, eZeeDataType.INT_SIZE, mintuserunkid.ToString)
    '        objDataOperation.AddParameter("@isvoid", SqlDbType.bit, eZeeDataType.BIT_SIZE, mblnisvoid.ToString)
    '        objDataOperation.AddParameter("@voiduserunkid", SqlDbType.int, eZeeDataType.INT_SIZE, mintvoiduserunkid.ToString)
    '        objDataOperation.AddParameter("@voiddatetime", SqlDbType.datetime, eZeeDataType.DATETIME_SIZE, mdtvoiddatetime.ToString)
    '        objDataOperation.AddParameter("@voidreason", SqlDbType.nvarchar, eZeeDataType.NAME_SIZE, mstrvoidreason.ToString)

    '        strQ = "UPDATE prempsalary_tran SET " & _
    '          "  paymenttranunkid = @paymenttranunkid" & _
    '          ", paymentdate = @paymentdate" & _
    '          ", employeeunkid = @employeeunkid" & _
    '          ", empbanktranid = @empbanktranid" & _
    '          ", amount = @amount" & _
    '          ", userunkid = @userunkid" & _
    '          ", isvoid = @isvoid" & _
    '          ", voiduserunkid = @voiduserunkid" & _
    '          ", voiddatetime = @voiddatetime" & _
    '          ", voidreason = @voidreason " & _
    '        "WHERE empsalarytranunkid = @empsalarytranunkid "

    '        Call objDataOperation.ExecNonQuery(strQ)

    '        If objDataOperation.ErrorMessage <> "" Then
    '            exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
    '            Throw exForce
    '        End If

    '        Return True
    '    Catch ex As Exception
    '        Throw New Exception(ex.Message & "; Procedure Name: Update; Module Name: " & mstrModuleName)
    '        Return False
    '    Finally
    '        exForce = Nothing
    '        If dsList IsNot Nothing Then dsList.Dispose()
    '        objDataOperation = Nothing
    '    End Try
    'End Function


    'Hemant (26 May 2023) -- Start
    'ENHANCEMENT(TRA) : A1X-943 - Integrate NBC payment API in Aruti
    Public Function Update(Optional ByVal xDataOp As clsDataOperation = Nothing) As Boolean
        'If isExist(mstrName, mintEmpsalarytranunkid) Then
        '    mstrMessage = "<Message>"
        '    Return False
        'End If

        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        If xDataOp Is Nothing Then
            objDataOperation = New clsDataOperation
            objDataOperation.BindTransaction()
        Else
            objDataOperation = xDataOp
        End If
        objDataOperation.ClearParameters()

        Try
            objDataOperation.AddParameter("@empsalarytranunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintEmpsalarytranunkid.ToString)
            objDataOperation.AddParameter("@paymenttranunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintPaymenttranunkid.ToString)
            objDataOperation.AddParameter("@paymentdate", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, mdtPaymentdate.ToString)
            objDataOperation.AddParameter("@employeeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintEmployeeunkid.ToString)
            objDataOperation.AddParameter("@empbanktranid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintEmpbanktranid.ToString)
            objDataOperation.AddParameter("@amount", SqlDbType.Decimal, eZeeDataType.DECIMAL_SIZE, mdecAmount.ToString) 'Sohail (11 May 2011)
            objDataOperation.AddParameter("@userunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintUserunkid.ToString)
            objDataOperation.AddParameter("@isvoid", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsvoid.ToString)
            objDataOperation.AddParameter("@voiduserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintVoiduserunkid.ToString)
            If mdtVoiddatetime <> Nothing Then
                objDataOperation.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, mdtVoiddatetime.ToString)
            Else
                objDataOperation.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, DBNull.Value)
            End If
            objDataOperation.AddParameter("@voidreason", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrVoidreason.ToString)

            strQ = "UPDATE prempsalary_tran SET " & _
              "  paymenttranunkid = @paymenttranunkid" & _
              ", paymentdate = @paymentdate" & _
              ", employeeunkid = @employeeunkid" & _
              ", empbanktranid = @empbanktranid" & _
              ", amount = @amount" & _
              ", userunkid = @userunkid" & _
              ", isvoid = @isvoid" & _
              ", voiduserunkid = @voiduserunkid" & _
              ", voiddatetime = @voiddatetime" & _
              ", voidreason = @voidreason " & _
            "WHERE empsalarytranunkid = @empsalarytranunkid "

            Call objDataOperation.ExecNonQuery(strQ)

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            If xDataOp Is Nothing Then objDataOperation.ReleaseTransaction(True)

            Return True
        Catch ex As Exception
            objDataOperation.ReleaseTransaction(False)
            Throw New Exception(ex.Message & "; Procedure Name: Update; Module Name: " & mstrModuleName)
            Return False
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            If xDataOp Is Nothing Then objDataOperation = Nothing
        End Try
    End Function
    'Hemant (26 May 2023) -- End

    '''' <summary>
    '''' Modify By: Sohail
    '''' </summary>
    '''' <returns>Boolean</returns>
    '''' <purpose> Delete Database Table (prempsalary_tran) </purpose>
    'Public Function Delete(ByVal intUnkid As Integer) As Boolean
    '    If isUsed(intUnkid) Then
    '        mstrMessage = "<Message>"
    '        Return False
    '    End If

    '    Dim dsList As DataSet = Nothing
    '    Dim strQ As String = ""
    '    Dim exForce As Exception

    '    objDataOperation = New clsDataOperation

    '    Try
    '        strQ = "DELETE FROM prempsalary_tran " & _
    '        "WHERE empsalarytranunkid = @empsalarytranunkid "

    '        objDataOperation.AddParameter("@empsalarytranunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intUnkid)

    '        Call objDataOperation.ExecNonQuery(strQ)

    '        If objDataOperation.ErrorMessage <> "" Then
    '            exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
    '            Throw exForce
    '        End If

    '        Return True
    '    Catch ex As Exception
    '        Throw New Exception(ex.Message & "; Procedure Name: Update; Module Name: " & mstrModuleName)
    '        Return False
    '    Finally
    '        exForce = Nothing
    '        If dsList IsNot Nothing Then dsList.Dispose()
    '        objDataOperation = Nothing
    '    End Try
    'End Function
    'Sohail (12 Oct 2011) -- End

    ''' <summary>
    ''' Modify By: Sohail
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>
    Public Function isUsed(ByVal intUnkid As Integer) As Boolean
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        objDataOperation = New clsDataOperation

        Try
            StrQ = "<Query>"

            objDataOperation.AddParameter("@empsalarytranunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intUnkid)

            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            Return dsList.tables(0).rows.count > 0
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: isUsed; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
    End Function

    ''' <summary>
    ''' Modify By: Sohail
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>
    Public Function isExist(ByVal strCode As String, ByVal strName As String, Optional ByVal intUnkid As Integer = -1) As Boolean
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        objDataOperation = New clsDataOperation

        Try
            strQ = "SELECT " & _
              "  empsalarytranunkid " & _
              ", paymenttranunkid " & _
              ", paymentdate " & _
              ", employeeunkid " & _
              ", empbanktranid " & _
              ", amount " & _
              ", userunkid " & _
              ", isvoid " & _
              ", voiduserunkid " & _
              ", voiddatetime " & _
              ", voidreason " & _
              ", basecurrencyid " & _
              ", baseexchangerate " & _
              ", paidcurrencyid " & _
              ", expaidrate " & _
              ", expaidamt " & _
              ", payment_typeid " & _
              ", mobileno " & _
             "FROM prempsalary_tran " & _
             "WHERE name = @name " & _
             "AND code = @code "
            'Hemant (07 July 2023) -- [payment_typeid,mobileno]
            'Sohail (25 May 2012) - [basecurrencyid,baseexchangerate,paidcurrencyid,expaidrate,expaidamt]

            If intUnkid > 0 Then
                strQ &= " AND empsalarytranunkid <> @empsalarytranunkid"
            End If

            objDataOperation.AddParameter("@code", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, strCode)
            objDataOperation.AddParameter("@name", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, strName)
            objDataOperation.AddParameter("@empsalarytranunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intUnkid)

            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            Return dsList.tables(0).rows.count > 0
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: isExist; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
    End Function

    'Sohail (11 Nov 2010) -- Start

    'Anjan (11 Jun 2011)-Start
    'Issue : In main table entry was get inserted if there any error occurs at time of insertion in AT table.
    'Public Sub InsertAuditTrailForEmpSalary(ByVal objDataOperation As clsDataOperation, ByVal AuditType As Integer)
    Public Function InsertAuditTrailForEmpSalary(ByVal objDataOperation As clsDataOperation, ByVal AuditType As Integer) As Boolean
        'Anjan (11 Jun 2011)-End
        Dim strQ As String = ""
        Dim exForce As Exception

        objDataOperation = New clsDataOperation

        Try
            'Sohail (18 May 2013) -- Start
            'TRA - ENHANCEMENT
            If mstrWebIP.Trim = "" Then mstrWebIP = getIP()
            If mstrWebhostName.Trim = "" Then mstrWebhostName = getHostName()
            'Sohail (18 May 2013) -- End


            'S.SANDEEP [ 19 JULY 2012 ] -- START
            'Enhancement : TRA Changes

            'strQ = "INSERT INTO atprempsalary_tran ( " & _
            '              "  empsalarytranunkid " & _
            '              ", paymenttranunkid " & _
            '              ", paymentdate " & _
            '              ", employeeunkid " & _
            '              ", empbanktranid " & _
            '              ", amount " & _
            '              ", audittype " & _
            '              ", audituserunkid " & _
            '              ", auditdatetime " & _
            '              ", ip " & _
            '              ", machine_name" & _
            '              ", basecurrencyid " & _
            '              ", baseexchangerate " & _
            '              ", paidcurrencyid " & _
            '              ", expaidrate " & _
            '              ", expaidamt " & _
            '") VALUES (" & _
            '              "  @empsalarytranunkid " & _
            '              ", @paymenttranunkid " & _
            '              ", @paymentdate " & _
            '              ", @employeeunkid " & _
            '              ", @empbanktranid " & _
            '              ", @amount " & _
            '              ", @audittype " & _
            '              ", @audituserunkid " & _
            '              ", @auditdatetime " & _
            '              ", @ip " & _
            '              ", @machine_name" & _
            '              ", @basecurrencyid " & _
            '              ", @baseexchangerate " & _
            '              ", @paidcurrencyid " & _
            '              ", @expaidrate " & _
            '              ", @expaidamt " & _
            '"); SELECT @@identity" 'Sohail (25 May 2012) - [basecurrencyid,baseexchangerate,paidcurrencyid,expaidrate,expaidamt]

            strQ = "INSERT INTO atprempsalary_tran ( " & _
                          "  empsalarytranunkid " & _
                          ", paymenttranunkid " & _
                          ", paymentdate " & _
                          ", employeeunkid " & _
                          ", empbanktranid " & _
                          ", amount " & _
                          ", audittype " & _
                          ", audituserunkid " & _
                          ", auditdatetime " & _
                          ", ip " & _
                          ", machine_name" & _
                          ", basecurrencyid " & _
                          ", baseexchangerate " & _
                          ", paidcurrencyid " & _
                          ", expaidrate " & _
                          ", expaidamt " & _
                        ", form_name " & _
                        " " & _
                        " " & _
                        " " & _
                        " " & _
                        " " & _
                        ", isweb " & _
                          ", payment_typeid " & _
                          ", mobileno " & _
            ") VALUES (" & _
                          "  @empsalarytranunkid " & _
                          ", @paymenttranunkid " & _
                          ", @paymentdate " & _
                          ", @employeeunkid " & _
                          ", @empbanktranid " & _
                          ", @amount " & _
                          ", @audittype " & _
                          ", @audituserunkid " & _
                          ", @auditdatetime " & _
                          ", @ip " & _
                          ", @machine_name" & _
                          ", @basecurrencyid " & _
                          ", @baseexchangerate " & _
                          ", @paidcurrencyid " & _
                          ", @expaidrate " & _
                          ", @expaidamt " & _
                        ", @form_name " & _
                        " " & _
                        " " & _
                        " " & _
                        " " & _
                        " " & _
                        ", @isweb " & _
                          ", @payment_typeid " & _
                          ", @mobileno " & _
                    "); SELECT @@identity"
            'Hemant (07 July 2023) -- [payment_typeid,mobileno]
            'Sohail (25 May 2012) - [basecurrencyid,baseexchangerate,paidcurrencyid,expaidrate,expaidamt]


            'S.SANDEEP [ 19 JULY 2012 ] -- END

            objDataOperation.ClearParameters()
            objDataOperation.AddParameter("@empsalarytranunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintEmpsalarytranunkid.ToString)
            objDataOperation.AddParameter("@paymenttranunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintPaymenttranunkid.ToString)
            objDataOperation.AddParameter("@paymentdate", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, mdtPaymentdate)
            objDataOperation.AddParameter("@employeeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintEmployeeunkid.ToString)
            objDataOperation.AddParameter("@empbanktranid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintEmpbanktranid.ToString)
            objDataOperation.AddParameter("@amount", SqlDbType.Decimal, eZeeDataType.DECIMAL_SIZE, mdecAmount.ToString) 'Sohail (11 May 2011)
            objDataOperation.AddParameter("@audittype", SqlDbType.Int, eZeeDataType.INT_SIZE, AuditType.ToString)
            objDataOperation.AddParameter("@audituserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintUserunkid)
            objDataOperation.AddParameter("@auditdatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, mdtAuditDate)
            objDataOperation.AddParameter("@ip", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrWebIP)
            objDataOperation.AddParameter("@machine_name", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrWebhostName)
            objDataOperation.AddParameter("@basecurrencyid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintBasecurrencyid.ToString)
            objDataOperation.AddParameter("@baseexchangerate", SqlDbType.Decimal, eZeeDataType.DECIMAL_SIZE, mdecBaseexchangerate.ToString)
            objDataOperation.AddParameter("@paidcurrencyid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintPaidcurrencyid.ToString)
            objDataOperation.AddParameter("@expaidrate", SqlDbType.Decimal, eZeeDataType.DECIMAL_SIZE, mdecExpaidrate.ToString)
            objDataOperation.AddParameter("@expaidamt", SqlDbType.Decimal, eZeeDataType.DECIMAL_SIZE, mdecExpaidamt.ToString)



            If mstrWebFormName.Trim.Length <= 0 Then
                objDataOperation.AddParameter("@form_name", SqlDbType.NVarChar, 500, mstrFormName)
                objDataOperation.AddParameter("@isweb", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsWeb)

            Else
                objDataOperation.AddParameter("@form_name", SqlDbType.NVarChar, 500, mstrWebFormName)
                objDataOperation.AddParameter("@isweb", SqlDbType.Bit, eZeeDataType.BIT_SIZE, True)
                objDataOperation.AddParameter("@module_name1", SqlDbType.NVarChar, 500, Language.getMessage(mstrModuleName, 1, "WEB"))
            End If

            'S.SANDEEP [ 19 JULY 2012 ] -- END
            'Hemant (07 July 2023) -- Start
            'Enhancement : A1X-961 : Mobile money account numbers - Create a new screen to capture mobile money account numbers.
            objDataOperation.AddParameter("@payment_typeid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintPayment_typeid.ToString)
            objDataOperation.AddParameter("@mobileno", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrMobileno.ToString)
            'Hemant (07 July 2023) -- End

            objDataOperation.ExecNonQuery(strQ)

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            'Anjan (11 Jun 2011)-Start
            'Issue : In main table entry was get inserted if there any error occurs at time of insertion in AT table.
            Return True
            'Anjan (11 Jun 2011)-End

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "InsertAuditTrailForEmpSalary", mstrModuleName)
            'Anjan (11 Jun 2011)-Start
            'Issue : In main table entry was get inserted if there any error occurs at time of insertion in AT table.
            Return False
            'Anjan (11 Jun 2011)-End
        Finally
            objDataOperation = Nothing
        End Try
    End Function
    'Sohail (11 Nov 2010) -- End


    'Hemant (26 May 2023) -- Start
    'ENHANCEMENT(TRA) : A1X-943 - Integrate NBC payment API in Aruti
    Public Function GetSalaryPaymentData(ByVal intPeriodId As Integer) As DataSet
        Dim StrQ As String = ""
        Dim exForce As Exception
        Dim dsList As DataSet

        objDataOperation = New clsDataOperation
        Try
            StrQ = "SELECT  " & _
                   "  prempsalary_tran.empsalarytranunkid " & _
                   ", prempsalary_tran.paymenttranunkid " & _
                   ", prempsalary_tran.paymentdate " & _
                   ", prempsalary_tran.employeeunkid " & _
                   ", prempsalary_tran.empbanktranid " & _
                   ", prempsalary_tran.amount " & _
                   ", prempsalary_tran.payment_typeid " & _
                   ", prempsalary_tran.mobileno "
            'Hemant (07 July 2023) -- [payment_typeid,mobileno]

            StrQ &= " FROM prpayment_tran " & _
                     "LEFT JOIN prempsalary_tran ON prempsalary_tran.paymenttranunkid = prpayment_tran.paymenttranunkid " & _
                     "   AND CONVERT(CHAR(8), prempsalary_tran.paymentdate, 112) = CONVERT(CHAR(8), prpayment_tran.paymentdate, 112) " & _
                     "JOIN cfcommon_period_tran ON prpayment_tran.periodunkid = cfcommon_period_tran.periodunkid "

            StrQ &= "WHERE   ISNULL(prpayment_tran.isvoid, 0) = 0 " & _
                  "    AND prempsalary_tran.isvoid = 0 " & _
                  "    AND prpayment_tran.referenceid = " & clsPayment_tran.enPaymentRefId.PAYSLIP & " " & _
                  "    AND prpayment_tran.paytypeid = " & clsPayment_tran.enPayTypeId.PAYMENT & " " & _
                  "	   AND prpayment_tran.periodunkid = '" & intPeriodId & "' "

            dsList = objDataOperation.ExecQuery(StrQ, "DataTable")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & " " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "GetSalaryPaymentData", mstrModuleName)
        End Try

        Return dsList
    End Function

    'Hemant (26 May 2023) -- End


	'<Language> This Auto Generated Text Please Do Not Modify it.
#Region " Language & UI Settings "
	Public Shared Sub SetMessages()
		Try
			Language.setMessage(mstrModuleName, 1, "WEB")

		Catch Ex As Exception
			DisplayError.Show("-1", ex.Message, "SetMessages", mstrModuleName)
		End Try
	End Sub
#End Region 'Language & UI Settings
	'</Language>
End Class