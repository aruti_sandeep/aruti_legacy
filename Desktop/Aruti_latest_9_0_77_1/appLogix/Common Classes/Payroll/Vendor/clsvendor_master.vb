﻿'************************************************************************************************************************************
'Class Name : clsvendor_master.vb
'Purpose    : All Vendor Opration like getList, Insert, Update, Delete, checkDuplicate
'Date       :01/07/2010
'Written By :Pinkal
'Modified   :
'Last Message Index = 3
'************************************************************************************************************************************

Imports eZeeCommonLib
''' <summary>
''' Purpose: 
''' Developer: Pinkal
''' </summary>
Public Class clsvendor_master
    Private Shared ReadOnly mstrModuleName As String = "clsvendor_master"
    Dim objDataOperation As clsDataOperation
    Dim mstrMessage As String = ""

#Region " Private variables "
    Private mintVendorunkid As Integer
    Private mintVendorGroupunkid As Integer
    Private mstrVendorcode As String = String.Empty
    Private mstrCompanyname As String = String.Empty
    Private mstrFirstname As String = String.Empty
    Private mstrLastname As String = String.Empty
    Private mstrAddress1 As String = String.Empty
    Private mstrAddress2 As String = String.Empty
    Private mintCountryunkid As Integer
    Private mintStateunkid As Integer
    Private mintCityunkid As Integer
    Private mintPincodeunkid As Integer
    Private mstrContactperson As String = String.Empty
    Private mstrContactno As String = String.Empty
    Private mstrWebsite As String = String.Empty
    Private mblnIsbenefitprovider As Boolean
    Private mblnIsactive As Boolean = True
#End Region

#Region " Properties "

'S.SANDEEP [28-May-2018] -- START 
'ISSUE/ENHANCEMENT : {Audit Trails} 
Private mstrFormName As String = String.Empty
Public WriteOnly Property _FormName() As String 
Set(ByVal value As String) 
mstrFormName = value 
End Set 
End Property 
 
Private mstrClientIP As String = ""
Public WriteOnly Property _ClientIP() As String 
Set(ByVal value As String) 
mstrClientIP = value 
End Set 
End Property 
 
Private mstrHostName As String = ""
Public WriteOnly Property _HostName() As String 
Set(ByVal value As String) 
mstrHostName = value 
End Set 
End Property 
 
Private mblnIsWeb As Boolean = False
Public WriteOnly Property _FromWeb() As Boolean 
Set(ByVal value As Boolean) 
mblnIsWeb = value 
End Set 
End Property 
 
    Private mintAuditUserId As Integer = 0
    Public WriteOnly Property _AuditUserId() As Integer
        Set(ByVal value As Integer)
            mintAuditUserId = value
        End Set
    End Property

    Private mintCompanyUnkid As Integer = 0
    Public WriteOnly Property _CompanyUnkid() As Integer
        Set(ByVal value As Integer)
            mintCompanyUnkid = value
        End Set
    End Property
 
Private mdtAuditDate As DateTime = Now
Public WriteOnly Property _AuditDate() As DateTime 
Set(ByVal value As DateTime) 
mdtAuditDate = value 
End Set 
End Property 
 
Private xDataOpr As clsDataOperation = Nothing
Public Property _xDataOpr() As clsDataOperation 
Get 
Return xDataOpr 
End Get 
Set(ByVal value As clsDataOperation) 
xDataOpr = value 
End Set 
End Property 
 
Private mintLoginEmployeeunkid As Integer = 0
Public Property _LoginEmployeeunkid() As Integer 
Get 
Return mintLoginEmployeeunkid 
End Get 
Set(ByVal value As Integer) 
mintLoginEmployeeunkid = value 
End Set 
End Property 
 
Private mintVoidlogingemployeeunkid As Integer=0
Public Property _Voidloginemployeeunkid() As Integer 
Get 
Return mintVoidlogingemployeeunkid 
End Get 
Set(ByVal value As Integer) 
mintVoidlogingemployeeunkid = value 
End Set 
End Property 
'S.SANDEEP [28-May-2018] -- END 
    ''' <summary>
    ''' Purpose: Get Message from Class 
    ''' Modify By: Pinkal
    ''' </summary>
    Public ReadOnly Property _Message() As String
        Get
            Return mstrMessage
        End Get
    End Property

    ''' <summary>
    ''' Purpose: Get or Set vendorunkid
    ''' Modify By: Pinkal
    ''' </summary>
    Public Property _Vendorunkid() As Integer
        Get
            Return mintVendorunkid
        End Get
        Set(ByVal value As Integer)
            mintVendorunkid = Value
            Call getData()
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set vendorgroupunkid
    ''' Modify By: Pinkal
    ''' </summary>
    Public Property _Vendorgroupunkid() As Integer
        Get
            Return mintVendorGroupunkid
        End Get
        Set(ByVal value As Integer)
            mintVendorGroupunkid = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set vendorcode
    ''' Modify By: Pinkal
    ''' </summary>
    Public Property _Vendorcode() As String
        Get
            Return mstrVendorcode
        End Get
        Set(ByVal value As String)
            mstrVendorcode = Value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set companyname
    ''' Modify By: Pinkal
    ''' </summary>
    Public Property _Companyname() As String
        Get
            Return mstrCompanyname
        End Get
        Set(ByVal value As String)
            mstrCompanyname = Value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set firstname
    ''' Modify By: Pinkal
    ''' </summary>
    Public Property _Firstname() As String
        Get
            Return mstrFirstname
        End Get
        Set(ByVal value As String)
            mstrFirstname = Value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set lastname
    ''' Modify By: Pinkal
    ''' </summary>
    Public Property _Lastname() As String
        Get
            Return mstrLastname
        End Get
        Set(ByVal value As String)
            mstrLastname = Value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set address1
    ''' Modify By: Pinkal
    ''' </summary>
    Public Property _Address1() As String
        Get
            Return mstrAddress1
        End Get
        Set(ByVal value As String)
            mstrAddress1 = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set address2
    ''' Modify By: Pinkal
    ''' </summary>
    Public Property _Address2() As String
        Get
            Return mstrAddress2
        End Get
        Set(ByVal value As String)
            mstrAddress2 = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set countryunkid
    ''' Modify By: Pinkal
    ''' </summary>
    Public Property _Countryunkid() As Integer
        Get
            Return mintCountryunkid
        End Get
        Set(ByVal value As Integer)
            mintCountryunkid = Value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set stateunkid
    ''' Modify By: Pinkal
    ''' </summary>
    Public Property _Stateunkid() As Integer
        Get
            Return mintStateunkid
        End Get
        Set(ByVal value As Integer)
            mintStateunkid = Value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set cityunkid
    ''' Modify By: Pinkal
    ''' </summary>
    Public Property _Cityunkid() As Integer
        Get
            Return mintCityunkid
        End Get
        Set(ByVal value As Integer)
            mintCityunkid = Value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set pincodeunkid
    ''' Modify By: Pinkal
    ''' </summary>
    Public Property _Pincodeunkid() As Integer
        Get
            Return mintPincodeunkid
        End Get
        Set(ByVal value As Integer)
            mintPincodeunkid = Value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set contactperson
    ''' Modify By: Pinkal
    ''' </summary>
    Public Property _Contactperson() As String
        Get
            Return mstrContactperson
        End Get
        Set(ByVal value As String)
            mstrContactperson = Value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set contactno
    ''' Modify By: Pinkal
    ''' </summary>
    Public Property _Contactno() As String
        Get
            Return mstrContactno
        End Get
        Set(ByVal value As String)
            mstrContactno = Value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set website
    ''' Modify By: Pinkal
    ''' </summary>
    Public Property _Website() As String
        Get
            Return mstrWebsite
        End Get
        Set(ByVal value As String)
            mstrWebsite = Value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set isbenefitprovider
    ''' Modify By: Pinkal
    ''' </summary>
    Public Property _Isbenefitprovider() As Boolean
        Get
            Return mblnIsbenefitprovider
        End Get
        Set(ByVal value As Boolean)
            mblnIsbenefitprovider = Value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set isactive
    ''' Modify By: Pinkal
    ''' </summary>
    Public Property _Isactive() As Boolean
        Get
            Return mblnIsactive
        End Get
        Set(ByVal value As Boolean)
            mblnIsactive = Value
        End Set
    End Property

#End Region

    ''' <summary>
    ''' Modify By: Pinkal
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>
    Public Sub GetData()
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        objDataOperation = New clsDataOperation

        Try
            strQ = "SELECT " & _
              "  vendorunkid " & _
              ", vendorgroupunkid " & _
              ", vendorcode " & _
              ", companyname " & _
              ", firstname " & _
              ", lastname " & _
              ", address1 " & _
              ", address2 " & _
              ", countryunkid " & _
              ", stateunkid " & _
              ", cityunkid " & _
              ", pincodeunkid " & _
              ", contactperson " & _
              ", contactno " & _
              ", website " & _
              ", isbenefitprovider " & _
              ", isactive " & _
             "FROM prvendor_master " & _
             "WHERE vendorunkid = @vendorunkid "

            objDataOperation.AddParameter("@vendorunkid", SqlDbType.int, eZeeDataType.INT_SIZE, mintVendorUnkId.ToString)

            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            For Each dtRow As DataRow In dsList.Tables(0).Rows
                mintVendorunkid = CInt(dtRow.Item("vendorunkid"))
                mintVendorGroupunkid = CInt(dtRow.Item("vendorgroupunkid"))
                mstrvendorcode = dtRow.Item("vendorcode").ToString
                mstrcompanyname = dtRow.Item("companyname").ToString
                mstrfirstname = dtRow.Item("firstname").ToString
                mstrlastname = dtRow.Item("lastname").ToString
                mstrAddress1 = dtRow.Item("address1").ToString
                mstrAddress2 = dtRow.Item("address2").ToString
                mintcountryunkid = CInt(dtRow.Item("countryunkid"))
                mintstateunkid = CInt(dtRow.Item("stateunkid"))
                mintcityunkid = CInt(dtRow.Item("cityunkid"))
                mintpincodeunkid = CInt(dtRow.Item("pincodeunkid"))
                mstrcontactperson = dtRow.Item("contactperson").ToString
                mstrcontactno = dtRow.Item("contactno").ToString
                mstrwebsite = dtRow.Item("website").ToString
                mblnisbenefitprovider = CBool(dtRow.Item("isbenefitprovider"))
                mblnisactive = CBool(dtRow.Item("isactive"))
                Exit For
            Next
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: GetData; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
    End Sub


    ''' <summary>
    ''' Modify By: Pinkal
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>
    Public Function GetList(ByVal strTableName As String, Optional ByVal blnOnlyActive As Boolean = True) As DataSet
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        objDataOperation = New clsDataOperation

        Try
            strQ = "SELECT " & _
              "  vendorunkid " & _
              ", vendorgroupunkid " & _
              ", prpayrollgroup_master.groupname as vendorgroup " & _
              ", vendorcode " & _
              ", companyname " & _
              ", firstname " & _
              ", lastname " & _
              ", isnull(address1,'' ) + isnull(address2,'') as address " & _
              ", prvendor_master.countryunkid " & _
              ", cfcommon_master.name as country " & _
              ", prvendor_master.stateunkid " & _
              ", hrmsConfiguration..cfstate_master.name as state " & _
              ", prvendor_master.cityunkid " & _
              ", hrmsConfiguration..cfcity_master.name as city " & _
              ", prvendor_master.pincodeunkid " & _
              ", hrmsConfiguration..cfzipcode_master.zipcode_no as pincode " & _
              ", contactperson " & _
              ", contactno " & _
              ", prpayrollgroup_master.website " & _
              ", isbenefitprovider " & _
              ", prvendor_master.isactive " & _
             "FROM prvendor_master " & _
             " LEFT JOIN prpayrollgroup_master on prpayrollgroup_master.groupmasterunkid = prvendor_master.vendorgroupunkid " & _
             " LEFT JOIN cfcommon_master on cfcommon_master.masterunkid = prvendor_master.countryunkid " & _
             " LEFT JOIN hrmsConfiguration..cfstate_master on hrmsConfiguration..cfstate_master.stateunkid = prvendor_master.stateunkid " & _
             " LEFT JOIN hrmsConfiguration..cfcity_master on hrmsConfiguration..cfcity_master.cityunkid = prvendor_master.cityunkid " & _
             " LEFT JOIN hrmsConfiguration..cfzipcode_master on hrmsConfiguration..cfzipcode_master.zipcodeunkid = prvendor_master.pincodeunkid "

            If blnOnlyActive Then
                strQ &= " WHERE prvendor_master.isactive = 1 "
            End If

            dsList = objDataOperation.ExecQuery(strQ, strTableName)

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: GetList; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
        Return dsList
    End Function


    ''' <summary>
    ''' Modify By: Pinkal
    ''' </summary>
    ''' <returns>Boolean</returns>
    ''' <purpose> INSERT INTO Database Table (prvendor_master) </purpose>
    Public Function Insert() As Boolean
        If isExist(mstrVendorcode) Then
            mstrMessage = Language.getMessage(mstrModuleName, 1, "This Vendor Code is already defined. Please define new Vendor Code.")
            Return False
        ElseIf isExist("", mstrCompanyname) Then
            mstrMessage = Language.getMessage(mstrModuleName, 2, "This Company Name is already defined. Please define new Company Name.")
            Return False
        End If

        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        objDataOperation = New clsDataOperation

        Try
            objDataOperation.AddParameter("@vendorcode", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrVendorcode.ToString)
            objDataOperation.AddParameter("@vendorgroupunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintVendorGroupunkid.ToString)
            objDataOperation.AddParameter("@companyname", SqlDbType.nvarchar, eZeeDataType.NAME_SIZE, mstrcompanyname.ToString)
            objDataOperation.AddParameter("@firstname", SqlDbType.nvarchar, eZeeDataType.NAME_SIZE, mstrfirstname.ToString)
            objDataOperation.AddParameter("@lastname", SqlDbType.nvarchar, eZeeDataType.NAME_SIZE, mstrlastname.ToString)
            objDataOperation.AddParameter("@address1", SqlDbType.NVarChar, eZeeDataType.ADD_SIZE, mstrAddress1.ToString)
            objDataOperation.AddParameter("@address2", SqlDbType.NVarChar, eZeeDataType.ADD_SIZE, mstrAddress2.ToString)
            objDataOperation.AddParameter("@countryunkid", SqlDbType.int, eZeeDataType.INT_SIZE, mintcountryunkid.ToString)
            objDataOperation.AddParameter("@stateunkid", SqlDbType.int, eZeeDataType.INT_SIZE, mintstateunkid.ToString)
            objDataOperation.AddParameter("@cityunkid", SqlDbType.int, eZeeDataType.INT_SIZE, mintcityunkid.ToString)
            objDataOperation.AddParameter("@pincodeunkid", SqlDbType.int, eZeeDataType.INT_SIZE, mintpincodeunkid.ToString)
            objDataOperation.AddParameter("@contactperson", SqlDbType.nvarchar, eZeeDataType.NAME_SIZE, mstrcontactperson.ToString)
            objDataOperation.AddParameter("@contactno", SqlDbType.nvarchar, eZeeDataType.NAME_SIZE, mstrcontactno.ToString)
            objDataOperation.AddParameter("@website", SqlDbType.nvarchar, eZeeDataType.NAME_SIZE, mstrwebsite.ToString)
            objDataOperation.AddParameter("@isbenefitprovider", SqlDbType.bit, eZeeDataType.BIT_SIZE, mblnisbenefitprovider.ToString)
            objDataOperation.AddParameter("@isactive", SqlDbType.bit, eZeeDataType.BIT_SIZE, mblnisactive.ToString)

            strQ = "INSERT INTO prvendor_master ( " & _
              "  vendorcode " & _
              ", vendorgroupunkid " & _
              ", companyname " & _
              ", firstname " & _
              ", lastname " & _
              ", address1 " & _
              ", address2 " & _
              ", countryunkid " & _
              ", stateunkid " & _
              ", cityunkid " & _
              ", pincodeunkid " & _
              ", contactperson " & _
              ", contactno " & _
              ", website " & _
              ", isbenefitprovider " & _
              ", isactive" & _
            ") VALUES (" & _
              "  @vendorcode " & _
              ", @vendorgroupunkid " & _
              ", @companyname " & _
              ", @firstname " & _
              ", @lastname " & _
              ", @address1 " & _
              ", @address2 " & _
              ", @countryunkid " & _
              ", @stateunkid " & _
              ", @cityunkid " & _
              ", @pincodeunkid " & _
              ", @contactperson " & _
              ", @contactno " & _
              ", @website " & _
              ", @isbenefitprovider " & _
              ", @isactive" & _
            "); SELECT @@identity"

            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            mintVendorUnkId = dsList.Tables(0).Rows(0).Item(0)

            Return True
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: Insert; Module Name: " & mstrModuleName)
            Return False
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
    End Function

    ''' <summary>
    ''' Modify By: Pinkal
    ''' </summary>
    ''' <returns>Boolean</returns>
    ''' <purpose> Update Database Table (prvendor_master) </purpose>
    Public Function Update() As Boolean
        If isExist(mstrVendorcode, "", mintVendorunkid) Then
            mstrMessage = Language.getMessage(mstrModuleName, 1, "This Vendor Code is already defined. Please define new Vendor Code.")
            Return False
        ElseIf isExist("", mstrCompanyname, mintVendorunkid) Then
            mstrMessage = Language.getMessage(mstrModuleName, 3, "This Vendor Name is already defined. Please define new Vendor Name.")
            Return False
        End If

        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        objDataOperation = New clsDataOperation

        Try
            objDataOperation.AddParameter("@vendorunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintVendorunkid.ToString)
            objDataOperation.AddParameter("@vendorgroupunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintVendorGroupunkid.ToString)
            objDataOperation.AddParameter("@vendorcode", SqlDbType.nvarchar, eZeeDataType.NAME_SIZE, mstrvendorcode.ToString)
            objDataOperation.AddParameter("@companyname", SqlDbType.nvarchar, eZeeDataType.NAME_SIZE, mstrcompanyname.ToString)
            objDataOperation.AddParameter("@firstname", SqlDbType.nvarchar, eZeeDataType.NAME_SIZE, mstrfirstname.ToString)
            objDataOperation.AddParameter("@lastname", SqlDbType.nvarchar, eZeeDataType.NAME_SIZE, mstrlastname.ToString)
            objDataOperation.AddParameter("@address1", SqlDbType.NVarChar, eZeeDataType.ADD_SIZE, mstrAddress1.ToString)
            objDataOperation.AddParameter("@address2", SqlDbType.NVarChar, eZeeDataType.ADD_SIZE, mstrAddress2.ToString)
            objDataOperation.AddParameter("@countryunkid", SqlDbType.int, eZeeDataType.INT_SIZE, mintcountryunkid.ToString)
            objDataOperation.AddParameter("@stateunkid", SqlDbType.int, eZeeDataType.INT_SIZE, mintstateunkid.ToString)
            objDataOperation.AddParameter("@cityunkid", SqlDbType.int, eZeeDataType.INT_SIZE, mintcityunkid.ToString)
            objDataOperation.AddParameter("@pincodeunkid", SqlDbType.int, eZeeDataType.INT_SIZE, mintpincodeunkid.ToString)
            objDataOperation.AddParameter("@contactperson", SqlDbType.nvarchar, eZeeDataType.NAME_SIZE, mstrcontactperson.ToString)
            objDataOperation.AddParameter("@contactno", SqlDbType.nvarchar, eZeeDataType.NAME_SIZE, mstrcontactno.ToString)
            objDataOperation.AddParameter("@website", SqlDbType.nvarchar, eZeeDataType.NAME_SIZE, mstrwebsite.ToString)
            objDataOperation.AddParameter("@isbenefitprovider", SqlDbType.bit, eZeeDataType.BIT_SIZE, mblnisbenefitprovider.ToString)
            objDataOperation.AddParameter("@isactive", SqlDbType.bit, eZeeDataType.BIT_SIZE, mblnisactive.ToString)

            strQ = "UPDATE prvendor_master SET " & _
              "  vendorcode = @vendorcode" & _
              ", vendorgroupunkid = @vendorgroupunkid " & _
              ", companyname = @companyname" & _
              ", firstname = @firstname" & _
              ", lastname = @lastname" & _
              ", address1 = @address1" & _
              ", address2 = @address2" & _
              ", countryunkid = @countryunkid" & _
              ", stateunkid = @stateunkid" & _
              ", cityunkid = @cityunkid" & _
              ", pincodeunkid = @pincodeunkid" & _
              ", contactperson = @contactperson" & _
              ", contactno = @contactno" & _
              ", website = @website" & _
              ", isbenefitprovider = @isbenefitprovider" & _
              ", isactive = @isactive " & _
            "WHERE vendorunkid = @vendorunkid "

            Call objDataOperation.ExecNonQuery(strQ)

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            Return True
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: Update; Module Name: " & mstrModuleName)
            Return False
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
    End Function

    ''' <summary>
    ''' Modify By: Pinkal
    ''' </summary>
    ''' <returns>Boolean</returns>
    ''' <purpose> Delete Database Table (prvendor_master) </purpose>
    Public Function Delete(ByVal intUnkid As Integer) As Boolean
        'If isUsed(intUnkid) Then
        '    mstrMessage = Language.getMessage(mstrModuleName, 3, "Sorry, You cannot delete selected Vendor. Reason: This Vendor is in use.")
        '    Return False
        'End If

        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        objDataOperation = New clsDataOperation

        Try
            StrQ = "DELETE FROM prvendor_master " & _
            "WHERE vendorunkid = @vendorunkid "

            objDataOperation.AddParameter("@vendorunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intUnkid)

            Call objDataOperation.ExecNonQuery(strQ)

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            Return True
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: Update; Module Name: " & mstrModuleName)
            Return False
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
    End Function

    ''' <summary>
    ''' Modify By: Pinkal
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>
    Public Function isUsed(ByVal intUnkid As Integer) As Boolean
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        objDataOperation = New clsDataOperation

        Try
            StrQ = "<Query>"

            objDataOperation.AddParameter("@vendorunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intUnkid)

            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            Return dsList.tables(0).rows.count > 0
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: isUsed; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
    End Function

    ''' <summary>
    ''' Modify By: Pinkal
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>
    Public Function isExist(Optional ByVal strCode As String = "", Optional ByVal strName As String = "", Optional ByVal intUnkid As Integer = -1) As Boolean
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        objDataOperation = New clsDataOperation

        Try
            strQ = "SELECT " & _
              "  vendorunkid " & _
              ", vendorgroupunkid " & _
              ", vendorcode " & _
              ", companyname " & _
              ", firstname " & _
              ", lastname " & _
              ", address1 " & _
              ", address2 " & _
              ", countryunkid " & _
              ", stateunkid " & _
              ", cityunkid " & _
              ", pincodeunkid " & _
              ", contactperson " & _
              ", contactno " & _
              ", website " & _
              ", isbenefitprovider " & _
              ", isactive " & _
             "FROM prvendor_master " & _
             "WHERE 1=1 "

            'Anjan (10 Feb 2012)-Start
            'ENHANCEMENT : TRA COMMENTS on Andrew sir's Request
            strQ &= " AND isactive = 1 "
            'Anjan (10 Feb 2012)-End 

            If strCode.Length > 0 Then
                strQ &= " AND vendorcode = @vendorcode "
            End If

            If strName.Length > 0 Then
                strQ &= " AND companyname = @companyname"
            End If

            If intUnkid > 0 Then
                strQ &= " AND vendorunkid <> @vendorunkid"
            End If

            objDataOperation.AddParameter("@vendorcode", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, strCode)
            objDataOperation.AddParameter("@companyname", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, strName)
            objDataOperation.AddParameter("@vendorunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intUnkid)

            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            Return dsList.Tables(0).Rows.Count > 0
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: isExist; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
    End Function

    ''' <summary>
    ''' Modify by : Suhail
    ''' </summary>
    ''' <param name="strList"></param>
    ''' <param name="mblnFlag"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Function getComborList(Optional ByVal strList As String = "List", Optional ByVal mblnFlag As Boolean = False) As DataSet
        Dim strQ As String = ""
        Dim exForce As Exception
        Dim dsList As New DataSet

        Try
            objDataOperation = New clsDataOperation

            If mblnFlag = True Then
                strQ = "SELECT 0 AS vendorunkid , ' ' AS vendorcode, ' ' + @Select AS companyname UNION "
            End If

            strQ &= " SELECT vendorunkid, vendorcode, companyname FROM prvendor_master WHERE isactive =1"

            objDataOperation.AddParameter("@Select", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, "Select")

            dsList = objDataOperation.ExecQuery(strQ, strList)

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            Return dsList

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "GetVendorList", mstrModuleName)
            Return Nothing
        Finally
            If dsList IsNot Nothing Then dsList.Dispose()
            dsList = Nothing
            If objDataOperation IsNot Nothing Then objDataOperation.Dispose()
            objDataOperation = Nothing

            exForce = Nothing
        End Try
    End Function
	'<Language> This Auto Generated Text Please Do Not Modify it.
#Region " Language & UI Settings "
	Public Shared Sub SetMessages()
		Try
			Language.setMessage(mstrModuleName, 1, "This Vendor Code is already defined. Please define new Vendor Code.")
			Language.setMessage(mstrModuleName, 2, "This Company Name is already defined. Please define new Company Name.")
			Language.setMessage(mstrModuleName, 3, "This Vendor Name is already defined. Please define new Vendor Name.")

		Catch Ex As Exception
			DisplayError.Show("-1", ex.Message, "SetMessages", mstrModuleName)
		End Try
	End Sub
#End Region 'Language & UI Settings
	'</Language>
End Class