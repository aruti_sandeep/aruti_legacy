﻿'************************************************************************************************************************************
'Class Name : clstraining_request_master.vb
'Purpose    :
'Date       :17-Feb-2021
'Written By :Hemant
'Modified   :
'************************************************************************************************************************************

Imports eZeeCommonLib
Imports System.Web
Imports System.Threading
Imports Aruti.Data.clsCommon_Master

''' <summary>
''' Purpose: 
''' Developer: Hemant
''' </summary>

Public Class clstraining_request_master
    Private Shared ReadOnly mstrModuleName As String = "clstraining_request_master"
    Dim mstrMessage As String = ""
    Dim objDataOperation As clsDataOperation

#Region " Private variables "
    Private mintTrainingRequestunkid As Integer
    Private mdtApplication_Date As Date
    Private mintEmployeeunkid As Integer
    Private mintCourseMasterunkid As Integer
    Private mblnIsScheduled As Boolean
    Private mdtStart_Date As Date
    Private mdtEnd_Date As Date
    Private mstrProviderName As String = String.Empty
    Private mstrProviderAddress As String = String.Empty
    Private mintFundingSourceunkid As Integer
    Private mdecTotalTrainingCost As Decimal
    Private mdecApprovedCost As Decimal
    Private mintApproverTranunkid As Integer
    Private mblnIsAlignedCurrentRole As Boolean
    Private mblnIsPartofPDP As Boolean
    Private mblnIsForeignTravelling As Boolean
    Private mstrExpectedReturn As String = String.Empty
    Private mstrRemarks As String = String.Empty
    Private mintStatusunkid As Integer
    Private mblnIsSubmitApproval As Boolean
    Private mintUserunkid As Integer
    Private mintLoginEmployeeunkid As Integer
    Private mblnIsvoid As Boolean
    Private mintVoiduserunkid As Integer
    Private mintVoidLoginEmployeeunkid As Integer
    Private mdtVoiddatetime As Date
    Private mstrVoidreason As String = String.Empty
    Private mstrHostName As String = ""
    Private mstrClientIP As String = ""
    Private mintAuditUserId As Integer = 0
    Private mblnIsWeb As Boolean = False
    Private mstrFormName As String = ""
    Private mintMaxPriority As String
    Private mintTrainingproviderunkid As Integer
    Private mintTrainingvenueunkid As Integer
    Private mintMinApprovedPriority As Integer = -1
    Dim objEmailList As New List(Of clsEmailCollection)
    Private objThread As Thread
    Private mintDepartmentaltrainingneedunkid As Integer
    Private mblnIsEnrollConfirm As Boolean = False
    Private mblnIsEnrollReject As Boolean = False
    Private mdecEnrollAmount As Decimal
    Private mintTrainingStatusunkid As Integer
    Private mdtTrainingCostItem As DataTable = Nothing
    Private mintQualificationGroupunkid As Integer
    Private mintQualificationunkid As Integer
    Private mintResultunkid As Integer
    Private mdecGPAcode As Decimal
    Private mblnIsqualificaionupdated As Boolean
    Private mblnIsCompletedSubmitApproval As Boolean
    Private mintCompletedStatusunkid As Integer
    Private mintPeriodunkid As Integer
    Private mstrOtherQualificationGrp As String = ""
    Private mstrOtherQualification As String = ""
    Private mstrOtherResultCode As String = ""
    Private mintCompletedUserunkid As Integer
    'Hemant (25 May 2021) -- Start
    'ISSUE/ENHANCEMENT : OLD-393 - Training module bug fixes and enhancements
    Private mstrCompletedRemark As String = ""
    Private mstrEnrollmentRemark As String = ""
    'Hemant (25 May 2021) -- End
    'Hemant (28 Jul 2021) -- Start             
    'ENHANCEMENT : OLD-293 - Training Evaluation
    Private mdtCompletedApprovaldate As Date = Nothing
    'Hemant (28 Jul 2021) -- End
    'Hemant (20 Aug 2021) -- Start
    'ISSUE/ENHANCEMENT : OLD-447 - Give option to accommodate line manager feedback. It should be possible to mark questions as either for Manager or for Employee.
    Private mblnIsDaysAfterFeedbackSubmitted As Boolean
    Private mdtDaysAfterFeedbackSubmittedDate As Date = Nothing
    Private mstrDaysAfterFeedbackSubmittedRemark As String = ""
    'Hemant (20 Aug 2021) -- End
    'Hemant (01 Sep 2021) -- Start
    'ISSUE/ENHANCEMENT : OLD-459 - SAVE button on Evaluation Forms in addition to the SUBMIT button.
    Private mblnIsPreTrainingFeedbackSubmitted As Boolean
    Private mdtPreTrainingFeedbackSubmittedDate As Date = Nothing
    Private mblnIsPostTrainingFeedbackSubmitted As Boolean
    Private mdtPostTrainingFeedbackSubmittedDate As Date = Nothing
    Private mblnIsDaysAfterLineManagerFeedbackSubmitted As Boolean
    Private mdtDaysAfterLineManagerFeedbackSubmittedDate As Date = Nothing
    'Hemant (01 Sep 2021) -- End
    'Hemant (23 Sep 2021) -- Start
    'ENHANCEMENT : OLD-470 - Silverlands - New Training Setting to skip Training Requests Process.
    Private mblnIsSkipTrainingRequestAndApproval As Boolean
    'Hemant (23 Sep 2021) -- End
    'Hemant (09 Feb 2022) -- Start            
    'OLD-551(NMB) : New config setting to allow Training Budget settings to be based on any allocation - Cost Center etc. 
    Private mdecTrainingCostEmp As Decimal
    Private mdecApprovedAmountEmp As Decimal
    Private mintInsertFormId As Integer
    'Hemant (09 Feb 2022) -- End
    'Sohail (15 Mar 2022) -- Start
    'Enhancement : OLD-583 : NMB - Modification of Training Request Approval Screen to allow batch approvals.
    Private mstrRefno As String = ""
    Private mintCreateuserunkid As Integer = 0
    Private mintCreateloginEmployeeunkid As Integer = 0
    'Sohail (15 Mar 2022) -- End
    'Hemant (29 Apr 2022) -- Start
    'ISSUE/ENHANCEMENT : AC2-314(NMB) - NMB - As a manager, I want different approval flow (Training requests) for different training courses.
    Private mintTrainingTypeId As Integer
    Private mblnIsLocalTravelling As Boolean
    'Hemant (29 Apr 2022) -- End
    'Hemant (25 Jul 2022) -- Start            
    'ENHANCEMENT(NMB) : AC2-723 - Implementation of Approver in Training request form
    Private mintTrainingApprovalSettingID As Integer
    'Hemant (25 Jul 2022) -- End
    'Hemant (12 Oct 2022) -- Start
    'ENHANCEMENT(NMB) :  AC2-952 - As a user, I want to have a document attachment option on the group training request page
    Private mintGroupTrainingRequestunkid As Integer
    'Hemant (12 Oct 2022) -- End
    'Hemant (10 Nov 2022) -- Start
    'ENHANCEMENT(NMB) :  AC2-1024 - As a user, I want to have a field to capture the Training Venue on both individual and group training request pages. This filed will be free text and mandatory
    Private mstrVenue As String
    'Hemant (27 Oct 2022) -- End
#End Region

#Region " Public variables "
    Public pintTrainingRequestunkid As Integer
    Public pdtApplication_Date As Date
    Public pintEmployeeunkid As Integer
    Public pintCourseMasterunkid As Integer
    Public pblnIsScheduled As Boolean
    Public pdtStart_Date As Date
    Public pdtEnd_Date As Date
    Public pstrProviderName As String = String.Empty
    Public pstrProviderAddress As String = String.Empty
    Public pintFundingSourceunkid As Integer
    Public pdecTotalTrainingCost As Decimal
    Public pdecApprovedCost As Decimal
    Public pintApproverTranunkid As Integer
    Public pblnIsAlignedCurrentRole As Boolean
    Public pblnIsPartofPDP As Boolean
    Public pblnIsForeignTravelling As Boolean
    Public pstrExpectedReturn As String = String.Empty
    Public pstrRemarks As String = String.Empty
    Public pintStatusunkid As Integer
    Public pblnIsSubmitApproval As Boolean
    Public pintUserunkid As Integer
    Public pintLoginEmployeeunkid As Integer
    Public pblnIsvoid As Boolean
    Public pintVoiduserunkid As Integer
    Public pintVoidLoginEmployeeunkid As Integer
    Public pdtVoiddatetime As Date
    Public pstrVoidreason As String = String.Empty
    Public pstrHostName As String = ""
    Public pstrClientIP As String = ""
    Public pintAuditUserId As Integer = 0
    Public pblnIsWeb As Boolean = False
    Public pstrFormName As String = ""
    Public pintMaxPriority As String
    Public pintTrainingproviderunkid As Integer
    Public pintTrainingvenueunkid As Integer
    Public pintMinApprovedPriority As Integer = -1
    Public pintDepartmentaltrainingneedunkid As Integer
    Public pblnIsEnrollConfirm As Boolean = False
    Public pblnIsEnrollReject As Boolean = False
    Public pdecEnrollAmount As Decimal
    Public pintTrainingStatusunkid As Integer
    Public pblnIsCompletedSubmitApproval As Boolean
    Public pintCompletedStatusunkid As Integer
    Public pintPeriodunkid As Integer
    Public pstrOtherQualificationGrp As String = ""
    Public pstrOtherQualification As String = ""
    Public pstrOtherResultCode As String = ""
    Public pintCompletedUserunkid As Integer
    'Hemant (25 May 2021) -- Start
    'ISSUE/ENHANCEMENT : OLD-393 - Training module bug fixes and enhancements
    Public pstrCompletedRemark As String = ""
    Public pstrEnrollmentRemark As String = ""
    'Hemant (25 May 2021) -- End
    'Hemant (20 Aug 2021) -- Start
    'ISSUE/ENHANCEMENT : OLD-447 - Give option to accommodate line manager feedback. It should be possible to mark questions as either for Manager or for Employee.
    Public pblnIsDaysAfterFeedbackSubmitted As Boolean
    Public pdtDaysAfterFeedbackSubmittedDate As Date = Nothing
    Public pstrDaysAfterSubmittedRemark As String = ""
    'Hemant (20 Aug 2021) -- End
    'Hemant (01 Sep 2021) -- Start
    'ISSUE/ENHANCEMENT : OLD-459 - SAVE button on Evaluation Forms in addition to the SUBMIT button.
    Private pblnIsPreTrainingFeedbackSubmitted As Boolean
    Private pdtPreTrainingFeedbackSubmittedDate As Date = Nothing
    Private pblnIsPostTrainingFeedbackSubmitted As Boolean
    Private pdtPostTrainingFeedbackSubmittedDate As Date = Nothing
    Private pblnIsDaysAfterLineManagerFeedbackSubmitted As Boolean
    Private pdtDaysAfterLineManagerFeedbackSubmittedDate As Date = Nothing
    'Hemant (01 Sep 2021) -- End
    'Hemant (09 Feb 2022) -- Start            
    'OLD-551(NMB) : New config setting to allow Training Budget settings to be based on any allocation - Cost Center etc. 
    Public pdecTrainingCostEmp As Decimal
    Public pdecApprovedAmountEmp As Decimal
    Public pintInsertFormId As Integer
    'Hemant (09 Feb 2022) -- End
    'Sohail (15 Mar 2022) -- Start
    'Enhancement : OLD-583 : NMB - Modification of Training Request Approval Screen to allow batch approvals.
    Public pstrRefno As String = ""
    Public pintCreateuserunkid As Integer = 0
    Public pintCreateloginEmployeeunkid As Integer = 0
    'Sohail (15 Mar 2022) -- End
    'Hemant (29 Apr 2022) -- Start
    'ISSUE/ENHANCEMENT : AC2-314(NMB) - NMB - As a manager, I want different approval flow (Training requests) for different training courses.
    Public pintTrainingTypeId As Integer
    Public pblnIsLocalTravelling As Boolean
    'Hemant (29 Apr 2022) -- End
    'Hemant (25 Jul 2022) -- Start            
    'ENHANCEMENT(NMB) : AC2-723 - Implementation of Approver in Training request form
    Private pintTrainingApprovalSettingID As Integer
    'Hemant (25 Jul 2022) -- End
    'Hemant (12 Oct 2022) -- Start
    'ENHANCEMENT(NMB) :  AC2-952 - As a user, I want to have a document attachment option on the group training request page
    Private pintGroupTrainingRequestunkid As Integer
    'Hemant (12 Oct 2022) -- End
#End Region

#Region " Properties "
    ''' <summary>
    ''' Purpose: Get Message from Class 
    ''' Modify By: Hemant
    ''' </summary>
    Public ReadOnly Property _Message() As String
        Get
            Return mstrMessage
        End Get
    End Property

    Private mDataOp As clsDataOperation = Nothing
    Public Property _DataOp() As clsDataOperation
        Get
            Return mDataOp
        End Get
        Set(ByVal value As clsDataOperation)
            mDataOp = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set trainingrequestunkid
    ''' Modify By: Hemant
    ''' </summary>
    Public Property _TrainingRequestunkid() As Integer
        Get
            Return mintTrainingRequestunkid
        End Get
        Set(ByVal value As Integer)
            mintTrainingRequestunkid = value
            Call GetData()
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set application_date
    ''' Modify By: Pinkal
    ''' </summary>
    Public Property _Application_Date() As Date
        Get
            Return mdtApplication_Date
        End Get
        Set(ByVal value As Date)
            mdtApplication_Date = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set employeeunkid
    ''' Modify By: Hemant
    ''' </summary>
    Public Property _Employeeunkid() As Integer
        Get
            Return mintEmployeeunkid
        End Get
        Set(ByVal value As Integer)
            mintEmployeeunkid = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set coursemasterunkid
    ''' Modify By: Hemant
    ''' </summary>
    Public Property _Coursemasterunkid() As Integer
        Get
            Return mintCourseMasterunkid
        End Get
        Set(ByVal value As Integer)
            mintCourseMasterunkid = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set isscheduled
    ''' Modify By: Hemant
    ''' </summary>
    Public Property _IsScheduled() As Boolean
        Get
            Return mblnIsScheduled
        End Get
        Set(ByVal value As Boolean)
            mblnIsScheduled = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set start_date
    ''' Modify By: Hemant
    ''' </summary>
    Public Property _Start_Date() As Date
        Get
            Return mdtStart_Date
        End Get
        Set(ByVal value As Date)
            mdtStart_Date = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set end_date
    ''' Modify By: Hemant
    ''' </summary>
    Public Property _End_Date() As Date
        Get
            Return mdtEnd_Date
        End Get
        Set(ByVal value As Date)
            mdtEnd_Date = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set provider_name
    ''' Modify By: Hemant
    ''' </summary>
    Public Property _ProviderName() As String
        Get
            Return mstrProviderName
        End Get
        Set(ByVal value As String)
            mstrProviderName = value
        End Set
    End Property

    Public Property _ProviderAddress() As String
        Get
            Return mstrProviderAddress
        End Get
        Set(ByVal value As String)
            mstrProviderAddress = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set fundingsourceunkid
    ''' Modify By: Hemant
    ''' </summary>
    Public Property _FundingSourceunkid() As Integer
        Get
            Return mintFundingSourceunkid
        End Get
        Set(ByVal value As Integer)
            mintFundingSourceunkid = value
        End Set
    End Property

    Public Property _TotalTrainingCost() As Decimal
        Get
            Return mdecTotalTrainingCost
        End Get
        Set(ByVal value As Decimal)
            mdecTotalTrainingCost = value
        End Set
    End Property

    Public Property _ApprovedCost() As Decimal
        Get
            Return mdecApprovedCost
        End Get
        Set(ByVal value As Decimal)
            mdecApprovedCost = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set approvertranunkid
    ''' Modify By: Hemant
    ''' </summary>
    Public Property _Approvertranunkid() As Integer
        Get
            Return mintApproverTranunkid
        End Get
        Set(ByVal value As Integer)
            mintApproverTranunkid = value
        End Set
    End Property

    Public Property _IsAlignedCurrentRole() As Boolean
        Get
            Return mblnIsAlignedCurrentRole
        End Get
        Set(ByVal value As Boolean)
            mblnIsAlignedCurrentRole = value
        End Set
    End Property

    Public Property _IsPartofPDP() As Boolean
        Get
            Return mblnIsPartofPDP
        End Get
        Set(ByVal value As Boolean)
            mblnIsPartofPDP = value
        End Set
    End Property


    'Hemant (29 Apr 2022) -- Start
    'ISSUE/ENHANCEMENT : AC2-314(NMB) - NMB - As a manager, I want different approval flow (Training requests) for different training courses.
    Public Property _IsLocalTravelling() As Boolean
        Get
            Return mblnIsLocalTravelling
        End Get
        Set(ByVal value As Boolean)
            mblnIsLocalTravelling = value
        End Set
    End Property
    'Hemant (29 Apr 2022) -- End


    Public Property _IsForeignTravelling() As Boolean
        Get
            Return mblnIsForeignTravelling
        End Get
        Set(ByVal value As Boolean)
            mblnIsForeignTravelling = value
        End Set
    End Property

    Public Property _ExpectedReturn() As String
        Get
            Return mstrExpectedReturn
        End Get
        Set(ByVal value As String)
            mstrExpectedReturn = value
        End Set
    End Property

    Public Property _Remarks() As String
        Get
            Return mstrRemarks
        End Get
        Set(ByVal value As String)
            mstrRemarks = value
        End Set
    End Property

    Public Property _Statusunkid() As Integer
        Get
            Return mintStatusunkid
        End Get
        Set(ByVal value As Integer)
            mintStatusunkid = value
        End Set
    End Property

    Public Property _IsSubmitApproval() As Boolean
        Get
            Return mblnIsSubmitApproval
        End Get
        Set(ByVal value As Boolean)
            mblnIsSubmitApproval = value
        End Set
    End Property

    Public Property _Userunkid() As Integer
        Get
            Return mintUserunkid
        End Get
        Set(ByVal value As Integer)
            mintUserunkid = value
        End Set
    End Property

    Public Property _LoginEmployeeunkid() As Integer
        Get
            Return mintLoginEmployeeunkid
        End Get
        Set(ByVal value As Integer)
            mintLoginEmployeeunkid = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set isvoid
    ''' Modify By: Hemant
    ''' </summary>
    Public Property _Isvoid() As Boolean
        Get
            Return mblnIsvoid
        End Get
        Set(ByVal value As Boolean)
            mblnIsvoid = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set voiduserunkid
    ''' Modify By: Hemant
    ''' </summary>
    Public Property _Voiduserunkid() As Integer
        Get
            Return mintVoiduserunkid
        End Get
        Set(ByVal value As Integer)
            mintVoiduserunkid = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set voidloginemployeeunkid
    ''' Modify By: Hemant
    ''' </summary>
    Public Property _VoidLoginEmployeeunkid() As Integer
        Get
            Return mintVoidLoginEmployeeunkid
        End Get
        Set(ByVal value As Integer)
            mintVoidLoginEmployeeunkid = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set voiddatetime
    ''' Modify By: Hemant
    ''' </summary>
    Public Property _Voiddatetime() As Date
        Get
            Return mdtVoiddatetime
        End Get
        Set(ByVal value As Date)
            mdtVoiddatetime = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set voidreason
    ''' Modify By: Hemant
    ''' </summary>
    Public Property _Voidreason() As String
        Get
            Return mstrVoidreason
        End Get
        Set(ByVal value As String)
            mstrVoidreason = value
        End Set
    End Property

    Public WriteOnly Property _HostName() As String
        Set(ByVal value As String)
            mstrHostName = value
        End Set
    End Property

    Public WriteOnly Property _ClientIP() As String
        Set(ByVal value As String)
            mstrClientIP = value
        End Set
    End Property

    Public WriteOnly Property _AuditUserId() As Integer
        Set(ByVal value As Integer)
            mintAuditUserId = value
        End Set
    End Property

    Public WriteOnly Property _IsWeb() As Boolean
        Set(ByVal value As Boolean)
            mblnIsWeb = value
        End Set
    End Property

    Public WriteOnly Property _FormName() As String
        Set(ByVal value As String)
            mstrFormName = value
        End Set
    End Property

    Public Property _MinApprovedPriority() As Integer
        Get
            Return mintMinApprovedPriority
        End Get
        Set(ByVal value As Integer)
            mintMinApprovedPriority = value
        End Set
    End Property

    Public Property _MaxPriority() As Integer
        Get
            Return mintMaxPriority
        End Get
        Set(ByVal value As Integer)
            mintMaxPriority = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set trainingproviderunkid
    ''' Modify By: Hemant
    ''' </summary>
    Public Property _Trainingproviderunkid() As Integer
        Get
            Return mintTrainingproviderunkid
        End Get
        Set(ByVal value As Integer)
            mintTrainingproviderunkid = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set trainingvenueunkid
    ''' Modify By: Hemant
    ''' </summary>
    Public Property _Trainingvenueunkid() As Integer
        Get
            Return mintTrainingvenueunkid
        End Get
        Set(ByVal value As Integer)
            mintTrainingvenueunkid = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set departmentaltrainingneedunkid 
    ''' Modify By: Hemant
    ''' </summary>
    Public Property _DepartmentalTrainingNeedunkid() As Integer
        Get
            Return mintDepartmentaltrainingneedunkid
        End Get
        Set(ByVal value As Integer)
            mintDepartmentaltrainingneedunkid = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set isenrollconfirm 
    ''' Modify By: Hemant
    ''' </summary>
    Public Property _IsEnrollConfirm() As Boolean
        Get
            Return mblnIsEnrollConfirm
        End Get
        Set(ByVal value As Boolean)
            mblnIsEnrollConfirm = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set isenrollreject 
    ''' Modify By: Hemant
    ''' </summary>
    Public Property _IsEnrollReject() As Boolean
        Get
            Return mblnIsEnrollReject
        End Get
        Set(ByVal value As Boolean)
            mblnIsEnrollReject = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set enrollamount
    ''' Modify By: Hemant
    ''' </summary>
    Public Property _EnrollAmount() As Decimal
        Get
            Return mdecEnrollAmount
        End Get
        Set(ByVal value As Decimal)
            mdecEnrollAmount = value
        End Set
    End Property

    Public Property _TrainingStatusunkid() As Integer
        Get
            Return mintTrainingStatusunkid
        End Get
        Set(ByVal value As Integer)
            mintTrainingStatusunkid = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set qualificationgroupunkid
    ''' Modify By: Hemant Morker
    ''' </summary>
    Public Property _Qualificationgroupunkid() As Integer
        Get
            Return mintQualificationGroupunkid
        End Get
        Set(ByVal value As Integer)
            mintQualificationGroupunkid = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set qualificationunkid
    ''' Modify By: Hemant Morker
    ''' </summary>
    Public Property _Qualificationunkid() As Integer
        Get
            Return mintQualificationunkid
        End Get
        Set(ByVal value As Integer)
            mintQualificationunkid = value
        End Set
    End Property


    ''' <summary>
    ''' Purpose: Get or Set resulunkid
    ''' Modify By: Hemant Morker
    ''' </summary>
    Public Property _Resultunkid() As Integer
        Get
            Return mintResultunkid
        End Get
        Set(ByVal value As Integer)
            mintResultunkid = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set GPAcode
    ''' Modify By: Hemant Morker
    ''' </summary>
    Public Property _GPAcode() As Decimal
        Get
            Return mdecGPAcode
        End Get
        Set(ByVal value As Decimal)
            mdecGPAcode = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set isqualificaionupdated
    ''' Modify By: Hemant Morker
    ''' </summary>
    Public Property _Isqualificaionupdated() As Boolean
        Get
            Return mblnIsqualificaionupdated
        End Get
        Set(ByVal value As Boolean)
            mblnIsqualificaionupdated = value
        End Set
    End Property

    Public Property _IsCompletedSubmitApproval() As Boolean
        Get
            Return mblnIsCompletedSubmitApproval
        End Get
        Set(ByVal value As Boolean)
            mblnIsCompletedSubmitApproval = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set completed_vstatusunkid
    ''' Modify By: Hemant
    ''' </summary>
    Public Property _CompletedStatusunkid() As Integer
        Get
            Return mintCompletedStatusunkid
        End Get
        Set(ByVal value As Integer)
            mintCompletedStatusunkid = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set periodunkid
    ''' Modify By: Hemant
    ''' </summary>
    Public Property _Periodunkid() As Integer
        Get
            Return mintPeriodunkid
        End Get
        Set(ByVal value As Integer)
            mintPeriodunkid = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set other_Qualificationgrp
    ''' Modify By: Hemant
    ''' </summary>
    Public Property _Other_QualificationGrp() As String
        Get
            Return mstrOtherQualificationGrp
        End Get
        Set(ByVal value As String)
            mstrOtherQualificationGrp = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set other_Qualification
    ''' Modify By: Hemant
    ''' </summary>
    Public Property _Other_Qualification() As String
        Get
            Return mstrOtherQualification
        End Get
        Set(ByVal value As String)
            mstrOtherQualification = value
        End Set
    End Property


    ''' <summary>
    ''' Purpose: Get or Set other_ResultCode
    ''' Modify By: Hemant
    ''' </summary>
    Public Property _other_ResultCode() As String
        Get
            Return mstrOtherResultCode
        End Get
        Set(ByVal value As String)
            mstrOtherResultCode = value
        End Set
    End Property

    Public Property _CompletedUserunkid() As Integer
        Get
            Return mintCompletedUserunkid
        End Get
        Set(ByVal value As Integer)
            mintCompletedUserunkid = value
        End Set
    End Property

    'Hemant (25 May 2021) -- Start
    'ISSUE/ENHANCEMENT : OLD-393 - Training module bug fixes and enhancements
    Public Property _CompletedRemark() As String
        Get
            Return mstrCompletedRemark
        End Get
        Set(ByVal value As String)
            mstrCompletedRemark = value
        End Set
    End Property

    Public Property _EnrollmentRemark() As String
        Get
            Return mstrEnrollmentRemark
        End Get
        Set(ByVal value As String)
            mstrEnrollmentRemark = value
        End Set
    End Property
    'Hemant (25 May 2021) -- End

    'Sohail (15 Mar 2022) -- Start
    'Enhancement : OLD-583 : NMB - Modification of Training Request Approval Screen to allow batch approvals.
    Public Property _Refno() As String
        Get
            Return mstrRefno
        End Get
        Set(ByVal value As String)
            mstrRefno = value
        End Set
    End Property

    Public Property _Createuserunkid() As Integer
        Get
            Return mintCreateuserunkid
        End Get
        Set(ByVal value As Integer)
            mintCreateuserunkid = value
        End Set
    End Property

    Public Property _CreateloginEmployeeunkid() As Integer
        Get
            Return mintCreateloginEmployeeunkid
        End Get
        Set(ByVal value As Integer)
            mintCreateloginEmployeeunkid = value
        End Set
    End Property
    'Sohail (15 Mar 2022) -- End

    Private mlstFinancingSourceNew As List(Of clstraining_request_financing_sources_tran)
    Public WriteOnly Property _lstFinancingSourceNew() As List(Of clstraining_request_financing_sources_tran)
        Set(ByVal value As List(Of clstraining_request_financing_sources_tran))
            mlstFinancingSourceNew = value
        End Set
    End Property

    Private mlstFinancingSourceVoid As List(Of clstraining_request_financing_sources_tran)
    Public WriteOnly Property _lstFinancingSourceVoid() As List(Of clstraining_request_financing_sources_tran)
        Set(ByVal value As List(Of clstraining_request_financing_sources_tran))
            mlstFinancingSourceVoid = value
        End Set
    End Property

    Private mlstTrainingCostItemNew As List(Of clstraining_request_cost_tran)
    Public WriteOnly Property _lstTrainingCostItemNew() As List(Of clstraining_request_cost_tran)
        Set(ByVal value As List(Of clstraining_request_cost_tran))
            mlstTrainingCostItemNew = value
        End Set
    End Property

    Private mlstTrainingCostItemVoid As List(Of clstraining_request_cost_tran)
    Public WriteOnly Property _lstTrainingCostItemVoid() As List(Of clstraining_request_cost_tran)
        Set(ByVal value As List(Of clstraining_request_cost_tran))
            mlstTrainingCostItemVoid = value
        End Set
    End Property

    Public Property _Datasource_TrainingCostItem() As DataTable
        Get
            Return mdtTrainingCostItem
        End Get
        Set(ByVal value As DataTable)
            mdtTrainingCostItem = value
        End Set
    End Property

    'Hemant (28 Jul 2021) -- Start             
    'ENHANCEMENT : OLD-293 - Training Evaluation
    Public Property _CompletedApprovaldate() As Date
        Get
            Return mdtCompletedApprovaldate
        End Get
        Set(ByVal value As Date)
            mdtCompletedApprovaldate = value
        End Set
    End Property
    'Hemant (28 Jul 2021) -- End

    'Hemant (20 Aug 2021) -- Start
    'ISSUE/ENHANCEMENT : OLD-447 - Give option to accommodate line manager feedback. It should be possible to mark questions as either for Manager or for Employee.
    Public Property _IsDaysAfterFeedbackSubmitted() As Boolean
        Get
            Return mblnIsDaysAfterFeedbackSubmitted
        End Get
        Set(ByVal value As Boolean)
            mblnIsDaysAfterFeedbackSubmitted = value
        End Set
    End Property

    Public Property _DaysAfterFeedbackSubmittedDate() As Date
        Get
            Return mdtDaysAfterFeedbackSubmittedDate
        End Get
        Set(ByVal value As Date)
            mdtDaysAfterFeedbackSubmittedDate = value
        End Set
    End Property

    Public Property _DaysAfterFeedbackSubmittedRemark() As String
        Get
            Return mstrDaysAfterFeedbackSubmittedRemark
        End Get
        Set(ByVal value As String)
            mstrDaysAfterFeedbackSubmittedRemark = value
        End Set
    End Property
    'Hemant (20 Aug 2021) -- End
    'Hemant (01 Sep 2021) -- Start
    'ISSUE/ENHANCEMENT : OLD-459 - SAVE button on Evaluation Forms in addition to the SUBMIT button.
    Public Property _IsPreTrainingFeedbackSubmitted() As Boolean
        Get
            Return mblnIsPreTrainingFeedbackSubmitted
        End Get
        Set(ByVal value As Boolean)
            mblnIsPreTrainingFeedbackSubmitted = value
        End Set
    End Property

    Public Property _PreTrainingFeedbackSubmittedDate() As Date
        Get
            Return mdtPreTrainingFeedbackSubmittedDate
        End Get
        Set(ByVal value As Date)
            mdtPreTrainingFeedbackSubmittedDate = value
        End Set
    End Property

    Public Property _IsPostTrainingFeedbackSubmitted() As Boolean
        Get
            Return mblnIsPostTrainingFeedbackSubmitted
        End Get
        Set(ByVal value As Boolean)
            mblnIsPostTrainingFeedbackSubmitted = value
        End Set
    End Property

    Public Property _PostTrainingFeedbackSubmittedDate() As Date
        Get
            Return mdtPostTrainingFeedbackSubmittedDate
        End Get
        Set(ByVal value As Date)
            mdtPostTrainingFeedbackSubmittedDate = value
        End Set
    End Property

    Public Property _IsDaysAfterLineManagerFeedbackSubmitted() As Boolean
        Get
            Return mblnIsDaysAfterLineManagerFeedbackSubmitted
        End Get
        Set(ByVal value As Boolean)
            mblnIsDaysAfterLineManagerFeedbackSubmitted = value
        End Set
    End Property

    Public Property _DaysAfterLineManagerFeedbackSubmittedDate() As Date
        Get
            Return mdtDaysAfterLineManagerFeedbackSubmittedDate
        End Get
        Set(ByVal value As Date)
            mdtDaysAfterLineManagerFeedbackSubmittedDate = value
        End Set
    End Property
    'Hemant (01 Sep 2021) -- End

    'Hemant (23 Sep 2021) -- Start
    'ENHANCEMENT : OLD-470 - Silverlands - New Training Setting to skip Training Requests Process.
    Public Property _IsSkipTrainingRequestAndApproval() As Boolean
        Get
            Return mblnIsSkipTrainingRequestAndApproval
        End Get
        Set(ByVal value As Boolean)
            mblnIsSkipTrainingRequestAndApproval = value
        End Set
    End Property
    'Hemant (23 Sep 2021) -- End

    'Hemant (09 Feb 2022) -- Start            
    'OLD-551(NMB) : New config setting to allow Training Budget settings to be based on any allocation - Cost Center etc. 
    Public Property _TrainingCostEmp() As Decimal
        Get
            Return mdecTrainingCostEmp
        End Get
        Set(ByVal value As Decimal)
            mdecTrainingCostEmp = value
        End Set
    End Property

    Public Property _ApprovedAmountEmp() As Decimal
        Get
            Return mdecApprovedAmountEmp
        End Get
        Set(ByVal value As Decimal)
            mdecApprovedAmountEmp = value
        End Set
    End Property

    Public Property _InsertFormId() As Integer
        Get
            Return mintInsertFormId
        End Get
        Set(ByVal value As Integer)
            mintInsertFormId = value
        End Set
    End Property
    'Hemant (09 Feb 2022) -- End

    'Hemant (29 Apr 2022) -- Start
    'ISSUE/ENHANCEMENT : AC2-314(NMB) - NMB - As a manager, I want different approval flow (Training requests) for different training courses.
    Public Property _TrainingTypeId() As Integer
        Get
            Return mintTrainingTypeId
        End Get
        Set(ByVal value As Integer)
            mintTrainingTypeId = value
        End Set
    End Property

    'Hemant (29 Apr 2022) -- End
    'Hemant (25 Jul 2022) -- Start            
    'ENHANCEMENT(NMB) : AC2-723 - Implementation of Approver in Training request form
    Public Property _TrainingApprovalSettingID() As Integer
        Get
            Return mintTrainingApprovalSettingID
        End Get
        Set(ByVal value As Integer)
            mintTrainingApprovalSettingID = value
        End Set
    End Property
    'Hemant (25 Jul 2022) -- End

    'Hemant (12 Oct 2022) -- Start
    'ENHANCEMENT(NMB) :  AC2-952 - As a user, I want to have a document attachment option on the group training request page   
    Public Property _GroupTrainingRequestunkid() As Integer
        Get
            Return mintGroupTrainingRequestunkid
        End Get
        Set(ByVal value As Integer)
            mintGroupTrainingRequestunkid = value
        End Set
    End Property
    'Hemant (12 Oct 2022) -- End

    'Hemant (10 Nov 2022) -- Start
    'ENHANCEMENT(NMB) :  AC2-1024 - As a user, I want to have a field to capture the Training Venue on both individual and group training request pages. This filed will be free text and mandatory
    Public Property _Venue() As String
        Get
            Return mstrVenue
        End Get
        Set(ByVal value As String)
            mstrVenue = value
        End Set
    End Property
    'Hemant (27 Oct 2022) -- End

#End Region

    
    Public Enum enEmailType
        Training_Approver = 1
        Completed_Training_Approver = 2
    End Enum


    ''' <summary>
    ''' Modify By: Hemant
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>
    Public Sub GetData()
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception
        Dim objDataOperation As clsDataOperation
        objDataOperation = New clsDataOperation

        Try
            strQ = "SELECT " & _
                       "trainingrequestunkid " & _
                       ",application_date " & _
                       ",employeeunkid " & _
                       ",coursemasterunkid " & _
                       ",isscheduled " & _
                       ",start_date " & _
                       ",end_date " & _
                       ",provider_name " & _
                       ",provider_address " & _
                       ",fundingsourceunkid " & _
                       ",totaltrainingcost " & _
                       ",approvedamount " & _
                       ",approvertranunkid " & _
                       ",isalignedcurrentrole " & _
                       ",ispartofpdp " & _
                       ",isforeigntravelling " & _
                       ",expectedreturn " & _
                       ",remarks " & _
                       ",statusunkid " & _
                       ",issubmit_approval " & _
                       ",userunkid " & _
                       ",loginemployeeunkid " & _
                       ",isvoid " & _
                       ",voiduserunkid " & _
                       ",voidloginemployeeunkid " & _
                       ",voiddatetime " & _
                       ",voidreason " & _
                       ", trainingproviderunkid " & _
                       ", trainingvenueunkid " & _
                       ", departmentaltrainingneedunkid " & _
                       ", isenroll_confirm " & _
                       ", isenroll_reject " & _
                       ", enroll_amount " & _
                       ", training_statusunkid " & _
                       ", qualificationgroupunkid " & _
                       ", qualificationunkid " & _
                       ", ISNULL(resultunkid,0) resultunkid " & _
                       ", ISNULL(gpacode,0)  gpacode " & _
                       ", ISNULL(isqualificaionupdated, 0) AS isqualificaionupdated " & _
                       ", iscompleted_submit_approval " & _
                       ", completed_statusunkid " & _
                       ", periodunkid " & _
                       ", other_qualificationgrp " & _
                       ", other_qualification " & _
                       ", other_resultcode " & _
                       ", completed_userunkid " & _
                       ", completed_remark " & _
                       ", enrollment_remark " & _
                       ", completed_approval_date " & _
                       ", ISNULL(isdaysafterfeedback_submitted, 0) AS isdaysafterfeedback_submitted  " & _
                       ", daysafter_submitted_date " & _
                       ", daysafter_submitted_remark " & _
                       ", ISNULL(ispretrainingfeedback_submitted, 0) AS ispretrainingfeedback_submitted  " & _
                       ", pretraining_submitted_date " & _
                       ", ISNULL(isposttrainingfeedback_submitted, 0) AS isposttrainingfeedback_submitted  " & _
                       ", posttraining_submitted_date " & _
                       ", ISNULL(isdaysafter_linemanager_submitted, 0) AS isdaysafter_linemanager_submitted  " & _
                       ", daysafter_linemanager_submitted_date " & _
                       ", ISNULL(isskip_trainingrequest_and_approval, 0) AS isskip_trainingrequest_and_approval  " & _
                       ", ISNULL(trainingcostemp,0) AS trainingcostemp " & _
                       ", ISNULL(approvedamountemp,0) AS approvedamountemp " & _
                       ", ISNULL(insertformid, 0) AS insertformid " & _
                       ", ISNULL(trtraining_request_master.refno, '') AS refno " & _
                       ", ISNULL(trtraining_request_master.createuserunkid, 0) AS createuserunkid " & _
                       ", ISNULL(trtraining_request_master.createloginemployeeunkid, 0) AS createloginemployeeunkid " & _
                       ", ISNULL(trtraining_request_master.trainingtypeid, 0) AS trainingtypeid " & _
                       ", ISNULL(trtraining_request_master.approvalsettingid, 0) AS approvalsettingid " & _
                       ", ISNULL(trtraining_request_master.grouptrainingrequestunkid, 0) AS grouptrainingrequestunkid " & _
                       ", ISNULL(venue, '') AS venue " & _
             "FROM trtraining_request_master " & _
             "WHERE trainingrequestunkid = @trainingrequestunkid "
            'Hemant (10 Nov 2022) -- [venue]
            'Hemant (12 Oct 2022) -- [grouptrainingrequestunkid]
            'Hemant (25 Jul 2022) -- [approvalsettingid]
            'Hemant (29 Apr 2022) -- [trainingtypeid]
            'Sohail (15 Mar 2022) - [refno, createuserunkid, createloginemployeeunkid]
            'Hemant (09 Feb 2022) -- [trainingcostemp,approvedamountemp,insertformid]
            'Hemant (23 Sep 2021) -- [isskip_trainingrequest_and_approval]
            'Hemant (01 Sep 2021) -- [ispretrainingfeedback_submitted,pretraining_submitted_date,isposttrainingfeedback_submitted,posttraining_submitted_date,isdaysafter_linemanager_submitted,daysafter_linemanager_submitted_date]
            'Hemant (20 Aug 2021) -- [isdaysafterfeedback_submitted,daysafter_submitted_date,daysafter_submitted_remark]
            'Hemant (28 Jul 2021) -- [completed_approval_date]
            'Hemant (25 May 2021) -- [completed_remark,enrollment_remark] 
            objDataOperation.AddParameter("@trainingrequestunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintTrainingRequestunkid.ToString)

            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            For Each dtRow As DataRow In dsList.Tables(0).Rows
                mintTrainingRequestunkid = CInt(dtRow.Item("trainingrequestunkid"))
                mdtApplication_Date = CDate(dtRow.Item("application_date"))
                mintEmployeeunkid = CInt(dtRow.Item("employeeunkid"))
                mintCourseMasterunkid = CInt(dtRow.Item("coursemasterunkid"))
                mblnIsScheduled = CBool(dtRow.Item("isscheduled"))
                mdtStart_Date = CDate(dtRow.Item("start_date"))
                mdtEnd_Date = CDate(dtRow.Item("end_date"))
                mstrProviderName = dtRow.Item("provider_name")
                mstrProviderAddress = dtRow.Item("provider_address")
                mintFundingSourceunkid = CInt(dtRow.Item("fundingsourceunkid"))
                mdecTotalTrainingCost = CDec(dtRow.Item("totaltrainingcost"))
                mdecApprovedCost = CDec(dtRow.Item("approvedamount"))
                mintApproverTranunkid = CInt(dtRow.Item("approvertranunkid"))
                mblnIsAlignedCurrentRole = CBool(dtRow.Item("isalignedcurrentrole"))
                mblnIsPartofPDP = CBool(dtRow.Item("ispartofpdp"))
                mblnIsForeignTravelling = CBool(dtRow.Item("isforeigntravelling"))
                mstrExpectedReturn = dtRow.Item("expectedreturn")
                mstrRemarks = dtRow.Item("remarks")
                mintStatusunkid = CInt(dtRow.Item("statusunkid"))
                mblnIsSubmitApproval = CBool(dtRow.Item("issubmit_approval"))
                mintUserunkid = CInt(dtRow.Item("userunkid"))
                mintLoginEmployeeunkid = CInt(dtRow.Item("loginemployeeunkid"))
                mblnIsvoid = CBool(dtRow.Item("isvoid"))
                mintVoiduserunkid = CInt(dtRow.Item("voiduserunkid"))
                mintVoidLoginEmployeeunkid = CInt(dtRow.Item("voidloginemployeeunkid"))
                If Not IsDBNull(dtRow.Item("voiddatetime")) Then
                    mdtVoiddatetime = dtRow.Item("voiddatetime")
                End If
                mstrVoidreason = dtRow.Item("voidreason").ToString
                mintTrainingproviderunkid = CInt(dtRow.Item("trainingproviderunkid"))
                mintTrainingvenueunkid = CInt(dtRow.Item("trainingvenueunkid"))
                mintDepartmentaltrainingneedunkid = CInt(dtRow.Item("departmentaltrainingneedunkid"))
                mblnIsEnrollConfirm = CBool(dtRow.Item("isenroll_confirm"))
                mblnIsEnrollReject = CBool(dtRow.Item("isenroll_reject"))
                mdecEnrollAmount = CDec(dtRow.Item("enroll_amount"))
                mintTrainingStatusunkid = CInt(dtRow.Item("training_statusunkid"))
                mintQualificationGroupunkid = CInt(dtRow.Item("qualificationgroupunkid"))
                mintQualificationunkid = CInt(dtRow.Item("qualificationunkid"))
                mintResultunkid = CInt(dtRow.Item("resultunkid").ToString())
                mdecGPAcode = CDec(dtRow.Item("gpacode").ToString())
                mblnIsqualificaionupdated = CBool(dtRow.Item("isqualificaionupdated"))
                mblnIsCompletedSubmitApproval = CBool(dtRow.Item("iscompleted_submit_approval"))
                mintCompletedStatusunkid = CInt(dtRow.Item("completed_statusunkid"))
                mintPeriodunkid = CInt(dtRow.Item("periodunkid"))
                mstrOtherQualificationGrp = dtRow.Item("other_qualificationgrp").ToString()
                mstrOtherQualification = dtRow.Item("other_qualification").ToString()
                mstrOtherResultCode = dtRow.Item("other_resultcode").ToString()
                mintCompletedUserunkid = CInt(dtRow.Item("completed_userunkid"))
                'Hemant (25 May 2021) -- Start
                'ISSUE/ENHANCEMENT : OLD-393 - Training module bug fixes and enhancements
                mstrCompletedRemark = dtRow.Item("completed_remark")
                mstrEnrollmentRemark = dtRow.Item("enrollment_remark")
                'Hemant (25 May 2021) -- End
                'Hemant (28 Jul 2021) -- Start             
                'ENHANCEMENT : OLD-293 - Training Evaluation
                If Not IsDBNull(dtRow.Item("completed_approval_date")) Then
                    mdtCompletedApprovaldate = dtRow.Item("completed_approval_date")
                End If
                'Hemant (28 Jul 2021) -- End
                'Hemant (20 Aug 2021) -- Start
                'ISSUE/ENHANCEMENT : OLD-447 - Give option to accommodate line manager feedback. It should be possible to mark questions as either for Manager or for Employee.
                mblnIsDaysAfterFeedbackSubmitted = CBool(dtRow.Item("isdaysafterfeedback_submitted"))
                If Not IsDBNull(dtRow.Item("daysafter_submitted_date")) Then
                    mdtDaysAfterFeedbackSubmittedDate = dtRow.Item("daysafter_submitted_date")
                End If
                mstrDaysAfterFeedbackSubmittedRemark = dtRow.Item("daysafter_submitted_remark")
                'Hemant (20 Aug 2021) -- End
                'Hemant (01 Sep 2021) -- Start
                'ISSUE/ENHANCEMENT : OLD-459 - SAVE button on Evaluation Forms in addition to the SUBMIT button.
                mblnIsPreTrainingFeedbackSubmitted = CBool(dtRow.Item("ispretrainingfeedback_submitted"))
                If Not IsDBNull(dtRow.Item("pretraining_submitted_date")) Then
                    mdtPreTrainingFeedbackSubmittedDate = dtRow.Item("pretraining_submitted_date")
                End If
                mblnIsPostTrainingFeedbackSubmitted = CBool(dtRow.Item("isposttrainingfeedback_submitted"))
                If Not IsDBNull(dtRow.Item("posttraining_submitted_date")) Then
                    mdtPostTrainingFeedbackSubmittedDate = dtRow.Item("posttraining_submitted_date")
                End If
                mblnIsDaysAfterLineManagerFeedbackSubmitted = CBool(dtRow.Item("isdaysafter_linemanager_submitted"))
                If Not IsDBNull(dtRow.Item("daysafter_linemanager_submitted_date")) Then
                    mdtDaysAfterLineManagerFeedbackSubmittedDate = dtRow.Item("daysafter_linemanager_submitted_date")
                End If
                'Hemant (01 Sep 2021) -- End
                'Hemant (23 Sep 2021) -- Start
                'ENHANCEMENT : OLD-470 - Silverlands - New Training Setting to skip Training Requests Process.
                mblnIsSkipTrainingRequestAndApproval = CBool(dtRow.Item("isskip_trainingrequest_and_approval"))
                'Hemant (23 Sep 2021) -- End
                'Hemant (09 Feb 2022) -- Start            
                'OLD-551(NMB) : New config setting to allow Training Budget settings to be based on any allocation - Cost Center etc. 
                mdecTrainingCostEmp = CDec(dtRow.Item("trainingcostemp"))
                mdecApprovedAmountEmp = CDec(dtRow.Item("approvedamountemp"))
                mintInsertFormId = CInt(dtRow.Item("insertformid"))
                'Hemant (09 Feb 2022) -- End
                'Sohail (15 Mar 2022) -- Start
                'Enhancement : OLD-583 : NMB - Modification of Training Request Approval Screen to allow batch approvals.
                mstrRefno = dtRow.Item("refno").ToString
                mintCreateuserunkid = CInt(dtRow.Item("createuserunkid"))
                mintCreateloginEmployeeunkid = CInt(dtRow.Item("createloginemployeeunkid"))
                'Sohail (15 Mar 2022) -- End
                'Hemant (29 Apr 2022) -- Start
                'ISSUE/ENHANCEMENT : AC2-314(NMB) - NMB - As a manager, I want different approval flow (Training requests) for different training courses.
                mintTrainingTypeId = CInt(dtRow.Item("trainingtypeid"))
                'Hemant (29 Apr 2022) -- End
                'Hemant (25 Jul 2022) -- Start            
                'ENHANCEMENT(NMB) : AC2-724 - Implementation of Approval in Training request Approval Form
                mintTrainingApprovalSettingID = CInt(dtRow.Item("approvalsettingid"))
                'Hemant (25 Jul 2022) -- End
                'Hemant (12 Oct 2022) -- Start
                'ENHANCEMENT(NMB) :  AC2-952 - As a user, I want to have a document attachment option on the group training request page
                mintGroupTrainingRequestunkid = CInt(dtRow.Item("grouptrainingrequestunkid"))
                'Hemant (12 Oct 2022) -- End
                'Hemant (10 Nov 2022) -- Start
                'ENHANCEMENT(NMB) :  AC2-1024 - As a user, I want to have a field to capture the Training Venue on both individual and group training request pages. This filed will be free text and mandatory
                mstrVenue = dtRow.Item("venue")
                'Hemant (27 Oct 2022) -- End
                Exit For
            Next
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: GetData; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
    End Sub

    ''' <summary>
    ''' Modify By: Hemant
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>    ''' 
    Public Function GetList(ByVal xDatabaseName As String, _
                            ByVal xUserUnkid As Integer, _
                            ByVal xYearUnkid As Integer, _
                            ByVal xCompanyUnkid As Integer, _
                            ByVal xPeriodStart As DateTime, _
                            ByVal xPeriodEnd As DateTime, _
                            ByVal xUserModeSetting As String, _
                            ByVal xOnlyApproved As Boolean, _
                            ByVal xIncludeIn_ActiveEmployee As Boolean, _
                            ByVal strTableName As String, _
                            ByVal xDeptAllocationId As Integer, _
                            Optional ByVal intStatusID As Integer = 0, _
                            Optional ByVal mstrFilter As String = "", _
                            Optional ByVal blnApplyUserAccessFilter As Boolean = True, _
                            Optional ByVal IsUsedAsMSS As Boolean = True, _
                            Optional ByVal blnAddGrouping As Boolean = False) As DataSet
        'Hemant (07 Mar 2022) -- [xDeptAllocationId,blnAddGrouping]
        Dim dsList As DataSet = Nothing
        Dim dsTrainingApproval As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        Dim objDataOperation As clsDataOperation
        objDataOperation = New clsDataOperation

        Try
            Dim xDateJoinQry, xDateFilterQry, xUACQry, xUACFiltrQry, xAdvanceJoinQry As String
            xDateJoinQry = "" : xDateFilterQry = "" : xUACQry = "" : xUACFiltrQry = "" : xAdvanceJoinQry = ""
            Call GetDatesFilterString(xDateJoinQry, xDateFilterQry, xPeriodStart, xPeriodEnd, , , xDatabaseName)
            Call GetAdvanceFilterQry(xAdvanceJoinQry, xPeriodEnd, xDatabaseName)

            If blnApplyUserAccessFilter = True Then
                If IsUsedAsMSS Then Call NewAccessLevelFilterString(xUACQry, xUACFiltrQry, xPeriodEnd, xOnlyApproved, xDatabaseName, xUserUnkid, xCompanyUnkid, xYearUnkid, xUserModeSetting)
            End If

            'Hemant (07 Mar 2022) -- Start            
            'ISSUE/ENHANCEMENT(NMB) : OLD-585 - NMB - Show consolidated total training cost and approved amount on Group training requests on Individual requests
            Dim strTable As String = ""
            Dim strUnkIdField As String = ""
            Dim strCodeField As String = ""
            Dim strNameField As String = ""

            Select Case xDeptAllocationId

                Case enAllocation.BRANCH

                    strTable = "hrstation_master"
                    strUnkIdField = "stationunkid"
                    strCodeField = "code"
                    strNameField = "name"

                Case enAllocation.DEPARTMENT_GROUP

                    strTable = "hrdepartment_group_master"
                    strUnkIdField = "deptgroupunkid"
                    strCodeField = "code"
                    strNameField = "name"

                Case enAllocation.DEPARTMENT

                    strTable = "hrdepartment_master"
                    strUnkIdField = "departmentunkid"
                    strCodeField = "code"
                    strNameField = "name"

                Case enAllocation.SECTION_GROUP

                    strTable = "hrsectiongroup_master"
                    strUnkIdField = "sectiongroupunkid"
                    strCodeField = "code"
                    strNameField = "name"

                Case enAllocation.SECTION

                    strTable = "hrsection_master"
                    strUnkIdField = "sectionunkid"
                    strCodeField = "code"
                    strNameField = "name"

                Case enAllocation.UNIT_GROUP

                    strTable = "hrunitgroup_master"
                    strUnkIdField = "unitgroupunkid"
                    strCodeField = "code"
                    strNameField = "name"

                Case enAllocation.UNIT

                    strTable = "hrunit_master"
                    strUnkIdField = "unitunkid"
                    strCodeField = "code"
                    strNameField = "name"

                Case enAllocation.TEAM

                    strTable = "hrteam_master"
                    strUnkIdField = "teamunkid"
                    strCodeField = "code"
                    strNameField = "name"

                Case enAllocation.JOB_GROUP

                    strTable = "hrjobgroup_master"
                    strUnkIdField = "jobgroupunkid"
                    strCodeField = "code"
                    strNameField = "name"

                Case enAllocation.JOBS

                    strTable = "hrjob_master"
                    strUnkIdField = "jobunkid"
                    strCodeField = "job_code"
                    strNameField = "job_name"

                Case enAllocation.CLASS_GROUP

                    strTable = "hrclassgroup_master"
                    strUnkIdField = "classgroupunkid"
                    strCodeField = "code"
                    strNameField = "name"

                Case enAllocation.CLASSES

                    strTable = "hrclasses_master"
                    strUnkIdField = "classesunkid"
                    strCodeField = "code"
                    strNameField = "name"

                Case enAllocation.COST_CENTER

                    strTable = "prcostcenter_master"
                    strUnkIdField = "costcenterunkid"
                    strCodeField = "customcode"
                    strNameField = "costcentername"


            End Select
            'Hemant (07 Mar 2022) -- End


            strQ = "SELECT " & _
                       "  trainingrequestunkid " & _
                       ", trtraining_request_master.application_date " & _
                       ", trtraining_request_master.employeeunkid " & _
                       ", ISNULL(hremployee_master.firstname,'')+' '+ISNULL(hremployee_master.othername,'')+' '+ISNULL(hremployee_master.surname,'') AS employeename " & _
                       ", coursemasterunkid " & _
                       ", ISNULL(cfcommon_master.name, '') AS Training " & _
                       ", isscheduled " & _
                       ", CASE " & _
                       "    WHEN isscheduled = 1 THEN @Yes " & _
                       "		ELSE @No " & _
                       "  END AS scheduled " & _
                       ", start_date " & _
                       ", end_date " & _
                       ", CONVERT(NVARCHAR(8),trtraining_request_master.start_date,112) AS SDate " & _
                       ", CONVERT(NVARCHAR(8),trtraining_request_master.end_date,112) AS EDate " & _
                       ", provider_name " & _
                       ", provider_address " & _
                       ", fundingsourceunkid " & _
                       ", totaltrainingcost AS TotalTrainingCost " & _
                       ", approvedamount AS ApprovedAmount " & _
                       ", approvertranunkid AS approvertranunkid " & _
                       ", isalignedcurrentrole " & _
                       ", ispartofpdp " & _
                       ", isforeigntravelling " & _
                       ", expectedreturn " & _
                       ", remarks " & _
                       ", trtraining_request_master.statusunkid " & _
                       ", CASE " & _
                       "       WHEN trtraining_request_master.statusunkid = 1 THEN @Pending " & _
                       "       WHEN trtraining_request_master.statusunkid = 2 THEN @Approved " & _
                       "       WHEN trtraining_request_master.statusunkid = 3 THEN @Rejected " & _
                       "  END AS Status " & _
                       ", issubmit_approval " & _
                       ", trtraining_request_master.userunkid " & _
                       ", trtraining_request_master.loginemployeeunkid " & _
                       ", trtraining_request_master.isvoid " & _
                       ", trtraining_request_master.voiduserunkid " & _
                       ", trtraining_request_master.voidloginemployeeunkid " & _
                       ", trtraining_request_master.voiddatetime " & _
                       ", trtraining_request_master.voidreason " & _
                       ", trtraining_request_master.trainingproviderunkid " & _
                       ", ISNULL(hrinstitute_master.institute_name, '') AS trainingprovidername " & _
                       ", trtraining_request_master.trainingvenueunkid " & _
                       ", ISNULL(trtrainingvenue_master.venuename, '') AS trainingvenuename " & _
                       ", trtraining_request_master.departmentaltrainingneedunkid " & _
                       ", trtraining_request_master.isenroll_confirm " & _
                       ", trtraining_request_master.isenroll_reject " & _
                       ", trtraining_request_master.enroll_amount " & _
                       ", CASE " & _
                       "       WHEN trtraining_request_master.isenroll_confirm = 1 THEN @Yes " & _
                       "       ELSE @No " & _
                       " END AS Enrolled " & _
                       ", training_statusunkid " & _
                       ", CASE " & _
                       "       WHEN trtraining_request_master.training_statusunkid = 1 THEN @OnHold " & _
                       "       WHEN trtraining_request_master.training_statusunkid = 2 THEN @PostPoned " & _
                       "       WHEN trtraining_request_master.training_statusunkid = 3 THEN @Cancel " & _
                       "  END AS Status " & _
                       ",  ISNULL(QG.masterunkid,0) AS QGrpId " & _
                       ",  ISNULL(QG.name, '') AS QGrp " & _
                       ",  ISNULL(hrqualification_master.qualificationunkid,0) AS QualifyId " & _
                       ",  ISNULL(hrqualification_master.qualificationname, '')  AS Qualify " & _
                       ",  ISNULL(trtraining_request_master.resultunkid,0) As resultunkid " & _
                       ",  ISNULL(hrresult_master.result_level,0) AS result_level " & _
                       ",  trtraining_request_master.gpacode As gpacode " & _
                       ",  ISNULL(trtraining_request_master.isqualificaionupdated, 0) AS isqualificaionupdated " & _
                       ",  iscompleted_submit_approval " & _
                       ", completed_statusunkid " & _
                       ", CASE " & _
                       "       WHEN trtraining_request_master.completed_statusunkid <= 1 THEN @Pending " & _
                       "       WHEN trtraining_request_master.completed_statusunkid = 2 THEN @Completed " & _
                       "       WHEN trtraining_request_master.completed_statusunkid = 3 THEN @Rejected " & _
                       "  END AS CompletedStatus " & _
                       ", ISNULL(trtraining_request_master.periodunkid,0) AS periodunkid " & _
                       ", ISNULL(other_qualificationgrp,'') AS other_qualificationgrp " & _
                       ", ISNULL(other_qualification,'') AS other_qualification " & _
                       ", ISNULL(other_resultcode,'') AS other_resultcode " & _
                       ", completed_userunkid " & _
                       ", completed_remark " & _
                       ", enrollment_remark " & _
                       ", ISNULL(completed_approval_date, '19000101') AS completed_approval_date " & _
                       ", ISNULL(isdaysafterfeedback_submitted, 0) AS isdaysafterfeedback_submitted  " & _
                       ", ISNULL(daysafter_submitted_date, '19000101') AS daysafter_submitted_date  " & _
                       ", daysafter_submitted_remark " & _
                       ", ISNULL(ispretrainingfeedback_submitted, 0) AS ispretrainingfeedback_submitted  " & _
                       ", ISNULL(pretraining_submitted_date, '19000101') AS pretraining_submitted_date  " & _
                       ", ISNULL(isposttrainingfeedback_submitted, 0) AS isposttrainingfeedback_submitted  " & _
                       ", ISNULL(posttraining_submitted_date, '19000101') AS posttraining_submitted_date  " & _
                       ", ISNULL(isdaysafter_linemanager_submitted, 0) AS isdaysafter_linemanager_submitted  " & _
                       ", ISNULL(daysafter_linemanager_submitted_date, '19000101') AS daysafter_linemanager_submitted_date  " & _
                       ", ISNULL(isskip_trainingrequest_and_approval, 0) AS isskip_trainingrequest_and_approval  " & _
                       ", trainingcostemp AS TrainingCostEmp " & _
                       ", approvedamountemp AS ApprovedAmountEmp " & _
                       ", ISNULL(trtraining_request_master.insertformid, 0) AS insertformid " & _
                       ", CAST(0 AS BIT) AS IsGrp " & _
                       ", ISNULL(trtraining_request_master.refno, '') AS refno " & _
                       ", ISNULL(trtraining_request_master.createuserunkid, 0) AS createuserunkid " & _
                       ", ISNULL(trtraining_request_master.createloginemployeeunkid, 0) AS createloginemployeeunkid " & _
                       ", CASE WHEN trtraining_request_master.createloginemployeeunkid > 0 THEN REPLACE(ReqEmp.firstname + ' ' + ISNULL(ReqEmp.othername, '') + ' ' + ReqEmp.surname, '  ', ' ') ELSE CASE WHEN RTRIM(ReqUser.firstname) <> '' THEN ReqUser.firstname + ' ' + ReqUser.lastname ELSE ReqUser.username END END AS CreateUserName " & _
                       ", ISNULL(trdepartmentaltrainingneed_master.allocationid, 0) AS allocationid " & _
                       ", ISNULL(trdepartmentaltrainingneed_master.departmentunkid, 0) AS allocationtranunkid " & _
                       ", CASE ISNULL(trdepartmentaltrainingneed_master.departmentunkid, -99) WHEN -99 THEN @Company ELSE ISNULL(" & strTable & "." & strCodeField & ",'') END AS allocationtrancode " & _
                       ", CASE ISNULL(trdepartmentaltrainingneed_master.departmentunkid, -99) WHEN -99 THEN @Company ELSE ISNULL(" & strTable & "." & strNameField & ",'') END AS allocationtranname " & _
                       ", ISNULL(trtraining_request_master.trainingtypeid, 0) AS trainingtypeid " & _
                       ", ISNULL(trtraining_request_master.approvalsettingid, 0) AS approvalsettingid " & _
                       ", ISNULL(trtraining_request_master.grouptrainingrequestunkid, 0) AS grouptrainingrequestunkid " & _
                       ", ISNULL(trtraining_request_master.venue, '') AS venue " & _
             " FROM trtraining_request_master " & _
             " JOIN hremployee_master ON hremployee_master.employeeunkid = trtraining_request_master.employeeunkid " & _
             " LEFT JOIN cfcommon_master ON cfcommon_master.masterunkid = trtraining_request_master.coursemasterunkid " & _
             " LEFT JOIN hrinstitute_master ON hrinstitute_master.instituteunkid = trtraining_request_master.trainingproviderunkid " & _
                            "AND hrinstitute_master.isactive = 1 " & _
              " LEFT JOIN trtrainingvenue_master ON trtrainingvenue_master.venueunkid = trtraining_request_master.trainingvenueunkid " & _
                            "AND trtrainingvenue_master.isactive = 1 " & _
             "  LEFT JOIN cfcommon_master QG ON QG.masterunkid = trtraining_request_master.qualificationgroupunkid AND cfcommon_master.mastertype = " & enCommonMaster.QUALIFICATION_COURSE_GROUP & _
             "  LEFT JOIN hrqualification_master ON trtraining_request_master.qualificationunkid = hrqualification_master.qualificationunkid " & _
             "  LEFT JOIN hrresult_master ON trtraining_request_master.resultunkid = hrresult_master.resultunkid " & _
             "  LEFT JOIN hrmsConfiguration..cfuser_master AS ReqUser ON trtraining_request_master.createuserunkid = ReqUser.userunkid " & _
             "  LEFT JOIN hremployee_master AS ReqEmp ON trtraining_request_master.createloginemployeeunkid = ReqEmp.employeeunkid " & _
             "  LEFT JOIN trdepartmentaltrainingneed_master ON trdepartmentaltrainingneed_master.departmentaltrainingneedunkid = trtraining_request_master.departmentaltrainingneedunkid " & _
             "  LEFT JOIN " & strTable & " ON " & strTable & "." & strUnkIdField & " = trdepartmentaltrainingneed_master.departmentunkid "
            'Hemant (10 Nov 2022) -- [venue]
            'Hemant (12 Oct 2022) -- [grouptrainingrequestunkid]
            'Hemant (25 Jul 2022) -- [approvalsettingid]
            'Hemant (29 Apr 2022) -- [trainingtypeid]
            'Sohail (15 Mar 2022) - [IsGrp, refno, createuserunkid, createloginemployeeunkid]
            'Hemant (09 Feb 2022) -- [TrainingCostEmp,ApprovedAmountEmp,insertformid]
            'Hemant (23 Sep 2021) -- [isskip_trainingrequest_and_approval]
            'Hemant (01 Sep 2021) -- [ispretrainingfeedback_submitted,pretraining_submitted_date,isposttrainingfeedback_submitted,posttraining_submitted_date,isdaysafter_linemanager_submitted,daysafter_linemanager_submitted_date]
            'Hemant (20 Aug 2021) -- [isdaysafterfeedback_submitted,daysafter_submitted_date,daysafter_submitted_remark]
            'Hemant (28 Jul 2021) -- [completed_approval_date]
            'Hemant (25 May 2021) -- [completed_remark,enrollment_remark]
            If xDateJoinQry.Trim.Length > 0 Then
                strQ &= xDateJoinQry
            End If

            If blnApplyUserAccessFilter = True Then
                If xUACQry.Trim.Length > 0 Then
                    strQ &= xUACQry
                End If
            End If

            If xAdvanceJoinQry.Trim.Length > 0 Then
                strQ &= xAdvanceJoinQry
            End If

            strQ &= " WHERE ISNULL(trtraining_request_master.isvoid,0) = 0 "

            If xUACFiltrQry.Trim.Length > 0 Then
                strQ &= " AND " & xUACFiltrQry
            End If

            If xIncludeIn_ActiveEmployee = False Then
                If xDateFilterQry.Trim.Length > 0 Then
                    strQ &= xDateFilterQry
                End If
            End If

            objDataOperation.ClearParameters()

            If intStatusID > 0 Then
                strQ &= "AND trtraining_request_master.statusunkid = @statusunkid"
                objDataOperation.AddParameter("@statusunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intStatusID.ToString)
            End If

            If mstrFilter.Trim.Length > 0 Then
                strQ &= " AND " & mstrFilter
            End If

            objDataOperation.AddParameter("@Pending", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 1, "Pending"))
            objDataOperation.AddParameter("@Approved", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 2, "Approved"))
            objDataOperation.AddParameter("@Rejected", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 3, "Rejected"))
            objDataOperation.AddParameter("@Yes", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 4, "Yes"))
            objDataOperation.AddParameter("@No", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 5, "No"))
            objDataOperation.AddParameter("@OnHold", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 6, "On Hold"))
            objDataOperation.AddParameter("@Postponed", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 7, "Postponed"))
            objDataOperation.AddParameter("@Cancel", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 8, "Cancel"))
            objDataOperation.AddParameter("@Completed", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 15, "Completed"))
            'Hemant (07 Mar 2022) -- Start            
            'ISSUE/ENHANCEMENT(NMB) : OLD-585 - NMB - Show consolidated total training cost and approved amount on Group training requests on Individual requests
            objDataOperation.AddParameter("@Company", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, " " & Language.getMessage("frmDepartmentalTrainingNeedsList", 82, "Company"))
            'Hemant (07 Mar 2022) -- End

            dsList = objDataOperation.ExecQuery(strQ, strTableName)

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            'Hemant (07 Mar 2022) -- Start            
            'ISSUE/ENHANCEMENT(NMB) : OLD-585 - NMB - Show consolidated total training cost and approved amount on Group training requests on Individual requests
            strQ = "SELECT " & _
                         "* INTO #TrainingApproval " & _
                    "FROM (SELECT " & _
                              "trainingrequestunkid " & _
                            ",approvedamount " & _
                            ",priority " & _
                            ",userunkid " & _
                            ",statusunkid " & _
                            ",DENSE_RANK() OVER (PARTITION BY trainingrequestunkid " & _
                              "ORDER BY priority DESC " & _
                              ") amount_rank " & _
                    " " & _
                         "FROM trtrainingapproval_process_tran " & _
                         "WHERE isvoid = 0 " & _
                         "AND statusunkid = " & enTrainingRequestStatus.APPROVED & " ) AS A " & _
                    "WHERE A.amount_rank = 1 " & _
                    " " & _
                    "SELECT " & _
                         "refno " & _
                       ",#TrainingApproval.approvedamount " & _
                    "FROM trtraining_request_master " & _
                    " JOIN hremployee_master ON hremployee_master.employeeunkid = trtraining_request_master.employeeunkid " & _
                    "JOIN #TrainingApproval " & _
                         "ON #TrainingApproval.trainingrequestunkid = trtraining_request_master.trainingrequestunkid "

            If xDateJoinQry.Trim.Length > 0 Then
                strQ &= xDateJoinQry
            End If

            If blnApplyUserAccessFilter = True Then
                If xUACQry.Trim.Length > 0 Then
                    strQ &= xUACQry
                End If
            End If

            If xAdvanceJoinQry.Trim.Length > 0 Then
                strQ &= xAdvanceJoinQry
            End If

            strQ &= " WHERE ISNULL(trtraining_request_master.isvoid,0) = 0 " & _
            " AND trtraining_request_master.statusunkid = " & enTrainingRequestStatus.APPROVED & " "

            If xUACFiltrQry.Trim.Length > 0 Then
                strQ &= " AND " & xUACFiltrQry
            End If

            If xIncludeIn_ActiveEmployee = False Then
                If xDateFilterQry.Trim.Length > 0 Then
                    strQ &= xDateFilterQry
                End If
            End If

            If mstrFilter.Trim.Length > 0 Then
                strQ &= " AND " & mstrFilter
            End If

            strQ &= "GROUP BY refno " & _
                              ",#TrainingApproval.userunkid " & _
                              ",#TrainingApproval.approvedamount " & _
                    "DROP TABLE #TrainingApproval "

            dsTrainingApproval = objDataOperation.ExecQuery(strQ, strTableName)

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            Dim dtTrainingApprovalRefNo As New DataTable
            Dim strcolname As String() = New String() {"RefNo"}
            dtTrainingApprovalRefNo = dsTrainingApproval.Tables(0).DefaultView.ToTable(True, strcolname)

            For Each drRow As DataRow In dtTrainingApprovalRefNo.Rows
                If IsDBNull(dsTrainingApproval.Tables(0).Compute("SUM(approvedamount)", "refno = '" & drRow.Item("refno") & "'")) = False Then
                    For Each drRowRequest As DataRow In dsList.Tables(0).Select("refno = '" & drRow.Item("refno") & "'")
                        drRowRequest.Item("approvedamount") = dsTrainingApproval.Tables(0).Compute("SUM(approvedamount)", "refno = '" & drRow.Item("refno") & "'")
                    Next
                End If
            Next

            If blnAddGrouping = True Then

                Dim dt As DataTable = New DataView(dsList.Tables(0)).ToTable(True, "refno", "CreateUserName", "Training", "allocationtranname", "start_date", "end_date", "totaltrainingcost", "approvedamount")

                Dim dtCol As New DataColumn("IsGrp", System.Type.GetType("System.Boolean"))
                dtCol.DefaultValue = True
                dtCol.AllowDBNull = False
                dt.Columns.Add(dtCol)

                dt.Merge(dsList.Tables(0), False)

                dsList.Tables.Clear()
                dsList.Tables.Add(dt.DefaultView.ToTable)
            End If

            Dim dtTable As DataTable
            dtTable = New DataView(dsList.Tables(strTableName), "", "refno, CreateUserName, Training, allocationtranname, start_date, end_date, IsGrp DESC, trainingrequestunkid DESC", DataViewRowState.CurrentRows).ToTable.Copy
            dsList.Tables.RemoveAt(0)
            dsList.Tables.Add(dtTable.Copy)
            'Hemant (07 Mar 2022) -- End


        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: GetList; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
        Return dsList
    End Function

    ''' <summary>
    ''' Modify By: Hemant
    ''' </summary>
    ''' <returns>Boolean</returns>
    ''' <purpose> INSERT INTO Database Table (trtraining_request_master) </purpose>
    Public Function Insert(Optional ByVal xDataOp As clsDataOperation = Nothing, Optional ByRef intNewUnkId As Integer = 0) As Boolean
        If isExist(mintEmployeeunkid, mintCourseMasterunkid, mintPeriodunkid, -1, xDataOp) Then
            'Hemant (03 Feb 2023) -- [mintPeriodunkid]
            mstrMessage = Language.getMessage(mstrModuleName, 9, "Sorry, This Training is already defined. Please define new Training.")
            Return False
        End If

        'If isExist(mstrName) Then
        '    mstrMessage = "<Message>"
        '    Return False
        'End If

        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        If xDataOp Is Nothing Then
            objDataOperation = New clsDataOperation
            objDataOperation.BindTransaction()
        Else
            objDataOperation = xDataOp
        End If
        objDataOperation.ClearParameters()

        Try
            objDataOperation.AddParameter("@employeeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintEmployeeunkid.ToString)
            objDataOperation.AddParameter("@application_date", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, mdtApplication_Date.ToString)
            objDataOperation.AddParameter("@coursemasterunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintCourseMasterunkid.ToString)
            objDataOperation.AddParameter("@isscheduled", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsScheduled.ToString)
            objDataOperation.AddParameter("@start_date", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, mdtStart_Date.ToString)
            objDataOperation.AddParameter("@end_date", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, mdtEnd_Date.ToString)
            objDataOperation.AddParameter("@provider_name", SqlDbType.NVarChar, mstrProviderName.Trim.Length, mstrProviderName.ToString)
            objDataOperation.AddParameter("@provider_address", SqlDbType.NVarChar, mstrProviderAddress.Trim.Length, mstrProviderAddress.ToString)
            objDataOperation.AddParameter("@fundingsourceunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintFundingSourceunkid.ToString)
            objDataOperation.AddParameter("@totaltrainingcost", SqlDbType.Decimal, eZeeDataType.DECIMAL_SIZE, mdecTotalTrainingCost.ToString)
            objDataOperation.AddParameter("@approvedamount", SqlDbType.Decimal, eZeeDataType.DECIMAL_SIZE, mdecApprovedCost.ToString)
            objDataOperation.AddParameter("@approvertranunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintApproverTranunkid.ToString)
            objDataOperation.AddParameter("@isalignedcurrentrole", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsAlignedCurrentRole.ToString)
            objDataOperation.AddParameter("@ispartofpdp", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsPartofPDP.ToString)
            objDataOperation.AddParameter("@isforeigntravelling", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsForeignTravelling.ToString)
            objDataOperation.AddParameter("@expectedreturn", SqlDbType.NVarChar, mstrExpectedReturn.Trim.Length, mstrExpectedReturn.ToString)
            objDataOperation.AddParameter("@remarks", SqlDbType.NVarChar, mstrRemarks.Trim.Length, mstrRemarks.ToString)
            objDataOperation.AddParameter("@statusunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintStatusunkid.ToString)
            objDataOperation.AddParameter("@issubmit_approval", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsSubmitApproval.ToString)
            objDataOperation.AddParameter("@userunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintUserunkid.ToString)
            objDataOperation.AddParameter("@loginemployeeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintLoginEmployeeunkid.ToString)
            objDataOperation.AddParameter("@isvoid", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsvoid.ToString)
            objDataOperation.AddParameter("@voiduserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintVoiduserunkid.ToString)
            objDataOperation.AddParameter("@voidloginemployeeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintVoidLoginEmployeeunkid.ToString)
            If mdtVoiddatetime = Nothing Then
                objDataOperation.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, DBNull.Value)
            Else
                objDataOperation.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, mdtVoiddatetime.ToString)
            End If
            objDataOperation.AddParameter("@voidreason", SqlDbType.NVarChar, mstrVoidreason.Trim.Length, mstrVoidreason.ToString)
            objDataOperation.AddParameter("@trainingproviderunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintTrainingproviderunkid.ToString)
            objDataOperation.AddParameter("@trainingvenueunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintTrainingvenueunkid.ToString)
            objDataOperation.AddParameter("@departmentaltrainingneedunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintDepartmentaltrainingneedunkid.ToString)
            objDataOperation.AddParameter("@isenroll_confirm", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsEnrollConfirm.ToString)
            objDataOperation.AddParameter("@isenroll_reject", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsEnrollReject.ToString)
            objDataOperation.AddParameter("@enroll_amount", SqlDbType.Decimal, eZeeDataType.DECIMAL_SIZE, mdecEnrollAmount.ToString)
            objDataOperation.AddParameter("@training_statusunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintTrainingStatusunkid.ToString)
            objDataOperation.AddParameter("@qualificationgroupunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintQualificationGroupunkid.ToString)
            objDataOperation.AddParameter("@qualificationunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintQualificationunkid.ToString)
            objDataOperation.AddParameter("@resultunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintResultunkid.ToString)
            objDataOperation.AddParameter("@gpacode", SqlDbType.Decimal, eZeeDataType.DECIMAL_SIZE, mdecGPAcode.ToString)
            objDataOperation.AddParameter("@isqualificaionupdated", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsqualificaionupdated.ToString)
            objDataOperation.AddParameter("@iscompleted_submit_approval", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsCompletedSubmitApproval.ToString)
            objDataOperation.AddParameter("@completed_statusunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintCompletedStatusunkid.ToString)
            objDataOperation.AddParameter("@periodunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintPeriodunkid.ToString)
            objDataOperation.AddParameter("@other_qualificationgrp", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrOtherQualificationGrp.ToString)
            objDataOperation.AddParameter("@other_qualification", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrOtherQualification.ToString)
            objDataOperation.AddParameter("@other_ResultCode", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrOtherResultCode.ToString)
            objDataOperation.AddParameter("@completed_userunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintCompletedUserunkid.ToString)
            'Hemant (25 May 2021) -- Start
            'ISSUE/ENHANCEMENT : OLD-393 - Training module bug fixes and enhancements
            objDataOperation.AddParameter("@completed_remark", SqlDbType.NVarChar, mstrRemarks.Trim.Length, mstrCompletedRemark.ToString)
            objDataOperation.AddParameter("@enrollment_remark", SqlDbType.NVarChar, mstrRemarks.Trim.Length, mstrEnrollmentRemark.ToString)
            'Hemant (25 May 2021) -- End
            'Hemant (28 Jul 2021) -- Start             
            'ENHANCEMENT : OLD-293 - Training Evaluation
            If mdtCompletedApprovaldate <> Nothing Then
                objDataOperation.AddParameter("@completed_approval_date", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, mdtCompletedApprovaldate)
            Else
                objDataOperation.AddParameter("@completed_approval_date", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, DBNull.Value)
            End If
            'Hemant (28 Jul 2021) -- End
            'Hemant (20 Aug 2021) -- Start
            'ISSUE/ENHANCEMENT : OLD-447 - Give option to accommodate line manager feedback. It should be possible to mark questions as either for Manager or for Employee.
            objDataOperation.AddParameter("@isdaysafterfeedback_submitted", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsDaysAfterFeedbackSubmitted.ToString)
            If mdtDaysAfterFeedbackSubmittedDate <> Nothing Then
                objDataOperation.AddParameter("@daysafter_submitted_date", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, mdtDaysAfterFeedbackSubmittedDate)
            Else
                objDataOperation.AddParameter("@daysafter_submitted_date", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, DBNull.Value)
            End If
            objDataOperation.AddParameter("@daysafter_submitted_remark", SqlDbType.NVarChar, mstrRemarks.Trim.Length, mstrDaysAfterFeedbackSubmittedRemark.ToString)
            'Hemant (20 Aug 2021) -- End
            'Hemant (01 Sep 2021) -- Start
            'ISSUE/ENHANCEMENT : OLD-459 - SAVE button on Evaluation Forms in addition to the SUBMIT button.
            objDataOperation.AddParameter("@ispretrainingfeedback_submitted", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsPreTrainingFeedbackSubmitted.ToString)
            If mdtPreTrainingFeedbackSubmittedDate <> Nothing Then
                objDataOperation.AddParameter("@pretraining_submitted_date", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, mdtPreTrainingFeedbackSubmittedDate)
            Else
                objDataOperation.AddParameter("@pretraining_submitted_date", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, DBNull.Value)
            End If
            objDataOperation.AddParameter("@isposttrainingfeedback_submitted", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsPostTrainingFeedbackSubmitted.ToString)
            If mdtPostTrainingFeedbackSubmittedDate <> Nothing Then
                objDataOperation.AddParameter("@posttraining_submitted_date", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, mdtPostTrainingFeedbackSubmittedDate)
            Else
                objDataOperation.AddParameter("@posttraining_submitted_date", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, DBNull.Value)
            End If
            objDataOperation.AddParameter("@isdaysafter_linemanager_submitted", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsDaysAfterLineManagerFeedbackSubmitted.ToString)
            If mdtDaysAfterLineManagerFeedbackSubmittedDate <> Nothing Then
                objDataOperation.AddParameter("@daysafter_linemanager_submitted_date", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, mdtDaysAfterLineManagerFeedbackSubmittedDate)
            Else
                objDataOperation.AddParameter("@daysafter_linemanager_submitted_date", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, DBNull.Value)
            End If
            'Hemant (01 Sep 2021) -- End
            'Hemant (23 Sep 2021) -- Start
            'ENHANCEMENT : OLD-470 - Silverlands - New Training Setting to skip Training Requests Process.
            objDataOperation.AddParameter("@isskip_trainingrequest_and_approval", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsSkipTrainingRequestAndApproval.ToString)
            'Hemant (23 Sep 2021) -- End
            'Hemant (09 Feb 2022) -- Start            
            'OLD-551(NMB) : New config setting to allow Training Budget settings to be based on any allocation - Cost Center etc. 
            objDataOperation.AddParameter("@trainingcostemp", SqlDbType.Decimal, eZeeDataType.DECIMAL_SIZE, mdecTrainingCostEmp.ToString)
            objDataOperation.AddParameter("@approvedamountemp", SqlDbType.Decimal, eZeeDataType.DECIMAL_SIZE, mdecApprovedAmountEmp.ToString)
            objDataOperation.AddParameter("@insertformid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintInsertFormId.ToString)
            'Hemant (09 Feb 2022) -- End
            'Sohail (15 Mar 2022) -- Start
            'Enhancement : OLD-583 : NMB - Modification of Training Request Approval Screen to allow batch approvals.
            objDataOperation.AddParameter("@refno", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrRefno.ToString)
            objDataOperation.AddParameter("@createuserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintCreateuserunkid.ToString)
            objDataOperation.AddParameter("@createloginemployeeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintCreateloginEmployeeunkid.ToString)
            'Sohail (15 Mar 2022) -- End
            'Hemant (29 Apr 2022) -- Start
            'ISSUE/ENHANCEMENT : AC2-314(NMB) - NMB - As a manager, I want different approval flow (Training requests) for different training courses.
            objDataOperation.AddParameter("@trainingtypeid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintTrainingTypeId.ToString)
            'Hemant (29 Apr 2022) -- End
            'Hemant (25 Jul 2022) -- Start            
            'ENHANCEMENT(NMB) : AC2-724 - Implementation of Approval in Training request Approval Form
            objDataOperation.AddParameter("@approvalsettingid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintTrainingApprovalSettingID.ToString)
            'Hemant (25 Jul 2022) -- End
            'Hemant (12 Oct 2022) -- Start
            'ENHANCEMENT(NMB) :  AC2-952 - As a user, I want to have a document attachment option on the group training request page
            objDataOperation.AddParameter("@grouptrainingrequestunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintGroupTrainingRequestunkid.ToString)
            'Hemant (12 Oct 2022) -- End
            'Hemant (10 Nov 2022) -- Start
            'ENHANCEMENT(NMB) :  AC2-1024 - As a user, I want to have a field to capture the Training Venue on both individual and group training request pages. This filed will be free text and mandatory
            objDataOperation.AddParameter("@venue", SqlDbType.NVarChar, mstrVenue.Trim.Length, mstrVenue.ToString)
            'Hemant (27 Oct 2022) -- End

            strQ = "INSERT INTO trtraining_request_master ( " & _
                       "  application_date " & _
                       ", employeeunkid " & _
                       ", coursemasterunkid " & _
                       ", isscheduled " & _
                       ", start_date " & _
                       ", end_date " & _
                       ", provider_name " & _
                       ", provider_address " & _
                       ", fundingsourceunkid " & _
                       ", totaltrainingcost " & _
                       ", approvedamount " & _
                       ", approvertranunkid " & _
                       ", isalignedcurrentrole " & _
                       ", ispartofpdp " & _
                       ", isforeigntravelling " & _
                       ", expectedreturn " & _
                       ", remarks " & _
                       ", statusunkid " & _
                       ", issubmit_approval " & _
                       ", userunkid " & _
                       ", loginemployeeunkid " & _
                       ", isvoid " & _
                       ", voiduserunkid " & _
                       ", voidloginemployeeunkid " & _
                       ", voiddatetime " & _
                       ", voidreason " & _
                       ", trainingproviderunkid " & _
                       ", trainingvenueunkid " & _
                       ", departmentaltrainingneedunkid " & _
                       ", isenroll_confirm " & _
                       ", isenroll_reject " & _
                       ", enroll_amount " & _
                       ", training_statusunkid " & _
                       ", qualificationgroupunkid " & _
                       ", qualificationunkid " & _
                       ", resultunkid " & _
                       ", gpacode " & _
                       ", isqualificaionupdated" & _
                       ", iscompleted_submit_approval " & _
                       ", completed_statusunkid " & _
                       ", periodunkid " & _
                       ", other_qualificationgrp " & _
                       ", other_qualification " & _
                       ", other_resultcode " & _
                       ", completed_userunkid " & _
                       ", completed_remark " & _
                       ", enrollment_remark " & _
                       ", completed_approval_date " & _
                       ", isdaysafterfeedback_submitted  " & _
                       ", daysafter_submitted_date " & _
                       ", daysafter_submitted_remark " & _
                       ", ispretrainingfeedback_submitted " & _
                       ", pretraining_submitted_date " & _
                       ", isposttrainingfeedback_submitted " & _
                       ", posttraining_submitted_date " & _
                       ", isdaysafter_linemanager_submitted " & _
                       ", daysafter_linemanager_submitted_date " & _
                       ", isskip_trainingrequest_and_approval " & _
                       ", trainingcostemp " & _
                       ", approvedamountemp " & _
                       ", insertformid " & _
                       ", refno " & _
                       ", createuserunkid " & _
                       ", createloginemployeeunkid " & _
                       ", trainingtypeid " & _
                       ", approvalsettingid " & _
                       ", grouptrainingrequestunkid " & _
                       ", venue " & _
                    ") VALUES (" & _
                       "  @application_date " & _
                       ", @employeeunkid " & _
                       ", @coursemasterunkid " & _
                       ", @isscheduled " & _
                       ", @start_date " & _
                       ", @end_date " & _
                       ", @provider_name " & _
                       ", @provider_address " & _
                       ", @fundingsourceunkid " & _
                       ", @totaltrainingcost " & _
                       ", @approvedamount " & _
                       ", @approvertranunkid " & _
                       ", @isalignedcurrentrole " & _
                       ", @ispartofpdp " & _
                       ", @isforeigntravelling " & _
                       ", @expectedreturn " & _
                       ", @remarks " & _
                       ", @statusunkid " & _
                       ", @issubmit_approval " & _
                       ", @userunkid " & _
                       ", @loginemployeeunkid " & _
                       ", @isvoid " & _
                       ", @voiduserunkid " & _
                       ", @voidloginemployeeunkid " & _
                       ", @voiddatetime " & _
                       ", @voidreason " & _
                       ", @trainingproviderunkid " & _
                       ", @trainingvenueunkid " & _
                       ", @departmentaltrainingneedunkid " & _
                       ", @isenroll_confirm " & _
                       ", @isenroll_reject " & _
                       ", @enroll_amount " & _
                       ", @training_statusunkid " & _
                       ", @qualificationgroupunkid " & _
                       ", @qualificationunkid " & _
                       ", @resultunkid " & _
                       ", @gpacode " & _
                       ", @isqualificaionupdated" & _
                       ", @iscompleted_submit_approval " & _
                       ", @completed_statusunkid " & _
                       ", @periodunkid " & _
                       ", @other_qualificationgrp " & _
                       ", @other_qualification " & _
                       ", @other_resultcode " & _
                       ", @completed_userunkid " & _
                       ", @completed_remark " & _
                       ", @enrollment_remark " & _
                       ", @completed_approval_date " & _
                       ", @isdaysafterfeedback_submitted  " & _
                       ", @daysafter_submitted_date " & _
                       ", @daysafter_submitted_remark " & _
                       ", @ispretrainingfeedback_submitted " & _
                       ", @pretraining_submitted_date " & _
                       ", @isposttrainingfeedback_submitted " & _
                       ", @posttraining_submitted_date " & _
                       ", @isdaysafter_linemanager_submitted " & _
                       ", @daysafter_linemanager_submitted_date " & _
                       ", @isskip_trainingrequest_and_approval " & _
                       ", @trainingcostemp " & _
                       ", @approvedamountemp " & _
                       ", @insertformid " & _
                       ", @refno " & _
                       ", @createuserunkid " & _
                       ", @createloginemployeeunkid " & _
                       ", @trainingtypeid " & _
                       ", @approvalsettingid " & _
                       ", @grouptrainingrequestunkid " & _
                       ", @venue " & _
                    "); SELECT @@identity"
            'Hemant (10 Nov 2022) -- [venue]
            'Hemant (12 Oct 2022) -- [grouptrainingrequestunkid]
            'Hemant (25 Jul 2022) -- [approvalsettingid]
            'Hemant (29 Apr 2022) -- [trainingtypeid]
            'Sohail (15 Mar 2022) - [refno, createuserunkid, createloginemployeeunkid]
            'Hemant (09 Feb 2022) -- [trainingcostemp,approvedamountemp,insertformid]
            'Hemant (23 Sep 2021) -- [isskip_trainingrequest_and_approval]
            'Hemant (01 Sep 2021) -- [ispretrainingfeedback_submitted,pretraining_submitted_date,isposttrainingfeedback_submitted,posttraining_submitted_date,isdaysafter_linemanager_submitted,daysafter_linemanager_submitted_date]
            'Hemant (20 Aug 2021) -- [isdaysafterfeedback_submitted,daysafter_submitted_date,daysafter_submitted_remark]
            'Hemant (28 Jul 2021) -- [completed_approval_date]
            'Hemant (25 May 2021) -- [completed_remark,enrollment_remark]
            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            mintTrainingRequestunkid = dsList.Tables(0).Rows(0).Item(0)

            intNewUnkId = mintTrainingRequestunkid

            If InsertAuditTrails(objDataOperation, 1) = False Then
                If xDataOp Is Nothing Then objDataOperation.ReleaseTransaction(False)
                Return False
            End If

            If xDataOp Is Nothing Then objDataOperation.ReleaseTransaction(True)

            Return True
        Catch ex As Exception
            If xDataOp Is Nothing Then objDataOperation.ReleaseTransaction(False)
            Throw New Exception(ex.Message & "; Procedure Name: Insert; Module Name: " & mstrModuleName)
            Return False
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            If xDataOp Is Nothing Then objDataOperation = Nothing
        End Try
    End Function

    ''' <summary>
    ''' Modify By: Hemant
    ''' </summary>
    ''' <returns>Boolean</returns>
    ''' <purpose> INSERT INTO Database Table (trtraining_request_master) </purpose>
    Public Function Insert(ByVal dtCostTran As DataTable, _
                           ByVal xDatabaseName As String, _
                           ByVal xUserUnkid As Integer, _
                           ByVal xYearUnkid As Integer, _
                           ByVal xCompanyUnkid As Integer, _
                           ByVal strUserAccessMode As String, _
                           ByVal xEmployeeAsOnDate As String, _
                           ByVal xTrainingApproverAllocationID As Integer _
                           ) As Boolean
        'Hemant (09 Feb 2022) -- [xTrainingApproverAllocationID]
        If isExist(mintEmployeeunkid, mintCourseMasterunkid, mintPeriodunkid, -1) Then
            'Hemant (03 Feb 2023) -- [mintPeriodunkid]
            mstrMessage = Language.getMessage(mstrModuleName, 9, "Sorry, This Training is already defined. Please define new Training.")
            Return False
        End If

        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        Dim objCostTran As New clstraining_request_cost_tran
        Dim objTrainingApproval As New clstrainingapproval_process_tran

        Dim objDataOperation As clsDataOperation
        objDataOperation = New clsDataOperation
        objDataOperation.BindTransaction()
        objDataOperation.ClearParameters()

        Try
            objDataOperation.AddParameter("@employeeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintEmployeeunkid.ToString)
            objDataOperation.AddParameter("@application_date", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, mdtApplication_Date.ToString)
            objDataOperation.AddParameter("@coursemasterunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintCourseMasterunkid.ToString)
            objDataOperation.AddParameter("@isscheduled", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsScheduled.ToString)
            objDataOperation.AddParameter("@start_date", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, mdtStart_Date.ToString)
            objDataOperation.AddParameter("@end_date", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, mdtEnd_Date.ToString)
            objDataOperation.AddParameter("@provider_name", SqlDbType.NVarChar, mstrProviderName.Trim.Length, mstrProviderName.ToString)
            objDataOperation.AddParameter("@provider_address", SqlDbType.NVarChar, mstrProviderAddress.Trim.Length, mstrProviderAddress.ToString)
            objDataOperation.AddParameter("@fundingsourceunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintFundingSourceunkid.ToString)
            objDataOperation.AddParameter("@totaltrainingcost", SqlDbType.Decimal, eZeeDataType.DECIMAL_SIZE, mdecTotalTrainingCost.ToString)
            objDataOperation.AddParameter("@approvedamount", SqlDbType.Decimal, eZeeDataType.DECIMAL_SIZE, mdecApprovedCost.ToString)
            objDataOperation.AddParameter("@approvertranunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintApproverTranunkid.ToString)
            objDataOperation.AddParameter("@isalignedcurrentrole", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsAlignedCurrentRole.ToString)
            objDataOperation.AddParameter("@ispartofpdp", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsPartofPDP.ToString)
            objDataOperation.AddParameter("@isforeigntravelling", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsForeignTravelling.ToString)
            objDataOperation.AddParameter("@expectedreturn", SqlDbType.NVarChar, mstrExpectedReturn.Trim.Length, mstrExpectedReturn.ToString)
            objDataOperation.AddParameter("@remarks", SqlDbType.NVarChar, mstrRemarks.Trim.Length, mstrRemarks.ToString)
            objDataOperation.AddParameter("@statusunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintStatusunkid.ToString)
            objDataOperation.AddParameter("@issubmit_approval", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsSubmitApproval.ToString)
            objDataOperation.AddParameter("@userunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintUserunkid.ToString)
            objDataOperation.AddParameter("@loginemployeeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintLoginEmployeeunkid.ToString)
            objDataOperation.AddParameter("@isvoid", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsvoid.ToString)
            objDataOperation.AddParameter("@voiduserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintVoiduserunkid.ToString)
            objDataOperation.AddParameter("@voidloginemployeeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintVoidLoginEmployeeunkid.ToString)
            If mdtVoiddatetime = Nothing Then
                objDataOperation.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, DBNull.Value)
            Else
                objDataOperation.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, mdtVoiddatetime.ToString)
            End If
            objDataOperation.AddParameter("@voidreason", SqlDbType.NVarChar, mstrVoidreason.Trim.Length, mstrVoidreason.ToString)
            objDataOperation.AddParameter("@trainingproviderunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintTrainingproviderunkid.ToString)
            objDataOperation.AddParameter("@trainingvenueunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintTrainingvenueunkid.ToString)
            objDataOperation.AddParameter("@departmentaltrainingneedunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintDepartmentaltrainingneedunkid.ToString)
            objDataOperation.AddParameter("@isenroll_confirm", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsEnrollConfirm.ToString)
            objDataOperation.AddParameter("@isenroll_reject", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsEnrollReject.ToString)
            objDataOperation.AddParameter("@enroll_amount", SqlDbType.Decimal, eZeeDataType.DECIMAL_SIZE, mdecEnrollAmount.ToString)
            objDataOperation.AddParameter("@training_statusunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintTrainingStatusunkid.ToString)
            objDataOperation.AddParameter("@qualificationgroupunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintQualificationGroupunkid.ToString)
            objDataOperation.AddParameter("@qualificationunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintQualificationunkid.ToString)
            objDataOperation.AddParameter("@resultunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintResultunkid.ToString)
            objDataOperation.AddParameter("@gpacode", SqlDbType.Decimal, eZeeDataType.DECIMAL_SIZE, mdecGPAcode.ToString)
            objDataOperation.AddParameter("@isqualificaionupdated", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsqualificaionupdated.ToString)
            objDataOperation.AddParameter("@iscompleted_submit_approval", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsCompletedSubmitApproval.ToString)
            objDataOperation.AddParameter("@completed_statusunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintCompletedStatusunkid.ToString)
            objDataOperation.AddParameter("@periodunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintPeriodunkid.ToString)
            objDataOperation.AddParameter("@other_qualificationgrp", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrOtherQualificationGrp.ToString)
            objDataOperation.AddParameter("@other_qualification", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrOtherQualification.ToString)
            objDataOperation.AddParameter("@other_ResultCode", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrOtherResultCode.ToString)
            objDataOperation.AddParameter("@completed_userunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintCompletedUserunkid.ToString)
            'Hemant (25 May 2021) -- Start
            'ISSUE/ENHANCEMENT : OLD-393 - Training module bug fixes and enhancements
            objDataOperation.AddParameter("@completed_remark", SqlDbType.NVarChar, mstrRemarks.Trim.Length, mstrCompletedRemark.ToString)
            objDataOperation.AddParameter("@enrollment_remark", SqlDbType.NVarChar, mstrRemarks.Trim.Length, mstrEnrollmentRemark.ToString)
            'Hemant (25 May 2021) -- End
            'Hemant (28 Jul 2021) -- Start             
            'ENHANCEMENT : OLD-293 - Training Evaluation
            If mdtCompletedApprovaldate <> Nothing Then
                objDataOperation.AddParameter("@completed_approval_date", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, mdtCompletedApprovaldate)
            Else
                objDataOperation.AddParameter("@completed_approval_date", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, DBNull.Value)
            End If
            'Hemant (28 Jul 2021) -- End
            'Hemant (20 Aug 2021) -- Start
            'ISSUE/ENHANCEMENT : OLD-447 - Give option to accommodate line manager feedback. It should be possible to mark questions as either for Manager or for Employee.
            objDataOperation.AddParameter("@isdaysafterfeedback_submitted", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsDaysAfterFeedbackSubmitted.ToString)
            If mdtDaysAfterFeedbackSubmittedDate <> Nothing Then
                objDataOperation.AddParameter("@daysafter_submitted_date", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, mdtDaysAfterFeedbackSubmittedDate)
            Else
                objDataOperation.AddParameter("@daysafter_submitted_date", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, DBNull.Value)
            End If
            objDataOperation.AddParameter("@daysafter_submitted_remark", SqlDbType.NVarChar, mstrRemarks.Trim.Length, mstrDaysAfterFeedbackSubmittedRemark.ToString)
            'Hemant (20 Aug 2021) -- End
            'Hemant (01 Sep 2021) -- Start
            'ISSUE/ENHANCEMENT : OLD-459 - SAVE button on Evaluation Forms in addition to the SUBMIT button.
            objDataOperation.AddParameter("@ispretrainingfeedback_submitted", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsPreTrainingFeedbackSubmitted.ToString)
            If mdtPreTrainingFeedbackSubmittedDate <> Nothing Then
                objDataOperation.AddParameter("@pretraining_submitted_date", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, mdtPreTrainingFeedbackSubmittedDate)
            Else
                objDataOperation.AddParameter("@pretraining_submitted_date", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, DBNull.Value)
            End If
            objDataOperation.AddParameter("@isposttrainingfeedback_submitted", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsPostTrainingFeedbackSubmitted.ToString)
            If mdtPostTrainingFeedbackSubmittedDate <> Nothing Then
                objDataOperation.AddParameter("@posttraining_submitted_date", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, mdtPostTrainingFeedbackSubmittedDate)
            Else
                objDataOperation.AddParameter("@posttraining_submitted_date", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, DBNull.Value)
            End If
            objDataOperation.AddParameter("@isdaysafter_linemanager_submitted", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsDaysAfterLineManagerFeedbackSubmitted.ToString)
            If mdtDaysAfterLineManagerFeedbackSubmittedDate <> Nothing Then
                objDataOperation.AddParameter("@daysafter_linemanager_submitted_date", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, mdtDaysAfterLineManagerFeedbackSubmittedDate)
            Else
                objDataOperation.AddParameter("@daysafter_linemanager_submitted_date", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, DBNull.Value)
            End If
            'Hemant (01 Sep 2021) -- End
            'Hemant (23 Sep 2021) -- Start
            'ENHANCEMENT : OLD-470 - Silverlands - New Training Setting to skip Training Requests Process.
            objDataOperation.AddParameter("@isskip_trainingrequest_and_approval", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsSkipTrainingRequestAndApproval.ToString)
            'Hemant (23 Sep 2021) -- End
            'Hemant (09 Feb 2022) -- Start            
            'OLD-551(NMB) : New config setting to allow Training Budget settings to be based on any allocation - Cost Center etc. 
            objDataOperation.AddParameter("@trainingcostemp", SqlDbType.Decimal, eZeeDataType.DECIMAL_SIZE, mdecTrainingCostEmp.ToString)
            objDataOperation.AddParameter("@approvedamountemp", SqlDbType.Decimal, eZeeDataType.DECIMAL_SIZE, mdecApprovedAmountEmp.ToString)
            objDataOperation.AddParameter("@insertformid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintInsertFormId.ToString)
            'Hemant (09 Feb 2022) -- End
            'Sohail (15 Mar 2022) -- Start
            'Enhancement : OLD-583 : NMB - Modification of Training Request Approval Screen to allow batch approvals.
            objDataOperation.AddParameter("@refno", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrRefno.ToString)
            objDataOperation.AddParameter("@createuserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintCreateuserunkid.ToString)
            objDataOperation.AddParameter("@createloginemployeeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintCreateloginEmployeeunkid.ToString)
            'Sohail (15 Mar 2022) -- End
            'Hemant (29 Apr 2022) -- Start
            'ISSUE/ENHANCEMENT : AC2-314(NMB) - NMB - As a manager, I want different approval flow (Training requests) for different training courses.
            objDataOperation.AddParameter("@trainingtypeid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintTrainingTypeId.ToString)
            'Hemant (29 Apr 2022) -- End
            'Hemant (25 Jul 2022) -- Start            
            'ENHANCEMENT(NMB) : AC2-724 - Implementation of Approval in Training request Approval Form
            objDataOperation.AddParameter("@approvalsettingid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintTrainingApprovalSettingID.ToString)
            'Hemant (25 Jul 2022) -- End
            'Hemant (12 Oct 2022) -- Start
            'ENHANCEMENT(NMB) :  AC2-952 - As a user, I want to have a document attachment option on the group training request page
            objDataOperation.AddParameter("@grouptrainingrequestunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintGroupTrainingRequestunkid.ToString)
            'Hemant (12 Oct 2022) -- End
            'Hemant (10 Nov 2022) -- Start
            'ENHANCEMENT(NMB) :  AC2-1024 - As a user, I want to have a field to capture the Training Venue on both individual and group training request pages. This filed will be free text and mandatory
            objDataOperation.AddParameter("@venue", SqlDbType.NVarChar, mstrVenue.Trim.Length, mstrVenue.ToString)
            'Hemant (27 Oct 2022) -- End

            strQ = "INSERT INTO trtraining_request_master ( " & _
                       "  application_date " & _
                       ", employeeunkid " & _
                       ", coursemasterunkid " & _
                       ", isscheduled " & _
                       ", start_date " & _
                       ", end_date " & _
                       ", provider_name " & _
                       ", provider_address " & _
                       ", fundingsourceunkid " & _
                       ", totaltrainingcost " & _
                       ", approvedamount " & _
                       ", approvertranunkid " & _
                       ", isalignedcurrentrole " & _
                       ", ispartofpdp " & _
                       ", isforeigntravelling " & _
                       ", expectedreturn " & _
                       ", remarks " & _
                       ", statusunkid " & _
                       ", issubmit_approval " & _
                       ", userunkid " & _
                       ", loginemployeeunkid " & _
                       ", isvoid " & _
                       ", voiduserunkid " & _
                       ", voidloginemployeeunkid " & _
                       ", voiddatetime " & _
                       ", voidreason " & _
                       ", trainingproviderunkid " & _
                       ", trainingvenueunkid " & _
                       ", departmentaltrainingneedunkid " & _
                       ", isenroll_confirm " & _
                       ", isenroll_reject " & _
                       ", enroll_amount " & _
                       ", training_statusunkid " & _
                       ", qualificationgroupunkid " & _
                       ", qualificationunkid " & _
                       ", resultunkid " & _
                       ", gpacode " & _
                       ", isqualificaionupdated" & _
                       ", iscompleted_submit_approval " & _
                       ", completed_statusunkid " & _
                       ", periodunkid " & _
                       ", other_qualificationgrp " & _
                       ", other_qualification " & _
                       ", other_resultcode " & _
                       ", completed_userunkid " & _
                       ", completed_remark " & _
                       ", enrollment_remark " & _
                       ", completed_approval_date " & _
                       ", isdaysafterfeedback_submitted  " & _
                       ", daysafter_submitted_date " & _
                       ", daysafter_submitted_remark " & _
                       ", ispretrainingfeedback_submitted " & _
                       ", pretraining_submitted_date " & _
                       ", isposttrainingfeedback_submitted " & _
                       ", posttraining_submitted_date " & _
                       ", isdaysafter_linemanager_submitted " & _
                       ", daysafter_linemanager_submitted_date " & _
                       ", isskip_trainingrequest_and_approval " & _
                       ", trainingcostemp " & _
                       ", approvedamountemp " & _
                       ", insertformid " & _
                       ", refno " & _
                       ", createuserunkid " & _
                       ", createloginemployeeunkid " & _
                       ", trainingtypeid " & _
                       ", approvalsettingid " & _
                       ", grouptrainingrequestunkid " & _
                       ", venue " & _
                    ") VALUES (" & _
                       "  @application_date " & _
                       ", @employeeunkid " & _
                       ", @coursemasterunkid " & _
                       ", @isscheduled " & _
                       ", @start_date " & _
                       ", @end_date " & _
                       ", @provider_name " & _
                       ", @provider_address " & _
                       ", @fundingsourceunkid " & _
                       ", @totaltrainingcost " & _
                       ", @approvedamount " & _
                       ", @approvertranunkid " & _
                       ", @isalignedcurrentrole " & _
                       ", @ispartofpdp " & _
                       ", @isforeigntravelling " & _
                       ", @expectedreturn " & _
                       ", @remarks " & _
                       ", @statusunkid " & _
                       ", @issubmit_approval " & _
                       ", @userunkid " & _
                       ", @loginemployeeunkid " & _
                       ", @isvoid " & _
                       ", @voiduserunkid " & _
                       ", @voidloginemployeeunkid " & _
                       ", @voiddatetime " & _
                       ", @voidreason " & _
                       ", @trainingproviderunkid " & _
                       ", @trainingvenueunkid " & _
                       ", @departmentaltrainingneedunkid " & _
                       ", @isenroll_confirm " & _
                       ", @isenroll_reject " & _
                       ", @enroll_amount " & _
                       ", @training_statusunkid " & _
                       ", @qualificationgroupunkid " & _
                       ", @qualificationunkid " & _
                       ", @resultunkid " & _
                       ", @gpacode " & _
                       ", @isqualificaionupdated" & _
                       ", @iscompleted_submit_approval " & _
                       ", @completed_statusunkid " & _
                       ", @periodunkid " & _
                       ", @other_qualificationgrp " & _
                       ", @other_qualification " & _
                       ", @other_resultcode " & _
                       ", @completed_userunkid " & _
                       ", @completed_remark " & _
                       ", @enrollment_remark " & _
                       ", @completed_approval_date " & _
                       ", @isdaysafterfeedback_submitted  " & _
                       ", @daysafter_submitted_date " & _
                       ", @daysafter_submitted_remark " & _
                       ", @ispretrainingfeedback_submitted " & _
                       ", @pretraining_submitted_date " & _
                       ", @isposttrainingfeedback_submitted " & _
                       ", @posttraining_submitted_date " & _
                       ", @isdaysafter_linemanager_submitted " & _
                       ", @daysafter_linemanager_submitted_date " & _
                       ", @isskip_trainingrequest_and_approval " & _
                       ", @trainingcostemp " & _
                       ", @approvedamountemp " & _
                       ", @insertformid " & _
                       ", @refno " & _
                       ", @createuserunkid " & _
                       ", @createloginemployeeunkid " & _
                       ", @trainingtypeid " & _
                       ", @approvalsettingid " & _
                       ", @grouptrainingrequestunkid " & _
                       ", @venue " & _
                    "); SELECT @@identity"
            'Hemant (10 Nov 2022) -- [venue]
            'Hemant (12 Oct 2022) -- [grouptrainingrequestunkid]
            'Hemant (25 Jul 2022) -- [approvalsettingid]
            'Hemant (29 Apr 2022) -- [trainingtypeid]
            'Sohail (15 Mar 2022) - [refno, createuserunkid, createloginemployeeunkid]
            'Hemant (09 Feb 2022) -- [trainingcostemp,approvedamountemp,insertformid]
            'Hemant (23 Sep 2021) -- [isskip_trainingrequest_and_approval]
            'Hemant (01 Sep 2021) -- [ispretrainingfeedback_submitted,pretraining_submitted_date,isposttrainingfeedback_submitted,posttraining_submitted_date,isdaysafter_linemanager_submitted,daysafter_linemanager_submitted_date]
            'Hemant (20 Aug 2021) -- [isdaysafterfeedback_submitted,daysafter_submitted_date,daysafter_submitted_remark]
            'Hemant (28 Jul 2021) -- [completed_approval_date]
            'Hemant (25 May 2021) -- [completed_remark,enrollment_remark]
            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            mintTrainingRequestunkid = dsList.Tables(0).Rows(0).Item(0)

            If InsertAuditTrails(objDataOperation, enAuditType.ADD) = False Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            With objCostTran
                ._TrainingRequestunkid = mintTrainingRequestunkid
                ._TranDataTable = dtCostTran
                ._Isvoid = mblnIsvoid
                ._Voidreason = ""
                ._Voiddatetime = Nothing
                ._Voiduserunkid = -1
                ._IsWeb = mblnIsWeb
                ._Userunkid = mintUserunkid
                ._LoginEmployeeunkid = mintLoginEmployeeunkid
                ._ClientIP = mstrClientIP
                ._FormName = mstrFormName
                ._HostName = mstrHostName
            End With


            If objCostTran.Insert_Update_Delete(objDataOperation) = False Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            If mblnIsSubmitApproval = True AndAlso mblnIsSkipTrainingRequestAndApproval = False Then
                'Hemant (23 Sep 2021) -- [mblnIsSkipTrainingRequestAndApproval = False]

                Dim objTrainingApprover As New clstraining_approver_master
                Dim objUsr As New clsUserAddEdit
                Dim objTrainingApprovalMatrix As New clsTraining_Approval_Matrix
                Dim dsUserList As New DataSet
                Dim intPrivilegeId As Integer
                'Dim dtApprover As DataTable = objTrainingApprover.GetEmployeeApprover(xDatabaseName, _
                '                                                                  xUserUnkid, _
                '                                                                  xYearUnkid, _
                '                                                                  xCompanyUnkid, _
                '                                                                  mintEmployeeunkid, _
                '                                                                  mintMaxPriority)
                If mblnIsForeignTravelling = True Then
                    'Hemant (29 Apr 2022) -- Start
                    'ISSUE/ENHANCEMENT : AC2-314(NMB) - NMB - As a manager, I want different approval flow (Training requests) for different training courses.
                    If mintTrainingTypeId = enTrainingTravelType.Local_Travel Then
                        intPrivilegeId = enUserPriviledge.AllowToApproveTrainingRequestLocalTravelling
                    ElseIf mintTrainingTypeId = enTrainingTravelType.Foreign_Travel Then
                        'Hemant (29 Apr 2022) -- End
                    intPrivilegeId = enUserPriviledge.AllowToApproveTrainingRequestForeignTravelling
                    End If 'Hemant (29 Apr 2022)
                Else
                    intPrivilegeId = enUserPriviledge.AllowToApproveTrainingRequestRelatedToCost
                End If

                Dim dtApprover As DataTable = objTrainingApprover.GetNextEmployeeApprovers(xDatabaseName, _
                                                                                           xCompanyUnkid, _
                                                                                           xYearUnkid, _
                                                                                           strUserAccessMode, _
                                                                                           intPrivilegeId, _
                                                                                           xEmployeeAsOnDate, _
                                                                                           xUserUnkid, _
                                                                                           mintEmployeeunkid, _
                                                                                           mdecTotalTrainingCost, _
                                                                                           mintPeriodunkid, _
                                                                                           xTrainingApproverAllocationID, _
                                                                                           mintTrainingTypeId, _
                                                                                           mintInsertFormId, _
                                                                                           objDataOperation _
                                                                                           )
                'Hemant (27 Jun 2022) -- [mintInsertFormId]
                'Hemant (29 Apr 2022) -- [mintTrainingTypeId]
                'Hemant (09 Feb 2022) -- [xTrainingApproverAllocationID]
                'Hemant (03 Dec 2021) -- [mintPeriodunkid]
                objTrainingApprover = Nothing
                'Dim intLevelId As Integer = -1
                'Dim dslevel As DataSet = objTrainingApprovalMatrix.getLevelByCostAmount(mdecTotalTrainingCost)
                'If dslevel IsNot Nothing AndAlso dslevel.Tables(0).Rows.Count > 0 Then
                '    intLevelId = CInt(dslevel.Tables(0).Rows(0).Item("levelunkid"))
                'End If


                Dim blnEnableVisibility As Boolean = False
                Dim intMinPriority As Integer = -1
                Dim intApproverID As Integer = -1

                If dtApprover IsNot Nothing AndAlso dtApprover.Rows.Count > 0 Then
                    For Each drRow As DataRow In dtApprover.Rows
                        objTrainingApproval._TrainingRequestunkid = mintTrainingRequestunkid
                        objTrainingApproval._Employeeunkid = mintEmployeeunkid
                        objTrainingApproval._Approvertranunkid = CInt(drRow("trapproverunkid"))
                        objTrainingApproval._Approvaldate = mdtApplication_Date
                        objTrainingApproval._Priority = CInt(drRow("priority"))
                        objTrainingApproval._TotalCostAmount = mdecTotalTrainingCost
                        objTrainingApproval._ApprovedAmount = mdecApprovedCost
                        objTrainingApproval._Statusunkid = mintStatusunkid
                        objTrainingApproval._Userunkid = mintUserunkid
                        objTrainingApproval._ClientIP = mstrClientIP
                        objTrainingApproval._FormName = mstrFormName
                        objTrainingApproval._HostName = mstrHostName
                        objTrainingApproval._IsWeb = mblnIsWeb

                        intMinPriority = CInt(dtApprover.Compute("MIN(priority)", "1=1"))

                        If intMinPriority = CInt(drRow("priority")) Then
                            If mintUserunkid = CInt(drRow("mapuserunkid")) Then
                                objTrainingApproval._Statusunkid = clstraining_requisition_approval_master.enApprovalStatus.Approved
                                mintMinApprovedPriority = CInt(drRow("priority"))
                                intApproverID = CInt(drRow("trapproverunkid"))
                                blnEnableVisibility = True
                            End If

                            If blnEnableVisibility = True Then
                                objTrainingApproval._VisibleId = clstraining_requisition_approval_master.enApprovalStatus.Approved
                                'Hemant (25 May 2021) -- Start
                                'ISSUE/ENHANCEMENT : OLD-393 - Training module bug fixes and enhancements
                                mdecApprovedCost = mdecTotalTrainingCost
                                objTrainingApproval._ApprovedAmount = mdecApprovedCost
                                'Hemant (25 May 2021) -- End
                            Else
                                Dim dRow As DataRow() = dtApprover.Select("priority=" & intMinPriority & " AND mapuserunkid=" & mintUserunkid & "")
                                If dRow.Length > 0 Then
                                    objTrainingApproval._VisibleId = clstraining_requisition_approval_master.enApprovalStatus.Approved
                                    'Hemant (25 May 2021) -- Start
                                    'ISSUE/ENHANCEMENT : OLD-393 - Training module bug fixes and enhancements
                                    mdecApprovedCost = mdecTotalTrainingCost
                                    objTrainingApproval._ApprovedAmount = mdecApprovedCost
                                    'Hemant (25 May 2021) -- End
                                Else
                                    objTrainingApproval._VisibleId = mintStatusunkid
                                End If
                            End If
                        Else

                            If blnEnableVisibility = True Then
                                Dim intNextMinPriority As Integer = CInt(dtApprover.Compute("MIN(priority)", "priority > " & intMinPriority))
                                If intNextMinPriority = CInt(drRow("priority")) Then
                                    objTrainingApproval._VisibleId = clstraining_requisition_approval_master.enApprovalStatus.SubmitForApproval
                                Else
                                    objTrainingApproval._VisibleId = -1
                                End If
                            Else
                                objTrainingApproval._VisibleId = -1
                            End If

                        End If

                        If objTrainingApproval.Insert(objDataOperation) = False Then
                            exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                            Throw exForce
                        End If
                    Next
                    'Hemant (25 May 2021) -- Start
                    'ISSUE/ENHANCEMENT : OLD-393 - Training module bug fixes and enhancements
                    If blnEnableVisibility = True Then
                        Dim intMaxPriority As Integer = CInt(dtApprover.Compute("MAX(priority)", "1=1"))
                        If intMaxPriority = intMinPriority Then
                            strQ = " UPDATE trtraining_request_master SET " & _
                              "      statusunkid = " & enTrainingRequestStatus.APPROVED & " " & _
                              "     ,approverunkid = " & intApproverID & " " & _
                              "     ,approved_amount = " & mdecTotalTrainingCost & " " & _
                              " WHERE isvoid=0 AND trainingrequestunkid = @trainingrequestunkid "

                            objDataOperation.ClearParameters()
                            objDataOperation.AddParameter("@trainingrequestunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintTrainingRequestunkid.ToString)

                            objDataOperation.ExecNonQuery(strQ)

                            If objDataOperation.ErrorMessage <> "" Then
                                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                                Throw exForce
                            End If

                            mintStatusunkid = enTrainingRequestStatus.APPROVED

                            If InsertAuditTrails(objDataOperation, enAuditType.EDIT) = False Then
                                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                                Throw exForce
                            End If
                        End If
                    End If
                    dtApprover.Rows.Clear()
                    'Hemant (25 May 2021) -- End
                End If
            End If

            objDataOperation.ReleaseTransaction(True)

            Return True
        Catch ex As Exception
            objDataOperation.ReleaseTransaction(False)
            Throw New Exception(ex.Message & "; Procedure Name: Insert; Module Name: " & mstrModuleName)
            Return False
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
            objTrainingApproval = Nothing
        End Try
    End Function

    ''' <summary>
    ''' Modify By: Hemant
    ''' </summary>
    ''' <returns>Boolean</returns>
    ''' <purpose> Update Database Table (trtraining_request_master) </purpose>
    Public Function Update(Optional ByVal xDataOp As clsDataOperation = Nothing) As Boolean
        If isExist(mintEmployeeunkid, mintCourseMasterunkid, mintPeriodunkid, mintTrainingRequestunkid, xDataOp) Then
            'Hemant (03 Feb 2023) -- [mintPeriodunkid]
            mstrMessage = Language.getMessage(mstrModuleName, 9, "Sorry, This Training is already defined. Please define new Training.")
            Return False
        End If
        'If isExist(mstrName, mintTrainingRequestunkid) Then
        '    mstrMessage = "<Message>"
        '    Return False
        'End If

        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        If xDataOp Is Nothing Then
            objDataOperation = New clsDataOperation
            objDataOperation.BindTransaction()
        Else
            objDataOperation = xDataOp
        End If
        objDataOperation.ClearParameters()

        Try
            objDataOperation.AddParameter("@trainingrequestunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintTrainingRequestunkid.ToString)
            objDataOperation.AddParameter("@application_date", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, mdtApplication_Date.ToString)
            objDataOperation.AddParameter("@employeeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintEmployeeunkid.ToString)
            objDataOperation.AddParameter("@coursemasterunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintCourseMasterunkid.ToString)
            objDataOperation.AddParameter("@isscheduled", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsScheduled.ToString)
            objDataOperation.AddParameter("@start_date", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, mdtStart_Date.ToString)
            objDataOperation.AddParameter("@end_date", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, mdtEnd_Date.ToString)
            objDataOperation.AddParameter("@provider_name", SqlDbType.NVarChar, mstrProviderName.Trim.Length, mstrProviderName.ToString)
            objDataOperation.AddParameter("@provider_address", SqlDbType.NVarChar, mstrProviderAddress.Trim.Length, mstrProviderAddress.ToString)
            objDataOperation.AddParameter("@fundingsourceunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintFundingSourceunkid.ToString)
            objDataOperation.AddParameter("@totaltrainingcost", SqlDbType.Decimal, eZeeDataType.DECIMAL_SIZE, mdecTotalTrainingCost.ToString)
            objDataOperation.AddParameter("@approvedamount", SqlDbType.Decimal, eZeeDataType.DECIMAL_SIZE, mdecApprovedCost.ToString)
            objDataOperation.AddParameter("@approvertranunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintApproverTranunkid.ToString)
            objDataOperation.AddParameter("@isalignedcurrentrole", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsAlignedCurrentRole.ToString)
            objDataOperation.AddParameter("@ispartofpdp", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsPartofPDP.ToString)
            objDataOperation.AddParameter("@isforeigntravelling", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsForeignTravelling.ToString)
            objDataOperation.AddParameter("@expectedreturn", SqlDbType.NVarChar, mstrExpectedReturn.Trim.Length, mstrExpectedReturn.ToString)
            objDataOperation.AddParameter("@remarks", SqlDbType.NVarChar, mstrRemarks.Trim.Length, mstrRemarks.ToString)
            objDataOperation.AddParameter("@statusunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintStatusunkid.ToString)
            objDataOperation.AddParameter("@issubmit_approval", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsSubmitApproval.ToString)
            objDataOperation.AddParameter("@userunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintUserunkid.ToString)
            objDataOperation.AddParameter("@loginemployeeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintLoginEmployeeunkid.ToString)
            objDataOperation.AddParameter("@isvoid", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsvoid.ToString)
            objDataOperation.AddParameter("@voiduserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintVoiduserunkid.ToString)
            objDataOperation.AddParameter("@voidloginemployeeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintVoidLoginEmployeeunkid.ToString)
            If mdtVoiddatetime = Nothing Then
                objDataOperation.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, DBNull.Value)
            Else
                objDataOperation.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, mdtVoiddatetime.ToString)
            End If
            objDataOperation.AddParameter("@voidreason", SqlDbType.NVarChar, mstrVoidreason.Trim.Length, mstrVoidreason.ToString)
            objDataOperation.AddParameter("@trainingproviderunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintTrainingproviderunkid.ToString)
            objDataOperation.AddParameter("@trainingvenueunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintTrainingvenueunkid.ToString)
            objDataOperation.AddParameter("@departmentaltrainingneedunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintDepartmentaltrainingneedunkid.ToString)
            objDataOperation.AddParameter("@isenroll_confirm", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsEnrollConfirm.ToString)
            objDataOperation.AddParameter("@isenroll_reject", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsEnrollReject.ToString)
            objDataOperation.AddParameter("@enroll_amount", SqlDbType.Decimal, eZeeDataType.DECIMAL_SIZE, mdecEnrollAmount.ToString)
            objDataOperation.AddParameter("@training_statusunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintTrainingStatusunkid.ToString)
            objDataOperation.AddParameter("@qualificationgroupunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintQualificationGroupunkid.ToString)
            objDataOperation.AddParameter("@qualificationunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintQualificationunkid.ToString)
            objDataOperation.AddParameter("@resultunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintResultunkid.ToString)
            objDataOperation.AddParameter("@gpacode", SqlDbType.Decimal, eZeeDataType.DECIMAL_SIZE, mdecGPAcode.ToString)
            objDataOperation.AddParameter("@isqualificaionupdated", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsqualificaionupdated.ToString)
            objDataOperation.AddParameter("@iscompleted_submit_approval", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsCompletedSubmitApproval.ToString)
            objDataOperation.AddParameter("@completed_statusunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintCompletedStatusunkid.ToString)
            objDataOperation.AddParameter("@periodunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintPeriodunkid.ToString)
            objDataOperation.AddParameter("@other_qualificationgrp", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrOtherQualificationGrp.ToString)
            objDataOperation.AddParameter("@other_qualification", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrOtherQualification.ToString)
            objDataOperation.AddParameter("@other_resultcode", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrOtherResultCode.ToString)
            objDataOperation.AddParameter("@completed_userunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintCompletedUserunkid.ToString)
            'Hemant (25 May 2021) -- Start
            'ISSUE/ENHANCEMENT : OLD-393 - Training module bug fixes and enhancements
            objDataOperation.AddParameter("@completed_remark", SqlDbType.NVarChar, mstrRemarks.Trim.Length, mstrCompletedRemark.ToString)
            objDataOperation.AddParameter("@enrollment_remark", SqlDbType.NVarChar, mstrRemarks.Trim.Length, mstrEnrollmentRemark.ToString)
            'Hemant (25 May 2021) -- End
            'Hemant (28 Jul 2021) -- Start             
            'ENHANCEMENT : OLD-293 - Training Evaluation
            If mdtCompletedApprovaldate <> Nothing Then
                objDataOperation.AddParameter("@completed_approval_date", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, mdtCompletedApprovaldate)
            Else
                objDataOperation.AddParameter("@completed_approval_date", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, DBNull.Value)
            End If
            'Hemant (28 Jul 2021) -- End
            'Hemant (20 Aug 2021) -- Start
            'ISSUE/ENHANCEMENT : OLD-447 - Give option to accommodate line manager feedback. It should be possible to mark questions as either for Manager or for Employee.
            objDataOperation.AddParameter("@isdaysafterfeedback_submitted", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsDaysAfterFeedbackSubmitted.ToString)
            If mdtDaysAfterFeedbackSubmittedDate <> Nothing Then
                objDataOperation.AddParameter("@daysafter_submitted_date", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, mdtDaysAfterFeedbackSubmittedDate)
            Else
                objDataOperation.AddParameter("@daysafter_submitted_date", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, DBNull.Value)
            End If
            objDataOperation.AddParameter("@daysafter_submitted_remark", SqlDbType.NVarChar, mstrRemarks.Trim.Length, mstrDaysAfterFeedbackSubmittedRemark.ToString)
            'Hemant (20 Aug 2021) -- End
            'Hemant (01 Sep 2021) -- Start
            'ISSUE/ENHANCEMENT : OLD-459 - SAVE button on Evaluation Forms in addition to the SUBMIT button.
            objDataOperation.AddParameter("@ispretrainingfeedback_submitted", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsPreTrainingFeedbackSubmitted.ToString)
            If mdtPreTrainingFeedbackSubmittedDate <> Nothing Then
                objDataOperation.AddParameter("@pretraining_submitted_date", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, mdtPreTrainingFeedbackSubmittedDate)
            Else
                objDataOperation.AddParameter("@pretraining_submitted_date", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, DBNull.Value)
            End If
            objDataOperation.AddParameter("@isposttrainingfeedback_submitted", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsPostTrainingFeedbackSubmitted.ToString)
            If mdtPostTrainingFeedbackSubmittedDate <> Nothing Then
                objDataOperation.AddParameter("@posttraining_submitted_date", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, mdtPostTrainingFeedbackSubmittedDate)
            Else
                objDataOperation.AddParameter("@posttraining_submitted_date", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, DBNull.Value)
            End If
            objDataOperation.AddParameter("@isdaysafter_linemanager_submitted", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsDaysAfterLineManagerFeedbackSubmitted.ToString)
            If mdtDaysAfterLineManagerFeedbackSubmittedDate <> Nothing Then
                objDataOperation.AddParameter("@daysafter_linemanager_submitted_date", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, mdtDaysAfterLineManagerFeedbackSubmittedDate)
            Else
                objDataOperation.AddParameter("@daysafter_linemanager_submitted_date", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, DBNull.Value)
            End If
            'Hemant (01 Sep 2021) -- End
            'Hemant (23 Sep 2021) -- Start
            'ENHANCEMENT : OLD-470 - Silverlands - New Training Setting to skip Training Requests Process.
            objDataOperation.AddParameter("@isskip_trainingrequest_and_approval", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsSkipTrainingRequestAndApproval.ToString)
            'Hemant (23 Sep 2021) -- End
            'Hemant (09 Feb 2022) -- Start            
            'OLD-551(NMB) : New config setting to allow Training Budget settings to be based on any allocation - Cost Center etc. 
            objDataOperation.AddParameter("@trainingcostemp", SqlDbType.Decimal, eZeeDataType.DECIMAL_SIZE, mdecTrainingCostEmp.ToString)
            objDataOperation.AddParameter("@approvedamountemp", SqlDbType.Decimal, eZeeDataType.DECIMAL_SIZE, mdecApprovedAmountEmp.ToString)
            'Hemant (09 Feb 2022) -- End
            'Sohail (15 Mar 2022) -- Start
            'Enhancement : OLD-583 : NMB - Modification of Training Request Approval Screen to allow batch approvals.
            objDataOperation.AddParameter("@refno", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrRefno.ToString)
            objDataOperation.AddParameter("@createuserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintCreateuserunkid.ToString)
            objDataOperation.AddParameter("@createloginemployeeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintCreateloginEmployeeunkid.ToString)
            'Sohail (15 Mar 2022) -- End
            'Hemant (29 Apr 2022) -- Start
            'ISSUE/ENHANCEMENT : AC2-314(NMB) - NMB - As a manager, I want different approval flow (Training requests) for different training courses.
            objDataOperation.AddParameter("@trainingtypeid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintTrainingTypeId.ToString)
            'Hemant (29 Apr 2022) -- End
            'Hemant (25 Jul 2022) -- Start            
            'ENHANCEMENT(NMB) : AC2-723 - Implementation of Approver in Training request form
            objDataOperation.AddParameter("@approvalsettingid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintTrainingApprovalSettingID.ToString)
            'Hemant (25 Jul 2022) -- End
            'Hemant (12 Oct 2022) -- Start
            'ENHANCEMENT(NMB) :  AC2-952 - As a user, I want to have a document attachment option on the group training request page
            objDataOperation.AddParameter("@grouptrainingrequestunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintGroupTrainingRequestunkid.ToString)
            'Hemant (12 Oct 2022) -- End
            'Hemant (10 Nov 2022) -- Start
            'ENHANCEMENT(NMB) :  AC2-1024 - As a user, I want to have a field to capture the Training Venue on both individual and group training request pages. This filed will be free text and mandatory
            objDataOperation.AddParameter("@venue", SqlDbType.NVarChar, mstrVenue.Trim.Length, mstrVenue.ToString)
            'Hemant (27 Oct 2022) -- End

            strQ = "UPDATE trtraining_request_master SET " & _
                        "  application_date = @application_date " & _
                        ", employeeunkid = @employeeunkid " & _
                        ", coursemasterunkid = @coursemasterunkid " & _
                        ", isscheduled = @isscheduled " & _
                        ", start_date = @start_date " & _
                        ", end_date = @end_date " & _
                        ", provider_name = @provider_name " & _
                        ", provider_address = @provider_address " & _
                        ", fundingsourceunkid = @fundingsourceunkid " & _
                        ", totaltrainingcost = @totaltrainingcost " & _
                        ", approvedamount = @approvedamount " & _
                        ", approvertranunkid = @approvertranunkid" & _
                        ", isalignedcurrentrole = @isalignedcurrentrole " & _
                        ", ispartofpdp = @ispartofpdp " & _
                        ", isforeigntravelling = @isforeigntravelling " & _
                        ", expectedreturn = @expectedreturn " & _
                        ", remarks = @remarks " & _
                        ", statusunkid = @statusunkid " & _
                        ", issubmit_approval = @issubmit_approval " & _
                        ", userunkid = @userunkid " & _
                        ", loginemployeeunkid = @loginemployeeunkid " & _
                        ", isvoid = @isvoid " & _
                        ", voiduserunkid = @voiduserunkid " & _
                        ", voidloginemployeeunkid = @voidloginemployeeunkid " & _
                        ", voiddatetime = @voiddatetime " & _
                        ", voidreason = @voidreason " & _
                        ", trainingproviderunkid = @trainingproviderunkid" & _
                        ", trainingvenueunkid = @trainingvenueunkid " & _
                        ", departmentaltrainingneedunkid = @departmentaltrainingneedunkid " & _
                        ", isenroll_confirm = @isenroll_confirm " & _
                        ", isenroll_reject = @isenroll_reject " & _
                        ", enroll_amount = @enroll_amount " & _
                        ", training_statusunkid = @training_statusunkid " & _
                        ", qualificationgroupunkid = @qualificationgroupunkid " & _
                        ", qualificationunkid = @qualificationunkid " & _
                        ", resultunkid = @resultunkid " & _
                        ", gpacode = @gpacode " & _
                        ", isqualificaionupdated = @isqualificaionupdated " & _
                        ", iscompleted_submit_approval = @iscompleted_submit_approval " & _
                        ", completed_statusunkid = @completed_statusunkid " & _
                        ", periodunkid = @periodunkid " & _
                        ", other_qualificationgrp = @other_qualificationgrp " & _
                        ", other_qualification = @other_qualification " & _
                        ", other_resultcode = @other_resultcode " & _
                        ", completed_userunkid = @completed_userunkid " & _
                        ", completed_remark = @completed_remark " & _
                        ", enrollment_remark = @enrollment_remark " & _
                        ", completed_approval_date = @completed_approval_date " & _
                        ", isdaysafterfeedback_submitted = @isdaysafterfeedback_submitted " & _
                        ", daysafter_submitted_date = @daysafter_submitted_date " & _
                        ", daysafter_submitted_remark = @daysafter_submitted_remark " & _
                        ", ispretrainingfeedback_submitted = @ispretrainingfeedback_submitted " & _
                        ", pretraining_submitted_date = @pretraining_submitted_date " & _
                        ", isposttrainingfeedback_submitted = @isposttrainingfeedback_submitted " & _
                        ", posttraining_submitted_date = @posttraining_submitted_date " & _
                        ", isdaysafter_linemanager_submitted = @isdaysafter_linemanager_submitted " & _
                        ", daysafter_linemanager_submitted_date = @daysafter_linemanager_submitted_date " & _
                        ", isskip_trainingrequest_and_approval = @isskip_trainingrequest_and_approval " & _
                        ", trainingcostemp = @trainingcostemp " & _
                        ", approvedamountemp = @approvedamountemp " & _
                        ", trainingtypeid = @trainingtypeid " & _
                        ", approvalsettingid = @approvalsettingid " & _
                        ", grouptrainingrequestunkid = @grouptrainingrequestunkid " & _
                        ", venue = @venue " & _
                    "WHERE trainingrequestunkid = @trainingrequestunkid "
            'Hemant (10 Nov 2022) -- [venue]
            'Hemant (12 Oct 2022) -- [grouptrainingrequestunkid]
            'Hemant (25 Jul 2022) -- [approvalsettingid]
            'Hemant (29 Apr 2022) -- [trainingtypeid]
            'Hemant (09 Feb 2022) -- [trainingcostemp,approvedamountemp]
            'Hemant (23 Sep 2021) -- [isskip_trainingrequest_and_approval]
            'Hemant (01 Sep 2021) -- [ispretrainingfeedback_submitted,pretraining_submitted_date,isposttrainingfeedback_submitted,posttraining_submitted_date,isdaysafter_linemanager_submitted,daysafter_linemanager_submitted_date]
            'Hemant (20 Aug 2021) -- [isdaysafterfeedback_submitted,daysafter_submitted_date,daysafter_submitted_remark]
            'Hemant (28 Jul 2021) -- [completed_approval_date]
            'Hemant (25 May 2021) -- [completed_remark,enrollment_remark]
            Call objDataOperation.ExecNonQuery(strQ)

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            If IsTableDataUpdate(mintTrainingRequestunkid, objDataOperation) = False Then

                If InsertAuditTrails(objDataOperation, 2) = False Then
                    If xDataOp Is Nothing Then objDataOperation.ReleaseTransaction(False)
                    Return False
                End If

            End If

            If xDataOp Is Nothing Then objDataOperation.ReleaseTransaction(True)

            Return True
        Catch ex As Exception
            If xDataOp Is Nothing Then objDataOperation.ReleaseTransaction(False)
            Throw New Exception(ex.Message & "; Procedure Name: Update; Module Name: " & mstrModuleName)
            Return False
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            If xDataOp Is Nothing Then objDataOperation = Nothing
        End Try
    End Function

    ''' <summary>
    ''' Modify By: Hemant
    ''' </summary>
    ''' <returns>Boolean</returns>
    ''' <purpose> Update Database Table (trtraining_request_master) </purpose>
    Public Function Update(ByVal dtCostTran As DataTable, _
                           ByVal xDatabaseName As String, _
                           ByVal xUserUnkid As Integer, _
                           ByVal xYearUnkid As Integer, _
                           ByVal xCompanyUnkid As Integer, _
                           ByVal strUserAccessMode As String, _
                           ByVal xEmployeeAsOnDate As String, _
                           ByVal xTrainingApproverAllocationID As Integer, _
                           Optional ByVal xDataOp As clsDataOperation = Nothing) As Boolean
        'Hemant (09 Feb 2022) -- [xTrainingApproverAllocationID]
        If isExist(mintEmployeeunkid, mintCourseMasterunkid, mintPeriodunkid, mintTrainingRequestunkid) Then
            'Hemant (03 Feb 2023) -- [mintPeriodunkid]
            mstrMessage = Language.getMessage(mstrModuleName, 9, "Sorry, This Training is already defined. Please define new Training.")
            Return False
        End If

        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        Dim objCostTran As New clstraining_request_cost_tran
        Dim objTrainingApproval As New clstrainingapproval_process_tran

        Dim objDataOperation As clsDataOperation
        If xDataOp Is Nothing Then
            objDataOperation = New clsDataOperation
            objDataOperation.BindTransaction()
        Else
            objDataOperation = xDataOp
        End If
        objDataOperation.ClearParameters()

        Try
            objDataOperation.AddParameter("@trainingrequestunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintTrainingRequestunkid.ToString)
            objDataOperation.AddParameter("@application_date", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, mdtApplication_Date.ToString)
            objDataOperation.AddParameter("@employeeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintEmployeeunkid.ToString)
            objDataOperation.AddParameter("@coursemasterunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintCourseMasterunkid.ToString)
            objDataOperation.AddParameter("@isscheduled", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsScheduled.ToString)
            objDataOperation.AddParameter("@start_date", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, mdtStart_Date.ToString)
            objDataOperation.AddParameter("@end_date", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, mdtEnd_Date.ToString)
            objDataOperation.AddParameter("@provider_name", SqlDbType.NVarChar, mstrProviderName.Trim.Length, mstrProviderName.ToString)
            objDataOperation.AddParameter("@provider_address", SqlDbType.NVarChar, mstrProviderAddress.Trim.Length, mstrProviderAddress.ToString)
            objDataOperation.AddParameter("@fundingsourceunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintFundingSourceunkid.ToString)
            objDataOperation.AddParameter("@totaltrainingcost", SqlDbType.Decimal, eZeeDataType.DECIMAL_SIZE, mdecTotalTrainingCost.ToString)
            objDataOperation.AddParameter("@approvedamount", SqlDbType.Decimal, eZeeDataType.DECIMAL_SIZE, mdecApprovedCost.ToString)
            objDataOperation.AddParameter("@approvertranunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintApproverTranunkid.ToString)
            objDataOperation.AddParameter("@isalignedcurrentrole", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsAlignedCurrentRole.ToString)
            objDataOperation.AddParameter("@ispartofpdp", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsPartofPDP.ToString)
            objDataOperation.AddParameter("@isforeigntravelling", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsForeignTravelling.ToString)
            objDataOperation.AddParameter("@expectedreturn", SqlDbType.NVarChar, mstrExpectedReturn.Trim.Length, mstrExpectedReturn.ToString)
            objDataOperation.AddParameter("@remarks", SqlDbType.NVarChar, mstrRemarks.Trim.Length, mstrRemarks.ToString)
            objDataOperation.AddParameter("@statusunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintStatusunkid.ToString)
            objDataOperation.AddParameter("@issubmit_approval", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsSubmitApproval.ToString)
            objDataOperation.AddParameter("@userunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintUserunkid.ToString)
            objDataOperation.AddParameter("@loginemployeeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintLoginEmployeeunkid.ToString)
            objDataOperation.AddParameter("@isvoid", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsvoid.ToString)
            objDataOperation.AddParameter("@voiduserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintVoiduserunkid.ToString)
            objDataOperation.AddParameter("@voidloginemployeeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintVoidLoginEmployeeunkid.ToString)
            If mdtVoiddatetime = Nothing Then
                objDataOperation.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, DBNull.Value)
            Else
                objDataOperation.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, mdtVoiddatetime.ToString)
            End If
            objDataOperation.AddParameter("@voidreason", SqlDbType.NVarChar, mstrVoidreason.Trim.Length, mstrVoidreason.ToString)
            objDataOperation.AddParameter("@trainingproviderunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintTrainingproviderunkid.ToString)
            objDataOperation.AddParameter("@trainingvenueunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintTrainingvenueunkid.ToString)
            objDataOperation.AddParameter("@departmentaltrainingneedunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintDepartmentaltrainingneedunkid.ToString)
            objDataOperation.AddParameter("@isenroll_confirm", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsEnrollConfirm.ToString)
            objDataOperation.AddParameter("@isenroll_reject", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsEnrollReject.ToString)
            objDataOperation.AddParameter("@enroll_amount", SqlDbType.Decimal, eZeeDataType.DECIMAL_SIZE, mdecEnrollAmount.ToString)
            objDataOperation.AddParameter("@training_statusunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintTrainingStatusunkid.ToString)
            objDataOperation.AddParameter("@qualificationgroupunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintQualificationGroupunkid.ToString)
            objDataOperation.AddParameter("@qualificationunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintQualificationunkid.ToString)
            objDataOperation.AddParameter("@resultunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintResultunkid.ToString)
            objDataOperation.AddParameter("@gpacode", SqlDbType.Decimal, eZeeDataType.DECIMAL_SIZE, mdecGPAcode.ToString)
            objDataOperation.AddParameter("@isqualificaionupdated", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsqualificaionupdated.ToString)
            objDataOperation.AddParameter("@iscompleted_submit_approval", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsCompletedSubmitApproval.ToString)
            objDataOperation.AddParameter("@completed_statusunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintCompletedStatusunkid.ToString)
            objDataOperation.AddParameter("@periodunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintPeriodunkid.ToString)
            objDataOperation.AddParameter("@other_qualificationgrp", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrOtherQualificationGrp.ToString)
            objDataOperation.AddParameter("@other_qualification", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrOtherQualification.ToString)
            objDataOperation.AddParameter("@other_ResultCode", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrOtherResultCode.ToString)
            objDataOperation.AddParameter("@completed_userunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintCompletedUserunkid.ToString)
            'Hemant (25 May 2021) -- Start
            'ISSUE/ENHANCEMENT : OLD-393 - Training module bug fixes and enhancements
            objDataOperation.AddParameter("@completed_remark", SqlDbType.NVarChar, mstrRemarks.Trim.Length, mstrCompletedRemark.ToString)
            objDataOperation.AddParameter("@enrollment_remark", SqlDbType.NVarChar, mstrRemarks.Trim.Length, mstrEnrollmentRemark.ToString)
            'Hemant (25 May 2021) -- End
            'Hemant (28 Jul 2021) -- Start             
            'ENHANCEMENT : OLD-293 - Training Evaluation
            If mdtCompletedApprovaldate <> Nothing Then
                objDataOperation.AddParameter("@completed_approval_date", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, mdtCompletedApprovaldate)
            Else
                objDataOperation.AddParameter("@completed_approval_date", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, DBNull.Value)
            End If
            'Hemant (28 Jul 2021) -- End
            'Hemant (20 Aug 2021) -- Start
            'ISSUE/ENHANCEMENT : OLD-447 - Give option to accommodate line manager feedback. It should be possible to mark questions as either for Manager or for Employee.
            objDataOperation.AddParameter("@isdaysafterfeedback_submitted", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsDaysAfterFeedbackSubmitted.ToString)
            If mdtDaysAfterFeedbackSubmittedDate <> Nothing Then
                objDataOperation.AddParameter("@daysafter_submitted_date", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, mdtDaysAfterFeedbackSubmittedDate)
            Else
                objDataOperation.AddParameter("@daysafter_submitted_date", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, DBNull.Value)
            End If
            objDataOperation.AddParameter("@daysafter_submitted_remark", SqlDbType.NVarChar, mstrRemarks.Trim.Length, mstrDaysAfterFeedbackSubmittedRemark.ToString)
            'Hemant (20 Aug 2021) -- End
            'Hemant (01 Sep 2021) -- Start
            'ISSUE/ENHANCEMENT : OLD-459 - SAVE button on Evaluation Forms in addition to the SUBMIT button.
            objDataOperation.AddParameter("@ispretrainingfeedback_submitted", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsPreTrainingFeedbackSubmitted.ToString)
            If mdtPreTrainingFeedbackSubmittedDate <> Nothing Then
                objDataOperation.AddParameter("@pretraining_submitted_date", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, mdtPreTrainingFeedbackSubmittedDate)
            Else
                objDataOperation.AddParameter("@pretraining_submitted_date", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, DBNull.Value)
            End If
            objDataOperation.AddParameter("@isposttrainingfeedback_submitted", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsPostTrainingFeedbackSubmitted.ToString)
            If mdtPostTrainingFeedbackSubmittedDate <> Nothing Then
                objDataOperation.AddParameter("@posttraining_submitted_date", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, mdtPostTrainingFeedbackSubmittedDate)
            Else
                objDataOperation.AddParameter("@posttraining_submitted_date", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, DBNull.Value)
            End If
            objDataOperation.AddParameter("@isdaysafter_linemanager_submitted", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsDaysAfterLineManagerFeedbackSubmitted.ToString)
            If mdtDaysAfterLineManagerFeedbackSubmittedDate <> Nothing Then
                objDataOperation.AddParameter("@daysafter_linemanager_submitted_date", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, mdtDaysAfterLineManagerFeedbackSubmittedDate)
            Else
                objDataOperation.AddParameter("@daysafter_linemanager_submitted_date", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, DBNull.Value)
            End If
            'Hemant (01 Sep 2021) -- End
            'Hemant (23 Sep 2021) -- Start
            'ENHANCEMENT : OLD-470 - Silverlands - New Training Setting to skip Training Requests Process.
            objDataOperation.AddParameter("@isskip_trainingrequest_and_approval", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsSkipTrainingRequestAndApproval.ToString)
            'Hemant (23 Sep 2021) -- End
            'Hemant (09 Feb 2022) -- Start            
            'OLD-551(NMB) : New config setting to allow Training Budget settings to be based on any allocation - Cost Center etc. 
            objDataOperation.AddParameter("@trainingcostemp", SqlDbType.Decimal, eZeeDataType.DECIMAL_SIZE, mdecTrainingCostEmp.ToString)
            objDataOperation.AddParameter("@approvedamountemp", SqlDbType.Decimal, eZeeDataType.DECIMAL_SIZE, mdecApprovedAmountEmp.ToString)
            'Hemant (09 Feb 2022) -- End
            'Sohail (15 Mar 2022) -- Start
            'Enhancement : OLD-583 : NMB - Modification of Training Request Approval Screen to allow batch approvals.
            objDataOperation.AddParameter("@refno", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrRefno.ToString)
            objDataOperation.AddParameter("@createuserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintCreateuserunkid.ToString)
            objDataOperation.AddParameter("@createloginemployeeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintCreateloginEmployeeunkid.ToString)
            'Sohail (15 Mar 2022) -- End
            'Hemant (29 Apr 2022) -- Start
            'ISSUE/ENHANCEMENT : AC2-314(NMB) - NMB - As a manager, I want different approval flow (Training requests) for different training courses.
            objDataOperation.AddParameter("@trainingtypeid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintTrainingTypeId.ToString)
            'Hemant (29 Apr 2022) -- End
            'Hemant (25 Jul 2022) -- Start            
            'ENHANCEMENT(NMB) : AC2-723 - Implementation of Approver in Training request form
            objDataOperation.AddParameter("@approvalsettingid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintTrainingApprovalSettingID.ToString)
            'Hemant (25 Jul 2022) -- End
            'Hemant (12 Oct 2022) -- Start
            'ENHANCEMENT(NMB) :  AC2-952 - As a user, I want to have a document attachment option on the group training request page
            objDataOperation.AddParameter("@grouptrainingrequestunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintGroupTrainingRequestunkid.ToString)
            'Hemant (12 Oct 2022) -- End
            'Hemant (10 Nov 2022) -- Start
            'ENHANCEMENT(NMB) :  AC2-1024 - As a user, I want to have a field to capture the Training Venue on both individual and group training request pages. This filed will be free text and mandatory
            objDataOperation.AddParameter("@venue", SqlDbType.NVarChar, mstrVenue.Trim.Length, mstrVenue.ToString)
            'Hemant (27 Oct 2022) -- End

            strQ = "UPDATE trtraining_request_master SET " & _
                        "  application_date = @application_date " & _
                        ", employeeunkid = @employeeunkid " & _
                        ", coursemasterunkid = @coursemasterunkid " & _
                        ", isscheduled = @isscheduled " & _
                        ", start_date = @start_date " & _
                        ", end_date = @end_date " & _
                        ", provider_name = @provider_name " & _
                        ", provider_address = @provider_address " & _
                        ", fundingsourceunkid = @fundingsourceunkid " & _
                        ", totaltrainingcost = @totaltrainingcost " & _
                        ", approvedamount = @approvedamount " & _
                        ", approvertranunkid = @approvertranunkid" & _
                        ", isalignedcurrentrole = @isalignedcurrentrole " & _
                        ", ispartofpdp = @ispartofpdp " & _
                        ", isforeigntravelling = @isforeigntravelling " & _
                        ", expectedreturn = @expectedreturn " & _
                        ", remarks = @remarks " & _
                        ", statusunkid = @statusunkid " & _
                        ", issubmit_approval = @issubmit_approval " & _
                        ", userunkid = @userunkid " & _
                        ", loginemployeeunkid = @loginemployeeunkid " & _
                        ", isvoid = @isvoid " & _
                        ", voiduserunkid = @voiduserunkid " & _
                        ", voidloginemployeeunkid = @voidloginemployeeunkid " & _
                        ", voiddatetime = @voiddatetime " & _
                        ", voidreason = @voidreason " & _
                        ", trainingproviderunkid = @trainingproviderunkid" & _
                        ", trainingvenueunkid = @trainingvenueunkid " & _
                        ", departmentaltrainingneedunkid = @departmentaltrainingneedunkid " & _
                        ", isenroll_confirm = @isenroll_confirm " & _
                        ", isenroll_reject = @isenroll_reject " & _
                        ", enroll_amount = @enroll_amount " & _
                        ", training_statusunkid = @training_statusunkid " & _
                        ", qualificationgroupunkid = @qualificationgroupunkid " & _
                        ", qualificationunkid = @qualificationunkid " & _
                        ", resultunkid = @resultunkid " & _
                        ", gpacode = @gpacode " & _
                        ", isqualificaionupdated = @isqualificaionupdated " & _
                        ", iscompleted_submit_approval = @iscompleted_submit_approval " & _
                        ", completed_statusunkid = @completed_statusunkid " & _
                        ", periodunkid = @periodunkid " & _
                        ", other_qualificationgrp = @other_qualificationgrp " & _
                        ", other_qualification = @other_qualification " & _
                        ", other_resultcode = @other_resultcode " & _
                        ", completed_userunkid = @completed_userunkid " & _
                        ", completed_remark = @completed_remark " & _
                        ", enrollment_remark = @enrollment_remark " & _
                        ", completed_approval_date = @completed_approval_date " & _
                        ", isdaysafterfeedback_submitted = @isdaysafterfeedback_submitted " & _
                        ", daysafter_submitted_date = @daysafter_submitted_date " & _
                        ", daysafter_submitted_remark = @daysafter_submitted_remark " & _
                        ", ispretrainingfeedback_submitted = @ispretrainingfeedback_submitted " & _
                        ", pretraining_submitted_date = @pretraining_submitted_date " & _
                        ", isposttrainingfeedback_submitted = @isposttrainingfeedback_submitted " & _
                        ", posttraining_submitted_date = @posttraining_submitted_date " & _
                        ", isdaysafter_linemanager_submitted = @isdaysafter_linemanager_submitted " & _
                        ", daysafter_linemanager_submitted_date = @daysafter_linemanager_submitted_date " & _
                        ", isskip_trainingrequest_and_approval = @isskip_trainingrequest_and_approval " & _
                        ", trainingcostemp = @trainingcostemp " & _
                        ", approvedamountemp = @approvedamountemp " & _
                        ", trainingtypeid = @trainingtypeid " & _
                        ", approvalsettingid = @approvalsettingid " & _
                        ", grouptrainingrequestunkid = @grouptrainingrequestunkid " & _
                        ", venue = @venue " & _
                    "WHERE trainingrequestunkid = @trainingrequestunkid "
            'Hemant (10 Nov 2022) -- [venue]
            'Hemant (12 Oct 2022) -- [grouptrainingrequestunkid]
            'Hemant (25 Jul 2022) -- [approvalsettingid]
            'Hemant (29 Apr 2022) -- [trainingtypeid]
            'Hemant (09 Feb 2022) -- [trainingcostemp,approvedamountemp]
            'Hemant (23 Sep 2021) -- [isskip_trainingrequest_and_approval]
            'Hemant (01 Sep 2021) -- [ispretrainingfeedback_submitted,pretraining_submitted_date,isposttrainingfeedback_submitted,posttraining_submitted_date,isdaysafter_linemanager_submitted,daysafter_linemanager_submitted_date]
            'Hemant (20 Aug 2021) -- [isdaysafterfeedback_submitted,daysafter_submitted_date,daysafter_submitted_remark]
            'Hemant (28 Jul 2021) -- [completed_approval_date]
            'Hemant (25 May 2021) -- [completed_remark,enrollment_remark]
            Call objDataOperation.ExecNonQuery(strQ)

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            If InsertAuditTrails(objDataOperation, enAuditType.EDIT) = False Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            If dtCostTran IsNot Nothing AndAlso dtCostTran.Rows.Count > 0 Then
                With objCostTran
                    ._TrainingRequestunkid = mintTrainingRequestunkid
                    ._TranDataTable = dtCostTran
                    ._IsWeb = mblnIsWeb
                    ._Userunkid = mintUserunkid
                    ._LoginEmployeeunkid = mintLoginEmployeeunkid
                    ._ClientIP = mstrClientIP
                    ._FormName = mstrFormName
                    ._HostName = mstrHostName
                End With

                If objCostTran.Insert_Update_Delete(objDataOperation) = False Then
                    exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                    Throw exForce
                End If
            End If

            If mblnIsSubmitApproval = True AndAlso mblnIsSkipTrainingRequestAndApproval = False Then
                'Hemant (23 Sep 2021) -- [mblnIsSkipTrainingRequestAndApproval = False]

                Dim objTrainingApprover As New clstraining_approver_master
                Dim intPrivilegeId As Integer
                'Dim dtApprover As DataTable = objTrainingApprover.GetEmployeeApprover(xDatabaseName, _
                '                                                                  xUserUnkid, _
                '                                                                  xYearUnkid, _
                '                                                                  xCompanyUnkid, _
                '                                                                  mintEmployeeunkid, _
                '                                                                  mintMaxPriority)
                If mblnIsForeignTravelling = True Then
                    'Hemant (29 Apr 2022) -- Start
                    'ISSUE/ENHANCEMENT : AC2-314(NMB) - NMB - As a manager, I want different approval flow (Training requests) for different training courses.
                    If mintTrainingTypeId = enTrainingTravelType.Local_Travel Then
                        intPrivilegeId = enUserPriviledge.AllowToApproveTrainingRequestLocalTravelling
                    ElseIf mintTrainingTypeId = enTrainingTravelType.Foreign_Travel Then
                        'Hemant (29 Apr 2022) -- End
                    intPrivilegeId = enUserPriviledge.AllowToApproveTrainingRequestForeignTravelling
                    End If 'Hemant (29 Apr 2022)
                Else
                    intPrivilegeId = enUserPriviledge.AllowToApproveTrainingRequestRelatedToCost
                End If
                Dim dtApprover As DataTable = objTrainingApprover.GetNextEmployeeApprovers(xDatabaseName, _
                                                                                           xCompanyUnkid, _
                                                                                           xYearUnkid, _
                                                                                           strUserAccessMode, _
                                                                                           intPrivilegeId, _
                                                                                           xEmployeeAsOnDate, _
                                                                                           xUserUnkid, _
                                                                                           mintEmployeeunkid, _
                                                                                           mdecTotalTrainingCost, _
                                                                                           mintPeriodunkid, _
                                                                                           xTrainingApproverAllocationID, _
                                                                                               mintTrainingTypeId, _
                                                                                           mintInsertFormId, _
                                                                                           objDataOperation _
                                                                                           )
                'Hemant (27 Jun 2022) -- [mintInsertFormId]
                    'Hemant (29 Apr 2022) -- [mintTrainingTypeId]
                'Hemant (09 Feb 2022) -- [xTrainingApproverAllocationID]
                'Hemant (03 Dec 2021) -- [mintPeriodunkid]
                objTrainingApprover = Nothing
                Dim blnEnableVisibility As Boolean = False
                Dim intMinPriority As Integer = -1
                Dim intApproverID As Integer = -1

                If dtApprover IsNot Nothing AndAlso dtApprover.Rows.Count > 0 Then
                    For Each drRow As DataRow In dtApprover.Rows
                        objTrainingApproval._TrainingRequestunkid = mintTrainingRequestunkid
                        objTrainingApproval._Employeeunkid = mintEmployeeunkid
                        objTrainingApproval._Approvertranunkid = CInt(drRow("trapproverunkid"))
                        objTrainingApproval._Approvaldate = mdtApplication_Date
                        objTrainingApproval._Priority = CInt(drRow("priority"))
                        objTrainingApproval._TotalCostAmount = mdecTotalTrainingCost
                        objTrainingApproval._ApprovedAmount = mdecApprovedCost
                        objTrainingApproval._Statusunkid = mintStatusunkid
                        objTrainingApproval._Userunkid = mintUserunkid
                        objTrainingApproval._ClientIP = mstrClientIP
                        objTrainingApproval._FormName = mstrFormName
                        objTrainingApproval._HostName = mstrHostName
                        objTrainingApproval._Mapuserunkid = CInt(drRow("mapuserunkid"))
                        objTrainingApproval._IsWeb = mblnIsWeb

                        intMinPriority = CInt(dtApprover.Compute("MIN(priority)", "1=1"))

                        If intMinPriority = CInt(drRow("priority")) Then
                            If mintUserunkid = CInt(drRow("mapuserunkid")) Then
                                objTrainingApproval._Statusunkid = clstraining_requisition_approval_master.enApprovalStatus.Approved
                                mintMinApprovedPriority = CInt(drRow("priority"))
                                intApproverID = CInt(drRow("trapproverunkid"))
                                blnEnableVisibility = True
                            End If

                            If blnEnableVisibility = True Then
                                objTrainingApproval._VisibleId = clstraining_requisition_approval_master.enApprovalStatus.Approved
                                'Hemant (25 May 2021) -- Start
                                'ISSUE/ENHANCEMENT : OLD-393 - Training module bug fixes and enhancements
                                mdecApprovedCost = mdecTotalTrainingCost
                                objTrainingApproval._ApprovedAmount = mdecApprovedCost
                                'Hemant (25 May 2021) -- End
                            Else
                                Dim dRow As DataRow() = dtApprover.Select("priority=" & intMinPriority & " AND mapuserunkid=" & mintUserunkid & "")
                                If dRow.Length > 0 Then
                                    objTrainingApproval._VisibleId = clstraining_requisition_approval_master.enApprovalStatus.Approved
                                    'Hemant (25 May 2021) -- Start
                                    'ISSUE/ENHANCEMENT : OLD-393 - Training module bug fixes and enhancements
                                    mdecApprovedCost = mdecTotalTrainingCost
                                    objTrainingApproval._ApprovedAmount = mdecApprovedCost
                                    'Hemant (25 May 2021) -- End
                                Else
                                    objTrainingApproval._VisibleId = mintStatusunkid
                                End If
                            End If
                        Else

                            If blnEnableVisibility = True Then
                                Dim intNextMinPriority As Integer = CInt(dtApprover.Compute("MIN(priority)", "priority > " & intMinPriority))
                                If intNextMinPriority = CInt(drRow("priority")) Then
                                    objTrainingApproval._VisibleId = clstraining_requisition_approval_master.enApprovalStatus.SubmitForApproval
                                Else
                                    objTrainingApproval._VisibleId = -1
                                End If
                            Else
                                objTrainingApproval._VisibleId = -1
                            End If

                        End If

                        If objTrainingApproval.Insert(objDataOperation) = False Then
                            exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                            Throw exForce
                        End If
                    Next
                    'Hemant (25 May 2021) -- Start
                    'ISSUE/ENHANCEMENT : OLD-393 - Training module bug fixes and enhancements
                    If blnEnableVisibility = True Then
                        Dim intMaxPriority As Integer = CInt(dtApprover.Compute("MAX(priority)", "1=1"))
                        If intMaxPriority = intMinPriority Then
                            strQ = " UPDATE trtraining_request_master SET " & _
                              "      statusunkid = " & enTrainingRequestStatus.APPROVED & " " & _
                              "     ,approverunkid = " & intApproverID & " " & _
                              "     ,approved_amount = " & mdecTotalTrainingCost & " " & _
                              " WHERE isvoid=0 AND trainingrequestunkid = @trainingrequestunkid "

                            objDataOperation.ClearParameters()
                            objDataOperation.AddParameter("@trainingrequestunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintTrainingRequestunkid.ToString)

                            objDataOperation.ExecNonQuery(strQ)

                            If objDataOperation.ErrorMessage <> "" Then
                                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                                Throw exForce
                            End If

                            mintStatusunkid = enTrainingRequestStatus.APPROVED

                            If InsertAuditTrails(objDataOperation, enAuditType.EDIT) = False Then
                                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                                Throw exForce
                            End If
                        End If
                    End If
                    dtApprover.Rows.Clear()
                    'Hemant (25 May 2021) -- End
                End If
                'Hemant (25 May 2021) -- Start
                'ISSUE/ENHANCEMENT : OLD-393 - Training module bug fixes and enhancements
                dtApprover = Nothing
                'Hemant (25 May 2021) -- End
            End If

            If xDataOp Is Nothing Then objDataOperation.ReleaseTransaction(True)

            Return True
        Catch ex As Exception
            If xDataOp Is Nothing Then objDataOperation.ReleaseTransaction(False)
            Throw New Exception(ex.Message & "; Procedure Name: Update; Module Name: " & mstrModuleName)
            Return False
        Finally
            exForce = Nothing
            objCostTran = Nothing
            objTrainingApproval = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            If xDataOp Is Nothing Then objDataOperation = Nothing
        End Try
    End Function

    Public Function Save(ByVal xDatabaseName As String, _
                         ByVal xUserUnkid As Integer, _
                         ByVal xYearUnkid As Integer, _
                         ByVal xCompanyUnkid As Integer, _
                         ByVal strUserAccessMode As String, _
                         ByVal xEmployeeAsOnDate As String, _
                         ByVal xTrainingApproverAllocationID As Integer, _
                         Optional ByVal xDataOp As clsDataOperation = Nothing, _
                         Optional ByVal mdtAttachmentTable As DataTable = Nothing) As Boolean
        'Hemant (12 Oct 2022) -- [mdtAttachmentTable]
        'Hemant (09 Feb 2022) -- [xTrainingApproverAllocationID]
        Dim objCostTran As New clstraining_request_cost_tran
        Dim objTrainingApproval As New clstrainingapproval_process_tran
        Dim exForce As Exception
        Dim dsList As DataSet = Nothing
        'Hemant (25 May 2021) -- Start
        'ISSUE/ENHANCEMENT : OLD-393 - Training module bug fixes and enhancements
        Dim strQ As String = ""
        'Hemant (25 May 2021) -- End

        If xDataOp Is Nothing Then
            objDataOperation = New clsDataOperation
            objDataOperation.BindTransaction()
        Else
            objDataOperation = xDataOp
        End If
        objDataOperation.ClearParameters()


        Try

            If mintTrainingRequestunkid > 0 Then
                If Update(objDataOperation) = False Then
                    'exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                    'Throw exForce
                    If xDataOp Is Nothing Then objDataOperation.ReleaseTransaction(False)
                    Return False
                End If

                If mlstFinancingSourceVoid IsNot Nothing AndAlso mlstFinancingSourceVoid.Count > 0 Then
                    Dim objFinancingSource As New clstraining_request_financing_sources_tran
                    If objFinancingSource.VoidAll(mlstFinancingSourceVoid, objDataOperation) = False Then
                        Return False
                    End If
                End If

                'If mlstTrainingCostItemVoid IsNot Nothing AndAlso mlstTrainingCostItemVoid.Count > 0 Then
                '    Dim objTrainingCostItem As New clstraining_request_cost_tran
                '    If objTrainingCostItem.VoidAll(mlstTrainingCostItemVoid, objDataOperation) = False Then
                '        Return False
                '    End If
                'End If

            Else
                If Insert(objDataOperation) = False Then
                    'exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                    'Throw exForce
                    If xDataOp Is Nothing Then objDataOperation.ReleaseTransaction(False)
                    Return False
                End If
            End If


            If mlstFinancingSourceNew IsNot Nothing AndAlso mlstFinancingSourceNew.Count > 0 Then
                Dim objFinancingSource As New clstraining_request_financing_sources_tran
                objFinancingSource._TrainingRequestunkid = mintTrainingRequestunkid
                If objFinancingSource.SaveAll(mlstFinancingSourceNew, , objDataOperation) = False Then
                    Return False
                End If
            End If

            'If mlstTrainingCostItemNew IsNot Nothing AndAlso mlstTrainingCostItemNew.Count > 0 Then
            '    Dim objTrainingCostItem As New clstraining_request_cost_tran
            '    objTrainingCostItem._TrainingRequestunkid = mintTrainingRequestunkid
            '    If objTrainingCostItem.SaveAll(mlstTrainingCostItemNew, , objDataOperation) = False Then
            '        Return False
            '    End If
            'End If

            If mdtTrainingCostItem IsNot Nothing AndAlso mdtTrainingCostItem.Rows.Count > 0 Then
                With objCostTran
                    ._TrainingRequestunkid = mintTrainingRequestunkid
                    ._TranDataTable = mdtTrainingCostItem
                    ._IsWeb = mblnIsWeb
                    ._Userunkid = mintUserunkid
                    ._LoginEmployeeunkid = mintLoginEmployeeunkid
                    ._ClientIP = mstrClientIP
                    ._FormName = mstrFormName
                    ._HostName = mstrHostName
                End With

                If objCostTran.Insert_Update_Delete(objDataOperation) = False Then
                    exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                    Throw exForce
                End If
            End If

            'Hemant (12 Oct 2022) -- Start
            'ENHANCEMENT(NMB) :  AC2-952 - As a user, I want to have a document attachment option on the group training request page
            If mdtAttachmentTable IsNot Nothing AndAlso mintGroupTrainingRequestunkid <= 0 AndAlso mintTrainingRequestunkid > 0 Then
                Dim objDocument As New clsScan_Attach_Documents
                Dim dtTran As DataTable = objDocument._Datatable
                Dim docLocalPath As String = ConfigParameter._Object._Document_Path & "\"

                Dim strFolderName As String = (New clsScan_Attach_Documents).GetDocFolderName("Docs", enScanAttactRefId.TRAINING_NEED_FORM).Tables(0).Rows(0)("Name").ToString
                If strFolderName IsNot Nothing Then strFolderName = strFolderName & "\"
                Dim dr As DataRow
                For Each drow As DataRow In mdtAttachmentTable.Rows
                    dr = dtTran.NewRow
                    dr("scanattachtranunkid") = drow("scanattachtranunkid")
                    dr("documentunkid") = drow("documentunkid")
                    dr("employeeunkid") = drow("employeeunkid")
                    dr("filename") = drow("filename")
                    dr("scanattachrefid") = drow("scanattachrefid")
                    dr("modulerefid") = drow("modulerefid")
                    dr("form_name") = drow("form_name")
                    dr("userunkid") = drow("userunkid")
                    dr("transactionunkid") = mintTrainingRequestunkid
                    dr("attached_date") = drow("attached_date")
                    dr("orgfilepath") = drow("localpath")
                    dr("destfilepath") = docLocalPath & strFolderName & CStr(drow.Item("filename"))
                    dr("AUD") = drow("AUD")
                    dr("userunkid") = mintUserunkid
                    dr("fileuniquename") = drow("fileuniquename")
                    dr("filepath") = drow("filepath")
                    dr("filesize") = drow("filesize_kb")
                    dr("file_data") = drow("file_data")

                    dtTran.Rows.Add(dr)
                Next
                objDocument._Datatable = dtTran
                objDocument.InsertUpdateDelete_Documents(objDataOperation)

                If objDataOperation.ErrorMessage <> "" Then
                    exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                    Throw exForce
                End If
            End If
            'Hemant (12 Oct 2022) -- End


            If mblnIsSubmitApproval = True AndAlso mblnIsSkipTrainingRequestAndApproval = False Then
                'Hemant (23 Sep 2021) -- [mblnIsSkipTrainingRequestAndApproval = False]

                'Hemant (25 Jul 2022) -- Start            
                'ENHANCEMENT(NMB) : AC2-723 - Implementation of Approver in Training request form
                'Dim objTrainingApprover As New clstraining_approver_master
                'Hemant (25 Jul 2022) -- End
                Dim intPrivilegeId As Integer

                If mblnIsForeignTravelling = True Then
                    'Hemant (29 Apr 2022) -- Start
                    'ISSUE/ENHANCEMENT : AC2-314(NMB) - NMB - As a manager, I want different approval flow (Training requests) for different training courses.
                    If mintTrainingTypeId = enTrainingTravelType.Local_Travel Then
                        intPrivilegeId = enUserPriviledge.AllowToApproveTrainingRequestLocalTravelling
                    ElseIf mintTrainingTypeId = enTrainingTravelType.Foreign_Travel Then
                        'Hemant (29 Apr 2022) -- End
                    intPrivilegeId = enUserPriviledge.AllowToApproveTrainingRequestForeignTravelling
                    End If 'Hemant (29 Apr 2022)
                Else
                    intPrivilegeId = enUserPriviledge.AllowToApproveTrainingRequestRelatedToCost
                End If

                Dim dtApprover As DataTable = Nothing
                'Hemant (25 Jul 2022) -- Start            
                'ENHANCEMENT(NMB) : AC2-723 - Implementation of Approver in Training request form
                If mintTrainingApprovalSettingID = enTrainingRequestApproval.ApproverEmpMapping Then
                    Dim objTrainingApprover As New clstrainingapprover_master_emp_map
                    Dim objTrainingApprovalMatrix As New clsTraining_Approval_Matrix
                    Dim dsApprovalMatrix As New DataSet
                    dtApprover = objTrainingApprover.GetEmployeeApprover(xDatabaseName, _
                                                                         xUserUnkid, _
                                                                         xYearUnkid, _
                                                                         xCompanyUnkid, _
                                                                         mintEmployeeunkid, _
                                                                         mintPeriodunkid, _
                                                                         mintTrainingTypeId, _
                                                                         , objDataOperation)
                    'Hemant (22 Dec 2022) -- [mintPeriodunkid]
                    Dim intPriority As Integer = -1
                    Dim intLevelId As Integer = -1
                    Dim strApprovalLevelIds As String = String.Empty
                    Dim blnAboveLevel As Boolean = False
                    Dim blnBelowLevel As Boolean = False


                    Dim dslevel As DataSet = objTrainingApprovalMatrix.getLevelByCostAmount(mdecTotalTrainingCost, mintPeriodunkid, mintTrainingTypeId, objDataOperation)
                    'Hemant (22 Dec 2022) -- [mintPeriodunkid]
                    'Hemant (04 Aug 2022) -- [mintTrainingTypeId]
                    If dslevel IsNot Nothing AndAlso dslevel.Tables(0).Rows.Count > 0 Then
                        intLevelId = CInt(dslevel.Tables(0).Rows(0).Item("levelunkid"))
                        intPriority = CInt(dslevel.Tables(0).Rows(0).Item("priority"))
                    End If

                    dsApprovalMatrix = objTrainingApprovalMatrix.GetList("List", mintPeriodunkid, " trainingtypeid = " & mintTrainingTypeId & "", "", StatusType.Open, objDataOperation)

                    For Each drApprovalMatrixLevel As DataRow In dsApprovalMatrix.Tables(0).Select("costamountfrom = 0 AND costamountto = 0 AND priority <= " & intPriority & " ")
                        strApprovalLevelIds &= "," & CStr(drApprovalMatrixLevel.Item("Levelunkid"))
                        blnAboveLevel = True
                    Next

                    strApprovalLevelIds &= "," & CStr(intLevelId.ToString)

                    For Each drApprovalMatrixLevel As DataRow In dsApprovalMatrix.Tables(0).Select("costamountfrom = 0 AND costamountto = 0 AND priority > " & intPriority & "")
                        strApprovalLevelIds &= "," & CStr(drApprovalMatrixLevel.Item("Levelunkid"))
                        blnBelowLevel = True
                    Next

                    strApprovalLevelIds = strApprovalLevelIds.Substring(1)

                    Dim drRow() As DataRow = dtApprover.Select("levelunkid in ( " & strApprovalLevelIds & ")")

                    If drRow.Length > 0 Then
                        dtApprover = drRow.CopyToDataTable
                    End If

                    Dim arrApprovalLevelId() As String = Split(strApprovalLevelIds, ",")
                    For Each intApprovalLevelId As Integer In arrApprovalLevelId
                        Dim drExitRow() As DataRow = dtApprover.Select("levelunkid = " & intApprovalLevelId & "")
                        If drExitRow.Length <= 0 Then
                            Dim objEmployee As New clsEmployee_Master
                            objEmployee._Employeeunkid(eZeeDate.convertDate(xEmployeeAsOnDate), objDataOperation) = mintEmployeeunkid
                            mstrMessage = Language.getMessage(mstrModuleName, 64, "Sorry, selected employee(s) not mapped to any approver for that cost amount:Employee Code - ") & objEmployee._Employeecode & ""
                            If xDataOp Is Nothing Then objDataOperation.ReleaseTransaction(False)
                            objEmployee = Nothing
                            Return False
                            Exit For
                        End If
                    Next

                    objTrainingApprovalMatrix = Nothing
                    objTrainingApprover = Nothing
                Else
                    Dim objTrainingApprover As New clstraining_approver_master
                    'Hemant (25 Jul 2022) -- End
                    dtApprover = objTrainingApprover.GetNextEmployeeApprovers(xDatabaseName, _
                                                                                           xCompanyUnkid, _
                                                                                           xYearUnkid, _
                                                                                           strUserAccessMode, _
                                                                                           intPrivilegeId, _
                                                                                           xEmployeeAsOnDate, _
                                                                                           xUserUnkid, _
                                                                                           mintEmployeeunkid, _
                                                                                           mdecTotalTrainingCost, _
                                                                                           mintPeriodunkid, _
                                                                                           xTrainingApproverAllocationID, _
                                                                                           mintTrainingTypeId, _
                                                                                           mintInsertFormId, _
                                                                                           objDataOperation _
                                                                                           )


                'Hemant (27 Jun 2022) -- [mintInsertFormId]
                'Hemant (29 Apr 2022) -- [mintTrainingTypeId]
                'Hemant (09 Feb 2022) -- [xTrainingApproverAllocationID]
                'Hemant (03 Dec 2021) -- [mintPeriodunkid]
                objTrainingApprover = Nothing
                End If 'Hemant (25 Jul 2022)
                Dim blnEnableVisibility As Boolean = False
                Dim intMinPriority As Integer = -1
                Dim intApproverID As Integer = -1

                If dtApprover IsNot Nothing AndAlso dtApprover.Rows.Count > 0 Then
                    For Each drRow As DataRow In dtApprover.Rows
                        objTrainingApproval._TrainingRequestunkid = mintTrainingRequestunkid
                        objTrainingApproval._Employeeunkid = mintEmployeeunkid
                        objTrainingApproval._Approvertranunkid = CInt(drRow("trapproverunkid"))
                        objTrainingApproval._Approvaldate = mdtApplication_Date
                        objTrainingApproval._Priority = CInt(drRow("priority"))
                        'Hemant (07 Mar 2022) -- Start            
                        'ISSUE/ENHANCEMENT(NMB) : Point#11. The total training costs filled from group training are not equally getting divided on the individual training request screen.         
                        'objTrainingApproval._TotalCostAmount = mdecTotalTrainingCost
                        'Hemant (07 Mar 2022) -- Start            
                        'ISSUE/ENHANCEMENT(NMB) : OLD-585 - NMB - Show consolidated total training cost and approved amount on Group training requests on Individual requests
                        'objTrainingApproval._TotalCostAmount = mdecTrainingCostEmp
                        objTrainingApproval._TotalCostAmount = mdecTotalTrainingCost
                        'Hemant (07 Mar 2022) -- End
                        'Hemant (07 Mar 2022) -- End
                        objTrainingApproval._ApprovedAmount = mdecApprovedCost
                        objTrainingApproval._Statusunkid = mintStatusunkid
                        objTrainingApproval._Userunkid = mintUserunkid
                        objTrainingApproval._ClientIP = mstrClientIP
                        objTrainingApproval._FormName = mstrFormName
                        objTrainingApproval._HostName = mstrHostName
                        objTrainingApproval._IsWeb = mblnIsWeb
                        objTrainingApproval._Mapuserunkid = CInt(drRow("mapuserunkid"))
                        objTrainingApproval._CompletedStatusunkid = 1
                        objTrainingApproval._CompletedRemark = ""

                        intMinPriority = CInt(dtApprover.Compute("MIN(priority)", "1=1"))

                        If intMinPriority = CInt(drRow("priority")) Then
                            If mintUserunkid = CInt(drRow("mapuserunkid")) Then
                                objTrainingApproval._Statusunkid = clstraining_requisition_approval_master.enApprovalStatus.Approved
                                mintMinApprovedPriority = CInt(drRow("priority"))
                                intApproverID = CInt(drRow("trapproverunkid"))
                                blnEnableVisibility = True
                            End If

                            If blnEnableVisibility = True Then
                                objTrainingApproval._VisibleId = clstraining_requisition_approval_master.enApprovalStatus.Approved
                                'Hemant (25 May 2021) -- Start
                                'ISSUE/ENHANCEMENT : OLD-393 - Training module bug fixes and enhancements
                                'Hemant (07 Mar 2022) -- Start            
                                'ISSUE/ENHANCEMENT(NMB) : Point#11. The total training costs filled from group training are not equally getting divided on the individual training request screen.         
                                'mdecApprovedCost = mdecTotalTrainingCost
                                'Hemant (07 Mar 2022) -- Start            
                                'ISSUE/ENHANCEMENT(NMB) : OLD-585 - NMB - Show consolidated total training cost and approved amount on Group training requests on Individual requests
                                'mdecApprovedCost = mdecTrainingCostEmp
                                mdecApprovedCost = mdecTotalTrainingCost
                                'Hemant (07 Mar 2022) -- End
                                'Hemant (07 Mar 2022) -- End
                                objTrainingApproval._ApprovedAmount = mdecApprovedCost
                                'Hemant (25 May 2021) -- End
                            Else
                                Dim dRow As DataRow() = dtApprover.Select("priority=" & intMinPriority & " AND mapuserunkid=" & mintUserunkid & "")
                                If dRow.Length > 0 Then
                                    objTrainingApproval._VisibleId = clstraining_requisition_approval_master.enApprovalStatus.Approved
                                    'Hemant (25 May 2021) -- Start
                                    'ISSUE/ENHANCEMENT : OLD-393 - Training module bug fixes and enhancements
                                    'Hemant (07 Mar 2022) -- Start            
                                    'ISSUE/ENHANCEMENT(NMB) : Point#11. The total training costs filled from group training are not equally getting divided on the individual training request screen.         
                                    'mdecApprovedCost = mdecTotalTrainingCost
                                    'Hemant (07 Mar 2022) -- Start            
                                    'ISSUE/ENHANCEMENT(NMB) : OLD-585 - NMB - Show consolidated total training cost and approved amount on Group training requests on Individual requests
                                    'mdecApprovedCost = mdecTrainingCostEmp
                                    mdecApprovedCost = mdecTotalTrainingCost
                                    'Hemant (07 Mar 2022) -- End
                                    'Hemant (07 Mar 2022) -- End
                                    objTrainingApproval._ApprovedAmount = mdecApprovedCost
                                    'Hemant (25 May 2021) -- End
                                Else
                                    objTrainingApproval._VisibleId = mintStatusunkid
                                End If
                            End If
                            objTrainingApproval._CompletedVisibleId = 1
                        Else

                            If blnEnableVisibility = True Then
                                Dim intNextMinPriority As Integer = CInt(dtApprover.Compute("MIN(priority)", "priority > " & intMinPriority))
                                If intNextMinPriority = CInt(drRow("priority")) Then
                                    objTrainingApproval._VisibleId = clstraining_requisition_approval_master.enApprovalStatus.SubmitForApproval
                                Else
                                    objTrainingApproval._VisibleId = -1
                                End If
                            Else
                                objTrainingApproval._VisibleId = -1
                            End If
                            objTrainingApproval._CompletedVisibleId = -1
                        End If

                        If objTrainingApproval.Insert(objDataOperation) = False Then
                            exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                            Throw exForce
                        End If
                    Next
                    'Hemant (25 May 2021) -- Start
                    'ISSUE/ENHANCEMENT : OLD-393 - Training module bug fixes and enhancements
                    If blnEnableVisibility = True Then
                        Dim intMaxPriority As Integer = CInt(dtApprover.Compute("MAX(priority)", "1=1"))
                        If intMaxPriority = intMinPriority Then
                            strQ = " UPDATE trtraining_request_master SET " & _
                              "      statusunkid = " & enTrainingRequestStatus.APPROVED & " " & _
                              "     ,approvertranunkid = " & intApproverID & " " & _
                              "     ,approvedamount = " & mdecTotalTrainingCost & " " & _
                              " WHERE isvoid=0 AND trainingrequestunkid = @trainingrequestunkid "
                            'Hemant (07 Mar 2022) -- [approvedamount = " & mdecTrainingCostEmp & " --> approvedamount = " & mdecTotalTrainingCost & "]
                            'Hemant (07 Mar 2022) -- [approvedamount = " & mdecTotalTrainingCost & " --> approvedamount = " & mdecTrainingCostEmp & "]
                            objDataOperation.ClearParameters()
                            objDataOperation.AddParameter("@trainingrequestunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintTrainingRequestunkid.ToString)

                            objDataOperation.ExecNonQuery(strQ)

                            If objDataOperation.ErrorMessage <> "" Then
                                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                                Throw exForce
                            End If

                            mintStatusunkid = enTrainingRequestStatus.APPROVED

                            If InsertAuditTrails(objDataOperation, enAuditType.EDIT) = False Then
                                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                                Throw exForce
                            End If
                        End If
                    End If
                    dtApprover.Rows.Clear()
                    'Hemant (25 May 2021) -- End
                Else
                    mstrMessage = Language.getMessage(mstrModuleName, 14, "Sorry, No approver is available for Selected Employee. Please assign approver for approval")
                    If xDataOp Is Nothing Then objDataOperation.ReleaseTransaction(False)
                    Return False
                End If
            End If

            If xDataOp Is Nothing Then objDataOperation.ReleaseTransaction(True)

            Return True

        Catch ex As Exception
            If xDataOp Is Nothing Then objDataOperation.ReleaseTransaction(False)
            Throw New Exception(ex.Message & "; Procedure Name: Save; Module Name: " & mstrModuleName)
        Finally
            objTrainingApproval = Nothing
            objCostTran = Nothing
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            If xDataOp Is Nothing Then objDataOperation = Nothing
        End Try
    End Function

    'Hemant (07 Jun 2021) -- Start
    'ENHANCEMENT : OLD-406 - Global Training Requests
    Public Function InsertAllByEmployeeList(ByVal xDatabaseName As String, _
                                            ByVal xUserUnkid As Integer, _
                                            ByVal xYearUnkid As Integer, _
                                            ByVal xCompanyUnkid As Integer, _
                                            ByVal strUserAccessMode As String, _
                                            ByVal xEmployeeAsOnDate As String, _
                                            ByVal strEmployeeList As String, _
                                            ByVal xTrainingApproverAllocationID As Integer, _
                                            ByVal xTrainingTypeId As Integer, _
                                            Optional ByVal xDataOp As clsDataOperation = Nothing, _
                                            Optional ByVal mdtAttachmentTable As DataTable = Nothing) As Boolean
        'Hemant (12 Oct 2022) -- [mdtAttachmentTable]
        'Hemant (29 Apr 2022) -- [xTrainingTypeId]
        'Hemant (09 Feb 2022) -- [xTrainingApproverAllocationID]
        Dim arrEmp As String()
        Dim exForce As Exception
        Dim blnFlag As Boolean
        If xDataOp IsNot Nothing Then
            objDataOperation = xDataOp
        Else
            objDataOperation = New clsDataOperation
            objDataOperation.BindTransaction()
        End If

        Try
            arrEmp = strEmployeeList.Split(",")
            For i As Integer = 0 To arrEmp.Length - 1
                mintEmployeeunkid = CInt(arrEmp(i))
                mintTrainingRequestunkid = 0
                objDataOperation.ClearParameters()
                blnFlag = Save(xDatabaseName, xUserUnkid, xYearUnkid, xCompanyUnkid, strUserAccessMode, xEmployeeAsOnDate, xTrainingApproverAllocationID, objDataOperation)
                'Hemant (09 Feb 2022) -- [xTrainingApproverAllocationID]
                If blnFlag = False Then
                    Exit For
                End If
            Next
            If blnFlag = False Then
                If xDataOp Is Nothing Then objDataOperation.ReleaseTransaction(False)
                Return False
            Else
                'Hemant (12 Oct 2022) -- Start
                'ENHANCEMENT(NMB) :  AC2-952 - As a user, I want to have a document attachment option on the group training request page
                If mdtAttachmentTable IsNot Nothing AndAlso mintGroupTrainingRequestunkid > 0 Then
                    Dim objDocument As New clsScan_Attach_Documents
                    Dim dtTran As DataTable = objDocument._Datatable
                    Dim docLocalPath As String = ConfigParameter._Object._Document_Path & "\"

                    Dim strFolderName As String = (New clsScan_Attach_Documents).GetDocFolderName("Docs", enScanAttactRefId.GROUP_TRAINING_REQUEST).Tables(0).Rows(0)("Name").ToString

                    If strFolderName IsNot Nothing Then strFolderName = strFolderName & "\"
                    Dim dr As DataRow
                    For Each drow As DataRow In mdtAttachmentTable.Rows
                        dr = dtTran.NewRow
                        dr("scanattachtranunkid") = drow("scanattachtranunkid")
                        dr("documentunkid") = drow("documentunkid")
                        dr("employeeunkid") = drow("employeeunkid")
                        dr("filename") = drow("filename")
                        dr("scanattachrefid") = drow("scanattachrefid")
                        dr("modulerefid") = drow("modulerefid")
                        dr("form_name") = drow("form_name")
                        dr("userunkid") = drow("userunkid")
                        dr("transactionunkid") = mintGroupTrainingRequestunkid
                        dr("attached_date") = drow("attached_date")
                        dr("orgfilepath") = drow("localpath")
                        dr("destfilepath") = docLocalPath & strFolderName & CStr(drow.Item("filename"))
                        dr("AUD") = drow("AUD")
                        dr("userunkid") = mintUserunkid
                        dr("fileuniquename") = drow("fileuniquename")
                        dr("filepath") = drow("filepath")
                        dr("filesize") = drow("filesize_kb")
                        dr("file_data") = drow("file_data")

                        dtTran.Rows.Add(dr)
                    Next
                    objDocument._Datatable = dtTran
                    objDocument.InsertUpdateDelete_Documents(objDataOperation)

                    If objDataOperation.ErrorMessage <> "" Then
                        exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                        Throw exForce
                    End If
                End If
                'Hemant (12 Oct 2022) -- End

            If xDataOp Is Nothing Then objDataOperation.ReleaseTransaction(True)
            End If

            Return True
        Catch ex As Exception
            If xDataOp Is Nothing Then objDataOperation.ReleaseTransaction(False)
            Throw New Exception(ex.Message & "; Procedure Name: InsertAllByEmployeeList; Module Name: " & mstrModuleName)
            Return False
        Finally
            exForce = Nothing
            If xDataOp Is Nothing Then objDataOperation = Nothing
        End Try

    End Function
    'Hemant (07 Jun 2021) -- End


    ''' <summary>
    ''' Modify By: Hemant
    ''' </summary>
    ''' <returns>Boolean</returns>
    ''' <purpose> Delete Database Table (trtraining_request_master) </purpose>
    Public Function Delete(ByVal intUnkid As Integer) As Boolean
        'If isUsed(intUnkid) Then
        '    mstrMessage = "<Message>"
        '    Return False
        'End If

        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        Dim objCostTran As New clstraining_request_cost_tran
        Dim objFinancingSource As New clstraining_request_financing_sources_tran

        Dim objDataOperation As clsDataOperation
        objDataOperation = New clsDataOperation
        objDataOperation.BindTransaction()


        Try
            With objCostTran
                ._Isvoid = mblnIsvoid
                ._Voiduserunkid = mintVoiduserunkid
                ._VoidLoginEmployeeunkid = mintVoidLoginEmployeeunkid
                ._Voiddatetime = mdtVoiddatetime
                ._Voidreason = mstrVoidreason
                ._IsWeb = mblnIsWeb
                ._Userunkid = mintUserunkid
                ._LoginEmployeeunkid = mintLoginEmployeeunkid
                ._ClientIP = mstrClientIP
                ._FormName = mstrFormName
                ._HostName = mstrHostName
                .DeleteByTrainingRequestUnkid(intUnkid, objDataOperation)
            End With

            With objFinancingSource
                ._Isvoid = mblnIsvoid
                ._Voiduserunkid = mintVoiduserunkid
                ._Voidloginemployeeunkid = mintVoidLoginEmployeeunkid
                ._Voiddatetime = mdtVoiddatetime
                ._Voidreason = mstrVoidreason
                ._Isweb = mblnIsWeb
                ._Userunkid = mintUserunkid
                ._Loginemployeeunkid = mintLoginEmployeeunkid
                ._ClientIP = mstrClientIP
                ._FormName = mstrFormName
                ._HostName = mstrHostName
                .VoidByMasterUnkID(intUnkid, 3, objDataOperation)
            End With


            strQ = "UPDATE trtraining_request_master SET " & _
                    " isvoid = @isvoid" & _
                    ", voiduserunkid = @voiduserunkid" & _
                    ", voiddatetime = @voiddatetime" & _
                    ", voidreason = @voidreason " & _
                   "WHERE trainingrequestunkid = @trainingrequestunkid "

            objDataOperation.ClearParameters()
            objDataOperation.AddParameter("@trainingrequestunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intUnkid)
            objDataOperation.AddParameter("@isvoid", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsvoid.ToString)
            objDataOperation.AddParameter("@voiduserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintVoiduserunkid.ToString)
            objDataOperation.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, mdtVoiddatetime)
            objDataOperation.AddParameter("@voidreason", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrVoidreason.ToString)

            _TrainingRequestunkid = intUnkid

            Call objDataOperation.ExecNonQuery(strQ)

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            If InsertAuditTrails(objDataOperation, enAuditType.DELETE) = False Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            objDataOperation.ReleaseTransaction(True)

            Return True
        Catch ex As Exception
            objDataOperation.ReleaseTransaction(False)
            Throw New Exception(ex.Message & "; Procedure Name: Update; Module Name: " & mstrModuleName)
            Return False
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
    End Function

    ''' <summary>
    ''' Modify By: Hemant
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>
    Public Function isUsed(ByVal intUnkid As Integer) As Boolean
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        Dim objDataOperation As clsDataOperation
        objDataOperation = New clsDataOperation

        Try
            strQ = "<Query>"

            objDataOperation.AddParameter("@trainingrequestunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intUnkid)

            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            Return dsList.Tables(0).Rows.Count > 0
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: isUsed; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
    End Function

    ''' <summary>
    ''' Modify By: Hemant
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>
    Public Function isExist(ByVal iEmployeeId As Integer, ByVal iCourseMasterId As String, ByVal iPeriodId As Integer, Optional ByVal intUnkid As Integer = -1, _
                            Optional ByVal xDataOpr As clsDataOperation = Nothing) As Boolean
        'Hemant (03 Feb 2023) -- [iPeriodId]
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        If xDataOpr Is Nothing Then
            objDataOperation = New clsDataOperation
        Else
            objDataOperation = xDataOpr
        End If
        objDataOperation.ClearParameters()

        Try
            strQ = "SELECT " & _
                       "  trainingrequestunkid " & _
                       ", application_date " & _
                       ", employeeunkid " & _
                       ", coursemasterunkid " & _
                       ", isscheduled " & _
                       ", start_date " & _
                       ", end_date " & _
                       ", provider_name " & _
                       ", provider_address " & _
                       ", fundingsourceunkid " & _
                       ", totaltrainingcost " & _
                       ", approvedamount " & _
                       ", approvertranunkid " & _
                       ", isalignedcurrentrole " & _
                       ", ispartofpdp " & _
                       ", isforeigntravelling " & _
                       ", expectedreturn " & _
                       ", remarks " & _
                       ", statusunkid " & _
                       ", issubmit_approval " & _
                       ", userunkid " & _
                       ", loginemployeeunkid " & _
                       ", isvoid " & _
                       ", voiduserunkid " & _
                       ", voidloginemployeeunkid " & _
                       ", voiddatetime " & _
                       ", voidreason " & _
                       ", qualificationgroupunkid " & _
                       ", qualificationunkid " & _
                       ", resultunkid " & _
                       ", gpacode " & _
                       ", isqualificaionupdated " & _
                       ", iscompleted_submit_approval " & _
                       ", completed_statusunkid " & _
                       ", periodunkid " & _
                       ", other_qualificationgrp " & _
                       ", other_qualification " & _
                       ", other_resultcode " & _
                       ", completed_userunkid " & _
                       ", completed_remark " & _
                       ", enrollment_remark " & _
                       ", completed_approval_date " & _
                       ", isdaysafterfeedback_submitted  " & _
                       ", daysafter_submitted_date " & _
                       ", daysafter_submitted_remark " & _
                       ", ispretrainingfeedback_submitted  " & _
                       ", pretraining_submitted_date " & _
                       ", isposttrainingfeedback_submitted  " & _
                       ", posttraining_submitted_date " & _
                       ", isdaysafter_linemanager_submitted " & _
                       ", daysafter_linemanager_submitted_date " & _
                       ", isskip_trainingrequest_and_approval " & _
                       ", trainingcostemp " & _
                       ", approvedamountemp " & _
                       ", ISNULL(trainingtypeid, 0) AS trainingtypeid " & _
                       ", ISNULL(approvalsettingid, 0) AS approvalsettingid " & _
                       ", ISNULL(grouptrainingrequestunkid, 0) AS grouptrainingrequestunkid " & _
                       ", ISNULL(venue, '') AS venue " & _
                    "FROM trtraining_request_master " & _
                    "WHERE isvoid = 0 " & _
                    " AND coursemasterunkid = @coursemasterunkid " & _
                    " AND employeeunkid  = @employeeunkid " & _
                    " AND periodunkid  = @periodunkid "
            'Hemant (03 Feb 2023) -- [AND periodunkid  = @periodunkid]
            'Hemant (10 Nov 2022) -- [venue]
            'Hemant (12 Oct 2022) -- [grouptrainingrequestunkid]
            'Hemant (25 Jul 2022) -- [approvalsettingid]
            'Hemant (29 Apr 2022) -- [trainingtypeid]
            'Hemant (09 Feb 2022) -- [trainingcostemp,approvedamountemp]
            'Hemant (23 Sep 2021) -- [isskip_trainingrequest_and_approval]
            'Hemant (01 Sep 2021) -- [ispretrainingfeedback_submitted,pretraining_submitted_date,isposttrainingfeedback_submitted,posttraining_submitted_date,isdaysafter_linemanager_submitted,daysafter_linemanager_submitted_date]
            'Hemant (20 Aug 2021) -- [isdaysafterfeedback_submitted,daysafter_submitted_date,daysafter_submitted_remark]
            'Hemant (28 Jul 2021) -- [completed_approval_date]
            'Hemant (25 May 2021) -- [completed_remark,enrollment_remark]

            If intUnkid > 0 Then
                strQ &= " AND trainingrequestunkid <> @trainingrequestunkid"
            End If

            'Hemant (27 Jun 2022) -- Start            
            'ISSUE/ENHANCEMENT(NMB) : AC2-649 - As a user, I want the system to allow employees to apply for trainings that have been previously rejected
            strQ &= " AND statusunkid <> " & CInt(enTrainingRequestStatus.REJECTED) & " "
            'Hemant (27 Jun 2022) -- End

            objDataOperation.AddParameter("@employeeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, iEmployeeId)
            objDataOperation.AddParameter("@coursemasterunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, iCourseMasterId)
            objDataOperation.AddParameter("@trainingrequestunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intUnkid)
            'Hemant (03 Feb 2023) -- Start
            'ISSUE/EHANCEMENT :  To allow same one course to same employee in multiple periods While Tranining Request
            objDataOperation.AddParameter("@periodunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, iPeriodId)
            'Hemant (03 Feb 2023) -- End 

            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            Return dsList.Tables(0).Rows.Count > 0
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: isExist; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            If xDataOpr Is Nothing Then objDataOperation = Nothing
        End Try
    End Function

    ''' <summary>
    ''' Modify By: Hemant
    ''' </summary>
    ''' <purpose> Send Approver Notification </purpose>
    ''' 
    Public Sub Send_Notification_Approver(ByVal xDatabaseName As String, _
                                          ByVal iEmployeeId As Integer, _
                                          ByVal intCurrentPriority As Integer, _
                                          ByVal enEmailType As enEmailType, _
                                          ByVal intCompanyUnkid As Integer, _
                                          ByVal strEmployeeName As String, _
                                          ByVal intCourseMasterunkid As Integer, _
                                          ByVal dtApplicationDate As Date, _
                                          ByVal intYearUnkid As Integer, _
                                          ByVal xPeriodStart As Date, _
                                          ByVal strArutiSelfServiceURL As String, _
                                          ByVal dtTrainingStartDate As Date, _
                                          ByVal dtTrainingEndDate As Date, _
                                          ByVal xUserModeSetting As String, _
                                          ByVal intInsertFormId As Integer, _
                                          ByVal dicTrainingRequest As Dictionary(Of Integer, Integer), _
                                          Optional ByVal iLoginTypeId As Integer = 0, _
                                          Optional ByVal iLoginEmployeeId As Integer = 0, _
                                          Optional ByVal iUserId As Integer = 0, _
                                          Optional ByVal blnIsSendMail As Boolean = True, _
                                          Optional ByRef lstEmailList As List(Of clsEmailCollection) = Nothing, _
                                          Optional ByVal blnSendEmailReportingTo As Boolean = False, _
                                          Optional ByVal strTrainingFinalApprovedNotificationUserIds As String = "", _
                                          Optional ByVal blnSendEmailFinalApprovedUsers As Boolean = False _
                                          )
        'Hemant (10 Nov 2022) -- [intInsertFormId,dicTrainingRequest]
        'Hemant (04 Aug 2022) -- [TrainingFinalApprovedNotificationUserIds,blnSendEmailFinalApprovedUsers]
        'Hemant (07 Mar 2022) -- [strTrainingName --> intCourseMasterunkid]
        'Hemant (09 Feb 2022) -- [dtTrainingStartDate,dtTrainingEndDate,blnSendEmailReportingTo]
        'Hemant (16 Nov 2021) -- [xDatabaseName]
        Dim objApproverTran As New clsTraining_Approver_Tran
        Dim objApprovalProcessTran As New clstrainingapproval_process_tran
        Dim objUser As New clsUserAddEdit
        'Hemant (10 Nov 2022) -- Start
        'ENHANCEMENT(NMB) :  AC2-1026 - As a user, I want the system to send out only a single email notification to approvers for group training request
        Dim objEmployee As New clsEmployee_Master
        Dim blnSendEmailApprovalUsers As Boolean = False
        'Hemant (10 Nov 2022) -- End
        Dim dtApprover As DataTable = Nothing
        'Hemant (18 May 2021) -- Start
        'ISSUE/ENHANCEMENT : Changed approval flow for Training Completion Status
        Dim objReportTo As New clsReportingToEmployee
        Dim dsUserList As New DataSet
        Dim strLink As String = String.Empty
        'Hemant (18 May 2021) -- End
        'Hemant (07 Mar 2022) -- Start            
        'ISSUE/ENHANCEMENT(NMB) : Point#11. The total training costs filled from group training are not equally getting divided on the individual training request screen.         
        Dim objCommon As New clsCommon_Master
        Dim strTrainingName As String = String.Empty
        'Hemant (07 Mar 2022) -- End
        'Hemant (04 Aug 2022) -- Start            
        'ENHANCEMENT(NMB) : AC2-766 - Notify specific users when a training request has been approved
        Dim blnFinalApproved As Boolean = False
        Dim strFinalApproverArrayIDs() As String
        'Hemant (04 Aug 2022) -- End
        Try
            'Hemant (07 Mar 2022) -- Start            
            'ISSUE/ENHANCEMENT(NMB) : Point#11. The total training costs filled from group training are not equally getting divided on the individual training request screen.         
            objCommon._Masterunkid = intCourseMasterunkid
            strTrainingName = objCommon._Name
            'Hemant (07 Mar 2022) -- End
            'Hemant (10 Nov 2022) -- Start
            'ENHANCEMENT(NMB) :  AC2-1026 - As a user, I want the system to send out only a single email notification to approvers for group training request
            blnSendEmailFinalApprovedUsers = False
            Dim strTrainingRequestUnkIds As String = String.Join(",", dicTrainingRequest.Select(Function(x) x.Key.ToString).ToArray())
            For Each key As KeyValuePair(Of Integer, Integer) In dicTrainingRequest
                mintTrainingRequestunkid = key.Key
                iEmployeeId = key.Value
                objEmployee._Employeeunkid(xPeriodStart) = iEmployeeId
                strEmployeeName = objEmployee._Employeecode & "-" & objEmployee._Firstname & " " & objEmployee._Othername & " " & objEmployee._Surname
                'Hemant (10 Nov 2022) -- End
            'Hemant (18 May 2021) -- Start
            'ISSUE/ENHANCEMENT : Changed approval flow for Training Completion Status            
            Dim objMail As New clsSendMail
            Dim strSubject As String = ""
            'Hemant (04 Aug 2022) -- Start            
            'ENHANCEMENT(NMB) : AC2-766 - Notify specific users when a training request has been approved
            If mintStatusunkid = enTrainingRequestStatus.APPROVED Then blnFinalApproved = True
            'Hemant (04 Aug 2022) -- End
            Select Case enEmailType
                Case enEmailType.Training_Approver
                    'Hemant (18 May 2021) -- End

                    'dtApprover = objApproverTran.GetApproverData(iEmployeeId).Tables(0)
                    'Hemant (25 Jul 2022) -- Start            
                    'ENHANCEMENT(NMB) : AC2-724 - Implementation of Approval in Training request Approval Form
                    objApprovalProcessTran._TrainingApprovalSettingID = mintTrainingApprovalSettingID
                    'Hemant (25 Jul 2022) -- End
                        'Hemant (10 Nov 2022) -- Start
                        'ENHANCEMENT(NMB) :  AC2-1026 - As a user, I want the system to send out only a single email notification to approvers for group training request
                        If intInsertFormId = 2 Then
                            If blnSendEmailApprovalUsers = False Then
                                dtApprover = objApprovalProcessTran.GetTrainingApprovalData(xDatabaseName, _
                                                                                            xPeriodStart, _
                                                                                            xPeriodStart, _
                                                                                            False, _
                                                                                            -1, _
                                                                                            -1, _
                                                                                            "  trtraining_request_master.trainingrequestunkid IN ( " & strTrainingRequestUnkIds & ")", _
                                                                                            objDataOperation)
                            End If

                        Else
                            'Hemant (10 Nov 2022) -- End
                    dtApprover = objApprovalProcessTran.GetTrainingApprovalData(xDatabaseName, _
                                                                                xPeriodStart, _
                                                                                xPeriodStart, _
                                                                                False, _
                                                                                iEmployeeId, _
                                                                       -1, _
                                                                       "  trtraining_request_master.trainingrequestunkid = " & mintTrainingRequestunkid, _
                                                                       objDataOperation)
                        End If
                        'Hemant (10 Nov 2022)

                    'Hemant (16 Nov 2021) -- [xDatabaseName,xPeriodStart,xPeriodStart,False]
                    'Hemant (07 Mar 2022) -- Start     
                    'ISSUE/ENHANCEMENT(NMB) : Sequence contains no elements
                    If dtApprover IsNot Nothing AndAlso dtApprover.Rows.Count > 0 Then
                        'Hemant (07 Mar 2022) -- End

                    Dim intMinPriority As Integer

                    'If mblnIsForeignTravelling = True Then
                    '    intMinPriority = dtApprover.AsEnumerable().Select(Function(x) x.Field(Of Integer)("priority")).Max()
                    'Else
                    If intCurrentPriority <= -1 Then
                        intMinPriority = dtApprover.AsEnumerable().Select(Function(x) x.Field(Of Integer)("priority")).Min()
                    Else
                        'intMinPriority = dtApprover.AsEnumerable().Where(Function(x) x.Field(Of Integer)("priority") > intCurrentPriority) _
                        '                                          .Select(Function(x) x.Field(Of Integer)("priority")).Min()
                        Dim drPriority() As DataRow = dtApprover.Select("priority > " & intCurrentPriority & "")
                        If drPriority.Length > 0 Then
                            intMinPriority = CInt(dtApprover.Compute("MIN(priority)", "priority > " & intCurrentPriority & ""))
                        Else
                            intMinPriority = -1
                        End If

                    End If
                    ' End If


                            'Hemant (10 Nov 2022) -- Start
                            'ENHANCEMENT(NMB) :  AC2-1026 - As a user, I want the system to send out only a single email notification to approvers for group training request
                            If intInsertFormId = 2 Then
                                Dim dtTable As DataTable
                                dtTable = dtApprover.DefaultView.ToTable(True, "approvertranunkid", "levelunkid", "levelname", "priority", "mapuserunkid")
                                dtApprover = Nothing
                                dtApprover = dtTable
                            End If
                            'Hemant (10 Nov 2022) -- End

                        'Hemant (04 Aug 2022) -- Start            
                        'ENHANCEMENT(NMB) : AC2-766 - Notify specific users when a training request has been approved
                        'If intMinPriority <= -1 Then Exit Sub
                        'Hemant (04 Aug 2022) -- End
                    'Hemant (18 May 2021) -- Start
                    'ISSUE/ENHANCEMENT : Changed approval flow for Training Completion Status
                    'Dim strSubject As String = ""

                    'Select Case enEmailType
                    '    Case enEmailType.Training_Approver
                    '        strSubject = Language.getMessage(mstrModuleName, 12, "Notification for Approving Training Request")
                    '    Case enEmailType.Completed_Training_Approver
                    '        strSubject = Language.getMessage(mstrModuleName, 1012, "Notification for Approving Completed Training")
                    'End Select
                    strSubject = Language.getMessage(mstrModuleName, 12, "Notification for Approving Training Request")
                    'Hemant (18 May 2021) -- End

                    'Hemant (18 May 2021) -- Start
                    'ISSUE/ENHANCEMENT : Changed approval flow for Training Completion Status
                    'Dim objMail As New clsSendMail
                    'Hemant (18 May 2021) -- End

                    For Each dtRow As DataRow In dtApprover.Select("priority = " & intMinPriority).Distinct
                        objUser._Userunkid = CInt(dtRow.Item("mapuserunkid"))

                        If objUser._Email = "" Then Continue For
                        Dim strMessage As String = ""
                        Dim strContain As String = ""

                        objMail._Subject = strSubject

                        strMessage = "<HTML> <BODY>"
                        Dim info1 As System.Globalization.TextInfo = System.Globalization.CultureInfo.InvariantCulture.TextInfo
                            
                            'Hemant (25 Jul 2022) -- Start            
                            'ENHANCEMENT(NMB) : AC2-724 - Implementation of Approval in Training request Approval Form
                            Dim strUserName As String = info1.ToTitleCase(objUser._Firstname.ToLower()) & " " & info1.ToTitleCase(objUser._Lastname.ToLower())
                            If strUserName.Trim.Length <= 0 Then strUserName = info1.ToTitleCase(objUser._Username.ToLower())
                            'strMessage &= Language.getMessage(mstrModuleName, 19, "Dear") & " " & info1.ToTitleCase(objUser._Firstname.ToLower()) & " " & info1.ToTitleCase(objUser._Lastname.ToLower()) & ", <BR><BR>"
                            strMessage &= Language.getMessage(mstrModuleName, 19, "Dear") & " " & strUserName & ", <BR><BR>"
                            'Hemant (25 Jul 2022) -- End

                        'Hemant (18 May 2021) -- Start
                        'ISSUE/ENHANCEMENT : Changed approval flow for Training Completion Status
                        'Select Case enEmailType
                        '    Case enEmailType.Training_Approver

                        '        strContain &= Language.getMessage(mstrModuleName, 24, "This is the notification for approving Training Request") & _
                        '                          Language.getMessage(mstrModuleName, 87, " with Training :") & _
                        '                          " " & Language.getMessage(mstrModuleName, 20, "#TrainingName#") & Language.getMessage(mstrModuleName, 26, " of Employee ") & _
                        '                          " " & Language.getMessage(mstrModuleName, 20, "#EmployeeName#") & _
                        '                          Language.getMessage(mstrModuleName, 27, " on application date :") & _
                        '                          " " & Language.getMessage(mstrModuleName, 20, "#ApplicationDate#") & "."

                        '    Case enEmailType.Completed_Training_Approver

                        '        strContain &= Language.getMessage(mstrModuleName, 1024, "This is the notification for approving Completed Training") & _
                        '                          Language.getMessage(mstrModuleName, 87, " with Training :") & _
                        '                          " " & Language.getMessage(mstrModuleName, 20, "#TrainingName#") & Language.getMessage(mstrModuleName, 26, " of Employee ") & _
                        '                          " " & Language.getMessage(mstrModuleName, 20, "#EmployeeName#") & _
                        '                          Language.getMessage(mstrModuleName, 27, " on application date :") & _
                        '                          " " & Language.getMessage(mstrModuleName, 20, "#ApplicationDate#") & "."
                        'End Select
                        strContain &= Language.getMessage(mstrModuleName, 24, "This is the notification for approving Training Request") & _
                                                  Language.getMessage(mstrModuleName, 25, " with Training :") & _
                                                          " " & Language.getMessage(mstrModuleName, 20, "#TrainingName#")
                                'Hemant (10 Nov 2022) -- Start
                                'ENHANCEMENT(NMB) :  AC2-1026 - As a user, I want the system to send out only a single email notification to approvers for group training request
                                If intInsertFormId <> 2 Then
                                    'Hemant (10 Nov 2022) -- End
                                    strContain &= " " & Language.getMessage(mstrModuleName, 26, " of Employee ") & _
                                                              " " & Language.getMessage(mstrModuleName, 10, "#EmployeeName#")
                                End If  'Hemant (10 Nov 2022)
                                strContain &= Language.getMessage(mstrModuleName, 27, " on application date :") & _
                                                  " " & Language.getMessage(mstrModuleName, 11, "#ApplicationDate#") & "."
                        'Hemant (18 May 2021) -- End


                        strMessage &= strContain

                            'Hemant (07 Mar 2022) -- Start            
                            'ISSUE/ENHANCEMENT(NMB) : OLD-585 - NMB - Show consolidated total training cost and approved amount on Group training requests on Individual requests
                            'strLink = strArutiSelfServiceURL & "/Training/Training_Request/wPg_TrainingRequestForm.aspx?" & HttpUtility.UrlEncode(clsCrypto.Encrypt( _
                            '                                                                                                                                      mintTrainingRequestunkid & "|" & _
                            '                                                                                                                                      intCompanyUnkid & "|" & _
                            '                                                                                                                                      iEmployeeId & "|" & _
                            '                                                                                                                                      CInt(dtRow.Item("mapuserunkid")) & "|" & _
                            '                                                                                                                                      CStr(True) & "|" & _
                            '                                                                                                                                      CInt(dtRow.Item("pendingtrainingtranunkid")) & "|" & _
                            '                                                                                                                                      CInt(dtRow.Item("approvertranunkid")) & "|" & _
                            '                                                                                                                                      CStr(True) & "|" & _
                            '                                                                                                                                      CStr(False) & "|" & _
                            '                                                                                                                                      CStr(False)))

                            strLink = strArutiSelfServiceURL & "/Training/Training_Request_Approval/wPg_TrainingRequestApprovalList.aspx?" & HttpUtility.UrlEncode(clsCrypto.Encrypt( _
                                                                                                                                                               intCompanyUnkid & "|" & _
                                                                                                                                                               CInt(dtRow.Item("mapuserunkid")) & "|" & _
                                                                                                                                                                  CInt(dtRow.Item("approvertranunkid")) ))
                            'Hemant (07 Mar 2022) -- End

                           

                        strMessage &= "<BR></BR><BR></BR>" & _
                                      Language.getMessage(mstrModuleName, 28, "Please click on the following link to Confirm/Reject Training Request.") & _
                                     "<BR></BR><a href='" & strLink & "'>" _
                                      & strLink & "</a>"

                        strMessage &= "<p><center style='padding-top:25px;font-size:10.0pt;font-family:&quot;Verdana&quot;,&quot;Sans-Serif&quot;;color:#333399;margin-left:0px;margin-right:0px;margin-top:0px;margin-bottom:10px'><b>&quot;POWERED BY ARUTI HR &amp; PAYROLL MANAGEMENT SOFTWARE.&quot;</b></center></p>"

                        strMessage &= "</BODY></HTML>"

                        Dim strEmailContent As String
                        strEmailContent = strMessage.ToString()
                        strEmailContent = strEmailContent.Replace("#TrainingName#", "<B>" & strTrainingName & "</B>")
                        strEmailContent = strEmailContent.Replace("#EmployeeName#", "<B>" & strEmployeeName & "</B>")
                        strEmailContent = strEmailContent.Replace("#ApplicationDate#", "<B>" & dtApplicationDate.ToShortDateString & "</B>")

                        objMail._Message = strEmailContent
                        objMail._ToEmail = objUser._Email
                        If iLoginTypeId <= 0 Then iLoginTypeId = enLogin_Mode.DESKTOP
                        If mstrFormName.Trim.Length > 0 Then
                            objMail._Form_Name = mstrFormName
                        End If
                        objMail._LogEmployeeUnkid = iLoginEmployeeId
                        objMail._OperationModeId = iLoginTypeId
                        objMail._UserUnkid = IIf(iUserId <= 0, User._Object._Userunkid, iUserId)
                        objMail._SenderAddress = IIf(objUser._Email = "", objUser._Email, objUser._Email)
                        objMail._ModuleRefId = clsSendMail.enAT_VIEW_TYPE.TRAININGREQUISITION_MGT

                        If iLoginTypeId = enLogin_Mode.EMP_SELF_SERVICE Then
                            Dim objEmp As New clsEmployee_Master
                            objEmp._Employeeunkid(ConfigParameter._Object._CurrentDateAndTime) = iLoginEmployeeId
                            Dim objEmailColl As New clsEmailCollection(objMail._ToEmail, objMail._Subject, objMail._Message, mstrFormName, mintLoginEmployeeunkid, _
                                                                       mstrClientIP, mstrHostName, 0, iLoginTypeId, clsSendMail.enAT_VIEW_TYPE.TRAININGREQUISITION_MGT, _
                                                                       IIf(objEmp._Email = "", objEmp._Firstname & " " & objEmp._Surname, objEmp._Email))

                            objEmailList.Add(objEmailColl)


                            objEmp = Nothing

                        Else
                            objUser._Userunkid = IIf(iUserId <= 0, User._Object._Userunkid, iUserId)
                            Dim objEmailColl As New clsEmailCollection(objMail._ToEmail, objMail._Subject, objMail._Message, mstrFormName, mintLoginEmployeeunkid, _
                                                                       mstrClientIP, mstrHostName, objUser._Userunkid, iLoginTypeId, clsSendMail.enAT_VIEW_TYPE.TRAININGREQUISITION_MGT, _
                                                                       IIf(objUser._Email = "", objUser._Firstname & " " & objUser._Lastname, objUser._Email))

                            objEmailList.Add(objEmailColl)



                        End If
                    Next

                    End If 'Hemant (07 Mar 2022)
                    'Hemant (09 Feb 2022) -- Start            
                    'OLD-550(NMB) : Group training request to shoot email notification to the reporting to detail for the employee.  
                    If blnSendEmailReportingTo = True Then
                        Dim strMessage As String = ""
                        Dim strContain As String = ""
                        Dim strToEmail As String = ""
                        Dim strReportingToName As String = ""
                        strSubject = Language.getMessage(mstrModuleName, 41, "Notification for Training Request")
                        objMail._Subject = strSubject

                        objReportTo._EmployeeUnkid(xPeriodStart) = iEmployeeId

                        Dim dt As DataTable = objReportTo._RDataTable
                        Dim DefaultReportList As List(Of DataRow) = (From p In dt.AsEnumerable() Where (CBool(p.Item("ishierarchy")) = True) Select (p)).ToList
                        If DefaultReportList.Count > 0 Then
                            strToEmail = DefaultReportList(0).Item("ReportingToEmail").ToString
                            strReportingToName = DefaultReportList(0).Item("ename").ToString
                        End If

                        strMessage = "<HTML> <BODY>"
                        Dim info1 As System.Globalization.TextInfo = System.Globalization.CultureInfo.InvariantCulture.TextInfo
                        strMessage &= Language.getMessage(mstrModuleName, 19, "Dear") & " " & info1.ToTitleCase(strReportingToName) & ", <BR><BR>"

                        strContain &= Language.getMessage(mstrModuleName, 42, "This is to inform you that your subordinate, employee") & _
                                          " " & Language.getMessage(mstrModuleName, 43, "#EmployeeName#") & _
                                          Language.getMessage(mstrModuleName, 44, " has been selected to attend ") & _
                                          " " & Language.getMessage(mstrModuleName, 45, "#TrainingName#") & Language.getMessage(mstrModuleName, 46, " starting  from ") & _
                                          " " & Language.getMessage(mstrModuleName, 47, "#StartDate#") & _
                                          Language.getMessage(mstrModuleName, 48, " to ") & _
                                          " " & Language.getMessage(mstrModuleName, 49, "#EndDate#")


                        strMessage &= strContain

                        strMessage &= "<p><center style='padding-top:25px;font-size:10.0pt;font-family:&quot;Verdana&quot;,&quot;Sans-Serif&quot;;color:#333399;margin-left:0px;margin-right:0px;margin-top:0px;margin-bottom:10px'><b>&quot;POWERED BY ARUTI HR &amp; PAYROLL MANAGEMENT SOFTWARE.&quot;</b></center></p>"
                        strMessage &= "<BR>" & Language.getMessage(mstrModuleName, 50, "Regards")
                        strMessage &= "</BODY></HTML>"

                        Dim strEmailContent As String
                        strEmailContent = strMessage.ToString()
                        strEmailContent = strEmailContent.Replace("#TrainingName#", "<B>" & strTrainingName & "</B>")
                        strEmailContent = strEmailContent.Replace("#EmployeeName#", "<B>" & strEmployeeName & "</B>")
                        strEmailContent = strEmailContent.Replace("#StartDate#", "<B>" & dtTrainingStartDate.ToShortDateString & "</B>")
                        strEmailContent = strEmailContent.Replace("#EndDate#", "<B>" & dtTrainingEndDate.ToShortDateString & "</B>")

                        If strToEmail.Trim.Length > 0 Then
                            objMail._Message = strEmailContent
                            objMail._ToEmail = strToEmail
                            If iLoginTypeId <= 0 Then iLoginTypeId = enLogin_Mode.DESKTOP
                            If mstrFormName.Trim.Length > 0 Then
                                objMail._Form_Name = mstrFormName
                            End If
                            objMail._LogEmployeeUnkid = iLoginEmployeeId
                            objMail._OperationModeId = iLoginTypeId
                            objMail._UserUnkid = IIf(iUserId <= 0, User._Object._Userunkid, iUserId)
                            objMail._SenderAddress = strToEmail
                            objMail._ModuleRefId = clsSendMail.enAT_VIEW_TYPE.TRAININGREQUISITION_MGT

                            If iLoginTypeId = enLogin_Mode.EMP_SELF_SERVICE Then
                                Dim objEmp As New clsEmployee_Master
                                objEmp._Employeeunkid(ConfigParameter._Object._CurrentDateAndTime) = iLoginEmployeeId
                                Dim objEmailColl As New clsEmailCollection(objMail._ToEmail, objMail._Subject, objMail._Message, mstrFormName, mintLoginEmployeeunkid, _
                                                                           mstrClientIP, mstrHostName, 0, iLoginTypeId, clsSendMail.enAT_VIEW_TYPE.TRAININGREQUISITION_MGT, _
                                                                           IIf(objEmp._Email = "", objEmp._Firstname & " " & objEmp._Surname, objEmp._Email))

                                objEmailList.Add(objEmailColl)


                                objEmp = Nothing

                            Else
                                objUser._Userunkid = IIf(iUserId <= 0, User._Object._Userunkid, iUserId)
                                Dim objEmailColl As New clsEmailCollection(objMail._ToEmail, objMail._Subject, objMail._Message, mstrFormName, mintLoginEmployeeunkid, _
                                                                           mstrClientIP, mstrHostName, objUser._Userunkid, iLoginTypeId, clsSendMail.enAT_VIEW_TYPE.TRAININGREQUISITION_MGT, _
                                                                           IIf(objUser._Email = "", objUser._Firstname & " " & objUser._Lastname, objUser._Email))

                                objEmailList.Add(objEmailColl)



                            End If
                        End If

                    End If
                    'Hemant (09 Feb 2022) -- End
                    'Hemant (04 Aug 2022) -- Start            
                    'ENHANCEMENT(NMB) : AC2-766 - Notify specific users when a training request has been approved
                    If strTrainingFinalApprovedNotificationUserIds.Trim <> "" AndAlso blnFinalApproved = True AndAlso blnSendEmailFinalApprovedUsers = False Then
                        Dim strRequesterName As String = String.Empty
                        If mintCreateuserunkid > 0 Then
                            objUser = New clsUserAddEdit
                            objUser._Userunkid = mintCreateuserunkid
                            strRequesterName = objUser._Firstname & " " & objUser._Lastname
                            If strRequesterName.Trim.Length <= 0 Then strRequesterName = objUser._Username.ToString
                        End If

                        If mintCreateloginEmployeeunkid > 0 Then
                            Dim objEmp As New clsEmployee_Master
                            objEmp._Employeeunkid(ConfigParameter._Object._CurrentDateAndTime) = mintCreateloginEmployeeunkid
                            strRequesterName = objEmp._Firstname & " " & objEmp._Surname
                            objEmp = Nothing
                        End If
                        strFinalApproverArrayIDs = strTrainingFinalApprovedNotificationUserIds.Split(",")

                        For Each strApproverID As String In strFinalApproverArrayIDs
                            Dim strMessage As String = ""
                            Dim strContain As String = ""

                            objMail._Subject = Language.getMessage(mstrModuleName, 76, "TRAINING APPROVAL")

                            objUser = New clsUserAddEdit

                            objUser._Userunkid = CInt(strApproverID)
                            If objUser._Email.Trim = "" Then Continue For

                            strMessage = "<HTML> <BODY>"
                            Dim info1 As System.Globalization.TextInfo = System.Globalization.CultureInfo.InvariantCulture.TextInfo

                            Dim strUserName As String = info1.ToTitleCase(objUser._Firstname.ToLower()) & " " & info1.ToTitleCase(objUser._Lastname.ToLower())
                            If strUserName.Trim.Length <= 0 Then strUserName = info1.ToTitleCase(objUser._Username.ToLower())
                            strMessage &= Language.getMessage(mstrModuleName, 19, "Dear") & " " & strUserName & ", <BR><BR>"

                            strContain &= Language.getMessage(mstrModuleName, 67, "This is to inform you that") & _
                                                     " " & Language.getMessage(mstrModuleName, 66, "#TrainingName#") & _
                                                     Language.getMessage(mstrModuleName, 68, " Training requested by") & _
                                                     " " & Language.getMessage(mstrModuleName, 69, "#RequesterName#") & _
                                                     Language.getMessage(mstrModuleName, 70, ",starting on") & _
                                                     " " & Language.getMessage(mstrModuleName, 71, "#StartDate#") & _
                                                     " " & Language.getMessage(mstrModuleName, 72, "and ending on") & _
                                                     " " & Language.getMessage(mstrModuleName, 73, "#EndDate#") & _
                                                     " " & Language.getMessage(mstrModuleName, 74, "has been approved. For more details please login in ARUTI and generate the necessary report")


                            strMessage &= strContain

                            strMessage &= "<p><center style='padding-top:25px;font-size:10.0pt;font-family:&quot;Verdana&quot;,&quot;Sans-Serif&quot;;color:#333399;margin-left:0px;margin-right:0px;margin-top:0px;margin-bottom:10px'><b>&quot;POWERED BY ARUTI HR &amp; PAYROLL MANAGEMENT SOFTWARE.&quot;</b></center></p>"
                            strMessage &= "<BR>" & Language.getMessage(mstrModuleName, 75, "Regards")
                            strMessage &= "</BODY></HTML>"

                            Dim strEmailContent As String
                            strEmailContent = strMessage.ToString()
                            strEmailContent = strEmailContent.Replace("#TrainingName#", "<B>" & strTrainingName & "</B>")
                            strEmailContent = strEmailContent.Replace("#RequesterName#", "<B>" & strRequesterName & "</B>")
                            strEmailContent = strEmailContent.Replace("#StartDate#", "<B>" & dtTrainingStartDate.ToShortDateString & "</B>")
                            strEmailContent = strEmailContent.Replace("#EndDate#", "<B>" & dtTrainingEndDate.ToShortDateString & "</B>")

                            objMail._Message = strEmailContent
                            objMail._ToEmail = objUser._Email
                            If iLoginTypeId <= 0 Then iLoginTypeId = enLogin_Mode.DESKTOP
                            If mstrFormName.Trim.Length > 0 Then
                                objMail._Form_Name = mstrFormName
                            End If
                            objMail._LogEmployeeUnkid = iLoginEmployeeId
                            objMail._OperationModeId = iLoginTypeId
                            objMail._UserUnkid = IIf(iUserId <= 0, User._Object._Userunkid, iUserId)
                            objMail._SenderAddress = IIf(objUser._Email = "", objUser._Email, objUser._Email)
                            objMail._ModuleRefId = clsSendMail.enAT_VIEW_TYPE.TRAININGREQUISITION_MGT

                            If iLoginTypeId = enLogin_Mode.EMP_SELF_SERVICE Then
                                Dim objEmp As New clsEmployee_Master
                                objEmp._Employeeunkid(ConfigParameter._Object._CurrentDateAndTime) = iLoginEmployeeId
                                Dim objEmailColl As New clsEmailCollection(objMail._ToEmail, objMail._Subject, objMail._Message, mstrFormName, mintLoginEmployeeunkid, _
                                                                           mstrClientIP, mstrHostName, 0, iLoginTypeId, clsSendMail.enAT_VIEW_TYPE.TRAININGREQUISITION_MGT, _
                                                                           IIf(objEmp._Email = "", objEmp._Firstname & " " & objEmp._Surname, objEmp._Email))

                                objEmailList.Add(objEmailColl)


                                objEmp = Nothing

                            Else
                                objUser._Userunkid = IIf(iUserId <= 0, User._Object._Userunkid, iUserId)
                                Dim objEmailColl As New clsEmailCollection(objMail._ToEmail, objMail._Subject, objMail._Message, mstrFormName, mintLoginEmployeeunkid, _
                                                                           mstrClientIP, mstrHostName, objUser._Userunkid, iLoginTypeId, clsSendMail.enAT_VIEW_TYPE.TRAININGREQUISITION_MGT, _
                                                                           IIf(objUser._Email = "", objUser._Firstname & " " & objUser._Lastname, objUser._Email))

                                objEmailList.Add(objEmailColl)



                            End If
                        Next

                    End If
                    'Hemant (04 Aug 2022) -- End
                Case enEmailType.Completed_Training_Approver
                    Dim strMessage As String = ""
                    Dim strContain As String = ""
                    Dim strToEmail As String = ""
                    Dim strReportingToName As String = ""
                    'Hemant (07 Mar 2022) -- Start            
                    'ISSUE/ENHANCEMENT(NMB) : Point#10. After marking training as complete, the email sent to employee’s reporting to is inaccurate and mis leading
                    'strSubject = Language.getMessage(mstrModuleName, 16, "Notification for Approving Completed Training")
                    strSubject = Language.getMessage(mstrModuleName, 58, "Notification for Completed Training")
                    'Hemant (07 Mar 2022) -- End
                    objMail._Subject = strSubject

                    objReportTo._EmployeeUnkid(xPeriodStart) = iEmployeeId

                    Dim dt As DataTable = objReportTo._RDataTable
                    Dim DefaultReportList As List(Of DataRow) = (From p In dt.AsEnumerable() Where (CBool(p.Item("ishierarchy")) = True) Select (p)).ToList
                    If DefaultReportList.Count > 0 Then
                        strToEmail = DefaultReportList(0).Item("ReportingToEmail").ToString
                        'Hemant (07 Mar 2022) -- Start            
                        'ISSUE/ENHANCEMENT(NMB) : Point#10. After marking training as complete, the email sent to employee’s reporting to is inaccurate and mis leading
                        'strReportingToName = DefaultReportList(0).Item("ename").ToString
                        strReportingToName = DefaultReportList(0).Item("fename").ToString & " " & DefaultReportList(0).Item("lename").ToString
                        'Hemant (07 Mar 2022) -- End
                    End If

                    strMessage = "<HTML> <BODY>"
                    Dim info1 As System.Globalization.TextInfo = System.Globalization.CultureInfo.InvariantCulture.TextInfo
                    strMessage &= Language.getMessage(mstrModuleName, 19, "Dear") & " " & info1.ToTitleCase(strReportingToName) & ", <BR><BR>"

                    'Hemant (07 Mar 2022) -- Start            
                    'ISSUE/ENHANCEMENT(NMB) : Point#10. After marking training as complete, the email sent to employee’s reporting to is inaccurate and mis leading
                    'strContain &= Language.getMessage(mstrModuleName, 17, "This is the notification for approving Completed Training") & _
                    '                  Language.getMessage(mstrModuleName, 25, " with Training :") & _
                    '                  " " & Language.getMessage(mstrModuleName, 20, "#TrainingName#") & Language.getMessage(mstrModuleName, 26, " of Employee ") & _
                    '                  " " & Language.getMessage(mstrModuleName, 10, "#EmployeeName#") & _
                    '                  Language.getMessage(mstrModuleName, 27, " on application date :") & _
                    '                  " " & Language.getMessage(mstrModuleName, 11, "#ApplicationDate#") & "."
                    strContain &= Language.getMessage(mstrModuleName, 60, "This is to notify you that,") & _
                                   "<B>" & Language.getMessage(mstrModuleName, 61, "Employee") & "</B>" & " " & Language.getMessage(mstrModuleName, 62, "#EmployeeName#") & _
                                      Language.getMessage(mstrModuleName, 63, " has completed training on") & _
                                      " " & Language.getMessage(mstrModuleName, 59, "#TrainingName#") & "."
                    'Hemant (07 Mar 2022) -- End


                    strMessage &= strContain

                    strMessage &= "<p><center style='padding-top:25px;font-size:10.0pt;font-family:&quot;Verdana&quot;,&quot;Sans-Serif&quot;;color:#333399;margin-left:0px;margin-right:0px;margin-top:0px;margin-bottom:10px'><b>&quot;POWERED BY ARUTI HR &amp; PAYROLL MANAGEMENT SOFTWARE.&quot;</b></center></p>"
                    strMessage &= "<BR>" & Language.getMessage(mstrModuleName, 65, "Regards")
                    strMessage &= "</BODY></HTML>"

                    Dim strEmailContent As String
                    strEmailContent = strMessage.ToString()
                    strEmailContent = strEmailContent.Replace("#TrainingName#", "<B>" & strTrainingName & "</B>")
                    strEmailContent = strEmailContent.Replace("#EmployeeName#", "<B>" & strEmployeeName & "</B>")
                    'Hemant (07 Mar 2022) -- Start            
                    'ISSUE/ENHANCEMENT(NMB) : Point#10. After marking training as complete, the email sent to employee’s reporting to is inaccurate and mis leading
                    'strEmailContent = strEmailContent.Replace("#ApplicationDate#", "<B>" & dtApplicationDate.ToShortDateString & "</B>")
                    'Hemant (07 Mar 2022) -- End

                    If strToEmail.Trim.Length > 0 Then
                        objMail._Message = strEmailContent
                        objMail._ToEmail = strToEmail
                        If iLoginTypeId <= 0 Then iLoginTypeId = enLogin_Mode.DESKTOP
                        If mstrFormName.Trim.Length > 0 Then
                            objMail._Form_Name = mstrFormName
                        End If
                        objMail._LogEmployeeUnkid = iLoginEmployeeId
                        objMail._OperationModeId = iLoginTypeId
                        objMail._UserUnkid = IIf(iUserId <= 0, User._Object._Userunkid, iUserId)
                        objMail._SenderAddress = strToEmail
                        objMail._ModuleRefId = clsSendMail.enAT_VIEW_TYPE.TRAININGREQUISITION_MGT

                        If iLoginTypeId = enLogin_Mode.EMP_SELF_SERVICE Then
                            Dim objEmp As New clsEmployee_Master
                            objEmp._Employeeunkid(ConfigParameter._Object._CurrentDateAndTime) = iLoginEmployeeId
                            Dim objEmailColl As New clsEmailCollection(objMail._ToEmail, objMail._Subject, objMail._Message, mstrFormName, mintLoginEmployeeunkid, _
                                                                       mstrClientIP, mstrHostName, 0, iLoginTypeId, clsSendMail.enAT_VIEW_TYPE.TRAININGREQUISITION_MGT, _
                                                                       IIf(objEmp._Email = "", objEmp._Firstname & " " & objEmp._Surname, objEmp._Email))

                            objEmailList.Add(objEmailColl)


                            objEmp = Nothing

                        Else
                            objUser._Userunkid = IIf(iUserId <= 0, User._Object._Userunkid, iUserId)
                            Dim objEmailColl As New clsEmailCollection(objMail._ToEmail, objMail._Subject, objMail._Message, mstrFormName, mintLoginEmployeeunkid, _
                                                                       mstrClientIP, mstrHostName, objUser._Userunkid, iLoginTypeId, clsSendMail.enAT_VIEW_TYPE.TRAININGREQUISITION_MGT, _
                                                                       IIf(objUser._Email = "", objUser._Firstname & " " & objUser._Lastname, objUser._Email))

                            objEmailList.Add(objEmailColl)



                        End If
                    End If

                    'Hemant (03 Oct 2022) -- Start
                    'ISSUE(NMB) : implement to have it sent to only approvers based on access while sending Notification for Completion Tranining
                    'dsUserList = objUser.Get_UserBy_PrivilegeId(enUserPriviledge.AllowToApproveRejectTrainingCompletion, intYearUnkid)
                    dsUserList = objUser.Get_UserBy_EmployeeAndPrivilegeId(enUserPriviledge.AllowToApproveRejectTrainingCompletion, iEmployeeId, intCompanyUnkid, xPeriodStart, xUserModeSetting, intYearUnkid)
                    'Hemant (03 Oct 2022) -- End

                    For Each drUser As DataRow In dsUserList.Tables(0).Rows
                        objUser._Userunkid = CInt(drUser.Item("UId"))
                        If drUser("Uemail").ToString().Trim().Length <= 0 Then Continue For

                        strMessage = ""
                        strContain = ""
                        strEmailContent = ""

                        strMessage = "<HTML> <BODY>"

                        strMessage &= Language.getMessage(mstrModuleName, 19, "Dear") & " " & info1.ToTitleCase(objUser._Firstname.ToLower()) & " " & info1.ToTitleCase(objUser._Lastname.ToLower()) & ", <BR><BR>"

                        strContain &= Language.getMessage(mstrModuleName, 17, "This is the notification for approving Completed Training") & _
                                          Language.getMessage(mstrModuleName, 25, " with Training :") & _
                                          " " & Language.getMessage(mstrModuleName, 20, "#TrainingName#") & Language.getMessage(mstrModuleName, 26, " of Employee ") & _
                                          " " & Language.getMessage(mstrModuleName, 10, "#EmployeeName#") & _
                                          Language.getMessage(mstrModuleName, 27, " on application date :") & _
                                          " " & Language.getMessage(mstrModuleName, 11, "#ApplicationDate#") & "."

                        strMessage &= strContain

                        strLink = strArutiSelfServiceURL & "/Training/Training_Request/wPg_TrainingRequestForm.aspx?" & HttpUtility.UrlEncode(clsCrypto.Encrypt( _
                                                                                                                                                               mintTrainingRequestunkid & "|" & _
                                                                                                                                                               intCompanyUnkid & "|" & _
                                                                                                                                                               iEmployeeId & "|" & _
                                                                                                                                                               CInt(drUser.Item("UId")) & "|" & _
                                                                                                                                                               CStr(True) & "|" & _
                                                                                                                                                               CStr(0) & "|" & _
                                                                                                                                                               CStr(0) & "|" & _
                                                                                                                                                               CStr(False) & "|" & _
                                                                                                                                                               CStr(True) & "|" & _
                                                                                                                                                               CStr(False)))

                        strMessage &= "<BR></BR><BR></BR>" & _
                                      Language.getMessage(mstrModuleName, 32, "Please click on the following link to Confirm/Reject Completion Training.") & _
                                     "<BR></BR><a href='" & strLink & "'>" _
                                      & strLink & "</a>"

                        strMessage &= "<p><center style='padding-top:25px;font-size:10.0pt;font-family:&quot;Verdana&quot;,&quot;Sans-Serif&quot;;color:#333399;margin-left:0px;margin-right:0px;margin-top:0px;margin-bottom:10px'><b>&quot;POWERED BY ARUTI HR &amp; PAYROLL MANAGEMENT SOFTWARE.&quot;</b></center></p>"

                        strMessage &= "</BODY></HTML>"

                        strEmailContent = strMessage.ToString()
                        strEmailContent = strEmailContent.Replace("#TrainingName#", "<B>" & strTrainingName & "</B>")
                        strEmailContent = strEmailContent.Replace("#EmployeeName#", "<B>" & strEmployeeName & "</B>")
                        strEmailContent = strEmailContent.Replace("#ApplicationDate#", "<B>" & dtApplicationDate.ToShortDateString & "</B>")

                        objMail._Message = strEmailContent
                        objMail._ToEmail = objUser._Email
                        If iLoginTypeId <= 0 Then iLoginTypeId = enLogin_Mode.DESKTOP
                        If mstrFormName.Trim.Length > 0 Then
                            objMail._Form_Name = mstrFormName
                        End If
                        objMail._LogEmployeeUnkid = iLoginEmployeeId
                        objMail._OperationModeId = iLoginTypeId
                        objMail._UserUnkid = IIf(iUserId <= 0, User._Object._Userunkid, iUserId)
                        objMail._SenderAddress = IIf(objUser._Email = "", objUser._Email, objUser._Email)
                        objMail._ModuleRefId = clsSendMail.enAT_VIEW_TYPE.TRAININGREQUISITION_MGT

                        If iLoginTypeId = enLogin_Mode.EMP_SELF_SERVICE Then
                            Dim objEmp As New clsEmployee_Master
                            objEmp._Employeeunkid(ConfigParameter._Object._CurrentDateAndTime) = iLoginEmployeeId
                            Dim objEmailColl As New clsEmailCollection(objMail._ToEmail, objMail._Subject, objMail._Message, mstrFormName, mintLoginEmployeeunkid, _
                                                                       mstrClientIP, mstrHostName, 0, iLoginTypeId, clsSendMail.enAT_VIEW_TYPE.TRAININGREQUISITION_MGT, _
                                                                       IIf(objEmp._Email = "", objEmp._Firstname & " " & objEmp._Surname, objEmp._Email))

                            objEmailList.Add(objEmailColl)


                            objEmp = Nothing

                        Else
                            objUser._Userunkid = IIf(iUserId <= 0, User._Object._Userunkid, iUserId)
                            Dim objEmailColl As New clsEmailCollection(objMail._ToEmail, objMail._Subject, objMail._Message, mstrFormName, mintLoginEmployeeunkid, _
                                                                       mstrClientIP, mstrHostName, objUser._Userunkid, iLoginTypeId, clsSendMail.enAT_VIEW_TYPE.TRAININGREQUISITION_MGT, _
                                                                       IIf(objUser._Email = "", objUser._Firstname & " " & objUser._Lastname, objUser._Email))

                            objEmailList.Add(objEmailColl)



                        End If
                    Next
            End Select  'Hemant (18 May 2021)
                dtApprover = Nothing
                blnSendEmailFinalApprovedUsers = True
                blnSendEmailApprovalUsers = True
            Next  'Hemant (10 Nov 2022)

            If blnIsSendMail = True Then
                If objEmailList.Count > 0 Then
                    If HttpContext.Current Is Nothing Then
                        objThread = New Thread(AddressOf Send_Notification)
                        objThread.IsBackground = True

                        Dim arr(1) As Object
                        arr(0) = intCompanyUnkid
                        objThread.Start(arr)
                    Else
                        Call Send_Notification(intCompanyUnkid)
                    End If
                End If
            Else
                lstEmailList = objEmailList
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "Send_Notification_Approver", mstrModuleName)
        Finally
            objApproverTran = Nothing
            objUser = Nothing
            objApprovalProcessTran = Nothing
            objReportTo = Nothing
            'Hemant (07 Mar 2022) -- Start            
            'ISSUE/ENHANCEMENT(NMB) : Point#11. The total training costs filled from group training are not equally getting divided on the individual training request screen.         
            objCommon = Nothing
            'Hemant (07 Mar 2022) -- End
            'Hemant (10 Nov 2022) -- Start
            'ENHANCEMENT(NMB) :  AC2-1026 - As a user, I want the system to send out only a single email notification to approvers for group training request
            objEmployee = Nothing
            'Hemant (10 Nov 2022) -- End
        End Try
    End Sub

    'Hemant (07 Jun 2021) -- Start
    'ENHANCEMENT : OLD-406 - Global Training Requests
    'Hemant (10 Nov 2022) -- Start
    'ENHANCEMENT(NMB) :  AC2-1026 - As a user, I want the system to send out only a single email notification to approvers for group training request
    'Public Sub SendNotificationApproverByEmployeeList(ByVal xDatabaseName As String, _
    '                                                      ByVal dicTrainingRequest As Dictionary(Of Integer, Integer), _
    '                                                      ByVal intCurrentPriority As Integer, _
    '                                                      ByVal enEmailType As enEmailType, _
    '                                                      ByVal intCompanyUnkid As Integer, _
    '                                                      ByVal intCourseMasterunkid As Integer, _
    '                                                      ByVal dtApplicationDate As Date, _
    '                                                      ByVal intYearUnkid As Integer, _
    '                                                      ByVal xPeriodStart As Date, _
    '                                                      ByVal strArutiSelfServiceURL As String, _
    '                                                      ByVal dtTrainingStartDate As Date, _
    '                                                      ByVal dtTrainingEndDate As Date, _
    '                                                      ByVal xUserModeSetting As String, _
    '                                                      Optional ByVal iLoginTypeId As Integer = 0, _
    '                                                      Optional ByVal iLoginEmployeeId As Integer = 0, _
    '                                                      Optional ByVal iUserId As Integer = 0, _
    '                                                      Optional ByVal blnIsSendMail As Boolean = True, _
    '                                                      Optional ByRef lstEmailList As List(Of clsEmailCollection) = Nothing, _
    '                                                      Optional ByVal blnSendEmailReportingTo As Boolean = False, _
    '                                                      Optional ByVal strTrainingFinalApprovedNotificationUserIds As String = "" _
    '                                                      )
    '    'Hemant (03 Oct 2022) -- [xUserModeSetting]
    '    'Hemant (04 Aug 2022) -- [strTrainingFinalApprovedNotificationUserIds]
    '    'Hemant (07 Mar 2022) -- [strTrainingName --> intCourseMasterunkid]
    '    'Hemant (09 Feb 2022) -- [dtTrainingStartDate,dtTrainingEndDate,blnSendEmailReportingTo]
    '    'Hemant (16 Nov 2021) -- [xDatabaseName]
    '    Dim objEmp As New clsEmployee_Master

    '    Dim intEmployeeunkid As Integer
    '    Dim strEmployeeName As String = String.Empty
    '    Try
    '        'Hemant (04 Aug 2022) -- Start            
    '        'ENHANCEMENT(NMB) : AC2-766 - Notify specific users when a training request has been approved
    '        Dim blnSendEmailFinalApprovedUsers As Boolean = False
    '        'Hemant (04 Aug 2022) -- End
    '        For Each key As KeyValuePair(Of Integer, Integer) In dicTrainingRequest
    '            mintTrainingRequestunkid = key.Key
    '            intEmployeeunkid = key.Value
    '            objEmp._Employeeunkid(xPeriodStart) = intEmployeeunkid
    '            strEmployeeName = objEmp._Employeecode & "-" & objEmp._Firstname & " " & objEmp._Othername & " " & objEmp._Surname
    '            Send_Notification_Approver(xDatabaseName, intEmployeeunkid, intCurrentPriority, enEmailType, intCompanyUnkid, _
    '                                       strEmployeeName, intCourseMasterunkid, dtApplicationDate, intYearUnkid, _
    '                                       xPeriodStart, strArutiSelfServiceURL, dtTrainingStartDate, dtTrainingEndDate, xUserModeSetting, iLoginTypeId, iLoginEmployeeId, _
    '                                       iUserId, blnIsSendMail, lstEmailList, blnSendEmailReportingTo, strTrainingFinalApprovedNotificationUserIds, _
    '                                       blnSendEmailFinalApprovedUsers)
    '            'Hemant (03 Oct 2022) -- [xUserModeSetting]
    '            'Hemant (04 Aug 2022) -- [strTrainingFinalApprovedNotificationUserIds,blnSendEmailFinalApprovedUsers]
    '            'Hemant (07 Mar 2022) -- [strTrainingName --> intCourseMasterunkid]
    '            'Hemant (09 Feb 2022) -- [dtTrainingStartDate,dtTrainingEndDate,blnSendEmailReportingTo]
    '            'Hemant (16 Nov 2021) -- [xDatabaseName]
    '            'Hemant (04 Aug 2022) -- Start            
    '            'ENHANCEMENT(NMB) : AC2-766 - Notify specific users when a training request has been approved
    '            blnSendEmailFinalApprovedUsers = True
    '            'Hemant (04 Aug 2022) -- End
    '        Next


    '    Catch ex As Exception
    '        DisplayError.Show("-1", ex.Message, "SendNotificationApproverByEmployeeList", mstrModuleName)
    '    Finally
    '        objEmp = Nothing  'Hemant (09 Feb 2022)
    '    End Try
    'End Sub
    'Hemant (10 Nov 2022) -- End
    'Hemant (07 Jun 2021) -- End

    Private Sub Send_Notification(ByVal intCompanyUnkid As Object)
        Try
            If objEmailList.Count > 0 Then
                Dim objSendMail As New clsSendMail
                For Each objEmail In objEmailList
                    objSendMail._ToEmail = objEmail._EmailTo
                    objSendMail._Subject = objEmail._Subject
                    objSendMail._Message = objEmail._Message
                    objSendMail._Form_Name = objEmail._FormName
                    objSendMail._LogEmployeeUnkid = objEmail._LogEmployeeUnkid
                    objSendMail._OperationModeId = objEmail._OperationModeId
                    objSendMail._UserUnkid = objEmail._UserUnkid
                    objSendMail._SenderAddress = objEmail._SenderAddress
                    objSendMail._ModuleRefId = objEmail._ModuleRefId

                    Dim intCUnkId As Integer = 0
                    If TypeOf intCompanyUnkid Is Integer Then
                        intCUnkId = intCompanyUnkid
                    Else
                        intCUnkId = CInt(intCompanyUnkid(0))
                    End If
                    If objSendMail.SendMail(intCUnkId).ToString.Length > 0 Then
                        Continue For
                    End If
                Next
                objEmailList.Clear()
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "Send_Notification", mstrModuleName)
        End Try
    End Sub

    Public Sub Send_Notification_Employee(ByVal iEmployeeId As Integer, _
                                          ByVal enTrainingStatus As enTrainingRequestStatus, _
                                          ByVal enEmailType As enEmailType, _
                                          ByVal dtEmployeeAsOnDate As Date, _
                                          ByVal intCompanyUnkId As Integer, _
                                          ByVal intCourseMasterunkid As Integer, _
                                          ByVal dtApplicationDate As Date, _
                                          ByVal intTrainingRequestId As Integer, _
                                          ByVal strArutiSelfServiceURL As String, _
                                          Optional ByVal strRemark As String = "", _
                                          Optional ByVal iLoginTypeId As Integer = 0, _
                                          Optional ByVal iLoginEmployeeId As Integer = 0, _
                                          Optional ByVal iUserId As Integer = 0, _
                                          Optional ByVal blnIsSendMail As Boolean = True _
                                          )
        'Hemant (07 Mar 2022) -- [strTrainingName --> intCourseMasterunkid]
        'Hemant (07 Mar 2022) -- Start            
        'ISSUE/ENHANCEMENT(NMB) : Point#11. The total training costs filled from group training are not equally getting divided on the individual training request screen.         
        Dim objCommon As New clsCommon_Master
        Dim strTrainingName As String = String.Empty
        'Hemant (07 Mar 2022) -- End
        Try
            'Hemant (07 Mar 2022) -- Start            
            'ISSUE/ENHANCEMENT(NMB) : Point#11. The total training costs filled from group training are not equally getting divided on the individual training request screen.         
            objCommon._Masterunkid = intCourseMasterunkid
            strTrainingName = objCommon._Name
            'Hemant (07 Mar 2022) -- End
            Dim objEmp As New clsEmployee_Master
            Dim strLink As String = String.Empty

            objEmp._Employeeunkid(dtEmployeeAsOnDate) = iEmployeeId

            Dim objMail As New clsSendMail

            If objEmp._Email = "" Then Exit Sub
            Dim strMessage As String = ""
            Dim strSubject As String = ""

            Select Case enEmailType
                Case enEmailType.Training_Approver
                    strSubject = Language.getMessage(mstrModuleName, 34, "Training Request Status Notification")
                Case enEmailType.Completed_Training_Approver
                    strSubject = Language.getMessage(mstrModuleName, 23, " Completed Training Status Notification")
            End Select

            strMessage = "<HTML> <BODY>"
            strMessage &= Language.getMessage(mstrModuleName, 19, "Dear") & " " & objEmp._Firstname & "  " & objEmp._Surname & ", <BR><BR>"

            Select Case enEmailType
                Case enEmailType.Training_Approver
                    strMessage &= Language.getMessage(mstrModuleName, 31, "This is to inform you that the application you have applied for Training Request") & _
                                          Language.getMessage(mstrModuleName, 25, " with Training :") & _
                                          " " & Language.getMessage(mstrModuleName, 20, "#TrainingName#") & _
                                          Language.getMessage(mstrModuleName, 27, " on application date :") & _
                                          " " & Language.getMessage(mstrModuleName, 11, "#ApplicationDate#") & _
                                          Language.getMessage(mstrModuleName, 30, " has been ")
                Case enEmailType.Completed_Training_Approver
                    strMessage &= Language.getMessage(mstrModuleName, 18, "This is to inform you that the application you have applied for Completed Training ") & _
                                          Language.getMessage(mstrModuleName, 25, " with Training :") & _
                                          " " & Language.getMessage(mstrModuleName, 20, "#TrainingName#") & _
                                          Language.getMessage(mstrModuleName, 27, " on application date :") & _
                                          " " & Language.getMessage(mstrModuleName, 11, "#ApplicationDate#") & _
                                          Language.getMessage(mstrModuleName, 30, " has been ")


            End Select

            Select Case enTrainingStatus
                Case enTrainingRequestStatus.APPROVED
                    strMessage &= Language.getMessage(mstrModuleName, 2, "Approved")
                Case enTrainingRequestStatus.REJECTED
                    strMessage &= Language.getMessage(mstrModuleName, 3, "Rejected")
            End Select
            strMessage &= "."

            Select Case enTrainingStatus
                Case enTrainingRequestStatus.APPROVED
                    'Hemant (28 Jul 2021) -- Start             
                    'ENHANCEMENT : OLD-293 - Training Evaluation
                    Select Case enEmailType
                        Case enEmailType.Training_Approver
                            Dim objEvalQuestion As New clseval_question_master
                            Dim dsQuestions As DataSet
                            dsQuestions = objEvalQuestion.GetList("List", True, , clseval_group_master.enFeedBack.PRETRAINING)
                            If dsQuestions IsNot Nothing AndAlso dsQuestions.Tables(0).Rows.Count > 0 Then
                                strLink = strArutiSelfServiceURL & "/Training/Training_Evalution/wPg_Training_Evalution_Form.aspx?" & HttpUtility.UrlEncode(clsCrypto.Encrypt( _
                                                                                                                                                                   intTrainingRequestId & "|" & _
                                                                                                                                                                   intCompanyUnkId & "|" & _
                                                                                                                                                                   iEmployeeId & "|" & _
                                                                                                                                                                   CInt(clseval_group_master.enFeedBack.PRETRAINING) & "|" & _
                                                                                                                                                                   iEmployeeId & "|" & _
                                                                                                                                                                   CStr(False)))

                                strMessage &= "<BR></BR><BR></BR>" & _
                                              Language.getMessage(mstrModuleName, 21, "Please click on the following link to submit Pre-Training Evaluation Form.") & _
                                             "<BR></BR><a href='" & strLink & "'>" _
                                              & strLink & "</a>"
                            End If
                            objEvalQuestion = Nothing
                            'Hemant (28 Jul 2021) -- End
                            strLink = strArutiSelfServiceURL & "/Training/Training_Request/wPg_TrainingRequestForm.aspx?" & HttpUtility.UrlEncode(clsCrypto.Encrypt( _
                                                                                                                                                                intTrainingRequestId & "|" & _
                                                                                                                                                                intCompanyUnkId & "|" & _
                                                                                                                                                                iEmployeeId & "|" & _
                                                                                                                                                                CStr(0) & "|" & _
                                                                                                                                                                CStr(False) & "|" & _
                                                                                                                                                                CStr(0) & "|" & _
                                                                                                                                                                CStr(0) & "|" & _
                                                                                                                                                                CStr(False) & "|" & _
                                                                                                                                                                CStr(False) & "|" & _
                                                                                                                                                                CStr(True)))

                            strMessage &= "<BR></BR><BR></BR>" & _
                                              Language.getMessage(mstrModuleName, 29, "Please click on the following link to Confirm/Reject Enrollment.") & _
                                             "<BR></BR><a href='" & strLink & "'>" _
                                              & strLink & "</a>"
                            'Hemant (28 Jul 2021) -- Start             
                            'ENHANCEMENT : OLD-293 - Training Evaluation
                        Case enEmailType.Completed_Training_Approver
                            Dim objEvalQuestion As New clseval_question_master
                            Dim dsQuestions As DataSet
                            dsQuestions = objEvalQuestion.GetList("List", True, , clseval_group_master.enFeedBack.POSTTRAINING)
                            If dsQuestions IsNot Nothing AndAlso dsQuestions.Tables(0).Rows.Count > 0 Then
                                strLink = strArutiSelfServiceURL & "/Training/Training_Evalution/wPg_Training_Evalution_Form.aspx?" & HttpUtility.UrlEncode(clsCrypto.Encrypt( _
                                                                                                                                                                   intTrainingRequestId & "|" & _
                                                                                                                                                                   intCompanyUnkId & "|" & _
                                                                                                                                                                   iEmployeeId & "|" & _
                                                                                                                                                                   CInt(clseval_group_master.enFeedBack.POSTTRAINING) & "|" & _
                                                                                                                                                                   iEmployeeId & "|" & _
                                                                                                                                                                   CStr(False)))

                                strMessage &= "<BR></BR><BR></BR>" & _
                                              Language.getMessage(mstrModuleName, 22, "Please click on the following link to submit Post-Training Evaluation Form.") & _
                                     "<BR></BR><a href='" & strLink & "'>" _
                                      & strLink & "</a>"
                            End If
                            objEvalQuestion = Nothing
                    End Select
                    'Hemant (28 Jul 2021) -- End

                Case enTrainingRequestStatus.REJECTED
                    strMessage &= " " & Language.getMessage(mstrModuleName, 35, "Please refer to the comments below for the same.")
                    strMessage &= "<BR></BR><BR></BR>"
                    strMessage &= " " & Language.getMessage(mstrModuleName, 33, "Remarks:") & " " & Language.getMessage(mstrModuleName, 13, "#Remark#")
            End Select


            strMessage &= "<BR></BR><BR></BR><center>""POWERED BY ARUTI HR & PAYROLL MANAGEMENT SOFTWARE""</center>"

            strMessage &= "</BODY></HTML>"

            Dim strEmailContent As String
            strEmailContent = strMessage.ToString()
            strEmailContent = strEmailContent.Replace("#TrainingName#", "<B>" & strTrainingName & "</B>")
            strEmailContent = strEmailContent.Replace("#ApplicationDate#", "<B>" & dtApplicationDate.ToShortDateString & "</B>")
            strEmailContent = strEmailContent.Replace("#Remark#", "<B>" & strRemark & "</B>")

            objMail._Subject = strSubject
            objMail._Message = strEmailContent
            objMail._ToEmail = objEmp._Email
            If iLoginTypeId <= 0 Then iLoginTypeId = enLogin_Mode.DESKTOP
            If mstrFormName.Trim.Length > 0 Then
                objMail._Form_Name = mstrFormName
            End If
            objMail._LogEmployeeUnkid = iLoginEmployeeId
            objMail._OperationModeId = iLoginTypeId
            objMail._UserUnkid = IIf(iUserId <= 0, User._Object._Userunkid, iUserId)
            objMail._SenderAddress = objEmp._Email
            objMail._ModuleRefId = clsSendMail.enAT_VIEW_TYPE.TRAININGREQUISITION_MGT

            Dim objUser As New clsUserAddEdit
            objUser._Userunkid = IIf(iUserId <= 0, User._Object._Userunkid, iUserId)
            Dim objEmailColl As New clsEmailCollection(objMail._ToEmail, objMail._Subject, objMail._Message, mstrFormName, mintLoginEmployeeunkid, _
                                                       mstrClientIP, mstrHostName, objUser._Userunkid, iLoginTypeId, clsSendMail.enAT_VIEW_TYPE.TRAININGREQUISITION_MGT, _
                                                       IIf(objUser._Email = "", objUser._Firstname & " " & objUser._Lastname, objUser._Email))

            objEmailList.Add(objEmailColl)

            objUser = Nothing


            If blnIsSendMail = True AndAlso objEmailList.Count > 0 Then
                If HttpContext.Current Is Nothing Then
                    objThread = New Thread(AddressOf Send_Notification)
                    objThread.IsBackground = True
                    Dim arr(1) As Object
                    arr(0) = intCompanyUnkId
                    objThread.Start(arr)
                Else
                    Call Send_Notification(intCompanyUnkId)
                End If
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "Send_Notification_Employee", mstrModuleName)
        Finally
            objCommon = Nothing
        End Try
    End Sub

    Public Function InsertAuditTrails(ByVal objDataOperation As clsDataOperation, ByVal eAuditType As enAuditType) As Boolean
        Dim StrQ As String = ""
        Try
            StrQ = "INSERT INTO attrtraining_request_master ( " & _
                       "  tranguid " & _
                       ", trainingrequestunkid " & _
                       ", application_date " & _
                       ", employeeunkid " & _
                       ", coursemasterunkid " & _
                       ", isscheduled " & _
                       ", start_date " & _
                       ", end_date " & _
                       ", provider_name " & _
                       ", provider_address " & _
                       ", fundingsourceunkid " & _
                       ", totaltrainingcost " & _
                       ", approvedamount " & _
                       ", approvertranunkid " & _
                       ", isalignedcurrentrole " & _
                       ", ispartofpdp " & _
                       ", isforeigntravelling " & _
                       ", expectedreturn " & _
                       ", remarks " & _
                       ", statusunkid " & _
                       ", issubmit_approval " & _
                       ", audituserunkid " & _
                       ", loginemployeeunkid " & _
                       ", audittypeid " & _
                       ", auditdatetime " & _
                       ", isweb " & _
                       ", ip " & _
                       ", host " & _
                       ", form_name " & _
                       ", trainingproviderunkid " & _
                       ", trainingvenueunkid " & _
                       ", departmentaltrainingneedunkid " & _
                       ", isenroll_confirm " & _
                       ", isenroll_reject " & _
                       ", enroll_amount " & _
                       ", training_statusunkid " & _
                       ", qualificationgroupunkid " & _
                       ", qualificationunkid " & _
                       ", resultunkid " & _
                       ", gpacode " & _
                       ", isqualificaionupdated" & _
                       ", iscompleted_submit_approval " & _
                       ", completed_statusunkid " & _
                       ", periodunkid " & _
                       ", other_qualificationgrp " & _
                       ", other_qualification " & _
                       ", other_resultcode " & _
                       ", completed_userunkid " & _
                       ", completed_remark " & _
                       ", enrollment_remark " & _
                       ", completed_approval_date " & _
                       ", isdaysafterfeedback_submitted  " & _
                       ", daysafter_submitted_date " & _
                       ", daysafter_submitted_remark " & _
                       ", ispretrainingfeedback_submitted " & _
                       ", pretraining_submitted_date " & _
                       ", isposttrainingfeedback_submitted " & _
                       ", posttraining_submitted_date " & _
                       ", isdaysafter_linemanager_submitted " & _
                       ", daysafter_linemanager_submitted_date " & _
                       ", isskip_trainingrequest_and_approval  " & _
                       ", trainingcostemp " & _
                       ", approvedamountemp " & _
                       ", insertformid " & _
                       ", refno " & _
                       ", createuserunkid " & _
                       ", createloginemployeeunkid " & _
                       ", trainingtypeid " & _
                       ", approvalsettingid " & _
                       ", grouptrainingrequestunkid " & _
                       ", venue " & _
                  ") VALUES (" & _
                       "  LOWER(NEWID()) " & _
                       ", @trainingrequestunkid " & _
                       ", @application_date " & _
                       ", @employeeunkid " & _
                       ", @coursemasterunkid " & _
                       ", @isscheduled " & _
                       ", @start_date " & _
                       ", @end_date " & _
                       ", @provider_name " & _
                       ", @provider_address " & _
                       ", @fundingsourceunkid " & _
                       ", @totaltrainingcost " & _
                       ", @approvedamount " & _
                       ", @approvertranunkid " & _
                       ", @isalignedcurrentrole " & _
                       ", @ispartofpdp " & _
                       ", @isforeigntravelling " & _
                       ", @expectedreturn " & _
                       ", @remarks " & _
                       ", @statusunkid " & _
                       ", @issubmit_approval " & _
                       ", @audituserunkid " & _
                       ", @loginemployeeunkid " & _
                       ", @audittypeid " & _
                       ", GETDATE() " & _
                       ", @isweb " & _
                       ", @ip " & _
                       ", @host " & _
                       ", @form_name " & _
                       ", @trainingproviderunkid " & _
                       ", @trainingvenueunkid " & _
                       ", @departmentaltrainingneedunkid " & _
                       ", @isenroll_confirm " & _
                       ", @isenroll_reject " & _
                       ", @enroll_amount " & _
                       ", @training_statusunkid " & _
                       ", @qualificationgroupunkid " & _
                       ", @qualificationunkid " & _
                       ", @resultunkid " & _
                       ", @gpacode " & _
                       ", @isqualificaionupdated" & _
                       ", @iscompleted_submit_approval " & _
                       ", @completed_statusunkid " & _
                       ", @periodunkid " & _
                       ", @other_qualificationgrp " & _
                       ", @other_qualification " & _
                       ", @other_resultcode " & _
                       ", @completed_userunkid " & _
                       ", @completed_remark " & _
                       ", @enrollment_remark " & _
                       ", @completed_approval_date " & _
                       ", @isdaysafterfeedback_submitted  " & _
                       ", @daysafter_submitted_date " & _
                       ", @daysafter_submitted_remark " & _
                       ", @ispretrainingfeedback_submitted " & _
                       ", @pretraining_submitted_date " & _
                       ", @isposttrainingfeedback_submitted " & _
                       ", @posttraining_submitted_date " & _
                       ", @isdaysafter_linemanager_submitted " & _
                       ", @daysafter_linemanager_submitted_date " & _
                       ", @isskip_trainingrequest_and_approval  " & _
                       ", @trainingcostemp " & _
                       ", @approvedamountemp " & _
                       ", @insertformid " & _
                       ", @refno " & _
                       ", @createuserunkid " & _
                       ", @createloginemployeeunkid " & _
                       ", @trainingtypeid " & _
                       ", @approvalsettingid " & _
                       ", @grouptrainingrequestunkid " & _
                       ", @venue " & _
                  ")"
            'Hemant (10 Nov 2022) -- [venue]
            'Hemant (12 Oct 2022) -- [grouptrainingrequestunkid]
            'Hemant (25 Jul 2022) -- [approvalsettingid]
            'Hemant (29 Apr 2022) -- [trainingtypeid]
            'Sohail (15 Mar 2022) - [refno, createuserunkid, createloginemployeeunkid]
            'Hemant (09 Feb 2022) -- [trainingcostemp,approvedamountemp,insertformid]
            'Hemant (23 Sep 2021) -- [isskip_trainingrequest_and_approval]
            'Hemant (01 Sep 2021) -- [ispretrainingfeedback_submitted,pretraining_submitted_date,isposttrainingfeedback_submitted,posttraining_submitted_date,isdaysafter_linemanager_submitted,daysafter_linemanager_submitted_date]
            'Hemant (20 Aug 2021) -- [isdaysafterfeedback_submitted,daysafter_submitted_date,daysafter_submitted_remark]
            'Hemant (28 Jul 2021) -- [completed_approval_date]
            'Hemant (25 May 2021) -- [completed_userunkid,completed_remark,enrollment_remark]

            objDataOperation.ClearParameters()
            objDataOperation.AddParameter("@trainingrequestunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintTrainingRequestunkid.ToString)
            objDataOperation.AddParameter("@application_date", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, mdtApplication_Date.ToString)
            objDataOperation.AddParameter("@employeeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintEmployeeunkid.ToString)
            objDataOperation.AddParameter("@coursemasterunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintCourseMasterunkid.ToString)
            objDataOperation.AddParameter("@isscheduled", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsScheduled.ToString)
            objDataOperation.AddParameter("@start_date", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, mdtStart_Date.ToString)
            objDataOperation.AddParameter("@end_date", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, mdtEnd_Date.ToString)
            objDataOperation.AddParameter("@provider_name", SqlDbType.NVarChar, mstrProviderName.Trim.Length, mstrProviderName.ToString)
            objDataOperation.AddParameter("@provider_address", SqlDbType.NVarChar, mstrProviderAddress.Trim.Length, mstrProviderAddress.ToString)
            objDataOperation.AddParameter("@fundingsourceunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintFundingSourceunkid.ToString)
            objDataOperation.AddParameter("@totaltrainingcost", SqlDbType.Decimal, eZeeDataType.DECIMAL_SIZE, mdecTotalTrainingCost.ToString)
            objDataOperation.AddParameter("@approvedamount", SqlDbType.Decimal, eZeeDataType.DECIMAL_SIZE, mdecApprovedCost.ToString)
            objDataOperation.AddParameter("@approvertranunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintApproverTranunkid.ToString)
            objDataOperation.AddParameter("@isalignedcurrentrole", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsAlignedCurrentRole.ToString)
            objDataOperation.AddParameter("@ispartofpdp", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsPartofPDP.ToString)
            objDataOperation.AddParameter("@isforeigntravelling", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsForeignTravelling.ToString)
            objDataOperation.AddParameter("@expectedreturn", SqlDbType.NVarChar, mstrExpectedReturn.Trim.Length, mstrExpectedReturn.ToString)
            objDataOperation.AddParameter("@remarks", SqlDbType.NVarChar, mstrRemarks.Trim.Length, mstrRemarks.ToString)
            objDataOperation.AddParameter("@statusunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintStatusunkid.ToString)
            objDataOperation.AddParameter("@issubmit_approval", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsSubmitApproval.ToString)
            objDataOperation.AddParameter("@audituserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintUserunkid)
            objDataOperation.AddParameter("@loginemployeeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintLoginEmployeeunkid)
            objDataOperation.AddParameter("@audittypeid", SqlDbType.Int, eZeeDataType.INT_SIZE, eAuditType)
            objDataOperation.AddParameter("@isweb", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsWeb)
            objDataOperation.AddParameter("@ip", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrClientIP)
            objDataOperation.AddParameter("@host", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrHostName)
            objDataOperation.AddParameter("@form_name", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrFormName)
            objDataOperation.AddParameter("@trainingproviderunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintTrainingproviderunkid.ToString)
            objDataOperation.AddParameter("@trainingvenueunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintTrainingvenueunkid.ToString)
            objDataOperation.AddParameter("@departmentaltrainingneedunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintDepartmentaltrainingneedunkid.ToString)
            objDataOperation.AddParameter("@isenroll_confirm", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsEnrollConfirm.ToString)
            objDataOperation.AddParameter("@isenroll_reject", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsEnrollReject.ToString)
            objDataOperation.AddParameter("@enroll_amount", SqlDbType.Decimal, eZeeDataType.DECIMAL_SIZE, mdecEnrollAmount.ToString)
            objDataOperation.AddParameter("@training_statusunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintTrainingStatusunkid.ToString)
            objDataOperation.AddParameter("@qualificationgroupunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintQualificationGroupunkid.ToString)
            objDataOperation.AddParameter("@qualificationunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintQualificationunkid.ToString)
            objDataOperation.AddParameter("@resultunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintResultunkid.ToString)
            objDataOperation.AddParameter("@gpacode", SqlDbType.Decimal, eZeeDataType.DECIMAL_SIZE, mdecGPAcode.ToString)
            objDataOperation.AddParameter("@isqualificaionupdated", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsqualificaionupdated.ToString)
            objDataOperation.AddParameter("@iscompleted_submit_approval", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsCompletedSubmitApproval.ToString)
            objDataOperation.AddParameter("@completed_statusunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintCompletedStatusunkid.ToString)
            objDataOperation.AddParameter("@periodunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintPeriodunkid.ToString)
            objDataOperation.AddParameter("@other_qualificationgrp", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrOtherQualificationGrp.ToString)
            objDataOperation.AddParameter("@other_qualification", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrOtherQualification.ToString)
            objDataOperation.AddParameter("@other_ResultCode", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrOtherResultCode.ToString)
            objDataOperation.AddParameter("@completed_userunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintCompletedUserunkid)
            'Hemant (25 May 2021) -- Start
            'ISSUE/ENHANCEMENT : OLD-393 - Training module bug fixes and enhancements
            objDataOperation.AddParameter("@completed_remark", SqlDbType.NVarChar, mstrRemarks.Trim.Length, mstrCompletedRemark.ToString)
            objDataOperation.AddParameter("@enrollment_remark", SqlDbType.NVarChar, mstrRemarks.Trim.Length, mstrEnrollmentRemark.ToString)
            'Hemant (25 May 2021) -- End
            'Hemant (28 Jul 2021) -- Start             
            'ENHANCEMENT : OLD-293 - Training Evaluation
            If mdtCompletedApprovaldate <> Nothing Then
                objDataOperation.AddParameter("@completed_approval_date", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, mdtCompletedApprovaldate)
            Else
                objDataOperation.AddParameter("@completed_approval_date", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, DBNull.Value)
            End If
            'Hemant (28 Jul 2021) -- End
            'Hemant (20 Aug 2021) -- Start
            'ISSUE/ENHANCEMENT : OLD-447 - Give option to accommodate line manager feedback. It should be possible to mark questions as either for Manager or for Employee.
            objDataOperation.AddParameter("@isdaysafterfeedback_submitted", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsDaysAfterFeedbackSubmitted.ToString)
            If mdtDaysAfterFeedbackSubmittedDate <> Nothing Then
                objDataOperation.AddParameter("@daysafter_submitted_date", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, mdtDaysAfterFeedbackSubmittedDate)
            Else
                objDataOperation.AddParameter("@daysafter_submitted_date", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, DBNull.Value)
            End If
            objDataOperation.AddParameter("@daysafter_submitted_remark", SqlDbType.NVarChar, mstrRemarks.Trim.Length, mstrDaysAfterFeedbackSubmittedRemark.ToString)
            'Hemant (20 Aug 2021) -- End
            'Hemant (01 Sep 2021) -- Start
            'ISSUE/ENHANCEMENT : OLD-459 - SAVE button on Evaluation Forms in addition to the SUBMIT button.
            objDataOperation.AddParameter("@ispretrainingfeedback_submitted", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsPreTrainingFeedbackSubmitted.ToString)
            If mdtPreTrainingFeedbackSubmittedDate <> Nothing Then
                objDataOperation.AddParameter("@pretraining_submitted_date", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, mdtPreTrainingFeedbackSubmittedDate)
            Else
                objDataOperation.AddParameter("@pretraining_submitted_date", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, DBNull.Value)
            End If
            objDataOperation.AddParameter("@isposttrainingfeedback_submitted", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsPostTrainingFeedbackSubmitted.ToString)
            If mdtPostTrainingFeedbackSubmittedDate <> Nothing Then
                objDataOperation.AddParameter("@posttraining_submitted_date", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, mdtPostTrainingFeedbackSubmittedDate)
            Else
                objDataOperation.AddParameter("@posttraining_submitted_date", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, DBNull.Value)
            End If
            objDataOperation.AddParameter("@isdaysafter_linemanager_submitted", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsDaysAfterLineManagerFeedbackSubmitted.ToString)
            If mdtDaysAfterLineManagerFeedbackSubmittedDate <> Nothing Then
                objDataOperation.AddParameter("@daysafter_linemanager_submitted_date", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, mdtDaysAfterLineManagerFeedbackSubmittedDate)
            Else
                objDataOperation.AddParameter("@daysafter_linemanager_submitted_date", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, DBNull.Value)
            End If
            'Hemant (01 Sep 2021) -- End
            'Hemant (23 Sep 2021) -- Start
            'ENHANCEMENT : OLD-470 - Silverlands - New Training Setting to skip Training Requests Process.
            objDataOperation.AddParameter("@isskip_trainingrequest_and_approval", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsSkipTrainingRequestAndApproval.ToString)
            'Hemant (23 Sep 2021) -- End
            'Hemant (09 Feb 2022) -- Start            
            'OLD-551(NMB) : New config setting to allow Training Budget settings to be based on any allocation - Cost Center etc. 
            objDataOperation.AddParameter("@trainingcostemp", SqlDbType.Decimal, eZeeDataType.DECIMAL_SIZE, mdecTrainingCostEmp.ToString)
            objDataOperation.AddParameter("@approvedamountemp", SqlDbType.Decimal, eZeeDataType.DECIMAL_SIZE, mdecApprovedAmountEmp.ToString)
            objDataOperation.AddParameter("@insertformid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintInsertFormId.ToString)
            'Hemant (09 Feb 2022) -- End
            'Sohail (15 Mar 2022) -- Start
            'Enhancement : OLD-583 : NMB - Modification of Training Request Approval Screen to allow batch approvals.
            objDataOperation.AddParameter("@refno", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrRefno.ToString)
            objDataOperation.AddParameter("@createuserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintCreateuserunkid.ToString)
            objDataOperation.AddParameter("@createloginemployeeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintCreateloginEmployeeunkid.ToString)
            'Sohail (15 Mar 2022) -- End
            'Hemant (29 Apr 2022) -- Start
            'ISSUE/ENHANCEMENT : AC2-314(NMB) - NMB - As a manager, I want different approval flow (Training requests) for different training courses.
            objDataOperation.AddParameter("@trainingtypeid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintTrainingTypeId.ToString)
            'Hemant (29 Apr 2022) -- End
            'Hemant (25 Jul 2022) -- Start            
            'ENHANCEMENT(NMB) : AC2-723 - Implementation of Approver in Training request form
            objDataOperation.AddParameter("@approvalsettingid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintTrainingApprovalSettingID.ToString)
            'Hemant (25 Jul 2022) -- End
            'Hemant (12 Oct 2022) -- Start
            'ENHANCEMENT(NMB) :  AC2-952 - As a user, I want to have a document attachment option on the group training request page
            objDataOperation.AddParameter("@grouptrainingrequestunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintGroupTrainingRequestunkid.ToString)
            'Hemant (12 Oct 2022) -- End
            'Hemant (10 Nov 2022) -- Start
            'ENHANCEMENT(NMB) :  AC2-1024 - As a user, I want to have a field to capture the Training Venue on both individual and group training request pages. This filed will be free text and mandatory
            objDataOperation.AddParameter("@venue", SqlDbType.NVarChar, mstrVenue.Trim.Length, mstrVenue.ToString)
            'Hemant (27 Oct 2022) -- End

            objDataOperation.ExecNonQuery(StrQ)

            If objDataOperation.ErrorMessage <> "" Then
                Throw New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
            End If

            Return True
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: InsertAuditTrails; Module Name: " & mstrModuleName)
        Finally
        End Try
    End Function

    Public Function getTrainingComboList(ByVal strTableName As String, ByVal intPeriodUnkId As Integer, Optional ByVal blnOnlySchedule As Boolean = False, _
                                         Optional ByVal blnNotScheduled As Boolean = False, _
                                         Optional ByVal blnAddSelect As Boolean = False) As DataSet
        'Hemant (09 Feb 2022) -- [intPeriodUnkId,blnAddSelect]
        Dim dsList As New DataSet
        Dim StrQ As String = String.Empty
        objDataOperation = New clsDataOperation
        Try
            'Hemant (09 Feb 2022) -- Start            
            'OLD-558(NMB) : Training Request & Training Group Request Screen Enhancement(Give Drop-down menu for Training Name selection)
            If blnAddSelect Then
                StrQ = "SELECT 0 AS masterunkid, @Select AS Name, '' AS description, 0 AS coursetypeid UNION "
            End If
            'Hemant (09 Feb 2022) -- End

            StrQ &= "SELECT " & _
                       "masterunkid " & _
                       ", name " & _
                       ",ISNULL(description, '') AS description " & _
                       ",ISNULL(coursetypeid, 0) AS coursetypeid " & _
                    "FROM cfcommon_master " & _
                    "WHERE mastertype = " & clsCommon_Master.enCommonMaster.TRAINING_COURSEMASTER & " " & _
                    "AND isactive = 1 "
            If blnOnlySchedule = True Then
                StrQ &= "AND masterunkid  IN (SELECT trainingcourseunkid from trdepartmentaltrainingneed_master where isvoid = 0 AND periodunkid = @periodunkid ) "
                'Hemant (09 Feb 2022) -- [AND periodunkid = @periodunkid]
            End If

            If blnNotScheduled = True Then
                StrQ &= "AND masterunkid NOT IN (SELECT trainingcourseunkid from trdepartmentaltrainingneed_master where isvoid = 0 AND periodunkid = @periodunkid )"
                'Hemant (09 Feb 2022) -- [AND periodunkid = @periodunkid]
            End If

            StrQ &= "ORDER BY name "

            'Hemant (09 Feb 2022) -- Start            
            'OLD-558(NMB) : Training Request & Training Group Request Screen Enhancement(Give Drop-down menu for Training Name selection)
            objDataOperation.AddParameter("@Select", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 51, "Select"))
            objDataOperation.AddParameter("@periodunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intPeriodUnkId)
            'Hemant (09 Feb 2022) -- End
            dsList = objDataOperation.ExecQuery(StrQ, strTableName)

            If objDataOperation.ErrorMessage <> "" Then
                Throw New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
            End If

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: getStatusComboList; Module Name: " & mstrModuleName)
        Finally
        End Try
        Return dsList
    End Function

    Public Function IsTableDataUpdate(ByVal unkid As Integer, Optional ByVal objDooperation As clsDataOperation = Nothing) As Boolean
        Dim strQ As String = ""
        Dim dsList As New DataSet
        Dim exForce As Exception
        Try
            If objDooperation Is Nothing Then
                objDataOperation = New clsDataOperation
            Else
                objDataOperation = objDooperation
            End If

            strQ = "SELECT TOP 1 * FROM attrtraining_request_master WHERE trainingrequestunkid = @trainingrequestunkid AND audittypeid <> 3 ORDER BY auditdatetime DESC"

            objDataOperation.ClearParameters()
            objDataOperation.AddParameter("@trainingrequestunkid", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, unkid)

            dsList = objDataOperation.ExecQuery(strQ, "List")

            For Each dr As DataRow In dsList.Tables(0).Rows

                If CDate(dr("application_date")) = mdtApplication_Date AndAlso dr("employeeunkid").ToString() = mintEmployeeunkid AndAlso dr("coursemasterunkid").ToString() = mintCourseMasterunkid _
                    AndAlso CBool(dr("isscheduled")) = mblnIsScheduled AndAlso CDate(dr("start_date")) = mdtStart_Date _
                    AndAlso CDate(dr("end_date")) = mdtEnd_Date AndAlso dr("fundingsourceunkid").ToString() = mintFundingSourceunkid _
                    AndAlso CDec(dr("totaltrainingcost")) = mdecTotalTrainingCost AndAlso CDec(dr("approvedamount")) = mdecApprovedCost _
                    AndAlso CBool(dr("isalignedcurrentrole")) = mblnIsAlignedCurrentRole AndAlso CBool(dr("ispartofpdp")) = mblnIsPartofPDP _
                    AndAlso CBool(dr("isforeigntravelling")) = mblnIsForeignTravelling AndAlso dr("expectedreturn").ToString() = mstrExpectedReturn _
                    AndAlso dr("remarks").ToString() = mstrRemarks AndAlso dr("statusunkid").ToString() = mintStatusunkid _
                    AndAlso CBool(dr("issubmit_approval")) = mblnIsSubmitApproval AndAlso dr("trainingproviderunkid").ToString() = mintTrainingproviderunkid _
                    AndAlso dr("trainingvenueunkid").ToString() = mintTrainingvenueunkid AndAlso dr("departmentaltrainingneedunkid").ToString() = mintDepartmentaltrainingneedunkid _
                    AndAlso CBool(dr("isenroll_confirm")) = mblnIsEnrollConfirm AndAlso CBool(dr("isenroll_reject")) = mblnIsEnrollReject _
                    AndAlso CDec(dr("enroll_amount")) = mdecEnrollAmount AndAlso dr("training_statusunkid").ToString() = mintTrainingStatusunkid _
                    AndAlso CInt(dr("qualificationgroupunkid")) = mintQualificationGroupunkid AndAlso dr("qualificationunkid").ToString() = mintQualificationunkid _
                    AndAlso dr("resultunkid").ToString() = mintResultunkid AndAlso CDec(dr("gpacode")) = mdecGPAcode _
                    AndAlso CBool(dr("isqualificaionupdated")) = mblnIsqualificaionupdated AndAlso CBool(dr("iscompleted_submit_approval")) = mblnIsCompletedSubmitApproval _
                    AndAlso CInt(dr("completed_statusunkid")) = mintCompletedStatusunkid AndAlso CInt(dr("periodunkid")) = mintPeriodunkid _
                    AndAlso dr("other_qualificationgrp").ToString() = mstrOtherQualificationGrp AndAlso dr("other_qualification").ToString() = mstrOtherQualification _
                    AndAlso dr("other_ResultCode").ToString() = mstrOtherResultCode AndAlso dr("completed_remark").ToString() = mstrCompletedRemark _
                    AndAlso dr("enrollment_remark").ToString() = mstrEnrollmentRemark AndAlso CBool(dr("isdaysafterfeedback_submitted")) = mblnIsDaysAfterFeedbackSubmitted _
                    AndAlso dr("daysafter_submitted_remark").ToString() = mstrDaysAfterFeedbackSubmittedRemark AndAlso CBool(dr("ispretrainingfeedback_submitted")) = mblnIsPreTrainingFeedbackSubmitted _
                    AndAlso CBool(dr("isposttrainingfeedback_submitted")) = mblnIsPostTrainingFeedbackSubmitted AndAlso CBool(dr("isdaysafter_linemanager_submitted")) = mblnIsDaysAfterLineManagerFeedbackSubmitted _
                    AndAlso CBool(dr("isskip_trainingrequest_and_approval")) = mblnIsSkipTrainingRequestAndApproval AndAlso CDec(dr("trainingcostemp")) = mdecTrainingCostEmp _
                    AndAlso CDec(dr("approvedamountemp")) = mdecApprovedAmountEmp AndAlso CInt(dr("trainingtypeid")) = mintTrainingTypeId AndAlso CInt(dr("approvalsettingid")) = mintTrainingApprovalSettingID _
                    AndAlso CInt(dr("grouptrainingrequestunkid")) = mintGroupTrainingRequestunkid AndAlso dr("venue").ToString() = mstrVenue Then
                    'Hemant (10 Nov 2022) -- [venue]
                    'Hemant (12 Oct 2022) -- [grouptrainingrequestunkid]
                    'Hemant (25 Jul 2022) -- [approvalsettingid]
                    'Hemant (29 Apr 2022) -- [trainingtypeid]
                    'Hemant (09 Feb 2022) -- [trainingcostemp,approvedamountemp]
                    'Hemant (23 Sep 2021) -- [isskip_trainingrequest_and_approval]
                    'Hemant (01 Sep 2021) -- [ispretrainingfeedback_submitted,isposttrainingfeedback_submitted,isdaysafter_linemanager_submitted]
                    'Hemant (20 Aug 2021) -- [isdaysafterfeedback_submitted,daysafter_submitted_remark]
                    'Hemant (25 May 2021) -- [completed_remark,enrollment_remark]

                    Return True
                Else
                    Return False
                End If
            Next
            Return False

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: IsTableDataUpdate; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If objDooperation Is Nothing Then objDataOperation = Nothing
        End Try
    End Function

    Public Function UpdateEnrollment(ByVal strUnkIDs As String, _
                                     ByVal IsEnrollConfirm As Boolean, _
                                     ByVal IsEnrollReject As Boolean, _
                                     ByVal decEnrollAmount As Decimal, _
                                     ByVal strEnrollmentRemark As String, _
                                     Optional ByVal xDataOp As clsDataOperation = Nothing _
                                     ) As Boolean
        'Hemant (25 May 2021) -- [strEnrollmentRemark]

        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        If strUnkIDs.Trim = "" Then Return True

        If xDataOp IsNot Nothing Then
            objDataOperation = xDataOp
        Else
            objDataOperation = New clsDataOperation
            objDataOperation.BindTransaction()
        End If
        objDataOperation.ClearParameters()

        Try
            For Each strID As String In strUnkIDs.Split(",")
                mDataOp = objDataOperation
                _TrainingRequestunkid = CInt(strID)
                mintUserunkid = pintUserunkid
                mintLoginEmployeeunkid = pintLoginEmployeeunkid
                mblnIsEnrollConfirm = IsEnrollConfirm
                mblnIsEnrollReject = IsEnrollReject
                mdecEnrollAmount = decEnrollAmount

                mintAuditUserId = pintAuditUserId
                mstrClientIP = pstrClientIP
                mstrHostName = pstrHostName
                mstrFormName = pstrFormName
                mblnIsWeb = pblnIsWeb

                'Hemant (25 May 2021) -- Start
                'ISSUE/ENHANCEMENT : OLD-393 - Training module bug fixes and enhancements
                mstrEnrollmentRemark = strEnrollmentRemark
                'Hemant (25 May 2021) -- End
                objDataOperation.ClearParameters()

                strQ = "UPDATE trtraining_request_master SET " & _
                          "  isenroll_confirm = @isenroll_confirm " & _
                          ", isenroll_reject = @isenroll_reject " & _
                          ", enroll_amount = @enroll_amount " & _
                          ", enrollment_remark = @enrollment_remark " & _
                        "WHERE trainingrequestunkid = @trainingrequestunkid "
                'Hemant (25 May 2021) -- [enrollment_remark]

                objDataOperation.AddParameter("@trainingrequestunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, CInt(strID))
                objDataOperation.AddParameter("@isenroll_confirm", SqlDbType.Bit, eZeeDataType.BIT_SIZE, IsEnrollConfirm)
                objDataOperation.AddParameter("@isenroll_reject", SqlDbType.Bit, eZeeDataType.BIT_SIZE, IsEnrollReject)
                objDataOperation.AddParameter("@enroll_amount", SqlDbType.Decimal, eZeeDataType.DECIMAL_SIZE, decEnrollAmount)
                'Hemant (25 May 2021) -- Start
                'ISSUE/ENHANCEMENT : OLD-393 - Training module bug fixes and enhancements
                objDataOperation.AddParameter("@enrollment_remark", SqlDbType.NVarChar, strEnrollmentRemark.Trim.Length, strEnrollmentRemark)
                'Hemant (25 May 2021) -- End

                Call objDataOperation.ExecNonQuery(strQ)

                If objDataOperation.ErrorMessage <> "" Then
                    exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                    Throw exForce
                End If

                If InsertAuditTrails(objDataOperation, 2) = False Then
                    If xDataOp Is Nothing Then objDataOperation.ReleaseTransaction(False)
                    Return False
                End If
            Next


            If xDataOp Is Nothing Then objDataOperation.ReleaseTransaction(True)

            Return True
        Catch ex As Exception
            If xDataOp Is Nothing Then objDataOperation.ReleaseTransaction(False)
            Throw New Exception(ex.Message & "; Procedure Name: UpdateStatus; Module Name: " & mstrModuleName)
            Return False
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            If xDataOp Is Nothing Then objDataOperation = Nothing
        End Try
    End Function

    Public Function IsTrainingRequestedByEmployee(ByVal strDepartmentalTrainingNeedunkIDs As String, _
                                            Optional ByVal xDataOpr As clsDataOperation = Nothing, _
                                            Optional ByVal intEmployeeId As Integer = 0) As Boolean
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception


        If xDataOpr Is Nothing Then
            objDataOperation = New clsDataOperation
        Else
            objDataOperation = xDataOpr
        End If
        objDataOperation.ClearParameters()

        If strDepartmentalTrainingNeedunkIDs.Trim = "" Then strDepartmentalTrainingNeedunkIDs = "-9999"

        Try
            strQ = "SELECT * " & _
                   " FROM trtraining_request_master where " & _
                   "  isvoid= 0 " & _
                   "  AND departmentaltrainingneedunkid IN (" & strDepartmentalTrainingNeedunkIDs & ")  "

            If intEmployeeId > 0 Then
                strQ &= " AND employeeunkid = @employeeunkid "
                objDataOperation.AddParameter("@employeeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intEmployeeId)
            End If

            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            If dsList.Tables(0).Rows.Count > 0 Then
                Return True
            Else
                Return False
            End If

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: IsTrainingRequestedByEmployee; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            If xDataOpr Is Nothing Then objDataOperation = Nothing
        End Try

    End Function

    Public Function UpdateCompletedTrainingInfo(ByVal strUnkIDs As String, _
                                                 ByVal intQualificationGroupunkid As Integer, _
                                                 ByVal intQualificationunkid As Integer, _
                                                 ByVal intResultunkid As Integer, _
                                                 ByVal decGPAcode As Decimal, _
                                                 ByVal strOtherQualificationGroup As String, _
                                                 ByVal strOtherQualification As String, _
                                                 ByVal strOtherResultCode As String, _
                                                 ByVal blnIsFromApproval As Boolean, _
                                                 ByVal intStatusunkid As Integer, _
                                                 ByVal intPendingTrainingTranunkid As Integer, _
                                                 ByVal strCompletedRemark As String, _
                                                 ByVal dtQualificationDocument As DataTable, _
                                                 Optional ByVal xDataOp As clsDataOperation = Nothing, _
                                                 Optional ByVal mdtAttachmentTable As DataTable = Nothing _
                                                 ) As Boolean
        'Hemant (12 Oct 2022) -- [mdtAttachmentTable]
        Dim objApprovalProcessTran As New clstrainingapproval_process_tran
        Dim dsList As DataSet = Nothing
        Dim dtApprover As DataTable = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        If strUnkIDs.Trim = "" Then Return True

        If xDataOp IsNot Nothing Then
            objDataOperation = xDataOp
        Else
            objDataOperation = New clsDataOperation
            objDataOperation.BindTransaction()
        End If
        objDataOperation.ClearParameters()

        Try
            For Each strID As String In strUnkIDs.Split(",")
                mDataOp = objDataOperation
                _TrainingRequestunkid = CInt(strID)
                mintUserunkid = pintUserunkid
                mintLoginEmployeeunkid = pintLoginEmployeeunkid
                mintQualificationGroupunkid = intQualificationGroupunkid
                mintQualificationunkid = intQualificationunkid
                mintResultunkid = intResultunkid
                mdecGPAcode = decGPAcode
                mblnIsCompletedSubmitApproval = True
                mstrOtherQualificationGrp = strOtherQualificationGroup
                mstrOtherQualification = strOtherQualification
                mstrOtherResultCode = strOtherResultCode

                mintAuditUserId = pintAuditUserId
                mstrClientIP = pstrClientIP
                mstrHostName = pstrHostName
                mstrFormName = pstrFormName
                mblnIsWeb = pblnIsWeb

                objDataOperation.ClearParameters()

                strQ = "UPDATE trtraining_request_master SET " & _
                          "  qualificationgroupunkid = @qualificationgroupunkid " & _
                          ", qualificationunkid = @qualificationunkid " & _
                          ", resultunkid = @resultunkid " & _
                          ", gpacode = @gpacode " & _
                          ", iscompleted_submit_approval = 1 " & _
                          ", completed_statusunkid = 1 " & _
                          ", other_qualificationgrp = @other_qualificationgrp " & _
                          ", other_qualification = @other_qualification " & _
                          ", other_resultcode = @other_resultcode " & _
                        "WHERE trainingrequestunkid = @trainingrequestunkid "

                objDataOperation.AddParameter("@trainingrequestunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, CInt(strID))
                objDataOperation.AddParameter("@qualificationgroupunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intQualificationGroupunkid)
                objDataOperation.AddParameter("@qualificationunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intQualificationunkid)
                objDataOperation.AddParameter("@resultunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intResultunkid)
                objDataOperation.AddParameter("@gpacode", SqlDbType.Decimal, eZeeDataType.DECIMAL_SIZE, decGPAcode)
                objDataOperation.AddParameter("@other_qualificationgrp", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, strOtherQualificationGroup)
                objDataOperation.AddParameter("@other_qualification", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, strOtherQualification)
                objDataOperation.AddParameter("@other_resultcode", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, strOtherResultCode)

                Call objDataOperation.ExecNonQuery(strQ)

                If objDataOperation.ErrorMessage <> "" Then
                    exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                    Throw exForce
                End If

                If InsertAuditTrails(objDataOperation, 2) = False Then
                    If xDataOp Is Nothing Then objDataOperation.ReleaseTransaction(False)
                    Return False
                End If

                'Hemant (18 May 2021) -- Start
                'ISSUE/ENHANCEMENT : Changed approval flow for Training Completion Status
                If 1 = 2 Then
                    'Hemant (18 May 2021) -- End
                    If blnIsFromApproval = True Then

                        objApprovalProcessTran._PendingTrainingTranunkid = intPendingTrainingTranunkid
                        With objApprovalProcessTran
                            ._Userunkid = mintAuditUserId
                            ._ClientIP = mstrClientIP
                            ._HostName = mstrHostName
                            ._FormName = mstrFormName
                            ._IsWeb = mblnIsWeb
                            ._CompletedStatusunkid = intStatusunkid
                            ._CompletedRemark = strCompletedRemark
                        End With

                        strQ = " UPDATE trtrainingapproval_process_tran set " & _
                                    " completed_statusunkid = " & intStatusunkid & _
                                    ", completed_remark = '" & strCompletedRemark & "' " & _
                                    " WHERE  pendingtrainingtranunkid = @pendingtrainingtranunkid   "

                        objDataOperation.ClearParameters()
                        objDataOperation.AddParameter("@pendingtrainingtranunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intPendingTrainingTranunkid.ToString)
                        Call objDataOperation.ExecNonQuery(strQ)

                        If objDataOperation.ErrorMessage <> "" Then
                            exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                            Throw exForce
                        End If

                        If objApprovalProcessTran.InsertAuditTrailForTrainingApproval(objDataOperation, 2) = False Then
                            exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                            Throw exForce
                        End If

                    End If

                    strQ = " SELECT pendingtrainingtranunkid,trainingrequestunkid,approvertranunkid, priority,completed_visibleid FROM trtrainingapproval_process_tran WHERE isvoid = 0 AND trainingrequestunkid = @trainingrequestunkid "
                    objDataOperation.ClearParameters()
                    objDataOperation.AddParameter("@trainingrequestunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintTrainingRequestunkid.ToString)
                    Dim dsApprover As DataSet = objDataOperation.ExecQuery(strQ, "List")

                    If objDataOperation.ErrorMessage <> "" Then
                        exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                        Throw exForce
                    End If

                    Dim intMinPriority As Integer

                    intMinPriority = dsApprover.Tables(0).AsEnumerable().Select(Function(x) x.Field(Of Integer)("priority")).Min()

                    Dim dtVisibility As DataTable = New DataView(dsApprover.Tables(0), "priority = " & intMinPriority & " AND priority <> -1", "", DataViewRowState.CurrentRows).ToTable

                    If dtVisibility IsNot Nothing AndAlso dtVisibility.Rows.Count > 0 Then

                        If blnIsFromApproval = True Then


                            strQ = " UPDATE trtrainingapproval_process_tran set " & _
                                     " completed_visibleid = " & intStatusunkid & _
                                     " WHERE  trainingrequestunkid = @trainingrequestunkid and employeeunkid = @employeeunkid AND approvertranunkid = @approvertranunkid  AND isvoid = 0   "

                        Else
                            strQ = " UPDATE trtrainingapproval_process_tran set " & _
                                     "  completed_statusunkid = 1 " & _
                                     ", completed_visibleid = 1 " & _
                                     " WHERE  trainingrequestunkid = @trainingrequestunkid and employeeunkid = @employeeunkid AND approvertranunkid = @approvertranunkid AND isvoid = 0   "
                        End If

                    End If

                    For i As Integer = 0 To dtVisibility.Rows.Count - 1
                        objDataOperation.ClearParameters()
                        objDataOperation.AddParameter("@trainingrequestunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, dtVisibility.Rows(i)("trainingrequestunkid").ToString)
                        objDataOperation.AddParameter("@employeeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintEmployeeunkid)
                        objDataOperation.AddParameter("@approvertranunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, dtVisibility.Rows(i)("approvertranunkid").ToString)
                        objDataOperation.ExecNonQuery(strQ)

                        If objDataOperation.ErrorMessage <> "" Then
                            exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                            Throw exForce
                        End If

                    Next

                End If 'Hemant (18 May 2021)

                If blnIsFromApproval = True Then

                    If intStatusunkid = enTrainingRequestStatus.APPROVED Then
                        Dim objQualification As New clsEmp_Qualification_Tran
                        objQualification._Employeeunkid = mintEmployeeunkid
                        objQualification._Instituteunkid = mintTrainingproviderunkid
                        objQualification._Award_Start_Date = mdtStart_Date
                        objQualification._Award_End_Date = mdtEnd_Date
                        objQualification._Reference_No = ""
                        objQualification._Transaction_Date = ConfigParameter._Object._CurrentDateAndTime
                        objQualification._Qualificationgroupunkid = intQualificationGroupunkid
                        objQualification._Qualificationunkid = intQualificationunkid
                        objQualification._Resultunkid = intResultunkid
                        objQualification._GPAcode = decGPAcode
                        objQualification._Other_QualificationGrp = strOtherQualificationGroup
                        objQualification._Other_Qualification = strOtherQualification
                        objQualification._other_ResultCode = strOtherResultCode
                        objQualification._Userunkid = mintAuditUserId
                        If objQualification.Insert(dtQualificationDocument, objDataOperation) = False Then
                            mstrMessage = objQualification._Message
                            If xDataOp Is Nothing Then objDataOperation.ReleaseTransaction(False)
                            Return False
                        End If
                    End If


                    Dim objTrainingRequest As New clstraining_request_master
                    objTrainingRequest._TrainingRequestunkid = mintTrainingRequestunkid
                    If intStatusunkid = enTrainingRequestStatus.APPROVED Then objTrainingRequest._Isqualificaionupdated = True
                    objTrainingRequest._CompletedStatusunkid = intStatusunkid
                    objTrainingRequest._CompletedUserunkid = pintCompletedUserunkid
                    objTrainingRequest._LoginEmployeeunkid = pintLoginEmployeeunkid
                    objTrainingRequest._ClientIP = mstrClientIP
                    objTrainingRequest._FormName = mstrFormName
                    objTrainingRequest._HostName = mstrHostName
                    objTrainingRequest._IsWeb = pblnIsWeb
                    'Hemant (25 May 2021) -- Start
                    'ISSUE/ENHANCEMENT : OLD-393 - Training module bug fixes and enhancements
                    objTrainingRequest._CompletedRemark = strCompletedRemark
                    'Hemant (25 May 2021) -- End
                    'Hemant (28 Jul 2021) -- Start             
                    'ENHANCEMENT : OLD-293 - Training Evaluation
                    objTrainingRequest._CompletedApprovaldate = ConfigParameter._Object._CurrentDateAndTime
                    'Hemant (28 Jul 2021) -- End

                    If objTrainingRequest.Update(objDataOperation) = False Then
                        exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                        Throw exForce
                    End If
                    objTrainingRequest = Nothing
                End If

                'Hemant (12 Oct 2022) -- Start
                'ENHANCEMENT(NMB) :  AC2-952 - As a user, I want to have a document attachment option on the group training request page
                If mdtAttachmentTable IsNot Nothing AndAlso mintTrainingRequestunkid > 0 Then
                    Dim objDocument As New clsScan_Attach_Documents
                    Dim dtTran As DataTable = objDocument._Datatable
                    Dim docLocalPath As String = ConfigParameter._Object._Document_Path & "\"

                    Dim strFolderName As String = (New clsScan_Attach_Documents).GetDocFolderName("Docs", enScanAttactRefId.TRAINING_NEED_FORM).Tables(0).Rows(0)("Name").ToString
                    If strFolderName IsNot Nothing Then strFolderName = strFolderName & "\"
                    Dim dr As DataRow
                    For Each drow As DataRow In mdtAttachmentTable.Rows
                        dr = dtTran.NewRow
                        dr("scanattachtranunkid") = drow("scanattachtranunkid")
                        dr("documentunkid") = drow("documentunkid")
                        dr("employeeunkid") = drow("employeeunkid")
                        dr("filename") = drow("filename")
                        dr("scanattachrefid") = drow("scanattachrefid")
                        dr("modulerefid") = drow("modulerefid")
                        dr("form_name") = drow("form_name")
                        dr("userunkid") = drow("userunkid")
                        dr("transactionunkid") = mintTrainingRequestunkid
                        dr("attached_date") = drow("attached_date")
                        dr("orgfilepath") = drow("localpath")
                        dr("destfilepath") = docLocalPath & strFolderName & CStr(drow.Item("filename"))
                        dr("AUD") = drow("AUD")
                        dr("userunkid") = mintUserunkid
                        dr("fileuniquename") = drow("fileuniquename")
                        dr("filepath") = drow("filepath")
                        dr("filesize") = drow("filesize_kb")
                        dr("file_data") = drow("file_data")

                        dtTran.Rows.Add(dr)
                    Next
                    objDocument._Datatable = dtTran
                    objDocument.InsertUpdateDelete_Documents(objDataOperation)

                    If objDataOperation.ErrorMessage <> "" Then
                        exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                        Throw exForce
                    End If
                End If
                'Hemant (12 Oct 2022) -- End

            Next


            If xDataOp Is Nothing Then objDataOperation.ReleaseTransaction(True)

            Return True
        Catch ex As Exception
            If xDataOp Is Nothing Then objDataOperation.ReleaseTransaction(False)
            Throw New Exception(ex.Message & "; Procedure Name: UpdateStatus; Module Name: " & mstrModuleName)
            Return False
        Finally
            exForce = Nothing
            objApprovalProcessTran = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            If xDataOp Is Nothing Then objDataOperation = Nothing
        End Try
    End Function

    Public Function GetUnScheduleTrainingList(ByVal xDatabaseName As String, _
                            ByVal xUserUnkid As Integer, _
                            ByVal xYearUnkid As Integer, _
                            ByVal xCompanyUnkid As Integer, _
                            ByVal xPeriodStart As DateTime, _
                            ByVal xPeriodEnd As DateTime, _
                            ByVal xUserModeSetting As String, _
                            ByVal xOnlyApproved As Boolean, _
                            ByVal xIncludeIn_ActiveEmployee As Boolean, _
                            ByVal strTableName As String, _
                            Optional ByVal intStatusID As Integer = 0, _
                            Optional ByVal mstrFilter As String = "", _
                            Optional ByVal blnApplyUserAccessFilter As Boolean = True, _
                            Optional ByVal IsUsedAsMSS As Boolean = True) As DataSet
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        Dim objDataOperation As clsDataOperation
        objDataOperation = New clsDataOperation

        Try
            Dim xDateJoinQry, xDateFilterQry, xUACQry, xUACFiltrQry, xAdvanceJoinQry As String
            xDateJoinQry = "" : xDateFilterQry = "" : xUACQry = "" : xUACFiltrQry = "" : xAdvanceJoinQry = ""
            Call GetDatesFilterString(xDateJoinQry, xDateFilterQry, xPeriodStart, xPeriodEnd, , , xDatabaseName)
            Call GetAdvanceFilterQry(xAdvanceJoinQry, xPeriodEnd, xDatabaseName)

            If blnApplyUserAccessFilter = True Then
                If IsUsedAsMSS Then Call NewAccessLevelFilterString(xUACQry, xUACFiltrQry, xPeriodEnd, xOnlyApproved, xDatabaseName, xUserUnkid, xCompanyUnkid, xYearUnkid, xUserModeSetting)
            End If
            strQ = "SELECT " & _
                   "    trtraining_request_master.departmentaltrainingneedunkid " & _
                   ",    CONVERT(CHAR(8), trtraining_request_master.start_date, 112) AS startdate " & _
                   ",    CONVERT(CHAR(8), trtraining_request_master.end_date, 112) AS enddate " & _
                   ",    ISNULL(tcourse.name, '') AS trainingcoursename " & _
                   ",    ISNULL(hremployee_master.firstname, '') + ' ' + ISNULL(hremployee_master.othername, '') + ' ' + ISNULL(hremployee_master.surname, '') AS allocationtranname " & _
                   ",    '' AS learningmethodname " & _
                   ",    ISNULL(trtraining_request_master.statusunkid, 0) AS statusunkid " & _
                   ",    CASE ISNULL(trtraining_request_master.statusunkid, 0) " & _
                   "        WHEN 0 THEN ' Select' " & _
                   "        WHEN 1 THEN @Pending " & _
                   "        WHEN 2 THEN @Approved " & _
                   "        WHEN 3 THEN @Rejected " & _
                   "     END AS statusname " & _
                   ",    -1 targetedgroupunkid " & _
                   ",    ISNULL(trtraining_request_master.training_statusunkid, 0) AS request_statusunkid " & _
                "FROM trtraining_request_master " & _
                " JOIN hremployee_master ON hremployee_master.employeeunkid = trtraining_request_master.employeeunkid  " & _
                "LEFT JOIN trtraining_calendar_master " & _
                    "ON trtraining_calendar_master.calendarunkid = trtraining_request_master.periodunkid " & _
                "LEFT JOIN cfcommon_master AS tcourse " & _
                    "ON tcourse.masterunkid = trtraining_request_master.coursemasterunkid " & _
                        "AND tcourse.mastertype = " & clsCommon_Master.enCommonMaster.TRAINING_COURSEMASTER & " " & _
                        "AND tcourse.isactive = 1 " & _
                "LEFT JOIN hrinstitute_master " & _
                    "ON hrinstitute_master.instituteunkid = trtraining_request_master.trainingproviderunkid " & _
                        "AND hrinstitute_master.isactive = 1 " & _
                "LEFT JOIN trtrainingvenue_master " & _
                    "ON trtrainingvenue_master.venueunkid = trtraining_request_master.trainingvenueunkid " & _
                        "AND trtrainingvenue_master.isactive = 1 "

            If xDateJoinQry.Trim.Length > 0 Then
                strQ &= xDateJoinQry
            End If

            If blnApplyUserAccessFilter = True Then
                If xUACQry.Trim.Length > 0 Then
                    strQ &= xUACQry
                End If
            End If

            If xAdvanceJoinQry.Trim.Length > 0 Then
                strQ &= xAdvanceJoinQry
            End If

            strQ &= " WHERE trtraining_request_master.isvoid = 0 "

            strQ &= "AND trtraining_calendar_master.isactive = 1 " & _
                    "AND trtraining_request_master.isscheduled = 0 " & _
                    "AND trtraining_request_master.statusunkid = " & clstraining_requisition_approval_master.enApprovalStatus.Approved

            If xUACFiltrQry.Trim.Length > 0 Then
                strQ &= " AND " & xUACFiltrQry
            End If

            If xIncludeIn_ActiveEmployee = False Then
                If xDateFilterQry.Trim.Length > 0 Then
                    strQ &= xDateFilterQry
                End If
            End If


            If mstrFilter.Trim.Length > 0 Then
                strQ &= " AND " & mstrFilter
            End If

            objDataOperation.ClearParameters()
            objDataOperation.AddParameter("@Pending", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 1, "Pending"))
            objDataOperation.AddParameter("@Approved", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 2, "Approved"))
            objDataOperation.AddParameter("@Rejected", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 3, "Rejected"))


            dsList = objDataOperation.ExecQuery(strQ, strTableName)

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: GetList; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
        Return dsList
    End Function


    'Hemant (09 Feb 2022) -- Start            
    'OLD-549(NMB) : Give new screen for training approver allocation mapping
    Public Function IsTrainingPendingForApproval(Optional ByVal xDataOpr As clsDataOperation = Nothing, Optional ByVal strDatabaseName As String = "", Optional ByVal intApprovalSettingID As Integer = 0) As Boolean
        'Hemant (25 Jul 2022) -- [intApprovalSettingID]
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception


        If xDataOpr Is Nothing Then
            objDataOperation = New clsDataOperation
        Else
            objDataOperation = xDataOpr
        End If
        objDataOperation.ClearParameters()

        Try
            Dim strDBName As String = If(strDatabaseName.Trim <> "", strDatabaseName & "..", "")

            strQ = "SELECT * " & _
                   " FROM " & strDBName & "trtraining_request_master where " & _
                   "   isvoid = 0 " & _
                   "   AND issubmit_approval = 1 " & _
                   "   AND statusunkid = " & clstraining_requisition_approval_master.enApprovalStatus.SubmitForApproval & " "

            If intApprovalSettingID > 0 Then
                strQ &= " AND  approvalsettingid = @approvalsettingid "
                objDataOperation.AddParameter("@approvalsettingid", SqlDbType.Int, eZeeDataType.INT_SIZE, intApprovalSettingID.ToString)
            End If

            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            If dsList.Tables(0).Rows.Count > 0 Then
                Return True
            Else
                Return False
            End If

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: IsTrainingPendingForApproval; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            If xDataOpr Is Nothing Then objDataOperation = Nothing
        End Try

    End Function
    'Hemant (09 Feb 2022) -- End

    'Hemant (07 Mar 2022) -- Start            
    'ISSUE/ENHANCEMENT(NMB) : on group training and individual training request,System is allowing to save even when budget is not enough.
    Public Function GetTrainingRequestedEmployeeAllocationData(ByVal xCurrentPeriodId As Integer _
                                                              , ByVal xBudgetAllocationId As Integer _
                                                              , ByVal xTrainingCostCenterAllocationID As Integer _
                                                              , ByVal lstEmployeeIds As List(Of String)) As DataTable
        Dim dtTable As New DataTable
        Dim StrQ As String = String.Empty
        Dim dsList As DataSet = Nothing
        Dim strTable As String = ""
        Dim strUnkIdField As String = ""
        Dim strCodeField As String = ""
        Dim strNameField As String = ""
        Dim strTransferTable As String = ""
        Dim strTransferUnkIdField As String = ""
        Dim strTransferFilter As String = ""
        Dim strTransferCCTable As String = ""
        Dim strTransferCCUnkIdField As String = ""
        Dim objPeriod As New clsTraining_Calendar_Master

        Dim dtCurrEndDate As Date = DateAndTime.Now
        Try
            objDataOperation = New clsDataOperation

            Dim dsCombo As DataSet = objPeriod.GetList("List", " trtraining_calendar_master.calendarunkid IN ( " & xCurrentPeriodId & ") ")
            Dim dr() As DataRow
            If xCurrentPeriodId > 0 Then
                dr = dsCombo.Tables(0).Select("calendarunkid = " & xCurrentPeriodId & " ")
                If dr.Length > 0 Then
                    dtCurrEndDate = eZeeDate.convertDate(dr(0).Item("edate").ToString)
                End If
            Else
                xCurrentPeriodId = -999
            End If

            Select Case xBudgetAllocationId

                Case enAllocation.BRANCH

                    strTable = "hrstation_master"
                    strUnkIdField = "stationunkid"
                    strCodeField = "code"
                    strNameField = "name"
                    strTransferTable = "hremployee_transfer_tran"
                    strTransferUnkIdField = "stationunkid"

                Case enAllocation.DEPARTMENT_GROUP

                    strTable = "hrdepartment_group_master"
                    strUnkIdField = "deptgroupunkid"
                    strCodeField = "code"
                    strNameField = "name"
                    strTransferTable = "hremployee_transfer_tran"
                    strTransferUnkIdField = "deptgroupunkid"

                Case enAllocation.DEPARTMENT

                    strTable = "hrdepartment_master"
                    strUnkIdField = "departmentunkid"
                    strCodeField = "code"
                    strNameField = "name"
                    strTransferTable = "hremployee_transfer_tran"
                    strTransferUnkIdField = "departmentunkid"

                Case enAllocation.SECTION_GROUP

                    strTable = "hrsectiongroup_master"
                    strUnkIdField = "sectiongroupunkid"
                    strCodeField = "code"
                    strNameField = "name"
                    strTransferTable = "hremployee_transfer_tran"
                    strTransferUnkIdField = "sectiongroupunkid"

                Case enAllocation.SECTION

                    strTable = "hrsection_master"
                    strUnkIdField = "sectionunkid"
                    strCodeField = "code"
                    strNameField = "name"
                    strTransferTable = "hremployee_transfer_tran"
                    strTransferUnkIdField = "sectionunkid"

                Case enAllocation.UNIT_GROUP

                    strTable = "hrunitgroup_master"
                    strUnkIdField = "unitgroupunkid"
                    strCodeField = "code"
                    strNameField = "name"
                    strTransferTable = "hremployee_transfer_tran"
                    strTransferUnkIdField = "unitgroupunkid"

                Case enAllocation.UNIT

                    strTable = "hrunit_master"
                    strUnkIdField = "unitunkid"
                    strCodeField = "code"
                    strNameField = "name"
                    strTransferTable = "hremployee_transfer_tran"
                    strTransferUnkIdField = "unitunkid"

                Case enAllocation.TEAM

                    strTable = "hrteam_master"
                    strUnkIdField = "teamunkid"
                    strCodeField = "code"
                    strNameField = "name"
                    strTransferTable = "hremployee_transfer_tran"
                    strTransferUnkIdField = "teamunkid"

                Case enAllocation.JOB_GROUP

                    strTable = "hrjobgroup_master"
                    strUnkIdField = "jobgroupunkid"
                    strCodeField = "code"
                    strNameField = "name"
                    strTransferTable = "hremployee_categorization_tran"
                    strTransferUnkIdField = "jobgroupunkid"

                Case enAllocation.JOBS

                    strTable = "hrjob_master"
                    strUnkIdField = "jobunkid"
                    strCodeField = "job_code"
                    strNameField = "job_name"
                    strTransferTable = "hremployee_categorization_tran"
                    strTransferUnkIdField = "jobunkid"

                Case enAllocation.CLASS_GROUP

                    strTable = "hrclassgroup_master"
                    strUnkIdField = "classgroupunkid"
                    strCodeField = "code"
                    strNameField = "name"
                    strTransferTable = "hremployee_transfer_tran"
                    strTransferUnkIdField = "classgroupunkid"

                Case enAllocation.CLASSES

                    strTable = "hrclasses_master"
                    strUnkIdField = "classesunkid"
                    strCodeField = "code"
                    strNameField = "name"
                    strTransferTable = "hremployee_transfer_tran"
                    strTransferUnkIdField = "classunkid"

                Case enAllocation.COST_CENTER

                    strTable = "prcostcenter_master"
                    strUnkIdField = "costcenterunkid"
                    strCodeField = "customcode"
                    strNameField = "costcentername"
                    strTransferTable = "hremployee_cctranhead_tran"
                    strTransferUnkIdField = "cctranheadvalueid"
                    strTransferFilter = " AND istransactionhead = 0 "
            End Select

            If xTrainingCostCenterAllocationID > 0 Then

                Select Case xTrainingCostCenterAllocationID

                    Case enAllocation.BRANCH

                        strTransferCCTable = "hremployee_transfer_tran"
                        strTransferCCUnkIdField = "stationunkid"

                    Case enAllocation.DEPARTMENT_GROUP

                        strTransferCCTable = "hremployee_transfer_tran"
                        strTransferCCUnkIdField = "deptgroupunkid"

                    Case enAllocation.DEPARTMENT

                        strTransferCCTable = "hremployee_transfer_tran"
                        strTransferCCUnkIdField = "departmentunkid"

                    Case enAllocation.SECTION_GROUP

                        strTransferTable = "hremployee_transfer_tran"
                        strTransferUnkIdField = "sectiongroupunkid"

                    Case enAllocation.SECTION

                        strTransferCCTable = "hremployee_transfer_tran"
                        strTransferCCUnkIdField = "sectionunkid"

                    Case enAllocation.UNIT_GROUP

                        strTransferCCTable = "hremployee_transfer_tran"
                        strTransferCCUnkIdField = "unitgroupunkid"

                    Case enAllocation.UNIT

                        strTransferCCTable = "hremployee_transfer_tran"
                        strTransferCCUnkIdField = "unitunkid"

                    Case enAllocation.TEAM

                        strTransferCCTable = "hremployee_transfer_tran"
                        strTransferCCUnkIdField = "teamunkid"

                    Case enAllocation.JOB_GROUP

                        strTransferCCTable = "hremployee_categorization_tran"
                        strTransferCCUnkIdField = "jobgroupunkid"

                    Case enAllocation.JOBS

                        strTransferCCTable = "hremployee_categorization_tran"
                        strTransferCCUnkIdField = "jobunkid"

                    Case enAllocation.CLASS_GROUP

                        strTransferCCTable = "hremployee_transfer_tran"
                        strTransferCCUnkIdField = "classgroupunkid"

                    Case enAllocation.CLASSES

                        strTransferCCTable = "hremployee_transfer_tran"
                        strTransferCCUnkIdField = "classunkid"

                    Case enAllocation.COST_CENTER

                        strTransferCCTable = "hremployee_cctranhead_tran"
                        strTransferCCUnkIdField = "cctranheadvalueid"

                End Select

                If lstEmployeeIds IsNot Nothing AndAlso lstEmployeeIds.Count > 0 Then
                    StrQ &= "IF OBJECT_ID('tempdb..#EmpList') IS NOT NULL " & _
                               "DROP TABLE #EmpList "
                    StrQ &= "DECLARE @words NVARCHAR (MAX) " & _
                            "SET @words = '" & String.Join(",", lstEmployeeIds.ToArray) & "' " & _
                            "DECLARE @split TABLE(word VARCHAR(MAX)) " & _
                            "DECLARE @word VARCHAR(64), @start INT, @end INT, @stop INT " & _
                            "SELECT " & _
                                "@words = @words + ',' " & _
                               ",@start = 1 " & _
                               ",@stop = LEN(@words) + 1 " & _
                            "WHILE @start < @stop begin " & _
                            "SELECT " & _
                                "@end = CHARINDEX(',', @words, @start) " & _
                               ",@word = RTRIM(LTRIM(SUBSTRING(@words, @start, @end - @start))) " & _
                               ",@start = @end + 1 " & _
                            "INSERT @split " & _
                                "VALUES (@word) " & _
                            "END " & _
                              "SELECT " & _
                                "* INTO #EmpList from ( " & _
                            "SELECT " & _
                                "CAST(word AS INT) " & _
                                 "AS empid " & _
                                 ",1 AS ROWNO " & _
                            "FROM @split " & _
                             ") AS A " & _
                            "WHERE A.ROWNO = 1 "
                End If

                StrQ &= "SELECT  " & _
                             xBudgetAllocationId & " AS allocationid " & _
                             ", A.allocationtranunkid " & _
                             ", A.employeeunkid " & _
                             ", A.allocationtrancode " & _
                             ", A.allocationtranname " & _
                             ", ISNULL(hremployee_master.employeecode, '') + ' - ' + ISNULL(hremployee_master.firstname, '') + ' ' + ISNULL(hremployee_master.othername,'')  + ' ' + ISNULL(hremployee_master.surname,'') employee " & _
                        "INTO #TableEmpCCCurr " & _
                        "FROM hremployee_master "
                If lstEmployeeIds IsNot Nothing AndAlso lstEmployeeIds.Count > 0 Then
                    StrQ &= " JOIN #EmpList on #EmpList.empid = hremployee_master.employeeunkid "
                End If
                StrQ &= "LEFT JOIN " & _
                            "( " & _
                                "SELECT " & strTransferTable & ".employeeunkid " & _
                                     ", " & strTransferUnkIdField & " AS allocationtranunkid " & _
                                     ", " & strTable & "." & strCodeField & " AS allocationtrancode " & _
                                     ", " & strTable & "." & strNameField & " AS allocationtranname " & _
                                     ", ROW_NUMBER() OVER (PARTITION BY " & strTransferTable & ".employeeunkid ORDER BY " & strTransferTable & ".effectivedate DESC) AS ROWNO " & _
                                "FROM " & strTransferTable & " " & _
                                    "JOIN hremployee_master ON hremployee_master.employeeunkid = " & strTransferTable & ".employeeunkid "

            If lstEmployeeIds IsNot Nothing AndAlso lstEmployeeIds.Count > 0 Then
                StrQ &= " JOIN #EmpList on #EmpList.empid = hremployee_master.employeeunkid "
            End If

            StrQ &= "LEFT JOIN " & strTable & " ON " & strTransferTable & "." & strTransferUnkIdField & " = " & strTable & "." & strUnkIdField & " " & _
                            "WHERE " & strTransferTable & ".isvoid = 0 " & _
                                  strTransferFilter

            StrQ &= "AND CONVERT(CHAR(8), " & strTransferTable & ".effectivedate, 112) <= @CurrEnddate " & _
                        ") AS A " & _
                            "ON A.employeeunkid = hremployee_master.employeeunkid " & _
                    "WHERE A.ROWNO = 1 "


            If xTrainingCostCenterAllocationID > 0 Then

                StrQ &= "SELECT B.employeeunkid " & _
                             ", trdept_costcenter_mapping.allocationtranunkid " & _
                             ", B.mappedallocationtranunkid " & _
                             ", trdept_costcenter_mapping.costcenterunkid " & _
                             ", prcostcenter_master.customcode AS cccode " & _
                             ", prcostcenter_master.costcentername AS ccname " & _
                        "INTO #CCMappingCurr " & _
                        "FROM trdept_costcenter_mapping " & _
                            "LEFT JOIN " & _
                            "( " & _
                                "SELECT " & strTransferCCTable & ".employeeunkid " & _
                                     ", " & strTransferCCTable & "." & strTransferCCUnkIdField & " AS mappedallocationtranunkid " & _
                                     ", ROW_NUMBER() OVER (PARTITION BY " & strTransferCCTable & ".employeeunkid ORDER BY effectivedate DESC ) AS ROWNO " & _
                                "FROM " & strTransferCCTable & " " & _
                                    "JOIN hremployee_master " & _
                                        "ON hremployee_master.employeeunkid = " & strTransferCCTable & ".employeeunkid " & _
                                "WHERE isvoid = 0 " & _
                                      "AND CONVERT(CHAR(8), " & strTransferCCTable & ".effectivedate, 112) <= @CurrEnddate " & _
                            ") AS B ON B.mappedallocationtranunkid = trdept_costcenter_mapping.allocationtranunkid " & _
                            "LEFT JOIN prcostcenter_master ON trdept_costcenter_mapping.costcenterunkid = prcostcenter_master.costcenterunkid " & _
                        "WHERE trdept_costcenter_mapping.isvoid = 0 " & _
                              "AND trdept_costcenter_mapping.costcenterunkid > 0 " & _
                              "AND trdept_costcenter_mapping.allocationid = " & xTrainingCostCenterAllocationID & " " & _
                              "AND B.ROWNO = 1 "

                StrQ &= "UPDATE #TableEmpCCCurr " & _
                        "SET allocationtranunkid = #CCMappingCurr.costcenterunkid " & _
                          ", allocationtrancode = #CCMappingCurr.cccode " & _
                          ", allocationtranname = #CCMappingCurr.ccname " & _
                        "FROM #CCMappingCurr " & _
                        "WHERE #CCMappingCurr.employeeunkid = #TableEmpCCCurr.employeeunkid "

            End If

            StrQ &= " SELECT * FROM #TableEmpCCCurr "
            StrQ &= " DROP TABLE #TableEmpCCCurr "
            StrQ &= " DROP TABLE #CCMappingCurr "

            objDataOperation.AddParameter("@CurrEnddate", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, eZeeDate.convertDate(dtCurrEndDate))

            dsList = objDataOperation.ExecQuery(StrQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                Throw New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
            End If

            dtTable = dsList.Tables(0)

            End If
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: GetTrainingRequestedEmployeeAllocationData; Module Name: " & mstrModuleName)
        Finally
            objPeriod = Nothing
        End Try
        Return dtTable
    End Function

    Public Function getNextRefNo(Optional ByVal xDataOp As clsDataOperation = Nothing) As Integer
        Dim dsList As New DataSet
        Dim StrQ As String = String.Empty
        Dim intRefNo As Integer = 1

        If xDataOp Is Nothing Then
            objDataOperation = New clsDataOperation
        Else
            objDataOperation = xDataOp
        End If
        objDataOperation.ClearParameters()

        Try

            StrQ = "SELECT ISNULL(MAX(CAST(refno AS INT)), 0) + 1 AS NextRefNo " & _
                    "FROM trtraining_request_master " & _
                    "WHERE trtraining_request_master.isvoid = 0 " & _
                          "AND refno <> '' "

            dsList = objDataOperation.ExecQuery(StrQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                Throw New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
            End If

            If dsList.Tables(0).Rows.Count > 0 Then
                intRefNo = CInt(dsList.Tables(0).Rows(0).Item(0))
            End If

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: getNextRefNo; Module Name: " & mstrModuleName)
        Finally
            If xDataOp Is Nothing Then objDataOperation = Nothing
        End Try
        Return intRefNo
    End Function
    'Hemant (07 Mar 2022) -- End


    'Hemant (04 Aug 2022) -- Start            
    'ENHANCEMENT(NMB) : AC2-719 - Training request form - new report
    Public Function getTrainingRequesterUserList(ByVal strListName As String, _
                                                Optional ByVal mblnFlag As Boolean = False, _
                                                Optional ByVal intCompanyId As Integer = -1 _
                                                ) As DataSet


        Dim dsList As DataSet = Nothing
        Dim StrQ As String = ""
        Dim exForce As Exception
        Dim objDataOperation As clsDataOperation


        objDataOperation = New clsDataOperation


        objDataOperation.ClearParameters()

        Try


            StrQ = "SELECT userunkid into #USRLIST from ( " & _
                        "select DISTINCT hrmsConfiguration..cfuser_master.userunkid from trtraining_request_master " & _
                        "LEFT JOIN  hrmsConfiguration..cfuser_master on  hrmsConfiguration..cfuser_master.employeeunkid = trtraining_request_master.createloginemployeeunkid " & _
                        "where createloginemployeeunkid > 0 AND companyunkid = " & intCompanyId & "  " & _
                        "UNION " & _
                        "select DISTINCT hrmsConfiguration..cfuser_master.userunkid from trtraining_request_master " & _
                        "LEFT JOIN  hrmsConfiguration..cfuser_master on  hrmsConfiguration..cfuser_master.userunkid = trtraining_request_master.createuserunkid " & _
                        "where  createuserunkid > 0 ) AS A "

            If mblnFlag = True Then
                StrQ &= "SELECT 0 As userunkid , @Select As  name  UNION "
            End If

            StrQ &= "SELECT " & _
                       "    hrmsConfiguration..cfuser_master.userunkid " & _
                       ",   ISNULL(username, '') AS Name " & _
                    "FROM hrmsConfiguration..cfuser_master " & _
                    "JOIN #USRLIST ON #USRLIST.userunkid = hrmsConfiguration..cfuser_master.userunkid " & _
                    "WHERE hrmsConfiguration..cfuser_master.isactive = 1 "

            StrQ &= "DROP TABLE   #USRLIST "



            objDataOperation.AddParameter("@Select", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 1003, "Select"))

            dsList = objDataOperation.ExecQuery(StrQ, strListName)

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: getNewComboList; Module Name: " & mstrModuleName)
        Finally
        End Try
        Return dsList
    End Function
    'Hemant (04 Aug 2022) -- End

    'Hemant (12 Oct 2022) -- Start
    'ENHANCEMENT(NMB) :  AC2-952 - As a user, I want to have a document attachment option on the group training request page
    Public Function getNextGroupTraningUnkId(Optional ByVal xDataOp As clsDataOperation = Nothing) As Integer
        Dim dsList As New DataSet
        Dim StrQ As String = String.Empty
        Dim intGroupTraningUnkId As Integer = 1

        If xDataOp Is Nothing Then
            objDataOperation = New clsDataOperation
        Else
            objDataOperation = xDataOp
        End If
        objDataOperation.ClearParameters()

        Try

            StrQ = "SELECT ISNULL(MAX(grouptrainingrequestunkid), 0) + 1 AS NextGroupTraningUnkId " & _
                    "FROM trtraining_request_master "

            dsList = objDataOperation.ExecQuery(StrQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                Throw New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
            End If

            If dsList.Tables(0).Rows.Count > 0 Then
                intGroupTraningUnkId = CInt(dsList.Tables(0).Rows(0).Item(0))
            End If

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: getNextGroupTraningUnkId; Module Name: " & mstrModuleName)
        Finally
            If xDataOp Is Nothing Then objDataOperation = Nothing
        End Try
        Return intGroupTraningUnkId
    End Function
    'Hemant (12 Oct 2022) -- End


    '<Language> This Auto Generated Text Please Do Not Modify it.
#Region " Language & UI Settings "
    Public Shared Sub SetMessages()
        Try
            Language.setMessage(mstrModuleName, 1, "Pending")
            Language.setMessage(mstrModuleName, 2, "Approved")
            Language.setMessage(mstrModuleName, 3, "Rejected")
            Language.setMessage(mstrModuleName, 4, "Yes")
            Language.setMessage(mstrModuleName, 5, "No")
            Language.setMessage(mstrModuleName, 6, "On Hold")
            Language.setMessage(mstrModuleName, 7, "Postponed")
            Language.setMessage(mstrModuleName, 8, "Cancel")
            Language.setMessage(mstrModuleName, 9, "Sorry, This Training is already defined. Please define new Training.")
            Language.setMessage(mstrModuleName, 10, "#EmployeeName#")
            Language.setMessage(mstrModuleName, 11, "#ApplicationDate#")
            Language.setMessage(mstrModuleName, 12, "Notification for Approving Training Request")
            Language.setMessage(mstrModuleName, 13, "#Remark#")
            Language.setMessage(mstrModuleName, 14, "Sorry, No approver is available for Selected Employee. Please assign approver for approval")
            Language.setMessage(mstrModuleName, 15, "Completed")
            Language.setMessage(mstrModuleName, 16, "Notification for Approving Completed Training")
            Language.setMessage(mstrModuleName, 17, "This is the notification for approving Completed Training")
            Language.setMessage(mstrModuleName, 18, "This is to inform you that the application you have applied for Completed Training")
            Language.setMessage(mstrModuleName, 19, "Dear")
            Language.setMessage(mstrModuleName, 20, "#TrainingName#")
            Language.setMessage(mstrModuleName, 21, "Please click on the following link to submit Pre-Training Evaluation Form.")
            Language.setMessage(mstrModuleName, 22, "Please click on the following link to submit Post-Training Evaluation Form.")
            Language.setMessage(mstrModuleName, 23, " Completed Training Status Notification")
            Language.setMessage(mstrModuleName, 24, "This is the notification for approving Training Request")
            Language.setMessage(mstrModuleName, 25, " with Training :")
            Language.setMessage(mstrModuleName, 26, " of Employee")
            Language.setMessage(mstrModuleName, 27, " on application date :")
            Language.setMessage(mstrModuleName, 28, "Please click on the following link to Confirm/Reject Training Request.")
            Language.setMessage(mstrModuleName, 29, "Please click on the following link to Confirm/Reject Enrollment.")
            Language.setMessage(mstrModuleName, 30, " has been")
            Language.setMessage(mstrModuleName, 31, "This is to inform you that the application you have applied for Training Request")
            Language.setMessage(mstrModuleName, 32, "Please click on the following link to Confirm/Reject Completion Training.")
            Language.setMessage(mstrModuleName, 33, "Remarks:")
            Language.setMessage(mstrModuleName, 34, "Training Request Status Notification")
            Language.setMessage(mstrModuleName, 35, "Please refer to the comments below for the same.")
            Language.setMessage(mstrModuleName, 36, "Training Evaluation Feedback Notification")
            Language.setMessage(mstrModuleName, 37, "This is to inform you that you have Completed Training :")
            Language.setMessage(mstrModuleName, 38, "Please submit Days After Training Evaluation Feedback Form on or after Date :")
            Language.setMessage(mstrModuleName, 39, "#SendMailDate#")
            Language.setMessage(mstrModuleName, 40, "Regards")
            Language.setMessage(mstrModuleName, 41, "Notification for Training Request")
            Language.setMessage(mstrModuleName, 42, "This is to inform you that your subordinate, employee")
            Language.setMessage(mstrModuleName, 43, "#EmployeeName#")
            Language.setMessage(mstrModuleName, 44, " has been selected to attend")
            Language.setMessage(mstrModuleName, 45, "#TrainingName#")
            Language.setMessage(mstrModuleName, 46, " starting  from")
            Language.setMessage(mstrModuleName, 47, "#StartDate#")
            Language.setMessage(mstrModuleName, 48, " to")
            Language.setMessage(mstrModuleName, 49, "#EndDate#")
            Language.setMessage(mstrModuleName, 50, "Regards")
            Language.setMessage(mstrModuleName, 51, "Select")
            Language.setMessage(mstrModuleName, 52, "Training Completion Notification")
            Language.setMessage(mstrModuleName, 53, "#TrainingName#")
            Language.setMessage(mstrModuleName, 54, "This is to inform you that you have not mark as Completed For Training :")
            Language.setMessage(mstrModuleName, 55, "Please Mark as Completed before Expiry Date :")
            Language.setMessage(mstrModuleName, 56, "#CompleteExpireDate#")
            Language.setMessage(mstrModuleName, 57, "Regards")
            Language.setMessage(mstrModuleName, 58, "Notification for Completed Training")
            Language.setMessage(mstrModuleName, 59, "#TrainingName#")
            Language.setMessage(mstrModuleName, 60, "This is to notify you that,")
            Language.setMessage(mstrModuleName, 61, "Employee")
            Language.setMessage(mstrModuleName, 62, "#EmployeeName#")
            Language.setMessage(mstrModuleName, 63, " has completed training on")
			Language.setMessage(mstrModuleName, 64, "Sorry, selected employee(s) not mapped to any approver for that cost amount:Employee Code -")
			Language.setMessage(mstrModuleName, 65, "Regards")
			Language.setMessage(mstrModuleName, 66, "#TrainingName#")
            Language.setMessage(mstrModuleName, 67, "This is to inform you that")
            Language.setMessage(mstrModuleName, 68, " Training requested by")
            Language.setMessage(mstrModuleName, 69, "#RequesterName#")
            Language.setMessage(mstrModuleName, 70, ",starting on")
            Language.setMessage(mstrModuleName, 71, "#StartDate#")
            Language.setMessage(mstrModuleName, 72, "and ending on")
            Language.setMessage(mstrModuleName, 73, "#EndDate#")
            Language.setMessage(mstrModuleName, 74, "has been approved. For more details please login in ARUTI and generate the necessary report")
            Language.setMessage(mstrModuleName, 75, "Regards")
            Language.setMessage(mstrModuleName, 76, "TRAINING APPROVAL")

        Catch Ex As Exception
            DisplayError.Show("-1", Ex.Message, "SetMessages", mstrModuleName)
        End Try
    End Sub
#End Region 'Language & UI Settings
    '</Language>
End Class
