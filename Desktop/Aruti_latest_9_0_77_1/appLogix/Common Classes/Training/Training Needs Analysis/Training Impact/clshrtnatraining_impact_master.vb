﻿'************************************************************************************************************************************
'Class Name : clshrtnatraining_impact_master.vb
'Purpose    :
'Date       :24/02/2012
'Written By :Pinkal Jariwala
'Modified   :
'************************************************************************************************************************************

Imports eZeeCommonLib
''' <summary>
''' Purpose: 
''' Developer: Pinkal Jariwala
''' </summary>
Public Class clshrtnatraining_impact_master
    Private Shared ReadOnly mstrModuleName As String = "clshrtnatraining_impact_master"
    Dim objDataOperation As clsDataOperation
    Dim mstrMessage As String = ""

#Region " Private variables "
    Private mintImpactunkid As Integer
    Private mintSchedulingid As Integer
    Private mintCourseunkid As Integer
    Private mintEmployeeunkid As Integer
    Private mintManagerunkid As Integer
    Private mstrSelf_Remark As String = String.Empty
    Private mblnIscomplete As Boolean
    Private mintUserunkid As Integer
    Private mblnIsvoid As Boolean
    Private mintVoiduserunkid As Integer
    Private mdtVoiddatetime As Date
    Private mstrVoidreason As String = String.Empty
    Private dsImpactKRA As DataSet
    Private dsImpactDevelopment As DataSet
    Private dsImpactObjective As DataSet
    Private dsImpactFeedback As DataSet

    Dim objImpactKRA As New clshrtnatraining_impact_kra
    Dim objImpactDevelopment As New clshrtnatraining_impact_development
    Dim objImpactObjective As New clshrtnatraining_impact_objective
    Dim objImpactFeedback As New clshrtnatraining_impact_feedback

#End Region

#Region " Properties "

'S.SANDEEP [28-May-2018] -- START 
'ISSUE/ENHANCEMENT : {Audit Trails} 
Private mstrFormName As String = String.Empty
Public WriteOnly Property _FormName() As String 
Set(ByVal value As String) 
mstrFormName = value 
End Set 
End Property 
 
Private mstrClientIP As String = ""
Public WriteOnly Property _ClientIP() As String 
Set(ByVal value As String) 
mstrClientIP = value 
End Set 
End Property 
 
Private mstrHostName As String = ""
Public WriteOnly Property _HostName() As String 
Set(ByVal value As String) 
mstrHostName = value 
End Set 
End Property 
 
Private mblnIsWeb As Boolean = False
Public WriteOnly Property _FromWeb() As Boolean 
Set(ByVal value As Boolean) 
mblnIsWeb = value 
End Set 
End Property 
 
    Private mintAuditUserId As Integer = 0
    Public WriteOnly Property _AuditUserId() As Integer
        Set(ByVal value As Integer)
            mintAuditUserId = value
        End Set
    End Property

    Private mintCompanyUnkid As Integer = 0
    Public WriteOnly Property _CompanyUnkid() As Integer
        Set(ByVal value As Integer)
            mintCompanyUnkid = value
        End Set
    End Property
 
Private mdtAuditDate As DateTime = Now
Public WriteOnly Property _AuditDate() As DateTime 
Set(ByVal value As DateTime) 
mdtAuditDate = value 
End Set 
End Property 
 
Private xDataOpr As clsDataOperation = Nothing
Public Property _xDataOpr() As clsDataOperation 
Get 
Return xDataOpr 
End Get 
Set(ByVal value As clsDataOperation) 
xDataOpr = value 
End Set 
End Property 
 
Private mintLoginEmployeeunkid As Integer = 0
Public Property _LoginEmployeeunkid() As Integer 
Get 
Return mintLoginEmployeeunkid 
End Get 
Set(ByVal value As Integer) 
mintLoginEmployeeunkid = value 
End Set 
End Property 
 
Private mintVoidlogingemployeeunkid As Integer=0
Public Property _Voidloginemployeeunkid() As Integer 
Get 
Return mintVoidlogingemployeeunkid 
End Get 
Set(ByVal value As Integer) 
mintVoidlogingemployeeunkid = value 
End Set 
End Property 
'S.SANDEEP [28-May-2018] -- END 


    Public ReadOnly Property _Message() As String
        Get
            Return mstrMessage
        End Get
    End Property

    ''' <summary>
    ''' Purpose: Get or Set impactunkid
    ''' Modify By: Pinkal Jariwala
    ''' </summary>
    Public Property _Impactunkid() As Integer
        Get
            Return mintImpactunkid
        End Get
        Set(ByVal value As Integer)
            mintImpactunkid = value
            Call GetData()
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set Schedulingid
    ''' Modify By: Pinkal Jariwala
    ''' </summary>
    Public Property _Schedulingid() As Integer
        Get
            Return mintSchedulingid
        End Get
        Set(ByVal value As Integer)
            mintSchedulingid = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set courseunkid
    ''' Modify By: Pinkal Jariwala
    ''' </summary>
    Public Property _Courseunkid() As Integer
        Get
            Return mintCourseunkid
        End Get
        Set(ByVal value As Integer)
            mintCourseunkid = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set employeeunkid
    ''' Modify By: Pinkal Jariwala
    ''' </summary>
    Public Property _Employeeunkid() As Integer
        Get
            Return mintEmployeeunkid
        End Get
        Set(ByVal value As Integer)
            mintEmployeeunkid = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set managerunkid
    ''' Modify By: Pinkal Jariwala
    ''' </summary>
    Public Property _Managerunkid() As Integer
        Get
            Return mintManagerunkid
        End Get
        Set(ByVal value As Integer)
            mintManagerunkid = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set Self Remark
    ''' Modify By: Pinkal Jariwala
    ''' </summary>
    Public Property _Self_Remark() As String
        Get
            Return mstrSelf_Remark
        End Get
        Set(ByVal value As String)
            mstrSelf_Remark = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set iscomplete
    ''' Modify By: Pinkal Jariwala
    ''' </summary>
    Public Property _Iscomplete() As Boolean
        Get
            Return mblnIscomplete
        End Get
        Set(ByVal value As Boolean)
            mblnIscomplete = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set userunkid
    ''' Modify By: Pinkal Jariwala
    ''' </summary>
    Public Property _Userunkid() As Integer
        Get
            Return mintUserunkid
        End Get
        Set(ByVal value As Integer)
            mintUserunkid = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set isvoid
    ''' Modify By: Pinkal Jariwala
    ''' </summary>
    Public Property _Isvoid() As Boolean
        Get
            Return mblnIsvoid
        End Get
        Set(ByVal value As Boolean)
            mblnIsvoid = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set voiduserunkid
    ''' Modify By: Pinkal Jariwala
    ''' </summary>
    Public Property _Voiduserunkid() As Integer
        Get
            Return mintVoiduserunkid
        End Get
        Set(ByVal value As Integer)
            mintVoiduserunkid = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set voiddatetime
    ''' Modify By: Pinkal Jariwala
    ''' </summary>
    Public Property _Voiddatetime() As Date
        Get
            Return mdtVoiddatetime
        End Get
        Set(ByVal value As Date)
            mdtVoiddatetime = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set voidreason
    ''' Modify By: Pinkal Jariwala
    ''' </summary>
    Public Property _Voidreason() As String
        Get
            Return mstrVoidreason
        End Get
        Set(ByVal value As String)
            mstrVoidreason = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set dsImpactKRA
    ''' Modify By: Pinkal Jariwala
    ''' </summary>
    Public Property _dsKRAList() As DataSet
        Get
            Return dsImpactKRA
        End Get
        Set(ByVal value As DataSet)
            dsImpactKRA = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set dsImpactDevelopment
    ''' Modify By: Pinkal Jariwala
    ''' </summary>
    Public Property _dsDevelopmentList() As DataSet
        Get
            Return dsImpactDevelopment
        End Get
        Set(ByVal value As DataSet)
            dsImpactDevelopment = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set dsImpactObjective
    ''' Modify By: Pinkal Jariwala
    ''' </summary>
    Public Property _dsObjectiveList() As DataSet
        Get
            Return dsImpactObjective
        End Get
        Set(ByVal value As DataSet)
            dsImpactObjective = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set dsImpactFeedback
    ''' Modify By: Pinkal Jariwala
    ''' </summary>
    Public Property _dsFeedBackList() As DataSet
        Get
            Return dsImpactFeedback
        End Get
        Set(ByVal value As DataSet)
            dsImpactFeedback = value
        End Set
    End Property

#End Region

    ''' <summary>
    ''' Modify By: Pinkal Jariwala
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>
    Public Sub GetData()
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        objDataOperation = New clsDataOperation

        Try
            strQ = "SELECT " & _
              "  impactunkid " & _
              ",  trainingschedulingunkid " & _
              ", courseunkid " & _
              ", employeeunkid " & _
              ", managerunkid " & _
              ", self_remark " & _
              ", iscomplete " & _
              ", userunkid " & _
              ", isvoid " & _
              ", voiduserunkid " & _
              ", voiddatetime " & _
              ", voidreason " & _
             "FROM hrtnatraining_impact_master " & _
             "WHERE impactunkid = @impactunkid "

            objDataOperation.AddParameter("@impactunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintImpactunkid.ToString)

            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            For Each dtRow As DataRow In dsList.Tables(0).Rows
                mintImpactunkid = CInt(dtRow.Item("impactunkid"))
                mintSchedulingid = CInt(dtRow.Item("trainingschedulingunkid"))
                mintCourseunkid = CInt(dtRow.Item("courseunkid"))
                mintEmployeeunkid = CInt(dtRow.Item("employeeunkid"))
                mintManagerunkid = CInt(dtRow.Item("managerunkid"))
                mstrSelf_Remark = CStr(dtRow.Item("self_remark"))
                mblnIscomplete = CBool(dtRow.Item("iscomplete"))
                mintUserunkid = CInt(dtRow.Item("userunkid"))
                mblnIsvoid = CBool(dtRow.Item("isvoid"))
                mintVoiduserunkid = CInt(dtRow.Item("voiduserunkid"))

                If Not IsDBNull(dtRow.Item("voiddatetime")) Then
                    mdtVoiddatetime = dtRow.Item("voiddatetime")
                End If
                mstrVoidreason = dtRow.Item("voidreason").ToString
                Exit For
            Next

            dsImpactKRA = objImpactKRA.GetList("KRA", True, -1, mintImpactunkid)
            dsImpactDevelopment = objImpactDevelopment.GetList("Development", True, -1, mintImpactunkid)
            dsImpactObjective = objImpactObjective.GetList("Objective", True, -1, mintImpactunkid)
            dsImpactFeedback = objImpactFeedback.GetList("Feedback", True, -1, mintImpactunkid)

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: GetData; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
    End Sub

    ''' <summary>
    ''' Modify By: Pinkal Jariwala
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>
    Public Function GetList(ByVal strTableName As String, Optional ByVal blnOnlyActive As Boolean = True) As DataSet
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        objDataOperation = New clsDataOperation

        Try
            strQ = "SELECT " & _
              "  impactunkid " & _
              ", hrtnatraining_impact_master.trainingschedulingunkid  " & _
              ", hrtnatraining_impact_master.courseunkid " & _
              ", cfcommon_master.name AS course " & _
              ", hrtraining_scheduling.start_date " & _
              ", hrtraining_scheduling.end_date " & _
              ", hrtnatraining_impact_master.employeeunkid " & _
              ", ISNULL(emp.firstname,'') + ' ' + ISNULL(emp.surname,'') employee " & _
              ", hrtnatraining_impact_master.managerunkid " & _
              ", ISNULL(Mgr.firstname,'') + ' ' + ISNULL(Mgr.surname,'') Manager " & _
              ", self_remark " & _
              ", iscomplete " & _
              ", CASE WHEN iscomplete = 0 then @NotCompleted " & _
              "           WHEN iscomplete = 1 then @Completed " & _
              "  END as status " & _
              ", hrtnatraining_impact_master.userunkid " & _
              ", hrtnatraining_impact_master.isvoid " & _
              ", hrtnatraining_impact_master.voiduserunkid " & _
              ", hrtnatraining_impact_master.voiddatetime " & _
              ", hrtnatraining_impact_master.voidreason " & _
              " FROM hrtnatraining_impact_master " & _
              " JOIN hrtraining_scheduling on hrtraining_scheduling.trainingschedulingunkid = hrtnatraining_impact_master.trainingschedulingunkid " & _
              " JOIN cfcommon_master ON cfcommon_master.masterunkid = hrtnatraining_impact_master.courseunkid AND mastertype = " & clsCommon_Master.enCommonMaster.TRAINING_COURSEMASTER & " AND isactive = 1 " & _
              " JOIN hremployee_master emp ON hrtnatraining_impact_master.employeeunkid = emp.employeeunkid AND emp.isactive = 1 " & _
              " JOIN hremployee_master Mgr ON hrtnatraining_impact_master.managerunkid = mgr.employeeunkid AND mgr.isactive = 1 "

            If blnOnlyActive Then
                strQ &= " WHERE ISNULL(hrtnatraining_impact_master.isvoid,0) = 0 "
            End If


            objDataOperation.ClearParameters()
            objDataOperation.AddParameter("@NotCompleted", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 1, "Not Completed"))
            objDataOperation.AddParameter("@Completed", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 2, "Completed"))
            dsList = objDataOperation.ExecQuery(strQ, strTableName)


            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: GetList; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
        Return dsList
    End Function

    ''' <summary>
    ''' Modify By: Pinkal Jariwala
    ''' </summary>
    ''' <returns>Boolean</returns>
    ''' <purpose> INSERT INTO Database Table (hrtnatraining_impact_master) </purpose>
    Public Function Insert() As Boolean
        'If isExist(mstrName) Then
        '    mstrMessage = "<Message>"
        '    Return False
        'End If

        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        Dim objDataOperation As New clsDataOperation
        objDataOperation.BindTransaction()

        Try
            objDataOperation.ClearParameters()
            objDataOperation.AddParameter("@trainingschedulingunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintSchedulingid.ToString)
            objDataOperation.AddParameter("@courseunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintCourseunkid.ToString)
            objDataOperation.AddParameter("@employeeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintEmployeeunkid.ToString)
            objDataOperation.AddParameter("@managerunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintManagerunkid.ToString)
            objDataOperation.AddParameter("@self_remark", SqlDbType.NVarChar, 4000, mstrSelf_Remark.ToString)
            objDataOperation.AddParameter("@iscomplete", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIscomplete.ToString)
            objDataOperation.AddParameter("@userunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintUserunkid.ToString)
            objDataOperation.AddParameter("@isvoid", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsvoid.ToString)
            objDataOperation.AddParameter("@voiduserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintVoiduserunkid.ToString)
            objDataOperation.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, IIf(mdtVoiddatetime <> Nothing, mdtVoiddatetime, DBNull.Value))
            objDataOperation.AddParameter("@voidreason", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrVoidreason.ToString)

            strQ = "INSERT INTO hrtnatraining_impact_master ( " & _
              "  trainingschedulingunkid  " & _
              ", courseunkid " & _
              ", employeeunkid " & _
              ", managerunkid " & _
              ", self_remark " & _
              ", iscomplete " & _
              ", userunkid " & _
              ", isvoid " & _
              ", voiduserunkid " & _
              ", voiddatetime " & _
              ", voidreason" & _
            ") VALUES (" & _
              "  @trainingschedulingunkid  " & _
              ", @courseunkid " & _
              ", @employeeunkid " & _
              ", @managerunkid " & _
              ", @self_remark " & _
              ", @iscomplete " & _
              ", @userunkid " & _
              ", @isvoid " & _
              ", @voiduserunkid " & _
              ", @voiddatetime " & _
              ", @voidreason" & _
            "); SELECT @@identity"

            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            mintImpactunkid = dsList.Tables(0).Rows(0).Item(0)

            objImpactKRA._Impactunkid = mintImpactunkid
            objImpactKRA._KRAList = dsImpactKRA
            objImpactKRA._Userunkid = mintUserunkid
            With objImpactKRA
                ._FormName = mstrFormName
                ._LoginEmployeeunkid = mintLoginEmployeeunkid
                ._ClientIP = mstrClientIP
                ._HostName = mstrHostName
                ._FromWeb = mblnIsWeb
                ._AuditUserId = mintAuditUserId
._CompanyUnkid = mintCompanyUnkid
                ._AuditDate = mdtAuditDate
            End With
            If objImpactKRA.InsertUpdateDelete_KRA(objDataOperation) = False Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            objImpactDevelopment._Impactunkid = mintImpactunkid
            objImpactDevelopment._DevelopmentList = dsImpactDevelopment
            objImpactDevelopment._Userunkid = mintUserunkid
            With objImpactDevelopment
                ._FormName = mstrFormName
                ._LoginEmployeeunkid = mintLoginEmployeeunkid
                ._ClientIP = mstrClientIP
                ._HostName = mstrHostName
                ._FromWeb = mblnIsWeb
                ._AuditUserId = mintAuditUserId
._CompanyUnkid = mintCompanyUnkid
                ._AuditDate = mdtAuditDate
            End With
            If objImpactDevelopment.InsertUpdateDelete_Development(objDataOperation) = False Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            objImpactObjective._Impactunkid = mintImpactunkid
            objImpactObjective._ObjectiveList = dsImpactObjective
            objImpactObjective._Userunkid = mintUserunkid
            With objImpactObjective
                ._FormName = mstrFormName
                ._LoginEmployeeunkid = mintLoginEmployeeunkid
                ._ClientIP = mstrClientIP
                ._HostName = mstrHostName
                ._FromWeb = mblnIsWeb
                ._AuditUserId = mintAuditUserId
._CompanyUnkid = mintCompanyUnkid
                ._AuditDate = mdtAuditDate
            End With
            If objImpactObjective.InsertUpdateDelete_Objective(objDataOperation) = False Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            objImpactFeedback._Impactunkid = mintImpactunkid
            objImpactFeedback._FeedBackList = dsImpactFeedback
            objImpactFeedback._Userunkid = mintUserunkid
            With objImpactFeedback
                ._FormName = mstrFormName
                ._LoginEmployeeunkid = mintLoginEmployeeunkid
                ._ClientIP = mstrClientIP
                ._HostName = mstrHostName
                ._FromWeb = mblnIsWeb
                ._AuditUserId = mintAuditUserId
._CompanyUnkid = mintCompanyUnkid
                ._AuditDate = mdtAuditDate
            End With
            If objImpactFeedback.InsertUpdateDelete_FeedBack(objDataOperation) = False Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If


            objDataOperation.ReleaseTransaction(True)
            Return True
        Catch ex As Exception
            objDataOperation.ReleaseTransaction(False)
            Throw New Exception(ex.Message & "; Procedure Name: Insert; Module Name: " & mstrModuleName)
            Return False
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
    End Function

    ''' <summary>
    ''' Modify By: Pinkal Jariwala
    ''' </summary>
    ''' <returns>Boolean</returns>
    ''' <purpose> Update Database Table (hrtnatraining_impact_master) </purpose>
    Public Function Update() As Boolean
        'If isExist(mstrName, mintImpactunkid) Then
        '    mstrMessage = "<Message>"
        '    Return False
        'End If

        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        Dim objDataOperation As New clsDataOperation
        objDataOperation.BindTransaction()

        Try
            objDataOperation.ClearParameters()
            objDataOperation.AddParameter("@trainingschedulingunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintSchedulingid.ToString)
            objDataOperation.AddParameter("@impactunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintImpactunkid.ToString)
            objDataOperation.AddParameter("@courseunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintCourseunkid.ToString)
            objDataOperation.AddParameter("@employeeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintEmployeeunkid.ToString)
            objDataOperation.AddParameter("@managerunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintManagerunkid.ToString)
            objDataOperation.AddParameter("@self_remark", SqlDbType.NVarChar, 4000, mstrSelf_Remark.ToString)
            objDataOperation.AddParameter("@iscomplete", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIscomplete.ToString)
            objDataOperation.AddParameter("@userunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintUserunkid.ToString)
            objDataOperation.AddParameter("@isvoid", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsvoid.ToString)
            objDataOperation.AddParameter("@voiduserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintVoiduserunkid.ToString)
            objDataOperation.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, IIf(mdtVoiddatetime <> Nothing, mdtVoiddatetime, DBNull.Value))
            objDataOperation.AddParameter("@voidreason", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrVoidreason.ToString)

            strQ = "UPDATE hrtnatraining_impact_master SET " & _
              "  trainingschedulingunkid = @trainingschedulingunkid  " & _
              ",  courseunkid = @courseunkid" & _
              ", employeeunkid = @employeeunkid" & _
              ", managerunkid = @managerunkid  " & _
              ", self_remark  = @self_remark " & _
              ", iscomplete = @iscomplete" & _
              ", userunkid = @userunkid" & _
              ", isvoid = @isvoid" & _
              ", voiduserunkid = @voiduserunkid" & _
              ", voiddatetime = @voiddatetime" & _
              ", voidreason = @voidreason " & _
            "WHERE impactunkid = @impactunkid "

            Call objDataOperation.ExecNonQuery(strQ)

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            objImpactKRA._Impactunkid = mintImpactunkid
            objImpactKRA._KRAList = dsImpactKRA
            objImpactKRA._Userunkid = mintUserunkid
            With objImpactKRA
                ._FormName = mstrFormName
                ._LoginEmployeeunkid = mintLoginEmployeeunkid
                ._ClientIP = mstrClientIP
                ._HostName = mstrHostName
                ._FromWeb = mblnIsWeb
                ._AuditUserId = mintAuditUserId
._CompanyUnkid = mintCompanyUnkid
                ._AuditDate = mdtAuditDate
            End With
            If objImpactKRA.InsertUpdateDelete_KRA(objDataOperation) = False Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            objImpactDevelopment._Impactunkid = mintImpactunkid
            objImpactDevelopment._DevelopmentList = dsImpactDevelopment
            objImpactDevelopment._Userunkid = mintUserunkid
            With objImpactDevelopment
                ._FormName = mstrFormName
                ._LoginEmployeeunkid = mintLoginEmployeeunkid
                ._ClientIP = mstrClientIP
                ._HostName = mstrHostName
                ._FromWeb = mblnIsWeb
                ._AuditUserId = mintAuditUserId
._CompanyUnkid = mintCompanyUnkid
                ._AuditDate = mdtAuditDate
            End With
            If objImpactDevelopment.InsertUpdateDelete_Development(objDataOperation) = False Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            objImpactObjective._Impactunkid = mintImpactunkid
            objImpactObjective._ObjectiveList = dsImpactObjective
            objImpactObjective._Userunkid = mintUserunkid
            With objImpactObjective
                ._FormName = mstrFormName
                ._LoginEmployeeunkid = mintLoginEmployeeunkid
                ._ClientIP = mstrClientIP
                ._HostName = mstrHostName
                ._FromWeb = mblnIsWeb
                ._AuditUserId = mintAuditUserId
._CompanyUnkid = mintCompanyUnkid
                ._AuditDate = mdtAuditDate
            End With
            If objImpactObjective.InsertUpdateDelete_Objective(objDataOperation) = False Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            objImpactFeedback._Impactunkid = mintImpactunkid
            objImpactFeedback._FeedBackList = dsImpactFeedback
            objImpactFeedback._Userunkid = mintUserunkid
            With objImpactFeedback
                ._FormName = mstrFormName
                ._LoginEmployeeunkid = mintLoginEmployeeunkid
                ._ClientIP = mstrClientIP
                ._HostName = mstrHostName
                ._FromWeb = mblnIsWeb
                ._AuditUserId = mintAuditUserId
._CompanyUnkid = mintCompanyUnkid
                ._AuditDate = mdtAuditDate
            End With
            If objImpactFeedback.InsertUpdateDelete_FeedBack(objDataOperation) = False Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            'S.SANDEEP [28-May-2018] -- START
            'ISSUE/ENHANCEMENT : {Audit Trails} 
            Dim objCommonATLog As New clsCommonATLog
            objCommonATLog._FormName = mstrFormName
            objCommonATLog._LoginEmployeeUnkid = mintLoginEmployeeunkid
            objCommonATLog._ClientIP = mstrClientIP
            objCommonATLog._HostName = mstrHostName
            objCommonATLog._FromWeb = mblnIsWeb
            objCommonATLog._AuditUserId = mintAuditUserId
objCommonATLog._CompanyUnkid = mintCompanyUnkid
            objCommonATLog._AuditDate = mdtAuditDate
            'S.SANDEEP [28-May-2018] -- END

            If objCommonATLog.IsTableDataUpdate("atcommon_tranlog", "hrtnatraining_impact_master", mintImpactunkid, "impactunkid", 2, objDataOperation) Then
                If objCommonATLog.Insert_TranAtLog(objDataOperation, "hrtnatraining_impact_master", "impactunkid", mintImpactunkid, "", "", -1, 2, 0, , mintUserunkid) = False Then
                    exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                    Throw exForce
                End If

            End If

            'S.SANDEEP [28-May-2018] -- START
            'ISSUE/ENHANCEMENT : {Audit Trails} 
            objCommonATLog = Nothing
            'S.SANDEEP [28-May-2018] -- END


            objDataOperation.ReleaseTransaction(True)
            Return True
        Catch ex As Exception
            objDataOperation.ReleaseTransaction(False)
            Throw New Exception(ex.Message & "; Procedure Name: Update; Module Name: " & mstrModuleName)
            Return False
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
    End Function

    ''' <summary>
    ''' Modify By: Pinkal Jariwala
    ''' </summary>
    ''' <returns>Boolean</returns>
    ''' <purpose> Delete Database Table (hrtnatraining_impact_master) </purpose>
    Public Function Delete(ByVal intUnkid As Integer) As Boolean
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        Dim objDataOperation As New clsDataOperation
        objDataOperation.BindTransaction()

        Try

            strQ = "UPDATE hrtnatraining_impact_kra SET " & _
                       " isvoid = @isvoid" & _
                       ", voiduserunkid = @voiduserunkid" & _
                       ", voiddatetime = @voiddatetime" & _
                       ", voidreason = @voidreason " & _
                      " WHERE impactunkid = @impactunkid "

            objDataOperation.ClearParameters()
            objDataOperation.AddParameter("@impactunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intUnkid)
            objDataOperation.AddParameter("@isvoid", SqlDbType.Bit, eZeeDataType.BIT_SIZE, True)
            'Sohail (23 Apr 2012) -- Start
            'TRA - ENHANCEMENT
            'objDataOperation.AddParameter("@voiduserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, User._Object._Userunkid)
            objDataOperation.AddParameter("@voiduserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintVoiduserunkid)
            objDataOperation.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, mdtAuditDate)
            objDataOperation.AddParameter("@voidreason", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrVoidreason)

            Call objDataOperation.ExecNonQuery(strQ)

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            strQ = "UPDATE hrtnatraining_impact_development SET " & _
                       " isvoid = @isvoid" & _
                       ", voiduserunkid = @voiduserunkid" & _
                       ", voiddatetime = @voiddatetime" & _
                       ", voidreason = @voidreason " & _
                      " WHERE impactunkid = @impactunkid"

            objDataOperation.ClearParameters()
            objDataOperation.AddParameter("@impactunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intUnkid)
            objDataOperation.AddParameter("@isvoid", SqlDbType.Bit, eZeeDataType.BIT_SIZE, True)
            'Sohail (23 Apr 2012) -- Start
            'TRA - ENHANCEMENT
            'objDataOperation.AddParameter("@voiduserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, User._Object._Userunkid)
            objDataOperation.AddParameter("@voiduserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintVoiduserunkid)
            objDataOperation.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, mdtAuditDate)
            objDataOperation.AddParameter("@voidreason", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrVoidreason)
            Call objDataOperation.ExecNonQuery(strQ)

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            strQ = "UPDATE hrtnatraining_impact_objective SET " & _
                    " isvoid = @isvoid" & _
                    ", voiduserunkid = @voiduserunkid" & _
                    ", voiddatetime = @voiddatetime" & _
                    ", voidreason = @voidreason " & _
                  "WHERE impactunkid = @impactunkid"

            objDataOperation.ClearParameters()
            objDataOperation.AddParameter("@impactunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intUnkid)
            objDataOperation.AddParameter("@isvoid", SqlDbType.Bit, eZeeDataType.BIT_SIZE, True)
            'Sohail (23 Apr 2012) -- Start
            'TRA - ENHANCEMENT
            'objDataOperation.AddParameter("@voiduserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, User._Object._Userunkid)
            objDataOperation.AddParameter("@voiduserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintVoiduserunkid)
            objDataOperation.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, mdtAuditDate)
            objDataOperation.AddParameter("@voidreason", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrVoidreason)
            Call objDataOperation.ExecNonQuery(strQ)

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            strQ = "UPDATE hrtnatraining_impact_feedback SET " & _
                   " isvoid = @isvoid" & _
                   ", voiduserunkid = @voiduserunkid" & _
                   ", voiddatetime = @voiddatetime" & _
                   ", voidreason = @voidreason " & _
                 "WHERE impactunkid = @impactunkid"

            objDataOperation.ClearParameters()
            objDataOperation.AddParameter("@impactunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intUnkid)
            objDataOperation.AddParameter("@isvoid", SqlDbType.Bit, eZeeDataType.BIT_SIZE, True)
            'Sohail (23 Apr 2012) -- Start
            'TRA - ENHANCEMENT
            'objDataOperation.AddParameter("@voiduserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, User._Object._Userunkid)
            objDataOperation.AddParameter("@voiduserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintVoiduserunkid)
            objDataOperation.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, mdtAuditDate)
            objDataOperation.AddParameter("@voidreason", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrVoidreason)
            Call objDataOperation.ExecNonQuery(strQ)

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            strQ = " Update hrtnatraining_impact_master set " & _
                      " isvoid = @isvoid " & _
                      ", voiduserunkid = @voiduserunkid " & _
                      ", voiddatetime = @voiddatetime " & _
                      ", voidreason = @voidreason " & _
                      " WHERE impactunkid = @impactunkid "

            objDataOperation.ClearParameters()
            objDataOperation.AddParameter("@impactunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intUnkid)
            objDataOperation.AddParameter("@isvoid", SqlDbType.Bit, eZeeDataType.BIT_SIZE, True)
            objDataOperation.AddParameter("@voiduserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintVoiduserunkid)
            objDataOperation.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, mdtAuditDate)
            objDataOperation.AddParameter("@voidreason", SqlDbType.NVarChar, eZeeDataType.DESC_SIZE, mstrVoidreason)
            Call objDataOperation.ExecNonQuery(strQ)

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If


            'S.SANDEEP [28-May-2018] -- START
            'ISSUE/ENHANCEMENT : {Audit Trails} 
            Dim objCommonATLog As New clsCommonATLog
            objCommonATLog._FormName = mstrFormName
            objCommonATLog._LoginEmployeeUnkid = mintLoginEmployeeunkid
            objCommonATLog._ClientIP = mstrClientIP
            objCommonATLog._HostName = mstrHostName
            objCommonATLog._FromWeb = mblnIsWeb
            objCommonATLog._AuditUserId = mintAuditUserId
objCommonATLog._CompanyUnkid = mintCompanyUnkid
            objCommonATLog._AuditDate = mdtAuditDate
            'S.SANDEEP [28-May-2018] -- END

            If objCommonATLog.VoidAtTranAtLog(objDataOperation, "hrtnatraining_impact_master", "impactunkid", intUnkid, "hrtnatraining_impact_kra", "kraunkid", 3, 3, , , , mintVoiduserunkid) = False Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            'S.SANDEEP [28-May-2018] -- START
            'ISSUE/ENHANCEMENT : {Audit Trails} 
            objCommonATLog = Nothing
            'S.SANDEEP [28-May-2018] -- END


            'S.SANDEEP [28-May-2018] -- START
            'ISSUE/ENHANCEMENT : {Audit Trails} 
            objCommonATLog = New clsCommonATLog
            objCommonATLog._FormName = mstrFormName
            objCommonATLog._LoginEmployeeUnkid = mintLoginEmployeeunkid
            objCommonATLog._ClientIP = mstrClientIP
            objCommonATLog._HostName = mstrHostName
            objCommonATLog._FromWeb = mblnIsWeb
            objCommonATLog._AuditUserId = mintAuditUserId
objCommonATLog._CompanyUnkid = mintCompanyUnkid
            objCommonATLog._AuditDate = mdtAuditDate
            'S.SANDEEP [28-May-2018] -- END

            If objCommonATLog.VoidAtTranAtLog(objDataOperation, "hrtnatraining_impact_master", "impactunkid", intUnkid, "hrtnatraining_impact_development", "developmentunkid", 3, 3, , , , mintVoiduserunkid) = False Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            'S.SANDEEP [28-May-2018] -- START
            'ISSUE/ENHANCEMENT : {Audit Trails} 
            objCommonATLog = Nothing
            'S.SANDEEP [28-May-2018] -- END


            'S.SANDEEP [28-May-2018] -- START
            'ISSUE/ENHANCEMENT : {Audit Trails} 
            objCommonATLog = New clsCommonATLog
            objCommonATLog._FormName = mstrFormName
            objCommonATLog._LoginEmployeeUnkid = mintLoginEmployeeunkid
            objCommonATLog._ClientIP = mstrClientIP
            objCommonATLog._HostName = mstrHostName
            objCommonATLog._FromWeb = mblnIsWeb
            objCommonATLog._AuditUserId = mintAuditUserId
objCommonATLog._CompanyUnkid = mintCompanyUnkid
            objCommonATLog._AuditDate = mdtAuditDate
            'S.SANDEEP [28-May-2018] -- END

            If objCommonATLog.VoidAtTranAtLog(objDataOperation, "hrtnatraining_impact_master", "impactunkid", intUnkid, "hrtnatraining_impact_objective", "objectiveunkid", 3, 3, , , , mintVoiduserunkid) = False Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            'S.SANDEEP [28-May-2018] -- START
            'ISSUE/ENHANCEMENT : {Audit Trails} 
            objCommonATLog = Nothing
            'S.SANDEEP [28-May-2018] -- END


            'S.SANDEEP [28-May-2018] -- START
            'ISSUE/ENHANCEMENT : {Audit Trails} 
            objCommonATLog = New clsCommonATLog
            objCommonATLog._FormName = mstrFormName
            objCommonATLog._LoginEmployeeUnkid = mintLoginEmployeeunkid
            objCommonATLog._ClientIP = mstrClientIP
            objCommonATLog._HostName = mstrHostName
            objCommonATLog._FromWeb = mblnIsWeb
            objCommonATLog._AuditUserId = mintAuditUserId
objCommonATLog._CompanyUnkid = mintCompanyUnkid
            objCommonATLog._AuditDate = mdtAuditDate
            'S.SANDEEP [28-May-2018] -- END

            If objCommonATLog.VoidAtTranAtLog(objDataOperation, "hrtnatraining_impact_master", "impactunkid", intUnkid, "hrtnatraining_impact_feedback", "feedbackunkid", 3, 3, , , , mintVoiduserunkid) = False Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            'S.SANDEEP [28-May-2018] -- START
            'ISSUE/ENHANCEMENT : {Audit Trails} 
            objCommonATLog = Nothing
            'S.SANDEEP [28-May-2018] -- END


            objDataOperation.ReleaseTransaction(True)
            Return True
        Catch ex As Exception
            objDataOperation.ReleaseTransaction(False)
            Throw New Exception(ex.Message & "; Procedure Name: Delete; Module Name: " & mstrModuleName)
            Return False
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
    End Function

    ''' <summary>
    ''' Modify By: Pinkal Jariwala
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>
    Public Function isExist(ByVal strCode As String, ByVal strName As String, Optional ByVal intUnkid As Integer = -1) As Boolean
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        objDataOperation = New clsDataOperation

        Try
            strQ = "SELECT " & _
              "  impactunkid " & _
              ", courseunkid " & _
              ", employeeunkid " & _
              ", managerunkid " & _
              ", self_remark " & _
              ", iscomplete " & _
              ", userunkid " & _
              ", isvoid " & _
              ", voiduserunkid " & _
              ", voiddatetime " & _
              ", voidreason " & _
             "FROM hrtnatraining_impact_master " & _
             "WHERE name = @name " & _
             "AND code = @code "

            If intUnkid > 0 Then
                strQ &= " AND impactunkid <> @impactunkid"
            End If

            objDataOperation.AddParameter("@code", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, strCode)
            objDataOperation.AddParameter("@name", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, strName)
            objDataOperation.AddParameter("@impactunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intUnkid)

            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            Return dsList.Tables(0).Rows.Count > 0
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: isExist; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
    End Function

    ''' <summary>
    ''' Modify By: Pinkal Jariwala
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>
    Public Function GetKRAFromTrainingPriority(ByVal intCourseunkid As Integer, ByVal intEmployeeunkid As Integer) As DataSet
        Dim dsList As DataSet = Nothing
        Dim exForce As Exception
        Dim strQ As String = ""
        Try
            Dim objDataOperation As New clsDataOperation
            strQ = " SELECT  hrtnacoursepriority_tran.courseunkid " & _
                       ", cfcommon_master.name  " & _
                       ", kra " & _
                       ", training_objective " & _
                       ", '' AUD " & _
                       ", '' GUID " & _
                       " FROM  hrtnacoursepriority_tran " & _
                       " JOIN cfcommon_master ON cfcommon_master.masterunkid = hrtnacoursepriority_tran.courseunkid AND cfcommon_master.isactive = 1 " & _
                       " JOIN hrtnaemp_course_tran ON hrtnaemp_course_tran.coursepriotranunkid = hrtnacoursepriority_tran.coursepriotranunkid " & _
                            " AND hrtnacoursepriority_tran.courseunkid = hrtnaemp_course_tran.courseunkid AND hrtnaemp_course_tran.isvoid = 0" & _
                       " WHERE   hrtnacoursepriority_tran.courseunkid = @courseId AND employeeunkid = @employeeunkid AND hrtnacoursepriority_tran.isvoid = 0 "

            objDataOperation.ClearParameters()
            objDataOperation.AddParameter("@courseId", SqlDbType.Int, eZeeDataType.INT_SIZE, intCourseunkid)
            objDataOperation.AddParameter("@employeeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intEmployeeunkid)
            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: GetKRAFromTrainingPriority; Module Name: " & mstrModuleName)
        End Try
        Return dsList
    End Function

    ''' <summary>
    ''' Modify By: Pinkal Jariwala
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>
    Public Function GetDevelopmentGAPFromAssessment(ByVal intCourseunkid As Integer, ByVal intEmployeeunkid As Integer) As DataSet
        Dim dsList As DataSet = Nothing
        Dim exForce As Exception
        Dim strQ As String = ""
        Try
            Dim objDataOperation As New clsDataOperation

            strQ = " SELECT  assessedemployeeunkid " & _
                      ", ISNULL(major_area,'') as development_area " & _
                      ", '' AUD " & _
                      ", '' GUID " & _
                      " FROM hrassess_analysis_master " & _
                      " JOIN hrassess_remarks_tran ON hrassess_analysis_master.analysisunkid = hrassess_remarks_tran.analysisunkid AND hrassess_remarks_tran.isvoid = 0" & _
                      " AND isimporvement = 0 " & _
                      " WHERE  assessmodeid = " & enAssessmentMode.APPRAISER_ASSESSMENT & _
                      " AND coursemasterunkid = @courseID AND assessedemployeeunkid = @employeeunkid AND hrassess_analysis_master.isvoid = 0"

            objDataOperation.ClearParameters()
            objDataOperation.AddParameter("@courseID", SqlDbType.Int, eZeeDataType.INT_SIZE, intCourseunkid)
            objDataOperation.AddParameter("@employeeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intEmployeeunkid)
            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: GetDevelopmentGAPFromAssessment; Module Name: " & mstrModuleName)
        End Try
        Return dsList
    End Function

    'S.SANDEEP [06-MAR-2017] -- START
    'ISSUE/ENHANCEMENT : Training Module Notification
    Public Sub SendNotification(ByVal intTrainingSchedulingId As Integer, _
                               ByVal intEmployeeId As Integer, _
                               ByVal mdtEmpAsOnDate As DateTime, _
                               ByVal xLoginMod As enLogin_Mode, _
                               ByVal xUserId As Integer, _
                               ByVal strCSVSelectedUserIds As String, ByVal xLogEmpId As Integer, ByVal strFormName As String, ByVal intCompanyUnkId As Integer)
        'Sohail (30 Nov 2017) - [intCompanyUnkId]
        Dim strMessage As New System.Text.StringBuilder
        Dim objEmp As New clsEmployee_Master : Dim objTrSchedule As New clsTraining_Scheduling : Dim objCMaster As New clsCommon_Master : Dim objUsr As New clsUserAddEdit
        Try
            objEmp._Employeeunkid(mdtEmpAsOnDate) = intEmployeeId
            objTrSchedule._Trainingschedulingunkid = intTrainingSchedulingId
            objCMaster._Masterunkid = objTrSchedule._Course_Title
            Dim dsLst As New DataSet
            dsLst = objUsr.GetList("List", "hrmsConfiguration..cfuser_master.userunkid IN (" & strCSVSelectedUserIds & ")")
            If dsLst.Tables("List").Rows.Count > 0 Then
                For Each row As DataRow In dsLst.Tables("List").Rows
                    If row.Item("email").ToString.Trim.Length > 0 Then
                        Dim strName As String = row.Item("firstname") & " " & row.Item("lastname")
                        strMessage.Append("<HTML> <BODY>")

                        'Gajanan [27-Mar-2019] -- Start
                        'Enhancement - Change Email Language
                        'strMessage.Append(Language.getMessage(mstrModuleName, 100, "Dear") & " <B>" & IIf(strName.Trim.Length > 0, strName, row.Item("username")) & "</B>, <BR><BR>")
                        strMessage.Append(Language.getMessage(mstrModuleName, 100, "Dear") & " <B>" & getTitleCase(IIf(strName.Trim.Length > 0, strName, row.Item("username"))) & "</B>, <BR><BR>")
                        'strMessage.Append("&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;" & Language.getMessage(mstrModuleName, 101, "This is to inform you that, I have done with level III evaluation for the training attended with details below :") & "<BR><BR>")
                        strMessage.Append(Language.getMessage(mstrModuleName, 101, "This is to inform you that, I have done with level III evaluation for the training attended with details below :") & "<BR><BR>")
                        'Gajanan [27-Mar-2019] -- End
                        strMessage.Append("<TABLE border = '1' WIDTH = '60%'>")
                        strMessage.Append("<TR WIDTH = '90%' bgcolor= 'SteelBlue'>")
                        strMessage.Append("<TD align = 'LEFT' STYLE='WIDTH:30%'><b><span style='font-size:9.0pt; font-family:" & Chr(34) & "Verdana" & Chr(34) & "," & Chr(34) & "Sans-Serif" & Chr(34) & "; color:#FFFFFF; '>" & Language.getMessage(mstrModuleName, 102, "Course Title") & "</span></b></TD>")
                        strMessage.Append("<TD align = 'LEFT' STYLE='WIDTH:30%'><b><span style='font-size:9.0pt; font-family:" & Chr(34) & "Verdana" & Chr(34) & "," & Chr(34) & "Sans-Serif" & Chr(34) & "; color:#FFFFFF; '>" & Language.getMessage(mstrModuleName, 103, "Duration") & "</span></b></TD>")
                        strMessage.Append("</TR>")
                        strMessage.Append("<TR WIDTH = '60%'>" & vbCrLf)
                        strMessage.Append("<TD align = 'LEFT' WIDTH = '30%'><span style='font-size:9.0pt; font-family:" & Chr(34) & "Verdana" & Chr(34) & "," & Chr(34) & "Sans-Serif" & Chr(34) & "; color:#000000; '>" & objCMaster._Name & "</span></TD>")
                        strMessage.Append("<TD align = 'LEFT' WIDTH = '60%'><span style='font-size:9.0pt; font-family:" & Chr(34) & "Verdana" & Chr(34) & "," & Chr(34) & "Sans-Serif" & Chr(34) & "; color:#000000; '>" & Language.getMessage(mstrModuleName, 106, "Date :") & " " & objTrSchedule._Start_Date.ToShortDateString & " - " & objTrSchedule._End_Date.ToShortDateString & "<BR> " & Language.getMessage(mstrModuleName, 107, "Time :") & " " & " (" & Format(objTrSchedule._Start_Time, "HH:MM") & " - " & Format(objTrSchedule._End_Time, "HH:MM") & ") </span></TD>")
                        strMessage.Append("</TR>" & vbCrLf)
                        strMessage.Append("</TABLE>")

                        'Gajanan [27-Mar-2019] -- Start
                        'Enhancement - Change Email Language
                        'strMessage.Append("<p><span style='padding-top:25px;font-size:10.0pt; font-family:" & Chr(34) & "Verdana" & Chr(34) & "," & Chr(34) & "Sans-Serif" & Chr(34) & "; color:#333399;margin-left:0px;margin-right:0px;margin-top:0px;margin-bottom:10px;'><b>&nbsp;&nbsp;&nbsp;&nbsp;&quot;POWERED BY ARUTI HR & PAYROLL MANAGEMENT SOFTWARE.&quot;</b>")

                        strMessage.Append("<p><center style='padding-top:25px;font-size:10.0pt;font-family:&quot;Verdana&quot;,&quot;Sans-Serif&quot;;color:#333399;margin-left:0px;margin-right:0px;margin-top:0px;margin-bottom:10px'><b>&quot;POWERED BY ARUTI HR &amp; PAYROLL MANAGEMENT SOFTWARE.&quot;</b></center></p>")

                        'Gajanan [27-Mar-2019] -- End
                        strMessage.Append("</span></p>")
                        strMessage.Append("</BODY></HTML>")
                    End If

                    If strMessage.ToString.Trim.Length > 0 Then
                        Dim objSendMail As New clsSendMail
                        objSendMail._ToEmail = row.Item("email").ToString.Trim
                        objSendMail._Subject = Language.getMessage(mstrModuleName, 105, "Notification of Level III Evaluation")
                        objSendMail._Message = strMessage.ToString
                        'objSendMail._FormName = strFormName
                        'objSendMail._LoginEmployeeunkid = xLogEmpId
                        With objSendMail
                            ._FormName = mstrFormName
                            ._LoginEmployeeunkid = mintLoginEmployeeunkid
                            ._ClientIP = mstrClientIP
                            ._HostName = mstrHostName
                            ._FromWeb = mblnIsWeb
                            ._AuditUserId = mintAuditUserId
._CompanyUnkid = mintCompanyUnkid
                            ._AuditDate = mdtAuditDate
                        End With
                        objSendMail._OperationModeId = xLoginMod
                        objSendMail._UserUnkid = xUserId
                        objSendMail._SenderAddress = objEmp._Firstname & " " & objEmp._Surname
                        objSendMail._ModuleRefId = clsSendMail.enAT_VIEW_TYPE.EMPLOYEE_MGT
                        Try
                            'Sohail (30 Nov 2017) -- Start
                            'SUMATRA Enhancement – SUMATRA – issue # 0001669: Additional feature for Exchange Server configuration setting in 70.1.
                            'objSendMail.SendMail()
                            objSendMail.SendMail(intCompanyUnkId)
                            'Sohail (30 Nov 2017) -- End
                        Catch ex As Exception
                        End Try
                        objSendMail = Nothing
                    End If
                Next
            End If
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: SendNotification; Module Name: " & mstrModuleName)
        Finally
            objEmp = Nothing : objTrSchedule = Nothing : objCMaster = Nothing : objUsr = Nothing
        End Try
    End Sub
    'S.SANDEEP [06-MAR-2017] -- END

	'<Language> This Auto Generated Text Please Do Not Modify it.
#Region " Language & UI Settings "
	Public Shared Sub SetMessages()
		Try
			Language.setMessage(mstrModuleName, 1, "Not Completed")
			Language.setMessage(mstrModuleName, 2, "Completed")
            Language.setMessage(mstrModuleName, 100, "Dear")
            Language.setMessage(mstrModuleName, 101, "This is to inform you that, I have done with level III evaluation for the training attended with details below :")
            Language.setMessage(mstrModuleName, 102, "Course Title")
            Language.setMessage(mstrModuleName, 103, "Duration")
            Language.setMessage(mstrModuleName, 105, "Notification of Level III Evaluation")
            Language.setMessage(mstrModuleName, 106, "Date :")
            Language.setMessage(mstrModuleName, 107, "Time :")

		Catch Ex As Exception
			DisplayError.Show("-1", ex.Message, "SetMessages", mstrModuleName)
		End Try
	End Sub
#End Region 'Language & UI Settings
	'</Language>
End Class