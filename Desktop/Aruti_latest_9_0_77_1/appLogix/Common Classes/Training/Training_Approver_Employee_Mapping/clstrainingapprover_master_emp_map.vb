﻿'****************************************************************************************************************
'Class Name :clstrainingapprover_master_emp_map.vb
'Purpose    :
'Date       :11-Jul-2022
'Written By :Hemant
'Modified   :
'****************************************************************************************************************
Imports eZeeCommonLib
Imports eZee.Common.eZeeForm

''' <summary>
''' Purpose: 
''' Developer: Hemant
''' </summary>
Public Class clstrainingapprover_master_emp_map
    Private Shared ReadOnly mstrModuleName As String = "clstrainingapprover_master_emp_map"
    Private objDataOperation As clsDataOperation
    Dim mstrMessage As String = ""

#Region "Private Variables"

    Private mintApproverunkid As Integer
    Private mintLevelunkid As Integer
    Private mintApproverEmpunkid As Integer
    Private mintMappedUserId As Integer = 0
    Private mintUserunkid As Integer
    Private mintCalendarunkid As Integer
    Private mblnIsswap As Boolean = False
    Private mblnIsactive As Boolean = True
    Private mblnIsvoid As Boolean
    Private mdtVoidDateTime As Date
    Private mintVoidUserunkid As Integer
    Private mstrVoidReason As String = ""
    Private mstrFormName As String = String.Empty
    Private mstrClientIP As String = String.Empty
    Private mstrHostName As String = String.Empty
    Private mblnIsWeb As Boolean = False
    Private objTrainingApproverTran As New clstrainingapprover_tran_emp_map
    Private mblnIsExternalApprover As Boolean = False
    Private mintTrainingTypeId As Integer

#End Region

#Region "Properties"
    ''' <summary>
    ''' Purpose: Get Message from Class 
    ''' Modify By: Hemant
    ''' </summary>
    Public ReadOnly Property _Message() As String
        Get
            Return mstrMessage
        End Get
    End Property

    ''' <summary>
    ''' Purpose: Get or Set approverunkid
    ''' Modify By: Hemant
    ''' </summary>
    Public Property _Approverunkid(Optional ByVal objDataOp As clsDataOperation = Nothing) As Integer
        Get
            Return mintApproverunkid
        End Get
        Set(ByVal value As Integer)
            mintApproverunkid = value
            Call GetData(objDataOp)
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set Levelunkid
    ''' Modify By: Hemant
    ''' </summary>
    Public Property _Levelunkid() As Integer
        Get
            Return mintLevelunkid
        End Get
        Set(ByVal value As Integer)
            mintLevelunkid = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set ApproverEmpunkid
    ''' Modify By: Hemant
    ''' </summary>
    Public Property _ApproverEmpunkid() As Integer
        Get
            Return mintApproverEmpunkid
        End Get
        Set(ByVal value As Integer)
            mintApproverEmpunkid = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set MappedUserunkid
    ''' Modify By: Hemant
    ''' </summary>
    Public Property _MappedUserunkid() As Integer
        Get
            Return mintMappedUserId
        End Get
        Set(ByVal value As Integer)
            mintMappedUserId = value
        End Set
    End Property

    'Hemant (16 Sep 2022) -- Start
    'ISSUE/ENHANCEMENT(NMB) : Pending Training approval should be go for New User instead of Old User  after changing User  in Training Approver Add/Edit Screen
    ''' <summary>
    ''' Purpose: Get or Set OldMappedUserunkid
    ''' Modify By: Hemant
    ''' </summary>
    Private mintOldMappedUserId As Integer = 0
    Public Property _OldMappedUserunkid() As Integer
        Get
            Return mintOldMappedUserId
        End Get
        Set(ByVal value As Integer)
            mintOldMappedUserId = value
        End Set
    End Property
    'Hemant (16 Sep 2022) -- End

    ''' <summary>
    ''' Purpose: Get or Set Userunkid
    ''' Modify By: Hemant
    ''' </summary>
    Public Property _Userunkid() As Integer
        Get
            Return mintUserunkid
        End Get
        Set(ByVal value As Integer)
            mintUserunkid = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set Calendarunkid
    ''' Modify By: Hemant
    ''' </summary>
    Public Property _Calendarunkid() As Integer
        Get
            Return mintCalendarunkid
        End Get
        Set(ByVal value As Integer)
            mintCalendarunkid = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set Isvoid
    ''' Modify By: Hemant
    ''' </summary>
    Public Property _Isvoid() As Boolean
        Get
            Return mblnIsvoid
        End Get
        Set(ByVal value As Boolean)
            mblnIsvoid = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set VoidDateTime
    ''' Modify By: Hemant
    ''' </summary>
    Public Property _VoidDateTime() As Date
        Get
            Return mdtVoidDateTime
        End Get
        Set(ByVal value As Date)
            mdtVoidDateTime = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set VoidUserunkid
    ''' Modify By: Hemant
    ''' </summary>
    Public Property _VoidUserunkid() As Integer
        Get
            Return mintVoidUserunkid
        End Get
        Set(ByVal value As Integer)
            mintVoidUserunkid = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set VoidReason
    ''' Modify By: Hemant
    ''' </summary>
    Public Property _VoidReason() As String
        Get
            Return mstrVoidReason
        End Get
        Set(ByVal value As String)
            mstrVoidReason = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set Isvoid
    ''' Modify By: Hemant 
    ''' </summary>
    Public Property _Isswap() As Boolean
        Get
            Return mblnIsswap
        End Get
        Set(ByVal value As Boolean)
            mblnIsswap = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set Isactive
    ''' Modify By: Hemant
    ''' </summary>
    Public Property _Isactive() As Boolean
        Get
            Return mblnIsactive
        End Get
        Set(ByVal value As Boolean)
            mblnIsactive = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set FormName
    ''' Modify By: Hemant
    ''' </summary>
    Public Property _FormName() As String
        Get
            Return mstrFormName
        End Get
        Set(ByVal value As String)
            mstrFormName = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set ClientIP
    ''' Modify By: Hemant
    ''' </summary>
    Public WriteOnly Property _ClientIP() As String
        Set(ByVal value As String)
            mstrClientIP = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set HostName
    ''' Modify By: Hemant
    ''' </summary>
    Public WriteOnly Property _HostName() As String
        Set(ByVal value As String)
            mstrHostName = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set IsWeb
    ''' Modify By: Hemant
    ''' </summary>
    Public WriteOnly Property _IsWeb() As Boolean
        Set(ByVal value As Boolean)
            mblnIsWeb = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set Isvoid
    ''' Modify By: Hemant
    ''' </summary>
    Public Property _IsExternalApprover() As Boolean
        Get
            Return mblnIsExternalApprover
        End Get
        Set(ByVal value As Boolean)
            mblnIsExternalApprover = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set TrainingTypeId
    ''' Modify By: Hemant
    ''' </summary>
    Public Property _TrainingTypeId() As Integer
        Get
            Return mintTrainingTypeId
        End Get
        Set(ByVal value As Integer)
            mintTrainingTypeId = value
        End Set
    End Property


#End Region

    Public Sub GetData(Optional ByVal objDataOp As clsDataOperation = Nothing)

        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        If objDataOp Is Nothing Then
            objDataOperation = New clsDataOperation
        Else
            objDataOperation = objDataOp
        End If
        objDataOperation.ClearParameters()

        Try
            strQ = "SELECT " & _
                    "  trtrainingapprover_master.approverunkid " & _
                    " ,trtrainingapprover_master.levelunkid " & _
                    " ,trtrainingapprover_master.approverempunkid " & _
                    " ,trtrainingapprover_master.userunkid " & _
                    " ,trtrainingapprover_master.isvoid " & _
                    " ,trtrainingapprover_master.voiddatetime " & _
                    " ,trtrainingapprover_master.voiduserunkid " & _
                    " ,trtrainingapprover_master.voidreason " & _
                    " ,trtrainingapprover_master.isswap " & _
                    " ,trtrainingapprover_master.isactive " & _
                    " ,trtrainingapprover_master.isexternalapprover " & _
                    " ,trtrainingapprover_master.calendarunkid " & _
                    " ,ISNULL(trtrainingapprover_master.trainingtypeid, 0) AS trainingtypeid " & _
                " FROM trtrainingapprover_master " & _
                " WHERE approverunkid=@approverunkid "

            objDataOperation.AddParameter("@approverunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintApproverunkid.ToString)

            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ":" & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            For Each drRow As DataRow In dsList.Tables(0).Rows
                mintApproverunkid = CInt(drRow.Item("approverunkid"))
                mintLevelunkid = CInt(drRow.Item("levelunkid"))
                mintApproverEmpunkid = CInt(drRow.Item("approverempunkid"))
                mintUserunkid = CInt(drRow.Item("userunkid"))
                mblnIsactive = CBool(drRow.Item("isactive"))
                mblnIsvoid = CBool(drRow.Item("isvoid"))
                If IsDBNull(drRow.Item("voiddatetime")) = False Then
                    mdtVoidDateTime = drRow.Item("voiddatetime")
                Else
                    mdtVoidDateTime = Nothing
                End If
                mintVoidUserunkid = CInt(drRow.Item("voiduserunkid"))
                mstrVoidReason = drRow.Item("voidreason").ToString
                mblnIsswap = CBool(drRow.Item("isswap"))
                mblnIsExternalApprover = CBool(drRow.Item("isexternalapprover"))
                mintCalendarunkid = CInt(drRow.Item("calendarunkid"))

                Dim objUserMapping As New clsapprover_Usermapping
                objUserMapping.GetData(enUserType.Training_Approver, mintApproverunkid)
                mintMappedUserId = objUserMapping._Userunkid
                mintTrainingTypeId = CInt(drRow.Item("trainingtypeid"))
                Exit For
            Next

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: GetData , Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
        End Try
    End Sub

    Public Function GetExternalApproverList(Optional ByVal objDataOpr As clsDataOperation = Nothing, Optional ByVal strList As String = "List") As DataSet

        Dim StrQ As String = ""
        Dim exForce As Exception
        Dim dsApprover As New DataSet

        Try
            If objDataOpr IsNot Nothing Then
                objDataOperation = objDataOpr
            Else
                objDataOperation = New clsDataOperation
            End If
            objDataOperation.ClearParameters()

            If strList.Trim.Length <= 0 Then strList = "List"

            StrQ = "SELECT DISTINCT " & _
                   "     ISNULL(cffinancial_year_tran.database_name,'') AS dbname " & _
                   "    ,ISNULL(cffinancial_year_tran.companyunkid,0) AS companyunkid " & _
                   "    ,ISNULL(cffinancial_year_tran.yearunkid,0) AS yearunkid " & _
                   "    ,ISNULL(EffDt.key_value,'') AS effectivedate " & _
                   "    ,ISNULL(UC.key_value,'') AS modeset " & _
                   "FROM trtrainingapprover_master " & _
                   "    JOIN hrmsConfiguration..cfuser_master ON cfuser_master.userunkid = trtrainingapprover_master.approverempunkid " & _
                   "    LEFT JOIN hrmsConfiguration..cffinancial_year_tran ON cffinancial_year_tran.companyunkid = cfuser_master.companyunkid " & _
                   "            AND cffinancial_year_tran.isclosed = 0 " & _
                   "    LEFT JOIN " & _
                   "    ( " & _
                   "        SELECT " & _
                   "             cfconfiguration.companyunkid " & _
                   "            ,cfconfiguration.key_value " & _
                   "        FROM hrmsConfiguration..cfconfiguration " & _
                   "        WHERE UPPER(cfconfiguration.key_name) IN ('EMPLOYEEASONDATE') " & _
                   "    ) AS EffDt ON EffDt.companyunkid = cffinancial_year_tran.companyunkid " & _
                   "    LEFT JOIN " & _
                   "    ( " & _
                   "        SELECT " & _
                   "             cfconfiguration.companyunkid " & _
                   "            ,cfconfiguration.key_value " & _
                   "        FROM hrmsConfiguration..cfconfiguration " & _
                   "        WHERE UPPER(cfconfiguration.key_name) IN ('USERACCESSMODESETTING') " & _
                   "    ) AS UC ON UC.companyunkid = cffinancial_year_tran.companyunkid " & _
                   "WHERE trtrainingapprover_master.isvoid = 0 AND trtrainingapprover_master.isswap = 0 " & _
                   "  AND trtrainingapprover_master.isexternalapprover = 1 "

            dsApprover = objDataOperation.ExecQuery(StrQ, strList)

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: GetExternalApproverList; Module Name: " & mstrModuleName)
        Finally
            If objDataOpr Is Nothing Then
                objDataOperation = Nothing
            End If
        End Try
        Return dsApprover
    End Function

    Public Function GetList(ByVal xDatabaseName As String, _
                            ByVal xUserUnkid As Integer, _
                            ByVal xYearUnkid As Integer, _
                            ByVal xCompanyUnkid As Integer, _
                            ByVal xPeriodStart As DateTime, _
                            ByVal xPeriodEnd As DateTime, _
                            ByVal xUserModeSetting As String, _
                            ByVal xOnlyApproved As Boolean, _
                            ByVal xIncludeIn_ActiveEmployee As Boolean, _
                            ByVal strTableName As String, _
                            Optional ByVal blnOnlyActive As Boolean = True, _
                            Optional ByVal blnIsvoid As Boolean = False, _
                            Optional ByVal strFilter As String = "", _
                            Optional ByVal intApproverTranunkid As Integer = 0 _
                            , Optional ByVal intTrainingTypeId As Integer = 0 _
                            ) As DataSet

        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception
        Dim objMaster As New clsMasterData

        objDataOperation = New clsDataOperation

        Try
            Dim dsTrainingTravelType As DataSet = objMaster.getComboListForTrainingType(False, "List")
            Dim dicTrainingTravelType As Dictionary(Of Integer, String) = (From p In dsTrainingTravelType.Tables("List") Select New With {.id = CInt(p.Item("Id")), .name = p.Item("Name").ToString}).ToDictionary(Function(x) x.id, Function(x) x.name)

            Dim StrFinalQurey As String = String.Empty
            Dim StrQCondition As String = String.Empty
            Dim StrQDtFilters As String = String.Empty
            Dim StrQDataJoin As String = String.Empty

            Dim xDateJoinQry, xDateFilterQry, xAdvanceJoinQry As String
            xDateJoinQry = "" : xDateFilterQry = "" : xAdvanceJoinQry = ""
            Call GetDatesFilterString(xDateJoinQry, xDateFilterQry, xPeriodStart, xPeriodEnd, , , xDatabaseName)
            Call GetAdvanceFilterQry(xAdvanceJoinQry, xPeriodEnd, xDatabaseName)

            'Hemant (16 Sep 2022) -- Start
            'ISSUE(NMB) : Showing Same Approver Name multiple times in Approver Name Combo Selection on Training Approver List Screen
            strQ = "IF OBJECT_ID('tempdb..#TrainingType') IS NOT NULL " & _
                   "DROP TABLE #TrainingType "

            strQ &= "SELECT Id, NAME into #TrainingType from " & _
                    "("
            Dim strTrainigTypeQry As String = String.Empty
            For Each pair In dicTrainingTravelType
                If strTrainigTypeQry.Trim.Length > 0 Then strTrainigTypeQry &= " UNION "
                strTrainigTypeQry &= " SELECT " & pair.Key & " AS Id, ISNULL('" & pair.Value & "','') AS Name "
            Next
            strQ &= strTrainigTypeQry
            strQ &= ") AS A "
            'Hemant (16 Sep 2022) -- End


            strQ &= "SELECT " & _
                      "  trtrainingapprover_master.approverunkid " & _
                      ", trtrainingapprover_master.levelunkid " & _
                      ", hrtraining_approverlevel_master.levelname " & _
                      ", hrtraining_approverlevel_master.priority " & _
                      ", trtrainingapprover_master.approverempunkid " & _
                      ", #APPROVER_CODE# AS code " & _
                      ", #APPROVER_NAME# as name " & _
                      ", #APPROVER_NAME# + ' - ' + ISNULL(#TrainingType.NAME, '') + ' - ' + hrtraining_approverlevel_master.levelname AS ApproverNameWithLevel " & _
                      ", #DEPT_NAME# as departmentname " & _
                      ", #SEC_NAME# as sectionname " & _
                      ", #JOB_NAME# As  jobname " & _
                      ", trtrainingapprover_master.isactive " & _
                      ", trtrainingapprover_master.isvoid " & _
                      ", trtrainingapprover_master.voiddatetime " & _
                      ", trtrainingapprover_master.userunkid " & _
                      ", trtrainingapprover_master.voiduserunkid " & _
                      ", trtrainingapprover_master.voidreason " & _
                      ", trtrainingapprover_master.isswap " & _
                      ", trtrainingapprover_master.isexternalapprover " & _
                      ", trtrainingapprover_master.calendarunkid " & _
                      ", ISNULL(trtrainingapprover_master.trainingtypeid, 0) AS trainingtypeid "
            'Hemant (16 Sep 2022) -- Start
            'ISSUE(NMB) : Showing Same Approver Name multiple times in Approver Name Combo Selection on Training Approver List Screen
            strQ &= ", ISNULL(#TrainingType.NAME, '') AS TrainingType "
            '", CASE trtrainingapprover_master.trainingtypeid "

            'For Each pair In dicTrainingTravelType
            '    strQ &= "       WHEN ISNULL(" & pair.Key & ",'') THEN ISNULL('" & pair.Value & "','') "
            'Next
            'strQ &= "         END AS TrainingType "
            'Hemant (16 Sep 2022) -- End

            strQ &= ", CASE WHEN trtrainingapprover_master.isexternalapprover = 1 THEN @YES ELSE @NO END AS extapprover " & _
                      ", #APPROVER_NAME_WITHOUT_CODE# as ApproverName " & _
                    " FROM trtrainingapprover_master " & _
                      " LEFT JOIN hrtraining_approverlevel_master ON hrtraining_approverlevel_master.levelunkid = trtrainingapprover_master.levelunkid " & _
                      " LEFT JOIN #TrainingType ON #TrainingType.id = trtrainingapprover_master.trainingtypeid " & _
                      " #EMPLOYEE_JOIN# " & _
                      " #DATA_JOIN# "
            'Hemant (16 Sep 2022) -- [ApproverNameWithLevel]

            StrFinalQurey = strQ

            If xDateJoinQry.Trim.Length > 0 Then
                strQ &= xDateJoinQry
            End If

            If xAdvanceJoinQry.Trim.Length > 0 Then
                strQ &= xAdvanceJoinQry
            End If

            StrQCondition = " WHERE 1=1 AND trtrainingapprover_master.isexternalapprover = #EXT_APPROVER# "

            If xIncludeIn_ActiveEmployee = False Then
                If xDateFilterQry.Trim.Length > 0 Then
                    StrQDtFilters &= xDateFilterQry & " "
                End If
            End If

            If blnOnlyActive = True Then
                StrQCondition &= " AND trtrainingapprover_master.isactive = 1 "
            Else
                StrQCondition &= " AND trtrainingapprover_master.isactive = 0 "
            End If

            If blnIsvoid = True Then
                StrQCondition &= " AND trtrainingapprover_master.isvoid = 1 "
            Else
                StrQCondition &= " AND trtrainingapprover_master.isvoid = 0 "
            End If

            If intApproverTranunkid > 0 Then
                StrQCondition &= " AND trtrainingapprover_master.approverunkid = " & intApproverTranunkid & " "
            End If

            StrQCondition &= " AND trtrainingapprover_master.isswap = 0 "

            If strFilter.Trim.Length > 0 Then
                StrQCondition &= " AND " & strFilter
            End If

            If intTrainingTypeId > 0 Then
                StrQCondition &= " AND trtrainingapprover_master.trainingtypeid = @trainingtypeid "
                objDataOperation.AddParameter("@trainingtypeid", SqlDbType.Int, eZeeDataType.INT_SIZE, intTrainingTypeId)
            End If

            strQ = strQ.Replace("#APPROVER_CODE#", "ISNULL(hremployee_master.employeecode,'') ")
            strQ = strQ.Replace("#APPROVER_NAME#", "ISNULL(hremployee_master.employeecode,'') + ' - ' + ISNULL(hremployee_master.firstname,'') + ' ' + ISNULL(hremployee_master.surname,'') ")
            strQ = strQ.Replace("#DEPT_NAME#", "ISNULL(DPT.name,'') ")
            strQ = strQ.Replace("#SEC_NAME#", "ISNULL(SEC.name,'') ")
            strQ = strQ.Replace("#JOB_NAME#", "ISNULL(JOB.job_name,'') ")
            strQ = strQ.Replace("#EMPLOYEE_JOIN#", "LEFT JOIN hremployee_master ON hremployee_master.employeeunkid = trtrainingapprover_master.approverempunkid AND hremployee_master.isapproved = 1 ")

            strQ = strQ.Replace("#APPROVER_NAME_WITHOUT_CODE#", "ISNULL(hremployee_master.firstname,'') + ' ' + ISNULL(hremployee_master.surname,'') ")

            StrQDataJoin = " LEFT JOIN " & _
                      "( " & _
                      "     SELECT " & _
                      "          departmentunkid " & _
                      "         ,sectionunkid " & _
                      "         ,employeeunkid " & _
                      "         ,ROW_NUMBER()OVER(PARTITION BY employeeunkid ORDER BY effectivedate DESC) AS rno " & _
                           "     FROM #DB_NAME#hremployee_transfer_tran " & _
                      "     WHERE isvoid = 0 AND CONVERT(CHAR(8),effectivedate,112) <= '" & eZeeDate.convertDate(xPeriodEnd) & "' " & _
                      ") AS ETT ON ETT.employeeunkid = hremployee_master.employeeunkid AND ETT.rno = 1 " & _
                           " LEFT JOIN #DB_NAME#hrdepartment_master AS DPT ON DPT.departmentunkid = ETT.departmentunkid " & _
                           " LEFT JOIN #DB_NAME#hrsection_master AS SEC ON SEC.sectionunkid = ETT.sectionunkid " & _
                      " LEFT JOIN " & _
                            "( " & _
                            "   SELECT " & _
                            "        jobunkid " & _
                            "       ,employeeunkid " & _
                            "       ,ROW_NUMBER()OVER(PARTITION BY employeeunkid ORDER BY effectivedate DESC) AS rno " & _
                                 "   FROM #DB_NAME#hremployee_categorization_tran " & _
                            "   WHERE isvoid = 0 AND CONVERT(CHAR(8),effectivedate,112) <= '" & eZeeDate.convertDate(xPeriodEnd) & "' " & _
                            ") AS ECT ON ECT.employeeunkid = hremployee_master.employeeunkid AND ECT.rno = 1 " & _
                           " LEFT JOIN #DB_NAME#hrjob_master AS JOB ON JOB.jobunkid = ECT.jobunkid "

            strQ = strQ.Replace("#DATA_JOIN#", StrQDataJoin)
            strQ = strQ.Replace("#DB_NAME#", "")
            strQ &= StrQCondition
            strQ = strQ.Replace("#EXT_APPROVER#", "0")
            strQ &= StrQDtFilters

            objDataOperation.AddParameter("@YES", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 3, "Yes"))
            objDataOperation.AddParameter("@NO", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 4, "No"))

            dsList = objDataOperation.ExecQuery(strQ, strTableName)

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            Dim dsCompany As DataSet
            dsCompany = GetExternalApproverList(objDataOperation, "Company")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            Dim dsExtList As New DataSet

            For Each dRow As DataRow In dsCompany.Tables("Company").Rows
                strQ = StrFinalQurey
                StrQDtFilters = ""

                If dRow("dbname").ToString.Trim.Length <= 0 Then
                    strQ = strQ.Replace("#APPROVER_CODE#", "ISNULL(cfuser_master.username,'') ")
                    strQ = strQ.Replace("#APPROVER_NAME#", "ISNULL(cfuser_master.username,'') ")
                    strQ = strQ.Replace("#DEPT_NAME#", "'' ")
                    strQ = strQ.Replace("#SEC_NAME#", "'' ")
                    strQ = strQ.Replace("#JOB_NAME#", "'' ")
                    strQ = strQ.Replace("#EMPLOYEE_JOIN#", "LEFT JOIN hrmsConfiguration..cfuser_master ON cfuser_master.userunkid  = trtrainingapprover_master.approverempunkid ")
                    strQ = strQ.Replace("#DATA_JOIN#", "")
                    strQ = strQ.Replace("#DB_NAME#", "")
                    strQ = strQ.Replace("#APPROVER_NAME_WITHOUT_CODE#", "ISNULL(cfuser_master.username,'') ")
                Else
                    xDateJoinQry = "" : xDateFilterQry = "" : xAdvanceJoinQry = ""
                    Call GetDatesFilterString(xDateJoinQry, xDateFilterQry, eZeeDate.convertDate(dRow("effectivedate")), eZeeDate.convertDate(dRow("effectivedate")), , , dRow("dbname").ToString)
                    Call GetAdvanceFilterQry(xAdvanceJoinQry, eZeeDate.convertDate(dRow("effectivedate")), dRow("dbname").ToString)

                    strQ = strQ.Replace("#APPROVER_CODE#", "ISNULL(cfuser_master.username,'') ")
                    strQ = strQ.Replace("#APPROVER_NAME#", "CASE WHEN ISNULL(hremployee_master.firstname, '') + ' ' + ISNULL(hremployee_master.surname, '') = ' ' THEN ISNULL(cfuser_master.username,'') " & _
                                                                "ELSE ISNULL(hremployee_master.employeecode,'') + ' - ' + ISNULL(hremployee_master.firstname, '') + ' ' + ISNULL(hremployee_master.surname, '') END ")
                    strQ = strQ.Replace("#DEPT_NAME#", "ISNULL(DPT.name,'') ")
                    strQ = strQ.Replace("#SEC_NAME#", "ISNULL(SEC.name,'') ")
                    strQ = strQ.Replace("#JOB_NAME#", "ISNULL(JOB.job_name,'') ")
                    strQ = strQ.Replace("#EMPLOYEE_JOIN#", "LEFT JOIN hrmsConfiguration..cfuser_master ON cfuser_master.userunkid = trtrainingapprover_master.approverempunkid " & _
                                                           "LEFT JOIN #DB_NAME#hremployee_master on hremployee_master.employeeunkid = cfuser_master.employeeunkid ")
                    strQ = strQ.Replace("#DATA_JOIN#", StrQDataJoin)
                    strQ = strQ.Replace("#DB_NAME#", dRow("dbname").ToString & "..")

                    strQ = strQ.Replace("#APPROVER_NAME_WITHOUT_CODE#", "CASE WHEN ISNULL(hremployee_master.firstname, '') + ' ' + ISNULL(hremployee_master.surname, '') = ' ' THEN ISNULL(cfuser_master.username,'') " & _
                                                                        "ELSE ISNULL(hremployee_master.employeecode,'') + ' - ' + ISNULL(hremployee_master.firstname, '') + ' ' + ISNULL(hremployee_master.surname, '') END ")

                    If xDateJoinQry.Trim.Length > 0 Then
                        strQ &= xDateJoinQry
                    End If

                    If xAdvanceJoinQry.Trim.Length > 0 Then
                        strQ &= xAdvanceJoinQry
                    End If

                End If

                strQ &= StrQCondition

                If xIncludeIn_ActiveEmployee = False Then
                    If xDateFilterQry.Trim.Length > 0 Then
                        StrQDtFilters &= xDateFilterQry & " "
                    End If
                End If

                strQ = strQ.Replace("#EXT_APPROVER#", "1")

                strQ &= " AND cfuser_master.companyunkid = " & dRow("companyunkid")

                objDataOperation.ClearParameters()
                objDataOperation.AddParameter("@YES", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 3, "Yes"))
                objDataOperation.AddParameter("@NO", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 4, "No"))

                dsExtList = objDataOperation.ExecQuery(strQ, strTableName)

                If objDataOperation.ErrorMessage <> "" Then
                    exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                    Throw exForce
                End If

                If dsList.Tables.Count <= 0 Then
                    dsList.Tables.Add(dsExtList.Tables(0).Copy)
                Else
                    dsList.Tables(0).Merge(dsExtList.Tables(0), True)
                End If

            Next

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: GetList; Module Name: " & mstrModuleName)
        Finally
            objMaster = Nothing
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
        Return dsList
    End Function

    ''' <summary>
    ''' Created By: Hemant
    ''' </summary>
    ''' <purpose> Get Approver Employee(s) Id(s) </purpose>
    Public Function GetApproverEmployeeId(ByVal intCalendarunkid As Integer, ByVal intApproverunkid As Integer, ByVal blnIsExternalApprover As Boolean, ByVal intTrainingTypeId As Integer) As String
        'Hemant (22 Dec 2022) -- [intCalendarunkid]
        Dim mstrEmployeeIds As String = ""
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception
        Try
            Dim objDataOperation As New clsDataOperation

            strQ = "SELECT  ISNULL " & _
                                "(STUFF " & _
                                        "(" & _
                                            "(SELECT ',' + CAST(trtrainingapprover_tran.employeeunkid AS VARCHAR(MAX)) " & _
                                            " FROM     trtrainingapprover_master " & _
                                            " JOIN trtrainingapprover_tran ON trtrainingapprover_master.approverunkid = trtrainingapprover_tran.approverunkid " & _
                                            "   AND trtrainingapprover_tran.isvoid = 0 " & _
                                            " WHERE trtrainingapprover_master.approverempunkid = " & intApproverunkid & " " & _
                                            "   AND trtrainingapprover_master.isvoid = 0 " & _
                                            "   AND trtrainingapprover_master.isswap = 0 " & _
                                            "   AND trtrainingapprover_master.isactive = 1 " & _
                                            "   AND trtrainingapprover_master.calendarunkid = @calendarunkid " & _
                                            "   AND trtrainingapprover_master.trainingtypeid = " & intTrainingTypeId & " " & _
                                            "   AND trtrainingapprover_master.isexternalapprover = @isexternalapprover " & _
                                            " ORDER BY trtrainingapprover_tran.employeeunkid " & _
                                            " FOR XML PATH('') " & _
                                            ") , 1, 1, '' " & _
                                        "), '' " & _
                                ") EmployeeIds "

            'Hemant (22 Dec 2022) -- Start
            'ENHANCEMENT(NMB) : A1X-22 - As a user, I want to have the ability to perform approver migrations incase of employee/approver movements
            objDataOperation.AddParameter("@calendarunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intCalendarunkid)
            'Hemant (22 Dec 2022) -- End	
            objDataOperation.AddParameter("@isexternalapprover", SqlDbType.Bit, eZeeDataType.BIT_SIZE, blnIsExternalApprover)

            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            If dsList.Tables("List").Rows.Count > 0 Then
                mstrEmployeeIds = dsList.Tables(0).Rows(0)("EmployeeIds").ToString()
            End If

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "GetApproverEmployeeId", mstrMessage)
        End Try
        Return mstrEmployeeIds
    End Function

    ''' <summary>
    ''' Modify By: Hemant
    ''' </summary>
    ''' <purpose> Insert into Database Table trtrainingapprover_master </purpose>
    Public Function Insert(ByVal dtAccess As DataTable) As Boolean

        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        If isExistApprover(mintApproverEmpunkid, mintLevelunkid, mintTrainingTypeId, , , mblnIsExternalApprover) = True Then
            mstrMessage = Language.getMessage(mstrModuleName, 1, "This Training Approver already exists. Please define new Training Approver.")
            Return False
        End If

        objDataOperation = New clsDataOperation
        objDataOperation.BindTransaction()
        Try

            objDataOperation.ClearParameters()
            objDataOperation.AddParameter("@levelunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintLevelunkid.ToString)
            objDataOperation.AddParameter("@approverempunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintApproverEmpunkid.ToString)
            objDataOperation.AddParameter("@userunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintUserunkid.ToString)
            objDataOperation.AddParameter("@isswap", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsswap)
            objDataOperation.AddParameter("@isactive", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsactive)
            objDataOperation.AddParameter("@isvoid", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsvoid.ToString)
            If mdtVoidDateTime <> Nothing Then
                objDataOperation.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, mdtVoidDateTime)
            Else
                objDataOperation.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, DBNull.Value)
            End If
            objDataOperation.AddParameter("@voiduserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintVoidUserunkid.ToString)
            objDataOperation.AddParameter("@voidreason", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrVoidReason.ToString)
            objDataOperation.AddParameter("@isexternalapprover", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsExternalApprover.ToString)
            objDataOperation.AddParameter("@calendarunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintCalendarunkid.ToString)
            objDataOperation.AddParameter("@trainingtypeid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintTrainingTypeId.ToString)

            strQ = "INSERT INTO trtrainingapprover_master (" & _
                        "  levelunkid" & _
                        " ,approverempunkid " & _
                        " ,userunkid " & _
                        " ,isswap " & _
                        " ,isactive " & _
                        " ,isvoid " & _
                        " ,voiddatetime " & _
                        " ,voiduserunkid " & _
                        " ,voidreason " & _
                        " ,isexternalapprover " & _
                        " ,calendarunkid " & _
                        " ,trainingtypeid" & _
                    ") VALUES (" & _
                        "  @levelunkid" & _
                        " ,@approverempunkid " & _
                        " ,@userunkid " & _
                        " ,@isswap " & _
                        " ,@isactive " & _
                        " ,@isvoid " & _
                        " ,@voiddatetime " & _
                        " ,@voiduserunkid " & _
                        " ,@voidreason " & _
                        " ,@isexternalapprover " & _
                        " ,@calendarunkid " & _
                        " ,@trainingtypeid" & _
                    "); SELECT @@identity"

            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ":" & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            mintApproverunkid = dsList.Tables(0).Rows(0).Item(0)

            If InsertAuditTrails(objDataOperation, 1) = False Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            objTrainingApproverTran._Approverunkid = mintApproverunkid
            objTrainingApproverTran._TranDataTable = dtAccess
            objTrainingApproverTran._UserId = mintUserunkid
            objTrainingApproverTran._ClientIP = mstrClientIP
            objTrainingApproverTran._FormName = mstrFormName
            objTrainingApproverTran._HostName = mstrHostName
            objTrainingApproverTran._IsWeb = mblnIsWeb

            If objTrainingApproverTran.Insert_Update_Delete(objDataOperation) = False Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            Dim objUserMapping As New clsapprover_Usermapping
            objUserMapping.GetData(enUserType.Training_Approver, mintApproverunkid, -1, -1)
            objUserMapping._Approverunkid = mintApproverunkid
            objUserMapping._Userunkid = mintMappedUserId
            objUserMapping._UserTypeid = enUserType.Training_Approver
            objUserMapping._AuditUserunkid = mintUserunkid

            If objUserMapping._Mappingunkid > 0 Then
                If objUserMapping.Update(objDataOperation) = False Then
                    exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                    Throw exForce
                End If
            Else
                If objUserMapping.Insert(objDataOperation) = False Then
                    exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                    Throw exForce
                End If
            End If

            objDataOperation.ReleaseTransaction(True)
            Return True
        Catch ex As Exception
            objDataOperation.ReleaseTransaction(False)
            Throw New Exception(ex.Message & "; Procedure Name: Insert , Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            objDataOperation = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
        End Try

    End Function

    ''' <summary>
    ''' Modify By: Hemant
    ''' </summary>
    ''' <purpose> Update Database Table trtrainingapprover_master </purpose>
    Public Function Update(ByVal dtAccess As DataTable, Optional ByVal dtMapping As DataTable = Nothing) As Boolean
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        If isExistApprover(mintApproverEmpunkid, mintLevelunkid, mintTrainingTypeId, mintApproverunkid) = True Then
            mstrMessage = Language.getMessage(mstrModuleName, 1, "This Training Approver already exists. Please define new Training Approver.")
            Return False
        End If

        objDataOperation = New clsDataOperation
        objDataOperation.BindTransaction()

        Try
            objDataOperation.ClearParameters()
            objDataOperation.AddParameter("@approverunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintApproverunkid.ToString)
            objDataOperation.AddParameter("@levelunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintLevelunkid.ToString)
            objDataOperation.AddParameter("@approverempunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintApproverEmpunkid.ToString)
            objDataOperation.AddParameter("@userunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintUserunkid.ToString)
            objDataOperation.AddParameter("@isswap", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsswap.ToString)
            objDataOperation.AddParameter("@isactive", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsactive.ToString)
            objDataOperation.AddParameter("@isvoid", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsvoid.ToString)
            If mdtVoidDateTime <> Nothing Then
                objDataOperation.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, mdtVoidDateTime)
            Else
                objDataOperation.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, DBNull.Value)
            End If
            objDataOperation.AddParameter("@voiduserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintVoidUserunkid.ToString)
            objDataOperation.AddParameter("@voidreason", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrVoidReason.ToString)
            objDataOperation.AddParameter("@isexternalapprover", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsExternalApprover.ToString)
            objDataOperation.AddParameter("@calendarunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintCalendarunkid.ToString)
            objDataOperation.AddParameter("@trainingtypeid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintTrainingTypeId.ToString)

            strQ = "UPDATE trtrainingapprover_master  SET " & _
                    " levelunkid = @levelunkid " & _
                    " ,approverempunkid = @approverempunkid " & _
                    " ,userunkid = @userunkid " & _
                    " ,isswap = @isswap " & _
                    " ,isactive = @isactive " & _
                    " ,isvoid = @isvoid " & _
                    " ,voiddatetime = @voiddatetime " & _
                    " ,voiduserunkid = @voiduserunkid " & _
                    " ,voidreason = @voidreason " & _
                    " ,isexternalapprover = @isexternalapprover " & _
                    " ,calendarunkid = @calendarunkid " & _
                    " ,trainingtypeid = @trainingtypeid " & _
                " WHERE approverunkid = @approverunkid"

            Call objDataOperation.ExecNonQuery(strQ)

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ":" & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            If InsertAuditTrails(objDataOperation, 2) = False Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            objTrainingApproverTran._Approverunkid = mintApproverunkid
            objTrainingApproverTran._TranDataTable = dtAccess
            objTrainingApproverTran._UserId = mintUserunkid
            objTrainingApproverTran._ClientIP = mstrClientIP
            objTrainingApproverTran._FormName = mstrFormName
            objTrainingApproverTran._HostName = mstrHostName
            objTrainingApproverTran._IsWeb = mblnIsWeb

            If objTrainingApproverTran.Insert_Update_Delete(objDataOperation) = False Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            Dim objUserMapping As New clsapprover_Usermapping
            objUserMapping.GetData(enUserType.Training_Approver, mintApproverunkid, -1, -1)
            objUserMapping._Approverunkid = mintApproverunkid
            objUserMapping._Userunkid = mintMappedUserId
            objUserMapping._UserTypeid = enUserType.Training_Approver
            objUserMapping._AuditUserunkid = mintUserunkid

            If objUserMapping._Mappingunkid > 0 Then
                If objUserMapping.Update(objDataOperation) = False Then
                    exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                    Throw exForce
                End If
            Else
                If objUserMapping.Insert(objDataOperation) = False Then
                    exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                    Throw exForce
                End If
            End If

            'Hemant (16 Sep 2022) -- Start
            'ISSUE/ENHANCEMENT(NMB) : Pending Training approval should be go for New User instead of Old User  after changing User  in Training Approver Add/Edit Screen
            If mintMappedUserId <> mintOldMappedUserId Then
                objTrainingApproverTran.UpdateMapedUser(mintMappedUserId, mintApproverunkid, objDataOperation)
            End If
            'Hemant (16 Sep 2022) -- End

            objDataOperation.ReleaseTransaction(True)
            Return True
        Catch ex As Exception
            objDataOperation.ReleaseTransaction(False)
            Throw New Exception(ex.Message & "; Procedure Name: Update , Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            objDataOperation = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
        End Try

    End Function

    Public Function Delete(ByVal intUnkid As Integer, Optional ByVal objDataOp As clsDataOperation = Nothing) As Boolean
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        If objDataOp Is Nothing Then
            objDataOperation = New clsDataOperation
            objDataOperation.BindTransaction()
        Else
            objDataOperation = objDataOp
            objDataOperation.ClearParameters()
        End If
        Dim objCommonATLog As New clsCommonATLog
        Try

            dsList = objCommonATLog.GetChildList(objDataOperation, "trtrainingapprover_tran", "approverunkid", intUnkid)

            strQ = "UPDATE trtrainingapprover_tran SET " & _
                    "  isvoid = @isvoid " & _
                    " ,voiddatetime = @voiddatetime " & _
                    " ,voiduserunkid = @voiduserunkid " & _
                    " ,voidreason = @voidreason " & _
                " WHERE approvertranunkid = @approvertranunkid AND isvoid = 0 "

            If dsList.Tables(0).Rows.Count > 0 Then
                For Each drRow As DataRow In dsList.Tables(0).Rows
                    objDataOperation.ClearParameters()
                    objDataOperation.AddParameter("@isvoid", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsvoid.ToString)
                    objDataOperation.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, mdtVoidDateTime)
                    objDataOperation.AddParameter("@voiduserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintVoidUserunkid.ToString)
                    objDataOperation.AddParameter("@voidreason", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrVoidReason)
                    objDataOperation.AddParameter("@approvertranunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, drRow.Item("approvertranunkid"))

                    Call objDataOperation.ExecNonQuery(strQ)

                    If objDataOperation.ErrorMessage <> "" Then
                        exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                        Throw exForce
                    End If

                    objTrainingApproverTran._ApproverTranunkid(objDataOperation) = CInt(drRow.Item("approvertranunkid"))
                    objTrainingApproverTran._Approverunkid = CInt(drRow.Item("approverunkid"))
                    objTrainingApproverTran._UserId = mintVoidUserunkid
                    objTrainingApproverTran._ClientIP = mstrClientIP
                    objTrainingApproverTran._FormName = mstrFormName
                    objTrainingApproverTran._HostName = mstrHostName
                    objTrainingApproverTran._IsWeb = mblnIsWeb

                    If objTrainingApproverTran.InsertAuditTrails(objDataOperation, 3, CInt(drRow.Item("employeeunkid"))) = False Then
                        exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                        Throw exForce
                    End If

                Next
            End If


            strQ = "UPDATE trtrainingapprover_master SET " & _
                    "  isvoid = @isvoid " & _
                    " ,voiddatetime = @voiddatetime " & _
                    " ,voiduserunkid = @voiduserunkid " & _
                    " ,voidreason = @voidreason " & _
                " WHERE approverunkid = @approverunkid AND isvoid = 0 "

            objDataOperation.ClearParameters()
            objDataOperation.AddParameter("@isvoid", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsvoid.ToString)
            objDataOperation.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, mdtVoidDateTime)
            objDataOperation.AddParameter("@voiduserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintVoidUserunkid.ToString)
            objDataOperation.AddParameter("@voidreason", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrVoidReason)
            objDataOperation.AddParameter("@approverunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intUnkid.ToString)

            Call objDataOperation.ExecNonQuery(strQ)

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            _Approverunkid(objDataOperation) = intUnkid

            If InsertAuditTrails(objDataOperation, 3) = False Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            objDataOperation.ReleaseTransaction(True)
            Return True
        Catch ex As Exception
            objDataOperation.ReleaseTransaction(False)
            Throw New Exception(ex.Message & "; Procedure Name: Delete , Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            objDataOperation = Nothing
            If dsList IsNot Nothing Then
                dsList.Dispose()
            End If
            objCommonATLog = Nothing
        End Try

    End Function

    Public Function isExistApprover(ByVal intTrainingApproverEmpunkid As Integer, ByVal intApproveLevelunkid As Integer, _
                                    ByVal intTrainingTypeId As Integer, _
                                    Optional ByVal intunkid As Integer = -1, Optional ByRef intApproverMstId As Integer = 0, _
                                    Optional ByVal blnIsExternalApprover As Boolean = False) As Boolean

        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        objDataOperation = New clsDataOperation
        Try
            strQ = "SELECT " & _
                    "  trtrainingapprover_master.approverunkid " & _
                    " ,trtrainingapprover_master.levelunkid " & _
                    " ,trtrainingapprover_master.approverempunkid " & _
                    " ,trtrainingapprover_master.userunkid " & _
                    " ,trtrainingapprover_master.isvoid " & _
                    " ,trtrainingapprover_master.voiddatetime " & _
                    " ,trtrainingapprover_master.voiduserunkid " & _
                    " ,trtrainingapprover_master.voidreason " & _
                    " ,trtrainingapprover_master.isactive " & _
                    " ,ISNULL(trtrainingapprover_master.trainingtypeid, 0) AS trainingtypeid " & _
                    " FROM trtrainingapprover_master " & _
                   " WHERE    trtrainingapprover_master.approverempunkid = @approverempunkid " & _
                        " AND trtrainingapprover_master.levelunkid = @levelunkid " & _
                        " AND trtrainingapprover_master.isvoid = 0 " & _
                        " AND trtrainingapprover_master.isswap = 0 " & _
                        " AND isexternalapprover = @isexternalapprover " & _
                        " AND trainingtypeid = @trainingtypeid "

            objDataOperation.ClearParameters()
            objDataOperation.AddParameter("@levelunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intApproveLevelunkid)
            objDataOperation.AddParameter("@approverempunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intTrainingApproverEmpunkid)
            objDataOperation.AddParameter("@isexternalapprover", SqlDbType.Bit, eZeeDataType.BIT_SIZE, blnIsExternalApprover)
            objDataOperation.AddParameter("@trainingtypeid", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, intTrainingTypeId)

            If intunkid > 0 Then
                strQ &= " AND trtrainingapprover_master.approverunkid <> @approverunkid"
                objDataOperation.AddParameter("@approverunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intunkid)
            End If

            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            If dsList.Tables(0).Rows.Count > 0 Then
                intApproverMstId = dsList.Tables(0).Rows(0)("approverunkid")
            End If

            Return dsList.Tables(0).Rows.Count > 0
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: isExistApprover; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
    End Function

    Public Function MakeActiveInActive(ByVal intUnkid As Integer) As Boolean
        Dim strQ As String = ""
        Dim exForce As Exception

        objDataOperation = New clsDataOperation
        objDataOperation.BindTransaction()

        Try

            objDataOperation.ClearParameters()
            objDataOperation.AddParameter("@isactive", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsactive.ToString)
            objDataOperation.AddParameter("@approverunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intUnkid.ToString)

            strQ = "UPDATE trtrainingapprover_master  SET " & _
                      " isactive = @isactive " & _
                      " WHERE approverunkid = @approverunkid AND isvoid = 0 AND trtrainingapprover_master.isswap = 0 "

            Call objDataOperation.ExecNonQuery(strQ)

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ":" & objDataOperation.ErrorMessage)
                Throw exForce
            End If


            If InsertAuditTrails(objDataOperation, 2) = False Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            objDataOperation.ReleaseTransaction(True)
            Return True

        Catch ex As Exception
            objDataOperation.ReleaseTransaction(False)
            Throw New Exception(ex.Message & "; Procedure Name: MakeActiveInActive , Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            objDataOperation = Nothing
        End Try
    End Function

    Public Function InsertAuditTrails(ByVal objDataOperation As clsDataOperation, ByVal AuditType As Integer) As Boolean
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        Try

            objDataOperation.ClearParameters()
            objDataOperation.AddParameter("@approverunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintApproverunkid.ToString)
            objDataOperation.AddParameter("@levelunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintLevelunkid.ToString)
            objDataOperation.AddParameter("@approverempunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintApproverEmpunkid.ToString)
            objDataOperation.AddParameter("@isswap", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsswap)
            objDataOperation.AddParameter("@isactive", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsactive)
            objDataOperation.AddParameter("@audittype", SqlDbType.Int, eZeeDataType.INT_SIZE, AuditType.ToString)
            objDataOperation.AddParameter("@audituserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintUserunkid)
            objDataOperation.AddParameter("@auditdatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, ConfigParameter._Object._CurrentDateAndTime)
            objDataOperation.AddParameter("@ip", SqlDbType.NVarChar, eZeeDataType.IP_SIZE, mstrClientIP)
            objDataOperation.AddParameter("@machine_name", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrHostName)
            objDataOperation.AddParameter("@form_name", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrFormName)
            objDataOperation.AddParameter("@isweb", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsWeb)
            objDataOperation.AddParameter("@isexternalapprover", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsExternalApprover)
            objDataOperation.AddParameter("@calendarunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintCalendarunkid)
            objDataOperation.AddParameter("@trainingtypeid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintTrainingTypeId.ToString)

            strQ = "INSERT INTO attrtrainingapprover_master (" & _
                        "  tranguid " & _
                        ", approverunkid " & _
                        ", levelunkid" & _
                        ", approverempunkid " & _
                        ", isswap " & _
                        ", isactive " & _
                        ", audittype " & _
                        ", audituserunkid " & _
                        ", auditdatetime " & _
                        ", ip " & _
                        ", machine_name " & _
                        ", form_name " & _
                        ", isweb " & _
                        ", isexternalapprover " & _
                        ", calendarunkid " & _
                        ", trainingtypeid " & _
                    ") VALUES (" & _
                        "  LOWER(NEWID()) " & _
                        ", @approverunkid " & _
                        " ,@levelunkid" & _
                        " ,@approverempunkid " & _
                        ", @isswap " & _
                        " ,@isactive " & _
                        ", @audittype " & _
                        ", @audituserunkid " & _
                        ", @auditdatetime " & _
                        ", @ip " & _
                        ", @machine_name " & _
                        ", @form_name " & _
                        ", @isweb " & _
                        ", @isexternalapprover " & _
                        ", @calendarunkid " & _
                        ", @trainingtypeid " & _
                    "); SELECT @@identity"

            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ":" & objDataOperation.ErrorMessage)
                Throw exForce
            End If
            Return True
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: InsertAuditTrails , Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            objDataOperation = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
        End Try

    End Function

    Public Function ImportApproverInsert() As Integer

        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        objDataOperation = New clsDataOperation

        Try
            objDataOperation.AddParameter("@levelunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintLevelunkid.ToString)
            objDataOperation.AddParameter("@approverempunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintApproverEmpunkid.ToString)
            objDataOperation.AddParameter("@userunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintUserunkid.ToString)
            objDataOperation.AddParameter("@isvoid", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsvoid.ToString)
            objDataOperation.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, IIf(mdtVoidDateTime <> Nothing, mdtVoidDateTime, DBNull.Value))
            objDataOperation.AddParameter("@voiduserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintVoidUserunkid.ToString)
            objDataOperation.AddParameter("@voidreason", SqlDbType.NVarChar, eZeeDataType.DESC_SIZE, mstrVoidReason.ToString)
            objDataOperation.AddParameter("@isswap", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsswap.ToString)
            objDataOperation.AddParameter("@isactive", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsactive.ToString)
            objDataOperation.AddParameter("@isexternalapprover", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsExternalApprover.ToString)
            objDataOperation.AddParameter("@calendarunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintCalendarunkid.ToString)
            objDataOperation.AddParameter("@trainingtypeid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintTrainingTypeId.ToString)

            strQ = "INSERT INTO trtrainingapprover_master (" & _
                        "  levelunkid" & _
                        " ,approverempunkid " & _
                        " ,userunkid " & _
                        " ,isswap " & _
                        " ,isactive " & _
                        " ,isvoid " & _
                        " ,voiddatetime " & _
                        " ,voiduserunkid " & _
                        " ,voidreason " & _
                        " ,isexternalapprover " & _
                        " ,calendarunkid " & _
                        " ,trainingtypeid " & _
                  ") VALUES (" & _
                        "  @levelunkid" & _
                        " ,@approverempunkid " & _
                        " ,@userunkid " & _
                        " ,@isswap " & _
                        " ,@isactive " & _
                        " ,@isvoid " & _
                        " ,@voiddatetime " & _
                        " ,@voiduserunkid " & _
                        " ,@voidreason " & _
                        " ,@isexternalapprover " & _
                        " ,@calendarunkid " & _
                        " ,@trainingtypeid " & _
                    "); SELECT @@identity"

            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            mintApproverunkid = dsList.Tables(0).Rows(0).Item(0)

            If InsertAuditTrails(objDataOperation, 1) = False Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            If mblnIsExternalApprover = True Then
                Dim objUserMapping As New clsapprover_Usermapping
                objUserMapping.GetData(enUserType.Training_Approver, mintApproverunkid, -1, -1)

                objUserMapping._Approverunkid = mintApproverunkid
                objUserMapping._Userunkid = mintMappedUserId
                objUserMapping._UserTypeid = enUserType.Training_Approver
                objUserMapping._AuditUserunkid = mintUserunkid

                If objUserMapping._Mappingunkid > 0 Then
                    If objUserMapping.Update(objDataOperation) = False Then
                        exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                        Throw exForce
                    End If
                Else
                    If objUserMapping.Insert(objDataOperation) = False Then
                        exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                        Throw exForce
                    End If
                End If
            Else
                strQ = "SELECT ISNULL(userunkid,0) AS UsrId " & _
                       "FROM hremployee_master " & _
                       "JOIN hrmsConfiguration..cfuser_master ON hremployee_master.displayname = hrmsConfiguration..cfuser_master.username " & _
                       "WHERE hremployee_master.employeeunkid = '" & mintApproverEmpunkid & "' "

                dsList = objDataOperation.ExecQuery(strQ, "Usr")

                If objDataOperation.ErrorMessage <> "" Then
                    exForce = New Exception(objDataOperation.ErrorNumber & " : " & objDataOperation.ErrorMessage)
                    Throw exForce
                End If

                If dsList.Tables("Usr").Rows.Count > 0 Then
                    If dsList.Tables("Usr").Rows(0).Item("UsrId") > 0 Then
                        Dim objUserMapping As New clsapprover_Usermapping
                        objUserMapping.GetData(enUserType.Training_Approver, mintApproverunkid, -1, -1)
                        objUserMapping._Approverunkid = mintApproverunkid
                        objUserMapping._Userunkid = dsList.Tables("Usr").Rows(0).Item("UsrId")
                        objUserMapping._UserTypeid = enUserType.Training_Approver
                        objUserMapping._AuditUserunkid = mintUserunkid

                        If objUserMapping._Mappingunkid > 0 Then
                            If objUserMapping.Update(objDataOperation) = False Then
                                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                                Throw exForce
                            End If
                        Else
                            If objUserMapping.Insert(objDataOperation) = False Then
                                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                                Throw exForce
                            End If
                        End If
                    End If
                End If
            End If

            Return mintApproverunkid

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: Insert; Module Name: " & mstrModuleName)
            Return False
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
    End Function

    Public Function GetEmployeeApprover(ByVal xDatabaseName As String, _
                                        ByVal xUserUnkid As Integer, _
                                        ByVal xYearUnkid As Integer, _
                                        ByVal xCompanyUnkid As Integer, _
                                        ByVal intEmployeeID As Integer, _
                                        ByVal intCalandarUnkid As Integer, _
                                        ByVal intTrainingTypeID As Integer, _
                                        Optional ByVal mstrFilter As String = "" _
                                        , Optional ByVal objDataOpr As clsDataOperation = Nothing _
                                        ) As DataTable

        'Hemant (22 Dec 2022) -- [intCalandarUnkid]
        'Hemant (25 Jul 2022) -- [intTrainingTypeID,objDataOpr]
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception
        Try
            Dim objDataOperation As New clsDataOperation
            'Hemant (25 Jul 2022) -- Start            
            'ENHANCEMENT(NMB) : AC2-648 - As a user, I want to be able to define approval amount limits (ranges) that will be tied to the training request approval levels
            If objDataOpr IsNot Nothing Then
                objDataOperation = objDataOpr
            Else
                objDataOperation = New clsDataOperation
            End If
            'Hemant (25 Jul 2022) -- End
            Dim strQFinal As String = String.Empty
            Dim strQCondition As String = String.Empty

            objDataOperation.ClearParameters()

            strQ &= " SELECT " & _
                      "  trtrainingapprover_master.approverunkid AS trapproverunkid " & _
                      ", trtrainingapprover_tran.approvertranunkid " & _
                      ", trtrainingapprover_master.approverempunkid " & _
                      ", trtrainingapprover_tran.employeeunkid " & _
                      ", #CODE# AS employeecode " & _
                      ", #APPROVER_NAME# AS employeename " & _
                      ", hrtraining_approverlevel_master.levelunkid " & _
                      ", hrtraining_approverlevel_master.levelname " & _
                      ", hrtraining_approverlevel_master.priority " & _
                      ", trtrainingapprover_master.isexternalapprover " & _
                      ", hrapprover_usermapping.userunkid AS mapuserunkid " & _
                      ", trtrainingapprover_master.trainingtypeid " & _
                      " FROM trtrainingapprover_tran " & _
                      " JOIN trtrainingapprover_master ON trtrainingapprover_tran.approverunkid = trtrainingapprover_master.approverunkid " & _
                      "     AND trtrainingapprover_master.isvoid = 0 AND trtrainingapprover_master.isactive = 1 AND trtrainingapprover_master.isswap = 0 " & _
                      " #EMPLOYEE_JOIN# " & _
                      " JOIN hrtraining_approverlevel_master ON hrtraining_approverlevel_master.levelunkid = trtrainingapprover_master.levelunkid "

            strQ &= " JOIN hrapprover_usermapping ON hrapprover_usermapping.approverunkid = trtrainingapprover_master.approverunkid AND hrapprover_usermapping.usertypeid=" & enUserType.Training_Approver

            strQFinal = strQ

            strQCondition = " WHERE trtrainingapprover_tran.isvoid = 0  AND trtrainingapprover_tran.employeeunkid = " & intEmployeeID

            strQCondition &= " AND trtrainingapprover_master.isexternalapprover = #EXT_APPROVER# "

            'Hemant (22 Dec 2022) -- Start
            'ENHANCEMENT(NMB) : A1X-22 - As a user, I want to have the ability to perform approver migrations incase of employee/approver movements
            strQCondition &= " AND trtrainingapprover_master.calendarunkid = " & intCalandarUnkid
            'Hemant (22 Dec 2022) -- End

            strQCondition &= " AND trtrainingapprover_master.trainingtypeid = " & intTrainingTypeID


            If mstrFilter.Trim.Length > 0 Then
                strQCondition &= " AND " & mstrFilter.Trim
            End If

            strQ &= strQCondition

            strQ = strQ.Replace("#CODE#", "ISNULL(hremployee_master.employeecode,'') ")
            strQ = strQ.Replace("#APPROVER_NAME#", "ISNULL(hremployee_master.firstname,'') + ' ' + ISNULL(hremployee_master.othername, '') + ' ' + ISNULL(hremployee_master.surname,'') ")
            strQ = strQ.Replace("#EMPLOYEE_JOIN#", "LEFT JOIN hremployee_master ON hremployee_master.employeeunkid = trtrainingapprover_master.approverempunkid AND hremployee_master.isapproved = 1 ")
            strQ = strQ.Replace("#EXT_APPROVER#", "0")

            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            Dim dsCompany As DataSet
            Dim objApprover As New clstrainingapprover_master_emp_map

            dsCompany = objApprover.GetExternalApproverList(objDataOperation, "Company")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            Dim dsExtList As New DataSet

            For Each dRow In dsCompany.Tables("Company").Rows
                strQ = strQFinal

                If dRow("dbname").ToString.Trim.Length <= 0 Then
                    strQ = strQ.Replace("#CODE#", "'' ")
                    strQ = strQ.Replace("#APPROVER_NAME#", "ISNULL(cfuser_master.username,'') ")
                    strQ = strQ.Replace("#EMPLOYEE_JOIN#", "LEFT JOIN hrmsConfiguration..cfuser_master ON cfuser_master.userunkid  = trtrainingapprover_master.approverempunkid ")

                Else
                    strQ = strQ.Replace("#CODE#", "CASE WHEN ISNULL(hremployee_master.firstname, '') + ' ' + ISNULL(hremployee_master.surname, '') = ' ' THEN '' " & _
                                                       "ELSE ISNULL(hremployee_master.employeecode,'') END ")
                    strQ = strQ.Replace("#APPROVER_NAME#", "CASE WHEN ISNULL(hremployee_master.firstname, '') + ' ' + ISNULL(hremployee_master.surname, '') = ' ' THEN ISNULL(cfuser_master.username,'') " & _
                                                                "ELSE ISNULL(hremployee_master.firstname, '') + ' ' + ISNULL(hremployee_master.surname, '') END ")
                    strQ = strQ.Replace("#EMPLOYEE_JOIN#", "LEFT JOIN hrmsConfiguration..cfuser_master ON cfuser_master.userunkid = trtrainingapprover_master.approverempunkid " & _
                                                           "LEFT JOIN #DB_NAME#hremployee_master on hremployee_master.employeeunkid = cfuser_master.employeeunkid ")
                    strQ = strQ.Replace("#DB_NAME#", dRow("dbname").ToString & "..")
                End If

                strQ &= strQCondition
                strQ = strQ.Replace("#EXT_APPROVER#", "1")
                strQ &= " AND cfuser_master.companyunkid = " & dRow("companyunkid")

                objDataOperation.ClearParameters()

                dsExtList = objDataOperation.ExecQuery(strQ, "List")

                If objDataOperation.ErrorMessage <> "" Then
                    exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                    Throw exForce
                End If

                If dsList.Tables.Count <= 0 Then
                    dsList.Tables.Add(dsExtList.Tables("List").Copy)
                Else
                    dsList.Tables("List").Merge(dsExtList.Tables("List"), True)
                End If
            Next

            Dim dtTable As DataTable
            dtTable = New DataView(dsList.Tables("List"), "", "priority,employeename", DataViewRowState.CurrentRows).ToTable.Copy
            dsList.Tables.RemoveAt(0)
            dsList.Tables.Add(dtTable.Copy)

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "GetEmployeeApprover", mstrMessage)
        Finally
            exForce = Nothing
            'Hemant (25 Jul 2022) -- Start            
            'ENHANCEMENT(NMB) : AC2-648 - As a user, I want to be able to define approval amount limits (ranges) that will be tied to the training request approval levels
            'objDataOperation = Nothing
            If objDataOpr Is Nothing Then objDataOperation = Nothing
            'Hemant (25 Jul 2022) -- End
        End Try
        If dsList IsNot Nothing AndAlso dsList.Tables.Count > 0 Then
            Return dsList.Tables("List")
        Else
            Return Nothing
        End If
    End Function

    'Hemant (22 Dec 2022) -- Start
    'ENHANCEMENT(NMB) : A1X-22 - As a user, I want to have the ability to perform approver migrations incase of employee/approver movements
    Public Function getListForCombo(ByVal intCalendarUnkId As Integer, _
                                    ByVal intTrainingTypeId As Integer, _
                                    Optional ByVal strListName As String = "List", _
                                    Optional ByVal mblFlag As Boolean = False, _
                                    Optional ByVal blnIsActive As Boolean = True, _
                                    Optional ByVal mblnShowLevels As Boolean = False, _
                                    Optional ByVal xIncludeIn_ActiveEmployee As Boolean = True, _
                                    Optional ByVal strEmployeeAsOnDate As String = "", _
                                    Optional ByVal xDatabaseName As String = "") As DataSet

        Dim dsList As New DataSet
        Dim objDataOperation As New clsDataOperation
        Dim strQ As String = String.Empty
        Dim exForce As Exception

        Try
            Dim strFinal As String = String.Empty
            Dim strQCondition As String = String.Empty
            Dim strSelect As String = String.Empty
            Dim strCombine As String = String.Empty


            Dim StrQDtFilters As String = String.Empty
            Dim xDateJoinQry, xDateFilterQry, xAdvanceJoinQry As String
            xDateJoinQry = "" : xDateFilterQry = "" : xAdvanceJoinQry = ""
            If xIncludeIn_ActiveEmployee = False Then
                Call GetDatesFilterString(xDateJoinQry, xDateFilterQry, eZeeDate.convertDate(strEmployeeAsOnDate), eZeeDate.convertDate(strEmployeeAsOnDate), , , xDatabaseName)
            End If

            If mblFlag = True Then
                strSelect = "SELECT  0 AS approverunkid, 0 AS empapproverunkid, ' ' +  @approvername  as name, 0 AS levelunkid, '' AS isexternalapprover "
                strSelect &= " UNION "
            End If

            strQ &= "SELECT " & _
                    "   trtrainingapprover_master.approverunkid " & _
                    "  ,trtrainingapprover_master.approverempunkid AS empapproverunkid "

            If mblnShowLevels = True Then
                strQ &= "  ,#APPROVER_NAME# + ' - ' + ISNULL(hrtraining_approverlevel_master.levelname,'') AS name "
            Else
                strQ &= "  ,#APPROVER_NAME# AS name "
            End If

            strQ &= "  ,trtrainingapprover_master.levelunkid AS levelunkid " & _
                    "  ,trtrainingapprover_master.isexternalapprover AS isexternalapprover " & _
                    "FROM trtrainingapprover_master " & _
                        " #EMPLOYEE_JOIN# "

            If mblnShowLevels = True Then
                strQ &= " JOIN hrtraining_approverlevel_master ON hrtraining_approverlevel_master.levelunkid = trtrainingapprover_master.levelunkid "
            End If

            strFinal = strQ

            strCombine = strSelect
            strCombine &= strQ
            strQ = strCombine
            strCombine = ""

            If xDateJoinQry.Trim.Length > 0 Then
                strQ &= xDateJoinQry
            End If


            strQCondition &= " WHERE   trtrainingapprover_master.isvoid = 0 AND trtrainingapprover_master.isswap = 0 " & _
                            " AND trtrainingapprover_master.isactive = " & IIf(blnIsActive, 1, 0) & ""

            strQCondition &= " AND trtrainingapprover_master.isexternalapprover = #EXT_APPROVER# "

            If intCalendarUnkId > 0 Then
                strQCondition &= " AND trtrainingapprover_master.calendarunkid = " & intCalendarUnkId & " "
            End If

            If intTrainingTypeId > 0 Then
                strQCondition &= " AND trtrainingapprover_master.trainingtypeid = " & intTrainingTypeId & " "
            End If


            If xIncludeIn_ActiveEmployee = False Then
                If xDateFilterQry.Trim.Length > 0 Then
                    StrQDtFilters &= xDateFilterQry & " "
                End If
            End If

            strQ &= strQCondition

            strQ = strQ.Replace("#APPROVER_NAME#", "ISNULL(hremployee_master.firstname, '') + ' ' + ISNULL(hremployee_master.othername, '') + ' ' + ISNULL(hremployee_master.surname, '') ")
            strQ = strQ.Replace("#EMPLOYEE_JOIN#", "LEFT JOIN hremployee_master ON hremployee_master.employeeunkid = trtrainingapprover_master.approverempunkid AND hremployee_master.isapproved = 1 ")
            strQ = strQ.Replace("#EXT_APPROVER#", "0")

            strQ &= StrQDtFilters

            objDataOperation.ClearParameters()

            objDataOperation.AddParameter("@approvername", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 2, "Select"))

            dsList = objDataOperation.ExecQuery(strQ, strListName)

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            Dim dsCompany As DataSet

            dsCompany = GetExternalApproverList(objDataOperation, "Company")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            Dim dsExtList As New DataSet

            For Each dRow As DataRow In dsCompany.Tables("Company").Rows
                strQ = strFinal
                If dRow("dbname").ToString.Trim.Length <= 0 Then
                    strQ = strQ.Replace("#APPROVER_NAME#", "ISNULL(cfuser_master.username,'') ")
                    strQ = strQ.Replace("#EMPLOYEE_JOIN#", "LEFT JOIN hrmsConfiguration..cfuser_master ON cfuser_master.userunkid  = trtrainingapprover_master.approverempunkid ")
                Else
                    strQ = strQ.Replace("#APPROVER_NAME#", "CASE WHEN ISNULL(hremployee_master.firstname, '') + ' ' + ISNULL(hremployee_master.othername, '') + ' ' + ISNULL(hremployee_master.surname, '') = ' ' THEN ISNULL(cfuser_master.username,'') " & _
                                                                "ELSE ISNULL(hremployee_master.firstname, '') + ' ' + ISNULL(hremployee_master.othername, '') + ' ' + ISNULL(hremployee_master.surname, '') END ")
                    strQ = strQ.Replace("#EMPLOYEE_JOIN#", "LEFT JOIN hrmsConfiguration..cfuser_master ON cfuser_master.userunkid = trtrainingapprover_master.approverempunkid " & _
                                                           "LEFT JOIN #DB_NAME#hremployee_master ON hremployee_master.employeeunkid = cfuser_master.employeeunkid ")
                    strQ = strQ.Replace("#DB_NAME#", dRow("dbname").ToString & "..")
                End If

                strQ &= strQCondition
                strQ = strQ.Replace("#EXT_APPROVER#", "1")
                strQ &= " AND cfuser_master.companyunkid = " & dRow("companyunkid")

                objDataOperation.ClearParameters()

                objDataOperation.AddParameter("@approvername", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 2, "Select"))

                dsExtList = objDataOperation.ExecQuery(strQ, strListName)

                If objDataOperation.ErrorMessage <> "" Then
                    exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                    Throw exForce
                End If

                If dsList.Tables.Count <= 0 Then
                    dsList.Tables.Add(dsExtList.Tables(strListName).Copy)
                Else
                    dsList.Tables(strListName).Merge(dsExtList.Tables(strListName), True)
                End If
            Next

            Dim dtTable As DataTable
            dtTable = New DataView(dsList.Tables(strListName), "", "levelunkid, name", DataViewRowState.CurrentRows).ToTable.Copy
            dsList.Tables.RemoveAt(0)
            dsList.Tables.Add(dtTable.Copy)

            Return dsList
        Catch ex As Exception
            DisplayError.Show(-1, ex.Message, "getListForCombo", mstrModuleName)
            Return Nothing
        Finally
            exForce = Nothing
            objDataOperation = Nothing
            dsList.Dispose()
            dsList = Nothing
        End Try

    End Function

    Public Function GetLevelFromTrainingApprover(ByVal intCalendarunkid As Integer, ByVal intTrainingTypeid As Integer, ByVal intTrainingApproverEmpunkid As Integer, ByVal blnIsExternalApprover As Boolean, Optional ByVal blnFlag As Boolean = True) As DataSet

        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception
        Try
            Dim objDataOperation As New clsDataOperation

            Dim strApproverunkid As String = ""
            strApproverunkid = GetApproverUnkid(intCalendarunkid, intTrainingTypeid, intTrainingApproverEmpunkid, blnIsExternalApprover, objDataOperation)

            If blnFlag Then
                strQ = " Select 0 as levelunkid , @name as name UNION "
            End If

            strQ &= " SELECT  trtrainingapprover_master.levelunkid,levelname as name " & _
                      " FROM trtrainingapprover_master " & _
                      " JOIN hrtraining_approverlevel_master ON trtrainingapprover_master.levelunkid = hrtraining_approverlevel_master.levelunkid AND hrtraining_approverlevel_master.isactive = 1 " & _
                      " WHERE  trtrainingapprover_master.isvoid = 0 AND trtrainingapprover_master.isactive = 1 AND trtrainingapprover_master.isswap = 0 "

            If intCalendarunkid > 0 Then
                strQ &= " AND trtrainingapprover_master.calendarunkid = " & intCalendarunkid
            End If

            If intTrainingTypeid > 0 Then
                strQ &= " AND trtrainingapprover_master.trainingtypeid = " & intTrainingTypeid
            End If

            If intTrainingApproverEmpunkid > 0 Then
                strQ &= " AND approverempunkid = " & intTrainingApproverEmpunkid
            End If

            If strApproverunkid.Trim.Length > 0 Then
                strQ &= " AND trtrainingapprover_master.approverunkid IN (" & strApproverunkid & ") "
            End If

            objDataOperation.ClearParameters()
            objDataOperation.AddParameter("@name", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 2, "Select"))
            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "GetLevelFromTrainingApprover", mstrMessage)
        End Try
        Return dsList
    End Function

    Public Function GetApproverUnkid(ByVal intCalendarunkid As Integer, ByVal intTrainingTypeid As Integer, _
                                     ByVal intApproverEmpunkid As Integer, ByVal blnIsExternalApprover As Boolean, _
                                     Optional ByVal objDataOp As clsDataOperation = Nothing) As String
        Dim strApproverunkid As String = ""
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception
        Try
            If objDataOp IsNot Nothing Then
                objDataOperation = objDataOp
            Else
                objDataOperation = New clsDataOperation
            End If
            objDataOperation.ClearParameters()

            strQ = "SELECT  ISNULL " & _
                                "(STUFF " & _
                                        "(" & _
                                            "(SELECT ',' + CAST(approverunkid AS VARCHAR(MAX)) " & _
                                            " FROM     trtrainingapprover_master " & _
                                            " WHERE " & _
                                            "   trtrainingapprover_master.calendarunkid = @calendarunkid " & _
                                            "   AND trtrainingapprover_master.trainingtypeid = @trainingtypeid " & _
                                            "   AND trtrainingapprover_master.approverempunkid = " & intApproverEmpunkid & " " & _
                                            "   AND trtrainingapprover_master.isvoid = 0 " & _
                                            "   AND trtrainingapprover_master.isswap = 0 " & _
                                            "   AND trtrainingapprover_master.isactive = 1 " & _
                                            "   AND trtrainingapprover_master.isexternalapprover = @isexternalapprover " & _
                                            " FOR XML PATH('') " & _
                                            ") , 1, 1, '' " & _
                                        "), '' " & _
                                ") ApproverIds "

            objDataOperation.AddParameter("@calendarunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intCalendarunkid)
            objDataOperation.AddParameter("@trainingtypeid", SqlDbType.Int, eZeeDataType.INT_SIZE, intTrainingTypeid)
            objDataOperation.AddParameter("@isexternalapprover", SqlDbType.Bit, eZeeDataType.BIT_SIZE, blnIsExternalApprover)

            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            If dsList.Tables("List").Rows.Count > 0 Then
                strApproverunkid = dsList.Tables(0).Rows(0)("ApproverIds").ToString()
            End If

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "GetApproverEmployeeId", mstrMessage)
        Finally
            If objDataOp Is Nothing Then
                objDataOperation = Nothing
            End If
        End Try

        Return strApproverunkid
    End Function

    Public Function GetEmployeeFromTrainingApprover(ByVal xDatabaseName As String, _
                                                    ByVal xUserUnkid As Integer, _
                                                    ByVal xYearUnkid As Integer, _
                                                    ByVal xCompanyUnkid As Integer, _
                                                    ByVal xPeriodStart As DateTime, _
                                                    ByVal xPeriodEnd As DateTime, _
                                                    ByVal xUserModeSetting As String, _
                                                    ByVal xOnlyApproved As Boolean, _
                                                    ByVal xIncludeIn_ActiveEmployee As Boolean, _
                                                    ByVal strTableName As String, _
                                                    ByVal intCalendarunkid As Integer, _
                                                    ByVal intTrainingTypeid As Integer, _
                                                    ByVal intLoanEmpApproveunkid As Integer, _
                                                    ByVal intLevelId As Integer, _
                                                    ByVal blnIsExternalApprover As Boolean) As DataSet

        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception
        Try
            Dim objDataOperation As New clsDataOperation

            Dim xDateJoinQry, xDateFilterQry, xAdvanceJoinQry As String
            If xIncludeIn_ActiveEmployee = False Then
                xDateJoinQry = "" : xDateFilterQry = "" : xAdvanceJoinQry = ""
                Call GetDatesFilterString(xDateJoinQry, xDateFilterQry, xPeriodStart, xPeriodStart, , , xDatabaseName)
                Call GetAdvanceFilterQry(xAdvanceJoinQry, xPeriodStart, xDatabaseName)
            End If

            Dim strApproverUnkids As String = ""
            strApproverUnkids = GetApproverUnkid(intCalendarunkid, intTrainingTypeid, intLoanEmpApproveunkid, blnIsExternalApprover, objDataOperation)

            strQ = "SELECT  " & _
                      " 0 as 'select' " & _
                      ", trtrainingapprover_master.approverunkid " & _
                      ", trtrainingapprover_tran.approvertranunkid " & _
                      ", trtrainingapprover_master.approverempunkid " & _
                      ", trtrainingapprover_tran.employeeunkid " & _
                      ", ISNULL(hremployee_master.employeecode,'') as employeecode " & _
                      ", ISNULL(hremployee_master.firstname,'') + ' '  + ISNULL(hremployee_master.othername,'')  + ' '  + ISNULL(hremployee_master.surname,'') as employeename " & _
                      ", hremployee_master.gender " & _
                      ", hremployee_master.maritalstatusunkid " & _
                      ", hremployee_master.employmenttypeunkid " & _
                      ", hremployee_master.paytypeunkid " & _
                      ", hremployee_master.paypointunkid " & _
                      ", hremployee_master.nationalityunkid " & _
                      ", hremployee_master.religionunkid " & _
                      ", ETT.stationunkid " & _
                      ", ETT.deptgroupunkid " & _
                      ", ETT.departmentunkid " & _
                      ", ISNULL(ETT.sectiongroupunkid,0) AS sectiongroupunkid " & _
                      ", ETT.sectionunkid " & _
                      ", ISNULL(ETT.unitgroupunkid,0) AS unitgroupunkid " & _
                      ", ETT.unitunkid " & _
                      ", ISNULL(ETT.teamunkid,0) AS teamunkid " & _
                      ", ETT.classgroupunkid " & _
                      ", ETT.classunkid " & _
                      ", ECT.jobgroupunkid " & _
                      ", ECT.jobunkid " & _
                      ", GRD.gradegroupunkid " & _
                      ", GRD.gradeunkid " & _
                      ", GRD.gradelevelunkid " & _
                      ", CC.cctranheadvalueid " & _
                      ", hrtraining_approverlevel_master.levelunkid " & _
                      ", hrtraining_approverlevel_master.levelname " & _
                      ", hrtraining_approverlevel_master.priority " & _
                      ", trtrainingapprover_master.isexternalapprover " & _
                " FROM  trtrainingapprover_master " & _
                      " JOIN trtrainingapprover_tran ON trtrainingapprover_master.approverunkid = trtrainingapprover_tran.approverunkid AND trtrainingapprover_tran.isvoid = 0  " & _
                      " JOIN hremployee_master ON hremployee_master.employeeunkid = trtrainingapprover_tran.employeeunkid " & _
                      " JOIN hrtraining_approverlevel_master ON hrtraining_approverlevel_master.levelunkid = trtrainingapprover_master.levelunkid  " & _
                      " LEFT JOIN " & _
                      "( " & _
                      "   SELECT " & _
                      "        stationunkid " & _
                      "       ,deptgroupunkid " & _
                      "       ,departmentunkid " & _
                      "       ,sectiongroupunkid " & _
                      "       ,sectionunkid " & _
                      "       ,unitgroupunkid " & _
                      "       ,unitunkid " & _
                      "       ,teamunkid " & _
                      "       ,classgroupunkid " & _
                      "       ,classunkid " & _
                      "       ,employeeunkid " & _
                      "       ,ROW_NUMBER()OVER(PARTITION BY employeeunkid ORDER BY effectivedate DESC) AS rno " & _
                      "   FROM hremployee_transfer_tran " & _
                      "   WHERE isvoid = 0 AND CONVERT(CHAR(8),effectivedate,112) <= '" & eZeeDate.convertDate(xPeriodEnd) & "' " & _
                      ") AS ETT ON ETT.employeeunkid = hremployee_master.employeeunkid AND ETT.rno = 1 " & _
                      " LEFT JOIN " & _
                      "( " & _
                      "   SELECT " & _
                      "        jobgroupunkid " & _
                      "       ,jobunkid " & _
                      "       ,employeeunkid " & _
                      "       ,ROW_NUMBER()OVER(PARTITION BY employeeunkid ORDER BY effectivedate DESC) AS rno " & _
                      "   FROM hremployee_categorization_tran " & _
                      "   WHERE isvoid = 0 AND CONVERT(CHAR(8),effectivedate,112) <= '" & eZeeDate.convertDate(xPeriodEnd) & "' " & _
                      ") AS ECT ON ECT.employeeunkid = hremployee_master.employeeunkid AND ECT.rno = 1 " & _
                      " LEFT JOIN " & _
                      "( " & _
                      "   SELECT " & _
                      "        gradegroupunkid " & _
                      "       ,gradeunkid " & _
                      "       ,gradelevelunkid " & _
                      "       ,employeeunkid " & _
                      "       ,ROW_NUMBER()OVER(PARTITION BY employeeunkid ORDER BY incrementdate DESC,salaryincrementtranunkid DESC) AS rno " & _
                      "   FROM prsalaryincrement_tran " & _
                      "   WHERE isvoid = 0 AND prsalaryincrement_tran.isapproved = 1 AND CONVERT(CHAR(8),incrementdate,112) <= '" & eZeeDate.convertDate(xPeriodEnd) & "' " & _
                      ") AS GRD ON GRD.employeeunkid = hremployee_master.employeeunkid AND GRD.rno = 1 " & _
                      " LEFT JOIN " & _
                      "( " & _
                      "     SELECT " & _
                      "         cctranheadvalueid " & _
                      "        ,employeeunkid " & _
                      "        ,ROW_NUMBER()OVER(PARTITION BY employeeunkid ORDER BY effectivedate DESC) AS rno " & _
                      "     FROM hremployee_cctranhead_tran " & _
                      "     WHERE istransactionhead = 0 AND isvoid = 0 AND CONVERT(CHAR(8),effectivedate,112) <= '" & eZeeDate.convertDate(xPeriodEnd) & "' " & _
                      ") AS CC ON CC.employeeunkid = hremployee_master.employeeunkid AND CC.rno = 1 "

            If xIncludeIn_ActiveEmployee = False Then
                If xDateJoinQry.Trim.Length > 0 Then
                    strQ &= xDateJoinQry
                End If
            End If

            strQ &= " WHERE trtrainingapprover_master.isvoid = 0 AND trtrainingapprover_master.isactive = 1 AND trtrainingapprover_master.approverempunkid = " & intLoanEmpApproveunkid

            If strApproverUnkids.Trim.Length > 0 Then
                strQ &= " AND trtrainingapprover_master.approverunkid IN (" & strApproverUnkids & ") "
            End If

            If intLevelId >= 0 Then
                strQ &= " AND trtrainingapprover_master.levelunkid = " & intLevelId
            End If

            strQ &= " AND hremployee_master.isapproved = 1 "

            strQ &= " AND trtrainingapprover_master.isswap = 0 "

            If xIncludeIn_ActiveEmployee = False Then
                If xDateFilterQry.Trim.Length > 0 Then
                    strQ &= xDateFilterQry & " "
                End If
            End If

            strQ &= " ORDER By levelname,isnull(hremployee_master.firstname,'') + ' ' + isnull(hremployee_master.surname,'') "

            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "GetEmployeeFromTrainingApprover", mstrMessage)
        End Try
        Return dsList
    End Function
    'Hemant (22 Dec 2022) -- End

End Class
