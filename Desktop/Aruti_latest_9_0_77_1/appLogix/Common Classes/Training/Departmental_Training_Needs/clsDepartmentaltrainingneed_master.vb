﻿'************************************************************************************************************************************
'Class Name : clsDepartmentaltrainingneed_master.vb
'Purpose    :
'Date       :26-02-2021
'Written By :Suhail
'Modified   :
'************************************************************************************************************************************

Imports eZeeCommonLib
Imports System.Globalization
Imports System.Web

''' <summary>
''' Purpose: 
''' Developer: Suhail
''' </summary>
Public Class clsDepartmentaltrainingneed_master
    Private Shared ReadOnly mstrModuleName As String = "clsDepartmentaltrainingneed_master"
    Dim objDataOperation As clsDataOperation
    Dim mstrMessage As String = ""

#Region " ENUM "

    Public Enum enIncludeColumnList
        LearningMethod = 1
        TrainingProvider = 2
        TrainingVenue = 3
        TrainingResourcesNeeded = 4
        FinancingSource = 5
        TrainingCoordinator = 6
        TrainingCertificateRequired = 7
        CommentRemark = 8
        TrainingCostItem = 9
        TrainingCostAmount = 10
    End Enum

    Public Enum enApprovalStatus
        Pending = 0
        SubmittedForApprovalFromDeptTrainingNeed = 1
        TentativeApproved = 2
        SubmittedForApprovalFromTrainingBacklog = 3
        AskedForReviewAsPerAmountSet = 4
        FinalApproved = 5
        Rejected = 6
    End Enum

    Public Enum enTrainingRequestStatus
        'Pending = 0
        On_Hold = 1
        Postponed = 2
        Cancelled = 3
    End Enum

    'Hemant (26 Mar 2021) -- Start
    'NMB Enhancement : : Changes in Departmental Training Need as per comments in online doc in New UI.
    Public Enum enEmailType
        Submit_For_Approval_From_Departmental_Training_Need = 1
        Submit_For_Approval_From_Training_Backlog = 2
        'Sohail (04 Aug 2021) -- Start
        'NMB Enhancement : OLD - 430 : Notify employees who are targeted in departmental training needs plans when such plans are final approved.
        Unlock_Submitted_For_Approval = 3
        Final_Approved = 4
        Final_Rejected = 5
        'Sohail (04 Aug 2021) -- End
    End Enum
    'Hemant (26 Mar 2021) -- End

#End Region

#Region " Private variables "
    Private mintDepartmentaltrainingneedunkid As Integer
    Private mintDepartmentunkid As Integer
    Private mintPeriodunkid As Integer
    Private mintCompetenceunkid As Integer
    Private mintTrainingcategoryunkid As Integer
    Private mintTrainingcourseunkid As Integer
    Private mintLearningmethodunkid As Integer
    Private mintTargetedgroupunkid As Integer
    Private mintNoofstaff As Integer
    Private mdtStartdate As Date
    Private mdtEnddate As Date
    Private mintTrainingpriority As Integer
    Private mintTrainingproviderunkid As Integer
    Private mintTrainingvenueunkid As Integer
    Private mstrOther_competence As String = String.Empty
    Private mstrOther_trainingcourse As String = String.Empty
    Private mintStatusunkid As Integer
    Private mblnIssubmitforapproval As Boolean
    Private mstrRemark As String = String.Empty
    Private mblnIscertirequired As Boolean
    Private mdecTotalcost As Decimal
    Private mblnIsactive As Boolean = True
    Private mintUserunkid As Integer
    Private mintLoginemployeeunkid As Integer
    Private mblnIsweb As Boolean
    Private mblnIsvoid As Boolean
    Private mintVoiduserunkid As Integer
    Private mintVoidloginemployeeunkid As Integer
    Private mdtVoiddatetime As Date
    Private mstrVoidreason As String = String.Empty
    Private mintAllocationId As Integer
    'Sohail (12 Apr 2021) -- Start
    'NMB Enhancement : : Changes in Departmental Training Need as per comments in online doc in New UI.
    Private mintModuleId As Integer
    Private mintModuleTranUnkId As Integer
    Private mblnIstrainingcostoptional As Boolean
    'Sohail (12 Apr 2021) -- End
    'Hemant (15 Apr 2021) -- Start
    'NMB Enhancement : : Changes in Departmental Training Need as per comments in online doc in New UI.
    Private mintInsertFormId As Integer
    'Hemant (15 Apr 2021) -- End
    Private mstrRefno As String = String.Empty
    Private mdecApproved_Totalcost As Decimal
    Private mintRequest_Statusunkid As Integer = 0
    'Sohail (04 Aug 2021) -- Start
    'NMB Enhancement : OLD - 429 : Move Submit for Approval button from current location to the Add/Edit Departmental Training Needs Screen.
    Private mstrUnlock_remark As String = String.Empty
    Private mstrFinalapproval_remark As String = String.Empty
    'Sohail (04 Aug 2021) -- End
    Private mintCreateuserunkid As Integer = 0 'Sohail (17 Aug 2021)
#End Region

#Region " Public variables "
    Public pintDepartmentaltrainingneedunkid As Integer
    Public pintDepartmentunkid As Integer
    Public pintPeriodunkid As Integer
    Public pintCompetenceunkid As Integer
    Public pintTrainingcategoryunkid As Integer
    Public pintTrainingcourseunkid As Integer
    Public pintLearningmethodunkid As Integer
    Public pintTargetedgroupunkid As Integer
    Public pintNoofstaff As Integer
    Public pdtStartdate As Date
    Public pdtEnddate As Date
    Public pintTrainingpriority As Integer
    Public pintTrainingproviderunkid As Integer
    Public pintTrainingvenueunkid As Integer
    Public pstrOther_competence As String = String.Empty
    Public pstrOther_trainingcourse As String = String.Empty
    Public pintStatusunkid As Integer
    Public pblnIssubmitforapproval As Boolean
    Public pstrRemark As String = String.Empty
    Public pblnIscertirequired As Boolean
    Public pdecTotalcost As Decimal
    Public pblnIsactive As Boolean = True
    Public pintUserunkid As Integer
    Public pintLoginemployeeunkid As Integer
    Public pblnIsweb As Boolean
    Public pblnIsvoid As Boolean
    Public pintVoiduserunkid As Integer
    Public pintVoidloginemployeeunkid As Integer
    Public pdtVoiddatetime As Date
    Public pstrVoidreason As String = String.Empty
    'Sohail (12 Apr 2021) -- Start
    'NMB Enhancement : : Changes in Departmental Training Need as per comments in online doc in New UI.
    Public pintAllocationId As Integer
    Public pintModuleId As Integer
    Public pintModuleTranUnkId As Integer
    Public pblnIstrainingcostoptional As Boolean
    'Sohail (12 Apr 2021) -- End
    'Hemant (15 Apr 2021) -- Start
    'NMB Enhancement : : Changes in Departmental Training Need as per comments in online doc in New UI.
    Public pintInsertFormId As Integer
    'Hemant (15 Apr 2021) -- End
    Public pstrRefno As String = String.Empty
    Public pdecApproved_Totalcost As Decimal
    Public pintRequest_Statusunkid As Integer
    'Sohail (04 Aug 2021) -- Start
    'NMB Enhancement : OLD - 429 : Move Submit for Approval button from current location to the Add/Edit Departmental Training Needs Screen.
    Public pstrUnlock_remark As String = String.Empty
    Public pstrFinalapproval_remark As String = String.Empty
    'Sohail (04 Aug 2021) -- End
    Public pintCreateuserunkid As Integer = 0 'Sohail (17 Aug 2021)

    Public pintAuditUserId As Integer = 0
    Public pdtAuditDate As DateTime
    Public pstrClientIp As String = ""
    Public pstrHostName As String = ""
    Public pstrFormName As String = ""
#End Region

#Region " Properties "
    ''' <summary>
    ''' Purpose: Get Message from Class 
    ''' Modify By: Suhail
    ''' </summary>
    Public ReadOnly Property _Message() As String
        Get
            Return mstrMessage
        End Get
    End Property

    Private mDataOp As clsDataOperation = Nothing
    Public Property _DataOp() As clsDataOperation
        Get
            Return mDataOp
        End Get
        Set(ByVal value As clsDataOperation)
            mDataOp = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set departmentaltrainingneedunkid
    ''' Modify By: Suhail
    ''' </summary>
    Public Property _Departmentaltrainingneedunkid() As Integer
        Get
            Return mintDepartmentaltrainingneedunkid
        End Get
        Set(ByVal value As Integer)
            mintDepartmentaltrainingneedunkid = value
            Call GetData()
        End Set
    End Property

    Public Property _Departmentunkid() As Integer
        Get
            Return mintDepartmentunkid
        End Get
        Set(ByVal value As Integer)
            mintDepartmentunkid = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set periodunkid
    ''' Modify By: Suhail
    ''' </summary>
    Public Property _Periodunkid() As Integer
        Get
            Return mintPeriodunkid
        End Get
        Set(ByVal value As Integer)
            mintPeriodunkid = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set competenceunkid
    ''' Modify By: Suhail
    ''' </summary>
    Public Property _Competenceunkid() As Integer
        Get
            Return mintCompetenceunkid
        End Get
        Set(ByVal value As Integer)
            mintCompetenceunkid = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set trainingcategoryunkid
    ''' Modify By: Suhail
    ''' </summary>
    Public Property _Trainingcategoryunkid() As Integer
        Get
            Return mintTrainingcategoryunkid
        End Get
        Set(ByVal value As Integer)
            mintTrainingcategoryunkid = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set trainingcourseunkid
    ''' Modify By: Suhail
    ''' </summary>
    Public Property _Trainingcourseunkid() As Integer
        Get
            Return mintTrainingcourseunkid
        End Get
        Set(ByVal value As Integer)
            mintTrainingcourseunkid = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set learningmethodunkid
    ''' Modify By: Suhail
    ''' </summary>
    Public Property _Learningmethodunkid() As Integer
        Get
            Return mintLearningmethodunkid
        End Get
        Set(ByVal value As Integer)
            mintLearningmethodunkid = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set targetedgroupunkid
    ''' Modify By: Suhail
    ''' </summary>
    Public Property _Targetedgroupunkid() As Integer
        Get
            Return mintTargetedgroupunkid
        End Get
        Set(ByVal value As Integer)
            mintTargetedgroupunkid = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set noofstaff
    ''' Modify By: Suhail
    ''' </summary>
    Public Property _Noofstaff() As Integer
        Get
            Return mintNoofstaff
        End Get
        Set(ByVal value As Integer)
            mintNoofstaff = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set startdate
    ''' Modify By: Suhail
    ''' </summary>
    Public Property _Startdate() As Date
        Get
            Return mdtStartdate
        End Get
        Set(ByVal value As Date)
            mdtStartdate = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set enddate
    ''' Modify By: Suhail
    ''' </summary>
    Public Property _Enddate() As Date
        Get
            Return mdtEnddate
        End Get
        Set(ByVal value As Date)
            mdtEnddate = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set trainingpriority
    ''' Modify By: Suhail
    ''' </summary>
    Public Property _Trainingpriority() As Integer
        Get
            Return mintTrainingpriority
        End Get
        Set(ByVal value As Integer)
            mintTrainingpriority = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set trainingproviderunkid
    ''' Modify By: Suhail
    ''' </summary>
    Public Property _Trainingproviderunkid() As Integer
        Get
            Return mintTrainingproviderunkid
        End Get
        Set(ByVal value As Integer)
            mintTrainingproviderunkid = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set trainingvenueunkid
    ''' Modify By: Suhail
    ''' </summary>
    Public Property _Trainingvenueunkid() As Integer
        Get
            Return mintTrainingvenueunkid
        End Get
        Set(ByVal value As Integer)
            mintTrainingvenueunkid = value
        End Set
    End Property

    Public Property _Other_competence() As String
        Get
            Return mstrOther_competence
        End Get
        Set(ByVal value As String)
            mstrOther_competence = value
        End Set
    End Property

    Public Property _Other_trainingcourse() As String
        Get
            Return mstrOther_trainingcourse
        End Get
        Set(ByVal value As String)
            mstrOther_trainingcourse = value
        End Set
    End Property

    Public Property _Statusunkid() As Integer
        Get
            Return mintStatusunkid
        End Get
        Set(ByVal value As Integer)
            mintStatusunkid = value
        End Set
    End Property

    Public Property _Request_Statusunkid() As Integer
        Get
            Return mintRequest_Statusunkid
        End Get
        Set(ByVal value As Integer)
            mintRequest_Statusunkid = value
        End Set
    End Property

    Public Property _Issubmitforapproval() As Boolean
        Get
            Return mblnIssubmitforapproval
        End Get
        Set(ByVal value As Boolean)
            mblnIssubmitforapproval = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set remark
    ''' Modify By: Suhail
    ''' </summary>
    Public Property _Remark() As String
        Get
            Return mstrRemark
        End Get
        Set(ByVal value As String)
            mstrRemark = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set iscertirequired
    ''' Modify By: Suhail
    ''' </summary>
    Public Property _Iscertirequired() As Boolean
        Get
            Return mblnIscertirequired
        End Get
        Set(ByVal value As Boolean)
            mblnIscertirequired = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set totalcost
    ''' Modify By: Suhail
    ''' </summary>
    Public Property _Totalcost() As Decimal
        Get
            Return mdecTotalcost
        End Get
        Set(ByVal value As Decimal)
            mdecTotalcost = value
        End Set
    End Property

    Public Property _Isactive() As Boolean
        Get
            Return mblnIsactive
        End Get
        Set(ByVal value As Boolean)
            mblnIsactive = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set userunkid
    ''' Modify By: Suhail
    ''' </summary>
    Public Property _Userunkid() As Integer
        Get
            Return mintUserunkid
        End Get
        Set(ByVal value As Integer)
            mintUserunkid = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set loginemployeeunkid
    ''' Modify By: Suhail
    ''' </summary>
    Public Property _Loginemployeeunkid() As Integer
        Get
            Return mintLoginemployeeunkid
        End Get
        Set(ByVal value As Integer)
            mintLoginemployeeunkid = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set isweb
    ''' Modify By: Suhail
    ''' </summary>
    Public Property _Isweb() As Boolean
        Get
            Return mblnIsweb
        End Get
        Set(ByVal value As Boolean)
            mblnIsweb = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set isvoid
    ''' Modify By: Suhail
    ''' </summary>
    Public Property _Isvoid() As Boolean
        Get
            Return mblnIsvoid
        End Get
        Set(ByVal value As Boolean)
            mblnIsvoid = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set voiduserunkid
    ''' Modify By: Suhail
    ''' </summary>
    Public Property _Voiduserunkid() As Integer
        Get
            Return mintVoiduserunkid
        End Get
        Set(ByVal value As Integer)
            mintVoiduserunkid = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set voidloginemployeeunkid
    ''' Modify By: Suhail
    ''' </summary>
    Public Property _Voidloginemployeeunkid() As Integer
        Get
            Return mintVoidloginemployeeunkid
        End Get
        Set(ByVal value As Integer)
            mintVoidloginemployeeunkid = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set voiddatetime
    ''' Modify By: Suhail
    ''' </summary>
    Public Property _Voiddatetime() As Date
        Get
            Return mdtVoiddatetime
        End Get
        Set(ByVal value As Date)
            mdtVoiddatetime = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set voidreason
    ''' Modify By: Suhail
    ''' </summary>
    Public Property _Voidreason() As String
        Get
            Return mstrVoidreason
        End Get
        Set(ByVal value As String)
            mstrVoidreason = value
        End Set
    End Property

    Private mstrTargetedGroupTranIDs As String = ""
    Public WriteOnly Property _TargetedGroupTranIDs() As String
        Set(ByVal value As String)
            mstrTargetedGroupTranIDs = value
        End Set
    End Property


    Private mintAuditUserId As Integer = 0
    Public WriteOnly Property _AuditUserId() As Integer
        Set(ByVal value As Integer)
            mintAuditUserId = value
        End Set
    End Property

    Private mdtAuditDate As DateTime
    Public WriteOnly Property _AuditDate() As DateTime
        Set(ByVal value As DateTime)
            mdtAuditDate = value
        End Set
    End Property

    Private mstrClientIp As String = ""
    Public WriteOnly Property _ClientIP() As String
        Set(ByVal value As String)
            mstrClientIp = value
        End Set
    End Property

    Private mstrHostName As String = ""
    Public WriteOnly Property _HostName() As String
        Set(ByVal value As String)
            mstrHostName = value
        End Set
    End Property

    Private mstrFormName As String = ""
    Public WriteOnly Property _FormName() As String
        Set(ByVal value As String)
            mstrFormName = value
        End Set
    End Property

    'Hemant (26 Mar 2021) -- Start
    'NMB Enhancement : : Changes in Departmental Training Need as per comments in online doc in New UI.
    Public Property _AllocationId() As Integer
        Get
            Return mintAllocationId
        End Get
        Set(ByVal value As Integer)
            mintAllocationId = value
        End Set
    End Property
    'Hemant (26 Mar 2021) -- End

    'Sohail (12 Apr 2021) -- Start
    'NMB Enhancement : : Changes in Departmental Training Need as per comments in online doc in New UI.
    Public Property _ModuleId() As Integer
        Get
            Return mintModuleId
        End Get
        Set(ByVal value As Integer)
            mintModuleId = value
        End Set
    End Property

    Public Property _ModuleTranUnkId() As Integer
        Get
            Return mintModuleTranUnkId
        End Get
        Set(ByVal value As Integer)
            mintModuleTranUnkId = value
        End Set
    End Property

    Public Property _IsTrainingCostOptional() As Boolean
        Get
            Return mblnIstrainingcostoptional
        End Get
        Set(ByVal value As Boolean)
            mblnIstrainingcostoptional = value
        End Set
    End Property
    'Sohail (12 Apr 2021) -- End

    'Hemant (15 Apr 2021) -- Start
    'NMB Enhancement : : Changes in Departmental Training Need as per comments in online doc in New UI.
    Public Property _InsertFormId() As Integer
        Get
            Return mintInsertFormId
        End Get
        Set(ByVal value As Integer)
            mintInsertFormId = value
        End Set
    End Property
    'Hemant (15 Apr 2021) -- End

    Public Property _RefNo() As String
        Get
            Return mstrRefno
        End Get
        Set(ByVal value As String)
            mstrRefno = value
        End Set
    End Property

    Public Property _Approved_Totalcost() As Decimal
        Get
            Return mdecApproved_Totalcost
        End Get
        Set(ByVal value As Decimal)
            mdecApproved_Totalcost = value
        End Set
    End Property

    'Sohail (04 Aug 2021) -- Start
    'NMB Enhancement : OLD - 428 : Email alert when plans are unlocked from the training backlog.
    Public Property _Unlock_Remark() As String
        Get
            Return mstrUnlock_remark
        End Get
        Set(ByVal value As String)
            mstrUnlock_remark = value
        End Set
    End Property

    Public Property _Finalapproval_Remark() As String
        Get
            Return mstrFinalapproval_remark
        End Get
        Set(ByVal value As String)
            mstrFinalapproval_remark = value
        End Set
    End Property
    'Sohail (04 Aug 2021) -- End

    'Sohail (17 Aug 2021) -- Start
    'NMB Enhancement : OLD-428 : Send alert to the creator of plan only in departmental training need.
    Public Property _Createuserunkid() As Integer
        Get
            Return mintCreateuserunkid
        End Get
        Set(ByVal value As Integer)
            mintCreateuserunkid = value
        End Set
    End Property
    'Sohail (17 Aug 2021) -- End

    Private mlstDeptTEmpNew As List(Of clsDepttrainingneed_employee_Tran)
    Public WriteOnly Property _lstDeptTEmpNew() As List(Of clsDepttrainingneed_employee_Tran)
        Set(ByVal value As List(Of clsDepttrainingneed_employee_Tran))
            mlstDeptTEmpNew = value
        End Set
    End Property

    Private mlstDeptTEmpVoid As List(Of clsDepttrainingneed_employee_Tran)
    Public WriteOnly Property _lstDeptTEmpVoid() As List(Of clsDepttrainingneed_employee_Tran)
        Set(ByVal value As List(Of clsDepttrainingneed_employee_Tran))
            mlstDeptTEmpVoid = value
        End Set
    End Property

    Private mlstDeptTAllocNew As List(Of clsDepttrainingneed_allocation_Tran)
    Public WriteOnly Property _lstDeptTAllocNew() As List(Of clsDepttrainingneed_allocation_Tran)
        Set(ByVal value As List(Of clsDepttrainingneed_allocation_Tran))
            mlstDeptTAllocNew = value
        End Set
    End Property

    Private mlstDeptTAllocVoid As List(Of clsDepttrainingneed_allocation_Tran)
    Public WriteOnly Property _lstDeptTAllocVoid() As List(Of clsDepttrainingneed_allocation_Tran)
        Set(ByVal value As List(Of clsDepttrainingneed_allocation_Tran))
            mlstDeptTAllocVoid = value
        End Set
    End Property

    Private mlstDeptTResourceNew As List(Of clsDepttrainingneed_resources_Tran)
    Public WriteOnly Property _lstDeptTResourceNew() As List(Of clsDepttrainingneed_resources_Tran)
        Set(ByVal value As List(Of clsDepttrainingneed_resources_Tran))
            mlstDeptTResourceNew = value
        End Set
    End Property

    Private mlstDeptTResourceVoid As List(Of clsDepttrainingneed_resources_Tran)
    Public WriteOnly Property _lstDeptTResourceVoid() As List(Of clsDepttrainingneed_resources_Tran)
        Set(ByVal value As List(Of clsDepttrainingneed_resources_Tran))
            mlstDeptTResourceVoid = value
        End Set
    End Property

    Private mlstDeptTFSourceNew As List(Of clsDepttrainingneed_financingsources_Tran)
    Public WriteOnly Property _lstDeptTFSourceNew() As List(Of clsDepttrainingneed_financingsources_Tran)
        Set(ByVal value As List(Of clsDepttrainingneed_financingsources_Tran))
            mlstDeptTFSourceNew = value
        End Set
    End Property

    Private mlstDeptTFSourceVoid As List(Of clsDepttrainingneed_financingsources_Tran)
    Public WriteOnly Property _lstDeptTFSourceVoid() As List(Of clsDepttrainingneed_financingsources_Tran)
        Set(ByVal value As List(Of clsDepttrainingneed_financingsources_Tran))
            mlstDeptTFSourceVoid = value
        End Set
    End Property

    Private mlstDeptTCoordNew As List(Of clsDepttrainingneed_trainingcoordinator_Tran)
    Public WriteOnly Property _lstDeptTCoordNew() As List(Of clsDepttrainingneed_trainingcoordinator_Tran)
        Set(ByVal value As List(Of clsDepttrainingneed_trainingcoordinator_Tran))
            mlstDeptTCoordNew = value
        End Set
    End Property

    Private mlstDeptTCoordVoid As List(Of clsDepttrainingneed_trainingcoordinator_Tran)
    Public WriteOnly Property _lstDeptTCoordVoid() As List(Of clsDepttrainingneed_trainingcoordinator_Tran)
        Set(ByVal value As List(Of clsDepttrainingneed_trainingcoordinator_Tran))
            mlstDeptTCoordVoid = value
        End Set
    End Property

    Private mlstDeptTCostItemNew As List(Of clsDepttrainingneed_costitem_Tran)
    Public WriteOnly Property _lstDeptTCostItemNew() As List(Of clsDepttrainingneed_costitem_Tran)
        Set(ByVal value As List(Of clsDepttrainingneed_costitem_Tran))
            mlstDeptTCostItemNew = value
        End Set
    End Property

    Private mlstDeptTCostItemVoid As List(Of clsDepttrainingneed_costitem_Tran)
    Public WriteOnly Property _lstDeptTCostItemVoid() As List(Of clsDepttrainingneed_costitem_Tran)
        Set(ByVal value As List(Of clsDepttrainingneed_costitem_Tran))
            mlstDeptTCostItemVoid = value
        End Set
    End Property

    'Sohail (07 Jun 2021) -- Start
    'KBC Enhancement : OLD-391 : Imprest posting to Payroll.
    Private mlstDeptTInstructNew As List(Of clsDepttrainingneed_traininginstructor_Tran)
    Public WriteOnly Property _lstDeptTInstructNew() As List(Of clsDepttrainingneed_traininginstructor_Tran)
        Set(ByVal value As List(Of clsDepttrainingneed_traininginstructor_Tran))
            mlstDeptTInstructNew = value
        End Set
    End Property

    Private mlstDeptTInstructVoid As List(Of clsDepttrainingneed_traininginstructor_Tran)
    Public WriteOnly Property _lstDeptTInstructVoid() As List(Of clsDepttrainingneed_traininginstructor_Tran)
        Set(ByVal value As List(Of clsDepttrainingneed_traininginstructor_Tran))
            mlstDeptTInstructVoid = value
        End Set
    End Property
    'Sohail (07 JUn 2021) -- End

#End Region

    ''' <summary>
    ''' Modify By: Suhail
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>
    Public Sub GetData()
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        If mDataOp IsNot Nothing Then
            objDataOperation = mDataOp
        Else
            objDataOperation = New clsDataOperation
        End If
        objDataOperation.ClearParameters()

        Try
            strQ = "SELECT " & _
              "  departmentaltrainingneedunkid " & _
              ", departmentunkid " & _
              ", periodunkid " & _
              ", competenceunkid " & _
              ", trainingcategoryunkid " & _
              ", trainingcourseunkid " & _
              ", learningmethodunkid " & _
              ", targetedgroupunkid " & _
              ", noofstaff " & _
              ", startdate " & _
              ", enddate " & _
              ", trainingpriority " & _
              ", trainingproviderunkid " & _
              ", trainingvenueunkid " & _
              ", ISNULL(other_competence, '') AS other_competence " & _
              ", ISNULL(other_trainingcourse, '') AS other_trainingcourse " & _
              ", ISNULL(statusunkid, 0) AS statusunkid " & _
              ", ISNULL(issubmitforapproval, 0) AS issubmitforapproval " & _
              ", remark " & _
              ", iscertirequired " & _
              ", totalcost " & _
              ", ISNULL(isactive, 0) AS isactive " & _
              ", userunkid " & _
              ", loginemployeeunkid " & _
              ", isweb " & _
              ", isvoid " & _
              ", voiduserunkid " & _
              ", voidloginemployeeunkid " & _
              ", voiddatetime " & _
              ", voidreason " & _
              ", allocationid " & _
              ", ISNULL(moduleid, 0) AS moduleid " & _
              ", ISNULL(moduletranunkid , 0) AS moduletranunkid " & _
              ", ISNULL(istrainingcostoptional, 0) AS istrainingcostoptional " & _
              ", ISNULL(insertformid, 0) AS insertformid " & _
              ", ISNULL(refno, '') AS refno " & _
              ", ISNULL(approved_totalcost, totalcost) AS approved_totalcost " & _
              ", ISNULL(request_statusunkid, 0) AS request_statusunkid " & _
              ", ISNULL(unlock_remark, '') AS unlock_remark " & _
              ", ISNULL(finalapproval_remark, '') AS finalapproval_remark " & _
              ", ISNULL(createuserunkid, 0) AS createuserunkid " & _
             "FROM trdepartmentaltrainingneed_master " & _
             "WHERE departmentaltrainingneedunkid = @departmentaltrainingneedunkid "
            'Sohail (17 Aug 2021) - [createuserunkid]
            'Sohail (04 Aug 2021) - [unlock_remark, finalapproval_remark]
            'Sohail (15 Apr 2021) - [refno, approved_totalcost, request_statusunkid]
            'Hemant (15 Apr 2021) -- [insertformid]
            'Sohail (12 Apr 2021) - [moduleid. moduletranunkid, istrainingcostoptional]

            objDataOperation.AddParameter("@departmentaltrainingneedunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintDepartmentaltrainingneedunkid.ToString)

            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            For Each dtRow As DataRow In dsList.Tables(0).Rows
                mintDepartmentaltrainingneedunkid = CInt(dtRow.Item("departmentaltrainingneedunkid"))
                mintDepartmentunkid = CInt(dtRow.Item("departmentunkid"))
                mintPeriodunkid = CInt(dtRow.Item("periodunkid"))
                mintCompetenceunkid = CInt(dtRow.Item("competenceunkid"))
                mintTrainingcategoryunkid = CInt(dtRow.Item("trainingcategoryunkid"))
                mintTrainingcourseunkid = CInt(dtRow.Item("trainingcourseunkid"))
                mintLearningmethodunkid = CInt(dtRow.Item("learningmethodunkid"))
                mintTargetedgroupunkid = CInt(dtRow.Item("targetedgroupunkid"))
                mintNoofstaff = CInt(dtRow.Item("noofstaff"))
                'mdtStartdate = dtRow.Item("startdate")
                'mdtEnddate = dtRow.Item("enddate")
                If IsDBNull(dtRow.Item("startdate")) = True Then
                    mdtStartdate = Nothing
                Else
                    mdtStartdate = CDate(dtRow.Item("startdate"))
                End If
                If IsDBNull(dtRow.Item("enddate")) = True Then
                    mdtEnddate = Nothing
                Else
                    mdtEnddate = CDate(dtRow.Item("enddate"))
                End If
                mintTrainingpriority = CInt(dtRow.Item("trainingpriority"))
                mintTrainingproviderunkid = CInt(dtRow.Item("trainingproviderunkid"))
                mintTrainingvenueunkid = CInt(dtRow.Item("trainingvenueunkid"))
                mstrOther_competence = dtRow.Item("other_competence").ToString
                mstrOther_trainingcourse = dtRow.Item("other_trainingcourse").ToString
                mintStatusunkid = CInt(dtRow.Item("statusunkid"))
                mblnIssubmitforapproval = CBool(dtRow.Item("issubmitforapproval"))
                mstrRemark = dtRow.Item("remark").ToString
                mblnIscertirequired = CBool(dtRow.Item("iscertirequired"))
                mdecTotalcost = dtRow.Item("totalcost")
                mblnIsactive = CBool(dtRow.Item("isactive"))
                mintUserunkid = CInt(dtRow.Item("userunkid"))
                mintLoginemployeeunkid = CInt(dtRow.Item("loginemployeeunkid"))
                mblnIsweb = CBool(dtRow.Item("isweb"))
                mblnIsvoid = CBool(dtRow.Item("isvoid"))
                mintVoiduserunkid = CInt(dtRow.Item("voiduserunkid"))
                mintVoidloginemployeeunkid = CInt(dtRow.Item("voidloginemployeeunkid"))
                If IsDBNull(dtRow.Item("voiddatetime")) = True Then
                    mdtVoiddatetime = Nothing
                Else
                    mdtVoiddatetime = CDate(dtRow.Item("voiddatetime"))
                End If
                mstrVoidreason = dtRow.Item("voidreason").ToString
                mintAllocationId = CInt(dtRow.Item("allocationid"))
                'Sohail (12 Apr 2021) -- Start
                'NMB Enhancement : : Changes in Departmental Training Need as per comments in online doc in New UI.
                mintModuleId = CInt(dtRow.Item("moduleid"))
                mintModuleTranUnkId = CInt(dtRow.Item("moduletranunkid"))
                mblnIstrainingcostoptional = CBool(dtRow.Item("istrainingcostoptional"))
                'Sohail (12 Apr 2021) -- End
                'Hemant (15 Apr 2021) -- Start
                'NMB Enhancement : : Changes in Departmental Training Need as per comments in online doc in New UI.
                mintInsertFormId = CInt(dtRow.Item("insertformid"))
                'Hemant (15 Apr 2021) -- End
                mstrRefno = dtRow.Item("refno").ToString
                mdecApproved_Totalcost = dtRow.Item("approved_totalcost")
                mintRequest_Statusunkid = CInt(dtRow.Item("request_statusunkid"))
                'Sohail (04 Aug 2021) -- Start
                'NMB Enhancement : OLD - 429 : Move Submit for Approval button from current location to the Add/Edit Departmental Training Needs Screen.
                mstrUnlock_remark = dtRow.Item("unlock_remark").ToString
                mstrFinalapproval_remark = dtRow.Item("finalapproval_remark").ToString
                'Sohail (04 Aug 2021) -- End
                mintCreateuserunkid = CInt(dtRow.Item("createuserunkid"))
                Exit For
            Next
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: GetData; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            If mDataOp Is Nothing Then objDataOperation = Nothing
        End Try
    End Sub


    ''' <summary>
    ''' Modify By: Suhail
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>
    Public Function GetList(ByVal xDatabaseName As String, _
                            ByVal xUserUnkid As Integer, _
                            ByVal xYearUnkid As Integer, _
                            ByVal xCompanyUnkid As Integer, _
                            ByVal xPeriodStart As DateTime, _
                            ByVal xPeriodEnd As DateTime, _
                            ByVal xUserModeSetting As String, _
                            ByVal xOnlyApproved As Boolean, _
                            ByVal xIncludeIn_ActiveEmployee As Boolean, _
                            ByVal xCurrentAllocationId As Integer, _
                            ByVal strTableName As String, _
                            Optional ByVal blnIncludeChecildTablesData As Boolean = False, _
                            Optional ByVal strFilter As String = "", _
                            Optional ByVal intEmployeeUnkId As Integer = 0, _
                            Optional ByVal intAllocationtranUnkId As Integer = 0 _
                            ) As DataSet

        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        If mDataOp IsNot Nothing Then
            objDataOperation = mDataOp
        Else
            objDataOperation = New clsDataOperation
        End If
        objDataOperation.ClearParameters()

        Try
            Dim dsAllocation As DataSet = (New clsMasterData).GetTrainingTargetedGroup("List", "", False, False)
            Dim dsStatus As DataSet = getStatusComboList("List", True)


            strQ = "IF OBJECT_ID('tempdb..#pdpitem') IS NOT NULL DROP TABLE #pdpitem " & _
                    "CREATE TABLE #pdpitem ( pdpitemname NVARCHAR(MAX),departmentaltrainingneedunkid int); " & _
                    "INSERT INTO #pdpitem " & _
                         "SELECT " & _
                             "CASE " & _
                                 "WHEN pdpitemdatatran.iscompetencyselection = 1 THEN hrassess_competencies_master.name " & _
                                 "ELSE pdpitemdatatran.fieldvalue " & _
                            "END AS fieldvalue " & _
                            ",trdepartmentaltrainingneed_master.departmentaltrainingneedunkid " & _
                         "FROM pdpitemdatatran " & _
                         "LEFT JOIN pdpgoals_master " & _
                              "ON pdpitemdatatran.itemtranunkid = pdpgoals_master.itemunkid " & _
                         "LEFT JOIN trdepartmentaltrainingneed_master " & _
                              "ON pdpgoals_master.pdpgoalsmstunkid = trdepartmentaltrainingneed_master.moduletranunkid " & _
                         "LEFT JOIN hrassess_competencies_master " & _
                              "ON hrassess_competencies_master.competenciesunkid = pdpitemdatatran.itemunkid " & _
                                   "AND pdpitemdatatran.iscompetencyselection = 1 " & _
                                   " and hrassess_competencies_master.isactive = 1 " & _
                         "WHERE trdepartmentaltrainingneed_master.moduleid = " & CInt(enModuleReference.PDP) & " " & _
                         "AND trdepartmentaltrainingneed_master.isvoid = 0 and pdpitemdatatran.isvoid=0 and pdpgoals_master.isvoid = 0  "

            strQ &= "SELECT trdepartmentaltrainingneed_master.departmentaltrainingneedunkid " & _
                     ", trdepartmentaltrainingneed_master.departmentunkid " & _
                     ", trdepartmentaltrainingneed_master.periodunkid " & _
                     ", ISNULL(trtraining_calendar_master.calendar_name, '') AS periodname " & _
                     ", trdepartmentaltrainingneed_master.competenceunkid " & _
                     ",CASE " & _
                          "WHEN trdepartmentaltrainingneed_master.competenceunkid > 0 THEN ISNULL(tcompetence.name, '') " & _
                          "WHEN trdepartmentaltrainingneed_master.competenceunkid <= 0 AND " & _
                               "trdepartmentaltrainingneed_master.other_competence = '' THEN ISNULL(#pdpitem.pdpitemname, '') " & _
                          "ELSE ISNULL(other_competence, '') " & _
                     "END AS competencename " & _
                     ", trdepartmentaltrainingneed_master.trainingcategoryunkid " & _
                     ", ISNULL(trtrainingcategory_master.categoryname, '') AS trainingcategoryname " & _
                     ", trdepartmentaltrainingneed_master.trainingcourseunkid " & _
                     ", CASE WHEN trdepartmentaltrainingneed_master.trainingcourseunkid > 0 THEN ISNULL(tcourse.name, '') ELSE ISNULL(other_trainingcourse, '') END AS trainingcoursename " & _
                     ", trdepartmentaltrainingneed_master.learningmethodunkid " & _
                     ", ISNULL(trtrainingitemsinfo_master.info_name, '') AS learningmethodname " & _
                     ", trdepartmentaltrainingneed_master.targetedgroupunkid " & _
                     ", trdepartmentaltrainingneed_master.noofstaff " & _
                     ", CONVERT(CHAR(8), trdepartmentaltrainingneed_master.startdate, 112) AS startdate " & _
                     ", CONVERT(CHAR(8), trdepartmentaltrainingneed_master.enddate, 112) AS enddate " & _
                     ", trdepartmentaltrainingneed_master.trainingpriority " & _
                     ", ISNULL(trtrainingpriority_master.trpriority_name, '') AS trainingpriorityname " & _
                     ", trdepartmentaltrainingneed_master.trainingproviderunkid " & _
                     ", ISNULL(hrinstitute_master.institute_name, '') AS trainingprovidername " & _
                     ", trdepartmentaltrainingneed_master.trainingvenueunkid " & _
                     ", ISNULL(trtrainingvenue_master.venuename, '') AS trainingvenuename " & _
                     ", ISNULL(trdepartmentaltrainingneed_master.other_competence, '') AS other_competence " & _
                     ", ISNULL(trdepartmentaltrainingneed_master.other_trainingcourse, '') AS other_trainingcourse " & _
                     ", ISNULL(trdepartmentaltrainingneed_master.statusunkid, 0) AS statusunkid " & _
                     ", ISNULL(trdepartmentaltrainingneed_master.issubmitforapproval, 0) AS issubmitforapproval " & _
                     ", trdepartmentaltrainingneed_master.remark " & _
                     ", trdepartmentaltrainingneed_master.iscertirequired " & _
                     ", trdepartmentaltrainingneed_master.totalcost " & _
                     ", ISNULL(trdepartmentaltrainingneed_master.isactive, 1) AS isactive " & _
                     ", ISNULL(trdepartmentaltrainingneed_master.unlock_remark, '') AS unlock_remark " & _
                     ", ISNULL(trdepartmentaltrainingneed_master.finalapproval_remark, '') AS finalapproval_remark " & _
                     ", ISNULL(trdepartmentaltrainingneed_master.createuserunkid, 0) AS createuserunkid " & _
                     "/*, ISNULL(hrdepartment_master.name, '') as Department*/ " & _
                     ", ISNULL(trdepartmentaltrainingneed_master.allocationid, 0) AS allocationid " & _
                     ", ISNULL(trdepartmentaltrainingneed_master.moduleid, 0) AS moduleid " & _
                     ", ISNULL(trdepartmentaltrainingneed_master.moduletranunkid, 0) AS moduletranunkid " & _
                     ", ISNULL(trdepartmentaltrainingneed_master.istrainingcostoptional, 0) AS istrainingcostoptional " & _
                     ", ISNULL(trdepartmentaltrainingneed_master.insertformid, 0) AS insertformid " & _
                     ", ISNULL(trdepartmentaltrainingneed_master.refno, '') AS refno " & _
                     ", ISNULL(trdepartmentaltrainingneed_master.approved_totalcost, trdepartmentaltrainingneed_master.totalcost) AS approved_totalcost " & _
                     ", ISNULL(trdepartmentaltrainingneed_master.request_statusunkid, 0) AS request_statusunkid " & _
                     ", CASE ISNULL(trdepartmentaltrainingneed_master.targetedgroupunkid, 0) "
            'Sohail (17 Aug 2021) - [createuserunkid]
            'Sohail (04 Aug 2021) - [unlock_remark, finalapproval_remark]
            'Sohail (15 Apr 2021) - [refno, approved_totalcost, request_statusunkid]
            'Hemant (15 Apr 2021) -- [insertformid]
            'Sohail (12 Apr 2021) - [moduleid. moduletranunkid, istrainingcostoptional]

            For Each dsRow As DataRow In dsAllocation.Tables(0).Rows
                If CInt(CInt(dsRow.Item("Id"))) <= 0 Then
                    strQ &= " WHEN " & CInt(dsRow.Item("Id")) & "  THEN @employeenames "
                Else
                    strQ &= " WHEN " & CInt(dsRow.Item("Id")) & "  THEN '" & dsRow.Item("Name").ToString & "' "
                End If

            Next
            strQ &= " END AS targetedgroupname " & _
                    ", CASE ISNULL(trdepartmentaltrainingneed_master.statusunkid, 0) "

            For Each dsRow As DataRow In dsStatus.Tables(0).Rows
                strQ &= " WHEN " & CInt(dsRow.Item("Id")) & "  THEN '" & dsRow.Item("Name").ToString & "' "
            Next
            strQ &= " END AS statusname "

            If blnIncludeChecildTablesData = True Then
                strQ &= ", ISNULL(trdepttrainingneed_employee_tran.employeeunkid, 0) AS allocationtranunkid " & _
                        ", ISNULL(hremployee_master.employeecode, '') AS allocationtrancode " & _
                        ", REPLACE(ISNULL(hremployee_master.firstname, '') + ' ' + ISNULL(hremployee_master.othername, '') + ' ' + ISNULL(hremployee_master.surname, ''), '  ', ' ') AS allocationtranname "
                '        ", ISNULL(trdepttrainingneed_resources_tran.depttrainingneedresourcestranunkid, 0) AS depttrainingneedresourcestranunkid " & _
                '        ", CAST(0 AS BIT ) AS isdeleted "
            Else
                strQ &= ", 0 AS allocationtranunkid " & _
                       ", '' AS allocationtrancode " & _
                       ", '' AS allocationtranname "
            End If

            strQ &= ", 0 AS depttrainingneedresourcestranunkid " & _
                       ", '' AS trainingresourcename " & _
                       ", 0 AS depttrainingneedfinancingsourcestranunkid " & _
                       ", '' AS financingsourcename " & _
                       ", 0 AS depttrainingneedtrainingcoordinatortranunkid " & _
                       ", '' AS coordinatorname " & _
                       ", 0 As depttrainingneedcostitemtranunkid " & _
                       ", '' AS info_name " & _
                       ", CAST(0 AS DECIMAL(36,6)) AS amount "

            Select Case xCurrentAllocationId

                Case enAllocation.BRANCH

                    strQ &= ", ISNULL(hrstation_master.name,'') AS Department "

                Case enAllocation.DEPARTMENT_GROUP

                    strQ &= ", ISNULL(hrdepartment_group_master.name,'') AS Department "

                Case enAllocation.DEPARTMENT

                    strQ &= ", ISNULL(hrdepartment_master.name,'') AS Department "

                Case enAllocation.SECTION_GROUP

                    strQ &= ", ISNULL(hrsectiongroup_master.name,'') AS Department "

                Case enAllocation.SECTION

                    strQ &= ",  ISNULL(hrsection_master.name,'') AS Department "

                Case enAllocation.UNIT_GROUP

                    strQ &= ", ISNULL(hrunitgroup_master.name,'') AS Department "

                Case enAllocation.UNIT

                    strQ &= ",  ISNULL(hrunit_master.name,'') AS Department "

                Case enAllocation.TEAM

                    strQ &= ", ISNULL(hrteam_master.name,'') AS Department "

                Case enAllocation.JOB_GROUP

                    strQ &= ", ISNULL(hrjobgroup_master.name,'') AS Department "

                Case enAllocation.JOBS

                    strQ &= ", ISNULL(hrjob_master.job_name,'') AS Department "

                Case enAllocation.CLASS_GROUP

                    strQ &= ", ISNULL(hrclassgroup_master.name,'') AS Department "

                Case enAllocation.CLASSES

                    strQ &= ", ISNULL(hrclasses_master.name,'') AS Department "

            End Select

            strQ &= "FROM trdepartmentaltrainingneed_master " & _
                    "LEFT JOIN #pdpitem ON #pdpitem.departmentaltrainingneedunkid = trdepartmentaltrainingneed_master.departmentaltrainingneedunkid " & _
                    "LEFT JOIN trtraining_calendar_master ON trtraining_calendar_master.calendarunkid = trdepartmentaltrainingneed_master.periodunkid " & _
                    "/*LEFT JOIN pdpgoals_master ON pdpgoals_master.pdpgoalsmstunkid = trdepartmentaltrainingneed_master.competenceunkid " & _
                            "AND pdpgoals_master.isvoid = 0 " & _
                    "LEFT JOIN pdpitemdatatran ON pdpitemdatatran.itemtranunkid = pdpgoals_master.itemunkid " & _
                            "AND pdpitemdatatran.isvoid = 0*/ " & _
                    "LEFT JOIN cfcommon_master AS tcompetence ON tcompetence.masterunkid = trdepartmentaltrainingneed_master.competenceunkid " & _
                           "AND tcompetence.mastertype = " & clsCommon_Master.enCommonMaster.COMPETENCE_CATEGORIES & " " & _
                           "AND tcompetence.isactive = 1 " & _
                    "LEFT JOIN trtrainingcategory_master ON trtrainingcategory_master.categoryunkid = trainingcategoryunkid " & _
                    "LEFT JOIN cfcommon_master AS tcourse ON tcourse.masterunkid = trdepartmentaltrainingneed_master.trainingcourseunkid " & _
                           "AND tcourse.mastertype = " & clsCommon_Master.enCommonMaster.TRAINING_COURSEMASTER & " " & _
                           "AND tcourse.isactive = 1 " & _
                    "LEFT JOIN trtrainingitemsinfo_master ON trtrainingitemsinfo_master.infounkid = trdepartmentaltrainingneed_master.learningmethodunkid " & _
                            "AND trtrainingitemsinfo_master.isactive = 1 " & _
                    "LEFT JOIN trtrainingpriority_master ON trtrainingpriority_master.trpriorityunkid = trdepartmentaltrainingneed_master.trainingpriority " & _
                    "LEFT JOIN hrinstitute_master ON hrinstitute_master.instituteunkid = trdepartmentaltrainingneed_master.trainingproviderunkid " & _
                            "AND hrinstitute_master.isactive = 1 " & _
                    "LEFT JOIN trtrainingvenue_master ON trtrainingvenue_master.venueunkid = trdepartmentaltrainingneed_master.trainingvenueunkid " & _
                            "AND trtrainingvenue_master.isactive = 1 " & _
                    "/*LEFT JOIN hrdepartment_master  ON hrdepartment_master.departmentunkid = trdepartmentaltrainingneed_master.departmentunkid*/ "

            Select Case xCurrentAllocationId

                Case enAllocation.BRANCH

                    strQ &= " LEFT JOIN hrstation_master ON hrstation_master.stationunkid = trdepartmentaltrainingneed_master.departmentunkid "

                Case enAllocation.DEPARTMENT_GROUP

                    strQ &= " LEFT JOIN hrdepartment_group_master ON hrdepartment_group_master.deptgroupunkid = trdepartmentaltrainingneed_master.departmentunkid "

                Case enAllocation.DEPARTMENT

                    strQ &= " LEFT JOIN hrdepartment_master ON hrdepartment_master.departmentunkid = trdepartmentaltrainingneed_master.departmentunkid "

                Case enAllocation.SECTION_GROUP

                    strQ &= " LEFT JOIN hrsectiongroup_master ON hrsectiongroup_master.sectiongroupunkid = trdepartmentaltrainingneed_master.departmentunkid "

                Case enAllocation.SECTION

                    strQ &= " LEFT JOIN hrsection_master ON hrsection_master.sectionunkid = trdepartmentaltrainingneed_master.departmentunkid "

                Case enAllocation.UNIT_GROUP

                    strQ &= " LEFT JOIN hrunitgroup_master ON hrunitgroup_master.unitgroupunkid = trdepartmentaltrainingneed_master.departmentunkid "

                Case enAllocation.UNIT

                    strQ &= " LEFT JOIN hrunit_master ON hrunit_master.unitunkid = trdepartmentaltrainingneed_master.departmentunkid "

                Case enAllocation.TEAM

                    strQ &= " LEFT JOIN hrteam_master ON hrteam_master.teamunkid = trdepartmentaltrainingneed_master.departmentunkid "

                Case enAllocation.JOB_GROUP

                    strQ &= " LEFT JOIN hrjobgroup_master ON hrjobgroup_master.jobgroupunkid = trdepartmentaltrainingneed_master.departmentunkid "

                Case enAllocation.JOBS

                    strQ &= " LEFT JOIN hrjob_master ON hrjob_master.jobunkid = trdepartmentaltrainingneed_master.departmentunkid "

                Case enAllocation.CLASS_GROUP

                    strQ &= " LEFT JOIN hrclassgroup_master ON hrclassgroup_master.classgroupunkid = trdepartmentaltrainingneed_master.departmentunkid "

                Case enAllocation.CLASSES

                    strQ &= " LEFT JOIN hrclasses_master ON hrclasses_master.classesunkid = trdepartmentaltrainingneed_master.departmentunkid "

            End Select

            If blnIncludeChecildTablesData = True Then
                strQ &= "LEFT JOIN trdepttrainingneed_employee_tran ON trdepartmentaltrainingneed_master.departmentaltrainingneedunkid = trdepttrainingneed_employee_tran.departmentaltrainingneedunkid " & _
                               "AND trdepartmentaltrainingneed_master.targetedgroupunkid <= 0 " & _
                               "AND trdepttrainingneed_employee_tran.isvoid = 0 " & _
                        "LEFT JOIN hremployee_master ON hremployee_master.employeeunkid = trdepttrainingneed_employee_tran.employeeunkid "
                '        "LEFT JOIN trdepttrainingneed_allocation_tran ON trdepartmentaltrainingneed_master.departmentaltrainingneedunkid = trdepttrainingneed_allocation_tran.departmentaltrainingneedunkid " & _
                '               "AND trdepartmentaltrainingneed_master.targetedgroupunkid > 0 " & _
                '               "AND trdepttrainingneed_allocation_tran.isvoid = 0 " & _
                '        "LEFT JOIN trdepttrainingneed_resources_tran ON trdepartmentaltrainingneed_master.departmentaltrainingneedunkid = trdepttrainingneed_resources_tran.departmentaltrainingneedunkid " & _
                '               "AND trdepttrainingneed_resources_tran.isvoid = 0 "
            Else
                If intEmployeeUnkId > 0 Then
                    strQ &= "LEFT JOIN trdepttrainingneed_employee_tran ON trdepartmentaltrainingneed_master.departmentaltrainingneedunkid = trdepttrainingneed_employee_tran.departmentaltrainingneedunkid " & _
                                "AND trdepttrainingneed_employee_tran.isvoid = 0 "
                End If
            End If

            If intAllocationtranUnkId > 0 Then
                strQ &= "LEFT JOIN trdepttrainingneed_allocation_tran ON trdepartmentaltrainingneed_master.departmentaltrainingneedunkid = trdepttrainingneed_allocation_tran.departmentaltrainingneedunkid " & _
                            "AND trdepttrainingneed_allocation_tran.isvoid = 0 "
            End If

            strQ &= "WHERE trdepartmentaltrainingneed_master.isvoid = 0 " & _
                              "AND trtraining_calendar_master.isactive = 1 " & _
                              "AND trtrainingcategory_master.isactive = 1 " & _
                              "AND trtrainingpriority_master.isactive = 1 "

            If intEmployeeUnkId > 0 Then
                strQ &= " AND trdepttrainingneed_employee_tran.employeeunkid = @employeeunkid "
                objDataOperation.AddParameter("@employeeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intEmployeeUnkId)
            End If

            If intAllocationtranUnkId > 0 Then
                strQ &= " AND trdepttrainingneed_allocation_tran.allocationtranunkid = @allocationtranunkid "
                objDataOperation.AddParameter("@allocationtranunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intAllocationtranUnkId)
            End If

            If strFilter.Trim <> "" Then
                strQ &= " " & strFilter & " "
            End If

            objDataOperation.AddParameter("@employeenames", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 19, "Employee Names"))

            dsList = objDataOperation.ExecQuery(strQ, strTableName)

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            If blnIncludeChecildTablesData = True Then
                Dim strMasterUnkIDs As String = String.Join(",", (From p In dsList.Tables(0) Select (p.Item("departmentaltrainingneedunkid").ToString)).ToArray)
                If strMasterUnkIDs.Trim <> "" Then
                    'Dim objTTemp As New clsDepttrainingneed_employee_Tran
                    'Dim dsEmp As DataSet = objTTemp.GetList(xDatabaseName, xUserUnkid, xYearUnkid, xCompanyUnkid, xPeriodStart, xPeriodEnd, xUserModeSetting, xOnlyApproved, xIncludeIn_ActiveEmployee, "Emp", True, "", -1, " AND trdepttrainingneed_employee_tran.departmentaltrainingneedunkid IN (" & strMasterUnkIDs & ") ")

                    Dim objTAlloc As New clsDepttrainingneed_allocation_Tran
                    objTAlloc._DataOp = objDataOperation
                    Dim dsTAlloc As DataSet = objTAlloc.GetList("Alloc", -1, " AND trdepttrainingneed_allocation_tran.departmentaltrainingneedunkid IN (" & strMasterUnkIDs & ") ")

                    Dim objTReource As New clsDepttrainingneed_resources_Tran
                    objTReource._DataOp = objDataOperation
                    Dim dsResource As DataSet = objTReource.GetList("Reource", -1, " AND trdepttrainingneed_resources_tran.departmentaltrainingneedunkid IN (" & strMasterUnkIDs & ") ")

                    Dim objFSource As New clsDepttrainingneed_financingsources_Tran
                    objFSource._DataOp = objDataOperation
                    Dim dsFSource As DataSet = objFSource.GetList("FSource", -1, " AND trdepttrainingneed_financingsources_tran.departmentaltrainingneedunkid IN (" & strMasterUnkIDs & ") ")

                    Dim objTCoord As New clsDepttrainingneed_trainingcoordinator_Tran
                    objTCoord._DataOp = objDataOperation
                    Dim dsTCoord As DataSet = objTCoord.GetList(xDatabaseName, xUserUnkid, xYearUnkid, xCompanyUnkid, xPeriodStart, xPeriodEnd, xUserModeSetting, xOnlyApproved, xIncludeIn_ActiveEmployee, "Emp", True, "", -1, " AND trdepttrainingneed_trainingcoordinator_tran.departmentaltrainingneedunkid IN (" & strMasterUnkIDs & ") ")

                    Dim objTCost As New clsDepttrainingneed_costitem_Tran
                    objTCost._DataOp = objDataOperation
                    Dim dsTCost As DataSet = objTCost.GetList("TCost", -1, " AND trdepttrainingneed_costitem_tran.departmentaltrainingneedunkid IN (" & strMasterUnkIDs & ") ")

                    'dsList.Tables(0).PrimaryKey = New DataColumn() {dsList.Tables(0).Columns("departmentaltrainingneedunkid")}
                    'dsEmp.Tables(0).PrimaryKey = New DataColumn() {dsEmp.Tables(0).Columns("departmentaltrainingneedunkid")}
                    'dsResource.Tables(0).PrimaryKey = New DataColumn() {dsResource.Tables(0).Columns("departmentaltrainingneedunkid")}

                    'dsList.Tables(0).Merge(dsEmp.Tables(0), True)
                    'dsList.Tables(0).Merge(dsResource.Tables(0), True, MissingSchemaAction.Add)
                    Dim dtTable As DataTable = New DataView(dsList.Tables(0)).ToTable(True, "departmentaltrainingneedunkid")
                    For Each dsRow As DataRow In dtTable.Rows
                        Dim drM() As DataRow = Nothing
                        Dim drT() As DataRow = Nothing

                        'drM = dsList.Tables(0).Select("departmentaltrainingneedunkid = " & CInt(dsRow.Item("departmentaltrainingneedunkid")) & " ")
                        'drT = dsEmp.Tables(0).Select("departmentaltrainingneedunkid = " & CInt(dsRow.Item("departmentaltrainingneedunkid")) & " ")

                        'For i As Integer = 0 To drT.Length - 1
                        '    If i > drM.Length - 1 Then
                        '        Dim dr As DataRow = dsList.Tables(0).NewRow
                        '        dr.Item("departmentaltrainingneedunkid") = CInt(drT(i).Item("departmentaltrainingneedunkid"))
                        '        dr.Item("periodunkid") = drM(0).Item("periodunkid")
                        '        dr.Item("periodname") = drM(0).Item("periodname")
                        '        dr.Item("competenceunkid") = drM(0).Item("competenceunkid")
                        '        dr.Item("competencename") = drM(0).Item("competencename")
                        '        dr.Item("trainingcategoryunkid") = drM(0).Item("trainingcategoryunkid")
                        '        dr.Item("trainingcategoryname") = drM(0).Item("trainingcategoryname")
                        '        dr.Item("trainingcourseunkid") = drM(0).Item("trainingcourseunkid")
                        '        dr.Item("trainingcoursename") = drM(0).Item("trainingcoursename")
                        '        dr.Item("learningmethodunkid") = drM(0).Item("learningmethodunkid")
                        '        dr.Item("learningmethodname") = drM(0).Item("learningmethodname")
                        '        dr.Item("targetedgroupunkid") = drM(0).Item("targetedgroupunkid")
                        '        dr.Item("targetedgroupname") = drM(0).Item("targetedgroupname")
                        '        dr.Item("noofstaff") = drM(0).Item("noofstaff")
                        '        dr.Item("startdate") = drM(0).Item("startdate")
                        '        dr.Item("enddate") = drM(0).Item("enddate")
                        '        dr.Item("trainingpriority") = drM(0).Item("trainingpriority")
                        '        dr.Item("trainingpriorityname") = drM(0).Item("trainingpriorityname")
                        '        dr.Item("trainingproviderunkid") = drM(0).Item("trainingproviderunkid")
                        '        dr.Item("trainingprovidername") = drM(0).Item("trainingprovidername")
                        '        dr.Item("trainingvenueunkid") = drM(0).Item("trainingvenueunkid")
                        '        dr.Item("trainingvenuename") = drM(0).Item("trainingvenuename")
                        '        dr.Item("remark") = drM(0).Item("remark")
                        '        dr.Item("iscertirequired") = drM(0).Item("iscertirequired")
                        '        dr.Item("totalcost") = drM(0).Item("totalcost")

                        '        dr.Item("allocationtranunkid") = CInt(drT(i).Item("employeeunkid"))
                        '        dr.Item("allocationtrancode") = drT(i).Item("employeecode").ToString
                        '        dr.Item("allocationtranname") = drT(i).Item("employeename").ToString
                        '        dsList.Tables(0).Rows.Add(dr)
                        '    Else
                        '        drM(i).Item("allocationtranunkid") = CInt(drT(i).Item("employeeunkid"))
                        '        drM(i).Item("allocationtrancode") = drT(i).Item("employeecode").ToString
                        '        drM(i).Item("allocationtranname") = drT(i).Item("employeename").ToString
                        '    End If
                        'Next
                        'dsList.Tables(0).AcceptChanges()

                        drM = dsList.Tables(0).Select("departmentaltrainingneedunkid = " & CInt(dsRow.Item("departmentaltrainingneedunkid")) & " ")
                        drT = dsTAlloc.Tables(0).Select("departmentaltrainingneedunkid = " & CInt(dsRow.Item("departmentaltrainingneedunkid")) & " ")

                        For i As Integer = 0 To drT.Length - 1
                            If i > drM.Length - 1 Then
                                Dim dr As DataRow = dsList.Tables(0).NewRow
                                dr.Item("departmentaltrainingneedunkid") = CInt(drT(i).Item("departmentaltrainingneedunkid"))
                                dr.Item("departmentunkid") = drM(0).Item("departmentunkid")
                                dr.Item("periodunkid") = drM(0).Item("periodunkid")
                                dr.Item("periodname") = drM(0).Item("periodname")
                                dr.Item("competenceunkid") = drM(0).Item("competenceunkid")
                                dr.Item("competencename") = drM(0).Item("competencename")
                                dr.Item("trainingcategoryunkid") = drM(0).Item("trainingcategoryunkid")
                                dr.Item("trainingcategoryname") = drM(0).Item("trainingcategoryname")
                                dr.Item("trainingcourseunkid") = drM(0).Item("trainingcourseunkid")
                                dr.Item("trainingcoursename") = drM(0).Item("trainingcoursename")
                                dr.Item("learningmethodunkid") = drM(0).Item("learningmethodunkid")
                                dr.Item("learningmethodname") = drM(0).Item("learningmethodname")
                                dr.Item("targetedgroupunkid") = drM(0).Item("targetedgroupunkid")
                                dr.Item("targetedgroupname") = drM(0).Item("targetedgroupname")
                                dr.Item("noofstaff") = drM(0).Item("noofstaff")
                                dr.Item("startdate") = drM(0).Item("startdate")
                                dr.Item("enddate") = drM(0).Item("enddate")
                                dr.Item("trainingpriority") = drM(0).Item("trainingpriority")
                                dr.Item("trainingpriorityname") = drM(0).Item("trainingpriorityname")
                                dr.Item("trainingproviderunkid") = drM(0).Item("trainingproviderunkid")
                                dr.Item("trainingprovidername") = drM(0).Item("trainingprovidername")
                                dr.Item("trainingvenueunkid") = drM(0).Item("trainingvenueunkid")
                                dr.Item("trainingvenuename") = drM(0).Item("trainingvenuename")
                                dr.Item("other_competence") = drM(0).Item("other_competence")
                                dr.Item("other_trainingcourse") = drM(0).Item("other_trainingcourse")
                                dr.Item("statusunkid") = drM(0).Item("statusunkid")
                                dr.Item("statusname") = drM(0).Item("statusname")
                                dr.Item("issubmitforapproval") = drM(0).Item("issubmitforapproval")
                                dr.Item("remark") = drM(0).Item("remark")
                                dr.Item("iscertirequired") = drM(0).Item("iscertirequired")
                                dr.Item("totalcost") = drM(0).Item("totalcost")
                                dr.Item("isactive") = drM(0).Item("isactive")
                                dr.Item("allocationid") = drM(0).Item("allocationid")
                                'Sohail (12 Apr 2021) -- Start
                                'NMB Enhancement : : Changes in Departmental Training Need as per comments in online doc in New UI.
                                dr.Item("moduleid") = drM(0).Item("moduleid")
                                dr.Item("moduletranunkid") = drM(0).Item("moduletranunkid")
                                dr.Item("istrainingcostoptional") = drM(0).Item("istrainingcostoptional")
                                'Sohail (12 Apr 2021) -- End
                                'Hemant (15 Apr 2021) -- Start
                                'NMB Enhancement : : Changes in Departmental Training Need as per comments in online doc in New UI.
                                dr.Item("insertformid") = drM(0).Item("insertformid")
                                'Hemant (15 Apr 2021) -- End
                                dr.Item("refno") = drM(0).Item("refno")
                                dr.Item("approved_totalcost") = drM(0).Item("approved_totalcost")
                                dr.Item("request_statusunkid") = drM(0).Item("request_statusunkid")
                                'Sohail (04 Aug 2021) -- Start
                                'NMB Enhancement : OLD - 429 : Move Submit for Approval button from current location to the Add/Edit Departmental Training Needs Screen.
                                dr.Item("unlock_remark") = drM(0).Item("unlock_remark").ToString
                                dr.Item("finalapproval_remark") = drM(0).Item("finalapproval_remark").ToString
                                'Sohail (04 Aug 2021) -- End
                                dr.Item("createuserunkid") = drM(0).Item("createuserunkid") 'Sohail (17 Aug 2021)
                                dr.Item("allocationtranunkid") = CInt(drT(i).Item("allocationtranunkid"))
                                dr.Item("allocationtrancode") = drT(i).Item("AllocationCode").ToString
                                dr.Item("allocationtranname") = drT(i).Item("AllocationName").ToString
                                dsList.Tables(0).Rows.Add(dr)
                            Else
                                drM(i).Item("allocationtranunkid") = CInt(drT(i).Item("allocationtranunkid"))
                                drM(i).Item("allocationtrancode") = drT(i).Item("AllocationCode").ToString
                                drM(i).Item("allocationtranname") = drT(i).Item("AllocationName").ToString
                            End If
                        Next
                        dsList.Tables(0).AcceptChanges()

                        drM = dsList.Tables(0).Select("departmentaltrainingneedunkid = " & CInt(dsRow.Item("departmentaltrainingneedunkid")) & " ")
                        drT = dsResource.Tables(0).Select("departmentaltrainingneedunkid = " & CInt(dsRow.Item("departmentaltrainingneedunkid")) & " ")

                        For i As Integer = 0 To drT.Length - 1
                            If i > drM.Length - 1 Then
                                Dim dr As DataRow = dsList.Tables(0).NewRow
                                dr.Item("departmentaltrainingneedunkid") = CInt(drT(i).Item("departmentaltrainingneedunkid"))
                                dr.Item("departmentunkid") = drM(0).Item("departmentunkid")
                                dr.Item("periodunkid") = drM(0).Item("periodunkid")
                                dr.Item("periodname") = drM(0).Item("periodname")
                                dr.Item("competenceunkid") = drM(0).Item("competenceunkid")
                                dr.Item("competencename") = drM(0).Item("competencename")
                                dr.Item("trainingcategoryunkid") = drM(0).Item("trainingcategoryunkid")
                                dr.Item("trainingcategoryname") = drM(0).Item("trainingcategoryname")
                                dr.Item("trainingcourseunkid") = drM(0).Item("trainingcourseunkid")
                                dr.Item("trainingcoursename") = drM(0).Item("trainingcoursename")
                                dr.Item("learningmethodunkid") = drM(0).Item("learningmethodunkid")
                                dr.Item("learningmethodname") = drM(0).Item("learningmethodname")
                                dr.Item("targetedgroupunkid") = drM(0).Item("targetedgroupunkid")
                                dr.Item("targetedgroupname") = drM(0).Item("targetedgroupname")
                                dr.Item("noofstaff") = drM(0).Item("noofstaff")
                                dr.Item("startdate") = drM(0).Item("startdate")
                                dr.Item("enddate") = drM(0).Item("enddate")
                                dr.Item("trainingpriority") = drM(0).Item("trainingpriority")
                                dr.Item("trainingpriorityname") = drM(0).Item("trainingpriorityname")
                                dr.Item("trainingproviderunkid") = drM(0).Item("trainingproviderunkid")
                                dr.Item("trainingprovidername") = drM(0).Item("trainingprovidername")
                                dr.Item("trainingvenueunkid") = drM(0).Item("trainingvenueunkid")
                                dr.Item("trainingvenuename") = drM(0).Item("trainingvenuename")
                                dr.Item("other_competence") = drM(0).Item("other_competence")
                                dr.Item("other_trainingcourse") = drM(0).Item("other_trainingcourse")
                                dr.Item("statusunkid") = drM(0).Item("statusunkid")
                                dr.Item("statusname") = drM(0).Item("statusname")
                                dr.Item("issubmitforapproval") = drM(0).Item("issubmitforapproval")
                                dr.Item("remark") = drM(0).Item("remark")
                                dr.Item("iscertirequired") = drM(0).Item("iscertirequired")
                                dr.Item("totalcost") = drM(0).Item("totalcost")
                                dr.Item("isactive") = drM(0).Item("isactive")
                                dr.Item("allocationid") = drM(0).Item("allocationid")
                                'Sohail (12 Apr 2021) -- Start
                                'NMB Enhancement : : Changes in Departmental Training Need as per comments in online doc in New UI.
                                dr.Item("moduleid") = drM(0).Item("moduleid")
                                dr.Item("moduletranunkid") = drM(0).Item("moduletranunkid")
                                dr.Item("istrainingcostoptional") = drM(0).Item("istrainingcostoptional")
                                'Sohail (12 Apr 2021) -- End
                                'Hemant (15 Apr 2021) -- Start
                                'NMB Enhancement : : Changes in Departmental Training Need as per comments in online doc in New UI.
                                dr.Item("insertformid") = drM(0).Item("insertformid")
                                'Hemant (15 Apr 2021) -- End
                                dr.Item("refno") = drM(0).Item("refno")
                                dr.Item("approved_totalcost") = drM(0).Item("approved_totalcost")
                                dr.Item("request_statusunkid") = drM(0).Item("request_statusunkid")
                                'Sohail (04 Aug 2021) -- Start
                                'NMB Enhancement : OLD - 429 : Move Submit for Approval button from current location to the Add/Edit Departmental Training Needs Screen.
                                dr.Item("unlock_remark") = drM(0).Item("unlock_remark").ToString
                                dr.Item("finalapproval_remark") = drM(0).Item("finalapproval_remark").ToString
                                'Sohail (04 Aug 2021) -- End
                                dr.Item("createuserunkid") = drM(0).Item("createuserunkid") 'Sohail (17 Aug 2021)
                                dr.Item("depttrainingneedresourcestranunkid") = CInt(drT(i).Item("depttrainingneedresourcestranunkid"))
                                dr.Item("trainingresourcename") = drT(i).Item("trainingresourcename").ToString
                                dsList.Tables(0).Rows.Add(dr)
                            Else
                                drM(i).Item("depttrainingneedresourcestranunkid") = CInt(drT(i).Item("depttrainingneedresourcestranunkid"))
                                drM(i).Item("trainingresourcename") = drT(i).Item("trainingresourcename").ToString
                            End If
                        Next
                        dsList.Tables(0).AcceptChanges()

                        drM = dsList.Tables(0).Select("departmentaltrainingneedunkid = " & CInt(dsRow.Item("departmentaltrainingneedunkid")) & " ")
                        drT = dsFSource.Tables(0).Select("departmentaltrainingneedunkid = " & CInt(dsRow.Item("departmentaltrainingneedunkid")) & " ")

                        For i As Integer = 0 To drT.Length - 1
                            If i > drM.Length - 1 Then
                                Dim dr As DataRow = dsList.Tables(0).NewRow
                                dr.Item("departmentaltrainingneedunkid") = CInt(drT(i).Item("departmentaltrainingneedunkid"))
                                dr.Item("departmentunkid") = drM(0).Item("departmentunkid")
                                dr.Item("periodunkid") = drM(0).Item("periodunkid")
                                dr.Item("periodname") = drM(0).Item("periodname")
                                dr.Item("competenceunkid") = drM(0).Item("competenceunkid")
                                dr.Item("competencename") = drM(0).Item("competencename")
                                dr.Item("trainingcategoryunkid") = drM(0).Item("trainingcategoryunkid")
                                dr.Item("trainingcategoryname") = drM(0).Item("trainingcategoryname")
                                dr.Item("trainingcourseunkid") = drM(0).Item("trainingcourseunkid")
                                dr.Item("trainingcoursename") = drM(0).Item("trainingcoursename")
                                dr.Item("learningmethodunkid") = drM(0).Item("learningmethodunkid")
                                dr.Item("learningmethodname") = drM(0).Item("learningmethodname")
                                dr.Item("targetedgroupunkid") = drM(0).Item("targetedgroupunkid")
                                dr.Item("targetedgroupname") = drM(0).Item("targetedgroupname")
                                dr.Item("noofstaff") = drM(0).Item("noofstaff")
                                dr.Item("startdate") = drM(0).Item("startdate")
                                dr.Item("enddate") = drM(0).Item("enddate")
                                dr.Item("trainingpriority") = drM(0).Item("trainingpriority")
                                dr.Item("trainingpriorityname") = drM(0).Item("trainingpriorityname")
                                dr.Item("trainingproviderunkid") = drM(0).Item("trainingproviderunkid")
                                dr.Item("trainingprovidername") = drM(0).Item("trainingprovidername")
                                dr.Item("trainingvenueunkid") = drM(0).Item("trainingvenueunkid")
                                dr.Item("trainingvenuename") = drM(0).Item("trainingvenuename")
                                dr.Item("other_competence") = drM(0).Item("other_competence")
                                dr.Item("other_trainingcourse") = drM(0).Item("other_trainingcourse")
                                dr.Item("statusunkid") = drM(0).Item("statusunkid")
                                dr.Item("statusname") = drM(0).Item("statusname")
                                dr.Item("issubmitforapproval") = drM(0).Item("issubmitforapproval")
                                dr.Item("remark") = drM(0).Item("remark")
                                dr.Item("iscertirequired") = drM(0).Item("iscertirequired")
                                dr.Item("totalcost") = drM(0).Item("totalcost")
                                dr.Item("isactive") = drM(0).Item("isactive")
                                dr.Item("allocationid") = drM(0).Item("allocationid")
                                'Sohail (12 Apr 2021) -- Start
                                'NMB Enhancement : : Changes in Departmental Training Need as per comments in online doc in New UI.
                                dr.Item("moduleid") = drM(0).Item("moduleid")
                                dr.Item("moduletranunkid") = drM(0).Item("moduletranunkid")
                                dr.Item("istrainingcostoptional") = drM(0).Item("istrainingcostoptional")
                                'Sohail (12 Apr 2021) -- End
                                'Hemant (15 Apr 2021) -- Start
                                'NMB Enhancement : : Changes in Departmental Training Need as per comments in online doc in New UI.
                                dr.Item("insertformid") = drM(0).Item("insertformid")
                                'Hemant (15 Apr 2021) -- End
                                dr.Item("refno") = drM(0).Item("refno")
                                dr.Item("approved_totalcost") = drM(0).Item("approved_totalcost")
                                dr.Item("request_statusunkid") = drM(0).Item("request_statusunkid")
                                'Sohail (04 Aug 2021) -- Start
                                'NMB Enhancement : OLD - 429 : Move Submit for Approval button from current location to the Add/Edit Departmental Training Needs Screen.
                                dr.Item("unlock_remark") = drM(0).Item("unlock_remark").ToString
                                dr.Item("finalapproval_remark") = drM(0).Item("finalapproval_remark").ToString
                                'Sohail (04 Aug 2021) -- End
                                dr.Item("createuserunkid") = drM(0).Item("createuserunkid") 'Sohail (17 Aug 2021)
                                dr.Item("depttrainingneedfinancingsourcestranunkid") = CInt(drT(i).Item("depttrainingneedfinancingsourcestranunkid"))
                                dr.Item("financingsourcename") = drT(i).Item("financingsourcename").ToString
                                dsList.Tables(0).Rows.Add(dr)
                            Else
                                drM(i).Item("depttrainingneedfinancingsourcestranunkid") = CInt(drT(i).Item("depttrainingneedfinancingsourcestranunkid"))
                                drM(i).Item("financingsourcename") = drT(i).Item("financingsourcename").ToString
                            End If
                        Next
                        dsList.Tables(0).AcceptChanges()

                        drM = dsList.Tables(0).Select("departmentaltrainingneedunkid = " & CInt(dsRow.Item("departmentaltrainingneedunkid")) & " ")
                        drT = dsTCoord.Tables(0).Select("departmentaltrainingneedunkid = " & CInt(dsRow.Item("departmentaltrainingneedunkid")) & " ")

                        For i As Integer = 0 To drT.Length - 1
                            If i > drM.Length - 1 Then
                                Dim dr As DataRow = dsList.Tables(0).NewRow
                                dr.Item("departmentaltrainingneedunkid") = CInt(drT(i).Item("departmentaltrainingneedunkid"))
                                dr.Item("departmentunkid") = drM(0).Item("departmentunkid")
                                dr.Item("periodunkid") = drM(0).Item("periodunkid")
                                dr.Item("periodname") = drM(0).Item("periodname")
                                dr.Item("competenceunkid") = drM(0).Item("competenceunkid")
                                dr.Item("competencename") = drM(0).Item("competencename")
                                dr.Item("trainingcategoryunkid") = drM(0).Item("trainingcategoryunkid")
                                dr.Item("trainingcategoryname") = drM(0).Item("trainingcategoryname")
                                dr.Item("trainingcourseunkid") = drM(0).Item("trainingcourseunkid")
                                dr.Item("trainingcoursename") = drM(0).Item("trainingcoursename")
                                dr.Item("learningmethodunkid") = drM(0).Item("learningmethodunkid")
                                dr.Item("learningmethodname") = drM(0).Item("learningmethodname")
                                dr.Item("targetedgroupunkid") = drM(0).Item("targetedgroupunkid")
                                dr.Item("targetedgroupname") = drM(0).Item("targetedgroupname")
                                dr.Item("noofstaff") = drM(0).Item("noofstaff")
                                dr.Item("startdate") = drM(0).Item("startdate")
                                dr.Item("enddate") = drM(0).Item("enddate")
                                dr.Item("trainingpriority") = drM(0).Item("trainingpriority")
                                dr.Item("trainingpriorityname") = drM(0).Item("trainingpriorityname")
                                dr.Item("trainingproviderunkid") = drM(0).Item("trainingproviderunkid")
                                dr.Item("trainingprovidername") = drM(0).Item("trainingprovidername")
                                dr.Item("trainingvenueunkid") = drM(0).Item("trainingvenueunkid")
                                dr.Item("trainingvenuename") = drM(0).Item("trainingvenuename")
                                dr.Item("other_competence") = drM(0).Item("other_competence")
                                dr.Item("other_trainingcourse") = drM(0).Item("other_trainingcourse")
                                dr.Item("statusunkid") = drM(0).Item("statusunkid")
                                dr.Item("statusname") = drM(0).Item("statusname")
                                dr.Item("issubmitforapproval") = drM(0).Item("issubmitforapproval")
                                dr.Item("remark") = drM(0).Item("remark")
                                dr.Item("iscertirequired") = drM(0).Item("iscertirequired")
                                dr.Item("totalcost") = drM(0).Item("totalcost")
                                dr.Item("isactive") = drM(0).Item("isactive")
                                dr.Item("allocationid") = drM(0).Item("allocationid")
                                'Sohail (12 Apr 2021) -- Start
                                'NMB Enhancement : : Changes in Departmental Training Need as per comments in online doc in New UI.
                                dr.Item("moduleid") = drM(0).Item("moduleid")
                                dr.Item("moduletranunkid") = drM(0).Item("moduletranunkid")
                                dr.Item("istrainingcostoptional") = drM(0).Item("istrainingcostoptional")
                                'Sohail (12 Apr 2021) -- End
                                'Hemant (15 Apr 2021) -- Start
                                'NMB Enhancement : : Changes in Departmental Training Need as per comments in online doc in New UI.
                                dr.Item("insertformid") = drM(0).Item("insertformid")
                                'Hemant (15 Apr 2021) -- End
                                dr.Item("refno") = drM(0).Item("refno")
                                dr.Item("approved_totalcost") = drM(0).Item("approved_totalcost")
                                dr.Item("request_statusunkid") = drM(0).Item("request_statusunkid")
                                'Sohail (04 Aug 2021) -- Start
                                'NMB Enhancement : OLD - 429 : Move Submit for Approval button from current location to the Add/Edit Departmental Training Needs Screen.
                                dr.Item("unlock_remark") = drM(0).Item("unlock_remark").ToString
                                dr.Item("finalapproval_remark") = drM(0).Item("finalapproval_remark").ToString
                                'Sohail (04 Aug 2021) -- End
                                dr.Item("createuserunkid") = drM(0).Item("createuserunkid") 'Sohail (17 Aug 2021)
                                dr.Item("depttrainingneedtrainingcoordinatortranunkid") = CInt(drT(i).Item("depttrainingneedtrainingcoordinatortranunkid"))
                                dr.Item("coordinatorname") = drT(i).Item("employeename").ToString
                                dsList.Tables(0).Rows.Add(dr)
                            Else
                                drM(i).Item("depttrainingneedtrainingcoordinatortranunkid") = CInt(drT(i).Item("depttrainingneedtrainingcoordinatortranunkid"))
                                drM(i).Item("coordinatorname") = drT(i).Item("employeename").ToString
                            End If
                        Next
                        dsList.Tables(0).AcceptChanges()

                        drM = dsList.Tables(0).Select("departmentaltrainingneedunkid = " & CInt(dsRow.Item("departmentaltrainingneedunkid")) & " ")
                        drT = dsTCost.Tables(0).Select("departmentaltrainingneedunkid = " & CInt(dsRow.Item("departmentaltrainingneedunkid")) & " ")

                        For i As Integer = 0 To drT.Length - 1
                            If i > drM.Length - 1 Then
                                Dim dr As DataRow = dsList.Tables(0).NewRow
                                dr.Item("departmentaltrainingneedunkid") = CInt(drT(i).Item("departmentaltrainingneedunkid"))
                                dr.Item("departmentunkid") = drM(0).Item("departmentunkid")
                                dr.Item("periodunkid") = drM(0).Item("periodunkid")
                                dr.Item("periodname") = drM(0).Item("periodname")
                                dr.Item("competenceunkid") = drM(0).Item("competenceunkid")
                                dr.Item("competencename") = drM(0).Item("competencename")
                                dr.Item("trainingcategoryunkid") = drM(0).Item("trainingcategoryunkid")
                                dr.Item("trainingcategoryname") = drM(0).Item("trainingcategoryname")
                                dr.Item("trainingcourseunkid") = drM(0).Item("trainingcourseunkid")
                                dr.Item("trainingcoursename") = drM(0).Item("trainingcoursename")
                                dr.Item("learningmethodunkid") = drM(0).Item("learningmethodunkid")
                                dr.Item("learningmethodname") = drM(0).Item("learningmethodname")
                                dr.Item("targetedgroupunkid") = drM(0).Item("targetedgroupunkid")
                                dr.Item("targetedgroupname") = drM(0).Item("targetedgroupname")
                                dr.Item("noofstaff") = drM(0).Item("noofstaff")
                                dr.Item("startdate") = drM(0).Item("startdate")
                                dr.Item("enddate") = drM(0).Item("enddate")
                                dr.Item("trainingpriority") = drM(0).Item("trainingpriority")
                                dr.Item("trainingpriorityname") = drM(0).Item("trainingpriorityname")
                                dr.Item("trainingproviderunkid") = drM(0).Item("trainingproviderunkid")
                                dr.Item("trainingprovidername") = drM(0).Item("trainingprovidername")
                                dr.Item("trainingvenueunkid") = drM(0).Item("trainingvenueunkid")
                                dr.Item("trainingvenuename") = drM(0).Item("trainingvenuename")
                                dr.Item("other_competence") = drM(0).Item("other_competence")
                                dr.Item("other_trainingcourse") = drM(0).Item("other_trainingcourse")
                                dr.Item("statusunkid") = drM(0).Item("statusunkid")
                                dr.Item("statusname") = drM(0).Item("statusname")
                                dr.Item("issubmitforapproval") = drM(0).Item("issubmitforapproval")
                                dr.Item("remark") = drM(0).Item("remark")
                                dr.Item("iscertirequired") = drM(0).Item("iscertirequired")
                                dr.Item("totalcost") = drM(0).Item("totalcost")
                                dr.Item("isactive") = drM(0).Item("isactive")
                                dr.Item("allocationid") = drM(0).Item("allocationid")
                                'Sohail (12 Apr 2021) -- Start
                                'NMB Enhancement : : Changes in Departmental Training Need as per comments in online doc in New UI.
                                dr.Item("moduleid") = drM(0).Item("moduleid")
                                dr.Item("moduletranunkid") = drM(0).Item("moduletranunkid")
                                dr.Item("istrainingcostoptional") = drM(0).Item("istrainingcostoptional")
                                'Sohail (12 Apr 2021) -- End
                                'Hemant (15 Apr 2021) -- Start
                                'NMB Enhancement : : Changes in Departmental Training Need as per comments in online doc in New UI.
                                dr.Item("insertformid") = drM(0).Item("insertformid")
                                'Hemant (15 Apr 2021) -- End
                                dr.Item("refno") = drM(0).Item("refno")
                                dr.Item("approved_totalcost") = drM(0).Item("approved_totalcost")
                                dr.Item("request_statusunkid") = drM(0).Item("request_statusunkid")
                                'Sohail (04 Aug 2021) -- Start
                                'NMB Enhancement : OLD - 429 : Move Submit for Approval button from current location to the Add/Edit Departmental Training Needs Screen.
                                dr.Item("unlock_remark") = drM(0).Item("unlock_remark").ToString
                                dr.Item("finalapproval_remark") = drM(0).Item("finalapproval_remark").ToString
                                'Sohail (04 Aug 2021) -- End
                                dr.Item("createuserunkid") = drM(0).Item("createuserunkid") 'Sohail (17 Aug 2021)
                                dr.Item("depttrainingneedcostitemtranunkid") = CInt(drT(i).Item("depttrainingneedcostitemtranunkid"))
                                dr.Item("info_name") = drT(i).Item("info_name").ToString
                                dr.Item("amount") = drT(i).Item("amount")
                                dsList.Tables(0).Rows.Add(dr)
                            Else
                                drM(i).Item("depttrainingneedcostitemtranunkid") = CInt(drT(i).Item("depttrainingneedcostitemtranunkid"))
                                drM(i).Item("info_name") = drT(i).Item("info_name").ToString
                                drM(i).Item("amount") = drT(i).Item("amount")
                            End If
                        Next
                        dsList.Tables(0).AcceptChanges()

                    Next

                    dtTable = New DataView(dsList.Tables(0), "", "departmentaltrainingneedunkid", DataViewRowState.CurrentRows).ToTable
                    dsList.Tables.Clear()
                    dsList.Tables.Add(dtTable)

                End If

            Else
                Dim strMasterUnkIDs As String = String.Join(",", (From p In dsList.Tables(0) Select (p.Item("departmentaltrainingneedunkid").ToString)).ToArray)
                If strMasterUnkIDs.Trim <> "" Then
                    Dim objTAlloc As New clsDepttrainingneed_allocation_Tran
                    objTAlloc._DataOp = objDataOperation
                    Dim dsTAlloc As DataSet = objTAlloc.GetList("Alloc", -1, " AND trdepttrainingneed_allocation_tran.departmentaltrainingneedunkid IN (" & strMasterUnkIDs & ") ")

                    Dim objTEmp As New clsDepttrainingneed_employee_Tran
                    objTEmp._DataOp = objDataOperation
                    Dim dsTEmp As DataSet = objTEmp.GetList(xDatabaseName, xUserUnkid, xYearUnkid, xCompanyUnkid, xPeriodStart, xPeriodEnd, xUserModeSetting, xOnlyApproved, xIncludeIn_ActiveEmployee, "Alloc", False, "", -1, " AND trdepttrainingneed_employee_tran.departmentaltrainingneedunkid IN (" & strMasterUnkIDs & ") ")

                    Dim objTReource As New clsDepttrainingneed_resources_Tran
                    objTReource._DataOp = objDataOperation
                    Dim dsResource As DataSet = objTReource.GetList("Reource", -1, " AND trdepttrainingneed_resources_tran.departmentaltrainingneedunkid IN (" & strMasterUnkIDs & ") ")

                    Dim objFSource As New clsDepttrainingneed_financingsources_Tran
                    objFSource._DataOp = objDataOperation
                    Dim dsFSource As DataSet = objFSource.GetList("FSource", -1, " AND trdepttrainingneed_financingsources_tran.departmentaltrainingneedunkid IN (" & strMasterUnkIDs & ") ")

                    Dim objTCoord As New clsDepttrainingneed_trainingcoordinator_Tran
                    objTCoord._DataOp = objDataOperation
                    Dim dsTCoord As DataSet = objTCoord.GetList(xDatabaseName, xUserUnkid, xYearUnkid, xCompanyUnkid, xPeriodStart, xPeriodEnd, xUserModeSetting, xOnlyApproved, xIncludeIn_ActiveEmployee, "Emp", True, "", -1, " AND trdepttrainingneed_trainingcoordinator_tran.departmentaltrainingneedunkid IN (" & strMasterUnkIDs & ") ")

                    Dim objTCost As New clsDepttrainingneed_costitem_Tran
                    objTCost._DataOp = objDataOperation
                    Dim dsTCost As DataSet = objTCost.GetList("TCost", -1, " AND trdepttrainingneed_costitem_tran.departmentaltrainingneedunkid IN (" & strMasterUnkIDs & ") ")

                    For Each dsRow As DataRow In dsList.Tables(0).Rows
                        Dim drT() As DataRow = dsTAlloc.Tables(0).Select(" departmentaltrainingneedunkid = " & CInt(dsRow.Item("departmentaltrainingneedunkid")) & " ")
                        If drT.Length > 0 Then
                            Dim dt As DataTable = drT.CopyToDataTable
                            If dt.Rows.Count = 1 Then
                                dsRow.Item("allocationtranunkid") = dt.Rows(0).Item("allocationtranunkid")
                                dsRow.Item("allocationtrancode") = dt.Rows(0).Item("AllocationCode").ToString
                                dsRow.Item("allocationtranname") = dt.Rows(0).Item("AllocationName").ToString
                            Else
                                dsRow.Item("allocationtranunkid") = dt.Rows(0).Item("allocationtranunkid")
                                dsRow.Item("allocationtrancode") = dt.Rows(0).Item("AllocationCode").ToString & " + " & (dt.Rows.Count - 1).ToString & " " & "more..."
                                dsRow.Item("allocationtranname") = dt.Rows(0).Item("AllocationName").ToString & " + " & (dt.Rows.Count - 1).ToString & " " & "more..."
                            End If

                        Else

                            drT = dsTEmp.Tables(0).Select(" departmentaltrainingneedunkid = " & CInt(dsRow.Item("departmentaltrainingneedunkid")) & " ")
                            If drT.Length > 0 Then
                                Dim dt As DataTable = drT.CopyToDataTable
                                If dt.Rows.Count = 1 Then
                                    dsRow.Item("allocationtranunkid") = dt.Rows(0).Item("employeeunkid")
                                    dsRow.Item("allocationtrancode") = dt.Rows(0).Item("employeecode").ToString
                                    dsRow.Item("allocationtranname") = dt.Rows(0).Item("employeename").ToString
                                Else
                                    dsRow.Item("allocationtranunkid") = dt.Rows(0).Item("employeeunkid")
                                    dsRow.Item("allocationtrancode") = dt.Rows(0).Item("employeecode").ToString & " + " & (dt.Rows.Count - 1).ToString & " " & "more..."
                                    dsRow.Item("allocationtranname") = dt.Rows(0).Item("employeename").ToString & " + " & (dt.Rows.Count - 1).ToString & " " & "more..."
                                End If
                            End If
                        End If

                        drT = dsResource.Tables(0).Select(" departmentaltrainingneedunkid = " & CInt(dsRow.Item("departmentaltrainingneedunkid")) & " ")
                        If drT.Length > 0 Then
                            Dim dt As DataTable = drT.CopyToDataTable
                            If dt.Rows.Count = 1 Then
                                dsRow.Item("trainingresourcename") = dt.Rows(0).Item("trainingresourcename").ToString
                            Else
                                dsRow.Item("trainingresourcename") = dt.Rows(0).Item("trainingresourcename").ToString & " + " & (dt.Rows.Count - 1).ToString & " " & "more..."
                            End If
                        End If

                        drT = dsFSource.Tables(0).Select(" departmentaltrainingneedunkid = " & CInt(dsRow.Item("departmentaltrainingneedunkid")) & " ")
                        If drT.Length > 0 Then
                            Dim dt As DataTable = drT.CopyToDataTable
                            If dt.Rows.Count = 1 Then
                                dsRow.Item("financingsourcename") = dt.Rows(0).Item("financingsourcename").ToString
                            Else
                                dsRow.Item("financingsourcename") = dt.Rows(0).Item("financingsourcename").ToString & " + " & (dt.Rows.Count - 1).ToString & " " & "more..."
                            End If
                        End If

                        drT = dsTCoord.Tables(0).Select(" departmentaltrainingneedunkid = " & CInt(dsRow.Item("departmentaltrainingneedunkid")) & " ")
                        If drT.Length > 0 Then
                            Dim dt As DataTable = drT.CopyToDataTable
                            If dt.Rows.Count = 1 Then
                                dsRow.Item("coordinatorname") = dt.Rows(0).Item("employeename").ToString
                            Else
                                dsRow.Item("coordinatorname") = dt.Rows(0).Item("employeename").ToString & " + " & (dt.Rows.Count - 1).ToString & " " & "more..."
                            End If
                        End If

                        drT = dsTCost.Tables(0).Select(" departmentaltrainingneedunkid = " & CInt(dsRow.Item("departmentaltrainingneedunkid")) & " ")
                        If drT.Length > 0 Then
                            Dim dt As DataTable = drT.CopyToDataTable
                            If dt.Rows.Count = 1 Then
                                dsRow.Item("info_name") = dt.Rows(0).Item("info_name").ToString
                                dsRow.Item("amount") = dt.Rows(0).Item("amount").ToString
                            Else
                                dsRow.Item("info_name") = dt.Rows(0).Item("info_name").ToString & " + " & (dt.Rows.Count - 1).ToString & " " & "more..."
                                dsRow.Item("amount") = dt.Rows(0).Item("amount").ToString '& " + " & (dt.Rows.Count - 1).ToString & " " & "more..."
                            End If
                        End If

                    Next
                    dsList.Tables(0).AcceptChanges()

                    objTAlloc = Nothing
                    objTEmp = Nothing
                    objTReource = Nothing
                    objFSource = Nothing
                    objTCoord = Nothing
                    objTCost = Nothing

                End If

            End If

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: GetList; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            If mDataOp Is Nothing Then objDataOperation = Nothing
        End Try
        Return dsList
    End Function


    ''' <summary>
    ''' Modify By: Suhail
    ''' </summary>
    ''' <returns>Boolean</returns>
    ''' <purpose> INSERT INTO Database Table (trdepartmentaltrainingneed_master) </purpose>
    Public Function Insert(Optional ByVal xDataOp As clsDataOperation = Nothing, Optional ByRef intNewUnkId As Integer = 0) As Boolean
        'If isExist(mstrName) Then
        '    mstrMessage = "<Message>"
        '    Return False
        'End If

        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        If xDataOp Is Nothing Then
            objDataOperation = New clsDataOperation
            objDataOperation.BindTransaction()
        Else
            objDataOperation = xDataOp
        End If
        objDataOperation.ClearParameters()

        Try
            objDataOperation.AddParameter("@departmentunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintDepartmentunkid.ToString)
            objDataOperation.AddParameter("@periodunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintPeriodunkid.ToString)
            objDataOperation.AddParameter("@competenceunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintCompetenceunkid.ToString)
            objDataOperation.AddParameter("@trainingcategoryunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintTrainingcategoryunkid.ToString)
            objDataOperation.AddParameter("@trainingcourseunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintTrainingcourseunkid.ToString)
            objDataOperation.AddParameter("@learningmethodunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintLearningmethodunkid.ToString)
            objDataOperation.AddParameter("@targetedgroupunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintTargetedgroupunkid.ToString)
            objDataOperation.AddParameter("@noofstaff", SqlDbType.Int, eZeeDataType.INT_SIZE, mintNoofstaff.ToString)
            'Gajanan [13-Apr-2021] -- Start
            If mdtStartdate = Nothing Then
                objDataOperation.AddParameter("@startdate", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, DBNull.Value)
            Else
                objDataOperation.AddParameter("@startdate", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, mdtStartdate.ToString())
            End If

            If mdtEnddate = Nothing Then
                objDataOperation.AddParameter("@enddate", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, DBNull.Value)
            Else
                objDataOperation.AddParameter("@enddate", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, mdtEnddate.ToString)
            End If
            'Gajanan [13-Apr-2021] -- End
            objDataOperation.AddParameter("@trainingpriority", SqlDbType.Int, eZeeDataType.INT_SIZE, mintTrainingpriority.ToString)
            objDataOperation.AddParameter("@trainingproviderunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintTrainingproviderunkid.ToString)
            objDataOperation.AddParameter("@trainingvenueunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintTrainingvenueunkid.ToString)
            objDataOperation.AddParameter("@other_competence", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrOther_competence.ToString)
            objDataOperation.AddParameter("@other_trainingcourse", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrOther_trainingcourse.ToString)
            objDataOperation.AddParameter("@statusunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintStatusunkid.ToString)
            objDataOperation.AddParameter("@issubmitforapproval", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIssubmitforapproval.ToString)
            objDataOperation.AddParameter("@remark", SqlDbType.NVarChar, eZeeDataType.DESC_SIZE, mstrRemark.ToString)
            objDataOperation.AddParameter("@iscertirequired", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIscertirequired.ToString)
            objDataOperation.AddParameter("@totalcost", SqlDbType.Decimal, eZeeDataType.DECIMAL_SIZE, mdecTotalcost.ToString)
            objDataOperation.AddParameter("@isactive", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsactive.ToString)
            objDataOperation.AddParameter("@userunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintUserunkid.ToString)
            objDataOperation.AddParameter("@loginemployeeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintLoginemployeeunkid.ToString)
            objDataOperation.AddParameter("@isweb", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsweb.ToString)
            objDataOperation.AddParameter("@isvoid", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsvoid.ToString)
            objDataOperation.AddParameter("@voiduserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintVoiduserunkid.ToString)
            objDataOperation.AddParameter("@voidloginemployeeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintVoidloginemployeeunkid.ToString)
            If mdtVoiddatetime = Nothing Then
                objDataOperation.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, DBNull.Value)
            Else
                objDataOperation.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, mdtVoiddatetime.ToString)
            End If
            objDataOperation.AddParameter("@voidreason", SqlDbType.NText, eZeeDataType.NAME_SIZE, mstrVoidreason.ToString)
            objDataOperation.AddParameter("@allocationid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintAllocationId.ToString)
            'Sohail (12 Apr 2021) -- Start
            'NMB Enhancement : : Changes in Departmental Training Need as per comments in online doc in New UI.
            objDataOperation.AddParameter("@moduleid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintModuleId.ToString)
            objDataOperation.AddParameter("@moduletranunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintModuleTranUnkId.ToString)
            objDataOperation.AddParameter("@istrainingcostoptional", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIstrainingcostoptional.ToString)
            'Sohail (12 Apr 2021) -- End
            'Hemant (15 Apr 2021) -- Start
            'NMB Enhancement : : Changes in Departmental Training Need as per comments in online doc in New UI.
            objDataOperation.AddParameter("@insertformid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintInsertFormId.ToString)
            'Hemant (15 Apr 2021) -- End
            objDataOperation.AddParameter("@refno", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrRefno.ToString)
            objDataOperation.AddParameter("@approved_totalcost", SqlDbType.Decimal, eZeeDataType.DECIMAL_SIZE, mdecApproved_Totalcost.ToString)
            objDataOperation.AddParameter("@request_statusunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintRequest_Statusunkid.ToString)
            'Sohail (04 Aug 2021) -- Start
            'NMB Enhancement : OLD - 429 : Move Submit for Approval button from current location to the Add/Edit Departmental Training Needs Screen.
            objDataOperation.AddParameter("@unlock_remark", SqlDbType.NVarChar, eZeeDataType.DESC_SIZE, mstrUnlock_remark.ToString)
            objDataOperation.AddParameter("@finalapproval_remark", SqlDbType.NVarChar, eZeeDataType.DESC_SIZE, mstrFinalapproval_remark.ToString)
            'Sohail (04 Aug 2021) -- End
            objDataOperation.AddParameter("@createuserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintCreateuserunkid.ToString) 'Sohail (17 Aug 2021)

            strQ = "INSERT INTO trdepartmentaltrainingneed_master ( " & _
              "  departmentunkid " & _
              ", periodunkid " & _
              ", competenceunkid " & _
              ", trainingcategoryunkid " & _
              ", trainingcourseunkid " & _
              ", learningmethodunkid " & _
              ", targetedgroupunkid " & _
              ", noofstaff " & _
              ", startdate " & _
              ", enddate " & _
              ", trainingpriority " & _
              ", trainingproviderunkid " & _
              ", trainingvenueunkid " & _
              ", other_competence " & _
              ", other_trainingcourse " & _
              ", statusunkid " & _
              ", issubmitforapproval " & _
              ", remark " & _
              ", iscertirequired " & _
              ", totalcost " & _
              ", isactive " & _
              ", userunkid " & _
              ", loginemployeeunkid " & _
              ", isweb " & _
              ", isvoid " & _
              ", voiduserunkid " & _
              ", voidloginemployeeunkid " & _
              ", voiddatetime " & _
              ", voidreason" & _
              ", allocationid " & _
              ", moduleid " & _
              ", moduletranunkid " & _
              ", istrainingcostoptional " & _
              ", insertformid " & _
              ", refno " & _
              ", approved_totalcost " & _
              ", request_statusunkid " & _
              ", unlock_remark " & _
              ", finalapproval_remark " & _
              ", createuserunkid " & _
            ") VALUES (" & _
              "  @departmentunkid " & _
              ", @periodunkid " & _
              ", @competenceunkid " & _
              ", @trainingcategoryunkid " & _
              ", @trainingcourseunkid " & _
              ", @learningmethodunkid " & _
              ", @targetedgroupunkid " & _
              ", @noofstaff " & _
              ", @startdate " & _
              ", @enddate " & _
              ", @trainingpriority " & _
              ", @trainingproviderunkid " & _
              ", @trainingvenueunkid " & _
              ", @other_competence " & _
              ", @other_trainingcourse " & _
              ", @statusunkid " & _
              ", @issubmitforapproval " & _
              ", @remark " & _
              ", @iscertirequired " & _
              ", @totalcost " & _
              ", @isactive " & _
              ", @userunkid " & _
              ", @loginemployeeunkid " & _
              ", @isweb " & _
              ", @isvoid " & _
              ", @voiduserunkid " & _
              ", @voidloginemployeeunkid " & _
              ", @voiddatetime " & _
              ", @voidreason" & _
              ", @allocationid " & _
              ", @moduleid " & _
              ", @moduletranunkid " & _
              ", @istrainingcostoptional " & _
              ", @insertformid " & _
              ", @refno " & _
              ", @approved_totalcost " & _
              ", @request_statusunkid " & _
              ", @unlock_remark " & _
              ", @finalapproval_remark " & _
              ", @createuserunkid " & _
            "); SELECT @@identity"
            'Sohail (17 Aug 2021) - [createuserunkid]
            'Sohail (04 Aug 2021) - [unlock_remark, finalapproval_remark]
            'Sohail (15 Apr 2021) - [refno, approved_totalcost, request_statusunkid]
            'Hemant (15 Apr 2021) -- [insertformid]
            'Sohail (12 Apr 2021) - [moduleid. moduletranunkid, istrainingcostoptional]

            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            mintDepartmentaltrainingneedunkid = dsList.Tables(0).Rows(0).Item(0)

            intNewUnkId = mintDepartmentaltrainingneedunkid

            If Insert_AtTranLog(objDataOperation, 1) = False Then
                If xDataOp Is Nothing Then objDataOperation.ReleaseTransaction(False)
                Return False
            End If

            If xDataOp Is Nothing Then objDataOperation.ReleaseTransaction(True)

            Return True
        Catch ex As Exception
            If xDataOp Is Nothing Then objDataOperation.ReleaseTransaction(False)
            Throw New Exception(ex.Message & "; Procedure Name: Insert; Module Name: " & mstrModuleName)
            Return False
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            If xDataOp Is Nothing Then objDataOperation = Nothing
        End Try
    End Function

    ''' <summary>
    ''' Modify By: Suhail
    ''' </summary>
    ''' <returns>Boolean</returns>
    ''' <purpose> Update Database Table (trdepartmentaltrainingneed_master) </purpose>
    Public Function Update(Optional ByVal xDataOp As clsDataOperation = Nothing) As Boolean
        'If isExist(mstrName, mintDepartmentaltrainingneedunkid) Then
        '    mstrMessage = "<Message>"
        '    Return False
        'End If

        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        If xDataOp Is Nothing Then
            objDataOperation = New clsDataOperation
            objDataOperation.BindTransaction()
        Else
            objDataOperation = xDataOp
        End If
        objDataOperation.ClearParameters()

        Try
            objDataOperation.AddParameter("@departmentaltrainingneedunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintDepartmentaltrainingneedunkid.ToString)
            objDataOperation.AddParameter("@departmentunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintDepartmentunkid.ToString)
            objDataOperation.AddParameter("@periodunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintPeriodunkid.ToString)
            objDataOperation.AddParameter("@competenceunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintCompetenceunkid.ToString)
            objDataOperation.AddParameter("@trainingcategoryunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintTrainingcategoryunkid.ToString)
            objDataOperation.AddParameter("@trainingcourseunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintTrainingcourseunkid.ToString)
            objDataOperation.AddParameter("@learningmethodunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintLearningmethodunkid.ToString)
            objDataOperation.AddParameter("@targetedgroupunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintTargetedgroupunkid.ToString)
            objDataOperation.AddParameter("@noofstaff", SqlDbType.Int, eZeeDataType.INT_SIZE, mintNoofstaff.ToString)
            'Gajanan [13-Apr-2021] -- Start
            If mdtStartdate = Nothing Then
                objDataOperation.AddParameter("@startdate", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, DBNull.Value)
            Else
                objDataOperation.AddParameter("@startdate", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, mdtStartdate.ToString())
            End If

            If mdtEnddate = Nothing Then
                objDataOperation.AddParameter("@enddate", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, DBNull.Value)
            Else
                objDataOperation.AddParameter("@enddate", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, mdtEnddate.ToString)
            End If
            'Gajanan [13-Apr-2021] -- End
            objDataOperation.AddParameter("@trainingpriority", SqlDbType.Int, eZeeDataType.INT_SIZE, mintTrainingpriority.ToString)
            objDataOperation.AddParameter("@trainingproviderunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintTrainingproviderunkid.ToString)
            objDataOperation.AddParameter("@trainingvenueunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintTrainingvenueunkid.ToString)
            objDataOperation.AddParameter("@other_competence", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrOther_competence.ToString)
            objDataOperation.AddParameter("@other_trainingcourse", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrOther_trainingcourse.ToString)
            objDataOperation.AddParameter("@statusunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintStatusunkid.ToString)
            objDataOperation.AddParameter("@issubmitforapproval", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIssubmitforapproval.ToString)
            objDataOperation.AddParameter("@remark", SqlDbType.NVarChar, eZeeDataType.DESC_SIZE, mstrRemark.ToString)
            objDataOperation.AddParameter("@iscertirequired", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIscertirequired.ToString)
            objDataOperation.AddParameter("@totalcost", SqlDbType.Decimal, eZeeDataType.DECIMAL_SIZE, mdecTotalcost.ToString)
            objDataOperation.AddParameter("@isactive", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsactive.ToString)
            objDataOperation.AddParameter("@userunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintUserunkid.ToString)
            objDataOperation.AddParameter("@loginemployeeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintLoginemployeeunkid.ToString)
            objDataOperation.AddParameter("@isweb", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsweb.ToString)
            objDataOperation.AddParameter("@isvoid", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsvoid.ToString)
            objDataOperation.AddParameter("@voiduserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintVoiduserunkid.ToString)
            objDataOperation.AddParameter("@voidloginemployeeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintVoidloginemployeeunkid.ToString)
            If mdtVoiddatetime = Nothing Then
                objDataOperation.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, DBNull.Value)
            Else
                objDataOperation.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, mdtVoiddatetime.ToString)
            End If
            objDataOperation.AddParameter("@voidreason", SqlDbType.NText, eZeeDataType.NAME_SIZE, mstrVoidreason.ToString)
            objDataOperation.AddParameter("@allocationid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintAllocationId.ToString)
            'Sohail (12 Apr 2021) -- Start
            'NMB Enhancement : : Changes in Departmental Training Need as per comments in online doc in New UI.
            'objDataOperation.AddParameter("@moduleid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintModuleId.ToString)
            'objDataOperation.AddParameter("@moduletranunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintModuleTranUnkId.ToString)
            objDataOperation.AddParameter("@istrainingcostoptional", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIstrainingcostoptional.ToString)
            'Hemant (15 Apr 2021) -- End
            objDataOperation.AddParameter("@refno", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrRefno.ToString)
            objDataOperation.AddParameter("@approved_totalcost", SqlDbType.Decimal, eZeeDataType.DECIMAL_SIZE, mdecApproved_Totalcost.ToString)
            objDataOperation.AddParameter("@request_statusunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintRequest_Statusunkid.ToString)
            'Sohail (04 Aug 2021) -- Start
            'NMB Enhancement : OLD - 429 : Move Submit for Approval button from current location to the Add/Edit Departmental Training Needs Screen.
            objDataOperation.AddParameter("@unlock_remark", SqlDbType.NVarChar, eZeeDataType.DESC_SIZE, mstrUnlock_remark.ToString)
            objDataOperation.AddParameter("@finalapproval_remark", SqlDbType.NVarChar, eZeeDataType.DESC_SIZE, mstrFinalapproval_remark.ToString)
            'Sohail (04 Aug 2021) -- End
            objDataOperation.AddParameter("@createuserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintCreateuserunkid.ToString) 'Sohail (17 Aug 2021)

            strQ = "UPDATE trdepartmentaltrainingneed_master SET " & _
              "  departmentunkid = @departmentunkid" & _
              ", periodunkid = @periodunkid" & _
              ", competenceunkid = @competenceunkid" & _
              ", trainingcategoryunkid = @trainingcategoryunkid" & _
              ", trainingcourseunkid = @trainingcourseunkid" & _
              ", learningmethodunkid = @learningmethodunkid" & _
              ", targetedgroupunkid = @targetedgroupunkid" & _
              ", noofstaff = @noofstaff" & _
              ", startdate = @startdate" & _
              ", enddate = @enddate" & _
              ", trainingpriority = @trainingpriority" & _
              ", trainingproviderunkid = @trainingproviderunkid" & _
              ", trainingvenueunkid = @trainingvenueunkid" & _
              ", other_competence = @other_competence " & _
              ", other_trainingcourse = @other_trainingcourse " & _
              ", statusunkid = @statusunkid " & _
              ", issubmitforapproval = @issubmitforapproval " & _
              ", remark = @remark" & _
              ", iscertirequired = @iscertirequired" & _
              ", totalcost = @totalcost" & _
              ", isactive = @isactive " & _
              ", userunkid = @userunkid" & _
              ", loginemployeeunkid = @loginemployeeunkid" & _
              ", isweb = @isweb" & _
              ", isvoid = @isvoid" & _
              ", voiduserunkid = @voiduserunkid" & _
              ", voidloginemployeeunkid = @voidloginemployeeunkid" & _
              ", voiddatetime = @voiddatetime" & _
              ", voidreason = @voidreason " & _
              ", allocationid = @allocationid " & _
              ", istrainingcostoptional = @istrainingcostoptional " & _
              ", refno = @refno " & _
              ", approved_totalcost = @approved_totalcost " & _
              ", request_statusunkid = @request_statusunkid " & _
              ", unlock_remark = @unlock_remark " & _
              ", finalapproval_remark = @finalapproval_remark " & _
              ", createuserunkid = @createuserunkid " & _
            "WHERE departmentaltrainingneedunkid = @departmentaltrainingneedunkid "
            'Sohail (17 Aug 2021) - [createuserunkid]
            'Sohail (04 Aug 2021) - [unlock_remark, finalapproval_remark]
            'Sohail (12 Apr 2021) - [moduleid. moduletranunkid, istrainingcostoptional, refno, approved_totalcost, request_statusunkid]

            Call objDataOperation.ExecNonQuery(strQ)

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            If IsTableDataUpdate(mintDepartmentaltrainingneedunkid, objDataOperation) = False Then

                If Insert_AtTranLog(objDataOperation, 2) = False Then
                    If xDataOp Is Nothing Then objDataOperation.ReleaseTransaction(False)
                    Return False
                End If

            End If

            If xDataOp Is Nothing Then objDataOperation.ReleaseTransaction(True)

            Return True
        Catch ex As Exception
            If xDataOp Is Nothing Then objDataOperation.ReleaseTransaction(False)
            Throw New Exception(ex.Message & "; Procedure Name: Update; Module Name: " & mstrModuleName)
            Return False
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            If xDataOp Is Nothing Then objDataOperation = Nothing
        End Try
    End Function

    Public Function Save(Optional ByVal xDataOp As clsDataOperation = Nothing) As Boolean
        Dim exForce As Exception
        Dim dsList As DataSet = Nothing

        If xDataOp Is Nothing Then
            objDataOperation = New clsDataOperation
            objDataOperation.BindTransaction()
        Else
            objDataOperation = xDataOp
        End If
        objDataOperation.ClearParameters()


        Try
            'If isExist(mintPeriodunkid, mintTrainingcourseunkid, mintCompetenceunkid, mintTargetedgroupunkid, mstrTargetedGroupTranIDs, mintDepartmentaltrainingneedunkid, objDataOperation) > 0 Then
            '    If Update(objDataOperation) = False Then
            '        exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
            '        Throw exForce
            '    End If
            'Else
            '    If Insert(objDataOperation) = False Then
            '        exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
            '        Throw exForce
            '    End If
            'End If
            If mintDepartmentaltrainingneedunkid > 0 Then
                If Update(objDataOperation) = False Then
                    exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                    Throw exForce
                End If

                If mlstDeptTAllocVoid IsNot Nothing AndAlso mlstDeptTAllocVoid.Count > 0 Then
                    Dim objTAlloc As New clsDepttrainingneed_allocation_Tran
                    If objTAlloc.VoidAll(mlstDeptTAllocVoid, , objDataOperation) = False Then
                        Return False
                    End If
                End If

                If mlstDeptTEmpVoid IsNot Nothing AndAlso mlstDeptTEmpVoid.Count > 0 Then
                    Dim objTEmp As New clsDepttrainingneed_employee_Tran
                    If objTEmp.VoidAll(mlstDeptTEmpVoid, objDataOperation) = False Then
                        Return False
                    End If
                End If

                If mlstDeptTResourceVoid IsNot Nothing AndAlso mlstDeptTResourceVoid.Count > 0 Then
                    Dim objTResource As New clsDepttrainingneed_resources_Tran
                    If objTResource.VoidAll(mlstDeptTResourceVoid, objDataOperation) = False Then
                        Return False
                    End If
                End If

                If mlstDeptTFSourceVoid IsNot Nothing AndAlso mlstDeptTFSourceVoid.Count > 0 Then
                    Dim objTFSource As New clsDepttrainingneed_financingsources_Tran
                    If objTFSource.VoidAll(mlstDeptTFSourceVoid, objDataOperation) = False Then
                        Return False
                    End If
                End If

                If mlstDeptTCoordVoid IsNot Nothing AndAlso mlstDeptTCoordVoid.Count > 0 Then
                    Dim objTFCoord As New clsDepttrainingneed_trainingcoordinator_Tran
                    If objTFCoord.VoidAll(mlstDeptTCoordVoid, objDataOperation) = False Then
                        Return False
                    End If
                End If

                If mlstDeptTCostItemVoid IsNot Nothing AndAlso mlstDeptTCostItemVoid.Count > 0 Then
                    Dim objTCostItem As New clsDepttrainingneed_costitem_Tran
                    If objTCostItem.VoidAll(mlstDeptTCostItemVoid, objDataOperation) = False Then
                        Return False
                    End If
                End If

                'Sohail (07 Jun 2021) -- Start
                'KBC Enhancement : OLD-391 : Imprest posting to Payroll.
                If mlstDeptTInstructVoid IsNot Nothing AndAlso mlstDeptTInstructVoid.Count > 0 Then
                    Dim objTnstruct As New clsDepttrainingneed_traininginstructor_Tran
                    If objTnstruct.VoidAll(mlstDeptTInstructVoid, objDataOperation) = False Then
                        Return False
                    End If
                End If
                'Sohail (07 JUn 2021) -- End

            Else
                If Insert(objDataOperation) = False Then
                    exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                    Throw exForce
                End If
            End If

            If mlstDeptTAllocNew IsNot Nothing AndAlso mlstDeptTAllocNew.Count > 0 Then
                Dim objTAlloc As New clsDepttrainingneed_allocation_Tran
                objTAlloc._Departmentaltrainingneedunkid = mintDepartmentaltrainingneedunkid
                If objTAlloc.SaveAll(mlstDeptTAllocNew, , objDataOperation) = False Then
                    Return False
                End If
            End If

            If mlstDeptTEmpNew IsNot Nothing AndAlso mlstDeptTEmpNew.Count > 0 Then
                Dim objTEmp As New clsDepttrainingneed_employee_Tran
                objTEmp._Departmentaltrainingneedunkid = mintDepartmentaltrainingneedunkid
                If objTEmp.SaveAll(mlstDeptTEmpNew, , objDataOperation) = False Then
                    Return False
                End If
            End If

            'Sohail (12 Apr 2021) -- Start
            'NMB Enhancement : : Changes in Departmental Training Need as per comments in online doc in New UI.
            If mlstDeptTResourceNew IsNot Nothing AndAlso mlstDeptTResourceNew.Count > 0 Then
                Dim objTResource As New clsDepttrainingneed_resources_Tran
                objTResource._Departmentaltrainingneedunkid = mintDepartmentaltrainingneedunkid
                If objTResource.SaveAll(mlstDeptTResourceNew, , objDataOperation) = False Then
                    Return False
                End If
            End If

            If mlstDeptTFSourceNew IsNot Nothing AndAlso mlstDeptTFSourceNew.Count > 0 Then
                Dim objTFSource As New clsDepttrainingneed_financingsources_Tran
                objTFSource._Departmentaltrainingneedunkid = mintDepartmentaltrainingneedunkid
                If objTFSource.SaveAll(mlstDeptTFSourceNew, , objDataOperation) = False Then
                    Return False
                End If
            End If

            If mlstDeptTCoordNew IsNot Nothing AndAlso mlstDeptTCoordNew.Count > 0 Then
                Dim objTFCoord As New clsDepttrainingneed_trainingcoordinator_Tran
                objTFCoord._Departmentaltrainingneedunkid = mintDepartmentaltrainingneedunkid
                If objTFCoord.SaveAll(mlstDeptTCoordNew, , objDataOperation) = False Then
                    Return False
                End If
            End If

            If mlstDeptTCostItemNew IsNot Nothing AndAlso mlstDeptTCostItemNew.Count > 0 Then
                Dim objTFCostItem As New clsDepttrainingneed_costitem_Tran
                objTFCostItem._Departmentaltrainingneedunkid = mintDepartmentaltrainingneedunkid
                If objTFCostItem.SaveAll(mlstDeptTCostItemNew, , objDataOperation) = False Then
                    Return False
                End If
            End If
            'Sohail (12 Apr 2021) -- End

            'Sohail (09 Jun 2021) -- Start
            'Finca Uganda Enhancement : OLD-405 : Add new informational Tab for Training Instructor on Departmental Training Needs Screen.
            If mlstDeptTInstructNew IsNot Nothing AndAlso mlstDeptTInstructNew.Count > 0 Then
                Dim objTInstruct As New clsDepttrainingneed_traininginstructor_Tran
                objTInstruct._Departmentaltrainingneedunkid = mintDepartmentaltrainingneedunkid
                If objTInstruct.SaveAll(mlstDeptTInstructNew, , objDataOperation) = False Then
                    Return False
                End If
            End If
            'Sohail (09 Jun 2021) -- End

            If xDataOp Is Nothing Then objDataOperation.ReleaseTransaction(True)

            Return True

        Catch ex As Exception
            If xDataOp Is Nothing Then objDataOperation.ReleaseTransaction(False)
            Throw New Exception(ex.Message & "; Procedure Name: Save; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            If xDataOp Is Nothing Then objDataOperation = Nothing
        End Try
    End Function

    ''' <summary>
    ''' Modify By: Suhail
    ''' </summary>
    ''' <returns>Boolean</returns>
    ''' <purpose> Delete Database Table (trdepartmentaltrainingneed_master) </purpose>
    Public Function Void(ByVal intUnkid As Integer, _
                         Optional ByVal xDataOp As clsDataOperation = Nothing _
                         ) As Boolean
        'If isUsed(intUnkid) Then
        '    mstrMessage = "<Message>"
        '    Return False
        'End If

        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        If xDataOp IsNot Nothing Then
            objDataOperation = xDataOp
        Else
            objDataOperation = New clsDataOperation
            objDataOperation.BindTransaction()
        End If
        objDataOperation.ClearParameters()

        Try
            strQ = "UPDATE trdepartmentaltrainingneed_master SET " & _
              "  isvoid = 1 " & _
              ", voiduserunkid = @voiduserunkid " & _
              ", voiddatetime = @voiddatetime " & _
              ", voidreason = @voidreason " & _
            "WHERE departmentaltrainingneedunkid = @departmentaltrainingneedunkid "

            objDataOperation.AddParameter("@departmentaltrainingneedunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intUnkid)
            objDataOperation.AddParameter("@voiduserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintVoiduserunkid)
            objDataOperation.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, mdtVoiddatetime)
            objDataOperation.AddParameter("@voidreason", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrVoidreason)

            Call objDataOperation.ExecNonQuery(strQ)

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            If Insert_AtTranLog(objDataOperation, 3) = False Then
                If xDataOp Is Nothing Then objDataOperation.ReleaseTransaction(False)
                Return False
            End If


            Dim objDTEmp As New clsDepttrainingneed_employee_Tran
            If (objDataOperation.RecordCount("SELECT 1 FROM trdepttrainingneed_employee_tran WHERE isvoid = 0 AND departmentaltrainingneedunkid = " & intUnkid & " ")) > 0 Then

                With objDTEmp
                    ._Isvoid = True
                    ._Voiddatetime = mdtVoiddatetime
                    ._Voidreason = mstrVoidreason

                    ._Voiduserunkid = mintVoiduserunkid
                    ._Voidloginemployeeunkid = mintVoidloginemployeeunkid
                    ._Isweb = mblnIsweb
                    ._AuditUserId = mintAuditUserId
                    ._AuditDate = mdtAuditDate
                    ._ClientIP = mstrClientIp
                    ._HostName = mstrHostName
                    ._FormName = mstrFormName

                    If .VoidByMasterUnkID(intUnkid, 3, objDataOperation) = False Then
                        objDataOperation.ReleaseTransaction(False)
                        Return False
                    End If
                End With

            End If

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            Dim objDTAlloc As New clsDepttrainingneed_allocation_Tran
            If (objDataOperation.RecordCount("SELECT 1 FROM trdepttrainingneed_allocation_tran WHERE isvoid = 0 AND departmentaltrainingneedunkid = " & intUnkid & " ")) > 0 Then

                With objDTAlloc
                    ._Isvoid = True
                    ._Voiddatetime = mdtVoiddatetime
                    ._Voidreason = mstrVoidreason

                    ._Voiduserunkid = mintVoiduserunkid
                    ._Voidloginemployeeunkid = mintVoidloginemployeeunkid
                    ._Isweb = mblnIsweb
                    ._AuditUserId = mintAuditUserId
                    ._AuditDate = mdtAuditDate
                    ._ClientIP = mstrClientIp
                    ._HostName = mstrHostName
                    ._FormName = mstrFormName

                    If .VoidByMasterUnkID(intUnkid, 3, objDataOperation) = False Then
                        objDataOperation.ReleaseTransaction(False)
                        Return False
                    End If
                End With

            End If

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            Dim objDTResource As New clsDepttrainingneed_resources_Tran
            If (objDataOperation.RecordCount("SELECT 1 FROM trdepttrainingneed_resources_tran WHERE isvoid = 0 AND departmentaltrainingneedunkid = " & intUnkid & " ")) > 0 Then

                With objDTResource
                    ._Isvoid = True
                    ._Voiddatetime = mdtVoiddatetime
                    ._Voidreason = mstrVoidreason

                    ._Voiduserunkid = mintVoiduserunkid
                    ._Voidloginemployeeunkid = mintVoidloginemployeeunkid
                    ._Isweb = mblnIsweb
                    ._AuditUserId = mintAuditUserId
                    ._AuditDate = mdtAuditDate
                    ._ClientIP = mstrClientIp
                    ._HostName = mstrHostName
                    ._FormName = mstrFormName

                    If .VoidByMasterUnkID(intUnkid, 3, objDataOperation) = False Then
                        objDataOperation.ReleaseTransaction(False)
                        Return False
                    End If
                End With

            End If

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            Dim objDTFSource As New clsDepttrainingneed_financingsources_Tran
            If (objDataOperation.RecordCount("SELECT 1 FROM trdepttrainingneed_financingsources_tran WHERE isvoid = 0 AND departmentaltrainingneedunkid = " & intUnkid & " ")) > 0 Then

                With objDTFSource
                    ._Isvoid = True
                    ._Voiddatetime = mdtVoiddatetime
                    ._Voidreason = mstrVoidreason

                    ._Voiduserunkid = mintVoiduserunkid
                    ._Voidloginemployeeunkid = mintVoidloginemployeeunkid
                    ._Isweb = mblnIsweb
                    ._AuditUserId = mintAuditUserId
                    ._AuditDate = mdtAuditDate
                    ._ClientIP = mstrClientIp
                    ._HostName = mstrHostName
                    ._FormName = mstrFormName

                    If .VoidByMasterUnkID(intUnkid, 3, objDataOperation) = False Then
                        objDataOperation.ReleaseTransaction(False)
                        Return False
                    End If
                End With

            End If

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            Dim objDTCoord As New clsDepttrainingneed_trainingcoordinator_Tran
            If (objDataOperation.RecordCount("SELECT 1 FROM trdepttrainingneed_trainingcoordinator_tran WHERE isvoid = 0 AND departmentaltrainingneedunkid = " & intUnkid & " ")) > 0 Then

                With objDTCoord
                    ._Isvoid = True
                    ._Voiddatetime = mdtVoiddatetime
                    ._Voidreason = mstrVoidreason

                    ._Voiduserunkid = mintVoiduserunkid
                    ._Voidloginemployeeunkid = mintVoidloginemployeeunkid
                    ._Isweb = mblnIsweb
                    ._AuditUserId = mintAuditUserId
                    ._AuditDate = mdtAuditDate
                    ._ClientIP = mstrClientIp
                    ._HostName = mstrHostName
                    ._FormName = mstrFormName

                    If .VoidByMasterUnkID(intUnkid, 3, objDataOperation) = False Then
                        objDataOperation.ReleaseTransaction(False)
                        Return False
                    End If
                End With

            End If

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            Dim objDTCostItem As New clsDepttrainingneed_costitem_Tran
            If (objDataOperation.RecordCount("SELECT 1 FROM trdepttrainingneed_costitem_tran WHERE isvoid = 0 AND departmentaltrainingneedunkid = " & intUnkid & " ")) > 0 Then

                With objDTCostItem
                    ._Isvoid = True
                    ._Voiddatetime = mdtVoiddatetime
                    ._Voidreason = mstrVoidreason

                    ._Voiduserunkid = mintVoiduserunkid
                    ._Voidloginemployeeunkid = mintVoidloginemployeeunkid
                    ._Isweb = mblnIsweb
                    ._AuditUserId = mintAuditUserId
                    ._AuditDate = mdtAuditDate
                    ._ClientIP = mstrClientIp
                    ._HostName = mstrHostName
                    ._FormName = mstrFormName

                    If .VoidByMasterUnkID(intUnkid, 3, objDataOperation) = False Then
                        objDataOperation.ReleaseTransaction(False)
                        Return False
                    End If
                End With

            End If

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            'Sohail (04 Aug 2021) -- Start
            'NMB Enhancement : OLD - 429 : Move Submit for Approval button from current location to the Add/Edit Departmental Training Needs Screen.
            Dim objDTInstruct As New clsDepttrainingneed_traininginstructor_Tran
            If (objDataOperation.RecordCount("SELECT 1 FROM trdepttrainingneed_traininginstructor_tran WHERE isvoid = 0 AND departmentaltrainingneedunkid = " & intUnkid & " ")) > 0 Then

                With objDTInstruct
                    ._Isvoid = True
                    ._Voiddatetime = mdtVoiddatetime
                    ._Voidreason = mstrVoidreason

                    ._Voiduserunkid = mintVoiduserunkid
                    ._Voidloginemployeeunkid = mintVoidloginemployeeunkid
                    ._Isweb = mblnIsweb
                    ._AuditUserId = mintAuditUserId
                    ._AuditDate = mdtAuditDate
                    ._ClientIP = mstrClientIp
                    ._HostName = mstrHostName
                    ._FormName = mstrFormName

                    If .VoidByMasterUnkID(intUnkid, 3, objDataOperation) = False Then
                        objDataOperation.ReleaseTransaction(False)
                        Return False
                    End If
                End With

            End If

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If
            'Sohail (04 Aug 2021) -- End

            If xDataOp Is Nothing Then objDataOperation.ReleaseTransaction(True)

            Return True
        Catch ex As Exception
            If xDataOp Is Nothing Then objDataOperation.ReleaseTransaction(False)
            Throw New Exception(ex.Message & "; Procedure Name: Void; Module Name: " & mstrModuleName)
            Return False
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            If xDataOp Is Nothing Then objDataOperation = Nothing
        End Try
    End Function

    Public Function MakeActiveInactive(ByVal intUnkid As Integer, _
                                         ByVal blnActive As Boolean, _
                                         Optional ByVal xDataOp As clsDataOperation = Nothing _
                                         ) As Boolean


        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        If xDataOp IsNot Nothing Then
            objDataOperation = xDataOp
        Else
            objDataOperation = New clsDataOperation
            objDataOperation.BindTransaction()
        End If
        objDataOperation.ClearParameters()

        Try
            strQ = "UPDATE trdepartmentaltrainingneed_master SET " & _
              "  isactive = @isactive " & _
              ", userunkid = @userunkid " & _
              ", loginemployeeunkid = @loginemployeeunkid " & _
            "WHERE departmentaltrainingneedunkid = @departmentaltrainingneedunkid "

            objDataOperation.AddParameter("@departmentaltrainingneedunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intUnkid)
            objDataOperation.AddParameter("@userunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintUserunkid)
            objDataOperation.AddParameter("@loginemployeeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintLoginemployeeunkid)
            objDataOperation.AddParameter("@isactive", SqlDbType.Bit, eZeeDataType.BIT_SIZE, blnActive)

            Call objDataOperation.ExecNonQuery(strQ)

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            If Insert_AtTranLog(objDataOperation, 2) = False Then
                If xDataOp Is Nothing Then objDataOperation.ReleaseTransaction(False)
                Return False
            End If


            If xDataOp Is Nothing Then objDataOperation.ReleaseTransaction(True)

            Return True
        Catch ex As Exception
            If xDataOp Is Nothing Then objDataOperation.ReleaseTransaction(False)
            Throw New Exception(ex.Message & "; Procedure Name: MakeActiveInactive; Module Name: " & mstrModuleName)
            Return False
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            If xDataOp Is Nothing Then objDataOperation = Nothing
        End Try
    End Function

    Public Function UpdateStatus(ByVal strUnkIDs As String, _
                                 ByVal intStatusUnkId As Integer, _
                                 Optional ByVal xDataOp As clsDataOperation = Nothing _
                                 ) As Boolean


        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        If strUnkIDs.Trim = "" Then Return True

        If xDataOp IsNot Nothing Then
            objDataOperation = xDataOp
        Else
            objDataOperation = New clsDataOperation
            objDataOperation.BindTransaction()
        End If
        objDataOperation.ClearParameters()

        Try
            For Each strID As String In strUnkIDs.Split(",")
                mDataOp = objDataOperation
                _Departmentaltrainingneedunkid = CInt(strID)
                mintUserunkid = pintUserunkid
                mintLoginemployeeunkid = pintLoginemployeeunkid
                mintStatusunkid = intStatusUnkId
                'Sohail (04 Aug 2021) -- Start
                'NMB Enhancement : OLD - 429 : Move Submit for Approval button from current location to the Add/Edit Departmental Training Needs Screen.
                mstrUnlock_remark = pstrUnlock_remark
                mstrFinalapproval_remark = pstrFinalapproval_remark
                'Sohail (04 Aug 2021) -- End
                If pstrRefno.Length > 0 AndAlso mstrRefno.Trim = "" Then 'For blank set one space
                    mstrRefno = pstrRefno
                End If


                mintAuditUserId = pintAuditUserId
                mdtAuditDate = pdtAuditDate
                mstrClientIp = pstrClientIp
                mstrHostName = pstrHostName
                mstrFormName = pstrFormName

                objDataOperation.ClearParameters()

                strQ = "UPDATE trdepartmentaltrainingneed_master SET " & _
                  "  statusunkid = @statusunkid " & _
                  ", refno = @refno " & _
                  ", userunkid = @userunkid " & _
                  ", loginemployeeunkid = @loginemployeeunkid "

                'Sohail (04 Aug 2021) -- Start
                'NMB Enhancement : OLD - 429 : Move Submit for Approval button from current location to the Add/Edit Departmental Training Needs Screen.
                If mstrUnlock_remark.Trim.Length > 0 Then
                    strQ &= ", unlock_remark = @unlock_remark "
                    objDataOperation.AddParameter("@unlock_remark", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrUnlock_remark.ToString.Trim)
                End If

                If mstrFinalapproval_remark.Trim.Length > 0 Then
                    strQ &= ", finalapproval_remark = @finalapproval_remark "
                    objDataOperation.AddParameter("@finalapproval_remark", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrFinalapproval_remark.ToString.Trim)
                End If
                'Sohail (04 Aug 2021) -- End

                strQ &= "WHERE departmentaltrainingneedunkid = @departmentaltrainingneedunkid "

                objDataOperation.AddParameter("@departmentaltrainingneedunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, CInt(strID))
                'objDataOperation.AddParameter("@isactive", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsactive)
                objDataOperation.AddParameter("@userunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintUserunkid)
                objDataOperation.AddParameter("@loginemployeeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintLoginemployeeunkid)
                objDataOperation.AddParameter("@statusunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intStatusUnkId)
                objDataOperation.AddParameter("@refno", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrRefno.ToString.Trim)

                Call objDataOperation.ExecNonQuery(strQ)

                If objDataOperation.ErrorMessage <> "" Then
                    exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                    Throw exForce
                End If

                If Insert_AtTranLog(objDataOperation, 2) = False Then
                    If xDataOp Is Nothing Then objDataOperation.ReleaseTransaction(False)
                    Return False
                End If
            Next


            If xDataOp Is Nothing Then objDataOperation.ReleaseTransaction(True)

            Return True
        Catch ex As Exception
            If xDataOp Is Nothing Then objDataOperation.ReleaseTransaction(False)
            Throw New Exception(ex.Message & "; Procedure Name: UpdateStatus; Module Name: " & mstrModuleName)
            Return False
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            If xDataOp Is Nothing Then objDataOperation = Nothing
        End Try
    End Function

    ''' <summary>
    ''' Modify By: Suhail
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>
    Public Function isUsed(ByVal intUnkid As Integer) As Boolean
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        objDataOperation = New clsDataOperation

        Try
            strQ = "<Query>"

            objDataOperation.AddParameter("@departmentaltrainingneedunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intUnkid)

            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            Return dsList.Tables(0).Rows.Count > 0
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: isUsed; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
    End Function

    ''' <summary>
    ''' Modify By: Suhail
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>
    Public Function isExist(ByVal intDepartmentunkid As Integer _
                            , ByVal intPeriodUnkId As Integer _
                            , ByVal intTrainingCourseUnkId As Integer _
                            , ByVal intCompetenceUnkId As Integer _
                            , ByVal intTargetedGroupUnkId As Integer _
                            , ByVal strTargetedGroupTranIDs As String _
                            , Optional ByVal intUnkid As Integer = -1 _
                            , Optional ByVal xDataOpr As clsDataOperation = Nothing _
                            ) As Integer

        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim intRetUnkId As Integer = 0
        Dim exForce As Exception

        If xDataOpr Is Nothing Then
            objDataOperation = New clsDataOperation
        Else
            objDataOperation = xDataOpr
        End If
        objDataOperation.ClearParameters()

        Try
            strQ = "SELECT " & _
              "  departmentaltrainingneedunkid " & _
             "FROM trdepartmentaltrainingneed_master "

            If strTargetedGroupTranIDs.Trim <> "" Then
                strQ &= "LEFT JOIN trdepttrainingneed_employee_tran ON trdepartmentaltrainingneed_master.departmentaltrainingneedunkid = trdepttrainingneed_employee_tran.departmentaltrainingneedunkid " & _
                               "AND trdepartmentaltrainingneed_master.targetedgroupunkid <= 0 " & _
                               "AND trdepttrainingneed_employee_tran.isvoid = 0 " & _
                        "LEFT JOIN trdepttrainingneed_allocation_tran ON trdepartmentaltrainingneed_master.departmentaltrainingneedunkid = trdepttrainingneed_allocation_tran.departmentaltrainingneedunkid " & _
                               "AND trdepartmentaltrainingneed_master.targetedgroupunkid > 0 " & _
                               "AND trdepttrainingneed_allocation_tran.isvoid = 0 "
            End If

            strQ &= "WHERE isvoid = 0 "

            If intDepartmentunkid > 0 OrElse intDepartmentunkid < -1 Then
                strQ &= " AND trdepartmentaltrainingneed_master.departmentunkid = @departmentunkid "
                objDataOperation.AddParameter("@departmentunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intDepartmentunkid)
            End If

            If intPeriodUnkId > 0 Then
                strQ &= " AND trdepartmentaltrainingneed_master.periodunkid = @periodunkid "
                objDataOperation.AddParameter("@periodunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intPeriodUnkId)
            End If

            If intTrainingCourseUnkId > 0 Then
                strQ &= " AND trdepartmentaltrainingneed_master.trainingcourseunkid = @trainingcourseunkid "
                objDataOperation.AddParameter("@trainingcourseunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intTrainingCourseUnkId)
            End If

            If intCompetenceUnkId > 0 Then
                strQ &= " AND trdepartmentaltrainingneed_master.competenceunkid = @competenceunkid "
                objDataOperation.AddParameter("@competenceunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intCompetenceUnkId)
            End If

            If intTargetedGroupUnkId > 0 Then
                strQ &= " AND trdepartmentaltrainingneed_master.targetedgroupunkid = @targetedgroupunkid "
                objDataOperation.AddParameter("@targetedgroupunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intTargetedGroupUnkId)
            End If

            If strTargetedGroupTranIDs.Trim <> "" Then
                strQ &= " AND ( (trdepartmentaltrainingneed_master.targetedgroupunkid <= 0 AND trdepttrainingneed_employee_tran.employeeunkid IN (" & strTargetedGroupTranIDs & ") ) " & _
                                " OR " & _
                              " (trdepartmentaltrainingneed_master.targetedgroupunkid > 0 AND trdepttrainingneed_allocation_tran.allocationtranunkid IN (" & strTargetedGroupTranIDs & ") ) " & _
                            " ) "
            End If

            If intUnkid > 0 Then
                strQ &= " AND trdepartmentaltrainingneed_master.departmentaltrainingneedunkid <> @departmentaltrainingneedunkid "
                objDataOperation.AddParameter("@departmentaltrainingneedunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intUnkid)
            End If


            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            If dsList.Tables(0).Rows.Count > 0 Then
                intRetUnkId = CInt(dsList.Tables(0).Rows(0).Item("departmentaltrainingneedunkid"))
            End If

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: isExist; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            If xDataOpr Is Nothing Then objDataOperation = Nothing
        End Try
        Return intRetUnkId
    End Function

    Public Function GetIncludeColumnsList(ByVal strTableName As String, Optional ByVal blnAddSelect As Boolean = False, Optional ByVal strIDs As String = "", Optional ByVal strNotInIDs As String = "") As DataSet
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        objDataOperation = New clsDataOperation

        Try
            strQ = "SELECT Id, Name FROM ( "

            If blnAddSelect = True Then
                strQ &= "SELECT 0 AS Id, @Select AS Name UNION "
            End If

            strQ &= "SELECT " & enIncludeColumnList.LearningMethod & " AS Id, @LearningMethod AS Name  " & _
                    "UNION SELECT " & enIncludeColumnList.TrainingProvider & " AS Id, @TrainingProvider AS Name " & _
                    "UNION SELECT " & enIncludeColumnList.TrainingVenue & " AS Id, @TrainingVenue AS Name " & _
                    "UNION SELECT " & enIncludeColumnList.TrainingResourcesNeeded & " AS Id, @TrainingResourcesNeeded AS Name " & _
                    "UNION SELECT " & enIncludeColumnList.FinancingSource & " AS Id, @FinancingSource AS Name " & _
                    "UNION SELECT " & enIncludeColumnList.TrainingCoordinator & " AS Id, @TrainingCoordinator AS Name " & _
                    "UNION SELECT " & enIncludeColumnList.TrainingCertificateRequired & " AS Id, @TrainingCertificateRequired AS Name " & _
                    "UNION SELECT " & enIncludeColumnList.CommentRemark & " AS Id, @CommentRemark AS Name " & _
                    "UNION SELECT " & enIncludeColumnList.TrainingCostItem & " AS Id, @TrainingCostItem AS Name " & _
                    "UNION SELECT " & enIncludeColumnList.TrainingCostAmount & " AS Id, @TrainingCostAmount AS Name "

            strQ &= " ) AS A " & _
                    "WHERE 1 = 1 "

            If strIDs.Trim <> "" Then
                strQ &= " AND A.Id IN (" & strIDs & ") "
            End If

            If strNotInIDs.Trim <> "" Then
                strQ &= " AND A.Id NOT IN (" & strNotInIDs & ") "
            End If

            objDataOperation.AddParameter("@LearningMethod", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 1, "Learning Method"))
            objDataOperation.AddParameter("@TrainingProvider", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 2, "Training Provider"))
            objDataOperation.AddParameter("@TrainingVenue", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 3, "Training Venue"))
            objDataOperation.AddParameter("@TrainingResourcesNeeded", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 4, "Training Resources Needed"))
            objDataOperation.AddParameter("@FinancingSource", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 5, "Financing Source"))
            objDataOperation.AddParameter("@TrainingCoordinator", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 6, "Training Coordinator"))
            objDataOperation.AddParameter("@TrainingCertificateRequired", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 7, "Training Certificate Required"))
            objDataOperation.AddParameter("@CommentRemark", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 8, "Comment Remark"))
            objDataOperation.AddParameter("@TrainingCostItem", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 9, "Training Cost Item"))
            objDataOperation.AddParameter("@TrainingCostAmount", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 10, "Amount"))


            dsList = objDataOperation.ExecQuery(strQ, strTableName)

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: GetIncludeColumnsList; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            'objDataOperation = Nothing
        End Try
        Return dsList
    End Function

    Public Function getStatusComboList(ByVal strTableName As String, Optional ByVal blnAddSelect As Boolean = False, Optional ByVal strIDs As String = "", Optional ByVal strNotInIDs As String = "") As DataSet
        'Sohail (26 Apr 2021) - [strIDs, strNotInIDs]
        Dim dsList As New DataSet
        Dim StrQ As String = String.Empty
        objDataOperation = New clsDataOperation
        Try
            'Sohail (26 Apr 2021) -- Start
            'NMB Enhancement : : Changes in Departmental Training Need as per comments in online doc in New UI.
            StrQ = "SELECT Id, Name FROM ( "
            'Sohail (26 Apr 2021) -- End

            If blnAddSelect Then
                StrQ &= "SELECT -1 AS Id, ' ' + @Select AS Name UNION "
                objDataOperation.AddParameter("@Select", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 11, "Select"))
            End If

            StrQ &= "SELECT '" & enApprovalStatus.Pending & "' AS Id, @Pending AS Name UNION " & _
                    "SELECT '" & enApprovalStatus.SubmittedForApprovalFromDeptTrainingNeed & "' AS Id, @SubmittedForApprovalFromDeptTrainingNeed AS Name UNION " & _
                    "/*SELECT '" & enApprovalStatus.TentativeApproved & "' AS Id, @TentativeApproved AS Name UNION*/ " & _
                    "SELECT '" & enApprovalStatus.SubmittedForApprovalFromTrainingBacklog & "' AS Id, @SubmittedForApprovalFromTrainingBacklog AS Name UNION " & _
                    "SELECT '" & enApprovalStatus.AskedForReviewAsPerAmountSet & "' AS Id, @AskedForReviewAsPerAmountSet AS Name UNION " & _
                    "SELECT '" & enApprovalStatus.FinalApproved & "' AS Id, @FinalApproved AS Name UNION " & _
                    "SELECT '" & enApprovalStatus.Rejected & "' AS Id, @Reject AS Name "

            'Sohail (26 Apr 2021) -- Start
            'NMB Enhancement : : Changes in Departmental Training Need as per comments in online doc in New UI.
            StrQ &= " ) AS A " & _
                               "WHERE 1 = 1 "

            If strIDs.Trim <> "" Then
                StrQ &= " AND A.Id IN (" & strIDs & ") "
            End If

            If strNotInIDs.Trim <> "" Then
                StrQ &= " AND A.Id NOT IN (" & strNotInIDs & ") "
            End If
            'Sohail (26 Apr 2021) -- End

            objDataOperation.AddParameter("@Pending", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 12, "Pending"))
            objDataOperation.AddParameter("@SubmittedForApprovalFromDeptTrainingNeed", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 13, "Submitted For Approval From Departmental Training Need"))
            objDataOperation.AddParameter("@TentativeApproved", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 14, "Tentative Approved"))
            objDataOperation.AddParameter("@SubmittedForApprovalFromTrainingBacklog", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 15, "Submitted For Approval From Training Backlog"))
            objDataOperation.AddParameter("@FinalApproved", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 16, "Approved"))
            objDataOperation.AddParameter("@Reject", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 17, "Rejected"))
            objDataOperation.AddParameter("@AskedForReviewAsPerAmountSet", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 18, "Asked For Review As Per Amount Set"))


            dsList = objDataOperation.ExecQuery(StrQ, strTableName)

            If objDataOperation.ErrorMessage <> "" Then
                Throw New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
            End If

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: getStatusComboList; Module Name: " & mstrModuleName)
        Finally
        End Try
        Return dsList
    End Function

    Public Function getTrainingRequestStatusComboList(ByVal strTableName As String, Optional ByVal blnAddSelect As Boolean = False) As DataSet
        Dim dsList As New DataSet
        Dim StrQ As String = String.Empty
        objDataOperation = New clsDataOperation
        Try
            If blnAddSelect Then
                StrQ = "SELECT 0 AS Id, @Select AS Name UNION "
                objDataOperation.AddParameter("@Select", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 21, "Select"))
            End If

            StrQ &= "SELECT '" & enTrainingRequestStatus.On_Hold & "' AS Id, @On_Hold AS Name UNION " & _
                    "SELECT '" & enTrainingRequestStatus.Postponed & "' AS Id, @Postponed AS Name UNION " & _
                    "SELECT '" & enTrainingRequestStatus.Cancelled & "' AS Id, @Cancelled AS Name "

            objDataOperation.AddParameter("@On_Hold", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 22, "On Hold"))
            objDataOperation.AddParameter("@Postponed", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 23, "Postponed"))
            objDataOperation.AddParameter("@Cancelled", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 24, "Cancelled"))


            dsList = objDataOperation.ExecQuery(StrQ, strTableName)

            If objDataOperation.ErrorMessage <> "" Then
                Throw New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
            End If

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: getTrainingRequestStatusComboList; Module Name: " & mstrModuleName)
        Finally
        End Try
        Return dsList
    End Function

    Public Function Insert_AtTranLog(ByVal objDoOps As clsDataOperation, ByVal AuditType As Integer)
        Dim StrQ As String = String.Empty
        Dim extForce As Exception
        Dim dsList As New DataSet

        Try
            StrQ = "INSERT INTO attrdepartmentaltrainingneed_master ( " & _
              "  departmentaltrainingneedunkid " & _
              ", departmentunkid " & _
              ", periodunkid " & _
              ", competenceunkid " & _
              ", trainingcategoryunkid " & _
              ", trainingcourseunkid " & _
              ", learningmethodunkid " & _
              ", targetedgroupunkid " & _
              ", noofstaff " & _
              ", startdate " & _
              ", enddate " & _
              ", trainingpriority " & _
              ", trainingproviderunkid " & _
              ", trainingvenueunkid " & _
              ", other_competence " & _
              ", other_trainingcourse " & _
              ", statusunkid " & _
              ", issubmitforapproval " & _
              ", remark " & _
              ", iscertirequired " & _
              ", totalcost " & _
              ", isactive " & _
              ", audituserunkid " & _
              ", loginemployeeunkid " & _
              ", audittypeid " & _
              ", auditdatetime " & _
              ", isweb " & _
              ", ip " & _
              ", host " & _
              ", form_name " & _
              ", allocationid " & _
              ", moduleid " & _
              ", moduletranunkid " & _
              ", istrainingcostoptional " & _
              ", insertformid " & _
              ", refno " & _
              ", approved_totalcost " & _
              ", request_statusunkid " & _
              ", unlock_remark " & _
              ", finalapproval_remark " & _
              ", createuserunkid " & _
            ") VALUES (" & _
              "  @departmentaltrainingneedunkid " & _
              ", @departmentunkid " & _
              ", @periodunkid " & _
              ", @competenceunkid " & _
              ", @trainingcategoryunkid " & _
              ", @trainingcourseunkid " & _
              ", @learningmethodunkid " & _
              ", @targetedgroupunkid " & _
              ", @noofstaff " & _
              ", @startdate " & _
              ", @enddate " & _
              ", @trainingpriority " & _
              ", @trainingproviderunkid " & _
              ", @trainingvenueunkid " & _
              ", @other_competence " & _
              ", @other_trainingcourse " & _
              ", @statusunkid " & _
              ", @issubmitforapproval " & _
              ", @remark " & _
              ", @iscertirequired " & _
              ", @totalcost " & _
              ", @isactive " & _
              ", @audituserunkid " & _
              ", @loginemployeeunkid " & _
              ", @audittypeid " & _
              ", @auditdatetime " & _
              ", @isweb " & _
              ", @ip " & _
              ", @host " & _
              ", @form_name " & _
              ", @allocationid " & _
              ", @moduleid " & _
              ", @moduletranunkid " & _
              ", @istrainingcostoptional " & _
              ", @insertformid " & _
              ", @refno " & _
              ", @approved_totalcost " & _
              ", @request_statusunkid " & _
              ", @unlock_remark " & _
              ", @finalapproval_remark " & _
              ", @createuserunkid " & _
            ")"
            'Sohail (17 Aug 2021) - [createuserunkid]
            'Sohail (04 Aug 2021) - [unlock_remark, finalapproval_remark]
            'Sohail (15 Apr 2021) - [refno, approved_totalcost, request_statusunkid]
            'Hemant (15 Apr 2021) -- [insertformid]
            'Sohail (12 Apr 2021) - [moduleid. moduletranunkid]

            objDoOps.ClearParameters()
            objDoOps.AddParameter("@departmentaltrainingneedunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintDepartmentaltrainingneedunkid.ToString)
            objDoOps.AddParameter("@departmentunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintDepartmentunkid.ToString)
            objDoOps.AddParameter("@periodunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintPeriodunkid.ToString)
            objDoOps.AddParameter("@competenceunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintCompetenceunkid.ToString)
            objDoOps.AddParameter("@trainingcategoryunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintTrainingcategoryunkid.ToString)
            objDoOps.AddParameter("@trainingcourseunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintTrainingcourseunkid.ToString)
            objDoOps.AddParameter("@learningmethodunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintLearningmethodunkid.ToString)
            objDoOps.AddParameter("@targetedgroupunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintTargetedgroupunkid.ToString)
            objDoOps.AddParameter("@noofstaff", SqlDbType.Int, eZeeDataType.INT_SIZE, mintNoofstaff.ToString)

            If mdtStartdate = Nothing Then
                objDoOps.AddParameter("@startdate", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, DBNull.Value)
            Else
                objDoOps.AddParameter("@startdate", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, mdtStartdate.ToString())
            End If

            If mdtEnddate = Nothing Then
                objDoOps.AddParameter("@enddate", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, DBNull.Value)
            Else
                objDoOps.AddParameter("@enddate", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, mdtEnddate.ToString)
            End If

            objDoOps.AddParameter("@trainingpriority", SqlDbType.Int, eZeeDataType.INT_SIZE, mintTrainingpriority.ToString)
            objDoOps.AddParameter("@trainingproviderunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintTrainingproviderunkid.ToString)
            objDoOps.AddParameter("@trainingvenueunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintTrainingvenueunkid.ToString)
            objDoOps.AddParameter("@other_competence", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrOther_competence.ToString)
            objDoOps.AddParameter("@other_trainingcourse", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrOther_trainingcourse.ToString)
            objDoOps.AddParameter("@statusunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintStatusunkid.ToString)
            objDoOps.AddParameter("@issubmitforapproval", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIssubmitforapproval.ToString)
            objDoOps.AddParameter("@remark", SqlDbType.NVarChar, eZeeDataType.DESC_SIZE, mstrRemark.ToString)
            objDoOps.AddParameter("@iscertirequired", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIscertirequired.ToString)
            objDoOps.AddParameter("@totalcost", SqlDbType.Decimal, eZeeDataType.DECIMAL_SIZE, mdecTotalcost.ToString)
            objDoOps.AddParameter("@isactive", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsactive.ToString)

            objDoOps.AddParameter("@audituserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintAuditUserId)
            objDoOps.AddParameter("@loginemployeeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintLoginemployeeunkid)
            objDoOps.AddParameter("@audittypeid", SqlDbType.Int, eZeeDataType.INT_SIZE, AuditType)
            objDoOps.AddParameter("@auditdatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, mdtAuditDate)
            objDoOps.AddParameter("@isweb", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsweb)
            objDoOps.AddParameter("@ip", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrClientIp)
            objDoOps.AddParameter("@host", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrHostName)
            objDoOps.AddParameter("@form_name", SqlDbType.NVarChar, eZeeDataType.DESC_SIZE, mstrFormName)
            objDoOps.AddParameter("@allocationid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintAllocationId.ToString)
            'Sohail (12 Apr 2021) -- Start
            'NMB Enhancement : : Changes in Departmental Training Need as per comments in online doc in New UI.
            objDoOps.AddParameter("@moduleid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintModuleId.ToString)
            objDoOps.AddParameter("@moduletranunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintModuleTranUnkId.ToString)
            objDoOps.AddParameter("@istrainingcostoptional", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIstrainingcostoptional.ToString)
            'Sohail (12 Apr 2021) -- End
            'Hemant (15 Apr 2021) -- Start
            'NMB Enhancement : : Changes in Departmental Training Need as per comments in online doc in New UI.
            objDoOps.AddParameter("@insertformid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintInsertFormId.ToString)
            'Hemant (15 Apr 2021) -- End
            objDoOps.AddParameter("@refno", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrRefno.ToString)
            objDoOps.AddParameter("@approved_totalcost", SqlDbType.Decimal, eZeeDataType.DECIMAL_SIZE, mdecApproved_Totalcost.ToString)
            objDoOps.AddParameter("@request_statusunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintRequest_Statusunkid.ToString)
            'Sohail (04 Aug 2021) -- Start
            'NMB Enhancement : OLD - 429 : Move Submit for Approval button from current location to the Add/Edit Departmental Training Needs Screen.
            objDoOps.AddParameter("@unlock_remark", SqlDbType.NVarChar, eZeeDataType.DESC_SIZE, mstrUnlock_remark.ToString)
            objDoOps.AddParameter("@finalapproval_remark", SqlDbType.NVarChar, eZeeDataType.DESC_SIZE, mstrFinalapproval_remark.ToString)
            'Sohail (04 Aug 2021) -- End
            objDoOps.AddParameter("@createuserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintCreateuserunkid.ToString) 'Sohail (17 Aug 2021)

            dsList = objDoOps.ExecQuery(StrQ, "List")

            If objDoOps.ErrorMessage <> "" Then
                extForce = New Exception(objDoOps.ErrorNumber & ": " & objDoOps.ErrorMessage)
                Throw extForce
            End If


        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: Insert_AtTranLog; Module Name: " & mstrModuleName)
        End Try
        Return True

    End Function

    Public Function IsTableDataUpdate(ByVal unkid As Integer, Optional ByVal objDooperation As clsDataOperation = Nothing) As Boolean
        Dim strQ As String = ""
        Dim dsList As New DataSet
        Dim exForce As Exception
        Try
            If objDooperation Is Nothing Then
                objDataOperation = New clsDataOperation
            Else
                objDataOperation = objDooperation
            End If

            strQ = "SELECT TOP 1 * FROM attrdepartmentaltrainingneed_master WHERE departmentaltrainingneedunkid = @departmentaltrainingneedunkid AND audittypeid <> 3 ORDER BY auditdatetime DESC"

            objDataOperation.ClearParameters()
            objDataOperation.AddParameter("@departmentaltrainingneedunkid", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, unkid)

            dsList = objDataOperation.ExecQuery(strQ, "List")

            For Each dr As DataRow In dsList.Tables(0).Rows

                If dr("departmentunkid").ToString() = mintDepartmentunkid AndAlso dr("periodunkid").ToString() = mintPeriodunkid AndAlso dr("competenceunkid").ToString() = mintCompetenceunkid _
                    AndAlso dr("trainingcategoryunkid").ToString() = mintTrainingcategoryunkid AndAlso dr("trainingcourseunkid").ToString() = mintTrainingcourseunkid _
                    AndAlso dr("learningmethodunkid").ToString() = mintLearningmethodunkid AndAlso dr("targetedgroupunkid").ToString() = mintTargetedgroupunkid _
                    AndAlso dr("noofstaff").ToString() = mintNoofstaff AndAlso dr("trainingpriority").ToString() = mintTrainingpriority _
                    AndAlso If(IsDBNull(dr("startdate")) = False, CDate(dr("startdate")), Nothing) = mdtStartdate AndAlso If(IsDBNull(dr("enddate")) = False, CDate(dr("enddate")), Nothing) = mdtEnddate _
                    AndAlso dr("trainingproviderunkid").ToString() = mintTrainingproviderunkid AndAlso dr("trainingvenueunkid").ToString() = mintTrainingvenueunkid _
                    AndAlso dr("other_competence").ToString() = mstrOther_competence AndAlso dr("other_trainingcourse").ToString() = mstrOther_trainingcourse AndAlso dr("statusunkid").ToString() = mintStatusunkid AndAlso CBool(dr("issubmitforapproval")) = mblnIssubmitforapproval _
                    AndAlso dr("remark").ToString() = mstrRemark AndAlso CBool(dr("iscertirequired")) = mblnIscertirequired _
                    AndAlso CDec(dr("totalcost")) = mdecTotalcost AndAlso CBool(dr("isactive")) = mblnIsactive AndAlso dr("allocationid").ToString() = mintAllocationId AndAlso dr("moduleid").ToString() = mintModuleId AndAlso dr("moduletranunkid").ToString() = mintModuleTranUnkId AndAlso CBool(dr("istrainingcostoptional")) = mblnIstrainingcostoptional AndAlso dr("refno").ToString() = mstrRefno _
                    AndAlso CDec(dr("approved_totalcost")) = mdecApproved_Totalcost AndAlso CInt(dr("request_statusunkid")) = mintRequest_Statusunkid AndAlso dr("unlock_remark").ToString() = mstrUnlock_remark AndAlso dr("finalapproval_remark").ToString() = mstrFinalapproval_remark AndAlso CInt(dr("createuserunkid")) = mintCreateuserunkid _
                    Then
                    'Sohail (17 Aug 2021) - [createuserunkid]
                    'Sohail (04 Aug 2021) - [unlock_remark, finalapproval_remark]
                    'Sohail (12 Apr 2021) - [moduleid. moduletranunkid, istrainingcostoptional, refno, request_statusunkid]

                    Return True
                Else
                    Return False
                End If
            Next
            Return False

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: IsTableDataUpdate; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If objDooperation Is Nothing Then objDataOperation = Nothing
        End Try
    End Function

    'Hemant (26 Mar 2021) -- Start
    'NMB Enhancement : : Changes in Departmental Training Need as per comments in online doc in New UI.
    Public Sub SendEmails(ByVal xDatabaseName As String, _
                            ByVal xUserUnkid As Integer, _
                            ByVal xYearUnkid As Integer, _
                            ByVal xCompanyUnkid As Integer, _
                            ByVal xPeriodStart As DateTime, _
                            ByVal xPeriodEnd As DateTime, _
                            ByVal xUserModeSetting As String, _
                            ByVal xOnlyApproved As Boolean, _
                            ByVal xIncludeIn_ActiveEmployee As Boolean, _
                            ByVal strTableName As String, _
                            ByVal xEmailTypeId As Integer, _
                            ByVal strDepartTrainingNeedIDs As String, _
                            ByVal strFormName As String, _
                            ByVal eMode As enLogin_Mode, _
                            ByVal strSenderAddress As String, _
                            ByVal strArutiSelfServiceURL As String, _
                            ByVal strAllocationName As String, _
                            ByVal intAllocatinID As Integer _
                            )
        Try
            Dim objUsr As New clsUserAddEdit : Dim StrMessage As New System.Text.StringBuilder
            Dim dsUserList As New DataSet
            Dim objEmp As New clsEmployee_Master
            Dim objReportTo As New clsReportingToEmployee
            Dim strSubject As String = ""
            Dim strToEmail As String = ""
            Dim strTrainingName As String = ""
            Dim strDepartment As String = ""
            Dim strTargetedGroupName As String = ""
            Dim blnSendReportingTo As Boolean = False
            Dim dsDeptTrainingList As DataSet
            Dim strLink As String = String.Empty
            Dim info1 As TextInfo = CultureInfo.InvariantCulture.TextInfo
            Dim objTEmp As New clsDepttrainingneed_employee_Tran 'Sohail (04 Aug 2021)

            'Sohail (27 Apr 2021) -- Start
            'NMB Enhancement : : Changes in Departmental Training Need as per comments in online doc in New UI.
            Dim intPeriodId As Integer = (New clsMasterData).getCurrentPeriodID(enModuleReference.Payroll, DateAndTime.Today, xYearUnkid, 0, , False, objDataOperation, , True)
            If intPeriodId < 0 Then
                intPeriodId = (New clsMasterData).getFirstPeriodID(enModuleReference.Payroll, xYearUnkid, enStatusType.OPEN)
            End If
            If intPeriodId < 0 Then
                intPeriodId = 1
            End If
            'Sohail (27 Apr 2021) -- End

            dsDeptTrainingList = GetList(xDatabaseName, xUserUnkid, xYearUnkid, xCompanyUnkid, xPeriodStart, xPeriodEnd, xUserModeSetting, xOnlyApproved, _
                                            xIncludeIn_ActiveEmployee, intAllocatinID, "List", True, " AND trdepartmentaltrainingneed_master.departmentaltrainingneedunkid IN ( " & strDepartTrainingNeedIDs & ")")
            Dim lstrDepartTrainingNeedIDs As String() = strDepartTrainingNeedIDs.Split(",")
            For Each intDepartTrainingNeedID As String In lstrDepartTrainingNeedIDs
                Dim drDeptTrainingList() As DataRow = dsDeptTrainingList.Tables(0).Select("departmentaltrainingneedunkid = " & CInt(intDepartTrainingNeedID))
                'Dim strtargetedgroupnamelist As String = String.Join(",", dsDeptTrainingList.Tables(0).Select("departmentaltrainingneedunkid = " & CInt(intDepartTrainingNeedID)).AsEnumerable().Select(Function(x) x.Field(Of String)("allocationtranname").ToString()).ToArray())

                If drDeptTrainingList.Count > 0 Then
                    strTargetedGroupName = drDeptTrainingList(0).Item("targetedgroupname")

                    Select Case xEmailTypeId
                        Case enEmailType.Submit_For_Approval_From_Departmental_Training_Need
                            dsUserList = objUsr.Get_UserBy_PrivilegeId(enUserPriviledge.AllowToTentativeApproveForDepartmentalTrainingNeed, xYearUnkid)
                            strSubject = Language.getMessage(mstrModuleName, 20, "Notification: Departmental Training Needs Approval/Rejection")
                            For Each drUser As DataRow In dsUserList.Tables(0).Rows
                                If drUser("Uemail").ToString().Trim().Length <= 0 Then Continue For

                                'Sohail (27 Apr 2021) -- Start
                                'NMB Enhancement : : Changes in Departmental Training Need as per comments in online doc in New UI.
                                Dim dsEmp As DataSet = objEmp.Get_AllocationBasedEmployee(CInt(drDeptTrainingList(0).Item("allocationid")), CInt(drDeptTrainingList(0).Item("departmentunkid")), intPeriodId, xDatabaseName, xUserModeSetting, xCompanyUnkid, CInt(drUser("UId")), True, objDataOperation)
                                If dsEmp.Tables(0).Rows.Count <= 0 Then Continue For
                                'Sohail (27 Apr 2021) -- End

                                StrMessage = New System.Text.StringBuilder
                                StrMessage.Append("<HTML><BODY>")
                                StrMessage.Append(vbCrLf)

                                StrMessage.Append("<p><span style='font-size:9.0pt; font-family:" & Chr(34) & "Verdana" & Chr(34) & "," & Chr(34) & "Sans-Serif" & Chr(34) & "; color:#333399; '>")

                                StrMessage.Append(Language.getMessage(mstrModuleName, 25, "Dear") & " " & "<b>" & getTitleCase(drUser.Item("Uname").ToString()) & "</b></span></p>")


                                StrMessage.Append(Language.getMessage(mstrModuleName, 26, "This is to notify you that the Training Needs for #AllocationName# are ready for your approval/rejection.") & " </span></p> ")

                                StrMessage.Append(vbCrLf)

                                StrMessage.Append("<p>" & Language.getMessage(mstrModuleName, 27, "Please login to Aruti or click on the link below to approve/reject.") & " </p> ")

                                StrMessage.Append(vbCrLf)

                                strLink = strArutiSelfServiceURL & "/Training/Departmental_Training_Needs/wPg_DepartmentalTrainingNeeds.aspx?id=2&c=" & HttpUtility.UrlEncode(clsCrypto.Encrypt(xCompanyUnkid.ToString & "|" & CInt(drUser("UId")).ToString & ""))


                                StrMessage.Append("<BR></BR><BR></BR>" & Language.getMessage(mstrModuleName, 28, "Please click on the following link to approve/reject Department Training Need.") & "<BR></BR><a href='" & strLink & "'>" & strLink & "</a>")

                                StrMessage.Append("<p><span style='font-size:9.0pt; font-family:" & Chr(34) & "Verdana" & Chr(34) & "," & Chr(34) & "Sans-Serif" & Chr(34) & "; color:#333399; '>")

                                StrMessage.Append("<p><center style='padding-top:25px;font-size:10.0pt;font-family:&quot;Verdana&quot;,&quot;Sans-Serif&quot;;color:#333399;margin-left:0px;margin-right:0px;margin-top:0px;margin-bottom:10px'><b>&quot;POWERED BY ARUTI HR &amp; PAYROLL MANAGEMENT SOFTWARE.&quot;</b></center></p>")

                                StrMessage.Append("</span></p>")

                                StrMessage.Append("</BODY></HTML>")

                                Dim strEmailContent As String
                                strEmailContent = StrMessage.ToString()
                                If CInt(drDeptTrainingList(0).Item("targetedgroupunkid")) = 0 Then
                                    strEmailContent = strEmailContent.Replace("of #TargetedGroupName# #TargetedGroupNameList#", "")
                                End If

                                strEmailContent = strEmailContent.Replace("#AllocationName#", "<B>" & drDeptTrainingList(0).Item("Department") & "</B>")
                                strEmailContent = strEmailContent.Replace("#TrainingName#", "<B>" & drDeptTrainingList(0).Item("trainingcoursename") & "</B>")
                                strEmailContent = strEmailContent.Replace("#NoOfStaff#", "<B>" & drDeptTrainingList(0).Item("NoOfStaff") & "</B>")
                                strEmailContent = strEmailContent.Replace("#Department#", "<B>" & drDeptTrainingList(0).Item("Department") & "</B>")
                                strEmailContent = strEmailContent.Replace("#TargetedGroupName#", strTargetedGroupName)
                                ' strEmailContent = strEmailContent.Replace("#TargetedGroupNameList#", "<B>" & strtargetedgroupnamelist & "</B>")

                                Dim objSendMail As New clsSendMail
                                objSendMail._ToEmail = drUser("Uemail").ToString().Trim()
                                objSendMail._Subject = strSubject
                                objSendMail._Message = strEmailContent
                                objSendMail._Form_Name = strFormName
                                objSendMail._LogEmployeeUnkid = -1
                                objSendMail._OperationModeId = eMode
                                objSendMail._UserUnkid = xUserUnkid
                                objSendMail._SenderAddress = strSenderAddress
                                objSendMail._ModuleRefId = clsSendMail.enAT_VIEW_TYPE.TRAININGREQUISITION_MGT
                                objSendMail.SendMail(xCompanyUnkid)
                                objSendMail = Nothing

                            Next

                        Case enEmailType.Submit_For_Approval_From_Training_Backlog
                            dsUserList = objUsr.Get_UserBy_PrivilegeId(enUserPriviledge.AllowToFinalApproveDepartmentalTrainingNeed, xYearUnkid)
                            strSubject = Language.getMessage(mstrModuleName, 29, "Notification: Backlog Training Needs Approval/Rejection")
                            For Each drUser As DataRow In dsUserList.Tables(0).Rows
                                If drUser("Uemail").ToString().Trim().Length <= 0 Then Continue For

                                'Sohail (27 Apr 2021) -- Start
                                'NMB Enhancement : : Changes in Departmental Training Need as per comments in online doc in New UI.
                                Dim dsEmp As DataSet = objEmp.Get_AllocationBasedEmployee(CInt(drDeptTrainingList(0).Item("allocationid")), CInt(drDeptTrainingList(0).Item("departmentunkid")), intPeriodId, xDatabaseName, xUserModeSetting, xCompanyUnkid, CInt(drUser("UId")), True, objDataOperation)
                                If dsEmp.Tables(0).Rows.Count <= 0 Then Continue For
                                'Sohail (27 Apr 2021) -- End

                                StrMessage = New System.Text.StringBuilder
                                StrMessage.Append("<HTML><BODY>")
                                StrMessage.Append(vbCrLf)

                                StrMessage.Append("<p><span style='font-size:9.0pt; font-family:" & Chr(34) & "Verdana" & Chr(34) & "," & Chr(34) & "Sans-Serif" & Chr(34) & "; color:#333399; '>")

                                StrMessage.Append(Language.getMessage(mstrModuleName, 30, "Dear") & " " & "<b>" & getTitleCase(drUser.Item("Uname").ToString()) & "</b></span></p>")

                                StrMessage.Append(Language.getMessage(mstrModuleName, 31, "This is to notify you that Training backlogs are tentatively approved and ready for your final approval/rejection.") & " </span></p> ")
                                StrMessage.Append(vbCrLf)
                                StrMessage.Append("<p>" & Language.getMessage(mstrModuleName, 32, "Please login to Aruti or click on the link below to approve/reject.") & " </p> ")
                                StrMessage.Append(vbCrLf)

                                strLink = strArutiSelfServiceURL & "/Training/Departmental_Training_Needs/wPg_DepartmentalTrainingNeeds.aspx?id=3&c=" & HttpUtility.UrlEncode(clsCrypto.Encrypt(xCompanyUnkid.ToString & "|" & CInt(drUser("UId")).ToString & ""))

                                StrMessage.Append("<BR></BR><BR></BR>" & Language.getMessage(mstrModuleName, 33, "Please click on the following link to approve/reject Department Training Need.") & "<BR></BR><a href='" & strLink & "'>" & strLink & "</a>")

                                StrMessage.Append("<p><span style='font-size:9.0pt; font-family:" & Chr(34) & "Verdana" & Chr(34) & "," & Chr(34) & "Sans-Serif" & Chr(34) & "; color:#333399; '>")

                                StrMessage.Append("<p><center style='padding-top:25px;font-size:10.0pt;font-family:&quot;Verdana&quot;,&quot;Sans-Serif&quot;;color:#333399;margin-left:0px;margin-right:0px;margin-top:0px;margin-bottom:10px'><b>&quot;POWERED BY ARUTI HR &amp; PAYROLL MANAGEMENT SOFTWARE.&quot;</b></center></p>")

                                StrMessage.Append("</span></p>")

                                StrMessage.Append("</BODY></HTML>")

                                Dim strEmailContent As String
                                strEmailContent = StrMessage.ToString()
                                If CInt(drDeptTrainingList(0).Item("targetedgroupunkid")) = 0 Then
                                    strEmailContent = strEmailContent.Replace("of #TargetedGroupName# #TargetedGroupNameList#", "")
                                End If
                                strEmailContent = strEmailContent.Replace("#TrainingName#", "<B>" & drDeptTrainingList(0).Item("trainingcoursename") & "</B>")
                                strEmailContent = strEmailContent.Replace("#NoOfStaff#", "<B>" & drDeptTrainingList(0).Item("NoOfStaff") & "</B>")
                                strEmailContent = strEmailContent.Replace("#Department#", "<B>" & drDeptTrainingList(0).Item("Department") & "</B>")
                                strEmailContent = strEmailContent.Replace("#TargetedGroupName#", strTargetedGroupName)
                                'strEmailContent = strEmailContent.Replace("#TargetedGroupNameList#", "<B>" & strtargetedgroupnamelist & "</B>")

                                Dim objSendMail As New clsSendMail
                                objSendMail._ToEmail = drUser("Uemail").ToString().Trim()
                                objSendMail._Subject = strSubject
                                objSendMail._Message = strEmailContent
                                objSendMail._Form_Name = strFormName
                                objSendMail._LogEmployeeUnkid = -1
                                objSendMail._OperationModeId = eMode
                                objSendMail._UserUnkid = xUserUnkid
                                objSendMail._SenderAddress = strSenderAddress
                                objSendMail._ModuleRefId = clsSendMail.enAT_VIEW_TYPE.TRAININGREQUISITION_MGT
                                objSendMail.SendMail(xCompanyUnkid)
                                objSendMail = Nothing

                            Next

                            'Sohail (04 Aug 2021) -- Start
                            'NMB Enhancement : OLD - 428 : Email alert when plans are unlocked from the training backlog.
                        Case enEmailType.Unlock_Submitted_For_Approval
                            'Sohail (17 Aug 2021) -- Start
                            'NMB Enhancement : OLD-428 : Send alert to the creator of plan only in departmental training need.
                            'dsUserList = objUsr.Get_UserBy_PrivilegeId(enUserPriviledge.AllowToSubmitForApprovalFromDepartmentalTrainingNeed, xYearUnkid)
                            dsUserList = objUsr.GetList("List", " hrmsConfiguration..cfuser_master.userunkid = " & CInt(drDeptTrainingList(0).Item("createuserunkid")) & " ")
                            'Sohail (17 Aug 2021) -- End
                            strSubject = Language.getMessage(mstrModuleName, 51, "Notification: Submitted Training Needs unlocked")
                            For Each drUser As DataRow In dsUserList.Tables(0).Rows
                                If drUser("email").ToString().Trim().Length <= 0 Then Continue For
                                'Sohail (17 Aug 2021) - [Uemail=email]

                                Dim dsEmp As DataSet = objEmp.Get_AllocationBasedEmployee(CInt(drDeptTrainingList(0).Item("allocationid")), CInt(drDeptTrainingList(0).Item("departmentunkid")), intPeriodId, xDatabaseName, xUserModeSetting, xCompanyUnkid, CInt(drUser("userunkid")), True, objDataOperation)
                                'Sohail (17 Aug 2021) - [UId = userunkid]
                                If dsEmp.Tables(0).Rows.Count <= 0 Then Continue For

                                StrMessage = New System.Text.StringBuilder
                                StrMessage.Append("<HTML><BODY>")
                                StrMessage.Append(vbCrLf)

                                StrMessage.Append("<p><span style='font-size:9.0pt; font-family:" & Chr(34) & "Verdana" & Chr(34) & "," & Chr(34) & "Sans-Serif" & Chr(34) & "; color:#333399; '>")

                                'Sohail (17 Aug 2021) -- Start
                                'NMB Enhancement : OLD-428 : Send alert to the creator of plan only in departmental training need.
                                'StrMessage.Append(Language.getMessage(mstrModuleName, 36, "Dear") & " " & "<b>" & getTitleCase(drUser.Item("Uname").ToString()) & "</b></span></p>")

                                'StrMessage.Append(Language.getMessage(mstrModuleName, 37, "Please be informed that the plan you submitted for #TargetedGroupName# with the program name #TrainingName# has been unlocked for changes as per the remarks below.") & " </span></p> ")
                                StrMessage.Append(Language.getMessage(mstrModuleName, 36, "Dear") & " " & "<b>" & getTitleCase(If(Trim(drUser.Item("firstname").ToString & " " & drUser.Item("lastname").ToString) = "", drUser.Item("username").ToString, drUser.Item("firstname").ToString & " " & drUser.Item("lastname").ToString)) & "</b></span></p>")

                                StrMessage.Append(Language.getMessage(mstrModuleName, 54, "Please be informed that the plan you submitted for approval with the program name #TrainingName# has been unlocked for changes as per the remarks below.") & " </span></p> ")
                                'Sohail (17 Aug 2021) -- End

                                StrMessage.Append(vbCrLf)
                                StrMessage.Append("<p>" & Language.getMessage(mstrModuleName, 38, "Remarks: #UnlockReason#.") & " </p> ")
                                StrMessage.Append(vbCrLf)


                                StrMessage.Append("<p><span style='font-size:9.0pt; font-family:" & Chr(34) & "Verdana" & Chr(34) & "," & Chr(34) & "Sans-Serif" & Chr(34) & "; color:#333399; '>")

                                StrMessage.Append("<p><center style='padding-top:25px;font-size:10.0pt;font-family:&quot;Verdana&quot;,&quot;Sans-Serif&quot;;color:#333399;margin-left:0px;margin-right:0px;margin-top:0px;margin-bottom:10px'><b>&quot;POWERED BY ARUTI HR &amp; PAYROLL MANAGEMENT SOFTWARE.&quot;</b></center></p>")

                                StrMessage.Append("</span></p>")

                                StrMessage.Append("</BODY></HTML>")

                                Dim strEmailContent As String
                                strEmailContent = StrMessage.ToString()
                                strEmailContent = strEmailContent.Replace("#TrainingName#", "<B>" & drDeptTrainingList(0).Item("trainingcoursename") & "</B>")
                                strEmailContent = strEmailContent.Replace("#TargetedGroupName#", "<B>" & strTargetedGroupName & "</B>")
                                strEmailContent = strEmailContent.Replace("#UnlockReason#", "<B>" & drDeptTrainingList(0).Item("unlock_remark") & "</B>")

                                Dim objSendMail As New clsSendMail
                                objSendMail._ToEmail = drUser("email").ToString().Trim()
                                'Sohail (17 Aug 2021) - [Uemail=email]
                                objSendMail._Subject = strSubject
                                objSendMail._Message = strEmailContent
                                objSendMail._Form_Name = strFormName
                                objSendMail._LogEmployeeUnkid = -1
                                objSendMail._OperationModeId = eMode
                                objSendMail._UserUnkid = xUserUnkid
                                objSendMail._SenderAddress = strSenderAddress
                                objSendMail._ModuleRefId = clsSendMail.enAT_VIEW_TYPE.TRAININGREQUISITION_MGT
                                objSendMail.SendMail(xCompanyUnkid)
                                objSendMail = Nothing

                            Next

                        Case enEmailType.Final_Approved
                            Dim arrEmp() As String
                            If CInt(drDeptTrainingList(0).Item("targetedgroupunkid")) <= 0 Then 'Employee Name
                                Dim dtUserList As DataTable = drDeptTrainingList.CopyToDataTable
                                arrEmp = (From p In dtUserList Where (CInt(p.Item("allocationtranunkid")) > 0) Select (p.Item("allocationtranunkid").ToString)).ToArray
                            Else
                                dsUserList = objTEmp.GetList(xDatabaseName, xUserUnkid, xYearUnkid, xCompanyUnkid, xPeriodStart, xPeriodEnd, xUserModeSetting, xOnlyApproved, xIncludeIn_ActiveEmployee, "List", False, "", CInt(drDeptTrainingList(0).Item("departmentaltrainingneedunkid")))
                                arrEmp = (From p In dsUserList.Tables(0) Select (p.Item("employeeunkid").ToString)).ToArray
                            End If

                            strSubject = Language.getMessage(mstrModuleName, 39, "Notification: Training has been planned")
                            For Each id As String In arrEmp
                                objEmp = New clsEmployee_Master
                                objEmp._Employeeunkid(xPeriodEnd, objDataOperation) = CInt(id)
                                If objEmp._Email.Trim().Length <= 0 Then Continue For

                                StrMessage = New System.Text.StringBuilder
                                StrMessage.Append("<HTML><BODY>")
                                StrMessage.Append(vbCrLf)

                                StrMessage.Append("<p><span style='font-size:9.0pt; font-family:" & Chr(34) & "Verdana" & Chr(34) & "," & Chr(34) & "Sans-Serif" & Chr(34) & "; color:#333399; '>")

                                StrMessage.Append(Language.getMessage(mstrModuleName, 40, "Dear") & " " & "<b>" & getTitleCase(objEmp._Firstname & " " & objEmp._Surname) & "</b></span></p>")

                                'Sohail (17 Aug 2021) -- Start
                                'NMB Enhancement : OLD-430 : Use this new message alert (to take care of multiple target groups) to notify employees who are targeted in departmental training needs plans when such plans are final approved.
                                'StrMessage.Append(Language.getMessage(mstrModuleName, 41, "This is to inform you that the following program has been planned for #TargetedGroupName# on #TrainingStartDate#. If you wish to apply, please log in to your self-service.") & " </span></p> ")
                                StrMessage.Append(Language.getMessage(mstrModuleName, 55, "This is to inform you that the following program has been planned to start on #TrainingStartDate#. If you wish to participate, please log in to your self-service to apply.") & " </span></p> ")
                                'Sohail (17 Aug 2021) -- End
                                StrMessage.Append(vbCrLf)
                                StrMessage.Append("<p>")
                                StrMessage.Append(Language.getMessage(mstrModuleName, 42, "Program Name: #TrainingName#.") & " <BR> ")
                                StrMessage.Append(Language.getMessage(mstrModuleName, 43, "Start Date: #TrainingStartDate#.") & " <BR> ")
                                StrMessage.Append(Language.getMessage(mstrModuleName, 44, "End Date: #TrainingEndDate#.") & " <BR> ")
                                StrMessage.Append("<p>")
                                StrMessage.Append(vbCrLf)


                                StrMessage.Append("<p><span style='font-size:9.0pt; font-family:" & Chr(34) & "Verdana" & Chr(34) & "," & Chr(34) & "Sans-Serif" & Chr(34) & "; color:#333399; '>")

                                StrMessage.Append("<p><center style='padding-top:25px;font-size:10.0pt;font-family:&quot;Verdana&quot;,&quot;Sans-Serif&quot;;color:#333399;margin-left:0px;margin-right:0px;margin-top:0px;margin-bottom:10px'><b>&quot;POWERED BY ARUTI HR &amp; PAYROLL MANAGEMENT SOFTWARE.&quot;</b></center></p>")

                                StrMessage.Append("</span></p>")

                                StrMessage.Append("</BODY></HTML>")

                                Dim strEmailContent As String
                                strEmailContent = StrMessage.ToString()
                                strEmailContent = strEmailContent.Replace("#TrainingName#", "<B>" & drDeptTrainingList(0).Item("trainingcoursename") & "</B>")
                                strEmailContent = strEmailContent.Replace("#TargetedGroupName#", "<B>" & strTargetedGroupName & "</B>")
                                strEmailContent = strEmailContent.Replace("#TrainingStartDate#", "<B>" & Format(eZeeDate.convertDate(drDeptTrainingList(0).Item("startdate").ToString), "dd-MMM-yyyy") & "</B>")
                                strEmailContent = strEmailContent.Replace("#TrainingEndDate#", "<B>" & Format(eZeeDate.convertDate(drDeptTrainingList(0).Item("enddate").ToString), "dd-MMM-yyyy") & "</B>")

                                Dim objSendMail As New clsSendMail
                                objSendMail._ToEmail = objEmp._Email.Trim
                                objSendMail._Subject = strSubject
                                objSendMail._Message = strEmailContent
                                objSendMail._Form_Name = strFormName
                                objSendMail._LogEmployeeUnkid = -1
                                objSendMail._OperationModeId = eMode
                                objSendMail._UserUnkid = xUserUnkid
                                objSendMail._SenderAddress = strSenderAddress
                                objSendMail._ModuleRefId = clsSendMail.enAT_VIEW_TYPE.TRAININGREQUISITION_MGT
                                objSendMail.SendMail(xCompanyUnkid)
                                objSendMail = Nothing
                            Next


                            'Sohail (17 Aug 2021) -- Start
                            'NMB Enhancement : OLD-431 : Send alert to the creator of the plan only and change alert message on budget Approval/Rejection in departmental plan.
                            'dsUserList = objUsr.Get_UserBy_PrivilegeId(enUserPriviledge.AllowToAddDepartmentalTrainingNeed, xYearUnkid)
                            dsUserList = objUsr.GetList("List", " hrmsConfiguration..cfuser_master.userunkid = " & CInt(drDeptTrainingList(0).Item("createuserunkid")) & " ")
                            'Sohail (17 Aug 2021) -- End
                            strSubject = Language.getMessage(mstrModuleName, 52, "Notification: Training has been approved")
                            For Each drUser As DataRow In dsUserList.Tables(0).Rows
                                If drUser("email").ToString().Trim().Length <= 0 Then Continue For
                                'Sohail (17 Aug 2021) - [Uemail=email]

                                Dim dsEmp As DataSet = objEmp.Get_AllocationBasedEmployee(CInt(drDeptTrainingList(0).Item("allocationid")), CInt(drDeptTrainingList(0).Item("departmentunkid")), intPeriodId, xDatabaseName, xUserModeSetting, xCompanyUnkid, CInt(drUser("userunkid")), True, objDataOperation)
                                'Sohail (17 Aug 2021) - [UId = userunkid]
                                If dsEmp.Tables(0).Rows.Count <= 0 Then Continue For

                                StrMessage = New System.Text.StringBuilder
                                StrMessage.Append("<HTML><BODY>")
                                StrMessage.Append(vbCrLf)

                                StrMessage.Append("<p><span style='font-size:9.0pt; font-family:" & Chr(34) & "Verdana" & Chr(34) & "," & Chr(34) & "Sans-Serif" & Chr(34) & "; color:#333399; '>")

                                'Sohail (17 Aug 2021) -- Start
                                'NMB Enhancement : OLD-431 : Send alert to the creator of the plan only and change alert message on budget Approval/Rejection in departmental plan.
                                'StrMessage.Append(Language.getMessage(mstrModuleName, 45, "Dear") & " " & "<b>" & getTitleCase(drUser.Item("Uname").ToString()) & "</b></span></p>")

                                'StrMessage.Append(Language.getMessage(mstrModuleName, 46, "Please be informed that the plan you submitted for #TargetedGroupName# with the program name #TrainingName# has been approved.") & " </span></p> ")
                                StrMessage.Append(Language.getMessage(mstrModuleName, 45, "Dear") & " " & "<b>" & getTitleCase(If(Trim(drUser.Item("firstname").ToString & " " & drUser.Item("lastname").ToString) = "", drUser.Item("username").ToString, drUser.Item("firstname").ToString & " " & drUser.Item("lastname").ToString)) & "</b></span></p>")

                                StrMessage.Append(Language.getMessage(mstrModuleName, 56, "Please be informed that the plan you submitted for approval with the program name #TrainingName# has been approved.") & " </span></p> ")
                                'Sohail (17 Aug 2021) -- End

                                StrMessage.Append(vbCrLf)
                                StrMessage.Append("<p>" & Language.getMessage(mstrModuleName, 47, "Remarks: #ApprovalRejectionRemark#.") & " </p> ")
                                StrMessage.Append(vbCrLf)


                                StrMessage.Append("<p><span style='font-size:9.0pt; font-family:" & Chr(34) & "Verdana" & Chr(34) & "," & Chr(34) & "Sans-Serif" & Chr(34) & "; color:#333399; '>")

                                StrMessage.Append("<p><center style='padding-top:25px;font-size:10.0pt;font-family:&quot;Verdana&quot;,&quot;Sans-Serif&quot;;color:#333399;margin-left:0px;margin-right:0px;margin-top:0px;margin-bottom:10px'><b>&quot;POWERED BY ARUTI HR &amp; PAYROLL MANAGEMENT SOFTWARE.&quot;</b></center></p>")

                                StrMessage.Append("</span></p>")

                                StrMessage.Append("</BODY></HTML>")

                                Dim strEmailContent As String
                                strEmailContent = StrMessage.ToString()
                                strEmailContent = strEmailContent.Replace("#TrainingName#", "<B>" & drDeptTrainingList(0).Item("trainingcoursename") & "</B>")
                                strEmailContent = strEmailContent.Replace("#TargetedGroupName#", "<B>" & strTargetedGroupName & "</B>")
                                strEmailContent = strEmailContent.Replace("#ApprovalRejectionRemark#", "<B>" & drDeptTrainingList(0).Item("finalapproval_remark") & "</B>")

                                Dim objSendMail As New clsSendMail
                                objSendMail._ToEmail = drUser("email").ToString().Trim()
                                'Sohail (17 Aug 2021) - [Uemail=email]
                                objSendMail._Subject = strSubject
                                objSendMail._Message = strEmailContent
                                objSendMail._Form_Name = strFormName
                                objSendMail._LogEmployeeUnkid = -1
                                objSendMail._OperationModeId = eMode
                                objSendMail._UserUnkid = xUserUnkid
                                objSendMail._SenderAddress = strSenderAddress
                                objSendMail._ModuleRefId = clsSendMail.enAT_VIEW_TYPE.TRAININGREQUISITION_MGT
                                objSendMail.SendMail(xCompanyUnkid)
                                objSendMail = Nothing
                            Next

                        Case enEmailType.Final_Rejected
                            'Sohail (17 Aug 2021) -- Start
                            'NMB Enhancement : OLD-431 : Send alert to the creator of the plan only and change alert message on budget Approval/Rejection in departmental plan.
                            'dsUserList = objUsr.Get_UserBy_PrivilegeId(enUserPriviledge.AllowToAddDepartmentalTrainingNeed, xYearUnkid)
                            dsUserList = objUsr.GetList("List", " hrmsConfiguration..cfuser_master.userunkid = " & CInt(drDeptTrainingList(0).Item("createuserunkid")) & " ")
                            'Sohail (17 Aug 2021) -- End
                            strSubject = Language.getMessage(mstrModuleName, 53, "Notification: Training has been rejected")
                            For Each drUser As DataRow In dsUserList.Tables(0).Rows
                                If drUser("email").ToString().Trim().Length <= 0 Then Continue For
                                'Sohail (17 Aug 2021) - [Uemail=email]

                                Dim dsEmp As DataSet = objEmp.Get_AllocationBasedEmployee(CInt(drDeptTrainingList(0).Item("allocationid")), CInt(drDeptTrainingList(0).Item("departmentunkid")), intPeriodId, xDatabaseName, xUserModeSetting, xCompanyUnkid, CInt(drUser("userunkid")), True, objDataOperation)
                                'Sohail (17 Aug 2021) - [UId = userunkid]
                                If dsEmp.Tables(0).Rows.Count <= 0 Then Continue For

                                StrMessage = New System.Text.StringBuilder
                                StrMessage.Append("<HTML><BODY>")
                                StrMessage.Append(vbCrLf)

                                StrMessage.Append("<p><span style='font-size:9.0pt; font-family:" & Chr(34) & "Verdana" & Chr(34) & "," & Chr(34) & "Sans-Serif" & Chr(34) & "; color:#333399; '>")

                                'Sohail (17 Aug 2021) -- Start
                                'NMB Enhancement : OLD-431 : Send alert to the creator of the plan only and change alert message on budget Approval/Rejection in departmental plan.
                                'StrMessage.Append(Language.getMessage(mstrModuleName, 48, "Dear") & " " & "<b>" & getTitleCase(drUser.Item("Uname").ToString()) & "</b></span></p>")

                                'StrMessage.Append(Language.getMessage(mstrModuleName, 49, "Please be informed that the plan you submitted for #TargetedGroupName# with the program name #TrainingName# has been rejected.") & " </span></p> ")
                                StrMessage.Append(Language.getMessage(mstrModuleName, 48, "Dear") & " " & "<b>" & getTitleCase(If(Trim(drUser.Item("firstname").ToString & " " & drUser.Item("lastname").ToString) = "", drUser.Item("username").ToString, drUser.Item("firstname").ToString & " " & drUser.Item("lastname").ToString)) & "</b></span></p>")

                                StrMessage.Append(Language.getMessage(mstrModuleName, 57, "Please be informed that the plan you submitted for approval with the program name #TrainingName# has been rejected.") & " </span></p> ")
                                'Sohail (17 Aug 2021) -- End
                                StrMessage.Append(vbCrLf)
                                StrMessage.Append("<p>" & Language.getMessage(mstrModuleName, 50, "Remarks: #ApprovalRejectionRemark#.") & " </p> ")
                                StrMessage.Append(vbCrLf)


                                StrMessage.Append("<p><span style='font-size:9.0pt; font-family:" & Chr(34) & "Verdana" & Chr(34) & "," & Chr(34) & "Sans-Serif" & Chr(34) & "; color:#333399; '>")

                                StrMessage.Append("<p><center style='padding-top:25px;font-size:10.0pt;font-family:&quot;Verdana&quot;,&quot;Sans-Serif&quot;;color:#333399;margin-left:0px;margin-right:0px;margin-top:0px;margin-bottom:10px'><b>&quot;POWERED BY ARUTI HR &amp; PAYROLL MANAGEMENT SOFTWARE.&quot;</b></center></p>")

                                StrMessage.Append("</span></p>")

                                StrMessage.Append("</BODY></HTML>")

                                Dim strEmailContent As String
                                strEmailContent = StrMessage.ToString()
                                strEmailContent = strEmailContent.Replace("#TrainingName#", "<B>" & drDeptTrainingList(0).Item("trainingcoursename") & "</B>")
                                strEmailContent = strEmailContent.Replace("#TargetedGroupName#", "<B>" & strTargetedGroupName & "</B>")
                                strEmailContent = strEmailContent.Replace("#ApprovalRejectionRemark#", "<B>" & drDeptTrainingList(0).Item("finalapproval_remark") & "</B>")

                                Dim objSendMail As New clsSendMail
                                objSendMail._ToEmail = drUser("email").ToString().Trim()
                                'Sohail (17 Aug 2021) - [Uemail=email]
                                objSendMail._Subject = strSubject
                                objSendMail._Message = strEmailContent
                                objSendMail._Form_Name = strFormName
                                objSendMail._LogEmployeeUnkid = -1
                                objSendMail._OperationModeId = eMode
                                objSendMail._UserUnkid = xUserUnkid
                                objSendMail._SenderAddress = strSenderAddress
                                objSendMail._ModuleRefId = clsSendMail.enAT_VIEW_TYPE.TRAININGREQUISITION_MGT
                                objSendMail.SendMail(xCompanyUnkid)
                                objSendMail = Nothing
                            Next
                            'Sohail (04 Aug 2021) -- End

                    End Select
                End If

            Next
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: SendEmails; Module Name: " & mstrModuleName)
        End Try
    End Sub
    'Hemant (26 Mar 2021) -- End

    Public Function getCompetenceListForCombo(ByVal xDatatabaseName As String, ByVal intCompanyUnkId As Integer, ByVal strTableName As String, Optional ByVal blnAddSelect As Boolean = False) As DataSet
        Dim objPdpItem As New clspdpitem_master
        Dim dsList As New DataSet
        Dim StrQ As String = String.Empty
        Dim intPeriod As Integer = -1

        objDataOperation = New clsDataOperation

        Try
            intPeriod = objPdpItem.GetAssesmentPeriod(xDatatabaseName, intCompanyUnkId)

            If blnAddSelect Then
                StrQ = "SELECT 0 AS Id, ' ' + @Select AS Name UNION "
                objDataOperation.AddParameter("@Select", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 35, "Select"))
            End If

            StrQ &= "SELECT DISTINCT " & _
                        "masterunkid AS Id " & _
                      ", cfcommon_master.name as Name " & _
                    "FROM hrassess_competencies_master " & _
                        "JOIN cfcommon_master ON hrassess_competencies_master.competence_categoryunkid = cfcommon_master.masterunkid " & _
                    "WHERE hrassess_competencies_master.isactive = 1 " & _
                          "AND cfcommon_master.isactive = 1 " & _
                          "AND hrassess_competencies_master.periodunkid = @periodunkid "

            StrQ &= " ORDER BY Name "

            objDataOperation.AddParameter("@periodunkid", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, intPeriod)

            dsList = objDataOperation.ExecQuery(StrQ, strTableName)

            If objDataOperation.ErrorMessage <> "" Then
                Throw New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
            End If

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: getCompetenceListForCombo; Module Name: " & mstrModuleName)
        Finally
            objPdpItem = Nothing
        End Try
        Return dsList
    End Function

    Public Function getTrainingCourseListForCombo(ByVal xDatatabaseName As String, ByVal intCompanyUnkId As Integer, ByVal intCompetenceUnkId As Integer, ByVal strTableName As String, Optional ByVal blnAddSelect As Boolean = False) As DataSet
        Dim objPdpItem As New clspdpitem_master
        Dim dsList As New DataSet
        Dim StrQ As String = String.Empty
        Dim intPeriod As Integer = -1

        objDataOperation = New clsDataOperation

        Try
            intPeriod = objPdpItem.GetAssesmentPeriod(xDatatabaseName, intCompanyUnkId)

            If blnAddSelect Then
                StrQ = "SELECT 0 AS Id, ' ' + @Select AS Name UNION "
                objDataOperation.AddParameter("@Select", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 34, "Select"))
            End If

            StrQ &= "SELECT DISTINCT " & _
                        "cfcommon_master.masterunkid AS Id " & _
                      ", cfcommon_master.name as Name " & _
                    "FROM hrassess_competencies_master " & _
                        "JOIN hrassess_competencies_trainingcourses_tran " & _
                            "ON hrassess_competencies_master.competenciesunkid = hrassess_competencies_trainingcourses_tran.competenciesunkid " & _
                        "JOIN cfcommon_master " & _
                            "ON hrassess_competencies_trainingcourses_tran.masterunkid = cfcommon_master.masterunkid " & _
                    "WHERE cfcommon_master.isactive = 1 " & _
                          "AND hrassess_competencies_master.isactive = 1 " & _
                          "AND hrassess_competencies_trainingcourses_tran.isvoid = 0 " & _
                          "AND hrassess_competencies_master.periodunkid = @periodunkid "

            If intCompetenceUnkId > 0 Then
                StrQ &= " AND hrassess_competencies_master.competence_categoryunkid = @competence_categoryunkid "
                objDataOperation.AddParameter("@competence_categoryunkid", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, intCompetenceUnkId)
            End If

            StrQ &= " ORDER BY Name "

            objDataOperation.AddParameter("@periodunkid", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, intPeriod)

            dsList = objDataOperation.ExecQuery(StrQ, strTableName)

            If objDataOperation.ErrorMessage <> "" Then
                Throw New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
            End If

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: getTrainingCourseListForCombo; Module Name: " & mstrModuleName)
        Finally
            objPdpItem = Nothing
        End Try
        Return dsList
    End Function

    Public Function getNextRefNo(Optional ByVal xDataOp As clsDataOperation = Nothing) As Integer
        Dim dsList As New DataSet
        Dim StrQ As String = String.Empty
        Dim intRefNo As Integer = 1

        If xDataOp Is Nothing Then
            objDataOperation = New clsDataOperation
        Else
            objDataOperation = xDataOp
        End If
        objDataOperation.ClearParameters()

        Try

            StrQ = "SELECT ISNULL(MAX(CAST(refno AS INT)), 0) + 1 AS NextRefNo " & _
                    "FROM trdepartmentaltrainingneed_master " & _
                    "WHERE trdepartmentaltrainingneed_master.isvoid = 0 " & _
                          "AND refno <> '' "

            dsList = objDataOperation.ExecQuery(StrQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                Throw New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
            End If

            If dsList.Tables(0).Rows.Count > 0 Then
                intRefNo = CInt(dsList.Tables(0).Rows(0).Item(0))
            End If

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: getStatusComboList; Module Name: " & mstrModuleName)
        Finally
            If xDataOp Is Nothing Then objDataOperation = Nothing
        End Try
        Return intRefNo
    End Function

    Public Function GetTotalApprovedAmtAllocationWise(ByVal xPeriodId As Integer, ByVal xAllocationId As Integer, Optional ByVal xAllocationTranId As Integer = 0) As Decimal
        Dim dsList As New DataSet
        Dim StrQ As String = String.Empty
        Dim mdecApprovedTotalCost As Decimal = 0
        Try
            Dim objDataOperation As New clsDataOperation

            objDataOperation.ClearParameters()

            StrQ = " SELECT ISNULL(SUM(approved_totalcost),0.00) AS totalcost " & _
                       " FROM trdepartmentaltrainingneed_master  " & _
                       " WHERE isvoid = 0 AND  periodunkid = @periodId AND allocationid = @allocationId " & _
                       " AND statusunkid = @statusunkid "

            If xAllocationTranId > 0 OrElse xAllocationTranId < -1 Then
                StrQ &= " AND departmentunkid = @allocationtranunkid "
                objDataOperation.AddParameter("@allocationtranunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, xAllocationTranId)
            End If

            objDataOperation.AddParameter("@periodId", SqlDbType.Int, eZeeDataType.INT_SIZE, xPeriodId)
            objDataOperation.AddParameter("@allocationId", SqlDbType.Int, eZeeDataType.INT_SIZE, xAllocationId)
            objDataOperation.AddParameter("@statusunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, enApprovalStatus.FinalApproved)

            dsList = objDataOperation.ExecQuery(StrQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                Throw New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
            End If

            If dsList IsNot Nothing AndAlso dsList.Tables(0).Rows.Count > 0 Then
                mdecApprovedTotalCost = CDec(dsList.Tables(0).Rows(0)("totalcost"))
            End If

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: GetTotalApprovedAmtAllocationWise; Module Name: " & mstrModuleName)
        End Try
        Return mdecApprovedTotalCost
    End Function

    Public Function GetTotalCostAmtAllocationWise(ByVal xPeriodId As Integer, ByVal xAllocationId As Integer, Optional ByVal xAllocationTranId As Integer = 0, Optional ByVal strDeptTNeedUnkIDs As String = "") As Decimal
        Dim dsList As New DataSet
        Dim StrQ As String = String.Empty
        Dim mdecTotalCost As Decimal = 0
        Try
            Dim objDataOperation As New clsDataOperation

            objDataOperation.ClearParameters()

            StrQ = " SELECT ISNULL(SUM(totalcost),0.00) AS totalcost " & _
                       " FROM trdepartmentaltrainingneed_master  " & _
                       " WHERE isvoid = 0 AND  periodunkid = @periodId AND allocationid = @allocationId "

            If xAllocationTranId > 0 OrElse xAllocationTranId < -1 Then
                StrQ &= " AND trdepartmentaltrainingneed_master.departmentunkid = @allocationtranunkid "
                objDataOperation.AddParameter("@allocationtranunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, xAllocationTranId)
            End If

            If strDeptTNeedUnkIDs.Trim <> "" Then
                StrQ &= " AND trdepartmentaltrainingneed_master.departmentaltrainingneedunkid IN (" & strDeptTNeedUnkIDs & ") "
            End If

            objDataOperation.AddParameter("@periodId", SqlDbType.Int, eZeeDataType.INT_SIZE, xPeriodId)
            objDataOperation.AddParameter("@allocationId", SqlDbType.Int, eZeeDataType.INT_SIZE, xAllocationId)

            dsList = objDataOperation.ExecQuery(StrQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                Throw New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
            End If

            If dsList IsNot Nothing AndAlso dsList.Tables(0).Rows.Count > 0 Then
                mdecTotalCost = CDec(dsList.Tables(0).Rows(0)("totalcost"))
            End If

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: GetTotalApprovedAmtAllocationWise; Module Name: " & mstrModuleName)
        End Try
        Return mdecTotalCost
    End Function

    'Sohail (10 Feb 2022) -- Start
    'Enhancement :  OLD-551 : NMB - New config setting to allow Training Budget settings to be based on any allocation - Cost Center etc.
    'Public Function GetBudgetSummaryData(ByVal xCurrentPeriodId As Integer, ByVal xPreviousPeriodId As Integer, ByVal xAllocationId As Integer) As DataTable
    '    Dim dtTable As New DataTable
    '    Dim StrQ As String = String.Empty
    '    Dim dsList As DataSet = Nothing
    '    Try

    '        Dim objDataOperation As New clsDataOperation

    '        StrQ = "  SELECT trdepartmentaltrainingneed_master.allocationid, trdepartmentaltrainingneed_master.departmentunkid " & _
    '                   "   , trdepartmentaltrainingneed_master.refno , ISNULL(SUM(trdepartmentaltrainingneed_master.approved_totalcost),0.00) AS PreviousApprovedAmt " & _
    '                   "  INTO #PrevApprovedAmt " & _
    '                   "  FROM trdepartmentaltrainingneed_master " & _
    '                   "  WHERE trdepartmentaltrainingneed_master.isvoid = 0 And trdepartmentaltrainingneed_master.periodunkid = @PrePeriodId  " & _
    '                   "  AND trdepartmentaltrainingneed_master.allocationid = @AllocationId AND trdepartmentaltrainingneed_master.refno <> '' " & _
    '                   "  AND trdepartmentaltrainingneed_master.statusunkid in (" & enApprovalStatus.FinalApproved & ")" & _
    '                   "  GROUP BY trdepartmentaltrainingneed_master.refno,trdepartmentaltrainingneed_master.allocationid	, trdepartmentaltrainingneed_master.departmentunkid ; "

    '        StrQ &= "  SELECT trdepartmentaltrainingneed_master.allocationid, trdepartmentaltrainingneed_master.departmentunkid " & _
    '                     "      , trdepartmentaltrainingneed_master.refno  , ISNULL(SUM(trdepartmentaltrainingneed_master.approved_totalcost),0.00) AS CurrentApprovedAmt " & _
    '                     "  INTO #CurrApprovedAmt " & _
    '                     "  FROM trdepartmentaltrainingneed_master " & _
    '                     "  WHERE trdepartmentaltrainingneed_master.isvoid = 0 And trdepartmentaltrainingneed_master.periodunkid =  @CurPeriodId " & _
    '                     "   AND trdepartmentaltrainingneed_master.allocationid = @AllocationId AND trdepartmentaltrainingneed_master.refno <> '' " & _
    '                     "   AND trdepartmentaltrainingneed_master.statusunkid in (" & enApprovalStatus.FinalApproved & ")" & _
    '                     "   GROUP BY trdepartmentaltrainingneed_master.refno,trdepartmentaltrainingneed_master.allocationid , trdepartmentaltrainingneed_master.departmentunkid ; "


    '        StrQ &= " SELECT trdepartmentaltrainingneed_master.allocationid ,trdepartmentaltrainingneed_master.departmentunkid " & _
    '                     " ,trdepartmentaltrainingneed_master.refNo , ISNULL(SUM(enroll_amount),0.00) AS PreviousEnrolledAmt " & _
    '                     " INTO #PrevEnroll " & _
    '                     " FROM trtraining_request_master " & _
    '                     " LEFT JOIN trdepartmentaltrainingneed_master ON trdepartmentaltrainingneed_master.departmentaltrainingneedunkid = trtraining_request_master.departmentaltrainingneedunkid " & _
    '                     " WHERE trtraining_request_master.isvoid = 0 AND trdepartmentaltrainingneed_master.isvoid = 0  AND trtraining_request_master.issubmit_approval = 1 " & _
    '                     " AND trdepartmentaltrainingneed_master.periodunkid = @PrePeriodId AND trdepartmentaltrainingneed_master.allocationid = @AllocationId  AND trdepartmentaltrainingneed_master.refno <> '' " & _
    '                     " AND trdepartmentaltrainingneed_master.statusunkid in (" & enApprovalStatus.SubmittedForApprovalFromTrainingBacklog & "," & enApprovalStatus.AskedForReviewAsPerAmountSet & "," & enApprovalStatus.FinalApproved & ") " & _
    '                     " AND trdepartmentaltrainingneed_master.departmentunkid IS NOT NULL " & _
    '                     " GROUP BY trdepartmentaltrainingneed_master.allocationid,trdepartmentaltrainingneed_master.departmentunkid ,trdepartmentaltrainingneed_master.refNo ; "

    '        StrQ &= " SELECT trdepartmentaltrainingneed_master.allocationid ,trdepartmentaltrainingneed_master.departmentunkid " & _
    '                     " ,trdepartmentaltrainingneed_master.refNo , ISNULL(SUM(enroll_amount),0.00) AS CurrentEnrolledAmt " & _
    '                     " INTO #CurrEnroll " & _
    '                     " FROM trtraining_request_master " & _
    '                     " LEFT JOIN trdepartmentaltrainingneed_master ON trdepartmentaltrainingneed_master.departmentaltrainingneedunkid = trtraining_request_master.departmentaltrainingneedunkid " & _
    '                     " WHERE trtraining_request_master.isvoid = 0 AND trdepartmentaltrainingneed_master.isvoid = 0  AND trtraining_request_master.issubmit_approval = 1 " & _
    '                     " AND trdepartmentaltrainingneed_master.periodunkid = @CurPeriodId AND trdepartmentaltrainingneed_master.allocationid = @AllocationId  AND trdepartmentaltrainingneed_master.refno <> '' " & _
    '                     " AND trdepartmentaltrainingneed_master.statusunkid in (" & enApprovalStatus.SubmittedForApprovalFromTrainingBacklog & "," & enApprovalStatus.AskedForReviewAsPerAmountSet & "," & enApprovalStatus.FinalApproved & ") " & _
    '                     " AND trdepartmentaltrainingneed_master.departmentunkid IS NOT NULL " & _
    '                     " GROUP BY trdepartmentaltrainingneed_master.allocationid,trdepartmentaltrainingneed_master.departmentunkid ,trdepartmentaltrainingneed_master.refNo ; "


    '        StrQ &= " SELECT " & _
    '                   " trdepartmentaltrainingneed_master.allocationid " & _
    '                   ", trdepartmentaltrainingneed_master.departmentunkid "

    '        Select Case xAllocationId

    '            Case enAllocation.BRANCH

    '                StrQ &= ", CASE trdepartmentaltrainingneed_master.departmentunkid WHEN -99 THEN @Company ELSE ISNULL(hrstation_master.code,'') END AS allocationtrancode " & _
    '                            ", CASE trdepartmentaltrainingneed_master.departmentunkid WHEN -99 THEN @Company ELSE ISNULL(hrstation_master.name,'') END AS allocationtranname "

    '            Case enAllocation.DEPARTMENT_GROUP

    '                StrQ &= ", CASE trdepartmentaltrainingneed_master.departmentunkid WHEN -99 THEN @Company ELSE ISNULL(hrdepartment_group_master.code,'') END AS allocationtrancode " & _
    '                            ", CASE trdepartmentaltrainingneed_master.departmentunkid WHEN -99 THEN @Company ELSE ISNULL(hrdepartment_group_master.name,'') END AS allocationtranname "

    '            Case enAllocation.DEPARTMENT

    '                StrQ &= ", CASE trdepartmentaltrainingneed_master.departmentunkid WHEN -99 THEN @Company ELSE ISNULL(hrdepartment_master.code,'') END AS allocationtrancode " & _
    '                             ", CASE trdepartmentaltrainingneed_master.departmentunkid WHEN -99 THEN @Company ELSE ISNULL(hrdepartment_master.name,'') END AS allocationtranname "

    '            Case enAllocation.SECTION_GROUP

    '                StrQ &= ", CASE trdepartmentaltrainingneed_master.departmentunkid WHEN -99 THEN @Company ELSE ISNULL(hrsectiongroup_master.code,'') END AS allocationtrancode " & _
    '                            ", CASE trdepartmentaltrainingneed_master.departmentunkid WHEN -99 THEN @Company ELSE ISNULL(hrsectiongroup_master.name,'') END AS allocationtranname "

    '            Case enAllocation.SECTION

    '                StrQ &= ",  CASE trdepartmentaltrainingneed_master.departmentunkid WHEN -99 THEN @Company ELSE ISNULL(hrsection_master.code,'') END AS allocationtrancode " & _
    '                            ",  CASE trdepartmentaltrainingneed_master.departmentunkid WHEN -99 THEN @Company ELSE ISNULL(hrsection_master.name,'') END AS allocationtranname "

    '            Case enAllocation.UNIT_GROUP

    '                StrQ &= ", CASE trdepartmentaltrainingneed_master.departmentunkid WHEN -99 THEN @Company ELSE ISNULL(hrunitgroup_master.code,'') END AS allocationtrancode " & _
    '                             ", CASE trdepartmentaltrainingneed_master.departmentunkid WHEN -99 THEN @Company ELSE ISNULL(hrunitgroup_master.name,'') END AS allocationtranname "

    '            Case enAllocation.UNIT

    '                StrQ &= ",  CASE trdepartmentaltrainingneed_master.departmentunkid WHEN -99 THEN @Company ELSE ISNULL(hrunit_master.code,'') END AS allocationtrancode " & _
    '                            ",  CASE trdepartmentaltrainingneed_master.departmentunkid WHEN -99 THEN @Company ELSE ISNULL(hrunit_master.name,'') END AS allocationtranname "

    '            Case enAllocation.TEAM

    '                StrQ &= ", CASE trdepartmentaltrainingneed_master.departmentunkid WHEN -99 THEN @Company ELSE ISNULL(hrteam_master.code,'') END AS allocationtrancod e" & _
    '                            ", CASE trdepartmentaltrainingneed_master.departmentunkid WHEN -99 THEN @Company ELSE ISNULL(hrteam_master.name,'') END AS allocationtranname "

    '            Case enAllocation.JOB_GROUP

    '                StrQ &= ", CASE trdepartmentaltrainingneed_master.departmentunkid WHEN -99 THEN @Company ELSE ISNULL(hrjobgroup_master.code,'') END AS allocationtrancode" & _
    '                             ", CASE trdepartmentaltrainingneed_master.departmentunkid WHEN -99 THEN @Company ELSE ISNULL(hrjobgroup_master.name,'') END AS allocationtranname "

    '            Case enAllocation.JOBS

    '                StrQ &= ", CASE trdepartmentaltrainingneed_master.departmentunkid WHEN -99 THEN @Company ELSE ISNULL(hrjob_master.job_code,'') END AS allocationtrancode" & _
    '                             ", CASE trdepartmentaltrainingneed_master.departmentunkid WHEN -99 THEN @Company ELSE ISNULL(hrjob_master.job_name,'') END AS allocationtranname "

    '            Case enAllocation.CLASS_GROUP

    '                StrQ &= ", CASE trdepartmentaltrainingneed_master.departmentunkid WHEN -99 THEN @Company ELSE ISNULL(hrclassgroup_master.code,'') END AS allocationtrancode" & _
    '                            ", CASE trdepartmentaltrainingneed_master.departmentunkid WHEN -99 THEN @Company ELSE ISNULL(hrclassgroup_master.name,'') END AS allocationtranname "

    '            Case enAllocation.CLASSES

    '                StrQ &= ", CASE trdepartmentaltrainingneed_master.departmentunkid WHEN -99 THEN @Company ELSE ISNULL(hrclasses_master.code,'') END AS allocationtrancode" & _
    '                            ", CASE trdepartmentaltrainingneed_master.departmentunkid WHEN -99 THEN @Company ELSE ISNULL(hrclasses_master.name,'') END AS allocationtranname "

    '        End Select

    '        StrQ &= ", ISNULL(trtraining_calendar_master.calendar_name,'') AS Period " & _
    '                   ", ISNULL(A.PreviousRefNo,'') AS PreviousRefNo " & _
    '                   ", ISNULL(A.PreviousRequestedAmt,0.00) AS PreviousRequestedAmt " & _
    '                   ", ISNULL(B.PreviousApprovedAmt,0.00) AS PreviousApprovedAmt " & _
    '                   ", trdepartmentaltrainingneed_master.refno AS CurrentRefNo " & _
    '                   ", ISNULL(SUM(trdepartmentaltrainingneed_master.totalcost),0.00) AS CurrentRequestedAmt " & _
    '                   ", ISNULL(C.CurrentApprovedAmt,0.00) AS CurrentApprovedAmt " & _
    '                   ", ISNULL(D.PreviousEnrolledAmt,0.00) AS PreviousEnrolledAmt " & _
    '                   ", ISNULL(E.CurrentEnrolledAmt,0.00) AS CurrentEnrolledAmt " & _
    '                   " FROM trdepartmentaltrainingneed_master "


    '        Select Case xAllocationId

    '            Case enAllocation.BRANCH

    '                StrQ &= " LEFT JOIN hrstation_master ON hrstation_master.stationunkid = trdepartmentaltrainingneed_master.departmentunkid "

    '            Case enAllocation.DEPARTMENT_GROUP

    '                StrQ &= " LEFT JOIN hrdepartment_group_master ON hrdepartment_group_master.deptgroupunkid = trdepartmentaltrainingneed_master.departmentunkid "

    '            Case enAllocation.DEPARTMENT

    '                StrQ &= " LEFT JOIN hrdepartment_master ON hrdepartment_master.departmentunkid = trdepartmentaltrainingneed_master.departmentunkid "

    '            Case enAllocation.SECTION_GROUP

    '                StrQ &= " LEFT JOIN hrsectiongroup_master ON hrsectiongroup_master.sectiongroupunkid = trdepartmentaltrainingneed_master.departmentunkid "

    '            Case enAllocation.SECTION

    '                StrQ &= " LEFT JOIN hrsection_master ON hrsection_master.sectionunkid = trdepartmentaltrainingneed_master.departmentunkid "

    '            Case enAllocation.UNIT_GROUP

    '                StrQ &= " LEFT JOIN hrunitgroup_master ON hrunitgroup_master.unitgroupunkid = trdepartmentaltrainingneed_master.departmentunkid "

    '            Case enAllocation.UNIT

    '                StrQ &= " LEFT JOIN hrunit_master ON hrunit_master.unitunkid = trdepartmentaltrainingneed_master.departmentunkid "

    '            Case enAllocation.TEAM

    '                StrQ &= " LEFT JOIN hrteam_master ON hrteam_master.teamunkid = trdepartmentaltrainingneed_master.departmentunkid "

    '            Case enAllocation.JOB_GROUP

    '                StrQ &= " LEFT JOIN hrjobgroup_master ON hrjobgroup_master.jobgroupunkid = trdepartmentaltrainingneed_master.departmentunkid "

    '            Case enAllocation.JOBS

    '                StrQ &= " LEFT JOIN hrjob_master ON hrjob_master.jobunkid = trdepartmentaltrainingneed_master.departmentunkid "

    '            Case enAllocation.CLASS_GROUP

    '                StrQ &= " LEFT JOIN hrclassgroup_master ON hrclassgroup_master.classgroupunkid = trdepartmentaltrainingneed_master.departmentunkid "

    '            Case enAllocation.CLASSES

    '                StrQ &= " LEFT JOIN hrclasses_master ON hrclasses_master.classesunkid = trdepartmentaltrainingneed_master.departmentunkid "

    '        End Select


    '        StrQ &= " LEFT JOIN trtraining_calendar_master ON trtraining_calendar_master.calendarunkid = trdepartmentaltrainingneed_master.periodunkid " & _
    '                   " FULL OUTER JOIN " & _
    '                   " ( " & _
    '                   "        SELECT trdepartmentaltrainingneed_master.allocationid, trdepartmentaltrainingneed_master.departmentunkid " & _
    '                   "          , trdepartmentaltrainingneed_master.refno AS PreviousRefNo " & _
    '                   "          , ISNULL(SUM(trdepartmentaltrainingneed_master.totalcost),0.00) AS PreviousRequestedAmt " & _
    '                   "       FROM trdepartmentaltrainingneed_master  " & _
    '                   "       WHERE trdepartmentaltrainingneed_master.isvoid = 0 and trdepartmentaltrainingneed_master.periodunkid = @PrePeriodId " & _
    '                   "       AND trdepartmentaltrainingneed_master.allocationid = @AllocationId AND trdepartmentaltrainingneed_master.refno <> '' " & _
    '                   "       AND trdepartmentaltrainingneed_master.statusunkid in (" & enApprovalStatus.SubmittedForApprovalFromTrainingBacklog & "," & enApprovalStatus.AskedForReviewAsPerAmountSet & "," & enApprovalStatus.FinalApproved & ") " & _
    '                   "       GROUP BY trdepartmentaltrainingneed_master.refno,trdepartmentaltrainingneed_master.allocationid " & _
    '                   "       ,trdepartmentaltrainingneed_master.departmentunkid " & _
    '                   " ) AS A on A.allocationid = trdepartmentaltrainingneed_master.allocationid AND A.departmentunkid = trdepartmentaltrainingneed_master.departmentunkid " & _
    '                   " LEFT JOIN  #PrevApprovedAmt AS B ON B.allocationid = trdepartmentaltrainingneed_master.allocationid AND B.departmentunkid = trdepartmentaltrainingneed_master.departmentunkid AND B.refno = trdepartmentaltrainingneed_master.refno " & _
    '                   " LEFT JOIN  #CurrApprovedAmt AS C on C.allocationid = trdepartmentaltrainingneed_master.allocationid AND C.departmentunkid = trdepartmentaltrainingneed_master.departmentunkid AND C.refno = trdepartmentaltrainingneed_master.refno " & _
    '                   " LEFT JOIN  #PrevEnroll AS D ON D.allocationid = trdepartmentaltrainingneed_master.allocationid AND D.departmentunkid = trdepartmentaltrainingneed_master.departmentunkid AND D.refno = trdepartmentaltrainingneed_master.refno " & _
    '                   " LEFT JOIN  #CurrEnroll AS E ON E.allocationid = trdepartmentaltrainingneed_master.allocationid AND E.departmentunkid = trdepartmentaltrainingneed_master.departmentunkid AND E.refno = trdepartmentaltrainingneed_master.refno " & _
    '                   " WHERE trdepartmentaltrainingneed_master.isvoid = 0 AND trdepartmentaltrainingneed_master.periodunkid = @CurPeriodId " & _
    '                   " AND trdepartmentaltrainingneed_master.allocationid = @AllocationId AND trdepartmentaltrainingneed_master.refno <> '' " & _
    '                   " AND trdepartmentaltrainingneed_master.statusunkid in (" & enApprovalStatus.SubmittedForApprovalFromTrainingBacklog & "," & enApprovalStatus.AskedForReviewAsPerAmountSet & "," & enApprovalStatus.FinalApproved & ") " & _
    '                   " GROUP BY trdepartmentaltrainingneed_master.refno,ISNULL(A.PreviousRefNo,'') , ISNULL(A.PreviousRequestedAmt,0.00),  ISNULL(B.PreviousApprovedAmt,0.00) " & _
    '                   ", ISNULL(C.CurrentApprovedAmt,0.00), ISNULL(D.PreviousEnrolledAmt,0.00),ISNULL(E.CurrentEnrolledAmt,0.00) " & _
    '                   ", ISNULL(trtraining_calendar_master.calendar_name,'') ,trdepartmentaltrainingneed_master.allocationid, trdepartmentaltrainingneed_master.departmentunkid "

    '        Select Case xAllocationId

    '            Case enAllocation.BRANCH

    '                StrQ &= ", ISNULL(hrstation_master.code,'') " & _
    '                             ", ISNULL(hrstation_master.name,'') "

    '            Case enAllocation.DEPARTMENT_GROUP

    '                StrQ &= ", ISNULL(hrdepartment_group_master.code,'')  " & _
    '                            ", ISNULL(hrdepartment_group_master.name,'') "

    '            Case enAllocation.DEPARTMENT

    '                StrQ &= ", ISNULL(hrdepartment_master.code,'')  " & _
    '                             ", ISNULL(hrdepartment_master.name,'') "

    '            Case enAllocation.SECTION_GROUP

    '                StrQ &= ", ISNULL(hrsectiongroup_master.code,'') " & _
    '                            ", ISNULL(hrsectiongroup_master.name,'')  "

    '            Case enAllocation.SECTION

    '                StrQ &= ",  ISNULL(hrsection_master.code,'') " & _
    '                            ",  ISNULL(hrsection_master.name,'') "

    '            Case enAllocation.UNIT_GROUP

    '                StrQ &= ", ISNULL(hrunitgroup_master.code,'') " & _
    '                             ", ISNULL(hrunitgroup_master.name,'') "

    '            Case enAllocation.UNIT

    '                StrQ &= ",  ISNULL(hrunit_master.code,'')  " & _
    '                            ",  ISNULL(hrunit_master.name,'')  "

    '            Case enAllocation.TEAM

    '                StrQ &= ", ISNULL(hrteam_master.code,'') " & _
    '                            ", ISNULL(hrteam_master.name,'')  "

    '            Case enAllocation.JOB_GROUP

    '                StrQ &= ", ISNULL(hrjobgroup_master.code,'') " & _
    '                             ", ISNULL(hrjobgroup_master.name,'')  "

    '            Case enAllocation.JOBS

    '                StrQ &= ", ISNULL(hrjob_master.job_code,'') " & _
    '                             ", ISNULL(hrjob_master.job_name,'') "

    '            Case enAllocation.CLASS_GROUP

    '                StrQ &= ", ISNULL(hrclassgroup_master.code,'') " & _
    '                            ", ISNULL(hrclassgroup_master.name,'')  "

    '            Case enAllocation.CLASSES

    '                StrQ &= ", ISNULL(hrclasses_master.code,'') " & _
    '                            ", ISNULL(hrclasses_master.name,'')  "

    '        End Select

    '        StrQ &= " DROP TABLE #PrevApprovedAmt " & _
    '                " DROP TABLE #CurrApprovedAmt " & _
    '                " DROP TABLE #PrevEnroll " & _
    '                " DROP TABLE #CurrEnroll "

    '        objDataOperation.ClearParameters()
    '        objDataOperation.AddParameter("@CurPeriodId", SqlDbType.Int, eZeeDataType.INT_SIZE, xCurrentPeriodId)
    '        objDataOperation.AddParameter("@PrePeriodId", SqlDbType.Int, eZeeDataType.INT_SIZE, xPreviousPeriodId)
    '        objDataOperation.AddParameter("@AllocationId", SqlDbType.Int, eZeeDataType.INT_SIZE, xAllocationId)
    '        objDataOperation.AddParameter("@Company", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, " " & Language.getMessage("frmDepartmentalTrainingNeedsList", 79, "Company")) 'Sohail (06 May 2021)

    '        dsList = objDataOperation.ExecQuery(StrQ, "List")

    '        If objDataOperation.ErrorMessage <> "" Then
    '            Throw New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
    '        End If

    '        dtTable = dsList.Tables(0)

    '    Catch ex As Exception
    '        Throw New Exception(ex.Message & "; Procedure Name: GetBudgetSummaryData; Module Name: " & mstrModuleName)
    '    End Try
    '    Return dtTable
    'End Function
    Public Function GetBudgetSummaryData(ByVal xCurrentPeriodId As Integer _
                                         , ByVal xPreviousPeriodId As Integer _
                                         , ByVal xAllocationId As Integer _
                                         , ByVal xBudgetAllocationId As Integer _
                                         , ByVal xTrainingCostCenterAllocationID As Integer _
                                         , ByVal xTrainingRemainingBalanceBasedOnID As Integer _
                                         , ByRef strAllocationName As String _
                                         , Optional ByVal xBudgetAllocationTranUnkId As Integer = 0 _
                                         , Optional ByVal blnIncludePendingBudgetAllocation As Boolean = False _
                                         ) As DataTable
        'Sohail (03 Mar 2022) - [xBudgetAllocationTranUnkId, blnIncludePendingBudgetAllocation]
        Dim dtTable As New DataTable
        Dim StrQ As String = String.Empty
        Dim dsList As DataSet = Nothing
        Dim strTable As String = ""
        Dim strUnkIdField As String = ""
        Dim strCodeField As String = ""
        Dim strNameField As String = ""
        Dim strTransferTable As String = ""
        Dim strTransferUnkIdField As String = ""
        Dim strTransferFilter As String = ""
        Dim strTransferCCTable As String = ""
        Dim strTransferCCUnkIdField As String = ""

        Dim dtPrevEndDate As Date = DateAndTime.Now
        Dim dtCurrEndDate As Date = DateAndTime.Now

        strAllocationName = ""

        Try

            Dim objPeriod As New clsTraining_Calendar_Master
            Dim dsCombo As DataSet = objPeriod.GetList("List", " trtraining_calendar_master.calendarunkid IN (" & xPreviousPeriodId & ", " & xCurrentPeriodId & ") ")
            Dim dr() As DataRow
            If xPreviousPeriodId > 0 Then
                dr = dsCombo.Tables(0).Select("calendarunkid = " & xPreviousPeriodId & " ")
                If dr.Length > 0 Then
                    dtPrevEndDate = eZeeDate.convertDate(dr(0).Item("edate").ToString)
                End If
            Else
                xPreviousPeriodId = -999
            End If

            If xCurrentPeriodId > 0 Then
                dr = dsCombo.Tables(0).Select("calendarunkid = " & xCurrentPeriodId & " ")
                If dr.Length > 0 Then
                    dtCurrEndDate = eZeeDate.convertDate(dr(0).Item("edate").ToString)
                End If
            Else
                xCurrentPeriodId = -999
            End If
            objPeriod = Nothing

            Dim objDataOperation As New clsDataOperation

            Select Case xBudgetAllocationId

                Case enAllocation.BRANCH

                    strAllocationName = Language.getMessage("clsMasterData", 430, "Branch")
                    strTable = "hrstation_master"
                    strUnkIdField = "stationunkid"
                    strCodeField = "code"
                    strNameField = "name"
                    strTransferTable = "hremployee_transfer_tran"
                    strTransferUnkIdField = "stationunkid"

                Case enAllocation.DEPARTMENT_GROUP

                    strAllocationName = Language.getMessage("clsMasterData", 429, "Department Group")
                    strTable = "hrdepartment_group_master"
                    strUnkIdField = "deptgroupunkid"
                    strCodeField = "code"
                    strNameField = "name"
                    strTransferTable = "hremployee_transfer_tran"
                    strTransferUnkIdField = "deptgroupunkid"

                Case enAllocation.DEPARTMENT

                    strAllocationName = Language.getMessage("clsMasterData", 428, "Department")
                    strTable = "hrdepartment_master"
                    strUnkIdField = "departmentunkid"
                    strCodeField = "code"
                    strNameField = "name"
                    strTransferTable = "hremployee_transfer_tran"
                    strTransferUnkIdField = "departmentunkid"

                Case enAllocation.SECTION_GROUP

                    strAllocationName = Language.getMessage("clsMasterData", 427, "Section Group")
                    strTable = "hrsectiongroup_master"
                    strUnkIdField = "sectiongroupunkid"
                    strCodeField = "code"
                    strNameField = "name"
                    strTransferTable = "hremployee_transfer_tran"
                    strTransferUnkIdField = "sectiongroupunkid"

                Case enAllocation.SECTION

                    strAllocationName = Language.getMessage("clsMasterData", 426, "Section")
                    strTable = "hrsection_master"
                    strUnkIdField = "sectionunkid"
                    strCodeField = "code"
                    strNameField = "name"
                    strTransferTable = "hremployee_transfer_tran"
                    strTransferUnkIdField = "sectionunkid"

                Case enAllocation.UNIT_GROUP

                    strAllocationName = Language.getMessage("clsMasterData", 425, "Unit Group")
                    strTable = "hrunitgroup_master"
                    strUnkIdField = "unitgroupunkid"
                    strCodeField = "code"
                    strNameField = "name"
                    strTransferTable = "hremployee_transfer_tran"
                    strTransferUnkIdField = "unitgroupunkid"

                Case enAllocation.UNIT

                    strAllocationName = Language.getMessage("clsMasterData", 424, "Unit")
                    strTable = "hrunit_master"
                    strUnkIdField = "unitunkid"
                    strCodeField = "code"
                    strNameField = "name"
                    strTransferTable = "hremployee_transfer_tran"
                    strTransferUnkIdField = "unitunkid"

                Case enAllocation.TEAM

                    strAllocationName = Language.getMessage("clsMasterData", 423, "Team")
                    strTable = "hrteam_master"
                    strUnkIdField = "teamunkid"
                    strCodeField = "code"
                    strNameField = "name"
                    strTransferTable = "hremployee_transfer_tran"
                    strTransferUnkIdField = "teamunkid"

                Case enAllocation.JOB_GROUP

                    strAllocationName = Language.getMessage("clsMasterData", 422, "Job Group")
                    strTable = "hrjobgroup_master"
                    strUnkIdField = "jobgroupunkid"
                    strCodeField = "code"
                    strNameField = "name"
                    strTransferTable = "hremployee_categorization_tran"
                    strTransferUnkIdField = "jobgroupunkid"

                Case enAllocation.JOBS

                    strAllocationName = Language.getMessage("clsMasterData", 421, "Jobs")
                    strTable = "hrjob_master"
                    strUnkIdField = "jobunkid"
                    strCodeField = "job_code"
                    strNameField = "job_name"
                    strTransferTable = "hremployee_categorization_tran"
                    strTransferUnkIdField = "jobunkid"

                Case enAllocation.CLASS_GROUP

                    strAllocationName = Language.getMessage("clsMasterData", 420, "Class Group")
                    strTable = "hrclassgroup_master"
                    strUnkIdField = "classgroupunkid"
                    strCodeField = "code"
                    strNameField = "name"
                    strTransferTable = "hremployee_transfer_tran"
                    strTransferUnkIdField = "classgroupunkid"

                Case enAllocation.CLASSES

                    strAllocationName = Language.getMessage("clsMasterData", 419, "Classes")
                    strTable = "hrclasses_master"
                    strUnkIdField = "classesunkid"
                    strCodeField = "code"
                    strNameField = "name"
                    strTransferTable = "hremployee_transfer_tran"
                    strTransferUnkIdField = "classunkid"

                Case enAllocation.COST_CENTER

                    strAllocationName = Language.getMessage("clsMasterData", 586, "Cost Center")
                    strTable = "prcostcenter_master"
                    strUnkIdField = "costcenterunkid"
                    strCodeField = "customcode"
                    strNameField = "costcentername"
                    strTransferTable = "hremployee_cctranhead_tran"
                    strTransferUnkIdField = "cctranheadvalueid"
                    strTransferFilter = " AND istransactionhead = 0 "
            End Select

            If xTrainingCostCenterAllocationID > 0 Then

                Select Case xTrainingCostCenterAllocationID

                    Case enAllocation.BRANCH

                        strTransferCCTable = "hremployee_transfer_tran"
                        strTransferCCUnkIdField = "stationunkid"

                    Case enAllocation.DEPARTMENT_GROUP

                        strTransferCCTable = "hremployee_transfer_tran"
                        strTransferCCUnkIdField = "deptgroupunkid"

                    Case enAllocation.DEPARTMENT

                        strTransferCCTable = "hremployee_transfer_tran"
                        strTransferCCUnkIdField = "departmentunkid"

                    Case enAllocation.SECTION_GROUP

                        strTransferTable = "hremployee_transfer_tran"
                        strTransferUnkIdField = "sectiongroupunkid"

                    Case enAllocation.SECTION

                        strTransferCCTable = "hremployee_transfer_tran"
                        strTransferCCUnkIdField = "sectionunkid"

                    Case enAllocation.UNIT_GROUP

                        strTransferCCTable = "hremployee_transfer_tran"
                        strTransferCCUnkIdField = "unitgroupunkid"

                    Case enAllocation.UNIT

                        strTransferCCTable = "hremployee_transfer_tran"
                        strTransferCCUnkIdField = "unitunkid"

                    Case enAllocation.TEAM

                        strTransferCCTable = "hremployee_transfer_tran"
                        strTransferCCUnkIdField = "teamunkid"

                    Case enAllocation.JOB_GROUP

                        strTransferCCTable = "hremployee_categorization_tran"
                        strTransferCCUnkIdField = "jobgroupunkid"

                    Case enAllocation.JOBS

                        strTransferCCTable = "hremployee_categorization_tran"
                        strTransferCCUnkIdField = "jobunkid"

                    Case enAllocation.CLASS_GROUP

                        strTransferCCTable = "hremployee_transfer_tran"
                        strTransferCCUnkIdField = "classgroupunkid"

                    Case enAllocation.CLASSES

                        strTransferCCTable = "hremployee_transfer_tran"
                        strTransferCCUnkIdField = "classunkid"

                    Case enAllocation.COST_CENTER

                        strTransferCCTable = "hremployee_cctranhead_tran"
                        strTransferCCUnkIdField = "cctranheadvalueid"

                End Select
            End If

            If xAllocationId = xBudgetAllocationId Then

                StrQ = "  SELECT trdepartmentaltrainingneed_master.allocationid, trdepartmentaltrainingneed_master.departmentunkid " & _
                           "   , trdepartmentaltrainingneed_master.refno , ISNULL(SUM(trdepartmentaltrainingneed_master.approved_totalcost),0.00) AS PreviousApprovedAmt " & _
                           "  INTO #PrevApprovedAmt " & _
                           "  FROM trdepartmentaltrainingneed_master " & _
                           "  WHERE trdepartmentaltrainingneed_master.isvoid = 0 And trdepartmentaltrainingneed_master.periodunkid = @PrePeriodId  " & _
                           "  AND trdepartmentaltrainingneed_master.allocationid = @AllocationId AND trdepartmentaltrainingneed_master.refno <> '' " & _
                           "  AND trdepartmentaltrainingneed_master.statusunkid in (" & enApprovalStatus.FinalApproved & ")" & _
                           "  GROUP BY trdepartmentaltrainingneed_master.refno,trdepartmentaltrainingneed_master.allocationid	, trdepartmentaltrainingneed_master.departmentunkid ; "

                StrQ &= "  SELECT trdepartmentaltrainingneed_master.allocationid, trdepartmentaltrainingneed_master.departmentunkid " & _
                             "      , trdepartmentaltrainingneed_master.refno  , ISNULL(SUM(trdepartmentaltrainingneed_master.approved_totalcost),0.00) AS CurrentApprovedAmt " & _
                             "  INTO #CurrApprovedAmt " & _
                             "  FROM trdepartmentaltrainingneed_master " & _
                             "  WHERE trdepartmentaltrainingneed_master.isvoid = 0 And trdepartmentaltrainingneed_master.periodunkid =  @CurPeriodId " & _
                             "   AND trdepartmentaltrainingneed_master.allocationid = @AllocationId AND trdepartmentaltrainingneed_master.refno <> '' " & _
                             "   AND trdepartmentaltrainingneed_master.statusunkid in (" & enApprovalStatus.FinalApproved & ")" & _
                             "   GROUP BY trdepartmentaltrainingneed_master.refno,trdepartmentaltrainingneed_master.allocationid , trdepartmentaltrainingneed_master.departmentunkid ; "


                StrQ &= " SELECT trdepartmentaltrainingneed_master.allocationid ,trdepartmentaltrainingneed_master.departmentunkid " & _
                             " ,trdepartmentaltrainingneed_master.refNo , ISNULL(SUM(enroll_amount),0.00) AS PreviousEnrolledAmt " & _
                             " INTO #PrevEnroll " & _
                             " FROM trtraining_request_master " & _
                             " LEFT JOIN trdepartmentaltrainingneed_master ON trdepartmentaltrainingneed_master.departmentaltrainingneedunkid = trtraining_request_master.departmentaltrainingneedunkid " & _
                             " WHERE trtraining_request_master.isvoid = 0 AND trdepartmentaltrainingneed_master.isvoid = 0  AND trtraining_request_master.isenroll_confirm = 1 " & _
                             " AND trdepartmentaltrainingneed_master.periodunkid = @PrePeriodId AND trdepartmentaltrainingneed_master.allocationid = @AllocationId  AND trdepartmentaltrainingneed_master.refno <> '' " & _
                             " AND trdepartmentaltrainingneed_master.statusunkid in (" & enApprovalStatus.SubmittedForApprovalFromTrainingBacklog & "," & enApprovalStatus.AskedForReviewAsPerAmountSet & "," & enApprovalStatus.FinalApproved & ") " & _
                             " AND trdepartmentaltrainingneed_master.departmentunkid IS NOT NULL " & _
                             " GROUP BY trdepartmentaltrainingneed_master.allocationid,trdepartmentaltrainingneed_master.departmentunkid ,trdepartmentaltrainingneed_master.refNo ; "

                StrQ &= " SELECT trdepartmentaltrainingneed_master.allocationid ,trdepartmentaltrainingneed_master.departmentunkid " & _
                             " ,trdepartmentaltrainingneed_master.refNo , ISNULL(SUM(enroll_amount),0.00) AS CurrentEnrolledAmt " & _
                             " INTO #CurrEnroll " & _
                             " FROM trtraining_request_master " & _
                             " LEFT JOIN trdepartmentaltrainingneed_master ON trdepartmentaltrainingneed_master.departmentaltrainingneedunkid = trtraining_request_master.departmentaltrainingneedunkid " & _
                             " WHERE trtraining_request_master.isvoid = 0 AND trdepartmentaltrainingneed_master.isvoid = 0  AND trtraining_request_master.isenroll_confirm = 1 " & _
                             " AND trdepartmentaltrainingneed_master.periodunkid = @CurPeriodId AND trdepartmentaltrainingneed_master.allocationid = @AllocationId  AND trdepartmentaltrainingneed_master.refno <> '' " & _
                             " AND trdepartmentaltrainingneed_master.statusunkid in (" & enApprovalStatus.SubmittedForApprovalFromTrainingBacklog & "," & enApprovalStatus.AskedForReviewAsPerAmountSet & "," & enApprovalStatus.FinalApproved & ") " & _
                             " AND trdepartmentaltrainingneed_master.departmentunkid IS NOT NULL " & _
                             " GROUP BY trdepartmentaltrainingneed_master.allocationid,trdepartmentaltrainingneed_master.departmentunkid ,trdepartmentaltrainingneed_master.refNo ; "


                StrQ &= " SELECT  trdepartmentaltrainingneed_master.allocationid " & _
                               ", trdepartmentaltrainingneed_master.departmentunkid " & _
                               ", CASE trdepartmentaltrainingneed_master.departmentunkid WHEN -99 THEN @Company ELSE ISNULL(" & strTable & "." & strCodeField & ",'') END AS allocationtrancode " & _
                               ", CASE trdepartmentaltrainingneed_master.departmentunkid WHEN -99 THEN @Company ELSE ISNULL(" & strTable & "." & strNameField & ",'') END AS allocationtranname " & _
                               ", ISNULL(trtraining_calendar_master.calendar_name,'') AS Period " & _
                               ", ISNULL(A.PreviousRefNo,'') AS PreviousRefNo " & _
                               ", ISNULL(A.PreviousRequestedAmt,0.00) AS PreviousRequestedAmt " & _
                               ", ISNULL(B.PreviousApprovedAmt,0.00) AS PreviousApprovedAmt " & _
                               ", trdepartmentaltrainingneed_master.refno AS CurrentRefNo " & _
                               ", ISNULL(SUM(trdepartmentaltrainingneed_master.totalcost),0.00) AS CurrentRequestedAmt " & _
                               ", ISNULL(C.CurrentApprovedAmt,0.00) AS CurrentApprovedAmt " & _
                               ", ISNULL(D.PreviousEnrolledAmt,0.00) AS PreviousEnrolledAmt " & _
                               ", ISNULL(E.CurrentEnrolledAmt,0.00) AS CurrentEnrolledAmt " & _
                               ", ISNULL(A.Prevmaxbudget_amt, 0) AS Prevmaxbudget_amt " & _
                               ", ISNULL(trdept_maxbudget.maxbudget_amt, 0) AS Currmaxbudget_amt "

                If xTrainingRemainingBalanceBasedOnID = enTrainingRemainingBalanceBasedOn.Approved_Amount Then
                    StrQ &= ", ISNULL(A.Prevmaxbudget_amt, 0) - ISNULL(B.PreviousApprovedAmt,0.00) AS PrevRemainingBalance " & _
                            ", ISNULL(trdept_maxbudget.maxbudget_amt, 0) - ISNULL(C.CurrentApprovedAmt,0.00) AS CurrRemainingBalance "
                Else
                    StrQ &= ", ISNULL(A.Prevmaxbudget_amt, 0) - ISNULL(D.PreviousEnrolledAmt,0.00) AS PrevRemainingBalance " & _
                            ", ISNULL(trdept_maxbudget.maxbudget_amt, 0) - ISNULL(E.CurrentEnrolledAmt,0.00) AS CurrRemainingBalance "
                End If

                StrQ &= " FROM trdepartmentaltrainingneed_master " & _
                        " LEFT JOIN " & strTable & " ON " & strTable & "." & strUnkIdField & " = trdepartmentaltrainingneed_master.departmentunkid "

                StrQ &= " LEFT JOIN trtraining_calendar_master ON trtraining_calendar_master.calendarunkid = trdepartmentaltrainingneed_master.periodunkid " & _
                       " FULL OUTER JOIN " & _
                       " ( " & _
                       "        SELECT trdepartmentaltrainingneed_master.allocationid, trdepartmentaltrainingneed_master.departmentunkid " & _
                       "          , trdepartmentaltrainingneed_master.refno AS PreviousRefNo " & _
                       "          , ISNULL(SUM(trdepartmentaltrainingneed_master.totalcost),0.00) AS PreviousRequestedAmt " & _
                       "          , ISNULL(trdept_maxbudget.maxbudget_amt, 0) AS Prevmaxbudget_amt " & _
                       "       FROM trdepartmentaltrainingneed_master  " & _
                       "        LEFT JOIN trdept_maxbudget ON trdept_maxbudget.allocationid = trdepartmentaltrainingneed_master.allocationid AND trdepartmentaltrainingneed_master.departmentunkid = trdept_maxbudget.allocationtranunkid AND trdepartmentaltrainingneed_master.periodunkid = trdept_maxbudget.periodunkid " & _
                                    "AND trdept_maxbudget.isvoid = 0 " & _
                       "       WHERE trdepartmentaltrainingneed_master.isvoid = 0 and trdepartmentaltrainingneed_master.periodunkid = @PrePeriodId " & _
                       "       AND trdepartmentaltrainingneed_master.allocationid = @AllocationId AND trdepartmentaltrainingneed_master.refno <> '' " & _
                       "       AND trdepartmentaltrainingneed_master.statusunkid in (" & enApprovalStatus.SubmittedForApprovalFromTrainingBacklog & "," & enApprovalStatus.AskedForReviewAsPerAmountSet & "," & enApprovalStatus.FinalApproved & ") "

                'Sohail (03 Mar 2022) -- Start
                'Enhancement :  OLD-553 : NMB - New Report "Training Request Budget Report" : Training Budgets Actuals and Remaining Balances Report.
                If xBudgetAllocationTranUnkId > 0 Then
                    StrQ &= " AND trdepartmentaltrainingneed_master.departmentunkid = @allocationtranunkid "
                End If
                'Sohail (03 Mar 2022) -- End

                StrQ &= "       GROUP BY trdepartmentaltrainingneed_master.refno,trdepartmentaltrainingneed_master.allocationid " & _
                       "       ,trdepartmentaltrainingneed_master.departmentunkid, ISNULL(trdept_maxbudget.maxbudget_amt, 0) " & _
                       " ) AS A on A.allocationid = trdepartmentaltrainingneed_master.allocationid AND A.departmentunkid = trdepartmentaltrainingneed_master.departmentunkid " & _
                           " LEFT JOIN  #PrevApprovedAmt AS B ON B.allocationid = trdepartmentaltrainingneed_master.allocationid AND B.departmentunkid = trdepartmentaltrainingneed_master.departmentunkid AND B.refno = trdepartmentaltrainingneed_master.refno " & _
                           " LEFT JOIN  #CurrApprovedAmt AS C on C.allocationid = trdepartmentaltrainingneed_master.allocationid AND C.departmentunkid = trdepartmentaltrainingneed_master.departmentunkid AND C.refno = trdepartmentaltrainingneed_master.refno " & _
                           " LEFT JOIN  #PrevEnroll AS D ON D.allocationid = trdepartmentaltrainingneed_master.allocationid AND D.departmentunkid = trdepartmentaltrainingneed_master.departmentunkid AND D.refno = trdepartmentaltrainingneed_master.refno " & _
                           " LEFT JOIN  #CurrEnroll AS E ON E.allocationid = trdepartmentaltrainingneed_master.allocationid AND E.departmentunkid = trdepartmentaltrainingneed_master.departmentunkid AND E.refno = trdepartmentaltrainingneed_master.refno " & _
                           " LEFT JOIN trdept_maxbudget ON trdept_maxbudget.allocationid = trdepartmentaltrainingneed_master.allocationid AND trdepartmentaltrainingneed_master.departmentunkid = trdept_maxbudget.allocationtranunkid AND trdepartmentaltrainingneed_master.periodunkid = trdept_maxbudget.periodunkid " & _
                                "AND trdept_maxbudget.isvoid = 0 " & _
                       " WHERE trdepartmentaltrainingneed_master.isvoid = 0 AND trdepartmentaltrainingneed_master.periodunkid = @CurPeriodId " & _
                       " AND trdepartmentaltrainingneed_master.allocationid = @AllocationId AND trdepartmentaltrainingneed_master.refno <> '' " & _
                       " AND trdepartmentaltrainingneed_master.statusunkid in (" & enApprovalStatus.SubmittedForApprovalFromTrainingBacklog & "," & enApprovalStatus.AskedForReviewAsPerAmountSet & "," & enApprovalStatus.FinalApproved & ") "

                'Sohail (03 Mar 2022) -- Start
                'Enhancement :  OLD-553 : NMB - New Report "Training Request Budget Report" : Training Budgets Actuals and Remaining Balances Report.
                If xBudgetAllocationTranUnkId > 0 Then
                    StrQ &= " AND trdepartmentaltrainingneed_master.departmentunkid = @allocationtranunkid "
                End If
                'Sohail (03 Mar 2022) -- End

                StrQ &= " GROUP BY ISNULL(trtraining_calendar_master.calendar_name,''), trdepartmentaltrainingneed_master.allocationid, ISNULL(" & strTable & "." & strNameField & ",''), ISNULL(" & strTable & "." & strCodeField & ",''), trdepartmentaltrainingneed_master.refno,ISNULL(A.PreviousRefNo,'') , ISNULL(A.PreviousRequestedAmt,0.00),  ISNULL(B.PreviousApprovedAmt,0.00) " & _
                       ", ISNULL(C.CurrentApprovedAmt,0.00), ISNULL(D.PreviousEnrolledAmt,0.00),ISNULL(E.CurrentEnrolledAmt,0.00) " & _
                       ", trdepartmentaltrainingneed_master.departmentunkid, ISNULL(A.Prevmaxbudget_amt, 0), ISNULL(trdept_maxbudget.maxbudget_amt, 0) "

            Else

                StrQ = "SELECT DISTINCT " & _
                            "trtraining_request_master.employeeunkid " & _
                        "INTO #TableEmpPrev " & _
                        "FROM trtraining_request_master " & _
                        "WHERE trtraining_request_master.isvoid = 0 " & _
                              "AND trtraining_request_master.periodunkid = @PrePeriodId "

                StrQ &= "SELECT DISTINCT " & _
                            "trtraining_request_master.employeeunkid " & _
                        "INTO #TableEmpCurr " & _
                        "FROM trtraining_request_master " & _
                        "WHERE trtraining_request_master.isvoid = 0 " & _
                              "AND trtraining_request_master.periodunkid = @CurPeriodId "

                StrQ &= "SELECT " & xBudgetAllocationId & " AS allocationid " & _
                             ", A.allocationtranunkid " & _
                             ", A.employeeunkid " & _
                             ", A.allocationtrancode " & _
                             ", A.allocationtranname " & _
                        "INTO #TableEmpCCPrev " & _
                        "FROM #TableEmpPrev " & _
                            "LEFT JOIN " & _
                            "( " & _
                                "SELECT " & strTransferTable & ".employeeunkid " & _
                                     ", " & strTransferUnkIdField & " AS allocationtranunkid " & _
                                     ", " & strTable & "." & strCodeField & " AS allocationtrancode " & _
                                     ", " & strTable & "." & strNameField & " AS allocationtranname " & _
                                     ", ROW_NUMBER() OVER (PARTITION BY " & strTransferTable & ".employeeunkid ORDER BY " & strTransferTable & ".effectivedate DESC) AS ROWNO " & _
                                "FROM " & strTransferTable & " " & _
                                    "JOIN #TableEmpPrev ON #TableEmpPrev.employeeunkid = " & strTransferTable & ".employeeunkid " & _
                                    "LEFT JOIN " & strTable & " ON " & strTransferTable & "." & strTransferUnkIdField & " = " & strTable & "." & strUnkIdField & " " & _
                                "WHERE " & strTransferTable & ".isvoid = 0 " & _
                                      strTransferFilter & _
                                      "AND CONVERT(CHAR(8), " & strTransferTable & ".effectivedate, 112) <= @PrevEnddate " & _
                            ") AS A " & _
                                "ON A.employeeunkid = #TableEmpPrev.employeeunkid " & _
                        "WHERE A.ROWNO = 1 "

                StrQ &= "SELECT " & xBudgetAllocationId & " AS allocationid " & _
                             ", A.allocationtranunkid " & _
                             ", A.employeeunkid " & _
                             ", A.allocationtrancode " & _
                             ", A.allocationtranname " & _
                        "INTO #TableEmpCCCurr " & _
                        "FROM #TableEmpCurr " & _
                            "LEFT JOIN " & _
                            "( " & _
                                "SELECT " & strTransferTable & ".employeeunkid " & _
                                     ", " & strTransferUnkIdField & " AS allocationtranunkid " & _
                                     ", " & strTable & "." & strCodeField & " AS allocationtrancode " & _
                                     ", " & strTable & "." & strNameField & " AS allocationtranname " & _
                                     ", ROW_NUMBER() OVER (PARTITION BY " & strTransferTable & ".employeeunkid ORDER BY " & strTransferTable & ".effectivedate DESC) AS ROWNO " & _
                                "FROM " & strTransferTable & " " & _
                                    "JOIN #TableEmpCurr ON #TableEmpCurr.employeeunkid = " & strTransferTable & ".employeeunkid " & _
                                    "LEFT JOIN " & strTable & " ON " & strTransferTable & "." & strTransferUnkIdField & " = " & strTable & "." & strUnkIdField & " " & _
                                "WHERE " & strTransferTable & ".isvoid = 0 " & _
                                      strTransferFilter & _
                                      "AND CONVERT(CHAR(8), " & strTransferTable & ".effectivedate, 112) <= @CurrEnddate " & _
                            ") AS A " & _
                                "ON A.employeeunkid = #TableEmpCurr.employeeunkid " & _
                        "WHERE A.ROWNO = 1 "

                If xTrainingCostCenterAllocationID > 0 Then

                    StrQ &= "SELECT B.employeeunkid " & _
                                 ", trdept_costcenter_mapping.allocationtranunkid " & _
                                 ", B.mappedallocationtranunkid " & _
                                 ", trdept_costcenter_mapping.costcenterunkid " & _
                                 ", prcostcenter_master.customcode AS cccode " & _
                                 ", prcostcenter_master.costcentername AS ccname " & _
                            "INTO #CCMappingPrev " & _
                            "FROM trdept_costcenter_mapping " & _
                                "LEFT JOIN " & _
                                "( " & _
                                    "SELECT " & strTransferCCTable & ".employeeunkid " & _
                                         ", " & strTransferCCTable & "." & strTransferCCUnkIdField & " AS mappedallocationtranunkid " & _
                                         ", ROW_NUMBER() OVER (PARTITION BY " & strTransferCCTable & ".employeeunkid ORDER BY effectivedate DESC ) AS ROWNO " & _
                                    "FROM " & strTransferCCTable & " " & _
                                        "JOIN #TableEmpPrev " & _
                                            "ON #TableEmpPrev.employeeunkid = " & strTransferCCTable & ".employeeunkid " & _
                                    "WHERE isvoid = 0 " & _
                                          "AND CONVERT(CHAR(8), " & strTransferCCTable & ".effectivedate, 112) <= @PrevEnddate " & _
                                ") AS B ON B.mappedallocationtranunkid = trdept_costcenter_mapping.allocationtranunkid " & _
                                "LEFT JOIN prcostcenter_master ON trdept_costcenter_mapping.costcenterunkid = prcostcenter_master.costcenterunkid " & _
                            "WHERE trdept_costcenter_mapping.isvoid = 0 " & _
                                  "AND trdept_costcenter_mapping.costcenterunkid > 0 " & _
                                  "AND trdept_costcenter_mapping.allocationid = " & xTrainingCostCenterAllocationID & " " & _
                                  "AND B.ROWNO = 1 "

                    StrQ &= "UPDATE #TableEmpCCPrev " & _
                            "SET allocationtranunkid = #CCMappingPrev.costcenterunkid " & _
                              ", allocationtrancode = #CCMappingPrev.cccode " & _
                              ", allocationtranname = #CCMappingPrev.ccname " & _
                            "FROM #CCMappingPrev " & _
                            "WHERE #CCMappingPrev.employeeunkid = #TableEmpCCPrev.employeeunkid "

                    StrQ &= "SELECT B.employeeunkid " & _
                                 ", trdept_costcenter_mapping.allocationtranunkid " & _
                                 ", B.mappedallocationtranunkid " & _
                                 ", trdept_costcenter_mapping.costcenterunkid " & _
                                 ", prcostcenter_master.customcode AS cccode " & _
                                 ", prcostcenter_master.costcentername AS ccname " & _
                            "INTO #CCMappingCurr " & _
                            "FROM trdept_costcenter_mapping " & _
                                "LEFT JOIN " & _
                                "( " & _
                                    "SELECT " & strTransferCCTable & ".employeeunkid " & _
                                         ", " & strTransferCCTable & "." & strTransferCCUnkIdField & " AS mappedallocationtranunkid " & _
                                         ", ROW_NUMBER() OVER (PARTITION BY " & strTransferCCTable & ".employeeunkid ORDER BY effectivedate DESC ) AS ROWNO " & _
                                    "FROM " & strTransferCCTable & " " & _
                                        "JOIN #TableEmpCurr " & _
                                            "ON #TableEmpCurr.employeeunkid = " & strTransferCCTable & ".employeeunkid " & _
                                    "WHERE isvoid = 0 " & _
                                          "AND CONVERT(CHAR(8), " & strTransferCCTable & ".effectivedate, 112) <= @CurrEnddate " & _
                                ") AS B ON B.mappedallocationtranunkid = trdept_costcenter_mapping.allocationtranunkid " & _
                                "LEFT JOIN prcostcenter_master ON trdept_costcenter_mapping.costcenterunkid = prcostcenter_master.costcenterunkid " & _
                            "WHERE trdept_costcenter_mapping.isvoid = 0 " & _
                                  "AND trdept_costcenter_mapping.costcenterunkid > 0 " & _
                                  "AND trdept_costcenter_mapping.allocationid = " & xTrainingCostCenterAllocationID & " " & _
                                  "AND B.ROWNO = 1 "

                    StrQ &= "UPDATE #TableEmpCCCurr " & _
                            "SET allocationtranunkid = #CCMappingCurr.costcenterunkid " & _
                              ", allocationtrancode = #CCMappingCurr.cccode " & _
                              ", allocationtranname = #CCMappingCurr.ccname " & _
                            "FROM #CCMappingCurr " & _
                            "WHERE #CCMappingCurr.employeeunkid = #TableEmpCCCurr.employeeunkid "

                End If

                StrQ &= "SELECT trtraining_request_master.employeeunkid " & _
                             ", #TableEmpCCPrev.allocationid " & _
                             ", #TableEmpCCPrev.allocationtranunkid AS departmentunkid " & _
                             ", #TableEmpCCPrev.allocationtrancode " & _
                             ", #TableEmpCCPrev.allocationtranname " & _
                             ", trtraining_request_master.periodunkid " & _
                             ", ISNULL(trtraining_calendar_master.calendar_name, '') AS Period " & _
                             ", trtraining_request_master.trainingcostemp " & _
                             ", trtraining_request_master.approvedamountemp " & _
                             ", trtraining_request_master.enroll_amount " & _
                             ", trtraining_request_master.issubmit_approval " & _
                             ", trtraining_request_master.isenroll_confirm " & _
                             ", trtraining_request_master.statusunkid " & _
                        "INTO #TrainingPrev " & _
                        "FROM trtraining_request_master " & _
                            "JOIN #TableEmpCCPrev " & _
                                "ON #TableEmpCCPrev.employeeunkid = trtraining_request_master.employeeunkid " & _
                            "LEFT JOIN trtraining_calendar_master ON trtraining_request_master.periodunkid = trtraining_calendar_master.calendarunkid " & _
                        "WHERE trtraining_request_master.isvoid = 0 " & _
                              "AND trtraining_request_master.periodunkid = @PrePeriodId "

                StrQ &= "SELECT trtraining_request_master.employeeunkid " & _
                            ", #TableEmpCCCurr.allocationid " & _
                            ", #TableEmpCCCurr.allocationtranunkid AS departmentunkid " & _
                            ", #TableEmpCCCurr.allocationtrancode " & _
                            ", #TableEmpCCCurr.allocationtranname " & _
                            ", trtraining_request_master.periodunkid " & _
                            ", ISNULL(trtraining_calendar_master.calendar_name, '') AS Period " & _
                            ", trtraining_request_master.trainingcostemp " & _
                            ", trtraining_request_master.approvedamountemp " & _
                            ", trtraining_request_master.enroll_amount " & _
                            ", trtraining_request_master.isenroll_confirm " & _
                            ", trtraining_request_master.statusunkid " & _
                       "INTO #TrainingCurr " & _
                       "FROM trtraining_request_master " & _
                           "JOIN #TableEmpCCCurr " & _
                               "ON #TableEmpCCCurr.employeeunkid = trtraining_request_master.employeeunkid " & _
                           "LEFT JOIN trtraining_calendar_master ON trtraining_request_master.periodunkid = trtraining_calendar_master.calendarunkid " & _
                       "WHERE trtraining_request_master.isvoid = 0 " & _
                             "AND trtraining_request_master.periodunkid = @CurPeriodId "

                StrQ &= "  SELECT #TrainingPrev.allocationid, #TrainingPrev.departmentunkid " & _
                           "  , ISNULL(SUM(#TrainingPrev.approvedamountemp),0.00) AS PreviousApprovedAmt " & _
                           "  INTO #PrevApprovedAmt " & _
                           "  FROM #TrainingPrev " & _
                           "  WHERE 1 = 1 " & _
                           "  AND #TrainingPrev.statusunkid in (" & clstraining_requisition_approval_master.enApprovalStatus.Approved & ")" & _
                           "  GROUP BY #TrainingPrev.allocationid, #TrainingPrev.departmentunkid ; "

                StrQ &= "  SELECT #TrainingCurr.allocationid, #TrainingCurr.departmentunkid " & _
                             "  , ISNULL(SUM(#TrainingCurr.approvedamountemp),0.00) AS CurrentApprovedAmt " & _
                             "  INTO #CurrApprovedAmt " & _
                             "  FROM #TrainingCurr " & _
                             "  WHERE 1 = 1 " & _
                             "   AND #TrainingCurr.statusunkid in (" & clstraining_requisition_approval_master.enApprovalStatus.Approved & ")" & _
                             "   GROUP BY #TrainingCurr.allocationid, #TrainingCurr.departmentunkid ; "


                StrQ &= " SELECT #TrainingPrev.allocationid ,#TrainingPrev.departmentunkid " & _
                             " , ISNULL(SUM(#TrainingPrev.enroll_amount),0.00) AS PreviousEnrolledAmt " & _
                             " INTO #PrevEnroll " & _
                             " FROM #TrainingPrev " & _
                             " WHERE #TrainingPrev.isenroll_confirm = 1 " & _
                             " GROUP BY #TrainingPrev.allocationid,#TrainingPrev.departmentunkid ; "

                StrQ &= " SELECT #TrainingCurr.allocationid ,#TrainingCurr.departmentunkid " & _
                             " , ISNULL(SUM(#TrainingCurr.enroll_amount),0.00) AS CurrentEnrolledAmt " & _
                             " INTO #CurrEnroll " & _
                             " FROM #TrainingCurr " & _
                             " WHERE #TrainingCurr.isenroll_confirm = 1 " & _
                             " GROUP BY #TrainingCurr.allocationid,#TrainingCurr.departmentunkid ; "

                'Hemant (03 Feb 2023) -- Start
                'ISSUE(NMB) : Error "Conversion from string "" to type Integer is not valid." pops up when user clicks on training budget approval 
                'StrQ &= " SELECT  #TrainingCurr.allocationid " & _
                '                             ", #TrainingCurr.departmentunkid " & _
                '                             ", #TrainingCurr.allocationtrancode " & _
                '                             ", #TrainingCurr.allocationtranname " & _
                '                             ", #TrainingCurr.Period "
                StrQ &= " SELECT  ISNULL(#TrainingCurr.allocationid, A.allocationid) AS allocationid " & _
                        ",        ISNULL(#TrainingCurr.departmentunkid, A.departmentunkid)  AS departmentunkid " & _
                        ",        ISNULL(#TrainingCurr.allocationtrancode, A.allocationtrancode)  AS allocationtrancode " & _
                        ",        ISNULL(#TrainingCurr.allocationtranname, A.allocationtranname)  AS allocationtranname " & _
                        ",        ISNULL(#TrainingCurr.Period, A.Period)  AS Period "
                'Hemant (03 Feb 2023) -- End  

                StrQ &= ", '' AS PreviousRefNo " & _
                             ", ISNULL(A.PreviousRequestedAmt,0.00) AS PreviousRequestedAmt " & _
                             ", ISNULL(B.PreviousApprovedAmt,0.00) AS PreviousApprovedAmt " & _
                             ", '' AS CurrentRefNo " & _
                             ", ISNULL(SUM(#TrainingCurr.trainingcostemp),0.00) AS CurrentRequestedAmt " & _
                             ", ISNULL(F.CurrentPendingRequestedAmt, 0.00) AS CurrentPendingRequestedAmt " & _
                             ", ISNULL(C.CurrentApprovedAmt,0.00) AS CurrentApprovedAmt " & _
                             ", ISNULL(D.PreviousEnrolledAmt,0.00) AS PreviousEnrolledAmt " & _
                             ", ISNULL(E.CurrentEnrolledAmt,0.00) AS CurrentEnrolledAmt " & _
                             ", ISNULL(A.Prevmaxbudget_amt, 0) AS Prevmaxbudget_amt " & _
                             ", ISNULL(trdept_maxbudget.maxbudget_amt, 0) AS Currmaxbudget_amt "

                If xTrainingRemainingBalanceBasedOnID = enTrainingRemainingBalanceBasedOn.Approved_Amount Then
                    StrQ &= ", ISNULL(A.Prevmaxbudget_amt, 0) - ISNULL(B.PreviousApprovedAmt,0.00) AS PrevRemainingBalance " & _
                            ", ISNULL(trdept_maxbudget.maxbudget_amt, 0) - ISNULL(C.CurrentApprovedAmt,0.00) AS CurrRemainingBalance "
                Else
                    StrQ &= ", ISNULL(A.Prevmaxbudget_amt, 0) - ISNULL(D.PreviousEnrolledAmt,0.00) AS PrevRemainingBalance " & _
                            ", ISNULL(trdept_maxbudget.maxbudget_amt, 0) - ISNULL(E.CurrentEnrolledAmt,0.00) AS CurrRemainingBalance "
                End If

                'StrQ &= " LEFT JOIN " & strTable & " ON " & strTable & "." & strUnkIdField & " = trdept_maxbudget.allocationtranunkid "

                StrQ &= " FROM #TrainingCurr " & _
                       " FULL OUTER JOIN " & _
                       " ( " & _
                       "        SELECT #TrainingPrev.allocationid, #TrainingPrev.departmentunkid " & _
                       "               ,#TrainingPrev.allocationtrancode " & _
                       "               ,#TrainingPrev.allocationtranname " & _
                       "               ,#TrainingPrev.periodunkid " & _
                       "               ,#TrainingPrev.Period " & _
                       "          , '' AS PreviousRefNo " & _
                       "          , ISNULL(SUM(#TrainingPrev.trainingcostemp),0.00) AS PreviousRequestedAmt " & _
                       "          , ISNULL(trdept_maxbudget.maxbudget_amt, 0) AS Prevmaxbudget_amt " & _
                       "       FROM #TrainingPrev  " & _
                       "        LEFT JOIN trdept_maxbudget ON trdept_maxbudget.allocationid = #TrainingPrev.allocationid AND #TrainingPrev.departmentunkid = trdept_maxbudget.allocationtranunkid AND #TrainingPrev.periodunkid = trdept_maxbudget.periodunkid " & _
                                    "AND trdept_maxbudget.isvoid = 0 " & _
                           "       WHERE 1 = 1 "
                'Hemant (03 Feb 2023) -- [allocationtrancode,allocationtranname,periodunkid,Period]
                'Sohail (03 Mar 2022) -- Start
                'Enhancement :  OLD-553 : NMB - New Report "Training Request Budget Report" : Training Budgets Actuals and Remaining Balances Report.
                If xBudgetAllocationTranUnkId > 0 Then
                    StrQ &= " AND #TrainingPrev.departmentunkid = @allocationtranunkid "
                End If
                'Sohail (03 Mar 2022) -- End

                StrQ &= "       GROUP BY #TrainingPrev.allocationid " & _
                        "                ,#TrainingPrev.allocationtrancode " & _
                        "                ,#TrainingPrev.allocationtranname " & _
                       "       , #TrainingPrev.departmentunkid " & _
                       "                ,#TrainingPrev.periodunkid " & _
                       "                ,#TrainingPrev.Period " & _
                       "       , ISNULL(trdept_maxbudget.maxbudget_amt, 0) " & _
                       " ) AS A on A.allocationid = #TrainingCurr.allocationid AND A.departmentunkid = #TrainingCurr.departmentunkid " & _
                       " LEFT JOIN " & _
                       " ( " & _
                       "        SELECT #TrainingCurr.allocationid, #TrainingCurr.departmentunkid " & _
                       "          , ISNULL(SUM(#TrainingCurr.trainingcostemp),0.00) AS CurrentPendingRequestedAmt " & _
                       "       FROM #TrainingCurr  " & _
                           "       WHERE #TrainingCurr.statusunkid = " & clstraining_requisition_approval_master.enApprovalStatus.SubmitForApproval & " "
                'Hemant (03 Feb 2023) -- [#TrainingPrev.allocationtrancode,#TrainingPrev.allocationtranname,#TrainingPrev.periodunkid,#TrainingPrev.period]
                If xBudgetAllocationTranUnkId > 0 Then
                    StrQ &= " AND #TrainingCurr.departmentunkid = @allocationtranunkid "
                End If

                StrQ &= "       GROUP BY #TrainingCurr.allocationid " & _
                       "       , #TrainingCurr.departmentunkid " & _
                       " ) AS F on F.allocationid = #TrainingCurr.allocationid AND F.departmentunkid = #TrainingCurr.departmentunkid " & _
                       " LEFT JOIN  #PrevApprovedAmt AS B ON B.allocationid = #TrainingCurr.allocationid AND B.departmentunkid = #TrainingCurr.departmentunkid " & _
                       " LEFT JOIN  #CurrApprovedAmt AS C on C.allocationid = #TrainingCurr.allocationid AND C.departmentunkid = #TrainingCurr.departmentunkid " & _
                       " LEFT JOIN  #PrevEnroll AS D ON D.allocationid = #TrainingCurr.allocationid AND D.departmentunkid = #TrainingCurr.departmentunkid " & _
                       " LEFT JOIN  #CurrEnroll AS E ON E.allocationid = #TrainingCurr.allocationid AND E.departmentunkid = #TrainingCurr.departmentunkid " & _
                       " LEFT JOIN trdept_maxbudget ON trdept_maxbudget.allocationid = #TrainingCurr.allocationid AND #TrainingCurr.departmentunkid = trdept_maxbudget.allocationtranunkid AND #TrainingCurr.periodunkid = trdept_maxbudget.periodunkid " & _
                                "AND trdept_maxbudget.isvoid = 0 " & _
                       " WHERE 1 = 1 "

                'Sohail (03 Mar 2022) -- Start
                'Enhancement :  OLD-553 : NMB - New Report "Training Request Budget Report" : Training Budgets Actuals and Remaining Balances Report.
                If xBudgetAllocationTranUnkId > 0 Then
                    StrQ &= " AND #TrainingCurr.departmentunkid = @allocationtranunkid "
                End If
                'Sohail (03 Mar 2022) -- End

                StrQ &= " GROUP BY ISNULL(#TrainingCurr.Period, A.Period), ISNULL(#TrainingCurr.allocationid, A.allocationid), ISNULL(#TrainingCurr.allocationtranname, A.allocationtranname), ISNULL(#TrainingCurr.allocationtrancode, A.allocationtrancode), ISNULL(A.PreviousRefNo,'') , ISNULL(A.PreviousRequestedAmt,0.00),  ISNULL(B.PreviousApprovedAmt,0.00) " & _
                       ", ISNULL(C.CurrentApprovedAmt,0.00), ISNULL(D.PreviousEnrolledAmt,0.00),ISNULL(E.CurrentEnrolledAmt,0.00), ISNULL(F.CurrentPendingRequestedAmt, 0.00) " & _
                       ", ISNULL(#TrainingCurr.departmentunkid, A.departmentunkid), ISNULL(A.Prevmaxbudget_amt, 0), ISNULL(trdept_maxbudget.maxbudget_amt, 0) "
                'Hemant (03 Feb 2023) -- [#TrainingCurr.Period-->ISNULL(#TrainingCurr.Period, A.Period), #TrainingCurr.allocationid-->ISNULL(#TrainingCurr.allocationid, A.allocationid), #TrainingCurr.allocationtranname-->ISNULL(#TrainingCurr.allocationtranname, A.allocationtranname), #TrainingCurr.allocationtrancode-->ISNULL(#TrainingCurr.allocationtrancode, A.allocationtrancode), #TrainingCurr.departmentunkid-->ISNULL(#TrainingCurr.departmentunkid, A.departmentunkid)]

                StrQ &= " DROP TABLE #TableEmpPrev " & _
                   " DROP TABLE #TableEmpCurr " & _
                   " DROP TABLE #TableEmpCCPrev " & _
                   " DROP TABLE #TableEmpCCCurr " & _
                   " DROP TABLE #TrainingPrev " & _
                   " DROP TABLE #TrainingCurr "

                If xTrainingCostCenterAllocationID > 0 Then
                    StrQ &= " DROP TABLE #CCMappingPrev " & _
                            " DROP TABLE #CCMappingCurr "
                End If
            End If



            StrQ &= " DROP TABLE #PrevApprovedAmt " & _
                    " DROP TABLE #CurrApprovedAmt " & _
                    " DROP TABLE #PrevEnroll " & _
                    " DROP TABLE #CurrEnroll "

            objDataOperation.ClearParameters()
            objDataOperation.AddParameter("@CurPeriodId", SqlDbType.Int, eZeeDataType.INT_SIZE, xCurrentPeriodId)
            objDataOperation.AddParameter("@PrePeriodId", SqlDbType.Int, eZeeDataType.INT_SIZE, xPreviousPeriodId)
            objDataOperation.AddParameter("@PrevEnddate", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, eZeeDate.convertDate(dtPrevEndDate))
            objDataOperation.AddParameter("@CurrEnddate", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, eZeeDate.convertDate(dtCurrEndDate))
            objDataOperation.AddParameter("@AllocationId", SqlDbType.Int, eZeeDataType.INT_SIZE, xAllocationId)
            objDataOperation.AddParameter("@Company", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, " " & Language.getMessage("frmDepartmentalTrainingNeedsList", 82, "Company")) 'Sohail (06 May 2021)
            'Sohail (03 Mar 2022) -- Start
            'Enhancement :  OLD-553 : NMB - New Report "Training Request Budget Report" : Training Budgets Actuals and Remaining Balances Report.
            If xBudgetAllocationTranUnkId > 0 Then
                objDataOperation.AddParameter("@allocationtranunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, xBudgetAllocationTranUnkId)
            End If
            'Sohail (03 Mar 2022) -- End


            dsList = objDataOperation.ExecQuery(StrQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                Throw New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
            End If

            dtTable = dsList.Tables(0)

            'Sohail (03 Mar 2022) -- Start
            'Enhancement :  OLD-553 : NMB - New Report "Training Request Budget Report" : Training Budgets Actuals and Remaining Balances Report.
            If blnIncludePendingBudgetAllocation = True Then
                Dim strAllocationTranIDs As String = String.Join(",", (From p In dtTable Select (p.Item("departmentunkid").ToString)).ToArray)
                If strAllocationTranIDs.Trim = "" Then
                    strAllocationTranIDs = "-8888"
                End If

                StrQ = "SELECT trdept_maxbudget.allocationid " & _
                         ", trdept_maxbudget.allocationtranunkid as departmentunkid " & _
                         ", " & strTable & "." & strCodeField & " AS allocationtrancode " & _
                         ", " & strTable & "." & strNameField & " AS allocationtranname " & _
                         ", trtraining_calendar_master.calendar_name AS Period " & _
                         ", '' AS PreviousRefNo " & _
                         ", 0.00 AS PreviousRequestedAmt " & _
                         ", 0.00 AS PreviousApprovedAmt " & _
                         ", '' AS CurrentRefNo " & _
                         ", 0.00 AS CurrentRequestedAmt " & _
                         ", 0.00 AS CurrentPendingRequestedAmt " & _
                         ", 0.00 AS CurrentApprovedAmt " & _
                         ", 0.00 AS PreviousEnrolledAmt " & _
                         ", 0.00 AS CurrentEnrolledAmt " & _
                         ", ISNULL(A.maxbudget_amt, 0) AS Prevmaxbudget_amt " & _
                         ", trdept_maxbudget.maxbudget_amt AS Currmaxbudget_amt " & _
                    "FROM trdept_maxbudget " & _
                        "FULL OUTER JOIN " & _
                        "( " & _
                            "SELECT trdept_maxbudget.periodunkid " & _
                                 ", trdept_maxbudget.allocationid " & _
                                 ", trdept_maxbudget.allocationtranunkid " & _
                                 ", trdept_maxbudget.maxbudget_amt " & _
                            "FROM trdept_maxbudget " & _
                                "LEFT JOIN trtraining_calendar_master " & _
                                    "ON trdept_maxbudget.periodunkid = trtraining_calendar_master.calendarunkid " & _
                            "WHERE trdept_maxbudget.isvoid = 0 " & _
                                  "AND trdept_maxbudget.periodunkid = @PrePeriodId " & _
                                  "AND trdept_maxbudget.allocationid = " & xBudgetAllocationId & " " & _
                                  "AND trdept_maxbudget.allocationtranunkid NOT IN ( " & strAllocationTranIDs & " ) "

                If xBudgetAllocationTranUnkId > 0 Then
                    StrQ &= " AND trdept_maxbudget.allocationtranunkid = @allocationtranunkid "
                End If

                StrQ &= ") AS A " & _
                            "ON A.allocationid = trdept_maxbudget.allocationid " & _
                               "AND A.allocationtranunkid = trdept_maxbudget.allocationtranunkid " & _
                        "LEFT JOIN trtraining_calendar_master " & _
                            "ON trdept_maxbudget.periodunkid = trtraining_calendar_master.calendarunkid " & _
                        "LEFT JOIN " & strTable & " ON " & strTable & "." & strUnkIdField & " = trdept_maxbudget.allocationtranunkid " & _
                    "WHERE trdept_maxbudget.isvoid = 0 " & _
                          "AND trdept_maxbudget.periodunkid = @CurPeriodId " & _
                          "AND trdept_maxbudget.allocationid = " & xBudgetAllocationId & " " & _
                          "AND trdept_maxbudget.allocationtranunkid NOT IN ( " & strAllocationTranIDs & " ) "

                If xBudgetAllocationTranUnkId > 0 Then
                    StrQ &= " AND trdept_maxbudget.allocationtranunkid = @allocationtranunkid "
                End If

                Dim ds As DataSet = objDataOperation.ExecQuery(StrQ, "List")

                If objDataOperation.ErrorMessage <> "" Then
                    Throw New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                End If

                If ds.Tables(0).Rows.Count > 0 Then
                    dtTable.Merge(ds.Tables(0))
                End If

                dtTable = New DataView(dtTable, "", "Period, allocationid, allocationtranname, allocationtrancode, PreviousRefNo, PreviousRequestedAmt, PreviousApprovedAmt, CurrentApprovedAmt", DataViewRowState.CurrentRows).ToTable

            End If
            'Sohail (03 Mar 2022) -- End

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: GetBudgetSummaryData; Module Name: " & mstrModuleName)
        End Try
        Return dtTable
    End Function

    Public Function IsValidForAllocationChange(ByVal strDatabaseName As String _
                                               , ByVal intAllocationID As Integer _
                                               , ByVal intPeriodUnkId As Integer _
                                               , Optional ByVal xDataOpr As clsDataOperation = Nothing) As Boolean
        Dim strQ As String = ""
        Dim dsList As New DataSet
        Dim exForce As Exception

        If xDataOpr Is Nothing Then
            objDataOperation = New clsDataOperation
        Else
            objDataOperation = xDataOpr
        End If

        Try
            Dim strDBName As String = If(strDatabaseName.Trim <> "", strDatabaseName & "..", "")

            strQ = "SELECT 1 " & _
                    "FROM " & strDBName & "trdepartmentaltrainingneed_master " & _
                    "WHERE trdepartmentaltrainingneed_master.isvoid = 0 " & _
                    "AND trdepartmentaltrainingneed_master.statusunkid NOT IN ( " & enApprovalStatus.FinalApproved & ", " & enApprovalStatus.Rejected & " ) "

            objDataOperation.ClearParameters()
            If intAllocationID > 0 Then
                strQ &= "AND trdepartmentaltrainingneed_master.allocationid = @allocationid "
                objDataOperation.AddParameter("@allocationid", SqlDbType.Int, eZeeDataType.INT_SIZE, intAllocationID)
            End If

            If intPeriodUnkId > 0 Then
                strQ &= "AND trdepartmentaltrainingneed_master.periodunkid = @periodunkid "
                objDataOperation.AddParameter("@periodunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intPeriodUnkId)
            End If

            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                Throw New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
            End If

            If dsList.Tables(0).Rows.Count > 0 Then
                Return False
            Else
                Return True
            End If

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: IsValidForAllocationChange; Module Name: " & mstrModuleName)
            Return False
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            If xDataOpr Is Nothing Then objDataOperation = Nothing
        End Try
    End Function

    Public Function IsValidForBudgetAllocationChange(ByVal strDatabaseName As String _
                                                     , ByVal intAllocationID As Integer _
                                                     , ByVal intPeriodUnkId As Integer _
                                                     , Optional ByVal xDataOpr As clsDataOperation = Nothing) As Boolean
        Dim strQ As String = ""
        Dim dsList As New DataSet
        Dim exForce As Exception

        If xDataOpr Is Nothing Then
            objDataOperation = New clsDataOperation
        Else
            objDataOperation = xDataOpr
        End If

        Try
            Dim strDBName As String = If(strDatabaseName.Trim <> "", strDatabaseName & "..", "")

            strQ = "SELECT 1 " & _
                    "FROM " & strDBName & "trdepartmentaltrainingneed_master " & _
                    "WHERE trdepartmentaltrainingneed_master.isvoid = 0 " & _
                    "AND trdepartmentaltrainingneed_master.statusunkid NOT IN ( " & enApprovalStatus.Pending & ", " & enApprovalStatus.SubmittedForApprovalFromDeptTrainingNeed & ", " & enApprovalStatus.FinalApproved & ", " & enApprovalStatus.Rejected & " ) "

            objDataOperation.ClearParameters()
            If intAllocationID > 0 Then
                strQ &= "AND trdepartmentaltrainingneed_master.allocationid = @allocationid "
                objDataOperation.AddParameter("@allocationid", SqlDbType.Int, eZeeDataType.INT_SIZE, intAllocationID)
            End If

            If intPeriodUnkId > 0 Then
                strQ &= "AND trdepartmentaltrainingneed_master.periodunkid = @periodunkid "
                objDataOperation.AddParameter("@periodunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intPeriodUnkId)
            End If

            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                Throw New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
            End If

            If dsList.Tables(0).Rows.Count > 0 Then
                Return False
            Else
                Return True
            End If

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: IsValidForBudgetAllocationChange; Module Name: " & mstrModuleName)
            Return False
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            If xDataOpr Is Nothing Then objDataOperation = Nothing
        End Try
    End Function
    'Sohail (10 Feb 2022) -- End

    Public Function GetTrainingNeedListForOtherModule(ByVal intModuletranunkid As Integer, _
                                                       ByVal intTrainingUnkid As Integer, _
                                                       ByVal intModuleid As Integer, _
                                                       ByVal intTrainingcategoryunkid As Integer, _
                                                       ByVal intEmployeeUnkid As Integer, _
                                                       Optional ByVal blnGetWithoutPendingStatus As Boolean = False, _
                                                       Optional ByVal xDataOpr As clsDataOperation = Nothing) As DataSet
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        If xDataOpr Is Nothing Then
            objDataOperation = New clsDataOperation
        Else
            objDataOperation = xDataOpr
        End If
        objDataOperation.ClearParameters()

        Try
            strQ = "SELECT " & _
               "  trdepartmentaltrainingneed_master.departmentaltrainingneedunkid " & _
               " ,trdepartmentaltrainingneed_master.statusunkid " & _
               " FROM trdepartmentaltrainingneed_master  "


            If intEmployeeUnkid > 0 Then
                strQ &= " JOIN trdepttrainingneed_employee_tran " & _
                        " ON trdepartmentaltrainingneed_master.departmentaltrainingneedunkid = trdepttrainingneed_employee_tran.departmentaltrainingneedunkid "
            End If



            strQ &= " where trdepartmentaltrainingneed_master.isvoid = 0 "

            If intEmployeeUnkid > 0 Then
                strQ &= " and trdepttrainingneed_employee_tran.employeeunkid= @employeeunkid and trdepttrainingneed_employee_tran.isvoid = 0 "
                objDataOperation.AddParameter("@employeeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intEmployeeUnkid)
            End If


            If intTrainingUnkid > 0 Then
                strQ &= " and trdepartmentaltrainingneed_master.trainingcourseunkid = @trainingcourseunkid "
                objDataOperation.AddParameter("@trainingcourseunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intTrainingUnkid)
            End If

            If blnGetWithoutPendingStatus Then
                strQ &= " and trdepartmentaltrainingneed_master.statusunkid <> @statusunkid "
                objDataOperation.AddParameter("@statusunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, CInt(clsDepartmentaltrainingneed_master.enApprovalStatus.Pending))
            End If


            If intModuletranunkid > 0 Then
                strQ &= " and trdepartmentaltrainingneed_master.moduletranunkid = @moduletranunkid "
                objDataOperation.AddParameter("@moduletranunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intModuletranunkid)
            End If


            If intModuleid > 0 Then
                strQ &= " and trdepartmentaltrainingneed_master.moduleid = @moduleid "
                objDataOperation.AddParameter("@moduleid", SqlDbType.Int, eZeeDataType.INT_SIZE, intModuleid)
            End If


            If intTrainingcategoryunkid > 0 Then
                strQ &= " and trainingcategoryunkid = @trainingcategoryunkid "
                objDataOperation.AddParameter("@trainingcategoryunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intTrainingcategoryunkid)
            End If


            dsList = objDataOperation.ExecQuery(strQ, "DepartmentTrainingList")
            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            Return dsList
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: GetTrainingNeedListForOtherModule; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            If xDataOpr Is Nothing Then objDataOperation = Nothing
        End Try
    End Function

    'Hemant (03 Dec 2021) -- Start
    'ENHANCEMENT : OLD-500 - Allow Multiple Training Calendars.
    Public Function GetTrainingRequestedStaffCount(ByVal strTableName As String, ByVal intPeriodUnkid As Integer) As DataSet
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        objDataOperation = New clsDataOperation

        Try
            strQ = " SELECT " & _
                   "    trdepartmentaltrainingneed_master.departmentaltrainingneedunkid " & _
                   ",   trdepartmentaltrainingneed_master.trainingcourseunkid " & _
                   ",   CONVERT(CHAR(8), trdepartmentaltrainingneed_master.startdate, 112) AS startdate " & _
                   ",   CONVERT(CHAR(8), trdepartmentaltrainingneed_master.enddate, 112) AS enddate " & _
                   ",   ISNULL(cfcommon_master.name, '') AS Training " & _
                   ",   ISNULL(trdepartmentaltrainingneed_master.trainingcategoryunkid, 0 ) AS trainingcategoryunkid " & _
                   ",   ISNULL(trtrainingcategory_master.categoryname, '') AS TrainingCategory " & _
                   ",   noofstaff AS NoOfStaff " & _
                   ",   ISNULL(A.Count, 0) AS RequestedCount " & _
                   " FROM trdepartmentaltrainingneed_master " & _
                   " LEFT JOIN cfcommon_master ON cfcommon_master.masterunkid = trdepartmentaltrainingneed_master.trainingcourseunkid " & _
                   "                        AND cfcommon_master.isactive = 1 " & _
                   "                        AND cfcommon_master.mastertype = " & clsCommon_Master.enCommonMaster.TRAINING_COURSEMASTER & " " & _
                   " LEFT JOIN trtrainingcategory_master on trtrainingcategory_master.categoryunkid = trdepartmentaltrainingneed_master.trainingcategoryunkid " & _
                   "                        AND trtrainingcategory_master.isactive = 1 " & _
                   " LEFT JOIN (    SELECT " & _
                   "                    departmentaltrainingneedunkid " & _
                   ",                   COUNT(*) AS Count " & _
                   "                FROM trtraining_request_master " & _
                   "                WHERE isvoid = 0 " & _
                   "                        AND periodunkid = @periodunkid " & _
                   "                        AND departmentaltrainingneedunkid > 0 " & _
                   "                GROUP BY departmentaltrainingneedunkid) AS A " & _
                   " ON A.departmentaltrainingneedunkid = trdepartmentaltrainingneed_master.departmentaltrainingneedunkid " & _
                   " WHERE isvoid = 0 " & _
                   "    AND trdepartmentaltrainingneed_master.statusunkid = " & enApprovalStatus.FinalApproved & " " & _
                   "    AND periodunkid = @periodunkid "

            objDataOperation.AddParameter("@periodunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intPeriodUnkid)

            dsList = objDataOperation.ExecQuery(strQ, strTableName)

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: GetTrainingRequestedStaffCount; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
        End Try
        Return dsList
    End Function
    'Hemant (03 Dec 2021) -- End


    '<Language> This Auto Generated Text Please Do Not Modify it.
#Region " Language & UI Settings "
    Public Shared Sub SetMessages()
        Try
            Language.setMessage(mstrModuleName, 1, "Learning Method")
            Language.setMessage(mstrModuleName, 2, "Training Provider")
            Language.setMessage(mstrModuleName, 3, "Training Venue")
            Language.setMessage(mstrModuleName, 4, "Training Resources Needed")
            Language.setMessage(mstrModuleName, 5, "Financing Source")
            Language.setMessage(mstrModuleName, 6, "Training Coordinator")
            Language.setMessage(mstrModuleName, 7, "Training Certificate Required")
            Language.setMessage(mstrModuleName, 8, "Comment Remark")
            Language.setMessage(mstrModuleName, 9, "Training Cost Item")
            Language.setMessage(mstrModuleName, 10, "Amount")
            Language.setMessage(mstrModuleName, 11, "Select")
            Language.setMessage(mstrModuleName, 12, "Pending")
            Language.setMessage(mstrModuleName, 13, "Submitted For Approval From Departmental Training Need")
            Language.setMessage(mstrModuleName, 14, "Tentative Approved")
            Language.setMessage(mstrModuleName, 15, "Submitted For Approval From Training Backlog")
            Language.setMessage(mstrModuleName, 16, "Approved")
            Language.setMessage(mstrModuleName, 17, "Rejected")
            Language.setMessage(mstrModuleName, 18, "Asked For Review As Per Amount Set")
            Language.setMessage(mstrModuleName, 19, "Employee Names")
            Language.setMessage(mstrModuleName, 20, "Notification: Departmental Training Needs Approval/Rejection")
            Language.setMessage(mstrModuleName, 21, "Select")
            Language.setMessage(mstrModuleName, 22, "On Hold")
            Language.setMessage(mstrModuleName, 23, "Postponed")
            Language.setMessage(mstrModuleName, 24, "Cancelled")
            Language.setMessage(mstrModuleName, 25, "Dear")
            Language.setMessage(mstrModuleName, 26, "This is to notify you that the Training Needs for #AllocationName# are ready for your approval/rejection.")
            Language.setMessage(mstrModuleName, 27, "Please login to Aruti or click on the link below to approve/reject.")
            Language.setMessage(mstrModuleName, 28, "Please click on the following link to approve/reject Department Training Need.")
            Language.setMessage(mstrModuleName, 29, "Notification: Backlog Training Needs Approval/Rejection")
            Language.setMessage(mstrModuleName, 30, "Dear")
            Language.setMessage(mstrModuleName, 31, "This is to notify you that Training backlogs are tentatively approved and ready for your final approval/rejection.")
            Language.setMessage(mstrModuleName, 32, "Please login to Aruti or click on the link below to approve/reject.")
            Language.setMessage(mstrModuleName, 33, "Please click on the following link to approve/reject Department Training Need.")
            Language.setMessage(mstrModuleName, 34, "Select")
            Language.setMessage(mstrModuleName, 35, "Select")
            Language.setMessage(mstrModuleName, 36, "Dear")
            Language.setMessage(mstrModuleName, 37, "Please be informed that the plan you submitted for #TargetedGroupName# with the program name #TrainingName# has been unlocked for changes as per the remarks below.")
            Language.setMessage(mstrModuleName, 38, "Remarks: #UnlockReason#.")
            Language.setMessage(mstrModuleName, 39, "Notification: Training has been planned")
            Language.setMessage(mstrModuleName, 40, "Dear")
            Language.setMessage(mstrModuleName, 41, "This is to inform you that the following program has been planned for #TargetedGroupName# on #TrainingStartDate#. If you wish to apply, please log in to your self-service.")
            Language.setMessage(mstrModuleName, 42, "Program Name: #TrainingName#.")
            Language.setMessage(mstrModuleName, 43, "Start Date: #TrainingStartDate#.")
            Language.setMessage(mstrModuleName, 44, "End Date: #TrainingEndDate#.")
            Language.setMessage(mstrModuleName, 45, "Dear")
            Language.setMessage(mstrModuleName, 46, "Please be informed that the plan you submitted for #TargetedGroupName# with the program name #TrainingName# has been approved.")
            Language.setMessage(mstrModuleName, 47, "Remarks: #ApprovalRejectionRemark#.")
            Language.setMessage(mstrModuleName, 48, "Dear")
            Language.setMessage(mstrModuleName, 49, "Please be informed that the plan you submitted for #TargetedGroupName# with the program name #TrainingName# has been rejected.")
            Language.setMessage(mstrModuleName, 50, "Remarks: #ApprovalRejectionRemark#.")
            Language.setMessage(mstrModuleName, 51, "Notification: Submitted Training Needs unlocked")
            Language.setMessage(mstrModuleName, 52, "Notification: Training has been approved")
            Language.setMessage(mstrModuleName, 53, "Notification: Training has been rejected")
            Language.setMessage(mstrModuleName, 54, "Please be informed that the plan you submitted for approval with the program name #TrainingName# has been unlocked for changes as per the remarks below.")
            Language.setMessage(mstrModuleName, 55, "This is to inform you that the following program has been planned to start on #TrainingStartDate#. If you wish to participate, please log in to your self-service to apply.")
            Language.setMessage(mstrModuleName, 56, "Please be informed that the plan you submitted for approval with the program name #TrainingName# has been approved.")
            Language.setMessage(mstrModuleName, 57, "Please be informed that the plan you submitted for approval with the program name #TrainingName# has been rejected.")
            Language.setMessage("frmDepartmentalTrainingNeedsList", 82, "Company")

        Catch Ex As Exception
            Throw New Exception(Ex.Message & "; Procedure Name: SetMessages; Module Name: " & mstrModuleName)
        End Try
    End Sub
#End Region 'Language & UI Settings
    '</Language>
End Class