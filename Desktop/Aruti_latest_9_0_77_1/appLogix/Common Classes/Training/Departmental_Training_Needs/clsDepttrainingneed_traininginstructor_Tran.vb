﻿'************************************************************************************************************************************
'Class Name : clsDepttrainingneed_traininginstructor_Tran.vb
'Purpose    :
'Date       :09-06-2021
'Written By :Suhail
'Modified   :
'************************************************************************************************************************************

Imports eZeeCommonLib
''' <summary>
''' Purpose: 
''' Developer: Suhail
''' </summary>
Public Class clsDepttrainingneed_traininginstructor_Tran
    Private Const mstrModuleName = "clsDepttrainingneed_traininginstructor_Tran"
    Dim objDataOperation As clsDataOperation
    Dim mstrMessage As String = ""

#Region " Private variables "
    Private mintDepttrainingneedtraininginstructortranunkid As Integer
    Private mintDepartmentaltrainingneedunkid As Integer
    Private mintEmployeeunkid As Integer
    Private mstrOthers_name As String = String.Empty
    Private mstrOthers_company As String = String.Empty
    Private mstrOthers_department As String = String.Empty
    Private mstrOthers_job As String = String.Empty
    Private mstrOthers_email As String = String.Empty
    Private mintUserunkid As Integer
    Private mintLoginemployeeunkid As Integer
    Private mblnIsweb As Boolean
    Private mblnIsvoid As Boolean
    Private mintVoiduserunkid As Integer
    Private mintVoidloginemployeeunkid As Integer
    Private mdtVoiddatetime As Date
    Private mstrVoidreason As String = String.Empty
#End Region

#Region " Public Variables "
    Public pintDepttrainingneedtraininginstructortranunkid As Integer
    Public pintDepartmentaltrainingneedunkid As Integer
    Public pintEmployeeunkid As Integer
    Public pstrOthers_name As String = String.Empty
    Public pstrOthers_company As String = String.Empty
    Public pstrOthers_department As String = String.Empty
    Public pstrOthers_job As String = String.Empty
    Public pstrOthers_email As String = String.Empty
    Public pintUserunkid As Integer
    Public pintLoginemployeeunkid As Integer
    Public pblnIsweb As Boolean
    Public pblnIsvoid As Boolean
    Public pintVoiduserunkid As Integer
    Public pintVoidloginemployeeunkid As Integer
    Public pdtVoiddatetime As Date
    Public pstrVoidreason As String = String.Empty

    Public pintAuditUserId As Integer = 0
    Public pdtAuditDate As DateTime
    Public pstrClientIp As String = ""
    Public pstrHostName As String = ""
    Public pstrFormName As String = ""
#End Region

#Region " Properties "
    ''' <summary>
    ''' Purpose: Get Message from Class 
    ''' Modify By: Suhail
    ''' </summary>
    Public ReadOnly Property _Message() As String
        Get
            Return mstrMessage
        End Get
    End Property

    Private mDataOp As clsDataOperation = Nothing
    Public Property _DataOp() As clsDataOperation
        Get
            Return mDataOp
        End Get
        Set(ByVal value As clsDataOperation)
            mDataOp = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set depttrainingneedtraininginstructortranunkid
    ''' Modify By: Suhail
    ''' </summary>
    Public Property _Depttrainingneedtraininginstructortranunkid() As Integer
        Get
            Return mintDepttrainingneedtraininginstructortranunkid
        End Get
        Set(ByVal value As Integer)
            mintDepttrainingneedtraininginstructortranunkid = value
            Call GetData()
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set departmentaltrainingneedunkid
    ''' Modify By: Suhail
    ''' </summary>
    Public Property _Departmentaltrainingneedunkid() As Integer
        Get
            Return mintDepartmentaltrainingneedunkid
        End Get
        Set(ByVal value As Integer)
            mintDepartmentaltrainingneedunkid = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set employeeunkid
    ''' Modify By: Suhail
    ''' </summary>
    Public Property _Employeeunkid() As Integer
        Get
            Return mintEmployeeunkid
        End Get
        Set(ByVal value As Integer)
            mintEmployeeunkid = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set others_name
    ''' Modify By: Suhail
    ''' </summary>
    Public Property _Others_name() As String
        Get
            Return mstrOthers_name
        End Get
        Set(ByVal value As String)
            mstrOthers_name = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set others_company
    ''' Modify By: Suhail
    ''' </summary>
    Public Property _Others_company() As String
        Get
            Return mstrOthers_company
        End Get
        Set(ByVal value As String)
            mstrOthers_company = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set others_department
    ''' Modify By: Suhail
    ''' </summary>
    Public Property _Others_department() As String
        Get
            Return mstrOthers_department
        End Get
        Set(ByVal value As String)
            mstrOthers_department = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set others_job
    ''' Modify By: Suhail
    ''' </summary>
    Public Property _Others_job() As String
        Get
            Return mstrOthers_job
        End Get
        Set(ByVal value As String)
            mstrOthers_job = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set others_email
    ''' Modify By: Suhail
    ''' </summary>
    Public Property _Others_email() As String
        Get
            Return mstrOthers_email
        End Get
        Set(ByVal value As String)
            mstrOthers_email = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set userunkid
    ''' Modify By: Suhail
    ''' </summary>
    Public Property _Userunkid() As Integer
        Get
            Return mintUserunkid
        End Get
        Set(ByVal value As Integer)
            mintUserunkid = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set loginemployeeunkid
    ''' Modify By: Suhail
    ''' </summary>
    Public Property _Loginemployeeunkid() As Integer
        Get
            Return mintLoginemployeeunkid
        End Get
        Set(ByVal value As Integer)
            mintLoginemployeeunkid = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set isweb
    ''' Modify By: Suhail
    ''' </summary>
    Public Property _Isweb() As Boolean
        Get
            Return mblnIsweb
        End Get
        Set(ByVal value As Boolean)
            mblnIsweb = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set isvoid
    ''' Modify By: Suhail
    ''' </summary>
    Public Property _Isvoid() As Boolean
        Get
            Return mblnIsvoid
        End Get
        Set(ByVal value As Boolean)
            mblnIsvoid = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set voiduserunkid
    ''' Modify By: Suhail
    ''' </summary>
    Public Property _Voiduserunkid() As Integer
        Get
            Return mintVoiduserunkid
        End Get
        Set(ByVal value As Integer)
            mintVoiduserunkid = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set voidloginemployeeunkid
    ''' Modify By: Suhail
    ''' </summary>
    Public Property _Voidloginemployeeunkid() As Integer
        Get
            Return mintVoidloginemployeeunkid
        End Get
        Set(ByVal value As Integer)
            mintVoidloginemployeeunkid = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set voiddatetime
    ''' Modify By: Suhail
    ''' </summary>
    Public Property _Voiddatetime() As Date
        Get
            Return mdtVoiddatetime
        End Get
        Set(ByVal value As Date)
            mdtVoiddatetime = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set voidreason
    ''' Modify By: Suhail
    ''' </summary>
    Public Property _Voidreason() As String
        Get
            Return mstrVoidreason
        End Get
        Set(ByVal value As String)
            mstrVoidreason = value
        End Set
    End Property


    Private mintAuditUserId As Integer = 0
    Public Property _AuditUserId() As Integer
        Get
            Return mintAuditUserId
        End Get
        Set(ByVal value As Integer)
            mintAuditUserId = value
        End Set
    End Property

    Private mdtAuditDate As DateTime
    Public Property _AuditDate() As DateTime
        Get
            Return mdtAuditDate
        End Get
        Set(ByVal value As DateTime)
            mdtAuditDate = value
        End Set
    End Property

    Private mstrClientIp As String = ""
    Public Property _ClientIP() As String
        Get
            Return mstrClientIp
        End Get
        Set(ByVal value As String)
            mstrClientIp = value
        End Set
    End Property

    Private mstrHostName As String = ""
    Public Property _HostName() As String
        Get
            Return mstrHostName
        End Get
        Set(ByVal value As String)
            mstrHostName = value
        End Set
    End Property

    Private mstrFormName As String = ""
    Public Property _FormName() As String
        Get
            Return mstrFormName
        End Get
        Set(ByVal value As String)
            mstrFormName = value
        End Set
    End Property

#End Region

    ''' <summary>
    ''' Modify By: Suhail
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>
    Public Sub GetData()
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        If mDataOp IsNot Nothing Then
            objDataOperation = mDataOp
        Else
            objDataOperation = New clsDataOperation
        End If
        objDataOperation.ClearParameters()

        Try
            strQ = "SELECT " & _
              "  depttrainingneedtraininginstructortranunkid " & _
              ", departmentaltrainingneedunkid " & _
              ", employeeunkid " & _
              ", others_name " & _
              ", others_company " & _
              ", others_department " & _
              ", others_job " & _
              ", others_email " & _
              ", userunkid " & _
              ", loginemployeeunkid " & _
              ", isweb " & _
              ", isvoid " & _
              ", voiduserunkid " & _
              ", voidloginemployeeunkid " & _
              ", voiddatetime " & _
              ", voidreason " & _
             "FROM trdepttrainingneed_traininginstructor_tran " & _
             "WHERE depttrainingneedtraininginstructortranunkid = @depttrainingneedtraininginstructortranunkid "

            objDataOperation.AddParameter("@depttrainingneedtraininginstructortranunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintDepttrainingneedtraininginstructortranunkid.ToString)

            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            For Each dtRow As DataRow In dsList.Tables(0).Rows
                mintDepttrainingneedtraininginstructortranunkid = CInt(dtRow.Item("depttrainingneedtraininginstructortranunkid"))
                mintDepartmentaltrainingneedunkid = CInt(dtRow.Item("departmentaltrainingneedunkid"))
                mintEmployeeunkid = CInt(dtRow.Item("employeeunkid"))
                mstrOthers_name = dtRow.Item("others_name").ToString
                mstrOthers_company = dtRow.Item("others_company").ToString
                mstrOthers_department = dtRow.Item("others_department").ToString
                mstrOthers_job = dtRow.Item("others_job").ToString
                mstrOthers_email = dtRow.Item("others_email").ToString
                mintUserunkid = CInt(dtRow.Item("userunkid"))
                mintLoginemployeeunkid = CInt(dtRow.Item("loginemployeeunkid"))
                mblnIsweb = CBool(dtRow.Item("isweb"))
                mblnIsvoid = CBool(dtRow.Item("isvoid"))
                mintVoiduserunkid = CInt(dtRow.Item("voiduserunkid"))
                mintVoidloginemployeeunkid = CInt(dtRow.Item("voidloginemployeeunkid"))
                If IsDBNull(dtRow.Item("voiddatetime")) = True Then
                    mdtVoiddatetime = Nothing
                Else
                    mdtVoiddatetime = CDate(dtRow.Item("voiddatetime"))
                End If
                mstrVoidreason = dtRow.Item("voidreason").ToString
                Exit For
            Next
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: GetData; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            If mDataOp Is Nothing Then objDataOperation = Nothing
        End Try
    End Sub


    ''' <summary>
    ''' Modify By: Suhail
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>
    Public Function GetList(ByVal xDatabaseName As String, _
                            ByVal xUserUnkid As Integer, _
                            ByVal xYearUnkid As Integer, _
                            ByVal xCompanyUnkid As Integer, _
                            ByVal xPeriodStart As DateTime, _
                            ByVal xPeriodEnd As DateTime, _
                            ByVal xUserModeSetting As String, _
                            ByVal xOnlyApproved As Boolean, _
                            ByVal xIncludeIn_ActiveEmployee As Boolean, _
                            ByVal strCompanyName As String, _
                            ByVal strTableName As String, _
                            Optional ByVal blnApplyUserAccessFilter As Boolean = True, _
                            Optional ByVal strAdvanceFilter As String = "", _
                            Optional ByVal intDepartmentalTrainingNeedUnkid As Integer = -1, _
                            Optional ByVal strFilter As String = "" _
                            ) As DataSet

        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        If mDataOp IsNot Nothing Then
            objDataOperation = mDataOp
        Else
            objDataOperation = New clsDataOperation
        End If
        objDataOperation.ClearParameters()

        Try
            Dim xDateJoinQry, xDateFilterQry, xUACQry, xUACFiltrQry, xAdvanceJoinQry As String
            xDateJoinQry = "" : xDateFilterQry = "" : xUACQry = "" : xUACFiltrQry = "" : xAdvanceJoinQry = ""
            Call GetDatesFilterString(xDateJoinQry, xDateFilterQry, xPeriodStart, xPeriodEnd, , , xDatabaseName)
            If blnApplyUserAccessFilter = True Then
                Call NewAccessLevelFilterString(xUACQry, xUACFiltrQry, xPeriodEnd, xOnlyApproved, xDatabaseName, xUserUnkid, xCompanyUnkid, xYearUnkid, xUserModeSetting)
            End If
            If strAdvanceFilter.Trim <> "" Then
                Call GetAdvanceFilterQry(xAdvanceJoinQry, xPeriodEnd, xDatabaseName)
            End If

            strQ = "SELECT  hremployee_master.employeeunkid " & _
                            ", REPLACE(ISNULL(hremployee_master.firstname, '') + ' ' + ISNULL(hremployee_master.othername, '') + ' ' + ISNULL(hremployee_master.surname, ''), '  ', ' ') AS employeename  " & _
                            ", hremployee_master.employeecode " & _
                            ", ISNULL(hrdepartment_master.name , '') AS DeptName " & _
                            ", ISNULL(hrjob_master.job_name , '') AS JobName " & _
                            ", hremployee_master.email " & _
                   "INTO    #tblEmp " & _
                   "FROM    hremployee_master "

            If xDateJoinQry.Trim.Length > 0 Then
                strQ &= xDateJoinQry
            End If

            If blnApplyUserAccessFilter = True Then
                If xUACQry.Trim.Length > 0 Then
                    strQ &= xUACQry
                End If
            End If

            If xAdvanceJoinQry.Trim.Length > 0 Then
                strQ &= xAdvanceJoinQry
            End If

            strQ &= "LEFT JOIN " & _
                   "( " & _
                   "    SELECT " & _
                   "         stationunkid " & _
                   "        ,deptgroupunkid " & _
                   "        ,departmentunkid " & _
                   "        ,sectiongroupunkid " & _
                   "        ,sectionunkid " & _
                   "        ,unitgroupunkid " & _
                   "        ,unitunkid " & _
                   "        ,teamunkid " & _
                   "        ,classgroupunkid " & _
                   "        ,classunkid " & _
                   "        ,employeeunkid " & _
                   "        ,ROW_NUMBER()OVER(PARTITION BY employeeunkid ORDER BY effectivedate DESC) as Rno " & _
                   "    FROM hremployee_transfer_tran " & _
                   "    WHERE isvoid = 0 AND CONVERT(CHAR(8),effectivedate,112) <= '" & eZeeDate.convertDate(xPeriodEnd) & "' " & _
                   ") AS T ON T.employeeunkid = hremployee_master.employeeunkid AND T.Rno = 1 " & _
                   "LEFT JOIN hrdepartment_master ON hrdepartment_master.departmentunkid = T.departmentunkid  " & _
                   "LEFT JOIN " & _
                   "( " & _
                   "    SELECT " & _
                   "         jobgroupunkid " & _
                   "        ,jobunkid " & _
                   "        ,employeeunkid " & _
                   "        ,ROW_NUMBER()OVER(PARTITION BY employeeunkid ORDER BY effectivedate DESC) as Rno " & _
                   "    FROM hremployee_categorization_tran " & _
                   "    WHERE isvoid = 0 AND CONVERT(CHAR(8),effectivedate,112) <= '" & eZeeDate.convertDate(xPeriodEnd) & "' " & _
                   ") AS J ON J.employeeunkid = hremployee_master.employeeunkid AND J.Rno = 1 " & _
                   "LEFT JOIN hrjob_master ON hrjob_master.jobunkid = J.jobunkid  "

            strQ &= "WHERE 1 = 1 "

            If blnApplyUserAccessFilter = True Then
                If xUACFiltrQry.Trim.Length > 0 Then
                    strQ &= " AND " & xUACFiltrQry
                End If
            End If

            If xIncludeIn_ActiveEmployee = False Then
                If xDateFilterQry.Trim.Length > 0 Then
                    strQ &= xDateFilterQry
                End If
            End If

            If strAdvanceFilter.Trim.Length > 0 Then
                strQ &= strAdvanceFilter
            End If

            strQ &= " SELECT " & _
              "  trdepttrainingneed_traininginstructor_tran.depttrainingneedtraininginstructortranunkid " & _
              ", trdepttrainingneed_traininginstructor_tran.departmentaltrainingneedunkid " & _
              ", trdepttrainingneed_traininginstructor_tran.employeeunkid " & _
              ", CASE WHEN trdepttrainingneed_traininginstructor_tran.employeeunkid > 0 THEN ISNULL(#tblEmp.employeecode, '') ELSE '' END AS employeecode " & _
              ", CASE WHEN trdepttrainingneed_traininginstructor_tran.employeeunkid > 0 THEN ISNULL(#tblEmp.employeename, '') ELSE trdepttrainingneed_traininginstructor_tran.others_name END AS employeename " & _
              ", CASE WHEN trdepttrainingneed_traininginstructor_tran.employeeunkid > 0 THEN '" & strCompanyName & "' ELSE trdepttrainingneed_traininginstructor_tran.others_company END AS CompanyName " & _
              ", CASE WHEN trdepttrainingneed_traininginstructor_tran.employeeunkid > 0 THEN ISNULL(#tblEmp.DeptName, '') ELSE trdepttrainingneed_traininginstructor_tran.others_department END AS DeptName " & _
              ", CASE WHEN trdepttrainingneed_traininginstructor_tran.employeeunkid > 0 THEN ISNULL(#tblEmp.JobName, '') ELSE trdepttrainingneed_traininginstructor_tran.others_job END AS JobName " & _
              ", CASE WHEN trdepttrainingneed_traininginstructor_tran.employeeunkid > 0 THEN ISNULL(#tblEmp.Email, '') ELSE trdepttrainingneed_traininginstructor_tran.others_email END AS Email " & _
              ", trdepttrainingneed_traininginstructor_tran.userunkid " & _
              ", trdepttrainingneed_traininginstructor_tran.loginemployeeunkid " & _
              ", trdepttrainingneed_traininginstructor_tran.isweb " & _
              ", trdepttrainingneed_traininginstructor_tran.isvoid " & _
              ", trdepttrainingneed_traininginstructor_tran.voiduserunkid " & _
              ", trdepttrainingneed_traininginstructor_tran.voidloginemployeeunkid " & _
              ", trdepttrainingneed_traininginstructor_tran.voiddatetime " & _
              ", trdepttrainingneed_traininginstructor_tran.voidreason " & _
             "FROM trdepttrainingneed_traininginstructor_tran " & _
             "LEFT JOIN #tblEmp ON #tblEmp.employeeunkid = trdepttrainingneed_traininginstructor_tran.employeeunkid " & _
             "WHERE trdepttrainingneed_traininginstructor_tran.isvoid = 0 "

            If intDepartmentalTrainingNeedUnkid > 0 Then
                strQ &= " AND trdepttrainingneed_traininginstructor_tran.departmentaltrainingneedunkid = @departmentaltrainingneedunkid "
                objDataOperation.AddParameter("@departmentaltrainingneedunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intDepartmentalTrainingNeedUnkid)
            ElseIf intDepartmentalTrainingNeedUnkid = 0 Then
                strQ &= " AND 1 = 2 "
            End If

            If strFilter.Trim <> "" Then
                strQ &= " " & strFilter & " "
            End If

            strQ &= " DROP TABLE #tblEmp "

            dsList = objDataOperation.ExecQuery(strQ, strTableName)

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: GetList; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            If mDataOp Is Nothing Then objDataOperation = Nothing
        End Try
        Return dsList
    End Function


    ''' <summary>
    ''' Modify By: Suhail
    ''' </summary>
    ''' <returns>Boolean</returns>
    ''' <purpose> INSERT INTO Database Table (trdepttrainingneed_traininginstructor_tran) </purpose>
    Public Function Insert(Optional ByVal xDataOp As clsDataOperation = Nothing) As Boolean
        'If isExist(mstrName) Then
        '    mstrMessage = "<Message>"
        '    Return False
        'End If

        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        If xDataOp IsNot Nothing Then
            objDataOperation = xDataOp
        Else
            objDataOperation = New clsDataOperation
            objDataOperation.BindTransaction()
        End If
        objDataOperation.ClearParameters()

        Try
            objDataOperation.AddParameter("@departmentaltrainingneedunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintDepartmentaltrainingneedunkid.ToString)
            objDataOperation.AddParameter("@employeeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintEmployeeunkid.ToString)
            objDataOperation.AddParameter("@others_name", SqlDbType.NText, eZeeDataType.NAME_SIZE, mstrOthers_name.ToString)
            objDataOperation.AddParameter("@others_company", SqlDbType.NText, eZeeDataType.NAME_SIZE, mstrOthers_company.ToString)
            objDataOperation.AddParameter("@others_department", SqlDbType.NText, eZeeDataType.NAME_SIZE, mstrOthers_department.ToString)
            objDataOperation.AddParameter("@others_job", SqlDbType.NText, eZeeDataType.NAME_SIZE, mstrOthers_job.ToString)
            objDataOperation.AddParameter("@others_email", SqlDbType.NText, eZeeDataType.NAME_SIZE, mstrOthers_email.ToString)
            objDataOperation.AddParameter("@userunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintUserunkid.ToString)
            objDataOperation.AddParameter("@loginemployeeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintLoginemployeeunkid.ToString)
            objDataOperation.AddParameter("@isweb", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsweb.ToString)
            objDataOperation.AddParameter("@isvoid", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsvoid.ToString)
            objDataOperation.AddParameter("@voiduserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintVoiduserunkid.ToString)
            objDataOperation.AddParameter("@voidloginemployeeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintVoidloginemployeeunkid.ToString)
            If mdtVoiddatetime = Nothing Then
                objDataOperation.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, DBNull.Value)
            Else
                objDataOperation.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, mdtVoiddatetime.ToString)
            End If
            objDataOperation.AddParameter("@voidreason", SqlDbType.NText, eZeeDataType.NAME_SIZE, mstrVoidreason.ToString)

            strQ = "INSERT INTO trdepttrainingneed_traininginstructor_tran ( " & _
              "  departmentaltrainingneedunkid " & _
              ", employeeunkid " & _
              ", others_name " & _
              ", others_company " & _
              ", others_department " & _
              ", others_job " & _
              ", others_email " & _
              ", userunkid " & _
              ", loginemployeeunkid " & _
              ", isweb " & _
              ", isvoid " & _
              ", voiduserunkid " & _
              ", voidloginemployeeunkid " & _
              ", voiddatetime " & _
              ", voidreason" & _
            ") VALUES (" & _
              "  @departmentaltrainingneedunkid " & _
              ", @employeeunkid " & _
              ", @others_name " & _
              ", @others_company " & _
              ", @others_department " & _
              ", @others_job " & _
              ", @others_email " & _
              ", @userunkid " & _
              ", @loginemployeeunkid " & _
              ", @isweb " & _
              ", @isvoid " & _
              ", @voiduserunkid " & _
              ", @voidloginemployeeunkid " & _
              ", @voiddatetime " & _
              ", @voidreason" & _
            "); SELECT @@identity"

            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            mintDepttrainingneedtraininginstructortranunkid = dsList.Tables(0).Rows(0).Item(0)

            If Insert_AtTranLog(objDataOperation, 1) = False Then
                If xDataOp Is Nothing Then objDataOperation.ReleaseTransaction(False)
                Return False
            End If

            If xDataOp Is Nothing Then objDataOperation.ReleaseTransaction(True)

            Return True
        Catch ex As Exception
            If xDataOp Is Nothing Then objDataOperation.ReleaseTransaction(False)
            Throw New Exception(ex.Message & "; Procedure Name: Insert; Module Name: " & mstrModuleName)
            Return False
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            If xDataOp Is Nothing Then objDataOperation = Nothing
        End Try
    End Function

    ''' <summary>
    ''' Modify By: Suhail
    ''' </summary>
    ''' <returns>Boolean</returns>
    ''' <purpose> Update Database Table (trdepttrainingneed_traininginstructor_tran) </purpose>
    Public Function Update(ByRef blnChildTableChanged As Boolean, Optional ByVal xDataOp As clsDataOperation = Nothing) As Boolean
        'If isExist(mstrName, mintDepttrainingneedtraininginstructortranunkid) Then
        '    mstrMessage = "<Message>"
        '    Return False
        'End If

        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        If xDataOp IsNot Nothing Then
            objDataOperation = xDataOp
        Else
            objDataOperation = New clsDataOperation
            objDataOperation.BindTransaction()
        End If
        objDataOperation.ClearParameters()

        Try
            objDataOperation.AddParameter("@depttrainingneedtraininginstructortranunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintDepttrainingneedtraininginstructortranunkid.ToString)
            objDataOperation.AddParameter("@departmentaltrainingneedunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintDepartmentaltrainingneedunkid.ToString)
            objDataOperation.AddParameter("@employeeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintEmployeeunkid.ToString)
            objDataOperation.AddParameter("@others_name", SqlDbType.NText, eZeeDataType.NAME_SIZE, mstrOthers_name.ToString)
            objDataOperation.AddParameter("@others_company", SqlDbType.NText, eZeeDataType.NAME_SIZE, mstrOthers_company.ToString)
            objDataOperation.AddParameter("@others_department", SqlDbType.NText, eZeeDataType.NAME_SIZE, mstrOthers_department.ToString)
            objDataOperation.AddParameter("@others_job", SqlDbType.NText, eZeeDataType.NAME_SIZE, mstrOthers_job.ToString)
            objDataOperation.AddParameter("@others_email", SqlDbType.NText, eZeeDataType.NAME_SIZE, mstrOthers_email.ToString)
            objDataOperation.AddParameter("@userunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintUserunkid.ToString)
            objDataOperation.AddParameter("@loginemployeeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintLoginemployeeunkid.ToString)
            objDataOperation.AddParameter("@isweb", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsweb.ToString)
            objDataOperation.AddParameter("@isvoid", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsvoid.ToString)
            objDataOperation.AddParameter("@voiduserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintVoiduserunkid.ToString)
            objDataOperation.AddParameter("@voidloginemployeeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintVoidloginemployeeunkid.ToString)
            If mdtVoiddatetime = Nothing Then
                objDataOperation.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, DBNull.Value)
            Else
                objDataOperation.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, mdtVoiddatetime.ToString)
            End If
            objDataOperation.AddParameter("@voidreason", SqlDbType.NText, eZeeDataType.NAME_SIZE, mstrVoidreason.ToString)

            strQ = "UPDATE trdepttrainingneed_traininginstructor_tran SET " & _
              "  departmentaltrainingneedunkid = @departmentaltrainingneedunkid" & _
              ", employeeunkid = @employeeunkid" & _
              ", others_name = @others_name " & _
              ", others_company = @others_company " & _
              ", others_department = @others_department " & _
              ", others_job = @others_job " & _
              ", others_email = @others_email " & _
              ", userunkid = @userunkid" & _
              ", loginemployeeunkid = @loginemployeeunkid" & _
              ", isweb = @isweb" & _
              ", isvoid = @isvoid" & _
              ", voiduserunkid = @voiduserunkid" & _
              ", voidloginemployeeunkid = @voidloginemployeeunkid" & _
              ", voiddatetime = @voiddatetime" & _
              ", voidreason = @voidreason " & _
            "WHERE depttrainingneedtraininginstructortranunkid = @depttrainingneedtraininginstructortranunkid "

            Call objDataOperation.ExecNonQuery(strQ)

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            If IsTableDataUpdate(mintDepttrainingneedtraininginstructortranunkid, objDataOperation) = False Then

                If Insert_AtTranLog(objDataOperation, 2) = False Then
                    If xDataOp Is Nothing Then objDataOperation.ReleaseTransaction(False)
                    Return False
                End If

                blnChildTableChanged = True
            End If

            If xDataOp Is Nothing Then objDataOperation.ReleaseTransaction(True)

            Return True
        Catch ex As Exception
            If xDataOp Is Nothing Then objDataOperation.ReleaseTransaction(False)
            Throw New Exception(ex.Message & "; Procedure Name: Update; Module Name: " & mstrModuleName)
            Return False
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            If xDataOp Is Nothing Then objDataOperation = Nothing
        End Try
    End Function

    Public Function SaveAll(ByVal lstDeptTInstruct As List(Of clsDepttrainingneed_traininginstructor_Tran) _
                            , Optional ByVal clsDeptTNeedMaster As clsDepartmentaltrainingneed_master = Nothing _
                            , Optional ByVal xDataOp As clsDataOperation = Nothing _
                            ) As Boolean
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        If xDataOp IsNot Nothing Then
            objDataOperation = xDataOp
        Else
            objDataOperation = New clsDataOperation
            objDataOperation.BindTransaction()
        End If
        objDataOperation.ClearParameters()

        Try

            If mintDepartmentaltrainingneedunkid <= 0 AndAlso clsDeptTNeedMaster IsNot Nothing Then
                Dim intNewUnkId As Integer = 0
                If clsDeptTNeedMaster.Insert(objDataOperation, intNewUnkId) = False Then
                    Return False
                Else
                    mintDepartmentaltrainingneedunkid = intNewUnkId
                End If
            ElseIf mintDepartmentaltrainingneedunkid > 0 AndAlso clsDeptTNeedMaster IsNot Nothing Then
                If clsDeptTNeedMaster.Update(objDataOperation) = False Then
                    Return False
                End If
            End If

            For Each clsDTInstruct As clsDepttrainingneed_traininginstructor_Tran In lstDeptTInstruct

                With clsDTInstruct
                    mintDepttrainingneedtraininginstructortranunkid = .pintDepttrainingneedtraininginstructortranunkid
                    mintDepartmentaltrainingneedunkid = mintDepartmentaltrainingneedunkid
                    mintEmployeeunkid = .pintEmployeeunkid
                    mstrOthers_name = .pstrOthers_name
                    mstrOthers_company = .pstrOthers_company
                    mstrOthers_department = .pstrOthers_department
                    mstrOthers_job = .pstrOthers_job
                    mstrOthers_email = .pstrOthers_email

                    mintUserunkid = .pintUserunkid
                    mintLoginemployeeunkid = .pintLoginemployeeunkid
                    mblnIsweb = .pblnIsweb
                    mblnIsvoid = .pblnIsvoid
                    mintVoiduserunkid = .pintVoiduserunkid
                    mintVoidloginemployeeunkid = .pintVoidloginemployeeunkid
                    mdtVoiddatetime = .pdtVoiddatetime
                    mstrVoidreason = .pstrVoidreason

                    mintAuditUserId = .pintAuditUserId
                    mdtAuditDate = .pdtAuditDate
                    mstrClientIp = .pstrClientIp
                    mstrHostName = .pstrHostName
                    mstrFormName = .pstrFormName

                    mintDepttrainingneedtraininginstructortranunkid = isExist(mintDepartmentaltrainingneedunkid, mintEmployeeunkid, mstrOthers_name, mstrOthers_email, , objDataOperation)
                    If mintDepttrainingneedtraininginstructortranunkid <= 0 Then
                        If Insert(objDataOperation) = False Then
                            If xDataOp Is Nothing Then objDataOperation.ReleaseTransaction(False)
                            Exit For
                        End If
                    Else
                        If Update(False, objDataOperation) = False Then
                            If xDataOp Is Nothing Then objDataOperation.ReleaseTransaction(False)
                            Exit For
                        End If
                    End If

                End With

            Next

            If xDataOp Is Nothing Then objDataOperation.ReleaseTransaction(True)

            Return True

        Catch ex As Exception
            If xDataOp Is Nothing Then objDataOperation.ReleaseTransaction(False)
            Throw New Exception(ex.Message & "; Procedure Name: SaveAll; Module Name: " & mstrModuleName)
            Return False
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            If xDataOp Is Nothing Then objDataOperation = Nothing
        End Try
    End Function

    ''' <summary>
    ''' Modify By: Suhail
    ''' </summary>
    ''' <returns>Boolean</returns>
    ''' <purpose> Delete Database Table (trdepttrainingneed_traininginstructor_tran) </purpose>
    Public Function Void(ByVal intUnkid As Integer, Optional ByVal xDataOp As clsDataOperation = Nothing) As Boolean
        'If isUsed(intUnkid) Then
        '    mstrMessage = "<Message>"
        '    Return False
        'End If

        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        If xDataOp IsNot Nothing Then
            objDataOperation = xDataOp
        Else
            objDataOperation = New clsDataOperation
            objDataOperation.BindTransaction()
        End If
        objDataOperation.ClearParameters()

        Try
            strQ = "UPDATE trdepttrainingneed_traininginstructor_tran SET " & _
              "  isvoid = 1 " & _
              ", voiduserunkid = @voiduserunkid " & _
              ", voiddatetime = @voiddatetime " & _
              ", voidreason = @voidreason " & _
            "WHERE depttrainingneedtraininginstructortranunkid = @depttrainingneedtraininginstructortranunkid "

            objDataOperation.AddParameter("@depttrainingneedtraininginstructortranunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intUnkid)
            objDataOperation.AddParameter("@voiduserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintVoiduserunkid)
            objDataOperation.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, mdtVoiddatetime)
            objDataOperation.AddParameter("@voidreason", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrVoidreason)

            Call objDataOperation.ExecNonQuery(strQ)

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            If Insert_AtTranLog(objDataOperation, 3) = False Then
                If xDataOp Is Nothing Then objDataOperation.ReleaseTransaction(False)
                Return False
            End If

            If xDataOp Is Nothing Then objDataOperation.ReleaseTransaction(True)

            Return True
        Catch ex As Exception
            If xDataOp Is Nothing Then objDataOperation.ReleaseTransaction(False)
            Throw New Exception(ex.Message & "; Procedure Name: Void; Module Name: " & mstrModuleName)
            Return False
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            If xDataOp Is Nothing Then objDataOperation = Nothing
        End Try
    End Function

    Public Function VoidAll(ByVal lstDeptTInstruct As List(Of clsDepttrainingneed_traininginstructor_Tran) _
                            , Optional ByVal xDataOp As clsDataOperation = Nothing _
                            ) As Boolean
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        If xDataOp IsNot Nothing Then
            objDataOperation = xDataOp
        Else
            objDataOperation = New clsDataOperation
            objDataOperation.BindTransaction()
        End If
        objDataOperation.ClearParameters()

        Try

            For Each clsDTInstruct As clsDepttrainingneed_traininginstructor_Tran In lstDeptTInstruct

                With clsDTInstruct
                    mDataOp = objDataOperation
                    _Depttrainingneedtraininginstructortranunkid = .pintDepttrainingneedtraininginstructortranunkid

                    mblnIsvoid = .pblnIsvoid
                    mintVoiduserunkid = .pintVoiduserunkid
                    mintVoidloginemployeeunkid = .pintVoidloginemployeeunkid
                    mdtVoiddatetime = .pdtVoiddatetime
                    mstrVoidreason = .pstrVoidreason

                    mblnIsweb = .pblnIsweb
                    mintAuditUserId = .pintAuditUserId
                    mdtAuditDate = .pdtAuditDate
                    mstrClientIp = .pstrClientIp
                    mstrHostName = .pstrHostName
                    mstrFormName = .pstrFormName

                    If Void(mintDepttrainingneedtraininginstructortranunkid, objDataOperation) = False Then
                        If xDataOp Is Nothing Then objDataOperation.ReleaseTransaction(False)
                        Exit For
                    End If

                End With

            Next

            If xDataOp Is Nothing Then objDataOperation.ReleaseTransaction(True)

            Return True

        Catch ex As Exception
            If xDataOp Is Nothing Then objDataOperation.ReleaseTransaction(False)
            Throw New Exception(ex.Message & "; Procedure Name: VoidAll; Module Name: " & mstrModuleName)
            Return False
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            If xDataOp Is Nothing Then objDataOperation = Nothing
        End Try
    End Function

    Public Function VoidByMasterUnkID(ByVal intMasterUnkID As Integer, _
                                      ByVal intParentAuditType As Integer, _
                                      Optional ByVal xDataOp As clsDataOperation = Nothing _
                                      ) As Boolean

        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        If xDataOp IsNot Nothing Then
            objDataOperation = xDataOp
        Else
            objDataOperation = New clsDataOperation
        End If
        objDataOperation.ClearParameters()

        Try

            strQ = "INSERT INTO attrdepttrainingneed_traininginstructor_tran ( " & _
                      "  depttrainingneedtraininginstructortranunkid " & _
                      ", departmentaltrainingneedunkid " & _
                      ", employeeunkid " & _
                      ", others_name " & _
                      ", others_company " & _
                      ", others_department " & _
                      ", others_job " & _
                      ", others_email " & _
                      ", audituserunkid " & _
                      ", loginemployeeunkid " & _
                      ", audittypeid " & _
                      ", auditdatetime " & _
                      ", isweb " & _
                      ", ip " & _
                      ", host " & _
                      ", form_name " & _
                ") SELECT " & _
                      "  depttrainingneedtraininginstructortranunkid " & _
                      ", departmentaltrainingneedunkid " & _
                      ", employeeunkid " & _
                      ", others_name " & _
                      ", others_company " & _
                      ", others_department " & _
                      ", others_job " & _
                      ", others_email " & _
                      ", @audituserunkid " & _
                      ", @loginemployeeunkid " & _
                      ", @audittypeid " & _
                      ", @auditdatetime " & _
                      ", @isweb " & _
                      ", @ip " & _
                      ", @host " & _
                      ", @form_name " & _
                "FROM trdepttrainingneed_traininginstructor_tran " & _
                         "WHERE isvoid = 0 " & _
                         "AND departmentaltrainingneedunkid = @departmentaltrainingneedunkid "

            objDataOperation.ClearParameters()
            objDataOperation.AddParameter("@departmentaltrainingneedunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intMasterUnkID)
            objDataOperation.AddParameter("@audituserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintAuditUserId)
            objDataOperation.AddParameter("@loginemployeeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintVoidloginemployeeunkid)
            objDataOperation.AddParameter("@audittypeid", SqlDbType.Int, eZeeDataType.INT_SIZE, intParentAuditType)
            objDataOperation.AddParameter("@auditdatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, mdtAuditDate)
            objDataOperation.AddParameter("@ip", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrClientIp)
            objDataOperation.AddParameter("@host", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrHostName)
            objDataOperation.AddParameter("@form_name", SqlDbType.NVarChar, eZeeDataType.DESC_SIZE, mstrFormName)
            objDataOperation.AddParameter("@isweb", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsweb)

            Call objDataOperation.ExecNonQuery(strQ)

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If
            objDataOperation.ClearParameters()

            strQ = "UPDATE trdepttrainingneed_traininginstructor_tran SET " & _
              "  isvoid = 1 " & _
              ", voiduserunkid = @voiduserunkid" & _
              ", voidloginemployeeunkid = @voidloginemployeeunkid" & _
              ", voiddatetime = @voiddatetime" & _
              ", voidreason = @voidreason " & _
            "WHERE departmentaltrainingneedunkid = @departmentaltrainingneedunkid "

            objDataOperation.AddParameter("@departmentaltrainingneedunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intMasterUnkID)
            objDataOperation.AddParameter("@voiduserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintVoiduserunkid)
            objDataOperation.AddParameter("@voidloginemployeeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintVoidloginemployeeunkid)
            objDataOperation.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, mdtVoiddatetime)
            objDataOperation.AddParameter("@voidreason", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrVoidreason)

            Call objDataOperation.ExecNonQuery(strQ)

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            Return True
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: VoidByMasterUnkID; Module Name: " & mstrModuleName)
            Return False
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            If xDataOp Is Nothing Then objDataOperation = Nothing
        End Try
    End Function

    ''' <summary>
    ''' Modify By: Suhail
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>
    Public Function isUsed(ByVal intUnkid As Integer) As Boolean
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        objDataOperation = New clsDataOperation

        Try
            strQ = "<Query>"

            objDataOperation.AddParameter("@depttrainingneedtraininginstructortranunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intUnkid)

            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            Return dsList.Tables(0).Rows.Count > 0
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: isUsed; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
    End Function

    ''' <summary>
    ''' Modify By: Suhail
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>
    Public Function isExist(ByVal intDepartmentaltrainingneedunkid As Integer _
                            , ByVal intEmployeeunkid As Integer _
                            , ByVal strOthers_Name As String _
                            , ByVal strOthers_Email As String _
                            , Optional ByVal intUnkid As Integer = -1 _
                            , Optional ByVal xDataOpr As clsDataOperation = Nothing _
                            ) As Integer

        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim intRetUnkId As Integer = 0
        Dim exForce As Exception

        If xDataOpr Is Nothing Then
            objDataOperation = New clsDataOperation
        Else
            objDataOperation = xDataOpr
        End If
        objDataOperation.ClearParameters()

        Try
            strQ = "SELECT " & _
              "  depttrainingneedtraininginstructortranunkid " & _
             "FROM trdepttrainingneed_traininginstructor_tran " & _
             "WHERE isvoid = 0 "

            If intDepartmentaltrainingneedunkid > 0 Then
                strQ &= " AND trdepttrainingneed_traininginstructor_tran.departmentaltrainingneedunkid = @departmentaltrainingneedunkid "
                objDataOperation.AddParameter("@departmentaltrainingneedunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intDepartmentaltrainingneedunkid)
            End If

            If intEmployeeunkid > 0 Then
                strQ &= " AND trdepttrainingneed_traininginstructor_tran.employeeunkid = @employeeunkid "
                objDataOperation.AddParameter("@employeeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intEmployeeunkid)
            End If

            If strOthers_Name.Trim <> "" Then
                strQ &= " AND trdepttrainingneed_traininginstructor_tran.others_name = @others_name "
                objDataOperation.AddParameter("@others_name", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, strOthers_Name)
            End If

            If strOthers_Email.Trim <> "" Then
                strQ &= " AND trdepttrainingneed_traininginstructor_tran.others_email = @others_email "
                objDataOperation.AddParameter("@others_email", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, strOthers_Email)
            End If

            If intUnkid > 0 Then
                strQ &= " AND trdepttrainingneed_traininginstructor_tran.depttrainingneedtraininginstructortranunkid <> @depttrainingneedtraininginstructortranunkid "
                objDataOperation.AddParameter("@depttrainingneedtraininginstructortranunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intUnkid)
            End If

            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            If dsList.Tables(0).Rows.Count > 0 Then
                intRetUnkId = CInt(dsList.Tables(0).Rows(0).Item("depttrainingneedtraininginstructortranunkid"))
            End If

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: isExist; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            If xDataOpr Is Nothing Then objDataOperation = Nothing
        End Try
        Return intRetUnkId
    End Function

    Public Function Insert_AtTranLog(ByVal objDoOps As clsDataOperation, ByVal AuditType As Integer)
        Dim StrQ As String = String.Empty
        Dim extForce As Exception
        Dim dsList As New DataSet

        Try
            StrQ = "INSERT INTO attrdepttrainingneed_traininginstructor_tran ( " & _
                      "  depttrainingneedtraininginstructortranunkid " & _
                      ", departmentaltrainingneedunkid " & _
                      ", employeeunkid " & _
                      ", others_name " & _
                      ", others_company " & _
                      ", others_department " & _
                      ", others_job " & _
                      ", others_email " & _
                      ", audituserunkid " & _
                      ", loginemployeeunkid " & _
                      ", audittypeid " & _
                      ", auditdatetime " & _
                      ", isweb " & _
                      ", ip " & _
                      ", host " & _
                      ", form_name " & _
            ") VALUES (" & _
                      "  @depttrainingneedtraininginstructortranunkid " & _
                      ", @departmentaltrainingneedunkid " & _
                      ", @employeeunkid " & _
                      ", @others_name " & _
                      ", @others_company " & _
                      ", @others_department " & _
                      ", @others_job " & _
                      ", @others_email " & _
                      ", @audituserunkid " & _
                      ", @loginemployeeunkid " & _
                      ", @audittypeid " & _
                      ", @auditdatetime " & _
                      ", @isweb " & _
                      ", @ip " & _
                      ", @host " & _
                      ", @form_name " & _
            ")"

            objDoOps.ClearParameters()
            objDoOps.AddParameter("@depttrainingneedtraininginstructortranunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintDepttrainingneedtraininginstructortranunkid.ToString)
            objDoOps.AddParameter("@departmentaltrainingneedunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintDepartmentaltrainingneedunkid.ToString)
            objDoOps.AddParameter("@employeeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintEmployeeunkid.ToString)
            objDoOps.AddParameter("@others_name", SqlDbType.NText, eZeeDataType.NAME_SIZE, mstrOthers_name.ToString)
            objDoOps.AddParameter("@others_company", SqlDbType.NText, eZeeDataType.NAME_SIZE, mstrOthers_company.ToString)
            objDoOps.AddParameter("@others_department", SqlDbType.NText, eZeeDataType.NAME_SIZE, mstrOthers_department.ToString)
            objDoOps.AddParameter("@others_job", SqlDbType.NText, eZeeDataType.NAME_SIZE, mstrOthers_job.ToString)
            objDoOps.AddParameter("@others_email", SqlDbType.NText, eZeeDataType.NAME_SIZE, mstrOthers_email.ToString)

            objDoOps.AddParameter("@audituserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintAuditUserId)
            objDoOps.AddParameter("@loginemployeeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintLoginemployeeunkid)
            objDoOps.AddParameter("@audittypeid", SqlDbType.Int, eZeeDataType.INT_SIZE, AuditType)
            objDoOps.AddParameter("@auditdatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, mdtAuditDate)
            objDoOps.AddParameter("@isweb", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsweb)
            objDoOps.AddParameter("@ip", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrClientIp)
            objDoOps.AddParameter("@host", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrHostName)
            objDoOps.AddParameter("@form_name", SqlDbType.NVarChar, eZeeDataType.DESC_SIZE, mstrFormName)

            dsList = objDoOps.ExecQuery(StrQ, "List")

            If objDoOps.ErrorMessage <> "" Then
                extForce = New Exception(objDoOps.ErrorNumber & ": " & objDoOps.ErrorMessage)
                Throw extForce
            End If


        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: Insert_AtTranLog; Module Name: " & mstrModuleName)
        End Try
        Return True

    End Function

    Public Function IsTableDataUpdate(ByVal unkid As Integer, Optional ByVal objDooperation As clsDataOperation = Nothing) As Boolean
        Dim strQ As String = ""
        Dim dsList As New DataSet
        Dim exForce As Exception
        Try
            If objDooperation Is Nothing Then
                objDataOperation = New clsDataOperation
            Else
                objDataOperation = objDooperation
            End If

            strQ = "SELECT TOP 1 * FROM attrdepttrainingneed_traininginstructor_tran WHERE depttrainingneedtraininginstructortranunkid = @depttrainingneedtraininginstructortranunkid AND departmentaltrainingneedunkid = @departmentaltrainingneedunkid AND audittypeid <> 3 ORDER BY auditdatetime DESC"

            objDataOperation.ClearParameters()
            objDataOperation.AddParameter("@depttrainingneedtraininginstructortranunkid", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, unkid)
            objDataOperation.AddParameter("@departmentaltrainingneedunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintDepartmentaltrainingneedunkid)

            dsList = objDataOperation.ExecQuery(strQ, "List")

            For Each dr As DataRow In dsList.Tables(0).Rows

                If dr("employeeunkid") = mintEmployeeunkid AndAlso dr("others_name").ToString = mstrOthers_name AndAlso dr("others_company").ToString = mstrOthers_company AndAlso dr("others_department").ToString = mstrOthers_department _
                    AndAlso dr("others_job").ToString = mstrOthers_job AndAlso dr("others_email").ToString = mstrOthers_email Then

                    Return True
                Else
                    Return False
                End If
            Next
            Return False

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: IsTableDataUpdate; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If objDooperation Is Nothing Then objDataOperation = Nothing
        End Try
    End Function

End Class