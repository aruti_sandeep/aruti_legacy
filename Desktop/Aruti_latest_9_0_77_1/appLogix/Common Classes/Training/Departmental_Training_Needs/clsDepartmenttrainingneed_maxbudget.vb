﻿
Imports eZeeCommonLib
Imports System.Globalization
Imports System.Web

''' <summary>
''' Purpose: 
''' Developer: Suhail
''' </summary>
Public Class clsDepartmenttrainingneed_maxbudget


    Private Const mstrModuleName = "clsDepartmenttrainingneed_maxbudget"
    Dim objDataOperation As clsDataOperation
    Dim mstrMessage As String = ""

#Region " Private variables "
    Private mintUnkid As Integer
    Private mintAllocationid As Integer
    Private mintAllocationtranunkid As Integer
    Private mintPeriodunkid As Integer
    Private mdecMaxbudget_Amt As Decimal
    Private mintUserunkid As Integer
    Private mblnIsvoid As Boolean
    Private mintVoiduserunkid As Integer
    Private mdtVoiddatetime As Date
    Private mstrVoidreason As String = String.Empty
    Private mstrFormName As String = ""
    Private mstrClientIp As String = ""
    Private mstrHostName As String = ""
    Private mblnIsweb As Boolean
#End Region


#Region " public variables "
    Public pmintUnkid As Integer
    Public pmintAllocationid As Integer
    Public pmintAllocationtranunkid As Integer
    Public pmintPeriodunkid As Integer
    Public pmdecMaxbudget_Amt As Decimal
    Public pmintUserunkid As Integer
    Public pmblnIsvoid As Boolean
    Public pmintVoiduserunkid As Integer
    Public pmdtVoiddatetime As Date
    Public pmstrVoidreason As String = String.Empty
    Public pmstrFormName As String = ""
    Public pmstrClientIp As String = ""
    Public pmstrHostName As String = ""
    Public pmblnIsweb As Boolean
#End Region

#Region " Properties "
    ''' <summary>
    ''' Purpose: Get Message from Class 
    ''' Modify By: Pinkal
    ''' </summary>
    Public ReadOnly Property _Message() As String
        Get
            Return mstrMessage
        End Get
    End Property

    ''' <summary>
    ''' Purpose: Get or Set unkid
    ''' Modify By: Pinkal
    ''' </summary>
    Public Property _Unkid() As Integer
        Get
            Return mintUnkid
        End Get
        Set(ByVal value As Integer)
            mintUnkid = value
            Call GetData()
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set allocationid
    ''' Modify By: Pinkal
    ''' </summary>
    Public Property _Allocationid() As Integer
        Get
            Return mintAllocationid
        End Get
        Set(ByVal value As Integer)
            mintAllocationid = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set allocationtranunkid
    ''' Modify By: Pinkal
    ''' </summary>
    Public Property _Allocationtranunkid() As Integer
        Get
            Return mintAllocationtranunkid
        End Get
        Set(ByVal value As Integer)
            mintAllocationtranunkid = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set periodunkid
    ''' Modify By: Pinkal
    ''' </summary>
    Public Property _Periodunkid() As Integer
        Get
            Return mintPeriodunkid
        End Get
        Set(ByVal value As Integer)
            mintPeriodunkid = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set maxbudget_amt
    ''' Modify By: Pinkal
    ''' </summary>
    Public Property _Maxbudget_Amt() As Decimal
        Get
            Return mdecMaxbudget_Amt
        End Get
        Set(ByVal value As Decimal)
            mdecMaxbudget_Amt = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set userunkid
    ''' Modify By: Pinkal
    ''' </summary>
    Public Property _Userunkid() As Integer
        Get
            Return mintUserunkid
        End Get
        Set(ByVal value As Integer)
            mintUserunkid = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set isvoid
    ''' Modify By: Pinkal
    ''' </summary>
    Public Property _Isvoid() As Boolean
        Get
            Return mblnIsvoid
        End Get
        Set(ByVal value As Boolean)
            mblnIsvoid = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set voiduserunkid
    ''' Modify By: Pinkal
    ''' </summary>
    Public Property _Voiduserunkid() As Integer
        Get
            Return mintVoiduserunkid
        End Get
        Set(ByVal value As Integer)
            mintVoiduserunkid = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set voiddatetime
    ''' Modify By: Pinkal
    ''' </summary>
    Public Property _Voiddatetime() As Date
        Get
            Return mdtVoiddatetime
        End Get
        Set(ByVal value As Date)
            mdtVoiddatetime = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set voidreason
    ''' Modify By: Pinkal
    ''' </summary>
    Public Property _Voidreason() As String
        Get
            Return mstrVoidreason
        End Get
        Set(ByVal value As String)
            mstrVoidreason = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set FormName
    ''' Modify By: Pinkal
    ''' </summary>
    Public WriteOnly Property _FormName() As String
        Set(ByVal value As String)
            mstrFormName = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set ClientIP
    ''' Modify By: Pinkal
    ''' </summary>
    Public WriteOnly Property _ClientIP() As String
        Set(ByVal value As String)
            mstrClientIp = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set HostName
    ''' Modify By: Pinkal
    ''' </summary>
    Public WriteOnly Property _HostName() As String
        Set(ByVal value As String)
            mstrHostName = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set IsWeb
    ''' Modify By: Pinkal
    ''' </summary>
    Public WriteOnly Property _IsWeb() As Boolean
        Set(ByVal value As Boolean)
            mblnIsweb = value
        End Set
    End Property

    Private lstDeptMaxBudget As List(Of clsDepartmenttrainingneed_maxbudget)
    Public WriteOnly Property _lstDeptMaxBudget() As List(Of clsDepartmenttrainingneed_maxbudget)
        Set(ByVal value As List(Of clsDepartmenttrainingneed_maxbudget))
            lstDeptMaxBudget = value
        End Set
    End Property

#End Region

    ''' <summary>
    ''' Modify By: Pinkal
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>
    Public Sub GetData()
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        objDataOperation = New clsDataOperation

        Try
            strQ = "SELECT " & _
              "  unkid " & _
              ", allocationid " & _
              ", allocationtranunkid " & _
              ", periodunkid " & _
              ", maxbudget_amt " & _
              ", userunkid " & _
              ", isvoid " & _
              ", voiduserunkid " & _
              ", voiddatetime " & _
              ", voidreason " & _
             "FROM trdept_maxbudget " & _
             "WHERE unkid = @unkid "

            objDataOperation.AddParameter("@unkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintUnkid.ToString)

            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            For Each dtRow As DataRow In dsList.Tables(0).Rows
                mintUnkid = CInt(dtRow.Item("unkid"))
                mintAllocationid = CInt(dtRow.Item("allocationid"))
                mintAllocationtranunkid = CInt(dtRow.Item("allocationtranunkid"))
                mintPeriodunkid = CInt(dtRow.Item("periodunkid"))
                mdecMaxbudget_Amt = dtRow.Item("maxbudget_amt")
                mintUserunkid = CInt(dtRow.Item("userunkid"))
                mblnIsvoid = CBool(dtRow.Item("isvoid"))
                mintVoiduserunkid = CInt(dtRow.Item("voiduserunkid"))
                mdtVoiddatetime = dtRow.Item("voiddatetime")
                mstrVoidreason = dtRow.Item("voidreason").ToString
                Exit For
            Next
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: GetData; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
    End Sub


    ''' <summary>
    ''' Modify By: Pinkal
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>
    Public Function GetList(ByVal strTableName As String, ByVal xPeriodId As Integer, ByVal xCurrentAllocationId As Integer, Optional ByVal blnOnlyActive As Boolean = True, Optional ByVal blnIncludePendingDepartments As Boolean = True) As DataSet
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception
        'Sohail (10 Feb 2022) -- Start
        'Enhancement :  OLD-551 : NMB - New config setting to allow Training Budget settings to be based on any allocation - Cost Center etc.
        Dim strTable As String = ""
        Dim strUnkIdField As String = ""
        Dim strCodeField As String = ""
        Dim strNameField As String = ""
        'Sohail (10 Feb 2022) -- End

        Dim objDataOperation As clsDataOperation
        objDataOperation = New clsDataOperation

        Try

            'Sohail (10 Feb 2022) -- Start
            'Enhancement :  OLD-551 : NMB - New config setting to allow Training Budget settings to be based on any allocation - Cost Center etc.
            Select Case xCurrentAllocationId

                Case enAllocation.BRANCH

                    strTable = "hrstation_master"
                    strUnkIdField = "stationunkid"
                    strCodeField = "code"
                    strNameField = "name"

                Case enAllocation.DEPARTMENT_GROUP

                    strTable = "hrdepartment_group_master"
                    strUnkIdField = "deptgroupunkid"
                    strCodeField = "code"
                    strNameField = "name"

                Case enAllocation.DEPARTMENT

                    strTable = "hrdepartment_master"
                    strUnkIdField = "departmentunkid"
                    strCodeField = "code"
                    strNameField = "name"

                Case enAllocation.SECTION_GROUP

                    strTable = "hrsectiongroup_master"
                    strUnkIdField = "sectiongroupunkid"
                    strCodeField = "code"
                    strNameField = "name"

                Case enAllocation.SECTION

                    strTable = "hrsection_master"
                    strUnkIdField = "sectionunkid"
                    strCodeField = "code"
                    strNameField = "name"

                Case enAllocation.UNIT_GROUP

                    strTable = "hrunitgroup_master"
                    strUnkIdField = "unitgroupunkid"
                    strCodeField = "code"
                    strNameField = "name"

                Case enAllocation.UNIT

                    strTable = "hrunit_master"
                    strUnkIdField = "unitunkid"
                    strCodeField = "code"
                    strNameField = "name"

                Case enAllocation.TEAM

                    strTable = "hrteam_master"
                    strUnkIdField = "teamunkid"
                    strCodeField = "code"
                    strNameField = "name"

                Case enAllocation.JOB_GROUP

                    strTable = "hrjobgroup_master"
                    strUnkIdField = "jobgroupunkid"
                    strCodeField = "code"
                    strNameField = "name"

                Case enAllocation.JOBS

                    strTable = "hrjob_master"
                    strUnkIdField = "jobunkid"
                    strCodeField = "job_code"
                    strNameField = "job_name"

                Case enAllocation.CLASS_GROUP

                    strTable = "hrclassgroup_master"
                    strUnkIdField = "classgroupunkid"
                    strCodeField = "code"
                    strNameField = "name"

                Case enAllocation.CLASSES

                    strTable = "hrclasses_master"
                    strUnkIdField = "classesunkid"
                    strCodeField = "code"
                    strNameField = "name"

                Case enAllocation.COST_CENTER

                    strTable = "prcostcenter_master"
                    strUnkIdField = "costcenterunkid"
                    strCodeField = "customcode"
                    strNameField = "costcentername"
            End Select
            'Sohail (10 Feb 2022) -- End

            strQ = "SELECT " & _
                      " allocationid " & _
                      ", allocationtranunkid " & _
                      ", CASE trdept_maxbudget.allocationtranunkid WHEN -99 THEN @Company ELSE ISNULL(" & strTable & "." & strCodeField & ",'') END AS allocationtrancode " & _
                      ", CASE trdept_maxbudget.allocationtranunkid WHEN -99 THEN @Company ELSE ISNULL(" & strTable & "." & strNameField & ",'') END AS allocationtranname "

            'Sohail (10 Feb 2022) -- Start
            'Enhancement :  OLD-551 : NMB - New config setting to allow Training Budget settings to be based on any allocation - Cost Center etc.
            'Select Case xCurrentAllocationId

            '    Case enAllocation.BRANCH

            '        strQ &= ", CASE trdept_maxbudget.allocationtranunkid WHEN -99 THEN @Company ELSE ISNULL(hrstation_master.code,'') END AS allocationtrancode " & _
            '                    ", CASE trdept_maxbudget.allocationtranunkid WHEN -99 THEN @Company ELSE ISNULL(hrstation_master.name,'') END AS allocationtranname "

            '    Case enAllocation.DEPARTMENT_GROUP

            '        strQ &= ", CASE trdept_maxbudget.allocationtranunkid WHEN -99 THEN @Company ELSE ISNULL(hrdepartment_group_master.code,'') END AS allocationtrancode " & _
            '                    ", CASE trdept_maxbudget.allocationtranunkid WHEN -99 THEN @Company ELSE ISNULL(hrdepartment_group_master.name,'') END AS allocationtranname "

            '    Case enAllocation.DEPARTMENT

            '        strQ &= ", CASE trdept_maxbudget.allocationtranunkid WHEN -99 THEN @Company ELSE ISNULL(hrdepartment_master.code,'') END AS allocationtrancode " & _
            '                     ", CASE trdept_maxbudget.allocationtranunkid WHEN -99 THEN @Company ELSE ISNULL(hrdepartment_master.name,'') END AS allocationtranname "

            '    Case enAllocation.SECTION_GROUP

            '        strQ &= ", CASE trdept_maxbudget.allocationtranunkid WHEN -99 THEN @Company ELSE ISNULL(hrsectiongroup_master.code,'') END AS allocationtrancode " & _
            '                    ", CASE trdept_maxbudget.allocationtranunkid WHEN -99 THEN @Company ELSE ISNULL(hrsectiongroup_master.name,'') END AS allocationtranname "

            '    Case enAllocation.SECTION

            '        strQ &= ",  CASE trdept_maxbudget.allocationtranunkid WHEN -99 THEN @Company ELSE ISNULL(hrsection_master.code,'') END AS allocationtrancode " & _
            '                    ",  CASE trdept_maxbudget.allocationtranunkid WHEN -99 THEN @Company ELSE ISNULL(hrsection_master.name,'') END AS allocationtranname "

            '    Case enAllocation.UNIT_GROUP

            '        strQ &= ", CASE trdept_maxbudget.allocationtranunkid WHEN -99 THEN @Company ELSE ISNULL(hrunitgroup_master.code,'') END AS allocationtrancode " & _
            '                     ", CASE trdept_maxbudget.allocationtranunkid WHEN -99 THEN @Company ELSE ISNULL(hrunitgroup_master.name,'') END AS allocationtranname "

            '    Case enAllocation.UNIT

            '        strQ &= ",  CASE trdept_maxbudget.allocationtranunkid WHEN -99 THEN @Company ELSE ISNULL(hrunit_master.code,'') END AS allocationtrancode " & _
            '                    ",  CASE trdept_maxbudget.allocationtranunkid WHEN -99 THEN @Company ELSE ISNULL(hrunit_master.name,'') END AS allocationtranname "

            '    Case enAllocation.TEAM

            '        strQ &= ", CASE trdept_maxbudget.allocationtranunkid WHEN -99 THEN @Company ELSE ISNULL(hrteam_master.code,'') END AS allocationtrancod e" & _
            '                    ", CASE trdept_maxbudget.allocationtranunkid WHEN -99 THEN @Company ELSE ISNULL(hrteam_master.name,'') END AS allocationtranname "

            '    Case enAllocation.JOB_GROUP

            '        strQ &= ", CASE trdept_maxbudget.allocationtranunkid WHEN -99 THEN @Company ELSE ISNULL(hrjobgroup_master.code,'') END AS allocationtrancode" & _
            '                     ", CASE trdept_maxbudget.allocationtranunkid WHEN -99 THEN @Company ELSE ISNULL(hrjobgroup_master.name,'') END AS allocationtranname "

            '    Case enAllocation.JOBS

            '        strQ &= ", CASE trdept_maxbudget.allocationtranunkid WHEN -99 THEN @Company ELSE ISNULL(hrjob_master.job_code,'') END AS allocationtrancode" & _
            '                     ", CASE trdept_maxbudget.allocationtranunkid WHEN -99 THEN @Company ELSE ISNULL(hrjob_master.job_name,'') END AS allocationtranname "

            '    Case enAllocation.CLASS_GROUP

            '        strQ &= ", CASE trdept_maxbudget.allocationtranunkid WHEN -99 THEN @Company ELSE ISNULL(hrclassgroup_master.code,'') END AS allocationtrancode" & _
            '                    ", CASE trdept_maxbudget.allocationtranunkid WHEN -99 THEN @Company ELSE ISNULL(hrclassgroup_master.name,'') END AS allocationtranname "

            '    Case enAllocation.CLASSES

            '        strQ &= ", CASE trdept_maxbudget.allocationtranunkid WHEN -99 THEN @Company ELSE ISNULL(hrclasses_master.code,'') END AS allocationtrancode" & _
            '                    ", CASE trdept_maxbudget.allocationtranunkid WHEN -99 THEN @Company ELSE ISNULL(hrclasses_master.name,'') END AS allocationtranname "

            'End Select
            'Sohail (10 Feb 2022) -- End


            strQ &= ", periodunkid " & _
                        ", maxbudget_amt " & _
                        " FROM trdept_maxbudget "

            'Sohail (10 Feb 2022) -- Start
            'Enhancement :  OLD-551 : NMB - New config setting to allow Training Budget settings to be based on any allocation - Cost Center etc.
            'Select Case xCurrentAllocationId

            '    Case enAllocation.BRANCH

            '        strQ &= " LEFT JOIN hrstation_master ON hrstation_master.stationunkid = trdept_maxbudget.allocationtranunkid "

            '    Case enAllocation.DEPARTMENT_GROUP

            '        strQ &= " LEFT JOIN hrdepartment_group_master ON hrdepartment_group_master.deptgroupunkid = trdept_maxbudget.allocationtranunkid "

            '    Case enAllocation.DEPARTMENT

            '        strQ &= " LEFT JOIN hrdepartment_master ON hrdepartment_master.departmentunkid = trdept_maxbudget.allocationtranunkid "

            '    Case enAllocation.SECTION_GROUP

            '        strQ &= " LEFT JOIN hrsectiongroup_master ON hrsectiongroup_master.sectiongroupunkid = trdept_maxbudget.allocationtranunkid "

            '    Case enAllocation.SECTION

            '        strQ &= " LEFT JOIN hrsection_master ON hrsection_master.sectionunkid = trdept_maxbudget.allocationtranunkid "

            '    Case enAllocation.UNIT_GROUP

            '        strQ &= " LEFT JOIN hrunitgroup_master ON hrunitgroup_master.unitgroupunkid = trdept_maxbudget.allocationtranunkid "

            '    Case enAllocation.UNIT

            '        strQ &= " LEFT JOIN hrunit_master ON hrunit_master.unitunkid = trdept_maxbudget.allocationtranunkid "

            '    Case enAllocation.TEAM

            '        strQ &= " LEFT JOIN hrteam_master ON hrteam_master.teamunkid = trdept_maxbudget.allocationtranunkid "

            '    Case enAllocation.JOB_GROUP

            '        strQ &= " LEFT JOIN hrjobgroup_master ON hrjobgroup_master.jobgroupunkid = trdept_maxbudget.allocationtranunkid "

            '    Case enAllocation.JOBS

            '        strQ &= " LEFT JOIN hrjob_master ON hrjob_master.jobunkid = trdept_maxbudget.allocationtranunkid "

            '    Case enAllocation.CLASS_GROUP

            '        strQ &= " LEFT JOIN hrclassgroup_master ON hrclassgroup_master.classgroupunkid = trdept_maxbudget.allocationtranunkid "

            '    Case enAllocation.CLASSES

            '        strQ &= " LEFT JOIN hrclasses_master ON hrclasses_master.classesunkid = trdept_maxbudget.allocationtranunkid "

            'End Select
            strQ &= " LEFT JOIN " & strTable & " ON " & strTable & "." & strUnkIdField & " = trdept_maxbudget.allocationtranunkid "
            'Sohail (10 Feb 2022) -- End

            strQ &= " WHERE trdept_maxbudget.isvoid= 0 AND trdept_maxbudget.periodunkid = @periodunkid AND trdept_maxbudget.allocationid = @allocationid "

            If blnIncludePendingDepartments = True Then


                strQ &= " UNION "

                strQ &= " SELECT " & _
                            " " & xCurrentAllocationId & " AS allocationid " & _
                            ", " & strTable & "." & strUnkIdField & " AS allocationtranid " & _
                            ", " & strTable & "." & strCodeField & " AS allocationtrancode " & _
                            ", " & strTable & "." & strNameField & " AS allocationtranname "

                'Sohail (10 Feb 2022) -- Start
                'Enhancement :  OLD-551 : NMB - New config setting to allow Training Budget settings to be based on any allocation - Cost Center etc.
                'Select Case xCurrentAllocationId

                '    Case enAllocation.BRANCH

                '        strQ &= ", hrstation_master.stationunkid AS allocationtranid " & _
                '                    ", ISNULL(hrstation_master.code,'') AS allocationtrancode " & _
                '                    ", ISNULL(hrstation_master.name,'') AS allocationtranname "

                '    Case enAllocation.DEPARTMENT_GROUP

                '        strQ &= ", hrdepartment_group_master.deptgroupunkid AS allocationtranid " & _
                '                    ", ISNULL(hrdepartment_group_master.code,'') AS allocationtrancode " & _
                '                    ", ISNULL(hrdepartment_group_master.name,'') AS allocationtranname "

                '    Case enAllocation.DEPARTMENT

                '        strQ &= ",  hrdepartment_master.departmentunkid AS allocationtranid " & _
                '                     ", ISNULL(hrdepartment_master.code,'') AS allocationtrancode " & _
                '                     ", ISNULL(hrdepartment_master.name,'') AS allocationtranname "

                '    Case enAllocation.SECTION_GROUP

                '        strQ &= ", hrsectiongroup_master.sectiongroupunkid AS allocationtranid " & _
                '                    ", ISNULL(hrsectiongroup_master.code,'') AS allocationtrancode " & _
                '                    ", ISNULL(hrsectiongroup_master.name,'') AS allocationtranname "

                '    Case enAllocation.SECTION

                '        strQ &= ",  hrsection_master.sectionunkid AS allocationtranid " & _
                '                    ",  ISNULL(hrsection_master.code,'') AS allocationtrancode " & _
                '                    ",  ISNULL(hrsection_master.name,'') AS allocationtranname "

                '    Case enAllocation.UNIT_GROUP

                '        strQ &= ",  hrunitgroup_master.unitgroupunkid AS allocationtranid " & _
                '                     ", ISNULL(hrunitgroup_master.code,'') AS allocationtrancode " & _
                '                     ", ISNULL(hrunitgroup_master.name,'') AS allocationtranname "

                '    Case enAllocation.UNIT

                '        strQ &= ",  hrunit_master.unitunkid AS allocationtranid " & _
                '                    ",  ISNULL(hrunit_master.code,'') AS allocationtrancode " & _
                '                    ",  ISNULL(hrunit_master.name,'') AS allocationtranname "

                '    Case enAllocation.TEAM

                '        strQ &= ", hrteam_master.teamunkid AS allocationtranid " & _
                '                    ", ISNULL(hrteam_master.code,'') AS allocationtrancod e" & _
                '                    ", ISNULL(hrteam_master.name,'') AS allocationtranname "

                '    Case enAllocation.JOB_GROUP

                '        strQ &= ", hrjobgroup_master.jobgroupunkid AS allocationtranid " & _
                '                     ", ISNULL(hrjobgroup_master.code,'') AS allocationtrancode" & _
                '                     ", ISNULL(hrjobgroup_master.name,'') AS allocationtranname "

                '    Case enAllocation.JOBS

                '        strQ &= ", hrjob_master.jobunkid AS allocationtranid " & _
                '                     ", ISNULL(hrjob_master.job_code,'') AS allocationtrancode" & _
                '                     ", ISNULL(hrjob_master.job_name,'') AS allocationtranname "

                '    Case enAllocation.CLASS_GROUP

                '        strQ &= ",  hrclassgroup_master.classgroupunkid AS allocationtranid " & _
                '                     ", ISNULL(hrclassgroup_master.code,'') AS allocationtrancode" & _
                '                     ", ISNULL(hrclassgroup_master.name,'') AS allocationtranname "

                '    Case enAllocation.CLASSES

                '        strQ &= ", hrclasses_master.classesunkid AS allocationtranid " & _
                '                    ", ISNULL(hrclasses_master.code,'') AS allocationtrancode" & _
                '                    ", ISNULL(hrclasses_master.name,'') AS allocationtranname "

                'End Select
                'Sohail (10 Feb 2022) -- End

                strQ &= "," & xPeriodId & " As periodunkid " & _
                            ", 0.000 AS maxbudget_amt "

                'Sohail (10 Feb 2022) -- Start
                'Enhancement :  OLD-551 : NMB - New config setting to allow Training Budget settings to be based on any allocation - Cost Center etc.
                'Select Case xCurrentAllocationId

                '    Case enAllocation.BRANCH

                '        strQ &= " FROM hrstation_master "

                '    Case enAllocation.DEPARTMENT_GROUP

                '        strQ &= " FROM hrdepartment_group_master "

                '    Case enAllocation.DEPARTMENT

                '        strQ &= " FROM hrdepartment_master "

                '    Case enAllocation.SECTION_GROUP

                '        strQ &= " FROM hrsectiongroup_master "

                '    Case enAllocation.SECTION

                '        strQ &= " FROM hrsection_master "

                '    Case enAllocation.UNIT_GROUP

                '        strQ &= " FROM hrunitgroup_master "

                '    Case enAllocation.UNIT

                '        strQ &= " FROM hrunit_master "

                '    Case enAllocation.TEAM

                '        strQ &= " FROM hrteam_master "

                '    Case enAllocation.JOB_GROUP

                '        strQ &= " FROM hrjobgroup_master  "

                '    Case enAllocation.JOBS

                '        strQ &= " FROM hrjob_master "

                '    Case enAllocation.CLASS_GROUP

                '        strQ &= " FROM hrclassgroup_master "

                '    Case enAllocation.CLASSES

                '        strQ &= " FROM hrclasses_master "

                'End Select
                strQ &= " FROM " & strTable & " "
                'Sohail (10 Feb 2022) -- End

                strQ &= " WHERE  "

                'Sohail (10 Feb 2022) -- Start
                'Enhancement :  OLD-551 : NMB - New config setting to allow Training Budget settings to be based on any allocation - Cost Center etc.
                'Select Case xCurrentAllocationId

                '    Case enAllocation.BRANCH

                '        strQ &= " hrstation_master.isactive = 1 AND hrstation_master.stationunkid NOT IN "

                '    Case enAllocation.DEPARTMENT_GROUP

                '        strQ &= " hrdepartment_group_master.isactive = 1 AND hrdepartment_group_master.deptgroupunkid NOT IN "

                '    Case enAllocation.DEPARTMENT

                '        strQ &= " hrdepartment_master.isactive = 1 AND hrdepartment_master.departmentunkid NOT IN "

                '    Case enAllocation.SECTION_GROUP

                '        strQ &= " hrsectiongroup_master.isactive = 1 AND hrsectiongroup_master.sectiongroupunkid NOT IN "

                '    Case enAllocation.SECTION

                '        strQ &= " hrsection_master.isactive = 1 AND hrsection_master.sectionunkid NOT IN "

                '    Case enAllocation.UNIT_GROUP

                '        strQ &= " hrunitgroup_master.isactive = 1 AND hrunitgroup_master.unitgroupunkid NOT IN "

                '    Case enAllocation.UNIT

                '        strQ &= " hrunit_master.isactive = 1 AND hrunit_master.unitunkid NOT IN "

                '    Case enAllocation.TEAM

                '        strQ &= " hrteam_master.iactive = 1 AND hrteam_master.teamunkid NOT IN "

                '    Case enAllocation.JOB_GROUP

                '        strQ &= " hrjobgroup_master.isactive = 1 AND hrjobgroup_master.jobgroupunkid NOT IN  "

                '    Case enAllocation.JOBS

                '        strQ &= " hrjob_master.isactive = 1 AND hrjob_master.jobunkid NOT IN "

                '    Case enAllocation.CLASS_GROUP

                '        strQ &= " hrclassgroup_master.isactive = 1 AND hrclassgroup_master.classgroupunkid NOT IN  "

                '    Case enAllocation.CLASSES

                '        strQ &= " hrclasses_master.isactive = 1 AND hrclasses_master.classesunkid NOT IN "

                'End Select
                strQ &= " " & strTable & ".isactive = 1 AND " & strTable & "." & strUnkIdField & " NOT IN "
                'Sohail (10 Feb 2022) -- End

                strQ &= " ( " & _
                            "       SELECT DISTINCT departmentunkid " & _
                            "       FROM trdepartmentaltrainingneed_master" & _
                            "       WHERE  isvoid = 0 AND periodunkid = @periodunkid AND allocationid = @allocationid " & _
                            "       AND ISNULL(refNo,'') <> '' " & _
                            " ) "

                'Sohail (10 Feb 2022) -- Start
                'Enhancement :  OLD-551 : NMB - New config setting to allow Training Budget settings to be based on any allocation - Cost Center etc.
                'Select Case xCurrentAllocationId

                '    Case enAllocation.BRANCH

                '        strQ &= " AND hrstation_master.stationunkid NOT IN "

                '    Case enAllocation.DEPARTMENT_GROUP

                '        strQ &= " AND hrdepartment_group_master.deptgroupunkid NOT IN "

                '    Case enAllocation.DEPARTMENT

                '        strQ &= " AND hrdepartment_master.departmentunkid NOT IN "

                '    Case enAllocation.SECTION_GROUP

                '        strQ &= " AND hrsectiongroup_master.sectiongroupunkid NOT IN "

                '    Case enAllocation.SECTION

                '        strQ &= " AND hrsection_master.sectionunkid NOT IN "

                '    Case enAllocation.UNIT_GROUP

                '        strQ &= " AND hrunitgroup_master.unitgroupunkid NOT IN "

                '    Case enAllocation.UNIT

                '        strQ &= " AND hrunit_master.unitunkid NOT IN "

                '    Case enAllocation.TEAM

                '        strQ &= " AND hrteam_master.teamunkid NOT IN "

                '    Case enAllocation.JOB_GROUP

                '        strQ &= " AND hrjobgroup_master.jobgroupunkid NOT IN  "

                '    Case enAllocation.JOBS

                '        strQ &= " AND hrjob_master.jobunkid NOT IN "

                '    Case enAllocation.CLASS_GROUP

                '        strQ &= " AND hrclassgroup_master.classgroupunkid NOT IN  "

                '    Case enAllocation.CLASSES

                '        strQ &= " AND hrclasses_master.classesunkid NOT IN "

                'End Select
                strQ &= " AND " & strTable & "." & strUnkIdField & " NOT IN "
                'Sohail (10 Feb 2022) -- End

                strQ &= " ( " & _
                            "      SELECT allocationtranunkid " & _
                            "      FROM trdept_maxbudget " & _
                            "      WHERE isvoid = 0 And periodunkid = @periodunkid And allocationid = @allocationid " & _
                            " ) "

                'Sohail (06 May 2021) -- Start
                'NMB Enhancement : : Changes in Departmental Training Need as per comments in online doc in New UI.
                strQ &= " UNION "

                strQ &= " SELECT " & _
                                " " & xCurrentAllocationId & " AS allocationid " & _
                                ", -99 AS allocationtranid " & _
                                ", @Company AS allocationtrancode " & _
                                ", @Company AS allocationtranname " & _
                                ", " & xPeriodId & " As periodunkid " & _
                                ", 0.000 AS maxbudget_amt " & _
                          "WHERE NOT EXISTS " & _
                                " ( " & _
                                "       SELECT DISTINCT departmentunkid " & _
                                "       FROM trdepartmentaltrainingneed_master" & _
                                "       WHERE  isvoid = 0 AND periodunkid = @periodunkid AND allocationid = @allocationid " & _
                                "       AND ISNULL(refNo,'') <> '' " & _
                                "       AND trdepartmentaltrainingneed_master.departmentunkid = -99 " & _
                                " ) " & _
                          "AND NOT EXISTS " & _
                          "( " & _
                                "SELECT allocationtranunkid " & _
                                "FROM trdept_maxbudget " & _
                                "WHERE isvoid = 0 " & _
                                      "And periodunkid = @periodunkid " & _
                                      "And allocationid = @allocationid " & _
                                      "AND trdept_maxbudget.allocationtranunkid = -99 " & _
                            ") "
                'Sohail (06 May 2021) -- End

            End If

            If xPeriodId <= 0 Then
                strQ &= " AND 1 = 2 "
            End If

            strQ &= " ORDER BY allocationtranname "

            objDataOperation.ClearParameters()
            objDataOperation.AddParameter("@periodunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, xPeriodId)
            objDataOperation.AddParameter("@allocationid", SqlDbType.Int, eZeeDataType.INT_SIZE, xCurrentAllocationId)
            objDataOperation.AddParameter("@Company", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, " " & Language.getMessage("frmDepartmentalTrainingNeedsList", 82, "Company")) 'Sohail (06 May 2021)
            dsList = objDataOperation.ExecQuery(strQ, strTableName)

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: GetList; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
        Return dsList
    End Function


    ''' <summary>
    ''' Modify By: Pinkal
    ''' </summary>
    ''' <returns>Boolean</returns>
    ''' <purpose> INSERT INTO Database Table (trdept_maxbudget) </purpose>
    Public Function Insert(Optional ByVal objDoOperation As clsDataOperation = Nothing) As Boolean
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception


        If objDoOperation Is Nothing Then
            objDataOperation = New clsDataOperation
            objDataOperation.BindTransaction()
        Else
            objDataOperation = objDoOperation
        End If

        Try

            objDataOperation.ClearParameters()

            isExist(mintAllocationid, mintAllocationtranunkid, mintPeriodunkid, objDataOperation)

            If mintUnkid <= 0 Then

                objDataOperation.ClearParameters()
                objDataOperation.AddParameter("@allocationid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintAllocationid.ToString)
                objDataOperation.AddParameter("@allocationtranunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintAllocationtranunkid.ToString)
                objDataOperation.AddParameter("@periodunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintPeriodunkid.ToString)
                objDataOperation.AddParameter("@maxbudget_amt", SqlDbType.Decimal, eZeeDataType.DECIMAL_SIZE, mdecMaxbudget_Amt.ToString)
                objDataOperation.AddParameter("@userunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintUserunkid.ToString)
                objDataOperation.AddParameter("@isvoid", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsvoid.ToString)
                objDataOperation.AddParameter("@voiduserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintVoiduserunkid.ToString)
                objDataOperation.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, IIf(mdtVoiddatetime <> Nothing, mdtVoiddatetime, DBNull.Value))
                objDataOperation.AddParameter("@voidreason", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrVoidreason.ToString)

                strQ = "INSERT INTO trdept_maxbudget ( " & _
                          "  allocationid " & _
                          ", allocationtranunkid " & _
                          ", periodunkid " & _
                          ", maxbudget_amt " & _
                          ", userunkid " & _
                          ", isvoid " & _
                          ", voiduserunkid " & _
                          ", voiddatetime " & _
                          ", voidreason" & _
                        ") VALUES (" & _
                          "  @allocationid " & _
                          ", @allocationtranunkid " & _
                          ", @periodunkid " & _
                          ", @maxbudget_amt " & _
                          ", @userunkid " & _
                          ", @isvoid " & _
                          ", @voiduserunkid " & _
                          ", @voiddatetime " & _
                          ", @voidreason" & _
                        "); SELECT @@identity"

                dsList = objDataOperation.ExecQuery(strQ, "List")

                If objDataOperation.ErrorMessage <> "" Then
                    exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                    Throw exForce
                End If

                mintUnkid = dsList.Tables(0).Rows(0).Item(0)

                If ATInsertMaxBudget(enAuditType.ADD, objDoOperation) = False Then
                    exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                    Throw exForce
                End If

            Else

                strQ = " UPDATE trdept_maxbudget SET " & _
                           "  allocationid = @allocationid" & _
                           ", allocationtranunkid = @allocationtranunkid" & _
                           ", periodunkid = @periodunkid" & _
                           ", maxbudget_amt = @maxbudget_amt" & _
                           ", userunkid = @userunkid" & _
                           ", isvoid = @isvoid" & _
                           ", voiduserunkid = @voiduserunkid" & _
                           ", voiddatetime = @voiddatetime" & _
                           ", voidreason = @voidreason " & _
                           " WHERE unkid = @unkid AND isvoid = 0"

                objDataOperation.ClearParameters()
                objDataOperation.AddParameter("@unkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintUnkid.ToString)
                objDataOperation.AddParameter("@allocationid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintAllocationid.ToString)
                objDataOperation.AddParameter("@allocationtranunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintAllocationtranunkid.ToString)
                objDataOperation.AddParameter("@periodunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintPeriodunkid.ToString)
                objDataOperation.AddParameter("@maxbudget_amt", SqlDbType.Decimal, eZeeDataType.DECIMAL_SIZE, mdecMaxbudget_Amt.ToString)
                objDataOperation.AddParameter("@userunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintUserunkid.ToString)
                objDataOperation.AddParameter("@isvoid", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsvoid.ToString)
                objDataOperation.AddParameter("@voiduserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintVoiduserunkid.ToString)
                objDataOperation.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, IIf(mdtVoiddatetime <> Nothing, mdtVoiddatetime, DBNull.Value))
                objDataOperation.AddParameter("@voidreason", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrVoidreason.ToString)

                objDataOperation.ExecNonQuery(strQ)

                If objDataOperation.ErrorMessage <> "" Then
                    exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                    Throw exForce
                End If

                If ATInsertMaxBudget(enAuditType.EDIT, objDoOperation) = False Then
                    exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                    Throw exForce
                End If

            End If

            mintUnkid = 0

            If objDoOperation Is Nothing Then
                objDataOperation.ReleaseTransaction(True)
                If HttpContext.Current IsNot Nothing AndAlso HttpContext.Current.Cache(CStr(HttpContext.Current.Session("Database_Name")) & "_DTBudgetSummaryData") IsNot Nothing Then HttpContext.Current.Cache.Remove(CStr(HttpContext.Current.Session("Database_Name")) & "_DTBudgetSummaryData") 'Sohail (10 Feb 2022)
            End If

            Return True
        Catch ex As Exception
            If objDoOperation Is Nothing Then objDataOperation.ReleaseTransaction(False)
            Throw New Exception(ex.Message & "; Procedure Name: Insert; Module Name: " & mstrModuleName)
            Return False
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            If objDoOperation Is Nothing Then objDataOperation = Nothing
        End Try
    End Function

    ''' <summary>
    ''' Modify By: Pinkal
    ''' </summary>
    ''' <returns>Boolean</returns>
    ''' <purpose> Update Database Table (trdept_maxbudget) </purpose>
    Public Function Update() As Boolean
        'If isExist(mstrName, mintUnkid) Then
        '    mstrMessage = "<Message>"
        '    Return False
        'End If

        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        objDataOperation = New clsDataOperation

        Try
            objDataOperation.AddParameter("@unkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintUnkid.ToString)
            objDataOperation.AddParameter("@allocationid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintAllocationid.ToString)
            objDataOperation.AddParameter("@allocationtranunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintAllocationtranunkid.ToString)
            objDataOperation.AddParameter("@periodunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintPeriodunkid.ToString)
            objDataOperation.AddParameter("@maxbudget_amt", SqlDbType.Decimal, eZeeDataType.DECIMAL_SIZE, mdecMaxbudget_Amt.ToString)
            objDataOperation.AddParameter("@userunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintUserunkid.ToString)
            objDataOperation.AddParameter("@isvoid", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsvoid.ToString)
            objDataOperation.AddParameter("@voiduserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintVoiduserunkid.ToString)
            objDataOperation.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, mdtVoiddatetime.ToString)
            objDataOperation.AddParameter("@voidreason", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrVoidreason.ToString)



            Call objDataOperation.ExecNonQuery(strQ)

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            If HttpContext.Current IsNot Nothing AndAlso HttpContext.Current.Cache(CStr(HttpContext.Current.Session("Database_Name")) & "_DTBudgetSummaryData") IsNot Nothing Then HttpContext.Current.Cache.Remove(CStr(HttpContext.Current.Session("Database_Name")) & "_DTBudgetSummaryData") 'Sohail (10 Feb 2022)

            Return True
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: Update; Module Name: " & mstrModuleName)
            Return False
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
    End Function

    ''' <summary>
    ''' Modify By: Pinkal
    ''' </summary>
    ''' <returns>Boolean</returns>
    ''' <purpose> Delete Database Table (trdept_maxbudget) </purpose>
    Public Function Delete(ByVal intUnkid As Integer) As Boolean
        If isUsed(intUnkid) Then
            mstrMessage = "<Message>"
            Return False
        End If

        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        objDataOperation = New clsDataOperation

        Try
            strQ = "DELETE FROM trdept_maxbudget " & _
            "WHERE unkid = @unkid "

            objDataOperation.AddParameter("@unkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intUnkid)

            Call objDataOperation.ExecNonQuery(strQ)

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            If HttpContext.Current IsNot Nothing AndAlso HttpContext.Current.Cache(CStr(HttpContext.Current.Session("Database_Name")) & "_DTBudgetSummaryData") IsNot Nothing Then HttpContext.Current.Cache.Remove(CStr(HttpContext.Current.Session("Database_Name")) & "_DTBudgetSummaryData") 'Sohail (10 Feb 2022)

            Return True
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: Update; Module Name: " & mstrModuleName)
            Return False
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
    End Function

    ''' <summary>
    ''' Modify By: Pinkal
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>
    Public Function isUsed(ByVal intUnkid As Integer) As Boolean
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        objDataOperation = New clsDataOperation

        Try
            strQ = "<Query>"

            objDataOperation.AddParameter("@unkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intUnkid)

            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            Return dsList.Tables(0).Rows.Count > 0
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: isUsed; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
    End Function

    ''' <summary>
    ''' Modify By: Pinkal
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>
    Public Function isExist(ByVal xAllocationId As Integer, ByVal xAllocationTranId As Integer, ByVal xPeriodId As Integer, Optional ByVal objDoOperation As clsDataOperation = Nothing) As Boolean
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception


        If objDoOperation Is Nothing Then
            objDataOperation = New clsDataOperation
        Else
            objDataOperation = objDoOperation
        End If

        Try
            strQ = " SELECT " & _
                      "  unkid " & _
                      ", allocationid " & _
                      ", allocationtranunkid " & _
                      ", periodunkid " & _
                      ", maxbudget_amt " & _
                      ", userunkid " & _
                      ", isvoid " & _
                      ", voiduserunkid " & _
                      ", voiddatetime " & _
                      ", voidreason " & _
                      " FROM trdept_maxbudget " & _
                      " WHERE isvoid = 0 AND periodunkid = @periodunkid AND allocationid = @allocationid AND allocationtranunkid = @allocationtranunkid "


            objDataOperation.ClearParameters()
            objDataOperation.AddParameter("@periodunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, xPeriodId)
            objDataOperation.AddParameter("@allocationid", SqlDbType.Int, eZeeDataType.INT_SIZE, xAllocationId)
            objDataOperation.AddParameter("@allocationtranunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, xAllocationTranId)
            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            If dsList IsNot Nothing AndAlso dsList.Tables(0).Rows.Count > 0 Then
                mintUnkid = CInt(dsList.Tables(0).Rows(0)("unkid"))
            End If

            Return dsList.Tables(0).Rows.Count > 0
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: isExist; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            If objDoOperation Is Nothing Then objDataOperation = Nothing
        End Try
    End Function


    ''' <summary>
    ''' Modify By: Pinkal
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>
    Public Function Save() As Boolean
        Dim exForce As Exception = Nothing
        Dim dsList As DataSet = Nothing

        Dim objDataOperation As New clsDataOperation
        objDataOperation.BindTransaction()
        Try
            objDataOperation.ClearParameters()

            If lstDeptMaxBudget IsNot Nothing AndAlso lstDeptMaxBudget.Count > 0 Then

                For Each objMaxBudget In lstDeptMaxBudget
                    With objMaxBudget
                        mintAllocationid = .pmintAllocationid
                        mintAllocationtranunkid = .pmintAllocationtranunkid
                        mintPeriodunkid = .pmintPeriodunkid
                        mdecMaxbudget_Amt = .pmdecMaxbudget_Amt
                        mblnIsvoid = .pmblnIsvoid
                        mintUserunkid = .pmintUserunkid
                        mstrClientIp = .pmstrClientIp
                        mstrHostName = .pmstrHostName
                        mstrFormName = .pmstrFormName
                        mblnIsweb = .pmblnIsweb

                        If Insert(objDataOperation) = False Then
                            If objDataOperation Is Nothing Then objDataOperation.ReleaseTransaction(False)
                            Exit For
                        End If
                    End With
                Next

            End If
            objDataOperation.ReleaseTransaction(True)
            If HttpContext.Current IsNot Nothing AndAlso HttpContext.Current.Cache(CStr(HttpContext.Current.Session("Database_Name")) & "_DTBudgetSummaryData") IsNot Nothing Then HttpContext.Current.Cache.Remove(CStr(HttpContext.Current.Session("Database_Name")) & "_DTBudgetSummaryData") 'Sohail (10 Feb 2022)

            Return True
        Catch ex As Exception
            objDataOperation.ReleaseTransaction(False)
            Throw New Exception(ex.Message & "; Procedure Name: Save; Module Name: " & mstrModuleName)
        End Try
    End Function


    ''' <summary>
    ''' Modify By: Pinkal
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>
    Public Function ATInsertMaxBudget(ByVal xAuditType As Integer, ByVal objDataOperation As clsDataOperation) As Boolean
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        Try
            strQ = "INSERT INTO attrdept_maxbudget ( " & _
                      "  unkid " & _
                      ", allocationid " & _
                      ", allocationtranunkid " & _
                      ", periodunkid " & _
                      ", maxbudget_amt " & _
                      ", audittype " & _
                      ", audituserunkid " & _
                      ", auditdatetime " & _
                      ", form_name " & _
                      ", ip " & _
                      ", machine_name " & _
                      ", isweb" & _
                    ") VALUES (" & _
                      "  @unkid " & _
                      ", @allocationid " & _
                      ", @allocationtranunkid " & _
                      ", @periodunkid " & _
                      ", @maxbudget_amt " & _
                      ", @audittype " & _
                      ", @audituserunkid " & _
                      ", GETDATE() " & _
                      ", @form_name " & _
                      ", @ip " & _
                      ", @machine_name " & _
                      ", @isweb" & _
                    "); SELECT @@identity"

            objDataOperation.ClearParameters()
            objDataOperation.AddParameter("@unkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintUnkid.ToString)
            objDataOperation.AddParameter("@allocationid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintAllocationid.ToString)
            objDataOperation.AddParameter("@allocationtranunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintAllocationtranunkid.ToString)
            objDataOperation.AddParameter("@periodunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintPeriodunkid.ToString)
            objDataOperation.AddParameter("@maxbudget_amt", SqlDbType.Decimal, eZeeDataType.DECIMAL_SIZE, mdecMaxbudget_Amt.ToString)
            objDataOperation.AddParameter("@audittype", SqlDbType.Int, eZeeDataType.INT_SIZE, xAuditType)
            objDataOperation.AddParameter("@audituserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintUserunkid)
            objDataOperation.AddParameter("@form_name", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrFormName)

            If mstrClientIp.Trim.Length > 0 Then
                objDataOperation.AddParameter("@ip", SqlDbType.NVarChar, eZeeDataType.IP_SIZE, mstrClientIp)
            Else
                objDataOperation.AddParameter("@ip", SqlDbType.NVarChar, eZeeDataType.IP_SIZE, getIP)
            End If

            If mstrHostName.Length > 0 Then
                objDataOperation.AddParameter("@machine_name", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrHostName)
            Else
                objDataOperation.AddParameter("@machine_name", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, getHostName())
            End If

            objDataOperation.AddParameter("@isweb", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsweb)

            objDataOperation.ExecNonQuery(strQ)

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            Return True
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: ATInsertMaxBudget; Module Name: " & mstrModuleName)
        End Try
    End Function


End Class
