﻿'************************************************************************************************************************************
'Class Name : clshrtraining_attendance_master.vb
'Purpose    :
'Date       :8/16/2010
'Written By :Vimal M. Gohil
'Modified   :
'************************************************************************************************************************************

Imports eZeeCommonLib
''' <summary>
''' Purpose: 
''' Developer: Vimal M. Gohil
''' </summary>


Public Class clsTraining_Attendance_Master
    'S.SANDEEP [ 20 AUG 2011 ] -- START
    'ENHANCEMENT : LANGUAGES IMPLEMENTATION
    'Private Shared ReadOnly mstrModuleName As String = "clsTraining_Attendance_Tran"
    Private Shared ReadOnly mstrModuleName As String = "clsTraining_Attendance_Master"
    'S.SANDEEP [ 20 AUG 2011 ] -- END 
    Dim objDataOperation As clsDataOperation
    Dim mstrMessage As String = ""
    Dim objAttendanTran As New clsTraining_Attendance_Tran


#Region " Private variables "
    Private mintAttendanceunkid As Integer
    Private minttrainingschedulingunkid As Integer
    Private mdtAttendance_Date As Date
    Private mstrRemark As String = String.Empty
    Private mintUserunkid As Integer
    Private mblnIsvoid As Boolean
    Private mintVoiduserunkid As Integer
    Private mdtVoiddatetime As Date
    Private mintEmployeeUnkid As Integer
#End Region

#Region " Properties "

    'S.SANDEEP [28-May-2018] -- START 
    'ISSUE/ENHANCEMENT : {Audit Trails} 
    Private mstrFormName As String = String.Empty
    Public WriteOnly Property _FormName() As String
        Set(ByVal value As String)
            mstrFormName = value
        End Set
    End Property

    Private mstrClientIP As String = ""
    Public WriteOnly Property _ClientIP() As String
        Set(ByVal value As String)
            mstrClientIP = value
        End Set
    End Property

    Private mstrHostName As String = ""
    Public WriteOnly Property _HostName() As String
        Set(ByVal value As String)
            mstrHostName = value
        End Set
    End Property

    Private mblnIsWeb As Boolean = False
    Public WriteOnly Property _FromWeb() As Boolean
        Set(ByVal value As Boolean)
            mblnIsWeb = value
        End Set
    End Property

    Private mintAuditUserId As Integer = 0
    Public WriteOnly Property _AuditUserId() As Integer
        Set(ByVal value As Integer)
            mintAuditUserId = value
        End Set
    End Property

    Private mintCompanyUnkid As Integer = 0
    Public WriteOnly Property _CompanyUnkid() As Integer
        Set(ByVal value As Integer)
            mintCompanyUnkid = value
        End Set
    End Property

    Private mdtAuditDate As DateTime = Now
    Public WriteOnly Property _AuditDate() As DateTime
        Set(ByVal value As DateTime)
            mdtAuditDate = value
        End Set
    End Property

    Private xDataOpr As clsDataOperation = Nothing
    Public Property _xDataOpr() As clsDataOperation
        Get
            Return xDataOpr
        End Get
        Set(ByVal value As clsDataOperation)
            xDataOpr = value
        End Set
    End Property

    Private mintLoginEmployeeunkid As Integer = 0
    Public Property _LoginEmployeeunkid() As Integer
        Get
            Return mintLoginEmployeeunkid
        End Get
        Set(ByVal value As Integer)
            mintLoginEmployeeunkid = value
        End Set
    End Property

    Private mintVoidlogingemployeeunkid As Integer = 0
    Public Property _Voidloginemployeeunkid() As Integer
        Get
            Return mintVoidlogingemployeeunkid
        End Get
        Set(ByVal value As Integer)
            mintVoidlogingemployeeunkid = value
        End Set
    End Property
    'S.SANDEEP [28-May-2018] -- END 

    Public ReadOnly Property _Message() As String
        Get
            Return mstrMessage
        End Get
    End Property

    ''' <summary>
    ''' Purpose: Get or Set attendanceunkid
    ''' Modify By: Vimal M. Gohil
    ''' </summary>
    Public Property _Attendanceunkid() As Integer
        Get
            Return mintAttendanceunkid
        End Get
        Set(ByVal value As Integer)
            mintAttendanceunkid = value
            Call GetData()
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set trainingschedulingunkid
    ''' Modify By: Vimal M. Gohil
    ''' </summary>
    Public Property _trainingschedulingunkid() As Integer
        Get
            Return minttrainingschedulingunkid
        End Get
        Set(ByVal value As Integer)
            minttrainingschedulingunkid = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set attendance_date
    ''' Modify By: Vimal M. Gohil
    ''' </summary>
    Public Property _Attendance_Date() As Date
        Get
            Return mdtAttendance_Date
        End Get
        Set(ByVal value As Date)
            mdtAttendance_Date = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set remark
    ''' Modify By: Vimal M. Gohil
    ''' </summary>
    Public Property _Remark() As String
        Get
            Return mstrRemark
        End Get
        Set(ByVal value As String)
            mstrRemark = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set userunkid
    ''' Modify By: Vimal M. Gohil
    ''' </summary>
    Public Property _Userunkid() As Integer
        Get
            Return mintUserunkid
        End Get
        Set(ByVal value As Integer)
            mintUserunkid = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set isvoid
    ''' Modify By: Vimal M. Gohil
    ''' </summary>
    Public Property _Isvoid() As Boolean
        Get
            Return mblnIsvoid
        End Get
        Set(ByVal value As Boolean)
            mblnIsvoid = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set voiduserunkid
    ''' Modify By: Vimal M. Gohil
    ''' </summary>
    Public Property _Voiduserunkid() As Integer
        Get
            Return mintVoiduserunkid
        End Get
        Set(ByVal value As Integer)
            mintVoiduserunkid = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set voiddatetime
    ''' Modify By: Vimal M. Gohil
    ''' </summary>
    Public Property _Voiddatetime() As Date
        Get
            Return mdtVoiddatetime
        End Get
        Set(ByVal value As Date)
            mdtVoiddatetime = value
        End Set
    End Property

    Public Property _EmployeeUnkid() As Integer
        Get
            Return mintEmployeeUnkid
        End Get
        Set(ByVal value As Integer)
            mintEmployeeUnkid = value
        End Set
    End Property

#End Region

    ''' <summary>
    ''' Modify By: Vimal M. Gohil
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>
    Public Sub GetData()
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        objDataOperation = New clsDataOperation

        Try
            strQ = "SELECT " & _
              "  attendanceunkid " & _
              ", trainingschedulingunkid " & _
              ", attendance_date " & _
              ", remark " & _
              ", userunkid " & _
              ", isvoid " & _
              ", voiduserunkid " & _
              ", voiddatetime " & _
             "FROM hrtraining_attendance_master " & _
             "WHERE attendanceunkid = @attendanceunkid "

            objDataOperation.AddParameter("@attendanceunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintAttendanceunkid.ToString)

            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            For Each dtRow As DataRow In dsList.Tables(0).Rows
                mintAttendanceunkid = CInt(dtRow.Item("attendanceunkid"))
                minttrainingschedulingunkid = CInt(dtRow.Item("trainingschedulingunkid"))
                mdtAttendance_Date = dtRow.Item("attendance_date")
                mstrRemark = dtRow.Item("remark").ToString
                mintUserunkid = CInt(dtRow.Item("userunkid"))
                mblnIsvoid = CBool(dtRow.Item("isvoid"))
                mintVoiduserunkid = CInt(dtRow.Item("voiduserunkid"))
                If IsDBNull(dtRow.Item("voiddatetime")) Then
                    mdtVoiddatetime = Nothing
                Else
                    mdtVoiddatetime = dtRow.Item("voiddatetime")
                End If
                Exit For
            Next
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: GetData; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
    End Sub


    ''' <summary>
    ''' Modify By: Vimal M. Gohil
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>
    Public Function GetList(ByVal strTableName As String, Optional ByVal blnOnlyActive As Boolean = True) As DataSet
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        objDataOperation = New clsDataOperation

        Try
            strQ = " SELECT " & _
                   " Course " & _
                   " ,AttID " & _
                   " ,CourseId " & _
                   " ,StDate " & _
                   " ,EnDate " & _
                   " ,AttendanceDate " & _
                   " ,Remark " & _
                   " ,SUM(ispresent) As ispresent " & _
                   " ,SUM(isabsent) As isabsent " & _
                  " FROM " & _
                    " ( " & _
                        " SELECT " & _
                     " hrtraining_scheduling.course_title AS Course " & _
                     ",hrtraining_attendance_master.attendanceunkid As AttID " & _
                     ",hrtraining_attendance_master.trainingschedulingunkid As CourseId " & _
                     ",CONVERT(CHAR(8),hrtraining_scheduling.start_date,112) AS StDate " & _
                     ",CONVERT(CHAR(8),hrtraining_scheduling.end_date,112) AS EnDate " & _
                     ",CONVERT(CHAR(8),hrtraining_attendance_master.attendance_date,112) AS AttendanceDate " & _
                     ",hrtraining_attendance_master.remark As Remark " & _
                     ",CASE WHEN ISNULL(hrtraining_attendance_tran.ispresent,0) = 1 THEN 1 ELSE 0 END As ispresent " & _
                     ",CASE WHEN ISNULL(hrtraining_attendance_tran.isabsent,0) = 1 THEN 1 ELSE 0 END As isabsent " & _
                     "FROM hrtraining_attendance_master " & _
                     "LEFT JOIN hrtraining_scheduling ON hrtraining_scheduling.trainingschedulingunkid = hrtraining_attendance_master.trainingschedulingunkid " & _
                     "LEFT JOIN hrtraining_attendance_tran ON hrtraining_attendance_master.attendanceunkid = hrtraining_attendance_tran.attendanceunkid " & _
                     "WHERE ISNULL(hrtraining_attendance_master.isvoid,0) = 0 " & _
                      ") As List " & _
                     "Group BY  Course " & _
                     ",AttID " & _
                     ",CourseId " & _
                     ",StDate " & _
                     ",EnDate " & _
                     ",AttendanceDate " & _
                     ",Remark "


            dsList = objDataOperation.ExecQuery(strQ, strTableName)

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: GetList; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
        Return dsList
    End Function


    ''' <summary>
    ''' Modify By: Vimal M. Gohil
    ''' </summary>
    ''' <returns>Boolean</returns>
    ''' <purpose> INSERT INTO Database Table (hrtraining_attendance_master) </purpose>
    ''' 
    Public Function Insert(ByVal dtAttendanceTran As DataTable) As Boolean
        If isExist(mdtAttendance_Date, minttrainingschedulingunkid) Then
            mstrMessage = Language.getMessage(mstrModuleName, 1, "This Attendance Date is already defined. Please define new attendance date.")
            Return False
        End If

        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        objDataOperation = New clsDataOperation
        objDataOperation.BindTransaction()

        Try
            objDataOperation.AddParameter("@trainingschedulingunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, minttrainingschedulingunkid.ToString)
            objDataOperation.AddParameter("@attendance_date", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, mdtAttendance_Date)
            objDataOperation.AddParameter("@remark", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrRemark.ToString)
            objDataOperation.AddParameter("@userunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintUserunkid.ToString)
            objDataOperation.AddParameter("@isvoid", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsvoid.ToString)
            If mdtVoiddatetime = Nothing Then
                objDataOperation.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, DBNull.Value)
            Else
                objDataOperation.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, mdtVoiddatetime)
            End If
            objDataOperation.AddParameter("@voiduserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintVoiduserunkid.ToString)

            strQ = "INSERT INTO hrtraining_attendance_master ( " & _
              "  trainingschedulingunkid " & _
              ", attendance_date " & _
              ", remark " & _
              ", userunkid " & _
              ", isvoid " & _
              ", voiduserunkid " & _
              ", voiddatetime " & _
            ") VALUES (" & _
              "  @trainingschedulingunkid " & _
              ", @attendance_date " & _
              ", @remark " & _
              ", @userunkid " & _
              ", @isvoid " & _
              ", @voiduserunkid " & _
              ", @voiddatetime " & _
            "); SELECT @@identity"

            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            mintAttendanceunkid = dsList.Tables(0).Rows(0).Item(0)


            'S.SANDEEP [ 12 OCT 2011 ] -- START
            'ENHANCEMENT : AUDIT TRAIL MAINTENANCE
            'Dim blnFlag As Boolean = False
            'If dtAttendanceTran IsNot Nothing Then
            '    objAttendanTran._AttendanceUnkid = mintAttendanceunkid
            '    objAttendanTran._DataList = dtAttendanceTran
            '    blnFlag = objAttendanTran.InserAttendanceTran()
            '    If blnFlag = False Then objDataOperation.ReleaseTransaction(True)
            'End If

            If dtAttendanceTran IsNot Nothing Then
                objAttendanTran._AttendanceUnkid = mintAttendanceunkid
                objAttendanTran._DataList = dtAttendanceTran
                With objAttendanTran
                    ._FormName = mstrFormName
                    ._LoginEmployeeunkid = mintLoginEmployeeunkid
                    ._ClientIP = mstrClientIP
                    ._HostName = mstrHostName
                    ._FromWeb = mblnIsWeb
                    ._AuditUserId = mintAuditUserId
._CompanyUnkid = mintCompanyUnkid
                    ._AuditDate = mdtAuditDate
                End With
                If objAttendanTran.InserAttendanceTran() = False Then
                    exForce = New Exception(objDataOperation.ErrorNumber & " : " & objDataOperation.ErrorMessage)
                    Throw exForce
                End If
            End If
            'S.SANDEEP [ 12 OCT 2011 ] -- END 

            objDataOperation.ReleaseTransaction(True)

            Return True
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: Insert; Module Name: " & mstrModuleName)
            Return False
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
    End Function

    'Public Function InsertOnEdit(ByVal dtAttendanceTran As DataTable) As Boolean
    '    If isExistOnEdit(mintAttendanceunkid, minttrainingschedulingunkid, mintEmployeeUnkid) Then
    '        mstrMessage = Language.getMessage(mstrModuleName, 3, "This Attendance Date is already defined. Please define new attendance date.")
    '        Return False
    '    End If

    '    Dim dsList As DataSet = Nothing
    '    Dim strQ As String = ""
    '    Dim exForce As Exception

    '    objDataOperation = New clsDataOperation
    '    objDataOperation.BindTransaction()

    '    Try
    '        objDataOperation.AddParameter("@trainingschedulingunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, minttrainingschedulingunkid.ToString)
    '        objDataOperation.AddParameter("@attendance_date", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, mdtAttendance_Date)
    '        objDataOperation.AddParameter("@remark", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrRemark.ToString)
    '        objDataOperation.AddParameter("@userunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintUserunkid.ToString)
    '        objDataOperation.AddParameter("@isvoid", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsvoid.ToString)
    '        If mdtVoiddatetime = Nothing Then
    '            objDataOperation.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, DBNull.Value)
    '        Else
    '            objDataOperation.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, mdtVoiddatetime)
    '        End If
    '        objDataOperation.AddParameter("@voiduserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintVoiduserunkid.ToString)

    '        strQ = "INSERT INTO hrtraining_attendance_master ( " & _
    '          "  trainingschedulingunkid " & _
    '          ", attendance_date " & _
    '          ", remark " & _
    '          ", userunkid " & _
    '          ", isvoid " & _
    '          ", voiduserunkid " & _
    '          ", voiddatetime " & _
    '        ") VALUES (" & _
    '          "  @trainingschedulingunkid " & _
    '          ", @attendance_date " & _
    '          ", @remark " & _
    '          ", @userunkid " & _
    '          ", @isvoid " & _
    '          ", @voiduserunkid " & _
    '          ", @voiddatetime " & _
    '        "); SELECT @@identity"

    '        dsList = objDataOperation.ExecQuery(strQ, "List")

    '        If objDataOperation.ErrorMessage <> "" Then
    '            exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
    '            Throw exForce
    '        End If

    '        mintAttendanceunkid = dsList.Tables(0).Rows(0).Item(0)

    '        Dim blnFlag As Boolean = False

    '        If dtAttendanceTran IsNot Nothing Then
    '            objAttendanTran._AttendanceUnkid = mintAttendanceunkid
    '            objAttendanTran._DataList = dtAttendanceTran
    '            blnFlag = objAttendanTran.InserAttendanceTran()
    '            If blnFlag = False Then objDataOperation.ReleaseTransaction(True)
    '        End If

    '        objDataOperation.ReleaseTransaction(True)

    '        Return True
    '    Catch ex As Exception
    '        Throw New Exception(ex.Message & "; Procedure Name: Insert; Module Name: " & mstrModuleName)
    '        Return False
    '    Finally
    '        exForce = Nothing
    '        If dsList IsNot Nothing Then dsList.Dispose()
    '        objDataOperation = Nothing
    '    End Try
    'End Function



    ''' <summary>
    ''' Modify By: Vimal M. Gohil
    ''' </summary>
    ''' <returns>Boolean</returns>
    ''' <purpose> Update Database Table (hrtraining_attendance_master) </purpose>
    Public Function Update(ByVal dtAttendanceTran As DataTable) As Boolean
        If isExist(mdtAttendance_Date, minttrainingschedulingunkid, mintAttendanceunkid) Then
            mstrMessage = Language.getMessage(mstrModuleName, 1, "This Attendance Date is already defined. Please define new attendance date.")
            Return False
        End If

        'If Row_Insert_On_Edit(mintAttendanceunkid, minttrainingschedulingunkid, ) Then
        ' objAttendanTran.InserAttendanceTran()
        'Else
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        objDataOperation = New clsDataOperation
        objDataOperation.BindTransaction()

        Try
            objDataOperation.AddParameter("@attendanceunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintAttendanceunkid.ToString)
            objDataOperation.AddParameter("@trainingschedulingunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, minttrainingschedulingunkid.ToString)
            objDataOperation.AddParameter("@attendance_date", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, mdtAttendance_Date)
            objDataOperation.AddParameter("@remark", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrRemark.ToString)
            objDataOperation.AddParameter("@userunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintUserunkid.ToString)
            objDataOperation.AddParameter("@isvoid", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsvoid.ToString)
            objDataOperation.AddParameter("@voiduserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintVoiduserunkid.ToString)
            If mdtVoiddatetime = Nothing Then
                objDataOperation.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, DBNull.Value)
            Else
                objDataOperation.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, mdtVoiddatetime)
            End If

            strQ = "UPDATE hrtraining_attendance_master SET " & _
              "  trainingschedulingunkid = @trainingschedulingunkid" & _
              ", attendance_date = @attendance_date" & _
              ", remark = @remark" & _
              ", userunkid = @userunkid" & _
              ", isvoid = @isvoid" & _
              ", voiduserunkid = @voiduserunkid" & _
              ", voiddatetime = @voiddatetime " & _
            "WHERE attendanceunkid = @attendanceunkid "

            Call objDataOperation.ExecNonQuery(strQ)

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            Dim blnFlag As Boolean = False

            If dtAttendanceTran IsNot Nothing Then
                objAttendanTran._AttendanceUnkid = mintAttendanceunkid
                objAttendanTran._DataList = dtAttendanceTran
                With objAttendanTran
                    ._FormName = mstrFormName
                    ._LoginEmployeeunkid = mintLoginEmployeeunkid
                    ._ClientIP = mstrClientIP
                    ._HostName = mstrHostName
                    ._FromWeb = mblnIsWeb
                    ._AuditUserId = mintAuditUserId
._CompanyUnkid = mintCompanyUnkid
                    ._AuditDate = mdtAuditDate
                End With
                blnFlag = objAttendanTran.UpdateAttendanceTran()
                If blnFlag = False Then objDataOperation.ReleaseTransaction(True)
            End If

            objDataOperation.ReleaseTransaction(True)

            Return True
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: Update; Module Name: " & mstrModuleName)
            Return False
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
        'End If


        'Dim dsList As DataSet = Nothing
        'Dim strQ As String = ""
        'Dim exForce As Exception

        'objDataOperation = New clsDataOperation
        'objDataOperation.BindTransaction()

        'Try
        '    objDataOperation.AddParameter("@attendanceunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintAttendanceunkid.ToString)
        '    objDataOperation.AddParameter("@trainingschedulingunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, minttrainingschedulingunkid.ToString)
        '    objDataOperation.AddParameter("@attendance_date", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, mdtAttendance_Date)
        '    objDataOperation.AddParameter("@remark", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrRemark.ToString)
        '    objDataOperation.AddParameter("@userunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintUserunkid.ToString)
        '    objDataOperation.AddParameter("@isvoid", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsvoid.ToString)
        '    objDataOperation.AddParameter("@voiduserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintVoiduserunkid.ToString)
        '    If mdtVoiddatetime = Nothing Then
        '        objDataOperation.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, DBNull.Value)
        '    Else
        '        objDataOperation.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, mdtVoiddatetime)
        '    End If

        '    strQ = "UPDATE hrtraining_attendance_master SET " & _
        '      "  trainingschedulingunkid = @trainingschedulingunkid" & _
        '      ", attendance_date = @attendance_date" & _
        '      ", remark = @remark" & _
        '      ", userunkid = @userunkid" & _
        '      ", isvoid = @isvoid" & _
        '      ", voiduserunkid = @voiduserunkid" & _
        '      ", voiddatetime = @voiddatetime " & _
        '    "WHERE attendanceunkid = @attendanceunkid "

        '    Call objDataOperation.ExecNonQuery(strQ)

        '    If objDataOperation.ErrorMessage <> "" Then
        '        exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
        '        Throw exForce
        '    End If

        '    Dim blnFlag As Boolean = False

        '    If dtAttendanceTran IsNot Nothing Then
        '        objAttendanTran._AttendanceUnkid = mintAttendanceunkid
        '        objAttendanTran._DataList = dtAttendanceTran
        '        blnFlag = objAttendanTran.UpdateAttendanceTran()
        '        If blnFlag = False Then objDataOperation.ReleaseTransaction(True)
        '    End If

        '    objDataOperation.ReleaseTransaction(True)

        '    Return True
        'Catch ex As Exception
        '    Throw New Exception(ex.Message & "; Procedure Name: Update; Module Name: " & mstrModuleName)
        '    Return False
        'Finally
        '    exForce = Nothing
        '    If dsList IsNot Nothing Then dsList.Dispose()
        '    objDataOperation = Nothing
        'End Try
    End Function

    ''' <summary>
    ''' Modify By: Vimal M. Gohil
    ''' </summary>
    ''' <returns>Boolean</returns>
    ''' <purpose> Delete Database Table (hrtraining_attendance_master) </purpose>
    Public Function Delete(ByVal intUnkid As Integer) As Boolean
        'If isUsed(intUnkid) Then
        '    mstrMessage = "<Message>"
        '    Return False
        'End If

        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        objDataOperation = New clsDataOperation
        objDataOperation.BindTransaction()

        Try
            strQ = "UPDATE hrtraining_attendance_master SET " & _
                   "  isvoid = @isvoid " & _
                   ", voiddatetime = @voiddatetime " & _
                   ", voiduserunkid = @voiduserunkid " & _
                   "WHERE attendanceunkid = @attendanceunkid "

            objDataOperation.AddParameter("@attendanceunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intUnkid)
            objDataOperation.AddParameter("@isvoid", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsvoid.ToString)
            objDataOperation.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, mdtVoiddatetime)
            objDataOperation.AddParameter("@voiduserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintVoiduserunkid.ToString)

            Call objDataOperation.ExecNonQuery(strQ)

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If


            Dim objCommonATLog As New clsCommonATLog
            Dim dsTran As DataSet = objCommonATLog.GetChildList(objDataOperation, "hrtraining_attendance_tran", "attendanceunkid", intUnkid)
            objCommonATLog = Nothing
            If dsTran.Tables(0).Rows.Count > 0 Then
                For Each dRow As DataRow In dsTran.Tables(0).Rows
                    'S.SANDEEP [28-May-2018] -- START
                    'ISSUE/ENHANCEMENT : {Audit Trails} 
                    objCommonATLog = New clsCommonATLog
                    objCommonATLog._FormName = mstrFormName
                    objCommonATLog._LoginEmployeeUnkid = mintLoginEmployeeunkid
                    objCommonATLog._ClientIP = mstrClientIP
                    objCommonATLog._HostName = mstrHostName
                    objCommonATLog._FromWeb = mblnIsWeb
                    objCommonATLog._AuditUserId = mintAuditUserId
objCommonATLog._CompanyUnkid = mintCompanyUnkid
                    objCommonATLog._AuditDate = mdtAuditDate
                    'S.SANDEEP [28-May-2018] -- END

                    If objCommonATLog.Insert_TranAtLog(objDataOperation, "hrtraining_attendance_master", "attendanceunkid", intUnkid, "hrtraining_attendance_tran", "attendancetranunkid", dRow.Item("attendancetranunkid"), 3, 3) = False Then
                        exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                        Throw exForce
                    End If

                    'S.SANDEEP [28-May-2018] -- START
                    'ISSUE/ENHANCEMENT : {Audit Trails} 
                    objCommonATLog = Nothing
                    'S.SANDEEP [28-May-2018] -- END

                Next
            End If

            objAttendanTran._AttendanceUnkid = intUnkid
            objAttendanTran._FormName = mstrFormName
            objAttendanTran._LoginEmployeeunkid = mintLoginEmployeeunkid
            objAttendanTran._ClientIP = mstrClientIP
            objAttendanTran._HostName = mstrHostName
            objAttendanTran._FromWeb = mblnIsWeb
            objAttendanTran._AuditUserId = mintAuditUserId
objAttendanTran._CompanyUnkid = mintCompanyUnkid
            objAttendanTran._AuditDate = mdtAuditDate
            If objAttendanTran.Void_All(mblnIsvoid, mdtVoiddatetime, mintVoiduserunkid) = False Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If
            'S.SANDEEP [ 12 OCT 2011 ] -- END 

            objDataOperation.ReleaseTransaction(True)

            Return True
        Catch ex As Exception
            'S.SANDEEP [ 12 OCT 2011 ] -- START
            'ENHANCEMENT : AUDIT TRAIL MAINTENANCE
            objDataOperation.ReleaseTransaction(False)
            'S.SANDEEP [ 12 OCT 2011 ] -- END 
            Throw New Exception(ex.Message & "; Procedure Name: Delete; Module Name: " & mstrModuleName)
            Return False
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
    End Function

    ''' <summary>
    ''' Modify By: Vimal M. Gohil
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>
    Public Function isUsed(ByVal intUnkid As Integer) As Boolean
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        objDataOperation = New clsDataOperation

        Try
            strQ = "<Query>"

            objDataOperation.AddParameter("@attendanceunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intUnkid)

            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            Return dsList.Tables(0).Rows.Count > 0
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: isUsed; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
    End Function

    ''' <summary>
    ''' Modify By: Vimal M. Gohil
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>
    Public Function isExist(ByVal dtDate As Date, ByVal intCourseId As Integer, Optional ByVal intUnkid As Integer = -1) As Boolean
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        objDataOperation = New clsDataOperation

        Try
            strQ = "SELECT " & _
              "  attendanceunkid " & _
              ", trainingschedulingunkid " & _
              ", attendance_date " & _
              ", remark " & _
              ", userunkid " & _
              ", isvoid " & _
              ", voiduserunkid " & _
              ", voiddatetime " & _
             "FROM hrtraining_attendance_master " & _
             "WHERE CONVERT(CHAR(8),attendance_date,112) = @attendance_date " & _
             "AND trainingschedulingunkid = @trainingschedulingunkid " & _
             "AND ISNULL(isvoid,0) = 0 "

            If intUnkid > 0 Then
                strQ &= " AND attendanceunkid <> @attendanceunkid "
            End If

            objDataOperation.AddParameter("@attendance_date", SqlDbType.NVarChar, eZeeDataType.DATETIME_SIZE, eZeeDate.convertDate(dtDate))
            objDataOperation.AddParameter("@trainingschedulingunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intCourseId)
            objDataOperation.AddParameter("@attendanceunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intUnkid)

            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            Return dsList.Tables(0).Rows.Count > 0
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: isExist; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
    End Function


    'S.SANDEEP [04 JUN 2015] -- START
    'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS

    Public Function Get_Employee_List(ByVal xDatabaseName As String, _
                                      ByVal xUserUnkid As Integer, _
                                      ByVal xYearUnkid As Integer, _
                                      ByVal xCompanyUnkid As Integer, _
                                      ByVal xPeriodStart As DateTime, _
                                      ByVal xPeriodEnd As DateTime, _
                                      ByVal xUserModeSetting As String, _
                                      ByVal xOnlyApproved As Boolean, _
                                      ByVal xIncludeIn_ActiveEmployee As Boolean, _
                                      ByVal intTrainingScheduleUnkid As Integer, ByVal strTableName As String) As DataSet
        Dim dsList As DataSet = Nothing
        Dim StrQ As String = ""
        Dim exForce As Exception
        Try
            Dim xDateJoinQry, xDateFilterQry As String
            xDateJoinQry = "" : xDateFilterQry = ""
            Call GetDatesFilterString(xDateJoinQry, xDateFilterQry, xPeriodStart, xPeriodEnd, , , xDatabaseName)

            Using objDo As New clsDataOperation
                StrQ = "SELECT " & _
                        "   ISNULL(hremployee_master.firstname, '') + ' ' + ISNULL(hremployee_master.othername, '') + ' ' + ISNULL(hremployee_master.surname, '') AS EmpName " & _
                        "  ,hrtraining_enrollment_tran.employeeunkid " & _
                        "  ,hrtraining_enrollment_tran.enroll_remark " & _
                        "FROM hrtraining_enrollment_tran " & _
                        "  LEFT JOIN hremployee_master ON hrtraining_enrollment_tran.employeeunkid = hremployee_master.employeeunkid "

                If xDateJoinQry.Trim.Length > 0 Then
                    StrQ &= xDateJoinQry & " "
                End If

                StrQ &= "WHERE hrtraining_enrollment_tran.trainingschedulingunkid = @trainingschedulingunkid " & _
                        " AND ISNULL(hrtraining_enrollment_tran.isvoid,0) = 0 " & _
                        " AND ISNULL(hrtraining_enrollment_tran.iscancel,0) = 0 "

                If xIncludeIn_ActiveEmployee = False Then
                    If xDateFilterQry.Trim.Length > 0 Then
                        StrQ &= xDateFilterQry & " "
                    End If
                End If

                objDo.AddParameter("@trainingschedulingunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intTrainingScheduleUnkid)

                dsList = objDataOperation.ExecQuery(StrQ, strTableName)

                If objDo.ErrorMessage <> "" Then
                    exForce = New Exception(objDo.ErrorNumber & ": " & objDo.ErrorMessage)
                    Throw exForce
                End If

            End Using
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: Get_Employee_List; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
        End Try
        Return dsList
    End Function

    'Public Function Get_Employee_List(ByVal intTrainingScheduleUnkid As Integer, ByVal strTableName As String) As DataSet
    '    Dim dsList As DataSet = Nothing
    '    Dim strQ As String = ""
    '    Dim exForce As Exception

    '    objDataOperation = New clsDataOperation

    '    Try
    '        'Sandeep [ 02 Oct 2010 ] -- Start
    '        'strQ &= "SELECT " & _
    '        '                "ISNULL(hremployee_master.firstname,'')+' '+ISNULL(hremployee_master.othername,'')+' '+ISNULL(hremployee_master.surname,'') AS EmpName " & _
    '        '                ",hrtraining_enrollment_tran.employeeunkid " & _
    '        '                ",hrtraining_enrollment_tran.enroll_remark " & _
    '        '                "FROM hrtraining_enrollment_tran " & _
    '        '                "LEFT JOIN hremployee_master " & _
    '        '                "ON hrtraining_enrollment_tran.employeeunkid = hremployee_master.employeeunkid " & _
    '        '                "WHERE hrtraining_enrollment_tran.trainingschedulingunkid = @trainingschedulingunkid " & _
    '        '                "AND hrtraining_enrollment_tran.isvoid = 0 "
    '        strQ &= "SELECT " & _
    '                        "    ISNULL(hremployee_master.firstname, '') + ' ' " & _
    '                        "    + ISNULL(hremployee_master.othername, '') + ' ' " & _
    '                        "    + ISNULL(hremployee_master.surname, '') AS EmpName " & _
    '                        "   ,hrtraining_enrollment_tran.employeeunkid " & _
    '                        "   ,hrtraining_enrollment_tran.enroll_remark " & _
    '                        "FROM hrtraining_enrollment_tran " & _
    '                        "    LEFT JOIN hremployee_master ON hrtraining_enrollment_tran.employeeunkid = hremployee_master.employeeunkid " & _
    '                        "WHERE hrtraining_enrollment_tran.trainingschedulingunkid = @trainingschedulingunkid " & _
    '                        "    AND ISNULL(hrtraining_enrollment_tran.isvoid,0) = 0 " & _
    '                        "    AND ISNULL(hrtraining_enrollment_tran.iscancel,0) = 0 "


    '        'S.SANDEEP [ 29 JUNE 2011 ] -- START
    '        'ISSUE : INACTIVE EMPLOYEE(S) COMING ON LIST
    '        If ConfigParameter._Object._IsIncludeInactiveEmp = False Then
    '            'Sohail (06 Jan 2012) -- Start
    '            'TRA - ENHANCEMENT
    '            'strQ &= " AND ISNULL(hremployee_master.isactive,0) = 1 "
    '            strQ &= " AND CONVERT(CHAR(8),hremployee_master.appointeddate,112) <= @enddate " & _
    '                       " AND ISNULL(CONVERT(CHAR(8),hremployee_master.termination_from_date,112),@startdate) >= @startdate " & _
    '                       " AND ISNULL(CONVERT(CHAR(8),hremployee_master.termination_to_date,112),@startdate) >= @startdate " & _
    '                       " AND ISNULL(CONVERT(CHAR(8),hremployee_master.empl_enddate,112), @startdate) >= @startdate " 'S.SANDEEP [ 15 MAY 2012 empl_enddate ] -- START -- END

    '            objDataOperation.AddParameter("@startdate", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, ConfigParameter._Object._EmployeeAsOnDate)
    '            objDataOperation.AddParameter("@enddate", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, ConfigParameter._Object._EmployeeAsOnDate)
    '            'Sohail (06 Jan 2012) -- End
    '        End If
    '        'S.SANDEEP [ 29 JUNE 2011 ] -- END 


    '        'Sandeep [ 02 Oct 2010 ] -- End 

    '        objDataOperation.AddParameter("@trainingschedulingunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intTrainingScheduleUnkid)

    '        dsList = objDataOperation.ExecQuery(strQ, strTableName)

    '        If objDataOperation.ErrorMessage <> "" Then
    '            exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
    '            Throw exForce
    '        End If

    '        Return dsList

    '    Catch ex As Exception
    '        Throw New Exception(ex.Message & "; Procedure Name: Get_Employee_List; Module Name: " & mstrModuleName)
    '    End Try
    'End Function
    '
    '
    '
    '
    '
    '
    '
    'Public Function Get_Employee_List_For_Edit(ByVal intTrainingScheduleUnkid As Integer, ByVal intAttendanceUnkid As Integer, ByVal strTableName As String) As DataSet
    '    Dim dsList As DataSet = Nothing
    '    Dim strQ As String = ""
    '    Dim exForce As Exception

    '    objDataOperation = New clsDataOperation

    '    Try
    '        'strQ &= "SELECT " & _
    '        '    "ISNULL(hremployee_master.firstname,'')+' '+ISNULL(hremployee_master.othername,'')+' '+ISNULL(hremployee_master.surname,'') AS EmpName " & _
    '        '    ",hrtraining_enrollment_tran.employeeunkid " & _
    '        '    ",hrtraining_enrollment_tran.enroll_remark " & _
    '        '    "FROM hrtraining_enrollment_tran " & _
    '        '    "LEFT JOIN hremployee_master " & _
    '        '    "ON hrtraining_enrollment_tran.employeeunkid = hremployee_master.employeeunkid " & _
    '        '    "WHERE hrtraining_enrollment_tran.trainingschedulingunkid = @trainingschedulingunkid " & _
    '        '    "AND hrtraining_enrollment_tran.isvoid = 0 "


    '        'Sandeep [ 02 Oct 2010 ] -- Start
    '        'strQ = "SELECT " & _
    '        '       "ISNULL(hremployee_master.firstname,'')+' '+ISNULL(hremployee_master.othername,'')+' '+ISNULL(hremployee_master.surname,'') AS EmpName " & _
    '        '       ",hrtraining_enrollment_tran.employeeunkid " & _
    '        '       "FROM hrtraining_enrollment_tran " & _
    '        '       "JOIN hrtraining_scheduling ON hrtraining_enrollment_tran.trainingschedulingunkid = hrtraining_scheduling.trainingschedulingunkid " & _
    '        '       "LEFT JOIN hremployee_master ON hrtraining_enrollment_tran.employeeunkid = hremployee_master.employeeunkid " & _
    '        '       "WHERE hrtraining_scheduling.trainingschedulingunkid = @trainingschedulingunkid " & _
    '        '       "AND hrtraining_enrollment_tran.isvoid = 0 " & _
    '        '       "AND hrtraining_enrollment_tran.employeeunkid NOT IN ( SELECT employeeunkid " & _
    '        '                                    "FROM hrtraining_attendance_tran " & _
    '        '                                    "WHERE attendanceunkid = @attendanceunkid " & _
    '        '                                    "AND isvoid = 0 ) "


    '        'S.SANDEEP [ 12 OCT 2011 ] -- START
    '        'ENHANCEMENT : AUDIT TRAIL MAINTENANCE
    '        'strQ = "SELECT " & _
    '        '            "    ISNULL(hremployee_master.firstname, '') + ' ' " & _
    '        '            "    + ISNULL(hremployee_master.othername, '') + ' ' " & _
    '        '            "    + ISNULL(hremployee_master.surname, '') AS EmpName " & _
    '        '            "   ,hrtraining_enrollment_tran.employeeunkid " & _
    '        '            "FROM hrtraining_enrollment_tran " & _
    '        '            "    JOIN hrtraining_scheduling ON hrtraining_enrollment_tran.trainingschedulingunkid = hrtraining_scheduling.trainingschedulingunkid " & _
    '        '            "    LEFT JOIN hremployee_master ON hrtraining_enrollment_tran.employeeunkid = hremployee_master.employeeunkid " & _
    '        '            "WHERE hrtraining_scheduling.trainingschedulingunkid = @trainingschedulingunkid " & _
    '        '            "    AND hrtraining_enrollment_tran.isvoid = 0 " & _
    '        '            "    AND hrtraining_enrollment_tran.employeeunkid NOT IN " & _
    '        '            "    (SELECT employeeunkid FROM hrtraining_attendance_tran WHERE attendanceunkid = @attendanceunkid AND isvoid = 0 ) "

    '        strQ = "SELECT " & _
    '                    "    ISNULL(hremployee_master.firstname, '') + ' ' " & _
    '                    "    + ISNULL(hremployee_master.othername, '') + ' ' " & _
    '                    "    + ISNULL(hremployee_master.surname, '') AS EmpName " & _
    '                    "   ,hrtraining_enrollment_tran.employeeunkid " & _
    '                    "FROM hrtraining_enrollment_tran " & _
    '                    "    JOIN hrtraining_scheduling ON hrtraining_enrollment_tran.trainingschedulingunkid = hrtraining_scheduling.trainingschedulingunkid " & _
    '                    "    LEFT JOIN hremployee_master ON hrtraining_enrollment_tran.employeeunkid = hremployee_master.employeeunkid " & _
    '                    "WHERE hrtraining_scheduling.trainingschedulingunkid = @trainingschedulingunkid " & _
    '                    "    AND hrtraining_enrollment_tran.isvoid = 0 " & _
    '                    "    AND hrtraining_enrollment_tran.iscancel = 0 " & _
    '                    "    AND hrtraining_enrollment_tran.employeeunkid NOT IN " & _
    '                    "    (SELECT employeeunkid FROM hrtraining_attendance_tran WHERE attendanceunkid = @attendanceunkid AND isvoid = 0 ) "
    '        'S.SANDEEP [ 12 OCT 2011 ] -- END 

    '        'Sandeep [ 02 Oct 2010 ] -- End 


    '        'S.SANDEEP [ 29 JUNE 2011 ] -- START
    '        'ISSUE : INACTIVE EMPLOYEE(S) COMING ON LIST
    '        If ConfigParameter._Object._IsIncludeInactiveEmp = False Then
    '            'Sohail (06 Jan 2012) -- Start
    '            'TRA - ENHANCEMENT
    '            'strQ &= " AND ISNULL(hremployee_master.isactive,0) = 1 "
    '            strQ &= " AND CONVERT(CHAR(8),hremployee_master.appointeddate,112) <= @enddate " & _
    '                       " AND ISNULL(CONVERT(CHAR(8),hremployee_master.termination_from_date,112),@startdate) >= @startdate " & _
    '                    " AND ISNULL(CONVERT(CHAR(8),hremployee_master.termination_to_date,112),@startdate) >= @startdate " & _
    '                    " AND ISNULL(CONVERT(CHAR(8),hremployee_master.empl_enddate,112), @startdate) >= @startdate " 'S.SANDEEP [ 15 MAY 2012 empl_enddate ] -- START -- END

    '            objDataOperation.AddParameter("@startdate", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, ConfigParameter._Object._EmployeeAsOnDate)
    '            objDataOperation.AddParameter("@enddate", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, ConfigParameter._Object._EmployeeAsOnDate)
    '            'Sohail (06 Jan 2012) -- End
    '        End If
    '        'S.SANDEEP [ 29 JUNE 2011 ] -- END 


    '        objDataOperation.AddParameter("@trainingschedulingunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intTrainingScheduleUnkid)
    '        objDataOperation.AddParameter("@attendanceunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intAttendanceUnkid)

    '        dsList = objDataOperation.ExecQuery(strQ, strTableName)

    '        If objDataOperation.ErrorMessage <> "" Then
    '            exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
    '            Throw exForce
    '        End If

    '        Return dsList

    '    Catch ex As Exception
    '        Throw New Exception(ex.Message & "; Procedure Name: Get_Employee_List; Module Name: " & mstrModuleName)
    '    End Try
    'End Function
    '
    '
    '
    '
    '
    '
    '
    'Public Function GetEmpAttendanceList(ByVal intUnkId As Integer, ByVal strTableName As String, Optional ByVal blnOnlyActive As Boolean = True) As DataSet
    '    Dim dsList As DataSet = Nothing
    '    Dim strQ As String = ""
    '    Dim exForce As Exception

    '    objDataOperation = New clsDataOperation

    '    Try
    '        strQ = " SELECT " & _
    '               " hrtraining_attendance_tran.attendancetranunkid " & _
    '               ",ISNULL(hremployee_master.firstname,'')+' '+ISNULL(hremployee_master.othername,'')+' '+ISNULL(hremployee_master.surname,'') AS EmpName " & _
    '               ",hrtraining_attendance_tran.employeeunkid " & _
    '               ",hrtraining_attendance_tran.ispresent " & _
    '               ",hrtraining_attendance_tran.isabsent " & _
    '               ",hrtraining_attendance_tran.remark " & _
    '               "FROM hrtraining_attendance_tran " & _
    '               "LEFT JOIN hremployee_master " & _
    '               "ON hrtraining_attendance_tran.employeeunkid = hremployee_master.employeeunkid " & _
    '               "WHERE hrtraining_attendance_tran.attendanceunkid = @attendanceunkid "


    '        'S.SANDEEP [ 29 JUNE 2011 ] -- START
    '        'ISSUE : INACTIVE EMPLOYEE(S) COMING ON LIST
    '        If ConfigParameter._Object._IsIncludeInactiveEmp = False Then
    '            'Sohail (06 Jan 2012) -- Start
    '            'TRA - ENHANCEMENT
    '            'strQ &= " AND ISNULL(hremployee_master.isactive,0) = 1 "
    '            strQ &= " AND CONVERT(CHAR(8),hremployee_master.appointeddate,112) <= @enddate " & _
    '                       " AND ISNULL(CONVERT(CHAR(8),hremployee_master.termination_from_date,112),@startdate) >= @startdate " & _
    '                       " AND ISNULL(CONVERT(CHAR(8),hremployee_master.termination_to_date,112),@startdate) >= @startdate " & _
    '                       " AND ISNULL(CONVERT(CHAR(8),hremployee_master.empl_enddate,112), @startdate) >= @startdate " 'S.SANDEEP [ 15 MAY 2012 empl_enddate ] -- START -- END

    '            objDataOperation.AddParameter("@startdate", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, ConfigParameter._Object._EmployeeAsOnDate)
    '            objDataOperation.AddParameter("@enddate", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, ConfigParameter._Object._EmployeeAsOnDate)
    '            'Sohail (06 Jan 2012) -- End
    '        End If
    '        'S.SANDEEP [ 29 JUNE 2011 ] -- END 

    '        objDataOperation.AddParameter("@attendanceunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intUnkId)

    '        dsList = objDataOperation.ExecQuery(strQ, strTableName)

    '        If objDataOperation.ErrorMessage <> "" Then
    '            exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
    '            Throw exForce
    '        End If

    '        Return dsList

    '    Catch ex As Exception
    '        Throw New Exception(ex.Message & "; Procedure Name: GetEmpAttendanceList; Module Name: " & mstrModuleName)
    '    Finally
    '        exForce = Nothing
    '        If dsList IsNot Nothing Then dsList.Dispose()
    '        objDataOperation = Nothing
    '    End Try
    '    Return dsList
    'End Function

    Public Function Get_Employee_List_For_Edit(ByVal xDatabaseName As String, _
                                               ByVal xUserUnkid As Integer, _
                                               ByVal xYearUnkid As Integer, _
                                               ByVal xCompanyUnkid As Integer, _
                                               ByVal xPeriodStart As DateTime, _
                                               ByVal xPeriodEnd As DateTime, _
                                               ByVal xUserModeSetting As String, _
                                               ByVal xOnlyApproved As Boolean, _
                                               ByVal xIncludeIn_ActiveEmployee As Boolean, _
                                               ByVal intTrainingScheduleUnkid As Integer, _
                                               ByVal intAttendanceUnkid As Integer, _
                                               ByVal strTableName As String) As DataSet
        Dim dsList As DataSet = Nothing
        Dim StrQ As String = ""
        Dim exForce As Exception
        Try
            Dim xDateJoinQry, xDateFilterQry As String
            xDateJoinQry = "" : xDateFilterQry = ""
            Call GetDatesFilterString(xDateJoinQry, xDateFilterQry, xPeriodStart, xPeriodEnd, , , xDatabaseName)

            Using objDo As New clsDataOperation
                StrQ = "SELECT " & _
                       "  ISNULL(hremployee_master.firstname, '') + ' ' + ISNULL(hremployee_master.othername, '') + ' ' + ISNULL(hremployee_master.surname, '') AS EmpName " & _
                       " ,hrtraining_enrollment_tran.employeeunkid " & _
                       "FROM hrtraining_enrollment_tran " & _
                       " JOIN hrtraining_scheduling ON hrtraining_enrollment_tran.trainingschedulingunkid = hrtraining_scheduling.trainingschedulingunkid " & _
                       " LEFT JOIN hremployee_master ON hrtraining_enrollment_tran.employeeunkid = hremployee_master.employeeunkid "

                If xDateJoinQry.Trim.Length > 0 Then
                    StrQ &= xDateJoinQry & " "
                End If

                StrQ &= "WHERE hrtraining_scheduling.trainingschedulingunkid = @trainingschedulingunkid " & _
                        "    AND hrtraining_enrollment_tran.isvoid = 0 " & _
                        "    AND hrtraining_enrollment_tran.iscancel = 0 " & _
                        "    AND hrtraining_enrollment_tran.employeeunkid NOT IN " & _
                        "    (SELECT employeeunkid FROM hrtraining_attendance_tran WHERE attendanceunkid = @attendanceunkid AND isvoid = 0 ) "

                If xIncludeIn_ActiveEmployee = False Then
                    If xDateFilterQry.Trim.Length > 0 Then
                        StrQ &= xDateFilterQry & " "
                    End If
                End If

                objDo.AddParameter("@trainingschedulingunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intTrainingScheduleUnkid)
                objDo.AddParameter("@attendanceunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intAttendanceUnkid)

                dsList = objDo.ExecQuery(StrQ, strTableName)

                If objDo.ErrorMessage <> "" Then
                    exForce = New Exception(objDo.ErrorNumber & ": " & objDo.ErrorMessage)
                    Throw exForce
                End If

            End Using

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: Get_Employee_List_For_Edit; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
        End Try
        Return dsList
    End Function

    Public Function GetEmpAttendanceList(ByVal xDatabaseName As String, _
                                         ByVal xUserUnkid As Integer, _
                                         ByVal xYearUnkid As Integer, _
                                         ByVal xCompanyUnkid As Integer, _
                                         ByVal xPeriodStart As DateTime, _
                                         ByVal xPeriodEnd As DateTime, _
                                         ByVal xUserModeSetting As String, _
                                         ByVal xOnlyApproved As Boolean, _
                                         ByVal xIncludeIn_ActiveEmployee As Boolean, _
                                         ByVal intUnkId As Integer, ByVal strTableName As String) As DataSet
        Dim dsList As DataSet = Nothing
        Dim StrQ As String = ""
        Dim exForce As Exception
        Try
            Dim xDateJoinQry, xDateFilterQry As String
            xDateJoinQry = "" : xDateFilterQry = ""
            Call GetDatesFilterString(xDateJoinQry, xDateFilterQry, xPeriodStart, xPeriodEnd, , , xDatabaseName)

            Using objDo As New clsDataOperation

                StrQ = " SELECT " & _
                   " hrtraining_attendance_tran.attendancetranunkid " & _
                   ",ISNULL(hremployee_master.firstname,'')+' '+ISNULL(hremployee_master.othername,'')+' '+ISNULL(hremployee_master.surname,'') AS EmpName " & _
                   ",hrtraining_attendance_tran.employeeunkid " & _
                   ",hrtraining_attendance_tran.ispresent " & _
                   ",hrtraining_attendance_tran.isabsent " & _
                   ",hrtraining_attendance_tran.remark " & _
                   "FROM hrtraining_attendance_tran " & _
                   "LEFT JOIN hremployee_master " & _
                   "ON hrtraining_attendance_tran.employeeunkid = hremployee_master.employeeunkid "

                If xDateJoinQry.Trim.Length > 0 Then
                    StrQ &= xDateJoinQry & " "
                End If

                StrQ &= "WHERE hrtraining_attendance_tran.attendanceunkid = @attendanceunkid "

                If xIncludeIn_ActiveEmployee = False Then
                    If xDateFilterQry.Trim.Length > 0 Then
                        StrQ &= xDateFilterQry & " "
                    End If
                End If

                objDo.AddParameter("@attendanceunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intUnkId)

                dsList = objDo.ExecQuery(StrQ, strTableName)

                If objDo.ErrorMessage <> "" Then
                    exForce = New Exception(objDo.ErrorNumber & ": " & objDo.ErrorMessage)
                    Throw exForce
                End If

            End Using

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: GetEmpAttendanceList; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
        End Try
        Return dsList
    End Function
    'S.SANDEEP [04 JUN 2015] -- END

    '<Language> This Auto Generated Text Please Do Not Modify it.
#Region " Language & UI Settings "
	Public Shared Sub SetMessages()
		Try
			Language.setMessage(mstrModuleName, 1, "This Attendance Date is already defined. Please define new attendance date.")

		Catch Ex As Exception
			DisplayError.Show("-1", ex.Message, "SetMessages", mstrModuleName)
		End Try
	End Sub
#End Region 'Language & UI Settings
	'</Language>
End Class
