﻿Imports eZeeCommonLib
Imports System.Data.SqlClient

Public Class clsDynamicNavision

#Region " Private Variables "
    Private Shared ReadOnly mstrModuleName As String = "clsDynamicNavision"
    Private mstrMessage As String = ""
    Private mstrDatabaseServer As String = ""
    Private mstrDatabaseName As String = ""
    Private mstrDatabaseUserName As String = ""
    Private mstrDatabasePassword As String = ""
    Private mdtTran As DataTable
#End Region

#Region " Properties "

    ''' <summary>
    ''' Purpose: Get or Set DatabaseServer
    ''' Modify By: Hemant
    ''' </summary>
    Public Property _DatabaseServer() As String
        Get
            Return mstrDatabaseServer
        End Get
        Set(ByVal value As String)
            mstrDatabaseServer = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set DatabaseName
    ''' Modify By: Hemant
    ''' </summary>
    Public Property _DatabaseName() As String
        Get
            Return mstrDatabaseName
        End Get
        Set(ByVal value As String)
            mstrDatabaseName = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set DatabaseUserName
    ''' Modify By: Hemant
    ''' </summary>
    Public Property _DatabaseUserName() As String
        Get
            Return mstrDatabaseUserName
        End Get
        Set(ByVal value As String)
            mstrDatabaseUserName = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set DatabasePassword
    ''' Modify By: Hemant
    ''' </summary>
    Public Property _DatabasePassword() As String
        Get
            Return mstrDatabasePassword
        End Get
        Set(ByVal value As String)
            mstrDatabasePassword = value
        End Set
    End Property

    Public Property _DataTable() As DataTable
        Get
            Return mdtTran
        End Get
        Set(ByVal value As DataTable)
            mdtTran = value
        End Set
    End Property


#End Region

#Region " Contructor "

    Public Sub New()
        Try
            mdtTran = New DataTable("File")
            mdtTran.Columns.Add("Ref_No_", GetType(String)).DefaultValue = ""
            mdtTran.Columns(mdtTran.Columns.Count - 1).Caption = Language.getMessage(mstrModuleName, 1, "Ref_No_")
            mdtTran.Columns.Add("Account_Type", GetType(Integer)).DefaultValue = 0
            mdtTran.Columns(mdtTran.Columns.Count - 1).Caption = Language.getMessage(mstrModuleName, 2, "Account_Type")
            mdtTran.Columns.Add("Account_No", GetType(String)).DefaultValue = ""
            mdtTran.Columns(mdtTran.Columns.Count - 1).Caption = Language.getMessage(mstrModuleName, 3, "Account_No")
            mdtTran.Columns.Add("Bal__Account_Type", GetType(Integer)).DefaultValue = 0
            mdtTran.Columns(mdtTran.Columns.Count - 1).Caption = Language.getMessage(mstrModuleName, 4, "Bal__Account_Type")
            mdtTran.Columns.Add("Bal__Account_No", GetType(String)).DefaultValue = ""
            mdtTran.Columns(mdtTran.Columns.Count - 1).Caption = Language.getMessage(mstrModuleName, 5, "Bal__Account_No")
            mdtTran.Columns.Add("Amount", GetType(Decimal)).DefaultValue = 0
            mdtTran.Columns(mdtTran.Columns.Count - 1).Caption = Language.getMessage(mstrModuleName, 6, "Amount")
            mdtTran.Columns.Add("Date", GetType(DateTime)).DefaultValue = DBNull.Value
            mdtTran.Columns(mdtTran.Columns.Count - 1).Caption = Language.getMessage(mstrModuleName, 7, "Date")
            mdtTran.Columns.Add("Created_By", GetType(String)).DefaultValue = ""
            mdtTran.Columns(mdtTran.Columns.Count - 1).Caption = Language.getMessage(mstrModuleName, 8, "Created_By")
            mdtTran.Columns.Add("Created_Date", GetType(DateTime)).DefaultValue = DBNull.Value
            mdtTran.Columns(mdtTran.Columns.Count - 1).Caption = Language.getMessage(mstrModuleName, 9, "Created_Date")
            mdtTran.Columns.Add("Status", GetType(Integer)).DefaultValue = 0
            mdtTran.Columns(mdtTran.Columns.Count - 1).Caption = Language.getMessage(mstrModuleName, 10, "Status")
            mdtTran.Columns.Add("Cost_Centre_Code", GetType(String)).DefaultValue = ""
            mdtTran.Columns(mdtTran.Columns.Count - 1).Caption = Language.getMessage(mstrModuleName, 11, "Cost_Centre_Code")
            mdtTran.Columns.Add("Department", GetType(String)).DefaultValue = ""
            mdtTran.Columns(mdtTran.Columns.Count - 1).Caption = Language.getMessage(mstrModuleName, 12, "Department")
            mdtTran.Columns.Add("Description", GetType(String)).DefaultValue = ""
            mdtTran.Columns(mdtTran.Columns.Count - 1).Caption = Language.getMessage(mstrModuleName, 13, "Description")
            mdtTran.Columns.Add("Payee", GetType(String)).DefaultValue = ""
            mdtTran.Columns(mdtTran.Columns.Count - 1).Caption = Language.getMessage(mstrModuleName, 14, "Payee")
            mdtTran.Columns.Add("Payment_No_", GetType(String)).DefaultValue = ""
            mdtTran.Columns(mdtTran.Columns.Count - 1).Caption = Language.getMessage(mstrModuleName, 15, "Payment_No_")
            mdtTran.Columns.Add("Payment_Date", GetType(DateTime)).DefaultValue = DBNull.Value
            mdtTran.Columns(mdtTran.Columns.Count - 1).Caption = Language.getMessage(mstrModuleName, 16, "Payment_Date")
            mdtTran.Columns.Add("PV_Number", GetType(String)).DefaultValue = ""
            mdtTran.Columns(mdtTran.Columns.Count - 1).Caption = Language.getMessage(mstrModuleName, 17, "PV_Number")
            mdtTran.Columns.Add("Payment Type", GetType(String)).DefaultValue = ""
            mdtTran.Columns(mdtTran.Columns.Count - 1).Caption = Language.getMessage(mstrModuleName, 18, "Payment Type")
            mdtTran.Columns.Add("Payroll_Month", GetType(Integer)).DefaultValue = 0
            mdtTran.Columns(mdtTran.Columns.Count - 1).Caption = Language.getMessage(mstrModuleName, 19, "Payroll_Month")
            mdtTran.Columns.Add("Payroll_Year", GetType(Integer)).DefaultValue = 0
            mdtTran.Columns(mdtTran.Columns.Count - 1).Caption = Language.getMessage(mstrModuleName, 20, "Payroll_Year")
            mdtTran.Columns.Add("PV Created?", GetType(Integer)).DefaultValue = 0
            mdtTran.Columns(mdtTran.Columns.Count - 1).Caption = Language.getMessage(mstrModuleName, 21, "PV Created?")
            mdtTran.Columns.Add("Select", GetType(Integer)).DefaultValue = 0
            mdtTran.Columns(mdtTran.Columns.Count - 1).Caption = Language.getMessage(mstrModuleName, 22, "Select")
            mdtTran.Columns.Add("PV_Amount", GetType(Decimal)).DefaultValue = 0
            mdtTran.Columns(mdtTran.Columns.Count - 1).Caption = Language.getMessage(mstrModuleName, 23, "PV_Amount")
            mdtTran.Columns.Add("Bank_Account_No_", GetType(String)).DefaultValue = ""
            mdtTran.Columns(mdtTran.Columns.Count - 1).Caption = Language.getMessage(mstrModuleName, 24, "Bank_Account_No_")
            mdtTran.Columns.Add("Bank_Name", GetType(String)).DefaultValue = ""
            mdtTran.Columns(mdtTran.Columns.Count - 1).Caption = Language.getMessage(mstrModuleName, 25, "Bank_Name")
            mdtTran.Columns.Add("Bank_Code", GetType(String)).DefaultValue = ""
            mdtTran.Columns(mdtTran.Columns.Count - 1).Caption = Language.getMessage(mstrModuleName, 26, "Bank_Code")
            mdtTran.Columns.Add("Staff_No_", GetType(String)).DefaultValue = ""
            mdtTran.Columns(mdtTran.Columns.Count - 1).Caption = Language.getMessage(mstrModuleName, 27, "Staff_No_")
            mdtTran.Columns.Add("Booked?", GetType(Integer)).DefaultValue = 0
            mdtTran.Columns(mdtTran.Columns.Count - 1).Caption = Language.getMessage(mstrModuleName, 28, "Booked?")
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "New", mstrModuleName)
        End Try
    End Sub

#End Region

    Public Function InsertAll() As Boolean
        Try
            Dim strQ As String = ""
            Dim trnSQL As SqlTransaction = Nothing
            Try
                Dim strConn As String = ""
                Using cnnSQL As SqlConnection = New SqlConnection
                    strConn = "Data Source=" & mstrDatabaseServer & "; Initial Catalog=" & mstrDatabaseName & "; user id =" & mstrDatabaseUserName & "; Password=" & mstrDatabasePassword & "; MultipleActiveResultSets=true; "
                    cnnSQL.ConnectionString = strConn
                    cnnSQL.Open()

                    trnSQL = cnnSQL.BeginTransaction(IsolationLevel.ReadCommitted)

                    strQ = " INSERT INTO ZSSF_NAV.dbo.ZSSF$Int_Staff_Payments ( " & _
                           "  Ref_No_ " & _
                           ", Account_Type " & _
                           ", Account_No " & _
                           ", Bal__Account_Type " & _
                           ", Bal__Account_No " & _
                           ", Amount " & _
                           ", Date " & _
                           ", Created_By " & _
                           ", Created_Date " & _
                           ", Status " & _
                           ", Cost_Centre_Code " & _
                           ", Department " & _
                           ", Description " & _
                           ", Payee " & _
                           ", Payment_No_ " & _
                           ", Payment_Date " & _
                           ", PV_Number " & _
                           ", [Payment Type] " & _
                           ", Payroll_Month " & _
                           ", Payroll_Year " & _
                           ", [PV Created?] " & _
                           ", [Select] " & _
                           ", Payment_Amt " & _
                           ", Bank_Account_No_ " & _
                           ", Bank_Name " & _
                           ", Bank_Code " & _
                           ", Staff_No_ " & _
                           ", [Booked?] " & _
                           " ) VALUES ( " & _
                           "  @Ref_No_ " & _
                           ", @Account_Type " & _
                           ", @Account_No " & _
                           ", @Bal__Account_Type " & _
                           ", @Bal__Account_No " & _
                           ", @Amount " & _
                           ", @Date " & _
                           ", @Created_By " & _
                           ", @Created_Date " & _
                           ", @Status " & _
                           ", @Cost_Centre_Code " & _
                           ", @Department " & _
                           ", @Description " & _
                           ", @Payee " & _
                           ", @Payment_No_ " & _
                           ", @Payment_Date " & _
                           ", @PV_Number " & _
                           ", @PaymentType " & _
                           ", @Payroll_Month " & _
                           ", @Payroll_Year " & _
                           ", @PVCreated " & _
                           ", @Select " & _
                           ", @Payment_Amt " & _
                           ", @Bank_Account_No_ " & _
                           ", @Bank_Name " & _
                           ", @Bank_Code " & _
                           ", @Staff_No_ " & _
                           ", @Booked " & _
                           "  ) "

                    Using cmdSQL As New SqlCommand()

                        Try
                            If cnnSQL.State = ConnectionState.Closed OrElse cnnSQL.State = ConnectionState.Broken Then
                                cnnSQL.ConnectionString = strConn
                                cnnSQL.Open()
                            End If

                            For Each dtRow As DataRow In mdtTran.Rows
                                cmdSQL.Parameters.Clear()

                                cmdSQL.Parameters.Add(New SqlParameter("@Ref_No_", SqlDbType.NVarChar)).Value = dtRow.Item("Ref_No_").ToString
                                cmdSQL.Parameters.Add(New SqlParameter("@Account_Type", SqlDbType.Int)).Value = dtRow.Item("Account_Type").ToString
                                cmdSQL.Parameters.Add(New SqlParameter("@Account_No", SqlDbType.NVarChar)).Value = dtRow.Item("Account_No").ToString
                                cmdSQL.Parameters.Add(New SqlParameter("@Bal__Account_Type", SqlDbType.Int)).Value = dtRow.Item("Bal__Account_Type").ToString
                                cmdSQL.Parameters.Add(New SqlParameter("@Bal__Account_No", SqlDbType.NVarChar)).Value = dtRow.Item("Bal__Account_No").ToString
                                cmdSQL.Parameters.Add(New SqlParameter("@Amount", SqlDbType.Decimal)).Value = dtRow.Item("Amount").ToString
                                If IsDBNull(dtRow.Item("Date")) = True Then
                                    cmdSQL.Parameters.Add(New SqlParameter("@Date", SqlDbType.DateTime)).Value = DBNull.Value
                                Else
                                    cmdSQL.Parameters.Add(New SqlParameter("@Date", SqlDbType.DateTime)).Value = dtRow.Item("Date").ToString
                                End If

                                cmdSQL.Parameters.Add(New SqlParameter("@Created_By", SqlDbType.NVarChar)).Value = dtRow.Item("Created_By").ToString

                                If IsDBNull(dtRow.Item("Created_Date")) = True Then
                                    cmdSQL.Parameters.Add(New SqlParameter("@Created_Date", SqlDbType.DateTime)).Value = DBNull.Value
                                Else
                                    cmdSQL.Parameters.Add(New SqlParameter("@Created_Date", SqlDbType.DateTime)).Value = dtRow.Item("Created_Date").ToString
                                End If

                                cmdSQL.Parameters.Add(New SqlParameter("@Status", SqlDbType.TinyInt)).Value = dtRow.Item("Status").ToString
                                cmdSQL.Parameters.Add(New SqlParameter("@Cost_Centre_Code", SqlDbType.NVarChar)).Value = dtRow.Item("Cost_Centre_Code").ToString
                                cmdSQL.Parameters.Add(New SqlParameter("@Department", SqlDbType.NVarChar)).Value = dtRow.Item("Department").ToString
                                cmdSQL.Parameters.Add(New SqlParameter("@Description", SqlDbType.NVarChar)).Value = dtRow.Item("Description").ToString
                                cmdSQL.Parameters.Add(New SqlParameter("@Payee", SqlDbType.NVarChar)).Value = dtRow.Item("Payee").ToString
                                cmdSQL.Parameters.Add(New SqlParameter("@Payment_No_", SqlDbType.NVarChar)).Value = dtRow.Item("Payment_No_").ToString

                                If IsDBNull(dtRow.Item("Payment_Date")) = True Then
                                    cmdSQL.Parameters.Add(New SqlParameter("@Payment_Date", SqlDbType.DateTime)).Value = DBNull.Value
                                Else
                                    cmdSQL.Parameters.Add(New SqlParameter("@Payment_Date", SqlDbType.DateTime)).Value = dtRow.Item("Payment_Date").ToString
                                End If

                                cmdSQL.Parameters.Add(New SqlParameter("@PV_Number", SqlDbType.NVarChar)).Value = dtRow.Item("PV_Number").ToString
                                cmdSQL.Parameters.Add(New SqlParameter("@PaymentType", SqlDbType.NVarChar)).Value = dtRow.Item("Payment Type").ToString
                                cmdSQL.Parameters.Add(New SqlParameter("@Payroll_Month", SqlDbType.Int)).Value = dtRow.Item("Payroll_Month").ToString
                                cmdSQL.Parameters.Add(New SqlParameter("@Payroll_Year", SqlDbType.Int)).Value = dtRow.Item("Payroll_Year").ToString
                                cmdSQL.Parameters.Add(New SqlParameter("@PVCreated", SqlDbType.TinyInt)).Value = dtRow.Item("PV Created?").ToString
                                cmdSQL.Parameters.Add(New SqlParameter("@Select", SqlDbType.TinyInt)).Value = dtRow.Item("Select").ToString
                                cmdSQL.Parameters.Add(New SqlParameter("@Payment_Amt", SqlDbType.Decimal)).Value = dtRow.Item("PV_Amount").ToString
                                cmdSQL.Parameters.Add(New SqlParameter("@Bank_Account_No_", SqlDbType.NVarChar)).Value = dtRow.Item("Bank_Account_No_").ToString
                                cmdSQL.Parameters.Add(New SqlParameter("@Bank_Name", SqlDbType.NVarChar)).Value = dtRow.Item("Bank_Name").ToString
                                cmdSQL.Parameters.Add(New SqlParameter("@Bank_Code", SqlDbType.NVarChar)).Value = dtRow.Item("Bank_Code").ToString
                                cmdSQL.Parameters.Add(New SqlParameter("@Staff_No_", SqlDbType.NVarChar)).Value = dtRow.Item("Staff_No_").ToString
                                cmdSQL.Parameters.Add(New SqlParameter("@Booked", SqlDbType.TinyInt)).Value = dtRow.Item("Booked?").ToString

                                cmdSQL.Connection = cnnSQL
                                cmdSQL.CommandType = CommandType.Text
                                cmdSQL.CommandText = strQ
                                cmdSQL.Transaction = trnSQL
                                cmdSQL.ExecuteNonQuery()
                            Next


                            trnSQL.Commit()
                            Return True
                        Catch ex As Exception
                            If trnSQL IsNot Nothing Then trnSQL.Rollback()
                            Throw New Exception(ex.Message & "; Procedure Name: DynamicNavisionPostToDB : SQL-ExecuteNonQuery; Module Name: " & mstrModuleName)
                            Return False
                        End Try
                    End Using
                End Using


            Catch ex As Exception
                Throw New Exception(ex.Message & "; Procedure Name: DynamicNavisionPostToDB : SQL-ExecuteNonQuery; Module Name: " & mstrModuleName)
                Return False
            End Try


        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: InsertAll; Module Name: " & mstrModuleName)
            Return False
        End Try
    End Function
	'<Language> This Auto Generated Text Please Do Not Modify it.
#Region " Language & UI Settings "
	Public Shared Sub SetMessages()
		Try
			Language.setMessage(mstrModuleName, 1, "Ref_No_")
			Language.setMessage(mstrModuleName, 2, "Account_Type")
			Language.setMessage(mstrModuleName, 3, "Account_No")
			Language.setMessage(mstrModuleName, 4, "Bal__Account_Type")
			Language.setMessage(mstrModuleName, 5, "Bal__Account_No")
			Language.setMessage(mstrModuleName, 6, "Amount")
			Language.setMessage(mstrModuleName, 7, "Date")
			Language.setMessage(mstrModuleName, 8, "Created_By")
			Language.setMessage(mstrModuleName, 9, "Created_Date")
			Language.setMessage(mstrModuleName, 10, "Status")
			Language.setMessage(mstrModuleName, 11, "Cost_Centre_Code")
			Language.setMessage(mstrModuleName, 12, "Department")
			Language.setMessage(mstrModuleName, 13, "Description")
			Language.setMessage(mstrModuleName, 14, "Payee")
			Language.setMessage(mstrModuleName, 15, "Payment_No_")
			Language.setMessage(mstrModuleName, 16, "Payment_Date")
			Language.setMessage(mstrModuleName, 17, "PV_Number")
			Language.setMessage(mstrModuleName, 18, "Payment Type")
			Language.setMessage(mstrModuleName, 19, "Payroll_Month")
			Language.setMessage(mstrModuleName, 20, "Payroll_Year")
			Language.setMessage(mstrModuleName, 21, "PV Created?")
			Language.setMessage(mstrModuleName, 22, "Select")
			Language.setMessage(mstrModuleName, 23, "PV_Amount")
			Language.setMessage(mstrModuleName, 24, "Bank_Account_No_")
			Language.setMessage(mstrModuleName, 25, "Bank_Name")
			Language.setMessage(mstrModuleName, 26, "Bank_Code")
			Language.setMessage(mstrModuleName, 27, "Staff_No_")
			Language.setMessage(mstrModuleName, 28, "Booked?")
			
		Catch Ex As Exception
            Throw New Exception(Ex.Message & "; Procedure Name: SetMessages; Module Name: " & mstrModuleName)
		End Try
	End Sub
#End Region 'Language & UI Settings
	'</Language>
End Class
