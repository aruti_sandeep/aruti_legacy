﻿'************************************************************************************************************************************
'Class Name : clscommom_period_Tran.vb
'Purpose    :
'Date       :03/07/2010
'Written By :Pinkal
'Modified   :
'Last Message Index = 3
'************************************************************************************************************************************

Imports eZeeCommonLib

''' <summary>
''' Purpose: 
''' Developer: Pinkal
''' </summary>
Public Class clscommom_period_Tran
    Private Shared ReadOnly mstrModuleName As String = "clscommom_period_Tran"
    Dim objDataOperation As clsDataOperation
    Dim mstrMessage As String = ""

#Region " Private variables "
    Private mintPeriodunkid As Integer
    Private mstrPeriod_Code As String = String.Empty
    Private mstrPeriod_Name As String = String.Empty
    Private mintYearunkid As Integer
    Private mintModulerefid As Integer
    Private mdtStart_Date As Date
    Private mdtEnd_Date As Date
    Private mintTotal_Days As Integer
    Private mstrDescrription As String = String.Empty
    Private mintStatusid As Integer
    Private mblnIsactive As Boolean = True
    'Sohail (19 Nov 2010) -- Start
    Private mintUserunkid As Integer
    Private mintCloseuserunkid As Integer
    Private mintInactiveuserunkid As Integer
    'Sohail (19 Nov 2010) -- End
    Private mintConstant_Days As Integer 'Sohail (10 Jul 2013)
    'Sohail (07 Jan 2014) -- Start
    'Enhancement - Separate TnA Periods from Payroll Periods
    Private mdtTnA_StartDate As Date
    Private mdtTnA_EndDate As Date
    'Sohail (07 Jan 2014) -- End

    Private mstrDatabaseName As String = String.Empty  'Sohail (21 Jul 2012)

    'S.SANDEEP [02 SEP 2015] -- START
    Private mblnIsAssessmentPeriod As Boolean = False
    'S.SANDEEP [02 SEP 2015] -- END

    Private mdecPrescribed_interest_rate As Decimal = 0 'Sohail (30 Oct 2015)

    'Nilay (16-Apr-2016) -- Start
    'ENHANCEMENT - 59.1 - Allow YYYY01 period code format for July period for FY Jul to June for HERON Portico.
    Private mstrSunjvPeriodCode As String = String.Empty
    'Nilay (16-Apr-2016) -- End

    'Sohail (26 Nov 2018) -- Start
    'NMB Enhancement - Flex Cube JV Integration in 75.1.
    Private mstrFlexCube_BatchNo As String = String.Empty
    'Sohail (26 Nov 2018) -- End

    'S.SANDEEP [19 OCT 2016] -- START
    'ISSUE : EMPLOYEE MASTER BIND TRANSACTION ISSUE
    Private xDataOperation As clsDataOperation = Nothing
    'S.SANDEEP [19 OCT 2016] -- END

    'S.SANDEEP |08-JAN-2019| -- START
    Private mintLockDaysAfter As Integer = 0
    Private mintLockDaysBefore As Integer = 0
    'S.SANDEEP |08-JAN-2019| -- END

    'S.SANDEEP |09-FEB-2021| -- START
    'ISSUE/ENHANCEMENT : Competencies Period Changes
    Private mblnIscmptperiod As Boolean = False
    'S.SANDEEP |09-FEB-2021| -- END

#End Region

#Region " Properties "

'S.SANDEEP [28-May-2018] -- START 
'ISSUE/ENHANCEMENT : {Audit Trails} 
Private mstrFormName As String = String.Empty
Public WriteOnly Property _FormName() As String 
Set(ByVal value As String) 
mstrFormName = value 
End Set 
End Property 
 
Private mstrClientIP As String = ""
Public WriteOnly Property _ClientIP() As String 
Set(ByVal value As String) 
mstrClientIP = value 
End Set 
End Property 
 
Private mstrHostName As String = ""
Public WriteOnly Property _HostName() As String 
Set(ByVal value As String) 
mstrHostName = value 
End Set 
End Property 
 
Private mblnIsWeb As Boolean = False
Public WriteOnly Property _FromWeb() As Boolean 
Set(ByVal value As Boolean) 
mblnIsWeb = value 
End Set 
End Property 
 
    Private mintAuditUserId As Integer = 0
    Public WriteOnly Property _AuditUserId() As Integer
        Set(ByVal value As Integer)
            mintAuditUserId = value
        End Set
    End Property

    Private mintCompanyUnkid As Integer = 0
    Public WriteOnly Property _CompanyUnkid() As Integer
        Set(ByVal value As Integer)
            mintCompanyUnkid = value
        End Set
    End Property
 
Private mdtAuditDate As DateTime = Now
Public WriteOnly Property _AuditDate() As DateTime 
Set(ByVal value As DateTime) 
mdtAuditDate = value 
End Set 
End Property 
 
Private xDataOpr As clsDataOperation = Nothing
Public Property _xDataOpr() As clsDataOperation 
Get 
Return xDataOpr 
End Get 
Set(ByVal value As clsDataOperation) 
xDataOpr = value 
End Set 
End Property 
 
Private mintLoginEmployeeunkid As Integer = 0
Public Property _LoginEmployeeunkid() As Integer 
Get 
Return mintLoginEmployeeunkid 
End Get 
Set(ByVal value As Integer) 
mintLoginEmployeeunkid = value 
End Set 
End Property 
 
Private mintVoidlogingemployeeunkid As Integer=0
Public Property _Voidloginemployeeunkid() As Integer 
Get 
Return mintVoidlogingemployeeunkid 
End Get 
Set(ByVal value As Integer) 
mintVoidlogingemployeeunkid = value 
End Set 
End Property 
'S.SANDEEP [28-May-2018] -- END 
    ''' <summary>
    ''' Purpose: Get Message from Class 
    ''' Modify By: Pinkal
    ''' </summary>
    Public ReadOnly Property _Message() As String
        Get
            Return mstrMessage
        End Get
    End Property

    ''' <summary>
    ''' Purpose: Get or Set periodunkid
    ''' Modify By: Pinkal
    ''' </summary>
    Public Property _Periodunkid(ByVal strDatabaseName As String) As Integer
        Get
            Return mintPeriodunkid
        End Get
        Set(ByVal value As Integer)
            mintPeriodunkid = value
            'Sohail (21 Aug 2015) -- Start
            'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
            'Call GetData()
            Call GetData(strDatabaseName)
            'Sohail (21 Aug 2015) -- End
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set period_code
    ''' Modify By: Pinkal
    ''' </summary>
    Public Property _Period_Code() As String
        Get
            Return mstrPeriod_Code
        End Get
        Set(ByVal value As String)
            mstrPeriod_Code = Value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set period_name
    ''' Modify By: Pinkal
    ''' </summary>
    Public Property _Period_Name() As String
        Get
            Return mstrPeriod_Name
        End Get
        Set(ByVal value As String)
            mstrPeriod_Name = Value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set yearunkid
    ''' Modify By: Pinkal
    ''' </summary>
    Public Property _Yearunkid() As Integer
        Get
            Return mintYearunkid
        End Get
        Set(ByVal value As Integer)
            mintYearunkid = Value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set modulerefid
    ''' Modify By: Pinkal
    ''' </summary>
    Public Property _Modulerefid() As Integer
        Get
            Return mintModulerefid
        End Get
        Set(ByVal value As Integer)
            mintModulerefid = Value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set start_date
    ''' Modify By: Pinkal
    ''' </summary>
    Public Property _Start_Date() As Date
        Get
            Return mdtStart_Date
        End Get
        Set(ByVal value As Date)
            mdtStart_Date = Value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set end_date
    ''' Modify By: Pinkal
    ''' </summary>
    Public Property _End_Date() As Date
        Get
            Return mdtEnd_Date
        End Get
        Set(ByVal value As Date)
            mdtEnd_Date = Value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set total_days
    ''' Modify By: Pinkal
    ''' </summary>
    Public Property _Total_Days() As Integer
        Get
            Return mintTotal_Days
        End Get
        Set(ByVal value As Integer)
            mintTotal_Days = Value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set description
    ''' Modify By: Pinkal
    ''' </summary>
    Public Property _Descrription() As String
        Get
            Return mstrDescrription
        End Get
        Set(ByVal value As String)
            mstrDescrription = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set statusid
    ''' Modify By: Pinkal
    ''' </summary>
    Public Property _Statusid() As Integer
        Get
            Return mintStatusid
        End Get
        Set(ByVal value As Integer)
            mintStatusid = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set isactive
    ''' Modify By: Pinkal
    ''' </summary>
    Public Property _Isactive() As Boolean
        Get
            Return mblnIsactive
        End Get
        Set(ByVal value As Boolean)
            mblnIsactive = value
        End Set
    End Property

    'Sohail (19 Nov 2010) -- Start
    ''' <summary>
    ''' Purpose: Get or Set userunkid
    ''' Modify By: Sohail
    ''' </summary>
    Public Property _Userunkid() As Integer
        Get
            Return mintUserunkid
        End Get
        Set(ByVal value As Integer)
            mintUserunkid = Value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set closeuserunkid
    ''' Modify By: Sohail
    ''' </summary>
    Public Property _Closeuserunkid() As Integer
        Get
            Return mintCloseuserunkid
        End Get
        Set(ByVal value As Integer)
            mintCloseuserunkid = Value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set inactiveuserunkid
    ''' Modify By: Sohail
    ''' </summary>
    Public Property _Inactiveuserunkid() As Integer
        Get
            Return mintInactiveuserunkid
        End Get
        Set(ByVal value As Integer)
            mintInactiveuserunkid = Value
        End Set
    End Property
    'Sohail (19 Nov 2010) -- End

    'Sohail (10 Jul 2013) -- Start
    'TRA - ENHANCEMENT
    ''' <summary>
    ''' Purpose: Get or Set constant_days
    ''' Modify By: Sohail
    ''' </summary>
    Public Property _Constant_Days() As Integer
        Get
            Return mintConstant_Days
        End Get
        Set(ByVal value As Integer)
            mintConstant_Days = Value
        End Set
    End Property
    'Sohail (10 Jul 2013) -- End

    'Sohail (07 Jan 2014) -- Start
    'Enhancement - Separate TnA Periods from Payroll Periods
    ''' <summary>
    ''' Purpose: Get or Set TnA_EndDate
    ''' Modify By: Sohail
    ''' </summary>
    Public Property _TnA_EndDate() As Date
        Get
            Return mdtTnA_EndDate
        End Get
        Set(ByVal value As Date)
            mdtTnA_EndDate = value
        End Set
    End Property

    Public ReadOnly Property _TnA_StartDate() As Date
        Get
            mdtTnA_StartDate = GetTnA_StartDate(mintPeriodunkid)
            Return mdtTnA_StartDate
        End Get
    End Property
    'Sohail (07 Jan 2014) -- End

    'S.SANDEEP [02 SEP 2015] -- START
    Public WriteOnly Property _IsAssessmentPeriod() As Boolean
        Set(ByVal value As Boolean)
            mblnIsAssessmentPeriod = value
        End Set
    End Property
    'S.SANDEEP [02 SEP 2015] -- END

    'Sohail (30 Oct 2015) -- Start
    'Enhancement - New Statutory Report I-TAX Form J Report and option for Prescribed Quarterly Interest Rate on Period Master for Kenya.
    Public Property _Prescribed_interest_rate() As Decimal
        Get
            Return mdecPrescribed_interest_rate
        End Get
        Set(ByVal value As Decimal)
            mdecPrescribed_interest_rate = value
        End Set
    End Property
    'Sohail (30 Oct 2015) -- End

    'Nilay (16-Apr-2016) -- Start
    'ENHANCEMENT - 59.1 - Allow YYYY01 period code format for July period for FY Jul to June for HERON Portico.
    Public Property _Sunjv_PeriodCode() As String
        Get
            Return mstrSunjvPeriodCode
        End Get
        Set(ByVal value As String)
            mstrSunjvPeriodCode = value
        End Set
    End Property
    'Nilay (16-Apr-2016) -- End

    'Sohail (26 Nov 2018) -- Start
    'NMB Enhancement - Flex Cube JV Integration in 75.1.
    Public Property _FlexCube_BatchNo() As String
        Get
            Return mstrFlexCube_BatchNo
        End Get
        Set(ByVal value As String)
            mstrFlexCube_BatchNo = value
        End Set
    End Property
    'Sohail (26 Nov 2018) -- End


    'S.SANDEEP |08-JAN-2019| -- START
    Public Property _LockDaysAfter() As Integer
        Get
            Return mintLockDaysAfter
        End Get
        Set(ByVal value As Integer)
            mintLockDaysAfter = value
        End Set
    End Property

    Public Property _LockDaysBefore() As Integer
        Get
            Return mintLockDaysBefore
        End Get
        Set(ByVal value As Integer)
            mintLockDaysBefore = value
        End Set
    End Property
    'S.SANDEEP |08-JAN-2019| -- END

    'S.SANDEEP |09-FEB-2021| -- START
    'ISSUE/ENHANCEMENT : Competencies Period Changes
    Public Property _Iscmptperiod() As Boolean
        Get
            Return mblnIscmptperiod
        End Get
        Set(ByVal value As Boolean)
            mblnIscmptperiod = value
        End Set
    End Property
    'S.SANDEEP |09-FEB-2021| -- END

#End Region

    'Sohail (21 Jul 2012) -- Start
    'TRA - ENHANCEMENT
#Region " Other Properties "
    Public WriteOnly Property _DatabaseName() As String
        Set(ByVal value As String)
            mstrDatabaseName = value
        End Set
    End Property


    'S.SANDEEP [19 OCT 2016] -- START
    'ISSUE : EMPLOYEE MASTER BIND TRANSACTION ISSUE
    Public WriteOnly Property _xDataOperation() As clsDataOperation
        Set(ByVal value As clsDataOperation)
            xDataOperation = value
        End Set
    End Property
    'S.SANDEEP [19 OCT 2016] -- END

#End Region
    'Sohail (21 Jul 2012) -- End

    ''' <summary>
    ''' Modify By: Pinkal
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>
    Public Sub GetData(ByVal strDatabaseName As String)
        'Sohail (21 Aug 2015) - [strDatabaseName]

        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        'S.SANDEEP [19 OCT 2016] -- START
        'ISSUE : EMPLOYEE MASTER BIND TRANSACTION ISSUE
        'objDataOperation = New clsDataOperation
        If xDataOperation IsNot Nothing Then
            objDataOperation = xDataOperation
        Else
        objDataOperation = New clsDataOperation
        End If
        objDataOperation.ClearParameters()
        'S.SANDEEP [19 OCT 2016] -- END


        'Sohail (21 Jul 2012) -- Start
        'TRA - ENHANCEMENT
        'Sohail (21 Aug 2015) -- Start
        'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
        'If mstrDatabaseName.Trim = "" Then mstrDatabaseName = FinancialYear._Object._DatabaseName
        mstrDatabaseName = strDatabaseName
        'Sohail (21 Aug 2015) -- End
        'Sohail (21 Jul 2012) -- End

        Try
            'Sohail (19 Nov 2010) -- Start
            'Changes : New fields 'userunkid', 'closeuserunkid', 'inactiveuserunkid' added.
            'strQ = "SELECT " & _
            '  "  periodunkid " & _
            '  ", period_code " & _
            '  ", period_name " & _
            '  ", yearunkid " & _
            '  ", modulerefid " & _
            '  ", start_date " & _
            '  ", end_date " & _
            '  ", total_days " & _
            '  ", description " & _
            '  ", statusid " & _
            '  ", isactive " & _
            ' "FROM cfcommon_period_tran " & _
            ' "WHERE periodunkid = @periodunkid "
            strQ = "SELECT " & _
              "  periodunkid " & _
              ", period_code " & _
              ", period_name " & _
              ", yearunkid " & _
              ", modulerefid " & _
              ", start_date " & _
              ", end_date " & _
              ", total_days " & _
              ", description " & _
              ", statusid " & _
              ", isactive " & _
                      ", userunkid " & _
                      ", closeuserunkid " & _
                      ", inactiveuserunkid " & _
                    ", ISNULL(constant_days, 0) AS constant_days " & _
                      ", tna_enddate " & _
                      ", ISNULL(prescribed_interest_rate, 0) AS prescribed_interest_rate " & _
                       ", ISNULL(sunjv_periodcode, period_code) AS sunjv_periodcode " & _
                ", ISNULL(flexcube_batchno, '') AS flexcube_batchno " & _
              ", ISNULL(lockdaysafter, 0) AS lockdaysafter " & _
              ", ISNULL(lockdaysbefore, 0) AS lockdaysbefore " & _
              ", ISNULL(iscmptperiod,0) AS iscmptperiod " & _
             "FROM  " & mstrDatabaseName & "..cfcommon_period_tran " & _
             "WHERE periodunkid = @periodunkid "
            'S.SANDEEP |09-FEB-2021| -- START {iscmptperiod} -- END
            'Sohail (26 Nov 2018) - [flexcube_batchno]
            'Nilay (16-Apr-2016) -- [sunjv_periodcode]
            'Sohail (30 Oct 2015) - [prescribed_interest_rate]
            'Sohail (07 Jan 2014) - [tna_enddate]
            'Sohail (10 Jul 2013) - [constant_days]
            'Sohail (19 Nov 2010) -- End
            'S.SANDEEP |08-JAN-2019| -- START {lockdaysafter,lockdaysbefore} -- END

            objDataOperation.AddParameter("@periodunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintPeriodunkid.ToString)

            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            For Each dtRow As DataRow In dsList.Tables(0).Rows
                mintPeriodunkid = CInt(dtRow.Item("periodunkid"))
                mstrPeriod_Code = dtRow.Item("period_code").ToString
                mstrPeriod_Name = dtRow.Item("period_name").ToString
                mintYearunkid = CInt(dtRow.Item("yearunkid"))
                mintModulerefid = CInt(dtRow.Item("modulerefid"))
                mdtStart_Date = dtRow.Item("start_date")
                mdtEnd_Date = dtRow.Item("end_date")
                mintTotal_Days = CInt(dtRow.Item("total_days"))
                mstrDescrription = dtRow.Item("description").ToString
                mintStatusid = CInt(dtRow.Item("statusid"))
                mblnIsactive = CBool(dtRow.Item("isactive"))
                'Sohail (19 Nov 2010) -- Start
                mintUserunkid = CInt(dtRow.Item("userunkid"))
                mintCloseuserunkid = CInt(dtRow.Item("closeuserunkid"))
                mintInactiveuserunkid = CInt(dtRow.Item("inactiveuserunkid"))
                'Sohail (19 Nov 2010) -- End
                mintConstant_Days = CInt(dtRow.Item("constant_days")) 'Sohail (10 Jul 2013)
                mdtTnA_EndDate = dtRow.Item("tna_enddate") 'Sohail (07 Jan 2014)
                mdecPrescribed_interest_rate = CDec(dtRow.Item("prescribed_interest_rate")) 'Sohail (30 Oct 2015)
                'Nilay (16-Apr-2016) -- Start
                'ENHANCEMENT - 59.1 - Allow YYYY01 period code format for July period for FY Jul to June for HERON Portico.
                mstrSunjvPeriodCode = dtRow.Item("sunjv_periodcode").ToString
                'Nilay (16-Apr-2016) -- End
                'Sohail (26 Nov 2018) -- Start
                'NMB Enhancement - Flex Cube JV Integration in 75.1.
                mstrFlexCube_BatchNo = dtRow.Item("flexcube_batchno").ToString
                'Sohail (26 Nov 2018) -- End

                'S.SANDEEP |08-JAN-2019| -- START
                mintLockDaysAfter = CInt(dtRow.Item("lockdaysafter"))
                mintLockDaysBefore = CInt(dtRow.Item("lockdaysbefore"))
                'S.SANDEEP |08-JAN-2019| -- END

                'S.SANDEEP |09-FEB-2021| -- START
                'ISSUE/ENHANCEMENT : Competencies Period Changes
                mblnIscmptperiod = CBool(dtRow.Item("iscmptperiod"))
                'S.SANDEEP |09-FEB-2021| -- END

                Exit For
            Next
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: GetData; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()

            'S.SANDEEP [19 OCT 2016] -- START
            'ISSUE : EMPLOYEE MASTER BIND TRANSACTION ISSUE
            'objDataOperation = Nothing
            If xDataOperation Is Nothing Then objDataOperation = Nothing
            'S.SANDEEP [19 OCT 2016] -- END
        End Try
    End Sub


    ''' <summary>
    ''' Modify By: Pinkal
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>
    Public Function GetList(ByVal strTableName As String _
                            , ByVal intModuleRefid As Integer _
                            , ByVal intYearUnkId As Integer _
                            , ByVal dtDatabase_Start_Date As Date _
                            , Optional ByVal blnOnlyActive As Boolean = True _
                            , Optional ByVal intStatusID As Integer = 0 _
                            , Optional ByVal blnGetUnpaidSalaryPeriod As Boolean = False _
                            , Optional ByVal blnIncludePreviousYearPeriods As Boolean = False _
                            ) As DataSet 'Sohail (28 Dec 2010)
        '                   'Sohail (18 May 2013) - [intYearUnkId]
        '                   'Sohail (08 Nov 2012) - [blnIncludePreviousYearPeriods]
        'Public Function GetList(ByVal strTableName As String, ByVal intModuleRefid As Integer, Optional ByVal blnOnlyActive As Boolean = True, Optional ByVal intStatusID As Integer = 0) As DataSet
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        objDataOperation = New clsDataOperation

        Try
            'Sohail (21 Aug 2015) -- Start
            'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
            'If intYearUnkId <= 0 Then intYearUnkId = FinancialYear._Object._YearUnkid 'Sohail (18 May 2013)
            'Sohail (21 Aug 2015) -- End

            'Sohail (27 Aug 2010) -- Start
            'Changes:Add filter of current year
            'strQ = "SELECT " & _
            '         "  periodunkid " & _
            '         ", period_code " & _
            '         ", period_name " & _
            '         ", cfcommon_period_tran.yearunkid " & _
            '         ", cffinancial_year_tran.financialyear_name as year " & _
            '         ", modulerefid " & _
            '         ", convert(char(8),start_date,112) as start_date " & _
            '         ", convert(char(8),end_date,112) as end_date " & _
            '         ", total_days " & _
            '         ", description " & _
            '         ", case when statusid  = 1 then '" & StatusType.Open.ToString() & "' when statusid = 2 then '" & StatusType.Close.ToString() & "' End status " & _
            '         ", isactive " & _
            '        "FROM cfcommon_period_tran " & _
            '        " LEFT JOIN cffinancial_year_tran on cffinancial_year_tran.yearunkid = cfcommon_period_tran.yearunkid "
            'Sohail (19 Nov 2010) -- Start
            'Changes : New fields 'userunkid', 'closeuserunkid', 'inactiveuserunkid' added.
            'strQ = "SELECT " & _
            '         "  periodunkid " & _
            '         ", period_code " & _
            '         ", period_name " & _
            '         ", cfcommon_period_tran.yearunkid " & _
            '  ", ISNULL(" & FinancialYear._Object._ConfigDatabaseName & "..cffinancial_year_tran.financialyear_name,'') as year " & _
            '         ", modulerefid " & _
            '  ", convert(char(8),cfcommon_period_tran.start_date,112) as start_date " & _
            '  ", convert(char(8),cfcommon_period_tran.end_date,112) as end_date " & _
            '         ", total_days " & _
            '         ", description " & _
            '  ", statusid " & _
            '         ", case when statusid  = 1 then '" & StatusType.Open.ToString() & "' when statusid = 2 then '" & StatusType.Close.ToString() & "' End status " & _
            '         ", isactive " & _
            '        "FROM cfcommon_period_tran " & _
            ' " LEFT JOIN " & FinancialYear._Object._ConfigDatabaseName & "..cffinancial_year_tran on " & FinancialYear._Object._ConfigDatabaseName & "..cffinancial_year_tran.yearunkid = cfcommon_period_tran.yearunkid " & _
            ' "WHERE cfcommon_period_tran.yearunkid = " & FinancialYear._Object._YearUnkid & " " & _
            '    "AND modulerefid = @modulerefid  "

            'Sohail (08 Nov 2012) -- Start
            'TRA - ENHANCEMENT
            'strQ = "SELECT " & _
            '         "  periodunkid " & _
            '         ", period_code " & _
            '         ", period_name " & _
            '         ", cfcommon_period_tran.yearunkid " & _
            '  ", ISNULL(" & FinancialYear._Object._ConfigDatabaseName & "..cffinancial_year_tran.financialyear_name,'') as year " & _
            '         ", modulerefid " & _
            '  ", convert(char(8),cfcommon_period_tran.start_date,112) as start_date " & _
            '  ", convert(char(8),cfcommon_period_tran.end_date,112) as end_date " & _
            '         ", total_days " & _
            '         ", description " & _
            '  ", statusid " & _
            '         ", case when statusid  = 1 then '" & StatusType.Open.ToString() & "' when statusid = 2 then '" & StatusType.Close.ToString() & "' End status " & _
            '         ", isactive " & _
            '            ", userunkid " & _
            '            ", closeuserunkid " & _
            '            ", inactiveuserunkid " & _
            '        "FROM cfcommon_period_tran " & _
            ' " LEFT JOIN " & FinancialYear._Object._ConfigDatabaseName & "..cffinancial_year_tran on " & FinancialYear._Object._ConfigDatabaseName & "..cffinancial_year_tran.yearunkid = cfcommon_period_tran.yearunkid " & _
            ' "WHERE cfcommon_period_tran.yearunkid = " & FinancialYear._Object._YearUnkid & " " & _
            '    "AND modulerefid = @modulerefid  "
            strQ = "SELECT " & _
                     "  periodunkid " & _
                     ", period_code " & _
                     ", period_name " & _
                     ", cfcommon_period_tran.yearunkid " & _
              ", ISNULL(" & FinancialYear._Object._ConfigDatabaseName & "..cffinancial_year_tran.financialyear_name,'') as year " & _
                     ", modulerefid " & _
              ", convert(char(8),cfcommon_period_tran.start_date,112) as start_date " & _
              ", convert(char(8),cfcommon_period_tran.end_date,112) as end_date " & _
                     ", total_days " & _
                     ", description " & _
              ", statusid " & _
                     ", case when statusid  = 1 then '" & StatusType.Open.ToString() & "' when statusid = 2 then '" & StatusType.Close.ToString() & "' End status " & _
                     ", isactive " & _
                        ", userunkid " & _
                        ", closeuserunkid " & _
                        ", inactiveuserunkid " & _
                        ", ISNULL(constant_days, 0) AS constant_days " & _
                        ", CONVERT(CHAR(8),cfcommon_period_tran.tna_enddate,112) AS tna_enddate " & _
                        ", ISNULL(prescribed_interest_rate, 0) AS prescribed_interest_rate " & _
                        ", ISNULL(sunjv_periodcode, period_code) AS sunjv_periodcode " & _
                    ", ISNULL(flexcube_batchno, '') AS flexcube_batchno " & _
                    ", ISNULL(lockdaysafter,0) AS lockdaysafter " & _
                    ", ISNULL(lockdaysbefore,0) AS lockdaysbefore " & _
                    ", ISNULL(iscmptperiod,0) AS iscmptperiod " & _
                    "FROM cfcommon_period_tran " & _
             " LEFT JOIN " & FinancialYear._Object._ConfigDatabaseName & "..cffinancial_year_tran on " & FinancialYear._Object._ConfigDatabaseName & "..cffinancial_year_tran.yearunkid = cfcommon_period_tran.yearunkid " & _
             "WHERE  modulerefid = @modulerefid  "
            'S.SANDEEP |09-FEB-2021| -- START {iscmptperiod} -- END
            'Sohail (26 Nov 2018) - [flexcube_batchno]
            'Nilay (16-Apr-2016) -- [sunjv_periodcode]
            'Sohail (30 Oct 2015) - [prescribed_interest_rate]
            'Sohail (07 Jan 2014) - [tna_enddate]
            'Sohail (10 Jul 2013) - [constant_days]
            'S.SANDEEP |08-JAN-2019| -- START {lockdaysafter,lockdaysbefore} -- END

            If blnIncludePreviousYearPeriods = False Then
                'Sohail (18 May 2013) -- Start
                'TRA - ENHANCEMENT
                'strQ &= " AND cfcommon_period_tran.yearunkid = " & FinancialYear._Object._YearUnkid & "  "
                strQ &= " AND cfcommon_period_tran.yearunkid = " & intYearUnkId & "  "
                'Sohail (18 May 2013) -- End
            End If
            'Sohail (08 Nov 2012) -- End
            'Sohail (19 Nov 2010) -- End
            'Sohail (27 Aug 2010) -- End

            If blnOnlyActive Then
                strQ &= " AND isactive = 1 "
                objDataOperation.AddParameter("@modulerefid", SqlDbType.Int, eZeeDataType.INT_SIZE, intModuleRefid)
            End If

            'Sohail (02 Aug 2010) -- Start
            If intStatusID > 0 Then
                strQ &= "AND statusid = @statusid "
                objDataOperation.AddParameter("@statusid", SqlDbType.Int, eZeeDataType.INT_SIZE, intStatusID)
            End If
            'Sohail (28 Dec 2010) -- Start
            If blnGetUnpaidSalaryPeriod = True Then
                strQ &= "UNION " & _
                        "SELECT DISTINCT payperiodunkid, period_code, period_name, cfcommon_period_tran.yearunkid, ISNULL(hrmsConfiguration..cffinancial_year_tran.financialyear_name, '') AS YEAR " & _
                              ", modulerefid, CONVERT(CHAR(8), cfcommon_period_tran.start_date, 112) AS start_date, CONVERT(CHAR(8), cfcommon_period_tran.end_date, 112) AS end_date, total_days " & _
                              ", description, statusid, CASE WHEN statusid = 1 THEN 'Open' WHEN statusid = 2 THEN 'Close' END status, isactive, cfcommon_period_tran.userunkid, closeuserunkid, inactiveuserunkid, ISNULL(cfcommon_period_tran.constant_days, 0) AS constant_days, CONVERT(CHAR(8),cfcommon_period_tran.tna_enddate,112) AS tna_enddate , ISNULL(prescribed_interest_rate, 0) AS prescribed_interest_rate " & _
                              ", ISNULL(sunjv_periodcode, period_code) AS sunjv_periodcode " & _
                              ", ISNULL(flexcube_batchno, '') AS flexcube_batchno " & _
                              ", ISNULL(lockdaysafter,0) AS lockdaysafter " & _
                              ", ISNULL(lockdaysbefore,0) AS lockdaysbefore " & _
                              ", ISNULL(iscmptperiod,0) AS iscmptperiod " & _
                        "FROM    prtnaleave_tran " & _
                                "LEFT JOIN cfcommon_period_tran ON prtnaleave_tran.payperiodunkid = cfcommon_period_tran.periodunkid " & _
                                "LEFT JOIN hrmsConfiguration..cffinancial_year_tran ON hrmsConfiguration..cffinancial_year_tran.yearunkid = cfcommon_period_tran.yearunkid " & _
                        "WHERE   ISNULL(isvoid, 0) = 0 AND CONVERT(CHAR(8), cfcommon_period_tran.end_date, 112) < '" & eZeeDate.convertDate(dtDatabase_Start_Date) & "' AND balanceamount > 0 "
                'S.SANDEEP |09-FEB-2021| -- START {iscmptperiod} -- END
                'Sohail (26 Nov 2018) - [flexcube_batchno]
                'Nilay (16-Apr-2016) -- [sunjv_periodcode]
                'Sohail (30 Oct 2015) - [prescribed_interest_rate]                
                'Sohail (21 Aug 2015) - [FinancialYear._Object._Database_Start_Date = dtDatabase_Start_Date]
                'Sohail (07 Jan 2014) - [tna_enddate]
                'S.SANDEEP |08-JAN-2019| -- START {lockdaysafter,lockdaysbefore} -- END
            End If
            'Sohail (28 Dec 2010) -- End

            strQ &= "ORDER BY end_date "
            'Sohail (02 Aug 2010) -- End

            dsList = objDataOperation.ExecQuery(strQ, strTableName)

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: GetList; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
        Return dsList
    End Function


    ''' <summary>
    ''' Modify By: Pinkal
    ''' </summary>
    ''' <returns>Boolean</returns>
    ''' <purpose> INSERT INTO Database Table (cfcommon_period_tran) </purpose>
    Public Function Insert() As Boolean

        'S.SANDEEP [02 SEP 2015] -- START
        'ISSUE : ALLOW TO MAKE SAME ASSESSMENT DATE PERIOD WITH DIFFERENT NAME FOR PERIODIC REVIEW

        'If isExist(mstrPeriod_Code) Then
        '    mstrMessage = Language.getMessage(mstrModuleName, 1, "This Period Code is already defined. Please define new Period Code.")
        '    Return False
        'ElseIf isExist("", mstrPeriod_Name) Then
        '    mstrMessage = Language.getMessage(mstrModuleName, 2, "This Period Name is already defined. Please define new Period Name.")
        '    Return False
        'ElseIf isExist("", "", mdtStart_Date, mdtEnd_Date) Then
        '    mstrMessage = Language.getMessage(mstrModuleName, 3, "This Period Date is already defined. Please define new Period Date.")
        '    Return False
        'End If

        If isExist(mstrPeriod_Code) Then
            mstrMessage = Language.getMessage(mstrModuleName, 1, "This Period Code is already defined. Please define new Period Code.")
            Return False
        ElseIf isExist("", mstrPeriod_Name) Then
            mstrMessage = Language.getMessage(mstrModuleName, 2, "This Period Name is already defined. Please define new Period Name.")
            Return False
        End If

        If mblnIsAssessmentPeriod = False Then
            If isExist("", "", mdtStart_Date, mdtEnd_Date) Then
            mstrMessage = Language.getMessage(mstrModuleName, 3, "This Period Date is already defined. Please define new Period Date.")
            Return False
        End If
        End If
        'S.SANDEEP [02 SEP 2015] -- END

        

        Dim objDataOperation As clsDataOperation 'Sohail (12 Oct 2011)
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        objDataOperation = New clsDataOperation
        objDataOperation.BindTransaction() 'Sohail (12 Oct 2011)

        Try
            '*****************************************************************************************************
            '********       PLEASE ADD SAME FIELD IN   CF_CommonPeriod()  IN clsColseYear CLASS   WITHOUT FAIL
            '*****************************************************************************************************

            objDataOperation.AddParameter("@period_code", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrPeriod_Code.ToString)
            objDataOperation.AddParameter("@period_name", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrPeriod_Name.ToString)
            objDataOperation.AddParameter("@yearunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintYearunkid.ToString)
            objDataOperation.AddParameter("@modulerefid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintModulerefid.ToString)
            objDataOperation.AddParameter("@start_date", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, mdtStart_Date)
            objDataOperation.AddParameter("@end_date", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, mdtEnd_Date)
            objDataOperation.AddParameter("@total_days", SqlDbType.Int, eZeeDataType.INT_SIZE, mintTotal_Days.ToString)
            objDataOperation.AddParameter("@description", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrDescrription.ToString)
            objDataOperation.AddParameter("@statusid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintStatusid.ToString)
            objDataOperation.AddParameter("@isactive", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsactive.ToString)
            'Sohail (19 Nov 2010) -- Start
            objDataOperation.AddParameter("@userunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintUserunkid.ToString)
            objDataOperation.AddParameter("@closeuserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintCloseuserunkid.ToString)
            objDataOperation.AddParameter("@inactiveuserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintInactiveuserunkid.ToString)
            'Sohail (19 Nov 2010) -- End
            objDataOperation.AddParameter("@constant_days", SqlDbType.Int, eZeeDataType.INT_SIZE, mintConstant_Days.ToString) 'Sohail (10 Jul 2013)
            objDataOperation.AddParameter("@tna_enddate", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, mdtTnA_EndDate) 'Sohail (07 Jan 2014)
            objDataOperation.AddParameter("@prescribed_interest_rate", SqlDbType.Decimal, eZeeDataType.DECIMAL_SIZE, mdecPrescribed_interest_rate) 'Sohail (30 Oct 2015)
            'Nilay (16-Apr-2016) -- Start
            'ENHANCEMENT - 59.1 - Allow YYYY01 period code format for July period for FY Jul to June for HERON Portico.
            objDataOperation.AddParameter("@sunjv_periodcode", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrSunjvPeriodCode.ToString)
            'Nilay (16-Apr-2016) -- End
            'Sohail (26 Nov 2018) -- Start
            'NMB Enhancement - Flex Cube JV Integration in 75.1.
            objDataOperation.AddParameter("@flexcube_batchno", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrFlexCube_BatchNo.ToString)
            'Sohail (26 Nov 2018) -- End

            'S.SANDEEP |08-JAN-2019| -- START
            objDataOperation.AddParameter("@lockdaysafter", SqlDbType.Int, eZeeDataType.INT_SIZE, mintLockDaysAfter)
            objDataOperation.AddParameter("@lockdaysbefore", SqlDbType.Int, eZeeDataType.INT_SIZE, mintLockDaysBefore)
            'S.SANDEEP |08-JAN-2019| -- END

            'S.SANDEEP |09-FEB-2021| -- START
            'ISSUE/ENHANCEMENT : Competencies Period Changes            
            objDataOperation.AddParameter("@iscmptperiod", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIscmptperiod)
            'S.SANDEEP |09-FEB-2021| -- END

            'Sohail (19 Nov 2010) -- Start
            'Changes : New fields 'userunkid', 'closeuserunkid', 'inactiveuserunkid' added.
            'strQ = "INSERT INTO cfcommon_period_tran ( " & _
            '  "  period_code " & _
            '  ", period_name " & _
            '  ", yearunkid " & _
            '  ", modulerefid " & _
            '  ", start_date " & _
            '  ", end_date " & _
            '  ", total_days " & _
            '  ", description " & _
            '  ", statusid " & _
            '  ", isactive" & _
            '") VALUES (" & _
            '  "  @period_code " & _
            '  ", @period_name " & _
            '  ", @yearunkid " & _
            '  ", @modulerefid " & _
            '  ", @start_date " & _
            '  ", @end_date " & _
            '  ", @total_days " & _
            '  ", @description " & _
            '  ", @statusid " & _
            '  ", @isactive" & _
            '"); SELECT @@identity"
            strQ = "INSERT INTO cfcommon_period_tran ( " & _
              "  period_code " & _
              ", period_name " & _
              ", yearunkid " & _
              ", modulerefid " & _
              ", start_date " & _
              ", end_date " & _
              ", total_days " & _
              ", description " & _
              ", statusid " & _
              ", isactive" & _
                          ", userunkid " & _
                          ", closeuserunkid " & _
                          ", inactiveuserunkid" & _
                    ", constant_days" & _
                            ", tna_enddate " & _
                            ", prescribed_interest_rate " & _
                        ", sunjv_periodcode " & _
                        ", flexcube_batchno " & _
                        ", lockdaysafter " & _
                        ", lockdaysbefore " & _
                        ", iscmptperiod " & _
            ") VALUES (" & _
              "  @period_code " & _
              ", @period_name " & _
              ", @yearunkid " & _
              ", @modulerefid " & _
              ", @start_date " & _
              ", @end_date " & _
              ", @total_days " & _
              ", @description " & _
              ", @statusid " & _
              ", @isactive" & _
                          ", @userunkid " & _
                          ", @closeuserunkid " & _
                          ", @inactiveuserunkid" & _
                    ", @constant_days" & _
                            ", @tna_enddate " & _
                            ", @prescribed_interest_rate " & _
                        ", @sunjv_periodcode " & _
                        ", @flexcube_batchno " & _
                        ", @lockdaysafter " & _
                        ", @lockdaysbefore " & _
                        ", @iscmptperiod " & _
            "); SELECT @@identity"
            'S.SANDEEP |09-FEB-2021| -- START {iscmptperiod} -- END
            'Sohail (26 Nov 2018) - [flexcube_batchno]
            'Nilay (16-Apr-2016) -- [sunjv_periodcode]
            'Sohail (30 Oct 2015) - [prescribed_interest_rate]
            'Sohail (07 Jan 2014) - [tna_enddate]
            'Sohail (10 Jul 2013) - [constant_days]
            'Sohail (19 Nov 2010) -- End
            'S.SANDEEP |08-JAN-2019| -- START {lockdaysafter,lockdaysbefore} -- END

            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            mintPeriodunkid = dsList.Tables(0).Rows(0).Item(0)

'S.SANDEEP [28-May-2018] -- START
'ISSUE/ENHANCEMENT : {Audit Trails} 
Dim objCommonATLog AS New clsCommonATLog 
objCommonATLog._FormName = mstrFormName 
objCommonATLog._LoginEmployeeUnkid = mintLoginEmployeeunkid 
            objCommonATLog._ClientIP = mstrClientIP
            objCommonATLog._HostName = mstrHostName
            objCommonATLog._FromWeb = mblnIsWeb
            objCommonATLog._AuditUserId = mintAuditUserId
objCommonATLog._CompanyUnkid = mintCompanyUnkid
            objCommonATLog._AuditDate = mdtAuditDate
            'S.SANDEEP [28-May-2018] -- END

            If objCommonATLog.Insert_AtLog(objDataOperation, 1, "cfcommon_period_tran", "periodunkid", mintPeriodunkid) = False Then
                objDataOperation.ReleaseTransaction(False)
                Return False
            Else
                objDataOperation.ReleaseTransaction(True)
            Return True
            End If

            'S.SANDEEP [28-May-2018] -- START
            'ISSUE/ENHANCEMENT : {Audit Trails} 
            objCommonATLog = Nothing
            'S.SANDEEP [28-May-2018] -- END

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: Insert; Module Name: " & mstrModuleName)
            Return False
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
    End Function

    ''' <summary>
    ''' Modify By: Pinkal
    ''' </summary>
    ''' <returns>Boolean</returns>
    ''' <purpose> Update Database Table (cfcommon_period_tran) </purpose>
    Public Function Update() As Boolean

        'S.SANDEEP [02 SEP 2015] -- START
        'ISSUE : ALLOW TO MAKE SAME ASSESSMENT DATE PERIOD WITH DIFFERENT NAME FOR PERIODIC REVIEW

        'If isExist(mstrPeriod_Code, "", Nothing, Nothing, mintPeriodunkid) Then
        '    mstrMessage = Language.getMessage(mstrModuleName, 1, "This Period Code is already defined. Please define new Period Code.")
        '    Return False
        'ElseIf isExist("", mstrPeriod_Name, Nothing, Nothing, mintPeriodunkid) Then
        '    mstrMessage = Language.getMessage(mstrModuleName, 2, "This Period Name is already defined. Please define new Period Name.")
        '    Return False
        'ElseIf isExist("", "", mdtStart_Date, mdtEnd_Date, mintPeriodunkid) Then
        '    mstrMessage = Language.getMessage(mstrModuleName, 3, "This Period Date is already defined. Please define new Period Date.")
        '    Return False
        'End If

        If isExist(mstrPeriod_Code, "", Nothing, Nothing, mintPeriodunkid) Then
            mstrMessage = Language.getMessage(mstrModuleName, 1, "This Period Code is already defined. Please define new Period Code.")
            Return False
        ElseIf isExist("", mstrPeriod_Name, Nothing, Nothing, mintPeriodunkid) Then
            mstrMessage = Language.getMessage(mstrModuleName, 2, "This Period Name is already defined. Please define new Period Name.")
            Return False
        End If

        If mblnIsAssessmentPeriod = False Then
            If isExist("", "", mdtStart_Date, mdtEnd_Date, mintPeriodunkid) Then
            mstrMessage = Language.getMessage(mstrModuleName, 3, "This Period Date is already defined. Please define new Period Date.")
            Return False
        End If
        End If
        'S.SANDEEP [02 SEP 2015] -- END




        Dim objDataOperation As clsDataOperation 'Sohail (12 Oct 2011)
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        objDataOperation = New clsDataOperation
        objDataOperation.BindTransaction() 'Sohail (12 Oct 2011)

        Try
            objDataOperation.AddParameter("@periodunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintPeriodunkid.ToString)
            objDataOperation.AddParameter("@period_code", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrPeriod_Code.ToString)
            objDataOperation.AddParameter("@period_name", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrPeriod_Name.ToString)
            objDataOperation.AddParameter("@yearunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintYearunkid.ToString)
            objDataOperation.AddParameter("@modulerefid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintModulerefid.ToString)
            objDataOperation.AddParameter("@start_date", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, mdtStart_Date)
            objDataOperation.AddParameter("@end_date", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, mdtEnd_Date)
            objDataOperation.AddParameter("@total_days", SqlDbType.Int, eZeeDataType.INT_SIZE, mintTotal_Days.ToString)
            objDataOperation.AddParameter("@description", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrDescrription.ToString)
            objDataOperation.AddParameter("@statusid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintStatusid.ToString)
            objDataOperation.AddParameter("@isactive", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsactive.ToString)
            'Sohail (19 Nov 2010) -- Start
            objDataOperation.AddParameter("@userunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintUserunkid.ToString)
            objDataOperation.AddParameter("@closeuserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintCloseuserunkid.ToString)
            objDataOperation.AddParameter("@inactiveuserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintInactiveuserunkid.ToString)
            'Sohail (19 Nov 2010) -- End
            objDataOperation.AddParameter("@constant_days", SqlDbType.Int, eZeeDataType.INT_SIZE, mintConstant_Days.ToString) 'Sohail (10 Jul 2013)
            objDataOperation.AddParameter("@tna_enddate", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, mdtTnA_EndDate) 'Sohail (07 Jan 2014)
            objDataOperation.AddParameter("@prescribed_interest_rate", SqlDbType.Decimal, eZeeDataType.DECIMAL_SIZE, mdecPrescribed_interest_rate) 'Sohail (30 Oct 2015)
            'Nilay (16-Apr-2016) -- Start
            'ENHANCEMENT - 59.1 - Allow YYYY01 period code format for July period for FY Jul to June for HERON Portico.
            objDataOperation.AddParameter("@sunjv_periodcode", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrSunjvPeriodCode.ToString)
            'Nilay (16-Apr-2016) -- End
            'Sohail (26 Nov 2018) -- Start
            'NMB Enhancement - Flex Cube JV Integration in 75.1.
            objDataOperation.AddParameter("@flexcube_batchno", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrFlexCube_BatchNo.ToString)
            'Sohail (26 Nov 2018) -- End

            'S.SANDEEP |08-JAN-2019| -- START
            objDataOperation.AddParameter("@lockdaysafter", SqlDbType.Int, eZeeDataType.INT_SIZE, mintLockDaysAfter)
            objDataOperation.AddParameter("@lockdaysbefore", SqlDbType.Int, eZeeDataType.INT_SIZE, mintLockDaysBefore)
            'S.SANDEEP |08-JAN-2019| -- END

            'S.SANDEEP |09-FEB-2021| -- START
            'ISSUE/ENHANCEMENT : Competencies Period Changes            
            objDataOperation.AddParameter("@iscmptperiod", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIscmptperiod)
            'S.SANDEEP |09-FEB-2021| -- END

            'Sohail (19 Nov 2010) -- Start
            'Changes : New fields 'userunkid', 'closeuserunkid', 'inactiveuserunkid' added.
            'strQ = "UPDATE cfcommon_period_tran SET " & _
            '  "  period_code = @period_code" & _
            '  ", period_name = @period_name" & _
            '  ", yearunkid = @yearunkid" & _
            '  ", modulerefid = @modulerefid" & _
            '  ", start_date = @start_date" & _
            '  ", end_date = @end_date" & _
            '  ", total_days = @total_days" & _
            '  ", description = @description" & _
            '  ", statusid = @statusid" & _
            '  ", isactive = @isactive " & _
            '"WHERE periodunkid = @periodunkid "
            strQ = "UPDATE cfcommon_period_tran SET " & _
              "  period_code = @period_code" & _
              ", period_name = @period_name" & _
              ", yearunkid = @yearunkid" & _
              ", modulerefid = @modulerefid" & _
              ", start_date = @start_date" & _
              ", end_date = @end_date" & _
              ", total_days = @total_days" & _
              ", description = @description" & _
              ", statusid = @statusid" & _
              ", isactive = @isactive " & _
                        ", userunkid = @userunkid" & _
                        ", closeuserunkid = @closeuserunkid" & _
                        ", inactiveuserunkid = @inactiveuserunkid " & _
                    ", constant_days = @constant_days " & _
                        ", tna_enddate = @tna_enddate " & _
                        ", prescribed_interest_rate = @prescribed_interest_rate " & _
                        ", sunjv_periodcode = @sunjv_periodcode " & _
                        ", flexcube_batchno = @flexcube_batchno " & _
                        ", lockdaysafter = @lockdaysafter " & _
                        ", lockdaysbefore = @lockdaysbefore " & _
                        ", iscmptperiod = @iscmptperiod " & _
            "WHERE periodunkid = @periodunkid "
            'S.SANDEEP |09-FEB-2021| -- START {iscmptperiod} -- END
            'Sohail (26 Nov 2018) - [flexcube_batchno]
            'Nilay (16-Apr-2016) -- [sunjv_periodcode]
            'Sohail (30 Oct 2015) - [prescribed_interest_rate]
            'Sohail (07 Jan 2014) - [tna_enddate]
            'Sohail (10 Jul 2013) - [constant_days]
            'Sohail (19 Nov 2010) -- End
            'S.SANDEEP |08-JAN-2019| -- START {lockdaysafter,lockdaysbefore} -- END

            Call objDataOperation.ExecNonQuery(strQ)

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            Dim blnToInsert As Boolean
            'S.SANDEEP [28-May-2018] -- START
            'ISSUE/ENHANCEMENT : {Audit Trails} 
            Dim objCommonATLog As New clsCommonATLog
            objCommonATLog._FormName = mstrFormName
            objCommonATLog._LoginEmployeeUnkid = mintLoginEmployeeunkid
            objCommonATLog._ClientIP = mstrClientIP
            objCommonATLog._HostName = mstrHostName
            objCommonATLog._FromWeb = mblnIsWeb
            objCommonATLog._AuditUserId = mintAuditUserId
objCommonATLog._CompanyUnkid = mintCompanyUnkid
            objCommonATLog._AuditDate = mdtAuditDate
            'S.SANDEEP [28-May-2018] -- END

            If objCommonATLog.IsTableDataUpdate("atcommon_log", "cfcommon_period_tran", mintPeriodunkid, "periodunkid", 2) Then
                blnToInsert = True
            End If
            If blnToInsert = True AndAlso objCommonATLog.Insert_AtLog(objDataOperation, 2, "cfcommon_period_tran", "periodunkid", mintPeriodunkid) = False Then
                objDataOperation.ReleaseTransaction(False)
                Return False
            Else
                objDataOperation.ReleaseTransaction(True)
            Return True
            End If

            'S.SANDEEP [28-May-2018] -- START
            'ISSUE/ENHANCEMENT : {Audit Trails} 
            objCommonATLog = Nothing
            'S.SANDEEP [28-May-2018] -- END

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: Update; Module Name: " & mstrModuleName)
            Return False
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
    End Function

    ''' <summary>
    ''' Modify By: Pinkal
    ''' </summary>
    ''' <returns>Boolean</returns>
    ''' <purpose> Delete Database Table (cfcommon_period_tran) </purpose>
    Public Function Delete(ByVal intUnkid As Integer, ByVal intUserunkid As Integer) As Boolean
        'Sohail (21 Aug 2015) - [intUserunkid]

        If isUsed(intUnkid) Then
            mstrMessage = Language.getMessage(mstrModuleName, 5, "Sorry, You cannot delete selected Period. Reason: This Period is in use.")
            Return False
        End If

        Dim objDataOperation As clsDataOperation 'Sohail (12 Oct 2011)
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        objDataOperation = New clsDataOperation
        objDataOperation.BindTransaction() 'Sohail (12 Oct 2011)

        Try
            'Sohail (26 Aug 2010) -- Start
            'strQ = "DELETE FROM cfcommon_period_tran " & _
            '"WHERE periodunkid = @periodunkid "
            'Sohail (19 Nov 2010) -- Start
            'strQ = "UPDATE cfcommon_period_tran SET " & _
            ' " isactive = 0 " & _
            '"WHERE periodunkid = @periodunkid "
            strQ = "UPDATE cfcommon_period_tran SET " & _
             " isactive = 0 " & _
                        ", inactiveuserunkid = @inactiveuserunkid " & _
            "WHERE periodunkid = @periodunkid "
            'Sohail (19 Nov 2010) -- End
            'Sohail (26 Aug 2010) -- End

            objDataOperation.AddParameter("@periodunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intUnkid)
            'Sohail (21 Aug 2015) -- Start
            'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
            'objDataOperation.AddParameter("@inactiveuserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, User._Object._Userunkid) 'Sohail (19 Nov 2010)
            objDataOperation.AddParameter("@inactiveuserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intUserunkid)
            'Sohail (21 Aug 2015) -- End

            Call objDataOperation.ExecNonQuery(strQ)

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            'S.SANDEEP [28-May-2018] -- START
            'ISSUE/ENHANCEMENT : {Audit Trails} 
            Dim objCommonATLog As New clsCommonATLog
            objCommonATLog._FormName = mstrFormName
            objCommonATLog._LoginEmployeeUnkid = mintLoginEmployeeunkid
            objCommonATLog._ClientIP = mstrClientIP
            objCommonATLog._HostName = mstrHostName
            objCommonATLog._FromWeb = mblnIsWeb
            objCommonATLog._AuditUserId = mintAuditUserId
objCommonATLog._CompanyUnkid = mintCompanyUnkid
            objCommonATLog._AuditDate = mdtAuditDate
            'S.SANDEEP [28-May-2018] -- END

            If objCommonATLog.Insert_AtLog(objDataOperation, 3, "cfcommon_period_tran", "periodunkid", intUnkid) = False Then
                objDataOperation.ReleaseTransaction(False)
                Return False
            Else
                objDataOperation.ReleaseTransaction(True)
            Return True
            End If

            'S.SANDEEP [28-May-2018] -- START
            'ISSUE/ENHANCEMENT : {Audit Trails} 
            objCommonATLog = Nothing
            'S.SANDEEP [28-May-2018] -- END

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: Update; Module Name: " & mstrModuleName)
            Return False
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
    End Function

    ''' <summary>
    ''' Modify By: Pinkal
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>
    Public Function isUsed(ByVal intUnkid As Integer) As Boolean
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        objDataOperation = New clsDataOperation

        Try
            'Sohail (30 Aug 2010) -- Start
            'strQ = "<Query>"
            strQ = "SELECT  TABLE_NAME " & _
                            ", COLUMN_NAME " & _
                    "FROM    INFORMATION_SCHEMA.COLUMNS " & _
                    "WHERE   COLUMN_NAME LIKE '%periodunkid' " & _
                            "AND TABLE_NAME <> 'cfcommon_period_tran' " & _
                    "ORDER BY TABLE_NAME "

            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            For Each dsRow As DataRow In dsList.Tables("List").Rows
                objDataOperation.ClearParameters()

                strQ = "SELECT TOP 1 * " & _
                        "FROM " & dsRow.Item("TABLE_NAME").ToString & " " & _
                        "WHERE " & dsRow.Item("COLUMN_NAME").ToString & " = @periodunkid "

                objDataOperation.AddParameter("@periodunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intUnkid)

                dsList = objDataOperation.ExecQuery(strQ, "Used")

                If objDataOperation.ErrorMessage <> "" Then
                    exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                    Throw exForce
                End If

                If dsList.Tables("Used").Rows.Count > 0 Then
                    Return True
                End If
            Next

            'Return dsList.Tables(0).Rows.Count > 0
            Return False
            'Sohail (30 Aug 2010) -- End
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: isUsed; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
    End Function

    ''' <summary>
    ''' Modify By: Pinkal
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>
    Public Function isExist(Optional ByVal strCode As String = "", Optional ByVal strName As String = "", Optional ByVal dtStartdate As Date = Nothing, Optional ByVal dtEnddate As Date = Nothing, Optional ByVal intUnkid As Integer = -1) As Boolean
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        objDataOperation = New clsDataOperation

        Try
            'Sohail (19 Nov 2010) -- Start

            'strQ = "SELECT " & _
            '  "  periodunkid " & _
            '  ", period_code " & _
            '  ", period_name " & _
            '  ", yearunkid " & _
            '  ", modulerefid " & _
            '  ", start_date " & _
            '  ", end_date " & _
            '  ", total_days " & _
            '  ", description " & _
            '  ", statusid " & _
            '  ", isactive " & _
            ' "FROM cfcommon_period_tran " & _
            ' "WHERE  1=1 AND modulerefid = @modulerefid"
            strQ = "SELECT " & _
              "  periodunkid " & _
              ", period_code " & _
              ", period_name " & _
              ", yearunkid " & _
              ", modulerefid " & _
              ", start_date " & _
              ", end_date " & _
              ", total_days " & _
              ", description " & _
              ", statusid " & _
              ", isactive " & _
                        ", userunkid " & _
                        ", closeuserunkid " & _
                        ", inactiveuserunkid " & _
                    ", constant_days " & _
                    ", tna_enddate " & _
                    ", ISNULL(prescribed_interest_rate, 0) AS prescribed_interest_rate " & _
                    ", ISNULL(lockdaysafter,0) AS lockdaysafter " & _
                    ", ISNULL(lockdaysbefore,0) AS lockdaysbefore " & _
                    ", ISNULL(iscmptperiod,0) AS iscmptperiod " & _
             "FROM cfcommon_period_tran " & _
             "WHERE  ISNULL(isactive, 1) = 1  AND modulerefid = @modulerefid"
            'S.SANDEEP |09-FEB-2021| -- START {iscmptperiod} -- END
            'Sohail (30 Oct 2015) - [prescribed_interest_rate]
            'Sohail (07 Jan 2014) - [tna_enddate]
            'Sohail (10 Jul 2013) - [constant_days]
            'Sohail (18 Dec 2010) 'Issue:Once deleted period can not be created again.
            'Sohail (19 Nov 2010) -- End
            'S.SANDEEP |08-JAN-2019| -- START {lockdaysafter,lockdaysbefore} -- END

            If strCode.Length > 0 Then
                strQ &= " AND period_code = @period_code "
            End If

            If strName.Length > 0 Then
                strQ &= " AND period_name = @period_name "
            End If

            If dtStartdate <> Nothing And dtEnddate <> Nothing Then
                'Sohail (18 Dec 2010) -- Start
                'strQ &= " AND (start_date between @startdate AND @enddate OR end_date between @startdate AND @enddate) "
                'Sohail (24 Mar 2014) -- Start
                'Issue - user can create period from 03-03-2014 to 25-03-2014 even if 01-03-2014 to 31-03-2014 period is already exist.
                'strQ &= " AND (CONVERT(CHAR(8),start_date,112) between @startdate AND @enddate OR CONVERT(CHAR(8),end_date,112) between @startdate AND @enddate) "
                strQ &= " AND ( (CONVERT(CHAR(8),start_date,112) between @startdate AND @enddate AND CONVERT(CHAR(8),end_date,112) between @startdate AND @enddate) " & _
                                " OR " & _
                                " ( @startdate BETWEEN CONVERT(CHAR(8), start_date, 112) AND CONVERT(CHAR(8), end_date, 112) OR @enddate BETWEEN CONVERT(CHAR(8), start_date, 112) AND CONVERT(CHAR(8), end_date, 112) )" & _
                             " ) "
                'Sohail (24 Mar 2014) -- End
                'Sohail (18 Dec 2010) -- End
            End If

            If intUnkid > 0 Then
                strQ &= " AND periodunkid <> @periodunkid"
            End If

            objDataOperation.AddParameter("@period_code", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, strCode)
            objDataOperation.AddParameter("@period_name", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, strName)
            'Sohail (18 Dec 2010) -- Start
            'objDataOperation.AddParameter("@startdate", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, IIf(dtStartdate <> Nothing, dtStartdate.ToString(), DBNull.Value))
            'objDataOperation.AddParameter("@enddate", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, IIf(dtEnddate <> Nothing, dtEnddate.ToString(), DBNull.Value))
            objDataOperation.AddParameter("@startdate", SqlDbType.VarChar, eZeeDataType.NAME_SIZE, IIf(dtStartdate <> Nothing, eZeeDate.convertDate(dtStartdate), DBNull.Value))
            objDataOperation.AddParameter("@enddate", SqlDbType.VarChar, eZeeDataType.NAME_SIZE, IIf(dtEnddate <> Nothing, eZeeDate.convertDate(dtEnddate), DBNull.Value))
            'Sohail (18 Dec 2010) -- End
            
            objDataOperation.AddParameter("@modulerefid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintModulerefid)
            objDataOperation.AddParameter("@periodunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intUnkid)

            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            Return dsList.Tables(0).Rows.Count > 0
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: isExist; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
    End Function

    '''' <summary>
    '''' Modify By: Pinkal
    '''' </summary>
    '''' <purpose> Assign all Property variable </purpose>
    'Public Function getListForYearCombo(Optional ByVal strListName As String = "List", Optional ByVal mblFlag As Boolean = False) As DataSet
    '    Dim dsList As New DataSet
    '    Dim objDataOperation As New clsDataOperation
    '    Dim strQ As String = String.Empty
    '    Dim exForce As Exception
    '    Try
    '        If mblFlag = True Then
    '            strQ = "SELECT 0 as yearunkid, ' ' +  @name  as name UNION "
    '        End If
    '        strQ &= "SELECT yearunkid, financialyear_name as name FROM cffinancial_year_tran where isnull(isclosed,0) = 0 ORDER BY name "

    '        objDataOperation.AddParameter("@name", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 2, "Select"))
    '        dsList = objDataOperation.ExecQuery(strQ, strListName)
    '        If objDataOperation.ErrorMessage <> "" Then
    '            exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
    '            Throw exForce
    '        End If
    '        Return dsList
    '    Catch ex As Exception
    '        DisplayError.Show(-1, ex.Message, "getListForYearCombo", mstrModuleName)
    '        Return Nothing
    '    Finally
    '        exForce = Nothing
    '        objDataOperation = Nothing
    '        dsList.Dispose()
    '        dsList = Nothing
    '    End Try

    'End Function

    '''' <summary>
    '''' Modify By: Pinkal
    '''' </summary>
    '''' <purpose> Assign all Property variable </purpose>
    'Public Function getListForCombo(ByVal intModuleRefid As Integer, Optional ByVal intYearunkid As Integer = 0, Optional ByVal strListName As String = "List", Optional ByVal mblFlag As Boolean = False) As DataSet
    '    Dim dsList As New DataSet
    '    Dim objDataOperation As New clsDataOperation
    '    Dim strQ As String = String.Empty
    '    Dim exForce As Exception
    '    Try
    '        If mblFlag = True Then
    '            strQ = "SELECT 0 as periodunkid, ' ' +  @name  as name UNION "
    '        End If
    '        strQ &= "SELECT periodunkid, period_name as name FROM cfcommon_period_tran where (yearunkid = @yearunkid or @yearunkid = 0) and modulerefid=@modulerefid  and isactive = 1  ORDER BY name "

    '        objDataOperation.AddParameter("@yearunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intYearunkid)
    '        objDataOperation.AddParameter("@modulerefid", SqlDbType.Int, eZeeDataType.INT_SIZE, intModuleRefid)
    '        objDataOperation.AddParameter("@name", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 2, "Select"))


    '        dsList = objDataOperation.ExecQuery(strQ, strListName)
    '        If objDataOperation.ErrorMessage <> "" Then
    '            exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
    '            Throw exForce
    '        End If
    '        Return dsList
    '    Catch ex As Exception
    '        DisplayError.Show(-1, ex.Message, "getListForCombo", mstrModuleName)
    '        Return Nothing
    '    Finally
    '        exForce = Nothing
    '        objDataOperation = Nothing
    '        dsList.Dispose()
    '        dsList = Nothing
    '    End Try

    'End Function

    'S.SANDEEP [ 27 APRIL 2012 ] -- START
    'ENHANCEMENT : TRA CHANGES
    Public Function getListForCombo(ByVal intModuleRefid As Integer, _
                                    ByVal intYearunkid As Integer, _
                                    ByVal strDatabaseName As String, _
                                    ByVal dtDatabaseStartDate As Date, _
                                    Optional ByVal strListName As String = "List", _
                                    Optional ByVal mblFlag As Boolean = False, _
                                    Optional ByVal intStatusID As Integer = 0, _
                                    Optional ByVal blnGetUnpaidSalaryPeriod As Boolean = False, _
                                    Optional ByVal blnIscmptperiod As Boolean = False, _
                                    Optional ByVal blnAddCompetencyPrdFilter As Boolean = False) As DataSet 'S.SANDEEP |09-FEB-2021| -- START {blnIscmptperiod,blnAddCompetencyPrdFilter} -- END
        'Sohail (16 Jul 2012) - [strDatabaseName]
        'Sohail (21 Aug 2015) - [strDatabaseStartDate = dtDatabaseStartDate]
        'Public Function getListForCombo(ByVal intModuleRefid As Integer, ByVal intYearunkid As Integer, Optional ByVal strListName As String = "List", Optional ByVal mblFlag As Boolean = False, Optional ByVal intStatusID As Integer = 0, Optional ByVal blnGetUnpaidSalaryPeriod As Boolean = False) As DataSet 'Sohail (28 Dec 2010)
        'S.SANDEEP [ 27 APRIL 2012 ] -- END
        'Public Function getListForCombo(ByVal intModuleRefid As Integer, Optional ByVal intYearunkid As Integer = 0, Optional ByVal strListName As String = "List", Optional ByVal mblFlag As Boolean = False, Optional ByVal intStatusID As Integer = 0) As DataSet
        Dim dsList As New DataSet
        Dim objDataOperation As New clsDataOperation
        Dim strQ As String = String.Empty
        Dim exForce As Exception
        Try

            'Sohail (16 Jul 2012) -- Start
            'TRA - ENHANCEMENT
            'Sohail (21 Aug 2015) -- Start
            'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
            'If strDatabaseName.Trim = "" Then
            '    strDatabaseName = FinancialYear._Object._DatabaseName
            'End If
            'Sohail (21 Aug 2015) -- End
            'Sohail (16 Jul 2012) -- End

            If mblFlag = True Then
                'Sohail (04 Mar 2011) -- Start
                'Changes : Add New column Start_Date (Allow to do process for current period too as per rutta's comment's to see the current status of salary.)
                'strQ = "SELECT 0 as periodunkid, ' ' +  @name  as name, '19000101' AS end_date UNION "
                strQ = "SELECT 0 as periodunkid, ' ' +  @name  as name, '19000101' AS start_date, '19000101' AS end_date, 0 AS yearunkid, '' AS code UNION "
                'Sohail (03 Dec 2013) - [yearunkid]
                'Sohail (04 Mar 2011) -- End
                'Nilay (15-Dec-2015) -- [code]

            End If
            'strQ &= "SELECT periodunkid, period_name as name FROM cfcommon_period_tran where modulerefid=@modulerefid  and isactive = 1  "
            'Sohail (27 Aug 2010) -- Start
            'Changes:Add filter of current year
            'strQ &= "SELECT periodunkid, period_name as name, convert(char(8),end_date,112) as end_date FROM cfcommon_period_tran where modulerefid=@modulerefid  and isactive = 1  "

            'Sandeep ( 18 JAN 2011 ) -- START
            'strQ &= "SELECT periodunkid, period_name as name, convert(char(8),cfcommon_period_tran.end_date,112) as end_date FROM cfcommon_period_tran where modulerefid=@modulerefid  and isactive = 1  " & _
            '"AND cfcommon_period_tran.yearunkid = " & FinancialYear._Object._YearUnkid & " "

            'Sohail (04 Mar 2011) -- Start
            'Changes : Add New column Start_Date (Allow to do process for current period too as per rutta's comment's to see the current status of salary.)
            'strQ &= "SELECT periodunkid, period_name as name, convert(char(8),cfcommon_period_tran.end_date,112) as end_date FROM cfcommon_period_tran where modulerefid=@modulerefid  and isactive = 1  "
            strQ &= "SELECT " & _
                        "  periodunkid " & _
                        ", period_name as name " & _
                        ", convert(char(8),cfcommon_period_tran.start_date,112) as start_date " & _
                        ", convert(char(8),cfcommon_period_tran.end_date,112) as end_date " & _
                        ", cfcommon_period_tran.yearunkid " & _
                        ", cfcommon_period_tran.period_code AS code " & _
                    "FROM " & strDatabaseName & "..cfcommon_period_tran where modulerefid=@modulerefid  and isactive = 1  "
            'Sohail (03 Dec 2013) - [yearunkid]
            'Sohail (04 Mar 2011) -- End
            'Nilay (15-Dec-2015) -- [code]

            If intYearunkid > 0 Then
                strQ &= "AND cfcommon_period_tran.yearunkid = @YearId "
                objDataOperation.AddParameter("@YearId", SqlDbType.Int, eZeeDataType.INT_SIZE, intYearunkid)
            End If

            'Sandeep ( 18 JAN 2011 ) -- END 


            'S.SANDEEP |09-FEB-2021| -- START
            'ISSUE/ENHANCEMENT : Competencies Period Changes
            If blnAddCompetencyPrdFilter Then
                If blnIscmptperiod Then
                    strQ &= "AND ISNULL(cfcommon_period_tran.iscmptperiod,0) = 1 "
                Else
                    strQ &= "AND ISNULL(cfcommon_period_tran.iscmptperiod,0) = 0 "
                End If
            End If            
            'S.SANDEEP |09-FEB-2021| -- END



            'Sohail (27 Aug 2010) -- End

            'If intYearunkid > 0 Then
            '    strQ &= " AND yearunkid = @yearunkid "
            '    objDataOperation.AddParameter("@yearunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intYearunkid)
            'End If

            If intStatusID > 0 Then
                strQ &= "AND statusid = @statusid "
                objDataOperation.AddParameter("@statusid", SqlDbType.Int, eZeeDataType.INT_SIZE, intStatusID)
            End If
            'Sohail (28 Dec 2010) -- Start
            If blnGetUnpaidSalaryPeriod = True Then
                strQ &= "UNION " & _
                        "SELECT DISTINCT payperiodunkid, period_name, convert(char(8),cfcommon_period_tran.start_date,112) as start_date, CONVERT(CHAR(8), end_date, 112) AS end_date, cfcommon_period_tran.yearunkid, cfcommon_period_tran.period_code AS code  " & _
                        "FROM   " & strDatabaseName & "..prtnaleave_tran " & _
                                "LEFT JOIN " & strDatabaseName & "..cfcommon_period_tran ON prtnaleave_tran.payperiodunkid = cfcommon_period_tran.periodunkid "
                'Nilay (15-Dec-2015) -- [code]
                'Sohail (03 Dec 2013) - [yearunkid]
                'S.SANDEEP [ 27 APRIL 2012 ] -- START
                'ENHANCEMENT : TRA CHANGES
                'Sohail (21 Aug 2015) -- Start
                'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
                'If strDatabaseStartDate.Trim.Length <= 0 Then
                '    strDatabaseStartDate = FinancialYear._Object._Database_Start_Date.ToShortDateString
                'End If
                'Sohail (21 Aug 2015) -- End
                'strQ &= "WHERE   ISNULL(isvoid, 0) = 0 AND CONVERT(CHAR(8), end_date, 112) < '" & eZeeDate.convertDate(FinancialYear._Object._Database_Start_Date) & "' AND balanceamount > 0 " 'Sohail (04 Mar 2011)

                'Sohail (21 Aug 2015) -- Start
                'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
                'strQ &= "WHERE   ISNULL(isvoid, 0) = 0 AND CONVERT(CHAR(8), end_date, 112) < '" & eZeeDate.convertDate(CDate(strDatabaseStartDate)) & "' AND balanceamount > 0 " 'Sohail (04 Mar 2011)
                strQ &= "WHERE   ISNULL(isvoid, 0) = 0 AND CONVERT(CHAR(8), end_date, 112) < '" & eZeeDate.convertDate(dtDatabaseStartDate) & "' AND balanceamount > 0 "
                'Sohail (21 Aug 2015) -- End
                'S.SANDEEP [ 27 APRIL 2012 ] -- END

                'S.SANDEEP |09-FEB-2021| -- START
                'ISSUE/ENHANCEMENT : Competencies Period Changes
                If blnAddCompetencyPrdFilter Then
                    If blnIscmptperiod Then
                        strQ &= "AND ISNULL(cfcommon_period_tran.iscmptperiod,0) = 1 "
                    Else
                        strQ &= "AND ISNULL(cfcommon_period_tran.iscmptperiod,0) = 0 "
                    End If
                End If                
                'S.SANDEEP |09-FEB-2021| -- END

            End If
            'Sohail (28 Dec 2010) -- End
            'Sohail (02 Aug 2010) -- Start
            'strQ &= "ORDER BY name "
            strQ &= "ORDER BY end_date "
            'Sohail (02 Aug 2010) -- End

            objDataOperation.AddParameter("@modulerefid", SqlDbType.Int, eZeeDataType.INT_SIZE, intModuleRefid)
            objDataOperation.AddParameter("@name", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 4, "Select"))

            dsList = objDataOperation.ExecQuery(strQ, strListName)
            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If
            Return dsList
        Catch ex As Exception
            'Sohail (07 Jan 2014) -- Start
            'Enhancement - Separate TnA Periods from Payroll Periods
            'DisplayError.Show(-1, ex.Message, "getListForCombo", mstrModuleName)
            Throw New Exception(ex.Message & "; Procedure Name: getListForCombo; Module Name: " & mstrModuleName)
            'Sohail (07 Jan 2014) -- End
            Return Nothing
        Finally
            exForce = Nothing
            objDataOperation = Nothing
            dsList.Dispose()
            dsList = Nothing
        End Try

    End Function

    'Sohail (04 Mar 2011) -- Start
    Public Function GetNextPeriod(ByVal strListName As String _
                                  , ByVal intModuleRefid As Integer _
                                  , ByVal intPeriodUnkID As Integer _
                                  , ByVal iYearUnkid As Integer) As DataSet
        Dim dsList As New DataSet
        Dim objDataOperation As New clsDataOperation
        Dim strQ As String = String.Empty
        Dim exForce As Exception
        Try

            'S.SANDEEP [ 25 JUNE 2013 ] -- START
            'ENHANCEMENT : OTHER CHANGES
            'Sohail (21 Aug 2015) -- Start
            'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
            'If iYearUnkid <= 0 Then iYearUnkid = FinancialYear._Object._YearUnkid
            'Sohail (21 Aug 2015) -- End
            'S.SANDEEP [ 25 JUNE 2013 ] -- END


            strQ = "SELECT TOP 1 " & _
                            "cfcommon_period_tran.periodunkid " & _
                          ", cfcommon_period_tran.period_code " & _
                          ", cfcommon_period_tran.period_name " & _
                          ", cfcommon_period_tran.yearunkid " & _
                          ", cfcommon_period_tran.modulerefid " & _
                          ", CONVERT(CHAR(8),cfcommon_period_tran.start_date, 112) AS start_date " & _
                          ", CONVERT(CHAR(8),cfcommon_period_tran.end_date, 112) AS end_date " & _
                          ", cfcommon_period_tran.total_days " & _
                          ", cfcommon_period_tran.description " & _
                          ", cfcommon_period_tran.statusid " & _
                          ", cfcommon_period_tran.isactive " & _
                          ", cfcommon_period_tran.userunkid " & _
                          ", cfcommon_period_tran.closeuserunkid " & _
                          ", cfcommon_period_tran.inactiveuserunkid " & _
                          ", cfcommon_period_tran.constant_days " & _
                          ", CONVERT(CHAR(8),cfcommon_period_tran.tna_enddate, 112) AS tna_enddate " & _
                          ", ISNULL(cfcommon_period_tran.prescribed_interest_rate, 0) AS prescribed_interest_rate " & _
                          ", ISNULL(cfcommon_period_tran.sunjv_periodcode, cfcommon_period_tran.period_code) AS sunjv_periodcode " & _
                          ", ISNULL(cfcommon_period_tran.flexcube_batchno, '') AS flexcube_batchno " & _
                          ", ISNULL(cfcommon_period_tran.iscmptperiod,0) AS iscmptperiod " & _
                    "FROM    cfcommon_period_tran " & _
                    "WHERE   CONVERT(CHAR(8),cfcommon_period_tran.end_date, 112) > ( SELECT CONVERT(CHAR(8),cfcommon_period_tran.end_date, 112) " & _
                                         "FROM   cfcommon_period_tran " & _
                                         "WHERE  periodunkid = @periodunkid " & _
                                       ") " & _
                            "AND cfcommon_period_tran.yearunkid = @yearunkid " & _
                            "AND cfcommon_period_tran.modulerefid = @modulerefid " & _
                            "AND cfcommon_period_tran.statusid = 1 " & _
                            "AND cfcommon_period_tran.isactive = 1 " & _
                    "ORDER BY cfcommon_period_tran.end_date "
            'S.SANDEEP |09-FEB-2021| -- START {iscmptperiod} -- END
            'Sohail (26 Nov 2018) - [flexcube_batchno]
            'Nilay (16-Apr-2016) -- [sunjv_periodcode]
            'Sohail (30 Oct 2015) - [prescribed_interest_rate]
            'Sohail (07 Jan 2014) - [tna_enddate]
            'Sohail (10 Jul 2013) - [constant_days]

            objDataOperation.AddParameter("@modulerefid", SqlDbType.Int, eZeeDataType.INT_SIZE, intModuleRefid)
            'S.SANDEEP [ 25 JUNE 2013 ] -- START
            'ENHANCEMENT : OTHER CHANGES
            'objDataOperation.AddParameter("@yearunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, FinancialYear._Object._YearUnkid)
            objDataOperation.AddParameter("@yearunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, iYearUnkid)
            'S.SANDEEP [ 25 JUNE 2013 ] -- END
            objDataOperation.AddParameter("@periodunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intPeriodUnkID)

            dsList = objDataOperation.ExecQuery(strQ, strListName)
            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If
        Catch ex As Exception
            'Sohail (07 Jan 2014) -- Start
            'Enhancement - Separate TnA Periods from Payroll Periods
            'DisplayError.Show(-1, ex.Message, "GetNextPeriod", mstrModuleName)
            Throw New Exception(ex.Message & "; Procedure Name: GetNextPeriod; Module Name: " & mstrModuleName)
            'Sohail (07 Jan 2014) -- End
        Finally
            exForce = Nothing
            objDataOperation = Nothing
            dsList.Dispose()
        End Try
        Return dsList
    End Function
    'Sohail (04 Mar 2011) -- End

    'Sohail (07 Jan 2014) -- Start
    'Enhancement - Separate TnA Periods from Payroll Periods
    Public Function GetPreviousPeriod(ByVal strListName As String _
                                      , ByVal intModuleRefid As Integer _
                                      , ByVal intPeriodUnkID As Integer _
                                      , Optional ByVal intYearUnkid As Integer = 0 _
                                      , Optional ByVal intStatusID As Integer = 0 _
                                      ) As DataSet

        Dim dsList As New DataSet
        Dim objDataOperation As New clsDataOperation
        Dim strQ As String = String.Empty
        Dim exForce As Exception
        Try


            strQ = "SELECT TOP 1 " & _
                            "cfcommon_period_tran.periodunkid " & _
                          ", cfcommon_period_tran.period_code " & _
                          ", cfcommon_period_tran.period_name " & _
                          ", cfcommon_period_tran.yearunkid " & _
                          ", cfcommon_period_tran.modulerefid " & _
                          ", CONVERT(CHAR(8),cfcommon_period_tran.start_date, 112) AS start_date " & _
                          ", CONVERT(CHAR(8),cfcommon_period_tran.end_date, 112) AS end_date " & _
                          ", cfcommon_period_tran.total_days " & _
                          ", cfcommon_period_tran.description " & _
                          ", cfcommon_period_tran.statusid " & _
                          ", cfcommon_period_tran.isactive " & _
                          ", cfcommon_period_tran.userunkid " & _
                          ", cfcommon_period_tran.closeuserunkid " & _
                          ", cfcommon_period_tran.inactiveuserunkid " & _
                          ", cfcommon_period_tran.constant_days " & _
                          ", CONVERT(CHAR(8),cfcommon_period_tran.tna_enddate, 112) AS tna_enddate " & _
                          ", ISNULL(cfcommon_period_tran.prescribed_interest_rate, 0) AS prescribed_interest_rate " & _
                          ", ISNULL(cfcommon_period_tran.sunjv_periodcode, cfcommon_period_tran.period_code) AS sunjv_periodcode " & _
                          ", ISNULL(cfcommon_period_tran.flexcube_batchno, '') AS flexcube_batchno " & _
                          ", ISNULL(cfcommon_period_tran.iscmptperiod,0) AS iscmptperiod " & _
                    "FROM    cfcommon_period_tran " & _
                    "WHERE   CONVERT(CHAR(8),cfcommon_period_tran.end_date, 112) < ( SELECT CONVERT(CHAR(8),cfcommon_period_tran.end_date, 112) " & _
                                         "FROM   cfcommon_period_tran " & _
                                         "WHERE  periodunkid = @periodunkid " & _
                                       ") " & _
                            "AND cfcommon_period_tran.modulerefid = @modulerefid " & _
                            "AND cfcommon_period_tran.isactive = 1 "
            'S.SANDEEP |09-FEB-2021| -- START {iscmptperiod} -- END
            'Sohail (26 Nov 2018) - [flexcube_batchno]
            'Nilay (16-Apr-2016) -- [sunjv_periodcode]
            'Sohail (30 Oct 2015) - [prescribed_interest_rate]

            If intYearUnkid > 0 Then
                strQ &= " AND cfcommon_period_tran.yearunkid = @yearunkid  "
                objDataOperation.AddParameter("@yearunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intYearUnkid)
            End If

            If intStatusID > 0 Then
                strQ &= " AND cfcommon_period_tran.statusid = @statusid  "
                objDataOperation.AddParameter("@statusid", SqlDbType.Int, eZeeDataType.INT_SIZE, intStatusID)
            End If

            strQ &= " ORDER BY cfcommon_period_tran.end_date DESC "

            objDataOperation.AddParameter("@modulerefid", SqlDbType.Int, eZeeDataType.INT_SIZE, intModuleRefid)
            objDataOperation.AddParameter("@periodunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intPeriodUnkID)

            dsList = objDataOperation.ExecQuery(strQ, strListName)

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: GetPreviousPeriod; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            objDataOperation = Nothing
            dsList.Dispose()
        End Try
        Return dsList
    End Function

    Public Function GetTnA_StartDate(ByVal intPeriodUnkID As Integer _
                                     , Optional ByVal intModuleRefid As Integer = enModuleReference.Payroll _
                                     , Optional ByVal intYearUnkid As Integer = 0 _
                                     , Optional ByVal intStatusID As Integer = 0 _
                                     , Optional ByVal xDataOperation As clsDataOperation = Nothing) As Date 'S.SANDEEP [19 OCT 2016] -- START {xDataOperation} -- END

         'Pinkal (05-Dec-2016) -- Start
        'Enhancement - Working on Period related Issue faced by Voltamp 
        '  , Optional ByVal intModuleRefid As Integer = 0 _
        'Pinkal (05-Dec-2016) -- End

        Dim dsList As New DataSet
        'S.SANDEEP [19 OCT 2016] -- START
        'ISSUE : EMPLOYEE MASTER BIND TRANSACTION ISSUE
        'Dim objDataOperation As New clsDataOperation
        Dim objDataOperation As clsDataOperation
        If xDataOperation IsNot Nothing Then
            objDataOperation = xDataOperation
        Else
            objDataOperation = New clsDataOperation
        End If
        objDataOperation.ClearParameters()
        'S.SANDEEP [19 OCT 2016] -- END
        Dim strQ As String = String.Empty
        Dim exForce As Exception
        Dim startdate As Date = Nothing

        Try


            strQ = "SELECT TOP 1 " & _
                            " CONVERT(CHAR(8), DATEADD(Day,1, cfcommon_period_tran.tna_enddate), 112) AS tna_enddate " & _
                    "FROM    cfcommon_period_tran " & _
                    "WHERE   CONVERT(CHAR(8),cfcommon_period_tran.end_date, 112) < ( SELECT CONVERT(CHAR(8),cfcommon_period_tran.end_date, 112) " & _
                                         "FROM   cfcommon_period_tran " & _
                                         "WHERE  periodunkid = @periodunkid " & _
                                       ") " & _
                            "AND cfcommon_period_tran.isactive = 1 "

            If intModuleRefid > 0 Then
                strQ &= " AND cfcommon_period_tran.modulerefid = @modulerefid  "
                objDataOperation.AddParameter("@modulerefid", SqlDbType.Int, eZeeDataType.INT_SIZE, intModuleRefid)
            End If

            If intYearUnkid > 0 Then
                strQ &= " AND cfcommon_period_tran.yearunkid = @yearunkid  "
                objDataOperation.AddParameter("@yearunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intYearUnkid)
            End If

            If intStatusID > 0 Then
                strQ &= " AND cfcommon_period_tran.statusid = @statusid  "
                objDataOperation.AddParameter("@statusid", SqlDbType.Int, eZeeDataType.INT_SIZE, intStatusID)
            End If

            strQ &= " ORDER BY cfcommon_period_tran.end_date DESC "

            'Pinkal (25-Feb-2015) -- Start
            'Enhancement - CHANGING FOR VOLTAMP NET BF ISSUE WHEN CLOSED PERIOD.
            'objDataOperation.AddParameter("@modulerefid", SqlDbType.Int, eZeeDataType.INT_SIZE, intModuleRefid)
            'Pinkal (25-Feb-2015) -- End

            objDataOperation.AddParameter("@periodunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intPeriodUnkID)

            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            If dsList.Tables(0).Rows.Count > 0 Then
                startdate = eZeeDate.convertDate(dsList.Tables(0).Rows(0).Item("tna_enddate").ToString)
            Else
                strQ = "SELECT   CONVERT(CHAR(8),cfcommon_period_tran.start_date, 112) AS tna_enddate " & _
                        "FROM    cfcommon_period_tran " & _
                        "WHERE   cfcommon_period_tran.isactive = 1 " & _
                                "AND periodunkid = @periodunkid "

                dsList = objDataOperation.ExecQuery(strQ, "List")

                If objDataOperation.ErrorMessage <> "" Then
                    exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                    Throw exForce
                End If

                If dsList.Tables(0).Rows.Count > 0 Then
                    startdate = eZeeDate.convertDate(dsList.Tables(0).Rows(0).Item("tna_enddate").ToString)
                End If
            End If

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: GetTnA_StartDate; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            'S.SANDEEP [19 OCT 2016] -- START
            'ISSUE : EMPLOYEE MASTER BIND TRANSACTION ISSUE
            'objDataOperation = Nothing
            If xDataOperation Is Nothing Then objDataOperation = Nothing
            'S.SANDEEP [19 OCT 2016] -- END
            dsList.Dispose()
        End Try
        Return startdate
    End Function
    'Sohail (07 Jan 2014) -- End

    'Sohail (12 Oct 2011) -- Start
    Public Function getComboListForCodeNameFormat(Optional ByVal strListName As String = "List" _
                                                  , Optional ByVal mblnFlagSelect As Boolean = True _
                                                  ) As DataSet
        Dim dsList As DataSet = Nothing
        Dim objDataOperation As New clsDataOperation
        Dim strQ As String = String.Empty
        Dim exForce As Exception

        Try
            If mblnFlagSelect = True Then
                strQ = "SELECT 0 AS Id, @Select AS NAME " & _
                    "UNION "
                objDataOperation.AddParameter("@Select", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 4, "Select"))
            End If

            strQ &= "SELECT 1 AS Id, @123 AS NAME " & _
                    "UNION " & _
                    "SELECT 2 AS Id, @010203 AS NAME " & _
                    "UNION " & _
                    "SELECT 3 AS Id, @MMMYY AS NAME " & _
                    "UNION " & _
                    "SELECT 4 AS Id, @MMMMYYYY AS NAME "

            objDataOperation.AddParameter("@123", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 6, "1, 2, 3..."))
            objDataOperation.AddParameter("@010203", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 7, "01, 02, 03..."))
            objDataOperation.AddParameter("@MMMYY", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 8, "MMM - YY"))
            objDataOperation.AddParameter("@MMMMYYYY", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 9, "MMMM - YYYY"))

            dsList = objDataOperation.ExecQuery(strQ, strListName)

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

        Catch ex As Exception
            DisplayError.Show(-1, ex.Message, "getComboListForCodeNameFormat", mstrModuleName)
        Finally
            exForce = Nothing
            objDataOperation = Nothing
        End Try
        Return dsList
    End Function

    Public Function getNextStartDate(ByVal intModuleRefid As Integer _
                                     , ByVal intYearUnkid As Integer _
                                     , ByVal dtDatabase_Start_Date As Date _
                                     , ByVal dtDatabase_End_Date As Date _
                                     ) As DateTime
        'Sohail (21 Aug 2015) - [intYearUnkid, dtDatabase_Start_Date, dtDatabase_End_Date]

        Dim dsList As DataSet = Nothing
        Dim objDataOperation As New clsDataOperation
        Dim strQ As String = String.Empty
        Dim exForce As Exception
        Dim dtStart As DateTime

        Try

            strQ = "SELECT  ISNULL(MAX(end_date), '" & dtDatabase_Start_Date.ToString("dd/MMM/yyyy") & "') + 1 AS startdate " & _
                    "FROM    cfcommon_period_tran " & _
                    "WHERE   isactive = 1 " & _
                             "AND yearunkid = @yearunkid " & _
                             "AND modulerefid = @modulerefid "
            'Sohail (21 Aug 2015) - [FinancialYear._Object._Database_Start_Date = dtDatabase_Start_Date]

            'Sohail (21 Aug 2015) -- Start
            'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
            'objDataOperation.AddParameter("@yearunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, FinancialYear._Object._YearUnkid)
            objDataOperation.AddParameter("@yearunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intYearUnkid)
            'Sohail (21 Aug 2015) -- End
            objDataOperation.AddParameter("@modulerefid", SqlDbType.Int, eZeeDataType.INT_SIZE, intModuleRefid)

            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            dtStart = dsList.Tables("List").Rows(0)("startdate")
            'Sohail (19 Nov 2011) -- Start
            'If dtStart > FinancialYear._Object._Financial_End_Date Then dtStart = FinancialYear._Object._Database_End_Date
            'Sohail (21 Aug 2015) -- Start
            'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
            'If dtStart = FinancialYear._Object._Database_Start_Date.AddDays(1) Then dtStart = FinancialYear._Object._Database_Start_Date
            'If dtStart > FinancialYear._Object._Database_End_Date Then dtStart = FinancialYear._Object._Database_End_Date
            If dtStart = dtDatabase_Start_Date.AddDays(1) Then dtStart = dtDatabase_Start_Date
            If dtStart > dtDatabase_End_Date Then dtStart = dtDatabase_End_Date
            'Sohail (21 Aug 2015) -- End
            'Sohail (19 Nov 2011) -- End

        Catch ex As Exception
            DisplayError.Show(-1, ex.Message, "getNextStartDate", mstrModuleName)
        Finally
            exForce = Nothing
            objDataOperation = Nothing
        End Try
        Return dtStart
    End Function
    'Sohail (12 Oct 2011) -- End

    'S.SANDEEP [ 29 DEC 2011 ] -- START
    'ENHANCEMENT : TRA CHANGES 
    'TYPE : EMPLOYEMENT CONTRACT PROCESS
    Public Function GetPeriodByName(ByVal strPeriodName As String, ByVal intModuleRefId As Integer) As Integer
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        objDataOperation = New clsDataOperation

        Try
            strQ = "SELECT " & _
                      "  periodunkid " & _
                      "FROM cfcommon_period_tran " & _
                      "WHERE period_name = @period_name " & _
                      "AND modulerefid = @modulerefid "

            objDataOperation.AddParameter("@period_name", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, strPeriodName.ToString)
            objDataOperation.AddParameter("@modulerefid", SqlDbType.Int, eZeeDataType.INT_SIZE, intModuleRefId.ToString)

            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            If dsList.Tables(0).Rows.Count > 0 Then
                Return dsList.Tables(0).Rows(0).Item("periodunkid")
            End If

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure GetPeriodByName: GetData; Module Name: " & mstrModuleName)
        Finally
        End Try
    End Function


    'S.SANDEEP [ 29 DEC 2011 ] -- END

    'Sohail (05 Sep 2014) -- Start
    'Enhancement - Allow Previous Closed Years on Statutory Payment screen.
    Public Function GetDISTINCTDatabaseNAMEs(ByVal intModuleRefId As Integer, ByVal dtStart As Date, ByVal dtEnd As Date, Optional ByVal strFilter As String = "") As String
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        objDataOperation = New clsDataOperation

        Try
            strQ = "SELECT " & _
                      "  periodunkid " & _
                      "FROM cfcommon_period_tran " & _
                      "WHERE period_name = @period_name " & _
                      "AND modulerefid = @modulerefid "
            strQ = "DECLARE @listStr VARCHAR(MAX) " & _
                    "SELECT  @listStr = COALESCE(@listStr + ',', '') + database_name " & _
                    "FROM    ( SELECT DISTINCT " & _
                                        "database_name " & _
                              "FROM      cfcommon_period_tran " & _
                                        "LEFT JOIN hrmsConfiguration..cffinancial_year_tran ON cfcommon_period_tran.yearunkid = cffinancial_year_tran.yearunkid " & _
                              "WHERE     isactive = 1 " & _
                                        "AND modulerefid = @modulerefid " & _
                                        "AND CONVERT(CHAR(8), cfcommon_period_tran.start_date, 112) >= @start_date " & _
                                        "AND CONVERT(CHAR(8), cfcommon_period_tran.end_date, 112) <= @end_date "

            If strFilter.Trim <> "" Then
                strQ &= " AND " & strFilter
            End If

            strQ &= "       ) AS t " & _
                    "SELECT  ISNULL(@listStr, '') AS IDs "

            objDataOperation.AddParameter("@modulerefid", SqlDbType.Int, eZeeDataType.INT_SIZE, intModuleRefId.ToString)
            objDataOperation.AddParameter("@start_date", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, eZeeDate.convertDate(dtStart))
            objDataOperation.AddParameter("@end_date", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, eZeeDate.convertDate(dtEnd))

            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            Return dsList.Tables(0).Rows(0).Item("IDs").ToString

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure GetDISTINCTDatabaseNAMEs: GetData; Module Name: " & mstrModuleName)
        Finally
        End Try
    End Function
    'Sohail (05 Sep 2014) -- End

    'Sohail (07 Oct 2014) -- Start
    'Enhancement - New Report Salary Reconciliation Report for FINCA DRC.
    Public Function GetDatabaseName(ByVal intYearUnkid As Integer, ByVal intCompanyunkid As Integer) As String
        'Sohail (21 Aug 2015) - [intCompanyunkid]

        Dim strQ As String = String.Empty
        Dim objDataOperation As New clsDataOperation
        Dim dsList As New DataSet
        Dim exForce As Exception
        Dim strName As String = String.Empty
        Try

            strQ = "SELECT " & _
                                "cffinancial_year_tran.database_name " & _
                                "FROM " & FinancialYear._Object._ConfigDatabaseName & "..cffinancial_year_tran " & _
                                "WHERE yearunkid = @yearunkid "

            'Sohail (21 Aug 2015) -- Start
            'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
            'objDataOperation.AddParameter("@CompanyId", SqlDbType.Int, eZeeDataType.INT_SIZE, Company._Object._Companyunkid)
            objDataOperation.AddParameter("@CompanyId", SqlDbType.Int, eZeeDataType.INT_SIZE, intCompanyunkid)
            'Sohail (21 Aug 2015) -- End
            objDataOperation.AddParameter("@yearunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intYearUnkid)

            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            If dsList.Tables(0).Rows.Count > 0 Then
                strName = dsList.Tables(0).Rows(0)(0).ToString
            End If

            Return strName

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure:GetDatabaseName; Module Name: " & mstrModuleName)
            Return Nothing
        End Try
    End Function
    'Sohail (07 Oct 2014) -- End

	'<Language> This Auto Generated Text Please Do Not Modify it.
#Region " Language & UI Settings "
	Public Shared Sub SetMessages()
		Try
			Language.setMessage(mstrModuleName, 1, "This Period Code is already defined. Please define new Period Code.")
			Language.setMessage(mstrModuleName, 2, "This Period Name is already defined. Please define new Period Name.")
			Language.setMessage(mstrModuleName, 3, "This Period Date is already defined. Please define new Period Date.")
			Language.setMessage(mstrModuleName, 4, "Select")
			Language.setMessage(mstrModuleName, 5, "Sorry, You cannot delete selected Period. Reason: This Period is in use.")
			Language.setMessage(mstrModuleName, 6, "1, 2, 3...")
			Language.setMessage(mstrModuleName, 7, "01, 02, 03...")
			Language.setMessage(mstrModuleName, 8, "MMM - YY")
			Language.setMessage(mstrModuleName, 9, "MMMM - YYYY")

		Catch Ex As Exception
			DisplayError.Show("-1", ex.Message, "SetMessages", mstrModuleName)
		End Try
	End Sub
#End Region 'Language & UI Settings
	'</Language>
End Class