﻿
Imports eZeeCommonLib
Imports eZee.Common.eZeeForm
Imports System.Globalization

Public Class clsHandpunch
    Private Shared ReadOnly mstrModuleName As String = "clsHandpunch"
    Dim objDataOperation As clsDataOperation
    Dim mstrMessage As String = ""


#Region " Private variables "

    Private mdtFromdate As Date
    Private mdtTodate As Date

#End Region


#Region " Properties "
'S.SANDEEP [28-May-2018] -- START 
'ISSUE/ENHANCEMENT : {Audit Trails} 
Private mstrFormName As String = String.Empty
Public WriteOnly Property _FormName() As String 
Set(ByVal value As String) 
mstrFormName = value 
End Set 
End Property 
 
Private mstrClientIP As String = ""
Public WriteOnly Property _ClientIP() As String 
Set(ByVal value As String) 
mstrClientIP = value 
End Set 
End Property 
 
Private mstrHostName As String = ""
Public WriteOnly Property _HostName() As String 
Set(ByVal value As String) 
mstrHostName = value 
End Set 
End Property 
 
Private mblnIsWeb As Boolean = False
Public WriteOnly Property _FromWeb() As Boolean 
Set(ByVal value As Boolean) 
mblnIsWeb = value 
End Set 
End Property 
 
    Private mintAuditUserId As Integer = 0
    Public WriteOnly Property _AuditUserId() As Integer
        Set(ByVal value As Integer)
            mintAuditUserId = value
        End Set
    End Property

    Private mintCompanyUnkid As Integer = 0
    Public WriteOnly Property _CompanyUnkid() As Integer
        Set(ByVal value As Integer)
            mintCompanyUnkid = value
        End Set
    End Property
 
Private mdtAuditDate As DateTime = Now
Public WriteOnly Property _AuditDate() As DateTime 
Set(ByVal value As DateTime) 
mdtAuditDate = value 
End Set 
End Property 
 
Private xDataOpr As clsDataOperation = Nothing
Public Property _xDataOpr() As clsDataOperation 
Get 
Return xDataOpr 
End Get 
Set(ByVal value As clsDataOperation) 
xDataOpr = value 
End Set 
End Property 
 
Private mintLoginEmployeeunkid As Integer = 0
Public Property _LoginEmployeeunkid() As Integer 
Get 
Return mintLoginEmployeeunkid 
End Get 
Set(ByVal value As Integer) 
mintLoginEmployeeunkid = value 
End Set 
End Property 
 
Private mintVoidlogingemployeeunkid As Integer=0
Public Property _Voidloginemployeeunkid() As Integer 
Get 
Return mintVoidlogingemployeeunkid 
End Get 
Set(ByVal value As Integer) 
mintVoidlogingemployeeunkid = value 
End Set 
End Property 
'S.SANDEEP [28-May-2018] -- END 
    ''' <summary>
    ''' Purpose: Get or Set fromdate
    ''' Modify By: Pinkal
    ''' </summary>
    Public Property _Fromdate() As Date
        Get
            Return mdtFromdate
        End Get
        Set(ByVal value As Date)
            mdtFromdate = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set todate
    ''' Modify By: Pinkal
    ''' </summary>
    Public Property _Todate() As Date
        Get
            Return mdtTodate
        End Get
        Set(ByVal value As Date)
            mdtTodate = value
        End Set
    End Property

#End Region


#Region " Private Methods "

    Public Function GetHandpunchAttendanceData() As DataTable
        Dim dtTable As DataTable = Nothing
        Dim exForce As Exception
        Dim strQ As String = ""
        Try
            Dim objDataOperation As New clsDataOperation
            objDataOperation.ClearParameters()

            strQ = " SELECT DV.DeviceName, Att.DeviceUserID,Att.DeviceID,Att.AttendanceTime,Att.TACode,Att.DownloadTime FROM " & _
                      " Handpunch_Data..AttendanceLogs Att" & _
                      " JOIN Handpunch_Data..DeviceList Dv ON DV.DeviceID = Att.DeviceID AND DV.Active = 1 " & _
                       " WHERE convert(CHAR(8),Att.AttendanceTime,112) >= @FromDate AND convert(CHAR(8),Att.AttendanceTime,112) <= @ToDate Order by Att.DeviceUserID,Att.DeviceID"

            objDataOperation.AddParameter("@FromDate", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, eZeeDate.convertDate(mdtFromdate))
            objDataOperation.AddParameter("@ToDate", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, eZeeDate.convertDate(mdtTodate))

            Dim dsList As DataSet = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            If dsList IsNot Nothing Then dtTable = dsList.Tables(0)

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: GetHandpunchAttendanceData; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            objDataOperation = Nothing
        End Try
        Return dtTable
    End Function


#End Region

End Class
