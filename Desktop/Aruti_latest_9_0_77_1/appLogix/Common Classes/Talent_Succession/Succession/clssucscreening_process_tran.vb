﻿'************************************************************************************************************************************
'Class Name : clsscscreening_process_tran.vb
'Purpose    :
'Date       :30-Oct-2020
'Written By :Sohail
'Modified   :
'************************************************************************************************************************************
Imports eZeeCommonLib
''' <summary>
''' Purpose: 
''' Developer: Sohail
''' </summary>
Public Class clssucscreening_process_tran
    Private Shared ReadOnly mstrModuleName As String = "clsscscreening_process_tran"
    Dim mstrMessage As String = ""
    Dim objDataOperation As clsDataOperation

#Region " Private variables "
    Private mintProcesstranunkid As Integer
    Private mintProcessmstunkid As Integer
    Private mintScreenermstunkid As Integer
    Private mintQuestionnaireunkid As Integer
    Private msinResult As Single
    Private mstrRemark As String = String.Empty
    Private mdtTransactiondate As Date
    Private mblnIsvoid As Boolean
    Private mintVoiduserunkid As Integer
    Private mdtVoiddatetime As Date
    Private mstrVoidreason As String = String.Empty
    Private mstrHostName As String = ""
    Private mstrClientIP As String = ""
    Private mintCompanyUnkid As Integer = 0
    Private mintAuditUserId As Integer = 0
    Private mblnIsWeb As Boolean = False
    Private mstrFormName As String = ""
    Private mstrDatabaseName As String = ""
#End Region

#Region " Properties "
    ''' <summary>
    ''' Purpose: Get Message from Class 
    ''' Modify By: Sohail
    ''' </summary>
    Public ReadOnly Property _Message() As String
        Get
            Return mstrMessage
        End Get
    End Property

    ''' <summary>
    ''' Purpose: Get or Set processtranunkid
    ''' Modify By: Sohail
    ''' </summary>
    Public Property _Processtranunkid(Optional ByVal xDataOpr As clsDataOperation = Nothing) As Integer
        Get
            Return mintProcesstranunkid
        End Get
        Set(ByVal value As Integer)
            mintProcesstranunkid = value
            Call GetData(xDataOpr)
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set processmstunkid
    ''' Modify By: Sohail
    ''' </summary>
    Public Property _Processmstunkid() As Integer
        Get
            Return mintProcessmstunkid
        End Get
        Set(ByVal value As Integer)
            mintProcessmstunkid = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set screenermstunkid
    ''' Modify By: Sohail
    ''' </summary>
    Public Property _Screenermstunkid() As Integer
        Get
            Return mintScreenermstunkid
        End Get
        Set(ByVal value As Integer)
            mintScreenermstunkid = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set questionnaireunkid
    ''' Modify By: Sohail
    ''' </summary>
    Public Property _Questionnaireunkid() As Integer
        Get
            Return mintQuestionnaireunkid
        End Get
        Set(ByVal value As Integer)
            mintQuestionnaireunkid = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set result
    ''' Modify By: Sohail
    ''' </summary>
    Public Property _Result() As Single
        Get
            Return msinResult
        End Get
        Set(ByVal value As Single)
            msinResult = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set remark
    ''' Modify By: Sohail
    ''' </summary>
    Public Property _Remark() As String
        Get
            Return mstrRemark
        End Get
        Set(ByVal value As String)
            mstrRemark = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set transactiondate
    ''' Modify By: Sohail
    ''' </summary>
    Public Property _Transactiondate() As Date
        Get
            Return mdtTransactiondate
        End Get
        Set(ByVal value As Date)
            mdtTransactiondate = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set isvoid
    ''' Modify By: Sohail
    ''' </summary>
    Public Property _Isvoid() As Boolean
        Get
            Return mblnIsvoid
        End Get
        Set(ByVal value As Boolean)
            mblnIsvoid = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set voiduserunkid
    ''' Modify By: Sohail
    ''' </summary>
    Public Property _Voiduserunkid() As Integer
        Get
            Return mintVoiduserunkid
        End Get
        Set(ByVal value As Integer)
            mintVoiduserunkid = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set voiddatetime
    ''' Modify By: Sohail
    ''' </summary>
    Public Property _Voiddatetime() As Date
        Get
            Return mdtVoiddatetime
        End Get
        Set(ByVal value As Date)
            mdtVoiddatetime = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set voidreason
    ''' Modify By: Sohail
    ''' </summary>
    Public Property _Voidreason() As String
        Get
            Return mstrVoidreason
        End Get
        Set(ByVal value As String)
            mstrVoidreason = value
        End Set
    End Property

    Public WriteOnly Property _FormName() As String
        Set(ByVal value As String)
            mstrFormName = value
        End Set
    End Property

    Public WriteOnly Property _ClientIP() As String
        Set(ByVal value As String)
            mstrClientIP = value
        End Set
    End Property

    Public WriteOnly Property _HostName() As String
        Set(ByVal value As String)
            mstrHostName = value
        End Set
    End Property

    Public WriteOnly Property _FromWeb() As Boolean
        Set(ByVal value As Boolean)
            mblnIsWeb = value
        End Set
    End Property

    Public WriteOnly Property _AuditUserId() As Integer
        Set(ByVal value As Integer)
            mintAuditUserId = value
        End Set
    End Property

    Public WriteOnly Property _CompanyUnkid() As Integer
        Set(ByVal value As Integer)
            mintCompanyUnkid = value
        End Set
    End Property

    Public WriteOnly Property _DatabaseName() As String
        Set(ByVal value As String)
            mstrDatabaseName = value
        End Set
    End Property
#End Region

    ''' <summary>
    ''' Modify By: Sohail
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>
    Public Sub GetData(Optional ByVal xDataOpr As clsDataOperation = Nothing)
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception


        If xDataOpr Is Nothing Then
            objDataOperation = New clsDataOperation
        Else
            objDataOperation = xDataOpr
        End If
        objDataOperation.ClearParameters()

        Try
            strQ = "SELECT " & _
              "  processtranunkid " & _
              ", processmstunkid " & _
              ", screenermstunkid " & _
              ", questionnaireunkid " & _
              ", result " & _
              ", remark " & _
              ", questionnaireunkid " & _
              ", transactiondate " & _
              ", isvoid " & _
              ", voiduserunkid " & _
              ", voiddatetime " & _
              ", voidreason " & _
             "FROM " & mstrDatabaseName & "..sucscreening_process_tran " & _
             "WHERE processtranunkid = @processtranunkid "

            objDataOperation.AddParameter("@processtranunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintProcesstranunkid.ToString)

            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            For Each dtRow As DataRow In dsList.Tables(0).Rows
                mintProcesstranunkid = CInt(dtRow.Item("processtranunkid"))
                mintProcessmstunkid = CInt(dtRow.Item("processmstunkid"))
                mintScreenermstunkid = CInt(dtRow.Item("screenermstunkid"))
                mintQuestionnaireunkid = CInt(dtRow.Item("questionnaireunkid"))
                msinResult = dtRow.Item("result")
                mstrRemark = dtRow.Item("remark")
                mdtTransactiondate = CDate(dtRow.Item("transactiondate"))
                mblnIsvoid = CBool(dtRow.Item("isvoid"))

                Exit For
            Next
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: GetData; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            If xDataOpr Is Nothing Then objDataOperation = Nothing
        End Try
    End Sub

    ''' <summary>
    ''' Modify By: Sohail
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>
    Public Function GetList(ByVal strTableName As String, _
                            Optional ByVal intprocessmstunkid As Integer = -1, _
                            Optional ByVal intscreenermstunkid As Integer = -1) As DataSet
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        objDataOperation = New clsDataOperation

        Try
            strQ = "SELECT " & _
              "  processtranunkid " & _
              ", processmstunkid " & _
              ", screenermstunkid " & _
              ", questionnaireunkid " & _
              ", result " & _
              ", remark " & _
              ", transactiondate " & _
              ", isvoid " & _
              ", voiduserunkid " & _
              ", voiddatetime " & _
              ", voidreason " & _
             "FROM " & mstrDatabaseName & "..sucscreening_process_tran " & _
             "WHERE sucscreening_process_tran.isvoid = 0 "


            If intprocessmstunkid > 0 Then
                strQ &= "and sucscreening_process_tran.processmstunkid = @processmstunkid "
                objDataOperation.AddParameter("@processmstunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intprocessmstunkid)
            End If

            If intscreenermstunkid > 0 Then
                strQ &= "and sucscreening_process_tran.screenermstunkid = @screenermstunkid "
                objDataOperation.AddParameter("@screenermstunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intscreenermstunkid)
            End If

            dsList = objDataOperation.ExecQuery(strQ, strTableName)

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: GetList; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
        Return dsList
    End Function

    Public Function SaveProcessTran(Optional ByVal xDataOpr As clsDataOperation = Nothing) As Boolean
        Dim exForce As Exception

        If xDataOpr Is Nothing Then
            objDataOperation = New clsDataOperation
            objDataOperation.BindTransaction()
        Else
            objDataOperation = xDataOpr
        End If
        objDataOperation.ClearParameters()
        Try

            If isExist(mintProcessmstunkid, mintScreenermstunkid, mintQuestionnaireunkid, -1, xDataOpr) Then
                If Update(xDataOpr) = False Then
                    exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                    Throw exForce
                End If
            Else
                If Insert(xDataOpr) = False Then
                    exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                    Throw exForce
                End If
            End If


            If xDataOpr Is Nothing Then objDataOperation.ReleaseTransaction(True)
            Return True
        Catch ex As Exception
            If xDataOpr Is Nothing Then objDataOperation.ReleaseTransaction(False)
            Throw New Exception(ex.Message & "; Procedure Name: Insert; Module Name: " & mstrModuleName)
            Return False
        Finally
            exForce = Nothing
            If xDataOpr Is Nothing Then objDataOperation = Nothing
        End Try
    End Function

    Public Function Insert(Optional ByVal xDataOpr As clsDataOperation = Nothing) As Boolean
        Dim strQ As String = ""
        Dim exForce As Exception
        Dim dsList As DataSet = Nothing

        If xDataOpr Is Nothing Then
            objDataOperation = New clsDataOperation
            objDataOperation.BindTransaction()
        Else
            objDataOperation = xDataOpr
        End If
        objDataOperation.ClearParameters()

        Try
            objDataOperation.AddParameter("@processmstunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintProcessmstunkid.ToString)
            objDataOperation.AddParameter("@screenermstunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintScreenermstunkid.ToString)
            objDataOperation.AddParameter("@questionnaireunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintQuestionnaireunkid.ToString)
            objDataOperation.AddParameter("@result", SqlDbType.Decimal, eZeeDataType.FLOAT_SIZE, msinResult.ToString)
            objDataOperation.AddParameter("@remark", SqlDbType.NVarChar, mstrRemark.Trim.Length, mstrRemark.ToString)
            objDataOperation.AddParameter("@isvoid", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsvoid.ToString)

            strQ = "INSERT INTO " & mstrDatabaseName & "..sucscreening_process_tran ( " & _
              "  processmstunkid " & _
              ", screenermstunkid " & _
              ", questionnaireunkid " & _
              ", result " & _
              ", remark " & _
              ", transactiondate " & _
              ", isvoid " & _
            ") VALUES (" & _
              "  @processmstunkid " & _
              ", @screenermstunkid " & _
              ", @questionnaireunkid " & _
              ", @result " & _
              ", @remark " & _
              ", getdate() " & _
              ", @isvoid " & _
            "); SELECT @@identity"

            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            mintProcesstranunkid = dsList.Tables(0).Rows(0).Item(0)

            If InsertAuditTrails(objDataOperation, enAuditType.ADD, mintProcesstranunkid) = False Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            If xDataOpr Is Nothing Then objDataOperation.ReleaseTransaction(True)

            Return True
        Catch ex As Exception
            If xDataOpr Is Nothing Then objDataOperation.ReleaseTransaction(False)
            Throw New Exception(ex.Message & "; Procedure Name: Insert; Module Name: " & mstrModuleName)
            Return False
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            If xDataOpr Is Nothing Then objDataOperation = Nothing
        End Try
    End Function

    Public Function Update(Optional ByVal xDataOpr As clsDataOperation = Nothing) As Boolean
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        If xDataOpr Is Nothing Then
            objDataOperation = New clsDataOperation
            objDataOperation.BindTransaction()
        Else
            objDataOperation = xDataOpr
        End If
        objDataOperation.ClearParameters()

        Try
            objDataOperation.AddParameter("@processmstunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintProcessmstunkid.ToString)
            objDataOperation.AddParameter("@screenermstunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintScreenermstunkid.ToString)
            objDataOperation.AddParameter("@questionnaireunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintQuestionnaireunkid.ToString)
            objDataOperation.AddParameter("@result", SqlDbType.Decimal, eZeeDataType.FLOAT_SIZE, msinResult.ToString)
            objDataOperation.AddParameter("@remark", SqlDbType.NVarChar, mstrRemark.Trim.Length, mstrRemark.ToString)
            objDataOperation.AddParameter("@isvoid", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsvoid.ToString)
            objDataOperation.AddParameter("@voiduserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintVoiduserunkid.ToString)
            If mdtVoiddatetime <> Nothing Then
                objDataOperation.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, mdtVoiddatetime)
            Else
                objDataOperation.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, DBNull.Value)
            End If
            objDataOperation.AddParameter("@voidreason", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrVoidreason.ToString)

            strQ = "UPDATE " & mstrDatabaseName & "..sucscreening_process_tran SET " & _
              " result = @result" & _
              ", remark = @remark" & _
              ", transactiondate = getdate() " & _
              ", isvoid = @isvoid" & _
              ", voiduserunkid = @voiduserunkid" & _
              ", voiddatetime = @voiddatetime" & _
              ", voidreason = @voidreason " & _
              "WHERE processmstunkid = @processmstunkid " & _
              "AND screenermstunkid = @screenermstunkid " & _
              "AND questionnaireunkid = @questionnaireunkid "
            Call objDataOperation.ExecNonQuery(strQ)

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            If InsertAuditTrails(objDataOperation, enAuditType.EDIT, -1, mintProcessmstunkid, mintScreenermstunkid, mintQuestionnaireunkid) = False Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            If xDataOpr Is Nothing Then objDataOperation.ReleaseTransaction(True)

            Return True
        Catch ex As Exception
            If xDataOpr Is Nothing Then objDataOperation.ReleaseTransaction(False)
            Throw New Exception(ex.Message & "; Procedure Name: Update; Module Name: " & mstrModuleName)
            Return False
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            If xDataOpr Is Nothing Then objDataOperation = Nothing
        End Try
    End Function

    Public Function Delete(ByVal strProcessmstunkid As String, _
                           Optional ByVal xDataOpr As clsDataOperation = Nothing, _
                           Optional ByVal intScreenerMstUnkId As Integer = 0) As Boolean
        'If isUsed(intUnkid) Then
        '    mstrMessage = "<Message>"
        '    Return False
        'End If

        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception


        If xDataOpr Is Nothing Then
            objDataOperation = New clsDataOperation
            objDataOperation.BindTransaction()
        Else
            objDataOperation = xDataOpr
        End If
        objDataOperation.ClearParameters()
        Try
            strQ = "UPDATE " & mstrDatabaseName & "..sucscreening_process_tran SET " & _
                   "  isvoid = @isvoid" & _
                   ", voiduserunkid = @voiduserunkid" & _
                   ", voiddatetime = @voiddatetime" & _
                   ", voidreason = @voidreason " & _
                   "WHERE processmstunkid in (" & strProcessmstunkid & ") "

            If intScreenerMstUnkId > 0 Then
                strQ &= " AND screenermstunkid = @screenermstunkid "
                objDataOperation.AddParameter("@screenermstunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intScreenerMstUnkId)
            End If

            objDataOperation.AddParameter("@isvoid", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsvoid.ToString)
            objDataOperation.AddParameter("@voiduserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintVoiduserunkid.ToString)
            objDataOperation.AddParameter("@voidreason", SqlDbType.NVarChar, eZeeDataType.DESC_SIZE, mstrVoidreason.ToString)
            If mdtVoiddatetime <> Nothing Then
                objDataOperation.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, mdtVoiddatetime)
            Else
                objDataOperation.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, DBNull.Value)
            End If

            Call objDataOperation.ExecNonQuery(strQ)

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            If InsertAuditTrails(objDataOperation, enAuditType.DELETE, -1, strProcessmstunkid, intScreenerMstUnkId) = False Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            If xDataOpr Is Nothing Then objDataOperation.ReleaseTransaction(True)

            Return True
        Catch ex As Exception
            If xDataOpr Is Nothing Then objDataOperation.ReleaseTransaction(False)
            Throw New Exception(ex.Message & "; Procedure Name: Delete; Module Name: " & mstrModuleName)
            Return False
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            If xDataOpr Is Nothing Then objDataOperation = Nothing
        End Try
    End Function

    Public Function isUsed(ByVal intUnkid As Integer) As Boolean
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        objDataOperation = New clsDataOperation

        Try
            strQ = "<Query>"

            objDataOperation.AddParameter("@processtranunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intUnkid)

            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            Return dsList.Tables(0).Rows.Count > 0
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: isUsed; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
    End Function

    Public Function isExist(ByVal intProcessMstId As Integer, _
                            ByVal intScreenerMstId As Integer, _
                            Optional ByVal intQuestionnaireId As Integer = -1, _
                            Optional ByVal intUnkid As Integer = -1, _
                            Optional ByVal xDataOpr As clsDataOperation = Nothing) As Boolean
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        If xDataOpr Is Nothing Then
            objDataOperation = New clsDataOperation
        Else
            objDataOperation = xDataOpr
        End If
        objDataOperation.ClearParameters()

        Try
            strQ = "SELECT " & _
              "  processtranunkid " & _
              ", processmstunkid " & _
              ", screenermstunkid " & _
              ", questionnaireunkid " & _
              ", result " & _
              ", remark " & _
              ", transactiondate " & _
              ", isvoid " & _
              ", voiduserunkid " & _
              ", voiddatetime " & _
              ", voidreason " & _
              " FROM " & mstrDatabaseName & "..sucscreening_process_tran " & _
              " WHERE isvoid = 0 " & _
              " AND processmstunkid  = @processmstunkid " & _
              " AND screenermstunkid = @screenermstunkid "

            If intQuestionnaireId > 0 Then
                strQ &= " AND questionnaireunkid = @questionnaireunkid "
                objDataOperation.AddParameter("@questionnaireunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intQuestionnaireId)
            End If


            If intUnkid > 0 Then
                strQ &= " AND processtranunkid <> @processtranunkid"
            End If

            objDataOperation.AddParameter("@processtranunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intUnkid)
            objDataOperation.AddParameter("@processmstunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intProcessMstId)
            objDataOperation.AddParameter("@screenermstunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intScreenerMstId)

            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            Return dsList.Tables(0).Rows.Count > 0
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: isExist; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            If xDataOpr Is Nothing Then objDataOperation = Nothing
        End Try
    End Function

    Public Function InsertAuditTrails(ByVal objDataOperation As clsDataOperation, _
                                      ByVal eAuditType As enAuditType, _
                                      Optional ByVal intProcesstranunkid As Integer = -1, _
                                      Optional ByVal strProcessmstunkid As String = "", _
                                      Optional ByVal intScreenermstunkid As Integer = -1, _
                                      Optional ByVal intQuestionnaireunkid As Integer = -1) As Boolean
        Dim StrQ As String = ""

        Try
            StrQ = "INSERT INTO " & mstrDatabaseName & "..atsucscreening_process_tran ( " & _
                    "  tranguid " & _
                    ", processtranunkid " & _
                    ", processmstunkid " & _
                    ", screenermstunkid " & _
                    ", questionnaireunkid " & _
                    ", result " & _
                    ", remark " & _
                    ", transactiondate " & _
                    ", audittypeid " & _
                    ", audtuserunkid " & _
                    ", auditdatetime " & _
                    ", formname " & _
                    ", ip " & _
                    ", host " & _
                    ", isweb" & _
                    ") Select " & _
                    "  LOWER(NEWID()) " & _
                    ", processtranunkid " & _
                    ", processmstunkid " & _
                    ", screenermstunkid " & _
                    ", questionnaireunkid " & _
                    ", result " & _
                    ", remark " & _
                    ", GETDATE() " & _
                    ", @audittypeid " & _
                    ", @audtuserunkid " & _
                    ", GETDATE() " & _
                    ", @formname " & _
                    ", @ip " & _
                    ", @host " & _
                    ", @isweb" & _
                    " From " & mstrDatabaseName & "..sucscreening_process_tran Where 1=1 "

            objDataOperation.ClearParameters()


            If intProcesstranunkid > 0 Then
                StrQ &= " AND sucscreening_process_tran.processtranunkid = @processtranunkid "
                objDataOperation.AddParameter("@processtranunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intProcesstranunkid)
            End If

            If strProcessmstunkid.Length > 0 Then
                StrQ &= " AND sucscreening_process_tran.processmstunkid in (" & strProcessmstunkid & ") "
            End If

            If intScreenermstunkid > 0 Then
                StrQ &= " AND sucscreening_process_tran.screenermstunkid = @screenermstunkid "
                objDataOperation.AddParameter("@screenermstunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intScreenermstunkid)
            End If

            If intQuestionnaireunkid > 0 Then
                StrQ &= " AND sucscreening_process_tran.questionnaireunkid = @questionnaireunkid "
                objDataOperation.AddParameter("@questionnaireunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intQuestionnaireunkid)
            End If

            objDataOperation.AddParameter("@audittypeid", SqlDbType.Int, eZeeDataType.INT_SIZE, eAuditType)
            objDataOperation.AddParameter("@audtuserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintAuditUserId)
            objDataOperation.AddParameter("@formname", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrFormName)
            objDataOperation.AddParameter("@ip", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrClientIP)
            objDataOperation.AddParameter("@host", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrHostName)
            objDataOperation.AddParameter("@isweb", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsWeb)

            objDataOperation.ExecNonQuery(StrQ)

            If objDataOperation.ErrorMessage <> "" Then
                Throw New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
            End If

            Return True
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: InsertAuditTrails; Module Name: " & mstrModuleName)
        Finally
        End Try
    End Function

    Public Function GetOtherScreenerData(ByVal strTableName As String, _
                                         ByVal intProcessmstunkid As Integer, _
                                         ByVal intScreenermstunkid As Integer, _
                                         ByVal intCurrentScreenerid As Integer, _
                                         Optional ByVal blnShowAllScreenerData As Boolean = False, _
                                         Optional ByVal xDataOpr As clsDataOperation = Nothing) As DataSet

        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        If xDataOpr Is Nothing Then
            objDataOperation = New clsDataOperation
        Else
            objDataOperation = xDataOpr
        End If
        objDataOperation.ClearParameters()

        Try

            strQ = "SELECT " & _
                    "question.question " & _
                    ",process_tran.result " & _
                    ",process_tran.remark " & _
                    "FROM " & mstrDatabaseName & "..sucscreening_process_tran AS process_tran " & _
                    "join " & mstrDatabaseName & "..sucquestionnaire_master as question " & _
                    "on question.questionnaireunkid= process_tran.questionnaireunkid " & _
                    "and question.isactive = 1 " & _
                    "WHERE process_tran.isvoid = 0 and process_tran.processmstunkid = @processmstunkid " & _
                    "and process_tran.screenermstunkid = @screenermstunkid " & _
                    "and process_tran.screenermstunkid not in(" & intCurrentScreenerid & " ) "


            objDataOperation.AddParameter("@processmstunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intProcessmstunkid)
            objDataOperation.AddParameter("@screenermstunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intScreenermstunkid)

            dsList = objDataOperation.ExecQuery(strQ, strTableName)

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: GetOtherScreenerData; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            If xDataOpr Is Nothing Then objDataOperation = Nothing
        End Try
        Return dsList
    End Function

End Class
