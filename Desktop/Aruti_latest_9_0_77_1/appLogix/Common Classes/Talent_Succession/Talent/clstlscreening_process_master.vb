﻿'************************************************************************************************************************************
'Class Name : clstlscreening_process_master.vb
'Purpose    :
'Date       :13-Oct-2020
'Written By :Hemant
'Modified   :
'************************************************************************************************************************************
Imports eZeeCommonLib
''' <summary>
''' Purpose: 
''' Developer: Hemant
''' </summary>
Public Class clstlscreening_process_master
    Private Shared ReadOnly mstrModuleName As String = "clstlscreening_process_master"
    Dim mstrMessage As String = ""
    Dim objDataOperation As clsDataOperation

#Region " Private variables "
    Private mintProcessmstunkid As Integer
    Private mintCycleunkid As Integer
    Private mintEmployeeunkid As Integer
    Private mintStageunkid As Integer
    Private mintScreenermstunkid As Integer
    Private mintAvgScreenermstunkid As Integer
    Private mintUserunkid As Integer
    Private mblnIsvoid As Boolean
    Private mintVoiduserunkid As Integer
    Private mdtVoiddatetime As Date
    Private mstrVoidreason As String = String.Empty
    Private mstrHostName As String = ""
    Private mstrClientIP As String = ""
    Private mintCompanyUnkid As Integer = 0
    Private mintAuditUserId As Integer = 0
    Private mblnIsWeb As Boolean = False
    Private mstrFormName As String = ""
    Private mstrDatabaseName As String = ""
    Private mdtQuestionnaire As DataTable = Nothing

    Private mblnIsapproved As Boolean = False
    Private mblnIsdisapproved As Boolean = False
    Private mstrRemark As String = ""
    Private mdtApprovaldate As Date = Nothing

    Private mblnIsmatch As Boolean = True

#End Region

#Region " Properties "
    ''' <summary>
    ''' Purpose: Get Message from Class 
    ''' Modify By: Hemant
    ''' </summary>
    Public ReadOnly Property _Message() As String
        Get
            Return mstrMessage
        End Get
    End Property

    Public Property _DtQuestionnaire() As DataTable
        Get
            Return mdtQuestionnaire
        End Get
        Set(ByVal value As DataTable)
            mdtQuestionnaire = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set processmstunkid
    ''' Modify By: Hemant
    ''' </summary>
    Public Property _Processmstunkid(Optional ByVal xDataOpr As clsDataOperation = Nothing) As Integer
        Get
            Return mintProcessmstunkid
        End Get
        Set(ByVal value As Integer)
            mintProcessmstunkid = value
            Call GetData(xDataOpr)
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set cycleunkid
    ''' Modify By: Hemant
    ''' </summary>
    Public Property _Cycleunkid() As Integer
        Get
            Return mintCycleunkid
        End Get
        Set(ByVal value As Integer)
            mintCycleunkid = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set employeeunkid
    ''' Modify By: Hemant
    ''' </summary>
    Public Property _Employeeunkid() As Integer
        Get
            Return mintEmployeeunkid
        End Get
        Set(ByVal value As Integer)
            mintEmployeeunkid = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set stageunkid
    ''' Modify By: Hemant
    ''' </summary>
    Public Property _Stageunkid() As Integer
        Get
            Return mintStageunkid
        End Get
        Set(ByVal value As Integer)
            mintStageunkid = value
        End Set
    End Property

    Public Property _Screenermstunkid() As Integer
        Get
            Return mintScreenermstunkid
        End Get
        Set(ByVal value As Integer)
            mintScreenermstunkid = value
        End Set
    End Property

    Public Property _AverageScreenermstunkid() As Integer
        Get
            Return mintAvgScreenermstunkid
        End Get
        Set(ByVal value As Integer)
            mintAvgScreenermstunkid = value
        End Set
    End Property


    ''' <summary>
    ''' Purpose: Get or Set userunkid
    ''' Modify By: Hemant
    ''' </summary>
    Public Property _Userunkid() As String
        Get
            Return mintUserunkid
        End Get
        Set(ByVal value As String)
            mintUserunkid = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set isvoid
    ''' Modify By: Hemant
    ''' </summary>
    Public Property _Isvoid() As Boolean
        Get
            Return mblnIsvoid
        End Get
        Set(ByVal value As Boolean)
            mblnIsvoid = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set voiduserunkid
    ''' Modify By: Hemant
    ''' </summary>
    Public Property _Voiduserunkid() As Integer
        Get
            Return mintVoiduserunkid
        End Get
        Set(ByVal value As Integer)
            mintVoiduserunkid = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set voiddatetime
    ''' Modify By: Hemant
    ''' </summary>
    Public Property _Voiddatetime() As Date
        Get
            Return mdtVoiddatetime
        End Get
        Set(ByVal value As Date)
            mdtVoiddatetime = value
        End Set
    End Property


    ''' <summary>
    ''' Purpose: Get or Set voidreason
    ''' Modify By: Hemant
    ''' </summary>
    Public Property _Voidreason() As String
        Get
            Return mstrVoidreason
        End Get
        Set(ByVal value As String)
            mstrVoidreason = value
        End Set
    End Property

    Public WriteOnly Property _FormName() As String
        Set(ByVal value As String)
            mstrFormName = value
        End Set
    End Property

    Public WriteOnly Property _ClientIP() As String
        Set(ByVal value As String)
            mstrClientIP = value
        End Set
    End Property

    Public WriteOnly Property _HostName() As String
        Set(ByVal value As String)
            mstrHostName = value
        End Set
    End Property

    Public WriteOnly Property _FromWeb() As Boolean
        Set(ByVal value As Boolean)
            mblnIsWeb = value
        End Set
    End Property

    Public WriteOnly Property _AuditUserId() As Integer
        Set(ByVal value As Integer)
            mintAuditUserId = value
        End Set
    End Property

    Public WriteOnly Property _CompanyUnkid() As Integer
        Set(ByVal value As Integer)
            mintCompanyUnkid = value
        End Set
    End Property

    Public WriteOnly Property _DatabaseName() As String
        Set(ByVal value As String)
            mstrDatabaseName = value
        End Set
    End Property

    Public Property _Isapproved() As Boolean
        Get
            Return mblnIsapproved
        End Get
        Set(ByVal value As Boolean)
            mblnIsapproved = value
        End Set
    End Property

    Public Property _Isdisapproved() As Boolean
        Get
            Return mblnIsdisapproved
        End Get
        Set(ByVal value As Boolean)
            mblnIsdisapproved = value
        End Set
    End Property

    Public Property _Remark() As String
        Get
            Return mstrRemark
        End Get
        Set(ByVal value As String)
            mstrRemark = value
        End Set
    End Property

    Public Property _Approvaldate() As Date
        Get
            Return mdtApprovaldate
        End Get
        Set(ByVal value As Date)
            mdtApprovaldate = value
        End Set
    End Property

    Public Property _Ismatch() As Boolean
        Get
            Return mblnIsmatch
        End Get
        Set(ByVal value As Boolean)
            mblnIsmatch = value
        End Set
    End Property

#End Region


#Region " Constuctor "
    Public Sub New()
        mdtQuestionnaire = New DataTable()
        Dim dCol As DataColumn
        Try
            dCol = New DataColumn("questionnaireunkid")
            dCol.DataType = System.Type.GetType("System.Int32")
            mdtQuestionnaire.Columns.Add(dCol)

            dCol = New DataColumn("result")
            dCol.DataType = System.Type.GetType("System.Decimal")
            mdtQuestionnaire.Columns.Add(dCol)

            dCol = New DataColumn("remark")
            dCol.DataType = System.Type.GetType("System.String")
            mdtQuestionnaire.Columns.Add(dCol)


        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "New", mstrModuleName)
        End Try
    End Sub
#End Region

    Public Sub GetData(Optional ByVal xDataOpr As clsDataOperation = Nothing)
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        Dim objDataOperation As clsDataOperation
        If xDataOpr Is Nothing Then
            objDataOperation = New clsDataOperation
        Else
            objDataOperation = xDataOpr
        End If
        objDataOperation.ClearParameters()
        Try
            strQ = "SELECT " & _
              "  processmstunkid " & _
              ", cycleunkid " & _
              ", employeeunkid " & _
              ", stageunkid " & _
              ", userunkid " & _
              ", isvoid " & _
              ", ismatch " & _
              ", voiduserunkid " & _
              ", voiddatetime " & _
              ", voidreason " & _
              ", isapproved " & _
              ", isdisapproved " & _
              ",remark " & _
             "FROM " & mstrDatabaseName & "..tlscreening_process_master " & _
             "WHERE processmstunkid = @processmstunkid and ismatch = 1 "

            objDataOperation.AddParameter("@processmstunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintProcessmstunkid.ToString)

            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            For Each dtRow As DataRow In dsList.Tables(0).Rows
                mintProcessmstunkid = CInt(dtRow.Item("processmstunkid"))
                mintCycleunkid = CInt(dtRow.Item("cycleunkid"))
                mintEmployeeunkid = CInt(dtRow.Item("employeeunkid"))
                mintStageunkid = CInt(dtRow.Item("stageunkid"))
                mintUserunkid = CInt(dtRow.Item("userunkid"))
                mblnIsvoid = CBool(dtRow.Item("isvoid"))
                mblnIsapproved = CBool(dtRow.Item("isapproved"))
                mblnIsdisapproved = CBool(dtRow.Item("isdisapproved"))
                mstrRemark = CStr(dtRow.Item("remark"))
                mblnIsmatch = CBool(dtRow.Item("ismatch"))
                Exit For
            Next
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: GetData; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            If xDataOpr Is Nothing Then objDataOperation = Nothing
        End Try
    End Sub

    Public Function GetList(ByVal strTableName As String, Optional ByVal intCycleunkid As Integer = -1, Optional ByVal blnIsMatchOnly As Boolean = True, Optional ByVal xDataOpr As clsDataOperation = Nothing) As DataSet
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        If xDataOpr IsNot Nothing Then
            objDataOperation = xDataOpr
        Else
        objDataOperation = New clsDataOperation
        End If
        objDataOperation.ClearParameters()

        Try
            strQ = "SELECT " & _
              "  tlscreening_process_master.processmstunkid " & _
              ", tlscreening_process_master.cycleunkid " & _
              ", tlscreening_process_master.employeeunkid " & _
              ", tlscreening_process_master.stageunkid " & _
              ", tlscreening_process_master.userunkid " & _
              ", tlscreening_process_master.isvoid " & _
              ", tlscreening_process_master.ismatch " & _
              ", CASE WHEN ISNULL(cfuser_master.firstname,'')+' '+ISNULL(cfuser_master.lastname,'') = '' THEN cfuser_master.username " & _
              "  ELSE ISNULL(cfuser_master.firstname, '') + ' ' + ISNULL(cfuser_master.lastname, '') End as name " & _
              ", ISNULL(tlcycle_master.name, '') as period " & _
              ", tlscreening_process_master.isapproved " & _
              ", tlscreening_process_master.isdisapproved " & _
              ", tlscreening_process_master.remark " & _
              ", tlscreening_process_master.approvaldate " & _
             "FROM " & mstrDatabaseName & "..tlscreening_process_master " & _
             "join hrmsConfiguration..cfuser_master on cfuser_master.userunkid  = tlscreening_process_master.userunkid " & _
             " LEFT JOIN tlcycle_master ON tlcycle_master.cycleunkid = tlscreening_process_master.cycleunkid " & _
             "WHERE tlscreening_process_master.isvoid = 0  "

            If blnIsMatchOnly Then
                strQ &= " and tlscreening_process_master.ismatch = 1 "
            End If

            If intCycleunkid > 0 Then
                strQ &= " and tlscreening_process_master.cycleunkid = @cycleunkid "
                objDataOperation.AddParameter("@cycleunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intCycleunkid)
            End If


            dsList = objDataOperation.ExecQuery(strQ, strTableName)

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: GetList; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            If xDataOpr Is Nothing Then objDataOperation = Nothing

        End Try
        Return dsList
    End Function

    Public Function SaveScreening(Optional ByVal xDataOpr As clsDataOperation = Nothing) As Boolean
        Dim exForce As Exception

        If xDataOpr Is Nothing Then
            objDataOperation = New clsDataOperation
            objDataOperation.BindTransaction()
        Else
            objDataOperation = xDataOpr
        End If
        objDataOperation.ClearParameters()

        Try

            If isExist(mintCycleunkid, mintEmployeeunkid, mintProcessmstunkid, objDataOperation) Then
                If Update(objDataOperation) = False Then
                    exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                    Throw exForce
                End If
            Else
                If Insert(objDataOperation) = False Then
                    exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                    Throw exForce
                End If
            End If

            If xDataOpr Is Nothing Then
                objDataOperation.ReleaseTransaction(True)
            End If

            Return True
        Catch ex As Exception
            If xDataOpr Is Nothing Then
                objDataOperation.ReleaseTransaction(False)
            End If
            Throw New Exception(ex.Message & "; Procedure Name: SaveScreening; Module Name: " & mstrModuleName)
            Return False
        Finally
            exForce = Nothing
            If xDataOpr Is Nothing Then objDataOperation = Nothing

        End Try
    End Function

    Public Function Insert(Optional ByVal xDataOpr As clsDataOperation = Nothing) As Boolean

        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception
        Dim objtlscreening_process_tran As New clstlscreening_process_tran
        Dim objtlscreening_stages_tran As New clstlscreening_stages_tran

        If xDataOpr Is Nothing Then
            objDataOperation = New clsDataOperation
            objDataOperation.BindTransaction()
        Else
            objDataOperation = xDataOpr
        End If
        objDataOperation.ClearParameters()

        Try
            objDataOperation.AddParameter("@cycleunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintCycleunkid.ToString)
            objDataOperation.AddParameter("@employeeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintEmployeeunkid.ToString)
            objDataOperation.AddParameter("@stageunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintAvgScreenermstunkid.ToString)
            objDataOperation.AddParameter("@userunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintUserunkid.ToString)
            objDataOperation.AddParameter("@isvoid", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsvoid.ToString)
            objDataOperation.AddParameter("@ismatch", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsmatch)

            objDataOperation.AddParameter("@isapproved", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsapproved)
            objDataOperation.AddParameter("@isdisapproved", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsdisapproved)
            objDataOperation.AddParameter("@remark", SqlDbType.NVarChar, mstrRemark.Length, mstrRemark)
            If mdtApprovaldate <> Nothing Then
                objDataOperation.AddParameter("@approvaldate", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, mdtApprovaldate)
            Else
                objDataOperation.AddParameter("@approvaldate", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, DBNull.Value)
            End If

            strQ = "INSERT INTO " & mstrDatabaseName & "..tlscreening_process_master ( " & _
              "  cycleunkid " & _
              ", employeeunkid " & _
              ", stageunkid " & _
              ", userunkid " & _
              ", isvoid " & _
              ", ismatch " & _
              ", isapproved " & _
              ", isdisapproved " & _
              ", remark " & _
              ", approvaldate " & _
            ") VALUES (" & _
              "  @cycleunkid " & _
              ", @employeeunkid " & _
              ", @stageunkid " & _
              ", @userunkid " & _
              ", @isvoid " & _
              ", @ismatch " & _
              ", @isapproved " & _
              ", @isdisapproved " & _
              ", @remark " & _
              ", @approvaldate " & _
            "); SELECT @@identity"

            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            mintProcessmstunkid = dsList.Tables(0).Rows(0).Item(0)

            objtlscreening_stages_tran._Cycleunkid = mintCycleunkid
            objtlscreening_stages_tran._Employeeunkid = mintEmployeeunkid
            objtlscreening_stages_tran._Stageunkid = mintStageunkid
            objtlscreening_stages_tran._Isvoid = False
            objtlscreening_stages_tran._AuditUserId = mintAuditUserId
            objtlscreening_stages_tran._HostName = mstrHostName
            objtlscreening_stages_tran._ClientIP = mstrClientIP
            objtlscreening_stages_tran._FromWeb = mblnIsWeb
            objtlscreening_stages_tran._DatabaseName = mstrDatabaseName
            objtlscreening_stages_tran._Processmstunkid = mintProcessmstunkid
            objtlscreening_stages_tran._Screenermstunkid = mintScreenermstunkid
            objtlscreening_stages_tran._FormName = mstrFormName

            If objtlscreening_stages_tran.SaveStageTran(objDataOperation) = False Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            objtlscreening_process_tran._Processmstunkid = mintProcessmstunkid
            objtlscreening_process_tran._Screenermstunkid = mintScreenermstunkid
            objtlscreening_process_tran._Isvoid = False
            objtlscreening_process_tran._AuditUserId = mintAuditUserId
            objtlscreening_process_tran._HostName = mstrHostName
            objtlscreening_process_tran._ClientIP = mstrClientIP
            objtlscreening_process_tran._FromWeb = mblnIsWeb
            objtlscreening_process_tran._DatabaseName = mstrDatabaseName
            objtlscreening_process_tran._FormName = mstrFormName

            For Each drow As DataRow In mdtQuestionnaire.Rows
                objtlscreening_process_tran._Questionnaireunkid = CInt(drow("questionnaireunkid").ToString())
                objtlscreening_process_tran._Result = CDbl(drow("result"))
                objtlscreening_process_tran._Remark = drow("remark").ToString()

                If objtlscreening_process_tran.SaveProcessTran(objDataOperation) = False Then
                    exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                    Throw exForce
                End If
            Next

            If InsertAuditTrails(objDataOperation, enAuditType.ADD, mintProcessmstunkid.ToString()) = False Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            If xDataOpr Is Nothing Then objDataOperation.ReleaseTransaction(True)

            Return True
        Catch ex As Exception
            If xDataOpr Is Nothing Then objDataOperation.ReleaseTransaction(False)
            Throw New Exception(ex.Message & "; Procedure Name: Insert; Module Name: " & mstrModuleName)
            Return False
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            If xDataOpr Is Nothing Then objDataOperation = Nothing
            objtlscreening_process_tran = Nothing
            objtlscreening_stages_tran = Nothing

        End Try
    End Function

    Public Function Update(Optional ByVal xDataOpr As clsDataOperation = Nothing) As Boolean

        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception
        Dim objtlscreening_process_tran As New clstlscreening_process_tran
        Dim objtlscreening_stages_tran As New clstlscreening_stages_tran

        If xDataOpr Is Nothing Then
            objDataOperation = New clsDataOperation
            objDataOperation.BindTransaction()
        Else
            objDataOperation = xDataOpr
        End If
        objDataOperation.ClearParameters()

        Try
            objDataOperation.AddParameter("@cycleunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintCycleunkid.ToString)
            objDataOperation.AddParameter("@employeeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintEmployeeunkid.ToString)
            objDataOperation.AddParameter("@stageunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintAvgScreenermstunkid.ToString)
            objDataOperation.AddParameter("@userunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintUserunkid.ToString)
            objDataOperation.AddParameter("@isvoid", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsvoid.ToString)
            objDataOperation.AddParameter("@processmstunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintProcessmstunkid)
            objDataOperation.AddParameter("@isapproved", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsapproved.ToString)
            objDataOperation.AddParameter("@isdisapproved", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsdisapproved.ToString)
            objDataOperation.AddParameter("@remark", SqlDbType.NVarChar, mstrRemark.Length, mstrRemark)
            If mdtApprovaldate <> Nothing Then
                objDataOperation.AddParameter("@approvaldate", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, mdtApprovaldate)
            Else
                objDataOperation.AddParameter("@approvaldate", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, DBNull.Value)
            End If

            strQ = "update  " & mstrDatabaseName & "..tlscreening_process_master set " & _
              "  cycleunkid = @cycleunkid " & _
              ", employeeunkid = @employeeunkid " & _
              ", stageunkid = @stageunkid " & _
              ", userunkid = @userunkid " & _
              ", isvoid = @isvoid " & _
              ", isapproved = @isapproved " & _
              ", isdisapproved = @isdisapproved " & _
              ", remark = @remark " & _
              ", approvaldate = @approvaldate " & _
              " where processmstunkid = @processmstunkid and ismatch = 1 "

            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            objtlscreening_stages_tran._Cycleunkid = mintCycleunkid
            objtlscreening_stages_tran._Employeeunkid = mintEmployeeunkid
            objtlscreening_stages_tran._Stageunkid = mintStageunkid
            objtlscreening_stages_tran._Isvoid = False
            objtlscreening_stages_tran._AuditUserId = mintAuditUserId
            objtlscreening_stages_tran._HostName = mstrHostName
            objtlscreening_stages_tran._ClientIP = mstrClientIP
            objtlscreening_stages_tran._FromWeb = mblnIsWeb
            objtlscreening_stages_tran._DatabaseName = mstrDatabaseName
            objtlscreening_stages_tran._Processmstunkid = mintProcessmstunkid
            objtlscreening_stages_tran._Screenermstunkid = mintScreenermstunkid
            objtlscreening_stages_tran._FormName = mstrFormName

            If objtlscreening_stages_tran.SaveStageTran(objDataOperation) = False Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            objtlscreening_process_tran._Processmstunkid = mintProcessmstunkid
            objtlscreening_process_tran._Screenermstunkid = mintScreenermstunkid
            objtlscreening_process_tran._Isvoid = False
            objtlscreening_process_tran._AuditUserId = mintAuditUserId
            objtlscreening_process_tran._HostName = mstrHostName
            objtlscreening_process_tran._ClientIP = mstrClientIP
            objtlscreening_process_tran._FromWeb = mblnIsWeb
            objtlscreening_process_tran._DatabaseName = mstrDatabaseName
            objtlscreening_process_tran._FormName = mstrFormName

            For Each drow As DataRow In mdtQuestionnaire.Rows
                objtlscreening_process_tran._Questionnaireunkid = CInt(drow("questionnaireunkid").ToString())
                objtlscreening_process_tran._Result = CDbl(drow("result"))
                objtlscreening_process_tran._Remark = drow("remark").ToString()

                If objtlscreening_process_tran.SaveProcessTran(objDataOperation) = False Then
                    exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                    Throw exForce
                End If
            Next

            If InsertAuditTrails(objDataOperation, enAuditType.EDIT, mintProcessmstunkid.ToString()) = False Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            If xDataOpr Is Nothing Then objDataOperation.ReleaseTransaction(True)

            Return True
        Catch ex As Exception
            If xDataOpr Is Nothing Then objDataOperation.ReleaseTransaction(False)
            Throw New Exception(ex.Message & "; Procedure Name: Update; Module Name: " & mstrModuleName)
            Return False
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            If xDataOpr Is Nothing Then objDataOperation = Nothing
            objtlscreening_process_tran = Nothing
            objtlscreening_stages_tran = Nothing

        End Try
    End Function

    Public Function Delete(ByVal strProcessmstUnkid As String, Optional ByVal xDataOpr As clsDataOperation = Nothing) As Boolean
        'If isUsed(intUnkid) Then
        '    mstrMessage = "<Message>"
        '    Return False
        'End If

        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        Dim objtlscreening_process_tran As New clstlscreening_process_tran
        Dim objtlscreening_stages_tran As New clstlscreening_stages_tran

        Dim objDataOperation As clsDataOperation
        If xDataOpr Is Nothing Then
            objDataOperation = New clsDataOperation
            objDataOperation.BindTransaction()
        Else
            objDataOperation = xDataOpr
        End If
        objDataOperation.ClearParameters()

        Try
            strQ = "UPDATE " & mstrDatabaseName & "..tlscreening_process_master SET " & _
                   "  isvoid = @isvoid" & _
                   ", voiduserunkid = @voiduserunkid" & _
                   ", voiddatetime = @voiddatetime" & _
                   ", voidreason = @voidreason " & _
                   "WHERE processmstunkid in ( " & strProcessmstUnkid & " ) "

            objDataOperation.AddParameter("@isvoid", SqlDbType.Bit, eZeeDataType.BIT_SIZE, True)
            objDataOperation.AddParameter("@voiduserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintVoiduserunkid.ToString)
            objDataOperation.AddParameter("@voidreason", SqlDbType.NVarChar, eZeeDataType.DESC_SIZE, mstrVoidreason.ToString)
            If mdtVoiddatetime <> Nothing Then
                objDataOperation.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, mdtVoiddatetime)
            Else
                objDataOperation.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, DBNull.Value)
            End If

            Call objDataOperation.ExecNonQuery(strQ)

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            'Remove Screener Tran Detail
            objtlscreening_stages_tran._Isvoid = True
            objtlscreening_stages_tran._Voidreason = mstrVoidreason
            objtlscreening_stages_tran._Voiduserunkid = mintVoiduserunkid
            objtlscreening_stages_tran._Voiddatetime = mdtVoiddatetime

            objtlscreening_stages_tran._HostName = mstrHostName
            objtlscreening_stages_tran._ClientIP = mstrClientIP
            objtlscreening_stages_tran._FormName = mstrFormName
            objtlscreening_stages_tran._AuditUserId = mintAuditUserId
            objtlscreening_stages_tran._FromWeb = mblnIsWeb


            If objtlscreening_stages_tran.Delete(strProcessmstUnkid, objDataOperation) = False Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            'Remove All Question
            objtlscreening_process_tran._Isvoid = True
            objtlscreening_process_tran._Voidreason = mstrVoidreason
            objtlscreening_process_tran._Voiduserunkid = mintVoiduserunkid
            objtlscreening_process_tran._Voiddatetime = mdtVoiddatetime

            objtlscreening_process_tran._HostName = mstrHostName
            objtlscreening_process_tran._ClientIP = mstrClientIP
            objtlscreening_process_tran._FormName = mstrFormName
            objtlscreening_process_tran._AuditUserId = mintAuditUserId
            objtlscreening_process_tran._FromWeb = mblnIsWeb

            If objtlscreening_process_tran.Delete(strProcessmstUnkid, objDataOperation) = False Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            If InsertAuditTrails(objDataOperation, enAuditType.DELETE, strProcessmstUnkid) = False Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            If xDataOpr Is Nothing Then objDataOperation.ReleaseTransaction(True)

            Return True
        Catch ex As Exception
            If xDataOpr Is Nothing Then objDataOperation.ReleaseTransaction(False)
            Throw New Exception(ex.Message & "; Procedure Name: Delete; Module Name: " & mstrModuleName)
            Return False
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            If xDataOpr Is Nothing Then objDataOperation = Nothing
            objtlscreening_process_tran = Nothing
            objtlscreening_stages_tran = Nothing
        End Try
    End Function

    Public Function isUsed(ByVal intUnkid As Integer) As Boolean
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        Dim objDataOperation As clsDataOperation
        objDataOperation = New clsDataOperation

        Try
            strQ = "<Query>"

            objDataOperation.AddParameter("@processmstunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intUnkid)

            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            Return dsList.Tables(0).Rows.Count > 0
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: isUsed; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
    End Function

    Public Function isExist(ByVal intCycleId As Integer, _
                            ByVal intEmployeeId As Integer, _
                            Optional ByRef ProcessMstunkid As Integer = -1, _
                            Optional ByVal xDataOpr As clsDataOperation = Nothing) As Boolean
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception


        If xDataOpr Is Nothing Then
            objDataOperation = New clsDataOperation
        Else
            objDataOperation = xDataOpr
        End If
        objDataOperation.ClearParameters()

        Try
            strQ = "SELECT " & _
              "  processmstunkid " & _
              ", cycleunkid " & _
              ", employeeunkid " & _
              ", stageunkid " & _
              ", userunkid " & _
              ", isvoid " & _
              ", voiduserunkid " & _
              ", voiddatetime " & _
              ", voidreason " & _
             "FROM " & mstrDatabaseName & "..tlscreening_process_master " & _
             "WHERE isvoid = 0 " & _
            " AND cycleunkid  = @cycleunkid " & _
            " AND employeeunkid = @employeeunkid "

            objDataOperation.AddParameter("@cycleunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intCycleId)
            objDataOperation.AddParameter("@employeeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intEmployeeId)

            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            If dsList.Tables(0).Rows.Count > 0 Then
                ProcessMstunkid = CInt(dsList.Tables(0).Rows(0)("processmstunkid"))
            End If


            Return dsList.Tables(0).Rows.Count > 0
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: isExist; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            If xDataOpr Is Nothing Then objDataOperation = Nothing
        End Try
    End Function

    Public Function SetApproveDisApprove(ByVal intEmpId As Integer, ByVal intCycleid As Integer, ByVal intProcessMstId As Integer, ByVal isApproved As Boolean, Optional ByVal xDataOpr As clsDataOperation = Nothing) As Boolean

        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        If xDataOpr Is Nothing Then
            objDataOperation = New clsDataOperation
            objDataOperation.BindTransaction()
        Else
            objDataOperation = xDataOpr
        End If
        objDataOperation.ClearParameters()

        Try
            objDataOperation.AddParameter("@cycleunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intCycleid.ToString)
            objDataOperation.AddParameter("@employeeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intEmpId.ToString)
            objDataOperation.AddParameter("@processmstunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intProcessMstId)
            objDataOperation.AddParameter("@userunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintUserunkid.ToString)
            objDataOperation.AddParameter("@remark", SqlDbType.NVarChar, mstrRemark.Length, mstrRemark)

            If isApproved Then
                objDataOperation.AddParameter("@isapproved", SqlDbType.Bit, eZeeDataType.BIT_SIZE, True)
                objDataOperation.AddParameter("@isdisapproved", SqlDbType.Bit, eZeeDataType.BIT_SIZE, False)
            Else
                objDataOperation.AddParameter("@isapproved", SqlDbType.Bit, eZeeDataType.BIT_SIZE, False)
                objDataOperation.AddParameter("@isdisapproved", SqlDbType.Bit, eZeeDataType.BIT_SIZE, True)
            End If


            strQ = "update  " & mstrDatabaseName & "..tlscreening_process_master set " & _
              "  userunkid = @userunkid " & _
              ", isapproved = @isapproved " & _
              ", isdisapproved = @isdisapproved " & _
              ", remark = @remark " & _
              ", approvaldate = getDate() " & _
              "  Where processmstunkid = @processmstunkid and employeeunkid = @employeeunkid and cycleunkid = @cycleunkid "

            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            If InsertAuditTrails(objDataOperation, enAuditType.EDIT, intProcessMstId.ToString()) = False Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            If xDataOpr Is Nothing Then objDataOperation.ReleaseTransaction(True)

            Return True
        Catch ex As Exception
            If xDataOpr Is Nothing Then objDataOperation.ReleaseTransaction(False)
            Throw New Exception(ex.Message & "; Procedure Name: SetApproveDisApprove; Module Name: " & mstrModuleName)
            Return False
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            If xDataOpr Is Nothing Then objDataOperation = Nothing
        End Try
    End Function

    Public Function SetBackToQulify(ByVal intCycleid As Integer, ByVal strProcessMstId As String, ByVal strRemark As String, Optional ByVal xDataOpr As clsDataOperation = Nothing) As Boolean

        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception
        Dim objDepartmentaltrainingneed_master As New clsDepartmentaltrainingneed_master



        If xDataOpr Is Nothing Then
            objDataOperation = New clsDataOperation
            objDataOperation.BindTransaction()
        Else
            objDataOperation = xDataOpr
        End If
        objDataOperation.ClearParameters()

        Try
            objDataOperation.AddParameter("@cycleunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intCycleid.ToString)
            objDataOperation.AddParameter("@userunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintUserunkid.ToString)
            objDataOperation.AddParameter("@remark", SqlDbType.NVarChar, mstrRemark.Length, mstrRemark)
            objDataOperation.AddParameter("@isapproved", SqlDbType.Bit, eZeeDataType.BIT_SIZE, False)
            objDataOperation.AddParameter("@isdisapproved", SqlDbType.Bit, eZeeDataType.BIT_SIZE, False)


            strQ = "update  " & mstrDatabaseName & "..tlscreening_process_master set " & _
              "  userunkid = @userunkid " & _
              ", isapproved = @isapproved " & _
              ", isdisapproved = @isdisapproved " & _
              ", remark = @remark " & _
              ", approvaldate = getDate() " & _
              "  Where processmstunkid in ( " & strProcessMstId & " ) and cycleunkid = @cycleunkid "

            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            If InsertAuditTrails(objDataOperation, enAuditType.EDIT, strProcessMstId.ToString()) = False Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            If ChangeDepartmentTrainingCategory(strProcessMstId, objDataOperation) = False Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            If xDataOpr Is Nothing Then objDataOperation.ReleaseTransaction(True)

            Return True
        Catch ex As Exception
            If xDataOpr Is Nothing Then objDataOperation.ReleaseTransaction(False)
            Throw New Exception(ex.Message & "; Procedure Name: ChangeDepartmentTrainingCategory; Module Name: " & mstrModuleName)
            Return False
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            If xDataOpr Is Nothing Then objDataOperation = Nothing
        End Try
    End Function


    Public Function ChangeDepartmentTrainingCategory(ByVal strProcessMstId As String, Optional ByVal xDataOpr As clsDataOperation = Nothing) As Boolean

        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception
        Dim objDepartmentaltrainingneed_master As New clsDepartmentaltrainingneed_master

        If xDataOpr Is Nothing Then
            objDataOperation = New clsDataOperation
            objDataOperation.BindTransaction()
        Else
            objDataOperation = xDataOpr
        End If
        objDataOperation.ClearParameters()

        Try

            _Processmstunkid() = strProcessMstId
            If mintEmployeeunkid > 0 Then
                Dim dsDepartmentalTrainingneedList As DataSet = Nothing
                dsDepartmentalTrainingneedList = objDepartmentaltrainingneed_master.GetTrainingNeedListForOtherModule(-1, -1, CInt(enModuleReference.PDP), CInt(clsTraining_Category_Master.enTrainingCategoryDefaultId.Talent_PDP), mintEmployeeunkid, False, objDataOperation)
                If IsNothing(dsDepartmentalTrainingneedList) = False AndAlso dsDepartmentalTrainingneedList.Tables(0).Rows.Count > 0 Then
                    For Each drrow As DataRow In dsDepartmentalTrainingneedList.Tables(0).Rows
                        objDepartmentaltrainingneed_master._Departmentaltrainingneedunkid = CInt(drrow("departmentaltrainingneedunkid"))

                        objDepartmentaltrainingneed_master._HostName = mstrHostName
                        objDepartmentaltrainingneed_master._ClientIP = mstrClientIP
                        objDepartmentaltrainingneed_master._FormName = mstrModuleName
                        objDepartmentaltrainingneed_master._AuditUserId = mintUserunkid
                        objDepartmentaltrainingneed_master._AuditDate = DateTime.Now
                        objDepartmentaltrainingneed_master._Isweb = mblnIsWeb
                        objDepartmentaltrainingneed_master._Userunkid = mintUserunkid
                        objDepartmentaltrainingneed_master._Loginemployeeunkid = -1

                        If objDepartmentaltrainingneed_master._Statusunkid = clsDepartmentaltrainingneed_master.enApprovalStatus.Pending Then
                            objDepartmentaltrainingneed_master._Trainingcategoryunkid = CInt(clsTraining_Category_Master.enTrainingCategoryDefaultId.Individual_Development_Plan)
                            If objDepartmentaltrainingneed_master.Update(objDataOperation) = False Then
                                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                                Throw exForce
                            End If
                        End If
                    Next
                End If
                dsDepartmentalTrainingneedList.Dispose()
            End If

            If xDataOpr Is Nothing Then objDataOperation.ReleaseTransaction(True)

            Return True
        Catch ex As Exception
            If xDataOpr Is Nothing Then objDataOperation.ReleaseTransaction(False)
            Throw New Exception(ex.Message & "; Procedure Name: SetBackToQulify; Module Name: " & mstrModuleName)
            Return False
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            If xDataOpr Is Nothing Then objDataOperation = Nothing
        End Try
    End Function


    Public Function InsertAuditTrails(ByVal objDataOperation As clsDataOperation, ByVal eAuditType As enAuditType, ByVal strProcessmstUnkid As String) As Boolean
        Dim StrQ As String = ""
        Try
            StrQ = "INSERT INTO attlscreening_process_master ( " & _
                    "  tranguid " & _
                    ", processmstunkid " & _
                    ", cycleunkid " & _
                    ", employeeunkid " & _
                    ", stageunkid " & _
                    ", isapproved " & _
                    ", isdisapproved " & _
                    ", remark " & _
                    ", ismatch " & _
                    ", approvaldate " & _
                    ", userunkid " & _
                    ", audittypeid " & _
                    ", audtuserunkid " & _
                    ", auditdatetime " & _
                    ", formname " & _
                    ", ip " & _
                    ", host " & _
                    ", isweb )" & _
                    " SELECT " & _
                    "  LOWER(NEWID()) " & _
                    ", processmstunkid " & _
                    ", cycleunkid " & _
                    ", employeeunkid " & _
                    ", stageunkid " & _
                    ", isapproved " & _
                    ", isdisapproved " & _
                    ", remark " & _
                    ", ismatch " & _
                    ", approvaldate " & _
                    ", @userunkid " & _
                    ", @audittypeid " & _
                    ", @audtuserunkid " & _
                    ", GETDATE() " & _
                    ", @formname " & _
                    ", @ip " & _
                    ", @host " & _
                    ", @isweb" & _
                    " From tlscreening_process_master where processmstunkid in ( " & strProcessmstUnkid & " ) "

            objDataOperation.ClearParameters()
            objDataOperation.AddParameter("@userunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintUserunkid.ToString)
            objDataOperation.AddParameter("@audittypeid", SqlDbType.Int, eZeeDataType.INT_SIZE, eAuditType)
            objDataOperation.AddParameter("@audtuserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintAuditUserId)
            objDataOperation.AddParameter("@formname", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrFormName)
            objDataOperation.AddParameter("@ip", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrClientIP)
            objDataOperation.AddParameter("@host", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrHostName)
            objDataOperation.AddParameter("@isweb", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsWeb)

            objDataOperation.ExecNonQuery(StrQ)

            If objDataOperation.ErrorMessage <> "" Then
                Throw New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
            End If

            Return True
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: InsertAuditTrails; Module Name: " & mstrModuleName)
        Finally
        End Try
    End Function

    Public Function IsTalentStartedForCycle(ByVal intCycleId As Integer, _
                                            Optional ByVal xDataOpr As clsDataOperation = Nothing) As Boolean
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception


        If xDataOpr Is Nothing Then
            objDataOperation = New clsDataOperation
        Else
            objDataOperation = xDataOpr
        End If
        objDataOperation.ClearParameters()

        Try
            strQ = "SELECT * " & _
                   " FROM " & mstrDatabaseName & "..tlscreening_process_master where " & _
                   "  cycleunkid = @cycleunkid  " & _
                   "   and isvoid= 0 "

            objDataOperation.AddParameter("@cycleunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intCycleId)
            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            If dsList.Tables(0).Rows.Count > 0 Then
                Return True
            Else
                Return False
            End If

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: IsTalentStartedForCycle; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            If xDataOpr Is Nothing Then objDataOperation = Nothing
        End Try

    End Function

    Public Function DeleteScreenerTranDetails(ByVal strProcessmstUnkid As String, ByVal intScreenerMstUnkId As Integer, Optional ByVal xDataOpr As clsDataOperation = Nothing) As Boolean
        'If isUsed(intUnkid) Then
        '    mstrMessage = "<Message>"
        '    Return False
        'End If

        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        Dim objtlscreening_process_tran As New clstlscreening_process_tran
        Dim objtlscreening_stages_tran As New clstlscreening_stages_tran

        Dim objDataOperation As clsDataOperation
        If xDataOpr Is Nothing Then
            objDataOperation = New clsDataOperation
            objDataOperation.BindTransaction()
        Else
            objDataOperation = xDataOpr
        End If
        objDataOperation.ClearParameters()

        Try
            'Remove Screener Tran Detail
            objtlscreening_stages_tran._Isvoid = True
            objtlscreening_stages_tran._Voidreason = mstrVoidreason
            objtlscreening_stages_tran._Voiduserunkid = mintVoiduserunkid
            objtlscreening_stages_tran._Voiddatetime = mdtVoiddatetime

            objtlscreening_stages_tran._HostName = mstrHostName
            objtlscreening_stages_tran._ClientIP = mstrClientIP
            objtlscreening_stages_tran._FormName = mstrFormName
            objtlscreening_stages_tran._AuditUserId = mintAuditUserId
            objtlscreening_stages_tran._FromWeb = mblnIsWeb


            If objtlscreening_stages_tran.Delete(strProcessmstUnkid, objDataOperation, intScreenerMstUnkId) = False Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            'Remove All Question
            objtlscreening_process_tran._Isvoid = True
            objtlscreening_process_tran._Voidreason = mstrVoidreason
            objtlscreening_process_tran._Voiduserunkid = mintVoiduserunkid
            objtlscreening_process_tran._Voiddatetime = mdtVoiddatetime

            objtlscreening_process_tran._HostName = mstrHostName
            objtlscreening_process_tran._ClientIP = mstrClientIP
            objtlscreening_process_tran._FormName = mstrFormName
            objtlscreening_process_tran._AuditUserId = mintAuditUserId
            objtlscreening_process_tran._FromWeb = mblnIsWeb

            If objtlscreening_process_tran.Delete(strProcessmstUnkid, objDataOperation, intScreenerMstUnkId) = False Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            If xDataOpr Is Nothing Then objDataOperation.ReleaseTransaction(True)

            Return True
        Catch ex As Exception
            If xDataOpr Is Nothing Then objDataOperation.ReleaseTransaction(False)
            Throw New Exception(ex.Message & "; Procedure Name: DeleteScreenerTranDetails; Module Name: " & mstrModuleName)
            Return False
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            If xDataOpr Is Nothing Then objDataOperation = Nothing
            objtlscreening_process_tran = Nothing
            objtlscreening_stages_tran = Nothing
        End Try
    End Function

    Public Function IsTalentStartedForEmployees(ByVal intCycleId As Integer, _
                                                ByVal strEmployeeList As String, _
                                                Optional ByVal xDataOpr As clsDataOperation = Nothing) As Boolean
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception


        If xDataOpr Is Nothing Then
            objDataOperation = New clsDataOperation
        Else
            objDataOperation = xDataOpr
        End If
        objDataOperation.ClearParameters()

        Try
            strQ = "SELECT * " & _
                   " FROM " & mstrDatabaseName & "..tlscreening_process_master where " & _
                   "    isvoid= 0 " & _
                   "  and cycleunkid = @cycleunkid  " & _
                   "  and employeeunkid in (" & strEmployeeList & " ) "


            objDataOperation.AddParameter("@cycleunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intCycleId)
            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            If dsList.Tables(0).Rows.Count > 0 Then
                Return True
            Else
                Return False
            End If

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: IsTalentStartedForCycle; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            If xDataOpr Is Nothing Then objDataOperation = Nothing
        End Try

    End Function

    Public Function UpdateIsMatch(ByVal strProcessmstunkids As String, ByVal blnstatus As Boolean, Optional ByVal xDataOpr As clsDataOperation = Nothing) As Boolean

        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception
        Dim objtlscreening_process_tran As New clstlscreening_process_tran
        Dim objtlscreening_stages_tran As New clstlscreening_stages_tran

        If xDataOpr Is Nothing Then
            objDataOperation = New clsDataOperation
            objDataOperation.BindTransaction()
        Else
            objDataOperation = xDataOpr
        End If
        objDataOperation.ClearParameters()

        Try
            For Each intProcessunkid As Integer In strProcessmstunkids.Split(CChar(","))
                _Processmstunkid() = intProcessunkid


                If blnstatus = False AndAlso mblnIsmatch = False Then
                    Continue For
                End If


                objDataOperation.ClearParameters()
                objDataOperation.AddParameter("@processmstunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intProcessunkid)

                strQ = "update " & mstrDatabaseName & "..tlscreening_process_master set "

                If blnstatus Then
                    strQ &= " ismatch = 1 "
                Else
                    strQ &= " ismatch = 0 "
                End If

                strQ &= " where processmstunkid = @processmstunkid "

                dsList = objDataOperation.ExecQuery(strQ, "List")

                If objDataOperation.ErrorMessage <> "" Then
                    exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                    Throw exForce
                End If

                If InsertAuditTrails(objDataOperation, enAuditType.EDIT, intProcessunkid) = False Then
                    exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                    Throw exForce
                End If


            Next


            If xDataOpr Is Nothing Then objDataOperation.ReleaseTransaction(True)

            Return True
        Catch ex As Exception
            If xDataOpr Is Nothing Then objDataOperation.ReleaseTransaction(False)
            Throw New Exception(ex.Message & "; Procedure Name: Update; Module Name: " & mstrModuleName)
            Return False
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            If xDataOpr Is Nothing Then objDataOperation = Nothing
            objtlscreening_process_tran = Nothing
            objtlscreening_stages_tran = Nothing

        End Try
    End Function

    Public Function GetTalentProcessedDataAfterSettingChage(ByVal xDatabaseName As String, _
                                     ByVal xUserUnkid As Integer, _
                                     ByVal xYearUnkid As Integer, _
                                     ByVal xCompanyUnkid As Integer, _
                                     ByVal xPeriodStart As DateTime, _
                                     ByVal xPeriodEnd As DateTime, _
                                     ByVal xAsOnDate As Date, _
                                     ByVal xCycleUnkid As Integer, _
                                     Optional ByVal strListName As String = "List", _
                                     Optional ByVal xDataOpr As clsDataOperation = Nothing _
                                    ) As DataSet

        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception


        Dim objtlsetting As New clstlsettings_master
        Dim objEmpDates As New clsemployee_dates_tran
        Dim objPotentialTalentTran As New clsPotentialTalent_Tran
        Dim objtlpipeline_master As New clstlpipeline_master
        Dim mdicSetting As New Dictionary(Of clstlsettings_master.enTalentConfiguration, String)


        If xDataOpr Is Nothing Then
            objDataOperation = New clsDataOperation
        Else
            objDataOperation = xDataOpr
        End If
        objDataOperation.ClearParameters()


        Try
            mdicSetting = objtlsetting.GetSettingFromPeriod(xCycleUnkid)

            If IsNothing(mdicSetting) Then
                mstrMessage = Language.getMessage(mstrModuleName, 1, "Sorry,Setting Not Available For this Cycle.")
                Return Nothing
            End If


            Dim strDBName As String = ""
            Dim currDB As String = ""
            If System.Web.HttpContext.Current IsNot Nothing Then
                currDB = System.Web.HttpContext.Current.Session("Database_Name").ToString
            Else
                currDB = eZeeDatabase.current_database()
            End If
            If currDB <> xDatabaseName Then
                strDBName = " " & xDatabaseName & ".."
            End If

            objDataOperation.ClearParameters()

            strQ &= "DECLARE @UserId AS INT;SET @UserId = @Uid "
            strQ += "SELECT " & _
                      "A.* " & _
                      ",ISNULL(Emp_suspension.suspensiondays, 0) AS suspensiondays " & _
                    "FROM " & _
                    "( " & _
                    "SELECT " & _
                    " hremployee_master.employeeunkid " & _
                    ",  ISNULL(hremployee_master.firstname,'')+' '+ISNULL(hremployee_master.othername,'')+' '+ISNULL(hremployee_master.surname,'') AS employeename " & _
                    ",  (CAST(CONVERT(CHAR(8), @Date ,112) AS INT)-CAST(CONVERT(CHAR(8),birthdate,112) AS INT))/10000 AS age " & _
                    ",  0 AS exyr" & _
                    ", CONVERT(CHAR(8), hremployee_master.appointeddate, 112) AS appointeddate " & _
                    ", ISNULL(CONVERT(CHAR(8), TRM.LEAVING, 112), '') AS termination_from_date " & _
                    ", ISNULL(CONVERT(CHAR(8), RET.RETIRE, 112), '') AS termination_to_date " & _
                    ", ISNULL(CONVERT(CHAR(8), TRM.EOC, 112), '') AS empl_enddate " & _
                    ", tlscreening_process_master.processmstunkid " & _
                    ", tlscreening_process_master.ismatch " & _
                    ", CAST(0 as BIT) as ismanual " & _
                    "  FROM " & strDBName & "..tlscreening_process_master left join hremployee_master on hremployee_master.employeeunkid = tlscreening_process_master.employeeunkid "


            strQ &= "LEFT JOIN (SELECT " & _
                         " T.EmpId " & _
                         ",T.EOC " & _
                         ",T.LEAVING " & _
                         ",T.isexclude_payroll AS IsExPayroll " & _
                      "FROM (SELECT " & _
                                "employeeunkid AS EmpId " & _
                              ",date1 AS EOC " & _
                              ",date2 AS LEAVING " & _
                              ",effectivedate " & _
                              ",isexclude_payroll " & _
                              ",ROW_NUMBER() OVER (PARTITION BY employeeunkid ORDER BY effectivedate DESC) AS xNo " & _
                           "FROM  " & strDBName & "..hremployee_dates_tran " & _
                           "WHERE datetypeunkid IN ('" & enEmp_Dates_Transaction.DT_TERMINATION & "') " & _
                           "AND isvoid = 0 " & _
                           "AND CONVERT(CHAR(8), effectivedate, 112) <= '" & eZeeDate.convertDate(xPeriodEnd).ToString & "') AS T " & _
                      "WHERE T.xNo = 1) AS TRM " & _
                      "ON TRM.EmpId = hremployee_master.employeeunkid " & _
                 "LEFT JOIN (SELECT " & _
                           "R.EmpId " & _
                         ",R.RETIRE " & _
                         ",R.isexclude_payroll AS IsExPayroll " & _
                      "FROM (SELECT " & _
                                "employeeunkid AS EmpId " & _
                              ",date1 AS RETIRE " & _
                              ",effectivedate " & _
                              ",isexclude_payroll " & _
                              ",ROW_NUMBER() OVER (PARTITION BY employeeunkid ORDER BY effectivedate DESC) AS xNo " & _
                           "FROM " & strDBName & "..hremployee_dates_tran " & _
                           "WHERE datetypeunkid IN ('" & enEmp_Dates_Transaction.DT_RETIREMENT & "') " & _
                           "AND isvoid = 0 " & _
                           "AND CONVERT(CHAR(8), effectivedate, 112) <= '" & eZeeDate.convertDate(xPeriodEnd).ToString & "') AS R " & _
                      "WHERE R.xNo = 1) AS RET " & _
                      "ON RET.EmpId = hremployee_master.employeeunkid " & _
                 "LEFT JOIN (SELECT " & _
                           "RH.EmpId " & _
                         ",RH.REHIRE " & _
                      "FROM (SELECT " & _
                                "employeeunkid AS EmpId " & _
                              ",reinstatment_date AS REHIRE " & _
                              ",effectivedate " & _
                              ",ROW_NUMBER() OVER (PARTITION BY employeeunkid ORDER BY effectivedate DESC) AS xNo " & _
                           "FROM " & strDBName & "..hremployee_rehire_tran " & _
                           "WHERE isvoid = 0 " & _
                           "AND CONVERT(CHAR(8), effectivedate, 112) <= '" & eZeeDate.convertDate(xPeriodEnd).ToString & "') AS RH " & _
                      "WHERE RH.xNo = 1) AS HIRE " & _
                      "ON HIRE.EmpId = hremployee_master.employeeunkid "


            '********************** Talent Seeting Conditions ************************' --- START

            strQ &= "JOIN (SELECT " & _
                    "emp_transfer.employeeunkid " & _
                    ",stationunkid " & _
                    ",deptgroupunkid " & _
                    ",departmentunkid " & _
                    ",sectiongroupunkid " & _
                    ",sectionunkid " & _
                    ",unitgroupunkid " & _
                    ",unitunkid " & _
                    ",teamunkid " & _
                    ",classgroupunkid " & _
                    ",classunkid " & _
                    ",emp_categorization.jobgroupunkid " & _
                    ",emp_categorization.jobunkid " & _
                    ",Emp_CCT.costcenterunkid " & _
                 "FROM (SELECT " & _
                           "Emp_TT.employeeunkid AS TrfEmpId " & _
                         ",ISNULL(Emp_TT.stationunkid, 0) AS stationunkid " & _
                         ",ISNULL(Emp_TT.deptgroupunkid, 0) AS deptgroupunkid " & _
                         ",ISNULL(Emp_TT.departmentunkid, 0) AS departmentunkid " & _
                         ",ISNULL(Emp_TT.sectiongroupunkid, 0) AS sectiongroupunkid " & _
                         ",ISNULL(Emp_TT.sectionunkid, 0) AS sectionunkid " & _
                         ",ISNULL(Emp_TT.unitgroupunkid, 0) AS unitgroupunkid " & _
                         ",ISNULL(Emp_TT.unitunkid, 0) AS unitunkid " & _
                         ",ISNULL(Emp_TT.teamunkid, 0) AS teamunkid " & _
                         ",ISNULL(Emp_TT.classgroupunkid, 0) AS classgroupunkid " & _
                         ",ISNULL(Emp_TT.classunkid, 0) AS classunkid " & _
                         ",CONVERT(CHAR(8), Emp_TT.effectivedate, 112) AS EfDt " & _
                         ",Emp_TT.employeeunkid " & _
                         ",ROW_NUMBER() OVER (PARTITION BY Emp_TT.employeeunkid ORDER BY Emp_TT.effectivedate DESC) AS Rno " & _
                      "FROM  " & strDBName & "hremployee_transfer_tran AS Emp_TT " & _
                      "WHERE isvoid = 0 " & _
                      "AND CONVERT(CHAR(8), effectivedate, 112) <= " & eZeeDate.convertDate(xPeriodEnd) & " ) AS emp_transfer " & _
                 "JOIN (SELECT " & _
                           "Emp_CT.employeeunkid AS CatEmpId " & _
                         ",Emp_CT.jobgroupunkid " & _
                         ",Emp_CT.jobunkid " & _
                         ",CONVERT(CHAR(8), Emp_CT.effectivedate, 112) AS CEfDt " & _
                         ",Emp_CT.employeeunkid " & _
                         ",ROW_NUMBER() OVER (PARTITION BY Emp_CT.employeeunkid ORDER BY Emp_CT.effectivedate DESC) AS Rno " & _
                      "FROM  " & strDBName & "hremployee_categorization_tran AS Emp_CT " & _
                      "WHERE isvoid = 0 " & _
                      "AND CONVERT(CHAR(8), effectivedate, 112) <= " & eZeeDate.convertDate(xPeriodEnd) & ") AS emp_categorization " & _
                      "ON emp_transfer.employeeunkid = emp_categorization.employeeunkid " & _
                 "JOIN (SELECT " & _
                           "Emp_CCT.employeeunkid AS CCTEmpId " & _
                         ",Emp_CCT.cctranheadvalueid AS costcenterunkid " & _
                         ",CONVERT(CHAR(8), Emp_CCT.effectivedate, 112) AS CTEfDt " & _
                         ",ROW_NUMBER() OVER (PARTITION BY Emp_CCT.employeeunkid ORDER BY Emp_CCT.effectivedate DESC) AS Rno " & _
                      "FROM  " & strDBName & "hremployee_cctranhead_tran AS Emp_CCT " & _
                      "WHERE Emp_CCT.isvoid = 0 " & _
                      "AND Emp_CCT.istransactionhead = 0 " & _
                      "AND CONVERT(CHAR(8), Emp_CCT.effectivedate, 112) <= " & eZeeDate.convertDate(xPeriodEnd) & " ) AS Emp_CCT " & _
                      "ON emp_transfer.employeeunkid = Emp_CCT.CCTEmpId " & _
                 "WHERE emp_transfer.Rno = 1 " & _
                 "AND emp_categorization.Rno = 1 " & _
                 "AND emp_CCT.Rno = 1"


            If mdicSetting.ContainsKey(clstlsettings_master.enTalentConfiguration.ALLOC_TYPE) Then
                Dim allocation As String = mdicSetting(clstlsettings_master.enTalentConfiguration.ALLOC_TYPE)
                Dim allocationType As String() = allocation.Split("|")

                Select Case CInt(allocationType(0))

                    Case CInt(enAllocation.BRANCH)
                        strQ &= "AND emp_transfer.stationunkid IN "

                    Case CInt(enAllocation.DEPARTMENT_GROUP)
                        strQ &= "AND emp_transfer.deptgroupunkid IN "

                    Case CInt(enAllocation.DEPARTMENT)
                        strQ &= "AND emp_transfer.departmentunkid IN "

                    Case CInt(enAllocation.SECTION_GROUP)
                        strQ &= "AND emp_transfer.sectiongroupunkid IN "

                    Case CInt(enAllocation.SECTION)
                        strQ &= "AND emp_transfer.sectionunkid IN "

                    Case CInt(enAllocation.UNIT_GROUP)
                        strQ &= "AND emp_transfer.unitgroupunkid IN "

                    Case CInt(enAllocation.UNIT)
                        strQ &= "AND emp_transfer.unitunkid IN "

                    Case CInt(enAllocation.JOB_GROUP)
                        strQ &= "AND emp_categorization.jobgroupunkid IN "

                    Case CInt(enAllocation.JOBS)
                        strQ &= "AND emp_categorization.jobunkid IN "

                    Case CInt(enAllocation.CLASS_GROUP)
                        strQ &= "AND emp_transfer.classgroupunkid IN "

                    Case CInt(enAllocation.CLASSES)
                        strQ &= "AND emp_transfer.classunkid IN "

                    Case CInt(enAllocation.COST_CENTER)
                        strQ &= "AND emp_CCT.costcenterunkid IN  "

                End Select

                strQ &= " (" & allocationType(1) & " ) "

            End If

            strQ &= " ) AS allocation " & _
                 "ON allocation.employeeunkid = hremployee_master.employeeunkid " & _
            "LEFT JOIN hrjob_master ON allocation.jobunkid = hrjob_master.jobunkid " & _
            "LEFT JOIN hrdepartment_master ON hrdepartment_master.departmentunkid = allocation.departmentunkid "

            strQ &= " WHERE 1=1 "


            If mdicSetting.ContainsKey(clstlsettings_master.enTalentConfiguration.MAX_AGE_NO) Then
                strQ &= " and (CAST(CONVERT(CHAR(8), @Date ,112) AS DECIMAL)-CAST(CONVERT(CHAR(8),birthdate,112) AS DECIMAL))/10000 <= " & mdicSetting(clstlsettings_master.enTalentConfiguration.MAX_AGE_NO) & " "
            End If

            If mdicSetting.ContainsKey(clstlsettings_master.enTalentConfiguration.MAX_SCREENER) Then
                strQ &= " AND hremployee_master.employeeunkid not in (SELECT " & _
                        " employeeunkid " & _
                        " FROM tlscreening_stages_tran " & _
                        " WHERE isvoid = 0 " & _
                        " AND cycleunkid = " & xCycleUnkid & " " & _
                        " GROUP BY employeeunkid " & _
                        "	HAVING count(employeeunkid) >= " & mdicSetting(clstlsettings_master.enTalentConfiguration.MAX_SCREENER) & " ) "
            End If
            '================ Employee ID



            objDataOperation.AddParameter("@Date", SqlDbType.NVarChar, eZeeDataType.DATETIME_SIZE, eZeeDate.convertDate(xAsOnDate.AddDays(1)))


            strQ &= ") as A " & _
                    "LEFT JOIN (SELECT " & _
                          "sus.EmpId " & _
                        ",sus.suspensiondays " & _
                     "FROM (SELECT " & _
                               "employeeunkid AS EmpId " & _
                               ",SUM(DATEDIFF(DAY, date1,  " & _
                               " CASE " & _
                               "    WHEN CONVERT(CHAR(8), date2, 112) IS NULL THEN CAST(@Date AS DATETIME) " & _
                               "    ELSE CAST(CONVERT(CHAR(8), date2 + 1, 112)AS DATETIME) " & _
                               " End " & _
                                " )) AS suspensiondays " & _
                          "FROM  " & strDBName & "hremployee_dates_tran " & _
                          "WHERE hremployee_dates_tran.datetypeunkid IN (" & enEmp_Dates_Transaction.DT_SUSPENSION & ") " & _
                          "AND isvoid = 0 " & _
                          "AND CONVERT(CHAR(8), date2, 112) <= " & eZeeDate.convertDate(xPeriodEnd) & " " & _
                          "GROUP BY hremployee_dates_tran.employeeunkid) AS sus) AS Emp_suspension " & _
                     "ON Emp_suspension.EmpId = A.employeeunkid "

            strQ &= "LEFT JOIN " & _
                      "( " & _
                        "SELECT " & _
                           "tlscreening_process_master.employeeunkid " & _
                          ",tlscreening_process_master.processmstunkid " & _
                        "FROM tlscreener_master AS TSM " & _
                          "JOIN tlscreening_stages_tran ON TSM.cycleunkid = tlscreening_stages_tran.cycleunkid " & _
                            "AND TSM.screenermstunkid = tlscreening_stages_tran.screenermstunkid " & _
                          "JOIN tlscreening_process_master ON tlscreening_stages_tran.processmstunkid = tlscreening_process_master.processmstunkid " & _
                        "WHERE TSM.isvoid = 0 AND TSM.cycleunkid = @cycleunkid and tlscreening_process_master.isvoid = 0 and tlscreening_stages_tran.isvoid= 0 " & _
                          "AND TSM.mapuserunkid = @UserId " & _
                      ") AS D ON A.employeeunkid = D.employeeunkid "

            objDataOperation.AddParameter("@cycleunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, xCycleUnkid)
            objDataOperation.AddParameter("@Uid", SqlDbType.Int, eZeeDataType.INT_SIZE, xUserUnkid)


            dsList = objDataOperation.ExecQuery(strQ, strListName)

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            Dim ExpYearNo As String = String.Empty
            If mdicSetting.ContainsKey(clstlsettings_master.enTalentConfiguration.EXP_YEAR_NO) Then
                ExpYearNo = mdicSetting(clstlsettings_master.enTalentConfiguration.EXP_YEAR_NO)
            End If
            If ExpYearNo.Length = 0 Then ExpYearNo = "0"
            If CInt(ExpYearNo) > 0 Then

                Dim strEmpIDs As String = String.Join(",", (From p In dsList.Tables(0) Select (p.Item("employeeunkid").ToString)).ToArray())

                If strEmpIDs.Length > 0 Then


                    Dim dsService As DataSet = objEmpDates.GetEmployeeServiceDays(objDataOperation, xCompanyUnkid, xDatabaseName, strEmpIDs, xAsOnDate)

                    Dim dtService = New DataView(dsService.Tables(0), "fromdate <> '' ", "", DataViewRowState.CurrentRows).ToTable

                    Dim result = (From e In dsList.Tables(0).AsEnumerable() Group Join s In dtService.AsEnumerable() On e.Item("employeeunkid") Equals s.Item("employeeunkid") Into es = Group From s In es.DefaultIfEmpty() Order By e.Field(Of Integer)("employeeunkid") _
                                  Select New With { _
                                      Key .employeeunkid = e.Field(Of Integer)("employeeunkid") _
                                    , Key .employeename = e.Field(Of String)("employeename") _
                                    , Key .age = e.Field(Of Integer)("age") _
                                    , Key .exyr = e.Field(Of Integer)("exyr") _
                                    , Key .suspensiondays = e.Field(Of Integer)("suspensiondays") _
                                    , Key .appointeddate = e.Field(Of String)("appointeddate") _
                                    , Key .termination_from_date = e.Field(Of String)("termination_from_date") _
                                    , Key .termination_to_date = e.Field(Of String)("termination_to_date") _
                                    , Key .empl_enddate = e.Field(Of String)("empl_enddate") _
                                    , Key .processmstunkid = e.Field(Of Integer)("processmstunkid") _
                                    , Key .ismatch = e.Field(Of Boolean)("ismatch") _
                                    , Key .ismanual = e.Field(Of Boolean)("ismanual") _
                                    , Key .FromDate = If(s Is Nothing, "", s.Field(Of String)("fromdate")) _
                                    , Key .ToDate = If(s Is Nothing, "", s.Field(Of String)("todate")) _
                                    , Key .ServiceDays = If(s Is Nothing, 0, s.Field(Of Integer)("ServiceDays")) _
                                    })

                    Dim intPrevEmp As Integer = 0
                    Dim dblTotDays As Double = 0

                    For Each drow In result

                        Dim StartDate As Date = eZeeDate.convertDate(drow.appointeddate)
                        Dim EndDate As Date = xAsOnDate


                        Dim intEmpId As Integer = drow.employeeunkid
                        Dim intCount As Integer = dtService.Select("employeeunkid = " & intEmpId & " ").Length

                        If intCount <= 0 Then 'No Dates record found                       
                            If drow.termination_from_date.Trim <> "" Then
                                EndDate = IIf(eZeeDate.convertDate(drow.termination_from_date) < EndDate, eZeeDate.convertDate(drow.termination_from_date), EndDate)
                            End If
                            If drow.termination_to_date.Trim <> "" Then
                                EndDate = IIf(eZeeDate.convertDate(drow.termination_to_date) < EndDate, eZeeDate.convertDate(drow.termination_to_date), EndDate)
                            End If
                            If drow.empl_enddate.Trim <> "" Then
                                EndDate = IIf(eZeeDate.convertDate(drow.empl_enddate) < EndDate, eZeeDate.convertDate(drow.empl_enddate), EndDate)
                            End If
                            dblTotDays = CDbl(DateDiff(DateInterval.Day, StartDate, EndDate.AddDays(1)))
                        ElseIf intCount = 1 Then
                            dblTotDays = drow.ServiceDays
                        Else
                            dblTotDays = (From p In dtService Where (p.Field(Of Integer)("employeeunkid") = intEmpId) Select (CDbl(p.Item("ServiceDays")))).Sum()
                        End If

                        For Each drRow As DataRow In dsList.Tables(0).Select("employeeunkid = " & intEmpId & " ")
                            drRow.Item("exyr") = (dblTotDays - drow.suspensiondays) / 365
                        Next


                    Next

                    If mdicSetting.ContainsKey(clstlsettings_master.enTalentConfiguration.EXP_YEAR_NO) Then
                        dsList.Tables(0).DefaultView.RowFilter = "exyr >= " & mdicSetting(clstlsettings_master.enTalentConfiguration.EXP_YEAR_NO) & " "
                    End If

                End If
            End If

            Dim intPrdNo As Integer = 0
            If mdicSetting.ContainsKey(clstlsettings_master.enTalentConfiguration.ANY_PERIOD) Or mdicSetting.ContainsKey(clstlsettings_master.enTalentConfiguration.ALL_PERIOD) Then
                If mdicSetting.ContainsKey(clstlsettings_master.enTalentConfiguration.ANY_PERIOD) Then
                    If mdicSetting.ContainsKey(clstlsettings_master.enTalentConfiguration.MIN_PERF_NO) Then
                        intPrdNo = mdicSetting(clstlsettings_master.enTalentConfiguration.MIN_PERF_NO)
                    Else
                        intPrdNo = 1
                    End If
                ElseIf mdicSetting.ContainsKey(clstlsettings_master.enTalentConfiguration.ALL_PERIOD) Then
                    intPrdNo = 0
                End If
            End If
            If mdicSetting.ContainsKey(clstlsettings_master.enTalentConfiguration.PERF_SCORE) Then
                Dim dtPerformanceScoreEmployeeList As DataTable
                'dtPerformanceScoreEmployeeList = objtlpipeline_master.GetPerformanceScoreEmployeeList(xDatabaseName, mdicSetting(clstlsettings_master.enTalentConfiguration.MIN_PERF_NO), mdicSetting(clstlsettings_master.enTalentConfiguration.PERF_SCORE), xCompanyUnkid, objDataOperation)
                dtPerformanceScoreEmployeeList = objtlpipeline_master.GetPerformanceScoreEmployeeList(xDatabaseName, intPrdNo, mdicSetting(clstlsettings_master.enTalentConfiguration.PERF_SCORE), xCompanyUnkid, xYearUnkid, objDataOperation)
                If dtPerformanceScoreEmployeeList IsNot Nothing AndAlso dtPerformanceScoreEmployeeList.Rows.Count > 0 Then
                    Dim strPerformanceScoreEmpIds As String = String.Join(",", (From p In dtPerformanceScoreEmployeeList Select (p.Item("employeeunkid").ToString)).ToArray())
                    If strPerformanceScoreEmpIds.Length > 0 Then
                        dsList.Tables(0).DefaultView.RowFilter = "employeeunkid in ( " & strPerformanceScoreEmpIds & ")"
                        Dim dtPerformanceScore As DataTable = dsList.Tables(0).DefaultView.ToTable
                        dsList.Tables.Remove(dsList.Tables(0))
                        dsList.Tables.Add(dtPerformanceScore)
                    End If
                Else
                    dsList.Tables(0).Rows.Clear()
                End If
            End If


            '============================ Add Extra Potential Succession Employee(s) -- Start
            Dim dsPotentialSuccession As DataSet
            strQ = "SELECT " & _
                       "hremployee_master.employeeunkid AS employeeunkid " & _
                       ",ISNULL(hremployee_master.firstname, '') + ' ' + ISNULL(hremployee_master.othername, '') + ' ' + ISNULL(hremployee_master.surname, '') AS employeename " & _
                       ",(CAST(CONVERT(CHAR(8), '20210101', 112) AS INT) - CAST(CONVERT(CHAR(8), birthdate, 112) AS INT)) / 10000 AS age " & _
                       ",0 AS exyr " & _
                       ",CONVERT(CHAR(8), hremployee_master.appointeddate, 112) AS appointeddate " & _
                       ",'' AS termination_from_date " & _
                       ",'' AS termination_to_date " & _
                       ",'' AS empl_enddate " & _
                       ",tlscreening_process_master.processmstunkid " & _
                       ",CAST(1 as BIT) AS ismatch " & _
                       ",CAST(1 as BIT) AS ismanual " & _
                       ",0 AS suspensiondays " & _
                    "FROM  tlscreening_process_master " & _
                    "JOIN tlpotentialtalent_tran " & _
                         "ON tlpotentialtalent_tran.employeeunkid = tlscreening_process_master.employeeunkid " & _
                    "JOIN hremployee_master " & _
                         "ON tlpotentialtalent_tran.employeeunkid = hremployee_master.employeeunkid " & _
                    "WHERE 1 = 1 "

            If mdicSetting.ContainsKey(clstlsettings_master.enTalentConfiguration.MAX_SCREENER) Then
                strQ &= " AND hremployee_master.employeeunkid not in (SELECT " & _
                        " employeeunkid " & _
                        " FROM tlscreening_stages_tran " & _
                        " WHERE isvoid = 0 " & _
                        " AND cycleunkid = " & xCycleUnkid & " " & _
                        " GROUP BY employeeunkid " & _
                        " HAVING count(employeeunkid) >= " & mdicSetting(clstlsettings_master.enTalentConfiguration.MAX_SCREENER) & " ) "
            End If

            objDataOperation.ClearParameters()
            dsPotentialSuccession = objDataOperation.ExecQuery(strQ, strListName)

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            If dsPotentialSuccession.Tables(0).Rows.Count > 0 Then
                dsList.Merge(dsPotentialSuccession.Tables(0))
            End If

            '============================ Add Extra Potential Succession Employee(s) -- End

            

            Return dsList

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "GetTalentProcessedDataAfterSettingChage", mstrModuleName)
            Return Nothing
        Finally
            If dsList IsNot Nothing Then dsList.Dispose()
            dsList = Nothing
            If objDataOperation IsNot Nothing Then objDataOperation.Dispose()
            objDataOperation = Nothing
            objEmpDates = Nothing
            exForce = Nothing
        End Try
    End Function

End Class
