﻿'************************************************************************************************************************************
'Class Name : clstlcycle_master.vb
'Purpose    :
'Date       :01-Oct-2020
'Written By :Sandeep
'Modified   :
'************************************************************************************************************************************

Imports eZeeCommonLib
''' <summary>
''' Purpose: 
''' Developer: Sandeep
''' </summary>
Public Class clstlcycle_master
    Private Shared ReadOnly mstrModuleName As String = "clstlcycle_master"
    Dim mstrMessage As String = ""

#Region " Private variables "

    Private mintCycleunkid As Integer
    Private mintYearunkid As Integer
    Private mstrCode As String = String.Empty
    Private mstrName As String = String.Empty
    Private mdtStart_Date As Date
    Private mdtEnd_Date As Date
    Private mintTotaldays As Integer
    Private mblnIsactive As Boolean = True
    Private mintUserunkid As Integer
    Private mintInactiveuserunkid As Integer
    Private mintStatusid As Integer
    Private mintCloseuserunkid As Integer
    Private mstrHostName As String = ""
    Private mstrClientIP As String = ""
    Private mintCompanyUnkid As Integer = 0
    Private mintAuditUserId As Integer = 0
    Private mblnIsWeb As Boolean = False
    Private mstrFormName As String = ""
    Private mstrDatabaseName As String = ""

#End Region

#Region " Properties "
    ''' <summary>
    ''' Purpose: Get Message from Class 
    ''' Modify By: Sandeep
    ''' </summary>
    Public ReadOnly Property _Message() As String
        Get
            Return mstrMessage
        End Get
    End Property

    ''' <summary>
    ''' Purpose: Get or Set cycleunkid
    ''' Modify By: Sandeep
    ''' </summary>
    Public Property _Cycleunkid() As Integer
        Get
            Return mintCycleunkid
        End Get
        Set(ByVal value As Integer)
            mintCycleunkid = value
            Call GetData()
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set yearunkid
    ''' Modify By: Sandeep
    ''' </summary>
    Public Property _Yearunkid() As Integer
        Get
            Return mintYearunkid
        End Get
        Set(ByVal value As Integer)
            mintYearunkid = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set code
    ''' Modify By: Sandeep
    ''' </summary>
    Public Property _Code() As String
        Get
            Return mstrCode
        End Get
        Set(ByVal value As String)
            mstrCode = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set name
    ''' Modify By: Sandeep
    ''' </summary>
    Public Property _Name() As String
        Get
            Return mstrName
        End Get
        Set(ByVal value As String)
            mstrName = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set start_date
    ''' Modify By: Sandeep
    ''' </summary>
    Public Property _Start_Date() As Date
        Get
            Return mdtStart_Date
        End Get
        Set(ByVal value As Date)
            mdtStart_Date = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set end_date
    ''' Modify By: Sandeep
    ''' </summary>
    Public Property _End_Date() As Date
        Get
            Return mdtEnd_Date
        End Get
        Set(ByVal value As Date)
            mdtEnd_Date = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set totaldays
    ''' Modify By: Sandeep
    ''' </summary>
    Public Property _Totaldays() As Integer
        Get
            Return mintTotaldays
        End Get
        Set(ByVal value As Integer)
            mintTotaldays = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set isactive
    ''' Modify By: Sandeep
    ''' </summary>
    Public Property _Isactive() As Boolean
        Get
            Return mblnIsactive
        End Get
        Set(ByVal value As Boolean)
            mblnIsactive = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set userunkid
    ''' Modify By: Sandeep
    ''' </summary>
    Public Property _Userunkid() As Integer
        Get
            Return mintUserunkid
        End Get
        Set(ByVal value As Integer)
            mintUserunkid = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set inactiveuserunkid
    ''' Modify By: Sandeep
    ''' </summary>
    Public Property _Inactiveuserunkid() As Integer
        Get
            Return mintInactiveuserunkid
        End Get
        Set(ByVal value As Integer)
            mintInactiveuserunkid = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set statusid
    ''' Modify By: Sandeep
    ''' </summary>
    Public Property _Statusid() As Integer
        Get
            Return mintStatusid
        End Get
        Set(ByVal value As Integer)
            mintStatusid = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set closeuserunkid
    ''' Modify By: Sandeep
    ''' </summary>
    Public Property _Closeuserunkid() As Integer
        Get
            Return mintCloseuserunkid
        End Get
        Set(ByVal value As Integer)
            mintCloseuserunkid = value
        End Set
    End Property

    Public WriteOnly Property _FormName() As String
        Set(ByVal value As String)
            mstrFormName = value
        End Set
    End Property

    Public WriteOnly Property _ClientIP() As String
        Set(ByVal value As String)
            mstrClientIP = value
        End Set
    End Property

    Public WriteOnly Property _HostName() As String
        Set(ByVal value As String)
            mstrHostName = value
        End Set
    End Property

    Public WriteOnly Property _FromWeb() As Boolean
        Set(ByVal value As Boolean)
            mblnIsWeb = value
        End Set
    End Property

    Public WriteOnly Property _AuditUserId() As Integer
        Set(ByVal value As Integer)
            mintAuditUserId = value
        End Set
    End Property

    Public WriteOnly Property _CompanyUnkid() As Integer
        Set(ByVal value As Integer)
            mintCompanyUnkid = value
        End Set
    End Property

    Public WriteOnly Property _DatabaseName() As String
        Set(ByVal value As String)
            mstrDatabaseName = value
        End Set
    End Property

#End Region

    ''' <summary>
    ''' Modify By: Sandeep
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>
    Public Sub GetData()
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception
        Dim objDataOperation As clsDataOperation
        objDataOperation = New clsDataOperation
        Try
            strQ = "SELECT " & _
              "  cycleunkid " & _
              ", yearunkid " & _
              ", code " & _
              ", name " & _
              ", start_date " & _
              ", end_date " & _
              ", totaldays " & _
              ", isactive " & _
              ", userunkid " & _
              ", inactiveuserunkid " & _
              ", statusid " & _
              ", closeuserunkid " & _
             "FROM " & mstrDatabaseName & "..tlcycle_master " & _
             "WHERE cycleunkid = @cycleunkid "

            objDataOperation.AddParameter("@cycleunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintCycleunkid.ToString)

            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            For Each dtRow As DataRow In dsList.Tables(0).Rows
                mintCycleunkid = CInt(dtRow.Item("cycleunkid"))
                mintYearunkid = CInt(dtRow.Item("yearunkid"))
                mstrCode = dtRow.Item("code").ToString
                mstrName = dtRow.Item("name").ToString
                mdtStart_Date = dtRow.Item("start_date")
                mdtEnd_Date = dtRow.Item("end_date")
                mintTotaldays = CInt(dtRow.Item("totaldays"))
                mblnIsactive = CBool(dtRow.Item("isactive"))
                mintUserunkid = CInt(dtRow.Item("userunkid"))
                mintInactiveuserunkid = CInt(dtRow.Item("inactiveuserunkid"))
                mintStatusid = CInt(dtRow.Item("statusid"))
                mintCloseuserunkid = CInt(dtRow.Item("closeuserunkid"))
                Exit For
            Next
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: GetData; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
    End Sub

    ''' <summary>
    ''' Modify By: Sandeep
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>
    Public Function GetList(ByVal strTableName As String) As DataSet
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        Dim objDataOperation As clsDataOperation
        objDataOperation = New clsDataOperation

        Try
            strQ = "SELECT " & _
              "  tlcycle_master.cycleunkid " & _
              ", tlcycle_master.yearunkid " & _
              ", tlcycle_master.code " & _
              ", tlcycle_master.name " & _
              ", tlcycle_master.start_date " & _
              ", tlcycle_master.end_date " & _
              ", tlcycle_master.totaldays " & _
              ", tlcycle_master.isactive " & _
              ", tlcycle_master.userunkid " & _
              ", tlcycle_master.inactiveuserunkid " & _
              ", tlcycle_master.statusid " & _
              ", tlcycle_master.closeuserunkid " & _
              ", ISNULL(financialyear_name,'') AS Year " & _
              ", CONVERT(NVARCHAR(8),tlcycle_master.start_date,112) AS sdate " & _
              ", CONVERT(NVARCHAR(8),tlcycle_master.end_date,112) AS edate " & _
              ", CASE WHEN tlcycle_master.statusid = 1 THEN @O ELSE @C END AS status " & _
             "FROM " & mstrDatabaseName & "..tlcycle_master " & _
             "  JOIN hrmsConfiguration..cffinancial_year_tran ON cffinancial_year_tran.yearunkid = tlcycle_master.yearunkid " & _
             " WHERE tlcycle_master.isactive = 1 "

            objDataOperation.AddParameter("@O", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 4, "Open"))
            objDataOperation.AddParameter("@C", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 5, "Close"))

            dsList = objDataOperation.ExecQuery(strQ, strTableName)

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            For Each row As DataRow In dsList.Tables(0).Rows
                row("sdate") = eZeeDate.convertDate(row("sdate").ToString).ToShortDateString
                row("edate") = eZeeDate.convertDate(row("edate").ToString).ToShortDateString
            Next

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: GetList; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
        Return dsList
    End Function

    ''' <summary>
    ''' Modify By: Sandeep
    ''' </summary>
    ''' <returns>Boolean</returns>
    ''' <purpose> INSERT INTO Database Table (tlcycle_master) </purpose>
    Public Function Insert() As Boolean
        If isExist(mstrCode, "", -1) Then
            mstrMessage = Language.getMessage(mstrModuleName, 1, "Sorry, This code is already defined. Please define new code.")
            Return False
        End If

        If isExist("", mstrName, -1) Then
            mstrMessage = Language.getMessage(mstrModuleName, 2, "Sorry, This name is already defined. Please define new name.")
            Return False
        End If

        Dim objStage As New clstlstages_master
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception
        Dim objDataOperation As clsDataOperation
        objDataOperation = New clsDataOperation
        objDataOperation.BindTransaction()
        objDataOperation.ClearParameters()

        Try
            objDataOperation.AddParameter("@yearunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintYearunkid.ToString)
            objDataOperation.AddParameter("@code", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrCode.ToString)
            objDataOperation.AddParameter("@name", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrName.ToString)
            objDataOperation.AddParameter("@start_date", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, mdtStart_Date.ToString)
            objDataOperation.AddParameter("@end_date", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, mdtEnd_Date.ToString)
            objDataOperation.AddParameter("@totaldays", SqlDbType.Int, eZeeDataType.INT_SIZE, mintTotaldays.ToString)
            objDataOperation.AddParameter("@isactive", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsactive.ToString)
            objDataOperation.AddParameter("@userunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintUserunkid.ToString)
            objDataOperation.AddParameter("@inactiveuserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintInactiveuserunkid.ToString)
            objDataOperation.AddParameter("@statusid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintStatusid.ToString)
            objDataOperation.AddParameter("@closeuserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintCloseuserunkid.ToString)

            strQ = "INSERT INTO " & mstrDatabaseName & "..tlcycle_master ( " & _
              "  yearunkid " & _
              ", code " & _
              ", name " & _
              ", start_date " & _
              ", end_date " & _
              ", totaldays " & _
              ", isactive " & _
              ", userunkid " & _
              ", inactiveuserunkid " & _
              ", statusid " & _
              ", closeuserunkid" & _
            ") VALUES (" & _
              "  @yearunkid " & _
              ", @code " & _
              ", @name " & _
              ", @start_date " & _
              ", @end_date " & _
              ", @totaldays " & _
              ", @isactive " & _
              ", @userunkid " & _
              ", @inactiveuserunkid " & _
              ", @statusid " & _
              ", @closeuserunkid" & _
            "); SELECT @@identity"

            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            mintCycleunkid = dsList.Tables(0).Rows(0).Item(0)

            If InsertAuditTrails(objDataOperation, enAuditType.ADD) = False Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            'INSERT DEFAULT STAGES (POTENTIAL TALENT,REJECTED,QUALIFIED,APPROVED)
            Dim strArr() As String = {"POTENTIAL TALENT", _
                                      "REJECTED", _
                                      "QUALIFIED", _
                                      "APPROVED"}

            Dim order As Integer = 1
            For Each value As String In strArr
                If objStage.isExist(value, mintCycleunkid, -1, objDataOperation) = False Then
                    With objStage
                        ._AuditUserId = mintAuditUserId
                        ._ClientIP = mstrClientIP
                        ._CompanyUnkid = mintCompanyUnkid
                        ._Cycleunkid = mintCycleunkid
                        ._DatabaseName = mstrDatabaseName
                        ._Floworder = order
                        ._FormName = mstrFormName
                        ._FromWeb = mblnIsWeb
                        ._HostName = mstrHostName
                        ._Isactive = True
                        ._Isdefault = True
                        ._Stage_Name = value
                    End With
                    If objStage.Insert(objDataOperation) = False Then
                        exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                        Throw exForce
                    End If
                    order += 1
                End If
            Next

            objDataOperation.ReleaseTransaction(True)

            Return True
        Catch ex As Exception
            objDataOperation.ReleaseTransaction(False)
            Throw New Exception(ex.Message & "; Procedure Name: Insert; Module Name: " & mstrModuleName)
            Return False
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
            objStage = Nothing
        End Try
    End Function

    ''' <summary>
    ''' Modify By: Sandeep
    ''' </summary>
    ''' <returns>Boolean</returns>
    ''' <purpose> Update Database Table (tlcycle_master) </purpose>
    Public Function Update() As Boolean
        If isExist(mstrCode, "", mintCycleunkid) Then
            mstrMessage = Language.getMessage(mstrModuleName, 1, "Sorry, This code is already defined. Please define new code.")
            Return False
        End If

        If isExist("", mstrName, mintCycleunkid) Then
            mstrMessage = Language.getMessage(mstrModuleName, 2, "Sorry, This name is already defined. Please define new name.")
            Return False
        End If

        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        Dim objDataOperation As clsDataOperation
        objDataOperation = New clsDataOperation
        objDataOperation.BindTransaction()
        objDataOperation.ClearParameters()

        Try
            objDataOperation.AddParameter("@cycleunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintCycleunkid.ToString)
            objDataOperation.AddParameter("@yearunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintYearunkid.ToString)
            objDataOperation.AddParameter("@code", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrCode.ToString)
            objDataOperation.AddParameter("@name", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrName.ToString)
            objDataOperation.AddParameter("@start_date", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, mdtStart_Date.ToString)
            objDataOperation.AddParameter("@end_date", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, mdtEnd_Date.ToString)
            objDataOperation.AddParameter("@totaldays", SqlDbType.Int, eZeeDataType.INT_SIZE, mintTotaldays.ToString)
            objDataOperation.AddParameter("@isactive", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsactive.ToString)
            objDataOperation.AddParameter("@userunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintUserunkid.ToString)
            objDataOperation.AddParameter("@inactiveuserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintInactiveuserunkid.ToString)
            objDataOperation.AddParameter("@statusid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintStatusid.ToString)
            objDataOperation.AddParameter("@closeuserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintCloseuserunkid.ToString)

            strQ = "UPDATE " & mstrDatabaseName & "..tlcycle_master SET " & _
              "  yearunkid = @yearunkid" & _
              ", code = @code" & _
              ", name = @name" & _
              ", start_date = @start_date" & _
              ", end_date = @end_date" & _
              ", totaldays = @totaldays" & _
              ", isactive = @isactive" & _
              ", userunkid = @userunkid" & _
              ", inactiveuserunkid = @inactiveuserunkid" & _
              ", statusid = @statusid" & _
              ", closeuserunkid = @closeuserunkid " & _
            "WHERE cycleunkid = @cycleunkid "

            Call objDataOperation.ExecNonQuery(strQ)

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            If InsertAuditTrails(objDataOperation, enAuditType.EDIT) = False Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            objDataOperation.ReleaseTransaction(True)

            Return True
        Catch ex As Exception
            objDataOperation.ReleaseTransaction(False)
            Throw New Exception(ex.Message & "; Procedure Name: Update; Module Name: " & mstrModuleName)
            Return False
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
    End Function

    ''' <summary>
    ''' Modify By: Sandeep
    ''' </summary>
    ''' <returns>Boolean</returns>
    ''' <purpose> Delete Database Table (tlcycle_master) </purpose>
    Public Function Delete(ByVal intUnkid As Integer) As Boolean
        'If isUsed(intUnkid) Then
        '    mstrMessage = "<Message>"
        '    Return False
        'End If

        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception
        Dim objStage As New clstlstages_master

        dsList = objStage.GetList("List", intUnkid)

        _Cycleunkid = intUnkid
        mblnIsactive = False

        Dim objDataOperation As clsDataOperation
        objDataOperation = New clsDataOperation
        objDataOperation.BindTransaction()
        objDataOperation.ClearParameters()

        Try
            strQ = "UPDATE " & mstrDatabaseName & "..tlcycle_master SET " & _
                   "  isactive = 0 " & _
                   ", inactiveuserunkid = @inactiveuserunkid " & _
                   "WHERE cycleunkid = @cycleunkid "

            objDataOperation.AddParameter("@cycleunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intUnkid)
            objDataOperation.AddParameter("@inactiveuserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintInactiveuserunkid.ToString)

            Call objDataOperation.ExecNonQuery(strQ)

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            If InsertAuditTrails(objDataOperation, enAuditType.DELETE) = False Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            With objStage
                ._AuditUserId = mintAuditUserId
                ._ClientIP = mstrClientIP
                ._DatabaseName = mstrDatabaseName
                ._FormName = mstrFormName
                ._FromWeb = mblnIsWeb
                ._HostName = mstrHostName
            End With

            For Each row As DataRow In dsList.Tables(0).Rows
                If objStage.Delete(row("stageunkid"), objDataOperation) = False Then
                    exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                    Throw exForce
                End If
            Next

            objDataOperation.ReleaseTransaction(True)

            Return True
        Catch ex As Exception
            objDataOperation.ReleaseTransaction(False)
            Throw New Exception(ex.Message & "; Procedure Name: Delete; Module Name: " & mstrModuleName)
            Return False
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
    End Function

    ''' <summary>
    ''' Modify By: Sandeep
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>
    Public Function isUsed(ByVal intUnkid As Integer) As Boolean
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        Dim objDataOperation As clsDataOperation
        objDataOperation = New clsDataOperation
        objDataOperation.ClearParameters()

        Try

            'Pinkal (12-Dec-2020) -- Start
            'Enhancement  -  Working on Talent Issue which is given by Andrew.

            Dim blnIsUsed As Boolean = False

            strQ = "SELECT " & _
                      " 'SELECT 1 FROM ' + TABLE_NAME + ' WHERE cycleunkid = ' + CAST(@cycleunkid AS NVARCHAR(20)) + ' AND ' + COLUMN_NAME + ' = ' + " & _
                      " CAST(CASE WHEN COLUMN_NAME = 'isactive' THEN 1  WHEN COLUMN_NAME = 'isvoid' THEN 0 END AS NVARCHAR(20)) AS CycleUsed" & _
                      " FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_NAME IN ( " & _
                      "                         SELECT TABLE_NAME FROM INFORMATION_SCHEMA.COLUMNS  " & _
                      "                         WHERE COLUMN_NAME = 'cycleunkid' " & _
                      "                         AND TABLE_NAME NOT IN ('tlcycle_master','tlstages_master') AND TABLE_NAME NOT LIKE 'at%' " & _
                      ") AND COLUMN_NAME IN ('isactive','isvoid') "

            objDataOperation.AddParameter("@cycleunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intUnkid)

            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If


            For Each dtRow As DataRow In dsList.Tables("List").Rows
                objDataOperation.ClearParameters()
                Dim dsData As DataSet = objDataOperation.ExecQuery(dtRow("CycleUsed").ToString(), "Used")
                If objDataOperation.ErrorMessage <> "" Then
                    exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                    Throw exForce
                End If
                If dsData IsNot Nothing AndAlso dsData.Tables("Used").Rows.Count > 0 Then
                    blnIsUsed = True
                    Exit For
                End If
            Next

            Return blnIsUsed

            'Pinkal (12-Dec-2020) -- End

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: isUsed; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
    End Function

    ''' <summary>
    ''' Modify By: Sandeep
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>
    Public Function isExist(ByVal strCode As String, ByVal strName As String, Optional ByVal intUnkid As Integer = -1) As Boolean
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        Dim objDataOperation As clsDataOperation
        objDataOperation = New clsDataOperation

        Try
            strQ = "SELECT " & _
              "  cycleunkid " & _
              ", yearunkid " & _
              ", code " & _
              ", name " & _
              ", start_date " & _
              ", end_date " & _
              ", totaldays " & _
              ", isactive " & _
              ", userunkid " & _
              ", inactiveuserunkid " & _
              ", statusid " & _
              ", closeuserunkid " & _
             "FROM " & mstrDatabaseName & "..tlcycle_master " & _
             "WHERE isactive = 1 "

            If strName.Trim.Length > 0 Then
                strQ &= " AND name = @name "
            End If

            If strCode.Trim.Length > 0 Then
                strQ &= "AND code = @code "
            End If

            If intUnkid > 0 Then
                strQ &= " AND cycleunkid <> @cycleunkid"
            End If

            objDataOperation.AddParameter("@code", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, strCode)
            objDataOperation.AddParameter("@name", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, strName)
            objDataOperation.AddParameter("@cycleunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intUnkid)

            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            Return dsList.Tables(0).Rows.Count > 0
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: isExist; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
    End Function

    Public Function InsertAuditTrails(ByVal objDataOperation As clsDataOperation, ByVal eAuditType As enAuditType) As Boolean
        Dim StrQ As String = ""
        Try
            StrQ = "INSERT INTO " & mstrDatabaseName & "..attlcycle_master ( " & _
                       "  tranguid " & _
                       ", cycleunkid " & _
                       ", yearunkid " & _
                       ", code " & _
                       ", name " & _
                       ", start_date " & _
                       ", end_date " & _
                       ", totaldays " & _
                       ", audittypeid " & _
                       ", audtuserunkid " & _
                       ", auditdatetime " & _
                       ", formname " & _
                       ", ip " & _
                       ", host " & _
                       ", isweb" & _
                   ") VALUES (" & _
                       "  LOWER(NEWID()) " & _
                       ", @cycleunkid " & _
                       ", @yearunkid " & _
                       ", @code " & _
                       ", @name " & _
                       ", @start_date " & _
                       ", @end_date " & _
                       ", @totaldays " & _
                       ", @audittypeid " & _
                       ", @audtuserunkid " & _
                       ", GETDATE() " & _
                       ", @formname " & _
                       ", @ip " & _
                       ", @host " & _
                       ", @isweb " & _
                    ") "
            objDataOperation.ClearParameters()
            objDataOperation.AddParameter("@cycleunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintCycleunkid.ToString)
            objDataOperation.AddParameter("@yearunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintYearunkid.ToString)
            objDataOperation.AddParameter("@code", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrCode.ToString)
            objDataOperation.AddParameter("@name", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrName.ToString)
            objDataOperation.AddParameter("@start_date", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, mdtStart_Date.ToString)
            objDataOperation.AddParameter("@end_date", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, mdtEnd_Date.ToString)
            objDataOperation.AddParameter("@totaldays", SqlDbType.Int, eZeeDataType.INT_SIZE, mintTotaldays.ToString)
            objDataOperation.AddParameter("@isactive", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsactive.ToString)
            objDataOperation.AddParameter("@userunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintUserunkid.ToString)
            objDataOperation.AddParameter("@inactiveuserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintInactiveuserunkid.ToString)
            objDataOperation.AddParameter("@statusid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintStatusid.ToString)
            objDataOperation.AddParameter("@closeuserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintCloseuserunkid.ToString)
            objDataOperation.AddParameter("@audittypeid", SqlDbType.Int, eZeeDataType.INT_SIZE, eAuditType)
            objDataOperation.AddParameter("@audtuserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintAuditUserId)
            objDataOperation.AddParameter("@formname", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrFormName)
            objDataOperation.AddParameter("@ip", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrClientIP)
            objDataOperation.AddParameter("@host", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrHostName)
            objDataOperation.AddParameter("@isweb", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsWeb)

            objDataOperation.ExecNonQuery(StrQ)

            If objDataOperation.ErrorMessage <> "" Then
                Throw New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
            End If

            Return True
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: InsertAuditTrails; Module Name: " & mstrModuleName)
        Finally
        End Try
    End Function

    Public Function getListForCombo(ByVal intYearunkid As Integer, _
                                    ByVal strDatabaseName As String, _
                                    ByVal dtDatabaseStartDate As Date, _
                                    Optional ByVal strListName As String = "List", _
                                    Optional ByVal mblFlag As Boolean = False, _
                                    Optional ByVal intStatusID As Integer = 0) As DataSet
        Dim dsList As New DataSet
        Dim objDataOperation As New clsDataOperation
        Dim strQ As String = ""
        Dim exForce As Exception
        Try

            If mblFlag = True Then
                strQ = "SELECT 0 as cycleunkid, ' ' +  @name  as name, '19000101' AS start_date, '19000101' AS end_date, 0 AS yearunkid, '' AS code UNION "
            End If
            strQ &= "SELECT " & _
                        "  tlcycle_master.cycleunkid " & _
                        ", tlcycle_master.name " & _
                        ", convert(char(8),tlcycle_master.start_date,112) as start_date " & _
                        ", convert(char(8),tlcycle_master.end_date,112) as end_date " & _
                        ", tlcycle_master.yearunkid " & _
                        ", tlcycle_master.code AS code " & _
                    "FROM " & strDatabaseName & "..tlcycle_master WHERE isactive = 1  "

            'Pinkal (28-Apr-2023) -- Start
            'Problem in Filling Talent Cycle.   
            'If intYearunkid > 0 Then
            '    strQ &= "AND tlcycle_master.yearunkid = @YearId "
            '    objDataOperation.AddParameter("@YearId", SqlDbType.Int, eZeeDataType.INT_SIZE, intYearunkid)
            'End If
            'Pinkal (28-Apr-2023) -- End
          

            If intStatusID > 0 Then
                strQ &= "AND statusid = @statusid "
                objDataOperation.AddParameter("@statusid", SqlDbType.Int, eZeeDataType.INT_SIZE, intStatusID)
            End If
            strQ &= "ORDER BY end_date "

            objDataOperation.AddParameter("@name", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 3, "Select"))

            dsList = objDataOperation.ExecQuery(strQ, strListName)

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: getListForCombo; Module Name: " & mstrModuleName)
        Finally
        End Try
        Return dsList
    End Function

    Public Function GetNextPeriod(ByVal strListName As String _
                                  , ByVal intModuleRefid As Integer _
                                  , ByVal intPeriodUnkID As Integer _
                                  , ByVal iYearUnkid As Integer) As DataSet
        Dim dsList As New DataSet
        Dim objDataOperation As New clsDataOperation
        Dim strQ As String = String.Empty
        Dim exForce As Exception
        Try
            strQ = "SELECT TOP 1 " & _
                            "tlcycle_master.cycleunkid " & _
                          ", tlcycle_master.code " & _
                          ", tlcycle_master.name " & _
                          ", tlcycle_master.yearunkid " & _
                          ", CONVERT(CHAR(8),tlcycle_master.start_date, 112) AS start_date " & _
                          ", CONVERT(CHAR(8),tlcycle_master.end_date, 112) AS end_date " & _
                          ", tlcycle_master.totaldays " & _
                          ", tlcycle_master.statusid " & _
                          ", tlcycle_master.isactive " & _
                          ", tlcycle_master.userunkid " & _
                          ", tlcycle_master.closeuserunkid " & _
                          ", tlcycle_master.inactiveuserunkid " & _
                    "FROM " & mstrDatabaseName & "..tlcycle_master " & _
                    "WHERE CONVERT(CHAR(8),tlcycle_master.end_date, 112) > ( SELECT CONVERT(CHAR(8),tlcycle_master.end_date, 112) " & _
                    "   FROM   " & mstrDatabaseName & "..tlcycle_master " & _
                    "   WHERE  cycleunkid = @cycleunkid " & _
                    ") " & _
                    "AND tlcycle_master.yearunkid = @yearunkid " & _
                    "AND tlcycle_master.statusid = 1 " & _
                    "AND tlcycle_master.isactive = 1 " & _
                    "ORDER BY tlcycle_master.end_date "

            objDataOperation.AddParameter("@yearunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, iYearUnkid)
            objDataOperation.AddParameter("@cycleunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intPeriodUnkID)

            dsList = objDataOperation.ExecQuery(strQ, strListName)
            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: GetNextPeriod; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            objDataOperation = Nothing
            dsList.Dispose()
        End Try
        Return dsList
    End Function


    'Pinkal (12-Dec-2020) -- Start
    'Enhancement  -  Working on Talent Issue which is given by Andrew.
    Public Function getCurrentCycleId(ByVal dtCurrentDate As DateTime, _
                                    ByVal intYearUnkId As Integer, _
                                    Optional ByVal intStatusID As Integer = 0, _
                                    Optional ByVal blnCheckPeriodExist As Boolean = False, _
                                    Optional ByVal blnIsApplyYearCnd As Boolean = True, _
                                    Optional ByVal xDataOperation As clsDataOperation = Nothing, _
                                    Optional ByVal blnIsApplyDateCnd As Boolean = True, _
                                    Optional ByVal blnConsiderPrevYearPeriods As Boolean = False _
                                    ) As Integer

        Dim dsList As New DataSet

        Dim objDataOperation As clsDataOperation
        If xDataOperation IsNot Nothing Then
            objDataOperation = xDataOperation
        Else
            objDataOperation = New clsDataOperation
        End If
        objDataOperation.ClearParameters()

        Dim strQ As String = String.Empty
        Dim exForce As Exception
        Dim intCycleId As Integer = 0
        Try
            strQ = "SELECT  TOP 1 tlcycle_master.cycleunkid " & _
                          ", tlcycle_master.code " & _
                          ", tlcycle_master.name " & _
                          ", tlcycle_master.yearunkid " & _
                          ", CONVERT(CHAR(8),tlcycle_master.start_date,112) AS start_date " & _
                          ", CONVERT(CHAR(8),tlcycle_master.end_date,112) AS end_date " & _
                          ", tlcycle_master.totaldays " & _
                          ", tlcycle_master.statusid " & _
                          ", tlcycle_master.isactive " & _
                          " FROM  tlcycle_master " & _
                          " WHERE  isactive = 1 "

            If blnConsiderPrevYearPeriods = False Then
                If intYearUnkId > 0 Then
                    strQ &= "AND yearunkid = " & intYearUnkId & "  "
                ElseIf intYearUnkId <= 0 AndAlso blnIsApplyYearCnd Then
                    strQ &= "AND yearunkid = " & FinancialYear._Object._YearUnkid & "  "
                End If
            End If

            If intStatusID > 0 Then
                strQ &= "AND statusid = @statusid "
                objDataOperation.AddParameter("@statusid", SqlDbType.Int, eZeeDataType.INT_SIZE, intStatusID)
            End If

            If blnCheckPeriodExist Then
                strQ &= " AND CONVERT(CHAR(8),tlcycle_master.start_date,112) <= @Date AND  CONVERT(CHAR(8),tlcycle_master.end_date,112) >= @Date"
            Else
                If blnIsApplyDateCnd Then
                    strQ &= " AND CONVERT(CHAR(8),tlcycle_master.start_date,112) <= @Date "
                End If
            End If

            If blnIsApplyDateCnd Then
                strQ &= " ORDER BY start_date DESC "
            Else
                strQ &= " ORDER BY start_date "
            End If


            objDataOperation.AddParameter("@Date", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, eZeeDate.convertDate(dtCurrentDate))

            dsList = objDataOperation.ExecQuery(strQ, "List")
            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            If dsList.Tables("List").Rows.Count > 0 Then
                intCycleId = CInt(dsList.Tables("List").Rows(0).Item("cycleunkid").ToString)
            End If

            Return intCycleId
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: getCurrentCycleId; Module Name: " & mstrModuleName)
            Return intCycleId
        Finally
            exForce = Nothing
            If xDataOperation Is Nothing Then objDataOperation = Nothing
            dsList.Dispose()
            dsList = Nothing
        End Try

    End Function
    'Pinkal (12-Dec-2020) -- End

	'<Language> This Auto Generated Text Please Do Not Modify it.
#Region " Language & UI Settings "
	Public Shared Sub SetMessages()
		Try
			Language.setMessage(mstrModuleName, 1, "Sorry, This code is already defined. Please define new code.")
			Language.setMessage(mstrModuleName, 2, "Sorry, This name is already defined. Please define new name.")
			Language.setMessage(mstrModuleName, 3, "Select")
			Language.setMessage(mstrModuleName, 4, "Open")
			Language.setMessage(mstrModuleName, 5, "Close")

		Catch Ex As Exception
			DisplayError.Show("-1", ex.Message, "SetMessages", mstrModuleName)
		End Try
	End Sub
#End Region 'Language & UI Settings
	'</Language>
End Class