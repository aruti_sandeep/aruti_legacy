﻿'************************************************************************************************************************************
'Class Name : clsloanTranche_request_Tran.vb
'Purpose    :
'Date       :10-Aug-2023
'Written By :Pinkal
'Modified   :
'************************************************************************************************************************************

Imports eZeeCommonLib
Imports System.Web

''' <summary>
''' Purpose: 
''' Developer: Pinkal
''' </summary>
Public Class clsloanTranche_request_Tran
    Private Shared ReadOnly mstrModuleName As String = "clsloanTranche_request_Tran"
    Dim objDataOperation As clsDataOperation
    Dim mstrMessage As String = ""

#Region " Private variables "
    Private mintLoantrancherequestunkid As Integer
    Private mintProcesspendingloanunkid As Integer
    Private mintEmployeeunkid As Integer
    Private mintLoanschemeunkid As Integer
    Private mstrApplicationno As String = String.Empty
    Private mdtTranchedate As Date
    Private mdecTrancheamt As Decimal
    Private mstrRemark As String = String.Empty
    Private mintStatusunkid As Integer
    Private mintCountryunkid As Integer
    Private mintDeductionPeriodunkid As Integer = 0
    Private mintFinalDeductionPeriodunkid As Integer = 0
    Private mintFinalapproverunkid As Integer
    Private mdecApproved_Trancheamt As Decimal
    Private mdtSendEmpReminderForNextTrance As DateTime = Nothing
    Private mblnNtfSendEmpForNextTranche As Boolean = False
    Private mintUserunkid As Integer
    Private mintLoginemployeeunkid As Integer
    Private mblnIsvoid As Boolean
    Private mdtVoiddatetime As Date
    Private mintVoiduserunkid As Integer
    Private mstrVoidreason As String = String.Empty
    Private mintVoidloginemployeeunkid As Integer = -1
    Private mstrFormName As String = ""
    Private mstrClientIP As String = ""
    Private mstrHostName As String = ""
    Private mblnIsFromWeb As Boolean = False
    Private mintAuditUserId As Integer = 0
    Private mdtDocument As DataTable = Nothing

    'Pinkal (16-Nov-2023) -- Start
    '(A1X-1489) NMB - Mortgage tranche disbursement API.
    Private mstrPostedData As String = ""
    Private mstrResponseData As String = ""
    Private mblnPostedError As Boolean = False
    'Pinkal (16-Nov-2023) -- End

#End Region

#Region " Properties "
    ''' <summary>
    ''' Purpose: Get Message from Class 
    ''' Modify By: Pinkal
    ''' </summary>
    Public ReadOnly Property _Message() As String
        Get
            Return mstrMessage
        End Get
    End Property

    ''' <summary>
    ''' Purpose: Get or Set loantrancherequestunkid
    ''' Modify By: Pinkal
    ''' </summary>
    Public Property _Loantrancherequestunkid(Optional ByVal objDoOperation As clsDataOperation = Nothing) As Integer
        Get
            Return mintLoantrancherequestunkid
        End Get
        Set(ByVal value As Integer)
            mintLoantrancherequestunkid = value
            Call GetData(objDoOperation)
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set processpendingloanunkid
    ''' Modify By: Pinkal
    ''' </summary>
    Public Property _Processpendingloanunkid() As Integer
        Get
            Return mintProcesspendingloanunkid
        End Get
        Set(ByVal value As Integer)
            mintProcesspendingloanunkid = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set employeeunkid
    ''' Modify By: Pinkal
    ''' </summary>
    Public Property _Employeeunkid() As Integer
        Get
            Return mintEmployeeunkid
        End Get
        Set(ByVal value As Integer)
            mintEmployeeunkid = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set loanschemeunkid
    ''' Modify By: Pinkal
    ''' </summary>
    Public Property _Loanschemeunkid() As Integer
        Get
            Return mintLoanschemeunkid
        End Get
        Set(ByVal value As Integer)
            mintLoanschemeunkid = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set applicationno
    ''' Modify By: Pinkal
    ''' </summary>
    Public Property _Applicationno() As String
        Get
            Return mstrApplicationno
        End Get
        Set(ByVal value As String)
            mstrApplicationno = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set tranchedate
    ''' Modify By: Pinkal
    ''' </summary>
    Public Property _Tranchedate() As Date
        Get
            Return mdtTranchedate
        End Get
        Set(ByVal value As Date)
            mdtTranchedate = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set trancheamt
    ''' Modify By: Pinkal
    ''' </summary>
    Public Property _Trancheamt() As Decimal
        Get
            Return mdecTrancheamt
        End Get
        Set(ByVal value As Decimal)
            mdecTrancheamt = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set remark
    ''' Modify By: Pinkal
    ''' </summary>
    Public Property _Remark() As String
        Get
            Return mstrRemark
        End Get
        Set(ByVal value As String)
            mstrRemark = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set statusunkid
    ''' Modify By: Pinkal
    ''' </summary>
    Public Property _Statusunkid() As Integer
        Get
            Return mintStatusunkid
        End Get
        Set(ByVal value As Integer)
            mintStatusunkid = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set Countryunkid
    ''' Modify By: Pinkal
    ''' </summary>
    Public Property _Countryunkid() As Integer
        Get
            Return mintCountryunkid
        End Get
        Set(ByVal value As Integer)
            mintCountryunkid = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set DeductionPeriodunkid
    ''' Modify By: Pinkal
    ''' </summary>
    Public Property _DeductionPeriodunkid() As Integer
        Get
            Return mintDeductionPeriodunkid
        End Get
        Set(ByVal value As Integer)
            mintDeductionPeriodunkid = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set FinalDeductionPeriodunkid
    ''' Modify By: Pinkal
    ''' </summary>
    Public Property _FinalDeductionPeriodunkid() As Integer
        Get
            Return mintFinalDeductionPeriodunkid
        End Get
        Set(ByVal value As Integer)
            mintFinalDeductionPeriodunkid = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set finalapproverunkid
    ''' Modify By: Pinkal
    ''' </summary>
    Public Property _Finalapproverunkid() As Integer
        Get
            Return mintFinalapproverunkid
        End Get
        Set(ByVal value As Integer)
            mintFinalapproverunkid = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set approved_trancheamt
    ''' Modify By: Pinkal
    ''' </summary>
    Public Property _Approved_Trancheamt() As Decimal
        Get
            Return mdecApproved_Trancheamt
        End Get
        Set(ByVal value As Decimal)
            mdecApproved_Trancheamt = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set ReminderSendEmpForNextTrance
    ''' Modify By: Pinkal
    ''' </summary>
    Public Property _SendEmpReminderForNextTrance() As DateTime
        Get
            Return mdtSendEmpReminderForNextTrance
        End Get
        Set(ByVal value As DateTime)
            mdtSendEmpReminderForNextTrance = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set NtfSendEmpForNextTranche
    ''' Modify By: Pinkal
    ''' </summary>
    Public Property _NtfSendEmpForNextTranche() As Boolean
        Get
            Return mblnNtfSendEmpForNextTranche
        End Get
        Set(ByVal value As Boolean)
            mblnNtfSendEmpForNextTranche = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set Document
    ''' Modify By: Pinkal
    ''' </summary>
    Public Property _Document() As DataTable
        Get
            Return mdtDocument
        End Get
        Set(ByVal value As DataTable)
            mdtDocument = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set userunkid
    ''' Modify By: Pinkal
    ''' </summary>
    Public Property _Userunkid() As Integer
        Get
            Return mintUserunkid
        End Get
        Set(ByVal value As Integer)
            mintUserunkid = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set loginemployeeunkid
    ''' Modify By: Pinkal
    ''' </summary>
    Public Property _Loginemployeeunkid() As Integer
        Get
            Return mintLoginemployeeunkid
        End Get
        Set(ByVal value As Integer)
            mintLoginemployeeunkid = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set isvoid
    ''' Modify By: Pinkal
    ''' </summary>
    Public Property _Isvoid() As Boolean
        Get
            Return mblnIsvoid
        End Get
        Set(ByVal value As Boolean)
            mblnIsvoid = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set voiddatetime
    ''' Modify By: Pinkal
    ''' </summary>
    Public Property _Voiddatetime() As Date
        Get
            Return mdtVoiddatetime
        End Get
        Set(ByVal value As Date)
            mdtVoiddatetime = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set voiduserunkid
    ''' Modify By: Pinkal
    ''' </summary>
    Public Property _Voiduserunkid() As Integer
        Get
            Return mintVoiduserunkid
        End Get
        Set(ByVal value As Integer)
            mintVoiduserunkid = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set voidreason
    ''' Modify By: Pinkal
    ''' </summary>
    Public Property _Voidreason() As String
        Get
            Return mstrVoidreason
        End Get
        Set(ByVal value As String)
            mstrVoidreason = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set voidloginemployeeunkid
    ''' Modify By: Pinkal
    ''' </summary>
    Public Property _Voidloginemployeeunkid() As Integer
        Get
            Return mintVoidloginemployeeunkid
        End Get
        Set(ByVal value As Integer)
            mintVoidloginemployeeunkid = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set FormName
    ''' Modify By: Pinkal
    ''' </summary>
    Public Property _FormName() As String
        Get
            Return mstrFormName
        End Get
        Set(ByVal value As String)
            mstrFormName = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set ClientIP
    ''' Modify By: Pinkal
    ''' </summary>
    Public Property _ClientIP() As String
        Get
            Return mstrClientIP
        End Get
        Set(ByVal value As String)
            mstrClientIP = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set HostName
    ''' Modify By: Pinkal
    ''' </summary>
    Public Property _HostName() As String
        Get
            Return mstrHostName
        End Get
        Set(ByVal value As String)
            mstrHostName = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set IsFromWeb
    ''' Modify By: Pinkal
    ''' </summary>
    Public Property _IsFromWeb() As Boolean
        Get
            Return mblnIsFromWeb
        End Get
        Set(ByVal value As Boolean)
            mblnIsFromWeb = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set AuditUserId
    ''' Modify By: Pinkal
    ''' </summary>
    Public Property _AuditUserId() As Integer
        Get
            Return mintAuditUserId
        End Get
        Set(ByVal value As Integer)
            mintAuditUserId = value
        End Set
    End Property

    'Pinkal (16-Nov-2023) -- Start
    '(A1X-1489) NMB - Mortgage tranche disbursement API.

    ''' <summary>
    ''' Purpose: Get or Set PostedData
    ''' Modify By: Pinkal
    ''' </summary>
    Public Property _PostedData() As String
        Get
            Return mstrPostedData
        End Get
        Set(ByVal value As String)
            mstrPostedData = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set ReponseData
    ''' Modify By: Pinkal
    ''' </summary>
    Public Property _ReponseData() As String
        Get
            Return mstrResponseData
        End Get
        Set(ByVal value As String)
            mstrResponseData = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set IsPostedError
    ''' Modify By: Pinkal
    ''' </summary>
    Public Property _IsPostedError() As Boolean
        Get
            Return mblnPostedError
        End Get
        Set(ByVal value As Boolean)
            mblnPostedError = value
        End Set
    End Property

    'Pinkal (16-Nov-2023) -- End


#End Region

    ''' <summary>
    ''' Modify By: Pinkal
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose> 
    ''' 
    Public Sub GetData(Optional ByVal objDoOperation As clsDataOperation = Nothing)
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception
        Dim objDataOperation As clsDataOperation = Nothing

        If objDoOperation Is Nothing Then
            objDataOperation = New clsDataOperation
        Else
            objDataOperation = objDoOperation
        End If

        Try
            strQ = "SELECT " & _
                      "  loantrancherequestunkid " & _
                      ", processpendingloanunkid " & _
                      ", employeeunkid " & _
                      ", loanschemeunkid " & _
                      ", applicationno " & _
                      ", tranchedate " & _
                      ", trancheamt " & _
                      ", remark " & _
                      ", statusunkid " & _
                      ", countryunkid " & _
                      ", deductionperiodunkid " & _
                      ", finaldeductionperiodunkid " & _
                      ", finalapproverunkid " & _
                      ", approved_trancheamt " & _
                      ", sendempreminderfornexttranche " & _
                      ", ntfsendempfornexttranche " & _
                      ", userunkid " & _
                      ", loginemployeeunkid " & _
                      ", voidloginemployeeunkid " & _
                      ", isvoid " & _
                      ", voiddatetime " & _
                      ", voiduserunkid " & _
                      ", voidreason " & _
                      ", ISNULL(fxcube_posteddata,'') AS fxcube_posteddata " & _
                      ", ISNULL(fxcube_responsedata,'') AS fxcube_responsedata " & _
                      ", ISNULL(isfxcube_error,0) AS isfxcube_error " & _
                      " FROM lnloantranche_request_tran " & _
                      " WHERE loantrancherequestunkid = @loantrancherequestunkid "

            'Pinkal (16-Nov-2023) -- (A1X-1489) NMB - Mortgage tranche disbursement API.[    ", ISNULL(fxcube_posteddata,'') AS fxcube_posteddata , ISNULL(fxcube_responsedata,'') AS fxcube_responsedata , ISNULL(isfxcube_error,0) AS isfxcube_error " & _] 

            objDataOperation.ClearParameters()
            objDataOperation.AddParameter("@loantrancherequestunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintLoantrancherequestunkid.ToString)

            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            For Each dtRow As DataRow In dsList.Tables(0).Rows
                mintLoantrancherequestunkid = CInt(dtRow.Item("loantrancherequestunkid"))
                mintProcesspendingloanunkid = CInt(dtRow.Item("processpendingloanunkid"))
                mintEmployeeunkid = CInt(dtRow.Item("employeeunkid"))
                mintLoanschemeunkid = CInt(dtRow.Item("loanschemeunkid"))
                mstrApplicationno = dtRow.Item("applicationno").ToString
                mdtTranchedate = dtRow.Item("tranchedate")
                mdecTrancheamt = CDec(dtRow.Item("trancheamt"))
                mstrRemark = dtRow.Item("remark").ToString
                mintStatusunkid = CInt(dtRow.Item("statusunkid"))
                mintCountryunkid = CInt(dtRow.Item("countryunkid"))
                mintDeductionPeriodunkid = CInt(dtRow.Item("deductionperiodunkid"))
                mintFinalDeductionPeriodunkid = CInt(dtRow.Item("finaldeductionperiodunkid"))
                mintFinalapproverunkid = CInt(dtRow.Item("finalapproverunkid"))
                mdecApproved_Trancheamt = CDec(dtRow.Item("approved_trancheamt"))
                mblnNtfSendEmpForNextTranche = CBool(dtRow.Item("ntfsendempfornexttranche"))
                mintUserunkid = CInt(dtRow.Item("userunkid"))
                mintLoginemployeeunkid = CInt(dtRow.Item("loginemployeeunkid"))
                mintVoidloginemployeeunkid = CInt(dtRow.Item("voidloginemployeeunkid"))
                mblnIsvoid = CBool(dtRow.Item("isvoid"))

                If IsDBNull(dtRow.Item("sendempreminderfornexttranche")) = False Then
                    mdtSendEmpReminderForNextTrance = dtRow.Item("sendempreminderfornexttranche")
                End If

                If IsDBNull(dtRow.Item("voiddatetime")) = False Then
                    mdtVoiddatetime = dtRow.Item("voiddatetime")
                End If

                mintVoiduserunkid = CInt(dtRow.Item("voiduserunkid"))
                mstrVoidreason = dtRow.Item("voidreason").ToString()

                'Pinkal (16-Nov-2023) -- Start
                '(A1X-1489) NMB - Mortgage tranche disbursement API.
                mstrPostedData = dtRow.Item("fxcube_posteddata").ToString()
                mstrResponseData = dtRow.Item("fxcube_responsedata").ToString()
                mblnPostedError = dtRow.Item("isfxcube_error").ToString()
                'Pinkal (16-Nov-2023) -- End  

                Exit For
            Next
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: GetData; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            If objDoOperation Is Nothing Then objDataOperation = Nothing
        End Try
    End Sub

    ''' <summary>
    ''' Modify By: Pinkal
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>
    Public Function GetList(ByVal strTableName As String, Optional ByVal blnOnlyActive As Boolean = True, Optional ByVal mblnIncludeGroup As Boolean = True, Optional ByVal mstrFilter As String = "", Optional ByVal objDoOperation As clsDataOperation = Nothing) As DataSet
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception
        Dim objDataOperation As clsDataOperation = Nothing

        If objDoOperation Is Nothing Then
            objDataOperation = New clsDataOperation
        Else
            objDataOperation = objDoOperation
        End If

        Try

            If mblnIncludeGroup Then

                strQ = " SELECT " & _
                          "  CAST(1 AS BIT) AS IsGrp " & _
                          ", lnloantranche_request_tran.loantrancherequestunkid " & _
                          ", lnloantranche_request_tran.processpendingloanunkid " & _
                          ", ''AS LoanApplicationNo " & _
                          ", lnloantranche_request_tran.employeeunkid " & _
                          ", '' AS Employeecode " & _
                          ", '' AS Employee " & _
                          ", -1 AS loanschemeunkid " & _
                          ", '' AS LoanschemeCode " & _
                          ", '' AS Loanscheme " & _
                          ", lnloantranche_request_tran.applicationno " & _
                          ", NULL AS tranchedate " & _
                          ", 0.00 AS trancheamt " & _
                          ", '' AS remark " & _
                          ", 0 AS statusunkid " & _
                          ", '' AS Status " & _
                          ", 0 AS countryunkid " & _
                          ", 0 AS deductionperiodunkid " & _
                          ", 0 AS finaldeductionperiodunkid " & _
                          ", -1 AS finalapproverunkid " & _
                          ", 0.00 AS approved_trancheamt " & _
                          ", NULL AS sendempreminderfornexttranche " & _
                          ", CAST(0 AS BIT) AS ntfsendempfornexttranche " & _
                          ", -1.userunkid " & _
                          ", 0 AS loginemployeeunkid " & _
                          ", 0 AS voidloginemployeeunkid " & _
                          ", 0 AS isvoid " & _
                          ", NULL AS voiddatetime " & _
                          ", 0 AS voiduserunkid " & _
                          ", '' AS voidreason " & _
                          ", '' AS fxcube_posteddata " & _
                          ", '' AS fxcube_responsedata " & _
                          ", CAST (0 AS BIT) AS isfxcube_error " & _
                          ", '' AS FlexcubeStatus " & _
                          " FROM lnloantranche_request_tran " & _
                          " LEFT JOIN lnloan_process_pending_loan ON lnloan_process_pending_loan.processpendingloanunkid = lnloantranche_request_tran.processpendingloanunkid AND lnloan_process_pending_loan.isvoid = 0 " & _
                          " LEFT JOIN hremployee_master ON lnloantranche_request_tran.employeeunkid = hremployee_master.employeeunkid "

                'Pinkal (16-Nov-2023) -- (A1X-1489) NMB - Mortgage tranche disbursement API.[ '' AS fxcube_posteddata, '' AS fxcube_responsedata, CAST (0 AS BIT) AS isfxcube_error]

                If blnOnlyActive Then
                    strQ &= " WHERE lnloantranche_request_tran.isvoid = 0 "
                End If

                If mstrFilter.Trim.Length > 0 Then
                    strQ &= " AND " & mstrFilter
                End If

                strQ &= " UNION "

            End If


            strQ &= "SELECT " & _
                          "  CAST(0 AS BIT) AS IsGrp " & _
                          ", lnloantranche_request_tran.loantrancherequestunkid " & _
                          ", lnloantranche_request_tran.processpendingloanunkid " & _
                          ", ISNULL(lnloan_process_pending_loan.application_no,'') AS LoanApplicationNo " & _
                          ", lnloantranche_request_tran.employeeunkid " & _
                          ", ISNULL(hremployee_master.employeecode,'') AS Employeecode " & _
                          ", ISNULL(hremployee_master.firstname,'') + ' ' + ISNULL(hremployee_master.surname,'')  AS Employee " & _
                          ", lnloantranche_request_tran.loanschemeunkid " & _
                          ", ISNULL(lnloan_scheme_master.code,'') AS LoanschemeCode " & _
                          ", ISNULL(lnloan_scheme_master.name,'') AS Loanscheme " & _
                          ", lnloantranche_request_tran.applicationno " & _
                          ", lnloantranche_request_tran.tranchedate " & _
                          ", lnloantranche_request_tran.trancheamt " & _
                          ", lnloantranche_request_tran.remark " & _
                          ", lnloantranche_request_tran.statusunkid " & _
                          ", CASE WHEN lnloantranche_request_tran.statusunkid = " & enLoanApplicationStatus.PENDING & " THEN @Pending " & _
                          "           WHEN lnloantranche_request_tran.statusunkid = " & enLoanApplicationStatus.APPROVED & " THEN @Approved " & _
                          "           WHEN lnloantranche_request_tran.statusunkid = " & enLoanApplicationStatus.REJECTED & " THEN @Rejected END Status " & _
                          ", lnloantranche_request_tran.countryunkid " & _
                          ", lnloantranche_request_tran.deductionperiodunkid " & _
                          ", lnloantranche_request_tran.finaldeductionperiodunkid " & _
                          ", lnloantranche_request_tran.finalapproverunkid " & _
                          ", lnloantranche_request_tran.approved_trancheamt " & _
                          ", lnloantranche_request_tran.sendempreminderfornexttranche " & _
                          ", lnloantranche_request_tran.ntfsendempfornexttranche " & _
                          ", lnloantranche_request_tran.userunkid " & _
                          ", lnloantranche_request_tran.loginemployeeunkid " & _
                          ", lnloantranche_request_tran.voidloginemployeeunkid " & _
                          ", lnloantranche_request_tran.isvoid " & _
                          ", lnloantranche_request_tran.voiddatetime " & _
                          ", lnloantranche_request_tran.voiduserunkid " & _
                          ", lnloantranche_request_tran.voidreason " & _
                          ", ISNULL(lnloantranche_request_tran.fxcube_posteddata,'') AS fxcube_posteddata " & _
                          ", ISNULL(lnloantranche_request_tran.fxcube_responsedata,'') AS fxcube_responsedata " & _
                          ", ISNULL(lnloantranche_request_tran.isfxcube_error,0) AS isfxcube_error " & _
                          ", CASE WHEN ISNULL(lnloantranche_request_tran.isfxcube_error,0) = 0 THEN @Success ELSE @Fail END AS FlexcubeStatus " & _
                          " FROM lnloantranche_request_tran " & _
                          " LEFT JOIN lnloan_process_pending_loan ON lnloan_process_pending_loan.processpendingloanunkid = lnloantranche_request_tran.processpendingloanunkid AND lnloan_process_pending_loan.isvoid = 0 " & _
                          " LEFT JOIN hremployee_master ON lnloantranche_request_tran.employeeunkid = hremployee_master.employeeunkid " & _
                          " LEFT JOIN lnloan_scheme_master ON lnloan_scheme_master.loanschemeunkid = lnloantranche_request_tran.loanschemeunkid "

            'Pinkal (16-Nov-2023) -- (A1X-1489) NMB - Mortgage tranche disbursement API.[, ISNULL(lnloantranche_request_tran.fxcube_posteddata,'') AS fxcube_posteddata , ISNULL(lnloantranche_request_tran.fxcube_responsedata,'') AS fxcube_responsedata , ISNULL(lnloantranche_request_tran.isfxcube_error,0) AS isfxcube_error " & _]


            If blnOnlyActive Then
                strQ &= " WHERE lnloantranche_request_tran.isvoid = 0 "
            End If

            If mstrFilter.Trim.Length > 0 Then
                strQ &= " AND " & mstrFilter
            End If

            strQ &= " ORDER BY lnloantranche_request_tran.loantrancherequestunkid DESC ,IsGrp DESC "

            objDataOperation.ClearParameters()
            objDataOperation.AddParameter("@Pending", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 2, "Pending"))
            objDataOperation.AddParameter("@Approved", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 3, "Approved"))
            objDataOperation.AddParameter("@Rejected", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 4, "Rejected"))

            'Pinkal (16-Nov-2023) -- Start
            '(A1X-1489) NMB - Mortgage tranche disbursement API.
            objDataOperation.AddParameter("@Success", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 26, "Success"))
            objDataOperation.AddParameter("@Fail", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 27, "Fail"))
            'Pinkal (16-Nov-2023) -- End
        

            dsList = objDataOperation.ExecQuery(strQ, strTableName)

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: GetList; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            If objDoOperation Is Nothing Then objDataOperation = Nothing
        End Try
        Return dsList
    End Function

    ''' <summary>
    ''' Modify By: Pinkal
    ''' </summary>
    ''' <returns>Boolean</returns>
    ''' <purpose> INSERT INTO Database Table (lnloantranche_request_tran) </purpose>
    Public Function Insert(ByVal xDatabaseName As String, _
                                   ByVal xUserUnkid As Integer, _
                                   ByVal xYearUnkid As Integer, _
                                   ByVal xCompanyUnkid As Integer) As Boolean
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception
        Dim objDataOperation As clsDataOperation = Nothing

        Dim objConfig As New clsConfigOptions
        Dim strLoanTrancheApplicationPrifix As String = objConfig.GetKeyValue(xCompanyUnkid, "LoanTrancheApplicationPrefix", Nothing)
        Dim mstrUserAccessModeSetting As String = objConfig.GetKeyValue(xCompanyUnkid, "UserAccessModeSetting", Nothing)
        Dim mstrDocumentPath As String = objConfig.GetKeyValue(xCompanyUnkid, "DocumentPath", Nothing)
        objConfig = Nothing

        objDataOperation = New clsDataOperation
        objDataOperation.BindTransaction()

        Try
            objDataOperation.ClearParameters()
            objDataOperation.AddParameter("@processpendingloanunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintProcesspendingloanunkid.ToString)
            objDataOperation.AddParameter("@employeeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintEmployeeunkid.ToString)
            objDataOperation.AddParameter("@loanschemeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintLoanschemeunkid.ToString)
            objDataOperation.AddParameter("@applicationno", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrApplicationno.ToString)
            objDataOperation.AddParameter("@tranchedate", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, mdtTranchedate.ToString)
            objDataOperation.AddParameter("@trancheamt", SqlDbType.Decimal, eZeeDataType.DECIMAL_SIZE, mdecTrancheamt.ToString)
            objDataOperation.AddParameter("@remark", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrRemark.ToString)
            objDataOperation.AddParameter("@statusunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintStatusunkid.ToString)
            objDataOperation.AddParameter("@countryunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintCountryunkid.ToString)
            objDataOperation.AddParameter("@deductionperiodunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintDeductionPeriodunkid.ToString)
            objDataOperation.AddParameter("@finaldeductionperiodunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintFinalDeductionPeriodunkid.ToString)
            objDataOperation.AddParameter("@finalapproverunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintFinalapproverunkid.ToString)
            objDataOperation.AddParameter("@approved_trancheamt", SqlDbType.Decimal, eZeeDataType.DECIMAL_SIZE, mdecApproved_Trancheamt.ToString)

            If IsDBNull(mdtSendEmpReminderForNextTrance) = False AndAlso mdtSendEmpReminderForNextTrance <> Nothing Then
                objDataOperation.AddParameter("@sendempreminderfornexttranche", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, mdtSendEmpReminderForNextTrance.ToString)
            Else
                objDataOperation.AddParameter("@sendempreminderfornexttranche", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, DBNull.Value)
            End If

            objDataOperation.AddParameter("@ntfsendempfornexttranche", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnNtfSendEmpForNextTranche.ToString)
            objDataOperation.AddParameter("@userunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintUserunkid.ToString)
            objDataOperation.AddParameter("@loginemployeeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintLoginemployeeunkid.ToString)
            objDataOperation.AddParameter("@voidloginemployeeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintVoidloginemployeeunkid.ToString)
            objDataOperation.AddParameter("@isvoid", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsvoid.ToString)

            If IsDBNull(mdtVoiddatetime) = False AndAlso mdtVoiddatetime <> Nothing Then
                objDataOperation.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, mdtVoiddatetime.ToString)
            Else
                objDataOperation.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, DBNull.Value)
            End If

            objDataOperation.AddParameter("@voiduserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintVoiduserunkid.ToString)
            objDataOperation.AddParameter("@voidreason", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrVoidreason.ToString)

            'Pinkal (16-Nov-2023) -- Start
            '(A1X-1489) NMB - Mortgage tranche disbursement API.
            objDataOperation.AddParameter("@fxcube_posteddata", SqlDbType.NVarChar, eZeeDataType.DESC_SIZE, mstrPostedData)
            objDataOperation.AddParameter("@fxcube_responsedata", SqlDbType.NVarChar, eZeeDataType.DESC_SIZE, mstrResponseData)
            objDataOperation.AddParameter("@isfxcube_error", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnPostedError)
            'Pinkal (16-Nov-2023) -- End

            strQ = "INSERT INTO lnloantranche_request_tran ( " & _
                      "  processpendingloanunkid " & _
                      ", employeeunkid " & _
                      ", loanschemeunkid " & _
                      ", applicationno " & _
                      ", tranchedate " & _
                      ", trancheamt " & _
                      ", remark " & _
                      ", statusunkid " & _
                      ", countryunkid " & _
                      ", deductionperiodunkid " & _
                      ", finaldeductionperiodunkid " & _
                      ", finalapproverunkid " & _
                      ", approved_trancheamt " & _
                      ", sendempreminderfornexttranche " & _
                      ", ntfsendempfornexttranche " & _
                      ", userunkid " & _
                      ", loginemployeeunkid " & _
                      ", voidloginemployeeunkid " & _
                      ", isvoid " & _
                      ", voiddatetime " & _
                      ", voiduserunkid " & _
                      ", voidreason" & _
                      ", fxcube_posteddata " & _
                      ", fxcube_responsedata " & _
                      ", isfxcube_error " & _
                    ") VALUES (" & _
                      "  @processpendingloanunkid " & _
                      ", @employeeunkid " & _
                      ", @loanschemeunkid " & _
                      ", @applicationno " & _
                      ", @tranchedate " & _
                      ", @trancheamt " & _
                      ", @remark " & _
                      ", @statusunkid " & _
                      ", @countryunkid " & _
                      ", @deductionperiodunkid " & _
                      ", @finaldeductionperiodunkid " & _
                      ", @finalapproverunkid " & _
                      ", @approved_trancheamt " & _
                      ", @sendempreminderfornexttranche " & _
                      ", @ntfsendempfornexttranche " & _
                      ", @userunkid " & _
                      ", @loginemployeeunkid " & _
                      ", @voidloginemployeeunkid " & _
                      ", @isvoid " & _
                      ", @voiddatetime " & _
                      ", @voiduserunkid " & _
                      ", @voidreason" & _
                      ", @fxcube_posteddata " & _
                      ", @fxcube_responsedata " & _
                      ", @isfxcube_error " & _
                    "); SELECT @@identity"


            'Pinkal (16-Nov-2023) -- (A1X-1489) NMB - Mortgage tranche disbursement API.[@fxcube_posteddata , @fxcube_responsedata , @isfxcube_error " ]

            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            mintLoantrancherequestunkid = dsList.Tables(0).Rows(0).Item(0)

            If Set_AutoNumber(objDataOperation, mintLoantrancherequestunkid, "lnloantranche_request_tran", "applicationno", "loantrancherequestunkid", "NextLoanTrancheApplicationNo", strLoanTrancheApplicationPrifix, xCompanyUnkid) = False Then
                If objDataOperation.ErrorMessage <> "" Then
                    objDataOperation.ReleaseTransaction(False)
                    Return False
                End If
            End If
            If Get_Saved_Number(objDataOperation, mintLoantrancherequestunkid, "lnloantranche_request_tran", "applicationno", "loantrancherequestunkid", mstrApplicationno) = False Then
                If objDataOperation.ErrorMessage <> "" Then
                    exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                    Throw exForce
                End If
            End If

            If InsertAuditTrailForLoanTrancheRequest(objDataOperation, enAuditType.ADD) = False Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            'If mdtDocument IsNot Nothing Then
            '    For Each drow As DataRow In mdtDocument.Rows
            '        drow("transactionunkid") = mintLoantrancherequestunkid
            '        drow.AcceptChanges()
            '    Next

            '    Dim objDocument As New clsScan_Attach_Documents
            '    objDocument._Datatable = mdtDocument
            '    objDocument.InsertUpdateDelete_Documents(objDataOperation)

            '    If objDataOperation.ErrorMessage <> "" Then
            '        exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
            '        Throw exForce
            '    End If
            '    objDocument = Nothing
            'End If

            If mdtDocument IsNot Nothing AndAlso mdtDocument.Rows.Count > 0 Then
                Dim objDocument As New clsScan_Attach_Documents
                Dim dtTran As DataTable = objDocument._Datatable
                Dim docLocalPath As String = mstrDocumentPath & "\"

                Dim strFolderName As String = (New clsScan_Attach_Documents).GetDocFolderName("Docs", enScanAttactRefId.LOAN_TRANCHE).Tables(0).Rows(0)("Name").ToString
                If strFolderName IsNot Nothing Then strFolderName = strFolderName & "\"
                Dim dr As DataRow
                For Each drow As DataRow In mdtDocument.Rows
                    dr = dtTran.NewRow
                    dr("scanattachtranunkid") = drow("scanattachtranunkid")
                    dr("documentunkid") = drow("documentunkid")
                    dr("employeeunkid") = drow("employeeunkid")
                    dr("filename") = drow("filename")
                    dr("scanattachrefid") = drow("scanattachrefid")
                    dr("modulerefid") = drow("modulerefid")
                    dr("form_name") = drow("form_name")
                    dr("userunkid") = drow("userunkid")
                    dr("transactionunkid") = mintLoantrancherequestunkid
                    dr("attached_date") = drow("attached_date")
                    dr("orgfilepath") = drow("localpath")
                    dr("destfilepath") = docLocalPath & strFolderName & CStr(drow.Item("filename"))
                    dr("AUD") = drow("AUD")
                    dr("userunkid") = mintVoiduserunkid
                    dr("fileuniquename") = drow("fileuniquename")
                    dr("filepath") = drow("filepath")
                    dr("filesize") = drow("filesize_kb")
                    dr("file_data") = drow("file_data")
                    dtTran.Rows.Add(dr)
                Next
                objDocument._Datatable = dtTran
                objDocument.InsertUpdateDelete_Documents(objDataOperation)

                If objDataOperation.ErrorMessage <> "" Then
                    exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                    Throw exForce
                End If
                objDocument = Nothing
            End If


            Dim objLoanScheme As New clsloanscheme_role_mapping
            Dim dtTable As DataTable = objLoanScheme.GetNextApprovers(xDatabaseName, xCompanyUnkid, xYearUnkid, mstrUserAccessModeSetting, enUserPriviledge.AllowToApproveLoan, mdtTranchedate, xUserUnkid, mintEmployeeunkid _
                                                                                                            , mintLoanschemeunkid, False, mdecTrancheamt, True, objDataOperation, "")

            Dim objLoanTrancheApproval As New clsloanTranche_approval_Tran
            Dim blnEnableVisibility As Boolean = False
            Dim intMinPriority As Integer = -1
            Dim intApproverID As Integer = -1

            If dtTable IsNot Nothing AndAlso dtTable.Rows.Count > 0 Then

                For Each drRow As DataRow In dtTable.Rows
                    objLoanTrancheApproval._Loantrancherequestunkid = mintLoantrancherequestunkid
                    objLoanTrancheApproval._Processpendingloanunkid = mintProcesspendingloanunkid
                    objLoanTrancheApproval._Employeeunkid = mintEmployeeunkid
                    objLoanTrancheApproval._Roleunkid = CInt(drRow("roleunkid"))
                    objLoanTrancheApproval._Levelunkid = CInt(drRow("levelunkid"))
                    objLoanTrancheApproval._Mappingunkid = CInt(drRow("mappingunkid"))
                    objLoanTrancheApproval._Priority = CInt(drRow("priority"))
                    objLoanTrancheApproval._Approvaldate = Nothing
                    objLoanTrancheApproval._Deductionperiodunkid = mintDeductionPeriodunkid
                    objLoanTrancheApproval._Loantranche_Amount = mdecTrancheamt
                    objLoanTrancheApproval._Countryunkid = mintCountryunkid
                    objLoanTrancheApproval._Mapuserunkid = CInt(drRow("mapuserunkid"))
                    objLoanTrancheApproval._Statusunkid = mintStatusunkid
                    objLoanTrancheApproval._Remark = ""
                    objLoanTrancheApproval._Userunkid = mintUserunkid
                    objLoanTrancheApproval._AuditUserId = mintUserunkid

                    If mstrFormName.Trim.Length <= 0 Then
                        objLoanTrancheApproval._IsFromWeb = False
                        objLoanTrancheApproval._ClientIP = getIP()
                        objLoanTrancheApproval._FormName = mstrFormName
                        objLoanTrancheApproval._HostName = getHostName()
                    Else
                        objLoanTrancheApproval._IsFromWeb = True
                        objLoanTrancheApproval._ClientIP = mstrClientIP
                        objLoanTrancheApproval._FormName = mstrFormName
                        objLoanTrancheApproval._HostName = mstrHostName
                    End If

                    intMinPriority = CInt(dtTable.Compute("MIN(priority)", "1=1"))

                    If intMinPriority = CInt(drRow("priority")) Then
                        If mintUserunkid = CInt(drRow("mapuserunkid")) Then
                            objLoanTrancheApproval._Statusunkid = enLoanApplicationStatus.APPROVED
                            blnEnableVisibility = True
                        End If

                        If blnEnableVisibility = True Then
                            objLoanTrancheApproval._Visibleunkid = enLoanApplicationStatus.APPROVED
                        Else
                            Dim dRow As DataRow() = dtTable.Select("priority=" & intMinPriority & " AND mapuserunkid=" & mintUserunkid & "")
                            If dRow.Length > 0 Then
                                objLoanTrancheApproval._Visibleunkid = enLoanApplicationStatus.APPROVED
                            Else
                                objLoanTrancheApproval._Visibleunkid = mintStatusunkid
                            End If
                        End If
                    Else
                        If blnEnableVisibility = True Then
                            Dim intNextMinPriority As Integer = CInt(dtTable.Compute("MIN(priority)", "priority > " & intMinPriority))
                            If intNextMinPriority = CInt(drRow("priority")) Then
                                objLoanTrancheApproval._Visibleunkid = enLoanApplicationStatus.PENDING
                            Else
                                objLoanTrancheApproval._Visibleunkid = -1
                            End If
                        Else
                            objLoanTrancheApproval._Visibleunkid = -1
                        End If

                    End If

                    If objLoanTrancheApproval.Insert(objDataOperation) = False Then
                        exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                        Throw exForce
                    End If
                Next

            End If

            objLoanTrancheApproval = Nothing
            objLoanScheme = Nothing

            objDataOperation.ReleaseTransaction(True)

            Return True
        Catch ex As Exception
            objDataOperation.ReleaseTransaction(False)
            Throw New Exception(ex.Message & "; Procedure Name: Insert; Module Name: " & mstrModuleName)
            Return False
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
    End Function

    ''' <summary>
    ''' Modify By: Pinkal
    ''' </summary>
    ''' <returns>Boolean</returns>
    ''' <purpose> Update Database Table (lnloantranche_request_tran) </purpose>
    Public Function Update(ByVal xCompanyUnkid As Integer, Optional ByVal isFromApplication As Boolean = True, Optional ByVal objDoOperation As clsDataOperation = Nothing) As Boolean
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception
        Dim objDataOperation As clsDataOperation = Nothing

        Dim objConfig As New clsConfigOptions
        Dim mstrDocumentPath As String = objConfig.GetKeyValue(xCompanyUnkid, "DocumentPath", Nothing)
        objConfig = Nothing

        If objDoOperation Is Nothing Then
            objDataOperation = New clsDataOperation
            objDataOperation.BindTransaction()
        Else
            objDataOperation = objDoOperation
        End If


        Try
            objDataOperation.ClearParameters()
            objDataOperation.AddParameter("@loantrancherequestunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintLoantrancherequestunkid.ToString)
            objDataOperation.AddParameter("@processpendingloanunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintProcesspendingloanunkid.ToString)
            objDataOperation.AddParameter("@employeeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintEmployeeunkid.ToString)
            objDataOperation.AddParameter("@loanschemeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintLoanschemeunkid.ToString)
            objDataOperation.AddParameter("@applicationno", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrApplicationno.ToString)
            objDataOperation.AddParameter("@tranchedate", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, mdtTranchedate.ToString)
            objDataOperation.AddParameter("@trancheamt", SqlDbType.Decimal, eZeeDataType.DECIMAL_SIZE, mdecTrancheamt.ToString)
            objDataOperation.AddParameter("@remark", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrRemark.ToString)
            objDataOperation.AddParameter("@statusunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintStatusunkid.ToString)
            objDataOperation.AddParameter("@countryunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintCountryunkid.ToString)
            objDataOperation.AddParameter("@deductionperiodunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintDeductionPeriodunkid.ToString)
            objDataOperation.AddParameter("@finaldeductionperiodunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintFinalDeductionPeriodunkid.ToString)
            objDataOperation.AddParameter("@finalapproverunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintFinalapproverunkid.ToString)
            objDataOperation.AddParameter("@approved_trancheamt", SqlDbType.Decimal, eZeeDataType.DECIMAL_SIZE, mdecApproved_Trancheamt.ToString)

            If IsDBNull(mdtSendEmpReminderForNextTrance) = False AndAlso mdtSendEmpReminderForNextTrance <> Nothing Then
                objDataOperation.AddParameter("@sendempreminderfornexttranche", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, mdtSendEmpReminderForNextTrance.ToString)
            Else
                objDataOperation.AddParameter("@sendempreminderfornexttranche", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, DBNull.Value)
            End If

            objDataOperation.AddParameter("@ntfsendempfornexttranche", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnNtfSendEmpForNextTranche.ToString)
            objDataOperation.AddParameter("@userunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintUserunkid.ToString)
            objDataOperation.AddParameter("@loginemployeeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintLoginemployeeunkid.ToString)
            objDataOperation.AddParameter("@voidloginemployeeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintVoidloginemployeeunkid.ToString)
            objDataOperation.AddParameter("@isvoid", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsvoid.ToString)

            If IsDBNull(mdtVoiddatetime) = False AndAlso mdtVoiddatetime <> Nothing Then
                objDataOperation.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, mdtVoiddatetime.ToString)
            Else
                objDataOperation.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, DBNull.Value)
            End If

            objDataOperation.AddParameter("@voiduserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintVoiduserunkid.ToString)
            objDataOperation.AddParameter("@voidreason", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrVoidreason.ToString)


            'Pinkal (16-Nov-2023) -- Start
            '(A1X-1489) NMB - Mortgage tranche disbursement API.
            objDataOperation.AddParameter("@fxcube_posteddata", SqlDbType.NVarChar, eZeeDataType.DESC_SIZE, mstrPostedData)
            objDataOperation.AddParameter("@fxcube_responsedata", SqlDbType.NVarChar, eZeeDataType.DESC_SIZE, mstrResponseData)
            objDataOperation.AddParameter("@isfxcube_error", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnPostedError)
            'Pinkal (16-Nov-2023) -- End

            strQ = "UPDATE lnloantranche_request_tran SET " & _
                      "  processpendingloanunkid = @processpendingloanunkid" & _
                      ", employeeunkid = @employeeunkid" & _
                      ", loanschemeunkid = @loanschemeunkid" & _
                      ", applicationno = @applicationno" & _
                      ", tranchedate = @tranchedate" & _
                      ", trancheamt = @trancheamt" & _
                      ", remark = @remark" & _
                      ", statusunkid = @statusunkid " & _
                      ", countryunkid = @countryunkid " & _
                      ", deductionperiodunkid = @deductionperiodunkid " & _
                      ", finaldeductionperiodunkid = @finaldeductionperiodunkid " & _
                      ", finalapproverunkid = @finalapproverunkid" & _
                      ", approved_trancheamt = @approved_trancheamt" & _
                      ", sendempreminderfornexttranche = @sendempreminderfornexttranche " & _
                      ", ntfsendempfornexttranche = @ntfsendempfornexttranche " & _
                      ", userunkid = @userunkid" & _
                      ", loginemployeeunkid = @loginemployeeunkid" & _
                      ", voidloginemployeeunkid = @voidloginemployeeunkid" & _
                      ", isvoid = @isvoid" & _
                      ", voiddatetime = @voiddatetime" & _
                      ", voiduserunkid = @voiduserunkid" & _
                      ", voidreason = @voidreason " & _
                      ", fxcube_posteddata = @fxcube_posteddata " & _
                      ", fxcube_responsedata = @fxcube_responsedata " & _
                      ", isfxcube_error = @isfxcube_error " & _
                      " WHERE isvoid = 0 AND loantrancherequestunkid = @loantrancherequestunkid "

            'Pinkal (16-Nov-2023) -- (A1X-1489) NMB - Mortgage tranche disbursement API.[fxcube_posteddata = @fxcube_posteddata , fxcube_responsedata = @fxcube_responsedata , isfxcube_error = @isfxcube_error " ]

            Call objDataOperation.ExecNonQuery(strQ)

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            If InsertAuditTrailForLoanTrancheRequest(objDataOperation, enAuditType.EDIT) = False Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            'If mdtDocument IsNot Nothing Then
            '    For Each drow As DataRow In mdtDocument.Rows
            '        drow("transactionunkid") = mintLoantrancherequestunkid
            '        drow.AcceptChanges()
            '    Next

            '    Dim objDocument As New clsScan_Attach_Documents
            '    objDocument._Datatable = mdtDocument
            '    objDocument.InsertUpdateDelete_Documents(objDataOperation)

            '    If objDataOperation.ErrorMessage <> "" Then
            '        exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
            '        Throw exForce
            '    End If
            '    objDocument = Nothing
            'End If

            If mdtDocument IsNot Nothing Then
                Dim objDocument As New clsScan_Attach_Documents
                Dim dtTran As DataTable = objDocument._Datatable
                Dim docLocalPath As String = mstrDocumentPath & "\"

                Dim strFolderName As String = (New clsScan_Attach_Documents).GetDocFolderName("Docs", enScanAttactRefId.LOAN_TRANCHE).Tables(0).Rows(0)("Name").ToString
                If strFolderName IsNot Nothing Then strFolderName = strFolderName & "\"
                Dim dr As DataRow
                For Each drow As DataRow In mdtDocument.Rows
                    dr = dtTran.NewRow
                    dr("scanattachtranunkid") = drow("scanattachtranunkid")
                    dr("documentunkid") = drow("documentunkid")
                    dr("employeeunkid") = drow("employeeunkid")
                    dr("filename") = drow("filename")
                    dr("scanattachrefid") = drow("scanattachrefid")
                    dr("modulerefid") = drow("modulerefid")
                    dr("form_name") = drow("form_name")
                    dr("userunkid") = drow("userunkid")
                    dr("transactionunkid") = mintLoantrancherequestunkid
                    dr("attached_date") = drow("attached_date")
                    dr("orgfilepath") = drow("localpath")
                    dr("destfilepath") = docLocalPath & strFolderName & CStr(drow.Item("filename"))
                    dr("AUD") = drow("AUD")
                    dr("userunkid") = mintUserunkid
                    dr("fileuniquename") = drow("fileuniquename")
                    dr("filepath") = drow("filepath")
                    dr("filesize") = drow("filesize_kb")
                    dr("file_data") = drow("file_data")
                    dtTran.Rows.Add(dr)
                Next
                objDocument._Datatable = dtTran
                objDocument.InsertUpdateDelete_Documents(objDataOperation)

                If objDataOperation.ErrorMessage <> "" Then
                    exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                    Throw exForce
                End If
            End If

            If isFromApplication Then
                Dim objLoanTrancheApproval As New clsloanTranche_approval_Tran
                objLoanTrancheApproval._Loantrancherequestunkid = mintLoantrancherequestunkid
                objLoanTrancheApproval._Employeeunkid = mintEmployeeunkid
                objLoanTrancheApproval._Deductionperiodunkid = mintDeductionPeriodunkid
                objLoanTrancheApproval._Loantranche_Amount = mdecTrancheamt
                objLoanTrancheApproval._Remark = mstrRemark
                objLoanTrancheApproval._AuditUserId = mintUserunkid

                If mstrFormName.Trim.Length <= 0 Then
                    objLoanTrancheApproval._IsFromWeb = False
                    objLoanTrancheApproval._ClientIP = getIP()
                    objLoanTrancheApproval._FormName = mstrFormName
                    objLoanTrancheApproval._HostName = getHostName()
                Else
                    objLoanTrancheApproval._IsFromWeb = True
                    objLoanTrancheApproval._ClientIP = mstrClientIP
                    objLoanTrancheApproval._FormName = mstrFormName
                    objLoanTrancheApproval._HostName = mstrHostName
                End If
                If objLoanTrancheApproval.UpdateLoanTrancheApplication(objDataOperation) = False Then
                    exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                    Throw exForce
                End If
                objLoanTrancheApproval = Nothing
            End If

            If objDoOperation Is Nothing Then objDataOperation.ReleaseTransaction(True)

            Return True
        Catch ex As Exception
            If objDoOperation Is Nothing Then objDataOperation.ReleaseTransaction(False)
            Throw New Exception(ex.Message & "; Procedure Name: Update; Module Name: " & mstrModuleName)
            Return False
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            If objDoOperation Is Nothing Then objDataOperation = Nothing
        End Try
    End Function

    ''' <summary>
    ''' Modify By: Pinkal
    ''' </summary>
    ''' <returns>Boolean</returns>
    ''' <purpose> Delete Database Table (lnloantranche_request_tran) </purpose>
    Public Function Delete(ByVal xCompanyUnkid As Integer, ByVal intUnkid As Integer) As Boolean
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception
        Dim objDataOperation As clsDataOperation = Nothing

        Dim objConfig As New clsConfigOptions
        Dim mstrDocumentPath As String = objConfig.GetKeyValue(xCompanyUnkid, "DocumentPath", Nothing)
        objConfig = Nothing

        objDataOperation = New clsDataOperation
        objDataOperation.BindTransaction()


        Try

            strQ = "SELECT ISNULL(loantrancheapprovalunkid,0) AS loantrancheapprovalunkid FROM lnloantranche_approval_tran  WHERE loantrancherequestunkid = @loantrancherequestunkid AND isvoid =0 "
            objDataOperation.ClearParameters()
            objDataOperation.AddParameter("@loantrancherequestunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intUnkid)
            dsList = objDataOperation.ExecQuery(strQ, "List")

            If dsList IsNot Nothing AndAlso dsList.Tables(0).Rows.Count > 0 Then
                Dim objAppoval As New clsloanTranche_approval_Tran
                For Each dr As DataRow In dsList.Tables(0).Rows
                    objAppoval._Loantrancheapprovalunkid(objDataOperation) = CInt(dr("loantrancheapprovalunkid"))
                    objAppoval._Isvoid = True
                    objAppoval._Voiduserunkid = mintVoiduserunkid
                    objAppoval._Voidreason = mstrVoidreason
                    objAppoval._ClientIP = mstrClientIP
                    objAppoval._HostName = mstrHostName
                    objAppoval._FormName = mstrFormName
                    objAppoval._IsFromWeb = mblnIsFromWeb
                    objAppoval._Voidreason = mstrVoidreason
                    If objAppoval.Delete(CInt(dr("loantrancheapprovalunkid")), objDataOperation) = False Then
                        exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                        Throw exForce
                    End If
                Next
                objAppoval = Nothing
            End If


            strQ = " UPDATE lnloantranche_request_tran SET " & _
                      "  isvoid = @isvoid" & _
                      ", voidloginemployeeunkid = @voidloginemployeeunkid" & _
                      ", voiddatetime = GETDATE() " & _
                      ", voidreason = @voidreason " & _
                      " WHERE loantrancherequestunkid = @loantrancherequestunkid "

            objDataOperation.ClearParameters()
            objDataOperation.AddParameter("@loantrancherequestunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intUnkid)
            objDataOperation.AddParameter("@isvoid", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsvoid.ToString)
            objDataOperation.AddParameter("@voidloginemployeeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintVoidloginemployeeunkid.ToString)
            objDataOperation.AddParameter("@voidreason", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrVoidreason.ToString)
            Call objDataOperation.ExecNonQuery(strQ)

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            If InsertAuditTrailForLoanTrancheRequest(objDataOperation, enAuditType.DELETE) = False Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            'If mdtDocument IsNot Nothing AndAlso mdtDocument.Rows.Count > 0 Then
            '    Dim objDocument As New clsScan_Attach_Documents
            '    For Each iRow As DataRow In mdtDocument.Rows
            '        iRow("AUD") = "D"
            '    Next
            '    objDocument._Datatable = mdtDocument
            '    objDocument.InsertUpdateDelete_Documents(objDataOperation)
            '    If objDataOperation.ErrorMessage <> "" Then
            '        exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
            '        Throw exForce
            '    End If
            '    objDocument = Nothing
            'End If

            If mdtDocument IsNot Nothing Then
                Dim objDocument As New clsScan_Attach_Documents
                Dim dtTran As DataTable = objDocument._Datatable
                Dim docLocalPath As String = mstrDocumentPath & "\"

                Dim strFolderName As String = (New clsScan_Attach_Documents).GetDocFolderName("Docs", enScanAttactRefId.LOAN_TRANCHE).Tables(0).Rows(0)("Name").ToString
                If strFolderName IsNot Nothing Then strFolderName = strFolderName & "\"
                Dim dr As DataRow
                For Each drow As DataRow In mdtDocument.Rows
                    dr = dtTran.NewRow
                    dr("scanattachtranunkid") = drow("scanattachtranunkid")
                    dr("documentunkid") = drow("documentunkid")
                    dr("employeeunkid") = drow("employeeunkid")
                    dr("filename") = drow("filename")
                    dr("scanattachrefid") = drow("scanattachrefid")
                    dr("modulerefid") = drow("modulerefid")
                    dr("form_name") = drow("form_name")
                    dr("userunkid") = drow("userunkid")
                    dr("transactionunkid") = drow("transactionunkid")
                    dr("attached_date") = drow("attached_date")
                    dr("orgfilepath") = drow("localpath")
                    dr("destfilepath") = docLocalPath & strFolderName & CStr(drow.Item("filename"))
                    dr("AUD") = "D"
                    dr("userunkid") = mintUserunkid
                    dr("fileuniquename") = drow("fileuniquename")
                    dr("filepath") = drow("filepath")
                    dr("filesize") = drow("filesize_kb")
                    dr("file_data") = drow("file_data")
                    dtTran.Rows.Add(dr)
                Next
                objDocument._Datatable = dtTran
                objDocument.InsertUpdateDelete_Documents(objDataOperation)

                If objDataOperation.ErrorMessage <> "" Then
                    exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                    Throw exForce
                End If
            End If

            objDataOperation.ReleaseTransaction(True)

            Return True
        Catch ex As Exception
            objDataOperation.ReleaseTransaction(False)
            Throw New Exception(ex.Message & "; Procedure Name: Delete; Module Name: " & mstrModuleName)
            Return False
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
    End Function

    ''' <summary>
    ''' Modify By: Pinkal
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>
    Public Function isUsed(ByVal intUnkid As Integer) As Boolean
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        objDataOperation = New clsDataOperation

        Try
            strQ = "<Query>"

            objDataOperation.AddParameter("@loantrancherequestunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intUnkid)

            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            Return dsList.Tables(0).Rows.Count > 0
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: isUsed; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
    End Function

    ''' <summary>
    ''' Modify By: Pinkal
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>
    Public Function InsertAuditTrailForLoanTrancheRequest(ByVal objDataOperation As clsDataOperation, ByVal intAuditType As Integer) As Boolean
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception
        Try
            objDataOperation.ClearParameters()
            objDataOperation.AddParameter("@loantrancherequestunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintLoantrancherequestunkid.ToString)
            objDataOperation.AddParameter("@processpendingloanunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintProcesspendingloanunkid.ToString)
            objDataOperation.AddParameter("@employeeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintEmployeeunkid.ToString)
            objDataOperation.AddParameter("@loanschemeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintLoanschemeunkid.ToString)
            objDataOperation.AddParameter("@applicationno", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrApplicationno.ToString)
            objDataOperation.AddParameter("@tranchedate", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, mdtTranchedate.ToString)
            objDataOperation.AddParameter("@trancheamt", SqlDbType.Decimal, eZeeDataType.DECIMAL_SIZE, mdecTrancheamt.ToString)
            objDataOperation.AddParameter("@remark", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrRemark.ToString)
            objDataOperation.AddParameter("@statusunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintStatusunkid.ToString)
            objDataOperation.AddParameter("@countryunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintCountryunkid.ToString)
            objDataOperation.AddParameter("@deductionperiodunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintDeductionPeriodunkid.ToString)
            objDataOperation.AddParameter("@finaldeductionperiodunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintFinalDeductionPeriodunkid.ToString)
            objDataOperation.AddParameter("@finalapproverunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintFinalapproverunkid.ToString)
            objDataOperation.AddParameter("@approved_trancheamt", SqlDbType.Decimal, eZeeDataType.DECIMAL_SIZE, mdecApproved_Trancheamt.ToString)

            If IsDBNull(mdtSendEmpReminderForNextTrance) = False AndAlso mdtSendEmpReminderForNextTrance <> Nothing Then
                objDataOperation.AddParameter("@sendempreminderfornexttranche", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, mdtSendEmpReminderForNextTrance.ToString)
            Else
                objDataOperation.AddParameter("@sendempreminderfornexttranche", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, DBNull.Value)
            End If

            objDataOperation.AddParameter("@ntfsendempfornexttranche", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnNtfSendEmpForNextTranche.ToString)
            objDataOperation.AddParameter("@loginemployeeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintLoginemployeeunkid.ToString)
            objDataOperation.AddParameter("@audittype", SqlDbType.Int, eZeeDataType.INT_SIZE, intAuditType.ToString)
            objDataOperation.AddParameter("@audituserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintAuditUserId)
            objDataOperation.AddParameter("@ip", SqlDbType.NVarChar, eZeeDataType.IP_SIZE, mstrClientIP)
            objDataOperation.AddParameter("@machine_name", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrHostName)
            objDataOperation.AddParameter("@form_name", SqlDbType.NVarChar, 500, mstrFormName)
            objDataOperation.AddParameter("@isweb", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsFromWeb)


            'Pinkal (16-Nov-2023) -- Start
            '(A1X-1489) NMB - Mortgage tranche disbursement API.
            objDataOperation.AddParameter("@fxcube_posteddata", SqlDbType.NVarChar, eZeeDataType.DESC_SIZE, mstrPostedData)
            objDataOperation.AddParameter("@fxcube_responsedata", SqlDbType.NVarChar, eZeeDataType.DESC_SIZE, mstrResponseData)
            objDataOperation.AddParameter("@isfxcube_error", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnPostedError)
            'Pinkal (16-Nov-2023) -- End

            strQ = "INSERT INTO atlnloantranche_request_tran ( " & _
                      "  loantrancherequestunkid " & _
                      ", processpendingloanunkid " & _
                      ", employeeunkid " & _
                      ", loanschemeunkid " & _
                      ", applicationno " & _
                      ", tranchedate " & _
                      ", trancheamt " & _
                      ", remark " & _
                      ", statusunkid " & _
                      ", countryunkid " & _
                      ", deductionperiodunkid " & _
                      ", finaldeductionperiodunkid " & _
                      ", finalapproverunkid " & _
                      ", approved_trancheamt " & _
                      ", sendempreminderfornexttranche " & _
                      ", ntfsendempfornexttranche " & _
                      ", loginemployeeunkid " & _
                      ", audittype " & _
                      ", audituserunkid " & _
                      ", auditdatetime " & _
                      ", ip " & _
                      ", machine_name" & _
                      ", form_name " & _
                      ", isweb " & _
                      ", fxcube_posteddata " & _
                      ", fxcube_responsedata " & _
                      ", isfxcube_error " & _
                    ") VALUES (" & _
                      "  @loantrancherequestunkid " & _
                      ", @processpendingloanunkid " & _
                      ", @employeeunkid " & _
                      ", @loanschemeunkid " & _
                      ", @applicationno " & _
                      ", @tranchedate " & _
                      ", @trancheamt " & _
                      ", @remark " & _
                      ", @statusunkid " & _
                      ", @countryunkid " & _
                      ", @deductionperiodunkid " & _
                      ", @finaldeductionperiodunkid " & _
                      ", @finalapproverunkid " & _
                      ", @approved_trancheamt " & _
                      ", @sendempreminderfornexttranche " & _
                      ", @ntfsendempfornexttranche " & _
                      ", @loginemployeeunkid " & _
                      ", @audittype " & _
                      ", @audituserunkid " & _
                      ", GETDATE() " & _
                      ", @ip " & _
                      ", @machine_name" & _
                      ", @form_name " & _
                      ", @isweb " & _
                      ", @fxcube_posteddata " & _
                      ", @fxcube_responsedata " & _
                      ", @isfxcube_error " & _
                    "); SELECT @@identity"


            'Pinkal (16-Nov-2023) -- (A1X-1489) NMB - Mortgage tranche disbursement API.[ @fxcube_posteddata , @fxcube_responsedata , @isfxcube_error " ]

            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            Return True
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: InsertAuditTrailForLoanTrancheRequest; Module Name: " & mstrModuleName)
            Return False
        End Try
    End Function

    ''' <summary>
    ''' Modify By: Pinkal
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>
    ''' 
    Public Function GetMortgageLoanApplicationNo(ByVal xEmployeeId As Integer, Optional ByVal blnFlag As Boolean = True, Optional ByVal mstrFilter As String = "") As DataTable
        Dim dtTable As DataTable = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception
        Dim objDataOperation As clsDataOperation = Nothing
        Try

            If blnFlag = True Then
                strQ = "SELECT 0 AS processpendingloanunkid,@Select AS application_no,0 AS loanschemeunkid UNION  "
            End If

            strQ &= " SELECT " & _
                          "      lnloan_process_pending_loan.processpendingloanunkid " & _
                          ",     lnloan_process_pending_loan.application_no " & _
                          ",     lnloan_process_pending_loan.loanschemeunkid " & _
                          " FROM lnloan_process_pending_loan " & _
                          " LEFT JOIN lnloan_scheme_master ON lnloan_scheme_master.loanschemeunkid = lnloan_process_pending_loan.loanschemeunkid " & _
                          " AND lnloan_scheme_master.ispostingtoflexcube = 1 AND lnloan_scheme_master.isactive = 1 " & _
                          " WHERE lnloan_process_pending_loan.isvoid = 0 AND lnloan_process_pending_loan.employeeunkid = @employeeunkid " & _
                          " AND lnloan_scheme_master.loanschemecategory_id = @loanschemecategory_id  AND lnloan_process_pending_loan.loan_statusunkid = @loan_statusunkid "

            If mstrFilter.Trim.Length > 0 Then
                strQ &= " AND " & mstrFilter.Trim
            End If

            objDataOperation = New clsDataOperation
            objDataOperation.ClearParameters()
            objDataOperation.AddParameter("@Select", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 1, "Select"))
            objDataOperation.AddParameter("@employeeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, xEmployeeId)
            objDataOperation.AddParameter("@loanschemecategory_id", SqlDbType.Int, eZeeDataType.INT_SIZE, enLoanSchemeCategories.MORTGAGE)
            objDataOperation.AddParameter("@loan_statusunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, enLoanApplicationStatus.APPROVED)
            Dim dsList As DataSet = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            dtTable = dsList.Tables(0)

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: GetMortgageLoanApplicationNo; Module Name: " & mstrModuleName)
        End Try
        Return dtTable
    End Function

    ''' <summary>
    ''' Modify By: Pinkal
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>
    ''' 
    Public Function GetLoanTracheNo(ByVal xLoanApplicationNo As Integer) As DataTable
        Dim dtTable As DataTable = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception
        Dim objDataOperation As clsDataOperation = Nothing
        Try

            strQ = " SELECT loantrancherequestunkid,processpendingloanunkid " & _
                      " FROM lnloantranche_request_tran  " & _
                      " WHERE isvoid = 0 AND processpendingloanunkid = @processpendingloanunkid " & _
                      " AND statusunkid <>  @statusunkid "

            objDataOperation = New clsDataOperation
            objDataOperation.ClearParameters()
            objDataOperation.AddParameter("@processpendingloanunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, xLoanApplicationNo)
            objDataOperation.AddParameter("@statusunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, enLoanApplicationStatus.REJECTED)
            Dim dsList As DataSet = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            dtTable = dsList.Tables(0)

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: GetLoanTracheNo; Module Name: " & mstrModuleName)
        End Try
        Return dtTable
    End Function

    ''' <summary>
    ''' Modify By: Pinkal
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>
    ''' 
    Public Function GetLoanTracheAmount(ByVal xLoanApplicationNo As Integer, ByVal xFinalApproverId As Integer, ByVal xLoanTrancheNo As Integer) As DataTable
        Dim dtTable As DataTable = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception
        Dim objDataOperation As clsDataOperation = Nothing
        Try

            strQ = " SELECT l.processpendingloanunkid, lr.pendingloanaprovalunkid "

            If xLoanTrancheNo = 1 Then
                strQ &= ",first_tranche,first_tranchedate "

            ElseIf xLoanTrancheNo = 2 Then
                strQ &= ",second_tranche,second_tranchedate "

            ElseIf xLoanTrancheNo = 3 Then
                strQ &= ",third_tranche,third_tranchedate "

            ElseIf xLoanTrancheNo = 4 Then
                strQ &= ",fourth_tranche,fourth_tranchedate "

            ElseIf xLoanTrancheNo = 5 Then
                strQ &= ",fifth_tranche,fifth_tranchedate "

            End If

            strQ &= " FROM lnroleloanapproval_process_tran lr " & _
                        " LEFT JOIN lnloan_process_pending_loan l on lr.processpendingloanunkid = l.processpendingloanunkid AND l.isvoid = 0 AND l.loan_statusunkid = @statusunkid " & _
                        " WHERE lr.isvoid = 0 AND lr.processpendingloanunkid = @processpendingloanunkid " & _
                        " AND lr.statusunkid = @statusunkid AND lr.mapuserunkid = @mapuserunkid "

            objDataOperation = New clsDataOperation
            objDataOperation.ClearParameters()
            objDataOperation.AddParameter("@processpendingloanunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, xLoanApplicationNo)
            objDataOperation.AddParameter("@mapuserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, xFinalApproverId)
            objDataOperation.AddParameter("@statusunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, enLoanApplicationStatus.APPROVED)
            Dim dsList As DataSet = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            dtTable = dsList.Tables(0)

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: GetLoanTracheNo; Module Name: " & mstrModuleName)
        End Try
        Return dtTable
    End Function

    ''' <summary>
    ''' Modify By: Pinkal
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>
    ''' 
    Public Sub Send_NotificationLoanTranche_Approver(ByVal xDatabaseName As String, _
                                                                     ByVal xYearId As Integer, _
                                                                     ByVal xUserAccessMode As String, _
                                                                     ByVal dtLoanApplicationDate As Date, _
                                                                     ByVal intSchemeId As Integer, _
                                                                     ByVal iEmployeeId As Integer, _
                                                                     ByVal strUnkId As String, _
                                                                     ByVal strArutiSelfServiceURL As String, _
                                                                     ByVal intCompanyUnkid As Integer, _
                                                                     Optional ByVal iLoginTypeId As Integer = 0, _
                                                                     Optional ByVal iLoginEmployeeId As Integer = 0, _
                                                                     Optional ByVal iUserId As Integer = 0)

        Dim strLink As String = String.Empty
        Dim StrQ As String = String.Empty
        objDataOperation = New clsDataOperation
        Try

            Dim objNet As New clsNetConnectivity : If objNet._Conected = False Then Exit Sub

            Dim objLoanTrancheApproval As New clsloanTranche_approval_Tran
            Dim mstrSearch As String = "lta.loantrancherequestunkid in (" & strUnkId & ") AND ltr.loanschemeunkid = " & intSchemeId & " AND ltr.statusunkid = " & enLoanApplicationStatus.PENDING & " AND lta.statusunkid = " & enLoanApplicationStatus.PENDING & " AND lta.visibleunkid = " & enLoanApplicationStatus.PENDING
            Dim dsList As DataSet = objLoanTrancheApproval.GetLoanTrancheApprovalList(xDatabaseName, iUserId, xYearId, intCompanyUnkid, dtLoanApplicationDate, dtLoanApplicationDate, xUserAccessMode, True, False, "List", mstrSearch, "", False, Nothing)
            objLoanTrancheApproval = Nothing

            If dsList IsNot Nothing AndAlso dsList.Tables(0).Rows.Count <= 0 Then Exit Sub

            Dim strSubject As String = ""
            Dim strOtherLoanType As String = ""

            strSubject = Language.getMessage(mstrModuleName, 5, "Loan Tranche Request Review/Decision")

            Dim objMail As New clsSendMail
            For Each dtRow As DataRow In dsList.Tables(0).Rows

                If dtRow.Item("UserEmail") = "" Then Continue For
                Dim strMessage As String = ""
                Dim strContain As String = ""

                strLink = strArutiSelfServiceURL & "/Loan_Savings/New_Loan/Loan_Tranche_Approval_Application/wPg_LoanTrancheApproval.aspx?" & HttpUtility.UrlEncode(clsCrypto.Encrypt(dtRow.Item("loantrancheapprovalunkid").ToString & "|" & dtRow.Item("mapuserunkid").ToString & "|" & dtRow.Item("loantrancherequestunkid").ToString & "|" & intCompanyUnkid.ToString))

                strMessage = "<HTML> <BODY>"

                Dim info1 As System.Globalization.TextInfo = System.Globalization.CultureInfo.InvariantCulture.TextInfo
                strMessage &= Language.getMessage(mstrModuleName, 6, "Dear") & " " & info1.ToTitleCase(CStr(IIf(dtRow.Item("UserName").ToString().Trim.Length > 0, dtRow.Item("UserName").ToString().Trim, dtRow.Item("loginuser").ToString()))) & ", <BR><BR>"
                info1 = Nothing

                strMessage &= Language.getMessage(mstrModuleName, 7, "Request No") & " " & "<b>(" & dtRow("ApplicationNo").ToString() & ")</b>" & Language.getMessage(mstrModuleName, 8, " for") & "<b>" & " (" & dtRow("LoanScheme").ToString() & ")</b>" & _
                                       " " & Language.getMessage(mstrModuleName, 9, "I.F.O") & " " & "<b>(" & dtRow("Employee").ToString() & ")</b>" & " " & Language.getMessage(mstrModuleName, 10, "has been submitted for your review and approval/decline.")

                strMessage &= "<BR></BR><BR></BR>"

                strMessage &= Language.getMessage(mstrModuleName, 11, "Please click on the link below for the appropriate action") & " <BR></BR><a href='" & strLink & "'>" & strLink & "</a>"
                strMessage &= "<BR></BR><BR></BR>"
                strMessage &= Language.getMessage(mstrModuleName, 12, "Regards.")

                strMessage &= "<p><center style='padding-top:25px;font-size:10.0pt;font-family:&quot;Verdana&quot;,&quot;Sans-Serif&quot;;color:#333399;margin-left:0px;margin-right:0px;margin-top:0px;margin-bottom:10px'><b>&quot;POWERED BY ARUTI HR &amp; PAYROLL MANAGEMENT SOFTWARE.&quot;</b></center></p>"
                strMessage &= "</BODY></HTML>"

                objMail._Subject = strSubject
                objMail._Message = strMessage
                objMail._ToEmail = dtRow.Item("UserEmail")
                objMail._Message = strMessage
                If mstrFormName.Trim.Length > 0 Then
                    objMail._Form_Name = mstrFormName
                End If
                objMail._LogEmployeeUnkid = iLoginEmployeeId
                objMail._OperationModeId = iLoginTypeId
                objMail._UserUnkid = IIf(iUserId <= 0, User._Object._Userunkid, iUserId)
                objMail._SenderAddress = IIf(dtRow.Item("UserEmail").ToString = "", CStr(IIf(dtRow.Item("UserName").ToString().Trim.Length > 0, dtRow.Item("UserName").ToString().Trim, dtRow.Item("loginuser").ToString())), dtRow.Item("UserEmail").ToString)
                objMail._ModuleRefId = clsSendMail.enAT_VIEW_TYPE.LOAN_MGT
                If objMail.SendMail(intCompanyUnkid).ToString.Length > 0 Then
                    Continue For
                End If

            Next

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: Send_NotificationLoanTranche_Approver; Module Name: " & mstrModuleName)
        Finally
        End Try
    End Sub

    ''' <summary>
    ''' Modify By: Pinkal
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>
    ''' 
    Public Sub Send_NotificationLoanTranche_Employee(ByVal iEmployeeId As Integer, _
                                                                      ByVal iloanUnkId As Integer, _
                                                                      ByVal mstrLoanApplicationNo As String, _
                                                                      ByVal dtEmployeeAsOnDate As Date, _
                                                                      ByVal intCompanyUnkId As Integer, _
                                                                      ByVal mstrLoanScheme As String, _
                                                                      ByVal xLoanApplicationStatus As Integer, _
                                                                      Optional ByVal strRemark As String = "", _
                                                                      Optional ByVal iLoginTypeId As Integer = 0, _
                                                                      Optional ByVal iLoginEmployeeId As Integer = 0, _
                                                                      Optional ByVal iUserId As Integer = 0)

        Dim dtApprover As DataTable = Nothing
        Dim strLink As String = String.Empty
        Dim StrQ As String = String.Empty
        objDataOperation = New clsDataOperation
        Try
            Dim objNet As New clsNetConnectivity : If objNet._Conected = False Then Exit Sub
            Dim objEmp As New clsEmployee_Master

            objEmp._Employeeunkid(dtEmployeeAsOnDate) = iEmployeeId

            Dim objMail As New clsSendMail

            If objEmp._Email = "" Then Exit Sub

            Dim strMessage As String = ""
            Dim strSubject As String = ""

            Select Case xLoanApplicationStatus

                Case enLoanApplicationStatus.PENDING
                    strSubject = Language.getMessage(mstrModuleName, 13, "Loan Tranche Request Submission")

                Case enLoanApplicationStatus.APPROVED
                    strSubject = Language.getMessage(mstrModuleName, 14, "Loan Tranche Request Approval")

                Case enLoanApplicationStatus.REJECTED
                    strSubject = Language.getMessage(mstrModuleName, 15, "Loan Tranche Request Decline")

            End Select

            strMessage = "<HTML> <BODY>"

            Dim info1 As System.Globalization.TextInfo = System.Globalization.CultureInfo.InvariantCulture.TextInfo
            strMessage &= Language.getMessage(mstrModuleName, 6, "Dear") & " " & info1.ToTitleCase(objEmp._Firstname & "  " & objEmp._Surname) & ", <BR><BR>"
            info1 = Nothing

            If xLoanApplicationStatus = enLoanApplicationStatus.REJECTED Then
                strMessage &= Language.getMessage(mstrModuleName, 16, "Sorry") & ","
            End If

            strMessage &= Language.getMessage(mstrModuleName, 7, "Request No") & " " & "<b>(" & mstrLoanApplicationNo & ")</b> " & Language.getMessage(mstrModuleName, 8, " for") & " <b>(" & mstrLoanScheme & ")</b> "

            Select Case xLoanApplicationStatus
                Case enLoanApplicationStatus.PENDING
                    strMessage &= Language.getMessage(mstrModuleName, 17, "has been submitted successfully for review/analysis.")

                Case enLoanApplicationStatus.APPROVED
                    strMessage &= Language.getMessage(mstrModuleName, 18, "has been approved for disbursement.")

                Case enLoanApplicationStatus.REJECTED
                    strMessage &= Language.getMessage(mstrModuleName, 19, "has been declined due to") & " <b>" & strRemark & "</b>." & Language.getMessage(mstrModuleName, 20, "Kindly contact HR Support for further clarity.")

            End Select


            strMessage &= "<BR></BR><BR></BR>"
            strMessage &= Language.getMessage(mstrModuleName, 12, "Regards.")
            strMessage &= "<BR></BR><BR></BR><center>""POWERED BY ARUTI HR & PAYROLL MANAGEMENT SOFTWARE""</center>"
            strMessage &= "</BODY></HTML>"
            objMail._Subject = strSubject
            objMail._Message = strMessage
            objMail._ToEmail = objEmp._Email
            If iLoginTypeId <= 0 Then iLoginTypeId = enLogin_Mode.DESKTOP
            If mstrFormName.Trim.Length > 0 Then
                objMail._Form_Name = mstrFormName
            End If
            objMail._LogEmployeeUnkid = iLoginEmployeeId
            objMail._OperationModeId = iLoginTypeId
            objMail._UserUnkid = IIf(iUserId <= 0, User._Object._Userunkid, iUserId)
            objMail._SenderAddress = IIf(objEmp._Email = "", objEmp._Firstname & " " & objEmp._Surname, objEmp._Email)
            objMail._ModuleRefId = clsSendMail.enAT_VIEW_TYPE.LOAN_MGT
            objMail.SendMail(intCompanyUnkId)
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: Send_NotificationLoanTranche_Employee; Module Name: " & mstrModuleName)
        Finally
        End Try
    End Sub

    'Pinkal (16-Nov-2023) -- Start
    '(A1X-1489) NMB - Mortgage tranche disbursement API.

    ''' <summary>
    ''' Modify By: Pinkal
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>
    ''' 
    Public Function SetSMSForLoanTrancheApplication(ByVal mstrEmployeeName As String, ByVal mstrTOTPCode As String, ByVal mintMaxSeconds As Integer, ByVal mblnLoanApproval As Boolean) As String
        Dim StrMessage As New System.Text.StringBuilder
        Try
            StrMessage.Append("<HTML><BODY>")
            StrMessage.Append(vbCrLf)

            StrMessage.Append("<p><span style='font-size:9.0pt; font-family:" & Chr(34) & "Verdana" & Chr(34) & "," & Chr(34) & "Sans-Serif" & Chr(34) & "; color:#333399; margin-left:0px;margin-right:0px;margin-top:20px;margin-bottom:20px; display:block;'>")
            StrMessage.Append(Language.getMessage(mstrModuleName, 21, "Dear") & " <b>" & getTitleCase(mstrEmployeeName) & "</b></span></p>")
            StrMessage.Append(vbCrLf)

            StrMessage.Append("<p><span style='font-size:9.0pt; font-family:" & Chr(34) & "Verdana" & Chr(34) & "," & Chr(34) & "Sans-Serif" & Chr(34) & "; color:#333399; margin-left:0px;margin-right:0px;margin-top:20px;margin-bottom:20px; display:block;'>")

            If mblnLoanApproval = False Then
                StrMessage.Append(Language.getMessage(mstrModuleName, 22, "OTP for applying Loan Tranche Application is") & " " & mstrTOTPCode & " " & Language.getMessage(mstrModuleName, 23, "(Valid for") & " " & mintMaxSeconds & " " & Language.getMessage(mstrModuleName, 24, "seconds).") & "</span></p>")
            Else
                StrMessage.Append(Language.getMessage(mstrModuleName, 25, "OTP for Loan Tranche Approval is") & " " & mstrTOTPCode & " " & Language.getMessage(mstrModuleName, 23, "(Valid for") & " " & mintMaxSeconds & " " & Language.getMessage(mstrModuleName, 24, "seconds).") & "</span></p>")
            End If

            StrMessage.Append("<p><center style='padding-top:25px;font-size:10.0pt;font-family:&quot;Verdana&quot;,&quot;Sans-Serif&quot;;color:#333399;margin-left:0px;margin-right:0px;margin-top:0px;margin-bottom:10px'><b>&quot;POWERED BY ARUTI HR &amp; PAYROLL MANAGEMENT SOFTWARE.&quot;</b></center></p>")
            StrMessage.Append("</span></p>")
            StrMessage.Append("</BODY></HTML>")

            Return StrMessage.ToString

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: SetSMSForLoanTrancheApplication; Module Name: " & mstrModuleName)
            Return ""
        End Try
    End Function

    'Pinkal (16-Nov-2023) -- End


    '<Language> This Auto Generated Text Please Do Not Modify it.
#Region " Language & UI Settings "
    Public Shared Sub SetMessages()
        Try
            Language.setMessage(mstrModuleName, 1, "Select")
            Language.setMessage(mstrModuleName, 2, "Pending")
            Language.setMessage(mstrModuleName, 3, "Approved")
            Language.setMessage(mstrModuleName, 4, "Rejected")
            Language.setMessage(mstrModuleName, 5, "Loan Tranche Request Review/Decision")
            Language.setMessage(mstrModuleName, 6, "Dear")
            Language.setMessage(mstrModuleName, 7, "Request No")
            Language.setMessage(mstrModuleName, 8, " for")
            Language.setMessage(mstrModuleName, 9, "I.F.O")
            Language.setMessage(mstrModuleName, 10, "has been submitted for your review and approval/decline.")
            Language.setMessage(mstrModuleName, 11, "Please click on the link below for the appropriate action")
            Language.setMessage(mstrModuleName, 12, "Regards.")
            Language.setMessage(mstrModuleName, 13, "Loan Tranche Request Submission")
            Language.setMessage(mstrModuleName, 14, "Loan Tranche Request Approval")
            Language.setMessage(mstrModuleName, 15, "Loan Tranche Request Decline")
            Language.setMessage(mstrModuleName, 16, "Sorry")
            Language.setMessage(mstrModuleName, 17, "has been submitted successfully for review/analysis.")
            Language.setMessage(mstrModuleName, 18, "has been approved for disbursement.")
            Language.setMessage(mstrModuleName, 19, "has been declined due to")
            Language.setMessage(mstrModuleName, 20, "Kindly contact HR Support for further clarity.")
            Language.setMessage(mstrModuleName, 21, "Dear")
            Language.setMessage(mstrModuleName, 22, "OTP for applying Loan Tranche Application is")
            Language.setMessage(mstrModuleName, 23, "(Valid for")
            Language.setMessage(mstrModuleName, 24, "seconds).")
            Language.setMessage(mstrModuleName, 25, "OTP for Loan Tranche Approval is")

        Catch Ex As Exception
            DisplayError.Show("-1", Ex.Message, "SetMessages", mstrModuleName)
        End Try
    End Sub
#End Region 'Language & UI Settings
    '</Language>
End Class