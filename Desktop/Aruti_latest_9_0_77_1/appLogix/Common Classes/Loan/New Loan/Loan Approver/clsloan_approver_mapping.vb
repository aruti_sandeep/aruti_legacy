﻿'************************************************************************************************************************************
'Class Name : clsloan_approver_mapping.vb
'Purpose    :
'Date       :15/06/2011
'Written By :Pinkal
'Modified   :
'************************************************************************************************************************************

Imports eZeeCommonLib
'' <summary>
'' Purpose: 
'' Developer: Pinkal
'' </summary>
'Public Class clsloan_approver_mapping
'    Private Const mstrModuleName = "clsloan_approver_mapping"
'    Dim objDataOperation As clsDataOperation
'    Dim mstrMessage As String = ""

'#Region " Private variables "
'    Private mintApprovermappingunkid As Integer
'    Private mintApprovertranunkid As Integer
'    Private mintUserunkid As Integer
'#End Region

'#Region " Properties "
'    ''' <summary>
'    ''' Purpose: Get Message from Class 
'    ''' Modify By: Pinkal
'    ''' </summary>
'    Public ReadOnly Property _Message() As String
'        Get
'            Return mstrMessage
'        End Get
'    End Property

'    ''' <summary>
'    ''' Purpose: Get or Set approvermappingunkid
'    ''' Modify By: Pinkal
'    ''' </summary>
'    Public Property _Approvermappingunkid() As Integer
'        Get
'            Return mintApprovermappingunkid
'        End Get
'        Set(ByVal value As Integer)
'            mintApprovermappingunkid = value
'            Call GetData()
'        End Set
'    End Property

'    ''' <summary>
'    ''' Purpose: Get or Set approvertranunkid
'    ''' Modify By: Pinkal
'    ''' </summary>
'    Public Property _Approvertranunkid() As Integer
'        Get
'            Return mintApprovertranunkid
'        End Get
'        Set(ByVal value As Integer)
'            mintApprovertranunkid = value
'        End Set
'    End Property

'    ''' <summary>
'    ''' Purpose: Get or Set userunkid
'    ''' Modify By: Pinkal
'    ''' </summary>
'    Public Property _Userunkid() As Integer
'        Get
'            Return mintUserunkid
'        End Get
'        Set(ByVal value As Integer)
'            mintUserunkid = value
'        End Set
'    End Property

'#End Region

'    ''' <summary>
'    ''' Modify By: Pinkal
'    ''' </summary>
'    ''' <purpose> Assign all Property variable </purpose>
'    Public Sub GetData()
'        Dim dsList As DataSet = Nothing
'        Dim strQ As String = ""
'        Dim exForce As Exception

'        objDataOperation = New clsDataOperation

'        Try
'            strQ = "SELECT " & _
'              "  approvermappingunkid " & _
'              ", approvertranunkid " & _
'              ", userunkid " & _
'             "FROM lnloan_approver_mapping " & _
'             "WHERE approvermappingunkid = @approvermappingunkid "

'            objDataOperation.AddParameter("@approvermappingunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintApprovermappingunkid.ToString)

'            dsList = objDataOperation.ExecQuery(strQ, "List")

'            If objDataOperation.ErrorMessage <> "" Then
'                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
'                Throw exForce
'            End If

'            For Each dtRow As DataRow In dsList.Tables(0).Rows
'                mintApprovermappingunkid = CInt(dtRow.Item("approvermappingunkid"))
'                mintApprovertranunkid = CInt(dtRow.Item("approvertranunkid"))
'                mintUserunkid = CInt(dtRow.Item("userunkid"))
'                Exit For
'            Next
'        Catch ex As Exception
'            Throw New Exception(ex.Message & "; Procedure Name: GetData; Module Name: " & mstrModuleName)
'        Finally
'            exForce = Nothing
'            If dsList IsNot Nothing Then dsList.Dispose()
'            objDataOperation = Nothing
'        End Try
'    End Sub

'    ''' <summary>
'    ''' Modify By: Pinkal
'    ''' </summary>
'    ''' <purpose> Assign all Property variable </purpose>
'    Public Function GetList(ByVal strTableName As String, Optional ByVal intUserunkid As Integer = -1) As DataSet
'        Dim dsList As DataSet = Nothing
'        Dim strQ As String = ""
'        Dim exForce As Exception

'        objDataOperation = New clsDataOperation

'        Try

'            'Anjan (24 Jun 2011)-Start
'            'Issue : According to privilege that lower level user should not see superior level employees.
'            strQ = "SELECT  approvermappingunkid ," & _
'                      " lnloan_approver_mapping.approvertranunkid , " & _
'                      " lnloan_approver_tran.approverunkid, " & _
'                      " ISNULL(hremployee_master.firstname,'')+' '+ISNULL(hremployee_master.othername,'') +' '+ ISNULL(hremployee_master.surname,'')  'Approver', " & _
'                      " lnloan_approver_mapping.userunkid, " & _
'                      " hrmsConfiguration..cfuser_master.username " & _
'                      " FROM lnloan_approver_mapping " & _
'                      " LEFT JOIN lnloan_approver_tran ON lnloan_approver_tran.approvertranunkid= lnloan_approver_mapping.approvertranunkid " & _
'                      " LEFT JOIN hremployee_master ON hremployee_master.employeeunkid = lnloan_approver_tran.approverunkid " & _
'                      " LEFT JOIN hrmsConfiguration..cfuser_master ON hrmsConfiguration..cfuser_master.userunkid = lnloan_approver_mapping.userunkid " & _
'                      " WHERE 1=1 " 'Anjan (09 Aug 2011)-Start
'            'strQ = "SELECT  approvermappingunkid ," & _
'            '          " lnloan_approver_mapping.approvertranunkid , " & _
'            '          " lnloan_approver_tran.approverunkid, " & _
'            '          " ISNULL(hremployee_master.firstname,'')+' '+ISNULL(hremployee_master.othername,'') +' '+ ISNULL(hremployee_master.surname,'')  'Approver', " & _
'            '          " lnloan_approver_mapping.userunkid, " & _
'            '          " hrmsConfiguration..cfuser_master.username " & _
'            '          " FROM lnloan_approver_mapping " & _
'            '          " LEFT JOIN lnloan_approver_tran ON lnloan_approver_tran.approvertranunkid= lnloan_approver_mapping.approvertranunkid " & _
'            '          " LEFT JOIN hremployee_master ON hremployee_master.employeeunkid = lnloan_approver_tran.approverunkid " & _
'            '          " LEFT JOIN hrmsConfiguration..cfuser_master ON hrmsConfiguration..cfuser_master.userunkid = lnloan_approver_mapping.userunkid " & _
'            '          "WHERE 1 = 1 "


'            'If UserAccessLevel._AccessLevel.Length > 0 Then
'            '    strQ &= "  AND hremployee_master.jobunkid IN (" & UserAccessLevel._AccessLevel & ")  "
'            'End If

'            'Anjan (24 Jun 2011)-End 
'            objDataOperation.ClearParameters()
'            'Anjan (09 Aug 2011)-Start
'            'Issue : For including setting of acitve and inactive employee.
'            If ConfigParameter._Object._IsIncludeInactiveEmp = False Then
'                'Sohail (06 Jan 2012) -- Start
'                'TRA - ENHANCEMENT
'                strQ &= " AND CONVERT(CHAR(8),hremployee_master.appointeddate,112) <= @enddate " & _
'                           " AND ISNULL(CONVERT(CHAR(8),hremployee_master.termination_from_date,112),@startdate) >= @startdate " & _
'                           " AND ISNULL(CONVERT(CHAR(8),hremployee_master.termination_to_date,112),@startdate) >= @startdate " & _
'                           " AND ISNULL(CONVERT(CHAR(8),hremployee_master.empl_enddate,112), @startdate) >= @startdate " 'S.SANDEEP [ 15 MAY 2012 empl_enddate ] -- START -- END

'                objDataOperation.AddParameter("@startdate", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, ConfigParameter._Object._EmployeeAsOnDate)
'                objDataOperation.AddParameter("@enddate", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, ConfigParameter._Object._EmployeeAsOnDate)
'                'Sohail (06 Jan 2012) -- End
'            End If
'            'Anjan (09 Aug 2011)-End 

'            If intUserunkid > 0 Then
'                'S.SANDEEP [ 20 AUG 2011 ] -- START
'                'ENHANCEMENT : LANGUAGES IMPLEMENTATION
'                'strQ &= " WHERE lnloan_approver_mapping.userunkid = @userunkid"
'                strQ &= " AND lnloan_approver_mapping.userunkid = @userunkid"
'                'S.SANDEEP [ 20 AUG 2011 ] -- END 

'                objDataOperation.AddParameter("@userunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intUserunkid)
'            End If

'            dsList = objDataOperation.ExecQuery(strQ, strTableName)

'            If objDataOperation.ErrorMessage <> "" Then
'                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
'                Throw exForce
'            End If

'        Catch ex As Exception
'            Throw New Exception(ex.Message & "; Procedure Name: GetList; Module Name: " & mstrModuleName)
'        Finally
'            exForce = Nothing
'            If dsList IsNot Nothing Then dsList.Dispose()
'            objDataOperation = Nothing
'        End Try
'        Return dsList
'    End Function

'    'Pinkal (12-Oct-2011) -- Start
'    'ENHANCEMENT : AUDIT TRAIL MAINTENANCE

'' <summary>
'    '' Modify By: Pinkal
'' </summary>
'    '' <returns>Boolean</returns>
'    '' <purpose> INSERT INTO Database Table (lnloan_approver_mapping) </purpose>
'   ' Public Function Insert() As Boolean

''        Dim dsList As DataSet = Nothing
''        Dim strQ As String = ""
''        Dim exForce As Exception

''        objDataOperation = New clsDataOperation

''        Try
''            objDataOperation.AddParameter("@approvertranunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintApprovertranunkid.ToString)
''            objDataOperation.AddParameter("@userunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintUserunkid.ToString)
''
''            strQ = "INSERT INTO lnloan_approver_mapping ( " & _
''              "  approvertranunkid " & _
''              ", userunkid" & _
''            ") VALUES (" & _
''              "  @approvertranunkid " & _
''              ", @userunkid" & _
''            "); SELECT @@identity"
''
''            dsList = objDataOperation.ExecQuery(strQ, "List")
''
''            If objDataOperation.ErrorMessage <> "" Then
''                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
''                Throw exForce
''            End If
''
''            mintApprovermappingunkid = dsList.Tables(0).Rows(0).Item(0)
''
''            Return True
''        Catch ex As Exception
''            Throw New Exception(ex.Message & "; Procedure Name: Insert; Module Name: " & mstrModuleName)
''            Return False
''        Finally
''            exForce = Nothing
''            If dsList IsNot Nothing Then dsList.Dispose()
' ''            objDataOperation = Nothing
''        End Try
''    End Function

''Pinkal (12-Oct-2011) -- Start
'    'ENHANCEMENT : AUDIT TRAIL MAINTENANCE

'    ''' <summary>
'    ''' Modify By: Pinkal
'    ''' </summary>
'    ''' <returns>Boolean</returns>
'    ''' <purpose> INSERT INTO Database Table (lnloan_approver_mapping) </purpose>
'    Public Function Insert(ByVal dctLoanApprover As Dictionary(Of Integer, String)) As Boolean

'        Dim dsList As DataSet = Nothing
'        Dim strQ As String = ""
'        Dim exForce As Exception

'        objDataOperation = New clsDataOperation
'        objDataOperation.BindTransaction()

'        Try

'            If dctLoanApprover.Keys.Count < 0 Then Return True


'            strQ = "INSERT INTO lnloan_approver_mapping ( " & _
'              "  approvertranunkid " & _
'              ", userunkid" & _
'            ") VALUES (" & _
'              "  @approvertranunkid " & _
'              ", @userunkid" & _
'            "); SELECT @@identity"


'            For i As Integer = 0 To dctLoanApprover.Keys.Count - 1

'                mintUserunkid = CInt(dctLoanApprover.Keys(i))

'                Dim strApprover() As String = dctLoanApprover(mintUserunkid).ToString().Split(",")

'                Dim dList As DataSet = clsCommonATLog.GetChildList(objDataOperation, "lnloan_approver_mapping", "userunkid", mintUserunkid)
'                Dim dtDel() As DataRow = Nothing

'                If dctLoanApprover(mintUserunkid).ToString().Trim.Length > 0 AndAlso dList.Tables(0).Rows.Count > 0 Then

'                    dtDel = dList.Tables(0).Select("approvertranunkid NOT IN (" & dctLoanApprover(mintUserunkid).ToString().Trim & ") AND userunkid = " & mintUserunkid)

'                    If dtDel.Length > 0 Then

'                        For k As Integer = 0 To dtDel.Length - 1

'                            If clsCommonATLog.Insert_AtLog(objDataOperation, 3, "lnloan_approver_mapping", "approvermappingunkid", CInt(dtDel(k)("approvermappingunkid"))) = False Then
'                                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
'                                Throw exForce
'                            End If

'                            'S.SANDEEP [ 11 SEP 2012 ] -- START
'                            'ENHANCEMENT : TRA CHANGES
'                            'strQ = "DELETE FROM lnloan_approver_mapping WHERE  userunkid = @userunkid AND approvertranunkid = @approvertranunkid"
'                            Dim strQ1 As String = "DELETE FROM lnloan_approver_mapping WHERE  userunkid = @userunkid AND approvertranunkid = @approvertranunkid"
'                            'S.SANDEEP [ 11 SEP 2012 ] -- END

'                            objDataOperation.ClearParameters()
'                            objDataOperation.AddParameter("@userunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, CInt(dtDel(k)("userunkid")))
'                            objDataOperation.AddParameter("@approvertranunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, CInt(dtDel(k)("approvertranunkid")))

'                            'S.SANDEEP [ 11 SEP 2012 ] -- START
'                            'ENHANCEMENT : TRA CHANGES
'                            'Call objDataOperation.ExecNonQuery(strQ)
'                            Call objDataOperation.ExecNonQuery(strQ1)
'                            'S.SANDEEP [ 11 SEP 2012 ] -- END


'                            If objDataOperation.ErrorMessage <> "" Then
'                                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
'                                Throw exForce
'                            End If

'                            dList.Tables(0).Rows.Remove(dtDel(k))

'                        Next

'                        dList.AcceptChanges()

'                    End If

'                End If

'                If strApprover.Length > 0 Then

'                    For k As Integer = 0 To strApprover.Length - 1

'                        If dList IsNot Nothing AndAlso dList.Tables(0).Rows.Count > 0 Then
'                            dtDel = dList.Tables(0).Select("approvertranunkid = " & CInt(strApprover.GetValue(k)))
'                            If dtDel.Length > 0 Then Continue For
'                        End If

'                        objDataOperation.ClearParameters()
'                        objDataOperation.AddParameter("@approvertranunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, strApprover.GetValue(k).ToString)
'                        objDataOperation.AddParameter("@userunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintUserunkid.ToString)
'                        dsList = objDataOperation.ExecQuery(strQ, "List")

'                        If objDataOperation.ErrorMessage <> "" Then
'                            exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
'                            Throw exForce
'                        End If

'                        mintApprovermappingunkid = dsList.Tables(0).Rows(0).Item(0)

'                        If clsCommonATLog.Insert_AtLog(objDataOperation, 1, "lnloan_approver_mapping", "approvermappingunkid", mintApprovermappingunkid) = False Then
'                            exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
'                            Throw exForce
'                        End If

'                    Next

'                End If

'            Next

'            objDataOperation.ReleaseTransaction(True)
'            Return True

'        Catch ex As Exception
'            objDataOperation.ReleaseTransaction(False)
'            Throw New Exception(ex.Message & "; Procedure Name: Insert; Module Name: " & mstrModuleName)
'            Return False
'        Finally
'            exForce = Nothing
'            If dsList IsNot Nothing Then dsList.Dispose()
'            objDataOperation = Nothing
'        End Try
'    End Function

'    'Pinkal (12-Oct-2011) -- End

'    ''' <summary>
'    ''' Modify By: Pinkal
'    ''' </summary>
'    ''' <returns>Boolean</returns>
'    ''' <purpose> Update Database Table (lnloan_approver_mapping) </purpose>
'    Public Function Update() As Boolean

'        Dim dsList As DataSet = Nothing
'        Dim strQ As String = ""
'        Dim exForce As Exception

'        'Pinkal (12-Oct-2011) -- Start
'        'ENHANCEMENT : AUDIT TRAIL MAINTENANCE
'        objDataOperation = New clsDataOperation
'        objDataOperation.BindTransaction()
'        'Pinkal (12-Oct-2011) -- End

'        Try
'            objDataOperation.AddParameter("@approvermappingunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintApprovermappingunkid.ToString)
'            objDataOperation.AddParameter("@approvertranunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintApprovertranunkid.ToString)
'            objDataOperation.AddParameter("@userunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintUserunkid.ToString)

'            strQ = "UPDATE lnloan_approver_mapping SET " & _
'              "  approvertranunkid = @approvertranunkid" & _
'              ", userunkid = @userunkid " & _
'            "WHERE approvermappingunkid = @approvermappingunkid "

'            Call objDataOperation.ExecNonQuery(strQ)

'            If objDataOperation.ErrorMessage <> "" Then
'                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
'                Throw exForce
'            End If

'            'Pinkal (12-Oct-2011) -- Start
'            'ENHANCEMENT : AUDIT TRAIL MAINTENANCE

'            If clsCommonATLog.IsTableDataUpdate("atcommon_log", "lnloan_approver_mapping", mintApprovermappingunkid, "approvermappingunkid", 2) Then

'                If clsCommonATLog.Insert_AtLog(objDataOperation, 2, "lnloan_approver_mapping", "approvermappingunkid", mintApprovermappingunkid) = False Then
'                    exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
'                    Throw exForce
'                End If

'            End If

'            objDataOperation.ReleaseTransaction(True)
'            'Pinkal (12-Oct-2011) -- End

'            Return True
'        Catch ex As Exception
'            'Pinkal (12-Oct-2011) -- Start
'            'ENHANCEMENT : AUDIT TRAIL MAINTENANCE
'            objDataOperation.ReleaseTransaction(False)
'            'Pinkal (12-Oct-2011) -- End
'            Throw New Exception(ex.Message & "; Procedure Name: Update; Module Name: " & mstrModuleName)
'            Return False
'        Finally
'            exForce = Nothing
'            If dsList IsNot Nothing Then dsList.Dispose()
'            objDataOperation = Nothing
'        End Try
'    End Function

'    ''' <summary>
'    ''' Modify By: Pinkal
'    ''' </summary>
'    ''' <returns>Boolean</returns>
'    ''' <purpose> Delete Database Table (lnloan_approver_mapping) </purpose>
'    Public Function Delete(ByVal intUserunkid As Integer) As Boolean
'        Dim dsList As DataSet = Nothing
'        Dim strQ As String = ""
'        Dim exForce As Exception

'        objDataOperation = New clsDataOperation

'        Try
'            objDataOperation.ClearParameters()

'            strQ = "DELETE FROM lnloan_approver_mapping WHERE  userunkid = @userunkid "

'            objDataOperation.AddParameter("@userunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intUserunkid)

'            Call objDataOperation.ExecNonQuery(strQ)

'            If objDataOperation.ErrorMessage <> "" Then
'                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
'                Throw exForce
'            End If

'            Return True
'        Catch ex As Exception
'            Throw New Exception(ex.Message & "; Procedure Name: Update; Module Name: " & mstrModuleName)
'            Return False
'        Finally
'            exForce = Nothing
'            If dsList IsNot Nothing Then dsList.Dispose()
'            objDataOperation = Nothing
'        End Try
'    End Function

'    ''' <summary>
'    ''' Modify By: Pinkal
'    ''' </summary>
'    ''' <purpose> Assign all Property variable </purpose>
'    Public Function isExist(ByVal intapprovertranunkid As Integer, ByVal intUserunkid As Integer) As Boolean
'        Dim dsList As DataSet = Nothing
'        Dim strQ As String = ""
'        Dim exForce As Exception

'        objDataOperation = New clsDataOperation

'        Try
'            strQ = "SELECT " & _
'              "  approvermappingunkid " & _
'              ", approvertranunkid " & _
'              ", userunkid " & _
'             "FROM lnloan_approver_mapping " & _
'             "WHERE approvertranunkid = @approvertranunkid " & _
'             "AND userunkid = @userunkid "


'            objDataOperation.AddParameter("@approvertranunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intapprovertranunkid)
'            objDataOperation.AddParameter("@userunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intUserunkid)

'            dsList = objDataOperation.ExecQuery(strQ, "List")

'            If objDataOperation.ErrorMessage <> "" Then
'                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
'                Throw exForce
'            End If

'            Return dsList.Tables(0).Rows.Count > 0
'        Catch ex As Exception
'            Throw New Exception(ex.Message & "; Procedure Name: isExist; Module Name: " & mstrModuleName)
'        Finally
'            exForce = Nothing
'            If dsList IsNot Nothing Then dsList.Dispose()
'            objDataOperation = Nothing
'        End Try
'    End Function

'End Class


Public Class clsloan_approver_mapping
    Private Const mstrModuleName = "clsloan_approver_mapping"
    Dim objDataOperation As clsDataOperation
    Dim mstrMessage As String = ""

#Region " Private variables "
    Private mintApprovermappingunkid As Integer
    Private mintApprovertranunkid As Integer
    'Nilay (05-May-2016) -- Start
    'Private mintUserunkid As Integer
    Private mintMappedUserunkid As Integer
    'Nilay (05-May-2016) -- End


#End Region

#Region " Properties "
    'S.SANDEEP [28-May-2018] -- START 
    'ISSUE/ENHANCEMENT : {Audit Trails} 
    Private mstrFormName As String = String.Empty
    Public WriteOnly Property _FormName() As String
        Set(ByVal value As String)
            mstrFormName = value
        End Set
    End Property

    Private mstrClientIP As String = ""
    Public WriteOnly Property _ClientIP() As String
        Set(ByVal value As String)
            mstrClientIP = value
        End Set
    End Property

    Private mstrHostName As String = ""
    Public WriteOnly Property _HostName() As String
        Set(ByVal value As String)
            mstrHostName = value
        End Set
    End Property

    Private mblnIsWeb As Boolean = False
    Public WriteOnly Property _FromWeb() As Boolean
        Set(ByVal value As Boolean)
            mblnIsWeb = value
        End Set
    End Property

    Private mintAuditUserId As Integer = 0
    Public WriteOnly Property _AuditUserId() As Integer
        Set(ByVal value As Integer)
            mintAuditUserId = value
        End Set
    End Property

    Private mintCompanyUnkid As Integer = 0
    Public WriteOnly Property _CompanyUnkid() As Integer
        Set(ByVal value As Integer)
            mintCompanyUnkid = value
        End Set
    End Property

    Private mdtAuditDate As DateTime = Now
    Public WriteOnly Property _AuditDate() As DateTime
        Set(ByVal value As DateTime)
            mdtAuditDate = value
        End Set
    End Property

    Private xDataOpr As clsDataOperation = Nothing
    Public Property _xDataOpr() As clsDataOperation
        Get
            Return xDataOpr
        End Get
        Set(ByVal value As clsDataOperation)
            xDataOpr = value
        End Set
    End Property

    Private mintLoginEmployeeunkid As Integer = 0
    Public Property _LoginEmployeeunkid() As Integer
        Get
            Return mintLoginEmployeeunkid
        End Get
        Set(ByVal value As Integer)
            mintLoginEmployeeunkid = value
        End Set
    End Property

    Private mintVoidlogingemployeeunkid As Integer = 0
    Public Property _Voidloginemployeeunkid() As Integer
        Get
            Return mintVoidlogingemployeeunkid
        End Get
        Set(ByVal value As Integer)
            mintVoidlogingemployeeunkid = value
        End Set
    End Property
    'S.SANDEEP [28-May-2018] -- END 

    ''' <summary>
    ''' Purpose: Get Message from Class 
    ''' Modify By: Pinkal
    ''' </summary>
    Public ReadOnly Property _Message() As String
        Get
            Return mstrMessage
        End Get
    End Property

    ''' <summary>
    ''' Purpose: Get or Set approvermappingunkid
    ''' Modify By: Pinkal
    ''' </summary>
    Public Property _Approvermappingunkid() As Integer
        Get
            Return mintApprovermappingunkid
        End Get
        Set(ByVal value As Integer)
            mintApprovermappingunkid = value
            Call GetData()
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set approvertranunkid
    ''' Modify By: Pinkal
    ''' </summary>
    Public Property _Approvertranunkid() As Integer
        Get
            Return mintApprovertranunkid
        End Get
        Set(ByVal value As Integer)
            mintApprovertranunkid = value
        End Set
    End Property

    '' <summary>
    '' Purpose: Get or Set userunkid
    '' Modify By: Pinkal
    '' </summary>

    'Nilay (05-May-2016) -- Start
    'Public Property _Userunkid() As Integer
    Public Property _MappedUserunkid() As Integer
        'Nilay (05-May-2016) -- End
        Get
            Return mintMappedUserunkid
        End Get
        Set(ByVal value As Integer)
            mintMappedUserunkid = value
        End Set
    End Property

#End Region

    ''' <summary>
    ''' Modify By: Pinkal
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>
    Public Sub GetData(Optional ByVal intApproverunkid As Integer = -1, Optional ByVal intUserunkid As Integer = -1, Optional ByVal intunkid As Integer = -1)
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        Dim objDataOperation As New clsDataOperation
        objDataOperation.ClearParameters()

        Try
            strQ = "SELECT " & _
              "  approvermappingunkid " & _
              ", approvertranunkid " & _
              ", userunkid " & _
             "FROM lnloan_approver_mapping " & _
                 "WHERE 1= 1 "


            If intunkid > 0 Then
                strQ &= " AND  mappingunkid = @approvermappingunkid "
                objDataOperation.AddParameter("@approvermappingunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intunkid.ToString)
            End If

            If intApproverunkid > 0 Then
                strQ &= " AND approvertranunkid = @approverunkid "
                objDataOperation.AddParameter("@approverunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intApproverunkid.ToString)
            End If

            If intUserunkid > 0 Then
                strQ &= " AND Userunkid = @Userunkid "
                objDataOperation.AddParameter("@Userunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intUserunkid.ToString)
            End If


            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            For Each dtRow As DataRow In dsList.Tables(0).Rows
                mintApprovermappingunkid = CInt(dtRow.Item("approvermappingunkid"))
                mintApprovertranunkid = CInt(dtRow.Item("approvertranunkid"))
                mintMappedUserunkid = CInt(dtRow.Item("userunkid")) 'Nilay (05-May-2016) -- [mintMappedUserunkid]

                Exit For
            Next
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: GetData; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
        End Try
    End Sub


    'Pinkal (14-Apr-2015) -- Start
    'Enhancement - WORKING ON REDESIGNING LOAN MODULE.

    '''' <summary>
    '''' Modify By: Pinkal
    '''' </summary>
    '''' <purpose> Assign all Property variable </purpose>
    'Public Function GetList(ByVal strTableName As String, Optional ByVal intUserunkid As Integer = -1) As DataSet
    '    Dim dsList As DataSet = Nothing
    '    Dim strQ As String = ""
    '    Dim exForce As Exception

    '    objDataOperation = New clsDataOperation

    '    Try

    '        strQ = "SELECT  approvermappingunkid ," & _
    '                  " lnloan_approver_mapping.approvertranunkid , " & _
    '                  " lnloan_approver_tran.approverunkid, " & _
    '                  " ISNULL(hremployee_master.firstname,'')+' '+ISNULL(hremployee_master.othername,'') +' '+ ISNULL(hremployee_master.surname,'')  'Approver', " & _
    '                  " lnloan_approver_mapping.userunkid, " & _
    '                  " hrmsConfiguration..cfuser_master.username " & _
    '                  " FROM lnloan_approver_mapping " & _
    '                  " LEFT JOIN lnloan_approver_tran ON lnloan_approver_tran.approvertranunkid= lnloan_approver_mapping.approvertranunkid " & _
    '                  " LEFT JOIN hremployee_master ON hremployee_master.employeeunkid = lnloan_approver_tran.approverunkid " & _
    '                  " LEFT JOIN hrmsConfiguration..cfuser_master ON hrmsConfiguration..cfuser_master.userunkid = lnloan_approver_mapping.userunkid " & _
    '                  " WHERE 1=1 "

    '        objDataOperation.ClearParameters()

    '        If ConfigParameter._Object._IsIncludeInactiveEmp = False Then

    '            strQ &= " AND CONVERT(CHAR(8),hremployee_master.appointeddate,112) <= @enddate " & _
    '                       " AND ISNULL(CONVERT(CHAR(8),hremployee_master.termination_from_date,112),@startdate) >= @startdate " & _
    '                       " AND ISNULL(CONVERT(CHAR(8),hremployee_master.termination_to_date,112),@startdate) >= @startdate " & _
    '                       " AND ISNULL(CONVERT(CHAR(8),hremployee_master.empl_enddate,112), @startdate) >= @startdate "

    '            objDataOperation.AddParameter("@startdate", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, ConfigParameter._Object._EmployeeAsOnDate)
    '            objDataOperation.AddParameter("@enddate", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, ConfigParameter._Object._EmployeeAsOnDate)

    '        End If


    '        If intUserunkid > 0 Then
    '            strQ &= " AND lnloan_approver_mapping.userunkid = @userunkid"
    '            objDataOperation.AddParameter("@userunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intUserunkid)
    '        End If

    '        dsList = objDataOperation.ExecQuery(strQ, strTableName)

    '        If objDataOperation.ErrorMessage <> "" Then
    '            exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
    '            Throw exForce
    '        End If

    '    Catch ex As Exception
    '        Throw New Exception(ex.Message & "; Procedure Name: GetList; Module Name: " & mstrModuleName)
    '    Finally
    '        exForce = Nothing
    '        If dsList IsNot Nothing Then dsList.Dispose()
    '        objDataOperation = Nothing
    '    End Try
    '    Return dsList
    'End Function

    'Nilay (10-Oct-2015) -- Start
    'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
    '' <summary>
    '' Modify By: Pinkal
    '' </summary>
    '' <purpose> Assign all Property variable </purpose>

    'Nilay (01-Mar-2016) -- Start
    'ENHANCEMENT - Implementing External Approval changes 
    Public Function GetList(ByVal xDatabaseName As String, _
                            ByVal xUserUnkid As Integer, _
                            ByVal xYearUnkid As Integer, _
                            ByVal xCompanyUnkid As Integer, _
                            ByVal xPeriodStart As DateTime, _
                            ByVal xPeriodEnd As DateTime, _
                            ByVal xUserModeSetting As String, _
                            ByVal xOnlyApproved As Boolean, _
                            ByVal xIncludeIn_ActiveEmployee As Boolean, _
                            ByVal strTableName As String, _
                            Optional ByVal intApproverID As Integer = -1, _
                            Optional ByVal objDataOp As clsDataOperation = Nothing) As DataSet
        'Nilay (07-Feb-2016) -- [objDataOp]
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        'Nilay (07-Feb-2016) -- Start
        'objDataOperation = New clsDataOperation
        If objDataOp IsNot Nothing Then
            objDataOperation = objDataOp
        Else
            objDataOperation = New clsDataOperation
        End If
        'Nilay (07-Feb-2016) -- End

        Try
            Dim strQFinal As String = String.Empty
            Dim strQCondition As String = String.Empty

            Dim xDateJoinQry, xDateFilterQry As String
            xDateJoinQry = "" : xDateFilterQry = ""
            Call GetDatesFilterString(xDateJoinQry, xDateFilterQry, xPeriodStart, xPeriodEnd, , , xDatabaseName)

            strQ = "SELECT " & _
                      "  approvermappingunkid " & _
                      ", lnloan_approver_mapping.approvertranunkid " & _
                      ", lnloanapprover_master.lnapproverunkid " & _
                      ", #APPROVER_NAME# AS Approver " & _
                      ", lnloan_approver_mapping.userunkid " & _
                      ", hrmsConfiguration..cfuser_master.username " & _
                      " FROM lnloan_approver_mapping " & _
                      " LEFT JOIN lnloanapprover_master ON lnloanapprover_master.lnapproverunkid = lnloan_approver_mapping.approvertranunkid " & _
                      " #APPROVER_JOIN# " & _
                      " LEFT JOIN hrmsConfiguration..cfuser_master ON hrmsConfiguration..cfuser_master.userunkid = lnloan_approver_mapping.userunkid "

            strQFinal = strQ

            If xDateJoinQry.Trim.Length > 0 Then
                strQ &= xDateJoinQry
            End If

            strQCondition &= " WHERE 1=1 "
            strQCondition &= " AND lnloanapprover_master.isexternalapprover = #EXT_APPROVER# "

            'objDataOperation.ClearParameters()

            If xUserUnkid > 0 Then
                strQCondition &= " AND lnloan_approver_mapping.userunkid = @userunkid"
                objDataOperation.AddParameter("@userunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, xUserUnkid)
            End If

            If intApproverID > 0 Then
                strQCondition &= " AND lnloan_approver_mapping.approvertranunkid = @approvertranunkid"
                objDataOperation.AddParameter("@approvertranunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intApproverID)
            End If

            strQ &= strQCondition

            If xIncludeIn_ActiveEmployee = False Then
                If xDateFilterQry.Trim.Length > 0 Then
                    strQ &= xDateFilterQry
                End If
            End If

            strQ = strQ.Replace("#APPROVER_NAME#", "ISNULL(hremployee_master.firstname,'')+' '+ISNULL(hremployee_master.othername,'') +' '+ ISNULL(hremployee_master.surname,'') ")
            strQ = strQ.Replace("#APPROVER_JOIN#", "LEFT JOIN hremployee_master ON hremployee_master.employeeunkid = lnloanapprover_master.approverempunkid ")
            strQ = strQ.Replace("#EXT_APPROVER#", "0")

            dsList = objDataOperation.ExecQuery(strQ, strTableName)

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            Dim dsCompany As DataSet
            Dim objlnApprover As New clsLoanApprover_master

            dsCompany = objlnApprover.GetExternalApproverList(objDataOperation, "Company")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            Dim dsExtList As New DataSet

            For Each dRow As DataRow In dsCompany.Tables("Company").Rows

                strQ = strQFinal

                If dRow("dbname").ToString.Trim.Length <= 0 Then
                    strQ = strQ.Replace("#APPROVER_NAME#", "ISNULL(cfuser_master.username,'') ")
                    strQ = strQ.Replace("#APPROVER_JOIN#", "LEFT JOIN hrmsConfiguration..cfuser_master AS UM ON UM.userunkid  = lnloanapprover_master.approverempunkid ")
                    strQ &= strQCondition
                Else
                    strQ = strQ.Replace("#APPROVER_NAME#", "CASE WHEN ISNULL(hremployee_master.firstname,'') + ' ' + ISNULL(hremployee_master.othername,'') + ' ' + ISNULL(hremployee_master.surname,'') = ' ' THEN ISNULL(UM.username,'') " & _
                                                                "ELSE ISNULL(hremployee_master.firstname,'') + ' ' + ISNULL(hremployee_master.othername,'') + ' ' + ISNULL(hremployee_master.surname,'') END ")
                    strQ = strQ.Replace("#APPROVER_JOIN#", "LEFT JOIN hrmsConfiguration..cfuser_master AS UM ON UM.userunkid = lnloanapprover_master.approverempunkid " & _
                                                           "LEFT JOIN #DB_NAME#hremployee_master ON hremployee_master.employeeunkid = UM.employeeunkid ")
                    strQ = strQ.Replace("#DB_NAME#", dRow("dbname").ToString & "..")

                    If xDateJoinQry.Trim.Length > 0 Then
                        strQ &= xDateJoinQry
                    End If

                    strQ &= strQCondition

                    If xIncludeIn_ActiveEmployee = False Then
                        If xDateFilterQry.Trim.Length > 0 Then
                            strQ &= xDateFilterQry
                        End If
                    End If
                End If

                strQ = strQ.Replace("#EXT_APPROVER#", "1")
                strQ &= " AND UM.companyunkid = " & dRow("companyunkid")

                objDataOperation.ClearParameters()

                If xUserUnkid > 0 Then
                    objDataOperation.AddParameter("@userunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, xUserUnkid)
                End If

                If intApproverID > 0 Then
                    objDataOperation.AddParameter("@approvertranunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intApproverID)
                End If

                dsExtList = objDataOperation.ExecQuery(strQ, strTableName)

                If objDataOperation.ErrorMessage <> "" Then
                    exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                    Throw exForce
                End If

                If dsList.Tables.Count <= 0 Then
                    dsList.Tables.Add(dsExtList.Tables(0).Copy)
                Else
                    dsList.Tables(0).Merge(dsExtList.Tables(0), True)
                End If
            Next

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: GetList; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            'Nilay (07-Feb-2016) -- Start
            'objDataOperation = Nothing
            If objDataOp Is Nothing Then objDataOperation = Nothing
            'Nilay (07-Feb-2016) -- End
            If dsList IsNot Nothing Then dsList.Dispose()
        End Try
        Return dsList
    End Function

    'Public Function GetList(ByVal xDatabaseName As String, _
    '                        ByVal xUserUnkid As Integer, _
    '                        ByVal xYearUnkid As Integer, _
    '                        ByVal xCompanyUnkid As Integer, _
    '                        ByVal xPeriodStart As DateTime, _
    '                        ByVal xPeriodEnd As DateTime, _
    '                        ByVal xUserModeSetting As String, _
    '                        ByVal xOnlyApproved As Boolean, _
    '                        ByVal xIncludeIn_ActiveEmployee As Boolean, _
    '                        ByVal strTableName As String, _
    '                        Optional ByVal intApproverID As Integer = -1, _
    '                        Optional ByVal objDataOp As clsDataOperation = Nothing) As DataSet
    '    'Nilay (07-Feb-2016) -- [objDataOp]
    '    Dim dsList As DataSet = Nothing
    '    Dim strQ As String = ""
    '    Dim exForce As Exception

    '    'Nilay (07-Feb-2016) -- Start
    '    'objDataOperation = New clsDataOperation
    '    If objDataOp IsNot Nothing Then
    '        objDataOperation = objDataOp
    '    Else
    '        objDataOperation = New clsDataOperation
    '    End If
    '    'Nilay (07-Feb-2016) -- End

    '    Try
    '        Dim xDateJoinQry, xDateFilterQry As String
    '        xDateJoinQry = "" : xDateFilterQry = ""
    '        Call GetDatesFilterString(xDateJoinQry, xDateFilterQry, xPeriodStart, xPeriodEnd, , , xDatabaseName)

    '        'Nilay (07-Feb-2016) -- Start
    '        'strQ = "SELECT  approvermappingunkid ," & _
    '        '          " lnloan_approver_mapping.approvertranunkid , " & _
    '        '          " lnloan_approver_tran.approverunkid, " & _
    '        '          " ISNULL(hremployee_master.firstname,'')+' '+ISNULL(hremployee_master.othername,'') +' '+ ISNULL(hremployee_master.surname,'')  'Approver', " & _
    '        '          " lnloan_approver_mapping.userunkid, " & _
    '        '          " hrmsConfiguration..cfuser_master.username " & _
    '        '          " FROM lnloan_approver_mapping " & _
    '        '          " LEFT JOIN lnloan_approver_tran ON lnloan_approver_tran.approvertranunkid= lnloan_approver_mapping.approvertranunkid " & _
    '        '          " LEFT JOIN hremployee_master ON hremployee_master.employeeunkid = lnloan_approver_tran.approverunkid " & _
    '        '          " LEFT JOIN hrmsConfiguration..cfuser_master ON hrmsConfiguration..cfuser_master.userunkid = lnloan_approver_mapping.userunkid "

    '        strQ = "SELECT " & _
    '                  "  approvermappingunkid " & _
    '                  ", lnloan_approver_mapping.approvertranunkid " & _
    '                  ", lnloanapprover_master.lnapproverunkid " & _
    '                  ", ISNULL(hremployee_master.firstname,'')+' '+ISNULL(hremployee_master.othername,'') +' '+ ISNULL(hremployee_master.surname,'') AS Approver " & _
    '                  ", lnloan_approver_mapping.userunkid " & _
    '                  ", hrmsConfiguration..cfuser_master.username " & _
    '                  " FROM lnloan_approver_mapping " & _
    '                  " LEFT JOIN lnloanapprover_master ON lnloanapprover_master.lnapproverunkid = lnloan_approver_mapping.approvertranunkid " & _
    '                  " LEFT JOIN hremployee_master ON hremployee_master.employeeunkid = lnloanapprover_master.approverempunkid " & _
    '                  " LEFT JOIN hrmsConfiguration..cfuser_master ON hrmsConfiguration..cfuser_master.userunkid = lnloan_approver_mapping.userunkid "
    '        'Nilay (07-Feb-2016) -- End

    '        If xDateJoinQry.Trim.Length > 0 Then
    '            strQ &= xDateJoinQry
    '        End If

    '        strQ &= " WHERE 1=1 "

    '        objDataOperation.ClearParameters()

    '        If xIncludeIn_ActiveEmployee = False Then
    '            If xDateFilterQry.Trim.Length > 0 Then
    '                strQ &= xDateFilterQry
    '            End If
    '        End If

    '        If xUserUnkid > 0 Then
    '            strQ &= " AND lnloan_approver_mapping.userunkid = @userunkid"
    '            objDataOperation.AddParameter("@userunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, xUserUnkid)
    '        End If

    '        If intApproverID > 0 Then
    '            strQ &= " AND lnloan_approver_mapping.approvertranunkid = @approvertranunkid"
    '            objDataOperation.AddParameter("@approvertranunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intApproverID)
    '        End If

    '        dsList = objDataOperation.ExecQuery(strQ, strTableName)

    '        If objDataOperation.ErrorMessage <> "" Then
    '            exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
    '            Throw exForce
    '        End If

    '    Catch ex As Exception
    '        Throw New Exception(ex.Message & "; Procedure Name: GetList; Module Name: " & mstrModuleName)
    '    Finally
    '        exForce = Nothing
    '        'Nilay (07-Feb-2016) -- Start
    '        'objDataOperation = Nothing
    '        If objDataOp Is Nothing Then objDataOperation = Nothing
    '        'Nilay (07-Feb-2016) -- End
    '        If dsList IsNot Nothing Then dsList.Dispose()
    '    End Try
    '    Return dsList
    'End Function
    'Nilay (01-Mar-2016) -- End



    'Public Function GetList(ByVal strTableName As String, Optional ByVal intUserunkid As Integer = -1, Optional ByVal intApproverID As Integer = -1) As DataSet
    '    Dim dsList As DataSet = Nothing
    '    Dim strQ As String = ""
    '    Dim exForce As Exception

    '    objDataOperation = New clsDataOperation

    '    Try

    '        strQ = "SELECT  approvermappingunkid ," & _
    '                  " lnloan_approver_mapping.approvertranunkid , " & _
    '                  " lnloan_approver_tran.approverunkid, " & _
    '                  " ISNULL(hremployee_master.firstname,'')+' '+ISNULL(hremployee_master.othername,'') +' '+ ISNULL(hremployee_master.surname,'')  'Approver', " & _
    '                  " lnloan_approver_mapping.userunkid, " & _
    '                  " hrmsConfiguration..cfuser_master.username " & _
    '                  " FROM lnloan_approver_mapping " & _
    '                  " LEFT JOIN lnloan_approver_tran ON lnloan_approver_tran.approvertranunkid= lnloan_approver_mapping.approvertranunkid " & _
    '                  " LEFT JOIN hremployee_master ON hremployee_master.employeeunkid = lnloan_approver_tran.approverunkid " & _
    '                  " LEFT JOIN hrmsConfiguration..cfuser_master ON hrmsConfiguration..cfuser_master.userunkid = lnloan_approver_mapping.userunkid " & _
    '                  " WHERE 1=1 "

    '        objDataOperation.ClearParameters()

    '        If ConfigParameter._Object._IsIncludeInactiveEmp = False Then

    '            strQ &= " AND CONVERT(CHAR(8),hremployee_master.appointeddate,112) <= @enddate " & _
    '                       " AND ISNULL(CONVERT(CHAR(8),hremployee_master.termination_from_date,112),@startdate) >= @startdate " & _
    '                       " AND ISNULL(CONVERT(CHAR(8),hremployee_master.termination_to_date,112),@startdate) >= @startdate " & _
    '                       " AND ISNULL(CONVERT(CHAR(8),hremployee_master.empl_enddate,112), @startdate) >= @startdate "

    '            objDataOperation.AddParameter("@startdate", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, ConfigParameter._Object._EmployeeAsOnDate)
    '            objDataOperation.AddParameter("@enddate", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, ConfigParameter._Object._EmployeeAsOnDate)

    '        End If


    '        If intUserunkid > 0 Then
    '            strQ &= " AND lnloan_approver_mapping.userunkid = @userunkid"
    '            objDataOperation.AddParameter("@userunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intUserunkid)
    '        End If

    '        If intApproverID > 0 Then
    '            strQ &= " AND lnloan_approver_mapping.approvertranunkid = @approvertranunkid"
    '            objDataOperation.AddParameter("@approvertranunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intApproverID)
    '        End If

    '        dsList = objDataOperation.ExecQuery(strQ, strTableName)

    '        If objDataOperation.ErrorMessage <> "" Then
    '            exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
    '            Throw exForce
    '        End If

    '    Catch ex As Exception
    '        Throw New Exception(ex.Message & "; Procedure Name: GetList; Module Name: " & mstrModuleName)
    '    Finally
    '        exForce = Nothing
    '        If dsList IsNot Nothing Then dsList.Dispose()
    '        objDataOperation = Nothing
    '    End Try
    '    Return dsList
    'End Function

    'Nilay (10-Oct-2015) -- End



    'Pinkal (14-Apr-2015) -- End

    '' <summary>
    '' Modify By: Pinkal
    '' </summary>
    '' <returns>Boolean</returns>
    '' <purpose> INSERT INTO Database Table (lnloan_approver_mapping) </purpose>

    'Nilay (05-May-2016) -- Start
    'Public Function Insert(ByVal objDataOperation As clsDataOperation) As Boolean
    Public Function Insert(ByVal objDataOperation As clsDataOperation, ByVal intUserunkid As Integer) As Boolean
        'Nilay (05-May-2016) -- End

        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        Try

            strQ = "INSERT INTO lnloan_approver_mapping ( " & _
              "  approvertranunkid " & _
              ", userunkid" & _
            ") VALUES (" & _
              "  @approvertranunkid " & _
              ", @userunkid" & _
            "); SELECT @@identity"



            objDataOperation.ClearParameters()
            objDataOperation.AddParameter("@approvertranunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintApprovertranunkid)
            objDataOperation.AddParameter("@userunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintMappedUserunkid.ToString) 'Nilay (05-May-2016) -- [mintMappedUserunkid]
            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            mintApprovermappingunkid = dsList.Tables(0).Rows(0).Item(0)

            'S.SANDEEP [28-May-2018] -- START
            'ISSUE/ENHANCEMENT : {Audit Trails} 
            Dim objCommonATLog As New clsCommonATLog
            objCommonATLog._FormName = mstrFormName
            objCommonATLog._LoginEmployeeUnkid = mintLoginEmployeeunkid
            objCommonATLog._ClientIP = mstrClientIP
            objCommonATLog._HostName = mstrHostName
            objCommonATLog._FromWeb = mblnIsWeb
            objCommonATLog._AuditUserId = mintAuditUserId
            objCommonATLog._CompanyUnkid = mintCompanyUnkid
            objCommonATLog._AuditDate = mdtAuditDate
            'S.SANDEEP [28-May-2018] -- END

            If objCommonATLog.Insert_AtLog(objDataOperation, 1, "lnloan_approver_mapping", "approvermappingunkid", mintApprovermappingunkid, , intUserunkid) = False Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            'S.SANDEEP [28-May-2018] -- START
            'ISSUE/ENHANCEMENT : {Audit Trails} 
            objCommonATLog = Nothing
            'S.SANDEEP [28-May-2018] -- END


            Return True

        Catch ex As Exception
            'Nilay (07-Feb-2016) -- Start
            'objDataOperation.ReleaseTransaction(False)
            'Nilay (07-Feb-2016) -- End
            Throw New Exception(ex.Message & "; Procedure Name: Insert; Module Name: " & mstrModuleName)
            Return False
        Finally
            exForce = Nothing
            'Nilay (07-Feb-2016) -- Start
            'objDataOperation = Nothing
            'Nilay (07-Feb-2016) -- End
            If dsList IsNot Nothing Then dsList.Dispose()
        End Try
    End Function

    '' <summary>
    '' Modify By: Pinkal
    '' </summary>
    '' <returns>Boolean</returns>
    '' <purpose> Update Database Table (lnloan_approver_mapping) </purpose>

    'Nilay (05-May-2016) -- Start
    'Public Function Update(Optional ByVal objDOperation As clsDataOperation = Nothing) As Boolean
    Public Function Update(ByVal objDOperation As clsDataOperation, ByVal intUserunkid As Integer) As Boolean
        'Nilay (05-May-2016) -- End

        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        If objDOperation Is Nothing Then
            objDataOperation = New clsDataOperation
            objDataOperation.BindTransaction()
        Else
            objDataOperation = objDOperation
        End If

        Try
            objDataOperation.ClearParameters()
            objDataOperation.AddParameter("@approvermappingunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintApprovermappingunkid.ToString)
            objDataOperation.AddParameter("@approvertranunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintApprovertranunkid.ToString)
            objDataOperation.AddParameter("@userunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintMappedUserunkid.ToString) 'Nilay (05-May-2016) -- [mintMappedUserunkid]

            strQ = "UPDATE lnloan_approver_mapping SET " & _
              "  approvertranunkid = @approvertranunkid" & _
              ", userunkid = @userunkid " & _
            "WHERE approvermappingunkid = @approvermappingunkid "

            Call objDataOperation.ExecNonQuery(strQ)

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            'S.SANDEEP [28-May-2018] -- START
            'ISSUE/ENHANCEMENT : {Audit Trails} 
            Dim objCommonATLog As New clsCommonATLog
            objCommonATLog._FormName = mstrFormName
            objCommonATLog._LoginEmployeeUnkid = mintLoginEmployeeunkid
            objCommonATLog._ClientIP = mstrClientIP
            objCommonATLog._HostName = mstrHostName
            objCommonATLog._FromWeb = mblnIsWeb
            objCommonATLog._AuditUserId = mintAuditUserId
            objCommonATLog._CompanyUnkid = mintCompanyUnkid
            objCommonATLog._AuditDate = mdtAuditDate
            'S.SANDEEP [28-May-2018] -- END

            If objCommonATLog.IsTableDataUpdate("atcommon_log", "lnloan_approver_mapping", mintApprovermappingunkid, "approvermappingunkid", 2) Then
                If objCommonATLog.Insert_AtLog(objDataOperation, 2, "lnloan_approver_mapping", "approvermappingunkid", mintApprovermappingunkid, , intUserunkid) = False Then
                    exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                    Throw exForce
                End If
            End If

            'S.SANDEEP [28-May-2018] -- START
            'ISSUE/ENHANCEMENT : {Audit Trails} 
            objCommonATLog = Nothing
            'S.SANDEEP [28-May-2018] -- END


            If objDOperation Is Nothing Then
                objDataOperation.ReleaseTransaction(True)
            End If

            Return True
        Catch ex As Exception
            If objDOperation Is Nothing Then objDataOperation.ReleaseTransaction(False)
            Throw New Exception(ex.Message & "; Procedure Name: Update; Module Name: " & mstrModuleName)
            Return False
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
        End Try
    End Function

    ''' <summary>
    ''' Modify By: Pinkal
    ''' </summary>
    ''' <returns>Boolean</returns>
    ''' <purpose> Delete Database Table (lnloan_approver_mapping) </purpose>
    Public Function Delete(ByVal intUserunkid As Integer) As Boolean
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        objDataOperation = New clsDataOperation

        Try
            objDataOperation.ClearParameters()

            strQ = "DELETE FROM lnloan_approver_mapping WHERE  userunkid = @userunkid "

            objDataOperation.AddParameter("@userunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intUserunkid)

            Call objDataOperation.ExecNonQuery(strQ)

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            Return True
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: Update; Module Name: " & mstrModuleName)
            Return False
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
    End Function

    ''' <summary>
    ''' Modify By: Pinkal
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>
    Public Function isExist(ByVal intapprovertranunkid As Integer, ByVal intUserunkid As Integer) As Boolean
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        objDataOperation = New clsDataOperation

        Try
            strQ = "SELECT " & _
              "  approvermappingunkid " & _
              ", approvertranunkid " & _
              ", userunkid " & _
             "FROM lnloan_approver_mapping " & _
             "WHERE approvertranunkid = @approvertranunkid " & _
             "AND userunkid = @userunkid "


            objDataOperation.AddParameter("@approvertranunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intapprovertranunkid)
            objDataOperation.AddParameter("@userunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intUserunkid)

            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            Return dsList.Tables(0).Rows.Count > 0
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: isExist; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
    End Function

End Class