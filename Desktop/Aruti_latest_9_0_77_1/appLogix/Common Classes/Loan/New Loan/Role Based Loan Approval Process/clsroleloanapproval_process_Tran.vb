﻿'************************************************************************************************************************************
'Class Name : clsroleloanapproval_process_Tran.vb
'Purpose    :
'Date       :26-Sep-2022
'Written By :Pinkal
'Modified   :
'************************************************************************************************************************************

Imports eZeeCommonLib
Imports System.Data.OracleClient

''' <summary>
''' Purpose: 
''' Developer: Pinkal
''' </summary>
Public Class clsroleloanapproval_process_Tran
    Private Shared ReadOnly mstrModuleName As String = "clsroleloanapproval_process_Tran"
    Dim objDataOperation As clsDataOperation
    Dim mstrMessage As String = ""

#Region " Private variables "
    Private mintPendingloanaprovalunkid As Integer
    Private mintProcesspendingloanunkid As Integer
    Private mintEmployeeunkid As Integer
    Private mintMappingunkid As Integer
    Private mintRoleunkid As Integer
    Private mintLevelunkid As Integer
    Private mintPriority As Integer
    Private mdtApprovaldate As Date
    Private mintDeductionperiodunkid As Integer
    Private mdecLoan_Amount As Decimal
    Private mdecInstallmentamt As Decimal
    Private mintNoofinstallment As Integer
    Private mintCountryunkid As Integer
    Private mintMapUserunkid As Integer
    Private mblnIsExceptional As Boolean = False
    Private mintStatusunkid As Integer
    Private mintVisibleunkid As Integer
    Private mstrRemark As String = String.Empty
    Private mintUserunkid As Integer
    Private mblnIsvoid As Boolean
    Private mdtVoiddatetime As Date
    Private mintVoiduserunkid As Integer
    Private mstrVoidreason As String = String.Empty
    Private mstrFormName As String = ""
    Private mstrClientIP As String = ""
    Private mstrHostName As String = ""
    Private mblnIsFromWeb As Boolean = False
    Private mintAuditUserId As Integer = 0

    'Pinkal (23-Nov-2022) -- Start
    'NMB Loan Module Enhancement.
    Private objCRBData As New clsloanapproval_crbdata
    'Pinkal (23-Nov-2022) -- End

    'Hemant (25 Nov 2022) -- Start
    'ENHANCEMENT(NMB) : A1X-353 - As a user, I want to have offer letter reports for the loan scheme name - General loan
    Private mintFinalApproverTransferunkid As Integer
    Private mintFinalApproverCategorizationTranunkid As Integer
    Private mintFinalApproverAtEmployeeunkid As Integer
    Private mintCentralizedTransferunkid As Integer
    Private mintCentralizedCategorizationTranunkid As Integer
    Private mintCentralizedAtEmployeeunkid As Integer
    'Hemant (25 Nov 2022) -- End
#End Region

#Region " Properties "
    ''' <summary>
    ''' Purpose: Get Message from Class 
    ''' Modify By: Pinkal
    ''' </summary>
    Public ReadOnly Property _Message() As String
        Get
            Return mstrMessage
        End Get
    End Property

    ''' <summary>
    ''' Purpose: Get or Set pendingloanaprovalunkid
    ''' Modify By: Pinkal
    ''' </summary>
    Public Property _Pendingloanaprovalunkid(Optional ByVal objDataOp As clsDataOperation = Nothing) As Integer
        Get
            Return mintPendingloanaprovalunkid
        End Get
        Set(ByVal value As Integer)
            mintPendingloanaprovalunkid = value
            Call GetData(objDataOp)
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set processpendingloanunkid
    ''' Modify By: Pinkal
    ''' </summary>
    Public Property _Processpendingloanunkid() As Integer
        Get
            Return mintProcesspendingloanunkid
        End Get
        Set(ByVal value As Integer)
            mintProcesspendingloanunkid = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set employeeunkid
    ''' Modify By: Pinkal
    ''' </summary>
    Public Property _Employeeunkid() As Integer
        Get
            Return mintEmployeeunkid
        End Get
        Set(ByVal value As Integer)
            mintEmployeeunkid = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set mappingunkid
    ''' Modify By: Pinkal
    ''' </summary>
    Public Property _Mappingunkid() As Integer
        Get
            Return mintMappingunkid
        End Get
        Set(ByVal value As Integer)
            mintMappingunkid = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set roleunkid
    ''' Modify By: Pinkal
    ''' </summary>
    Public Property _Roleunkid() As Integer
        Get
            Return mintRoleunkid
        End Get
        Set(ByVal value As Integer)
            mintRoleunkid = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set levelunkid
    ''' Modify By: Pinkal
    ''' </summary>
    Public Property _Levelunkid() As Integer
        Get
            Return mintLevelunkid
        End Get
        Set(ByVal value As Integer)
            mintLevelunkid = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set Priority
    ''' Modify By: Pinkal
    ''' </summary>
    Public Property _Priority() As Integer
        Get
            Return mintPriority
        End Get
        Set(ByVal value As Integer)
            mintPriority = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set approvaldate
    ''' Modify By: Pinkal
    ''' </summary>
    Public Property _Approvaldate() As Date
        Get
            Return mdtApprovaldate
        End Get
        Set(ByVal value As Date)
            mdtApprovaldate = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set deductionperiodunkid
    ''' Modify By: Pinkal
    ''' </summary>
    Public Property _Deductionperiodunkid() As Integer
        Get
            Return mintDeductionperiodunkid
        End Get
        Set(ByVal value As Integer)
            mintDeductionperiodunkid = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set loan_amount
    ''' Modify By: Pinkal
    ''' </summary>
    Public Property _Loan_Amount() As Decimal
        Get
            Return mdecLoan_Amount
        End Get
        Set(ByVal value As Decimal)
            mdecLoan_Amount = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set installmentamt
    ''' Modify By: Pinkal
    ''' </summary>
    Public Property _Installmentamt() As Decimal
        Get
            Return mdecInstallmentamt
        End Get
        Set(ByVal value As Decimal)
            mdecInstallmentamt = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set noofinstallment
    ''' Modify By: Pinkal
    ''' </summary>
    Public Property _Noofinstallment() As Integer
        Get
            Return mintNoofinstallment
        End Get
        Set(ByVal value As Integer)
            mintNoofinstallment = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set countryunkid
    ''' Modify By: Pinkal
    ''' </summary>
    Public Property _Countryunkid() As Integer
        Get
            Return mintCountryunkid
        End Get
        Set(ByVal value As Integer)
            mintCountryunkid = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set Mapuserunkid
    ''' Modify By: Pinkal
    ''' </summary>
    Public Property _MapUserunkid() As Integer
        Get
            Return mintMapUserunkid
        End Get
        Set(ByVal value As Integer)
            mintMapUserunkid = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set IsExceptional
    ''' Modify By: Pinkal
    ''' </summary>
    Public Property _IsExceptional() As Boolean
        Get
            Return mblnIsExceptional
        End Get
        Set(ByVal value As Boolean)
            mblnIsExceptional = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set statusunkid
    ''' Modify By: Pinkal
    ''' </summary>
    Public Property _Statusunkid() As Integer
        Get
            Return mintStatusunkid
        End Get
        Set(ByVal value As Integer)
            mintStatusunkid = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set visibleunkid
    ''' Modify By: Pinkal
    ''' </summary>
    Public Property _Visibleunkid() As Integer
        Get
            Return mintVisibleunkid
        End Get
        Set(ByVal value As Integer)
            mintVisibleunkid = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set remark
    ''' Modify By: Pinkal
    ''' </summary>
    Public Property _Remark() As String
        Get
            Return mstrRemark
        End Get
        Set(ByVal value As String)
            mstrRemark = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set userunkid
    ''' Modify By: Pinkal
    ''' </summary>
    Public Property _Userunkid() As Integer
        Get
            Return mintUserunkid
        End Get
        Set(ByVal value As Integer)
            mintUserunkid = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set isvoid
    ''' Modify By: Pinkal
    ''' </summary>
    Public Property _Isvoid() As Boolean
        Get
            Return mblnIsvoid
        End Get
        Set(ByVal value As Boolean)
            mblnIsvoid = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set voiddatetime
    ''' Modify By: Pinkal
    ''' </summary>
    Public Property _Voiddatetime() As Date
        Get
            Return mdtVoiddatetime
        End Get
        Set(ByVal value As Date)
            mdtVoiddatetime = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set voiduserunkid
    ''' Modify By: Pinkal
    ''' </summary>
    Public Property _Voiduserunkid() As Integer
        Get
            Return mintVoiduserunkid
        End Get
        Set(ByVal value As Integer)
            mintVoiduserunkid = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set voidreason
    ''' Modify By: Pinkal
    ''' </summary>
    Public Property _Voidreason() As String
        Get
            Return mstrVoidreason
        End Get
        Set(ByVal value As String)
            mstrVoidreason = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set FormName
    ''' Modify By: Pinkal
    ''' </summary>
    Public Property _FormName() As String
        Get
            Return mstrFormName
        End Get
        Set(ByVal value As String)
            mstrFormName = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set ClientIP
    ''' Modify By: Pinkal
    ''' </summary>
    Public Property _ClientIP() As String
        Get
            Return mstrClientIP
        End Get
        Set(ByVal value As String)
            mstrClientIP = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set HostName
    ''' Modify By: Pinkal
    ''' </summary>
    Public Property _HostName() As String
        Get
            Return mstrHostName
        End Get
        Set(ByVal value As String)
            mstrHostName = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set IsFromWeb
    ''' Modify By: Pinkal
    ''' </summary>
    Public Property _IsFromWeb() As Boolean
        Get
            Return mblnIsFromWeb
        End Get
        Set(ByVal value As Boolean)
            mblnIsFromWeb = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set AuditUserId
    ''' Modify By: Pinkal
    ''' </summary>
    Public Property _AuditUserId() As Integer
        Get
            Return mintAuditUserId
        End Get
        Set(ByVal value As Integer)
            mintAuditUserId = value
        End Set
    End Property


    'Pinkal (12-Oct-2022) -- Start
    'NMB Loan Module Enhancement.

    ''' <summary>
    ''' Purpose: Get or Set OutStanding_PrincipalAmt
    ''' Modify By: Pinkal
    ''' </summary>
    Private mdecOutStanding_PrincipalAmt As Decimal = 0
    Public Property _OutStanding_PrincipalAmt() As Decimal
        Get
            Return mdecOutStanding_PrincipalAmt
        End Get
        Set(ByVal value As Decimal)
            mdecOutStanding_PrincipalAmt = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set OutStanding_InterestAmt
    ''' Modify By: Pinkal
    ''' </summary>
    Private mdecOutStanding_InterestAmt As Decimal = 0
    Public Property _OutStanding_InterestAmt() As Decimal
        Get
            Return mdecOutStanding_InterestAmt
        End Get
        Set(ByVal value As Decimal)
            mdecOutStanding_InterestAmt = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set PaidInstallments(
    ''' Modify By: Pinkal
    ''' </summary>
    Private mintPaidInstallments As Integer = 0
    Public Property _PaidInstallments() As Integer
        Get
            Return mintPaidInstallments
        End Get
        Set(ByVal value As Integer)
            mintPaidInstallments = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set TakeHomeAmount(
    ''' Modify By: Pinkal
    ''' </summary>
    Private mdecTakeHomeAmount As Decimal = 0
    Public Property _TakeHomeAmount() As Decimal
        Get
            Return mdecTakeHomeAmount
        End Get
        Set(ByVal value As Decimal)
            mdecTakeHomeAmount = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set IsTopup
    ''' Modify By: Pinkal
    ''' </summary>
    Private mblnIsTopup As Boolean = False
    Public Property _IsTopup() As Boolean
        Get
            Return mblnIsTopup
        End Get
        Set(ByVal value As Boolean)
            mblnIsTopup = value
        End Set
    End Property

    'Pinkal (12-Oct-2022) -- End

    'Pinkal (27-Oct-2022) -- Start
    'NMB Loan Module Enhancement.
    Private mblnRequiredReportingToApproval As Boolean = False
    Public Property _RequiredReportingToApproval() As Boolean
        Get
            Return mblnRequiredReportingToApproval
        End Get
        Set(ByVal value As Boolean)
            mblnRequiredReportingToApproval = value
        End Set
    End Property
    'Pinkal (27-Oct-2022) -- End

    'Pinkal (10-Nov-2022) -- Start
    'NMB Loan Module Enhancement.
    Private mblnApproval_DailyReminder As Boolean
    Public Property _IsApprovalDailyReminder() As Boolean
        Get
            Return mblnApproval_DailyReminder
        End Get
        Set(ByVal value As Boolean)
            mblnApproval_DailyReminder = value
        End Set
    End Property

    Private mdtLoanApproval_DailyReminder As DateTime = Nothing
    Public Property _LoanApproval_DailyReminder() As DateTime
        Get
            Return mdtLoanApproval_DailyReminder
        End Get
        Set(ByVal value As DateTime)
            mdtLoanApproval_DailyReminder = value
        End Set
    End Property

    Private mintEscalationDays As Integer
    Public Property _EscalationDays() As Integer
        Get
            Return mintEscalationDays
        End Get
        Set(ByVal value As Integer)
            mintEscalationDays = value
        End Set
    End Property

    Private mdtEscalationReminder As DateTime = Nothing
    Public Property _EscalationReminder() As DateTime
        Get
            Return mdtEscalationReminder
        End Get
        Set(ByVal value As DateTime)
            mdtEscalationReminder = value
        End Set
    End Property
    'Pinkal (10-Nov-2022) -- End

    'Pinkal (23-Nov-2022) -- Start
    'NMB Loan Module Enhancement.
    Private mstrLoanSchemeCode As String = String.Empty
    Public Property _LoanSchemeCode() As String
        Get
            Return mstrLoanSchemeCode
        End Get
        Set(ByVal value As String)
            mstrLoanSchemeCode = value
        End Set
    End Property

    Private mstrLoanScheme As String = String.Empty
    Public Property _LoanScheme() As String
        Get
            Return mstrLoanScheme
        End Get
        Set(ByVal value As String)
            mstrLoanScheme = value
        End Set
    End Property

    Private mdtCRBData As DataTable = Nothing
    Public Property _CRBData() As DataTable
        Get
            Return mdtCRBData
        End Get
        Set(ByVal value As DataTable)
            mdtCRBData = value
        End Set
    End Property

    Private mdtCRBAttachement As DataTable = Nothing
    Public Property _CRBAttachment() As DataTable
        Get
            Return mdtCRBAttachement
        End Get
        Set(ByVal value As DataTable)
            mdtCRBAttachement = value
        End Set
    End Property
    'Pinkal (23-Nov-2022) -- End

    'Hemant (25 Nov 2022) -- Start
    'ENHANCEMENT(NMB) : A1X-353 - As a user, I want to have offer letter reports for the loan scheme name - General loan
    ''' <summary>
    ''' Purpose: Get or Set transferunkid
    ''' Modify By: Hemant
    ''' </summary>
    Public Property _FinalApproverTransferunkid() As Integer
        Get
            Return mintFinalApproverTransferunkid
        End Get
        Set(ByVal value As Integer)
            mintFinalApproverTransferunkid = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set categorizationTranunkid
    ''' Modify By: Hemant
    ''' </summary>
    Public Property _FinalApproverCategorizationTranunkid() As Integer
        Get
            Return mintFinalApproverCategorizationTranunkid
        End Get
        Set(ByVal value As Integer)
            mintFinalApproverCategorizationTranunkid = value
        End Set
    End Property


    ''' <summary>
    ''' Purpose: Get or Set employeeunkid
    ''' Modify By: Hemant
    ''' </summary>
    Public Property _FinalApproverAtEmployeeunkid() As Integer
        Get
            Return mintFinalApproverAtEmployeeunkid
        End Get
        Set(ByVal value As Integer)
            mintFinalApproverAtEmployeeunkid = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set transferunkid
    ''' Modify By: Hemant
    ''' </summary>
    Public Property _CentralizedTransferunkid() As Integer
        Get
            Return mintCentralizedTransferunkid
        End Get
        Set(ByVal value As Integer)
            mintCentralizedTransferunkid = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set categorizationTranunkid
    ''' Modify By: Hemant
    ''' </summary>
    Public Property _CentralizedCategorizationTranunkid() As Integer
        Get
            Return mintCentralizedCategorizationTranunkid
        End Get
        Set(ByVal value As Integer)
            mintCentralizedCategorizationTranunkid = value
        End Set
    End Property


    ''' <summary>
    ''' Purpose: Get or Set employeeunkid
    ''' Modify By: Hemant
    ''' </summary>
    Public Property _CentralizedAtEmployeeunkid() As Integer
        Get
            Return mintCentralizedAtEmployeeunkid
        End Get
        Set(ByVal value As Integer)
            mintCentralizedAtEmployeeunkid = value
        End Set
    End Property
    'Hemant (25 Nov 2022) -- End

    'Pinkal (14-Dec-2022) -- Start
    'NMB Loan Module Enhancement.

    Private mdecFirstTranche As Decimal = 0
    Public Property _FirstTranche() As Decimal
        Get
            Return mdecFirstTranche
        End Get
        Set(ByVal value As Decimal)
            mdecFirstTranche = value
        End Set
    End Property

    Private mdecSecondTranche As Decimal = 0
    Public Property _SecondTranche() As Decimal
        Get
            Return mdecSecondTranche
        End Get
        Set(ByVal value As Decimal)
            mdecSecondTranche = value
        End Set
    End Property

    Private mdecThirdTranche As Decimal = 0
    Public Property _ThirdTranche() As Decimal
        Get
            Return mdecThirdTranche
        End Get
        Set(ByVal value As Decimal)
            mdecThirdTranche = value
        End Set
    End Property

    Private mdecFourTranche As Decimal = 0
    Public Property _FourTranche() As Decimal
        Get
            Return mdecFourTranche
        End Get
        Set(ByVal value As Decimal)
            mdecFourTranche = value
        End Set
    End Property

    'Pinkal (14-Dec-2022) -- End


    'Pinkal (04-Aug-2023) -- Start
    '(A1X-1158) NMB - Allow to configure up to 5 disbursement tranches for Mortgage loans on the loan approval page.
    Private mdecFifthTranche As Decimal = 0
    Public Property _FifthTranche() As Decimal
        Get
            Return mdecFifthTranche
        End Get
        Set(ByVal value As Decimal)
            mdecFifthTranche = value
        End Set
    End Property

    Private mdtFirstTrancheDate As DateTime = Nothing
    Public Property _FirstrancheDate() As DateTime
        Get
            Return mdtFirstTrancheDate
        End Get
        Set(ByVal value As DateTime)
            mdtFirstTrancheDate = value
        End Set
    End Property

    Private mdtSecondTrancheDate As DateTime = Nothing
    Public Property _SecondtrancheDate() As DateTime
        Get
            Return mdtSecondTrancheDate
        End Get
        Set(ByVal value As DateTime)
            mdtSecondTrancheDate = value
        End Set
    End Property

    Private mdtThirdTrancheDate As DateTime = Nothing
    Public Property _ThirdtrancheDate() As DateTime
        Get
            Return mdtThirdTrancheDate
        End Get
        Set(ByVal value As DateTime)
            mdtThirdTrancheDate = value
        End Set
    End Property

    Private mdtFourthTrancheDate As DateTime = Nothing
    Public Property _FourthtrancheDate() As DateTime
        Get
            Return mdtFourthTrancheDate
        End Get
        Set(ByVal value As DateTime)
            mdtFourthTrancheDate = value
        End Set
    End Property

    Private mdtFifthTrancheDate As DateTime = Nothing
    Public Property _FifthtrancheDate() As DateTime
        Get
            Return mdtFifthTrancheDate
        End Get
        Set(ByVal value As DateTime)
            mdtFifthTrancheDate = value
        End Set
    End Property
    'Pinkal (04-Aug-2023) -- End

#End Region

    ''' <summary>
    ''' Modify By: Pinkal
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>
    Public Sub GetData(Optional ByVal objDataOp As clsDataOperation = Nothing)
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        If objDataOp Is Nothing Then
            objDataOperation = New clsDataOperation
        Else
            objDataOperation = objDataOp
        End If

        Try
            strQ = "SELECT " & _
                      "  pendingloanaprovalunkid " & _
                      ", processpendingloanunkid " & _
                      ", employeeunkid " & _
                      ", mappingunkid " & _
                      ", roleunkid " & _
                      ", levelunkid " & _
                      ", priority " & _
                      ", approvaldate " & _
                      ", deductionperiodunkid " & _
                      ", loan_amount " & _
                      ", installmentamt " & _
                      ", noofinstallment " & _
                      ", countryunkid " & _
                      ", mapuserunkid " & _
                      ", isexceptional " & _
                      ", statusunkid " & _
                      ", visibleunkid " & _
                      ", remark " & _
                      ", userunkid " & _
                      ", isvoid " & _
                      ", voiddatetime " & _
                      ", voiduserunkid " & _
                      ", voidreason " & _
                      ", ISNULL(outst_princialamt,0.00) AS outst_princialamt " & _
                      ", ISNULL(outst_Interestamt,0.00) AS outst_Interestamt " & _
                      ", ISNULL(paidinstallments,0) AS paidinstallments " & _
                      ", ISNULL(take_homeamt,0.00) AS take_homeamt " & _
                      ", ISNULL(istopup,0) AS istopup " & _
                      ", approval_dailyreminder " & _
                      ", escalation_reminder " & _
                      ", ISNULL(first_tranche,0.00) AS first_tranche " & _
                      ", ISNULL(second_tranche,0.00) AS second_tranche " & _
                      ", ISNULL(third_tranche,0.00) AS third_tranche " & _
                      ", ISNULL(fourth_tranche,0.00) AS fourth_tranche " & _
                      ", ISNULL(fifth_tranche,0.00) AS fifth_tranche " & _
                      ", first_tranchedate " & _
                      ", second_tranchedate " & _
                      ", third_tranchedate " & _
                      ", fourth_tranchedate " & _
                      ", fifth_tranchedate " & _
                      " FROM lnroleloanapproval_process_tran " & _
                      " WHERE pendingloanaprovalunkid = @pendingloanaprovalunkid "

            'Pinkal (04-Aug-2023) -- (A1X-1158) NMB - Allow to configure up to 5 disbursement tranches for Mortgage loans on the loan approval page.[  ", ISNULL(fifth_tranche,0.00) AS fifth_tranche , first_tranchedate , second_tranchedate , third_tranchedate , fourth_tranchedate , fifth_tranchedate ]

            'Pinkal (14-Dec-2022) --    'NMB Loan Module Enhancement.[ ", ISNULL(first_tranche,0.00) AS first_tranche  ", ISNULL(second_tranche,0.00) AS second_tranche  ", ISNULL(third_tranche,0.00) AS third_tranche ", ISNULL(fourth_tranche,0.00) AS fourth_tranche " & _]

            'Pinkal (10-Nov-2022) -- NMB Loan Module Enhancement.[", approval_dailyreminder, escalation_reminder " & _]

            'Pinkal (12-Oct-2022) -- NMB Loan Module Enhancement.[  ", ISNULL(outst_princialamt,0.00) AS outst_princialamt , ISNULL(outst_Interestamt,0.00) AS outst_Interestamt , ISNULL(paidinstallments,0) AS paidinstallments ,  ISNULL(take_homeamt,0.00) AS take_homeamt , ISNULL(istopup,0) AS istopup ]

            objDataOperation.ClearParameters()
            objDataOperation.AddParameter("@pendingloanaprovalunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintPendingloanaprovalunkid.ToString)

            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            For Each dtRow As DataRow In dsList.Tables(0).Rows
                mintPendingloanaprovalunkid = CInt(dtRow.Item("pendingloanaprovalunkid"))
                mintProcesspendingloanunkid = CInt(dtRow.Item("processpendingloanunkid"))
                mintEmployeeunkid = CInt(dtRow.Item("employeeunkid"))
                mintMappingunkid = CInt(dtRow.Item("mappingunkid"))
                mintRoleunkid = CInt(dtRow.Item("roleunkid"))
                mintLevelunkid = CInt(dtRow.Item("levelunkid"))
                mintPriority = CInt(dtRow.Item("priority"))
                If IsDBNull(dtRow.Item("approvaldate")) = False Then
                    mdtApprovaldate = dtRow.Item("approvaldate")
                Else
                    mdtApprovaldate = Nothing
                End If
                mintDeductionperiodunkid = CInt(dtRow.Item("deductionperiodunkid"))
                mdecLoan_Amount = CDec(dtRow.Item("loan_amount"))
                mdecInstallmentamt = CDec(dtRow.Item("installmentamt"))
                mintNoofinstallment = CInt(dtRow.Item("noofinstallment"))
                mintCountryunkid = CInt(dtRow.Item("countryunkid"))
                mintMapUserunkid = CInt(dtRow.Item("mapuserunkid"))
                mblnIsExceptional = CBool(dtRow.Item("isexceptional"))
                mintStatusunkid = CInt(dtRow.Item("statusunkid"))
                mintVisibleunkid = CInt(dtRow.Item("visibleunkid"))
                mstrRemark = dtRow.Item("remark").ToString
                mintUserunkid = CInt(dtRow.Item("userunkid"))
                mblnIsvoid = CBool(dtRow.Item("isvoid"))
                If IsDBNull(dtRow.Item("voiddatetime")) = False Then
                    mdtVoiddatetime = dtRow.Item("voiddatetime")
                End If
                mintVoiduserunkid = CInt(dtRow.Item("voiduserunkid"))
                mstrVoidreason = dtRow.Item("voidreason").ToString

                'Pinkal (12-Oct-2022) -- Start
                'NMB Loan Module Enhancement.

                'Pinkal (20-Feb-2023) -- Start
                'NMB - On Loan Approval They want exclude product code CL27 in loan approval.
                'mdecOutStanding_PrincipalAmt = CDec(dtRow.Item("first_tranche"))
                mdecOutStanding_PrincipalAmt = CDec(dtRow.Item("outst_princialamt"))
                'Pinkal (20-Feb-2023) -- End

                mdecOutStanding_InterestAmt = CDec(dtRow.Item("outst_Interestamt"))
                mintPaidInstallments = CInt(dtRow.Item("paidinstallments"))
                mdecTakeHomeAmount = CDec(dtRow.Item("take_homeamt"))
                mblnIsTopup = CBool(dtRow.Item("istopup"))
                'Pinkal (12-Oct-2022) -- End


                'Pinkal (10-Nov-2022) -- Start
                'NMB Loan Module Enhancement.
                If dtRow.Item("approval_dailyreminder") IsNot Nothing AndAlso IsDBNull(dtRow.Item("approval_dailyreminder")) = False Then
                    mdtLoanApproval_DailyReminder = CDate(dtRow.Item("approval_dailyreminder"))
                Else
                    mdtLoanApproval_DailyReminder = Nothing
                End If

                If dtRow.Item("escalation_reminder") IsNot Nothing AndAlso IsDBNull(dtRow.Item("escalation_reminder")) = False Then
                    mdtEscalationReminder = CDate(dtRow.Item("escalation_reminder"))
                Else
                    mdtEscalationReminder = Nothing
                End If
                'Pinkal (10-Nov-2022) -- End

                'Pinkal (14-Dec-2022) -- Start
                'NMB Loan Module Enhancement.
                mdecFirstTranche = CDec(dtRow.Item("first_tranche"))
                mdecSecondTranche = CDec(dtRow.Item("second_tranche"))
                mdecThirdTranche = CDec(dtRow.Item("third_tranche"))
                mdecFourTranche = CDec(dtRow.Item("fourth_tranche"))
                'Pinkal (14-Dec-2022) -- End

                'Pinkal (04-Aug-2023) -- Start
                '(A1X-1158) NMB - Allow to configure up to 5 disbursement tranches for Mortgage loans on the loan approval page.
                mdecFifthTranche = CDec(dtRow.Item("fifth_tranche"))

                If IsDBNull(dtRow.Item("first_tranchedate")) = False Then
                    mdtFirstTrancheDate = CDate(dtRow.Item("first_tranchedate"))
                End If

                If IsDBNull(dtRow.Item("second_tranchedate")) = False Then
                    mdtSecondTrancheDate = CDate(dtRow.Item("second_tranchedate"))
                End If

                If IsDBNull(dtRow.Item("third_tranchedate")) = False Then
                    mdtThirdTrancheDate = CDate(dtRow.Item("third_tranchedate"))
                End If

                If IsDBNull(dtRow.Item("fourth_tranchedate")) = False Then
                    mdtFourthTrancheDate = CDate(dtRow.Item("fourth_tranchedate"))
                End If

                If IsDBNull(dtRow.Item("fifth_tranchedate")) = False Then
                    mdtFifthTrancheDate = CDate(dtRow.Item("fifth_tranchedate"))
                End If
                'Pinkal (04-Aug-2023) -- End

                Exit For
            Next
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: GetData; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            If objDataOp Is Nothing Then objDataOperation = Nothing
        End Try
    End Sub

    ''' <summary>
    ''' Modify By: Pinkal
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>
    Public Function GetRoleBasedApprovalList(ByVal xDatabaseName As String, _
                            ByVal xUserUnkid As Integer, _
                            ByVal xYearUnkid As Integer, _
                            ByVal xCompanyUnkid As Integer, _
                            ByVal xPeriodStart As DateTime, _
                            ByVal xPeriodEnd As DateTime, _
                            ByVal xUserModeSetting As String, _
                            ByVal xOnlyApproved As Boolean, _
                            ByVal xIncludeIn_ActiveEmployee As Boolean, _
                            ByVal strTableName As String, _
                            Optional ByVal mstrFilter As String = "", _
                            Optional ByVal strAdvanceFilter As String = "", _
                            Optional ByVal mblnIncludeGroup As Boolean = True, _
                            Optional ByVal objDataOp As clsDataOperation = Nothing) As DataSet
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        If objDataOp Is Nothing Then
            objDataOperation = New clsDataOperation
        Else
            objDataOperation = objDataOp
        End If

        Try

            Dim StrFinalQurey As String = String.Empty
            Dim StrQCondition As String = String.Empty
            Dim StrQDtFilters As String = String.Empty
            Dim StrQDataJoin As String = String.Empty

            Dim xDateJoinQry, xDateFilterQry, xAdvanceJoinQry As String
            xDateJoinQry = "" : xDateFilterQry = "" : xAdvanceJoinQry = ""
            Call GetDatesFilterString(xDateJoinQry, xDateFilterQry, xPeriodStart, xPeriodEnd, , , xDatabaseName, "Emp")
            Call GetAdvanceFilterQry(xAdvanceJoinQry, xPeriodEnd, xDatabaseName, "Emp")


            If mblnIncludeGroup Then

                strQ = " SELECT " & _
                          " -1 AS pendingloanaprovalunkid " & _
                          ",lna.processpendingloanunkid " & _
                          ",ln.application_no AS ApplicationNo " & _
                          ",NULL AS ApplicationDate " & _
                          ",lna.employeeunkid " & _
                          ",'' AS Employee " & _
                          ",'' AS EmployeeName " & _
                          ",-1 AS mappingunkid " & _
                          ",'' AS LoanScheme " & _
                          ",-1 AS roleunkid " & _
                          ",'' AS Role " & _
                          ",-1 AS levelunkid " & _
                          ",'' AS Level " & _
                          ",-1 AS priority " & _
                          ",NULL AS approvaldate " & _
                          ",-1 AS deductionperiodunkid " & _
                          ",'' AS Period " & _
                          ",0.00 AS loan_amount " & _
                          ",0.00 AS ApprovedAmt  " & _
                          ",0.00 AS lnApprovedAmt  " & _
                          ",0.00 AS installmentamt " & _
                          ",0 AS noofinstallment " & _
                          ",-1 AS countryunkid " & _
                          ",'' AS Currency " & _
                          ",'' Currencysign " & _
                          ",-1 AS mapuserunkid " & _
                          ",''  AS CUser " & _
                          ",'' AS loginuser " & _
                          ",''  AS UserName " & _
                          ",''  AS UserEmail " & _
                          ",ln.loan_statusunkid " & _
                          ",-1 AS statusunkid " & _
                          ",'' AS Status " & _
                          ",0 AS visibleunkid " & _
                          ",'' AS remark " & _
                          ",-1 AS userunkid " & _
                          ",0 AS isvoid " & _
                          ",NULL AS voiddatetime " & _
                          ",-1 AS voiduserunkid " & _
                          ",'' AS voidreason " & _
                          ",-1 AS stationunkid " & _
                          ",-1 AS deptgroupunkid " & _
                          " ,-1 AS departmentunkid " & _
                          " ,-1 AS sectiongroupunkid " & _
                          " ,-1 AS sectionunkid " & _
                          " ,-1 AS unitgroupunkid " & _
                          " ,-1 AS unitunkid " & _
                          " ,-1 AS teamunkid " & _
                          " ,-1 AS classgroupunkid " & _
                          " ,-1 AS classunkid " & _
                          " ,-1 AS jobgroupunkid " & _
                          " ,-1 AS jobunkid " & _
                          "  ,-1 AS gradegroupunkid " & _
                          "  ,-1 AS gradeunkid " & _
                          " ,-1 AS gradelevelunkid " & _
                          ", 1 AS isgrp " & _
                          ", ln.isexceptional " & _
                          ", 0.00 AS outst_princialamt " & _
                          ", 0.00 AS outst_Interestamt " & _
                          ", 0 AS paidinstallments " & _
                          ", 0.00 AS take_homeamt " & _
                          ", 0 AS istopup " & _
                          ", '' AS station " & _
                          ", '' AS deptgrp " & _
                          ", '' AS department " & _
                          ", '' AS sectiongrp " & _
                          ", '' AS section " & _
                          ", '' AS unitgrp " & _
                          ", '' AS unit " & _
                          ", '' AS team " & _
                          ", '' AS classgrp " & _
                          ", '' AS classes " & _
                          ", '' AS jobgrp " & _
                          ", '' AS job " & _
                          ", NULL AS approval_dailyreminder " & _
                          ", NULL AS escalation_reminder " & _
                          ", 0.00 AS first_tranche " & _
                          ", 0.00 AS second_tranche " & _
                          ", 0.00 AS third_tranche " & _
                          ", 0.00 AS fourth_tranche " & _
                          ", 0.00 AS fifth_tranche " & _
                          ", NULL AS first_tranchedate " & _
                          ", NULL AS second_tranchedate " & _
                          ", NULL AS third_tranchedate " & _
                          ", NULL AS fourth_tranchedate " & _
                          ", NULL AS fifth_tranchedate " & _
                          " FROM lnloan_process_pending_loan ln  " & _
                          " LEFT JOIN lnroleloanapproval_process_tran lna ON ln.processpendingloanunkid = lna.processpendingloanunkid AND ln.isvoid = 0 " & _
                          " LEFT JOIN hremployee_master AS Emp ON Emp.employeeunkid = ln.employeeunkid " & _
                          " LEFT JOIN hremployee_master rp ON rp.employeeunkid = lna.mapuserunkid " & _
                          " LEFT JOIN lnapproverlevel_master lvl ON lvl.lnlevelunkid = lna.levelunkid " & _
                          " LEFT JOIN cfcommon_period_tran p ON p.periodunkid = lna.deductionperiodunkid " & _
                          " LEFT JOIN cfexchange_rate ex ON ex.countryunkid = lna.countryunkid " & _
                          " LEFT JOIN hrmsConfiguration..cfrole_master r ON r.roleunkid = lna.roleunkid "

                'Pinkal (04-Aug-2023) -- (A1X-1158) NMB - Allow to configure up to 5 disbursement tranches for Mortgage loans on the loan approval page.[", 0.00 AS fifth_tranche , NULL AS first_tranchedate , NULL AS second_tranchedate , NULL AS third_tranchedate , NULL AS fourth_tranchedate , NULL AS fifth_tranchedate ]

                'Pinkal (14-Dec-2022) --    'NMB Loan Module Enhancement.[", 0.00 AS first_tranche , 0.00 AS second_tranche , 0.00 AS third_tranche , 0.00 AS fourth_tranche "]

                'Pinkal (10-Nov-2022) -- NMB Loan Module Enhancement.[", NULL AS approval_dailyreminder " & _]

                'Pinkal (12-Oct-2022) -- NMB Loan Module Enhancement.[  ", ISNULL(outst_princialamt,0.00) AS outst_princialamt , ISNULL(outst_Interestamt,0.00) AS outst_Interestamt , ISNULL(paidinstallments,0) AS paidinstallments , ISNULL(istopup,0) AS istopup ]

                If xDateJoinQry.Trim.Length > 0 Then
                    strQ &= xDateJoinQry '
                End If

                If xAdvanceJoinQry.Trim.Length > 0 Then
                    strQ &= xAdvanceJoinQry
                End If

                strQ &= " WHERE lna.isvoid = 0  "

                If xIncludeIn_ActiveEmployee = False Then
                    If xDateFilterQry.Trim.Length > 0 Then
                        strQ &= xDateFilterQry
                    End If
                End If

                If mstrFilter.Trim.Length > 0 Then
                    strQ &= " AND " & mstrFilter
                End If

                If strAdvanceFilter.Trim.Length > 0 Then
                    strQ &= " AND " & strAdvanceFilter
                End If

                strQ &= " UNION "

            End If

            strQ &= "   SELECT " & _
                       " lna.pendingloanaprovalunkid " & _
                       ",lna.processpendingloanunkid " & _
                       ",ln.application_no AS ApplicationNo " & _
                       ",ln.application_date AS ApplicationDate " & _
                       ",lna.employeeunkid " & _
                       ",ISNULL(Emp.employeecode, '') + ' - ' + ISNULL(Emp.firstname, '') + ' ' + ISNULL(Emp.surname, '') AS Employee " & _
                       ",ISNULL(Emp.firstname, '') + ' ' + ISNULL(Emp.surname, '') AS EmployeeName " & _
                       ",lna.mappingunkid" & _
                       ",ISNULL(s.name,'') AS LoanScheme " & _
                       ",lna.roleunkid " & _
                       ",ISNULL(r.name, '') AS Role " & _
                       ",lna.levelunkid " & _
                       ",ISNULL(lvl.lnlevelname, @ReportingTo) AS Level " & _
                       ",lna.priority " & _
                       ",lna.approvaldate " & _
                       ",lna.deductionperiodunkid " & _
                       ",ISNULL(p.period_name, '') AS Period " & _
                       ",ln.loan_amount  " & _
                       ",CASE WHEN lna.statusunkid = 1 THEN 0.00 " & _
                       "          WHEN lna.statusunkid = 2THEN lna.loan_amount " & _
                       "          WHEN lna.statusunkid = 3 THEN 0.00 END as ApprovedAmt " & _
                       ",ln.approved_amount AS lnApprovedAmt  " & _
                       ",lna.installmentamt " & _
                       ",lna.noofinstallment " & _
                       ",lna.countryunkid " & _
                       ",ISNULL(ex.currency_name, '') AS Currency " & _
                       ",ISNULL(ex.currency_sign, '') AS Currencysign " & _
                       ",ISNULL(u.userunkid,lna.mapuserunkid) AS mapuserunkid " & _
                       ",ISNULL(rp.employeecode, '') + ' - ' + ISNULL(rp.firstname, '') + ' ' + ISNULL(rp.surname, '')  + ' - ' + ISNULL(lvl.lnlevelname, @ReportingTo)  AS CUser " & _
                       ",ISNULL(u.username,'') AS loginuser " & _
                       ",ISNULL(u.firstname,'') + ' ' + ISNULL(u.lastname,'')  AS UserName " & _
                       ",ISNULL(u.email,'')  AS UserEmail " & _
                       ",ln.loan_statusunkid " & _
                       ",lna.statusunkid " & _
                       ",CASE WHEN lna.statusunkid =1 THEN @Pending " & _
                       "         WHEN lna.statusunkid =2 THEN @Approved " & _
                       "        WHEN lna.statusunkid =3 THEN  @Rejected " & _
                       "  END AS Status " & _
                       ",lna.visibleunkid " & _
                       ",lna.remark " & _
                       ",lna.userunkid " & _
                       ",lna.isvoid " & _
                       ",lna.voiddatetime " & _
                       ",lna.voiduserunkid " & _
                       ",lna.voidreason " & _
                       ",ISNULL(ETT.stationunkid, 0) AS stationunkid  " & _
                       ",ISNULL(ETT.deptgroupunkid, 0) AS deptgroupunkid  " & _
                       ",ISNULL(ETT.departmentunkid, 0) AS departmentunkid  " & _
                       ",ISNULL(ETT.sectiongroupunkid, 0) AS sectiongroupunkid  " & _
                       ",ISNULL(ETT.sectionunkid, 0) AS sectionunkid  " & _
                       ",ISNULL(ETT.unitgroupunkid, 0) AS unitgroupunkid  " & _
                       ",ISNULL(ETT.unitunkid, 0) AS unitunkid  " & _
                       ",ISNULL(ETT.teamunkid, 0) AS teamunkid  " & _
                       ",ISNULL(ETT.classgroupunkid, 0) AS classgroupunkid  " & _
                       ",ISNULL(ETT.classunkid, 0) AS classunkid  " & _
                       ",ISNULL(ECT.jobgroupunkid, 0) AS jobgroupunkid  " & _
                       ",ISNULL(ECT.jobunkid, 0) AS jobunkid  " & _
                       ",ISNULL(GRD.gradegroupunkid, 0) AS gradegroupunkid  " & _
                       ",ISNULL(GRD.gradeunkid, 0) AS gradeunkid  " & _
                       ",ISNULL(GRD.gradelevelunkid, 0) AS gradelevelunkid  " & _
                       ",0 AS isgrp " & _
                       ", ln.isexceptional " & _
                       ", ISNULL(lna.outst_princialamt,0.00) AS outst_princialamt " & _
                       ", ISNULL(lna.outst_Interestamt,0.00) AS outst_Interestamt " & _
                       ", ISNULL(lna.paidinstallments,0) AS paidinstallments " & _
                       ", ISNULL(lna.take_homeamt,0) AS take_homeamt " & _
                       ", ISNULL(lna.istopup,0) AS istopup " & _
                       ", ISNULL(hrstation_master.name,'') AS station " & _
                       ", ISNULL(hrdepartment_group_master.name,'') AS deptgrp " & _
                       ", ISNULL(hrdepartment_master.name,'') AS department " & _
                       ", ISNULL(hrsectiongroup_master.name,'') AS sectiongrp " & _
                       ", ISNULL(hrsection_master.name,'') AS section " & _
                       ", ISNULL(hrunitgroup_master.name,'') AS unitgrp " & _
                       ", ISNULL(hrunit_master.name,'') AS unit " & _
                       ", ISNULL(hrteam_master.name,'') AS team " & _
                       ", ISNULL(hrclassgroup_master.name,'') AS classgrp " & _
                       ", ISNULL(hrclasses_master.name,'') AS classes " & _
                       ", ISNULL(hrjobgroup_master.name,'') AS jobgrp " & _
                       ", ISNULL(hrjob_master.job_name,'') AS job " & _
                       ", lna.approval_dailyreminder " & _
                       ", lna.escalation_reminder " & _
                       ", ISNULL(first_tranche,0.00) AS first_tranche " & _
                       ", ISNULL(second_tranche,0.00) AS second_tranche " & _
                       ", ISNULL(third_tranche,0.00) AS third_tranche " & _
                       ", ISNULL(fourth_tranche,0.00) AS fourth_tranche " & _
                       ", ISNULL(fifth_tranche,0.00) AS fifth_tranche " & _
                       ", first_tranchedate " & _
                       ", second_tranchedate " & _
                       ", third_tranchedate " & _
                       ", fourth_tranchedate " & _
                       ", fifth_tranchedate " & _
                       " FROM lnroleloanapproval_process_tran lna " & _
                       " LEFT JOIN lnloan_process_pending_loan ln ON ln.processpendingloanunkid = lna.processpendingloanunkid AND ln.isvoid = 0 " & _
                       " LEFT JOIN lnloan_scheme_master s ON s.loanschemeunkid = ln.loanschemeunkid  " & _
                       " LEFT JOIN hremployee_master AS Emp ON Emp.employeeunkid = ln.employeeunkid " & _
                       " LEFT JOIN hremployee_master rp ON rp.employeeunkid = lna.mapuserunkid " & _
                       " LEFT JOIN lnapproverlevel_master lvl ON lvl.lnlevelunkid = lna.levelunkid " & _
                       " LEFT JOIN cfcommon_period_tran p ON p.periodunkid = lna.deductionperiodunkid " & _
                       " LEFT JOIN cfexchange_rate ex ON ex.countryunkid = lna.countryunkid " & _
                       " LEFT JOIN hrmsConfiguration..cfrole_master r ON r.roleunkid = lna.roleunkid " & _
                      " LEFT JOIN hrmsConfiguration..cfuser_master u ON u.employeeunkid = rp.employeeunkid AND u.ismanager = 1 " & _
                       " LEFT JOIN " & _
                       "( " & _
                       "   SELECT " & _
                       "        stationunkid " & _
                       "       ,deptgroupunkid " & _
                       "       ,departmentunkid " & _
                       "       ,sectiongroupunkid " & _
                       "       ,sectionunkid " & _
                       "       ,unitgroupunkid " & _
                       "       ,unitunkid " & _
                       "       ,teamunkid " & _
                       "       ,classgroupunkid " & _
                       "       ,classunkid " & _
                       "       ,employeeunkid " & _
                       "       ,ROW_NUMBER()OVER(PARTITION BY employeeunkid ORDER BY effectivedate DESC) AS rno " & _
                       "   FROM hremployee_transfer_tran " & _
                       "   WHERE isvoid = 0 AND CONVERT(CHAR(8),effectivedate,112) <= '" & eZeeDate.convertDate(xPeriodEnd) & "' " & _
                       ") AS ETT ON ETT.employeeunkid = Emp.employeeunkid AND ETT.rno = 1 " & _
                       " LEFT JOIN " & _
                       "( " & _
                        "   SELECT " & _
                        "        jobgroupunkid " & _
                        "       ,jobunkid " & _
                        "       ,employeeunkid " & _
                        "       ,ROW_NUMBER()OVER(PARTITION BY employeeunkid ORDER BY effectivedate DESC) AS rno " & _
                        "   FROM hremployee_categorization_tran " & _
                        "   WHERE isvoid = 0 AND CONVERT(CHAR(8),effectivedate,112) <= '" & eZeeDate.convertDate(xPeriodEnd) & "' " & _
                        ") AS ECT ON ECT.employeeunkid = Emp.employeeunkid AND ECT.rno = 1 " & _
                        " LEFT JOIN " & _
                        "( " & _
                        "   SELECT " & _
                        "        gradegroupunkid " & _
                        "       ,gradeunkid " & _
                        "       ,gradelevelunkid " & _
                        "       ,employeeunkid " & _
                        "       ,ROW_NUMBER()OVER(PARTITION BY employeeunkid ORDER BY incrementdate DESC,salaryincrementtranunkid DESC) AS rno " & _
                        "   FROM prsalaryincrement_tran " & _
                        "   WHERE isvoid = 0 AND prsalaryincrement_tran.isapproved = 1 AND CONVERT(CHAR(8),incrementdate,112) <= '" & eZeeDate.convertDate(xPeriodEnd) & "' " & _
                        " ) AS GRD ON GRD.employeeunkid = Emp.employeeunkid AND GRD.rno = 1 " & _
                        " LEFT JOIN hrstation_master ON ETT.stationunkid = hrstation_master.stationunkid AND hrstation_master.isactive = 1 " & _
                        " LEFT JOIN hrdepartment_group_master ON ETT.deptgroupunkid = hrdepartment_group_master.deptgroupunkid AND hrdepartment_group_master.isactive = 1 " & _
                        " LEFT JOIN hrdepartment_master ON ETT.departmentunkid = hrdepartment_master.departmentunkid AND hrdepartment_master.isactive = 1 " & _
                        " LEFT JOIN hrsectiongroup_master ON ETT.sectiongroupunkid = hrsectiongroup_master.sectiongroupunkid AND hrsectiongroup_master.isactive = 1 " & _
                        " LEFT JOIN hrsection_master ON ETT.sectionunkid = hrsection_master.sectionunkid AND hrsection_master.isactive = 1 " & _
                        " LEFT JOIN hrunitgroup_master ON ETT.unitgroupunkid = hrunitgroup_master.unitgroupunkid AND hrunitgroup_master.isactive = 1 " & _
                        " LEFT JOIN hrunit_master ON ETT.unitunkid = hrunit_master.unitunkid AND hrunit_master.isactive = 1 " & _
                        " LEFT JOIN hrteam_master ON ETT.teamunkid = hrteam_master.teamunkid AND hrteam_master.isactive = 1 " & _
                        " LEFT JOIN hrclassgroup_master ON ETT.classgroupunkid = hrclassgroup_master.classgroupunkid AND hrclassgroup_master.isactive = 1 " & _
                        " LEFT JOIN hrclasses_master ON ETT.classunkid = hrclasses_master.classesunkid AND hrclasses_master.isactive = 1 " & _
                        " LEFT JOIN hrjob_master ON ECT.jobunkid = hrjob_master.jobunkid AND hrjob_master.isactive = 1 " & _
                        " LEFT JOIN hrjobgroup_master ON ECT.jobgroupunkid = hrjobgroup_master.jobgroupunkid AND hrjobgroup_master.isactive = 1 "

            'Pinkal (04-Aug-2023) -- (A1X-1158) NMB - Allow to configure up to 5 disbursement tranches for Mortgage loans on the loan approval page.[" ISNULL(fifth_tranchedate,0.00) AS fifth_tranche, first_tranchedate , second_tranchedate , third_tranchedate , fourth_tranchedate , fifth_tranchedate ]

            'Pinkal (14-Dec-2022) --    'NMB Loan Module Enhancement.[ ", ISNULL(first_tranche,0.00) AS first_tranche  ", ISNULL(second_tranche,0.00) AS second_tranche  ", ISNULL(third_tranche,0.00) AS third_tranche ", ISNULL(fourth_tranche,0.00) AS fourth_tranche " & _]

            'Pinkal (10-Nov-2022) -- NMB Loan Module Enhancement.[  ", lna.approval_dailyreminder " & _]

            'Pinkal (12-Oct-2022) -- NMB Loan Module Enhancement.[  ", ISNULL(outst_princialamt,0.00) AS outst_princialamt , ISNULL(outst_Interestamt,0.00) AS outst_Interestamt , ISNULL(paidinstallments,0) AS paidinstallments , ISNULL(istopup,0) AS istopup ]


            If xDateJoinQry.Trim.Length > 0 Then
                strQ &= xDateJoinQry '
            End If

            If xAdvanceJoinQry.Trim.Length > 0 Then
                strQ &= xAdvanceJoinQry
            End If

            strQ &= " WHERE lna.isvoid = 0 AND lna.mappingunkid <=0 AND lna.roleunkid <= 0 AND lna.levelunkid <= 0 "

            If xIncludeIn_ActiveEmployee = False Then
                If xDateFilterQry.Trim.Length > 0 Then
                    strQ &= xDateFilterQry
                End If
            End If

            If mstrFilter.Trim.Length > 0 Then
                strQ &= " AND " & mstrFilter
            End If

            If strAdvanceFilter.Trim.Length > 0 Then
                strQ &= " AND " & strAdvanceFilter
            End If

            strQ &= " UNION " & _
                         " SELECT " & _
                         " lna.pendingloanaprovalunkid " & _
                         ",lna.processpendingloanunkid " & _
                         ",ln.application_no AS ApplicationNo " & _
                         ",ln.application_date AS ApplicationDate " & _
                         ",lna.employeeunkid " & _
                         ",ISNULL(Emp.employeecode, '') + ' - ' + ISNULL(Emp.firstname, '') + ' ' + ISNULL(Emp.surname, '') AS Employee " & _
                         ",ISNULL(Emp.firstname, '') + ' ' + ISNULL(Emp.surname, '') AS EmployeeName " & _
                         ",lna.mappingunkid " & _
                         ",ISNULL(s.name,'') AS LoanScheme " & _
                         ",lna.roleunkid " & _
                         ",ISNULL(r.name, '') AS Role " & _
                         ",lna.levelunkid " & _
                         ",ISNULL(lvl.lnlevelname, @ReportingTo) AS Level " & _
                         ",lna.priority " & _
                         ",lna.approvaldate " & _
                         ",lna.deductionperiodunkid " & _
                         ",ISNULL(p.period_name, '') AS Period " & _
                         ",ln.loan_amount  " & _
                         ",CASE WHEN lna.statusunkid = 1 THEN 0.00 " & _
                         "          WHEN lna.statusunkid = 2 THEN lna.loan_amount " & _
                         "          WHEN lna.statusunkid = 3 THEN 0.00 END as ApprovedAmt " & _
                         ",ln.approved_amount AS lnApprovedAmt  " & _
                         ",lna.installmentamt " & _
                         ",lna.noofinstallment " & _
                         ",lna.countryunkid " & _
                         ",ISNULL(ex.currency_name, '') AS Currency " & _
                         ",ISNULL(ex.currency_sign, '') AS Currencysign " & _
                         ",lna.mapuserunkid " & _
                         ",ISNULL(u.username,'') + ' - ' + ISNULL(lvl.lnlevelname, @ReportingTo)  AS CUser  " & _
                         ",ISNULL(u.username,'') AS loginuser  " & _
                         ",ISNULL(u.firstname,'') + ' ' + ISNULL(u.lastname,'')  AS UserName " & _
                         ",ISNULL(u.email,'')  AS UserEmail " & _
                         ",ln.loan_statusunkid " & _
                         ",lna.statusunkid " & _
                         ",CASE WHEN lna.statusunkid =1 THEN @Pending " & _
                         "          WHEN lna.statusunkid =2 THEN @Approved " & _
                         "          WHEN lna.statusunkid =3 THEN @Rejected " & _
                         "  END AS Status " & _
                         ",lna.visibleunkid " & _
                         ",lna.remark " & _
                         ",lna.userunkid " & _
                         ",lna.isvoid " & _
                         ",lna.voiddatetime " & _
                         ",lna.voiduserunkid " & _
                         ",lna.voidreason " & _
                         ",ISNULL(ETT.stationunkid, 0) AS stationunkid  " & _
                         ",ISNULL(ETT.deptgroupunkid, 0) AS deptgroupunkid  " & _
                         ",ISNULL(ETT.departmentunkid, 0) AS departmentunkid  " & _
                         ",ISNULL(ETT.sectiongroupunkid, 0) AS sectiongroupunkid  " & _
                         ",ISNULL(ETT.sectionunkid, 0) AS sectionunkid  " & _
                         ",ISNULL(ETT.unitgroupunkid, 0) AS unitgroupunkid  " & _
                         ",ISNULL(ETT.unitunkid, 0) AS unitunkid  " & _
                         ",ISNULL(ETT.teamunkid, 0) AS teamunkid  " & _
                         ",ISNULL(ETT.classgroupunkid, 0) AS classgroupunkid  " & _
                         ",ISNULL(ETT.classunkid, 0) AS classunkid  " & _
                         ",ISNULL(ECT.jobgroupunkid, 0) AS jobgroupunkid  " & _
                         ",ISNULL(ECT.jobunkid, 0) AS jobunkid  " & _
                         ",ISNULL(GRD.gradegroupunkid, 0) AS gradegroupunkid  " & _
                         ",ISNULL(GRD.gradeunkid, 0) AS gradeunkid  " & _
                         ",ISNULL(GRD.gradelevelunkid, 0) AS gradelevelunkid  " & _
                         ",0 AS isgrp " & _
                         ", ln.isexceptional " & _
                         ", ISNULL(lna.outst_princialamt,0.00) AS outst_princialamt " & _
                         ", ISNULL(lna.outst_Interestamt,0.00) AS outst_Interestamt " & _
                         ", ISNULL(lna.paidinstallments,0) AS paidinstallments " & _
                         ", ISNULL(lna.take_homeamt,0) AS take_homeamt " & _
                         ", ISNULL(lna.istopup,0) AS istopup " & _
                         ", ISNULL(hrstation_master.name,'') AS station " & _
                         ", ISNULL(hrdepartment_group_master.name,'') AS deptgrp " & _
                         ", ISNULL(hrdepartment_master.name,'') AS department " & _
                         ", ISNULL(hrsectiongroup_master.name,'') AS sectiongrp " & _
                         ", ISNULL(hrsection_master.name,'') AS section " & _
                         ", ISNULL(hrunitgroup_master.name,'') AS unitgrp " & _
                         ", ISNULL(hrunit_master.name,'') AS unit " & _
                         ", ISNULL(hrteam_master.name,'') AS team " & _
                         ", ISNULL(hrclassgroup_master.name,'') AS classgrp " & _
                         ", ISNULL(hrclasses_master.name,'') AS classes " & _
                         ", ISNULL(hrjobgroup_master.name,'') AS jobgrp " & _
                         ", ISNULL(hrjob_master.job_name,'') AS job " & _
                         ", lna.approval_dailyreminder " & _
                         ", lna.escalation_reminder " & _
                         ", ISNULL(first_tranche,0.00) AS first_tranche " & _
                         ", ISNULL(second_tranche,0.00) AS second_tranche " & _
                         ", ISNULL(third_tranche,0.00) AS third_tranche " & _
                         ", ISNULL(fourth_tranche,0.00) AS fourth_tranche " & _
                         ", ISNULL(fifth_tranche,0.00) AS fifth_tranche " & _
                         ", first_tranchedate " & _
                         ", second_tranchedate " & _
                         ", third_tranchedate " & _
                         ", fourth_tranchedate " & _
                         ", fifth_tranchedate " & _
                         " FROM lnroleloanapproval_process_tran lna " & _
                         " LEFT JOIN lnloan_process_pending_loan ln ON ln.processpendingloanunkid = lna.processpendingloanunkid AND ln.isvoid = 0 " & _
                         " LEFT JOIN lnloan_scheme_master s ON s.loanschemeunkid = ln.loanschemeunkid " & _
                         " LEFT JOIN hremployee_master AS Emp  ON Emp.employeeunkid = ln.employeeunkid " & _
                         " LEFT JOIN lnapproverlevel_master lvl ON lvl.lnlevelunkid = lna.levelunkid " & _
                         " LEFT JOIN cfcommon_period_tran p ON p.periodunkid = lna.deductionperiodunkid " & _
                         " LEFT JOIN cfexchange_rate ex ON ex.countryunkid = lna.countryunkid " & _
                         " LEFT JOIN hrmsConfiguration..cfrole_master r ON r.roleunkid = lna.roleunkid " & _
                         " LEFT JOIN hrmsConfiguration..cfuser_master u ON u.userunkid = lna.mapuserunkid " & _
                         " LEFT JOIN " & _
                         "( " & _
                         "   SELECT " & _
                         "        stationunkid " & _
                         "       ,deptgroupunkid " & _
                         "       ,departmentunkid " & _
                         "       ,sectiongroupunkid " & _
                         "       ,sectionunkid " & _
                         "       ,unitgroupunkid " & _
                         "       ,unitunkid " & _
                         "       ,teamunkid " & _
                         "       ,classgroupunkid " & _
                         "       ,classunkid " & _
                         "       ,employeeunkid " & _
                         "       ,ROW_NUMBER()OVER(PARTITION BY employeeunkid ORDER BY effectivedate DESC) AS rno " & _
                         "   FROM hremployee_transfer_tran " & _
                         "   WHERE isvoid = 0 AND CONVERT(CHAR(8),effectivedate,112) <= '" & eZeeDate.convertDate(xPeriodEnd) & "' " & _
                         ") AS ETT ON ETT.employeeunkid = Emp.employeeunkid AND ETT.rno = 1 " & _
                         " LEFT JOIN " & _
                         "( " & _
                          "   SELECT " & _
                          "        jobgroupunkid " & _
                          "       ,jobunkid " & _
                          "       ,employeeunkid " & _
                          "       ,ROW_NUMBER()OVER(PARTITION BY employeeunkid ORDER BY effectivedate DESC) AS rno " & _
                          "   FROM hremployee_categorization_tran " & _
                          "   WHERE isvoid = 0 AND CONVERT(CHAR(8),effectivedate,112) <= '" & eZeeDate.convertDate(xPeriodEnd) & "' " & _
                          ") AS ECT ON ECT.employeeunkid = Emp.employeeunkid AND ECT.rno = 1 " & _
                           " LEFT JOIN " & _
                           "( " & _
                            "   SELECT " & _
                            "        gradegroupunkid " & _
                            "       ,gradeunkid " & _
                            "       ,gradelevelunkid " & _
                            "       ,employeeunkid " & _
                            "       ,ROW_NUMBER()OVER(PARTITION BY employeeunkid ORDER BY incrementdate DESC,salaryincrementtranunkid DESC) AS rno " & _
                            "   FROM prsalaryincrement_tran " & _
                            "   WHERE isvoid = 0 AND prsalaryincrement_tran.isapproved = 1 AND CONVERT(CHAR(8),incrementdate,112) <= '" & eZeeDate.convertDate(xPeriodEnd) & "' " & _
                            " ) AS GRD ON GRD.employeeunkid = Emp.employeeunkid AND GRD.rno = 1 " & _
                            " LEFT JOIN hrstation_master ON ETT.stationunkid = hrstation_master.stationunkid AND hrstation_master.isactive = 1 " & _
                            " LEFT JOIN hrdepartment_group_master ON ETT.deptgroupunkid = hrdepartment_group_master.deptgroupunkid AND hrdepartment_group_master.isactive = 1 " & _
                            " LEFT JOIN hrdepartment_master ON ETT.departmentunkid = hrdepartment_master.departmentunkid AND hrdepartment_master.isactive = 1 " & _
                            " LEFT JOIN hrsectiongroup_master ON ETT.sectiongroupunkid = hrsectiongroup_master.sectiongroupunkid AND hrsectiongroup_master.isactive = 1 " & _
                            " LEFT JOIN hrsection_master ON ETT.sectionunkid = hrsection_master.sectionunkid AND hrsection_master.isactive = 1 " & _
                            " LEFT JOIN hrunitgroup_master ON ETT.unitgroupunkid = hrunitgroup_master.unitgroupunkid AND hrunitgroup_master.isactive = 1 " & _
                            " LEFT JOIN hrunit_master ON ETT.unitunkid = hrunit_master.unitunkid AND hrunit_master.isactive = 1 " & _
                            " LEFT JOIN hrteam_master ON ETT.teamunkid = hrteam_master.teamunkid AND hrteam_master.isactive = 1 " & _
                            " LEFT JOIN hrclassgroup_master ON ETT.classgroupunkid = hrclassgroup_master.classgroupunkid AND hrclassgroup_master.isactive = 1 " & _
                            " LEFT JOIN hrclasses_master ON ETT.classunkid = hrclasses_master.classesunkid AND hrclasses_master.isactive = 1 " & _
                            " LEFT JOIN hrjob_master ON ECT.jobunkid = hrjob_master.jobunkid AND hrjob_master.isactive = 1 " & _
                            " LEFT JOIN hrjobgroup_master ON ECT.jobgroupunkid = hrjobgroup_master.jobgroupunkid AND hrjobgroup_master.isactive = 1 "

            'Pinkal (04-Aug-2023) -- (A1X-1158) NMB - Allow to configure up to 5 disbursement tranches for Mortgage loans on the loan approval page.[" ISNULL(fifth_tranchedate,0.00) AS fifth_tranche, first_tranchedate , second_tranchedate , third_tranchedate , fourth_tranchedate , fifth_tranchedate ]

            'Pinkal (14-Dec-2022) --    'NMB Loan Module Enhancement.[ ", ISNULL(first_tranche,0.00) AS first_tranche  ", ISNULL(second_tranche,0.00) AS second_tranche  ", ISNULL(third_tranche,0.00) AS third_tranche ", ISNULL(fourth_tranche,0.00) AS fourth_tranche " & _]

            'Pinkal (10-Nov-2022) -- NMB Loan Module Enhancement.[  ", lna.approval_dailyreminder " & _]

            'Pinkal (12-Oct-2022) -- NMB Loan Module Enhancement.[  ", ISNULL(outst_princialamt,0.00) AS outst_princialamt , ISNULL(outst_Interestamt,0.00) AS outst_Interestamt , ISNULL(paidinstallments,0) AS paidinstallments , ISNULL(istopup,0) AS istopup ]

            If xDateJoinQry.Trim.Length > 0 Then
                strQ &= xDateJoinQry '
            End If

            If xAdvanceJoinQry.Trim.Length > 0 Then
                strQ &= xAdvanceJoinQry
            End If

            strQ &= " WHERE lna.isvoid = 0 AND lna.mappingunkid > 0 AND lna.roleunkid > 0 AND lna.levelunkid > 0 "


            If xIncludeIn_ActiveEmployee = False Then
                If xDateFilterQry.Trim.Length > 0 Then
                    strQ &= xDateFilterQry
                End If
            End If

            If mstrFilter.Trim.Length > 0 Then
                strQ &= " AND " & mstrFilter
            End If

            If strAdvanceFilter.Trim.Length > 0 Then
                strQ &= " AND " & strAdvanceFilter
            End If

            strQ &= " ORDER BY ApplicationNo DESC,priority,isgrp DESC "

            objDataOperation.ClearParameters()
            objDataOperation.AddParameter("@Pending", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 1, "Pending"))
            objDataOperation.AddParameter("@Approved", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 2, "Approved"))
            objDataOperation.AddParameter("@Rejected", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 3, "Rejected"))
            objDataOperation.AddParameter("@ReportingTo", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 4, "Reporting To"))
            dsList = objDataOperation.ExecQuery(strQ, strTableName)

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: GetRoleBasedApprovalList; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            If objDataOp Is Nothing Then objDataOperation = Nothing
        End Try
        Return dsList
    End Function

    ''' <summary>
    ''' Modify By: Pinkal
    ''' </summary>
    ''' <returns>Boolean</returns>
    ''' <purpose> INSERT INTO Database Table (lnroleloanapproval_process_tran) </purpose>
    Public Function Insert(Optional ByVal objDOperation As clsDataOperation = Nothing) As Boolean
        'If isExist(mstrName) Then
        '    mstrMessage = "<Message>"
        '    Return False
        'End If
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        If objDOperation Is Nothing Then
            objDataOperation = New clsDataOperation
        Else
            objDataOperation = objDOperation
        End If

        Try

            objDataOperation.ClearParameters()
            objDataOperation.AddParameter("@processpendingloanunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintProcesspendingloanunkid.ToString)
            objDataOperation.AddParameter("@employeeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintEmployeeunkid.ToString)
            objDataOperation.AddParameter("@mappingunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintMappingunkid.ToString)
            objDataOperation.AddParameter("@roleunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintRoleunkid.ToString)
            objDataOperation.AddParameter("@levelunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintLevelunkid.ToString)
            objDataOperation.AddParameter("@priority", SqlDbType.Int, eZeeDataType.INT_SIZE, mintPriority.ToString)
            If mdtApprovaldate <> Nothing Then
                objDataOperation.AddParameter("@approvaldate", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, mdtApprovaldate.ToString)
            Else
                objDataOperation.AddParameter("@approvaldate", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, DBNull.Value)
            End If
            objDataOperation.AddParameter("@deductionperiodunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintDeductionperiodunkid.ToString)
            objDataOperation.AddParameter("@loan_amount", SqlDbType.Decimal, eZeeDataType.DECIMAL_SIZE, mdecLoan_Amount.ToString)
            objDataOperation.AddParameter("@installmentamt", SqlDbType.Decimal, eZeeDataType.DECIMAL_SIZE, mdecInstallmentamt.ToString)
            objDataOperation.AddParameter("@noofinstallment", SqlDbType.Int, eZeeDataType.INT_SIZE, mintNoofinstallment.ToString)
            objDataOperation.AddParameter("@countryunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintCountryunkid.ToString)
            objDataOperation.AddParameter("@mapuserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintMapUserunkid.ToString)
            objDataOperation.AddParameter("@isexceptional", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsExceptional.ToString)
            objDataOperation.AddParameter("@statusunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintStatusunkid.ToString)
            objDataOperation.AddParameter("@visibleunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintVisibleunkid.ToString)
            objDataOperation.AddParameter("@remark", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrRemark.ToString)
            objDataOperation.AddParameter("@userunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintUserunkid.ToString)
            objDataOperation.AddParameter("@isvoid", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsvoid.ToString)
            If mdtVoiddatetime <> Nothing Then
                objDataOperation.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, mdtVoiddatetime.ToString)
            Else
                objDataOperation.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, DBNull.Value)
            End If

            objDataOperation.AddParameter("@voiduserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintVoiduserunkid.ToString)
            objDataOperation.AddParameter("@voidreason", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrVoidreason.ToString)

            'Pinkal (12-Oct-2022) -- Start
            'NMB Loan Module Enhancement.
            objDataOperation.AddParameter("@outst_princialamt", SqlDbType.Decimal, eZeeDataType.DECIMAL_SIZE, mdecOutStanding_PrincipalAmt)
            objDataOperation.AddParameter("@outst_Interestamt", SqlDbType.Decimal, eZeeDataType.DECIMAL_SIZE, mdecOutStanding_InterestAmt)
            objDataOperation.AddParameter("@paidinstallments", SqlDbType.Int, eZeeDataType.INT_SIZE, mintPaidInstallments)
            objDataOperation.AddParameter("@take_homeamt", SqlDbType.Decimal, eZeeDataType.DECIMAL_SIZE, mdecTakeHomeAmount)
            objDataOperation.AddParameter("@istopup", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsTopup.ToString)
            'Pinkal (12-Oct-2022) -- End

            'Pinkal (10-Nov-2022) -- Start
            'NMB Loan Module Enhancement.
            If mdtLoanApproval_DailyReminder <> Nothing Then
                objDataOperation.AddParameter("@approval_dailyreminder", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, mdtLoanApproval_DailyReminder)
            Else
                objDataOperation.AddParameter("@approval_dailyreminder", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, DBNull.Value)
            End If

            If mdtEscalationReminder <> Nothing Then
                objDataOperation.AddParameter("@escalation_reminder", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, mdtEscalationReminder)
            Else
                objDataOperation.AddParameter("@escalation_reminder", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, DBNull.Value)
            End If
            'Pinkal (10-Nov-2022) -- End


            'Pinkal (14-Dec-2022) -- Start
            'NMB Loan Module Enhancement.
            objDataOperation.AddParameter("@first_tranche", SqlDbType.Decimal, eZeeDataType.DECIMAL_SIZE, mdecFirstTranche)
            objDataOperation.AddParameter("@second_tranche", SqlDbType.Decimal, eZeeDataType.DECIMAL_SIZE, mdecSecondTranche)
            objDataOperation.AddParameter("@third_tranche", SqlDbType.Decimal, eZeeDataType.DECIMAL_SIZE, mdecThirdTranche)
            objDataOperation.AddParameter("@fourth_tranche", SqlDbType.Decimal, eZeeDataType.DECIMAL_SIZE, mdecFourTranche)
            'Pinkal (14-Dec-2022) -- End

            'Pinkal (04-Aug-2023) -- Start
            '(A1X-1158) NMB - Allow to configure up to 5 disbursement tranches for Mortgage loans on the loan approval page.
            objDataOperation.AddParameter("@fifth_tranche", SqlDbType.Decimal, eZeeDataType.DECIMAL_SIZE, mdecFifthTranche)

            If mdtFirstTrancheDate <> Nothing Then
                objDataOperation.AddParameter("@first_tranchedate", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, mdtFirstTrancheDate)
            Else
                objDataOperation.AddParameter("@first_tranchedate", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, DBNull.Value)
            End If

            If mdtSecondTrancheDate <> Nothing Then
                objDataOperation.AddParameter("@second_tranchedate", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, mdtSecondTrancheDate)
            Else
                objDataOperation.AddParameter("@second_tranchedate", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, DBNull.Value)
            End If

            If mdtThirdTrancheDate <> Nothing Then
                objDataOperation.AddParameter("@third_tranchedate", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, mdtThirdTrancheDate)
            Else
                objDataOperation.AddParameter("@third_tranchedate", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, DBNull.Value)
            End If

            If mdtFourthTrancheDate <> Nothing Then
                objDataOperation.AddParameter("@fourth_tranchedate", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, mdtFourthTrancheDate)
            Else
                objDataOperation.AddParameter("@fourth_tranchedate", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, DBNull.Value)
            End If

            If mdtFifthTrancheDate <> Nothing Then
                objDataOperation.AddParameter("@fifth_tranchedate", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, mdtFifthTrancheDate)
            Else
                objDataOperation.AddParameter("@fifth_tranchedate", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, DBNull.Value)
            End If
            'Pinkal (04-Aug-2023) -- End



            strQ = "INSERT INTO lnroleloanapproval_process_tran ( " & _
                      "  processpendingloanunkid " & _
                      ", employeeunkid " & _
                      ", mappingunkid " & _
                      ", roleunkid " & _
                      ", levelunkid " & _
                      ", priority " & _
                      ", approvaldate " & _
                      ", deductionperiodunkid " & _
                      ", loan_amount " & _
                      ", installmentamt " & _
                      ", noofinstallment " & _
                      ", countryunkid " & _
                      ", mapuserunkid " & _
                      ", isexceptional " & _
                      ", statusunkid " & _
                      ", visibleunkid " & _
                      ", remark " & _
                      ", userunkid " & _
                      ", isvoid " & _
                      ", voiddatetime " & _
                      ", voiduserunkid " & _
                      ", voidreason" & _
                      ", outst_princialamt " & _
                      ", outst_Interestamt " & _
                      ", paidinstallments " & _
                      ", take_homeamt " & _
                      ", istopup " & _
                      ", approval_dailyreminder " & _
                      ", escalation_reminder " & _
                      ", first_tranche " & _
                      ", second_tranche " & _
                      ", third_tranche " & _
                      ", fourth_tranche " & _
                      ", fifth_tranche " & _
                      ", first_tranchedate " & _
                      ", second_tranchedate " & _
                      ", third_tranchedate " & _
                      ", fourth_tranchedate " & _
                      ", fifth_tranchedate " & _
                    ") VALUES (" & _
                      "  @processpendingloanunkid " & _
                      ", @employeeunkid " & _
                      ", @mappingunkid " & _
                      ", @roleunkid " & _
                      ", @levelunkid " & _
                      ", @priority " & _
                      ", @approvaldate " & _
                      ", @deductionperiodunkid " & _
                      ", @loan_amount " & _
                      ", @installmentamt " & _
                      ", @noofinstallment " & _
                      ", @countryunkid " & _
                      ", @mapuserunkid " & _
                      ", @isexceptional " & _
                      ", @statusunkid " & _
                      ", @visibleunkid " & _
                      ", @remark " & _
                      ", @userunkid " & _
                      ", @isvoid " & _
                      ", @voiddatetime " & _
                      ", @voiduserunkid " & _
                      ", @voidreason" & _
                      ", @outst_princialamt " & _
                      ", @outst_Interestamt " & _
                      ", @paidinstallments " & _
                      ", @take_homeamt " & _
                      ", @istopup " & _
                      ", @approval_dailyreminder " & _
                      ", @escalation_reminder " & _
                      ", @first_tranche " & _
                      ", @second_tranche " & _
                      ", @third_tranche " & _
                      ", @fourth_tranche " & _
                      ", @fifth_tranche " & _
                      ", @first_tranchedate " & _
                      ", @second_tranchedate " & _
                      ", @third_tranchedate " & _
                      ", @fourth_tranchedate " & _
                      ", @fifth_tranchedate " & _
                    "); SELECT @@identity"

            'Pinkal (04-Aug-2023) --(A1X-1158) NMB - Allow to configure up to 5 disbursement tranches for Mortgage loans on the loan approval page.[ @fifth_tranche , @first_tranchedate , @second_tranchedate , @third_tranchedate , @fourth_tranchedate , @fifth_tranchedate ]

            'Pinkal (14-Dec-2022) -- NMB Loan Module Enhancement.[ ", @first_tranche , @second_tranche , @third_tranche , @fourth_tranche "]

            'Pinkal (23-Nov-2022) -- NMB Loan Module Enhancement.[ @crb_posteddata , @crb_responsedata , @iscrb_error ]

            'Pinkal (10-Nov-2022) -- NMB Loan Module Enhancement.[approval_dailyreminder,escalation_reminder]

            'Pinkal (12-Oct-2022) -- NMB Loan Module Enhancement.[outst_princialamt,outst_Interestamt,paidinstallments,take_homeamt,istopup]

            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            mintPendingloanaprovalunkid = dsList.Tables(0).Rows(0).Item(0)


            If InsertAuditTrailForRoleLoanApproval(objDataOperation, enAuditType.ADD) = False Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            If objDOperation Is Nothing Then objDataOperation.ReleaseTransaction(True)

            Return True
        Catch ex As Exception
            If objDOperation Is Nothing Then objDataOperation.ReleaseTransaction(False)
            Throw New Exception(ex.Message & "; Procedure Name: Insert; Module Name: " & mstrModuleName)
            Return False
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            If objDOperation Is Nothing Then objDataOperation = Nothing
        End Try
    End Function

    ''' <summary>
    ''' Modify By: Pinkal
    ''' </summary>
    ''' <returns>Boolean</returns>
    ''' <purpose> Update Database Table (lnroleloanapproval_process_tran) </purpose>
    Public Function Update(ByVal xDatabaseName As String, _
                                    ByVal xUserUnkid As Integer, _
                                    ByVal xYearUnkid As Integer, _
                                    ByVal xCompanyUnkid As Integer, _
                                    ByVal xPeriodStart As DateTime, _
                                    ByVal xPeriodEnd As DateTime, _
                                    ByVal xUserModeSetting As String, _
                                    ByVal xOnlyApproved As Boolean, _
                                    ByVal xIncludeIn_ActiveEmployee As Boolean, _
                                    Optional ByVal objDOperation As clsDataOperation = Nothing, _
                                    Optional ByVal mdtApplicationDate As DateTime = Nothing, _
                                    Optional ByVal mintLoanschemeunkid As Integer = -1) As Boolean

        'If isExist(mstrName, mintPendingloanaprovalunkid) Then
        '    mstrMessage = "<Message>"
        '    Return False
        'End If

        'Pinkal (27-Oct-2022) -- NMB Loan Module Enhancement.[Optional ByVal mdtApplicationDate As DateTime = Nothing,Optional ByVal mintLoanschemeunkid As Integer = -1]

        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        'Pinkal (04-Aug-2023) -- Start
        '(A1X-1158) NMB - Allow to configure up to 5 disbursement tranches for Mortgage loans on the loan approval page.
        Dim mstrEmpNotificationTrancheDateBeforeDays As String = ""
        Dim mintEmpNotificationTrancheDateBeforeDays As Integer = 0
        Dim objConfigOption As New clsConfigOptions
        mstrEmpNotificationTrancheDateBeforeDays = objConfigOption.GetKeyValue(xCompanyUnkid, "EmpNotificationTrancheDateBeforeDays", Nothing)
        If mstrEmpNotificationTrancheDateBeforeDays.Trim.Length > 0 Then mintEmpNotificationTrancheDateBeforeDays = mstrEmpNotificationTrancheDateBeforeDays.Trim
        objConfigOption = Nothing
        'Pinkal (04-Aug-2023) -- End




        If objDOperation Is Nothing Then
            objDataOperation = New clsDataOperation
            'Pinkal (27-Oct-2022) -- Start
            'NMB Loan Module Enhancement.
            objDataOperation.BindTransaction()
            'Pinkal (27-Oct-2022) -- End
        Else
            objDataOperation = objDOperation
        End If


        Try

            Dim xRoleId As Integer = mintRoleunkid
            Dim xLevelId As Integer = mintLevelunkid
            Dim xPriority As Integer = mintPriority
            'Pinkal (27-Oct-2022) -- Start
            'NMB Loan Module Enhancement.
            Dim xStatusId As Integer = mintStatusunkid
            'Pinkal (27-Oct-2022) -- End
            'Hemant (25 Nov 2022) -- Start
            'ENHANCEMENT(NMB) : A1X-353 - As a user, I want to have offer letter reports for the loan scheme name - General loan
            Dim xPendingloanaprovalunkid As Integer = mintPendingloanaprovalunkid
            'Hemant (25 Nov 2022) -- End

            objDataOperation.ClearParameters()
            objDataOperation.AddParameter("@pendingloanaprovalunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintPendingloanaprovalunkid.ToString)
            objDataOperation.AddParameter("@processpendingloanunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintProcesspendingloanunkid.ToString)
            objDataOperation.AddParameter("@employeeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintEmployeeunkid.ToString)
            objDataOperation.AddParameter("@mappingunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintMappingunkid.ToString)
            objDataOperation.AddParameter("@roleunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintRoleunkid.ToString)
            objDataOperation.AddParameter("@levelunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintLevelunkid.ToString)
            objDataOperation.AddParameter("@priority", SqlDbType.Int, eZeeDataType.INT_SIZE, mintPriority.ToString)
            If mdtApprovaldate <> Nothing Then
                objDataOperation.AddParameter("@approvaldate", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, mdtApprovaldate.ToString)
            Else
                objDataOperation.AddParameter("@approvaldate", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, DBNull.Value)
            End If
            objDataOperation.AddParameter("@deductionperiodunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintDeductionperiodunkid.ToString)
            objDataOperation.AddParameter("@loan_amount", SqlDbType.Decimal, eZeeDataType.DECIMAL_SIZE, mdecLoan_Amount.ToString)
            objDataOperation.AddParameter("@installmentamt", SqlDbType.Decimal, eZeeDataType.DECIMAL_SIZE, mdecInstallmentamt.ToString)
            objDataOperation.AddParameter("@noofinstallment", SqlDbType.Int, eZeeDataType.INT_SIZE, mintNoofinstallment.ToString)
            objDataOperation.AddParameter("@countryunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintCountryunkid.ToString)
            objDataOperation.AddParameter("@mapuserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintMapUserunkid.ToString)
            objDataOperation.AddParameter("@isexceptional", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsExceptional.ToString)
            objDataOperation.AddParameter("@statusunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintStatusunkid.ToString)
            objDataOperation.AddParameter("@visibleunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintVisibleunkid.ToString)
            objDataOperation.AddParameter("@remark", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrRemark.ToString)
            objDataOperation.AddParameter("@userunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintUserunkid.ToString)
            objDataOperation.AddParameter("@isvoid", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsvoid.ToString)
            If mdtVoiddatetime <> Nothing Then
                objDataOperation.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, mdtVoiddatetime.ToString)
            Else
                objDataOperation.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, DBNull.Value)
            End If

            objDataOperation.AddParameter("@voiduserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintVoiduserunkid.ToString)
            objDataOperation.AddParameter("@voidreason", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrVoidreason.ToString)

            'Pinkal (12-Oct-2022) -- Start
            'NMB Loan Module Enhancement.
            objDataOperation.AddParameter("@outst_princialamt", SqlDbType.Decimal, eZeeDataType.DECIMAL_SIZE, mdecOutStanding_PrincipalAmt)
            objDataOperation.AddParameter("@outst_Interestamt", SqlDbType.Decimal, eZeeDataType.DECIMAL_SIZE, mdecOutStanding_InterestAmt)
            objDataOperation.AddParameter("@paidinstallments", SqlDbType.Int, eZeeDataType.INT_SIZE, mintPaidInstallments)
            objDataOperation.AddParameter("@take_homeamt", SqlDbType.Decimal, eZeeDataType.DECIMAL_SIZE, mdecTakeHomeAmount)
            objDataOperation.AddParameter("@istopup", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsTopup.ToString)
            'Pinkal (12-Oct-2022) -- End

            'Pinkal (10-Nov-2022) -- Start
            'NMB Loan Module Enhancement.
            If mdtLoanApproval_DailyReminder <> Nothing Then
                objDataOperation.AddParameter("@approval_dailyreminder", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, mdtLoanApproval_DailyReminder)
            Else
                objDataOperation.AddParameter("@approval_dailyreminder", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, DBNull.Value)
            End If

            If mdtEscalationReminder <> Nothing Then
                objDataOperation.AddParameter("@escalation_reminder", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, mdtEscalationReminder)
            Else
                objDataOperation.AddParameter("@escalation_reminder", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, DBNull.Value)
            End If
            'Pinkal (10-Nov-2022) -- End

            'Pinkal (14-Dec-2022) -- Start
            'NMB Loan Module Enhancement.
            objDataOperation.AddParameter("@first_tranche", SqlDbType.Decimal, eZeeDataType.DECIMAL_SIZE, mdecFirstTranche)
            objDataOperation.AddParameter("@second_tranche", SqlDbType.Decimal, eZeeDataType.DECIMAL_SIZE, mdecSecondTranche)
            objDataOperation.AddParameter("@third_tranche", SqlDbType.Decimal, eZeeDataType.DECIMAL_SIZE, mdecThirdTranche)
            objDataOperation.AddParameter("@fourth_tranche", SqlDbType.Decimal, eZeeDataType.DECIMAL_SIZE, mdecFourTranche)
            'Pinkal (14-Dec-2022) -- End

            'Pinkal (04-Aug-2023) -- Start
            '(A1X-1158) NMB - Allow to configure up to 5 disbursement tranches for Mortgage loans on the loan approval page.
            objDataOperation.AddParameter("@fifth_tranche", SqlDbType.Decimal, eZeeDataType.DECIMAL_SIZE, mdecFifthTranche)

            If IsDBNull(mdtFirstTrancheDate) = False AndAlso mdtFirstTrancheDate <> Nothing Then
                objDataOperation.AddParameter("@first_tranchedate", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, mdtFirstTrancheDate)
            Else
                objDataOperation.AddParameter("@first_tranchedate", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, DBNull.Value)
            End If

            If IsDBNull(mdtSecondTrancheDate) = False AndAlso mdtSecondTrancheDate <> Nothing Then
                objDataOperation.AddParameter("@second_tranchedate", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, mdtSecondTrancheDate)
            Else
                objDataOperation.AddParameter("@second_tranchedate", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, DBNull.Value)
            End If

            If IsDBNull(mdtThirdTrancheDate) = False AndAlso mdtThirdTrancheDate <> Nothing Then
                objDataOperation.AddParameter("@third_tranchedate", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, mdtThirdTrancheDate)
            Else
                objDataOperation.AddParameter("@third_tranchedate", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, DBNull.Value)
            End If

            If IsDBNull(mdtFourthTrancheDate) = False AndAlso mdtFourthTrancheDate <> Nothing Then
                objDataOperation.AddParameter("@fourth_tranchedate", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, mdtFourthTrancheDate)
            Else
                objDataOperation.AddParameter("@fourth_tranchedate", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, DBNull.Value)
            End If

            If IsDBNull(mdtFifthTrancheDate) = False AndAlso mdtFifthTrancheDate <> Nothing Then
                objDataOperation.AddParameter("@fifth_tranchedate", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, mdtFifthTrancheDate)
            Else
                objDataOperation.AddParameter("@fifth_tranchedate", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, DBNull.Value)
            End If
            'Pinkal (04-Aug-2023) -- End


            strQ = "UPDATE lnroleloanapproval_process_tran SET " & _
                      "  processpendingloanunkid = @processpendingloanunkid" & _
                      ", employeeunkid = @employeeunkid" & _
                      ", mappingunkid = @mappingunkid" & _
                      ", roleunkid = @roleunkid" & _
                      ", levelunkid = @levelunkid" & _
                      ", priority = @priority" & _
                      ", approvaldate = @approvaldate" & _
                      ", deductionperiodunkid = @deductionperiodunkid" & _
                      ", loan_amount = @loan_amount" & _
                      ", installmentamt = @installmentamt" & _
                      ", noofinstallment = @noofinstallment" & _
                      ", countryunkid = @countryunkid" & _
                      ", mapuserunkid = @mapuserunkid " & _
                      ", isexceptional = @isexceptional " & _
                      ", statusunkid = @statusunkid" & _
                      ", visibleunkid = @visibleunkid" & _
                      ", remark = @remark" & _
                      ", userunkid = @userunkid" & _
                      ", isvoid = @isvoid" & _
                      ", voiddatetime = @voiddatetime" & _
                      ", voiduserunkid = @voiduserunkid" & _
                      ", voidreason = @voidreason " & _
                      ", outst_princialamt = @outst_princialamt " & _
                      ", outst_Interestamt = @outst_Interestamt " & _
                      ", paidinstallments = @paidinstallments " & _
                      ", take_homeamt = @take_homeamt " & _
                      ", istopup = @istopup " & _
                      ",approval_dailyreminder = @approval_dailyreminder " & _
                      ",escalation_reminder = @escalation_reminder " & _
                      ", first_tranche = @first_tranche " & _
                      ", second_tranche = @second_tranche " & _
                      ", third_tranche = @third_tranche " & _
                      ", fourth_tranche = @fourth_tranche " & _
                      ", fifth_tranche = @fifth_tranche " & _
                      ", first_tranchedate = @first_tranchedate " & _
                      ", second_tranchedate = @second_tranchedate " & _
                      ", third_tranchedate = @third_tranchedate " & _
                      ", fourth_tranchedate = @fourth_tranchedate " & _
                      ", fifth_tranchedate = @fifth_tranchedate " & _
                      " WHERE pendingloanaprovalunkid = @pendingloanaprovalunkid "

            'Pinkal (04-Aug-2023) -- (A1X-1158) NMB - Allow to configure up to 5 disbursement tranches for Mortgage loans on the loan approval page.[ fifth_tranche = @fifth_tranche , first_tranchedate = @first_tranchedate , second_tranchedate = @second_tranchedate , third_tranchedate = @third_tranchedate , fourth_tranchedate = @fourth_tranchedate , fifth_tranchedate = @fifth_tranchedate ]

            'Pinkal (14-Dec-2022) -- NMB Loan Module Enhancement.[", first_tranche = @first_tranche , second_tranche = @second_tranche , third_tranche = @third_tranche , fourth_tranche = @fourth_tranche " & _]

            'Pinkal (10-Nov-2022) -- NMB Loan Module Enhancement.[",approval_dailyreminder = @approval_dailyreminder ,escalation_reminder = @escalation_reminder"]

            'Pinkal (12-Oct-2022) -- NMB Loan Module Enhancement.[outst_princialamt = @outst_princialamt ,outst_Interestamt = @outst_Interestamt,paidinstallments = @paidinstallments,istopup = @istopup]

            Call objDataOperation.ExecNonQuery(strQ)

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            If mintStatusunkid = enLoanApplicationStatus.REJECTED Then
                If InsertAuditTrailForRoleLoanApproval(objDataOperation, enAuditType.EDIT) = False Then
                    exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                    Throw exForce
                End If
            End If

            strQ = " SELECT pendingloanaprovalunkid,processpendingloanunkid,mappingunkid, roleunkid,levelunkid,priority,visibleunkid FROM lnroleloanapproval_process_tran WHERE isvoid = 0 AND processpendingloanunkid = @processpendingloanunkid "
            objDataOperation.ClearParameters()
            objDataOperation.AddParameter("@processpendingloanunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintProcesspendingloanunkid.ToString)
            Dim dsApprover As DataSet = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            'Pinkal (27-Oct-2022) -- Start
            'NMB Loan Module Enhancement.
            If mintStatusunkid <> enLoanApplicationStatus.RETURNTOAPPLICANT Then

                Dim dtVisibility As DataTable = Nothing
                If dsApprover IsNot Nothing AndAlso dsApprover.Tables(0).Rows.Count > 0 Then

                    strQ = " UPDATE lnroleloanapproval_process_tran set " & _
                              " visibleunkid = " & mintStatusunkid & _
                              ", first_tranche = @first_tranche " & _
                              ", second_tranche = @second_tranche " & _
                              ", third_tranche = @third_tranche " & _
                              ", fourth_tranche = @fourth_tranche " & _
                              ", fifth_tranche = @fifth_tranche " & _
                              ", first_tranchedate = @first_tranchedate " & _
                              ", second_tranchedate = @second_tranchedate " & _
                              ", third_tranchedate = @third_tranchedate " & _
                              ", fourth_tranchedate = @fourth_tranchedate " & _
                              ", fifth_tranchedate = @fifth_tranchedate " & _
                              " WHERE  pendingloanaprovalunkid = @pendingloanaprovalunkid AND processpendingloanunkid = @processpendingloanunkid and employeeunkid = @employeeunkid " & _
                              " AND mappingunkid = @mappingunkid AND roleunkid = @roleunkid AND levelunkid = @levelunkid  AND isvoid = 0   "

                    'Pinkal (04-Aug-2023) -- (A1X-1158) NMB - Allow to configure up to 5 disbursement tranches for Mortgage loans on the loan approval page.[ fifth_tranche = @fifth_tranche , first_tranchedate = @first_tranchedate , second_tranchedate = @second_tranchedate , third_tranchedate = @third_tranchedate , fourth_tranchedate = @fourth_tranchedate , fifth_tranchedate = @fifth_tranchedate]

                    'Pinkal (14-Dec-2022) -- NMB Loan Module Enhancement.[", first_tranche = @first_tranche , second_tranche = @second_tranche , third_tranche = @third_tranche , fourth_tranche = @fourth_tranche " & _]

                    dtVisibility = New DataView(dsApprover.Tables(0), "priority = " & mintPriority, "", DataViewRowState.CurrentRows).ToTable

                    For i As Integer = 0 To dtVisibility.Rows.Count - 1
                        objDataOperation.ClearParameters()
                        objDataOperation.AddParameter("@pendingloanaprovalunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, dtVisibility.Rows(i)("pendingloanaprovalunkid").ToString)
                        objDataOperation.AddParameter("@processpendingloanunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, dtVisibility.Rows(i)("processpendingloanunkid").ToString)
                        objDataOperation.AddParameter("@employeeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintEmployeeunkid)
                        objDataOperation.AddParameter("@mappingunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, dtVisibility.Rows(i)("mappingunkid").ToString)
                        objDataOperation.AddParameter("@roleunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, dtVisibility.Rows(i)("roleunkid").ToString)
                        objDataOperation.AddParameter("@levelunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, dtVisibility.Rows(i)("levelunkid").ToString)

                        'Pinkal (14-Dec-2022) -- Start
                        'NMB Loan Module Enhancement.
                        objDataOperation.AddParameter("@first_tranche", SqlDbType.Decimal, eZeeDataType.DECIMAL_SIZE, mdecFirstTranche)
                        objDataOperation.AddParameter("@second_tranche", SqlDbType.Decimal, eZeeDataType.DECIMAL_SIZE, mdecSecondTranche)
                        objDataOperation.AddParameter("@third_tranche", SqlDbType.Decimal, eZeeDataType.DECIMAL_SIZE, mdecThirdTranche)
                        objDataOperation.AddParameter("@fourth_tranche", SqlDbType.Decimal, eZeeDataType.DECIMAL_SIZE, mdecFourTranche)
                        'Pinkal (14-Dec-2022) -- End

                        'Pinkal (04-Aug-2023) -- Start
                        '(A1X-1158) NMB - Allow to configure up to 5 disbursement tranches for Mortgage loans on the loan approval page.

                        objDataOperation.AddParameter("@fifth_tranche", SqlDbType.Decimal, eZeeDataType.DECIMAL_SIZE, mdecFifthTranche)

                        If IsDBNull(mdtFirstTrancheDate) = False AndAlso mdtFirstTrancheDate <> Nothing Then
                            objDataOperation.AddParameter("@first_tranchedate", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, mdtFirstTrancheDate)
                        Else
                            objDataOperation.AddParameter("@first_tranchedate", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, DBNull.Value)
                        End If

                        If IsDBNull(mdtSecondTrancheDate) = False AndAlso mdtSecondTrancheDate <> Nothing Then
                            objDataOperation.AddParameter("@second_tranchedate", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, mdtSecondTrancheDate)
                        Else
                            objDataOperation.AddParameter("@second_tranchedate", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, DBNull.Value)
                        End If

                        If IsDBNull(mdtThirdTrancheDate) = False AndAlso mdtThirdTrancheDate <> Nothing Then
                            objDataOperation.AddParameter("@third_tranchedate", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, mdtThirdTrancheDate)
                        Else
                            objDataOperation.AddParameter("@third_tranchedate", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, DBNull.Value)
                        End If

                        If IsDBNull(mdtFourthTrancheDate) = False AndAlso mdtFourthTrancheDate <> Nothing Then
                            objDataOperation.AddParameter("@fourth_tranchedate", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, mdtFourthTrancheDate)
                        Else
                            objDataOperation.AddParameter("@fourth_tranchedate", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, DBNull.Value)
                        End If

                        If IsDBNull(mdtFifthTrancheDate) = False AndAlso mdtFifthTrancheDate <> Nothing Then
                            objDataOperation.AddParameter("@fifth_tranchedate", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, mdtFifthTrancheDate)
                        Else
                            objDataOperation.AddParameter("@fifth_tranchedate", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, DBNull.Value)
                        End If
                        'Pinkal (04-Aug-2023) -- End


                        objDataOperation.ExecNonQuery(strQ)

                        If objDataOperation.ErrorMessage <> "" Then
                            exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                            Throw exForce
                        End If
                    Next

                End If

                Dim intMinPriority As Integer = dsApprover.Tables(0).AsEnumerable().Where(Function(x) x.Field(Of Integer)("priority") > mintPriority).Select(Function(x) x.Field(Of Integer)("priority")).DefaultIfEmpty().Min()

                dtVisibility = New DataView(dsApprover.Tables(0), "priority = " & intMinPriority & " AND priority <> -1", "", DataViewRowState.CurrentRows).ToTable

                If dtVisibility IsNot Nothing AndAlso dtVisibility.Rows.Count > 0 Then

                    strQ = " UPDATE lnroleloanapproval_process_tran set " & _
                              " visibleunkid = @visibleunkid " & _
                                  ", approval_dailyreminder = @approval_dailyreminder " & _
                                  ", escalation_reminder = @escalation_reminder " & _
                              ", first_tranche = @first_tranche " & _
                              ", second_tranche = @second_tranche " & _
                              ", third_tranche = @third_tranche " & _
                              ", fourth_tranche = @fourth_tranche " & _
                              ", fifth_tranche = @fifth_tranche " & _
                              ", first_tranchedate = @first_tranchedate " & _
                              ", second_tranchedate = @second_tranchedate " & _
                              ", third_tranchedate = @third_tranchedate " & _
                              ", fourth_tranchedate = @fourth_tranchedate " & _
                              ", fifth_tranchedate = @fifth_tranchedate " & _
                              " WHERE  pendingloanaprovalunkid = @pendingloanaprovalunkid AND processpendingloanunkid = @processpendingloanunkid  " & _
                              " AND employeeunkid = @employeeunkid AND mappingunkid = @mappingunkid AND roleunkid = @roleunkid AND levelunkid = @levelunkid AND isvoid = 0   "

                    'Pinkal (04-Aug-2023) -- (A1X-1158) NMB - Allow to configure up to 5 disbursement tranches for Mortgage loans on the loan approval page.[ fifth_tranche = @fifth_tranche , first_tranchedate = @first_tranchedate , second_tranchedate = @second_tranchedate , third_tranchedate = @third_tranchedate , fourth_tranchedate = @fourth_tranchedate , fifth_tranchedate = @fifth_tranchedate]

                    'Pinkal (14-Dec-2022) -- NMB Loan Module Enhancement.[", first_tranche = @first_tranche , second_tranche = @second_tranche , third_tranche = @third_tranche , fourth_tranche = @fourth_tranche " & _]

                    For i As Integer = 0 To dtVisibility.Rows.Count - 1
                        objDataOperation.ClearParameters()
                        objDataOperation.AddParameter("@pendingloanaprovalunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, dtVisibility.Rows(i)("pendingloanaprovalunkid").ToString)
                        objDataOperation.AddParameter("@processpendingloanunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, dtVisibility.Rows(i)("processpendingloanunkid").ToString)
                        objDataOperation.AddParameter("@employeeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintEmployeeunkid)
                        objDataOperation.AddParameter("@mappingunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, dtVisibility.Rows(i)("mappingunkid").ToString)
                        objDataOperation.AddParameter("@roleunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, dtVisibility.Rows(i)("roleunkid").ToString)
                        objDataOperation.AddParameter("@levelunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, dtVisibility.Rows(i)("levelunkid").ToString)
                        'Pinkal (14-Dec-2022) -- Start
                        'NMB Loan Module Enhancement.
                        objDataOperation.AddParameter("@first_tranche", SqlDbType.Decimal, eZeeDataType.DECIMAL_SIZE, mdecFirstTranche)
                        objDataOperation.AddParameter("@second_tranche", SqlDbType.Decimal, eZeeDataType.DECIMAL_SIZE, mdecSecondTranche)
                        objDataOperation.AddParameter("@third_tranche", SqlDbType.Decimal, eZeeDataType.DECIMAL_SIZE, mdecThirdTranche)
                        objDataOperation.AddParameter("@fourth_tranche", SqlDbType.Decimal, eZeeDataType.DECIMAL_SIZE, mdecFourTranche)
                        'Pinkal (14-Dec-2022) -- End

                        'Pinkal (04-Aug-2023) -- Start
                        '(A1X-1158) NMB - Allow to configure up to 5 disbursement tranches for Mortgage loans on the loan approval page.

                        objDataOperation.AddParameter("@fifth_tranche", SqlDbType.Decimal, eZeeDataType.DECIMAL_SIZE, mdecFifthTranche)

                        If IsDBNull(mdtFirstTrancheDate) = False AndAlso mdtFirstTrancheDate <> Nothing Then
                            objDataOperation.AddParameter("@first_tranchedate", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, mdtFirstTrancheDate)
                        Else
                            objDataOperation.AddParameter("@first_tranchedate", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, DBNull.Value)
                        End If

                        If IsDBNull(mdtSecondTrancheDate) = False AndAlso mdtSecondTrancheDate <> Nothing Then
                            objDataOperation.AddParameter("@second_tranchedate", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, mdtSecondTrancheDate)
                        Else
                            objDataOperation.AddParameter("@second_tranchedate", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, DBNull.Value)
                        End If

                        If IsDBNull(mdtThirdTrancheDate) = False AndAlso mdtThirdTrancheDate <> Nothing Then
                            objDataOperation.AddParameter("@third_tranchedate", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, mdtThirdTrancheDate)
                        Else
                            objDataOperation.AddParameter("@third_tranchedate", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, DBNull.Value)
                        End If

                        If IsDBNull(mdtFourthTrancheDate) = False AndAlso mdtFourthTrancheDate <> Nothing Then
                            objDataOperation.AddParameter("@fourth_tranchedate", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, mdtFourthTrancheDate)
                        Else
                            objDataOperation.AddParameter("@fourth_tranchedate", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, DBNull.Value)
                        End If

                        If IsDBNull(mdtFifthTrancheDate) = False AndAlso mdtFifthTrancheDate <> Nothing Then
                            objDataOperation.AddParameter("@fifth_tranchedate", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, mdtFifthTrancheDate)
                        Else
                            objDataOperation.AddParameter("@fifth_tranchedate", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, DBNull.Value)
                        End If
                        'Pinkal (04-Aug-2023) -- End

                        If mintStatusunkid = enLoanApplicationStatus.PENDING OrElse mintStatusunkid = enLoanApplicationStatus.APPROVED Then
                            objDataOperation.AddParameter("@visibleunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, enLoanApplicationStatus.PENDING)
                            'Pinkal (10-Nov-2022) -- Start
                            'NMB Loan Module Enhancement.
                            'If mblnApproval_DailyReminder AndAlso mdtLoanApproval_DailyReminder <> Nothing AndAlso IsDBNull(mdtLoanApproval_DailyReminder) = False Then
                            If mblnApproval_DailyReminder Then
                                objDataOperation.AddParameter("@approval_dailyreminder", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, Now.AddDays(1))
                            Else
                                objDataOperation.AddParameter("@approval_dailyreminder", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, DBNull.Value)
                            End If

                            If mintEscalationDays > 0 Then
                                objDataOperation.AddParameter("@escalation_reminder", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, Now.AddDays(mintEscalationDays))
                            Else
                                objDataOperation.AddParameter("@escalation_reminder", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, DBNull.Value)
                            End If
                            'Pinkal (10-Nov-2022) -- End
                        ElseIf mintStatusunkid = enLoanApplicationStatus.REJECTED Then
                            objDataOperation.AddParameter("@visibleunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, -1)
                            'Pinkal (10-Nov-2022) -- Start
                            'NMB Loan Module Enhancement.
                            objDataOperation.AddParameter("@approval_dailyreminder", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, DBNull.Value)
                            objDataOperation.AddParameter("@escalation_reminder", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, DBNull.Value)
                            'Pinkal (10-Nov-2022) -- End
                        End If

                        objDataOperation.ExecNonQuery(strQ)

                        If objDataOperation.ErrorMessage <> "" Then
                            exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                            Throw exForce
                        End If

                    Next

                End If

                'Pinkal (23-Nov-2022) -- Start
                'NMB Loan Module Enhancement.
                If mdtCRBData IsNot Nothing AndAlso mdtCRBData.Rows.Count > 0 Then
                    objCRBData._DtCRBData = mdtCRBData.Copy
                    objCRBData._Audittype = enAuditType.ADD
                    objCRBData._Form_Name = mstrFormName
                    objCRBData._Host = mstrHostName
                    objCRBData._Ip = mstrClientIP
                    objCRBData._Isweb = mblnIsFromWeb
                    If objCRBData.Insert(objDataOperation) = False Then
                        exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                        Throw exForce
                    End If
                End If

                If mdtCRBAttachement IsNot Nothing AndAlso mdtCRBAttachement.Rows.Count > 0 Then
                    Dim objDocument As New clsScan_Attach_Documents
                    Dim dtTran As DataTable = objDocument._Datatable
                    Dim docLocalPath As String = ConfigParameter._Object._Document_Path & "\"

                    Dim strFolderName As String = (New clsScan_Attach_Documents).GetDocFolderName("Docs", enScanAttactRefId.LOAN_APPROVAL).Tables(0).Rows(0)("Name").ToString
                    If strFolderName IsNot Nothing Then strFolderName = strFolderName & "\"
                    Dim dr As DataRow
                    For Each drow As DataRow In mdtCRBAttachement.Rows
                        dr = dtTran.NewRow
                        dr("scanattachtranunkid") = drow("scanattachtranunkid")
                        dr("documentunkid") = drow("documentunkid")
                        dr("employeeunkid") = drow("employeeunkid")
                        dr("filename") = drow("filename")
                        dr("scanattachrefid") = drow("scanattachrefid")
                        dr("modulerefid") = drow("modulerefid")
                        dr("form_name") = drow("form_name")
                        dr("userunkid") = drow("userunkid")
                        dr("transactionunkid") = mintPendingloanaprovalunkid
                        dr("attached_date") = drow("attached_date")
                        dr("orgfilepath") = drow("localpath")
                        dr("destfilepath") = docLocalPath & strFolderName & CStr(drow.Item("filename"))
                        dr("AUD") = drow("AUD")
                        dr("userunkid") = mintUserunkid
                        dr("fileuniquename") = drow("fileuniquename")
                        dr("filepath") = drow("filepath")
                        dr("filesize") = drow("filesize_kb")
                        dr("file_data") = drow("file_data")

                        dtTran.Rows.Add(dr)
                    Next
                    objDocument._Datatable = dtTran
                    objDocument.InsertUpdateDelete_Documents(objDataOperation)

                    If objDataOperation.ErrorMessage <> "" Then
                        exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                        Throw exForce
                    End If
                End If

                'Pinkal (23-Nov-2022) -- End

            Else

                If dsApprover IsNot Nothing AndAlso dsApprover.Tables(0).Rows.Count > 0 Then

                    Dim xVoidUserId As Integer = mintVoiduserunkid
                    Dim xVoidReason As String = mstrVoidreason
                    Dim xVoidDatetime As DateTime = mdtVoiddatetime

                    For i As Integer = 0 To dsApprover.Tables(0).Rows.Count - 1

                        mintVoiduserunkid = xVoidUserId
                        mstrVoidreason = xVoidReason
                        mdtVoiddatetime = xVoidDatetime

                        If Delete(CInt(dsApprover.Tables(0).Rows(i)("pendingloanaprovalunkid")), objDataOperation) = False Then
                            exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                            Throw exForce
                        End If

                        Dim mblnReporting As Boolean = False
                        _Pendingloanaprovalunkid(objDataOperation) = CInt(dsApprover.Tables(0).Rows(i)("pendingloanaprovalunkid"))

                        mintStatusunkid = enLoanApplicationStatus.PENDING
                        mblnIsvoid = False
                        mintVoiduserunkid = -1
                        mstrRemark = ""
                        mdtApprovaldate = Nothing
                        mdtVoiddatetime = Nothing
                        mstrVoidreason = String.Empty



                        If mblnRequiredReportingToApproval = True AndAlso (mintRoleunkid <= -1 AndAlso mintLevelunkid <= -1 AndAlso mintMappingunkid <= -1 AndAlso mintPriority <= -1) Then    'REPORTING TO
                            'Pinkal (10-Nov-2022) -- Start
                            'NMB Loan Module Enhancement.
                            'mintVisibleunkid = enLoanApplicationStatus.PENDING
                            mintVisibleunkid = -1
                            mdtLoanApproval_DailyReminder = Nothing
                            mdtEscalationReminder = Nothing
                            'Pinkal (10-Nov-2022) -- End
                            mblnReporting = True
                            If Insert(objDataOperation) = False Then
                                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                                Throw exForce
                            End If

                        Else
                            Dim mstrSearch As String = "mapuserunkid = " & mintMapUserunkid
                            Dim objLoanScheme As New clsloanscheme_role_mapping

                            'Pinkal (04-Aug-2023) -- Start
                            '(A1X-1158) NMB - Allow to configure up to 5 disbursement tranches for Mortgage loans on the loan approval page.
                            'Dim dtTable As DataTable = objLoanScheme.GetNextApprovers(xDatabaseName, xCompanyUnkid, xYearUnkid, xUserModeSetting, enUserPriviledge.AllowToApproveLoan, mdtApplicationDate, xUserUnkid, mintEmployeeunkid, mintLoanschemeunkid, mblnIsExceptional, mdecOutStanding_PrincipalAmt + mdecLoan_Amount, objDataOperation, "")
                            Dim dtTable As DataTable = objLoanScheme.GetNextApprovers(xDatabaseName, xCompanyUnkid, xYearUnkid, xUserModeSetting, enUserPriviledge.AllowToApproveLoan _
                                                                                                                      , mdtApplicationDate, xUserUnkid, mintEmployeeunkid, mintLoanschemeunkid, mblnIsExceptional _
                                                                                                                      , mdecOutStanding_PrincipalAmt + mdecLoan_Amount, False, objDataOperation, "")
                            'Pinkal (04-Aug-2023) -- End

                            Dim dtUserWise As DataTable = New DataView(dtTable, mstrSearch, "", DataViewRowState.CurrentRows).ToTable()
                            objLoanScheme = Nothing

                            Dim intMinPriority As Integer = -1

                            If dtUserWise IsNot Nothing AndAlso dtUserWise.Rows.Count > 0 Then

                                For Each drRow As DataRow In dtUserWise.Rows

                                    intMinPriority = CInt(dtTable.Compute("MIN(priority)", "1=1"))

                                    If intMinPriority = CInt(drRow("priority")) AndAlso mblnRequiredReportingToApproval = False Then
                                        'Pinkal (10-Nov-2022) -- Start
                                        'NMB Loan Module Enhancement.
                                        '  mintVisibleunkid = enLoanApplicationStatus.PENDING
                                        mintVisibleunkid = -1
                                        mdtLoanApproval_DailyReminder = Nothing
                                        mdtEscalationReminder = Nothing
                                        'Pinkal (10-Nov-2022) -- End
                                    Else
                                        mintVisibleunkid = -1
                                        'Pinkal (10-Nov-2022) -- Start
                                        'NMB Loan Module Enhancement.
                                        mdtLoanApproval_DailyReminder = Nothing
                                        mdtEscalationReminder = Nothing
                                        'Pinkal (10-Nov-2022) -- End
                                    End If '   If intMinPriority = CInt(drRow("priority")) AndAlso mblnRequiredReportingToApproval = False Then

                                    If Insert(objDataOperation) = False Then
                                        exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                                        Throw exForce
                                    End If

                                Next  '  For Each drRow As DataRow In dtTable.Rows 

                            End If  ' If dtTable IsNot Nothing AndAlso dtTable.Rows.Count > 0 Then

                        End If  'If mintRoleunkid <= -1 AndAlso mintLevelunkid <= -1 AndAlso mintMappingunkid <= -1 AndAlso mintPriority <= -1 Then    'REPORTING TO

                    Next  'For i As Integer = 0 To dsApprover.Tables(0).Rows.Count - 1


                    'Pinkal (23-Nov-2022) -- Start
                    'NMB Loan Module Enhancement.
                    objCRBData._Processpendingloanunkid(objDataOperation) = mintProcesspendingloanunkid
                    If objCRBData._DtCRBData IsNot Nothing AndAlso objCRBData._DtCRBData.Rows.Count > 0 Then
                        objCRBData._DtCRBData = objCRBData._DtCRBData.Copy
                        objCRBData._Audittype = enAuditType.DELETE
                        objCRBData._isvoid = True
                        objCRBData._Voiddatetime = Now
                        objCRBData._Voiduserunkid = mintUserunkid
                        objCRBData._Voidreason = "Return To Applicant"
                        objCRBData._Form_Name = mstrFormName
                        objCRBData._Host = mstrHostName
                        objCRBData._Ip = mstrClientIP
                        objCRBData._Isweb = mblnIsFromWeb
                        If objCRBData.Update(objDataOperation) = False Then
                            exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                            Throw exForce
                        End If
                    End If
                    'Pinkal (23-Nov-2022) -- End

                End If 'If dsApprover IsNot Nothing AndAlso dsApprover.Tables(0).Rows.Count > 0 Then

            End If  ' If mintStatusunkid <> enLoanApplicationStatus.RETURNTOAPPLICANT Then

            'Pinkal (27-Oct-2022) -- End

            If mintStatusunkid = enLoanApplicationStatus.APPROVED Then

                strQ = " SELECT pendingloanaprovalunkid,processpendingloanunkid,mappingunkid, roleunkid,levelunkid,priority,visibleunkid,approval_dailyreminder,escalation_reminder  " & _
                          " FROM lnroleloanapproval_process_tran  " & _
                          " WHERE isvoid = 0 AND processpendingloanunkid = @processpendingloanunkid AND employeeunkid = @employeeunkid AND priority >= @priority "

                'Pinkal (10-Nov-2022) -- NMB Loan Module Enhancement.[approval_dailyreminder,escalation_reminder]

                objDataOperation.ClearParameters()
                objDataOperation.AddParameter("@processpendingloanunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintProcesspendingloanunkid.ToString)
                objDataOperation.AddParameter("@employeeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintEmployeeunkid.ToString)
                objDataOperation.AddParameter("@priority", SqlDbType.Int, eZeeDataType.INT_SIZE, mintPriority.ToString)
                Dim dsData As DataSet = objDataOperation.ExecQuery(strQ, "List")

                If objDataOperation.ErrorMessage <> "" Then
                    exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                    Throw exForce
                End If


                strQ = " Update lnroleloanapproval_process_tran SET " & _
                          " deductionperiodunkid=@deductionperiodunkid" & _
                          ",loan_amount = @loan_amount " & _
                          ",installmentamt = @installmentamt " & _
                          ",noofinstallment = @noofinstallment " & _
                          ",countryunkid = @countryunkid " & _
                          " WHERE isvoid = 0 AND processpendingloanunkid = @processpendingloanunkid and employeeunkid = @employeeunkid AND priority >= @priority "

                objDataOperation.ClearParameters()
                objDataOperation.AddParameter("@deductionperiodunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintDeductionperiodunkid)
                objDataOperation.AddParameter("@loan_amount", SqlDbType.Decimal, eZeeDataType.DECIMAL_SIZE, mdecLoan_Amount)
                objDataOperation.AddParameter("@installmentamt", SqlDbType.Decimal, eZeeDataType.DECIMAL_SIZE, mdecInstallmentamt)
                objDataOperation.AddParameter("@noofinstallment", SqlDbType.Int, eZeeDataType.INT_SIZE, mintNoofinstallment)
                objDataOperation.AddParameter("@countryunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintCountryunkid)
                objDataOperation.AddParameter("@processpendingloanunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintProcesspendingloanunkid)
                objDataOperation.AddParameter("@employeeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintEmployeeunkid)
                objDataOperation.AddParameter("@priority", SqlDbType.Int, eZeeDataType.INT_SIZE, mintPriority)
                objDataOperation.ExecNonQuery(strQ)

                If objDataOperation.ErrorMessage <> "" Then
                    exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                    Throw exForce
                End If

                If dsData IsNot Nothing AndAlso dsData.Tables(0).Rows.Count > 0 Then
                    For i As Integer = 0 To dsData.Tables(0).Rows.Count - 1
                        _Pendingloanaprovalunkid(objDataOperation) = CInt(dsData.Tables(0).Rows(i)("pendingloanaprovalunkid"))

                        'Pinkal (10-Nov-2022) -- Start
                        'NMB Loan Module Enhancement.
                        mdtLoanApproval_DailyReminder = IIf(IsDBNull(dsData.Tables(0).Rows(i)("approval_dailyreminder")) = False, dsData.Tables(0).Rows(i)("approval_dailyreminder"), Nothing)
                        mdtEscalationReminder = IIf(IsDBNull(dsData.Tables(0).Rows(i)("escalation_reminder")) = False, dsData.Tables(0).Rows(i)("escalation_reminder"), Nothing)
                        'Pinkal (10-Nov-2022) -- End
                        mintAuditUserId = mintUserunkid

                        If InsertAuditTrailForRoleLoanApproval(objDataOperation, enAuditType.EDIT) = False Then
                            exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                            Throw exForce
                        End If
                    Next
                End If

            End If


            'Pinkal (04-Aug-2023) -- Start
            '(A1X-1158) NMB - Allow to configure up to 5 disbursement tranches for Mortgage loans on the loan approval page.
            _Pendingloanaprovalunkid(objDataOperation) = xPendingloanaprovalunkid
            'Pinkal (04-Aug-2023) -- End

            mintRoleunkid = xRoleId
            mintLevelunkid = xLevelId
            mintPriority = xPriority

            'NMB Loan Module Enhancement.
            mintStatusunkid = xStatusId
            'Pinkal (27-Oct-2022) -- End

            'Pinkal (14-Dec-2022) -- Start
            'NMB Loan Module Enhancement.
            mintUserunkid = xUserUnkid
            'Pinkal (14-Dec-2022) -- End


            'Pinkal (04-Aug-2023) -- Start
            '(A1X-1158) NMB - Allow to configure up to 5 disbursement tranches for Mortgage loans on the loan approval page.
            'If UpdateLoanApplicationStatus(xDatabaseName, xUserUnkid, xYearUnkid, xCompanyUnkid, xPeriodStart, xPeriodEnd, xUserModeSetting, xOnlyApproved, xIncludeIn_ActiveEmployee, objDataOperation) = False Then
            '    exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
            '    Throw exForce
            'End If
            If UpdateLoanApplicationStatus(xDatabaseName, xUserUnkid, xYearUnkid, xCompanyUnkid, xPeriodStart, xPeriodEnd, xUserModeSetting _
                                                        , xOnlyApproved, xIncludeIn_ActiveEmployee, mintEmpNotificationTrancheDateBeforeDays, objDataOperation) = False Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If
            'Pinkal (04-Aug-2023) -- End


            'Hemant (25 Nov 2022) -- Start
            'ENHANCEMENT(NMB) : A1X-353 - As a user, I want to have offer letter reports for the loan scheme name - General loan
            If mintStatusunkid = enLoanApplicationStatus.RETURNTOAPPLICANT Then
                Dim objLoanApplicationHistoryTran As New clsloanapplication_history_tran
                objLoanApplicationHistoryTran._Voiduserunkid = mintVoiduserunkid
                objLoanApplicationHistoryTran._Voidreason = mstrVoidreason
                If objLoanApplicationHistoryTran.DeleteByProcessPendingLoanUnkId(mintProcesspendingloanunkid, objDataOperation, True) = False Then
                    exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                    Throw exForce
                End If
                objLoanApplicationHistoryTran = Nothing
            Else
                Dim objProcess_pending_loan As New clsProcess_pending_loan
                objProcess_pending_loan._Processpendingloanunkid = mintProcesspendingloanunkid

                Dim objLoanApplicationHistoryTran As New clsloanapplication_history_tran
                objLoanApplicationHistoryTran._Processpendingloanunkid = mintProcesspendingloanunkid
                objLoanApplicationHistoryTran._Pendingloanaprovalunkid = xPendingloanaprovalunkid
                objLoanApplicationHistoryTran._Transferunkid = mintFinalApproverTransferunkid
                objLoanApplicationHistoryTran._CategorizationTranunkid = mintFinalApproverCategorizationTranunkid
                objLoanApplicationHistoryTran._AtEmployeeunkid = mintFinalApproverAtEmployeeunkid
                objLoanApplicationHistoryTran._IsApplicant = False
                objLoanApplicationHistoryTran._IsCentralized = False
                If objProcess_pending_loan._Loan_Statusunkid = enLoanApplicationStatus.APPROVED Then
                    objLoanApplicationHistoryTran._IsFinalApprover = True
                Else
                    objLoanApplicationHistoryTran._IsFinalApprover = False
                End If
                objLoanApplicationHistoryTran._AuditUserId = mintUserunkid

                objLoanApplicationHistoryTran._IsFromWeb = mblnIsFromWeb
                objLoanApplicationHistoryTran._ClientIP = mstrClientIP
                objLoanApplicationHistoryTran._FormName = mstrFormName
                objLoanApplicationHistoryTran._HostName = mstrHostName


                If objLoanApplicationHistoryTran.Insert(objDataOperation) = False Then
                    exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                    Throw exForce
                End If

                If objProcess_pending_loan._Loan_Statusunkid = enLoanApplicationStatus.APPROVED OrElse objProcess_pending_loan._Loan_Statusunkid = enLoanApplicationStatus.REJECTED Then

                    objLoanApplicationHistoryTran._Pendingloanaprovalunkid = 0
                    objLoanApplicationHistoryTran._Transferunkid = mintCentralizedTransferunkid
                    objLoanApplicationHistoryTran._CategorizationTranunkid = mintCentralizedCategorizationTranunkid
                    objLoanApplicationHistoryTran._AtEmployeeunkid = mintCentralizedAtEmployeeunkid
                    objLoanApplicationHistoryTran._IsApplicant = False
                    objLoanApplicationHistoryTran._IsCentralized = True
                    objLoanApplicationHistoryTran._IsFinalApprover = False

                    If objLoanApplicationHistoryTran.Insert(objDataOperation) = False Then
                        exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                        Throw exForce
                    End If

                End If
                objLoanApplicationHistoryTran = Nothing

                objProcess_pending_loan = Nothing
            End If

            'Hemant (25 Nov 2022) -- End
            If objDOperation Is Nothing Then objDataOperation.ReleaseTransaction(True)

            'Pinkal (23-Nov-2022) -- Start
            'NMB Loan Module Enhancement.
            Dim objLoanApplication As New clsProcess_pending_loan
            objLoanApplication._xDataOp = Nothing
            objLoanApplication._Processpendingloanunkid = mintProcesspendingloanunkid

            If objLoanApplication._Loan_Statusunkid = enLoanApplicationStatus.APPROVED Then

                Dim mstrEmpBankAcNo As String = ""
                'Dim mstrError As String = ""
                'Dim mstrPostedData As String = ""
                'Dim mstrResponseData As String = ""
                'Dim objMasterData As New clsMasterData
                Dim objconfig As New clsConfigOptions
                'Dim objLoanFlexCube As New clsLoanFlexCubeIntegration

                Dim mstrOracleHostName As String = objconfig.GetKeyValue(xCompanyUnkid, "OracleHostName", Nothing)
                Dim mstrOraclePortNo As String = objconfig.GetKeyValue(xCompanyUnkid, "OraclePortNo", Nothing)
                Dim mstrOracleServiceName As String = objconfig.GetKeyValue(xCompanyUnkid, "OracleServiceName", Nothing)
                Dim mstrOracleUserName As String = objconfig.GetKeyValue(xCompanyUnkid, "OracleUserName", Nothing)
                Dim mstrOracleUserPassword As String = objconfig.GetKeyValue(xCompanyUnkid, "OracleUserPassword", Nothing)
                Dim mstrNewLoanRequestFlexcubeURL As String = objconfig.GetKeyValue(xCompanyUnkid, "NewLoanRequestFlexcubeURL", Nothing)
                Dim mstrTopupRequestFlexcubeURL As String = objconfig.GetKeyValue(xCompanyUnkid, "TopupRequestFlexcubeURL", Nothing)


                'Pinkal (28-Apr-2023) -- Start
                '(A1X-860,A1X-861,A1X-862) NMB - Testing loan disbursement API - car loan,pre-paid   loan,mortgage loan    
                Dim mstrCarLoanRequestFlexcubeURL As String = objconfig.GetKeyValue(xCompanyUnkid, "CarLoanRequestFlexcubeURL", Nothing)
                Dim mstrMortgageLoanRequestFlexcubeURL As String = objconfig.GetKeyValue(xCompanyUnkid, "MortgageRequestFlexcubeURL", Nothing)
                Dim mstrPrepaidLoanRequestFlexcubeURL As String = objconfig.GetKeyValue(xCompanyUnkid, "PrepaidRequestFlexcubeURL", Nothing)
                'Pinkal (28-Apr-2023) -- End


                If objconfig.IsKeyExist(xCompanyUnkid, "Advance_CostCenterunkid", Nothing) = True Then
                    mstrOracleUserPassword = clsSecurity.Decrypt(mstrOracleUserPassword, "ezee")
                End If
                objconfig = Nothing

                If objLoanApplication._DeductionPeriodunkid > 0 Then
                    Dim dsEmpBank As DataSet = Nothing
                    Dim objEmpBankTran As New clsEmployeeBanks
                    dsEmpBank = objEmpBankTran.GetList(xDatabaseName, xUserUnkid, xYearUnkid, xCompanyUnkid, xPeriodStart, xPeriodEnd, xUserModeSetting, True, False, "EmployeeBank", False, "", objLoanApplication._Employeeunkid.ToString(), xPeriodEnd, "BankGrp, BranchName, EmpName, end_date DESC")
                    If dsEmpBank IsNot Nothing AndAlso dsEmpBank.Tables(0).Rows.Count > 0 Then
                        mstrEmpBankAcNo = dsEmpBank.Tables(0).Rows(0)("accountno").ToString()
                    End If
                    If dsEmpBank IsNot Nothing Then dsEmpBank.Clear()
                    dsEmpBank = Nothing
                End If

                'Dim byt As Byte() = System.Text.Encoding.UTF8.GetBytes(mstrOracleUserName.Trim & ":" & mstrOracleUserPassword)
                'Dim mstrBase64Credential As String = Convert.ToBase64String(byt)

                'If mstrNewLoanRequestFlexcubeURL.Trim.Length > 0 AndAlso mstrBase64Credential.Trim.Length > 0 AndAlso objLoanApplication._IsTopUp = False AndAlso mstrEmpBankAcNo.Trim.Length > 0 Then   'NEW LOAN REQUEST

                '    Dim mstrCustomerNo As String = GetFlexCubeEmpCustomerNo(mstrOracleHostName, mstrOraclePortNo, mstrOracleServiceName, mstrOracleUserName, mstrOracleUserPassword, mstrEmpBankAcNo)

                '    objLoanFlexCube.service = "aruti-loan-automation"
                '    objLoanFlexCube.type = "loan-request"
                '    objLoanFlexCube.payload.transactionReference = objLoanApplication._Application_No
                '    objLoanFlexCube.payload.applicationNumber = objLoanApplication._Application_No
                '    objLoanFlexCube.payload.accountNumber = mstrEmpBankAcNo
                '    objLoanFlexCube.payload.loanAccountNumber = Nothing
                '    objLoanFlexCube.payload.productCode = mstrLoanSchemeCode
                '    objLoanFlexCube.payload.productCategory = "STAFF"
                '    objLoanFlexCube.payload.customerNumber = mstrCustomerNo
                '    objLoanFlexCube.payload.maturityDate = New Date(xPeriodStart.AddMonths(objLoanApplication._NoOfInstallment).Year, xPeriodStart.AddMonths(objLoanApplication._NoOfInstallment).Month, 1).ToString("yyyy-MM-dd")
                '    objLoanFlexCube.payload.loanAmount = objLoanApplication._Loan_Amount
                '    objLoanFlexCube.payload.currency = "TZS"
                '    objLoanFlexCube.payload.remark = "Loan request"
                '    objLoanFlexCube.payload.loanAccountNumber = Nothing
                '    objLoanFlexCube.payload.outStandingPrincipal = Nothing
                '    objLoanFlexCube.payload.outStandingInterest = Nothing
                '    objLoanFlexCube.payload.totalOutstanding = Nothing
                '    objLoanFlexCube.payload.valueDate = Nothing

                '    If objMasterData.GetSetFlexCubeLoanRequest(mstrNewLoanRequestFlexcubeURL, mstrBase64Credential, objLoanFlexCube, mstrError, mstrPostedData, mstrResponseData) = False Then
                '        exForce = New Exception("New Loan Request Flexcube Error : " & mstrError)
                '        Throw exForce
                '    End If


                'ElseIf mstrTopupRequestFlexcubeURL.Trim.Length > 0 AndAlso mstrBase64Credential.Trim.Length > 0 AndAlso objLoanApplication._IsTopUp AndAlso mstrEmpBankAcNo.Trim.Length > 0 Then 'TOP UP LOAN REQUEST

                '    Dim mstrCustomerNo As String = GetFlexCubeEmpCustomerNo(mstrOracleHostName, mstrOraclePortNo, mstrOracleServiceName, mstrOracleUserName, mstrOracleUserPassword, mstrEmpBankAcNo)

                '    Dim objLoanAdvance As New clsLoan_Advance
                '    Dim dsFlexcube As DataSet = objLoanAdvance.GetFlexcubeLoanData(mstrOracleHostName, mstrOraclePortNo, mstrOracleServiceName, mstrOracleUserName, mstrOracleUserPassword, mstrEmpBankAcNo, mstrLoanSchemeCode, False)
                '    objLoanAdvance = Nothing

                '    If dsFlexcube IsNot Nothing AndAlso dsFlexcube.Tables(0).Rows.Count > 0 Then
                '        objLoanFlexCube.service = "aruti-loan-automation"
                '        objLoanFlexCube.type = "loan-roll-over"
                '        objLoanFlexCube.payload.transactionReference = mstrLoanSchemeCode + objLoanApplication._Application_No
                '        objLoanFlexCube.payload.applicationNumber = mstrLoanSchemeCode + objLoanApplication._Application_No
                '        objLoanFlexCube.payload.accountNumber = mstrEmpBankAcNo
                '        objLoanFlexCube.payload.loanAccountNumber = dsFlexcube.Tables(0).Rows(0)("ACCOUNT_NUMBER").ToString()
                '        objLoanFlexCube.payload.productCode = mstrLoanSchemeCode
                '        objLoanFlexCube.payload.customerNumber = mstrCustomerNo
                '        objLoanFlexCube.payload.outStandingPrincipal = mdecOutStanding_PrincipalAmt
                '        'objLoanFlexCube.payload.loanAmount = mdecOutStanding_PrincipalAmt + mdecLoan_Amount
                '        objLoanFlexCube.payload.loanAmount = mdecLoan_Amount
                '        objLoanFlexCube.payload.totalOutstanding = mdecOutStanding_PrincipalAmt + mdecOutStanding_InterestAmt
                '        objLoanFlexCube.payload.currency = "TZS"
                '        'objLoanFlexCube.payload.maturityDate = CDate(dsFlexcube.Tables(0).Rows(0)("MATURITY_DATE").ToString()).ToString("yyyy-MM-dd")
                '        objLoanFlexCube.payload.maturityDate = New Date(xPeriodStart.AddMonths(objLoanApplication._NoOfInstallment).Year, xPeriodStart.AddMonths(objLoanApplication._NoOfInstallment).Month, 1).ToString("yyyy-MM-dd")

                '        'TEMPARORY SET DATE 05-SEP-2022 FOR UAT REMOVE IT AS SOON AS POSSIBLE
                '        'objLoanFlexCube.payload.valueDate = Now.Date.ToString("yyyy-MM-dd")
                '        objLoanFlexCube.payload.valueDate = "2022-09-05"
                '        'TEMPARORY SET DATE 05-SEP-2022 FOR UAT REMOVE IT AS SOON AS POSSIBLE

                '        objLoanFlexCube.payload.outStandingInterest = mdecOutStanding_InterestAmt
                '        objLoanFlexCube.payload.remark = "Loan request"
                '        objLoanFlexCube.payload.productCategory = Nothing

                '        If objMasterData.GetSetFlexCubeLoanRequest(mstrTopupRequestFlexcubeURL, mstrBase64Credential, objLoanFlexCube, mstrError, mstrPostedData, mstrResponseData) = False Then
                '            exForce = New Exception("Top up Request Flexcube Error : " & mstrError)
                '            Throw exForce
                '        End If

                '    End If  ' If dsFlexcube IsNot Nothing AndAlso dsFlexcube.Tables(0).Rows.Count > 0 Then

                'End If  ' If mstrNewLoanRequestFlexcubeURL.Trim.Length > 0 AndAlso mstrBase64Credential.Trim.Length > 0 AndAlso objLoanApplication._IsTopUp = False AndAlso mstrEmpBankAcNo.Trim.Length > 0 Then   'NEW LOAN REQUEST 
                'objLoanFlexCube = Nothing
                'objMasterData = Nothing

                'objLoanApplication._PostedData = mstrPostedData
                'objLoanApplication._ReponseData = mstrResponseData

                'Dim dtTable As DataTable = JsonStringToDataTable(mstrResponseData)
                'If dtTable IsNot Nothing AndAlso dtTable.Rows.Count > 0 Then
                '    If CInt(dtTable.Rows(0)("statusCode")) <> 600 Then
                '        objLoanApplication._IsPostedError = True
                '    End If
                'End If

                'objLoanApplication._Userunkid = mintUserunkid
                'objLoanApplication._WebClientIP = mstrClientIP
                'objLoanApplication._WebFormName = mstrFormName
                'objLoanApplication._WebHostName = mstrHostName

                'If objLoanApplication.Update(xDatabaseName, xUserUnkid, xYearUnkid, xCompanyUnkid, xPeriodStart, xPeriodEnd, xUserModeSetting, xOnlyApproved, xIncludeIn_ActiveEmployee, False, Nothing, Nothing) = False Then
                '    exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                '    Throw exForce
                'End If

                'Pinkal (28-Apr-2023) -- Start
                '(A1X-860,A1X-861,A1X-862) NMB - Testing loan disbursement API - car loan,pre-paid   loan,mortgage loan    
                Select Case mstrLoanSchemeCode.Trim().ToUpper
                    Case "CL17"    'Car Loan
                        mstrNewLoanRequestFlexcubeURL = mstrCarLoanRequestFlexcubeURL

                    Case "CL25", "CL27"  'Annual Loan , Prepaid Loan
                        mstrNewLoanRequestFlexcubeURL = mstrPrepaidLoanRequestFlexcubeURL

                    Case "CL28"   'Mortgage Loan
                        mstrNewLoanRequestFlexcubeURL = mstrMortgageLoanRequestFlexcubeURL
                End Select
                'Pinkal (28-Apr-2023) -- End


                If SendFlexcubeRequest(mstrOracleHostName, mstrOraclePortNo, mstrOracleServiceName, mstrOracleUserName, mstrOracleUserPassword, mstrNewLoanRequestFlexcubeURL, mstrTopupRequestFlexcubeURL _
                                               , xDatabaseName, xUserUnkid, xYearUnkid, xCompanyUnkid, xPeriodStart, xPeriodEnd, xUserModeSetting, xOnlyApproved, xIncludeIn_ActiveEmployee _
                                               , mdecOutStanding_PrincipalAmt, mdecOutStanding_InterestAmt, mstrEmpBankAcNo, mstrLoanSchemeCode, mintProcesspendingloanunkid) = False Then
                    exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                    Throw exForce
                End If

            End If
            'objLoanApplication = Nothing
            'Pinkal (23-Nov-2022) -- End

            Return True
        Catch ex As Exception
            If objDOperation Is Nothing Then objDataOperation.ReleaseTransaction(False)
            Throw New Exception(ex.Message & "; Procedure Name: Update; Module Name: " & mstrModuleName)
            Return False
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            If objDOperation Is Nothing Then objDataOperation = Nothing
        End Try
    End Function

    'Pinkal (27-Oct-2022) -- Start
    'NMB Loan Module Enhancement.

    ''' <summary>
    ''' Modify By: Pinkal
    ''' </summary>
    ''' <returns>Boolean</returns>
    ''' <purpose> Delete Database Table (lnroleloanapproval_process_tran) </purpose>
    Public Function Delete(ByVal intUnkid As Integer, Optional ByVal objDOperation As clsDataOperation = Nothing) As Boolean
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        If objDOperation Is Nothing Then
            objDataOperation = New clsDataOperation
            objDataOperation.BindTransaction()
        Else
            objDataOperation = objDOperation
        End If


        Try

            strQ = " UPDATE lnroleloanapproval_process_tran SET " & _
                      " isvoid = @isvoid,voiduserunkid = @voiduserunkid " & _
                      ",voiddatetime = GetDate(),voidreason = @voidreason " & _
            "WHERE pendingloanaprovalunkid = @pendingloanaprovalunkid "

            objDataOperation.ClearParameters()
            objDataOperation.AddParameter("@pendingloanaprovalunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intUnkid)
            objDataOperation.AddParameter("@voiduserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintVoiduserunkid)
            objDataOperation.AddParameter("@voidreason", SqlDbType.NVarChar, eZeeDataType.DESC_SIZE, mstrVoidreason)
            objDataOperation.AddParameter("@isvoid", SqlDbType.Int, eZeeDataType.INT_SIZE, True)

            Call objDataOperation.ExecNonQuery(strQ)

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            _Pendingloanaprovalunkid(objDataOperation) = intUnkid

            If InsertAuditTrailForRoleLoanApproval(objDataOperation, enAuditType.DELETE) = False Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            If objDOperation Is Nothing Then objDataOperation.ReleaseTransaction(True)

            Return True
        Catch ex As Exception
            If objDOperation Is Nothing Then objDataOperation.ReleaseTransaction(False)
            Throw New Exception(ex.Message & "; Procedure Name: Delete; Module Name: " & mstrModuleName)
            Return False
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            If objDOperation Is Nothing Then objDataOperation = Nothing
        End Try
    End Function

    'Pinkal (27-Oct-2022) -- End

    ''' <summary>
    ''' Modify By: Pinkal
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>
    Public Function isUsed(ByVal intUnkid As Integer) As Boolean
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        objDataOperation = New clsDataOperation

        Try
            strQ = "<Query>"

            objDataOperation.AddParameter("@pendingloanaprovalunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intUnkid)

            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            Return dsList.Tables(0).Rows.Count > 0
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: isUsed; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
    End Function

    ''' <summary>
    ''' Modify By: Pinkal
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>
    Public Function isExist(ByVal strCode As String, ByVal strName As String, Optional ByVal intUnkid As Integer = -1) As Boolean
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        objDataOperation = New clsDataOperation

        Try
            strQ = "SELECT " & _
                      "  pendingloanaprovalunkid " & _
                      ", processpendingloanunkid " & _
                      ", employeeunkid " & _
                      ", mappingunkid " & _
                      ", roleunkid " & _
                      ", levelunkid " & _
                      ", priority " & _
                      ", approvaldate " & _
                      ", deductionperiodunkid " & _
                      ", loan_amount " & _
                      ", installmentamt " & _
                      ", noofinstallment " & _
                      ", countryunkid " & _
                      ", mapuserunkid " & _
                      ", isexceptional " & _
                      ", statusunkid " & _
                      ", visibleunkid " & _
                      ", remark " & _
                      ", userunkid " & _
                      ", isvoid " & _
                      ", voiddatetime " & _
                      ", voiduserunkid " & _
                      ", voidreason " & _
                      ", ISNULL(outst_princialamt,0.00) AS outst_princialamt " & _
                      ", ISNULL(outst_Interestamt,0.00) AS outst_Interestamt " & _
                      ", ISNULL(paidinstallments,0) AS paidinstallments " & _
                      ", ISNULL(take_homeamt,0.00) AS take_homeamt " & _
                      ", ISNULL(istopup,0) AS istopup " & _
                      ", ISNULL(first_tranche,0.00) AS first_tranche " & _
                      ", ISNULL(second_tranche,0.00) AS second_tranche " & _
                      ", ISNULL(third_tranche,0.00) AS third_tranche " & _
                      ", ISNULL(fourth_tranche,0.00) AS fourth_tranche " & _
                     "FROM lnroleloanapproval_process_tran " & _
                      " WHERE 1 = 1 "

            'Pinkal (14-Dec-2022) --    'NMB Loan Module Enhancement.[ ", ISNULL(first_tranche,0.00) AS first_tranche  ", ISNULL(second_tranche,0.00) AS second_tranche  ", ISNULL(third_tranche,0.00) AS third_tranche ", ISNULL(fourth_tranche,0.00) AS fourth_tranche " & _]

            'Pinkal (12-Oct-2022) -- NMB Loan Module Enhancement.[  ", ISNULL(outst_princialamt,0.00) AS outst_princialamt , ISNULL(outst_Interestamt,0.00) AS outst_Interestamt , ISNULL(paidinstallments,0) AS paidinstallments , ISNULL(take_homeamt,0.00) AS take_homeamt,ISNULL(istopup,0) AS istopup ]

            If intUnkid > 0 Then
                strQ &= " AND pendingloanaprovalunkid <> @pendingloanaprovalunkid"
            End If

            objDataOperation.AddParameter("@code", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, strCode)
            objDataOperation.AddParameter("@name", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, strName)
            objDataOperation.AddParameter("@pendingloanaprovalunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intUnkid)

            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            Return dsList.Tables(0).Rows.Count > 0
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: isExist; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
    End Function

    ''' <summary>
    ''' Modify By: Pinkal
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>
    Public Function InsertAuditTrailForRoleLoanApproval(ByVal objDataOperation As clsDataOperation, ByVal intAuditType As Integer) As Boolean
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        Try
            objDataOperation.ClearParameters()
            objDataOperation.AddParameter("@pendingloanaprovalunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintPendingloanaprovalunkid.ToString)
            objDataOperation.AddParameter("@processpendingloanunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintProcesspendingloanunkid.ToString)
            objDataOperation.AddParameter("@mappingunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintMappingunkid.ToString)
            objDataOperation.AddParameter("@roleunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintRoleunkid.ToString)
            objDataOperation.AddParameter("@levelunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintLevelunkid.ToString)
            objDataOperation.AddParameter("@priority", SqlDbType.Int, eZeeDataType.INT_SIZE, mintPriority.ToString)
            objDataOperation.AddParameter("@employeeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintEmployeeunkid.ToString)
            If mdtApprovaldate <> Nothing Then
                objDataOperation.AddParameter("@approvaldate", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, mdtApprovaldate.ToString)
            Else
                objDataOperation.AddParameter("@approvaldate", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, DBNull.Value)
            End If
            objDataOperation.AddParameter("@deductionperiodunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintDeductionperiodunkid.ToString)
            objDataOperation.AddParameter("@loan_amount", SqlDbType.Decimal, eZeeDataType.FLOAT_SIZE, mdecLoan_Amount.ToString)
            objDataOperation.AddParameter("@installmentamt", SqlDbType.Decimal, eZeeDataType.FLOAT_SIZE, mdecInstallmentamt.ToString)
            objDataOperation.AddParameter("@noofinstallment", SqlDbType.Int, eZeeDataType.INT_SIZE, mintNoofinstallment.ToString)
            objDataOperation.AddParameter("@countryunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintCountryunkid.ToString)
            objDataOperation.AddParameter("@mapuserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintMapUserunkid.ToString)
            objDataOperation.AddParameter("@isexceptional", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsExceptional.ToString)
            objDataOperation.AddParameter("@statusunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintStatusunkid.ToString)
            objDataOperation.AddParameter("@remark", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrRemark.ToString)
            objDataOperation.AddParameter("@visibleunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintVisibleunkid.ToString)
            objDataOperation.AddParameter("@audittype", SqlDbType.Int, eZeeDataType.INT_SIZE, intAuditType.ToString)
            objDataOperation.AddParameter("@audituserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintAuditUserId)
            objDataOperation.AddParameter("@ip", SqlDbType.NVarChar, eZeeDataType.IP_SIZE, mstrClientIP)
            objDataOperation.AddParameter("@hostname", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrHostName)
            objDataOperation.AddParameter("@form_name", SqlDbType.NVarChar, 500, mstrFormName)
            objDataOperation.AddParameter("@isweb", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsFromWeb)

            'Pinkal (12-Oct-2022) -- Start
            'NMB Loan Module Enhancement.
            objDataOperation.AddParameter("@outst_princialamt", SqlDbType.Decimal, eZeeDataType.DECIMAL_SIZE, mdecOutStanding_PrincipalAmt)
            objDataOperation.AddParameter("@outst_Interestamt", SqlDbType.Decimal, eZeeDataType.DECIMAL_SIZE, mdecOutStanding_PrincipalAmt)
            objDataOperation.AddParameter("@paidinstallments", SqlDbType.Int, eZeeDataType.INT_SIZE, mintPaidInstallments)
            objDataOperation.AddParameter("@take_homeamt", SqlDbType.Decimal, eZeeDataType.DECIMAL_SIZE, mdecTakeHomeAmount)
            objDataOperation.AddParameter("@istopup", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsTopup.ToString)
            'Pinkal (12-Oct-2022) -- End


            'Pinkal (10-Nov-2022) -- Start
            'NMB Loan Module Enhancement.
            If mdtLoanApproval_DailyReminder <> Nothing Then
                objDataOperation.AddParameter("@approval_dailyreminder", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, mdtLoanApproval_DailyReminder)
            Else
                objDataOperation.AddParameter("@approval_dailyreminder", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, DBNull.Value)
            End If

            If mdtEscalationReminder <> Nothing Then
                objDataOperation.AddParameter("@escalation_reminder", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, mdtEscalationReminder)
            Else
                objDataOperation.AddParameter("@escalation_reminder", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, DBNull.Value)
            End If

            'Pinkal (10-Nov-2022) -- End


            'Pinkal (14-Dec-2022) -- Start
            'NMB Loan Module Enhancement.
            objDataOperation.AddParameter("@first_tranche", SqlDbType.Decimal, eZeeDataType.DECIMAL_SIZE, mdecFirstTranche)
            objDataOperation.AddParameter("@second_tranche", SqlDbType.Decimal, eZeeDataType.DECIMAL_SIZE, mdecSecondTranche)
            objDataOperation.AddParameter("@third_tranche", SqlDbType.Decimal, eZeeDataType.DECIMAL_SIZE, mdecThirdTranche)
            objDataOperation.AddParameter("@fourth_tranche", SqlDbType.Decimal, eZeeDataType.DECIMAL_SIZE, mdecFourTranche)
            'Pinkal (14-Dec-2022) -- End

            'Pinkal (04-Aug-2023) -- Start
            '(A1X-1158) NMB - Allow to configure up to 5 disbursement tranches for Mortgage loans on the loan approval page.
            objDataOperation.AddParameter("@fifth_tranche", SqlDbType.Decimal, eZeeDataType.DECIMAL_SIZE, mdecFifthTranche)

            If mdtFirstTrancheDate <> Nothing Then
                objDataOperation.AddParameter("@first_tranchedate", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, mdtFirstTrancheDate)
            Else
                objDataOperation.AddParameter("@first_tranchedate", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, DBNull.Value)
            End If

            If mdtSecondTrancheDate <> Nothing Then
                objDataOperation.AddParameter("@second_tranchedate", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, mdtSecondTrancheDate)
            Else
                objDataOperation.AddParameter("@second_tranchedate", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, DBNull.Value)
            End If

            If mdtThirdTrancheDate <> Nothing Then
                objDataOperation.AddParameter("@third_tranchedate", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, mdtThirdTrancheDate)
            Else
                objDataOperation.AddParameter("@third_tranchedate", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, DBNull.Value)
            End If

            If mdtFourthTrancheDate <> Nothing Then
                objDataOperation.AddParameter("@fourth_tranchedate", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, mdtFourthTrancheDate)
            Else
                objDataOperation.AddParameter("@fourth_tranchedate", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, DBNull.Value)
            End If

            If mdtFifthTrancheDate <> Nothing Then
                objDataOperation.AddParameter("@fifth_tranchedate", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, mdtFifthTrancheDate)
            Else
                objDataOperation.AddParameter("@fifth_tranchedate", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, DBNull.Value)
            End If
            'Pinkal (04-Aug-2023) -- End


            strQ = "INSERT INTO atlnroleloanapproval_process_tran ( " & _
                      "  pendingloanaprovalunkid " & _
                      ", processpendingloanunkid " & _
                      ", employeeunkid " & _
                      ", mappingunkid " & _
                      ", roleunkid " & _
                      ", levelunkid " & _
                      ", priority " & _
                      ", approvaldate " & _
                      ", deductionperiodunkid " & _
                      ", loan_amount " & _
                      ", installmentamt " & _
                      ", noofinstallment " & _
                      ", countryunkid " & _
                      ", mapuserunkid " & _
                      ", isexceptional " & _
                      ", statusunkid " & _
                      ", visibleunkid " & _
                      ", remark " & _
                      ", audittype " & _
                      ", audituserunkid " & _
                      ", auditdatetime " & _
                      ", ip " & _
                      ", hostname" & _
                      ", form_name " & _
                      ", isweb " & _
                       ", outst_princialamt " & _
                      ", outst_Interestamt " & _
                      ", paidinstallments " & _
                      ", take_homeamt " & _
                      ", istopup " & _
                      ", approval_dailyreminder " & _
                      ", escalation_reminder " & _
                      ", first_tranche " & _
                      ", second_tranche " & _
                      ", third_tranche " & _
                      ", fourth_tranche " & _
                      ", fifth_tranche " & _
                      ", first_tranchedate " & _
                      ", second_tranchedate " & _
                      ", third_tranchedate " & _
                      ", fourth_tranchedate " & _
                      ", fifth_tranchedate " & _
                    ") VALUES (" & _
                       " @pendingloanaprovalunkid " & _
                      ", @processpendingloanunkid " & _
                      ", @employeeunkid " & _
                      ", @mappingunkid " & _
                      ", @roleunkid " & _
                      ", @levelunkid " & _
                      ", @priority " & _
                      ", @approvaldate " & _
                      ", @deductionperiodunkid " & _
                      ", @loan_amount " & _
                      ", @installmentamt " & _
                      ", @noofinstallment " & _
                      ", @countryunkid " & _
                      ", @mapuserunkid " & _
                      ", @isexceptional " & _
                      ", @statusunkid " & _
                      ", @visibleunkid " & _
                      ", @remark " & _
                      ", @audittype " & _
                      ", @audituserunkid " & _
                      ", GETDATE() " & _
                      ", @ip " & _
                      ", @hostname" & _
                      ", @form_name " & _
                      ", @isweb " & _
                      ", @outst_princialamt " & _
                      ", @outst_Interestamt " & _
                      ", @paidinstallments " & _
                      ", @take_homeamt " & _
                      ", @istopup " & _
                      ", @approval_dailyreminder " & _
                      ", @escalation_reminder " & _
                      ", @first_tranche " & _
                      ", @second_tranche " & _
                      ", @third_tranche " & _
                      ", @fourth_tranche " & _
                      ", @fifth_tranche " & _
                      ", @first_tranchedate " & _
                      ", @second_tranchedate " & _
                      ", @third_tranchedate " & _
                      ", @fourth_tranchedate " & _
                      ", @fifth_tranchedate " & _
                    "); SELECT @@identity"

            'Pinkal (04-Aug-2023) -- (A1X-1158) NMB - Allow to configure up to 5 disbursement tranches for Mortgage loans on the loan approval page.[ @fifth_tranche , @first_tranchedate , @second_tranchedate , @third_tranchedate , @fourth_tranchedate , @fifth_tranchedate ]

            'Pinkal (14-Dec-2022) -- NMB Loan Module Enhancement.[ ", @first_tranche , @second_tranche , @third_tranche , @fourth_tranche "]

            'Pinkal (10-Nov-2022) -- NMB Loan Module Enhancement.[approval_dailyreminder,escalation_reminder]

            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            Return True
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: InsertAuditTrailForRoleLoanApproval; Module Name: " & mstrModuleName)
            Return False
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
        End Try
    End Function

    ''' <summary>
    ''' Modify By: Pinkal
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>
    Public Function UpdateLoanApplicationStatus(ByVal xDatabaseName As String, _
                                                                    ByVal xUserUnkid As Integer, _
                                                                    ByVal xYearUnkid As Integer, _
                                                                    ByVal xCompanyUnkid As Integer, _
                                                                    ByVal xPeriodStart As DateTime, _
                                                                    ByVal xPeriodEnd As DateTime, _
                                                                    ByVal xUserModeSetting As String, _
                                                                    ByVal xOnlyApproved As Boolean, _
                                                                    ByVal xIncludeIn_ActiveEmployee As Boolean, _
                                                                    ByVal mintEmpNotificationTrancheDateBeforeDays As Integer, _
                                                                    ByVal objDoOperation As clsDataOperation) As Boolean

        'Pinkal (04-Aug-2023) -- (A1X-1158) NMB - Allow to configure up to 5 disbursement tranches for Mortgage loans on the loan approval page.[ByVal mintEmpNotificationTrancheDateBeforeDays As Integer]

        Try
            Dim exForce As Exception
            'Pinkal (27-Oct-2022) -- Start
            'NMB Loan Module Enhancement.
            'If mintStatusunkid <> enLoanApplicationStatus.REJECTED AndAlso mintStatusunkid <> enLoanApplicationStatus.CANCELLED  Then
            'Pinkal (23-Nov-2022) -- Start
            'NMB Loan Module Enhancement.
            'If mintStatusunkid <> enLoanApplicationStatus.REJECTED AndAlso mintStatusunkid <> enLoanApplicationStatus.CANCELLED AndAlso mintStatusunkid <> enLoanApplicationStatus.RETURNTOAPPLICANT Then
            If mintStatusunkid <> enLoanApplicationStatus.RETURNTOAPPLICANT Then
                'Pinkal (23-Nov-2022) -- End


                'Pinkal (27-Oct-2022) -- End

                Dim dsApprover As DataSet = GetRoleBasedApprovalList(xDatabaseName, xUserUnkid, xYearUnkid, xCompanyUnkid _
                                                                       , xPeriodStart, xPeriodEnd, xUserModeSetting, xOnlyApproved _
                                                                       , xIncludeIn_ActiveEmployee, "List", " lna.processpendingloanunkid = " & mintProcesspendingloanunkid & " AND lna.priority > " & mintPriority _
                                                                       , "", False, objDoOperation)

                If dsApprover IsNot Nothing AndAlso dsApprover.Tables(0).Rows.Count <= 0 Then
                    Dim objLoanApplication As New clsProcess_pending_loan
                    objLoanApplication._xDataOp = objDoOperation
                    objLoanApplication._Processpendingloanunkid = mintProcesspendingloanunkid
                    objLoanApplication._Approverunkid = mintUserunkid
                    objLoanApplication._Approved_Amount = mdecLoan_Amount
                    objLoanApplication._Loan_Statusunkid = enLoanApplicationStatus.APPROVED
                    objLoanApplication._ClientIP = mstrClientIP
                    objLoanApplication._FormName = mstrFormName
                    objLoanApplication._HostName = mstrHostName

                    'Pinkal (12-Oct-2022) -- Start
                    'NMB Loan Module Enhancement.
                    objLoanApplication._ApprovedOutstandingPrincipalAmount = mdecOutStanding_PrincipalAmt
                    objLoanApplication._ApprovedOutstandingInterestAmount = mdecOutStanding_InterestAmt
                    objLoanApplication._ApprovedPaidInstallment = mintPaidInstallments
                    objLoanApplication._ApprovedTakeHomeAmount = mdecTakeHomeAmount
                    'Pinkal (12-Oct-2022) -- End

                    'Pinkal (02-Jun-2023) -- Start
                    'RESOLVED BUG FOR NMB WHEN FINAL APPROVER CHANGED DEDCUTION PERIOD AT THAT TIME IT SHOULD BE UPDATE ON LOAN APPLICATION ALSO .
                    objLoanApplication._DeductionPeriodunkid = mintDeductionperiodunkid
                    'Pinkal (02-Jun-2023) -- End

                    'Pinkal (04-Aug-2023) -- Start
                    '(A1X-1158) NMB - Allow to configure up to 5 disbursement tranches for Mortgage loans on the loan approval page.
                    Dim objLoanScheme As New clsLoan_Scheme
                    objLoanScheme._Loanschemeunkid = objLoanApplication._Loanschemeunkid
                    If objLoanScheme._LoanSchemeCategoryId = enLoanSchemeCategories.MORTGAGE Then
                        If IsDBNull(mdtSecondTrancheDate) = False AndAlso mdtSecondTrancheDate <> Nothing Then
                            objLoanApplication._SendEmpReminderForNextTrance = DateAdd(DateInterval.Day, mintEmpNotificationTrancheDateBeforeDays * -1, mdtSecondTrancheDate)
                        End If
                    Else
                        objLoanApplication._SendEmpReminderForNextTrance = Nothing
                        objLoanApplication._NtfSendEmpForNextTranche = True
                    End If
                    objLoanScheme = Nothing
                    'Pinkal (04-Aug-2023) -- End

                    If objLoanApplication.Update(xDatabaseName, xUserUnkid, xYearUnkid, xCompanyUnkid, xPeriodStart, xPeriodEnd, xUserModeSetting, xOnlyApproved, xIncludeIn_ActiveEmployee, False, objDoOperation, Nothing) = False Then
                        exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                        Throw exForce
                    End If

                    dsApprover.Tables(0).Rows.Clear()
                    dsApprover = Nothing

                ElseIf mintStatusunkid = enLoanApplicationStatus.REJECTED Then
                    Dim objLoanApplication As New clsProcess_pending_loan
                    objLoanApplication._xDataOp = objDoOperation
                    objLoanApplication._Processpendingloanunkid = mintProcesspendingloanunkid
                    objLoanApplication._Approverunkid = mintUserunkid
                    objLoanApplication._Loan_Statusunkid = enLoanApplicationStatus.REJECTED
                    objLoanApplication._ClientIP = mstrClientIP
                    objLoanApplication._FormName = mstrFormName
                    objLoanApplication._HostName = mstrHostName
                    If objLoanApplication.Update(xDatabaseName, xUserUnkid, xYearUnkid, xCompanyUnkid, xPeriodStart, xPeriodEnd, xUserModeSetting, xOnlyApproved, xIncludeIn_ActiveEmployee, False, objDoOperation, Nothing) = False Then
                        exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                        Throw exForce
                    End If
                    objLoanApplication = Nothing

                    'Pinkal (10-Nov-2022) -- Start
                    'NMB Loan Module Enhancement.
                ElseIf mintStatusunkid = enLoanApplicationStatus.RETURNTOAPPLICANT Then
                    Dim objLoanApplication As New clsProcess_pending_loan
                    objLoanApplication._xDataOp = objDoOperation
                    objLoanApplication._Processpendingloanunkid = mintProcesspendingloanunkid
                    objLoanApplication._IssubmitApproval = False
                    objLoanApplication._ClientIP = mstrClientIP
                    objLoanApplication._FormName = mstrFormName
                    objLoanApplication._HostName = mstrHostName
                    If objLoanApplication.Update(xDatabaseName, xUserUnkid, xYearUnkid, xCompanyUnkid, xPeriodStart, xPeriodEnd, xUserModeSetting, xOnlyApproved, xIncludeIn_ActiveEmployee, False, objDoOperation, Nothing) = False Then
                        exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                        Throw exForce
                    End If
                    objLoanApplication = Nothing

                End If '  If dsApprover IsNot Nothing AndAlso dsApprover.Tables(0).Rows.Count <= 0 Then

            End If  '  If mintStatusunkid <> enLoanApplicationStatus.REJECTED AndAlso mintStatusunkid <> enLoanApplicationStatus.CANCELLED AndAlso mintStatusunkid <> enLoanApplicationStatus.RETURNTOAPPLICANT Then
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: UpdateLoanApplicationStatus; Module Name: " & mstrModuleName)
            Return False
        End Try
        Return True
    End Function

    ''' <summary>
    ''' Modify By: Pinkal
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>
    Public Function UpdateLoanApplication(Optional ByVal objDOperation As clsDataOperation = Nothing) As Boolean

        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        If objDOperation Is Nothing Then
            objDataOperation = New clsDataOperation
            objDataOperation.BindTransaction()
        Else
            objDataOperation = objDOperation
        End If

        Try
            strQ = " UPDATE lnroleloanapproval_process_tran SET " & _
                      "     employeeunkid = @employeeunkid" & _
                      "    ,deductionperiodunkid = @deductionperiodunkid" & _
                      "    ,loan_amount = @loan_amount" & _
                      "    ,installmentamt = @installmentamt" & _
                      "    ,noofinstallment = @noofinstallment" & _
                      "    ,remark = @remark" & _
                      "    WHERE processpendingloanunkid = @processpendingloanunkid " & _
                      "    AND isvoid = 0 "

            objDataOperation.ClearParameters()
            objDataOperation.AddParameter("@processpendingloanunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintProcesspendingloanunkid.ToString)
            objDataOperation.AddParameter("@employeeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintEmployeeunkid.ToString)
            objDataOperation.AddParameter("@deductionperiodunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintDeductionperiodunkid.ToString)
            objDataOperation.AddParameter("@loan_amount", SqlDbType.Decimal, eZeeDataType.FLOAT_SIZE, mdecLoan_Amount.ToString)
            objDataOperation.AddParameter("@installmentamt", SqlDbType.Decimal, eZeeDataType.FLOAT_SIZE, mdecInstallmentamt.ToString)
            objDataOperation.AddParameter("@noofinstallment", SqlDbType.Int, eZeeDataType.INT_SIZE, mintNoofinstallment.ToString)
            objDataOperation.AddParameter("@remark", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrRemark.ToString)
            Call objDataOperation.ExecNonQuery(strQ)

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            strQ = " SELECT " & _
                      "  lna.pendingloanaprovalunkid " & _
                      ", lna.processpendingloanunkid " & _
                      ", lna.statusunkid " & _
                      ", lna.visibleunkid " & _
                      ", lna.priority " & _
                      ", ln.application_date " & _
                      " FROM lnroleloanapproval_process_tran lna " & _
                      " JOIN lnloan_process_pending_loan ln on ln.processpendingloanunkid = lna.processpendingloanunkid AND ln.isvoid = 0 " & _
                      " WHERE lna.isvoid=0 AND lna.processpendingloanunkid = @processpendingloanunkid "

            'Pinkal (10-Nov-2022) -- NMB Loan Module Enhancement.[", statusunkid , visibleunkid , priority " & _]

            objDataOperation.ClearParameters()
            objDataOperation.AddParameter("@processpendingloanunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintProcesspendingloanunkid.ToString)

            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If


            If dsList IsNot Nothing AndAlso dsList.Tables("List").Rows.Count > 0 Then

                'Pinkal (10-Nov-2022) -- Start
                'NMB Loan Module Enhancement.

                'Pinkal (23-Nov-2022) -- Start
                'NMB Loan Module Enhancement.
                'Dim xCount As Integer = dsList.Tables(0).AsEnumerable().Where(Function(x) x.Field(Of Integer)("visibleunkid") = -1).Count
                'Dim xPriority As Integer = dsList.Tables(0).AsEnumerable().Where(Function(x) x.Field(Of Integer)("statusunkid") = enLoanApplicationStatus.PENDING AndAlso x.Field(Of Integer)("visibleunkid") = -1).Select(Function(x) x.Field(Of Integer)("priority")).Min
                Dim xCount As Integer = dsList.Tables(0).AsEnumerable().Where(Function(x) x.Field(Of Integer)("visibleunkid") = -1).DefaultIfEmpty.Count
                Dim xPriority As Integer = dsList.Tables(0).AsEnumerable().Where(Function(x) x.Field(Of Integer)("statusunkid") = enLoanApplicationStatus.PENDING AndAlso x.Field(Of Integer)("visibleunkid") = -1).Select(Function(x) x.Field(Of Integer)("priority")).DefaultIfEmpty.Min
                'Pinkal (23-Nov-2022) -- End

                For Each dRow As DataRow In dsList.Tables("List").Rows

                    mdtLoanApproval_DailyReminder = Nothing

                    _Pendingloanaprovalunkid(objDataOperation) = dRow.Item("pendingloanaprovalunkid")

                    If dsList.Tables(0).Rows.Count = xCount AndAlso xPriority = mintPriority AndAlso mintStatusunkid = enLoanApplicationStatus.PENDING AndAlso mintVisibleunkid = -1 Then

                        mintVisibleunkid = enLoanApplicationStatus.PENDING

                        If mblnApproval_DailyReminder Then
                            mdtLoanApproval_DailyReminder = Now.AddDays(1)
                        Else
                            mdtLoanApproval_DailyReminder = Nothing
                        End If

                        If mintEscalationDays > 0 Then
                            mdtEscalationReminder = CDate(dRow("application_date")).AddDays(mintEscalationDays)
                        Else
                            mdtEscalationReminder = Nothing
                        End If

                        strQ = " UPDATE lnroleloanapproval_process_tran SET " & _
                                  "  visibleunkid = @visibleunkid " & _
                                  ", approval_dailyreminder = @approval_dailyreminder " & _
                                  ", escalation_reminder = @escalation_reminder " & _
                                  "  WHERE processpendingloanunkid = @processpendingloanunkid " & _
                                  "  AND pendingloanaprovalunkid = @pendingloanaprovalunkid " & _
                                  "  AND isvoid = 0 "

                        objDataOperation.ClearParameters()
                        objDataOperation.AddParameter("@pendingloanaprovalunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintPendingloanaprovalunkid.ToString)
                        objDataOperation.AddParameter("@processpendingloanunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintProcesspendingloanunkid.ToString)
                        objDataOperation.AddParameter("@visibleunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintVisibleunkid)

                        If mdtLoanApproval_DailyReminder <> Nothing Then
                            objDataOperation.AddParameter("@approval_dailyreminder", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, mdtLoanApproval_DailyReminder)
                        Else
                            objDataOperation.AddParameter("@approval_dailyreminder", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, DBNull.Value)
                        End If

                        If mdtEscalationReminder <> Nothing Then
                            objDataOperation.AddParameter("@escalation_reminder", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, mdtEscalationReminder)
                        Else
                            objDataOperation.AddParameter("@escalation_reminder", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, DBNull.Value)
                        End If

                        objDataOperation.ExecNonQuery(strQ)

                        If objDataOperation.ErrorMessage <> "" Then
                            exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                            Throw exForce
                        End If

                    End If
                    'Pinkal (10-Nov-2022) -- End

                    If InsertAuditTrailForRoleLoanApproval(objDataOperation, enAuditType.EDIT) = False Then
                        exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                        Throw exForce
                    End If
                Next
            End If

            If objDOperation Is Nothing Then
                objDataOperation.ReleaseTransaction(True)
            End If

            Return True
        Catch ex As Exception
            If objDOperation Is Nothing Then objDataOperation.ReleaseTransaction(False)
            Throw New Exception(ex.Message & "; Procedure Name: UpdateLoanApplication; Module Name: " & mstrModuleName)
            Return False
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            If objDOperation Is Nothing Then objDataOperation = Nothing
        End Try
    End Function

    'Hemant (12 Oct 2022) -- Start
    'ISSUE/ENHANCEMENT(NMB) :  UAT Change - On loan application list screen, provide edit/delete buttons if loan application is not approved/rejected
    Public Function IsPendingLoanApplication(ByVal intProcessPendingLoanunkid As Integer, Optional ByVal blnGetApplicationStatus As Boolean = False) As Boolean
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception
        Try
            objDataOperation = New clsDataOperation

            strQ = " SELECT " & _
                   "     processpendingloanunkid " & _
                   "    ,statusunkid " & _
                   " FROM lnroleloanapproval_process_tran " & _
                   " WHERE isvoid = 0 " & _
                   "    AND processpendingloanunkid = @processpendingloanunkid "

            If blnGetApplicationStatus = True Then
                strQ &= "    AND statusunkid = 1 AND visibleid < 2 "
            Else
                strQ &= "    AND statusunkid <> 1 "
            End If


            objDataOperation.ClearParameters()
            objDataOperation.AddParameter("@processpendingloanunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intProcessPendingLoanunkid)

            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            If dsList.Tables("List").Rows.Count > 0 Then
                Return False
            Else
                Return True
            End If

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: IsPendingLoanApplication; Module Name: " & mstrModuleName)
        End Try
    End Function
    'Hemant (12 Oct 2022) -- End

    'Pinkal (23-Nov-2022) -- Start
    'NMB Loan Module Enhancement.
    Public Function GetFlexCubeEmpCustomerNo(ByVal strOracleHostName As String, _
                                                                      ByVal strOraclePortNo As String, _
                                                                      ByVal strOracleServiceName As String, _
                                                                      ByVal strOracleUserName As String, _
                                                                      ByVal strOracleUserPassword As String, _
                                                                      ByVal mstrEmpBankAcNo As String) As String
        Dim mstrCustomerNo As String = ""
        Dim dsList As New DataSet
        Try
            Dim strConn As String = "Data Source=(DESCRIPTION=(ADDRESS_LIST=(ADDRESS=(PROTOCOL=TCP)(HOST=" & strOracleHostName & ")(PORT=" & strOraclePortNo & ")))(CONNECT_DATA=(SERVER=DEDICATED)(SERVICE_NAME=" & strOracleServiceName & "))); User Id=" & strOracleUserName & ";Password=" & strOracleUserPassword & "; "
            Using cnnOracle As OracleConnection = New OracleConnection
                cnnOracle.ConnectionString = strConn

                Try
                    cnnOracle.Open()
                Catch ex As Exception
                    Throw ex
                End Try

                Dim StrQ As String = " SELECT " & _
                                                "     CUST_AC_NO " & _
                                                "    ,CUST_NO  " & _
                                                " FROM fcubs.nmb_staff_cusac " & _
                                                " WHERE CUST_AC_NO  IN ('" & mstrEmpBankAcNo & "')" 'ALWAYS TAKE FIRST ROW

                Using cmdOracle As New OracleCommand()

                    If cnnOracle.State = ConnectionState.Closed Or cnnOracle.State = ConnectionState.Broken Then
                        cnnOracle.Open()
                    End If
                    cmdOracle.Connection = cnnOracle
                    cmdOracle.CommandType = CommandType.Text
                    cmdOracle.CommandText = StrQ
                    cmdOracle.Parameters.Clear()
                    Dim oda As New OracleDataAdapter(cmdOracle)
                    oda.Fill(dsList)
                End Using
            End Using

            If dsList IsNot Nothing AndAlso dsList.Tables(0).Rows.Count > 0 Then
                mstrCustomerNo = dsList.Tables(0).Rows(0)("CUST_NO").ToString()
            End If
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: GetFlexCubeEmpCustomerNo; Module Name: " & mstrModuleName)
        End Try
        Return mstrCustomerNo
    End Function
    'Pinkal (23-Nov-2022) -- End

    'Pinkal (23-Nov-2022) -- Start
    'NMB Loan Module Enhancement.
    Public Function SendFlexcubeRequest(ByVal mstrOracleHostName As String, ByVal mstrOraclePortNo As String, ByVal mstrOracleServiceName As String, ByVal mstrOracleUserName As String, ByVal mstrOracleUserPassword As String _
                                                   , ByVal mstrNewLoanRequestFlexcubeURL As String, ByVal mstrTopupRequestFlexcubeURL As String, ByVal xDatabaseName As String, ByVal xUserUnkid As Integer, ByVal xYearUnkid As Integer _
                                                   , ByVal xCompanyUnkid As Integer, ByVal xPeriodStart As Date, ByVal xPeriodEnd As Date, ByVal xUserModeSetting As String, ByVal xOnlyApproved As Boolean, ByVal xIncludeIn_ActiveEmployee As Boolean _
                                                   , ByVal mdecOutStanding_PrincipalAmt As Decimal, ByVal mdecOutStanding_InterestAmt As Decimal, ByVal mstrEmpBankAcNo As String, ByVal mstrLoanSchemeCode As String, ByVal xProcesspendingloanunkid As Integer) As Boolean

        Dim objLoanFlexCube As New clsLoanFlexCubeIntegration
        Dim objLoanFlexCube_RollOver As New clsLoanFlexCubeIntegration_RollOver
        Dim objLoanApplication As New clsProcess_pending_loan
        Dim objMasterData As New clsMasterData
        Dim mblnFlag As Boolean = True

        'Pinkal (14-Dec-2022) -- Start
        'NMB Loan Module Enhancement.
        Dim mstrLoanFlexcubeFailureNotificationUserIds As String = ""
        Dim dtTable As DataTable = Nothing
        'Pinkal (14-Dec-2022) -- End

        'Pinkal (28-Apr-2023) -- Start
        '(A1X-860,A1X-861,A1X-862) NMB - Testing loan disbursement API - car loan,pre-paid   loan,mortgage loan    
        Dim objCarPrepaidAnnualLoanFlexCube As New clsCarPrepaidAnnualLoanFlexCubeIntegration
        'Pinkal (28-Apr-2023) -- End


        Try
            Dim exForce As Exception = Nothing
            Dim mstrError As String = ""
            Dim mstrPostedData As String = ""
            Dim mstrResponseData As String = ""

            'Pinkal (02-Jun-2023) -- Start
            'Checking Loan Application Exist in CBS for NMB.
            Dim mdtServerDate As DateTime = Nothing
            Dim objConfig As New clsConfigOptions
            mstrLoanFlexcubeFailureNotificationUserIds = objConfig.GetKeyValue(xCompanyUnkid, "LoanFlexcubeFailureNotificationUserIds", Nothing)

            'Pinkal (16-Nov-2023) -- Start
            '(A1X-1489) NMB - Mortgage tranche disbursement API.
            Dim mstrUserAccessModeSettings As String = objConfig.GetKeyValue(xCompanyUnkid, "UserAccessModeSetting", Nothing)
            Dim mstrArutiSelfServiceURL As String = objConfig.GetKeyValue(xCompanyUnkid, "ArutiSelfServiceURL", Nothing)
            'Pinkal (16-Nov-2023) -- End

            mdtServerDate = objConfig._CurrentDateAndTime
            objConfig = Nothing
            'Pinkal (02-Jun-2023) -- End

            objLoanApplication._Processpendingloanunkid = xProcesspendingloanunkid

            'Pinkal (02-Jun-2023) -- Start
            'Checking Loan Application Exist in CBS for NMB.
            Dim mblnLoanApplicationExistInCBS As Boolean = False
            If objLoanApplication._IsPostedError Then
                mblnLoanApplicationExistInCBS = LoanApplicantExistInCBS(mstrOracleHostName, mstrOraclePortNo, mstrOracleServiceName, mstrOracleUserName, mstrOracleUserPassword, objLoanApplication._Application_No, mstrEmpBankAcNo)
            End If
            'Pinkal (02-Jun-2023) -- End

            Dim byt As Byte() = System.Text.Encoding.UTF8.GetBytes(mstrOracleUserName.Trim & ":" & mstrOracleUserPassword)
            Dim mstrBase64Credential As String = Convert.ToBase64String(byt)

            'Pinkal (28-Apr-2023) -- Start
            '(A1X-860,A1X-861,A1X-862) NMB - Testing loan disbursement API - car loan,pre-paid loan,mortgage loan    

            Dim mstrCustomerNo As String = ""

            If mstrEmpBankAcNo.Trim.Length > 0 Then
                mstrCustomerNo = GetFlexCubeEmpCustomerNo(mstrOracleHostName, mstrOraclePortNo, mstrOracleServiceName, mstrOracleUserName, mstrOracleUserPassword, mstrEmpBankAcNo)
            End If

            Select Case mstrLoanSchemeCode.Trim().ToUpper()

                Case "CL17", "CL25", "CL27", "CL28"   'Car Loan, 'Annual Loan,'Prepaid Loan, 'Mortgage Loan

                    If mstrNewLoanRequestFlexcubeURL.Trim.Length > 0 AndAlso mstrBase64Credential.Trim.Length > 0 AndAlso objLoanApplication._IsTopUp = False Then   'CAR,ANNUAL,PREPAID,MORTGAGE LOAN REQUEST

                        objCarPrepaidAnnualLoanFlexCube.service = "aruti-loan-automation"

                        Select Case mstrLoanSchemeCode.Trim().ToUpper()
                            Case "CL17"    'Car Loan
                                objCarPrepaidAnnualLoanFlexCube.type = "car-loan"

                                'Case "CL25"
                                '    objCarPrepaidAnnualLoanFlexCube.type = "annual-loan"

                            Case "CL25", "CL27"   'Annual Loan  'Prepaid Loan
                                objCarPrepaidAnnualLoanFlexCube.type = "prepaid-loan"

                            Case "CL28"   'Mortgage Loan
                                objCarPrepaidAnnualLoanFlexCube.type = "mortgage-loan"
                        End Select

                        'Pinkal (02-Jun-2023) -- Start
                        'Checking Loan Application Exist in CBS for NMB.
                        If objLoanApplication._IsPostedError Then
                            If mblnLoanApplicationExistInCBS Then
                                objLoanApplication._ReponseData = "{""statusCode"": 600,""message"": ""Success"",""body"": {""reason"": ""ST-SAVE-052:Successfully Saved and Authorized""}}"
                                objLoanApplication._IsPostedError = False
                                objLoanApplication._Userunkid = xUserUnkid
                                objLoanApplication._WebClientIP = mstrClientIP
                                objLoanApplication._WebFormName = mstrFormName
                                objLoanApplication._WebHostName = mstrHostName

                                If objLoanApplication.Update(xDatabaseName, xUserUnkid, xYearUnkid, xCompanyUnkid, xPeriodStart, xPeriodEnd, xUserModeSetting, xOnlyApproved, xIncludeIn_ActiveEmployee, False, Nothing, Nothing) = False Then
                                    mblnFlag = False
                                    exForce = New Exception(objLoanApplication._Message)
                                    Throw exForce
                                End If
                                Return mblnFlag
                            Else
                                objCarPrepaidAnnualLoanFlexCube.payload.transactionReference = objLoanApplication._Application_No + mdtServerDate.ToString("yyyyMMddhhmmssff")
                            End If  ' If mblnLoanApplicationExistInCBS Then
                        Else
                        objCarPrepaidAnnualLoanFlexCube.payload.transactionReference = objLoanApplication._Application_No
                        End If '    If objLoanApplication._IsPostedError Then
                        'Pinkal (02-Jun-2023) -- End

                        objCarPrepaidAnnualLoanFlexCube.payload.applicationNumber = objLoanApplication._Application_No
                        objCarPrepaidAnnualLoanFlexCube.payload.accountNumber = mstrEmpBankAcNo
                        objCarPrepaidAnnualLoanFlexCube.payload.productCode = mstrLoanSchemeCode
                        objCarPrepaidAnnualLoanFlexCube.payload.productCategory = "STAFF"
                        objCarPrepaidAnnualLoanFlexCube.payload.customerNumber = mstrCustomerNo

                        'Pinkal (15-Sep-2023) -- Start
                        '(A1X-1166) NMB - Disburse approved tranche amount on Flexcube via API.
                        If mstrLoanSchemeCode.Trim().ToUpper() = "CL17" Then  'CAR LOAN
                            objCarPrepaidAnnualLoanFlexCube.payload.maturityDate = Nothing
                            objCarPrepaidAnnualLoanFlexCube.payload.installmentStartDate = New Date(xPeriodStart.Year, xPeriodStart.Month, 25).ToString("yyyy-MM-dd")
                        Else
                        'Pinkal (15-May-2023) -- Start
                        'As per NMB and Matthew's comment Changing maturity Date of 1st Date to 25th.
                        'objCarPrepaidAnnualLoanFlexCube.payload.maturityDate = New Date(xPeriodStart.AddMonths(objLoanApplication._NoOfInstallment - 1).Year, xPeriodStart.AddMonths(objLoanApplication._NoOfInstallment - 1).Month, 1).ToString("yyyy-MM-dd")
                        objCarPrepaidAnnualLoanFlexCube.payload.maturityDate = New Date(xPeriodStart.AddMonths(objLoanApplication._NoOfInstallment - 1).Year, xPeriodStart.AddMonths(objLoanApplication._NoOfInstallment - 1).Month, 25).ToString("yyyy-MM-dd")
                        'Pinkal (15-May-2023) -- End

                            objCarPrepaidAnnualLoanFlexCube.payload.installmentStartDate = Nothing
                        End If
                        'Pinkal (15-Sep-2023) -- End


                        objCarPrepaidAnnualLoanFlexCube.payload.loanAmount = objLoanApplication._Loan_Amount
                        objCarPrepaidAnnualLoanFlexCube.payload.currency = "TZS"

                        Select Case mstrLoanSchemeCode.Trim().ToUpper()
                            Case "CL17"    'Car Loan

                                'Pinkal (15-Sep-2023) -- Start
                                '(A1X-1166) NMB - Disburse approved tranche amount on Flexcube via API.
                                'objCarPrepaidAnnualLoanFlexCube.payload.remark = "Car Loan request"
                                objCarPrepaidAnnualLoanFlexCube.payload.numberOfInstallments = objLoanApplication._NoOfInstallment.ToString()
                                objCarPrepaidAnnualLoanFlexCube.payload.bookDate = Now.Date.ToString("yyyy-MM-dd") 'mdtApprovaldate.ToString("yyyy-MM-dd")
                                objCarPrepaidAnnualLoanFlexCube.payload.postingDate = Now.Date.ToString("yyyy-MM-dd")  'mdtApprovaldate.ToString("yyyy-MM-dd")
                                'objCarPrepaidAnnualLoanFlexCube.payload.bookDate = "2023-10-13"
                                'objCarPrepaidAnnualLoanFlexCube.payload.postingDate = "2023-10-13"
                                objCarPrepaidAnnualLoanFlexCube.payload.relationOfficerId = "JM02121"
                                objCarPrepaidAnnualLoanFlexCube.payload.sector = "SC999"
                                objCarPrepaidAnnualLoanFlexCube.payload.remark = "OTHR"
                                'Pinkal (15-Sep-2023) -- End

                            Case "CL25"    'Annual Loan
                                objCarPrepaidAnnualLoanFlexCube.payload.remark = "Annual Loan request"

                                'Pinkal (15-Sep-2023) -- Start
                                '(A1X-1166) NMB - Disburse approved tranche amount on Flexcube via API.
                                objCarPrepaidAnnualLoanFlexCube.payload.numberOfInstallments = Nothing
                                objCarPrepaidAnnualLoanFlexCube.payload.bookDate = Nothing
                                objCarPrepaidAnnualLoanFlexCube.payload.postingDate = Nothing
                                objCarPrepaidAnnualLoanFlexCube.payload.relationOfficerId = Nothing
                                objCarPrepaidAnnualLoanFlexCube.payload.sector = Nothing
                                'Pinkal (15-Sep-2023) -- End

                            Case "CL27"    'Prepaid Loan
                                objCarPrepaidAnnualLoanFlexCube.payload.remark = "Prepaid Loan request"

                                'Pinkal (15-Sep-2023) -- Start
                                '(A1X-1166) NMB - Disburse approved tranche amount on Flexcube via API.
                                objCarPrepaidAnnualLoanFlexCube.payload.numberOfInstallments = Nothing
                                objCarPrepaidAnnualLoanFlexCube.payload.bookDate = Nothing
                                objCarPrepaidAnnualLoanFlexCube.payload.postingDate = Nothing
                                objCarPrepaidAnnualLoanFlexCube.payload.relationOfficerId = Nothing
                                objCarPrepaidAnnualLoanFlexCube.payload.sector = Nothing
                                'Pinkal (15-Sep-2023) -- End

                            Case "CL28"   'Mortgage Loan
                                objCarPrepaidAnnualLoanFlexCube.payload.remark = "Mortgage Loan request"

                                'Pinkal (15-Sep-2023) -- Start
                                '(A1X-1166) NMB - Disburse approved tranche amount on Flexcube via API.
                                objCarPrepaidAnnualLoanFlexCube.payload.numberOfInstallments = Nothing
                                objCarPrepaidAnnualLoanFlexCube.payload.bookDate = Nothing
                                objCarPrepaidAnnualLoanFlexCube.payload.postingDate = Nothing
                                objCarPrepaidAnnualLoanFlexCube.payload.relationOfficerId = Nothing
                                objCarPrepaidAnnualLoanFlexCube.payload.sector = Nothing
                                'Pinkal (15-Sep-2023) -- End

                        End Select

                        'Pinkal (15-Sep-2023) -- Start
                        '(A1X-1166) NMB - Disburse approved tranche amount on Flexcube via API.
                        If mstrLoanSchemeCode.Trim().ToUpper() = "CL17" Then   'CAR LOAN
                            objCarPrepaidAnnualLoanFlexCube.payload.valueDate = Nothing
                        Else
                        'TEMPARORY SET DATE 05-SEP-2022 FOR UAT REMOVE IT AS SOON AS POSSIBLE
                            'Pinkal (23-Dec-2023) -- Start
                            '(A1X-1555)  NMB - P2P Changes for Claim Request and Company Asset.
                            'objCarPrepaidAnnualLoanFlexCube.payload.valueDate = mdtApprovaldate.Date.ToString("yyyy-MM-dd")  'Now.Date.ToString("yyyy-MM-dd")
                            'objCarPrepaidAnnualLoanFlexCube.payload.valueDate = "2023-10-13"
                            objCarPrepaidAnnualLoanFlexCube.payload.valueDate = Now.Date.ToString("yyyy-MM-dd")  'Now.Date.ToString("yyyy-MM-dd")
                            'Pinkal (23-Dec-2023) -- End
                        End If
                        'Pinkal (15-Sep-2023) -- End


                        If objMasterData.GetSetFlexCubeLoanRequest(mstrNewLoanRequestFlexcubeURL, mstrBase64Credential, objCarPrepaidAnnualLoanFlexCube, mstrError, mstrPostedData, mstrResponseData) = False Then
                            mblnFlag = False
                            exForce = New Exception(mstrLoanSchemeCode.Trim().ToUpper() & " " & "Loan Request Flexcube Error : " & mstrError)
                            Throw exForce
                        End If

                    End If  ' If mstrNewLoanRequestFlexcubeURL.Trim.Length > 0 AndAlso mstrBase64Credential.Trim.Length > 0 AndAlso objLoanApplication._IsTopUp = False AndAlso mstrEmpBankAcNo.Trim.Length > 0 Then   'CAR,ANNUAL,PREPAID,MORTGAGE LOAN REQUEST

                Case Else   'OTHER LOAN

                    If mstrNewLoanRequestFlexcubeURL.Trim.Length > 0 AndAlso mstrBase64Credential.Trim.Length > 0 AndAlso objLoanApplication._IsTopUp = False Then   'NEW LOAN REQUEST  'AndAlso mstrEmpBankAcNo.Trim.Length > 0

                        'Pinkal (28-Apr-2023) -- Start
                        '(A1X-860,A1X-861,A1X-862) NMB - Testing loan disbursement API - car loan,pre-paid loan,mortgage loan    
                        'Dim mstrCustomerNo As String = GetFlexCubeEmpCustomerNo(mstrOracleHostName, mstrOraclePortNo, mstrOracleServiceName, mstrOracleUserName, mstrOracleUserPassword, mstrEmpBankAcNo)
                        'Pinkal (28-Apr-2023) -- End

                objLoanFlexCube.service = "aruti-loan-automation"
                objLoanFlexCube.type = "loan-request"


                        'Pinkal (02-Jun-2023) -- Start
                        'Checking Loan Application Exist in CBS for NMB.
                        If objLoanApplication._IsPostedError Then
                            If mblnLoanApplicationExistInCBS Then
                                objLoanApplication._ReponseData = "{""statusCode"": 600,""message"": ""Success"",""body"": {""reason"": ""ST-SAVE-052:Successfully Saved and Authorized""}}"
                                objLoanApplication._IsPostedError = False
                                objLoanApplication._Userunkid = xUserUnkid
                                objLoanApplication._WebClientIP = mstrClientIP
                                objLoanApplication._WebFormName = mstrFormName
                                objLoanApplication._WebHostName = mstrHostName

                                If objLoanApplication.Update(xDatabaseName, xUserUnkid, xYearUnkid, xCompanyUnkid, xPeriodStart, xPeriodEnd, xUserModeSetting, xOnlyApproved, xIncludeIn_ActiveEmployee, False, Nothing, Nothing) = False Then
                                    mblnFlag = False
                                    exForce = New Exception(objLoanApplication._Message)
                                    Throw exForce
                                End If
                                Return mblnFlag
                            Else
                                objLoanFlexCube.payload.transactionReference = objLoanApplication._Application_No + mdtServerDate.ToString("yyyyMMddhhmmssff")
                            End If  ' If mblnLoanApplicationExistInCBS Then
                        Else
                objLoanFlexCube.payload.transactionReference = objLoanApplication._Application_No
                        End If 'If objLoanApplication._IsPostedError Then
                        'Pinkal (02-Jun-2023) -- End

                objLoanFlexCube.payload.applicationNumber = objLoanApplication._Application_No
                objLoanFlexCube.payload.accountNumber = mstrEmpBankAcNo
                objLoanFlexCube.payload.productCode = mstrLoanSchemeCode
                objLoanFlexCube.payload.productCategory = "STAFF"
                objLoanFlexCube.payload.customerNumber = mstrCustomerNo
                'Pinkal (14-Dec-2022) -- Start
                'NMB Loan Module Enhancement. [AS PER UAT DOCUMENT]
                'objLoanFlexCube.payload.maturityDate = New Date(xPeriodStart.AddMonths(objLoanApplication._NoOfInstallment).Year, xPeriodStart.AddMonths(objLoanApplication._NoOfInstallment).Month, 1).ToString("yyyy-MM-dd")
                        'Pinkal (15-May-2023) -- Start
                        'As per NMB and Matthew's comment Changing maturity Date of 1st Date to 25th.
                        'objLoanFlexCube.payload.maturityDate = New Date(xPeriodStart.AddMonths(objLoanApplication._NoOfInstallment - 1).Year, xPeriodStart.AddMonths(objLoanApplication._NoOfInstallment - 1).Month, 1).ToString("yyyy-MM-dd")
                        objLoanFlexCube.payload.maturityDate = New Date(xPeriodStart.AddMonths(objLoanApplication._NoOfInstallment - 1).Year, xPeriodStart.AddMonths(objLoanApplication._NoOfInstallment - 1).Month, 25).ToString("yyyy-MM-dd")
                        'Pinkal (15-May-2023) -- End
                'Pinkal (14-Dec-2022) -- End
                objLoanFlexCube.payload.loanAmount = objLoanApplication._Loan_Amount
                objLoanFlexCube.payload.currency = "TZS"
                objLoanFlexCube.payload.remark = "Loan request"

                If objMasterData.GetSetFlexCubeLoanRequest(mstrNewLoanRequestFlexcubeURL, mstrBase64Credential, objLoanFlexCube, mstrError, mstrPostedData, mstrResponseData) = False Then
                    mblnFlag = False
                    exForce = New Exception("New Loan Request Flexcube Error : " & mstrError)
                    Throw exForce
                End If

                    ElseIf mstrTopupRequestFlexcubeURL.Trim.Length > 0 AndAlso mstrBase64Credential.Trim.Length > 0 AndAlso objLoanApplication._IsTopUp Then 'TOP UP LOAN REQUEST   'AndAlso mstrEmpBankAcNo.Trim.Length > 0

                        'Pinkal (28-Apr-2023) -- Start
                        '(A1X-860,A1X-861,A1X-862) NMB - Testing loan disbursement API - car loan,pre-paid loan,mortgage loan
                        'Dim mstrCustomerNo As String = GetFlexCubeEmpCustomerNo(mstrOracleHostName, mstrOraclePortNo, mstrOracleServiceName, mstrOracleUserName, mstrOracleUserPassword, mstrEmpBankAcNo)
                        'Pinkal (28-Apr-2023) -- End

                Dim objLoanAdvance As New clsLoan_Advance
                Dim dsFlexcube As DataSet = objLoanAdvance.GetFlexcubeLoanData(mstrOracleHostName, mstrOraclePortNo, mstrOracleServiceName, mstrOracleUserName, mstrOracleUserPassword, mstrEmpBankAcNo, mstrLoanSchemeCode, False, False)
                'Hemant (01 Sep 2023) -- [blnUseOldView := False]
                objLoanAdvance = Nothing

                If dsFlexcube IsNot Nothing AndAlso dsFlexcube.Tables(0).Rows.Count > 0 Then
                    objLoanFlexCube_RollOver.service = "aruti-loan-automation"
                    objLoanFlexCube_RollOver.type = "loan-roll-over"


                            'Pinkal (02-Jun-2023) -- Start
                            'Checking Loan Application Exist in CBS for NMB.
                            If objLoanApplication._IsPostedError Then
                                If mblnLoanApplicationExistInCBS Then
                                    objLoanApplication._ReponseData = "{""statusCode"": 600,""message"": ""Success"",""body"": {""reason"": ""ST-SAVE-052:Successfully Saved and Authorized""}}"
                                    objLoanApplication._IsPostedError = False
                                    objLoanApplication._Userunkid = xUserUnkid
                                    objLoanApplication._WebClientIP = mstrClientIP
                                    objLoanApplication._WebFormName = mstrFormName
                                    objLoanApplication._WebHostName = mstrHostName

                                    If objLoanApplication.Update(xDatabaseName, xUserUnkid, xYearUnkid, xCompanyUnkid, xPeriodStart, xPeriodEnd, xUserModeSetting, xOnlyApproved, xIncludeIn_ActiveEmployee, False, Nothing, Nothing) = False Then
                                        mblnFlag = False
                                        exForce = New Exception(objLoanApplication._Message)
                                        Throw exForce
                                    End If
                                    Return mblnFlag
                                Else
                                    objLoanFlexCube_RollOver.payload.transactionReference = objLoanApplication._Application_No + mdtServerDate.ToString("yyyyMMddhhmmssff")
                                End If  '  If mblnLoanApplicationExistInCBS Then
                            Else
                                objLoanFlexCube_RollOver.payload.transactionReference = objLoanApplication._Application_No
                            End If   ' If objLoanApplication._IsPostedError Then

                            ' objLoanFlexCube_RollOver.payload.transactionReference = mstrLoanSchemeCode + objLoanApplication._Application_No   CHANGED ON GUIDANCE OF MATTHEW TO REMOVE LOAN SCHEME CODE FROM SYSTEM FOR UNIFORMITY[02-Jun-2023 12:41 PM]
                            ' objLoanFlexCube_RollOver.payload.applicationNumber = mstrLoanSchemeCode + objLoanApplication._Application_No   CHANGED ON GUIDANCE OF MATTHEW TO REMOVE LOAN SCHEME CODE FROM SYSTEM FOR UNIFORMITY[02-Jun-2023 12:41 PM]
                            objLoanFlexCube_RollOver.payload.applicationNumber = objLoanApplication._Application_No
                            'Pinkal (02-Jun-2023) -- End

                    objLoanFlexCube_RollOver.payload.accountNumber = mstrEmpBankAcNo
                    objLoanFlexCube_RollOver.payload.loanAccountNumber = dsFlexcube.Tables(0).Rows(0)("ACCOUNT_NUMBER").ToString()
                    objLoanFlexCube_RollOver.payload.productCode = mstrLoanSchemeCode
                    objLoanFlexCube_RollOver.payload.customerNumber = mstrCustomerNo
                    objLoanFlexCube_RollOver.payload.outStandingPrincipal = mdecOutStanding_PrincipalAmt
                    'objLoanFlexCube.payload.loanAmount = mdecOutStanding_PrincipalAmt + mdecLoan_Amount
                    objLoanFlexCube_RollOver.payload.loanAmount = objLoanApplication._Loan_Amount
                    objLoanFlexCube_RollOver.payload.totalOutstanding = mdecOutStanding_PrincipalAmt + mdecOutStanding_InterestAmt
                    objLoanFlexCube_RollOver.payload.currency = "TZS"

                    'Pinkal (14-Dec-2022) -- Start
                    'NMB Loan Module Enhancement. [AS PER UAT DOCUMENT]
                    'objLoanFlexCube.payload.maturityDate = CDate(dsFlexcube.Tables(0).Rows(0)("MATURITY_DATE").ToString()).ToString("yyyy-MM-dd")
                    'objLoanFlexCube.payload.maturityDate = New Date(xPeriodStart.AddMonths(objLoanApplication._NoOfInstallment).Year, xPeriodStart.AddMonths(objLoanApplication._NoOfInstallment).Month, 1).ToString("yyyy-MM-dd")
                            'Pinkal (15-May-2023) -- Start
                            'As per NMB and Matthew's comment Changing maturity Date of 1st Date to 25th.
                            'objLoanFlexCube_RollOver.payload.maturityDate = New Date(xPeriodStart.AddMonths(objLoanApplication._NoOfInstallment - 1).Year, xPeriodStart.AddMonths(objLoanApplication._NoOfInstallment - 1).Month, 1).ToString("yyyy-MM-dd")
                            objLoanFlexCube_RollOver.payload.maturityDate = New Date(xPeriodStart.AddMonths(objLoanApplication._NoOfInstallment - 1).Year, xPeriodStart.AddMonths(objLoanApplication._NoOfInstallment - 1).Month, 25).ToString("yyyy-MM-dd")
                            'Pinkal (15-May-2023) -- End
                    'Pinkal (14-Dec-2022) -- End


                    'TEMPARORY SET DATE 05-SEP-2022 FOR UAT REMOVE IT AS SOON AS POSSIBLE
                    'Pinkal (14-Dec-2022) -- Start
                    'NMB Loan Module Enhancement.

                            'Pinkal (23-Dec-2023) -- Start
                            '(A1X-1555)  NMB - P2P Changes for Claim Request and Company Asset.
                            'objLoanFlexCube_RollOver.payload.valueDate = mdtApprovaldate.Date.ToString("yyyy-MM-dd") 'Now.Date.ToString("yyyy-MM-dd")
                            objLoanFlexCube_RollOver.payload.valueDate = Now.Date.ToString("yyyy-MM-dd") 'Now.Date.ToString("yyyy-MM-dd")
                            'Pinkal (23-Dec-2023) -- End

                            'objLoanFlexCube_RollOver.payload.valueDate = "2022-09-05"
                    'Pinkal (14-Dec-2022) -- End

                    'TEMPARORY SET DATE 05-SEP-2022 FOR UAT REMOVE IT AS SOON AS POSSIBLE

                    objLoanFlexCube_RollOver.payload.outStandingInterest = mdecOutStanding_InterestAmt
                    objLoanFlexCube_RollOver.payload.remark = "Loan request"
                    'objLoanFlexCube_RollOver.payload.productCategory = Nothing

                    If objMasterData.GetSetFlexCubeLoanRequest(mstrTopupRequestFlexcubeURL, mstrBase64Credential, objLoanFlexCube_RollOver, mstrError, mstrPostedData, mstrResponseData) = False Then
                        mblnFlag = False
                        exForce = New Exception("Top up Request Flexcube Error : " & mstrError)
                        Throw exForce
                    End If

                End If  ' If dsFlexcube IsNot Nothing AndAlso dsFlexcube.Tables(0).Rows.Count > 0 Then

            End If  ' If mstrNewLoanRequestFlexcubeURL.Trim.Length > 0 AndAlso mstrBase64Credential.Trim.Length > 0 AndAlso objLoanApplication._IsTopUp = False AndAlso mstrEmpBankAcNo.Trim.Length > 0 Then   'NEW LOAN REQUEST 

            End Select  'Select Case mstrLoanSchemeCode.Trim().ToUpper()
            'Pinkal (28-Apr-2023) -- End

            objMasterData = Nothing

            objLoanApplication._PostedData = mstrPostedData
            objLoanApplication._ReponseData = mstrResponseData

            'Pinkal (14-Dec-2022) -- Start
            'NMB Loan Module Enhancement.
            'Dim dtTable As DataTable = JsonStringToDataTable(mstrResponseData)
            dtTable = JsonStringToDataTable(mstrResponseData)
            'Pinkal (14-Dec-2022) -- End
            If dtTable IsNot Nothing AndAlso dtTable.Rows.Count > 0 Then
                If CInt(dtTable.Rows(0)("statusCode")) <> 600 Then
                    objLoanApplication._IsPostedError = True
                Else
                    objLoanApplication._IsPostedError = False
                End If
            End If

            objLoanApplication._Userunkid = xUserUnkid
            objLoanApplication._ClientIP = mstrClientIP
            objLoanApplication._FormName = mstrFormName
            objLoanApplication._HostName = mstrHostName

            If objLoanApplication.Update(xDatabaseName, xUserUnkid, xYearUnkid, xCompanyUnkid, xPeriodStart, xPeriodEnd, xUserModeSetting, xOnlyApproved, xIncludeIn_ActiveEmployee, False, Nothing, Nothing) = False Then
                mblnFlag = False
                exForce = New Exception(objLoanApplication._Message)
                Throw exForce
            End If

            'Pinkal (16-Nov-2023) -- Start
            '(A1X-1489) NMB - Mortgage tranche disbursement API.

            If mstrLoanSchemeCode.Trim().ToUpper() = "CL28" AndAlso objLoanApplication._IsPostedError = False AndAlso mstrPostedData.Trim.Length > 0 AndAlso mstrResponseData.Trim.Length > 0 Then  'Mortgage Loan and Applied for first Loan Tranche

                Dim objLoanTranche As New clsloanTranche_request_Tran
                objLoanTranche._Processpendingloanunkid = mintProcesspendingloanunkid
                objLoanTranche._Employeeunkid = mintEmployeeunkid
                objLoanTranche._Loanschemeunkid = objLoanApplication._Loanschemeunkid
                objLoanTranche._Tranchedate = mdtFirstTrancheDate.Date
                objLoanTranche._Trancheamt = mdecFirstTranche
                objLoanTranche._Statusunkid = enLoanApplicationStatus.PENDING
                objLoanTranche._Countryunkid = mintCountryunkid
                objLoanTranche._DeductionPeriodunkid = mintDeductionperiodunkid
                objLoanTranche._FinalDeductionPeriodunkid = 0
                objLoanTranche._Finalapproverunkid = 0
                objLoanTranche._Approved_Trancheamt = 0
                objLoanTranche._SendEmpReminderForNextTrance = Nothing

                objLoanTranche._Remark = ""
                objLoanTranche._Document = Nothing
                objLoanTranche._Isvoid = False
                objLoanTranche._Voiduserunkid = -1
                objLoanTranche._Voiddatetime = Nothing
                objLoanTranche._Voidreason = ""
                objLoanTranche._Userunkid = xUserUnkid

                objLoanTranche._Loginemployeeunkid = -1
                objLoanTranche._ClientIP = mstrClientIP
                objLoanTranche._HostName = mstrHostName
                objLoanTranche._FormName = mstrFormName
                objLoanTranche._IsFromWeb = mblnIsFromWeb

                If objLoanTranche.Insert(xDatabaseName, xUserUnkid, xYearUnkid, xCompanyUnkid) = False Then
                    mblnFlag = False
                    exForce = New Exception(objLoanTranche._Message)
                    Throw exForce
                Else
                    objLoanTranche.Send_NotificationLoanTranche_Approver(xDatabaseName, xYearUnkid, mstrUserAccessModeSettings, objLoanTranche._Tranchedate.Date, objLoanTranche._Loanschemeunkid, objLoanTranche._Employeeunkid, objLoanTranche._Loantrancherequestunkid.ToString() _
                                                                                                        , mstrArutiSelfServiceURL, xCompanyUnkid, enLogin_Mode.MGR_SELF_SERVICE, 0, xUserUnkid)
                End If
                objLoanTranche = Nothing

            End If   ' If mstrLoanSchemeCode.Trim().ToUpper() = "CL28" AndAlso objLoanApplication._IsPostedError = False AndAlso mstrPostedData.Trim.Length > 0 AndAlso mstrResponseData.Trim.Length > 0 Then

            'Pinkal (16-Nov-2023) -- End

        Catch ex As Exception
            mblnFlag = False
            Throw New Exception(ex.Message & "; Procedure Name: SendFlexcubeRequest; Module Name: " & mstrModuleName)
        Finally
            'Pinkal (14-Dec-2022) -- Start
            'NMB Loan Module Enhancement.
            If mblnFlag = False Then
                SendLoanFlexcubeFailureNotificationToUsers(xCompanyUnkid, xPeriodStart, mstrLoanFlexcubeFailureNotificationUserIds, objLoanApplication._Application_No.Trim, objLoanApplication._Employeeunkid)
            Else
                If objLoanApplication._IsPostedError Then
                    SendLoanFlexcubeFailureNotificationToUsers(xCompanyUnkid, xPeriodStart, mstrLoanFlexcubeFailureNotificationUserIds, objLoanApplication._Application_No.Trim, objLoanApplication._Employeeunkid)
                End If
            End If
            'Pinkal (14-Dec-2022) -- End
            objMasterData = Nothing
            objLoanFlexCube = Nothing
            objLoanApplication = Nothing
        End Try
        Return mblnFlag
    End Function

    'Pinkal (23-Nov-2022) -- End

    'Pinkal (14-Dec-2022) -- Start
    'NMB Loan Module Enhancement.
    Public Sub SendLoanFlexcubeFailureNotificationToUsers(ByVal xCompanyId As Integer, ByVal mdtEmployeeAsonDate As Date, ByVal xUserIds As String, ByVal mstrApplicationNo As String, ByVal xEmployeeId As Integer)
        Dim StrMessage As String = String.Empty
        Dim mstrSenderAddress As String = String.Empty
        Dim mstrEmployeeName As String = String.Empty
        Try
            If xUserIds.Trim.Length > 0 Then

                Dim objcompany As New clsCompany_Master
                objcompany._Companyunkid = xCompanyId
                mstrSenderAddress = objcompany._Senderaddress
                objcompany = Nothing


                Dim objEmployee As New clsEmployee_Master
                objEmployee._Employeeunkid(mdtEmployeeAsonDate) = xEmployeeId
                mstrEmployeeName = objEmployee._Firstname & " " & objEmployee._Surname
                objEmployee = Nothing

                Dim objUsr As New clsUserAddEdit
                Dim info1 As System.Globalization.TextInfo = System.Globalization.CultureInfo.InvariantCulture.TextInfo
                For Each sId As String In xUserIds.Trim.Split(CChar(","))

                    StrMessage = ""
                    If sId.Trim = "" Then Continue For

                    objUsr._Userunkid = CInt(sId)

                    If objUsr._Firstname.Trim.Length <= 0 AndAlso objUsr._Lastname.Trim.Length <= 0 Then
                        StrMessage &= Language.getMessage(mstrModuleName, 5, "Dear") & " " & info1.ToTitleCase(objUsr._Username) & ", <BR><BR>"
                    Else
                        StrMessage &= Language.getMessage(mstrModuleName, 5, "Dear") & " " & info1.ToTitleCase(objUsr._Firstname & " " & objUsr._Lastname) & ", <BR><BR>"

                    End If

                    StrMessage &= Language.getMessage(mstrModuleName, 6, " This is to inform you that auto disbursement of Loan Application no") & " <b>(" & mstrApplicationNo.Trim & ")</b>" & _
                                              " " & Language.getMessage(mstrModuleName, 7, "I.F.O") & " " & "<b>(" & info1.ToTitleCase(mstrEmployeeName) & ")</b>" & " " & Language.getMessage(mstrModuleName, 8, "has failed. Kindly log into Aruti disbursement dashboard for more information.")

                    StrMessage &= "<BR></BR><BR></BR>"
                    StrMessage &= Language.getMessage(mstrModuleName, 9, "Regards.")

                    StrMessage &= "<p><center style='padding-top:25px;font-size:10.0pt;font-family:&quot;Verdana&quot;,&quot;Sans-Serif&quot;;color:#333399;margin-left:0px;margin-right:0px;margin-top:0px;margin-bottom:10px'><b>&quot;POWERED BY ARUTI HR &amp; PAYROLL MANAGEMENT SOFTWARE.&quot;</b></center></p>"
                    StrMessage &= "</BODY></HTML>"


                    Dim objSendMail As New clsSendMail
                    objSendMail._ToEmail = objUsr._Email
                    objSendMail._Subject = Language.getMessage(mstrModuleName, 10, "Failure in Disbursement")
                    objSendMail._Message = StrMessage
                    objSendMail._Form_Name = mstrFormName
                    objSendMail._LogEmployeeUnkid = -1
                    objSendMail._OperationModeId = enLogin_Mode.MGR_SELF_SERVICE
                    objSendMail._UserUnkid = objUsr._Userunkid
                    objSendMail._SenderAddress = mstrSenderAddress
                    objSendMail._ModuleRefId = clsSendMail.enAT_VIEW_TYPE.LOAN_MGT
                    If objSendMail.SendMail(xCompanyId).ToString.Length > 0 Then
                        Continue For
                    End If
                Next
                info1 = Nothing
                objUsr = Nothing
            End If
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: SendLoanFlexcubeFailureNotificationToUsers; Module Name: " & mstrModuleName)
        End Try
    End Sub
    'Pinkal (14-Dec-2022) -- End

    'Hemant (03 Feb 2023) -- Start
    'ENHANCEMENT(NMB) : A1X-612 - As a user, I want to have an effective date for reporting to detail on employee reporting to screen
    Public Function GetProcessPendingLoanList(ByVal mintApproverEmployeeID As Integer, ByVal intEmployeeUnkId As Integer, Optional ByVal mobjDataOperation As clsDataOperation = Nothing) As String
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception
        Dim mstrFormID As String = String.Empty
        Try
            Dim objDataOperation As clsDataOperation = Nothing

            If mobjDataOperation IsNot Nothing Then
                objDataOperation = mobjDataOperation
            Else
                objDataOperation = New clsDataOperation
            End If

            objDataOperation.ClearParameters()

            strQ = "SELECT " & _
                   "ISNULL( " & _
                   "STUFF((SELECT " & _
                             "',' + CAST(lnroleloanapproval_process_tran.pendingloanaprovalunkid AS NVARCHAR(MAX)) " & _
                        "FROM lnroleloanapproval_process_tran " & _
                        " LEFT JOIN lnloan_process_pending_loan on lnloan_process_pending_loan.processpendingloanunkid = lnroleloanapproval_process_tran.processpendingloanunkid AND lnloan_process_pending_loan.isvoid = 0 " & _
                        "WHERE lnroleloanapproval_process_tran.isvoid = 0 " & _
                        "AND lnloan_process_pending_loan.loan_statusunkid = " & enLoanApplicationStatus.PENDING & " " & _
                        "AND lnroleloanapproval_process_tran.statusunkid = " & enLoanApplicationStatus.PENDING & " " & _
                        "AND lnloan_process_pending_loan.employeeunkid = " & intEmployeeUnkId & " " & _
                        "AND lnroleloanapproval_process_tran.mappingunkid = -1 " & _
                        "AND lnroleloanapproval_process_tran.roleunkid = -1 " & _
                        "AND lnroleloanapproval_process_tran.levelunkid = -1 " & _
                        "AND lnroleloanapproval_process_tran.priority = -1 " & _
                        "AND lnroleloanapproval_process_tran.mapuserunkid  = " & mintApproverEmployeeID & " " & _
                        "ORDER BY lnroleloanapproval_process_tran.processpendingloanunkid " & _
                        "FOR XML PATH ('')) " & _
                   ", 1, 1, '' " & _
                   "), '') AS CSV "

            dsList = objDataOperation.ExecQuery(strQ, "LoanList")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            mstrFormID = dsList.Tables(0).Rows(0)("CSV").ToString()

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "GetProcessPendingLoanList", mstrModuleName)
        End Try
        Return mstrFormID
    End Function
    'Hemant (03 Feb 2023) -- End

    'Pinkal (02-Jun-2023) -- Start
    'Checking Loan Application Exist in CBS for NMB.
    Public Function LoanApplicantExistInCBS(ByVal strOracleHostName As String, _
                                                              ByVal strOraclePortNo As String, _
                                                              ByVal strOracleServiceName As String, _
                                                              ByVal strOracleUserName As String, _
                                                              ByVal strOracleUserPassword As String, _
                                                              ByVal mstrLoanTransactionReference As String, _
                                                              ByVal mstrEmpBankAcNo As String) As Boolean
        Dim mblnFlag As Boolean = False
        Dim dsList As New DataSet
        Try

            Dim strConn As String = "Data Source=(DESCRIPTION=(ADDRESS_LIST=(ADDRESS=(PROTOCOL=TCP)(HOST=" & strOracleHostName & ")(PORT=" & strOraclePortNo & ")))(CONNECT_DATA=(SERVER=DEDICATED)(SERVICE_NAME=" & strOracleServiceName & "))); User Id=" & strOracleUserName & ";Password=" & strOracleUserPassword & "; "
            Using cnnOracle As OracleConnection = New OracleConnection
                cnnOracle.ConnectionString = strConn

                Try
                    cnnOracle.Open()
                Catch ex As Exception
                    Throw ex
                End Try

                Dim StrQ As String = " SELECT * " & _
                                                " FROM fcubs.NMB_STAFF_LOANS_ALL " & _
                                                " WHERE CUST_AC_NO IN ('" & mstrEmpBankAcNo & "') AND APPLICATION_NUM IN ('" & mstrLoanTransactionReference & "')"

                Using cmdOracle As New OracleCommand()

                    If cnnOracle.State = ConnectionState.Closed Or cnnOracle.State = ConnectionState.Broken Then
                        cnnOracle.Open()
                    End If
                    cmdOracle.Connection = cnnOracle
                    cmdOracle.CommandType = CommandType.Text
                    cmdOracle.CommandText = StrQ
                    cmdOracle.Parameters.Clear()
                    Dim oda As New OracleDataAdapter(cmdOracle)
                    oda.Fill(dsList)
                End Using
            End Using

            If dsList IsNot Nothing AndAlso dsList.Tables(0).Rows.Count > 0 Then
                Dim xLoanTransactionReference As String = dsList.Tables(0).Rows(0)("APPLICATION_NUM").ToString()
                If xLoanTransactionReference.Trim.Length > 0 Then mblnFlag = True Else mblnFlag = False
            End If

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: LoanApplicantExistInCBS; Module Name: " & mstrModuleName)
        End Try
        Return mblnFlag
    End Function
    'Pinkal (02-Jun-2023) -- End


    '<Language> This Auto Generated Text Please Do Not Modify it.
#Region " Language & UI Settings "
    Public Shared Sub SetMessages()
        Try
            Language.setMessage(mstrModuleName, 1, "Pending")
            Language.setMessage(mstrModuleName, 2, "Approved")
            Language.setMessage(mstrModuleName, 3, "Rejected")
            Language.setMessage(mstrModuleName, 4, "Reporting To")

        Catch Ex As Exception
            DisplayError.Show("-1", Ex.Message, "SetMessages", mstrModuleName)
        End Try
    End Sub
#End Region 'Language & UI Settings
    '</Language>
End Class