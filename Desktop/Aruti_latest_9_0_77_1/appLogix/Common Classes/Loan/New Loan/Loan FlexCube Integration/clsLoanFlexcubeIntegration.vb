﻿Imports System.Runtime.Serialization
Imports Newtonsoft.Json
Imports System.Windows

Public Class clsLoanFlexCubeIntegration

    Dim mstrService As String = ""
    Dim mstrType As String = ""
    Dim clsPayload As New payload

#Region "Properties"

    Public Property service() As String
        Get
            Return mstrService
        End Get
        Set(ByVal value As String)
            mstrService = value
        End Set
    End Property

    Public Property type() As String
        Get
            Return mstrType
        End Get
        Set(ByVal value As String)
            mstrType = value
        End Set
    End Property

    Public Property payload() As payload
        Get
            Return clsPayload
        End Get
        Set(ByVal value As payload)
            clsPayload = value
        End Set
    End Property

#End Region

End Class

<DataContract()> _
Public Class payload

    Dim mstrTransactionReference As String = ""
    Dim mstrApplicationNumber As String = ""
    Dim mstrAccountNumber As String = ""
    'Dim mstrLoanAccountNumber As String = ""
    Dim mstrProductCode As String = ""
    Dim mstrProductCategory As String = ""
    Dim mstrCustomerNumber As String = ""
    'Dim mdecOutStandingPrincipal As Decimal = 0
    'Dim mdecTotalOutStanding As Decimal = 0
    Dim mstrMaturityDate As String = Nothing
    'Pinkal (28-Apr-2023) -- Start
    '(A1X-860,A1X-861,A1X-862) NMB - Testing loan disbursement API - car loan,pre-paid loan,mortgage loan.
    'Dim mstrValueDate As String = ""
    'Pinkal (28-Apr-2023) -- End
    Dim mdecLoanAmount As Decimal = 0
    Dim mstrCurrency As String = ""
    'Dim mdecOutStandingInterest As Decimal = 0
    Dim mstrRemark As String = ""

#Region "Properties"

    <DataMember()> _
    Public Property transactionReference() As String
        Get
            Return mstrTransactionReference
        End Get
        Set(ByVal value As String)
            mstrTransactionReference = value
        End Set
    End Property

    <DataMember()> _
    Public Property applicationNumber() As String
        Get
            Return mstrApplicationNumber
        End Get
        Set(ByVal value As String)
            mstrApplicationNumber = value
        End Set
    End Property

    <DataMember()> _
    Public Property accountNumber() As String
        Get
            Return mstrAccountNumber
        End Get
        Set(ByVal value As String)
            mstrAccountNumber = value
        End Set
    End Property

    '<DataMember(EmitDefaultValue:=False)> _
    'Public Property loanAccountNumber() As String
    '    Get
    '        Return mstrLoanAccountNumber
    '    End Get
    '    Set(ByVal value As String)
    '        mstrLoanAccountNumber = value
    '    End Set
    'End Property

    <DataMember()> _
    Public Property productCode() As String
        Get
            Return mstrProductCode
        End Get
        Set(ByVal value As String)
            mstrProductCode = value
        End Set
    End Property

    '<DataMember(EmitDefaultValue:=False)> _
    <DataMember()> _
    Public Property productCategory() As String
        Get
            Return mstrProductCategory
        End Get
        Set(ByVal value As String)
            mstrProductCategory = value
        End Set
    End Property

    <DataMember()> _
    Public Property customerNumber() As String
        Get
            Return mstrCustomerNumber
        End Get
        Set(ByVal value As String)
            mstrCustomerNumber = value
        End Set
    End Property

    ' <DataMember(EmitDefaultValue:=False)> _
    'Public Property outStandingPrincipal() As Decimal
    '     Get
    '         Return mdecOutStandingPrincipal
    '     End Get
    '     Set(ByVal value As Decimal)
    '         mdecOutStandingPrincipal = value
    '     End Set
    ' End Property

    ' <DataMember(EmitDefaultValue:=False)> _
    'Public Property totalOutstanding() As Decimal
    '     Get
    '         Return mdecTotalOutStanding
    '     End Get
    '     Set(ByVal value As Decimal)
    '         mdecTotalOutStanding = value
    '     End Set
    ' End Property

    <DataMember()> _
    Public Property maturityDate() As String
        Get
            Return mstrMaturityDate
        End Get
        Set(ByVal value As String)
            mstrMaturityDate = value
        End Set
    End Property

    'Pinkal (28-Apr-2023) -- Start
    '(A1X-860,A1X-861,A1X-862) NMB - Testing loan disbursement API - car loan,pre-paid loan,mortgage loan.
    ' <DataMember(EmitDefaultValue:=False)> _
    'Public Property valueDate() As String
    '     Get
    '         Return mstrValueDate
    '     End Get
    '     Set(ByVal value As String)
    '         mstrValueDate = value
    '     End Set
    ' End Property
    'Pinkal (28-Apr-2023) -- End

    <DataMember()> _
    Public Property loanAmount() As Decimal
        Get
            Return mdecLoanAmount
        End Get
        Set(ByVal value As Decimal)
            mdecLoanAmount = value
        End Set
    End Property

    <DataMember()> _
    Public Property currency() As String
        Get
            Return mstrCurrency
        End Get
        Set(ByVal value As String)
            mstrCurrency = value
        End Set
    End Property

    '  <DataMember(EmitDefaultValue:=False)> _
    'Public Property outStandingInterest() As Decimal
    '      Get
    '          Return mdecOutStandingInterest
    '      End Get
    '      Set(ByVal value As Decimal)
    '          mdecOutStandingInterest = value
    '      End Set
    '  End Property

    <DataMember()> _
    Public Property remark() As String
        Get
            Return mstrRemark
        End Get
        Set(ByVal value As String)
            mstrRemark = value
        End Set
    End Property

#End Region

End Class


Public Class clsLoanFlexCubeIntegration_RollOver

    Dim mstrService As String = ""
    Dim mstrType As String = ""
    'Dim clsPayload As New payload
    Dim clsPayload_rollover As New payload_rollover

#Region "Properties"

    Public Property service() As String
        Get
            Return mstrService
        End Get
        Set(ByVal value As String)
            mstrService = value
        End Set
    End Property

    Public Property type() As String
        Get
            Return mstrType
        End Get
        Set(ByVal value As String)
            mstrType = value
        End Set
    End Property

    <DataMember(Name:="payload")> _
    Public Property payload() As payload_rollover
        Get
            Return clsPayload_rollover
        End Get
        Set(ByVal value As payload_rollover)
            clsPayload_rollover = value
        End Set
    End Property

#End Region

End Class

<DataContract()> _
Public Class payload_rollover

    Dim mstrTransactionReference As String = ""
    Dim mstrApplicationNumber As String = ""
    Dim mstrAccountNumber As String = ""
    Dim mstrLoanAccountNumber As String = ""
    Dim mstrProductCode As String = ""
    'Dim mstrProductCategory As String = ""
    Dim mstrCustomerNumber As String = ""
    Dim mdecOutStandingPrincipal As Decimal = 0
    Dim mdecTotalOutStanding As Decimal = 0
    Dim mstrMaturityDate As String = Nothing
    Dim mstrValueDate As String = Nothing
    Dim mdecLoanAmount As Decimal = 0
    Dim mstrCurrency As String = ""
    Dim mdecOutStandingInterest As Decimal = 0
    Dim mstrRemark As String = ""

#Region "Properties"

    <DataMember()> _
    Public Property transactionReference() As String
        Get
            Return mstrTransactionReference
        End Get
        Set(ByVal value As String)
            mstrTransactionReference = value
        End Set
    End Property

    <DataMember()> _
    Public Property applicationNumber() As String
        Get
            Return mstrApplicationNumber
        End Get
        Set(ByVal value As String)
            mstrApplicationNumber = value
        End Set
    End Property

    <DataMember()> _
    Public Property accountNumber() As String
        Get
            Return mstrAccountNumber
        End Get
        Set(ByVal value As String)
            mstrAccountNumber = value
        End Set
    End Property

    <DataMember()> _
    Public Property loanAccountNumber() As String
        Get
            Return mstrLoanAccountNumber
        End Get
        Set(ByVal value As String)
            mstrLoanAccountNumber = value
        End Set
    End Property

    <DataMember()> _
    Public Property productCode() As String
        Get
            Return mstrProductCode
        End Get
        Set(ByVal value As String)
            mstrProductCode = value
        End Set
    End Property

    '<DataMember(EmitDefaultValue:=False)> _
    'Public Property productCategory() As String
    '    Get
    '        Return mstrProductCategory
    '    End Get
    '    Set(ByVal value As String)
    '        mstrProductCategory = value
    '    End Set
    'End Property

    <DataMember()> _
    Public Property customerNumber() As String
        Get
            Return mstrCustomerNumber
        End Get
        Set(ByVal value As String)
            mstrCustomerNumber = value
        End Set
    End Property

    <DataMember()> _
   Public Property outStandingPrincipal() As Decimal
        Get
            Return mdecOutStandingPrincipal
        End Get
        Set(ByVal value As Decimal)
            mdecOutStandingPrincipal = value
        End Set
    End Property

    <DataMember()> _
   Public Property totalOutstanding() As Decimal
        Get
            Return mdecTotalOutStanding
        End Get
        Set(ByVal value As Decimal)
            mdecTotalOutStanding = value
        End Set
    End Property

    <DataMember()> _
    Public Property maturityDate() As String
        Get
            Return mstrMaturityDate
        End Get
        Set(ByVal value As String)
            mstrMaturityDate = value
        End Set
    End Property

    <DataMember()> _
   Public Property valueDate() As String
        Get
            Return mstrValueDate
        End Get
        Set(ByVal value As String)
            mstrValueDate = value
        End Set
    End Property

    <DataMember()> _
    Public Property loanAmount() As Decimal
        Get
            Return mdecLoanAmount
        End Get
        Set(ByVal value As Decimal)
            mdecLoanAmount = value
        End Set
    End Property

    <DataMember()> _
    Public Property currency() As String
        Get
            Return mstrCurrency
        End Get
        Set(ByVal value As String)
            mstrCurrency = value
        End Set
    End Property

    <DataMember()> _
  Public Property outStandingInterest() As Decimal
        Get
            Return mdecOutStandingInterest
        End Get
        Set(ByVal value As Decimal)
            mdecOutStandingInterest = value
        End Set
    End Property

    <DataMember()> _
    Public Property remark() As String
        Get
            Return mstrRemark
        End Get
        Set(ByVal value As String)
            mstrRemark = value
        End Set
    End Property

#End Region

End Class


'Pinkal (28-Apr-2023) -- Start
'(A1X-860,A1X-861,A1X-862) NMB - Testing loan disbursement API - car loan,pre-paid   loan,mortgage loan    

Public Class clsCarPrepaidAnnualLoanFlexCubeIntegration

    Dim mstrService As String = ""
    Dim mstrType As String = ""
    Dim clsPayload_CarPrepaidAnnualLoan As New payload_CarPrepaidAnnualLoan

#Region "Properties"

    Public Property service() As String
        Get
            Return mstrService
        End Get
        Set(ByVal value As String)
            mstrService = value
        End Set
    End Property

    Public Property type() As String
        Get
            Return mstrType
        End Get
        Set(ByVal value As String)
            mstrType = value
        End Set
    End Property

    <DataMember(Name:="payload")> _
    Public Property payload() As payload_CarPrepaidAnnualLoan
        Get
            Return clsPayload_CarPrepaidAnnualLoan
        End Get
        Set(ByVal value As payload_CarPrepaidAnnualLoan)
            clsPayload_CarPrepaidAnnualLoan = value
        End Set
    End Property

#End Region

End Class

<DataContract()> _
Public Class payload_CarPrepaidAnnualLoan

    Dim mstrTransactionReference As String = ""
    Dim mstrApplicationNumber As String = ""
    Dim mstrAccountNumber As String = ""
    Dim mstrProductCode As String = ""
    Dim mstrProductCategory As String = ""
    Dim mstrCustomerNumber As String = ""
    Dim mstrMaturityDate As String = Nothing
    Dim mstrValueDate As String = ""
    Dim mdecLoanAmount As Decimal = 0
    Dim mstrCurrency As String = ""
    Dim mstrRemark As String = ""

    'Pinkal (15-Sep-2023) -- Start
    '(A1X-1166) NMB - Disburse approved tranche amount on Flexcube via API.
    Dim mstrnumberOfInstallments As String = Nothing
    Dim mstrbookDate As String = Nothing
    Dim mstrpostingDate As String = Nothing
    Dim mstrinstallmentStartDate As String = Nothing
    Dim mstrrelationOfficerId As String = Nothing
    Dim mstrsector As String = Nothing
    'Pinkal (15-Sep-2023) -- End

#Region "Properties"

    <DataMember()> _
    Public Property transactionReference() As String
        Get
            Return mstrTransactionReference
        End Get
        Set(ByVal value As String)
            mstrTransactionReference = value
        End Set
    End Property

    <DataMember()> _
    Public Property applicationNumber() As String
        Get
            Return mstrApplicationNumber
        End Get
        Set(ByVal value As String)
            mstrApplicationNumber = value
        End Set
    End Property

    <DataMember()> _
    Public Property accountNumber() As String
        Get
            Return mstrAccountNumber
        End Get
        Set(ByVal value As String)
            mstrAccountNumber = value
        End Set
    End Property

    <DataMember()> _
    Public Property productCode() As String
        Get
            Return mstrProductCode
        End Get
        Set(ByVal value As String)
            mstrProductCode = value
        End Set
    End Property

    <DataMember()> _
    Public Property productCategory() As String
        Get
            Return mstrProductCategory
        End Get
        Set(ByVal value As String)
            mstrProductCategory = value
        End Set
    End Property

    <DataMember()> _
    Public Property customerNumber() As String
        Get
            Return mstrCustomerNumber
        End Get
        Set(ByVal value As String)
            mstrCustomerNumber = value
        End Set
    End Property


    'Pinkal (15-Sep-2023) -- Start
    '(A1X-1166) NMB - Disburse approved tranche amount on Flexcube via API.
    <DataMember(EmitDefaultValue:=False)> _
    Public Property maturityDate() As String
        Get
            Return mstrMaturityDate
        End Get
        Set(ByVal value As String)
            mstrMaturityDate = value
        End Set
    End Property

    <DataMember(EmitDefaultValue:=False)> _
    Public Property valueDate() As String
        Get
            Return mstrValueDate
        End Get
        Set(ByVal value As String)
            mstrValueDate = value
        End Set
    End Property
    'Pinkal (15-Sep-2023) -- End

    <DataMember()> _
    Public Property loanAmount() As Decimal
        Get
            Return mdecLoanAmount
        End Get
        Set(ByVal value As Decimal)
            mdecLoanAmount = value
        End Set
    End Property

    <DataMember()> _
    Public Property currency() As String
        Get
            Return mstrCurrency
        End Get
        Set(ByVal value As String)
            mstrCurrency = value
        End Set
    End Property

    <DataMember()> _
    Public Property remark() As String
        Get
            Return mstrRemark
        End Get
        Set(ByVal value As String)
            mstrRemark = value
        End Set
    End Property

    'Pinkal (15-Sep-2023) -- Start
    '(A1X-1166) NMB - Disburse approved tranche amount on Flexcube via API.

    <DataMember(EmitDefaultValue:=False)> _
 Public Property numberOfInstallments() As String
        Get
            Return mstrnumberOfInstallments
        End Get
        Set(ByVal value As String)
            mstrnumberOfInstallments = value
        End Set
    End Property


    <DataMember(EmitDefaultValue:=False)> _
   Public Property bookDate() As String
        Get
            Return mstrbookDate
        End Get
        Set(ByVal value As String)
            mstrbookDate = value
        End Set
    End Property

    <DataMember(EmitDefaultValue:=False)> _
   Public Property postingDate() As String
        Get
            Return mstrpostingDate
        End Get
        Set(ByVal value As String)
            mstrpostingDate = value
        End Set
    End Property

    <DataMember(EmitDefaultValue:=False)> _
   Public Property installmentStartDate() As String
        Get
            Return mstrinstallmentStartDate
        End Get
        Set(ByVal value As String)
            mstrinstallmentStartDate = value
        End Set
    End Property

    <DataMember(EmitDefaultValue:=False)> _
   Public Property relationOfficerId() As String
        Get
            Return mstrrelationOfficerId
        End Get
        Set(ByVal value As String)
            mstrrelationOfficerId = value
        End Set
    End Property

    <DataMember(EmitDefaultValue:=False)> _
   Public Property sector() As String
        Get
            Return mstrsector
        End Get
        Set(ByVal value As String)
            mstrsector = value
        End Set
    End Property

    'Pinkal (15-Sep-2023) -- End


#End Region

End Class

'Pinkal (28-Apr-2023) -- End


'Pinkal (16-Nov-2023) -- Start
'(A1X-1489) NMB - Mortgage tranche disbursement API.


Public Class clsLoanTrancheFlexCubeIntegration

    Dim mstrService As String = ""
    Dim mstrType As String = ""
    Dim clsPayloadLoanTranche As New payload_LoanTranche

#Region "Properties"

    Public Property service() As String
        Get
            Return mstrService
        End Get
        Set(ByVal value As String)
            mstrService = value
        End Set
    End Property

    Public Property type() As String
        Get
            Return mstrType
        End Get
        Set(ByVal value As String)
            mstrType = value
        End Set
    End Property

    <DataMember(Name:="payload")> _
    Public Property payload() As payload_LoanTranche
        Get
            Return clsPayloadLoanTranche
        End Get
        Set(ByVal value As payload_LoanTranche)
            clsPayloadLoanTranche = value
        End Set
    End Property

#End Region


End Class

<DataContract()> _
Public Class payload_LoanTranche

    Dim mstrTransactionReference As String = ""
    Dim mstrLoanAccountNumber As String = ""
    Dim mstrValueDate As String = Nothing
    Dim mstrDisbursementSerialNumber As String = ""
    Dim mdecSettlementAmount As Decimal = 0
    Dim mstrCurrency As String = ""
    Dim mstrSettlementAccountNumber As String = ""
    Dim mstrProductCode As String = ""
    Dim mstrDisbursementChecklistDescription As String = ""
    Dim mstrDisbursementChecklistValue As String = ""
    Dim mstrRemark As String = ""

#Region "Properties"

    <DataMember()> _
    Public Property transactionReference() As String
        Get
            Return mstrTransactionReference
        End Get
        Set(ByVal value As String)
            mstrTransactionReference = value
        End Set
    End Property

    <DataMember()> _
    Public Property loanAccountNumber() As String
        Get
            Return mstrLoanAccountNumber
        End Get
        Set(ByVal value As String)
            mstrLoanAccountNumber = value
        End Set
    End Property

    <DataMember()> _
     Public Property valueDate() As String
        Get
            Return mstrValueDate
        End Get
        Set(ByVal value As String)
            mstrValueDate = value
        End Set
    End Property

    <DataMember()> _
    Public Property disbursementSerialNumber() As String
        Get
            Return mstrDisbursementSerialNumber
        End Get
        Set(ByVal value As String)
            mstrDisbursementSerialNumber = value
        End Set
    End Property

    <DataMember()> _
    Public Property settlementAmount() As Decimal
        Get
            Return mdecSettlementAmount
        End Get
        Set(ByVal value As Decimal)
            mdecSettlementAmount = value
        End Set
    End Property

    <DataMember()> _
  Public Property currency() As String
        Get
            Return mstrCurrency
        End Get
        Set(ByVal value As String)
            mstrCurrency = value
        End Set
    End Property

    <DataMember()> _
   Public Property settlementAccountNumber() As String
        Get
            Return mstrSettlementAccountNumber
        End Get
        Set(ByVal value As String)
            mstrSettlementAccountNumber = value
        End Set
    End Property

    <DataMember()> _
   Public Property productCode() As String
        Get
            Return mstrProductCode
        End Get
        Set(ByVal value As String)
            mstrProductCode = value
        End Set
    End Property

    <DataMember()> _
    Public Property disbursementChecklistDescription() As String
        Get
            Return mstrDisbursementChecklistDescription
        End Get
        Set(ByVal value As String)
            mstrDisbursementChecklistDescription = value
        End Set
    End Property

    <DataMember()> _
    Public Property disbursementChecklistValue() As String
        Get
            Return mstrDisbursementChecklistValue
        End Get
        Set(ByVal value As String)
            mstrDisbursementChecklistValue = value
        End Set
    End Property

    <DataMember()> _
    Public Property remark() As String
        Get
            Return mstrRemark
        End Get
        Set(ByVal value As String)
            mstrRemark = value
        End Set
    End Property

#End Region

End Class

'Pinkal (16-Nov-2023) -- End