﻿'************************************************************************************************************************************
'Class Name : clsloanscheme_role_mapping.vb
'Purpose    :
'Date       :21-Sep-2022
'Written By :Pinkal
'Modified   :
'************************************************************************************************************************************

Imports eZeeCommonLib
''' <summary>
''' Purpose: 
''' Developer: Pinkal
''' </summary>
Public Class clsloanscheme_role_mapping
    Private Shared ReadOnly mstrModuleName As String = "clsloanscheme_role_mapping"
    Dim objDataOperation As clsDataOperation
    Dim mstrMessage As String = ""

#Region " Private variables "
    Private mintMappingunkid As Integer
    Private mintLevelunkid As Integer
    Private mintRoleunkid As Integer
    Private mintLoanschemeunkid As Integer
    Private mblnIsExceptional As Boolean = False
    Private mintUserunkid As Integer
    Private mblnIsvoid As Boolean
    Private mintVoiduserunkid As Integer
    Private mdtVoiddatetime As Date
    Private mstrVoidreason As String = String.Empty
    Private mstrFormName As String = ""
    Private mstrClientIP As String = ""
    Private mstrHostName As String = ""
    Private mblnIsFromWeb As Boolean = False
    Private mintAuditUserId As Integer = 0
    Private mdtAuditDatetime As DateTime = Nothing

    'Pinkal (04-Aug-2023) -- Start
    '(A1X-1158) NMB - Allow to configure up to 5 disbursement tranches for Mortgage loans on the loan approval page.
    Private mblnIsLoanTranche As Boolean = False
    'Pinkal (04-Aug-2023) -- End

#End Region

#Region " Properties "
    ''' <summary>
    ''' Purpose: Get Message from Class 
    ''' Modify By: Pinkal
    ''' </summary>
    Public ReadOnly Property _Message() As String
        Get
            Return mstrMessage
        End Get
    End Property

    ''' <summary>
    ''' Purpose: Get or Set mappingunkid
    ''' Modify By: Pinkal
    ''' </summary>
    Public Property _Mappingunkid(Optional ByVal objDoOperation As clsDataOperation = Nothing) As Integer
        Get
            Return mintMappingunkid
        End Get
        Set(ByVal value As Integer)
            mintMappingunkid = value
            Call GetData(objDoOperation)
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set levelunkid
    ''' Modify By: Pinkal
    ''' </summary>
    Public Property _Levelunkid() As Integer
        Get
            Return mintLevelunkid
        End Get
        Set(ByVal value As Integer)
            mintLevelunkid = Value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set roleunkid
    ''' Modify By: Pinkal
    ''' </summary>
    Public Property _Roleunkid() As Integer
        Get
            Return mintRoleunkid
        End Get
        Set(ByVal value As Integer)
            mintRoleunkid = Value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set loanschemeunkid
    ''' Modify By: Pinkal
    ''' </summary>
    Public Property _Loanschemeunkid() As Integer
        Get
            Return mintLoanschemeunkid
        End Get
        Set(ByVal value As Integer)
            mintLoanschemeunkid = Value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set IsExceptional
    ''' Modify By: Pinkal
    ''' </summary>
    Public Property _IsExceptional() As Boolean
        Get
            Return mblnIsExceptional
        End Get
        Set(ByVal value As Boolean)
            mblnIsExceptional = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set userunkid
    ''' Modify By: Pinkal
    ''' </summary>
    Public Property _Userunkid() As Integer
        Get
            Return mintUserunkid
        End Get
        Set(ByVal value As Integer)
            mintUserunkid = Value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set isvoid
    ''' Modify By: Pinkal
    ''' </summary>
    Public Property _Isvoid() As Boolean
        Get
            Return mblnIsvoid
        End Get
        Set(ByVal value As Boolean)
            mblnIsvoid = Value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set voiduserunkid
    ''' Modify By: Pinkal
    ''' </summary>
    Public Property _Voiduserunkid() As Integer
        Get
            Return mintVoiduserunkid
        End Get
        Set(ByVal value As Integer)
            mintVoiduserunkid = Value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set voiddatetime
    ''' Modify By: Pinkal
    ''' </summary>
    Public Property _Voiddatetime() As Date
        Get
            Return mdtVoiddatetime
        End Get
        Set(ByVal value As Date)
            mdtVoiddatetime = Value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set voidreason
    ''' Modify By: Pinkal
    ''' </summary>
    Public Property _Voidreason() As String
        Get
            Return mstrVoidreason
        End Get
        Set(ByVal value As String)
            mstrVoidreason = Value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set FormName
    ''' Modify By: Pinkal
    ''' </summary>
    Public Property _FormName() As String
        Get
            Return mstrFormName
        End Get
        Set(ByVal value As String)
            mstrFormName = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set ClientIP
    ''' Modify By: Pinkal
    ''' </summary>
    Public Property _ClientIP() As String
        Get
            Return mstrClientIP
        End Get
        Set(ByVal value As String)
            mstrClientIP = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set HostName
    ''' Modify By: Pinkal
    ''' </summary>
    Public Property _HostName() As String
        Get
            Return mstrHostName
        End Get
        Set(ByVal value As String)
            mstrHostName = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set IsFromWeb
    ''' Modify By: Pinkal
    ''' </summary>
    Public Property _IsFromWeb() As Boolean
        Get
            Return mblnIsFromWeb
        End Get
        Set(ByVal value As Boolean)
            mblnIsFromWeb = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set AuditUserId
    ''' Modify By: Pinkal
    ''' </summary>
    Public Property _AuditUserId() As Integer
        Get
            Return mintAuditUserId
        End Get
        Set(ByVal value As Integer)
            mintAuditUserId = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set AuditDatetime
    ''' Modify By: Pinkal
    ''' </summary>
    Public Property _AuditDatetime() As DateTime
        Get
            Return mdtAuditDatetime
        End Get
        Set(ByVal value As DateTime)
            mdtAuditDatetime = value
        End Set
    End Property

    'Pinkal (04-Aug-2023) -- Start
    '(A1X-1158) NMB - Allow to configure up to 5 disbursement tranches for Mortgage loans on the loan approval page.
    ''' <summary>
    ''' Purpose: Get or Set IsLoanTranche
    ''' Modify By: Pinkal
    ''' </summary>
    Public Property _IsLoanTranche() As Boolean
        Get
            Return mblnIsLoanTranche
        End Get
        Set(ByVal value As Boolean)
            mblnIsLoanTranche = value
        End Set
    End Property
    'Pinkal (04-Aug-2023) -- End

#End Region

    ''' <summary>
    ''' Modify By: Pinkal
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>
    Public Sub GetData(Optional ByVal objDoOperation As clsDataOperation = Nothing)
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        If objDoOperation Is Nothing Then
            objDataOperation = New clsDataOperation
        Else
            objDataOperation = objDoOperation
        End If

        Try
            strQ = "SELECT " & _
                      "  mappingunkid " & _
                      ", levelunkid " & _
                      ", roleunkid " & _
                      ", loanschemeunkid " & _
                      ", isexceptional " & _
                      ", userunkid " & _
                      ", isvoid " & _
                      ", voiduserunkid " & _
                      ", voiddatetime " & _
                      ", voidreason " & _
                      ", ISNULL(isloantranche,0) AS isloantranche " & _
                      " FROM lnloanscheme_role_mapping " & _
                      " WHERE mappingunkid = @mappingunkid "

            'Pinkal (04-Aug-2023) -- (A1X-1158) NMB - Allow to configure up to 5 disbursement tranches for Mortgage loans on the loan approval page.[", ISNULL(isloantranche,0) AS isloantranche " & _]

            objDataOperation.ClearParameters()
            objDataOperation.AddParameter("@mappingunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintMappingunkid.ToString)

            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            For Each dtRow As DataRow In dsList.Tables(0).Rows
                mintMappingunkid = CInt(dtRow.Item("mappingunkid"))
                mintLevelunkid = CInt(dtRow.Item("levelunkid"))
                mintRoleunkid = CInt(dtRow.Item("roleunkid"))
                mintLoanschemeunkid = CInt(dtRow.Item("loanschemeunkid"))
                mblnIsExceptional = CBool(dtRow.Item("isexceptional"))
                mintUserunkid = CInt(dtRow.Item("userunkid"))
                mblnIsvoid = CBool(dtRow.Item("isvoid"))
                mintVoiduserunkid = CInt(dtRow.Item("voiduserunkid"))
                If IsDBNull(dtRow.Item("voiddatetime")) = False Then
                    mdtVoiddatetime = dtRow.Item("voiddatetime")
                End If
                mstrVoidreason = dtRow.Item("voidreason").ToString

                'Pinkal (04-Aug-2023) -- Start
                '(A1X-1158) NMB - Allow to configure up to 5 disbursement tranches for Mortgage loans on the loan approval page.
                mblnIsLoanTranche = CBool(dtRow.Item("isloantranche"))
                'Pinkal (04-Aug-2023) -- End

                Exit For
            Next
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: GetData; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            If objDoOperation Is Nothing Then objDataOperation = Nothing
        End Try
    End Sub

    ''' <summary>
    ''' Modify By: Pinkal
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>
    Public Function GetList(ByVal strTableName As String, Optional ByVal blnOnlyActive As Boolean = True, Optional ByVal mblnLoanTranche As Boolean = False _
                                  , Optional ByVal mstrFilter As String = "", Optional ByVal objDataOpr As clsDataOperation = Nothing) As DataSet
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        If objDataOpr Is Nothing Then
            objDataOperation = New clsDataOperation
        Else
            objDataOperation = objDataOpr
        End If


        Try
            strQ = " SELECT " & _
                      "  lnloanscheme_role_mapping.mappingunkid " & _
                      ", lnloanscheme_role_mapping.levelunkid " & _
                      ", ISNULL(lnapproverlevel_master.lnlevelname,'') As Level " & _
                      ", lnapproverlevel_master.priority " & _
                      ", lnloanscheme_role_mapping.roleunkid " & _
                      ", ISNULL(cr.name,'') AS Role " & _
                      ", lnloanscheme_role_mapping.loanschemeunkid " & _
                      ", ISNULL(lnloan_scheme_master.name,'') AS Loan_Scheme " & _
                      ", lnloanscheme_role_mapping.isexceptional " & _
                      ", CASE WHEN lnloanscheme_role_mapping.isexceptional = 1 THEN @Yes ELSE @No End As Exceptional " & _
                      ", lnloanscheme_role_mapping.userunkid " & _
                      ", lnloanscheme_role_mapping.isvoid " & _
                      ", lnloanscheme_role_mapping.voiduserunkid " & _
                      ", lnloanscheme_role_mapping.voiddatetime " & _
                      ", lnloanscheme_role_mapping.voidreason " & _
                      ", ISNULL(lnloanscheme_role_mapping.isloantranche,0) AS isloantranche " & _
                      " FROM lnloanscheme_role_mapping " & _
                      " LEFT JOIN lnloan_scheme_master ON lnloan_scheme_master.loanschemeunkid = lnloanscheme_role_mapping.loanschemeunkid  " & _
                      " LEFT JOIN lnapproverlevel_master ON lnapproverlevel_master.lnlevelunkid = lnloanscheme_role_mapping.levelunkid " & _
                      " LEFT JOIN hrmsConfiguration..cfrole_master cr ON cr.roleunkid = lnloanscheme_role_mapping.roleunkid "

            'Pinkal (04-Aug-2023) -- (A1X-1158) NMB - Allow to configure up to 5 disbursement tranches for Mortgage loans on the loan approval page.[ ISNULL(lnloanscheme_role_mapping.isloantranche,0) AS isloantranche]

            If blnOnlyActive Then
                strQ &= " WHERE lnloanscheme_role_mapping.isvoid =0 "
            End If

            'Pinkal (04-Aug-2023) -- Start
            '(A1X-1158) NMB - Allow to configure up to 5 disbursement tranches for Mortgage loans on the loan approval page.
            strQ &= " AND ISNULL(lnloanscheme_role_mapping.isloantranche,0) = @isloantranche"
            'Pinkal (04-Aug-2023) -- End


            If mstrFilter.Trim.Length > 0 Then
                strQ &= " " & mstrFilter.Trim
            End If

            objDataOperation.ClearParameters()
            objDataOperation.AddParameter("@Yes", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 2, "Yes"))
            objDataOperation.AddParameter("@No", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 3, "No"))

            'Pinkal (04-Aug-2023) -- Start
            '(A1X-1158) NMB - Allow to configure up to 5 disbursement tranches for Mortgage loans on the loan approval page.
            objDataOperation.AddParameter("@isloantranche", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnLoanTranche)
            'Pinkal (04-Aug-2023) -- End

            dsList = objDataOperation.ExecQuery(strQ, strTableName)

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: GetList; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            If objDataOpr Is Nothing Then objDataOperation = Nothing
        End Try
        Return dsList
    End Function

    ''' <summary>
    ''' Modify By: Pinkal
    ''' </summary>
    ''' <returns>Boolean</returns>
    ''' <purpose> INSERT INTO Database Table (lnloanscheme_role_mapping) </purpose>
    Public Function Insert(Optional ByVal objDoOperation As clsDataOperation = Nothing) As Boolean

        'Pinkal (04-Aug-2023) -- Start
        '(A1X-1158) NMB - Allow to configure up to 5 disbursement tranches for Mortgage loans on the loan approval page.
        'If isExist(mintRoleunkid, mintLoanschemeunkid, mintLevelunkid, mblnIsExceptional, mintMappingunkid) Then
        '    mstrMessage = Language.getMessage(mstrModuleName, 1, "This Loan Scheme already exists for this role and level. Please mapped different loan scheme.")
        '    Return False
        'End If
        If isExist(mintRoleunkid, mintLoanschemeunkid, mintLevelunkid, mblnIsExceptional, mblnIsLoanTranche, mintMappingunkid) Then
            mstrMessage = Language.getMessage(mstrModuleName, 1, "This Loan Scheme already exists for this role and level. Please mapped different loan scheme.")
            Return False
        End If
        'Pinkal (04-Aug-2023) -- End
        

        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        If objDoOperation Is Nothing Then
            objDataOperation = New clsDataOperation
            objDataOperation.BindTransaction()
        Else
            objDataOperation = objDoOperation
        End If

        Try
            objDataOperation.ClearParameters()
            objDataOperation.AddParameter("@levelunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintLevelunkid.ToString)
            objDataOperation.AddParameter("@roleunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintRoleunkid.ToString)
            objDataOperation.AddParameter("@loanschemeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintLoanschemeunkid.ToString)
            objDataOperation.AddParameter("@isexceptional", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsExceptional)
            objDataOperation.AddParameter("@userunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintUserunkid.ToString)
            objDataOperation.AddParameter("@isvoid", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsvoid.ToString)
            objDataOperation.AddParameter("@voiduserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintVoiduserunkid.ToString)
            If mdtVoiddatetime <> Nothing Then
                objDataOperation.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, mdtVoiddatetime.ToString)
            Else
                objDataOperation.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, DBNull.Value)
            End If

            objDataOperation.AddParameter("@voidreason", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrVoidreason.ToString)

            'Pinkal (04-Aug-2023) -- Start
            '(A1X-1158) NMB - Allow to configure up to 5 disbursement tranches for Mortgage loans on the loan approval page.
            objDataOperation.AddParameter("@isloantranche", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsLoanTranche)
            'Pinkal (04-Aug-2023) -- End


            strQ = "INSERT INTO lnloanscheme_role_mapping ( " & _
                      "  levelunkid " & _
                      ", roleunkid " & _
                      ", loanschemeunkid " & _
                      ", isexceptional " & _
                      ", userunkid " & _
                      ", isvoid " & _
                      ", voiduserunkid " & _
                      ", voiddatetime " & _
                      ", voidreason" & _
                      ", isloantranche " & _
                    ") VALUES (" & _
                      "  @levelunkid " & _
                      ", @roleunkid " & _
                      ", @loanschemeunkid " & _
                      ", @isexceptional " & _
                      ", @userunkid " & _
                      ", @isvoid " & _
                      ", @voiduserunkid " & _
                      ", @voiddatetime " & _
                      ", @voidreason" & _
                      ", @isloantranche " & _
                    "); SELECT @@identity"

            'Pinkal (04-Aug-2023) -- (A1X-1158) NMB - Allow to configure up to 5 disbursement tranches for Mortgage loans on the loan approval page.[@isloantranche]

            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            mintMappingunkid = dsList.Tables(0).Rows(0).Item(0)

            If InsertAuditTrailLoanSchemeMapping(objDataOperation, enAuditType.ADD) = False Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            If objDoOperation Is Nothing Then objDataOperation.ReleaseTransaction(True)

            Return True
        Catch ex As Exception
            If objDoOperation Is Nothing Then objDataOperation.ReleaseTransaction(False)
            Throw New Exception(ex.Message & "; Procedure Name: Insert; Module Name: " & mstrModuleName)
            Return False
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            If objDoOperation Is Nothing Then objDataOperation = Nothing
        End Try
    End Function

    ''' <summary>
    ''' Modify By: Pinkal
    ''' </summary>
    ''' <returns>Boolean</returns>
    ''' <purpose> Update Database Table (lnloanscheme_role_mapping) </purpose>
    Public Function Update(Optional ByVal objDoOperation As clsDataOperation = Nothing) As Boolean

        'Pinkal (04-Aug-2023) -- Start
        '(A1X-1158) NMB - Allow to configure up to 5 disbursement tranches for Mortgage loans on the loan approval page.
        'If isExist(mintRoleunkid, mintLoanschemeunkid, mintLevelunkid, mblnIsExceptional, mintMappingunkid) Then
        '    mstrMessage = Language.getMessage(mstrModuleName, 1, "This Loan Scheme already exists for this role and level. Please mapped different loan scheme.")
        '    Return False
        'End If

        If isExist(mintRoleunkid, mintLoanschemeunkid, mintLevelunkid, mblnIsExceptional, mblnIsLoanTranche, mintMappingunkid) Then
            mstrMessage = Language.getMessage(mstrModuleName, 1, "This Loan Scheme already exists for this role and level. Please mapped different loan scheme.")
            Return False
        End If
        'Pinkal (04-Aug-2023) -- End
        

        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        If objDoOperation Is Nothing Then
            objDataOperation = New clsDataOperation
            objDataOperation.BindTransaction()
        Else
            objDataOperation = objDoOperation
        End If


        Try
            objDataOperation.ClearParameters()
            objDataOperation.AddParameter("@mappingunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintMappingunkid.ToString)
            objDataOperation.AddParameter("@levelunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintLevelunkid.ToString)
            objDataOperation.AddParameter("@roleunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintRoleunkid.ToString)
            objDataOperation.AddParameter("@loanschemeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintLoanschemeunkid.ToString)
            objDataOperation.AddParameter("@isexceptional", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsExceptional)
            objDataOperation.AddParameter("@userunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintUserunkid.ToString)
            objDataOperation.AddParameter("@isvoid", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsvoid.ToString)
            objDataOperation.AddParameter("@voiduserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintVoiduserunkid.ToString)
            If mdtVoiddatetime <> Nothing Then
                objDataOperation.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, mdtVoiddatetime.ToString)
            Else
                objDataOperation.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, DBNull.Value)
            End If
            objDataOperation.AddParameter("@voidreason", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrVoidreason.ToString)

            'Pinkal (04-Aug-2023) -- Start
            '(A1X-1158) NMB - Allow to configure up to 5 disbursement tranches for Mortgage loans on the loan approval page.
            objDataOperation.AddParameter("@isloantranche", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsLoanTranche)
            'Pinkal (04-Aug-2023) -- End



            strQ = " UPDATE lnloanscheme_role_mapping SET " & _
                      "  levelunkid = @levelunkid" & _
                      ", roleunkid = @roleunkid" & _
                      ", loanschemeunkid = @loanschemeunkid" & _
                      ", isexceptional = @isexceptional " & _
                      ", userunkid = @userunkid" & _
                      ", isvoid = @isvoid" & _
                      ", voiduserunkid = @voiduserunkid" & _
                      ", voiddatetime = @voiddatetime" & _
                      ", voidreason = @voidreason " & _
                      ", isloantranche = @isloantranche " & _
                      " WHERE mappingunkid = @mappingunkid "

            'Pinkal (04-Aug-2023) -- (A1X-1158) NMB - Allow to configure up to 5 disbursement tranches for Mortgage loans on the loan approval page.[", isloantranche = @isloantranche " & _]

            Call objDataOperation.ExecNonQuery(strQ)

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            If InsertAuditTrailLoanSchemeMapping(objDataOperation, enAuditType.EDIT) = False Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            If objDoOperation Is Nothing Then objDataOperation.ReleaseTransaction(True)

            Return True
        Catch ex As Exception
            If objDoOperation Is Nothing Then objDataOperation.ReleaseTransaction(False)
            Throw New Exception(ex.Message & "; Procedure Name: Update; Module Name: " & mstrModuleName)
            Return False
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            If objDoOperation Is Nothing Then objDataOperation = Nothing
        End Try
    End Function

    ''' <summary>
    ''' Modify By: Pinkal
    ''' </summary>
    ''' <returns>Boolean</returns>
    ''' <purpose> Delete Database Table (lnloanscheme_role_mapping) </purpose>
    Public Function Delete(ByVal intUnkid As Integer) As Boolean
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        Dim objDataOperation As New clsDataOperation
        objDataOperation.BindTransaction()

        Try
            strQ = " UPDATE  lnloanscheme_role_mapping SET isvoid = @isvoid,voiduserunkid  = @voiduserunkid,voiddatetime = getdate(),voidreason = @voidreason " & _
                      " WHERE mappingunkid = @mappingunkid "

            objDataOperation.ClearParameters()
            objDataOperation.AddParameter("@mappingunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intUnkid)
            objDataOperation.AddParameter("@isvoid", SqlDbType.Bit, eZeeDataType.BIT_SIZE, True)
            objDataOperation.AddParameter("@voiduserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintVoiduserunkid)
            objDataOperation.AddParameter("@voidreason", SqlDbType.NVarChar, eZeeDataType.DESC_SIZE, mstrVoidreason)
            Call objDataOperation.ExecNonQuery(strQ)

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            _Mappingunkid(objDataOperation) = intUnkid

            If InsertAuditTrailLoanSchemeMapping(objDataOperation, enAuditType.DELETE) = False Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            objDataOperation.ReleaseTransaction(True)

            Return True
        Catch ex As Exception
            objDataOperation.ReleaseTransaction(False)
            Throw New Exception(ex.Message & "; Procedure Name: Delete; Module Name: " & mstrModuleName)
            Return False
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
    End Function

    ''' <summary>
    ''' Modify By: Pinkal
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>
    Public Function isUsed(ByVal intUnkid As Integer) As Boolean
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        Dim objDataOperation As New clsDataOperation

        Try
            strQ = " SELECT ISNULL(processpendingloanunkid,0) AS processpendingloanunkid " & _
                      " FROM lnloan_process_pending_loan " & _
                      " WHERE isvoid = 0 " & _
                      " AND processpendingloanunkid in ( " & _
                      "        SELECT processpendingloanunkid " & _
                      "         FROM lnroleloanapproval_process_tran " & _
                      "         WHERE isvoid = 0 and mappingunkid = @mappingunkid AND statusunkid = @statusunkid " & _
                      " ) AND loan_statusunkid = @statusunkid "

            objDataOperation.ClearParameters()
            objDataOperation.AddParameter("@mappingunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intUnkid)
            objDataOperation.AddParameter("@statusunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, enLoanApplicationStatus.PENDING)
            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            Return dsList.tables(0).rows.count > 0
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: isUsed; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
    End Function

    ''' <summary>
    ''' Modify By: Pinkal
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>
    Public Function isExist(ByVal xRoleId As Integer, ByVal xLoanSchemeId As Integer, ByVal xApproverLevelId As Integer, ByVal xIsExceptional As Boolean, ByVal xIsLoantranche As Boolean, Optional ByVal intUnkid As Integer = -1) As Boolean
        'Pinkal (04-Aug-2023) -- (A1X-1158) NMB - Allow to configure up to 5 disbursement tranches for Mortgage loans on the loan approval page.[Public Function isExist(ByVal xRoleId As Integer, ByVal xLoanSchemeId As Integer, ByVal xApproverLevelId As Integer, ByVal xIsExceptional As Boolean, Optional ByVal intUnkid As Integer = -1) As Boolean]

        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        objDataOperation = New clsDataOperation

        Try
            strQ = "SELECT " & _
                      "  mappingunkid " & _
                      ", levelunkid " & _
                      ", roleunkid " & _
                      ", loanschemeunkid " & _
                      ", isexceptional " & _
                      ", userunkid " & _
                      ", isvoid " & _
                      ", voiduserunkid " & _
                      ", voiddatetime " & _
                      ", voidreason " & _
                      ", ISNULL(isloantranche,0) AS isloantranche " & _
                      " FROM lnloanscheme_role_mapping " & _
                      " WHERE isvoid = 0 AND roleunkid = @roleunkid " & _
                      " AND loanschemeunkid = @loanschemeunkid " & _
                      " AND isexceptional = @isexceptional " & _
                      " AND isloantranche = @isloantranche "

            'Pinkal (04-Aug-2023) -- (A1X-1158) NMB - Allow to configure up to 5 disbursement tranches for Mortgage loans on the loan approval page.[ " AND isloantranche = @isloantranche "]

            If intUnkid > 0 Then
                strQ &= " AND mappingunkid <> @mappingunkid"
            End If

            objDataOperation.AddParameter("@roleunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, xRoleId)
            objDataOperation.AddParameter("@loanschemeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, xLoanSchemeId)
            objDataOperation.AddParameter("@levelunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, xApproverLevelId)
            objDataOperation.AddParameter("@isexceptional", SqlDbType.Bit, eZeeDataType.BIT_SIZE, xIsExceptional)
            objDataOperation.AddParameter("@mappingunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intUnkid)

            'Pinkal (04-Aug-2023) -- Start
            '(A1X-1158) NMB - Allow to configure up to 5 disbursement tranches for Mortgage loans on the loan approval page.
            objDataOperation.AddParameter("@isloantranche", SqlDbType.Bit, eZeeDataType.BIT_SIZE, xIsLoantranche)
            'Pinkal (04-Aug-2023) -- End

            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            Return dsList.Tables(0).Rows.Count > 0
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: isExist; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
    End Function

    ''' <summary>
    ''' Modify By: Pinkal Jariwala
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>
    ''' 
    Public Function InsertAuditTrailLoanSchemeMapping(ByVal objDataOperation As clsDataOperation, ByVal AuditType As Integer) As Boolean
        Dim strQ As String = ""
        Dim exForce As Exception
        Try
            strQ = "INSERT INTO atlnloanscheme_role_mapping ( " & _
                      "  mappingunkid " & _
                      ", levelunkid " & _
                      ", roleunkid " & _
                      ", loanschemeunkid " & _
                      ", isexceptional " & _
                      ", audittype " & _
                      ", audituserunkid " & _
                      ", auditdatetime " & _
                      ", ip" & _
                      ", hostname" & _
                      ", form_name " & _
                      ", isweb " & _
                      " ) VALUES (" & _
                      "  @mappingunkid " & _
                      ", @levelunkid " & _
                      ", @roleunkid " & _
                      ", @loanschemeunkid " & _
                      ", @isexceptional " & _
                      ", @audittype " & _
                      ", @audituserunkid " & _
                      ", @auditdatetime " & _
                      ", @ip" & _
                      ", @hostname" & _
                      ", @form_name " & _
                      ", @isweb " & _
                      ") "

            objDataOperation.ClearParameters()
            objDataOperation.AddParameter("@mappingunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintMappingunkid)
            objDataOperation.AddParameter("@levelunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintLevelunkid.ToString)
            objDataOperation.AddParameter("@roleunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintRoleunkid.ToString)
            objDataOperation.AddParameter("@loanschemeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintLoanschemeunkid.ToString)
            objDataOperation.AddParameter("@isexceptional", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsExceptional)
            objDataOperation.AddParameter("@audittype", SqlDbType.Int, eZeeDataType.INT_SIZE, AuditType.ToString)
            objDataOperation.AddParameter("@audituserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintAuditUserId)
            objDataOperation.AddParameter("@auditdatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, mdtAuditDatetime)
            objDataOperation.AddParameter("@ip", SqlDbType.NVarChar, eZeeDataType.IP_SIZE, mstrClientIP)
            objDataOperation.AddParameter("@hostname", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrHostName)
            objDataOperation.AddParameter("@form_name", SqlDbType.NVarChar, 500, mstrFormName)
            objDataOperation.AddParameter("@isweb", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsFromWeb)

            'Pinkal (04-Aug-2023) -- Start
            '(A1X-1158) NMB - Allow to configure up to 5 disbursement tranches for Mortgage loans on the loan approval page.
            objDataOperation.AddParameter("@isloantranche", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsLoanTranche)
            'Pinkal (04-Aug-2023) -- End

            objDataOperation.ExecNonQuery(strQ)

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            Return True
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: InsertAuditTrailLoanSchemeMapping; Module Name: " & mstrModuleName)
            Return False
        Finally
        End Try
    End Function

    ''' <summary>
    ''' Modify By: Pinkal Jariwala
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>
    ''' 
    Public Function GetNextApprovers(ByVal strDatabaseName As String _
                                                                  , ByVal xCompanyId As Integer _
                                                                  , ByVal xYearId As Integer _
                                                                  , ByVal strUserAccessMode As String _
                                                                  , ByVal xPrivilegeId As Integer _
                                                                  , ByVal xEmployeeAsOnDate As Date _
                                                                  , ByVal intUserId As Integer _
                                                                  , ByVal intEmployeeID As Integer _
                                                                 , ByVal xLoanSchemeId As Integer _
                                                                  , ByVal mblnIsExceptional As Boolean _
                                                                  , ByVal mdecOutstandingPrincipal As Decimal _
                                                                  , ByVal mblnIsLoanTranche As Boolean _
                                                                  , Optional ByVal objDataOpr As clsDataOperation = Nothing _
                                                                  , Optional ByVal mstrFilterString As String = "" _
                                                                  ) As DataTable

        'Pinkal (04-Aug-2023) -- (A1X-1158) NMB - Allow to configure up to 5 disbursement tranches for Mortgage loans on the loan approval page.[ByVal mblnIsLoanTranche As Boolean]

        Dim StrQ As String = String.Empty
        Dim dtList As DataTable = Nothing
        Dim objDataOperation As New clsDataOperation
        If objDataOpr IsNot Nothing Then
            objDataOperation = objDataOpr
        Else
            objDataOperation = New clsDataOperation
        End If
        objDataOperation.ClearParameters()
        Try
            Dim strFilter As String = String.Empty
            Dim strJoin As String = String.Empty
            Dim strSelect As String = String.Empty
            Dim strAccessJoin As String = String.Empty
            Dim strOuterJoin As String = String.Empty
            Dim strFinalString As String = ""

            Dim xDateJoinQry, xDateFilterQry, xAdvanceJoinQry As String
            xDateJoinQry = "" : xDateFilterQry = "" : xAdvanceJoinQry = ""
            Call GetDatesFilterString(xDateJoinQry, xDateFilterQry, xEmployeeAsOnDate, xEmployeeAsOnDate, , , strDatabaseName)
            Call GetAdvanceFilterQry(xAdvanceJoinQry, xEmployeeAsOnDate, strDatabaseName)

            StrQ = "IF OBJECT_ID('tempdb..#USR') IS NOT NULL " & _
                 "DROP TABLE #USR "
            StrQ &= "SELECT " & _
                    "* " & _
                    "INTO #USR " & _
                    "FROM " & _
                    "( "

            If strUserAccessMode.Trim.Length <= 0 Then strUserAccessMode = CInt(enAllocation.DEPARTMENT).ToString()
            Dim strvalues() As String = strUserAccessMode.Split(CChar(","))
            For index As Integer = 0 To strvalues.Length - 1
                Dim xStrJoinColName As String = ""
                Dim xIntAllocId As Integer = 0
                Select Case CInt(strvalues(index))
                    Case enAllocation.BRANCH
                        xStrJoinColName = "stationunkid"
                        xIntAllocId = CInt(enAllocation.BRANCH)
                    Case enAllocation.DEPARTMENT_GROUP
                        xStrJoinColName = "deptgroupunkid"
                        xIntAllocId = CInt(enAllocation.DEPARTMENT_GROUP)
                    Case enAllocation.DEPARTMENT
                        xStrJoinColName = "departmentunkid"
                        xIntAllocId = CInt(enAllocation.DEPARTMENT)
                    Case enAllocation.SECTION_GROUP
                        xStrJoinColName = "sectiongroupunkid"
                        xIntAllocId = CInt(enAllocation.SECTION_GROUP)
                    Case enAllocation.SECTION
                        xStrJoinColName = "sectionunkid"
                        xIntAllocId = CInt(enAllocation.SECTION)
                    Case enAllocation.UNIT_GROUP
                        xStrJoinColName = "unitgroupunkid"
                        xIntAllocId = CInt(enAllocation.UNIT_GROUP)
                    Case enAllocation.UNIT
                        xStrJoinColName = "unitunkid"
                        xIntAllocId = CInt(enAllocation.UNIT)
                    Case enAllocation.TEAM
                        xStrJoinColName = "teamunkid"
                        xIntAllocId = CInt(enAllocation.TEAM)
                    Case enAllocation.JOB_GROUP
                        xStrJoinColName = "jobgroupunkid"
                        xIntAllocId = CInt(enAllocation.JOB_GROUP)
                    Case enAllocation.JOBS
                        xStrJoinColName = "jobunkid"
                        xIntAllocId = CInt(enAllocation.JOBS)
                    Case enAllocation.CLASS_GROUP
                        xStrJoinColName = "classgroupunkid"
                        xIntAllocId = CInt(enAllocation.CLASS_GROUP)
                    Case enAllocation.CLASSES
                        xStrJoinColName = "classunkid"
                        xIntAllocId = CInt(enAllocation.CLASSES)
                End Select
                StrQ &= "SELECT DISTINCT " & _
                        "    B" & index.ToString() & ".userunkid AS mapuserunkid " & _
                        "   ,A.employeeunkid " & _
                        "FROM " & _
                        "( " & _
                        "   SELECT " & _
                        "        AEM.employeeunkid " & _
                        "       ,ISNULL(T.departmentunkid, 0) AS departmentunkid " & _
                        "       ,ISNULL(J.jobunkid, 0) AS jobunkid " & _
                        "       ,ISNULL(T.classgroupunkid, 0) AS classgroupunkid " & _
                        "       ,ISNULL(T.classunkid, 0) AS classunkid " & _
                        "       ,ISNULL(T.stationunkid, 0) AS stationunkid " & _
                        "       ,ISNULL(T.deptgroupunkid, 0) AS deptgroupunkid " & _
                        "       ,ISNULL(T.sectiongroupunkid, 0) AS sectiongroupunkid " & _
                        "       ,ISNULL(T.sectionunkid, 0) AS sectionunkid " & _
                        "       ,ISNULL(T.unitgroupunkid, 0) AS unitgroupunkid " & _
                        "       ,ISNULL(T.unitunkid, 0) AS unitunkid " & _
                        "       ,ISNULL(T.teamunkid, 0) AS teamunkid " & _
                        "       ,ISNULL(J.jobgroupunkid, 0) AS jobgroupunkid " & _
                        "   FROM " & strDatabaseName & "..hremployee_master AS AEM " & _
                        "   LEFT JOIN " & _
                        "   ( " & _
                        "       SELECT " & _
                        "            stationunkid " & _
                        "           ,deptgroupunkid " & _
                        "           ,departmentunkid " & _
                        "           ,sectiongroupunkid " & _
                        "           ,sectionunkid " & _
                        "           ,unitgroupunkid " & _
                        "           ,unitunkid " & _
                        "           ,teamunkid " & _
                        "           ,classgroupunkid " & _
                        "           ,classunkid " & _
                        "           ,employeeunkid " & _
                        "           ,ROW_NUMBER() OVER (PARTITION BY employeeunkid ORDER BY effectivedate DESC) AS Rno " & _
                        "       FROM " & strDatabaseName & "..hremployee_transfer_tran " & _
                        "       WHERE isvoid = 0 " & _
                        "       AND CONVERT(CHAR(8), effectivedate, 112) <= '" & eZeeDate.convertDate(xEmployeeAsOnDate) & "' " & _
                        "   ) AS T ON T.employeeunkid = AEM.employeeunkid " & _
                        "   AND T.Rno = 1 " & _
                        "   LEFT JOIN " & _
                        "   ( " & _
                        "       SELECT " & _
                        "            jobgroupunkid " & _
                        "           ,jobunkid " & _
                        "           ,employeeunkid " & _
                        "           ,ROW_NUMBER() OVER (PARTITION BY employeeunkid ORDER BY effectivedate DESC) AS Rno " & _
                        "       FROM " & strDatabaseName & "..hremployee_categorization_tran " & _
                        "       WHERE isvoid = 0 " & _
                        "       AND CONVERT(CHAR(8), effectivedate, 112) <= '" & eZeeDate.convertDate(xEmployeeAsOnDate) & "' " & _
                        "   ) AS J ON J.employeeunkid = AEM.employeeunkid " & _
                        "   AND J.Rno = 1 " & _
                        ") AS A " & _
                        "JOIN " & _
                        "( " & _
                        "   SELECT " & _
                        "        UPM.userunkid " & _
                        "       ,UPT.allocationunkid " & _
                        "   FROM hrmsConfiguration..cfuseraccess_privilege_master AS UPM " & _
                        "       JOIN hrmsConfiguration..cfuseraccess_privilege_tran UPT ON UPM.useraccessprivilegeunkid = UPT.useraccessprivilegeunkid " & _
                        "   WHERE UPM.companyunkid = " & xCompanyId & " AND UPM.yearunkid = @Y AND UPM.referenceunkid IN (" & xIntAllocId & ") " & _
                        ") AS B" & index.ToString() & " ON A." & xStrJoinColName & " = B" & index.ToString() & ".allocationunkid "

                StrQ &= " AND A.employeeunkid = " & intEmployeeID & " "
                If index < strvalues.Length - 1 Then
                    StrQ &= " INTERSECT "
                End If
            Next

            StrQ &= ") AS Fl "

            StrQ &= "SELECT " & _
                       "    mapuserunkid " & _
                       ",   employeeunkid " & _
                       ",   roleunkid " & _
                       ",   levelunkid " & _
                       ",   priority " & _
                       ",   mappingunkid " & _
                         ",   minrange " & _
                         ",   maxrange " & _
                         "  FROM ( " & _
                         "              SELECT " & _
                        "#USR.mapuserunkid " & _
                       ",#USR.employeeunkid " & _
                       ",UM.roleunkid " & _
                       ",TAM.levelunkid " & _
                       ",TLM.priority " & _
                       ",TAM.mappingunkid  " & _
                         "                   ,DENSE_RANK() OVER (PARTITION BY #USR.mapuserunkid ORDER BY TAM.roleunkid ASC, TLM.priority ASC, TAM.levelunkid ASC, TAM.mappingunkid ASC) AS rowno " & _
                         "                   ,TLM.minrange " & _
                         "                   ,TLM.maxrange " & _
                         "              FROM #USR " & _
                    " JOIN hrmsConfiguration..cfuser_master UM ON UM.userunkid = #USR.mapuserunkid " & _
                         "             AND UM.employeeunkid <> " & intEmployeeID & _
                         "              JOIN lnloanscheme_role_mapping TAM  ON TAM.roleunkid = UM.roleunkid  AND TAM.isvoid = 0  AND TAM.loanschemeunkid = @loanschemeunkid AND TAM.isexceptional = @isexceptional AND TAM.isloantranche = @isloantranche " & _
                         "             JOIN lnapproverlevel_master TLM ON TLM.lnlevelunkid = TAM.levelunkid AND @outstandingprincipal BETWEEN TLM.minrange AND TLM.maxrange AND TLM.isactive = 1 " & _
                         "              JOIN " & _
                   "    ( " & _
                   "        SELECT DISTINCT " & _
                   "             cfuser_master.userunkid " & _
                   "            ,cfuser_master.firstname + ' ' +cfuser_master.lastname as username " & _
                   "            ,cfuser_master.email " & _
                   "            ,hrmsConfiguration..cfuser_master.employeeunkid AS uempid " & _
                   "            ,hrmsConfiguration..cfuser_master.companyunkid AS ecompid " & _
                   "        FROM hrmsConfiguration..cfuser_master " & _
                   "            JOIN hrmsConfiguration..cfcompanyaccess_privilege ON cfcompanyaccess_privilege.userunkid = cfuser_master.userunkid " & _
                   "            JOIN hrmsConfiguration..cfuser_privilege ON cfuser_master.userunkid = cfuser_privilege.userunkid " & _
                         "                   WHERE cfuser_master.isactive = 1  AND yearunkid = @Y AND privilegeunkid = @P  " & _
                         "              ) AS Fn ON #USR.mapuserunkid = Fn.userunkid  " & _
                         " LEFT JOIN hrmsConfiguration..cfuser_master on cfuser_master.userunkid = #USR.mapuserunkid " & _
                    " LEFT JOIN hremployee_master UserEmp ON UserEmp.employeeunkid = hrmsConfiguration..cfuser_master.employeeunkid "


            'Pinkal (04-Aug-2023) -- (A1X-1158) NMB - Allow to configure up to 5 disbursement tranches for Mortgage loans on the loan approval page.[AND TAM.isloantranche = @isloantranche]

            If xDateJoinQry.Trim.Length > 0 Then
                StrQ &= xDateJoinQry.Replace("hremployee_master", "UserEmp")
            End If

            If xAdvanceJoinQry.Trim.Length > 0 Then
                StrQ &= xAdvanceJoinQry.Replace("hremployee_master", "UserEmp")
            End If

            StrQ &= " WHERE #USR.employeeunkid = " & intEmployeeID & " " & " AND UM.roleunkid > 0 "

            If xDateFilterQry.Trim.Length > 0 Then
                StrQ &= xDateFilterQry.Replace("hremployee_master", "UserEmp")
            End If

            StrQ &= " ) AS Y " & _
                         " WHERE Y.rowno = 1 "

            'Pinkal (27-Oct-2022) -- Start
            'NMB Loan Module Enhancement.

            If mstrFilterString.Trim.Length > 0 Then
                StrQ &= " AND " & mstrFilterString.Trim
            End If

            StrQ &= " ORDER BY priority "

            'Pinkal (27-Oct-2022) -- End

            StrQ &= " DROP TABLE #USR "

            'Pinkal (12-Oct-2022) -- Start
            'NMB Loan Module Enhancement.
            objDataOperation.AddParameter("@outstandingprincipal", SqlDbType.Decimal, eZeeDataType.DECIMAL_SIZE, mdecOutstandingPrincipal)
            'Pinkal (12-Oct-2022) -- End

            objDataOperation.AddParameter("@loanschemeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, xLoanSchemeId)
            objDataOperation.AddParameter("@isexceptional", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsExceptional)
            objDataOperation.AddParameter("@C", SqlDbType.Int, eZeeDataType.INT_SIZE, xCompanyId)
            objDataOperation.AddParameter("@Y", SqlDbType.Int, eZeeDataType.INT_SIZE, xYearId)
            objDataOperation.AddParameter("@P", SqlDbType.Int, eZeeDataType.INT_SIZE, xPrivilegeId)

            'Pinkal (04-Aug-2023) -- Start
            '(A1X-1158) NMB - Allow to configure up to 5 disbursement tranches for Mortgage loans on the loan approval page.
            objDataOperation.AddParameter("@isloantranche", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsLoanTranche)
            'Pinkal (04-Aug-2023) -- End

            Dim dsList As DataSet = objDataOperation.ExecQuery(StrQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                Throw New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
            End If

            dtList = dsList.Tables("List").Copy()

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: GetNextApprovers; Module Name: " & mstrModuleName)
        Finally
            If objDataOpr Is Nothing Then objDataOperation = Nothing
        End Try
        Return dtList
    End Function

    '<Language> This Auto Generated Text Please Do Not Modify it.
#Region " Language & UI Settings "
    Public Shared Sub SetMessages()
        Try
            Language.setMessage(mstrModuleName, 1, "This Loan Scheme already exists for this role and level. Please mapped different loan scheme.")
            Language.setMessage(mstrModuleName, 2, "Yes")
            Language.setMessage(mstrModuleName, 3, "No")

        Catch Ex As Exception
            DisplayError.Show("-1", Ex.Message, "SetMessages", mstrModuleName)
        End Try
    End Sub
#End Region 'Language & UI Settings
    '</Language>
End Class