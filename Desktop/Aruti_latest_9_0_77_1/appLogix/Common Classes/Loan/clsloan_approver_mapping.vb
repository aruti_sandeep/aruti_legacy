﻿'************************************************************************************************************************************
'Class Name : clsloan_approver_mapping.vb
'Purpose    :
'Date       :15/06/2011
'Written By :Pinkal
'Modified   :
'************************************************************************************************************************************

Imports eZeeCommonLib
''' <summary>
''' Purpose: 
''' Developer: Pinkal
''' </summary>
Public Class clsloan_approver_mapping
    Private Const mstrModuleName = "clsloan_approver_mapping"
    Dim objDataOperation As clsDataOperation
    Dim mstrMessage As String = ""

#Region " Private variables "
    Private mintApprovermappingunkid As Integer
    Private mintApprovertranunkid As Integer
    Private mintUserunkid As Integer
#End Region

#Region " Properties "

'S.SANDEEP [28-May-2018] -- START 
'ISSUE/ENHANCEMENT : {Audit Trails} 
Private mstrFormName As String = String.Empty
Public WriteOnly Property _FormName() As String 
Set(ByVal value As String) 
mstrFormName = value 
End Set 
End Property 
 
Private mstrClientIP As String = ""
Public WriteOnly Property _ClientIP() As String 
Set(ByVal value As String) 
mstrClientIP = value 
End Set 
End Property 
 
Private mstrHostName As String = ""
Public WriteOnly Property _HostName() As String 
Set(ByVal value As String) 
mstrHostName = value 
End Set 
End Property 
 
Private mblnIsWeb As Boolean = False
Public WriteOnly Property _FromWeb() As Boolean 
Set(ByVal value As Boolean) 
mblnIsWeb = value 
End Set 
End Property 
 
Private mintAuditUserId As Integer = 0
Public WriteOnly Property _AuditUserId() As Boolean 
Set(ByVal value As Integer) 
mintAuditUserId = value 
End Set 
End Property 
 
Private mdtAuditDate As DateTime = Now
Public WriteOnly Property _AuditDate() As DateTime 
Set(ByVal value As DateTime) 
mdtAuditDate = value 
End Set 
End Property 
 
Private xDataOpr As clsDataOperation = Nothing
Public Property _xDataOpr() As clsDataOperation 
Get 
Return xDataOpr 
End Get 
Set(ByVal value As clsDataOperation) 
xDataOpr = value 
End Set 
End Property 
 
Private mintLoginEmployeeunkid As Integer = 0
Public Property _LoginEmployeeunkid() As Integer 
Get 
Return mintLoginEmployeeunkid 
End Get 
Set(ByVal value As Integer) 
mintLoginEmployeeunkid = value 
End Set 
End Property 
 
Private mintVoidlogingemployeeunkid As Integer=0
Public Property _Voidloginemployeeunkid() As Integer 
Get 
Return mintVoidlogingemployeeunkid 
End Get 
Set(ByVal value As Integer) 
mintVoidlogingemployeeunkid = value 
End Set 
End Property 
'S.SANDEEP [28-May-2018] -- END 

    ''' <summary>
    ''' Purpose: Get Message from Class 
    ''' Modify By: Pinkal
    ''' </summary>
    Public ReadOnly Property _Message() As String
        Get
            Return mstrMessage
        End Get
    End Property

    ''' <summary>
    ''' Purpose: Get or Set approvermappingunkid
    ''' Modify By: Pinkal
    ''' </summary>
    Public Property _Approvermappingunkid() As Integer
        Get
            Return mintApprovermappingunkid
        End Get
        Set(ByVal value As Integer)
            mintApprovermappingunkid = value
            Call GetData()
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set approvertranunkid
    ''' Modify By: Pinkal
    ''' </summary>
    Public Property _Approvertranunkid() As Integer
        Get
            Return mintApprovertranunkid
        End Get
        Set(ByVal value As Integer)
            mintApprovertranunkid = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set userunkid
    ''' Modify By: Pinkal
    ''' </summary>
    Public Property _Userunkid() As Integer
        Get
            Return mintUserunkid
        End Get
        Set(ByVal value As Integer)
            mintUserunkid = value
        End Set
    End Property

#End Region

    ''' <summary>
    ''' Modify By: Pinkal
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>
    Public Sub GetData()
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        objDataOperation = New clsDataOperation

        Try
            strQ = "SELECT " & _
              "  approvermappingunkid " & _
              ", approvertranunkid " & _
              ", userunkid " & _
             "FROM lnloan_approver_mapping " & _
             "WHERE approvermappingunkid = @approvermappingunkid "

            objDataOperation.AddParameter("@approvermappingunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintApprovermappingunkid.ToString)

            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            For Each dtRow As DataRow In dsList.Tables(0).Rows
                mintApprovermappingunkid = CInt(dtRow.Item("approvermappingunkid"))
                mintApprovertranunkid = CInt(dtRow.Item("approvertranunkid"))
                mintUserunkid = CInt(dtRow.Item("userunkid"))
                Exit For
            Next
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: GetData; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
    End Sub

    ''' <summary>
    ''' Modify By: Pinkal
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>
    Public Function GetList(ByVal strTableName As String, Optional ByVal intUserunkid As Integer = -1) As DataSet
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        objDataOperation = New clsDataOperation

        Try

            'Anjan (24 Jun 2011)-Start
            'Issue : According to privilege that lower level user should not see superior level employees.
            strQ = "SELECT  approvermappingunkid ," & _
                      " lnloan_approver_mapping.approvertranunkid , " & _
                      " lnloan_approver_tran.approverunkid, " & _
                      " ISNULL(hremployee_master.firstname,'')+' '+ISNULL(hremployee_master.othername,'') +' '+ ISNULL(hremployee_master.surname,'')  'Approver', " & _
                      " lnloan_approver_mapping.userunkid, " & _
                      " hrmsConfiguration..cfuser_master.username " & _
                      " FROM lnloan_approver_mapping " & _
                      " LEFT JOIN lnloan_approver_tran ON lnloan_approver_tran.approvertranunkid= lnloan_approver_mapping.approvertranunkid " & _
                      " LEFT JOIN hremployee_master ON hremployee_master.employeeunkid = lnloan_approver_tran.approverunkid " & _
                      " LEFT JOIN hrmsConfiguration..cfuser_master ON hrmsConfiguration..cfuser_master.userunkid = lnloan_approver_mapping.userunkid " & _
                      " WHERE 1=1 " 'Anjan (09 Aug 2011)-Start
            'strQ = "SELECT  approvermappingunkid ," & _
            '          " lnloan_approver_mapping.approvertranunkid , " & _
            '          " lnloan_approver_tran.approverunkid, " & _
            '          " ISNULL(hremployee_master.firstname,'')+' '+ISNULL(hremployee_master.othername,'') +' '+ ISNULL(hremployee_master.surname,'')  'Approver', " & _
            '          " lnloan_approver_mapping.userunkid, " & _
            '          " hrmsConfiguration..cfuser_master.username " & _
            '          " FROM lnloan_approver_mapping " & _
            '          " LEFT JOIN lnloan_approver_tran ON lnloan_approver_tran.approvertranunkid= lnloan_approver_mapping.approvertranunkid " & _
            '          " LEFT JOIN hremployee_master ON hremployee_master.employeeunkid = lnloan_approver_tran.approverunkid " & _
            '          " LEFT JOIN hrmsConfiguration..cfuser_master ON hrmsConfiguration..cfuser_master.userunkid = lnloan_approver_mapping.userunkid " & _
            '          "WHERE 1 = 1 "


            'If UserAccessLevel._AccessLevel.Length > 0 Then
            '    strQ &= "  AND hremployee_master.jobunkid IN (" & UserAccessLevel._AccessLevel & ")  "
            'End If

            'Anjan (24 Jun 2011)-End 
            objDataOperation.ClearParameters()
            'Anjan (09 Aug 2011)-Start
            'Issue : For including setting of acitve and inactive employee.
            If ConfigParameter._Object._IsIncludeInactiveEmp = False Then
                'Sohail (06 Jan 2012) -- Start
                'TRA - ENHANCEMENT
                strQ &= " AND CONVERT(CHAR(8),hremployee_master.appointeddate,112) <= @enddate " & _
                           " AND ISNULL(CONVERT(CHAR(8),hremployee_master.termination_from_date,112),@startdate) >= @startdate " & _
                           " AND ISNULL(CONVERT(CHAR(8),hremployee_master.termination_to_date,112),@startdate) >= @startdate " & _
                           " AND ISNULL(CONVERT(CHAR(8),hremployee_master.empl_enddate,112), @startdate) >= @startdate " 'S.SANDEEP [ 15 MAY 2012 empl_enddate ] -- START -- END

                objDataOperation.AddParameter("@startdate", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, ConfigParameter._Object._EmployeeAsOnDate)
                objDataOperation.AddParameter("@enddate", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, ConfigParameter._Object._EmployeeAsOnDate)
                'Sohail (06 Jan 2012) -- End
            End If
            'Anjan (09 Aug 2011)-End 

            If intUserunkid > 0 Then
                'S.SANDEEP [ 20 AUG 2011 ] -- START
                'ENHANCEMENT : LANGUAGES IMPLEMENTATION
                'strQ &= " WHERE lnloan_approver_mapping.userunkid = @userunkid"
                strQ &= " AND lnloan_approver_mapping.userunkid = @userunkid"
                'S.SANDEEP [ 20 AUG 2011 ] -- END 

                objDataOperation.AddParameter("@userunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intUserunkid)
            End If

            dsList = objDataOperation.ExecQuery(strQ, strTableName)

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: GetList; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
        Return dsList
    End Function

    'Pinkal (12-Oct-2011) -- Start
    'ENHANCEMENT : AUDIT TRAIL MAINTENANCE

    '' <summary>
    '' Modify By: Pinkal
    '' </summary>
    '' <returns>Boolean</returns>
    '' <purpose> INSERT INTO Database Table (lnloan_approver_mapping) </purpose>
   ' Public Function Insert() As Boolean

'        Dim dsList As DataSet = Nothing
'        Dim strQ As String = ""
'        Dim exForce As Exception

'        objDataOperation = New clsDataOperation

'        Try
'            objDataOperation.AddParameter("@approvertranunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintApprovertranunkid.ToString)
'            objDataOperation.AddParameter("@userunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintUserunkid.ToString)
'
'            strQ = "INSERT INTO lnloan_approver_mapping ( " & _
'              "  approvertranunkid " & _
'              ", userunkid" & _
'            ") VALUES (" & _
'              "  @approvertranunkid " & _
'              ", @userunkid" & _
'            "); SELECT @@identity"
'
'            dsList = objDataOperation.ExecQuery(strQ, "List")
'
'            If objDataOperation.ErrorMessage <> "" Then
'                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
'                Throw exForce
'            End If
'
'            mintApprovermappingunkid = dsList.Tables(0).Rows(0).Item(0)
'
'            Return True
'        Catch ex As Exception
'            Throw New Exception(ex.Message & "; Procedure Name: Insert; Module Name: " & mstrModuleName)
'            Return False
'        Finally
'            exForce = Nothing
'            If dsList IsNot Nothing Then dsList.Dispose()
''            objDataOperation = Nothing
'        End Try
'    End Function

'Pinkal (12-Oct-2011) -- Start
    'ENHANCEMENT : AUDIT TRAIL MAINTENANCE

    ''' <summary>
    ''' Modify By: Pinkal
    ''' </summary>
    ''' <returns>Boolean</returns>
    ''' <purpose> INSERT INTO Database Table (lnloan_approver_mapping) </purpose>
    Public Function Insert(ByVal dctLoanApprover As Dictionary(Of Integer, String)) As Boolean

        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        objDataOperation = New clsDataOperation
        objDataOperation.BindTransaction()

        Try

            If dctLoanApprover.Keys.Count < 0 Then Return True


            strQ = "INSERT INTO lnloan_approver_mapping ( " & _
              "  approvertranunkid " & _
              ", userunkid" & _
            ") VALUES (" & _
              "  @approvertranunkid " & _
              ", @userunkid" & _
            "); SELECT @@identity"


            For i As Integer = 0 To dctLoanApprover.Keys.Count - 1

                mintUserunkid = CInt(dctLoanApprover.Keys(i))

                Dim strApprover() As String = dctLoanApprover(mintUserunkid).ToString().Split(",")

                Dim dList As DataSet = clsCommonATLog.GetChildList(objDataOperation, "lnloan_approver_mapping", "userunkid", mintUserunkid)
                Dim dtDel() As DataRow = Nothing

                If dctLoanApprover(mintUserunkid).ToString().Trim.Length > 0 AndAlso dList.Tables(0).Rows.Count > 0 Then

                    dtDel = dList.Tables(0).Select("approvertranunkid NOT IN (" & dctLoanApprover(mintUserunkid).ToString().Trim & ") AND userunkid = " & mintUserunkid)

                    If dtDel.Length > 0 Then

                        For k As Integer = 0 To dtDel.Length - 1

                       'S.SANDEEP [28-May-2018] -- START
'ISSUE/ENHANCEMENT : {Audit Trails} 
Dim objCommonATLog AS New clsCommonATLog 
objCommonATLog._FormName = mstrFormName 
objCommonATLog._LoginEmployeeUnkid = mintLoginEmployeeunkid 
objCommonATLog._WebClientIP = mstrClientIP 
objCommonATLog._WebHostName = mstrHostName 
objCommonATLog._FromWeb = mblnIsWeb 
objCommonATLog._AuditUserId = mintAuditUserId 
objCommonATLog._CompanyUnkid = mintCompanyUnkid
objCommonATLog._AuditDate = mdtAuditDate 
'S.SANDEEP [28-May-2018] -- END

                            If objCommonATLog.Insert_AtLog(objDataOperation, 3, "lnloan_approver_mapping", "approvermappingunkid", CInt(dtDel(k)("approvermappingunkid"))) = False Then
                                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                                Throw exForce
                            End If

'S.SANDEEP [28-May-2018] -- START
'ISSUE/ENHANCEMENT : {Audit Trails} 
objCommonATLog = Nothing 
'S.SANDEEP [28-May-2018] -- END

                            'S.SANDEEP [ 11 SEP 2012 ] -- START
                            'ENHANCEMENT : TRA CHANGES
                            'strQ = "DELETE FROM lnloan_approver_mapping WHERE  userunkid = @userunkid AND approvertranunkid = @approvertranunkid"
                            Dim strQ1 As String = "DELETE FROM lnloan_approver_mapping WHERE  userunkid = @userunkid AND approvertranunkid = @approvertranunkid"
                            'S.SANDEEP [ 11 SEP 2012 ] -- END

                            objDataOperation.ClearParameters()
                            objDataOperation.AddParameter("@userunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, CInt(dtDel(k)("userunkid")))
                            objDataOperation.AddParameter("@approvertranunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, CInt(dtDel(k)("approvertranunkid")))

                            'S.SANDEEP [ 11 SEP 2012 ] -- START
                            'ENHANCEMENT : TRA CHANGES
                            'Call objDataOperation.ExecNonQuery(strQ)
                            Call objDataOperation.ExecNonQuery(strQ1)
                            'S.SANDEEP [ 11 SEP 2012 ] -- END


                            If objDataOperation.ErrorMessage <> "" Then
                                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                                Throw exForce
                            End If

                            dList.Tables(0).Rows.Remove(dtDel(k))

                        Next

                        dList.AcceptChanges()

                    End If

                End If

                If strApprover.Length > 0 Then

                    For k As Integer = 0 To strApprover.Length - 1

                        If dList IsNot Nothing AndAlso dList.Tables(0).Rows.Count > 0 Then
                            dtDel = dList.Tables(0).Select("approvertranunkid = " & CInt(strApprover.GetValue(k)))
                            If dtDel.Length > 0 Then Continue For
                        End If

                        objDataOperation.ClearParameters()
                        objDataOperation.AddParameter("@approvertranunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, strApprover.GetValue(k).ToString)
                        objDataOperation.AddParameter("@userunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintUserunkid.ToString)
                        dsList = objDataOperation.ExecQuery(strQ, "List")

                        If objDataOperation.ErrorMessage <> "" Then
                            exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                            Throw exForce
                        End If

                        mintApprovermappingunkid = dsList.Tables(0).Rows(0).Item(0)

'S.SANDEEP [28-May-2018] -- START
'ISSUE/ENHANCEMENT : {Audit Trails} 
Dim objCommonATLog AS New clsCommonATLog 
objCommonATLog._FormName = mstrFormName 
objCommonATLog._LoginEmployeeUnkid = mintLoginEmployeeunkid 
objCommonATLog._WebClientIP = mstrClientIP 
objCommonATLog._WebHostName = mstrHostName 
objCommonATLog._FromWeb = mblnIsWeb 
objCommonATLog._AuditUserId = mintAuditUserId 
objCommonATLog._CompanyUnkid = mintCompanyUnkid
objCommonATLog._AuditDate = mdtAuditDate 
'S.SANDEEP [28-May-2018] -- END

                        If objCommonATLog.Insert_AtLog(objDataOperation, 1, "lnloan_approver_mapping", "approvermappingunkid", mintApprovermappingunkid) = False Then
                            exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                            Throw exForce
                        End If

'S.SANDEEP [28-May-2018] -- START
'ISSUE/ENHANCEMENT : {Audit Trails} 
objCommonATLog = Nothing 
'S.SANDEEP [28-May-2018] -- END


                    Next

                End If

            Next

            objDataOperation.ReleaseTransaction(True)
            Return True

        Catch ex As Exception
            objDataOperation.ReleaseTransaction(False)
            Throw New Exception(ex.Message & "; Procedure Name: Insert; Module Name: " & mstrModuleName)
            Return False
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
    End Function

    'Pinkal (12-Oct-2011) -- End

    ''' <summary>
    ''' Modify By: Pinkal
    ''' </summary>
    ''' <returns>Boolean</returns>
    ''' <purpose> Update Database Table (lnloan_approver_mapping) </purpose>
    Public Function Update() As Boolean

        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        'Pinkal (12-Oct-2011) -- Start
        'ENHANCEMENT : AUDIT TRAIL MAINTENANCE
        objDataOperation = New clsDataOperation
        objDataOperation.BindTransaction()
        'Pinkal (12-Oct-2011) -- End

        Try
            objDataOperation.AddParameter("@approvermappingunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintApprovermappingunkid.ToString)
            objDataOperation.AddParameter("@approvertranunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintApprovertranunkid.ToString)
            objDataOperation.AddParameter("@userunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintUserunkid.ToString)

            strQ = "UPDATE lnloan_approver_mapping SET " & _
              "  approvertranunkid = @approvertranunkid" & _
              ", userunkid = @userunkid " & _
            "WHERE approvermappingunkid = @approvermappingunkid "

            Call objDataOperation.ExecNonQuery(strQ)

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If


'S.SANDEEP [28-May-2018] -- START
'ISSUE/ENHANCEMENT : {Audit Trails} 
Dim objCommonATLog AS New clsCommonATLog 
objCommonATLog._FormName = mstrFormName 
objCommonATLog._LoginEmployeeUnkid = mintLoginEmployeeunkid 
objCommonATLog._WebClientIP = mstrClientIP 
objCommonATLog._WebHostName = mstrHostName 
objCommonATLog._FromWeb = mblnIsWeb 
objCommonATLog._AuditUserId = mintAuditUserId 
objCommonATLog._CompanyUnkid = mintCompanyUnkid
objCommonATLog._AuditDate = mdtAuditDate 
'S.SANDEEP [28-May-2018] -- END

            If objCommonATLog.IsTableDataUpdate("atcommon_log", "lnloan_approver_mapping", mintApprovermappingunkid, "approvermappingunkid", 2) Then

                If objCommonATLog.Insert_AtLog(objDataOperation, 2, "lnloan_approver_mapping", "approvermappingunkid", mintApprovermappingunkid) = False Then
                    exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                    Throw exForce
                End If

            End If

'S.SANDEEP [28-May-2018] -- START
'ISSUE/ENHANCEMENT : {Audit Trails} 
objCommonATLog = Nothing 
'S.SANDEEP [28-May-2018] -- END


            objDataOperation.ReleaseTransaction(True)
            'Pinkal (12-Oct-2011) -- End

            Return True
        Catch ex As Exception
            'Pinkal (12-Oct-2011) -- Start
            'ENHANCEMENT : AUDIT TRAIL MAINTENANCE
            objDataOperation.ReleaseTransaction(False)
            'Pinkal (12-Oct-2011) -- End
            Throw New Exception(ex.Message & "; Procedure Name: Update; Module Name: " & mstrModuleName)
            Return False
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
    End Function

    ''' <summary>
    ''' Modify By: Pinkal
    ''' </summary>
    ''' <returns>Boolean</returns>
    ''' <purpose> Delete Database Table (lnloan_approver_mapping) </purpose>
    Public Function Delete(ByVal intUserunkid As Integer) As Boolean
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        objDataOperation = New clsDataOperation

        Try
            objDataOperation.ClearParameters()

            strQ = "DELETE FROM lnloan_approver_mapping WHERE  userunkid = @userunkid "

            objDataOperation.AddParameter("@userunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intUserunkid)

            Call objDataOperation.ExecNonQuery(strQ)

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            Return True
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: Update; Module Name: " & mstrModuleName)
            Return False
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
    End Function

    ''' <summary>
    ''' Modify By: Pinkal
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>
    Public Function isExist(ByVal intapprovertranunkid As Integer, ByVal intUserunkid As Integer) As Boolean
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        objDataOperation = New clsDataOperation

        Try
            strQ = "SELECT " & _
              "  approvermappingunkid " & _
              ", approvertranunkid " & _
              ", userunkid " & _
             "FROM lnloan_approver_mapping " & _
             "WHERE approvertranunkid = @approvertranunkid " & _
             "AND userunkid = @userunkid "


            objDataOperation.AddParameter("@approvertranunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intapprovertranunkid)
            objDataOperation.AddParameter("@userunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intUserunkid)

            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            Return dsList.Tables(0).Rows.Count > 0
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: isExist; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
    End Function

End Class