﻿'************************************************************************************************************************************
'Class Name : clsLoan_Status_tran.vb
'Purpose    :
'Date       :09/07/2010
'Written By :Sandeep J. Sharma
'Modified   :
'************************************************************************************************************************************

Imports eZeeCommonLib
''' <summary>
''' Purpose: 
''' Developer: Sandeep J. Sharma
''' </summary>
Public Class clsLoan_Status_tran
    Private Const mstrModuleName = "clsLoan_Status_tran"
    Dim objDataOperation As clsDataOperation
    Dim mstrMessage As String = ""

#Region " Private variables "
    Private mintLoanstatustranunkid As Integer
    Private mintLoanadvancetranunkid As Integer
    Private mdtStaus_Date As Date
    Private mintStatusunkid As Integer

    'Anjan (11 May 2011)-Start
    'Private mdblSettle_Amount As Double
    Private mdecSettle_Amount As Decimal
    'Anjan (11 May 2011)-End 


    Private mstrRemark As String = String.Empty
    Private mblnIsvoid As Boolean
    Private mdtVoiddatetime As Date
    Private mintVoiduserunkid As Integer
    Private mstrVoidreason As String = String.Empty
#End Region

#Region " Properties "
'S.SANDEEP [28-May-2018] -- START 
'ISSUE/ENHANCEMENT : {Audit Trails} 
Private mstrFormName As String = String.Empty
Public WriteOnly Property _FormName() As String 
Set(ByVal value As String) 
mstrFormName = value 
End Set 
End Property 
 
Private mstrClientIP As String = ""
Public WriteOnly Property _ClientIP() As String 
Set(ByVal value As String) 
mstrClientIP = value 
End Set 
End Property 
 
Private mstrHostName As String = ""
Public WriteOnly Property _HostName() As String 
Set(ByVal value As String) 
mstrHostName = value 
End Set 
End Property 
 
Private mblnIsWeb As Boolean = False
Public WriteOnly Property _FromWeb() As Boolean 
Set(ByVal value As Boolean) 
mblnIsWeb = value 
End Set 
End Property 
 
Private mintAuditUserId As Integer = 0
Public WriteOnly Property _AuditUserId() As Boolean 
Set(ByVal value As Integer) 
mintAuditUserId = value 
End Set 
End Property 
 
Private mdtAuditDate As DateTime = Now
Public WriteOnly Property _AuditDate() As DateTime 
Set(ByVal value As DateTime) 
mdtAuditDate = value 
End Set 
End Property 
 
Private xDataOpr As clsDataOperation = Nothing
Public Property _xDataOpr() As clsDataOperation 
Get 
Return xDataOpr 
End Get 
Set(ByVal value As clsDataOperation) 
xDataOpr = value 
End Set 
End Property 
 
Private mintLoginEmployeeunkid As Integer = 0
Public Property _LoginEmployeeunkid() As Integer 
Get 
Return mintLoginEmployeeunkid 
End Get 
Set(ByVal value As Integer) 
mintLoginEmployeeunkid = value 
End Set 
End Property 
 
Private mintVoidlogingemployeeunkid As Integer=0
Public Property _Voidloginemployeeunkid() As Integer 
Get 
Return mintVoidlogingemployeeunkid 
End Get 
Set(ByVal value As Integer) 
mintVoidlogingemployeeunkid = value 
End Set 
End Property 
'S.SANDEEP [28-May-2018] -- END 
    ''' <summary>
    ''' Purpose: Get Message from Class 
    ''' Modify By: Sandeep J. Sharma
    ''' </summary>
    Public ReadOnly Property _Message() As String
        Get
            Return mstrMessage
        End Get
    End Property

    ''' <summary>
    ''' Purpose: Get or Set loanstatustranunkid
    ''' Modify By: Sandeep J. Sharma
    ''' </summary>
    Public Property _Loanstatustranunkid() As Integer
        Get
            Return mintLoanstatustranunkid
        End Get
        Set(ByVal value As Integer)
            mintLoanstatustranunkid = Value
            Call getData()
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set loanadvancetranunkid
    ''' Modify By: Sandeep J. Sharma
    ''' </summary>
    Public Property _Loanadvancetranunkid() As Integer
        Get
            Return mintLoanadvancetranunkid
        End Get
        Set(ByVal value As Integer)
            mintLoanadvancetranunkid = Value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set status_date
    ''' Modify By: Sandeep J. Sharma
    ''' </summary>
    Public Property _Staus_Date() As Date
        Get
            Return mdtStaus_Date
        End Get
        Set(ByVal value As Date)
            mdtStaus_Date = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set statusunkid
    ''' Modify By: Sandeep J. Sharma
    ''' </summary>
    Public Property _Statusunkid() As Integer
        Get
            Return mintStatusunkid
        End Get
        Set(ByVal value As Integer)
            mintStatusunkid = value
        End Set
    End Property

    '''' <summary>
    '''' Purpose: Get or Set settle_amount
    '''' Modify By: Sandeep J. Sharma
    '''' </summary>


    'Anjan (11 May 2011)-Start
    'Public Property _Settle_Amount() As Double
    Public Property _Settle_Amount() As Decimal
        'Anjan (11 May 2011)-End 
        Get
            Return mdecSettle_Amount
        End Get

        'Anjan (11 May 2011)-Start
        'Set(ByVal value As Double)
        Set(ByVal value As Decimal)
            'Anjan (11 May 2011)-End 
            mdecSettle_Amount = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set remark
    ''' Modify By: Sandeep J. Sharma
    ''' </summary>
    Public Property _Remark() As String
        Get
            Return mstrRemark
        End Get
        Set(ByVal value As String)
            mstrRemark = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set isvoid
    ''' Modify By: Sandeep J. Sharma
    ''' </summary>
    Public Property _Isvoid() As Boolean
        Get
            Return mblnIsvoid
        End Get
        Set(ByVal value As Boolean)
            mblnIsvoid = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set voiddatetime
    ''' Modify By: Sandeep J. Sharma
    ''' </summary>
    Public Property _Voiddatetime() As Date
        Get
            Return mdtVoiddatetime
        End Get
        Set(ByVal value As Date)
            mdtVoiddatetime = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set voiduserunkid
    ''' Modify By: Sandeep J. Sharma
    ''' </summary>
    Public Property _Voiduserunkid() As Integer
        Get
            Return mintVoiduserunkid
        End Get
        Set(ByVal value As Integer)
            mintVoiduserunkid = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set voidreason
    ''' Modify By: Sandeep J. Sharma
    ''' </summary>
    Public Property _Voidreason() As String
        Get
            Return mstrVoidreason
        End Get
        Set(ByVal value As String)
            mstrVoidreason = value
        End Set
    End Property


#End Region

    ''' <summary>
    ''' Modify By: Sandeep J. Sharma
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>
    Public Sub GetData()
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        objDataOperation = New clsDataOperation

        Try
            strQ = "SELECT " & _
                          "  loanstatustranunkid " & _
                          ", loanadvancetranunkid " & _
                          ", status_date " & _
                          ", statusunkid " & _
                          ", settle_amount " & _
                          ", remark " & _
                          ", isvoid " & _
                          ", voiddatetime " & _
                          ", voiduserunkid " & _
                          ", voidreason " & _
                      "FROM lnloan_status_tran " & _
                      "WHERE loanstatustranunkid = @loanstatustranunkid "

            objDataOperation.AddParameter("@loanstatustranunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintLoanstatustranunkid.ToString)

            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            For Each dtRow As DataRow In dsList.Tables(0).Rows
                mintLoanstatustranunkid = CInt(dtRow.Item("loanstatustranunkid"))
                mintLoanadvancetranunkid = CInt(dtRow.Item("loanadvancetranunkid"))
                mdtStaus_Date = dtRow.Item("status_date")
                mintStatusunkid = CInt(dtRow.Item("statusunkid"))

                'Anjan (11 May 2011)-Start
                'mdecSettle_Amount = CDbl(dtRow.Item("settle_amount"))
                mdecSettle_Amount = CDec(dtRow.Item("settle_amount"))
                'Anjan (11 May 2011)-End 


                mstrRemark = dtRow.Item("remark").ToString
                mblnIsvoid = CBool(dtRow.Item("isvoid"))
                If IsDBNull(dtRow.Item("voiddatetime")) Then
                    mdtVoiddatetime = Nothing
                Else
                    mdtVoiddatetime = dtRow.Item("voiddatetime")
                End If
                mintVoiduserunkid = CInt(dtRow.Item("voiduserunkid"))
                mstrVoidreason = dtRow.Item("voidreason").ToString
                Exit For
            Next
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: GetData; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
    End Sub

    ''' <summary>
    ''' Modify By: Sandeep J. Sharma
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>
    Public Function GetList(ByVal strTableName As String, Optional ByVal blnOnlyActive As Boolean = True) As DataSet
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        objDataOperation = New clsDataOperation

        Try
            strQ = "SELECT " & _
              "  loanstatustranunkid " & _
              ", loanadvancetranunkid " & _
              ", status_date " & _
              ", statusunkid " & _
              ", settle_amount " & _
              ", remark " & _
              ", isvoid " & _
              ", voiddatetime " & _
              ", voiduserunkid " & _
             "FROM lnloan_status_tran "

            If blnOnlyActive Then
                strQ &= " WHERE isvoid = 0 "
            End If

            dsList = objDataOperation.ExecQuery(strQ, strTableName)

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: GetList; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
        Return dsList
    End Function

    ''' <summary>
    ''' Modify By: Sandeep J. Sharma
    ''' </summary>
    ''' <returns>Boolean</returns>
    ''' <purpose> INSERT INTO Database Table (lnloan_status_tran) </purpose>
    Public Function Insert(Optional ByVal objDataOpr As clsDataOperation = Nothing) As Boolean 'S.SANDEEP [ 04 DEC 2013 ] -- START -- END
        'Public Function Insert() As Boolean
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        'S.SANDEEP [ 04 DEC 2013 ] -- START
        'objDataOperation = New clsDataOperation
        If objDataOpr Is Nothing Then
        objDataOperation = New clsDataOperation
        Else
            objDataOperation = objDataOpr
            objDataOperation.ClearParameters()
        End If
        'S.SANDEEP [ 04 DEC 2013 ] -- END

        Try
            objDataOperation.AddParameter("@loanadvancetranunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintLoanadvancetranunkid.ToString)
            objDataOperation.AddParameter("@status_date", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, mdtStaus_Date)
            objDataOperation.AddParameter("@statusunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintStatusunkid.ToString)


            'Anjan (11 May 2011)-Start
            'objDataOperation.AddParameter("@settle_amount", SqlDbType.Money, eZeeDataType.MONEY_SIZE, mdecSettle_Amount.ToString)
            objDataOperation.AddParameter("@settle_amount", SqlDbType.Decimal, eZeeDataType.DECIMAL_SIZE, mdecSettle_Amount)
            'Anjan (11 May 2011)-End 


            objDataOperation.AddParameter("@remark", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrRemark.ToString)
            objDataOperation.AddParameter("@isvoid", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsvoid.ToString)
            If mdtVoiddatetime = Nothing Then
                objDataOperation.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, DBNull.Value)
            Else
                objDataOperation.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, mdtVoiddatetime)
            End If
            objDataOperation.AddParameter("@voiduserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintVoiduserunkid.ToString)
            objDataOperation.AddParameter("@voidreason", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrVoidreason.ToString)

            strQ = "INSERT INTO lnloan_status_tran ( " & _
                        "  loanadvancetranunkid " & _
                        ", status_date " & _
                        ", statusunkid " & _
                        ", settle_amount " & _
                        ", remark " & _
                        ", isvoid " & _
                        ", voiddatetime " & _
                        ", voiduserunkid" & _
                        ", voidreason " & _
                    ") VALUES (" & _
                        "  @loanadvancetranunkid " & _
                        ", @status_date " & _
                        ", @statusunkid " & _
                        ", @settle_amount " & _
                        ", @remark " & _
                        ", @isvoid " & _
                        ", @voiddatetime " & _
                        ", @voiduserunkid" & _
                        ", @voidreason " & _
                    "); SELECT @@identity"

            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            mintLoanstatustranunkid = dsList.Tables(0).Rows(0).Item(0)

            Return True
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: Insert; Module Name: " & mstrModuleName)
            Return False
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            'S.SANDEEP [ 04 DEC 2013 ] -- START
            'objDataOperation = Nothing
            'S.SANDEEP [ 04 DEC 2013 ] -- END
        End Try
    End Function

    ''' <summary>
    ''' Modify By: Sandeep J. Sharma
    ''' </summary>
    ''' <returns>Boolean</returns>
    ''' <purpose> Update Database Table (lnloan_status_tran) </purpose>
    Public Function Update() As Boolean
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        objDataOperation = New clsDataOperation

        Try
            objDataOperation.AddParameter("@loanstatustranunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintLoanstatustranunkid.ToString)
            objDataOperation.AddParameter("@loanadvancetranunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintLoanadvancetranunkid.ToString)
            objDataOperation.AddParameter("@status_date", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, mdtStaus_Date)
            objDataOperation.AddParameter("@statusunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintStatusunkid.ToString)

            'Anjan (11 May 2011)-Start
            'objDataOperation.AddParameter("@settle_amount", SqlDbType.Money, eZeeDataType.MONEY_SIZE, mdecSettle_Amount.ToString)
            objDataOperation.AddParameter("@settle_amount", SqlDbType.Decimal, eZeeDataType.DECIMAL_SIZE, mdecSettle_Amount)
            'Anjan (11 May 2011)-End 


            objDataOperation.AddParameter("@remark", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrRemark.ToString)
            objDataOperation.AddParameter("@isvoid", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsvoid.ToString)
            If mdtVoiddatetime = Nothing Then
                objDataOperation.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, DBNull.Value)
            Else
                objDataOperation.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, mdtVoiddatetime)
            End If
            objDataOperation.AddParameter("@voiduserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintVoiduserunkid.ToString)
            objDataOperation.AddParameter("@voidreason", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrVoidreason.ToString)

            strQ = "UPDATE lnloan_status_tran SET " & _
                        "  loanadvancetranunkid = @loanadvancetranunkid" & _
                        ", status_date = @status_date" & _
                        ", statusunkid = @statusunkid" & _
                        ", settle_amount = @settle_amount" & _
                        ", remark = @remark" & _
                        ", isvoid = @isvoid" & _
                        ", voiddatetime = @voiddatetime" & _
                        ", voiduserunkid = @voiduserunkid " & _
                        ", voidreason = @voidreason " & _
                    "WHERE loanstatustranunkid = @loanstatustranunkid "

            Call objDataOperation.ExecNonQuery(strQ)

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            Return True
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: Update; Module Name: " & mstrModuleName)
            Return False
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
    End Function

    ''' <summary>
    ''' Modify By: Sandeep J. Sharma
    ''' </summary>
    ''' <returns>Boolean</returns>
    ''' <purpose> Delete Database Table (lnloan_status_tran) </purpose>
    Public Function Delete(ByVal intUnkid As Integer, Optional ByVal objDataOpr As clsDataOperation = Nothing) As Boolean 'S.SANDEEP [ 04 DEC 2013 ] -- START -- END
        'Public Function Delete(ByVal intUnkid As Integer) As Boolean
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        'S.SANDEEP [ 04 DEC 2013 ] -- START
        'objDataOperation = New clsDataOperation
        If objDataOpr Is Nothing Then
        objDataOperation = New clsDataOperation
        Else
            objDataOperation = objDataOpr
            objDataOperation.ClearParameters()
        End If
        'S.SANDEEP [ 04 DEC 2013 ] -- END

        Try
            strQ = "UPDATE lnloan_status_tran SET " & _
                    "  isvoid = @isvoid" & _
                    ", voiddatetime = @voiddatetime" & _
                    ", voiduserunkid = @voiduserunkid " & _
                    ", voidreason = @voidreason " & _
                  "WHERE loanadvancetranunkid = @loanadvancetranunkid "

            objDataOperation.AddParameter("@loanadvancetranunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intUnkid)
            objDataOperation.AddParameter("@isvoid", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsvoid.ToString)
            objDataOperation.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, mdtVoiddatetime)
            objDataOperation.AddParameter("@voiduserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintVoiduserunkid.ToString)
            objDataOperation.AddParameter("@voidreason", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrVoidreason.ToString)

            Call objDataOperation.ExecNonQuery(strQ)

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            Return True
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: Update; Module Name: " & mstrModuleName)
            Return False
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            'S.SANDEEP [ 04 DEC 2013 ] -- START
            'objDataOperation = Nothing
            'S.SANDEEP [ 04 DEC 2013 ] -- END
        End Try
    End Function


End Class