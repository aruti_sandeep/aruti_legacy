﻿Imports eZeeCommonLib

Public Class clsOrganizationChart
    Private Shared ReadOnly mstrModuleName As String = "clsOrganizationChart"
    Dim objDataOperation As clsDataOperation

    Public Function GetEmployeeList(ByVal xDatabaseName As String, _
                                    ByVal xUserUnkid As Integer, _
                                    ByVal xYearUnkid As Integer, _
                                    ByVal xCompanyUnkid As Integer, _
                                    ByVal xPeriodStart As DateTime, _
                                    ByVal xPeriodEnd As DateTime, _
                                    ByVal xUserModeSetting As String, _
                                    ByVal xOnlyApproved As Boolean, _
                                    ByVal xIncludeIn_ActiveEmployee As Boolean, _
                                    ByVal xNoImageString As String, _
                                    ByVal xIsImgInDataBase As Boolean, _
                                    ByVal xChartType As enOrganizationChartViewType, _
                                    Optional ByVal strListName As String = "List", _
                                    Optional ByVal mblnAddSelect As Boolean = False, _
                                    Optional ByVal strEmployeeID As String = "", _
                                    Optional ByVal intDeptID As Integer = 0, _
                                    Optional ByVal intSectionID As Integer = 0, _
                                    Optional ByVal intUnitID As Integer = 0, _
                                    Optional ByVal intGradeID As Integer = 0, _
                                    Optional ByVal intAccessID As Integer = 0, _
                                    Optional ByVal intClassID As Integer = 0, _
                                    Optional ByVal intCostCenterID As Integer = 0, _
                                    Optional ByVal intServiceID As Integer = 0, _
                                    Optional ByVal intJobID As Integer = 0, _
                                    Optional ByVal intPayPointID As Integer = 0, _
                                    Optional ByVal intBranchID As Integer = 0, _
                                    Optional ByVal intGenderID As Integer = 0, _
                                    Optional ByVal blnExcludeTermEmp_PayProcess As Boolean = False, _
                                    Optional ByVal strFilterQuery As String = "", _
                                    Optional ByVal blnReinstatementDate As Boolean = False, _
                                    Optional ByVal blnIncludeAccessFilterQry As Boolean = True, _
                                    Optional ByVal blnAddApprovalCondition As Boolean = True, _
                                    Optional ByVal isOriginaldata As Boolean = False) As DataSet
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception
        Dim objDataOperation As New clsDataOperation
        Try

            Dim xDateJoinQry, xDataFilterQry, xUACQry, xUACFiltrQry, xAdvanceJoinQry As String
            Dim xJobDateJoinQry, xJobDataFilterQry As String

            xDateJoinQry = "" : xDataFilterQry = "" : xUACQry = "" : xUACFiltrQry = "" : xAdvanceJoinQry = ""
            xJobDateJoinQry = "" : xJobDataFilterQry = ""

            Call GetDatesFilterString(xDateJoinQry, xDataFilterQry, xPeriodStart, xPeriodEnd, blnReinstatementDate, blnExcludeTermEmp_PayProcess, xDatabaseName)

            If xChartType = enOrganizationChartViewType.JOBTOJOB Then
                Call GetDatesFilterString(xJobDateJoinQry, xJobDataFilterQry, xPeriodStart, xPeriodEnd, blnReinstatementDate, blnExcludeTermEmp_PayProcess, xDatabaseName)
            End If


            Call NewAccessLevelFilterString(xUACQry, xUACFiltrQry, xPeriodEnd, xOnlyApproved, xDatabaseName, xUserUnkid, xCompanyUnkid, xYearUnkid, xUserModeSetting, , blnAddApprovalCondition)


            'Hemant (03 Feb 2023) -- Start
            'ENHANCEMENT(NMB) : A1X-612 - As a user, I want to have an effective date for reporting to detail on employee reporting to screen
            'Call GetAdvanceFilterQry(xAdvanceJoinQry, xPeriodEnd, xDatabaseName, "hremployee_reportto")
            Call GetAdvanceFilterQry(xAdvanceJoinQry, xPeriodEnd, xDatabaseName, "Reportto")
            'Hemant (03 Feb 2023) -- End

            If mblnAddSelect = True Then

                strQ = "SELECT ' ' AS employeecode, ' ' + @Select AS employeename, 0 AS employeeunkid,0 AS isapproved, ' ' + @Select AS EmpCodeName,0 AS parent,0 AS ishierarchy,  UNION ALL "
                objDataOperation.AddParameter("@Select", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 10, "  Select"))
            End If

            'strQ += "SELECT " & _
            '          " ISNULL(hremployee_master.firstname, '') + ' ' + ISNULL(hremployee_master.othername, '') + ' ' + ISNULL(hremployee_master.surname, '') AS name " & _
            '          ",hremployee_master.employeeunkid AS employeeunkid " & _
            '          ",hremployee_master.email AS empemail " & _
            '          ",hremployee_master.present_mobile AS empmobile " & _
            '          ",ISNULL(hremployee_master.isapproved, 0) AS isapproved " & _
            '          ",ISNULL(hremployee_master.employeecode, '') + ' - ' + ISNULL(hremployee_master.firstname, '') + ' ' + ISNULL(hremployee_master.othername, '') + ' ' + ISNULL(hremployee_master.surname, '') AS EmpCodeName " & _
            '          ",ISNULL(rpt.reporttoemployeeunkid, 0) AS parent " & _
            '          ",ISNULL(rpt.ishierarchy, 0) AS ishierarchy " & _
            '          ",ISNULL(hrstation_master.name, '') AS branch " & _
            '          ",ISNULL(hrdepartment_group_master.name, '') AS deptgroup " & _
            '          ",ISNULL(hrdepartment_master.name, '') AS Department " & _
            '          ",ISNULL(hrsectiongroup_master.name, '') AS secgroup " & _
            '          ",ISNULL(hrsection_master.name, '') AS section " & _
            '          ",ISNULL(hrunitgroup_master.name, '') AS unitname " & _
            '          ",ISNULL(hrunit_master.name, '') AS unit " & _
            '          ",ISNULL(hrteam_master.name, '') AS team " & _
            '          ",ISNULL(hrclassgroup_master.name, '') AS classgrp " & _
            '          ",ISNULL(hrclasses_master.name, '') AS class " & _
            '          ",ISNULL(hrjob_master.job_name, '') AS Job " & _
            '          ",ISNULL(hrjob_master.jobunkid, '') AS jobunkid "


            strQ += "IF OBJECT_ID('tempdb..#Results') IS NOT NULL DROP TABLE #Results " & _
                    "CREATE TABLE #Results (reporttounkid int,employeeunkid int, reporttoemployeeunkid int, ishierarchy int) " & _
                    "; " & _
                    "WITH CTE " & _
                    "AS " & _
                    "(SELECT " & _
                              "reporttounkid " & _
                    "       ,Reportto.employeeunkid " & _
                    "       ,reporttoemployeeunkid " & _
                    "       ,ishierarchy " & _
                    " FROM " & _
                    "       (SELECT " & _
                    "           reporttounkid " & _
                            ",hremployee_reportto.employeeunkid " & _
                            ",reporttoemployeeunkid " & _
                            ",ishierarchy " & _
                    "           ,ROW_NUMBER() OVER (PARTITION BY hremployee_reportto.employeeunkid ORDER BY effectivedate DESC) AS Rno " & _
                    "        FROM hremployee_reportto " & _
                    "        WHERE isvoid = 0 " & _
                    "             AND ishierarchy = 1 " & _
                    "             AND CONVERT(CHAR(8), effectivedate, 112) <= '" & eZeeDate.convertDate(xPeriodEnd) & "' " & _
                    "       ) AS Reportto  "

            If xAdvanceJoinQry.Trim.Length > 0 Then
                strQ &= xAdvanceJoinQry
            End If


            'Hemant (03 Feb 2023) -- Start
            'ENHANCEMENT(NMB) : A1X-612 - As a user, I want to have an effective date for reporting to detail on employee reporting to screen
            'strQ &= "WHERE " & _
            '            "ishierarchy = 1 " & _
            '            "AND isvoid = 0 "
            strQ &= "WHERE " & _
                        "Reportto.Rno = 1 " 
            'Hemant (03 Feb 2023) -- End

           

            If strFilterQuery.Trim <> "" Then
                strQ &= " AND " & strFilterQuery & " "
            End If


            'strQ &= "UNION ALL " & _
            '             "SELECT " & _
            '                  "0 AS reporttounkid " & _
            '                ",employeeunkid " & _
            '                ",0 AS reporttoemployeeunkid " & _
            '                ",CAST(0 AS BIT) AS ishierarchy " & _
            '             "FROM hremployee_master " & _
            '             "WHERE employeeunkid NOT IN (SELECT " & _
            '                       "employeeunkid " & _
            '                  "FROM hremployee_reportto " & _
            '                  "WHERE isvoid = 0) " & _

            strQ &= "UNION ALL " & _
                         "SELECT " & _
                              "E.reporttounkid " & _
                            ",e.employeeunkid " & _
                            ",CASE " & _
                                   "WHEN E.reporttoemployeeunkid <> e.employeeunkid THEN E.reporttoemployeeunkid " & _
                                   "ELSE 0 " & _
                              "END AS reporttoemployeeunkid " & _
                            ",E.ishierarchy " & _
                    "   FROM ( " & _
                    "           SELECT " & _
                    "               reporttounkid " & _
                    "               ,employeeunkid " & _
                    "               ,reporttoemployeeunkid " & _
                    "               ,ishierarchy " & _
                    "               ,ROW_NUMBER() OVER (PARTITION BY hremployee_reportto.employeeunkid ORDER BY effectivedate DESC) AS Rno " & _
                    "           FROM hremployee_reportto " & _
                    "           WHERE isvoid = 0 " & _
                    "               AND ishierarchy = 1 " & _
                    "               AND CONVERT(CHAR(8), effectivedate, 112) <= '" & eZeeDate.convertDate(xPeriodEnd) & "' " & _
                    "       ) AS E " & _
                         "INNER JOIN CTE " & _
                              "ON E.employeeunkid = CTE.reporttoemployeeunkid " & _
                    "           WHERE E.Rno = 1 " & _
                    " )  " & _
                    "INSERT INTO #Results "

            strQ += "SELECT " & _
                           "b.* " & _
                      "FROM (SELECT " & _
                                "* " & _
                           "FROM CTE " & _
                           "UNION ALL " & _
                         "SELECT " & _
                                "a.* " & _
                           "FROM CTE " & _
                           "left JOIN (SELECT " & _
                                     "0 AS reporttounkid " & _
                                   ",employeeunkid " & _
                                   ",0 AS reporttoemployeeunkid " & _
                                   ",CAST(0 AS BIT) AS ishierarchy " & _
                                "FROM hremployee_master " & _
                                "WHERE employeeunkid NOT IN (SELECT " & _
                                          "employeeunkid " & _
                                     "FROM hremployee_reportto " & _
                                     "WHERE isvoid = 0 AND CONVERT(CHAR(8), effectivedate, 112) <= '" & eZeeDate.convertDate(xPeriodEnd) & "' " & _
                                     " )) AS a " & _
                                "ON CTE.reporttoemployeeunkid = a.employeeunkid) AS b "

            strQ += " Select data.* "


            If xChartType = enOrganizationChartViewType.JOBTOJOB Then
                strQ += ",otherjobdetail.[Total Postion]," & _
                        "otherjobdetail.[Position Held]," & _
                        "otherjobdetail.Variance "
            End If

            strQ += " from ("

            strQ += "SELECT " & _
                      " ISNULL(hremployee_master.firstname, '') + ' ' + ISNULL(hremployee_master.othername, '') + ' ' + ISNULL(hremployee_master.surname, '') AS name " & _
                      ",hremployee_master.employeeunkid AS id " & _
                      ",hremployee_master.email AS Email " & _
                      ",hremployee_master.present_mobile AS Mobile " & _
                      ",ISNULL(reportto.reporttoemployeeunkid, 0) AS parent " & _
                      ",ISNULL(hrdepartment_master.name, '') AS Department " & _
                      ",ISNULL(hrjob_master.job_name, '') AS Job " & _
                      ",ISNULL(hrjob_master.jobunkid, '') AS jobunkid " & _
                      ",'' as empBaseImage "

            If xChartType = enOrganizationChartViewType.JOBTOJOB Then
                strQ += ",ROW_NUMBER () OVER(PARTITION by ISNULL(reportto.reporttoemployeeunkid, 0),ISNULL(hrjob_master.jobunkid, '') ORDER by reportto.reporttoemployeeunkid) as jobrno "

               
            End If

            If xChartType = enOrganizationChartViewType.EMPLOYEEWISE Then
            If xIsImgInDataBase Then
                    strQ &= ",arutiimages..hremployee_images.photo as empImage "
            Else
                    strQ &= ",hr_images_tran.imagename as empImage "
            End If
            End If


            If isOriginaldata Then
                strQ &= ",Cast( 1 as Bit) as  isOriginaldata "
            Else
                strQ &= ",Cast( 0 as Bit) as  isOriginaldata "
            End If

            strQ &= "FROM hremployee_master "

            '"LEFT JOIN hremployee_reportto AS rpt " & _
            '"ON hremployee_master.employeeunkid = rpt.employeeunkid " & _
            '"AND rpt.ishierarchy = 1 AND rpt.isvoid =0 "

            'If xAdvanceJoinQry.Trim.Length > 0 Then
            '    strQ &= xAdvanceJoinQry
            'End If

            strQ &= "JOIN (SELECT " & _
                        "employeeunkid " & _
                        ",reporttoemployeeunkid " & _
                     "FROM (SELECT " & _
                               "* " & _
                             ",ROW_NUMBER() OVER (PARTITION BY employeeunkid, reporttoemployeeunkid ORDER BY ishierarchy) AS rno " & _
                          "FROM #Results) AS a " & _
                     "WHERE a.rno = 1) AS reportto " & _
                     "ON hremployee_master.employeeunkid = reportto.employeeunkid "

            strQ &= "LEFT JOIN " & _
                    "( " & _
                    "    SELECT " & _
                    "         stationunkid " & _
                    "        ,deptgroupunkid " & _
                    "        ,departmentunkid " & _
                    "        ,sectiongroupunkid " & _
                    "        ,sectionunkid " & _
                    "        ,unitgroupunkid " & _
                    "        ,unitunkid " & _
                    "        ,teamunkid " & _
                    "        ,classgroupunkid " & _
                    "        ,classunkid " & _
                    "        ,employeeunkid " & _
                    "        ,ROW_NUMBER()OVER(PARTITION BY employeeunkid ORDER BY effectivedate DESC) as Rno " & _
                    "    FROM hremployee_transfer_tran " & _
                    "    WHERE isvoid = 0 AND CONVERT(CHAR(8),effectivedate,112) <= '" & eZeeDate.convertDate(xPeriodEnd) & "' " & _
                    ") AS T ON T.employeeunkid = hremployee_master.employeeunkid AND T.Rno = 1 " & _
                    "LEFT JOIN hrstation_master " & _
                    "ON hrstation_master.stationunkid = T.stationunkid " & _
                    "LEFT JOIN hrdepartment_group_master " & _
                    "ON hrdepartment_group_master.deptgroupunkid = T.deptgroupunkid " & _
                    "LEFT JOIN hrdepartment_master " & _
                    "ON hrdepartment_master.departmentunkid = T.departmentunkid " & _
                    "LEFT JOIN hrsectiongroup_master " & _
                    "ON T.sectiongroupunkid = hrsectiongroup_master.sectiongroupunkid " & _
                    "LEFT JOIN hrsection_master " & _
                    "ON hrsection_master.sectionunkid = T.sectionunkid " & _
                    "LEFT JOIN hrunitgroup_master " & _
                    "ON T.unitgroupunkid = hrunitgroup_master.unitgroupunkid " & _
                    "LEFT JOIN hrunit_master " & _
                    "ON hrunit_master.unitunkid = T.unitunkid " & _
                    "LEFT JOIN hrteam_master " & _
                    "ON T.teamunkid = hrteam_master.teamunkid " & _
                    "LEFT JOIN hrclassgroup_master " & _
                    "ON hrclassgroup_master.classgroupunkid = T.classgroupunkid " & _
                    "LEFT JOIN hrclasses_master " & _
                    "ON hrclasses_master.classesunkid = T.classunkid "

            If xChartType = enOrganizationChartViewType.EMPLOYEEWISE Then
            If xIsImgInDataBase Then
                strQ &= "LEFT JOIN arutiimages..hremployee_images " & _
                             "ON arutiimages..hremployee_images.employeeunkid = hremployee_master.employeeunkid " & _
                         "AND hremployee_images.companyunkid = '" & xCompanyUnkid & "' AND hremployee_images.isvoid = 0 "

			'Gajanan [23-June-2020] -- ADD hremployee_images.isvoid
			'Issue #004675: Employee appears multiple times on organization structure.						 

            Else
                strQ &= "LEFT join hr_images_tran " & _
                        "ON hr_images_tran.employeeunkid = t.employeeunkid "
            End If
            End If


            strQ &= "LEFT JOIN " & _
              "( " & _
              "    SELECT " & _
              "         jobgroupunkid " & _
              "        ,jobunkid " & _
              "        ,employeeunkid " & _
              "        ,ROW_NUMBER()OVER(PARTITION BY employeeunkid ORDER BY effectivedate DESC) as Rno " & _
              "    FROM hremployee_categorization_tran " & _
              "    WHERE isvoid = 0 AND CONVERT(CHAR(8),effectivedate,112) <= '" & eZeeDate.convertDate(xPeriodEnd) & "' " & _
              ") AS J ON J.employeeunkid = hremployee_master.employeeunkid AND J.Rno = 1 " & _
              "LEFT JOIN hrjob_master " & _
                   "ON hrjob_master.jobunkid = J.jobunkid " & _
              "LEFT JOIN " & _
              "( " & _
              "    SELECT " & _
              "         cctranheadvalueid AS costcenterunkid " & _
              "        ,employeeunkid " & _
              "        ,ROW_NUMBER()OVER(PARTITION BY employeeunkid ORDER BY effectivedate DESC) as Rno " & _
              "    FROM hremployee_cctranhead_tran " & _
              "    WHERE isvoid = 0 AND istransactionhead = 0 AND CONVERT(CHAR(8),effectivedate,112) <= '" & eZeeDate.convertDate(xPeriodEnd) & "' " & _
              ") AS C ON C.employeeunkid = hremployee_master.employeeunkid AND C.Rno = 1 " & _
              "LEFT JOIN " & _
              "( " & _
              "	SELECT " & _
              "		 gradegroupunkid " & _
              "		,gradeunkid " & _
              "		,gradelevelunkid " & _
              "		,employeeunkid " & _
              "		,ROW_NUMBER() OVER ( PARTITION BY employeeunkid ORDER BY incrementdate DESC, salaryincrementtranunkid DESC ) AS Rno " & _
              "	FROM prsalaryincrement_tran " & _
              "	WHERE isvoid = 0 AND isapproved = 1 AND CONVERT(CHAR(8), incrementdate, 112) <= '" & eZeeDate.convertDate(xPeriodEnd) & "' " & _
              ") AS G ON G.employeeunkid = hremployee_master.employeeunkid AND G.Rno = 1 " & _
              "LEFT JOIN " & _
              "( " & _
              "   SELECT " & _
              "        hremployee_shift_tran.employeeunkid " & _
              "       ,hremployee_shift_tran.shiftunkid " & _
              "       ,ROW_NUMBER()OVER(PARTITION BY hremployee_shift_tran.employeeunkid ORDER BY hremployee_shift_tran.effectivedate DESC) AS Rno " & _
              "   FROM hremployee_shift_tran " & _
              "   WHERE hremployee_shift_tran.isvoid = 0 " & _
              "   AND hremployee_shift_tran.effectivedate IS NOT NULL AND CONVERT(NVARCHAR(8),hremployee_shift_tran.effectivedate,112) <= '" & eZeeDate.convertDate(xPeriodEnd) & "' " & _
              ") AS S ON S.employeeunkid = hremployee_master.employeeunkid AND S.Rno = 1 "



            '********************** DATA FOR DATES CONDITION ************************' --- START
            If xDateJoinQry.Trim.Length > 0 Then
                strQ &= xDateJoinQry
            End If


            If blnIncludeAccessFilterQry = True Then
                If xUACQry.Trim.Length > 0 Then
                    strQ &= xUACQry
                End If
            End If

            '********************** DATA FOR DATES CONDITION ************************' --- END




            strQ &= " WHERE 1=1 "
            '================ Employee ID
            'If intEmployeeID > 0 Then

            '    strQ += " AND hremployee_master.employeeunkid = @employeeunkid"
            '    objDataOperation.AddParameter("@employeeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intEmployeeID)
            'End If

            If strEmployeeID.Length > 0 Then
                strQ += " AND hremployee_master.employeeunkid in(" & strEmployeeID & ")"
            End If


            '================ PayPoint
            If intPayPointID > 0 Then
                strQ += " AND paypointunkid = @paypointunkid"
                objDataOperation.AddParameter("@paypointunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intPayPointID)
            End If

            '================ Department
            If intDeptID > 0 Then

                strQ += " AND T.departmentunkid = @departmentunkid"
                objDataOperation.AddParameter("@departmentunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intDeptID)
            End If

            '================ Section
            If intSectionID > 0 Then

                strQ += " AND T.sectionunkid = @sectionunkid"
                objDataOperation.AddParameter("@sectionunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intSectionID)
            End If

            '================ Unit
            If intUnitID > 0 Then

                strQ += " AND T.unitunkid = @unitunkid"
                objDataOperation.AddParameter("@unitunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intUnitID)
            End If

            '================ Job
            If intJobID > 0 Then

                strQ += " AND J.jobunkid = @jobunkid"

                objDataOperation.AddParameter("@jobunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intJobID)
            End If

            '================ Grade
            If intGradeID > 0 Then
                strQ += " AND G.gradeunkid = @gradeunkid"
                objDataOperation.AddParameter("@gradeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intGradeID)
            End If

            '================ Access
            If intAccessID > 0 Then
                strQ += " AND accessunkid = @accessunkid"
                objDataOperation.AddParameter("@accessunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intAccessID)
            End If

            '================ Class
            If intClassID > 0 Then

                strQ += " AND T.classunkid = @classunkid"

                objDataOperation.AddParameter("@classunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intClassID)
            End If

            '================ Service
            If intServiceID > 0 Then
                strQ += " AND serviceunkid = @serviceunkid"
                objDataOperation.AddParameter("@serviceunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intServiceID)
            End If

            '================ CostCenter
            If intCostCenterID > 0 Then

                strQ += " AND C.costcenterunkid = @costcenterunkid"

                objDataOperation.AddParameter("@costcenterunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intCostCenterID)
            End If

            '================ Branch
            If intBranchID > 0 Then

                strQ += " AND T.stationunkid = @stationunkid "
                objDataOperation.AddParameter("@stationunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intBranchID)
            End If

            If xIncludeIn_ActiveEmployee = False Then
                If xDataFilterQry.Trim.Length > 0 Then
                    strQ &= xDataFilterQry
                End If
            End If

            If intGenderID > 0 Then
                strQ += " AND hremployee_master.gender = @genderID "
                objDataOperation.AddParameter("@genderID", SqlDbType.Int, eZeeDataType.INT_SIZE, intGenderID)
            End If
            strQ += ") as data "

            If xChartType = enOrganizationChartViewType.JOBTOJOB Then

                strQ &= "Left Join ( " & _
                        "SELECT " & _
                             "hrjob_master.jobunkid AS [JobId] " & _
                             ",hrjob_master.total_position AS [Total Postion] " & _
                             ",COUNT(JB.jobunkid) AS [Position Held] " & _
                             ",(hrjob_master.total_position - COUNT(JB.jobunkid)) AS [Variance] " & _
                        "FROM hrjob_master " & _
                             "LEFT JOIN " & _
                                            "( " & _
                                            "SELECT " & _
                                            "hremployee_categorization_tran.jobunkid " & _
                                            ",ROW_NUMBER()OVER(PARTITION BY hremployee_categorization_tran.employeeunkid ORDER BY effectivedate DESC) as rno " & _
                                            "FROM hremployee_categorization_tran " & _
                                            "left join hremployee_master on " & _
                                            "hremployee_categorization_tran.employeeunkid = hremployee_master.employeeunkid "

                If xJobDateJoinQry.Trim.Length > 0 Then
                    strQ &= xJobDateJoinQry
                End If



                strQ &= "WHERE isvoid = 0 AND CONVERT(NVARCHAR(8),effectivedate,112) <=  CONVERT(NVARCHAR(8),GETDATE(),112) "

                If xIncludeIn_ActiveEmployee = False Then
                    If xJobDataFilterQry.Trim.Length > 0 Then
                        strQ &= xJobDataFilterQry
                    End If
                End If

                strQ &= ") AS JB ON JB.jobunkid = hrjob_master.jobunkid AND JB.rno = 1 " & _
                                            "GROUP BY job_name,total_position,report_tounkid,hrjob_master.jobunkid Having 1=1 " & _
                        ") as otherjobdetail " & _
                        "on otherjobdetail.JobId = data.jobunkid " & _
                        "WHERE data.jobrno = 1 "
            End If



            If xIncludeIn_ActiveEmployee = False Then
                objDataOperation.AddParameter("@startdate", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, eZeeDate.convertDate(xPeriodStart))
                objDataOperation.AddParameter("@enddate", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, eZeeDate.convertDate(xPeriodEnd))
            Else
                objDataOperation.AddParameter("@startdate", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, ConfigParameter._Object._EmployeeAsOnDate)
                objDataOperation.AddParameter("@enddate", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, ConfigParameter._Object._EmployeeAsOnDate)
            End If



            dsList = objDataOperation.ExecQuery(strQ, strListName)

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If


            If xChartType = enOrganizationChartViewType.EMPLOYEEWISE Then
                dsList.Tables(0).AsEnumerable().Cast(Of DataRow).ToList().ForEach(Function(x) convertImageToBase64(x, xNoImageString))
                dsList.Tables(0).Columns.Remove("empImage")
            End If

            Return dsList

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "GetEmployeeList", mstrModuleName)
            Return Nothing
        Finally
            If dsList IsNot Nothing Then dsList.Dispose()
            dsList = Nothing
            If objDataOperation IsNot Nothing Then objDataOperation.Dispose()
            objDataOperation = Nothing

            exForce = Nothing
        End Try
    End Function

    Private Function convertImageToBase64(ByVal dr As DataRow, ByVal strNoimage As String) As Boolean
        Try
            If IsNothing(dr.Field(Of Byte())("empImage")) = False Then
                Dim bytes As Byte() = dr.Field(Of Byte())("empImage")
                Dim base64String As String = Convert.ToBase64String(bytes, 0, bytes.Length)
                dr("empBaseImage") = base64String
            Else
                dr("empBaseImage") = strNoimage
            End If
            Return True

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "GetEmployeeList", mstrModuleName)
        End Try
    End Function


    'Gajanan [13-Feb-2019] -- Start
    'Enhancement - Add Filters IN Organization Chart.
    Public Function RecursionJob(ByVal Table As DataTable, ByVal jobid As Integer) As DataTable
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception
        Dim objDataOperation As New clsDataOperation
        Try
            'strQ += "SELECT JobunkId,job_Name,report_tounkid from hrjob_master WHERE jobunkid = " & jobid & "  "

            dsList = GetJobWiseList("data", "and hrjob_master.jobunkid =" & jobid & " ", ",hrjob_master.jobunkid", False)

            If dsList.Tables("data").Rows.Count > 0 Then
                Dim result As DataRow() = Table.Select("id = '" & dsList.Tables("data").Rows(0)("JobId") & "'")
                If result.Length <= 0 Then
                    Dim dRow As DataRow
                    dRow = Table.NewRow

                    dRow.Item("id") = dsList.Tables("data").Rows(0)("JobId")
                    dRow.Item("name") = dsList.Tables("data").Rows(0)("Job Name")
                    dRow.Item("parent") = dsList.Tables("data").Rows(0)("parent")

                    dRow.Item("totalpostion") = dsList.Tables("data").Rows(0)("Total Postion")
                    dRow.Item("positionheld") = dsList.Tables("data").Rows(0)("Position Held")
                    dRow.Item("variance") = dsList.Tables("data").Rows(0)("Variance")
                    dRow.Item("isOriginaldata") = False

                    Table.Rows.Add(dRow)
                    Table.AcceptChanges()
                End If

                If CInt(dsList.Tables("data").Rows(0)("parent")) > 0 Then
                    RecursionJob(Table, CInt(dsList.Tables("data").Rows(0)("parent")))
                End If
            End If


            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            Return Table
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "GetJobWiseList", mstrModuleName)
            Return Nothing
        Finally
            If dsList IsNot Nothing Then dsList.Dispose()
            dsList = Nothing
            If objDataOperation IsNot Nothing Then objDataOperation.Dispose()
            objDataOperation = Nothing
        End Try

    End Function

    'Public Function RecursionEmp(ByVal xDatabaseName As String, _
    '                             ByVal xUserUnkid As Integer, _
    '                             ByVal xYearUnkid As Integer, _
    '                             ByVal xCompanyUnkid As Integer, _
    '                             ByVal xPeriodStart As DateTime, _
    '                             ByVal xPeriodEnd As DateTime, _
    '                             ByVal xUserModeSetting As String, _
    '                             ByVal xOnlyApproved As Boolean, _
    '                             ByVal xIncludeIn_ActiveEmployee As Boolean, _
    '                             ByVal xEmployeeId As Integer, _
    '                             ByVal xTable As DataTable, _
    '                             ByVal xIsImgInDataBase As Boolean, _
    '                             ByVal xBlankImage As String, _
    '                             ByVal xChartType As enOrganizationChartViewType) As DataTable

    '    Dim dsList As DataSet = Nothing
    '    Dim strQ As String = ""
    '    Dim exForce As Exception
    '    Dim objDataOperation As New clsDataOperation
    '    Try

    '        strQ = "IF OBJECT_ID('tempdb..#Results') IS NOT NULL DROP TABLE #Results " & _
    '                "CREATE TABLE #Results (empid int, rempid int) " & _
    '                "DECLARE  @REmpId AS INT, @NxEmpId AS INT " & _
    '                "SET @REmpId = 0 " & _
    '                "SET @NxEmpId  = @EmployeeID " & _
    '                "WHILE (@NxEmpId) > 0 " & _
    '                "BEGIN " & _
    '                     "SET @REmpId = ISNULL((SELECT  reporttoemployeeunkid FROM hremployee_reportto WHERE employeeunkid = @NxEmpId and ishierarchy =1 and isvoid = 0),0) " & _
    '                          "if (@NxEmpId = @REmpId ) " & _
    '                          "set @REmpId =0 " & _
    '                          "INSERT INTO #Results(empid, rempid)VALUES(@NxEmpId, @REmpId) " & _
    '                          "SET @NxEmpId = @REmpId " & _
    '                          "DECLARE @Rcnt AS INT " & _
    '                          "SET @Rcnt = (SELECT COUNT(1) FROM #Results WHERE rempid = @NxEmpId) " & _
    '                          "IF (@Rcnt > 1) " & _
    '                            "BREAK " & _
    '                          "ELSE " & _
    '                            "CONTINUE " & _
    '                "END " & _
    '                "SELECT * " & _
    '                "FROM #Results " & _
    '                "WHERE empid > 0 "

    '        objDataOperation.AddParameter("@EmployeeID", SqlDbType.Int, eZeeDataType.INT_SIZE, xEmployeeId)
    '        dsList = objDataOperation.ExecQuery(strQ, "RecursionEmp")

    '        If objDataOperation.ErrorMessage <> "" Then
    '            exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
    '            Throw exForce
    '        End If


    '        If IsNothing(dsList) = False AndAlso dsList.Tables("RecursionEmp").Rows.Count > 0 Then

    '            If CInt(dsList.Tables("RecursionEmp").Rows(dsList.Tables("RecursionEmp").Rows.Count - 1)("rempid")) = 0 Then
    '                Dim StrEmp As String = ""
    '                For Each drow As DataRow In dsList.Tables("RecursionEmp").Rows
    '                    Dim result As DataRow() = xTable.Select("id = '" & drow("rempid") & "'")

    '                    If result.Length <= 0 Then
    '                        If StrEmp.Length > 0 Then
    '                            StrEmp &= "," + drow("rempid").ToString()
    '                        Else
    '                            StrEmp &= drow("rempid").ToString()
    '                        End If
    '                    End If
    '                Next


    '                If StrEmp.Length > 0 Then
    '                    dsList = GetEmployeeList(xDatabaseName, xUserUnkid, xYearUnkid, xCompanyUnkid, xPeriodStart, xPeriodEnd, xUserModeSetting, xOnlyApproved, xIncludeIn_ActiveEmployee, xBlankImage, xIsImgInDataBase, "Emp", False, StrEmp, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, False, "", False, True, True, False)

    '                    For Each drow As DataRow In dsList.Tables(0).Rows

    '                        Dim isJobExist As Integer = 0
    '                        If xChartType = enOrganizationChartViewType.JOBTOJOB Then
    '                            isJobExist = xTable.AsEnumerable().Where(Function(x) x.Field(Of Integer)("parent") = CInt(dsList.Tables("emp").Rows(0)("parent").ToString()) AndAlso x.Field(Of Integer)("jobunkid") = CInt(dsList.Tables("emp").Rows(0)("jobunkid").ToString())).Count
    '                        End If

    '                        If isJobExist <= 0 Then
    '                            Dim dtRow As DataRow
    '                            dtRow = xTable.NewRow

    '                            xTable.ImportRow(dtRow)
    '                            xTable.AcceptChanges()
    '                        End If
    '                    Next
    '                End If

    '            End If
    '        End If


    '        'dsList = GetEmployeeList(xDatabaseName, xUserUnkid, xYearUnkid, xCompanyUnkid, xPeriodStart, xPeriodEnd, xUserModeSetting, xOnlyApproved, xIncludeIn_ActiveEmployee, xIsImgInDataBase, "Emp", False, xEmployeeId, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, False, "", False, True, True, False)

    '        'If dsList.Tables("emp").Rows.Count > 0 Then
    '        '    Dim result As DataRow() = xTable.Select("id = '" & dsList.Tables("emp").Rows(0)("employeeunkid") & "'")

    '        '    If result.Length <= 0 Then
    '        '        Dim isJobExist As Integer = 0
    '        '        If xChartType = enOrganizationChartViewType.JOBTOJOB Then
    '        '            isJobExist = xTable.AsEnumerable().Where(Function(x) x.Field(Of Integer)("parent") = CInt(dsList.Tables("emp").Rows(0)("parent").ToString()) AndAlso x.Field(Of Integer)("jobunkid") = CInt(dsList.Tables("emp").Rows(0)("jobunkid").ToString())).Count
    '        '        End If


    '        '        If isJobExist <= 0 Then
    '        '            Dim dtRow As DataRow
    '        '            dtRow = xTable.NewRow

    '        '            dtRow.Item("id") = CInt(dsList.Tables("emp").Rows(0)("employeeunkid"))
    '        '            dtRow.Item("name") = dsList.Tables("emp").Rows(0)("employeename").ToString
    '        '            'Gajanan [24-06-2020] -- Start   
    '        '            'Issue:Fix For if Employee Assign Own self As Reporting to 

    '        '            If CInt(dsList.Tables("emp").Rows(0)("employeeunkid")) = dsList.Tables("emp").Rows(0)("parent") Then
    '        '                dtRow.Item("parent") = 0
    '        '            Else
    '        '                dtRow.Item("parent") = dsList.Tables("emp").Rows(0)("parent")
    '        '            End If
    '        '            'Gajanan [24-06-2020] -- End   


    '        '            dtRow.Item("Department") = dsList.Tables("emp").Rows(0)("dept").ToString
    '        '            dtRow.Item("Job") = dsList.Tables("emp").Rows(0)("job").ToString
    '        '            dtRow.Item("Email") = dsList.Tables("emp").Rows(0)("empemail").ToString
    '        '            dtRow.Item("Mobile") = dsList.Tables("emp").Rows(0)("empmobile").ToString
    '        '            dtRow.Item("isOriginaldata") = False
    '        '            dtRow.Item("jobunkid") = dsList.Tables("emp").Rows(0)("jobunkid").ToString()

    '        '            Dim st As String = ""
    '        '            If xIsImgInDataBase AndAlso xChartType = enOrganizationChartViewType.EMPLOYEEWISE Then
    '        '                If IsDBNull(dsList.Tables("emp").Rows(0)("photo")) <> True Then
    '        '                    Dim bytes As Byte() = CType(dsList.Tables("emp").Rows(0)("photo"), Byte())
    '        '                    If xIsImgInDataBase Then
    '        '                        st = Convert.ToBase64String(bytes)
    '        '                    End If
    '        '                Else
    '        '                    st = xBlankImage
    '        '                End If
    '        '            Else
    '        '                If IsDBNull(dsList.Tables("emp").Rows(0)("photo")) <> True Then
    '        '                    st = dsList.Tables("emp").Rows(0)("photo").ToString()
    '        '                Else
    '        '                    st = xBlankImage
    '        '                End If
    '        '            End If

    '        '            dtRow.Item("Image") = st

    '        '            xTable.Rows.Add(dtRow)
    '        '            xTable.AcceptChanges()
    '        '        End If

    '        '        If CInt(dsList.Tables("emp").Rows(0)("parent")) > 0 Then
    '        '            'Gajanan [24-06-2020] -- Start   
    '        '            'Issue:Fix For if Employee Assign Own self As Reporting to 

    '        '            If xEmployeeId = CInt(dsList.Tables("emp").Rows(0)("parent")) Then
    '        '                Return xTable
    '        '            Else
    '        '                RecursionEmp(xDatabaseName, xUserUnkid, xYearUnkid, xCompanyUnkid, xPeriodStart, xPeriodEnd, xUserModeSetting, xOnlyApproved, xIncludeIn_ActiveEmployee, CInt(dsList.Tables("emp").Rows(0)("parent")), xTable, xIsImgInDataBase, xBlankImage, xChartType)
    '        '            End If
    '        '            'Gajanan [24-06-2020] -- End   

    '        '        End If
    '        '    End If

    '        'End If


    '        'If objDataOperation.ErrorMessage <> "" Then
    '        '    exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
    '        '    Throw exForce
    '        'End If

    '        Return xTable
    '    Catch ex As Exception
    '        DisplayError.Show("-1", ex.Message, "GetJobWiseList", mstrModuleName)
    '        Return Nothing
    '    Finally
    '        If dsList IsNot Nothing Then dsList.Dispose()
    '        dsList = Nothing
    '        If objDataOperation IsNot Nothing Then objDataOperation.Dispose()
    '        objDataOperation = Nothing
    '    End Try

    'End Function
    'Gajanan [13-Feb-2019] -- End



    Public Function GetJobWiseList(ByVal strListName As String, Optional ByVal strFilterQuery As String = "", Optional ByVal strGroupFilter As String = "", Optional ByVal isOriginaldata As Boolean = False) As DataSet
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception
        Dim objDataOperation As New clsDataOperation
        Try
            strQ += "SELECT " & _
                    "hrjob_master.jobunkid AS [JobId] " & _
                    ",hrjob_master.job_name AS [Job Name] " & _
                    ",hrjob_master.total_position AS [Total Postion] " & _
                    ",COUNT(JB.jobunkid) AS [Position Held] " & _
                    ",(hrjob_master.total_position - COUNT(JB.jobunkid)) AS [Variance] " & _
                    ",CASE WHEN hrjob_master.jobunkid = hrjob_master.report_tounkid THEN 0 WHEN hrjob_master.report_tounkid < 0 THEN 0 ELSE hrjob_master.report_tounkid END AS parent "

            If isOriginaldata Then
                strQ += ",1 as isOriginaldata "
            Else
                strQ += ",0 as isOriginaldata "
            End If

            strQ += "FROM hrjob_master " & _
                    "LEFT JOIN " & _
                    "( " & _
                    "SELECT " & _
                    "jobunkid " & _
                    ",ROW_NUMBER()OVER(PARTITION BY employeeunkid ORDER BY effectivedate DESC) as rno " & _
                    "FROM hremployee_categorization_tran " & _
                    "WHERE isvoid = 0 AND CONVERT(NVARCHAR(8),effectivedate,112) <=  CONVERT(NVARCHAR(8),GETDATE(),112) " & _
                    ") AS JB ON JB.jobunkid = hrjob_master.jobunkid AND JB.rno = 1 "
            'Gajanan [13-Feb-2019] -- Start
            'Enhancement - Add Filters IN Organization Chart.
            '"GROUP BY job_name,total_position,report_tounkid,hrjob_master.jobunkid "
            strQ &= "GROUP BY job_name,total_position,report_tounkid " & strGroupFilter & " Having 1=1 " & strFilterQuery & " "
            'Gajanan [13-Feb-2019] -- End            
            strQ &= "ORDER by parent "




            dsList = objDataOperation.ExecQuery(strQ, strListName)


            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            Return dsList

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "GetJobWiseList", mstrModuleName)
            Return Nothing
        Finally
            If dsList IsNot Nothing Then dsList.Dispose()
            dsList = Nothing
            If objDataOperation IsNot Nothing Then objDataOperation.Dispose()
            objDataOperation = Nothing

            exForce = Nothing
        End Try
    End Function

    Public Function GetInterAllocationList(ByVal strListName As String, ByVal intDepartmentgroupUnkid As Integer) As DataSet

        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception
        Dim objDataOperation As New clsDataOperation
        objDataOperation.ClearParameters()
        Try

            'strQ = "SELECT " & _
            '       "CASE hralloc_mapping_master.c_allocationid " & _
            '             "WHEN " & enAllocation.BRANCH & " THEN ST.name " & _
            '             "WHEN " & enAllocation.DEPARTMENT_GROUP & " THEN DG.name " & _
            '             "WHEN " & enAllocation.DEPARTMENT & " THEN DP.name " & _
            '             "WHEN " & enAllocation.SECTION_GROUP & " THEN SG.name " & _
            '             "WHEN " & enAllocation.SECTION & " THEN SC.name " & _
            '             "WHEN " & enAllocation.UNIT_GROUP & " THEN UG.name " & _
            '             "WHEN " & enAllocation.UNIT & " THEN UM.name " & _
            '             "WHEN " & enAllocation.TEAM & " THEN TM.name " & _
            '       "END AS name " & _
            '       ",hralloc_mapping_master.p_referenceunkid AS parent " & _
            '       ",hralloc_mapping_master.p_allocationid as parent_alloc " & _
            '       "FROM (SELECT " & _
            '                  "hralloc_mapping_tran.* " & _
            '                ",ROW_NUMBER() OVER (PARTITION BY c_referenceunkid ORDER BY mappingunkid) AS rno " & _
            '             "FROM hralloc_mapping_tran " & _
            '             "WHERE isvoid = 0) AS a " & _
            '        "LEFT JOIN hralloc_mapping_master " & _
            '             "ON a.mappingunkid = hralloc_mapping_master.mappingunkid " & _
            '             "and  hralloc_mapping_master.isvoid = 0 " & _
            '        " " & _
            '        "LEFT JOIN hrstation_master AS ST " & _
            '             "ON ST.stationunkid = a.c_referenceunkid " & _
            '                  "AND ST.isactive = 1 " & _
            '        "LEFT JOIN hrdepartment_group_master AS DG " & _
            '             "ON DG.deptgroupunkid = a.c_referenceunkid " & _
            '                  "AND DG.isactive = 1 " & _
            '        "LEFT JOIN hrdepartment_master AS DP " & _
            '             "ON DP.departmentunkid = a.c_referenceunkid " & _
            '                  "AND DP.isactive = 1 " & _
            '        "LEFT JOIN hrsectiongroup_master AS SG " & _
            '             "ON SG.sectiongroupunkid = a.c_referenceunkid " & _
            '                  "AND SG.isactive = 1 " & _
            '        "LEFT JOIN hrsection_master AS SC " & _
            '             "ON SC.sectionunkid = a.c_referenceunkid " & _
            '                  "AND SC.isactive = 1 " & _
            '        "LEFT JOIN hrunitgroup_master AS UG " & _
            '             "ON UG.unitgroupunkid = a.c_referenceunkid " & _
            '                  "AND UG.isactive = 1 " & _
            '        "LEFT JOIN hrunit_master AS UM " & _
            '             "ON UM.unitunkid = a.c_referenceunkid " & _
            '                  "AND UM.isactive = 1 " & _
            '        "LEFT JOIN hrteam_master AS TM " & _
            '             "ON TM.unitunkid = a.c_referenceunkid " & _
            '                  "AND TM.isactive = 1 " & _
            '        "WHERE rno = 1 "

            'strQ = "SELECT a.childid as id, a.childname as name ,a.parentid as parent from ( SELECT " & _
            '         "MT.c_referenceunkid as childid " & _
            '         ",AM.p_referenceunkid as parentid " & _
            '         ",SG.name as childname " & _
            '         ",DP.name as parentname " & _
            '         ",AM.p_allocationid " & _
            '         ",AM.c_allocationid " & _
            '         ",MT.mappingunkid " & _
            '         ",ROW_NUMBER() OVER (PARTITION BY c_referenceunkid ORDER BY MT.mappingunkid) AS rno " & _
            '    "FROM hralloc_mapping_master AS AM " & _
            '    "LEFT JOIN hralloc_mapping_tran AS MT ON AM.mappingunkid = MT.mappingunkid " & _
            '    "LEFT JOIN hrdepartment_master AS DP ON DP.departmentunkid = AM.p_referenceunkid and DP.isactive = 1 " & _
            '    "LEFT JOIN hrsectiongroup_master AS SG ON SG.sectiongroupunkid = MT.c_referenceunkid AND SG.isactive = 1 " & _
            '    "WHERE AM.isvoid = 0 AND AM.p_allocationid = " & enAllocation.DEPARTMENT & " AND AM.c_allocationid = " & enAllocation.SECTION_GROUP & " " & _
            '    ") as A where a.rno =  1 " & _
            '    " " & _
            '    "UNION ALL " & _
            '    " " & _
            '    "SELECT b.childid as id, b.childname as name ,b.parentid as parent FROM( " & _
            '    "SELECT " & _
            '          "MT.c_referenceunkid as childid " & _
            '         ",AM.p_referenceunkid as parentid " & _
            '         ",DP.name as childname " & _
            '         ",DG.name as parentname " & _
            '         ",AM.p_allocationid " & _
            '         ",AM.c_allocationid " & _
            '         ",ROW_NUMBER() OVER (PARTITION BY c_referenceunkid ORDER BY MT.mappingunkid) AS rno " & _
            '    "FROM hralloc_mapping_master AS AM " & _
            '    "LEFT JOIN hralloc_mapping_tran AS MT ON AM.mappingunkid = MT.mappingunkid " & _
            '    "LEFT JOIN hrdepartment_master AS DP ON DP.departmentunkid = MT.c_referenceunkid  and DP.isactive = 1 " & _
            '    "LEFT JOIN hrdepartment_group_master AS DG ON DG.deptgroupunkid =AM.p_referenceunkid AND DG.isactive = 1 " & _
            '    "WHERE AM.isvoid = 0 AND AM.p_allocationid = " & enAllocation.DEPARTMENT_GROUP & " AND AM.c_allocationid = " & enAllocation.DEPARTMENT & " " & _
            '    ") as B where b.rno =  1 " & _
            '    "UNION all " & _
            '    "SELECT c.childid as id, c.childname as name ,c.parentid as parent FROM( " & _
            '    "SELECT " & _
            '          "AM.p_referenceunkid as childid " & _
            '         ",AM.p_referenceunkid as parentid " & _
            '         ",DG.name as childname " & _
            '         ",'' as parentname " & _
            '         ",AM.p_allocationid " & _
            '         ",AM.c_allocationid " & _
            '         ",1 AS rno " & _
            '    "FROM hralloc_mapping_master AS AM " & _
            '    "LEFT JOIN hrdepartment_group_master AS DG ON DG.deptgroupunkid= AM.p_referenceunkid AND DG.isactive = 1 " & _
            '    "WHERE AM.isvoid = 0 AND AM.p_allocationid = " & enAllocation.DEPARTMENT_GROUP & " ) as c "





            strQ = "SELECT " & _
                         "a.childid AS id " & _
                       ",a.childname AS name " & _
                       ",a.parentid AS parent from ( " & _
                    "SELECT " & _
                     "MT.c_referenceunkid as childid " & _
                     ",AM.p_referenceunkid as parentid " & _
                     ",SG.name as childname " & _
                     ",DP.name as parentname " & _
                     ",AM.p_allocationid " & _
                     ",AM.c_allocationid " & _
                     ",MT.mappingunkid " & _
                "FROM hralloc_mapping_master AS AM " & _
                    "LEFT JOIN hralloc_mapping_tran AS MT " & _
                         "ON AM.mappingunkid = MT.mappingunkid " & _
                              "AND mt.isvoid = 0 " & _
                    "LEFT JOIN hrdepartment_master AS DP " & _
                         "ON DP.departmentunkid = AM.p_referenceunkid " & _
                              "AND DP.isactive = 1 " & _
                    "LEFT JOIN hrsectiongroup_master AS SG " & _
                         "ON SG.sectiongroupunkid = MT.c_referenceunkid " & _
                              "AND SG.isactive = 1 " & _
                    "WHERE AM.isvoid = 0 " & _
                    "AND AM.p_allocationid = " & enAllocation.DEPARTMENT & " " & _
                    "AND AM.c_allocationid = " & enAllocation.SECTION_GROUP & " " & _
                    ") as a " & _
                " " & _
                "UNION ALL " & _
                " " & _
                    "SELECT " & _
                         "b.childid AS id " & _
                       ",b.childname AS name " & _
                       ",b.parentid AS parent " & _
                    "FROM ( " & _
                "SELECT " & _
                      "MT.c_referenceunkid as childid " & _
                     ",AM.p_referenceunkid as parentid " & _
                     ",DP.name as childname " & _
                     ",DG.name as parentname " & _
                     ",AM.p_allocationid " & _
                     ",AM.c_allocationid " & _
                "FROM hralloc_mapping_master AS AM " & _
                    "LEFT JOIN hralloc_mapping_tran AS MT " & _
                         "ON AM.mappingunkid = MT.mappingunkid " & _
                              "AND mt.isvoid = 0 " & _
                    "LEFT JOIN hrdepartment_master AS DP " & _
                         "ON DP.departmentunkid = MT.c_referenceunkid " & _
                              "AND DP.isactive = 1 " & _
                    "LEFT JOIN hrdepartment_group_master AS DG " & _
                         "ON DG.deptgroupunkid = AM.p_referenceunkid " & _
                              "AND DG.isactive = 1 " & _
                    "WHERE AM.isvoid = 0 " & _
                    "AND AM.p_allocationid = " & enAllocation.DEPARTMENT_GROUP & " " & _
                    "AND AM.c_allocationid = " & enAllocation.DEPARTMENT & " " & _
                    "AND mt.c_referenceunkid IN (SELECT " & _
                         "DISTINCT " & _
                              "AM.p_referenceunkid AS parentid " & _
                         "FROM hralloc_mapping_master AS AM " & _
                         "LEFT JOIN hralloc_mapping_tran AS MT " & _
                              "ON AM.mappingunkid = MT.mappingunkid " & _
                              "AND mt.isvoid = 0 " & _
                         "LEFT JOIN hrdepartment_master AS DP " & _
                              "ON DP.departmentunkid = AM.p_referenceunkid " & _
                              "AND DP.isactive = 1 " & _
                         "LEFT JOIN hrsectiongroup_master AS SG " & _
                              "ON SG.sectiongroupunkid = MT.c_referenceunkid " & _
                              "AND SG.isactive = 1 " & _
                         "WHERE AM.isvoid = 0 " & _
                         "AND AM.p_allocationid = " & enAllocation.DEPARTMENT & " " & _
                         "AND AM.c_allocationid = " & enAllocation.SECTION_GROUP & " ) " & _
                    ") as b " & _
                    " " & _
                    " " & _
                "UNION all " & _
                    " " & _
                    " " & _
                    "SELECT " & _
                         "c.childid AS id " & _
                       ",c.childname AS name " & _
                       ",c.parentid AS parent " & _
                    "FROM ( " & _
                "SELECT " & _
                      "AM.p_referenceunkid as childid " & _
                     ",AM.p_referenceunkid as parentid " & _
                     ",DG.name as childname " & _
                     ",'' as parentname " & _
                     ",AM.p_allocationid " & _
                     ",AM.c_allocationid " & _
                "FROM hralloc_mapping_master AS AM " & _
                    "LEFT JOIN hrdepartment_group_master AS DG " & _
                         "ON DG.deptgroupunkid = AM.p_referenceunkid " & _
                              "AND DG.isactive = 1 " & _
                    "WHERE AM.isvoid = 0 " & _
                    "AND AM.p_allocationid = " & enAllocation.DEPARTMENT_GROUP & " " & _
                    "and AM.p_referenceunkid = @Departmentgroupid " & _
                    "AND am.p_referenceunkid IN (SELECT DISTINCT " & _
                              "AM.p_referenceunkid AS parentid " & _
                         "FROM hralloc_mapping_master AS AM " & _
                         "LEFT JOIN hralloc_mapping_tran AS MT " & _
                              "ON AM.mappingunkid = MT.mappingunkid " & _
                              "AND mt.isvoid = 0 " & _
                         "LEFT JOIN hrdepartment_master AS DP " & _
                              "ON DP.departmentunkid = MT.c_referenceunkid " & _
                              "AND DP.isactive = 1 " & _
                         "LEFT JOIN hrdepartment_group_master AS DG " & _
                              "ON DG.deptgroupunkid = AM.p_referenceunkid " & _
                              "AND DG.isactive = 1 " & _
                         "WHERE AM.isvoid = 0 " & _
                         "AND AM.p_allocationid = " & enAllocation.DEPARTMENT_GROUP & " " & _
                         "AND AM.c_allocationid = " & enAllocation.DEPARTMENT & " " & _
                         "AND mt.c_referenceunkid IN (SELECT " & _
                              "DISTINCT " & _
                                   "AM.p_referenceunkid AS parentid " & _
                              "FROM hralloc_mapping_master AS AM " & _
                              "LEFT JOIN hralloc_mapping_tran AS MT " & _
                                   "ON AM.mappingunkid = MT.mappingunkid " & _
                                   "AND mt.isvoid = 0 " & _
                              "LEFT JOIN hrdepartment_master AS DP " & _
                                   "ON DP.departmentunkid = AM.p_referenceunkid " & _
                                   "AND DP.isactive = 1 " & _
                              "LEFT JOIN hrsectiongroup_master AS SG " & _
                                   "ON SG.sectiongroupunkid = MT.c_referenceunkid " & _
                                   "AND SG.isactive = 1 " & _
                              "WHERE AM.isvoid = 0 " & _
                              "AND AM.p_allocationid = " & enAllocation.DEPARTMENT & " " & _
                              "AND AM.c_allocationid = " & enAllocation.SECTION_GROUP & ")) " & _
                    ") as c "

            objDataOperation.AddParameter("@Departmentgroupid", SqlDbType.Int, eZeeDataType.INT_SIZE, intDepartmentgroupUnkid)


            dsList = objDataOperation.ExecQuery(strQ, strListName)

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            'If dsList.Tables(0).Rows.Count > 0 Then
            '    Dim data = (From x In dsList.Tables(0).AsEnumerable Select New With {Key .Parent = x.Field(Of Integer)("parent"), .Parent_alloc = x.Field(Of Integer)("Parent_alloc")}).ToList()
            'End If

            Return dsList
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "GetInterAllocationList", mstrModuleName)
            Return Nothing
        Finally
            If dsList IsNot Nothing Then dsList.Dispose()
            dsList = Nothing
            If objDataOperation IsNot Nothing Then objDataOperation.Dispose()
            objDataOperation = Nothing

            exForce = Nothing
        End Try
    End Function

    Public Function RecursionAllocationList(ByVal Table As DataTable, ByVal AllocationId As Integer) As DataTable
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception
        Dim objDataOperation As New clsDataOperation
        Try

            strQ = "SELECT " & _
                   "CASE hrinterallocmapping_tran.allocationrefid " & _
                                               "WHEN " & enAllocation.BRANCH & "  THEN ST.name " & _
                                               "WHEN " & enAllocation.DEPARTMENT_GROUP & " THEN DG.name " & _
                                               "WHEN " & enAllocation.DEPARTMENT & " THEN DP.name " & _
                                               "WHEN " & enAllocation.SECTION_GROUP & "  THEN SG.name " & _
                                               "WHEN " & enAllocation.SECTION & " THEN SC.name " & _
                                               "WHEN " & enAllocation.UNIT_GROUP & "  THEN UG.name " & _
                                               "WHEN " & enAllocation.UNIT & "  THEN UM.name " & _
                                               "WHEN " & enAllocation.TEAM & "  THEN TM.name " & _
                   "END AS alloc " & _
                   ",hrinterallocmapping_tran.p_referenceunkid AS parent " & _
                   "from hrinterallocmapping_tran " & _
                     "LEFT JOIN hrstation_master AS ST " & _
                         "ON ST.stationunkid = hrinterallocmapping_tran.p_referenceunkid " & _
                              "AND ST.isactive = 1 " & _
                    "LEFT JOIN hrdepartment_group_master AS DG " & _
                         "ON DG.deptgroupunkid = hrinterallocmapping_tran.p_referenceunkid " & _
                              "AND DG.isactive = 1 " & _
                    "LEFT JOIN hrdepartment_master AS DP " & _
                         "ON DP.departmentunkid = hrinterallocmapping_tran.p_referenceunkid " & _
                              "AND DP.isactive = 1 " & _
                    "LEFT JOIN hrsectiongroup_master AS SG " & _
                         "ON SG.sectiongroupunkid = hrinterallocmapping_tran.p_referenceunkid " & _
                              "AND SG.isactive = 1 " & _
                    "LEFT JOIN hrsection_master AS SC " & _
                         "ON SC.sectionunkid = hrinterallocmapping_tran.p_referenceunkid " & _
                              "AND SC.isactive = 1 " & _
                    "LEFT JOIN hrunitgroup_master AS UG " & _
                         "ON UG.unitgroupunkid = hrinterallocmapping_tran.p_referenceunkid " & _
                              "AND UG.isactive = 1 " & _
                    "LEFT JOIN hrunit_master AS UM " & _
                         "ON UM.unitunkid = hrinterallocmapping_tran.p_referenceunkid " & _
                              "AND UM.isactive = 1 " & _
                    "LEFT JOIN hrteam_master AS TM " & _
                         "ON TM.unitunkid = hrinterallocmapping_tran.p_referenceunkid " & _
                              "AND TM.isactive = 1 " & _
                              "WHERE hrinterallocmapping_tran.isvoid = 0 " & _
                    "And hrinterallocmapping_tran.c_referenceunkid = @c_referenceunkid "



            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            If dsList.Tables("data").Rows.Count > 0 Then
                Dim result As DataRow() = Table.Select("id = '" & dsList.Tables("data").Rows(0)("JobId") & "'")
                If result.Length <= 0 Then
                    Dim dRow As DataRow
                    dRow = Table.NewRow

                    dRow.Item("id") = dsList.Tables("data").Rows(0)("JobId")
                    dRow.Item("name") = dsList.Tables("data").Rows(0)("Job Name")
                    dRow.Item("parent") = dsList.Tables("data").Rows(0)("parent")
                    dRow.Item("isOriginaldata") = False

                    Table.Rows.Add(dRow)
                    Table.AcceptChanges()
                End If

                If CInt(dsList.Tables("data").Rows(0)("parent")) > 0 Then
                    RecursionJob(Table, CInt(dsList.Tables("data").Rows(0)("parent")))
                End If
            End If


            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            Return Table
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "RecursionAllocationList", mstrModuleName)
            Return Nothing
        Finally
            If dsList IsNot Nothing Then dsList.Dispose()
            dsList = Nothing
            If objDataOperation IsNot Nothing Then objDataOperation.Dispose()
            objDataOperation = Nothing
        End Try

    End Function

End Class
