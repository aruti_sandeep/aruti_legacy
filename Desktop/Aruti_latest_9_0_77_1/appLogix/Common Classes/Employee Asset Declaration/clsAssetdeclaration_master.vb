﻿'************************************************************************************************************************************
'Class Name : clsAssetdeclaration_master.vb
'Purpose    :
'Date       :09/01/2012
'Written By :Sohail
'Modified   :
'************************************************************************************************************************************

Imports eZeeCommonLib
''' <summary>
''' Purpose: 
''' Developer: Sohail
''' </summary>
Public Class clsAssetdeclaration_master
    Private Const mstrModuleName = "clsAssetdeclaration_master"
    Dim objDataOperation As clsDataOperation
    Dim mstrMessage As String = ""

#Region " Private variables "
    Private mintAssetdeclarationunkid As Integer
    Private mintEmployeeunkid As Integer
    Private mdecCash As Decimal
    'Sohail (26 Mar 2012) -- Start
    'TRA - ENHANCEMENT
    'Private mstrCashExistingBank As String = String.Empty
    Private mdecCashExistingBank As Decimal
    'Sohail (26 Mar 2012) -- End
    'Private mstrNoofemployment As String = String.Empty
    'Private mstrPosition As String = String.Empty
    'Private mstrCenterforwork As String = String.Empty
    Private mdtFinyear_Start As Date
    Private mdtFinyear_End As Date
    Private mstrResources As String = String.Empty
    Private mstrDebt As String = String.Empty
    Private mblnIsfinalsaved As Boolean
    Private mintUserunkid As Integer
    Private mblnIsvoid As Boolean
    Private mintVoiduserunkid As Integer
    Private mdtVoiddatetime As Date
    Private mstrVoidreason As String = String.Empty
    Private mdtTransactiondate As Date 'Sohail (06 Apr 2012)
    'Sohail (29 Jan 2013) -- Start
    'TRA - ENHANCEMENT
    Private mdtSavedate As Date
    Private mdtFinalsavedate As Date
    Private mdtUnlockfinalsavedate As Date
    Private mdecBanktotal As Decimal
    Private mdecSharedividendtotal As Decimal
    Private mdecHousetotal As Decimal
    Private mdecParkfarmtotal As Decimal
    Private mdecVehicletotal As Decimal
    Private mdecMachinerytotal As Decimal
    Private mdecOtherbusinesstotal As Decimal
    Private mdecOtherresourcetotal As Decimal
    Private mdecDebttotal As Decimal
    'Sohail (29 Jan 2013) -- End

    Private mdtBank As DataTable = Nothing
    Private mdtShareDividend As DataTable = Nothing
    Private mdtHouseBuilding As DataTable = Nothing
    Private mdtParkFarmMines As DataTable = Nothing
    Private mdtVehicles As DataTable = Nothing
    Private mdtMachinery As DataTable = Nothing
    Private mdtOtherBusiness As DataTable = Nothing
    'Sohail (29 Jan 2013) -- Start
    'TRA - ENHANCEMENT
    Private mdtResources As DataTable = Nothing
    Private mdtDebts As DataTable = Nothing
    'Sohail (29 Jan 2013) -- End

    'Sohail (26 Mar 2012) -- Start
    'TRA - ENHANCEMENT
    Private mintLoginemployeeunkid As Integer = -1
    Private mintVoidloginemployeeunkid As Integer = -1
    'Sohail (26 Mar 2012) -- End
    Private mstrDatabaseName As String = "" 'Sohail (16 Apr 2015)
    'Hemant (24 Dec 2019) -- Start
    'ENHANCEMENT : Changes for Bind Transactions & Perfomance Enhacement.
    'Private minAuditUserid As Integer = 0
    'Private minAuditDate As DateTime
    'Private minloginemployeeunkid As Integer = 0
    'Private minClientIp As String = String.Empty
    'Private mstrHostName As String = String.Empty
    'Private mstrFormName As String = String.Empty
    'Private blnIsFromWeb As Boolean = False
    'Hemant (24 Dec 2019) -- End
#End Region

#Region " Properties "
    'S.SANDEEP [28-May-2018] -- START 
    'ISSUE/ENHANCEMENT : {Audit Trails} 
    Private mstrFormName As String = String.Empty
    Public WriteOnly Property _FormName() As String
        Set(ByVal value As String)
            mstrFormName = value
        End Set
    End Property

    Private mstrClientIP As String = ""
    Public WriteOnly Property _ClientIP() As String
        Set(ByVal value As String)
            mstrClientIP = value
        End Set
    End Property

    Private mstrHostName As String = ""
    Public WriteOnly Property _HostName() As String
        Set(ByVal value As String)
            mstrHostName = value
        End Set
    End Property

    Private mblnIsWeb As Boolean = False
    Public WriteOnly Property _FromWeb() As Boolean
        Set(ByVal value As Boolean)
            mblnIsWeb = value
        End Set
    End Property

    Private mintAuditUserId As Integer = 0
    Public WriteOnly Property _AuditUserId() As Integer
        Set(ByVal value As Integer)
            mintAuditUserId = value
        End Set
    End Property

    Private mintCompanyUnkid As Integer = 0
    Public WriteOnly Property _CompanyUnkid() As Integer
        Set(ByVal value As Integer)
            mintCompanyUnkid = value
        End Set
    End Property

    Private mdtAuditDate As DateTime = Now
    Public WriteOnly Property _AuditDate() As DateTime
        Set(ByVal value As DateTime)
            mdtAuditDate = value
        End Set
    End Property

    Private xDataOpr As clsDataOperation = Nothing
    Public Property _xDataOpr() As clsDataOperation
        Get
            Return xDataOpr
        End Get
        Set(ByVal value As clsDataOperation)
            xDataOpr = value
        End Set
    End Property
    'S.SANDEEP [28-May-2018] -- END 
    ''' <summary>
    ''' Purpose: Get Message from Class 
    ''' Modify By: Sohail
    ''' </summary>
    Public ReadOnly Property _Message() As String
        Get
            Return mstrMessage
        End Get
    End Property

    ''' <summary>
    ''' Purpose: Get or Set assetdeclarationunkid
    ''' Modify By: Sohail
    ''' </summary>
    ''' Shani(24-Aug-2015) -- Start
    ''' ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
    ''' Public Property _Assetdeclarationunkid() As Integer
    Public Property _Assetdeclarationunkid(ByVal xDatabaseName As String) As Integer
        'Shani(24-Aug-2015) -- End
        Get
            Return mintAssetdeclarationunkid
        End Get
        Set(ByVal value As Integer)
            mintAssetdeclarationunkid = value

            'Shani(24-Aug-2015) -- Start
            'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
            'Call GetData()
            Call GetData(xDatabaseName)
            'Shani(24-Aug-2015) -- End
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set employeeunkid
    ''' Modify By: Sohail
    ''' </summary>
    Public Property _Employeeunkid() As Integer
        Get
            Return mintEmployeeunkid
        End Get
        Set(ByVal value As Integer)
            mintEmployeeunkid = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set Cash
    ''' Modify By: Sohail
    ''' </summary>
    Public Property _Cash() As Decimal
        Get
            Return mdecCash
        End Get
        Set(ByVal value As Decimal)
            mdecCash = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set CashExistingBank
    ''' Modify By: Sohail
    ''' </summary>
    Public Property _CashExistingBank() As Decimal
        Get
            Return mdecCashExistingBank
        End Get
        Set(ByVal value As Decimal)
            mdecCashExistingBank = value
        End Set
    End Property

    '''' <summary>
    '''' Purpose: Get or Set noofemployment
    '''' Modify By: Sohail
    '''' </summary>
    'Public Property _Noofemployment() As String
    '    Get
    '        Return mstrNoofemployment
    '    End Get
    '    Set(ByVal value As String)
    '        mstrNoofemployment = value
    '    End Set
    'End Property

    '''' <summary>
    '''' Purpose: Get or Set position
    '''' Modify By: Sohail
    '''' </summary>
    'Public Property _Position() As String
    '    Get
    '        Return mstrPosition
    '    End Get
    '    Set(ByVal value As String)
    '        mstrPosition = value
    '    End Set
    'End Property

    '''' <summary>
    '''' Purpose: Get or Set centerforwork
    '''' Modify By: Sohail
    '''' </summary>
    'Public Property _Centerforwork() As String
    '    Get
    '        Return mstrCenterforwork
    '    End Get
    '    Set(ByVal value As String)
    '        mstrCenterforwork = value
    '    End Set
    'End Property

    ''' <summary>
    ''' Purpose: Get or Set finyear_start
    ''' Modify By: Sohail
    ''' </summary>
    Public Property _Finyear_Start() As Date
        Get
            Return mdtFinyear_Start
        End Get
        Set(ByVal value As Date)
            mdtFinyear_Start = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set finyear_end
    ''' Modify By: Sohail
    ''' </summary>
    Public Property _Finyear_End() As Date
        Get
            Return mdtFinyear_End
        End Get
        Set(ByVal value As Date)
            mdtFinyear_End = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set isfinalsaved
    ''' Modify By: Sohail
    ''' </summary>
    Public Property _Isfinalsaved() As Boolean
        Get
            Return mblnIsfinalsaved
        End Get
        Set(ByVal value As Boolean)
            mblnIsfinalsaved = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set userunkid
    ''' Modify By: Sohail
    ''' </summary>
    Public Property _Userunkid() As Integer
        Get
            Return mintUserunkid
        End Get
        Set(ByVal value As Integer)
            mintUserunkid = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set Resources
    ''' Modify By: Sohail
    ''' </summary>
    Public Property _Resources() As String
        Get
            Return mstrResources
        End Get
        Set(ByVal value As String)
            mstrResources = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set Debt
    ''' Modify By: Sohail
    ''' </summary>
    Public Property _Debt() As String
        Get
            Return mstrDebt
        End Get
        Set(ByVal value As String)
            mstrDebt = value
        End Set
    End Property


    ''' <summary>
    ''' Purpose: Get or Set isvoid
    ''' Modify By: Sohail
    ''' </summary>
    Public Property _Isvoid() As Boolean
        Get
            Return mblnIsvoid
        End Get
        Set(ByVal value As Boolean)
            mblnIsvoid = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set voiduserunkid
    ''' Modify By: Sohail
    ''' </summary>
    Public Property _Voiduserunkid() As Integer
        Get
            Return mintVoiduserunkid
        End Get
        Set(ByVal value As Integer)
            mintVoiduserunkid = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set voiddatetime
    ''' Modify By: Sohail
    ''' </summary>
    Public Property _Voiddatetime() As Date
        Get
            Return mdtVoiddatetime
        End Get
        Set(ByVal value As Date)
            mdtVoiddatetime = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set voidreason
    ''' Modify By: Sohail
    ''' </summary>
    Public Property _Voidreason() As String
        Get
            Return mstrVoidreason
        End Get
        Set(ByVal value As String)
            mstrVoidreason = value
        End Set
    End Property

    'Sohail (06 Apr 2012) -- Start
    'TRA - ENHANCEMENT
    ''' <summary>
    ''' Purpose: Get or Set voiddatetime
    ''' Modify By: Sohail
    ''' </summary>
    Public Property _TransactionDate() As Date
        Get
            Return mdtTransactiondate
        End Get
        Set(ByVal value As Date)
            mdtTransactiondate = value
        End Set
    End Property
    'Sohail (06 Apr 2012) -- End

    'Sohail (26 Mar 2012) -- Start
    'TRA - ENHANCEMENT
    Public Property _LoginEmployeeunkid() As Integer
        Get
            Return mintLoginemployeeunkid
        End Get
        Set(ByVal value As Integer)
            mintLoginemployeeunkid = value
        End Set
    End Property

    Public Property _VoidLoginEmployeeunkid() As Integer
        Get
            Return mintVoidloginemployeeunkid
        End Get
        Set(ByVal value As Integer)
            mintVoidloginemployeeunkid = value
        End Set
    End Property
    'Sohail (26 Mar 2012) -- End

    'Sohail (29 Jan 2013) -- Start
    'TRA - ENHANCEMENT
    ''' <summary>
    ''' Purpose: Get or Set savedate
    ''' Modify By: Sohail
    ''' </summary>
    Public Property _Savedate() As Date
        Get
            Return mdtSavedate
        End Get
        Set(ByVal value As Date)
            mdtSavedate = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set finalsavedate
    ''' Modify By: Sohail
    ''' </summary>
    Public Property _Finalsavedate() As Date
        Get
            Return mdtFinalsavedate
        End Get
        Set(ByVal value As Date)
            mdtFinalsavedate = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set unlockfinalsavedate
    ''' Modify By: Sohail
    ''' </summary>
    Public Property _Unlockfinalsavedate() As Date
        Get
            Return mdtUnlockfinalsavedate
        End Get
        Set(ByVal value As Date)
            mdtUnlockfinalsavedate = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set banktotal
    ''' Modify By: Sohail
    ''' </summary>
    Public Property _Banktotal() As Decimal
        Get
            Return mdecBanktotal
        End Get
        Set(ByVal value As Decimal)
            mdecBanktotal = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set sharedividendtotal
    ''' Modify By: Sohail
    ''' </summary>
    Public Property _Sharedividendtotal() As Decimal
        Get
            Return mdecSharedividendtotal
        End Get
        Set(ByVal value As Decimal)
            mdecSharedividendtotal = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set housetotal
    ''' Modify By: Sohail
    ''' </summary>
    Public Property _Housetotal() As Decimal
        Get
            Return mdecHousetotal
        End Get
        Set(ByVal value As Decimal)
            mdecHousetotal = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set parkfarmtotal
    ''' Modify By: Sohail
    ''' </summary>
    Public Property _Parkfarmtotal() As Decimal
        Get
            Return mdecParkfarmtotal
        End Get
        Set(ByVal value As Decimal)
            mdecParkfarmtotal = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set vehicletotal
    ''' Modify By: Sohail
    ''' </summary>
    Public Property _Vehicletotal() As Decimal
        Get
            Return mdecVehicletotal
        End Get
        Set(ByVal value As Decimal)
            mdecVehicletotal = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set machinerytotal
    ''' Modify By: Sohail
    ''' </summary>
    Public Property _Machinerytotal() As Decimal
        Get
            Return mdecMachinerytotal
        End Get
        Set(ByVal value As Decimal)
            mdecMachinerytotal = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set otherbusinesstotal
    ''' Modify By: Sohail
    ''' </summary>
    Public Property _Otherbusinesstotal() As Decimal
        Get
            Return mdecOtherbusinesstotal
        End Get
        Set(ByVal value As Decimal)
            mdecOtherbusinesstotal = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set otherresourcetotal
    ''' Modify By: Sohail
    ''' </summary>
    Public Property _Otherresourcetotal() As Decimal
        Get
            Return mdecOtherresourcetotal
        End Get
        Set(ByVal value As Decimal)
            mdecOtherresourcetotal = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set debttotal
    ''' Modify By: Sohail
    ''' </summary>
    Public Property _Debttotal() As Decimal
        Get
            Return mdecDebttotal
        End Get
        Set(ByVal value As Decimal)
            mdecDebttotal = value
        End Set
    End Property

    'Sohail (29 Jan 2013) -- End


    Public Property _Datasource_Bank() As DataTable
        Get
            Return mdtBank
        End Get
        Set(ByVal value As DataTable)
            mdtBank = value
        End Set
    End Property

    Public WriteOnly Property _Datasource_ShareDividend() As DataTable
        Set(ByVal value As DataTable)
            mdtShareDividend = value
        End Set
    End Property

    Public WriteOnly Property _Datasource_HouseBuilding() As DataTable
        Set(ByVal value As DataTable)
            mdtHouseBuilding = value
        End Set
    End Property

    Public WriteOnly Property _Datasource_ParkFarmMines() As DataTable
        Set(ByVal value As DataTable)
            mdtParkFarmMines = value
        End Set
    End Property

    Public WriteOnly Property _Datasource_Vehicles() As DataTable
        Set(ByVal value As DataTable)
            mdtVehicles = value
        End Set
    End Property

    Public WriteOnly Property _Datasource_Machinery() As DataTable
        Set(ByVal value As DataTable)
            mdtMachinery = value
        End Set
    End Property

    Public WriteOnly Property _Datasource_OtherBusiness() As DataTable
        Set(ByVal value As DataTable)
            mdtOtherBusiness = value
        End Set
    End Property

    'Sohail (29 Jan 2013) -- Start
    'TRA - ENHANCEMENT
    Public WriteOnly Property _Datasource_Resources() As DataTable
        Set(ByVal value As DataTable)
            mdtResources = value
        End Set
    End Property

    Public WriteOnly Property _Datasource_Debts() As DataTable
        Set(ByVal value As DataTable)
            mdtDebts = value
        End Set
    End Property
    'Sohail (29 Jan 2013) -- End

    'Sohail (16 Apr 2015) -- Start
    'Enhancement #449 - On Employee Asset Declaration employee should be able to see other Previous year asset Form.
    Public WriteOnly Property _DatabaseName() As String
        Set(ByVal value As String)
            mstrDatabaseName = value
        End Set
    End Property
    'Sohail (16 Apr 2015) -- End

    'Hemant (24 Dec 2019) -- Start
    'ENHANCEMENT : Changes for Bind Transactions & Perfomance Enhacement.

    'Public Property _AuditUserid() As Integer
    '    Get
    '        Return minAuditUserid
    '    End Get
    '    Set(ByVal value As Integer)
    '        minAuditUserid = value
    '    End Set
    'End Property

    'Public Property _AuditDate() As DateTime
    '    Get
    '        Return minAuditDate
    '    End Get
    '    Set(ByVal value As DateTime)
    '        minAuditDate = value
    '    End Set
    'End Property

    'Public Property _ClientIp() As String
    '    Get
    '        Return minClientIp
    '    End Get
    '    Set(ByVal value As String)
    '        minClientIp = value
    '    End Set
    'End Property

    'Public Property _HostName() As String
    '    Get
    '        Return mstrHostName
    '    End Get
    '    Set(ByVal value As String)
    '        mstrHostName = value
    '    End Set
    'End Property

    'Public Property _FormName() As String
    '    Get
    '        Return mstrFormName
    '    End Get
    '    Set(ByVal value As String)
    '        mstrFormName = value
    '    End Set
    'End Property

    'Public Property _IsFromWeb() As Boolean
    '    Get
    '        Return blnIsFromWeb
    '    End Get
    '    Set(ByVal value As Boolean)
    '        blnIsFromWeb = value
    '    End Set
    'End Property
    'Hemant (24 Dec 2019) -- End

#End Region

    ''' <summary>
    ''' Modify By: Sohail
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>
    ''' Shani(24-Aug-2015) -- Start
    ''' ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
    ''' Public Sub GetData()
    Public Sub GetData(ByVal xDatabaseName As String)
        'Shani(24-Aug-2015) -- End
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        objDataOperation = New clsDataOperation

        'Sohail (16 Apr 2015) -- Start
        'Enhancement #449 - On Employee Asset Declaration employee should be able to see other Previous year asset Form.

        'Shani(24-Aug-2015) -- Start
        'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
        'If mstrDatabaseName.Trim = "" Then mstrDatabaseName = FinancialYear._Object._DatabaseName
        mstrDatabaseName = xDatabaseName
        'Shani(24-Aug-2015) -- End

        'Sohail (16 Apr 2015) -- End

        Try
            strQ = "SELECT " & _
              "  assetdeclarationunkid " & _
              ", employeeunkid " & _
              ", ISNULL(cash, 0) AS cash " & _
              ", ISNULL(cashexistingbank, 0) AS cashexistingbank " & _
              ", finyear_start " & _
              ", finyear_end " & _
              ", resources " & _
              ", debt " & _
              ", isfinalsaved " & _
              ", userunkid " & _
              ", isvoid " & _
              ", voiduserunkid " & _
              ", voiddatetime " & _
              ", voidreason " & _
              ", ISNULL(transactiondate, GETDATE()) AS transactiondate " & _
              ", savedate " & _
              ", finalsavedate " & _
              ", unlockfinalsavedate " & _
              ", ISNULL(banktotal, 0) AS banktotal " & _
              ", ISNULL(sharedividendtotal, 0) AS sharedividendtotal " & _
              ", ISNULL(housetotal, 0) AS housetotal " & _
              ", ISNULL(parkfarmtotal, 0) AS parkfarmtotal " & _
              ", ISNULL(vehicletotal, 0) AS vehicletotal " & _
              ", ISNULL(machinerytotal, 0) AS machinerytotal " & _
              ", ISNULL(otherbusinesstotal, 0) AS otherbusinesstotal " & _
              ", ISNULL(otherresourcetotal, 0) AS otherresourcetotal " & _
              ", ISNULL(debttotal, 0) AS debttotal " & _
             "FROM " & mstrDatabaseName & "..hrassetdeclaration_master " & _
             "WHERE assetdeclarationunkid = @assetdeclarationunkid "
            'Sohail (16 Apr 2015) - [mstrDatabaseName]
            'Sohail (29 Jan 2013) - [savedate, finalsavedate, unlockfinalsavedate, banktotal, sharedividendtotal, housetotal, parkfarmtotal, vehicletotal, machinerytotal, otherbusinesstotal, otherresourcetotal, debttotal]
            'Sohail (06 Apr 2012) - [transactiondate]

            objDataOperation.AddParameter("@assetdeclarationunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintAssetdeclarationunkid.ToString)

            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            For Each dtRow As DataRow In dsList.Tables(0).Rows
                mintAssetdeclarationunkid = CInt(dtRow.Item("assetdeclarationunkid"))
                mintEmployeeunkid = CInt(dtRow.Item("employeeunkid"))
                mdecCash = CDec(dtRow.Item("cash"))
                mdecCashExistingBank = CDec(dtRow.Item("cashexistingbank").ToString)
                'mstrNoofemployment = dtRow.Item("noofemployment").ToString
                'mstrPosition = dtRow.Item("position").ToString
                'mstrCenterforwork = dtRow.Item("centerforwork").ToString
                mdtFinyear_Start = dtRow.Item("finyear_start")
                mdtFinyear_End = dtRow.Item("finyear_end")
                mstrResources = dtRow.Item("resources").ToString
                mstrDebt = dtRow.Item("debt").ToString
                mblnIsfinalsaved = CBool(dtRow.Item("isfinalsaved"))
                mintUserunkid = CInt(dtRow.Item("userunkid"))
                mblnIsvoid = CBool(dtRow.Item("isvoid"))
                mintVoiduserunkid = CInt(dtRow.Item("voiduserunkid"))
                If IsDBNull(dtRow.Item("voiddatetime")) = True Then
                    mdtVoiddatetime = Nothing
                Else
                    mdtVoiddatetime = dtRow.Item("voiddatetime")
                End If
                mstrVoidreason = dtRow.Item("voidreason").ToString
                mdtTransactiondate = dtRow.Item("transactiondate") 'Sohail (06 Apr 2012)
                'Sohail (29 Jan 2013) -- Start
                'TRA - ENHANCEMENT
                If IsDBNull(dtRow.Item("savedate")) = True Then
                    mdtSavedate = Nothing
                Else
                    mdtSavedate = dtRow.Item("savedate")
                End If
                If IsDBNull(dtRow.Item("finalsavedate")) = True Then
                    mdtFinalsavedate = Nothing
                Else
                    mdtFinalsavedate = dtRow.Item("finalsavedate")
                End If
                If IsDBNull(dtRow.Item("unlockfinalsavedate")) = True Then
                    mdtUnlockfinalsavedate = Nothing
                Else
                    mdtUnlockfinalsavedate = dtRow.Item("unlockfinalsavedate")
                End If
                mdecBanktotal = dtRow.Item("banktotal")
                mdecSharedividendtotal = dtRow.Item("sharedividendtotal")
                mdecHousetotal = dtRow.Item("housetotal")
                mdecParkfarmtotal = dtRow.Item("parkfarmtotal")
                mdecVehicletotal = dtRow.Item("vehicletotal")
                mdecMachinerytotal = dtRow.Item("machinerytotal")
                mdecOtherbusinesstotal = dtRow.Item("otherbusinesstotal")
                mdecOtherresourcetotal = dtRow.Item("otherresourcetotal")
                mdecDebttotal = dtRow.Item("debttotal")
                'Sohail (29 Jan 2013) -- End

                Exit For
            Next
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: GetData; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
    End Sub

    '' <summary>
    '' Modify By: Sohail
    '' </summary>
    '' <param name="strTableName"></param>
    '' <param name="dicDatabaseNames">Dictionary(Of DatabaseName[i.e. tran_], YearName[i.e. 2015-2015])</param>
    '' <param name="intAssetDeclarationUnkID"></param>
    '' <param name="intEmployeeUnkID"></param>
    '' <param name="blnNotIncludeFinalSaved"></param>
    '' <param name="strIncludeInactiveEmployee"></param>
    '' <param name="strEmployeeAsOnDate"></param>
    '' <param name="strUserAccessLevelFilterString"></param>
    '' <param name="strCarRegno"></param>
    '' <returns></returns>
    '' <remarks></remarks>
    '' <purpose> Assign all Property variable </purpose>
    'Shani(24-Aug-2015) -- Start
    'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
    'Public Shared Function GetList(ByVal strTableName As String _
    '                                   , ByVal dicDatabaseNames As Dictionary(Of String, String) _
    '                                   , Optional ByVal intAssetDeclarationUnkID As Integer = 0 _
    '                                   , Optional ByVal intEmployeeUnkID As Integer = 0 _
    '                                   , Optional ByVal blnNotIncludeFinalSaved As Boolean = False _
    '                                   , Optional ByVal strIncludeInactiveEmployee As String = "" _
    '                                   , Optional ByVal strEmployeeAsOnDate As String = "" _
    '                                   , Optional ByVal strUserAccessLevelFilterString As String = "" _
    '                                   , Optional ByVal strCarRegno As String = "" _
    '                                   , Optional ByVal strFilter As String = "" _
    '                                   , Optional ByVal strOrderByFields As String = "" _
    '                                   ) As DataSet
    '    '                          'Sohail (16 Apr 2015) - [arrDatabaseNames, strFilter, strOrderByFields] 
    '    '                          'Anjan (05 Feb 2013) included car reg.no filter.
    '    '                          'Sohail (23 Apr 2012) - [strIncludeInactiveEmployee,strEmployeeAsOnDate,strUserAccessLevelFilterString] 

    '    Dim dsList As DataSet = Nothing
    '    Dim strQ As String = ""
    '    Dim exForce As Exception
    '    Dim objDataOperation As New clsDataOperation 'Sohail (07 Mar 2012)
    '    Dim strInnerQ As String = "" 'Sohail (16 Apr 2015)

    '    objDataOperation = New clsDataOperation

    '    Try

    '        'Sohail (16 Apr 2015) -- Start
    '        'Enhancement #449 - On Employee Asset Declaration employee should be able to see other Previous year asset Form.
    '        'strQ = "SELECT  DISTINCT hrassetdeclaration_master.assetdeclarationunkid " & _
    '        '              ", hrassetdeclaration_master.employeeunkid " & _
    '        '              ", ISNULL(hremployee_master.firstname,'')+' '+ISNULL(hremployee_master.othername,'')+' '+ISNULL(hremployee_master.surname,'') AS employeename " & _
    '        '              ", ISNULL(hremployee_master.employeecode, '') AS noofemployment " & _
    '        '              ", ISNULL(hrjob_master.job_name, '') AS position " & _
    '        '              ", ISNULL(hrdepartment_master.name, '') AS centerforwork " & _
    '        '              ", ISNULL(hrclasses_master.name, '') AS workstation " & _
    '        '              ", hrassetdeclaration_master.finyear_start " & _
    '        '              ", ISNULL(hrassetdeclaration_master.cash, 0) AS cash " & _
    '        '              ", ISNULL(hrassetdeclaration_master.cashexistingbank, 0) AS cashexistingbank " & _
    '        '              ", hrassetdeclaration_master.finyear_end " & _
    '        '              ", hrassetdeclaration_master.resources " & _
    '        '              ", hrassetdeclaration_master.debt " & _
    '        '              ", hrassetdeclaration_master.isfinalsaved " & _
    '        '              ", hrassetdeclaration_master.userunkid " & _
    '        '              ", hrassetdeclaration_master.isvoid " & _
    '        '              ", hrassetdeclaration_master.voiduserunkid " & _
    '        '              ", hrassetdeclaration_master.voiddatetime " & _
    '        '              ", hrassetdeclaration_master.voidreason " & _
    '        '              ", ISNULL(hrassetdeclaration_master.transactiondate, GETDATE()) AS transactiondate " & _
    '        '              ", savedate " & _
    '        '              ", finalsavedate " & _
    '        '              ", unlockfinalsavedate " & _
    '        '              ", ISNULL(banktotal, 0) AS banktotal " & _
    '        '              ", ISNULL(sharedividendtotal, 0) AS sharedividendtotal " & _
    '        '              ", ISNULL(housetotal, 0) AS housetotal " & _
    '        '              ", ISNULL(parkfarmtotal, 0) AS parkfarmtotal " & _
    '        '              ", ISNULL(vehicletotal, 0) AS vehicletotal " & _
    '        '              ", ISNULL(machinerytotal, 0) AS machinerytotal " & _
    '        '              ", ISNULL(otherbusinesstotal, 0) AS otherbusinesstotal " & _
    '        '              ", ISNULL(otherresourcetotal, 0) AS otherresourcetotal " & _
    '        '              ", ISNULL(debttotal, 0) AS debttotal " & _
    '        '        "FROM    hrassetdeclaration_master " & _
    '        '                "LEFT JOIN hremployee_master ON hrassetdeclaration_master.employeeunkid = hremployee_master.employeeunkid " & _
    '        '                "LEFT JOIN hrjob_master ON hremployee_master.jobunkid = hrjob_master.jobunkid " & _
    '        '                "LEFT JOIN hrdepartment_master ON hremployee_master.departmentunkid = hrdepartment_master.departmentunkid " & _
    '        '                "LEFT JOIN hrclasses_master ON hremployee_master.classunkid = hrclasses_master.classesunkid "
    '        ''Anjan [ 05 Feb 2013 ] -- Start
    '        ''ENHANCEMENT : TRA CHANGES
    '        'If strCarRegno <> "" Then
    '        '    strQ &= " LEFT JOIN hrasset_vehicles_tran ON hrasset_vehicles_tran.assetdeclarationunkid = hrassetdeclaration_master.assetdeclarationunkid "
    '        'End If
    '        ''Anjan [ 05 Feb 2013 ] -- End

    '        ''Sohail (29 Jan 2013) - [savedate, finalsavedate, unlockfinalsavedate, banktotal, sharedividendtotal, housetotal, parkfarmtotal, vehicletotal, machinerytotal, otherbusinesstotal, otherresourcetotal, debttotal]

    '        ''Sohail (23 Apr 2012) -- Start
    '        ''TRA - ENHANCEMENT
    '        'If strIncludeInactiveEmployee = "" Then strIncludeInactiveEmployee = ConfigParameter._Object._IsIncludeInactiveEmp.ToString
    '        'If CBool(strIncludeInactiveEmployee) = False Then
    '        '    'If ConfigParameter._Object._IsIncludeInactiveEmp = False Then
    '        '    'Sohail (23 Apr 2012) -- End
    '        '    strQ &= " AND CONVERT(CHAR(8),hremployee_master.appointeddate,112) <= @enddate " & _
    '        '               " AND ISNULL(CONVERT(CHAR(8),hremployee_master.termination_from_date,112),@startdate) >= @startdate " & _
    '        '               " AND ISNULL(CONVERT(CHAR(8),hremployee_master.termination_to_date,112),@startdate) >= @startdate " & _
    '        '               " AND ISNULL(CONVERT(CHAR(8),hremployee_master.empl_enddate,112), @startdate) >= @startdate " 'S.SANDEEP [ 15 MAY 2012 empl_enddate ] -- START -- END

    '        '    'Sohail (23 Apr 2012) -- Start
    '        '    'TRA - ENHANCEMENT
    '        '    'objDataOperation.AddParameter("@startdate", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, ConfigParameter._Object._EmployeeAsOnDate)
    '        '    'objDataOperation.AddParameter("@enddate", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, ConfigParameter._Object._EmployeeAsOnDate)
    '        '    objDataOperation.AddParameter("@startdate", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, IIf(strEmployeeAsOnDate = "", ConfigParameter._Object._EmployeeAsOnDate, strEmployeeAsOnDate))
    '        '    objDataOperation.AddParameter("@enddate", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, IIf(strEmployeeAsOnDate = "", ConfigParameter._Object._EmployeeAsOnDate, strEmployeeAsOnDate))
    '        '    'Sohail (23 Apr 2012) -- End
    '        'End If

    '        'If intAssetDeclarationUnkID > 0 Then
    '        '    strQ &= " AND hrassetdeclaration_master.assetdeclarationunkid = @assetdeclarationunkid "
    '        '    objDataOperation.AddParameter("@assetdeclarationunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intAssetDeclarationUnkID)
    '        'End If

    '        'If intEmployeeUnkID > 0 Then
    '        '    strQ &= " AND hremployee_master.employeeunkid = @employeeunkid "
    '        '    objDataOperation.AddParameter("@employeeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intEmployeeUnkID)
    '        'End If

    '        ''Sohail (26 Mar 2012) -- Start
    '        ''TRA - ENHANCEMENT
    '        'If blnNotIncludeFinalSaved = True Then
    '        '    strQ &= " AND hrassetdeclaration_master.isfinalsaved = 0 "
    '        'End If
    '        ''Sohail (26 Mar 2012) -- End

    '        ''Sohail (23 Apr 2012) -- Start
    '        ''TRA - ENHANCEMENT
    '        ''Select Case ConfigParameter._Object._UserAccessModeSetting
    '        ''    Case enAllocation.BRANCH
    '        ''        If UserAccessLevel._AccessLevel.Length > 0 Then
    '        ''            strQ &= " AND  hremployee_master.stationunkid IN (" & UserAccessLevel._AccessLevel & ") "
    '        ''        End If
    '        ''    Case enAllocation.DEPARTMENT_GROUP
    '        ''        If UserAccessLevel._AccessLevel.Length > 0 Then
    '        ''            strQ &= " AND  hremployee_master.deptgroupunkid IN (" & UserAccessLevel._AccessLevel & ") "
    '        ''        End If
    '        ''    Case enAllocation.DEPARTMENT
    '        ''        If UserAccessLevel._AccessLevel.Length > 0 Then
    '        ''            strQ &= " AND  hremployee_master.departmentunkid IN (" & UserAccessLevel._AccessLevel & ") "
    '        ''        End If
    '        ''    Case enAllocation.SECTION_GROUP
    '        ''        If UserAccessLevel._AccessLevel.Length > 0 Then
    '        ''            strQ &= " AND  hremployee_master.sectiongroupunkid IN (" & UserAccessLevel._AccessLevel & ") "
    '        ''        End If
    '        ''    Case enAllocation.SECTION
    '        ''        If UserAccessLevel._AccessLevel.Length > 0 Then
    '        ''            strQ &= " AND  hremployee_master.sectionunkid IN (" & UserAccessLevel._AccessLevel & ") "
    '        ''        End If
    '        ''    Case enAllocation.UNIT_GROUP
    '        ''        If UserAccessLevel._AccessLevel.Length > 0 Then
    '        ''            strQ &= " AND  hremployee_master.unitgroupunkid IN (" & UserAccessLevel._AccessLevel & ") "
    '        ''        End If
    '        ''    Case enAllocation.UNIT
    '        ''        If UserAccessLevel._AccessLevel.Length > 0 Then
    '        ''            strQ &= " AND  hremployee_master.unitunkid IN (" & UserAccessLevel._AccessLevel & ") "
    '        ''        End If
    '        ''    Case enAllocation.TEAM
    '        ''        If UserAccessLevel._AccessLevel.Length > 0 Then
    '        ''            strQ &= " AND  hremployee_master.teamunkid IN (" & UserAccessLevel._AccessLevel & ") "
    '        ''        End If
    '        ''    Case enAllocation.JOB_GROUP
    '        ''        If UserAccessLevel._AccessLevel.Length > 0 Then
    '        ''            strQ &= " AND  hremployee_master.jobgroupunkid IN (" & UserAccessLevel._AccessLevel & ") "
    '        ''        End If
    '        ''    Case enAllocation.JOBS
    '        ''        If UserAccessLevel._AccessLevel.Length > 0 Then
    '        ''            strQ &= "  AND hremployee_master.jobunkid IN (" & UserAccessLevel._AccessLevel & ")  "
    '        ''        End If
    '        ''End Select
    '        'If strUserAccessLevelFilterString = "" Then
    '        '    strQ &= UserAccessLevel._AccessLevelFilterString
    '        'Else
    '        '    strQ &= strUserAccessLevelFilterString
    '        'End If
    '        ''Sohail (23 Apr 2012) -- End
    '        If dicDatabaseNames Is Nothing OrElse dicDatabaseNames.Count <= 0 Then
    '            dicDatabaseNames = New Dictionary(Of String, String)
    '            dicDatabaseNames.Add(FinancialYear._Object._DatabaseName, FinancialYear._Object._FinancialYear_Name)
    '        End If

    '        strQ = "SELECT  A.assetdeclarationunkid  " & _
    '                      ", A.employeeunkid " & _
    '                      ", A.employeename " & _
    '                      ", A.noofemployment " & _
    '                      ", A.position " & _
    '                      ", A.centerforwork " & _
    '                      ", A.workstation " & _
    '                      ", A.finyear_start " & _
    '                      ", A.cash " & _
    '                      ", A.cashexistingbank " & _
    '                      ", A.finyear_end " & _
    '                      ", A.resources " & _
    '                      ", A.debt " & _
    '                      ", A.isfinalsaved " & _
    '                      ", A.userunkid " & _
    '                      ", A.isvoid " & _
    '                      ", A.voiduserunkid " & _
    '                      ", A.voiddatetime " & _
    '                      ", A.voidreason " & _
    '                      ", A.transactiondate " & _
    '                      ", A.savedate " & _
    '                      ", A.finalsavedate " & _
    '                      ", A.unlockfinalsavedate " & _
    '                      ", A.banktotal " & _
    '                      ", A.sharedividendtotal " & _
    '                      ", A.housetotal " & _
    '                      ", A.parkfarmtotal " & _
    '                      ", A.vehicletotal " & _
    '                      ", A.machinerytotal " & _
    '                      ", A.otherbusinesstotal " & _
    '                      ", A.otherresourcetotal " & _
    '                      ", A.debttotal " & _
    '                      ", A.databasename " & _
    '                      ", A.yearname " & _
    '                "FROM    (  "

    '        For Each key As KeyValuePair(Of String, String) In dicDatabaseNames

    '            strInnerQ &= "UNION ALL " & _
    '                              "SELECT  DISTINCT " & _
    '                                        "hrassetdeclaration_master.assetdeclarationunkid  " & _
    '              ", hrassetdeclaration_master.employeeunkid " & _
    '                                      ", ISNULL(hremployee_master.firstname, '') + ' ' " & _
    '                                        "+ ISNULL(hremployee_master.othername, '') + ' ' " & _
    '                                        "+ ISNULL(hremployee_master.surname, '') AS employeename  " & _
    '              ", ISNULL(hremployee_master.employeecode, '') AS noofemployment " & _
    '              ", ISNULL(hrjob_master.job_name, '') AS position " & _
    '              ", ISNULL(hrdepartment_master.name, '') AS centerforwork " & _
    '              ", ISNULL(hrclasses_master.name, '') AS workstation " & _
    '              ", hrassetdeclaration_master.finyear_start " & _
    '              ", ISNULL(hrassetdeclaration_master.cash, 0) AS cash " & _
    '              ", ISNULL(hrassetdeclaration_master.cashexistingbank, 0) AS cashexistingbank " & _
    '              ", hrassetdeclaration_master.finyear_end " & _
    '              ", hrassetdeclaration_master.resources " & _
    '              ", hrassetdeclaration_master.debt " & _
    '              ", hrassetdeclaration_master.isfinalsaved " & _
    '              ", hrassetdeclaration_master.userunkid " & _
    '              ", hrassetdeclaration_master.isvoid " & _
    '              ", hrassetdeclaration_master.voiduserunkid " & _
    '              ", hrassetdeclaration_master.voiddatetime " & _
    '              ", hrassetdeclaration_master.voidreason " & _
    '                                      ", ISNULL(hrassetdeclaration_master.transactiondate " & _
    '                                             ", GETDATE()) AS transactiondate " & _
    '              ", savedate " & _
    '              ", finalsavedate " & _
    '              ", unlockfinalsavedate " & _
    '              ", ISNULL(banktotal, 0) AS banktotal " & _
    '              ", ISNULL(sharedividendtotal, 0) AS sharedividendtotal " & _
    '              ", ISNULL(housetotal, 0) AS housetotal " & _
    '              ", ISNULL(parkfarmtotal, 0) AS parkfarmtotal " & _
    '              ", ISNULL(vehicletotal, 0) AS vehicletotal " & _
    '              ", ISNULL(machinerytotal, 0) AS machinerytotal " & _
    '              ", ISNULL(otherbusinesstotal, 0) AS otherbusinesstotal " & _
    '              ", ISNULL(otherresourcetotal, 0) AS otherresourcetotal " & _
    '              ", ISNULL(debttotal, 0) AS debttotal " & _
    '                                      ", '" & key.Key & "' AS databasename " & _
    '                                      ", '" & key.Value & "' AS yearname " & _
    '                              "FROM      " & key.Key & "..hrassetdeclaration_master " & _
    '                                        "LEFT JOIN " & key.Key & "..hremployee_master ON hrassetdeclaration_master.employeeunkid = hremployee_master.employeeunkid " & _
    '                                        "LEFT JOIN " & key.Key & "..hrjob_master ON hremployee_master.jobunkid = hrjob_master.jobunkid " & _
    '                                        "LEFT JOIN " & key.Key & "..hrdepartment_master ON hremployee_master.departmentunkid = hrdepartment_master.departmentunkid " & _
    '                                        "LEFT JOIN " & key.Key & "..hrclasses_master ON hremployee_master.classunkid = hrclasses_master.classesunkid "

    'If strCarRegno <> "" Then
    '                strInnerQ &= " LEFT JOIN " & key.Key & "..hrasset_vehicles_tran ON hrasset_vehicles_tran.assetdeclarationunkid = hrassetdeclaration_master.assetdeclarationunkid "
    'End If

    '            strInnerQ &= "      WHERE     ISNULL(hrassetdeclaration_master.isvoid, 0) = 0 "

    '            If strCarRegno <> "" Then
    '                strInnerQ &= " AND hrasset_vehicles_tran.regno like '%" & strCarRegno & "%' "
    '            End If

    'If strIncludeInactiveEmployee = "" Then strIncludeInactiveEmployee = ConfigParameter._Object._IsIncludeInactiveEmp.ToString
    'If CBool(strIncludeInactiveEmployee) = False Then
    '                strInnerQ &= " AND CONVERT(CHAR(8),hremployee_master.appointeddate,112) <= @enddate " & _
    '               " AND ISNULL(CONVERT(CHAR(8),hremployee_master.termination_from_date,112),@startdate) >= @startdate " & _
    '               " AND ISNULL(CONVERT(CHAR(8),hremployee_master.termination_to_date,112),@startdate) >= @startdate " & _
    '                           " AND ISNULL(CONVERT(CHAR(8),hremployee_master.empl_enddate,112), @startdate) >= @startdate "
    '            End If

    '            If intAssetDeclarationUnkID > 0 Then
    '                strInnerQ &= " AND hrassetdeclaration_master.assetdeclarationunkid = @assetdeclarationunkid "

    '            End If

    '            If intEmployeeUnkID > 0 Then
    '                strInnerQ &= " AND hremployee_master.employeeunkid = @employeeunkid "
    '            End If

    '            If blnNotIncludeFinalSaved = True Then
    '                strInnerQ &= " AND hrassetdeclaration_master.isfinalsaved = 0 "
    '            End If

    '            If strUserAccessLevelFilterString = "" Then
    '                strInnerQ &= UserAccessLevel._AccessLevelFilterString
    '            Else
    '                strInnerQ &= strUserAccessLevelFilterString
    '            End If

    '            If strFilter.Trim <> "" Then
    '                strInnerQ &= " AND " & strFilter
    '            End If

    '        Next

    '        strQ &= strInnerQ.Substring(9)

    '        strQ &= "        ) AS A "

    '        strQ &= " ORDER BY A.yearname DESC "

    '        If strOrderByFields.Trim <> "" Then
    '            strQ &= ", " & strOrderByFields
    '        End If

    '        If CBool(strIncludeInactiveEmployee) = False Then
    '    objDataOperation.AddParameter("@startdate", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, IIf(strEmployeeAsOnDate = "", ConfigParameter._Object._EmployeeAsOnDate, strEmployeeAsOnDate))
    '    objDataOperation.AddParameter("@enddate", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, IIf(strEmployeeAsOnDate = "", ConfigParameter._Object._EmployeeAsOnDate, strEmployeeAsOnDate))
    'End If

    'If intAssetDeclarationUnkID > 0 Then
    '    objDataOperation.AddParameter("@assetdeclarationunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intAssetDeclarationUnkID)
    'End If

    'If intEmployeeUnkID > 0 Then
    '    objDataOperation.AddParameter("@employeeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intEmployeeUnkID)
    'End If

    '        dsList = objDataOperation.ExecQuery(strQ, strTableName)

    '        If objDataOperation.ErrorMessage <> "" Then
    '            exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
    '            Throw exForce
    'End If

    '    Catch ex As Exception
    '        Throw New Exception(ex.Message & "; Procedure Name: GetList; Module Name: " & mstrModuleName)
    '    Finally
    '        exForce = Nothing
    '        If dsList IsNot Nothing Then dsList.Dispose()
    '        objDataOperation = Nothing
    '    End Try
    '    Return dsList
    'End Function
    Public Shared Function GetList(ByVal xUserUnkid As Integer, _
                                   ByVal xCompanyUnkid As Integer, _
                                   ByVal xUserModeSetting As String, _
                                   ByVal xOnlyApproved As Boolean, _
                                   ByVal xIncludeIn_ActiveEmployee As Boolean, _
                                   ByVal strTableName As String, _
                                   ByVal dicDatabaseNames As Dictionary(Of String, String), _
                                   ByVal dicDatabaseEndDate As Dictionary(Of String, String), _
                                   ByVal dicDatabasYearUnkId As Dictionary(Of String, Integer), _
                                   Optional ByVal intAssetDeclarationUnkID As Integer = 0, _
                                   Optional ByVal intEmployeeUnkID As Integer = 0, _
                                   Optional ByVal blnNotIncludeFinalSaved As Boolean = False, _
                                   Optional ByVal strEmployeeAsOnDate As String = "", _
                                   Optional ByVal strUserAccessLevelFilterString As String = "", _
                                   Optional ByVal strCarRegno As String = "", _
                                   Optional ByVal strFilter As String = "", _
                                   Optional ByVal strOrderByFields As String = "", _
                                   Optional ByVal blnApplyUserAccessFilter As Boolean = True _
                                   ) As DataSet
        'Sohail (06 Oct 2017) - [blnApplyUserAccessFilter]

        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception
        Dim objDataOperation As New clsDataOperation 'Sohail (07 Mar 2012)
        Dim strInnerQ As String = ""

        objDataOperation = New clsDataOperation

        Try
            If dicDatabaseNames Is Nothing OrElse dicDatabaseNames.Count <= 0 Then
                dicDatabaseNames = New Dictionary(Of String, String)
                dicDatabaseNames.Add(FinancialYear._Object._DatabaseName, FinancialYear._Object._FinancialYear_Name)
            End If

            Dim xDateJoinQry, xDateFilterQry, xUACQry, xUACFiltrQry As String
            xDateJoinQry = "" : xDateFilterQry = "" : xUACQry = "" : xUACFiltrQry = ""

            strQ = "SELECT  A.assetdeclarationunkid  " & _
                          ", A.employeeunkid " & _
                          ", A.employeename " & _
                          ", A.noofemployment " & _
                          ", A.position " & _
                          ", A.centerforwork " & _
                          ", A.workstation " & _
                          ", A.finyear_start " & _
                          ", A.cash " & _
                          ", A.cashexistingbank " & _
                          ", A.finyear_end " & _
                          ", A.resources " & _
                          ", A.debt " & _
                          ", A.isfinalsaved " & _
                          ", A.userunkid " & _
                          ", A.isvoid " & _
                          ", A.voiduserunkid " & _
                          ", A.voiddatetime " & _
                          ", A.voidreason " & _
                          ", A.transactiondate " & _
                          ", A.savedate " & _
                          ", A.finalsavedate " & _
                          ", A.unlockfinalsavedate " & _
                          ", A.banktotal " & _
                          ", A.sharedividendtotal " & _
                          ", A.housetotal " & _
                          ", A.parkfarmtotal " & _
                          ", A.vehicletotal " & _
                          ", A.machinerytotal " & _
                          ", A.otherbusinesstotal " & _
                          ", A.otherresourcetotal " & _
                          ", A.debttotal " & _
                          ", A.databasename " & _
                          ", A.yearname " & _
                          ", A.yearunkid " & _
                    "FROM    (  "
            'Sohail (04 Jul 2019) - [yearunkid]

            For Each key As KeyValuePair(Of String, String) In dicDatabaseNames
                Dim intYearId As Integer = 0
                Dim dtPeriodStart As Date = Nothing
                Dim dtPeriodEnd As Date = Nothing



                If dicDatabaseEndDate.ContainsKey(key.Key) AndAlso dicDatabasYearUnkId.ContainsKey(key.Key) Then
                    intYearId = dicDatabasYearUnkId(key.Key)
                    dtPeriodStart = eZeeDate.convertDate(dicDatabaseEndDate(key.Key).ToString().Split("|")(0))
                    dtPeriodEnd = eZeeDate.convertDate(dicDatabaseEndDate(key.Key).ToString().Split("|")(1))
                End If

                xDateJoinQry = "" : xDateFilterQry = "" : xUACQry = "" : xUACFiltrQry = ""
                Call GetDatesFilterString(xDateJoinQry, xDateFilterQry, dtPeriodStart, dtPeriodEnd, , , key.Key)
                'Sohail (06 Oct 2017) -- Start
                'TRA Issue - 68.1 / 70.1 - Asset Declaration is not coming on ESS.
                'Call NewAccessLevelFilterString(xUACQry, xUACFiltrQry, dtPeriodEnd, xOnlyApproved, key.Key, xUserUnkid, xCompanyUnkid, intYearId, xUserModeSetting)
                If blnApplyUserAccessFilter = True Then
                Call NewAccessLevelFilterString(xUACQry, xUACFiltrQry, dtPeriodEnd, xOnlyApproved, key.Key, xUserUnkid, xCompanyUnkid, intYearId, xUserModeSetting)
                End If
                'Sohail (06 Oct 2017) -- End

                strInnerQ &= "UNION ALL " & _
                                  "SELECT  DISTINCT " & _
                                            "hrassetdeclaration_master.assetdeclarationunkid  " & _
                          ", hrassetdeclaration_master.employeeunkid " & _
                                          ", ISNULL(hremployee_master.firstname, '') + ' ' " & _
                                            "+ ISNULL(hremployee_master.othername, '') + ' ' " & _
                                            "+ ISNULL(hremployee_master.surname, '') AS employeename  " & _
                          ", ISNULL(hremployee_master.employeecode, '') AS noofemployment " & _
                             "      , ISNULL(JM.job_name, '') AS position " & _
                             "      , ISNULL(DM.name, '') AS centerforwork " & _
                             "      , ISNULL(CM.name, '') AS workstation " & _
                          ", hrassetdeclaration_master.finyear_start " & _
                          ", ISNULL(hrassetdeclaration_master.cash, 0) AS cash " & _
                          ", ISNULL(hrassetdeclaration_master.cashexistingbank, 0) AS cashexistingbank " & _
                          ", hrassetdeclaration_master.finyear_end " & _
                          ", hrassetdeclaration_master.resources " & _
                          ", hrassetdeclaration_master.debt " & _
                          ", hrassetdeclaration_master.isfinalsaved " & _
                          ", hrassetdeclaration_master.userunkid " & _
                          ", hrassetdeclaration_master.isvoid " & _
                          ", hrassetdeclaration_master.voiduserunkid " & _
                          ", hrassetdeclaration_master.voiddatetime " & _
                          ", hrassetdeclaration_master.voidreason " & _
                                          ", ISNULL(hrassetdeclaration_master.transactiondate " & _
                                                 ", GETDATE()) AS transactiondate " & _
                          ", savedate " & _
                          ", finalsavedate " & _
                          ", unlockfinalsavedate " & _
                          ", ISNULL(banktotal, 0) AS banktotal " & _
                          ", ISNULL(sharedividendtotal, 0) AS sharedividendtotal " & _
                          ", ISNULL(housetotal, 0) AS housetotal " & _
                          ", ISNULL(parkfarmtotal, 0) AS parkfarmtotal " & _
                          ", ISNULL(vehicletotal, 0) AS vehicletotal " & _
                          ", ISNULL(machinerytotal, 0) AS machinerytotal " & _
                          ", ISNULL(otherbusinesstotal, 0) AS otherbusinesstotal " & _
                          ", ISNULL(otherresourcetotal, 0) AS otherresourcetotal " & _
                          ", ISNULL(debttotal, 0) AS debttotal " & _
                                          ", '" & key.Key & "' AS databasename " & _
                                          ", '" & key.Value & "' AS yearname " & _
                                          ", " & intYearId & " AS yearunkid " & _
                                  "FROM      " & key.Key & "..hrassetdeclaration_master " & _
                                            "LEFT JOIN " & key.Key & "..hremployee_master ON hrassetdeclaration_master.employeeunkid = hremployee_master.employeeunkid " & _
                             "      LEFT JOIN " & _
                             "          ( " & _
                             "              SELECT " & _
                             "                  departmentunkid " & _
                             "                  ,classunkid " & _
                             "                  ,employeeunkid " & _
                             "                  ,ROW_NUMBER()OVER(PARTITION BY employeeunkid ORDER BY effectivedate DESC) AS rno " & _
                             "              FROM " & key.Key & "..hremployee_transfer_tran " & _
                             "              WHERE isvoid = 0 AND CONVERT(CHAR(8),effectivedate,112) <= '" & eZeeDate.convertDate(dtPeriodEnd) & "' " & _
                             "          ) AS Alloc ON Alloc.employeeunkid = " & key.Key & "..hremployee_master.employeeunkid AND Alloc.rno = 1 " & _
                             "      LEFT JOIN " & key.Key & "..hrdepartment_master AS DM ON Alloc.departmentunkid = DM.departmentunkid " & _
                             "      LEFT JOIN " & key.Key & "..hrclasses_master AS CM ON Alloc.classunkid = CM.classesunkid " & _
                             "      LEFT JOIN " & _
                             "          ( " & _
                             "              SELECT " & _
                             "                  jobunkid " & _
                             "                  ,employeeunkid " & _
                             "                  ,ROW_NUMBER()OVER(PARTITION BY employeeunkid ORDER BY effectivedate DESC) AS rno " & _
                             "              FROM " & key.Key & "..hremployee_categorization_tran " & _
                             "              WHERE isvoid = 0 AND CONVERT(CHAR(8),effectivedate,112) <= '" & eZeeDate.convertDate(dtPeriodEnd) & "' " & _
                             "          ) AS Jobs ON Jobs.employeeunkid = " & key.Key & "..hremployee_master.employeeunkid AND Jobs.rno = 1 " & _
                             "      LEFT JOIN " & key.Key & "..hrjob_master AS JM ON Jobs.jobunkid = JM.jobunkid "
                'Sohail (04 Jul 2019) - [yearunkid]

                If xDateJoinQry.Trim.Length > 0 Then
                    strInnerQ &= xDateJoinQry
                End If

                If xUACQry.Trim.Length > 0 Then
                    strInnerQ &= xUACQry
                End If

                If strCarRegno <> "" Then
                    strInnerQ &= " LEFT JOIN " & key.Key & "..hrasset_vehicles_tran ON hrasset_vehicles_tran.assetdeclarationunkid = hrassetdeclaration_master.assetdeclarationunkid "
                End If

                strInnerQ &= "      WHERE     ISNULL(hrassetdeclaration_master.isvoid, 0) = 0 "

                If xIncludeIn_ActiveEmployee = False Then
                    If xDateFilterQry.Trim.Length > 0 Then
                        strInnerQ &= xDateFilterQry
                    End If
                End If

                If xUACFiltrQry.Trim.Length > 0 Then
                    strInnerQ &= " AND " & xUACFiltrQry
                End If

                If strCarRegno <> "" Then
                    strInnerQ &= " AND hrasset_vehicles_tran.regno like '%" & strCarRegno & "%' "
                End If

                If intAssetDeclarationUnkID > 0 Then
                    strInnerQ &= " AND hrassetdeclaration_master.assetdeclarationunkid = @assetdeclarationunkid "

                End If

                If intEmployeeUnkID > 0 Then
                    strInnerQ &= " AND hremployee_master.employeeunkid = @employeeunkid "
                End If

                If blnNotIncludeFinalSaved = True Then
                    strInnerQ &= " AND hrassetdeclaration_master.isfinalsaved = 0 "
                End If

                If strFilter.Trim <> "" Then
                    strInnerQ &= " AND " & strFilter
                End If

            Next

            strQ &= strInnerQ.Substring(9)

            strQ &= "        ) AS A "

            strQ &= " ORDER BY A.yearname DESC "

            If strOrderByFields.Trim <> "" Then
                strQ &= ", " & strOrderByFields
            End If

            If intAssetDeclarationUnkID > 0 Then
                objDataOperation.AddParameter("@assetdeclarationunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intAssetDeclarationUnkID)
            End If

            If intEmployeeUnkID > 0 Then
                objDataOperation.AddParameter("@employeeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intEmployeeUnkID)
            End If

            dsList = objDataOperation.ExecQuery(strQ, strTableName)

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: GetList; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
        Return dsList
    End Function
    'Shani(24-Aug-2015) -- End




    'Sohail (29 Jan 2013) -- Start
    'TRA - ENHANCEMENT

    'Public Function InsertData() As Boolean

    '    Dim objDataOperation As clsDataOperation

    '    Dim dsList As DataSet = Nothing
    '    Dim strQ As String = ""
    '    Dim exForce As Exception
    '    Dim blnFlag As Boolean = False
    '    Dim intAssetDeclarationUnkID As Integer = 0
    '    Dim blnChildExist As Boolean = False

    '    objDataOperation = New clsDataOperation
    '    objDataOperation.BindTransaction()

    '    Try
    '        If isExist(mintEmployeeunkid, , intAssetDeclarationUnkID) = True Then

    '            If VoidAssetDeclarationMaster(intAssetDeclarationUnkID, mintUserunkid, ConfigParameter._Object._CurrentDateAndTime, "") = False Then
    '                objDataOperation.ReleaseTransaction(False)
    '                Return False
    '            End If    '            

    '            Dim objAssetBank As New clsAsset_bank_tran
    '            dsList = objAssetBank.GetList("Bank", intAssetDeclarationUnkID)
    '            If dsList.Tables("Bank").Rows.Count > 0 Then
    '                blnChildExist = True
    '                If objAssetBank.VoidByAssetDeclarationUnkID(intAssetDeclarationUnkID, mintUserunkid, ConfigParameter._Object._CurrentDateAndTime, "", 2) = False Then
    '                    objDataOperation.ReleaseTransaction(False)
    '                    Return False
    '                End If
    '            End If


    '            Dim objAssetShareDividend As New clsAsset_sharedividend_tran
    '            dsList = objAssetShareDividend.GetList("Share", intAssetDeclarationUnkID)
    '            If dsList.Tables("Share").Rows.Count > 0 Then
    '                blnChildExist = True
    '                If objAssetShareDividend.VoidByAssetDeclarationUnkID(intAssetDeclarationUnkID, mintUserunkid, ConfigParameter._Object._CurrentDateAndTime, "", 2) = False Then
    '                    objDataOperation.ReleaseTransaction(False)
    '                    Return False
    '                End If
    '            End If


    '            Dim objAssetHouseBuilding As New clsAsset_housebuilding_tran
    '            dsList = objAssetHouseBuilding.GetList("House", intAssetDeclarationUnkID)
    '            If dsList.Tables("House").Rows.Count > 0 Then
    '                blnChildExist = True
    '                If objAssetHouseBuilding.VoidByAssetDeclarationUnkID(intAssetDeclarationUnkID, mintUserunkid, ConfigParameter._Object._CurrentDateAndTime, "", 2) = False Then
    '                    objDataOperation.ReleaseTransaction(False)
    '                    Return False
    '                End If
    '            End If


    '            Dim objAssetParkFarm As New clsAsset_parkfarmmines_tran
    '            dsList = objAssetParkFarm.GetList("Park", intAssetDeclarationUnkID)
    '            If dsList.Tables("Park").Rows.Count > 0 Then
    '                blnChildExist = True
    '                If objAssetParkFarm.VoidByAssetDeclarationUnkID(intAssetDeclarationUnkID, mintUserunkid, ConfigParameter._Object._CurrentDateAndTime, "", 2) = False Then
    '                    objDataOperation.ReleaseTransaction(False)
    '                    Return False
    '                End If
    '            End If


    '            Dim objAssetVehicles As New clsAsset_vehicles_tran
    '            dsList = objAssetVehicles.GetList("Vehicle", intAssetDeclarationUnkID)
    '            If dsList.Tables("Vehicle").Rows.Count > 0 Then
    '                blnChildExist = True
    '                If objAssetVehicles.VoidByAssetDeclarationUnkID(intAssetDeclarationUnkID, mintUserunkid, ConfigParameter._Object._CurrentDateAndTime, "", 2) = False Then
    '                    objDataOperation.ReleaseTransaction(False)
    '                    Return False
    '                End If
    '            End If


    '            Dim objAssetMachinery As New clsAsset_machinery_tran
    '            dsList = objAssetMachinery.GetList("Machinery", intAssetDeclarationUnkID)
    '            If dsList.Tables("Machinery").Rows.Count > 0 Then
    '                blnChildExist = True
    '                If objAssetMachinery.VoidByAssetDeclarationUnkID(intAssetDeclarationUnkID, mintUserunkid, ConfigParameter._Object._CurrentDateAndTime, "", 2) = False Then
    '                    objDataOperation.ReleaseTransaction(False)
    '                    Return False
    '                End If
    '            End If


    '            Dim objAssetOtherBusiness As New clsAsset_otherbusiness_tran
    '            dsList = objAssetOtherBusiness.GetList("Business", intAssetDeclarationUnkID)
    '            If dsList.Tables("Business").Rows.Count > 0 Then
    '                blnChildExist = True
    '                If objAssetOtherBusiness.VoidByAssetDeclarationUnkID(intAssetDeclarationUnkID, mintUserunkid, ConfigParameter._Object._CurrentDateAndTime, "", 2) = False Then
    '                    objDataOperation.ReleaseTransaction(False)
    '                    Return False
    '                End If
    '            End If
    '            
    '            '---------------------------
    '            If blnChildExist = False Then
    '                'Sohail (29 Jan 2013) -- Start
    '                'TRA - ENHANCEMENT
    '                'If clsCommonATLog.Insert_TranAtLog(objDataOperation, "hrassetdeclaration_master", "assetdeclarationunkid", intAssetDeclarationUnkID, "", "", 0, 3, 0) = False Then
    '                '    objDataOperation.ReleaseTransaction(False)
    '                '    Return False
    '                'End If
    '                If clsCommonATLog.IsTableDataUpdate("atcommon_tranlog", "hrassetdeclaration_master", intAssetDeclarationUnkID, "assetdeclarationunkid", 2, objDataOperation) = True Then
    '                    If clsCommonATLog.Insert_TranAtLog(objDataOperation, "hrassetdeclaration_master", "assetdeclarationunkid", intAssetDeclarationUnkID, "", "", 0, 2, 0) = False Then
    '                        objDataOperation.ReleaseTransaction(False)
    '                        Return False
    '                    End If
    '                End If
    '                'Sohail (29 Jan 2013) -- End
    '            End If
    '            
    '        End If

    '        blnFlag = Insert()
    '        If blnFlag = False Then
    '            objDataOperation.ReleaseTransaction(False)
    '            Return False
    '        End If


    '        blnChildExist = False
    '        If mdtBank IsNot Nothing AndAlso mdtBank.Rows.Count > 0 Then
    '            blnChildExist = True
    '            Dim objAssetBank As New clsAsset_bank_tran
    '            objAssetBank._Assetdeclarationunkid = mintAssetdeclarationunkid
    '            objAssetBank._Userunkid = mintUserunkid 'Sohail (07 Aug 2012)
    '            objAssetBank._Datasource = mdtBank
    '            If objAssetBank.InserByDataTable() = False Then
    '                objDataOperation.ReleaseTransaction(False)
    '                Return False
    '            End If
    '        End If

    '        If mdtShareDividend IsNot Nothing AndAlso mdtShareDividend.Rows.Count > 0 Then
    '            blnChildExist = True
    '            Dim objAssetShareDividend As New clsAsset_sharedividend_tran
    '            objAssetShareDividend._Assetdeclarationunkid = mintAssetdeclarationunkid
    '            objAssetShareDividend._Userunkid = mintUserunkid 'Sohail (07 Aug 2012)
    '            objAssetShareDividend._Datasource = mdtShareDividend
    '            If objAssetShareDividend.InserByDataTable() = False Then
    '                objDataOperation.ReleaseTransaction(False)
    '                Return False
    '            End If
    '        End If

    '        If mdtHouseBuilding IsNot Nothing AndAlso mdtHouseBuilding.Rows.Count > 0 Then
    '            blnChildExist = True
    '            Dim objAssetHouseBuilding As New clsAsset_housebuilding_tran
    '            objAssetHouseBuilding._Assetdeclarationunkid = mintAssetdeclarationunkid
    '            objAssetHouseBuilding._Userunkid = mintUserunkid 'Sohail (07 Aug 2012)
    '            objAssetHouseBuilding._Datasource = mdtHouseBuilding
    '            If objAssetHouseBuilding.InserByDataTable() = False Then
    '                objDataOperation.ReleaseTransaction(False)
    '                Return False
    '            End If
    '        End If

    '        If mdtParkFarmMines IsNot Nothing AndAlso mdtParkFarmMines.Rows.Count > 0 Then
    '            blnChildExist = True
    '            Dim objAssetParkFarm As New clsAsset_parkfarmmines_tran
    '            objAssetParkFarm._Assetdeclarationunkid = mintAssetdeclarationunkid
    '            objAssetParkFarm._Userunkid = mintUserunkid 'Sohail (07 Aug 2012)
    '            objAssetParkFarm._Datasource = mdtParkFarmMines
    '            If objAssetParkFarm.InserByDataTable() = False Then
    '                objDataOperation.ReleaseTransaction(False)
    '                Return False
    '            End If
    '        End If

    '        If mdtVehicles IsNot Nothing AndAlso mdtVehicles.Rows.Count > 0 Then
    '            blnChildExist = True
    '            Dim objAssetVehicles As New clsAsset_vehicles_tran
    '            objAssetVehicles._Assetdeclarationunkid = mintAssetdeclarationunkid
    '            objAssetVehicles._Userunkid = mintUserunkid 'Sohail (07 Aug 2012)
    '            objAssetVehicles._Datasource = mdtVehicles
    '            If objAssetVehicles.InserByDataTable() = False Then
    '                objDataOperation.ReleaseTransaction(False)
    '                Return False
    '            End If
    '        End If

    '        If mdtMachinery IsNot Nothing AndAlso mdtMachinery.Rows.Count > 0 Then
    '            blnChildExist = True
    '            Dim objAssetMachinery As New clsAsset_machinery_tran
    '            objAssetMachinery._Assetdeclarationunkid = mintAssetdeclarationunkid
    '            objAssetMachinery._Userunkid = mintUserunkid 'Sohail (07 Aug 2012)
    '            objAssetMachinery._Datasource = mdtMachinery
    '            If objAssetMachinery.InserByDataTable() = False Then
    '                objDataOperation.ReleaseTransaction(False)
    '                Return False
    '            End If
    '        End If

    '        If mdtOtherBusiness IsNot Nothing AndAlso mdtOtherBusiness.Rows.Count > 0 Then
    '            blnChildExist = True
    '            Dim objAssetOtherBusiness As New clsAsset_otherbusiness_tran
    '            objAssetOtherBusiness._Assetdeclarationunkid = mintAssetdeclarationunkid
    '            objAssetOtherBusiness._Userunkid = mintUserunkid 'Sohail (07 Aug 2012)
    '            objAssetOtherBusiness._Datasource = mdtOtherBusiness
    '            If objAssetOtherBusiness.InserByDataTable() = False Then
    '                objDataOperation.ReleaseTransaction(False)
    '                Return False
    '            End If

    '        End If
    '        
    '        '---------------------------
    '        If blnChildExist = False Then
    '            If clsCommonATLog.Insert_TranAtLog(objDataOperation, "hrassetdeclaration_master", "assetdeclarationunkid", mintAssetdeclarationunkid, "", "", 0, 1, 0) = False Then
    '                objDataOperation.ReleaseTransaction(False)
    '                Return False
    '            End If
    '        End If

    '        objDataOperation.ReleaseTransaction(True)

    '        Return True
    '    Catch ex As Exception
    '        objDataOperation.ReleaseTransaction(False)
    '        Throw New Exception(ex.Message & "; Procedure Name: Insert; Module Name: " & mstrModuleName)
    '        Return False
    '    Finally
    '        exForce = Nothing
    '        If dsList IsNot Nothing Then dsList.Dispose()
    '        objDataOperation = Nothing
    '    End Try
    'End Function

    'Shani(24-Aug-2015) -- Start
    'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
    'Public Function InsertData() As Boolean
    Public Function InsertData(ByVal xDatabaseName As String, ByVal xCurrentDatetTime As DateTime) As Boolean
        'Shani(24-Aug-2015) -- End

        Dim objDataOperation As clsDataOperation

        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception
        Dim blnFlag As Boolean = False
        Dim intAssetDeclarationUnkID As Integer = 0
        Dim blnChildTableChanged As Boolean = False
        Dim blnIsUpdate As Boolean = False
        Dim decBankTotal As Decimal = 0
        Dim decShareDividendTotal As Decimal = 0
        Dim decHouseTotal As Decimal = 0
        Dim decParkFarmTotal As Decimal = 0
        Dim decVehicleTotal As Decimal = 0
        Dim decMachineryTotal As Decimal = 0
        Dim decOtherBusinessTotal As Decimal = 0
        Dim decOtherResourceTotal As Decimal = 0
        Dim decDebtTotal As Decimal = 0
        Dim dtOld As DataTable

        objDataOperation = New clsDataOperation
        objDataOperation.BindTransaction()

        Try
            intAssetDeclarationUnkID = mintAssetdeclarationunkid

            If isExist(mintEmployeeunkid, , intAssetDeclarationUnkID, objDataOperation) = True Then
                'Hemant (24 Dec 2019) -- [objDataOperation]
                blnIsUpdate = True

                'Sohail (11 Feb 2015) -- Start
                'ISSUE : mintAssetdeclarationunkid was remaining -1 when record is EXIST due to clicking Save button 2 times if save process taking more times.
                mintAssetdeclarationunkid = intAssetDeclarationUnkID
                'Sohail (11 Feb 2015) -- End

                If Update(, objDataOperation) = False Then
                    'Hemant (24 Dec 2019) -- [objDataOperation]
                    objDataOperation.ReleaseTransaction(False)
                    Return False
                End If

            Else

                'Hemant (24 Dec 2019) -- Start
                'ENHANCEMENT : Changes for Bind Transactions & Perfomance Enhacement.
                'blnFlag = Insert()
                blnFlag = Insert(objDataOperation)
                'Hemant (24 Dec 2019) -- End
                If blnFlag = False Then
                    objDataOperation.ReleaseTransaction(False)
                    Return False
                End If

            End If


            If mdtBank IsNot Nothing Then
                Dim objAssetBank As New clsAsset_bank_tran

                'Shani(24-Aug-2015) -- Start
                'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
                'objAssetBank._Assetdeclarationunkid = mintAssetdeclarationunkid
                'Hemant (24 Dec 2019) -- Start
                'ENHANCEMENT : Changes for Bind Transactions & Perfomance Enhacement.
                'objAssetBank._Assetdeclarationunkid(xDatabaseName) = mintAssetdeclarationunkid
                objAssetBank.GetData(xDatabaseName, mintAssetdeclarationunkid, objDataOperation)
                'Hemant (24 Dec 2019) -- End
                'Shani(24-Aug-2015) -- End

                dtOld = objAssetBank._Datasource
                objAssetBank._Userunkid = mintUserunkid
                objAssetBank._Datasource = mdtBank
                With objAssetBank
                    ._FormName = mstrFormName
                    ._LoginEmployeeunkid = mintLoginemployeeunkid
                    ._ClientIP = mstrClientIP
                    ._HostName = mstrHostName
                    ._FromWeb = mblnIsWeb
                    ._AuditUserId = mintAuditUserId
                    ._CompanyUnkid = mintCompanyUnkid
                    ._AuditDate = mdtAuditDate
                End With
                If objAssetBank.InserByDataTable(blnChildTableChanged, dtOld, decBankTotal, xCurrentDatetTime, objDataOperation) = False Then
                    'Hemant (24 Dec 2019) -- [objDataOperation]
                    objDataOperation.ReleaseTransaction(False)
                    Return False
                End If
            End If



            If mdtShareDividend IsNot Nothing Then
                Dim objAssetShareDividend As New clsAsset_sharedividend_tran

                'Shani(24-Aug-2015) -- Start
                'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
                'objAssetShareDividend._Assetdeclarationunkid = mintAssetdeclarationunkid
                'Hemant (24 Dec 2019) -- Start
                'ENHANCEMENT : Changes for Bind Transactions & Perfomance Enhacement.
                'objAssetShareDividend._Assetdeclarationunkid(xDatabaseName) = mintAssetdeclarationunkid
                objAssetShareDividend.GetData(xDatabaseName, mintAssetdeclarationunkid, objDataOperation)
                'Hemant (24 Dec 2019) -- End
                'Shani(24-Aug-2015) -- End

                dtOld = objAssetShareDividend._Datasource
                objAssetShareDividend._Userunkid = mintUserunkid
                objAssetShareDividend._Datasource = mdtShareDividend
                With objAssetShareDividend
                    ._FormName = mstrFormName
                    ._LoginEmployeeunkid = mintLoginemployeeunkid
                    ._ClientIP = mstrClientIP
                    ._HostName = mstrHostName
                    ._FromWeb = mblnIsWeb
                    ._AuditUserId = mintAuditUserId
                    ._CompanyUnkid = mintCompanyUnkid
                    ._AuditDate = mdtAuditDate
                End With
                If objAssetShareDividend.InserByDataTable(blnChildTableChanged, dtOld, decShareDividendTotal, xCurrentDatetTime, objDataOperation) = False Then
                    'Hemant (24 Dec 2019) -- [objDataOperation]
                    objDataOperation.ReleaseTransaction(False)
                    Return False
                End If
            End If

            If mdtHouseBuilding IsNot Nothing Then
                Dim objAssetHouseBuilding As New clsAsset_housebuilding_tran

                'Shani(24-Aug-2015) -- Start
                'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
                'objAssetHouseBuilding._Assetdeclarationunkid = mintAssetdeclarationunkid
                'Hemant (24 Dec 2019) -- Start
                'ENHANCEMENT : Changes for Bind Transactions & Perfomance Enhacement.
                'objAssetHouseBuilding._Assetdeclarationunkid(xDatabaseName) = mintAssetdeclarationunkid
                objAssetHouseBuilding.GetData(xDatabaseName, mintAssetdeclarationunkid, objDataOperation)
                'Hemant (24 Dec 2019) -- End
                'Shani(24-Aug-2015) -- End

                dtOld = objAssetHouseBuilding._Datasource
                objAssetHouseBuilding._Userunkid = mintUserunkid
                objAssetHouseBuilding._Datasource = mdtHouseBuilding
                With objAssetHouseBuilding
                    ._FormName = mstrFormName
                    ._LoginEmployeeunkid = mintLoginemployeeunkid
                    ._ClientIP = mstrClientIP
                    ._HostName = mstrHostName
                    ._FromWeb = mblnIsWeb
                    ._AuditUserId = mintAuditUserId
                    ._CompanyUnkid = mintCompanyUnkid
                    ._AuditDate = mdtAuditDate
                End With
                If objAssetHouseBuilding.InserByDataTable(blnChildTableChanged, dtOld, decHouseTotal, xCurrentDatetTime, objDataOperation) = False Then
                    'Hemant (24 Dec 2019) -- [objDataOperation]
                    objDataOperation.ReleaseTransaction(False)
                    Return False
                End If
            End If

            If mdtParkFarmMines IsNot Nothing Then
                Dim objAssetParkFarm As New clsAsset_parkfarmmines_tran

                'Shani(24-Aug-2015) -- Start
                'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
                'objAssetParkFarm._Assetdeclarationunkid() = mintAssetdeclarationunkid
                'Hemant (24 Dec 2019) -- Start
                'ENHANCEMENT : Changes for Bind Transactions & Perfomance Enhacement.
                'objAssetParkFarm._Assetdeclarationunkid(xDatabaseName) = mintAssetdeclarationunkid
                objAssetParkFarm.GetData(xDatabaseName, mintAssetdeclarationunkid, objDataOperation)
                'Hemant (24 Dec 2019) -- End
                'Shani(24-Aug-2015) -- End

                dtOld = objAssetParkFarm._Datasource
                objAssetParkFarm._Userunkid = mintUserunkid
                objAssetParkFarm._Datasource = mdtParkFarmMines
                With objAssetParkFarm
                    ._FormName = mstrFormName
                    ._LoginEmployeeunkid = mintLoginemployeeunkid
                    ._ClientIP = mstrClientIP
                    ._HostName = mstrHostName
                    ._FromWeb = mblnIsWeb
                    ._AuditUserId = mintAuditUserId
                    ._CompanyUnkid = mintCompanyUnkid
                    ._AuditDate = mdtAuditDate
                End With
                If objAssetParkFarm.InserByDataTable(blnChildTableChanged, dtOld, decParkFarmTotal, xCurrentDatetTime, objDataOperation) = False Then
                    'Hemant (24 Dec 2019) -- [objDataOperation]

                    objDataOperation.ReleaseTransaction(False)
                    Return False
                End If
            End If

            If mdtVehicles IsNot Nothing Then
                Dim objAssetVehicles As New clsAsset_vehicles_tran

                'Shani(24-Aug-2015) -- Start
                'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
                'objAssetVehicles._Assetdeclarationunkid = mintAssetdeclarationunkid
                'Hemant (24 Dec 2019) -- Start
                'ENHANCEMENT : Changes for Bind Transactions & Perfomance Enhacement.
                'objAssetVehicles._Assetdeclarationunkid(xDatabaseName) = mintAssetdeclarationunkid
                objAssetVehicles.GetData(xDatabaseName, mintAssetdeclarationunkid, objDataOperation)
                'Hemant (24 Dec 2019) -- End
                'Shani(24-Aug-2015) -- End

                dtOld = objAssetVehicles._Datasource
                objAssetVehicles._Userunkid = mintUserunkid
                objAssetVehicles._Datasource = mdtVehicles
                With objAssetVehicles
                    ._FormName = mstrFormName
                    ._LoginEmployeeunkid = mintLoginemployeeunkid
                    ._ClientIP = mstrClientIP
                    ._HostName = mstrHostName
                    ._FromWeb = mblnIsWeb
                    ._AuditUserId = mintAuditUserId
                    ._CompanyUnkid = mintCompanyUnkid
                    ._AuditDate = mdtAuditDate
                End With
                If objAssetVehicles.InserByDataTable(blnChildTableChanged, dtOld, decVehicleTotal, xCurrentDatetTime, objDataOperation) = False Then
                    'Hemant (24 Dec 2019) -- [objDataOperation]
                    'Shani(24-Aug-2015) -- End
                    objDataOperation.ReleaseTransaction(False)
                    Return False
                End If
            End If

            If mdtMachinery IsNot Nothing Then
                Dim objAssetMachinery As New clsAsset_machinery_tran

                'Shani(24-Aug-2015) -- Start
                'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
                'objAssetMachinery._Assetdeclarationunkid = mintAssetdeclarationunkid
                'Hemant (24 Dec 2019) -- Start
                'ENHANCEMENT : Changes for Bind Transactions & Perfomance Enhacement.
                'objAssetMachinery._Assetdeclarationunkid(xDatabaseName) = mintAssetdeclarationunkid
                objAssetMachinery.GetData(xDatabaseName, mintAssetdeclarationunkid, objDataOperation)
                'Hemant (24 Dec 2019) -- End
                'Shani(24-Aug-2015) -- End

                dtOld = objAssetMachinery._Datasource
                objAssetMachinery._Userunkid = mintUserunkid
                objAssetMachinery._Datasource = mdtMachinery
                With objAssetMachinery
                    ._FormName = mstrFormName
                    ._LoginEmployeeunkid = mintLoginemployeeunkid
                    ._ClientIP = mstrClientIP
                    ._HostName = mstrHostName
                    ._FromWeb = mblnIsWeb
                    ._AuditUserId = mintAuditUserId
                    ._CompanyUnkid = mintCompanyUnkid
                    ._AuditDate = mdtAuditDate
                End With
               If objAssetMachinery.InserByDataTable(blnChildTableChanged, dtOld, decMachineryTotal, xCurrentDatetTime, objDataOperation) = False Then
                    'Hemant (24 Dec 2019) -- [objDataOperation]
                    objDataOperation.ReleaseTransaction(False)
                    Return False
                End If
            End If

            If mdtOtherBusiness IsNot Nothing Then
                Dim objAssetOtherBusiness As New clsAsset_otherbusiness_tran

                'Shani(24-Aug-2015) -- Start
                'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
                'objAssetOtherBusiness._Assetdeclarationunkid = mintAssetdeclarationunkid
                'Hemant (24 Dec 2019) -- Start
                'ENHANCEMENT : Changes for Bind Transactions & Perfomance Enhacement.
                'objAssetOtherBusiness._Assetdeclarationunkid(xDatabaseName) = mintAssetdeclarationunkid
                objAssetOtherBusiness.GetData(xDatabaseName, mintAssetdeclarationunkid, objDataOperation)
                'Hemant (24 Dec 2019) -- End
                'Shani(24-Aug-2015) -- End

                dtOld = objAssetOtherBusiness._Datasource
                objAssetOtherBusiness._Userunkid = mintUserunkid
                objAssetOtherBusiness._Datasource = mdtOtherBusiness
                With objAssetOtherBusiness
                    ._FormName = mstrFormName
                    ._LoginEmployeeunkid = mintLoginemployeeunkid
                    ._ClientIP = mstrClientIP
                    ._HostName = mstrHostName
                    ._FromWeb = mblnIsWeb
                    ._AuditUserId = mintAuditUserId
                    ._CompanyUnkid = mintCompanyUnkid
                    ._AuditDate = mdtAuditDate
                End With
                If objAssetOtherBusiness.InserByDataTable(blnChildTableChanged, dtOld, decOtherBusinessTotal, xCurrentDatetTime, objDataOperation) = False Then
                    'Hemant (24 Dec 2019) -- [objDataOperation]
                    'Shani(24-Aug-2015) -- End
                    objDataOperation.ReleaseTransaction(False)
                    Return False
                End If

            End If

            If mdtResources IsNot Nothing Then
                Dim objAssetResources As New clsAsset_otherresources_tran

                'Shani(24-Aug-2015) -- Start
                'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
                'objAssetResources._Assetdeclarationunkid = mintAssetdeclarationunkid
                'Hemant (24 Dec 2019) -- Start
                'ENHANCEMENT : Changes for Bind Transactions & Perfomance Enhacement.
                'objAssetResources._Assetdeclarationunkid(xDatabaseName) = mintAssetdeclarationunkid
                objAssetResources.GetData(xDatabaseName, mintAssetdeclarationunkid, objDataOperation)
                'Hemant (24 Dec 2019) -- End
                'Shani(24-Aug-2015) -- End

                dtOld = objAssetResources._Datasource
                objAssetResources._Userunkid = mintUserunkid
                objAssetResources._Datasource = mdtResources
                With objAssetResources
                    ._FormName = mstrFormName
                    ._LoginEmployeeunkid = mintLoginemployeeunkid
                    ._ClientIP = mstrClientIP
                    ._HostName = mstrHostName
                    ._FromWeb = mblnIsWeb
                    ._AuditUserId = mintAuditUserId
                    ._CompanyUnkid = mintCompanyUnkid
                    ._AuditDate = mdtAuditDate
                End With
            If objAssetResources.InserByDataTable(blnChildTableChanged, dtOld, decOtherResourceTotal, xCurrentDatetTime, objDataOperation) = False Then
                    'Hemant (24 Dec 2019) -- [objDataOperation]
                    'Shani(24-Aug-2015) -- End
                    objDataOperation.ReleaseTransaction(False)
                    Return False
                End If
            End If

            If mdtDebts IsNot Nothing Then
                Dim objAssetDebts As New clsAsset_debts_tran

                'Shani(24-Aug-2015) -- Start
                'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
                'objAssetDebts._Assetdeclarationunkid = mintAssetdeclarationunkid
                'Hemant (24 Dec 2019) -- Start
                'ENHANCEMENT : Changes for Bind Transactions & Perfomance Enhacement.
                'objAssetDebts._Assetdeclarationunkid(xDatabaseName) = mintAssetdeclarationunkid
                objAssetDebts.GetData(xDatabaseName, mintAssetdeclarationunkid, objDataOperation)
                'Hemant (24 Dec 2019) -- End
                'Shani(24-Aug-2015) -- End

                dtOld = objAssetDebts._Datasource
                objAssetDebts._Userunkid = mintUserunkid
                objAssetDebts._Datasource = mdtDebts
                With objAssetDebts
                    ._FormName = mstrFormName
                    ._LoginEmployeeunkid = mintLoginemployeeunkid
                    ._ClientIP = mstrClientIP
                    ._HostName = mstrHostName
                    ._FromWeb = mblnIsWeb
                    ._AuditUserId = mintAuditUserId
                    ._CompanyUnkid = mintCompanyUnkid
                    ._AuditDate = mdtAuditDate
                End With
               'If objAssetDebts.InserByDataTable(blnChildTableChanged, dtOld, decDebtTotal) = False Then
                If objAssetDebts.InserByDataTable(blnChildTableChanged, dtOld, decDebtTotal, xCurrentDatetTime, objDataOperation) = False Then
                    'Hemant (24 Dec 2019) -- [objDataOperation]
                    objDataOperation.ReleaseTransaction(False)
                    Return False
                End If
            End If





            '******     UPDATE ALL CHILD TABLE TOTAL IN ASSET_DECLARATION_MASTER CLASS    *****
            '----------------------------------------------------------------------------------

            'Shani(24-Aug-2015) -- Start
            'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
            'Me._Assetdeclarationunkid = mintAssetdeclarationunkid
            'Hemant (24 Dec 2019) -- Start
            'ENHANCEMENT : Changes for Bind Transactions & Perfomance Enhacement.
            'Me._Assetdeclarationunkid(xDatabaseName) = mintAssetdeclarationunkid
            ''Shani(24-Aug-2015) -- End
            'Me._Banktotal = decBankTotal
            'Me._Sharedividendtotal = decShareDividendTotal
            'Me._Housetotal = decHouseTotal
            'Me._Parkfarmtotal = decParkFarmTotal
            'Me._Vehicletotal = decVehicleTotal
            'Me._Machinerytotal = decMachineryTotal
            'Me._Otherbusinesstotal = decOtherBusinessTotal
            'Me._Otherresourcetotal = decOtherResourceTotal
            'Me._Debttotal = decDebtTotal

            'If Update() = False Then
            '    objDataOperation.ReleaseTransaction(False)
            '    Return False
            'End If

            '---------------------------
            'If blnIsUpdate = False Then
            '    If blnChildTableChanged = False Then
            '        'S.SANDEEP [28-May-2018] -- START
            '        'ISSUE/ENHANCEMENT : {Audit Trails} 
            '        Dim objCommonATLog As New clsCommonATLog
            '        objCommonATLog._FormName = mstrFormName
            '        objCommonATLog._LoginEmployeeUnkid = mintLoginemployeeunkid
            '        objCommonATLog._ClientIP = mstrClientIP
            ''        objCommonATLog._HostName = mstrHostName
            '        objCommonATLog._FromWeb = mblnIsWeb
            '        objCommonATLog._AuditUserId = mintAuditUserId
            '        objCommonATLog._CompanyUnkid = mintCompanyUnkid
            '        objCommonATLog._AuditDate = mdtAuditDate
            '        'S.SANDEEP [28-May-2018] -- END
'
            '        If objCommonATLog.Insert_TranAtLog(objDataOperation, "hrassetdeclaration_master", "assetdeclarationunkid", mintAssetdeclarationunkid, "", "", 0, 1, 0) = False Then
            '            objDataOperation.ReleaseTransaction(False)
            '            Return False
            '        End If

             '       'S.SANDEEP [28-May-2018] -- START
             '       'ISSUE/ENHANCEMENT : {Audit Trails} 
             '       objCommonATLog = Nothing
             '       'S.SANDEEP [28-May-2018] -- END
'
'                End If
'            Else
'                If blnChildTableChanged = False Then
'                    'S.SANDEEP [28-May-2018] -- START
'                    'ISSUE/ENHANCEMENT : {Audit Trails} 
'                    Dim objCommonATLog As New clsCommonATLog
'                    objCommonATLog._FormName = mstrFormName
'                    objCommonATLog._LoginEmployeeUnkid = mintLoginemployeeunkid
'                    objCommonATLog._ClientIP = mstrClientIP
'                    objCommonATLog._HostName = mstrHostName
'                    objCommonATLog._FromWeb = mblnIsWeb
'                    objCommonATLog._AuditUserId = mintAuditUserId
'                    objCommonATLog._CompanyUnkid = mintCompanyUnkid
'                    objCommonATLog._AuditDate = mdtAuditDate
'                    'S.SANDEEP [28-May-2018] -- END
'
'                    If objCommonATLog.IsTableDataUpdate("atcommon_tranlog", "hrassetdeclaration_master", intAssetDeclarationUnkID, "assetdeclarationunkid", 2, objDataOperation) = True Then
'                        If objCommonATLog.Insert_TranAtLog(objDataOperation, "hrassetdeclaration_master", "assetdeclarationunkid", intAssetDeclarationUnkID, "", "", 0, 2, 0) = False Then
'                            objDataOperation.ReleaseTransaction(False)
'                            Return False
'                        End If
'                    End If

'                    'S.SANDEEP [28-May-2018] -- START
'                    'ISSUE/ENHANCEMENT : {Audit Trails} 
'                    objCommonATLog = Nothing
'                    'S.SANDEEP [28-May-2018] -- END

'                End If
'            End If
          'Hemant (24 Dec 2019) -- End

            objDataOperation.ReleaseTransaction(True)

            Return True
        Catch ex As Exception
            objDataOperation.ReleaseTransaction(False)
            Throw New Exception(ex.Message & "; Procedure Name: Insert; Module Name: " & mstrModuleName)
            Return False
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
    End Function
    'Sohail (29 Jan 2013) -- End

    ''' <summary>
    ''' Modify By: Sohail
    ''' </summary>
    ''' <returns>Boolean</returns>
    ''' <purpose> INSERT INTO Database Table (hrassetdeclaration_master) </purpose>
    Public Function Insert(Optional ByVal xDataOp As clsDataOperation = Nothing, Optional ByRef intNewUnkId As Integer = 0) As Boolean
        'Hemant (24 Dec 2019) -- [xDataOp,intNewUnkId]
        'If isExist(mstrName) Then
        '    mstrMessage = "<Message>"
        '    Return False
        'End If

        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception


        'Hemant (24 Dec 2019) -- Start
        'ENHANCEMENT : Changes for Bind Transactions & Perfomance Enhacement.
        'objDataOperation = New clsDataOperation
        If xDataOp IsNot Nothing Then
            objDataOperation = xDataOp
        Else
        objDataOperation = New clsDataOperation
            objDataOperation.BindTransaction()
        End If
        objDataOperation.ClearParameters()
        'Hemant (24 Dec 2019) -- End


        Try
            'Hemant (24 Dec 2019) -- Start
            'ENHANCEMENT : Changes for Bind Transactions & Perfomance Enhacement.
            intNewUnkId = 0
            Dim intUnkId As Integer = 0
            If isExist(mintEmployeeunkid, 0, intUnkId, objDataOperation) = True Then
                If intUnkId > 0 Then
                    mintAssetdeclarationunkid = intUnkId
                    intNewUnkId = mintAssetdeclarationunkid
                    If Update(enAction.EDIT_ONE, objDataOperation) = False Then
                        Return False
                    Else
                        Return True
                    End If
                End If
            End If
            objDataOperation.ClearParameters()
            'Hemant (24 Dec 2019) -- End

            objDataOperation.AddParameter("@employeeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintEmployeeunkid.ToString)
            objDataOperation.AddParameter("@cash", SqlDbType.Decimal, eZeeDataType.DECIMAL_SIZE, mdecCash.ToString)
            'Sohail (26 Mar 2012) -- Start
            'TRA - ENHANCEMENT
            'objDataOperation.AddParameter("@cashexistingbank", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrCashExistingBank.ToString)
            objDataOperation.AddParameter("@cashexistingbank", SqlDbType.Decimal, eZeeDataType.DECIMAL_SIZE, mdecCashExistingBank.ToString)
            'Sohail (26 Mar 2012) -- End
            objDataOperation.AddParameter("@finyear_start", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, mdtFinyear_Start.ToString)
            objDataOperation.AddParameter("@finyear_end", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, mdtFinyear_End.ToString)
            'Sohail (02 Apr 2012) -- Start
            'TRA - ENHANCEMENT
            'objDataOperation.AddParameter("@resources", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrResources.ToString)
            'objDataOperation.AddParameter("@debt", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrDebt.ToString)
            objDataOperation.AddParameter("@resources", SqlDbType.NVarChar, eZeeDataType.DESC_SIZE, mstrResources.ToString)
            objDataOperation.AddParameter("@debt", SqlDbType.NVarChar, eZeeDataType.DESC_SIZE, mstrDebt.ToString)
            'Sohail (02 Apr 2012) -- End
            objDataOperation.AddParameter("@isfinalsaved", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsfinalsaved.ToString)
            objDataOperation.AddParameter("@userunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintUserunkid.ToString)
            objDataOperation.AddParameter("@isvoid", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsvoid.ToString)
            objDataOperation.AddParameter("@voiduserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintVoiduserunkid.ToString)
            If mdtVoiddatetime = Nothing Then
                objDataOperation.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, DBNull.Value)
            Else
                objDataOperation.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, mdtVoiddatetime.ToString)
            End If
            objDataOperation.AddParameter("@voidreason", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrVoidreason.ToString)
            'Sohail (26 Mar 2012) -- Start
            'TRA - ENHANCEMENT
            objDataOperation.AddParameter("@loginemployeeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintLoginemployeeunkid.ToString)
            objDataOperation.AddParameter("@voidloginemployeeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintVoidloginemployeeunkid.ToString)
            'Sohail (26 Mar 2012) -- End
            'Sohail (06 Apr 2012) -- Start
            'TRA - ENHANCEMENT
            If mdtTransactiondate = Nothing Then
                objDataOperation.AddParameter("@transactiondate", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, DBNull.Value)
            Else
                objDataOperation.AddParameter("@transactiondate", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, mdtTransactiondate.ToString)
            End If
            'Sohail (06 Apr 2012) -- End
            'Sohail (29 Jan 2013) -- Start
            'TRA - ENHANCEMENT
            If mdtSavedate = Nothing Then
                objDataOperation.AddParameter("@savedate", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, DBNull.Value)
            Else
                objDataOperation.AddParameter("@savedate", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, mdtSavedate.ToString)
            End If
            If mdtFinalsavedate = Nothing Then
                objDataOperation.AddParameter("@finalsavedate", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, DBNull.Value)
            Else
                objDataOperation.AddParameter("@finalsavedate", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, mdtFinalsavedate.ToString)
            End If
            If mdtUnlockfinalsavedate = Nothing Then
                objDataOperation.AddParameter("@unlockfinalsavedate", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, DBNull.Value)
            Else
                objDataOperation.AddParameter("@unlockfinalsavedate", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, mdtUnlockfinalsavedate.ToString)
            End If
            'Sohail (29 Jan 2013) -- End
            'Hemant (24 Dec 2019) -- Start
            'ENHANCEMENT : Changes for Bind Transactions & Perfomance Enhacement.
            objDataOperation.AddParameter("@banktotal", SqlDbType.Decimal, eZeeDataType.FLOAT_SIZE, mdecBanktotal.ToString)
            objDataOperation.AddParameter("@sharedividendtotal", SqlDbType.Decimal, eZeeDataType.FLOAT_SIZE, mdecSharedividendtotal.ToString)
            objDataOperation.AddParameter("@housetotal", SqlDbType.Decimal, eZeeDataType.FLOAT_SIZE, mdecHousetotal.ToString)
            objDataOperation.AddParameter("@parkfarmtotal", SqlDbType.Decimal, eZeeDataType.FLOAT_SIZE, mdecParkfarmtotal.ToString)
            objDataOperation.AddParameter("@vehicletotal", SqlDbType.Decimal, eZeeDataType.FLOAT_SIZE, mdecVehicletotal.ToString)
            objDataOperation.AddParameter("@machinerytotal", SqlDbType.Decimal, eZeeDataType.FLOAT_SIZE, mdecMachinerytotal.ToString)
            objDataOperation.AddParameter("@otherbusinesstotal", SqlDbType.Decimal, eZeeDataType.FLOAT_SIZE, mdecOtherbusinesstotal.ToString)
            objDataOperation.AddParameter("@otherresourcetotal", SqlDbType.Decimal, eZeeDataType.FLOAT_SIZE, mdecOtherresourcetotal.ToString)
            objDataOperation.AddParameter("@debttotal", SqlDbType.Decimal, eZeeDataType.FLOAT_SIZE, mdecDebttotal.ToString)
            'Hemant (24 Dec 2019) -- End

            strQ = "INSERT INTO hrassetdeclaration_master ( " & _
              "  employeeunkid " & _
              ", cash " & _
              ", cashexistingbank " & _
              ", finyear_start " & _
              ", finyear_end " & _
              ", resources " & _
              ", debt " & _
              ", isfinalsaved " & _
              ", userunkid " & _
              ", isvoid " & _
              ", voiduserunkid " & _
              ", voiddatetime " & _
              ", voidreason" & _
              ", loginemployeeunkid" & _
              ", voidloginemployeeunkid" & _
              ", transactiondate" & _
              ", savedate " & _
              ", finalsavedate " & _
              ", unlockfinalsavedate" & _
              ", banktotal  " & _
              ", sharedividendtotal " & _
              ", housetotal " & _
              ", parkfarmtotal " & _
              ", vehicletotal " & _
              ", machinerytotal " & _
              ", otherbusinesstotal " & _
              ", otherresourcetotal " & _
              ", debttotal  " & _
            ") VALUES (" & _
              "  @employeeunkid " & _
              ", @cash " & _
              ", @cashexistingbank " & _
              ", @finyear_start " & _
              ", @finyear_end " & _
              ", @resources " & _
              ", @debt " & _
              ", @isfinalsaved " & _
              ", @userunkid " & _
              ", @isvoid " & _
              ", @voiduserunkid " & _
              ", @voiddatetime " & _
              ", @voidreason" & _
              ", @loginemployeeunkid" & _
              ", @voidloginemployeeunkid" & _
              ", @transactiondate" & _
              ", @savedate " & _
              ", @finalsavedate " & _
              ", @unlockfinalsavedate" & _
              ", @banktotal  " & _
              ", @sharedividendtotal " & _
              ", @housetotal " & _
              ", @parkfarmtotal " & _
              ", @vehicletotal " & _
              ", @machinerytotal " & _
              ", @otherbusinesstotal " & _
              ", @otherresourcetotal " & _
              ", @debttotal  " & _
            "); SELECT @@identity" 'Sohail (06 Apr 2012) - [transactiondate]
            'Hemant (24 Dec 2019) -- [banktotal, sharedividendtotal, housetotal, parkfarmtotal , vehicletotal , machinerytotal, otherbusinesstotal, otherresourcetotal, debttotal]
            'Sohail (29 Jan 2013) - [savedate, finalsavedate, unlockfinalsavedate]

            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                'Hemant (24 Dec 2019) -- Start
                'ENHANCEMENT : Changes for Bind Transactions & Perfomance Enhacement.
                'objDataOperation.ReleaseTransaction(False)
                If xDataOp Is Nothing Then objDataOperation.ReleaseTransaction(False)
                'Hemant (24 Dec 2019) -- End
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            mintAssetdeclarationunkid = dsList.Tables(0).Rows(0).Item(0)
            'Hemant (24 Dec 2019) -- Start
            'ENHANCEMENT : Changes for Bind Transactions & Perfomance Enhacement.
            intNewUnkId = mintAssetdeclarationunkid
            If xDataOp Is Nothing Then objDataOperation.ReleaseTransaction(True)
            'Hemant (24 Dec 2019) -- End
            Return True
        Catch ex As Exception
            'Hemant (24 Dec 2019) -- Start
            'ENHANCEMENT : Changes for Bind Transactions & Perfomance Enhacement.
            If xDataOp Is Nothing Then objDataOperation.ReleaseTransaction(False)
            'Hemant (24 Dec 2019) -- End
            Throw New Exception(ex.Message & "; Procedure Name: Insert; Module Name: " & mstrModuleName)
            Return False
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            'Hemant (24 Dec 2019) -- Start
            'ENHANCEMENT : Changes for Bind Transactions & Perfomance Enhacement.
            'objDataOperation = Nothing
            If xDataOp Is Nothing Then objDataOperation = Nothing
            'Hemant (24 Dec 2019) -- End
        End Try
    End Function

    ''' <summary>
    ''' Modify By: Sohail
    ''' </summary>
    ''' <returns>Boolean</returns>
    ''' <purpose> Update Database Table (hrassetdeclaration_master) </purpose>
    Public Function Update(Optional ByVal blnInsertLog As Boolean = False, Optional ByVal xDataOp As clsDataOperation = Nothing) As Boolean
        'Hemant (24 Dec 2019) -- [xDataOp]
        'If isExist(mstrName, mintAssetdeclarationunkid) Then
        '    mstrMessage = "<Message>"
        '    Return False
        'End If

        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        'Hemant (24 Dec 2019) -- Start
        'ENHANCEMENT : Changes for Bind Transactions & Perfomance Enhacement.
        'objDataOperation = New clsDataOperation 'Sohail (04 Feb 2020) - [commented]
        If xDataOp IsNot Nothing Then
            objDataOperation = xDataOp
        Else
        objDataOperation = New clsDataOperation
            objDataOperation.BindTransaction()
        End If
        objDataOperation.ClearParameters()
        'Hemant (24 Dec 2019) -- End

        Try
            objDataOperation.AddParameter("@assetdeclarationunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintAssetdeclarationunkid.ToString)
            objDataOperation.AddParameter("@employeeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintEmployeeunkid.ToString)
            objDataOperation.AddParameter("@cash", SqlDbType.Decimal, eZeeDataType.DECIMAL_SIZE, mdecCash.ToString)
            'Sohail (26 Mar 2012) -- Start
            'TRA - ENHANCEMENT
            'objDataOperation.AddParameter("@cashexistingbank", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrCashExistingBank.ToString)
            objDataOperation.AddParameter("@cashexistingbank", SqlDbType.Decimal, eZeeDataType.DECIMAL_SIZE, mdecCashExistingBank.ToString)
            'Sohail (26 Mar 2012) -- End
            objDataOperation.AddParameter("@finyear_start", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, mdtFinyear_Start.ToString)
            objDataOperation.AddParameter("@finyear_end", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, mdtFinyear_End.ToString)
            'Sohail (02 Apr 2012) -- Start
            'TRA - ENHANCEMENT
            'objDataOperation.AddParameter("@resources", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrResources.ToString)
            'objDataOperation.AddParameter("@debt", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrDebt.ToString)
            objDataOperation.AddParameter("@resources", SqlDbType.NVarChar, eZeeDataType.DESC_SIZE, mstrResources.ToString)
            objDataOperation.AddParameter("@debt", SqlDbType.NVarChar, eZeeDataType.DESC_SIZE, mstrDebt.ToString)
            'Sohail (02 Apr 2012) -- End
            objDataOperation.AddParameter("@isfinalsaved", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsfinalsaved.ToString)
            objDataOperation.AddParameter("@userunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintUserunkid.ToString)
            objDataOperation.AddParameter("@isvoid", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsvoid.ToString)
            objDataOperation.AddParameter("@voiduserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintVoiduserunkid.ToString)
            If mdtVoiddatetime = Nothing Then
                objDataOperation.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, DBNull.Value)
            Else
                objDataOperation.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, mdtVoiddatetime.ToString)
            End If
            objDataOperation.AddParameter("@voidreason", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrVoidreason.ToString)
            'Sohail (26 Mar 2012) -- Start
            'TRA - ENHANCEMENT
            objDataOperation.AddParameter("@loginemployeeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintLoginemployeeunkid.ToString)
            objDataOperation.AddParameter("@voidloginemployeeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintVoidloginemployeeunkid.ToString)
            'Sohail (26 Mar 2012) -- End
            'Sohail (06 Apr 2012) -- Start
            'TRA - ENHANCEMENT
            If mdtTransactiondate = Nothing Then
                objDataOperation.AddParameter("@transactiondate", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, DBNull.Value)
            Else
                objDataOperation.AddParameter("@transactiondate", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, mdtTransactiondate.ToString)
            End If
            'Sohail (06 Apr 2012) -- End
            'Sohail (29 Jan 2013) -- Start
            'TRA - ENHANCEMENT
            If mdtSavedate = Nothing Then
                objDataOperation.AddParameter("@savedate", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, DBNull.Value)
            Else
                objDataOperation.AddParameter("@savedate", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, mdtSavedate.ToString)
            End If
            If mdtFinalsavedate = Nothing Then
                objDataOperation.AddParameter("@finalsavedate", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, DBNull.Value)
            Else
                objDataOperation.AddParameter("@finalsavedate", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, mdtFinalsavedate.ToString)
            End If
            If mdtUnlockfinalsavedate = Nothing Then
                objDataOperation.AddParameter("@unlockfinalsavedate", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, DBNull.Value)
            Else
                objDataOperation.AddParameter("@unlockfinalsavedate", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, mdtUnlockfinalsavedate.ToString)
            End If
            objDataOperation.AddParameter("@banktotal", SqlDbType.Decimal, eZeeDataType.FLOAT_SIZE, mdecBanktotal.ToString)
            objDataOperation.AddParameter("@sharedividendtotal", SqlDbType.Decimal, eZeeDataType.FLOAT_SIZE, mdecSharedividendtotal.ToString)
            objDataOperation.AddParameter("@housetotal", SqlDbType.Decimal, eZeeDataType.FLOAT_SIZE, mdecHousetotal.ToString)
            objDataOperation.AddParameter("@parkfarmtotal", SqlDbType.Decimal, eZeeDataType.FLOAT_SIZE, mdecParkfarmtotal.ToString)
            objDataOperation.AddParameter("@vehicletotal", SqlDbType.Decimal, eZeeDataType.FLOAT_SIZE, mdecVehicletotal.ToString)
            objDataOperation.AddParameter("@machinerytotal", SqlDbType.Decimal, eZeeDataType.FLOAT_SIZE, mdecMachinerytotal.ToString)
            objDataOperation.AddParameter("@otherbusinesstotal", SqlDbType.Decimal, eZeeDataType.FLOAT_SIZE, mdecOtherbusinesstotal.ToString)
            objDataOperation.AddParameter("@otherresourcetotal", SqlDbType.Decimal, eZeeDataType.FLOAT_SIZE, mdecOtherresourcetotal.ToString)
            objDataOperation.AddParameter("@debttotal", SqlDbType.Decimal, eZeeDataType.FLOAT_SIZE, mdecDebttotal.ToString)
            'Sohail (29 Jan 2013) -- End

            strQ = "UPDATE hrassetdeclaration_master SET " & _
              "  employeeunkid = @employeeunkid" & _
              ", cash = @cash " & _
              ", cashexistingbank = @cashexistingbank " & _
              ", finyear_start = @finyear_start" & _
              ", finyear_end = @finyear_end" & _
              ", resources = @resources " & _
              ", debt = @debt " & _
              ", isfinalsaved = @isfinalsaved " & _
              ", userunkid = @userunkid" & _
              ", isvoid = @isvoid" & _
              ", voiduserunkid = @voiduserunkid" & _
              ", voiddatetime = @voiddatetime" & _
              ", voidreason = @voidreason " & _
              ", loginemployeeunkid = @loginemployeeunkid " & _
              ", voidloginemployeeunkid = @voidloginemployeeunkid " & _
              ", transactiondate = @transactiondate " & _
                        ", savedate = @savedate" & _
                        ", finalsavedate = @finalsavedate" & _
                        ", unlockfinalsavedate = @unlockfinalsavedate " & _
                        ", banktotal = @banktotal" & _
                        ", sharedividendtotal = @sharedividendtotal" & _
                        ", housetotal = @housetotal" & _
                        ", parkfarmtotal = @parkfarmtotal" & _
                        ", vehicletotal = @vehicletotal" & _
                        ", machinerytotal = @machinerytotal" & _
                        ", otherbusinesstotal = @otherbusinesstotal" & _
                        ", otherresourcetotal = @otherresourcetotal" & _
                        ", debttotal = @debttotal " & _
            "WHERE assetdeclarationunkid = @assetdeclarationunkid "
            'Sohail (29 Jan 2013) - [savedate, finalsavedate, unlockfinalsavedate, banktotal, sharedividendtotal, housetotal, parkfarmtotal, vehicletotal, machinerytotal, otherbusinesstotal, otherresourcetotal, debttotal]

            Call objDataOperation.ExecNonQuery(strQ)

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            'Sohail (23 Apr 2012) -- Start
            'TRA - ENHANCEMENT
            If blnInsertLog = True Then 'Sohail (29 Jan 2013)
                'S.SANDEEP [28-May-2018] -- START
                'ISSUE/ENHANCEMENT : {Audit Trails} 
                Dim objCommonATLog As New clsCommonATLog
                objCommonATLog._FormName = mstrFormName
                objCommonATLog._LoginEmployeeUnkid = mintLoginemployeeunkid
                objCommonATLog._ClientIP = mstrClientIP
                objCommonATLog._HostName = mstrHostName
                objCommonATLog._FromWeb = mblnIsWeb
                objCommonATLog._AuditUserId = mintAuditUserId
objCommonATLog._CompanyUnkid = mintCompanyUnkid
                objCommonATLog._AuditDate = mdtAuditDate
                'S.SANDEEP [28-May-2018] -- END

                    'Hemant (24 Dec 2019) -- Start
                'ENHANCEMENT : Changes for Bind Transactions & Perfomance Enhacement.
                If objCommonATLog.IsTableDataUpdate("atcommon_tranlog", "hrassetdeclaration_master", mintAssetdeclarationunkid, "assetdeclarationunkid", 2) Then
                    'Hemant (24 Dec 2019) -- End
                If objCommonATLog.Insert_TranAtLog(objDataOperation, "hrassetdeclaration_master", "assetdeclarationunkid", mintAssetdeclarationunkid, "", "", 0, 2, 0, , mintUserunkid) = False Then
                        'Hemant (24 Dec 2019) -- Start
                        'ENHANCEMENT : Changes for Bind Transactions & Perfomance Enhacement.
                        If xDataOp Is Nothing Then objDataOperation.ReleaseTransaction(False)
                        'Hemant (24 Dec 2019) -- End
                    Return False
                End If
                 End If
                'S.SANDEEP [28-May-2018] -- START
                'ISSUE/ENHANCEMENT : {Audit Trails} 
                objCommonATLog = Nothing
                'S.SANDEEP [28-May-2018] -- END

            End If 'Sohail (29 Jan 2013)           
            'Sohail (23 Apr 2012) -- End
            'Hemant (24 Dec 2019) -- Start
            'ENHANCEMENT : Changes for Bind Transactions & Perfomance Enhacement.
            If xDataOp Is Nothing Then objDataOperation.ReleaseTransaction(True)
            'Hemant (24 Dec 2019) -- End
            Return True
        Catch ex As Exception
            'Hemant (24 Dec 2019) -- Start
            'ENHANCEMENT : Changes for Bind Transactions & Perfomance Enhacement.
            If xDataOp Is Nothing Then objDataOperation.ReleaseTransaction(False)
            'Hemant (24 Dec 2019) -- End
            Throw New Exception(ex.Message & "; Procedure Name: Update; Module Name: " & mstrModuleName)
            Return False
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            'Hemant (24 Dec 2019) -- Start
            'ENHANCEMENT : Changes for Bind Transactions & Perfomance Enhacement.
            'objDataOperation = Nothing
            If xDataOp Is Nothing Then objDataOperation = Nothing
            'Hemant (24 Dec 2019) -- End
        End Try
    End Function

    ''' <summary>
    ''' Modify By: Sohail
    ''' </summary>
    ''' <returns>Boolean</returns>
    ''' <purpose> Delete Database Table (hrassetdeclaration_master) </purpose>
    Public Function Void(ByVal intUnkid As Integer, _
                         ByVal intVoidUserID As Integer, ByVal dtVoidDateTime As DateTime, ByVal strVoidReason As String) As Boolean
        'If isUsed(intUnkid) Then
        '    mstrMessage = "<Message>"
        '    Return False
        'End If
        Dim objDataOperation As clsDataOperation

        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception
        Dim blnChildExist As Boolean = False

        objDataOperation = New clsDataOperation
        objDataOperation.BindTransaction()

        Try

            If VoidAssetDeclarationMaster(intUnkid, intVoidUserID, dtVoidDateTime, strVoidReason) = False Then
                objDataOperation.ReleaseTransaction(False)
                Return False
            End If


            Dim objAssetBank As New clsAsset_bank_tran
            dsList = objAssetBank.GetList("Bank", intUnkid)
            If dsList.Tables("Bank").Rows.Count > 0 Then
                blnChildExist = True
                'Sohail (23 Apr 2012) -- Start
                'TRA - ENHANCEMENT

                'Shani(24-Aug-2015) -- Start
                'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
                'If objAssetBank.VoidByAssetDeclarationUnkID(intUnkid, intVoidUserID, ConfigParameter._Object._CurrentDateAndTime, "", 3) = False Then
                If objAssetBank.VoidByAssetDeclarationUnkID(intUnkid, intVoidUserID, dtVoidDateTime, "", 3, objDataOperation) = False Then
                    'Hemant (24 Dec 2019) -- [objDataOperation]
                    'Shani(24-Aug-2015) -- End

                    'If objAssetBank.VoidByAssetDeclarationUnkID(intUnkid, mintUserunkid, ConfigParameter._Object._CurrentDateAndTime, "") = False Then
                    'Sohail (23 Apr 2012) -- End
                    objDataOperation.ReleaseTransaction(False)
                    Return False
                End If
            End If


            Dim objAssetShareDividend As New clsAsset_sharedividend_tran
            dsList = objAssetShareDividend.GetList("Share", intUnkid)
            If dsList.Tables("Share").Rows.Count > 0 Then
                blnChildExist = True
                'Sohail (23 Apr 2012) -- Start
                'TRA - ENHANCEMENT

                'Shani(24-Aug-2015) -- Start
                'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
                'If objAssetShareDividend.VoidByAssetDeclarationUnkID(intUnkid, intVoidUserID, ConfigParameter._Object._CurrentDateAndTime, "", 3) = False Then
                If objAssetShareDividend.VoidByAssetDeclarationUnkID(intUnkid, intVoidUserID, dtVoidDateTime, "", 3, objDataOperation) = False Then
                    'Hemant (24 Dec 2019) -- [objDataOperation]
                    'Shani(24-Aug-2015) -- End

                    'If objAssetShareDividend.VoidByAssetDeclarationUnkID(intUnkid, mintUserunkid, ConfigParameter._Object._CurrentDateAndTime, "") = False Then
                    'Sohail (23 Apr 2012) -- End
                    objDataOperation.ReleaseTransaction(False)
                    Return False
                End If
            End If


            Dim objAssetHouseBuilding As New clsAsset_housebuilding_tran
            dsList = objAssetHouseBuilding.GetList("House", intUnkid)
            If dsList.Tables("House").Rows.Count > 0 Then
                blnChildExist = True
                'Sohail (23 Apr 2012) -- Start
                'TRA - ENHANCEMENT

                'Shani(24-Aug-2015) -- Start
                'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
                'If objAssetHouseBuilding.VoidByAssetDeclarationUnkID(intUnkid, intVoidUserID, ConfigParameter._Object._CurrentDateAndTime, "", 3) = False Then
                If objAssetHouseBuilding.VoidByAssetDeclarationUnkID(intUnkid, intVoidUserID, dtVoidDateTime, "", 3, objDataOperation) = False Then
                    'Hemant (24 Dec 2019) -- [objDataOperation]
                    'Shani(24-Aug-2015) -- End

                    'If objAssetHouseBuilding.VoidByAssetDeclarationUnkID(intUnkid, mintUserunkid, ConfigParameter._Object._CurrentDateAndTime, "") = False Then
                    'Sohail (23 Apr 2012) -- End
                    objDataOperation.ReleaseTransaction(False)
                    Return False
                End If
            End If


            Dim objAssetParkFarm As New clsAsset_parkfarmmines_tran
            dsList = objAssetParkFarm.GetList("Park", intUnkid)
            If dsList.Tables("Park").Rows.Count > 0 Then
                blnChildExist = True
                'Sohail (23 Apr 2012) -- Start
                'TRA - ENHANCEMENT

                'Shani(24-Aug-2015) -- Start
                'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
                'If objAssetParkFarm.VoidByAssetDeclarationUnkID(intUnkid, intVoidUserID, ConfigParameter._Object._CurrentDateAndTime, "", 3) = False Then
                If objAssetParkFarm.VoidByAssetDeclarationUnkID(intUnkid, intVoidUserID, dtVoidDateTime, "", 3, objDataOperation) = False Then
                    'Hemant (24 Dec 2019) -- [objDataOperation]
                    'Shani(24-Aug-2015) -- End

                    'If objAssetParkFarm.VoidByAssetDeclarationUnkID(intUnkid, mintUserunkid, ConfigParameter._Object._CurrentDateAndTime, "") = False Then
                    'Sohail (23 Apr 2012) -- End
                    objDataOperation.ReleaseTransaction(False)
                    Return False
                End If
            End If


            Dim objAssetVehicles As New clsAsset_vehicles_tran
            dsList = objAssetVehicles.GetList("Vehicle", intUnkid)
            If dsList.Tables("Vehicle").Rows.Count > 0 Then
                blnChildExist = True
                'Sohail (23 Apr 2012) -- Start
                'TRA - ENHANCEMENT

                'Shani(24-Aug-2015) -- Start
                'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
                'If objAssetVehicles.VoidByAssetDeclarationUnkID(intUnkid, intVoidUserID, ConfigParameter._Object._CurrentDateAndTime, "", 3) = False Then
                If objAssetVehicles.VoidByAssetDeclarationUnkID(intUnkid, intVoidUserID, dtVoidDateTime, "", 3, objDataOperation) = False Then
                    'Hemant (24 Dec 2019) -- [objDataOperation]
                    'Shani(24-Aug-2015) -- End

                    'If objAssetVehicles.VoidByAssetDeclarationUnkID(intUnkid, mintUserunkid, ConfigParameter._Object._CurrentDateAndTime, "") = False Then
                    'Sohail (23 Apr 2012) -- End
                    objDataOperation.ReleaseTransaction(False)
                    Return False
                End If
            End If


            Dim objAssetMachinery As New clsAsset_machinery_tran
            dsList = objAssetMachinery.GetList("Machinery", intUnkid)
            If dsList.Tables("Machinery").Rows.Count > 0 Then
                blnChildExist = True
                'Sohail (23 Apr 2012) -- Start
                'TRA - ENHANCEMENT

                'Shani(24-Aug-2015) -- Start
                'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
                'If objAssetMachinery.VoidByAssetDeclarationUnkID(intUnkid, intVoidUserID, ConfigParameter._Object._CurrentDateAndTime, "", 3) = False Then
                If objAssetMachinery.VoidByAssetDeclarationUnkID(intUnkid, intVoidUserID, dtVoidDateTime, "", 3, objDataOperation) = False Then
                    'Hemant (24 Dec 2019) -- [objDataOperation]
                    'Shani(24-Aug-2015) -- End

                    'If objAssetMachinery.VoidByAssetDeclarationUnkID(intUnkid, mintUserunkid, ConfigParameter._Object._CurrentDateAndTime, "") = False Then
                    'Sohail (23 Apr 2012) -- End
                    objDataOperation.ReleaseTransaction(False)
                    Return False
                End If
            End If


            Dim objAssetOtherBusiness As New clsAsset_otherbusiness_tran
            dsList = objAssetOtherBusiness.GetList("Business", intUnkid)
            If dsList.Tables("Business").Rows.Count > 0 Then
                blnChildExist = True
                'Sohail (23 Apr 2012) -- Start
                'TRA - ENHANCEMENT

                'Shani(24-Aug-2015) -- Start
                'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
                'If objAssetOtherBusiness.VoidByAssetDeclarationUnkID(intUnkid, intVoidUserID, ConfigParameter._Object._CurrentDateAndTime, "", 3) = False Then
                If objAssetOtherBusiness.VoidByAssetDeclarationUnkID(intUnkid, intVoidUserID, dtVoidDateTime, "", 3, objDataOperation) = False Then
                    'Hemant (24 Dec 2019) -- [objDataOperation]
                    'Shani(24-Aug-2015) -- End

                    'If objAssetOtherBusiness.VoidByAssetDeclarationUnkID(intUnkid, mintUserunkid, ConfigParameter._Object._CurrentDateAndTime, "") = False Then
                    'Sohail (23 Apr 2012) -- End
                    objDataOperation.ReleaseTransaction(False)
                    Return False
                End If
            End If

            'Sohail (29 Jan 2013) -- Start
            'TRA - ENHANCEMENT
            Dim objAssetResources As New clsAsset_otherresources_tran
            dsList = objAssetResources.GetList("Resources", intUnkid)
            If dsList.Tables("Resources").Rows.Count > 0 Then
                blnChildExist = True

                'Shani(24-Aug-2015) -- Start
                'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
                'If objAssetResources.VoidByAssetDeclarationUnkID(intUnkid, intVoidUserID, ConfigParameter._Object._CurrentDateAndTime, "", 3) = False Then
                If objAssetResources.VoidByAssetDeclarationUnkID(intUnkid, intVoidUserID, dtVoidDateTime, "", 3, objDataOperation) = False Then
                    'Hemant (24 Dec 2019) -- [objDataOperation]
                    'Shani(24-Aug-2015) -- End

                    objDataOperation.ReleaseTransaction(False)
                    Return False
                End If
            End If

            Dim objAssetDebts As New clsAsset_debts_tran
            dsList = objAssetDebts.GetList("Debts", intUnkid)
            If dsList.Tables("Debts").Rows.Count > 0 Then
                blnChildExist = True

                'Shani(24-Aug-2015) -- Start
                'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
                'If objAssetDebts.VoidByAssetDeclarationUnkID(intUnkid, intVoidUserID, ConfigParameter._Object._CurrentDateAndTime, "", 3) = False Then
                If objAssetDebts.VoidByAssetDeclarationUnkID(intUnkid, intVoidUserID, dtVoidDateTime, "", 3, objDataOperation) = False Then
                    'Hemant (24 Dec 2019) -- [objDataOperation]
                    'Shani(24-Aug-2015) -- End

                    objDataOperation.ReleaseTransaction(False)
                    Return False
                End If
            End If
            'Sohail (29 Jan 2013) -- End

            '-------------------
            If blnChildExist = False Then
                'S.SANDEEP [28-May-2018] -- START
                'ISSUE/ENHANCEMENT : {Audit Trails} 
                Dim objCommonATLog As New clsCommonATLog
                objCommonATLog._FormName = mstrFormName
                objCommonATLog._LoginEmployeeUnkid = mintLoginemployeeunkid
                objCommonATLog._ClientIP = mstrClientIP
                objCommonATLog._HostName = mstrHostName
                objCommonATLog._FromWeb = mblnIsWeb
                objCommonATLog._AuditUserId = mintAuditUserId
objCommonATLog._CompanyUnkid = mintCompanyUnkid
                objCommonATLog._AuditDate = mdtAuditDate
                'S.SANDEEP [28-May-2018] -- END

                If objCommonATLog.Insert_TranAtLog(objDataOperation, "hrassetdeclaration_master", "assetdeclarationunkid", intUnkid, "", "", 0, 3, 0, , intVoidUserID) = False Then
                    objDataOperation.ReleaseTransaction(False)
                    Return False
                End If

                'S.SANDEEP [28-May-2018] -- START
                'ISSUE/ENHANCEMENT : {Audit Trails} 
                objCommonATLog = Nothing
                'S.SANDEEP [28-May-2018] -- END

            End If

            objDataOperation.ReleaseTransaction(True)
            Return True
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: Void; Module Name: " & mstrModuleName)
            Return False
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
    End Function

    Private Function VoidAssetDeclarationMaster(ByVal intAssetDeclarationUnkID As Integer, _
                                        ByVal intVoidUserID As Integer, ByVal dtVoidDateTime As DateTime, ByVal strVoidReason As String) As Boolean
        'If isUsed(intUnkid) Then
        '    mstrMessage = "<Message>"
        '    Return False
        'End If

        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        objDataOperation = New clsDataOperation

        Try

            strQ = "UPDATE hrassetdeclaration_master SET " & _
             "  isvoid = 1 " & _
             ", voiduserunkid = @voiduserunkid" & _
             ", voiddatetime = @voiddatetime" & _
             ", voidreason = @voidreason " & _
             ", voidloginemployeeunkid = @voidloginemployeeunkid " & _
           "WHERE assetdeclarationunkid = @assetdeclarationunkid "

            objDataOperation.AddParameter("@assetdeclarationunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intAssetDeclarationUnkID)
            objDataOperation.AddParameter("@voiduserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intVoidUserID)
            objDataOperation.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, dtVoidDateTime)
            objDataOperation.AddParameter("@voidreason", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, strVoidReason)
            objDataOperation.AddParameter("@voidloginemployeeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintVoidloginemployeeunkid)

            Call objDataOperation.ExecNonQuery(strQ)

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            Return True
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: VoidAssetDeclarationMaster; Module Name: " & mstrModuleName)
            Return False
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
    End Function

    ''' <summary>
    ''' Modify By: Sohail
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>
    Public Function isUsed(ByVal intUnkid As Integer) As Boolean
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        objDataOperation = New clsDataOperation

        Try
            strQ = "<Query>"

            objDataOperation.AddParameter("@assetdeclarationunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intUnkid)

            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            Return dsList.Tables(0).Rows.Count > 0
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: isUsed; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
    End Function

    ''' <summary>
    ''' Modify By: Sohail
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>
    Public Function isExist(ByVal intEmpUnkID As Integer, Optional ByVal intUnkid As Integer = -1, Optional ByRef intAssetDeclarationUnkID As Integer = 0, Optional ByVal xDataOp As clsDataOperation = Nothing) As Boolean
        'Hemant (24 Dec 2019) -- [xDataOp]
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        'Hemant (24 Dec 2019) -- Start
        'ENHANCEMENT : Changes for Bind Transactions & Perfomance Enhacement.
        'objDataOperation = New clsDataOperation
        If xDataOp IsNot Nothing Then
            objDataOperation = xDataOp
        Else
        objDataOperation = New clsDataOperation
        End If
        objDataOperation.ClearParameters()
        'Hemant (24 Dec 2019) -- End

        Try
            strQ = "SELECT " & _
              "  assetdeclarationunkid " & _
              ", employeeunkid " & _
              ", ISNULL(cash, 0) AS cash " & _
              ", ISNULL(cashexistingbank, 0) AS cashexistingbank " & _
              ", finyear_start " & _
              ", finyear_end " & _
              ", resources " & _
              ", debt " & _
              ", isfinalsaved " & _
              ", userunkid " & _
              ", isvoid " & _
              ", voiduserunkid " & _
              ", voiddatetime " & _
              ", voidreason " & _
              ", ISNULL(transactiondate, GETDATE()) AS transactiondate " & _
             "FROM hrassetdeclaration_master " & _
             "WHERE ISNULL(isvoid, 0) = 0 " & _
             "AND employeeunkid = @employeeunkid " 'Sohail (06 Apr 2012) - [transactiondate]

            If intUnkid > 0 Then
                strQ &= " AND assetdeclarationunkid <> @assetdeclarationunkid"
                objDataOperation.AddParameter("@assetdeclarationunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intUnkid)
            End If

            objDataOperation.AddParameter("@employeeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intEmpUnkID)

            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            If dsList.Tables(0).Rows.Count > 0 Then
                intAssetDeclarationUnkID = CInt(dsList.Tables(0).Rows(0).Item("assetdeclarationunkid"))
            End If

            Return dsList.Tables(0).Rows.Count > 0
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: isExist; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            'Hemant (24 Dec 2019) -- Start
            'ENHANCEMENT : Changes for Bind Transactions & Perfomance Enhacement.
            'objDataOperation = Nothing
            If xDataOp Is Nothing Then objDataOperation = Nothing
            'Hemant (24 Dec 2019) -- End
        End Try
    End Function

End Class