﻿'************************************************************************************************************************************
'Class Name : clsInterviewAnalysis_master.vb
'Purpose    :
'Date       : 13/09/2010
'Written By : Anjan
'Modified   :
'************************************************************************************************************************************

Imports eZeeCommonLib
Imports Aruti.Data.clsCommon_Master
Public Class clsInterviewAnalysis_master


    'Pinkal (07-Jan-2012) -- Start
    'Enhancement : TRA Changes

    'Private Shared Readonly mstrModuleName As String = "clsInteviewAnalysis_tran"
    Private Shared ReadOnly mstrModuleName As String = "clsInterviewAnalysis_master"

    'Pinkal (07-Jan-2012) -- End


    Dim objDataOperation As clsDataOperation
    Dim mstrMessage As String = ""
    Dim objAnalysisTran As New clsInteviewAnalysis_tran

#Region " Private Variables "

    Private mintAnalysisunkid As Integer
    Private mintAppbatchscheduletranunkid As Integer
    Private mdtAnalysis_Date As Date
    Private mintUserunkid As Integer
    Private mblnIsvoid As Boolean = False
    Private mdtVoidatetime As Date
    Private mintVoiduserunkid As Integer
    Private mstrVoidreason As String = String.Empty
    Private mblnIsComplete As Boolean = False
    Private mblnIsEligible As Boolean = False

    'S.SANDEEP [ 14 May 2013 ] -- START
    'ENHANCEMENT : TRA ENHANCEMENT
    Private mintStatustypid As Integer
    Private mblnIssent As Boolean
    Private mintVacancyunkid As Integer
    Private mintApplicantunkid As Integer
    Private mintApproveuserunkid As Integer = 0
    Private mstrAppr_Remark As String = String.Empty
    Private mdtAppr_Date As Date = Nothing
    'S.SANDEEP [ 14 May 2013 ] -- END

#End Region

#Region " Properties "
'S.SANDEEP [28-May-2018] -- START 
'ISSUE/ENHANCEMENT : {Audit Trails} 
Private mstrFormName As String = String.Empty
Public WriteOnly Property _FormName() As String 
Set(ByVal value As String) 
mstrFormName = value 
End Set 
End Property 
 
Private mstrClientIP As String = ""
Public WriteOnly Property _ClientIP() As String 
Set(ByVal value As String) 
mstrClientIP = value 
End Set 
End Property 
 
Private mstrHostName As String = ""
Public WriteOnly Property _HostName() As String 
Set(ByVal value As String) 
mstrHostName = value 
End Set 
End Property 
 
Private mblnIsWeb As Boolean = False
Public WriteOnly Property _FromWeb() As Boolean 
Set(ByVal value As Boolean) 
mblnIsWeb = value 
End Set 
End Property 
 
    Private mintAuditUserId As Integer = 0
    Public WriteOnly Property _AuditUserId() As Integer
        Set(ByVal value As Integer)
            mintAuditUserId = value
        End Set
    End Property

    Private mintCompanyUnkid As Integer = 0
    Public WriteOnly Property _CompanyUnkid() As Integer
        Set(ByVal value As Integer)
            mintCompanyUnkid = value
        End Set
    End Property
 
Private mdtAuditDate As DateTime = Now
Public WriteOnly Property _AuditDate() As DateTime 
Set(ByVal value As DateTime) 
mdtAuditDate = value 
End Set 
End Property 
 
Private xDataOpr As clsDataOperation = Nothing
Public Property _xDataOpr() As clsDataOperation 
Get 
Return xDataOpr 
End Get 
Set(ByVal value As clsDataOperation) 
xDataOpr = value 
End Set 
End Property 
 
Private mintLoginEmployeeunkid As Integer = 0
Public Property _LoginEmployeeunkid() As Integer 
Get 
Return mintLoginEmployeeunkid 
End Get 
Set(ByVal value As Integer) 
mintLoginEmployeeunkid = value 
End Set 
End Property 
 
Private mintVoidlogingemployeeunkid As Integer=0
Public Property _Voidloginemployeeunkid() As Integer 
Get 
Return mintVoidlogingemployeeunkid 
End Get 
Set(ByVal value As Integer) 
mintVoidlogingemployeeunkid = value 
End Set 
End Property 
'S.SANDEEP [28-May-2018] -- END 
    ''' <summary>
    ''' Purpose: Get Message from Class 
    ''' Modify By: Anjan
    ''' </summary>
    Public ReadOnly Property _Message() As String
        Get
            Return mstrMessage
        End Get
    End Property

    ''' <summary>
    ''' Purpose: Get or Set analysisunkid
    ''' Modify By: Anjan
    ''' </summary>
    Public Property _Analysisunkid() As Integer
        Get
            Return mintAnalysisunkid
        End Get
        Set(ByVal value As Integer)
            mintAnalysisunkid = value
            Call GetData()
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set analysisunkid
    ''' Modify By: Anjan
    ''' </summary>
    Public Property _Appbatchscheduletranunkid() As Integer
        Get
            Return mintAppbatchscheduletranunkid
        End Get
        Set(ByVal value As Integer)
            mintAppbatchscheduletranunkid = value
        End Set
    End Property

    '''' <summary>
    '''' Purpose: Get or Set analysis_date
    '''' Modify By: Anjan
    '''' </summary>
    'Public Property _Analysis_Date() As Date
    '    Get
    '        Return mdtAnalysis_Date
    '    End Get
    '    Set(ByVal value As Date)
    '        mdtAnalysis_Date = value
    '    End Set
    'End Property

    ''' <summary>
    ''' Purpose: Get or Set userinkid
    ''' Modify By: Anjan
    ''' </summary>
    Public Property _Userunkid() As Integer
        Get
            Return mintUserunkid
        End Get
        Set(ByVal value As Integer)
            mintUserunkid = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set isvoid
    ''' Modify By: Anjan
    ''' </summary>
    Public Property _Isvoid() As Boolean
        Get
            Return mblnIsvoid
        End Get
        Set(ByVal value As Boolean)
            mblnIsvoid = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set voiddatetime
    ''' Modify By: Anjan
    ''' </summary>
    Public Property _Voidatetime() As Date
        Get
            Return mdtVoidatetime
        End Get
        Set(ByVal value As Date)
            mdtVoidatetime = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set voiduserunkid
    ''' Modify By: Anjan
    ''' </summary>
    Public Property _Voiduserunkid() As Integer
        Get
            Return mintVoiduserunkid
        End Get
        Set(ByVal value As Integer)
            mintVoiduserunkid = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set voidreason
    ''' Modify By: Anjan
    ''' </summary>
    Public Property _Voidreason() As String
        Get
            Return mstrVoidreason
        End Get
        Set(ByVal value As String)
            mstrVoidreason = value
        End Set
    End Property

    '''' <summary>
    '''' Purpose: Get or Set iscomplete
    '''' Modify By: Anjan
    '''' </summary>
    Public Property _Iscomplete() As Boolean
        Get
            Return mblnIscomplete
        End Get
        Set(ByVal value As Boolean)
            mblnIscomplete = value
        End Set
    End Property
    ''''<summary>
    '''' Purpose: Get or Set iscomplete
    '''' Modify By: Anjan
    '''' </summary>
    Public Property _IsEligible() As Boolean
        Get
            Return mblnIsEligible
        End Get
        Set(ByVal value As Boolean)
            mblnIsEligible = value
        End Set
    End Property

    'S.SANDEEP [ 14 May 2013 ] -- START
    'ENHANCEMENT : TRA ENHANCEMENT
    ''' <summary>
    ''' Purpose: Get or Set statustypid
    ''' Modify By: Sandeep Sharma
    ''' </summary>
    Public Property _Statustypid() As Integer
        Get
            Return mintStatustypid
        End Get
        Set(ByVal value As Integer)
            mintStatustypid = Value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set issent
    ''' Modify By: Sandeep Sharma
    ''' </summary>
    Public Property _Issent() As Boolean
        Get
            Return mblnIssent
        End Get
        Set(ByVal value As Boolean)
            mblnIssent = Value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set vacancyunkid
    ''' Modify By: Sandeep Sharma
    ''' </summary>
    Public Property _Vacancyunkid() As Integer
        Get
            Return mintVacancyunkid
        End Get
        Set(ByVal value As Integer)
            mintVacancyunkid = Value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set applicantunkid
    ''' Modify By: Sandeep Sharma
    ''' </summary>
    Public Property _Applicantunkid() As Integer
        Get
            Return mintApplicantunkid
        End Get
        Set(ByVal value As Integer)
            mintApplicantunkid = Value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set approveuserunkid
    ''' Modify By: Sandeep Sharma
    ''' </summary>
    Public Property _Approveuserunkid() As Integer
        Get
            Return mintApproveuserunkid
        End Get
        Set(ByVal value As Integer)
            mintApproveuserunkid = Value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set appr_remark
    ''' Modify By: Sandeep Sharma
    ''' </summary>
    Public Property _Appr_Remark() As String
        Get
            Return mstrAppr_Remark
        End Get
        Set(ByVal value As String)
            mstrAppr_Remark = Value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set appr_date
    ''' Modify By: Sandeep Sharma
    ''' </summary>
    Public Property _Appr_Date() As Date
        Get
            Return mdtAppr_Date
        End Get
        Set(ByVal value As Date)
            mdtAppr_Date = Value
        End Set
    End Property
    'S.SANDEEP [ 14 May 2013 ] -- END

#End Region

    ''' <summary>
    ''' Modify By: Anjan
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>
    Public Sub GetData()
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        objDataOperation = New clsDataOperation

        Try
            strQ = "SELECT analysisunkid " & _
                       ",appbatchscheduletranunkid " & _
                       ",userunkid " & _
                       ",isvoid " & _
                       ",voiddatetime " & _
                       ",voiduserunkid " & _
                       ",voidreason " & _
                       ",iscomplete " & _
                       ",iseligible " & _
                   ",statustypid " & _
                   ",issent " & _
                   ",vacancyunkid " & _
                   ",applicantunkid " & _
                   ",appr_date " & _
                   ",approveuserunkid " & _
                   ",appr_remark " & _
                    "FROM rcinterviewanalysis_master " & _
                   "WHERE analysisunkid = @analysisunkid " 'S.SANDEEP [ 14 May 2013 {statustypid,issent,vacancyunkid,applicantunkid,appr_date,approveuserunkid,appr_remark} ] -- START -- END

            objDataOperation.AddParameter("@analysisunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintAnalysisunkid.ToString)

            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            For Each dtRow As DataRow In dsList.Tables(0).Rows
                mintAnalysisunkid = CInt(dtRow.Item("analysisunkid"))
                mintAppbatchscheduletranunkid = CInt(dtRow.Item("appbatchscheduletranunkid"))
                mintUserunkid = CInt(dtRow.Item("userunkid"))
                mblnIsvoid = CBool(dtRow.Item("isvoid"))
                If IsDBNull(dtRow.Item("voiddatetime")) Then
                    mdtVoidatetime = Nothing
                Else
                    mdtVoidatetime = dtRow.Item("voiddatetime")
                End If
                mintVoiduserunkid = CInt(dtRow.Item("voiduserunkid"))
                mstrVoidreason = dtRow.Item("voidreason").ToString
                mblnIsComplete = CBool(dtRow.Item("iscomplete"))
                mblnIsEligible = CBool(dtRow.Item("iseligible"))

                'S.SANDEEP [ 14 May 2013 ] -- START
                'ENHANCEMENT : TRA ENHANCEMENT
                mintStatustypid = CInt(dtRow.Item("statustypid"))
                mblnIssent = CBool(dtRow.Item("issent"))
                mintVacancyunkid = CInt(dtRow.Item("vacancyunkid"))
                mintApplicantunkid = CInt(dtRow.Item("applicantunkid"))
                mintApproveuserunkid = CInt(dtRow.Item("approveuserunkid"))
                mstrAppr_Remark = dtRow.Item("appr_remark").ToString
                If IsDBNull(dtRow.Item("appr_date")) Then
                    mdtAppr_Date = Nothing
                Else
                    mdtAppr_Date = dtRow.Item("appr_date")
                End If
                'S.SANDEEP [ 14 May 2013 ] -- END

                Exit For
            Next
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: GetData; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
    End Sub

    ''' <summary>
    ''' Modify By: Anjan
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>
    Public Function GetList(ByVal strTableName As String, Optional ByVal intEnrollId As Integer = -1) As DataSet
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        objDataOperation = New clsDataOperation

        Try
            'S.SANDEEP [ 10 June 2011 ] -- START
            'ISSUE : MULTIPLE INTERVIEW TYPE FOR ONE VACANCY
            'strQ = "SELECT " & _
            '             "analysistranunkid " & _
            '             ",rcinterviewanalysis_tran.interviewertranunkid " & _
            '             ",rcapplicant_master.firstname +' '+ rcapplicant_master.surname AS Applicant " & _
            '             ",cfcommon_master.name AS interviewtype " & _
            '             ",CONVERT(Char(8),analysis_date,112) AS analysisdate " & _
            '             ",CONVERT(CHAR(8),analysis_date,108) AS analysistime " & _
            '             ",CASE WHEN iscomplete = 0 THEN 'Incomplete' ELSE 'Complete' END AS status " & _
            '             ",CASE WHEN rcinterviewer_tran.interviewerunkid <> -1 " & _
            '                    "THEN hremployee_master.firstname +' ' + hremployee_master.surname " & _
            '                "ELSE rcinterviewer_tran.otherinterviewer_name END   AS reviewer " & _
            '             ",hrresult_master.resultname AS score " & _
            '             ",resultcodeunkid " & _
            '             ",rcinterviewanalysis_tran.remark " & _
            '             ",rcinterviewanalysis_master.iscomplete " & _
            '             ",rcinterviewanalysis_master.iseligible " & _
            '             ",rcinterviewanalysis_master.appbatchscheduletranunkid " & _
            '             ",rcinterviewanalysis_tran.analysisunkid " & _
            '             ",rcapplicant_master.applicantunkid " & _
            '             ",rcinterviewer_tran.interviewtypeunkid " & _
            '             ",rcinterviewer_tran.interviewer_level " & _
            '             ",rcinterviewer_tran.vacancyunkid " & _
            '       "FROM " & _
            '             "rcinterviewanalysis_tran " & _
            '             "JOIN rcinterviewanalysis_master ON rcinterviewanalysis_tran.analysisunkid = rcinterviewanalysis_master.analysisunkid " & _
            '             "LEFT JOIN rcapplicant_batchschedule_tran ON rcapplicant_batchschedule_tran.appbatchscheduletranunkid = rcinterviewanalysis_master.appbatchscheduletranunkid " & _
            '             "LEFT JOIN rcinterviewer_tran ON rcinterviewer_tran.interviewertranunkid=rcinterviewanalysis_tran.interviewertranunkid " & _
            '             "LEFT JOIN hremployee_master ON hremployee_master.employeeunkid=rcinterviewer_tran.interviewerunkid " & _
            '             "LEFT JOIN rcapplicant_master ON rcapplicant_master.applicantunkid=rcapplicant_batchschedule_tran.applicantunkid " & _
            '             "LEFT JOIN hrresult_master ON hrresult_master.resultunkid = rcinterviewanalysis_tran.resultcodeunkid " & _
            '             "LEFT JOIN cfcommon_master ON cfcommon_master.masterunkid=rcinterviewer_tran.interviewtypeunkid AND cfcommon_master.mastertype = " & enCommonMaster.INTERVIEW_TYPE & " " & _
            '       "WHERE ISNULL(rcapplicant_batchschedule_tran.iscancel,0)=0 " & _
            '           "AND ISNULL(rcapplicant_batchschedule_tran.isvoid,0)=0 " & _
            '           "AND ISNULL(rcinterviewanalysis_master.isvoid,0)=0 " & _
            '           "AND ISNULL(rcinterviewanalysis_tran.isvoid,0)=0 "



            'S.SANDEEP [ 25 DEC 2011 ] -- START
            'ENHANCEMENT : TRA CHANGES
            'strQ = "SELECT " & _
            '        "	 analysistranunkid " & _
            '        "	,rcinterviewanalysis_tran.interviewertranunkid " & _
            '        "	,rcapplicant_master.firstname + ' ' + rcapplicant_master.surname AS Applicant " & _
            '        "	,cfcommon_master.name AS interviewtype " & _
            '        "	,CONVERT(CHAR(8), analysis_date, 112) AS analysisdate " & _
            '        "	,CONVERT(CHAR(8), analysis_date, 108) AS analysistime " & _
            '        "	,CASE WHEN iscomplete = 0 THEN 'Incomplete' ELSE 'Complete' END AS status " & _
            '        "	,CASE WHEN rcinterviewer_tran.interviewerunkid <> -1 THEN hremployee_master.firstname + ' ' + hremployee_master.surname ELSE rcinterviewer_tran.otherinterviewer_name END AS reviewer " & _
            '        "	,hrresult_master.resultname AS score " & _
            '        "	,resultcodeunkid " & _
            '        "	,rcinterviewanalysis_tran.remark " & _
            '        "	,rcinterviewanalysis_master.iscomplete " & _
            '        "	,rcinterviewanalysis_master.iseligible " & _
            '        "	,rcinterviewanalysis_master.appbatchscheduletranunkid " & _
            '        "	,rcinterviewanalysis_tran.analysisunkid " & _
            '        "	,rcapplicant_master.applicantunkid " & _
            '        "	,rcinterviewer_tran.interviewtypeunkid " & _
            '        "	,rcinterviewer_tran.interviewer_level " & _
            '        "	,rcinterviewer_tran.vacancyunkid " & _
            '        "FROM rcinterviewanalysis_tran " & _
            '        "	JOIN rcinterviewanalysis_master ON rcinterviewanalysis_tran.analysisunkid = rcinterviewanalysis_master.analysisunkid " & _
            '        "	JOIN rcapplicant_batchschedule_tran ON rcapplicant_batchschedule_tran.appbatchscheduletranunkid = rcinterviewanalysis_master.appbatchscheduletranunkid " & _
            '        "	JOIN rcinterviewer_tran ON rcinterviewer_tran.interviewertranunkid = rcinterviewanalysis_tran.interviewertranunkid " & _
            '        "	LEFT JOIN hremployee_master ON hremployee_master.employeeunkid = rcinterviewer_tran.interviewerunkid " & _
            '        "	JOIN rcapplicant_master ON rcapplicant_master.applicantunkid = rcapplicant_batchschedule_tran.applicantunkid " & _
            '        "	JOIN hrresult_master ON hrresult_master.resultunkid = rcinterviewanalysis_tran.resultcodeunkid " & _
            '        "	JOIN rcbatchschedule_master ON rcbatchschedule_master.batchscheduleunkid = rcapplicant_batchschedule_tran.batchscheduleunkid " & _
            '        "	JOIN cfcommon_master ON cfcommon_master.masterunkid = rcbatchschedule_master.interviewtypeunkid AND cfcommon_master.mastertype = " & enCommonMaster.INTERVIEW_TYPE & " " & _
            '        "WHERE ISNULL(rcapplicant_batchschedule_tran.iscancel, 0) = 0 " & _
            '        "	AND ISNULL(rcapplicant_batchschedule_tran.isvoid, 0) = 0 " & _
            '        "	AND ISNULL(rcinterviewanalysis_master.isvoid, 0) = 0 " & _
            '        "	AND ISNULL(rcinterviewanalysis_tran.isvoid, 0) = 0 "

            'S.SANDEEP [ 28 FEB 2012 ] -- START
            'ENHANCEMENT : TRA CHANGES
            'strQ = "SELECT " & _
            '        "	 analysistranunkid " & _
            '        "	,rcinterviewanalysis_tran.interviewertranunkid " & _
            '        "	,rcapplicant_master.firstname + ' ' + rcapplicant_master.surname AS Applicant " & _
            '        "	,cfcommon_master.name AS interviewtype " & _
            '        "	,CONVERT(CHAR(8), analysis_date, 112) AS analysisdate " & _
            '        "	,CONVERT(CHAR(8), analysis_date, 108) AS analysistime " & _
            '        "	,CASE WHEN iscomplete = 0 THEN 'Incomplete' ELSE 'Complete' END AS status " & _
            '        "	,CASE WHEN rcinterviewer_tran.interviewerunkid <> -1 THEN hremployee_master.firstname + ' ' + hremployee_master.surname ELSE rcinterviewer_tran.otherinterviewer_name END AS reviewer " & _
            '        "  ,ISNULL(hrresult_master.resultname,'') END AS score " & _
            '        "	,resultcodeunkid " & _
            '        "	,rcinterviewanalysis_tran.remark " & _
            '        "	,rcinterviewanalysis_master.iscomplete " & _
            '        "	,rcinterviewanalysis_master.iseligible " & _
            '        "	,rcinterviewanalysis_master.appbatchscheduletranunkid " & _
            '        "	,rcinterviewanalysis_tran.analysisunkid " & _
            '        "	,rcapplicant_master.applicantunkid " & _
            '        "	,rcinterviewer_tran.interviewtypeunkid " & _
            '        "	,rcinterviewer_tran.interviewer_level " & _
            '        "	,rcinterviewer_tran.vacancyunkid " & _
            '        "   ,rcbatchschedule_master.batchscheduleunkid " & _
            '        "FROM rcinterviewanalysis_tran " & _
            '        "	JOIN rcinterviewanalysis_master ON rcinterviewanalysis_tran.analysisunkid = rcinterviewanalysis_master.analysisunkid " & _
            '        "	JOIN rcapplicant_batchschedule_tran ON rcapplicant_batchschedule_tran.appbatchscheduletranunkid = rcinterviewanalysis_master.appbatchscheduletranunkid " & _
            '        "	JOIN rcinterviewer_tran ON rcinterviewer_tran.interviewertranunkid = rcinterviewanalysis_tran.interviewertranunkid " & _
            '        "	LEFT JOIN hremployee_master ON hremployee_master.employeeunkid = rcinterviewer_tran.interviewerunkid " & _
            '        "	JOIN rcapplicant_master ON rcapplicant_master.applicantunkid = rcapplicant_batchschedule_tran.applicantunkid " & _
            '        "	LEFT JOIN hrresult_master ON hrresult_master.resultunkid = rcinterviewanalysis_tran.resultcodeunkid " & _
            '        "	JOIN rcbatchschedule_master ON rcbatchschedule_master.batchscheduleunkid = rcapplicant_batchschedule_tran.batchscheduleunkid " & _
            '        "	JOIN cfcommon_master ON cfcommon_master.masterunkid = rcbatchschedule_master.interviewtypeunkid AND cfcommon_master.mastertype = " & enCommonMaster.INTERVIEW_TYPE & " " & _
            '        "WHERE ISNULL(rcapplicant_batchschedule_tran.iscancel, 0) = 0 " & _
            '        "	AND ISNULL(rcapplicant_batchschedule_tran.isvoid, 0) = 0 " & _
            '        "	AND ISNULL(rcinterviewanalysis_master.isvoid, 0) = 0 " & _
            '        "	AND ISNULL(rcinterviewanalysis_tran.isvoid, 0) = 0 "

            strQ = "SELECT " & _
                    "	 analysistranunkid " & _
                    "	,rcinterviewanalysis_tran.interviewertranunkid " & _
                    "	,rcapplicant_master.firstname + ' ' + rcapplicant_master.surname AS Applicant " & _
                    "	,cfcommon_master.name AS interviewtype " & _
                    "	,CONVERT(CHAR(8), analysis_date, 112) AS analysisdate " & _
                    "	,CONVERT(CHAR(8), analysis_date, 108) AS analysistime " & _
                    "	,CASE WHEN iscomplete = 0 THEN @Incomplete ELSE @Complete END AS status " & _
                    "   ,ISNULL(CASE WHEN rcbatchschedule_interviewer_tran.interviewerunkid > 0 AND resultcodeunkid > 0  THEN hremployee_master.firstname + ' ' + hremployee_master.surname " & _
                    "                WHEN rcbatchschedule_interviewer_tran.interviewerunkid <= 0 AND resultcodeunkid > 0 THEN rcbatchschedule_interviewer_tran.otherinterviewer_name END " & _
                    "       ,ISNULL(hrmsConfiguration..cfuser_master.username,'')) AS reviewer " & _
                    "   ,CASE WHEN resultcodeunkid > 0 THEN ISNULL(hrresult_master.resultname,'') " & _
                    "         WHEN resultcodeunkid <=0 AND rcinterviewanalysis_master.iseligible = 1 THEN @Eligible " & _
                    "         WHEN resultcodeunkid <=0 AND rcinterviewanalysis_master.iseligible = 0 THEN @NotEligible " & _
                    "    END AS score " & _
                    "	,resultcodeunkid " & _
                    "	,rcinterviewanalysis_tran.remark " & _
                    "	,rcinterviewanalysis_master.iscomplete " & _
                    "	,rcinterviewanalysis_master.iseligible " & _
                    "	,rcinterviewanalysis_master.appbatchscheduletranunkid " & _
                    "	,rcinterviewanalysis_tran.analysisunkid " & _
                    "	,rcapplicant_master.applicantunkid " & _
                    "	,rcbatchschedule_interviewer_tran.interviewtypeunkid " & _
                    "	,rcbatchschedule_interviewer_tran.interviewer_level " & _
                    "	,rcbatchschedule_interviewer_tran.vacancyunkid " & _
                    "   ,rcbatchschedule_master.batchscheduleunkid " & _
                    "   ,rcinterviewanalysis_master.statustypid " & _
                    "   ,rcinterviewanalysis_master.issent " & _
                    "   ,rcinterviewanalysis_master.vacancyunkid " & _
                    "   ,rcinterviewanalysis_master.applicantunkid " & _
                    "   ,rcinterviewanalysis_master.appr_date " & _
                    "   ,rcinterviewanalysis_master.approveuserunkid " & _
                    "   ,rcinterviewanalysis_master.appr_remark " & _
                    "FROM rcinterviewanalysis_tran " & _
                    "	JOIN rcinterviewanalysis_master ON rcinterviewanalysis_tran.analysisunkid = rcinterviewanalysis_master.analysisunkid " & _
                    "   LEFT JOIN hrmsConfiguration..cfuser_master ON rcinterviewanalysis_master.userunkid = hrmsConfiguration..cfuser_master.userunkid " & _
                    "	JOIN rcapplicant_batchschedule_tran ON rcapplicant_batchschedule_tran.appbatchscheduletranunkid = rcinterviewanalysis_master.appbatchscheduletranunkid " & _
                    "	JOIN rcbatchschedule_interviewer_tran ON rcbatchschedule_interviewer_tran.batchscheduleinterviewertranunkid = rcinterviewanalysis_tran.interviewertranunkid " & _
                    "	LEFT JOIN hremployee_master ON hremployee_master.employeeunkid = rcbatchschedule_interviewer_tran.interviewerunkid " & _
                    "	JOIN rcapplicant_master ON rcapplicant_master.applicantunkid = rcapplicant_batchschedule_tran.applicantunkid " & _
                    "	LEFT JOIN hrresult_master ON hrresult_master.resultunkid = rcinterviewanalysis_tran.resultcodeunkid " & _
                    "	JOIN rcbatchschedule_master ON rcbatchschedule_master.batchscheduleunkid = rcapplicant_batchschedule_tran.batchscheduleunkid " & _
                    "	JOIN cfcommon_master ON cfcommon_master.masterunkid = rcbatchschedule_master.interviewtypeunkid AND cfcommon_master.mastertype = " & enCommonMaster.INTERVIEW_TYPE & " " & _
                    "WHERE ISNULL(rcapplicant_batchschedule_tran.iscancel, 0) = 0 " & _
                    "	AND ISNULL(rcapplicant_batchschedule_tran.isvoid, 0) = 0 " & _
                    "	AND ISNULL(rcinterviewanalysis_master.isvoid, 0) = 0 " & _
                    "	AND ISNULL(rcinterviewanalysis_tran.isvoid, 0) = 0 "
            'Hemant (30 Oct 2019) -- [rcinterviewer_tran --> rcbatchschedule_interviewer_tran]
            'S.SANDEEP [ 14 May 2013 {statustypid,issent,vacancyunkid,applicantunkid,appr_date} ] -- START -- END
            'S.SANDEEP [ 28 FEB 2012 ] -- END




            'Pinkal (07-Jan-2012) -- Start
            'Enhancement : TRA Changes
            strQ &= " AND rcapplicant_master.isimport = 0 "

            'Pinkal (07-Jan-2012) -- End


            'S.SANDEEP [ 25 DEC 2011 ] -- END


            'S.SANDEEP [ 10 June 2011 ] -- END

            'If intEnrollId > 0 Then
            'strQ &= "AND hrtraining_analysis_master.trainingenrolltranunkid = @EnrollId "
            'objDataOperation.AddParameter("@EnrollId", SqlDbType.Int, eZeeDataType.INT_SIZE, intEnrollId)
            'End If


            'S.SANDEEP [ 28 FEB 2012 ] -- START
            'ENHANCEMENT : TRA CHANGES
            objDataOperation.AddParameter("@Incomplete", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 2, "Incomplete"))
            objDataOperation.AddParameter("@Complete", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 3, "Complete"))
            objDataOperation.AddParameter("@Eligible", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 4, "Eligible"))
            objDataOperation.AddParameter("@NotEligible", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 5, "Not Eligible"))
            'S.SANDEEP [ 28 FEB 2012 ] -- END


            dsList = objDataOperation.ExecQuery(strQ, strTableName)

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: GetList; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
        Return dsList
    End Function

    ''' <summary>
    ''' Modify By: Anjan
    ''' </summary>
    ''' <returns>Boolean</returns>
    ''' <purpose> INSERT INTO Database Table (hrtraining_analysis_master) </purpose>
    Public Function Insert(Optional ByVal dtAnalysisTran As DataTable = Nothing, Optional ByVal isFinalAnalysis As Boolean = False) As Boolean
        If isFinalAnalysis = False Then
            If isExist(mintAppbatchscheduletranunkid) Then
                mstrMessage = Language.getMessage(mstrModuleName, 1, "Analysis for this Applicant is present.")
                Return False
            End If
        End If
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        objDataOperation = New clsDataOperation
        objDataOperation.BindTransaction()
        Try
            objDataOperation.AddParameter("@appbatchscheduletranunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintAppbatchscheduletranunkid.ToString)
            objDataOperation.AddParameter("@userunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintUserunkid.ToString)
            objDataOperation.AddParameter("@isvoid", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsvoid.ToString)
            If mdtVoidatetime = Nothing Then
                objDataOperation.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, DBNull.Value)
            Else
                objDataOperation.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, mdtVoidatetime)
            End If
            objDataOperation.AddParameter("@voiduserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintVoiduserunkid.ToString)
            objDataOperation.AddParameter("@voidreason", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrVoidreason.ToString)
            objDataOperation.AddParameter("@iseligible", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsEligible.ToString)
            objDataOperation.AddParameter("@iscomplete", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsComplete.ToString)
            'S.SANDEEP [ 14 May 2013 ] -- START
            'ENHANCEMENT : TRA ENHANCEMENT
            objDataOperation.AddParameter("@statustypid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintStatustypid.ToString)
            objDataOperation.AddParameter("@issent", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIssent.ToString)
            objDataOperation.AddParameter("@vacancyunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintVacancyunkid.ToString)
            objDataOperation.AddParameter("@applicantunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintApplicantunkid.ToString)
            objDataOperation.AddParameter("@approveuserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintApproveuserunkid.ToString)
            objDataOperation.AddParameter("@appr_remark", SqlDbType.Text, eZeeDataType.NAME_SIZE, mstrAppr_Remark.ToString)
            If mdtAppr_Date <> Nothing Then
                objDataOperation.AddParameter("@appr_date", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, mdtAppr_Date)
            Else
                objDataOperation.AddParameter("@appr_date", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, DBNull.Value)
            End If
            'S.SANDEEP [ 14 May 2013 ] -- END


            strQ = "INSERT INTO rcinterviewanalysis_master ( " & _
                     "  appbatchscheduletranunkid " & _
                     ", userunkid " & _
                     ", isvoid " & _
                     ", voiddatetime " & _
                     ", voiduserunkid " & _
                     ", voidreason " & _
                     ", iscomplete " & _
                     ", iseligible " & _
                     ", statustypid " & _
                     ", issent " & _
                     ", vacancyunkid " & _
                     ", applicantunkid " & _
                     ", approveuserunkid " & _
                     ", appr_remark " & _
                     ", appr_date" & _
                   ") VALUES (" & _
                     "  @appbatchscheduletranunkid " & _
                     ", @userunkid " & _
                     ", @isvoid " & _
                     ", @voiddatetime " & _
                     ", @voiduserunkid " & _
                     ", @voidreason " & _
                     ", @iscomplete " & _
                     ", @iseligible " & _
                     ", @statustypid " & _
                     ", @issent " & _
                     ", @vacancyunkid " & _
                     ", @applicantunkid " & _
                     ", @approveuserunkid " & _
                     ", @appr_remark " & _
                     ", @appr_date" & _
                    "); SELECT @@identity "
            'S.SANDEEP [ 14 May 2013 {statustypid,issent,vacancyunkid,applicantunkid,appr_date} ] -- START -- END

            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            mintAnalysisunkid = dsList.Tables(0).Rows(0).Item(0)


            Dim blnFlag As Boolean = False

            If dtAnalysisTran IsNot Nothing Then
                objAnalysisTran._AnalysisUnkid = mintAnalysisunkid
                objAnalysisTran._DataTable = dtAnalysisTran

                If objAnalysisTran._DataTable.Rows.Count > 0 Then
                    With objAnalysisTran
                        ._FormName = mstrFormName
                        ._LoginEmployeeunkid = mintLoginEmployeeunkid
                        ._ClientIP = mstrClientIP
                        ._HostName = mstrHostName
                        ._FromWeb = mblnIsWeb
                        ._AuditUserId = mintAuditUserId
._CompanyUnkid = mintCompanyUnkid
                        ._AuditDate = mdtAuditDate
                    End With
                    If objAnalysisTran.InsertUpdateDelete_AnalysisTran(objDataOperation) = False Then
                        exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                        Throw exForce
                    End If

                Else
                    'S.SANDEEP [28-May-2018] -- START
                    'ISSUE/ENHANCEMENT : {Audit Trails} 
                    Dim objCommonATLog As New clsCommonATLog
                    objCommonATLog._FormName = mstrFormName
                    objCommonATLog._LoginEmployeeUnkid = mintLoginEmployeeunkid
                    objCommonATLog._ClientIP = mstrClientIP
                    objCommonATLog._HostName = mstrHostName
                    objCommonATLog._FromWeb = mblnIsWeb
                    objCommonATLog._AuditUserId = mintAuditUserId
objCommonATLog._CompanyUnkid = mintCompanyUnkid
                    objCommonATLog._AuditDate = mdtAuditDate
                    'S.SANDEEP [28-May-2018] -- END

                    If objCommonATLog.Insert_TranAtLog(objDataOperation, "rcinterviewanalysis_master", "analysisunkid", mintAnalysisunkid, "rcinterviewanalysis_tran", "analysistranunkid", -1, 1, 0) = False Then
                        exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                        Throw exForce
                    End If

                    'S.SANDEEP [28-May-2018] -- START
                    'ISSUE/ENHANCEMENT : {Audit Trails} 
                    objCommonATLog = Nothing
                    'S.SANDEEP [28-May-2018] -- END

                End If
            End If

            'If isFinalAnalysis = True Then
            '    strQ = "UPDATE hrtraining_enrollment_tran SET " & _
            '            " status_id = 3 " & _
            '           "WHERE hrtraining_enrollment_tran.trainingenrolltranunkid = " & mintTrainingenrolltranunkid

            '    objDataOperation.ExecNonQuery(strQ)

            '    If objDataOperation.ErrorMessage <> "" Then
            '        exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
            '        Throw exForce
            '    End If
            'End If

            objDataOperation.ReleaseTransaction(True)

            Return True

        Catch ex As Exception
            objDataOperation.ReleaseTransaction(False)
            Throw New Exception(ex.Message & "; Procedure Name: Insert; Module Name: " & mstrModuleName)
            Return False
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
    End Function

    'Hemant (03 Jun 2020) -- Start
    'ISSUE/ENHANCEMENT(NMB) :  RECRUITMENTS FINAL UAT CHANGES(Point No - 1 : On the interview analysis screen, they want to record interview scores for all applicants in a single window instead of analysing the interview an applicant at a time. Also, they want to record all the interview scores for all the interviewers on the same screen.)
    Public Function InsertUpdateDelete_All(Optional ByVal dtAnalysis As DataTable = Nothing, Optional ByVal dtAnalysisTran As DataTable = Nothing, Optional ByVal isFinalAnalysis As Boolean = False) As Boolean

        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        objDataOperation = New clsDataOperation
        objDataOperation.BindTransaction()
        Try
            Dim objCommonATLog As New clsCommonATLog
            For i = 0 To dtAnalysis.Rows.Count - 1
                With dtAnalysis.Rows(i)
                    If Not IsDBNull(.Item("AUD")) Then
                        Select Case (.Item("AUD"))
                            Case "A"
                                mintAppbatchscheduletranunkid = CInt(.Item("Appbatchscheduletranunkid"))
                                mintVacancyunkid = CInt(.Item("Vacancyunkid"))
                                mintApplicantunkid = CInt(.Item("Applicantunkid"))

                                objDataOperation.ClearParameters()

                                objDataOperation.AddParameter("@appbatchscheduletranunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintAppbatchscheduletranunkid.ToString)
                                objDataOperation.AddParameter("@userunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintUserunkid.ToString)
                                objDataOperation.AddParameter("@isvoid", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsvoid.ToString)
                                If mdtVoidatetime = Nothing Then
                                    objDataOperation.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, DBNull.Value)
                                Else
                                    objDataOperation.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, mdtVoidatetime)
                                End If
                                objDataOperation.AddParameter("@voiduserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintVoiduserunkid.ToString)
                                objDataOperation.AddParameter("@voidreason", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrVoidreason.ToString)
                                objDataOperation.AddParameter("@iseligible", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsEligible.ToString)
                                objDataOperation.AddParameter("@iscomplete", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsComplete.ToString)
                                objDataOperation.AddParameter("@statustypid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintStatustypid.ToString)
                                objDataOperation.AddParameter("@issent", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIssent.ToString)
                                objDataOperation.AddParameter("@vacancyunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintVacancyunkid.ToString)
                                objDataOperation.AddParameter("@applicantunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintApplicantunkid.ToString)
                                objDataOperation.AddParameter("@approveuserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintApproveuserunkid.ToString)
                                objDataOperation.AddParameter("@appr_remark", SqlDbType.Text, eZeeDataType.NAME_SIZE, mstrAppr_Remark.ToString)
                                If mdtAppr_Date <> Nothing Then
                                    objDataOperation.AddParameter("@appr_date", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, mdtAppr_Date)
                                Else
                                    objDataOperation.AddParameter("@appr_date", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, DBNull.Value)
                                End If


                                strQ = "INSERT INTO rcinterviewanalysis_master ( " & _
                                         "  appbatchscheduletranunkid " & _
                                         ", userunkid " & _
                                         ", isvoid " & _
                                         ", voiddatetime " & _
                                         ", voiduserunkid " & _
                                         ", voidreason " & _
                                         ", iscomplete " & _
                                         ", iseligible " & _
                                         ", statustypid " & _
                                         ", issent " & _
                                         ", vacancyunkid " & _
                                         ", applicantunkid " & _
                                         ", approveuserunkid " & _
                                         ", appr_remark " & _
                                         ", appr_date" & _
                                       ") VALUES (" & _
                                         "  @appbatchscheduletranunkid " & _
                                         ", @userunkid " & _
                                         ", @isvoid " & _
                                         ", @voiddatetime " & _
                                         ", @voiduserunkid " & _
                                         ", @voidreason " & _
                                         ", @iscomplete " & _
                                         ", @iseligible " & _
                                         ", @statustypid " & _
                                         ", @issent " & _
                                         ", @vacancyunkid " & _
                                         ", @applicantunkid " & _
                                         ", @approveuserunkid " & _
                                         ", @appr_remark " & _
                                         ", @appr_date" & _
                                        "); SELECT @@identity "

                                dsList = objDataOperation.ExecQuery(strQ, "List")

                                If objDataOperation.ErrorMessage <> "" Then
                                    exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                                    Throw exForce
                                End If

                                mintAnalysisunkid = dsList.Tables(0).Rows(0).Item(0)


                                Dim blnFlag As Boolean = False

                                If dtAnalysisTran IsNot Nothing Then
                                    objAnalysisTran._AnalysisUnkid = mintAnalysisunkid
                                    dtAnalysisTran.DefaultView.RowFilter = "applicantunkid =" & mintApplicantunkid
                                    objAnalysisTran._DataTable = dtAnalysisTran.DefaultView.ToTable

                                    If objAnalysisTran._DataTable.Rows.Count > 0 Then

                                        If objAnalysisTran.InsertUpdateDeleteAll_AnalysisTran(objDataOperation) = False Then
                                            exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                                            Throw exForce
                                        End If

                                    Else

                                        objCommonATLog._FormName = mstrFormName
                                        objCommonATLog._LoginEmployeeUnkid = mintLoginEmployeeunkid
                                        objCommonATLog._ClientIP = mstrClientIP
                                        objCommonATLog._HostName = mstrHostName
                                        objCommonATLog._FromWeb = mblnIsWeb
                                        objCommonATLog._AuditUserId = mintAuditUserId
                                        objCommonATLog._CompanyUnkid = mintCompanyUnkid
                                        objCommonATLog._AuditDate = mdtAuditDate

                                        If objCommonATLog.Insert_TranAtLog(objDataOperation, "rcinterviewanalysis_master", "analysisunkid", mintAnalysisunkid, "rcinterviewanalysis_tran", "analysistranunkid", -1, 1, 0) = False Then
                                            exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                                            Throw exForce
                                        End If
                                    End If
                                End If
                            Case "U"

                                mintAppbatchscheduletranunkid = CInt(.Item("Appbatchscheduletranunkid"))
                                mintVacancyunkid = CInt(.Item("Vacancyunkid"))
                                mintApplicantunkid = CInt(.Item("Applicantunkid"))
                                mintAnalysisunkid = CInt(.Item("Analysisunkid"))

                                objDataOperation.ClearParameters()

                                objDataOperation.AddParameter("@analysisunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintAnalysisunkid.ToString)
                                objDataOperation.AddParameter("@appbatchscheduletranunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintAppbatchscheduletranunkid.ToString)
                                objDataOperation.AddParameter("@userunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintUserunkid.ToString)
                                objDataOperation.AddParameter("@iscomplete", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsComplete.ToString)
                                objDataOperation.AddParameter("@iseligible", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsEligible.ToString)

                                objDataOperation.AddParameter("@statustypid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintStatustypid.ToString)
                                objDataOperation.AddParameter("@issent", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIssent.ToString)
                                objDataOperation.AddParameter("@vacancyunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintVacancyunkid.ToString)
                                objDataOperation.AddParameter("@applicantunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintApplicantunkid.ToString)
                                objDataOperation.AddParameter("@approveuserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintApproveuserunkid.ToString)
                                objDataOperation.AddParameter("@appr_remark", SqlDbType.Text, eZeeDataType.NAME_SIZE, mstrAppr_Remark.ToString)
                                If mdtAppr_Date <> Nothing Then
                                    objDataOperation.AddParameter("@appr_date", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, mdtAppr_Date)
                                Else
                                    objDataOperation.AddParameter("@appr_date", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, DBNull.Value)
                                End If

                                strQ = "UPDATE rcinterviewanalysis_master SET " & _
                                         "  appbatchscheduletranunkid = @appbatchscheduletranunkid " & _
                                         ", userunkid = @userunkid " & _
                                         ", iscomplete =@iscomplete " & _
                                         ", iseligible = @iseligible " & _
                                         ", statustypid = @statustypid" & _
                                         ", issent = @issent" & _
                                         ", vacancyunkid = @vacancyunkid" & _
                                         ", applicantunkid = @applicantunkid " & _
                                         ", approveuserunkid = @approveuserunkid " & _
                                         ", appr_remark = @appr_remark " & _
                                         ", appr_date = @appr_date " & _
                                       "WHERE analysisunkid = @analysisunkid "

                                Call objDataOperation.ExecNonQuery(strQ)

                                If objDataOperation.ErrorMessage <> "" Then
                                    exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                                    Throw exForce
                                End If

                                Dim blnFlag As Boolean = False


                                If dtAnalysisTran IsNot Nothing Then
                                    objAnalysisTran._AnalysisUnkid = mintAnalysisunkid

                                    objAnalysisTran._DataTable = dtAnalysisTran.Copy()
                                    objAnalysisTran._DataTable.DefaultView.RowFilter = "analysisunkid =" & mintAnalysisunkid
                                    objAnalysisTran._DataTable = objAnalysisTran._DataTable.DefaultView.ToTable
                                    objAnalysisTran._Voiduserunkid = mintUserunkid

                                    Dim dt() As DataRow = objAnalysisTran._DataTable.Select("AUD=''")
                                    If dt.Length = objAnalysisTran._DataTable.Rows.Count Then
                                        objCommonATLog = New clsCommonATLog
                                        objCommonATLog._FormName = mstrFormName
                                        objCommonATLog._LoginEmployeeUnkid = mintLoginEmployeeunkid
                                        objCommonATLog._ClientIP = mstrClientIP
                                        objCommonATLog._HostName = mstrHostName
                                        objCommonATLog._FromWeb = mblnIsWeb
                                        objCommonATLog._AuditUserId = mintAuditUserId
                                        objCommonATLog._CompanyUnkid = mintCompanyUnkid
                                        objCommonATLog._AuditDate = mdtAuditDate
                                        If objCommonATLog.IsTableDataUpdate("atcommon_tranlog", "rcinterviewanalysis_master", mintAnalysisunkid, "analysisunkid", 2) Then

                                            If objCommonATLog.Insert_TranAtLog(objDataOperation, "rcinterviewanalysis_master", "analysisunkid", mintAnalysisunkid, "rcinterviewanalysis_tran", "analysistranunkid", -1, 2, 0) = False Then
                                                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                                                Throw exForce
                                            End If

                                        End If

                                    Else
                                        If objAnalysisTran.InsertUpdateDeleteAll_AnalysisTran(objDataOperation) = False Then
                                            exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                                            Throw exForce
                                        End If
                                    End If


                                Else
                                    objCommonATLog = New clsCommonATLog
                                    objCommonATLog._FormName = mstrFormName
                                    objCommonATLog._LoginEmployeeUnkid = mintLoginEmployeeunkid
                                    objCommonATLog._ClientIP = mstrClientIP
                                    objCommonATLog._HostName = mstrHostName
                                    objCommonATLog._FromWeb = mblnIsWeb
                                    objCommonATLog._AuditUserId = mintAuditUserId
                                    objCommonATLog._CompanyUnkid = mintCompanyUnkid
                                    objCommonATLog._AuditDate = mdtAuditDate
                                    If objCommonATLog.IsTableDataUpdate("atcommon_tranlog", "rcinterviewanalysis_master", mintAnalysisunkid, "analysisunkid", 2) Then

                                        If objCommonATLog.Insert_TranAtLog(objDataOperation, "rcinterviewanalysis_master", "analysisunkid", mintAnalysisunkid, "rcinterviewanalysis_tran", "analysistranunkid", -1, 2, 0) = False Then
                                            exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                                            Throw exForce
                                        End If

                                    End If

                                End If
                            Case "D"

                                mintAppbatchscheduletranunkid = CInt(.Item("Appbatchscheduletranunkid"))
                                mintVacancyunkid = CInt(.Item("Vacancyunkid"))
                                mintApplicantunkid = CInt(.Item("Applicantunkid"))
                                mintAnalysisunkid = CInt(.Item("Analysisunkid"))

                                strQ = "UPDATE rcinterviewanalysis_master SET " & _
                                    " isvoid = 1 " & _
                                    ", voiddatetime = getdate() " & _
                                    ", voiduserunkid = @voiduserunkid " & _
                                    ", voidreason = @voidreason " & _
                                   "WHERE analysisunkid = @analysisunkid "

                                objDataOperation.ClearParameters()
                                objDataOperation.AddParameter("@analysisunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintAnalysisunkid)
                                If mdtVoidatetime = Nothing Then
                                    objDataOperation.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, DBNull.Value)
                                Else
                                    objDataOperation.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, mdtVoidatetime)
                                End If
                                objDataOperation.AddParameter("@voiduserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintVoiduserunkid)
                                objDataOperation.AddParameter("@voidreason", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrVoidreason)
                                Call objDataOperation.ExecNonQuery(strQ)

                                If objDataOperation.ErrorMessage <> "" Then
                                    exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                                    Throw exForce
                                End If

                                Dim blnFlag As Boolean = False

                                objAnalysisTran._AnalysisUnkid = mintAnalysisunkid


                                blnFlag = objAnalysisTran.VoidAll(objDataOperation, mintAnalysisunkid, True, mintVoiduserunkid, mdtVoidatetime, mstrVoidreason)

                        End Select
                    End If
                End With



            Next
            objDataOperation.ReleaseTransaction(True)

            Return True

        Catch ex As Exception
            objDataOperation.ReleaseTransaction(False)
            Throw New Exception(ex.Message & "; Procedure Name: InsertAll; Module Name: " & mstrModuleName)
            Return False
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
    End Function
    'Hemant (03 Jun 2020) -- End
    ''' <summary>
    ''' Modify By: Anjan
    ''' </summary>
    ''' <returns>Boolean</returns>
    ''' <purpose> Update Database Table (hrtraining_analysis_master) </purpose>
    Public Function Update(Optional ByVal dtAnalysisTran As DataTable = Nothing, Optional ByVal isFinalAnalysis As Boolean = False) As Boolean
        If isFinalAnalysis = False Then
            If isExist(mintAppbatchscheduletranunkid, mintAnalysisunkid) Then
                mstrMessage = Language.getMessage(mstrModuleName, 6, "Analysis for this Interviewer is already present.")
                Return False
            End If
        End If

        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        objDataOperation = New clsDataOperation
        objDataOperation.BindTransaction()
        Try
            objDataOperation.AddParameter("@analysisunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintAnalysisunkid.ToString)
            objDataOperation.AddParameter("@appbatchscheduletranunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintAppbatchscheduletranunkid.ToString)
            objDataOperation.AddParameter("@userunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintUserunkid.ToString)
            objDataOperation.AddParameter("@iscomplete", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsComplete.ToString)
            'S.SANDEEP [ 25 DEC 2011 ] -- START
            'ENHANCEMENT : TRA CHANGES
            objDataOperation.AddParameter("@iseligible", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsEligible.ToString)
            'S.SANDEEP [ 25 DEC 2011 ] -- END

            'S.SANDEEP [ 14 May 2013 ] -- START
            'ENHANCEMENT : TRA ENHANCEMENT
            objDataOperation.AddParameter("@statustypid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintStatustypid.ToString)
            objDataOperation.AddParameter("@issent", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIssent.ToString)
            objDataOperation.AddParameter("@vacancyunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintVacancyunkid.ToString)
            objDataOperation.AddParameter("@applicantunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintApplicantunkid.ToString)
            objDataOperation.AddParameter("@approveuserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintApproveuserunkid.ToString)
            objDataOperation.AddParameter("@appr_remark", SqlDbType.Text, eZeeDataType.NAME_SIZE, mstrAppr_Remark.ToString)
            If mdtAppr_Date <> Nothing Then
                objDataOperation.AddParameter("@appr_date", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, mdtAppr_Date)
            Else
                objDataOperation.AddParameter("@appr_date", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, DBNull.Value)
            End If
            'S.SANDEEP [ 14 May 2013 ] -- END



            'S.SANDEEP [ 25 DEC 2011 ] -- START
            'ENHANCEMENT : TRA CHANGES
            'strQ = "UPDATE rcinterviewanalysis_master SET " & _
            '                     "  appbatchscheduletranunkid = @appbatchscheduletranunkid " & _
            '                     ", userunkid = @userunkid " & _
            '                     ", iscomplete =@iscomplete " & _
            '                     "WHERE analysisunkid = @analysisunkid "

            strQ = "UPDATE rcinterviewanalysis_master SET " & _
                     "  appbatchscheduletranunkid = @appbatchscheduletranunkid " & _
                     ", userunkid = @userunkid " & _
                     ", iscomplete =@iscomplete " & _
                     ", iseligible = @iseligible " & _
                     ", statustypid = @statustypid" & _
                     ", issent = @issent" & _
                     ", vacancyunkid = @vacancyunkid" & _
                     ", applicantunkid = @applicantunkid " & _
                     ", approveuserunkid = @approveuserunkid " & _
                     ", appr_remark = @appr_remark " & _
                     ", appr_date = @appr_date " & _
                   "WHERE analysisunkid = @analysisunkid " 'S.SANDEEP [ 14 May 2013 {statustypid,issent,vacancyunkid,applicantunkid} ] -- START -- END
            'S.SANDEEP [ 25 DEC 2011 ] -- END



            Call objDataOperation.ExecNonQuery(strQ)

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            Dim blnFlag As Boolean = False


            'Pinkal (24-Jul-2012) -- Start
            'Enhancement : TRA Changes

            If dtAnalysisTran IsNot Nothing Then
                objAnalysisTran._AnalysisUnkid = mintAnalysisunkid
                'S.SANDEEP [ 14 May 2013 ] -- START
                'ENHANCEMENT : TRA ENHANCEMENT
                'objAnalysisTran._DataTable = dtAnalysisTran
                objAnalysisTran._DataTable = dtAnalysisTran.Copy
                'S.SANDEEP [ 14 May 2013 ] -- END



                Dim dt() As DataRow = objAnalysisTran._DataTable.Select("AUD=''")
                If dt.Length = objAnalysisTran._DataTable.Rows.Count Then

                    'S.SANDEEP [28-May-2018] -- START
                    'ISSUE/ENHANCEMENT : {Audit Trails} 
                    Dim objCommonATLog As New clsCommonATLog
                    objCommonATLog._FormName = mstrFormName
                    objCommonATLog._LoginEmployeeUnkid = mintLoginEmployeeunkid
                    objCommonATLog._ClientIP = mstrClientIP
                    objCommonATLog._HostName = mstrHostName
                    objCommonATLog._FromWeb = mblnIsWeb
                    objCommonATLog._AuditUserId = mintAuditUserId
objCommonATLog._CompanyUnkid = mintCompanyUnkid
                    objCommonATLog._AuditDate = mdtAuditDate
                    'S.SANDEEP [28-May-2018] -- END

                    If objCommonATLog.IsTableDataUpdate("atcommon_tranlog", "rcinterviewanalysis_master", mintAnalysisunkid, "analysisunkid", 2) Then

                        If objCommonATLog.Insert_TranAtLog(objDataOperation, "rcinterviewanalysis_master", "analysisunkid", mintAnalysisunkid, "rcinterviewanalysis_tran", "analysistranunkid", -1, 2, 0) = False Then
                            exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                            Throw exForce
            End If

                    End If

                    'S.SANDEEP [28-May-2018] -- START
                    'ISSUE/ENHANCEMENT : {Audit Trails} 
                    objCommonATLog = Nothing
                    'S.SANDEEP [28-May-2018] -- END


                Else
                    With objAnalysisTran
                        ._FormName = mstrFormName
                        ._LoginEmployeeunkid = mintLoginEmployeeunkid
                        ._ClientIP = mstrClientIP
                        ._HostName = mstrHostName
                        ._FromWeb = mblnIsWeb
                        ._AuditUserId = mintAuditUserId
._CompanyUnkid = mintCompanyUnkid
                        ._AuditDate = mdtAuditDate
                    End With
                    If objAnalysisTran.InsertUpdateDelete_AnalysisTran(objDataOperation) = False Then
                        exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                        Throw exForce
                    End If
                End If


            Else
                'S.SANDEEP [28-May-2018] -- START
                'ISSUE/ENHANCEMENT : {Audit Trails} 
                Dim objCommonATLog As New clsCommonATLog
                objCommonATLog._FormName = mstrFormName
                objCommonATLog._LoginEmployeeUnkid = mintLoginEmployeeunkid
                objCommonATLog._ClientIP = mstrClientIP
                objCommonATLog._HostName = mstrHostName
                objCommonATLog._FromWeb = mblnIsWeb
                objCommonATLog._AuditUserId = mintAuditUserId
objCommonATLog._CompanyUnkid = mintCompanyUnkid
                objCommonATLog._AuditDate = mdtAuditDate
                'S.SANDEEP [28-May-2018] -- END

                If objCommonATLog.IsTableDataUpdate("atcommon_tranlog", "rcinterviewanalysis_master", mintAnalysisunkid, "analysisunkid", 2) Then

                    If objCommonATLog.Insert_TranAtLog(objDataOperation, "rcinterviewanalysis_master", "analysisunkid", mintAnalysisunkid, "rcinterviewanalysis_tran", "analysistranunkid", -1, 2, 0) = False Then
                        exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                        Throw exForce
                    End If

                End If

                'S.SANDEEP [28-May-2018] -- START
                'ISSUE/ENHANCEMENT : {Audit Trails} 
                objCommonATLog = Nothing
                'S.SANDEEP [28-May-2018] -- END


            End If

            'Pinkal (24-Jul-2012) -- End


            objDataOperation.ReleaseTransaction(True)

            Return True
        Catch ex As Exception
            objDataOperation.ReleaseTransaction(False)
            Throw New Exception(ex.Message & "; Procedure Name: Update; Module Name: " & mstrModuleName)
            Return False
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
    End Function

    ''' <summary>
    ''' Modify By: Anjan
    ''' </summary>
    ''' <returns>Boolean</returns>
    ''' <purpose> Delete Database Table (rcrecruitment_analysis_master) </purpose>
    Public Function Delete(ByVal intUnkid As Integer) As Boolean
        'If isUsed(intUnkid) Then
        '    mstrMessage = "<Message>"
        '    Return False
        'End If

        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        objDataOperation = New clsDataOperation
        objDataOperation.BindTransaction()
        Try

            strQ = "UPDATE rcinterviewanalysis_master SET " & _
                     " isvoid = @isvoid " & _
                     ", voiddatetime = @voiddatetime " & _
                     ", voiduserunkid = @voiduserunkid " & _
                     ", voidreason = @voidreason " & _
                    "WHERE analysisunkid = @analysisunkid "

            objDataOperation.AddParameter("@analysisunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intUnkid)
            objDataOperation.AddParameter("@isvoid", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsvoid.ToString)
            If mdtVoidatetime = Nothing Then
                objDataOperation.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, DBNull.Value)
            Else
                objDataOperation.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, mdtVoidatetime)
            End If
            objDataOperation.AddParameter("@voiduserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintVoiduserunkid.ToString)
            objDataOperation.AddParameter("@voidreason", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrVoidreason.ToString)
            Call objDataOperation.ExecNonQuery(strQ)

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            Dim blnFlag As Boolean = False
            ''''''''''''''''' Voiding All Resources
            objAnalysisTran._AnalysisUnkid = intUnkid

            With objAnalysisTran
                ._FormName = mstrFormName
                ._LoginEmployeeunkid = mintLoginEmployeeunkid
                ._ClientIP = mstrClientIP
                ._HostName = mstrHostName
                ._FromWeb = mblnIsWeb
                ._AuditUserId = mintAuditUserId
._CompanyUnkid = mintCompanyUnkid
                ._AuditDate = mdtAuditDate
            End With

            'Pinkal (24-Jul-2012) -- Start
            'Enhancement : TRA Changes
            'blnFlag = objAnalysisTran.VoidAll(mintAppbatchscheduletranunkid, mblnIsvoid, mintVoiduserunkid, mdtVoidatetime, mstrVoidreason)

            'S.SANDEEP [ 14 May 2013 ] -- START
            'ENHANCEMENT : TRA ENHANCEMENT
            'blnFlag = objAnalysisTran.VoidAll(objDataOperation, mintAppbatchscheduletranunkid, mblnIsvoid, mintVoiduserunkid, mdtVoidatetime, mstrVoidreason)
            blnFlag = objAnalysisTran.VoidAll(objDataOperation, intUnkid, mblnIsvoid, mintVoiduserunkid, mdtVoidatetime, mstrVoidreason)
            'S.SANDEEP [ 14 May 2013 ] -- END

            'Pinkal (24-Jul-2012) -- End


            If blnFlag = False Then objDataOperation.ReleaseTransaction(False)

            objDataOperation.ReleaseTransaction(True)

            Return True

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: Update; Module Name: " & mstrModuleName)
            Return False
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
    End Function

    ''' <summary>
    ''' Modify By: Anjan
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>
    Public Function isUsed(ByVal intUnkid As Integer) As Boolean
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        objDataOperation = New clsDataOperation

        Try
            strQ = "SELECT  analysisunkid " & _
                    "FROM rcinterviewanalysis_tran " & _
                    "WHERE analysisunkid = @analysisunkid " & _
                    "AND isvoid = 0 "

            objDataOperation.AddParameter("@analysisunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intUnkid)

            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            If dsList.Tables(0).Rows.Count > 0 Then
                Return True
            Else
                Return False
            End If
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: isUsed; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
    End Function

    ''' <summary>
    ''' Modify By: Anjan
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>
    Public Function isExist(ByVal intAppbatchscheduleUnkid As Integer, Optional ByVal intUnkid As Integer = -1) As Boolean
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        objDataOperation = New clsDataOperation

        Try
            strQ = "SELECT " & _
                         "  analysisunkid " & _
                         ", appbatchscheduletranunkid " & _
                         ", userunkid " & _
                         ", isvoid " & _
                         ", voiddatetime " & _
                         ", voiduserunkid " & _
                         ", voidreason " & _
                         ", iscomplete " & _
                         ", iseligible " & _
                   "FROM rcinterviewanalysis_master " & _
                   "WHERE appbatchscheduletranunkid = @appbatchscheduletranunkid " & _
                   " AND isvoid = 0 "

            If intUnkid > 0 Then
                strQ &= " AND analysisunkid <> @analysisunkid "
            End If

            objDataOperation.AddParameter("@appbatchscheduletranunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intAppbatchscheduleUnkid)
            objDataOperation.AddParameter("@analysisunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intUnkid)

            dsList = objDataOperation.ExecQuery(strQ, "List")


            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            Return dsList.Tables(0).Rows.Count > 0
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: isExist; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
    End Function

    Public Function Get_Analysis_Data(Optional ByVal strListName As String = "List", Optional ByVal intAppBatchScheduleId As Integer = -1, Optional ByVal intVacancyunkid As Integer = -1) As DataSet
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        objDataOperation = New clsDataOperation
        Try

            'Sandeep [ 21 Aug 2010 ] -- Start
            'strQ = "SELECT  CONVERT(CHAR(8),rcinterviewanalysis_tran.analysis_date,112) AS analysisdate " & _
            '                            ", CASE WHEN rcinterviewer_tran.interviewerunkid =-1 THEN ISNULL(rcinterviewer_tran.otherinterviewer_name,'') " & _
            '                                    "WHEN rcinterviewer_tran.interviewerunkid >0 THEN ISNULL(hremployee_master.firstname,'') +' ' + " & _
            '                                                                       "ISNULL(hremployee_master.othername,'')+' ' + " & _
            '                                                                       "ISNULL(hremployee_master.surname,'') " & _
            '                                "END AS reviewer " & _
            '                            ",hrresult_master.resultname AS score " & _
            '                            ",rcinterviewanalysis_tran.remark " & _
            '                            ",rcinterviewanalysis_tran.analysistranunkid " & _
            '                            ",rcinterviewanalysis_master.analysisunkid " & _
            '                            ",hrresult_master.resultgroupunkid " & _
            '                    "FROM rcinterviewanalysis_tran " & _
            '                      "LEFT JOIN rcinterviewer_tran ON rcinterviewer_tran.interviewertranunkid=rcinterviewanalysis_tran.interviewertranunkid " & _
            '                      "LEFT JOIN hremployee_master ON hremployee_master.employeeunkid=rcinterviewer_tran.interviewerunkid " & _
            '                      "LEFT JOIN hrresult_master ON rcinterviewanalysis_tran.resultcodeunkid=hrresult_master.resultcode " & _
            '                      "LEFT JOIN rcinterviewanalysis_master ON rcinterviewanalysis_master.analysisunkid=rcinterviewanalysis_tran.analysisunkid " & _
            '                    "WHERE ISNULL(rcinterviewanalysis_master.isvoid,0)=0 " & _
            '                       "AND ISNULL(rcinterviewanalysis_tran.isvoid ,0)=0 "


            'Pinkal (07-Jan-2012) -- Start
            'Enhancement : TRA Changes


            'strQ = "SELECT  CONVERT(CHAR(8),rcinterviewanalysis_tran.analysis_date,112) AS analysisdate " & _
            '                ", CASE WHEN rcinterviewer_tran.interviewerunkid =-1 THEN ISNULL(rcinterviewer_tran.otherinterviewer_name,'') " & _
            '                        "WHEN rcinterviewer_tran.interviewerunkid >0 THEN ISNULL(hremployee_master.firstname,'') +' ' + " & _
            '                                                           "ISNULL(hremployee_master.othername,'')+' ' + " & _
            '                                                           "ISNULL(hremployee_master.surname,'') " & _
            '                    "END AS reviewer " & _
            '                ",hrresult_master.resultname AS score " & _
            '                ",rcinterviewanalysis_tran.remark " & _
            '                ",rcinterviewanalysis_tran.analysistranunkid " & _
            '                ",rcinterviewanalysis_master.analysisunkid " & _
            '                ",hrresult_master.resultgroupunkid " & _
            '        "FROM rcinterviewanalysis_tran " & _
            '          "LEFT JOIN rcinterviewer_tran ON rcinterviewer_tran.interviewertranunkid=rcinterviewanalysis_tran.interviewertranunkid " & _
            '          "LEFT JOIN hremployee_master ON hremployee_master.employeeunkid=rcinterviewer_tran.interviewerunkid " & _
            '          "LEFT JOIN hrresult_master ON rcinterviewanalysis_tran.resultcodeunkid=hrresult_master.resultunkid " & _
            '          "LEFT JOIN rcinterviewanalysis_master ON rcinterviewanalysis_master.analysisunkid=rcinterviewanalysis_tran.analysisunkid " & _
            '        "WHERE ISNULL(rcinterviewanalysis_master.isvoid,0)=0 " & _
            '           "AND ISNULL(rcinterviewanalysis_tran.isvoid ,0)=0 "
            'Sandeep [ 21 Aug 2010 ] -- End 


            strQ = "SELECT cfcommon_master.name as InterviewType, CONVERT(CHAR(8),rcinterviewanalysis_tran.analysis_date,112) AS analysisdate " & _
                            ", CASE WHEN rcbatchschedule_interviewer_tran.interviewerunkid =-1 THEN ISNULL(rcbatchschedule_interviewer_tran.otherinterviewer_name,'') " & _
                                    "WHEN rcbatchschedule_interviewer_tran.interviewerunkid >0 THEN ISNULL(hremployee_master.firstname,'') +' ' + " & _
                                                                       "ISNULL(hremployee_master.othername,'')+' ' + " & _
                                                                       "ISNULL(hremployee_master.surname,'') " & _
                                "END AS reviewer " & _
                            ",hrresult_master.resultname AS score " & _
                            ",rcinterviewanalysis_tran.remark " & _
                            ",rcinterviewanalysis_tran.analysistranunkid " & _
                            ",rcinterviewanalysis_master.analysisunkid " & _
                            ",hrresult_master.resultgroupunkid " & _
                    "FROM rcinterviewanalysis_tran " & _
                      "LEFT JOIN rcbatchschedule_interviewer_tran ON rcbatchschedule_interviewer_tran.interviewertranunkid=rcinterviewanalysis_tran.interviewertranunkid " & _
                      " JOIN cfcommon_master ON cfcommon_master.masterunkid = rcbatchschedule_interviewer_tran.interviewtypeunkid AND cfcommon_master.mastertype = " & clsCommon_Master.enCommonMaster.INTERVIEW_TYPE & _
                      "LEFT JOIN hremployee_master ON hremployee_master.employeeunkid=rcbatchschedule_interviewer_tran.interviewerunkid " & _
                      "LEFT JOIN hrresult_master ON rcinterviewanalysis_tran.resultcodeunkid=hrresult_master.resultunkid " & _
                      "LEFT JOIN rcinterviewanalysis_master ON rcinterviewanalysis_master.analysisunkid=rcinterviewanalysis_tran.analysisunkid " & _
                    "WHERE ISNULL(rcinterviewanalysis_master.isvoid,0)=0 " & _
                       "AND ISNULL(rcinterviewanalysis_tran.isvoid ,0)=0 "


            If intAppBatchScheduleId > 0 Then
                strQ &= "AND rcinterviewanalysis_master.appbatchscheduletranunkid = " & intAppBatchScheduleId
            End If

            If intVacancyunkid > 0 Then
                strQ &= "AND rcbatchschedule_interviewer_tran.vacancyunkid = " & intVacancyunkid
            End If

            strQ &= " ORDER BY  CONVERT(CHAR(8),rcinterviewanalysis_tran.analysis_date,112) "
            'Hemant (30 Oct 2019) -- [rcinterviewer_tran --> rcbatchschedule_interviewer_tran]
            'Pinkal (07-Jan-2012) -- End

            dsList = objDataOperation.ExecQuery(strQ, strListName)

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            Return dsList

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "Get_Analysis_Data", mstrModuleName)
            Return Nothing
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
    End Function

    Public Function MarkEligible_Status(ByVal intAnalysisUnkid As Integer) As Boolean

        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        objDataOperation = New clsDataOperation

        Try
            objDataOperation.AddParameter("@analysisunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intAnalysisUnkid.ToString)
            'objDataOperation.AddParameter("@appbatchscheduletranunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintAppbatchscheduletranunkid.ToString)
            'objDataOperation.AddParameter("@userunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintUserunkid.ToString)
            objDataOperation.AddParameter("@iseligible", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsEligible.ToString)

            strQ = "UPDATE rcinterviewanalysis_master SET " & _
                       " iseligible =@iseligible " & _
                   "WHERE analysisunkid = @analysisunkid "

            Call objDataOperation.ExecNonQuery(strQ)

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If


            Return True
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: MarkEligible_Status; Module Name: " & mstrModuleName)
            Return False
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
    End Function


    'Sandeep [ 09 Oct 2010 ] -- Start
    'Public Function GetList_FinalApplicant(ByVal strTableName As String) As DataSet

    ''S.SANDEEP [ 25 DEC 2011 ] -- START
    ''ENHANCEMENT : TRA CHANGES
    '''' <summary>
    '''' Modify By: Anjan
    '''' </summary>
    '''' <param name="intApplicanttype">0 = All Eligible Applicant, 1 = External Eligible Applicant, 2 = Internal Eligible Applicant</param>
    ''''Public Function GetList_FinalApplicant(ByVal strTableName As String, Optional ByVal blnIsImport As Boolean = False) As DataSet
    'Public Function GetList_FinalApplicant(ByVal strTableName As String, Optional ByVal blnIsImport As Boolean = False, Optional ByVal intApplicanttype As Integer = 0) As DataSet
    '    'S.SANDEEP [ 25 DEC 2011 ] -- END


    '    'Sandeep [ 09 Oct 2010 ] -- End 
    '    Dim dsList As DataSet = Nothing
    '    Dim strQ As String = ""
    '    Dim exForce As Exception

    '    objDataOperation = New clsDataOperation

    '    Try
    '        strQ = "SELECT " & _
    '                    "rcapplicant_master.firstname +' ' + rcapplicant_master.othername+' '+ rcapplicant_master.surname AS applicantname " & _
    '                    ",rcapplicant_master.present_address1 +' ' + rcapplicant_master.present_address2 AS Address " & _
    '                    ",rcapplicant_master.present_mobileno AS mobile " & _
    '                    ",rcapplicant_master.email AS email " & _
    '                    ",rcvacancy_master.vacancytitle " & _
    '                    ",rcapplicant_master.applicantunkid " & _
    '                    ",rcvacancy_master.vacancyunkid " & _
    '                    ",rcapplicant_master.applicant_code " & _
    '              "FROM rcapplicant_master " & _
    '                "LEFT JOIN rcapplicant_batchschedule_tran ON rcapplicant_batchschedule_tran.applicantunkid = rcapplicant_master.applicantunkid " & _
    '                "LEFT JOIN rcinterviewanalysis_master ON rcinterviewanalysis_master.appbatchscheduletranunkid = rcapplicant_batchschedule_tran.appbatchscheduletranunkid " & _
    '                "LEFT JOIN rcbatchschedule_master ON rcbatchschedule_master.batchscheduleunkid=rcapplicant_batchschedule_tran.batchscheduleunkid " & _
    '                "LEFT JOIN rcvacancy_master ON rcvacancy_master.vacancyunkid=rcbatchschedule_master.vacancyunkid " & _
    '             "WHERE rcinterviewanalysis_master.iseligible = 1 AND rcinterviewanalysis_master.iscomplete = 1 "


    '        'Sandeep [ 09 Oct 2010 ] -- Start
    '        If blnIsImport = False Then
    '            strQ &= " AND ISNULL(rcapplicant_master.isimport,0) = 0 "
    '        End If
    '        'Sandeep [ 09 Oct 2010 ] -- End 


    '        'S.SANDEEP [ 25 DEC 2011 ] -- START
    '        'ENHANCEMENT : TRA CHANGES
    '        Select Case intApplicanttype
    '            Case enVacancyType.EXTERNAL_VACANCY  'EXTERNAL
    '                strQ &= " AND ISNULL(employeecode,'') = '' "
    '            Case enVacancyType.INTERNAL_VACANCY  'INTERNAL
    '                strQ &= " AND ISNULL(employeecode,'') <> '' "
    '        End Select
    '        'S.SANDEEP [ 25 DEC 2011 ] -- END




    '        'S.SANDEEP [ 10 June 2011 ] -- START
    '        'ISSUE : MULTIPLE INTERVIEW TYPE FOR ONE VACANCY
    '        strQ &= "GROUP BY " & _
    '                "	 rcapplicant_master.firstname +' ' + rcapplicant_master.othername+' '+ rcapplicant_master.surname " & _
    '                "	,rcapplicant_master.present_address1 +' ' + rcapplicant_master.present_address2 " & _
    '                "	,rcapplicant_master.present_mobileno " & _
    '                "	,rcapplicant_master.email " & _
    '                "	,rcvacancy_master.vacancytitle " & _
    '                "	,rcapplicant_master.applicantunkid " & _
    '                "	,rcvacancy_master.vacancyunkid " & _
    '                "	,rcapplicant_master.applicant_code "
    '        'S.SANDEEP [ 10 June 2011 ] -- END

    '        dsList = objDataOperation.ExecQuery(strQ, strTableName)

    '        If objDataOperation.ErrorMessage <> "" Then
    '            exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
    '            Throw exForce
    '        End If

    '    Catch ex As Exception
    '        Throw New Exception(ex.Message & "; Procedure Name: GetList_FinalApplicant; Module Name: " & mstrModuleName)
    '    Finally
    '        exForce = Nothing
    '        If dsList IsNot Nothing Then dsList.Dispose()
    '        objDataOperation = Nothing
    '    End Try
    '    Return dsList
    'End Function


    'Pinkal (07-Jan-2012) -- Start
    'Enhancement : TRA Changes

    '''' <summary>
    '''' Modify By: Anjan
    '''' </summary>
    '''' <param name="intApplicanttype">0 = All Eligible Applicant, 1 = External Eligible Applicant, 2 = Internal Eligible Applicant</param>

    'S.SANDEEP [ 14 May 2013 ] -- START
    'ENHANCEMENT : TRA ENHANCEMENT
    Public Function GetList_FinalApplicant(ByVal strTableName As String, _
                                           Optional ByVal blnIsImport As Boolean = False, _
                                           Optional ByVal intApplicanttype As Integer = 0, _
                                           Optional ByVal eAEType As enApplicant_Eligibility = enApplicant_Eligibility.AE_NONE, _
                                           Optional ByVal intVacancyId As Integer = -1, _
                                           Optional ByVal eSCType As enShortListing_Status = enShortListing_Status.SC_NONE, _
                                           Optional ByVal mblnIncludeIsComplete As Boolean = True) As DataSet

        'Pinkal (31-Mar-2023) --(A1X-717) NMB - As a user, based on the data displayed from the selection, I want to be able to shoot emails from the Feedback screen instead of going to the mail utility again. I should be able to select the template directly from this screen and input the subject.[Optional ByVal mblnIncludeIsComplete As Boolean = True]

        'Public Function GetList_FinalApplicant(ByVal strTableName As String, _
        '                           Optional ByVal blnIsImport As Boolean = False, _
        '                           Optional ByVal intApplicanttype As Integer = 0, _
        '                           Optional ByVal IsEligible As Boolean = True, _
        '                           Optional ByVal intVacancyId As Integer = -1) As DataSet
        'S.SANDEEP [ 14 May 2013 ] -- END

        'S.SANDEEP [ 25 DEC 2011 ] -- END


        'Sandeep [ 09 Oct 2010 ] -- End 
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        objDataOperation = New clsDataOperation

        Try
            'S.SANDEEP [ 28 FEB 2012 ] -- START
            'ENHANCEMENT : TRA CHANGES
            'strQ = "SELECT " & _
            '           "rcapplicant_master.firstname +' ' + rcapplicant_master.othername+' '+ rcapplicant_master.surname AS applicantname " & _
            '           ",rcapplicant_master.present_address1 +' ' + rcapplicant_master.present_address2 AS Address " & _
            '           ",rcapplicant_master.present_mobileno AS mobile " & _
            '           ",rcapplicant_master.email AS email " & _
            '           ",rcvacancy_master.vacancytitle " & _
            '           ",rcapplicant_master.applicantunkid " & _
            '           ",rcvacancy_master.vacancyunkid " & _
            '           ",rcapplicant_master.applicant_code " & _
            '     "FROM rcapplicant_master " & _
            '       "LEFT JOIN rcapplicant_batchschedule_tran ON rcapplicant_batchschedule_tran.applicantunkid = rcapplicant_master.applicantunkid " & _
            '       "LEFT JOIN rcinterviewanalysis_master ON rcinterviewanalysis_master.appbatchscheduletranunkid = rcapplicant_batchschedule_tran.appbatchscheduletranunkid " & _
            '       "LEFT JOIN rcbatchschedule_master ON rcbatchschedule_master.batchscheduleunkid=rcapplicant_batchschedule_tran.batchscheduleunkid " & _
            '       "LEFT JOIN rcvacancy_master ON rcvacancy_master.vacancyunkid=rcbatchschedule_master.vacancyunkid " & _
            '    "WHERE rcinterviewanalysis_master.iscomplete = 1 "


            'Pinkal (01-Dec-2012) -- Start
            'Enhancement : TRA Changes


            strQ = "SELECT DISTINCT " & _
                        "rcapplicant_master.firstname +' ' + rcapplicant_master.othername+' '+ rcapplicant_master.surname AS applicantname " & _
                        ",rcapplicant_master.firstname " & _
                        ",rcapplicant_master.othername " & _
                        ",rcapplicant_master.surname " & _
                        ",rcapplicant_master.present_address1 +' ' + rcapplicant_master.present_address2 AS Address " & _
                        ",rcapplicant_master.present_mobileno AS mobile " & _
                        ",rcapplicant_master.email AS email " & _
                       ",cfcommon_master.name AS vacancytitle " & _
                        ",rcapplicant_master.applicantunkid " & _
                        ",rcvacancy_master.vacancyunkid " & _
                        ",rcapplicant_master.applicant_code " & _
                       ",CONVERT(CHAR(8),openingdate,112) AS ODate " & _
                       ",CONVERT(CHAR(8),closingdate,112) AS CDate " & _
                       ",ISNULL(hrstation_master.name,'') AS Branch " & _
                       ",ISNULL(hrdepartment_group_master.name,'') AS DeptGroup " & _
                       ",ISNULL(hrdepartment_master.name,'') AS Dept " & _
                       ",ISNULL(hrsection_master.name,'') AS Section " & _
                       ",ISNULL(hrunit_master.name,'') AS Unit " & _
                       ",ISNULL(hrjobgroup_master.name,'') AS JobGrp " & _
                       ",ISNULL(hrjob_master.job_name,'') AS Job " & _
                       ",ISNULL(hrgradegroup_master.name,'') AS GradeGrp " & _
                       ",ISNULL(hrgrade_master.name,'') AS Grade " & _
                       ",ISNULL(ET.name,'') AS EmplType " & _
                       ",ISNULL(hrstation_master.stationunkid,0) AS BranchId " & _
                       ",ISNULL(hrdepartment_group_master.deptgroupunkid,0) AS DeptGroupId " & _
                       ",ISNULL(hrdepartment_master.departmentunkid,0) AS DeptId " & _
                       ",ISNULL(hrsection_master.sectionunkid,0) AS SectionId " & _
                       ",ISNULL(hrunit_master.unitunkid,0) AS UnitId " & _
                       ",ISNULL(hrjobgroup_master.jobgroupunkid,0) AS JobGrpId " & _
                       ",ISNULL(hrjob_master.jobunkid,0) AS JobId " & _
                       ",ISNULL(hrgradegroup_master.gradegroupunkid,0) AS GradeGrpId " & _
                       ",ISNULL(hrgrade_master.gradeunkid,0) AS GradeId " & _
                       ",ISNULL(hrsectiongroup_master.sectiongroupunkid, 0) AS SectionGrpId " & _
                       ",ISNULL(hrsectiongroup_master.name, 0) AS SectionGrp " & _
                       ",ISNULL(hrunitgroup_master.unitgroupunkid, 0) AS UnitGrpId " & _
                       ",ISNULL(hrunitgroup_master.name, 0) AS UnitGrp   " & _
                       ",ISNULL(hrteam_master.teamunkid, 0) AS TeamId " & _
                       ",ISNULL(hrteam_master.name, 0) AS Team  " & _
                       ",ISNULL(hrclassgroup_master.classgroupunkid, 0) AS ClassGrpId " & _
                       ",ISNULL(hrclassgroup_master.name, 0) AS ClassGrp  " & _
                       ",ISNULL(hrclasses_master.classesunkid, 0) AS ClassId " & _
                       ",ISNULL(hrclasses_master.name, 0) AS Class " & _
                       ",ISNULL(prcostcenter_master.costcenterunkid, 0) AS CostCenterId " & _
                       ",ISNULL(prcostcenter_master.costcentername, 0) AS CostCenter " & _
                       ",ISNULL(hrgradelevel_master.gradelevelunkid, 0) AS GradeLevelId " & _
                       ",ISNULL(hrgradelevel_master.name, 0) AS GradeLevel " & _
                       ",ISNULL(ET.masterunkid,0) AS EmplTypeId " & _
                        ",CASE WHEN statustypid = 1 THEN @Approved WHEN statustypid = 2 THEN @Pending WHEN statustypid = 3 THEN @Disapproved END AS I_Status " & _
                  "FROM rcapplicant_master " & _
                    "LEFT JOIN rcapplicant_batchschedule_tran ON rcapplicant_batchschedule_tran.applicantunkid = rcapplicant_master.applicantunkid " & _
                    "LEFT JOIN rcinterviewanalysis_master ON rcinterviewanalysis_master.appbatchscheduletranunkid = rcapplicant_batchschedule_tran.appbatchscheduletranunkid " & _
                    "LEFT JOIN rcbatchschedule_master ON rcbatchschedule_master.batchscheduleunkid=rcapplicant_batchschedule_tran.batchscheduleunkid " & _
                    "LEFT JOIN rcvacancy_master ON rcvacancy_master.vacancyunkid=rcbatchschedule_master.vacancyunkid " & _
                        "LEFT JOIN rcapp_vacancy_mapping ON rcapplicant_master.applicantunkid = rcapp_vacancy_mapping.applicantunkid  AND rcapp_vacancy_mapping.vacancyunkid = rcvacancy_master.vacancyunkid " & _
                   "LEFT JOIN hrstation_master ON hrstation_master.stationunkid = rcvacancy_master.stationunkid " & _
                   "LEFT JOIN hrdepartment_group_master ON rcvacancy_master.deptgroupunkid = hrdepartment_group_master.deptgroupunkid " & _
                   "LEFT JOIN hrdepartment_master ON rcvacancy_master.departmentunkid = hrdepartment_master.departmentunkid " & _
                   "LEFT JOIN hrsection_master ON rcvacancy_master.sectionunkid = hrsection_master.sectionunkid " & _
                   "LEFT JOIN hrunit_master ON rcvacancy_master.unitunkid = hrunit_master.unitunkid " & _
                   "LEFT JOIN hrjobgroup_master ON rcvacancy_master.jobgroupunkid = hrjobgroup_master.jobgroupunkid " & _
                   "LEFT JOIN hrjob_master ON rcvacancy_master.jobunkid = hrjob_master.jobunkid " & _
                   "LEFT JOIN hrgrade_master ON rcvacancy_master.gradeunkid = hrgrade_master.gradeunkid " & _
                   "LEFT JOIN hrgradegroup_master ON rcvacancy_master.gradegroupunkid = hrgradegroup_master.gradegroupunkid " & _
                   "LEFT JOIN hrsectiongroup_master	ON rcvacancy_master.sectiongroupunkid = hrsectiongroup_master.sectiongroupunkid " & _
                   "LEFT JOIN hrunitgroup_master ON rcvacancy_master.unitgroupunkid = hrunitgroup_master.unitgroupunkid " & _
                   "LEFT JOIN hrteam_master ON rcvacancy_master.teamunkid = hrteam_master.teamunkid " & _
                   "LEFT JOIN hrclassgroup_master ON rcvacancy_master.classgroupunkid = hrclassgroup_master.classgroupunkid " & _
                   "LEFT JOIN hrclasses_master ON rcvacancy_master.classunkid = hrclasses_master.classesunkid " & _
                   "LEFT JOIN prcostcenter_master ON rcvacancy_master.costcenterunkid = prcostcenter_master.costcenterunkid " & _
                   "LEFT JOIN hrgradelevel_master ON rcvacancy_master.gradelevelunkid = hrgradelevel_master.gradelevelunkid " & _
                   "JOIN cfcommon_master ON cfcommon_master.masterunkid = rcvacancy_master.vacancytitle AND  cfcommon_master.mastertype = '" & CInt(clsCommon_Master.enCommonMaster.VACANCY_MASTER) & "'  " & _
                   "LEFT JOIN cfcommon_master AS ET ON ET.masterunkid = rcvacancy_master.employeementtypeunkid AND cfcommon_master.mastertype = '" & CInt(clsCommon_Master.enCommonMaster.EMPLOYEMENT_TYPE) & "'  " & _
                   " WHERE ISNULL(rcapp_vacancy_mapping.isvoid, 0) = 0 "

            'Pinkal (31-Mar-2023) -- Start
            '(A1X-717) NMB - As a user, based on the data displayed from the selection, I want to be able to shoot emails from the Feedback screen instead of going to the mail utility again. I should be able to select the template directly from this screen and input the subject.
            If mblnIsComplete Then
                strQ &= " AND rcinterviewanalysis_master.iscomplete = 1 "
            End If
            'Pinkal (31-Mar-2023) -- End

            'Hemant (12 Dec 2020) -- [SectionGrpId, SectionGrp, UnitGrpId, UnitGrp, TeamId, Team, ClassGrpId, ClassGrp, ClassId, Class, CostCenterId, CostCenter, GradeLevelId, GradeLevel]
            'Sohail (09 Oct 2018) - [ISNULL(rcapp_vacancy_mapping.isvoid, 0) = 0]
            'S.SANDEEP [ 14 May 2013 {LEFT JOIN rcapp_vacancy_mapping ON rcapplicant_master.applicantunkid = rcapp_vacancy_mapping.applicantunkid  AND rcapp_vacancy_mapping.vacancyunkid = rcvacancy_master.vacancyunkid AND rcapp_vacancy_mapping.isimport = 0  "}] -- START -- END


            'Pinkal (01-Dec-2012) -- End

            If intVacancyId > 0 Then
                strQ &= " AND rcvacancy_master.vacancyunkid = '" & intVacancyId & "' "
            End If
            'S.SANDEEP [ 28 FEB 2012 ] -- END


            

            'S.SANDEEP [ 14 May 2013 ] -- START
            'ENHANCEMENT : TRA ENHANCEMENT
            'If IsEligible Then
            '    strQ &= " AND rcinterviewanalysis_master.iseligible = 1 "
            'Else
            '    strQ &= " AND rcinterviewanalysis_master.iseligible = 0 "
            'End If
            Select Case eAEType
                Case enApplicant_Eligibility.AE_ELIGIBLE
                strQ &= " AND rcinterviewanalysis_master.iseligible = 1 "
                Case enApplicant_Eligibility.AE_NOTELIGIBLE
                strQ &= " AND rcinterviewanalysis_master.iseligible = 0 "
                Case enApplicant_Eligibility.AE_ELIGIBLE_APPROVED
                    strQ &= " AND rcinterviewanalysis_master.iseligible = 1 "
                Case enApplicant_Eligibility.AE_ELIGIBLE_DISAPPROVED
                    strQ &= " AND rcinterviewanalysis_master.iseligible = 1 "
            End Select
            If eSCType <> enShortListing_Status.SC_NONE Then
                strQ &= " AND rcinterviewanalysis_master.statustypid = '" & eSCType & "' "
            End If
            'S.SANDEEP [ 14 May 2013 ] -- END



            'S.SANDEEP [ 14 May 2013 ] -- START
            'ENHANCEMENT : TRA ENHANCEMENT
            'If blnIsImport = False Then
            '    strQ &= " AND ISNULL(rcapplicant_master.isimport,0) = 0 "
            'End If
            If blnIsImport = False Then
                strQ &= "AND rcapp_vacancy_mapping.isimport = 0 "
            End If
            'S.SANDEEP [ 14 May 2013 ] -- END


            Select Case intApplicanttype
                Case enVacancyType.EXTERNAL_VACANCY  'EXTERNAL

                    'Sohail (12 Nov 2020) -- Start
                    'Internal Issue # : - External applicants from External Internal vacancies are not coming on Import Eligible Applicant screen.
                    'strQ &= " AND ISNULL(employeecode,'') = '' "
                    strQ &= "AND ISNULL(rcapplicant_master.employeeunkid, 0) <= 0 "
                    'Sohail (12 Nov 2020) -- End
                Case enVacancyType.INTERNAL_VACANCY  'INTERNAL
                    'Sohail (12 Nov 2020) -- Start
                    'Internal Issue # : - External applicants from External Internal vacancies are not coming on Import Eligible Applicant screen.
                    'strQ &= " AND ISNULL(employeecode,'') <> '' "
                    strQ &= "AND ISNULL(rcapplicant_master.employeeunkid, 0) > 0 "
                    'Sohail (12 Nov 2020) -- End
            End Select

            'S.SANDEEP [ 28 FEB 2012 ] -- START
            'ENHANCEMENT : TRA CHANGES
            'strQ &= "GROUP BY " & _
            '        "	 rcapplicant_master.firstname +' ' + rcapplicant_master.othername+' '+ rcapplicant_master.surname " & _
            '        "	,rcapplicant_master.present_address1 +' ' + rcapplicant_master.present_address2 " & _
            '        "	,rcapplicant_master.present_mobileno " & _
            '        "	,rcapplicant_master.email " & _
            '        "	,rcvacancy_master.vacancytitle " & _
            '        "	,rcapplicant_master.applicantunkid " & _
            '        "	,rcvacancy_master.vacancyunkid " & _
            '        "	,rcapplicant_master.applicant_code "           
            'S.SANDEEP [ 28 FEB 2012 ] -- END


            'S.SANDEEP [ 14 May 2013 ] -- START
            'ENHANCEMENT : TRA ENHANCEMENT
            objDataOperation.AddParameter("@Approved", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 7, "Approved"))
            objDataOperation.AddParameter("@Pending", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 8, "Pending"))
            objDataOperation.AddParameter("@Disapproved", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 9, "Disapproved"))
            'S.SANDEEP [ 14 May 2013 ] -- END

            dsList = objDataOperation.ExecQuery(strQ, strTableName)

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: GetList_FinalApplicant; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
        Return dsList
    End Function

    'Pinkal (07-Jan-2012) -- End

    Public Function GetAnalysisID(ByVal intappbatchscheduletranId As Integer) As Integer
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        objDataOperation = New clsDataOperation
        Try
            strQ = "SELECT analysisunkid FROM rcinterviewanalysis_master " & _
                   "WHERE appbatchscheduletranunkid = @appbatchscheduletranunkid "

            objDataOperation.AddParameter("@appbatchscheduletranunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intappbatchscheduletranId)

            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            If dsList.Tables(0).Rows.Count > 0 Then
                Return CInt(dsList.Tables(0).Rows(0).Item(0))
            Else
                Return -1
            End If

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "GetAnalysisID", mstrModuleName)
            Return Nothing
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try

    End Function

    'S.SANDEEP [ 17 AUG 2012 ] -- START
    'ENHANCEMENT : TRA CHANGES
    Public Sub Get_Eligible_Count(ByRef intPosition As Integer, ByRef intECount As Integer, ByVal intVacancyId As Integer)
        Dim StrQ As String = String.Empty
        Dim exForce As Exception : Dim dsList As DataSet = Nothing
        objDataOperation = New clsDataOperation
        Try

            'S.SANDEEP [ 31 DEC 2013 ] -- START
            'StrQ = "SELECT " & _
            '       "     rcvacancy_master.noofposition AS total_position " & _
            '       "    ,ISNULL(total_eligible,0) AS total_eligible " & _
            '       "FROM rcvacancy_master " & _
            '       "LEFT JOIN " & _
            '       "( " & _
            '       "    SELECT " & _
            '       "         rcbatchschedule_master.vacancyunkid AS vacancyunkid " & _
            '       "        ,COUNT(analysistranunkid) AS total_eligible " & _
            '       "    FROM rcinterviewanalysis_tran " & _
            '       "        JOIN rcinterviewanalysis_master ON rcinterviewanalysis_tran.analysisunkid = rcinterviewanalysis_master.analysisunkid " & _
            '       "        JOIN rcapplicant_batchschedule_tran ON rcinterviewanalysis_master.appbatchscheduletranunkid = rcapplicant_batchschedule_tran.appbatchscheduletranunkid " & _
            '       "        JOIN rcbatchschedule_master ON rcapplicant_batchschedule_tran.batchscheduleunkid = rcbatchschedule_master.batchscheduleunkid " & _
            '       "    WHERE rcinterviewanalysis_tran.interviewertranunkid<=0 " & _
            '       "        AND iseligible = 1 AND iscomplete = 1 AND rcbatchschedule_master.vacancyunkid = '" & intVacancyId & "' " & _
            '       "    GROUP BY rcbatchschedule_master.vacancyunkid " & _
            '       ")AS A ON rcvacancy_master.vacancyunkid = A.vacancyunkid " & _
            '       "WHERE rcvacancy_master.vacancyunkid = '" & intVacancyId & "' "
            StrQ = "SELECT " & _
                   "     rcvacancy_master.noofposition AS total_position " & _
                   "    ,ISNULL(total_eligible,0) AS total_eligible " & _
                   "FROM rcvacancy_master " & _
                   "LEFT JOIN " & _
                   "( " & _
                   "    SELECT " & _
                   "         rcbatchschedule_master.vacancyunkid AS vacancyunkid " & _
                   "        ,COUNT(DISTINCT rcinterviewanalysis_master.analysisunkid) AS total_eligible " & _
                   "    FROM rcinterviewanalysis_tran " & _
                   "        JOIN rcinterviewanalysis_master ON rcinterviewanalysis_tran.analysisunkid = rcinterviewanalysis_master.analysisunkid " & _
                   "        JOIN rcapplicant_batchschedule_tran ON rcinterviewanalysis_master.appbatchscheduletranunkid = rcapplicant_batchschedule_tran.appbatchscheduletranunkid " & _
                   "        JOIN rcbatchschedule_master ON rcapplicant_batchschedule_tran.batchscheduleunkid = rcbatchschedule_master.batchscheduleunkid " & _
                   "    WHERE iseligible = 1 AND iscomplete = 1 AND rcbatchschedule_master.vacancyunkid = '" & intVacancyId & "' AND approveuserunkid = 1 " & _
                   "    GROUP BY rcbatchschedule_master.vacancyunkid " & _
                   ")AS A ON rcvacancy_master.vacancyunkid = A.vacancyunkid " & _
                   "WHERE rcvacancy_master.vacancyunkid = '" & intVacancyId & "' "
            'S.SANDEEP [ 31 DEC 2013 ] -- END

            dsList = objDataOperation.ExecQuery(StrQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & " : " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            If dsList.Tables("List").Rows.Count > 0 Then
                intPosition = Convert.ToInt32(dsList.Tables(0).Rows(0)("total_position"))
                intECount = Convert.ToInt32(dsList.Tables(0).Rows(0)("total_eligible"))
            End If

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: Get_Eligible_Count; Module Name: " & mstrModuleName)
        End Try
    End Sub
    'S.SANDEEP [ 17 AUG 2012 ] -- END

    'S.SANDEEP [ 14 May 2013 ] -- START
    'ENHANCEMENT : TRA ENHANCEMENT
    Public Function Get_DataTable(ByVal iVacancyId As Integer, Optional ByVal sTableName As String = "List", Optional ByVal strAnalysisIds As String = "") As DataTable
        Dim StrQ As String = ""
        Dim exForce As Exception
        Dim dtFinal As DataTable = Nothing
        Dim dsList As New DataSet
        Try
            objDataOperation = New clsDataOperation

            StrQ = "SELECT " & _
                    "	 CAST(0 AS BIT) AS ischeck " & _
                    "	,applicant_code + ' - ' + rcapplicant_master.firstname + ' ' + rcapplicant_master.surname AS Applicant " & _
                    "	,cfcommon_master.name AS interviewtype " & _
                    "	,CONVERT(CHAR(8), analysis_date, 112) AS analysisdate " & _
                    "	,CONVERT(CHAR(8), analysis_date, 108) AS analysistime " & _
                    "	,CASE WHEN statustypid = 1 THEN @Approved WHEN statustypid = 2 THEN @Pending WHEN statustypid = 3 THEN @Disapproved END AS I_Status " & _
                    "	,ISNULL(CASE WHEN rcbatchschedule_interviewer_tran.interviewerunkid > 0 AND resultcodeunkid > 0  THEN hremployee_master.firstname + ' ' + hremployee_master.surname " & _
                    "			WHEN rcbatchschedule_interviewer_tran.interviewerunkid <= 0 AND resultcodeunkid > 0 THEN rcbatchschedule_interviewer_tran.otherinterviewer_name END " & _
                    "	,ISNULL(hrmsConfiguration..cfuser_master.username,'')) AS reviewer " & _
                    "	,ISNULL(hrresult_master.resultname,'') AS score " & _
                    "   ,CASE WHEN iscomplete = 0 THEN '' ELSE CASE WHEN iseligible = 1 THEN @Eligible WHEN iseligible = 0 THEN @NotEligible END END AS E_Status " & _
                    "   ,CASE WHEN iscomplete = 0 THEN 2  ELSE CASE WHEN iseligible = 1 THEN 1 WHEN iseligible = 0 THEN 0 END END AS E_StatusId " & _
                    "	,rcinterviewanalysis_tran.remark " & _
                    "	,rcinterviewanalysis_master.appbatchscheduletranunkid " & _
                    "	,rcinterviewanalysis_tran.analysisunkid " & _
                    "	,rcinterviewanalysis_master.statustypid " & _
                    "	,rcinterviewanalysis_master.issent " & _
                    "	,rcinterviewanalysis_master.vacancyunkid " & _
                    "	,rcinterviewanalysis_master.applicantunkid " & _
                    "	,A.A_Id " & _
                    "   ,rcbatchschedule_interviewer_tran.interviewtypeunkid " & _
                    "   ,rcapplicant_batchschedule_tran.batchscheduleunkid " & _
                    "   ,resultcodeunkid " & _
                    "   ,rcinterviewanalysis_master.iscomplete " & _
                    "   ,rcinterviewanalysis_master.approveuserunkid " & _
                    "   ,rcinterviewanalysis_master.appr_remark " & _
                    "   ,rcinterviewanalysis_master.appr_date " & _
                    "   ,CONVERT(CHAR(8),rcinterviewanalysis_master.appr_date,112) AS adate " & _
                    "	,CONVERT(CHAR(8),rcinterviewanalysis_master.appr_date,108) AS atime " & _
                    "   ,CAST(CAST(rcinterviewanalysis_master.vacancyunkid AS NVARCHAR(MAX))+''+CAST(rcinterviewanalysis_master.applicantunkid AS NVARCHAR(MAX)) AS INT) AS Grp_Id " & _
                    "   ,rcinterviewanalysis_master.iseligible " & _
                    "   ,ISNULL(app_usr.username,'') AS approver " & _
                    "FROM rcinterviewanalysis_tran " & _
                    "	JOIN rcinterviewanalysis_master ON rcinterviewanalysis_tran.analysisunkid = rcinterviewanalysis_master.analysisunkid " & _
                    "   LEFT JOIN hrmsConfiguration..cfuser_master AS app_usr ON rcinterviewanalysis_master.approveuserunkid = app_usr.userunkid " & _
                    "	LEFT JOIN hrmsConfiguration..cfuser_master ON rcinterviewanalysis_master.userunkid = hrmsConfiguration..cfuser_master.userunkid " & _
                    "	JOIN rcapplicant_batchschedule_tran ON rcapplicant_batchschedule_tran.appbatchscheduletranunkid = rcinterviewanalysis_master.appbatchscheduletranunkid " & _
                    "	JOIN rcbatchschedule_interviewer_tran ON rcbatchschedule_interviewer_tran.batchscheduleinterviewertranunkid = rcinterviewanalysis_tran.interviewertranunkid " & _
                    "	LEFT JOIN hremployee_master ON hremployee_master.employeeunkid = rcbatchschedule_interviewer_tran.interviewerunkid " & _
                    "	JOIN rcapplicant_master ON rcapplicant_master.applicantunkid = rcapplicant_batchschedule_tran.applicantunkid " & _
                    "   LEFT JOIN rcapp_vacancy_mapping ON rcapplicant_master.applicantunkid = rcapp_vacancy_mapping.applicantunkid  AND rcapp_vacancy_mapping.isactive = 1 AND rcapp_vacancy_mapping.vacancyunkid  = rcinterviewanalysis_master.vacancyunkid " & _
                    "	LEFT JOIN hrresult_master ON hrresult_master.resultunkid = rcinterviewanalysis_tran.resultcodeunkid " & _
                    "	JOIN rcbatchschedule_master ON rcbatchschedule_master.batchscheduleunkid = rcapplicant_batchschedule_tran.batchscheduleunkid " & _
                    "	JOIN cfcommon_master ON cfcommon_master.masterunkid = rcbatchschedule_master.interviewtypeunkid AND cfcommon_master.mastertype = " & enCommonMaster.INTERVIEW_TYPE & " " & _
                    "	LEFT JOIN " & _
                    "	( " & _
                    "		SELECT " & _
                    "			applicantunkid,STUFF((SELECT ',' + '''' + CAST(analysisunkid AS NVARCHAR(MAX)) + '''' " & _
                    "							  FROM rcinterviewanalysis_master t2 WHERE t1.applicantunkid = t2.applicantunkid " & _
                    "							  FOR XML PATH('')),1,1,'') AS A_Id " & _
                    "		FROM rcinterviewanalysis_master t1 " & _
                    "		GROUP BY applicantunkid " & _
                    "	) AS A ON A.applicantunkid = rcinterviewanalysis_master.applicantunkid " & _
                    "WHERE ISNULL(rcapplicant_batchschedule_tran.iscancel, 0) = 0 " & _
                    "	AND ISNULL(rcapplicant_batchschedule_tran.isvoid, 0) = 0 " & _
                    "	AND ISNULL(rcinterviewanalysis_master.isvoid, 0) = 0 " & _
                    "	AND ISNULL(rcinterviewanalysis_tran.isvoid, 0) = 0 " & _
                    "AND rcinterviewanalysis_master.vacancyunkid = '" & iVacancyId & "' " & _
                    "AND ISNULL(rcapp_vacancy_mapping.isvoid, 0) = 0 "
            'Hemant (30 Oct 2019) -- [rcinterviewer_tran --> rcbatchschedule_interviewer_tran]
            'Sohail (09 Oct 2018) - [ISNULL(isvoid, 0) = 0]
            'S.SANDEEP [12-DEC-2017] -- START {applicant_code + ' - '}> -- END


            'S.SANDEEP [ 09 JULY 2013 ] -- START
            'ENHANCEMENT : OTHER CHANGES
            '  LEFT JOIN hrmsConfiguration..cfuser_master AS app_usr ON rcinterviewanalysis_master.approveuserunkid = app_usr.userunkid --> ADDED
            '  ISNULL(app_usr.username,'') AS approver --> ADDED
            '  CONVERT(CHAR(8),rcinterviewanalysis_master.appr_date,112) --> ADDED
            '  CONVERT(CHAR(8),rcinterviewanalysis_master.appr_date,108) --> ADDED
            'S.SANDEEP [ 09 JULY 2013 ] -- END

            If strAnalysisIds.Trim.Length > 0 Then
                StrQ &= "AND rcinterviewanalysis_tran.analysisunkid IN(" & strAnalysisIds & ") "
            End If

            StrQ &= " AND rcapp_vacancy_mapping.isimport = 0 "

            StrQ &= "ORDER BY rcapplicant_master.firstname + ' ' + rcapplicant_master.surname "

            objDataOperation.AddParameter("@Eligible", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 10, "Yes"))
            objDataOperation.AddParameter("@NotEligible", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 11, "No"))
            objDataOperation.AddParameter("@Approved", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 7, "Approved"))
            objDataOperation.AddParameter("@Pending", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 8, "Pending"))
            objDataOperation.AddParameter("@Disapproved", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 9, "Disapproved"))

            dsList = objDataOperation.ExecQuery(StrQ, IIf(sTableName.Trim = "", "List", sTableName))

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & " : " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            dtFinal = New DataTable(sTableName)
            dtFinal.Columns.Add("ischeck", System.Type.GetType("System.Boolean")).DefaultValue = False
            dtFinal.Columns.Add("interview_type", System.Type.GetType("System.String")).DefaultValue = ""
            dtFinal.Columns.Add("analysis_date", System.Type.GetType("System.String")).DefaultValue = ""
            dtFinal.Columns.Add("reviewer", System.Type.GetType("System.String")).DefaultValue = ""
            dtFinal.Columns.Add("score", System.Type.GetType("System.String")).DefaultValue = ""
            dtFinal.Columns.Add("remark", System.Type.GetType("System.String")).DefaultValue = ""
            dtFinal.Columns.Add("a_ids", System.Type.GetType("System.String")).DefaultValue = ""
            dtFinal.Columns.Add("appbatchscheduletranunkid", System.Type.GetType("System.Int32")).DefaultValue = 0
            dtFinal.Columns.Add("analysisunkid", System.Type.GetType("System.Int32")).DefaultValue = 0
            dtFinal.Columns.Add("vacancyunkid", System.Type.GetType("System.Int32")).DefaultValue = 0
            dtFinal.Columns.Add("applicantunkid", System.Type.GetType("System.Int32")).DefaultValue = 0
            dtFinal.Columns.Add("statustypid", System.Type.GetType("System.Int32")).DefaultValue = 0
            dtFinal.Columns.Add("issent", System.Type.GetType("System.Boolean")).DefaultValue = False
            dtFinal.Columns.Add("is_grp", System.Type.GetType("System.Boolean")).DefaultValue = False
            dtFinal.Columns.Add("grp_id", System.Type.GetType("System.Int32")).DefaultValue = 0
            dtFinal.Columns.Add("sort_id", System.Type.GetType("System.Int32")).DefaultValue = 0
            dtFinal.Columns.Add("E_StatusId", System.Type.GetType("System.Int32")).DefaultValue = 0
            dtFinal.Columns.Add("interviewtypeunkid", System.Type.GetType("System.Int32")).DefaultValue = 0
            dtFinal.Columns.Add("batchscheduleunkid", System.Type.GetType("System.Int32")).DefaultValue = 0
            dtFinal.Columns.Add("resultcodeunkid", System.Type.GetType("System.Int32")).DefaultValue = 0
            dtFinal.Columns.Add("analysisdate", System.Type.GetType("System.String")).DefaultValue = ""
            dtFinal.Columns.Add("iscomplete", System.Type.GetType("System.Boolean")).DefaultValue = False
            dtFinal.Columns.Add("approveuserunkid", System.Type.GetType("System.Int32")).DefaultValue = 0
            dtFinal.Columns.Add("appr_remark", System.Type.GetType("System.String")).DefaultValue = ""
            dtFinal.Columns.Add("appr_date", System.Type.GetType("System.DateTime")).DefaultValue = Nothing
            dtFinal.Columns.Add("iseligible", System.Type.GetType("System.Boolean")).DefaultValue = False

            Dim StrApplicantName As String = ""
            Dim dtRow As DataRow = Nothing

            'S.SANDEEP [ 09 JULY 2013 ] -- START
            'ENHANCEMENT : OTHER CHANGES
            Dim iCnt As Integer = 0 : Dim dTemp() As DataRow = Nothing
            'S.SANDEEP [ 09 JULY 2013 ] -- END

            For Each dRow As DataRow In dsList.Tables(0).Rows
                dtRow = dtFinal.NewRow

                'S.SANDEEP [12-DEC-2017] -- START
                'ISSUE/ENHANCEMENT : GROUPING WAS IN NAME, DUE TO THAT, APPLICANT WAS IN DIFFERENT REC.
                If StrApplicantName <> dRow.Item("applicantunkid").ToString Then
                    'If StrApplicantName <> dRow.Item("Applicant").ToString Then
                    'S.SANDEEP [12-DEC-2017] -- END

                    dtRow.Item("ischeck") = dRow.Item("ischeck")
                    If CBool(dRow.Item("iscomplete")) = False Then
                        dtRow.Item("interview_type") = dRow.Item("Applicant")
                    ElseIf CBool(dRow.Item("iscomplete")) = True And dRow.Item("E_Status").ToString = "" Then
                        dtRow.Item("interview_type") = dRow.Item("Applicant") & " [Approval Status : " & dRow.Item("I_Status") & "]"
                    ElseIf dRow.Item("E_Status").ToString <> "" Then
                        dtRow.Item("interview_type") = dRow.Item("Applicant") & " [Eligible : " & dRow.Item("E_Status") & "] - [Approval Status : " & dRow.Item("I_Status") & "]"
                    End If
                    dtRow.Item("vacancyunkid") = dRow.Item("vacancyunkid")
                    dtRow.Item("statustypid") = dRow.Item("statustypid")
                    dtRow.Item("issent") = dRow.Item("issent")
                    dtRow.Item("a_ids") = dRow.Item("A_Id")
                    dtRow.Item("is_grp") = True
                    dtRow.Item("grp_id") = dRow.Item("Grp_Id")
                    dtRow.Item("sort_id") = -1
                    dtRow.Item("applicantunkid") = dRow.Item("applicantunkid")
                    dtRow.Item("analysisdate") = dRow.Item("analysisdate")
                    dtRow.Item("iscomplete") = dRow.Item("iscomplete")
                    dtRow.Item("E_StatusId") = dRow.Item("E_StatusId")
                    dtRow.Item("approveuserunkid") = dRow.Item("approveuserunkid")
                    dtRow.Item("appr_remark") = dRow.Item("appr_remark")
                    dtRow.Item("appr_date") = dRow.Item("appr_date")
                    dtRow.Item("iseligible") = dRow.Item("iseligible")
                    'Hemant (07 Oct 2019) -- Start
                    'ISSUE/ENHANCEMENT(NMB) :  RECRUITMENTS UAT CHANGES(Point No 12 : On batch scheduling, system should have a functionality to indicate the interview panel members.)
                    dtRow.Item("batchscheduleunkid") = dRow.Item("batchscheduleunkid")
                    'Hemant (07 Oct 2019) -- End

                    dtFinal.Rows.Add(dtRow)
                    'S.SANDEEP [12-DEC-2017] -- START
                    'ISSUE/ENHANCEMENT : GROUPING WAS IN NAME, DUE TO THAT, APPLICANT WAS IN DIFFERENT REC.
                    'StrApplicantName = dRow.Item("Applicant").ToString
                    StrApplicantName = dRow.Item("applicantunkid").ToString
                    'S.SANDEEP [12-DEC-2017] -- END

                    'S.SANDEEP [ 09 JULY 2013 ] -- START
                    'ENHANCEMENT : OTHER CHANGES
                    'S.SANDEEP [ 16 NOV 2013 ] -- START
                    'iCnt = 0 : dTemp = dsList.Tables(0).Select("Applicant = '" & StrApplicantName & "'")

                    'S.SANDEEP [12-DEC-2017] -- START
                    'ISSUE/ENHANCEMENT : GROUPING WAS IN NAME, DUE TO THAT, APPLICANT WAS IN DIFFERENT REC.
                    'iCnt = 0 : dTemp = dsList.Tables(0).Select("Applicant = '" & StrApplicantName.Replace("'", "''") & "'")
                    iCnt = 0 : dTemp = dsList.Tables(0).Select("applicantunkid = '" & StrApplicantName.Replace("'", "''") & "'")
                    'S.SANDEEP [12-DEC-2017] -- END

                    'S.SANDEEP [ 16 NOV 2013 ] -- END
                    'S.SANDEEP [ 09 JULY 2013 ] -- END
                    dtRow = dtFinal.NewRow
                End If

                If dRow.Item("analysisdate").ToString.Trim.Length > 0 Then
                    dtRow.Item("analysis_date") = eZeeDate.convertDate(dRow.Item("analysisdate").ToString).ToShortDateString & " " & dRow.Item("analysistime")
                End If
                dtRow.Item("a_ids") = dRow.Item("A_Id")
                dtRow.Item("reviewer") = dRow.Item("reviewer")
                dtRow.Item("interview_type") = Space(5) & dRow.Item("interviewtype")
                dtRow.Item("score") = dRow.Item("score")
                dtRow.Item("remark") = dRow.Item("remark")
                dtRow.Item("appbatchscheduletranunkid") = dRow.Item("appbatchscheduletranunkid")
                dtRow.Item("analysisunkid") = dRow.Item("analysisunkid")
                dtRow.Item("applicantunkid") = dRow.Item("applicantunkid")
                dtRow.Item("vacancyunkid") = dRow.Item("vacancyunkid")
                dtRow.Item("grp_id") = dRow.Item("Grp_Id")
                dtRow.Item("analysisdate") = dRow.Item("analysisdate")
                dtRow.Item("E_StatusId") = dRow.Item("E_StatusId")
                dtRow.Item("interviewtypeunkid") = dRow.Item("interviewtypeunkid")
                dtRow.Item("batchscheduleunkid") = dRow.Item("batchscheduleunkid")
                dtRow.Item("resultcodeunkid") = dRow.Item("resultcodeunkid")
                dtRow.Item("iscomplete") = dRow.Item("iscomplete")
                dtRow.Item("statustypid") = dRow.Item("statustypid")
                dtRow.Item("approveuserunkid") = dRow.Item("approveuserunkid")
                dtRow.Item("appr_remark") = dRow.Item("appr_remark")
                dtRow.Item("appr_date") = dRow.Item("appr_date")
                dtRow.Item("iseligible") = dRow.Item("iseligible")

                'S.SANDEEP [ 09 JULY 2013 ] -- START
                'ENHANCEMENT : OTHER CHANGES
                iCnt += 1
                If iCnt = dTemp.Length Then
                    dtFinal.Rows.Add(dtRow)
                    If dRow.Item("E_Status").ToString = "" Then Continue For
                    dtRow = dtFinal.NewRow
                    If dRow.Item("adate").ToString.Trim.Length > 0 Then
                        dtRow.Item("analysis_date") = eZeeDate.convertDate(dRow.Item("adate").ToString).ToShortDateString & " " & dRow.Item("atime")
                    End If
                    dtRow.Item("a_ids") = dRow.Item("A_Id")
                    dtRow.Item("reviewer") = dRow.Item("I_Status") & " : " & dRow.Item("approver")
                    dtRow.Item("interview_type") = Space(5) & dRow.Item("interviewtype")
                    dtRow.Item("score") = dRow.Item("score")
                    dtRow.Item("remark") = dRow.Item("appr_remark")
                    dtRow.Item("appbatchscheduletranunkid") = dRow.Item("appbatchscheduletranunkid")
                    dtRow.Item("analysisunkid") = dRow.Item("analysisunkid")
                    dtRow.Item("applicantunkid") = dRow.Item("applicantunkid")
                    dtRow.Item("vacancyunkid") = dRow.Item("vacancyunkid")
                    dtRow.Item("grp_id") = dRow.Item("Grp_Id")
                    dtRow.Item("analysisdate") = dRow.Item("analysisdate")
                    dtRow.Item("E_StatusId") = dRow.Item("E_StatusId")
                    dtRow.Item("interviewtypeunkid") = dRow.Item("interviewtypeunkid")
                    dtRow.Item("batchscheduleunkid") = dRow.Item("batchscheduleunkid")
                    dtRow.Item("resultcodeunkid") = dRow.Item("resultcodeunkid")
                    dtRow.Item("iscomplete") = dRow.Item("iscomplete")
                    dtRow.Item("statustypid") = dRow.Item("statustypid")
                    dtRow.Item("approveuserunkid") = dRow.Item("approveuserunkid")
                    dtRow.Item("appr_remark") = dRow.Item("appr_remark")
                    dtRow.Item("appr_date") = dRow.Item("appr_date")
                    dtRow.Item("iseligible") = dRow.Item("iseligible")
                End If
                'S.SANDEEP [ 09 JULY 2013 ] -- END

                dtFinal.Rows.Add(dtRow)
            Next

            Return dtFinal

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "Get_DataTable", mstrModuleName)
            Return Nothing
        End Try
    End Function

    Public Function Is_Analysis_Done(ByVal iVacanyId As Integer, ByVal iApplicantId As Integer, ByRef sMessage As String) As Boolean
        Dim StrQ As String = String.Empty
        Dim iTot_Batch, iTot_Interviw As Integer
        Dim exForce As Exception
        Dim blnFlag As Boolean = False
        Try
            objDataOperation = New clsDataOperation
            iTot_Batch = 0 : iTot_Interviw = 0

            StrQ = "SELECT " & _
                   "	applicantunkid " & _
                   "FROM rcapplicant_batchschedule_tran " & _
                   "	JOIN rcbatchschedule_master ON rcapplicant_batchschedule_tran.batchscheduleunkid = rcbatchschedule_master.batchscheduleunkid " & _
                   "WHERE applicantunkid = '" & iApplicantId & "' AND vacancyunkid = '" & iVacanyId & "' " & _
                   "	AND rcapplicant_batchschedule_tran.isvoid = 0 AND rcbatchschedule_master.isvoid = 0 " & _
                   "	AND rcapplicant_batchschedule_tran.iscancel = 0 "

            iTot_Batch = objDataOperation.RecordCount(StrQ)

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & " : " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            StrQ = "SELECT " & _
                   "	applicantunkid " & _
                   "FROM rcinterviewanalysis_master " & _
                   "WHERE applicantunkid = '" & iApplicantId & "' AND vacancyunkid = '" & iVacanyId & "' AND isvoid = 0 "

            iTot_Interviw = objDataOperation.RecordCount(StrQ)

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & " : " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            If iTot_Batch = iTot_Interviw Then
                blnFlag = True
            Else
                blnFlag = False
                Dim objApplicant As New clsApplicant_master
                objApplicant._Applicantunkid = iApplicantId
                sMessage = Language.getMessage(mstrModuleName, 12, "Sorry, Selected applicant : ") & objApplicant._Applicant_Code & " - " & objApplicant._Firstname & " " & objApplicant._Surname & _
                           Language.getMessage(mstrModuleName, 13, "'s interview process is still not finished. Please complete all interview process for this applicant.")
                objApplicant = Nothing
            End If
            Return blnFlag
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "Is_Analysis_Done", mstrModuleName)
            Return False
        Finally
        End Try
    End Function
    'S.SANDEEP [ 14 May 2013 ] -- END

    'Pinkal (31-Mar-2023) -- Start
    '(A1X-717) NMB - As a user, based on the data displayed from the selection, I want to be able to shoot emails from the Feedback screen instead of going to the mail utility again. I should be able to select the template directly from this screen and input the subject.

    Public Function GetNonInterviewedApplicantList(ByVal xApplicanttype As Integer, ByVal xVacancyId As Integer) As DataSet
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception
        Try
            Dim objDataOperation As New clsDataOperation

            strQ = " SELECT DISTINCT " & _
                      " rcapplicant_master.firstname +' ' + rcapplicant_master.othername+' '+ rcapplicant_master.surname AS applicantname " & _
                      ",rcapplicant_master.firstname " & _
                      ",rcapplicant_master.othername " & _
                      ",rcapplicant_master.surname " & _
                      ",rcapplicant_master.present_address1 +' ' + rcapplicant_master.present_address2 AS Address " & _
                      ",rcapplicant_master.present_mobileno AS mobile " & _
                      ",rcapplicant_master.email AS email " & _
                      ",cfcommon_master.name AS vacancytitle " & _
                      ",rcapplicant_master.applicantunkid " & _
                      ",rcvacancy_master.vacancyunkid " & _
                      ",rcapplicant_master.applicant_code " & _
                      ",CONVERT(CHAR(8),openingdate,112) AS ODate " & _
                      ",CONVERT(CHAR(8),closingdate,112) AS CDate " & _
                      ",ISNULL(hrstation_master.name,'') AS Branch " & _
                      ",ISNULL(hrdepartment_group_master.name,'') AS DeptGroup " & _
                      ",ISNULL(hrdepartment_master.name,'') AS Dept " & _
                      ",ISNULL(hrsection_master.name,'') AS Section " & _
                      ",ISNULL(hrunit_master.name,'') AS Unit " & _
                      ",ISNULL(hrjobgroup_master.name,'') AS JobGrp " & _
                      ",ISNULL(hrjob_master.job_name,'') AS Job " & _
                      ",ISNULL(hrgradegroup_master.name,'') AS GradeGrp " & _
                      ",ISNULL(hrgrade_master.name,'') AS Grade " & _
                      ",ISNULL(ET.name,'') AS EmplType " & _
                      ",ISNULL(hrstation_master.stationunkid,0) AS BranchId " & _
                      ",ISNULL(hrdepartment_group_master.deptgroupunkid,0) AS DeptGroupId " & _
                      ",ISNULL(hrdepartment_master.departmentunkid,0) AS DeptId " & _
                      ",ISNULL(hrsection_master.sectionunkid,0) AS SectionId " & _
                      ",ISNULL(hrunit_master.unitunkid,0) AS UnitId " & _
                      ",ISNULL(hrjobgroup_master.jobgroupunkid,0) AS JobGrpId " & _
                      ",ISNULL(hrjob_master.jobunkid,0) AS JobId " & _
                      ",ISNULL(hrgradegroup_master.gradegroupunkid,0) AS GradeGrpId " & _
                      ",ISNULL(hrgrade_master.gradeunkid,0) AS GradeId " & _
                      ",ISNULL(hrsectiongroup_master.sectiongroupunkid, 0) AS SectionGrpId " & _
                      ",ISNULL(hrsectiongroup_master.name, 0) AS SectionGrp " & _
                      ",ISNULL(hrunitgroup_master.unitgroupunkid, 0) AS UnitGrpId " & _
                      ",ISNULL(hrunitgroup_master.name, 0) AS UnitGrp   " & _
                      ",ISNULL(hrteam_master.teamunkid, 0) AS TeamId " & _
                      ",ISNULL(hrteam_master.name, 0) AS Team  " & _
                      ",ISNULL(hrclassgroup_master.classgroupunkid, 0) AS ClassGrpId " & _
                      ",ISNULL(hrclassgroup_master.name, 0) AS ClassGrp  " & _
                      ",ISNULL(hrclasses_master.classesunkid, 0) AS ClassId " & _
                      ",ISNULL(hrclasses_master.name, 0) AS Class " & _
                      ",ISNULL(prcostcenter_master.costcenterunkid, 0) AS CostCenterId " & _
                      ",ISNULL(prcostcenter_master.costcentername, 0) AS CostCenter " & _
                      ",ISNULL(hrgradelevel_master.gradelevelunkid, 0) AS GradeLevelId " & _
                      ",ISNULL(hrgradelevel_master.name, 0) AS GradeLevel " & _
                      ",ISNULL(ET.masterunkid,0) AS EmplTypeId " & _
                      " FROM rcapp_vacancy_mapping  " & _
                      " LEFT JOIN rcapplicant_master	ON rcapp_vacancy_mapping.applicantunkid = rcapplicant_master.applicantunkid " & _
                      " LEFT JOIN rcvacancy_master ON rcvacancy_master.vacancyunkid=rcapp_vacancy_mapping.vacancyunkid " & _
                      " LEFT JOIN hrstation_master ON hrstation_master.stationunkid = rcvacancy_master.stationunkid " & _
                      " LEFT JOIN hrdepartment_group_master ON rcvacancy_master.deptgroupunkid = hrdepartment_group_master.deptgroupunkid " & _
                      " LEFT JOIN hrdepartment_master ON rcvacancy_master.departmentunkid = hrdepartment_master.departmentunkid " & _
                      " LEFT JOIN hrsection_master ON rcvacancy_master.sectionunkid = hrsection_master.sectionunkid " & _
                      " LEFT JOIN hrunit_master ON rcvacancy_master.unitunkid = hrunit_master.unitunkid " & _
                      " LEFT JOIN hrjobgroup_master ON rcvacancy_master.jobgroupunkid = hrjobgroup_master.jobgroupunkid " & _
                      " LEFT JOIN hrjob_master ON rcvacancy_master.jobunkid = hrjob_master.jobunkid " & _
                      " LEFT JOIN hrgrade_master ON rcvacancy_master.gradeunkid = hrgrade_master.gradeunkid " & _
                      " LEFT JOIN hrgradegroup_master ON rcvacancy_master.gradegroupunkid = hrgradegroup_master.gradegroupunkid " & _
                      " LEFT JOIN hrsectiongroup_master	ON rcvacancy_master.sectiongroupunkid = hrsectiongroup_master.sectiongroupunkid " & _
                      " LEFT JOIN hrunitgroup_master ON rcvacancy_master.unitgroupunkid = hrunitgroup_master.unitgroupunkid " & _
                      " LEFT JOIN hrteam_master ON rcvacancy_master.teamunkid = hrteam_master.teamunkid " & _
                      " LEFT JOIN hrclassgroup_master ON rcvacancy_master.classgroupunkid = hrclassgroup_master.classgroupunkid " & _
                      " LEFT JOIN hrclasses_master ON rcvacancy_master.classunkid = hrclasses_master.classesunkid " & _
                      " LEFT JOIN prcostcenter_master ON rcvacancy_master.costcenterunkid = prcostcenter_master.costcenterunkid " & _
                      " LEFT JOIN hrgradelevel_master ON rcvacancy_master.gradelevelunkid = hrgradelevel_master.gradelevelunkid " & _
                      " JOIN cfcommon_master ON cfcommon_master.masterunkid = rcvacancy_master.vacancytitle AND  cfcommon_master.mastertype = '" & CInt(clsCommon_Master.enCommonMaster.VACANCY_MASTER) & "'  " & _
                      " LEFT JOIN cfcommon_master AS ET ON ET.masterunkid = rcvacancy_master.employeementtypeunkid AND cfcommon_master.mastertype = '" & CInt(clsCommon_Master.enCommonMaster.EMPLOYEMENT_TYPE) & "'  " & _
                      " WHERE ISNULL(rcapp_vacancy_mapping.isvoid, 0) = 0 "

            If xVacancyId > 0 Then
                strQ &= " AND rcvacancy_master.vacancyunkid = '" & xVacancyId & "' "
            End If

            Select Case xApplicanttype
                Case enVacancyType.EXTERNAL_VACANCY  'EXTERNAL
                    strQ &= "AND ISNULL(rcapplicant_master.employeeunkid, 0) <= 0 "
                Case enVacancyType.INTERNAL_VACANCY  'INTERNAL
                    strQ &= "AND ISNULL(rcapplicant_master.employeeunkid, 0) > 0 "
            End Select

            strQ &= " AND rcapp_vacancy_mapping.applicantunkid NOT IN " & _
                        "(" & _
                        "       SELECT " & _
                        "           rcapplicant_master.applicantunkid " & _
                        "       FROM rcapplicant_master " & _
                        "       LEFT JOIN rcapplicant_batchschedule_tran ON rcapplicant_batchschedule_tran.applicantunkid = rcapplicant_master.applicantunkid " & _
                        "       LEFT JOIN rcinterviewanalysis_master ON rcinterviewanalysis_master.appbatchscheduletranunkid = rcapplicant_batchschedule_tran.appbatchscheduletranunkid " & _
                        "       LEFT JOIN rcbatchschedule_master ON rcbatchschedule_master.batchscheduleunkid=rcapplicant_batchschedule_tran.batchscheduleunkid " & _
                        "       LEFT JOIN rcvacancy_master ON rcvacancy_master.vacancyunkid=rcbatchschedule_master.vacancyunkid " & _
                        "       LEFT JOIN rcapp_vacancy_mapping ON rcapplicant_master.applicantunkid = rcapp_vacancy_mapping.applicantunkid  AND rcapp_vacancy_mapping.vacancyunkid = rcvacancy_master.vacancyunkid " & _
                        "       JOIN cfcommon_master ON cfcommon_master.masterunkid = rcvacancy_master.vacancytitle AND  cfcommon_master.mastertype = '" & CInt(clsCommon_Master.enCommonMaster.VACANCY_MASTER) & "'  " & _
                        "       LEFT JOIN cfcommon_master AS ET ON ET.masterunkid = rcvacancy_master.employeementtypeunkid AND cfcommon_master.mastertype = '" & CInt(clsCommon_Master.enCommonMaster.EMPLOYEMENT_TYPE) & "'  " & _
                        "       WHERE ISNULL(rcapp_vacancy_mapping.isvoid, 0) = 0 "
            If xVacancyId > 0 Then
                strQ &= " AND rcvacancy_master.vacancyunkid = '" & xVacancyId & "' "
            End If

            Select Case xApplicanttype
                Case enVacancyType.EXTERNAL_VACANCY  'EXTERNAL
                    strQ &= "AND ISNULL(rcapplicant_master.employeeunkid, 0) <= 0 "
                Case enVacancyType.INTERNAL_VACANCY  'INTERNAL
                    strQ &= "AND ISNULL(rcapplicant_master.employeeunkid, 0) > 0 "
            End Select

            strQ &= ")"


            objDataOperation.ClearParameters()
            objDataOperation.AddParameter("@Approved", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 7, "Approved"))
            objDataOperation.AddParameter("@Pending", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 8, "Pending"))
            objDataOperation.AddParameter("@Disapproved", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 9, "Disapproved"))

            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: GetNonInterviewedApplicantList; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
        Return dsList
    End Function

    'Pinkal (31-Mar-2023) -- End
    

	'<Language> This Auto Generated Text Please Do Not Modify it.
#Region " Language & UI Settings "
	Public Shared Sub SetMessages()
		Try
			Language.setMessage(mstrModuleName, 1, "Analysis for this Applicant is present.")
			Language.setMessage(mstrModuleName, 2, "Incomplete")
			Language.setMessage(mstrModuleName, 3, "Complete")
			Language.setMessage(mstrModuleName, 4, "Eligible")
			Language.setMessage(mstrModuleName, 5, "Not Eligible")
			Language.setMessage(mstrModuleName, 6, "Analysis for this Interviewer is already present.")
			Language.setMessage(mstrModuleName, 7, "Approved")
			Language.setMessage(mstrModuleName, 8, "Pending")
			Language.setMessage(mstrModuleName, 9, "Disapproved")
			Language.setMessage(mstrModuleName, 10, "Yes")
			Language.setMessage(mstrModuleName, 11, "No")
			Language.setMessage(mstrModuleName, 12, "Sorry, Selected applicant :")
			Language.setMessage(mstrModuleName, 13, "'s interview process is still not finished. Please complete all interview process for this applicant.")

		Catch Ex As Exception
			DisplayError.Show("-1", ex.Message, "SetMessages", mstrModuleName)
		End Try
	End Sub
#End Region 'Language & UI Settings
	'</Language>
End Class
