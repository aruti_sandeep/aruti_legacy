﻿'************************************************************************************************************************************
'Class Name : clsApplicantSkill_tran.vb
'Purpose    :
'Date       :21/08/2010
'Written By :Anjan
'Modified   :
'************************************************************************************************************************************
Imports eZeeCommonLib
Imports Aruti.Data


Public Class clsApplicantSkill_tran
    Private Const mstrModuleName = "clsApplicantSkill_tran"
    Dim objDataOperation As clsDataOperation
    Dim mstrMessage As String = ""

#Region " Private Variables "
    Private mintSkilltranunkid As Integer
    Private mintApplicantunkid As Integer
    Private mdtTran As DataTable

#End Region

#Region " Properties "

'S.SANDEEP [28-May-2018] -- START 
'ISSUE/ENHANCEMENT : {Audit Trails} 
Private mstrFormName As String = String.Empty
Public WriteOnly Property _FormName() As String 
Set(ByVal value As String) 
mstrFormName = value 
End Set 
End Property 
 
Private mstrClientIP As String = ""
Public WriteOnly Property _ClientIP() As String 
Set(ByVal value As String) 
mstrClientIP = value 
End Set 
End Property 
 
Private mstrHostName As String = ""
Public WriteOnly Property _HostName() As String 
Set(ByVal value As String) 
mstrHostName = value 
End Set 
End Property 
 
Private mblnIsWeb As Boolean = False
Public WriteOnly Property _FromWeb() As Boolean 
Set(ByVal value As Boolean) 
mblnIsWeb = value 
End Set 
End Property 
 
    Private mintAuditUserId As Integer = 0
    Public WriteOnly Property _AuditUserId() As Integer
        Set(ByVal value As Integer)
            mintAuditUserId = value
        End Set
    End Property

    Private mintCompanyUnkid As Integer = 0
    Public WriteOnly Property _CompanyUnkid() As Integer
        Set(ByVal value As Integer)
            mintCompanyUnkid = value
        End Set
    End Property
 
Private mdtAuditDate As DateTime = Now
Public WriteOnly Property _AuditDate() As DateTime 
Set(ByVal value As DateTime) 
mdtAuditDate = value 
End Set 
End Property 
 
Private xDataOpr As clsDataOperation = Nothing
Public Property _xDataOpr() As clsDataOperation 
Get 
Return xDataOpr 
End Get 
Set(ByVal value As clsDataOperation) 
xDataOpr = value 
End Set 
End Property 
 
Private mintLoginEmployeeunkid As Integer = 0
Public Property _LoginEmployeeunkid() As Integer 
Get 
Return mintLoginEmployeeunkid 
End Get 
Set(ByVal value As Integer) 
mintLoginEmployeeunkid = value 
End Set 
End Property 
 
Private mintVoidlogingemployeeunkid As Integer=0
Public Property _Voidloginemployeeunkid() As Integer 
Get 
Return mintVoidlogingemployeeunkid 
End Get 
Set(ByVal value As Integer) 
mintVoidlogingemployeeunkid = value 
End Set 
End Property 
'S.SANDEEP [28-May-2018] -- END 

    Public Property _ApplicantUnkid() As Integer
        Get
            Return mintApplicantunkid
        End Get
        Set(ByVal value As Integer)
            mintApplicantunkid = value
            Call GetSkill_tran()
        End Set
    End Property

    Public Property _DataList() As DataTable
        Get
            Return mdtTran
        End Get
        Set(ByVal value As DataTable)
            mdtTran = value
        End Set
    End Property

    'Sohail (02 May 2022) -- Start
    'Enhancement :  AC2-312 / AC2-326 : ZRA - Newly added skills should not populate on old job applications for closed vacancies on applicant master.
    Private mdtVacancyEndDate As Date
    Public WriteOnly Property _VacancyEndDate() As Date
        Set(ByVal value As Date)
            mdtVacancyEndDate = value
        End Set
    End Property
    'Sohail (02 May 2022) -- End

#End Region

#Region " Constructor "
    Public Sub New()
        mdtTran = New DataTable("applicantskilltran")
        Dim dcol As DataColumn

        dcol = New DataColumn("skilltranunkid")
        dcol.DataType = System.Type.GetType("System.Int32")
        mdtTran.Columns.Add(dcol)

        dcol = New DataColumn("applicantunkid")
        dcol.DataType = System.Type.GetType("System.Int32")
        mdtTran.Columns.Add(dcol)

        dcol = New DataColumn("skillcategoryunkid")
        dcol.DataType = System.Type.GetType("System.Int32")
        mdtTran.Columns.Add(dcol)

        dcol = New DataColumn("skillunkid")
        dcol.DataType = System.Type.GetType("System.Int32")
        mdtTran.Columns.Add(dcol)

        dcol = New DataColumn("remark")
        dcol.DataType = System.Type.GetType("System.String")
        mdtTran.Columns.Add(dcol)


        'Pinkal (06-Feb-2012) -- Start
        'Enhancement : TRA Changes
        dcol = New DataColumn("other_skillcategory")
        dcol.DataType = System.Type.GetType("System.String")
        mdtTran.Columns.Add(dcol)

        dcol = New DataColumn("other_skill")
        dcol.DataType = System.Type.GetType("System.String")
        mdtTran.Columns.Add(dcol)

        'Pinkal (06-Feb-2012) -- End

        'Sohail (27 Apr 2017) -- Start
        'TRA Enhancement - 66.1 - Online Recruitment Redesign changes for tra security issues.
        mdtTran.Columns.Add("skillexpertiseunkid", System.Type.GetType("System.Int32")).DefaultValue = 0
        'skillexpertiseunkid
        'Sohail (27 Apr 2017) -- End


        dcol = New DataColumn("AUD")
        dcol.DataType = System.Type.GetType("System.String")
        dcol.AllowDBNull = True
        dcol.DefaultValue = DBNull.Value
        mdtTran.Columns.Add(dcol)

        dcol = New DataColumn("GUID")
        dcol.DataType = System.Type.GetType("System.String")
        mdtTran.Columns.Add(dcol)

        'Sohail (26 May 2017) -- Start
        'Issue - 67.1 - applicants details gets removed if he changes after data is imported.
        mdtTran.Columns.Add("serverskilltranunkid", System.Type.GetType("System.Int32")).DefaultValue = 0
        mdtTran.Columns.Add("isvoid", System.Type.GetType("System.Boolean")).DefaultValue = False
        mdtTran.Columns.Add("voiduserunkid", System.Type.GetType("System.Int32")).DefaultValue = 0
        mdtTran.Columns.Add("voiddatetime", System.Type.GetType("System.DateTime")).DefaultValue = Nothing
        mdtTran.Columns.Add("voidreason", System.Type.GetType("System.String")).DefaultValue = ""
        'Sohail (26 May 2017) -- End

        'Hemant (11 Apr 2022) -- Start            
        'ISSUE/ENHANCEMENT(ZRA) : the updated information gets updated across all the vacancies including the closed ones. Only open vacancies should be updated with the new information
        dcol = New DataColumn("created_date")
        dcol.DataType = System.Type.GetType("System.DateTime")
        mdtTran.Columns.Add(dcol)
        'Hemant (11 Apr 2022) -- End

    End Sub
#End Region

#Region " Private Methods "
    Private Sub GetSkill_tran()
        Dim strQ As String = ""
        Dim strErrorMessage As String = ""
        Dim dsList As DataSet = Nothing
        Dim dRowID_Tran As DataRow
        Dim exForce As Exception

        objDataOperation = New clsDataOperation

        Try


            'Pinkal (06-Feb-2012) -- Start
            'Enhancement : TRA Changes

            'strQ = "SELECT  skilltranunkid" & _
            '                ", applicantunkid " & _
            '                ", skillcategoryunkid " & _
            '                ", skillunkid " & _
            '                ", remark " & _
            '                ", '' AUD " & _
            '        "FROM rcapplicantskill_tran " & _
            '        "WHERE applicantunkid = @applicantunkid "

            strQ = "SELECT  skilltranunkid" & _
                            ", applicantunkid " & _
                            ", skillcategoryunkid " & _
                            ", skillunkid " & _
                            ", remark " & _
                          ", ISNULL(other_skillcategory,'') as other_skillcategory " & _
                          ", ISNULL(other_skill,'') as other_skill " & _
                          ", ISNULL(skillexpertiseunkid,0) as skillexpertiseunkid " & _
                            ", '' AUD " & _
                            ", ISNULL(serverskilltranunkid, 0) AS serverskilltranunkid " & _
                        ", isvoid " & _
                        ", voiduserunkid " & _
                        ", voiddatetime " & _
                        ", voidreason " & _
                        ", created_date " & _
                    "FROM rcapplicantskill_tran " & _
                    "WHERE ISNULL(isvoid, 0) = 0 AND applicantunkid = @applicantunkid "
            'Hemant (11 Apr 2022) -- [created_date]
            'Sohail (26 May 2017) - [serverskilltranunkid, isvoid, voiduserunkid, voiddatetime, voidreason]
            'Sohail (27 Apr 2017) - [skillexpertiseunkid]
            'Pinkal (06-Feb-2012) -- End

            'Sohail (02 May 2022) -- Start
            'Enhancement :  AC2-312 / AC2-326 : ZRA - Newly added skills should not populate on old job applications for closed vacancies on applicant master.
            If mdtVacancyEndDate <> Nothing Then
                strQ &= " AND ISNULL(CONVERT(CHAR(8), rcapplicantskill_tran.created_date ,112), '19000101') <= '" & eZeeDate.convertDate(mdtVacancyEndDate) & "' "
            End If
            'Sohail (02 May 2022) -- End

            objDataOperation.AddParameter("@applicantunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintApplicantunkid)

            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & " :" & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            'S.SANDEEP [ 28 FEB 2012 ] -- START
            'ENHANCEMENT : TRA CHANGES
            mdtTran.Rows.Clear()
            'S.SANDEEP [ 28 FEB 2012 ] -- END

            For i As Integer = 0 To dsList.Tables(0).Rows.Count - 1
                With dsList.Tables(0).Rows(i)
                    dRowID_Tran = mdtTran.NewRow
                    dRowID_Tran.Item("skilltranunkid") = .Item("skilltranunkid").ToString
                    dRowID_Tran.Item("applicantunkid") = .Item("applicantunkid").ToString
                    dRowID_Tran.Item("skillcategoryunkid") = .Item("skillcategoryunkid").ToString
                    dRowID_Tran.Item("skillunkid") = .Item("skillunkid").ToString
                    dRowID_Tran.Item("remark") = .Item("remark").ToString


                    'Pinkal (06-Feb-2012) -- Start
                    'Enhancement : TRA Changes
                    dRowID_Tran.Item("other_skillcategory") = .Item("other_skillcategory").ToString
                    dRowID_Tran.Item("other_skill") = .Item("other_skill").ToString
                    'Pinkal (06-Feb-2012) -- End
                    dRowID_Tran.Item("skillexpertiseunkid") = CInt(.Item("skillexpertiseunkid")) 'Sohail (27 Apr 2017)

                    dRowID_Tran.Item("AUD") = .Item("AUD").ToString
                    'Sohail (26 May 2017) -- Start
                    'Issue - 67.1 - applicants details gets removed if he changes after data is imported.
                    dRowID_Tran.Item("serverskilltranunkid") = .Item("serverskilltranunkid")
                    dRowID_Tran.Item("isvoid") = .Item("isvoid")
                    dRowID_Tran.Item("voiduserunkid") = .Item("voiduserunkid")
                    If IsDBNull(.Item("voiddatetime")) = True Then
                        dRowID_Tran.Item("voiddatetime") = DBNull.Value
                    Else
                        dRowID_Tran.Item("voiddatetime") = .Item("voiddatetime")
                    End If
                    dRowID_Tran.Item("voidreason") = .Item("voidreason")
                    'Sohail (26 May 2017) -- End
                    'Hemant (11 Apr 2022) -- Start            
                    'ISSUE/ENHANCEMENT(ZRA) : the updated information gets updated across all the vacancies including the closed ones. Only open vacancies should be updated with the new information
                    dRowID_Tran.Item("created_date") = .Item("created_date")
                    'Hemant (11 Apr 2022) -- End
                    mdtTran.Rows.Add(dRowID_Tran)
                End With
            Next

        Catch ex As Exception
            'DisplayError.Show("-1", ex.Message, "GetSkill_tran", mstrModuleName)
            Throw New Exception(ex.Message & "; Procedure Name: GetSkill_tran; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try

    End Sub

    Public Function InsertUpdateDelete_SkillTran() As Boolean
        Dim i As Integer
        Dim strQ As String = ""
        Dim strErrorMessage As String = ""
        Dim exForce As Exception

        Try
            objDataOperation = New clsDataOperation
            objDataOperation.BindTransaction()
            For i = 0 To mdtTran.Rows.Count - 1
                With mdtTran.Rows(i)
                    objDataOperation.ClearParameters()
                    If Not IsDBNull(.Item("AUD")) Then
                        Select Case .Item("AUD")
                            Case "A"
                               

                                'Pinkal (06-Feb-2012) -- Start
                                'Enhancement : TRA Changes

                                'strQ = "INSERT INTO rcapplicantskill_tran ( " & _
                                '  "  applicantunkid " & _
                                '  ", skillcategoryunkid " & _
                                '  ", skillunkid " & _
                                '  ", remark" & _
                                '") VALUES (" & _
                                '  "  @applicantunkid " & _
                                '  ", @skillcategoryunkid " & _
                                '  ", @skillunkid " & _
                                '  ", @remark" & _
                                '"); SELECT @@identity"

                                strQ = "INSERT INTO rcapplicantskill_tran ( " & _
                                  "  applicantunkid " & _
                                  ", skillcategoryunkid " & _
                                  ", skillunkid " & _
                                  ", remark" & _
                                ", other_skillcategory " & _
                                ", other_skill " & _
                                ", skillexpertiseunkid " & _
                                        ", serverskilltranunkid" & _
                                        ", isvoid" & _
                                        ", voiduserunkid" & _
                                        ", voiddatetime" & _
                                        ", voidreason" & _
                                          ", created_date " & _
                                ") VALUES (" & _
                                  "  @applicantunkid " & _
                                  ", @skillcategoryunkid " & _
                                  ", @skillunkid " & _
                                  ", @remark" & _
                                ", @other_skillcategory " & _
                                ", @other_skill " & _
                                ", @skillexpertiseunkid " & _
                                        ", @serverskilltranunkid" & _
                                        ", @isvoid" & _
                                        ", @voiduserunkid" & _
                                        ", @voiddatetime" & _
                                        ", @voidreason" & _
                                          ", @created_date " & _
                                "); SELECT @@identity"
                                'Hemant (11 Apr 2022) -- [created_date]
                                'Sohail (26 May 2017) - [serverskilltranunkid, isvoid, voiduserunkid, voiddatetime, voidreason]
                                'Sohail (27 Apr 2017) - [skillexpertiseunkid]
                                'Pinkal (06-Feb-2012) -- End

                                'Sohail (11 Sep 2010) -- Start
                                'objDataOperation.AddParameter("@applicantunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, .Item("applicantunkid").ToString)
                                objDataOperation.AddParameter("@applicantunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintApplicantunkid)
                                'Sohail (11 Sep 2010) -- End
                                objDataOperation.AddParameter("@skillcategoryunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, .Item("skillcategoryunkid").ToString)
                                objDataOperation.AddParameter("@skillunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, .Item("skillunkid").ToString)
                                objDataOperation.AddParameter("@remark", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, .Item("remark").ToString)


                                'Pinkal (06-Feb-2012) -- Start
                                'Enhancement : TRA Changes
                                objDataOperation.AddParameter("@other_skillcategory", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, .Item("other_skillcategory").ToString)
                                objDataOperation.AddParameter("@other_skill", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, .Item("other_skill").ToString)
                                'Pinkal (06-Feb-2012) -- End
                                objDataOperation.AddParameter("@skillexpertiseunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, .Item("skillexpertiseunkid").ToString) 'Sohail (27 Apr 2017)
                                'Sohail (26 May 2017) -- Start
                                'Issue - 67.1 - applicants details gets removed if he changes after data is imported.
                                objDataOperation.AddParameter("@serverskilltranunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, .Item("serverskilltranunkid"))
                                objDataOperation.AddParameter("@isvoid", SqlDbType.Bit, eZeeDataType.BIT_SIZE, .Item("isvoid"))
                                objDataOperation.AddParameter("@voiduserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, .Item("voiduserunkid"))
                                If IsDBNull(.Item("voiddatetime")) = True Then
                                    objDataOperation.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, DBNull.Value)
                                Else
                                    objDataOperation.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, .Item("voiddatetime"))
                                End If
                                objDataOperation.AddParameter("@voidreason", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, .Item("voidreason").ToString)
                                'Sohail (26 May 2017) -- End
                                'Hemant (11 Apr 2022) -- Start            
                                'ISSUE/ENHANCEMENT(ZRA) : the updated information gets updated across all the vacancies including the closed ones. Only open vacancies should be updated with the new information
                                objDataOperation.AddParameter("@created_date", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, .Item("created_date").ToString)
                                'Hemant (11 Apr 2022) -- End

                                'Anjan (12 Oct 2011)-Start
                                'ENHANCEMENT : AUDIT TRAIL MAINTENANCE.
                                'objDataOperation.ExecNonQuery(strQ)
                                Dim dsList As DataSet = objDataOperation.ExecQuery(strQ, "List")
                                'Anjan (12 Oct 2011)-End 


                                If objDataOperation.ErrorMessage <> "" Then
                                    exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                                    Throw exForce
                                End If


                                'Anjan (12 Oct 2011)-Start
                                'ENHANCEMENT : AUDIT TRAIL MAINTENANCE.
                                mintSkilltranunkid = dsList.Tables(0).Rows(0)(0)

                                If .Item("applicantunkid") > 0 Then
                                    'S.SANDEEP [28-May-2018] -- START
                                     'ISSUE/ENHANCEMENT : {Audit Trails} 
                                    Dim objCommonATLog AS New clsCommonATLog 
                                    objCommonATLog._FormName = mstrFormName 
                                    objCommonATLog._LoginEmployeeUnkid = mintLoginEmployeeunkid 
                                    objCommonATLog._ClientIP = mstrClientIP
                                    objCommonATLog._HostName = mstrHostName
                                    objCommonATLog._FromWeb = mblnIsWeb
                                    objCommonATLog._AuditUserId = mintAuditUserId
objCommonATLog._CompanyUnkid = mintCompanyUnkid
                                    objCommonATLog._AuditDate = mdtAuditDate
                                    'S.SANDEEP [28-May-2018] -- END
                                    'Sohail (09 Oct 2018) -- Start
                                    'TANAPA - Support Issue Id # 2502 - linking recruitment server app_vacancy with aruti desktop app_vacancy in 75.1.
                                    'If clsCommonATLog.Insert_TranAtLog(objDataOperation, "rcapplicant_master", "applicantunkid", .Item("applicantunkid"), "rcapplicantskill_tran", "skilltranunkid", mintSkilltranunkid, 2, 1) = False Then
                                    If objCommonATLog.Insert_TranAtLog(objDataOperation, "rcapplicant_master", "applicantunkid", mintApplicantunkid, "rcapplicantskill_tran", "skilltranunkid", mintSkilltranunkid, 2, 1) = False Then
                                        'Sohail (09 Oct 2018) -- End
                                        exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                                        Throw exForce
                                    End If

                                    'S.SANDEEP [28-May-2018] -- START
                                    'ISSUE/ENHANCEMENT : {Audit Trails} 
                                    objCommonATLog = Nothing
                                    'S.SANDEEP [28-May-2018] -- END

                                Else
                                    'S.SANDEEP [28-May-2018] -- START
                                    'ISSUE/ENHANCEMENT : {Audit Trails} 
                                    Dim objCommonATLog As New clsCommonATLog
                                    objCommonATLog._FormName = mstrFormName
                                    objCommonATLog._LoginEmployeeUnkid = mintLoginEmployeeunkid
                                    objCommonATLog._ClientIP = mstrClientIP
                                    objCommonATLog._HostName = mstrHostName
                                    objCommonATLog._FromWeb = mblnIsWeb
                                    objCommonATLog._AuditUserId = mintAuditUserId
objCommonATLog._CompanyUnkid = mintCompanyUnkid
                                    objCommonATLog._AuditDate = mdtAuditDate
                                    'S.SANDEEP [28-May-2018] -- END

                                    If objCommonATLog.Insert_TranAtLog(objDataOperation, "rcapplicant_master", "applicantunkid", mintApplicantunkid, "rcapplicantskill_tran", "skilltranunkid", mintSkilltranunkid, 1, 1) = False Then
                                        exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                                        Throw exForce
                                    End If

                                    'S.SANDEEP [28-May-2018] -- START
                                    'ISSUE/ENHANCEMENT : {Audit Trails} 
                                    objCommonATLog = Nothing
                                    'S.SANDEEP [28-May-2018] -- END

                                End If
                                'Anjan (12 Oct 2011)-End 


                            Case "U"


                                'Pinkal (06-Feb-2012) -- Start
                                'Enhancement : TRA Changes

                                'strQ = "UPDATE rcapplicantskill_tran SET " & _
                                '        "  applicantunkid = @applicantunkid" & _
                                '        ", skillcategoryunkid = @skillcategoryunkid" & _
                                '        ", skillunkid = @skillunkid" & _
                                '        ", remark = @remark " & _
                                '        "WHERE skilltranunkid = @skilltranunkid "

                                strQ = "UPDATE rcapplicantskill_tran SET " & _
                                        "  applicantunkid = @applicantunkid" & _
                                        ", skillcategoryunkid = @skillcategoryunkid" & _
                                        ", skillunkid = @skillunkid" & _
                                        ", remark = @remark " & _
                                        ", other_skillcategory = @other_skillcategory " & _
                                        ", other_skill = @other_skill " & _
                                        ", skillexpertiseunkid = @skillexpertiseunkid " & _
                                        ", serverskilltranunkid = @serverskilltranunkid" & _
                                          ", isvoid = @isvoid" & _
                                          ", voiduserunkid = @voiduserunkid" & _
                                          ", voiddatetime = @voiddatetime" & _
                                          ", voidreason = @voidreason " & _
                                        "WHERE skilltranunkid = @skilltranunkid "
                                'Sohail (26 May 2017) - [serverskilltranunkid, isvoid, voiduserunkid, voiddatetime, voidreason]
                                'Pinkal (06-Feb-2012) -- End

                                objDataOperation.AddParameter("@skilltranunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, .Item("skilltranunkid").ToString)
                                objDataOperation.AddParameter("@applicantunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintApplicantunkid)
                                objDataOperation.AddParameter("@skillcategoryunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, .Item("skillcategoryunkid").ToString)
                                objDataOperation.AddParameter("@skillunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, .Item("skillunkid").ToString)
                                objDataOperation.AddParameter("@remark", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, .Item("remark").ToString)


                                'Pinkal (06-Feb-2012) -- Start
                                'Enhancement : TRA Changes
                                objDataOperation.AddParameter("@other_skillcategory", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, .Item("other_skillcategory").ToString)
                                objDataOperation.AddParameter("@other_skill", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, .Item("other_skill").ToString)
                                'Pinkal (06-Feb-2012) -- End
                                objDataOperation.AddParameter("@skillexpertiseunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, .Item("skillexpertiseunkid").ToString) 'Sohail (27 Apr 2017)
                                'Sohail (26 May 2017) -- Start
                                'Issue - 67.1 - applicants details gets removed if he changes after data is imported.
                                objDataOperation.AddParameter("@serverskilltranunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, .Item("serverskilltranunkid"))
                                objDataOperation.AddParameter("@isvoid", SqlDbType.Bit, eZeeDataType.BIT_SIZE, .Item("isvoid"))
                                objDataOperation.AddParameter("@voiduserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, .Item("voiduserunkid"))
                                If IsDBNull(.Item("voiddatetime")) = True Then
                                    objDataOperation.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, DBNull.Value)
                                Else
                                    objDataOperation.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, .Item("voiddatetime"))
                                End If
                                objDataOperation.AddParameter("@voidreason", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, .Item("voidreason").ToString)
                                'Sohail (26 May 2017) -- End

                                Call objDataOperation.ExecNonQuery(strQ)

                                If objDataOperation.ErrorMessage <> "" Then
                                    exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                                    Throw exForce
                                End If

                                'S.SANDEEP [28-May-2018] -- START
                                'ISSUE/ENHANCEMENT : {Audit Trails} 
                                Dim objCommonATLog As New clsCommonATLog
                                objCommonATLog._FormName = mstrFormName
                                objCommonATLog._LoginEmployeeUnkid = mintLoginEmployeeunkid
                                objCommonATLog._ClientIP = mstrClientIP
                                objCommonATLog._HostName = mstrHostName
                                objCommonATLog._FromWeb = mblnIsWeb
                                objCommonATLog._AuditUserId = mintAuditUserId
objCommonATLog._CompanyUnkid = mintCompanyUnkid
                                objCommonATLog._AuditDate = mdtAuditDate
                                'S.SANDEEP [28-May-2018] -- END

                                If objCommonATLog.Insert_TranAtLog(objDataOperation, "rcapplicant_master", "applicantunkid", .Item("applicantunkid"), "rcapplicantskill_tran", "skilltranunkid", .Item("skilltranunkid"), 2, 2) = False Then
                                    exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                                    Throw exForce
                                End If

                                'S.SANDEEP [28-May-2018] -- START
                                'ISSUE/ENHANCEMENT : {Audit Trails} 
                                objCommonATLog = Nothing
                                'S.SANDEEP [28-May-2018] -- END



                            Case "D"

                                If .Item("skilltranunkid") > 0 Then
                                    'S.SANDEEP [28-May-2018] -- START
                                    'ISSUE/ENHANCEMENT : {Audit Trails} 
                                    Dim objCommonATLog As New clsCommonATLog
                                    objCommonATLog._FormName = mstrFormName
                                    objCommonATLog._LoginEmployeeUnkid = mintLoginEmployeeunkid
                                    objCommonATLog._ClientIP = mstrClientIP
                                    objCommonATLog._HostName = mstrHostName
                                    objCommonATLog._FromWeb = mblnIsWeb
                                    objCommonATLog._AuditUserId = mintAuditUserId
objCommonATLog._CompanyUnkid = mintCompanyUnkid
                                    objCommonATLog._AuditDate = mdtAuditDate
                                    'S.SANDEEP [28-May-2018] -- END

                                    If objCommonATLog.Insert_TranAtLog(objDataOperation, "rcapplicant_master", "applicantunkid", .Item("applicantunkid"), "rcapplicantskill_tran", "skilltranunkid", .Item("skilltranunkid"), 2, 3) = False Then
                                        exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                                        Throw exForce
                                    End If

                                    'S.SANDEEP [28-May-2018] -- START
                                    'ISSUE/ENHANCEMENT : {Audit Trails} 
                                    objCommonATLog = Nothing
                                    'S.SANDEEP [28-May-2018] -- END

                                End If


                                strQ = "DELETE FROM rcapplicantskill_tran " & _
                                        "WHERE skilltranunkid = @skilltranunkid "

                                objDataOperation.AddParameter("@skilltranunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, .Item("skilltranunkid").ToString)

                                Call objDataOperation.ExecNonQuery(strQ)

                                If objDataOperation.ErrorMessage <> "" Then
                                    exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                                    Throw exForce
                                End If


                        End Select
                    End If
                End With
            Next
            objDataOperation.ReleaseTransaction(True)
            Return True
        Catch ex As Exception
            objDataOperation.ReleaseTransaction(False)
            'DisplayError.Show("-1", ex.Message, "InsertUpdateDelete_SkillTran", mstrModuleName)
            Throw New Exception(ex.Message & "; Procedure Name: InsertUpdateDelete_SkillTran; Module Name: " & mstrModuleName)
            Return False
        End Try

    End Function

    Public Function GetApplicantSkills(ByVal strSkillunkid As String) As DataSet
        Dim strQ As String = ""
        Dim strErrorMessage As String = ""
        Dim dsList As DataSet = Nothing
        Dim exForce As Exception
        objDataOperation = New clsDataOperation

        Try
            strQ = "SELECT DISTINCT applicantunkid " & _
                    "FROM rcapplicantskill_tran " & _
                    "WHERE skillunkid IN (" & strSkillunkid & " ) "

            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exforce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw New Exception
            End If
            Return dsList
        Catch ex As Exception
            'DisplayError.Show("-1", ex.Message, "GetApplicantSkills", mstrModuleName)
            Throw New Exception(ex.Message & "; Procedure Name: GetApplicantSkills; Module Name: " & mstrModuleName)
            Return Nothing
        End Try
    End Function


    'Pinkal (12-Oct-2011) -- Start
    'Enhancement : TRA Changes

    ' DON'T CHANGE THIS METHOD 

    Public Function GetApplicantSkillName(ByVal blnOtherQualification As Boolean, ByVal intApplicantunkid As Integer, ByVal dtVacancyEndDate As DateTime) As DataSet
        'Hemant (11 Apr 2022) -- [dtVacancyEndDate]
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""

        Try

            Dim objDataOperation As New clsDataOperation


            If blnOtherQualification Then

                strQ = "SELECT    " & _
                          " ISNULL(STUFF(( SELECT DISTINCT ',' + CAST(other_skillcategory AS VARCHAR(max)) " & _
                              " FROM rcapplicantskill_tran " & _
                              " WHERE rcapplicantskill_tran.applicantunkid =  @applicantunkid AND rcapplicantskill_tran.isvoid = 0 " & _
                              " AND rcapplicantskill_tran.created_date <= '" & eZeeDate.convertDate(dtVacancyEndDate) & "'  " & _
                              " FOR  XML PATH('')), 1, 1, ''),'') AS SkillCategory, " & _
                        " ISNULL(STUFF(( SELECT DISTINCT ',' + CAST(other_skill AS VARCHAR(max)) " & _
                            " FROM rcapplicantskill_tran " & _
                            " WHERE rcapplicantskill_tran.applicantunkid = @applicantunkid AND rcapplicantskill_tran.isvoid = 0 " & _
                            " AND rcapplicantskill_tran.created_date <= '" & eZeeDate.convertDate(dtVacancyEndDate) & "'  " & _
                            " FOR  XML PATH('')), 1, 1, ''),'')  AS Skill "
                'Hemant (11 Apr 2022) -- [" AND rcapplicantskill_tran.created_date <= '" & eZeeDate.convertDate(dtVacancyEndDate) & "'  " & _]

            Else

            strQ = "SELECT    " & _
                      " ISNULL(STUFF(( SELECT  ',' + CAST(cfcommon_master.name AS VARCHAR(max)) " & _
                          " FROM rcapplicantskill_tran " & _
                          " JOIN cfcommon_master ON cfcommon_master.masterunkid = rcapplicantskill_tran.skillcategoryunkid AND dbo.cfcommon_master.mastertype =" & clsCommon_Master.enCommonMaster.SKILL_CATEGORY & _
                              " WHERE rcapplicantskill_tran.applicantunkid =  @applicantunkid  AND rcapplicantskill_tran.isvoid = 0 " & _
                                  " AND rcapplicantskill_tran.created_date <= '" & eZeeDate.convertDate(dtVacancyEndDate) & "'  " & _
                          " ORDER BY cfcommon_master.masterunkid" & _
                          " FOR  XML PATH('')), 1, 1, ''),'') AS SkillCategory, " & _
                    " ISNULL(STUFF(( SELECT  ',' + CAST(hrskill_master.skillname AS VARCHAR(max)) " & _
                        " FROM rcapplicantskill_tran " & _
                        " JOIN hrskill_master ON rcapplicantskill_tran.skillunkid = hrskill_master.skillunkid  " & _
                            " WHERE rcapplicantskill_tran.applicantunkid = @applicantunkid  AND rcapplicantskill_tran.isvoid = 0  " & _
                                " AND rcapplicantskill_tran.created_date <= '" & eZeeDate.convertDate(dtVacancyEndDate) & "'  " & _
                        " ORDER BY hrskill_master.skillunkid " & _
                        " FOR  XML PATH('')), 1, 1, ''),'')  AS Skill "
                'Hemant (11 Apr 2022) -- [" AND rcapplicantskill_tran.created_date <= '" & eZeeDate.convertDate(dtVacancyEndDate) & "'  " & _]
            End If

            objDataOperation.ClearParameters()
            objDataOperation.AddParameter("@applicantunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intApplicantunkid)
            dsList = objDataOperation.ExecQuery(strQ, "List")

        Catch ex As Exception
            'DisplayError.Show("-1", ex.Message, "GetApplicantSkill", mstrModuleName)
            Throw New Exception(ex.Message & "; Procedure Name: GetApplicantSkill; Module Name: " & mstrModuleName)
        End Try
        Return dsList
    End Function

    'Pinkal (12-Oct-2011) -- End

#End Region
End Class


