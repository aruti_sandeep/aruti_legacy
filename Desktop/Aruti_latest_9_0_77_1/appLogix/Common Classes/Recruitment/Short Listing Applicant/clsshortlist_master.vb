﻿'************************************************************************************************************************************
'Class Name : clsshortlist_master.vb
'Purpose    :
'Date       :12/23/2011
'Written By :Pinkal Jariwala
'Modified   :
'************************************************************************************************************************************

Imports eZeeCommonLib
''' <summary>
''' Purpose: 
''' Developer: Pinkal Jariwala
''' </summary>
Public Class clsshortlist_master
    Private Shared ReadOnly mstrModuleName As String = "clsshortlist_master"
    Dim objDataOperation As clsDataOperation
    Dim objShortFilter As New clsshortlist_filter
    Dim mstrMessage As String = ""

#Region " Private variables "
    Private mintShortlistunkid As Integer
    Private mintVacancyunkid As Integer
    Private mstrRefno As String = String.Empty
    Private mblnIsInvalidEmpCode As Boolean
    Private mintUserunkid As Integer
    Private mblnIsvoid As Boolean
    Private mintVoiduserunkid As Integer
    Private mdtVoiddatetime As Date
    Private mstrVoidreason As String = String.Empty
    'S.SANDEEP [ 14 May 2013 ] -- START
    'ENHANCEMENT : TRA ENHANCEMENT
    Private mintStatustypid As Integer = 0
    Private mstrRemark As String = String.Empty
    Private mintApproveuserunkid As Integer = 0
    Private mblnIssent As Boolean = False
    Private mdtAppr_Date As Date = Nothing
    'S.SANDEEP [ 14 May 2013 ] -- END
#End Region

#Region " Properties "
'S.SANDEEP [28-May-2018] -- START 
'ISSUE/ENHANCEMENT : {Audit Trails} 
Private mstrFormName As String = String.Empty
Public WriteOnly Property _FormName() As String 
Set(ByVal value As String) 
mstrFormName = value 
End Set 
End Property 
 
Private mstrClientIP As String = ""
Public WriteOnly Property _ClientIP() As String 
Set(ByVal value As String) 
mstrClientIP = value 
End Set 
End Property 
 
Private mstrHostName As String = ""
Public WriteOnly Property _HostName() As String 
Set(ByVal value As String) 
mstrHostName = value 
End Set 
End Property 
 
Private mblnIsWeb As Boolean = False
Public WriteOnly Property _FromWeb() As Boolean 
Set(ByVal value As Boolean) 
mblnIsWeb = value 
End Set 
End Property 
 
    Private mintAuditUserId As Integer = 0
    Public WriteOnly Property _AuditUserId() As Integer
        Set(ByVal value As Integer)
            mintAuditUserId = value
        End Set
    End Property

    Private mintCompanyUnkid As Integer = 0
    Public WriteOnly Property _CompanyUnkid() As Integer
        Set(ByVal value As Integer)
            mintCompanyUnkid = value
        End Set
    End Property
 
Private mdtAuditDate As DateTime = Now
Public WriteOnly Property _AuditDate() As DateTime 
Set(ByVal value As DateTime) 
mdtAuditDate = value 
End Set 
End Property 
 
Private xDataOpr As clsDataOperation = Nothing
Public Property _xDataOpr() As clsDataOperation 
Get 
Return xDataOpr 
End Get 
Set(ByVal value As clsDataOperation) 
xDataOpr = value 
End Set 
End Property 
 
Private mintLoginEmployeeunkid As Integer = 0
Public Property _LoginEmployeeunkid() As Integer 
Get 
Return mintLoginEmployeeunkid 
End Get 
Set(ByVal value As Integer) 
mintLoginEmployeeunkid = value 
End Set 
End Property 
 
Private mintVoidlogingemployeeunkid As Integer=0
Public Property _Voidloginemployeeunkid() As Integer 
Get 
Return mintVoidlogingemployeeunkid 
End Get 
Set(ByVal value As Integer) 
mintVoidlogingemployeeunkid = value 
End Set 
End Property 
'S.SANDEEP [28-May-2018] -- END 
    ''' <summary>
    ''' Purpose: Get Message from Class 
    ''' Modify By: Pinkal Jariwala
    ''' </summary>
    Public ReadOnly Property _Message() As String
        Get
            Return mstrMessage
        End Get
    End Property

    ''' <summary>
    ''' Purpose: Get or Set shortlistunkid
    ''' Modify By: Pinkal Jariwala
    ''' </summary>
    Public Property _Shortlistunkid() As Integer
        Get
            Return mintShortlistunkid
        End Get
        Set(ByVal value As Integer)
            mintShortlistunkid = Value
            Call getData()
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set vacancyunkid
    ''' Modify By: Pinkal Jariwala
    ''' </summary>
    Public Property _Vacancyunkid() As Integer
        Get
            Return mintVacancyunkid
        End Get
        Set(ByVal value As Integer)
            mintVacancyunkid = Value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set refno
    ''' Modify By: Pinkal Jariwala
    ''' </summary>
    Public Property _Refno() As String
        Get
            Return mstrRefno
        End Get
        Set(ByVal value As String)
            mstrRefno = Value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set isinvalidempcode
    ''' Modify By: Pinkal Jariwala
    ''' </summary>
    Public Property _IsInvalidEmpcode() As Boolean
        Get
            Return mblnIsInvalidEmpCode
        End Get
        Set(ByVal value As Boolean)
            mblnIsInvalidEmpCode = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set userunkid
    ''' Modify By: Pinkal Jariwala
    ''' </summary>
    Public Property _Userunkid() As Integer
        Get
            Return mintUserunkid
        End Get
        Set(ByVal value As Integer)
            mintUserunkid = Value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set isvoid
    ''' Modify By: Pinkal Jariwala
    ''' </summary>
    Public Property _Isvoid() As Boolean
        Get
            Return mblnIsvoid
        End Get
        Set(ByVal value As Boolean)
            mblnIsvoid = Value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set voiduserunkid
    ''' Modify By: Pinkal Jariwala
    ''' </summary>
    Public Property _Voiduserunkid() As Integer
        Get
            Return mintVoiduserunkid
        End Get
        Set(ByVal value As Integer)
            mintVoiduserunkid = Value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set voiddatetime
    ''' Modify By: Pinkal Jariwala
    ''' </summary>
    Public Property _Voiddatetime() As Date
        Get
            Return mdtVoiddatetime
        End Get
        Set(ByVal value As Date)
            mdtVoiddatetime = Value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set voidreason
    ''' Modify By: Pinkal Jariwala
    ''' </summary>
    Public Property _Voidreason() As String
        Get
            Return mstrVoidreason
        End Get
        Set(ByVal value As String)
            mstrVoidreason = Value
        End Set
    End Property

    'S.SANDEEP [ 14 May 2013 ] -- START
    'ENHANCEMENT : TRA ENHANCEMENT
    ''' <summary>
    ''' Purpose: Get or Set statustypid
    ''' Modify By: Sandeep Sharma
    ''' </summary>
    Public Property _Statustypid() As Integer
        Get
            Return mintStatustypid
        End Get
        Set(ByVal value As Integer)
            mintStatustypid = Value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set remark
    ''' Modify By: Sandeep Sharma
    ''' </summary>
    Public Property _Remark() As String
        Get
            Return mstrRemark
        End Get
        Set(ByVal value As String)
            mstrRemark = Value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set approveuserunkid
    ''' Modify By: Sandeep Sharma
    ''' </summary>
    Public Property _Approveuserunkid() As Integer
        Get
            Return mintApproveuserunkid
        End Get
        Set(ByVal value As Integer)
            mintApproveuserunkid = Value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set issent
    ''' Modify By: Sandeep Sharma
    ''' </summary>
    Public Property _Issent() As Boolean
        Get
            Return mblnIssent
        End Get
        Set(ByVal value As Boolean)
            mblnIssent = Value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set appr_date
    ''' Modify By: Sandeep Sharma
    ''' </summary>
    Public Property _Appr_Date() As Date
        Get
            Return mdtAppr_Date
        End Get
        Set(ByVal value As Date)
            mdtAppr_Date = Value
        End Set
    End Property
    'S.SANDEEP [ 14 May 2013 ] -- END

#End Region

    ''' <summary>
    ''' Modify By: Pinkal Jariwala
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>
    Public Sub GetData()
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        objDataOperation = New clsDataOperation

        Try
            strQ = "SELECT " & _
              "  shortlistunkid " & _
              ", vacancyunkid " & _
              ", refno " & _
              ", isinvalidempcode " & _
              ", userunkid " & _
              ", isvoid " & _
              ", voiduserunkid " & _
              ", voiddatetime " & _
              ", voidreason " & _
              ", statustypid " & _
              ", remark " & _
              ", approveuserunkid " & _
              ", issent " & _
              ", appr_date " & _
             "FROM rcshortlist_master " & _
              "WHERE shortlistunkid = @shortlistunkid " 'S.SANDEEP [ 14 May 2013 (statustypid,remark,approveuserunkid,issent,appr_date) ] -- START -- END

            objDataOperation.AddParameter("@shortlistunkid", SqlDbType.int, eZeeDataType.INT_SIZE, mintShortlistUnkId.ToString)

            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            For Each dtRow As DataRow In dsList.Tables(0).Rows
                mintshortlistunkid = CInt(dtRow.Item("shortlistunkid"))
                mintvacancyunkid = CInt(dtRow.Item("vacancyunkid"))
                mstrRefno = dtRow.Item("refno").ToString
                mblnIsInvalidEmpCode = CBool(dtRow.Item("isinvalidempcode"))
                mintuserunkid = CInt(dtRow.Item("userunkid"))
                mblnisvoid = CBool(dtRow.Item("isvoid"))
                mintVoiduserunkid = CInt(dtRow.Item("voiduserunkid"))

                'S.SANDEEP [ 14 May 2013 ] -- START
                'ENHANCEMENT : TRA ENHANCEMENT
                'mdtVoiddatetime = dtRow.Item("voiddatetime")
                If IsDBNull(dtRow.Item("voiddatetime")) = False Then
                    mdtVoiddatetime = dtRow.Item("voiddatetime")
                Else
                    mdtVoiddatetime = Nothing
                End If
                'S.SANDEEP [ 14 May 2013 ] -- END


                mstrVoidreason = dtRow.Item("voidreason").ToString
                'S.SANDEEP [ 14 May 2013 ] -- START
                'ENHANCEMENT : TRA ENHANCEMENT
                mintStatustypid = CInt(dtRow.Item("statustypid"))
                mstrRemark = dtRow.Item("remark").ToString
                mintApproveuserunkid = CInt(dtRow.Item("approveuserunkid"))
                mblnIssent = CBool(dtRow.Item("issent"))
                If IsDBNull(dtRow.Item("appr_date")) Then
                    mdtAppr_Date = Nothing
                Else
                    mdtAppr_Date = dtRow.Item("appr_date")
                End If
                'S.SANDEEP [ 14 May 2013 ] -- END

                Exit For
            Next
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: GetData; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
    End Sub

    ''' <summary>
    ''' Modify By: Pinkal Jariwala
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>
    Public Function GetList(ByVal strTableName As String, Optional ByVal blnOnlyActive As Boolean = True) As DataSet
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        objDataOperation = New clsDataOperation

        Try
            'S.SANDEEP [ 28 FEB 2012 ] -- START
            'ENHANCEMENT : TRA CHANGES
            'strQ = "SELECT " & _
            '       "  shortlistunkid " & _
            '       ", rcshortlist_master.vacancyunkid " & _
            '       ", rcvacancy_master.vacancytitle " & _
            '       ", rcvacancy_master.openingdate " & _
            '       ", rcvacancy_master.closingdate " & _
            '       ", refno " & _
            '       ", isinvalidempcode " & _
            '       ", rcshortlist_master.userunkid " & _
            '       ", rcshortlist_master.isvoid " & _
            '       ", rcshortlist_master.voiduserunkid " & _
            '       ", rcshortlist_master.voiddatetime " & _
            '       ", rcshortlist_master.voidreason " & _
            '       " FROM rcshortlist_master " & _
            '       " JOIN rcvacancy_master on rcvacancy_master.vacancyunkid = rcshortlist_master.vacancyunkid "
            strQ = "SELECT " & _
              "  shortlistunkid " & _
              ", rcshortlist_master.vacancyunkid " & _
                    " ,cfcommon_master.name as vacancytitle " & _
                    " ,CONVERT(CHAR(8),rcvacancy_master.openingdate,112) AS openingdate " & _
                    " ,CONVERT(CHAR(8),rcvacancy_master.closingdate,112) AS closingdate " & _
              ", refno " & _
              ", isinvalidempcode " & _
              ", rcshortlist_master.userunkid " & _
              ", rcshortlist_master.isvoid " & _
              ", rcshortlist_master.voiduserunkid " & _
              ", rcshortlist_master.voiddatetime " & _
              ", rcshortlist_master.voidreason " & _
                   ", statustypid " & _
                   ", rcshortlist_master.remark " & _
                   ", approveuserunkid " & _
                   ", issent " & _
                   ", appr_date " & _
             " FROM rcshortlist_master " & _
                    " JOIN rcvacancy_master on rcvacancy_master.vacancyunkid = rcshortlist_master.vacancyunkid " & _
                    " JOIN cfcommon_master ON cfcommon_master.masterunkid = rcvacancy_master.vacancytitle AND mastertype = '" & clsCommon_Master.enCommonMaster.VACANCY_MASTER & "'"
            'S.SANDEEP [ 14 May 2013 (statustypid,remark,approveuserunkid,issent) ] -- START -- END

            'S.SANDEEP [ 28 FEB 2012 ] -- END

            If blnOnlyActive = True Then
                strQ &= " WHERE rcshortlist_master.isvoid = 0 "
            End If

            dsList = objDataOperation.ExecQuery(strQ, strTableName)

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            For Each dRow As DataRow In dsList.Tables(0).Rows
                dRow.Item("vacancytitle") = dRow.Item("vacancytitle") & " ( " & eZeeDate.convertDate(dRow.Item("openingdate").ToString).ToShortDateString & " - " & eZeeDate.convertDate(dRow.Item("closingdate").ToString).ToShortDateString & " ) "
            Next

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: GetList; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
        Return dsList
    End Function

    'S.SANDEEP [ 14 May 2013 ] -- START
    'ENHANCEMENT : TRA ENHANCEMENT
    Public Function Get_Table(ByVal strTableName As String, ByVal intVacancyId As Integer, Optional ByVal strShortListIds As String = "" _
                                        , Optional ByVal blnIsIncludeFinalApplicant As Boolean = False _
                                        , Optional ByVal blnIsIncludeApplicantCount As Boolean = True _
                                        , Optional ByVal mstrFilter As String = "") As DataTable   'Pinkal (12-May-2013) [blnIsIncludeFinalApplicant,blnIsIncludeApplicantCount,mstrFilter]
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception
        Dim dsTran As DataSet = Nothing
        objDataOperation = New clsDataOperation
        Try

            strQ = "SELECT " & _
                   "  CAST(0 AS BIT) AS ischeck" & _
                   " ,refno " & _
                   " ,T_Count " & _
                   " ,cfcommon_master.name AS vacancytitle " & _
                   " ,CASE WHEN statustypid = '" & enShortListing_Status.SC_APPROVED & "' THEN @Approved WHEN statustypid = '" & enShortListing_Status.SC_PENDING & "' THEN @Pending WHEN statustypid = '" & enShortListing_Status.SC_REJECT & "' THEN @Reject END AS Status " & _
                   " ,ISNULL(rcshortlist_master.remark,'') AS remark " & _
                   " ,statustypid AS StatusId " & _
                   " ,shortlistunkid " & _
                   " ,rcshortlist_master.vacancyunkid " & _
                   " ,CONVERT(CHAR(8),rcvacancy_master.openingdate,112)AS openingdate " & _
                   " ,CONVERT(CHAR(8),rcvacancy_master.closingdate,112)AS closingdate " & _
                   " ,isinvalidempcode " & _
                   " ,rcshortlist_master.userunkid " & _
                   " ,rcshortlist_master.isvoid " & _
                   " ,rcshortlist_master.voiduserunkid " & _
                   " ,rcshortlist_master.voiddatetime " & _
                   " ,rcshortlist_master.voidreason " & _
                   ",rcshortlist_master.issent " & _
                   ",rcshortlist_master.appr_date " & _
                   "FROM rcshortlist_master " & _
                   " JOIN rcvacancy_master ON rcvacancy_master.vacancyunkid = rcshortlist_master.vacancyunkid " & _
                   " JOIN cfcommon_master ON cfcommon_master.masterunkid = rcvacancy_master.vacancytitle AND mastertype = '" & clsCommon_Master.enCommonMaster.VACANCY_MASTER & "' " & _
                   " JOIN " & _
                   " ( " & _
                   "	SELECT " & _
                   "		 shortlistunkid AS SrtId " & _
                   "		,COUNT(DISTINCT applicantunkid) AS T_Count " & _
                   "	FROM rcshortlist_finalapplicant " & _
                   "	WHERE isvoid = 0 AND isshortlisted = 1 "
            'Pinkal (12-May-2013) -- Start
            'Enhancement : TRA Changes
            If blnIsIncludeFinalApplicant Then
                strQ &= " AND isfinalshortlisted =  1 "
            End If

            If mstrFilter.Trim.Length > 0 Then
                strQ &= " AND  " & mstrFilter
            End If
            'Pinkal (12-May-2013) -- End

                   strQ &="	GROUP BY  shortlistunkid " & _
                   " ) AS Cnt ON Cnt.SrtId = rcshortlist_master.shortlistunkid " & _
                   "WHERE rcshortlist_master.isvoid = 0 AND rcshortlist_master.vacancyunkid = '" & intVacancyId & "' "

            If strShortListIds.Length > 0 Then
                strQ &= " AND  shortlistunkid IN(" & strShortListIds & ") "
            End If

            objDataOperation.AddParameter("@Approved", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 2, "Approved"))
            objDataOperation.AddParameter("@Pending", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 3, "Pending"))
            objDataOperation.AddParameter("@Reject", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 4, "Disapprove"))

            dsList = objDataOperation.ExecQuery(strQ, strTableName)

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            Dim dtTable As New DataTable
            dtTable.Columns.Add("ischeck", System.Type.GetType("System.Boolean")).DefaultValue = False
            dtTable.Columns.Add("refno", System.Type.GetType("System.String")).DefaultValue = ""
            dtTable.Columns.Add("vacancytitle", System.Type.GetType("System.String")).DefaultValue = ""
            dtTable.Columns.Add("Status", System.Type.GetType("System.String")).DefaultValue = ""
            dtTable.Columns.Add("remark", System.Type.GetType("System.String")).DefaultValue = ""
            dtTable.Columns.Add("StatusId", System.Type.GetType("System.Int32")).DefaultValue = 0
            dtTable.Columns.Add("shortlistunkid", System.Type.GetType("System.Int32")).DefaultValue = 0
            dtTable.Columns.Add("vacancyunkid", System.Type.GetType("System.Int32")).DefaultValue = 0
            dtTable.Columns.Add("applicantunkid", System.Type.GetType("System.Int32")).DefaultValue = 0
            dtTable.Columns.Add("IsGrp", System.Type.GetType("System.Boolean")).DefaultValue = False
            dtTable.Columns.Add("GrpId", System.Type.GetType("System.Int32")).DefaultValue = 0
            dtTable.Columns.Add("openingdate", System.Type.GetType("System.String")).DefaultValue = False
            dtTable.Columns.Add("closingdate", System.Type.GetType("System.String")).DefaultValue = 0
            dtTable.Columns.Add("orefno", System.Type.GetType("System.String")).DefaultValue = ""
            dtTable.Columns.Add("sortid", System.Type.GetType("System.Int32")).DefaultValue = 0
            dtTable.Columns.Add("issent", System.Type.GetType("System.Boolean")).DefaultValue = False
            dtTable.Columns.Add("appr_date", System.Type.GetType("System.DateTime")).DefaultValue = Nothing

            'Pinkal (12-May-2013) -- Start
            'Enhancement : TRA Changes
            dtTable.Columns.Add("gender", System.Type.GetType("System.String")).DefaultValue = ""
            dtTable.Columns.Add("birthdate", System.Type.GetType("System.DateTime")).DefaultValue = DBNull.Value
            dtTable.Columns.Add("phone", System.Type.GetType("System.String")).DefaultValue = ""
            dtTable.Columns.Add("email", System.Type.GetType("System.String")).DefaultValue = ""
            'Pinkal (12-May-2013) -- End

            Dim dtRow As DataRow = Nothing

            For Each dRow As DataRow In dsList.Tables(0).Rows
                dRow.Item("vacancytitle") = dRow.Item("vacancytitle") & " ( " & eZeeDate.convertDate(dRow.Item("openingdate").ToString).ToShortDateString & " - " & eZeeDate.convertDate(dRow.Item("closingdate").ToString).ToShortDateString & " ) "

                dtRow = dtTable.NewRow

                dtRow.Item("ischeck") = dRow.Item("ischeck")


                'Pinkal (12-May-2013) -- Start
                'Enhancement : TRA Changes
                If blnIsIncludeApplicantCount Then
                dtRow.Item("refno") = dRow.Item("refno") & " - (" & dRow.Item("T_Count").ToString & ")"
                Else
                    dtRow.Item("refno") = dRow.Item("refno")
                End If
                'Pinkal (12-May-2013) -- End

                dtRow.Item("vacancytitle") = dRow.Item("vacancytitle")
                dtRow.Item("Status") = dRow.Item("Status")
                dtRow.Item("remark") = dRow.Item("remark")
                dtRow.Item("StatusId") = dRow.Item("StatusId")
                dtRow.Item("shortlistunkid") = dRow.Item("shortlistunkid")
                dtRow.Item("vacancyunkid") = dRow.Item("vacancyunkid")
                dtRow.Item("IsGrp") = True
                dtRow.Item("GrpId") = dRow.Item("shortlistunkid")
                dtRow.Item("openingdate") = dRow.Item("openingdate")
                dtRow.Item("closingdate") = dRow.Item("closingdate")
                dtRow.Item("orefno") = dRow.Item("refno")
                dtRow.Item("issent") = dRow.Item("issent")
                dtRow.Item("appr_date") = dRow.Item("appr_date")
                'Pinkal (12-May-2013) -- Start
                'Enhancement : TRA Changes
                dtRow.Item("gender") = ""
                dtRow.Item("birthdate") = DBNull.Value
                dtRow.Item("phone") = ""
                dtRow.Item("email") = ""
                'Pinkal (12-May-2013) -- End

                dtTable.Rows.Add(dtRow)


                'Pinkal (12-May-2013) -- Start
                'Enhancement : TRA Changes

                'strQ = "SELECT DISTINCT " & _
                ' "  applicant_code AS App_Code " & _
                ' " ,ISNULL(firstname,'')+' '+ISNULL(surname,'') AS Appl_Name " & _
                ' " ,rcshortlist_finalapplicant.applicantunkid AS AppId " & _
                ' "FROM rcshortlist_finalapplicant " & _
                ' " JOIN rcapplicant_master ON rcshortlist_finalapplicant.applicantunkid = rcapplicant_master.applicantunkid " & _
                ' "WHERE isvoid = 0 AND isshortlisted = 1 AND shortlistunkid = '" & dRow.Item("shortlistunkid") & "' "


                strQ = "SELECT DISTINCT " & _
                       "  CASE WHEN rcapplicant_master.employeeunkid > 0 THEN hremployee_master.employeecode ELSE applicant_code  END AS App_Code " & _
                       " ,ISNULL(rcapplicant_master.firstname,'')+' '+ISNULL(rcapplicant_master.surname,'') AS Appl_Name " & _
                       " ,rcshortlist_finalapplicant.applicantunkid AS AppId " & _
                         " ,CASE WHEN rcapplicant_master.gender = 1 THEN @male " & _
                         "         WHEN rcapplicant_master.gender = 2 THEN @female " & _
                         "         WHEN rcapplicant_master.gender = 3 THEN @other " & _
                         "         ELSE '' " & _
                         "    END AS gender " & _
                         ",rcapplicant_master.birth_date As birthdate " & _
                         ",ISNULL(rcapplicant_master.present_mobileno,'') AS phone " & _
                         ",rcapplicant_master.email " & _
                         ",rcapplicant_master.employeeunkid " & _
                       "FROM rcshortlist_finalapplicant " & _
                       " JOIN rcapplicant_master ON rcshortlist_finalapplicant.applicantunkid = rcapplicant_master.applicantunkid " & _
                       " LEFT JOIN hremployee_master ON hremployee_master.employeeunkid = rcapplicant_master.employeeunkid " & _
                       "WHERE rcshortlist_finalapplicant.isvoid = 0 AND isshortlisted = 1 AND shortlistunkid = '" & dRow.Item("shortlistunkid") & "' "

                'Pinkal (12-Nov-2020) -- Enhancement NMB Recruitment UAT-   Working on Recruitement UAT Changes for NMB.[ "  CASE WHEN rcapplicant_master.employeeunkid > 0 THEN hremployee_master.employeecode ELSE applicant_code  END AS App_Code ,rcapplicant_master.employeeunkid " & _]

                'Pinkal (20-Oct-2020) -- Enhancement Recruitment NMB -   Working on Recruitement Changes for NMB.[ rcshortlist_finalapplicant.isvoid = 0]

                If blnIsIncludeFinalApplicant Then
                    strQ &= " AND isfinalshortlisted =  1 "
                End If

                If mstrFilter.Trim.Length > 0 Then
                    strQ &= " AND  " & mstrFilter
                End If
                
                objDataOperation.ClearParameters()
                objDataOperation.AddParameter("@male", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage("clsEmployee_Master", 5, "Male"))
                objDataOperation.AddParameter("@female", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage("clsEmployee_Master", 6, "Female"))
                objDataOperation.AddParameter("@other", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage("clsEmployee_Master", 7, "Other"))
                'Pinkal (12-May-2013) -- End

                dsTran = objDataOperation.ExecQuery(strQ, strTableName)

                If objDataOperation.ErrorMessage <> "" Then
                    exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                    Throw exForce
                End If

                For Each dARow As DataRow In dsTran.Tables(0).Rows
                    dtRow = dtTable.NewRow

                    dtRow.Item("ischeck") = dRow.Item("ischeck")
                    dtRow.Item("refno") = Space(5) & dARow.Item("App_Code") & " - " & dARow.Item("Appl_Name")
                    dtRow.Item("vacancytitle") = ""
                    dtRow.Item("Status") = ""
                    dtRow.Item("remark") = ""
                    dtRow.Item("StatusId") = dRow.Item("StatusId")
                    dtRow.Item("shortlistunkid") = dRow.Item("shortlistunkid")
                    dtRow.Item("vacancyunkid") = dRow.Item("vacancyunkid")
                    dtRow.Item("IsGrp") = False
                    dtRow.Item("GrpId") = dRow.Item("shortlistunkid")
                    dtRow.Item("openingdate") = dRow.Item("openingdate")
                    dtRow.Item("closingdate") = dRow.Item("closingdate")
                    dtRow.Item("applicantunkid") = dARow.Item("AppId")
                    dtRow.Item("orefno") = dRow.Item("refno")
                    dtRow.Item("sortid") = 1
                    dtRow.Item("issent") = dRow.Item("issent")

                    'Pinkal (12-May-2013) -- Start
                    'Enhancement : TRA Changes
                    dtRow.Item("gender") = dARow.Item("gender")
                    If Not IsDBNull(dARow.Item("birthdate")) Then
                        dtRow.Item("birthdate") = CDate(dARow.Item("birthdate")).ToShortDateString
                    End If
                    dtRow.Item("phone") = dARow.Item("phone")
                    dtRow.Item("email") = dARow.Item("email")
                    'Pinkal (12-May-2013) -- End


                    dtTable.Rows.Add(dtRow)
                Next
            Next

            Return dtTable

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: GetList; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
    End Function
    'S.SANDEEP [ 14 May 2013 ] -- END


    ''' <summary>
    ''' Modify By: Pinkal Jariwala
    ''' </summary>
    ''' <returns>Boolean</returns>
    ''' <purpose> INSERT INTO Database Table (rcshortlist_master) </purpose>
    Public Function Insert(ByVal mdtFilter As DataTable, ByVal mdtApplicant As DataTable) As Boolean
        'If isExist(mstrName) Then
        '    mstrMessage = "<Message>"
        '    Return False
        'End If

        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        objDataOperation = New clsDataOperation
        objDataOperation.BindTransaction()

        Try
            objDataOperation.AddParameter("@vacancyunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintVacancyunkid.ToString)
            objDataOperation.AddParameter("@refno", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrRefno.ToString)
            objDataOperation.AddParameter("@isinvalidempcode", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsInvalidEmpCode.ToString)
            objDataOperation.AddParameter("@userunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintUserunkid.ToString)
            objDataOperation.AddParameter("@isvoid", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsvoid.ToString)
            objDataOperation.AddParameter("@voiduserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintVoiduserunkid.ToString)
            objDataOperation.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, IIf(mdtVoiddatetime <> Nothing, mdtVoiddatetime, DBNull.Value))
            objDataOperation.AddParameter("@voidreason", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrVoidreason.ToString)

            'S.SANDEEP [ 14 May 2013 ] -- START
            'ENHANCEMENT : TRA ENHANCEMENT
            objDataOperation.AddParameter("@statustypid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintStatustypid.ToString)
            objDataOperation.AddParameter("@remark", SqlDbType.VarChar, mstrRemark.Length, mstrRemark.ToString)
            objDataOperation.AddParameter("@approveuserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintApproveuserunkid.ToString)
            objDataOperation.AddParameter("@issent", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIssent.ToString)
            If mdtAppr_Date <> Nothing Then
                objDataOperation.AddParameter("@appr_date", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, mdtAppr_Date)
            Else
                objDataOperation.AddParameter("@appr_date", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, DBNull.Value)
            End If
            'S.SANDEEP [ 14 May 2013 ] -- END

            strQ = "INSERT INTO rcshortlist_master ( " & _
                      "  vacancyunkid " & _
                      ", refno " & _
                      ", isinvalidempcode " & _
                      ", userunkid " & _
                      ", isvoid " & _
                      ", voiduserunkid " & _
                      ", voiddatetime " & _
                      ", voidreason" & _
                      ", statustypid " & _
                      ", remark " & _
                      ", approveuserunkid " & _
                      ", issent " & _
                      ", appr_date" & _
                    ") VALUES (" & _
                      "  @vacancyunkid " & _
                      ", @refno " & _
                      ", @isinvalidempcode " & _
                      ", @userunkid " & _
                      ", @isvoid " & _
                      ", @voiduserunkid " & _
                      ", @voiddatetime " & _
                      ", @voidreason" & _
                      ", @statustypid " & _
                      ", @remark " & _
                      ", @approveuserunkid " & _
                      ", @issent " & _
                      ", @appr_date" & _
                    "); SELECT @@identity" 'S.SANDEEP [ 14 May 2013 (statustypid,remark,approveuserunkid,appr_date) ] -- START -- END

            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            mintShortlistunkid = dsList.Tables(0).Rows(0).Item(0)




            objShortFilter._Shortlistunkid = mintShortlistunkid
            objShortFilter._DtFilter = mdtFilter
            objShortFilter._Userunkid = mintUserunkid


            If objShortFilter._DtFilter IsNot Nothing Then
                If objShortFilter._DtFilter.Rows.Count > 0 Then
                    With objShortFilter
                        ._FormName = mstrFormName
                        ._LoginEmployeeunkid = mintLoginEmployeeunkid
                        ._ClientIP = mstrClientIP
                        ._HostName = mstrHostName
                        ._FromWeb = mblnIsWeb
                        ._AuditUserId = mintAuditUserId
._CompanyUnkid = mintCompanyUnkid
                        ._AuditDate = mdtAuditDate
                    End With
                    If objShortFilter.InsertDelete_Filter(objDataOperation, mdtApplicant) = False Then
                        exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                        Throw exForce
                    End If

                Else

                    'S.SANDEEP [28-May-2018] -- START
                    'ISSUE/ENHANCEMENT : {Audit Trails} 
                    Dim objCommonATLog As New clsCommonATLog
                    objCommonATLog._FormName = mstrFormName
                    objCommonATLog._LoginEmployeeUnkid = mintLoginEmployeeunkid
                    objCommonATLog._ClientIP = mstrClientIP
                    objCommonATLog._HostName = mstrHostName
                    objCommonATLog._FromWeb = mblnIsWeb
                    objCommonATLog._AuditUserId = mintAuditUserId
objCommonATLog._CompanyUnkid = mintCompanyUnkid
                    objCommonATLog._AuditDate = mdtAuditDate
                    'S.SANDEEP [28-May-2018] -- END

                    If objCommonATLog.Insert_TranAtLog(objDataOperation, "rcshortlist_master", "shortlistunkid", mintShortlistunkid, "rcshortlist_filter", "filterunkid", -1, 1, 0) = False Then
                        exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                        Throw exForce
                    End If

                    'S.SANDEEP [28-May-2018] -- START
                    'ISSUE/ENHANCEMENT : {Audit Trails} 
                    objCommonATLog = Nothing
                    'S.SANDEEP [28-May-2018] -- END

                End If

            End If

            objDataOperation.ReleaseTransaction(True)

            Return True
        Catch ex As Exception
            objDataOperation.ReleaseTransaction(False)
            Throw New Exception(ex.Message & "; Procedure Name: Insert; Module Name: " & mstrModuleName)
            Return False
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
    End Function

    ''' <summary>
    ''' Modify By: Pinkal Jariwala
    ''' </summary>
    ''' <returns>Boolean</returns>
    ''' <purpose> Update Database Table (rcshortlist_master) </purpose>
    Public Function Update() As Boolean
        'If isExist(mstrName, mintShortlistunkid) Then
        '    mstrMessage = "<Message>"
        '    Return False
        'End If

        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception


        'S.SANDEEP [ 14 May 2013 ] -- START
        'ENHANCEMENT : TRA ENHANCEMENT
        objDataOperation = New clsDataOperation
        objDataOperation.BindTransaction()
        'S.SANDEEP [ 14 May 2013 ] -- END


        Try
            objDataOperation.AddParameter("@shortlistunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintShortlistunkid.ToString)
            objDataOperation.AddParameter("@vacancyunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintVacancyunkid.ToString)
            objDataOperation.AddParameter("@refno", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrRefno.ToString)
            objDataOperation.AddParameter("@isinvalidempcode", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsInvalidEmpCode.ToString)
            objDataOperation.AddParameter("@userunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintUserunkid.ToString)
            objDataOperation.AddParameter("@isvoid", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsvoid.ToString)
            objDataOperation.AddParameter("@voiduserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintVoiduserunkid.ToString)

            'S.SANDEEP [ 14 May 2013 ] -- START
            'ENHANCEMENT : TRA ENHANCEMENT
            'objDataOperation.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, mdtVoiddatetime)
            objDataOperation.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, IIf(mdtVoiddatetime <> Nothing, mdtVoiddatetime, DBNull.Value))
            'S.SANDEEP [ 14 May 2013 ] -- END
            objDataOperation.AddParameter("@voidreason", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrVoidreason.ToString)

            'S.SANDEEP [ 14 May 2013 ] -- START
            'ENHANCEMENT : TRA ENHANCEMENT
            objDataOperation.AddParameter("@statustypid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintStatustypid.ToString)
            objDataOperation.AddParameter("@remark", SqlDbType.VarChar, mstrRemark.Length, mstrRemark.ToString)
            objDataOperation.AddParameter("@approveuserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintApproveuserunkid.ToString)
            objDataOperation.AddParameter("@issent", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIssent.ToString)
            If mdtAppr_Date <> Nothing Then
                objDataOperation.AddParameter("@appr_date", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, mdtAppr_Date)
            Else
                objDataOperation.AddParameter("@appr_date", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, DBNull.Value)
            End If
            'S.SANDEEP [ 14 May 2013 ] -- END

            strQ = "UPDATE rcshortlist_master SET " & _
              "  vacancyunkid = @vacancyunkid" & _
              ", refno = @refno" & _
              ", isinvalidempcode = @isinvalidempcode " & _
              ", userunkid = @userunkid" & _
              ", isvoid = @isvoid" & _
              ", voiduserunkid = @voiduserunkid" & _
              ", voiddatetime = @voiddatetime" & _
              ", voidreason = @voidreason " & _
                   ", statustypid = @statustypid" & _
                   ", remark = @remark" & _
                   ", approveuserunkid = @approveuserunkid " & _
                   ", issent = @issent " & _
                   ", appr_date = @appr_date " & _
            "WHERE shortlistunkid = @shortlistunkid "
            'S.SANDEEP [ 14 May 2013 (statustypid,remark,approveuserunkid,appr_date) ] -- START -- END

            Call objDataOperation.ExecNonQuery(strQ)

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            'S.SANDEEP [28-May-2018] -- START
            'ISSUE/ENHANCEMENT : {Audit Trails} 
            Dim objCommonATLog As New clsCommonATLog
            objCommonATLog._FormName = mstrFormName
            objCommonATLog._LoginEmployeeUnkid = mintLoginEmployeeunkid
            objCommonATLog._ClientIP = mstrClientIP
            objCommonATLog._HostName = mstrHostName
            objCommonATLog._FromWeb = mblnIsWeb
            objCommonATLog._AuditUserId = mintAuditUserId
objCommonATLog._CompanyUnkid = mintCompanyUnkid
            objCommonATLog._AuditDate = mdtAuditDate
            'S.SANDEEP [28-May-2018] -- END

            If objCommonATLog.Insert_TranAtLog(objDataOperation, "rcshortlist_master", "shortlistunkid", mintShortlistunkid, "rcshortlist_filter", "filterunkid", -1, 2, 0) = False Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            'S.SANDEEP [28-May-2018] -- START
            'ISSUE/ENHANCEMENT : {Audit Trails} 
            objCommonATLog = Nothing
            'S.SANDEEP [28-May-2018] -- END


            objDataOperation.ReleaseTransaction(True)

            Return True
        Catch ex As Exception
            objDataOperation.ReleaseTransaction(False) 'S.SANDEEP [ 14 May 2013 ] -- START
            Throw New Exception(ex.Message & "; Procedure Name: Update; Module Name: " & mstrModuleName)
            Return False
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
    End Function

    ''' <summary>
    ''' Modify By: Pinkal Jariwala
    ''' </summary>
    ''' <returns>Boolean</returns>
    ''' <purpose> Delete Database Table (rcshortlist_master) </purpose>
    Public Function Delete(ByVal intUnkid As Integer) As Boolean
        'If isUsed(intUnkid) Then
        '    mstrMessage = "<Message>"
        '    Return False
        'End If

        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        objDataOperation = New clsDataOperation
        objDataOperation.BindTransaction()

        Try


            strQ = "UPDATE rcshortlist_master SET isvoid = 1,voiduserunkid=@voiduserunkid,voiddatetime=@voiddatetime,voidreason = @voidreason " & _
                  "WHERE shortlistunkid = @shortlistunkid "

            objDataOperation.ClearParameters()
            objDataOperation.AddParameter("@voiduserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintVoiduserunkid)
            objDataOperation.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, mdtVoiddatetime)
            objDataOperation.AddParameter("@voidreason", SqlDbType.NVarChar, eZeeDataType.DESC_SIZE, mstrVoidreason)
            objDataOperation.AddParameter("@shortlistunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intUnkid)

            Call objDataOperation.ExecNonQuery(strQ)

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            objShortFilter._Shortlistunkid = intUnkid
            objShortFilter._Voiduserunkid = mintVoiduserunkid
            objShortFilter._Voiddatetime = mdtVoiddatetime
            objShortFilter._Voidreason = mstrVoidreason
            With objShortFilter
                ._FormName = mstrFormName
                ._LoginEmployeeunkid = mintLoginEmployeeunkid
                ._ClientIP = mstrClientIP
                ._HostName = mstrHostName
                ._FromWeb = mblnIsWeb
                ._AuditUserId = mintAuditUserId
._CompanyUnkid = mintCompanyUnkid
                ._AuditDate = mdtAuditDate
            End With
            If objShortFilter.Delete_ShortListedFilterApplicant(objDataOperation, 3) = False Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If


            Dim objFinalApplicant As New clsshortlist_finalapplicant
            objFinalApplicant._ShortListUnkid = intUnkid
            objFinalApplicant._Voiduserunkid = mintVoiduserunkid
            objFinalApplicant._Voiddatetime = mdtVoiddatetime
            objFinalApplicant._Voidreason = mstrVoidreason
            With objFinalApplicant
                ._FormName = mstrFormName
                ._LoginEmployeeunkid = mintLoginEmployeeunkid
                ._ClientIP = mstrClientIP
                ._HostName = mstrHostName
                ._FromWeb = mblnIsWeb
                ._AuditUserId = mintAuditUserId
._CompanyUnkid = mintCompanyUnkid
                ._AuditDate = mdtAuditDate
            End With
            If objFinalApplicant.Delete_ShortListedApplicant(objDataOperation, 3) = False Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            objDataOperation.ReleaseTransaction(True)
            Return True
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: Update; Module Name: " & mstrModuleName)
            Return False
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
    End Function

    ''' <summary>
    ''' Modify By: Pinkal Jariwala
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>
    Public Function isUsed(ByVal intUnkid As Integer) As Boolean
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        objDataOperation = New clsDataOperation

        Try
            StrQ = "<Query>"

            objDataOperation.AddParameter("@shortlistunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intUnkid)

            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            Return dsList.tables(0).rows.count > 0
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: isUsed; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
    End Function

    ''' <summary>
    ''' Modify By: Pinkal Jariwala
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>
    Public Function isExist(ByVal strCode As String, ByVal strName As String, Optional ByVal intUnkid As Integer = -1) As Boolean
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        objDataOperation = New clsDataOperation

        Try
            strQ = "SELECT " & _
              "  shortlistunkid " & _
              ", vacancyunkid " & _
              ", refno " & _
              ", isinvalidempcode " & _
              ", userunkid " & _
              ", isvoid " & _
              ", voiduserunkid " & _
              ", voiddatetime " & _
              ", voidreason " & _
             "FROM rcshortlist_master " & _
             "WHERE name = @name " & _
             "AND code = @code "

            If intUnkid > 0 Then
                strQ &= " AND shortlistunkid <> @shortlistunkid"
            End If

            objDataOperation.AddParameter("@code", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, strCode)
            objDataOperation.AddParameter("@name", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, strName)
            objDataOperation.AddParameter("@shortlistunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intUnkid)

            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            Return dsList.tables(0).rows.count > 0
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: isExist; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
    End Function

    Public Function getComboList(ByVal strList As String, Optional ByVal blnFlag As Boolean = False, Optional ByVal blnApproved As Boolean = False) As DataSet 'Anjan [ 29 May 2013 ] -- Start (blnApproved)
        Dim StrQ As String = String.Empty
        Dim dsList As New DataSet
        Dim exForce As Exception
        Try
            objDataOperation = New clsDataOperation
            If blnFlag = True Then
                StrQ = "SELECT 0 AS shortlistunkid, 0 AS vacancyunkid, @Select refno, '' AS Vacancy, '' AS StDate, '' as EndDate UNION "
            End If

            'S.SANDEEP [ 28 FEB 2012 ] -- START
            'ENHANCEMENT : TRA CHANGES
            'StrQ &= "SELECT " & _
            '            " shortlistunkid " & _
            '            ",rcvacancy_master.vacancyunkid " & _
            '            ",refno " & _
            '            ",vacancytitle " & _
            '            ",ISNULL(CONVERT(CHAR(8),openingdate,112),'') AS openingdate " & _
            '            ",ISNULL(CONVERT(CHAR(8),closingdate,112),'') AS closingdate " & _
            '        "FROM rcshortlist_master " & _
            '           "JOIN rcvacancy_master ON rcshortlist_master.vacancyunkid = rcvacancy_master.vacancyunkid " & _
            '        "WHERE rcshortlist_master.isvoid = 0 AND rcvacancy_master.isvoid = 0 "
            StrQ &= "SELECT " & _
                        " shortlistunkid " & _
                        ",rcvacancy_master.vacancyunkid " & _
                        ",refno " & _
                        ",cfcommon_master.name AS vacancytitle " & _
                        ",ISNULL(CONVERT(CHAR(8),openingdate,112),'') AS openingdate " & _
                        ",ISNULL(CONVERT(CHAR(8),closingdate,112),'') AS closingdate " & _
                    "FROM rcshortlist_master " & _
                        "JOIN rcvacancy_master ON rcshortlist_master.vacancyunkid = rcvacancy_master.vacancyunkid " & _
                       "JOIN cfcommon_master ON cfcommon_master.masterunkid = rcvacancy_master.vacancytitle AND cfcommon_master.mastertype = '" & clsCommon_Master.enCommonMaster.VACANCY_MASTER & "' " & _
                    "WHERE rcshortlist_master.isvoid = 0 AND rcvacancy_master.isvoid = 0 "
            'S.SANDEEP [ 28 FEB 2012 ] -- END



            'Anjan [ 29 May 2013 ] -- Start
            'ENHANCEMENT : Recruitment TRA changes requested by Andrew
            If blnApproved = True Then
                StrQ &= " AND rcshortlist_master.statustypid = " & enShortListing_Status.SC_APPROVED
            End If
            'Anjan [ 29 May 2013 ] -- End



            objDataOperation.AddParameter("@Select", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 1, "Select"))

            If strList.Trim.Length <= 0 Then strList = "List"

            dsList = objDataOperation.ExecQuery(StrQ, strList)

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & " : " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            Return dsList

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: getComboList; Module Name: " & mstrModuleName)
            Return Nothing
        Finally
            dsList.Dispose()
        End Try
    End Function


    'Pinkal (16-Oct-2023) -- Start
    '(A1X-1400) NMB - Post assigned employee assets to P2P
    Public Function GetFileStrctureForSendFeedBackToApplicants(ByVal mblnShortListedFileStructure As Boolean) As DataSet
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim objDataOperation As New clsDataOperation
        Try

            If mblnShortListedFileStructure Then

                strQ = "SELECT " & _
                          " '' AS   [App.Code] " & _
                          ", '' AS  [RecipientName] " & _
                          ", '' AS  [Vacancy] " & _
                          ", '' AS  [Openingdate] " & _
                          ", '' AS  [Closingdate] " & _
                          ", '' AS  [Location] " & _
                          ", '' AS  [Date] " & _
                          ", '' AS  [Time] " & _
                          ", '' AS  [Interview Number] "
            Else
                strQ = "SELECT " & _
                         " '' AS   [App.Code] " & _
                         ", '' AS  [RecipientName] " & _
                         ", '' AS  [Vacancy] " & _
                         ", '' AS  [Openingdate] " & _
                         ", '' AS  [Closingdate] " & _
                         ", '' AS  [Remark]"
            End If

            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                Throw New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
            End If
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: GetFileStrctureForSendFeedBackToApplicants; Module Name: " & mstrModuleName)
        End Try
        Return dsList
    End Function
    'Pinkal (16-Oct-2023) -- End

	'<Language> This Auto Generated Text Please Do Not Modify it.
#Region " Language & UI Settings "
	Public Shared Sub SetMessages()
		Try
			Language.setMessage(mstrModuleName, 1, "Select")
			Language.setMessage(mstrModuleName, 2, "Approved")
			Language.setMessage(mstrModuleName, 3, "Pending")
			Language.setMessage(mstrModuleName, 4, "Disapprove")
			Language.setMessage("clsEmployee_Master", 5, "Male")
			Language.setMessage("clsEmployee_Master", 6, "Female")
			Language.setMessage("clsEmployee_Master", 7, "Other")

		Catch Ex As Exception
			DisplayError.Show("-1", ex.Message, "SetMessages", mstrModuleName)
		End Try
	End Sub
#End Region 'Language & UI Settings
	'</Language>
End Class
