﻿'************************************************************************************************************************************
'Class Name : clsJobhistory.vb
'Purpose    :
'Date       : 25/08/2010
'Written By : Anjan
'Modified   :
'************************************************************************************************************************************
Imports eZeeCommonLib
Imports Aruti.Data


Public Class clsJobhistory
    Private Const mstrModuleName = "clsJobhistory"
    Dim objDataOperation As clsDataOperation
    Dim mstrMessage As String = ""

#Region " Private Variables "
    Private mintApplicantunkid As Integer
    Private mintJobhistoryunkid As Integer
    Private mdtTran As DataTable
#End Region

#Region " Properties "

'S.SANDEEP [28-May-2018] -- START 
'ISSUE/ENHANCEMENT : {Audit Trails} 
Private mstrFormName As String = String.Empty
Public WriteOnly Property _FormName() As String 
Set(ByVal value As String) 
mstrFormName = value 
End Set 
End Property 
 
Private mstrClientIP As String = ""
Public WriteOnly Property _ClientIP() As String 
Set(ByVal value As String) 
mstrClientIP = value 
End Set 
End Property 
 
Private mstrHostName As String = ""
Public WriteOnly Property _HostName() As String 
Set(ByVal value As String) 
mstrHostName = value 
End Set 
End Property 
 
Private mblnIsWeb As Boolean = False
Public WriteOnly Property _FromWeb() As Boolean 
Set(ByVal value As Boolean) 
mblnIsWeb = value 
End Set 
End Property 
 
    Private mintAuditUserId As Integer = 0
    Public WriteOnly Property _AuditUserId() As Integer
        Set(ByVal value As Integer)
            mintAuditUserId = value
        End Set
    End Property

    Private mintCompanyUnkid As Integer = 0
    Public WriteOnly Property _CompanyUnkid() As Integer
        Set(ByVal value As Integer)
            mintCompanyUnkid = value
        End Set
    End Property
 
Private mdtAuditDate As DateTime = Now
Public WriteOnly Property _AuditDate() As DateTime 
Set(ByVal value As DateTime) 
mdtAuditDate = value 
End Set 
End Property 
 
Private xDataOpr As clsDataOperation = Nothing
Public Property _xDataOpr() As clsDataOperation 
Get 
Return xDataOpr 
End Get 
Set(ByVal value As clsDataOperation) 
xDataOpr = value 
End Set 
End Property 
 
Private mintLoginEmployeeunkid As Integer = 0
Public Property _LoginEmployeeunkid() As Integer 
Get 
Return mintLoginEmployeeunkid 
End Get 
Set(ByVal value As Integer) 
mintLoginEmployeeunkid = value 
End Set 
End Property 
 
Private mintVoidlogingemployeeunkid As Integer=0
Public Property _Voidloginemployeeunkid() As Integer 
Get 
Return mintVoidlogingemployeeunkid 
End Get 
Set(ByVal value As Integer) 
mintVoidlogingemployeeunkid = value 
End Set 
End Property 
'S.SANDEEP [28-May-2018] -- END 

    Public Property _Applicantunkid() As Integer
        Get
            Return mintApplicantunkid
        End Get
        Set(ByVal value As Integer)
            mintApplicantunkid = value
            Call GetApplicantJobHistory_tran()
        End Set
    End Property
    Public Property _DataList() As DataTable
        Get
            Return mdtTran
        End Get
        Set(ByVal value As DataTable)
            mdtTran = value
        End Set
    End Property

    'Sohail (02 May 2022) -- Start
    'Enhancement :  AC2-312 / AC2-328 : ZRA - Newly added job experiences should not populate on old job applications for closed vacancies on applicant master.
    Private mdtVacancyEndDate As Date
    Public WriteOnly Property _VacancyEndDate() As Date
        Set(ByVal value As Date)
            mdtVacancyEndDate = value
        End Set
    End Property
    'Sohail (02 May 2022) -- End

#End Region

#Region " Constructor "
    Public Sub New()
        mdtTran = New DataTable("applicantjobhistory")
        Dim dcol As New DataColumn

        dcol = New DataColumn("jobhistorytranunkid")
        dcol.DataType = System.Type.GetType("System.Int32")
        mdtTran.Columns.Add(dcol)

        dcol = New DataColumn("applicantunkid")
        dcol.DataType = System.Type.GetType("System.Int32")
        mdtTran.Columns.Add(dcol)

        dcol = New DataColumn("employername")
        dcol.DataType = System.Type.GetType("System.String")
        mdtTran.Columns.Add(dcol)

        dcol = New DataColumn("companyname")
        dcol.DataType = System.Type.GetType("System.String")
        mdtTran.Columns.Add(dcol)

        dcol = New DataColumn("designation")
        dcol.DataType = System.Type.GetType("System.String")
        mdtTran.Columns.Add(dcol)

        dcol = New DataColumn("responsibility")
        dcol.DataType = System.Type.GetType("System.String")
        mdtTran.Columns.Add(dcol)

        dcol = New DataColumn("joiningdate")
        dcol.DataType = System.Type.GetType("System.DateTime")
        mdtTran.Columns.Add(dcol)

        dcol = New DataColumn("terminationdate")
        dcol.DataType = System.Type.GetType("System.DateTime")
        mdtTran.Columns.Add(dcol)

        dcol = New DataColumn("officephone")
        dcol.DataType = System.Type.GetType("System.String")
        mdtTran.Columns.Add(dcol)

        dcol = New DataColumn("leavingreason")
        dcol.DataType = System.Type.GetType("System.String")
        mdtTran.Columns.Add(dcol)

        'Sohail (22 Apr 2015) -- Start
        'Enhancement - Providing qualification attachment, certificate no., achivement, email confirmation in online recruitmenmt.
        mdtTran.Columns.Add("achievements", Type.GetType("System.String")).DefaultValue = ""
        'Sohail (22 Apr 2015) -- End

        dcol = New DataColumn("AUD")
        dcol.DataType = System.Type.GetType("System.String")
        dcol.AllowDBNull = True
        dcol.DefaultValue = DBNull.Value
        mdtTran.Columns.Add(dcol)

        dcol = New DataColumn("GUID")
        dcol.DataType = System.Type.GetType("System.String")
        mdtTran.Columns.Add(dcol)

        'Sohail (26 May 2017) -- Start
        'Issue - 67.1 - applicants details gets removed if he changes after data is imported.
        mdtTran.Columns.Add("serverjobhistorytranunkid", System.Type.GetType("System.Int32")).DefaultValue = 0
        mdtTran.Columns.Add("isvoid", System.Type.GetType("System.Boolean")).DefaultValue = False
        mdtTran.Columns.Add("voiduserunkid", System.Type.GetType("System.Int32")).DefaultValue = 0
        mdtTran.Columns.Add("voiddatetime", System.Type.GetType("System.DateTime")).DefaultValue = Nothing
        mdtTran.Columns.Add("voidreason", System.Type.GetType("System.String")).DefaultValue = ""
        'Sohail (26 May 2017) -- End

        'Hemant (11 Apr 2022) -- Start            
        'ISSUE/ENHANCEMENT(ZRA) : the updated information gets updated across all the vacancies including the closed ones. Only open vacancies should be updated with the new information
        dcol = New DataColumn("created_date")
        dcol.DataType = System.Type.GetType("System.DateTime")
        mdtTran.Columns.Add(dcol)
        'Hemant (11 Apr 2022) -- End

        'S.SANDEEP |04-MAY-2023| -- START
        'ISSUE/ENHANCEMENT : A1X-833
        dcol = New DataColumn("isgovjob")
        dcol.DataType = GetType(System.Boolean)
        dcol.DefaultValue = False
        mdtTran.Columns.Add(dcol)
        'S.SANDEEP |04-MAY-2023| -- END
    End Sub
#End Region

#Region " Private Methods "
    Private Sub GetApplicantJobHistory_tran()
        Dim strQ As String = ""
        Dim dsList As DataSet = Nothing
        Dim strErrorMessage As String = ""
        Dim dRowID_Tran As DataRow
        Dim exForce As Exception

        objDataOperation = New clsDataOperation
        Try
            strQ = "SELECT " & _
                        "  jobhistorytranunkid " & _
                        ", applicantunkid " & _
                        ", employername " & _
                        ", companyname " & _
                        ", designation " & _
                        ", responsibility " & _
                        ", joiningdate " & _
                        ", terminationdate " & _
                        ", officephone " & _
                        ", leavingreason " & _
                        ", ISNULL(achievements, '') AS achievements " & _
                        ", ''AUD " & _
                        ", ISNULL(serverjobhistorytranunkid, 0) AS serverjobhistorytranunkid " & _
                        ", isvoid " & _
                        ", voiduserunkid " & _
                        ", voiddatetime " & _
                        ", voidreason " & _
                        ", created_date " & _
                        ", ISNULL(isgovjob,0) AS isgovjob " & _
                    "FROM rcjobhistory " & _
                    "WHERE ISNULL(isvoid, 0) = 0 AND applicantunkid = @applicantunkid "
            'S.SANDEEP |04-MAY-2023| -- START {A1X-833} [isgovjob] -- END
            'Hemant (11 Apr 2022) -- [created_date]
            'Sohail (26 May 2017) - [serverjobhistorytranunkid, isvoid, voiduserunkid, voiddatetime, voidreason]

            'Sohail (02 May 2022) -- Start
            'Enhancement :  AC2-312 / AC2-328 : ZRA - Newly added job experiences should not populate on old job applications for closed vacancies on applicant master.
            If mdtVacancyEndDate <> Nothing Then
                strQ &= " AND ISNULL(CONVERT(CHAR(8), rcjobhistory.created_date ,112), '19000101') <= '" & eZeeDate.convertDate(mdtVacancyEndDate) & "' "
            End If
            'Sohail (02 May 2022) -- End

            objDataOperation.AddParameter("@applicantunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintApplicantunkid.ToString)

            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            'S.SANDEEP [ 28 FEB 2012 ] -- START
            'ENHANCEMENT : TRA CHANGES
            mdtTran.Rows.Clear()
            'S.SANDEEP [ 28 FEB 2012 ] -- END

            For i As Integer = 0 To dsList.Tables(0).Rows.Count - 1
                With dsList.Tables(0).Rows(i)
                    dRowID_Tran = mdtTran.NewRow
                    dRowID_Tran.Item("jobhistorytranunkid") = .Item("jobhistorytranunkid")
                    dRowID_Tran.Item("applicantunkid") = .Item("applicantunkid")
                    dRowID_Tran.Item("employername") = .Item("employername")
                    dRowID_Tran.Item("companyname") = .Item("companyname")
                    dRowID_Tran.Item("designation") = .Item("designation")
                    dRowID_Tran.Item("responsibility") = .Item("responsibility")
                    'Sohail (11 Sep 2010) -- Start
                    'Issue : Operator '=' is not defined for type 'DBNull' and 'Nothing'.
                    'If .Item("joiningdate") = Nothing Then
                    If .Item("joiningdate").ToString = Nothing Then
                        'Sohail (11 Sep 2010) -- End
                        dRowID_Tran.Item("joiningdate") = DBNull.Value
                    Else
                        dRowID_Tran.Item("joiningdate") = .Item("joiningdate").ToString
                    End If

                    'Sohail (11 Sep 2010) -- Start
                    'Issue : Operator '=' is not defined for type 'DBNull' and 'Nothing'.
                    'If .Item("terminationdate") = Nothing Then
                    If .Item("terminationdate").ToString = Nothing Then
                        dRowID_Tran.Item("terminationdate") = DBNull.Value
                    Else
                        dRowID_Tran.Item("terminationdate") = .Item("terminationdate").ToString
                    End If

                    dRowID_Tran.Item("officephone") = .Item("officephone")
                    dRowID_Tran.Item("leavingreason") = .Item("leavingreason")
                    dRowID_Tran.Item("achievements") = .Item("achievements") 'Sohail (22 Apr 2015)
                    dRowID_Tran.Item("AUD") = .Item("AUD").ToString
                    'Sohail (26 May 2017) -- Start
                    'Issue - 67.1 - applicants details gets removed if he changes after data is imported.
                    dRowID_Tran.Item("serverjobhistorytranunkid") = .Item("serverjobhistorytranunkid")
                    dRowID_Tran.Item("isvoid") = .Item("isvoid")
                    dRowID_Tran.Item("voiduserunkid") = .Item("voiduserunkid")
                    If IsDBNull(.Item("voiddatetime")) = True Then
                        dRowID_Tran.Item("voiddatetime") = DBNull.Value
                    Else
                        dRowID_Tran.Item("voiddatetime") = .Item("voiddatetime")
                    End If
                    dRowID_Tran.Item("voidreason") = .Item("voidreason")
                    'Sohail (26 May 2017) -- End
                    'Hemant (11 Apr 2022) -- Start            
                    'ISSUE/ENHANCEMENT(ZRA) : the updated information gets updated across all the vacancies including the closed ones. Only open vacancies should be updated with the new information
                    dRowID_Tran.Item("created_date") = .Item("created_date")
                    'Hemant (11 Apr 2022) -- End

                    dRowID_Tran.Item("isgovjob") = .Item("isgovjob") 'S.SANDEEP |04-MAY-2023| -- START {A1X-833} [isgovjob] -- END

                    mdtTran.Rows.Add(dRowID_Tran)
                End With
            Next
        Catch ex As Exception
            'DisplayError.Show("-1", ex.Message, "GetApplicantJobHistory_tran", mstrModuleName)
            Throw New Exception(ex.Message & "; Procedure Name: GetApplicantJobHistory_tran; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
    End Sub

    Public Function InsertUpdateDelete_JobHistoryTran() As Boolean
        Dim i As Integer
        Dim strQ As String = ""
        Dim strErrorMessage As String = ""
        Dim exForce As Exception

        Try
            objDataOperation = New clsDataOperation
            objDataOperation.BindTransaction()

            For i = 0 To mdtTran.Rows.Count - 1
                With mdtTran.Rows(i)
                    objDataOperation.ClearParameters()

                    If Not IsDBNull(.Item("AUD")) Then
                        Select Case .Item("AUD")
                            Case "A"
                               
                                strQ = "INSERT INTO rcjobhistory ( " & _
                                  "  applicantunkid " & _
                                  ", employername " & _
                                  ", companyname " & _
                                  ", designation " & _
                                  ", responsibility " & _
                                  ", joiningdate " & _
                                  ", terminationdate " & _
                                  ", officephone " & _
                                  ", leavingreason" & _
                                  ", achievements" & _
                                  ", serverjobhistorytranunkid" & _
                                  ", isvoid" & _
                                  ", voiduserunkid" & _
                                  ", voiddatetime" & _
                                  ", voidreason" & _
                                  ", created_date " & _
                                  ", isgovjob " & _
                                ") VALUES (" & _
                                  "  @applicantunkid " & _
                                  ", @employername " & _
                                  ", @companyname " & _
                                  ", @designation " & _
                                  ", @responsibility " & _
                                  ", @joiningdate " & _
                                  ", @terminationdate " & _
                                  ", @officephone " & _
                                  ", @leavingreason" & _
                                  ", @achievements" & _
                                  ", @serverjobhistorytranunkid" & _
                                  ", @isvoid" & _
                                  ", @voiduserunkid" & _
                                  ", @voiddatetime" & _
                                  ", @voidreason" & _
                                  ", @created_date " & _
                                  ", @isgovjob " & _
                                "); SELECT @@identity"
                                'S.SANDEEP |04-MAY-2023| -- START {A1X-833} [isgovjob] -- END
                                'Hemant (11 Apr 2022) -- [created_date]
                                'Sohail (26 May 2017) - [serverjobhistorytranunkid, isvoid, voiduserunkid, voiddatetime, voidreason]
                                'Sohail (22 Apr 2015) - [achievements]

                                objDataOperation.AddParameter("@applicantunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintApplicantunkid.ToString)
                                objDataOperation.AddParameter("@employername", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, .Item("employername").ToString)
                                objDataOperation.AddParameter("@companyname", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, .Item("companyname").ToString)
                                objDataOperation.AddParameter("@designation", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, .Item("designation").ToString)
                                'Sohail (12 Oct 2018) -- Start
                                'Issue : Data truncated due to NAME_SIZE datatype in 75.1.
                                'objDataOperation.AddParameter("@responsibility", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, .Item("responsibility").ToString)
                                'Sohail (01 Sep 2022) -- Start
                                'Issue :  : Data truncated due to DESC_SIZE datatype.
                                'objDataOperation.AddParameter("@responsibility", SqlDbType.NVarChar, eZeeDataType.DESC_SIZE, .Item("responsibility").ToString)
                                objDataOperation.AddParameter("@responsibility", SqlDbType.NVarChar, .Item("responsibility").ToString.Length, .Item("responsibility").ToString)
                                'Sohail (01 Sep 2022) -- End
                                'Sohail (12 Oct 2018) -- End
                                If .Item("joiningdate").ToString = Nothing Then
                                    objDataOperation.AddParameter("@joiningdate", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, DBNull.Value)
                                Else
                                    objDataOperation.AddParameter("@joiningdate", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, .Item("joiningdate").ToString)
                                End If
                                If .Item("terminationdate").ToString = Nothing Then
                                    objDataOperation.AddParameter("@terminationdate", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, DBNull.Value)
                                Else
                                    objDataOperation.AddParameter("@terminationdate", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, .Item("terminationdate").ToString)
                                End If

                                objDataOperation.AddParameter("@officephone", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, .Item("officephone").ToString)
                                'Sohail (12 Oct 2018) -- Start
                                'Issue : Data truncated due to NAME_SIZE datatype in 75.1.
                                'objDataOperation.AddParameter("@leavingreason", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, .Item("leavingreason").ToString)
                                'objDataOperation.AddParameter("@achievements", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, .Item("achievements").ToString) 'Sohail (22 Apr 2015)
                                objDataOperation.AddParameter("@leavingreason", SqlDbType.NVarChar, eZeeDataType.DESC_SIZE, .Item("leavingreason").ToString)
                                objDataOperation.AddParameter("@achievements", SqlDbType.NVarChar, eZeeDataType.DESC_SIZE, .Item("achievements").ToString)
                                'Sohail (12 Oct 2018) -- End
                                'Sohail (26 May 2017) -- Start
                                'Issue - 67.1 - applicants details gets removed if he changes after data is imported.
                                objDataOperation.AddParameter("@serverjobhistorytranunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, .Item("serverjobhistorytranunkid"))
                                objDataOperation.AddParameter("@isvoid", SqlDbType.Bit, eZeeDataType.BIT_SIZE, .Item("isvoid"))
                                objDataOperation.AddParameter("@voiduserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, .Item("voiduserunkid"))
                                If IsDBNull(.Item("voiddatetime")) = True Then
                                    objDataOperation.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, DBNull.Value)
                                Else
                                    objDataOperation.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, .Item("voiddatetime"))
                                End If
                                objDataOperation.AddParameter("@voidreason", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, .Item("voidreason").ToString)
                                'Sohail (26 May 2017) -- End

                                'Hemant (11 Apr 2022) -- Start            
                                'ISSUE/ENHANCEMENT(ZRA) : the updated information gets updated across all the vacancies including the closed ones. Only open vacancies should be updated with the new information
                                objDataOperation.AddParameter("@created_date", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, .Item("created_date").ToString)
                                'Hemant (11 Apr 2022) -- End

                                'S.SANDEEP |04-MAY-2023| -- START
                                'ISSUE/ENHANCEMENT : A1X-833
                                objDataOperation.AddParameter("@isgovjob", SqlDbType.Bit, eZeeDataType.BIT_SIZE, .Item("isgovjob").ToString)
                                'S.SANDEEP |04-MAY-2023| -- END

                                'Anjan (12 Oct 2011)-Start
                                'ENHANCEMENT : AUDIT TRAIL MAINTENANCE.
                                'objDataOperation.ExecNonQuery(strQ)
                                Dim dsList As DataSet = objDataOperation.ExecQuery(strQ, "List")
                                'Anjan (12 Oct 2011)-End 




                                If objDataOperation.ErrorMessage <> "" Then
                                    exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                                    Throw exForce
                                End If


                                'Anjan (12 Oct 2011)-Start
                                'ENHANCEMENT : AUDIT TRAIL MAINTENANCE.
                                mintJobhistoryunkid = dsList.Tables(0).Rows(0)(0)

                                If .Item("applicantunkid") > 0 Then
'S.SANDEEP [28-May-2018] -- START
'ISSUE/ENHANCEMENT : {Audit Trails} 
Dim objCommonATLog AS New clsCommonATLog 
objCommonATLog._FormName = mstrFormName 
objCommonATLog._LoginEmployeeUnkid = mintLoginEmployeeunkid 
                                    objCommonATLog._ClientIP = mstrClientIP
                                    objCommonATLog._HostName = mstrHostName
                                    objCommonATLog._FromWeb = mblnIsWeb
                                    objCommonATLog._AuditUserId = mintAuditUserId
objCommonATLog._CompanyUnkid = mintCompanyUnkid
                                    objCommonATLog._AuditDate = mdtAuditDate
                                    'S.SANDEEP [28-May-2018] -- END
                                    'Sohail (09 Oct 2018) -- Start
                                    'TANAPA - Support Issue Id # 2502 - linking recruitment server app_vacancy with aruti desktop app_vacancy in 75.1.
                                    'If clsCommonATLog.Insert_TranAtLog(objDataOperation, "rcapplicant_master", "applicantunkid", .Item("applicantunkid"), "rcjobhistory", "jobhistorytranunkid", mintJobhistoryunkid, 2, 1) = False Then
                                    If objCommonATLog.Insert_TranAtLog(objDataOperation, "rcapplicant_master", "applicantunkid", mintApplicantunkid, "rcjobhistory", "jobhistorytranunkid", mintJobhistoryunkid, 2, 1) = False Then
                                        'Sohail (09 Oct 2018) -- End
                                        exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                                        Throw exForce
                                    End If

                                    'S.SANDEEP [28-May-2018] -- START
                                    'ISSUE/ENHANCEMENT : {Audit Trails} 
                                    objCommonATLog = Nothing
                                    'S.SANDEEP [28-May-2018] -- END
                                Else
                                   'S.SANDEEP [28-May-2018] -- START
                                    'ISSUE/ENHANCEMENT : {Audit Trails} 
                                    Dim objCommonATLog As New clsCommonATLog
                                    objCommonATLog._FormName = mstrFormName
                                    objCommonATLog._LoginEmployeeUnkid = mintLoginEmployeeunkid
                                    objCommonATLog._ClientIP = mstrClientIP
                                    objCommonATLog._HostName = mstrHostName
                                    objCommonATLog._FromWeb = mblnIsWeb
                                    objCommonATLog._AuditUserId = mintAuditUserId
objCommonATLog._CompanyUnkid = mintCompanyUnkid
                                    objCommonATLog._AuditDate = mdtAuditDate
                                    'S.SANDEEP [28-May-2018] -- END
                                    If objCommonATLog.Insert_TranAtLog(objDataOperation, "rcapplicant_master", "applicantunkid", mintApplicantunkid, "rcjobhistory", "jobhistorytranunkid", mintJobhistoryunkid, 1, 1) = False Then
                                        exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                                        Throw exForce
                                    End If

                                    'S.SANDEEP [28-May-2018] -- START
                                    'ISSUE/ENHANCEMENT : {Audit Trails} 
                                    objCommonATLog = Nothing
                                    'S.SANDEEP [28-May-2018] -- END

                                End If
                                'Anjan (12 Oct 2011)-End 


                            Case "U"
                                objDataOperation.AddParameter("@jobhistorytranunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, .Item("jobhistorytranunkid").ToString)
                                objDataOperation.AddParameter("@applicantunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintApplicantunkid.ToString)
                                objDataOperation.AddParameter("@employername", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, .Item("employername").ToString)
                                objDataOperation.AddParameter("@companyname", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, .Item("companyname").ToString)
                                objDataOperation.AddParameter("@designation", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, .Item("designation").ToString)
                                'Sohail (12 Oct 2018) -- Start
                                'Issue : Data truncated due to NAME_SIZE datatype in 75.1.
                                'objDataOperation.AddParameter("@responsibility", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, .Item("responsibility").ToString)
                                'Sohail (01 Sep 2022) -- Start
                                'Issue :  : Data truncated due to DESC_SIZE datatype.
                                'objDataOperation.AddParameter("@responsibility", SqlDbType.NVarChar, eZeeDataType.DESC_SIZE, .Item("responsibility").ToString)
                                objDataOperation.AddParameter("@responsibility", SqlDbType.NVarChar, .Item("responsibility").ToString.Length, .Item("responsibility").ToString)
                                'Sohail (01 Sep 2022) -- End
                                'Sohail (12 Oct 2018) -- End

                                If .Item("joiningdate").ToString = Nothing Then
                                    objDataOperation.AddParameter("@joiningdate", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, DBNull.Value)
                                Else
                                    objDataOperation.AddParameter("@joiningdate", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, .Item("joiningdate").ToString)
                                End If
                                If .Item("terminationdate").ToString = Nothing Then
                                    objDataOperation.AddParameter("@terminationdate", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, DBNull.Value)
                                Else
                                    objDataOperation.AddParameter("@terminationdate", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, .Item("terminationdate").ToString)
                                End If

                                objDataOperation.AddParameter("@officephone", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, .Item("officephone").ToString)
                                'Sohail (12 Oct 2018) -- Start
                                'Issue : Data truncated due to NAME_SIZE datatype in 75.1.
                                'objDataOperation.AddParameter("@leavingreason", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, .Item("leavingreason").ToString)
                                'objDataOperation.AddParameter("@achievements", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, .Item("achievements").ToString) 'Sohail (22 Apr 2015)
                                objDataOperation.AddParameter("@leavingreason", SqlDbType.NVarChar, eZeeDataType.DESC_SIZE, .Item("leavingreason").ToString)
                                objDataOperation.AddParameter("@achievements", SqlDbType.NVarChar, eZeeDataType.DESC_SIZE, .Item("achievements").ToString)
                                'Sohail (12 Oct 2018) -- End
                                'Sohail (26 May 2017) -- Start
                                'Issue - 67.1 - applicants details gets removed if he changes after data is imported.
                                objDataOperation.AddParameter("@serverjobhistorytranunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, .Item("serverjobhistorytranunkid"))
                                objDataOperation.AddParameter("@isvoid", SqlDbType.Bit, eZeeDataType.BIT_SIZE, .Item("isvoid"))
                                objDataOperation.AddParameter("@voiduserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, .Item("voiduserunkid"))
                                If IsDBNull(.Item("voiddatetime")) = True Then
                                    objDataOperation.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, DBNull.Value)
                                Else
                                    objDataOperation.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, .Item("voiddatetime"))
                                End If
                                objDataOperation.AddParameter("@voidreason", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, .Item("voidreason").ToString)
                                'Sohail (26 May 2017) -- End

                                'S.SANDEEP |04-MAY-2023| -- START
                                'ISSUE/ENHANCEMENT : A1X-833
                                objDataOperation.AddParameter("@isgovjob", SqlDbType.Bit, eZeeDataType.BIT_SIZE, .Item("isgovjob").ToString)
                                'S.SANDEEP |04-MAY-2023| -- END

                                strQ = "UPDATE rcjobhistory SET " & _
                                              "  applicantunkid = @applicantunkid" & _
                                              ", employername = @employername" & _
                                              ", companyname = @companyname" & _
                                              ", designation = @designation" & _
                                              ", responsibility = @responsibility" & _
                                              ", joiningdate = @joiningdate" & _
                                              ", terminationdate = @terminationdate" & _
                                              ", officephone = @officephone" & _
                                              ", leavingreason = @leavingreason " & _
                                              ", achievements = @achievements " & _
                                              ", serverjobhistorytranunkid = @serverjobhistorytranunkid" & _
                                              ", isvoid = @isvoid" & _
                                              ", voiduserunkid = @voiduserunkid" & _
                                              ", voiddatetime = @voiddatetime" & _
                                              ", voidreason = @voidreason " & _
                                              ", isgovjob = @isgovjob " & _
                                        "WHERE jobhistorytranunkid = @jobhistorytranunkid "
                                'S.SANDEEP |04-MAY-2023| -- START {A1X-833} [isgovjob] -- END
                                'Sohail (26 May 2017) - [serverjobhistorytranunkid, isvoid, voiduserunkid, voiddatetime, voidreason]
                                'Sohail (22 Apr 2015) - [achievements]

                                Call objDataOperation.ExecNonQuery(strQ)

                                If objDataOperation.ErrorMessage <> "" Then
                                    exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                                    Throw exForce
                                End If


                                'S.SANDEEP [28-May-2018] -- START
                                'ISSUE/ENHANCEMENT : {Audit Trails} 
                                Dim objCommonATLog As New clsCommonATLog
                                objCommonATLog._FormName = mstrFormName
                                objCommonATLog._LoginEmployeeUnkid = mintLoginEmployeeunkid
                                objCommonATLog._ClientIP = mstrClientIP
                                objCommonATLog._HostName = mstrHostName
                                objCommonATLog._FromWeb = mblnIsWeb
                                objCommonATLog._AuditUserId = mintAuditUserId
objCommonATLog._CompanyUnkid = mintCompanyUnkid
                                objCommonATLog._AuditDate = mdtAuditDate
                                'S.SANDEEP [28-May-2018] -- END

                                If objCommonATLog.Insert_TranAtLog(objDataOperation, "rcapplicant_master", "applicantunkid", .Item("applicantunkid"), "rcjobhistory", "jobhistorytranunkid", .Item("jobhistorytranunkid"), 2, 2) = False Then
                                    exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                                    Throw exForce
                                End If

                                'S.SANDEEP [28-May-2018] -- START
                                'ISSUE/ENHANCEMENT : {Audit Trails} 
                                objCommonATLog = Nothing
                                'S.SANDEEP [28-May-2018] -- END

                            Case "D"


                                If .Item("jobhistorytranunkid") > 0 Then
                                    'S.SANDEEP [28-May-2018] -- START
                                    'ISSUE/ENHANCEMENT : {Audit Trails} 
                                    Dim objCommonATLog As New clsCommonATLog
                                    objCommonATLog._FormName = mstrFormName
                                    objCommonATLog._LoginEmployeeUnkid = mintLoginEmployeeunkid
                                    objCommonATLog._ClientIP = mstrClientIP
                                    objCommonATLog._HostName = mstrHostName
                                    objCommonATLog._FromWeb = mblnIsWeb
                                    objCommonATLog._AuditUserId = mintAuditUserId
objCommonATLog._CompanyUnkid = mintCompanyUnkid
                                    objCommonATLog._AuditDate = mdtAuditDate
                                    'S.SANDEEP [28-May-2018] -- END

                                    If objCommonATLog.Insert_TranAtLog(objDataOperation, "rcapplicant_master", "applicantunkid", .Item("applicantunkid"), "rcjobhistory", "jobhistorytranunkid", .Item("jobhistorytranunkid"), 2, 3) = False Then
                                        exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                                        Throw exForce
                                    End If

                                    'S.SANDEEP [28-May-2018] -- START
                                    'ISSUE/ENHANCEMENT : {Audit Trails} 
                                    objCommonATLog = Nothing
                                    'S.SANDEEP [28-May-2018] -- END

                                End If


                                strQ = "DELETE FROM rcjobhistory " & _
                                        "WHERE jobhistorytranunkid = @jobhistorytranunkid "

                                objDataOperation.AddParameter("@jobhistorytranunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, .Item("jobhistorytranunkid").ToString)

                                Call objDataOperation.ExecNonQuery(strQ)

                                If objDataOperation.ErrorMessage <> "" Then
                                    exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                                    Throw exForce
                                End If
                        End Select
                    End If
                End With
            Next
            objDataOperation.ReleaseTransaction(True)
            Return True
        Catch ex As Exception
            objDataOperation.ReleaseTransaction(False)
            'DisplayError.Show("-1", ex.Message, "InsertUpdateDelete_JobHistoryTran", mstrModuleName)
            Throw New Exception(ex.Message & "; Procedure Name: InsertUpdateDelete_JobHistoryTran; Module Name: " & mstrModuleName)
            Return False
        End Try
    End Function
#End Region
End Class
