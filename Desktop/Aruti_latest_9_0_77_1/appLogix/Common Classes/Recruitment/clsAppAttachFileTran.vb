﻿'************************************************************************************************************************************
'Class Name : clsAppAttachFileTran.vb
'Purpose    :
'Date       :27/05/2017
'Written By :Sohail
'Modified   :
'************************************************************************************************************************************
Imports eZeeCommonLib
Imports Aruti.Data
Imports System.Web


Public Class clsAppAttachFileTran
    Private Const mstrModuleName = "clsAppAttachFileTran"
    Dim objDataOperation As clsDataOperation
    Dim mstrMessage As String = ""

#Region " Private Variables "
    Private mintAttachfiletranunkid As Integer
    Private mintApplicantunkid As Integer
    Private mdtTran As DataTable

#End Region

#Region " Properties "

'S.SANDEEP [28-May-2018] -- START 
'ISSUE/ENHANCEMENT : {Audit Trails} 
Private mstrFormName As String = String.Empty
Public WriteOnly Property _FormName() As String 
Set(ByVal value As String) 
mstrFormName = value 
End Set 
End Property 
 
Private mstrClientIP As String = ""
Public WriteOnly Property _ClientIP() As String 
Set(ByVal value As String) 
mstrClientIP = value 
End Set 
End Property 
 
Private mstrHostName As String = ""
Public WriteOnly Property _HostName() As String 
Set(ByVal value As String) 
mstrHostName = value 
End Set 
End Property 
 
Private mblnIsWeb As Boolean = False
Public WriteOnly Property _FromWeb() As Boolean 
Set(ByVal value As Boolean) 
mblnIsWeb = value 
End Set 
End Property 
 
    Private mintAuditUserId As Integer = 0
    Public WriteOnly Property _AuditUserId() As Integer
        Set(ByVal value As Integer)
            mintAuditUserId = value
        End Set
    End Property

    Private mintCompanyUnkid As Integer = 0
    Public WriteOnly Property _CompanyUnkid() As Integer
        Set(ByVal value As Integer)
            mintCompanyUnkid = value
        End Set
    End Property
 
Private mdtAuditDate As DateTime = Now
Public WriteOnly Property _AuditDate() As DateTime 
Set(ByVal value As DateTime) 
mdtAuditDate = value 
End Set 
End Property 
 
Private xDataOpr As clsDataOperation = Nothing
Public Property _xDataOpr() As clsDataOperation 
Get 
Return xDataOpr 
End Get 
Set(ByVal value As clsDataOperation) 
xDataOpr = value 
End Set 
End Property 
 
Private mintLoginEmployeeunkid As Integer = 0
Public Property _LoginEmployeeunkid() As Integer 
Get 
Return mintLoginEmployeeunkid 
End Get 
Set(ByVal value As Integer) 
mintLoginEmployeeunkid = value 
End Set 
End Property 
 
Private mintVoidlogingemployeeunkid As Integer=0
Public Property _Voidloginemployeeunkid() As Integer 
Get 
Return mintVoidlogingemployeeunkid 
End Get 
Set(ByVal value As Integer) 
mintVoidlogingemployeeunkid = value 
End Set 
End Property 
'S.SANDEEP [28-May-2018] -- END 

    Public Property _ApplicantUnkid() As Integer
        Get
            Return mintApplicantunkid
        End Get
        Set(ByVal value As Integer)
            mintApplicantunkid = value
            Call GetAttachFile_tran()
        End Set
    End Property

    Public Property _DataList() As DataTable
        Get
            Return mdtTran
        End Get
        Set(ByVal value As DataTable)
            mdtTran = value
        End Set
    End Property
#End Region

#Region " Constructor "
    Public Sub New()
        mdtTran = New DataTable("applicantattachfiletran")

        mdtTran.Columns.Add("attachfiletranunkid", System.Type.GetType("System.Int32")).DefaultValue = 0
        mdtTran.Columns.Add("applicantunkid", System.Type.GetType("System.Int32")).DefaultValue = 0
        mdtTran.Columns.Add("documentunkid", System.Type.GetType("System.Int32")).DefaultValue = 0
        mdtTran.Columns.Add("modulerefid", System.Type.GetType("System.Int32")).DefaultValue = 0
        mdtTran.Columns.Add("attachrefid", System.Type.GetType("System.Int32")).DefaultValue = 0
        mdtTran.Columns.Add("filepath", System.Type.GetType("System.String")).DefaultValue = ""
        mdtTran.Columns.Add("filename", System.Type.GetType("System.String")).DefaultValue = ""
        mdtTran.Columns.Add("fileuniquename", System.Type.GetType("System.String")).DefaultValue = ""
        mdtTran.Columns.Add("attached_date", System.Type.GetType("System.DateTime")).DefaultValue = Nothing
        mdtTran.Columns.Add("file_size", System.Type.GetType("System.Int32")).DefaultValue = 0
        mdtTran.Columns.Add("AUD", System.Type.GetType("System.String")).DefaultValue = ""
        mdtTran.Columns.Add("GUID", System.Type.GetType("System.String")).DefaultValue = ""
        mdtTran.Columns.Add("serverattachfiletranunkid", System.Type.GetType("System.Int32")).DefaultValue = 0
        mdtTran.Columns.Add("isvoid", System.Type.GetType("System.Boolean")).DefaultValue = False
        mdtTran.Columns.Add("voiduserunkid", System.Type.GetType("System.Int32")).DefaultValue = 0
        mdtTran.Columns.Add("voiddatetime", System.Type.GetType("System.DateTime")).DefaultValue = Nothing
        mdtTran.Columns.Add("voidreason", System.Type.GetType("System.String")).DefaultValue = ""
        mdtTran.Columns.Add("appvacancytranunkid", System.Type.GetType("System.Int32")).DefaultValue = 0
        'S.SANDEEP |04-MAY-2023| -- START
        'ISSUE/ENHANCEMENT : A1X-833
        mdtTran.Columns.Add("jobhistorytranunkid", GetType(System.Int32)).DefaultValue = 0
        'S.SANDEEP |04-MAY-2023| -- END

        'Pinkal (30-Sep-2023) -- Start 
        '(A1X-1354) TRA - Disable "Other Qualification" option from the qualifications tab on the applicant portal. 
        mdtTran.Columns.Add("applicantqualiftranunkid", GetType(System.Int32)).DefaultValue = 0
        'Pinkal (30-Sep-2023) -- End 

    End Sub
#End Region

#Region " Private Methods "
    Private Sub GetAttachFile_tran()
        Dim strQ As String = ""
        Dim strErrorMessage As String = ""
        Dim dsList As DataSet = Nothing
        Dim dRowID_Tran As DataRow
        Dim exForce As Exception

        objDataOperation = New clsDataOperation

        Try




            strQ = "SELECT  attachfiletranunkid" & _
                            ", applicantunkid " & _
                            ", documentunkid " & _
                            ", modulerefid " & _
                            ", attachrefid " & _
                            ", filepath " & _
                            ", filename " & _
                            ", fileuniquename " & _
                            ", attached_date " & _
                            ", ISNULL(file_size, 0) AS file_size " & _
                            ", '' AUD " & _
                            ", ISNULL(serverattachfiletranunkid, 0) AS serverattachfiletranunkid " & _
                            ", isvoid " & _
                            ", voiduserunkid " & _
                            ", voiddatetime " & _
                            ", voidreason " & _
                            ", ISNULL(rcattachfiletran.appvacancytranunkid, 0) AS appvacancytranunkid " & _
                            ", ISNULL(jobhistorytranunkid,0) AS jobhistorytranunkid " & _
                            ", ISNULL(applicantqualiftranunkid,0) AS applicantqualiftranunkid " & _
                    "FROM rcattachfiletran " & _
                    "WHERE ISNULL(isvoid, 0) = 0 AND applicantunkid = @applicantunkid "

            'Pinkal (30-Sep-2023) -- (A1X-1354) TRA - Disable "Other Qualification" option from the qualifications tab on the applicant portal. [", ISNULL(applicantqualiftranunkid,0) AS applicantqualiftranunkid " & _]

            'S.SANDEEP |04-MAY-2023| -- START {A1X-833} [jobhistorytranunkid] -- END

            objDataOperation.AddParameter("@applicantunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintApplicantunkid)

            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & " :" & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            mdtTran.Rows.Clear()

            For i As Integer = 0 To dsList.Tables(0).Rows.Count - 1
                With dsList.Tables(0).Rows(i)
                    dRowID_Tran = mdtTran.NewRow
                    dRowID_Tran.Item("attachfiletranunkid") = .Item("attachfiletranunkid").ToString
                    dRowID_Tran.Item("applicantunkid") = .Item("applicantunkid").ToString
                    dRowID_Tran.Item("documentunkid") = .Item("documentunkid").ToString
                    dRowID_Tran.Item("modulerefid") = .Item("modulerefid").ToString
                    dRowID_Tran.Item("attachrefid") = .Item("attachrefid").ToString
                    dRowID_Tran.Item("filepath") = .Item("filepath").ToString
                    dRowID_Tran.Item("filename") = .Item("filename").ToString
                    dRowID_Tran.Item("fileuniquename") = .Item("fileuniquename").ToString
                    If IsDBNull(.Item("attached_date")) = True Then
                        dRowID_Tran.Item("attached_date") = DBNull.Value
                    Else
                        dRowID_Tran.Item("attached_date") = .Item("attached_date")
                    End If
                    dRowID_Tran.Item("file_size") = .Item("file_size").ToString
                    dRowID_Tran.Item("AUD") = .Item("AUD").ToString
                    dRowID_Tran.Item("serverattachfiletranunkid") = .Item("serverattachfiletranunkid")
                    dRowID_Tran.Item("isvoid") = .Item("isvoid")
                    dRowID_Tran.Item("voiduserunkid") = .Item("voiduserunkid")
                    If IsDBNull(.Item("voiddatetime")) = True Then
                        dRowID_Tran.Item("voiddatetime") = DBNull.Value
                    Else
                        dRowID_Tran.Item("voiddatetime") = .Item("voiddatetime")
                    End If
                    dRowID_Tran.Item("voidreason") = .Item("voidreason")
                    dRowID_Tran.Item("appvacancytranunkid") = .Item("appvacancytranunkid")
                    dRowID_Tran.Item("jobhistorytranunkid") = .Item("jobhistorytranunkid")  'S.SANDEEP |04-MAY-2023| -- START {A1X-833} [jobhistorytranunkid] -- END

                    'Pinkal (30-Sep-2023) -- Start 
                    '(A1X-1354) TRA - Disable "Other Qualification" option from the qualifications tab on the applicant portal. 
                    dRowID_Tran.Item("applicantqualiftranunkid") = .Item("applicantqualiftranunkid")
                    'Pinkal (30-Sep-2023) -- End 

                    mdtTran.Rows.Add(dRowID_Tran)
                End With
            Next

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: GetAttachFile_tran; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try

    End Sub

    Public Function InsertUpdateDelete_AttachFileTran() As Boolean
        Dim i As Integer
        Dim strQ As String = ""
        Dim strErrorMessage As String = ""
        Dim exForce As Exception

        Try
            objDataOperation = New clsDataOperation
            objDataOperation.BindTransaction()
            For i = 0 To mdtTran.Rows.Count - 1
                With mdtTran.Rows(i)
                    objDataOperation.ClearParameters()
                    If Not IsDBNull(.Item("AUD")) Then
                        Select Case .Item("AUD")
                            Case "A"


                                strQ = "INSERT INTO rcattachfiletran ( " & _
                                        "  applicantunkid " & _
                                        ", documentunkid " & _
                                        ", modulerefid " & _
                                        ", attachrefid " & _
                                        ", filepath " & _
                                        ", filename " & _
                                        ", fileuniquename " & _
                                        ", attached_date " & _
                                        ", file_size " & _
                                        ", serverattachfiletranunkid" & _
                                        ", isvoid" & _
                                        ", voiduserunkid" & _
                                        ", voiddatetime" & _
                                        ", voidreason" & _
                                        ", appvacancytranunkid " & _
                                        ", jobhistorytranunkid " & _
                                        ", applicantqualiftranunkid " & _
                                ") VALUES (" & _
                                        "  @applicantunkid " & _
                                        ", @documentunkid " & _
                                        ", @modulerefid " & _
                                        ", @attachrefid " & _
                                        ", @filepath " & _
                                        ", @filename " & _
                                        ", @fileuniquename " & _
                                        ", @attached_date " & _
                                        ", @file_size " & _
                                        ", @serverattachfiletranunkid" & _
                                        ", @isvoid" & _
                                        ", @voiduserunkid" & _
                                        ", @voiddatetime" & _
                                        ", @voidreason" & _
                                        ", @appvacancytranunkid " & _
                                        ", @jobhistorytranunkid " & _
                                        ", @applicantqualiftranunkid " & _
                                "); SELECT @@identity"

                                'Pinkal (30-Sep-2023) -- (A1X-1354) TRA - Disable "Other Qualification" option from the qualifications tab on the applicant portal. [@applicantqualiftranunkid ]

                                'S.SANDEEP |04-MAY-2023| -- START {A1X-833} [jobhistorytranunkid] -- END

                                objDataOperation.AddParameter("@applicantunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintApplicantunkid)
                                objDataOperation.AddParameter("@documentunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, .Item("documentunkid").ToString)
                                objDataOperation.AddParameter("@modulerefid", SqlDbType.Int, eZeeDataType.INT_SIZE, .Item("modulerefid").ToString)
                                objDataOperation.AddParameter("@attachrefid", SqlDbType.Int, eZeeDataType.INT_SIZE, .Item("attachrefid").ToString)
                                objDataOperation.AddParameter("@filepath", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, .Item("filepath").ToString)
                                objDataOperation.AddParameter("@filename", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, .Item("filename").ToString)
                                objDataOperation.AddParameter("@fileuniquename", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, .Item("fileuniquename").ToString)
                                If IsDBNull(.Item("attached_date")) = True Then
                                    objDataOperation.AddParameter("@attached_date", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, DBNull.Value)
                                Else
                                    objDataOperation.AddParameter("@attached_date", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, .Item("attached_date"))
                                End If
                                objDataOperation.AddParameter("@file_size", SqlDbType.Int, eZeeDataType.INT_SIZE, .Item("file_size").ToString)
                                objDataOperation.AddParameter("@serverattachfiletranunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, .Item("serverattachfiletranunkid"))
                                objDataOperation.AddParameter("@isvoid", SqlDbType.Bit, eZeeDataType.BIT_SIZE, .Item("isvoid"))
                                objDataOperation.AddParameter("@voiduserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, .Item("voiduserunkid"))
                                If IsDBNull(.Item("voiddatetime")) = True Then
                                    objDataOperation.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, DBNull.Value)
                                Else
                                    objDataOperation.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, .Item("voiddatetime"))
                                End If
                                objDataOperation.AddParameter("@voidreason", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, .Item("voidreason").ToString)
                                objDataOperation.AddParameter("@appvacancytranunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, .Item("appvacancytranunkid"))

                                'S.SANDEEP |04-MAY-2023| -- START
                                'ISSUE/ENHANCEMENT : A1X-833
                                objDataOperation.AddParameter("@jobhistorytranunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, .Item("jobhistorytranunkid"))
                                'S.SANDEEP |04-MAY-2023| -- END

                                'Pinkal (30-Sep-2023) -- Start 
                                '(A1X-1354) TRA - Disable "Other Qualification" option from the qualifications tab on the applicant portal. 
                                objDataOperation.AddParameter("@applicantqualiftranunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, .Item("applicantqualiftranunkid"))
                                'Pinkal (30-Sep-2023) -- End 

                                Dim dsList As DataSet = objDataOperation.ExecQuery(strQ, "List")

                                If objDataOperation.ErrorMessage <> "" Then
                                    exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                                    Throw exForce
                                End If


                                mintAttachfiletranunkid = dsList.Tables(0).Rows(0)(0)

                                If .Item("applicantunkid") > 0 Then
'S.SANDEEP [28-May-2018] -- START
'ISSUE/ENHANCEMENT : {Audit Trails} 
Dim objCommonATLog AS New clsCommonATLog 
objCommonATLog._FormName = mstrFormName 
objCommonATLog._LoginEmployeeUnkid = mintLoginEmployeeunkid 
                                    objCommonATLog._ClientIP = mstrClientIP
                                    objCommonATLog._HostName = mstrHostName
                                    objCommonATLog._FromWeb = mblnIsWeb
                                    objCommonATLog._AuditUserId = mintAuditUserId
objCommonATLog._CompanyUnkid = mintCompanyUnkid
                                    objCommonATLog._AuditDate = mdtAuditDate
                                    'S.SANDEEP [28-May-2018] -- END
                                    'Sohail (09 Oct 2018) -- Start
                                    'TANAPA - Support Issue Id # 2502 - linking recruitment server app_vacancy with aruti desktop app_vacancy in 75.1.
                                    'If clsCommonATLog.Insert_TranAtLog(objDataOperation, "rcapplicant_master", "applicantunkid", .Item("applicantunkid"), "rcattachfiletran", "attachfiletranunkid", mintAttachfiletranunkid, 2, 1) = False Then
                                    If objCommonATLog.Insert_TranAtLog(objDataOperation, "rcapplicant_master", "applicantunkid", mintApplicantunkid, "rcattachfiletran", "attachfiletranunkid", mintAttachfiletranunkid, 2, 1) = False Then
                                        'Sohail (09 Oct 2018) -- End
                                        exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                                        Throw exForce
                                    End If

                                    'S.SANDEEP [28-May-2018] -- START
                                    'ISSUE/ENHANCEMENT : {Audit Trails} 
                                    objCommonATLog = Nothing
                                    'S.SANDEEP [28-May-2018] -- END

                                Else
                                    'S.SANDEEP [28-May-2018] -- START
                                    'ISSUE/ENHANCEMENT : {Audit Trails} 
                                    Dim objCommonATLog As New clsCommonATLog
                                    objCommonATLog._FormName = mstrFormName
                                    objCommonATLog._LoginEmployeeUnkid = mintLoginEmployeeunkid
                                    objCommonATLog._ClientIP = mstrClientIP
                                    objCommonATLog._HostName = mstrHostName
                                    objCommonATLog._FromWeb = mblnIsWeb
                                    objCommonATLog._AuditUserId = mintAuditUserId
objCommonATLog._CompanyUnkid = mintCompanyUnkid
                                    objCommonATLog._AuditDate = mdtAuditDate
                                    'S.SANDEEP [28-May-2018] -- END

                                    If objCommonATLog.Insert_TranAtLog(objDataOperation, "rcapplicant_master", "applicantunkid", mintApplicantunkid, "rcattachfiletran", "attachfiletranunkid", mintAttachfiletranunkid, 1, 1) = False Then
                                        exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                                        Throw exForce
                                    End If

                                    'S.SANDEEP [28-May-2018] -- START
                                    'ISSUE/ENHANCEMENT : {Audit Trails} 
                                    objCommonATLog = Nothing
                                    'S.SANDEEP [28-May-2018] -- END

                                End If


                            Case "U"


                                strQ = "UPDATE rcattachfiletran SET " & _
                                            "  applicantunkid = @applicantunkid" & _
                                            ", documentunkid = @documentunkid " & _
                                            ", modulerefid = @modulerefid " & _
                                            ", attachrefid = @attachrefid " & _
                                            ", filepath = @filepath " & _
                                            ", filename = @filename " & _
                                            ", fileuniquename = @fileuniquename " & _
                                            ", attached_date = @attached_date " & _
                                            ", file_size = @file_size " & _
                                            ", serverattachfiletranunkid = @serverattachfiletranunkid" & _
                                            ", isvoid = @isvoid" & _
                                            ", voiduserunkid = @voiduserunkid" & _
                                            ", voiddatetime = @voiddatetime" & _
                                            ", voidreason = @voidreason " & _
                                            ", appvacancytranunkid = @appvacancytranunkid " & _
                                            ", jobhistorytranunkid = @jobhistorytranunkid " & _
                                            ", applicantqualiftranunkid = @applicantqualiftranunkid " & _
                                        "WHERE attachfiletranunkid = @attachfiletranunkid "

                                'Pinkal (30-Sep-2023) --(A1X-1354) TRA - Disable "Other Qualification" option from the qualifications tab on the applicant portal. [", applicantqualiftranunkid = @applicantqualiftranunkid " & _]
                                'S.SANDEEP |04-MAY-2023| -- START {A1X-833} [jobhistorytranunkid] -- END

                                objDataOperation.AddParameter("@attachfiletranunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, .Item("attachfiletranunkid").ToString)
                                objDataOperation.AddParameter("@applicantunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintApplicantunkid)
                                objDataOperation.AddParameter("@documentunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, .Item("documentunkid").ToString)
                                objDataOperation.AddParameter("@modulerefid", SqlDbType.Int, eZeeDataType.INT_SIZE, .Item("modulerefid").ToString)
                                objDataOperation.AddParameter("@attachrefid", SqlDbType.Int, eZeeDataType.INT_SIZE, .Item("attachrefid").ToString)
                                objDataOperation.AddParameter("@filepath", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, .Item("filepath").ToString)
                                objDataOperation.AddParameter("@filename", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, .Item("filename").ToString)
                                objDataOperation.AddParameter("@fileuniquename", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, .Item("fileuniquename").ToString)
                                If IsDBNull(.Item("attached_date")) = True Then
                                    objDataOperation.AddParameter("@attached_date", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, DBNull.Value)
                                Else
                                    objDataOperation.AddParameter("@attached_date", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, .Item("attached_date"))
                                End If
                                objDataOperation.AddParameter("@file_size", SqlDbType.Int, eZeeDataType.INT_SIZE, .Item("file_size").ToString)
                                objDataOperation.AddParameter("@serverattachfiletranunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, .Item("serverattachfiletranunkid"))
                                objDataOperation.AddParameter("@isvoid", SqlDbType.Bit, eZeeDataType.BIT_SIZE, .Item("isvoid"))
                                objDataOperation.AddParameter("@voiduserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, .Item("voiduserunkid"))
                                If IsDBNull(.Item("voiddatetime")) = True Then
                                    objDataOperation.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, DBNull.Value)
                                Else
                                    objDataOperation.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, .Item("voiddatetime"))
                                End If
                                objDataOperation.AddParameter("@voidreason", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, .Item("voidreason").ToString)
                                objDataOperation.AddParameter("@appvacancytranunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, .Item("appvacancytranunkid"))

                                'S.SANDEEP |04-MAY-2023| -- START
                                'ISSUE/ENHANCEMENT : A1X-833
                                objDataOperation.AddParameter("@jobhistorytranunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, .Item("jobhistorytranunkid"))
                                'S.SANDEEP |04-MAY-2023| -- END

                                'Pinkal (30-Sep-2023) -- Start 
                                '(A1X-1354) TRA - Disable "Other Qualification" option from the qualifications tab on the applicant portal. 
                                objDataOperation.AddParameter("@applicantqualiftranunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, .Item("applicantqualiftranunkid"))
                                'Pinkal (30-Sep-2023) -- End 

                                Call objDataOperation.ExecNonQuery(strQ)

                                If objDataOperation.ErrorMessage <> "" Then
                                    exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                                    Throw exForce
                                End If

                                'S.SANDEEP [28-May-2018] -- START
                                'ISSUE/ENHANCEMENT : {Audit Trails} 
                                Dim objCommonATLog As New clsCommonATLog
                                objCommonATLog._FormName = mstrFormName
                                objCommonATLog._LoginEmployeeUnkid = mintLoginEmployeeunkid
                                objCommonATLog._ClientIP = mstrClientIP
                                objCommonATLog._HostName = mstrHostName
                                objCommonATLog._FromWeb = mblnIsWeb
                                objCommonATLog._AuditUserId = mintAuditUserId
objCommonATLog._CompanyUnkid = mintCompanyUnkid
                                objCommonATLog._AuditDate = mdtAuditDate
                                'S.SANDEEP [28-May-2018] -- END

                                If objCommonATLog.Insert_TranAtLog(objDataOperation, "rcapplicant_master", "applicantunkid", .Item("applicantunkid"), "rcattachfiletran", "attachfiletranunkid", .Item("attachfiletranunkid"), 2, 2) = False Then
                                    exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                                    Throw exForce
                                End If

                                'S.SANDEEP [28-May-2018] -- START
                                'ISSUE/ENHANCEMENT : {Audit Trails} 
                                objCommonATLog = Nothing
                                'S.SANDEEP [28-May-2018] -- END



                            Case "D"

                                If .Item("attachfiletranunkid") > 0 Then
                                    'S.SANDEEP [28-May-2018] -- START
                                    'ISSUE/ENHANCEMENT : {Audit Trails} 
                                    Dim objCommonATLog As New clsCommonATLog
                                    objCommonATLog._FormName = mstrFormName
                                    objCommonATLog._LoginEmployeeUnkid = mintLoginEmployeeunkid
                                    objCommonATLog._ClientIP = mstrClientIP
                                    objCommonATLog._HostName = mstrHostName
                                    objCommonATLog._FromWeb = mblnIsWeb
                                    objCommonATLog._AuditUserId = mintAuditUserId
objCommonATLog._CompanyUnkid = mintCompanyUnkid
                                    objCommonATLog._AuditDate = mdtAuditDate
                                    'S.SANDEEP [28-May-2018] -- END

                                    If objCommonATLog.Insert_TranAtLog(objDataOperation, "rcapplicant_master", "applicantunkid", .Item("applicantunkid"), "rcattachfiletran", "attachfiletranunkid", .Item("attachfiletranunkid"), 2, 3) = False Then
                                        exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                                        Throw exForce
                                    End If

                                    'S.SANDEEP [28-May-2018] -- START
                                    'ISSUE/ENHANCEMENT : {Audit Trails} 
                                    objCommonATLog = Nothing
                                    'S.SANDEEP [28-May-2018] -- END

                                End If


                                strQ = "DELETE FROM rcattachfiletran " & _
                                        "WHERE attachfiletranunkid = @attachfiletranunkid "

                                objDataOperation.AddParameter("@attachfiletranunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, .Item("attachfiletranunkid").ToString)

                                Call objDataOperation.ExecNonQuery(strQ)

                                If objDataOperation.ErrorMessage <> "" Then
                                    exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                                    Throw exForce
                                End If


                        End Select
                    End If
                End With
            Next
            objDataOperation.ReleaseTransaction(True)
            Return True
        Catch ex As Exception
            objDataOperation.ReleaseTransaction(False)
            Throw New Exception(ex.Message & "; Procedure Name: InsertUpdateDelete_AttachFileTran; Module Name: " & mstrModuleName)
            Return False
        End Try

    End Function

    Public Function InsertAll(ByVal intAppVacancyTranunkid As Integer, ByVal lstAttach As List(Of dtoAddAppAttach), Optional ByVal xDataOp As clsDataOperation = Nothing) As Boolean
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        Dim objScan As New clsScan_Attach_Documents
        Dim dt As DataTable = objScan._Datatable

        If xDataOp IsNot Nothing Then
            objDataOperation = xDataOp
        Else
            objDataOperation = New clsDataOperation
            objDataOperation.BindTransaction()
        End If
        objDataOperation.ClearParameters()

        Try

            strQ = "INSERT INTO rcattachfiletran ( " & _
                                        "  applicantunkid " & _
                                        ", documentunkid " & _
                                        ", modulerefid " & _
                                        ", attachrefid " & _
                                        ", filepath " & _
                                        ", filename " & _
                                        ", fileuniquename " & _
                                        ", attached_date " & _
                                        ", file_size " & _
                                        ", serverattachfiletranunkid" & _
                                        ", isvoid" & _
                                        ", voiduserunkid" & _
                                        ", voiddatetime" & _
                                        ", voidreason" & _
                                        ", appvacancytranunkid " & _
                                        ", jobhistorytranunkid " & _
                                        ", applicantqualiftranunkid " & _
                                ") VALUES (" & _
                                        "  @applicantunkid " & _
                                        ", @documentunkid " & _
                                        ", @modulerefid " & _
                                        ", @attachrefid " & _
                                        ", @filepath " & _
                                        ", @filename " & _
                                        ", @fileuniquename " & _
                                        ", @attached_date " & _
                                        ", @file_size " & _
                                        ", @serverattachfiletranunkid" & _
                                        ", @isvoid" & _
                                        ", @voiduserunkid" & _
                                        ", @voiddatetime" & _
                                        ", @voidreason" & _
                                        ", @appvacancytranunkid " & _
                                        ", @jobhistorytranunkid " & _
                                        ", @applicantqualiftranunkid " & _
                                "); SELECT @@identity"


            'Pinkal (30-Sep-2023) -- (A1X-1354) TRA - Disable "Other Qualification" option from the qualifications tab on the applicant portal. [applicantqualiftranunkid]

            'S.SANDEEP |04-MAY-2023| -- START {A1X-833} [jobhistorytranunkid] -- END
            For Each dto As dtoAddAppAttach In lstAttach

                If dto.Employeeunkid <= 0 Then 'External applicant

                    objDataOperation.ClearParameters()
                    objDataOperation.AddParameter("@applicantunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, dto.ApplicantUnkid)
                    objDataOperation.AddParameter("@documentunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, dto.DocumentUnkid.ToString)
                    objDataOperation.AddParameter("@modulerefid", SqlDbType.Int, eZeeDataType.INT_SIZE, dto.ModulerefId.ToString)
                    objDataOperation.AddParameter("@attachrefid", SqlDbType.Int, eZeeDataType.INT_SIZE, dto.AttachrefId.ToString)
                    objDataOperation.AddParameter("@filepath", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, dto.Filepath.ToString)
                    objDataOperation.AddParameter("@filename", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, dto.Filename.ToString)
                    objDataOperation.AddParameter("@fileuniquename", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, dto.Fileuniquename.ToString)
                    If IsDBNull(dto.Attached_Date) = True Then
                        objDataOperation.AddParameter("@attached_date", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, DBNull.Value)
                    Else
                        objDataOperation.AddParameter("@attached_date", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, dto.Attached_Date)
                    End If
                    objDataOperation.AddParameter("@file_size", SqlDbType.Int, eZeeDataType.INT_SIZE, dto.File_size.ToString)
                    objDataOperation.AddParameter("@serverattachfiletranunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, dto.ServerAttachFileTranunkid)
                    objDataOperation.AddParameter("@isvoid", SqlDbType.Bit, eZeeDataType.BIT_SIZE, dto.IsVoid)
                    objDataOperation.AddParameter("@voiduserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, dto.VoidUserUnkid)
                    If IsDBNull(dto.VoidDateTime) = True Then
                        objDataOperation.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, DBNull.Value)
                    Else
                        objDataOperation.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, dto.VoidDateTime)
                    End If
                    objDataOperation.AddParameter("@voidreason", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, dto.VoidReason.ToString)
                    objDataOperation.AddParameter("@appvacancytranunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intAppVacancyTranunkid)

                    'S.SANDEEP |04-MAY-2023| -- START
                    'ISSUE/ENHANCEMENT : A1X-833
                    objDataOperation.AddParameter("@jobhistorytranunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, dto.Jobhistorytranunkid)
                    'S.SANDEEP |04-MAY-2023| -- END

                    'Pinkal (30-Sep-2023) -- Start 
                    '(A1X-1354) TRA - Disable "Other Qualification" option from the qualifications tab on the applicant portal. 
                    objDataOperation.AddParameter("@applicantqualiftranunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, dto.ApplicantQualiftranunkid)
                    'Pinkal (30-Sep-2023) -- End 

                    dsList = objDataOperation.ExecQuery(strQ, "List")

                    If objDataOperation.ErrorMessage <> "" Then
                        If xDataOp Is Nothing Then objDataOperation.ReleaseTransaction(False)
                        exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                        Throw exForce
                    End If

                    Dim objCommonATLog As New clsCommonATLog

                    'S.SANDEEP [28-May-2018] -- START
                    'ISSUE/ENHANCEMENT : {Audit Trails}                                 
                    objCommonATLog._FormName = mstrFormName
                    objCommonATLog._LoginEmployeeUnkid = mintLoginEmployeeunkid
                    objCommonATLog._ClientIP = mstrClientIP
                    objCommonATLog._HostName = mstrHostName
                    objCommonATLog._FromWeb = mblnIsWeb
                    objCommonATLog._AuditUserId = mintAuditUserId
                    objCommonATLog._CompanyUnkid = mintCompanyUnkid
                    objCommonATLog._AuditDate = mdtAuditDate
                    'S.SANDEEP [28-May-2018] -- END

                    If dto.ApplicantUnkid > 0 Then
                        If objCommonATLog.Insert_TranAtLog(objDataOperation, "rcapplicant_master", "applicantunkid", dto.ApplicantUnkid, "rcattachfiletran", "attachfiletranunkid", dto.AttachFileTranunkid, 2, 1) = False Then
                            exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                            Throw exForce
                        End If
                    Else
                        If objCommonATLog.Insert_TranAtLog(objDataOperation, "rcapplicant_master", "applicantunkid", dto.ApplicantUnkid, "rcattachfiletran", "attachfiletranunkid", dto.AttachFileTranunkid, 1, 1) = False Then
                            exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                            Throw exForce
                        End If
                    End If
                    objCommonATLog = Nothing

                End If

                Dim rw As DataRow = dt.NewRow
                rw.Item("scanattachtranunkid") = -1
                rw.Item("documentunkid") = dto.DocumentUnkid.ToString
                If dto.Employeeunkid > 0 Then
                    rw.Item("employeeunkid") = dto.Employeeunkid
                Else
                    rw.Item("employeeunkid") = dto.ApplicantUnkid
                End If
                rw.Item("filename") = dto.Filename
                rw.Item("scanattachrefid") = dto.AttachrefId
                rw.Item("modulerefid") = dto.ModulerefId
                rw.Item("userunkid") = dto.UserUnkId
                rw.Item("transactionunkid") = -1
                rw.Item("attached_date") = dto.Attached_Date
                rw.Item("destfilepath") = dto.Filepath
                rw.Item("fileuniquename") = dto.Fileuniquename
                rw.Item("filepath") = dto.Filepath
                rw.Item("filesize") = dto.File_size
                'Hemant (15 Jun 2022) -- Start            
                'ISSUE : NMB - Unable to preview attachment from Client Machine
                'Dim xDocumentData As Byte() = IO.File.ReadAllBytes(dto.Filepath)
                'rw.Item("file_data") = xDocumentData
                rw.Item("file_data") = dto.FileData
                'Hemant (15 Jun 2022) -- End
                rw.Item("form_name") = dto.ModuleName
                rw.Item("appvacancytranunkid") = intAppVacancyTranunkid
                
                rw.Item("AUD") = "A"

                dt.Rows.Add(rw)
            Next

            objScan._Datatable = dt
            If objScan.InsertUpdateDelete_Documents(objDataOperation) = False Then
                If xDataOp Is Nothing Then objDataOperation.ReleaseTransaction(False)
                Throw New Exception(objScan._Message)
                Return False
            End If

            If xDataOp Is Nothing Then objDataOperation.ReleaseTransaction(True)

            Return True
        Catch ex As Exception
            If xDataOp Is Nothing Then objDataOperation.ReleaseTransaction(False)
            Throw New Exception(ex.Message & "; Procedure Name: Insert; Module Name: " & mstrModuleName)
            Return False
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            If xDataOp Is Nothing Then objDataOperation = Nothing

        End Try
    End Function

#End Region

End Class

'Hemant (11 Apr 2022) -- Start            
'ISSUE/ENHANCEMENT(ZRA) : Move CV, cover letter and qualification on search job page to allow to attach documents for each vacancy in self service
Public Class dtoAddAppAttach

    Private mintAttachFileTranunkid As Integer
    Public Property AttachFileTranunkid() As Integer
        Get
            Return mintAttachFileTranunkid
        End Get
        Set(ByVal value As Integer)
            mintAttachFileTranunkid = value
        End Set
    End Property

    Private mintApplicantUnkid As Integer
    Public Property ApplicantUnkid() As Integer
        Get
            Return mintApplicantUnkid
        End Get
        Set(ByVal value As Integer)
            mintApplicantUnkid = value
        End Set
    End Property

    Private mintEmployeeunkid As Integer = 0
    Public Property Employeeunkid() As Integer
        Get
            Return mintEmployeeunkid
        End Get
        Set(ByVal value As Integer)
            mintEmployeeunkid = value
        End Set
    End Property

    Private mguiUserId As Guid = Nothing
    Public Property UserId() As Guid
        Get
            Return mguiUserId
        End Get
        Set(ByVal value As Guid)
            mguiUserId = value
        End Set
    End Property

    Private mintDocumentUnkid As Integer
    Public Property DocumentUnkid() As Integer
        Get
            Return mintDocumentUnkid
        End Get
        Set(ByVal value As Integer)
            mintDocumentUnkid = value
        End Set
    End Property

    Private mintModulerefId As Integer
    Public Property ModulerefId() As Integer
        Get
            Return mintModulerefId
        End Get
        Set(ByVal value As Integer)
            mintModulerefId = value
        End Set
    End Property

    Private mintAttachrefId As Integer
    Public Property AttachrefId() As Integer
        Get
            Return mintAttachrefId
        End Get
        Set(ByVal value As Integer)
            mintAttachrefId = value
        End Set
    End Property

    Private mstrFilepath As String
    Public Property Filepath() As String
        Get
            Return mstrFilepath
        End Get
        Set(ByVal value As String)
            mstrFilepath = value
        End Set
    End Property

    Private mstrFolderName As String
    Public Property FolderName() As String
        Get
            Return mstrFolderName
        End Get
        Set(ByVal value As String)
            mstrFolderName = value
        End Set
    End Property

    Private mstrFilename As String
    Public Property Filename() As String
        Get
            Return mstrFilename
        End Get
        Set(ByVal value As String)
            mstrFilename = value
        End Set
    End Property

    Private mstrFileuniquename As String
    Public Property Fileuniquename() As String
        Get
            Return mstrFileuniquename
        End Get
        Set(ByVal value As String)
            mstrFileuniquename = value
        End Set
    End Property

    Private mdtAttached_Date As Date
    Public Property Attached_Date() As Date
        Get
            Return mdtAttached_Date
        End Get
        Set(ByVal value As Date)
            mdtAttached_Date = value
        End Set
    End Property

    Private mintFile_size As Integer
    Public Property File_size() As Integer
        Get
            Return mintFile_size
        End Get
        Set(ByVal value As Integer)
            mintFile_size = value
        End Set
    End Property

    Public mintRetAppvacancytranunkid_UnkID As Integer
    Public Property RetAppvacancytranunkid_UnkID() As Integer
        Get
            Return mintRetAppvacancytranunkid_UnkID
        End Get
        Set(ByVal value As Integer)
            mintRetAppvacancytranunkid_UnkID = value
        End Set
    End Property

    Private mhpfflUpload As HttpPostedFile
    Public Property flUpload() As HttpPostedFile
        Get
            Return mhpfflUpload
        End Get
        Set(ByVal value As HttpPostedFile)
            mhpfflUpload = value
        End Set
    End Property


    Private mintserverattachfiletranunkid As Integer
    Public Property ServerAttachFileTranunkid() As Integer
        Get
            Return mintserverattachfiletranunkid
        End Get
        Set(ByVal value As Integer)
            mintserverattachfiletranunkid = value
        End Set
    End Property

    Private mintUserUnkId As Integer
    Public Property UserUnkId() As Integer
        Get
            Return mintUserUnkId
        End Get
        Set(ByVal value As Integer)
            mintUserUnkId = value
        End Set
    End Property

    Private mblIsVoid As Boolean
    Public Property IsVoid() As Boolean
        Get
            Return mblIsVoid
        End Get
        Set(ByVal value As Boolean)
            mblIsVoid = value
        End Set
    End Property


    Private mintVoidUserUnkid As Integer
    Public Property VoidUserUnkid() As Integer
        Get
            Return mintVoidUserUnkid
        End Get
        Set(ByVal value As Integer)
            mintVoidUserUnkid = value
        End Set
    End Property


    Private mdtVoidDateTime As DateTime
    Public Property VoidDateTime() As DateTime
        Get
            Return mdtVoidDateTime
        End Get
        Set(ByVal value As DateTime)
            mdtVoidDateTime = value
        End Set
    End Property


    Private mstrVoidReason As String
    Public Property VoidReason() As String
        Get
            Return mstrVoidReason
        End Get
        Set(ByVal value As String)
            mstrVoidReason = value
        End Set
    End Property


    'Private mintAppVacancyTranunkid As Integer
    'Public Property AppVacancyTranunkid() As Integer
    '    Get
    '        Return mintAppVacancyTranunkid
    '    End Get
    '    Set(ByVal value As Integer)
    '        mintAppVacancyTranunkid = value
    '    End Set

    'End Property


    Private mstrModuleName As String
    Public Property ModuleName() As String
        Get
            Return mstrModuleName
        End Get
        Set(ByVal value As String)
            mstrModuleName = value
        End Set
    End Property


    'Hemant (15 Jun 2022) -- Start            
    'ISSUE : NMB - Unable to preview attachment from Client Machine
    Private mbytFileData As Byte()
    Public Property FileData() As Byte()
        Get
            Return mbytFileData
        End Get
        Set(ByVal value As Byte())
            mbytFileData = value
        End Set
    End Property
    'Hemant (15 Jun 2022) -- End

    'S.SANDEEP |04-MAY-2023| -- START
    'ISSUE/ENHANCEMENT : A1X-833
    Private mintjobhistorytranunkid As Integer = 0
    Public Property Jobhistorytranunkid() As Integer
        Get
            Return mintjobhistorytranunkid
        End Get
        Set(ByVal value As Integer)
            mintjobhistorytranunkid = value
        End Set
    End Property
    'S.SANDEEP |04-MAY-2023| -- END


    'Pinkal (30-Sep-2023) -- Start 
    '(A1X-1354) TRA - Disable "Other Qualification" option from the qualifications tab on the applicant portal. 
    Private mintApplicantQualiftranunkid As Integer = 0
    Public Property ApplicantQualiftranunkid() As Integer
        Get
            Return mintApplicantQualiftranunkid
        End Get
        Set(ByVal value As Integer)
            mintApplicantQualiftranunkid = value
        End Set
    End Property
    'Pinkal (30-Sep-2023) -- End 


End Class
'Hemant (11 Apr 2022) -- End


