﻿'************************************************************************************************************************************
'Class Name : clsApplicantQualification_tran.vb
'Purpose    :
'Date       :12/10/2010
'Written By :Sandeep J. Sharma
'Modified   :
'************************************************************************************************************************************

#Region " Imports "

Imports eZeeCommonLib
Imports Aruti.Data

#End Region

''' <summary>
''' Purpose: 
''' Developer: Sandeep J. Sharma
''' </summary>

Public Class clsApplicantQualification_tran
    Private Const mstrModuleName = "clsApplicantQualification_tran"
    Dim objDataOperation As clsDataOperation
    Dim mstrMessage As String = ""

#Region " Private Variables "

    Private mintQualificationtranunkid As Integer
    Private mintApplicantunkid As Integer
    Private mdtTran As DataTable

#End Region

#Region " Properties "
'S.SANDEEP [28-May-2018] -- START 
'ISSUE/ENHANCEMENT : {Audit Trails} 
Private mstrFormName As String = String.Empty
Public WriteOnly Property _FormName() As String 
Set(ByVal value As String) 
mstrFormName = value 
End Set 
End Property 
 
Private mstrClientIP As String = ""
Public WriteOnly Property _ClientIP() As String 
Set(ByVal value As String) 
mstrClientIP = value 
End Set 
End Property 
 
Private mstrHostName As String = ""
Public WriteOnly Property _HostName() As String 
Set(ByVal value As String) 
mstrHostName = value 
End Set 
End Property 
 
Private mblnIsWeb As Boolean = False
Public WriteOnly Property _FromWeb() As Boolean 
Set(ByVal value As Boolean) 
mblnIsWeb = value 
End Set 
End Property 
 
    Private mintAuditUserId As Integer = 0
    Public WriteOnly Property _AuditUserId() As Integer
        Set(ByVal value As Integer)
            mintAuditUserId = value
        End Set
    End Property

    Private mintCompanyUnkid As Integer = 0
    Public WriteOnly Property _CompanyUnkid() As Integer
        Set(ByVal value As Integer)
            mintCompanyUnkid = value
        End Set
    End Property
 
Private mdtAuditDate As DateTime = Now
Public WriteOnly Property _AuditDate() As DateTime 
Set(ByVal value As DateTime) 
mdtAuditDate = value 
End Set 
End Property 
 
Private xDataOpr As clsDataOperation = Nothing
Public Property _xDataOpr() As clsDataOperation 
Get 
Return xDataOpr 
End Get 
Set(ByVal value As clsDataOperation) 
xDataOpr = value 
End Set 
End Property 
 
Private mintLoginEmployeeunkid As Integer = 0
Public Property _LoginEmployeeunkid() As Integer 
Get 
Return mintLoginEmployeeunkid 
End Get 
Set(ByVal value As Integer) 
mintLoginEmployeeunkid = value 
End Set 
End Property 
 
Private mintVoidlogingemployeeunkid As Integer=0
Public Property _Voidloginemployeeunkid() As Integer 
Get 
Return mintVoidlogingemployeeunkid 
End Get 
Set(ByVal value As Integer) 
mintVoidlogingemployeeunkid = value 
End Set 
End Property 
'S.SANDEEP [28-May-2018] -- END 

    'Sohail (27 Apr 2017) -- Start
    'TRA Enhancement - 66.1 - Online Recruitment Redesign changes for tra security issues.
    'Public Property _ApplicantUnkid() As Integer
    Public Property _ApplicantUnkid(Optional ByVal intApplicantQualificationSortBy As Integer = enApplicantQualificationSortBy.CompletionDate) As Integer
        'Sohail (27 Apr 2017) -- End
        Get
            Return mintApplicantunkid
        End Get
        Set(ByVal value As Integer)
            mintApplicantunkid = value
            'Sohail (27 Apr 2017) -- Start
            'TRA Enhancement - 66.1 - Online Recruitment Redesign changes for tra security issues.
            'Call GetApplicantQualification_tran()
            Call GetApplicantQualification_tran(intApplicantQualificationSortBy)
            'Sohail (27 Apr 2017) -- End
        End Set
    End Property

    Public Property _DataList() As DataTable
        Get
            Return mdtTran
        End Get
        Set(ByVal value As DataTable)
            mdtTran = value
        End Set
    End Property

    'Sohail (02 May 2022) -- Start
    'Enhancement :  AC2-312 / AC2-327 : ZRA - Newly added qualifications should not populate on old job applications for closed vacancies on applicant master.
    Private mdtVacancyEndDate As Date
    Public WriteOnly Property _VacancyEndDate() As Date
        Set(ByVal value As Date)
            mdtVacancyEndDate = value
        End Set
    End Property
    'Sohail (02 May 2022) -- End

#End Region

#Region " Constructor "
    Public Sub New()
        mdtTran = New DataTable("applicantqualificationtran")
        Dim dCol As DataColumn
        Try

            dCol = New DataColumn("qualificationtranunkid")
            dCol.DataType = System.Type.GetType("System.Int32")
            mdtTran.Columns.Add(dCol)

            dCol = New DataColumn("applicantunkid")
            dCol.DataType = System.Type.GetType("System.Int32")
            mdtTran.Columns.Add(dCol)

            dCol = New DataColumn("qualificationgroupunkid")
            dCol.DataType = System.Type.GetType("System.Int32")
            mdtTran.Columns.Add(dCol)

            'Sohail (21 Apr 2020) -- Start
            'NMB Enhancement # : On shortlisting screen, the screen is returning false results when AND condition is used and combined with other different parameters. E.g qualification group AND Nationality.
            mdtTran.Columns.Add("qlevel", System.Type.GetType("System.Int32")).DefaultValue = -1
            'Sohail (21 Apr 2020) -- End

            dCol = New DataColumn("qualificationunkid")
            dCol.DataType = System.Type.GetType("System.Int32")
            mdtTran.Columns.Add(dCol)

            dCol = New DataColumn("transaction_date")
            dCol.DataType = System.Type.GetType("System.DateTime")
            mdtTran.Columns.Add(dCol)

            dCol = New DataColumn("reference_no")
            dCol.DataType = System.Type.GetType("System.String")
            mdtTran.Columns.Add(dCol)

            dCol = New DataColumn("award_start_date")
            dCol.DataType = System.Type.GetType("System.DateTime")
            mdtTran.Columns.Add(dCol)

            dCol = New DataColumn("award_end_date")
            dCol.DataType = System.Type.GetType("System.DateTime")
            mdtTran.Columns.Add(dCol)

            dCol = New DataColumn("instituteunkid")
            dCol.DataType = System.Type.GetType("System.Int32")
            mdtTran.Columns.Add(dCol)


            'Pinkal (12-Oct-2011) -- Start
            'Enhancement : TRA Changes

            dCol = New DataColumn("resultunkid")
            dCol.DataType = System.Type.GetType("System.Int32")
            mdtTran.Columns.Add(dCol)

            dCol = New DataColumn("result_level")
            dCol.DataType = System.Type.GetType("System.Int32")
            mdtTran.Columns.Add(dCol)

            dCol = New DataColumn("gpacode")
            dCol.DataType = System.Type.GetType("System.Decimal")
            mdtTran.Columns.Add(dCol)

            'Pinkal (12-Oct-2011) -- End



            'Pinkal (06-Feb-2012) -- Start
            'Enhancement : TRA Changes

            dCol = New DataColumn("award_start_Year")
            dCol.DataType = System.Type.GetType("System.Int32")
            mdtTran.Columns.Add(dCol)

            dCol = New DataColumn("award_end_Year")
            dCol.DataType = System.Type.GetType("System.Int32")
            mdtTran.Columns.Add(dCol)


            dCol = New DataColumn("other_qualificationgrp")
            dCol.DataType = System.Type.GetType("System.String")
            mdtTran.Columns.Add(dCol)

            dCol = New DataColumn("other_qualification")
            dCol.DataType = System.Type.GetType("System.String")
            mdtTran.Columns.Add(dCol)


            dCol = New DataColumn("other_institute")
            dCol.DataType = System.Type.GetType("System.String")
            mdtTran.Columns.Add(dCol)

            dCol = New DataColumn("other_resultcode")
            dCol.DataType = System.Type.GetType("System.String")
            mdtTran.Columns.Add(dCol)


            'Pinkal (06-Feb-2012) -- End

            'Sohail (22 Apr 2015) -- Start
            'Enhancement - Providing qualification attachment, certificate no., achivement, email confirmation in online recruitmenmt.
            mdtTran.Columns.Add("certificateno", Type.GetType("System.String")).DefaultValue = ""
            'Sohail (22 Apr 2015) -- End

            'Nilay (13 Apr 2017) -- Start
            mdtTran.Columns.Add("award_end_date_string", Type.GetType("System.String")).DefaultValue = "88880101"
            mdtTran.Columns.Add("ishighestqualification", Type.GetType("System.Boolean")).DefaultValue = False
            'Nilay (13 Apr 2017) -- End


            dCol = New DataColumn("remark")
            dCol.DataType = System.Type.GetType("System.String")
            mdtTran.Columns.Add(dCol)


            dCol = New DataColumn("AUD")
            dCol.DataType = System.Type.GetType("System.String")
            dCol.AllowDBNull = True
            dCol.DefaultValue = DBNull.Value
            mdtTran.Columns.Add(dCol)

            dCol = New DataColumn("GUID")
            dCol.DataType = System.Type.GetType("System.String")
            mdtTran.Columns.Add(dCol)

            'Sohail (26 May 2017) -- Start
            'Issue - 67.1 - applicants details gets removed if he changes after data is imported.
            mdtTran.Columns.Add("serverqualificationtranunkid", System.Type.GetType("System.Int32")).DefaultValue = 0
            mdtTran.Columns.Add("isvoid", System.Type.GetType("System.Boolean")).DefaultValue = False
            mdtTran.Columns.Add("voiduserunkid", System.Type.GetType("System.Int32")).DefaultValue = 0
            mdtTran.Columns.Add("voiddatetime", System.Type.GetType("System.DateTime")).DefaultValue = Nothing
            mdtTran.Columns.Add("voidreason", System.Type.GetType("System.String")).DefaultValue = ""
            'Sohail (26 May 2017) -- End

            'Sohail (19 Mar 2020) -- Start
            'NMB Enhancement # : Separate other option for qualification group, qualification, result and institute for applicant in recruitment.
            mdtTran.Columns.Add("qualificationgrp", System.Type.GetType("System.String")).DefaultValue = ""
            mdtTran.Columns.Add("qualification", System.Type.GetType("System.String")).DefaultValue = ""
            mdtTran.Columns.Add("institute", System.Type.GetType("System.String")).DefaultValue = ""
            mdtTran.Columns.Add("resultcode", System.Type.GetType("System.String")).DefaultValue = ""
            'Sohail (19 Mar 2020) -- End

            'Hemant (11 Apr 2022) -- Start            
            'ISSUE/ENHANCEMENT(ZRA) : the updated information gets updated across all the vacancies including the closed ones. Only open vacancies should be updated with the new information
            dCol = New DataColumn("created_date")
            dCol.DataType = System.Type.GetType("System.DateTime")
            mdtTran.Columns.Add(dCol)
            'Hemant (11 Apr 2022) -- End

        Catch ex As Exception
            'DisplayError.Show("-1", ex.Message, "New", mstrModuleName)
            Throw New Exception(ex.Message & "; Procedure Name: New; Module Name: " & mstrModuleName)
        End Try
    End Sub
#End Region

#Region " Private Methods "
    'Sohail (27 Apr 2017) -- Start
    'TRA Enhancement - 66.1 - Online Recruitment Redesign changes for tra security issues.
    'Private Sub GetApplicantQualification_tran()
    Private Sub GetApplicantQualification_tran(ByVal intApplicantQualificationSortBy As Integer)
        'Sohail (27 Apr 2017) -- End
        Dim strQ As String = ""
        Dim dsList As DataSet = Nothing
        Dim dRowQ_tran As DataRow
        Dim exForce As Exception
        Try
            objDataOperation = New clsDataOperation


            'Pinkal (12-Oct-2011) -- Start
            'Enhancement : TRA Changes

            'strQ = "SELECT " & _
            '            "  qualificationtranunkid " & _
            '            ", applicantunkid " & _
            '            ", qualificationgroupunkid " & _
            '            ", qualificationunkid " & _
            '            ", transaction_date " & _
            '            ", reference_no " & _
            '            ", award_start_date " & _
            '            ", award_end_date " & _
            '            ", instituteunkid " & _
            '            ", remark " & _
            '            ", '' As AUD " & _
            '          "FROM rcapplicantqualification_tran " & _
            '          "WHERE applicantunkid = @applicantunkid "

            strQ = "SELECT " & _
                        "  qualificationtranunkid " & _
                        ", applicantunkid " & _
                        ", rcapplicantqualification_tran.qualificationgroupunkid " & _
                        ", ISNULL(cfcommon_master.name, ' ') AS qualificationgrp " & _
                        ", ISNULL(cfcommon_master.qlevel, -1) AS qlevel " & _
                        ", rcapplicantqualification_tran.qualificationunkid " & _
                        ", ISNULL(hrqualification_master.qualificationname, ' ') AS qualification " & _
                        ", transaction_date " & _
                        ", reference_no " & _
                        ", award_start_date " & _
                        ", award_end_date " & _
                        ", YEAR(award_start_date) award_start_Year " & _
                        ", YEAR(award_end_date) award_end_Year " & _
                        ", rcapplicantqualification_tran.instituteunkid " & _
                        ", ISNULL(hrinstitute_master.institute_name, '') AS institute " & _
                    ", ISNULL(rcapplicantqualification_tran.resultunkid,0) resultunkid " & _
                        ", ISNULL(hrresult_master.resultname, '') AS resultcode " & _
                        ", ISNULL(result_level,0) result_level  " & _
                    ", ISNULL(gpacode,0.00)  gpacode " & _
                        ", remark " & _
                        ", ISNULL(other_qualificationgrp,'') other_qualificationgrp " & _
                        ", ISNULL(other_qualification,'') other_qualification " & _
                        ", ISNULL(other_institute,'') other_institute " & _
                        ", ISNULL(other_resultcode,'') other_resultcode " & _
                        ", ISNULL(certificateno,'') certificateno " & _
                        ", ishighestqualification " & _
                        ", ISNULL(CONVERT(CHAR(8), award_end_date, 112), '88880101') AS award_end_date_string " & _
                        ", '' As AUD " & _
                        ", ISNULL(rcapplicantqualification_tran.serverqualificationtranunkid, 0) AS serverqualificationtranunkid " & _
                        ", rcapplicantqualification_tran.isvoid " & _
                        ", rcapplicantqualification_tran.voiduserunkid " & _
                        ", rcapplicantqualification_tran.voiddatetime " & _
                        ", rcapplicantqualification_tran.voidreason " & _
                        ", rcapplicantqualification_tran.created_date " & _
                      "FROM rcapplicantqualification_tran " & _
                        "LEFT JOIN cfcommon_master ON rcapplicantqualification_tran.qualificationgroupunkid = cfcommon_master.masterunkid AND cfcommon_master.mastertype = " & clsCommon_Master.enCommonMaster.QUALIFICATION_COURSE_GROUP & " " & _
                        "LEFT JOIN hrqualification_master ON rcapplicantqualification_tran.qualificationunkid = hrqualification_master.qualificationunkid " & _
                  " LEFT JOIN hrresult_master ON hrresult_master.resultunkid = rcapplicantqualification_tran.resultunkid " & _
                        "LEFT JOIN hrinstitute_master ON rcapplicantqualification_tran.instituteunkid = hrinstitute_master.instituteunkid " & _
                      "WHERE ISNULL(rcapplicantqualification_tran.isvoid, 0) = 0 AND applicantunkid = @applicantunkid "
            'Hemant (11 Apr 2022) -- [rcapplicantqualification_tran.created_date]
            'Sohail (21 Apr 2020) - [qlevel]
            'Sohail (19 Mar 2020) - [LEFT JOIN cfcommon_master, hrqualification_master, hrinstitute_master]
            'Sohail (26 May 2017) - [serverqualificationtranunkid, isvoid, voiduserunkid, voiddatetime, voidreason]
            'Nilay (13 Apr 2017) -- [ishighestqualification, award_end_date_string]

            'Sohail (22 Apr 2015) - [certificateno]
            'Pinkal (12-Oct-2011) -- End

            'Sohail (02 May 2022) -- Start
            'Enhancement :  AC2-312 / AC2-327 : ZRA - Newly added qualifications should not populate on old job applications for closed vacancies on applicant master.
            If mdtVacancyEndDate <> Nothing Then
                strQ &= " AND ISNULL(CONVERT(CHAR(8), rcapplicantqualification_tran.created_date ,112), '19000101') <= '" & eZeeDate.convertDate(mdtVacancyEndDate) & "' "
            End If
            'Sohail (02 May 2022) -- End

            'Sohail (27 Apr 2017) -- Start
            'TRA Enhancement - 66.1 - Online Recruitment Redesign changes for tra security issues.
            strQ &= "ORDER BY CASE @appqualisortbyid WHEN " & enApplicantQualificationSortBy.HighestQualification & " THEN ISNULL(ishighestqualification, 0)  ELSE ISNULL(rcapplicantqualification_tran.award_end_date, '88880101') END DESC " & _
                                ", ISNULL(rcapplicantqualification_tran.award_end_date, '88880101')  DESC "
            'Sohail (27 Apr 2017) -- End

            objDataOperation.AddParameter("@applicantunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintApplicantunkid.ToString)
            objDataOperation.AddParameter("@appqualisortbyid", SqlDbType.Int, eZeeDataType.INT_SIZE, intApplicantQualificationSortBy.ToString)

            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            mdtTran.Rows.Clear()

            For i As Integer = 0 To dsList.Tables(0).Rows.Count - 1
                With dsList.Tables(0).Rows(i)
                    dRowQ_tran = mdtTran.NewRow

                    dRowQ_tran.Item("qualificationtranunkid") = .Item("qualificationtranunkid")
                    dRowQ_tran.Item("applicantunkid") = .Item("applicantunkid")
                    dRowQ_tran.Item("qualificationgroupunkid") = .Item("qualificationgroupunkid")
                    'Sohail (21 Apr 2020) -- Start
                    'NMB Enhancement # : On shortlisting screen, the screen is returning false results when AND condition is used and combined with other different parameters. E.g qualification group AND Nationality.
                    dRowQ_tran.Item("qlevel") = .Item("qlevel")
                    'Sohail (21 Apr 2020) -- End
                    dRowQ_tran.Item("qualificationunkid") = .Item("qualificationunkid")
                    dRowQ_tran.Item("transaction_date") = .Item("transaction_date")
                    dRowQ_tran.Item("reference_no") = .Item("reference_no")
                    dRowQ_tran.Item("award_start_date") = .Item("award_start_date")
                    dRowQ_tran.Item("award_end_date") = .Item("award_end_date")
                    dRowQ_tran.Item("instituteunkid") = .Item("instituteunkid")
                    dRowQ_tran.Item("remark") = .Item("remark")
                    dRowQ_tran.Item("AUD") = .Item("AUD")


                    'Pinkal (12-Oct-2011) -- Start
                    'Enhancement : TRA Changes
                    dRowQ_tran("resultunkid") = .Item("resultunkid")
                    dRowQ_tran("result_level") = .Item("result_level")
                    dRowQ_tran("gpacode") = .Item("gpacode")
                    'Pinkal (12-Oct-2011) -- End


                    'Pinkal (06-Feb-2012) -- Start
                    'Enhancement : TRA Changes

                    dRowQ_tran("award_start_Year") = .Item("award_start_Year")
                    dRowQ_tran("award_end_Year") = .Item("award_end_Year")
                    dRowQ_tran("other_qualificationgrp") = .Item("other_qualificationgrp")
                    dRowQ_tran("other_qualification") = .Item("other_qualification")
                    dRowQ_tran("other_institute") = .Item("other_institute")
                    dRowQ_tran("other_resultcode") = .Item("other_resultcode")
                    'Pinkal (06-Feb-2012) -- End
                    dRowQ_tran("certificateno") = .Item("certificateno") 'Sohail (22 Apr 2015)

                    'Nilay (13 Apr 2017) -- Start
                    dRowQ_tran("award_end_date_string") = .Item("award_end_date_string")
                    dRowQ_tran("ishighestqualification") = .Item("ishighestqualification")
                    'Nilay (13 Apr 2017) -- End
                    'Sohail (26 May 2017) -- Start
                    'Issue - 67.1 - applicants details gets removed if he changes after data is imported.
                    dRowQ_tran("serverqualificationtranunkid") = .Item("serverqualificationtranunkid")
                    dRowQ_tran("isvoid") = .Item("isvoid")
                    dRowQ_tran("voiduserunkid") = .Item("voiduserunkid")
                    If IsDBNull(.Item("voiddatetime")) = True Then
                        dRowQ_tran("voiddatetime") = DBNull.Value
                    Else
                        dRowQ_tran("voiddatetime") = .Item("voiddatetime")
                    End If
                    dRowQ_tran("voidreason") = .Item("voidreason")
                    'Sohail (26 May 2017) -- End

                    'Sohail (19 Mar 2020) -- Start
                    'NMB Enhancement # : Separate other option for qualification group, qualification, result and institute for applicant in recruitment.
                    dRowQ_tran("qualificationgrp") = .Item("qualificationgrp")
                    dRowQ_tran("qualification") = .Item("qualification")
                    dRowQ_tran("institute") = .Item("institute")
                    dRowQ_tran("resultcode") = .Item("resultcode")
                    'Sohail (19 Mar 2020) -- End
                    'Hemant (11 Apr 2022) -- Start            
                    'ISSUE/ENHANCEMENT(ZRA) : the updated information gets updated across all the vacancies including the closed ones. Only open vacancies should be updated with the new information
                    dRowQ_tran.Item("created_date") = .Item("created_date")
                    'Hemant (11 Apr 2022) -- End
                    mdtTran.Rows.Add(dRowQ_tran)
                End With
            Next

        Catch ex As Exception
            'DisplayError.Show("-1", ex.Message, "GetApplicantQualification_tran", mstrModuleName)
            Throw New Exception(ex.Message & "; Procedure Name: GetApplicantQualification_tran; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
    End Sub

    Public Function InsertUpdateDelete_QualificationTran() As Boolean
        Dim i As Integer = 0
        Dim strQ As String = ""
        Dim strErrorMessage As String = ""
        Dim exForce As Exception
        Try
            objDataOperation = New clsDataOperation
            objDataOperation.BindTransaction()
            For i = 0 To mdtTran.Rows.Count - 1
                With mdtTran.Rows(i)
                    objDataOperation.ClearParameters()
                    If Not IsDBNull(.Item("AUD")) Then
                        strQ = "" 'Sohail (26 May 2017)

                        Select Case .Item("AUD")
                            Case "A"

                                'Sohail (27 Apr 2017) -- Start
                                'TRA Enhancement - 66.1 - Online Recruitment Redesign changes for tra security issues.
                                If CBool(.Item("ishighestqualification").ToString) = True Then
                                    strQ = "UPDATE rcapplicantqualification_tran SET ishighestqualification = 0 WHERE applicantunkid = @applicantunkid "
                                End If
                                    'Sohail (27 Apr 2017) -- End

                                'Pinkal (12-Oct-2011) -- Start
                                'Enhancement : TRA Changes

                                'strQ = "INSERT INTO rcapplicantqualification_tran ( " & _
                                '                "  applicantunkid " & _
                                '                ", qualificationgroupunkid " & _
                                '                ", qualificationunkid " & _
                                '                ", transaction_date " & _
                                '                ", reference_no " & _
                                '                ", award_start_date " & _
                                '                ", award_end_date " & _
                                '                ", instituteunkid " & _
                                '                ", remark" & _
                                '          ") VALUES (" & _
                                '                "  @applicantunkid " & _
                                '                ", @qualificationgroupunkid " & _
                                '                ", @qualificationunkid " & _
                                '                ", @transaction_date " & _
                                '                ", @reference_no " & _
                                '                ", @award_start_date " & _
                                '                ", @award_end_date " & _
                                '                ", @instituteunkid " & _
                                '                ", @remark" & _
                                '          "); SELECT @@identity"


                                    strQ &= "; INSERT INTO rcapplicantqualification_tran ( " & _
                                                "  applicantunkid " & _
                                                ", qualificationgroupunkid " & _
                                                ", qualificationunkid " & _
                                                ", transaction_date " & _
                                                ", reference_no " & _
                                                ", award_start_date " & _
                                                ", award_end_date " & _
                                                ", instituteunkid " & _
                                               ", resultunkid " & _
                                               ", gpacode " & _
                                                ", remark" & _
                                               ", other_qualificationgrp " & _
                                               ", other_qualification " & _
                                               ", other_institute " & _
                                               ", other_resultcode " & _
                                               ", certificateno " & _
                                                   ", ishighestqualification " & _
                                        ", serverqualificationtranunkid" & _
                                        ", isvoid" & _
                                        ", voiduserunkid" & _
                                        ", voiddatetime" & _
                                        ", voidreason" & _
                                            ", created_date " & _
                                          ") VALUES (" & _
                                                "  @applicantunkid " & _
                                                ", @qualificationgroupunkid " & _
                                                ", @qualificationunkid " & _
                                                ", @transaction_date " & _
                                                ", @reference_no " & _
                                                ", @award_start_date " & _
                                                ", @award_end_date " & _
                                                ", @instituteunkid " & _
                                               ", @resultunkid " & _
                                               ", @gpacode " & _
                                                ", @remark" & _
                                                ", @other_qualificationgrp " & _
                                                ", @other_qualification " & _
                                                ", @other_institute " & _
                                                ", @other_resultcode " & _
                                                ", @certificateno " & _
                                                    ", @ishighestqualification " & _
                                        ", @serverqualificationtranunkid" & _
                                        ", @isvoid" & _
                                        ", @voiduserunkid" & _
                                        ", @voiddatetime" & _
                                        ", @voidreason" & _
                                            ", @created_date " & _
                                          "); SELECT @@identity"
                                'Hemant (11 Apr 2022) -- [created_date]
                                'Sohail (26 May 2017) - [serverqualificationtranunkid, isvoid, voiduserunkid, voiddatetime, voidreason]
                                    'Nilay (13 Apr 2017) -- [ishighestqualification]
                                'Sohail (22 Apr 2015) - [certificateno]


                                objDataOperation.ClearParameters()
                                objDataOperation.AddParameter("@resultunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, .Item("resultunkid").ToString)
                                objDataOperation.AddParameter("@gpacode", SqlDbType.Decimal, eZeeDataType.DECIMAL_SIZE, .Item("gpacode").ToString)
                                'Pinkal (12-Oct-2011) -- End

                                objDataOperation.AddParameter("@applicantunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintApplicantunkid.ToString)
                                objDataOperation.AddParameter("@qualificationgroupunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, .Item("qualificationgroupunkid").ToString)
                                objDataOperation.AddParameter("@qualificationunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, .Item("qualificationunkid").ToString)
                                objDataOperation.AddParameter("@transaction_date", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, .Item("transaction_date").ToString)
                                objDataOperation.AddParameter("@reference_no", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, .Item("reference_no").ToString)
                                If IsDBNull(.Item("award_start_date")) Then
                                    objDataOperation.AddParameter("@award_start_date", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, DBNull.Value)
                                Else
                                    objDataOperation.AddParameter("@award_start_date", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, .Item("award_start_date").ToString)
                                End If
                                If IsDBNull(.Item("award_end_date")) Then
                                    objDataOperation.AddParameter("@award_end_date", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, DBNull.Value)
                                Else
                                    objDataOperation.AddParameter("@award_end_date", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, .Item("award_end_date").ToString)
                                End If
                                objDataOperation.AddParameter("@instituteunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, .Item("instituteunkid").ToString)
                                objDataOperation.AddParameter("@remark", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, .Item("remark").ToString)



                                'Pinkal (06-Feb-2012) -- Start
                                'Enhancement : TRA Changes
                                objDataOperation.AddParameter("@other_qualificationgrp", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, .Item("other_qualificationgrp").ToString)
                                objDataOperation.AddParameter("@other_qualification", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, .Item("other_qualification").ToString)
                                objDataOperation.AddParameter("@other_institute", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, .Item("other_institute").ToString)
                                objDataOperation.AddParameter("@other_resultcode", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, .Item("other_resultcode").ToString)
                                'Pinkal (06-Feb-2012) -- End
                                objDataOperation.AddParameter("@certificateno", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, .Item("certificateno").ToString) 'Sohail (22 Apr 2015)
                                    'Nilay (13 Apr 2017) -- Start
                                    objDataOperation.AddParameter("@ishighestqualification", SqlDbType.Bit, eZeeDataType.BIT_SIZE, CBool(.Item("ishighestqualification").ToString))
                                    'Nilay (13 Apr 2017) -- End


                                'Sohail (26 May 2017) -- Start
                                'Issue - 67.1 - applicants details gets removed if he changes after data is imported.
                                objDataOperation.AddParameter("@serverqualificationtranunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, .Item("serverqualificationtranunkid"))
                                objDataOperation.AddParameter("@isvoid", SqlDbType.Bit, eZeeDataType.BIT_SIZE, .Item("isvoid"))
                                objDataOperation.AddParameter("@voiduserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, .Item("voiduserunkid"))
                                If IsDBNull(.Item("voiddatetime")) = True Then
                                    objDataOperation.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, DBNull.Value)
                                Else
                                    objDataOperation.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, .Item("voiddatetime"))
                                End If
                                objDataOperation.AddParameter("@voidreason", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, .Item("voidreason").ToString)
                                'Sohail (26 May 2017) -- End
                                'Hemant (11 Apr 2022) -- Start            
                                'ISSUE/ENHANCEMENT(ZRA) : the updated information gets updated across all the vacancies including the closed ones. Only open vacancies should be updated with the new information
                                objDataOperation.AddParameter("@created_date", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, .Item("created_date").ToString)
                                'Hemant (11 Apr 2022) -- End
                                'Anjan (12 Oct 2011)-Start
                                'ENHANCEMENT : AUDIT TRAIL MAINTENANCE.
                                'objDataOperation.ExecNonQuery(strQ)
                                Dim dsList As DataSet = objDataOperation.ExecQuery(strQ, "List")
                                'Anjan (12 Oct 2011)-End 

                                If objDataOperation.ErrorMessage <> "" Then
                                    exForce = New Exception(objDataOperation.ErrorNumber & " : " & objDataOperation.ErrorMessage)
                                    Throw exForce
                                End If

                                'Anjan (12 Oct 2011)-Start
                                'ENHANCEMENT : AUDIT TRAIL MAINTENANCE.

                                mintQualificationtranunkid = dsList.Tables(0).Rows(0)(0)

                                If .Item("applicantunkid") > 0 Then
'S.SANDEEP [28-May-2018] -- START
'ISSUE/ENHANCEMENT : {Audit Trails} 
Dim objCommonATLog AS New clsCommonATLog 
objCommonATLog._FormName = mstrFormName 
objCommonATLog._LoginEmployeeUnkid = mintLoginEmployeeunkid 
                                    objCommonATLog._ClientIP = mstrClientIP
                                    objCommonATLog._HostName = mstrHostName
                                    objCommonATLog._FromWeb = mblnIsWeb
                                    objCommonATLog._AuditUserId = mintAuditUserId
objCommonATLog._CompanyUnkid = mintCompanyUnkid
                                    objCommonATLog._AuditDate = mdtAuditDate
                                    'S.SANDEEP [28-May-2018] -- END
                                    'Sohail (09 Oct 2018) -- Start
                                    'TANAPA - Support Issue Id # 2502 - linking recruitment server app_vacancy with aruti desktop app_vacancy in 75.1.
                                    'If clsCommonATLog.Insert_TranAtLog(objDataOperation, "rcapplicant_master", "applicantunkid", .Item("applicantunkid"), "rcapplicantqualification_tran", "qualificationtranunkid", mintQualificationtranunkid, 2, 1) = False Then
                                    If objCommonATLog.Insert_TranAtLog(objDataOperation, "rcapplicant_master", "applicantunkid", mintApplicantunkid, "rcapplicantqualification_tran", "qualificationtranunkid", mintQualificationtranunkid, 2, 1) = False Then
                                        'Sohail (09 Oct 2018) -- End
                                        exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                                        Throw exForce
                                    End If

                                    'S.SANDEEP [28-May-2018] -- START
                                    'ISSUE/ENHANCEMENT : {Audit Trails} 
                                    objCommonATLog = Nothing
                                    'S.SANDEEP [28-May-2018] -- END

                                Else
                                    'S.SANDEEP [28-May-2018] -- START
                                    'ISSUE/ENHANCEMENT : {Audit Trails} 
                                    Dim objCommonATLog As New clsCommonATLog
                                    objCommonATLog._FormName = mstrFormName
                                    objCommonATLog._LoginEmployeeUnkid = mintLoginEmployeeunkid
                                    objCommonATLog._ClientIP = mstrClientIP
                                    objCommonATLog._HostName = mstrHostName
                                    objCommonATLog._FromWeb = mblnIsWeb
                                    objCommonATLog._AuditUserId = mintAuditUserId
objCommonATLog._CompanyUnkid = mintCompanyUnkid
                                    objCommonATLog._AuditDate = mdtAuditDate
                                    'S.SANDEEP [28-May-2018] -- END

                                    If objCommonATLog.Insert_TranAtLog(objDataOperation, "rcapplicant_master", "applicantunkid", mintApplicantunkid, "rcapplicantqualification_tran", "qualificationtranunkid", mintQualificationtranunkid, 1, 1) = False Then
                                        exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                                        Throw exForce
                                    End If

                                    'S.SANDEEP [28-May-2018] -- START
                                    'ISSUE/ENHANCEMENT : {Audit Trails} 
                                    objCommonATLog = Nothing
                                    'S.SANDEEP [28-May-2018] -- END

                                End If

                            Case "U"

                                    'Sohail (27 Apr 2017) -- Start
                                    'TRA Enhancement - 66.1 - Online Recruitment Redesign changes for tra security issues.
                                    If CBool(.Item("ishighestqualification").ToString) = True Then
                                    strQ = "UPDATE rcapplicantqualification_tran SET ishighestqualification = 0 WHERE applicantunkid = @applicantunkid "
                                End If
                                    'Sohail (27 Apr 2017) -- End

                                'Pinkal (12-Oct-2011) -- Start
                                'Enhancement : TRA Changes

                                'strQ = "UPDATE rcapplicantqualification_tran SET " & _
                                '                "  applicantunkid = @applicantunkid" & _
                                '                ", qualificationgroupunkid = @qualificationgroupunkid" & _
                                '                ", qualificationunkid = @qualificationunkid" & _
                                '                ", transaction_date = @transaction_date" & _
                                '                ", reference_no = @reference_no" & _
                                '                ", award_start_date = @award_start_date" & _
                                '                ", award_end_date = @award_end_date" & _
                                '                ", instituteunkid = @instituteunkid" & _
                                '                ", remark = @remark " & _
                                '          "WHERE qualificationtranunkid = @qualificationtranunkid "

                                    strQ &= "; UPDATE rcapplicantqualification_tran SET " & _
                                                "  applicantunkid = @applicantunkid" & _
                                                ", qualificationgroupunkid = @qualificationgroupunkid" & _
                                                ", qualificationunkid = @qualificationunkid" & _
                                                ", transaction_date = @transaction_date" & _
                                                ", reference_no = @reference_no" & _
                                                ", award_start_date = @award_start_date" & _
                                                ", award_end_date = @award_end_date" & _
                                                ", instituteunkid = @instituteunkid" & _
                                               ", resultunkid = @resultunkid " & _
                                               ", gpacode = @gpacode " & _
                                                ", remark = @remark " & _
                                                ", other_qualificationgrp = @other_qualificationgrp " & _
                                                ", other_qualification = @other_qualification " & _
                                                ", other_institute =  @other_institute " & _
                                                ", other_resultcode = @other_resultcode " & _
                                                ", certificateno = @certificateno " & _
                                                    ", ishighestqualification = @ishighestqualification " & _
                                        ", serverqualificationtranunkid=@serverqualificationtranunkid" & _
                                        ", isvoid = @isvoid" & _
                                        ", voiduserunkid = @voiduserunkid" & _
                                        ", voiddatetime = @voiddatetime" & _
                                        ", voidreason = @voidreason " & _
                                          "WHERE qualificationtranunkid = @qualificationtranunkid "
                                'Sohail (26 May 2017) - [serverqualificationtranunkid, isvoid, voiduserunkid, voiddatetime, voidreason]
                                'Sohail (22 Apr 2015) - [certificateno]

                                objDataOperation.ClearParameters()
                                objDataOperation.AddParameter("@resultunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, .Item("resultunkid").ToString)
                                objDataOperation.AddParameter("@gpacode", SqlDbType.Decimal, eZeeDataType.DECIMAL_SIZE, .Item("gpacode").ToString)

                                'Pinkal (12-Oct-2011) -- End

                                objDataOperation.AddParameter("@qualificationtranunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, .Item("qualificationtranunkid").ToString)
                                objDataOperation.AddParameter("@applicantunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, .Item("applicantunkid").ToString)
                                objDataOperation.AddParameter("@qualificationgroupunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, .Item("qualificationgroupunkid").ToString)
                                objDataOperation.AddParameter("@qualificationunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, .Item("qualificationunkid").ToString)
                                objDataOperation.AddParameter("@transaction_date", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, .Item("transaction_date").ToString)
                                objDataOperation.AddParameter("@reference_no", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, .Item("reference_no").ToString)
                                If IsDBNull(.Item("award_start_date")) Then
                                    objDataOperation.AddParameter("@award_start_date", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, DBNull.Value)
                                Else
                                    objDataOperation.AddParameter("@award_start_date", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, .Item("award_start_date").ToString)
                                End If
                                If IsDBNull(.Item("award_end_date")) Then
                                    objDataOperation.AddParameter("@award_end_date", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, DBNull.Value)
                                Else
                                    objDataOperation.AddParameter("@award_end_date", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, .Item("award_end_date").ToString)
                                End If
                                objDataOperation.AddParameter("@instituteunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, .Item("instituteunkid").ToString)
                                objDataOperation.AddParameter("@remark", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, .Item("remark").ToString)

                                'Pinkal (06-Feb-2012) -- Start
                                'Enhancement : TRA Changes
                                objDataOperation.AddParameter("@other_qualificationgrp", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, .Item("other_qualificationgrp").ToString)
                                objDataOperation.AddParameter("@other_qualification", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, .Item("other_qualification").ToString)
                                objDataOperation.AddParameter("@other_institute", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, .Item("other_institute").ToString)
                                objDataOperation.AddParameter("@other_resultcode", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, .Item("other_resultcode").ToString)
                                'Pinkal (06-Feb-2012) -- End
                                objDataOperation.AddParameter("@certificateno", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, .Item("certificateno").ToString) 'Sohail (22 Apr 2015)
                                    'Nilay (13 Apr 2017) -- Start
                                    objDataOperation.AddParameter("@ishighestqualification", SqlDbType.Bit, eZeeDataType.BIT_SIZE, .Item("ishighestqualification").ToString)
                                    'Nilay (13 Apr 2017) -- End

                                'Sohail (26 May 2017) -- Start
                                'Issue - 67.1 - applicants details gets removed if he changes after data is imported.
                                objDataOperation.AddParameter("@serverqualificationtranunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, .Item("serverqualificationtranunkid"))
                                objDataOperation.AddParameter("@isvoid", SqlDbType.Bit, eZeeDataType.BIT_SIZE, .Item("isvoid"))
                                objDataOperation.AddParameter("@voiduserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, .Item("voiduserunkid"))
                                If IsDBNull(.Item("voiddatetime")) = True Then
                                    objDataOperation.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, DBNull.Value)
                                Else
                                    objDataOperation.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, .Item("voiddatetime"))
                                End If
                                objDataOperation.AddParameter("@voidreason", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, .Item("voidreason").ToString)
                                'Sohail (26 May 2017) -- End

                                objDataOperation.ExecNonQuery(strQ)

                                If objDataOperation.ErrorMessage <> "" Then
                                    exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                                    Throw exForce
                                End If


                                'S.SANDEEP [28-May-2018] -- START
                                'ISSUE/ENHANCEMENT : {Audit Trails} 
                                Dim objCommonATLog As New clsCommonATLog
                                objCommonATLog._FormName = mstrFormName
                                objCommonATLog._LoginEmployeeUnkid = mintLoginEmployeeunkid
                                objCommonATLog._ClientIP = mstrClientIP
                                objCommonATLog._HostName = mstrHostName
                                objCommonATLog._FromWeb = mblnIsWeb
                                objCommonATLog._AuditUserId = mintAuditUserId
objCommonATLog._CompanyUnkid = mintCompanyUnkid
                                objCommonATLog._AuditDate = mdtAuditDate
                                'S.SANDEEP [28-May-2018] -- END

                                If objCommonATLog.Insert_TranAtLog(objDataOperation, "rcapplicant_master", "applicantunkid", .Item("applicantunkid"), "rcapplicantqualification_tran", "qualificationtranunkid", .Item("qualificationtranunkid"), 2, 2) = False Then
                                    exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                                    Throw exForce
                                End If

                                'S.SANDEEP [28-May-2018] -- START
                                'ISSUE/ENHANCEMENT : {Audit Trails} 
                                objCommonATLog = Nothing
                                'S.SANDEEP [28-May-2018] -- END



                            Case "D"

                                If .Item("qualificationtranunkid") > 0 Then
                                    'S.SANDEEP [28-May-2018] -- START
                                    'ISSUE/ENHANCEMENT : {Audit Trails} 
                                    Dim objCommonATLog As New clsCommonATLog
                                    objCommonATLog._FormName = mstrFormName
                                    objCommonATLog._LoginEmployeeUnkid = mintLoginEmployeeunkid
                                    objCommonATLog._ClientIP = mstrClientIP
                                    objCommonATLog._HostName = mstrHostName
                                    objCommonATLog._FromWeb = mblnIsWeb
                                    objCommonATLog._AuditUserId = mintAuditUserId
objCommonATLog._CompanyUnkid = mintCompanyUnkid
                                    objCommonATLog._AuditDate = mdtAuditDate
                                    'S.SANDEEP [28-May-2018] -- END

                                    If objCommonATLog.Insert_TranAtLog(objDataOperation, "rcapplicant_master", "applicantunkid", .Item("applicantunkid"), "rcapplicantqualification_tran", "qualificationtranunkid", .Item("qualificationtranunkid"), 2, 3) = False Then
                                        exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                                        Throw exForce
                                    End If

                                    'S.SANDEEP [28-May-2018] -- START
                                    'ISSUE/ENHANCEMENT : {Audit Trails} 
                                    objCommonATLog = Nothing
                                    'S.SANDEEP [28-May-2018] -- END

                                End If
                                'Anjan (12 Oct 2011)-End 


                                strQ = "DELETE FROM rcapplicantqualification_tran " & _
                                        "WHERE qualificationtranunkid = @qualificationtranunkid "

                                objDataOperation.AddParameter("@qualificationtranunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, .Item("qualificationtranunkid").ToString)

                                Call objDataOperation.ExecNonQuery(strQ)

                                If objDataOperation.ErrorMessage <> "" Then
                                    exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                                    Throw exForce
                                End If
                        End Select
                    End If
                End With
            Next

            objDataOperation.ReleaseTransaction(True)

            Return True

        Catch ex As Exception
            objDataOperation.ReleaseTransaction(False)
            'DisplayError.Show("-1", ex.Message, "InsertUpdateDelete_QualificationTran", mstrModuleName)
            Throw New Exception(ex.Message & "; Procedure Name: InsertUpdateDelete_QualificationTran; Module Name: " & mstrModuleName)
            Return False
        End Try
    End Function



    'Pinkal (12-Oct-2011) -- Start
    'Enhancement : TRA Changes

    ' DON'T CHANGE THIS METHOD 

    Public Function GetApplicantQualification(ByVal blnOtherQualification As Boolean, ByVal intApplicantunkid As Integer, ByVal dtVacancyEndDate As DateTime) As DataSet
        'Hemant (11 Apr 2022) -- [dtVacancyEndDate]
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""

        Try

            Dim objDataOperation As New clsDataOperation

            If blnOtherQualification Then

                strQ = "SELECT    " & _
                          " ISNULL(STUFF(( SELECT DISTINCT CASE WHEN other_qualificationgrp <> '' THEN  ',' + CAST(other_qualificationgrp AS VARCHAR(max)) ELSE  " & _
                              " CAST(other_qualificationgrp AS VARCHAR(max)) END " & _
                              " FROM rcapplicantqualification_tran " & _
                              " WHERE rcapplicantqualification_tran.applicantunkid =  @applicantunkid AND rcapplicantqualification_tran.isvoid = 0 " & _
                              " AND rcapplicantqualification_tran.created_date <= '" & eZeeDate.convertDate(dtVacancyEndDate) & "'  " & _
                              " FOR  XML PATH('')), 1, 1, ''),'') AS QualificationGrp, " & _
                        " ISNULL(STUFF(( SELECT DISTINCT CASE WHEN other_qualification <> '' THEN ',' + CAST(other_qualification AS VARCHAR(max)) ELSE " & _
                            " CAST(other_qualification AS VARCHAR(max)) END   " & _
                            " FROM rcapplicantqualification_tran " & _
                            " WHERE rcapplicantqualification_tran.applicantunkid = @applicantunkid  AND rcapplicantqualification_tran.isvoid = 0 " & _
                            " AND rcapplicantqualification_tran.created_date <= '" & eZeeDate.convertDate(dtVacancyEndDate) & "'  " & _
                            " FOR  XML PATH('')), 1, 1, ''),'')  AS Qualification," & _
                        " ISNULL(STUFF(( SELECT  ',' + CAST(rcapplicantqualification_tran.gpacode AS VARCHAR(max))  " & _
                            " FROM rcapplicantqualification_tran  " & _
                            " WHERE rcapplicantqualification_tran.applicantunkid = @applicantunkid  AND rcapplicantqualification_tran.isvoid = 0 " & _
                            " AND rcapplicantqualification_tran.created_date <= '" & eZeeDate.convertDate(dtVacancyEndDate) & "'  " & _
                            " FOR  XML PATH('')), 1, 1, ''),'')  AS gpacode "
                'Hemant (11 Apr 2022) -- [" AND rcapplicantqualification_tran.created_date <= '" & eZeeDate.convertDate(dtVacancyEndDate) & "'  " & _]


            Else

            strQ = "SELECT    " & _
                      " ISNULL(STUFF(( SELECT  ',' + CAST(cfcommon_master.name AS VARCHAR(max)) " & _
                          " FROM rcapplicantqualification_tran " & _
                          " JOIN cfcommon_master ON cfcommon_master.masterunkid = rcapplicantqualification_tran.qualificationgroupunkid AND dbo.cfcommon_master.mastertype =" & clsCommon_Master.enCommonMaster.QUALIFICATION_COURSE_GROUP & _
                              " WHERE rcapplicantqualification_tran.applicantunkid =  @applicantunkid  AND rcapplicantqualification_tran.isvoid = 0 " & _
                                  " AND rcapplicantqualification_tran.created_date <= '" & eZeeDate.convertDate(dtVacancyEndDate) & "'  " & _
                          " ORDER BY cfcommon_master.masterunkid" & _
                          " FOR  XML PATH('')), 1, 1, ''),'') AS QualificationGrp, " & _
                    " ISNULL(STUFF(( SELECT  ',' + CAST(hrqualification_master.qualificationname AS VARCHAR(max)) " & _
                        " FROM rcapplicantqualification_tran " & _
                        " JOIN hrqualification_master ON rcapplicantqualification_tran.qualificationunkid = hrqualification_master.qualificationunkid  " & _
                            " WHERE rcapplicantqualification_tran.applicantunkid = @applicantunkid  AND rcapplicantqualification_tran.isvoid = 0 " & _
                                " AND rcapplicantqualification_tran.created_date <= '" & eZeeDate.convertDate(dtVacancyEndDate) & "'  " & _
                        " ORDER BY hrqualification_master.qualificationunkid " & _
                        " FOR  XML PATH('')), 1, 1, ''),'')  AS Qualification," & _
                    " ISNULL(STUFF(( SELECT  ',' + CAST(rcapplicantqualification_tran.gpacode AS VARCHAR(max))  " & _
                        " FROM rcapplicantqualification_tran  " & _
                                " WHERE rcapplicantqualification_tran.applicantunkid =@applicantunkid  AND rcapplicantqualification_tran.isvoid = 0 " & _
                                " AND rcapplicantqualification_tran.created_date <= '" & eZeeDate.convertDate(dtVacancyEndDate) & "'  " & _
                                " FOR  XML PATH('')), 1, 1, ''),'')  AS gpacode "
                'Hemant (11 Apr 2022) -- [" AND rcapplicantqualification_tran.created_date <= '" & eZeeDate.convertDate(dtVacancyEndDate) & "'  " & _]

            End If

            'Pinkal (06-Feb-2012) -- End


            objDataOperation.ClearParameters()
            objDataOperation.AddParameter("@applicantunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intApplicantunkid)
            dsList = objDataOperation.ExecQuery(strQ, "List")

        Catch ex As Exception
            'DisplayError.Show("-1", ex.Message, "GetApplicantQualification", mstrModuleName)
            Throw New Exception(ex.Message & "; Procedure Name: GetApplicantQualification; Module Name: " & mstrModuleName)
        End Try
        Return dsList
    End Function

    'Pinkal (12-Oct-2011) -- End


#End Region

End Class

'Public Class clsApplicantQualification_tran1
'    Private Const mstrModuleName As String = "clsApplicantQualification_tran"
'    Dim objDataOperation As clsDataOperation
'    Dim mstrMessage As String = ""

'#Region " Private Variables "
'    Private mintQualificationtranunkid As Integer
'    Private mintApplicantunkid As Integer
'    Private mdtTran As DataTable

'#End Region

'#Region " Properties "
'    Public Property _ApplicantUnkid() As Integer
'        Get
'            Return mintApplicantunkid
'        End Get
'        Set(ByVal value As Integer)
'            mintApplicantunkid = value
'            Call GetApplicantQualification_tran()
'        End Set
'    End Property

'    Public Property _DataList() As DataTable
'        Get
'            Return mdtTran
'        End Get
'        Set(ByVal value As DataTable)
'            mdtTran = value
'        End Set
'    End Property
'#End Region


'#Region " Constructor "
'    Public Sub New()
'        mdtTran = New DataTable("applicantqualificationtran")
'        Dim dCol As DataColumn

'        dCol = New DataColumn("qualificationtranunkid")
'        dCol.DataType = System.Type.GetType("System.Int32")
'        mdtTran.Columns.Add(dCol)

'        dCol = New DataColumn("applicantunkid")
'        dCol.DataType = System.Type.GetType("System.Int32")
'        mdtTran.Columns.Add(dCol)

'        dCol = New DataColumn("qualificationgroupunkid")
'        dCol.DataType = System.Type.GetType("System.Int32")
'        mdtTran.Columns.Add(dCol)

'        dCol = New DataColumn("qualificationunkid")
'        dCol.DataType = System.Type.GetType("System.Int32")
'        mdtTran.Columns.Add(dCol)

'        dCol = New DataColumn("qualificationdate")
'        dCol.DataType = System.Type.GetType("System.DateTime")
'        mdtTran.Columns.Add(dCol)

'        dCol = New DataColumn("award")
'        dCol.DataType = System.Type.GetType("System.String")
'        mdtTran.Columns.Add(dCol)

'        dCol = New DataColumn("referenceno")
'        dCol.DataType = System.Type.GetType("System.String")
'        mdtTran.Columns.Add(dCol)

'        dCol = New DataColumn("institution_name")
'        dCol.DataType = System.Type.GetType("System.String")
'        mdtTran.Columns.Add(dCol)

'        dCol = New DataColumn("address")
'        dCol.DataType = System.Type.GetType("System.String")
'        mdtTran.Columns.Add(dCol)

'        dCol = New DataColumn("contactperson")
'        dCol.DataType = System.Type.GetType("System.String")
'        mdtTran.Columns.Add(dCol)

'        dCol = New DataColumn("contactno")
'        dCol.DataType = System.Type.GetType("System.String")
'        mdtTran.Columns.Add(dCol)

'        dCol = New DataColumn("remark")
'        dCol.DataType = System.Type.GetType("System.String")
'        mdtTran.Columns.Add(dCol)


'        dCol = New DataColumn("AUD")
'        dCol.DataType = System.Type.GetType("System.String")
'        dCol.AllowDBNull = True
'        dCol.DefaultValue = DBNull.Value
'        mdtTran.Columns.Add(dCol)

'        dCol = New DataColumn("GUID")
'        dCol.DataType = System.Type.GetType("System.String")
'        mdtTran.Columns.Add(dCol)


'    End Sub
'#End Region
'#Region " Private Methods "
'    Private Sub GetApplicantQualification_tran()
'        Dim strQ As String = ""
'        Dim strErrorMessage As String = ""
'        Dim dsList As DataSet = Nothing
'        Dim dRowID_tran As DataRow
'        Dim exForce As Exception

'        Try
'            objDataOperation = New clsDataOperation
'            strQ = "SELECT " & _
'                    "  qualificationtranunkid " & _
'                    ", applicantunkid " & _
'                    ", qualificationgroupunkid " & _
'                    ", qualificationunkid " & _
'                    ", qualificationdate " & _
'                    ", award " & _
'                    ", referenceno " & _
'                    ", institution_name " & _
'                    ", address " & _
'                    ", contactperson " & _
'                    ", contactno " & _
'                    ", remark " & _
'                    ", '' AUD " & _
'                "FROM rcapplicantqualification_tran " & _
'                "WHERE applicantunkid = @applicantunkid "

'            objDataOperation.AddParameter("@applicantunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintApplicantunkid.ToString)

'            dsList = objDataOperation.ExecQuery(strQ, "List")

'            If objDataOperation.ErrorMessage <> "" Then
'                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
'                Throw exForce
'            End If

'            For i As Integer = 0 To dsList.Tables(0).Rows.Count - 1
'                With dsList.Tables(0).Rows(i)
'                    dRowID_tran = mdtTran.NewRow
'                    dRowID_tran.Item("qualificationtranunkid") = .Item("qualificationtranunkid").ToString
'                    dRowID_tran.Item("applicantunkid") = .Item("applicantunkid").ToString
'                    dRowID_tran.Item("qualificationgroupunkid") = .Item("qualificationgroupunkid").ToString
'                    dRowID_tran.Item("qualificationunkid") = .Item("qualificationunkid").ToString
'                    If IsDBNull(.Item("qualificationdate")) Then
'                        dRowID_tran.Item("qualificationdate") = DBNull.Value
'                    Else
'                        dRowID_tran.Item("qualificationdate") = .Item("qualificationdate").ToString
'                    End If

'                    dRowID_tran.Item("award") = .Item("award").ToString
'                    dRowID_tran.Item("referenceno") = .Item("referenceno").ToString
'                    dRowID_tran.Item("institution_name") = .Item("institution_name").ToString
'                    dRowID_tran.Item("address") = .Item("address").ToString
'                    dRowID_tran.Item("contactperson") = .Item("contactperson").ToString
'                    dRowID_tran.Item("contactno") = .Item("contactno").ToString
'                    dRowID_tran.Item("remark") = .Item("remark").ToString
'                    dRowID_tran.Item("AUD") = .Item("AUD").ToString
'                    mdtTran.Rows.Add(dRowID_tran)
'                End With
'            Next

'        Catch ex As Exception
'            DisplayError.Show("-1", ex.Message, "GetApplicantQualification_tran", mstrModuleName)
'        Finally
'            exForce = Nothing
'            If dsList IsNot Nothing Then dsList.Dispose()
'            objDataOperation = Nothing
'        End Try
'    End Sub

'    Public Function InsertUpdateDelete_QualificationTran() As Boolean
'        Dim i As Integer = 0
'        Dim strQ As String = ""
'        Dim strErrorMessage As String = ""
'        Dim exForce As Exception

'        Try
'            objDataOperation = New clsDataOperation
'            objDataOperation.BindTransaction()
'            For i = 0 To mdtTran.Rows.Count - 1
'                With mdtTran.Rows(i)
'                    objDataOperation.ClearParameters()
'                    If Not IsDBNull(.Item("AUD")) Then
'                        Select Case .Item("AUD")
'                            Case "A"

'                                strQ = "INSERT INTO rcapplicantqualification_tran ( " & _
'                                  "  applicantunkid " & _
'                                  ", qualificationgroupunkid " & _
'                                  ", qualificationunkid " & _
'                                  ", qualificationdate " & _
'                                  ", award " & _
'                                  ", referenceno " & _
'                                  ", institution_name " & _
'                                  ", address " & _
'                                  ", contactperson " & _
'                                  ", contactno " & _
'                                  ", remark" & _
'                                ") VALUES (" & _
'                                  "  @applicantunkid " & _
'                                  ", @qualificationgroupunkid " & _
'                                  ", @qualificationunkid " & _
'                                  ", @qualificationdate " & _
'                                  ", @award " & _
'                                  ", @referenceno " & _
'                                  ", @institution_name " & _
'                                  ", @address " & _
'                                  ", @contactperson " & _
'                                  ", @contactno " & _
'                                  ", @remark" & _
'                                "); SELECT @@identity"

'                                'Sohail (11 Sep 2010) -- Start
'                                'objDataOperation.AddParameter("@applicantunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, .Item("applicantunkid").ToString)
'                                objDataOperation.AddParameter("@applicantunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintApplicantunkid)
'                                'Sohail (11 Sep 2010) -- End
'                                objDataOperation.AddParameter("@qualificationgroupunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, .Item("qualificationgroupunkid").ToString)
'                                objDataOperation.AddParameter("@qualificationunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, .Item("qualificationunkid").ToString)
'                                If .Item("qualificationdate").ToString = Nothing Then
'                                    objDataOperation.AddParameter("@qualificationdate", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, DBNull.Value)
'                                Else
'                                    objDataOperation.AddParameter("@qualificationdate", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, .Item("qualificationdate").ToString)
'                                End If
'                                objDataOperation.AddParameter("@award", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, .Item("award").ToString)
'                                objDataOperation.AddParameter("@referenceno", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, .Item("referenceno").ToString)
'                                objDataOperation.AddParameter("@institution_name", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, .Item("institution_name").ToString)
'                                objDataOperation.AddParameter("@address", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, .Item("address").ToString)
'                                objDataOperation.AddParameter("@contactperson", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, .Item("contactperson").ToString)
'                                objDataOperation.AddParameter("@contactno", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, .Item("contactno").ToString)
'                                objDataOperation.AddParameter("@remark", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, .Item("remark").ToString)


'                                objDataOperation.ExecNonQuery(strQ)

'                                If objDataOperation.ErrorMessage <> "" Then
'                                    exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
'                                    Throw exForce
'                                End If

'                            Case "U"

'                                strQ = "UPDATE rcapplicantqualification_tran SET " & _
'                                              "  applicantunkid = @applicantunkid" & _
'                                              ", qualificationgroupunkid = @qualificationgroupunkid" & _
'                                              ", qualificationunkid = @qualificationunkid" & _
'                                              ", qualificationdate = @qualificationdate" & _
'                                              ", award = @award" & _
'                                              ", referenceno = @referenceno" & _
'                                              ", institution_name = @institution_name" & _
'                                              ", address = @address" & _
'                                              ", contactperson = @contactperson" & _
'                                              ", contactno = @contactno" & _
'                                              ", remark = @remark " & _
'                                        "WHERE qualificationtranunkid = @qualificationtranunkid "

'                                objDataOperation.AddParameter("@qualificationtranunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, .Item("qualificationtranunkid").ToString)
'                                objDataOperation.AddParameter("@applicantunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintApplicantunkid.ToString)
'                                objDataOperation.AddParameter("@qualificationgroupunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, .Item("qualificationgroupunkid").ToString)
'                                objDataOperation.AddParameter("@qualificationunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, .Item("qualificationunkid").ToString)
'                                If .Item("qualificationdate").ToString = Nothing Then
'                                    objDataOperation.AddParameter("@qualificationdate", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, DBNull.Value)
'                                Else
'                                    objDataOperation.AddParameter("@qualificationdate", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, .Item("qualificationdate").ToString)
'                                End If

'                                objDataOperation.AddParameter("@award", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, .Item("award").ToString)
'                                objDataOperation.AddParameter("@referenceno", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, .Item("referenceno").ToString)
'                                objDataOperation.AddParameter("@institution_name", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, .Item("institution_name").ToString)
'                                objDataOperation.AddParameter("@address", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, .Item("address").ToString)
'                                objDataOperation.AddParameter("@contactperson", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, .Item("contactperson").ToString)
'                                objDataOperation.AddParameter("@contactno", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, .Item("contactno").ToString)
'                                objDataOperation.AddParameter("@remark", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, .Item("remark").ToString)


'                                Call objDataOperation.ExecNonQuery(strQ)

'                                If objDataOperation.ErrorMessage <> "" Then
'                                    exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
'                                    Throw exForce
'                                End If


'                            Case "D"
'                                strQ = "DELETE FROM rcapplicantqualification_tran " & _
'                                        "WHERE qualificationtranunkid = @qualificationtranunkid "

'                                objDataOperation.AddParameter("@qualificationtranunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, .Item("qualificationtranunkid").ToString)

'                                Call objDataOperation.ExecNonQuery(strQ)

'                                If objDataOperation.ErrorMessage <> "" Then
'                                    exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
'                                    Throw exForce
'                                End If

'                        End Select
'                    End If

'                End With
'            Next
'            objDataOperation.ReleaseTransaction(True)
'            Return True
'        Catch ex As Exception
'            objDataOperation.ReleaseTransaction(False)
'            DisplayError.Show("-1", ex.Message, "InsertUpdateDelete_QualificationTran", mstrModuleName)
'            Return False
'        End Try
'    End Function
'#End Region

'End Class
