﻿'************************************************************************************************************************************
'Class Name : clsvacancy_master.vb
'Purpose    :
'Date       :26/07/2010
'Written By :Anjan
'Modified   :
'************************************************************************************************************************************

Imports eZeeCommonLib
Imports Aruti.Data.clsCommon_Master
Imports System.ComponentModel

''' <summary>
''' Purpose: 
''' Developer: Anjan
''' </summary>
Public Class clsVacancy
    Private Shared ReadOnly mstrModuleName As String = "clsVacancy"
    Dim objDataOperation As clsDataOperation
    Dim mstrMessage As String = ""

#Region " Private variables "
    Private mintVacancyunkid As Integer
    'S.SANDEEP [ 28 FEB 2012 ] -- START
    'ENHANCEMENT : TRA CHANGES
    'Private mstrVacancytitle As String = String.Empty
    Private mintVacancyMasterUnkid As Integer = 0
    'S.SANDEEP [ 28 FEB 2012 ] -- END
    Private mintStationunkid As Integer
    Private mintDeptgroupunkid As Integer
    Private mintDepartmentunkid As Integer
    Private mintSectionunkid As Integer
    Private mintUnitunkid As Integer
    Private mintJobgroupunkid As Integer
    Private mintJobunkid As Integer
    Private mintEmployeementtypeunkid As Integer
    Private mdtOpeningdate As Date
    Private mdtClosingdate As Date
    Private mdtInterview_Startdate As Date
    Private mdtInterview_Closedate As Date
    Private mintPaytypeunkid As Integer
    Private mintShifttypeunkid As Integer

    'Anjan (11 May 2011)-Start
    'Private mdblPay_From As Double
    'Private mdblPay_To As Double
    Private mdecPay_From As Decimal
    Private mdecPay_To As Decimal
    'Anjan (11 May 2011)-End 


    Private mintNoofposition As Integer
    Private mintExperience As Integer
    Private mstrResponsibilities_Duties As String = String.Empty
    Private mstrRemark As String = String.Empty
    Private mintUserunkid As Integer
    Private mblnIsvoid As Boolean
    Private mintVoiduserunkid As Integer
    Private mdtVoiddatetime As Date
    Private objInterviewerTran As New clsinterviewer_tran
    Private objAdvertiseTran As New clsAdvertise_tran
    Private mstrVoidreason As String = String.Empty

    'S.SANDEEP [ 25 DEC 2011 ] -- START
    'ENHANCEMENT : TRA CHANGES
    Private mblnIsExternalVacancy As Boolean = False
    'S.SANDEEP [ 25 DEC 2011 ] -- END

    'S.SANDEEP [ 28 FEB 2012 ] -- START
    'ENHANCEMENT : TRA CHANGES
    Private mblnIswebexport As Boolean
    Private mintGradegroupunkid As Integer
    Private mintGradeunkid As Integer
    'S.SANDEEP [ 28 FEB 2012 ] -- END

    Private mintStaffrequisitiontranunkid = 0 'Sohail (28 May 2014)
#End Region

#Region " Properties "
    'S.SANDEEP [28-May-2018] -- START 
    'ISSUE/ENHANCEMENT : {Audit Trails} 
    Private mstrFormName As String = String.Empty
    Public WriteOnly Property _FormName() As String
        Set(ByVal value As String)
            mstrFormName = value
        End Set
    End Property

    Private mstrClientIP As String = ""
    Public WriteOnly Property _ClientIP() As String
        Set(ByVal value As String)
            mstrClientIP = value
        End Set
    End Property

    Private mstrHostName As String = ""
    Public WriteOnly Property _HostName() As String
        Set(ByVal value As String)
            mstrHostName = value
        End Set
    End Property

    Private mblnIsWeb As Boolean = False
    Public WriteOnly Property _FromWeb() As Boolean
        Set(ByVal value As Boolean)
            mblnIsWeb = value
        End Set
    End Property

    'Hemant (03 Sep 2019) -- Start
    'ISSUE/ENHANCEMENT(NMB) :  RECRUITMENTS UAT CHANGES(Point No - 22 : On vacancy master screen, provide all allocations – TC005).
    Private mintSectionGrpUnkId As Integer
    Private mintUnitGrpUnkId As Integer
    Private mintTeamunkid As Integer
    Private mintCostcenterunkid As Integer
    Private mintClassGroupunkid As Integer
    Private mintClassUnkid As Integer
    Private mintGradeLevelUnkId As Integer
    'Hemant (03 Sep 2019) -- End

    'Sohail (27 Sep 2019) -- Start
    'NMB Enhancement # : They want option to bolden the content of the advert as per the needs of the advertised position..
    Private mblnIsskillbold As Boolean
    Private mblnIsskillitalic As Boolean
    Private mblnIsqualibold As Boolean
    Private mblnIsqualiitalic As Boolean
    Private mblnIsexpbold As Boolean
    Private mblnIsexpitalic As Boolean
    'Sohail (27 Sep 2019) -- End
    'Sohail (18 Feb 2020) -- Start
    'NMB Enhancement # : On vacancy master, it should be possible to publish a vacancy as an internal and external vacancy simultaneously.
    Private mblnIsbothintext As Boolean
    'Sohail (18 Feb 2020) -- End


 'Pinkal (13-Sep-2021)-- Start
    'NMB Enhancement : Adding New Fields in Vacancy Master required by NMB.
    Private mstrOther_Qualification As String = ""
    Private mstrOther_Skill As String = ""
    Private mstrOther_Lanaguage As String = ""
    Private mstrOther_Experience As String = ""
    'Pinkal (13-Sep-2021) -- End

    'S.SANDEEP |05-JUN-2023| -- START
    'ISSUE/ENHANCEMENT : A1X-948
    Private mblnIsApproved As Boolean = False
    Private mstrApprovalRemark As String = ""
    'S.SANDEEP |0-JUN-2023| -- END

#End Region

#Region " Properties "

    Private mintAuditUserId As Integer = 0
    Public WriteOnly Property _AuditUserId() As Integer
        Set(ByVal value As Integer)
            mintAuditUserId = value
        End Set
    End Property

    Private mintCompanyUnkid As Integer = 0
    Public WriteOnly Property _CompanyUnkid() As Integer
        Set(ByVal value As Integer)
            mintCompanyUnkid = value
        End Set
    End Property

    Private mdtAuditDate As DateTime = Now
    Public WriteOnly Property _AuditDate() As DateTime
        Set(ByVal value As DateTime)
            mdtAuditDate = value
        End Set
    End Property

    Private xDataOpr As clsDataOperation = Nothing
    Public Property _xDataOpr() As clsDataOperation
        Get
            Return xDataOpr
        End Get
        Set(ByVal value As clsDataOperation)
            xDataOpr = value
        End Set
    End Property

    Private mintLoginEmployeeunkid As Integer = 0
    Public Property _LoginEmployeeunkid() As Integer
        Get
            Return mintLoginEmployeeunkid
        End Get
        Set(ByVal value As Integer)
            mintLoginEmployeeunkid = value
        End Set
    End Property

    Private mintVoidlogingemployeeunkid As Integer = 0
    Public Property _Voidloginemployeeunkid() As Integer
        Get
            Return mintVoidlogingemployeeunkid
        End Get
        Set(ByVal value As Integer)
            mintVoidlogingemployeeunkid = value
        End Set
    End Property
    'S.SANDEEP [28-May-2018] -- END 
    ''' <summary>
    ''' Purpose: Get Message from Class 
    ''' Modify By: Anjan
    ''' </summary>
    Public ReadOnly Property _Message() As String
        Get
            Return mstrMessage
        End Get
    End Property

    ''' <summary>
    ''' Purpose: Get or Set vacancyunkid
    ''' Modify By: Anjan
    ''' </summary>
    Public Property _Vacancyunkid() As Integer
        Get
            Return mintVacancyunkid
        End Get
        Set(ByVal value As Integer)
            mintVacancyunkid = value
            Call GetData()
        End Set
    End Property



    'S.SANDEEP [ 28 FEB 2012 ] -- START
    'ENHANCEMENT : TRA CHANGES
    '''' <summary>
    '''' Purpose: Get or Set vacancytitle
    '''' Modify By: Anjan
    '''' </summary>
    'Public Property _Vacancytitle() As String
    '    Get
    '        Return mstrVacancytitle
    '    End Get
    '    Set(ByVal value As String)
    '        mstrVacancytitle = Value
    '    End Set
    'End Property

    ''' <summary>
    ''' Purpose: Get or Set vacancytitle
    ''' Modify By: Anjan
    ''' </summary>
    Public Property _Vacancy_MasterUnkid() As Integer
        Get
            Return mintVacancyMasterUnkid
        End Get
        Set(ByVal value As Integer)
            mintVacancyMasterUnkid = value
        End Set
    End Property
    'S.SANDEEP [ 28 FEB 2012 ] -- END

    ''' <summary>
    ''' Purpose: Get or Set stationunkid
    ''' Modify By: Anjan
    ''' </summary>
    Public Property _Stationunkid() As Integer
        Get
            Return mintStationunkid
        End Get
        Set(ByVal value As Integer)
            mintStationunkid = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set deptgroupunkid
    ''' Modify By: Anjan
    ''' </summary>
    Public Property _Deptgroupunkid() As Integer
        Get
            Return mintDeptgroupunkid
        End Get
        Set(ByVal value As Integer)
            mintDeptgroupunkid = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set departmentunkid
    ''' Modify By: Anjan
    ''' </summary>
    Public Property _Departmentunkid() As Integer
        Get
            Return mintDepartmentunkid
        End Get
        Set(ByVal value As Integer)
            mintDepartmentunkid = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set sectionunkid
    ''' Modify By: Anjan
    ''' </summary>
    Public Property _Sectionunkid() As Integer
        Get
            Return mintSectionunkid
        End Get
        Set(ByVal value As Integer)
            mintSectionunkid = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set unitunkid
    ''' Modify By: Anjan
    ''' </summary>
    Public Property _Unitunkid() As Integer
        Get
            Return mintUnitunkid
        End Get
        Set(ByVal value As Integer)
            mintUnitunkid = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set jobgroupunkid
    ''' Modify By: Anjan
    ''' </summary>
    Public Property _Jobgroupunkid() As Integer
        Get
            Return mintJobgroupunkid
        End Get
        Set(ByVal value As Integer)
            mintJobgroupunkid = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set jobunkid
    ''' Modify By: Anjan
    ''' </summary>
    Public Property _Jobunkid() As Integer
        Get
            Return mintJobunkid
        End Get
        Set(ByVal value As Integer)
            mintJobunkid = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set employeementtypeunkid
    ''' Modify By: Anjan
    ''' </summary>
    Public Property _Employeementtypeunkid() As Integer
        Get
            Return mintEmployeementtypeunkid
        End Get
        Set(ByVal value As Integer)
            mintEmployeementtypeunkid = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set openingdate
    ''' Modify By: Anjan
    ''' </summary>
    Public Property _Openingdate() As Date
        Get
            Return mdtOpeningdate
        End Get
        Set(ByVal value As Date)
            mdtOpeningdate = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set closingdate
    ''' Modify By: Anjan
    ''' </summary>
    Public Property _Closingdate() As Date
        Get
            Return mdtClosingdate
        End Get
        Set(ByVal value As Date)
            mdtClosingdate = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set interview_startdate
    ''' Modify By: Anjan
    ''' </summary>
    Public Property _Interview_Startdate() As Date
        Get
            Return mdtInterview_Startdate
        End Get
        Set(ByVal value As Date)
            mdtInterview_Startdate = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set interview_closedate
    ''' Modify By: Anjan
    ''' </summary>
    Public Property _Interview_Closedate() As Date
        Get
            Return mdtInterview_Closedate
        End Get
        Set(ByVal value As Date)
            mdtInterview_Closedate = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set paytypeunkid
    ''' Modify By: Anjan
    ''' </summary>
    Public Property _Paytypeunkid() As Integer
        Get
            Return mintPaytypeunkid
        End Get
        Set(ByVal value As Integer)
            mintPaytypeunkid = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set shifttypeunkid
    ''' Modify By: Anjan
    ''' </summary>
    Public Property _Shifttypeunkid() As Integer
        Get
            Return mintShifttypeunkid
        End Get
        Set(ByVal value As Integer)
            mintShifttypeunkid = value
        End Set
    End Property

    'Anjan (11 May 2011)-Start
    '''' <summary>
    '''' Purpose: Get or Set pay_from
    '''' Modify By: Anjan
    '''' </summary>
    'Public Property _Pay_From() As Double
    '    Get
    '        Return mdblPay_From
    '    End Get
    '    Set(ByVal value As Double)
    '        mdblPay_From = Value
    '    End Set
    'End Property

    '''' <summary>
    '''' Purpose: Get or Set pay_to
    '''' Modify By: Anjan
    '''' </summary>
    'Public Property _Pay_To() As Double
    '    Get
    '        Return mdblPay_To
    '    End Get
    '    Set(ByVal value As Double)
    '        mdblPay_To = Value
    '    End Set
    'End Property
    ''' <summary>
    ''' Purpose: Get or Set pay_from
    ''' Modify By: Anjan
    ''' </summary>
    Public Property _Pay_From() As Decimal
        Get
            Return mdecPay_From
        End Get
        Set(ByVal value As Decimal)
            mdecPay_From = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set pay_to
    ''' Modify By: Anjan
    ''' </summary>
    Public Property _Pay_To() As Decimal
        Get
            Return mdecPay_To
        End Get
        Set(ByVal value As Decimal)
            mdecPay_To = value
        End Set
    End Property

    'Anjan (11 May 2011)-End 



    ''' <summary>
    ''' Purpose: Get or Set noofposition
    ''' Modify By: Anjan
    ''' </summary>
    Public Property _Noofposition() As Integer
        Get
            Return mintNoofposition
        End Get
        Set(ByVal value As Integer)
            mintNoofposition = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set experience
    ''' Modify By: Anjan
    ''' </summary>
    Public Property _Experience() As Integer
        Get
            Return mintExperience
        End Get
        Set(ByVal value As Integer)
            mintExperience = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set responsibilities_duties
    ''' Modify By: Anjan
    ''' </summary>
    Public Property _Responsibilities_Duties() As String
        Get
            Return mstrResponsibilities_Duties
        End Get
        Set(ByVal value As String)
            mstrResponsibilities_Duties = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set remark
    ''' Modify By: Anjan
    ''' </summary>
    Public Property _Remark() As String
        Get
            Return mstrRemark
        End Get
        Set(ByVal value As String)
            mstrRemark = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set userunkid
    ''' Modify By: Anjan
    ''' </summary>
    Public Property _Userunkid() As Integer
        Get
            Return mintUserunkid
        End Get
        Set(ByVal value As Integer)
            mintUserunkid = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set isvoid
    ''' Modify By: Anjan
    ''' </summary>
    Public Property _Isvoid() As Boolean
        Get
            Return mblnIsvoid
        End Get
        Set(ByVal value As Boolean)
            mblnIsvoid = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set voiduserunkid
    ''' Modify By: Anjan
    ''' </summary>
    Public Property _Voiduserunkid() As Integer
        Get
            Return mintVoiduserunkid
        End Get
        Set(ByVal value As Integer)
            mintVoiduserunkid = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set voiddatetime
    ''' Modify By: Anjan
    ''' </summary>
    Public Property _Voiddatetime() As Date
        Get
            Return mdtVoiddatetime
        End Get
        Set(ByVal value As Date)
            mdtVoiddatetime = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set voidreason
    ''' Modify By: Anjan
    ''' </summary>
    Public Property _Voidreason() As String
        Get
            Return mstrVoidreason
        End Get
        Set(ByVal value As String)
            mstrVoidreason = value
        End Set
    End Property


    'S.SANDEEP [ 25 DEC 2011 ] -- START
    'ENHANCEMENT : TRA CHANGES
    ''' <summary>
    ''' Purpose: Get or Set External Vacancy Boolean
    ''' Modify By: Sandeep J. Sharma
    ''' </summary>
    Public Property _Is_External_Vacancy() As Boolean
        Get
            Return mblnIsExternalVacancy
        End Get
        Set(ByVal value As Boolean)
            mblnIsExternalVacancy = value
        End Set
    End Property
    'S.SANDEEP [ 25 DEC 2011 ] -- END

    'S.SANDEEP [ 28 FEB 2012 ] -- START
    'ENHANCEMENT : TRA CHANGES
    ''' <summary>
    ''' Purpose: Get or Set iswebexport
    ''' Modify By: Sandeep J. Sharma
    ''' </summary>
    Public Property _Iswebexport() As Boolean
        Get
            Return mblnIswebexport
        End Get
        Set(ByVal value As Boolean)
            mblnIswebexport = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set gradegroupunkid
    ''' Modify By: Sandeep J. Sharma
    ''' </summary>
    Public Property _Gradegroupunkid() As Integer
        Get
            Return mintGradegroupunkid
        End Get
        Set(ByVal value As Integer)
            mintGradegroupunkid = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set gradeunkid
    ''' Modify By: Sandeep J. Sharma
    ''' </summary>
    Public Property _Gradeunkid() As Integer
        Get
            Return mintGradeunkid
        End Get
        Set(ByVal value As Integer)
            mintGradeunkid = value
        End Set
    End Property
    'S.SANDEEP [ 28 FEB 2012 ] -- END

    'Sohail (28 May 2014) -- Start
    'Enhancement - Staff Requisition.
    Public Property _Staffrequisitiontranunkid() As Integer
        Get
            Return mintStaffrequisitiontranunkid
        End Get
        Set(ByVal value As Integer)
            mintStaffrequisitiontranunkid = value
        End Set
    End Property
    'Sohail (28 May 2014) -- End

    'Hemant (03 Sep 2019) -- Start
    'ISSUE/ENHANCEMENT(NMB) :  RECRUITMENTS UAT CHANGES(Point No - 22 : On vacancy master screen, provide all allocations – TC005).
    Public Property _SectionGrpUnkId() As Integer
        Get
            Return mintSectionGrpUnkId
        End Get
        Set(ByVal value As Integer)
            mintSectionGrpUnkId = value
        End Set
    End Property

    Public Property _UnitGrpUnkId() As Integer
        Get
            Return mintUnitGrpUnkId
        End Get
        Set(ByVal value As Integer)
            mintUnitGrpUnkId = value
        End Set
    End Property

    Public Property _Teamunkid() As Integer
        Get
            Return mintTeamunkid
        End Get
        Set(ByVal value As Integer)
            mintTeamunkid = value
        End Set
    End Property

    Public Property _Costcenterunkid() As Integer
        Get
            Return mintCostcenterunkid
        End Get
        Set(ByVal value As Integer)
            mintCostcenterunkid = value
        End Set
    End Property

    Public Property _ClassGroupunkid() As Integer
        Get
            Return mintClassGroupunkid
        End Get
        Set(ByVal value As Integer)
            mintClassGroupunkid = value
        End Set
    End Property

    Public Property _ClassUnkid() As Integer
        Get
            Return mintClassUnkid
        End Get
        Set(ByVal value As Integer)
            mintClassUnkid = value
        End Set
    End Property

    Public Property _GradeLevelUnkId() As Integer
        Get
            Return mintGradeLevelUnkId
        End Get
        Set(ByVal value As Integer)
            mintGradeLevelUnkId = value
        End Set
    End Property
    'Hemant (03 Sep 2019) -- End

    'Sohail (27 Sep 2019) -- Start
    'NMB Enhancement # : They want option to bolden the content of the advert as per the needs of the advertised position..
    Public Property _Isskillbold() As Boolean
        Get
            Return mblnIsskillbold
        End Get
        Set(ByVal value As Boolean)
            mblnIsskillbold = value
        End Set
    End Property

    Public Property _Isskillitalic() As Boolean
        Get
            Return mblnIsskillitalic
        End Get
        Set(ByVal value As Boolean)
            mblnIsskillitalic = value
        End Set
    End Property

    Public Property _Isqualibold() As Boolean
        Get
            Return mblnIsqualibold
        End Get
        Set(ByVal value As Boolean)
            mblnIsqualibold = value
        End Set
    End Property

    Public Property _Isqualiitalic() As Boolean
        Get
            Return mblnIsqualiitalic
        End Get
        Set(ByVal value As Boolean)
            mblnIsqualiitalic = value
        End Set
    End Property

    Public Property _Isexpbold() As Boolean
        Get
            Return mblnIsexpbold
        End Get
        Set(ByVal value As Boolean)
            mblnIsexpbold = value
        End Set
    End Property

    Public Property _Isexpitalic() As Boolean
        Get
            Return mblnIsexpitalic
        End Get
        Set(ByVal value As Boolean)
            mblnIsexpitalic = value
        End Set
    End Property
    'Sohail (27 Sep 2019) -- End

    'Sohail (18 Feb 2020) -- Start
    'NMB Enhancement # : On vacancy master, it should be possible to publish a vacancy as an internal and external vacancy simultaneously.
    ''' <summary>
    ''' Is Both Internal and External Vacancy
    ''' </summary>
    ''' <value></value>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Property _Isbothintext() As Boolean
        Get
            Return mblnIsbothintext
        End Get
        Set(ByVal value As Boolean)
            mblnIsbothintext = value
        End Set
    End Property
    'Sohail (18 Feb 2020) -- End


    'Pinkal (13-Sep-2021)-- Start
    'NMB Enhancement : Adding New Fields in Vacancy Master required by NMB.
    Public Property _Other_Qualification() As String
        Get
            Return mstrOther_Qualification
        End Get
        Set(ByVal value As String)
            mstrOther_Qualification = value
        End Set
    End Property

    Public Property _Other_Skill() As String
        Get
            Return mstrOther_Skill
        End Get
        Set(ByVal value As String)
            mstrOther_Skill = value
        End Set
    End Property

    Public Property _Other_Language() As String
        Get
            Return mstrOther_Lanaguage
        End Get
        Set(ByVal value As String)
            mstrOther_Lanaguage = value
        End Set
    End Property

    Public Property _Other_Experience() As String
        Get
            Return mstrOther_Experience
        End Get
        Set(ByVal value As String)
            mstrOther_Experience = value
        End Set
    End Property
    'Pinkal (13-Sep-2021) -- End

    'S.SANDEEP |05-JUN-2023| -- START
    'ISSUE/ENHANCEMENT : A1X-948
    Public Property _IsApproved() As Boolean
        Get
            Return mblnIsApproved
        End Get
        Set(ByVal value As Boolean)
            mblnIsApproved = value
        End Set
    End Property

    Public Property _ApprovalRemark() As String
        Get
            Return mstrApprovalRemark
        End Get
        Set(ByVal value As String)
            mstrApprovalRemark = value
        End Set
    End Property
    'S.SANDEEP |0-JUN-2023| -- END

#End Region

    ''' <summary>
    ''' Modify By: Anjan
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>
    Public Sub GetData()
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        objDataOperation = New clsDataOperation

        Try
            strQ = "SELECT " & _
              "  vacancyunkid " & _
              ", vacancytitle " & _
              ", stationunkid " & _
              ", deptgroupunkid " & _
              ", departmentunkid " & _
              ", sectionunkid " & _
              ", unitunkid " & _
              ", jobgroupunkid " & _
              ", jobunkid " & _
              ", employeementtypeunkid " & _
              ", CONVERT(CHAR(8),openingdate,112) AS openingdate " & _
              ", CONVERT(CHAR(8),closingdate,112) AS closingdate " & _
              ", CONVERT(CHAR(8),interview_startdate,112) AS interview_startdate " & _
              ", CONVERT(CHAR(8),interview_closedate,112) AS interview_closedate " & _
              ", paytypeunkid " & _
              ", shifttypeunkid " & _
              ", pay_from " & _
              ", pay_to " & _
              ", noofposition " & _
              ", experience " & _
              ", responsibilities_duties " & _
              ", remark " & _
              ", userunkid " & _
              ", isvoid " & _
              ", voiduserunkid " & _
              ", voiddatetime " & _
              ", ISNULL(isexternalvacancy,0) AS isexternalvacancy " & _
              ", iswebexport " & _
              ", gradegroupunkid " & _
              ", gradeunkid " & _
              ", ISNULL(staffrequisitiontranunkid,0) AS staffrequisitiontranunkid " & _
              ", ISNULL(sectiongroupunkid,0) AS sectiongroupunkid " & _
              ", ISNULL(unitgroupunkid,0) AS unitgroupunkid " & _
              ", ISNULL(teamunkid,0) AS teamunkid " & _
              ", ISNULL(costcenterunkid,0) AS costcenterunkid " & _
              ", ISNULL(classgroupunkid,0) AS classgroupunkid " & _
              ", ISNULL(classunkid,0) AS classunkid " & _
              ", ISNULL(gradelevelunkid,0) AS gradelevelunkid " & _
              ", ISNULL(isskillbold, 0) AS isskillbold " & _
              ", ISNULL(isskillitalic, 0) AS isskillitalic " & _
              ", ISNULL(isqualibold, 0) AS isqualibold " & _
              ", ISNULL(isqualiitalic, 0) AS isqualiitalic " & _
              ", ISNULL(isexpbold, 0) AS isexpbold " & _
              ", ISNULL(isexpitalic, 0) AS isexpitalic " & _
              ", ISNULL(isbothintext, 0) AS isbothintext " & _
              ", ISNULL(other_qualification, '') AS other_qualification " & _
              ", ISNULL(other_skill, '') AS other_skill " & _
              ", ISNULL(other_language, '') AS other_language " & _
              ", ISNULL(other_experience, '') AS other_experience " & _
              ", ISNULL(isapproved,0) AS isapproved " & _
             "FROM rcvacancy_master " & _
             "WHERE vacancyunkid = @vacancyunkid "
            'S.SANDEEP |05-JUN-2023| -- START {isapproved} -- END

            'Pinkal (13-Sep-2021)--NMB Enhancement : Adding New Fields in Vacancy Master required by NMB.[", ISNULL(other_qualification, '') AS other_qualification, ISNULL(other_skill, '') AS other_skill , ISNULL(other_language, '') AS other_language " , ISNULL(other_experience, '') AS other_experience "]


            'Sohail (18 Feb 2020) -- [isbothintext]
            'Sohail (27 Sep 2019) -- [isskillbold, isskillitalic, isqualibold, isqualiitalic, isexpbold, isexpitalic]
            'Hemant (03 Sep 2019) -- [sectiongroupunkid, unitgroupunkid, teamunkid, costcenterunkid, classgroupunkid, classunkid, gradelevelunkid]
            'S.SANDEEP [ 25 DEC 2011 - {isexternalvacancy} ] 'S.SANDEEP [ 28 FEB 2012 -{iswebexport,gradegroupunkid,gradeunkid} ]
            'Sohail (28 May 2014) - [staffrequisitiontranunkid]

            objDataOperation.AddParameter("@vacancyunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintVacancyunkid.ToString)

            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            For Each dtRow As DataRow In dsList.Tables(0).Rows
                mintVacancyunkid = CInt(dtRow.Item("vacancyunkid"))

                'S.SANDEEP [ 28 FEB 2012 ] -- START
                'ENHANCEMENT : TRA CHANGES
                'mstrvacancytitle = dtRow.Item("vacancytitle").ToString
                mintVacancyMasterUnkid = CInt(dtRow.Item("vacancytitle"))
                'S.SANDEEP [ 28 FEB 2012 ] -- END

                mintStationunkid = CInt(dtRow.Item("stationunkid"))
                mintDeptgroupunkid = CInt(dtRow.Item("deptgroupunkid"))
                mintDepartmentunkid = CInt(dtRow.Item("departmentunkid"))
                mintSectionunkid = CInt(dtRow.Item("sectionunkid"))
                mintUnitunkid = CInt(dtRow.Item("unitunkid"))
                mintJobgroupunkid = CInt(dtRow.Item("jobgroupunkid"))
                mintJobunkid = CInt(dtRow.Item("jobunkid"))
                mintEmployeementtypeunkid = CInt(dtRow.Item("employeementtypeunkid"))
                'Sohail (28 May 2014) -- Start
                'Enhancement - Staff Requisition.
                'mdtOpeningdate = eZeeDate.convertDate(dtRow.Item("openingdate").ToString).ToShortDateString
                'mdtClosingdate = eZeeDate.convertDate(dtRow.Item("closingdate").ToString).ToShortDateString
                'mdtInterview_Startdate = eZeeDate.convertDate(dtRow.Item("interview_startdate").ToString).ToShortDateString
                'mdtInterview_Closedate = eZeeDate.convertDate(dtRow.Item("interview_closedate").ToString).ToShortDateString
                If IsDBNull(dtRow.Item("openingdate")) Then
                    mdtOpeningdate = Nothing
                Else
                mdtOpeningdate = eZeeDate.convertDate(dtRow.Item("openingdate").ToString).ToShortDateString
                End If
                If IsDBNull(dtRow.Item("closingdate")) Then
                    mdtClosingdate = Nothing
                Else
                mdtClosingdate = eZeeDate.convertDate(dtRow.Item("closingdate").ToString).ToShortDateString
                End If
                If IsDBNull(dtRow.Item("interview_startdate")) Then
                    mdtInterview_Startdate = Nothing
                Else
                mdtInterview_Startdate = eZeeDate.convertDate(dtRow.Item("interview_startdate").ToString).ToShortDateString
                End If
                If IsDBNull(dtRow.Item("interview_closedate")) Then
                    mdtInterview_Closedate = Nothing
                Else
                mdtInterview_Closedate = eZeeDate.convertDate(dtRow.Item("interview_closedate").ToString).ToShortDateString
                End If
                'Sohail (28 May 2014) -- End
                mintPaytypeunkid = CInt(dtRow.Item("paytypeunkid"))
                mintShifttypeunkid = CInt(dtRow.Item("shifttypeunkid"))
                mdecPay_From = CDec(dtRow.Item("pay_from"))
                mdecPay_To = CDec(dtRow.Item("pay_to"))
                mintNoofposition = CInt(dtRow.Item("noofposition"))
                mintExperience = CInt(dtRow.Item("experience"))
                mstrResponsibilities_Duties = dtRow.Item("responsibilities_duties").ToString
                mstrRemark = dtRow.Item("remark").ToString
                mintUserunkid = CInt(dtRow.Item("userunkid"))
                mblnIsvoid = CBool(dtRow.Item("isvoid"))
                mintVoiduserunkid = CInt(dtRow.Item("voiduserunkid"))

                If IsDBNull(dtRow.Item("voiddatetime")) Then
                    mdtVoiddatetime = Nothing
                Else
                    mdtVoiddatetime = dtRow.Item("voiddatetime")
                End If

                'S.SANDEEP [ 25 DEC 2011 ] -- START
                'ENHANCEMENT : TRA CHANGES
                mblnIsExternalVacancy = CBool(dtRow.Item("isexternalvacancy"))
                'S.SANDEEP [ 25 DEC 2011 ] -- END

                'S.SANDEEP [ 28 FEB 2012 ] -- START
                'ENHANCEMENT : TRA CHANGES
                mblnIswebexport = CBool(dtRow.Item("iswebexport"))
                mintGradegroupunkid = CInt(dtRow.Item("gradegroupunkid"))
                mintGradeunkid = CInt(dtRow.Item("gradeunkid"))
                'S.SANDEEP [ 28 FEB 2012 ] -- END
                mintStaffrequisitiontranunkid = CInt(dtRow.Item("staffrequisitiontranunkid")) 'Sohail (28 May 2014)

                'Hemant (03 Sep 2019) -- Start
                'ISSUE/ENHANCEMENT(NMB) :  RECRUITMENTS UAT CHANGES(Point No - 22 : On vacancy master screen, provide all allocations – TC005).
                mintSectionGrpUnkId = CInt(dtRow.Item("sectiongroupunkid"))
                mintUnitGrpUnkId = CInt(dtRow.Item("unitgroupunkid"))
                mintTeamunkid = CInt(dtRow.Item("teamunkid"))
                mintCostcenterunkid = CInt(dtRow.Item("costcenterunkid"))
                mintClassGroupunkid = CInt(dtRow.Item("classgroupunkid"))
                mintClassUnkid = CInt(dtRow.Item("classunkid"))
                mintGradeLevelUnkId = CInt(dtRow.Item("gradelevelunkid"))
                'Hemant (03 Sep 2019) -- End
                'Sohail (27 Sep 2019) -- Start
                'NMB Enhancement # : They want option to bolden the content of the advert as per the needs of the advertised position..
                mblnIsskillbold = CBool(dtRow.Item("isskillbold"))
                mblnIsskillitalic = CBool(dtRow.Item("isskillitalic"))
                mblnIsqualibold = CBool(dtRow.Item("isqualibold"))
                mblnIsqualiitalic = CBool(dtRow.Item("isqualiitalic"))
                mblnIsexpbold = CBool(dtRow.Item("isexpbold"))
                mblnIsexpitalic = CBool(dtRow.Item("isexpitalic"))
                'Sohail (27 Sep 2019) -- End
                'Sohail (18 Feb 2020) -- Start
                'NMB Enhancement # : On vacancy master, it should be possible to publish a vacancy as an internal and external vacancy simultaneously.
                mblnIsbothintext = CBool(dtRow.Item("isbothintext"))
                'Sohail (18 Feb 2020) -- End


                'Pinkal (13-Sep-2021)-- Start
                'NMB Enhancement : Adding New Fields in Vacancy Master required by NMB.
                mstrOther_Qualification = dtRow.Item("other_qualification").ToString()
                mstrOther_Skill = dtRow.Item("other_skill").ToString()
                mstrOther_Lanaguage = dtRow.Item("other_language").ToString()
                mstrOther_Experience = dtRow.Item("other_experience").ToString()
                'Pinkal (13-Sep-2021) -- End

                mblnIsApproved = CBool(dtRow.Item("isapproved")) 'S.SANDEEP |05-JUN-2023| -- START {isapproved} -- END

                Exit For
            Next
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: GetData; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
    End Sub


    ''' <summary>
    ''' Modify By: Anjan
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>
    Public Function GetList(ByVal strTableName As String, Optional ByVal blnOnlyActive As Boolean = True, Optional ByVal mstrFilter As String = "") As DataSet
        'Pinkal (09-Sep-2023) -- TRA Include New Screen For Importing Final ShortListing Applicant.[Optional ByVal mstrFilter As String = ""]

        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        objDataOperation = New clsDataOperation

        Try
            strQ = "SELECT " & _
                          "  rcvacancy_master.vacancyunkid " & _
                          ", rcvacancy_master.vacancytitle AS vacancytitleunkid " & _
                          ", ISNULL(VM.name,'') AS vacancytitle " & _
                          ", rcvacancy_master.stationunkid " & _
                          ", rcvacancy_master.deptgroupunkid " & _
                          ", rcvacancy_master.departmentunkid " & _
                          ", rcvacancy_master.sectionunkid " & _
                          ", rcvacancy_master.unitunkid " & _
                          ", rcvacancy_master.jobgroupunkid " & _
                          ", rcvacancy_master.jobunkid " & _
                          ", rcvacancy_master.employeementtypeunkid " & _
                          ", CONVERT(CHAR(8),rcvacancy_master.openingdate,112) AS openingdate " & _
                          ", CONVERT(CHAR(8),rcvacancy_master.closingdate,112) AS closingdate " & _
                          ", CONVERT(CHAR(8),rcvacancy_master.interview_startdate ,112) AS interview_startdate " & _
                          ", CONVERT(CHAR(8),rcvacancy_master.interview_closedate ,112) AS interview_closedate " & _
                          ", rcvacancy_master.paytypeunkid " & _
                          ", rcvacancy_master.shifttypeunkid " & _
                          ", rcvacancy_master.pay_from " & _
                          ", rcvacancy_master.pay_to " & _
                          ", rcvacancy_master.noofposition " & _
                          ", rcvacancy_master.experience " & _
                          ", rcvacancy_master.responsibilities_duties " & _
                          ", rcvacancy_master.remark " & _
                          ", rcvacancy_master.userunkid " & _
                          ", rcvacancy_master.isvoid " & _
                          ", rcvacancy_master.voiduserunkid " & _
                          ", rcvacancy_master.voiddatetime " & _
                          ", rcvacancy_master.voidreason " & _
                          ", ISNULL(hrstation_master.name,'') AS stationname " & _
                          ", ISNULL(hrdepartment_group_master.name,'') AS departmentgroup " & _
                          ", ISNULL(hrdepartment_master.name,'') AS departmentname " & _
                          ", ISNULL(hrsection_master.name,'') AS sectionname " & _
                          ", ISNULL(hrunit_master.name,'') AS unitname " & _
                          ", ISNULL(hrjobgroup_master.name,'') AS jobgroupname " & _
                          ", ISNULL(hrjob_master.job_name,'') AS jobname " & _
                          ", ISNULL(shifttype.name ,'')AS shifttype " & _
                          ", ISNULL(paytype.name,'') AS paytype " & _
                          ", ISNULL(employeementtype.name,'') AS employeementtypename " & _
                          ", ISNULL(isexternalvacancy,0) AS isexternalvacancy " & _
                          ", iswebexport " & _
                          ", rcvacancy_master.gradegroupunkid " & _
                          ", rcvacancy_master.gradeunkid " & _
                          ", ISNULL(hrgradegroup_master.name,'') AS GradeGroupName " & _
                          ", ISNULL(hrgrade_master.name,'') AS Grades " & _
                          ", ISNULL(rcvacancy_master.staffrequisitiontranunkid,0) AS staffrequisitiontranunkid " & _
                          ", ISNULL(rcvacancy_master.sectiongroupunkid,0) AS sectiongroupunkid " & _
                          ", ISNULL(hrsectiongroup_master.name,'') AS sectiongroupname " & _
                          ", ISNULL(rcvacancy_master.unitgroupunkid,0) AS unitgroupunkid " & _
                          ", ISNULL(hrunitgroup_master.name,'') AS unitgroupname " & _
                          ", ISNULL(rcvacancy_master.teamunkid,0) AS teamunkid " & _
                          ", ISNULL(hrteam_master.name,'') AS teamname " & _
                          ", ISNULL(rcvacancy_master.costcenterunkid,0) AS costcenterunkid " & _
                          ", ISNULL(prcostcenter_master.costcentername,'') AS costcentername " & _
                          ", ISNULL(rcvacancy_master.classgroupunkid,0) AS classgroupunkid " & _
                          ", ISNULL(hrclassgroup_master.name,'') AS classgroupname " & _
                          ", ISNULL(rcvacancy_master.classunkid,0) AS classunkid " & _
                          ", ISNULL(hrclasses_master.name,'') AS classesname " & _
                          ", ISNULL(rcvacancy_master.gradelevelunkid,0) AS gradelevelunkid " & _
                          ", ISNULL(hrgradelevel_master.name,'') AS gradelevelname " & _
                          ", ISNULL(rcvacancy_master.isskillbold, 0) AS isskillbold " & _
                          ", ISNULL(rcvacancy_master.isskillitalic, 0) AS isskillitalic " & _
                          ", ISNULL(rcvacancy_master.isqualibold, 0) AS isqualibold " & _
                          ", ISNULL(rcvacancy_master.isqualiitalic, 0) AS isqualiitalic " & _
                          ", ISNULL(rcvacancy_master.isexpbold, 0) AS isexpbold " & _
                          ", ISNULL(rcvacancy_master.isexpitalic, 0) AS isexpitalic " & _
                          ", ISNULL(rcvacancy_master.isbothintext, 0) AS isbothintext " & _
                          ", ISNULL(other_qualification, '') AS other_qualification " & _
                          ", ISNULL(other_skill, '') AS other_skill " & _
                          ", ISNULL(other_language, '') AS other_language " & _
                          ", ISNULL(other_experience, '') AS other_experience " & _
                          ", ISNULL(rcstaffrequisition_tran.formno, '') AS formno " & _
                          ", ISNULL(rcvacancy_master.isapproved,0) AS isapproved " & _
                          ", CASE WHEN ISNULL(rcvacancy_master.isapproved,0) = 1 THEN @A " & _
                          "       WHEN ISNULL(rcvacancy_master.isapproved,0) = 0 THEN @P " & _
                          "  END AS aprstatus " & _
                     "FROM rcvacancy_master " & _
                        "LEFT JOIN hrstation_master ON hrstation_master.stationunkid= rcvacancy_master.stationunkid " & _
                        "LEFT JOIN hrdepartment_group_master ON hrdepartment_group_master.deptgroupunkid=rcvacancy_master.deptgroupunkid " & _
                        "LEFT JOIN hrdepartment_master ON hrdepartment_master.departmentunkid=rcvacancy_master.departmentunkid " & _
                        "LEFT JOIN hrsection_master ON hrsection_master.sectionunkid=rcvacancy_master.sectionunkid " & _
                        "LEFT JOIN hrunit_master ON hrunit_master.unitunkid=rcvacancy_master.unitunkid " & _
                        "LEFT JOIN hrjobgroup_master ON hrjobgroup_master.jobgroupunkid=rcvacancy_master.jobgroupunkid " & _
                        "LEFT JOIN hrjob_master ON hrjob_master.jobunkid=rcvacancy_master.jobunkid " & _
                        "LEFT JOIN cfcommon_master AS shifttype ON shifttype.masterunkid=rcvacancy_master.shifttypeunkid AND shifttype.mastertype = " & enCommonMaster.SHIFT_TYPE & _
                        "LEFT JOIN cfcommon_master AS paytype ON paytype.masterunkid=rcvacancy_master.paytypeunkid AND paytype.mastertype = " & enCommonMaster.PAY_TYPE & _
                        "LEFT JOIN cfcommon_master AS employeementtype ON employeementtype.masterunkid=rcvacancy_master.employeementtypeunkid AND employeementtype.mastertype = " & enCommonMaster.EMPLOYEMENT_TYPE & _
                        "LEFT JOIN cfcommon_master AS VM ON VM.masterunkid=rcvacancy_master.vacancytitle AND VM.mastertype = " & enCommonMaster.VACANCY_MASTER & _
                        "LEFT JOIN hrgradegroup_master ON rcvacancy_master.gradegroupunkid = hrgradegroup_master.gradegroupunkid " & _
                        "LEFT JOIN hrgrade_master ON rcvacancy_master.gradeunkid = hrgrade_master.gradeunkid " & _
                        "LEFT JOIN hrsectiongroup_master ON rcvacancy_master.sectiongroupunkid = hrsectiongroup_master.sectiongroupunkid " & _
                        "LEFT JOIN hrunitgroup_master ON rcvacancy_master.unitgroupunkid = hrunitgroup_master.unitgroupunkid " & _
                        "LEFT JOIN hrteam_master ON rcvacancy_master.teamunkid = hrteam_master.teamunkid " & _
                        "LEFT JOIN prcostcenter_master ON rcvacancy_master.costcenterunkid = prcostcenter_master.costcenterunkid " & _
                        "LEFT JOIN hrclassgroup_master ON rcvacancy_master.classgroupunkid = hrclassgroup_master.classgroupunkid " & _
                        "LEFT JOIN hrclasses_master ON rcvacancy_master.classunkid = hrclasses_master.classesunkid " & _
                        "LEFT JOIN hrgradelevel_master ON rcvacancy_master.gradelevelunkid = hrgradelevel_master.gradelevelunkid " & _
                        "LEFT JOIN rcstaffrequisition_tran ON rcstaffrequisition_tran.staffrequisitiontranunkid = rcvacancy_master.staffrequisitiontranunkid " & _
                                "AND rcstaffrequisition_tran.isvoid = 0 " & _
                      " WHERE rcvacancy_master.isvoid = 0 "

            'Pinkal (09-Sep-2023) -- Start
            'TRA Include New Screen For Importing Final ShortListing Applicant.
            If mstrFilter.Trim.Length > 0 Then
                strQ &= " AND " & mstrFilter
            End If
            'Pinkal (09-Sep-2023) -- End
            
            'S.SANDEEP |05-JUN-2023| -- START {isapproved,aprstatus} -- END
            'Sohail (11 Feb 2022) - [formno, LEFT JOIN rcstaffrequisition_tran]
            'Pinkal (13-Sep-2021)--NMB Enhancement : Adding New Fields in Vacancy Master required by NMB.[", ISNULL(other_qualification, '') AS other_qualification, ISNULL(other_skill, '') AS other_skill , ISNULL(other_language, '') AS other_language " , ISNULL(other_experience, '') AS other_experience "]

            'Sohail (18 Feb 2020) -- [isbothintext]
            'Sohail (27 Sep 2019) -- [isskillbold, isskillitalic, isqualibold, isqualiitalic, isexpbold, isexpitalic]
            'Hemant (03 Sep 2019) -- [sectiongroupunkid, unitgroupunkid, teamunkid, costcenterunkid, classgroupunkid, classunkid, gradelevelunkid, sectiongroupname, unitgroupname, teamname, costcentername, classgroupname, classesname, gradelevelname]
            'S.SANDEEP [ 25 DEC 2011 - {isexternalvacancy} ] - 'S.SANDEEP [ 28 FEB 2012 -{iswebexport,gradegroupunkid,gradeunkid} ]
            'Sohail (28 May 2014) - [vacancytitleunkid, staffrequisitiontranunkid]


            'S.SANDEEP |05-JUN-2023| -- START
            'ISSUE/ENHANCEMENT : A1X-948
            objDataOperation.AddParameter("@A", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 500, "A"))
            objDataOperation.AddParameter("@P", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 501, "P"))
            'S.SANDEEP |0-JUN-2023| -- END

            dsList = objDataOperation.ExecQuery(strQ, strTableName)

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: GetList; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
        Return dsList
    End Function


    ''' <summary>
    ''' Modify By: Anjan
    ''' </summary>
    ''' <returns>Boolean</returns>
    ''' <purpose> INSERT INTO Database Table (rcvacancy_master) </purpose>
    Public Function Insert(ByVal dtCurrentDateAndTime As Date, Optional ByVal dtInterviewerTran As DataTable = Nothing, Optional ByVal dtAdvertiseTran As DataTable = Nothing, Optional ByVal objDataOp As clsDataOperation = Nothing) As Boolean
        'Hemant (13 Sep 2019) -- [dtCurrentDateAndTime]
        'Sohail (28 May 2014) - [objDataOp]

        'S.SANDEEP [ 25 DEC 2011 ] -- START
        'ENHANCEMENT : TRA CHANGES
        'If isExist(mstrName) Then
        '    mstrMessage = "<Message>"
        '    Return False
        'End If
        'Sohail (28 May 2014) -- Start
        'Enhancement - Staff Requisition.
        'If isExist(mintVacancyMasterUnkid, mblnIsExternalVacancy, mdtOpeningdate.Date) Then
        If isExist(mintVacancyMasterUnkid, mblnIsExternalVacancy, mdtOpeningdate.Date, , mintStaffrequisitiontranunkid) Then
            'Sohail (28 May 2014) -- End
            mstrMessage = Language.getMessage(mstrModuleName, 1, "This vacancy is aleardy define. Please define new vacancy.")
            Return False
        End If
        'S.SANDEEP [ 25 DEC 2011 ] -- END


        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception


        objDataOperation = New clsDataOperation

        Try
            'Sohail (28 May 2014) -- Start
            'Enhancement - Staff Requisition.
            'objDataOperation.BindTransaction()
            If objDataOp IsNot Nothing Then
                objDataOperation = objDataOp
                objDataOperation.ClearParameters()
            Else
            objDataOperation.BindTransaction()
            End If
            'Sohail (28 May 2014) -- End

            'S.SANDEEP [ 28 FEB 2012 ] -- START
            'ENHANCEMENT : TRA CHANGES
            'objDataOperation.AddParameter("@vacancytitle", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrVacancytitle.ToString)
            objDataOperation.AddParameter("@vacancytitle", SqlDbType.Int, eZeeDataType.INT_SIZE, mintVacancyMasterUnkid.ToString)
            'S.SANDEEP [ 28 FEB 2012 ] -- END
            objDataOperation.AddParameter("@stationunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintStationunkid.ToString)
            objDataOperation.AddParameter("@deptgroupunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintDeptgroupunkid.ToString)
            objDataOperation.AddParameter("@departmentunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintDepartmentunkid.ToString)
            objDataOperation.AddParameter("@sectionunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintSectionunkid.ToString)
            objDataOperation.AddParameter("@unitunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintUnitunkid.ToString)
            objDataOperation.AddParameter("@jobgroupunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintJobgroupunkid.ToString)
            objDataOperation.AddParameter("@jobunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintJobunkid.ToString)
            objDataOperation.AddParameter("@employeementtypeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintEmployeementtypeunkid.ToString)
            If mdtOpeningdate = Nothing Then
                objDataOperation.AddParameter("@openingdate", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, DBNull.Value)
            Else
                objDataOperation.AddParameter("@openingdate", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, mdtOpeningdate)
            End If

            If mdtClosingdate = Nothing Then
                objDataOperation.AddParameter("@closingdate", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, DBNull.Value)
            Else
                objDataOperation.AddParameter("@closingdate", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, mdtClosingdate)
            End If

            If mdtInterview_Startdate = Nothing Then
                objDataOperation.AddParameter("@interview_startdate", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, DBNull.Value)
            Else
                objDataOperation.AddParameter("@interview_startdate", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, mdtInterview_Startdate)
            End If

            If mdtInterview_Closedate = Nothing Then
                objDataOperation.AddParameter("@interview_closedate", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, DBNull.Value)
            Else

                objDataOperation.AddParameter("@interview_closedate", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, mdtInterview_Closedate)

            End If

            objDataOperation.AddParameter("@paytypeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintPaytypeunkid.ToString)
            objDataOperation.AddParameter("@shifttypeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintShifttypeunkid.ToString)
            objDataOperation.AddParameter("@pay_from", SqlDbType.Decimal, eZeeDataType.DECIMAL_SIZE, mdecPay_From.ToString)
            objDataOperation.AddParameter("@pay_to", SqlDbType.Decimal, eZeeDataType.DECIMAL_SIZE, mdecPay_To.ToString)
            objDataOperation.AddParameter("@noofposition", SqlDbType.Int, eZeeDataType.INT_SIZE, mintNoofposition.ToString)
            objDataOperation.AddParameter("@experience", SqlDbType.Int, eZeeDataType.INT_SIZE, mintExperience.ToString)
            'Sohail (23 Mar 2018) -- Start
            'Viscar Industrial Capacity Ltd Issue - Not bale to save text more than 1000 characters in vacancy master in 70.2.
            'objDataOperation.AddParameter("@responsibilities_duties", SqlDbType.NVarChar, eZeeDataType.DESC_SIZE, mstrResponsibilities_Duties.ToString)
            'objDataOperation.AddParameter("@remark", SqlDbType.NVarChar, eZeeDataType.DESC_SIZE, mstrRemark.ToString)
            objDataOperation.AddParameter("@responsibilities_duties", SqlDbType.NVarChar, Len(mstrResponsibilities_Duties), mstrResponsibilities_Duties.ToString)
            objDataOperation.AddParameter("@remark", SqlDbType.NVarChar, Len(mstrRemark), mstrRemark.ToString)
            'Sohail (23 Mar 2018) -- End
            objDataOperation.AddParameter("@userunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintUserunkid.ToString)
            objDataOperation.AddParameter("@isvoid", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsvoid.ToString)
            objDataOperation.AddParameter("@voiduserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintVoiduserunkid.ToString)
            objDataOperation.AddParameter("@voidreason", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrVoidreason.ToString)

            If mdtVoiddatetime = Nothing Then
                objDataOperation.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, DBNull.Value)
            Else
                objDataOperation.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, mdtVoiddatetime)
            End If

            'S.SANDEEP [ 25 DEC 2011 ] -- START
            'ENHANCEMENT : TRA CHANGES
            objDataOperation.AddParameter("@isexternalvacancy", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsExternalVacancy.ToString)
            'S.SANDEEP [ 25 DEC 2011 ] -- END


            'S.SANDEEP [ 28 FEB 2012 ] -- START
            'ENHANCEMENT : TRA CHANGES
            objDataOperation.AddParameter("@iswebexport", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIswebexport.ToString)
            objDataOperation.AddParameter("@gradegroupunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintGradegroupunkid.ToString)
            objDataOperation.AddParameter("@gradeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintGradeunkid.ToString)
            'S.SANDEEP [ 28 FEB 2012 ] -- END
            objDataOperation.AddParameter("@staffrequisitiontranunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintStaffrequisitiontranunkid.ToString) 'Sohail (28 May 2014)

            'Hemant (03 Sep 2019) -- Start
            'ISSUE/ENHANCEMENT(NMB) :  RECRUITMENTS UAT CHANGES(Point No - 22 : On vacancy master screen, provide all allocations – TC005).
            objDataOperation.AddParameter("@sectiongroupunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintSectionGrpUnkId.ToString)
            objDataOperation.AddParameter("@unitgroupunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintUnitGrpUnkId.ToString)
            objDataOperation.AddParameter("@teamunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintTeamunkid.ToString)
            objDataOperation.AddParameter("@costcenterunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintCostcenterunkid.ToString)
            objDataOperation.AddParameter("@classgroupunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintClassGroupunkid.ToString)
            objDataOperation.AddParameter("@classunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintClassUnkid.ToString)
            objDataOperation.AddParameter("@gradelevelunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintGradeLevelUnkId.ToString)
            'Hemant (03 Sep 2019) -- End
            'Sohail (27 Sep 2019) -- Start
            'NMB Enhancement # : They want option to bolden the content of the advert as per the needs of the advertised position..
            objDataOperation.AddParameter("@isskillbold", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsskillbold.ToString)
            objDataOperation.AddParameter("@isskillitalic", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsskillitalic.ToString)
            objDataOperation.AddParameter("@isqualibold", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsqualibold.ToString)
            objDataOperation.AddParameter("@isqualiitalic", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsqualiitalic.ToString)
            objDataOperation.AddParameter("@isexpbold", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsexpbold.ToString)
            objDataOperation.AddParameter("@isexpitalic", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsexpitalic.ToString)
            'Sohail (27 Sep 2019) -- End
            'Sohail (18 Feb 2020) -- Start
            'NMB Enhancement # : On vacancy master, it should be possible to publish a vacancy as an internal and external vacancy simultaneously.
            objDataOperation.AddParameter("@isbothintext", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsbothintext.ToString)
            'Sohail (18 Feb 2020) -- End


            'Pinkal (13-Sep-2021)-- Start
            'NMB Enhancement : Adding New Fields in Vacancy Master required by NMB.
            objDataOperation.AddParameter("@other_qualification", SqlDbType.NVarChar, mstrOther_Qualification.Trim.Length, mstrOther_Qualification.ToString)
            objDataOperation.AddParameter("@other_skill", SqlDbType.NVarChar, mstrOther_Skill.Trim.Length, mstrOther_Skill.ToString)
            objDataOperation.AddParameter("@other_language", SqlDbType.NVarChar, mstrOther_Lanaguage.Trim.Length, mstrOther_Lanaguage.ToString)
            objDataOperation.AddParameter("@other_experience", SqlDbType.NVarChar, mstrOther_Experience.Trim.Length, mstrOther_Experience.ToString)
            'Pinkal (13-Sep-2021) -- End

            'S.SANDEEP |05-JUN-2023| -- START
            'ISSUE/ENHANCEMENT : A1X-948           
            objDataOperation.AddParameter("@isapproved", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsApproved)            
            'S.SANDEEP |0-JUN-2023| -- END

            strQ = "INSERT INTO rcvacancy_master ( " & _
              "  vacancytitle " & _
              ", stationunkid " & _
              ", deptgroupunkid " & _
              ", departmentunkid " & _
              ", sectionunkid " & _
              ", unitunkid " & _
              ", jobgroupunkid " & _
              ", jobunkid " & _
              ", employeementtypeunkid " & _
              ", openingdate " & _
              ", closingdate " & _
              ", interview_startdate " & _
              ", interview_closedate " & _
              ", paytypeunkid " & _
              ", shifttypeunkid " & _
              ", pay_from " & _
              ", pay_to " & _
              ", noofposition " & _
              ", experience " & _
              ", responsibilities_duties " & _
              ", remark " & _
              ", userunkid " & _
              ", isvoid " & _
              ", voiduserunkid " & _
              ", voiddatetime " & _
              ", voidreason " & _
              ", isexternalvacancy " & _
              ", iswebexport " & _
              ", gradegroupunkid " & _
              ", gradeunkid " & _
              ", staffrequisitiontranunkid " & _
              ", sectiongroupunkid " & _
              ", unitgroupunkid " & _
              ", teamunkid " & _
              ", costcenterunkid " & _
              ", classgroupunkid " & _
              ", classunkid " & _
              ", gradelevelunkid " & _
              ", isskillbold " & _
              ", isskillitalic " & _
              ", isqualibold " & _
              ", isqualiitalic " & _
              ", isexpbold " & _
              ", isexpitalic " & _
              ", isbothintext " & _
                      ", other_qualification " & _
                      ", other_skill " & _
                      ", other_language " & _
                      ", other_experience " & _
              ", isapproved " & _
            ") VALUES (" & _
              "  @vacancytitle " & _
              ", @stationunkid " & _
              ", @deptgroupunkid " & _
              ", @departmentunkid " & _
              ", @sectionunkid " & _
              ", @unitunkid " & _
              ", @jobgroupunkid " & _
              ", @jobunkid " & _
              ", @employeementtypeunkid " & _
              ", @openingdate " & _
              ", @closingdate " & _
              ", @interview_startdate " & _
              ", @interview_closedate " & _
              ", @paytypeunkid " & _
              ", @shifttypeunkid " & _
              ", @pay_from " & _
              ", @pay_to " & _
              ", @noofposition " & _
              ", @experience " & _
              ", @responsibilities_duties " & _
              ", @remark " & _
              ", @userunkid " & _
              ", @isvoid " & _
              ", @voiduserunkid " & _
              ", @voiddatetime" & _
              ", @voidreason " & _
              ", @isexternalvacancy " & _
              ", @iswebexport " & _
              ", @gradegroupunkid " & _
              ", @gradeunkid" & _
              ", @staffrequisitiontranunkid " & _
              ", @sectiongroupunkid " & _
              ", @unitgroupunkid " & _
              ", @teamunkid " & _
              ", @costcenterunkid " & _
              ", @classgroupunkid " & _
              ", @classunkid " & _
              ", @gradelevelunkid " & _
              ", @isskillbold " & _
              ", @isskillitalic " & _
              ", @isqualibold " & _
              ", @isqualiitalic " & _
              ", @isexpbold " & _
              ", @isexpitalic " & _
              ", @isbothintext " & _
                      ", @other_qualification " & _
                      ", @other_skill " & _
                      ", @other_language " & _
                      ", @other_experience " & _
              ", @isapproved " & _
            "); SELECT @@identity"

            'S.SANDEEP |05-JUN-2023| -- START {isapproved} -- END
            'Pinkal (13-Sep-2021)-- NMB Enhancement : Adding New Fields in Vacancy Master required by NMB.[@other_qualification,@other_skill,@other_language,@other_experience]

            'Sohail (18 Feb 2020) -- [isbothintext]
            'Sohail (27 Sep 2019) -- [isskillbold, isskillitalic, isqualibold, isqualiitalic, isexpbold, isexpitalic]
            'Hemant (03 Sep 2019) -- [sectiongroupunkid, unitgroupunkid, teamunkid, costcenterunkid, classgroupunkid, classunkid, gradelevelunkid]
            'S.SANDEEP [ 25 DEC 2011 - {isexternalvacancy} ] 'S.SANDEEP [ 28 FEB 2012 -{iswebexport,gradegroupunkid,gradeunkid} ]
            'Sohail (28 May 2014) - [staffrequisitiontranunkid]

            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            mintVacancyunkid = dsList.Tables(0).Rows(0).Item(0)

            Dim blnFlag As Boolean = False

            If dtInterviewerTran IsNot Nothing Then
                objInterviewerTran._VacancyUnkid = mintVacancyunkid
                objInterviewerTran._DataTable = dtInterviewerTran
                With objInterviewerTran
                    ._FormName = mstrFormName
                    ._LoginEmployeeunkid = mintLoginEmployeeunkid
                    ._ClientIP = mstrClientIP
                    ._HostName = mstrHostName
                    ._FromWeb = mblnIsWeb
                    ._AuditUserId = mintAuditUserId
._CompanyUnkid = mintCompanyUnkid
                    ._AuditDate = mdtAuditDate
                End With
                blnFlag = objInterviewerTran.InsertUpdateDelete_InterviewerTran(dtCurrentDateAndTime, objDataOperation)
                'Hemant (13 Sep 2019) -- [dtCurrentDateAndTime, objDataOperation]
                If blnFlag = False Then objDataOperation.ReleaseTransaction(False)

                'If dtInterviewerTran.Rows.Count > 0 Then
                '    objInterviewerTran._VacancyUnkid = mintVacancyunkid
                '    objInterviewerTran._DataTable = dtInterviewerTran
                '    blnFlag = objInterviewerTran.InsertUpdateDelete_InterviewerTran()
                '    If blnFlag = False Then objDataOperation.ReleaseTransaction(False)

                'Else
                '    If clsCommonATLog.Insert_TranAtLog(objDataOperation, "rcvacancy_master", "vacancyunkid", mintVacancyunkid, "rcinterviewer_tran", "interviewertranunkid", -1, 1, 0) = False Then
                '        exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                '        Throw exForce
                '    End If
                'End If
                'Anjan (12 Oct 2011)-End 
            End If

            If dtAdvertiseTran IsNot Nothing Then

                'Anjan (12 Oct 2011)-Start
                'ENHANCEMENT : AUDIT TRAIL MAINTENANCE.
                'objAdvertiseTran._VacancyUnkid = mintVacancyunkid
                'objAdvertiseTran._DataTable = dtAdvertiseTran
                'blnFlag = objAdvertiseTran.InsertUpdateDelete_AdvertiseTran
                'If blnFlag = False Then objDataOperation.ReleaseTransaction(False)

                If dtAdvertiseTran.Rows.Count > 0 Then
                objAdvertiseTran._VacancyUnkid = mintVacancyunkid
                objAdvertiseTran._DataTable = dtAdvertiseTran
                    With objAdvertiseTran
                        ._FormName = mstrFormName
                        ._LoginEmployeeunkid = mintLoginEmployeeunkid
                        ._ClientIP = mstrClientIP
                        ._HostName = mstrHostName
                        ._FromWeb = mblnIsWeb
                        ._AuditUserId = mintAuditUserId
._CompanyUnkid = mintCompanyUnkid
                        ._AuditDate = mdtAuditDate
                    End With
                blnFlag = objAdvertiseTran.InsertUpdateDelete_AdvertiseTran(dtCurrentDateAndTime, objDataOperation)
                    'Hemant (13 Sep 2019) -- [dtCurrentDateAndTime,objDataOperation]
                If blnFlag = False Then objDataOperation.ReleaseTransaction(False)
                    'Else
                    '    If clsCommonATLog.Insert_TranAtLog(objDataOperation, "rcvacancy_master", "vacancyunkid", mintVacancyunkid, "rcadvertise_tran", "advertisetranunkid", -1, 1, 0) = False Then
                    '        exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                    '        Throw exForce
                    '    End If
            End If
                'Anjan (12 Oct 2011)-End 



            End If

            'S.SANDEEP |05-JUN-2023| -- START
            'ISSUE/ENHANCEMENT : A1X-948
            If InsertVacancyApproval(objDataOperation) = False Then
                objDataOperation.ReleaseTransaction(False)
            End If
            'S.SANDEEP |0-JUN-2023| -- END


            'Sohail (28 May 2014) -- Start
            'Enhancement - Staff Requisition.
            'objDataOperation.ReleaseTransaction(True)
            If objDataOp Is Nothing Then
            objDataOperation.ReleaseTransaction(True)
            End If
            'Sohail (28 May 2014) -- End

            Return True
        Catch ex As Exception
            objDataOperation.ReleaseTransaction(False)
            Throw New Exception(ex.Message & "; Procedure Name: Insert; Module Name: " & mstrModuleName)
            Return False
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
    End Function

    ''' <summary>
    ''' Modify By: Anjan
    ''' </summary>
    ''' <returns>Boolean</returns>
    ''' <purpose> Update Database Table (rcvacancy_master) </purpose>
    Public Function Update(ByVal intOldVacancyMasterUnkid As Integer, ByVal dtCurrentDateAndTime As Date, Optional ByVal dtInterviewerTran As DataTable = Nothing, Optional ByVal dtAdvertiseTran As DataTable = Nothing) As Boolean
        'Sohail (21 Aug 2015) - [dtCurrentDateAndTime]
        'Sohail (28 May 2014) - [intOldVacancyUnkid]

        'S.SANDEEP [ 25 DEC 2011 ] -- START
        'ENHANCEMENT : TRA CHANGES
        'If isExist(mstrName, mintVacancyunkid) Then
        '    mstrMessage = "<Message>"
        '    Return False
        'End If

        If isExist(mintVacancyMasterUnkid, mblnIsExternalVacancy, mdtOpeningdate.Date, mintVacancyunkid, mintStaffrequisitiontranunkid) Then
            'Sohail (17 Dec 2021) - [mintStaffrequisitiontranunkid]
            mstrMessage = Language.getMessage(mstrModuleName, 1, "This vacancy is aleardy define. Please define new vacancy.")
            Return False
        End If
        'S.SANDEEP [ 25 DEC 2011 ] -- END

        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        objDataOperation = New clsDataOperation

        Try
            objDataOperation.BindTransaction()

            objDataOperation.AddParameter("@vacancyunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintVacancyunkid.ToString)
            'S.SANDEEP [ 28 FEB 2012 ] -- START
            'ENHANCEMENT : TRA CHANGES
            'objDataOperation.AddParameter("@vacancytitle", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrVacancytitle.ToString)
            objDataOperation.AddParameter("@vacancytitle", SqlDbType.Int, eZeeDataType.INT_SIZE, mintVacancyMasterUnkid.ToString)
            'S.SANDEEP [ 28 FEB 2012 ] -- END
            objDataOperation.AddParameter("@stationunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintStationunkid.ToString)
            objDataOperation.AddParameter("@deptgroupunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintDeptgroupunkid.ToString)
            objDataOperation.AddParameter("@departmentunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintDepartmentunkid.ToString)
            objDataOperation.AddParameter("@sectionunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintSectionunkid.ToString)
            objDataOperation.AddParameter("@unitunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintUnitunkid.ToString)
            objDataOperation.AddParameter("@jobgroupunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintJobgroupunkid.ToString)
            objDataOperation.AddParameter("@jobunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintJobunkid.ToString)
            objDataOperation.AddParameter("@employeementtypeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintEmployeementtypeunkid.ToString)
            If mdtOpeningdate = Nothing Then
                objDataOperation.AddParameter("@openingdate", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, DBNull.Value)
            Else
                objDataOperation.AddParameter("@openingdate", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, mdtOpeningdate)
            End If

            If mdtClosingdate = Nothing Then
                objDataOperation.AddParameter("@closingdate", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, DBNull.Value)
            Else
                objDataOperation.AddParameter("@closingdate", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, mdtClosingdate)
            End If

            If mdtInterview_Startdate = Nothing Then
                objDataOperation.AddParameter("@interview_startdate", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, DBNull.Value)
            Else
                objDataOperation.AddParameter("@interview_startdate", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, mdtInterview_Startdate)
            End If

            If mdtInterview_Closedate = Nothing Then
                objDataOperation.AddParameter("@interview_closedate", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, DBNull.Value)
            Else

                objDataOperation.AddParameter("@interview_closedate", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, mdtInterview_Closedate)

            End If
            objDataOperation.AddParameter("@paytypeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintPaytypeunkid.ToString)
            objDataOperation.AddParameter("@shifttypeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintShifttypeunkid.ToString)
            objDataOperation.AddParameter("@pay_from", SqlDbType.Decimal, eZeeDataType.DECIMAL_SIZE, mdecPay_From.ToString)
            objDataOperation.AddParameter("@pay_to", SqlDbType.Decimal, eZeeDataType.DECIMAL_SIZE, mdecPay_To.ToString)
            objDataOperation.AddParameter("@noofposition", SqlDbType.Int, eZeeDataType.INT_SIZE, mintNoofposition.ToString)
            objDataOperation.AddParameter("@experience", SqlDbType.Int, eZeeDataType.INT_SIZE, mintExperience.ToString)
            'Sohail (23 Mar 2018) -- Start
            'Viscar Industrial Capacity Ltd Issue - Not bale to save text more than 1000 characters in vacancy master in 70.2.
            'objDataOperation.AddParameter("@responsibilities_duties", SqlDbType.NVarChar, eZeeDataType.DESC_SIZE, mstrResponsibilities_Duties.ToString)
            'objDataOperation.AddParameter("@remark", SqlDbType.NVarChar, eZeeDataType.DESC_SIZE, mstrRemark.ToString)
            objDataOperation.AddParameter("@responsibilities_duties", SqlDbType.NVarChar, Len(mstrResponsibilities_Duties), mstrResponsibilities_Duties.ToString)
            objDataOperation.AddParameter("@remark", SqlDbType.NVarChar, Len(mstrRemark), mstrRemark.ToString)
            'Sohail (23 Mar 2018) -- End
            objDataOperation.AddParameter("@userunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintUserunkid.ToString)
            objDataOperation.AddParameter("@isvoid", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsvoid.ToString)
            objDataOperation.AddParameter("@voiduserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintVoiduserunkid.ToString)
            objDataOperation.AddParameter("@voidreason", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrVoidreason.ToString)

            If mdtVoiddatetime = Nothing Then
                objDataOperation.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, DBNull.Value)
            Else
                objDataOperation.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, mdtVoiddatetime)
            End If

            'S.SANDEEP [ 25 DEC 2011 ] -- START
            'ENHANCEMENT : TRA CHANGES
            objDataOperation.AddParameter("@isexternalvacancy", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsExternalVacancy.ToString)
            'S.SANDEEP [ 25 DEC 2011 ] -- END

            'S.SANDEEP [ 28 FEB 2012 ] -- START
            'ENHANCEMENT : TRA CHANGES
            objDataOperation.AddParameter("@iswebexport", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIswebexport.ToString)
            objDataOperation.AddParameter("@gradegroupunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintGradegroupunkid.ToString)
            objDataOperation.AddParameter("@gradeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintGradeunkid.ToString)
            'S.SANDEEP [ 28 FEB 2012 ] -- END
            objDataOperation.AddParameter("@staffrequisitiontranunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintStaffrequisitiontranunkid.ToString) 'Sohail (28 May 2014)

            'Hemant (03 Sep 2019) -- Start
            'ISSUE/ENHANCEMENT(NMB) :  RECRUITMENTS UAT CHANGES(Point No - 22 : On vacancy master screen, provide all allocations – TC005).
            objDataOperation.AddParameter("@sectiongroupunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintSectionGrpUnkId.ToString)
            objDataOperation.AddParameter("@unitgroupunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintUnitGrpUnkId.ToString)
            objDataOperation.AddParameter("@teamunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintTeamunkid.ToString)
            objDataOperation.AddParameter("@costcenterunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintCostcenterunkid.ToString)
            objDataOperation.AddParameter("@classgroupunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintClassGroupunkid.ToString)
            objDataOperation.AddParameter("@classunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintClassUnkid.ToString)
            objDataOperation.AddParameter("@gradelevelunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintGradeLevelUnkId.ToString)
            'Hemant (03 Sep 2019) -- End
            'Sohail (27 Sep 2019) -- Start
            'NMB Enhancement # : They want option to bolden the content of the advert as per the needs of the advertised position..
            objDataOperation.AddParameter("@isskillbold", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsskillbold.ToString)
            objDataOperation.AddParameter("@isskillitalic", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsskillitalic.ToString)
            objDataOperation.AddParameter("@isqualibold", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsqualibold.ToString)
            objDataOperation.AddParameter("@isqualiitalic", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsqualiitalic.ToString)
            objDataOperation.AddParameter("@isexpbold", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsexpbold.ToString)
            objDataOperation.AddParameter("@isexpitalic", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsexpitalic.ToString)
            'Sohail (27 Sep 2019) -- End
            'Sohail (18 Feb 2020) -- Start
            'NMB Enhancement # : On vacancy master, it should be possible to publish a vacancy as an internal and external vacancy simultaneously.
            objDataOperation.AddParameter("@isbothintext", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsbothintext.ToString)
            'Sohail (18 Feb 2020) -- End

            'Pinkal (13-Sep-2021)-- Start
            'NMB Enhancement : Adding New Fields in Vacancy Master required by NMB.
            objDataOperation.AddParameter("@other_qualification", SqlDbType.NVarChar, mstrOther_Qualification.Trim.Length, mstrOther_Qualification.ToString)
            objDataOperation.AddParameter("@other_skill", SqlDbType.NVarChar, mstrOther_Skill.Trim.Length, mstrOther_Skill.ToString)
            objDataOperation.AddParameter("@other_language", SqlDbType.NVarChar, mstrOther_Lanaguage.Trim.Length, mstrOther_Lanaguage.ToString)
            objDataOperation.AddParameter("@other_experience", SqlDbType.NVarChar, mstrOther_Experience.Trim.Length, mstrOther_Experience.ToString)
            'Pinkal (13-Sep-2021) -- End

            'S.SANDEEP |05-JUN-2023| -- START
            'ISSUE/ENHANCEMENT : A1X-948            
            objDataOperation.AddParameter("@isapproved", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsApproved)            
            'S.SANDEEP |0-JUN-2023| -- END

            strQ = "UPDATE rcvacancy_master SET " & _
              "  vacancytitle = @vacancytitle" & _
              ", stationunkid = @stationunkid" & _
              ", deptgroupunkid = @deptgroupunkid" & _
              ", departmentunkid = @departmentunkid" & _
              ", sectionunkid = @sectionunkid" & _
              ", unitunkid = @unitunkid" & _
              ", jobgroupunkid = @jobgroupunkid" & _
              ", jobunkid = @jobunkid" & _
              ", employeementtypeunkid = @employeementtypeunkid" & _
              ", openingdate = @openingdate" & _
              ", closingdate = @closingdate" & _
              ", interview_startdate = @interview_startdate" & _
              ", interview_closedate = @interview_closedate" & _
              ", paytypeunkid = @paytypeunkid" & _
              ", shifttypeunkid = @shifttypeunkid" & _
              ", pay_from = @pay_from" & _
              ", pay_to = @pay_to" & _
              ", noofposition = @noofposition" & _
              ", experience = @experience" & _
              ", responsibilities_duties = @responsibilities_duties" & _
              ", remark = @remark" & _
              ", userunkid = @userunkid" & _
              ", isvoid = @isvoid" & _
              ", voiduserunkid = @voiduserunkid" & _
              ", voiddatetime = @voiddatetime " & _
              ", voidreason = @voidreason " & _
              ", isexternalvacancy = @isexternalvacancy " & _
              ", iswebexport = @iswebexport" & _
              ", gradegroupunkid = @gradegroupunkid" & _
              ", gradeunkid = @gradeunkid " & _
              ", staffrequisitiontranunkid = @staffrequisitiontranunkid " & _
              ", Syncdatetime  = NULL " & _
              ", sectiongroupunkid = @sectiongroupunkid " & _
              ", unitgroupunkid = @unitgroupunkid " & _
              ", teamunkid = @teamunkid " & _
              ", costcenterunkid = @costcenterunkid " & _
              ", classgroupunkid = @classgroupunkid " & _
              ", classunkid = @classunkid " & _
              ", gradelevelunkid = @gradelevelunkid " & _
              ", isskillbold = @isskillbold " & _
              ", isskillitalic = @isskillitalic " & _
              ", isqualibold = @isqualibold " & _
              ", isqualiitalic = @isqualiitalic " & _
              ", isexpbold = @isexpbold " & _
              ", isexpitalic = @isexpitalic " & _
              ", isbothintext = @isbothintext " & _
              ", other_qualification = @other_qualification " & _
              ", other_skill = @other_skill " & _
              ", other_language = @other_language " & _
              ", other_experience = @other_experience " & _
              ", isapproved = @isapproved " & _
            "WHERE vacancyunkid = @vacancyunkid "

            'S.SANDEEP |05-JUN-2023| -- START {isapproved} -- END
            'Pinkal (13-Sep-2021)-- NMB Enhancement : Adding New Fields in Vacancy Master required by NMB.[other_qualification = @other_qualification , other_skill = @other_skill , other_language = @other_language , other_experience = @other_experience "]

            'Sohail (18 Feb 2020) -- [isbothintext]
            'Sohail (27 Sep 2019) -- [isskillbold, isskillitalic, isqualibold, isqualiitalic, isexpbold, isexpitalic]
            'Hemant (03 Sep 2019) -- [sectiongroupunkid, unitgroupunkid, teamunkid, costcenterunkid, classgroupunkid, classunkid, gradelevelunkid]
            'S.SANDEEP [ 28 FEB 2012 -{iswebexport,gradegroupunkid,gradeunkid} ], 'Sohail (28 Mar 2012) - [Syncdatetime]
            'Sohail (28 May 2014) - [staffrequisitiontranunkid]

            Call objDataOperation.ExecNonQuery(strQ)

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            Dim blnFlag As Boolean = False


            'Anjan (12 Oct 2011)-Start
            'ENHANCEMENT : AUDIT TRAIL MAINTENANCE.
            Dim blnToInsert As Boolean = False
            'Anjan (12 Oct 2011)-End 


            If dtInterviewerTran IsNot Nothing Then

                'Anjan (12 Oct 2011)-Start
                'ENHANCEMENT : AUDIT TRAIL MAINTENANCE.
                'objInterviewerTran._VacancyUnkid = mintVacancyunkid
                'objInterviewerTran._DataTable = dtInterviewerTran
                'blnFlag = objInterviewerTran.InsertUpdateDelete_InterviewerTran()
                'If blnFlag = False Then objDataOperation.ReleaseTransaction(False)
                If dtInterviewerTran.Rows.Count > 0 Then

                    Dim dt() As DataRow = dtInterviewerTran.Select("AUD=''")
                    If dt.Length = dtInterviewerTran.Rows.Count Then
                        'S.SANDEEP [28-May-2018] -- START
                        'ISSUE/ENHANCEMENT : {Audit Trails} 
                        Dim objCommonATLog As New clsCommonATLog
                        objCommonATLog._FormName = mstrFormName
                        objCommonATLog._LoginEmployeeUnkid = mintLoginEmployeeunkid
                        objCommonATLog._ClientIP = mstrClientIP
                        objCommonATLog._HostName = mstrHostName
                        objCommonATLog._FromWeb = mblnIsWeb
                        objCommonATLog._AuditUserId = mintAuditUserId
objCommonATLog._CompanyUnkid = mintCompanyUnkid
                        objCommonATLog._AuditDate = mdtAuditDate
                        'S.SANDEEP [28-May-2018] -- END

                        If objCommonATLog.IsTableDataUpdate("atcommon_tranlog", "rcvacancy_master", mintVacancyunkid, "vacancyunkid", 2, objDataOperation) Then
                            blnToInsert = True
                        End If

                    Else
                objInterviewerTran._VacancyUnkid = mintVacancyunkid
                objInterviewerTran._DataTable = dtInterviewerTran
                        With objInterviewerTran
                            ._FormName = mstrFormName
                            ._LoginEmployeeunkid = mintLoginEmployeeunkid
                            ._ClientIP = mstrClientIP
                            ._HostName = mstrHostName
                            ._FromWeb = mblnIsWeb
                            ._AuditUserId = mintAuditUserId
._CompanyUnkid = mintCompanyUnkid
                            ._AuditDate = mdtAuditDate
                        End With
                 blnFlag = objInterviewerTran.InsertUpdateDelete_InterviewerTran(dtCurrentDateAndTime, objDataOperation)
                        'Hemant (13 Sep 2019) -- [dtCurrentDateAndTime,objDataOperation]
                If blnFlag = False Then objDataOperation.ReleaseTransaction(False)
                        blnToInsert = False
                    End If
                    'Anjan (12 Oct 2011)-End 
                End If
            End If

            If dtAdvertiseTran IsNot Nothing Then
                'Anjan (12 Oct 2011)-Start
                'ENHANCEMENT : AUDIT TRAIL MAINTENANCE.
                'objAdvertiseTran._VacancyUnkid = mintVacancyunkid
                'objAdvertiseTran._DataTable = dtAdvertiseTran
                'blnFlag = objAdvertiseTran.InsertUpdateDelete_AdvertiseTran
                'If blnFlag = False Then objDataOperation.ReleaseTransaction(False)

                If dtAdvertiseTran.Rows.Count > 0 Then
                    Dim dt() As DataRow = dtAdvertiseTran.Select("AUD=''")

                    If dt.Length = dtAdvertiseTran.Rows.Count Then
                        'S.SANDEEP [28-May-2018] -- START
                        'ISSUE/ENHANCEMENT : {Audit Trails} 
                        Dim objCommonATLog As New clsCommonATLog
                        objCommonATLog._FormName = mstrFormName
                        objCommonATLog._LoginEmployeeUnkid = mintLoginEmployeeunkid
                        objCommonATLog._ClientIP = mstrClientIP
                        objCommonATLog._HostName = mstrHostName
                        objCommonATLog._FromWeb = mblnIsWeb
                        objCommonATLog._AuditUserId = mintAuditUserId
objCommonATLog._CompanyUnkid = mintCompanyUnkid
                        objCommonATLog._AuditDate = mdtAuditDate
                        'S.SANDEEP [28-May-2018] -- END

                        If objCommonATLog.IsTableDataUpdate("atcommon_tranlog", "rcvacancy_master", mintVacancyunkid, "vacancyunkid", 2, objDataOperation) Then
                            blnToInsert = True
                        End If
                    Else
                objAdvertiseTran._VacancyUnkid = mintVacancyunkid
                objAdvertiseTran._DataTable = dtAdvertiseTran
                        With objAdvertiseTran
                            ._FormName = mstrFormName
                            ._LoginEmployeeunkid = mintLoginEmployeeunkid
                            ._ClientIP = mstrClientIP
                            ._HostName = mstrHostName
                            ._FromWeb = mblnIsWeb
                            ._AuditUserId = mintAuditUserId
._CompanyUnkid = mintCompanyUnkid
                            ._AuditDate = mdtAuditDate
                        End With
                blnFlag = objAdvertiseTran.InsertUpdateDelete_AdvertiseTran(dtCurrentDateAndTime, objDataOperation)
                        'Hemant (13 Sep 2019) -- [dtCurrentDateAndTime, objDataOperation]
                If blnFlag = False Then objDataOperation.ReleaseTransaction(False)
                        blnToInsert = False
                    End If

                End If
                    'Anjan (12 Oct 2011)-End 
            End If


            'Sohail (28 May 2014) -- Start
            'Enhancement - Staff Requisition. [Update Staffrequisition status to PUBLISH]
            If intOldVacancyMasterUnkid = 0 AndAlso mintStaffrequisitiontranunkid > 0 Then
                Dim objStaffReq As New clsStaffrequisition_Tran
                objStaffReq._Staffrequisitiontranunkid = mintStaffrequisitiontranunkid
                objStaffReq._Form_Statusunkid = enApprovalStatus.PUBLISHED
                'Sohail (21 Aug 2015) -- Start
                'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
                'If objStaffReq.Update(objDataOperation) = False Then
                If objStaffReq.Update(dtCurrentDateAndTime, objDataOperation) = False Then
                    'Sohail (21 Aug 2015) -- End
                    mstrMessage = objStaffReq._Message 'Sohail (04 Aug 2022)
                    objDataOperation.ReleaseTransaction(False)
                    Return False
                End If
                objStaffReq = Nothing
            End If
            'Sohail (28 May 2014) -- End

                'Anjan (12 Oct 2011)-Start
                'ENHANCEMENT : AUDIT TRAIL MAINTENANCE.
                If blnToInsert = True Then
                'S.SANDEEP [28-May-2018] -- START
                'ISSUE/ENHANCEMENT : {Audit Trails} 
                Dim objCommonATLog As New clsCommonATLog
                objCommonATLog._FormName = mstrFormName
                objCommonATLog._LoginEmployeeUnkid = mintLoginEmployeeunkid
                objCommonATLog._ClientIP = mstrClientIP
                objCommonATLog._HostName = mstrHostName
                objCommonATLog._FromWeb = mblnIsWeb
                objCommonATLog._AuditUserId = mintAuditUserId
objCommonATLog._CompanyUnkid = mintCompanyUnkid
                objCommonATLog._AuditDate = mdtAuditDate
                'S.SANDEEP [28-May-2018] -- END
                If objCommonATLog.Insert_TranAtLog(objDataOperation, "rcvacancy_master", "vacancyunkid", mintVacancyunkid, "rcadvertise_tran", "advertisetranunkid", -1, 2, 0) = False Then
                        exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                        Throw exForce
                    End If
                'S.SANDEEP [28-May-2018] -- START
                'ISSUE/ENHANCEMENT : {Audit Trails} 
                objCommonATLog = Nothing
                'S.SANDEEP [28-May-2018] -- END
                End If
                'Anjan (12 Oct 2011)-End 

            'S.SANDEEP |05-JUN-2023| -- START
            'ISSUE/ENHANCEMENT : A1X-948
            If InsertVacancyApproval(objDataOperation) = False Then
                objDataOperation.ReleaseTransaction(False)
            End If
            'S.SANDEEP |0-JUN-2023| -- END

            objDataOperation.ReleaseTransaction(True)

            Return True
        Catch ex As Exception
            objDataOperation.ReleaseTransaction(False)
            Throw New Exception(ex.Message & "; Procedure Name: Update; Module Name: " & mstrModuleName)
            Return False
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
    End Function

    ''' <summary>
    ''' Modify By: Anjan
    ''' </summary>
    ''' <returns>Boolean</returns>
    ''' <purpose> Delete Database Table (rcvacancy_master) </purpose>
    Public Function Delete(ByVal intUnkid As Integer, Optional ByVal objDataOp As clsDataOperation = Nothing) As Boolean
        'Sohail (28 May 2014) - [objDataOp]
        'Sandeep [ 09 Oct 2010 ] -- Start
        'If isUsed(intUnkid) Then
        '    mstrMessage = "<Message>"
        '    Return False
        'End If

        If isUsed(intUnkid) Then
            mstrMessage = Language.getMessage(mstrModuleName, 3, "Sorry, you cannot delete this Vacancy. Reason : This Vacancy is already linked with some transaction.")
            Return False
        End If
        'Sandeep [ 09 Oct 2010 ] -- End 

        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        objDataOperation = New clsDataOperation

        Try

            'Sohail (28 May 2014) -- Start
            'Enhancement - Staff Requisition.
            'objDataOperation.BindTransaction()
            If objDataOp IsNot Nothing Then
                objDataOperation = objDataOp
                objDataOperation.ClearParameters()
            Else
            objDataOperation.BindTransaction()
            End If
            'Sohail (28 May 2014) -- End

            strQ = " UPDATE rcvacancy_master SET " & _
                  "  isvoid = 1 " & _
                  ", voiduserunkid = @voiduserunkid " & _
                  ", voiddatetime = @voiddatetime  " & _
                  ", voidreason= @voidreason " & _
            "WHERE vacancyunkid = @vacancyunkid "

            objDataOperation.AddParameter("@vacancyunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intUnkid)
            objDataOperation.AddParameter("@voiddatetime", SqlDbType.SmallDateTime, eZeeDataType.DATETIME_SIZE, Now)
            objDataOperation.AddParameter("@voidreason", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrVoidreason.ToString)
            objDataOperation.AddParameter("@voiduserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintVoiduserunkid.ToString)

            Call objDataOperation.ExecNonQuery(strQ)

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            Dim blnFlag As Boolean
            With objInterviewerTran
                ._FormName = mstrFormName
                ._LoginEmployeeunkid = mintLoginEmployeeunkid
                ._ClientIP = mstrClientIP
                ._HostName = mstrHostName
                ._FromWeb = mblnIsWeb
                ._AuditUserId = mintAuditUserId
._CompanyUnkid = mintCompanyUnkid
                ._AuditDate = mdtAuditDate
            End With
            blnFlag = objInterviewerTran.VoidAll(intUnkid, True, 1, CDate(Now.Date & " " & Format(Now, "hh:mm:ss tt")), mstrVoidreason)
            If blnFlag = False Then objDataOperation.ReleaseTransaction(False)

            With objAdvertiseTran
                ._FormName = mstrFormName
                ._LoginEmployeeunkid = mintLoginEmployeeunkid
                ._ClientIP = mstrClientIP
                ._HostName = mstrHostName
                ._FromWeb = mblnIsWeb
                ._AuditUserId = mintAuditUserId
._CompanyUnkid = mintCompanyUnkid
                ._AuditDate = mdtAuditDate
            End With
            blnFlag = objAdvertiseTran.VoidAll(intUnkid, True, 1, CDate(Now.Date & " " & Format(Now, "hh:mm:ss tt")), mstrVoidreason)
            If blnFlag = False Then objDataOperation.ReleaseTransaction(False)



            'Sohail (28 May 2014) -- Start
            'Enhancement - Staff Requisition.
            'objDataOperation.ReleaseTransaction(True)
            If objDataOp Is Nothing Then
            objDataOperation.ReleaseTransaction(True)
            End If
            'Sohail (28 May 2014) -- End

            Return True
        Catch ex As Exception
            objDataOperation.ReleaseTransaction(False)
            Throw New Exception(ex.Message & "; Procedure Name: Update; Module Name: " & mstrModuleName)
            Return False
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
    End Function

    ''' <summary>
    ''' Modify By: Anjan
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>
    Public Function isUsed(ByVal intUnkid As Integer) As Boolean
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception
        'Sandeep [ 09 Oct 2010 ] -- Start
        Dim dsTables As DataSet = Nothing
        Dim blnIsUsed As Boolean = False
        'Sandeep [ 09 Oct 2010 ] -- End 

        objDataOperation = New clsDataOperation

        Try

            'Sandeep [ 09 Oct 2010 ] -- Start
            'strQ = "<Query>"
            strQ = "SELECT " & _
                       "	TABLE_NAME AS TableName " & _
                       "FROM INFORMATION_SCHEMA.COLUMNS " & _
                       "WHERE COLUMN_NAME = 'vacancyunkid' "

            dsTables = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            strQ = ""
            'Sandeep [ 09 Oct 2010 ] -- End 

            objDataOperation.AddParameter("@vacancyunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intUnkid)

            'Sandeep [ 09 Oct 2010 ] -- Start
            'dsList = objDataOperation.ExecQuery(strQ, "List")
            'If objDataOperation.ErrorMessage <> "" Then
            '    exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
            '    Throw exForce
            'End If
            'Return dsList.Tables(0).Rows.Count > 0

            For Each dtRow As DataRow In dsTables.Tables("List").Rows
                If dtRow.Item("TableName") = "rcvacancy_master" Or dtRow.Item("TableName") = "rcinterviewer_tran" Or dtRow.Item("TableName") = "rcadvertise_tran" Then Continue For

                strQ = "SELECT vacancyunkid FROM " & dtRow.Item("TableName").ToString & " WHERE vacancyunkid = @vacancyunkid "

                dsList = objDataOperation.ExecQuery(strQ, "Used")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

                If dsList.Tables("Used").Rows.Count > 0 Then
                    blnIsUsed = True
                    Exit For
                End If
            Next

            mstrMessage = ""

            Return blnIsUsed
            'Sandeep [ 09 Oct 2010 ] -- End 
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: isUsed; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
    End Function

    'S.SANDEEP |05-JUN-2023| -- START
    'ISSUE/ENHANCEMENT : A1X-948
    Public Function InsertVacancyApproval(ByVal _objDataOpr As clsDataOperation) As Boolean
        Dim StrQ As String = ""
        Dim exForce As Exception = Nothing
        Try
            StrQ = "INSERT INTO rcvacancy_approval_tran " & _
                   "( " & _
                   "	 vacancyunkid " & _
                   "	,approvedatetime " & _
                   "	,userunkid " & _
                   "	,approval_remark " & _
                   "    ,isapproved " & _
                   ") " & _
                   "VALUES " & _
                   "( " & _
                   "	 @vacancyunkid " & _
                   "	,GETDATE() " & _
                   "	,@userunkid " & _
                   "	,@approval_remark " & _
                   "    ,@isapproved " & _
                   ") "

            _objDataOpr.ClearParameters()
            _objDataOpr.AddParameter("@vacancyunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintVacancyunkid)
            _objDataOpr.AddParameter("@userunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintUserunkid)
            _objDataOpr.AddParameter("@approval_remark", SqlDbType.NVarChar, eZeeDataType.DESC_SIZE, mstrApprovalRemark)
            _objDataOpr.AddParameter("@isapproved", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsApproved)

            _objDataOpr.ExecNonQuery(StrQ)

            If _objDataOpr.ErrorMessage <> "" Then
                exForce = New Exception(_objDataOpr.ErrorNumber & ": " & _objDataOpr.ErrorMessage)
                Throw exForce
            End If

            Return True
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: InsertVacancyApproval; Module Name: " & mstrModuleName)
        End Try
    End Function
    'S.SANDEEP |0-JUN-2023| -- END

    'S.SANDEEP [ 25 DEC 2011 ] -- START
    'ENHANCEMENT : TRA CHANGES
    '''' <summary>
    '''' Modify By: Anjan
    '''' </summary>
    '''' <purpose> Assign all Property variable </purpose>
    ''''Public Function isExist(ByVal strCode As String, ByVal strName As String, Optional ByVal intUnkid As Integer = -1) As Boolean
    ''''S.SANDEEP [ 28 FEB 2012 ] -- START
    ''''S.SANDEEP [ 25 DEC 2011 ] -- END
    ''''ENHANCEMENT : TRA CHANGES
    ''''Public Function isExist(ByVal strName As String, ByVal blnIsExVacancy As Boolean, Optional ByVal intUnkid As Integer = -1) As Boolean
    Public Function isExist(ByVal intVMasterId As Integer, ByVal blnIsExVacancy As Boolean, ByVal dtStartDate As DateTime, Optional ByVal intUnkid As Integer = -1, Optional ByVal intStaffrequisitiontranunkid As Integer = 0) As Boolean
        'S.SANDEEP [ 28 FEB 2012 ] -- END
        'Sohail (28 May 2014) - [intStaffrequisitiontranunkid]

        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        Dim objDataOperation As New clsDataOperation

        Try
            strQ = "SELECT " & _
              "  vacancyunkid " & _
              ", vacancytitle " & _
              ", stationunkid " & _
              ", deptgroupunkid " & _
              ", departmentunkid " & _
              ", sectionunkid " & _
              ", unitunkid " & _
              ", jobgroupunkid " & _
              ", jobunkid " & _
              ", employeementtypeunkid " & _
              ", openingdate " & _
              ", closingdate " & _
              ", interview_startdate " & _
              ", interview_closedate " & _
              ", paytypeunkid " & _
              ", shifttypeunkid " & _
              ", pay_from " & _
              ", pay_to " & _
              ", noofposition " & _
              ", experience " & _
              ", responsibilities_duties " & _
              ", remark " & _
              ", userunkid " & _
              ", isvoid " & _
              ", voiduserunkid " & _
              ", voiddatetime " & _
              ", ISNULL(isexternalvacancy,0) AS isexternalvacancy " & _
              ", ISNULL(staffrequisitiontranunkid,0) AS staffrequisitiontranunkid " & _
              ", ISNULL(sectiongroupunkid,0) AS sectiongroupunkid " & _
              ", ISNULL(unitgroupunkid,0) AS unitgroupunkid " & _
              ", ISNULL(teamunkid,0) AS teamunkid " & _
              ", ISNULL(costcenterunkid,0) AS costcenterunkid " & _
              ", ISNULL(classgroupunkid,0) AS classgroupunkid " & _
              ", ISNULL(classunkid,0) AS classunkid " & _
              ", ISNULL(gradelevelunkid,0) AS gradelevelunkid " & _
             "FROM rcvacancy_master " & _
             "WHERE isvoid = 0 AND vacancytitle = @vacancytitle " & _
             " AND ISNULL(isexternalvacancy,0)  = @IsExVacancy " & _
             " AND @Date BETWEEN CONVERT(CHAR(8),openingdate,112) AND CONVERT(CHAR(8),closingdate,112) "
            'Sohail (17 Dec 2021) - [isvoid = 0 AND]
            'Hemant (03 Sep 2019) -- [sectiongroupunkid, unitgroupunkid, teamunkid, costcenterunkid, classgroupunkid, classunkid, gradelevelunkid]
            'Sohail (28 May 2014) - [staffrequisitiontranunkid]
            'S.SANDEEP [ 25 DEC 2011 - {isexternalvacancy} ]


            'Sohail (28 May 2014) -- Start
            'Enhancement - Staff Requisition.
            If intStaffrequisitiontranunkid > 0 Then
                strQ &= " AND ISNULL(staffrequisitiontranunkid,0) = @staffrequisitiontranunkid"
                objDataOperation.AddParameter("@staffrequisitiontranunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intStaffrequisitiontranunkid)
            End If
            'Sohail (28 May 2014) -- End

            If intUnkid > 0 Then
                strQ &= " AND vacancyunkid <> @vacancyunkid"
                objDataOperation.AddParameter("@vacancyunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intUnkid)
            End If



            'S.SANDEEP [ 28 FEB 2012 ] -- START
            'ENHANCEMENT : TRA CHANGES
            'objDataOperation.AddParameter("@vacancytitle", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, strName)
            objDataOperation.AddParameter("@vacancytitle", SqlDbType.Int, eZeeDataType.INT_SIZE, intVMasterId)
            objDataOperation.AddParameter("@Date", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, eZeeDate.convertDate(dtStartDate).ToString)
            'S.SANDEEP [ 28 FEB 2012 ] -- END




            'S.SANDEEP [ 25 DEC 2011 ] -- START
            'ENHANCEMENT : TRA CHANGES
            objDataOperation.AddParameter("@IsExVacancy", SqlDbType.Bit, eZeeDataType.BIT_SIZE, blnIsExVacancy.ToString)
            'S.SANDEEP [ 25 DEC 2011 ] -- END




            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            Return dsList.Tables(0).Rows.Count > 0
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: isExist; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
        End Try
    End Function



   'S.SANDEEP [ 25 DEC 2011 ] -- START
    'ENHANCEMENT : TRA CHANGES
    ''' <summary>
    '''  Modify By: Anjan
    ''' </summary>
    ''' <param name="blnNA"></param>
    ''' <param name="strListName"></param>
    ''' <param name="intLanguageID"></param>
    ''' <param name="intVacancyType">1 = Only External Vacancy ,2 = Only Internal Vancancy</param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    '''Public Function getComboList(Optional ByVal blnNA As Boolean = False, _
    '''                             Optional ByVal strListName As String = "List", _
    '''                             Optional ByVal intLanguageID As Integer = -1) As DataSet
    Public Function getComboList(ByVal dtCurrentDateAndTime As DateTime, Optional ByVal blnNA As Boolean = False, _
                                 Optional ByVal strListName As String = "List", _
                                 Optional ByVal intLanguageID As Integer = -1, _
                                 Optional ByVal intVacancyType As Integer = 0, _
                                 Optional ByVal blnCheckClosingDate As Boolean = False, Optional ByVal blnNotShowFutureVacancy As Boolean = False, Optional ByVal IsFromReport As Boolean = False, Optional ByVal blnAddDatesInName As Boolean = True, Optional ByVal strFilter As String = "") As DataSet
        'Sohail (05 Jun 2020) - [blnAddDatesInName, strFilter]
        'Shani(24-Aug-2015) -- [dtCurrentDateAndTime]

        'S.SANDEEP [ 25 DEC 2011 ] -- END

        Dim strQ As String = ""
        Dim exForce As Exception
        Dim dsList As New DataSet
        Dim objDataOperation As New clsDataOperation

        Try

            If blnNA Then


                'S.SANDEEP [ 25 DEC 2011 ] -- START
                'ENHANCEMENT : TRA CHANGES
                'strQ = "SELECT 0 AS id " & _
                '          ",  ' ' + @Select AS name " & _
                '       "UNION "
                strQ = "SELECT 0 AS id " & _
                          ",  ' ' + @Select AS name,'' AS SDate, '' AS EDate,0 AS masterunkid "
                If intVacancyType > 0 Or IsFromReport = True Then
                    strQ &= " UNION "
                End If

                'S.SANDEEP [ 25 DEC 2011 ] -- END


                objDataOperation.AddParameter("@Select", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 2, "Select"))

            End If

            'S.SANDEEP [ 25 DEC 2011 ] -- START
            'ENHANCEMENT : TRA CHANGES

            'strQ &= "SELECT vacancyunkid as id " & _
            '            ", vacancytitle AS name " & _
            '         "FROM rcvacancy_master " & _
            '         "WHERE isvoid = 0 " & _
            '         "ORDER BY  name "

            If intVacancyType > 0 Or IsFromReport = True Then
                strQ &= "SELECT vacancyunkid as id " & _
                        ", cfcommon_master.name AS name " & _
                        ", CONVERT(CHAR(8),openingdate,112) AS SDate " & _
                        ", CONVERT(CHAR(8),closingdate,112) AS EDate " & _
                        ", cfcommon_master.masterunkid as masterunkid " & _
                     "FROM rcvacancy_master " & _
                     " JOIN cfcommon_master ON cfcommon_master.masterunkid = rcvacancy_master.vacancytitle AND cfcommon_master.mastertype = '" & clsCommon_Master.enCommonMaster.VACANCY_MASTER & "' " & _
                     "WHERE isvoid = 0 "
                If IsFromReport = False Then
                    Select Case intVacancyType
                        Case enVacancyType.EXTERNAL_VACANCY  'EXTERNAL
                            'Sohail (13 Mar 2020) -- Start
                            'NMB Enhancement # : On vacancy master, it should be possible to publish a vacancy as an internal and external vacancy simultaneously. (changes in desktop)
                            'strQ &= " AND isexternalvacancy = 1 "
                            'Sohail (18 Apr 2020) -- Start
                            'NMB Enhancement # : Need a filter type on vacancy type called Internal and External.
                            'strQ &= " AND (isexternalvacancy = 1 OR ISNULL(isbothintext, 0) = 1) "
                            strQ &= " AND (isexternalvacancy = 1 AND ISNULL(isbothintext, 0) = 0) "
                            'Sohail (18 Apr 2020) -- End
                            'Sohail (13 Mar 2020) -- End
                        Case enVacancyType.INTERNAL_VACANCY  'INTERNAL
                            'Sohail (13 Mar 2020) -- Start
                            'NMB Enhancement # : On vacancy master, it should be possible to publish a vacancy as an internal and external vacancy simultaneously. (changes in desktop)
                            'strQ &= " AND isexternalvacancy = 0 "
                            'Sohail (18 Apr 2020) -- Start
                            'NMB Enhancement # : Need a filter type on vacancy type called Internal and External.
                            'strQ &= " AND (isexternalvacancy = 0 OR ISNULL(isbothintext, 0) = 1) "
                            strQ &= " AND (isexternalvacancy = 0 AND ISNULL(isbothintext, 0) = 0) "
                            'Sohail (18 Apr 2020) -- End
                            'Sohail (13 Mar 2020) -- End

                            'Sohail (18 Apr 2020) -- Start
                            'NMB Enhancement # : Need a filter type on vacancy type called Internal and External.
                        Case enVacancyType.INTERNAL_AND_EXTERNAL
                            strQ &= " AND ISNULL(isbothintext, 0) = 1 "
                            'Sohail (18 Apr 2020) -- End

                    End Select
                End If



                'Pinkal (07-Jan-2012) -- Start
                'Enhancement : TRA Changes

                If blnCheckClosingDate Then

                    'Shani(24-Aug-2015) -- Start
                    'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
                    'strQ &= " AND convert(char(8),closingdate,112) <= '" & eZeeDate.convertDate(ConfigParameter._Object._CurrentDateAndTime.Date) & "'"
                    strQ &= " AND convert(char(8),closingdate,112) <= '" & eZeeDate.convertDate(dtCurrentDateAndTime) & "'"
                    'Shani(24-Aug-2015) -- End

                End If

                If blnNotShowFutureVacancy = True Then

                    'Shani(24-Aug-2015) -- Start
                    'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
                    'objDataOperation.AddParameter("@mydate", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, eZeeDate.convertDate(ConfigParameter._Object._CurrentDateAndTime.Date))
                    objDataOperation.AddParameter("@mydate", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, eZeeDate.convertDate(dtCurrentDateAndTime))
                    'Shani(24-Aug-2015) -- End

                    'S.SANDEEP [ 02 MAY 2012 ] -- START
                    'ENHANCEMENT : TRA CHANGES
                    'strQ &= " AND CONVERT(CHAR(8),openingdate,112) BETWEEN CONVERT(VARCHAR(8),DATEADD(dd,-(DAY(@mydate)-1),@mydate),112) AND CONVERT(VARCHAR(8),DATEADD(dd,-(DAY(DATEADD(mm,1,@mydate))),DATEADD(mm,1,@mydate)),112) AND CONVERT(CHAR(8),closingdate,112) >= '" & eZeeDate.convertDate(ConfigParameter._Object._CurrentDateAndTime.Date) & "'"

                    'Shani(24-Aug-2015) -- Start
                    'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
                    'strQ &= " AND (CONVERT(CHAR(8),openingdate,112) BETWEEN CONVERT(VARCHAR(8),DATEADD(dd,-(DAY(@mydate)-1),@mydate),112) AND CONVERT(VARCHAR(8),DATEADD(dd,-(DAY(DATEADD(mm,1,@mydate))),DATEADD(mm,1,@mydate)),112) OR CONVERT(CHAR(8),closingdate,112) >= '" & eZeeDate.convertDate(ConfigParameter._Object._CurrentDateAndTime.Date) & "')"
                    strQ &= " AND (CONVERT(CHAR(8),openingdate,112) BETWEEN CONVERT(VARCHAR(8),DATEADD(dd,-(DAY(@mydate)-1),@mydate),112) AND CONVERT(VARCHAR(8),DATEADD(dd,-(DAY(DATEADD(mm,1,@mydate))),DATEADD(mm,1,@mydate)),112) OR CONVERT(CHAR(8),closingdate,112) >= '" & eZeeDate.convertDate(dtCurrentDateAndTime) & "')"
                    'Shani(24-Aug-2015) -- End

                    'S.SANDEEP [ 02 MAY 2012 ] -- END
                End If

                'Pinkal (07-Jan-2012) -- End

                'Sohail (05 Jun 2020) -- Start
                'NMB Enhancement # : Upon successful application of a vacancy for internal vacancies, system to copy the reporting-to on the reverse email sent back to the employee.
                If strFilter.Trim <> "" Then
                    strQ &= " " & strFilter & " "
                End If
                'Sohail (05 Jun 2020) -- End

                strQ &= " ORDER BY  id "
            End If
            'S.SANDEEP [ 25 DEC 2011 ] -- END

            dsList = objDataOperation.ExecQuery(strQ, strListName)
            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            'S.SANDEEP [ 28 FEB 2012 ] -- START
            'ENHANCEMENT : TRA CHANGES
            If blnAddDatesInName = True Then 'Sohail (05 Jun 2020)
            For Each dRow As DataRow In dsList.Tables(0).Rows
                If dRow.Item("id") <= 0 Then Continue For
                'S.SANDEEP [ 04 MAY 2012 ] -- START
                'ENHANCEMENT : TRA CHANGES
                'dRow.Item("name") = dRow.Item("name") & " ( " & eZeeDate.convertDate(dRow.Item("SDate").ToString).ToShortDateString & " - " & eZeeDate.convertDate(dRow.Item("EDate").ToString).ToShortDateString & " ) "
                dRow.Item("name") = dRow.Item("name").ToString.Trim & " ( " & eZeeDate.convertDate(dRow.Item("SDate").ToString).ToShortDateString & " - " & eZeeDate.convertDate(dRow.Item("EDate").ToString).ToShortDateString & " ) "
                'S.SANDEEP [ 04 MAY 2012 ] -- END
            Next
            End If 'Sohail (05 Jun 2020)
            'S.SANDEEP [ 28 FEB 2012 ] -- END

            Return dsList
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: getComboList; Module Name: " & mstrModuleName)
            Return Nothing
        Finally
            If dsList IsNot Nothing Then dsList.Dispose()
            dsList = Nothing
            objDataOperation.Dispose()
            objDataOperation = Nothing

            exForce = Nothing
        End Try
    End Function

    Public Function getVacancyType(Optional ByVal blnIncludeExternalInternal As Boolean = True) As DataSet
        'Sohail (25 Sep 2020) - [blnIncludeExternalInternal]
        Dim StrQ As String = ""
        Dim exForce As Exception
        Dim dsList As New DataSet
        Dim objDataOperation As New clsDataOperation
        Try
            StrQ &= "SELECT 0 AS Id, @Select AS Name " & _
                    "UNION SELECT " & enVacancyType.EXTERNAL_VACANCY & " AS Id, @External AS Name " & _
                    "UNION SELECT " & enVacancyType.INTERNAL_VACANCY & " AS Id, @Internal AS Name "

            If blnIncludeExternalInternal = True Then 'Sohail (25 Sep 2020)
                StrQ &= "UNION SELECT " & enVacancyType.INTERNAL_AND_EXTERNAL & " AS Id, @INTERNAL_AND_EXTERNAL AS Name "
            End If 'Sohail (25 Sep 2020)

            objDataOperation.AddParameter("@Select", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 4, "Select"))
            objDataOperation.AddParameter("@External", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 5, "External"))
            objDataOperation.AddParameter("@Internal", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 6, "Internal"))
            'Sohail (18 Apr 2020) -- Start
            'NMB Enhancement # : Need a filter type on vacancy type called Internal and External.
            objDataOperation.AddParameter("@INTERNAL_AND_EXTERNAL", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 7, "Internal and External"))
            'Sohail (18 Apr 2020) -- End

            dsList = objDataOperation.ExecQuery(StrQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & " " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            Return dsList

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: getVacancyType; Module Name: " & mstrModuleName)
        Finally
            dsList.Dispose()
            StrQ = ""
        End Try
    End Function

    'Sohail (13 Mar 2020) -- Start
    'NMB Enhancement # : System should not shortlist suspended employees on shortlisting and final shortlisting.
    <DataObjectMethod(DataObjectMethodType.Select, True)> _
    Public Shared Function GetApplicantVacancies(ByVal strDatabaseName As String _
                                                , ByVal intMasterTypeId As Integer _
                                                , ByVal intEType As Integer _
                                                , ByVal blnVacancyType As Boolean _
                                                , ByVal blnAllVacancy As Boolean _
                                                , ByVal intDateZoneDifference As Integer _
                                                , ByVal strVacancyUnkIdLIs As String _
                                                , ByVal intApplicantUnkId As Integer _
                                                , Optional ByVal blnOnlyCurrent As Boolean = True _
                                                , Optional ByVal blnOnlyExportToWeb As Boolean = True _
                                                ) As DataTable
        'Sohail (14 Oct 2020) - [blnOnlyExportToWeb]

        Dim ds As New DataSet
        Dim dt As DataTable = Nothing 'Sohail (25 Sep 2020)
        Dim strQ As String = ""
        Dim dsApplied As DataSet

        Dim objDataOperation As New clsDataOperation

        Try
            If strVacancyUnkIdLIs Is Nothing Then strVacancyUnkIdLIs = ""

            strQ = "SELECT  t1.jobunkid  " & _
                          ", STUFF(( SELECT  '; ' + hrskill_master.skillname " & _
                                    "FROM    " & strDatabaseName & "..hrjob_skill_tran t2 " & _
                                            "JOIN " & strDatabaseName & "..hrskill_master ON t2.skillunkid = hrskill_master.skillunkid " & _
                                    "WHERE   t1.jobunkid = t2.jobunkid " & _
                                            "AND t2.isactive = 1 " & _
                                    "FOR " & _
                                    "XML PATH('') " & _
                                    "), 1, 1, '') Skill " & _
                            "INTO #Vac_Skill " & _
                    "FROM    " & strDatabaseName & "..hrjob_skill_tran t1 " & _
                            "JOIN " & strDatabaseName & "..hrskill_master ON t1.skillunkid = hrskill_master.skillunkid " & _
                    "WHERE   t1.isactive = 1 " & _
                    "GROUP BY t1.jobunkid " & _
                    " " & _
                    "SELECT  t1.jobunkid  " & _
                          ", STUFF(( SELECT  ', ' " & _
                                            "+ hrqualification_master.qualificationname " & _
                                    "FROM    " & strDatabaseName & "..hrjob_qualification_tran t2 " & _
                                            "JOIN " & strDatabaseName & "..hrqualification_master ON t2.qualificationunkid = hrqualification_master.qualificationunkid " & _
                                    "WHERE   t1.jobunkid = t2.jobunkid " & _
                                            "AND t2.isactive = 1 " & _
                                    "FOR " & _
                                    "XML PATH('') " & _
                                    "), 1, 1, '') Qualification " & _
                            "INTO #Vac_Quali " & _
                    "FROM    " & strDatabaseName & "..hrjob_qualification_tran t1 " & _
                            "JOIN " & strDatabaseName & "..hrqualification_master ON t1.qualificationunkid = hrqualification_master.qualificationunkid " & _
                    "WHERE   t1.isactive = 1 " & _
                    "GROUP BY t1.jobunkid " & _
                    " " & _
                    "SELECT  t1.jobunkid  " & _
                          ", STUFF(( SELECT  ', ' + cfcommon_master.name " & _
                                    "FROM    " & strDatabaseName & "..hrjob_language_tran t2 " & _
                                            "JOIN " & strDatabaseName & "..cfcommon_master ON cfcommon_master.masterunkid = t2.masterunkid " & _
                                    "WHERE   t1.jobunkid = t2.jobunkid " & _
                                            "AND t2.isactive = 1 " & _
                                    "FOR " & _
                                    "XML PATH('') " & _
                                    "), 1, 1, '') Lang " & _
                            "INTO #Vac_Lang " & _
                    "FROM    " & strDatabaseName & "..hrjob_language_tran t1 " & _
                            "JOIN " & strDatabaseName & "..cfcommon_master ON t1.masterunkid = cfcommon_master.masterunkid " & _
                    "WHERE   t1.isactive = 1 " & _
                    "GROUP BY t1.jobunkid " & _
                    " " & _
                    "SELECT  vacancyunkid AS vacancyid  " & _
                          ", ISNULL(cfcommon_master.masterunkid, 0) AS vacancytitleid " & _
                          ", ISNULL(cfcommon_master.name, '') AS vacancytitle " & _
                          ", ISNULL(cfcommon_master.name, '') + ' ( ' " & _
                            "+ CONVERT(CHAR(10), openingdate, 120) + ' - ' " & _
                            "+ CONVERT(CHAR(10), closingdate, 120) + ' )' AS vacancy_title  " & _
                          ", openingdate AS openingdate " & _
                          ", closingdate AS closingdate " & _
                          ", interview_Startdate AS interview_Startdate " & _
                          ", interview_closedate AS interview_closeddate " & _
                          ", experience AS experience " & _
                          ", responsibilities_duties AS duties " & _
                          ", remark AS remark " & _
                          ", EType.name AS EmployeementType " & _
                          ", rcvacancy_master.vacancytitle As vacancytitleunkid " & _
                          ", CAST(0 AS BIT) AS IsApplied " & _
                          ", ISNULL(Vac_Skill.Skill, '') AS skill " & _
                          ", ISNULL(Vac_Quali.Qualification, '') AS Qualification " & _
                          ", ISNULL(Vac_Lang.Lang, '') AS Lang " & _
                          ", ISNULL(rcvacancy_master.noofposition, 0) AS noposition " & _
                          ", ISNULL(pay_from, 0) AS pay_from " & _
                          ", ISNULL(pay_to, 0) AS pay_to " & _
                          ", ISNULL(isskillbold, 0) AS isskillbold " & _
                          ", ISNULL(isskillitalic, 0) AS isskillitalic " & _
                          ", ISNULL(isqualibold, 0) AS isqualibold " & _
                          ", ISNULL(isqualiitalic, 0) AS isqualiitalic " & _
                          ", ISNULL(isexpbold, 0) AS isexpbold " & _
                          ", ISNULL(isexpitalic, 0) AS isexpitalic " & _
                          ", ISNULL(hrjob_master.experience_comment, '') AS experience_comment " & _
                          ", ROW_NUMBER() OVER ( ORDER BY rcvacancy_master.closingdate DESC, ISNULL(cfcommon_master.name, '') ) AS ROWNO " & _
                          ", ISNULL(rcvacancy_master.other_qualification, '') AS other_qualification " & _
                          ", ISNULL(rcvacancy_master.other_skill, '') AS other_skill " & _
                          ", ISNULL(rcvacancy_master.other_language, '') AS other_language " & _
                          ", ISNULL(rcvacancy_master.other_experience, '') AS other_experience " & _
                          ", ISNULL(hrclassgroup_master.name, '') AS classgroupname " & _
                          ", ISNULL(hrclasses_master.name, '') AS classname " & _
                    "FROM    " & strDatabaseName & "..rcvacancy_master " & _
                            "JOIN " & strDatabaseName & "..cfcommon_master ON cfcommon_master.masterunkid = rcvacancy_master.vacancytitle " & _
                            "LEFT JOIN " & strDatabaseName & "..cfcommon_master AS EType ON cfcommon_master.masterunkid = rcvacancy_master.employeementtypeunkid " & _
                                                                  "AND EType.mastertype = ISNULL(@EType, EType.mastertype) " & _
                            "LEFT JOIN #Vac_Skill AS Vac_Skill ON rcvacancy_master.jobunkid = Vac_Skill.jobunkid " & _
                            "LEFT JOIN #Vac_Quali AS Vac_Quali ON rcvacancy_master.jobunkid = Vac_Quali.jobunkid " & _
                            "LEFT JOIN #Vac_Lang AS Vac_Lang ON rcvacancy_master.jobunkid = Vac_Lang.jobunkid " & _
                            "LEFT JOIN " & strDatabaseName & "..hrjob_master ON hrjob_master.jobunkid = rcvacancy_master.jobunkid and hrjob_master.isactive = 1 " & _
                            "LEFT JOIN " & strDatabaseName & "..hrclassgroup_master ON rcvacancy_master.classgroupunkid = hrclassgroup_master.classgroupunkid AND hrclassgroup_master.isactive = 1 " & _
                            "LEFT JOIN " & strDatabaseName & "..hrclasses_master ON rcvacancy_master.classunkid = hrclasses_master.classesunkid  AND hrclasses_master.isactive = 1 " & _
                    "WHERE   rcvacancy_master.isvoid = 0 " & _
                            "AND cfcommon_master.mastertype = @mastertypeid " & _
                            "AND (isexternalvacancy = ISNULL(@VacancyType, isexternalvacancy) OR ISNULL(isbothintext, 0) = 1) " & _
                            "AND ((@AllVacancy = CAST(1 AS BIT)) OR CONVERT(CHAR(8), closingdate, 112) >= (CASE WHEN @AllVacancy = CAST(0 AS BIT) THEN CASE WHEN @DateZoneDifference <> 0 THEN CONVERT(CHAR(8), DATEADD(MINUTE,@DateZoneDifference,GETDATE()),112) ELSE CONVERT(CHAR(8),GETDATE(),112) END END )) " & _
                            "AND (@onlycurrent =0 OR ((@AllVacancy = CAST(1 AS BIT)) OR CONVERT(CHAR(8), openingdate, 112) <= (CASE WHEN @AllVacancy = CAST(0 AS BIT) THEN CASE WHEN @DateZoneDifference <> 0 THEN CONVERT(CHAR(8), DATEADD(MINUTE,@DateZoneDifference,GETDATE()),112) ELSE CONVERT(CHAR(8),GETDATE(),112) END END ))) "


            'Hemant (01 Nov 2021) -- [classgroupname,classname]
            'Pinkal (13-Sep-2021)-- NMB Enhancement : Adding New Fields in Vacancy Master required by NMB.[ISNULL(other_qualification, '') AS other_qualification,ISNULL(other_skill, '') AS other_skill,ISNULL(other_language, '') AS other_language,ISNULL(other_experience, '') AS other_experience]

            'Sohail (12 Nov 2020) - [LEFT JOIN hrjob_master = LEFT JOIN " & strDatabaseName & "..hrjob_master][NMB Issue : invalid object name hrjob_master on preview external / internal vacancy link click on vacancy master screen]
            'Sohail (25 Sep 2020) - [experience_comment, rcvacancy_master]

            If strVacancyUnkIdLIs.Trim <> "" Then
                strQ &= " AND rcvacancy_master.vacancyunkid IN (" & strVacancyUnkIdLIs & ") "
            End If

            'Sohail (14 Oct 2020) -- Start
            'NMB Enhancement # : - Allow to view only marked as Export to Web vacancies in ESS.
            If blnOnlyExportToWeb = True Then
                strQ &= " AND ISNULL(rcvacancy_master.iswebexport, 0) = 1 "
            End If
            'Sohail (14 Oct 2020) -- End

            strQ &= "DROP TABLE #Vac_Skill " & _
                    "DROP TABLE #Vac_Quali " & _
                    "DROP TABLE #Vac_Lang "

            objDataOperation.ClearParameters()
            objDataOperation.AddParameter("@mastertypeid", SqlDbType.Int, eZeeDataType.INT_SIZE, intMasterTypeId)
            objDataOperation.AddParameter("@EType", SqlDbType.Int, eZeeDataType.INT_SIZE, intEType)
            objDataOperation.AddParameter("@VacancyType", SqlDbType.Bit, eZeeDataType.BIT_SIZE, blnVacancyType)
            objDataOperation.AddParameter("@AllVacancy", SqlDbType.Bit, eZeeDataType.BIT_SIZE, blnAllVacancy)
            objDataOperation.AddParameter("@DateZoneDifference", SqlDbType.Int, eZeeDataType.INT_SIZE, intDateZoneDifference)
            objDataOperation.AddParameter("@onlycurrent", SqlDbType.Bit, eZeeDataType.BIT_SIZE, blnOnlyCurrent)

            ds = objDataOperation.ExecQuery(strQ, "VacancyList")

            If objDataOperation.ErrorMessage <> "" Then
                Throw New Exception(objDataOperation.ErrorNumber & " " & objDataOperation.ErrorMessage)
            End If

            'Sohail (25 Sep 2020) -- Start
            'NMB Enhancement : OLD-75 #  : Search job page with apply button in ESS (same as MSS, only current open vacancies)..
            'dt = ds.Tables(0)
            If intApplicantUnkId > 0 Then
                dsApplied = clsApplicant_Vacancy_Mapping.GetApplicantAppliedJob(intApplicantUnkId:=intApplicantUnkId)

                If dsApplied IsNot Nothing AndAlso dsApplied.Tables(0).Rows.Count > 0 Then
                    Dim str = String.Join(",", dsApplied.Tables(0).AsEnumerable().Select(Function(x) x.Field(Of Integer)("vacancyid").ToString).ToArray)
                    For Each dRow As DataRow In ds.Tables(0).Select("vacancyid IN (" & str & ")")
                        dRow.Item("IsApplied") = True
                    Next
                    ds.AcceptChanges()
                End If

            End If

            If ds IsNot Nothing AndAlso ds.Tables(0).Rows.Count > 0 Then
                dt = New DataView(ds.Tables(0), "IsApplied = 0", "IsApplied ASC, vacancytitle ASC", DataViewRowState.CurrentRows).ToTable
            End If
            'Sohail (25 Sep 2020) -- End

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: GetApplicantVacancies; Module Name: " & mstrModuleName)
        End Try
        Return dt
    End Function
    'Sohail (13 Mar 2020) -- End


	'<Language> This Auto Generated Text Please Do Not Modify it.
#Region " Language & UI Settings "
	Public Shared Sub SetMessages()
		Try
			Language.setMessage(mstrModuleName, 1, "This vacancy is aleardy define. Please define new vacancy.")
			Language.setMessage(mstrModuleName, 2, "Select")
			Language.setMessage(mstrModuleName, 3, "Sorry, you cannot delete this Vacancy. Reason : This Vacancy is already linked with some transaction.")
			Language.setMessage(mstrModuleName, 4, "Select")
			Language.setMessage(mstrModuleName, 5, "External")
			Language.setMessage(mstrModuleName, 6, "Internal")
			Language.setMessage(mstrModuleName, 7, "Internal and External")

		Catch Ex As Exception
			DisplayError.Show("-1", ex.Message, "SetMessages", mstrModuleName)
		End Try
	End Sub
#End Region 'Language & UI Settings
	'</Language>
End Class
