'************************************************************************************************************************************
'Class Name : clsApplicationSettings.vb
'Purpose    : Read Write Application Level Settings in Windows Registry.
'Date       : 
'Written By : Jitu
'Modified   : Naimish
'Note       : MergeJitu
'************************************************************************************************************************************
Imports eZeeCommonLib


''' <summary>
''' Read Write Application Level Settings in Windows Registry.
''' </summary>
Public Class clsApplicationSettings
    Private ReadOnly mstrModuleName As String = "clsApplicationSettings"
    Private objReg As clsRegistry

    Private Const conREG_NODE = "Software\NPK\Aruti"

    ''' <summary>
    ''' Constructor 
    ''' </summary>
    Public Sub New()
        objReg = New clsRegistry
    End Sub

    ''' <summary>
    ''' Destructors
    ''' </summary>
    Protected Overrides Sub Finalize()
        objReg = Nothing
        MyBase.Finalize()
    End Sub

#Region " Properties "

'S.SANDEEP [28-May-2018] -- START 
'ISSUE/ENHANCEMENT : {Audit Trails} 
Private mstrFormName As String = String.Empty
Public WriteOnly Property _FormName() As String 
Set(ByVal value As String) 
mstrFormName = value 
End Set 
End Property 
 
Private mstrClientIP As String = ""
Public WriteOnly Property _ClientIP() As String 
Set(ByVal value As String) 
mstrClientIP = value 
End Set 
End Property 
 
Private mstrHostName As String = ""
Public WriteOnly Property _HostName() As String 
Set(ByVal value As String) 
mstrHostName = value 
End Set 
End Property 
 
Private mblnIsWeb As Boolean = False
Public WriteOnly Property _FromWeb() As Boolean 
Set(ByVal value As Boolean) 
mblnIsWeb = value 
End Set 
End Property 
 
    Private mintAuditUserId As Integer = 0
    Public WriteOnly Property _AuditUserId() As Integer
        Set(ByVal value As Integer)
            mintAuditUserId = value
        End Set
    End Property

    Private mintCompanyUnkid As Integer = 0
    Public WriteOnly Property _CompanyUnkid() As Integer
        Set(ByVal value As Integer)
            mintCompanyUnkid = value
        End Set
    End Property
 
Private mdtAuditDate As DateTime = Now
Public WriteOnly Property _AuditDate() As DateTime 
Set(ByVal value As DateTime) 
mdtAuditDate = value 
End Set 
End Property 
 
Private xDataOpr As clsDataOperation = Nothing
Public Property _xDataOpr() As clsDataOperation 
Get 
Return xDataOpr 
End Get 
Set(ByVal value As clsDataOperation) 
xDataOpr = value 
End Set 
End Property 
 
Private mintLoginEmployeeunkid As Integer = 0
Public Property _LoginEmployeeunkid() As Integer 
Get 
Return mintLoginEmployeeunkid 
End Get 
Set(ByVal value As Integer) 
mintLoginEmployeeunkid = value 
End Set 
End Property 
 
Private mintVoidlogingemployeeunkid As Integer=0
Public Property _Voidloginemployeeunkid() As Integer 
Get 
Return mintVoidlogingemployeeunkid 
End Get 
Set(ByVal value As Integer) 
mintVoidlogingemployeeunkid = value 
End Set 
End Property 
'S.SANDEEP [28-May-2018] -- END 

    ''' <summary>
    ''' Get or set application directory path from windows registry. 
    ''' </summary>
    Public Property _ApplicationPath() As String
        Get
            Dim strAppPath As String = ""
            If objReg.getValue(objReg.HKeyLocalMachine, conREG_NODE, "AppPath", strAppPath) Then
                Return strAppPath
            Else
                Return ""
            End If
        End Get
        Set(ByVal value As String)
            objReg.setValue(objReg.HKeyLocalMachine, conREG_NODE, "AppPath", value)
        End Set
    End Property
    Public Property _SetDefaultExportAction() As enExportAction
        Get
            Dim ExportAction As enExportAction = enExportAction.HTML
            If objReg.getValue(objReg.HKeyLocalMachine, conREG_NODE, "_SetDefaultExportAction", ExportAction) Then
                If ExportAction = enExportAction.None Then
                    Return enExportAction.HTML
                Else
                    Return ExportAction
                End If
            Else
                Return enExportAction.HTML
            End If

        End Get
        Set(ByVal value As enExportAction)
            objReg.setValue(objReg.HKeyLocalMachine, conREG_NODE, "_SetDefaultExportAction", Val(value))
        End Set
    End Property

    Public Property _SetDefaultPrintAction() As enPrintAction
        Get
            Dim PrintAction As enPrintAction = enPrintAction.Preview
            If objReg.getValue(objReg.HKeyLocalMachine, conREG_NODE, "_SetDefaultPrintAction", PrintAction) Then
                If PrintAction = enPrintAction.None Then
                    Return enPrintAction.Preview
                Else
                    Return PrintAction
                End If
            Else
                Return enPrintAction.Preview
            End If
        End Get
        Set(ByVal value As enPrintAction)
            objReg.setValue(objReg.HKeyLocalMachine, conREG_NODE, "_SetDefaultPrintAction", Val(value))
        End Set
    End Property

    Public Property _LastViewAccessed() As Integer
        Get
            Dim intLastView As Integer = 0
            If objReg.getValue(objReg.HKeyLocalMachine, conREG_NODE, "_LastViewAccessed", intLastView) Then
                Return intLastView
            End If
        End Get
        Set(ByVal value As Integer)
            objReg.setValue(objReg.HKeyLocalMachine, conREG_NODE, "_LastViewAccessed", Val(value))
        End Set
    End Property
    ''Jitu (03 Oct 2009) --Start
    'Public Property _LanguageId() As Integer
    '    Get
    '        Dim strLangId As String = ""
    '        If objReg.getValue(objReg.HKeyLocalMachine, conREG_NODE, "LangId", strLangId) Then
    '            Return CInt(strLangId)
    '        Else
    '            Return 0
    '        End If
    '    End Get
    '    Set(ByVal value As Integer)
    '        objReg.setValue(objReg.HKeyLocalMachine, conREG_NODE, "LangId", CStr(value))
    '    End Set
    'End Property
    ''Jitu (03 Oct 2009) --End

    ''Krishna (27 Jan 2010) -- Start
    'Public Property _SearchAnyChar() As Boolean
    '    Get
    '        Dim blnSearchAnyChar As Boolean = True
    '        objReg.getValue(objReg.HKeyLocalMachine, conREG_NODE, "SearchAnyChar", blnSearchAnyChar)

    '        Return blnSearchAnyChar
    '    End Get
    '    Set(ByVal value As Boolean)
    '        objReg.setValue(objReg.HKeyLocalMachine, conREG_NODE, "SearchAnyChar", value)
    '    End Set
    'End Property
    'Krishna (27 Jan 2010) -- End

    'Sandeep [ 01 FEB 2011 ] -- START
    Public ReadOnly Property _ClearCompatibility() As Boolean
        Get
            Dim blnIsClear As Boolean = False
            Dim mstrPath As String = "Software\Microsoft\Windows NT\CurrentVersion\AppCompatFlags\Layers"
            Dim file() As String = Nothing

            If objReg.getValues(objReg.HKeyCurrentUser, mstrPath, file) Then
                If file.Length > 0 Then
                    For Each strpath As String In file
                        If strpath.ToString.Substring(0, strpath.LastIndexOf("\") + 1) = _ApplicationPath Then
                            objReg.setValue(objReg.HKeyCurrentUser, mstrPath, strpath, "")
                            blnIsClear = True
                        End If
                    Next
                End If
            End If

            Return blnIsClear
        End Get
    End Property
    'Sandeep [ 01 FEB 2011 ] -- END

'S.SANDEEP [ 30 May 2011 ] -- START
    Public Property _IsWizardRun() As Boolean
        Get
            Dim blnFlag As Boolean = False
            If objReg.getValue(objReg.HKeyLocalMachine, conREG_NODE, "IsWizardRun", blnFlag) Then
                Return blnFlag
            End If
        End Get
        Set(ByVal value As Boolean)
            objReg.setValue(objReg.HKeyLocalMachine, conREG_NODE, "IsWizardRun", value)
        End Set
    End Property
    'S.SANDEEP [ 30 May 2011 ] -- END 

    'S.SANDEEP [ 08 June 2011 ] -- START
    Public Property _LastUserId() As Integer
        Get
            Dim intLastUId As Integer = 1
            If objReg.getValue(objReg.HKeyLocalMachine, conREG_NODE, "_LastUserId", intLastUId) Then
                Return intLastUId
            End If

        End Get
        Set(ByVal value As Integer)
            objReg.setValue(objReg.HKeyLocalMachine, conREG_NODE, "_LastUserId", Val(value))
        End Set
    End Property

    Public Property _LastCompanyId() As Integer
        Get
            Dim intLastCId As Integer = 1
            If objReg.getValue(objReg.HKeyLocalMachine, conREG_NODE, "_LastCompanyId", intLastCId) Then
                Return intLastCId
            End If

        End Get
        Set(ByVal value As Integer)
            objReg.setValue(objReg.HKeyLocalMachine, conREG_NODE, "_LastCompanyId", Val(value))
        End Set
    End Property

    Public Property _LastDatabaseId() As Integer
        Get
            Dim intLastDBId As Integer = 1
            If objReg.getValue(objReg.HKeyLocalMachine, conREG_NODE, "_LastDatabaseId", intLastDBId) Then
                Return intLastDBId
            End If

        End Get
        Set(ByVal value As Integer)
            objReg.setValue(objReg.HKeyLocalMachine, conREG_NODE, "_LastDatabaseId", Val(value))
        End Set
    End Property
    'S.SANDEEP [ 08 June 2011 ] -- START

    'S.SANDEEP [ 09 AUG 2011 ] -- START
    'ENHANCEMENT : AUTO BACKUP & AUTO UPDATE
    Public Property _IsAutoUpdateEnabled() As Boolean
        Get
            Dim mblnIsAutoUpdateEnabled As Boolean = False
            If objReg.getValue(objReg.HKeyLocalMachine, conREG_NODE, "_IsAutoUpdateEnabled", mblnIsAutoUpdateEnabled) Then
                Return mblnIsAutoUpdateEnabled
            End If
        End Get
        Set(ByVal value As Boolean)
            objReg.setValue(objReg.HKeyLocalMachine, conREG_NODE, "_IsAutoUpdateEnabled", CBool(value))
        End Set
    End Property
    'S.SANDEEP [ 09 AUG 2011 ] -- END 

    Public Property _IsClient() As Integer
        Get
            Dim intIsClient As Integer = 0
            If objReg.getValue(objReg.HKeyLocalMachine, conREG_NODE, "IsClient", intIsClient) Then
                Return intIsClient
            End If
        End Get
        Set(ByVal value As Integer)
            objReg.setValue(objReg.HKeyLocalMachine, conREG_NODE, "IsClient", Val(value))
        End Set
    End Property

    'S.SANDEEP [09-AUG-2018] -- START
    'ISSUE/ENHANCEMENT : {Ref#292}
    Public Property _FlexcubeRunningTime() As String
        Get
            Dim strTime As String = ""
            If objReg.getValue(objReg.HKeyLocalMachine, conREG_NODE, "FSrvTime", strTime) Then
                Return strTime
            Else
                Return "00:05"
            End If
        End Get
        Set(ByVal value As String)
            objReg.setValue(objReg.HKeyLocalMachine, conREG_NODE, "FSrvTime", value)
        End Set
    End Property
    'S.SANDEEP [09-AUG-2018] -- END

#End Region

End Class
