﻿'************************************************************************************************************************************
'Class Name : clsmedical_sicksheet.vb
'Purpose    :
'Date       :11/17/2011
'Written By :Pinkal
'Modified   :
'************************************************************************************************************************************

Imports eZeeCommonLib

''' <summary>
''' Purpose: 
''' Developer: Pinkal
''' </summary>
Public Class clsmedical_sicksheet
    Private Shared ReadOnly mstrModuleName As String = "clsmedical_sicksheet"
    Dim mstrMessage As String = ""

#Region " Private variables "
    Private mintSicksheetunkid As Integer
    Private mstrSicksheetno As String = String.Empty
    Private mdtSicksheetdate As Date
    Private mintInstituteunkid As Integer
    Private mintEmployeeunkid As Integer
    Private mintJobunkid As Integer
    Private mintMedicalcoverunkid As Integer
    Private mstrDependantunkid As String = String.Empty
    Private mintUserunkid As Integer
    Private mblnIsvoid As Boolean
    Private mintVoiduserunkid As Integer
    Private mdtVoiddatetime As Date
    Private mstrVoidreason As String = String.Empty


    'Pinkal (12-Sep-2014) -- Start
    'Medical Enhancement - TANAPA URGENT CHANGES FOR MEDICAL SICK SHEET GIVEN BY OLAIS
    Private mblnRevokeUserAccessOnSickSheet As Boolean = False
    Private mintAllocationTypeId As Integer = 0
    Private mintAllocationTranunkId As Integer = 0
    'Pinkal (12-Sep-2014) -- End


#End Region

#Region " Properties "
'S.SANDEEP [28-May-2018] -- START 
'ISSUE/ENHANCEMENT : {Audit Trails} 
Private mstrFormName As String = String.Empty
Public WriteOnly Property _FormName() As String 
Set(ByVal value As String) 
mstrFormName = value 
End Set 
End Property 
 
Private mstrClientIP As String = ""
Public WriteOnly Property _ClientIP() As String 
Set(ByVal value As String) 
mstrClientIP = value 
End Set 
End Property 
 
Private mstrHostName As String = ""
Public WriteOnly Property _HostName() As String 
Set(ByVal value As String) 
mstrHostName = value 
End Set 
End Property 
 
Private mblnIsWeb As Boolean = False
Public WriteOnly Property _FromWeb() As Boolean 
Set(ByVal value As Boolean) 
mblnIsWeb = value 
End Set 
End Property 
 
    Private mintAuditUserId As Integer = 0
    Public WriteOnly Property _AuditUserId() As Integer
        Set(ByVal value As Integer)
            mintAuditUserId = value
        End Set
    End Property

    Private mintCompanyUnkid As Integer = 0
    Public WriteOnly Property _CompanyUnkid() As Integer
        Set(ByVal value As Integer)
            mintCompanyUnkid = value
        End Set
    End Property
 
Private mdtAuditDate As DateTime = Now
Public WriteOnly Property _AuditDate() As DateTime 
Set(ByVal value As DateTime) 
mdtAuditDate = value 
End Set 
End Property 
 
Private xDataOpr As clsDataOperation = Nothing
Public Property _xDataOpr() As clsDataOperation 
Get 
Return xDataOpr 
End Get 
Set(ByVal value As clsDataOperation) 
xDataOpr = value 
End Set 
End Property 
 
Private mintLoginEmployeeunkid As Integer = 0
Public Property _LoginEmployeeunkid() As Integer 
Get 
Return mintLoginEmployeeunkid 
End Get 
Set(ByVal value As Integer) 
mintLoginEmployeeunkid = value 
End Set 
End Property 
 
Private mintVoidlogingemployeeunkid As Integer=0
Public Property _Voidloginemployeeunkid() As Integer 
Get 
Return mintVoidlogingemployeeunkid 
End Get 
Set(ByVal value As Integer) 
mintVoidlogingemployeeunkid = value 
End Set 
End Property 
'S.SANDEEP [28-May-2018] -- END 
    ''' <summary>
    ''' Purpose: Get Message from Class 
    ''' Modify By: Pinkal
    ''' </summary>
    Public ReadOnly Property _Message() As String
        Get
            Return mstrMessage
        End Get
    End Property

    ''' <summary>
    ''' Purpose: Get or Set sicksheetunkid
    ''' Modify By: Pinkal
    ''' </summary>
    Public Property _Sicksheetunkid() As Integer
        Get
            Return mintSicksheetunkid
        End Get
        Set(ByVal value As Integer)
            mintSicksheetunkid = Value
            Call getData()
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set sicksheetno
    ''' Modify By: Pinkal
    ''' </summary>
    Public Property _Sicksheetno() As String
        Get
            Return mstrSicksheetno
        End Get
        Set(ByVal value As String)
            mstrSicksheetno = Value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set sicksheetdate
    ''' Modify By: Pinkal
    ''' </summary>
    Public Property _Sicksheetdate() As Date
        Get
            Return mdtSicksheetdate
        End Get
        Set(ByVal value As Date)
            mdtSicksheetdate = Value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set instituteunkid
    ''' Modify By: Pinkal
    ''' </summary>
    Public Property _Instituteunkid() As Integer
        Get
            Return mintInstituteunkid
        End Get
        Set(ByVal value As Integer)
            mintInstituteunkid = Value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set employeeunkid
    ''' Modify By: Pinkal
    ''' </summary>
    Public Property _Employeeunkid() As Integer
        Get
            Return mintEmployeeunkid
        End Get
        Set(ByVal value As Integer)
            mintEmployeeunkid = Value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set jobunkid
    ''' Modify By: Pinkal
    ''' </summary>
    Public Property _Jobunkid() As Integer
        Get
            Return mintJobunkid
        End Get
        Set(ByVal value As Integer)
            mintJobunkid = Value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set medicalcoverunkid
    ''' Modify By: Pinkal
    ''' </summary>
    Public Property _Medicalcoverunkid() As Integer
        Get
            Return mintMedicalcoverunkid
        End Get
        Set(ByVal value As Integer)
            mintMedicalcoverunkid = Value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set dependantunkid
    ''' Modify By: Pinkal
    ''' </summary>
    Public Property _Dependantunkid() As String
        Get
            Return mstrDependantunkid
        End Get
        Set(ByVal value As String)
            mstrDependantunkid = Value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set userunkid
    ''' Modify By: Pinkal
    ''' </summary>
    Public Property _Userunkid() As Integer
        Get
            Return mintUserunkid
        End Get
        Set(ByVal value As Integer)
            mintUserunkid = Value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set isvoid
    ''' Modify By: Pinkal
    ''' </summary>
    Public Property _Isvoid() As Boolean
        Get
            Return mblnIsvoid
        End Get
        Set(ByVal value As Boolean)
            mblnIsvoid = Value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set voiduserunkid
    ''' Modify By: Pinkal
    ''' </summary>
    Public Property _Voiduserunkid() As Integer
        Get
            Return mintVoiduserunkid
        End Get
        Set(ByVal value As Integer)
            mintVoiduserunkid = Value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set voiddatetime
    ''' Modify By: Pinkal
    ''' </summary>
    Public Property _Voiddatetime() As Date
        Get
            Return mdtVoiddatetime
        End Get
        Set(ByVal value As Date)
            mdtVoiddatetime = Value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set voidreason
    ''' Modify By: Pinkal
    ''' </summary>
    Public Property _Voidreason() As String
        Get
            Return mstrVoidreason
        End Get
        Set(ByVal value As String)
            mstrVoidreason = Value
        End Set
    End Property

    'Pinkal (12-Sep-2014) -- Start
    'Medical Enhancement - TANAPA URGENT CHANGES FOR MEDICAL SICK SHEET GIVEN BY OLAIS

    ''' <summary>
    ''' Purpose: Get or Set RevokeUserAccessOnSickSheet
    ''' Modify By: Pinkal
    ''' </summary>
    Public Property _RevokeUserAccessOnSickSheet() As Boolean
        Get
            Return mblnRevokeUserAccessOnSickSheet
        End Get
        Set(ByVal value As Boolean)
            mblnRevokeUserAccessOnSickSheet = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set allocationtypeid
    ''' Modify By: Pinkal
    ''' </summary>
    Public Property _AllocationtypeId() As Integer
        Get
            Return mintAllocationTypeId
        End Get
        Set(ByVal value As Integer)
            mintAllocationTypeId = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set allocationtranunkid
    ''' Modify By: Pinkal
    ''' </summary>
    Public Property _Allocationtranunkid() As Integer
        Get
            Return mintAllocationTranunkId
        End Get
        Set(ByVal value As Integer)
            mintAllocationTranunkId = value
        End Set
    End Property

    'Pinkal (12-Sep-2014) -- End


#End Region

    ''' <summary>
    ''' Modify By: Pinkal
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>
    Public Sub GetData()
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        Dim objDataOperation As New clsDataOperation

        Try
            StrQ = "SELECT " & _
              "  sicksheetunkid " & _
              ", sicksheetno " & _
              ", sicksheetdate " & _
              ", instituteunkid " & _
              ", employeeunkid " & _
              ", jobunkid " & _
              ", medicalcoverunkid " & _
              ", dependantunkid " & _
              ", userunkid " & _
              ", isvoid " & _
              ", voiduserunkid " & _
              ", voiddatetime " & _
              ", voidreason " & _
              ", ISNULL(allocationtypeid,0) AS allocationtypeid " & _
              ", ISNULL(allocationtranunkid,0) AS allocationtranunkid " & _
             "FROM mdmedical_sicksheet " & _
             "WHERE sicksheetunkid = @sicksheetunkid "

            objDataOperation.AddParameter("@sicksheetunkid", SqlDbType.int, eZeeDataType.INT_SIZE, mintSicksheetUnkId.ToString)

            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            For Each dtRow As DataRow In dsList.Tables(0).Rows
                mintsicksheetunkid = CInt(dtRow.Item("sicksheetunkid"))
                mstrsicksheetno = dtRow.Item("sicksheetno").ToString
                mdtsicksheetdate = dtRow.Item("sicksheetdate")
                mintinstituteunkid = CInt(dtRow.Item("instituteunkid"))
                mintemployeeunkid = CInt(dtRow.Item("employeeunkid"))
                mintjobunkid = CInt(dtRow.Item("jobunkid"))
                mintmedicalcoverunkid = CInt(dtRow.Item("medicalcoverunkid"))
                mstrdependantunkid = dtRow.Item("dependantunkid").ToString
                mintuserunkid = CInt(dtRow.Item("userunkid"))
                mblnisvoid = CBool(dtRow.Item("isvoid"))
                mintVoiduserunkid = CInt(dtRow.Item("voiduserunkid"))

                If Not IsDBNull(dtRow.Item("voiddatetime")) Then
                    mdtVoiddatetime = dtRow.Item("voiddatetime")
                End If
                mstrVoidreason = dtRow.Item("voidreason").ToString

                'Pinkal (12-Sep-2014) -- Start
                'Medical Enhancement - TANAPA URGENT CHANGES FOR MEDICAL SICK SHEET GIVEN BY OLAIS
                mintAllocationTypeId = CInt(dtRow.Item("allocationtypeid"))
                mintAllocationTranunkId = CInt(dtRow.Item("allocationtranunkid"))
                'Pinkal (12-Sep-2014) -- End

                Exit For
            Next
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: GetData; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
    End Sub

    ''' <summary>
    ''' Modify By: Pinkal
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>
    '''Shani(24-Aug-2015) -- Start
    '''ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
    '''Public Function GetList(ByVal strTableName As String _
    '''                        , Optional ByVal blnOnlyActive As Boolean = True _
    '''                        , Optional ByVal mstrSickSheetNo As String = "" _
    '''                        , Optional ByVal intEmployeeunkid As Integer = -1 _
    '''                        , Optional ByVal blnIncludeInactiveEmployee As String = "" _
    '''                        , Optional ByVal strEmployeeAsOnDate As String = "" _
    '''                        , Optional ByVal strUserAccessLevelFilterString As String = "" _
    '''                        , Optional ByVal intUserUnkId As Integer = 0) As DataSet 'Sohail (23 Apr 2012)
    '''Public Function GetList(ByVal strTableName As String, Optional ByVal blnOnlyActive As Boolean = True, Optional ByVal mstrSickSheetNo As String = "", Optional ByVal intEmployeeunkid As Integer = -1) As DataSet
    Public Function GetList(ByVal strDatabaseName As String, _
                            ByVal intUserUnkid As Integer, _
                            ByVal intYearUnkid As Integer, _
                            ByVal intCompanyUnkid As Integer, _
                            ByVal dtPeriodStart As Date, _
                            ByVal dtPeriodEnd As Date, _
                            ByVal strUserModeSetting As String, _
                            ByVal blnOnlyApproved As Boolean, _
                            ByVal xIncludeIn_ActiveEmployee As Boolean, _
                            ByVal strTableName As String, _
                            Optional ByVal blnOnlyActive As Boolean = True, _
                            Optional ByVal mstrSickSheetNo As String = "", _
                            Optional ByVal intEmployeeunkid As Integer = -1, _
                            Optional ByVal mstrFilter As String = "") As DataSet
        'Shani(24-Aug-2015) -- End


        'Pinkal (06-Jan-2016) -- Start       'Enhancement - Working on Changes in SS for Leave Module.[ Optional ByVal mstrFilter As String = "")]


        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        Dim objDataOperation As New clsDataOperation

        Try
            'Shani(24-Aug-2015) -- Start
            'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
            Dim xDateJoinQry, xDateFilterQry, xUACQry, xUACFiltrQry, xAdvanceJoinQry As String
            xDateJoinQry = "" : xDateFilterQry = "" : xUACQry = "" : xUACFiltrQry = "" : xAdvanceJoinQry = ""
            Call GetDatesFilterString(xDateJoinQry, xDateFilterQry, dtPeriodStart, dtPeriodEnd, , , strDatabaseName)

            'Pinkal (06-Jan-2016) -- Start
            'Enhancement - Working on Changes in SS for Leave Module.
            'Call NewAccessLevelFilterString(xUACQry, xUACFiltrQry, dtPeriodEnd, blnOnlyApproved, strDatabaseName, intUserUnkid, intCompanyUnkid, intYearUnkid, strUserModeSetting)
            If mblnRevokeUserAccessOnSickSheet = False Then
            Call NewAccessLevelFilterString(xUACQry, xUACFiltrQry, dtPeriodEnd, blnOnlyApproved, strDatabaseName, intUserUnkid, intCompanyUnkid, intYearUnkid, strUserModeSetting)
            End If
            'Pinkal (06-Jan-2016) -- End


            'Shani(24-Aug-2015) -- End

            strQ = " SELECT distinct mdmedical_sicksheet.sicksheetunkid  " & _
                      ", mdmedical_sicksheet.sicksheetno  " & _
                      ", convert(char(8),mdmedical_sicksheet.sicksheetdate,112) AS sicksheetdate " & _
                      ", mdmedical_sicksheet.instituteunkid " & _
                      ", hrinstitute_master.institute_name " & _
                      ", mdmedical_sicksheet.employeeunkid " & _
                      ", ISNULL(hremployee_master.employeecode,'') as employeecode " & _
                      ", hremployee_master.firstname + ' ' + hremployee_master.othername + ' ' + hremployee_master.surname AS employeename " & _
                      ", mdmedical_sicksheet.jobunkid " & _
                      ", hrjob_master.job_name AS job " & _
                      ", mdmedical_sicksheet.medicalcoverunkid " & _
                      ", mdmedical_master.mdmastername AS cover " & _
                      ", dependantunkid " & _
                      ", mdmedical_sicksheet.userunkid " & _
                      ", mdmedical_sicksheet.isvoid " & _
                      ", mdmedical_sicksheet.voiduserunkid " & _
                      ", mdmedical_sicksheet.voiddatetime " & _
                      ", mdmedical_sicksheet.voidreason " & _
                      ", mdmedical_sicksheet.allocationtypeid " & _
                      ", mdmedical_sicksheet.allocationtranunkid " & _
                      " FROM mdmedical_sicksheet " & _
                      " JOIN hrinstitute_master ON mdmedical_sicksheet.instituteunkid = hrinstitute_master.instituteunkid " & _
                      " LEFT JOIN mdprovider_mapping ON mdprovider_mapping.providerunkid = hrinstitute_master.instituteunkid "

            If intUserUnkid <= 0 Then
                strQ &= " AND mdprovider_mapping.userunkid = " & User._Object._Userunkid
            Else
                strQ &= " AND mdprovider_mapping.userunkid = " & intUserUnkid
            End If

            'Shani(24-Aug-2015) -- Start
            'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
            'strQ &= " LEFT JOIN hremployee_master ON mdmedical_sicksheet.employeeunkid = hremployee_master.employeeunkid " & _
            '          " LEFT JOIN hrjob_master ON mdmedical_sicksheet.jobunkid = hrjob_master.jobunkid " & _
            '          " LEFT JOIN mdmedical_master ON mdmedical_sicksheet.medicalcoverunkid = mdmedical_master.mdmasterunkid AND mdmedical_master.mdmastertype_id = " & enMedicalMasterType.Cover & _
            '          " WHERE 1 = 1 " 'Sohail (23 Apr 2012) - [intUserUnkId]

            strQ &= " LEFT JOIN hremployee_master ON mdmedical_sicksheet.employeeunkid = hremployee_master.employeeunkid " & _
                      " LEFT JOIN hrjob_master ON mdmedical_sicksheet.jobunkid = hrjob_master.jobunkid " & _
                      " LEFT JOIN mdmedical_master ON mdmedical_sicksheet.medicalcoverunkid = mdmedical_master.mdmasterunkid AND mdmedical_master.mdmastertype_id = " & enMedicalMasterType.Cover & " "

            If xUACQry.Trim.Length > 0 Then
                strQ &= xUACQry
            End If

            If xDateJoinQry.Trim.Length > 0 Then
                strQ &= xDateJoinQry
            End If

            strQ &= " WHERE 1 = 1 "

            'Shani(24-Aug-2015) -- End
            If blnOnlyActive Then
                strQ &= " AND mdmedical_sicksheet.isvoid = 0 "
            End If

            objDataOperation.ClearParameters()

            'If UserAccessLevel._AccessLevel.Length > 0 Then
            '    strQ &= "  AND hremployee_master.jobunkid IN (" & UserAccessLevel._AccessLevel & ")  "
            'End If

            'Sohail (23 Apr 2012) -- Start
            'TRA - ENHANCEMENT
            'Select Case ConfigParameter._Object._UserAccessModeSetting
            '    Case enAllocation.BRANCH
            '        If UserAccessLevel._AccessLevel.Length > 0 Then
            '            strQ &= " AND  hremployee_master.stationunkid IN (" & UserAccessLevel._AccessLevel & ") "
            '        End If
            '    Case enAllocation.DEPARTMENT_GROUP
            '        If UserAccessLevel._AccessLevel.Length > 0 Then
            '            strQ &= " AND  hremployee_master.deptgroupunkid IN (" & UserAccessLevel._AccessLevel & ") "
            '        End If
            '    Case enAllocation.DEPARTMENT
            '        If UserAccessLevel._AccessLevel.Length > 0 Then
            '            strQ &= " AND  hremployee_master.departmentunkid IN (" & UserAccessLevel._AccessLevel & ") "
            '        End If
            '    Case enAllocation.SECTION_GROUP
            '        If UserAccessLevel._AccessLevel.Length > 0 Then
            '            strQ &= " AND  hremployee_master.sectiongroupunkid IN (" & UserAccessLevel._AccessLevel & ") "
            '        End If
            '    Case enAllocation.SECTION
            '        If UserAccessLevel._AccessLevel.Length > 0 Then
            '            strQ &= " AND  hremployee_master.sectionunkid IN (" & UserAccessLevel._AccessLevel & ") "
            '        End If
            '    Case enAllocation.UNIT_GROUP
            '        If UserAccessLevel._AccessLevel.Length > 0 Then
            '            strQ &= " AND  hremployee_master.unitgroupunkid IN (" & UserAccessLevel._AccessLevel & ") "
            '        End If
            '    Case enAllocation.UNIT
            '        If UserAccessLevel._AccessLevel.Length > 0 Then
            '            strQ &= " AND  hremployee_master.unitunkid IN (" & UserAccessLevel._AccessLevel & ") "
            '        End If
            '    Case enAllocation.TEAM
            '        If UserAccessLevel._AccessLevel.Length > 0 Then
            '            strQ &= " AND  hremployee_master.teamunkid IN (" & UserAccessLevel._AccessLevel & ") "
            '        End If
            '    Case enAllocation.JOB_GROUP
            '        If UserAccessLevel._AccessLevel.Length > 0 Then
            '            strQ &= " AND  hremployee_master.jobgroupunkid IN (" & UserAccessLevel._AccessLevel & ") "
            '        End If
            '    Case enAllocation.JOBS
            '        If UserAccessLevel._AccessLevel.Length > 0 Then
            '            strQ &= "  AND hremployee_master.jobunkid IN (" & UserAccessLevel._AccessLevel & ")  "
            '        End If
            'End Select

            'Pinkal (12-Sep-2014) -- Start
            'Medical Enhancement - TANAPA URGENT CHANGES FOR MEDICAL SICK SHEET GIVEN BY OLAIS

            'Shani(24-Aug-2015) -- Start
            'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
            'If mblnRevokeUserAccessOnSickSheet = False Then
            '    If strUserAccessLevelFilterString = "" Then
            '        strQ &= UserAccessLevel._AccessLevelFilterString
            '    Else
            '        strQ &= strUserAccessLevelFilterString
            '    End If
            'End If

            ''Pinkal (12-Sep-2014) -- End

            'If blnIncludeInactiveEmployee.Trim.Length <= 0 Then
            '    blnIncludeInactiveEmployee = ConfigParameter._Object._IsIncludeInactiveEmp.ToString()
            'End If

            'If CBool(blnIncludeInactiveEmployee) = False Then
            '    strQ &= " AND CONVERT(CHAR(8),hremployee_master.appointeddate,112) <= @enddate " & _
            '            " AND ISNULL(CONVERT(CHAR(8),hremployee_master.termination_from_date,112),@startdate) >= @startdate " & _
            '            " AND ISNULL(CONVERT(CHAR(8),hremployee_master.termination_to_date,112),@startdate) >= @startdate " & _
            '            " AND ISNULL(CONVERT(CHAR(8),hremployee_master.empl_enddate,112), @startdate) >= @startdate " 'S.SANDEEP [ 15 MAY 2012 empl_enddate ] -- START -- END

            '    objDataOperation.AddParameter("@startdate", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, IIf(strEmployeeAsOnDate = "", ConfigParameter._Object._EmployeeAsOnDate, strEmployeeAsOnDate))
            '    objDataOperation.AddParameter("@enddate", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, IIf(strEmployeeAsOnDate = "", ConfigParameter._Object._EmployeeAsOnDate, strEmployeeAsOnDate))
            'End If

            If xUACFiltrQry.Trim.Length > 0 Then
                strQ &= " AND " & xUACFiltrQry & " "
                    End If

            If xIncludeIn_ActiveEmployee = False Then
                If xDateFilterQry.Trim.Length > 0 Then
                    strQ &= xDateFilterQry & " "
                End If
            End If

            'Shani(24-Aug-2015) -- End
            If mstrSickSheetNo.Trim.Length > 0 Then
                strQ &= " AND mdmedical_sicksheet.sicksheetno=@sicksheetno"
                objDataOperation.AddParameter("@sicksheetno", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrSickSheetNo)
            End If

            If intEmployeeunkid > 0 Then
                strQ &= " AND mdmedical_sicksheet.employeeunkid=@employeeunkid"
                objDataOperation.AddParameter("@employeeunkid", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, intEmployeeunkid)
            End If


            'Pinkal (06-Jan-2016) -- Start
            'Enhancement - Working on Changes in SS for Leave Module.
            If mstrFilter.Trim.Length > 0 Then
                strQ &= " AND " & mstrFilter
            End If
            'Pinkal (06-Jan-2016) -- End


            dsList = objDataOperation.ExecQuery(strQ, strTableName)

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: GetList; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
        Return dsList
    End Function

    'Pinkal (12-Sep-2014) -- Start
    'Medical Enhancement - TANAPA URGENT CHANGES FOR MEDICAL SICK SHEET GIVEN BY OLAIS

    ''' <summary>
    ''' Modify By: Pinkal
    ''' </summary>
    ''' <returns>Boolean</returns>
    ''' <purpose> INSERT INTO Database Table (mdmedical_sicksheet) </purpose>
    Public Function Insert(Optional ByVal intCompanyUnkId As Integer = 0) As Boolean 'Sohail (23 Apr 2012) - [intCompanyUnkId]
      

        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        Dim objDataOperation As New clsDataOperation
        objDataOperation.BindTransaction()

        Dim intSickSheetNoType As Integer = 0
        Dim objConfig As New clsConfigOptions
        objConfig._Companyunkid = IIf(intCompanyUnkId = 0, Company._Object._Companyunkid, intCompanyUnkId)
        intSickSheetNoType = objConfig._SickSheetNotype
        If intSickSheetNoType = 0 Then
        If isExist(mstrSicksheetno) Then
            mstrMessage = Language.getMessage(mstrModuleName, 1, "This Sick Sheet No is already define.Please define new Sick Sheet No.")
                objDataOperation.ReleaseTransaction(False)
            Return False
        End If
        End If
        Try
            objDataOperation.ClearParameters()
            objDataOperation.AddParameter("@sicksheetno", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrSicksheetno.ToString)
            objDataOperation.AddParameter("@sicksheetdate", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, mdtSicksheetdate)
            objDataOperation.AddParameter("@instituteunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintInstituteunkid.ToString)
            objDataOperation.AddParameter("@employeeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintEmployeeunkid.ToString)
            objDataOperation.AddParameter("@jobunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintJobunkid.ToString)
            objDataOperation.AddParameter("@medicalcoverunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintMedicalcoverunkid.ToString)
            objDataOperation.AddParameter("@dependantunkid", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrDependantunkid.ToString)
            objDataOperation.AddParameter("@userunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintUserunkid.ToString)
            objDataOperation.AddParameter("@isvoid", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsvoid.ToString)
            objDataOperation.AddParameter("@voiduserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintVoiduserunkid.ToString)
            objDataOperation.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, IIf(mdtVoiddatetime <> Nothing, mdtVoiddatetime, DBNull.Value))
            objDataOperation.AddParameter("@voidreason", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrVoidreason.ToString)


            'Pinkal (12-Sep-2014) -- Start
            'Medical Enhancement - TANAPA URGENT CHANGES FOR MEDICAL SICK SHEET GIVEN BY OLAIS
            objDataOperation.AddParameter("@allocationtypeid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintAllocationTypeId.ToString)
            objDataOperation.AddParameter("@allocationtranunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintAllocationTranunkId.ToString)
            'Pinkal (12-Sep-2014) -- End


            strQ = "INSERT INTO mdmedical_sicksheet ( " & _
              "  sicksheetno " & _
              ", sicksheetdate " & _
              ", instituteunkid " & _
              ", employeeunkid " & _
              ", jobunkid " & _
              ", medicalcoverunkid " & _
              ", dependantunkid " & _
              ", userunkid " & _
              ", isvoid " & _
              ", voiduserunkid " & _
              ", voiddatetime " & _
              ", voidreason" & _
              ",allocationtypeid " & _
              ",allocationtranunkid " & _
            ") VALUES (" & _
              "  @sicksheetno " & _
              ", @sicksheetdate " & _
              ", @instituteunkid " & _
              ", @employeeunkid " & _
              ", @jobunkid " & _
              ", @medicalcoverunkid " & _
              ", @dependantunkid " & _
              ", @userunkid " & _
              ", @isvoid " & _
              ", @voiduserunkid " & _
              ", @voiddatetime " & _
              ", @voidreason" & _
              ", @allocationtypeid " & _
              ", @allocationtranunkid " & _
            "); SELECT @@identity"

            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            mintSicksheetunkid = dsList.Tables(0).Rows(0).Item(0)
            If intSickSheetNoType = 1 Then
                If Set_AutoNumber(objDataOperation, mintSicksheetunkid, "mdmedical_sicksheet", "sicksheetno", "sicksheetunkid", "NextSickSheetNo", objConfig._SickSheetPrifix, objConfig._Companyunkid) = False Then
                    If objDataOperation.ErrorMessage <> "" Then
                        objDataOperation.ReleaseTransaction(False)
                        Return False
                    End If
                End If
            End If

'S.SANDEEP [28-May-2018] -- START
'ISSUE/ENHANCEMENT : {Audit Trails} 
Dim objCommonATLog AS New clsCommonATLog 
objCommonATLog._FormName = mstrFormName 
objCommonATLog._LoginEmployeeUnkid = mintLoginEmployeeunkid 
            objCommonATLog._ClientIP = mstrClientIP
            objCommonATLog._HostName = mstrHostName
            objCommonATLog._FromWeb = mblnIsWeb
            objCommonATLog._AuditUserId = mintAuditUserId
objCommonATLog._CompanyUnkid = mintCompanyUnkid
            objCommonATLog._AuditDate = mdtAuditDate
            'S.SANDEEP [28-May-2018] -- END

            If objCommonATLog.Insert_AtLog(objDataOperation, 1, "mdmedical_sicksheet", "sicksheetunkid", mintSicksheetunkid, False, mintUserunkid) = False Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            'S.SANDEEP [28-May-2018] -- START
            'ISSUE/ENHANCEMENT : {Audit Trails} 
            objCommonATLog = Nothing
            'S.SANDEEP [28-May-2018] -- END


            objDataOperation.ReleaseTransaction(True)
            Return True
        Catch ex As Exception
            objDataOperation.ReleaseTransaction(False)
            Throw New Exception(ex.Message & "; Procedure Name: Insert; Module Name: " & mstrModuleName)
            Return False
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
    End Function

    'Pinkal (12-Sep-2014) -- End


    'Pinkal (12-Sep-2014) -- Start
    'Medical Enhancement - TANAPA URGENT CHANGES FOR MEDICAL SICK SHEET GIVEN BY OLAIS

    ''' <summary>
    ''' Modify By: Pinkal
    ''' </summary>
    ''' <returns>Boolean</returns>
    ''' <purpose> Update Database Table (mdmedical_sicksheet) </purpose>
    Public Function Update() As Boolean

        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        Dim objDataOperation As New clsDataOperation
        objDataOperation.BindTransaction()

        If isExist(mstrSicksheetno, mintSicksheetunkid) Then
            mstrMessage = Language.getMessage(mstrModuleName, 1, "This Sick Sheet No is already define.Please define new Sick Sheet No.")
            objDataOperation.ReleaseTransaction(False)
            Return False
        End If

        Try
            objDataOperation.ClearParameters()
            objDataOperation.AddParameter("@sicksheetunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintSicksheetunkid.ToString)
            objDataOperation.AddParameter("@sicksheetno", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrSicksheetno.ToString)
            objDataOperation.AddParameter("@sicksheetdate", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, mdtSicksheetdate)
            objDataOperation.AddParameter("@instituteunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintInstituteunkid.ToString)
            objDataOperation.AddParameter("@employeeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintEmployeeunkid.ToString)
            objDataOperation.AddParameter("@jobunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintJobunkid.ToString)
            objDataOperation.AddParameter("@medicalcoverunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintMedicalcoverunkid.ToString)
            objDataOperation.AddParameter("@dependantunkid", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrDependantunkid.ToString)
            objDataOperation.AddParameter("@userunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintUserunkid.ToString)
            objDataOperation.AddParameter("@isvoid", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsvoid.ToString)
            objDataOperation.AddParameter("@voiduserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintVoiduserunkid.ToString)
            objDataOperation.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, IIf(mdtVoiddatetime <> Nothing, mdtVoiddatetime, DBNull.Value))
            objDataOperation.AddParameter("@voidreason", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrVoidreason.ToString)

            'Pinkal (12-Sep-2014) -- Start
            'Medical Enhancement - TANAPA URGENT CHANGES FOR MEDICAL SICK SHEET GIVEN BY OLAIS
            objDataOperation.AddParameter("@allocationtypeid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintAllocationTypeId.ToString)
            objDataOperation.AddParameter("@allocationtranunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintAllocationTranunkId.ToString)
            'Pinkal (12-Sep-2014) -- End

            strQ = "UPDATE mdmedical_sicksheet SET " & _
              "  sicksheetno = @sicksheetno" & _
              ", sicksheetdate = @sicksheetdate" & _
              ", instituteunkid = @instituteunkid" & _
              ", employeeunkid = @employeeunkid" & _
              ", jobunkid = @jobunkid" & _
              ", medicalcoverunkid = @medicalcoverunkid" & _
              ", dependantunkid = @dependantunkid" & _
              ", userunkid = @userunkid" & _
              ", isvoid = @isvoid" & _
              ", voiduserunkid = @voiduserunkid" & _
              ", voiddatetime = @voiddatetime" & _
              ", voidreason = @voidreason " & _
              ", allocationtypeid = @allocationtypeid " & _
              ", allocationtranunkid = @allocationtranunkid " & _
            "WHERE sicksheetunkid = @sicksheetunkid "

            Call objDataOperation.ExecNonQuery(strQ)

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            'S.SANDEEP [28-May-2018] -- START
            'ISSUE/ENHANCEMENT : {Audit Trails} 
            Dim objCommonATLog As New clsCommonATLog
            objCommonATLog._FormName = mstrFormName
            objCommonATLog._LoginEmployeeUnkid = mintLoginEmployeeunkid
            objCommonATLog._ClientIP = mstrClientIP
            objCommonATLog._HostName = mstrHostName
            objCommonATLog._FromWeb = mblnIsWeb
            objCommonATLog._AuditUserId = mintAuditUserId
objCommonATLog._CompanyUnkid = mintCompanyUnkid
            objCommonATLog._AuditDate = mdtAuditDate
            'S.SANDEEP [28-May-2018] -- END

            If objCommonATLog.IsTableDataUpdate("atcommon_log", "mdmedical_sicksheet", mintSicksheetunkid, "sicksheetunkid", 2, objDataOperation) Then
                If objCommonATLog.Insert_AtLog(objDataOperation, 2, "mdmedical_sicksheet", "sicksheetunkid", mintSicksheetunkid, , mintUserunkid) = False Then
                    exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                    Throw exForce
                End If
            End If

            'S.SANDEEP [28-May-2018] -- START
            'ISSUE/ENHANCEMENT : {Audit Trails} 
            objCommonATLog = Nothing
            'S.SANDEEP [28-May-2018] -- END


            objDataOperation.ReleaseTransaction(True)
            Return True
        Catch ex As Exception
            objDataOperation.ReleaseTransaction(False)
            Throw New Exception(ex.Message & "; Procedure Name: Update; Module Name: " & mstrModuleName)
            Return False
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
    End Function

    'Pinkal (12-Sep-2014) -- End

    ''' <summary>
    ''' Modify By: Pinkal
    ''' </summary>
    ''' <returns>Boolean</returns>
    ''' <purpose> Delete Database Table (mdmedical_sicksheet) </purpose>
    Public Function Delete(ByVal intUnkid As Integer) As Boolean
        'If isUsed(intUnkid) Then
        '    mstrMessage = "<Message>"
        '    Return False
        'End If

        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        Dim objDataOperation As New clsDataOperation
        objDataOperation.BindTransaction()


        Try
            strQ = "Update mdmedical_sicksheet set isvoid = 1,voiduserunkid=@voiduserunkid,voiddatetime=@voiddatetime,voidreason=@voidreason " & _
            "WHERE sicksheetunkid = @sicksheetunkid "

            objDataOperation.ClearParameters()
            objDataOperation.AddParameter("@sicksheetunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intUnkid)
            objDataOperation.AddParameter("@voiduserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintVoiduserunkid)
            objDataOperation.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, mdtVoiddatetime)
            objDataOperation.AddParameter("@voidreason", SqlDbType.NVarChar, eZeeDataType.DESC_SIZE, mstrVoidreason)

            Call objDataOperation.ExecNonQuery(strQ)

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If


            'Pinkal (29-Aug-2012) -- Start
            'Enhancement : TRA Changes

            'If clsCommonATLog.Insert_AtLog(objDataOperation, 3, "mdmedical_sicksheet", "sicksheetunkid", intUnkid) = False Then
            '    exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
            '    Throw exForce
            'End If

            'S.SANDEEP [28-May-2018] -- START
            'ISSUE/ENHANCEMENT : {Audit Trails} 
            Dim objCommonATLog As New clsCommonATLog
            objCommonATLog._FormName = mstrFormName
            objCommonATLog._LoginEmployeeUnkid = mintLoginEmployeeunkid
            objCommonATLog._ClientIP = mstrClientIP
            objCommonATLog._HostName = mstrHostName
            objCommonATLog._FromWeb = mblnIsWeb
            objCommonATLog._AuditUserId = mintAuditUserId
objCommonATLog._CompanyUnkid = mintCompanyUnkid
            objCommonATLog._AuditDate = mdtAuditDate
            'S.SANDEEP [28-May-2018] -- END

            If objCommonATLog.Insert_AtLog(objDataOperation, 3, "mdmedical_sicksheet", "sicksheetunkid", intUnkid, False, mintVoiduserunkid) = False Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            'S.SANDEEP [28-May-2018] -- START
            'ISSUE/ENHANCEMENT : {Audit Trails} 
            objCommonATLog = Nothing
            'S.SANDEEP [28-May-2018] -- END



       

            objDataOperation.ReleaseTransaction(True)
            Return True
        Catch ex As Exception
            objDataOperation.ReleaseTransaction(False)
            Throw New Exception(ex.Message & "; Procedure Name: Update; Module Name: " & mstrModuleName)
            Return False
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
    End Function

    ''' <summary>
    ''' Modify By: Pinkal
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>
    Public Function isUsed(ByVal intUnkid As Integer) As Boolean
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        Dim objDataOperation As New clsDataOperation
        objDataOperation.ClearParameters()
        Try
            strQ = "SELECT sicksheetunkid FROM mdmedical_claim_tran WHERE isvoid = 0 AND sicksheetunkid = @sicksheetunkid"
            objDataOperation.AddParameter("@sicksheetunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intUnkid)
            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            Return dsList.Tables(0).Rows.Count > 0
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: isUsed; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
    End Function

    ''' <summary>
    ''' Modify By: Pinkal
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>
    Public Function isExist(ByVal strSheetNo As String, Optional ByVal intUnkid As Integer = -1, Optional ByVal intEmployeeunkid As Integer = -1, Optional ByVal intProviderNo As Integer = -1) As Boolean
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception
        Dim objDataOperation As New clsDataOperation

        Try
            strQ = "SELECT " & _
              "  sicksheetunkid " & _
              ", sicksheetno " & _
              ", sicksheetdate " & _
              ", instituteunkid " & _
              ", employeeunkid " & _
              ", jobunkid " & _
              ", medicalcoverunkid " & _
              ", dependantunkid " & _
              ", userunkid " & _
              ", isvoid " & _
              ", voiduserunkid " & _
              ", voiddatetime " & _
              ", voidreason " & _
              ", allocationtypeid " & _
              ", allocationtranunkid " & _
             "FROM mdmedical_sicksheet " & _
             "WHERE sicksheetno = @sicksheetno and isvoid = 0 "

            If intUnkid > 0 Then
                strQ &= " AND sicksheetunkid <> @sicksheetunkid"
            End If

            objDataOperation.ClearParameters()
            objDataOperation.AddParameter("@sicksheetno", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, strSheetNo)
            objDataOperation.AddParameter("@sicksheetunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intUnkid)

            If intEmployeeunkid > 0 Then
                strQ &= " AND employeeunkid = @employeeunkid"
                objDataOperation.AddParameter("@employeeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intEmployeeunkid)
            End If

            If intProviderNo > 0 Then
                strQ &= " AND instituteunkid = @instituteunkid"
                objDataOperation.AddParameter("@instituteunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intProviderNo)
            End If

            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            Return dsList.Tables(0).Rows.Count > 0
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: isExist; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
        End Try
    End Function

 

	'<Language> This Auto Generated Text Please Do Not Modify it.
#Region " Language & UI Settings "
	Public Shared Sub SetMessages()
		Try
			Language.setMessage(mstrModuleName, 1, "This Sick Sheet No is already define.Please define new Sick Sheet No.")

		Catch Ex As Exception
            DisplayError.Show("-1", Ex.Message, "SetMessages", mstrModuleName)
		End Try
	End Sub
#End Region 'Language & UI Settings
	'</Language>
End Class