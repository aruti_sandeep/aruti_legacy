﻿'************************************************************************************************************************************
'Class Name : clsmedical_category_master.vb
'Purpose    : All Medical Category Opration like getList, Insert, Update, Delete, checkDuplicate
'Date       :28/06/2010
'Written By :Pinkal
'Modified   :
'Last Message Index = 2
'************************************************************************************************************************************

Imports eZeeCommonLib
''' <summary>
''' Purpose: 
''' Developer: Pinkal
''' </summary>
Public Class clsmedical_category_master
    Private Shared ReadOnly mstrModuleName As String = "clsmedical_category_master"
    Dim objDataOperation As clsDataOperation
    Dim mstrMessage As String = ""

#Region " Private variables "
    Private mintMedicalcategoryunkid As Integer
    Private mintMdcategorymasterunkid As Integer
    Private mintDepandants_No As Integer
    'Sohail (17 Jun 2011) -- Start
    'Private mdblAmount As Double
    Private mdecAmount As Decimal
    'Sohail (17 Jun 2011) -- End
    Private mstrDescription As String = String.Empty
    Private mblnIsactive As Boolean = True

    'Pinkal (01-Oct-2014) -- Start
    'Enhancement -  Changes For FDRC Report
    Private mintMaritalstatusunkid As Integer = 0
    'Pinkal (01-Oct-2014) -- End

#End Region

#Region " Properties "
'S.SANDEEP [28-May-2018] -- START 
'ISSUE/ENHANCEMENT : {Audit Trails} 
Private mstrFormName As String = String.Empty
Public WriteOnly Property _FormName() As String 
Set(ByVal value As String) 
mstrFormName = value 
End Set 
End Property 
 
Private mstrClientIP As String = ""
Public WriteOnly Property _ClientIP() As String 
Set(ByVal value As String) 
mstrClientIP = value 
End Set 
End Property 
 
Private mstrHostName As String = ""
Public WriteOnly Property _HostName() As String 
Set(ByVal value As String) 
mstrHostName = value 
End Set 
End Property 
 
Private mblnIsWeb As Boolean = False
Public WriteOnly Property _FromWeb() As Boolean 
Set(ByVal value As Boolean) 
mblnIsWeb = value 
End Set 
End Property 
 
    Private mintAuditUserId As Integer = 0
    Public WriteOnly Property _AuditUserId() As Integer
        Set(ByVal value As Integer)
            mintAuditUserId = value
        End Set
    End Property

    Private mintCompanyUnkid As Integer = 0
    Public WriteOnly Property _CompanyUnkid() As Integer
        Set(ByVal value As Integer)
            mintCompanyUnkid = value
        End Set
    End Property
 
Private mdtAuditDate As DateTime = Now
Public WriteOnly Property _AuditDate() As DateTime 
Set(ByVal value As DateTime) 
mdtAuditDate = value 
End Set 
End Property 
 
Private xDataOpr As clsDataOperation = Nothing
Public Property _xDataOpr() As clsDataOperation 
Get 
Return xDataOpr 
End Get 
Set(ByVal value As clsDataOperation) 
xDataOpr = value 
End Set 
End Property 
 
Private mintLoginEmployeeunkid As Integer = 0
Public Property _LoginEmployeeunkid() As Integer 
Get 
Return mintLoginEmployeeunkid 
End Get 
Set(ByVal value As Integer) 
mintLoginEmployeeunkid = value 
End Set 
End Property 
 
Private mintVoidlogingemployeeunkid As Integer=0
Public Property _Voidloginemployeeunkid() As Integer 
Get 
Return mintVoidlogingemployeeunkid 
End Get 
Set(ByVal value As Integer) 
mintVoidlogingemployeeunkid = value 
End Set 
End Property 
'S.SANDEEP [28-May-2018] -- END 

    ''' <summary>
    ''' Purpose: Get Message from Class 
    ''' Modify By: Pinkal
    ''' </summary>
    Public ReadOnly Property _Message() As String
        Get
            Return mstrMessage
        End Get
    End Property

    ''' <summary>
    ''' Purpose: Get or Set medicalcategoryunkid
    ''' Modify By: Pinkal
    ''' </summary>
    Public Property _Medicalcategoryunkid() As Integer
        Get
            Return mintMedicalcategoryunkid
        End Get
        Set(ByVal value As Integer)
            mintMedicalcategoryunkid = Value
            Call getData()
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set mdcategorymasterunkid
    ''' Modify By: Pinkal
    ''' </summary>
    Public Property _Mdcategorymasterunkid() As Integer
        Get
            Return mintMdcategorymasterunkid
        End Get
        Set(ByVal value As Integer)
            mintMdcategorymasterunkid = Value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set dependants_no
    ''' Modify By: Pinkal
    ''' </summary>
    Public Property _Depandants_No() As Integer
        Get
            Return mintDepandants_No
        End Get
        Set(ByVal value As Integer)
            mintDepandants_No = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set amount
    ''' Modify By: Pinkal
    ''' </summary>
    Public Property _Amount() As Decimal 'Sohail (17 Jun 2011)
        Get
            Return mdecAmount
        End Get
        Set(ByVal value As Decimal) 'Sohail (17 Jun 2011)
            mdecAmount = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set description
    ''' Modify By: Pinkal
    ''' </summary>
    Public Property _Description() As String
        Get
            Return mstrDescription
        End Get
        Set(ByVal value As String)
            mstrDescription = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set isactive
    ''' Modify By: Pinkal
    ''' </summary>
    Public Property _Isactive() As Boolean
        Get
            Return mblnIsactive
        End Get
        Set(ByVal value As Boolean)
            mblnIsactive = value
        End Set
    End Property


    'Pinkal (01-Oct-2014) -- Start
    'Enhancement -  Changes For FDRC Report

    ''' <summary>
    ''' Purpose: Get or Set maritalstatusunkid
    ''' Modify By: Pinkal
    ''' </summary>
    Public Property _MaritalStatusunkid() As Integer
        Get
            Return mintMaritalstatusunkid
        End Get
        Set(ByVal value As Integer)
            mintMaritalstatusunkid = value
        End Set
    End Property

    'Pinkal (01-Oct-2014) -- End


#End Region

    ''' <summary>
    ''' Modify By: Pinkal
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>
    Public Sub GetData()
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        objDataOperation = New clsDataOperation

        Try
            strQ = "SELECT " & _
              "  medicalcategoryunkid " & _
              ", mdcategorymasterunkid " & _
              ", dependants_no " & _
              ", amount " & _
              ", description " & _
              ", isactive " & _
                      ", ISNULL(maritalstatusunkid,0) AS maritalstatusunkid " & _
             "FROM mdmedical_category_master " & _
             "WHERE medicalcategoryunkid = @medicalcategoryunkid "

            objDataOperation.AddParameter("@medicalcategoryunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintMedicalcategoryunkid.ToString)

            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            For Each dtRow As DataRow In dsList.Tables(0).Rows
                mintMedicalcategoryunkid = CInt(dtRow.Item("medicalcategoryunkid"))
                mintMdcategorymasterunkid = CInt(dtRow.Item("mdcategorymasterunkid"))
                mintDepandants_No = CInt(dtRow.Item("dependants_no"))
                mdecAmount = CDec(dtRow.Item("amount"))
                mstrDescription = dtRow.Item("description").ToString
                mblnIsactive = CBool(dtRow.Item("isactive"))

                'Pinkal (01-Oct-2014) -- Start
                'Enhancement -  Changes For FDRC Report
                mintMaritalstatusunkid = CInt(dtRow.Item("maritalstatusunkid"))
                'Pinkal (01-Oct-2014) -- End

                Exit For
            Next
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: GetData; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
    End Sub


    'Pinkal (19-Sep-2014) -- Start
    'Enhancement -  DEVEPLOING NEW MEDICAL REPORTS FOR FINCA CONGO(FDRC)

    ''' <summary>
    ''' Modify By: Pinkal
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>
    Public Function GetList(ByVal strTableName As String, Optional ByVal blnOnlyActive As Boolean = True, Optional ByVal intCategoryMstID As Integer = 0) As DataSet
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        objDataOperation = New clsDataOperation

        Try
            strQ = "SELECT " & _
              "  medicalcategoryunkid " & _
              ", mdcategorymasterunkid " & _
              ", mdmedical_master.mdmastercode" & _
              ", mdmedical_master.mdmastername" & _
              ", dependants_no " & _
              ", amount " & _
              ", description " & _
              ", mdmedical_category_master.isactive " & _
                      ", ISNULL(mdmedical_category_master.maritalstatusunkid,0) AS maritalstatusunkid " & _
             "FROM mdmedical_category_master  " & _
             " LEFT JOIN mdmedical_master on mdmedical_master.mdmasterunkid = mdmedical_category_master.mdcategorymasterunkid"

            If blnOnlyActive Then
                strQ &= " WHERE mdmedical_category_master.isactive = 1 "
            End If

            If intCategoryMstID > 0 Then
                strQ &= " AND mdcategorymasterunkid = " & intCategoryMstID
            End If

            dsList = objDataOperation.ExecQuery(strQ, strTableName)

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: GetList; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
        Return dsList
    End Function

    'Pinkal (19-Sep-2014) -- End

    ''' <summary>
    ''' Modify By: Pinkal
    ''' </summary>
    ''' <returns>Boolean</returns>
    ''' <purpose> INSERT INTO Database Table (mdmedical_category_master) </purpose>
    Public Function Insert() As Boolean
        If isExist(mintMdcategorymasterunkid) Then
            mstrMessage = Language.getMessage(mstrModuleName, 1, "This Category is already assigned. Please assign new Category.")
            Return False
        End If

        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        objDataOperation = New clsDataOperation
        objDataOperation.BindTransaction()

        Try
            objDataOperation.AddParameter("@mdcategorymasterunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintMdcategorymasterunkid.ToString)
            objDataOperation.AddParameter("@dependants_no", SqlDbType.Int, eZeeDataType.INT_SIZE, mintDepandants_No.ToString)
            objDataOperation.AddParameter("@amount", SqlDbType.Decimal, eZeeDataType.DECIMAL_SIZE, mdecAmount.ToString)
            objDataOperation.AddParameter("@description", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrDescription.ToString)
            objDataOperation.AddParameter("@isactive", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsactive.ToString)


            'Pinkal (01-Oct-2014) -- Start
            'Enhancement -  Changes For FDRC Report

            objDataOperation.AddParameter("@maritalstatusunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintMaritalstatusunkid.ToString)

            'strQ = "INSERT INTO mdmedical_category_master ( " & _
            '          "  mdcategorymasterunkid " & _
            '          ", dependants_no " & _
            '          ", amount " & _
            '          ", description " & _
            '          ", isactive" & _
            '        ") VALUES (" & _
            '          "  @mdcategorymasterunkid " & _
            '          ", @dependants_no " & _
            '          ", @amount " & _
            '          ", @description " & _
            '          ", @isactive" & _
            '        "); SELECT @@identity"


            strQ = "INSERT INTO mdmedical_category_master ( " & _
              "  mdcategorymasterunkid " & _
              ", dependants_no " & _
              ", amount " & _
              ", description " & _
              ", isactive" & _
                   ", maritalstatusunkid " & _
            ") VALUES (" & _
              "  @mdcategorymasterunkid " & _
              ", @dependants_no " & _
              ", @amount " & _
              ", @description " & _
              ", @isactive" & _
                   ", @maritalstatusunkid " & _
            "); SELECT @@identity"


            'Pinkal (01-Oct-2014) -- End

            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            mintMedicalcategoryunkid = dsList.Tables(0).Rows(0).Item(0)
'S.SANDEEP [28-May-2018] -- START
'ISSUE/ENHANCEMENT : {Audit Trails} 
Dim objCommonATLog AS New clsCommonATLog 
objCommonATLog._FormName = mstrFormName 
objCommonATLog._LoginEmployeeUnkid = mintLoginEmployeeunkid 
            objCommonATLog._ClientIP = mstrClientIP
            objCommonATLog._HostName = mstrHostName
            objCommonATLog._FromWeb = mblnIsWeb
            objCommonATLog._AuditUserId = mintAuditUserId
objCommonATLog._CompanyUnkid = mintCompanyUnkid
            objCommonATLog._AuditDate = mdtAuditDate
            'S.SANDEEP [28-May-2018] -- END

            If objCommonATLog.Insert_AtLog(objDataOperation, 1, "mdmedical_category_master", "medicalcategoryunkid", mintMedicalcategoryunkid) = False Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            'S.SANDEEP [28-May-2018] -- START
            'ISSUE/ENHANCEMENT : {Audit Trails} 
            objCommonATLog = Nothing
            'S.SANDEEP [28-May-2018] -- END

            objDataOperation.ReleaseTransaction(True)

            Return True
        Catch ex As Exception
            objDataOperation.ReleaseTransaction(False)
            Throw New Exception(ex.Message & "; Procedure Name: Insert; Module Name: " & mstrModuleName)
            Return False
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
    End Function

    ''' <summary>
    ''' Modify By: Pinkal
    ''' </summary>
    ''' <returns>Boolean</returns>
    ''' <purpose> Update Database Table (mdmedical_category_master) </purpose>
    Public Function Update() As Boolean
        If isExist(mintMdcategorymasterunkid, mintMedicalcategoryunkid) Then
            mstrMessage = Language.getMessage(mstrModuleName, 1, "This Category is already assigned. Please assign new Category.")
            Return False
        End If

        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        objDataOperation = New clsDataOperation
        objDataOperation.BindTransaction()

        Try
            objDataOperation.AddParameter("@medicalcategoryunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintMedicalcategoryunkid.ToString)
            objDataOperation.AddParameter("@mdcategorymasterunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintMdcategorymasterunkid.ToString)
            objDataOperation.AddParameter("@dependants_no", SqlDbType.Int, eZeeDataType.INT_SIZE, mintDepandants_No.ToString)
            objDataOperation.AddParameter("@amount", SqlDbType.Decimal, eZeeDataType.DECIMAL_SIZE, mdecAmount.ToString)
            objDataOperation.AddParameter("@description", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrDescription.ToString)
            objDataOperation.AddParameter("@isactive", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsactive.ToString)


            'Pinkal (01-Oct-2014) -- Start
            'Enhancement -  Changes For FDRC Report

            objDataOperation.AddParameter("@maritalstatusunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintMaritalstatusunkid.ToString)

            'strQ = "UPDATE mdmedical_category_master SET " & _
            '          "  mdcategorymasterunkid = @mdcategorymasterunkid" & _
            '          ", dependants_no = @dependants_no" & _
            '          ", amount = @amount" & _
            '          ", description = @description" & _
            '          ", isactive = @isactive " & _
            '        "WHERE medicalcategoryunkid = @medicalcategoryunkid "

            strQ = "UPDATE mdmedical_category_master SET " & _
              "  mdcategorymasterunkid = @mdcategorymasterunkid" & _
              ", dependants_no = @dependants_no" & _
              ", amount = @amount" & _
              ", description = @description" & _
              ", isactive = @isactive " & _
                    ", maritalstatusunkid = @maritalstatusunkid " & _
            "WHERE medicalcategoryunkid = @medicalcategoryunkid "


            'Pinkal (01-Oct-2014) -- End

            Call objDataOperation.ExecNonQuery(strQ)

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If


            'S.SANDEEP [28-May-2018] -- START
            'ISSUE/ENHANCEMENT : {Audit Trails} 
            Dim objCommonATLog As New clsCommonATLog
            objCommonATLog._FormName = mstrFormName
            objCommonATLog._LoginEmployeeUnkid = mintLoginEmployeeunkid
            objCommonATLog._ClientIP = mstrClientIP
            objCommonATLog._HostName = mstrHostName
            objCommonATLog._FromWeb = mblnIsWeb
            objCommonATLog._AuditUserId = mintAuditUserId
objCommonATLog._CompanyUnkid = mintCompanyUnkid
            objCommonATLog._AuditDate = mdtAuditDate
            'S.SANDEEP [28-May-2018] -- END

            If objCommonATLog.IsTableDataUpdate("atcommon_log", "mdmedical_category_master", mintMedicalcategoryunkid, "medicalcategoryunkid", 2) Then

                If objCommonATLog.Insert_AtLog(objDataOperation, 2, "mdmedical_category_master", "medicalcategoryunkid", mintMedicalcategoryunkid) = False Then
                    exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                    Throw exForce
                End If

            End If

            'S.SANDEEP [28-May-2018] -- START
            'ISSUE/ENHANCEMENT : {Audit Trails} 
            objCommonATLog = Nothing
            'S.SANDEEP [28-May-2018] -- END

            objDataOperation.ReleaseTransaction(True)
            Return True
        Catch ex As Exception
            objDataOperation.ReleaseTransaction(False)
            Throw New Exception(ex.Message & "; Procedure Name: Update; Module Name: " & mstrModuleName)
            Return False
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
    End Function

    ''' <summary>
    ''' Modify By: Pinkal
    ''' </summary>
    ''' <returns>Boolean</returns>
    ''' <purpose> Delete Database Table (mdmedical_category_master) </purpose>
    Public Function Delete(ByVal intUnkid As Integer) As Boolean
        'If isUsed(intUnkid) Then
        '    mstrMessage = Language.getMessage(mstrModuleName, 2, "Sorry, You cannot delete selected Medical Category. Reason: This Medical Category is in use.")
        '    Return False
        'End If

        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        'Pinkal (12-Oct-2011) -- Start
        'ENHANCEMENT : AUDIT TRAIL MAINTENANCE
        objDataOperation = New clsDataOperation
        objDataOperation.BindTransaction()
        'Pinkal (12-Oct-2011) -- End

        Try
            strQ = "Update mdmedical_category_master set isactive = 0 " & _
            "WHERE medicalcategoryunkid = @medicalcategoryunkid "

            objDataOperation.AddParameter("@medicalcategoryunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intUnkid)

            Call objDataOperation.ExecNonQuery(strQ)

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            'Pinkal (12-Oct-2011) -- Start
            'ENHANCEMENT : AUDIT TRAIL MAINTENANCE

            'S.SANDEEP [28-May-2018] -- START
            'ISSUE/ENHANCEMENT : {Audit Trails} 
            Dim objCommonATLog As New clsCommonATLog
            objCommonATLog._FormName = mstrFormName
            objCommonATLog._LoginEmployeeUnkid = mintLoginEmployeeunkid
            objCommonATLog._ClientIP = mstrClientIP
            objCommonATLog._HostName = mstrHostName
            objCommonATLog._FromWeb = mblnIsWeb
            objCommonATLog._AuditUserId = mintAuditUserId
objCommonATLog._CompanyUnkid = mintCompanyUnkid
            objCommonATLog._AuditDate = mdtAuditDate
            'S.SANDEEP [28-May-2018] -- END

            If objCommonATLog.Insert_AtLog(objDataOperation, 3, "mdmedical_category_master", "medicalcategoryunkid", intUnkid) = False Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            'S.SANDEEP [28-May-2018] -- START
            'ISSUE/ENHANCEMENT : {Audit Trails} 
            objCommonATLog = Nothing
            'S.SANDEEP [28-May-2018] -- END

            objDataOperation.ReleaseTransaction(True)
            'Pinkal (12-Oct-2011) -- End

            Return True
        Catch ex As Exception
            'Pinkal (12-Oct-2011) -- Start
            'ENHANCEMENT : AUDIT TRAIL MAINTENANCE
            objDataOperation.ReleaseTransaction(False)
            'Pinkal (12-Oct-2011) -- End
            Throw New Exception(ex.Message & "; Procedure Name: Update; Module Name: " & mstrModuleName)
            Return False
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
    End Function

    ''' <summary>
    ''' Modify By: Pinkal
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>
    Public Function isUsed(ByVal intUnkid As Integer) As Boolean
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        objDataOperation = New clsDataOperation

        Try
            strQ = "<Query>"

            objDataOperation.AddParameter("@medicalcategoryunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intUnkid)

            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            Return dsList.Tables(0).Rows.Count > 0
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: isUsed; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
    End Function

    ''' <summary>
    ''' Modify By: Pinkal
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>
    Public Function isExist(ByVal intMedicalmasterunkid As Integer, Optional ByVal intUnkid As Integer = -1) As Boolean
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception
        objDataOperation = New clsDataOperation
        Try

            strQ = "SELECT " & _
              "  medicalcategoryunkid " & _
              ", mdcategorymasterunkid " & _
              ", dependants_no " & _
              ", amount " & _
              ", description " & _
              ", isactive " & _
                      ", maritalstatusunkid " & _
             "FROM mdmedical_category_master " & _
          "WHERE mdcategorymasterunkid = @medicalmasterunkid  AND isactive = 1"

            If intUnkid > 0 Then
                strQ &= " AND medicalcategoryunkid <> @medicalcategoryunkid"
            End If

            objDataOperation.AddParameter("@medicalmasterunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intMedicalmasterunkid)
            objDataOperation.AddParameter("@medicalcategoryunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intUnkid)

            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            Return dsList.Tables(0).Rows.Count > 0
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: isExist; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
    End Function


	'<Language> This Auto Generated Text Please Do Not Modify it.
#Region " Language & UI Settings "
	Public Shared Sub SetMessages()
		Try
			Language.setMessage(mstrModuleName, 1, "This Category is already assigned. Please assign new Category.")

		Catch Ex As Exception
			DisplayError.Show("-1", ex.Message, "SetMessages", mstrModuleName)
		End Try
	End Sub
#End Region 'Language & UI Settings
	'</Language>
End Class