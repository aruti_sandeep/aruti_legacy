﻿'************************************************************************************************************************************
'Class Name : clsbiostarconnection_setting.vb
'Purpose    :
'Date       :21-Jul-2023
'Written By :Pinkal
'Modified   :
'************************************************************************************************************************************

Imports eZeeCommonLib
Imports MySql.Data.MySqlClient

''' <summary>
''' Purpose: 
''' Developer: Pinkal
''' </summary>
Public Class clsdeviceconnection_setting
    Private Shared ReadOnly mstrModuleName As String = "clsdeviceconnection_setting"
    Dim objDataOperation As clsDataOperation
    Dim mstrMessage As String = ""

#Region "Enum"

    Public Enum enDeviceConnectionType
        SQLServer = 1
        MYSQL = 2
    End Enum

#End Region

#Region " Private variables "
    Private mintDeviceconnectionunkid As Integer
    Private mintCompanyunkid As Integer
    Private mintConnectiontypeunkid As Integer
    Private mintDevicetypeunkid As Integer
    Private mstrDatabase_Name As String = String.Empty
    Private mstrDb_Servername As String = String.Empty
    Private mstrDb_Username As String = String.Empty
    Private mstrDb_Password As String = String.Empty
    Private mstrDb_Port As String
    Private mblnIsvoid As Boolean
    Private mdtVoiddatetime As Date
    Private mintVoiduserunkid As Integer
#End Region

#Region " Properties "
    ''' <summary>
    ''' Purpose: Get Message from Class 
    ''' Modify By: Pinkal
    ''' </summary>
    Public ReadOnly Property _Message() As String
        Get
            Return mstrMessage
        End Get
    End Property

    ''' <summary>
    ''' Purpose: Get or Set Deviceconnectionunkid
    ''' Modify By: Pinkal
    ''' </summary>
    Public Property _Deviceconnectionunkid() As Integer
        Get
            Return mintDeviceconnectionunkid
        End Get
        Set(ByVal value As Integer)
            mintDeviceconnectionunkid = value
            Call GetData()
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set companyunkid
    ''' Modify By: Pinkal
    ''' </summary>
    Public Property _Companyunkid() As Integer
        Get
            Return mintCompanyunkid
        End Get
        Set(ByVal value As Integer)
            mintCompanyunkid = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set connectiontypeunkid
    ''' Modify By: Pinkal
    ''' </summary>
    Public Property _Connectiontypeunkid() As Integer
        Get
            Return mintConnectiontypeunkid
        End Get
        Set(ByVal value As Integer)
            mintConnectiontypeunkid = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set Devicetypeunkid
    ''' Modify By: Pinkal
    ''' </summary>
    Public Property _Devicetypeunkid() As Integer
        Get
            Return mintDevicetypeunkid
        End Get
        Set(ByVal value As Integer)
            mintDevicetypeunkid = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set database_name
    ''' Modify By: Pinkal
    ''' </summary>
    Public Property _Database_Name() As String
        Get
            Return mstrDatabase_Name
        End Get
        Set(ByVal value As String)
            mstrDatabase_Name = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set db_servername
    ''' Modify By: Pinkal
    ''' </summary>
    Public Property _Db_Servername() As String
        Get
            Return mstrDb_Servername
        End Get
        Set(ByVal value As String)
            mstrDb_Servername = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set db_username
    ''' Modify By: Pinkal
    ''' </summary>
    Public Property _Db_Username() As String
        Get
            Return mstrDb_Username
        End Get
        Set(ByVal value As String)
            mstrDb_Username = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set db_password
    ''' Modify By: Pinkal
    ''' </summary>
    Public Property _Db_Password() As String
        Get
            Return mstrDb_Password
        End Get
        Set(ByVal value As String)
            mstrDb_Password = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set db_port
    ''' Modify By: Pinkal
    ''' </summary>
    Public Property _Db_Port() As String
        Get
            Return mstrDb_Port
        End Get
        Set(ByVal value As String)
            mstrDb_Port = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set isvoid
    ''' Modify By: Pinkal
    ''' </summary>
    Public Property _Isvoid() As Boolean
        Get
            Return mblnIsvoid
        End Get
        Set(ByVal value As Boolean)
            mblnIsvoid = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set voiddatetime
    ''' Modify By: Pinkal
    ''' </summary>
    Public Property _Voiddatetime() As Date
        Get
            Return mdtVoiddatetime
        End Get
        Set(ByVal value As Date)
            mdtVoiddatetime = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set voiduserunkid
    ''' Modify By: Pinkal
    ''' </summary>
    Public Property _Voiduserunkid() As Integer
        Get
            Return mintVoiduserunkid
        End Get
        Set(ByVal value As Integer)
            mintVoiduserunkid = value
        End Set
    End Property

#End Region

    ''' <summary>
    ''' Modify By: Pinkal
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>
    Public Sub GetData()
        Dim objDataOperation As clsDataOperation = Nothing
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        objDataOperation = New clsDataOperation()

        Try
            strQ = "SELECT " & _
                      "  deviceconnectionunkid " & _
                      ", companyunkid " & _
                      ", connectiontypeunkid " & _
                      ", devicetypeunkid " & _
                      ", database_name " & _
                      ", db_servername " & _
                      ", db_username " & _
                      ", db_password " & _
                      ", db_port " & _
                      ", isvoid " & _
                      ", voiddatetime " & _
                      ", voiduserunkid " & _
                      " FROM hrmsconfiguration..cfdeviceconnection_setting " & _
                      " WHERE deviceconnectionunkid = @deviceconnectionunkid AND companyunkid = @companyunkid "

            objDataOperation.ClearParameters()
            objDataOperation.AddParameter("@deviceconnectionunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintDeviceconnectionunkid.ToString)
            objDataOperation.AddParameter("@companyunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintCompanyunkid.ToString)
            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            For Each dtRow As DataRow In dsList.Tables(0).Rows
                mintDeviceconnectionunkid = CInt(dtRow.Item("deviceconnectionunkid"))
                mintCompanyunkid = CInt(dtRow.Item("companyunkid"))
                mintConnectiontypeunkid = CInt(dtRow.Item("connectiontypeunkid"))
                mintDevicetypeunkid = CInt(dtRow.Item("devicetypeunkid"))
                mstrDatabase_Name = dtRow.Item("database_name").ToString
                mstrDb_Servername = dtRow.Item("db_servername").ToString
                mstrDb_Username = dtRow.Item("db_username").ToString
                mstrDb_Password = dtRow.Item("db_password").ToString
                mstrDb_Port = dtRow.Item("db_port").ToString()
                mblnIsvoid = CBool(dtRow.Item("isvoid"))

                If IsDBNull(dtRow.Item("voiddatetime")) = False AndAlso dtRow.Item("voiddatetime") <> Nothing Then
                    mdtVoiddatetime = dtRow.Item("voiddatetime")
                End If

                mintVoiduserunkid = CInt(dtRow.Item("voiduserunkid"))
                Exit For
            Next
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: GetData; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
    End Sub

    ''' <summary>
    ''' Modify By: Pinkal
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>
    Public Function GetList(ByVal strTableName As String, ByVal xCompanyId As Integer, Optional ByVal blnOnlyActive As Boolean = True, Optional ByVal mstrFilter As String = "") As DataSet
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception
        Dim objDataOperation As New clsDataOperation

        Try
            strQ = "SELECT " & _
                      "  deviceconnectionunkid " & _
                      ", companyunkid " & _
                      ", connectiontypeunkid " & _
                      ", CASE WHEN connectiontypeunkid = 1 THEN @SQLSERVER " & _
                      "           WHEN connectiontypeunkid = 2 THEN @MYSQL " & _
                      "  ELSE '' END AS connectiontype " & _
                      ", devicetypeunkid " & _
                      ", CASE WHEN devicetypeunkid = 1 THEN @Biostar " & _
                      "           WHEN devicetypeunkid = 2 THEN @Biostar2 " & _
                      "           WHEN devicetypeunkid = 3 THEN @Biostar3 " & _
                      " END AS biostartype " & _
                      ", database_name " & _
                      ", db_servername " & _
                      ", db_username " & _
                      ", db_password " & _
                      ", db_port " & _
                      ", isvoid " & _
                      ", voiddatetime " & _
                      ", voiduserunkid " & _
                      " FROM hrmsconfiguration..cfdeviceconnection_setting "

            If blnOnlyActive Then
                strQ &= " WHERE isvoid = 0 "
            End If

            If xCompanyId > 0 Then
                strQ &= " AND companyunkid = @companyunkid"
            End If

            If mstrFilter.Trim.Length > 0 Then
                strQ &= " AND " & mstrFilter
            End If

            objDataOperation.ClearParameters()
            objDataOperation.AddParameter("@companyunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, xCompanyId)
            objDataOperation.AddParameter("@SQLSERVER", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 2, "SQL Server"))
            objDataOperation.AddParameter("@MYSQL", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 3, "MYSQL"))
            objDataOperation.AddParameter("@Biostar", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 4, "Biostar"))
            objDataOperation.AddParameter("@Biostar2", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 5, "Biostar 2"))
            objDataOperation.AddParameter("@Biostar3", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 6, "Biostar 3"))
            dsList = objDataOperation.ExecQuery(strQ, strTableName)

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: GetList; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
        Return dsList
    End Function

    ''' <summary>
    ''' Modify By: Pinkal
    ''' </summary>
    ''' <returns>Boolean</returns>
    ''' <purpose> INSERT INTO Database Table (cfdeviceconnection_setting) </purpose>
    Public Function Insert() As Boolean

        'If isExist(mintCompanyunkid, mintdevicetypeunkid,mintdeviceconnectionunkid) Then
        '    mstrMessage = Language.getMessage(mstrModuleName, 1, "This Biostar Connection is already defined for this company. Please define new Biostar connection for this company.")
        '    Return False
        'End If


        Dim objDataOperation As clsDataOperation = Nothing
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        objDataOperation = New clsDataOperation()

        Try
            objDataOperation.ClearParameters()
            objDataOperation.AddParameter("@companyunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintCompanyunkid.ToString)
            objDataOperation.AddParameter("@connectiontypeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintConnectiontypeunkid.ToString)
            objDataOperation.AddParameter("@devicetypeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintDevicetypeunkid.ToString)
            objDataOperation.AddParameter("@database_name", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrDatabase_Name.ToString)
            objDataOperation.AddParameter("@db_servername", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrDb_Servername.ToString)
            objDataOperation.AddParameter("@db_username", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrDb_Username.ToString)
            objDataOperation.AddParameter("@db_password", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrDb_Password.ToString)
            objDataOperation.AddParameter("@db_port", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrDb_Port.ToString)
            objDataOperation.AddParameter("@isvoid", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsvoid.ToString)

            If IsDBNull(mdtVoiddatetime) = False AndAlso mdtVoiddatetime <> Nothing Then
                objDataOperation.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, mdtVoiddatetime.ToString)
            Else
                objDataOperation.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, DBNull.Value)
            End If
            objDataOperation.AddParameter("@voiduserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintVoiduserunkid.ToString)

            strQ = "INSERT INTO hrmsconfiguration..cfdeviceconnection_setting ( " & _
                      "  companyunkid " & _
                      ", connectiontypeunkid " & _
                      ", devicetypeunkid " & _
                      ", database_name " & _
                      ", db_servername " & _
                      ", db_username " & _
                      ", db_password " & _
                      ", db_port " & _
                      ", isvoid " & _
                      ", voiddatetime " & _
                      ", voiduserunkid" & _
                    ") VALUES (" & _
                      "  @companyunkid " & _
                      ", @connectiontypeunkid " & _
                      ", @devicetypeunkid " & _
                      ", @database_name " & _
                      ", @db_servername " & _
                      ", @db_username " & _
                      ", @db_password " & _
                      ", @db_port " & _
                      ", @isvoid " & _
                      ", @voiddatetime " & _
                      ", @voiduserunkid" & _
                    "); SELECT @@identity"

            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            mintDeviceconnectionunkid = dsList.Tables(0).Rows(0).Item(0)

            Return True
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: Insert; Module Name: " & mstrModuleName)
            Return False
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
    End Function

    ''' <summary>
    ''' Modify By: Pinkal
    ''' </summary>
    ''' <returns>Boolean</returns>
    ''' <purpose> Update Database Table (cfdeviceconnection_setting) </purpose>
    Public Function Update() As Boolean
        'If isExist(mintCompanyunkid, mintdevicetypeunkid, mintdeviceconnectionunkid) Then
        '    mstrMessage = Language.getMessage(mstrModuleName, 1, "This Biostar Connection is already defined for this company. Please define new Biostar connection for this company.")
        '    Return False
        'End If

        Dim objDataOperation As clsDataOperation
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        objDataOperation = New clsDataOperation()

        Try
            objDataOperation.ClearParameters()
            objDataOperation.AddParameter("@deviceconnectionunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintDeviceconnectionunkid.ToString)
            objDataOperation.AddParameter("@companyunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintCompanyunkid.ToString)
            objDataOperation.AddParameter("@connectiontypeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintConnectiontypeunkid.ToString)
            objDataOperation.AddParameter("@devicetypeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintDevicetypeunkid.ToString)
            objDataOperation.AddParameter("@database_name", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrDatabase_Name.ToString)
            objDataOperation.AddParameter("@db_servername", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrDb_Servername.ToString)
            objDataOperation.AddParameter("@db_username", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrDb_Username.ToString)
            objDataOperation.AddParameter("@db_password", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrDb_Password.ToString)
            objDataOperation.AddParameter("@db_port", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrDb_Port.ToString)
            objDataOperation.AddParameter("@isvoid", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsvoid.ToString)

            If IsDBNull(mdtVoiddatetime) = False AndAlso mdtVoiddatetime <> Nothing Then
                objDataOperation.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, mdtVoiddatetime.ToString)
            Else
                objDataOperation.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, DBNull.Value)
            End If
            objDataOperation.AddParameter("@voiduserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintVoiduserunkid.ToString)

            strQ = "UPDATE hrmsconfiguration..cfdeviceconnection_setting SET " & _
                      "  companyunkid = @companyunkid" & _
                      ", connectiontypeunkid = @connectiontypeunkid" & _
                      ", devicetypeunkid = @devicetypeunkid" & _
                      ", database_name = @database_name" & _
                      ", db_servername = @db_servername" & _
                      ", db_username = @db_username" & _
                      ", db_password = @db_password" & _
                      ", db_port = @db_port" & _
                      ", isvoid = @isvoid" & _
                      ", voiddatetime = @voiddatetime" & _
                      ", voiduserunkid = @voiduserunkid " & _
                      " WHERE deviceconnectionunkid = @deviceconnectionunkid AND companyunkid = @companyunkid "

            Call objDataOperation.ExecNonQuery(strQ)

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            Return True
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: Update; Module Name: " & mstrModuleName)
            Return False
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
    End Function

    ''' <summary>
    ''' Modify By: Pinkal
    ''' </summary>
    ''' <returns>Boolean</returns>
    ''' <purpose> Delete Database Table (cfdeviceconnection_setting) </purpose>
    Public Function Delete(ByVal xCompanyId As Integer, ByVal intUnkid As Integer) As Boolean
        Dim objDataOperation As clsDataOperation = Nothing
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        objDataOperation = New clsDataOperation()
        Try
            strQ = "UPDATE hrmsconfiguration..cfdeviceconnection_setting SET " & _
                      " isvoid = @isvoid " & _
                      " ,voiduserunkid = @voiduserunkid " & _
                      ", voiddatetime = GETDATE() " & _
                      " WHERE deviceconnectionunkid = @deviceconnectionunkid AND companyunkid = @companyunkid "

            objDataOperation.ClearParameters()
            objDataOperation.AddParameter("@isvoid", SqlDbType.Bit, eZeeDataType.BIT_SIZE, True)
            objDataOperation.AddParameter("@voiduserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintVoiduserunkid)
            objDataOperation.AddParameter("@companyunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, xCompanyId)
            objDataOperation.AddParameter("@deviceconnectionunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intUnkid)

            Call objDataOperation.ExecNonQuery(strQ)

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            Return True
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: Delete; Module Name: " & mstrModuleName)
            Return False
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
    End Function

    ''' <summary>
    ''' Modify By: Pinkal
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>
    Public Function isExist(ByVal xCompanyId As Integer, ByVal xBiostarTypeId As Integer, Optional ByVal intUnkid As Integer = -1) As Boolean
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        objDataOperation = New clsDataOperation

        Try
            strQ = "SELECT " & _
                      "  deviceconnectionunkid " & _
                      ", companyunkid " & _
                      ", connectiontypeunkid " & _
                      ", devicetypeunkid " & _
                      ", database_name " & _
                      ", db_servername " & _
                      ", db_username " & _
                      ", db_password " & _
                      ", db_port " & _
                      ", isvoid " & _
                      ", voiddatetime " & _
                      ", voiduserunkid " & _
                      " FROM hrmsconfiguration..cfdeviceconnection_setting " & _
                      " WHERE isvoid = 0 AND companyunkid = @companyunkid AND devicetypeunkid = @devicetypeunkid "

            If intUnkid > 0 Then
                strQ &= " AND deviceconnectionunkid <> @deviceconnectionunkid"
            End If

            objDataOperation.AddParameter("@companyunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, xCompanyId)
            objDataOperation.AddParameter("@devicetypeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, xBiostarTypeId)
            objDataOperation.AddParameter("@deviceconnectionunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intUnkid)

            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            Return dsList.Tables(0).Rows.Count > 0
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: isExist; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
    End Function

    ''' <summary>
    ''' Modify By: Pinkal
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>
    ''' 
    Public Function getListForComboForConnectionType(Optional ByVal strListName As String = "List", Optional ByVal mblFlag As Boolean = False) As DataSet
        Dim dsList As New DataSet
        Dim objDataOperation As New clsDataOperation
        Dim strQ As String = String.Empty
        Dim exForce As Exception
        Try
            If mblFlag = True Then
                strQ = "SELECT 0 as Id, ' ' +  @name  as name UNION "
            End If

            strQ &= " SELECT " & enDeviceConnectionType.SQLServer & " AS Id, @SQLServer AS name " & _
                        " UNION  SELECT " & enDeviceConnectionType.MYSQL & " AS Id, @MYSQL AS name "

            objDataOperation.AddParameter("@name", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 7, "Select"))
            objDataOperation.AddParameter("@SQLServer", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 2, "SQL Server"))
            objDataOperation.AddParameter("@MYSQL", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 3, "MYSQL"))
            dsList = objDataOperation.ExecQuery(strQ, strListName)
            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If
            Return dsList
        Catch ex As Exception
            DisplayError.Show(-1, ex.Message, "getListForComboForConnectionType", mstrModuleName)
            Return Nothing
        Finally
            exForce = Nothing
            objDataOperation = Nothing
            dsList.Dispose()
            dsList = Nothing
        End Try
    End Function

    ''' <summary>
    ''' Modify By: Pinkal
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>
    ''' 
    Public Function getListForComboForBioStarType(Optional ByVal strListName As String = "List", Optional ByVal mblFlag As Boolean = False) As DataSet
        Dim dsList As New DataSet
        Dim objDataOperation As New clsDataOperation
        Dim strQ As String = String.Empty
        Dim exForce As Exception
        Try
            If mblFlag = True Then
                strQ = "SELECT 0 as Id, ' ' +  @name  as name UNION "
            End If

            strQ &= " SELECT " & enFingerPrintDevice.BioStar & " AS Id, @BioStar AS name " & _
                        " UNION  SELECT " & enFingerPrintDevice.BioStar2 & " AS Id, @BioStar2 AS name " & _
                        " UNION  SELECT " & enFingerPrintDevice.BioStar3 & " AS Id, @BioStar3 AS name "

            objDataOperation.AddParameter("@name", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 7, "Select"))
            objDataOperation.AddParameter("@BioStar", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 4, "Biostar"))
            objDataOperation.AddParameter("@BioStar2", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 5, "Biostar 2"))
            objDataOperation.AddParameter("@BioStar3", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 6, "Biostar 3"))
            dsList = objDataOperation.ExecQuery(strQ, strListName)
            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If
            Return dsList
        Catch ex As Exception
            DisplayError.Show(-1, ex.Message, "getListForComboForBioStarType", mstrModuleName)
            Return Nothing
        Finally
            exForce = Nothing
            objDataOperation = Nothing
            dsList.Dispose()
            dsList = Nothing
        End Try
    End Function

    ''' <summary>
    ''' Modify By: Pinkal
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>
    ''' 
    Public Function GetBioStarAttendanceData(ByVal mdtFromdate As Date, ByVal mdtTodate As Date) As DataTable
        Dim dtTable As DataTable = Nothing
        Dim exForce As Exception
        Dim strQ As String = ""
        Try

            If mintConnectiontypeunkid = enDeviceConnectionType.SQLServer Then

                Dim StrConn As String = ""
                Dim sqlcmd As SqlClient.SqlCommand = Nothing
                Dim adp As SqlClient.SqlDataAdapter = Nothing

                Using oSql As SqlClient.SqlConnection = New SqlClient.SqlConnection
                    StrConn = "Data Source=" & mstrDb_Servername & "; Initial Catalog=" & mstrDatabase_Name & "; user id =" & mstrDb_Username & "; Password=" & mstrDb_Password & "; MultipleActiveResultSets=true; "
                    oSql.ConnectionString = StrConn
                    oSql.Open()

                    strQ = " SELECT  " & _
                              " TB_USER.sUserID AS UserId  " & _
                              " ,TB_USER.sUserName AS UserName" & _
                              " ,TB_EVENT_LOG.nReaderIdn AS Device " & _
                              " ,TB_READER.sIP AS IPAddress" & _
                              " ,DATEADD(s,nDateTime,'1970-01-01') AS Logindate " & _
                              " FROM " & mstrDatabase_Name & "..TB_EVENT_LOG " & _
                              " JOIN " & mstrDatabase_Name & "..TB_USER on TB_EVENT_LOG.nUserID = TB_USER.sUserID " & _
                              " LEFT JOIN " & mstrDatabase_Name & "..TB_READER ON TB_READER.nReaderIdn = TB_EVENT_LOG.nReaderIdn " & _
                              " WHERE TB_EVENT_LOG.nUserID > 0 AND CONVERT(CHAR(8),DATEADD(s,nDateTime,'1970-01-01'),112) BETWEEN @FromDate AND @ToDate " & _
                              " UNION " & _
                              " SELECT  " & _
                              " TB_USER.sUserID AS UserId  " & _
                              " ,TB_USER.sUserName AS UserName" & _
                              " ,TB_EVENT_LOG_BK.nReaderIdn AS Device " & _
                              " ,TB_READER.sIP AS IPAddress" & _
                              " ,DATEADD(s,nDateTime,'1970-01-01') AS Logindate " & _
                              " FROM " & mstrDatabase_Name & "..TB_EVENT_LOG_BK " & _
                              " JOIN " & mstrDatabase_Name & "..TB_USER on TB_EVENT_LOG_BK.nUserID = TB_USER.sUserID " & _
                              " LEFT JOIN " & mstrDatabase_Name & "..TB_READER ON TB_READER.nReaderIdn = TB_EVENT_LOG_BK.nReaderIdn " & _
                              " WHERE TB_EVENT_LOG_BK.nUserID > 0 AND CONVERT(CHAR(8),DATEADD(s,nDateTime,'1970-01-01'),112) BETWEEN @FromDate AND @ToDate " & _
                              " ORDER BY TB_USER.sUserID,Logindate "

                    'Pinkal (04-Aug-2023) -- Voltamp having issue when biostar 1 having issue when records transfered to another table.

                    sqlcmd = New SqlClient.SqlCommand(strQ)
                    sqlcmd.Connection = oSql
                    sqlcmd.CommandTimeout = 0
                    sqlcmd.Parameters.Clear()
                    sqlcmd.Parameters.Add("@FromDate", SqlDbType.NVarChar).Value = eZeeDate.convertDate(mdtFromdate).ToString()
                    sqlcmd.Parameters.Add("@ToDate", SqlDbType.NVarChar).Value = eZeeDate.convertDate(mdtTodate).ToString()
                    adp = New SqlClient.SqlDataAdapter(sqlcmd)
                    Dim dsList As New DataSet
                    adp.Fill(dsList)
                    If dsList IsNot Nothing Then dtTable = dsList.Tables(0)

                    oSql.Close()
                End Using

            ElseIf mintConnectiontypeunkid = enDeviceConnectionType.MYSQL Then

                Dim StrConn As String = ""
                Dim sqlcmd As MySqlCommand = Nothing
                Dim adp As MySqlDataAdapter = Nothing

                Using oSql As MySqlConnection = New MySqlConnection
                    StrConn = "Server=" & mstrDb_Servername & ";Port=" & mstrDb_Port & ";Database=" & mstrDatabase_Name & ";Uid=" & mstrDb_Username & ";Pwd = " & mstrDb_Password & " "
                    oSql.ConnectionString = StrConn
                    oSql.Open()

                    strQ = " SELECT  " & _
                              " TB_USER.sUserID AS UserId  " & _
                              " ,TB_USER.sUserName AS UserName" & _
                              " ,TB_EVENT_LOG.nReaderIdn AS Device " & _
                              " ,TB_READER.sIP AS IPAddress" & _
                              " ,DATEADD('1970-01-01' ,INTERVAL nDateTime SECOND) AS Logindate " & _
                              " FROM " & mstrDatabase_Name & ".TB_EVENT_LOG " & _
                              " JOIN " & mstrDatabase_Name & ".TB_USER on TB_EVENT_LOG.nUserID = TB_USER.sUserID " & _
                              " LEFT JOIN " & mstrDatabase_Name & ".TB_READER ON TB_READER.nReaderIdn = TB_EVENT_LOG.nReaderIdn " & _
                              " WHERE TB_EVENT_LOG.nUserID > 0 AND DATE_FORMAT(DATE_ADD('1970-01-01', INTERVAL nDateTime SECOND), '%Y%m%d') BETWEEN @FromDate AND @ToDate " & _
                              " UNION " & _
                              " SELECT  " & _
                              " TB_USER.sUserID AS UserId  " & _
                              " ,TB_USER.sUserName AS UserName" & _
                              " ,TB_EVENT_LOG_BK.nReaderIdn AS Device " & _
                              " ,TB_READER.sIP AS IPAddress" & _
                              " ,DATEADD(s,nDateTime,'1970-01-01') AS Logindate " & _
                              " FROM " & mstrDatabase_Name & "..TB_EVENT_LOG_BK " & _
                              " JOIN " & mstrDatabase_Name & "..TB_USER on TB_EVENT_LOG_BK.nUserID = TB_USER.sUserID " & _
                              " LEFT JOIN " & mstrDatabase_Name & "..TB_READER ON TB_READER.nReaderIdn = TB_EVENT_LOG_BK.nReaderIdn " & _
                              " WHERE TB_EVENT_LOG_BK.nUserID > 0 AND CONVERT(CHAR(8),DATEADD(s,nDateTime,'1970-01-01'),112) BETWEEN @FromDate AND @ToDate " & _
                              " ORDER BY TB_USER.sUserID,Logindate "

                    'Pinkal (04-Aug-2023) -- Voltamp having issue when biostar 1 having issue when records transfered to another table.


                    sqlcmd = New MySqlCommand(strQ)
                    sqlcmd.Connection = oSql
                    sqlcmd.CommandTimeout = 0
                    sqlcmd.Parameters.Clear()
                    sqlcmd.Parameters.Add("@FromDate", MySqlDbType.VarChar).Value = eZeeDate.convertDate(mdtFromdate).ToString()
                    sqlcmd.Parameters.Add("@ToDate", MySqlDbType.VarChar).Value = eZeeDate.convertDate(mdtTodate).ToString()
                    adp = New MySqlDataAdapter(sqlcmd)
                    Dim dsList As New DataSet
                    adp.Fill(dsList)
                    If dsList IsNot Nothing Then dtTable = dsList.Tables(0)

                    oSql.Close()
                End Using

            End If

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: GetBioStarAttendanceData; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
        End Try
        Return dtTable
    End Function

    ''' <summary>
    ''' Modify By: Pinkal
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>
    ''' 
    Public Function GetBioStar2AttendanceData(ByVal mdtFromdate As Date, ByVal mdtTodate As Date) As DataTable
        Dim dtTable As DataTable = Nothing
        Dim exForce As Exception
        Dim strQ As String = ""
        Try

            If mintConnectiontypeunkid = enDeviceConnectionType.SQLServer Then

                Dim StrConn As String = ""
                Dim sqlcmd As SqlClient.SqlCommand = Nothing
                Dim adp As SqlClient.SqlDataAdapter = Nothing

                Using oSql As SqlClient.SqlConnection = New SqlClient.SqlConnection
                    StrConn = "Data Source=" & mstrDb_Servername & "; Initial Catalog=" & mstrDatabase_Name & "; user id =" & mstrDb_Username & "; Password=" & mstrDb_Password & "; MultipleActiveResultSets=true; "
                    oSql.ConnectionString = StrConn
                    oSql.Open()

                    strQ = " SELECT  " & _
                              " p.user_id AS UserId " & _
                              ", p.user_name AS UserName " & _
                              ", p.devnm AS Device " & _
                              ", d.ip_address AS IPAddress " & _
                              ", p.bsevtdt AS LoginDate " & _
                              " FROM  " & mstrDatabase_Name & "..punchlog AS p " & _
                              " JOIN  " & mstrDatabase_Name & "..device AS d ON d.id = p.devid " & _
                              " WHERE p.user_id > 0 AND CONVERT(CHAR(8),p.bsevtdt,112) BETWEEN @FromDate AND @ToDate " & _
                              " ORDER BY CONVERT(CHAR(8),p.bsevtdt,112),p.user_id "


                    sqlcmd = New SqlClient.SqlCommand(strQ)
                    sqlcmd.Connection = oSql
                    sqlcmd.CommandTimeout = 0
                    sqlcmd.Parameters.Clear()
                    sqlcmd.Parameters.Add("@FromDate", SqlDbType.NVarChar).Value = eZeeDate.convertDate(mdtFromdate).ToString()
                    sqlcmd.Parameters.Add("@ToDate", SqlDbType.NVarChar).Value = eZeeDate.convertDate(mdtTodate).ToString()
                    adp = New SqlClient.SqlDataAdapter(sqlcmd)
                    Dim dsList As New DataSet
                    adp.Fill(dsList)
                    If dsList IsNot Nothing Then dtTable = dsList.Tables(0)

                    oSql.Close()
                End Using

            ElseIf mintConnectiontypeunkid = enDeviceConnectionType.MYSQL Then

                Dim StrConn As String = ""
                Dim sqlcmd As MySqlCommand = Nothing
                Dim adp As MySqlDataAdapter = Nothing

                Using oSql As MySqlConnection = New MySqlConnection
                    StrConn = "Server=" & mstrDb_Servername & ";Port=" & mstrDb_Port & ";Database=" & mstrDatabase_Name & ";Uid=" & mstrDb_Username & ";Pwd = " & mstrDb_Password & " "
                    oSql.ConnectionString = StrConn
                    oSql.Open()

                    strQ = " SELECT  " & _
                            " p.user_id AS UserId " & _
                            ", p.user_name AS UserName " & _
                            ", p.devnm AS Device " & _
                            ", d.ip_address AS IPAddress " & _
                            ", p.bsevtdt AS LoginDate " & _
                            " FROM  " & mstrDatabase_Name & ".punchlog AS p " & _
                            " JOIN  " & mstrDatabase_Name & ".device AS d ON d.id = p.devid " & _
                            " WHERE p.user_id > 0 AND DATE_FORMAT(p.bsevtdt, '%Y%m%d') BETWEEN @FromDate AND @ToDate " & _
                            " ORDER BY DATE_FORMAT(p.bsevtdt, '%Y%m%d'),p.user_id "

                    sqlcmd = New MySqlCommand(strQ)
                    sqlcmd.Connection = oSql
                    sqlcmd.CommandTimeout = 0
                    sqlcmd.Parameters.Clear()
                    sqlcmd.Parameters.Add("@FromDate", MySqlDbType.VarChar).Value = eZeeDate.convertDate(mdtFromdate).ToString()
                    sqlcmd.Parameters.Add("@ToDate", MySqlDbType.VarChar).Value = eZeeDate.convertDate(mdtTodate).ToString()
                    adp = New MySqlDataAdapter(sqlcmd)
                    Dim dsList As New DataSet
                    adp.Fill(dsList)
                    If dsList IsNot Nothing Then dtTable = dsList.Tables(0)

                    oSql.Close()
                End Using

            End If

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: GetBioStar2AttendanceData; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
        End Try
        Return dtTable
    End Function

    ''' <summary>
    ''' Modify By: Pinkal
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>
    ''' 
    Public Function GetBioStar3AttendanceData(ByVal mdtFromdate As Date, ByVal mdtTodate As Date) As DataTable
        Dim dtTable As DataTable = Nothing
        Dim exForce As Exception
        Dim strQ As String = ""
        Try

            If mintConnectiontypeunkid = enDeviceConnectionType.SQLServer Then

                Dim StrConn As String = ""
                Dim sqlcmd As SqlClient.SqlCommand = Nothing
                Dim adp As SqlClient.SqlDataAdapter = Nothing

                Using oSql As SqlClient.SqlConnection = New SqlClient.SqlConnection
                    StrConn = "Data Source=" & mstrDb_Servername & "; Initial Catalog=" & mstrDatabase_Name & "; user id =" & mstrDb_Username & "; Password=" & mstrDb_Password & "; MultipleActiveResultSets=true; "
                    oSql.ConnectionString = StrConn
                    oSql.Open()

                    strQ = " SELECT  " & _
                              " p.user_id AS UserId " & _
                              ", p.user_name AS UserName " & _
                              ", p.devnm AS Device " & _
                              ", d.ip_address AS IPAddress " & _
                              ", p.devdt AS LoginDate " & _
                              " FROM  " & mstrDatabase_Name & "..punchlog AS p " & _
                              " JOIN  " & mstrDatabase_Name & "..device AS d ON d.id = p.devid " & _
                              " WHERE p.user_id > 0 AND CONVERT(CHAR(8),p.devdt,112) BETWEEN @FromDate AND @ToDate " & _
                              " ORDER BY CONVERT(CHAR(8),p.devdt,112),p.user_id "


                    sqlcmd = New SqlClient.SqlCommand(strQ)
                    sqlcmd.Connection = oSql
                    sqlcmd.CommandTimeout = 0
                    sqlcmd.Parameters.Clear()
                    sqlcmd.Parameters.Add("@FromDate", SqlDbType.NVarChar).Value = eZeeDate.convertDate(mdtFromdate).ToString()
                    sqlcmd.Parameters.Add("@ToDate", SqlDbType.NVarChar).Value = eZeeDate.convertDate(mdtTodate).ToString()
                    adp = New SqlClient.SqlDataAdapter(sqlcmd)
                    Dim dsList As New DataSet
                    adp.Fill(dsList)
                    If dsList IsNot Nothing Then dtTable = dsList.Tables(0)

                    oSql.Close()
                End Using

            ElseIf mintConnectiontypeunkid = enDeviceConnectionType.MYSQL Then

                Dim StrConn As String = ""
                Dim sqlcmd As MySqlCommand = Nothing
                Dim adp As MySqlDataAdapter = Nothing

                Using oSql As MySqlConnection = New MySqlConnection
                    StrConn = "Server=" & mstrDb_Servername & ";Port=" & mstrDb_Port & ";Database=" & mstrDatabase_Name & ";Uid=" & mstrDb_Username & ";Pwd = " & mstrDb_Password & " "
                    oSql.ConnectionString = StrConn
                    oSql.Open()

                    strQ = " SELECT  " & _
                            " p.user_id AS UserId " & _
                            ", p.user_name AS UserName " & _
                            ", p.devnm AS Device " & _
                            ", d.ip_address AS IPAddress " & _
                            ", p.devdt AS LoginDate " & _
                            " FROM  " & mstrDatabase_Name & ".punchlog AS p " & _
                            " JOIN  " & mstrDatabase_Name & ".device AS d ON d.id = p.devid " & _
                            " WHERE p.user_id > 0 AND DATE_FORMAT(p.devdt, '%Y%m%d') BETWEEN @FromDate AND @ToDate " & _
                            " ORDER BY DATE_FORMAT(p.devdt, '%Y%m%d'),p.user_id "

                    sqlcmd = New MySqlCommand(strQ)
                    sqlcmd.Connection = oSql
                    sqlcmd.CommandTimeout = 0
                    sqlcmd.Parameters.Clear()
                    sqlcmd.Parameters.Add("@FromDate", MySqlDbType.VarChar).Value = eZeeDate.convertDate(mdtFromdate).ToString()
                    sqlcmd.Parameters.Add("@ToDate", MySqlDbType.VarChar).Value = eZeeDate.convertDate(mdtTodate).ToString()
                    adp = New MySqlDataAdapter(sqlcmd)
                    Dim dsList As New DataSet
                    adp.Fill(dsList)
                    If dsList IsNot Nothing Then dtTable = dsList.Tables(0)

                    oSql.Close()
                End Using

            End If

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: GetBioStar3AttendanceData; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
        End Try
        Return dtTable
    End Function


    ''' <summary>
    ''' Modify By: Pinkal
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>
    ''' 
    Public Function GetCosecAttendanceData(ByVal mdtFromdate As Date, ByVal mdtTodate As Date) As DataTable
        Dim dtTable As DataTable = Nothing
        Dim exForce As Exception
        Dim strQ As String = ""
        Try

            If mintConnectiontypeunkid = enDeviceConnectionType.SQLServer Then

                Dim StrConn As String = ""
                Dim sqlcmd As SqlClient.SqlCommand = Nothing
                Dim adp As SqlClient.SqlDataAdapter = Nothing

                Using oSql As SqlClient.SqlConnection = New SqlClient.SqlConnection
                    StrConn = "Data Source=" & mstrDb_Servername & "; Initial Catalog=" & mstrDatabase_Name & "; user id =" & mstrDb_Username & "; Password=" & mstrDb_Password & "; MultipleActiveResultSets=true; "
                    oSql.ConnectionString = StrConn
                    oSql.Open()

                    strQ = " SELECT  " & _
                              " p.UserID AS UserId " & _
                              ", p.UserName AS UserName " & _
                              ", p.PDate AS PunchDate " & _
                              ", CASE WHEN Punch1 IS NOT NULL THEN Punch1 " & _
                              "           WHEN Punch3 IS NOT NULL THEN Punch3 " & _
                              "           WHEN Punch5 IS NOT NULL THEN Punch5 " & _
                              "           WHEN Punch7 IS NOT NULL THEN Punch7 " & _
                              "           WHEN Punch9 IS NOT NULL THEN Punch9 " & _
                              "           WHEN Punch11 IS NOT NULL THEN Punch11 " & _
                              " END AS LoginTime " & _
                              ", CASE WHEN Punch2 IS NOT NULL THEN Punch2 " & _
                              "           WHEN Punch4 IS NOT NULL THEN Punch4 " & _
                              "           WHEN Punch6 IS NOT NULL THEN Punch6 " & _
                              "           WHEN Punch8 IS NOT NULL THEN Punch8 " & _
                              "           WHEN Punch10 IS NOT NULL THEN Punch10 " & _
                              "           WHEN Punch12 IS NOT NULL THEN Punch12 " & _
                              " END AS Logouttime " & _
                              " FROM  " & mstrDatabase_Name & "..Mx_VEW_DailyAttendance AS p " & _
                              " WHERE CASE WHEN Punch1 IS NOT NULL THEN Punch1 " & _
                              "                     WHEN Punch3 IS NOT NULL THEN Punch3 " & _
                              "                     WHEN Punch5 IS NOT NULL THEN Punch5 " & _
                              "                     WHEN Punch7 IS NOT NULL THEN Punch7 " & _
                              "                     WHEN Punch9 IS NOT NULL THEN Punch9 " & _
                              "                     WHEN Punch11 IS NOT NULL THEN Punch11 END IS NOT NULL" & _
                              " AND CONVERT(CHAR(8),p.PDate,112) BETWEEN @FromDate AND @ToDate " & _
                              " ORDER BY UserID, CASE WHEN Punch1 IS NOT NULL THEN Punch1 " & _
                              "           WHEN Punch3 IS NOT NULL THEN Punch3 " & _
                              "           WHEN Punch5 IS NOT NULL THEN Punch5 " & _
                              "           WHEN Punch7 IS NOT NULL THEN Punch7 " & _
                              "           WHEN Punch9 IS NOT NULL THEN Punch9 " & _
                              "           WHEN Punch11 IS NOT NULL THEN Punch11 " & _
                              " END"


                    sqlcmd = New SqlClient.SqlCommand(strQ)
                    sqlcmd.Connection = oSql
                    sqlcmd.CommandTimeout = 0
                    sqlcmd.Parameters.Clear()
                    sqlcmd.Parameters.Add("@FromDate", SqlDbType.NVarChar).Value = eZeeDate.convertDate(mdtFromdate).ToString()
                    sqlcmd.Parameters.Add("@ToDate", SqlDbType.NVarChar).Value = eZeeDate.convertDate(mdtTodate).ToString()
                    adp = New SqlClient.SqlDataAdapter(sqlcmd)
                    Dim dsList As New DataSet
                    adp.Fill(dsList)
                    If dsList IsNot Nothing Then dtTable = dsList.Tables(0)

                    oSql.Close()
                End Using
            End If

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: GetCosecAttendanceData; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
        End Try
        Return dtTable
    End Function


    '<Language> This Auto Generated Text Please Do Not Modify it.
#Region " Language & UI Settings "
    Public Shared Sub SetMessages()
        Try
            Language.setMessage(mstrModuleName, 1, "This Biostar Connection is already defined for this company. Please define new Biostar connection for this company.")
            Language.setMessage(mstrModuleName, 2, "SQL Server")
            Language.setMessage(mstrModuleName, 3, "MYSQL")
            Language.setMessage(mstrModuleName, 4, "Biostar")
            Language.setMessage(mstrModuleName, 5, "Biostar 2")
            Language.setMessage(mstrModuleName, 6, "Biostar 3")
            Language.setMessage(mstrModuleName, 7, "Select")

        Catch Ex As Exception
            DisplayError.Show("-1", Ex.Message, "SetMessages", mstrModuleName)
        End Try
    End Sub
#End Region 'Language & UI Settings
    '</Language>
End Class