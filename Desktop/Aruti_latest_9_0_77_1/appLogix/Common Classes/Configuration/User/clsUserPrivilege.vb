﻿Imports eZeeCommonLib
Imports Aruti.Data

Public Class clsUserPrivilege
    Private ReadOnly mstrModuleName As String = "clsUserPrivilege"

#Region " Property Variables "
    'CONFIGURE PROPERTY
    Private mpreAccessConfiguaration As Boolean = False
    Private mpreChangeLanguage As Boolean = False
    Private mpreCompanyCreation As Boolean = False

    'S.SANDEEP [ 23 NOV 2012 ] -- START
    'ENHANCEMENT : TRA CHANGES
    'Private mpreUserCreation As Boolean = False
    'Private mpreUserRole As Boolean = False
    Private mpreAllowtoViewUserList As Boolean = False
    Private mpreAllowtoViewAbilityLevel As Boolean = False
    'S.SANDEEP [ 23 NOV 2012 ] -- END

    'S.SANDEEP [ 30 May 2011 ] -- START
    'ISSUE : FINCA REQ.
    Private mpreAddUser As Boolean = False
    Private mpreEditUser As Boolean = False
    Private mpreDeleteUser As Boolean = False
    Private mpreChangePassword As Boolean = False
    Private mpreAssignReportPrivilege As Boolean = False
    Private mpreAddUserRole As Boolean = False
    Private mpreEditUserRole As Boolean = False
    Private mpreDeleteUserRole As Boolean = False
    Private mpreAddCompany As Boolean = False
    Private mpreEditCompany As Boolean = False
    Private mpreDeleteCompany As Boolean = False
    'S.SANDEEP [ 30 May 2011 ] -- END 


    'GENERAL SETTINGS
    Private mpreDatabaseBackup As Boolean = False
    Private mpreDatabaseRestore As Boolean = False

    'Anjan (14 Feb 2010)-Start
    Private mpreAllowCommonExport As Boolean = False
    Private mpreAllowCommonImport As Boolean = False
    'Anjan (14 Feb 2010)-End

    'Anjan (20 Jan 2012)-Start
    'ENHANCEMENT : TRA COMMENTS
    Private mpreChangeGeneralSettings As Boolean = False
    Private mpreAllowAddLetter As Boolean = False
    Private mpreAllowEditLetter As Boolean = False
    Private mpreAllowDeleteLetter As Boolean = False

    Private mpreAllowSendMail As Boolean = False
    Private mpreAllowDeleteMail As Boolean = False
    Private mpreAllowViewUserLog As Boolean = False

   
    'Anjan (20 Jan 2012)-End 


    'HR
    Private mpreAddCommonMasters As Boolean = False
    Private mpreEditCommonMaster As Boolean = False
    Private mpreDeleteCommonMaster As Boolean = False
    Private mpreAddTerminationReason As Boolean = False
    Private mpreEditTerminationReason As Boolean = False
    Private mpreDeleteTerminationReason As Boolean = False
    Private mpreAddSkills As Boolean = False
    Private mpreEditSkills As Boolean = False
    Private mpreDeleteSkills As Boolean = False
    Private mpreAddAdvertise As Boolean = False
    Private mpreEditAdvertise As Boolean = False
    Private mpreDeleteAdvertise As Boolean = False
    Private mpreAddQualification_Course As Boolean = False
    Private mpreEditQualification_Course As Boolean = False
    Private mpreDeleteQualification_Course As Boolean = False
    Private mpreAddResultCode As Boolean = False
    Private mpreEditResultCode As Boolean = False
    Private mpreDeleteResultCode As Boolean = False
    Private mpreAddMembership As Boolean = False
    Private mpreEditMembership As Boolean = False
    Private mpreDeleteMembership As Boolean = False
    Private mpreAddBenefitPlan As Boolean = False
    Private mpreEditBenefitPlan As Boolean = False
    Private mpreDeleteBenefitPlan As Boolean = False
    Private mpreAddState As Boolean = False
    Private mpreEditState As Boolean = False
    Private mpreDeleteState As Boolean = False
    Private mpreAddCity As Boolean = False
    Private mpreEditCity As Boolean = False
    Private mpreDeleteCity As Boolean = False
    Private mpreAddZipCode As Boolean = False
    Private mpreEditZipCode As Boolean = False
    Private mpreDeleteZipCode As Boolean = False
    Private mpreAddEmployee As Boolean = False
    Private mpreEditEmployee As Boolean = False
    Private mpreDeleteEmployee As Boolean = False
    Private mpreAddDependant As Boolean = False
    Private mpreEditDependant As Boolean = False
    Private mpreDeleteDependant As Boolean = False
    Private mpreAddBeneficiaries As Boolean = False
    Private mpreEditBeneficiaries As Boolean = False
    Private mpreDeleteBeneficiaries As Boolean = False
    Private mpreAddBenefitAllocation As Boolean = False
    Private mpreEditBenefitAllocation As Boolean = False
    Private mpreDeleteBenefitAllocation As Boolean = False
    Private mpreAllowAssignGroupBenefit As Boolean = False
    Private mpreAddEmployeeBenefit As Boolean = False
    Private mpreEditEmployeeBenefit As Boolean = False
    Private mpreDeleteEmployeeBenefit As Boolean = False
    Private mpreAddEmployeeReferee As Boolean = False
    Private mpreEditEmployeeReferess As Boolean = False
    Private mpreDeleteEmployeeReferess As Boolean = False
    Private mpreAddEmployeeReferences As Boolean = False
    Private mpreEditEmployeeReferences As Boolean = False
    Private mpreDeleteEmployeeReferences As Boolean = False
    Private mpreAddEmployeeAssets As Boolean = False
    Private mpreEditEmployeeAssets As Boolean = False
    Private mpreDeleteEmployeeAssets As Boolean = False
    Private mpreAddEmployeeSkill As Boolean = False
    Private mpreEditEmployeeSkill As Boolean = False
    Private mpreDeleteEmployeeSkill As Boolean = False
    Private mpreAddEmployeeQualification As Boolean = False
    Private mpreEditEmployeeQualification As Boolean = False
    Private mpreDeleteEmployeeQualification As Boolean = False
    Private mpreAddEmployeeExperience As Boolean = False
    Private mpreEditEmployeeExperience As Boolean = False
    Private mpreDeleteEmployeeExperience As Boolean = False
    Private mpreAddEmployeeDiscipline As Boolean = False
    Private mpreEditEmployeeDiscipline As Boolean = False
    Private mpreDeleteEmployeeDiscipline As Boolean = False
    Private mpreAddDisciplinaryAction As Boolean = False
    Private mpreEditDisciplinaryAction As Boolean = False
    Private mpreDeleteDisciplinaryAction As Boolean = False
    Private mpreAddDisciplineOffence As Boolean = False
    Private mpreEditDisciplineOffence As Boolean = False
    Private mpreDeleteDisciplineOffence As Boolean = False

    'S.SANDEEP [28 MAY 2015] -- START
    'ENHANCEMENT : NEW PRIVILEGE (Performance Assessment)
    'Private mpreAddAssessmentGroup As Boolean = False
    'Private mpreEditAssessmentGroup As Boolean = False
    'Private mpreDeleteAssessmentGroup As Boolean = False
    'Private mpreAddAssessmentItem As Boolean = False
    'Private mpreEditAssessmentItem As Boolean = False
    'Private mpreDeleteAssessmentItem As Boolean = False
    'Private mpreAddAssessmentAnalysis As Boolean = False
    'Private mpreEditAssessmentAnalysis As Boolean = False
    'Private mpreDeleteAssessmentAnalysis As Boolean = False
    'S.SANDEEP [28 MAY 2015] -- END

    Private mpreAddMedicalMasters As Boolean = False
    Private mpreEditMedicalMasters As Boolean = False
    Private mpreDeleteMedicalMasters As Boolean = False
    Private mpreAddMedicalInjuries As Boolean = False
    Private mpreEditMedicalInjuries As Boolean = False
    Private mpreDeleteMedicalInjuries As Boolean = False
    Private mpreAddMedicalInstitutes As Boolean = False
    Private mpreEditMedicalInstitutes As Boolean = False
    Private mpreDeleteMedicalInstitutes As Boolean = False
    Private mpreAddAssignMedicalCategory As Boolean = False
    Private mpreEditAssignMedicalCategory As Boolean = False
    Private mpreDeleteAssignMedicalCategory As Boolean = False
    Private mpreAddMedicalCover As Boolean = False
    Private mpreEditMedicalCover As Boolean = False
    Private mpreDeleteMedicalCover As Boolean = False
    Private mpreAddMedicalClaim As Boolean = False
    Private mpreEditMedicalClaim As Boolean = False
    Private mpreDeleteMedicalClaim As Boolean = False
    'Anjan (14 Feb 2011)-Start

    'S.SANDEEP [28 MAY 2015] -- START
    'ENHANCEMENT : NEW PRIVILEGE (Performance Assessment)
    'Private mpreAllowtoAddAssessor As Boolean = False
    'Private mpreAllowtoDeleteAssessor As Boolean = False
    'Private mpreAddAssessorAccess As Boolean = False
    'Private mpreEditAssessorAccess As Boolean = False
    'Private mpreDeleteAssessorAccess As Boolean = False
    'Private mpreAllowtoMapAssessorUser As Boolean = False
    'Private mpreAddEmployeeAssessment As Boolean = False
    'Private mpreEditEmployeeAssessment As Boolean = False
    'Private mpreDeleteEmployeeAssessment As Boolean = False
    'S.SANDEEP [28 MAY 2015] -- END

    Private mpreAddDisciplineStatus As Boolean = False
    Private mpreEditDisciplineStatus As Boolean = False
    Private mpreDeleteDisciplineStatus As Boolean = False
    Private mpreAllowtoViewEmployeeMovement As Boolean = False
    Private mpreAllowtoImportEmployee As Boolean = False
    Private mpreAddTrainingInstitute As Boolean = False
    Private mpreEditTrainingInstitute As Boolean = False
    Private mpreDeleteTrainingInstitute As Boolean = False
    Private mpreAllowFinalAnalysis As Boolean = False
    Private mpreAddTrainingAnalysis As Boolean = False
    Private mpreEditTrainingAnalysis As Boolean = False
    Private mpreDeleteTrainingAnalysis As Boolean = False
    Private mpreAddTrainingEnrollment As Boolean = False
    Private mpreEditTrainingEnrollment As Boolean = False
    Private mpreDeleteTrainingEnrollment As Boolean = False
    Private mpreCancelTrainingEnrollment As Boolean = False
    Private mpreAddCourseScheduling As Boolean = False
    Private mpreEditCourseScheduling As Boolean = False
    Private mpreDeleteCourseScheduling As Boolean = False

    'S.SANDEEP [28 MAY 2015] -- START
    'ENHANCEMENT : NEW PRIVILEGE (Performance Assessment)
    'Private mpreAddAssessmentPeriod As Boolean = False
    'Private mpreEditAssessmentPeriod As Boolean = False
    'Private mpreDeleteAssessmentPeriod As Boolean = False
    'S.SANDEEP [28 MAY 2015] -- END


    'Anjan [06 April 2015] -- Start
    'ENHANCEMENT : Implementing Peoplesoft integration,requested by Andrew.
    Private mpreAllowImportPeoplesoftData As Boolean = False
    'Anjan [06 April 2015] -- End

    'Anjan (14 Feb 2011)-End


    'Anjan (17 Apr 2012)-Start
    'ENHANCEMENT : TRA COMMENTS on Andrew sir's Request
    Private mpreAddDisciplineCommittee As Boolean = False
    Private mpreEditDisciplineCommittee As Boolean = False
    Private mpreDeleteDisciplineCommittee As Boolean = False
    Private mpreAllowtoCloseCase As Boolean = False
    Private mpreAllowtoReopenCase As Boolean = False
    'Anjan (17 Apr 2012)-End 



    'S.SANDEEP [ 13 JUNE 2011 ] -- START
    'ISSUE : EMPLOYEE & LOAN PRIVILEGE
    'Private mpreAllowtoMarkEmployeeActive As Boolean = False


    'S.SANDEEP [ 29 DEC 2011 ] -- START
    'ENHANCEMENT : TRA CHANGES 
    'TYPE : EMPLOYEMENT CONTRACT PROCESS
    Private mpreAllowtoApproveEmployee As Boolean = False
    Private mpreAllowtoViewScale As Boolean = False
    Private mpreAllowtoChangeConfirmationDate As Boolean = False
    Private mpreAllowtoChangeAppointmentDate As Boolean = False
    Private mpreAllowtoSetBirthDate As Boolean = False
    Private mpreAllowtoSetSuspensionDate As Boolean = False
    Private mpreAllowtoSetProbationDate As Boolean = False
    Private mpreAllowtoSetEmploymentEndDate As Boolean = False
    Private mpreAllowtoSetLeavingDate As Boolean = False
    Private mpreAllowtoChangeRetirementDate As Boolean = False
    Private mpreAllowtoMarkEmployeeClear As Boolean = False
    Private mpreAllowChangeAssetStatus As Boolean = False
    'S.SANDEEP [ 29 DEC 2011 ] -- END
    'S.SANDEEP [ 13 JUNE 2011 ] -- END



    'Anjan (26 Oct 2012)-Start
    'ENHANCEMENT : TRA COMMENTS on Andrew's Request
    Private mpreAllowtoScanAttachDocument As Boolean = False
    Private mpreAllowtoExportMembership As Boolean = False
    Private mpreAllowtoImportMembership As Boolean = False
    Private mpreAllowtoSetReportingTo As Boolean = False
    Private mpreAllowtoViewDiary As Boolean = False
    Private mpreAllowtoSetAllocationMapping As Boolean = False
    Private mpreAllowtoDeleteAllocationMapping As Boolean = False
    Private mpreAllowtoImportDependants As Boolean = False
    Private mpreAllowtoExportDependants As Boolean = False

    Private mpreAllowtoImportEmpReferee As Boolean = False
    Private mpreAllowtoExportEmpReferee As Boolean = False
    Private mpreAllowtoImportEmpSkills As Boolean = False
    Private mpreAllowtoExportEmpSkills As Boolean = False
    Private mpreAllowtoImportEmpQualification As Boolean = False
    Private mpreAllowtoExportEmpQualification As Boolean = False
    Private mpreAllowtoExportOtherQualification As Boolean = False
    Private mpreAllowtoImportEmpExperience As Boolean = False
    Private mpreAllowtoExportEmpExperience As Boolean = False
    Private mpreAllowtoMapVacancy As Boolean = False
    Private mpreAllowtoViewInterviewAnalysisList As Boolean = False
    Private mpreAllowtoImportAccounts As Boolean = False
    Private mpreAllowtoExportAccounts As Boolean = False
    Private mpreAllowtoExportEmpBanks As Boolean = False
    Private mpreAllowtoImportEmpBanks As Boolean = False
    Private mpreAllowtoSetTranHeadActive As Boolean = False
    Private mpreAllowtoMapLeaveType As Boolean = False
    Private mrpeAllowtoViewLetterTemplate As Boolean = False

    'Anjan (26 Oct 2012)-End 


    'PAYROLL
    Private mpreAddPayrollGroup As Boolean = False
    Private mpreEditPayrollGroup As Boolean = False
    Private mpreDeletePayrollGroup As Boolean = False
    Private mpreAddCostCenter As Boolean = False
    Private mpreEditCostCenter As Boolean = False
    Private mpreDeleteCostCenter As Boolean = False
    Private mpreAddPayPoint As Boolean = False
    Private mpreEditPayPoint As Boolean = False
    Private mpreDeletePayPoint As Boolean = False
    Private mpreAddPeriods As Boolean = False
    Private mpreEditPeriods As Boolean = False
    Private mpreDeletePeriods As Boolean = False
    Private mpreAddVendor As Boolean = False
    Private mpreEditVendor As Boolean = False
    Private mpreDeleteVendor As Boolean = False
    Private mpreAddBankBranch As Boolean = False
    Private mpreEditBankBranch As Boolean = False
    Private mpreDeleteBankBranch As Boolean = False
    Private mpreAddBankAccType As Boolean = False
    Private mpreEditBankAccType As Boolean = False
    Private mpreDeleteBankAccType As Boolean = False
    Private mpreAddBankEDI As Boolean = False
    Private mpreEditBankEDI As Boolean = False
    Private mpreDeleteBankEDI As Boolean = False
    Private mpreAddCurrency As Boolean = False
    Private mpreEditCurrency As Boolean = False
    Private mpreDeleteCurrency As Boolean = False
    Private mpreAddDenomination As Boolean = False
    Private mpreEditDenomination As Boolean = False
    Private mpreDeleteDenomination As Boolean = False
    Private mpreAddEmployeeCostCenter As Boolean = False
    Private mpreEditEmployeeCostCenter As Boolean = False
    Private mpreDeleteEmployeeCostCenter As Boolean = False
    Private mpreAddEmployeeBanks As Boolean = False
    Private mpreEditEmployeeBanks As Boolean = False
    Private mpreDeleteEmployeeBanks As Boolean = False
    Private mpreAddBatchTransaction As Boolean = False
    Private mpreEditBatchTransaction As Boolean = False
    Private mpreDeleteBatchTransaction As Boolean = False
    Private mpreAddTransactionHead As Boolean = False
    Private mpreEditTransactionHead As Boolean = False
    Private mpreDeleteTransactionHead As Boolean = False
    Private mpreAddEarningDeduction As Boolean = False
    Private mpreEditEarningDeduction As Boolean = False
    Private mpreDeleteEarningDeduction As Boolean = False
    Private mpreAllowBatchTransactionOnED As Boolean = False
    Private mpreAllowPerformGlobalAssignOnED As Boolean = False
    Private mpreAllowPerformEmployeeExemptionOnED As Boolean = False
    Private mpreAddEmployeeExemption As Boolean = False
    Private mpreEditEmployeeExemption As Boolean = False
    Private mpreDeleteEmployeeExemption As Boolean = False
    Private mpreAllowProcessPayroll As Boolean = False
    Private mpreAllowViewPayslip As Boolean = False
    Private mpreAllowPrintPayslip As Boolean = False
    Private mpreAddPayment As Boolean = False
    Private mpreEditPayment As Boolean = False
    Private mpreDeletePayment As Boolean = False
    Private mpreAllowClosePeriod As Boolean = False
    Private mpreAllowCloseYear As Boolean = False
    Private mpreAllowPrepareBudget As Boolean = False
    Private mpreAddSalaryIncrement As Boolean = False
    Private mpreEditSalaryIncrement As Boolean = False
    Private mpreDeleteSalaryIncrement As Boolean = False
    Private mpreAllowDefineWageTable As Boolean = False
    'Anjan (14 Feb 2011)-Start
    Private mpreAllowtoImportTransactionHead As Boolean = False
    Private mpreAddPayslipMessage As Boolean = False
    Private mpreEditPayslipMessage As Boolean = False
    Private mpreDeletePayslipMessage As Boolean = False
    'Nilay (28-Apr-2016) -- Start
    'Enhancement : 60.1 - #77 (KBC & Kenya Project Comments List.xls) - For payslip messages, include payroll period so that If you print payslip for last month and you had a new payslip message this month, It does not pick up message of current month but last month.
    'Private mpreAllowtoAddEditGPayslipMessage As Boolean = False
    Private mpreAllowtoAddGlobalPayslipMessage As Boolean = False
    Private mpreAllowtoEditGlobalPayslipMessage As Boolean = False
    Private mpreAllowtoDeleteGlobalPayslipMessage As Boolean = False
    Private mpreAllowtoViewGlobalPayslipMessage As Boolean = False
    'Nilay (28-Apr-2016) -- End
    Private mpreAllowAssignBatchtoEmployee As Boolean = False
    Private mpreAddCashDenomination As Boolean = False
    Private mpreEditCashDenomination As Boolean = False
    Private mpreAllowImportED As Boolean = False
    Private mpreAllowExportED As Boolean = False
    Private mpreAllowGlobalPayment As Boolean = False
    Private mpreAllowGlobalSalaryIncrement As Boolean = False
    'Sohail (14 Jun 2011) -- Start
    Private mpreAllowToApproveEarningDeduction As Boolean = False
    'Sohail (14 Jun 2011) -- End
    Private mpreAllowToApproveEmpExemption As Boolean = False 'Sohail (26 Nov 2011)
    'Anjan (14 Feb 2011)-End
    'Anjan (20 Jan 2012)-Start
    'ENHANCEMENT : TRA COMMENTS
    Private mpreAllowAddAccount As Boolean = False
    Private mpreAllowEditAccount As Boolean = False
    Private mpreAllowDeleteAccount As Boolean = False

    Private mpreAllowAddAccountConfiguration As Boolean = False
    Private mpreAllowEditAccountConfiguration As Boolean = False
    Private mpreAllowDeleteAccountConfiguration As Boolean = False

    Private mpreAllowAddEmpConfiguration As Boolean = False
    Private mpreAllowEditEmpConfiguration As Boolean = False
    Private mpreAllowDeleteEmpConfiguration As Boolean = False

    Private mpreAllowAddCCConfiguration As Boolean = False
    Private mpreAllowEditCCConfiguration As Boolean = False
    Private mpreAllowDeleteCCConfiguration As Boolean = False
    'Anjan (20 Jan 2012)-End
    Private mpreAllowVoidPayroll As Boolean = False 'Sohail (22 May 2012)
    'Sohail (02 Jul 2012) -- Start
    'TRA - ENHANCEMENT
    Private mpreAllowAuthorizePayslipPayment As Boolean = False
    Private mpreAllowVoidAuthorizedPayslipPayment As Boolean = False
    'Sohail (02 Jul 2012) -- End
    Private mpreAllowToApproveSalaryChange As Boolean = False 'Sohail (24 Sep 2012)
    'Sohail (05 Dec 2012) -- Start
    'TRA - ENHANCEMENT
    Private mpreAllowToAddBatchPosting As Boolean = False
    Private mpreAllowToEditBatchPosting As Boolean = False
    Private mpreAllowToDeleteBatchPosting As Boolean = False
    Private mpreAllowToPostBatchPostingToED As Boolean = False
    Private mpreAllowToViewBatchPostingList As Boolean = False
    'Sohail (05 Dec 2012) -- End
    Private mpreAllowToVoidBatchPostingToED As Boolean = False 'Sohail (24 Feb 2016) 

    'LEAVE & TNA
    Private mpreAddLeaveType As Boolean = False
    Private mpreEditLeaveType As Boolean = False
    Private mpreDeleteLeaveType As Boolean = False
    Private mpreAddHolidays As Boolean = False
    Private mpreEditHolidays As Boolean = False
    Private mpreDeleteHolidays As Boolean = False
    Private mpreAddLeaveApproverLevel As Boolean = False
    Private mpreEditLeaveApproverLevel As Boolean = False
    Private mpreDeleteLeaveApproverLevel As Boolean = False
    Private mpreAddLeaveApprover As Boolean = False
    Private mpreEditLeaveApprover As Boolean = False
    Private mpreDeleteLeaveApprover As Boolean = False
    Private mpreAddLeaveForm As Boolean = False
    Private mpreEditLeaveForm As Boolean = False
    Private mpreDeleteLeaveForm As Boolean = False
    Private mpreAllowPrintLeaveForm As Boolean = False
    Private mpreAllowPreviewLeaveForm As Boolean = False
    Private mpreAllowEmailLeaveForm As Boolean = False
    Private mpreAllowProcessLeaveForm As Boolean = False
    Private mpreAllowIssueLeave As Boolean = False
    Private mpreAllowLeaveAccrue As Boolean = False
    Private mpreAddEmployeeHolidays As Boolean = False
    Private mpreEditEmployeeHolidays As Boolean = False
    Private mpreDeleteEmployeeHolidays As Boolean = False
    Private mpreAllowLeaveViewing As Boolean = False
    Private mpreAddShiftInformation As Boolean = False
    Private mpreEditShiftInformation As Boolean = False
    Private mpreDeleteShiftInformation As Boolean = False
    Private mpreAddTimeSheetInformation As Boolean = False
    Private mpreEditTimeSheetInformation As Boolean = False
    Private mpreDeleteTimeSheetInformation As Boolean = False
    Private mpreAllowHoldEmployee As Boolean = False
    Private mpreAllowUnHoldEmployee As Boolean = False
    Private mpreAllowProcessEmployeeAbsent As Boolean = False

    'Anjan (12 Apr 2012)-Start
    'ENHANCEMENT : TRA COMMENTS on Andrew sir's Request
    Private mpreAllowAddLeaveAllowance As Boolean = False
    'Anjan (12 Apr 2012)-End 


    'Anjan (14 Feb 2011)-Start
    Private mpreAllowtoEnrollFingerPrint As Boolean = False
    Private mpreAllowtoDeleteFingerPrint As Boolean = False
    Private mpreAllowtoEnrollNewCard As Boolean = False
    Private mpreAllowtoDeleteEnrolledCard As Boolean = False
    Private mpreAddPlanLeave As Boolean = False
    Private mpreEditPlanLeave As Boolean = False
    Private mpreDeletePlanLeave As Boolean = False
    Private mpreAllowDeleteAccruedLeave As Boolean = False
    Private mpreAllowViewPlannedLeaveViewer As Boolean = False
    'Sohail (04 Oct 2013) -- Start
    'TRA - ENHANCEMENT
    Private mpreAllowtoMapDeviceUser As Boolean = False
    Private mpreAllowtoImportDeviceUser As Boolean = False
    'Sohail (04 Oct 2013) -- End

    Private mpreAllowLoginTimesheet As Boolean = False
    Private mpreAllowImportLeaveTnA As Boolean = False
    Private mpreAllowExportLeaveTnA As Boolean = False
    Private mpreAllowMapApproverWithUser As Boolean = False
    Private mpreAllowChangeLeaveFormStatus As Boolean = False

    'Anjan (14 Feb 2011)-End

    'GENERAL SETUPS
    Private mpreAddStation As Boolean = False
    Private mpreEditStation As Boolean = False
    Private mpreDeleteStation As Boolean = False
    Private mpreAddDepartmentGroup As Boolean = False
    Private mpreEditDepartmentGroup As Boolean = False
    Private mpreDeleteDepartmentGroup As Boolean = False
    Private mpreAddDepartment As Boolean = False
    Private mpreEditDepartment As Boolean = False
    Private mpreDeleteDepartment As Boolean = False
    Private mpreAddSection As Boolean = False
    Private mpreEditSection As Boolean = False
    Private mpreDeleteSection As Boolean = False
    Private mpreAddUnit As Boolean = False
    Private mpreEditUnit As Boolean = False
    Private mpreDeleteUnit As Boolean = False
    Private mpreAddJobGroup As Boolean = False
    Private mpreEditJobGroup As Boolean = False
    Private mpreDeleteJobGroup As Boolean = False
    Private mpreAddJob As Boolean = False
    Private mpreEditJob As Boolean = False
    Private mpreDeleteJob As Boolean = False
    Private mpreAddClassGroup As Boolean = False
    Private mpreEditClassGroup As Boolean = False
    Private mpreDeleteClassGroup As Boolean = False
    Private mpreAddClasses As Boolean = False
    Private mpreEditClasses As Boolean = False
    Private mpreDeleteClasses As Boolean = False
    Private mpreAddGradeGroup As Boolean = False
    Private mpreEditGradeGroup As Boolean = False
    Private mpreDeleteGradeGroup As Boolean = False
    Private mpreAddGrade As Boolean = False
    Private mpreEditGrade As Boolean = False
    Private mpreDeleteGrade As Boolean = False
    Private mpreAddGradeLevel As Boolean = False
    Private mpreEditGradeLevel As Boolean = False
    Private mpreDeleteGradeLevel As Boolean = False
    'LOAN & SAVINGS
    Private mpreAddLoanScheme As Boolean = False
    Private mpreEditLoanScheme As Boolean = False
    Private mpreDeleteLoanScheme As Boolean = False
    Private mpreAllowLoanApproverCreation As Boolean = False
    Private mpreAddPendingLoan As Boolean = False
    Private mpreEditPendingLoan As Boolean = False
    Private mpreDeletePendingLoan As Boolean = False
    Private mpreAllowAssignPendingLoan As Boolean = False
    Private mpreEditLoanAdvance As Boolean = False
    Private mpreDeleteLoanAdvance As Boolean = False
    Private mpreAllowChangeLoanAvanceStatus As Boolean = False
    Private mpreAddSavingScheme As Boolean = False
    Private mpreEditSavingScheme As Boolean = False
    Private mpreDeleteSavingScheme As Boolean = False
    Private mpreAddEmployeeSavings As Boolean = False
    Private mpreEditEmployeeSavings As Boolean = False
    Private mpreDeleteEmployeeSavings As Boolean = False
    Private mpreAllowChangeSavingStatus As Boolean = False
    'Nilay (20-Sept-2016) -- Start
    'Enhancement : Cancel feature for approved but not assigned loan application
    Private mpreAllowToCancelApprovedLoanApp As Boolean = False
    'Nilay (20-Sept-2016) -- End

    'S.SANDEEP [ 13 JUNE 2011 ] -- START
    'ISSUE : EMPLOYEE & LOAN PRIVILEGE
    Private mpreAllowApproveLoan As Boolean = False
    Private mpreAllowApproveAdvance As Boolean = False
    'S.SANDEEP [ 13 JUNE 2011 ] -- END

    'Anjan (14 Feb 2011)-Start
    Private mpreAddLoanAdvancePayment As Boolean = False
    Private mpreEditLoanAdvancePayment As Boolean = False
    Private mpreDeleteLoanAdvancePayment As Boolean = False

    Private mpreAddLoanAdvanceReceived As Boolean = False
    Private mpreEditLoanAdvanceReceived As Boolean = False
    Private mpreDeleteLoanAdvanceReceived As Boolean = False
    Private mpreAllowChangePendingLoanAdvanceStatus As Boolean = False
    Private mpreAddSavingsPayment As Boolean = False
    Private mpreEditSavingsPayment As Boolean = False
    Private mpreDeleteSavingsPayment As Boolean = False
    'Anjan (14 Feb 2011)-End
    'Sohail (21 Nov 2015) -- Start
    'Enhancement - Provide Deposit feaure in Employee Saving.
    Private mpreAddSavingsDeposit As Boolean = False
    Private mpreEditSavingsDeposit As Boolean = False
    Private mpreDeleteSavingsDeposit As Boolean = False
    'Sohail (21 Nov 2015) -- End

    'Sandeep [ 23 Oct 2010 ] -- Start
    Private mpreAllowtoApproveLeave As Boolean = False
    'Sandeep [ 23 Oct 2010 ] -- End 

    'Dashboard
    'Anjan (21 Nov 2011)-Start
    'ENHANCEMENT : TRA COMMENTS
    Private mpreAllowAccessSalaryAnalysis As Boolean = False
    Private mpreAllowAccessAttendanceSummary As Boolean = False
    Private mpreAllowAccessLeaveAnalysis As Boolean = False
    Private mpreAllowAccessStaffTurnOver As Boolean = False
    Private mpreAllowAccessTrainingAnalysis As Boolean = False
    'Anjan (21 Nov 2011)-End 

    'S.SANDEEP [ 07 NOV 2011 ] -- START
    'ENHANCEMENT : TRA CHANGES
    Private mpreAllowToApproveResolutionStep As Boolean = False
    'S.SANDEEP [24 MAY 2016] -- START
    'ENHANCEMENT : DISCIPLINE MAKE-OVER {BY ANDREW}
    Private mpreAllowToApproveProceedingCounts As Boolean = False
    'S.SANDEEP [24 MAY 2016] -- END

    Private mpreAllowToAddResolutionStep As Boolean = False
    Private mpreAllowToEditResolutionStep As Boolean = False
    Private mpreAllowToDeleteResolutionStep As Boolean = False

    Private mpreAllowToAddDiscipileExemption As Boolean = False
    Private mpreAllowToAddDisciplinePosting As Boolean = False
    Private mpreAllowToViewDisciplineExemption As Boolean = False
    Private mpreAllowToViewDisciplinePosting As Boolean = False
    Private mpreAllowToDeleteDisciplineExemption As Boolean = False
    Private mpreAllowToDeleteDisciplinePosting As Boolean = False
    'S.SANDEEP [ 07 NOV 2011 ] -- END


    'RECRUTITMENT PRIVILEGES
    'S.SANDEEP [ 25 DEC 2011 ] -- START
    'ENHANCEMENT : TRA CHANGES  : 
    Private mpreAllowtoAddVacancy As Boolean = False
    Private mpreAllowtoEditVacancy As Boolean = False
    Private mpreAllowtoDeleteVacancy As Boolean = False
    Private mpreAllowtoAddApplicant As Boolean = False
    Private mpreAllowtoEditApplicant As Boolean = False
    Private mpreAllowtoDeleteApplicant As Boolean = False
    Private mpreAllowtoExportDataOnWeb As Boolean = False
    Private mpreAllowtoImportDataFromWeb As Boolean = False
    Private mpreAllowtoAddBatch As Boolean = False
    Private mpreAllowtoEditBatch As Boolean = False
    Private mpreAllowtoDeleteBatch As Boolean = False
    Private mpreAllowtoCancelBatch As Boolean = False
    Private mpreAllowtoAddInterviewSchedule As Boolean = False
    Private mpreAllowtoDeleteInterviewSchedule As Boolean = False
    Private mpreAllowtoCancelInterviewSchedule As Boolean = False
    Private mpreAllowtoAddAnalysis As Boolean = False
    Private mpreAllowtoChangeBatch As Boolean = False
    Private mpreAllowtoAddFinalEvaluation As Boolean = False
    Private mpreAllowtoEditAnalysis As Boolean = False
    Private mpreAllowtoDeleteAnalysis As Boolean = False
    Private mpreAllowtoMarkEligible As Boolean = False
    Private mpreAllowtoSendMail As Boolean = False
    Private mpreAllowtoGenerateLetter As Boolean = False
    Private mpreAllowtoPrintList As Boolean = False
    Private mpreAllowtoImportEligibleApplicant As Boolean = False
    Private mpreAllowtoMakeBatchActive As Boolean = False
    Private mpreAllowtoAttachApplicantDocuments As Boolean = False
    'S.SANDEEP [ 25 DEC 2011 ] -- END

    'Anjan (11 Jan 2012)-Start
    'ENHANCEMENT : TRA COMMENTS
    Private mpreAllowtoShortList_FinalShortListApplicants As Boolean = False
    'Anjan (11 Jan 2012)-End 
    'Hemant (03 Jun 2020) -- Start
    'ISSUE/ENHANCEMENT(NMB) :  RECRUITMENTS FINAL UAT CHANGES(Point No - 1 : On the interview analysis screen, they want to record interview scores for all applicants in a single window instead of analysing the interview an applicant at a time. Also, they want to record all the interview scores for all the interviewers on the same screen.)
    Private mpreAllowtoViewGlobalInterviewAnalysisList As Boolean = False
    'Hemant (03 Jun 2020) -- End
    'S.SANDEEP [ 05 MARCH 2012 ] -- START
    'ENHANCEMENT : TRA CHANGES

    'S.SANDEEP [28 MAY 2015] -- START
    'ENHANCEMENT : NEW PRIVILEGE (Performance Assessment)
    'Private mpreAllowUnlockCommittedGeneralAssessment As Boolean = False
    'Private mpreAllowUnlockCommittedBSCAssessment As Boolean = False
    'Private mpreAllowUnlockFinalSaveBSCPlanning As Boolean = False
    'Private mpreAllowFinalSaveBSCPlanning As Boolean = False
    'S.SANDEEP [28 MAY 2015] -- END

    'S.SANDEEP [ 05 MARCH 2012 ] -- END

    'Anjan (02 Mar 2012)-Start
    'ENHANCEMENT : TRA COMMENTS on Andrew sir's Request
    Private mpreAllowtosetReinstatementDate As Boolean = False
    'Anjan (02 Mar 2012)-End 

    'Sohail (26 Mar 2012) -- Start
    'TRA - ENHANCEMENT
    Private mpreAllowToAddAssetDeclaration As Boolean = False
    Private mpreAllowToEditAssetDeclaration As Boolean = False
    Private mpreAllowToDeleteAssetDeclaration As Boolean = False
    Private mpreAllowToFinalSaveAssetDeclaration As Boolean = False
    Private mpreAllowToUnlockFinalSaveAssetDeclaration As Boolean = False
    'Sohail (26 Mar 2012) -- End


    'S.SANDEEP [ 16 MAY 2012 ] -- START
    'ENHANCEMENT : TRA USER PRIVILEGE CHANGES
    Private mpreAllowToExportMedicalClaim As Boolean = False
    Private mpreAllowToSaveMedicalClaim As Boolean = False
    Private mpreAllowToFinalSaveMedicalClaim As Boolean = False
    Private mpreAllowToCancelExportedMedicalClaim As Boolean = False
    Private mpreAllowToMakeServiceProviderActive As Boolean = False
    Private mpreAllowToMapUserWithServiceProvider As Boolean = False
    Private mpreAllowToAddMedicalSickSheet As Boolean = False
    Private mpreAllowToEditMedicalSickSheet As Boolean = False
    Private mpreAllowToDeleteMedicalSickSheet As Boolean = False
    Private mpreAllowToPrintMedicalSickSheet As Boolean = False

    'S.SANDEEP [28 MAY 2015] -- START
    'ENHANCEMENT : NEW PRIVILEGE (Performance Assessment)
    'Private mpreAllowToAddBSCObjective As Boolean = False
    'Private mpreAllowToEditBSCObjective As Boolean = False
    'Private mpreAllowToDeleteBSCObjective As Boolean = False
    'Private mpreAllowToAddBSCKPI As Boolean = False
    'Private mpreAllowToEditBSCKPI As Boolean = False
    'Private mpreAllowToDeleteBSCKPI As Boolean = False
    'Private mpreAllowToAddBSCTargets As Boolean = False
    'Private mpreAllowToEditBSCTargets As Boolean = False
    'Private mpreAllowToDeleteBSCTargets As Boolean = False
    'Private mpreAllowToAddBSCInitiative_Action As Boolean = False
    'Private mpreAllowToEditBSCInitiative_Action As Boolean = False
    'Private mpreAllowToDeleteBSCInitiative_Action As Boolean = False
    'Private mpreAllowToAddSelfBSCAssessment As Boolean = False
    'Private mpreAllowToEditSelfBSCAssessment As Boolean = False
    'Private mpreAllowToDeleteSelfBSCAssessment As Boolean = False
    'Private mpreAllowToAddAssessorBSCAssessment As Boolean = False
    'Private mpreAllowToEditAssessorBSCAssessment As Boolean = False
    'Private mpreAllowToDeleteAssessorBSCAssessment As Boolean = False
    'Private mpreAllowToAddReviewerBSCAssessment As Boolean = False
    'Private mpreAllowToEditReviewerBSCAssessment As Boolean = False
    'Private mpreAllowToDeleteReviewerBSCAssessment As Boolean = False
    'Private mpreAllowToAddReviewerGeneralAssessment As Boolean = False
    'Private mpreAllowToEditReviewerGeneralAssessment As Boolean = False
    'Private mpreAllowToDeleteReviewerGeneralAssessment As Boolean = False
    'S.SANDEEP [28 MAY 2015] -- END



    Private mpreAllowToAddAppraisalAnalysis As Boolean = False
    Private mpreAllowToDeleteAppraisalAnalysis As Boolean = False
    Private mpreAllowToPerformSalaryIncrement As Boolean = False
    Private mpreAllowToVoidSalaryIncrement As Boolean = False
    Private mpreAllowToAddReminder As Boolean = False
    Private mpreAllowToAddEmployeeToFinalList As Boolean = False
    Private mpreAllowToSaveAppraisals As Boolean = False

    Private mpreAllowToAddEvaluationGroup As Boolean = False
    Private mpreAllowToEditEvaluationGroup As Boolean = False
    Private mpreAllowToDeleteEvaluationGroup As Boolean = False
    Private mpreAllowToAddEvaluationItems As Boolean = False
    Private mpreAllowToEditEvaluationItems As Boolean = False
    Private mpreAllowToDeleteEvaluationItems As Boolean = False
    Private mpreAllowToAddEvaluationSubItems As Boolean = False
    Private mpreAllowToEditEvaluationSubItems As Boolean = False
    Private mpreAllowToDeleteEvaluationSubItems As Boolean = False
    Private mpreAllowToAddEvaluationIIIItems As Boolean = False
    Private mpreAllowToEditEvaluationIIIItems As Boolean = False
    Private mpreAllowToDeleteEvaluationIIIItems As Boolean = False
    Private mpreAllowToAddLevelIEvaluation As Boolean = False
    Private mpreAllowToEditLevelIEvaluation As Boolean = False
    Private mpreAllowToDeleteLevelIEvaluation As Boolean = False
    Private mpreAllowToPrintLevelIEvaluation As Boolean = False
    Private mpreAllowToPriviewLevelIEvaluation As Boolean = False
    Private mpreAllowToSave_CompleteLevelIEvaluation As Boolean = False
    Private mpreAllowToAddLevelIIIEvaluation As Boolean = False
    Private mpreAllowToEditLevelIIIEvaluation As Boolean = False
    Private mpreAllowToDeleteLevelIIIEvaluation As Boolean = False
    Private mpreAllowToPrintLevelIIIEvaluation As Boolean = False
    Private mpreAllowToPriviewLevelIIIEvaluation As Boolean = False
    Private mpreAllowToSave_CompleteLevelIIIEvaluation As Boolean = False


    'S.SANDEEP [28 MAY 2015] -- START
    'ENHANCEMENT : NEW PRIVILEGE (Performance Assessment)
    'Private mpreAdd_Reviewer_Access As Boolean = False
    'Private mpreEdit_Reviewer_Access As Boolean = False
    'Private mpreDelete_Reviewer_Access As Boolean = False
    'Private mpreAllowToMapReviewerWithUser As Boolean = False
    'Private mpreAllowToAddAssessmentSubItems As Boolean = False
    'Private mpreAllowToEditAssessmentSubItems As Boolean = False
    'Private mpreAllowToDeleteAssessmentSubItems As Boolean = False
    'Private mpreAllowToAddExternalAssessor As Boolean = False
    'Private mpreAllowToEditExternalAssessor As Boolean = False
    'Private mpreAllowToDeleteExternalAssessor As Boolean = False
    'Private mpreAllowToMigrateAssessor_Reviewer As Boolean = False
    'S.SANDEEP [28 MAY 2015] -- END


    Private mpreAllowToAddTrainingPriorityList As Boolean = False
    Private mpreAllowToEditTrainingPriorityList As Boolean = False
    Private mpreAllowToDeleteTrainingPriorityList As Boolean = False
    Private mpreAllowToExportWithEmployee As Boolean = False
    Private mpreAllowToExportWithoutEmployee As Boolean = False
    Private mpreAllowToPreviewScheduledTraining As Boolean = False
    Private mpreAllowToPrintScheduledTraining As Boolean = False
    Private mpreAllowToUpdateQualifictaion As Boolean = False
    Private mpreAllowToRe_Categorize As Boolean = False
    Private mpreAllowSalaryIncrementFromEnrollment As Boolean = False
    Private mpreAllowToVoidSalaryIncrementFromEnrollment As Boolean = False

    Private mpreShowProbationDates As Boolean = False
    Private mpreShowSuspensionDates As Boolean = False
    Private mpreShowAppointmentDates As Boolean = False
    Private mpreShowConfirmationDates As Boolean = False
    Private mpreShowBirthDates As Boolean = False
    Private mpreShowAnniversaryDates As Boolean = False
    Private mpreShowContractEndingDates As Boolean = False
    Private mpreShowTodayRetirementDates As Boolean = False
    Private mpreShowForcastedRetirementDates As Boolean = False
    'S.SANDEEP [ 16 MAY 2012 ] -- END

    'S.SANDEEP [ 24 MAY 2012 ] -- START
    'ENHANCEMENT : TRA USER PRIVILEGE CHANGES
    Private mpreShowForcastedEOCDates As Boolean = False
    'S.SANDEEP [ 24 MAY 2012 ] -- END


    'Pinkal (03-Nov-2014) -- Start
    'Enhancement -  ENHANCMENT ON LEAVE MODULE GIVEN BY TRA.
    Private mpreShowForcastedELCDates As Boolean = False
    'Pinkal (03-Nov-2014) -- End


    'Pinkal (28-MAY-2012) -- Start
    'Enhancement : TRA Changes
    Private mpreAllowToCancelLeave As Boolean = False
    Private mpreAllowToCancelPreviousDateLeave As Boolean = False
    'Pinkal (28-MAY-2012) -- End

    'S.SANDEEP [ 19 JUNE 2012 ] -- START
    'ENHANCEMENT : TRA CHANGES
    Private mpreAllowToEditQueries As Boolean = False
    Private mpreAllowToAddTemplate As Boolean = False
    Private mpreAllowToEditTemplate As Boolean = False
    Private mpreAllowToDeleteTemplate As Boolean = False
    Private mpreAllowToAccessCustomReport As Boolean = False
    'S.SANDEEP [ 19 JUNE 2012 ] -- END



    'Pinkal (02-Jul-2012) -- Start
    'Enhancement : TRA Changes


    Private mpreAllowToViewLeaveTypeList As Boolean = False
    Private mpreAllowToViewHolidayList As Boolean = False
    Private mpreAllowToViewApproverLevelList As Boolean = False
    Private mpreAllowToViewLeaveApproverList As Boolean = False
    Private mpreAllowToViewLeaveFormList As Boolean = False
    Private mpreAllowToViewLeaveProcessList As Boolean = False
    Private mpreAllowToViewLeavePlannerList As Boolean = False
    Private mpreAllowToViewEmpHolidayList As Boolean = False
    Private mpreAllowToViewLeaveViewer As Boolean = False

    Private mpreAllowToViewMedicalMasterList As Boolean = False
    Private mpreAllowToViewServiceProviderList As Boolean = False
    Private mpreAllowToViewMedicalCategoryList As Boolean = False
    Private mpreAllowToViewMedicalCoverList As Boolean = False
    Private mpreAllowToViewMedicalClaimList As Boolean = False
    Private mpreAllowToViewEmpInjuryList As Boolean = False
    Private mpreAllowToViewEmpEmpSickSheetList As Boolean = False

    Private mpreAllowToViewEmpList As Boolean = False
    Private mpreAllowToViewBenefitAllocationList As Boolean = False
    Private mpreAllowToViewEmpBenefitList As Boolean = False
    Private mpreAllowToViewEmpReferenceList As Boolean = False
    Private mpreAllowToViewCompanyAssetList As Boolean = False
    Private mpreAllowToViewEmpSkillList As Boolean = False
    Private mpreAllowToViewEmpQualificationList As Boolean = False
    Private mpreAllowToViewEmpExperienceList As Boolean = False
    Private mpreAllowToViewEmpDependantsList As Boolean = False

    Private mpreAllowToViewChargesProceedingList As Boolean = False
    Private mpreAllowToViewProceedingApprovalList As Boolean = False
    Private mpreAllowToViewDisciplinaryCommitteeList As Boolean = False

    Private mpreAllowToViewCommonMasterList As Boolean = False
    Private mpreAllowToViewMembershipMasterList As Boolean = False
    Private mpreAllowToViewStateList As Boolean = False
    Private mpreAllowToViewCityList As Boolean = False
    Private mpreAllowToViewZipcodeList As Boolean = False
    Private mpreAllowToViewEmpMovementList As Boolean = False
    Private mpreAllowToViewShiftList As Boolean = False
    Private mpreAllowToViewTrainingInstituteList As Boolean = False


    'S.SANDEEP [28 MAY 2015] -- START
    'ENHANCEMENT : NEW PRIVILEGE (Performance Assessment)
    'Private mpreAllowToViewSelfAssessmentList As Boolean = False
    'Private mpreAllowToViewAssessorAssessmentList As Boolean = False
    'Private mpreAllowToViewReviewerAssessmentList As Boolean = False
    'Private mpreAllowToViewSelfAssessedBSCList As Boolean = False
    'Private mpreAllowToViewAssessorAssessedBSCList As Boolean = False
    'Private mpreAllowToViewReviewerAssessedBSCList As Boolean = False
    'S.SANDEEP [28 MAY 2015] -- END

    
    
    Private mpreAllowToViewApprisalAnalysisList As Boolean = False
    Private mpreAllowToViewTrainingNeedsAnalysisList As Boolean = False
    Private mpreAllowToViewTrainingSchedulingList As Boolean = False
    Private mpreAllowToViewTrainingEnrollmentList As Boolean = False
    Private mpreAllowToViewLevel1EvaluationList As Boolean = False
    Private mpreAllowToViewLevel3EvaluationList As Boolean = False

    Private mpreAllowToViewAssetsDeclarationList As Boolean = False

    Private mpreAllowToViewProcessLoanAdvanceList As Boolean = False
    Private mpreAllowToViewLoanAdvanceList As Boolean = False
    Private mpreAllowToViewEmployeeSavingsList As Boolean = False

    Private mpreAllowToViewVancancyMasterList As Boolean = False
    Private mpreAllowToViewApplicantMasterList As Boolean = False
    Private mpreAllowToViewShortListApplicants As Boolean = False
    Private mpreAllowToViewBatchSchedulingList As Boolean = False
    Private mpreAllowToViewInterviewSchedulingList As Boolean = False
    Private mpreAllowToViewFinalApplicantList As Boolean = False

    Private mpreAllowToViewEmpDistributedCostCenterList As Boolean = False
    Private mpreAllowToViewEmpBankList As Boolean = False
    Private mpreAllowToViewTransactionHeadsList As Boolean = False
    Private mpreAllowToViewEmpEDList As Boolean = False
    Private mpreAllowToViewEmpExemptionList As Boolean = False
    Private mpreAllowToViewPayslipList As Boolean = False
    Private mpreAllowToViewPaymentList As Boolean = False
    Private mpreAllowToViewGlobalVoidPaymentList As Boolean = False
    'Pinkal (02-Jul-2012) -- End
    Private mpreAllowToPerformForceLogout As Boolean = False


    'Pinkal (09-Jul-2012) -- Start
    'Enhancement : TRA Changes

    Private mpreAllowToViewStationList As Boolean = False
    Private mpreAllowToViewDeptGroupList As Boolean = False
    Private mpreAllowToViewDepartmentList As Boolean = False
    Private mpreAllowToViewSectionGroupList As Boolean = False
    Private mpreAllowToViewSectionList As Boolean = False
    Private mpreAllowToViewUnitGroupList As Boolean = False
    Private mpreAllowToViewUnitList As Boolean = False
    Private mpreAllowToViewTeamList As Boolean = False
    Private mpreAllowToViewJobGroupList As Boolean = False
    Private mpreAllowToViewJobList As Boolean = False
    Private mpreAllowToViewClassGroupList As Boolean = False
    Private mpreAllowToViewClassList As Boolean = False
    Private mpreAllowToViewGradeGroupList As Boolean = False
    Private mpreAllowToViewGradeList As Boolean = False
    Private mpreAllowToViewGradeLevelList As Boolean = False
    Private mpreAllowToViewCostCenterList As Boolean = False
    Private mpreAllowToViewPayrollPeriodList As Boolean = False
    Private mpreAllowToViewSalaryChangeList As Boolean = False
    Private mpreAllowToViewReasonMasterList As Boolean = False
    Private mpreAllowToViewSkillMasterList As Boolean = False
    Private mpreAllowToViewAgencyMasterList As Boolean = False
    Private mpreAllowToViewQualificationMasterList As Boolean = False
    Private mpreAllowToViewResultCodeList As Boolean = False
    Private mpreAllowToViewBenefitPlanList As Boolean = False
    Private mpreAllowToViewDisciplinaryOffencesList As Boolean = False
    Private mpreAllowToViewDisciplinaryPenaltiesList As Boolean = False
    Private mpreAllowToViewDisciplinaryStatusList As Boolean = False

    'S.SANDEEP [28 MAY 2015] -- START
    'ENHANCEMENT : NEW PRIVILEGE (Performance Assessment)
    'Private mpreAllowToViewAssessmentPeriodList As Boolean = False
    'Private mpreAllowToViewAssessorAccessList As Boolean = False
    'Private mpreAllowToViewReviewerAccessList As Boolean = False
    'Private mpreAllowToViewExternalAssessorList As Boolean = False
    'Private mpreAllowToViewAssessmentGroupList As Boolean = False
    'Private mpreAllowToViewAssessmentItemList As Boolean = False
    'Private mpreAllowToViewAssessmentSubItemList As Boolean = False
    'Private mpreAllowToViewBSCObjectiveList As Boolean = False
    'Private mpreAllowToViewBSCKPIList As Boolean = False
    'Private mpreAllowToViewBSCTargetsList As Boolean = False
    'Private mpreAllowToViewBSCInitiativeList As Boolean = False
    'S.SANDEEP [28 MAY 2015] -- END

    Private mpreAllowToViewEvaluationGroupList As Boolean = False
    Private mpreAllowToViewEvaluationItemList As Boolean = False
    Private mpreAllowToViewEvaluationSubItemList As Boolean = False
    Private mpreAllowToViewEvaluationIIIitemList As Boolean = False


    Private mpreAllowToViewPayrollGroupList As Boolean = False
    Private mpreAllowToViewPayPointList As Boolean = False
    Private mpreAllowToViewBankBranchList As Boolean = False
    Private mpreAllowToViewBankAccountTypeList As Boolean = False
    Private mpreAllowToViewBankEDIList As Boolean = False
    Private mpreAllowToViewCurrencyList As Boolean = False
    Private mpreAllowToViewCurrencyDenominationList As Boolean = False
    Private mpreAllowToViewPayslipMessageList As Boolean = False
    Private mpreAllowToViewAccountList As Boolean = False
    Private mpreAllowToViewCompanyAccountConfigurationList As Boolean = False
    Private mpreAllowToViewEmployeeAccountConfigurationList As Boolean = False
    Private mpreAllowToViewCostcenterAccountConfigurationList As Boolean = False
    Private mpreAllowToViewBatchTransactionList As Boolean = False
    Private mpreAllowToViewCashDenominationList As Boolean = False

    Private mpreAllowToViewLoanSchemeList As Boolean = False
    Private mpreAllowToViewSavingSchemeList As Boolean = False
    'Pinkal (09-Jul-2012) -- End 

    'S.SANDEEP [ 17 AUG 2012 ] -- START
    'ENHANCEMENT : TRA CHANGES
    Private mpreAllowToViewApplicantReferenceNo As Boolean = False
    'S.SANDEEP [ 17 AUG 2012 ] -- END


    'Anjan (03 Oct 2012)-Start
    'ENHANCEMENT : TRA COMMENTS on Rutta's Request
    Private mpreAddTeam As Boolean = False
    Private mpreEditTeam As Boolean = False
    Private mpreDeleteTeam As Boolean = False

    Private mpreAddSectionGroup As Boolean = False
    Private mpreEditSectionGroup As Boolean = False
    Private mpreDeleteSectionGroup As Boolean = False

    Private mpreAddUnitGroup As Boolean = False
    Private mpreEditUnitGroup As Boolean = False
    Private mpreDeleteUnitGroup As Boolean = False

    'Anjan (03 Oct 2012)-End 

    'S.SANDEEP [ 08 OCT 2012 ] -- START
    'ENHANCEMENT : TRA CHANGES
    Private mpreEditImportedEmployee As Boolean = False
    'S.SANDEEP [ 08 OCT 2012 ] -- END

    'S.SANDEEP [ 29 OCT 2012 ] -- START
    'ENHANCEMENT : TRA CHANGES
    Private mpreAllowtoMapLoanApprover As Boolean = False
    Private mpreAllowtoImportLoan_Advance As Boolean = False
    Private mpreAllowtoImportSavings As Boolean = False
    Private mpreAllowtoImportAccrueLeave As Boolean = False
    Private mpreAllowtoMigrateLeaveApprover As Boolean = False
    Private mpreAllowtoImportAttendanceData As Boolean = False
    Private mpreAllowtoMakeGroupAttendance As Boolean = False
    Private mpreAllowtoAddGeneralReminder As Boolean = False
    Private mpreAllowtoEditGeneralReminder As Boolean = False
    Private mpreAllowtoDeleteGeneralReminder As Boolean = False
    Private mpreAllowtoChangeUser As Boolean = False
    Private mpreAllowtoChangeCompany As Boolean = False
    Private mpreAllowtoChangeDatabase As Boolean = False
    Private mpreAllowtoChangePassword As Boolean = False
    Private mpreAllowtoViewSentItems As Boolean = False
    Private mpreAllowtoViewTrashItems As Boolean = False
    Private mpreAllowtoPrintLetters As Boolean = False
    Private mpreAllowtoExportLetters As Boolean = False
    Private mpreAllowtoReadMails As Boolean = False
    Private mpreAllowtoViewAuditTrails As Boolean = False
    Private mpreAllowtoViewApplicationEvtLog As Boolean = False
    Private mpreAllowtoViewUserAuthLog As Boolean = False
    Private mpreAllowtoViewUserAttemptsLog As Boolean = False
    Private mpreAllowtoViewAuditLogs As Boolean = False
    Private mpreAllowtoUnlockUser As Boolean = False
    Private mpreAllowtoExportAbilityLevel As Boolean = False
    Private mpreAllowtoSaveAbilityLevel As Boolean = False
    Private mpreAllowtoTakeDatabaseBackup As Boolean = False
    Private mpreAllowtoRestoreDatabase As Boolean = False
    Private mpreAllowtoAddVoidReason As Boolean = False
    Private mpreAllowtoEditVoidReason As Boolean = False
    Private mpreAllowtoDeleteVoidReason As Boolean = False
    Private mpreAllowtoAddReminderType As Boolean = False
    Private mpreAllowtoEditReminderType As Boolean = False
    Private mpreAllowtoDeleteReminderType As Boolean = False
    Private mpreAllowtoSavePasswordOption As Boolean = False
    Private mpreAllowtoAddDeviceSettings As Boolean = False
    Private mpreAllowtoEditDeviceSettings As Boolean = False
    Private mpreAllowtoDeleteDeviceSettings As Boolean = False
    Private mpreAllowtoChangeCompanyOptions As Boolean = False
    Private mpreAllowtoSaveCompanyGroup As Boolean = False
    'S.SANDEEP [ 29 OCT 2012 ] -- END



    'Pinkal (19-Nov-2012) -- Start
    'Enhancement : TRA Changes
    Private mpreAddLeaveExpense As Boolean = False
    Private mpreEditLeaveExpense As Boolean = False
    Private mpreDeleteLeaveExpense As Boolean = False
    'Pinkal (19-Nov-2012) -- End

    'S.SANDEEP [ 23 NOV 2012 ] -- START
    'ENHANCEMENT : TRA CHANGES
    Private mpreImportADUsers As Boolean = False
    Private mpreAssignUserAccess As Boolean = False
    Private mpreEditUserAccess As Boolean = False
    Private mpreExportPrivilegs As Boolean = False
    'S.SANDEEP [ 23 NOV 2012 ] -- END



    'Pinkal (22-Nov-2012) -- Start
    'Enhancement : TRA Changes
    Private mpreAllowtoChangeBranch As Boolean = False
    Private mpreAllowtoChangeDepartmentGroup As Boolean = False
    Private mpreAllowtoChangeDepartment As Boolean = False
    Private mpreAllowtoChangeSectionGroup As Boolean = False
    Private mpreAllowtoChangeSection As Boolean = False
    Private mpreAllowtoChangeUnitGroup As Boolean = False
    Private mpreAllowtoChangeUnit As Boolean = False
    Private mpreAllowtoChangeTeam As Boolean = False
    Private mpreAllowtoChangeJobGroup As Boolean = False
    Private mpreAllowtoChangeJob As Boolean = False
    Private mpreAllowtoChangeGradeGroup As Boolean = False
    Private mpreAllowtoChangeGrade As Boolean = False
    Private mpreAllowtoChangeGradeLevel As Boolean = False
    Private mpreAllowtoChangeClassGroup As Boolean = False
    Private mpreAllowtoChangeClass As Boolean = False
    Private mpreAllowtoChangeCostCenter As Boolean = False
    'Pinkal (22-Nov-2012) -- End


    'Pinkal (01-Dec-2012) -- Start
    'Enhancement : TRA Changes
    Private mpreAllowtoChangeCompanyEmail As Boolean = False
    'Pinkal (01-Dec-2012) -- End

'S.SANDEEP [ 01 DEC 2012 ] -- START
    'ENHANCEMENT : TRA CHANGES
    Private mpreAllowtoImportEmployee_User As Boolean = False
    'S.SANDEEP [ 01 DEC 2012 ] -- END

    'S.SANDEEP [ 13 DEC 2012 ] -- START
    'ENHANCEMENT : TRA CHANGES
    Private mpreAllowToViewMembershipOnDiary As Boolean = False
    Private mpreAllowToViewBanksOnDiary As Boolean = False
    'S.SANDEEP [ 13 DEC 2012 ] -- END



    'Pinkal (18-Dec-2012) -- Start
    'Enhancement : TRA Changes
    Private mpreAllowToAssignIssueUsertoEmp As Boolean = False
    Private mpreAllowToMigrateIssueUser As Boolean = False
    'Pinkal (18-Dec-2012) -- End

    'Sohail (13 Feb 2013) -- Start
    'TRA - ENHANCEMENT
    Private mpreAllowToAddPaymentApproverLevel As Boolean = False
    Private mpreAllowToEditPaymentApproverLevel As Boolean = False
    Private mpreAllowToDeletePaymentApproverLevel As Boolean = False
    Private mpreAllowToViewPaymentApproverLevelList As Boolean = False
    'Sohail (13 Feb 2013) -- End
    Private mpreAllowToSetPaymentApproverLevelActive As Boolean = False  'Sohail (29 Oct 2014)

    'S.SANDEEP [ 13 FEB 2013 ] -- START
    'ENHANCEMENT : TRA CHANGES
    Private mpreAllowToAddPaymentApproverMapping As Boolean = False
    Private mpreAllowToEditPaymentApproverMapping As Boolean = False
    Private mpreAllowToDeletePaymentApproverMapping As Boolean = False
    Private mpreAllowToApprovePayment As Boolean = False
    'S.SANDEEP [ 13 FEB 2013 ] -- END
    Private mpreAllowToVoidApprovedPayment As Boolean = False 'Sohail (13 Feb 2013)

    'Sohail (28 May 2014) -- Start
    'Enhancement - Staff Requisition.
    Private mpreAllowToAddStaffRequisitionApproverLevel As Boolean = False
    Private mpreAllowToEditStaffRequisitionApproverLevel As Boolean = False
    Private mpreAllowToDeleteStaffRequisitionApproverLevel As Boolean = False
    Private mpreAllowToViewStaffRequisitionApproverLevelList As Boolean = False
    Private mpreAllowToAddStaffRequisitionApproverMapping As Boolean = False
    Private mpreAllowToEditStaffRequisitionApproverMapping As Boolean = False
    Private mpreAllowToDeleteStaffRequisitionApproverMapping As Boolean = False
    Private mpreAllowToViewStaffRequisitionApproverMappingList As Boolean = False
    Private mpreAllowToAddStaffRequisition As Boolean = False
    Private mpreAllowToEditStaffRequisition As Boolean = False
    Private mpreAllowToDeleteStaffRequisition As Boolean = False
    Private mpreAllowToViewStaffRequisitionList As Boolean = False
    Private mpreAllowToApproveStaffRequisition As Boolean = False
    Private mpreAllowToCancelStaffRequisition As Boolean = False
    Private mpreAllowToViewStaffRequisitionApprovals As Boolean = False
    'Sohail (28 May 2014) -- End
    'Sohail (05 Aug 2014) -- Start
    'Enhancement - Statutory Payment Priviledge.
    Private mpreAllowToAddStatutoryPayment As Boolean = False
    Private mpreAllowToEditStatutoryPayment As Boolean = False
    Private mpreAllowToDeleteStatutoryPayment As Boolean = False
    Private mpreAllowToViewStatutoryPaymentList As Boolean = False
    'Sohail (05 Aug 2014) -- End
'Pinkal (06-Feb-2013) -- Start
    'Enhancement : TRA Changes
    Private mpreAllowToEndLeaveCycle As Boolean = False
    'Pinkal (06-Feb-2013) -- End


    'Pinkal (06-Mar-2013) -- Start
    'Enhancement : TRA Changes
    Private mpreAllowToAddMedicalDependantException As Boolean = False
    'Pinkal (06-Mar-2013) -- End


    'Pinkal (01-Apr-2013) -- Start
    'Enhancement : TRA Changes
    Private mpreAllowToAddEditPhoto As Boolean = False
    Private mpreAllowToDeletePhoto As Boolean = False
    Private mpreAllowToImportPhoto As Boolean = False
    'Pinkal (01-Apr-2013) -- End

    'S.SANDEEP [ 10 APR 2013 ] -- START
    'ENHANCEMENT : TRA CHANGES

    'S.SANDEEP [28 MAY 2015] -- START
    'ENHANCEMENT : NEW PRIVILEGE (Performance Assessment)
    'Private mpreAllowToSubmitBSCPlan_Approval As Boolean = False
    'S.SANDEEP [28 MAY 2015] -- END


    'S.SANDEEP [ 10 APR 2013 ] -- END

    'S.SANDEEP [ 14 May 2013 ] -- START
    'ENHANCEMENT : TRA ENHANCEMENT
    Private mpreAllowToSubmitApplicantFilter_Approval As Boolean = False
    Private mpreAllowToApproveApplicantFilter As Boolean = False
    Private mpreAllowToRejectApplicantFilter As Boolean = False
    Private mpreAllowToAddApplicantFilter As Boolean = False
    Private mpreAllowToDeleteApplicantFilter As Boolean = False
    Private mpreAllowtoSubmitApplicantEligibilityForApproval As Boolean = False
    Private mpreAllowtoApproveApplicantEligibility As Boolean = False
    Private mpreAllowtoDisapproveApplicantEligibility As Boolean = False
    'S.SANDEEP [ 14 May 2013 ] -- END

    'Pinkal (12-May-2013) -- Start
    'Enhancement : TRA Changes
    Private mpreAllowToSubmitForApprovalInFinalShortlistedApplicant As Boolean
    Private mpreAllowToApproveFinalShortListedApplicant As Boolean
    Private mpreAllowToDisapproveFinalShortListedApplicant As Boolean
    'Pinkal (12-May-2013) -- End


    'Pinkal (03-Jul-2013) -- Start
    'Enhancement : TRA Changes
    Private mpreAllowToMoveFinalShortListedApplicantToShortListed As Boolean = False
    'Pinkal (03-Jul-2013) -- End

    'S.SANDEEP [ 10 JUNE 2013 ] -- START
    'ENHANCEMENT : OTHER CHANGES
    Private mpreAllowtoAddPayActivity As Boolean = False
    Private mpreAllowtoEditPayActivity As Boolean = False
    Private mpreAllowtoDeletePayActivity As Boolean = False
    Private mpreAllowtoAddUnitOfMeasure As Boolean = False
    Private mpreAllowtoEditUnitOfMeasure As Boolean = False
    Private mpreAllowtoDeleteUnitOfMeasure As Boolean = False
    Private mpreAllowtoAdd_EditActivityRate As Boolean = False
    Private mpreAllowtoPostActivitytoPayroll As Boolean = False
    Private mpreAllowtoVoidActivtyfromPayroll As Boolean = False
    Private mpreAllowtoAdd_EditPayPerActivity As Boolean = False
    Private mpreAllowtoDeletePayPerActivity As Boolean = False
    Private mpreAllowtoExportPayPerActivity As Boolean = False
    Private mpreAllowtoImportPayPerActivity As Boolean = False
    Private mpreAllowtoAssignPayPerActivityGlobally As Boolean = False
    Private mpreAllowtoviewActivityList As Boolean = False
    Private mpreAllowtoViewMeasureList As Boolean = False
    'S.SANDEEP [ 10 JUNE 2013 ] -- END


    'Pinkal (24-Aug-2013) -- Start
    'Enhancement : TRA Changes
    Private mpreAllowToImportEmpIdentity As Boolean = False
    'Pinkal (24-Aug-2013) -- End


    'Pinkal (30-Dec-2013) -- Start
    'Enhancement : Oman Changes
    Private mpreAllowToEditTimesheetCard As Boolean = False
    Private mpreAllowToAddEditDeleteGlobalTimesheet As Boolean = False
    Private mpreAllowToRoundoffTimings As Boolean = False
    Private mpreAllowToReCalculateTimings As Boolean = False
    'Pinkal (30-Dec-2013) -- End


    'S.SANDEEP [ 28 JAN 2014 ] -- START
    Private mpreAllowToDeleteEmailNotificationAuditTrails As Boolean = False
    'S.SANDEEP [ 28 JAN 2014 ] -- END


    'Pinkal (06-May-2014) -- Start
    'Enhancement : TRA Changes
    Private mpreAllowToChangeOtherUserPassword As Boolean = False
    'Pinkal (06-May-2014) -- End


    'Pinkal (12-Sep-2014) -- Start
    'Medical Enhancement - TANAPA URGENT CHANGES FOR MEDICAL SICK SHEET GIVEN BY OLAIS
    Private mpreRevokeUserAccessonSicksheet As Boolean = False
    'Pinkal (12-Sep-2014) -- End

    'S.SANDEEP [ 17 OCT 2014 ] -- START
    Private mpreAllowtoDeletePayPerActivityAuditTrails = False
    'S.SANDEEP [ 17 OCT 2014 ] -- END


    'S.SANDEEP [28 MAY 2015] -- START
    'ENHANCEMENT : NEW PRIVILEGE (Performance Assessment)
    Private mpreAllowtoAddAssessmentPeriod As Boolean = False
    Private mpreAllowtoEditAssessmentPeriod As Boolean = False
    Private mpreAllowtoDeleteAssessmentPeriod As Boolean = False
    Private mpreAllowtoAddAssessorAccess As Boolean = False
    Private mpreAllowtoEditAssessorAccess As Boolean = False
    Private mpreAllowtoDeleteAssessorAccess As Boolean = False
    Private mpreAllowtoImportAssessor As Boolean = False
    Private mpreAllowtoAddReviewerAccess As Boolean = False
    Private mpreAllowtoEditReviewerAccess As Boolean = False
    Private mpreAllowtoDeleteReviewerAccess As Boolean = False
    Private mpreAllowtoImportReviewer As Boolean = False
    Private mpreAllowtoPerformAssessorReviewerMigration As Boolean = False
    Private mpreAllowtoAddAssessmentRatios As Boolean = False
    Private mpreAllowtoEditAssessmentRatios As Boolean = False
    Private mpreAllowtoDeleteAssessmentRatios As Boolean = False
    Private mpreAllowtoCloseAssessmentPeriod As Boolean = False
    Private mpreAllowtoAddAssessmentGroup As Boolean = False
    Private mpreAllowtoEditAssessmentGroup As Boolean = False
    Private mpreAllowtoDeleteAssessmentGroup As Boolean = False
    Private mpreAllowtoAddAssessmentScales As Boolean = False
    Private mpreAllowtoEditAssessmentScales As Boolean = False
    Private mpreAllowtoDeleteAssessmentScales As Boolean = False
    Private mpreAllowtoMapScaleGrouptoGoals As Boolean = False
    Private mpreAllowtoAddCompetencies As Boolean = False
    Private mpreAllowtoEditCompetencies As Boolean = False
    Private mpreAllowtoDeleteCompetencies As Boolean = False
    Private mpreAllowtoLoadfromCompetenciesLibrary As Boolean = False
    Private mpreAllowtoAddAssignedCompetencies As Boolean = False
    Private mpreAllowtoEditAssignedCompetencies As Boolean = False
    Private mpreAllowtoDeleteAssignedCompetencies As Boolean = False
    Private mpreAllowtoAddPerspective As Boolean = False
    Private mpreAllowtoEditPerspective As Boolean = False
    Private mpreAllowtoDeletePerspective As Boolean = False
    Private mpreAllowtoSaveBSCTitles As Boolean = False
    Private mpreAllowtoAddBSCTitlesMapping As Boolean = False
    Private mpreAllowtoEditBSCTitlesMapping As Boolean = False
    Private mpreAllowtoDeleteBSCTitlesMapping As Boolean = False
    Private mpreAllowtoSaveBSCTitlesViewSetting As Boolean = False
    Private mpreAllowtoAddCompanyGoals As Boolean = False
    Private mpreAllowtoEditCompanyGoals As Boolean = False
    Private mpreAllowtoDeleteCompanyGoals As Boolean = False
    Private mpreAllowtoCommitCompanyGoals As Boolean = False
    Private mpreAllowtoUnlockcommittedCompanyGoals As Boolean = False
    Private mpreAllowtoAddAllocationGoals As Boolean = False
    Private mpreAllowtoEditAllocationGoals As Boolean = False
    Private mpreAllowtoDeleteAllocationGoals As Boolean = False
    Private mpreAllowtoCommitAllocationGoals As Boolean = False
    Private mpreAllowtoUnlockcommittedAllocationGoals As Boolean = False
    Private mpreAllowtoPerformGlobalAssignAllocationGoals As Boolean = False
    Private mpreAllowtoUpdatePercentCompletedAllocationGoals As Boolean = False
    Private mpreAllowtoAddEmployeeGoals As Boolean = False
    Private mpreAllowtoEditEmployeeGoals As Boolean = False
    Private mpreAllowtoDeleteEmployeeGoals As Boolean = False
    Private mpreAllowtoPerformGlobalAssignEmployeeGoals As Boolean = False
    Private mpreAllowtoUpdatePercentCompletedEmployeeGoals As Boolean = False
    Private mpreAllowtoSubmitGoalsforApproval As Boolean = False
    Private mpreAllowtoApproveRejectGoalsPlanning As Boolean = False
    Private mpreAllowtoUnlockFinalSavedGoals As Boolean = False
    Private mpreAllowtoAddCustomHeaders As Boolean = False
    Private mpreAllowtoEditCustomHeaders As Boolean = False
    Private mpreAllowtoDeleteCustomHeaders As Boolean = False
    Private mpreAllowtoAddCustomItems As Boolean = False
    Private mpreAllowtoEditCustomItems As Boolean = False
    Private mpreAllowtoDeleteCustomItems As Boolean = False
    Private mpreAllowtoAddComputationFormula As Boolean = False
    Private mpreAllowtoDeleteComputationFormula As Boolean = False
    Private mpreAllowtoViewAssessmentPeriodList As Boolean = False
    Private mpreAllowtoViewAssessorAccessList As Boolean = False
    Private mpreAllowtoViewReviewerAccessList As Boolean = False
    Private mpreAllowtoViewAssessmentGroupList As Boolean = False
    Private mpreAllowtoViewCompanyGoalsList As Boolean = False
    Private mpreAllowtoViewAllocationGoalsList As Boolean = False
    Private mpreAllowtoViewEmployeeGoalsList As Boolean = False

    Private mpreAllowtoAddSelfEvaluation As Boolean = False
    Private mpreAllowtoEditSelfEvaluation As Boolean = False
    Private mpreAllowtoDeleteSelfEvaluation As Boolean = False
    Private mpreAllowtoUnlockcommittedSelfEvaluation As Boolean = False
    Private mpreAllowtoAddAssessorEvaluation As Boolean = False
    Private mpreAllowtoEditAssessorEvaluation As Boolean = False
    Private mpreAllowtoDeleteAssessorEvaluation As Boolean = False
    Private mpreAllowtoUnlockcommittedAssessorEvaluation As Boolean = False
    Private mpreAllowtoAddReviewerEvaluation As Boolean = False
    Private mpreAllowtoEditReviewerEvaluation As Boolean = False
    Private mpreAllowtoDeleteReviewerEvaluation As Boolean = False
    Private mpreAllowtoUnlockcommittedReviewerEvaluation As Boolean = False
    Private mpreAllowtoViewSelfEvaluationList As Boolean = False
    Private mpreAllowtoViewAssessorEvaluationList As Boolean = False
    Private mpreAllowtoViewReviewerEvaluationList As Boolean = False
    Private mpreAllowtoViewPerformanceEvaluation As Boolean = False


    'Shani(06-Feb-2016) -- Start
    'PA Changes Given By CCBRT
    Private mpreAllowToSaveAppraisalSetup As Boolean = False
    Private mpreAllowToSaveExportAppraisalAnalysis As Boolean = False
    Private mpreAllowToVoidEmployeeGoals As Boolean = False
    Private mpreAllowToVoidOwnerGoals As Boolean = False
    Private mpreAllowToOpenPeriodicReiew As Boolean = False
    Private mpreAllowToGlobalVoidAssessment As Boolean = False
    'Shani(06-Feb-2016) -- End

    'S.SANDEEP [28 MAY 2015] -- END


    'Pinkal (13-Jul-2015) -- Start
    'Enhancement - WORKING ON C & R ACCESS PRIVILEGE.
    Private mpreAllowtoAddClaimExpenses As Boolean = False
    Private mpreAllowtoEditClaimExpenses As Boolean = False
    Private mpreAllowtoDeleteClaimExpenses As Boolean = False
    Private mpreAllowtoViewClaimExpenseList As Boolean = False
    Private mpreAllowtoAddSectorRouteCost As Boolean = False
    Private mpreAllowtoEditSectorRouteCost As Boolean = False
    Private mpreAllowtoDeleteSectorRouteCost As Boolean = False
    Private mpreAllowtoViewSectorRouteCostList As Boolean = False
    Private mpreAllowtoAddEmpExpesneAssignment As Boolean = False
    Private mpreAllowtoDeleteEmpExpesneAssignment As Boolean = False
    Private mpreAllowtoViewEmpExpesneAssignment As Boolean = False
    Private mpreAllowtoAddExpenseApprovalLevel As Boolean = False
    Private mpreAllowtoEditExpenseApprovalLevel As Boolean = False
    Private mpreAllowtoDeleteExpenseApprovalLevel As Boolean = False
    Private mpreAllowtoViewExpenseApprovalLevelList As Boolean = False
    Private mpreAllowtoAddExpenseApprover As Boolean = False
    Private mpreAllowtoEditExpenseApprover As Boolean = False
    Private mpreAllowtoDeleteExpenseApprover As Boolean = False
    Private mpreAllowtoViewExpenseApproverList As Boolean = False
    Private mpreAllowtoMigrateExpenseApprover As Boolean = False
    Private mpreAllowtoSwapExpenseApprover As Boolean = False
    Private mpreAllowtoAddClaimExpenseForm As Boolean = False
    Private mpreAllowtoEditClaimExpenseForm As Boolean = False
    Private mpreAllowtoDeleteClaimExpenseForm As Boolean = False
    Private mpreAllowtoViewClaimExpenseFormList As Boolean = False
    Private mpreAllowtoCancelClaimExpenseForm As Boolean = False
    Private mpreAllowtoProcessClaimExpenseForm As Boolean = False
    Private mpreAllowtoViewProcessClaimExpenseFormList As Boolean = False
    Private mpreAllowtoPostClaimExpenseToPayroll As Boolean = False
    'Pinkal (13-Jeul-2015) -- End


    'Shani(17-Aug-2015) -- Start
    'Leave Enhancement : Putting Activate Feature in Leave Approver Master
    Private mpreAllowToSetLeaveActiveApprover As Boolean = False
    Private mpreAllowToSetLeaveInactiveApprover As Boolean = False
    Private mpreAllowToSetClaimActiveApprover As Boolean = False
    Private mpreAllowToSetClaimInactiveApprover As Boolean = False
    'Shani(17-Aug-2015) -- End


    'S.SANDEEP [10 AUG 2015] -- START
    'ENHANCEMENT : Aruti SaaS Changes
    Private mpreAllowtoViewReportAbilityLevel As Boolean = False
    Private mpreAllowtoExportReportAbilityLevel As Boolean = False
    Private mpreAllowtoSaveReportAbilityLevel As Boolean = False
    'S.SANDEEP [10 AUG 2015] -- END


    'S.SANDEEP [23 FEB 2016] -- START
    Private mpreAllowtoChangePreassignedCompetencies As Boolean = False
    'S.SANDEEP [23 FEB 2016] -- END

    'Nilay (27 Apr 2016) -- Start
    'Enhancement - 61.1 - Priority Order on Grade Group, Grade and Grade Level; Auto Salary Increment to next Grade Level as per Priority Order; Grade filter on Wages Table; Export and Import for Wages Table for KBC.
    '#23 (KBC & Kenya Project Comments List.xls) - Auto Salary incrment Per employee Anniversary,Employee Anniversay,All KBC Employees have Annivesary date for Salary increment,date is sent  when employee is confimed
    Private mpreAllowToAssignGradePriority As Boolean = False
    Private mpreAllowToViewSalaryAnniversaryMonth As Boolean = False
    Private mpreAllowToAddSalaryAnniversaryMonth As Boolean = False
    Private mpreAllowToDeleteSalaryAnniversaryMonth As Boolean = False
    'Nilay (27 Apr 2016) -- End

   'Shani (24-May-2016) -- Start
    Private mpreAllowtoComputataionProcess As Boolean = False
    Private mpreAllowtoComputataionVoidProcess As Boolean = False
    'Shani (24-May-2016) -- End


    'Nilay (06-Jun-2016) -- Start
    'Enhancement : Import option in Wages Table for KBC
    Private mpreAllowToImportWagesTable As Boolean = False
    Private mpreAllowToExportWagesTable As Boolean = False
    'Nilay (06-Jun-2016) -- End

    'Nilay (20-May-2016) -- Start
    'ENHANCEMENT - 61.1 - New Budget Redesign for Marie Stopes (Fund Sources, Funds Adjustments, Budget Formula and new Budget screen Allocation/Employee wise, Trans./Summary wise)
    Private mpreAllowToViewFundSource As Boolean = False
    Private mpreAllowToAddFundSource As Boolean = False
    Private mpreAllowToEditFundSource As Boolean = False
    Private mpreAllowToDeleteFundSource As Boolean = False

    Private mpreAllowToViewFundAdjustment As Boolean = False
    Private mpreAllowToAddFundAdjustment As Boolean = False
    Private mpreAllowToEditFundAdjustment As Boolean = False
    Private mpreAllowToDeleteFundAdjustment As Boolean = False
    'Nilay (20-May-2016) -- Endq

    'Sohail (07 Jun 2016) -- Start
    'Enhancement -  62.1 - New Budget Redesign for Marie Stopes (Fund Sources, Funds Adjustments, Budget Formula and new Budget screen Allocation/Employee wise, Trans./Summary wise).
    Private mpreAllowToViewFundActivity As Boolean = False
    Private mpreAllowToAddFundActivity As Boolean = False
    Private mpreAllowToEditFundActivity As Boolean = False
    Private mpreAllowToDeleteFundActivity As Boolean = False
    Private mpreAllowToViewFundActivityAdjustment As Boolean = False
    Private mpreAllowToAddFundActivityAdjustment As Boolean = False
    Private mpreAllowToEditFundActivityAdjustment As Boolean = False
    Private mpreAllowToDeleteFundActivityAdjustment As Boolean = False

    Private mpreAllowToViewBudgetFormula As Boolean = False
    Private mpreAllowToAddBudgetFormula As Boolean = False
    Private mpreAllowToEditBudgetFormula As Boolean = False
    Private mpreAllowToDeleteBudgetFormula As Boolean = False

    Private mpreAllowToViewBudget As Boolean = False
    Private mpreAllowToAddBudget As Boolean = False
    Private mpreAllowToEditBudget As Boolean = False
    Private mpreAllowToDeleteBudget As Boolean = False
    'Sohail (02 Sep 2016) -- Start
    'Enhancement -  63.1 - New Budget Redesign for Marie Stopes (Period Wise budget codes).
    'Private mpreAllowToSaveBudgetCodes As Boolean = False
    Private mpreAllowToAddBudgetCodes As Boolean = False
    Private mpreAllowToEditBudgetCodes As Boolean = False
    Private mpreAllowToDeleteBudgetCodes As Boolean = False
    Private mpreAllowToViewBudgetCodes As Boolean = False
    'Sohail (02 Sep 2016) -- End

    Private mpreAllowToViewFundProjectCode As Boolean = False
    Private mpreAllowToAddFundProjectCode As Boolean = False
    Private mpreAllowToEditFundProjectCode As Boolean = False
    Private mpreAllowToDeleteFundProjectCode As Boolean = False
    Private mpreAllowToMapLevelToApprover As Boolean = False
    Private mpreAllowToViewBudgetApproverLevelMapping As Boolean = False
    Private mpreAllowToDeleteBudgetApproverLevelMapping As Boolean = False
    Private mpreAllowToViewBudgetApproverLevel As Boolean = False
    Private mpreAllowToAddBudgetApproverLevel As Boolean = False
    Private mpreAllowToEditBudgetApproverLevel As Boolean = False
    Private mpreAllowToDeleteBudgetApproverLevel As Boolean = False
    Private mpreAllowToApproveBudget As Boolean = False
    Private mpreAllowToVoidApprovedBudget As Boolean = False
    Private mpreAllowToViewBudgetApprovals As Boolean = False
    'Sohail (07 Jun 2016) -- End

    'Sohail (06 Oct 2021) -- Start
    'NMB Enhancement : OLD-483 : Separate sender's email configuration on recruitment portal from company email.
    Private mpreAllowToViewEmailSetup As Boolean = False
    Private mpreAllowToAddEmailSetup As Boolean = False
    Private mpreAllowToEditEmailSetup As Boolean = False
    Private mpreAllowToDeleteEmailSetup As Boolean = False
    'Sohail (06 Oct 2021) -- End

    'Shani (26-Sep-2016) -- Start
    'Enhancement -Changes PA Flow And Add New screen Given By (Andrew)Goals Accomplishment
    Private mpreAllowToApproveGoalsAccomplishment As Boolean = False
    'Shani (26-Sep-2016) -- End


    'Pinkal (21-Oct-2016) -- Start
    'Enhancement - Working on Budget Employee Timesheet as Per Mr.Requirement.
    Private mpreAllowtoMapBudgetTimesheetApprover As Boolean = False
    'Pinkal (21-Oct-2016) -- End
    'Shani (08-Dec-2016) -- Start
    'Enhancement -  Add Employee Allocaion/Date Privilage
    Private mpreAllowToChangeEmpRecategorize As Boolean = False
    Private mpreAllowToChangeEmpTransfers As Boolean = False
    Private mpreAllowToChangeEmpWorkPermit As Boolean = False
    'Shani (08-Dec-2016) -- End

    'Pinkal (21-Dec-2016) -- Start
    'Enhancement - Adding Swap Approver Privilage for Leave Module.
    Private mpreAllowToSwapLeaveApprover As Boolean = False
    'Pinkal (21-Dec-2016) -- End

    'Nilay (13 Apr 2017) -- Start
    'Enhancements: Settings for mandatory options in online recruitment
    Private mpreAllowToViewSkillExpertise As Boolean = False
    Private mpreAllowToAddSkillExpertise As Boolean = False
    Private mpreAllowToEditSkillExpertise As Boolean = False
    Private mpreAllowToDeleteSkillExpertise As Boolean = False
    'Nilay (13 Apr 2017) -- End


    'Pinkal (03-May-2017) -- Start
    'Enhancement - Working On Implementing privileges in Budget Timesheet Module.
    Private mpreAllowToViewBudgetTimesheetApproverLevelList As Boolean = False
    Private mpreAllowToAddBudgetTimesheetApproverLevel As Boolean = False
    Private mpreAllowToEditBudgetTimesheetApproverLevel As Boolean = False
    Private mpreAllowToDeleteBudgetTimesheetApproverLevel As Boolean = False

    Private mpreAllowToViewBudgetTimesheetApproverList As Boolean = False
    Private mpreAllowToAddBudgetTimesheetApprover As Boolean = False
    Private mpreAllowToEditBudgetTimesheetApprover As Boolean = False
    Private mpreAllowToDeleteBudgetTimesheetApprover As Boolean = False
    Private mpreAllowToSetBudgetTimesheetApproverAsActive As Boolean = False
    Private mpreAllowToSetBudgetTimesheetApproverAsInActive As Boolean = False

    Private mpreAllowToViewEmployeeBudgetTimesheetList As Boolean = False
    Private mpreAllowToAddEmployeeBudgetTimesheet As Boolean = False
    Private mpreAllowToEditEmployeeBudgetTimesheet As Boolean = False
    Private mpreAllowToDeleteEmployeeBudgetTimesheet As Boolean = False
    Private mpreAllowToCancelEmployeeBudgetTimesheet As Boolean = False
    Private mpreAllowToViewPendingEmpBudgetTimesheetSubmitForApproval As Boolean = False
    Private mpreAllowToViewCompletedEmpBudgetTimesheetSubmitForApproval As Boolean = False
    Private mpreAllowToSubmitForApprovalForPendingEmpBudgetTimesheet As Boolean = False

    Private mpreAllowToViewEmployeeBudgetTimesheetApprovalList As Boolean = False
    Private mpreAllowToChangeEmployeeBudgetTimesheetStatus As Boolean = False

    Private mpreAllowToMigrateBudgetTimesheetApprover As Boolean = False

    Private mpreAllowToImportEmployeeShiftAssignment As Boolean = False

    'Pinkal (03-May-2017) -- End

    'Pinkal (23-AUG-2017) -- Start
    'Enhancement - Working on Enable/Disable Active Directory User for Budget Timesheet.
    Private mpreAllowToEnableDisableADUser As Boolean = False
    'Pinkal (23-AUG-2017) -- End


    'Varsha Rana (17-Oct-2017) -- Start
    'Enhancement - Give user privileges 

    Private mpreAllowToImportEmployeeAccountConfiguration As Boolean = False
    Private mpreAllowToExportEmployeeAccountConfiguration As Boolean = False
    Private mpreAllowToGetFileFormatOfEmployeeAccountConfiguration As Boolean = False

    'Private mpreAllowToAddPayslipGlobalMessage As Boolean = False
    'Private mpreAllowToEditPayslipGlobalMessage As Boolean = False
    'Private mpreAllowToDeletePayslipGlobalMessage As Boolean = False

    Private mpreAllowToAddLoanApproverLevel As Boolean = False
    Private mpreAllowToEditLoanApproverLevel As Boolean = False
    Private mpreAllowToDeleteLoanApproverLevel As Boolean = False

    Private mpreAllowToAddLoanApprover As Boolean = False
    Private mpreAllowToEditLoanApprover As Boolean = False
    Private mpreAllowToDeleteLoanApprover As Boolean = False
    Private mpreAllowToSetActiveInactiveLoanApprover As Boolean = False
    
    Private mpreAllowToViewLoanApproverLevelList As Boolean = False
    Private mpreAllowToViewLoanApproverList As Boolean = False

    Private mpreAllowToProcessGlobalLoanApprove As Boolean = False
    Private mpreAllowToProcessGlobalLoanAssign As Boolean = False

    Private mpreAllowToViewLoanApprovalList As Boolean = False
    Private mpreAllowToChangeLoanStatus As Boolean = False

    Private mpreAllowToTransferLoanApprover As Boolean = False
    Private mpreAllowToSwapLoanApprover As Boolean = False
    Private mpreAllowToHistoricalSalaryChange As Boolean = False

    Private mpreAllowToImportLeaveApprovers As Boolean = False
    Private mpreAllowToImportLeavePlanner As Boolean = False

    Private mpreAllowToViewDeviceUserMapping As Boolean = False

    Private mpreAllowToImportEmployeeBirthInfo As Boolean = False
    Private mpreAllowToImportEmployeeAddress As Boolean = False

    Private mpreAllowToViewMissingEmployee As Boolean = False

    Private mpreAllowToUpdateEmployeeDetails As Boolean = False
    Private mpreAllowToUpdateEmployeeMovements As Boolean = False

    Private mpreAllowToViewAssignedShiftList As Boolean = False
    Private mpreAllowToDeleteAssignedShift As Boolean = False

    Private mpreAllowToImportCompanyAssets As Boolean = False

    Private mpreAllowToExportEmployeeList As Boolean = False
    Private mpreAllowToAddRemoveFields As Boolean = False

    Private mpreAllowToEditTransferEmployeeDetails As Boolean = False
    Private mpreAllowToDeleteTransferEmployeeDetails As Boolean = False

    Private mpreAllowToEditRecategorizeEmployeeDetails As Boolean = False
    Private mpreAllowToDeleteRecategorizeEmployeeDetails As Boolean = False

    Private mpreAllowToEditProbationEmployeeDetails As Boolean = False
    Private mpreAllowToDeleteProbationEmployeeDetails As Boolean = False

    Private mpreAllowToEditConfirmationEmployeeDetails As Boolean = False
    Private mpreAllowToDeleteConfirmationEmployeeDetails As Boolean = False

    Private mpreAllowToEditSuspensionEmployeeDetails As Boolean = False
    Private mpreAllowToDeleteSuspensionEmployeeDetails As Boolean = False

    Private mpreAllowToEditTerminationEmployeeDetails As Boolean = False
    Private mpreAllowToDeleteTerminationEmployeeDetails As Boolean = False

    Private mpreAllowToAssignBenefitGroup As Boolean = False
    Private mpreAllowToRehireEmployee As Boolean = False

    Private mpreAllowToEditWorkPermitEmployeeDetails As Boolean = False
    Private mpreAllowToDeleteWorkPermitEmployeeDetails As Boolean = False

    Private mpreAllowToEditRetiredEmployeeDetails As Boolean = False
    Private mpreAllowToDeleteRetiredEmployeeDetails As Boolean = False
    Private mpreAllowToGlobalVoidEmployeeMembership As Boolean = False

    Private mpreAllowToAddShiftPolicyAssignment As Boolean = False
    Private mpreAllowToAssignEmployeeDayOff As Boolean = False
    Private mpreAllowToViewEmployeeDayOff As Boolean = False
    Private mpreAllowToDeleteEmployeeDayOff As Boolean = False

    Private mpreAllowToViewAssignedPolicyList As Boolean = False
    Private mpreAllowToDeleteAssignedPolicy As Boolean = False

    Private mpreAllowToViewEligibleApplicants As Boolean = False

    Private mpreAllowToPerformComputeScoreProcess As Boolean = False
    Private mpreAllowToPerformVoidComputedScore As Boolean = False

    Private mpreAllowToEditAppraisalSetup As Boolean = False
    Private mpreAllowToDeleteAppraisalSetup As Boolean = False

    Private mpreAllowToImportTrainingEnrollment As Boolean = False

    Private mpreAllowToPostNewProceedings As Boolean = False
    Private mpreAllowToEditViewProceedings As Boolean = False

    Private mpreAllowToScanAttachmentDocuments As Boolean = False
    Private mpreAllowToProceedingSubmitForApproval As Boolean = False
    Private mpreAllowToApproveDisapproveProceedings As Boolean = False
    Private mpreAllowToExemptTransactionHead As Boolean = False
    Private mpreAllowToPostTransactionHead As Boolean = False
    Private mpreAllowToViewDisciplineHead As Boolean = False
    Private mpreAllowToPrintPreviewDisciplineCharge As Boolean = False

    Private mpreAllowToAddDisciplinaryCommittee As Boolean = False
    Private mpreAllowToEditDeleteDisciplinaryCommittee As Boolean = False

    Private mpreAllowToViewDisciplineProceedingList As Boolean = False

    Private mpreAllowToViewDisciplineHearingList As Boolean = False
    Private mpreAllowToAddDisciplineHearing As Boolean = False
    Private mpreAllowToEditDisciplineHearing As Boolean = False
    Private mpreAllowToDeleteDisciplineHearing As Boolean = False
    Private mpreAllowToMarkAsClosedDisciplineHearing As Boolean = False

    Private mpreAllowToPerformGlobalGoalOperations As Boolean = False
    Private mpreAllowToPerformGlobalVoidGoals As Boolean = False
    Private mpreAllowToSubmitForGoalAccomplished As Boolean = False
    Private mpreAllowToImportEmployeeGoals As Boolean = False

    Private mpreAllowToPrintEmployeeScoreCard As Boolean = False
    Private mpreAllowToExportEmployeeScoreCard As Boolean = False
    Private mpreAllowToSetTranHeadInactive As Boolean = False

    Private mpreAllowToAddLeaveFrequency As Boolean = False
    Private mpreAllowToDeleteLeaveFrequency As Boolean = False
    Private mpreAllowToViewLeaveFrequency As Boolean = False

    Private mpreAllowToPerformLeaveAdjustment As Boolean = False

    Private mpreAllowToEditRehireEmployeeDetails As Boolean = False
    Private mpreAllowToDeleteRehireEmployeeDetails As Boolean = False

    Private mpreAllowToPerformEligibleOperation As Boolean = False
    Private mpreAllowToPerformNotEligibleOperation As Boolean = False
    'Varsha Rana (17-Oct-2017) -- End

'S.SANDEEP [11-AUG-2017] -- START
    'ISSUE/ENHANCEMENT : ARUTI SYMMETRY INTEGRATION
    Private mpreAllowtoSeeEmployeeSignature As Boolean = False
    'S.SANDEEP [11-AUG-2017] -- END

    'S.SANDEEP [16-Jan-2018] -- START
    'ISSUE/ENHANCEMENT : REF-ID # 118
    Private mpreAllowtoAddEmployeeSignature As Boolean = False
    Private mpreAllowtoDeleteEmployeeSignature As Boolean = False
    'S.SANDEEP [16-Jan-2018] -- END

    'S.SANDEEP [12-Apr-2018] -- START
    'ISSUE/ENHANCEMENT : {Ref#218|#ARUTI-106}
    Private mpreAllowtoViewPendingAccrueData As Boolean = False
    'S.SANDEEP [12-Apr-2018] -- END


    'S.SANDEEP [20-JUN-2018] -- Start
    'Enhancement - Implementing Employee Approver Flow For NMB .

    Private mpreAllowToViewEmpApproverLevelList As Boolean = False
    Private mpreAllowToAddEmpApproverLevel As Boolean = False
    Private mpreAllowToEditEmpApproverLevel As Boolean = False
    Private mpreAllowToDeleteEmpApproverLevel As Boolean = False

    Private mpreAllowToViewEmployeeApproverList As Boolean = False
    Private mpreAllowToAddEmployeeApprover As Boolean = False
    Private mpreAllowToDeleteEmployeeApprover As Boolean = False
    Private mpreAllowtoSetInactiveEmployeeApprover As Boolean = False
    Private mpreAllowToSetActiveEmployeeApprover As Boolean = False

    Private mpreAllowToPerformEmpSubmitForApproval As Boolean = False

    'S.SANDEEP [20-JUN-2018] -- End


    'Gajanan [13-AUG-2018] -- Start
    'Enhancement - Implementing Grievance Module.
    Private mpreAllowtoApproveGrievance As Boolean = False

    Private mpreAllowToAddGrievanceApproverLevel As Boolean = False
    Private mpreAllowToEditGrievanceApproverLevel As Boolean = False
    Private mpreAllowToDeleteGrievanceApproverLevel As Boolean = False
    Private mpreAllowToViewGrievanceApproverLevel As Boolean = False
   
    Private mpreAllowToAddGrievanceApprover As Boolean = False
    Private mpreAllowToEditGrievanceApprover As Boolean = False
    Private mpreAllowToDeleteGrievanceApprover As Boolean = False
    Private mpreAllowToViewGrievanceApprover As Boolean = False
    Private mpreAllowToActivateGrievanceApprover As Boolean = False
    Private mpreAllowToInActivateGrievanceApprover As Boolean = False

    'Gajanan(13-AUG-2018) -- End

    'Hemant (27 Aug 2018) -- Start
    'Enhancement : Implementing Grievance Module
    Private mpreAllowToViewGrievanceResolutionStepList As Boolean = False
    Private mpreAllowToAddGrievanceResolutionStep As Boolean = False
    Private mpreAllowToEditGrievanceResolutionStep As Boolean = False
    Private mpreAllowToDeleteGrievanceResolutionStep As Boolean = False
    'Hemant (27 Aug 2018)) -- End

    'S.SANDEEP [09-OCT-2018] -- START
    Private mpreAllowToAddTrainingApproverLevel As Boolean = False
    Private mpreAllowToEditTrainingApproverLevel As Boolean = False
    Private mpreAllowToDeleteTrainingApproverLevel As Boolean = False
    Private mpreAllowToViewTrainingApproverLevel As Boolean = False
    Private mpreAllowToAddTrainingApprover As Boolean = False
    Private mpreAllowToDeleteTrainingApprover As Boolean = False
    Private mpreAllowToViewTrainingApprover As Boolean = False
    Private mpreAllowToApproveTrainingRequisition As Boolean = False
    Private mpreAllowToActivateTrainingApprover As Boolean = False
    Private mpreAllowToDeactivateTrainingApprover As Boolean = False
    'S.SANDEEP [09-OCT-2018] -- END

    'Hemant (22 Oct 2018) -- Start
    'Enhancement : Implementing New Module of OT Requisition Approver Level & Approver Master
    Private mpreAllowToViewOTRequisitionApproverLevel As Boolean = False
    Private mpreAllowToAddOTRequisitionApproverLevel As Boolean = False
    Private mpreAllowToEditOTRequisitionApproverLevel As Boolean = False
    Private mpreAllowToDeleteOTRequisitionApproverLevel As Boolean = False
    Private mpreAllowToAddOTRequisitionApprover As Boolean = False
    Private mpreAllowToEditOTRequisitionApprover As Boolean = False
    Private mpreAllowToDeleteOTRequisitionApprover As Boolean = False
    Private mpreAllowToViewOTRequisitionApprover As Boolean = False
    Private mpreAllowToActivateOTRequisitionApprover As Boolean = False
    Private mpreAllowToInActivateOTRequisitionApprover As Boolean = False
    Private mpreAllowtoApproveOTRequisition As Boolean = False
    'Hemant (22 Oct 2018) -- End


    'Pinkal (03-Dec-2018) -- Start
    'Enhancement - Working on Asset Declaration Notification settings and notification from reflex.
    Private mpreAllowtoUnlockEmployeeForAssetDeclaration As Boolean = False
    'Pinkal (03-Dec-2018) -- End
    'Sohail (04 Feb 2020) -- Start
    'NMB Enhancement # : New screen "Non-Disclosure Declaration" with lock / unlock options.
    Private mpreAllowtoUnlockEmployeeForNonDisclosureDeclaration As Boolean = False
    'Sohail (04 Feb 2020) -- End

    'Gajanan [17-DEC-2018] -- Start
    'Enhancement - Implementing Employee Approver Flow On Employee Data.
    Private mpreAllowToApproveRejectEmployeeQualifications As Boolean = False
    Private mpreAllowToApproveRejectEmployeeReferences As Boolean = False
    Private mpreAllowToApproveRejectEmployeeSkills As Boolean = False
    Private mpreAllowToApproveRejectEmployeeJobExperiences As Boolean = False
    'Gajanan [17-DEC-2018] -- End


    'Gajanan [22-Feb-2019] -- Start
    'Enhancement - Implementing Employee Approver Flow On Employee Data.
    Private mpreAllowToApproveRejectEmployeeDependants As Boolean = False
    Private mpreAllowToApproveRejectEmployeeIdentities As Boolean = False
    Private mpreAllowToApproveRejectEmployeeMemberships As Boolean = False
    Private mpreAllowToApproveRejectEmployeeBenefits As Boolean = False
    'Gajanan [22-Feb-2019] -- End
#End Region

#Region " Properties "

'S.SANDEEP [28-May-2018] -- START 
'ISSUE/ENHANCEMENT : {Audit Trails} 
Private mstrFormName As String = String.Empty
Public WriteOnly Property _FormName() As String 
Set(ByVal value As String) 
mstrFormName = value 
End Set 
End Property 
 
Private mstrClientIP As String = ""
Public WriteOnly Property _ClientIP() As String 
Set(ByVal value As String) 
mstrClientIP = value 
End Set 
End Property 
 
Private mstrHostName As String = ""
Public WriteOnly Property _HostName() As String 
Set(ByVal value As String) 
mstrHostName = value 
End Set 
End Property 
 
Private mblnIsWeb As Boolean = False
Public WriteOnly Property _FromWeb() As Boolean 
Set(ByVal value As Boolean) 
mblnIsWeb = value 
End Set 
End Property 
 
    Private mintAuditUserId As Integer = 0
    Public WriteOnly Property _AuditUserId() As Integer
        Set(ByVal value As Integer)
            mintAuditUserId = value
        End Set
    End Property

    Private mintCompanyUnkid As Integer = 0
    Public WriteOnly Property _CompanyUnkid() As Integer
        Set(ByVal value As Integer)
            mintCompanyUnkid = value
        End Set
    End Property
 
Private mdtAuditDate As DateTime = Now
Public WriteOnly Property _AuditDate() As DateTime 
Set(ByVal value As DateTime) 
mdtAuditDate = value 
End Set 
End Property 
 
Private xDataOpr As clsDataOperation = Nothing
Public Property _xDataOpr() As clsDataOperation 
Get 
Return xDataOpr 
End Get 
Set(ByVal value As clsDataOperation) 
xDataOpr = value 
End Set 
End Property 
 
Private mintLoginEmployeeunkid As Integer = 0
Public Property _LoginEmployeeunkid() As Integer 
Get 
Return mintLoginEmployeeunkid 
End Get 
Set(ByVal value As Integer) 
mintLoginEmployeeunkid = value 
End Set 
End Property 
 
Private mintVoidlogingemployeeunkid As Integer=0
Public Property _Voidloginemployeeunkid() As Integer 
Get 
Return mintVoidlogingemployeeunkid 
End Get 
Set(ByVal value As Integer) 
mintVoidlogingemployeeunkid = value 
End Set 
End Property 
'S.SANDEEP [28-May-2018] -- END  
 'Gajanan [18-Mar-2019] -- Start
 'Enhancement - Implementing Employee Approver Flow On Employee Data.
    Private mpreAllowToApproveRejectEmployeeAddress As Boolean = False
    Private mpreAllowToApproveRejectEmployeeEmergencyAddress As Boolean = False
 'Gajanan [18-Mar-2019] -- End


    'Gajanan [17-April-2019] -- Start
    'Enhancement - Implementing Employee Approver Flow On Employee Data.
    Private mpreAllowToApproveRejectEmployeePersonalinfo As Boolean = False
    'Gajanan [17-April-2019] -- End

    'S.SANDEEP |27-MAY-2019| -- START
    'ISSUE/ENHANCEMENT : [Score Calibration Process in Performance & Assessment Module]
    Private mpreAllowToViewCalibrationApproverLevel As Boolean = False
    Private mpreAllowToAddCalibrationApproverLevel As Boolean = False
    Private mpreAllowToEditCalibrationApproverLevel As Boolean = False
    Private mpreAllowToDeleteCalibrationApproverLevel As Boolean = False
    Private mpreAllowToViewCalibrationApproverMaster As Boolean = False
    Private mpreAllowToAddCalibrationApproverMaster As Boolean = False
    Private mpreAllowToActivateCalibrationApproverMaster As Boolean = False
    Private mpreAllowToDeleteCalibrationApproverMaster As Boolean = False
    Private mpreAllowToDeactivateCalibrationApproverMaster As Boolean = False
    Private mpreAllowToApproveRejectCalibratedScore As Boolean = False
    'S.SANDEEP |27-MAY-2019| -- END
    
    'Pinkal (27-Jun-2019) -- Start
    'Enhancement - IMPLEMENTING OT MODULE.
    Private mpreAllowtoViewOTRequisitionApprovalList As Boolean = False
     'Pinkal (27-Jun-2019) -- End
    
    'S.SANDEEP |27-JUL-2019| -- START
    'ISSUE/ENHANCEMENT : CALIBRATION COMMENTS FROM NMB
    Private mpreAllowToEditCalibratedScore As Boolean = False
    'S.SANDEEP |27-JUL-2019| -- END

    'Pinkal (13-Aug-2019) -- Start
    'Enhancement [0003693 - PACT] - Working on Allow people to exceed time assigned to the project initially.
    Private mpreAllowToViewBudgetTimesheetExemptEmployeeList As Boolean = False
    Private mpreAllowToAddBudgetTimesheetExemptEmployee As Boolean = False
    Private mpreAllowToDeleteBudgetTimesheetExemptEmployee As Boolean = False
    'Pinkal (13-Aug-2019) -- End

'S.SANDEEP |16-AUG-2019| -- START
    'ISSUE/ENHANCEMENT : ZRA {Language & Custom Item} & NMB Email Notification {Calibration}
    Private mpreAllowtoCalibrateProvisionalScore As Boolean = False
    'S.SANDEEP |16-AUG-2019| -- END

    'S.SANDEEP |18-JAN-2020| -- START
    'ISSUE/ENHANCEMENT : PA-OPTIMIZATION
    Private mpreAllowToUnlockEmployeeInPlanning As Boolean = False
    Private mpreAllowToUnlockEmployeeInAssessment As Boolean = False
    'S.SANDEEP |18-JAN-2020| -- END


    'Pinkal (29-Jan-2020) -- Start
    'Enhancement - Changes related To OT NMB Testing.
    Private mpreAllowToPostOTRequisitionToPayroll As Boolean = False
    'Pinkal (29-Jan-2020) -- End


    'Pinkal (03-Mar-2020) -- Start
    'ENHANCEMENT NMB:  Working on OT Requisition Approver Migration and transfer and recategorization migration enforcement screen.
    Private mpreAllowToMigrateOTRequisitionApprover As Boolean = False
    'Pinkal (03-Mar-2020) -- End

    'S.SANDEEP |01-MAY-2020| -- START
    'ISSUE/ENHANCEMENT : CALIBRATION MAKEOVER
    Private mpreAllowToViewCalibratorList As Boolean = False
    Private mpreAllowToAddCalibrator As Boolean = False
    Private mpreAllowToMakeCalibratorActive As Boolean = False
    Private mpreAllowToDeleteCalibrator As Boolean = False
    Private mpreAllowToMakeCalibratorInactive As Boolean = False
    Private mpreAllowToEditCalibrator As Boolean = False
    Private mpreAllowToEditCalibrationApproverMaster As Boolean = False
    'S.SANDEEP |01-MAY-2020| -- END

    'Gajanan [24-Aug-2020] -- Start
    'NMB Enhancement : Allow to set account configuration mapping 
    'inactive for closed period to allow to map head on other account 
    'configuration screen from new period
    Private mpreAllowToInactiveEmployeeCostCenter As Boolean = False
    Private mpreAllowToInactiveCompanyAccountConfiguration As Boolean = False
    Private mpreAllowToInactiveEmployeeAccountConfiguration As Boolean = False
    Private mpreAllowToInactiveCostCenterAccountConfiguration As Boolean = False
    'Gajanan [24-Aug-2020] -- End

    'Hemant (26 Nov 2020) -- Start
    'Enhancement : Talent Pipeline new changes
    Private mpreAllowToAddPotentialTalentEmployee As Boolean = False
    Private mpreAllowToApproveRejectTalentPipelineEmployee As Boolean = False
    Private mpreAllowToMoveApprovedEmployeeToQulified As Boolean = False
    Private mpreAllowToViewPotentialTalentEmployee As Boolean = False
    Private mpreAllowToRemoveTalentProcess As Boolean = False
    'Hemant (26 Nov 2020) -- End

    'Gajanan [17-Dec-2020] -- Start
    Private mpreAllowToAddPotentialSuccessionEmployee As Boolean = False
    Private mpreAllowToApproveRejectSuccessionPipelineEmployee As Boolean = False
    Private mpreAllowToMoveApprovedEmployeeToQulifiedSuccession As Boolean = False
    Private mpreAllowToViewPotentialSuccessionEmployee As Boolean = False
    Private mpreAllowToRemoveSuccessionProcess As Boolean = False
    'Gajanan [17-Dec-2020] -- End

    'Gajanan [19-Feb-2021] -- Start
    Private mpreAllowToSendNotificationToTalentScreener As Boolean = False
    Private mpreAllowToSendNotificationToTalentEmployee As Boolean = False
    Private mpreAllowForTalentScreeningProcess As Boolean = False

    Private mpreAllowToSendNotificationToSuccessionScreener As Boolean = False
    Private mpreAllowToSendNotificationToSuccessionEmployee As Boolean = False
    Private mpreAllowForSuccessionScreeningProcess As Boolean = False

    Private mpreAllowToSendNotificationToTalentApprover As Boolean = False
    Private mpreAllowToSendNotificationToSuccessionApprover As Boolean = False

    'Gajanan [19-Feb-2021] -- End

'Gajanan [30-JAN-2021] -- Start   
    'Enhancement:Worked On PDP Module
    Private mpreAllowToViewLearningAndDevelopmentPlan As Boolean = False
    Private mpreAllowToAddPersonalAnalysisGoal As Boolean = False
    Private mpreAllowToEditPersonalAnalysisGoal As Boolean = False
    Private mpreAllowToDeletePersonalAnalysisGoal As Boolean = False
    Private mpreAllowToAddDevelopmentActionPlan As Boolean = False
    Private mpreAllowToEditDevelopmentActionPlan As Boolean = False
    Private mpreAllowToDeleteDevelopmentActionPlan As Boolean = False
    Private mpreAllowToAddUpdateProgressForDevelopmentActionPlan As Boolean = False
    'Gajanan [30-JAN-2021] -- End

    'Gajanan [26-Feb-2021] -- Start
    Private mpreAllowToNominateEmployeeForSuccession As Boolean = False

    Private mpreAllowToModifyTalentSetting As Boolean = False
    Private mpreAllowToViewTalentSetting As Boolean = False
    Private mpreAllowToModifySuccessionSetting As Boolean = False
    Private mpreAllowToViewSuccessionSetting As Boolean = False
    'Gajanan [26-Feb-2021] -- End


    'Pinkal (03-Mar-2021)-- Start
    'KADCO Enhancement  -  Working on  KADCO - Option to Adjust Expense Accrue Balance.
    Private mpreAllowToAdjustExpenseBalance As Boolean = False
    'Pinkal (03-Mar-2021) -- End


    'Pinkal (10-Mar-2021) -- Start
    'Enhancement/Bug Retirement -   Working Retirement enhancement/Bug.
    Private mpreAllowToViewRetireClaimApplication As Boolean = False
    Private mpreAllowToRetireClaimApplication As Boolean = False
    Private mpreAllowToEditRetiredClaimApplication As Boolean = False
    Private mpreAllowToViewRetiredApplicationApproval As Boolean = False
    Private mpreAllowToApproveRetiredApplication As Boolean = False
    Private mpreAllowToPostRetiredApplicationtoPayroll As Boolean = False
    'Pinkal (10-Mar-2021) -- End

Private mpreAllowToViewTalentScreeningDetail As Boolean = False
    Private mpreAllowToViewSuccessionScreeningDetail As Boolean = False

    'Sohail (01 Mar 2021) -- Start
    'NMB Enhancement : : New screen Departmental Training Need in New UI.
    Private mpreAllowToViewDepartmentalTrainingNeed As Boolean = False
    Private mpreAllowToAddDepartmentalTrainingNeed As Boolean = False
    Private mpreAllowToEditDepartmentalTrainingNeed As Boolean = False
    Private mpreAllowToDeleteDepartmentalTrainingNeed As Boolean = False
    'Sohail (01 Mar 2021) -- End

    'Hemant (26 Mar 2021) -- Start
    'NMB Enhancement : : Changes in Departmental Training Need as per comments in online doc in New UI.
    Private mpreAllowToSubmitForApprovalFromDepartmentalTrainingNeed As Boolean = False
    Private mpreAllowToTentativeApproveForDepartmentalTrainingNeed As Boolean = False
    Private mpreAllowToSubmitForApprovalFromTrainingBacklog As Boolean = False
    Private mpreAllowToFinalApproveDepartmentalTrainingNeed As Boolean = False
    Private mpreAllowToRejectDepartmentalTrainingNeed As Boolean = False
    Private mpreAllowToUnlockSubmitApprovalForDepartmentalTrainingNeed As Boolean = False
    Private mpreAllowToUnlockSubmittedForTrainingBudgetApproval As Boolean = False
    Private mpreAllowToAskForReviewTrainingAsPerAmountSet As Boolean = False
    Private mpreAllowToUndoApprovedTrainingBudgetApproval As Boolean = False
    'Hemant (26 Mar 2021) -- End

    Private mpreAllowToAddCommentForDevelopmentActionPlan As Boolean = False
    Private mpreAllowToViewActionPlanAssignedTrainingCourses As Boolean = False

    'Sohail (12 Apr 2021) -- Start
    'NMB Enhancement : : Changes in Departmental Training Need as per comments in online doc in New UI.
    Private mpreAllowToUndoRejectedTrainingBudgetApproval As Boolean = False
    'Sohail (12 Apr 2021) -- End

    'Sohail (26 Apr 2021) -- Start
    'NMB Enhancement : : Changes in Departmental Training Need as per comments in online doc in New UI.
    Private mpreAllowToViewBudgetSummaryForDepartmentalTrainingNeed As Boolean = False
    Private mpreAllowToSetMaxBudgetForDepartmentalTrainingNeed As Boolean = False
    'Sohail (26 Apr 2021) -- End

    'Hemant (15 Apr 2021) -- Start
    'NMB Enhancement : : Approvers concepts need to be revisited. Should be based on the concept of Allocations and no on employee assignment.
    Private mpreAllowToApproveTrainingRequestRelatedToCost As Boolean = False
    Private mpreAllowToApproveTrainingRequestForeignTravelling As Boolean = False
    'Hemant (15 Apr 2021) -- End

Private mpreAllowToUnlockPDPForm As Boolean = False
    Private mpreAllowToLockPDPForm As Boolean = False
    Private mpreAllowToUndoUpdateProgressForDevelopmentActionPlan As Boolean = False

    'Hemant (18 May 2021) -- Start
    'ISSUE/ENHANCEMENT : Changed approval flow for Training Completion Status
    Private mpreAllowToApproveRejectTrainingCompletion As Boolean = False
    Private mpreAllowToAddAttendedTraining As Boolean = False
    'Hemant (18 May 2021) -- End

    'Hemant (25 May 2021) -- Start
    'ISSUE/ENHANCEMENT : OLD-398 - NMB COE Modules demo feedback for Training Modulue
    Private mpreAllowToMarkTrainingAsComplete As Boolean = False
    'Hemant (25 May 2021) -- End

    'Hemant (29 Apr 2022) -- Start
    'ISSUE/ENHANCEMENT : AC2-314(NMB) - NMB - As a manager, I want different approval flow (Training requests) for different training courses.
    Private mpreAllowToApproveTrainingRequestLocalTravelling As Boolean = False
    'Hemant (29 Apr 2022) -- End

    'Hemant (11 Jul 2022) -- Start            
    'ENHANCEMENT(NMB) : AC2-720 - Training approver add/edit with User Mapping
    Private mpreAllowToViewTrainingApproverEmployeeMappingList As Boolean = False
    Private mpreAllowToEditTrainingApproverEmployeeMapping As Boolean = False
    Private mpreAllowToDeleteTrainingApproverEmployeeMapping As Boolean = False
    Private mpreAllowToSetActiveInactiveTrainingApproverEmployeeMapping As Boolean = False
    Private mpreAllowToAddTrainingApproverEmployeeMapping As Boolean = False
    Private mpreAllowToApproveTrainingRequestEmployeeMapping As Boolean = False
    'Hemant (11 Jul 2022) -- End

    'Pinkal (27-Oct-2022) -- Start
    'NMB Loan Module Enhancement.
    Private mpreAllowToChangeDeductionPeriodOnLoanApproval As Boolean = False
    'Pinkal (27-Oct-2022) -- End

    'Pinkal (23-Nov-2022) -- Start
    'NMB Loan Module Enhancement.
    Private mpreAllowToAccessLoanDisbursementDashboard As Boolean = False
    Private mpreAllowToGetCRBData As Boolean = False
    'Pinkal (23-Nov-2022) -- End

    'Pinkal (14-Dec-2022) -- Start
    'NMB Loan Module Enhancement.
    Private mpreAllowToSetDisbursementTranches As Boolean = False
    'Pinkal (14-Dec-2022) -- End


    'Pinkal (06-Jan-2023) -- Start
    '(A1X-540) FHS/PSI - As a user, I want to select the cost center that applies to my project when filling my project budget timesheets.
    Private mpreAllowToViewLoanSchemeRoleMapping As Boolean = False
    Private mpreAllowToAddLoanSchemeRoleMapping As Boolean = False
    Private mpreAllowToEditLoanSchemeRoleMapping As Boolean = False
    Private mpreAllowToDeleteLoanSchemeRoleMapping As Boolean = False
    'Pinkal (06-Jan-2023) -- End

    'Pinkal (20-Feb-2023) -- Start
    'NMB - Loan Approval Screen Enhancements.
    Private mpreAllowToViewLoanExposuresOnLoanApproval As Boolean = False
    'Pinkal (20-Feb-2023) -- End

    'Pinkal (31-Mar-2023) -- Start
    '(A1X-717) NMB - As a user, based on the data displayed from the selection, I want to be able to shoot emails from the Feedback screen instead of going to the mail utility again. I should be able to select the template directly from this screen and input the subject.
    Private mpreAllowToSendCandidateFeedback As Boolean = False
    'Pinkal (31-Mar-2023) -- End

    'Pinkal (28-Apr-2023) -- Start
    '(A1X-865) NMB - Read aptitude test results from provided views.
    Private mpreAllowToDownloadAptitudeResult As Boolean = False
    'Pinkal (28-Apr-2023) -- End

    'Pinkal (03-Jun-2023) -- Start
    'NMB Enhancement - Adding Staff Loan Balance View privilege.
    Private mpreAllowToViewStaffLoanBalance As Boolean = False
    'Pinkal (03-Jun-2023) -- End

    'S.SANDEEP |05-JUN-2023| -- START
    'ISSUE/ENHANCEMENT : A1X-948
    Private mpreAllowToApproveVacancyChanges As Boolean = False
    'S.SANDEEP |0-JUN-2023| -- END

    'Hemant (26 May 2023) -- Start
    'ENHANCEMENT(TRA) : A1X-943 - Integrate NBC payment API in Aruti
    Private mpreAllowToApproveRejectPaymentBatchPosting As Boolean = False
    'Hemant (26 May 2023) -- End

    'Pinkal (26-Jun-2023) -- Start
    'NMB Enhancement :(A1X-1029) NMB - Staff transfers approval(role based).
    Private mpreAllowToViewStaffTransferApproverLevel As Boolean = False
    Private mpreAllowToAddStaffTransferApproverLevel As Boolean = False
    Private mpreAllowToEditStaffTransferApproverLevel As Boolean = False
    Private mpreAllowToDeleteStaffTransferApproverLevel As Boolean = False

    Private mpreAllowToViewStaffTransferLevelRoleMapping As Boolean = False
    Private mpreAllowToAddStaffTransferLevelRoleMapping As Boolean = False
    Private mpreAllowToEditStaffTransferLevelRoleMapping As Boolean = False
    Private mpreAllowToDeleteStaffTransferLevelRoleMapping As Boolean = False

    Private mpreAllowToViewStaffTransferApprovalList As Boolean = False
    Private mpreAllowToApproveStaffTransferApproval As Boolean = False
    'Pinkal (26-Jun-2023) -- End

    'Hemant (15 Sep 2023) -- Start
    'ENHANCEMENT(TRA): A1X-1257 - Coaches/coachees nomination request and approval
    Private mpreAllowToViewCoachingApproverLevel As Boolean = False
    Private mpreAllowToAddCoachingApproverLevel As Boolean = False
    Private mpreAllowToEditCoachingApproverLevel As Boolean = False
    Private mpreAllowToDeleteCoachingApproverLevel As Boolean = False
    Private mpreAllowToFillCoachingForm As Boolean = False
    Private mpreAllowToApproveCoachingForm As Boolean = False
    Private mpreAllowToEditCoachingForm As Boolean = False
    Private mpreAllowToDeleteCoachingForm As Boolean = False
    Private mpreAllowToViewCoachingForm As Boolean = False
    Private mpreAllowToFillReplacementForm As Boolean = False
    'Hemant (15 Sep 2023) -- End

'Pinkal (30-Sep-2023) -- Start
    '(A1X-1371) Toyota - New desktop dashboard item: Total Work Anniversaries Within 7 Days from Now.
    Private mpreShowForcastedWorkAnniversary As Boolean = False
    Private mpreShowForcastedWorkResidencePermitExpiry As Boolean = False
    Private mpreShowTotalLeaversFromCurrentFY As Boolean = False
    'Pinkal (30-Sep-2023) -- End



#End Region

#Region " Properties "
    'CONFIGURE PROPERTY
    Public ReadOnly Property _AccessConfiguaration() As Boolean
        Get
            Return mpreAccessConfiguaration
        End Get
    End Property

    Public ReadOnly Property _ChangeLanguage() As Boolean
        Get
            Return mpreChangeLanguage
        End Get
    End Property

    Public ReadOnly Property _CompanyCreation() As Boolean
        Get
            Return mpreCompanyCreation
        End Get
    End Property


    'S.SANDEEP [ 23 NOV 2012 ] -- START
    'ENHANCEMENT : TRA CHANGES
    'Public ReadOnly Property _UserCreation() As Boolean
    '    Get
    '        Return mpreUserCreation
    '    End Get
    'End Property

    Public ReadOnly Property _AllowtoViewUserList() As Boolean
        Get
            Return mpreAllowtoViewUserList
        End Get
    End Property

    'Public ReadOnly Property _UserRole() As Boolean
    '    Get
    '        Return mpreUserRole
    '    End Get
    'End Property

    Public ReadOnly Property _AllowtoViewAbilityLevel() As Boolean
        Get
            Return mpreAllowtoViewAbilityLevel
        End Get
    End Property
    'S.SANDEEP [ 23 NOV 2012 ] -- END



    'S.SANDEEP [ 30 May 2011 ] -- START
    'ISSUE : FINCA REQ.
    Public ReadOnly Property _AllowAddUser() As Boolean
        Get
            Return mpreAddUser
        End Get
    End Property

    Public ReadOnly Property _AllowEditUser() As Boolean
        Get
            Return mpreEditUser
        End Get
    End Property

    Public ReadOnly Property _AllowDeleteUser() As Boolean
        Get
            Return mpreDeleteUser
        End Get
    End Property

    Public ReadOnly Property _AllowChangePassword() As Boolean
        Get
            Return mpreChangePassword
        End Get
    End Property

    Public ReadOnly Property _AssignReportPrivilege() As Boolean
        Get
            Return mpreAssignReportPrivilege
        End Get
    End Property

    Public ReadOnly Property _AllowAddUserRole() As Boolean
        Get
            Return mpreAddUserRole
        End Get
    End Property

    Public ReadOnly Property _AllowEditUserRole() As Boolean
        Get
            Return mpreEditUserRole
        End Get
    End Property

    Public ReadOnly Property _AllowDeleteUserRole() As Boolean
        Get
            Return mpreDeleteUserRole
        End Get
    End Property

    Public ReadOnly Property _AllowAddCompany() As Boolean
        Get
            Return mpreAddCompany
        End Get
    End Property

    Public ReadOnly Property _AllowEditCompany() As Boolean
        Get
            Return mpreEditCompany
        End Get
    End Property

    Public ReadOnly Property _AllowDeleteCompany() As Boolean
        Get
            Return mpreDeleteCompany
        End Get
    End Property
    'S.SANDEEP [ 30 May 2011 ] -- END 



    'GENERAL SETTINGS
    Public ReadOnly Property _DatabaseBackup() As Boolean
        Get
            Return mpreDatabaseBackup
        End Get
    End Property

    Public ReadOnly Property _DatabaseRestore() As Boolean
        Get
            Return mpreDatabaseRestore
        End Get
    End Property

    'HR
    Public ReadOnly Property _AddCommonMasters() As Boolean
        Get
            Return mpreAddCommonMasters
        End Get
    End Property

    Public ReadOnly Property _EditCommonMaster() As Boolean
        Get
            Return mpreEditCommonMaster
        End Get
    End Property

    Public ReadOnly Property _DeleteCommonMaster() As Boolean
        Get
            Return mpreDeleteCommonMaster
        End Get
    End Property

    Public ReadOnly Property _AddTerminationReason() As Boolean
        Get
            Return mpreAddTerminationReason
        End Get
    End Property

    Public ReadOnly Property _EditTerminationReason() As Boolean
        Get
            Return mpreEditTerminationReason
        End Get
    End Property

    Public ReadOnly Property _DeleteTerminationReason() As Boolean
        Get
            Return mpreDeleteTerminationReason
        End Get
    End Property

    Public ReadOnly Property _AddSkills() As Boolean
        Get
            Return mpreAddSkills
        End Get
    End Property

    Public ReadOnly Property _EditSkills() As Boolean
        Get
            Return mpreEditSkills
        End Get
    End Property

    Public ReadOnly Property _DeleteSkills() As Boolean
        Get
            Return mpreDeleteSkills
        End Get
    End Property

    Public ReadOnly Property _AddAdvertise() As Boolean
        Get
            Return mpreAddAdvertise
        End Get
    End Property

    Public ReadOnly Property _EditAdvertise() As Boolean
        Get
            Return mpreEditAdvertise
        End Get
    End Property

    Public ReadOnly Property _DeleteAdvertise() As Boolean
        Get
            Return mpreDeleteAdvertise
        End Get
    End Property

    Public ReadOnly Property _AddQualification_Course() As Boolean
        Get
            Return mpreAddQualification_Course
        End Get
    End Property

    Public ReadOnly Property _EditQualification_Course() As Boolean
        Get
            Return mpreEditQualification_Course
        End Get
    End Property

    Public ReadOnly Property _DeleteQualification_Course() As Boolean
        Get
            Return mpreDeleteQualification_Course
        End Get
    End Property

    Public ReadOnly Property _AddResultCode() As Boolean
        Get
            Return mpreAddResultCode
        End Get
    End Property

    Public ReadOnly Property _EditResultCode() As Boolean
        Get
            Return mpreEditResultCode
        End Get
    End Property

    Public ReadOnly Property _DeleteResultCode() As Boolean
        Get
            Return mpreDeleteResultCode
        End Get
    End Property

    Public ReadOnly Property _AddMembership() As Boolean
        Get
            Return mpreAddMembership
        End Get
    End Property

    Public ReadOnly Property _EditMembership() As Boolean
        Get
            Return mpreEditMembership
        End Get
    End Property

    Public ReadOnly Property _DeleteMembership() As Boolean
        Get
            Return mpreDeleteMembership
        End Get
    End Property

    Public ReadOnly Property _AddBenefitPlan() As Boolean
        Get
            Return mpreAddBenefitPlan
        End Get
    End Property

    Public ReadOnly Property _EditBenefitPlan() As Boolean
        Get
            Return mpreEditBenefitPlan
        End Get
    End Property

    Public ReadOnly Property _DeleteBenefitPlan() As Boolean
        Get
            Return mpreDeleteBenefitPlan
        End Get
    End Property

    'Sohail (10 Jan 2020) -- Start
    'NMB Enhancement # : New screen for Country list and allow to edit currency sign on country list.
    Private mpreAllowToEditCurrencySign As Boolean = False
    Public ReadOnly Property _AllowToEditCurrencySign() As Boolean
        Get
            Return mpreAllowToEditCurrencySign
        End Get
    End Property
    'Sohail (10 Jan 2020) -- End

    Public ReadOnly Property _AddState() As Boolean
        Get
            Return mpreAddState
        End Get
    End Property

    Public ReadOnly Property _EditState() As Boolean
        Get
            Return mpreEditState
        End Get
    End Property

    Public ReadOnly Property _DeleteState() As Boolean
        Get
            Return mpreDeleteState
        End Get
    End Property

    Public ReadOnly Property _AddCity() As Boolean
        Get
            Return mpreAddCity
        End Get
    End Property

    Public ReadOnly Property _EditCity() As Boolean
        Get
            Return mpreEditCity
        End Get
    End Property

    Public ReadOnly Property _DeleteCity() As Boolean
        Get
            Return mpreDeleteCity
        End Get
    End Property

    Public ReadOnly Property _AddZipCode() As Boolean
        Get
            Return mpreAddZipCode
        End Get
    End Property

    Public ReadOnly Property _EditZipCode() As Boolean
        Get
            Return mpreEditZipCode
        End Get
    End Property

    Public ReadOnly Property _DeleteZipCode() As Boolean
        Get
            Return mpreDeleteZipCode
        End Get
    End Property

    Public ReadOnly Property _AddEmployee() As Boolean
        Get
            Return mpreAddEmployee
        End Get
    End Property

    Public ReadOnly Property _EditEmployee() As Boolean
        Get
            Return mpreEditEmployee
        End Get
    End Property

    Public ReadOnly Property _DeleteEmployee() As Boolean
        Get
            Return mpreDeleteEmployee
        End Get
    End Property

    Public ReadOnly Property _AddDependant() As Boolean
        Get
            Return mpreAddDependant
        End Get
    End Property

    Public ReadOnly Property _EditDependant() As Boolean
        Get
            Return mpreEditDependant
        End Get
    End Property

    Public ReadOnly Property _DeleteDependant() As Boolean
        Get
            Return mpreDeleteDependant
        End Get
    End Property

    Public ReadOnly Property _AddBeneficiaries() As Boolean
        Get
            Return mpreAddBeneficiaries
        End Get
    End Property

    Public ReadOnly Property _EditBeneficiaries() As Boolean
        Get
            Return mpreEditBeneficiaries
        End Get
    End Property

    Public ReadOnly Property _DeleteBeneficiaries() As Boolean
        Get
            Return mpreDeleteBeneficiaries
        End Get
    End Property

    Public ReadOnly Property _AddBenefitAllocation() As Boolean
        Get
            Return mpreAddBenefitAllocation
        End Get
    End Property

    Public ReadOnly Property _EditBenefitAllocation() As Boolean
        Get
            Return mpreEditBenefitAllocation
        End Get
    End Property

    Public ReadOnly Property _DeleteBenefitAllocation() As Boolean
        Get
            Return mpreDeleteBenefitAllocation
        End Get
    End Property

    'Sohail (17 Sep 2019) -- Start
    'NMB Enhancement # TC006: system should be able to bind earning/deduction (benefit) with specific allocation and automatically assign to employee as per his/her allocation.
    Private mpreAllowToSetBenefitAllocationActive As Boolean = False
    Public ReadOnly Property _AllowToSetBenefitAllocationActive() As Boolean
        Get
            Return mpreAllowToSetBenefitAllocationActive
        End Get
    End Property

    Private mpreAllowToSetBenefitAllocationInactive As Boolean
    Public ReadOnly Property _AllowToSetBenefitAllocationInactive() As Boolean
        Get
            Return mpreAllowToSetBenefitAllocationInactive
        End Get
    End Property
    'Sohail (17 Sep 2019) -- End

    'Sohail (09 Oct 2019) -- Start
    'NMB Enhancement # : privilege  Allow to change date on Closed Period on user master to change eoc / termination date in paid period or closed period.
    Private mpreAllowToChangeEOCLeavingDateOnClosedPeriod As Boolean
    Public ReadOnly Property _AllowToChangeEOCLeavingDateOnClosedPeriod() As Boolean
        Get
            Return mpreAllowToChangeEOCLeavingDateOnClosedPeriod
        End Get
    End Property
    'Sohail (09 Oct 2019) -- End

    'Sohail (16 Oct 2019) -- Start
    'NMB Enhancement # : Add privileges for "Allow To Post Flexcube JV To Oracle" to prevent erroneous postings by other users to Payroll GL accounts.
    Private mpreAllowToPostFlexcubeJVToOracle As Boolean
    Public ReadOnly Property _AllowToPostFlexcubeJVToOracle() As Boolean
        Get
            Return mpreAllowToPostFlexcubeJVToOracle
        End Get
    End Property
    'Sohail (16 Oct 2019) -- End

    'Sohail (16 Oct 2019) -- Start
    'NMB Enhancement # : Privileges "Allow to view Paid Amount" to hide Amount on Approval/Authorize Screen for Automatic Integration between Payroll and Finance modules,Online Approval workflow for Payroll runs.
    Private mpreAllowToViewPaidAmount As Boolean
    Public ReadOnly Property _AllowToViewPaidAmount() As Boolean
        Get
            Return mpreAllowToViewPaidAmount
        End Get
    End Property
    'Sohail (16 Oct 2019) -- End

    'Sohail (18 Oct 2019) -- Start
    'NMB Enhancement # : For Add/New/Edit employee system should allow to set appointment date to last closed period for It should be possible to set appointment date of an employee to past closed period by use of the first appointment date but during payroll process, consider 1st of the following month
    Private mpreAllowToChangeAppointmentDateOnClosedPeriod As Boolean
    Public ReadOnly Property _AllowToChangeAppointmentDateOnClosedPeriod() As Boolean
        Get
            Return mpreAllowToChangeAppointmentDateOnClosedPeriod
        End Get
    End Property
    'Sohail (18 Oct 2019) -- End

    'Sohail (21 Oct 2019) -- Start
    'NMB Enhancement # : New screen employee exemption will be added on employee movement with end date as optional with approval flow for It should be possible to update active employee status as Unpaid and automatically prorate payment of that employee effectively from the date selected. Also when employee is back, user should be able to update end date of the Unpaid status and automatically include that employee in payroll and prorate based on the date set
    Private mpreAllowToApproveRejectEmployeeExemption As Boolean
    Public ReadOnly Property _AllowToApproveRejectEmployeeExemption() As Boolean
        Get
            Return mpreAllowToApproveRejectEmployeeExemption
        End Get
    End Property

    Private mpreAllowToSetEmployeeExemptionDate As Boolean
    Public ReadOnly Property _AllowToSetEmployeeExemptionDate() As Boolean
        Get
            Return mpreAllowToSetEmployeeExemptionDate
        End Get
    End Property

    Private mpreAllowToEditEmployeeExemptionDate As Boolean
    Public ReadOnly Property _AllowToEditEmployeeExemptionDate() As Boolean
        Get
            Return mpreAllowToEditEmployeeExemptionDate
        End Get
    End Property

    Private mpreAllowToDeleteEmployeeExemptionDate As Boolean
    Public ReadOnly Property _AllowToDeleteEmployeeExemptionDate() As Boolean
        Get
            Return mpreAllowToDeleteEmployeeExemptionDate
        End Get
    End Property
    'Sohail (21 Oct 2019) -- End

    'Sohail (14 Nov 2019) -- Start
    'NMB UAT Enhancement # : New Screen Training Need Form.
    Private mpreAllowToViewTrainingNeedForm As Boolean
    Public ReadOnly Property _AllowToViewTrainingNeedForm() As Boolean
        Get
            Return mpreAllowToViewTrainingNeedForm
        End Get
    End Property

    Private mpreAllowToAddTrainingNeedForm As Boolean
    Public ReadOnly Property _AllowToAddTrainingNeedForm() As Boolean
        Get
            Return mpreAllowToAddTrainingNeedForm
        End Get
    End Property

    Private mpreAllowToEditTrainingNeedForm As Boolean
    Public ReadOnly Property _AllowToEditTrainingNeedForm() As Boolean
        Get
            Return mpreAllowToEditTrainingNeedForm
        End Get
    End Property

    Private mpreAllowToDeleteTrainingNeedForm As Boolean
    Public ReadOnly Property _AllowToDeleteTrainingNeedForm() As Boolean
        Get
            Return mpreAllowToDeleteTrainingNeedForm
        End Get
    End Property
    'Sohail (14 Nov 2019) -- End

    'Anjan (09 Jan 2018) - Start
    'Issue: Not in use
    'Public ReadOnly Property _AllowAssignGroupBenefit() As Boolean
    '    Get
    '        Return mpreAllowAssignGroupBenefit
    '    End Get
    'End Property
    'Anjan (09 Jan 2018) - End

    Public ReadOnly Property _AddEmployeeBenefit() As Boolean
        Get
            Return mpreAddEmployeeBenefit
        End Get
    End Property

    Public ReadOnly Property _EditEmployeeBenefit() As Boolean
        Get
            Return mpreEditEmployeeBenefit
        End Get
    End Property

    Public ReadOnly Property _DeleteEmployeeBenefit() As Boolean
        Get
            Return mpreDeleteEmployeeBenefit
        End Get
    End Property

    Public ReadOnly Property _AddEmployeeReferee() As Boolean
        Get
            Return mpreAddEmployeeReferee
        End Get
    End Property

    Public ReadOnly Property _EditEmployeeReferee() As Boolean
        Get
            Return mpreEditEmployeeReferess
        End Get
    End Property

    Public ReadOnly Property _DeleteEmployeeReferee() As Boolean
        Get
            Return mpreDeleteEmployeeReferess
        End Get
    End Property

    Public ReadOnly Property _AddEmployeeReferences() As Boolean
        Get
            Return mpreAddEmployeeReferences
        End Get
    End Property

    Public ReadOnly Property _EditEmployeeReferences() As Boolean
        Get
            Return mpreEditEmployeeReferences
        End Get
    End Property

    Public ReadOnly Property _DeleteEmployeeReferences() As Boolean
        Get
            Return mpreDeleteEmployeeReferences
        End Get
    End Property

    Public ReadOnly Property _AddEmployeeAssets() As Boolean
        Get
            Return mpreAddEmployeeAssets
        End Get
    End Property

    Public ReadOnly Property _EditEmployeeAssets() As Boolean
        Get
            Return mpreEditEmployeeAssets
        End Get
    End Property

    Public ReadOnly Property _DeleteEmployeeAssets() As Boolean
        Get
            Return mpreDeleteEmployeeAssets
        End Get
    End Property

    Public ReadOnly Property _AddEmployeeSkill() As Boolean
        Get
            Return mpreAddEmployeeSkill
        End Get
    End Property

    Public ReadOnly Property _EditEmployeeSkill() As Boolean
        Get
            Return mpreEditEmployeeSkill
        End Get
    End Property

    Public ReadOnly Property _DeleteEmployeeSkill() As Boolean
        Get
            Return mpreDeleteEmployeeSkill
        End Get
    End Property

    Public ReadOnly Property _AddEmployeeQualification() As Boolean
        Get
            Return mpreAddEmployeeQualification
        End Get
    End Property

    Public ReadOnly Property _EditEmployeeQualification() As Boolean
        Get
            Return mpreEditEmployeeQualification
        End Get
    End Property

    Public ReadOnly Property _DeleteEmployeeQualification() As Boolean
        Get
            Return mpreDeleteEmployeeQualification
        End Get
    End Property

    Public ReadOnly Property _AddEmployeeExperience() As Boolean
        Get
            Return mpreAddEmployeeExperience
        End Get
    End Property

    Public ReadOnly Property _EditEmployeeExperience() As Boolean
        Get
            Return mpreEditEmployeeExperience
        End Get
    End Property

    Public ReadOnly Property _DeleteEmployeeExperience() As Boolean
        Get
            Return mpreDeleteEmployeeExperience
        End Get
    End Property

    Public ReadOnly Property _AddEmployeeDiscipline() As Boolean
        Get
            Return mpreAddEmployeeDiscipline
        End Get
    End Property

    Public ReadOnly Property _EditEmployeeDiscipline() As Boolean
        Get
            Return mpreEditEmployeeDiscipline
        End Get
    End Property

    Public ReadOnly Property _DeleteEmployeeDiscipline() As Boolean
        Get
            Return mpreDeleteEmployeeDiscipline
        End Get
    End Property

    Public ReadOnly Property _AddDisciplinaryAction() As Boolean
        Get
            Return mpreAddDisciplinaryAction
        End Get
    End Property

    Public ReadOnly Property _EditDisciplinaryAction() As Boolean
        Get
            Return mpreEditDisciplinaryAction
        End Get
    End Property

    Public ReadOnly Property _DeleteDisciplinaryAction() As Boolean
        Get
            Return mpreDeleteDisciplinaryAction
        End Get
    End Property

    Public ReadOnly Property _AddDisciplineOffence() As Boolean
        Get
            Return mpreAddDisciplineOffence
        End Get
    End Property

    Public ReadOnly Property _EditDisciplineOffence() As Boolean
        Get
            Return mpreEditDisciplineOffence
        End Get
    End Property

    Public ReadOnly Property _DeleteDisciplineOffence() As Boolean
        Get
            Return mpreDeleteDisciplineOffence
        End Get
    End Property


    'S.SANDEEP [28 MAY 2015] -- START
    'ENHANCEMENT : NEW PRIVILEGE (Performance Assessment)
    'Public ReadOnly Property _AddAssessmentGroup() As Boolean
    '    Get
    '        Return mpreAddAssessmentGroup
    '    End Get
    'End Property

    'Public ReadOnly Property _EditAssessmentGroup() As Boolean
    '    Get
    '        Return mpreEditAssessmentGroup
    '    End Get
    'End Property

    'Public ReadOnly Property _DeleteAssessmentGroup() As Boolean
    '    Get
    '        Return mpreDeleteAssessmentGroup
    '    End Get
    'End Property

    'Public ReadOnly Property _AddAssessmentItem() As Boolean
    '    Get
    '        Return mpreAddAssessmentItem
    '    End Get
    'End Property

    'Public ReadOnly Property _EditAssessmentItem() As Boolean
    '    Get
    '        Return mpreEditAssessmentItem
    '    End Get
    'End Property

    'Public ReadOnly Property _DeleteAssessmentItem() As Boolean
    '    Get
    '        Return mpreDeleteAssessmentItem
    '    End Get
    'End Property

    'Public ReadOnly Property _AddAssessmentAnalysis() As Boolean
    '    Get
    '        Return mpreAddAssessmentAnalysis
    '    End Get
    'End Property

    'Public ReadOnly Property _EditAssessmentAnalysis() As Boolean
    '    Get
    '        Return mpreEditAssessmentAnalysis
    '    End Get
    'End Property

    'Public ReadOnly Property _DeleteAssessmentAnalysis() As Boolean
    '    Get
    '        Return mpreDeleteAssessmentAnalysis
    '    End Get
    'End Property
    'S.SANDEEP [28 MAY 2015] -- END


    Public ReadOnly Property _AddMedicalMasters() As Boolean
        Get
            Return mpreAddMedicalMasters
        End Get
    End Property

    Public ReadOnly Property _EditMedicalMasters() As Boolean
        Get
            Return mpreEditMedicalMasters
        End Get
    End Property

    Public ReadOnly Property _DeleteMedicalMasters() As Boolean
        Get
            Return mpreDeleteMedicalMasters
        End Get
    End Property

    Public ReadOnly Property _AddMedicalInjuries() As Boolean
        Get
            Return mpreAddMedicalInjuries
        End Get
    End Property

    Public ReadOnly Property _EditMedicalInjuries() As Boolean
        Get
            Return mpreEditMedicalInjuries
        End Get
    End Property

    Public ReadOnly Property _DeleteMedicalInjuries() As Boolean
        Get
            Return mpreDeleteMedicalInjuries
        End Get
    End Property

    Public ReadOnly Property _AddMedicalInstitutes() As Boolean
        Get
            Return mpreAddMedicalInstitutes
        End Get
    End Property

    Public ReadOnly Property _EditMedicalInstitutes() As Boolean
        Get
            Return mpreEditMedicalInstitutes
        End Get
    End Property

    Public ReadOnly Property _DeleteMedicalInstitutes() As Boolean
        Get
            Return mpreDeleteMedicalInstitutes
        End Get
    End Property

    Public ReadOnly Property _AddAssignMedicalCategory() As Boolean
        Get
            Return mpreAddAssignMedicalCategory
        End Get
    End Property

    Public ReadOnly Property _EditAssignMedicalCategory() As Boolean
        Get
            Return mpreEditAssignMedicalCategory
        End Get
    End Property

    Public ReadOnly Property _DeleteAssignMedicalCategory() As Boolean
        Get
            Return mpreDeleteAssignMedicalCategory
        End Get
    End Property

    Public ReadOnly Property _AddMedicalCover() As Boolean
        Get
            Return mpreAddMedicalCover
        End Get
    End Property

    Public ReadOnly Property _EditMedicalCover() As Boolean
        Get
            Return mpreEditMedicalCover
        End Get
    End Property

    Public ReadOnly Property _DeleteMedicalCover() As Boolean
        Get
            Return mpreDeleteMedicalCover
        End Get
    End Property

    Public ReadOnly Property _AddMedicalClaim() As Boolean
        Get
            Return mpreAddMedicalClaim
        End Get
    End Property

    Public ReadOnly Property _EditMedicalClaim() As Boolean
        Get
            Return mpreEditMedicalClaim
        End Get
    End Property

    Public ReadOnly Property _DeleteMedicalClaim() As Boolean
        Get
            Return mpreDeleteMedicalClaim
        End Get
    End Property

    'S.SANDEEP [ 13 JUNE 2011 ] -- START
    'ISSUE : EMPLOYEE & LOAN PRIVILEGE


    'S.SANDEEP [ 29 DEC 2011 ] -- START
    'ENHANCEMENT : TRA CHANGES 
    'TYPE : EMPLOYEMENT CONTRACT PROCESS
    'Public ReadOnly Property _AllowMakeEmployeeActive() As Boolean
    '    Get
    '        Return mpreAllowtoMarkEmployeeActive
    '    End Get
    'End Property
    Public ReadOnly Property _AllowToApproveEmployee() As Boolean
        Get
            Return mpreAllowtoApproveEmployee
        End Get
    End Property

    Public ReadOnly Property _AllowTo_View_Scale() As Boolean
        Get
            Return mpreAllowtoViewScale
        End Get
    End Property

    Public ReadOnly Property _AllowtoChangeConfirmationDate() As Boolean
        Get
            Return mpreAllowtoChangeConfirmationDate
        End Get
    End Property

    Public ReadOnly Property _AllowtoChangeAppointmentDate() As Boolean
        Get
            Return mpreAllowtoChangeAppointmentDate
        End Get
    End Property

    Public ReadOnly Property _AllowtoSetEmployeeBirthDate() As Boolean
        Get
            Return mpreAllowtoSetBirthDate
        End Get
    End Property

    Public ReadOnly Property _AllowtoSetEmpSuspensionDate() As Boolean
        Get
            Return mpreAllowtoSetSuspensionDate
        End Get
    End Property

    Public ReadOnly Property _AllowtoSetEmpProbationDate() As Boolean
        Get
            Return mpreAllowtoSetProbationDate
        End Get
    End Property

    Public ReadOnly Property _AllowtoSetEmploymentEndDate() As Boolean
        Get
            Return mpreAllowtoSetEmploymentEndDate
        End Get
    End Property

    Public ReadOnly Property _AllowtoSetLeavingDate() As Boolean
        Get
            Return mpreAllowtoSetLeavingDate
        End Get
    End Property

    Public ReadOnly Property _AllowtoChangeRetirementDate() As Boolean
        Get
            Return mpreAllowtoChangeRetirementDate
        End Get
    End Property

    Public ReadOnly Property _AllowtoMarkEmployeeClear() As Boolean
        Get
            Return mpreAllowtoMarkEmployeeClear
        End Get
    End Property
    'S.SANDEEP [ 29 DEC 2011 ] -- END
    'S.SANDEEP [ 13 JUNE 2011 ] -- END


    'Anjan (26 Oct 2012)-Start
    'ENHANCEMENT : TRA COMMENTS on Andrew's Request
    Public ReadOnly Property _AllowToScanAttachDocument() As Boolean
        Get
            Return mpreAllowtoScanAttachDocument
        End Get
    End Property
    Public ReadOnly Property _AllowToExportMembership() As Boolean
        Get
            Return mpreAllowtoExportMembership
        End Get
    End Property
    Public ReadOnly Property _AllowToImporttMembership() As Boolean
        Get
            Return mpreAllowtoImportMembership
        End Get
    End Property
    Public ReadOnly Property _AllowToSetReportingTo() As Boolean
        Get
            Return mpreAllowtoSetReportingTo
        End Get
    End Property
    Public ReadOnly Property _AllowToViewDiary() As Boolean
        Get
            Return mpreAllowtoViewDiary
        End Get
    End Property

    Public ReadOnly Property _AllowToSetAllocationMapping() As Boolean
        Get
            Return mpreAllowtoSetAllocationMapping
        End Get
    End Property
    Public ReadOnly Property _AllowToDeleteAllocationMapping() As Boolean
        Get
            Return mpreAllowtoDeleteAllocationMapping
        End Get
    End Property

    Public ReadOnly Property _AllowToImportDependants() As Boolean
        Get
            Return mpreAllowtoImportDependants
        End Get
    End Property
    Public ReadOnly Property _AllowToExportDependants() As Boolean
        Get
            Return mpreAllowtoExportDependants
        End Get
    End Property

    Public ReadOnly Property _AllowToViewMembershipMasterList() As Boolean
        Get
            Return mpreAllowToViewMembershipMasterList
        End Get
    End Property

    Public ReadOnly Property _AllowtoImportEmpReferee() As Boolean
        Get
            Return mpreAllowtoImportEmpReferee
        End Get
    End Property
    Public ReadOnly Property _AllowtoExportEmpReferee() As Boolean
        Get
            Return mpreAllowtoExportEmpReferee
        End Get
    End Property
    Public ReadOnly Property _AllowtoImportEmpSkills() As Boolean
        Get
            Return mpreAllowtoImportEmpSkills
        End Get
    End Property
    Public ReadOnly Property _AllowtoExportEmpSkills() As Boolean
        Get
            Return mpreAllowtoExportEmpSkills
        End Get
    End Property
    Public ReadOnly Property _AllowtoImportEmpQualification() As Boolean
        Get
            Return mpreAllowtoImportEmpQualification
        End Get
    End Property
    Public ReadOnly Property _AllowtoExportEmpQualification() As Boolean
        Get
            Return mpreAllowtoExportEmpQualification
        End Get
    End Property
    Public ReadOnly Property _AllowtoExportOtherQualification() As Boolean
        Get
            Return mpreAllowtoExportOtherQualification
        End Get
    End Property
    Public ReadOnly Property _AllowtoImportEmpExperience() As Boolean
        Get
            Return mpreAllowtoImportEmpExperience
        End Get
    End Property
    Public ReadOnly Property _AllowtoExportEmpExperience() As Boolean
        Get
            Return mpreAllowtoExportEmpExperience
        End Get
    End Property

    Public ReadOnly Property _AllowtoMapVacancy() As Boolean
        Get
            Return mpreAllowtoMapVacancy
        End Get
    End Property

    Public ReadOnly Property _AllowtoViewInterviewAnalysisList() As Boolean
        Get
            Return mpreAllowtoViewInterviewAnalysisList
        End Get
    End Property

    Public ReadOnly Property _AllowtoImportAccounts() As Boolean
        Get
            Return mpreAllowtoImportAccounts
        End Get
    End Property

    Public ReadOnly Property _AllowtoExportAccounts() As Boolean
        Get
            Return mpreAllowtoExportAccounts
        End Get
    End Property
    Public ReadOnly Property _AllowtoExportEmpBanks() As Boolean
        Get
            Return mpreAllowtoExportEmpBanks
        End Get
    End Property
    Public ReadOnly Property _AllowtoImportEmpBanks() As Boolean
        Get
            Return mpreAllowtoImportEmpBanks
        End Get
    End Property

    Public ReadOnly Property _AllowtoSetTranHeadActive() As Boolean
        Get
            Return mpreAllowtoSetTranHeadActive
        End Get
    End Property

    Public ReadOnly Property _AllowtoMapLeaveType() As Boolean
        Get
            Return mpreAllowtoMapLeaveType
        End Get
    End Property

    Public ReadOnly Property _AllowtoViewLetterTemplate() As Boolean
        Get
            Return mrpeAllowtoViewLetterTemplate
        End Get
    End Property


    'Anjan (26 Oct 2012)-End 


    'PAYROLL
    Public ReadOnly Property _AddPayrollGroup() As Boolean
        Get
            Return mpreAddPayrollGroup
        End Get
    End Property

    Public ReadOnly Property _EditPayrollGroup() As Boolean
        Get
            Return mpreEditPayrollGroup
        End Get
    End Property

    Public ReadOnly Property _DeletePayrollGroup() As Boolean
        Get
            Return mpreDeletePayrollGroup
        End Get
    End Property

    Public ReadOnly Property _AddCostCenter() As Boolean
        Get
            Return mpreAddCostCenter
        End Get
    End Property

    Public ReadOnly Property _EditCostCenter() As Boolean
        Get
            Return mpreEditCostCenter
        End Get
    End Property

    Public ReadOnly Property _DeleteCostCenter() As Boolean
        Get
            Return mpreDeleteCostCenter
        End Get
    End Property

    Public ReadOnly Property _AddPayPoint() As Boolean
        Get
            Return mpreAddPayPoint
        End Get
    End Property

    Public ReadOnly Property _EditPayPoint() As Boolean
        Get
            Return mpreEditPayPoint
        End Get
    End Property

    Public ReadOnly Property _DeletePayPoint() As Boolean
        Get
            Return mpreDeletePayPoint
        End Get
    End Property

    Public ReadOnly Property _AddPeriods() As Boolean
        Get
            Return mpreAddPeriods
        End Get
    End Property

    Public ReadOnly Property _EditPeriods() As Boolean
        Get
            Return mpreEditPeriods
        End Get
    End Property

    Public ReadOnly Property _DeletePeriods() As Boolean
        Get
            Return mpreDeletePeriods
        End Get
    End Property

    Public ReadOnly Property _AddVendor() As Boolean
        Get
            Return mpreAddVendor
        End Get
    End Property

    Public ReadOnly Property _EditVendor() As Boolean
        Get
            Return mpreEditVendor
        End Get
    End Property

    Public ReadOnly Property _DeleteVendor() As Boolean
        Get
            Return mpreDeleteVendor
        End Get
    End Property

    Public ReadOnly Property _AddBankBranch() As Boolean
        Get
            Return mpreAddBankBranch
        End Get
    End Property

    Public ReadOnly Property _EditBankBranch() As Boolean
        Get
            Return mpreEditBankBranch
        End Get
    End Property

    Public ReadOnly Property _DeleteBankBranch() As Boolean
        Get
            Return mpreDeleteBankBranch
        End Get
    End Property

    Public ReadOnly Property _AddBankAccType() As Boolean
        Get
            Return mpreAddBankAccType
        End Get
    End Property

    Public ReadOnly Property _EditBankAccType() As Boolean
        Get
            Return mpreEditBankAccType
        End Get
    End Property

    Public ReadOnly Property _DeleteBankAccType() As Boolean
        Get
            Return mpreDeleteBankAccType
        End Get
    End Property

    Public ReadOnly Property _AddBankEDI() As Boolean
        Get
            Return mpreAddBankEDI
        End Get
    End Property

    Public ReadOnly Property _EditBankEDI() As Boolean
        Get
            Return mpreEditBankEDI
        End Get
    End Property

    Public ReadOnly Property _DeleteBankEDI() As Boolean
        Get
            Return mpreDeleteBankEDI
        End Get
    End Property

    Public ReadOnly Property _AddCurrency() As Boolean
        Get
            Return mpreAddCurrency
        End Get
    End Property

    Public ReadOnly Property _EditCurrency() As Boolean
        Get
            Return mpreEditCurrency
        End Get
    End Property

    Public ReadOnly Property _DeleteCurrency() As Boolean
        Get
            Return mpreDeleteCurrency
        End Get
    End Property

    Public ReadOnly Property _AddDenomination() As Boolean
        Get
            Return mpreAddDenomination
        End Get
    End Property

    Public ReadOnly Property _EditDenomination() As Boolean
        Get
            Return mpreEditDenomination
        End Get
    End Property

    Public ReadOnly Property _DeleteDenomination() As Boolean
        Get
            Return mpreDeleteDenomination
        End Get
    End Property

    Public ReadOnly Property _AddEmployeeCostCenter() As Boolean
        Get
            Return mpreAddEmployeeCostCenter
        End Get
    End Property

    Public ReadOnly Property _EditEmployeeCostCenter() As Boolean
        Get
            Return mpreEditEmployeeCostCenter
        End Get
    End Property

    Public ReadOnly Property _DeleteEmployeeCostCenter() As Boolean
        Get
            Return mpreDeleteEmployeeCostCenter
        End Get
    End Property

    Public ReadOnly Property _AddEmployeeBanks() As Boolean
        Get
            Return mpreAddEmployeeBanks
        End Get
    End Property

    Public ReadOnly Property _EditEmployeeBanks() As Boolean
        Get
            Return mpreEditEmployeeBanks
        End Get
    End Property

    Public ReadOnly Property _DeleteEmployeeBanks() As Boolean
        Get
            Return mpreDeleteEmployeeBanks
        End Get
    End Property

    Public ReadOnly Property _AddBatchTransaction() As Boolean
        Get
            Return mpreAddBatchTransaction
        End Get
    End Property

    Public ReadOnly Property _EditBatchTransaction() As Boolean
        Get
            Return mpreEditBatchTransaction
        End Get
    End Property

    Public ReadOnly Property _DeleteBatchTransaction() As Boolean
        Get
            Return mpreDeleteBatchTransaction
        End Get
    End Property

    Public ReadOnly Property _AddTransactionHead() As Boolean
        Get
            Return mpreAddTransactionHead
        End Get
    End Property

    Public ReadOnly Property _EditTransactionHead() As Boolean
        Get
            Return mpreEditTransactionHead
        End Get
    End Property

    Public ReadOnly Property _DeleteTransactionHead() As Boolean
        Get
            Return mpreDeleteTransactionHead
        End Get
    End Property

    Public ReadOnly Property _AddEarningDeduction() As Boolean
        Get
            Return mpreAddEarningDeduction
        End Get
    End Property

    Public ReadOnly Property _EditEarningDeduction() As Boolean
        Get
            Return mpreEditEarningDeduction
        End Get
    End Property

    Public ReadOnly Property _DeleteEarningDeduction() As Boolean
        Get
            Return mpreDeleteEarningDeduction
        End Get
    End Property

    Public ReadOnly Property _AllowBatchTransactionOnED() As Boolean
        Get
            Return mpreAllowBatchTransactionOnED
        End Get
    End Property

    Public ReadOnly Property _AllowPerformGlobalAssignOnED() As Boolean
        Get
            Return mpreAllowPerformGlobalAssignOnED
        End Get
    End Property

    Public ReadOnly Property _AllowPerformEmployeeExemptionOnED() As Boolean
        Get
            Return mpreAllowPerformEmployeeExemptionOnED
        End Get
    End Property

    Public ReadOnly Property _AddEmployeeExemption() As Boolean
        Get
            Return mpreAddEmployeeExemption
        End Get
    End Property

    Public ReadOnly Property _EditEmployeeExemption() As Boolean
        Get
            Return mpreEditEmployeeExemption
        End Get
    End Property

    Public ReadOnly Property _DeleteEmployeeExemption() As Boolean
        Get
            Return mpreDeleteEmployeeExemption
        End Get
    End Property

    Public ReadOnly Property _AllowProcessPayroll() As Boolean
        Get
            Return mpreAllowProcessPayroll
        End Get
    End Property

    Public ReadOnly Property _AllowViewPayslip() As Boolean
        Get
            Return mpreAllowViewPayslip
        End Get
    End Property

    Public ReadOnly Property _AllowPrintPayslip() As Boolean
        Get
            Return mpreAllowPrintPayslip
        End Get
    End Property

    Public ReadOnly Property _AddPayment() As Boolean
        Get
            Return mpreAddPayment
        End Get
    End Property

    Public ReadOnly Property _EditPayment() As Boolean
        Get
            Return mpreEditPayment
        End Get
    End Property

    Public ReadOnly Property _DeletePayment() As Boolean
        Get
            Return mpreDeletePayment
        End Get
    End Property

    Public ReadOnly Property _AllowClosePeriod() As Boolean
        Get
            Return mpreAllowClosePeriod
        End Get
    End Property

    Public ReadOnly Property _AllowCloseYear() As Boolean
        Get
            Return mpreAllowCloseYear
        End Get
    End Property

    Public ReadOnly Property _AllowPrepareBudget() As Boolean
        Get
            Return mpreAllowPrepareBudget
        End Get
    End Property

    Public ReadOnly Property _AddSalaryIncrement() As Boolean
        Get
            Return mpreAddSalaryIncrement
        End Get
    End Property

    Public ReadOnly Property _EditSalaryIncrement() As Boolean
        Get
            Return mpreEditSalaryIncrement
        End Get
    End Property

    Public ReadOnly Property _DeleteSalaryIncrement() As Boolean
        Get
            Return mpreDeleteSalaryIncrement
        End Get
    End Property

    Public ReadOnly Property _AllowDefineWageTable() As Boolean
        Get
            Return mpreAllowDefineWageTable
        End Get
    End Property

    'Sohail (14 Jun 2011) -- Start
    Public ReadOnly Property _AllowToApproveEarningDeduction() As Boolean
        Get
            Return mpreAllowToApproveEarningDeduction
        End Get
    End Property
    'Sohail (14 Jun 2011) -- End

    'Sohail (26 Nov 2011) -- Start
    Public ReadOnly Property _AllowToApproveEmpExemtion() As Boolean
        Get
            Return mpreAllowToApproveEmpExemption
        End Get
    End Property
    'Sohail (26 Nov 2011) -- End

    'Sohail (22 May 2012) -- Start
    'TRA - ENHANCEMENT
    Public ReadOnly Property _AllowVoidPayroll() As Boolean
        Get
            Return mpreAllowVoidPayroll
        End Get
    End Property
    'Sohail (22 May 2012) -- End

    'Sohail (02 Jul 2012) -- Start
    'TRA - ENHANCEMENT
    Public ReadOnly Property _AllowAuthorizePayslipPayment() As Boolean
        Get
            Return mpreAllowAuthorizePayslipPayment
        End Get
    End Property

    Public ReadOnly Property _AllowVoidAuthorizedPayslipPayment() As Boolean
        Get
            Return mpreAllowVoidAuthorizedPayslipPayment
        End Get
    End Property
    'Sohail (02 Jul 2012) -- End

    'Sohail (24 Sep 2012) -- Start
    'TRA - ENHANCEMENT
    Public ReadOnly Property _AllowToApproveSalaryChange() As Boolean
        Get
            Return mpreAllowToApproveSalaryChange
        End Get
    End Property
    'Sohail (24 Sep 2012) -- End

    'Sohail (05 Dec 2012) -- Start
    'TRA - ENHANCEMENT
    Public ReadOnly Property _AllowToAddBatchPosting() As Boolean
        Get
            Return mpreAllowToAddBatchPosting
        End Get
    End Property

    Public ReadOnly Property _AllowToEditBatchPosting() As Boolean
        Get
            Return mpreAllowToEditBatchPosting
        End Get
    End Property

    Public ReadOnly Property _AllowToDeleteBatchPosting() As Boolean
        Get
            Return mpreAllowToDeleteBatchPosting
        End Get
    End Property

    Public ReadOnly Property _AllowToPostBatchPostingToED() As Boolean
        Get
            Return mpreAllowToPostBatchPostingToED
        End Get
    End Property

    Public ReadOnly Property _AllowToViewBatchPostingList() As Boolean
        Get
            Return mpreAllowToViewBatchPostingList
        End Get
    End Property
    'Sohail (05 Dec 2012) -- End

    'Sohail (24 Feb 2016) -- Start
    'Enhancement - Batch Posting changes as per online doc in 58.1 SP. (#15 (KBC & Kenya Project Comments List.xls))
    Public ReadOnly Property _AllowToVoidBatchPostingToED() As Boolean
        Get
            Return mpreAllowToVoidBatchPostingToED
        End Get
    End Property
    'Sohail (24 Feb 2016) -- End


    'LEAVE & TNA
    Public ReadOnly Property _AddLeaveType() As Boolean
        Get
            Return mpreAddLeaveType
        End Get
    End Property

    Public ReadOnly Property _EditLeaveType() As Boolean
        Get
            Return mpreEditLeaveType
        End Get
    End Property

    Public ReadOnly Property _DeleteLeaveType() As Boolean
        Get
            Return mpreDeleteLeaveType
        End Get
    End Property

    Public ReadOnly Property _AddHolidays() As Boolean
        Get
            Return mpreAddHolidays
        End Get
    End Property

    Public ReadOnly Property _EditHolidays() As Boolean
        Get
            Return mpreEditHolidays
        End Get
    End Property

    Public ReadOnly Property _DeleteHolidays() As Boolean
        Get
            Return mpreDeleteHolidays
        End Get
    End Property

    Public ReadOnly Property _AddLeaveApproverLevel() As Boolean
        Get
            Return mpreAddLeaveApproverLevel
        End Get
    End Property

    Public ReadOnly Property _EditLeaveApproverLevel() As Boolean
        Get
            Return mpreEditLeaveApproverLevel
        End Get
    End Property

    Public ReadOnly Property _DeleteLeaveApproverLevel() As Boolean
        Get
            Return mpreDeleteLeaveApproverLevel
        End Get
    End Property

    Public ReadOnly Property _AddLeaveApprover() As Boolean
        Get
            Return mpreAddLeaveApprover
        End Get
    End Property

    Public ReadOnly Property _EditLeaveApprover() As Boolean
        Get
            Return mpreEditLeaveApprover
        End Get
    End Property

    Public ReadOnly Property _DeleteLeaveApprover() As Boolean
        Get
            Return mpreDeleteLeaveApprover
        End Get
    End Property

    Public ReadOnly Property _AddLeaveForm() As Boolean
        Get
            Return mpreAddLeaveForm
        End Get
    End Property

    Public ReadOnly Property _EditLeaveForm() As Boolean
        Get
            Return mpreEditLeaveForm
        End Get
    End Property

    Public ReadOnly Property _DeleteLeaveForm() As Boolean
        Get
            Return mpreDeleteLeaveForm
        End Get
    End Property

    Public ReadOnly Property _AllowPrintLeaveForm() As Boolean
        Get
            Return mpreAllowPrintLeaveForm
        End Get
    End Property

    Public ReadOnly Property _AllowPreviewLeaveForm() As Boolean
        Get
            Return mpreAllowPreviewLeaveForm
        End Get
    End Property

    Public ReadOnly Property _AllowEmailLeaveForm() As Boolean
        Get
            Return mpreAllowEmailLeaveForm
        End Get
    End Property

    Public ReadOnly Property _AllowProcessLeaveForm() As Boolean
        Get
            Return mpreAllowProcessLeaveForm
        End Get
    End Property

    Public ReadOnly Property _AllowIssueLeave() As Boolean
        Get
            Return mpreAllowIssueLeave
        End Get
    End Property

    Public ReadOnly Property _AllowLeaveAccrue() As Boolean
        Get
            Return mpreAllowLeaveAccrue
        End Get
    End Property

    Public ReadOnly Property _AddEmployeeHolidays() As Boolean
        Get
            Return mpreAddEmployeeHolidays
        End Get
    End Property

    Public ReadOnly Property _EditEmployeeHolidays() As Boolean
        Get
            Return mpreEditEmployeeHolidays
        End Get
    End Property

    Public ReadOnly Property _DeleteEmployeeHolidays() As Boolean
        Get
            Return mpreDeleteEmployeeHolidays
        End Get
    End Property

    Public ReadOnly Property _AllowLeaveViewing() As Boolean
        Get
            Return mpreAllowLeaveViewing
        End Get
    End Property

    Public ReadOnly Property _AddShiftInformation() As Boolean
        Get
            Return mpreAddShiftInformation
        End Get
    End Property

    Public ReadOnly Property _EditShiftInformation() As Boolean
        Get
            Return mpreEditShiftInformation
        End Get
    End Property

    Public ReadOnly Property _DeleteShiftInformation() As Boolean
        Get
            Return mpreDeleteShiftInformation
        End Get
    End Property

    Public ReadOnly Property _AddTimeSheetInformation() As Boolean
        Get
            Return mpreAddTimeSheetInformation
        End Get
    End Property

    Public ReadOnly Property _EditTimeSheetInformation() As Boolean
        Get
            Return mpreEditTimeSheetInformation
        End Get
    End Property

    Public ReadOnly Property _DeleteTimeSheetInformation() As Boolean
        Get
            Return mpreDeleteTimeSheetInformation
        End Get
    End Property

    Public ReadOnly Property _AllowHoldEmployee() As Boolean
        Get
            Return mpreAllowHoldEmployee
        End Get
    End Property

    Public ReadOnly Property _AllowUnHoldEmployee() As Boolean
        Get
            Return mpreAllowUnHoldEmployee
        End Get
    End Property

    Public ReadOnly Property _AllowProcessEmployeeAbsent() As Boolean
        Get
            Return mpreAllowProcessEmployeeAbsent
        End Get
    End Property

    'GENERAL SETUPS
    Public ReadOnly Property _AddStation() As Boolean
        Get
            Return mpreAddStation
        End Get
    End Property

    Public ReadOnly Property _EditStation() As Boolean
        Get
            Return mpreEditStation
        End Get
    End Property

    Public ReadOnly Property _DeleteStation() As Boolean
        Get
            Return mpreDeleteStation
        End Get
    End Property

    Public ReadOnly Property _AddDepartmentGroup() As Boolean
        Get
            Return mpreAddDepartmentGroup
        End Get
    End Property

    Public ReadOnly Property _EditDepartmentGroup() As Boolean
        Get
            Return mpreEditDepartmentGroup
        End Get
    End Property

    Public ReadOnly Property _DeleteDepartmentGroup() As Boolean
        Get
            Return mpreDeleteDepartmentGroup
        End Get
    End Property

    Public ReadOnly Property _AddDepartment() As Boolean
        Get
            Return mpreAddDepartment
        End Get
    End Property

    Public ReadOnly Property _EditDepartment() As Boolean
        Get
            Return mpreEditDepartment
        End Get
    End Property

    Public ReadOnly Property _DeleteDepartment() As Boolean
        Get
            Return mpreDeleteDepartment
        End Get
    End Property

    Public ReadOnly Property _AddSection() As Boolean
        Get
            Return mpreAddSection
        End Get
    End Property

    Public ReadOnly Property _EditSection() As Boolean
        Get
            Return mpreEditSection
        End Get
    End Property

    Public ReadOnly Property _DeleteSection() As Boolean
        Get
            Return mpreDeleteSection
        End Get
    End Property

    Public ReadOnly Property _AddUnit() As Boolean
        Get
            Return mpreAddUnit
        End Get
    End Property

    Public ReadOnly Property _EditUnit() As Boolean
        Get
            Return mpreEditUnit
        End Get
    End Property

    Public ReadOnly Property _DeleteUnit() As Boolean
        Get
            Return mpreDeleteUnit
        End Get
    End Property

    Public ReadOnly Property _AddJobGroup() As Boolean
        Get
            Return mpreAddJobGroup
        End Get
    End Property

    Public ReadOnly Property _EditJobGroup() As Boolean
        Get
            Return mpreEditJobGroup
        End Get
    End Property

    Public ReadOnly Property _DeleteJobGroup() As Boolean
        Get
            Return mpreDeleteJobGroup
        End Get
    End Property

    Public ReadOnly Property _AddJob() As Boolean
        Get
            Return mpreAddJob
        End Get
    End Property

    Public ReadOnly Property _EditJob() As Boolean
        Get
            Return mpreEditJob
        End Get
    End Property

    Public ReadOnly Property _DeleteJob() As Boolean
        Get
            Return mpreDeleteJob
        End Get
    End Property

    Public ReadOnly Property _AddClassGroup() As Boolean
        Get
            Return mpreAddClassGroup
        End Get
    End Property

    Public ReadOnly Property _EditClassGroup() As Boolean
        Get
            Return mpreEditClassGroup
        End Get
    End Property

    Public ReadOnly Property _DeleteClassGroup() As Boolean
        Get
            Return mpreDeleteClassGroup
        End Get
    End Property

    Public ReadOnly Property _AddClasses() As Boolean
        Get
            Return mpreAddClasses
        End Get
    End Property

    Public ReadOnly Property _EditClasses() As Boolean
        Get
            Return mpreEditClasses
        End Get
    End Property

    Public ReadOnly Property _DeleteClasses() As Boolean
        Get
            Return mpreDeleteClasses
        End Get
    End Property

    Public ReadOnly Property _AddGradeGroup() As Boolean
        Get
            Return mpreAddGradeGroup
        End Get
    End Property

    Public ReadOnly Property _EditGradeGroup() As Boolean
        Get
            Return mpreEditGradeGroup
        End Get
    End Property

    Public ReadOnly Property _DeleteGradeGroup() As Boolean
        Get
            Return mpreDeleteGradeGroup
        End Get
    End Property

    Public ReadOnly Property _AddGrade() As Boolean
        Get
            Return mpreAddGrade
        End Get
    End Property

    Public ReadOnly Property _EditGrade() As Boolean
        Get
            Return mpreEditGrade
        End Get
    End Property

    Public ReadOnly Property _DeleteGrade() As Boolean
        Get
            Return mpreDeleteGrade
        End Get
    End Property

    Public ReadOnly Property _AddGradeLevel() As Boolean
        Get
            Return mpreAddGradeLevel
        End Get
    End Property

    Public ReadOnly Property _EditGradeLevel() As Boolean
        Get
            Return mpreEditGradeLevel
        End Get
    End Property

    Public ReadOnly Property _DeleteGradeLevel() As Boolean
        Get
            Return mpreDeleteGradeLevel
        End Get
    End Property

    'LOAN & SAVINGS
    Public ReadOnly Property _AddLoanScheme() As Boolean
        Get
            Return mpreAddLoanScheme
        End Get
    End Property

    Public ReadOnly Property _EditLoanScheme() As Boolean
        Get
            Return mpreEditLoanScheme
        End Get
    End Property

    Public ReadOnly Property _DeleteLoanScheme() As Boolean
        Get
            Return mpreDeleteLoanScheme
        End Get
    End Property

    Public ReadOnly Property _AllowLoanApproverCreation() As Boolean
        Get
            Return mpreAllowLoanApproverCreation
        End Get
    End Property

    Public ReadOnly Property _AddPendingLoan() As Boolean
        Get
            Return mpreAddPendingLoan
        End Get
    End Property

    Public ReadOnly Property _EditPendingLoan() As Boolean
        Get
            Return mpreEditPendingLoan
        End Get
    End Property

    Public ReadOnly Property _DeletePendingLoan() As Boolean
        Get
            Return mpreDeletePendingLoan
        End Get
    End Property

    Public ReadOnly Property _AllowAssignPendingLoan() As Boolean
        Get
            Return mpreAllowAssignPendingLoan
        End Get
    End Property

    Public ReadOnly Property _EditLoanAdvance() As Boolean
        Get
            Return mpreEditLoanAdvance
        End Get
    End Property

    Public ReadOnly Property _DeleteLoanAdvance() As Boolean
        Get
            Return mpreDeleteLoanAdvance
        End Get
    End Property

    Public ReadOnly Property _AllowChangeLoanAvanceStatus() As Boolean
        Get
            Return mpreAllowChangeLoanAvanceStatus
        End Get
    End Property

    Public ReadOnly Property _AddSavingScheme() As Boolean
        Get
            Return mpreAddSavingScheme
        End Get
    End Property

    Public ReadOnly Property _EditSavingScheme() As Boolean
        Get
            Return mpreEditSavingScheme
        End Get
    End Property

    Public ReadOnly Property _DeleteSavingScheme() As Boolean
        Get
            Return mpreDeleteSavingScheme
        End Get
    End Property

    Public ReadOnly Property _AddEmployeeSavings() As Boolean
        Get
            Return mpreAddEmployeeSavings
        End Get
    End Property

    Public ReadOnly Property _EditEmployeeSavings() As Boolean
        Get
            Return mpreEditEmployeeSavings
        End Get
    End Property

    Public ReadOnly Property _DeleteEmployeeSavings() As Boolean
        Get
            Return mpreDeleteEmployeeSavings
        End Get
    End Property

    Public ReadOnly Property _AllowChangeSavingStatus() As Boolean
        Get
            Return mpreAllowChangeSavingStatus
        End Get
    End Property

    'Nilay (20-Sept-2016) -- Start
    'Enhancement : Cancel feature for approved but not assigned loan application
    Public ReadOnly Property _AllowToCancelApprovedLoanApp() As Boolean
        Get
            Return mpreAllowToCancelApprovedLoanApp
        End Get
    End Property
    'Nilay (20-Sept-2016) -- End

    'S.SANDEEP [ 13 JUNE 2011 ] -- START
    'ISSUE : EMPLOYEE & LOAN PRIVILEGE
    Public ReadOnly Property _AllowtoApproveLoan() As Boolean
        Get
            Return mpreAllowApproveLoan
        End Get
    End Property

    Public ReadOnly Property _AllowtoApproveAdvance() As Boolean
        Get
            Return mpreAllowApproveAdvance
        End Get
    End Property
    'S.SANDEEP [ 13 JUNE 2011 ] -- END


    'Sandeep [ 23 Oct 2010 ] -- Start
    Public ReadOnly Property _AllowtoApproveLeave() As Boolean
        Get
            Return mpreAllowtoApproveLeave
        End Get
    End Property
    'Sandeep [ 23 Oct 2010 ] -- End 

    'Anjan (14 Feb 2011)-Start

    'S.SANDEEP [28 MAY 2015] -- START
    'ENHANCEMENT : NEW PRIVILEGE (Performance Assessment)
    'Public ReadOnly Property _AllowAddAssessor() As Boolean
    '    Get
    '        Return mpreAllowtoAddAssessor
    '    End Get
    'End Property
    'Public ReadOnly Property _AllowRemoveAssessor() As Boolean
    '    Get
    '        Return mpreAllowtoDeleteAssessor
    '    End Get
    'End Property

    'Public ReadOnly Property _AddAssessorAccess() As Boolean
    '    Get
    '        Return mpreAddAssessorAccess
    '    End Get
    'End Property
    'Public ReadOnly Property _EditAssessorAccess() As Boolean
    '    Get
    '        Return mpreEditAssessorAccess
    '    End Get
    'End Property
    'Public ReadOnly Property _DeleteAssessorAccess() As Boolean
    '    Get
    '        Return mpreDeleteAssessorAccess
    '    End Get
    'End Property
    'Public ReadOnly Property _AllowMapAssessorwithUser() As Boolean
    '    Get
    '        Return mpreAllowtoMapAssessorUser
    '    End Get
    'End Property

    'Public ReadOnly Property _AddEmployeeAssessment() As Boolean
    '    Get
    '        Return mpreAddEmployeeAssessment
    '    End Get
    'End Property

    'Public ReadOnly Property _EditEmployeeAssessment() As Boolean
    '    Get
    '        Return mpreEditEmployeeAssessment
    '    End Get
    'End Property

    'Public ReadOnly Property _DeleteEmployeeAssessment() As Boolean
    '    Get
    '        Return mpreDeleteEmployeeAssessment
    '    End Get
    'End Property
    'S.SANDEEP [28 MAY 2015] -- END

    'Varsha Rana (17-Oct-2017) -- Start
    'Enhancement - Give user privileges 
    'Public ReadOnly Property _AddAssessorAssessment() As Boolean
    '    Get
    '        Return mpreAddDisciplineStatus
    '    End Get
    'End Property

    'Public ReadOnly Property _EditAssessorAssessment() As Boolean
    '    Get
    '        Return mpreEditDisciplineStatus
    '    End Get
    'End Property

    'Public ReadOnly Property _DeleteAssessorAssessment() As Boolean
    '    Get
    '        Return mpreDeleteDisciplineStatus
    '    End Get
    'End Property
    'Varsha Rana (17-Oct-2017) -- End
    Public ReadOnly Property _AddDisciplineStatus() As Boolean
        Get
            Return mpreAddDisciplineStatus
        End Get
    End Property
    Public ReadOnly Property _EditDisciplineStatus() As Boolean
        Get
            Return mpreEditDisciplineStatus
        End Get
    End Property
    Public ReadOnly Property _DeleteDisciplineStatus() As Boolean
        Get
            Return mpreDeleteDisciplineStatus
        End Get
    End Property

    Public ReadOnly Property _AllowViewEmployeeMovement() As Boolean
        Get
            Return mpreAllowtoViewEmployeeMovement
        End Get
    End Property
    Public ReadOnly Property _AllowEnrollFingerPrint() As Boolean
        Get
            Return mpreAllowtoEnrollFingerPrint
        End Get
    End Property

    Public ReadOnly Property _AllowDeleteFingerPrint() As Boolean
        Get
            Return mpreAllowtoDeleteFingerPrint
        End Get
    End Property

    Public ReadOnly Property _AllowEnrollNewCard() As Boolean
        Get
            Return mpreAllowtoEnrollNewCard
        End Get
    End Property

    Public ReadOnly Property _AllowDeleteEnrolledCard() As Boolean
        Get
            Return mpreAllowtoDeleteEnrolledCard
        End Get
    End Property

    'Sohail (04 Oct 2013) -- Start
    'TRA - ENHANCEMENT
    Public ReadOnly Property _AllowToMapDeviceUser() As Boolean
        Get
            Return mpreAllowtoMapDeviceUser
        End Get
    End Property

    Public ReadOnly Property _AllowToImportDeviceUser() As Boolean
        Get
            Return mpreAllowtoImportDeviceUser
        End Get
    End Property
    'Sohail (04 Oct 2013) -- End

    Public ReadOnly Property _AllowImportEmployee() As Boolean
        Get
            Return mpreAllowtoImportEmployee
        End Get
    End Property

    Public ReadOnly Property _AddTrainingInstitute() As Boolean
        Get
            Return mpreAddTrainingInstitute
        End Get
    End Property
    Public ReadOnly Property _EditTrainingInstitute() As Boolean
        Get
            Return mpreEditTrainingInstitute
        End Get
    End Property
    Public ReadOnly Property _DeleteTrainingInstitute() As Boolean
        Get
            Return mpreDeleteTrainingInstitute
        End Get
    End Property

    Public ReadOnly Property _AllowImportTransactionHead() As Boolean
        Get
            Return mpreAllowtoImportTransactionHead
        End Get
    End Property
    Public ReadOnly Property _AddPayslipMessage() As Boolean
        Get
            Return mpreAddPayslipMessage
        End Get
    End Property
    Public ReadOnly Property _EditPayslipMessage() As Boolean
        Get
            Return mpreEditPayslipMessage
        End Get
    End Property
    Public ReadOnly Property _DeletePayslipMessage() As Boolean
        Get
            Return mpreDeletePayslipMessage
        End Get
    End Property

    'Nilay (28-Apr-2016) -- Start
    'Enhancement : 60.1 - #77 (KBC & Kenya Project Comments List.xls) - For payslip messages, include payroll period so that If you print payslip for last month and you had a new payslip message this month, It does not pick up message of current month but last month.
    'Public ReadOnly Property _AllowAddEditGPayslipMessage() As Boolean
    '    Get
    '        Return mpreAllowtoAddEditGPayslipMessage
    '    End Get
    'End Property
    Public ReadOnly Property _AllowAddGlobalPayslipMessage() As Boolean
        Get
            Return mpreAllowtoAddGlobalPayslipMessage
        End Get
    End Property

    Public ReadOnly Property _AllowEditGlobalPayslipMessage() As Boolean
        Get
            Return mpreAllowtoEditGlobalPayslipMessage
        End Get
    End Property

    Public ReadOnly Property _AllowDeleteGlobalPayslipMessage() As Boolean
        Get
            Return mpreAllowtoDeleteGlobalPayslipMessage
        End Get
    End Property

    Public ReadOnly Property _AllowViewGlobalPayslipMessage() As Boolean
        Get
            Return mpreAllowtoViewGlobalPayslipMessage
        End Get
    End Property
    'Nilay (28-Apr-2016) -- End

    Public ReadOnly Property _AddPlanLeave() As Boolean
        Get
            Return mpreAddPlanLeave
        End Get
    End Property

    Public ReadOnly Property _EditPlanLeave() As Boolean
        Get
            Return mpreEditPlanLeave
        End Get
    End Property
    Public ReadOnly Property _DeletePlanLeave() As Boolean
        Get
            Return mpreDeletePlanLeave
        End Get
    End Property
    Public ReadOnly Property _AllowDeleteAccruedLeave() As Boolean
        Get
            Return mpreAllowDeleteAccruedLeave
        End Get
    End Property
    Public ReadOnly Property _AllowtoViewPlannedLeaveViewer() As Boolean
        Get
            Return mpreAllowViewPlannedLeaveViewer
        End Get
    End Property

    Public ReadOnly Property _AddLoanAdvancePayment() As Boolean
        Get
            Return mpreAddLoanAdvancePayment
        End Get
    End Property

    Public ReadOnly Property _EditLoanAdvancePayment() As Boolean
        Get
            Return mpreEditLoanAdvancePayment
        End Get
    End Property

    Public ReadOnly Property _DeleteLoanAdvancePayment() As Boolean
        Get
            Return mpreDeleteLoanAdvancePayment
        End Get
    End Property
    Public ReadOnly Property _AddLoanAdvanceReceived() As Boolean
        Get
            Return mpreAddLoanAdvanceReceived
        End Get
    End Property

    Public ReadOnly Property _EditLoanAdvanceReceived() As Boolean
        Get
            Return mpreEditLoanAdvanceReceived
        End Get
    End Property
    Public ReadOnly Property _DeleteLoanAdvanceReceived() As Boolean
        Get
            Return mpreDeleteLoanAdvanceReceived
        End Get
    End Property
    Public ReadOnly Property _AllowAssignBatchtoEmployee() As Boolean
        Get
            Return mpreAllowAssignBatchtoEmployee
        End Get
    End Property
    Public ReadOnly Property _AllowLoginTimesheet() As Boolean
        Get
            Return mpreAllowLoginTimesheet
        End Get
    End Property

    Public ReadOnly Property _AllowImportLeaveTnA() As Boolean
        Get
            Return mpreAllowImportLeaveTnA
        End Get
    End Property
    Public ReadOnly Property _AllowExportLeaveTnA() As Boolean
        Get
            Return mpreAllowExportLeaveTnA
        End Get
    End Property
    Public ReadOnly Property _AllowFinalAnalysis() As Boolean
        Get
            Return mpreAllowFinalAnalysis
        End Get
    End Property

    Public ReadOnly Property _AddTrainingAnalysis() As Boolean
        Get
            Return mpreAddTrainingAnalysis
        End Get
    End Property
    Public ReadOnly Property _EditTrainingAnalysis() As Boolean
        Get
            Return mpreEditTrainingAnalysis
        End Get
    End Property

    Public ReadOnly Property _DeleteTrainingAnalysis() As Boolean
        Get
            Return mpreDeleteTrainingAnalysis
        End Get
    End Property
    Public ReadOnly Property _AddTrainingEnrollment() As Boolean
        Get
            Return mpreAddTrainingEnrollment
        End Get
    End Property
    Public ReadOnly Property _EditTrainingEnrollment() As Boolean
        Get
            Return mpreEditTrainingEnrollment
        End Get
    End Property
    Public ReadOnly Property _DeleteTrainingEnrollment() As Boolean
        Get
            Return mpreDeleteTrainingEnrollment
        End Get
    End Property
    Public ReadOnly Property _CancelTrainingEnrollment() As Boolean
        Get
            Return mpreCancelTrainingEnrollment
        End Get
    End Property
    Public ReadOnly Property _AddCourseScheduling() As Boolean
        Get
            Return mpreAddCourseScheduling
        End Get
    End Property
    Public ReadOnly Property _EditCourseScheduling() As Boolean
        Get
            Return mpreEditCourseScheduling
        End Get
    End Property
    Public ReadOnly Property _DeleteCourseScheduling() As Boolean
        Get
            Return mpreDeleteCourseScheduling
        End Get
    End Property

    'S.SANDEEP [28 MAY 2015] -- START
    'ENHANCEMENT : NEW PRIVILEGE (Performance Assessment)
    'Public ReadOnly Property _AddAssessmentPeriod() As Boolean
    '    Get
    '        Return mpreAddAssessmentPeriod
    '    End Get
    'End Property
    'Public ReadOnly Property _EditAssessmentPeriod() As Boolean
    '    Get
    '        Return mpreEditAssessmentPeriod
    '    End Get
    'End Property
    'Public ReadOnly Property _DeleteAssessmentPeriod() As Boolean
    '    Get
    '        Return mpreDeleteAssessmentPeriod
    '    End Get
    'End Property
    'S.SANDEEP [28 MAY 2015] -- END

    Public ReadOnly Property _AddCashDenomination() As Boolean
        Get
            Return mpreAddCashDenomination
        End Get
    End Property
    Public ReadOnly Property _EditCashDenomination() As Boolean
        Get
            Return mpreEditCashDenomination
        End Get
    End Property
    Public ReadOnly Property _AllowImportED() As Boolean
        Get
            Return mpreAllowImportED
        End Get
    End Property
    Public ReadOnly Property _AllowExportED() As Boolean
        Get
            Return mpreAllowExportED
        End Get
    End Property
    Public ReadOnly Property _AllowGlobalPayment() As Boolean
        Get
            Return mpreAllowGlobalPayment
        End Get
    End Property
    Public ReadOnly Property _AllowGlobalSalaryIncrement() As Boolean
        Get
            Return mpreAllowGlobalSalaryIncrement
        End Get
    End Property
    Public ReadOnly Property _AllowChangePendingLoanAdvanceStatus() As Boolean
        Get
            Return mpreAllowChangePendingLoanAdvanceStatus
        End Get
    End Property
    Public ReadOnly Property _AddSavingsPayment() As Boolean
        Get
            Return mpreAddSavingsPayment
        End Get
    End Property
    Public ReadOnly Property _EditSavingsPayment() As Boolean
        Get
            Return mpreEditSavingsPayment
        End Get
    End Property
    Public ReadOnly Property _DeleteSavingsPayment() As Boolean
        Get
            Return mpreDeleteSavingsPayment
        End Get
    End Property

    'Sohail (21 Nov 2015) -- Start
    'Enhancement - Provide Deposit feaure in Employee Saving.
    Public ReadOnly Property _AddSavingsDeposit() As Boolean
        Get
            Return mpreAddSavingsDeposit
        End Get
    End Property
    Public ReadOnly Property _EditSavingsDeposit() As Boolean
        Get
            Return mpreEditSavingsDeposit
        End Get
    End Property
    Public ReadOnly Property _DeleteSavingsDeposit() As Boolean
        Get
            Return mpreDeleteSavingsDeposit
        End Get
    End Property
    'Sohail (21 Nov 2015) -- End

    Public ReadOnly Property _AllowMapApproverWithUser() As Boolean
        Get
            Return mpreAllowMapApproverWithUser
        End Get
    End Property
    Public ReadOnly Property _AllowChangeLeaveFormStatus() As Boolean
        Get
            Return mpreAllowChangeLeaveFormStatus
        End Get
    End Property
    Public ReadOnly Property _AllowCommonExport() As Boolean
        Get
            Return mpreAllowCommonExport
        End Get
    End Property
    Public ReadOnly Property _AllowCommonImport() As Boolean
        Get
            Return mpreAllowCommonImport
        End Get
    End Property
    'Anjan (14 Feb 2011)-End



    'Anjan (21 Nov 2011)-Start
    'ENHANCEMENT : TRA COMMENTS
    Public ReadOnly Property _AllowAccessSalaryAnalysis() As Boolean
        Get
            Return mpreAllowAccessSalaryAnalysis
        End Get
    End Property

    Public ReadOnly Property _AllowAccessLeaveAnalysis() As Boolean
        Get
            Return mpreAllowAccessLeaveAnalysis
        End Get
    End Property

    Public ReadOnly Property _AllowAccessStaffTurnOver() As Boolean
        Get
            Return mpreAllowAccessStaffTurnOver
        End Get
    End Property

    Public ReadOnly Property _AllowAccessTrainingAnalysis() As Boolean
        Get
            Return mpreAllowAccessTrainingAnalysis
        End Get
    End Property

    Public ReadOnly Property _AllowAccessAttendanceSummary() As Boolean
        Get
            Return mpreAllowAccessAttendanceSummary
        End Get
    End Property

    'Anjan (21 Nov 2011)-End 

    'S.SANDEEP [ 07 NOV 2011 ] -- START
    'ENHANCEMENT : TRA CHANGES
    Public ReadOnly Property _AllowToApproveResolutionStep() As Boolean
        Get
            Return mpreAllowToApproveResolutionStep
        End Get
    End Property

    'S.SANDEEP [24 MAY 2016] -- START
    'ENHANCEMENT : DISCIPLINE MAKE-OVER {BY ANDREW}
    Public ReadOnly Property _AllowToApproveProceedingCounts() As Boolean
        Get
            Return mpreAllowToApproveProceedingCounts
        End Get
    End Property
    'S.SANDEEP [24 MAY 2016] -- END


    Public ReadOnly Property _AllowToAddResolutionStep() As Boolean
        Get
            Return mpreAllowToAddResolutionStep
        End Get
    End Property

    Public ReadOnly Property _AllowToEditResolutionStep() As Boolean
        Get
            Return mpreAllowToEditResolutionStep
        End Get
    End Property

    Public ReadOnly Property _AllowToDeleteResolutionStep() As Boolean
        Get
            Return mpreAllowToDeleteResolutionStep
        End Get
    End Property

    Public ReadOnly Property _AllowToAddDiscipileExemption() As Boolean
        Get
            Return mpreAllowToAddDiscipileExemption
        End Get
    End Property

    Public ReadOnly Property _AllowToAddDisciplinePosting() As Boolean
        Get
            Return mpreAllowToAddDisciplinePosting
        End Get
    End Property

    Public ReadOnly Property _AllowToViewDisciplineExemption() As Boolean
        Get
            Return mpreAllowToViewDisciplineExemption
        End Get
    End Property

    Public ReadOnly Property _AllowToViewDisciplinePosting() As Boolean
        Get
            Return mpreAllowToViewDisciplinePosting
        End Get
    End Property

    Public ReadOnly Property _AllowToDeleteDisciplineExemption() As Boolean
        Get
            Return mpreAllowToDeleteDisciplineExemption
        End Get
    End Property

    Public ReadOnly Property _AllowToDeleteDisciplinePosting() As Boolean
        Get
            Return mpreAllowToDeleteDisciplinePosting
        End Get
    End Property
    'S.SANDEEP [ 07 NOV 2011 ] -- END


    'RECRUITMENT PRIVELEGES
    'S.SANDEEP [ 25 DEC 2011 ] -- START
    'ENHANCEMENT : TRA CHANGES
    Public ReadOnly Property _AllowtoAddVacancy() As Boolean
        Get
            Return mpreAllowtoAddVacancy
        End Get
    End Property

    Public ReadOnly Property _AllowtoEditVacancy() As Boolean
        Get
            Return mpreAllowtoEditVacancy
        End Get
    End Property

    Public ReadOnly Property _AllowtoDeleteVacancy() As Boolean
        Get
            Return mpreAllowtoDeleteVacancy
        End Get
    End Property

    Public ReadOnly Property _AllowtoAddApplicant() As Boolean
        Get
            Return mpreAllowtoAddApplicant
        End Get
    End Property

    Public ReadOnly Property _AllowtoEditApplicant() As Boolean
        Get
            Return mpreAllowtoEditApplicant
        End Get
    End Property

    Public ReadOnly Property _AllowtoDeleteApplicant() As Boolean
        Get
            Return mpreAllowtoDeleteApplicant
        End Get
    End Property

    Public ReadOnly Property _AllowtoExportDataOnWeb() As Boolean
        Get
            Return mpreAllowtoExportDataOnWeb
        End Get
    End Property

    Public ReadOnly Property _AllowtoImportDataFromWeb() As Boolean
        Get
            Return mpreAllowtoImportDataFromWeb
        End Get
    End Property

    Public ReadOnly Property _AllowtoAddBatch() As Boolean
        Get
            Return mpreAllowtoAddBatch
        End Get
    End Property

    Public ReadOnly Property _AllowtoEditBatch() As Boolean
        Get
            Return mpreAllowtoEditBatch
        End Get
    End Property

    Public ReadOnly Property _AllowtoDeleteBatch() As Boolean
        Get
            Return mpreAllowtoDeleteBatch
        End Get
    End Property

    Public ReadOnly Property _AllowtoCancelBatch() As Boolean
        Get
            Return mpreAllowtoCancelBatch
        End Get
    End Property

    Public ReadOnly Property _AllowtoAddInterviewSchedule() As Boolean
        Get
            Return mpreAllowtoAddInterviewSchedule
        End Get
    End Property

    Public ReadOnly Property _AllowtoDeleteInterviewSchedule() As Boolean
        Get
            Return mpreAllowtoDeleteInterviewSchedule
        End Get
    End Property

    Public ReadOnly Property _AllowtoCancelInterviewSchedule() As Boolean
        Get
            Return mpreAllowtoCancelInterviewSchedule
        End Get
    End Property

    Public ReadOnly Property _AllowtoAddInterviewAnalysis() As Boolean
        Get
            Return mpreAllowtoAddAnalysis
        End Get
    End Property

    Public ReadOnly Property _AllowtoChangeBatch() As Boolean
        Get
            Return mpreAllowtoChangeBatch
        End Get
    End Property

    Public ReadOnly Property _AllowtoAddInterviewFinalEvaluation() As Boolean
        Get
            Return mpreAllowtoAddFinalEvaluation
        End Get
    End Property

    Public ReadOnly Property _AllowtoEditInterviewAnalysis() As Boolean
        Get
            Return mpreAllowtoEditAnalysis
        End Get
    End Property

    Public ReadOnly Property _AllowtoDeleteInterviewAnalysis() As Boolean
        Get
            Return mpreAllowtoDeleteAnalysis
        End Get
    End Property

    Public ReadOnly Property _AllowtoMarkApplicantEligible() As Boolean
        Get
            Return mpreAllowtoMarkEligible
        End Get
    End Property

    Public ReadOnly Property _AllowtoSendMailApplicant() As Boolean
        Get
            Return mpreAllowtoSendMail
        End Get
    End Property

    Public ReadOnly Property _AllowtoGenerateApplicantLetter() As Boolean
        Get
            Return mpreAllowtoGenerateLetter
        End Get
    End Property

    Public ReadOnly Property _AllowtoPrintFinalApplicantList() As Boolean
        Get
            Return mpreAllowtoPrintList
        End Get
    End Property

    Public ReadOnly Property _AllowtoImportEligibleApplicant() As Boolean
        Get
            Return mpreAllowtoImportEligibleApplicant
        End Get
    End Property

    Public ReadOnly Property _AllowtoMakeBatchActive() As Boolean
        Get
            Return mpreAllowtoMakeBatchActive
        End Get
    End Property

    Public ReadOnly Property _AllowtoAttachApplicantDocuments() As Boolean
        Get
            Return mpreAllowtoAttachApplicantDocuments
        End Get
    End Property
    'S.SANDEEP [ 25 DEC 2011 ] -- END


    'Anjan (11 Jan 2011)-Start
    'ENHANCEMENT : TRA COMMENTS
    Public ReadOnly Property _AllowtoShortListFinalShortListApplicants() As Boolean
        Get
            Return mpreAllowtoShortList_FinalShortListApplicants
        End Get
    End Property
    'Anjan (11 Jan 2011)-End 

    'Hemant (03 Jun 2020) -- Start
    'ISSUE/ENHANCEMENT(NMB) :  RECRUITMENTS FINAL UAT CHANGES(Point No - 1 : On the interview analysis screen, they want to record interview scores for all applicants in a single window instead of analysing the interview an applicant at a time. Also, they want to record all the interview scores for all the interviewers on the same screen.)
    Public ReadOnly Property _AllowtoViewGlobalInterviewAnalysisList() As Boolean
        Get
            Return mpreAllowtoViewGlobalInterviewAnalysisList
        End Get
    End Property
    'Hemant (03 Jun 2020) -- End

    'Sohail (04 Jan 2012) -- Start
    Public ReadOnly Property _AllowChangeAssetStatus() As Boolean
        Get
            Return mpreAllowChangeAssetStatus
        End Get
    End Property
    'Sohail (04 Jan 2012) -- End

    'Anjan (20 Jan 2012)-Start
    'ENHANCEMENT : TRA COMMENTS
    Public ReadOnly Property _AllowChangeGeneralSettings() As Boolean
        Get
            Return mpreChangeGeneralSettings
        End Get
    End Property
    Public ReadOnly Property _AllowAddLetterTemplate() As Boolean
        Get
            Return mpreAllowAddLetter
        End Get
    End Property
    Public ReadOnly Property _AllowEditLetterTemplate() As Boolean
        Get
            Return mpreAllowEditLetter
        End Get
    End Property
    Public ReadOnly Property _AllowDeleteLetterTemplate() As Boolean
        Get
            Return mpreAllowDeleteLetter
        End Get
    End Property
    Public ReadOnly Property _AllowToSendMail() As Boolean
        Get
            Return mpreAllowSendMail
        End Get
    End Property
    Public ReadOnly Property _AllowToDeleteMail() As Boolean
        Get
            Return mpreAllowDeleteMail
        End Get
    End Property
    Public ReadOnly Property _AllowToViewUserLog() As Boolean
        Get
            Return mpreAllowViewUserLog
        End Get
    End Property
    Public ReadOnly Property _AllowToAddAccount() As Boolean
        Get
            Return mpreAllowAddAccount
        End Get
    End Property
    Public ReadOnly Property _AllowToEditAccount() As Boolean
        Get
            Return mpreAllowEditAccount
        End Get
    End Property
    Public ReadOnly Property _AllowToDeleteAccount() As Boolean
        Get
            Return mpreAllowDeleteAccount
        End Get
    End Property
    Public ReadOnly Property _AllowToAddCompanyConfig() As Boolean
        Get
            Return mpreAllowAddAccountConfiguration
        End Get
    End Property
    Public ReadOnly Property _AllowToEditCompanyConfig() As Boolean
        Get
            Return mpreAllowEditAccountConfiguration
        End Get
    End Property
    Public ReadOnly Property _AllowToDeleteCompanyConfig() As Boolean
        Get
            Return mpreAllowDeleteAccountConfiguration
        End Get
    End Property
    Public ReadOnly Property _AllowToAddCCConfig() As Boolean
        Get
            Return mpreAllowAddCCConfiguration
        End Get
    End Property
    Public ReadOnly Property _AllowToEditCCConfig() As Boolean
        Get
            Return mpreAllowEditCCConfiguration
        End Get
    End Property
    Public ReadOnly Property _AllowToDeleteCCConfig() As Boolean
        Get
            Return mpreAllowDeleteCCConfiguration
        End Get
    End Property
    Public ReadOnly Property _AllowToAddEmpConfig() As Boolean
        Get
            Return mpreAllowAddEmpConfiguration
        End Get
    End Property

    Public ReadOnly Property _AllowToEditEmpConfig() As Boolean
        Get
            Return mpreAllowEditEmpConfiguration
        End Get
    End Property

    Public ReadOnly Property _AllowToDeleteEmpConfig() As Boolean
        Get
            Return mpreAllowDeleteEmpConfiguration
        End Get
    End Property
    'Anjan (20 Jan 2012)-End 

    'S.SANDEEP [ 05 MARCH 2012 ] -- START
    'ENHANCEMENT : TRA CHANGES

    'S.SANDEEP [28 MAY 2015] -- START
    'ENHANCEMENT : NEW PRIVILEGE (Performance Assessment)
    'Public ReadOnly Property _Allow_UnlockCommittedGeneralAssessment() As Boolean
    '    Get
    '        Return mpreAllowUnlockCommittedGeneralAssessment
    '    End Get
    'End Property

    'Public ReadOnly Property _Allow_UnlockCommittedBSCAssessment() As Boolean
    '    Get
    '        Return mpreAllowUnlockCommittedBSCAssessment
    '    End Get
    'End Property

    'Public ReadOnly Property _Allow_UnlockFinalSaveBSCPlanning() As Boolean
    '    Get
    '        Return mpreAllowUnlockFinalSaveBSCPlanning
    '    End Get
    'End Property

    'Public ReadOnly Property _Allow_FinalSaveBSCPlanning() As Boolean
    '    Get
    '        Return mpreAllowFinalSaveBSCPlanning
    '    End Get
    'End Property
    'S.SANDEEP [28 MAY 2015] -- END

    'S.SANDEEP [ 05 MARCH 2012 ] -- END


    'Anjan (20 Jan 2012)-End 

    'Anjan (02 Mar 2012)-Start
    'ENHANCEMENT : TRA COMMENTS on Andrew sir's Request
    Public ReadOnly Property _AllowtoSetEmpReinstatementDate() As Boolean
        Get
            Return mpreAllowtosetReinstatementDate
        End Get
    End Property

    'Anjan (02 Mar 2012)-End 

    'Sohail (26 Mar 2012) -- Start
    'TRA - ENHANCEMENT
    Public ReadOnly Property _Allow_AddAssetDeclaration() As Boolean
        Get
            Return mpreAllowToAddAssetDeclaration
        End Get
    End Property

    Public ReadOnly Property _Allow_EditAssetDeclaration() As Boolean
        Get
            Return mpreAllowToEditAssetDeclaration
        End Get
    End Property

    Public ReadOnly Property _Allow_DeleteAssetDeclaration() As Boolean
        Get
            Return mpreAllowToDeleteAssetDeclaration
        End Get
    End Property

    Public ReadOnly Property _Allow_FinalSaveAssetDeclaration() As Boolean
        Get
            Return mpreAllowToFinalSaveAssetDeclaration
        End Get
    End Property

    Public ReadOnly Property _Allow_UnlockFinalSaveAssetDeclaration() As Boolean
        Get
            Return mpreAllowToUnlockFinalSaveAssetDeclaration
        End Get
    End Property
    'Sohail (26 Mar 2012) -- End

    'Anjan (12 Apr 2012)-Start
    'ENHANCEMENT : TRA COMMENTS on Andrew sir's Request
    Public ReadOnly Property _AllowtoAddLeaveAllowance() As Boolean
        Get
            Return mpreAllowAddLeaveAllowance
        End Get
    End Property
    'Anjan (12 Apr 2012)-End 


    'Anjan (17 Apr 2012)-Start
    'ENHANCEMENT : TRA COMMENTS on Andrew sir's Request
    Public ReadOnly Property _AllowtoAddDisciplineCommittee() As Boolean
        Get
            Return mpreAddDisciplineCommittee
        End Get
    End Property

    Public ReadOnly Property _AllowtoEditDisciplineCommittee() As Boolean
        Get
            Return mpreEditDisciplineCommittee
        End Get
    End Property

    Public ReadOnly Property _AllowtoDeleteDisciplineCommittee() As Boolean
        Get
            Return mpreDeleteDisciplineCommittee
        End Get
    End Property

    Public ReadOnly Property _AllowtoCloseCase() As Boolean
        Get
            Return mpreAllowtoCloseCase
        End Get
    End Property

    Public ReadOnly Property _AllowtoReOpenCase() As Boolean
        Get
            Return mpreAllowtoReopenCase
        End Get
    End Property
    'Anjan (17 Apr 2012)-End 

    'S.SANDEEP [ 16 MAY 2012 ] -- START
    'ENHANCEMENT : TRA USER PRIVILEGE CHANGES
    Public ReadOnly Property _AllowToExportMedicalClaim() As Boolean
        Get
            Return mpreAllowToExportMedicalClaim
        End Get
    End Property

    Public ReadOnly Property _AllowToSaveMedicalClaim() As Boolean
        Get
            Return mpreAllowToSaveMedicalClaim
        End Get
    End Property

    Public ReadOnly Property _AllowToFinalSaveMedicalClaim() As Boolean
        Get
            Return mpreAllowToFinalSaveMedicalClaim
        End Get
    End Property

    Public ReadOnly Property _AllowToCancelExportedMedicalClaim() As Boolean
        Get
            Return mpreAllowToCancelExportedMedicalClaim
        End Get
    End Property

    Public ReadOnly Property _AllowToMakeServiceProviderActive() As Boolean
        Get
            Return mpreAllowToMakeServiceProviderActive
        End Get
    End Property

    Public ReadOnly Property _AllowToMapUserWithServiceProvider() As Boolean
        Get
            Return mpreAllowToMapUserWithServiceProvider
        End Get
    End Property

    Public ReadOnly Property _AllowToAddMedicalSickSheet() As Boolean
        Get
            Return mpreAllowToAddMedicalSickSheet
        End Get
    End Property

    Public ReadOnly Property _AllowToEditMedicalSickSheet() As Boolean
        Get
            Return mpreAllowToEditMedicalSickSheet
        End Get
    End Property

    Public ReadOnly Property _AllowToDeleteMedicalSickSheet() As Boolean
        Get
            Return mpreAllowToDeleteMedicalSickSheet
        End Get
    End Property

    Public ReadOnly Property _AllowToPrintMedicalSickSheet() As Boolean
        Get
            Return mpreAllowToPrintMedicalSickSheet
        End Get
    End Property



    'S.SANDEEP [28 MAY 2015] -- START
    'ENHANCEMENT : NEW PRIVILEGE (Performance Assessment)
    'Public ReadOnly Property _AllowToAddBSCObjective() As Boolean
    '    Get
    '        Return mpreAllowToAddBSCObjective
    '    End Get
    'End Property

    'Public ReadOnly Property _AllowToEditBSCObjective() As Boolean
    '    Get
    '        Return mpreAllowToEditBSCObjective
    '    End Get
    'End Property

    'Public ReadOnly Property _AllowToDeleteBSCObjective() As Boolean
    '    Get
    '        Return mpreAllowToDeleteBSCObjective
    '    End Get
    'End Property

    'Public ReadOnly Property _AllowToAddBSCKPI() As Boolean
    '    Get
    '        Return mpreAllowToAddBSCKPI
    '    End Get
    'End Property

    'Public ReadOnly Property _AllowToEditBSCKPI() As Boolean
    '    Get
    '        Return mpreAllowToEditBSCKPI
    '    End Get
    'End Property

    'Public ReadOnly Property _AllowToDeleteBSCKPI() As Boolean
    '    Get
    '        Return mpreAllowToDeleteBSCKPI
    '    End Get
    'End Property

    'Public ReadOnly Property _AllowToAddBSCTargets() As Boolean
    '    Get
    '        Return mpreAllowToAddBSCTargets
    '    End Get
    'End Property

    'Public ReadOnly Property _AllowToEditBSCTargets() As Boolean
    '    Get
    '        Return mpreAllowToEditBSCTargets
    '    End Get
    'End Property

    'Public ReadOnly Property _AllowToDeleteBSCTargets() As Boolean
    '    Get
    '        Return mpreAllowToDeleteBSCTargets
    '    End Get
    'End Property

    'Public ReadOnly Property _AllowToAddBSCInitiative_Action() As Boolean
    '    Get
    '        Return mpreAllowToAddBSCInitiative_Action
    '    End Get
    'End Property

    'Public ReadOnly Property _AllowToEditBSCInitiative_Action() As Boolean
    '    Get
    '        Return mpreAllowToEditBSCInitiative_Action
    '    End Get
    'End Property

    'Public ReadOnly Property _AllowToDeleteBSCInitiative_Action() As Boolean
    '    Get
    '        Return mpreAllowToDeleteBSCInitiative_Action
    '    End Get
    'End Property

    'Public ReadOnly Property _AllowToAddSelfBSCAssessment() As Boolean
    '    Get
    '        Return mpreAllowToAddSelfBSCAssessment
    '    End Get
    'End Property

    'Public ReadOnly Property _AllowToEditSelfBSCAssessment() As Boolean
    '    Get
    '        Return mpreAllowToEditSelfBSCAssessment
    '    End Get
    'End Property

    'Public ReadOnly Property _AllowToDeleteSelfBSCAssessment() As Boolean
    '    Get
    '        Return mpreAllowToDeleteSelfBSCAssessment
    '    End Get
    'End Property

    'Public ReadOnly Property _AllowToAddAssessorBSCAssessment() As Boolean
    '    Get
    '        Return mpreAllowToAddAssessorBSCAssessment
    '    End Get
    'End Property

    'Public ReadOnly Property _AllowToEditAssessorBSCAssessment() As Boolean
    '    Get
    '        Return mpreAllowToEditAssessorBSCAssessment
    '    End Get
    'End Property

    'Public ReadOnly Property _AllowToDeleteAssessorBSCAssessment() As Boolean
    '    Get
    '        Return mpreAllowToDeleteAssessorBSCAssessment
    '    End Get
    'End Property

    'Public ReadOnly Property _AllowToAddReviewerBSCAssessment() As Boolean
    '    Get
    '        Return mpreAllowToAddReviewerBSCAssessment
    '    End Get
    'End Property

    'Public ReadOnly Property _AllowToEditReviewerBSCAssessment() As Boolean
    '    Get
    '        Return mpreAllowToEditReviewerBSCAssessment
    '    End Get
    'End Property

    'Public ReadOnly Property _AllowToDeleteReviewerBSCAssessment() As Boolean
    '    Get
    '        Return mpreAllowToDeleteReviewerBSCAssessment
    '    End Get
    'End Property


    'Public ReadOnly Property _AllowToAddReviewerGeneralAssessment() As Boolean
    '    Get
    '        Return mpreAllowToAddReviewerGeneralAssessment
    '    End Get
    'End Property

    'Public ReadOnly Property _AllowToEditReviewerGeneralAssessment() As Boolean
    '    Get
    '        Return mpreAllowToEditReviewerGeneralAssessment
    '    End Get
    'End Property

    'Public ReadOnly Property _AllowToDeleteReviewerGeneralAssessment() As Boolean
    '    Get
    '        Return mpreAllowToDeleteReviewerGeneralAssessment
    '    End Get
    'End Property
    'S.SANDEEP [28 MAY 2015] -- END



    Public ReadOnly Property _AllowToAddAppraisalAnalysis() As Boolean
        Get
            Return mpreAllowToAddAppraisalAnalysis
        End Get
    End Property

    Public ReadOnly Property _AllowToDeleteAppraisalAnalysis() As Boolean
        Get
            Return mpreAllowToDeleteAppraisalAnalysis
        End Get
    End Property

    Public ReadOnly Property _AllowToPerformSalaryIncrement() As Boolean
        Get
            Return mpreAllowToPerformSalaryIncrement
        End Get
    End Property

    Public ReadOnly Property _AllowToVoidSalaryIncrement() As Boolean
        Get
            Return mpreAllowToVoidSalaryIncrement
        End Get
    End Property

    Public ReadOnly Property _AllowToAddReminder() As Boolean
        Get
            Return mpreAllowToAddReminder
        End Get
    End Property

    Public ReadOnly Property _AllowToAddEmployeeToFinalList() As Boolean
        Get
            Return mpreAllowToAddEmployeeToFinalList
        End Get
    End Property

    Public ReadOnly Property _AllowToSaveAppraisals() As Boolean
        Get
            Return mpreAllowToSaveAppraisals
        End Get
    End Property


    Public ReadOnly Property _AllowToAddEvaluationGroup() As Boolean
        Get
            Return mpreAllowToAddEvaluationGroup
        End Get
    End Property

    Public ReadOnly Property _AllowToEditEvaluationGroup() As Boolean
        Get
            Return mpreAllowToEditEvaluationGroup
        End Get
    End Property

    Public ReadOnly Property _AllowToDeleteEvaluationGroup() As Boolean
        Get
            Return mpreAllowToDeleteEvaluationGroup
        End Get
    End Property

    Public ReadOnly Property _AllowToAddEvaluationItems() As Boolean
        Get
            Return mpreAllowToAddEvaluationItems
        End Get
    End Property

    Public ReadOnly Property _AllowToEditEvaluationItems() As Boolean
        Get
            Return mpreAllowToEditEvaluationItems
        End Get
    End Property

    Public ReadOnly Property _AllowToDeleteEvaluationItems() As Boolean
        Get
            Return mpreAllowToDeleteEvaluationItems
        End Get
    End Property

    Public ReadOnly Property _AllowToAddEvaluationSubItems() As Boolean
        Get
            Return mpreAllowToAddEvaluationSubItems
        End Get
    End Property

    Public ReadOnly Property _AllowToEditEvaluationSubItems() As Boolean
        Get
            Return mpreAllowToEditEvaluationSubItems
        End Get
    End Property

    Public ReadOnly Property _AllowToDeleteEvaluationSubItems() As Boolean
        Get
            Return mpreAllowToDeleteEvaluationSubItems
        End Get
    End Property

    Public ReadOnly Property _AllowToAddEvaluationIIIItems() As Boolean
        Get
            Return mpreAllowToAddEvaluationIIIItems
        End Get
    End Property

    Public ReadOnly Property _AllowToEditEvaluationIIIItems() As Boolean
        Get
            Return mpreAllowToEditEvaluationIIIItems
        End Get
    End Property

    Public ReadOnly Property _AllowToDeleteEvaluationIIIItems() As Boolean
        Get
            Return mpreAllowToDeleteEvaluationIIIItems
        End Get
    End Property

    Public ReadOnly Property _AllowToAddLevelIEvaluation() As Boolean
        Get
            Return mpreAllowToAddLevelIEvaluation
        End Get
    End Property

    Public ReadOnly Property _AllowToEditLevelIEvaluation() As Boolean
        Get
            Return mpreAllowToEditLevelIEvaluation
        End Get
    End Property

    Public ReadOnly Property _AllowToDeleteLevelIEvaluation() As Boolean
        Get
            Return mpreAllowToDeleteLevelIEvaluation

        End Get
    End Property

    Public ReadOnly Property _AllowToPrintLevelIEvaluation() As Boolean
        Get
            Return mpreAllowToPrintLevelIEvaluation
        End Get
    End Property

    Public ReadOnly Property _AllowToPreviewLevelIEvaluation() As Boolean
        Get
            Return mpreAllowToPriviewLevelIEvaluation
        End Get
    End Property

    Public ReadOnly Property _AllowToSave_CompleteLevelIEvaluation() As Boolean
        Get
            Return mpreAllowToSave_CompleteLevelIEvaluation
        End Get
    End Property

    Public ReadOnly Property _AllowToAddLevelIIIEvaluation() As Boolean
        Get
            Return mpreAllowToAddLevelIIIEvaluation
        End Get
    End Property

    Public ReadOnly Property _AllowToEditLevelIIIEvaluation() As Boolean
        Get
            Return mpreAllowToEditLevelIIIEvaluation
        End Get
    End Property

    Public ReadOnly Property _AllowToDeleteLevelIIIEvaluation() As Boolean
        Get
            Return mpreAllowToDeleteLevelIIIEvaluation
        End Get
    End Property

    Public ReadOnly Property _AllowToPrintLevelIIIEvaluation() As Boolean
        Get
            Return mpreAllowToPrintLevelIIIEvaluation
        End Get
    End Property

    Public ReadOnly Property _AllowToPreviewLevelIIIEvaluation() As Boolean
        Get
            Return mpreAllowToPriviewLevelIIIEvaluation
        End Get
    End Property

    Public ReadOnly Property _AllowToSave_CompleteLevelIIIEvaluation() As Boolean
        Get
            Return mpreAllowToSave_CompleteLevelIIIEvaluation
        End Get
    End Property



    'S.SANDEEP [28 MAY 2015] -- START
    'ENHANCEMENT : NEW PRIVILEGE (Performance Assessment)
    'Public ReadOnly Property _Add_Reviewer_Access() As Boolean
    '    Get
    '        Return mpreAdd_Reviewer_Access
    '    End Get
    'End Property

    'Public ReadOnly Property _Edit_Reviewer_Access() As Boolean
    '    Get
    '        Return mpreEdit_Reviewer_Access
    '    End Get
    'End Property

    'Public ReadOnly Property _Delete_Reviewer_Access() As Boolean
    '    Get
    '        Return mpreDelete_Reviewer_Access
    '    End Get
    'End Property

    'Public ReadOnly Property _AllowToMapReviewerWithUser() As Boolean
    '    Get
    '        Return mpreAllowToMapReviewerWithUser
    '    End Get
    'End Property

    'Public ReadOnly Property _AllowToAddAssessmentSubItems() As Boolean
    '    Get
    '        Return mpreAllowToAddAssessmentSubItems
    '    End Get
    'End Property

    'Public ReadOnly Property _AllowToEditAssessmentSubItems() As Boolean
    '    Get
    '        Return mpreAllowToEditAssessmentSubItems
    '    End Get
    'End Property

    'Public ReadOnly Property _AllowToDeleteAssessmentSubItems() As Boolean
    '    Get
    '        Return mpreAllowToDeleteAssessmentSubItems
    '    End Get
    'End Property

    'Public ReadOnly Property _AllowToAddExternalAssessor() As Boolean
    '    Get
    '        Return mpreAllowToAddExternalAssessor
    '    End Get
    'End Property

    'Public ReadOnly Property _AllowToEditExternalAssessor() As Boolean
    '    Get
    '        Return mpreAllowToEditExternalAssessor
    '    End Get
    'End Property

    'Public ReadOnly Property _AllowToDeleteExternalAssessor() As Boolean
    '    Get
    '        Return mpreAllowToDeleteExternalAssessor
    '    End Get
    'End Property

    'Public ReadOnly Property _AllowToMigrateAssessor_Reviewer() As Boolean
    '    Get
    '        Return mpreAllowToMigrateAssessor_Reviewer
    '    End Get
    'End Property
    'S.SANDEEP [28 MAY 2015] -- END



    Public ReadOnly Property _AllowToAddTrainingPriority() As Boolean
        Get
            Return mpreAllowToAddTrainingPriorityList
        End Get
    End Property

    Public ReadOnly Property _AllowToEditTrainingPriority() As Boolean
        Get
            Return mpreAllowToEditTrainingPriorityList
        End Get
    End Property

    Public ReadOnly Property _AllowToDeleteTrainingPriority() As Boolean
        Get
            Return mpreAllowToDeleteTrainingPriorityList
        End Get
    End Property

    Public ReadOnly Property _AllowToExportWithEmployee() As Boolean
        Get
            Return mpreAllowToExportWithEmployee
        End Get
    End Property

    Public ReadOnly Property _AllowToExportWithoutEmployee() As Boolean
        Get
            Return mpreAllowToExportWithoutEmployee
        End Get
    End Property

    Public ReadOnly Property _AllowToPreviewScheduledTraining() As Boolean
        Get
            Return mpreAllowToPreviewScheduledTraining
        End Get
    End Property

    Public ReadOnly Property _AllowToPrintScheduledTraining() As Boolean
        Get
            Return mpreAllowToPrintScheduledTraining
        End Get
    End Property

    Public ReadOnly Property _AllowToUpdateQualifictaion() As Boolean
        Get
            Return mpreAllowToUpdateQualifictaion
        End Get
    End Property

    Public ReadOnly Property _AllowToRe_Categorize() As Boolean
        Get
            Return mpreAllowToRe_Categorize
        End Get
    End Property

    Public ReadOnly Property _AllowSalaryIncrementFromEnrollment() As Boolean
        Get
            Return mpreAllowSalaryIncrementFromEnrollment
        End Get
    End Property

    Public ReadOnly Property _AllowToVoidSalaryIncrementFromEnrollment() As Boolean
        Get
            Return mpreAllowToVoidSalaryIncrementFromEnrollment
        End Get
    End Property

    Public ReadOnly Property _Show_Probation_Dates() As Boolean
        Get
            Return mpreShowProbationDates
        End Get
    End Property

    Public ReadOnly Property _Show_Suspension_Dates() As Boolean
        Get
            Return mpreShowSuspensionDates
        End Get
    End Property

    Public ReadOnly Property _Show_Appointment_Dates() As Boolean
        Get
            Return mpreShowAppointmentDates
        End Get
    End Property

    Public ReadOnly Property _Show_Confirmation_Dates() As Boolean
        Get
            Return mpreShowConfirmationDates
        End Get
    End Property

    Public ReadOnly Property _Show_BirthDates() As Boolean
        Get
            Return mpreShowBirthDates
        End Get
    End Property

    Public ReadOnly Property _Show_Anniversary_Dates() As Boolean
        Get
            Return mpreShowAnniversaryDates
        End Get
    End Property

    Public ReadOnly Property _Show_Contract_Ending_Dates() As Boolean
        Get
            Return mpreShowContractEndingDates
        End Get
    End Property

    Public ReadOnly Property _Show_TodayRetirement_Dates() As Boolean
        Get
            Return mpreShowTodayRetirementDates
        End Get
    End Property

    Public ReadOnly Property _Show_ForcastedRetirement_Dates() As Boolean
        Get
            Return mpreShowForcastedRetirementDates
        End Get
    End Property
    'S.SANDEEP [ 16 MAY 2012 ] -- END

    'S.SANDEEP [ 24 MAY 2012 ] -- START
    'ENHANCEMENT : TRA USER PRIVILEGE CHANGES
    Public ReadOnly Property _Show_ForcastedEOC_Dates() As Boolean
        Get
            Return mpreShowForcastedEOCDates
        End Get
    End Property
    'S.SANDEEP [ 24 MAY 2012 ] -- END


    'Pinkal (03-Nov-2014) -- Start
    'Enhancement -  ENHANCMENT ON LEAVE MODULE GIVEN BY TRA.

    Public ReadOnly Property _Show_ForcastedELC_Dates() As Boolean
        Get
            Return mpreShowForcastedELCDates
        End Get
    End Property

    'Pinkal (03-Nov-2014) -- End


    'Pinkal (28-MAY-2012) -- Start
    'Enhancement : TRA Changes

    Public ReadOnly Property _AllowToCancelLeave() As Boolean
        Get
            Return mpreAllowToCancelLeave
        End Get
    End Property

    Public ReadOnly Property _AllowToCancelPreviousDateLeave() As Boolean
        Get
            Return mpreAllowToCancelPreviousDateLeave
        End Get
    End Property
    'Pinkal (28-MAY-2012) -- End

    'S.SANDEEP [ 19 JUNE 2012 ] -- START
    'ENHANCEMENT : TRA CHANGES
    Public ReadOnly Property _AllowToEditQueries() As Boolean
        Get
            Return mpreAllowToEditQueries
        End Get
    End Property

    Public ReadOnly Property _AllowToAddTemplate() As Boolean
        Get
            Return mpreAllowToAddTemplate
        End Get
    End Property

    Public ReadOnly Property _AllowToEditTemplate() As Boolean
        Get
            Return mpreAllowToEditTemplate
        End Get
    End Property

    Public ReadOnly Property _AllowToDeleteTemplate() As Boolean
        Get
            Return mpreAllowToDeleteTemplate
        End Get
    End Property

    Public ReadOnly Property _AllowToAccessCustomReport() As Boolean
        Get
            Return mpreAllowToAccessCustomReport
        End Get
    End Property
    'S.SANDEEP [ 19 JUNE 2012 ] -- END


    'Pinkal (02-Jul-2012) -- Start
    'Enhancement : TRA Changes

    Public ReadOnly Property _AllowToViewLeaveApproverList() As Boolean
        Get
            Return mpreAllowToViewLeaveApproverList
        End Get
    End Property

    Public ReadOnly Property _AllowToViewLeaveFormList() As Boolean
        Get
            Return mpreAllowToViewLeaveFormList
        End Get
    End Property

    Public ReadOnly Property _AllowToViewLeaveProcessList() As Boolean
        Get
            Return mpreAllowToViewLeaveProcessList
        End Get
    End Property

    Public ReadOnly Property _AllowToViewLeavePlannerList() As Boolean
        Get
            Return mpreAllowToViewLeavePlannerList
        End Get
    End Property

    Public ReadOnly Property _AllowToViewEmpHolidayList() As Boolean
        Get
            Return mpreAllowToViewEmpHolidayList
        End Get
    End Property

    Public ReadOnly Property _AllowToViewMedicalCoverList() As Boolean
        Get
            Return mpreAllowToViewMedicalCoverList
        End Get
    End Property

    Public ReadOnly Property _AllowToViewMedicalClaimList() As Boolean
        Get
            Return mpreAllowToViewMedicalClaimList
        End Get
    End Property

    Public ReadOnly Property _AllowToViewEmpInjuryList() As Boolean
        Get
            Return mpreAllowToViewEmpInjuryList
        End Get
    End Property

    Public ReadOnly Property _AllowToViewEmpSickSheetList() As Boolean
        Get
            Return mpreAllowToViewEmpEmpSickSheetList
        End Get
    End Property

    Public ReadOnly Property _AllowToViewEmpList() As Boolean
        Get
            Return mpreAllowToViewEmpList
        End Get
    End Property

    Public ReadOnly Property _AllowToViewBenefitAllocationList() As Boolean
        Get
            Return mpreAllowToViewBenefitAllocationList
        End Get
    End Property

    Public ReadOnly Property _AllowToViewEmpBenefitList() As Boolean
        Get
            Return mpreAllowToViewEmpBenefitList
        End Get
    End Property

    Public ReadOnly Property _AllowToViewEmpReferenceList() As Boolean
        Get
            Return mpreAllowToViewEmpReferenceList
        End Get
    End Property

    Public ReadOnly Property _AllowToViewCompanyAssetList() As Boolean
        Get
            Return mpreAllowToViewCompanyAssetList
        End Get
    End Property

    Public ReadOnly Property _AllowToViewEmpSkillList() As Boolean
        Get
            Return mpreAllowToViewEmpSkillList
        End Get
    End Property

    Public ReadOnly Property _AllowToViewEmpQualificationList() As Boolean
        Get
            Return mpreAllowToViewEmpQualificationList
        End Get
    End Property

    Public ReadOnly Property _AllowToViewEmpExperienceList() As Boolean
        Get
            Return mpreAllowToViewEmpExperienceList
        End Get
    End Property

    Public ReadOnly Property _AllowToViewEmpDependantsList() As Boolean
        Get
            Return mpreAllowToViewEmpDependantsList
        End Get
    End Property

    Public ReadOnly Property _AllowToViewChargesProceedingList() As Boolean
        Get
            Return mpreAllowToViewChargesProceedingList
        End Get
    End Property

    Public ReadOnly Property _AllowToViewProceedingApprovalList() As Boolean
        Get
            Return mpreAllowToViewProceedingApprovalList
        End Get
    End Property

    Public ReadOnly Property _AllowToViewDisciplinaryCommitteeList() As Boolean
        Get
            Return mpreAllowToViewDisciplinaryCommitteeList
        End Get
    End Property

    Public ReadOnly Property _AllowToViewLeaveTypeList() As Boolean
        Get
            Return mpreAllowToViewLeaveTypeList
        End Get
    End Property

    Public ReadOnly Property _AllowToViewHolidayList() As Boolean
        Get
            Return mpreAllowToViewHolidayList
        End Get
    End Property

    Public ReadOnly Property _AllowToViewApproverLevelList() As Boolean
        Get
            Return mpreAllowToViewApproverLevelList
        End Get
    End Property

    Public ReadOnly Property _AllowToViewLeaveViewer() As Boolean
        Get
            Return mpreAllowToViewLeaveViewer
        End Get
    End Property

    Public ReadOnly Property _AllowToViewMedicalMasterList() As Boolean
        Get
            Return mpreAllowToViewMedicalMasterList
        End Get
    End Property

    Public ReadOnly Property _AllowToViewServiceProviderList() As Boolean
        Get
            Return mpreAllowToViewServiceProviderList
        End Get
    End Property

    Public ReadOnly Property _AllowToViewMedicalCategoryList() As Boolean
        Get
            Return mpreAllowToViewMedicalCategoryList
        End Get
    End Property

    Public ReadOnly Property _AllowToViewCommonMasterList() As Boolean
        Get
            Return mpreAllowToViewCommonMasterList
        End Get
    End Property

    Public ReadOnly Property _AllowToViewStateList() As Boolean
        Get
            Return mpreAllowToViewStateList
        End Get
    End Property

    Public ReadOnly Property _AllowToViewCityList() As Boolean
        Get
            Return mpreAllowToViewCityList
        End Get
    End Property

    Public ReadOnly Property _AllowToViewZipcodeList() As Boolean
        Get
            Return mpreAllowToViewZipcodeList
        End Get
    End Property

    Public ReadOnly Property _AllowToViewEmpMovementList() As Boolean
        Get
            Return mpreAllowToViewEmpMovementList
        End Get
    End Property

    Public ReadOnly Property _AllowToViewShiftList() As Boolean
        Get
            Return mpreAllowToViewShiftList
        End Get
    End Property

    Public ReadOnly Property _AllowToViewTrainingInstituteList() As Boolean
        Get
            Return mpreAllowToViewTrainingInstituteList
        End Get
    End Property


    'S.SANDEEP [28 MAY 2015] -- START
    'ENHANCEMENT : NEW PRIVILEGE (Performance Assessment)
    'Public ReadOnly Property _AllowToViewSelfAssessmentList() As Boolean
    '    Get
    '        Return mpreAllowToViewSelfAssessmentList
    '    End Get
    'End Property

    'Public ReadOnly Property _AllowToViewAssessorAssessmentList() As Boolean
    '    Get
    '        Return mpreAllowToViewAssessorAssessmentList
    '    End Get
    'End Property

    'Public ReadOnly Property _AllowToViewReviewerAssessmentList() As Boolean
    '    Get
    '        Return mpreAllowToViewReviewerAssessmentList
    '    End Get
    'End Property

    'Public ReadOnly Property _AllowToViewSelfAssessedBSCList() As Boolean
    '    Get
    '        Return mpreAllowToViewSelfAssessedBSCList
    '    End Get
    'End Property

    'Public ReadOnly Property _AllowToViewAssessorAssessedBSCList() As Boolean
    '    Get
    '        Return mpreAllowToViewAssessorAssessedBSCList
    '    End Get
    'End Property

    'Public ReadOnly Property _AllowToViewReviewerAssessedBSCList() As Boolean
    '    Get
    '        Return mpreAllowToViewReviewerAssessedBSCList
    '    End Get
    'End Property
    'S.SANDEEP [28 MAY 2015] -- END


    Public ReadOnly Property _AllowToViewApprisalAnalysisList() As Boolean
        Get
            Return mpreAllowToViewApprisalAnalysisList
        End Get
    End Property

    Public ReadOnly Property _AllowToViewTrainingNeedsAnalysisList() As Boolean
        Get
            Return mpreAllowToViewTrainingNeedsAnalysisList
        End Get
    End Property

    Public ReadOnly Property _AllowToViewTrainingSchedulingList() As Boolean
        Get
            Return mpreAllowToViewTrainingSchedulingList
        End Get
    End Property

    Public ReadOnly Property _AllowToViewTrainingEnrollmentList() As Boolean
        Get
            Return mpreAllowToViewTrainingEnrollmentList
        End Get
    End Property

    Public ReadOnly Property _AllowToViewLevel1EvaluationList() As Boolean
        Get
            Return mpreAllowToViewLevel1EvaluationList
        End Get
    End Property

    Public ReadOnly Property _AllowToViewLevel3EvaluationList() As Boolean
        Get
            Return mpreAllowToViewLevel3EvaluationList
        End Get
    End Property

    Public ReadOnly Property _AllowToViewAssetsDeclarationList() As Boolean
        Get
            Return mpreAllowToViewAssetsDeclarationList
        End Get
    End Property

    Public ReadOnly Property _AllowToViewProcessLoanAdvanceList() As Boolean
        Get
            Return mpreAllowToViewProcessLoanAdvanceList
        End Get
    End Property

    Public ReadOnly Property _AllowToViewLoanAdvanceList() As Boolean
        Get
            Return mpreAllowToViewLoanAdvanceList
        End Get
    End Property

    Public ReadOnly Property _AllowToViewEmployeeSavingsList() As Boolean
        Get
            Return mpreAllowToViewEmployeeSavingsList
        End Get
    End Property

    Public ReadOnly Property _AllowToViewVacancyMasterList() As Boolean
        Get
            Return mpreAllowToViewVancancyMasterList
        End Get
    End Property

    Public ReadOnly Property _AllowToViewApplicantMasterList() As Boolean
        Get
            Return mpreAllowToViewApplicantMasterList
        End Get
    End Property

    Public ReadOnly Property _AllowToViewShortListApplicants() As Boolean
        Get
            Return mpreAllowToViewShortListApplicants
        End Get
    End Property

    Public ReadOnly Property _AllowToViewBatchSchedulingList() As Boolean
        Get
            Return mpreAllowToViewBatchSchedulingList
        End Get
    End Property

    Public ReadOnly Property _AllowToViewInterviewSchedulingList() As Boolean
        Get
            Return mpreAllowToViewInterviewSchedulingList
        End Get
    End Property

    Public ReadOnly Property _AllowToViewFinalApplicantList() As Boolean
        Get
            Return mpreAllowToViewFinalApplicantList
        End Get
    End Property

    Public ReadOnly Property _AllowToViewEmpDistributedCostCenterList() As Boolean
        Get
            Return mpreAllowToViewEmpDistributedCostCenterList
        End Get
    End Property

    Public ReadOnly Property _AllowToViewEmpBankList() As Boolean
        Get
            Return mpreAllowToViewEmpBankList
        End Get
    End Property

    Public ReadOnly Property _AllowToViewTransactionHeadsList() As Boolean
        Get
            Return mpreAllowToViewTransactionHeadsList
        End Get
    End Property

    Public ReadOnly Property _AllowToViewEmpEDList() As Boolean
        Get
            Return mpreAllowToViewEmpEDList
        End Get
    End Property

    Public ReadOnly Property _AllowToViewEmpExemptionList() As Boolean
        Get
            Return mpreAllowToViewEmpExemptionList
        End Get
    End Property

    Public ReadOnly Property _AllowToViewPaySlipList() As Boolean
        Get
            Return mpreAllowToViewPaySlipList
        End Get
    End Property

    Public ReadOnly Property _AllowToViewPaymentList() As Boolean
        Get
            Return mpreAllowToViewPaymentList
        End Get
    End Property

    Public ReadOnly Property _AllowToViewGlobalVoidPaymentList() As Boolean
        Get
            Return mpreAllowToViewGlobalVoidPaymentList
        End Get
    End Property
    'Pinkal (02-Jul-2012) -- End

    Public ReadOnly Property _AllowToPerformForceLogout() As Boolean
        Get
            Return mpreAllowToPerformForceLogout
        End Get
    End Property



    'Pinkal (09-Jul-2012) -- Start
    'Enhancement : TRA Changes

    Public ReadOnly Property _AllowToViewStationList() As Boolean
        Get
            Return mpreAllowToViewStationList
        End Get
    End Property

    Public ReadOnly Property _AllowToViewDeptGroupList() As Boolean
        Get
            Return mpreAllowToViewDeptGroupList
        End Get
    End Property

    Public ReadOnly Property _AllowToViewDepartmentList() As Boolean
        Get
            Return mpreAllowToViewDepartmentList
        End Get
    End Property

    Public ReadOnly Property _AllowToViewSectionGroupList() As Boolean
        Get
            Return mpreAllowToViewSectionGroupList
        End Get
    End Property

    Public ReadOnly Property _AllowToViewSectionList() As Boolean
        Get
            Return mpreAllowToViewSectionList
        End Get
    End Property

    Public ReadOnly Property _AllowToViewUnitGroupList() As Boolean
        Get
            Return mpreAllowToViewUnitGroupList
        End Get
    End Property

    Public ReadOnly Property _AllowToViewUnitList() As Boolean
        Get
            Return mpreAllowToViewUnitList
        End Get
    End Property

    Public ReadOnly Property _AllowToViewTeamList() As Boolean
        Get
            Return mpreAllowToViewTeamList
        End Get
    End Property

    Public ReadOnly Property _AllowToViewJobGroupList() As Boolean
        Get
            Return mpreAllowToViewJobGroupList
        End Get
    End Property

    Public ReadOnly Property _AllowToViewJobList() As Boolean
        Get
            Return mpreAllowToViewJobList
        End Get
    End Property

    Public ReadOnly Property _AllowToViewClassGroupList() As Boolean
        Get
            Return mpreAllowToViewClassGroupList
        End Get
    End Property

    Public ReadOnly Property _AllowToViewClassList() As Boolean
        Get
            Return mpreAllowToViewClassList
        End Get
    End Property

    Public ReadOnly Property _AllowToViewGradeGroupList() As Boolean
        Get
            Return mpreAllowToViewGradeGroupList
        End Get
    End Property

    Public ReadOnly Property _AllowToViewGradeList() As Boolean
        Get
            Return mpreAllowToViewGradeList
        End Get
    End Property

    Public ReadOnly Property _AllowToViewGradeLevelList() As Boolean
        Get
            Return mpreAllowToViewGradeLevelList
        End Get
    End Property

    Public ReadOnly Property _AllowToViewCostCenterList() As Boolean
        Get
            Return mpreAllowToViewCostCenterList
        End Get
    End Property

    Public ReadOnly Property _AllowToViewPayrollPeriodList() As Boolean
        Get
            Return mpreAllowToViewPayrollPeriodList
        End Get
    End Property

    Public ReadOnly Property _AllowToViewSalaryChangeList() As Boolean
        Get
            Return mpreAllowToViewSalaryChangeList
        End Get
    End Property

    Public ReadOnly Property _AllowToViewReasonMasterList() As Boolean
        Get
            Return mpreAllowToViewReasonMasterList
        End Get
    End Property

    Public ReadOnly Property _AllowToViewSkillMasterList() As Boolean
        Get
            Return mpreAllowToViewSkillMasterList
        End Get
    End Property

    Public ReadOnly Property _AllowToViewAgencyMasterList() As Boolean
        Get
            Return mpreAllowToViewAgencyMasterList
        End Get
    End Property

    Public ReadOnly Property _AllowToViewQualificationMasterList() As Boolean
        Get
            Return mpreAllowToViewQualificationMasterList
        End Get
    End Property

    Public ReadOnly Property _AllowToViewResultCodeList() As Boolean
        Get
            Return mpreAllowToViewResultCodeList
        End Get
    End Property

    Public ReadOnly Property _AllowToViewBenefitPlanList() As Boolean
        Get
            Return mpreAllowToViewBenefitPlanList
        End Get
    End Property

    Public ReadOnly Property _AllowToViewDisciplinaryOffencesList() As Boolean
        Get
            Return mpreAllowToViewDisciplinaryOffencesList
        End Get
    End Property

    Public ReadOnly Property _AllowToViewDisciplinaryPenaltiesList() As Boolean
        Get
            Return mpreAllowToViewDisciplinaryPenaltiesList
        End Get
    End Property

    Public ReadOnly Property _AllowToViewDisciplinaryStatusList() As Boolean
        Get
            Return mpreAllowToViewDisciplinaryStatusList
        End Get
    End Property


    'S.SANDEEP [28 MAY 2015] -- START
    'ENHANCEMENT : NEW PRIVILEGE (Performance Assessment)
    'Public ReadOnly Property _AllowToViewAssessmentPeriodList() As Boolean
    '    Get
    '        Return mpreAllowToViewAssessmentPeriodList
    '    End Get
    'End Property

    'Public ReadOnly Property _AllowToViewAssessorAccessList() As Boolean
    '    Get
    '        Return mpreAllowToViewAssessorAccessList
    '    End Get
    'End Property

    'Public ReadOnly Property _AllowToViewReviewerAccessList() As Boolean
    '    Get
    '        Return mpreAllowToViewReviewerAccessList
    '    End Get
    'End Property

    'Public ReadOnly Property _AllowToViewExternalAssessorList() As Boolean
    '    Get
    '        Return mpreAllowToViewExternalAssessorList
    '    End Get
    'End Property

    'Public ReadOnly Property _AllowToViewAssessmentGroupList() As Boolean
    '    Get
    '        Return mpreAllowToViewAssessmentGroupList
    '    End Get
    'End Property

    'Public ReadOnly Property _AllowToViewAssessmentItemList() As Boolean
    '    Get
    '        Return mpreAllowToViewAssessmentItemList
    '    End Get
    'End Property

    'Public ReadOnly Property _AllowToViewAssessmentSubItemList() As Boolean
    '    Get
    '        Return mpreAllowToViewAssessmentSubItemList
    '    End Get
    'End Property

    'Public ReadOnly Property _AllowToViewBSCObjectiveList() As Boolean
    '    Get
    '        Return mpreAllowToViewBSCObjectiveList
    '    End Get
    'End Property

    'Public ReadOnly Property _AllowToViewBSCKPIList() As Boolean
    '    Get
    '        Return mpreAllowToViewBSCKPIList
    '    End Get
    'End Property

    'Public ReadOnly Property _AllowToViewBSCTargetsList() As Boolean
    '    Get
    '        Return mpreAllowToViewBSCTargetsList
    '    End Get
    'End Property

    'Public ReadOnly Property _AllowToViewBSCInitiativeList() As Boolean
    '    Get
    '        Return mpreAllowToViewBSCInitiativeList
    '    End Get
    'End Property
    'S.SANDEEP [28 MAY 2015] -- END


    Public ReadOnly Property _AllowToViewEvaluationGroupList() As Boolean
        Get
            Return mpreAllowToViewEvaluationGroupList
        End Get
    End Property

    Public ReadOnly Property _AllowToViewEvaluationItemList() As Boolean
        Get
            Return mpreAllowToViewEvaluationItemList
        End Get
    End Property

    Public ReadOnly Property _AllowToViewEvaluationSubItemList() As Boolean
        Get
            Return mpreAllowToViewEvaluationSubItemList
        End Get
    End Property

    Public ReadOnly Property _AllowToViewEvaluationIIIitemList() As Boolean
        Get
            Return mpreAllowToViewEvaluationIIIitemList
        End Get
    End Property

    Public ReadOnly Property _AllowToViewPayrollGroupList() As Boolean
        Get
            Return mpreAllowToViewPayrollGroupList
        End Get
    End Property

    Public ReadOnly Property _AllowToViewPayPointList() As Boolean
        Get
            Return mpreAllowToViewPayPointList
        End Get
    End Property

    Public ReadOnly Property _AllowToViewBankBranchList() As Boolean
        Get
            Return mpreAllowToViewBankBranchList
        End Get
    End Property

    Public ReadOnly Property _AllowToViewBankAccountTypeList() As Boolean
        Get
            Return mpreAllowToViewBankAccountTypeList
        End Get
    End Property

    Public ReadOnly Property _AllowToViewBankEDIList() As Boolean
        Get
            Return mpreAllowToViewBankEDIList
        End Get
    End Property

    Public ReadOnly Property _AllowToViewCurrencyList() As Boolean
        Get
            Return mpreAllowToViewCurrencyList
        End Get
    End Property

    Public ReadOnly Property _AllowToViewCurrencyDenominationList() As Boolean
        Get
            Return mpreAllowToViewCurrencyDenominationList
        End Get
    End Property

    Public ReadOnly Property _AllowToViewPayslipMessageList() As Boolean
        Get
            Return mpreAllowToViewPayslipMessageList
        End Get
    End Property

    Public ReadOnly Property _AllowToViewAccountList() As Boolean
        Get
            Return mpreAllowToViewAccountList
        End Get
    End Property

    Public ReadOnly Property _AllowToViewCompanyAccountConfigurationList() As Boolean
        Get
            Return mpreAllowToViewCompanyAccountConfigurationList
        End Get
    End Property

    Public ReadOnly Property _AllowToViewEmployeeAccountConfigurationList() As Boolean
        Get
            Return mpreAllowToViewEmployeeAccountConfigurationList
        End Get
    End Property

    Public ReadOnly Property _AllowToViewCostcenterAccountConfigurationList() As Boolean
        Get
            Return mpreAllowToViewCostcenterAccountConfigurationList
        End Get
    End Property

    Public ReadOnly Property _AllowToViewBatchTransactionList() As Boolean
        Get
            Return mpreAllowToViewBatchTransactionList
        End Get
    End Property

    Public ReadOnly Property _AllowToViewCashDenominationList() As Boolean
        Get
            Return mpreAllowToViewCashDenominationList
        End Get
    End Property

    Public ReadOnly Property _AllowToViewLoanSchemeList() As Boolean
        Get
            Return mpreAllowToViewLoanSchemeList
        End Get
    End Property

    Public ReadOnly Property _AllowToViewSavingSchemeList() As Boolean
        Get
            Return mpreAllowToViewSavingSchemeList
        End Get
    End Property
    'Pinkal (09-Jul-2012) -- End

    'S.SANDEEP [ 17 AUG 2012 ] -- START
    'ENHANCEMENT : TRA CHANGES
    Public ReadOnly Property _AllowToViewApplicantReferenceNo() As Boolean
        Get
            Return mpreAllowToViewApplicantReferenceNo
        End Get
    End Property
    'S.SANDEEP [ 17 AUG 2012 ] -- END


    'Anjan (03 Oct 2012)-Start
    Public ReadOnly Property _AddTeam() As Boolean
        Get
            Return mpreAddTeam
        End Get
    End Property

    Public ReadOnly Property _EditTeam() As Boolean
        Get
            Return mpreEditTeam
        End Get
    End Property

    Public ReadOnly Property _DeleteTeam() As Boolean
        Get
            Return mpreDeleteTeam
        End Get
    End Property


    Public ReadOnly Property _AddSectionGroup() As Boolean
        Get
            Return mpreAddSectionGroup
        End Get
    End Property

    Public ReadOnly Property _EditSectionGroup() As Boolean
        Get
            Return mpreEditSectionGroup
        End Get
    End Property

    Public ReadOnly Property _DeleteSectionGroup() As Boolean
        Get
            Return mpreDeleteSectionGroup
        End Get
    End Property


    Public ReadOnly Property _AddUnitGroup() As Boolean
        Get
            Return mpreAddUnitGroup
        End Get
    End Property

    Public ReadOnly Property _EditUnitGroup() As Boolean
        Get
            Return mpreEditUnitGroup
        End Get
    End Property

    Public ReadOnly Property _DeleteUnitGroup() As Boolean
        Get
            Return mpreDeleteUnitGroup
        End Get
    End Property
    'Anjan (03 Oct 2012)-End 


    'S.SANDEEP [ 08 OCT 2012 ] -- START
    'ENHANCEMENT : TRA CHANGES
    Public ReadOnly Property _EditImportedEmployee() As Boolean
        Get
            Return mpreEditImportedEmployee
        End Get
    End Property
    'S.SANDEEP [ 08 OCT 2012 ] -- END

    'S.SANDEEP [ 29 OCT 2012 ] -- START
    'ENHANCEMENT : TRA CHANGES
    Public ReadOnly Property _AllowtoMapLoanApprover() As Boolean
        Get
            Return mpreAllowtoMapLoanApprover
        End Get
    End Property

    Public ReadOnly Property _AllowtoImportLoan_Advance() As Boolean
        Get
            Return mpreAllowtoImportLoan_Advance
        End Get
    End Property

    Public ReadOnly Property _AllowtoImportSavings() As Boolean
        Get
            Return mpreAllowtoImportSavings
        End Get
    End Property

    Public ReadOnly Property _AllowtoImportAccrueLeave() As Boolean
        Get
            Return mpreAllowtoImportAccrueLeave
        End Get
    End Property

    Public ReadOnly Property _AllowtoMigrateLeaveApprover() As Boolean
        Get
            Return mpreAllowtoMigrateLeaveApprover
        End Get
    End Property

    Public ReadOnly Property _AllowtoImportAttendanceData() As Boolean
        Get
            Return mpreAllowtoImportAttendanceData
        End Get
    End Property

    Public ReadOnly Property _AllowtoMakeGroupAttendance() As Boolean
        Get
            Return mpreAllowtoMakeGroupAttendance
        End Get
    End Property

    Public ReadOnly Property _AllowtoAddGeneralReminder() As Boolean
        Get
            Return mpreAllowtoAddGeneralReminder
        End Get
    End Property

    Public ReadOnly Property _AllowtoEditGeneralReminder() As Boolean
        Get
            Return mpreAllowtoEditGeneralReminder
        End Get
    End Property

    Public ReadOnly Property _AllowtoDeleteGeneralReminder() As Boolean
        Get
            Return mpreAllowtoDeleteGeneralReminder
        End Get
    End Property

    Public ReadOnly Property _AllowtoChangeUser() As Boolean
        Get
            Return mpreAllowtoChangeUser
        End Get
    End Property

    Public ReadOnly Property _AllowtoChangeCompany() As Boolean
        Get
            Return mpreAllowtoChangeCompany
        End Get
    End Property

    Public ReadOnly Property _AllowtoChangeDatabase() As Boolean
        Get
            Return mpreAllowtoChangeDatabase
        End Get
    End Property

    Public ReadOnly Property _AllowtoChangePassword() As Boolean
        Get
            Return mpreAllowtoChangePassword
        End Get
    End Property

    Public ReadOnly Property _AllowtoViewSentItems() As Boolean
        Get
            Return mpreAllowtoViewSentItems
        End Get
    End Property

    Public ReadOnly Property _AllowtoViewTrashItems() As Boolean
        Get
            Return mpreAllowtoViewTrashItems
        End Get
    End Property

    Public ReadOnly Property _AllowtoPrintLetters() As Boolean
        Get
            Return mpreAllowtoPrintLetters
        End Get
    End Property

    Public ReadOnly Property _AllowtoExportLetters() As Boolean
        Get
            Return mpreAllowtoExportLetters
        End Get
    End Property

    Public ReadOnly Property _AllowtoReadMails() As Boolean
        Get
            Return mpreAllowtoReadMails
        End Get
    End Property

    Public ReadOnly Property _AllowtoViewAuditTrails() As Boolean
        Get
            Return mpreAllowtoViewAuditTrails
        End Get
    End Property

    Public ReadOnly Property _AllowtoViewApplicationEvtLog() As Boolean
        Get
            Return mpreAllowtoViewApplicationEvtLog
        End Get
    End Property

    Public ReadOnly Property _AllowtoViewUserAuthLog() As Boolean
        Get
            Return mpreAllowtoViewUserAuthLog
        End Get
    End Property

    Public ReadOnly Property _AllowtoViewUserAttemptsLog() As Boolean
        Get
            Return mpreAllowtoViewUserAttemptsLog
        End Get
    End Property

    Public ReadOnly Property _AllowtoViewAuditLogs() As Boolean
        Get
            Return mpreAllowtoViewAuditLogs
        End Get
    End Property

    Public ReadOnly Property _AllowtoUnlockUser() As Boolean
        Get
            Return mpreAllowtoUnlockUser
        End Get
    End Property

    Public ReadOnly Property _AllowtoExportAbilityLevel() As Boolean
        Get
            Return mpreAllowtoExportAbilityLevel
        End Get
    End Property

    Public ReadOnly Property _AllowtoSaveAbilityLevel() As Boolean
        Get
            Return mpreAllowtoSaveAbilityLevel
        End Get
    End Property

    Public ReadOnly Property _AllowtoTakeDatabaseBackup() As Boolean
        Get
            Return mpreAllowtoTakeDatabaseBackup
        End Get
    End Property

    Public ReadOnly Property _AllowtoRestoreDatabase() As Boolean
        Get
            Return mpreAllowtoRestoreDatabase
        End Get
    End Property

    Public ReadOnly Property _AllowtoAddVoidReason() As Boolean
        Get
            Return mpreAllowtoAddVoidReason
        End Get
    End Property

    Public ReadOnly Property _AllowtoEditVoidReason() As Boolean
        Get
            Return mpreAllowtoEditVoidReason
        End Get
    End Property

    Public ReadOnly Property _AllowtoDeleteVoidReason() As Boolean
        Get
            Return mpreAllowtoDeleteVoidReason
        End Get
    End Property

    Public ReadOnly Property _AllowtoAddReminderType() As Boolean
        Get
            Return mpreAllowtoAddReminderType
        End Get
    End Property

    Public ReadOnly Property _AllowtoEditReminderType() As Boolean
        Get
            Return mpreAllowtoEditReminderType
        End Get
    End Property

    Public ReadOnly Property _AllowtoDeleteReminderType() As Boolean
        Get
            Return mpreAllowtoDeleteReminderType
        End Get
    End Property

    Public ReadOnly Property _AllowtoSavePasswordOption() As Boolean
        Get
            Return mpreAllowtoSavePasswordOption
        End Get
    End Property

    Public ReadOnly Property _AllowtoAddDeviceSettings() As Boolean
        Get
            Return mpreAllowtoAddDeviceSettings
        End Get
    End Property

    Public ReadOnly Property _AllowtoEditDeviceSettings() As Boolean
        Get
            Return mpreAllowtoEditDeviceSettings
        End Get
    End Property

    Public ReadOnly Property _AllowtoDeleteDeviceSettings() As Boolean
        Get
            Return mpreAllowtoDeleteDeviceSettings
        End Get
    End Property

    Public ReadOnly Property _AllowtoChangeCompanyOptions() As Boolean
        Get
            Return mpreAllowtoChangeCompanyOptions
        End Get
    End Property

    Public ReadOnly Property _AllowtoSaveCompanyGroup() As Boolean
        Get
            Return mpreAllowtoSaveCompanyGroup
        End Get
    End Property
    'S.SANDEEP [ 29 OCT 2012 ] -- END



    'Pinkal (19-Nov-2012) -- Start
    'Enhancement : TRA Changes


    Public ReadOnly Property _AddLeaveExpense() As Boolean
        Get
            Return mpreAddLeaveExpense
        End Get
    End Property

    Public ReadOnly Property _EditLeaveExpense() As Boolean
        Get
            Return mpreEditLeaveExpense
        End Get
    End Property

    Public ReadOnly Property _DeleteLeaveExpense() As Boolean
        Get
            Return mpreDeleteLeaveExpense
        End Get
    End Property

    'Pinkal (19-Nov-2012) -- End

    'S.SANDEEP [ 23 NOV 2012 ] -- START
    'ENHANCEMENT : TRA CHANGES
    Public ReadOnly Property _ImportADUsers() As Boolean
        Get
            Return mpreImportADUsers
        End Get
    End Property

    Public ReadOnly Property _AssignUserAccess() As Boolean
        Get
            Return mpreAssignUserAccess
        End Get
    End Property

    Public ReadOnly Property _EditUserAccess() As Boolean
        Get
            Return mpreEditUserAccess
        End Get
    End Property

    Public ReadOnly Property _ExportPrivilegs() As Boolean
        Get
            Return mpreExportPrivilegs
        End Get
    End Property
    'S.SANDEEP [ 23 NOV 2012 ] -- END



    'Pinkal (22-Nov-2012) -- Start
    'Enhancement : TRA Changes

    Public ReadOnly Property _AllowtoChangeBranch() As Boolean
        Get
            Return mpreAllowtoChangeBranch
        End Get
    End Property

    Public ReadOnly Property _AllowtoChangeDepartmentGroup() As Boolean
        Get
            Return mpreAllowtoChangeDepartmentGroup
        End Get
    End Property

    Public ReadOnly Property _AllowtoChangeDepartment() As Boolean
        Get
            Return mpreAllowtoChangeDepartment
        End Get
    End Property

    Public ReadOnly Property _AllowtoChangeSectionGroup() As Boolean
        Get
            Return mpreAllowtoChangeSectionGroup
        End Get
    End Property

    Public ReadOnly Property _AllowtoChangeSection() As Boolean
        Get
            Return mpreAllowtoChangeSection
        End Get
    End Property

    Public ReadOnly Property _AllowtoChangeUnitGroup() As Boolean
        Get
            Return mpreAllowtoChangeUnitGroup
        End Get
    End Property

    Public ReadOnly Property _AllowtoChangeUnit() As Boolean
        Get
            Return mpreAllowtoChangeUnit
        End Get
    End Property

    Public ReadOnly Property _AllowtoChangeTeam() As Boolean
        Get
            Return mpreAllowtoChangeTeam
        End Get
    End Property

    Public ReadOnly Property _AllowtoChangeJobGroup() As Boolean
        Get
            Return mpreAllowtoChangeJobGroup
        End Get
    End Property

    Public ReadOnly Property _AllowtoChangeJob() As Boolean
        Get
            Return mpreAllowtoChangeJob
        End Get
    End Property

    Public ReadOnly Property _AllowtoChangeGradeGroup() As Boolean
        Get
            Return mpreAllowtoChangeGradeGroup
        End Get
    End Property

    Public ReadOnly Property _AllowtoChangeGrade() As Boolean
        Get
            Return mpreAllowtoChangeGrade
        End Get
    End Property

    Public ReadOnly Property _AllowtoChangeGradeLevel() As Boolean
        Get
            Return mpreAllowtoChangeGradeLevel
        End Get
    End Property

    Public ReadOnly Property _AllowtoChangeClassGroup() As Boolean
        Get
            Return mpreAllowtoChangeClassGroup
        End Get
    End Property

    Public ReadOnly Property _AllowtoChangeClass() As Boolean
        Get
            Return mpreAllowtoChangeClass
        End Get
    End Property

    Public ReadOnly Property _AllowtoChangeCostCenter() As Boolean
        Get
            Return mpreAllowtoChangeCostCenter
        End Get
    End Property

    'Pinkal (22-Nov-2012) -- End



    'Pinkal (01-Dec-2012) -- Start
    'Enhancement : TRA Changes

    Public ReadOnly Property _AllowtoChangeCompanyEmail() As Boolean
        Get
            Return mpreAllowtoChangeCompanyEmail
        End Get
    End Property


    'Pinkal (01-Dec-2012) -- End

    'S.SANDEEP [ 01 DEC 2012 ] -- START
    'ENHANCEMENT : TRA CHANGES
    Public ReadOnly Property _AllowtoImportEmployee_User() As Boolean
        Get
            Return mpreAllowtoImportEmployee_User
        End Get
    End Property
    'S.SANDEEP [ 01 DEC 2012 ] -- END

    'S.SANDEEP [ 13 DEC 2012 ] -- START
    'ENHANCEMENT : TRA CHANGES
    Public ReadOnly Property _AllowToViewMembershipOnDiary() As Boolean
        Get
            Return mpreAllowToViewMembershipOnDiary
        End Get
    End Property

    Public ReadOnly Property _AllowToViewBanksOnDiary() As Boolean
        Get
            Return mpreAllowToViewBanksOnDiary
        End Get
    End Property
    'S.SANDEEP [ 13 DEC 2012 ] -- END


    'Pinkal (18-Dec-2012) -- Start
    'Enhancement : TRA Changes

    Public ReadOnly Property _AllowToAssignIssueUsertoEmp() As Boolean
        Get
            Return mpreAllowToAssignIssueUsertoEmp
        End Get
    End Property

    Public ReadOnly Property _AllowToMigrateIssueUser() As Boolean
        Get
            Return mpreAllowToMigrateIssueUser
        End Get
    End Property

    'Pinkal (18-Dec-2012) -- End

    'Sohail (13 Feb 2013) -- Start
    'TRA - ENHANCEMENT
    Public ReadOnly Property _AllowToAddPaymentApproverLevel() As Boolean
        Get
            Return mpreAllowToAddPaymentApproverLevel
        End Get
    End Property

    Public ReadOnly Property _AllowToEditPaymentApproverLevel() As Boolean
        Get
            Return mpreAllowToEditPaymentApproverLevel
        End Get
    End Property

    Public ReadOnly Property _AllowToDeletePaymentApproverLevel() As Boolean
        Get
            Return mpreAllowToDeletePaymentApproverLevel
        End Get
    End Property

    Public ReadOnly Property _AllowToViewPaymentApproverLevelList() As Boolean
        Get
            Return mpreAllowToViewPaymentApproverLevelList
        End Get
    End Property
    'Sohail (13 Feb 2013) -- End

    'Sohail (29 Oct 2014) -- Start
    'AKF Enhancement - Inactive option on Payment Approver Level.
    Public ReadOnly Property _AllowToSetPaymentApproverLevelActive() As Boolean
        Get
            Return mpreAllowToSetPaymentApproverLevelActive
        End Get
    End Property
    'Sohail (29 Oct 2014) -- End

    'S.SANDEEP [ 13 FEB 2013 ] -- START
    'ENHANCEMENT : TRA CHANGES
    Public ReadOnly Property _AllowToAddPaymentApproverMapping() As Boolean
        Get
            Return mpreAllowToAddPaymentApproverMapping
        End Get
    End Property

    Public ReadOnly Property _AllowToEditPaymentApproverMapping() As Boolean
        Get
            Return mpreAllowToEditPaymentApproverMapping
        End Get
    End Property

    Public ReadOnly Property _AllowToDeletePaymentApproverMapping() As Boolean
        Get
            Return mpreAllowToDeletePaymentApproverMapping
        End Get
    End Property

    Public ReadOnly Property _AllowToApprovePayment() As Boolean
        Get
            Return mpreAllowToApprovePayment
        End Get
    End Property
    'S.SANDEEP [ 13 FEB 2013 ] -- END

    'Sohail (13 Feb 2013) -- Start
    'TRA - ENHANCEMENT
    Public ReadOnly Property _AllowToVoidApprovedPayment() As Boolean
        Get
            Return mpreAllowToVoidApprovedPayment
        End Get
    End Property
    'Sohail (13 Feb 2013) -- End

    'Sohail (28 May 2014) -- Start
    'Enhancement - Staff Requisition.
    Public ReadOnly Property _AllowToAddStaffRequisitionApproverLevel() As Boolean
        Get
            Return mpreAllowToAddStaffRequisitionApproverLevel
        End Get
    End Property

    Public ReadOnly Property _AllowToEditStaffRequisitionApproverLevel() As Boolean
        Get
            Return mpreAllowToEditStaffRequisitionApproverLevel
        End Get
    End Property

    Public ReadOnly Property _AllowToDeleteStaffRequisitionApproverLevel() As Boolean
        Get
            Return mpreAllowToDeleteStaffRequisitionApproverLevel
        End Get
    End Property

    Public ReadOnly Property _AllowToViewStaffRequisitionApproverLevelList() As Boolean
        Get
            Return mpreAllowToViewStaffRequisitionApproverLevelList
        End Get
    End Property

    Public ReadOnly Property _AllowToAddStaffRequisitionApproverMapping() As Boolean
        Get
            Return mpreAllowToAddStaffRequisitionApproverMapping
        End Get
    End Property

    Public ReadOnly Property _AllowToEditStaffRequisitionApproverMapping() As Boolean
        Get
            Return mpreAllowToEditStaffRequisitionApproverMapping
        End Get
    End Property

    Public ReadOnly Property _AllowToDeleteStaffRequisitionApproverMapping() As Boolean
        Get
            Return mpreAllowToDeleteStaffRequisitionApproverMapping
        End Get
    End Property

    Public ReadOnly Property _AllowToViewStaffRequisitionApproverMappingList() As Boolean
        Get
            Return mpreAllowToViewStaffRequisitionApproverMappingList
        End Get
    End Property

    Public ReadOnly Property _AllowToAddStaffRequisition() As Boolean
        Get
            Return mpreAllowToAddStaffRequisition
        End Get
    End Property

    Public ReadOnly Property _AllowToEditStaffRequisition() As Boolean
        Get
            Return mpreAllowToEditStaffRequisition
        End Get
    End Property

    Public ReadOnly Property _AllowToDeleteStaffRequisition() As Boolean
        Get
            Return mpreAllowToDeleteStaffRequisition
        End Get
    End Property

    Public ReadOnly Property _AllowToViewStaffRequisitionList() As Boolean
        Get
            Return mpreAllowToViewStaffRequisitionList
        End Get
    End Property

    Public ReadOnly Property _AllowToApproveStaffRequisition() As Boolean
        Get
            Return mpreAllowToApproveStaffRequisition
        End Get
    End Property

    Public ReadOnly Property _AllowToCancelStaffRequisition() As Boolean
        Get
            Return mpreAllowToCancelStaffRequisition
        End Get
    End Property

    Public ReadOnly Property _AllowToViewStaffRequisitionApprovals() As Boolean
        Get
            Return mpreAllowToViewStaffRequisitionApprovals
        End Get
    End Property
    'Sohail (28 May 2014) -- End


    'Sohail (05 Aug 2014) -- Start
    'Enhancement - Statutory Payment Priviledge.
    Public ReadOnly Property _AllowToAddStatutoryPayment() As Boolean
        Get
            Return mpreAllowToAddStatutoryPayment
        End Get
    End Property

    Public ReadOnly Property _AllowToEditStatutoryPayment() As Boolean
        Get
            Return mpreAllowToEditStatutoryPayment
        End Get
    End Property

    Public ReadOnly Property _AllowToDeleteStatutoryPayment() As Boolean
        Get
            Return mpreAllowToDeleteStatutoryPayment
        End Get
    End Property

    Public ReadOnly Property _AllowToViewStatutoryPaymentList() As Boolean
        Get
            Return mpreAllowToViewStatutoryPaymentList
        End Get
    End Property
    'Sohail (05 Aug 2014) -- End

    'Pinkal (06-Feb-2013) -- Start
    'Enhancement : TRA Changes

    Public ReadOnly Property _AllowToEndLeaveCycle() As Boolean
        Get
            Return mpreAllowToEndLeaveCycle
        End Get
    End Property

    'Pinkal (06-Feb-2013) -- End

    'Pinkal (06-Mar-2013) -- Start
    'Enhancement : TRA Changes

    Public ReadOnly Property _AllowToAddMedicalDependantException() As Boolean
        Get
            Return mpreAllowToAddMedicalDependantException
        End Get
    End Property

    'Pinkal (06-Mar-2013) -- End


    'Pinkal (01-Apr-2013) -- Start
    'Enhancement : TRA Changes

    Public ReadOnly Property _AllowToAddEditPhoto() As Boolean
        Get
            Return mpreAllowToAddEditPhoto
        End Get
    End Property

    Public ReadOnly Property _AllowToDeletePhoto() As Boolean
        Get
            Return mpreAllowToDeletePhoto
        End Get
    End Property

    Public ReadOnly Property _AllowToImportPhoto() As Boolean
        Get
            Return mpreAllowToImportPhoto
        End Get
    End Property

    'Pinkal (01-Apr-2013) -- End

    'S.SANDEEP [ 10 APR 2013 ] -- START
    'ENHANCEMENT : TRA CHANGES

    'S.SANDEEP [28 MAY 2015] -- START
    'ENHANCEMENT : NEW PRIVILEGE (Performance Assessment)
    'Public ReadOnly Property _AllowToSubmitBSCPlan_Approval() As Boolean
    '    Get
    '        Return mpreAllowToSubmitBSCPlan_Approval
    '    End Get
    'End Property
    'S.SANDEEP [28 MAY 2015] -- END

    'S.SANDEEP [ 10 APR 2013 ] -- END

    'S.SANDEEP [ 14 May 2013 ] -- START
    'ENHANCEMENT : TRA ENHANCEMENT
    Public ReadOnly Property _AllowToSubmitApplicantFilter_Approval() As Boolean
        Get
            Return mpreAllowToSubmitApplicantFilter_Approval
        End Get
    End Property

    Public ReadOnly Property _AllowToApproveApplicantFilter() As Boolean
        Get
            Return mpreAllowToApproveApplicantFilter
        End Get
    End Property

    Public ReadOnly Property _AllowToRejectApplicantFilter() As Boolean
        Get
            Return mpreAllowToRejectApplicantFilter
        End Get
    End Property

    Public ReadOnly Property _AllowToAddApplicantFilter() As Boolean
        Get
            Return mpreAllowToAddApplicantFilter
        End Get
    End Property

    Public ReadOnly Property _AllowToDeleteApplicantFilter() As Boolean
        Get
            Return mpreAllowToDeleteApplicantFilter
        End Get
    End Property

    Public ReadOnly Property _AllowtoSubmitApplicantEligibilityForApproval() As Boolean
        Get
            Return mpreAllowtoSubmitApplicantEligibilityForApproval
        End Get
    End Property

    Public ReadOnly Property _AllowtoApproveApplicantEligibility() As Boolean
        Get
            Return mpreAllowtoApproveApplicantEligibility
        End Get
    End Property

    Public ReadOnly Property _AllowtoDisapproveApplicantEligibility() As Boolean
        Get
            Return mpreAllowtoDisapproveApplicantEligibility
        End Get
    End Property
    'S.SANDEEP [ 14 May 2013 ] -- END

    'Pinkal (12-May-2013) -- Start
    'Enhancement : TRA Changes
    Public ReadOnly Property _AllowToSubmitForApprovalInFinalShortlistedApplicant() As Boolean
        Get
            Return mpreAllowToSubmitForApprovalInFinalShortlistedApplicant
        End Get
    End Property

    Public ReadOnly Property _AllowToApproveFinalShortListedApplicant() As Boolean
        Get
            Return mpreAllowToApproveFinalShortListedApplicant
        End Get
    End Property

    Public ReadOnly Property _AllowToDisapproveFinalShortListedApplicant() As Boolean
        Get
            Return mpreAllowToDisapproveFinalShortListedApplicant
        End Get
    End Property
    'Pinkal (12-May-2013) -- End


    'Pinkal (03-Jul-2013) -- Start
    'Enhancement : TRA Changes

    Public ReadOnly Property _AllowToMoveFinalShortListedApplicantToShortListed() As Boolean
        Get
            Return mpreAllowToMoveFinalShortListedApplicantToShortListed
        End Get
    End Property

    'Pinkal (03-Jul-2013) -- End

    'S.SANDEEP [ 10 JUNE 2013 ] -- START
    'ENHANCEMENT : OTHER CHANGES
    Public ReadOnly Property _AllowtoAddPayActivity() As Boolean
        Get
            Return mpreAllowtoAddPayActivity
        End Get
    End Property

    Public ReadOnly Property _AllowtoEditPayActivity() As Boolean
        Get
            Return mpreAllowtoEditPayActivity
        End Get
    End Property

    Public ReadOnly Property _AllowtoDeletePayActivity() As Boolean
        Get
            Return mpreAllowtoDeletePayActivity
        End Get
    End Property

    Public ReadOnly Property _AllowtoAddUnitOfMeasure() As Boolean
        Get
            Return mpreAllowtoAddUnitOfMeasure
        End Get
    End Property

    Public ReadOnly Property _AllowtoEditUnitOfMeasure() As Boolean
        Get
            Return mpreAllowtoEditUnitOfMeasure
        End Get
    End Property

    Public ReadOnly Property _AllowtoDeleteUnitOfMeasure() As Boolean
        Get
            Return mpreAllowtoDeleteUnitOfMeasure
        End Get
    End Property

    Public ReadOnly Property _AllowtoAdd_EditActivityRate() As Boolean
        Get
            Return mpreAllowtoAdd_EditActivityRate
        End Get
    End Property

    Public ReadOnly Property _AllowtoPostActivitytoPayroll() As Boolean
        Get
            Return mpreAllowtoPostActivitytoPayroll
        End Get
    End Property

    Public ReadOnly Property _AllowtoVoidActivtyfromPayroll() As Boolean
        Get
            Return mpreAllowtoVoidActivtyfromPayroll
        End Get
    End Property

    Public ReadOnly Property _AllowtoAdd_EditPayPerActivity() As Boolean
        Get
            Return mpreAllowtoAdd_EditPayPerActivity
        End Get
    End Property

    Public ReadOnly Property _AllowtoDeletePayPerActivity() As Boolean
        Get
            Return mpreAllowtoDeletePayPerActivity
        End Get
    End Property

    Public ReadOnly Property _AllowtoExportPayPerActivity() As Boolean
        Get
            Return mpreAllowtoExportPayPerActivity
        End Get
    End Property

    Public ReadOnly Property _AllowtoImportPayPerActivity() As Boolean
        Get
            Return mpreAllowtoImportPayPerActivity
        End Get
    End Property

    Public ReadOnly Property _AllowtoAssignPayPerActivityGlobally() As Boolean
        Get
            Return mpreAllowtoAssignPayPerActivityGlobally
        End Get
    End Property

    Public ReadOnly Property _AllowtoviewActivityList() As Boolean
        Get
            Return mpreAllowtoviewActivityList
        End Get
    End Property

    Public ReadOnly Property _AllowtoViewMeasureList() As Boolean
        Get
            Return mpreAllowtoViewMeasureList
        End Get
    End Property
    'S.SANDEEP [ 10 JUNE 2013 ] -- END


    'Pinkal (24-Aug-2013) -- Start
    'Enhancement : TRA Changes

    Public ReadOnly Property _AllowToImportEmpIdentity() As Boolean
        Get
            Return mpreAllowToImportEmpIdentity
        End Get
    End Property

    'Pinkal (24-Aug-2013) -- End


    'Pinkal (30-Dec-2013) -- Start
    'Enhancement : Oman Changes

    Public ReadOnly Property _AllowToEditTimesheetCard() As Boolean
        Get
            Return mpreAllowToEditTimesheetCard
        End Get
    End Property

    Public ReadOnly Property _AllowToAddEditDeleteGlobalTimesheet() As Boolean
        Get
            Return mpreAllowToAddEditDeleteGlobalTimesheet
        End Get
    End Property

    Public ReadOnly Property _AllowToReCalculateTimings() As Boolean
        Get
            Return mpreAllowToReCalculateTimings
        End Get
    End Property

    Public ReadOnly Property _AllowToRoundoffTimings() As Boolean
        Get
            Return mpreAllowToRoundoffTimings
        End Get
    End Property

    'Pinkal (30-Dec-2013) -- End

    'S.SANDEEP [ 28 JAN 2014 ] -- START
    Public ReadOnly Property _AllowToDeleteEmailNotificationAuditTrails() As Boolean
        Get
            Return mpreAllowToDeleteEmailNotificationAuditTrails
        End Get
    End Property
    'S.SANDEEP [ 28 JAN 2014 ] -- END



    'Pinkal (06-May-2014) -- Start
    'Enhancement : TRA Changes

    Public ReadOnly Property _AllowToChangeOtherUserPassword() As Boolean
        Get
            Return mpreAllowToChangeOtherUserPassword
        End Get
    End Property

    'Pinkal (06-May-2014) -- End

    'Pinkal (12-Sep-2014) -- Start
    'Medical Enhancement - TANAPA URGENT CHANGES FOR MEDICAL SICK SHEET GIVEN BY OLAIS
    Public ReadOnly Property _RevokeUserAccessOnSicksheet() As Boolean
        Get
            Return mpreRevokeUserAccessonSicksheet
        End Get
    End Property
    'Pinkal (12-Sep-2014) -- End

    'S.SANDEEP [ 17 OCT 2014 ] -- START
    Public ReadOnly Property _AllowtoDeletePayPerActivityAuditTrails() As Boolean
        Get
            Return mpreAllowtoDeletePayPerActivityAuditTrails
        End Get
    End Property
    'S.SANDEEP [ 17 OCT 2014 ] -- END


    'Anjan [06 April 2015] -- Start
    'ENHANCEMENT : Implementing Peoplesoft integration,requested by Andrew.
    Public ReadOnly Property _AllowtoImportPeopleSoftData() As Boolean
        Get
            Return mpreAllowImportPeoplesoftData
        End Get
    End Property
    'Anjan [06 April 2015] -- End


    'S.SANDEEP [28 MAY 2015] -- START
    'ENHANCEMENT : NEW PRIVILEGE (Performance Assessment)
    Public ReadOnly Property _AllowtoAddAssessmentPeriod() As Boolean
        Get
            Return mpreAllowtoAddAssessmentPeriod
        End Get
    End Property

    Public ReadOnly Property _AllowtoEditAssessmentPeriod() As Boolean
        Get
            Return mpreAllowtoEditAssessmentPeriod
        End Get
    End Property

    Public ReadOnly Property _AllowtoDeleteAssessmentPeriod() As Boolean
        Get
            Return mpreAllowtoDeleteAssessmentPeriod
        End Get
    End Property

    Public ReadOnly Property _AllowtoAddAssessorAccess() As Boolean
        Get
            Return mpreAllowtoAddAssessorAccess
        End Get
    End Property

    Public ReadOnly Property _AllowtoEditAssessorAccess() As Boolean
        Get
            Return mpreAllowtoEditAssessorAccess
        End Get
    End Property

    Public ReadOnly Property _AllowtoDeleteAssessorAccess() As Boolean
        Get
            Return mpreAllowtoDeleteAssessorAccess
        End Get
    End Property

    Public ReadOnly Property _AllowtoImportAssessor() As Boolean
        Get
            Return mpreAllowtoImportAssessor
        End Get
    End Property

    Public ReadOnly Property _AllowtoAddReviewerAccess() As Boolean
        Get
            Return mpreAllowtoAddReviewerAccess
        End Get
    End Property

    Public ReadOnly Property _AllowtoEditReviewerAccess() As Boolean
        Get
            Return mpreAllowtoEditReviewerAccess
        End Get
    End Property

    Public ReadOnly Property _AllowtoDeleteReviewerAccess() As Boolean
        Get
            Return mpreAllowtoDeleteReviewerAccess
        End Get
    End Property

    Public ReadOnly Property _AllowtoImportReviewer() As Boolean
        Get
            Return mpreAllowtoImportReviewer
        End Get
    End Property

    Public ReadOnly Property _AllowtoPerformAssessorReviewerMigration() As Boolean
        Get
            Return mpreAllowtoPerformAssessorReviewerMigration
        End Get
    End Property

    Public ReadOnly Property _AllowtoAddAssessmentRatios() As Boolean
        Get
            Return mpreAllowtoAddAssessmentRatios
        End Get
    End Property

    Public ReadOnly Property _AllowtoEditAssessmentRatios() As Boolean
        Get
            Return mpreAllowtoEditAssessmentRatios
        End Get
    End Property

    Public ReadOnly Property _AllowtoDeleteAssessmentRatios() As Boolean
        Get
            Return mpreAllowtoDeleteAssessmentRatios
        End Get
    End Property

    Public ReadOnly Property _AllowtoCloseAssessmentPeriod() As Boolean
        Get
            Return mpreAllowtoCloseAssessmentPeriod
        End Get
    End Property

    Public ReadOnly Property _AllowtoAddAssessmentGroup() As Boolean
        Get
            Return mpreAllowtoAddAssessmentGroup
        End Get
    End Property

    Public ReadOnly Property _AllowtoEditAssessmentGroup() As Boolean
        Get
            Return mpreAllowtoEditAssessmentGroup
        End Get
    End Property

    Public ReadOnly Property _AllowtoDeleteAssessmentGroup() As Boolean
        Get
            Return mpreAllowtoDeleteAssessmentGroup
        End Get
    End Property

    Public ReadOnly Property _AllowtoAddAssessmentScales() As Boolean
        Get
            Return mpreAllowtoAddAssessmentScales
        End Get
    End Property

    Public ReadOnly Property _AllowtoEditAssessmentScales() As Boolean
        Get
            Return mpreAllowtoEditAssessmentScales
        End Get
    End Property

    Public ReadOnly Property _AllowtoDeleteAssessmentScales() As Boolean
        Get
            Return mpreAllowtoDeleteAssessmentScales
        End Get
    End Property

    Public ReadOnly Property _AllowtoMapScaleGrouptoGoals() As Boolean
        Get
            Return mpreAllowtoMapScaleGrouptoGoals
        End Get
    End Property

    Public ReadOnly Property _AllowtoAddCompetencies() As Boolean
        Get
            Return mpreAllowtoAddCompetencies
        End Get
    End Property

    Public ReadOnly Property _AllowtoEditCompetencies() As Boolean
        Get
            Return mpreAllowtoEditCompetencies
        End Get
    End Property

    Public ReadOnly Property _AllowtoDeleteCompetencies() As Boolean
        Get
            Return mpreAllowtoDeleteCompetencies
        End Get
    End Property

    Public ReadOnly Property _AllowtoLoadfromCompetenciesLibrary() As Boolean
        Get
            Return mpreAllowtoLoadfromCompetenciesLibrary
        End Get
    End Property

    Public ReadOnly Property _AllowtoAddAssignedCompetencies() As Boolean
        Get
            Return mpreAllowtoAddAssignedCompetencies
        End Get
    End Property

    Public ReadOnly Property _AllowtoEditAssignedCompetencies() As Boolean
        Get
            Return mpreAllowtoEditAssignedCompetencies
        End Get
    End Property

    Public ReadOnly Property _AllowtoDeleteAssignedCompetencies() As Boolean
        Get
            Return mpreAllowtoDeleteAssignedCompetencies
        End Get
    End Property

    Public ReadOnly Property _AllowtoAddPerspective() As Boolean
        Get
            Return mpreAllowtoAddPerspective
        End Get
    End Property

    Public ReadOnly Property _AllowtoEditPerspective() As Boolean
        Get
            Return mpreAllowtoEditPerspective
        End Get
    End Property

    Public ReadOnly Property _AllowtoDeletePerspective() As Boolean
        Get
            Return mpreAllowtoDeletePerspective
        End Get
    End Property

    Public ReadOnly Property _AllowtoSaveBSCTitles() As Boolean
        Get
            Return mpreAllowtoSaveBSCTitles
        End Get
    End Property

    Public ReadOnly Property _AllowtoAddBSCTitlesMapping() As Boolean
        Get
            Return mpreAllowtoAddBSCTitlesMapping
        End Get
    End Property

    Public ReadOnly Property _AllowtoEditBSCTitlesMapping() As Boolean
        Get
            Return mpreAllowtoEditBSCTitlesMapping
        End Get
    End Property

    Public ReadOnly Property _AllowtoDeleteBSCTitlesMapping() As Boolean
        Get
            Return mpreAllowtoDeleteBSCTitlesMapping
        End Get
    End Property

    Public ReadOnly Property _AllowtoSaveBSCTitlesViewSetting() As Boolean
        Get
            Return mpreAllowtoSaveBSCTitlesViewSetting
        End Get
    End Property

    Public ReadOnly Property _AllowtoAddCompanyGoals() As Boolean
        Get
            Return mpreAllowtoAddCompanyGoals
        End Get
    End Property

    Public ReadOnly Property _AllowtoEditCompanyGoals() As Boolean
        Get
            Return mpreAllowtoEditCompanyGoals
        End Get
    End Property

    Public ReadOnly Property _AllowtoDeleteCompanyGoals() As Boolean
        Get
            Return mpreAllowtoDeleteCompanyGoals
        End Get
    End Property

    Public ReadOnly Property _AllowtoCommitCompanyGoals() As Boolean
        Get
            Return mpreAllowtoCommitCompanyGoals
        End Get
    End Property

    Public ReadOnly Property _AllowtoUnlockcommittedCompanyGoals() As Boolean
        Get
            Return mpreAllowtoUnlockcommittedCompanyGoals
        End Get
    End Property

    Public ReadOnly Property _AllowtoAddAllocationGoals() As Boolean
        Get
            Return mpreAllowtoAddAllocationGoals
        End Get
    End Property

    Public ReadOnly Property _AllowtoEditAllocationGoals() As Boolean
        Get
            Return mpreAllowtoEditAllocationGoals
        End Get
    End Property

    Public ReadOnly Property _AllowtoDeleteAllocationGoals() As Boolean
        Get
            Return mpreAllowtoDeleteAllocationGoals
        End Get
    End Property

    Public ReadOnly Property _AllowtoCommitAllocationGoals() As Boolean
        Get
            Return mpreAllowtoCommitAllocationGoals
        End Get
    End Property

    Public ReadOnly Property _AllowtoUnlockcommittedAllocationGoals() As Boolean
        Get
            Return mpreAllowtoUnlockcommittedAllocationGoals
        End Get
    End Property

    Public ReadOnly Property _AllowtoPerformGlobalAssignAllocationGoals() As Boolean
        Get
            Return mpreAllowtoPerformGlobalAssignAllocationGoals
        End Get
    End Property

    Public ReadOnly Property _AllowtoUpdatePercentCompletedAllocationGoals() As Boolean
        Get
            Return mpreAllowtoUpdatePercentCompletedAllocationGoals
        End Get
    End Property

    Public ReadOnly Property _AllowtoAddEmployeeGoals() As Boolean
        Get
            Return mpreAllowtoAddEmployeeGoals
        End Get
    End Property

    Public ReadOnly Property _AllowtoEditEmployeeGoals() As Boolean
        Get
            Return mpreAllowtoEditEmployeeGoals
        End Get
    End Property

    Public ReadOnly Property _AllowtoDeleteEmployeeGoals() As Boolean
        Get
            Return mpreAllowtoDeleteEmployeeGoals
        End Get
    End Property

    Public ReadOnly Property _AllowtoPerformGlobalAssignEmployeeGoals() As Boolean
        Get
            Return mpreAllowtoPerformGlobalAssignEmployeeGoals
        End Get
    End Property

    Public ReadOnly Property _AllowtoUpdatePercentCompletedEmployeeGoals() As Boolean
        Get
            Return mpreAllowtoUpdatePercentCompletedEmployeeGoals
        End Get
    End Property

    Public ReadOnly Property _AllowtoSubmitGoalsforApproval() As Boolean
        Get
            Return mpreAllowtoSubmitGoalsforApproval
        End Get
    End Property

    Public ReadOnly Property _AllowtoApproveRejectGoalsPlanning() As Boolean
        Get
            Return mpreAllowtoApproveRejectGoalsPlanning
        End Get
    End Property

    Public ReadOnly Property _AllowtoUnlockFinalSavedGoals() As Boolean
        Get
            Return mpreAllowtoUnlockFinalSavedGoals
        End Get
    End Property

    Public ReadOnly Property _AllowtoAddCustomHeaders() As Boolean
        Get
            Return mpreAllowtoAddCustomHeaders
        End Get
    End Property

    Public ReadOnly Property _AllowtoEditCustomHeaders() As Boolean
        Get
            Return mpreAllowtoEditCustomHeaders
        End Get
    End Property

    Public ReadOnly Property _AllowtoDeleteCustomHeaders() As Boolean
        Get
            Return mpreAllowtoDeleteCustomHeaders
        End Get
    End Property

    Public ReadOnly Property _AllowtoAddCustomItems() As Boolean
        Get
            Return mpreAllowtoAddCustomItems
        End Get
    End Property

    Public ReadOnly Property _AllowtoEditCustomItems() As Boolean
        Get
            Return mpreAllowtoEditCustomItems
        End Get
    End Property

    Public ReadOnly Property _AllowtoDeleteCustomItems() As Boolean
        Get
            Return mpreAllowtoDeleteCustomItems
        End Get
    End Property

    Public ReadOnly Property _AllowtoAddComputationFormula() As Boolean
        Get
            Return mpreAllowtoAddComputationFormula
        End Get
    End Property

    Public ReadOnly Property _AllowtoDeleteComputationFormula() As Boolean
        Get
            Return mpreAllowtoDeleteComputationFormula
        End Get
    End Property

    Public ReadOnly Property _AllowtoViewAssessmentPeriodList() As Boolean
        Get
            Return mpreAllowtoViewAssessmentPeriodList
        End Get
    End Property

    Public ReadOnly Property _AllowtoViewAssessorAccessList() As Boolean
        Get
            Return mpreAllowtoViewAssessorAccessList
        End Get
    End Property

    Public ReadOnly Property _AllowtoViewReviewerAccessList() As Boolean
        Get
            Return mpreAllowtoViewReviewerAccessList
        End Get
    End Property

    Public ReadOnly Property _AllowtoViewAssessmentGroupList() As Boolean
        Get
            Return mpreAllowtoViewAssessmentGroupList
        End Get
    End Property

    Public ReadOnly Property _AllowtoViewCompanyGoalsList() As Boolean
        Get
            Return mpreAllowtoViewCompanyGoalsList
        End Get
    End Property

    Public ReadOnly Property _AllowtoViewAllocationGoalsList() As Boolean
        Get
            Return mpreAllowtoViewAllocationGoalsList
        End Get
    End Property

    Public ReadOnly Property _AllowtoViewEmployeeGoalsList() As Boolean
        Get
            Return mpreAllowtoViewEmployeeGoalsList
        End Get
    End Property


    Public ReadOnly Property _AllowtoAddSelfEvaluation() As Boolean
        Get
            Return mpreAllowtoAddSelfEvaluation
        End Get
    End Property

    Public ReadOnly Property _AllowtoEditSelfEvaluation() As Boolean
        Get
            Return mpreAllowtoEditSelfEvaluation
        End Get
    End Property

    Public ReadOnly Property _AllowtoDeleteSelfEvaluation() As Boolean
        Get
            Return mpreAllowtoDeleteSelfEvaluation
        End Get
    End Property

    Public ReadOnly Property _AllowtoUnlockcommittedSelfEvaluation() As Boolean
        Get
            Return mpreAllowtoUnlockcommittedSelfEvaluation
        End Get
    End Property

    Public ReadOnly Property _AllowtoAddAssessorEvaluation() As Boolean
        Get
            Return mpreAllowtoAddAssessorEvaluation
        End Get
    End Property

    Public ReadOnly Property _AllowtoEditAssessorEvaluation() As Boolean
        Get
            Return mpreAllowtoEditAssessorEvaluation
        End Get
    End Property

    Public ReadOnly Property _AllowtoDeleteAssessorEvaluation() As Boolean
        Get
            Return mpreAllowtoDeleteAssessorEvaluation
        End Get
    End Property

    Public ReadOnly Property _AllowtoUnlockcommittedAssessorEvaluation() As Boolean
        Get
            Return mpreAllowtoUnlockcommittedAssessorEvaluation
        End Get
    End Property

    Public ReadOnly Property _AllowtoAddReviewerEvaluation() As Boolean
        Get
            Return mpreAllowtoAddReviewerEvaluation
        End Get
    End Property

    Public ReadOnly Property _AllowtoEditReviewerEvaluation() As Boolean
        Get
            Return mpreAllowtoEditReviewerEvaluation
        End Get
    End Property

    Public ReadOnly Property _AllowtoDeleteReviewerEvaluation() As Boolean
        Get
            Return mpreAllowtoDeleteReviewerEvaluation
        End Get
    End Property

    Public ReadOnly Property _AllowtoUnlockcommittedReviewerEvaluation() As Boolean
        Get
            Return mpreAllowtoUnlockcommittedReviewerEvaluation
        End Get
    End Property

    Public ReadOnly Property _AllowtoViewSelfEvaluationList() As Boolean
        Get
            Return mpreAllowtoViewSelfEvaluationList
        End Get
    End Property

    Public ReadOnly Property _AllowtoViewAssessorEvaluationList() As Boolean
        Get
            Return mpreAllowtoViewAssessorEvaluationList
        End Get
    End Property

    Public ReadOnly Property _AllowtoViewReviewerEvaluationList() As Boolean
        Get
            Return mpreAllowtoViewReviewerEvaluationList
        End Get
    End Property

    Public ReadOnly Property _AllowtoViewPerformanceEvaluation() As Boolean
        Get
            Return mpreAllowtoViewPerformanceEvaluation
        End Get
    End Property

    'Shani(06-Feb-2016) -- Start
    'PA Changes Given By CCBRT

    Public ReadOnly Property _AllowToSaveAppraisalSetup() As Boolean
        Get
            Return mpreAllowToSaveAppraisalSetup
        End Get
    End Property


    Public ReadOnly Property _AllowToSaveExportAppraisalAnalysis() As Boolean
        Get
            Return mpreAllowToSaveExportAppraisalAnalysis
        End Get
    End Property

    Public ReadOnly Property _AllowToVoidEmployeeGoals() As Boolean
        Get
            Return mpreAllowToVoidEmployeeGoals
        End Get
    End Property

    Public ReadOnly Property _AllowToVoidOwnerGoals() As Boolean
        Get
            Return mpreAllowToVoidOwnerGoals
        End Get
    End Property

    Public ReadOnly Property _AllowToOpenPeriodicReiew() As Boolean
        Get
            Return mpreAllowToOpenPeriodicReiew
        End Get
    End Property

    Public ReadOnly Property _AllowToGlobalVoidAssessment() As Boolean
        Get
            Return mpreAllowToGlobalVoidAssessment
        End Get
    End Property
    'Shani(06-Feb-2016) -- End

    'S.SANDEEP [28 MAY 2015] -- END


    'Pinkal (13-Jul-2015) -- Start
    'Enhancement - WORKING ON C & R ACCESS PRIVILEGE.

    Public ReadOnly Property _AllowtoAddClaimExpenses() As Boolean
        Get
            Return mpreAllowtoAddClaimExpenses
        End Get
    End Property

    Public ReadOnly Property _AllowtoEditClaimExpenses() As Boolean
        Get
            Return mpreAllowtoEditClaimExpenses
        End Get
    End Property

    Public ReadOnly Property _AllowtoDeleteClaimExpenses() As Boolean
        Get
            Return mpreAllowtoDeleteClaimExpenses
        End Get
    End Property

    Public ReadOnly Property _AllowtoViewClaimExpenseList() As Boolean
        Get
            Return mpreAllowtoViewClaimExpenseFormList
        End Get
    End Property

    Public ReadOnly Property _AllowtoAddSectorRouteCost() As Boolean
        Get
            Return mpreAllowtoAddSectorRouteCost
        End Get
    End Property

    Public ReadOnly Property _AllowtoEditSectorRouteCost() As Boolean
        Get
            Return mpreAllowtoEditSectorRouteCost
        End Get
    End Property

    Public ReadOnly Property _AllowtoDeleteSectorRouteCost() As Boolean
        Get
            Return mpreAllowtoDeleteSectorRouteCost
        End Get
    End Property

    Public ReadOnly Property _AllowtoViewSectorRouteCostList() As Boolean
        Get
            Return mpreAllowtoViewSectorRouteCostList
        End Get
    End Property

    Public ReadOnly Property _AllowtoAddEmpExpesneAssignment() As Boolean
        Get
            Return mpreAllowtoAddEmpExpesneAssignment
        End Get
    End Property

    Public ReadOnly Property _AllowtoDeleteEmpExpesneAssignment() As Boolean
        Get
            Return mpreAllowtoDeleteEmpExpesneAssignment
        End Get
    End Property

    Public ReadOnly Property _AllowtoViewEmpExpesneAssignment() As Boolean
        Get
            Return mpreAllowtoViewEmpExpesneAssignment
        End Get
    End Property

    Public ReadOnly Property _AllowtoAddExpenseApprovalLevel() As Boolean
        Get
            Return mpreAllowtoAddExpenseApprovalLevel
        End Get
    End Property

    Public ReadOnly Property _AllowtoEditExpenseApprovalLevel() As Boolean
        Get
            Return mpreAllowtoEditExpenseApprovalLevel
        End Get
    End Property

    Public ReadOnly Property _AllowtoDeleteExpenseApprovalLevel() As Boolean
        Get
            Return mpreAllowtoDeleteExpenseApprovalLevel
        End Get
    End Property

    Public ReadOnly Property _AllowtoViewExpenseApprovalLevelList() As Boolean
        Get
            Return mpreAllowtoViewExpenseApprovalLevelList
        End Get
    End Property

    Public ReadOnly Property _AllowtoAddExpenseApprover() As Boolean
        Get
            Return mpreAllowtoAddExpenseApprover
        End Get
    End Property

    Public ReadOnly Property _AllowtoEditExpenseApprover() As Boolean
        Get
            Return mpreAllowtoEditExpenseApprover
        End Get
    End Property

    Public ReadOnly Property _AllowtoDeleteExpenseApprover() As Boolean
        Get
            Return mpreAllowtoDeleteExpenseApprover
        End Get
    End Property

    Public ReadOnly Property _AllowtoViewExpenseApproverList() As Boolean
        Get
            Return mpreAllowtoViewExpenseApproverList
        End Get
    End Property

    Public ReadOnly Property _AllowtoMigrateExpenseApprover() As Boolean
        Get
            Return mpreAllowtoMigrateExpenseApprover
        End Get
    End Property

    Public ReadOnly Property _AllowtoSwapExpenseApprover() As Boolean
        Get
            Return mpreAllowtoSwapExpenseApprover
        End Get
    End Property

    Public ReadOnly Property _AllowtoAddClaimExpenseForm() As Boolean
        Get
            Return mpreAllowtoAddClaimExpenseForm
        End Get
    End Property

    Public ReadOnly Property _AllowtoEditClaimExpenseForm() As Boolean
        Get
            Return mpreAllowtoEditClaimExpenseForm
        End Get
    End Property

    Public ReadOnly Property _AllowtoDeleteClaimExpenseForm() As Boolean
        Get
            Return mpreAllowtoDeleteClaimExpenseForm
        End Get
    End Property

    Public ReadOnly Property _AllowtoViewClaimExpenseFormList() As Boolean
        Get
            Return mpreAllowtoViewClaimExpenseFormList
        End Get
    End Property

    Public ReadOnly Property _AllowtoCancelClaimExpenseForm() As Boolean
        Get
            Return mpreAllowtoCancelClaimExpenseForm
        End Get
    End Property

    Public ReadOnly Property _AllowtoProcessClaimExpenseForm() As Boolean
        Get
            Return mpreAllowtoProcessClaimExpenseForm
        End Get
    End Property

    Public ReadOnly Property _AllowtoViewProcessClaimExpenseFormList() As Boolean
        Get
            Return mpreAllowtoViewProcessClaimExpenseFormList
        End Get
    End Property

    Public ReadOnly Property _AllowtoPostClaimExpenseToPayroll() As Boolean
        Get
            Return mpreAllowtoPostClaimExpenseToPayroll
        End Get
    End Property


    'Pinkal (13-Jul-2015) -- End

    'S.SANDEEP [10 AUG 2015] -- START
    'ENHANCEMENT : Aruti SaaS Changes
    Public ReadOnly Property _AllowtoViewReportAbilityLevel() As Boolean
        Get
            Return mpreAllowtoViewReportAbilityLevel
        End Get
    End Property

    Public ReadOnly Property _AllowtoExportReportAbilityLevel() As Boolean
        Get
            Return mpreAllowtoExportReportAbilityLevel
        End Get
    End Property

    Public ReadOnly Property _AllowtoSaveReportAbilityLevel() As Boolean
        Get
            Return mpreAllowtoSaveReportAbilityLevel
        End Get
    End Property
    'S.SANDEEP [10 AUG 2015] -- END

    'Shani(17-Aug-2015) -- Start
    'Leave Enhancement : Putting Activate Feature in Leave Approver Master
    Public ReadOnly Property _AllowtoSetLeaveActiveApprover() As Boolean
        Get
            Return mpreAllowToSetLeaveActiveApprover
        End Get
    End Property
    Public ReadOnly Property _AllowtoSetLeaveInactiveApprover() As Boolean
        Get
            Return mpreAllowToSetLeaveInactiveApprover
        End Get
    End Property

    Public ReadOnly Property _AllowtoSetClaimActiveApprover() As Boolean
        Get
            Return mpreAllowToSetClaimActiveApprover
        End Get
    End Property
    Public ReadOnly Property _AllowtoSetClaimInactiveApprover() As Boolean
        Get
            Return mpreAllowToSetClaimInactiveApprover
        End Get
    End Property
    'Shani(17-Aug-2015) -- End


    'S.SANDEEP [23 FEB 2016] -- START
    Public ReadOnly Property _AllowtoChangePreassignedCompetencies() As Boolean
        Get
            Return mpreAllowtoChangePreassignedCompetencies
        End Get
    End Property
    'S.SANDEEP [23 FEB 2016] -- END

    'Nilay (27 Apr 2016) -- Start
    'Enhancement - 61.1 - Priority Order on Grade Group, Grade and Grade Level; Auto Salary Increment to next Grade Level as per Priority Order; Grade filter on Wages Table; Export and Import for Wages Table for KBC.
    '#23 (KBC & Kenya Project Comments List.xls) - Auto Salary incrment Per employee Anniversary,Employee Anniversay,All KBC Employees have Annivesary date for Salary increment,date is sent  when employee is confimed
    Public ReadOnly Property _AllowToAssignGradePriority() As Boolean
        Get
            Return mpreAllowToAssignGradePriority
        End Get
    End Property

    Public ReadOnly Property _AllowToViewSalaryAnniversaryMonth() As Boolean
        Get
            Return mpreAllowToViewSalaryAnniversaryMonth
        End Get
    End Property

    Public ReadOnly Property _AllowToAddSalaryAnniversaryMonth() As Boolean
        Get
            Return mpreAllowToAddSalaryAnniversaryMonth
        End Get
    End Property

    Public ReadOnly Property _AllowToDeleteSalaryAnniversaryMonth() As Boolean
        Get
            Return mpreAllowToDeleteSalaryAnniversaryMonth
        End Get
    End Property
    'Nilay (27 Apr 2016) -- End

'Shani (24-May-2016) -- Start
    Public ReadOnly Property _AllowtoComputeProcess() As Boolean
        Get
            Return mpreAllowtoComputataionProcess
        End Get
    End Property

    Public ReadOnly Property _AllowtoComputeVoidProcess() As Boolean
        Get
            Return mpreAllowtoComputataionVoidProcess
        End Get
    End Property
    'Shani (24-May-2016) -- End


 'Nilay (06-Jun-2016) -- Start
    'Enhancement : Import option in Wages Table for KBC
    Public ReadOnly Property _AllowToImportWagesTable() As Boolean
        Get
            Return mpreAllowToImportWagesTable
        End Get
    End Property

    Public ReadOnly Property _AllowToExportWagesTable() As Boolean
        Get
            Return mpreAllowToExportWagesTable
        End Get
    End Property
    'Nilay (06-Jun-2016) -- End

    'Nilay (20-May-2016) -- Start
    'ENHANCEMENT - 61.1 - New Budget Redesign for Marie Stopes (Fund Sources, Funds Adjustments, Budget Formula and new Budget screen Allocation/Employee wise, Trans./Summary wise)
    Public ReadOnly Property _AllowToViewFundSource() As Boolean
        Get
            Return mpreAllowToViewFundSource
        End Get
    End Property

    Public ReadOnly Property _AllowToAddFundSource() As Boolean
        Get
            Return mpreAllowToAddFundSource
        End Get
    End Property

    Public ReadOnly Property _AllowToEditFundSource() As Boolean
        Get
            Return mpreAllowToEditFundSource
        End Get
    End Property

    Public ReadOnly Property _AllowToDeleteFundSource() As Boolean
        Get
            Return mpreAllowToDeleteFundSource
        End Get
    End Property

    Public ReadOnly Property _AllowToViewFundAdjustment() As Boolean
        Get
            Return mpreAllowToViewFundAdjustment
        End Get
    End Property

    Public ReadOnly Property _AllowToAddFundAdjustment() As Boolean
        Get
            Return mpreAllowToAddFundAdjustment
        End Get
    End Property

    Public ReadOnly Property _AllowToEditFundAdjustment() As Boolean
        Get
            Return mpreAllowToEditFundAdjustment
        End Get
    End Property

    Public ReadOnly Property _AllowToDeleteFundAdjustment() As Boolean
        Get
            Return mpreAllowToDeleteFundAdjustment
        End Get
    End Property
    'Nilay (20-May-2016) -- End

    'Sohail (07 Jun 2016) -- Start
    'Enhancement -  62.1 - New Budget Redesign for Marie Stopes (Fund Sources, Funds Adjustments, Budget Formula and new Budget screen Allocation/Employee wise, Trans./Summary wise).
    Public ReadOnly Property _AllowToViewFundActivity() As Boolean
        Get
            Return mpreAllowToViewFundActivity
        End Get
    End Property

    Public ReadOnly Property _AllowToAddFundActivity() As Boolean
        Get
            Return mpreAllowToAddFundActivity
        End Get
    End Property

    Public ReadOnly Property _AllowToEditFundActivity() As Boolean
        Get
            Return mpreAllowToEditFundActivity
        End Get
    End Property

    Public ReadOnly Property _AllowToDeleteFundActivity() As Boolean
        Get
            Return mpreAllowToDeleteFundActivity
        End Get
    End Property

    Public ReadOnly Property _AllowToViewFundActivityAdjustment() As Boolean
        Get
            Return mpreAllowToViewFundActivityAdjustment
        End Get
    End Property

    Public ReadOnly Property _AllowToAddFundActivityAdjustment() As Boolean
        Get
            Return mpreAllowToAddFundActivityAdjustment
        End Get
    End Property

    Public ReadOnly Property _AllowToEditFundActivityAdjustment() As Boolean
        Get
            Return mpreAllowToEditFundActivityAdjustment
        End Get
    End Property

    Public ReadOnly Property _AllowToDeleteFundActivityAdjustment() As Boolean
        Get
            Return mpreAllowToDeleteFundActivityAdjustment
        End Get
    End Property

    Public ReadOnly Property _AllowToViewBudgetFormula() As Boolean
        Get
            Return mpreAllowToViewBudgetFormula
        End Get
    End Property

    Public ReadOnly Property _AllowToAddBudgetFormula() As Boolean
        Get
            Return mpreAllowToAddBudgetFormula
        End Get
    End Property

    Public ReadOnly Property _AllowToEditBudgetFormula() As Boolean
        Get
            Return mpreAllowToEditBudgetFormula
        End Get
    End Property

    Public ReadOnly Property _AllowToDeleteBudgetFormula() As Boolean
        Get
            Return mpreAllowToDeleteBudgetFormula
        End Get
    End Property

    Public ReadOnly Property _AllowToViewBudget() As Boolean
        Get
            Return mpreAllowToViewBudget
        End Get
    End Property

    Public ReadOnly Property _AllowToAddBudget() As Boolean
        Get
            Return mpreAllowToAddBudget
        End Get
    End Property

    Public ReadOnly Property _AllowToEditBudget() As Boolean
        Get
            Return mpreAllowToEditBudget
        End Get
    End Property

    Public ReadOnly Property _AllowToDeleteBudget() As Boolean
        Get
            Return mpreAllowToDeleteBudget
        End Get
    End Property

    'Sohail (02 Sep 2016) -- Start
    'Enhancement -  63.1 - New Budget Redesign for Marie Stopes (Period Wise budget codes).
    'Public ReadOnly Property _AllowToSaveBudgetCodes() As Boolean
    '    Get
    '        Return mpreAllowToSaveBudgetCodes
    '    End Get
    'End Property
    Public ReadOnly Property _AllowToAddBudgetCodes() As Boolean
        Get
            Return mpreAllowToAddBudgetCodes
        End Get
    End Property

    Public ReadOnly Property _AllowToEditBudgetCodes() As Boolean
        Get
            Return mpreAllowToEditBudgetCodes
        End Get
    End Property

    Public ReadOnly Property _AllowToDeleteBudgetCodes() As Boolean
        Get
            Return mpreAllowToDeleteBudgetCodes
        End Get
    End Property

    Public ReadOnly Property _AllowToViewBudgetCodes() As Boolean
        Get
            Return mpreAllowToViewBudgetCodes
        End Get
    End Property
    'Sohail (02 Sep 2016) -- End

    Public ReadOnly Property _AllowToViewFundProjectCode() As Boolean
        Get
            Return mpreAllowToViewFundProjectCode
        End Get
    End Property

    Public ReadOnly Property _AllowToAddFundProjectCode() As Boolean
        Get
            Return mpreAllowToAddFundProjectCode
        End Get
    End Property

    Public ReadOnly Property _AllowToEditFundProjectCode() As Boolean
        Get
            Return mpreAllowToEditFundProjectCode
        End Get
    End Property

    Public ReadOnly Property _AllowToDeleteFundProjectCode() As Boolean
        Get
            Return mpreAllowToDeleteFundProjectCode
        End Get
    End Property

    Public ReadOnly Property _AllowToMapLevelToApprover() As Boolean
        Get
            Return mpreAllowToMapLevelToApprover
        End Get
    End Property

    Public ReadOnly Property _AllowToViewBudgetApproverLevelMapping() As Boolean
        Get
            Return mpreAllowToViewBudgetApproverLevelMapping
        End Get
    End Property

    Public ReadOnly Property _AllowToDeleteBudgetApproverLevelMapping() As Boolean
        Get
            Return mpreAllowToDeleteBudgetApproverLevelMapping
        End Get
    End Property

    Public ReadOnly Property _AllowToViewBudgetApproverLevel() As Boolean
        Get
            Return mpreAllowToViewBudgetApproverLevel
        End Get
    End Property

    Public ReadOnly Property _AllowToAddBudgetApproverLevel() As Boolean
        Get
            Return mpreAllowToAddBudgetApproverLevel
        End Get
    End Property

    Public ReadOnly Property _AllowToEditBudgetApproverLevel() As Boolean
        Get
            Return mpreAllowToEditBudgetApproverLevel
        End Get
    End Property

    Public ReadOnly Property _AllowToDeleteBudgetApproverLevel() As Boolean
        Get
            Return mpreAllowToDeleteBudgetApproverLevel
        End Get
    End Property

    Public ReadOnly Property _AllowToApproveBudget() As Boolean
        Get
            Return mpreAllowToApproveBudget
        End Get
    End Property

    Public ReadOnly Property _AllowToVoidApprovedBudget() As Boolean
        Get
            Return mpreAllowToVoidApprovedBudget
        End Get
    End Property

    Public ReadOnly Property _AllowToViewBudgetApprovals() As Boolean
        Get
            Return mpreAllowToViewBudgetApprovals
        End Get
    End Property
    'Sohail (07 Jun 2016) -- End

    'Sohail (06 Oct 2021) -- Start
    'NMB Enhancement : OLD-483 : Separate sender's email configuration on recruitment portal from company email.
    Public ReadOnly Property _AllowToViewEmailSetup() As Boolean
        Get
            Return mpreAllowToViewEmailSetup
        End Get
    End Property

    Public ReadOnly Property _AllowToAddEmailSetup() As Boolean
        Get
            Return mpreAllowToAddEmailSetup
        End Get
    End Property

    Public ReadOnly Property _AllowToEditEmailSetup() As Boolean
        Get
            Return mpreAllowToEditEmailSetup
        End Get
    End Property

    Public ReadOnly Property _AllowToDeleteEmailSetup() As Boolean
        Get
            Return mpreAllowToDeleteEmailSetup
        End Get
    End Property
    'Sohail (06 Oct 2021) -- End


    'Shani (26-Sep-2016) -- Start
    'Enhancement -Changes PA Flow And Add New screen Given By (Andrew)
    Public ReadOnly Property _AllowToApproveGoalsAccomplishment() As Boolean
        Get
            Return mpreAllowToApproveGoalsAccomplishment
        End Get
    End Property
    'Shani (26-Sep-2016) -- End



    'Pinkal (03-May-2017) -- Start
    'Enhancement - Working On Implementing privileges in Budget Timesheet Module.
    Public ReadOnly Property _AllowToMapBudgetTimesheetApprover() As Boolean
        Get
            Return mpreAllowtoMapBudgetTimesheetApprover
        End Get
    End Property
    'Pinkal (03-May-2017) -- End

  'Shani (08-Dec-2016) -- Start
    'Enhancement -  Add Employee Allocaion/Date Privilage
    Public ReadOnly Property _AllowToChangeEmpRecategorize() As Boolean
        Get
            Return mpreAllowToChangeEmpRecategorize
        End Get
    End Property
    Public ReadOnly Property _AllowToChangeEmpTransfers() As Boolean
        Get
            Return mpreAllowToChangeEmpTransfers
        End Get
    End Property
    Public ReadOnly Property _AllowToChangeEmpWorkPermit() As Boolean
        Get
            Return mpreAllowToChangeEmpWorkPermit
        End Get
    End Property
    'Shani (08-Dec-2016) -- End

    'Pinkal (21-Dec-2016) -- Start
    'Enhancement - Adding Swap Approver Privilage for Leave Module.
    Public ReadOnly Property _AllowToSwapLeaveApprover() As Boolean
        Get
            Return mpreAllowToSwapLeaveApprover
        End Get
    End Property
    'Pinkal (21-Dec-2016) -- End


    'Nilay (13 Apr 2017) -- Start
    'Enhancements: Settings for mandatory options in online recruitment
    Public ReadOnly Property _AllowToViewSkillExpertise() As Boolean
        Get
            Return mpreAllowToViewSkillExpertise
        End Get
    End Property

    Public ReadOnly Property _AllowToAddSkillExpertise() As Boolean
        Get
            Return mpreAllowToAddSkillExpertise
        End Get
    End Property

    Public ReadOnly Property _AllowToEditSkillExpertise() As Boolean
        Get
            Return mpreAllowToEditSkillExpertise
        End Get
    End Property

    Public ReadOnly Property _AllowToDeleteSkillExpertise() As Boolean
        Get
            Return mpreAllowToDeleteSkillExpertise
        End Get
    End Property
    'Nilay (13 Apr 2017) -- End


    'Pinkal (03-May-2017) -- Start
    'Enhancement - Working On Implementing privileges in Budget Timesheet Module.

    Public ReadOnly Property _AllowToViewBudgetTimesheetApproverLevelList() As Boolean
        Get
            Return mpreAllowToViewBudgetTimesheetApproverLevelList
        End Get
    End Property

    Public ReadOnly Property _AllowToAddBudgetTimesheetApproverLevel() As Boolean
        Get
            Return mpreAllowToAddBudgetTimesheetApproverLevel
        End Get
    End Property

    Public ReadOnly Property _AllowToEditBudgetTimesheetApproverLevel() As Boolean
        Get
            Return mpreAllowToEditBudgetTimesheetApproverLevel
        End Get
    End Property

    Public ReadOnly Property _AllowToDeleteBudgetTimesheetApproverLevel() As Boolean
        Get
            Return mpreAllowToDeleteBudgetTimesheetApproverLevel
        End Get
    End Property

    Public ReadOnly Property _AllowToViewBudgetTimesheetApproverList() As Boolean
        Get
            Return mpreAllowToViewBudgetTimesheetApproverList
        End Get
    End Property

    Public ReadOnly Property _AllowToAddBudgetTimesheetApprover() As Boolean
        Get
            Return mpreAllowToAddBudgetTimesheetApprover
        End Get
    End Property

    Public ReadOnly Property _AllowToEditBudgetTimesheetApprover() As Boolean
        Get
            Return mpreAllowToEditBudgetTimesheetApprover
        End Get
    End Property

    Public ReadOnly Property _AllowToDeleteBudgetTimesheetApprover() As Boolean
        Get
            Return mpreAllowToDeleteBudgetTimesheetApprover
        End Get
    End Property

    Public ReadOnly Property _AllowToSetBudgetTimesheetApproverAsActive() As Boolean
        Get
            Return mpreAllowToSetBudgetTimesheetApproverAsActive
        End Get
    End Property

    Public ReadOnly Property _AllowToSetBudgetTimesheetApproverAsInActive() As Boolean
        Get
            Return mpreAllowToSetBudgetTimesheetApproverAsInActive
        End Get
    End Property

    Public ReadOnly Property _AllowToViewEmployeeBudgetTimesheetList() As Boolean
        Get
            Return mpreAllowToViewEmployeeBudgetTimesheetList
        End Get
    End Property

    Public ReadOnly Property _AllowToAddEmployeeBudgetTimesheet() As Boolean
        Get
            Return mpreAllowToAddEmployeeBudgetTimesheet
        End Get
    End Property

    Public ReadOnly Property _AllowToEditEmployeeBudgetTimesheet() As Boolean
        Get
            Return mpreAllowToEditEmployeeBudgetTimesheet
        End Get
    End Property

    Public ReadOnly Property _AllowToDeleteEmployeeBudgetTimesheet() As Boolean
        Get
            Return mpreAllowToDeleteEmployeeBudgetTimesheet
        End Get
    End Property

    Public ReadOnly Property _AllowToCancelEmployeeBudgetTimesheet() As Boolean
        Get
            Return mpreAllowToCancelEmployeeBudgetTimesheet
        End Get
    End Property

    Public ReadOnly Property _AllowToViewPendingEmpBudgetTimesheetSubmitForApproval() As Boolean
        Get
            Return mpreAllowToViewPendingEmpBudgetTimesheetSubmitForApproval
        End Get
    End Property

    Public ReadOnly Property _AllowToViewCompletedEmpBudgetTimesheetSubmitForApproval() As Boolean
        Get
            Return mpreAllowToViewCompletedEmpBudgetTimesheetSubmitForApproval
        End Get
    End Property

    Public ReadOnly Property _AllowToSubmitForApprovalForPendingEmpBudgetTimesheet() As Boolean
        Get
            Return mpreAllowToSubmitForApprovalForPendingEmpBudgetTimesheet
        End Get
    End Property

    Public ReadOnly Property _AllowToViewEmployeeBudgetTimesheetApprovalList() As Boolean
        Get
            Return mpreAllowToViewEmployeeBudgetTimesheetApprovalList
        End Get
    End Property

    Public ReadOnly Property _AllowToChangeEmployeeBudgetTimesheetStatus() As Boolean
        Get
            Return mpreAllowToChangeEmployeeBudgetTimesheetStatus
        End Get
    End Property

    Public ReadOnly Property _AllowToMigrateBudgetTimesheetApprover() As Boolean
        Get
            Return mpreAllowToMigrateBudgetTimesheetApprover
        End Get
    End Property


    Public ReadOnly Property _AllowToImportEmployeeShiftAssignment() As Boolean
        Get
            Return mpreAllowToImportEmployeeShiftAssignment
        End Get
    End Property

    'Pinkal (03-May-2017) -- End


    'Pinkal (23-AUG-2017) -- Start
    'Enhancement - Working on Enable/Disable Active Directory User for Budget Timesheet.
    Public ReadOnly Property _AllowToEnableDisableADUser() As Boolean
        Get
            Return mpreAllowToEnableDisableADUser
        End Get
    End Property
    'Pinkal (23-AUG-2017) -- End



    'Varsha Rana (17-Oct-2017) -- Start
    'Enhancement - Give user privileges

    Public ReadOnly Property _AllowToImportEmployeeAccountConfiguration() As Boolean
        Get
            Return mpreAllowToImportEmployeeAccountConfiguration
        End Get
    End Property
    Public ReadOnly Property _AllowToExportEmployeeAccountConfiguration() As Boolean
        Get
            Return mpreAllowToExportEmployeeAccountConfiguration
        End Get
    End Property
    Public ReadOnly Property _AllowToGetFileFormatOfEmployeeAccountConfiguration() As Boolean
        Get
            Return mpreAllowToGetFileFormatOfEmployeeAccountConfiguration
        End Get
    End Property

    'Public ReadOnly Property _AllowToAddPayslipGlobalMessage() As Boolean
    '    Get
    '        Return mpreAllowToAddPayslipGlobalMessage
    '    End Get
    'End Property

    'Public ReadOnly Property _AllowToEditPayslipGlobalMessage() As Boolean
    '    Get
    '        Return mpreAllowToEditPayslipGlobalMessage
    '    End Get
    'End Property

    'Public ReadOnly Property _AllowToDeletePayslipGlobalMessage() As Boolean
    '    Get
    '        Return mpreAllowToDeletePayslipGlobalMessage
    '    End Get
    'End Property

    Public ReadOnly Property _AllowToHistoricalSalaryChange() As Boolean
        Get
            Return mpreAllowToHistoricalSalaryChange
        End Get
    End Property

    Public ReadOnly Property _AllowToAddLoanApproverLevel() As Boolean
        Get
            Return mpreAllowToAddLoanApproverLevel
        End Get
    End Property
    Public ReadOnly Property _AllowToEditLoanApproverLevel() As Boolean
        Get
            Return mpreAllowToEditLoanApproverLevel
        End Get
    End Property
    Public ReadOnly Property _AllowToDeleteLoanApproverLevel() As Boolean
        Get
            Return mpreAllowToDeleteLoanApproverLevel
        End Get
    End Property

    Public ReadOnly Property _AllowToAddLoanApprover() As Boolean
        Get
            Return mpreAllowToAddLoanApprover
        End Get
    End Property
    Public ReadOnly Property _AllowToEditLoanApprover() As Boolean
        Get
            Return mpreAllowToEditLoanApprover
        End Get
    End Property
    Public ReadOnly Property _AllowToDeleteLoanApprover() As Boolean
        Get
            Return mpreAllowToDeleteLoanApprover
        End Get
    End Property
    Public ReadOnly Property _AllowToSetActiveInactiveLoanApprover() As Boolean
        Get
            Return mpreAllowToSetActiveInactiveLoanApprover
        End Get
    End Property
    
    Public ReadOnly Property _AllowToViewLoanApproverLevelList() As Boolean
        Get
            Return mpreAllowToViewLoanApproverLevelList
        End Get
    End Property

    Public ReadOnly Property _AllowToViewLoanApproverList() As Boolean
        Get
            Return mpreAllowToViewLoanApproverList
        End Get
    End Property

    Public ReadOnly Property _AllowToProcessGlobalLoanApprove() As Boolean
        Get
            Return mpreAllowToProcessGlobalLoanApprove
        End Get
    End Property

    Public ReadOnly Property _AllowToProcessGlobalLoanAssign() As Boolean
        Get
            Return mpreAllowToProcessGlobalLoanAssign
        End Get
    End Property

    Public ReadOnly Property _AllowToViewLoanApprovalList() As Boolean
        Get
            Return mpreAllowToViewLoanApprovalList
        End Get
    End Property

    Public ReadOnly Property _AllowToChangeLoanStatus() As Boolean
        Get
            Return mpreAllowToChangeLoanStatus
        End Get
    End Property

    Public ReadOnly Property _AllowToTransferLoanApprover() As Boolean
        Get
            Return mpreAllowToTransferLoanApprover
        End Get
    End Property
    Public ReadOnly Property _AllowToSwapLoanApprover() As Boolean
        Get
            Return mpreAllowToSwapLoanApprover
        End Get
    End Property

    Public ReadOnly Property _AllowToImportLeaveApprovers() As Boolean
        Get
            Return mpreAllowToImportLeaveApprovers
        End Get
    End Property
    Public ReadOnly Property _AllowToImportLeavePlanner() As Boolean
        Get
            Return mpreAllowToImportLeavePlanner
        End Get
    End Property

    Public ReadOnly Property _AllowToViewDeviceUserMapping() As Boolean
        Get
            Return mpreAllowToViewDeviceUserMapping
        End Get
    End Property

    Public ReadOnly Property _AllowToAddShiftPolicyAssignment() As Boolean
        Get
            Return mpreAllowToAddShiftPolicyAssignment
        End Get
    End Property

    Public ReadOnly Property _AllowToAssignEmployeeDayOff() As Boolean
        Get
            Return mpreAllowToAssignEmployeeDayOff
        End Get
    End Property

    Public ReadOnly Property _AllowToViewEmployeeDayOff() As Boolean
        Get
            Return mpreAllowToViewEmployeeDayOff
        End Get
    End Property

    Public ReadOnly Property _AllowToDeleteEmployeeDayOff() As Boolean
        Get
            Return mpreAllowToDeleteEmployeeDayOff
        End Get
    End Property

    Public ReadOnly Property _AllowToImportEmployeeBirthInfo() As Boolean
        Get
            Return mpreAllowToImportEmployeeBirthInfo
        End Get
    End Property
    Public ReadOnly Property _AllowToImportEmployeeAddress() As Boolean
        Get
            Return mpreAllowToImportEmployeeAddress
        End Get
    End Property
    Public ReadOnly Property _AllowToViewMissingEmployee() As Boolean
        Get
            Return mpreAllowToViewMissingEmployee
        End Get
    End Property
   
    Public ReadOnly Property _AllowToUpdateEmployeeDetails() As Boolean
        Get
            Return mpreAllowToUpdateEmployeeDetails
        End Get
    End Property

    Public ReadOnly Property _AllowToUpdateEmployeeMovements() As Boolean
        Get
            Return mpreAllowToUpdateEmployeeMovements
        End Get
    End Property

    Public ReadOnly Property _AllowToViewAssignedShiftList() As Boolean
        Get
            Return mpreAllowToViewAssignedShiftList
        End Get
    End Property
    Public ReadOnly Property _AllowToDeleteAssignedShift() As Boolean
        Get
            Return mpreAllowToDeleteAssignedShift
        End Get
    End Property

    Public ReadOnly Property _AllowToImportCompanyAssets() As Boolean
        Get
            Return mpreAllowToImportCompanyAssets
        End Get
    End Property

    Public ReadOnly Property _AllowToExportEmployeeList() As Boolean
        Get
            Return mpreAllowToExportEmployeeList
        End Get
    End Property

    Public ReadOnly Property _AllowToAddRemoveFields() As Boolean
        Get
            Return mpreAllowToAddRemoveFields
        End Get
    End Property

    Public ReadOnly Property _AllowToEditTransferEmployeeDetails() As Boolean
        Get
            Return mpreAllowToEditTransferEmployeeDetails
        End Get
    End Property

    Public ReadOnly Property _AllowToDeleteTransferEmployeeDetails() As Boolean
        Get
            Return mpreAllowToDeleteTransferEmployeeDetails
        End Get
    End Property

    Public ReadOnly Property _AllowToEditRecategorizeEmployeeDetails() As Boolean
        Get
            Return mpreAllowToEditRecategorizeEmployeeDetails
        End Get
    End Property

    Public ReadOnly Property _AllowToDeleteRecategorizeEmployeeDetails() As Boolean
        Get
            Return mpreAllowToDeleteRecategorizeEmployeeDetails
        End Get
    End Property

    Public ReadOnly Property _AllowToEditProbationEmployeeDetails() As Boolean
        Get
            Return mpreAllowToEditProbationEmployeeDetails
        End Get
    End Property

    Public ReadOnly Property _AllowToDeleteProbationEmployeeDetails() As Boolean
        Get
            Return mpreAllowToDeleteProbationEmployeeDetails
        End Get
    End Property

    Public ReadOnly Property _AllowToEditConfirmationEmployeeDetails() As Boolean
        Get
            Return mpreAllowToEditConfirmationEmployeeDetails
        End Get
    End Property

    Public ReadOnly Property _AllowToDeleteConfirmationEmployeeDetails() As Boolean
        Get
            Return mpreAllowToDeleteConfirmationEmployeeDetails
        End Get
    End Property

    Public ReadOnly Property _AllowToEditSuspensionEmployeeDetails() As Boolean
        Get
            Return mpreAllowToEditSuspensionEmployeeDetails
        End Get
    End Property

    Public ReadOnly Property _AllowToDeleteSuspensionEmployeeDetails() As Boolean
        Get
            Return mpreAllowToDeleteSuspensionEmployeeDetails
        End Get
    End Property

    Public ReadOnly Property _AllowToEditTerminationEmployeeDetails() As Boolean
        Get
            Return mpreAllowToEditTerminationEmployeeDetails
        End Get
    End Property

    Public ReadOnly Property _AllowToDeleteTerminationEmployeeDetails() As Boolean
        Get
            Return mpreAllowToDeleteTerminationEmployeeDetails
        End Get
    End Property

    Public ReadOnly Property _AllowToAssignBenefitGroup() As Boolean
        Get
            Return mpreAllowToAssignBenefitGroup
        End Get
    End Property

    Public ReadOnly Property _AllowToRehireEmployee() As Boolean
        Get
            Return mpreAllowToRehireEmployee
        End Get
    End Property

    Public ReadOnly Property _AllowToEditWorkPermitEmployeeDetails() As Boolean
        Get
            Return mpreAllowToEditWorkPermitEmployeeDetails
        End Get
    End Property

    Public ReadOnly Property _AllowToDeleteWorkPermitEmployeeDetails() As Boolean
        Get
            Return mpreAllowToDeleteWorkPermitEmployeeDetails
        End Get
    End Property

    Public ReadOnly Property _AllowToEditRetiredEmployeeDetails() As Boolean
        Get
            Return mpreAllowToEditRetiredEmployeeDetails
        End Get
    End Property

    Public ReadOnly Property _AllowToDeleteRetiredEmployeeDetails() As Boolean
        Get
            Return mpreAllowToDeleteRetiredEmployeeDetails
        End Get
    End Property

    Public ReadOnly Property _AllowToGlobalVoidEmployeeMembership() As Boolean
        Get
            Return mpreAllowToGlobalVoidEmployeeMembership
        End Get
    End Property

    Public ReadOnly Property _AllowToViewEligibleApplicants() As Boolean
        Get
            Return mpreAllowToViewEligibleApplicants
        End Get
    End Property

    Public ReadOnly Property _AllowToPerformComputeScoreProcess() As Boolean
        Get
            Return mpreAllowToPerformComputeScoreProcess
        End Get
    End Property

    Public ReadOnly Property _AllowToPerformVoidComputedScore() As Boolean
        Get
            Return mpreAllowToPerformVoidComputedScore
        End Get
    End Property

    Public ReadOnly Property _AllowToEditAppraisalSetup() As Boolean
        Get
            Return mpreAllowToEditAppraisalSetup
        End Get
    End Property

    Public ReadOnly Property _AllowToDeleteAppraisalSetup() As Boolean
        Get
            Return mpreAllowToDeleteAppraisalSetup
        End Get
    End Property

    Public ReadOnly Property _AllowToImportTrainingEnrollment() As Boolean
        Get
            Return mpreAllowToImportTrainingEnrollment
        End Get
    End Property

    Public ReadOnly Property _AllowToPostNewProceedings() As Boolean
        Get
            Return mpreAllowToPostNewProceedings
        End Get
    End Property

    Public ReadOnly Property _AllowToEditViewProceedings() As Boolean
        Get
            Return mpreAllowToEditViewProceedings
        End Get
    End Property

    Public ReadOnly Property _AllowToScanAttachmentDocuments() As Boolean
        Get
            Return mpreAllowToScanAttachmentDocuments
        End Get
    End Property

    Public ReadOnly Property _AllowToProceedingSubmitForApproval() As Boolean
        Get
            Return mpreAllowToProceedingSubmitForApproval
        End Get
    End Property

    Public ReadOnly Property _AllowToApproveDisapproveProceedings() As Boolean
        Get
            Return mpreAllowToApproveDisapproveProceedings
        End Get
    End Property

    Public ReadOnly Property _AllowToExemptTransactionHead() As Boolean
        Get
            Return mpreAllowToExemptTransactionHead
        End Get
    End Property

    Public ReadOnly Property _AllowToPostTransactionHead() As Boolean
        Get
            Return mpreAllowToPostTransactionHead
        End Get
    End Property

    Public ReadOnly Property _AllowToViewDisciplineHead() As Boolean
        Get
            Return mpreAllowToViewDisciplineHead
        End Get
    End Property

    Public ReadOnly Property _AllowToPrintPreviewDisciplineCharge() As Boolean
        Get
            Return mpreAllowToPrintPreviewDisciplineCharge
        End Get
    End Property

    Public ReadOnly Property _AllowToAddDisciplinaryCommittee() As Boolean
        Get
            Return mpreAllowToAddDisciplinaryCommittee
        End Get
    End Property

    Public ReadOnly Property _AllowToEditDeleteDisciplinaryCommittee() As Boolean
        Get
            Return mpreAllowToEditDeleteDisciplinaryCommittee
        End Get
    End Property

    Public ReadOnly Property _AllowToViewDisciplineProceedingList() As Boolean
        Get
            Return mpreAllowToViewDisciplineProceedingList
        End Get
    End Property

    Public ReadOnly Property _AllowToViewDisciplineHearingList() As Boolean
        Get
            Return mpreAllowToViewDisciplineHearingList
        End Get
    End Property

    Public ReadOnly Property _AllowToAddDisciplineHearing() As Boolean
        Get
            Return mpreAllowToAddDisciplineHearing
        End Get
    End Property

    Public ReadOnly Property _AllowToEditDisciplineHearing() As Boolean
        Get
            Return mpreAllowToEditDisciplineHearing
        End Get
    End Property

    Public ReadOnly Property _AllowToDeleteDisciplineHearing() As Boolean
        Get
            Return mpreAllowToDeleteDisciplineHearing
        End Get
    End Property

    Public ReadOnly Property _AllowToMarkAsClosedDisciplineHearing() As Boolean
        Get
            Return mpreAllowToMarkAsClosedDisciplineHearing
        End Get
    End Property

    Public ReadOnly Property _AllowToViewAssignedPolicyList() As Boolean
        Get
            Return mpreAllowToViewAssignedPolicyList
        End Get
    End Property

    Public ReadOnly Property _AllowToDeleteAssignedPolicy() As Boolean
        Get
            Return mpreAllowToDeleteAssignedPolicy
        End Get
    End Property
  
    Public ReadOnly Property _AllowToPerformGlobalGoalOperations() As Boolean
        Get
            Return mpreAllowToPerformGlobalGoalOperations
        End Get
    End Property

    Public ReadOnly Property _AllowToPerformGlobalVoidGoals() As Boolean
        Get
            Return mpreAllowToPerformGlobalVoidGoals
        End Get
    End Property

    Public ReadOnly Property _AllowToSubmitForGoalAccomplished() As Boolean
        Get
            Return mpreAllowToSubmitForGoalAccomplished
        End Get
    End Property


    Public ReadOnly Property _AllowToImportEmployeeGoals() As Boolean
        Get
            Return mpreAllowToImportEmployeeGoals
        End Get
    End Property

    Public ReadOnly Property _AllowToPrintEmployeeScoreCard() As Boolean
        Get
            Return mpreAllowToPrintEmployeeScoreCard
        End Get
    End Property


    Public ReadOnly Property _AllowToExportEmployeeScoreCard() As Boolean
        Get
            Return mpreAllowToExportEmployeeScoreCard
        End Get
    End Property

    Public ReadOnly Property _AllowToSetTranHeadInactive() As Boolean
        Get
            Return mpreAllowToSetTranHeadInactive
        End Get
    End Property

    Public ReadOnly Property _AllowToAddLeaveFrequency() As Boolean
        Get
            Return mpreAllowToAddLeaveFrequency
        End Get
    End Property

    Public ReadOnly Property _AllowToDeleteLeaveFrequency() As Boolean
        Get
            Return mpreAllowToDeleteLeaveFrequency
        End Get
    End Property

    Public ReadOnly Property _AllowToViewLeaveFrequency() As Boolean
        Get
            Return mpreAllowToViewLeaveFrequency
        End Get
    End Property
   
    Public ReadOnly Property _AllowToPerformLeaveAdjustment() As Boolean
        Get
            Return mpreAllowToPerformLeaveAdjustment
        End Get
    End Property

    Public ReadOnly Property _AllowToEditRehireEmployeeDetails() As Boolean
        Get
            Return mpreAllowToEditRehireEmployeeDetails
        End Get
    End Property

    Public ReadOnly Property _AllowToDeleteRehireEmployeeDetails() As Boolean
        Get
            Return mpreAllowToDeleteRehireEmployeeDetails
        End Get
    End Property
    Public ReadOnly Property _AllowToPerformEligibleOperation() As Boolean
        Get
            Return mpreAllowToPerformEligibleOperation
        End Get
    End Property

    Public ReadOnly Property _AllowToPerformNotEligibleOperation() As Boolean
        Get
            Return mpreAllowToPerformNotEligibleOperation
        End Get
    End Property
    'Varsha Rana (17-Oct-2017) -- End

'S.SANDEEP [11-AUG-2017] -- START
    'ISSUE/ENHANCEMENT : ARUTI SYMMETRY INTEGRATION
    Public ReadOnly Property _AllowtoSeeEmployeeSignature() As Boolean
        Get
            Return mpreAllowtoSeeEmployeeSignature
        End Get
    End Property
    'S.SANDEEP [11-AUG-2017] -- END

    'S.SANDEEP [16-Jan-2018] -- START
    'ISSUE/ENHANCEMENT : REF-ID # 118
    Public ReadOnly Property _AllowtoAddEmployeeSignature() As Boolean
        Get
            Return mpreAllowtoAddEmployeeSignature
        End Get
    End Property
    Public ReadOnly Property _AllowtoDeleteEmployeeSignature() As Boolean
        Get
            Return mpreAllowtoDeleteEmployeeSignature
        End Get
    End Property
    'S.SANDEEP [16-Jan-2018] -- END

    'S.SANDEEP [12-Apr-2018] -- START
    'ISSUE/ENHANCEMENT : {Ref#218|#ARUTI-106}
    Public ReadOnly Property _AllowtoViewPendingAccrueData() As Boolean
        Get
            Return mpreAllowtoViewPendingAccrueData
        End Get
    End Property
    'S.SANDEEP [12-Apr-2018] -- END


    'S.SANDEEP [20-JUN-2018] -- Start
    'Enhancement - Implementing Employee Approver Flow For NMB .

    Public ReadOnly Property _AllowToViewEmpApproverLevelList() As Boolean
        Get
            Return mpreAllowToViewEmpApproverLevelList
        End Get
    End Property

    Public ReadOnly Property _AllowToAddEmpApproverLevel() As Boolean
        Get
            Return mpreAllowToAddEmpApproverLevel
        End Get
    End Property

    Public ReadOnly Property _AllowToEditEmpApproverLevel() As Boolean
        Get
            Return mpreAllowToEditEmpApproverLevel
        End Get
    End Property

    Public ReadOnly Property _AllowToDeleteEmpApproverLevel() As Boolean
        Get
            Return mpreAllowToDeleteEmpApproverLevel
        End Get
    End Property

    Public ReadOnly Property _AllowToViewEmployeeApproverList() As Boolean
        Get
            Return mpreAllowToViewEmployeeApproverList
        End Get
    End Property

    Public ReadOnly Property _AllowToAddEmployeeApprover() As Boolean
        Get
            Return mpreAllowToAddEmployeeApprover
        End Get
    End Property

    Public ReadOnly Property _AllowToDeleteEmployeeApprover() As Boolean
        Get
            Return mpreAllowToDeleteEmployeeApprover
        End Get
    End Property

    Public ReadOnly Property _AllowtoSetInactiveEmployeeApprover() As Boolean
        Get
            Return mpreAllowtoSetInactiveEmployeeApprover
        End Get
    End Property

    Public ReadOnly Property _AllowToSetActiveEmployeeApprover() As Boolean
        Get
            Return mpreAllowToSetActiveEmployeeApprover
        End Get
    End Property

    Public ReadOnly Property _AllowToPerformEmpSubmitForApproval() As Boolean
        Get
            Return mpreAllowToPerformEmpSubmitForApproval
        End Get
    End Property

    'S.SANDEEP [20-JUN-2018] -- End

    'Gajanan [13-AUG-2018] -- Start
    'Enhancement : Implementing Grievance Module
    Public ReadOnly Property _AllowtoApproveGrievance() As Boolean
        Get
            Return mpreAllowtoApproveGrievance
        End Get

    End Property

    Public ReadOnly Property _AllowToAddGrievanceApproverLevel() As Boolean
        Get
            Return mpreAllowToAddGrievanceApproverLevel
        End Get

    End Property

    Public ReadOnly Property _AllowToEditGrievanceApproverLevel() As Boolean
        Get
            Return mpreAllowToEditGrievanceApproverLevel
        End Get

    End Property

    Public ReadOnly Property _AllowToDeleteGrievanceApproverLevel() As Boolean
        Get
            Return mpreAllowToDeleteGrievanceApproverLevel
        End Get

    End Property

    Public ReadOnly Property _AllowToViewGrievanceApproverLevel() As Boolean
        Get
            Return mpreAllowToViewGrievanceApproverLevel
        End Get

    End Property

    Public ReadOnly Property _AllowToAddGrievanceApprover() As Boolean
        Get
            Return mpreAllowToAddGrievanceApprover
        End Get

    End Property

    Public ReadOnly Property _AllowToEditGrievanceApprover() As Boolean
        Get
            Return mpreAllowToEditGrievanceApprover
        End Get

    End Property

    Public ReadOnly Property _AllowToDeleteGrievanceApprover() As Boolean
        Get
            Return mpreAllowToDeleteGrievanceApprover
        End Get

    End Property

    Public ReadOnly Property _AllowToViewGrievanceApprover() As Boolean
        Get
            Return mpreAllowToViewGrievanceApprover
        End Get

    End Property

    Public ReadOnly Property _AllowToActivateGrievanceApprover() As Boolean
        Get
            Return mpreAllowToActivateGrievanceApprover
        End Get

    End Property

    Public ReadOnly Property _AllowToInActivateGrievanceApprover() As Boolean
        Get
            Return mpreAllowToInActivateGrievanceApprover
        End Get

    End Property

    'Gajanan [13-AUG-2018] -- End


    'Hemant (27 Aug 2018) -- Start
    'Enhancement : Implementing Grievance Module
    Public ReadOnly Property _AllowToViewGrievanceResolutionStepList() As Boolean
        Get
            Return mpreAllowToViewGrievanceResolutionStepList
        End Get

    End Property

    Public ReadOnly Property _AllowToAddGrievanceResolutionStep() As Boolean
        Get
            Return mpreAllowToAddGrievanceResolutionStep
        End Get

    End Property

    Public ReadOnly Property _AllowToEditGrievanceResolutionStep() As Boolean
        Get
            Return mpreAllowToEditGrievanceResolutionStep
        End Get

    End Property

    Public ReadOnly Property _AllowToDeleteGrievanceResolutionStep() As Boolean
        Get
            Return mpreAllowToDeleteGrievanceResolutionStep
        End Get

    End Property

    'Hemant (27 Aug 2018)) -- End

    'S.SANDEEP [09-OCT-2018] -- START
    Public ReadOnly Property _AllowToAddTrainingApproverLevel() As Boolean
        Get
            Return mpreAllowToAddTrainingApproverLevel
        End Get
    End Property
    Public ReadOnly Property _AllowToEditTrainingApproverLevel() As Boolean
        Get
            Return mpreAllowToEditTrainingApproverLevel
        End Get
    End Property
    Public ReadOnly Property _AllowToDeleteTrainingApproverLevel() As Boolean
        Get
            Return mpreAllowToDeleteTrainingApproverLevel
        End Get
    End Property
    Public ReadOnly Property _AllowToViewTrainingApproverLevel() As Boolean
        Get
            Return mpreAllowToViewTrainingApproverLevel
        End Get
    End Property
    Public ReadOnly Property _AllowToAddTrainingApprover() As Boolean
        Get
            Return mpreAllowToAddTrainingApprover
        End Get
    End Property
    Public ReadOnly Property _AllowToDeleteTrainingApprover() As Boolean
        Get
            Return mpreAllowToDeleteTrainingApprover
        End Get
    End Property
    Public ReadOnly Property _AllowToViewTrainingApprover() As Boolean
        Get
            Return mpreAllowToViewTrainingApprover
        End Get
    End Property
    Public ReadOnly Property _AllowToApproveTrainingRequisition() As Boolean
        Get
            Return mpreAllowToApproveTrainingRequisition
        End Get
    End Property
    Public ReadOnly Property _AllowToActivateTrainingApprover() As Boolean
        Get
            Return mpreAllowToActivateTrainingApprover
        End Get
    End Property
    Public ReadOnly Property _AllowToDeactivateTrainingApprover() As Boolean
        Get
            Return mpreAllowToDeactivateTrainingApprover
        End Get
    End Property
    'S.SANDEEP [09-OCT-2018] -- END

    'Hemant (22 Oct 2018) -- Start
    'Enhancement : Implementing New Module of OT Requisition Approver Level & Approver Master
    Public ReadOnly Property _AllowToViewOTRequisitionApproverLevel() As Boolean
        Get
            Return mpreAllowToViewOTRequisitionApproverLevel
        End Get

    End Property

    Public ReadOnly Property _AllowToAddOTRequisitionApproverLevel() As Boolean
        Get
            Return mpreAllowToAddOTRequisitionApproverLevel
        End Get

    End Property


    Public ReadOnly Property _AllowToEditOTRequisitionApproverLevel() As Boolean
        Get
            Return mpreAllowToEditOTRequisitionApproverLevel
        End Get

    End Property


    Public ReadOnly Property _AllowToDeleteOTRequisitionApproverLevel() As Boolean
        Get
            Return mpreAllowToDeleteOTRequisitionApproverLevel
        End Get

    End Property

    Public ReadOnly Property _AllowToAddOTRequisitionApprover() As Boolean
        Get
            Return mpreAllowToAddOTRequisitionApprover
        End Get

    End Property

    Public ReadOnly Property _AllowToEditOTRequisitionApprover() As Boolean
        Get
            Return mpreAllowToEditOTRequisitionApprover
        End Get

    End Property

    Public ReadOnly Property _AllowToDeleteOTRequisitionApprover() As Boolean
        Get
            Return mpreAllowToDeleteOTRequisitionApprover
        End Get

    End Property

    Public ReadOnly Property _AllowToViewOTRequisitionApprover() As Boolean
        Get
            Return mpreAllowToViewOTRequisitionApprover
        End Get

    End Property

    Public ReadOnly Property _AllowToActivateOTRequisitionApprover() As Boolean
        Get
            Return mpreAllowToActivateOTRequisitionApprover
        End Get

    End Property

    Public ReadOnly Property _AllowToInActivateOTRequisitionApprover() As Boolean
        Get
            Return mpreAllowToInActivateOTRequisitionApprover
        End Get

    End Property
    Public ReadOnly Property _AllowtoApproveOTRequisition() As Boolean
        Get
            Return mpreAllowtoApproveOTRequisition
        End Get

    End Property
    'Hemant (22 Oct 2018) -- End


    'Pinkal (03-Dec-2018) -- Start
    'Enhancement - Working on Asset Declaration Notification settings and notification from reflex.
    Public ReadOnly Property _AllowtoUnlockEmployeeForAssetDeclaration() As Boolean
        Get
            Return mpreAllowtoUnlockEmployeeForAssetDeclaration
        End Get
    End Property
    'Pinkal (03-Dec-2018) -- End

    'Sohail (04 Feb 2020) -- Start
    'NMB Enhancement # : New screen "Non-Disclosure Declaration" with lock / unlock options.
    Public ReadOnly Property _AllowtoUnlockEmployeeForNonDisclosureDeclaration() As Boolean
        Get
            Return mpreAllowtoUnlockEmployeeForNonDisclosureDeclaration
        End Get
    End Property
    'Sohail (04 Feb 2020) -- End

    'Gajanan [17-DEC-2018] -- Start
    'Enhancement - Implementing Employee Approver Flow On Employee Data.
    Public ReadOnly Property _AllowToApproveRejectEmployeeQualifications() As Boolean
        Get
            Return mpreAllowToApproveRejectEmployeeQualifications
        End Get
    End Property

    Public ReadOnly Property _AllowToApproveRejectEmployeeReferences() As Boolean
        Get
            Return mpreAllowToApproveRejectEmployeeReferences
        End Get
    End Property

    Public ReadOnly Property _AllowToApproveRejectEmployeeSkills() As Boolean
        Get
            Return mpreAllowToApproveRejectEmployeeSkills
        End Get
    End Property

    Public ReadOnly Property _AllowToApproveRejectEmployeeJobExperiences() As Boolean
        Get
            Return mpreAllowToApproveRejectEmployeeJobExperiences
        End Get
    End Property
    'Gajanan [17-DEC-2018] -- End


    'Gajanan [22-Feb-2019] -- Start
    'Enhancement - Implementing Employee Approver Flow On Employee Data.
    Public ReadOnly Property _AllowToApproveRejectEmployeeDependant() As Boolean
        Get
            Return mpreAllowToApproveRejectEmployeeDependants
        End Get
    End Property

    Public ReadOnly Property _AllowToApproveRejectEmployeeIdentities() As Boolean
        Get
            Return mpreAllowToApproveRejectEmployeeIdentities
        End Get
    End Property

    Public ReadOnly Property _AllowToApproveRejectEmployeeMembership() As Boolean
        Get
            Return mpreAllowToApproveRejectEmployeeMemberships
        End Get
    End Property

    Public ReadOnly Property _AllowToApproveRejectEmployeeBenefits() As Boolean
        Get
            Return mpreAllowToApproveRejectEmployeeBenefits
        End Get
    End Property
    'Gajanan [22-Feb-2019] -- End


    'Gajanan [18-Mar-2019] -- Start
    'Enhancement - Implementing Employee Approver Flow On Employee Data.

    Public ReadOnly Property _AllowToApproveRejectEmployeeAddress() As Boolean
        Get
            Return mpreAllowToApproveRejectEmployeeAddress
        End Get
    End Property
    Public ReadOnly Property _AllowToApproveRejectEmployeeEmergencyAddress() As Boolean
        Get
            Return mpreAllowToApproveRejectEmployeeEmergencyAddress
        End Get
    End Property
    'Gajanan [18-Mar-2019] -- End


    'Gajanan [17-April-2019] -- Start
    'Enhancement - Implementing Employee Approver Flow On Employee Data.
    Public ReadOnly Property _AllowToApproveRejectEmployeePersonalinfo() As Boolean
        Get
            Return mpreAllowToApproveRejectEmployeePersonalinfo
        End Get
    End Property

    'Gajanan [17-April-2019] -- End




    'S.SANDEEP |12-FEB-2019| -- START
    'ISSUE/ENHANCEMENT : {Performance Assessment Changes}
    Private mpreAllowToDeletePercentCompletedEmployeeGoals As Boolean = False
    Private mpreAllowToViewPercentCompletedEmployeeGoals As Boolean = False

    Public ReadOnly Property _AllowToDeletePercentCompletedEmployeeGoals() As Boolean
        Get
            Return mpreAllowToDeletePercentCompletedEmployeeGoals
        End Get
    End Property

    Public ReadOnly Property _AllowToViewPercentCompletedEmployeeGoals() As Boolean
        Get
            Return mpreAllowToViewPercentCompletedEmployeeGoals
        End Get
    End Property
    'S.SANDEEP |12-FEB-2019| -- END

    'Sohail (12 Apr 2019) -- Start
    'NMB Enhancement - 76.1 - Store System Errors in database and allow to resend and archieve system errors.
    Private mpreAllowToArchiveSystemErrorLog As Boolean = False
    Public ReadOnly Property _AllowToArchiveSystemErrorLog() As Boolean
        Get
            Return mpreAllowToArchiveSystemErrorLog
        End Get
    End Property

    Private mpreAllowToSendSystemErrorLogEmail As Boolean = False
    Public ReadOnly Property _AllowToSendSystemErrorLogEmail() As Boolean
        Get
            Return mpreAllowToSendSystemErrorLogEmail
        End Get
    End Property

    Private mpreAllowToExportSystemErrorLog As Boolean = False
    Public ReadOnly Property _AllowToExportSystemErrorLog() As Boolean
        Get
            Return mpreAllowToExportSystemErrorLog
        End Get
    End Property

    Private mpreAllowToViewSystemErrorLog As Boolean = False
    Public ReadOnly Property _AllowToViewSystemErrorLog() As Boolean
        Get
            Return mpreAllowToViewSystemErrorLog
        End Get
    End Property
    'Sohail (12 Apr 2019) -- End

    'Sohail (18 May 2019) -- Start
    'NMB Enhancement - REF # - 76.1 - On dependants menu, provide a way for user to set status of dependant as deceased etc. Allow user to deactivate a dependant with a reason instead of deleting.
    Private mpreAllowToSetDependentActive As Boolean = False
    Public ReadOnly Property _AllowToSetDependentActive() As Boolean
        Get
            Return mpreAllowToSetDependentActive
        End Get
    End Property

    Private mpreAllowToSetDependentInactive As Boolean = False
    Public ReadOnly Property _AllowToSetDependentInactive() As Boolean
        Get
            Return mpreAllowToSetDependentInactive
        End Get
    End Property

    Private mpreAllowToDeleteDependentStatus As Boolean = False
    Public ReadOnly Property _AllowToDeleteDependentStatus() As Boolean
        Get
            Return mpreAllowToDeleteDependentStatus
        End Get
    End Property

    Private mpreAllowToViewDependentStatus As Boolean = False
    Public ReadOnly Property _AllowToViewDependentStatus() As Boolean
        Get
            Return mpreAllowToViewDependentStatus
        End Get
    End Property
    'Sohail (18 May 2019) -- End

    'Sohail (24 Jun 2019) -- Start
    'NMB Enhancement - REF # - 76.1 - TC012: There should be payroll authorization check before payroll batch posting to flex cube.
    Private mpreAllowToViewJVPosting As Boolean = False
    Public ReadOnly Property _AllowToViewJVPosting() As Boolean
        Get
            Return mpreAllowToViewJVPosting
        End Get
    End Property
    'Sohail (24 Jun 2019) -- End

'S.SANDEEP |27-MAY-2019| -- START
    'ISSUE/ENHANCEMENT : [Score Calibration Process in Performance & Assessment Module]
    Public ReadOnly Property _AllowToViewCalibrationApproverLevel() As Boolean
        Get
            Return mpreAllowToViewCalibrationApproverLevel
        End Get
    End Property
    Public ReadOnly Property _AllowToAddCalibrationApproverLevel() As Boolean
        Get
            Return mpreAllowToAddCalibrationApproverLevel
        End Get
    End Property
    Public ReadOnly Property _AllowToEditCalibrationApproverLevel() As Boolean
        Get
            Return mpreAllowToEditCalibrationApproverLevel
        End Get
    End Property
    Public ReadOnly Property _AllowToDeleteCalibrationApproverLevel() As Boolean
        Get
            Return mpreAllowToDeleteCalibrationApproverLevel
        End Get
    End Property
    Public ReadOnly Property _AllowToViewCalibrationApproverMaster() As Boolean
        Get
            Return mpreAllowToViewCalibrationApproverMaster
        End Get
    End Property
    Public ReadOnly Property _AllowToAddCalibrationApproverMaster() As Boolean
        Get
            Return mpreAllowToAddCalibrationApproverMaster
        End Get
    End Property
    Public ReadOnly Property _AllowToActivateCalibrationApproverMaster() As Boolean
        Get
            Return mpreAllowToActivateCalibrationApproverMaster
        End Get
    End Property
    Public ReadOnly Property _AllowToDeleteCalibrationApproverMaster() As Boolean
        Get
            Return mpreAllowToDeleteCalibrationApproverMaster
        End Get
    End Property
    Public ReadOnly Property _AllowToDeactivateCalibrationApproverMaster() As Boolean
        Get
            Return mpreAllowToDeactivateCalibrationApproverMaster
        End Get
    End Property
    Public ReadOnly Property _AllowToApproveRejectCalibratedScore() As Boolean
        Get
            Return mpreAllowToApproveRejectCalibratedScore
        End Get
    End Property
    'S.SANDEEP |27-MAY-2019| -- END


    'Pinkal (27-Jun-2019) -- Start
    'Enhancement - IMPLEMENTING OT MODULE.
   Public ReadOnly Property _AllowtoViewOTRequisitionApprovalList() As Boolean
        Get
            Return mpreAllowtoViewOTRequisitionApprovalList
        End Get
    End Property
   'Pinkal (27-Jun-2019) -- End


    'S.SANDEEP |27-JUL-2019| -- START
    'ISSUE/ENHANCEMENT : CALIBRATION COMMENTS FROM NMB
    Public ReadOnly Property _AllowToEditCalibratedScore() As Boolean
        Get
            Return mpreAllowToEditCalibratedScore
        End Get
    End Property
    'S.SANDEEP |27-JUL-2019| -- END

    'Pinkal (13-Aug-2019) -- Start
    'Enhancement [0003693 - PACT] - Working on Allow people to exceed time assigned to the project initially.

    Public ReadOnly Property _AllowToViewBudgetTimesheetExemptEmployeeList() As Boolean
        Get
            Return mpreAllowToViewBudgetTimesheetExemptEmployeeList
        End Get
    End Property

    Public ReadOnly Property _AllowToAddBudgetTimesheetExemptEmployee() As Boolean
        Get
            Return mpreAllowToAddBudgetTimesheetExemptEmployee
        End Get
    End Property

    Public ReadOnly Property _AllowToDeleteBudgetTimesheetExemptEmployee() As Boolean
        Get
            Return mpreAllowToDeleteBudgetTimesheetExemptEmployee
        End Get
    End Property

    'Pinkal (13-Aug-2019) -- End

    'Sohail (29 Aug 2019) -- Start
    'NMB Payroll UAT # TC008 - 76.1 - System should be able to restrict batch posting when it is holiday unless it is activated by user to allow batch posting on holidays.
    Private mpreAllowToPostJVOnHolidays As Boolean = False
    Public ReadOnly Property _AllowToPostJVOnHolidays() As Boolean
        Get
            Return mpreAllowToPostJVOnHolidays
        End Get
    End Property
    'Sohail (29 Aug 2019) -- End

'S.SANDEEP |16-AUG-2019| -- START
    'ISSUE/ENHANCEMENT : ZRA {Language & Custom Item} & NMB Email Notification {Calibration}
    Public ReadOnly Property _AllowtoCalibrateProvisionalScore() As Boolean
        Get
            Return mpreAllowtoCalibrateProvisionalScore
        End Get
    End Property
    'S.SANDEEP |16-AUG-2019| -- END

    'Gajanan [18-OCT-2019] -- Start    
    'Enhancement:Provide Privileges "allowto Add/edit Opertional privileges".
        Private mpreAllowToAddEditOprationalPrivileges As Boolean = False
    Public ReadOnly Property _AllowToAddEditOprationalPrivileges() As Boolean
        Get
            Return mpreAllowToAddEditOprationalPrivileges
        End Get
    End Property
    'Gajanan [18-OCT-2019] -- End


    'Pinkal (08-Jan-2020) -- Start
    'Enhancement - NMB - Working on NMB OT Requisition Requirement.
    Private mpreAllowToViewEmpOTAssignment As Boolean = False
    Public ReadOnly Property _AllowToViewEmpOTAssignment() As Boolean
        Get
            Return mpreAllowToViewEmpOTAssignment
        End Get
    End Property

    Private mpreAllowToAddEmpOTAssignment As Boolean = False
    Public ReadOnly Property _AllowToAddEmpOTAssignment() As Boolean
        Get
            Return mpreAllowToAddEmpOTAssignment
        End Get
    End Property

    Private mpreAllowToDeleteEmpOTAssignment As Boolean = False
    Public ReadOnly Property _AllowToDeleteEmpOTAssignment() As Boolean
        Get
            Return mpreAllowToDeleteEmpOTAssignment
        End Get
    End Property

    Private mpreAllowToCancelEmpOTRequisition As Boolean = False
    Public ReadOnly Property _AllowToCancelEmpOTRequisition() As Boolean
        Get
            Return mpreAllowToCancelEmpOTRequisition
        End Get
    End Property
    'Pinkal (08-Jan-2020) -- End

    'S.SANDEEP |18-JAN-2020| -- START
    'ISSUE/ENHANCEMENT : PA-OPTIMIZATION
    Public ReadOnly Property _AllowToUnlockEmployeeInPlanning() As Boolean
        Get
            Return mpreAllowToUnlockEmployeeInPlanning
        End Get
    End Property
    Public ReadOnly Property _AllowToUnlockEmployeeInAssessment() As Boolean
        Get
            Return mpreAllowToUnlockEmployeeInAssessment
        End Get
    End Property
    'S.SANDEEP |18-JAN-2020| -- END

    'Pinkal (29-Jan-2020) -- Start
    'Enhancement - Changes related To OT NMB Testing.
    Public ReadOnly Property _AllowToPostOTRequisitionToPayroll() As Boolean
        Get
            Return mpreAllowToPostOTRequisitionToPayroll
        End Get
    End Property
    'Pinkal (29-Jan-2020) -- End


    'Pinkal (03-Mar-2020) -- Start
    'ENHANCEMENT NMB:  Working on OT Requisition Approver Migration and transfer and recategorization migration enforcement screen.
    Public ReadOnly Property _AllowToMigrateOTRequisitionApprover() As Boolean
        Get
            Return mpreAllowToMigrateOTRequisitionApprover
        End Get
    End Property
    'Pinkal (03-Mar-2020) -- End

    'S.SANDEEP |01-MAY-2020| -- START
    'ISSUE/ENHANCEMENT : CALIBRATION MAKEOVER
    Public ReadOnly Property _AllowToViewCalibratorList() As Boolean
        Get
            Return mpreAllowToViewCalibratorList
        End Get
    End Property
    Public ReadOnly Property _AllowToAddCalibrator() As Boolean
        Get
            Return mpreAllowToAddCalibrator
        End Get
    End Property
    Public ReadOnly Property _AllowToMakeCalibratorActive() As Boolean
        Get
            Return mpreAllowToMakeCalibratorActive
        End Get
    End Property
    Public ReadOnly Property _AllowToDeleteCalibrator() As Boolean
        Get
            Return mpreAllowToDeleteCalibrator
        End Get
    End Property
    Public ReadOnly Property _AllowToMakeCalibratorInactive() As Boolean
        Get
            Return mpreAllowToMakeCalibratorInactive
        End Get
    End Property
    Public ReadOnly Property _AllowToEditCalibrator() As Boolean
        Get
            Return mpreAllowToEditCalibrator
        End Get
    End Property
    Public ReadOnly Property _AllowToEditCalibrationApproverMaster() As Boolean
        Get
            Return mpreAllowToEditCalibrationApproverMaster
        End Get
    End Property
    'S.SANDEEP |01-MAY-2020| -- END


    'Gajanan [24-Aug-2020] -- Start
    'NMB Enhancement : Allow to set account configuration mapping 
    'inactive for closed period to allow to map head on other account 
    'configuration screen from new period

    Public ReadOnly Property _AllowToInactiveEmployeeCostCenter() As Boolean
        Get
            Return mpreAllowToInactiveEmployeeCostCenter
        End Get
    End Property

    Public ReadOnly Property _AllowToInactiveCompanyAccountConfiguration() As Boolean
        Get
            Return mpreAllowToInactiveCompanyAccountConfiguration
        End Get
    End Property
    
    Public ReadOnly Property _AllowToInactiveEmployeeAccountConfiguration() As Boolean
        Get
            Return mpreAllowToInactiveEmployeeAccountConfiguration
        End Get
    End Property

    Public ReadOnly Property _AllowToInactiveCostCenterAccountConfiguration() As Boolean
        Get
            Return mpreAllowToInactiveCostCenterAccountConfiguration
        End Get
    End Property
    'Gajanan [24-Aug-2020] -- End

    'Hemant (26 Nov 2020) -- Start
    'Enhancement : Talent Pipeline new changes
    Public ReadOnly Property _AllowToAddPotentialTalentEmployee() As Boolean
        Get
            Return mpreAllowToAddPotentialTalentEmployee
        End Get
    End Property

    Public ReadOnly Property _AllowToApproveRejectTalentPipelineEmployee() As Boolean
        Get
            Return mpreAllowToApproveRejectTalentPipelineEmployee
        End Get
    End Property

    Public ReadOnly Property _AllowToMoveApprovedEmployeeToQulified() As Boolean
        Get
            Return mpreAllowToMoveApprovedEmployeeToQulified
        End Get
    End Property

    Public ReadOnly Property _AllowToViewPotentialTalentEmployee() As Boolean
        Get
            Return mpreAllowToViewPotentialTalentEmployee
        End Get
    End Property

    Public ReadOnly Property _AllowToRemoveTalentProcess() As Boolean
        Get
            Return mpreAllowToRemoveTalentProcess
        End Get
    End Property

    'Hemant (26 Nov 2020) -- End

    'Gajanan [17-Dec-2020] -- Start
    Public ReadOnly Property _AllowToAddPotentialSuccessionEmployee() As Boolean
        Get
            Return mpreAllowToAddPotentialSuccessionEmployee
        End Get
    End Property

    Public ReadOnly Property _AllowToApproveRejectSuccessionPipelineEmployee() As Boolean
        Get
            Return mpreAllowToApproveRejectSuccessionPipelineEmployee
        End Get
    End Property

    Public ReadOnly Property _AllowToMoveApprovedEmployeeToQulifiedSuccession() As Boolean
        Get
            Return mpreAllowToMoveApprovedEmployeeToQulifiedSuccession
        End Get
    End Property

    Public ReadOnly Property _AllowToViewPotentialSuccessionEmployee() As Boolean
        Get
            Return mpreAllowToViewPotentialSuccessionEmployee
        End Get
    End Property

    Public ReadOnly Property _AllowToRemoveSuccessionProcess() As Boolean
        Get
            Return mpreAllowToRemoveSuccessionProcess
        End Get
    End Property
    'Gajanan [17-Dec-2020] -- End

    'Gajanan [19-Feb-2021] -- Start
    Public ReadOnly Property _AllowToSendNotificationToTalentScreener() As Boolean
        Get
            Return mpreAllowToSendNotificationToTalentScreener
        End Get
    End Property

    Public ReadOnly Property _AllowToSendNotificationToTalentEmployee() As Boolean
        Get
            Return mpreAllowToSendNotificationToTalentEmployee
        End Get
    End Property

    Public ReadOnly Property _AllowForTalentScreeningProcess() As Boolean
        Get
            Return mpreAllowForTalentScreeningProcess
        End Get
    End Property


    Public ReadOnly Property _AllowToSendNotificationToSuccessionScreener() As Boolean
        Get
            Return mpreAllowToSendNotificationToSuccessionScreener
        End Get
    End Property

    Public ReadOnly Property _AllowToSendNotificationToSuccessionEmployee() As Boolean
        Get
            Return mpreAllowToSendNotificationToSuccessionEmployee
        End Get
    End Property

    Public ReadOnly Property _AllowForSuccessionScreeningProcess() As Boolean
        Get
            Return mpreAllowForSuccessionScreeningProcess
        End Get
    End Property

    Public ReadOnly Property _AllowToSendNotificationToTalentApprover() As Boolean
        Get
            Return mpreAllowToSendNotificationToTalentApprover
        End Get
    End Property

    Public ReadOnly Property _AllowToSendNotificationToSuccessionApprover() As Boolean
        Get
            Return mpreAllowToSendNotificationToSuccessionApprover
        End Get
    End Property
    'Gajanan [19-Feb-2021] -- End

'Gajanan [30-JAN-2021] -- Start   
    'Enhancement:Worked On PDP Module
    Public ReadOnly Property _AllowToViewLearningAndDevelopmentPlan() As Boolean
        Get
            Return mpreAllowToViewLearningAndDevelopmentPlan
        End Get
    End Property

    Public ReadOnly Property _AllowToAddPersonalAnalysisGoal() As Boolean
        Get
            Return mpreAllowToAddPersonalAnalysisGoal
        End Get
    End Property

    Public ReadOnly Property _AllowToEditPersonalAnalysisGoal() As Boolean
        Get
            Return mpreAllowToEditPersonalAnalysisGoal
        End Get
    End Property

    Public ReadOnly Property _AllowToDeletePersonalAnalysisGoal() As Boolean
        Get
            Return mpreAllowToDeletePersonalAnalysisGoal
        End Get
    End Property

    Public ReadOnly Property _AllowToAddDevelopmentActionPlan() As Boolean
        Get
            Return mpreAllowToAddDevelopmentActionPlan
        End Get
    End Property

    Public ReadOnly Property _AllowToEditDevelopmentActionPlan() As Boolean
        Get
            Return mpreAllowToEditDevelopmentActionPlan
        End Get
    End Property

    Public ReadOnly Property _AllowToDeleteDevelopmentActionPlan() As Boolean
        Get
            Return mpreAllowToDeleteDevelopmentActionPlan
        End Get
    End Property

    Public ReadOnly Property _AllowToAddUpdateProgressForDevelopmentActionPlan() As Boolean
        Get
            Return mpreAllowToAddUpdateProgressForDevelopmentActionPlan
        End Get
    End Property
    'Gajanan [30-JAN-2021] -- End

    'Gajanan [26-Feb-2021] -- Start
    Public ReadOnly Property _AllowToNominateEmployeeForSuccession() As Boolean
        Get
            Return mpreAllowToNominateEmployeeForSuccession
        End Get
    End Property

    Public ReadOnly Property _AllowToModifyTalentSetting() As Boolean
        Get
            Return mpreAllowToModifyTalentSetting
        End Get
    End Property

    Public ReadOnly Property _AllowToViewTalentSetting() As Boolean
        Get
            Return mpreAllowToViewTalentSetting
        End Get
    End Property

    Public ReadOnly Property _AllowToModifySuccessionSetting() As Boolean
        Get
            Return mpreAllowToModifySuccessionSetting
        End Get
    End Property

    Public ReadOnly Property _AllowToViewSuccessionSetting() As Boolean
        Get
            Return mpreAllowToViewSuccessionSetting
        End Get
    End Property
    'Gajanan [26-Feb-2021] -- End


    'Pinkal (03-Mar-2021)-- Start
    'KADCO Enhancement  -  Working on  KADCO - Option to Adjust Expense Accrue Balance.
    Public ReadOnly Property _AllowToAdjustExpenseBalance() As Boolean
        Get
            Return mpreAllowToAdjustExpenseBalance
        End Get
    End Property
    'Pinkal (03-Mar-2021) -- End


    'Pinkal (10-Mar-2021) -- Start
    'Enhancement/Bug Retirement -   Working Retirement enhancement/Bug.

    Public ReadOnly Property _AllowToViewRetireClaimApplication() As Boolean
        Get
            Return mpreAllowToViewRetireClaimApplication
        End Get
    End Property

    Public ReadOnly Property _AllowToRetireClaimApplication() As Boolean
        Get
            Return mpreAllowToRetireClaimApplication
        End Get
    End Property

    Public ReadOnly Property _AllowToEditRetiredClaimApplication() As Boolean
        Get
            Return mpreAllowToEditRetiredClaimApplication
        End Get
    End Property

    Public ReadOnly Property _AllowToViewRetiredApplicationApproval() As Boolean
        Get
            Return mpreAllowToViewRetiredApplicationApproval
        End Get
    End Property

    Public ReadOnly Property _AllowToApproveRetiredApplication() As Boolean
        Get
            Return mpreAllowToApproveRetiredApplication
        End Get
    End Property

    Public ReadOnly Property _AllowToPostRetiredApplicationtoPayroll() As Boolean
        Get
            Return mpreAllowToPostRetiredApplicationtoPayroll
        End Get
    End Property

    'Pinkal (10-Mar-2021) -- End


 Public ReadOnly Property _AllowToViewTalentScreeningDetail() As Boolean
        Get
            Return mpreAllowToViewTalentScreeningDetail
        End Get
    End Property

    Public ReadOnly Property _AllowToViewSuccessionScreeningDetail() As Boolean
        Get
            Return mpreAllowToViewSuccessionScreeningDetail
        End Get
    End Property

    'Sohail (01 Mar 2021) -- Start
    'NMB Enhancement : : New screen Departmental Training Need in New UI.
    Public ReadOnly Property _AllowToViewDepartmentalTrainingNeed() As Boolean
        Get
            Return mpreAllowToViewDepartmentalTrainingNeed
        End Get
    End Property

    Public ReadOnly Property _AllowToAddDepartmentalTrainingNeed() As Boolean
        Get
            Return mpreAllowToAddDepartmentalTrainingNeed
        End Get
    End Property

    Public ReadOnly Property _AllowToEditDepartmentalTrainingNeed() As Boolean
        Get
            Return mpreAllowToEditDepartmentalTrainingNeed
        End Get
    End Property

    Public ReadOnly Property _AllowToDeleteDepartmentalTrainingNeed() As Boolean
        Get
            Return mpreAllowToDeleteDepartmentalTrainingNeed
        End Get
    End Property
    'Sohail (01 Mar 2021) -- End

    'Hemant (26 Mar 2021) -- Start
    'NMB Enhancement : : Changes in Departmental Training Need as per comments in online doc in New UI.
    Public ReadOnly Property _AllowToSubmitForApprovalFromDepartmentalTrainingNeed() As Boolean
        Get
            Return mpreAllowToSubmitForApprovalFromDepartmentalTrainingNeed
        End Get
    End Property

    Public ReadOnly Property _AllowToTentativeApproveForDepartmentalTrainingNeed() As Boolean
        Get
            Return mpreAllowToTentativeApproveForDepartmentalTrainingNeed
        End Get
    End Property

    Public ReadOnly Property _AllowToSubmitForApprovalFromTrainingBacklog() As Boolean
        Get
            Return mpreAllowToSubmitForApprovalFromTrainingBacklog
        End Get
    End Property

    Public ReadOnly Property _AllowToFinalApproveDepartmentalTrainingNeed() As Boolean
        Get
            Return mpreAllowToFinalApproveDepartmentalTrainingNeed
        End Get
    End Property

    Public ReadOnly Property _AllowToRejectDepartmentalTrainingNeed() As Boolean
        Get
            Return mpreAllowToRejectDepartmentalTrainingNeed
        End Get
    End Property

    Public ReadOnly Property _AllowToUnlockSubmitApprovalForDepartmentalTrainingNeed() As Boolean
        Get
            Return mpreAllowToUnlockSubmitApprovalForDepartmentalTrainingNeed
        End Get
    End Property

    Public ReadOnly Property _AllowToUnlockSubmittedForTrainingBudgetApproval() As Boolean
        Get
            Return mpreAllowToUnlockSubmittedForTrainingBudgetApproval
        End Get
    End Property

    Public ReadOnly Property _AllowToAskForReviewTrainingAsPerAmountSet() As Boolean
        Get
            Return mpreAllowToAskForReviewTrainingAsPerAmountSet
        End Get
    End Property

    Public ReadOnly Property _AllowToUndoApprovedTrainingBudgetApproval() As Boolean
        Get
            Return mpreAllowToUndoApprovedTrainingBudgetApproval
        End Get
    End Property
    'Hemant (26 Mar 2021) -- End

    Public ReadOnly Property _AllowToAddCommentForDevelopmentActionPlan() As Boolean
        Get
            Return mpreAllowToAddCommentForDevelopmentActionPlan
        End Get
    End Property

    Public ReadOnly Property _AllowToViewActionPlanAssignedTrainingCourses() As Boolean
        Get
            Return mpreAllowToViewActionPlanAssignedTrainingCourses
        End Get
    End Property

    'Sohail (12 Apr 2021) -- Start
    'NMB Enhancement : : Changes in Departmental Training Need as per comments in online doc in New UI.
    Public ReadOnly Property _AllowToUndoRejectedTrainingBudgetApproval() As Boolean
        Get
            Return mpreAllowToUndoRejectedTrainingBudgetApproval
        End Get
    End Property
    'Sohail (12 Apr 2021) -- End

    'Sohail (26 Apr 2021) -- Start
    'NMB Enhancement : : Changes in Departmental Training Need as per comments in online doc in New UI.
    Public ReadOnly Property _AllowToViewBudgetSummaryForDepartmentalTrainingNeed() As Boolean
        Get
            Return mpreAllowToViewBudgetSummaryForDepartmentalTrainingNeed
        End Get
    End Property

    Public ReadOnly Property _AllowToSetMaxBudgetForDepartmentalTrainingNeed() As Boolean
        Get
            Return mpreAllowToSetMaxBudgetForDepartmentalTrainingNeed
        End Get
    End Property
    'Sohail (26 Apr 2021) -- End

    'Hemant (15 Apr 2021) -- Start
    'NMB Enhancement : : Approvers concepts need to be revisited. Should be based on the concept of Allocations and no on employee assignment.
    Public ReadOnly Property _AllowToApproveTrainingRequestRelatedToCost() As Boolean
        Get
            Return mpreAllowToApproveTrainingRequestRelatedToCost
        End Get
    End Property

    Public ReadOnly Property _AllowToApproveTrainingRequestForeignTravelling() As Boolean
        Get
            Return mpreAllowToApproveTrainingRequestForeignTravelling
        End Get
    End Property
    'Hemant (15 Apr 2021) -- End

Public ReadOnly Property _AllowToUnlockPDPForm() As Boolean
        Get
            Return mpreAllowToUnlockPDPForm
        End Get
    End Property

    Public ReadOnly Property _AllowToLockPDPForm() As Boolean
        Get
            Return mpreAllowToLockPDPForm
        End Get
    End Property

    Public ReadOnly Property _AllowToUndoUpdateProgressForDevelopmentActionPlan() As Boolean
        Get
            Return mpreAllowToUndoUpdateProgressForDevelopmentActionPlan
        End Get
    End Property

    'Hemant (18 May 2021) -- Start
    'ISSUE/ENHANCEMENT : Changed approval flow for Training Completion Status
    Public ReadOnly Property _AllowToApproveRejectTrainingCompletion() As Boolean
        Get
            Return mpreAllowToApproveRejectTrainingCompletion
        End Get
    End Property

    Public ReadOnly Property _AllowToAddAttendedTraining() As Boolean
        Get
            Return mpreAllowToAddAttendedTraining
        End Get
    End Property
    'Hemant (18 May 2021) -- End

    'Hemant (25 May 2021) -- Start
    'ISSUE/ENHANCEMENT : OLD-398 - NMB COE Modules demo feedback for Training Modulue
    Public ReadOnly Property _AllowToMarkTrainingAsComplete() As Boolean
        Get
            Return mpreAllowToMarkTrainingAsComplete
        End Get
    End Property
    'Hemant (25 May 2021) -- End


    'S.SANDEEP |10-MAR-2022| -- START
    'ISSUE/ENHANCEMENT : OLD-580
    Private mpreAllowToAddRebateForNonEmployee As Boolean = False
    Public ReadOnly Property _AllowToAddRebateForNonEmployee() As Boolean
        Get
            Return mpreAllowToAddRebateForNonEmployee
        End Get
    End Property
    'S.SANDEEP |10-MAR-2022| -- END


    'Hemant (29 Apr 2022) -- Start
    'ISSUE/ENHANCEMENT : AC2-314(NMB) - NMB - As a manager, I want different approval flow (Training requests) for different training courses.
    Public ReadOnly Property _AllowToApproveTrainingRequestLocalTravelling() As Boolean
        Get
            Return mpreAllowToApproveTrainingRequestLocalTravelling
        End Get
    End Property
    'Hemant (29 Apr 2022) -- End

    'Hemant (11 Jul 2022) -- Start            
    'ENHANCEMENT(NMB) : AC2-720 - Training approver add/edit with User Mapping
    Public ReadOnly Property _AllowToViewTrainingApproverEmployeeMappingList() As Boolean
        Get
            Return mpreAllowToViewTrainingApproverEmployeeMappingList
        End Get
    End Property

    Public ReadOnly Property _AllowToEditTrainingApproverEmployeeMapping() As Boolean
        Get
            Return mpreAllowToEditTrainingApproverEmployeeMapping
        End Get
    End Property

    Public ReadOnly Property _AllowToDeleteTrainingApproverEmployeeMapping() As Boolean
        Get
            Return mpreAllowToDeleteTrainingApproverEmployeeMapping
        End Get
    End Property

    Public ReadOnly Property _AllowToSetActiveInactiveTrainingApproverEmployeeMapping() As Boolean
        Get
            Return mpreAllowToSetActiveInactiveTrainingApproverEmployeeMapping
        End Get
    End Property

    Public ReadOnly Property _AllowToAddTrainingApproverEmployeeMapping() As Boolean
        Get
            Return mpreAllowToAddTrainingApproverEmployeeMapping
        End Get
    End Property
    'Hemant (11 Jul 2022) -- End

    'Pinkal (27-Oct-2022) -- Start
    'NMB Loan Module Enhancement.
    Public ReadOnly Property _AllowToChangeDeductionPeriodOnLoanApproval() As Boolean
        Get
            Return mpreAllowToChangeDeductionPeriodOnLoanApproval
        End Get
    End Property
    'Pinkal (27-Oct-2022) -- End

    'Pinkal (23-Nov-2022) -- Start
    'NMB Loan Module Enhancement.
    Public ReadOnly Property _AllowToAccessLoanDisbursementDashboard() As Boolean
        Get
            Return mpreAllowToAccessLoanDisbursementDashboard
        End Get
    End Property

    Public ReadOnly Property _AllowToGetCRBData() As Boolean
        Get
            Return mpreAllowToGetCRBData
        End Get
    End Property
    'Pinkal (23-Nov-2022) -- End

    'Pinkal (14-Dec-2022) -- Start
    'NMB Loan Module Enhancement.
    Public ReadOnly Property _AllowToSetDisbursementTranches() As Boolean
        Get
            Return mpreAllowToSetDisbursementTranches
        End Get
    End Property
    'Pinkal (14-Dec-2022) -- End

    'Pinkal (06-Jan-2023) -- Start
    '(A1X-540) FHS/PSI - As a user, I want to select the cost center that applies to my project when filling my project budget timesheets.

    Public ReadOnly Property _AllowToViewLoanSchemeRoleMapping() As Boolean
        Get
            Return mpreAllowToViewLoanSchemeRoleMapping
        End Get
    End Property

    Public ReadOnly Property _AllowToaddLoanSchemeRoleMapping() As Boolean
        Get
            Return mpreAllowToAddLoanSchemeRoleMapping
        End Get
    End Property

    Public ReadOnly Property _AllowToEditLoanSchemeRoleMapping() As Boolean
        Get
            Return mpreAllowToEditLoanSchemeRoleMapping
        End Get
    End Property

    Public ReadOnly Property _AllowToDeleteLoanSchemeRoleMapping() As Boolean
        Get
            Return mpreAllowToDeleteLoanSchemeRoleMapping
        End Get
    End Property

    'Pinkal (06-Jan-2023) -- End

    'Pinkal (20-Feb-2023) -- Start
    'NMB - Loan Approval Screen Enhancements.
    Public ReadOnly Property _AllowToViewLoanExposuresOnLoanApproval() As Boolean
        Get
            Return mpreAllowToViewLoanExposuresOnLoanApproval
        End Get
    End Property
    'Pinkal (20-Feb-2023) -- End

    'Pinkal (31-Mar-2023) -- Start
    '(A1X-717) NMB - As a user, based on the data displayed from the selection, I want to be able to shoot emails from the Feedback screen instead of going to the mail utility again. I should be able to select the template directly from this screen and input the subject.
    Public ReadOnly Property _AllowToSendCandidateFeedback() As Boolean
        Get
            Return mpreAllowToSendCandidateFeedback
        End Get
    End Property
    'Pinkal (31-Mar-2023) -- End

    'Pinkal (28-Apr-2023) -- Start
    '(A1X-865) NMB - Read aptitude test results from provided views.
    Public ReadOnly Property _AllowToDownloadAptitudeResult() As Boolean
        Get
            Return mpreAllowToDownloadAptitudeResult
        End Get
    End Property
    'Pinkal (28-Apr-2023) -- End

    'Pinkal (03-Jun-2023) -- Start
    'NMB Enhancement - Adding Staff Loan Balance View privilege.
    Public ReadOnly Property _AllowToViewStaffLoanBalance() As Boolean
        Get
            Return mpreAllowToViewStaffLoanBalance
        End Get
    End Property
    'Pinkal (03-Jun-2023) -- End

 'S.SANDEEP |05-JUN-2023| -- START
    'ISSUE/ENHANCEMENT : A1X-948
    Public ReadOnly Property _AllowToApproveVacancyChanges() As Boolean
        Get
            Return mpreAllowToApproveVacancyChanges
        End Get
    End Property
    'S.SANDEEP |0-JUN-2023| -- END

    'Hemant (26 May 2023) -- Start
    'ENHANCEMENT(TRA) : A1X-943 - Integrate NBC payment API in Aruti
    Public ReadOnly Property _AllowToApproveRejectPaymentBatchPosting() As Boolean
        Get
            Return mpreAllowToApproveRejectPaymentBatchPosting
        End Get
    End Property
    'Hemant (26 May 2023) -- End


    'Pinkal (26-Jun-2023) -- Start
    'NMB Enhancement :(A1X-1029) NMB - Staff transfers approval(role based).
    Public ReadOnly Property _AllowToViewStaffTransferApproverLevel() As Boolean
        Get
            Return mpreAllowToViewStaffTransferApproverLevel
        End Get
    End Property

    Public ReadOnly Property _AllowToAddStaffTransferApproverLevel() As Boolean
        Get
            Return mpreAllowToAddStaffTransferApproverLevel
        End Get
    End Property

    Public ReadOnly Property _AllowToEditStaffTransferApproverLevel() As Boolean
        Get
            Return mpreAllowToEditStaffTransferApproverLevel
        End Get
    End Property

    Public ReadOnly Property _AllowToDeleteStaffTransferApproverLevel() As Boolean
        Get
            Return mpreAllowToDeleteStaffTransferApproverLevel
        End Get
    End Property

    Public ReadOnly Property _AllowToViewStaffTransferLevelRoleMapping() As Boolean
        Get
            Return mpreAllowToViewStaffTransferLevelRoleMapping
        End Get
    End Property

    Public ReadOnly Property _AllowToAddStaffTransferLevelRoleMapping() As Boolean
        Get
            Return mpreAllowToAddStaffTransferLevelRoleMapping
        End Get
    End Property

    Public ReadOnly Property _AllowToEditStaffTransferLevelRoleMapping() As Boolean
        Get
            Return mpreAllowToEditStaffTransferLevelRoleMapping
        End Get
    End Property

    Public ReadOnly Property _AllowToDeleteStaffTransferLevelRoleMapping() As Boolean
        Get
            Return mpreAllowToDeleteStaffTransferLevelRoleMapping
        End Get
    End Property

    Public ReadOnly Property _AllowToViewStaffTransferApprovalList() As Boolean
        Get
            Return mpreAllowToViewStaffTransferApprovalList
        End Get
    End Property

    Public ReadOnly Property _AllowToApproveStaffTransferApproval() As Boolean
        Get
            Return mpreAllowToApproveStaffTransferApproval
        End Get
    End Property

    'Pinkal (26-Jun-2023) -- End

    'Hemant (15 Sep 2023) -- Start
    'ENHANCEMENT(TRA): A1X-1257 - Coaches/coachees nomination request and approval
    Public ReadOnly Property _AllowToViewCoachingApproverLevel() As Boolean
        Get
            Return mpreAllowToViewCoachingApproverLevel
        End Get
    End Property

    Public ReadOnly Property _AllowToAddCoachingApproverLevel() As Boolean
        Get
            Return mpreAllowToAddCoachingApproverLevel
        End Get
    End Property

    Public ReadOnly Property _AllowToEditCoachingApproverLevel() As Boolean
        Get
            Return mpreAllowToEditCoachingApproverLevel
        End Get
    End Property

    Public ReadOnly Property _AllowToDeleteCoachingApproverLevel() As Boolean
        Get
            Return mpreAllowToDeleteCoachingApproverLevel
        End Get
    End Property

    Public ReadOnly Property _AllowToFillCoachingForm() As Boolean
        Get
            Return mpreAllowToFillCoachingForm
        End Get
    End Property

    Public ReadOnly Property _AllowToApproveCoachingForm() As Boolean
        Get
            Return mpreAllowToApproveCoachingForm
        End Get
    End Property

    Public ReadOnly Property _AllowToEditCoachingForm() As Boolean
        Get
            Return mpreAllowToEditCoachingForm
        End Get
    End Property

    Public ReadOnly Property _AllowToDeleteCoachingForm() As Boolean
        Get
            Return mpreAllowToDeleteCoachingForm
        End Get
    End Property

    Public ReadOnly Property _AllowToViewCoachingForm() As Boolean
        Get
            Return mpreAllowToViewCoachingForm
        End Get
    End Property

    Public ReadOnly Property _AllowToFillReplacementForm() As Boolean
        Get
            Return mpreAllowToFillReplacementForm
        End Get
    End Property
    'Hemant (15 Sep 2023) -- End

'Pinkal (30-Sep-2023) -- Start
    '(A1X-1371) Toyota - New desktop dashboard item: Total Work Anniversaries Within 7 Days from Now.
    Public ReadOnly Property _Show_ForcastedWorkAnniversary() As Boolean
        Get
            Return mpreShowForcastedWorkAnniversary
        End Get
    End Property

    Public ReadOnly Property _Show_ForcastedWorkResidencePermitExpiry() As Boolean
        Get
            Return mpreShowForcastedWorkResidencePermitExpiry
        End Get
    End Property

    Public ReadOnly Property _ShowTotalLeaversFromCurrentFY() As Boolean
        Get
            Return mpreShowTotalLeaversFromCurrentFY
        End Get
    End Property
    'Pinkal (30-Sep-2023) -- End


#End Region

#Region " Private Function "

    Public Sub setUserPrivilege(ByVal intUserUnkid As Integer)
        Dim dsList As New DataSet
        Dim i As Integer
        Try
            dsList = getUserPrivilegeList(intUserUnkid)
            For i = 0 To dsList.Tables(0).Rows.Count - 1
                Select Case dsList.Tables(0).Rows(i)("privilegeunkid")
                    Case 1
                        mpreAccessConfiguaration = True
                    Case 2
                        mpreChangeLanguage = True
                    Case 3
                        mpreCompanyCreation = True
                    Case 4
                        'S.SANDEEP [ 23 NOV 2012 ] -- START
                        'ENHANCEMENT : TRA CHANGES
                        'mpreUserCreation = True
                        mpreAllowtoViewUserList = True
                    Case 5
                        'mpreUserRole = True
                        mpreAllowtoViewAbilityLevel = True
                        'S.SANDEEP [ 23 NOV 2012 ] -- END
                    Case 6
                        mpreDatabaseBackup = True
                    Case 7
                        mpreDatabaseRestore = True
                    Case 8
                        mpreAddCommonMasters = True
                    Case 9
                        mpreEditCommonMaster = True
                    Case 10
                        mpreDeleteCommonMaster = True
                    Case 11
                        mpreAddTerminationReason = True
                    Case 12
                        mpreEditTerminationReason = True
                    Case 13
                        mpreDeleteTerminationReason = True
                    Case 14
                        mpreAddSkills = True
                    Case 15
                        mpreEditSkills = True
                    Case 16
                        mpreDeleteSkills = True
                    Case 17
                        mpreAddAdvertise = True
                    Case 18
                        mpreEditAdvertise = True
                    Case 19
                        mpreDeleteAdvertise = True
                    Case 20
                        mpreAddQualification_Course = True
                    Case 21
                        mpreEditQualification_Course = True
                    Case 22
                        mpreDeleteQualification_Course = True
                    Case 23
                        mpreAddResultCode = True
                    Case 24
                        mpreEditResultCode = True
                    Case 25
                        mpreDeleteResultCode = True
                    Case 26
                        mpreAddMembership = True
                    Case 27
                        mpreEditMembership = True
                    Case 28
                        mpreDeleteMembership = True
                    Case 29
                        mpreAddBenefitPlan = True
                    Case 30
                        mpreEditBenefitPlan = True
                    Case 31
                        mpreDeleteBenefitPlan = True
                        'Sohail (10 Jan 2020) -- Start
                        'NMB Enhancement # : New screen for Country list and allow to edit currency sign on country list.
                    Case enUserPriviledge.AllowToEditCurrencySign
                        mpreAllowToEditCurrencySign = True
                        'Sohail (10 Jan 2020) -- End
                    Case 32
                        mpreAddState = True
                    Case 33
                        mpreEditState = True
                    Case 34
                        mpreDeleteState = True
                    Case 35
                        mpreAddCity = True
                    Case 36
                        mpreEditCity = True
                    Case 37
                        mpreDeleteCity = True
                    Case 38
                        mpreAddZipCode = True
                    Case 39
                        mpreEditZipCode = True
                    Case 40
                        mpreDeleteZipCode = True
                    Case 41
                        mpreAddEmployee = True
                    Case 42
                        mpreEditEmployee = True
                    Case 43
                        mpreDeleteEmployee = True
                    Case 44
                        mpreAddDependant = True
                    Case 45
                        mpreEditDependant = True
                    Case 46
                        mpreDeleteDependant = True
                    Case 47
                        mpreAddBeneficiaries = True
                    Case 48
                        mpreEditBeneficiaries = True
                    Case 49
                        mpreDeleteBeneficiaries = True
                    Case 50
                        mpreAddBenefitAllocation = True
                    Case 51
                        mpreEditBenefitAllocation = True
                    Case 52
                        mpreDeleteBenefitAllocation = True

                        'Sohail (17 Sep 2019) -- Start
                        'NMB Enhancement # TC006: system should be able to bind earning/deduction (benefit) with specific allocation and automatically assign to employee as per his/her allocation.
                    Case enUserPriviledge.AllowToSetBenefitAllocationActive
                        mpreAllowToSetBenefitAllocationActive = True

                    Case enUserPriviledge.AllowToSetBenefitAllocationInactive
                        mpreAllowToSetBenefitAllocationInactive = True
                        'Sohail (17 Sep 2019) -- End

                        'Sohail (09 Oct 2019) -- Start
                        'NMB Enhancement # : privilege  Allow to change date on Closed Period on user master to change eoc / termination date in paid period or closed period.
                    Case enUserPriviledge.AllowToChangeEOCLeavingDateOnClosedPeriod
                        mpreAllowToChangeEOCLeavingDateOnClosedPeriod = True
                        'Sohail (09 Oct 2019) -- End

                        'Sohail (16 Oct 2019) -- Start
                        'NMB Enhancement # : Add privileges for "Allow To Post Flexcube JV To Oracle" to prevent erroneous postings by other users to Payroll GL accounts.
                    Case enUserPriviledge.AllowToPostFlexcubeJVToOracle
                        mpreAllowToPostFlexcubeJVToOracle = True
                        'Sohail (16 Oct 2019) -- End

                        'Sohail (16 Oct 2019) -- Start
                        'NMB Enhancement # : Privileges "Allow to view Paid Amount" to hide Amount on Approval/Authorize Screen for Automatic Integration between Payroll and Finance modules,Online Approval workflow for Payroll runs.
                    Case enUserPriviledge.AllowToViewPaidAmount
                        mpreAllowToViewPaidAmount = True
                        'Sohail (16 Oct 2019) -- End

                        'Sohail (18 Oct 2019) -- Start
                        'NMB Enhancement # : For Add/New/Edit employee system should allow to set appointment date to last closed period for It should be possible to set appointment date of an employee to past closed period by use of the first appointment date but during payroll process, consider 1st of the following month
                    Case enUserPriviledge.AllowToChangeAppointmentDateOnClosedPeriod
                        mpreAllowToChangeAppointmentDateOnClosedPeriod = True
                        'Sohail (18 Oct 2019) -- End

                        'Sohail (21 Oct 2019) -- Start
                        'NMB Enhancement # : New screen employee exemption will be added on employee movement with end date as optional with approval flow for It should be possible to update active employee status as Unpaid and automatically prorate payment of that employee effectively from the date selected. Also when employee is back, user should be able to update end date of the Unpaid status and automatically include that employee in payroll and prorate based on the date set
                    Case enUserPriviledge.AllowToApproveRejectEmployeeExemption
                        mpreAllowToApproveRejectEmployeeExemption = True

                    Case enUserPriviledge.AllowToSetEmployeeExemptionDate
                        mpreAllowToSetEmployeeExemptionDate = True

                    Case enUserPriviledge.AllowToEditEmployeeExemptionDate
                        mpreAllowToEditEmployeeExemptionDate = True

                    Case enUserPriviledge.AllowToDeleteEmployeeExemptionDate
                        mpreAllowToDeleteEmployeeExemptionDate = True
                        'Sohail (21 Oct 2019) -- End

                        'Sohail (14 Nov 2019) -- Start
                        'NMB UAT Enhancement # : New Screen Training Need Form.
                    Case enUserPriviledge.AllowToViewTrainingNeedForm
                        mpreAllowToViewTrainingNeedForm = True

                    Case enUserPriviledge.AllowToAddTrainingNeedForm
                        mpreAllowToAddTrainingNeedForm = True

                    Case enUserPriviledge.AllowToEditTrainingNeedForm
                        mpreAllowToEditTrainingNeedForm = True

                    Case enUserPriviledge.AllowToDeleteTrainingNeedForm
                        mpreAllowToDeleteTrainingNeedForm = True
                        'Sohail (14 Nov 2019) -- End

                        'Varsha Rana (17-Oct-2017) -- Start
                        'Enhancement - Give user privileges.
                        'Case 53
                        '    mpreAllowAssignGroupBenefit = True
                    Case enUserPriviledge.AllowToAssignBenefitGroup
                        mpreAllowToAssignBenefitGroup = True

                        'Varsha Rana (17-Oct-2017) -- End

                        
                    Case 54
                        mpreAddEmployeeBenefit = True
                    Case 55
                        mpreEditEmployeeBenefit = True
                    Case 56
                        mpreDeleteEmployeeBenefit = True
                    Case 57
                        mpreAddEmployeeReferee = True
                    Case 58
                        mpreEditEmployeeReferess = True
                    Case 59
                        mpreDeleteEmployeeReferess = True
                    Case 60
                        mpreAddEmployeeReferences = True
                    Case 61
                        mpreEditEmployeeReferences = True
                    Case 62
                        mpreDeleteEmployeeReferences = True
                    Case 63
                        mpreAddEmployeeAssets = True
                    Case 64
                        mpreEditEmployeeAssets = True
                    Case 65
                        mpreDeleteEmployeeAssets = True
                    Case 66
                        mpreAddEmployeeSkill = True
                    Case 67
                        mpreEditEmployeeSkill = True
                    Case 68
                        mpreDeleteEmployeeSkill = True
                    Case 69
                        mpreAddEmployeeQualification = True
                    Case 70
                        mpreEditEmployeeQualification = True
                    Case 71
                        mpreDeleteEmployeeQualification = True
                    Case 72
                        mpreAddEmployeeExperience = True
                    Case 73
                        mpreEditEmployeeExperience = True
                    Case 74
                        mpreDeleteEmployeeExperience = True
                    Case 75
                        mpreAddEmployeeDiscipline = True
                    Case 76
                        mpreEditEmployeeDiscipline = True
                    Case 77
                        mpreDeleteEmployeeDiscipline = True
                    Case 78
                        mpreAddDisciplinaryAction = True
                    Case 79
                        mpreEditDisciplinaryAction = True
                    Case 80
                        mpreDeleteDisciplinaryAction = True
                    Case 81
                        mpreAddDisciplineOffence = True
                    Case 82
                        mpreEditDisciplineOffence = True
                    Case 83
                        mpreDeleteDisciplineOffence = True

                        'S.SANDEEP [28 MAY 2015] -- START
                        'ENHANCEMENT : NEW PRIVILEGE (Performance Assessment)
                        'Case 84
                        '    mpreAddAssessmentGroup = True
                        'Case 85
                        '    mpreEditAssessmentGroup = True
                        'Case 86
                        '    mpreDeleteAssessmentGroup = True
                        'Case 87
                        '    mpreAddAssessmentItem = True
                        'Case 88
                        '    mpreEditAssessmentItem = True
                        'Case 89
                        '    mpreDeleteAssessmentItem = True
                        'Case 90
                        '    mpreAddAssessmentAnalysis = True
                        'Case 91
                        '    mpreEditAssessmentAnalysis = True
                        'Case 92
                        '    mpreDeleteAssessmentAnalysis = True
                        'S.SANDEEP [28 MAY 2015] -- END

                    Case 93
                        mpreAddMedicalMasters = True
                    Case 94
                        mpreEditMedicalMasters = True
                    Case 95
                        mpreDeleteMedicalMasters = True
                    Case 96
                        mpreAddMedicalInjuries = True
                    Case 97
                        mpreEditMedicalInjuries = True
                    Case 98
                        mpreDeleteMedicalInjuries = True
                    Case 99
                        mpreAddMedicalInstitutes = True
                    Case 100
                        mpreEditMedicalInstitutes = True
                    Case 101
                        mpreDeleteMedicalInstitutes = True
                    Case 102
                        mpreAddAssignMedicalCategory = True
                    Case 103
                        mpreEditAssignMedicalCategory = True
                    Case 104
                        mpreDeleteAssignMedicalCategory = True
                    Case 105
                        mpreAddMedicalCover = True
                    Case 106
                        mpreEditMedicalCover = True
                    Case 107
                        mpreDeleteMedicalCover = True
                    Case 108
                        mpreAddMedicalClaim = True
                    Case 109
                        mpreEditMedicalClaim = True
                    Case 110
                        mpreDeleteMedicalClaim = True
                    Case 111
                        mpreAddPayrollGroup = True
                    Case 112
                        mpreEditPayrollGroup = True
                    Case 113
                        mpreDeletePayrollGroup = True
                    Case 114
                        mpreAddCostCenter = True
                    Case 115
                        mpreEditCostCenter = True
                    Case 116
                        mpreDeleteCostCenter = True
                    Case 117
                        mpreAddPayPoint = True
                    Case 118
                        mpreEditPayPoint = True
                    Case 119
                        mpreDeletePayPoint = True
                    Case 120
                        mpreAddPeriods = True
                    Case 121
                        mpreEditPeriods = True
                    Case 122
                        mpreDeletePeriods = True
                    Case 123
                        mpreAddVendor = True
                    Case 124
                        mpreEditVendor = True
                    Case 125
                        mpreDeleteVendor = True
                    Case 126
                        mpreAddBankBranch = True
                    Case 127
                        mpreEditBankBranch = True
                    Case 128
                        mpreDeleteBankBranch = True
                    Case 129
                        mpreAddBankAccType = True
                    Case 130
                        mpreEditBankAccType = True
                    Case 131
                        mpreDeleteBankAccType = True
                    Case 132
                        mpreAddBankEDI = True
                    Case 133
                        mpreEditBankEDI = True
                    Case 134
                        mpreDeleteBankEDI = True
                    Case 135
                        mpreAddCurrency = True
                    Case 136
                        mpreEditCurrency = True
                    Case 137
                        mpreDeleteCurrency = True
                    Case 138
                        mpreAddDenomination = True
                    Case 139
                        mpreEditDenomination = True
                    Case 140
                        mpreDeleteDenomination = True
                    Case 141
                        mpreAddEmployeeCostCenter = True
                    Case 142
                        mpreEditEmployeeCostCenter = True
                    Case 143
                        mpreDeleteEmployeeCostCenter = True
                    Case 144
                        mpreAddEmployeeBanks = True
                    Case 145
                        mpreEditEmployeeBanks = True
                    Case 146
                        mpreDeleteEmployeeBanks = True
                    Case 147
                        mpreAddBatchTransaction = True
                    Case 148
                        mpreEditBatchTransaction = True
                    Case 149
                        mpreDeleteBatchTransaction = True
                    Case 150
                        mpreAddTransactionHead = True
                    Case 151
                        mpreEditTransactionHead = True
                    Case 152
                        mpreDeleteTransactionHead = True
                    Case 153
                        mpreAddEarningDeduction = True
                    Case 154
                        mpreEditEarningDeduction = True
                    Case 155
                        mpreDeleteEarningDeduction = True
                    Case 156
                        mpreAllowBatchTransactionOnED = True
                    Case 157
                        mpreAllowPerformGlobalAssignOnED = True
                    Case 158
                        mpreAllowPerformEmployeeExemptionOnED = True
                    Case 159
                        mpreAddEmployeeExemption = True
                    Case 160
                        mpreEditEmployeeExemption = True
                    Case 161
                        mpreDeleteEmployeeExemption = True
                    Case 162
                        mpreAllowProcessPayroll = True
                    Case 163
                        mpreAllowViewPayslip = True
                    Case 164
                        mpreAllowPrintPayslip = True
                    Case 165
                        mpreAddPayment = True
                    Case 166
                        mpreEditPayment = True
                    Case 167
                        mpreDeletePayment = True
                    Case 168
                        mpreAllowClosePeriod = True
                    Case 169
                        mpreAllowCloseYear = True
                    Case 170
                        mpreAllowPrepareBudget = True
                    Case 171
                        mpreAddSalaryIncrement = True
                    Case 172
                        mpreEditSalaryIncrement = True
                    Case 173
                        mpreDeleteSalaryIncrement = True
                    Case 174
                        mpreAllowDefineWageTable = True
                    Case 175
                        mpreAddLeaveType = True
                    Case 176
                        mpreEditLeaveType = True
                    Case 177
                        mpreDeleteLeaveType = True
                    Case 178
                        mpreAddHolidays = True
                    Case 179
                        mpreEditHolidays = True
                    Case 180
                        mpreDeleteHolidays = True
                    Case 181
                        mpreAddLeaveApproverLevel = True
                    Case 182
                        mpreEditLeaveApproverLevel = True
                    Case 183
                        mpreDeleteLeaveApproverLevel = True
                    Case 184
                        mpreAddLeaveApprover = True
                    Case 185
                        mpreEditLeaveApprover = True
                    Case 186
                        mpreDeleteLeaveApprover = True
                    Case 187
                        mpreAddLeaveForm = True
                    Case 188
                        mpreEditLeaveForm = True
                    Case 189
                        mpreDeleteLeaveForm = True
                    Case 190
                        mpreAllowPrintLeaveForm = True
                    Case 191
                        mpreAllowPreviewLeaveForm = True
                    Case 192
                        mpreAllowEmailLeaveForm = True
                    Case 193
                        mpreAllowProcessLeaveForm = True
                    Case 194
                        mpreAllowIssueLeave = True
                    Case 195
                        mpreAllowLeaveAccrue = True
                    Case 196
                        mpreAddEmployeeHolidays = True
                    Case 197
                        mpreEditEmployeeHolidays = True
                    Case 198
                        mpreDeleteEmployeeHolidays = True
                    Case 199
                        mpreAllowLeaveViewing = True
                    Case 200
                        mpreAddShiftInformation = True
                    Case 201
                        mpreEditShiftInformation = True
                    Case 202
                        mpreDeleteShiftInformation = True
                    Case 203
                        mpreAddTimeSheetInformation = True
                    Case 204
                        mpreEditTimeSheetInformation = True
                    Case 205
                        mpreDeleteTimeSheetInformation = True
                    Case 206
                        mpreAllowLoginTimesheet = True
                    Case 207
                        mpreAllowHoldEmployee = True
                    Case 208
                        mpreAllowUnHoldEmployee = True
                    Case 209
                        mpreAllowProcessEmployeeAbsent = True
                    Case 210
                        mpreAddStation = True
                    Case 211
                        mpreEditStation = True
                    Case 212
                        mpreDeleteStation = True
                    Case 213
                        mpreAddDepartmentGroup = True
                    Case 214
                        mpreEditDepartmentGroup = True
                    Case 215
                        mpreDeleteDepartmentGroup = True
                    Case 216
                        mpreAddDepartment = True
                    Case 217
                        mpreEditDepartment = True
                    Case 218
                        mpreDeleteDepartment = True
                    Case 219
                        mpreAddSection = True
                    Case 220
                        mpreEditSection = True
                    Case 221
                        mpreDeleteSection = True
                    Case 222
                        mpreAddUnit = True
                    Case 223
                        mpreEditUnit = True
                    Case 224
                        mpreDeleteUnit = True
                    Case 225
                        mpreAddJobGroup = True
                    Case 226
                        mpreEditJobGroup = True
                    Case 227
                        mpreDeleteJobGroup = True
                    Case 228
                        mpreAddJob = True
                    Case 229
                        mpreEditJob = True
                    Case 230
                        mpreDeleteJob = True
                    Case 231
                        mpreAddClassGroup = True
                    Case 232
                        mpreEditClassGroup = True
                    Case 233
                        mpreDeleteClassGroup = True
                    Case 234
                        mpreAddClasses = True
                    Case 235
                        mpreEditClasses = True
                    Case 236
                        mpreDeleteClasses = True
                    Case 237
                        mpreAddGradeGroup = True
                    Case 238
                        mpreEditGradeGroup = True
                    Case 239
                        mpreDeleteGradeGroup = True
                    Case 240
                        mpreAddGrade = True
                    Case 241
                        mpreEditGrade = True
                    Case 242
                        mpreDeleteGrade = True
                    Case 243
                        mpreAddGradeLevel = True
                    Case 244
                        mpreEditGradeLevel = True
                    Case 245
                        mpreDeleteGradeLevel = True
                    Case 246
                        mpreAddLoanScheme = True
                    Case 247
                        mpreEditLoanScheme = True
                    Case 248
                        mpreDeleteLoanScheme = True
                    Case 249
                        mpreAllowLoanApproverCreation = True
                    Case 250
                        mpreAddPendingLoan = True
                    Case 251
                        mpreEditPendingLoan = True
                    Case 252
                        mpreDeletePendingLoan = True
                    Case 253
                        mpreAllowAssignPendingLoan = True
                    Case 254
                        mpreEditLoanAdvance = True
                    Case 255
                        mpreDeleteLoanAdvance = True
                    Case 256
                        mpreAllowChangeLoanAvanceStatus = True
                    Case 257
                        mpreAddSavingScheme = True
                    Case 258
                        mpreEditSavingScheme = True
                    Case 259
                        mpreDeleteSavingScheme = True
                    Case 260
                        mpreAddEmployeeSavings = True
                    Case 261
                        mpreEditEmployeeSavings = True
                    Case 262
                        mpreDeleteEmployeeSavings = True
                    Case 263
                        mpreAllowChangeSavingStatus = True
                        'Sandeep [ 23 Oct 2010 ] -- Start
                    Case 264
                        mpreAllowtoApproveLeave = True
                        'Sandeep [ 23 Oct 2010 ] -- End 

                        'Anjan (14 Feb 2011)-Start

                        'S.SANDEEP [28 MAY 2015] -- START
                        'ENHANCEMENT : NEW PRIVILEGE (Performance Assessment)
                        'Case 265
                        '    mpreAllowtoAddAssessor = True
                        'Case 266
                        '    mpreAllowtoDeleteAssessor = True
                        'Case 267
                        '    mpreAddAssessorAccess = True
                        'Case 268
                        '    mpreEditAssessorAccess = True
                        'Case 269
                        '    mpreDeleteAssessorAccess = True
                        'Case 270
                        '    mpreAllowtoMapAssessorUser = True
                        'Case 271
                        '    mpreAddEmployeeAssessment = True
                        'Case 272
                        '    mpreEditEmployeeAssessment = True
                        'Case 273
                        '    mpreDeleteEmployeeAssessment = True
                        'S.SANDEEP [28 MAY 2015] -- END

                    Case 274
                        mpreAddDisciplineStatus = True
                    Case 275
                        mpreEditDisciplineStatus = True
                    Case 276
                        mpreDeleteDisciplineStatus = True
                    Case 277
                        mpreAllowtoViewEmployeeMovement = True
                    Case 278
                        mpreAllowtoEnrollFingerPrint = True
                    Case 279
                        mpreAllowtoDeleteFingerPrint = True
                    Case 280
                        mpreAllowtoEnrollNewCard = True
                    Case 281
                        mpreAllowtoDeleteEnrolledCard = True
                    Case 282
                        mpreAllowtoImportEmployee = True
                    Case 283
                        mpreAddTrainingInstitute = True
                    Case 284
                        mpreEditTrainingInstitute = True
                    Case 285
                        mpreDeleteTrainingInstitute = True
                    Case 286
                        mpreAllowtoImportTransactionHead = True
                    Case 287
                        mpreAddPayslipMessage = True
                    Case 288
                        mpreEditPayslipMessage = True
                    Case 289
                        mpreDeletePayslipMessage = True
                    Case 290
                        'Nilay (28-Apr-2016) -- Start
                        'Enhancement : 60.1 - #77 (KBC & Kenya Project Comments List.xls) - For payslip messages, include payroll period so that If you print payslip for last month and you had a new payslip message this month, It does not pick up message of current month but last month.
                        mpreAllowtoAddGlobalPayslipMessage = True
                        'Nilay (28-Apr-2016) -- End
                    Case 291
                        mpreAllowDeleteAccruedLeave = True
                    Case 292
                        mpreAddPlanLeave = True
                    Case 293
                        mpreEditPlanLeave = True
                    Case 294
                        mpreDeletePlanLeave = True
                    Case 295
                        mpreAllowViewPlannedLeaveViewer = True
                    Case 296
                        mpreAddLoanAdvancePayment = True
                    Case 297
                        mpreEditLoanAdvancePayment = True
                    Case 298
                        mpreDeleteLoanAdvancePayment = True
                    Case 299
                        mpreAddLoanAdvanceReceived = True
                    Case 300
                        mpreEditLoanAdvanceReceived = True
                    Case 301
                        mpreDeleteLoanAdvanceReceived = True
                    Case 302
                        mpreAllowAssignBatchtoEmployee = True
                    Case 303
                        mpreAllowImportLeaveTnA = True
                    Case 304
                        mpreAllowExportLeaveTnA = True
                    Case 305
                        mpreAllowFinalAnalysis = True
                    Case 306
                        mpreAddTrainingAnalysis = True
                    Case 307
                        mpreEditTrainingAnalysis = True
                    Case 308
                        mpreDeleteTrainingAnalysis = True
                    Case 309
                        mpreAddTrainingEnrollment = True
                    Case 310
                        mpreEditTrainingEnrollment = True
                    Case 311
                        mpreDeleteTrainingEnrollment = True
                    Case 312
                        mpreCancelTrainingEnrollment = True
                    Case 313
                        mpreAddCourseScheduling = True
                    Case 314
                        mpreEditCourseScheduling = True
                    Case 315
                        mpreDeleteCourseScheduling = True

                        'S.SANDEEP [28 MAY 2015] -- START
                        'ENHANCEMENT : NEW PRIVILEGE (Performance Assessment)
                        'Case 316
                        '    mpreAddAssessmentPeriod = True
                        'Case 317
                        '    mpreEditAssessmentPeriod = True
                        'Case 318
                        '    mpreDeleteAssessmentPeriod = True
                        'S.SANDEEP [28 MAY 2015] -- END

                    Case 319
                        mpreAddCashDenomination = True
                    Case 320
                        mpreEditCashDenomination = True
                    Case 321
                        mpreAllowImportED = True
                    Case 322
                        mpreAllowExportED = True
                    Case 323
                        mpreAllowGlobalPayment = True
                    Case 324
                        mpreAllowGlobalSalaryIncrement = True
                    Case 325
                        mpreAllowChangePendingLoanAdvanceStatus = True
                    Case 326
                        mpreAddSavingsPayment = True
                    Case 327
                        mpreEditSavingsPayment = True
                    Case 328
                        mpreDeleteSavingsPayment = True
                    Case 329
                        mpreAllowMapApproverWithUser = True
                    Case 330
                        mpreAllowChangeLeaveFormStatus = True
                    Case 331
                        mpreAllowCommonExport = True
                    Case 332
                        mpreAllowCommonImport = True
                        'Anjan (14 Feb 2011)-End

                        'S.SANDEEP [ 30 May 2011 ] -- START
                        'ISSUE : FINCA REQ.
                    Case 333
                        mpreAddUser = True
                    Case 334
                        mpreEditUser = True
                    Case 335
                        mpreDeleteUser = True
                    Case 336
                        mpreChangePassword = True
                    Case 337
                        mpreAssignReportPrivilege = True
                    Case 338
                        mpreAddUserRole = True
                    Case 339
                        mpreEditUserRole = True
                    Case 340
                        mpreDeleteUserRole = True
                    Case 341
                        mpreAddCompany = True
                    Case 342
                        mpreEditCompany = True
                    Case 343
                        mpreDeleteCompany = True
                        'S.SANDEEP [ 30 May 2011 ] -- END 
                        'S.SANDEEP [ 13 JUNE 2011 ] -- START
                        'ISSUE : EMPLOYEE & LOAN PRIVILEGE
                    Case 344
                        'S.SANDEEP [ 29 DEC 2011 ] -- START
                        'ENHANCEMENT : TRA CHANGES 
                        'TYPE : EMPLOYEMENT CONTRACT PROCESS
                        'mpreAllowtoMarkEmployeeActive = True
                        mpreAllowtoApproveEmployee = True
                        'S.SANDEEP [ 29 DEC 2011 ] -- END
                    Case 345
                        mpreAllowApproveLoan = True
                    Case 346
                        mpreAllowApproveAdvance = True
                        'S.SANDEEP [ 13 JUNE 2011 ] -- END
                        'Sohail (14 Jun 2011) -- Start
                    Case 347
                        mpreAllowToApproveEarningDeduction = True
                        'Sohail (14 Jun 2011) -- End

                        'Anjan (21 Nov 2011)-Start
                        'ENHANCEMENT : TRA COMMENTS
                    Case 348
                        mpreAllowAccessSalaryAnalysis = True
                    Case 349
                        mpreAllowAccessLeaveAnalysis = True
                    Case 350
                        mpreAllowAccessAttendanceSummary = True
                    Case 351
                        mpreAllowAccessStaffTurnOver = True
                    Case 352
                        mpreAllowAccessTrainingAnalysis = True
                        'Anjan (21 Nov 2011)-End 
                        'Sohail (26 Nov 2011) -- Start
                    Case 353
                        mpreAllowToApproveEmpExemption = True
                        'Sohail (26 Nov 2011) -- End 

                        'S.SANDEEP [ 25 DEC 2011 ] -- START
                        'ENHANCEMENT : TRA CHANGES
                    Case 354
                        mpreAllowtoAddVacancy = True
                    Case 355
                        mpreAllowtoEditVacancy = True
                    Case 356
                        mpreAllowtoDeleteVacancy = True
                    Case 357
                        mpreAllowtoAddApplicant = True
                    Case 358
                        mpreAllowtoEditApplicant = True
                    Case 359
                        mpreAllowtoDeleteApplicant = True
                    Case 360
                        mpreAllowtoExportDataOnWeb = True
                    Case 361
                        mpreAllowtoImportDataFromWeb = True
                    Case 362
                        mpreAllowtoAddBatch = True
                    Case 363
                        mpreAllowtoEditBatch = True
                    Case 364
                        mpreAllowtoDeleteBatch = True
                    Case 365
                        mpreAllowtoCancelBatch = True
                    Case 366
                        mpreAllowtoAddInterviewSchedule = True
                    Case 367
                        mpreAllowtoDeleteInterviewSchedule = True
                    Case 368
                        mpreAllowtoCancelInterviewSchedule = True
                    Case 369
                        mpreAllowtoAddAnalysis = True
                    Case 370
                        mpreAllowtoChangeBatch = True
                    Case 371
                        mpreAllowtoAddFinalEvaluation = True
                    Case 372
                        mpreAllowtoEditAnalysis = True
                    Case 373
                        mpreAllowtoDeleteAnalysis = True
                    Case 374
                        mpreAllowtoMarkEligible = True
                    Case 375
                        mpreAllowtoSendMail = True
                    Case 376
                        mpreAllowtoGenerateLetter = True
                    Case 377
                        mpreAllowtoPrintList = True
                    Case 378
                        mpreAllowtoImportEligibleApplicant = True
                    Case 379
                        mpreAllowtoMakeBatchActive = True
                    Case 380
                        mpreAllowtoAttachApplicantDocuments = True
                        'S.SANDEEP [ 25 DEC 2011 ] -- END

                        'Anjan (11 Jan 2012)-Start
                        'ENHANCEMENT : TRA COMMENTS
                    Case 381
                        mpreAllowtoShortList_FinalShortListApplicants = True
                        'Anjan (11 Jan 2012)-End 
                        'S.SANDEEP [ 29 DEC 2011 ] -- START
                        'ENHANCEMENT : TRA CHANGES 
                        'TYPE : EMPLOYEMENT CONTRACT PROCESS
                    Case 382
                        mpreAllowtoViewScale = True
                    Case 383
                        mpreAllowtoChangeConfirmationDate = True
                    Case 384
                        mpreAllowtoChangeAppointmentDate = True
                    Case 385
                        mpreAllowtoSetBirthDate = True
                    Case 386
                        mpreAllowtoSetSuspensionDate = True
                    Case 387
                        mpreAllowtoSetProbationDate = True
                    Case 388
                        mpreAllowtoSetEmploymentEndDate = True
                    Case 389
                        mpreAllowtoSetLeavingDate = True
                    Case 390
                        mpreAllowtoChangeRetirementDate = True
                    Case 391
                        mpreAllowtoMarkEmployeeClear = True
                        'S.SANDEEP [ 29 DEC 2011 ] -- END

                        'Sohail (04 Jan 2012) -- Start
                    Case 392
                        mpreAllowChangeAssetStatus = True
                        'Sohail (04 Jan 2012) -- End

                        'S.SANDEEP [ 07 NOV 2011 ] -- START
                        'ENHANCEMENT : TRA CHANGES
                    Case 393
                        mpreAllowToApproveResolutionStep = True
                    Case 394
                        mpreAllowToAddResolutionStep = True
                    Case 395
                        mpreAllowToEditResolutionStep = True
                    Case 396
                        mpreAllowToDeleteResolutionStep = True
                    Case 397
                        mpreAllowToAddDiscipileExemption = True
                    Case 398
                        mpreAllowToAddDisciplinePosting = True
                    Case 399
                        mpreAllowToViewDisciplineExemption = True
                    Case 400
                        mpreAllowToViewDisciplinePosting = True
                    Case 401
                        mpreAllowToDeleteDisciplineExemption = True
                    Case 402
                        mpreAllowToDeleteDisciplinePosting = True
                        'S.SANDEEP [ 07 NOV 2011 ] -- END
                        'Anjan (20 Jan 2012)-Start
                        'ENHANCEMENT : TRA COMMENTS
                    Case 403
                        mpreChangeGeneralSettings = True
                    Case 404
                        mpreAllowAddLetter = True
                    Case 405
                        mpreAllowEditLetter = True
                    Case 406
                        mpreAllowDeleteLetter = True
                    Case 407
                        mpreAllowSendMail = True
                    Case 408
                        mpreAllowDeleteMail = True
                    Case 409
                        mpreAllowViewUserLog = True
                    Case 410
                        mpreAllowAddAccount = True
                    Case 411
                        mpreAllowEditAccount = True
                    Case 412
                        mpreAllowDeleteAccount = True
                    Case 413
                        mpreAllowAddAccountConfiguration = True
                    Case 414
                        mpreAllowEditAccountConfiguration = True
                    Case 415
                        mpreAllowDeleteAccountConfiguration = True
                    Case 416
                        mpreAllowAddEmpConfiguration = True
                    Case 417
                        mpreAllowEditEmpConfiguration = True
                    Case 418
                        mpreAllowDeleteEmpConfiguration = True
                    Case 419
                        mpreAllowAddCCConfiguration = True
                    Case 420
                        mpreAllowEditCCConfiguration = True
                    Case 421
                        mpreAllowDeleteCCConfiguration = True
                        'Anjan (20 Jan 2012)-End 

                        'S.SANDEEP [ 05 MARCH 2012 ] -- START
                        'ENHANCEMENT : TRA CHANGES

                        'S.SANDEEP [28 MAY 2015] -- START
                        'ENHANCEMENT : NEW PRIVILEGE (Performance Assessment)
                        'Case 422
                        '    mpreAllowUnlockCommittedGeneralAssessment = True
                        'Case 423
                        '    mpreAllowUnlockCommittedBSCAssessment = True
                        'Case 424
                        '    mpreAllowUnlockFinalSaveBSCPlanning = True
                        'Case 425
                        '    mpreAllowFinalSaveBSCPlanning = True
                        'S.SANDEEP [28 MAY 2015] -- END

                        'S.SANDEEP [ 05 MARCH 2012 ] -- END

                        'Anjan (02 Mar 2012)-Start
                        'ENHANCEMENT : TRA COMMENTS on Andrew sir's Request
                    Case 426
                        mpreAllowtosetReinstatementDate = True
                        'Anjan (02 Mar 2012)-End 

                        'Sohail (26 Mar 2012) -- Start
                        'TRA - ENHANCEMENT
                    Case 427
                        mpreAllowToAddAssetDeclaration = True
                    Case 428
                        mpreAllowToEditAssetDeclaration = True
                    Case 429
                        mpreAllowToDeleteAssetDeclaration = True
                    Case 430
                        mpreAllowToFinalSaveAssetDeclaration = True
                    Case 431
                        mpreAllowToUnlockFinalSaveAssetDeclaration = True
                        'Sohail (26 Mar 2012) -- End

                        'Anjan (12 Apr 2012)-Start
                        'ENHANCEMENT : TRA COMMENTS on Andrew sir's Request
                    Case 432
                        mpreAllowAddLeaveAllowance = True
                        'Anjan (12 Apr 2012)-End 


                        'Anjan (17 Apr 2012)-Start
                        'ENHANCEMENT : TRA COMMENTS on Andrew sir's Request
                    Case 433
                        mpreAddDisciplineCommittee = True
                    Case 434
                        mpreEditDisciplineCommittee = True
                    Case 435
                        mpreDeleteDisciplineCommittee = True
                    Case 436
                        mpreAllowtoCloseCase = True
                    Case 437
                        mpreAllowtoReopenCase = True
                        'Anjan (17 Apr 2012)-End 

                        'S.SANDEEP [ 16 MAY 2012 ] -- START
                        'ENHANCEMENT : TRA USER PRIVILEGE CHANGES
                    Case 438
                        mpreAllowToExportMedicalClaim = True
                    Case 439
                        mpreAllowToSaveMedicalClaim = True
                    Case 440
                        mpreAllowToFinalSaveMedicalClaim = True
                    Case 441
                        mpreAllowToCancelExportedMedicalClaim = True
                    Case 442
                        mpreAllowToMakeServiceProviderActive = True
                    Case 443
                        mpreAllowToMapUserWithServiceProvider = True
                    Case 444
                        mpreAllowToAddMedicalSickSheet = True
                    Case 445
                        mpreAllowToEditMedicalSickSheet = True
                    Case 446
                        mpreAllowToDeleteMedicalSickSheet = True
                    Case 447
                        mpreAllowToPrintMedicalSickSheet = True


                        'S.SANDEEP [28 MAY 2015] -- START
                        'ENHANCEMENT : NEW PRIVILEGE (Performance Assessment)
                        'Case 448
                        '    mpreAllowToAddBSCObjective = True
                        'Case 449
                        '    mpreAllowToEditBSCObjective = True
                        'Case 450
                        '    mpreAllowToDeleteBSCObjective = True
                        'Case 451
                        '    mpreAllowToAddBSCKPI = True
                        'Case 452
                        '    mpreAllowToEditBSCKPI = True
                        'Case 453
                        '    mpreAllowToDeleteBSCKPI = True
                        'Case 454
                        '    mpreAllowToAddBSCTargets = True
                        'Case 455
                        '    mpreAllowToEditBSCTargets = True
                        'Case 456
                        '    mpreAllowToDeleteBSCTargets = True
                        'Case 457
                        '    mpreAllowToAddBSCInitiative_Action = True
                        'Case 458
                        '    mpreAllowToEditBSCInitiative_Action = True
                        'Case 459
                        '    mpreAllowToDeleteBSCInitiative_Action = True
                        'Case 460
                        '    mpreAllowToAddSelfBSCAssessment = True
                        'Case 461
                        '    mpreAllowToEditSelfBSCAssessment = True
                        'Case 462
                        '    mpreAllowToDeleteSelfBSCAssessment = True
                        'Case 463
                        '    mpreAllowToAddAssessorBSCAssessment = True
                        'Case 464
                        '    mpreAllowToEditAssessorBSCAssessment = True
                        'Case 465
                        '    mpreAllowToDeleteAssessorBSCAssessment = True
                        'Case 466
                        '    mpreAllowToAddReviewerBSCAssessment = True
                        'Case 467
                        '    mpreAllowToEditReviewerBSCAssessment = True
                        'Case 468
                        '    mpreAllowToDeleteReviewerBSCAssessment = True

                        'Case 469
                        '    mpreAllowToAddReviewerGeneralAssessment = True
                        'Case 470
                        '    mpreAllowToEditReviewerGeneralAssessment = True
                        'Case 471
                        '    mpreAllowToDeleteReviewerGeneralAssessment = True
                        'S.SANDEEP [28 MAY 2015] -- END


                    Case 472
                        mpreAllowToAddAppraisalAnalysis = True
                    Case 473
                        mpreAllowToDeleteAppraisalAnalysis = True
                    Case 474
                        mpreAllowToPerformSalaryIncrement = True
                    Case 475
                        mpreAllowToVoidSalaryIncrement = True
                    Case 476
                        mpreAllowToAddReminder = True
                    Case 477
                        mpreAllowToAddEmployeeToFinalList = True
                    Case 478
                        mpreAllowToSaveAppraisals = True

                    Case 479
                        mpreAllowToAddEvaluationGroup = True
                    Case 480
                        mpreAllowToEditEvaluationGroup = True
                    Case 481
                        mpreAllowToDeleteEvaluationGroup = True
                    Case 482
                        mpreAllowToAddEvaluationItems = True
                    Case 483
                        mpreAllowToEditEvaluationItems = True
                    Case 484
                        mpreAllowToDeleteEvaluationItems = True
                    Case 485
                        mpreAllowToAddEvaluationSubItems = True
                    Case 486
                        mpreAllowToEditEvaluationSubItems = True
                    Case 487
                        mpreAllowToDeleteEvaluationSubItems = True
                    Case 488
                        mpreAllowToAddEvaluationIIIItems = True
                    Case 489
                        mpreAllowToEditEvaluationIIIItems = True
                    Case 490
                        mpreAllowToDeleteEvaluationIIIItems = True
                    Case 491
                        mpreAllowToAddLevelIEvaluation = True
                    Case 492
                        mpreAllowToEditLevelIEvaluation = True
                    Case 493
                        mpreAllowToDeleteLevelIEvaluation = True
                    Case 494
                        mpreAllowToPrintLevelIEvaluation = True
                    Case 495
                        mpreAllowToPriviewLevelIEvaluation = True
                    Case 496
                        mpreAllowToSave_CompleteLevelIEvaluation = True
                    Case 497
                        mpreAllowToAddLevelIIIEvaluation = True
                    Case 498
                        mpreAllowToEditLevelIIIEvaluation = True
                    Case 499
                        mpreAllowToDeleteLevelIIIEvaluation = True
                    Case 500
                        mpreAllowToPrintLevelIIIEvaluation = True
                    Case 501
                        mpreAllowToPriviewLevelIIIEvaluation = True
                    Case 502
                        mpreAllowToSave_CompleteLevelIIIEvaluation = True


                        'S.SANDEEP [28 MAY 2015] -- START
                        'ENHANCEMENT : NEW PRIVILEGE (Performance Assessment)
                        'Case 503
                        '    mpreAdd_Reviewer_Access = True
                        'Case 504
                        '    mpreEdit_Reviewer_Access = True
                        'Case 505
                        '    mpreDelete_Reviewer_Access = True
                        'Case 506
                        '    mpreAllowToMapReviewerWithUser = True
                        'Case 507
                        '    mpreAllowToAddAssessmentSubItems = True
                        'Case 508
                        '    mpreAllowToEditAssessmentSubItems = True
                        'Case 509
                        '    mpreAllowToDeleteAssessmentSubItems = True
                        'Case 510
                        '    mpreAllowToAddExternalAssessor = True
                        'Case 511
                        '    mpreAllowToEditExternalAssessor = True
                        'Case 512
                        '    mpreAllowToDeleteExternalAssessor = True
                        'Case 513
                        '    mpreAllowToMigrateAssessor_Reviewer = True
                        'S.SANDEEP [28 MAY 2015] -- END

                    Case 514
                        mpreAllowToAddTrainingPriorityList = True
                    Case 515
                        mpreAllowToEditTrainingPriorityList = True
                    Case 516
                        mpreAllowToDeleteTrainingPriorityList = True
                    Case 517
                        mpreAllowToExportWithEmployee = True
                    Case 518
                        mpreAllowToExportWithoutEmployee = True
                    Case 519
                        mpreAllowToPreviewScheduledTraining = True
                    Case 520
                        mpreAllowToPrintScheduledTraining = True
                    Case 521
                        mpreAllowToUpdateQualifictaion = True
                    Case 522
                        mpreAllowToRe_Categorize = True
                    Case 523
                        mpreAllowSalaryIncrementFromEnrollment = True
                    Case 524
                        mpreAllowToVoidSalaryIncrementFromEnrollment = True
                    Case 525
                        mpreShowProbationDates = True
                    Case 526
                        mpreShowSuspensionDates = True
                    Case 527
                        mpreShowAppointmentDates = True
                    Case 528
                        mpreShowConfirmationDates = True
                    Case 529
                        mpreShowBirthDates = True
                    Case 530
                        mpreShowAnniversaryDates = True
                    Case 531
                        mpreShowContractEndingDates = True
                    Case 532
                        mpreShowTodayRetirementDates = True
                    Case 533
                        mpreShowForcastedRetirementDates = True
                        'S.SANDEEP [ 16 MAY 2012 ] -- END                    
                        'Sohail (22 May 2012) -- Start
                        'TRA - ENHANCEMENT
                    Case 534
                        mpreAllowVoidPayroll = True
                        'Sohail (22 May 2012) -- End

                        'S.SANDEEP [ 24 MAY 2012 ] -- START
                        'ENHANCEMENT : TRA USER PRIVILEGE CHANGES
                    Case 535
                        mpreShowForcastedEOCDates = True
                        'S.SANDEEP [ 24 MAY 2012 ] -- END


                        'Pinkal (28-MAY-2012) -- Start
                        'Enhancement : TRA Changes
                    Case 536
                        mpreAllowToCancelLeave = True
                    Case 537
                        mpreAllowToCancelPreviousDateLeave = True
                        'Pinkal (28-MAY-2012) -- End

                        'S.SANDEEP [ 19 JUNE 2012 ] -- START
                        'ENHANCEMENT : TRA CHANGES
                    Case 538
                        mpreAllowToEditQueries = True
                    Case 539
                        mpreAllowToAddTemplate = True
                    Case 540
                        mpreAllowToEditTemplate = True
                    Case 541
                        mpreAllowToDeleteTemplate = True
                    Case 542
                        mpreAllowToAccessCustomReport = True
                        'S.SANDEEP [ 19 JUNE 2012 ] -- END

                        'Pinkal (02-Jul-2012) -- Start
                        'Enhancement : TRA Changes
                    Case 543
                        mpreAllowToViewLeaveApproverList = True
                    Case 544
                        mpreAllowToViewLeaveFormList = True
                    Case 545
                        mpreAllowToViewLeaveProcessList = True
                    Case 546
                        mpreAllowToViewLeavePlannerList = True
                    Case 547
                        mpreAllowToViewEmpHolidayList = True
                    Case 548
                        mpreAllowToViewMedicalCoverList = True
                    Case 549
                        mpreAllowToViewMedicalClaimList = True
                    Case 550
                        mpreAllowToViewEmpInjuryList = True
                    Case 551
                        mpreAllowToViewEmpEmpSickSheetList = True
                    Case 552
                        mpreAllowToViewEmpList = True
                    Case 553
                        mpreAllowToViewBenefitAllocationList = True
                    Case 554
                        mpreAllowToViewEmpBenefitList = True
                    Case 555
                        mpreAllowToViewEmpReferenceList = True
                    Case 556
                        mpreAllowToViewCompanyAssetList = True
                    Case 557
                        mpreAllowToViewEmpSkillList = True
                    Case 558
                        mpreAllowToViewEmpQualificationList = True
                    Case 559
                        mpreAllowToViewEmpExperienceList = True
                    Case 560
                        mpreAllowToViewEmpDependantsList = True
                    Case 561
                        mpreAllowToViewChargesProceedingList = True
                    Case 562
                        mpreAllowToViewProceedingApprovalList = True
                    Case 563
                        mpreAllowToViewDisciplinaryCommitteeList = True
                    Case 564
                        mpreAllowToViewLeaveTypeList = True
                    Case 565
                        mpreAllowToViewHolidayList = True
                    Case 566
                        mpreAllowToViewApproverLevelList = True
                    Case 567
                        mpreAllowToViewLeaveViewer = True
                    Case 568
                        mpreAllowToViewMedicalMasterList = True
                    Case 569
                        mpreAllowToViewServiceProviderList = True
                    Case 570
                        mpreAllowToViewMedicalCategoryList = True
                    Case 571
                        mpreAllowToViewCommonMasterList = True
                    Case 572
                        mpreAllowToViewMembershipMasterList = True
                    Case 573
                        mpreAllowToViewStateList = True
                    Case 574
                        mpreAllowToViewCityList = True
                    Case 575
                        mpreAllowToViewZipcodeList = True
                    Case 576
                        mpreAllowToViewEmpMovementList = True
                    Case 577
                        mpreAllowToViewShiftList = True
                    Case 578
                        mpreAllowToViewTrainingInstituteList = True

                        'S.SANDEEP [28 MAY 2015] -- START
                        'ENHANCEMENT : NEW PRIVILEGE (Performance Assessment)
                        'Case 579
                        '    mpreAllowToViewSelfAssessmentList = True
                        'Case 580
                        '    mpreAllowToViewAssessorAssessmentList = True
                        'Case 581
                        '    mpreAllowToViewReviewerAssessmentList = True
                        'Case 582
                        '    mpreAllowToViewSelfAssessedBSCList = True
                        'Case 583
                        '    mpreAllowToViewAssessorAssessedBSCList = True
                        'Case 584
                        '    mpreAllowToViewReviewerAssessedBSCList = True
                        'S.SANDEEP [28 MAY 2015] -- END

                    Case 585
                        mpreAllowToViewApprisalAnalysisList = True
                    Case 586
                        mpreAllowToViewTrainingNeedsAnalysisList = True
                    Case 587
                        mpreAllowToViewTrainingSchedulingList = True
                    Case 588
                        mpreAllowToViewTrainingEnrollmentList = True
                    Case 589
                        mpreAllowToViewLevel1EvaluationList = True
                    Case 590
                        mpreAllowToViewLevel3EvaluationList = True
                    Case 591
                        mpreAllowToViewAssetsDeclarationList = True
                    Case 592
                        mpreAllowToViewProcessLoanAdvanceList = True
                    Case 593
                        mpreAllowToViewLoanAdvanceList = True
                    Case 594
                        mpreAllowToViewEmployeeSavingsList = True
                    Case 595
                        mpreAllowToViewVancancyMasterList = True
                    Case 596
                        mpreAllowToViewApplicantMasterList = True
                    Case 597
                        mpreAllowToViewShortListApplicants = True
                    Case 598
                        mpreAllowToViewBatchSchedulingList = True
                    Case 599
                        mpreAllowToViewInterviewSchedulingList = True
                        'Varsha Rana (17-Oct-2017) -- Start
                        'Enhancement - Give user privileges.
                        'Case 600
                        '    mpreAllowToViewFinalApplicantList = True
                    Case enUserPriviledge.AllowToViewFinalApplicantList
                        mpreAllowToViewFinalApplicantList = True
                        'Varsha Rana (17-Oct-2017) -- End
                    Case 601
                        mpreAllowToViewEmpDistributedCostCenterList = True
                    Case 602
                        mpreAllowToViewEmpBankList = True
                    Case 603
                        mpreAllowToViewTransactionHeadsList = True
                    Case 604
                        mpreAllowToViewEmpEDList = True
                    Case 605
                        mpreAllowToViewEmpExemptionList = True
                    Case 606
                        mpreAllowToViewPayslipList = True
                    Case 607
                        mpreAllowToViewPaymentList = True
                    Case 608
                        mpreAllowToViewGlobalVoidPaymentList = True

                        'Pinkal (02-Jul-2012) -- End

                        'Sohail (02 Jul 2012) -- Start
                        'TRA - ENHANCEMENT
                    Case 609
                        mpreAllowAuthorizePayslipPayment = True

                    Case 610
                        mpreAllowVoidAuthorizedPayslipPayment = True
                        'Sohail (02 Jul 2012) -- End

                    Case 611
                        mpreAllowToPerformForceLogout = True


                        'Pinkal (09-Jul-2012) -- Start
                        'Enhancement : TRA Changes

                    Case 612
                        mpreAllowToViewStationList = True
                    Case 613
                        mpreAllowToViewDeptGroupList = True
                    Case 614
                        mpreAllowToViewDepartmentList = True
                    Case 615
                        mpreAllowToViewSectionGroupList = True
                    Case 616
                        mpreAllowToViewSectionList = True
                    Case 617
                        mpreAllowToViewUnitGroupList = True
                    Case 618
                        mpreAllowToViewUnitList = True
                    Case 619
                        mpreAllowToViewTeamList = True
                    Case 620
                        mpreAllowToViewJobGroupList = True
                    Case 621
                        mpreAllowToViewJobList = True
                    Case 622
                        mpreAllowToViewClassGroupList = True
                    Case 623
                        mpreAllowToViewClassList = True
                    Case 624
                        mpreAllowToViewGradeGroupList = True
                    Case 625
                        mpreAllowToViewGradeList = True
                    Case 626
                        mpreAllowToViewGradeLevelList = True
                    Case 627
                        mpreAllowToViewCostCenterList = True
                    Case 628
                        mpreAllowToViewPayrollPeriodList = True
                    Case 629
                        mpreAllowToViewSalaryChangeList = True
                    Case 630
                        mpreAllowToViewReasonMasterList = True
                    Case 631
                        mpreAllowToViewSkillMasterList = True
                    Case 632
                        mpreAllowToViewAgencyMasterList = True
                    Case 633
                        mpreAllowToViewQualificationMasterList = True
                    Case 634
                        mpreAllowToViewResultCodeList = True
                    Case 635
                        mpreAllowToViewBenefitPlanList = True
                    Case 636
                        mpreAllowToViewDisciplinaryOffencesList = True
                    Case 637
                        mpreAllowToViewDisciplinaryPenaltiesList = True
                    Case 638
                        mpreAllowToViewDisciplinaryStatusList = True

                        'S.SANDEEP [28 MAY 2015] -- START
                        'ENHANCEMENT : NEW PRIVILEGE (Performance Assessment)
                        'Case 639
                        '    mpreAllowToViewAssessmentPeriodList = True
                        'Case 640
                        '    mpreAllowToViewAssessorAccessList = True
                        'Case 641
                        '    mpreAllowToViewReviewerAccessList = True
                        'Case 642
                        '    mpreAllowToViewExternalAssessorList = True
                        'Case 643
                        '    mpreAllowToViewAssessmentGroupList = True
                        'Case 644
                        '    mpreAllowToViewAssessmentItemList = True
                        'Case 645
                        '    mpreAllowToViewAssessmentSubItemList = True
                        'Case 646
                        '    mpreAllowToViewBSCObjectiveList = True
                        'Case 647
                        '    mpreAllowToViewBSCKPIList = True
                        'Case 648
                        '    mpreAllowToViewBSCTargetsList = True
                        'Case 649
                        '    mpreAllowToViewBSCInitiativeList = True
                        'S.SANDEEP [28 MAY 2015] -- END

                    Case 650
                        mpreAllowToViewEvaluationGroupList = True
                    Case 651
                        mpreAllowToViewEvaluationItemList = True
                    Case 652
                        mpreAllowToViewEvaluationSubItemList = True
                    Case 653
                        mpreAllowToViewEvaluationIIIitemList = True
                    Case 654
                        mpreAllowToViewPayrollGroupList = True
                    Case 655
                        mpreAllowToViewPayPointList = True
                    Case 656
                        mpreAllowToViewBankBranchList = True
                    Case 657
                        mpreAllowToViewBankAccountTypeList = True
                    Case 658
                        mpreAllowToViewBankEDIList = True
                    Case 659
                        mpreAllowToViewCurrencyList = True
                    Case 660
                        mpreAllowToViewCurrencyDenominationList = True
                    Case 661
                        mpreAllowToViewPayslipMessageList = True
                    Case 662
                        mpreAllowToViewAccountList = True
                    Case 663
                        mpreAllowToViewCompanyAccountConfigurationList = True
                    Case 664
                        mpreAllowToViewEmployeeAccountConfigurationList = True
                    Case 665
                        mpreAllowToViewCostcenterAccountConfigurationList = True
                    Case 666
                        mpreAllowToViewBatchTransactionList = True
                    Case 667
                        mpreAllowToViewCashDenominationList = True
                    Case 668
                        mpreAllowToViewLoanSchemeList = True
                    Case 669
                        mpreAllowToViewSavingSchemeList = True

                        'Pinkal (09-Jul-2012) -- End

                        'S.SANDEEP [ 17 AUG 2012 ] -- START
                        'ENHANCEMENT : TRA CHANGES
                    Case 670
                        mpreAllowToViewApplicantReferenceNo = True
                        'S.SANDEEP [ 17 AUG 2012 ] -- END

                        'Sohail (24 Sep 2012) -- Start
                        'TRA - ENHANCEMENT
                    Case 671
                        mpreAllowToApproveSalaryChange = True
                        'Sohail (24 Sep 2012) -- End
                    Case 672
                        mpreAddTeam = True
                    Case 673
                        mpreEditTeam = True
                    Case 674
                        mpreDeleteTeam = True
                    Case 675
                        mpreAddSectionGroup = True
                    Case 676
                        mpreEditSectionGroup = True
                    Case 677
                        mpreDeleteSectionGroup = True
                    Case 678
                        mpreAddUnitGroup = True
                    Case 679
                        mpreEditUnitGroup = True
                    Case 680
                        mpreDeleteUnitGroup = True

                        'S.SANDEEP [ 08 OCT 2012 ] -- START
                        'ENHANCEMENT : TRA CHANGES
                    Case 681
                        mpreEditImportedEmployee = True
                        'S.SANDEEP [ 08 OCT 2012 ] -- END

                        'Anjan (25 Oct 2012)-Start
                        'ENHANCEMENT : TRA COMMENTS on Andrew's Request
                    Case 682
                        mpreAllowtoScanAttachDocument = True
                    Case 683
                        mpreAllowtoExportMembership = True
                    Case 684
                        mpreAllowtoImportMembership = True
                    Case 685
                        mpreAllowtoSetReportingTo = True
                    Case 686
                        mpreAllowtoViewDiary = True
                    Case 687
                        mpreAllowtoSetAllocationMapping = True
                    Case 688
                        mpreAllowtoDeleteAllocationMapping = True
                    Case 689
                        mpreAllowtoImportDependants = True
                    Case 690
                        mpreAllowtoExportDependants = True


                    Case 691
                        mpreAllowtoImportEmpReferee = True
                    Case 692
                        mpreAllowtoExportEmpReferee = True
                    Case 693
                        mpreAllowtoImportEmpSkills = True
                    Case 694
                        mpreAllowtoExportEmpSkills = True
                    Case 695
                        mpreAllowtoImportEmpQualification = True
                    Case 696
                        mpreAllowtoExportEmpQualification = True
                    Case 697
                        mpreAllowtoExportOtherQualification = True
                    Case 698
                        mpreAllowtoExportEmpExperience = True
                    Case 699
                        mpreAllowtoImportEmpExperience = True
                    Case 700
                        mpreAllowtoMapVacancy = True
                    Case 701
                        mpreAllowtoViewInterviewAnalysisList = True
                        'S.SANDEEP [ 29 OCT 2012 ] -- START
                        'ENHANCEMENT : TRA CHANGES
                    Case 702
                        mpreAllowtoMapLoanApprover = True
                    Case 703
                        mpreAllowtoImportLoan_Advance = True
                    Case 704
                        mpreAllowtoImportSavings = True
                    Case 705
                        mpreAllowtoImportAccrueLeave = True
                    Case 706
                        mpreAllowtoMigrateLeaveApprover = True
                    Case 707
                        mpreAllowtoImportAttendanceData = True
                    Case 708
                        mpreAllowtoMakeGroupAttendance = True
                    Case 709
                        mpreAllowtoAddGeneralReminder = True
                    Case 710
                        mpreAllowtoEditGeneralReminder = True
                    Case 711
                        mpreAllowtoDeleteGeneralReminder = True
                    Case 712
                        mpreAllowtoChangeUser = True
                    Case 713
                        mpreAllowtoChangeCompany = True
                    Case 714
                        mpreAllowtoChangeDatabase = True
                    Case 715
                        mpreAllowtoChangePassword = True
                    Case 716
                        mpreAllowtoViewSentItems = True
                    Case 717
                        mpreAllowtoViewTrashItems = True
                    Case 718
                        mpreAllowtoPrintLetters = True
                    Case 719
                        mpreAllowtoExportLetters = True
                    Case 720
                        mpreAllowtoReadMails = True
                    Case 721
                        mpreAllowtoViewAuditTrails = True
                    Case 722
                        mpreAllowtoViewApplicationEvtLog = True
                    Case 723
                        mpreAllowtoViewUserAuthLog = True
                    Case 724
                        mpreAllowtoViewUserAttemptsLog = True
                    Case 725
                        mpreAllowtoViewAuditLogs = True
                    Case 726
                        mpreAllowtoUnlockUser = True
                    Case 727
                        mpreAllowtoExportAbilityLevel = True
                    Case 728
                        mpreAllowtoSaveAbilityLevel = True
                    Case 729
                        mpreAllowtoTakeDatabaseBackup = True
                    Case 730
                        mpreAllowtoRestoreDatabase = True
                    Case 731
                        mpreAllowtoAddVoidReason = True
                    Case 732
                        mpreAllowtoEditVoidReason = True
                    Case 733
                        mpreAllowtoDeleteVoidReason = True
                    Case 734
                        mpreAllowtoAddReminderType = True
                    Case 735
                        mpreAllowtoEditReminderType = True
                    Case 736
                        mpreAllowtoDeleteReminderType = True
                    Case 737
                        mpreAllowtoSavePasswordOption = True
                    Case 738
                        mpreAllowtoAddDeviceSettings = True
                    Case 739
                        mpreAllowtoEditDeviceSettings = True
                    Case 740
                        mpreAllowtoDeleteDeviceSettings = True
                    Case 741
                        mpreAllowtoChangeCompanyOptions = True
                    Case 742
                        mpreAllowtoSaveCompanyGroup = True
                        'S.SANDEEP [ 29 OCT 2012 ] -- END
                    Case 743
                        mpreAllowtoExportAccounts = True
                    Case 744
                        mpreAllowtoImportAccounts = True
                    Case 745
                        mpreAllowtoExportEmpBanks = True
                    Case 746
                        mpreAllowtoImportEmpBanks = True
                    Case 747
                        mpreAllowtoSetTranHeadActive = True
                    Case 748
                        mpreAllowtoMapLeaveType = True
                    Case 749
                        mrpeAllowtoViewLetterTemplate = True

                        'Anjan (25 Oct 2012)-End 

                        'Pinkal (19-Nov-2012) -- Start
                        'Enhancement : TRA Changes

                    Case 750
                        mpreAddLeaveExpense = True
                    Case 751
                        mpreEditLeaveExpense = True
                    Case 752
                        mpreDeleteLeaveExpense = True

                        'Pinkal (19-Nov-2012) -- End

                        'S.SANDEEP [ 23 NOV 2012 ] -- START
                        'ENHANCEMENT : TRA CHANGES
                    Case 753
                        mpreImportADUsers = True
                    Case 754
                        mpreAssignUserAccess = True
                    Case 755
                        mpreEditUserAccess = True
                    Case 756
                        mpreExportPrivilegs = True
                        'S.SANDEEP [ 23 NOV 2012 ] -- END



                        'Pinkal (22-Nov-2012) -- Start
                        'Enhancement : TRA Changes

                    Case 757
                        mpreAllowtoChangeBranch = True
                    Case 758
                        mpreAllowtoChangeDepartmentGroup = True
                    Case 759
                        mpreAllowtoChangeDepartment = True
                    Case 760
                        mpreAllowtoChangeSectionGroup = True
                    Case 761
                        mpreAllowtoChangeSection = True
                    Case 762
                        mpreAllowtoChangeUnitGroup = True
                    Case 763
                        mpreAllowtoChangeUnit = True
                    Case 764
                        mpreAllowtoChangeTeam = True
                    Case 765
                        mpreAllowtoChangeJobGroup = True
                    Case 766
                        mpreAllowtoChangeJob = True
                    Case 767
                        mpreAllowtoChangeGradeGroup = True
                    Case 768
                        mpreAllowtoChangeGrade = True
                    Case 769
                        mpreAllowtoChangeGradeLevel = True
                    Case 770
                        mpreAllowtoChangeClassGroup = True
                    Case 771
                        mpreAllowtoChangeClass = True
                    Case 772
                        mpreAllowtoChangeCostCenter = True

                        'Pinkal (22-Nov-2012) -- End


                        'Pinkal (01-Dec-2012) -- Start
                        'Enhancement : TRA Changes
                    Case 773
                        mpreAllowtoChangeCompanyEmail = True

                        'Pinkal (01-Dec-2012) -- End
                        'S.SANDEEP [ 01 DEC 2012 ] -- START
                        'ENHANCEMENT : TRA CHANGES
                    Case 774
                        mpreAllowtoImportEmployee_User = True
                        'S.SANDEEP [ 01 DEC 2012 ] -- END

                        'Sohail (05 Dec 2012) -- Start
                        'TRA - ENHANCEMENT
                    Case 775
                        mpreAllowToAddBatchPosting = True

                    Case 776
                        mpreAllowToEditBatchPosting = True

                    Case 777
                        mpreAllowToDeleteBatchPosting = True

                    Case 778
                        mpreAllowToPostBatchPostingToED = True

                    Case 779
                        mpreAllowToViewBatchPostingList = True
                        'Sohail (05 Dec 2012) -- End

                        'S.SANDEEP [ 13 DEC 2012 ] -- START
                        'ENHANCEMENT : TRA CHANGES
                    Case 780
                        mpreAllowToViewMembershipOnDiary = True
                    Case 781
                        mpreAllowToViewBanksOnDiary = True
                        'S.SANDEEP [ 13 DEC 2012 ] -- END


                        'Pinkal (18-Dec-2012) -- Start
                        'Enhancement : TRA Changes
                    Case 782
                        mpreAllowToAssignIssueUsertoEmp = True

                    Case 783
                        mpreAllowToMigrateIssueUser = True
                        'Pinkal (18-Dec-2012) -- End

                        'Sohail (13 Feb 2013) -- Start
                        'TRA - ENHANCEMENT
                    Case 784
                        mpreAllowToAddPaymentApproverLevel = True

                    Case 785
                        mpreAllowToEditPaymentApproverLevel = True

                    Case 786
                        mpreAllowToDeletePaymentApproverLevel = True
                        'Sohail (13 Feb 2013) -- End

                    Case 787
                        mpreAllowToAddPaymentApproverMapping = True
                    Case 788
                        mpreAllowToEditPaymentApproverMapping = True
                    Case 789
                        mpreAllowToDeletePaymentApproverMapping = True
                    Case 790
                        mpreAllowToApprovePayment = True

                        'Sohail (13 Feb 2013) -- Start
                        'TRA - ENHANCEMENT
                    Case 791
                        mpreAllowToVoidApprovedPayment = True
                    Case 792
                        mpreAllowToViewPaymentApproverLevelList = True
                        'Sohail (13 Feb 2013) -- End

                        'Pinkal (06-Feb-2013) -- Start
                        'Enhancement : TRA Changes

                    Case 793
                        mpreAllowToEndLeaveCycle = True

                        'Pinkal (06-Feb-2013) -- End

                        'Pinkal (06-Mar-2013) -- Start
                        'Enhancement : TRA Changes
                    Case 794
                        mpreAllowToAddMedicalDependantException = True
                        'Pinkal (06-Mar-2013) -- End

                        'Pinkal (01-Apr-2013) -- Start
                        'Enhancement : TRA Changes

                    Case 795
                        mpreAllowToAddEditPhoto = True

                    Case 796
                        mpreAllowToDeletePhoto = True

                    Case 797
                        mpreAllowToImportPhoto = True
                        'Pinkal (01-Apr-2013) -- End

                        'S.SANDEEP [ 10 APR 2013 ] -- START
                        'ENHANCEMENT : TRA CHANGES

                        'S.SANDEEP [28 MAY 2015] -- START
                        'ENHANCEMENT : NEW PRIVILEGE (Performance Assessment)
                        'Case 798
                        '    mpreAllowToSubmitBSCPlan_Approval = True
                        'S.SANDEEP [28 MAY 2015] -- END

                        'S.SANDEEP [ 10 APR 2013 ] -- END

                        'S.SANDEEP [ 14 May 2013 ] -- START
                        'ENHANCEMENT : TRA ENHANCEMENT
                    Case 799
                        mpreAllowToSubmitApplicantFilter_Approval = True
                    Case 800
                        mpreAllowToApproveApplicantFilter = True
                    Case 801
                        mpreAllowToRejectApplicantFilter = True
                    Case 802
                        mpreAllowToAddApplicantFilter = True
                    Case 803
                        mpreAllowToDeleteApplicantFilter = True
                        'S.SANDEEP [ 14 May 2013 ] -- END

                        'Pinkal (12-May-2013) -- Start
                        'Enhancement : TRA Changes
                    Case 804
                        mpreAllowToSubmitForApprovalInFinalShortlistedApplicant = True
                    Case 805
                        mpreAllowToApproveFinalShortListedApplicant = True
                    Case 806
                        mpreAllowToDisapproveFinalShortListedApplicant = True
                        'Pinkal (12-May-2013) -- End

                        'S.SANDEEP [ 14 May 2013 ] -- START
                        'ENHANCEMENT : TRA ENHANCEMENT
                    Case 807
                        mpreAllowtoSubmitApplicantEligibilityForApproval = True
                    Case 808
                        mpreAllowtoApproveApplicantEligibility = True
                    Case 809
                        mpreAllowtoDisapproveApplicantEligibility = True
                        'S.SANDEEP [ 14 May 2013 ] -- END


                        'Pinkal (03-Jul-2013) -- Start
                        'Enhancement : TRA Changes
                    Case 810
                        mpreAllowToMoveFinalShortListedApplicantToShortListed = True
                        'Pinkal (03-Jul-2013) -- End

                        'S.SANDEEP [ 10 JUNE 2013 ] -- START
                        'ENHANCEMENT : OTHER CHANGES
                    Case 811
                        mpreAllowtoAddPayActivity = True
                    Case 812
                        mpreAllowtoEditPayActivity = True
                    Case 813
                        mpreAllowtoDeletePayActivity = True
                    Case 814
                        mpreAllowtoAddUnitOfMeasure = True
                    Case 815
                        mpreAllowtoEditUnitOfMeasure = True
                    Case 816
                        mpreAllowtoDeleteUnitOfMeasure = True
                    Case 817
                        mpreAllowtoAdd_EditActivityRate = True
                    Case 818
                        mpreAllowtoPostActivitytoPayroll = True
                    Case 819
                        mpreAllowtoVoidActivtyfromPayroll = True
                    Case 820
                        mpreAllowtoAdd_EditPayPerActivity = True
                    Case 821
                        mpreAllowtoDeletePayPerActivity = True
                    Case 822
                        mpreAllowtoExportPayPerActivity = True
                    Case 823
                        mpreAllowtoImportPayPerActivity = True
                    Case 824
                        mpreAllowtoAssignPayPerActivityGlobally = True
                    Case 825
                        mpreAllowtoviewActivityList = True
                    Case 826
                        mpreAllowtoViewMeasureList = True
                        'S.SANDEEP [ 10 JUNE 2013 ] -- END

                        'Pinkal (24-Aug-2013) -- Start
                        'Enhancement : TRA Changes
                    Case 827
                        mpreAllowToImportEmpIdentity = True
                        'Pinkal (24-Aug-2013) -- End

                        'Sohail (04 Oct 2013) -- Start
                        'TRA - ENHANCEMENT
                    Case 828
                        mpreAllowtoMapDeviceUser = True

                    Case 829
                        mpreAllowtoImportDeviceUser = True
                        'Sohail (04 Oct 2013) -- End

                        'Pinkal (30-Dec-2013) -- Start
                        'Enhancement : Oman Changes

                        'Varsha Rana (17-Oct-2017) -- Start
                        'Enhancement - Give user privileges.
                        'Case 830
                        '    mpreAllowToEditTimesheetCard = True
                    Case enUserPriviledge.AllowToEditTimeSheetCard
                        mpreAllowToEditTimesheetCard = True

                        'Case 831
                        '    mpreAllowToAddEditDeleteGlobalTimesheet = True
                    Case enUserPriviledge.AllowToAddEditDeleteGlobalTimeSheet
                        mpreAllowToAddEditDeleteGlobalTimesheet = True

                        'Case 832
                        '    mpreAllowToReCalculateTimings = True

                    Case enUserPriviledge.AllowToReCalculateTimings
                        mpreAllowToReCalculateTimings = True

                        'Case 833
                        '    mpreAllowToRoundoffTimings = True
                    Case enUserPriviledge.AllowToRoundoffTimings
                        mpreAllowToRoundoffTimings = True
                        'Varsha Rana (17-Oct-2017) -- End




                        'Pinkal (30-Dec-2013) -- End

                        'S.SANDEEP [ 28 JAN 2014 ] -- START
                    Case 834
                        mpreAllowToDeleteEmailNotificationAuditTrails = True
                        'S.SANDEEP [ 28 JAN 2014 ] -- END



                        'Pinkal (06-May-2014) -- Start
                        'Enhancement : TRA Changes
                    Case 835
                        mpreAllowToChangeOtherUserPassword = True
                        'Pinkal (06-May-2014) -- End


                        'Sohail (28 May 2014) -- Start
                        'Enhancement - Staff Requisition.
                    Case 836
                        mpreAllowToAddStaffRequisitionApproverLevel = True

                    Case 837
                        mpreAllowToEditStaffRequisitionApproverLevel = True

                    Case 838
                        mpreAllowToDeleteStaffRequisitionApproverLevel = True

                    Case 839
                        mpreAllowToViewStaffRequisitionApproverLevelList = True

                    Case 840
                        mpreAllowToAddStaffRequisitionApproverMapping = True
                    Case 841
                        mpreAllowToEditStaffRequisitionApproverMapping = True
                    Case 842
                        mpreAllowToDeleteStaffRequisitionApproverMapping = True
                    Case 843
                        mpreAllowToViewStaffRequisitionApproverMappingList = True
                    Case 844
                        mpreAllowToAddStaffRequisition = True
                    Case 845
                        mpreAllowToEditStaffRequisition = True
                    Case 846
                        mpreAllowToDeleteStaffRequisition = True
                    Case 847
                        mpreAllowToViewStaffRequisitionList = True
                    Case 848
                        mpreAllowToApproveStaffRequisition = True
                    Case 849
                        mpreAllowToCancelStaffRequisition = True
                    Case 850
                        mpreAllowToViewStaffRequisitionApprovals = True
                        'Sohail (28 May 2014) -- End

                        'Sohail (05 Aug 2014) -- Start
                        'Enhancement - Statutory Payment Priviledge.
                    Case 851
                        mpreAllowToAddStatutoryPayment = True
                    Case 852
                        mpreAllowToEditStatutoryPayment = True
                    Case 853
                        mpreAllowToDeleteStatutoryPayment = True
                    Case 854
                        mpreAllowToViewStatutoryPaymentList = True
                        'Sohail (05 Aug 2014) -- End


                        'Pinkal (12-Sep-2014) -- Start
                        'Medical Enhancement - TANAPA URGENT CHANGES FOR MEDICAL SICK SHEET GIVEN BY OLAIS
                    Case 855
                        mpreRevokeUserAccessonSicksheet = True
                        'Pinkal (12-Sep-2014) -- End

                        'Sohail (29 Oct 2014) -- Start
                        'AKF Enhancement - Inactive option on Payment Approver Level.
                    Case 856
                        mpreAllowToSetPaymentApproverLevelActive = True
                        'Sohail (29 Oct 2014) -- End


                        'Pinkal (03-Nov-2014) -- Start
                        'Enhancement -  ENHANCMENT ON LEAVE MODULE GIVEN BY TRA.
                    Case 857
                        mpreShowForcastedELCDates = True
                        'Pinkal (03-Nov-2014) -- End

                      'S.SANDEEP [ 17 OCT 2014 ] -- START
                    Case 858
                        mpreAllowtoDeletePayPerActivityAuditTrails = True
                        'S.SANDEEP [ 17 OCT 2014 ] -- END

                        'Anjan [06 April 2015] -- Start
                        'ENHANCEMENT : Implementing Peoplesoft integration,requested by Andrew.
                    Case 859
                        mpreAllowImportPeoplesoftData = True
                        'Anjan [06 April 2015] -- End

                        'S.SANDEEP [28 MAY 2015] -- START
                        'ENHANCEMENT : NEW PRIVILEGE (Performance Assessment)
                    Case 860
                        mpreAllowtoAddAssessmentPeriod = True
                    Case 861
                        mpreAllowtoEditAssessmentPeriod = True
                    Case 862
                        mpreAllowtoDeleteAssessmentPeriod = True
                    Case 863
                        mpreAllowtoAddAssessorAccess = True
                    Case 864
                        mpreAllowtoEditAssessorAccess = True
                    Case 865
                        mpreAllowtoDeleteAssessorAccess = True
                    Case 866
                        mpreAllowtoImportAssessor = True
                    Case 867
                        mpreAllowtoAddReviewerAccess = True
                    Case 868
                        mpreAllowtoEditReviewerAccess = True
                    Case 869
                        mpreAllowtoDeleteReviewerAccess = True
                    Case 870
                        mpreAllowtoImportReviewer = True
                    Case 871
                        mpreAllowtoPerformAssessorReviewerMigration = True
                    Case 872
                        mpreAllowtoAddAssessmentRatios = True
                    Case 873
                        mpreAllowtoEditAssessmentRatios = True
                    Case 874
                        mpreAllowtoDeleteAssessmentRatios = True
                    Case 875
                        mpreAllowtoCloseAssessmentPeriod = True
                    Case 876
                        mpreAllowtoAddAssessmentGroup = True
                    Case 877
                        mpreAllowtoEditAssessmentGroup = True
                    Case 878
                        mpreAllowtoDeleteAssessmentGroup = True
                    Case 879
                        mpreAllowtoAddAssessmentScales = True
                    Case 880
                        mpreAllowtoEditAssessmentScales = True
                    Case 881
                        mpreAllowtoDeleteAssessmentScales = True
                    Case 882
                        mpreAllowtoMapScaleGrouptoGoals = True
                    Case 883
                        mpreAllowtoAddCompetencies = True
                    Case 884
                        mpreAllowtoEditCompetencies = True
                    Case 885
                        mpreAllowtoDeleteCompetencies = True
                    Case 886
                        mpreAllowtoLoadfromCompetenciesLibrary = True
                    Case 887
                        mpreAllowtoAddAssignedCompetencies = True
                    Case 888
                        mpreAllowtoEditAssignedCompetencies = True
                    Case 889
                        mpreAllowtoDeleteAssignedCompetencies = True
                    Case 890
                        mpreAllowtoAddPerspective = True
                    Case 891
                        mpreAllowtoEditPerspective = True
                    Case 892
                        mpreAllowtoDeletePerspective = True
                    Case 893
                        mpreAllowtoSaveBSCTitles = True
                    Case 894
                        mpreAllowtoAddBSCTitlesMapping = True
                    Case 895
                        mpreAllowtoEditBSCTitlesMapping = True
                    Case 896
                        mpreAllowtoDeleteBSCTitlesMapping = True
                    Case 897
                        mpreAllowtoSaveBSCTitlesViewSetting = True
                    Case 898
                        mpreAllowtoAddCompanyGoals = True
                    Case 899
                        mpreAllowtoEditCompanyGoals = True
                    Case 900
                        mpreAllowtoDeleteCompanyGoals = True
                    Case 901
                        mpreAllowtoCommitCompanyGoals = True
                    Case 902
                        mpreAllowtoUnlockcommittedCompanyGoals = True
                    Case 903
                        mpreAllowtoAddAllocationGoals = True
                    Case 904
                        mpreAllowtoEditAllocationGoals = True
                    Case 905
                        mpreAllowtoDeleteAllocationGoals = True
                    Case 906
                        mpreAllowtoCommitAllocationGoals = True
                    Case 907
                        mpreAllowtoUnlockcommittedAllocationGoals = True
                    Case 908
                        mpreAllowtoPerformGlobalAssignAllocationGoals = True
                    Case 909
                        mpreAllowtoUpdatePercentCompletedAllocationGoals = True
                    Case 910
                        mpreAllowtoAddEmployeeGoals = True
                    Case 911
                        mpreAllowtoEditEmployeeGoals = True
                    Case 912
                        mpreAllowtoDeleteEmployeeGoals = True
                    Case 913
                        mpreAllowtoPerformGlobalAssignEmployeeGoals = True
                    Case 914
                        mpreAllowtoUpdatePercentCompletedEmployeeGoals = True
                    Case 915
                        mpreAllowtoSubmitGoalsforApproval = True
                    Case 916
                        mpreAllowtoApproveRejectGoalsPlanning = True
                    Case 917
                        mpreAllowtoUnlockFinalSavedGoals = True
                    Case 918
                        mpreAllowtoAddCustomHeaders = True
                    Case 919
                        mpreAllowtoEditCustomHeaders = True
                    Case 920
                        mpreAllowtoDeleteCustomHeaders = True
                    Case 921
                        mpreAllowtoAddCustomItems = True
                    Case 922
                        mpreAllowtoEditCustomItems = True
                    Case 923
                        mpreAllowtoDeleteCustomItems = True
                    Case 924
                        mpreAllowtoAddComputationFormula = True
                    Case 925
                        mpreAllowtoDeleteComputationFormula = True
                    Case 926
                        mpreAllowtoViewAssessmentPeriodList = True
                    Case 927
                        mpreAllowtoViewAssessorAccessList = True
                    Case 928
                        mpreAllowtoViewReviewerAccessList = True
                    Case 929
                        mpreAllowtoViewAssessmentGroupList = True
                    Case 930
                        mpreAllowtoViewCompanyGoalsList = True
                    Case 931
                        mpreAllowtoViewAllocationGoalsList = True
                    Case 932
                        mpreAllowtoViewEmployeeGoalsList = True
                    Case 933
                        mpreAllowtoAddSelfEvaluation = True
                    Case 934
                        mpreAllowtoEditSelfEvaluation = True
                    Case 935
                        mpreAllowtoDeleteSelfEvaluation = True
                    Case 936
                        mpreAllowtoUnlockcommittedSelfEvaluation = True
                    Case 937
                        mpreAllowtoAddAssessorEvaluation = True
                    Case 938
                        mpreAllowtoEditAssessorEvaluation = True
                    Case 939
                        mpreAllowtoDeleteAssessorEvaluation = True
                    Case 940
                        mpreAllowtoUnlockcommittedAssessorEvaluation = True
                    Case 941
                        mpreAllowtoAddReviewerEvaluation = True
                    Case 942
                        mpreAllowtoEditReviewerEvaluation = True
                    Case 943
                        mpreAllowtoDeleteReviewerEvaluation = True
                    Case 944
                        mpreAllowtoUnlockcommittedReviewerEvaluation = True
                    Case 945
                        mpreAllowtoViewSelfEvaluationList = True
                    Case 946
                        mpreAllowtoViewAssessorEvaluationList = True
                    Case 947
                        mpreAllowtoViewReviewerEvaluationList = True
                    Case 948
                        mpreAllowtoViewPerformanceEvaluation = True
                        'S.SANDEEP [28 MAY 2015] -- END

                        'Pinkal (13-Jul-2015) -- Start
                        'Enhancement - WORKING ON C & R ACCESS PRIVILEGE.
                    Case 949
                        mpreAllowtoAddClaimExpenses = True
                    Case 950
                        mpreAllowtoEditClaimExpenses = True
                    Case 951
                        mpreAllowtoDeleteClaimExpenses = True
                    Case 952
                        mpreAllowtoViewClaimExpenseList = True
                    Case 953
                        mpreAllowtoAddSectorRouteCost = True
                    Case 954
                        mpreAllowtoEditSectorRouteCost = True
                    Case 955
                        mpreAllowtoDeleteSectorRouteCost = True
                    Case 956
                        mpreAllowtoViewSectorRouteCostList = True
                    Case 957
                        mpreAllowtoAddEmpExpesneAssignment = True
                    Case 958
                        mpreAllowtoDeleteEmpExpesneAssignment = True
                    Case 959
                        mpreAllowtoViewEmpExpesneAssignment = True
                    Case 960
                        mpreAllowtoAddExpenseApprovalLevel = True
                    Case 961
                        mpreAllowtoEditExpenseApprovalLevel = True
                    Case 962
                        mpreAllowtoDeleteExpenseApprovalLevel = True
                    Case 963
                        mpreAllowtoViewExpenseApprovalLevelList = True
                    Case 964
                        mpreAllowtoAddExpenseApprover = True
                    Case 965
                        mpreAllowtoEditExpenseApprover = True
                    Case 966
                        mpreAllowtoDeleteExpenseApprover = True
                    Case 967
                        mpreAllowtoViewExpenseApproverList = True
                    Case 968
                        mpreAllowtoMigrateExpenseApprover = True
                    Case 969
                        mpreAllowtoSwapExpenseApprover = True
                    Case 970
                        mpreAllowtoAddClaimExpenseForm = True
                    Case 971
                        mpreAllowtoEditClaimExpenseForm = True
                    Case 972
                        mpreAllowtoDeleteClaimExpenseForm = True
                    Case 973
                        mpreAllowtoViewClaimExpenseFormList = True
                    Case 974
                        mpreAllowtoCancelClaimExpenseForm = True
                    Case 975
                        mpreAllowtoProcessClaimExpenseForm = True
                    Case 976
                        mpreAllowtoViewProcessClaimExpenseFormList = True
                    Case 977
                        mpreAllowtoPostClaimExpenseToPayroll = True
                        'Pinkal (13-Jul-2015) -- End

                       'S.SANDEEP [10 AUG 2015] -- START
                        'ENHANCEMENT : Aruti SaaS Changes
                    Case 978
                        mpreAllowtoViewReportAbilityLevel = True
                    Case 979
                        mpreAllowtoExportReportAbilityLevel = True
                    Case 980
                        mpreAllowtoSaveReportAbilityLevel = True
                        'S.SANDEEP [10 AUG 2015] -- END

                        'Shani(17-Aug-2015) -- Start
                        'Leave Enhancement : Putting Activate Feature in Leave Approver Master
                    Case 981
                        mpreAllowToSetClaimInactiveApprover = True
                        'Shani(17-Aug-2015) -- End

                     'Sohail (21 Nov 2015) -- Start
                        'Enhancement - Provide Deposit feaure in Employee Saving.
                    Case 982
                        mpreAddSavingsDeposit = True
                    Case 983
                        mpreEditSavingsDeposit = True
                    Case 984
                        mpreDeleteSavingsDeposit = True
                        'Sohail (21 Nov 2015) -- End

                        'Shani(06-Feb-2016) -- Start
                        'PA Changes Given By CCBRT
                    Case 985
                        mpreAllowToSaveAppraisalSetup = True '15
                    Case 986
                        mpreAllowToSaveExportAppraisalAnalysis = True '15
                    Case 987
                        mpreAllowToVoidEmployeeGoals = True '23
                    Case 988
                        mpreAllowToVoidOwnerGoals = True '23
                    Case 989
                        mpreAllowToOpenPeriodicReiew = True '23
                    Case 990
                        mpreAllowToGlobalVoidAssessment = True '24
                        'Shani(06-Feb-2016) -- End

                        'S.SANDEEP [23 FEB 2016] -- START
                    Case 991
                        mpreAllowtoChangePreassignedCompetencies = True
                        'S.SANDEEP [23 FEB 2016] -- END

                        'Sohail (24 Feb 2016) -- Start
                        'Enhancement - Batch Posting changes as per online doc in 58.1 SP. (#15 (KBC & Kenya Project Comments List.xls))
                    Case 992
                        mpreAllowToVoidBatchPostingToED = True
                        'Sohail (24 Feb 2016) -- End

                        'Nilay (28-Apr-2016) -- Start
                        'Enhancement : 60.1 - #77 (KBC & Kenya Project Comments List.xls) - For payslip messages, include payroll period so that If you print payslip for last month and you had a new payslip message this month, It does not pick up message of current month but last month.
                    Case 993
                        mpreAllowtoEditGlobalPayslipMessage = True
                    Case 994
                        mpreAllowtoDeleteGlobalPayslipMessage = True
                    Case 995
                        mpreAllowtoViewGlobalPayslipMessage = True
                        'Nilay (28-Apr-2016) -- End

                        'Nilay (27 Apr 2016) -- Start
                        'Enhancement - 61.1 - Priority Order on Grade Group, Grade and Grade Level; Auto Salary Increment to next Grade Level as per Priority Order; Grade filter on Wages Table; Export and Import for Wages Table for KBC.
                        '#23 (KBC & Kenya Project Comments List.xls) - Auto Salary incrment Per employee Anniversary,Employee Anniversay,All KBC Employees have Annivesary date for Salary increment,date is sent  when employee is confimed
                    Case 996
                        mpreAllowToAssignGradePriority = True
                    Case 997
                        mpreAllowToViewSalaryAnniversaryMonth = True
                    Case 998
                        mpreAllowToAddSalaryAnniversaryMonth = True
                    Case 999
                        mpreAllowToDeleteSalaryAnniversaryMonth = True
                        'Nilay (27 Apr 2016) -- End


                        'Shani (24-May-2016) -- Start
                    Case 1000
                        mpreAllowtoComputataionProcess = True
                    Case 1001
                        mpreAllowtoComputataionVoidProcess = True
                        'Shani (24-May-2016) -- End


                        'Nilay (06-Jun-2016) -- Start
                        'Enhancement : Import option in Wages Table for KBC
                    Case 1002
                        mpreAllowToImportWagesTable = True
                    Case 1003
                        mpreAllowToExportWagesTable = True
                        'Nilay (06-Jun-2016) -- End


                       'Nilay (20-May-2016) -- Start
                        'ENHANCEMENT - 61.1 - New Budget Redesign for Marie Stopes (Fund Sources, Funds Adjustments, Budget Formula and new Budget screen Allocation/Employee wise, Trans./Summary wise)
                    Case 1004
                        mpreAllowToViewFundSource = True
                    Case 1005
                        mpreAllowToAddFundSource = True
                    Case 1006
                        mpreAllowToEditFundSource = True
                    Case 1007
                        mpreAllowToDeleteFundSource = True
                    Case 1008
                        mpreAllowToViewFundAdjustment = True
                    Case 1009
                        mpreAllowToAddFundAdjustment = True
                    Case 1010
                        mpreAllowToEditFundAdjustment = True
                    Case 1011
                        mpreAllowToDeleteFundAdjustment = True
                        'Nilay (20-May-2016) -- End
                        'Sohail (07 Jun 2016) -- Start
                        'Enhancement -  62.1 - New Budget Redesign for Marie Stopes (Fund Sources, Funds Adjustments, Budget Formula and new Budget screen Allocation/Employee wise, Trans./Summary wise).
                    Case 1012
                        mpreAllowToViewBudgetFormula = True
                    Case 1013
                        mpreAllowToAddBudgetFormula = True
                    Case 1014
                        mpreAllowToEditBudgetFormula = True
                    Case 1015
                        mpreAllowToDeleteBudgetFormula = True
                    Case 1016
                        mpreAllowToViewBudget = True
                    Case 1017
                        mpreAllowToAddBudget = True
                    Case 1018
                        mpreAllowToEditBudget = True
                    Case 1019
                        mpreAllowToDeleteBudget = True
                       'S.SANDEEP [24 MAY 2016] -- START
                        'ENHANCEMENT : DISCIPLINE MAKE-OVER {BY ANDREW}
                    Case 1020
                        mpreAllowToApproveProceedingCounts = True
                        'S.SANDEEP [24 MAY 2016] -- END
                    Case 1021
                        mpreAllowToViewFundActivity = True
                    Case 1022
                        mpreAllowToAddFundActivity = True
                    Case 1023
                        mpreAllowToEditFundActivity = True
                    Case 1024
                        mpreAllowToDeleteFundActivity = True
                    Case 1025
                        mpreAllowToViewFundActivityAdjustment = True
                    Case 1026
                        mpreAllowToAddFundActivityAdjustment = True
                    Case 1027
                        mpreAllowToEditFundActivityAdjustment = True
                    Case 1028
                        mpreAllowToDeleteFundActivityAdjustment = True
                    Case enUserPriviledge.AllowToAddBudgetCodes
                        'Sohail (02 Sep 2016) -- Start
                        'Enhancement -  63.1 - New Budget Redesign for Marie Stopes (Period Wise budget codes).
                        'mpreAllowToSaveBudgetCodes = True
                        mpreAllowToAddBudgetCodes = True
                        'Sohail (02 Sep 2016) -- End
                    Case 1030
                        mpreAllowToViewFundProjectCode = True
                    Case 1031
                        mpreAllowToAddFundProjectCode = True
                    Case 1032
                        mpreAllowToEditFundProjectCode = True
                    Case 1033
                        mpreAllowToDeleteFundProjectCode = True
                    Case 1034
                        mpreAllowToMapLevelToApprover = True
                    Case 1035
                        mpreAllowToViewBudgetApproverLevelMapping = True
                    Case 1036
                        mpreAllowToDeleteBudgetApproverLevelMapping = True
                    Case 1037
                        mpreAllowToViewBudgetApproverLevel = True
                    Case 1038
                        mpreAllowToAddBudgetApproverLevel = True
                    Case 1039
                        mpreAllowToEditBudgetApproverLevel = True
                    Case 1040
                        mpreAllowToDeleteBudgetApproverLevel = True
                    Case 1041
                        mpreAllowToApproveBudget = True
                    Case 1042
                        mpreAllowToVoidApprovedBudget = True
                    Case 1043
                        mpreAllowToViewBudgetApprovals = True
                        'Sohail (07 Jun 2016) -- End

                        'Sohail (06 Oct 2021) -- Start
                        'NMB Enhancement : OLD-483 : Separate sender's email configuration on recruitment portal from company email.
                    Case enUserPriviledge.AllowToViewEmailSetup
                        mpreAllowToViewEmailSetup = True

                    Case enUserPriviledge.AllowToAddEmailSetup
                        mpreAllowToAddEmailSetup = True

                    Case enUserPriviledge.AllowToEditEmailSetup
                        mpreAllowToEditEmailSetup = True

                    Case enUserPriviledge.AllowToDeleteEmailSetup
                        mpreAllowToDeleteEmailSetup = True
                        'Sohail (06 Oct 2021) -- End

                        'Shani(17-Aug-2015) -- Start
                        'Leave Enhancement : Putting Activate Feature in Leave Approver Master
                    Case 1044
                        mpreAllowToSetLeaveActiveApprover = True
                    Case 1045
                        mpreAllowToSetLeaveInactiveApprover = True
                    Case 1046
                        mpreAllowToSetClaimActiveApprover = True
                        'Shani(17-Aug-2015) -- End

                        'Sohail (02 Sep 2016) -- Start
                        'Enhancement -  63.1 - New Budget Redesign for Marie Stopes (Period Wise budget codes).
                    Case enUserPriviledge.AllowToEditBudgetCodes
                        mpreAllowToEditBudgetCodes = True

                    Case enUserPriviledge.AllowToDeleteBudgetCodes
                        mpreAllowToDeleteBudgetCodes = True

                    Case enUserPriviledge.AllowToViewBudgetCodes
                        mpreAllowToViewBudgetCodes = True
                        'Sohail (02 Sep 2016) -- End

                        'Nilay (20-Sept-2016) -- Start
                        'Enhancement : Cancel feature for approved but not assigned loan application
                    Case enUserPriviledge.AllowToCancelApprovedLoanApp
                        mpreAllowToCancelApprovedLoanApp = True
                        'Nilay (20-Sept-2016) -- End
                         'Shani (26-Sep-2016) -- Start
                        'Enhancement -Changes PA Flow And Add New screen Given By (Andrew)
                    Case enUserPriviledge.AllowToApproveGoalsAccomplishment
                        mpreAllowToApproveGoalsAccomplishment = True
                        'Shani (26-Sep-2016) -- End


                        'Pinkal (21-Oct-2016) -- Start
                        'Enhancement - Working on Budget Employee Timesheet as Per Mr.Requirement.
                    Case enUserPriviledge.AllowToMapBudgetTimesheetApprover
                        mpreAllowtoMapBudgetTimesheetApprover = True
                        'Pinkal (21-Oct-2016) -- End

                        'Shani (08-Dec-2016) -- Start
                        'Enhancement -  Add Employee Allocaion/Date Privilage
                    Case enUserPriviledge.AllowToChangeEmpRecategorize
                        mpreAllowToChangeEmpRecategorize = True
                    Case enUserPriviledge.AllowToChangeEmpTransfers
                        mpreAllowToChangeEmpTransfers = True
                    Case enUserPriviledge.AllowToChangeEmpWorkPermit
                        mpreAllowToChangeEmpWorkPermit = True
                        'Shani (08-Dec-2016) -- End

                        'Pinkal (21-Dec-2016) -- Start
                        'Enhancement - Adding Swap Approver Privilage for Leave Module.

                    Case enUserPriviledge.AllowToSwapLeaveApprover
                        mpreAllowToSwapLeaveApprover = True
                        'Pinkal (21-Dec-2016) -- End

                        'Nilay (13 Apr 2017) -- Start
                        'Enhancements: Settings for mandatory options in online recruitment
                    Case enUserPriviledge.AllowToViewSkillExpertise
                        mpreAllowToViewSkillExpertise = True
                    Case enUserPriviledge.AllowToAddSkillExpertise
                        mpreAllowToAddSkillExpertise = True
                    Case enUserPriviledge.AllowToEditSkillExpertise
                        mpreAllowToEditSkillExpertise = True
                    Case enUserPriviledge.AllowToDeleteSkillExpertise
                        mpreAllowToDeleteSkillExpertise = True
                        'Nilay (13 Apr 2017) -- End


                        'Pinkal (03-May-2017) -- Start
                        'Enhancement - Working On Implementing privileges in Budget Timesheet Module.

                    Case enUserPriviledge.AllowToViewBudgetTimesheetApproverLevelList
                        mpreAllowToViewBudgetTimesheetApproverLevelList = True

                    Case enUserPriviledge.AllowToAddBudgetTimesheetApproverLevel
                        mpreAllowToAddBudgetTimesheetApproverLevel = True

                    Case enUserPriviledge.AllowToEditBudgetTimesheetApproverLevel
                        mpreAllowToEditBudgetTimesheetApproverLevel = True

                    Case enUserPriviledge.AllowToDeleteBudgetTimesheetApproverLevel
                        mpreAllowToDeleteBudgetTimesheetApproverLevel = True

                    Case enUserPriviledge.AllowToViewBudgetTimesheetApproverList
                        mpreAllowToViewBudgetTimesheetApproverList = True

                    Case enUserPriviledge.AllowToAddBudgetTimesheetApprover
                        mpreAllowToAddBudgetTimesheetApprover = True

                    Case enUserPriviledge.AllowToEditBudgetTimesheetApprover
                        mpreAllowToEditBudgetTimesheetApprover = True

                    Case enUserPriviledge.AllowToDeleteBudgetTimesheetApprover
                        mpreAllowToDeleteBudgetTimesheetApprover = True

                    Case enUserPriviledge.AllowToSetBudgetTimesheetApproverAsActive
                        mpreAllowToSetBudgetTimesheetApproverAsActive = True

                    Case enUserPriviledge.AllowToSetBudgetTimesheetApproverAsInactive
                        mpreAllowToSetBudgetTimesheetApproverAsInActive = True

                    Case enUserPriviledge.AllowToViewEmployeeBudgetTimesheetList
                        mpreAllowToViewEmployeeBudgetTimesheetList = True

                    Case enUserPriviledge.AllowToAddEmployeeBudgetTimesheet
                        mpreAllowToAddEmployeeBudgetTimesheet = True

                    Case enUserPriviledge.AllowToEditEmployeeBudgetTimesheet
                        mpreAllowToEditEmployeeBudgetTimesheet = True

                    Case enUserPriviledge.AllowToDeleteEmployeeBudgetTimesheet
                        mpreAllowToDeleteEmployeeBudgetTimesheet = True

                    Case enUserPriviledge.AllowToCancelEmployeeBudgetTimesheet
                        mpreAllowToCancelEmployeeBudgetTimesheet = True

                    Case enUserPriviledge.AllowToViewPendingEmpBudgetTimesheetSubmitForApproval
                        mpreAllowToViewPendingEmpBudgetTimesheetSubmitForApproval = True

                    Case enUserPriviledge.AllowToViewCompletedEmpBudgetTimesheetSubmitForApproval
                        mpreAllowToViewCompletedEmpBudgetTimesheetSubmitForApproval = True

                    Case enUserPriviledge.AllowToSubmitForApprovalForPendingEmpBudgetTimesheet
                        mpreAllowToSubmitForApprovalForPendingEmpBudgetTimesheet = True

                    Case enUserPriviledge.AllowToViewEmployeeBudgetTimesheetApprovalList
                        mpreAllowToViewEmployeeBudgetTimesheetApprovalList = True

                    Case enUserPriviledge.AllowToChangeEmployeeBudgetTimesheetStatus
                        mpreAllowToChangeEmployeeBudgetTimesheetStatus = True

                    Case enUserPriviledge.AllowToMigrateBudgetTimesheetApprover
                        mpreAllowToMigrateBudgetTimesheetApprover = True

                    Case enUserPriviledge.AllowToImportEmployeeShiftAssignment
                        mpreAllowToImportEmployeeShiftAssignment = True

                        'Pinkal (03-May-2017) -- End

                        'Pinkal (23-AUG-2017) -- Start
                        'Enhancement - Working on Enable/Disable Active Directory User for Budget Timesheet.
                    Case enUserPriviledge.AllowToEnableDisableADUser
                        mpreAllowToEnableDisableADUser = True
                        'Pinkal (23-AUG-2017) -- End

                        'Varsha Rana (17-Oct-2017) -- Start
                        'Enhancement - Give user privileges in 

                    Case enUserPriviledge.AllowToImportEmployeeAccountConfiguration
                        mpreAllowToImportEmployeeAccountConfiguration = True

                    Case enUserPriviledge.AllowToExportEmployeeAccountConfiguration
                        mpreAllowToExportEmployeeAccountConfiguration = True

                    Case enUserPriviledge.AllowToGetFileFormatOfEmployeeAccountConfiguration
                        mpreAllowToGetFileFormatOfEmployeeAccountConfiguration = True

                        'Case enUserPriviledge.AllowToAddPayslipGlobalMessage
                        '    mpreAllowToAddPayslipGlobalMessage = True

                        'Case enUserPriviledge.AllowToEditPayslipGlobalMessage
                        '    mpreAllowToEditPayslipGlobalMessage = True

                        'Case enUserPriviledge.AllowToDeletePayslipGlobalMessage
                        '    mpreAllowToDeletePayslipGlobalMessage = True
                    Case enUserPriviledge.AllowToDeleteRehireEmployeeDetails
                        mpreAllowToDeleteRehireEmployeeDetails = True

                    Case enUserPriviledge.AllowToPerformEligibleOperation
                        mpreAllowToPerformEligibleOperation = True

                    Case enUserPriviledge.AllowToPerformNotEligibleOperation
                        mpreAllowToPerformNotEligibleOperation = True


                    Case enUserPriviledge.AllowToAddLoanApproverLevel
                        mpreAllowToAddLoanApproverLevel = True

                    Case enUserPriviledge.AllowToEditLoanApproverLevel
                        mpreAllowToEditLoanApproverLevel = True

                    Case enUserPriviledge.AllowToDeleteLoanApproverLevel
                        mpreAllowToDeleteLoanApproverLevel = True

                    Case enUserPriviledge.AllowToAddLoanApprover
                        mpreAllowToAddLoanApprover = True

                    Case enUserPriviledge.AllowToEditLoanApprover
                        mpreAllowToEditLoanApprover = True

                    Case enUserPriviledge.AllowToDeleteLoanApprover
                        mpreAllowToDeleteLoanApprover = True

                    Case enUserPriviledge.AllowToSetActiveInactiveLoanApprover
                        mpreAllowToSetActiveInactiveLoanApprover = True

                    Case enUserPriviledge.AllowToViewLoanApproverLevelList
                        mpreAllowToViewLoanApproverLevelList = True

                    Case enUserPriviledge.AllowToViewLoanApproverList
                        mpreAllowToViewLoanApproverList = True

                    Case enUserPriviledge.AllowToProcessGlobalLoanApprove
                        mpreAllowToProcessGlobalLoanApprove = True

                    Case enUserPriviledge.AllowToProcessGlobalLoanAssign
                        mpreAllowToProcessGlobalLoanAssign = True

                    Case enUserPriviledge.AllowToViewLoanApprovalList
                        mpreAllowToViewLoanApprovalList = True

                    Case enUserPriviledge.AllowToChangeLoanStatus
                        mpreAllowToChangeLoanStatus = True

                    Case enUserPriviledge.AllowToTransferLoanApprover
                        mpreAllowToTransferLoanApprover = True

                    Case enUserPriviledge.AllowToSwapLoanApprover
                        mpreAllowToSwapLoanApprover = True


                    Case enUserPriviledge.AllowToImportLeaveApprovers
                        mpreAllowToImportLeaveApprovers = True

                    Case enUserPriviledge.AllowToImportLeavePlanner
                        mpreAllowToImportLeavePlanner = True


                    Case enUserPriviledge.AllowToViewDeviceUserMapping
                        mpreAllowToViewDeviceUserMapping = True


                    Case enUserPriviledge.AllowToImportEmployeeBirthInfo
                        mpreAllowToImportEmployeeBirthInfo = True

                    Case enUserPriviledge.AllowToImportEmployeeAddress
                        mpreAllowToImportEmployeeAddress = True

                    Case enUserPriviledge.AllowToViewMissingEmployee
                        mpreAllowToViewMissingEmployee = True

                    Case enUserPriviledge.AllowToUpdateEmployeeDetails
                        mpreAllowToUpdateEmployeeDetails = True

                    Case enUserPriviledge.AllowToUpdateEmployeeMovements
                        mpreAllowToUpdateEmployeeMovements = True

                    Case enUserPriviledge.AllowToViewAssignedShiftList
                        mpreAllowToViewAssignedShiftList = True

                    Case enUserPriviledge.AllowToDeleteAssignedShift
                        mpreAllowToDeleteAssignedShift = True

                    Case enUserPriviledge.AllowToImportCompanyAssets
                        mpreAllowToImportCompanyAssets = True

                    Case enUserPriviledge.AllowToViewEligibleApplicants
                        mpreAllowToViewEligibleApplicants = True

                    Case enUserPriviledge.AllowToPerformComputeScoreProcess
                        mpreAllowToPerformComputeScoreProcess = True

                    Case enUserPriviledge.AllowToPerformVoidComputedScore
                        mpreAllowToPerformVoidComputedScore = True

                    Case enUserPriviledge.AllowToEditAppraisalSetup
                        mpreAllowToEditAppraisalSetup = True

                    Case enUserPriviledge.AllowToDeleteAppraisalSetup
                        mpreAllowToDeleteAppraisalSetup = True

                    Case enUserPriviledge.AllowToImportTrainingEnrollment
                        mpreAllowToImportTrainingEnrollment = True

                    Case enUserPriviledge.AllowToExportEmployeeList
                        mpreAllowToExportEmployeeList = True

                    Case enUserPriviledge.AllowToAddRemoveFields
                        mpreAllowToAddRemoveFields = True

                    Case enUserPriviledge.AllowToHistoricalSalaryChange
                        mpreAllowToHistoricalSalaryChange = True

                    Case enUserPriviledge.AllowToPostNewProceedings
                        mpreAllowToPostNewProceedings = True

                    Case enUserPriviledge.AllowToEditViewProceedings
                        mpreAllowToEditViewProceedings = True

                    Case enUserPriviledge.AllowToScanAttachmentDocuments
                        mpreAllowToScanAttachmentDocuments = True

                    Case enUserPriviledge.AllowToProceedingSubmitForApproval
                        mpreAllowToProceedingSubmitForApproval = True

                    Case enUserPriviledge.AllowToApproveDisapproveProceedings
                        mpreAllowToApproveDisapproveProceedings = True

                    Case enUserPriviledge.AllowToExemptTransactionHead
                        mpreAllowToExemptTransactionHead = True

                    Case enUserPriviledge.AllowToPostTransactionHead
                        mpreAllowToPostTransactionHead = True

                    Case enUserPriviledge.AllowToViewDisciplineHead
                        mpreAllowToViewDisciplineHead = True

                    Case enUserPriviledge.AllowToPrintPreviewDisciplineCharge
                        mpreAllowToPrintPreviewDisciplineCharge = True

                    Case enUserPriviledge.AllowToAddDisciplinaryCommittee
                        mpreAllowToAddDisciplinaryCommittee = True

                    Case enUserPriviledge.AllowToEditDeleteDisciplinaryCommittee
                        mpreAllowToEditDeleteDisciplinaryCommittee = True

                    Case enUserPriviledge.AllowToViewDisciplineProceedingList
                        mpreAllowToViewDisciplineProceedingList = True

                    Case enUserPriviledge.AllowToViewDisciplineHearingList
                        mpreAllowToViewDisciplineHearingList = True

                    Case enUserPriviledge.AllowToAddDisciplineHearing
                        mpreAllowToAddDisciplineHearing = True

                    Case enUserPriviledge.AllowToEditDisciplineHearing
                        mpreAllowToEditDisciplineHearing = True

                    Case enUserPriviledge.AllowToDeleteDisciplineHearing
                        mpreAllowToDeleteDisciplineHearing = True

                    Case enUserPriviledge.AllowToMarkAsClosedDisciplineHearing
                        mpreAllowToMarkAsClosedDisciplineHearing = True

                    Case enUserPriviledge.AllowToEditTransferEmployeeDetails
                        mpreAllowToEditTransferEmployeeDetails = True

                    Case enUserPriviledge.AllowToDeleteTransferEmployeeDetails
                        mpreAllowToDeleteTransferEmployeeDetails = True

                    Case enUserPriviledge.AllowToEditRecategorizeEmployeeDetails
                        mpreAllowToEditRecategorizeEmployeeDetails = True

                    Case enUserPriviledge.AllowToDeleteRecategorizeEmployeeDetails
                        mpreAllowToDeleteRecategorizeEmployeeDetails = True

                    Case enUserPriviledge.AllowToEditProbationEmployeeDetails
                        mpreAllowToEditProbationEmployeeDetails = True

                    Case enUserPriviledge.AllowToDeleteProbationEmployeeDetails
                        mpreAllowToDeleteProbationEmployeeDetails = True

                    Case enUserPriviledge.AllowToEditConfirmationEmployeeDetails
                        mpreAllowToEditConfirmationEmployeeDetails = True

                   
                    Case enUserPriviledge.AllowToDeleteConfirmationEmployeeDetails
                        mpreAllowToDeleteConfirmationEmployeeDetails = True

                    Case enUserPriviledge.AllowToEditSuspensionEmployeeDetails
                        mpreAllowToEditSuspensionEmployeeDetails = True

                    Case enUserPriviledge.AllowToDeleteSuspensionEmployeeDetails
                        mpreAllowToDeleteSuspensionEmployeeDetails = True

                    Case enUserPriviledge.AllowToEditTerminationEmployeeDetails
                        mpreAllowToEditTerminationEmployeeDetails = True

                    Case enUserPriviledge.AllowToDeleteTerminationEmployeeDetails
                        mpreAllowToDeleteTerminationEmployeeDetails = True

                    Case enUserPriviledge.AllowToRehireEmployee
                        mpreAllowToRehireEmployee = True

                    Case enUserPriviledge.AllowToEditWorkPermitEmployeeDetails
                        mpreAllowToEditWorkPermitEmployeeDetails = True

                    Case enUserPriviledge.AllowToDeleteWorkPermitEmployeeDetails
                        mpreAllowToDeleteWorkPermitEmployeeDetails = True

                    Case enUserPriviledge.AllowToEditRetiredEmployeeDetails
                        mpreAllowToEditRetiredEmployeeDetails = True

                    Case enUserPriviledge.AllowToDeleteRetiredEmployeeDetails
                        mpreAllowToDeleteRetiredEmployeeDetails = True

                    Case enUserPriviledge.AllowToGlobalVoidEmployeeMembership
                        mpreAllowToGlobalVoidEmployeeMembership = True

                    Case enUserPriviledge.AllowToAddShiftPolicyAssignment
                        mpreAllowToAddShiftPolicyAssignment = True

                    Case enUserPriviledge.AllowToAssignEmployeeDayOff
                        mpreAllowToAssignEmployeeDayOff = True

                    Case enUserPriviledge.AllowToViewEmployeeDayOff
                        mpreAllowToViewEmployeeDayOff = True

                    Case enUserPriviledge.AllowToDeleteEmployeeDayOff
                        mpreAllowToDeleteEmployeeDayOff = True

                    Case enUserPriviledge.AllowToViewAssignedPolicyList
                        mpreAllowToViewAssignedPolicyList = True

                    Case enUserPriviledge.AllowToDeleteAssignedPolicy
                        mpreAllowToDeleteAssignedPolicy = True

                    Case enUserPriviledge.AllowToPerformGlobalGoalOperations
                        mpreAllowToPerformGlobalGoalOperations = True

                    Case enUserPriviledge.AllowToPerformGlobalVoidGoals
                        mpreAllowToPerformGlobalVoidGoals = True

                    Case enUserPriviledge.AllowToSubmitForGoalAccomplished
                        mpreAllowToSubmitForGoalAccomplished = True

                    Case enUserPriviledge.AllowToImportEmployeeGoals
                        mpreAllowToImportEmployeeGoals = True

                    Case enUserPriviledge.AllowToPrintEmployeeScoreCard
                        mpreAllowToPrintEmployeeScoreCard = True

                    Case enUserPriviledge.AllowToExportEmployeeScoreCard
                        mpreAllowToExportEmployeeScoreCard = True

                    Case enUserPriviledge.AllowToSetTranHeadInactive
                        mpreAllowToSetTranHeadInactive = True

                    Case enUserPriviledge.AllowToAddLeaveFrequency
                        mpreAllowToAddLeaveFrequency = True

                    Case enUserPriviledge.AllowToDeleteLeaveFrequency
                        mpreAllowToDeleteLeaveFrequency = True

                    Case enUserPriviledge.AllowToViewLeaveFrequency
                        mpreAllowToViewLeaveFrequency = True

                    Case enUserPriviledge.AllowToPerformLeaveAdjustment
                        mpreAllowToPerformLeaveAdjustment = True

                    Case enUserPriviledge.AllowToEditRehireEmployeeDetails
                        mpreAllowToEditRehireEmployeeDetails = True

                        'Case enUserPriviledge.AllowToDeleteRehireEmployeeDetails
                        '    mpreAllowToDeleteRehireEmployeeDetails = True

                        'Case enUserPriviledge.AllowToPerformEligibleOperation
                        '    mpreAllowToPerformEligibleOperation = True

                        'Case enUserPriviledge.AllowToPerformNotEligibleOperation
                        '    mpreAllowToPerformNotEligibleOperation = True
                        'Varsha Rana (17-Oct-2017) -- End

 'S.SANDEEP [11-AUG-2017] -- START
                        'ISSUE/ENHANCEMENT : ARUTI SYMMETRY INTEGRATION
                    Case enUserPriviledge.AllowToSeeEmployeeSignature
                        mpreAllowtoSeeEmployeeSignature = True
                        'S.SANDEEP [11-AUG-2017] -- END

                        'S.SANDEEP [16-Jan-2018] -- START
                        'ISSUE/ENHANCEMENT : REF-ID # 118
                    Case enUserPriviledge.AllowtoAddEmployeeSignature
                        mpreAllowtoAddEmployeeSignature = True
                    Case enUserPriviledge.AllowtoDeleteEmployeeSignature
                        mpreAllowtoDeleteEmployeeSignature = True
                        'S.SANDEEP [16-Jan-2018] -- END

                        'S.SANDEEP [12-Apr-2018] -- START
                        'ISSUE/ENHANCEMENT : {Ref#218|#ARUTI-106}
                    Case enUserPriviledge.AllowtoViewPendingAccrueLeaves
                        mpreAllowtoViewPendingAccrueData = True
                        'S.SANDEEP [12-Apr-2018] -- END


                        'S.SANDEEP [20-JUN-2018] -- Start
                        'Enhancement - Implementing Employee Approver Flow For NMB .
                    Case enUserPriviledge.AllowToViewEmpApproverLevelList
                        mpreAllowToViewEmpApproverLevelList = True

                    Case enUserPriviledge.AllowToAddEmpApproverLevel
                        mpreAllowToAddEmpApproverLevel = True

                    Case enUserPriviledge.AllowToEditEmpApproverLevel
                        mpreAllowToEditEmpApproverLevel = True

                    Case enUserPriviledge.AllowToDeleteEmpApproverLevel
                        mpreAllowToDeleteEmpApproverLevel = True

                    Case enUserPriviledge.AllowToViewEmployeeApproverList
                        mpreAllowToViewEmployeeApproverList = True

                    Case enUserPriviledge.AllowToAddEmployeeApprover
                        mpreAllowToAddEmployeeApprover = True

                    Case enUserPriviledge.AllowToDeleteEmployeeApprover
                        mpreAllowToDeleteEmployeeApprover = True

                    Case enUserPriviledge.AllowToSetInActiveEmployeeApprover
                        mpreAllowtoSetInactiveEmployeeApprover = True

                    Case enUserPriviledge.AllowToSetActiveEmployeeApprover
                        mpreAllowToSetActiveEmployeeApprover = True

                    Case enUserPriviledge.AllowToPerformEmpSubmitForApproval
                        mpreAllowToPerformEmpSubmitForApproval = True

                        'S.SANDEEP [20-JUN-2018] -- End


                       'Gajanan [13-AUG-2018] -- Start
                        'Enhancement - Implementing Grievance Module.
                    Case enUserPriviledge.AllowtoApproveGrievance
                        mpreAllowtoApproveGrievance = True

                    Case enUserPriviledge.AllowToAddGrievanceApproverLevel
                        mpreAllowToAddGrievanceApproverLevel = True

                    Case enUserPriviledge.AllowToEditGrievanceApproverLevel
                        mpreAllowToEditGrievanceApproverLevel = True

                    Case enUserPriviledge.AllowToDeleteGrievanceApproverLevel
                        mpreAllowToDeleteGrievanceApproverLevel = True

                    Case enUserPriviledge.AllowToViewGrievanceApproverLevel
                        mpreAllowToViewGrievanceApproverLevel = True

                    Case enUserPriviledge.AllowToAddGrievanceApprover
                        mpreAllowToAddGrievanceApprover = True

                    Case enUserPriviledge.AllowToEditGrievanceApprover
                        mpreAllowToEditGrievanceApprover = True

                    Case enUserPriviledge.AllowToDeleteGrievanceApprover
                        mpreAllowToDeleteGrievanceApprover = True

                    Case enUserPriviledge.AllowToViewGrievanceApprover
                        mpreAllowToViewGrievanceApprover = True

                    Case enUserPriviledge.AllowToActivateGrievanceApprover
                        mpreAllowToActivateGrievanceApprover = True

                    Case enUserPriviledge.AllowToInActivateGrievanceApprover
                        mpreAllowToInActivateGrievanceApprover = True
                        'Gajanan(13-AUG-2018) -- End

                        'Hemant (27 Aug 2018) -- Start
                        'Enhancement : Implementing Grievance Module
                    Case enUserPriviledge.AllowToViewGrievanceResolutionStepList
                        mpreAllowToViewGrievanceResolutionStepList = True

                    Case enUserPriviledge.AllowToAddGrievanceResolutionStep
                        mpreAllowToAddGrievanceResolutionStep = True

                    Case enUserPriviledge.AllowToEditGrievanceResolutionStep
                        mpreAllowToEditGrievanceResolutionStep = True

                    Case enUserPriviledge.AllowToDeleteGrievanceResolutionStep
                        mpreAllowToDeleteGrievanceResolutionStep = True
                        'Hemant (27 Aug 2018)) -- End

                        'S.SANDEEP [09-OCT-2018] -- START
                    Case enUserPriviledge.AllowToAddTrainingApproverLevel
                        mpreAllowToAddTrainingApproverLevel = True
                    Case enUserPriviledge.AllowToEditTrainingApproverLevel
                        mpreAllowToEditTrainingApproverLevel = True
                    Case enUserPriviledge.AllowToDeleteTrainingApproverLevel
                        mpreAllowToDeleteTrainingApproverLevel = True
                    Case enUserPriviledge.AllowToViewTrainingApproverLevel
                        mpreAllowToViewTrainingApproverLevel = True
                    Case enUserPriviledge.AllowToAddTrainingApprover
                        mpreAllowToAddTrainingApprover = True
                    Case enUserPriviledge.AllowToDeleteTrainingApprover
                        mpreAllowToDeleteTrainingApprover = True
                    Case enUserPriviledge.AllowToViewTrainingApprover
                        mpreAllowToViewTrainingApprover = True
                    Case enUserPriviledge.AllowToApproveTrainingRequisition
                        mpreAllowToApproveTrainingRequisition = True
                    Case enUserPriviledge.AllowToActivateTrainingApprover
                        mpreAllowToActivateTrainingApprover = True
                    Case enUserPriviledge.AllowToDeactivateTrainingApprover
                        mpreAllowToDeactivateTrainingApprover = True
                        'S.SANDEEP [09-OCT-2018] -- END

                        'Hemant (22 Oct 2018) -- Start
                        'Enhancement : Implementing New Module of OT Requisition Approver Level & Approver Master
                    Case enUserPriviledge.AllowToAddOTRequisitionApproverLevel
                        mpreAllowToAddOTRequisitionApproverLevel = True

                    Case enUserPriviledge.AllowToEditOTRequisitionApproverLevel
                        mpreAllowToEditOTRequisitionApproverLevel = True

                    Case enUserPriviledge.AllowToDeleteOTRequisitionApproverLevel
                        mpreAllowToDeleteOTRequisitionApproverLevel = True

                    Case enUserPriviledge.AllowToViewOTRequisitionApproverLevel
                        mpreAllowToViewOTRequisitionApproverLevel = True

                    Case enUserPriviledge.AllowToAddOTRequisitionApprover
                        mpreAllowToAddOTRequisitionApprover = True

                    Case enUserPriviledge.AllowToEditOTRequisitionApprover
                        mpreAllowToEditOTRequisitionApprover = True

                    Case enUserPriviledge.AllowToDeleteOTRequisitionApprover
                        mpreAllowToDeleteOTRequisitionApprover = True

                    Case enUserPriviledge.AllowToViewOTRequisitionApprover
                        mpreAllowToViewOTRequisitionApprover = True

                    Case enUserPriviledge.AllowToActivateOTRequisitionApprover
                        mpreAllowToActivateOTRequisitionApprover = True

                    Case enUserPriviledge.AllowToInActivateOTRequisitionApprover
                        mpreAllowToInActivateOTRequisitionApprover = True

                    Case enUserPriviledge.AllowtoApproveOTRequisition
                        mpreAllowtoApproveOTRequisition = True
                        'Hemant (22 Oct 2018) -- End


                        'Pinkal (03-Dec-2018) -- Start
                        'Enhancement - Working on Asset Declaration Notification settings and notification from reflex.
                    Case enUserPriviledge.AllowtoUnlockEmployeeForAssetDeclaration
                        mpreAllowtoUnlockEmployeeForAssetDeclaration = True
                        'Pinkal (03-Dec-2018) -- End

                        'Sohail (04 Feb 2020) -- Start
                        'NMB Enhancement # : New screen "Non-Disclosure Declaration" with lock / unlock options.
                    Case enUserPriviledge.AllowtoUnlockEmployeeForNonDisclosureDeclaration
                        mpreAllowtoUnlockEmployeeForNonDisclosureDeclaration = True
                        'Sohail (04 Feb 2020) -- End

                        'Gajanan [17-DEC-2018] -- Start
                        'Enhancement - Implementing Employee Approver Flow On Employee Data.
                    Case enUserPriviledge.AllowToApproveRejectEmployeeQualifications
                        mpreAllowToApproveRejectEmployeeQualifications = True

                    Case enUserPriviledge.AllowToApproveRejectEmployeeReferences
                        mpreAllowToApproveRejectEmployeeReferences = True

                    Case enUserPriviledge.AllowToApproveRejectEmployeeSkills
                        mpreAllowToApproveRejectEmployeeSkills = True

                    Case enUserPriviledge.AllowToApproveRejectEmployeeJobExperiences
                        mpreAllowToApproveRejectEmployeeJobExperiences = True
                        'Gajanan [17-DEC-2018] -- End


                        'Gajanan [22-Feb-2019] -- Start
                        'Enhancement - Implementing Employee Approver Flow On Employee Data.
                    Case enUserPriviledge.AllowToApproveRejectEmployeeDependants
                        mpreAllowToApproveRejectEmployeeDependants = True
                    Case enUserPriviledge.AllowToApproveRejectEmployeeIdentities
                        mpreAllowToApproveRejectEmployeeIdentities = True
                    Case enUserPriviledge.AllowToApproveRejectEmployeeMemberships
                        mpreAllowToApproveRejectEmployeeMemberships = True
                        'Gajanan [22-Feb-2019] -- End


                        'Gajanan [18-Mar-2019] -- Start
                        'Enhancement - Implementing Employee Approver Flow On Employee Data.
                    Case enUserPriviledge.AllowToApproveRejectAddress
                        mpreAllowToApproveRejectEmployeeAddress = True

                    Case enUserPriviledge.AllowToApproveRejectEmergencyAddress
                        mpreAllowToApproveRejectEmployeeEmergencyAddress = True
                        'Gajanan [18-Mar-2019] -- End


                        'Gajanan [17-April-2019] -- Start
                        'Enhancement - Implementing Employee Approver Flow On Employee Data.

                    Case enUserPriviledge.AllowToApproveRejectPersonalInfo
                        mpreAllowToApproveRejectEmployeePersonalinfo = True
                        'Gajanan [17-April-2019] -- End



                        'S.SANDEEP |12-FEB-2019| -- START
                        'ISSUE/ENHANCEMENT : {Performance Assessment Changes}
                    Case enUserPriviledge.AllowToDeletePercentCompletedEmployeeGoals
                        mpreAllowToDeletePercentCompletedEmployeeGoals = True
                    Case enUserPriviledge.AllowToViewPercentCompletedEmployeeGoals
                        mpreAllowToViewPercentCompletedEmployeeGoals = True
                        'S.SANDEEP |12-FEB-2019| -- END

                        'Sohail (12 Apr 2019) -- Start
                        'NMB Enhancement - 76.1 - Store System Errors in database and allow to resend and archieve system errors.
                    Case enUserPriviledge.AllowToArchiveSystemErrorLog
                        mpreAllowToArchiveSystemErrorLog = True

                    Case enUserPriviledge.AllowToSendSystemErrorLogEmail
                        mpreAllowToSendSystemErrorLogEmail = True

                    Case enUserPriviledge.AllowToExportSystemErrorLog
                        mpreAllowToExportSystemErrorLog = True

                    Case enUserPriviledge.AllowToViewSystemErrorLog
                        mpreAllowToViewSystemErrorLog = True
                        'Sohail (12 Apr 2019) -- End

                        'Sohail (18 May 2019) -- Start
                        'NMB Enhancement - REF # - 76.1 - On dependants menu, provide a way for user to set status of dependant as deceased etc. Allow user to deactivate a dependant with a reason instead of deleting.
                    Case enUserPriviledge.AllowToSetDependentActive
                        mpreAllowToSetDependentActive = True

                    Case enUserPriviledge.AllowToSetDependentInactive
                        mpreAllowToSetDependentInactive = True

                    Case enUserPriviledge.AllowToDeleteDependentStatus
                        mpreAllowToDeleteDependentStatus = True

                    Case enUserPriviledge.AllowToViewDependentStatus
                        mpreAllowToViewDependentStatus = True
                        'Sohail (18 May 2019) -- End

                        'Sohail (24 Jun 2019) -- Start
                        'NMB Enhancement - REF # - 76.1 - TC012: There should be payroll authorization check before payroll batch posting to flex cube.
                    Case enUserPriviledge.AllowToViewJVPosting
                        mpreAllowToViewJVPosting = True
                        'Sohail (24 Jun 2019) -- End

'S.SANDEEP |27-MAY-2019| -- START
                        'ISSUE/ENHANCEMENT : [Score Calibration Process in Performance & Assessment Module]
                    Case enUserPriviledge.AllowToViewCalibrationApproverLevel
                        mpreAllowToViewCalibrationApproverLevel = True

                    Case enUserPriviledge.AllowToAddCalibrationApproverLevel
                        mpreAllowToAddCalibrationApproverLevel = True

                    Case enUserPriviledge.AllowToEditCalibrationApproverLevel
                        mpreAllowToEditCalibrationApproverLevel = True

                    Case enUserPriviledge.AllowToDeleteCalibrationApproverLevel
                        mpreAllowToDeleteCalibrationApproverLevel = True

                    Case enUserPriviledge.AllowToViewCalibrationApproverMaster
                        mpreAllowToViewCalibrationApproverMaster = True

                    Case enUserPriviledge.AllowToAddCalibrationApproverMaster
                        mpreAllowToAddCalibrationApproverMaster = True

                    Case enUserPriviledge.AllowToActivateCalibrationApproverMaster
                        mpreAllowToActivateCalibrationApproverMaster = True

                    Case enUserPriviledge.AllowToDeleteCalibrationApproverMaster
                        mpreAllowToDeleteCalibrationApproverMaster = True

                    Case enUserPriviledge.AllowToDeActivateCalibrationApproverMaster
                        mpreAllowToDeactivateCalibrationApproverMaster = True

                    Case enUserPriviledge.AllowToApproveRejectCalibratedScore
                        mpreAllowToApproveRejectCalibratedScore = True
                        'S.SANDEEP |27-MAY-2019| -- END

                      'Pinkal (27-Jun-2019) -- Start
                      'Enhancement - IMPLEMENTING OT MODULE
                    Case enUserPriviledge.AllowtoViewOTRequisitionApprovalList
                        mpreAllowtoViewOTRequisitionApprovalList = True
                       'Pinkal (27-Jun-2019) -- End

                        'S.SANDEEP |27-JUL-2019| -- START
                        'ISSUE/ENHANCEMENT : CALIBRATION COMMENTS FROM NMB
                    Case enUserPriviledge.AllowToEditCalibratedScore
                        mpreAllowToEditCalibratedScore = True
                        'S.SANDEEP |27-JUL-2019| -- END

                        'Pinkal (13-Aug-2019) -- Start
                        'Enhancement [0003693 - PACT] - Working on Allow people to exceed time assigned to the project initially.
                    Case enUserPriviledge.AllowToViewBudgetTimesheetExemptEmployeeList
                        mpreAllowToViewBudgetTimesheetExemptEmployeeList = True

                    Case enUserPriviledge.AllowToAddBudgetTimesheetExemptEmployee
                        mpreAllowToAddBudgetTimesheetExemptEmployee = True

                    Case enUserPriviledge.AllowToDeleteBudgetTimesheetExemptEmployee
                        mpreAllowToDeleteBudgetTimesheetExemptEmployee = True
                        'Pinkal (13-Aug-2019) -- End

                        'Sohail (29 Aug 2019) -- Start
                        'NMB Payroll UAT # TC008 - 76.1 - System should be able to restrict batch posting when it is holiday unless it is activated by user to allow batch posting on holidays.
                    Case enUserPriviledge.AllowToPostJVOnHolidays
                        mpreAllowToPostJVOnHolidays = True
                        'Sohail (29 Aug 2019) -- End

'S.SANDEEP |16-AUG-2019| -- START
                        'ISSUE/ENHANCEMENT : ZRA {Language & Custom Item} & NMB Email Notification {Calibration}
                    Case enUserPriviledge.AllowtoCalibrateProvisionalScore
                        mpreAllowtoCalibrateProvisionalScore = True
                        'S.SANDEEP |16-AUG-2019| -- END



                        'Gajanan [18-OCT-2019] -- Start    
                        'Enhancement:Provide Privileges "allowto Add/edit Opertional privileges".
                    Case enUserPriviledge.AllowToAddEditOprationalPrivileges
                        mpreAllowToAddEditOprationalPrivileges = True
                        'Gajanan [18-OCT-2019] -- End



                        'Pinkal (08-Jan-2020) -- Start
                        'Enhancement - NMB - Working on NMB OT Requisition Requirement.

                    Case enUserPriviledge.AllowToViewEmpOTAssignment
                        mpreAllowToViewEmpOTAssignment = True

                    Case enUserPriviledge.AllowToAddEmpOTAssignment
                        mpreAllowToAddEmpOTAssignment = True

                    Case enUserPriviledge.AllowToDeleteEmpOTAssignment
                        mpreAllowToDeleteEmpOTAssignment = True

                    Case enUserPriviledge.AllowToCancelEmpOTRequisition
                        mpreAllowToCancelEmpOTRequisition = True

                        'Pinkal (08-Jan-2020) -- End

                        'S.SANDEEP |18-JAN-2020| -- START
                        'ISSUE/ENHANCEMENT : PA-OPTIMIZATION
                    Case enUserPriviledge.AllowToUnlockEmployeeInPlanning
                        mpreAllowToUnlockEmployeeInPlanning = True
                    Case enUserPriviledge.AllowToUnlockEmployeeInAssessment
                        mpreAllowToUnlockEmployeeInAssessment = True
                        'S.SANDEEP |18-JAN-2020| -- END

                        'Pinkal (29-Jan-2020) -- Start
                        'Enhancement - Changes related To OT NMB Testing.
                    Case enUserPriviledge.AllowToPostOTRequisitionToPayroll
                        mpreAllowToPostOTRequisitionToPayroll = True
                        'Pinkal (29-Jan-2020) -- End

                        'Pinkal (03-Mar-2020) -- Start
                        'ENHANCEMENT NMB:  Working on OT Requisition Approver Migration and transfer and recategorization migration enforcement screen.
                    Case enUserPriviledge.AllowToMigrateOTRequisitionApprover
                        mpreAllowToMigrateOTRequisitionApprover = True
                        'Pinkal (03-Mar-2020) -- End

                        'S.SANDEEP |01-MAY-2020| -- START
                        'ISSUE/ENHANCEMENT : CALIBRATION MAKEOVER
                    Case enUserPriviledge.AllowToViewCalibratorList
                        mpreAllowToViewCalibratorList = True
                    Case enUserPriviledge.AllowToAddCalibrator
                        mpreAllowToAddCalibrator = True
                    Case enUserPriviledge.AllowToMakeCalibratorActive
                        mpreAllowToMakeCalibratorActive = True
                    Case enUserPriviledge.AllowToDeleteCalibrator
                        mpreAllowToDeleteCalibrator = True
                    Case enUserPriviledge.AllowToMakeCalibratorInactive
                        mpreAllowToMakeCalibratorInactive = True
                    Case enUserPriviledge.AllowToEditCalibrator
                        mpreAllowToEditCalibrator = True
                    Case enUserPriviledge.AllowToEditCalibrationApproverMaster
                        mpreAllowToEditCalibrationApproverMaster = True
                        'S.SANDEEP |01-MAY-2020| -- END

                        'Hemant (03 Jun 2020) -- Start
                        'ISSUE/ENHANCEMENT(NMB) :  RECRUITMENTS FINAL UAT CHANGES(Point No - 1 : On the interview analysis screen, they want to record interview scores for all applicants in a single window instead of analysing the interview an applicant at a time. Also, they want to record all the interview scores for all the interviewers on the same screen.)
                    Case enUserPriviledge.AllowtoViewGlobalInterviewAnalysisList
                        mpreAllowtoViewGlobalInterviewAnalysisList = True
                        'Hemant (03 Jun 2020) -- End


                        'Gajanan [24-Aug-2020] -- Start
                        'NMB Enhancement : Allow to set account configuration mapping 
                        'inactive for closed period to allow to map head on other account 
                        'configuration screen from new period
                    Case enUserPriviledge.AllowToInactiveEmployeeCostCenter
                        mpreAllowToInactiveEmployeeCostCenter = True

                    Case enUserPriviledge.AllowToInactiveCompanyAccountConfiguration
                        mpreAllowToInactiveCompanyAccountConfiguration = True

                    Case enUserPriviledge.AllowToInactiveEmployeeAccountConfiguration
                        mpreAllowToInactiveEmployeeAccountConfiguration = True

                    Case enUserPriviledge.AllowToInactiveCostCenterAccountConfiguration
                        mpreAllowToInactiveCostCenterAccountConfiguration = True
                        'Gajanan [24-Aug-2020] -- End

                        'Hemant (26 Nov 2020) -- Start
                        'Enhancement : Talent Pipeline new changes
                    Case enUserPriviledge.AllowToAddPotentialTalentEmployee
                        mpreAllowToAddPotentialTalentEmployee = True

                    Case enUserPriviledge.AllowToApproveRejectTalentPipelineEmployee
                        mpreAllowToApproveRejectTalentPipelineEmployee = True

                    Case enUserPriviledge.AllowToMoveApprovedEmployeeToQulified
                        mpreAllowToMoveApprovedEmployeeToQulified = True

                    Case enUserPriviledge.AllowToViewPotentialTalentEmployee
                        mpreAllowToViewPotentialTalentEmployee = True

                    Case enUserPriviledge.AllowToRemoveTalentProcess
                        mpreAllowToRemoveTalentProcess = True

                        'Hemant (26 Nov 2020) -- End

                        'Gajanan [17-Dec-2020] -- Start
                    Case enUserPriviledge.AllowToAddPotentialSuccessionEmployee
                        mpreAllowToAddPotentialSuccessionEmployee = True

                    Case enUserPriviledge.AllowToApproveRejectSuccessionPipelineEmployee
                        mpreAllowToApproveRejectSuccessionPipelineEmployee = True

                    Case enUserPriviledge.AllowToMoveApprovedEmployeeToQulifiedSuccession
                        mpreAllowToMoveApprovedEmployeeToQulifiedSuccession = True

                    Case enUserPriviledge.AllowToViewPotentialSuccessionEmployee
                        mpreAllowToViewPotentialSuccessionEmployee = True

                    Case enUserPriviledge.AllowToRemoveSuccessionProcess
                        mpreAllowToRemoveSuccessionProcess = True

                        'Gajanan [17-Dec-2020] -- End

                        'Gajanan [19-Feb-2021] -- Start
                    Case enUserPriviledge.AllowToSendNotificationToTalentScreener
                        mpreAllowToSendNotificationToTalentScreener = True

                    Case enUserPriviledge.AllowToSendNotificationToTalentEmployee
                        mpreAllowToSendNotificationToTalentEmployee = True

                    Case enUserPriviledge.AllowForTalentScreeningProcess
                        mpreAllowForTalentScreeningProcess = True

                    Case enUserPriviledge.AllowToSendNotificationToSuccessionScreener
                        mpreAllowToSendNotificationToSuccessionScreener = True

                    Case enUserPriviledge.AllowToSendNotificationToSuccessionEmployee
                        mpreAllowToSendNotificationToSuccessionEmployee = True

                    Case enUserPriviledge.AllowForSuccessionScreeningProcess
                        mpreAllowForSuccessionScreeningProcess = True

                    Case enUserPriviledge.AllowToSendNotificationToTalentApprover
                        mpreAllowToSendNotificationToTalentApprover = True

                    Case enUserPriviledge.AllowToSendNotificationToSuccessionApprover
                        mpreAllowToSendNotificationToSuccessionApprover = True

                        'Gajanan [19-Feb-2021] -- End

'Gajanan [30-JAN-2021] -- Start   
                        'Enhancement:Worked On PDP Module

                    Case enUserPriviledge.AllowToViewLearningAndDevelopmentPlan
                        mpreAllowToViewLearningAndDevelopmentPlan = True

                    Case enUserPriviledge.AllowToAddPersonalAnalysisGoal
                        mpreAllowToAddPersonalAnalysisGoal = True

                    Case enUserPriviledge.AllowToEditPersonalAnalysisGoal
                        mpreAllowToEditPersonalAnalysisGoal = True

                    Case enUserPriviledge.AllowToDeletePersonalAnalysisGoal
                        mpreAllowToDeletePersonalAnalysisGoal = True

                    Case enUserPriviledge.AllowToAddDevelopmentActionPlan
                        mpreAllowToAddDevelopmentActionPlan = True

                    Case enUserPriviledge.AllowToEditDevelopmentActionPlan
                        mpreAllowToEditDevelopmentActionPlan = True

                    Case enUserPriviledge.AllowToDeleteDevelopmentActionPlan
                        mpreAllowToDeleteDevelopmentActionPlan = True

                    Case enUserPriviledge.AllowToAddUpdateProgressForDevelopmentActionPlan
                        mpreAllowToAddUpdateProgressForDevelopmentActionPlan = True

                        'Gajanan [30-JAN-2021] -- End

                        'Gajanan [26-Feb-2021] -- Start
                    Case enUserPriviledge.AllowToNominateEmployeeForSuccession
                        mpreAllowToNominateEmployeeForSuccession = True

                    Case enUserPriviledge.AllowToModifyTalentSetting
                        mpreAllowToModifyTalentSetting = True

                    Case enUserPriviledge.AllowToViewTalentSetting
                        mpreAllowToViewTalentSetting = True

                    Case enUserPriviledge.AllowToModifySuccessionSetting
                        mpreAllowToModifySuccessionSetting = True

                    Case enUserPriviledge.AllowToViewSuccessionSetting
                        mpreAllowToViewSuccessionSetting = True
                        'Gajanan [26-Feb-2021] -- End

                        'Pinkal (03-Mar-2021)-- Start
                        'KADCO Enhancement  -  Working on  KADCO - Option to Adjust Expense Accrue Balance.
                    Case enUserPriviledge.AllowToAdjustExpenseBalance
                        mpreAllowToAdjustExpenseBalance = True
                        'Pinkal (03-Mar-2021) -- End


                        'Pinkal (10-Mar-2021) -- Start
                        'Enhancement/Bug Retirement -   Working Retirement enhancement/Bug.
                    Case enUserPriviledge.AllowToViewRetireClaimApplication
                        mpreAllowToViewRetireClaimApplication = True

                    Case enUserPriviledge.AllowToRetireClaimApplication
                        mpreAllowToRetireClaimApplication = True

                    Case enUserPriviledge.AllowToEditRetiredClaimApplication
                        mpreAllowToEditRetiredClaimApplication = True

                    Case enUserPriviledge.AllowToViewRetiredApplicationApproval
                        mpreAllowToViewRetiredApplicationApproval = True

                    Case enUserPriviledge.AllowToApproveRetiredApplication
                        mpreAllowToApproveRetiredApplication = True

                    Case enUserPriviledge.AllowToPostRetiredApplicationtoPayroll
                        mpreAllowToPostRetiredApplicationtoPayroll = True
                        'Pinkal (10-Mar-2021) -- End

Case enUserPriviledge.AllowToViewTalentScreeningDetail
                        mpreAllowToViewTalentScreeningDetail = True

                    Case enUserPriviledge.AllowToViewSuccessionScreeningDetail
                        mpreAllowToViewSuccessionScreeningDetail = True

                        'Sohail (01 Mar 2021) -- Start
                        'NMB Enhancement : : New screen Departmental Training Need in New UI.
                    Case enUserPriviledge.AllowToViewDepartmentalTrainingNeed
                        mpreAllowToViewDepartmentalTrainingNeed = True

                    Case enUserPriviledge.AllowToAddDepartmentalTrainingNeed
                        mpreAllowToAddDepartmentalTrainingNeed = True

                    Case enUserPriviledge.AllowToEditDepartmentalTrainingNeed
                        mpreAllowToEditDepartmentalTrainingNeed = True

                    Case enUserPriviledge.AllowToDeleteDepartmentalTrainingNeed
                        mpreAllowToDeleteDepartmentalTrainingNeed = True
                        'Sohail (01 Mar 2021) -- End

                        'Hemant (26 Mar 2021) -- Start
                        'NMB Enhancement : : Changes in Departmental Training Need as per comments in online doc in New UI.
                    Case enUserPriviledge.AllowToSubmitForApprovalFromDepartmentalTrainingNeed
                        mpreAllowToSubmitForApprovalFromDepartmentalTrainingNeed = True

                    Case enUserPriviledge.AllowToTentativeApproveForDepartmentalTrainingNeed
                        mpreAllowToTentativeApproveForDepartmentalTrainingNeed = True

                    Case enUserPriviledge.AllowToSubmitForApprovalFromTrainingBacklog
                        mpreAllowToSubmitForApprovalFromTrainingBacklog = True

                    Case enUserPriviledge.AllowToFinalApproveDepartmentalTrainingNeed
                        mpreAllowToFinalApproveDepartmentalTrainingNeed = True

                    Case enUserPriviledge.AllowToRejectDepartmentalTrainingNeed
                        mpreAllowToRejectDepartmentalTrainingNeed = True

                    Case enUserPriviledge.AllowToUnlockSubmitApprovalForDepartmentalTrainingNeed
                        mpreAllowToUnlockSubmitApprovalForDepartmentalTrainingNeed = True

                    Case enUserPriviledge.AllowToUnlockSubmittedForTrainingBudgetApproval
                        mpreAllowToUnlockSubmittedForTrainingBudgetApproval = True

                    Case enUserPriviledge.AllowToAskForReviewTrainingAsPerAmountSet
                        mpreAllowToAskForReviewTrainingAsPerAmountSet = True

                    Case enUserPriviledge.AllowToUndoApprovedTrainingBudgetApproval
                        mpreAllowToUndoApprovedTrainingBudgetApproval = True
                        'Hemant (26 Mar 2021) -- End

                    Case enUserPriviledge.AllowToAddCommentForDevelopmentActionPlan
                        mpreAllowToAddCommentForDevelopmentActionPlan = True

                    Case enUserPriviledge.AllowToViewActionPlanAssignedTrainingCourses
                        mpreAllowToViewActionPlanAssignedTrainingCourses = True

                        'Sohail (12 Apr 2021) -- Start
                        'NMB Enhancement : : Changes in Departmental Training Need as per comments in online doc in New UI.
                    Case enUserPriviledge.AllowToUndoRejectedTrainingBudgetApproval
                        mpreAllowToUndoRejectedTrainingBudgetApproval = True
                        'Sohail (12 Apr 2021) -- End

                        'Sohail (26 Apr 2021) -- Start
                        'NMB Enhancement : : Changes in Departmental Training Need as per comments in online doc in New UI.
                    Case enUserPriviledge.AllowToViewBudgetSummaryForDepartmentalTrainingNeed
                        mpreAllowToViewBudgetSummaryForDepartmentalTrainingNeed = True

                    Case enUserPriviledge.AllowToSetMaxBudgetForDepartmentalTrainingNeed
                        mpreAllowToSetMaxBudgetForDepartmentalTrainingNeed = True
                        'Sohail (26 Apr 2021) -- End

                        'Hemant (15 Apr 2021) -- Start
                        'NMB Enhancement : : Approvers concepts need to be revisited. Should be based on the concept of Allocations and no on employee assignment.
                    Case enUserPriviledge.AllowToApproveTrainingRequestRelatedToCost
                        mpreAllowToApproveTrainingRequestRelatedToCost = True

                    Case enUserPriviledge.AllowToApproveTrainingRequestForeignTravelling
                        mpreAllowToApproveTrainingRequestForeignTravelling = True
                        'Hemant (15 Apr 2021) -- End

Case enUserPriviledge.AllowToUnlockPDPForm
                        mpreAllowToUnlockPDPForm = True

                    Case enUserPriviledge.AllowToLockPDPForm
                        mpreAllowToLockPDPForm = True

                    Case enUserPriviledge.AllowToUndoUpdateProgressForDevelopmentActionPlan
                        mpreAllowToUndoUpdateProgressForDevelopmentActionPlan = True

                        'Hemant (18 May 2021) -- Start
                        'ISSUE/ENHANCEMENT : Changed approval flow for Training Completion Status
                    Case enUserPriviledge.AllowToApproveRejectTrainingCompletion
                        mpreAllowToApproveRejectTrainingCompletion = True

                    Case enUserPriviledge.AllowToAddAttendedTraining
                        mpreAllowToAddAttendedTraining = True
                        'Hemant (18 May 2021) -- End

                        'Hemant (25 May 2021) -- Start
                        'ISSUE/ENHANCEMENT : OLD-398 - NMB COE Modules demo feedback for Training Modulue
                    Case enUserPriviledge.AllowToMarkTrainingAsComplete
                        mpreAllowToMarkTrainingAsComplete = True
                        'Hemant (25 May 2021) -- End

                        'S.SANDEEP |10-MAR-2022| -- START
                        'ISSUE/ENHANCEMENT : OLD-580
                    Case enUserPriviledge.Allowtoaddrebatefornonemployee
                        mpreAllowToAddRebateForNonEmployee = True
                        'S.SANDEEP |10-MAR-2022| -- END


                        'Hemant (29 Apr 2022) -- Start
                        'ISSUE/ENHANCEMENT : AC2-314(NMB) - NMB - As a manager, I want different approval flow (Training requests) for different training courses.
                    Case enUserPriviledge.AllowToApproveTrainingRequestLocalTravelling
                        mpreAllowToApproveTrainingRequestLocalTravelling = True
                        'Hemant (29 Apr 2022) -- End

                        'Hemant (11 Jul 2022) -- Start            
                        'ENHANCEMENT(NMB) : AC2-720 - Training approver add/edit with User Mapping
                    Case enUserPriviledge.AllowToViewTrainingApproverEmployeeMappingList
                        mpreAllowToViewTrainingApproverEmployeeMappingList = True

                    Case enUserPriviledge.AllowToEditTrainingApproverEmployeeMapping
                        mpreAllowToEditTrainingApproverEmployeeMapping = True

                    Case enUserPriviledge.AllowToDeleteTrainingApproverEmployeeMapping
                        mpreAllowToDeleteTrainingApproverEmployeeMapping = True

                    Case enUserPriviledge.AllowToSetActiveInactiveTrainingApproverEmployeeMapping
                        mpreAllowToSetActiveInactiveTrainingApproverEmployeeMapping = True

                    Case enUserPriviledge.AllowToAddTrainingApproverEmployeeMapping
                        mpreAllowToAddTrainingApproverEmployeeMapping = True

                    Case enUserPriviledge.AllowToApproveTrainingRequestEmployeeMapping
                        mpreAllowToApproveTrainingRequestEmployeeMapping = True
                        'Hemant (11 Jul 2022) -- End

                        'Pinkal (27-Oct-2022) -- Start
                        'NMB Loan Module Enhancement.
                    Case enUserPriviledge.AllowToChangeDeductionPeriodonLoanApproval
                        mpreAllowToChangeDeductionPeriodOnLoanApproval = True
                        'Pinkal (27-Oct-2022) -- End

                        'Pinkal (23-Nov-2022) -- Start
                        'NMB Loan Module Enhancement.
                    Case enUserPriviledge.AllowToAccessLoanDisbursementDashboard
                        mpreAllowToAccessLoanDisbursementDashboard = True

                    Case enUserPriviledge.AllowToGetCRBData
                        mpreAllowToGetCRBData = True
                        'Pinkal (23-Nov-2022) -- End

                        'Pinkal (14-Dec-2022) -- Start
                        'NMB Loan Module Enhancement.
                    Case enUserPriviledge.AllowToSetDisbursementTranches
                        mpreAllowToSetDisbursementTranches = True
                        'Pinkal (14-Dec-2022) -- End

                        'Pinkal (06-Jan-2023) -- Start
                        '(A1X-540) FHS/PSI - As a user, I want to select the cost center that applies to my project when filling my project budget timesheets.
                    Case enUserPriviledge.AllowToViewLoanSchemeRoleMapping
                        mpreAllowToViewLoanSchemeRoleMapping = True

                    Case enUserPriviledge.AllowToAddLoanSchemeRoleMapping
                        mpreAllowToAddLoanSchemeRoleMapping = True

                    Case enUserPriviledge.AllowToEditLoanSchemeRoleMapping
                        mpreAllowToEditLoanSchemeRoleMapping = True

                    Case enUserPriviledge.AllowToDeleteLoanSchemeRoleMapping
                        mpreAllowToDeleteLoanSchemeRoleMapping = True

                        'Pinkal (06-Jan-2023) -- End 

                        'Pinkal (20-Feb-2023) -- Start
                        'NMB - Loan Approval Screen Enhancements.
                    Case enUserPriviledge.AllowToViewLoanExposuresOnLoanApproval
                        mpreAllowToViewLoanExposuresOnLoanApproval = True
                        'Pinkal (20-Feb-2023) -- End

                        'Pinkal (31-Mar-2023) -- Start
                        '(A1X-717) NMB - As a user, based on the data displayed from the selection, I want to be able to shoot emails from the Feedback screen instead of going to the mail utility again. I should be able to select the template directly from this screen and input the subject.
                    Case enUserPriviledge.AllowToSendCandidateFeedback
                        mpreAllowToSendCandidateFeedback = True
                        'Pinkal (31-Mar-2023) -- End

                        'Pinkal (28-Apr-2023) -- Start
                        '(A1X-865) NMB - Read aptitude test results from provided views.
                    Case enUserPriviledge.AllowToDownloadAptitudeResult
                        mpreAllowToDownloadAptitudeResult = True
                        'Pinkal (28-Apr-2023) -- End

                        'Pinkal (03-Jun-2023) -- Start
                        'NMB Enhancement - Adding Staff Loan Balance View privilege.
                    Case enUserPriviledge.AllowToViewStaffLoanBalance
                        mpreAllowToViewStaffLoanBalance = True
                        'Pinkal (03-Jun-2023) -- End

'S.SANDEEP |05-JUN-2023| -- START
                        'ISSUE/ENHANCEMENT : A1X-948
                    Case enUserPriviledge.AllowToApproveVacancy
                        mpreAllowToApproveVacancyChanges = True
                        'S.SANDEEP |0-JUN-2023| -- END

                        'Hemant (26 May 2023) -- Start
                        'ENHANCEMENT(TRA) : A1X-943 - Integrate NBC payment API in Aruti
                    Case enUserPriviledge.AllowToApproveRejectPaymentBatchPosting
                        mpreAllowToApproveRejectPaymentBatchPosting = True
                        'Hemant (26 May 2023) -- End


                        'Pinkal (26-Jun-2023) -- Start
                        'NMB Enhancement :(A1X-1029) NMB - Staff transfers approval(role based).
                    Case enUserPriviledge.AllowToViewStaffTransferApproverLevel
                        mpreAllowToViewStaffTransferApproverLevel = True

                    Case enUserPriviledge.AllowToAddStaffTransferApproverLevel
                        mpreAllowToAddStaffTransferApproverLevel = True

                    Case enUserPriviledge.AllowToEditStaffTransferApproverLevel
                        mpreAllowToEditStaffTransferApproverLevel = True

                    Case enUserPriviledge.AllowToDeleteStaffTransferApproverLevel
                        mpreAllowToDeleteStaffTransferApproverLevel = True

                    Case enUserPriviledge.AllowToViewStaffTransferLevelRoleMapping
                        mpreAllowToViewStaffTransferLevelRoleMapping = True

                    Case enUserPriviledge.AllowToAddStaffTransferLevelRoleMapping
                        mpreAllowToAddStaffTransferLevelRoleMapping = True

                    Case enUserPriviledge.AllowToEditStaffTransferLevelRoleMapping
                        mpreAllowToEditStaffTransferLevelRoleMapping = True

                    Case enUserPriviledge.AllowToDeleteStaffTransferLevelRoleMapping
                        mpreAllowToDeleteStaffTransferLevelRoleMapping = True

                    Case enUserPriviledge.AllowToViewStaffTransferApprovalList
                        mpreAllowToViewStaffTransferApprovalList = True

                    Case enUserPriviledge.AllowToApproveStaffTransferApproval
                        mpreAllowToApproveStaffTransferApproval = True

                        'Pinkal (26-Jun-2023) -- End

                        'Hemant (15 Sep 2023) -- Start
                        'ENHANCEMENT(TRA): A1X-1257 - Coaches/coachees nomination request and approval
                    Case enUserPriviledge.AllowToViewCoachingApproverLevel
                        mpreAllowToViewCoachingApproverLevel = True

                    Case enUserPriviledge.AllowToAddCoachingApproverLevel
                        mpreAllowToAddCoachingApproverLevel = True

                    Case enUserPriviledge.AllowToEditCoachingApproverLevel
                        mpreAllowToEditCoachingApproverLevel = True

                    Case enUserPriviledge.AllowToDeleteCoachingApproverLevel
                        mpreAllowToDeleteCoachingApproverLevel = True

                    Case enUserPriviledge.AllowToFillCoachingForm
                        mpreAllowToFillCoachingForm = True

                    Case enUserPriviledge.AllowToApproveCoachingForm
                        mpreAllowToApproveCoachingForm = True

                    Case enUserPriviledge.AllowToEditCoachingForm
                        mpreAllowToEditCoachingForm = True

                    Case enUserPriviledge.AllowToDeleteCoachingForm
                        mpreAllowToDeleteCoachingForm = True

                    Case enUserPriviledge.AllowToViewCoachingForm
                        mpreAllowToViewCoachingForm = True

                    Case enUserPriviledge.AllowToFillReplacementForm
                        mpreAllowToFillReplacementForm = True
                        'Hemant (15 Sep 2023) -- End

 'Pinkal (30-Sep-2023) -- Start
                        '(A1X-1371) Toyota - New desktop dashboard item: Total Work Anniversaries Within 7 Days from Now.
                    Case enUserPriviledge.ShowForcastedWorkAnniversary
                        mpreShowForcastedWorkAnniversary = True

                    Case enUserPriviledge.ShowForcastedWorkResidencePermitExpiry
                        mpreShowForcastedWorkResidencePermitExpiry = True

                    Case enUserPriviledge.ShowTotalLeaversFromCurrentFY
                        mpreShowTotalLeaversFromCurrentFY = True

                        'Pinkal (30-Sep-2023) -- End

                End Select
            Next

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "setUserPrivilege", mstrModuleName)
        End Try
    End Sub

    Public Function getUserPrivilegeList(ByVal intUserUnkid As Integer, Optional ByVal strListName As String = "List", Optional ByVal intPrivilegeunkId As Integer = -1) As DataSet
        'Hemant (11 Jul 2022) -- [intPrivilegeunkId]
        Dim dsList As New DataSet
        Dim objDataOperation As New clsDataOperation
        Dim StrQ As String
        Dim exForce As Exception
        Try
            'S.SANDEEP [ 13 JUNE 2011 ] -- START
            StrQ = "SELECT " & _
                   "    hrmsConfiguration..cfuser_privilege.privilegeunkid " & _
                   "   ,privilege_name " & _
                   "FROM hrmsConfiguration..cfuser_privilege,hrmsConfiguration..cfuserprivilege_master " & _
                   "WHERE userunkid = @userunkid " & _
                   "AND hrmsConfiguration..cfuser_privilege.privilegeunkid = hrmsConfiguration..cfuserprivilege_master.privilegeunkid " & _
                   "AND hrmsConfiguration..cfuserprivilege_master.isactive = 1 "
            'S.SANDEEP [ 13 JUNE 2011 ] -- END

            'Hemant (11 Jul 2022) -- Start            
            'ISSUE(Bencon) : After Final approval of loan, needs to be alerted to assign it to another company User
            If intPrivilegeunkId > 0 Then
                StrQ &= " AND hrmsConfiguration..cfuser_privilege.privilegeunkid = @privilegeunkid"
                objDataOperation.AddParameter("@privilegeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intPrivilegeunkId.ToString)
            End If
            'Hemant (11 Jul 2022) -- End

            'S.SANDEEP [28 MAY 2015] -- START
            'ENHANCEMENT : NEW PRIVILEGE (Performance Assessment)
            ' AND hrmsConfiguration..cfuserprivilege_master.isactive = 1 ---> ADDED
            'S.SANDEEP [28 MAY 2015] -- END


            objDataOperation.AddParameter("@userunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intUserUnkid.ToString)

            dsList = objDataOperation.ExecQuery(StrQ, strListName)

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            Return dsList

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "getUserPrivilegeList", mstrModuleName)
            Return Nothing
        Finally
            exForce = Nothing
            dsList.Dispose()
            dsList = Nothing
            objDataOperation = Nothing
        End Try
    End Function

#End Region

End Class