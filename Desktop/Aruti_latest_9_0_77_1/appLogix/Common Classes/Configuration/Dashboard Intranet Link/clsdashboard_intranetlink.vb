﻿'************************************************************************************************************************************
'Class Name : clsdashboard_intranetlink.vb
'Purpose    :
'Date       :25-Sep-2023
'Written By :Pinkal
'Modified   :
'************************************************************************************************************************************

Imports eZeeCommonLib
''' <summary>
''' Purpose: 
''' Developer: Pinkal
''' </summary>
Public Class clsdashboard_intranetlink
    Private Shared ReadOnly mstrModuleName As String = "clsdashboard_intranetlink"
    Dim objDataOperation As clsDataOperation
    Dim mstrMessage As String = ""

#Region " Private variables "
    Private mintIntranetlinkunkid As Integer
    Private mintCompanyunkid As Integer
    Private mstrCaption As String = String.Empty
    Private mstrIntranetlink As String = String.Empty
    Private mintUserunkid As Integer
    Private mblnIsvoid As Boolean
    Private mintVoiduserunkid As Integer
    Private mdtVoiddatetime As Date
    Private mstrVoidreason As String = String.Empty
    Private mintAuditUserId As Integer = 0
    Private mstrClientIP As String = ""
    Private mstrHostName As String = ""
    Private mblnIsFromWeb As Boolean = False
    Private mstrFormName As String = ""
    Private mdtTran As DataTable = Nothing

#End Region

#Region " Properties "
    ''' <summary>
    ''' Purpose: Get Message from Class 
    ''' Modify By: Pinkal
    ''' </summary>
    Public ReadOnly Property _Message() As String
        Get
            Return mstrMessage
        End Get
    End Property

    ''' <summary>
    ''' Purpose: Get or Set intranetlinkunkid
    ''' Modify By: Pinkal
    ''' </summary>
    Public Property _Intranetlinkunkid(Optional ByVal objDoOperation As clsDataOperation = Nothing) As Integer
        Get
            Return mintIntranetlinkunkid
        End Get
        Set(ByVal value As Integer)
            mintIntranetlinkunkid = value
            Call GetData(objDoOperation)
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set companyunkid
    ''' Modify By: Pinkal
    ''' </summary>
    Public Property _Companyunkid() As Integer
        Get
            Return mintCompanyunkid
        End Get
        Set(ByVal value As Integer)
            mintCompanyunkid = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set caption
    ''' Modify By: Pinkal
    ''' </summary>
    Public Property _Caption() As String
        Get
            Return mstrCaption
        End Get
        Set(ByVal value As String)
            mstrCaption = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set intranetlink
    ''' Modify By: Pinkal
    ''' </summary>
    Public Property _Intranetlink() As String
        Get
            Return mstrIntranetlink
        End Get
        Set(ByVal value As String)
            mstrIntranetlink = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set userunkid
    ''' Modify By: Pinkal
    ''' </summary>
    Public Property _Userunkid() As Integer
        Get
            Return mintUserunkid
        End Get
        Set(ByVal value As Integer)
            mintUserunkid = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set isvoid
    ''' Modify By: Pinkal
    ''' </summary>
    Public Property _Isvoid() As Boolean
        Get
            Return mblnIsvoid
        End Get
        Set(ByVal value As Boolean)
            mblnIsvoid = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set voiduserunkid
    ''' Modify By: Pinkal
    ''' </summary>
    Public Property _Voiduserunkid() As Integer
        Get
            Return mintVoiduserunkid
        End Get
        Set(ByVal value As Integer)
            mintVoiduserunkid = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set voiddatetime
    ''' Modify By: Pinkal
    ''' </summary>
    Public Property _Voiddatetime() As Date
        Get
            Return mdtVoiddatetime
        End Get
        Set(ByVal value As Date)
            mdtVoiddatetime = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set voidreason
    ''' Modify By: Pinkal
    ''' </summary>
    Public Property _Voidreason() As String
        Get
            Return mstrVoidreason
        End Get
        Set(ByVal value As String)
            mstrVoidreason = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set FormName
    ''' Modify By: Pinkal
    ''' </summary>
    Public Property _FormName() As String
        Get
            Return mstrFormName
        End Get
        Set(ByVal value As String)
            mstrFormName = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set ClientIP
    ''' Modify By: Pinkal
    ''' </summary>
    Public Property _ClientIP() As String
        Get
            Return mstrClientIP
        End Get
        Set(ByVal value As String)
            mstrClientIP = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set HostName
    ''' Modify By: Pinkal
    ''' </summary>
    Public Property _HostName() As String
        Get
            Return mstrHostName
        End Get
        Set(ByVal value As String)
            mstrHostName = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set IsFromWeb
    ''' Modify By: Pinkal
    ''' </summary>
    Public Property _IsFromWeb() As Boolean
        Get
            Return mblnIsFromWeb
        End Get
        Set(ByVal value As Boolean)
            mblnIsFromWeb = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set AuditUserId
    ''' Modify By: Pinkal
    ''' </summary>
    Public Property _AuditUserId() As Integer
        Get
            Return mintAuditUserId
        End Get
        Set(ByVal value As Integer)
            mintAuditUserId = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set AuditUserId
    ''' Modify By: Pinkal
    ''' </summary>
    Public Property _dtTable() As DataTable
        Get
            Return mdtTran
        End Get
        Set(ByVal value As DataTable)
            mdtTran = value
        End Set
    End Property


#End Region

#Region " Constructor "

    Public Sub New()
        mdtTran = New DataTable("IntranetLink")
        mdtTran.Columns.Add("intranetlinkunkid", System.Type.GetType("System.Int32")).DefaultValue = 0
        mdtTran.Columns.Add("companyunkid", System.Type.GetType("System.Int32")).DefaultValue = 0
        mdtTran.Columns.Add("caption", System.Type.GetType("System.String")).DefaultValue = ""
        mdtTran.Columns.Add("intranetlink", System.Type.GetType("System.String")).DefaultValue = ""
        mdtTran.Columns.Add("userunkid", System.Type.GetType("System.Int32")).DefaultValue = 0
        mdtTran.Columns.Add("isvoid", System.Type.GetType("System.Boolean")).DefaultValue = False
        mdtTran.Columns.Add("voiduserunkid", System.Type.GetType("System.Int32")).DefaultValue = -1
        mdtTran.Columns.Add("voiddatetime", System.Type.GetType("System.DateTime")).DefaultValue = Nothing
        mdtTran.Columns.Add("voidreason", System.Type.GetType("System.String")).DefaultValue = ""
    End Sub
#End Region

    ''' <summary>
    ''' Modify By: Pinkal
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>
    Public Sub GetData(Optional ByVal objDoOperation As clsDataOperation = Nothing)
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        If objDoOperation Is Nothing Then
            objDataOperation = New clsDataOperation
        Else
            objDataOperation = objDoOperation
        End If

        Try
            strQ = " SELECT " & _
                      "  intranetlinkunkid " & _
                      ", companyunkid " & _
                      ", caption " & _
                      ", intranetlink " & _
                      ", userunkid " & _
                      ", isvoid " & _
                      ", voiduserunkid " & _
                      ", voiddatetime " & _
                      ", voidreason " & _
                      " FROM hrmsconfiguration..cfdashboard_intranetlink " & _
                      " WHERE intranetlinkunkid = @intranetlinkunkid "

            objDataOperation.ClearParameters()
            objDataOperation.AddParameter("@intranetlinkunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintIntranetlinkunkid.ToString)

            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            For Each dtRow As DataRow In dsList.Tables(0).Rows
                mintIntranetlinkunkid = CInt(dtRow.Item("intranetlinkunkid"))
                mintCompanyunkid = CInt(dtRow.Item("companyunkid"))
                mstrCaption = dtRow.Item("caption").ToString
                mstrIntranetlink = dtRow.Item("intranetlink").ToString
                mintUserunkid = CInt(dtRow.Item("userunkid"))
                mblnIsvoid = CBool(dtRow.Item("isvoid"))
                mintVoiduserunkid = CInt(dtRow.Item("voiduserunkid"))

                If IsDBNull(dtRow.Item("voiddatetime")) = False Then
                    mdtVoiddatetime = dtRow.Item("voiddatetime")
                End If

                mstrVoidreason = dtRow.Item("voidreason").ToString
                Exit For
            Next
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: GetData; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            If objDoOperation Is Nothing Then objDataOperation = Nothing
        End Try
    End Sub

    ''' <summary>
    ''' Modify By: Pinkal
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>
    Public Function GetList(ByVal strTableName As String, ByVal xCompanyId As Integer, Optional ByVal blnOnlyActive As Boolean = True, Optional ByVal objDoOperation As clsDataOperation = Nothing) As DataSet
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        If objDoOperation Is Nothing Then
            objDataOperation = New clsDataOperation
        Else
            objDataOperation = objDoOperation
        End If

        Try
            strQ = "SELECT " & _
                      "  intranetlinkunkid " & _
                      ", companyunkid " & _
                      ", caption " & _
                      ", intranetlink " & _
                      ", userunkid " & _
                      ", isvoid " & _
                      ", voiduserunkid " & _
                      ", voiddatetime " & _
                      ", voidreason " & _
                      " FROM hrmsconfiguration..cfdashboard_intranetlink " & _
                      " WHERE 1 = 1"

            If blnOnlyActive Then
                strQ &= " AND isvoid = 0 "
            End If

            If xCompanyId > 0 Then
                strQ &= " AND companyunkid = @companyunkid "
            End If

            objDataOperation.ClearParameters()
            objDataOperation.AddParameter("@companyunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, xCompanyId)
            dsList = objDataOperation.ExecQuery(strQ, strTableName)

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            mdtTran = dsList.Tables(0).Copy()

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: GetList; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            If objDoOperation Is Nothing Then objDataOperation = Nothing
        End Try
        Return dsList
    End Function

    ''' <summary>
    ''' Modify By: Pinkal
    ''' </summary>
    ''' <returns>Boolean</returns>
    ''' <purpose> INSERT INTO Database Table (cfdashboard_intranetlink) </purpose>
    Public Function InsertUpdateDelete_IntranetLink(ByVal dtOldTable As DataTable) As Boolean
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception
        Dim objDataOperation As clsDataOperation = Nothing
        Dim mstrIntranetIds As String = String.Empty
        Dim xCompanyId As Integer = 0

        objDataOperation = New clsDataOperation
        objDataOperation.BindTransaction()

        Try

            For Each dtRow As DataRow In mdtTran.Rows

                If isExist(CInt(dtRow.Item("companyunkid")), dtRow.Item("caption").ToString(), CInt(dtRow.Item("intranetlinkunkid")), objDataOperation) Then
                    mstrMessage = Language.getMessage(mstrModuleName, 1, "This caption is already defined. Please define new caption.")
                    objDataOperation.ReleaseTransaction(False)
                    Return False
                End If

                If CInt(dtRow.Item("intranetlinkunkid")) <= 0 Then

                    strQ = "INSERT INTO hrmsconfiguration..cfdashboard_intranetlink ( " & _
                              "  companyunkid " & _
                              ", caption " & _
                              ", intranetlink " & _
                              ", userunkid " & _
                              ", isvoid " & _
                              ", voiduserunkid " & _
                              ", voiddatetime " & _
                              ", voidreason" & _
                            ") VALUES (" & _
                              "  @companyunkid " & _
                              ", @caption " & _
                              ", @intranetlink " & _
                              ", @userunkid " & _
                              ", @isvoid " & _
                              ", @voiduserunkid " & _
                              ", @voiddatetime " & _
                              ", @voidreason" & _
                            "); SELECT @@identity"


                    objDataOperation.ClearParameters()
                    objDataOperation.AddParameter("@companyunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, CInt(dtRow.Item("companyunkid")))
                    objDataOperation.AddParameter("@caption", SqlDbType.NVarChar, eZeeDataType.DESC_SIZE, dtRow.Item("caption").ToString())
                    objDataOperation.AddParameter("@intranetlink", SqlDbType.NVarChar, eZeeDataType.DESC_SIZE, dtRow.Item("intranetlink").ToString())
                    objDataOperation.AddParameter("@userunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintUserunkid.ToString)
                    objDataOperation.AddParameter("@isvoid", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsvoid.ToString)
                    objDataOperation.AddParameter("@voiduserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintVoiduserunkid.ToString)

                    If IsDBNull(mdtVoiddatetime) = False AndAlso mdtVoiddatetime <> Nothing Then
                        objDataOperation.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, mdtVoiddatetime.ToString)
                    Else
                        objDataOperation.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, DBNull.Value)
                    End If

                    objDataOperation.AddParameter("@voidreason", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrVoidreason.ToString)


                    dsList = objDataOperation.ExecQuery(strQ, "List")

                    If objDataOperation.ErrorMessage <> "" Then
                        exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                        Throw exForce
                    End If

                    mintIntranetlinkunkid = dsList.Tables(0).Rows(0).Item(0)

                    _Intranetlinkunkid(objDataOperation) = mintIntranetlinkunkid

                    If InsertAuditTrail(enAuditType.ADD, objDataOperation) = False Then
                        exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                        Throw exForce
                    End If

                    'If mstrIntranetIds.Trim.Length > 0 Then
                    '    mstrIntranetIds &= "," & dtRow.Item("intranetlinkunkid").ToString()
                    'Else
                    '    mstrIntranetIds = dtRow.Item("intranetlinkunkid").ToString()
                    'End If

                ElseIf CInt(dtRow.Item("intranetlinkunkid")) > 0 Then

                    strQ = " UPDATE hrmsconfiguration..cfdashboard_intranetlink SET " & _
                              "  companyunkid = @companyunkid" & _
                              ", caption = @caption" & _
                              ", intranetlink = @intranetlink" & _
                              ", userunkid = @userunkid" & _
                              ", isvoid = @isvoid" & _
                              ", voiduserunkid = @voiduserunkid" & _
                              ", voiddatetime = @voiddatetime" & _
                              ", voidreason = @voidreason " & _
                              " WHERE intranetlinkunkid = @intranetlinkunkid "

                    objDataOperation.ClearParameters()
                    objDataOperation.AddParameter("@intranetlinkunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, CInt(dtRow.Item("intranetlinkunkid")).ToString)
                    objDataOperation.AddParameter("@companyunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, CInt(dtRow.Item("companyunkid")).ToString)
                    objDataOperation.AddParameter("@caption", SqlDbType.NVarChar, eZeeDataType.DESC_SIZE, dtRow.Item("caption").ToString())
                    objDataOperation.AddParameter("@intranetlink", SqlDbType.NVarChar, eZeeDataType.DESC_SIZE, dtRow.Item("intranetlink").ToString())
                    objDataOperation.AddParameter("@userunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintUserunkid.ToString)
                    objDataOperation.AddParameter("@isvoid", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsvoid.ToString)
                    objDataOperation.AddParameter("@voiduserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintVoiduserunkid.ToString)

                    If IsDBNull(mdtVoiddatetime) = False AndAlso mdtVoiddatetime <> Nothing Then
                        objDataOperation.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, mdtVoiddatetime.ToString)
                    Else
                        objDataOperation.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, DBNull.Value)
                    End If

                    objDataOperation.AddParameter("@voidreason", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrVoidreason.ToString)


                    Call objDataOperation.ExecNonQuery(strQ)

                    If objDataOperation.ErrorMessage <> "" Then
                        exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                        Throw exForce
                    End If

                    _Intranetlinkunkid(objDataOperation) = CInt(dtRow.Item("intranetlinkunkid"))

                    If InsertAuditTrail(enAuditType.EDIT, objDataOperation) = False Then
                        exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                        Throw exForce
                    End If

                    If mstrIntranetIds.Trim.Length > 0 Then
                        mstrIntranetIds &= "," & dtRow.Item("intranetlinkunkid").ToString()
                    Else
                        mstrIntranetIds = dtRow.Item("intranetlinkunkid").ToString()
                    End If

                End If

                xCompanyId = CInt(dtRow.Item("companyunkid"))

            Next


            If dtOldTable IsNot Nothing AndAlso dtOldTable.Rows.Count > 0 Then
                Dim dRow() As DataRow = Nothing

                If mstrIntranetIds.Trim <> "" Then

                    dRow = dtOldTable.Select("intranetlinkunkid NOT IN (" & mstrIntranetIds & ") ")

                    For Each drRow In dRow

                        strQ = " UPDATE hrmsconfiguration..cfdashboard_intranetlink SET " & _
                                    "  isvoid = @isvoid,voiduserunkid = @voiduserunkid " & _
                                    ", voiddatetime = GETDATE(),voidreason = @voidreason " & _
                                    " WHERE isvoid = 0 AND  intranetlinkunkid = @intranetlinkunkid "

                        objDataOperation.ClearParameters()
                        objDataOperation.AddParameter("@intranetlinkunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, CInt(drRow("intranetlinkunkid")))
                        objDataOperation.AddParameter("@isvoid", SqlDbType.Bit, eZeeDataType.BIT_SIZE, True)
                        objDataOperation.AddParameter("@voiduserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintUserunkid)
                        objDataOperation.AddParameter("@voidreason", SqlDbType.NVarChar, eZeeDataType.DESC_SIZE, "Posting Error")

                        Call objDataOperation.ExecNonQuery(strQ)

                        If objDataOperation.ErrorMessage <> "" Then
                            exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                            Throw exForce
                        End If

                        _Intranetlinkunkid(objDataOperation) = CInt(drRow("intranetlinkunkid"))

                        If InsertAuditTrail(enAuditType.DELETE, objDataOperation) = False Then
                            exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                            Throw exForce
                        End If

                    Next

                End If  'If mstrIntranetIds.Trim <> "" Then

            End If  ' If dtTable IsNot Nothing AndAlso dtTable.Rows.Count > 0 Then

            objDataOperation.ReleaseTransaction(True)

            Return True
        Catch ex As Exception
            objDataOperation.ReleaseTransaction(False)
            Throw New Exception(ex.Message & "; Procedure Name: InsertUpdateDelete_IntranetLink; Module Name: " & mstrModuleName)
            Return False
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
    End Function

    ''' <summary>
    ''' Modify By: Pinkal
    ''' </summary>
    ''' <returns>Boolean</returns>
    ''' <purpose> INSERT INTO Database Table (cfdashboard_intranetlink) </purpose>
    Public Function InsertAuditTrail(ByVal xAuditType As Integer, ByVal objDoOperation As clsDataOperation) As Boolean
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        Try
            objDoOperation.ClearParameters()
            objDoOperation.AddParameter("@intranetlinkunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintIntranetlinkunkid.ToString)
            objDoOperation.AddParameter("@companyunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintCompanyunkid.ToString)
            objDoOperation.AddParameter("@caption", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrCaption.ToString)
            objDoOperation.AddParameter("@intranetlink", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrIntranetlink.ToString)
            objDoOperation.AddParameter("@audittype", SqlDbType.Int, eZeeDataType.INT_SIZE, xAuditType.ToString)
            objDoOperation.AddParameter("@audituserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintAuditUserId)
            objDoOperation.AddParameter("@ip", SqlDbType.NVarChar, eZeeDataType.IP_SIZE, mstrClientIP)
            objDoOperation.AddParameter("@hostname", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrHostName)
            objDoOperation.AddParameter("@form_name", SqlDbType.NVarChar, 500, mstrFormName)
            objDoOperation.AddParameter("@isweb", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsFromWeb)

            strQ = "INSERT INTO hrmsconfiguration..atcfdashboard_intranetlink ( " & _
                      " intranetlinkunkid " & _
                      ", companyunkid " & _
                      ", caption " & _
                      ", intranetlink " & _
                      ", audittype " & _
                      ", audituserunkid " & _
                      ", auditdatetime " & _
                      ", ip " & _
                      ", hostname " & _
                      ", form_name " & _
                      ", isweb " & _
                      " ) VALUES (" & _
                      "  @intranetlinkunkid " & _
                      ", @companyunkid " & _
                      ", @caption " & _
                      ", @intranetlink " & _
                      ", @audittype " & _
                      ", @audituserunkid " & _
                      ", GETDATE() " & _
                      ", @ip " & _
                      ", @hostname " & _
                      ", @form_name " & _
                      ", @isweb " & _
                      " ); SELECT @@identity"

            dsList = objDoOperation.ExecQuery(strQ, "List")

            If objDoOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDoOperation.ErrorNumber & ": " & objDoOperation.ErrorMessage)
                Throw exForce
            End If

            Return True
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: InsertAuditTrail; Module Name: " & mstrModuleName)
            Return False
        Finally
            exForce = Nothing
        End Try
    End Function

    ''' <summary>
    ''' Modify By: Sohail
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>
    Public Function isExist(ByVal xCompanyId As Integer, ByVal strCaption As String, Optional ByVal intUnkid As Integer = -1 _
                                  , Optional ByVal objDoOperation As clsDataOperation = Nothing) As Boolean
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception
        Dim objDataOperation As clsDataOperation = Nothing


        If objDoOperation Is Nothing Then
            objDataOperation = New clsDataOperation
        Else
            objDataOperation = objDoOperation
        End If


        Try
            strQ = " SELECT " & _
                      "  intranetlinkunkid " & _
                      ", companyunkid " & _
                      ", caption " & _
                      ", intranetlink " & _
                      ", userunkid " & _
                      ", isvoid " & _
                      ", voiduserunkid " & _
                      ", voiddatetime " & _
                      ", voidreason " & _
                      " FROM hrmsconfiguration..cfdashboard_intranetlink " & _
                      " WHERE isvoid = 0 AND caption = @caption"

            If xCompanyId > 0 Then
                strQ &= " AND companyunkid = @companyunkid "
            End If

            If intUnkid > 0 Then
                strQ &= " AND intranetlinkunkid <> @intranetlinkunkid"
            End If

            objDataOperation.ClearParameters()
            objDataOperation.AddParameter("@companyunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, xCompanyId)
            objDataOperation.AddParameter("@caption", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, strCaption)
            objDataOperation.AddParameter("@intranetlinkunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intUnkid)

            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            Return dsList.Tables(0).Rows.Count > 0
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: isExist; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            If objDoOperation Is Nothing Then objDataOperation = Nothing
        End Try
    End Function



    '<Language> This Auto Generated Text Please Do Not Modify it.
#Region " Language & UI Settings "
    Public Shared Sub SetMessages()
        Try
            Language.setMessage(mstrModuleName, 1, "This caption is already defined. Please define new caption.")

        Catch Ex As Exception
            DisplayError.Show("-1", Ex.Message, "SetMessages", mstrModuleName)
        End Try
    End Sub
#End Region 'Language & UI Settings
    '</Language>
End Class