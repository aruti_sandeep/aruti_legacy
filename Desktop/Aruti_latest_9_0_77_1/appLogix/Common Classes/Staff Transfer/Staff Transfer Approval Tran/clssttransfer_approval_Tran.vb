﻿'************************************************************************************************************************************
'Class Name : clssttransfer_approval_Tran.vb
'Purpose    :
'Date       :04-Jul-2023
'Written By :Pinkal
'Modified   :
'************************************************************************************************************************************

Imports eZeeCommonLib
''' <summary>
''' Purpose: 
''' Developer: Pinkal
''' </summary>
Public Class clssttransfer_approval_Tran
    Private Shared ReadOnly mstrModuleName As String = "clssttransfer_approval_Tran"
    Dim objDataOperation As clsDataOperation
    Dim mstrMessage As String = ""

#Region " Private variables "
    Private mintTransferapprovalunkid As Integer
    Private mintTransferrequestunkid As Integer
    Private mintEmployeeunkid As Integer
    Private mintStmappingunkid As Integer
    Private mintRoleunkid As Integer
    Private mintLevelunkid As Integer
    Private mintPriority As Integer
    Private mintMapuserunkid As Integer
    Private mdtApprovaldate As Date
    Private mintStationunkid As Integer
    Private mintDeptgroupunkid As Integer
    Private mintDepartmentunkid As Integer
    Private mintSectiongroupunkid As Integer
    Private mintSectionunkid As Integer
    Private mintUnitgroupunkid As Integer
    Private mintUnitunkid As Integer
    Private mintTeamunkid As Integer
    Private mintClassgroupunkid As Integer
    Private mintClassunkid As Integer
    Private mintJobgroupunkid As Integer
    Private mintJobunkid As Integer
    Private mintGradegroupunkid As Integer
    Private mintGradeunkid As Integer
    Private mintGradelevelunkid As Integer
    Private mintReasonunkid As Integer
    Private mstrApprovalremark As String = String.Empty
    Private mintStatusunkid As Integer
    Private mintVisibleunkid As Integer
    Private mintUserunkid As Integer
    Private mblnIsvoid As Boolean
    Private mdtVoiddatetime As Date
    Private mintVoiduserunkid As Integer
    Private mstrVoidreason As String = String.Empty
    Private mstrWebFormName As String = String.Empty
    Private mstrWebClientIP As String = String.Empty
    Private mstrWebHostName As String = String.Empty
    Private mblnIsWeb As Boolean = False
#End Region

#Region " Properties "
    ''' <summary>
    ''' Purpose: Get Message from Class 
    ''' Modify By: Pinkal
    ''' </summary>
    Public ReadOnly Property _Message() As String
        Get
            Return mstrMessage
        End Get
    End Property

    ''' <summary>
    ''' Purpose: Get or Set transferapprovalunkid
    ''' Modify By: Pinkal
    ''' </summary>
    Public Property _Transferapprovalunkid(Optional ByVal objDoOperation As clsDataOperation = Nothing) As Integer
        Get
            Return mintTransferapprovalunkid
        End Get
        Set(ByVal value As Integer)
            mintTransferapprovalunkid = value
            Call GetData(objDoOperation)
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set transferrequestunkid
    ''' Modify By: Pinkal
    ''' </summary>
    Public Property _Transferrequestunkid() As Integer
        Get
            Return mintTransferrequestunkid
        End Get
        Set(ByVal value As Integer)
            mintTransferrequestunkid = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set employeeunkid
    ''' Modify By: Pinkal
    ''' </summary>
    Public Property _Employeeunkid() As Integer
        Get
            Return mintEmployeeunkid
        End Get
        Set(ByVal value As Integer)
            mintEmployeeunkid = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set stmappingunkid
    ''' Modify By: Pinkal
    ''' </summary>
    Public Property _Stmappingunkid() As Integer
        Get
            Return mintStmappingunkid
        End Get
        Set(ByVal value As Integer)
            mintStmappingunkid = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set roleunkid
    ''' Modify By: Pinkal
    ''' </summary>
    Public Property _Roleunkid() As Integer
        Get
            Return mintRoleunkid
        End Get
        Set(ByVal value As Integer)
            mintRoleunkid = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set levelunkid
    ''' Modify By: Pinkal
    ''' </summary>
    Public Property _Levelunkid() As Integer
        Get
            Return mintLevelunkid
        End Get
        Set(ByVal value As Integer)
            mintLevelunkid = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set priority
    ''' Modify By: Pinkal
    ''' </summary>
    Public Property _Priority() As Integer
        Get
            Return mintPriority
        End Get
        Set(ByVal value As Integer)
            mintPriority = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set mapuserunkid
    ''' Modify By: Pinkal
    ''' </summary>
    Public Property _Mapuserunkid() As Integer
        Get
            Return mintMapuserunkid
        End Get
        Set(ByVal value As Integer)
            mintMapuserunkid = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set approvaldate
    ''' Modify By: Pinkal
    ''' </summary>
    Public Property _Approvaldate() As Date
        Get
            Return mdtApprovaldate
        End Get
        Set(ByVal value As Date)
            mdtApprovaldate = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set stationunkid
    ''' Modify By: Pinkal
    ''' </summary>
    Public Property _Stationunkid() As Integer
        Get
            Return mintStationunkid
        End Get
        Set(ByVal value As Integer)
            mintStationunkid = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set deptgroupunkid
    ''' Modify By: Pinkal
    ''' </summary>
    Public Property _Deptgroupunkid() As Integer
        Get
            Return mintDeptgroupunkid
        End Get
        Set(ByVal value As Integer)
            mintDeptgroupunkid = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set departmentunkid
    ''' Modify By: Pinkal
    ''' </summary>
    Public Property _Departmentunkid() As Integer
        Get
            Return mintDepartmentunkid
        End Get
        Set(ByVal value As Integer)
            mintDepartmentunkid = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set sectiongroupunkid
    ''' Modify By: Pinkal
    ''' </summary>
    Public Property _Sectiongroupunkid() As Integer
        Get
            Return mintSectiongroupunkid
        End Get
        Set(ByVal value As Integer)
            mintSectiongroupunkid = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set sectionunkid
    ''' Modify By: Pinkal
    ''' </summary>
    Public Property _Sectionunkid() As Integer
        Get
            Return mintSectionunkid
        End Get
        Set(ByVal value As Integer)
            mintSectionunkid = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set unitgroupunkid
    ''' Modify By: Pinkal
    ''' </summary>
    Public Property _Unitgroupunkid() As Integer
        Get
            Return mintUnitgroupunkid
        End Get
        Set(ByVal value As Integer)
            mintUnitgroupunkid = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set unitunkid
    ''' Modify By: Pinkal
    ''' </summary>
    Public Property _Unitunkid() As Integer
        Get
            Return mintUnitunkid
        End Get
        Set(ByVal value As Integer)
            mintUnitunkid = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set teamunkid
    ''' Modify By: Pinkal
    ''' </summary>
    Public Property _Teamunkid() As Integer
        Get
            Return mintTeamunkid
        End Get
        Set(ByVal value As Integer)
            mintTeamunkid = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set classgroupunkid
    ''' Modify By: Pinkal
    ''' </summary>
    Public Property _Classgroupunkid() As Integer
        Get
            Return mintClassgroupunkid
        End Get
        Set(ByVal value As Integer)
            mintClassgroupunkid = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set classunkid
    ''' Modify By: Pinkal
    ''' </summary>
    Public Property _Classunkid() As Integer
        Get
            Return mintClassunkid
        End Get
        Set(ByVal value As Integer)
            mintClassunkid = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set jobgroupunkid
    ''' Modify By: Pinkal
    ''' </summary>
    Public Property _Jobgroupunkid() As Integer
        Get
            Return mintJobgroupunkid
        End Get
        Set(ByVal value As Integer)
            mintJobgroupunkid = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set jobunkid
    ''' Modify By: Pinkal
    ''' </summary>
    Public Property _Jobunkid() As Integer
        Get
            Return mintJobunkid
        End Get
        Set(ByVal value As Integer)
            mintJobunkid = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set gradegroupunkid
    ''' Modify By: Pinkal
    ''' </summary>
    Public Property _Gradegroupunkid() As Integer
        Get
            Return mintGradegroupunkid
        End Get
        Set(ByVal value As Integer)
            mintGradegroupunkid = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set gradeunkid
    ''' Modify By: Pinkal
    ''' </summary>
    Public Property _Gradeunkid() As Integer
        Get
            Return mintGradeunkid
        End Get
        Set(ByVal value As Integer)
            mintGradeunkid = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set gradelevelunkid
    ''' Modify By: Pinkal
    ''' </summary>
    Public Property _Gradelevelunkid() As Integer
        Get
            Return mintGradelevelunkid
        End Get
        Set(ByVal value As Integer)
            mintGradelevelunkid = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set reasonunkid
    ''' Modify By: Pinkal
    ''' </summary>
    Public Property _Reasonunkid() As Integer
        Get
            Return mintReasonunkid
        End Get
        Set(ByVal value As Integer)
            mintReasonunkid = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set approvalremark
    ''' Modify By: Pinkal
    ''' </summary>
    Public Property _Approvalremark() As String
        Get
            Return mstrApprovalremark
        End Get
        Set(ByVal value As String)
            mstrApprovalremark = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set statusunkid
    ''' Modify By: Pinkal
    ''' </summary>
    Public Property _Statusunkid() As Integer
        Get
            Return mintStatusunkid
        End Get
        Set(ByVal value As Integer)
            mintStatusunkid = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set visibleunkid
    ''' Modify By: Pinkal
    ''' </summary>
    Public Property _Visibleunkid() As Integer
        Get
            Return mintVisibleunkid
        End Get
        Set(ByVal value As Integer)
            mintVisibleunkid = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set userunkid
    ''' Modify By: Pinkal
    ''' </summary>
    Public Property _Userunkid() As Integer
        Get
            Return mintUserunkid
        End Get
        Set(ByVal value As Integer)
            mintUserunkid = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set isvoid
    ''' Modify By: Pinkal
    ''' </summary>
    Public Property _Isvoid() As Boolean
        Get
            Return mblnIsvoid
        End Get
        Set(ByVal value As Boolean)
            mblnIsvoid = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set voiddatetime
    ''' Modify By: Pinkal
    ''' </summary>
    Public Property _Voiddatetime() As Date
        Get
            Return mdtVoiddatetime
        End Get
        Set(ByVal value As Date)
            mdtVoiddatetime = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set voiduserunkid
    ''' Modify By: Pinkal
    ''' </summary>
    Public Property _Voiduserunkid() As Integer
        Get
            Return mintVoiduserunkid
        End Get
        Set(ByVal value As Integer)
            mintVoiduserunkid = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set voidreason
    ''' Modify By: Pinkal
    ''' </summary>
    Public Property _Voidreason() As String
        Get
            Return mstrVoidreason
        End Get
        Set(ByVal value As String)
            mstrVoidreason = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set FormName
    ''' Modify By: Pinkal
    ''' </summary>
    Public Property _FormName() As String
        Get
            Return mstrWebFormName
        End Get
        Set(ByVal value As String)
            mstrWebFormName = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set ClientIP
    ''' Modify By: Pinkal
    ''' </summary>
    Public Property _ClientIP() As String
        Get
            Return mstrWebClientIP
        End Get
        Set(ByVal value As String)
            mstrWebClientIP = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set HostName
    ''' Modify By: Pinkal
    ''' </summary>
    Public Property _HostName() As String
        Get
            Return mstrWebHostName
        End Get
        Set(ByVal value As String)
            mstrWebHostName = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set IsFromWeb
    ''' Modify By: Pinkal
    ''' </summary>
    Public Property _IsWeb() As Boolean
        Get
            Return mblnIsWeb
        End Get
        Set(ByVal value As Boolean)
            mblnIsWeb = value
        End Set
    End Property

#End Region

    ''' <summary>
    ''' Modify By: Pinkal
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>
    Public Sub GetData(Optional ByVal objDoOperation As clsDataOperation = Nothing)
        Dim objDataOperation As clsDataOperation = Nothing
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        If objDoOperation Is Nothing Then
            objDataOperation = New clsDataOperation
        Else
            objDataOperation = objDoOperation
        End If

        Try
            strQ = "SELECT " & _
                      "  transferapprovalunkid " & _
                      ", transferrequestunkid " & _
                      ", employeeunkid " & _
                      ", stmappingunkid " & _
                      ", roleunkid " & _
                      ", levelunkid " & _
                      ", priority " & _
                      ", mapuserunkid " & _
                      ", approvaldate " & _
                      ", stationunkid " & _
                      ", deptgroupunkid " & _
                      ", departmentunkid " & _
                      ", sectiongroupunkid " & _
                      ", sectionunkid " & _
                      ", unitgroupunkid " & _
                      ", unitunkid " & _
                      ", teamunkid " & _
                      ", classgroupunkid " & _
                      ", classunkid " & _
                      ", jobgroupunkid " & _
                      ", jobunkid " & _
                      ", gradegroupunkid " & _
                      ", gradeunkid " & _
                      ", gradelevelunkid " & _
                      ", reasonunkid " & _
                      ", approvalremark " & _
                      ", statusunkid " & _
                      ", visibleunkid " & _
                      ", userunkid " & _
                      ", isvoid " & _
                      ", voiddatetime " & _
                      ", voiduserunkid " & _
                      ", voidreason " & _
                      " FROM sttransfer_approval_tran " & _
                      " WHERE transferapprovalunkid = @transferapprovalunkid "

            objDataOperation.ClearParameters()
            objDataOperation.AddParameter("@transferapprovalunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintTransferapprovalunkid.ToString)

            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            For Each dtRow As DataRow In dsList.Tables(0).Rows
                mintTransferapprovalunkid = CInt(dtRow.Item("transferapprovalunkid"))
                mintTransferrequestunkid = CInt(dtRow.Item("transferrequestunkid"))
                mintEmployeeunkid = CInt(dtRow.Item("employeeunkid"))
                mintStmappingunkid = CInt(dtRow.Item("stmappingunkid"))
                mintRoleunkid = CInt(dtRow.Item("roleunkid"))
                mintLevelunkid = CInt(dtRow.Item("levelunkid"))
                mintPriority = CInt(dtRow.Item("priority"))
                mintMapuserunkid = CInt(dtRow.Item("mapuserunkid"))

                If IsDBNull(dtRow.Item("approvaldate")) = False AndAlso dtRow.Item("approvaldate") <> Nothing Then
                    mdtApprovaldate = dtRow.Item("approvaldate")
                End If

                mintStationunkid = CInt(dtRow.Item("stationunkid"))
                mintDeptgroupunkid = CInt(dtRow.Item("deptgroupunkid"))
                mintDepartmentunkid = CInt(dtRow.Item("departmentunkid"))
                mintSectiongroupunkid = CInt(dtRow.Item("sectiongroupunkid"))
                mintSectionunkid = CInt(dtRow.Item("sectionunkid"))
                mintUnitgroupunkid = CInt(dtRow.Item("unitgroupunkid"))
                mintUnitunkid = CInt(dtRow.Item("unitunkid"))
                mintTeamunkid = CInt(dtRow.Item("teamunkid"))
                mintClassgroupunkid = CInt(dtRow.Item("classgroupunkid"))
                mintClassunkid = CInt(dtRow.Item("classunkid"))
                mintJobgroupunkid = CInt(dtRow.Item("jobgroupunkid"))
                mintJobunkid = CInt(dtRow.Item("jobunkid"))
                mintGradegroupunkid = CInt(dtRow.Item("gradegroupunkid"))
                mintGradeunkid = CInt(dtRow.Item("gradeunkid"))
                mintGradelevelunkid = CInt(dtRow.Item("gradelevelunkid"))
                mintReasonunkid = CInt(dtRow.Item("reasonunkid"))
                mstrApprovalremark = dtRow.Item("approvalremark").ToString
                mintStatusunkid = CInt(dtRow.Item("statusunkid"))
                mintVisibleunkid = CInt(dtRow.Item("visibleunkid"))
                mintUserunkid = CInt(dtRow.Item("userunkid"))
                mblnIsvoid = CBool(dtRow.Item("isvoid"))

                If IsDBNull(dtRow.Item("voiddatetime")) = False AndAlso dtRow.Item("voiddatetime") <> Nothing Then
                    mdtVoiddatetime = dtRow.Item("voiddatetime")
                End If

                mintVoiduserunkid = CInt(dtRow.Item("voiduserunkid"))
                mstrVoidreason = dtRow.Item("voidreason").ToString
                Exit For
            Next
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: GetData; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            If objDoOperation Is Nothing Then objDataOperation = Nothing
        End Try
    End Sub

    ''' <summary>
    ''' Modify By: Pinkal
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>
    Public Function GetTrasferApprovalList(ByVal xDatabaseName As String, _
                                    ByVal xCompanyUnkid As Integer, _
                                    ByVal xPeriodStart As DateTime, _
                                    ByVal xPeriodEnd As DateTime, _
                                    ByVal xUserModeSetting As String, _
                                    ByVal xOnlyApproved As Boolean, _
                                    ByVal xIncludeIn_ActiveEmployee As Boolean, _
                                    ByVal strTableName As String, _
                                    Optional ByVal mstrFilter As String = "", _
                                    Optional ByVal mblnIncludeGroup As Boolean = True, _
                                    Optional ByVal objDataOp As clsDataOperation = Nothing) As DataSet

        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        If objDataOp Is Nothing Then
            objDataOperation = New clsDataOperation
        Else
            objDataOperation = objDataOp
        End If


        Dim StrFinalQurey As String = String.Empty
        Dim StrQCondition As String = String.Empty
        Dim StrQDtFilters As String = String.Empty
        Dim StrQDataJoin As String = String.Empty

        Dim xDateJoinQry, xDateFilterQry, xAdvanceJoinQry As String
        xDateJoinQry = "" : xDateFilterQry = "" : xAdvanceJoinQry = ""
        Call GetDatesFilterString(xDateJoinQry, xDateFilterQry, xPeriodStart, xPeriodEnd, , , xDatabaseName, "Emp")
        Call GetAdvanceFilterQry(xAdvanceJoinQry, xPeriodEnd, xDatabaseName, "Emp")

        Try

            If mblnIncludeGroup Then


                strQ = " SELECT " & _
                          "  -1 AS transferapprovalunkid " & _
                          ", sta.transferrequestunkid " & _
                          ", sta.employeeunkid " & _
                          ", -1 AS stmappingunkid " & _
                          ", -1 AS roleunkid " & _
                          ", -1 AS levelunkid " & _
                          ", -1 AS priority " & _
                          ", -1 AS mapuserunkid " & _
                          ", NULL AS approvaldate " & _
                          ", -1 AS stationunkid " & _
                          ", -1 AS deptgroupunkid " & _
                          ", -1 AS departmentunkid " & _
                          ", -1 AS sectiongroupunkid " & _
                          ", -1 AS sectionunkid " & _
                          ", -1 AS unitgroupunkid " & _
                          ", -1 AS unitunkid " & _
                          ", -1 AS teamunkid " & _
                          ", -1 AS classgroupunkid " & _
                          ", -1 AS classunkid " & _
                          ", -1 AS jobgroupunkid " & _
                          ", -1 AS jobunkid " & _
                          ", -1 AS gradegroupunkid " & _
                          ", -1 AS gradeunkid " & _
                          ", -1 AS gradelevelunkid " & _
                          ", -1 AS reasonunkid " & _
                          ", '' AS approvalremark " & _
                          ", st.statusunkid as appstatusunkid  " & _
                          ", -1 AS statusunkid " & _
                          ", '' AS Status " & _
                          ", 0 AS visibleunkid " & _
                          ", -1 AS userunkid " & _
                          ", 0 AS isvoid " & _
                          ", NULL AS voiddatetime " & _
                          ", -1 AS voiduserunkid " & _
                          ", '' AS voidreason " & _
                          ", st.requestdate " & _
                          ", CONVERT(CHAR(8),st.requestdate,112) AS rdate " & _
                          ", -1 AS roleunkid " & _
                          ", '' AS Role " & _
                          ", -1 AS levelunkid " & _
                          ",'' AS Level " & _
                           ", ISNULL(Emp.employeecode, '') + ' - ' + ISNULL(Emp.firstname, '') + ' ' + ISNULL(Emp.surname, '') AS Employee " & _
                          ",'' AS EmployeeName " & _
                          ",'' AS CUser " & _
                          ",'' AS loginuser " & _
                          ",''  AS UserName " & _
                          ",'' AS UserEmail " & _
                          ",'' AS station " & _
                          ",'' AS deptgrp " & _
                          ",'' AS department " & _
                          ",'' AS sectiongrp " & _
                          ",' 'AS section " & _
                          ",'' AS unitgrp " & _
                          ",'' AS unit " & _
                          ",'' AS team " & _
                          ",'' AS classgrp " & _
                          ",'' AS classes " & _
                          ",'' AS jobgrp " & _
                          ",'' AS job " & _
                          ",'' AS Reason " & _
                          ", 1 AS isgrp " & _
                          " FROM sttransfer_approval_tran sta " & _
                          " LEFT JOIN sttransfer_request_tran st ON st.transferrequestunkid = sta.transferrequestunkid AND st.isvoid = 0 " & _
                          " LEFT JOIN hremployee_master AS Emp ON Emp.employeeunkid = st.employeeunkid " & _
                          " LEFT JOIN hrmsConfiguration..cfrole_master r ON r.roleunkid = sta.roleunkid " & _
                          " LEFT JOIN stapproverlevel_master lvl ON lvl.stlevelunkid = sta.levelunkid " & _
                          " LEFT JOIN hrmsConfiguration..cfuser_master u ON u.userunkid = sta.mapuserunkid AND u.ismanager = 1 "


                If xDateJoinQry.Trim.Length > 0 Then
                    strQ &= xDateJoinQry '
                End If

                If xAdvanceJoinQry.Trim.Length > 0 Then
                    strQ &= xAdvanceJoinQry
                End If

                strQ &= " WHERE sta.isvoid = 0  "

                If xIncludeIn_ActiveEmployee = False Then
                    If xDateFilterQry.Trim.Length > 0 Then
                        strQ &= xDateFilterQry
                    End If
                End If

                If mstrFilter.Trim.Length > 0 Then
                    strQ &= " AND " & mstrFilter
                End If

                'If strAdvanceFilter.Trim.Length > 0 Then
                '    strQ &= " AND " & strAdvanceFilter
                'End If

                strQ &= " UNION "

            End If

            strQ &= " SELECT " & _
                      "  sta.transferapprovalunkid " & _
                      ", sta.transferrequestunkid " & _
                      ", sta.employeeunkid " & _
                      ", sta.stmappingunkid " & _
                      ", sta.roleunkid " & _
                      ", sta.levelunkid " & _
                      ", sta.priority " & _
                      ", sta.mapuserunkid " & _
                      ", sta.approvaldate " & _
                      ", sta.stationunkid " & _
                      ", sta.deptgroupunkid " & _
                      ", sta.departmentunkid " & _
                      ", sta.sectiongroupunkid " & _
                      ", sta.sectionunkid " & _
                      ", sta.unitgroupunkid " & _
                      ", sta.unitunkid " & _
                      ", sta.teamunkid " & _
                      ", sta.classgroupunkid " & _
                      ", sta.classunkid " & _
                      ", sta.jobgroupunkid " & _
                      ", sta.jobunkid " & _
                      ", sta.gradegroupunkid " & _
                      ", sta.gradeunkid " & _
                      ", sta.gradelevelunkid " & _
                      ", sta.reasonunkid " & _
                      ", sta.approvalremark " & _
                      ", st.statusunkid as appstatusunkid  " & _
                      ", sta.statusunkid " & _
                      ",CASE WHEN sta.statusunkid =1 THEN @Pending " & _
                      "          WHEN sta.statusunkid =2 THEN @Approved " & _
                      "          WHEN sta.statusunkid =3 THEN @PushedBack " & _
                      "  END AS Status " & _
                      ", sta.visibleunkid " & _
                      ", sta.userunkid " & _
                      ", sta.isvoid " & _
                      ", sta.voiddatetime " & _
                      ", sta.voiduserunkid " & _
                      ", sta.voidreason " & _
                      ", st.requestdate " & _
                      ", CONVERT(CHAR(8),st.requestdate,112) AS rdate " & _
                      ", sta.roleunkid " & _
                      ", ISNULL(r.name, '') AS Role " & _
                      ", sta.levelunkid " & _
                      ", ISNULL(lvl.stlevelname, '') AS Level " & _
                      ", ISNULL(Emp.employeecode, '') + ' - ' + ISNULL(Emp.firstname, '') + ' ' + ISNULL(Emp.surname, '') AS Employee " & _
                      ", ISNULL(Emp.firstname, '') + ' ' + ISNULL(Emp.surname, '') AS EmployeeName " & _
                      ", ISNULL(u.username,'')  + ' - ' + ISNULL(lvl.stlevelname, '')  AS CUser " & _
                      ", ISNULL(u.username,'') AS loginuser " & _
                      ", ISNULL(u.firstname,'') + ' ' + ISNULL(u.lastname,'')  AS UserName " & _
                      ", ISNULL(u.email,'')  AS UserEmail " & _
                      ", ISNULL(hrstation_master.name,'') AS station " & _
                      ", ISNULL(hrdepartment_group_master.name,'') AS deptgrp " & _
                      ", ISNULL(hrdepartment_master.name,'') AS department " & _
                      ", ISNULL(hrsectiongroup_master.name,'') AS sectiongrp " & _
                      ", ISNULL(hrsection_master.name,'') AS section " & _
                      ", ISNULL(hrunitgroup_master.name,'') AS unitgrp " & _
                      ", ISNULL(hrunit_master.name,'') AS unit " & _
                      ", ISNULL(hrteam_master.name,'') AS team " & _
                      ", ISNULL(hrclassgroup_master.name,'') AS classgrp " & _
                      ", ISNULL(hrclasses_master.name,'') AS classes " & _
                      ", ISNULL(hrjobgroup_master.name,'') AS jobgrp " & _
                      ", ISNULL(hrjob_master.job_name,'') AS job " & _
                      ", ISNULL(cfcommon_master.name,'') AS Reason " & _
                      ",0 AS isgrp " & _
                      " FROM sttransfer_approval_tran sta " & _
                      " LEFT JOIN sttransfer_request_tran st ON st.transferrequestunkid = sta.transferrequestunkid AND st.isvoid = 0 " & _
                      " LEFT JOIN hremployee_master AS Emp ON Emp.employeeunkid = st.employeeunkid " & _
                      " LEFT JOIN hrmsConfiguration..cfrole_master r ON r.roleunkid = sta.roleunkid " & _
                      " LEFT JOIN stapproverlevel_master lvl ON lvl.stlevelunkid = sta.levelunkid " & _
                      " LEFT JOIN cfcommon_master ON cfcommon_master.masterunkid = st.reasonunkid AND cfcommon_master.mastertype = @mastertype  " & _
                      " LEFT JOIN hrmsConfiguration..cfuser_master u ON u.userunkid = sta.mapuserunkid AND u.ismanager = 1 " & _
                      " LEFT JOIN " & _
                      "( " & _
                      "   SELECT " & _
                      "        stationunkid " & _
                      "       ,deptgroupunkid " & _
                      "       ,departmentunkid " & _
                       "       ,sectiongroupunkid " & _
                      "       ,sectionunkid " & _
                      "       ,unitgroupunkid " & _
                      "       ,unitunkid " & _
                      "       ,teamunkid " & _
                      "       ,classgroupunkid " & _
                      "       ,classunkid " & _
                      "       ,employeeunkid " & _
                      "       ,ROW_NUMBER()OVER(PARTITION BY employeeunkid ORDER BY effectivedate DESC) AS rno " & _
                      "   FROM hremployee_transfer_tran " & _
                      "   WHERE isvoid = 0 AND CONVERT(CHAR(8),effectivedate,112) <= '" & eZeeDate.convertDate(xPeriodEnd) & "' " & _
                      ") AS ETT ON ETT.employeeunkid = Emp.employeeunkid AND ETT.rno = 1 " & _
                      " LEFT JOIN " & _
                      "( " & _
                      "   SELECT " & _
                      "        jobgroupunkid " & _
                      "       ,jobunkid " & _
                      "       ,employeeunkid " & _
                      "       ,ROW_NUMBER()OVER(PARTITION BY employeeunkid ORDER BY effectivedate DESC) AS rno " & _
                      "   FROM hremployee_categorization_tran " & _
                      "   WHERE isvoid = 0 AND CONVERT(CHAR(8),effectivedate,112) <= '" & eZeeDate.convertDate(xPeriodEnd) & "' " & _
                      ") AS ECT ON ECT.employeeunkid = Emp.employeeunkid AND ECT.rno = 1 " & _
                      " LEFT JOIN " & _
                      "( " & _
                      "   SELECT " & _
                      "        gradegroupunkid " & _
                      "       ,gradeunkid " & _
                      "       ,gradelevelunkid " & _
                      "       ,employeeunkid " & _
                      "       ,ROW_NUMBER()OVER(PARTITION BY employeeunkid ORDER BY incrementdate DESC,salaryincrementtranunkid DESC) AS rno " & _
                      "   FROM prsalaryincrement_tran " & _
                      "   WHERE isvoid = 0 AND prsalaryincrement_tran.isapproved = 1 AND CONVERT(CHAR(8),incrementdate,112) <= '" & eZeeDate.convertDate(xPeriodEnd) & "' " & _
                      " ) AS GRD ON GRD.employeeunkid = Emp.employeeunkid AND GRD.rno = 1 " & _
                      " LEFT JOIN hrstation_master ON sta.stationunkid = hrstation_master.stationunkid AND hrstation_master.isactive = 1 " & _
                      " LEFT JOIN hrdepartment_group_master ON sta.deptgroupunkid = hrdepartment_group_master.deptgroupunkid AND hrdepartment_group_master.isactive = 1 " & _
                      " LEFT JOIN hrdepartment_master ON sta.departmentunkid = hrdepartment_master.departmentunkid AND hrdepartment_master.isactive = 1 " & _
                      " LEFT JOIN hrsectiongroup_master ON sta.sectiongroupunkid = hrsectiongroup_master.sectiongroupunkid AND hrsectiongroup_master.isactive = 1 " & _
                      " LEFT JOIN hrsection_master ON sta.sectionunkid = hrsection_master.sectionunkid AND hrsection_master.isactive = 1 " & _
                      " LEFT JOIN hrunitgroup_master ON sta.unitgroupunkid = hrunitgroup_master.unitgroupunkid AND hrunitgroup_master.isactive = 1 " & _
                      " LEFT JOIN hrunit_master ON sta.unitunkid = hrunit_master.unitunkid AND hrunit_master.isactive = 1 " & _
                      " LEFT JOIN hrteam_master ON sta.teamunkid = hrteam_master.teamunkid AND hrteam_master.isactive = 1 " & _
                      " LEFT JOIN hrclassgroup_master ON sta.classgroupunkid = hrclassgroup_master.classgroupunkid AND hrclassgroup_master.isactive = 1 " & _
                      " LEFT JOIN hrclasses_master ON sta.classunkid = hrclasses_master.classesunkid AND hrclasses_master.isactive = 1 " & _
                      " LEFT JOIN hrjob_master ON sta.jobunkid = hrjob_master.jobunkid AND hrjob_master.isactive = 1 " & _
                      " LEFT JOIN hrjobgroup_master ON sta.jobgroupunkid = hrjobgroup_master.jobgroupunkid AND hrjobgroup_master.isactive = 1 "

            If xDateJoinQry.Trim.Length > 0 Then
                strQ &= xDateJoinQry '
            End If

            If xAdvanceJoinQry.Trim.Length > 0 Then
                strQ &= xAdvanceJoinQry
            End If

            strQ &= " WHERE sta.isvoid = 0 AND sta.stmappingunkid > 0 AND sta.roleunkid > 0 AND sta.levelunkid > 0 "

            If xIncludeIn_ActiveEmployee = False Then
                If xDateFilterQry.Trim.Length > 0 Then
                    strQ &= xDateFilterQry
                End If
            End If


            'If strAdvanceFilter.Trim.Length > 0 Then
            '    strQ &= " AND " & strAdvanceFilter
            'End If

            If mstrFilter.Trim.Length > 0 Then
                strQ &= " AND " & mstrFilter
            End If

            strQ &= " ORDER BY CONVERT(CHAR(8),st.requestdate,112) DESC,transferrequestunkid DESC,priority,isgrp DESC "

            objDataOperation.ClearParameters()
            objDataOperation.AddParameter("@mastertype", SqlDbType.Int, eZeeDataType.INT_SIZE, CInt(clsCommon_Master.enCommonMaster.STAFF_TRANSFER))
            objDataOperation.AddParameter("@Pending", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage("clstransfer_request_tran", 2, "Pending"))
            objDataOperation.AddParameter("@Approved", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage("clstransfer_request_tran", 3, "Approved"))
            objDataOperation.AddParameter("@PushedBack", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage("clstransfer_request_tran", 4, "Pushed Back"))
            dsList = objDataOperation.ExecQuery(strQ, strTableName)

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: GetTrasferApprovalList; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
        Return dsList
    End Function

    ''' <summary>
    ''' Modify By: Pinkal
    ''' </summary>
    ''' <returns>Boolean</returns>
    ''' <purpose> INSERT INTO Database Table (sttransfer_approval_tran) </purpose>
    Public Function Insert(Optional ByVal objDoOperation As clsDataOperation = Nothing) As Boolean
        'If isExist(mstrName) Then
        '    mstrMessage = "<Message>"
        '    Return False
        'End If
        Dim objDataOperation As clsDataOperation = Nothing
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception


        If objDoOperation Is Nothing Then
            objDataOperation = New clsDataOperation
            objDataOperation.BindTransaction()
        Else
            objDataOperation = objDoOperation
        End If

        Try
            objDataOperation.ClearParameters()
            objDataOperation.AddParameter("@transferrequestunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintTransferrequestunkid.ToString)
            objDataOperation.AddParameter("@employeeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintEmployeeunkid.ToString)
            objDataOperation.AddParameter("@stmappingunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintStmappingunkid.ToString)
            objDataOperation.AddParameter("@roleunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintRoleunkid.ToString)
            objDataOperation.AddParameter("@levelunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintLevelunkid.ToString)
            objDataOperation.AddParameter("@priority", SqlDbType.Int, eZeeDataType.INT_SIZE, mintPriority.ToString)
            objDataOperation.AddParameter("@mapuserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintMapuserunkid.ToString)

            If IsDBNull(mdtApprovaldate) = False AndAlso mdtApprovaldate <> Nothing Then
                objDataOperation.AddParameter("@approvaldate", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, mdtApprovaldate.ToString)
            Else
                objDataOperation.AddParameter("@approvaldate", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, DBNull.Value)
            End If

            objDataOperation.AddParameter("@stationunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintStationunkid.ToString)
            objDataOperation.AddParameter("@deptgroupunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintDeptgroupunkid.ToString)
            objDataOperation.AddParameter("@departmentunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintDepartmentunkid.ToString)
            objDataOperation.AddParameter("@sectiongroupunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintSectiongroupunkid.ToString)
            objDataOperation.AddParameter("@sectionunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintSectionunkid.ToString)
            objDataOperation.AddParameter("@unitgroupunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintUnitgroupunkid.ToString)
            objDataOperation.AddParameter("@unitunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintUnitunkid.ToString)
            objDataOperation.AddParameter("@teamunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintTeamunkid.ToString)
            objDataOperation.AddParameter("@classgroupunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintClassgroupunkid.ToString)
            objDataOperation.AddParameter("@classunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintClassunkid.ToString)
            objDataOperation.AddParameter("@jobgroupunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintJobgroupunkid.ToString)
            objDataOperation.AddParameter("@jobunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintJobunkid.ToString)
            objDataOperation.AddParameter("@gradegroupunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintGradegroupunkid.ToString)
            objDataOperation.AddParameter("@gradeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintGradeunkid.ToString)
            objDataOperation.AddParameter("@gradelevelunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintGradelevelunkid.ToString)
            objDataOperation.AddParameter("@reasonunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintReasonunkid.ToString)
            objDataOperation.AddParameter("@approvalremark", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrApprovalremark.ToString)
            objDataOperation.AddParameter("@statusunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintStatusunkid.ToString)
            objDataOperation.AddParameter("@visibleunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintVisibleunkid.ToString)
            objDataOperation.AddParameter("@userunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintUserunkid.ToString)
            objDataOperation.AddParameter("@isvoid", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsvoid.ToString)

            If IsDBNull(mdtVoiddatetime) = False AndAlso mdtVoiddatetime <> Nothing Then
                objDataOperation.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, mdtVoiddatetime.ToString)
            Else
                objDataOperation.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, DBNull.Value)
            End If

            objDataOperation.AddParameter("@voiduserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintVoiduserunkid.ToString)
            objDataOperation.AddParameter("@voidreason", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrVoidreason.ToString)

            strQ = " INSERT INTO sttransfer_approval_tran ( " & _
                      "  transferrequestunkid " & _
                      ", employeeunkid " & _
                      ", stmappingunkid " & _
                      ", roleunkid " & _
                      ", levelunkid " & _
                      ", priority " & _
                      ", mapuserunkid " & _
                      ", approvaldate " & _
                      ", stationunkid " & _
                      ", deptgroupunkid " & _
                      ", departmentunkid " & _
                      ", sectiongroupunkid " & _
                      ", sectionunkid " & _
                      ", unitgroupunkid " & _
                      ", unitunkid " & _
                      ", teamunkid " & _
                      ", classgroupunkid " & _
                      ", classunkid " & _
                      ", jobgroupunkid " & _
                      ", jobunkid " & _
                      ", gradegroupunkid " & _
                      ", gradeunkid " & _
                      ", gradelevelunkid " & _
                      ", reasonunkid " & _
                      ", approvalremark " & _
                      ", statusunkid " & _
                      ", visibleunkid " & _
                      ", userunkid " & _
                      ", isvoid " & _
                      ", voiddatetime " & _
                      ", voiduserunkid " & _
                      ", voidreason" & _
                      " ) VALUES (" & _
                      "  @transferrequestunkid " & _
                      ", @employeeunkid " & _
                      ", @stmappingunkid " & _
                      ", @roleunkid " & _
                      ", @levelunkid " & _
                      ", @priority " & _
                      ", @mapuserunkid " & _
                      ", @approvaldate " & _
                      ", @stationunkid " & _
                      ", @deptgroupunkid " & _
                      ", @departmentunkid " & _
                      ", @sectiongroupunkid " & _
                      ", @sectionunkid " & _
                      ", @unitgroupunkid " & _
                      ", @unitunkid " & _
                      ", @teamunkid " & _
                      ", @classgroupunkid " & _
                      ", @classunkid " & _
                      ", @jobgroupunkid " & _
                      ", @jobunkid " & _
                      ", @gradegroupunkid " & _
                      ", @gradeunkid " & _
                      ", @gradelevelunkid " & _
                      ", @reasonunkid " & _
                      ", @approvalremark " & _
                      ", @statusunkid " & _
                      ", @visibleunkid " & _
                      ", @userunkid " & _
                      ", @isvoid " & _
                      ", @voiddatetime " & _
                      ", @voiduserunkid " & _
                      ", @voidreason" & _
                      " ); SELECT @@identity"

            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            mintTransferapprovalunkid = dsList.Tables(0).Rows(0).Item(0)


            If InsertAuditTrailStaffTransferApproval(objDataOperation, enAuditType.ADD) = False Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            If objDoOperation Is Nothing Then objDataOperation.ReleaseTransaction(True)

            Return True
        Catch ex As Exception
            If objDoOperation Is Nothing Then objDataOperation.ReleaseTransaction(False)
            Throw New Exception(ex.Message & "; Procedure Name: Insert; Module Name: " & mstrModuleName)
            Return False
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            If objDoOperation Is Nothing Then objDataOperation = Nothing
        End Try
    End Function

    ''' <summary>
    ''' Modify By: Pinkal
    ''' </summary>
    ''' <returns>Boolean</returns>
    ''' <purpose> Update Database Table (sttransfer_approval_tran) </purpose>
    Public Function Update(ByVal xCompanyUnkid As Integer, ByVal xDatabaseName As String, ByVal xYearUnkid As Integer, ByVal mdtRequestDate As Date _
                                    , Optional ByVal objDoOperation As clsDataOperation = Nothing) As Boolean
        'If isExist(mstrName, mintTransferapprovalunkid) Then
        '    mstrMessage = "<Message>"
        '    Return False
        'End If

        Dim objDataOperation As clsDataOperation = Nothing
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception
        Dim mblnIncludeInactiveEmployee As Boolean = False

        Dim objConfig As New clsConfigOptions
        Dim mstrUserAccessModeSetting As String = objConfig.GetKeyValue(xCompanyUnkid, "UserAccessModeSetting", Nothing)
        Dim mstrIncludeInactiveEmployee As String = objConfig.GetKeyValue(xCompanyUnkid, "IncludeInactiveEmployee", Nothing)
        If mstrIncludeInactiveEmployee.Trim.Length > 0 Then mblnIncludeInactiveEmployee = CBool(mstrIncludeInactiveEmployee)
        objConfig = Nothing

        Dim xRoleId As Integer = mintRoleunkid
        Dim xLevelId As Integer = mintLevelunkid
        Dim xPriority As Integer = mintPriority
        Dim xStatusId As Integer = mintStatusunkid
        Dim xUserId As Integer = mintUserunkid

        If objDoOperation Is Nothing Then
            objDataOperation = New clsDataOperation
            objDataOperation.BindTransaction()
        Else
            objDataOperation = objDoOperation
        End If

        Try
            objDataOperation.ClearParameters()
            objDataOperation.AddParameter("@transferapprovalunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintTransferapprovalunkid.ToString)
            objDataOperation.AddParameter("@transferrequestunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintTransferrequestunkid.ToString)
            objDataOperation.AddParameter("@employeeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintEmployeeunkid.ToString)
            objDataOperation.AddParameter("@stmappingunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintStmappingunkid.ToString)
            objDataOperation.AddParameter("@roleunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintRoleunkid.ToString)
            objDataOperation.AddParameter("@levelunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintLevelunkid.ToString)
            objDataOperation.AddParameter("@priority", SqlDbType.Int, eZeeDataType.INT_SIZE, mintPriority.ToString)
            objDataOperation.AddParameter("@mapuserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintMapuserunkid.ToString)
            objDataOperation.AddParameter("@approvaldate", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, mdtApprovaldate.ToString)
            objDataOperation.AddParameter("@stationunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintStationunkid.ToString)
            objDataOperation.AddParameter("@deptgroupunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintDeptgroupunkid.ToString)
            objDataOperation.AddParameter("@departmentunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintDepartmentunkid.ToString)
            objDataOperation.AddParameter("@sectiongroupunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintSectiongroupunkid.ToString)
            objDataOperation.AddParameter("@sectionunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintSectionunkid.ToString)
            objDataOperation.AddParameter("@unitgroupunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintUnitgroupunkid.ToString)
            objDataOperation.AddParameter("@unitunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintUnitunkid.ToString)
            objDataOperation.AddParameter("@teamunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintTeamunkid.ToString)
            objDataOperation.AddParameter("@classgroupunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintClassgroupunkid.ToString)
            objDataOperation.AddParameter("@classunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintClassunkid.ToString)
            objDataOperation.AddParameter("@jobgroupunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintJobgroupunkid.ToString)
            objDataOperation.AddParameter("@jobunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintJobunkid.ToString)
            objDataOperation.AddParameter("@gradegroupunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintGradegroupunkid.ToString)
            objDataOperation.AddParameter("@gradeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintGradeunkid.ToString)
            objDataOperation.AddParameter("@gradelevelunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintGradelevelunkid.ToString)
            objDataOperation.AddParameter("@reasonunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintReasonunkid.ToString)
            objDataOperation.AddParameter("@approvalremark", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrApprovalremark.ToString)
            objDataOperation.AddParameter("@statusunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintStatusunkid.ToString)
            objDataOperation.AddParameter("@visibleunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintVisibleunkid.ToString)
            objDataOperation.AddParameter("@userunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintUserunkid.ToString)
            objDataOperation.AddParameter("@isvoid", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsvoid.ToString)

            If IsDBNull(mdtVoiddatetime) = False AndAlso mdtVoiddatetime <> Nothing Then
                objDataOperation.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, mdtVoiddatetime.ToString)
            Else
                objDataOperation.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, DBNull.Value)
            End If

            objDataOperation.AddParameter("@voiduserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintVoiduserunkid.ToString)
            objDataOperation.AddParameter("@voidreason", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrVoidreason.ToString)

            strQ = " UPDATE sttransfer_approval_tran SET " & _
                      "  transferrequestunkid = @transferrequestunkid" & _
                      ", employeeunkid = @employeeunkid" & _
                      ", stmappingunkid = @stmappingunkid" & _
                      ", roleunkid = @roleunkid" & _
                      ", levelunkid = @levelunkid" & _
                      ", priority = @priority" & _
                      ", mapuserunkid = @mapuserunkid" & _
                      ", approvaldate = @approvaldate" & _
                      ", stationunkid = @stationunkid" & _
                      ", deptgroupunkid = @deptgroupunkid" & _
                      ", departmentunkid = @departmentunkid" & _
                      ", sectiongroupunkid = @sectiongroupunkid" & _
                      ", sectionunkid = @sectionunkid" & _
                      ", unitgroupunkid = @unitgroupunkid" & _
                      ", unitunkid = @unitunkid" & _
                      ", teamunkid = @teamunkid" & _
                      ", classgroupunkid = @classgroupunkid" & _
                      ", classunkid = @classunkid" & _
                      ", jobgroupunkid = @jobgroupunkid" & _
                      ", jobunkid = @jobunkid" & _
                      ", gradegroupunkid = @gradegroupunkid" & _
                      ", gradeunkid = @gradeunkid" & _
                      ", gradelevelunkid = @gradelevelunkid" & _
                      ", reasonunkid = @reasonunkid" & _
                      ", approvalremark = @approvalremark" & _
                      ", statusunkid = @statusunkid" & _
                      ", visibleunkid = @visibleunkid" & _
                      ", userunkid = @userunkid" & _
                      ", isvoid = @isvoid" & _
                      ", voiddatetime = @voiddatetime" & _
                      ", voiduserunkid = @voiduserunkid" & _
                      ", voidreason = @voidreason " & _
                      " WHERE transferapprovalunkid = @transferapprovalunkid "

            Call objDataOperation.ExecNonQuery(strQ)

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            If InsertAuditTrailStaffTransferApproval(objDataOperation, enAuditType.EDIT) = False Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            strQ = " SELECT transferapprovalunkid,transferrequestunkid,stmappingunkid,roleunkid,levelunkid,priority,visibleunkid FROM sttransfer_approval_tran WHERE isvoid = 0 AND transferrequestunkid = @transferrequestunkid "
            objDataOperation.ClearParameters()
            objDataOperation.AddParameter("@transferrequestunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintTransferrequestunkid.ToString)
            Dim dsApprover As DataSet = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            If mintStatusunkid <> clstransfer_request_tran.enTransferRequestStatus.PushedBack Then


                Dim dtVisibility As DataTable = Nothing
                If dsApprover IsNot Nothing AndAlso dsApprover.Tables(0).Rows.Count > 0 Then

                    strQ = " UPDATE sttransfer_approval_tran set " & _
                              " visibleunkid = " & mintStatusunkid & _
                              " WHERE  transferapprovalunkid = @transferapprovalunkid AND transferrequestunkid = @transferrequestunkid and employeeunkid = @employeeunkid " & _
                              " AND stmappingunkid = @stmappingunkid AND roleunkid = @roleunkid AND levelunkid = @levelunkid  AND isvoid = 0   "


                    dtVisibility = New DataView(dsApprover.Tables(0), "priority = " & mintPriority, "", DataViewRowState.CurrentRows).ToTable


                    For i As Integer = 0 To dtVisibility.Rows.Count - 1
                        objDataOperation.ClearParameters()
                        objDataOperation.AddParameter("@transferapprovalunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, dtVisibility.Rows(i)("transferapprovalunkid").ToString)
                        objDataOperation.AddParameter("@transferrequestunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, dtVisibility.Rows(i)("transferrequestunkid").ToString)
                        objDataOperation.AddParameter("@employeeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintEmployeeunkid)
                        objDataOperation.AddParameter("@stmappingunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, dtVisibility.Rows(i)("stmappingunkid").ToString)
                        objDataOperation.AddParameter("@roleunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, dtVisibility.Rows(i)("roleunkid").ToString)
                        objDataOperation.AddParameter("@levelunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, dtVisibility.Rows(i)("levelunkid").ToString)

                        objDataOperation.ExecNonQuery(strQ)

                        If objDataOperation.ErrorMessage <> "" Then
                            exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                            Throw exForce
                        End If
                    Next

                End If

                Dim intMinPriority As Integer = dsApprover.Tables(0).AsEnumerable().Where(Function(x) x.Field(Of Integer)("priority") > mintPriority).Select(Function(x) x.Field(Of Integer)("priority")).DefaultIfEmpty().Min()

                dtVisibility = New DataView(dsApprover.Tables(0), "priority = " & intMinPriority & " AND priority <> -1", "", DataViewRowState.CurrentRows).ToTable

                If dtVisibility IsNot Nothing AndAlso dtVisibility.Rows.Count > 0 Then

                    strQ = " UPDATE sttransfer_approval_tran set " & _
                              " visibleunkid = @visibleunkid " & _
                              " WHERE  transferapprovalunkid = @transferapprovalunkid AND transferrequestunkid = @transferrequestunkid  " & _
                              " AND employeeunkid = @employeeunkid AND stmappingunkid = @stmappingunkid AND roleunkid = @roleunkid AND levelunkid = @levelunkid AND isvoid = 0   "

                    For i As Integer = 0 To dtVisibility.Rows.Count - 1
                        objDataOperation.ClearParameters()
                        objDataOperation.AddParameter("@visibleunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, clstransfer_request_tran.enTransferRequestStatus.Pending)
                        objDataOperation.AddParameter("@transferapprovalunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, dtVisibility.Rows(i)("transferapprovalunkid").ToString)
                        objDataOperation.AddParameter("@transferrequestunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, dtVisibility.Rows(i)("transferrequestunkid").ToString)
                        objDataOperation.AddParameter("@employeeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintEmployeeunkid)
                        objDataOperation.AddParameter("@stmappingunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, dtVisibility.Rows(i)("stmappingunkid").ToString)
                        objDataOperation.AddParameter("@roleunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, dtVisibility.Rows(i)("roleunkid").ToString)
                        objDataOperation.AddParameter("@levelunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, dtVisibility.Rows(i)("levelunkid").ToString)

                        objDataOperation.ExecNonQuery(strQ)

                        If objDataOperation.ErrorMessage <> "" Then
                            exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                            Throw exForce
                        End If

                    Next

                End If

            ElseIf mintStatusunkid = clstransfer_request_tran.enTransferRequestStatus.PushedBack Then

                If dsApprover IsNot Nothing AndAlso dsApprover.Tables(0).Rows.Count > 0 Then
                    Dim xVoidUserId As Integer = mintVoiduserunkid
                    Dim xVoidReason As String = mstrVoidreason
                    Dim xVoidDatetime As DateTime = mdtVoiddatetime
                    Dim xPushedBackPriority As Integer = -1
                    Dim dtPushedBack As DataTable = Nothing

                    'Dim objLevelRoleMapping As New clsstapproverlevel_role_mapping
                    'Dim dtTable As DataTable = objLevelRoleMapping.GetNextApprovers(xDatabaseName, xCompanyUnkid, xYearUnkid, mstrUserAccessModeSetting, enUserPriviledge.AllowToApproveStaffTransferApproval, mdtRequestDate, mintUserunkid, mintEmployeeunkid, False, objDataOperation, "")
                    'objLevelRoleMapping = Nothing
                    Dim dsAppovalList As DataSet = GetTrasferApprovalList(xDatabaseName, xCompanyUnkid, mdtRequestDate, mdtRequestDate, mstrUserAccessModeSetting, True, mblnIncludeInactiveEmployee, "List", "sta.transferrequestunkid = " & mintTransferrequestunkid, False, objDataOperation)
                    Dim dtTable As DataTable = dsAppovalList.Tables(0).Copy

                    xPushedBackPriority = dsApprover.Tables(0).Copy().AsEnumerable().Where(Function(x) x.Field(Of Integer)("priority") < mintPriority).Select(Function(x) x.Field(Of Integer)("priority")).DefaultIfEmpty().Max()

                    If dsApprover.Tables(0).Copy().AsEnumerable().Where(Function(x) x.Field(Of Integer)("priority") = xPushedBackPriority).Count > 0 Then
                        dtPushedBack = dsApprover.Tables(0).Copy().AsEnumerable().Where(Function(x) x.Field(Of Integer)("priority") >= xPushedBackPriority AndAlso x.Field(Of Integer)("priority") <= mintPriority).CopyToDataTable()
                    Else
                        dtPushedBack = dsApprover.Tables(0).Copy().AsEnumerable().Where(Function(x) x.Field(Of Integer)("priority") = mintPriority).CopyToDataTable()
                        xPushedBackPriority = -1
                    End If

                    If xPushedBackPriority >= 0 AndAlso dtPushedBack IsNot Nothing AndAlso dtPushedBack.Rows.Count > 0 Then

                        For i As Integer = 0 To dtPushedBack.Rows.Count - 1

                            mintUserunkid = xUserId
                            mintVoiduserunkid = xUserId
                            mstrVoidreason = mstrApprovalremark

                            If Delete(CInt(dtPushedBack.Rows(i)("transferapprovalunkid")), objDataOperation) = False Then
                                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                                Throw exForce
                            End If

                            mintStatusunkid = clstransfer_request_tran.enTransferRequestStatus.Pending
                            mstrApprovalremark = ""
                            mintUserunkid = xUserId
                            mblnIsvoid = False
                            mintVoiduserunkid = -1
                            mdtApprovaldate = Nothing
                            mdtVoiddatetime = Nothing
                            mstrVoidreason = String.Empty

                            Dim mstrSearch As String = "mapuserunkid = " & mintMapuserunkid & " AND priority = " & CInt(dtPushedBack.Rows(i)("priority"))
                            Dim dtUserWise As DataTable = New DataView(dtTable, mstrSearch, "", DataViewRowState.CurrentRows).ToTable()

                            Dim intMinPriority As Integer = -1

                            If dtUserWise IsNot Nothing AndAlso dtUserWise.Rows.Count > 0 Then

                                intMinPriority = dtPushedBack.AsEnumerable().Select(Function(x) x.Field(Of Integer)("priority")).DefaultIfEmpty().Min()

                                For Each drRow As DataRow In dtUserWise.Rows

                                    mintStatusunkid = clstransfer_request_tran.enTransferRequestStatus.Pending

                                    If intMinPriority = CInt(drRow("priority")) Then
                                        mintVisibleunkid = clstransfer_request_tran.enTransferRequestStatus.Pending
                                    Else
                                        mintVisibleunkid = -1
                                    End If

                                    If Insert(objDataOperation) = False Then
                                        exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                                        Throw exForce
                                    End If

                                Next  '  For Each drRow As DataRow In dtTable.Rows 

                            End If  ' If dtTable IsNot Nothing AndAlso dtTable.Rows.Count > 0 Then

                        Next  '    For i As Integer = 0 To dsApprover.Tables(0).Rows.Count - 1

                    Else    'IF FIRST LEVEL APPROVER PUSHED BACK APPLICATION THEN WILL GO TO APPLICANT AGAIN TO CHANGE ALLOCATION.

                        For i As Integer = 0 To dtPushedBack.Rows.Count - 1

                            mintVoiduserunkid = xVoidUserId
                            mstrVoidreason = xVoidReason
                            mdtVoiddatetime = xVoidDatetime

                            If Delete(CInt(dtPushedBack.Rows(i)("transferapprovalunkid")), objDataOperation) = False Then
                                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                                Throw exForce
                            End If

                            mintStatusunkid = clstransfer_request_tran.enTransferRequestStatus.Pending
                            mstrApprovalremark = ""
                            mintUserunkid = 0
                            mblnIsvoid = False
                            mintVoiduserunkid = -1
                            mdtApprovaldate = Nothing
                            mdtVoiddatetime = Nothing
                            mstrVoidreason = String.Empty


                            Dim mstrSearch As String = "mapuserunkid = " & mintMapuserunkid
                            Dim dtUserWise As DataTable = New DataView(dtTable, mstrSearch, "", DataViewRowState.CurrentRows).ToTable()

                            If dtUserWise IsNot Nothing AndAlso dtUserWise.Rows.Count > 0 Then

                                For Each drRow As DataRow In dtUserWise.Rows

                                    mintStatusunkid = clstransfer_request_tran.enTransferRequestStatus.Pending
                                    mintVisibleunkid = -1

                                    If Insert(objDataOperation) = False Then
                                        exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                                        Throw exForce
                                    End If

                                Next  '  For Each drRow As DataRow In dtTable.Rows 

                            End If  ' If dtTable IsNot Nothing AndAlso dtTable.Rows.Count > 0 Then

                        Next  'For i As Integer = 0 To dtPushedBack.Rows.Count - 1

                    End If   ' If drPushedBack IsNot Nothing AndAlso drPushedBack.Count > 0 Then

                End If   ' If dsApprover IsNot Nothing AndAlso dsApprover.Tables(0).Rows.Count > 0 Then

            End If   '    If mintStatusunkid <> clstransfer_request_tran.enTransferRequestStatus.PushedBack Then


            mintRoleunkid = xRoleId
            mintLevelunkid = xLevelId
            mintPriority = xPriority
            mintStatusunkid = xStatusId
            mintUserunkid = xUserId

            If UpdateTransferRequestStatus(xDatabaseName, xCompanyUnkid, xYearUnkid, mdtRequestDate, mdtRequestDate, mstrUserAccessModeSetting, True, mblnIncludeInactiveEmployee, objDataOperation) = False Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            If objDoOperation Is Nothing Then objDataOperation.ReleaseTransaction(True)

            Return True
        Catch ex As Exception
            If objDoOperation Is Nothing Then objDataOperation.ReleaseTransaction(False)
            Throw New Exception(ex.Message & "; Procedure Name: Update; Module Name: " & mstrModuleName)
            Return False
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            If objDoOperation Is Nothing Then objDataOperation = Nothing
        End Try
    End Function

    ''' <summary>
    ''' Modify By: Pinkal
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>
    Public Function UpdateTransferApproval(Optional ByVal objDOperation As clsDataOperation = Nothing) As Boolean

        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        If objDOperation Is Nothing Then
            objDataOperation = New clsDataOperation
            objDataOperation.BindTransaction()
        Else
            objDataOperation = objDOperation
        End If

        Try

            strQ = " UPDATE sttransfer_approval_tran SET " & _
                     "  stationunkid = @stationunkid" & _
                     ", deptgroupunkid = @deptgroupunkid" & _
                     ", departmentunkid = @departmentunkid" & _
                     ", sectiongroupunkid = @sectiongroupunkid" & _
                     ", sectionunkid = @sectionunkid" & _
                     ", unitgroupunkid = @unitgroupunkid" & _
                     ", unitunkid = @unitunkid" & _
                     ", teamunkid = @teamunkid" & _
                     ", classgroupunkid = @classgroupunkid" & _
                     ", classunkid = @classunkid" & _
                     ", jobgroupunkid = @jobgroupunkid" & _
                     ", jobunkid = @jobunkid" & _
                     ", gradegroupunkid = @gradegroupunkid" & _
                     ", gradeunkid = @gradeunkid" & _
                     ", gradelevelunkid = @gradelevelunkid" & _
                     ", reasonunkid = @reasonunkid" & _
                     ", userunkid = @userunkid" & _
                     ", isvoid = @isvoid" & _
                     ", voiddatetime = @voiddatetime" & _
                     ", voiduserunkid = @voiduserunkid" & _
                     ", voidreason = @voidreason " & _
                     " WHERE isvoid= 0 AND transferrequestunkid = @transferrequestunkid "

            objDataOperation.ClearParameters()
            objDataOperation.AddParameter("@transferrequestunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintTransferrequestunkid.ToString)
            objDataOperation.AddParameter("@stationunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintStationunkid.ToString)
            objDataOperation.AddParameter("@deptgroupunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintDeptgroupunkid.ToString)
            objDataOperation.AddParameter("@departmentunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintDepartmentunkid.ToString)
            objDataOperation.AddParameter("@sectiongroupunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintSectiongroupunkid.ToString)
            objDataOperation.AddParameter("@sectionunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintSectionunkid.ToString)
            objDataOperation.AddParameter("@unitgroupunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintUnitgroupunkid.ToString)
            objDataOperation.AddParameter("@unitunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintUnitunkid.ToString)
            objDataOperation.AddParameter("@teamunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintTeamunkid.ToString)
            objDataOperation.AddParameter("@classgroupunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintClassgroupunkid.ToString)
            objDataOperation.AddParameter("@classunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintClassunkid.ToString)
            objDataOperation.AddParameter("@jobgroupunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintJobgroupunkid.ToString)
            objDataOperation.AddParameter("@jobunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintJobunkid.ToString)
            objDataOperation.AddParameter("@gradegroupunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintGradegroupunkid.ToString)
            objDataOperation.AddParameter("@gradeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintGradeunkid.ToString)
            objDataOperation.AddParameter("@gradelevelunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintGradelevelunkid.ToString)
            objDataOperation.AddParameter("@reasonunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintReasonunkid.ToString)
            objDataOperation.AddParameter("@userunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintUserunkid.ToString)
            objDataOperation.AddParameter("@isvoid", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsvoid.ToString)

            If IsDBNull(mdtVoiddatetime) = False AndAlso mdtVoiddatetime <> Nothing Then
                objDataOperation.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, mdtVoiddatetime.ToString)
            Else
                objDataOperation.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, DBNull.Value)
            End If

            objDataOperation.AddParameter("@voiduserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintVoiduserunkid.ToString)
            objDataOperation.AddParameter("@voidreason", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrVoidreason.ToString)
            Call objDataOperation.ExecNonQuery(strQ)

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            strQ = " SELECT " & _
                      "  ta.transferapprovalunkid " & _
                      ", ta.transferrequestunkid " & _
                      ", ta.statusunkid " & _
                      ", ta.visibleunkid " & _
                      ", ta.priority " & _
                      ", tr.requestdate " & _
                      " FROM sttransfer_approval_tran ta " & _
                      " JOIN sttransfer_request_tran tr on tr.transferrequestunkid = ta.transferrequestunkid AND tr.isvoid = 0 " & _
                      " WHERE ta.isvoid=0 AND ta.transferrequestunkid = @transferrequestunkid "

            objDataOperation.ClearParameters()
            objDataOperation.AddParameter("@transferrequestunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintTransferrequestunkid.ToString)

            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If


            If dsList IsNot Nothing AndAlso dsList.Tables("List").Rows.Count > 0 Then
                Dim xCount As Integer = dsList.Tables(0).AsEnumerable().Where(Function(x) x.Field(Of Integer)("visibleunkid") = -1).DefaultIfEmpty.Count
                Dim xPriority As Integer = dsList.Tables(0).AsEnumerable().Where(Function(x) x.Field(Of Integer)("statusunkid") = enLoanApplicationStatus.PENDING AndAlso x.Field(Of Integer)("visibleunkid") = -1).Select(Function(x) x.Field(Of Integer)("priority")).DefaultIfEmpty.Min

                For Each dRow As DataRow In dsList.Tables("List").Rows

                    _Transferapprovalunkid(objDataOperation) = CInt(dRow.Item("transferapprovalunkid"))

                    If dsList.Tables(0).Rows.Count = xCount AndAlso xPriority = mintPriority AndAlso mintStatusunkid = clstransfer_request_tran.enTransferRequestStatus.Pending AndAlso mintVisibleunkid = -1 Then

                        mintVisibleunkid = clstransfer_request_tran.enTransferRequestStatus.Pending

                        strQ = " UPDATE sttransfer_approval_tran SET " & _
                                  "  visibleunkid = @visibleunkid " & _
                                  "  WHERE transferrequestunkid = @transferrequestunkid " & _
                                  "  AND transferapprovalunkid = @transferapprovalunkid " & _
                                  "  AND isvoid = 0 "

                        objDataOperation.ClearParameters()
                        objDataOperation.AddParameter("@transferapprovalunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintTransferapprovalunkid.ToString)
                        objDataOperation.AddParameter("@transferrequestunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintTransferrequestunkid.ToString)
                        objDataOperation.AddParameter("@visibleunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintVisibleunkid)
                        objDataOperation.ExecNonQuery(strQ)

                        If objDataOperation.ErrorMessage <> "" Then
                            exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                            Throw exForce
                        End If

                    End If

                    If InsertAuditTrailStaffTransferApproval(objDataOperation, enAuditType.EDIT) = False Then
                        exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                        Throw exForce
                    End If
                Next
            End If

            If objDOperation Is Nothing Then
                objDataOperation.ReleaseTransaction(True)
            End If

            Return True
        Catch ex As Exception
            If objDOperation Is Nothing Then objDataOperation.ReleaseTransaction(False)
            Throw New Exception(ex.Message & "; Procedure Name: UpdateTransferApproval; Module Name: " & mstrModuleName)
            Return False
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            If objDOperation Is Nothing Then objDataOperation = Nothing
        End Try
    End Function

    ''' <summary>
    ''' Modify By: Pinkal
    ''' </summary>
    ''' <returns>Boolean</returns>
    ''' <purpose> Delete Database Table (sttransfer_approval_tran) </purpose>
    Public Function Delete(ByVal intUnkid As Integer, Optional ByVal objDoOperation As clsDataOperation = Nothing) As Boolean
        'If isUsed(intUnkid) Then
        '    mstrMessage = "<Message>"
        '    Return False
        'End If
        Dim objDataOperation As clsDataOperation = Nothing
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception


        If objDoOperation Is Nothing Then
            objDataOperation = New clsDataOperation
            objDataOperation.BindTransaction()
        Else
            objDataOperation = objDoOperation
        End If

        Try
            strQ = "UPDATE sttransfer_approval_tran SET " & _
                      " isvoid = @isvoid" & _
                      ", voiddatetime = GETDATE()" & _
                      ", voiduserunkid = @voiduserunkid" & _
                      ", voidreason = @voidreason " & _
                      " WHERE transferapprovalunkid = @transferapprovalunkid "

            objDataOperation.ClearParameters()
            objDataOperation.AddParameter("@isvoid", SqlDbType.Bit, eZeeDataType.BIT_SIZE, True)
            objDataOperation.AddParameter("@voiduserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintVoiduserunkid)
            objDataOperation.AddParameter("@voidreason", SqlDbType.NVarChar, eZeeDataType.DESC_SIZE, mstrVoidreason)
            objDataOperation.AddParameter("@transferapprovalunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intUnkid)

            Call objDataOperation.ExecNonQuery(strQ)

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If


            objDataOperation.ClearParameters()
            _Transferapprovalunkid(objDataOperation) = intUnkid

            If InsertAuditTrailStaffTransferApproval(objDataOperation, enAuditType.DELETE) = False Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            If objDoOperation Is Nothing Then objDataOperation.ReleaseTransaction(True)

            Return True
        Catch ex As Exception
            If objDoOperation Is Nothing Then objDataOperation.ReleaseTransaction(False)
            Throw New Exception(ex.Message & "; Procedure Name: Delete; Module Name: " & mstrModuleName)
            Return False
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            If objDoOperation Is Nothing Then objDataOperation = Nothing
        End Try
    End Function

    ''' <summary>
    ''' Modify By: Pinkal
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>
    Public Function isUsed(ByVal intUnkid As Integer) As Boolean
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        objDataOperation = New clsDataOperation

        Try
            strQ = "<Query>"

            objDataOperation.AddParameter("@transferapprovalunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intUnkid)

            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            Return dsList.Tables(0).Rows.Count > 0
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: isUsed; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
    End Function

    ''' <summary>
    ''' Modify By: Pinkal
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>
    Public Function isExist(ByVal strCode As String, ByVal strName As String, Optional ByVal intUnkid As Integer = -1) As Boolean
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        objDataOperation = New clsDataOperation

        Try
            strQ = "SELECT " & _
              "  transferapprovalunkid " & _
              ", transferrequestunkid " & _
              ", employeeunkid " & _
              ", stmappingunkid " & _
              ", roleunkid " & _
              ", levelunkid " & _
              ", priority " & _
              ", mapuserunkid " & _
              ", approvaldate " & _
              ", stationunkid " & _
              ", deptgroupunkid " & _
              ", departmentunkid " & _
              ", sectiongroupunkid " & _
              ", sectionunkid " & _
              ", unitgroupunkid " & _
              ", unitunkid " & _
              ", teamunkid " & _
              ", classgroupunkid " & _
              ", classunkid " & _
              ", jobgroupunkid " & _
              ", jobunkid " & _
              ", gradegroupunkid " & _
              ", gradeunkid " & _
              ", gradelevelunkid " & _
              ", reasonunkid " & _
              ", approvalremark " & _
              ", statusunkid " & _
              ", visibleunkid " & _
              ", userunkid " & _
              ", isvoid " & _
              ", voiddatetime " & _
              ", voiduserunkid " & _
              ", voidreason " & _
             "FROM sttransfer_approval_tran " & _
             "WHERE name = @name " & _
             "AND code = @code "

            If intUnkid > 0 Then
                strQ &= " AND transferapprovalunkid <> @transferapprovalunkid"
            End If

            objDataOperation.AddParameter("@code", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, strCode)
            objDataOperation.AddParameter("@name", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, strName)
            objDataOperation.AddParameter("@transferapprovalunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intUnkid)

            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            Return dsList.Tables(0).Rows.Count > 0
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: isExist; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
    End Function

    ''' <summary>
    ''' Modify By: Pinkal
    ''' </summary>
    ''' <returns>Boolean</returns>
    ''' <purpose> INSERT INTO Database Table (sttransfer_request_tran) </purpose>
    Public Function InsertAuditTrailStaffTransferApproval(ByVal objDataOperation As clsDataOperation, ByVal intAuditType As enAuditType) As Boolean
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        Try
            strQ = " INSERT INTO atsttransfer_approval_tran ( " & _
                     " transferapprovalunkid " & _
                     ", transferrequestunkid " & _
                     ", employeeunkid " & _
                     ", stmappingunkid " & _
                     ", roleunkid " & _
                     ", levelunkid " & _
                     ", priority " & _
                     ", mapuserunkid " & _
                     ", approvaldate " & _
                     ", stationunkid " & _
                     ", deptgroupunkid " & _
                     ", departmentunkid " & _
                     ", sectiongroupunkid " & _
                     ", sectionunkid " & _
                     ", unitgroupunkid " & _
                     ", unitunkid " & _
                     ", teamunkid " & _
                     ", classgroupunkid " & _
                     ", classunkid " & _
                     ", jobgroupunkid " & _
                     ", jobunkid " & _
                     ", gradegroupunkid " & _
                     ", gradeunkid " & _
                     ", gradelevelunkid " & _
                     ", reasonunkid " & _
                     ", approvalremark " & _
                     ", statusunkid " & _
                     ", visibleunkid " & _
                     ", audittype " & _
                     ", audituserunkid " & _
                     ", auditdatetime " & _
                     ", ip " & _
                     ", machine_name" & _
                     ", form_name " & _
                     ", isweb" & _
                     " ) VALUES (" & _
                     "  @transferapprovalunkid " & _
                     ", @transferrequestunkid " & _
                     ", @employeeunkid " & _
                     ", @stmappingunkid " & _
                     ", @roleunkid " & _
                     ", @levelunkid " & _
                     ", @priority " & _
                     ", @mapuserunkid " & _
                     ", @approvaldate " & _
                     ", @stationunkid " & _
                     ", @deptgroupunkid " & _
                     ", @departmentunkid " & _
                     ", @sectiongroupunkid " & _
                     ", @sectionunkid " & _
                     ", @unitgroupunkid " & _
                     ", @unitunkid " & _
                     ", @teamunkid " & _
                     ", @classgroupunkid " & _
                     ", @classunkid " & _
                     ", @jobgroupunkid " & _
                     ", @jobunkid " & _
                     ", @gradegroupunkid " & _
                     ", @gradeunkid " & _
                     ", @gradelevelunkid " & _
                     ", @reasonunkid " & _
                     ", @approvalremark " & _
                     ", @statusunkid " & _
                     ", @visibleunkid " & _
                     ", @audittype " & _
                     ", @audituserunkid " & _
                     ", GETDATE() " & _
                     ", @ip " & _
                     ", @machine_name" & _
                     ", @form_name " & _
                     ", @isweb" & _
                     " ); SELECT @@identity"

            objDataOperation.ClearParameters()
            objDataOperation.AddParameter("@transferapprovalunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintTransferapprovalunkid.ToString)
            objDataOperation.AddParameter("@transferrequestunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintTransferrequestunkid.ToString)
            objDataOperation.AddParameter("@employeeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintEmployeeunkid.ToString)
            objDataOperation.AddParameter("@stmappingunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintStmappingunkid.ToString)
            objDataOperation.AddParameter("@roleunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintRoleunkid.ToString)
            objDataOperation.AddParameter("@levelunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintLevelunkid.ToString)
            objDataOperation.AddParameter("@priority", SqlDbType.Int, eZeeDataType.INT_SIZE, mintPriority.ToString)
            objDataOperation.AddParameter("@mapuserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintMapuserunkid.ToString)
            If IsDBNull(mdtApprovaldate) = False AndAlso mdtApprovaldate <> Nothing Then
                objDataOperation.AddParameter("@approvaldate", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, mdtApprovaldate.ToString)
            Else
                objDataOperation.AddParameter("@approvaldate", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, DBNull.Value)
            End If
            objDataOperation.AddParameter("@stationunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintStationunkid.ToString)
            objDataOperation.AddParameter("@deptgroupunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintDeptgroupunkid.ToString)
            objDataOperation.AddParameter("@departmentunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintDepartmentunkid.ToString)
            objDataOperation.AddParameter("@sectiongroupunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintSectiongroupunkid.ToString)
            objDataOperation.AddParameter("@sectionunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintSectionunkid.ToString)
            objDataOperation.AddParameter("@unitgroupunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintUnitgroupunkid.ToString)
            objDataOperation.AddParameter("@unitunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintUnitunkid.ToString)
            objDataOperation.AddParameter("@teamunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintTeamunkid.ToString)
            objDataOperation.AddParameter("@classgroupunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintClassgroupunkid.ToString)
            objDataOperation.AddParameter("@classunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintClassunkid.ToString)
            objDataOperation.AddParameter("@jobgroupunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintJobgroupunkid.ToString)
            objDataOperation.AddParameter("@jobunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintJobunkid.ToString)
            objDataOperation.AddParameter("@gradegroupunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintGradegroupunkid.ToString)
            objDataOperation.AddParameter("@gradeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintGradeunkid.ToString)
            objDataOperation.AddParameter("@gradelevelunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintGradelevelunkid.ToString)
            objDataOperation.AddParameter("@reasonunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintReasonunkid.ToString)
            objDataOperation.AddParameter("@approvalremark", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrApprovalremark.ToString)
            objDataOperation.AddParameter("@statusunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintStatusunkid.ToString)
            objDataOperation.AddParameter("@visibleunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintVisibleunkid.ToString)
            objDataOperation.AddParameter("@audittype", SqlDbType.Int, eZeeDataType.INT_SIZE, intAuditType)
            objDataOperation.AddParameter("@audituserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintUserunkid)
            objDataOperation.AddParameter("@ip", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, IIf(mstrWebClientIP.Trim.Length <= 0, getIP, mstrWebClientIP.Trim))
            objDataOperation.AddParameter("@machine_name", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, IIf(mstrWebHostName.Trim.Length <= 0, getHostName, mstrWebHostName.Trim))
            objDataOperation.AddParameter("@form_name", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrWebFormName)
            objDataOperation.AddParameter("@isweb", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsWeb)

            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            Return True
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: InsertAuditTrailStaffTransferApproval; Module Name: " & mstrModuleName)
            Return False
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
        End Try
    End Function

    ''' <summary>
    ''' Modify By: Pinkal
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>
    Public Function UpdateTransferRequestStatus(ByVal xDatabaseName As String, _
                                                                    ByVal xCompanyUnkid As Integer, _
                                                                    ByVal xYearId As Integer, _
                                                                    ByVal xPeriodStart As DateTime, _
                                                                    ByVal xPeriodEnd As DateTime, _
                                                                    ByVal xUserModeSetting As String, _
                                                                    ByVal xOnlyApproved As Boolean, _
                                                                    ByVal xIncludeIn_ActiveEmployee As Boolean, _
                                                                    ByVal objDoOperation As clsDataOperation) As Boolean
        Try
            Dim exForce As Exception

            If mintStatusunkid = clstransfer_request_tran.enTransferRequestStatus.Approved Then


                Dim dsApprover As DataSet = GetTrasferApprovalList(xDatabaseName, xCompanyUnkid _
                                                                                           , xPeriodStart, xPeriodEnd, xUserModeSetting, xOnlyApproved _
                                                                                           , xIncludeIn_ActiveEmployee, "List", " sta.transferrequestunkid = " & mintTransferrequestunkid & " AND sta.priority > " & mintPriority _
                                                                                           , False, objDoOperation)

                If dsApprover IsNot Nothing AndAlso dsApprover.Tables(0).Rows.Count <= 0 Then
                    Dim objTransfer As New clstransfer_request_tran
                    objTransfer._Transferrequestunkid(objDoOperation) = mintTransferrequestunkid
                    objTransfer._Statusunkid = clstransfer_request_tran.enTransferRequestStatus.Approved
                    objTransfer._WebClientIP = mstrWebClientIP
                    objTransfer._WebFormName = mstrWebFormName
                    objTransfer._WebHostName = mstrWebHostName
                    objTransfer._IsWeb = mblnIsWeb
                    objTransfer._Userunkid = mintUserunkid

                    If objTransfer.Update(xDatabaseName, xYearId, xCompanyUnkid, objDoOperation, Nothing, False) = False Then
                        exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                        Throw exForce
                    End If

                    dsApprover.Tables(0).Rows.Clear()
                    dsApprover = Nothing

                End If '  If dsApprover IsNot Nothing AndAlso dsApprover.Tables(0).Rows.Count <= 0 Then

            End If  '    If mintStatusunkid = clstransfer_request_tran.enTransferRequestStatus.Approved Then

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: UpdateTransferRequestStatus; Module Name: " & mstrModuleName)
            Return False
        End Try
        Return True
    End Function

    ''' <summary>
    ''' Modify By: Pinkal
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>
    Public Function IsPendingTraferRequestApplication(ByVal xTransferRequestId As Integer, Optional ByVal blnGetApplicationStatus As Boolean = False) As Boolean
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception
        Try
            objDataOperation = New clsDataOperation

            strQ = " SELECT " & _
                   "     transferrequestunkid " & _
                   "    ,statusunkid " & _
                   " FROM sttransfer_approval_tran " & _
                   " WHERE isvoid = 0 " & _
                   " AND transferrequestunkid = @transferrequestunkid "

            If blnGetApplicationStatus = True Then
                strQ &= " AND statusunkid = " & clstransfer_request_tran.enTransferRequestStatus.Pending & "  AND visibleid < " & clstransfer_request_tran.enTransferRequestStatus.Approved
            Else
                strQ &= " AND statusunkid <> " & clstransfer_request_tran.enTransferRequestStatus.Pending
            End If

            objDataOperation.ClearParameters()
            objDataOperation.AddParameter("@transferrequestunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, xTransferRequestId)

            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            If dsList.Tables("List").Rows.Count > 0 Then
                Return False
            Else
                Return True
            End If

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: IsPendingTraferRequestApplication; Module Name: " & mstrModuleName)
        End Try
    End Function

    ''' <summary>
    ''' Modify By: Pinkal
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>
    Public Sub SendStaffTransferFinalApprovalNotificationToUsers(ByVal xCompanyId As Integer, ByVal mdtEmployeeAsonDate As Date _
                                                                                            , ByVal xUserIds As String, ByVal xEmployeeId As Integer _
                                                                                            , ByVal strPath As String, ByVal strFileName As String)
        Dim StrMessage As String = String.Empty
        Dim mstrSenderAddress As String = String.Empty
        Dim mstrEmployeeName As String = String.Empty
        Try
            If xUserIds.Trim.Length > 0 Then

                Dim objcompany As New clsCompany_Master
                objcompany._Companyunkid = xCompanyId
                mstrSenderAddress = objcompany._Senderaddress
                objcompany = Nothing


                Dim objEmployee As New clsEmployee_Master
                objEmployee._Employeeunkid(mdtEmployeeAsonDate) = xEmployeeId
                mstrEmployeeName = objEmployee._Employeecode & " - " & objEmployee._Firstname & " " & objEmployee._Surname
                objEmployee = Nothing

                Dim objUsr As New clsUserAddEdit
                Dim info1 As System.Globalization.TextInfo = System.Globalization.CultureInfo.InvariantCulture.TextInfo
                For Each sId As String In xUserIds.Trim.Split(CChar(","))

                    StrMessage = ""
                    If sId.Trim = "" Then Continue For

                    objUsr._Userunkid = CInt(sId)

                    If objUsr._Firstname.Trim.Length <= 0 AndAlso objUsr._Lastname.Trim.Length <= 0 Then
                        StrMessage &= Language.getMessage(mstrModuleName, 1, "Dear") & " " & info1.ToTitleCase(objUsr._Username) & ", <BR><BR>"
                    Else
                        StrMessage &= Language.getMessage(mstrModuleName, 1, "Dear") & " " & info1.ToTitleCase(objUsr._Firstname & " " & objUsr._Lastname) & ", <BR><BR>"

                    End If

                    StrMessage &= Language.getMessage(mstrModuleName, 2, " Staff Initiated Request I.F.O") & " <b>(" & info1.ToTitleCase(mstrEmployeeName) & ")</b>" & _
                                              " " & Language.getMessage(mstrModuleName, 3, "has been final approved.Attached is the approved application form for your reference.")

                    StrMessage &= "<BR></BR><BR></BR>"
                    StrMessage &= Language.getMessage(mstrModuleName, 4, "Regards.")

                    StrMessage &= "<p><center style='padding-top:25px;font-size:10.0pt;font-family:&quot;Verdana&quot;,&quot;Sans-Serif&quot;;color:#333399;margin-left:0px;margin-right:0px;margin-top:0px;margin-bottom:10px'><b>&quot;POWERED BY ARUTI HR &amp; PAYROLL MANAGEMENT SOFTWARE.&quot;</b></center></p>"
                    StrMessage &= "</BODY></HTML>"


                    Dim objSendMail As New clsSendMail
                    objSendMail._ToEmail = objUsr._Email
                    objSendMail._Subject = Language.getMessage(mstrModuleName, 5, "Staff Transfer Request Approval")
                    objSendMail._Message = StrMessage

                    If strPath <> "" Then
                        objSendMail._AttachedFiles = strFileName
                    End If

                    objSendMail._Form_Name = mstrWebFormName
                    objSendMail._LogEmployeeUnkid = -1
                    objSendMail._OperationModeId = enLogin_Mode.MGR_SELF_SERVICE
                    objSendMail._UserUnkid = objUsr._Userunkid
                    objSendMail._SenderAddress = mstrSenderAddress
                    objSendMail._ModuleRefId = clsSendMail.enAT_VIEW_TYPE.EMPLOYEE_MGT

                    If strPath <> "" Then
                        If objSendMail.SendMail(xCompanyId, True, strPath).ToString.Length > 0 Then
                            Continue For
                        End If
                    Else
                        If objSendMail.SendMail(xCompanyId).ToString.Length > 0 Then
                            Continue For
                        End If
                    End If
                Next
                info1 = Nothing
                objUsr = Nothing
            End If
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: SendStaffTransferFinalApprovalNotificationToUsers; Module Name: " & mstrModuleName)
        End Try
    End Sub

    '<Language> This Auto Generated Text Please Do Not Modify it.
#Region " Language & UI Settings "
    Public Shared Sub SetMessages()
        Try
            Language.setMessage("clstransfer_request_tran", 2, "Pending")
            Language.setMessage("clstransfer_request_tran", 3, "Approved")
            Language.setMessage("clstransfer_request_tran", 4, "Pushed Back")
            Language.setMessage(mstrModuleName, 1, "Dear")
            Language.setMessage(mstrModuleName, 2, " Staff Initiated Request I.F.O")
            Language.setMessage(mstrModuleName, 3, "has been final approved.Attached is the approved application form for your reference.")
            Language.setMessage(mstrModuleName, 4, "Regards.")
            Language.setMessage(mstrModuleName, 5, "Staff Transfer Request Approval")

        Catch Ex As Exception
            DisplayError.Show("-1", Ex.Message, "SetMessages", mstrModuleName)
        End Try
    End Sub
#End Region 'Language & UI Settings
    '</Language>
End Class