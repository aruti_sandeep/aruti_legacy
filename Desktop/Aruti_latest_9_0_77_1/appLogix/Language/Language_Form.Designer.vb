<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmLanguage
    Inherits eZee.Common.eZeeForm

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing AndAlso components IsNot Nothing Then
            components.Dispose()
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmLanguage))
        Dim DataGridViewCellStyle1 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle
        Dim DataGridViewCellStyle2 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle
        Dim DataGridViewCellStyle3 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle
        Me.btnSave = New eZee.Common.eZeeLightButton(Me.components)
        Me.btnCancel = New eZee.Common.eZeeLightButton(Me.components)
        Me.pnlMain = New System.Windows.Forms.Panel
        Me.objefFooter = New eZee.Common.eZeeFooter
        Me.btnFindReplace = New eZee.Common.eZeeLightButton(Me.components)
        Me.tabcLanguage = New System.Windows.Forms.TabControl
        Me.tabpCaption = New System.Windows.Forms.TabPage
        Me.dgLanguage = New System.Windows.Forms.DataGridView
        Me.dgColLanguage = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.dgcollanguage1 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.dgcolLanguage2 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.dgcollangunkid = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.dgcolformname = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.dgcolcontrolname = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.dgcolApplication = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.tabpMessages = New System.Windows.Forms.TabPage
        Me.dgMessage = New System.Windows.Forms.DataGridView
        Me.dgcolMessage = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.dgcolMessage1 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.dgcolMessage2 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.dgcolMsgCode = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.dgcolMsgModuleName = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.dgcolMsgApplication = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.tabpOthers = New System.Windows.Forms.TabPage
        Me.dgOtherMessages = New System.Windows.Forms.DataGridView
        Me.dgcolOtLanguage = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.dgcolOtLanguage1 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.dgcolOtLanguage2 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.dgcolOtMsgID = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.dgcolOtModuleName = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.dgcolOtApplication = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.cboFormFilter = New System.Windows.Forms.ComboBox
        Me.cboModuleFilter = New System.Windows.Forms.ComboBox
        Me.cboOtherModuleFilter = New System.Windows.Forms.ComboBox
        Me.pnlMain.SuspendLayout()
        Me.objefFooter.SuspendLayout()
        Me.tabcLanguage.SuspendLayout()
        Me.tabpCaption.SuspendLayout()
        CType(Me.dgLanguage, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.tabpMessages.SuspendLayout()
        CType(Me.dgMessage, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.tabpOthers.SuspendLayout()
        CType(Me.dgOtherMessages, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'btnSave
        '
        Me.btnSave.BackColor = System.Drawing.Color.White
        Me.btnSave.BackgroundImage = CType(resources.GetObject("btnSave.BackgroundImage"), System.Drawing.Image)
        Me.btnSave.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.btnSave.BorderColor = System.Drawing.Color.Empty
        Me.btnSave.ButtonType = eZee.Common.eZeeLightButton.enButtonType.NormalButton
        Me.btnSave.FlatAppearance.BorderSize = 0
        Me.btnSave.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnSave.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnSave.ForeColor = System.Drawing.Color.Black
        Me.btnSave.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.btnSave.GradientForeColor = System.Drawing.Color.Black
        Me.btnSave.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.btnSave.HoverGradientForeColor = System.Drawing.Color.Black
        Me.btnSave.Location = New System.Drawing.Point(538, 13)
        Me.btnSave.Name = "btnSave"
        Me.btnSave.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.btnSave.PressedGradientForeColor = System.Drawing.Color.Black
        Me.btnSave.Selected = False
        Me.btnSave.ShowDefaultBorderColor = True
        Me.btnSave.Size = New System.Drawing.Size(91, 30)
        Me.btnSave.TabIndex = 4
        Me.btnSave.Text = "&Save"
        Me.btnSave.UseVisualStyleBackColor = False
        '
        'btnCancel
        '
        Me.btnCancel.BackColor = System.Drawing.Color.White
        Me.btnCancel.BackgroundImage = CType(resources.GetObject("btnCancel.BackgroundImage"), System.Drawing.Image)
        Me.btnCancel.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.btnCancel.BorderColor = System.Drawing.Color.Empty
        Me.btnCancel.ButtonType = eZee.Common.eZeeLightButton.enButtonType.NormalButton
        Me.btnCancel.FlatAppearance.BorderSize = 0
        Me.btnCancel.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnCancel.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnCancel.ForeColor = System.Drawing.Color.Black
        Me.btnCancel.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.btnCancel.GradientForeColor = System.Drawing.Color.Black
        Me.btnCancel.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.btnCancel.HoverGradientForeColor = System.Drawing.Color.Black
        Me.btnCancel.Location = New System.Drawing.Point(635, 13)
        Me.btnCancel.Name = "btnCancel"
        Me.btnCancel.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.btnCancel.PressedGradientForeColor = System.Drawing.Color.Black
        Me.btnCancel.Selected = False
        Me.btnCancel.ShowDefaultBorderColor = True
        Me.btnCancel.Size = New System.Drawing.Size(91, 30)
        Me.btnCancel.TabIndex = 5
        Me.btnCancel.Text = "&Close"
        Me.btnCancel.UseVisualStyleBackColor = False
        '
        'pnlMain
        '
        Me.pnlMain.Controls.Add(Me.objefFooter)
        Me.pnlMain.Controls.Add(Me.tabcLanguage)
        Me.pnlMain.Dock = System.Windows.Forms.DockStyle.Fill
        Me.pnlMain.Location = New System.Drawing.Point(0, 0)
        Me.pnlMain.Name = "pnlMain"
        Me.pnlMain.Size = New System.Drawing.Size(736, 499)
        Me.pnlMain.TabIndex = 0
        '
        'objefFooter
        '
        Me.objefFooter.BorderColor = System.Drawing.Color.Silver
        Me.objefFooter.Controls.Add(Me.btnCancel)
        Me.objefFooter.Controls.Add(Me.btnFindReplace)
        Me.objefFooter.Controls.Add(Me.btnSave)
        Me.objefFooter.Dock = System.Windows.Forms.DockStyle.Bottom
        Me.objefFooter.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.objefFooter.GradiantStyle = eZee.Common.eZeeFooter.GradientStyle.Up
        Me.objefFooter.GradientColor1 = System.Drawing.SystemColors.Control
        Me.objefFooter.GradientColor2 = System.Drawing.SystemColors.Control
        Me.objefFooter.Location = New System.Drawing.Point(0, 444)
        Me.objefFooter.Name = "objefFooter"
        Me.objefFooter.Size = New System.Drawing.Size(736, 55)
        Me.objefFooter.TabIndex = 13
        '
        'btnFindReplace
        '
        Me.btnFindReplace.BackColor = System.Drawing.Color.White
        Me.btnFindReplace.BackgroundImage = CType(resources.GetObject("btnFindReplace.BackgroundImage"), System.Drawing.Image)
        Me.btnFindReplace.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.btnFindReplace.BorderColor = System.Drawing.Color.Empty
        Me.btnFindReplace.ButtonType = eZee.Common.eZeeLightButton.enButtonType.NormalButton
        Me.btnFindReplace.FlatAppearance.BorderSize = 0
        Me.btnFindReplace.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnFindReplace.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnFindReplace.ForeColor = System.Drawing.Color.Black
        Me.btnFindReplace.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.btnFindReplace.GradientForeColor = System.Drawing.Color.Black
        Me.btnFindReplace.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.btnFindReplace.HoverGradientForeColor = System.Drawing.Color.Black
        Me.btnFindReplace.Location = New System.Drawing.Point(9, 13)
        Me.btnFindReplace.Name = "btnFindReplace"
        Me.btnFindReplace.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.btnFindReplace.PressedGradientForeColor = System.Drawing.Color.Black
        Me.btnFindReplace.Selected = False
        Me.btnFindReplace.ShowDefaultBorderColor = True
        Me.btnFindReplace.Size = New System.Drawing.Size(108, 30)
        Me.btnFindReplace.TabIndex = 12
        Me.btnFindReplace.Text = "&Find && Replace"
        Me.btnFindReplace.UseVisualStyleBackColor = False
        '
        'tabcLanguage
        '
        Me.tabcLanguage.Controls.Add(Me.tabpCaption)
        Me.tabcLanguage.Controls.Add(Me.tabpMessages)
        Me.tabcLanguage.Controls.Add(Me.tabpOthers)
        Me.tabcLanguage.ItemSize = New System.Drawing.Size(60, 18)
        Me.tabcLanguage.Location = New System.Drawing.Point(9, 9)
        Me.tabcLanguage.Multiline = True
        Me.tabcLanguage.Name = "tabcLanguage"
        Me.tabcLanguage.SelectedIndex = 0
        Me.tabcLanguage.Size = New System.Drawing.Size(719, 456)
        Me.tabcLanguage.TabIndex = 11
        '
        'tabpCaption
        '
        Me.tabpCaption.Controls.Add(Me.cboFormFilter)
        Me.tabpCaption.Controls.Add(Me.dgLanguage)
        Me.tabpCaption.Location = New System.Drawing.Point(4, 22)
        Me.tabpCaption.Name = "tabpCaption"
        Me.tabpCaption.Padding = New System.Windows.Forms.Padding(3)
        Me.tabpCaption.Size = New System.Drawing.Size(711, 430)
        Me.tabpCaption.TabIndex = 0
        Me.tabpCaption.Text = "Language"
        Me.tabpCaption.UseVisualStyleBackColor = True
        '
        'dgLanguage
        '
        Me.dgLanguage.AllowUserToAddRows = False
        Me.dgLanguage.AllowUserToDeleteRows = False
        Me.dgLanguage.BackgroundColor = System.Drawing.Color.White
        Me.dgLanguage.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D
        Me.dgLanguage.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.dgLanguage.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.dgColLanguage, Me.dgcollanguage1, Me.dgcolLanguage2, Me.dgcollangunkid, Me.dgcolformname, Me.dgcolcontrolname, Me.dgcolApplication})
        DataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft
        DataGridViewCellStyle1.BackColor = System.Drawing.SystemColors.Window
        DataGridViewCellStyle1.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle1.ForeColor = System.Drawing.SystemColors.ControlText
        DataGridViewCellStyle1.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle1.SelectionForeColor = System.Drawing.SystemColors.HighlightText
        DataGridViewCellStyle1.WrapMode = System.Windows.Forms.DataGridViewTriState.[True]
        Me.dgLanguage.DefaultCellStyle = DataGridViewCellStyle1
        Me.dgLanguage.Location = New System.Drawing.Point(4, 35)
        Me.dgLanguage.MultiSelect = False
        Me.dgLanguage.Name = "dgLanguage"
        Me.dgLanguage.RowHeadersVisible = False
        Me.dgLanguage.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.CellSelect
        Me.dgLanguage.ShowCellErrors = False
        Me.dgLanguage.ShowRowErrors = False
        Me.dgLanguage.Size = New System.Drawing.Size(700, 391)
        Me.dgLanguage.TabIndex = 1
        '
        'dgColLanguage
        '
        Me.dgColLanguage.Frozen = True
        Me.dgColLanguage.HeaderText = "Language"
        Me.dgColLanguage.Name = "dgColLanguage"
        Me.dgColLanguage.ReadOnly = True
        Me.dgColLanguage.Width = 233
        '
        'dgcollanguage1
        '
        Me.dgcollanguage1.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.None
        Me.dgcollanguage1.Frozen = True
        Me.dgcollanguage1.HeaderText = "Language1"
        Me.dgcollanguage1.Name = "dgcollanguage1"
        Me.dgcollanguage1.Width = 233
        '
        'dgcolLanguage2
        '
        Me.dgcolLanguage2.Frozen = True
        Me.dgcolLanguage2.HeaderText = "Language2"
        Me.dgcolLanguage2.Name = "dgcolLanguage2"
        Me.dgcolLanguage2.Width = 233
        '
        'dgcollangunkid
        '
        Me.dgcollangunkid.Frozen = True
        Me.dgcollangunkid.HeaderText = "ID"
        Me.dgcollangunkid.Name = "dgcollangunkid"
        Me.dgcollangunkid.ReadOnly = True
        Me.dgcollangunkid.Visible = False
        Me.dgcollangunkid.Width = 5
        '
        'dgcolformname
        '
        Me.dgcolformname.Frozen = True
        Me.dgcolformname.HeaderText = "Form Name"
        Me.dgcolformname.Name = "dgcolformname"
        Me.dgcolformname.ReadOnly = True
        Me.dgcolformname.Visible = False
        Me.dgcolformname.Width = 150
        '
        'dgcolcontrolname
        '
        Me.dgcolcontrolname.Frozen = True
        Me.dgcolcontrolname.HeaderText = "Control Name"
        Me.dgcolcontrolname.Name = "dgcolcontrolname"
        Me.dgcolcontrolname.ReadOnly = True
        Me.dgcolcontrolname.Visible = False
        Me.dgcolcontrolname.Width = 150
        '
        'dgcolApplication
        '
        Me.dgcolApplication.Frozen = True
        Me.dgcolApplication.HeaderText = "Application"
        Me.dgcolApplication.Name = "dgcolApplication"
        Me.dgcolApplication.ReadOnly = True
        Me.dgcolApplication.Visible = False
        '
        'tabpMessages
        '
        Me.tabpMessages.Controls.Add(Me.cboModuleFilter)
        Me.tabpMessages.Controls.Add(Me.dgMessage)
        Me.tabpMessages.Location = New System.Drawing.Point(4, 22)
        Me.tabpMessages.Name = "tabpMessages"
        Me.tabpMessages.Padding = New System.Windows.Forms.Padding(3)
        Me.tabpMessages.Size = New System.Drawing.Size(711, 430)
        Me.tabpMessages.TabIndex = 1
        Me.tabpMessages.Text = "Messages"
        Me.tabpMessages.UseVisualStyleBackColor = True
        '
        'dgMessage
        '
        Me.dgMessage.AllowUserToAddRows = False
        Me.dgMessage.AllowUserToDeleteRows = False
        Me.dgMessage.AutoSizeRowsMode = System.Windows.Forms.DataGridViewAutoSizeRowsMode.AllCellsExceptHeaders
        Me.dgMessage.BackgroundColor = System.Drawing.Color.White
        Me.dgMessage.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D
        Me.dgMessage.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.dgMessage.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.dgcolMessage, Me.dgcolMessage1, Me.dgcolMessage2, Me.dgcolMsgCode, Me.dgcolMsgModuleName, Me.dgcolMsgApplication})
        DataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft
        DataGridViewCellStyle2.BackColor = System.Drawing.SystemColors.Window
        DataGridViewCellStyle2.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle2.ForeColor = System.Drawing.SystemColors.ControlText
        DataGridViewCellStyle2.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle2.SelectionForeColor = System.Drawing.SystemColors.HighlightText
        DataGridViewCellStyle2.WrapMode = System.Windows.Forms.DataGridViewTriState.[True]
        Me.dgMessage.DefaultCellStyle = DataGridViewCellStyle2
        Me.dgMessage.Location = New System.Drawing.Point(4, 35)
        Me.dgMessage.MultiSelect = False
        Me.dgMessage.Name = "dgMessage"
        Me.dgMessage.RowHeadersVisible = False
        Me.dgMessage.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.CellSelect
        Me.dgMessage.ShowCellErrors = False
        Me.dgMessage.ShowRowErrors = False
        Me.dgMessage.Size = New System.Drawing.Size(700, 391)
        Me.dgMessage.TabIndex = 2
        '
        'dgcolMessage
        '
        Me.dgcolMessage.Frozen = True
        Me.dgcolMessage.HeaderText = "Message"
        Me.dgcolMessage.Name = "dgcolMessage"
        Me.dgcolMessage.ReadOnly = True
        Me.dgcolMessage.Width = 233
        '
        'dgcolMessage1
        '
        Me.dgcolMessage1.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.None
        Me.dgcolMessage1.Frozen = True
        Me.dgcolMessage1.HeaderText = "Message1"
        Me.dgcolMessage1.Name = "dgcolMessage1"
        Me.dgcolMessage1.Width = 233
        '
        'dgcolMessage2
        '
        Me.dgcolMessage2.Frozen = True
        Me.dgcolMessage2.HeaderText = "Message2"
        Me.dgcolMessage2.Name = "dgcolMessage2"
        Me.dgcolMessage2.Width = 233
        '
        'dgcolMsgCode
        '
        Me.dgcolMsgCode.Frozen = True
        Me.dgcolMsgCode.HeaderText = "ID"
        Me.dgcolMsgCode.Name = "dgcolMsgCode"
        Me.dgcolMsgCode.ReadOnly = True
        Me.dgcolMsgCode.Visible = False
        Me.dgcolMsgCode.Width = 5
        '
        'dgcolMsgModuleName
        '
        Me.dgcolMsgModuleName.Frozen = True
        Me.dgcolMsgModuleName.HeaderText = "Module Name"
        Me.dgcolMsgModuleName.Name = "dgcolMsgModuleName"
        Me.dgcolMsgModuleName.ReadOnly = True
        Me.dgcolMsgModuleName.Visible = False
        Me.dgcolMsgModuleName.Width = 150
        '
        'dgcolMsgApplication
        '
        Me.dgcolMsgApplication.HeaderText = "Application"
        Me.dgcolMsgApplication.Name = "dgcolMsgApplication"
        Me.dgcolMsgApplication.ReadOnly = True
        Me.dgcolMsgApplication.Visible = False
        '
        'tabpOthers
        '
        Me.tabpOthers.Controls.Add(Me.cboOtherModuleFilter)
        Me.tabpOthers.Controls.Add(Me.dgOtherMessages)
        Me.tabpOthers.Location = New System.Drawing.Point(4, 22)
        Me.tabpOthers.Name = "tabpOthers"
        Me.tabpOthers.Padding = New System.Windows.Forms.Padding(3)
        Me.tabpOthers.Size = New System.Drawing.Size(711, 430)
        Me.tabpOthers.TabIndex = 2
        Me.tabpOthers.Text = "Others"
        Me.tabpOthers.UseVisualStyleBackColor = True
        '
        'dgOtherMessages
        '
        Me.dgOtherMessages.AllowUserToAddRows = False
        Me.dgOtherMessages.AllowUserToDeleteRows = False
        Me.dgOtherMessages.AutoSizeRowsMode = System.Windows.Forms.DataGridViewAutoSizeRowsMode.AllCellsExceptHeaders
        Me.dgOtherMessages.BackgroundColor = System.Drawing.Color.White
        Me.dgOtherMessages.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D
        Me.dgOtherMessages.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.dgOtherMessages.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.dgcolOtLanguage, Me.dgcolOtLanguage1, Me.dgcolOtLanguage2, Me.dgcolOtMsgID, Me.dgcolOtModuleName, Me.dgcolOtApplication})
        DataGridViewCellStyle3.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft
        DataGridViewCellStyle3.BackColor = System.Drawing.SystemColors.Window
        DataGridViewCellStyle3.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle3.ForeColor = System.Drawing.SystemColors.ControlText
        DataGridViewCellStyle3.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle3.SelectionForeColor = System.Drawing.SystemColors.HighlightText
        DataGridViewCellStyle3.WrapMode = System.Windows.Forms.DataGridViewTriState.[True]
        Me.dgOtherMessages.DefaultCellStyle = DataGridViewCellStyle3
        Me.dgOtherMessages.Location = New System.Drawing.Point(4, 35)
        Me.dgOtherMessages.MultiSelect = False
        Me.dgOtherMessages.Name = "dgOtherMessages"
        Me.dgOtherMessages.RowHeadersVisible = False
        Me.dgOtherMessages.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.CellSelect
        Me.dgOtherMessages.ShowCellErrors = False
        Me.dgOtherMessages.ShowRowErrors = False
        Me.dgOtherMessages.Size = New System.Drawing.Size(700, 391)
        Me.dgOtherMessages.TabIndex = 3
        '
        'dgcolOtLanguage
        '
        Me.dgcolOtLanguage.Frozen = True
        Me.dgcolOtLanguage.HeaderText = "Message"
        Me.dgcolOtLanguage.Name = "dgcolOtLanguage"
        Me.dgcolOtLanguage.ReadOnly = True
        Me.dgcolOtLanguage.Width = 233
        '
        'dgcolOtLanguage1
        '
        Me.dgcolOtLanguage1.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.None
        Me.dgcolOtLanguage1.Frozen = True
        Me.dgcolOtLanguage1.HeaderText = "Message1"
        Me.dgcolOtLanguage1.Name = "dgcolOtLanguage1"
        Me.dgcolOtLanguage1.Width = 233
        '
        'dgcolOtLanguage2
        '
        Me.dgcolOtLanguage2.Frozen = True
        Me.dgcolOtLanguage2.HeaderText = "Message2"
        Me.dgcolOtLanguage2.Name = "dgcolOtLanguage2"
        Me.dgcolOtLanguage2.Width = 233
        '
        'dgcolOtMsgID
        '
        Me.dgcolOtMsgID.Frozen = True
        Me.dgcolOtMsgID.HeaderText = "ID"
        Me.dgcolOtMsgID.Name = "dgcolOtMsgID"
        Me.dgcolOtMsgID.ReadOnly = True
        Me.dgcolOtMsgID.Visible = False
        Me.dgcolOtMsgID.Width = 5
        '
        'dgcolOtModuleName
        '
        Me.dgcolOtModuleName.Frozen = True
        Me.dgcolOtModuleName.HeaderText = "Module Name"
        Me.dgcolOtModuleName.Name = "dgcolOtModuleName"
        Me.dgcolOtModuleName.ReadOnly = True
        Me.dgcolOtModuleName.Visible = False
        Me.dgcolOtModuleName.Width = 150
        '
        'dgcolOtApplication
        '
        Me.dgcolOtApplication.HeaderText = "Application"
        Me.dgcolOtApplication.Name = "dgcolOtApplication"
        Me.dgcolOtApplication.ReadOnly = True
        Me.dgcolOtApplication.Visible = False
        '
        'cboFormFilter
        '
        Me.cboFormFilter.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboFormFilter.FormattingEnabled = True
        Me.cboFormFilter.Location = New System.Drawing.Point(6, 7)
        Me.cboFormFilter.Margin = New System.Windows.Forms.Padding(3, 4, 3, 4)
        Me.cboFormFilter.Name = "cboFormFilter"
        Me.cboFormFilter.Size = New System.Drawing.Size(232, 21)
        Me.cboFormFilter.TabIndex = 19
        Me.cboFormFilter.Visible = False
        '
        'cboModuleFilter
        '
        Me.cboModuleFilter.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboModuleFilter.FormattingEnabled = True
        Me.cboModuleFilter.Location = New System.Drawing.Point(6, 7)
        Me.cboModuleFilter.Margin = New System.Windows.Forms.Padding(3, 4, 3, 4)
        Me.cboModuleFilter.Name = "cboModuleFilter"
        Me.cboModuleFilter.Size = New System.Drawing.Size(232, 21)
        Me.cboModuleFilter.TabIndex = 20
        Me.cboModuleFilter.Visible = False
        '
        'cboOtherModuleFilter
        '
        Me.cboOtherModuleFilter.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboOtherModuleFilter.FormattingEnabled = True
        Me.cboOtherModuleFilter.Location = New System.Drawing.Point(6, 7)
        Me.cboOtherModuleFilter.Margin = New System.Windows.Forms.Padding(3, 4, 3, 4)
        Me.cboOtherModuleFilter.Name = "cboOtherModuleFilter"
        Me.cboOtherModuleFilter.Size = New System.Drawing.Size(232, 21)
        Me.cboOtherModuleFilter.TabIndex = 20
        Me.cboOtherModuleFilter.Visible = False
        '
        'frmLanguage
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(736, 522)
        Me.Controls.Add(Me.pnlMain)
        Me.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle
        Me.KeyPreview = True
        Me.MaximizeBox = False
        Me.MinimizeBox = False
        Me.Name = "frmLanguage"
        Me.ShowInTaskbar = False
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Language"
        Me.pnlMain.ResumeLayout(False)
        Me.objefFooter.ResumeLayout(False)
        Me.tabcLanguage.ResumeLayout(False)
        Me.tabpCaption.ResumeLayout(False)
        CType(Me.dgLanguage, System.ComponentModel.ISupportInitialize).EndInit()
        Me.tabpMessages.ResumeLayout(False)
        CType(Me.dgMessage, System.ComponentModel.ISupportInitialize).EndInit()
        Me.tabpOthers.ResumeLayout(False)
        CType(Me.dgOtherMessages, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents btnSave As eZee.Common.eZeeLightButton
    Friend WithEvents btnCancel As eZee.Common.eZeeLightButton
    Friend WithEvents pnlMain As System.Windows.Forms.Panel
    Friend WithEvents tabcLanguage As System.Windows.Forms.TabControl
    Friend WithEvents tabpCaption As System.Windows.Forms.TabPage
    Friend WithEvents tabpMessages As System.Windows.Forms.TabPage
    Friend WithEvents dgLanguage As System.Windows.Forms.DataGridView
    Friend WithEvents dgMessage As System.Windows.Forms.DataGridView
    Friend WithEvents btnFindReplace As eZee.Common.eZeeLightButton
    Friend WithEvents tabpOthers As System.Windows.Forms.TabPage
    Friend WithEvents dgOtherMessages As System.Windows.Forms.DataGridView
    Friend WithEvents objefFooter As eZee.Common.eZeeFooter
    Friend WithEvents dgColLanguage As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents dgcollanguage1 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents dgcolLanguage2 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents dgcollangunkid As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents dgcolformname As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents dgcolcontrolname As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents dgcolApplication As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents dgcolMessage As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents dgcolMessage1 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents dgcolMessage2 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents dgcolMsgCode As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents dgcolMsgModuleName As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents dgcolMsgApplication As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents dgcolOtLanguage As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents dgcolOtLanguage1 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents dgcolOtLanguage2 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents dgcolOtMsgID As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents dgcolOtModuleName As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents dgcolOtApplication As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents cboFormFilter As System.Windows.Forms.ComboBox
    Friend WithEvents cboModuleFilter As System.Windows.Forms.ComboBox
    Friend WithEvents cboOtherModuleFilter As System.Windows.Forms.ComboBox
End Class
