﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmFieldSort
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmFieldSort))
        Me.pnlMain = New System.Windows.Forms.Panel
        Me.objbtnAscDesc = New eZee.Common.eZeeLightButton(Me.components)
        Me.lvListOrderBy = New eZee.Common.eZeeListView(Me.components)
        Me.colhCheck = New System.Windows.Forms.ColumnHeader
        Me.colhFieldName = New System.Windows.Forms.ColumnHeader
        Me.colhSorting = New System.Windows.Forms.ColumnHeader
        Me.objefFooter = New eZee.Common.eZeeFooter
        Me.btnCancel = New eZee.Common.eZeeLightButton(Me.components)
        Me.btnOk = New eZee.Common.eZeeLightButton(Me.components)
        Me.lblFieldsOrColumn = New System.Windows.Forms.Label
        Me.objbtnUp = New eZee.Common.eZeeLightButton(Me.components)
        Me.objbtnDown = New eZee.Common.eZeeLightButton(Me.components)
        Me.pnlMain.SuspendLayout()
        Me.objefFooter.SuspendLayout()
        Me.SuspendLayout()
        '
        'pnlMain
        '
        Me.pnlMain.Controls.Add(Me.objbtnAscDesc)
        Me.pnlMain.Controls.Add(Me.lvListOrderBy)
        Me.pnlMain.Controls.Add(Me.objefFooter)
        Me.pnlMain.Controls.Add(Me.lblFieldsOrColumn)
        Me.pnlMain.Controls.Add(Me.objbtnUp)
        Me.pnlMain.Controls.Add(Me.objbtnDown)
        Me.pnlMain.Dock = System.Windows.Forms.DockStyle.Fill
        Me.pnlMain.Location = New System.Drawing.Point(0, 0)
        Me.pnlMain.Name = "pnlMain"
        Me.pnlMain.Size = New System.Drawing.Size(274, 366)
        Me.pnlMain.TabIndex = 94
        '
        'objbtnAscDesc
        '
        Me.objbtnAscDesc.BackColor = System.Drawing.Color.White
        Me.objbtnAscDesc.BackgroundImage = CType(resources.GetObject("objbtnAscDesc.BackgroundImage"), System.Drawing.Image)
        Me.objbtnAscDesc.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.objbtnAscDesc.BorderColor = System.Drawing.Color.Empty
        Me.objbtnAscDesc.ButtonType = eZee.Common.eZeeLightButton.enButtonType.CheckButton
        Me.objbtnAscDesc.FlatAppearance.BorderSize = 0
        Me.objbtnAscDesc.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.objbtnAscDesc.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.objbtnAscDesc.ForeColor = System.Drawing.Color.Black
        Me.objbtnAscDesc.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.objbtnAscDesc.GradientForeColor = System.Drawing.Color.Black
        Me.objbtnAscDesc.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.objbtnAscDesc.HoverGradientForeColor = System.Drawing.Color.Black
        Me.objbtnAscDesc.Image = Global.Aruti.Data.My.Resources.Resources.Descending_24
        Me.objbtnAscDesc.Location = New System.Drawing.Point(229, 90)
        Me.objbtnAscDesc.Name = "objbtnAscDesc"
        Me.objbtnAscDesc.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.objbtnAscDesc.PressedGradientForeColor = System.Drawing.Color.Black
        Me.objbtnAscDesc.Size = New System.Drawing.Size(32, 30)
        Me.objbtnAscDesc.TabIndex = 96
        Me.objbtnAscDesc.UseVisualStyleBackColor = False
        '
        'lvListOrderBy
        '
        Me.lvListOrderBy.BackColorOnChecked = True
        Me.lvListOrderBy.CheckBoxes = True
        Me.lvListOrderBy.ColumnHeaders = Nothing
        Me.lvListOrderBy.Columns.AddRange(New System.Windows.Forms.ColumnHeader() {Me.colhCheck, Me.colhFieldName, Me.colhSorting})
        Me.lvListOrderBy.CompulsoryColumns = ""
        Me.lvListOrderBy.FullRowSelect = True
        Me.lvListOrderBy.GridLines = True
        Me.lvListOrderBy.GroupingColumn = Nothing
        Me.lvListOrderBy.HeaderStyle = System.Windows.Forms.ColumnHeaderStyle.Nonclickable
        Me.lvListOrderBy.HideSelection = False
        Me.lvListOrderBy.Location = New System.Drawing.Point(13, 25)
        Me.lvListOrderBy.MinColumnWidth = 50
        Me.lvListOrderBy.MultiSelect = False
        Me.lvListOrderBy.Name = "lvListOrderBy"
        Me.lvListOrderBy.OptionalColumns = ""
        Me.lvListOrderBy.ShowMoreItem = False
        Me.lvListOrderBy.ShowSaveItem = False
        Me.lvListOrderBy.ShowSelectAll = True
        Me.lvListOrderBy.ShowSizeAllColumnsToFit = True
        Me.lvListOrderBy.Size = New System.Drawing.Size(210, 260)
        Me.lvListOrderBy.Sortable = False
        Me.lvListOrderBy.TabIndex = 95
        Me.lvListOrderBy.UseCompatibleStateImageBehavior = False
        Me.lvListOrderBy.View = System.Windows.Forms.View.Details
        '
        'colhCheck
        '
        Me.colhCheck.Text = ""
        Me.colhCheck.Width = 30
        '
        'colhFieldName
        '
        Me.colhFieldName.Tag = "colhFieldName"
        Me.colhFieldName.Text = "Column Name"
        Me.colhFieldName.Width = 110
        '
        'colhSorting
        '
        Me.colhSorting.Tag = "colhSorting"
        Me.colhSorting.Text = "Sorting"
        Me.colhSorting.Width = 50
        '
        'objefFooter
        '
        Me.objefFooter.BackColor = System.Drawing.Color.Transparent
        Me.objefFooter.BorderColor = System.Drawing.Color.Silver
        Me.objefFooter.Controls.Add(Me.btnCancel)
        Me.objefFooter.Controls.Add(Me.btnOk)
        Me.objefFooter.Dock = System.Windows.Forms.DockStyle.Bottom
        Me.objefFooter.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.objefFooter.GradiantStyle = eZee.Common.eZeeFooter.GradientStyle.Up
        Me.objefFooter.GradientColor1 = System.Drawing.SystemColors.Control
        Me.objefFooter.GradientColor2 = System.Drawing.SystemColors.Control
        Me.objefFooter.Location = New System.Drawing.Point(0, 311)
        Me.objefFooter.Name = "objefFooter"
        Me.objefFooter.Size = New System.Drawing.Size(274, 55)
        Me.objefFooter.TabIndex = 92
        '
        'btnCancel
        '
        Me.btnCancel.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnCancel.BackColor = System.Drawing.Color.White
        Me.btnCancel.BackgroundImage = CType(resources.GetObject("btnCancel.BackgroundImage"), System.Drawing.Image)
        Me.btnCancel.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.btnCancel.BorderColor = System.Drawing.Color.Empty
        Me.btnCancel.ButtonType = eZee.Common.eZeeLightButton.enButtonType.NormalButton
        Me.btnCancel.DialogResult = System.Windows.Forms.DialogResult.Cancel
        Me.btnCancel.FlatAppearance.BorderSize = 0
        Me.btnCancel.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnCancel.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold)
        Me.btnCancel.ForeColor = System.Drawing.Color.Black
        Me.btnCancel.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.btnCancel.GradientForeColor = System.Drawing.Color.Black
        Me.btnCancel.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.btnCancel.HoverGradientForeColor = System.Drawing.Color.Black
        Me.btnCancel.Location = New System.Drawing.Point(172, 13)
        Me.btnCancel.Name = "btnCancel"
        Me.btnCancel.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.btnCancel.PressedGradientForeColor = System.Drawing.Color.Black
        Me.btnCancel.Size = New System.Drawing.Size(90, 30)
        Me.btnCancel.TabIndex = 89
        Me.btnCancel.Text = "Close"
        Me.btnCancel.UseVisualStyleBackColor = False
        '
        'btnOk
        '
        Me.btnOk.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnOk.BackColor = System.Drawing.Color.White
        Me.btnOk.BackgroundImage = CType(resources.GetObject("btnOk.BackgroundImage"), System.Drawing.Image)
        Me.btnOk.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.btnOk.BorderColor = System.Drawing.Color.Empty
        Me.btnOk.ButtonType = eZee.Common.eZeeLightButton.enButtonType.NormalButton
        Me.btnOk.FlatAppearance.BorderSize = 0
        Me.btnOk.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnOk.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold)
        Me.btnOk.ForeColor = System.Drawing.Color.Black
        Me.btnOk.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.btnOk.GradientForeColor = System.Drawing.Color.Black
        Me.btnOk.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.btnOk.HoverGradientForeColor = System.Drawing.Color.Black
        Me.btnOk.Location = New System.Drawing.Point(76, 13)
        Me.btnOk.Name = "btnOk"
        Me.btnOk.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.btnOk.PressedGradientForeColor = System.Drawing.Color.Black
        Me.btnOk.Size = New System.Drawing.Size(90, 30)
        Me.btnOk.TabIndex = 88
        Me.btnOk.Text = "OK"
        Me.btnOk.UseVisualStyleBackColor = False
        '
        'lblFieldsOrColumn
        '
        Me.lblFieldsOrColumn.BackColor = System.Drawing.Color.Transparent
        Me.lblFieldsOrColumn.Font = New System.Drawing.Font("Tahoma", 8.25!)
        Me.lblFieldsOrColumn.Location = New System.Drawing.Point(10, 9)
        Me.lblFieldsOrColumn.Name = "lblFieldsOrColumn"
        Me.lblFieldsOrColumn.Size = New System.Drawing.Size(214, 13)
        Me.lblFieldsOrColumn.TabIndex = 91
        Me.lblFieldsOrColumn.Text = "Fields/Column:"
        '
        'objbtnUp
        '
        Me.objbtnUp.BackColor = System.Drawing.Color.White
        Me.objbtnUp.BackgroundImage = CType(resources.GetObject("objbtnUp.BackgroundImage"), System.Drawing.Image)
        Me.objbtnUp.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.objbtnUp.BorderColor = System.Drawing.Color.Empty
        Me.objbtnUp.ButtonType = eZee.Common.eZeeLightButton.enButtonType.NormalButton
        Me.objbtnUp.FlatAppearance.BorderSize = 0
        Me.objbtnUp.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.objbtnUp.ForeColor = System.Drawing.Color.Black
        Me.objbtnUp.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.objbtnUp.GradientForeColor = System.Drawing.Color.Black
        Me.objbtnUp.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.objbtnUp.HoverGradientForeColor = System.Drawing.Color.Black
        Me.objbtnUp.Image = Global.Aruti.Data.My.Resources.Resources.MoveUp_16
        Me.objbtnUp.Location = New System.Drawing.Point(230, 25)
        Me.objbtnUp.Name = "objbtnUp"
        Me.objbtnUp.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.objbtnUp.PressedGradientForeColor = System.Drawing.Color.Black
        Me.objbtnUp.Size = New System.Drawing.Size(32, 30)
        Me.objbtnUp.TabIndex = 86
        Me.objbtnUp.UseVisualStyleBackColor = False
        '
        'objbtnDown
        '
        Me.objbtnDown.BackColor = System.Drawing.Color.White
        Me.objbtnDown.BackgroundImage = CType(resources.GetObject("objbtnDown.BackgroundImage"), System.Drawing.Image)
        Me.objbtnDown.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.objbtnDown.BorderColor = System.Drawing.Color.Empty
        Me.objbtnDown.ButtonType = eZee.Common.eZeeLightButton.enButtonType.NormalButton
        Me.objbtnDown.FlatAppearance.BorderSize = 0
        Me.objbtnDown.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.objbtnDown.ForeColor = System.Drawing.Color.Black
        Me.objbtnDown.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.objbtnDown.GradientForeColor = System.Drawing.Color.Black
        Me.objbtnDown.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.objbtnDown.HoverGradientForeColor = System.Drawing.Color.Black
        Me.objbtnDown.Image = Global.Aruti.Data.My.Resources.Resources.MoveDown_16
        Me.objbtnDown.Location = New System.Drawing.Point(230, 54)
        Me.objbtnDown.Name = "objbtnDown"
        Me.objbtnDown.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.objbtnDown.PressedGradientForeColor = System.Drawing.Color.Black
        Me.objbtnDown.Size = New System.Drawing.Size(32, 30)
        Me.objbtnDown.TabIndex = 87
        Me.objbtnDown.UseVisualStyleBackColor = False
        '
        'frmFieldSort
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(274, 366)
        Me.Controls.Add(Me.pnlMain)
        Me.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle
        Me.KeyPreview = True
        Me.MaximizeBox = False
        Me.MinimizeBox = False
        Me.Name = "frmFieldSort"
        Me.ShowInTaskbar = False
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent
        Me.Text = "Field List To Sort"
        Me.pnlMain.ResumeLayout(False)
        Me.objefFooter.ResumeLayout(False)
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents pnlMain As System.Windows.Forms.Panel
    Friend WithEvents objefFooter As eZee.Common.eZeeFooter
    Friend WithEvents btnCancel As eZee.Common.eZeeLightButton
    Friend WithEvents btnOk As eZee.Common.eZeeLightButton
    Friend WithEvents lblFieldsOrColumn As System.Windows.Forms.Label
    Friend WithEvents objbtnUp As eZee.Common.eZeeLightButton
    Friend WithEvents objbtnDown As eZee.Common.eZeeLightButton
    Friend WithEvents lvListOrderBy As eZee.Common.eZeeListView
    Friend WithEvents colhCheck As System.Windows.Forms.ColumnHeader
    Friend WithEvents colhFieldName As System.Windows.Forms.ColumnHeader
    Friend WithEvents colhSorting As System.Windows.Forms.ColumnHeader
    Friend WithEvents objbtnAscDesc As eZee.Common.eZeeLightButton
End Class
