﻿Option Strict On

Imports eZeeCommonLib
Imports Aruti.Data
Imports Aruti.Data.Language

Public Class frmCommonSelection
    
#Region " Private Variables "
    Private mstrModuleName As String = "frmCommonSelection"
    Private mintCompanyUnkid As Integer = 0
    Private mblnCancel As Boolean = True
    Private objCompany As clsCompany_Master
    Private mblnIsFinancialYear As Boolean = False
    Private mblnIsFromTimesheet As Boolean = False
    Private mintYearUnkid As Integer = 0
#End Region

#Region " Properties "
    Public Property _IsFinancial_Year() As Boolean
        Get
            Return mblnIsFinancialYear
        End Get
        Set(ByVal value As Boolean)
            mblnIsFinancialYear = value
        End Set
    End Property

    Public Property _IsFromTimesheet() As Boolean
        Get
            Return mblnIsFromTimesheet
        End Get
        Set(ByVal value As Boolean)
            mblnIsFromTimesheet = value
        End Set
    End Property

#End Region

#Region " Display Dialog "
    Public Function displayDialog() As Boolean
        Try
            Me.ShowDialog()
            Return Not mblnCancel
        Catch ex As Exception
            Call DisplayError.Show("-1", ex.Message, "displayDialog", mstrModuleName)
        End Try
    End Function
#End Region

#Region " Form's Events "
    Private Sub frmCommonSelection_FormClosed(ByVal sender As Object, ByVal e As System.Windows.Forms.FormClosedEventArgs) Handles Me.FormClosed
        objCompany = Nothing
    End Sub

    Private Sub frmCommonSelection_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles Me.KeyPress
        If Asc(e.KeyChar) = 13 Then
            Call btnOpen.PerformClick()
        ElseIf Asc(e.KeyChar) = 27 Then
            Me.Close()
        End If
    End Sub

    Private Sub frmCommonSelection_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            Call Set_Logo(Me, enArutiApplicatinType.Aruti_Payroll)
            objCompany = New clsCompany_Master
            If mblnIsFinancialYear = True Then
                lvCompany.Visible = False
                lvDatabase.Visible = True
                chkDoNotNextTime.Visible = False
                Me.Text = Language.getMessage(mstrModuleName, 3, "Select Database")
                Me.eZeeHeader.Title = Language.getMessage(mstrModuleName, 3, "Select Database")
                Me.picSideImage.Image = My.Resources.select_db
                Call FillYearList()
            Else
                Me.picSideImage.Image = My.Resources.company
                lvCompany.Visible = True
                lvDatabase.Visible = False
                Call FillList()
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmCommonSelection_Load", mstrModuleName)
        End Try
    End Sub
#End Region

#Region " Button's Events "
    Private Sub btnOpen_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnOpen.Click
        If mblnIsFinancialYear = False Then
            If lvCompany.SelectedItems.Count <= 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 1, "Please select a Company from the list to work."), enMsgBoxStyle.Information)
                Exit Sub
            End If
        Else
            If lvDatabase.SelectedItems.Count <= 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 2, "Please select a Database from the list to work."), enMsgBoxStyle.Information)
                Exit Sub
            End If
        End If

        Dim objTempCompany As New clsCompany_Master
        Try
            If mblnIsFinancialYear = False Then
                mintCompanyUnkid = CInt(lvCompany.SelectedItems(0).Tag)

                objTempCompany._Companyunkid = mintCompanyUnkid

                If mintCompanyUnkid > 0 Then
                    Company._Object._Companyunkid = mintCompanyUnkid

                    ConfigParameter._Object.Refresh()
                    ConfigParameter._Object._Companyunkid = mintCompanyUnkid

                    mblnCancel = False
                    Me.Close()
                    frmSplash.Hide()
                End If
            Else
                mintYearUnkid = CInt(lvDatabase.SelectedItems(0).Tag)
                objTempCompany._YearUnkid = mintYearUnkid
                If mintYearUnkid > 0 Then
                    FinancialYear._Object._YearUnkid = mintYearUnkid

                    Dim objMaster As New clsMasterData

                    'S.SANDEEP [04 JUN 2015] -- START
                    'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
                    'UserAccessLevel._AccessLevel = objMaster.GetUserAccessLevel(1, ConfigParameter._Object._Companyunkid, mintYearUnkid, "")
                    'S.SANDEEP [04 JUN 2015] -- END


                    mblnCancel = False
                    Me.Close()
                    frmSplash.Hide()
                End If
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "btnOpen_Click", mstrModuleName)
        Finally
            objTempCompany = Nothing
        End Try
    End Sub

    Private Sub btnClose_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnCancel.Click
        Try
            mblnCancel = True
            Me.Close()
        Catch ex As Exception
            Call DisplayError.Show("-1", ex.Message, "btnClose_Click", mstrModuleName)
        End Try
    End Sub
#End Region

#Region " Private Function "
    Private Sub FillList()
        Dim dsList As New DataSet
        Dim lvItem As ListViewItem
        Try


            If mblnIsFromTimesheet Then

                'Pinkal (15-MAY-2012) -- Start
                'Enhancement : TRA Changes
                'dsList = objCompany.GetList("Company", False)
                dsList = objCompany.GetList("Company", True)
                'Pinkal (15-MAY-2012) -- End


            Else
            dsList = objCompany.GetCompanyAcessList(User._Object._Userunkid, "Company")
            End If


        lvCompany.Items.Clear()
            For Each dtRow As DataRow In dsList.Tables("Company").Rows
                lvItem = New ListViewItem

                lvItem.Text = dtRow("name").ToString
                lvItem.Tag = dtRow("companyunkid").ToString

            lvCompany.Items.Add(lvItem)

                lvItem = Nothing
            Next
            'Sohail (02 Nov 2013) -- Start
            'TRA - ENHANCEMENT
            If lvCompany.Items.Count > 9 Then
                colhProperty.Width = colhProperty.Width - 15
            Else
                colhProperty.Width = colhProperty.Width
            End If
            'Sohail (02 Nov 2013) -- End
            lvCompany.Select()
            If lvCompany.Items.Count > 0 Then lvCompany.Items(0).Selected = True


        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "FillList", mstrModuleName)
        End Try
    End Sub

    Private Sub FillYearList()
        Dim dsList As New DataSet
        Dim lvItem As ListViewItem
        Try


            'Sohail (02 Nov 2013) -- Start
            'TRA - ENHANCEMENT
            'If mblnIsFromTimesheet Then
            '    dsList = objCompany.GetFinancialYearList(Company._Object._Companyunkid, )
            'Else
            '    dsList = objCompany.GetFinancialYearList(Company._Object._Companyunkid, User._Object._Userunkid, "List")
            'End If
            dsList = objCompany.GetFinancialYearList(Company._Object._Companyunkid, 1, "List")
            'Sohail (02 Nov 2013) -- End

            lvDatabase.Items.Clear()
            For Each dtRow As DataRow In dsList.Tables("List").Rows
                If CBool(dtRow.Item("isclosed")) = True Then Continue For 'Sohail (02 Nov 2013)

                lvItem = New ListViewItem

                lvItem.Text = dtRow.Item("database_name").ToString
                lvItem.SubItems.Add(dtRow.Item("CompanyName").ToString)
                lvItem.Tag = dtRow.Item("yearunkid")
                'Sandeep ( 18 JAN 2011 ) -- START
                If CBool(dtRow.Item("isclosed")) = True Then lvItem.ForeColor = Color.Gray
                'Sandeep ( 18 JAN 2011 ) -- END 
                lvDatabase.Items.Add(lvItem)

            lvItem = Nothing
        Next
            lvDatabase.GroupingColumn = objcolCompany
            objcolCompany.Width = 0
            lvDatabase.DisplayGroups(True)

            'Sohail (02 Nov 2013) -- Start
            'TRA - ENHANCEMENT
            If lvDatabase.Items.Count > 9 Then
                colhDatabase.Width = colhDatabase.Width - 15
            Else
                colhDatabase.Width = colhDatabase.Width
            End If
            'Sohail (02 Nov 2013) -- End

            lvDatabase.Select()
            If lvDatabase.Items.Count > 0 Then lvDatabase.Items(0).Selected = True
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "FillYearList", mstrModuleName)
        End Try
    End Sub
#End Region

	'<Language> This Auto Generated Text Please Do Not Modify it.
#Region " Language & UI Settings "
	Private Sub OtherSettings()
		Try
			Me.SuspendLayout()
			
			Call SetLanguage()
			
			
			Me.eZeeHeader.GradientColor1 = GUI._HeaderBackColor1 
			Me.eZeeHeader.GradientColor2 = GUI._HeaderBackColor2 
			Me.eZeeHeader.BorderColor = GUI._HeaderBorderColor 
			Me.eZeeHeader.HeaderTextForeColor = GUI._HeaderTitleFontColor 
			Me.eZeeHeader.DescriptionForeColor = GUI._HeaderMessageFontColor 


			Me.btnCancel.GradientBackColor = GUI._ButttonBackColor 
			Me.btnCancel.GradientForeColor = GUI._ButttonFontColor

			Me.btnOpen.GradientBackColor = GUI._ButttonBackColor 
			Me.btnOpen.GradientForeColor = GUI._ButttonFontColor


			Me.ResumeLayout()
		Catch Ex As Exception
			DisplayError.Show("-1", ex.Message, "OtherSettings", mstrModuleName)
		End Try
	End Sub
			
			
	Private Sub SetLanguage()
		Try
			Me.Text = Language._Object.getCaption(Me.Name, Me.Text)
			
			Me.eZeeHeader.Title = Language._Object.getCaption(Me.eZeeHeader.Name & "_Title" , Me.eZeeHeader.Title)
			Me.eZeeHeader.Message = Language._Object.getCaption(Me.eZeeHeader.Name & "_Message" , Me.eZeeHeader.Message)
			Me.btnCancel.Text = Language._Object.getCaption(Me.btnCancel.Name, Me.btnCancel.Text)
			Me.btnOpen.Text = Language._Object.getCaption(Me.btnOpen.Name, Me.btnOpen.Text)
			Me.colhProperty.Text = Language._Object.getCaption(CStr(Me.colhProperty.Tag), Me.colhProperty.Text)
			Me.chkDoNotNextTime.Text = Language._Object.getCaption(Me.chkDoNotNextTime.Name, Me.chkDoNotNextTime.Text)
			Me.colhDatabase.Text = Language._Object.getCaption(CStr(Me.colhDatabase.Tag), Me.colhDatabase.Text)

		Catch Ex As Exception
			DisplayError.Show("-1", ex.Message, "SetLanguage", mstrModuleName)
		End Try
	End Sub
			
			
	Private Sub SetMessages()
		Try
			Language.setMessage(mstrModuleName, 1, "Please select a Company from the list to work.")
			Language.setMessage(mstrModuleName, 2, "Please select a Database from the list to work.")
			Language.setMessage(mstrModuleName, 3, "Select Database")

		Catch Ex As Exception
			DisplayError.Show("-1", ex.Message, "SetMessages", mstrModuleName)
		End Try
	End Sub
#End Region 'Language & UI Settings
	'</Language>
End Class