﻿Imports eZeeCommonLib
Imports Aruti.Data

Public Class frmDatabaseSelection

    Private Sub frmDatabaseSelection_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Call Set_Logo(Me, enArutiApplicatinType.Aruti_Payroll)
        lvDatabase.Items.Clear()
        For i As Integer = 1 To 2
            Dim lvItem As New ListViewItem
            Select Case i
                Case 1
                    lvItem.Text = "eZee Demo"
                Case 2
                    lvItem.Text = "eZee Demo1"
            End Select
            lvDatabase.Items.Add(lvItem)
            lvItem = Nothing
        Next
    End Sub
End Class