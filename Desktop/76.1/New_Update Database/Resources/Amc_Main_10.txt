<?xml version="1.0" standalone="yes"?>
<Data>
<xs:schema id="Data" xmlns="" xmlns:xs="http://www.w3.org/2001/XMLSchema" xmlns:msdata="urn:schemas-microsoft-com:xml-msdata">
<xs:element name="Data" msdata:IsDataSet="true" msdata:UseCurrentLocale="true">
<xs:complexType>
<xs:choice minOccurs="0" maxOccurs="unbounded">
<xs:element name="Data">
<xs:complexType>
<xs:sequence>
<xs:element name="Id" type="xs:int" minOccurs="0" />
<xs:element name="Version" type="xs:string" minOccurs="0" />
<xs:element name="CheckType" type="xs:int" minOccurs="0" />
<xs:element name="ConditionValue1" type="xs:string" minOccurs="0" />
<xs:element name="ConditionValue2" type="xs:string" minOccurs="0" />
<xs:element name="Script" type="xs:string" minOccurs="0" />
</xs:sequence>
</xs:complexType>
</xs:element>
</xs:choice>
</xs:complexType>
</xs:element>
</xs:schema>
<Data>
<Id>1</Id>
<Version>9.0.76.1</Version>
<CheckType>90761</CheckType>
<ConditionValue1>9</ConditionValue1>
<ConditionValue2 />
<Script>
<![CDATA[
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[amsmc]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[amsmc](
	[activationcode] [nvarchar](max) NULL,
	[lastused] [datetime] NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
END
]]>
</Script>
</Data>
</Data>