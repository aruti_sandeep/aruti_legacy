﻿#Region " Import "

Imports System
Imports System.IO
Imports System.Timers
Imports Aruti.Data
Imports System.Threading
Imports Suprema
Imports System.Runtime.InteropServices
Imports System.Text
Imports eZeeCommonLib
Imports System.Globalization

#End Region


Public Class ArutiAttendanceTracker

#Region "Private Variable"
    Private timer As New System.Timers.Timer
    Dim strIp As String = ""
    Dim intPort As Integer = 0
    Dim context As IntPtr = IntPtr.Zero
    Dim mintMachineSrNo As Integer = 0
    Dim mstrDeviceCode As String = ""
    Dim intDeviceType As Integer = -1
    Dim mstrCommunicationKey As String = ""
    Private mstrUserID As String = ""
    Private mstrPassword As String = ""
    Private mstrDeviceModel As String = ""
    Dim t As Thread
    Dim xCompanyId As Integer = 0
    Dim xYearId As Integer = 0
    Dim cbOnLogReceived As API.OnLogReceived = Nothing
    Dim cbOnLogReceivedEx As API.OnLogReceivedEx = Nothing
    Dim cbOnDeviceDisconnected As API.OnDeviceDisconnected = Nothing
    Dim dctConnectedDevice As Dictionary(Of String, String)
    Dim dctDisconnectedDevice As Dictionary(Of String, String)
    Dim mblnFirstCheckInLastCheckOut As Boolean = False
    Dim mstrUserAccessModeSetting As String = enAllocation.DEPARTMENT
    Dim mblnIsIncludeInactiveEmp As Boolean = False
    Dim mblnPolicyManagementTNA As Boolean = False
    Dim mblnDonotAttendanceinSeconds As Boolean = False
    Dim mblnIsHolidayConsiderOnWeekend As Boolean = True
    Dim mblnIsDayOffConsiderOnWeekend As Boolean = True
    Dim mblnIsHolidayConsiderOnDayoff As Boolean = True
    Dim mstrTnAFailureNotificationUserIds As String = ""
    Dim mstrArutisignature As String = ""
#End Region

#Region "Enum"

    Public Enum enFingerPrintDevice
        None = 0
        DigitalPersona = 1
        SecuGen = 2
        ZKSoftware = 3
        BioStar = 4
        ZKAcessControl = 5
        Anviz = 6
        ACTAtek = 7
        Handpunch = 8
        SAGEM = 9
        BioStar2 = 10
        FingerTec = 11
        NewAnviz = 12
    End Enum

    Public Enum enInOutSource
        LoginPassword = 1
        FingerPrint = 2
        Swipecard = 3
        Import = 4
        Manual = 5
        GroupLogin = 6
        GlobalAdd = 7
        GlobalEdit = 8
        GlobalDelete = 9
    End Enum

    Public Enum enAllocation
        BRANCH = 1
        DEPARTMENT_GROUP = 2
        DEPARTMENT = 3
        SECTION_GROUP = 4
        SECTION = 5
        UNIT_GROUP = 6
        UNIT = 7
        TEAM = 8
        JOB_GROUP = 9
        JOBS = 10
        EMPLOYEE = 11
        GENERAL = 12
        CLASS_GROUP = 13
        CLASSES = 14
        COST_CENTER = 15
        EMPLOYEE_GRADES = 16
    End Enum

#End Region

#Region "Service Method"

    Protected Overrides Sub OnStart(ByVal args() As String)
        Try
            System.Diagnostics.Debugger.Launch()
            AddHandler timer.Elapsed, AddressOf OnElapsedTime
            timer.Interval = 60000   '2 Mins
            timer.Enabled = True
            dctConnectedDevice = New Dictionary(Of String, String)
            dctDisconnectedDevice = New Dictionary(Of String, String)
        Catch ex As Exception
            WriteLog("OnStart:- " & ex.Message)
        End Try
    End Sub

    Protected Overrides Sub OnStop()
        Try
            timer.Enabled = False
        Catch ex As Exception
            WriteLog("OnStop:- " & ex.Message)
        End Try
    End Sub

    Private Sub OnElapsedTime(ByVal source As Object, ByVal e As ElapsedEventArgs)
        Dim objMain As New clsMain
        Try
            System.Diagnostics.Debugger.Launch()
            timer.Enabled = False

            If objMain.connect() = False Then
                WriteLog("OnElapsedTime :  Not able to connect with database.")
                Exit Sub
            End If

            Dim objFinancial As New clsCompany_Master
            Dim dtCompDB As DataTable = objFinancial.Get_DataBaseList("List", False, True, False)

            dtCompDB = New DataView(dtCompDB, "YearId > 0", "GrpId", DataViewRowState.CurrentRows).ToTable()

            If dtCompDB IsNot Nothing AndAlso dtCompDB.Rows.Count > 0 Then

                For Each drComp As DataRow In dtCompDB.Rows
                    xCompanyId = Convert.ToInt32(drComp("GrpId"))  'GrpId = CompanyId
                    xYearId = Convert.ToInt32(drComp("YearId"))

                    Dim objconfig As New clsConfigOptions
                    mblnFirstCheckInLastCheckOut = objconfig.GetKeyValue(xCompanyId, "FirstCheckInLastCheckOut")
                    mstrUserAccessModeSetting = objconfig.GetKeyValue(xCompanyId, "UserAccessModeSetting")
                    mblnIsIncludeInactiveEmp = objconfig.GetKeyValue(xCompanyId, "IncludeInactiveEmployee")
                    mblnPolicyManagementTNA = objconfig.GetKeyValue(xCompanyId, "PolicyManagementTNA")
                    mblnDonotAttendanceinSeconds = objconfig.GetKeyValue(xCompanyId, "DonotAttendanceinSeconds")
                    mblnIsHolidayConsiderOnWeekend = objconfig.GetKeyValue(xCompanyId, "IsHolidayConsiderOnWeekend")
                    mblnIsDayOffConsiderOnWeekend = objconfig.GetKeyValue(xCompanyId, "IsDayOffConsiderOnWeekend")
                    mblnIsHolidayConsiderOnDayoff = objconfig.GetKeyValue(xCompanyId, "IsHolidayConsiderOnDayoff")
                    mstrTnAFailureNotificationUserIds = objconfig.GetKeyValue(xCompanyId, "TnAFailureNotificationUserIds")
                    mstrArutisignature = objconfig.GetKeyValue(xCompanyId, "ShowArutisignature")
                    objconfig = Nothing

                    If eZeeDatabase.current_database.ToString().Trim() <> drComp("Database").ToString().Trim() Then
                        eZeeDatabase.change_database(drComp("Database").ToString().Trim())  'CHANGE THE DATABASE
                        gobjLocalization = New clsLocalization(enArutiApplicatinType.Aruti_Payroll)
                        gobjLocalization._LangId = 1
                    End If

                    Dim path As String = AppDomain.CurrentDomain.BaseDirectory + "\DeviceSetting.xml"

                    If System.IO.File.Exists(path) Then
                        Dim dsMachineSetting As New DataSet()
                        dsMachineSetting.ReadXml(path)

                        If IsDBNull(dsMachineSetting) = False AndAlso dsMachineSetting.Tables(0).Rows.Count > 0 Then

                            Dim drMachineDetail = dsMachineSetting.Tables(0).AsEnumerable().Where(Function(x) x.Field(Of String)("companyunkid") = xCompanyId.ToString()).ToList()

                            If drMachineDetail IsNot Nothing AndAlso drMachineDetail.Count() > 0 Then

                                If dctDisconnectedDevice.Count <= 0 AndAlso dctConnectedDevice.Count <= 0 Then

                                    For i As Integer = 0 To drMachineDetail.Count - 1

                                        intDeviceType = CInt(drMachineDetail(i)("commdeviceid").ToString())

                                        mstrDeviceCode = CStr(drMachineDetail(i)("devicecode").ToString()).Trim.Remove(drMachineDetail(i)("devicecode").ToString().IndexOf("||"), drMachineDetail(i)("devicecode").ToString().Trim.Length - drMachineDetail(i)("devicecode").ToString().IndexOf("||"))

                                        strIp = drMachineDetail(i)("ip").ToString()

                                        If IsDBNull(drMachineDetail(i)("port")) = False AndAlso drMachineDetail(i)("port").ToString() <> "" Then
                                            intPort = CInt(drMachineDetail(i)("port").ToString())
                                        End If

                                        mintMachineSrNo = IIf(drMachineDetail(i)("machinesrno").ToString().Trim.Length <= 0, 0, CInt(drMachineDetail(i)("machinesrno").ToString()))

                                        If dsMachineSetting.Tables(0).Columns.Contains("commkey") Then
                                            If IsDBNull(drMachineDetail(i)("commkey")) = False AndAlso drMachineDetail(i)("commkey").ToString.Trim.Length > 0 Then
                                                mstrCommunicationKey = drMachineDetail(i)("commkey").ToString()
                                            End If
                                        End If

                                        If dsMachineSetting.Tables(0).Columns.Contains("userid") Then
                                            If IsDBNull(drMachineDetail(i)("userid")) = False AndAlso drMachineDetail(i)("userid").ToString.Trim.Length > 0 Then
                                                mstrUserID = drMachineDetail(i)("userid").ToString()
                                            End If
                                        End If

                                        If dsMachineSetting.Tables(0).Columns.Contains("password") Then
                                            If IsDBNull(drMachineDetail(i)("password")) = False AndAlso drMachineDetail(i)("password").ToString.Trim.Length > 0 Then
                                                mstrPassword = drMachineDetail(i)("password").ToString()
                                            End If
                                        End If

                                        If dsMachineSetting.Tables(0).Columns.Contains("devicemodel") Then
                                            If IsDBNull(drMachineDetail(i)("devicemodel")) = False AndAlso drMachineDetail(i)("devicemodel").ToString.Trim.Length > 0 Then
                                                mstrDeviceModel = drMachineDetail(i)("devicemodel").ToString()
                                            End If
                                        End If

                                        If intDeviceType <> enFingerPrintDevice.Handpunch AndAlso intDeviceType <> enFingerPrintDevice.SAGEM Then
                                            Dim Parameters = New Object() {drComp("Database").ToString().Trim(), xCompanyId, mstrDeviceCode, xYearId, strIp}
                                            t = New Thread(AddressOf DeviceIntegration)
                                            t.Start(Parameters)
                                            t.Join()

                                        ElseIf intDeviceType = enFingerPrintDevice.Handpunch Then
                                            'DownloadHandpunch_Data()
                                            Exit For

                                        ElseIf intDeviceType = enFingerPrintDevice.SAGEM Then
                                            'DownloadSAGEM_Data()
                                            Exit For

                                        End If

                                    Next  '  For Each dr As DataRow In dsMachineSetting.Tables(0).Rows

                                ElseIf dctDisconnectedDevice.Count > 0 Then  'FOR DEVICE WHICH IS DISCONNECTED.
                                    Dim xCount As Integer = dctDisconnectedDevice.Count - 1
                                    'For i As Integer = 0 To xCount
                                    '    Dim Parameters = New Object() {dctDisconnectedDevice.Keys(i).ToString(), dctDisconnectedDevice(dctDisconnectedDevice.Keys(i).ToString()).ToString()}
                                    '    t = New Thread(AddressOf ConnectBioStar2WithDeviceID)
                                    '    t.Start(Parameters)
                                    '    t.Join()
                                    'Next
                                    For i As Integer = 0 To xCount
                                        If dctDisconnectedDevice(dctDisconnectedDevice.Keys(i).ToString()).ToString().Trim.Length > 0 Then
                                            Dim mstrDatabaseName As String = ""
                                            Dim ar() As String = dctDisconnectedDevice(dctDisconnectedDevice.Keys(i).ToString()).ToString().ToString().Split(CChar("|"))
                                            If ar IsNot Nothing AndAlso ar.Length > 0 Then
                                                strIp = ar(0).ToString()
                                                xCompanyId = CInt(ar(1))
                                                mstrDeviceCode = ar(2).ToString()
                                                mstrDatabaseName = ar(3).ToString()
                                                xYearId = ar(4).ToString()
                                            End If
                                            Dim Parameters = New Object() {mstrDatabaseName.Trim(), xCompanyId, mstrDeviceCode, xYearId, strIp}
                                            t = New Thread(AddressOf DeviceIntegration)
                                            t.Start(Parameters)
                                            t.Join()
                                        End If
                                    Next

                                End If  ' If dctDisconnectedDevice.Count <= 0 Then

                            End If  '      If drMachineDetail IsNot Nothing AndAlso drMachineDetail.Count() > 0 Then

                        End If  '   If IsDBNull(dsMachineSetting) = False AndAlso dsMachineSetting.Tables(0).Rows.Count > 0 Then

                    Else
                        Dim mstrError As String = "Device setting file does not exist on application path.Please put device setting file on application folder."
                        If mstrTnAFailureNotificationUserIds.Trim.Length > 0 Then
                            SendFailedNotificationToUser(xCompanyId, "", "", mstrError)
                            Continue For
                        End If 'If mstrTnAFailureNotificationUserIds.Trim.Length > 0 Then

                    End If   '   If System.IO.File.Exists(path) Then

                Next  '  For Each drComp As DataRow In dtCompDB.Rows

            End If  '   If dtCompDB IsNot Nothing AndAlso dtCompDB.Rows.Count > 0 Then


        Catch ex As Exception
            WriteLog("OnElapsedTime:- " & ex.Message)
        Finally
            objMain = Nothing
            timer.Start()
        End Try
    End Sub

#End Region

#Region "Private Method"

    Public Sub DeviceIntegration(ByVal objParameters As Object())
        Try
            Dim xDatabaseName As String = ""
            Dim xDeviceCode As String = ""
            Dim xCompanyId As Integer = 0
            Dim objSendmail As clsSendMail = Nothing
            Dim xYearId As Integer = 0
            Dim mstrIP As String = ""

            If objParameters IsNot Nothing Then
                xDatabaseName = objParameters(0).ToString()
                xCompanyId = CInt(objParameters(1))
                xDeviceCode = objParameters(2).ToString()
                xYearId = objParameters(3).ToString()
                mstrIP = objParameters(4).ToString()
            End If


            If intDeviceType = Convert.ToInt16(enFingerPrintDevice.ZKSoftware) Then
                'DownloadZkData()    ;

            ElseIf intDeviceType = Convert.ToInt16(enFingerPrintDevice.BioStar) Then
                'DownloadBioStarData();    

            ElseIf intDeviceType = Convert.ToInt16(enFingerPrintDevice.ZKAcessControl) Then
                'DownloadZkAccessControlData()    ;

            ElseIf intDeviceType = Convert.ToInt16(enFingerPrintDevice.Anviz) Then
                'DownloadAnvizData(xDatabaseName.ToString(), CInt(xCompanyId), objSendmail)

            ElseIf intDeviceType = Convert.ToInt16(enFingerPrintDevice.ACTAtek) Then
                'DownloadACTAtekData();    

            ElseIf intDeviceType = Convert.ToInt16(enFingerPrintDevice.BioStar2) Then
                ConnectionBioStar2(xDatabaseName, xCompanyId, xDeviceCode, xYearId, mstrIP)

            ElseIf intDeviceType = Convert.ToInt16(enFingerPrintDevice.FingerTec) Then
                'DownloadFingerTecData();   

            End If
        Catch ex As Exception
            WriteLog("DeviceIntegration " & Date.Now & " : " & ex.Message)
        End Try
    End Sub

    Private Sub ConnectionBioStar2(ByVal xDatabaseName As String, ByVal xCompanyId As Integer, ByVal xDeviceCode As String, ByVal xYearId As Integer, ByVal mstrIp As String)
        Dim objBiostar2 As ArutiBS2.BS2Integration
        Dim strErrorMsg As String = ""
        Try
            Dim result As BS2ErrorCode
            Dim xPort As UShort = 0
            Dim xDeviceID As UInteger = 0
            Dim ptrIPAddr As IntPtr = Marshal.StringToHGlobalAnsi(mstrIp)

            If xPort <= 0 Then
                xPort = BS2Envirionment.BS2_TCP_DEVICE_PORT_DEFAULT
            End If


            If context = IntPtr.Zero Then
                context = API.BS2_AllocateContext()
                If context = IntPtr.Zero Then
                    WriteLog("Context Failed For Device IP:- " & mstrIp)
                    Exit Sub
                End If
            End If

            result = API.BS2_Initialize(context)
            If result <> BS2ErrorCode.BS_SDK_SUCCESS Then
                WriteLog("BS2_Initialization Failed Device IP :- " & mstrIp & " Error : " + result.ToString())
                Exit Sub
            End If

            result = API.BS2_ConnectDeviceViaIP(context, ptrIPAddr, xPort, xDeviceID)
            If result <> BS2ErrorCode.BS_SDK_SUCCESS Then
                WriteLog("BS2_ConnectDeviceViaIP Failed Device :- " & xDeviceCode & " Error : " + result.ToString())
                Exit Sub
            End If

            If cbOnLogReceived Is Nothing OrElse IsDBNull(cbOnLogReceived) Then
                cbOnLogReceived = New API.OnLogReceived(AddressOf RealtimeLogReceived)
            End If

            If cbOnDeviceDisconnected Is Nothing OrElse IsDBNull(cbOnDeviceDisconnected) Then
                cbOnDeviceDisconnected = New API.OnDeviceDisconnected(AddressOf DeviceDisconnected)
            End If

            API.BS2_SetDeviceEventListener(context, Nothing, Nothing, Nothing, cbOnDeviceDisconnected)

            result = API.BS2_StartMonitoringLog(context, xDeviceID, cbOnLogReceived)
            If result <> BS2ErrorCode.BS_SDK_SUCCESS Then
                WriteLog("MonitoringLog Failed Device IP :- " & mstrIp & " Error : " + result.ToString())
                Exit Sub
            End If

            If dctConnectedDevice.ContainsKey(xDeviceID.ToString()) = False Then
                dctConnectedDevice.Add(xDeviceID.ToString(), mstrIp & "|" & xCompanyId & "|" & xDeviceCode & "|" & xDatabaseName & "|" & xYearId)
                If dctDisconnectedDevice.ContainsKey(xDeviceID) Then
                    dctDisconnectedDevice.Remove(xDeviceID)
                End If
            End If

        Catch ex As Exception
            WriteLog("ConnectionBioStar2 Device IP :- " & mstrIp & " Error : " & ex.Message)
        Finally
            objBiostar2 = Nothing
        End Try
    End Sub

    'Private Sub ConnectBioStar2WithDeviceID(ByVal objParameters As Object())
    '    Dim objBiostar2 As ArutiBS2.BS2Integration
    '    Dim strErrorMsg As String = ""
    '    Dim mstrIP As String = ""
    '    Dim iCompanyId As Integer = 0
    '    Dim iYearId As Integer = 0
    '    Dim mstrDatabaseName As String = ""
    '    Dim mstrValue As String = ""
    '    Dim mstrDeviceId As String = ""
    '    Dim result As BS2ErrorCode
    '    Dim xPort As UShort = 0
    '    Dim ptrIPAddr As IntPtr = IntPtr.Zero
    '    Try

    '        If objParameters IsNot Nothing Then
    '            mstrDeviceId = objParameters(0).ToString()
    '            mstrValue = objParameters(1).ToString()
    '        End If

    '        If mstrValue.Trim.Length > 0 Then
    '            Dim ar() As String = mstrValue.ToString().Split(CChar("|"))
    '            If ar IsNot Nothing AndAlso ar.Length > 0 Then
    '                mstrIP = ar(0).ToString()
    '                iCompanyId = CInt(ar(1))
    '                mstrDeviceCode = ar(2).ToString()
    '                mstrDatabaseName = ar(3).ToString()
    '                iYearId = ar(4).ToString()
    '                ptrIPAddr = Marshal.StringToHGlobalAnsi(mstrIP)
    '            End If

    '            If xPort <= 0 Then
    '                xPort = BS2Envirionment.BS2_TCP_DEVICE_PORT_DEFAULT
    '            End If

    '            If context = IntPtr.Zero Then
    '                context = API.BS2_AllocateContext()
    '                If context = IntPtr.Zero Then
    '                    WriteLog("Context Failed For Device IP:- " & mstrIP)
    '                    Exit Sub
    '                End If
    '            End If

    '            result = API.BS2_Initialize(context)
    '            If result <> BS2ErrorCode.BS_SDK_SUCCESS Then
    '                WriteLog("BS2_Initialization Failed Device IP :- " & mstrIP & " Error : " + result.ToString())
    '                Exit Sub
    '            End If


    '            Dim xDeviceId As UInteger = 0
    '            result = API.BS2_ConnectDeviceViaIP(context, ptrIPAddr, xPort, xDeviceId)
    '            If result <> BS2ErrorCode.BS_SDK_SUCCESS Then
    '                WriteLog("BS2_ConnectDeviceViaIP Failed Device IP :- " & mstrIP & " Error : " + result.ToString())
    '                Exit Sub
    '            End If

    '            If cbOnLogReceived Is Nothing OrElse IsDBNull(cbOnLogReceived) Then
    '                cbOnLogReceived = New API.OnLogReceived(AddressOf RealtimeLogReceived)
    '            End If

    '            If cbOnDeviceDisconnected Is Nothing OrElse IsDBNull(cbOnDeviceDisconnected) Then
    '                cbOnDeviceDisconnected = New API.OnDeviceDisconnected(AddressOf DeviceDisconnected)
    '            End If

    '            API.BS2_SetDeviceEventListener(context, Nothing, Nothing, Nothing, cbOnDeviceDisconnected)

    '            result = API.BS2_StartMonitoringLog(context, xDeviceId, cbOnLogReceived)
    '            If result <> BS2ErrorCode.BS_SDK_SUCCESS Then
    '                WriteLog("MonitoringLog Failed Device IP :- " & mstrIP & " Error : " + result.ToString())
    '                Exit Sub
    '            End If

    '            If dctConnectedDevice.ContainsKey(mstrDeviceId) = False Then
    '                dctConnectedDevice.Add(mstrDeviceId.ToString(), mstrIP & "|" & iCompanyId & "|" & mstrDeviceCode & "|" & mstrDatabaseName & "|" & iYearId)
    '                If dctDisconnectedDevice.ContainsKey(mstrDeviceId) Then
    '                    dctDisconnectedDevice.Remove(mstrDeviceId)
    '                End If
    '            End If

    '        End If

    '    Catch ex As Exception
    '        WriteLog("ConnectBioStar2WithDeviceID Device IP :- " & mstrIP & " Error : " & ex.Message)
    '    Finally
    '        objBiostar2 = Nothing
    '    End Try
    'End Sub

    Public Sub RealtimeLogReceived(ByVal deviceID As UInt32, ByVal log As IntPtr)
        Dim mstrIP As String = ""
        Dim xCompanyId As Integer = 0
        Dim xYearId As Integer = 0
        Dim mstrDeviceCode As String = ""
        Dim mstrDatabaseName As String = ""
        Dim objLogin As New clslogin_Tran
        Dim objShift As New clsNewshift_master
        Dim objEmpShift As New clsEmployee_Shift_Tran
        Dim objShiftTran As New clsshift_tran
        Dim eventLog As BS2Event
        Dim eventTime As DateTime
        Try
            If log <> IntPtr.Zero Then

                eventLog = CType(Marshal.PtrToStructure(log, GetType(BS2Event)), BS2Event)
                eventTime = Util.ConvertFromUnixTimestamp(eventLog.dateTime)

                If dctConnectedDevice.ContainsKey(deviceID) Then
                    Dim ar() As String = dctConnectedDevice(deviceID).ToString().Split(CChar("|"))
                    If ar IsNot Nothing AndAlso ar.Length > 0 Then
                        mstrIP = ar(0).ToString()
                        xCompanyId = CInt(ar(1))
                        mstrDeviceCode = ar(2).ToString()
                        mstrDatabaseName = ar(3).ToString()
                        xYearId = ar(4).ToString()
                    End If
                End If

                If xCompanyId > 0 AndAlso mstrDeviceCode.Trim.Length > 0 Then

                    Dim mintshiftId As Integer = 0
                    Dim mdtLoginDate As DateTime = Util.ConvertFromUnixTimestamp(eventLog.dateTime).ToLocalTime()
                    Dim xEmployeeId As String = Encoding.UTF8.GetString(eventLog.userID, 0, eventLog.userID.Length)

                    If xEmployeeId.ToString().Trim.Length <= 0 Then Exit Sub

                    Dim objDeviceUser As New clsEmpid_devicemapping
                    Dim mintEmployeeId As Integer = objDeviceUser.GetEmployeeUnkID(xEmployeeId.ToString())
                    objDeviceUser = Nothing

                    If mintEmployeeId <= 0 Then Exit Sub

                    objLogin._Loginunkid = 0

                    objLogin._Employeeunkid = mintEmployeeId
                    objLogin._WebFormName = "ArutiAttendanceTracker"
                    Language._Object._LangId = 1

                    If mblnFirstCheckInLastCheckOut = False Then

                        Dim mblnIsOpenShift As Boolean = False
                        Dim mblnAttendanceOnDeviceInOutStatus As Boolean = False

                        mintshiftId = objEmpShift.GetEmployee_Current_ShiftId(mdtLoginDate.Date, mintEmployeeId)
                        objShift._Shiftunkid = mintshiftId
                        mblnIsOpenShift = objShift._IsOpenShift
                        mblnAttendanceOnDeviceInOutStatus = objShift._AttOnDeviceInOutStatus
                        objShiftTran.GetShiftTran(mintshiftId)

                        Dim drShiftTran() As DataRow = objShiftTran._dtShiftday.Select("dayid = " & GetWeekDayNumber(WeekdayName(Weekday(mdtLoginDate.Date), False, FirstDayOfWeek.Sunday)))
                        If drShiftTran.Length > 0 Then

                            If mblnIsOpenShift = False Then
                                Dim mdtDayStartTime As DateTime = CDate(mdtLoginDate).Date & " " & CDate(drShiftTran(0)("daystart_time")).ToString("hh:mm:ss tt")
                                If DateDiff(DateInterval.Minute, mdtDayStartTime, CDate(mdtLoginDate)) <= 0 Then   ' FOR NIGHT SHIFT PROBLEM

                                    Dim mintPreviousShift As Integer = objEmpShift.GetEmployee_Current_ShiftId(CDate(mdtLoginDate).Date.AddDays(-1).Date, mintEmployeeId)
                                    Dim objPrevShift As New clsNewshift_master
                                    objPrevShift._Shiftunkid = mintPreviousShift

                                    If objPrevShift._IsOpenShift = False Then

                                        If mintPreviousShift <> mintshiftId Then
                                            objShiftTran.GetShiftTran(mintPreviousShift)
                                            Dim drPreviousShiftTran() As DataRow = objShiftTran._dtShiftday.Select("dayid = " & GetWeekDayNumber(WeekdayName(Weekday(CDate(mdtLoginDate).Date.AddDays(-1).Date), False, FirstDayOfWeek.Sunday)))
                                            If drPreviousShiftTran.Length > 0 Then
                                                Dim mdtPreviousDayStartTime As DateTime = CDate(CDate(mdtLoginDate).Date & " " & CDate(drPreviousShiftTran(0)("daystart_time")).ToString("hh:mm tt"))
                                                If DateDiff(DateInterval.Hour, mdtPreviousDayStartTime, CDate(mdtLoginDate)) >= 24 Then
                                                    objLogin._Logindate = CDate(mdtLoginDate).Date
                                                Else
                                                    objLogin._Employeeunkid = mintEmployeeId
                                                    If objLogin.IsEmployeeLoginExist(eZeeDate.convertDate(CDate(mdtLoginDate).Date.AddDays(-1).Date).ToString(), "", mintEmployeeId, -1, Nothing) Then
                                                        objLogin._Logindate = mdtLoginDate.Date.AddDays(-1)
                                                    Else
                                                        objLogin._Logindate = mdtLoginDate.Date
                                                    End If

                                                End If 'If DateDiff(DateInterval.Hour, mdtPreviousDayStartTime, CDate(dr("Logintime"))) >= 24 Then

                                            End If ' If drPreviousShiftTran.Length > 0 Then
                                        Else
                                            objLogin._Logindate = mdtLoginDate.Date.AddDays(-1)
                                        End If  '   If mintPreviousShift <> mintshiftId Then
                                    Else
                                        objLogin._Logindate = mdtLoginDate.Date
                                    End If  'If objPrevShift._IsOpenShift = False Then

                                    objPrevShift = Nothing

                                ElseIf DateDiff(DateInterval.Minute, mdtDayStartTime, mdtLoginDate) >= 0 Then
                                    objLogin._Logindate = mdtLoginDate.Date
                                End If 'If DateDiff(DateInterval.Minute, mdtDayStartTime, CDate(mdtLoginDate)) <= 0 Then

                            Else
                                objLogin._Logindate = mdtLoginDate.Date
                            End If  'If mblnIsOpenShift = False Then

                        End If  'If drShiftTran.Length > 0 Then


                        If mblnAttendanceOnDeviceInOutStatus Then

                            Dim mintCurrentShiftId As Integer = objEmpShift.GetEmployee_Current_ShiftId(mdtLoginDate.Date, mintEmployeeId)
                            objLogin._Shiftunkid = mintCurrentShiftId

                            Dim mintLoginTypeId As Integer = objLogin.GetLoginType(mintEmployeeId, mdtLoginDate)

                            If mintLoginTypeId = 0 Then    'Status
                                objLogin._checkintime = mdtLoginDate
                                objLogin._Original_InTime = mdtLoginDate
                                objLogin._CheckInDevice = mstrDeviceCode
                                objLogin._Checkouttime = Nothing
                                objLogin._Original_OutTime = Nothing
                                objLogin._CheckOutDevice = ""
                                objLogin._InOutType = 0
                                objLogin._Workhour = 0
                                objLogin._Holdunkid = 0
                                objLogin._Voiddatetime = Nothing
                                objLogin._Isvoid = False
                                objLogin._Voidreason = ""
                                objLogin._SourceType = enInOutSource.Import
                                objLogin._Userunkid = 1

                                If objLogin.Insert(mstrDatabaseName, 1, xYearId, xCompanyId, Now.Date, Now.Date, mstrUserAccessModeSetting _
                                                                 , True, mblnIsIncludeInactiveEmp, mblnPolicyManagementTNA, mblnDonotAttendanceinSeconds _
                                                                 , mblnFirstCheckInLastCheckOut, mblnIsHolidayConsiderOnWeekend, mblnIsDayOffConsiderOnWeekend _
                                                                 , mblnIsHolidayConsiderOnDayoff, False, -1, Nothing, "", "") = False Then

                                    If objLogin._Message.Trim.Length > 0 Then
                                        WriteLog("RealtimeLogReceived Insert :- " & objLogin._Message)
                                    End If

                                End If  '  If objLogin.Insert

                            ElseIf mintLoginTypeId = 1 Then   'Status
                                Dim intLoginId As Integer = -1

                                If objLogin.IsEmployeeLoginExist(eZeeDate.convertDate(objLogin._Logindate.Date), "", mintEmployeeId, intLoginId) Then

                                    If objLogin._Loginunkid <= 0 Then
                                        objLogin._Loginunkid = intLoginId
                                    ElseIf objLogin._Loginunkid > 0 Then
                                        objLogin.GetData(Nothing)
                                    End If

                                    objLogin._Checkouttime = mdtLoginDate
                                    objLogin._Original_OutTime = mdtLoginDate
                                    objLogin._CheckOutDevice = mstrDeviceCode
                                    Dim wkmins As Double = DateDiff(DateInterval.Second, objLogin._checkintime, objLogin._Checkouttime)
                                    objLogin._Workhour = CInt(wkmins)
                                    objLogin._InOutType = 1
                                    objLogin._SourceType = enInOutSource.Import
                                    objLogin._Shiftunkid = objEmpShift.GetEmployee_Current_ShiftId(objLogin._Logindate.Date, mintEmployeeId)
                                    objLogin._Userunkid = 1

                                    If objLogin.Update(mstrDatabaseName, 1, xYearId, xCompanyId, Now.Date, Now.Date, mstrUserAccessModeSetting _
                                                               , True, mblnIsIncludeInactiveEmp, mblnPolicyManagementTNA, mblnDonotAttendanceinSeconds _
                                                               , mblnFirstCheckInLastCheckOut, mblnIsHolidayConsiderOnWeekend, mblnIsDayOffConsiderOnWeekend _
                                                               , mblnIsHolidayConsiderOnDayoff, False, -1, Nothing, "", "") = False Then

                                        If objLogin._Message.Trim.Length > 0 Then
                                            WriteLog("RealtimeLogReceived Update :- " & objLogin._Message)
                                        End If

                                    End If  'If objLogin.Update

                                End If  'If objLogin.IsEmployeeLoginExist

                            End If  ' If mintLoginTypeId = 0 Then    'Status

                        ElseIf mblnAttendanceOnDeviceInOutStatus = False Then

                            Dim mintLoginTypeId As Integer = objLogin.GetLoginType(mintEmployeeId, mdtLoginDate)

                            If mintLoginTypeId = 0 Then   'Status
                                objLogin._Logindate = mdtLoginDate.Date
                                objLogin._Shiftunkid = objEmpShift.GetEmployee_Current_ShiftId(mdtLoginDate.Date, mintEmployeeId)
                                objLogin._checkintime = mdtLoginDate
                                objLogin._Original_InTime = mdtLoginDate
                                objLogin._CheckInDevice = mstrDeviceCode
                                objLogin._SourceType = enInOutSource.Import
                                objLogin._Checkouttime = Nothing
                                objLogin._CheckOutDevice = ""
                                objLogin._Original_OutTime = Nothing
                                objLogin._InOutType = 0
                                objLogin._Workhour = 0
                                objLogin._Holdunkid = 0
                                objLogin._Voiddatetime = Nothing
                                objLogin._Isvoid = False
                                objLogin._Voidreason = ""
                                objLogin._Userunkid = 1

                                If objLogin.Insert(mstrDatabaseName, 1, xYearId, xCompanyId, Now.Date, Now.Date, mstrUserAccessModeSetting _
                                                                , True, mblnIsIncludeInactiveEmp, mblnPolicyManagementTNA, mblnDonotAttendanceinSeconds _
                                                                , mblnFirstCheckInLastCheckOut, mblnIsHolidayConsiderOnWeekend, mblnIsDayOffConsiderOnWeekend _
                                                                , mblnIsHolidayConsiderOnDayoff, False, -1, Nothing, "", "") = False Then

                                    If objLogin._Message.Trim.Length > 0 Then
                                        WriteLog("RealtimeLogReceived Insert :- " & objLogin._Message)
                                    End If

                                End If  ' If objLogin.Insert

                            ElseIf mintLoginTypeId = 1 Then   'Status

                                Dim intLoginId As Integer = -1

                                If objLogin.IsEmployeeLoginExist(eZeeDate.convertDate(objLogin._Logindate.Date), "", mintEmployeeId, intLoginId) Then

                                    If objLogin._Loginunkid <= 0 Then
                                        objLogin._Loginunkid = intLoginId
                                    ElseIf objLogin._Loginunkid > 0 Then
                                        objLogin.GetData(Nothing)
                                    End If

                                    objLogin._Checkouttime = mdtLoginDate
                                    objLogin._Original_OutTime = mdtLoginDate
                                    objLogin._SourceType = enInOutSource.Import
                                    objLogin._CheckOutDevice = mstrDeviceCode
                                    objLogin._Shiftunkid = objEmpShift.GetEmployee_Current_ShiftId(mdtLoginDate.Date, mintEmployeeId)
                                    Dim wkmins As Double = DateDiff(DateInterval.Second, objLogin._checkintime, objLogin._Checkouttime)
                                    objLogin._Workhour = CInt(wkmins)
                                    objLogin._InOutType = 1
                                    objLogin._Userunkid = 1

                                    If objLogin.Update(mstrDatabaseName, 1, xYearId, xCompanyId, Now.Date, Now.Date, mstrUserAccessModeSetting _
                                                              , True, mblnIsIncludeInactiveEmp, mblnPolicyManagementTNA, mblnDonotAttendanceinSeconds _
                                                              , mblnFirstCheckInLastCheckOut, mblnIsHolidayConsiderOnWeekend, mblnIsDayOffConsiderOnWeekend _
                                                              , mblnIsHolidayConsiderOnDayoff, False, -1, Nothing, "", "") = False Then

                                        If objLogin._Message.Trim.Length > 0 Then
                                            WriteLog("RealtimeLogReceived Update :- " & objLogin._Message)
                                        End If

                                    End If  'If objLogin.Update

                                End If  'If objLogin.IsEmployeeLoginExist

                            End If   ' If mintLoginTypeId = 0 Then   'Status

                        End If  'If mblnAttendanceOnDeviceInOutStatus Then

                    ElseIf mblnFirstCheckInLastCheckOut Then

                        Dim mblnIsOpenShift As Boolean = False
                        Dim mblnAttendanceOnDeviceInOutStatus As Boolean = False

                        mintshiftId = objEmpShift.GetEmployee_Current_ShiftId(mdtLoginDate.Date, mintEmployeeId)
                        objShift._Shiftunkid = mintshiftId
                        mblnIsOpenShift = objShift._IsOpenShift
                        mblnAttendanceOnDeviceInOutStatus = objShift._AttOnDeviceInOutStatus
                        objShiftTran.GetShiftTran(mintshiftId)

                        Dim drShiftTran() As DataRow = objShiftTran._dtShiftday.Select("dayid = " & GetWeekDayNumber(WeekdayName(Weekday(mdtLoginDate.Date), False, FirstDayOfWeek.Sunday)))

                        If drShiftTran.Length > 0 Then

                            If mblnIsOpenShift = False Then

                                Dim mdtDayStartTime As DateTime = CDate(mdtLoginDate).Date & " " & CDate(drShiftTran(0)("daystart_time")).ToString("hh:mm:ss tt")

                                If DateDiff(DateInterval.Minute, mdtDayStartTime, CDate(mdtLoginDate)) <= 0 Then   ' FOR NIGHT SHIFT PROBLEM

                                    Dim mintPreviousShift As Integer = objEmpShift.GetEmployee_Current_ShiftId(CDate(mdtLoginDate).Date.AddDays(-1).Date, mintEmployeeId)
                                    Dim objPrevShift As New clsNewshift_master
                                    objPrevShift._Shiftunkid = mintPreviousShift

                                    If objPrevShift._IsOpenShift = False Then

                                        If mintPreviousShift <> mintshiftId Then
                                            objShiftTran.GetShiftTran(mintPreviousShift)
                                            Dim drPreviousShiftTran() As DataRow = objShiftTran._dtShiftday.Select("dayid = " & GetWeekDayNumber(WeekdayName(Weekday(CDate(mdtLoginDate).Date.AddDays(-1).Date), False, FirstDayOfWeek.Sunday)))

                                            If drPreviousShiftTran.Length > 0 Then
                                                Dim mdtPreviousDayStartTime As DateTime = CDate(CDate(mdtLoginDate).Date & " " & CDate(drPreviousShiftTran(0)("daystart_time")).ToString("hh:mm tt"))
                                                If DateDiff(DateInterval.Hour, mdtPreviousDayStartTime, CDate(mdtLoginDate)) >= 24 Then
                                                    objLogin._Logindate = CDate(mdtLoginDate).Date
                                                Else
                                                    objLogin._Employeeunkid = mintEmployeeId
                                                    If objLogin.IsEmployeeLoginExist(eZeeDate.convertDate(CDate(mdtLoginDate).Date.AddDays(-1).Date).ToString(), "", mintEmployeeId, -1, Nothing) Then
                                                        objLogin._Logindate = mdtLoginDate.Date.AddDays(-1)
                                                    Else
                                                        objLogin._Logindate = mdtLoginDate.Date
                                                    End If  'If objLogin.IsEmployeeLoginExist

                                                End If ' If DateDiff(DateInterval.Hour, mdtPreviousDayStartTime, CDate(mdtLoginDate)) >= 24 Then

                                            End If  '  If drPreviousShiftTran.Length > 0 Then

                                        Else
                                            objLogin._Logindate = mdtLoginDate.Date.AddDays(-1)
                                        End If 'If mintPreviousShift <> mintshiftId Then
                                    Else
                                        objLogin._Logindate = mdtLoginDate.Date
                                    End If  'If objPrevShift._IsOpenShift = False Then

                                ElseIf DateDiff(DateInterval.Minute, mdtDayStartTime, mdtLoginDate) >= 0 Then
                                    objLogin._Logindate = mdtLoginDate.Date
                                End If  'If DateDiff(DateInterval.Minute, mdtDayStartTime, CDate(mdtLoginDate))

                            Else
                                objLogin._Logindate = mdtLoginDate.Date
                            End If  '   If mblnIsOpenShift = False Then


                            If mblnAttendanceOnDeviceInOutStatus Then

                                Dim mintLoginTypeId As Integer = objLogin.GetLoginType(mintEmployeeId, mdtLoginDate)

                                If mintLoginTypeId = 0 Then 'STATUS
                                    Dim mintLoginId As Integer = -1

                                    If objLogin.IsEmployeeLoginExist(eZeeDate.convertDate(mdtLoginDate), "", mintEmployeeId, mintLoginId) Then   'objLogin.IsEmployeeLoginExist

                                        If objLogin._Loginunkid <= 0 Then
                                            objLogin._Loginunkid = mintLoginId
                                        ElseIf objLogin._Loginunkid > 0 Then
                                            objLogin.GetData(Nothing)
                                        End If

                                        If objLogin._checkintime = Nothing AndAlso objLogin._Checkouttime <> Nothing Then  ' objLogin._checkintime = Nothing AndAlso objLogin._Checkouttime <> Nothing
                                            objLogin._checkintime = mdtLoginDate
                                            objLogin._Original_InTime = mdtLoginDate

                                            Dim objShiftInMst As New clsNewshift_master
                                            objShiftInMst._Shiftunkid = mintshiftId

                                            If objShiftInMst._MaxHrs > 0 Then
DeviceInStatus:
                                                If DateDiff(DateInterval.Minute, objLogin._checkintime, objLogin._Checkouttime) <= CInt(objShiftInMst._MaxHrs) Then
                                                    Dim wkmins As Double = DateDiff(DateInterval.Second, objLogin._checkintime, objLogin._Checkouttime)
                                                    objLogin._Workhour = CInt(wkmins)
                                                    objLogin._InOutType = 1
                                                    objLogin._SourceType = enInOutSource.Import
                                                    objLogin._CheckOutDevice = mstrDeviceCode

                                                    If objLogin.Update(mstrDatabaseName, 1, xYearId, xCompanyId, Now.Date, Now.Date, mstrUserAccessModeSetting _
                                                                   , True, mblnIsIncludeInactiveEmp, mblnPolicyManagementTNA, mblnDonotAttendanceinSeconds _
                                                                   , mblnFirstCheckInLastCheckOut, mblnIsHolidayConsiderOnWeekend, mblnIsDayOffConsiderOnWeekend _
                                                                   , mblnIsHolidayConsiderOnDayoff, False, -1, Nothing, "", "") = False Then

                                                        If objLogin._Message.Trim.Length > 0 Then
                                                            WriteLog("RealtimeLogReceived Update :- " & objLogin._Message)
                                                        End If

                                                    End If  'If objLogin.Update

                                                ElseIf DateDiff(DateInterval.Minute, objLogin._checkintime, objLogin._Checkouttime) > CInt(objShiftInMst._MaxHrs) Then  '
                                                    GoTo DeviceInStatus1

                                                End If  ' If DateDiff(DateInterval.Minute, objLogin._checkintime, objLogin._Checkouttime) <= CInt(objShiftInMst._MaxHrs) Then

                                            ElseIf objShiftInMst._MaxHrs <= 0 Then

                                                objLogin._checkintime = mdtLoginDate
                                                objLogin._Original_InTime = mdtLoginDate
                                                GoTo DeviceInStatus
                                            End If  'If objShiftInMst._MaxHrs > 0 Then

                                        End If  'If objLogin._checkintime = Nothing AndAlso objLogin._Checkouttime <> Nothing Then

                                    Else

DeviceInStatus1:
                                        objLogin._Logindate = mdtLoginDate.Date
                                        objLogin._checkintime = mdtLoginDate
                                        objLogin._Original_InTime = mdtLoginDate
                                        objLogin._CheckInDevice = mstrDeviceCode
                                        objLogin._Checkouttime = Nothing
                                        objLogin._Original_OutTime = Nothing
                                        objLogin._CheckOutDevice = ""
                                        objLogin._InOutType = 0
                                        objLogin._Workhour = 0
                                        objLogin._Holdunkid = 0
                                        objLogin._Voiddatetime = Nothing
                                        objLogin._Isvoid = False
                                        objLogin._Voidreason = ""

                                        If objLogin.Insert(mstrDatabaseName, 1, xYearId, xCompanyId, Now.Date, Now.Date, mstrUserAccessModeSetting _
                                                    , True, mblnIsIncludeInactiveEmp, mblnPolicyManagementTNA, mblnDonotAttendanceinSeconds _
                                                    , mblnFirstCheckInLastCheckOut, mblnIsHolidayConsiderOnWeekend, mblnIsDayOffConsiderOnWeekend _
                                                    , mblnIsHolidayConsiderOnDayoff, False, -1, Nothing, "", "") = False Then

                                            If objLogin._Message.Trim.Length > 0 Then
                                                WriteLog("RealtimeLogReceived Insert :- " & objLogin._Message)
                                            End If

                                        End If  '  If objLogin.Insert


                                    End If  'If objLogin.IsEmployeeLoginExist


                                ElseIf mintLoginTypeId = 1 Then ' Status
                                    Dim intLoginId As Integer = -1

                                    If objLogin.IsEmployeeLoginExist(eZeeDate.convertDate(mdtLoginDate.Date), "", mintEmployeeId, intLoginId) Then

                                        If objLogin._Loginunkid <= 0 Then
                                            objLogin._Loginunkid = intLoginId
                                        ElseIf objLogin._Loginunkid > 0 Then
                                            objLogin.GetData(Nothing)
                                        End If

                                        objLogin._Checkouttime = mdtLoginDate
                                        objLogin._Original_OutTime = objLogin._Checkouttime

                                        If objLogin._checkintime <> Nothing Then
                                            Dim objShiftMst As New clsNewshift_master
                                            objShiftMst._Shiftunkid = mintshiftId

                                            If objShiftMst._MaxHrs > 0 Then

                                                If DateDiff(DateInterval.Minute, objLogin._checkintime, mdtLoginDate.Date) <= CInt(objShiftMst._MaxHrs) Then
DeviceOutStatus:

                                                    Dim wkmins As Double = DateDiff(DateInterval.Second, objLogin._checkintime, objLogin._Checkouttime)
                                                    objLogin._Workhour = CInt(wkmins)
                                                    objLogin._InOutType = 1
                                                    objLogin._SourceType = enInOutSource.Import
                                                    objLogin._CheckOutDevice = mstrDeviceCode


                                                    If objLogin.Update(mstrDatabaseName, 1, xYearId, xCompanyId, Now.Date, Now.Date, mstrUserAccessModeSetting _
                                                                , True, mblnIsIncludeInactiveEmp, mblnPolicyManagementTNA, mblnDonotAttendanceinSeconds _
                                                                , mblnFirstCheckInLastCheckOut, mblnIsHolidayConsiderOnWeekend, mblnIsDayOffConsiderOnWeekend _
                                                                , mblnIsHolidayConsiderOnDayoff, False, -1, Nothing, "", "") = False Then

                                                        If objLogin._Message.Trim.Length > 0 Then
                                                            WriteLog("RealtimeLogReceived Update :- " & objLogin._Message)
                                                        End If

                                                    End If  'If objLogin.Update

                                                ElseIf DateDiff(DateInterval.Minute, objLogin._checkintime, mdtLoginDate.Date) > CInt(objShiftMst._MaxHrs) Then  ''objShiftMst._MaxHrs > 0 
DeviceOutStatus1:
                                                    objLogin._Checkouttime = Nothing
                                                    objLogin._Original_OutTime = Nothing
                                                    objLogin._CheckOutDevice = ""
                                                    objLogin._InOutType = 0
                                                    objLogin._Workhour = 0
                                                    objLogin._Holdunkid = 0
                                                    objLogin._Voiddatetime = Nothing
                                                    objLogin._Isvoid = False
                                                    objLogin._Voidreason = ""
                                                    objLogin._CheckInDevice = mstrDeviceCode


                                                    If objLogin.Insert(mstrDatabaseName, 1, xYearId, xCompanyId, Now.Date, Now.Date, mstrUserAccessModeSetting _
                                                                             , True, mblnIsIncludeInactiveEmp, mblnPolicyManagementTNA, mblnDonotAttendanceinSeconds _
                                                                             , mblnFirstCheckInLastCheckOut, mblnIsHolidayConsiderOnWeekend, mblnIsDayOffConsiderOnWeekend _
                                                                             , mblnIsHolidayConsiderOnDayoff, False, -1, Nothing, "", "") = False Then

                                                        If objLogin._Message.Trim.Length > 0 Then
                                                            WriteLog("RealtimeLogReceived Insert :- " & objLogin._Message)
                                                        End If

                                                    End If  '  If objLogin.Insert

                                                End If  '    If DateDiff(DateInterval.Minute, objLogin._checkintime, mdtLoginDate.Date) <= CInt(objShiftMst._MaxHrs) Then

                                            ElseIf objShiftMst._MaxHrs <= 0 Then   'objShiftMst._MaxHrs <= 0 

                                                If objLogin._checkintime <> Nothing Then

DeviceOutStatus2:
                                                    Dim wkmins As Double = DateDiff(DateInterval.Second, objLogin._checkintime, objLogin._Checkouttime)
                                                    objLogin._Workhour = CInt(wkmins)
                                                    objLogin._InOutType = 1
                                                    objLogin._SourceType = enInOutSource.Import
                                                    objLogin._CheckOutDevice = mstrDeviceCode

                                                    If objLogin.Update(mstrDatabaseName, 1, xYearId, xCompanyId, Now.Date, Now.Date, mstrUserAccessModeSetting _
                                                                          , True, mblnIsIncludeInactiveEmp, mblnPolicyManagementTNA, mblnDonotAttendanceinSeconds _
                                                                          , mblnFirstCheckInLastCheckOut, mblnIsHolidayConsiderOnWeekend, mblnIsDayOffConsiderOnWeekend _
                                                                          , mblnIsHolidayConsiderOnDayoff, False, -1, Nothing, "", "") = False Then

                                                        If objLogin._Message.Trim.Length > 0 Then
                                                            WriteLog("RealtimeLogReceived Update :- " & objLogin._Message)
                                                        End If

                                                    End If  'If objLogin.Update

                                                ElseIf objLogin._checkintime = Nothing Then
                                                    Dim dtDeviceTable As DataTable = objLogin.GetMaxLoginDate(mdtLoginDate.Date, objLogin._Employeeunkid)
                                                    If dtDeviceTable IsNot Nothing And dtDeviceTable.Rows.Count > 0 Then
                                                        objLogin._Loginunkid = CInt(dtDeviceTable.Rows(0)("loginunkid"))
                                                        objLogin._Checkouttime = mdtLoginDate
                                                        objLogin._Original_OutTime = objLogin._Checkouttime
                                                        Dim wkmins As Double = DateDiff(DateInterval.Second, objLogin._checkintime, objLogin._Checkouttime)
                                                        objLogin._Workhour = CInt(wkmins)
                                                        objLogin._InOutType = 1
                                                        objLogin._SourceType = enInOutSource.Import
                                                        objLogin._CheckOutDevice = mstrDeviceCode

                                                        If objLogin.Update(mstrDatabaseName, 1, xYearId, xCompanyId, Now.Date, Now.Date, mstrUserAccessModeSetting _
                                                                          , True, mblnIsIncludeInactiveEmp, mblnPolicyManagementTNA, mblnDonotAttendanceinSeconds _
                                                                          , mblnFirstCheckInLastCheckOut, mblnIsHolidayConsiderOnWeekend, mblnIsDayOffConsiderOnWeekend _
                                                                          , mblnIsHolidayConsiderOnDayoff, False, -1, Nothing, "", "") = False Then

                                                            If objLogin._Message.Trim.Length > 0 Then
                                                                WriteLog("RealtimeLogReceived Update :- " & objLogin._Message)
                                                            End If

                                                        End If  ' If objLogin.Update

                                                    End If  ' If dtDeviceTable IsNot Nothing And dtDeviceTable.Rows.Count > 0 Then

                                                End If  ' If objLogin._checkintime <> Nothing Then

                                            End If  'If objShiftMst._MaxHrs > 0 Then

                                        ElseIf objLogin._checkintime = Nothing Then
                                            GoTo DeviceOutStatus1
                                        End If  ' If objLogin._checkintime <> Nothing Then

                                    Else   'employee login is not exist.

                                        Dim dtDeviceTable As DataTable = objLogin.GetMaxLoginDate(mdtLoginDate.Date, objLogin._Employeeunkid)
                                        If dtDeviceTable IsNot Nothing And dtDeviceTable.Rows.Count > 0 Then
                                            objLogin._Loginunkid = CInt(dtDeviceTable.Rows(0)("loginunkid"))
                                            objLogin._Checkouttime = mdtLoginDate
                                            objLogin._Original_OutTime = mdtLoginDate

                                            Dim objShiftMst As New clsNewshift_master
                                            objShiftMst._Shiftunkid = mintshiftId
                                            If objShiftMst._MaxHrs > 0 Then
                                                If objLogin._checkintime <> Nothing AndAlso DateDiff(DateInterval.Minute, objLogin._checkintime, mdtLoginDate) <= CInt(objShiftMst._MaxHrs) Then
                                                    GoTo DeviceOutStatus
                                                ElseIf objLogin._checkintime <> Nothing AndAlso DateDiff(DateInterval.Minute, objLogin._checkintime, mdtLoginDate) > CInt(objShiftMst._MaxHrs) Then
                                                    objLogin._Logindate = mdtLoginDate.Date
                                                    GoTo DeviceOutStatus1
                                                End If '  If objLogin._checkintime <> Nothing AndAlso DateDiff(DateInterval.Minute, objLogin._checkintime, CDate(dr("Logintime"))) <= CInt(objShiftMst._MaxHrs) Then
                                            ElseIf objShiftMst._MaxHrs <= 0 Then
                                                GoTo DeviceOutStatus2
                                            End If 'objShiftMst._MaxHrs > 0

                                        Else
                                            objLogin._Logindate = mdtLoginDate.Date
                                            objLogin._Checkouttime = mdtLoginDate
                                            objLogin._Original_OutTime = mdtLoginDate
                                            GoTo DeviceOutStatus1

                                        End If  'If dtDeviceTable IsNot Nothing And dtDeviceTable.Rows.Count > 0 Then

                                    End If  '  If objLogin.IsEmployeeLoginExist

                                End If  '  If mintLoginTypeId = 0 Then

                            ElseIf mblnAttendanceOnDeviceInOutStatus = False Then
                                Dim intLoginId As Integer = -1

                                If objLogin.IsEmployeeLoginExist(eZeeDate.convertDate(objLogin._Logindate.Date), "", mintEmployeeId, intLoginId) Then

                                    If objLogin._Loginunkid <= 0 Then
                                        objLogin._Loginunkid = intLoginId
                                    ElseIf objLogin._Loginunkid > 0 Then
                                        objLogin.GetData(Nothing)
                                    End If

                                    objLogin._Checkouttime = mdtLoginDate
                                    objLogin._Original_OutTime = mdtLoginDate
                                    objLogin._CheckOutDevice = mstrDeviceCode
                                    objLogin._SourceType = enInOutSource.Import
                                    Dim wkmins As Double = DateDiff(DateInterval.Second, objLogin._checkintime, objLogin._Checkouttime)
                                    objLogin._Workhour = CInt(wkmins)
                                    objLogin._InOutType = 1
                                    objLogin._Shiftunkid = objEmpShift.GetEmployee_Current_ShiftId(objLogin._Logindate.Date, mintEmployeeId)


                                    If objLogin.Update(mstrDatabaseName, 1, xYearId, xCompanyId, Now.Date, Now.Date, mstrUserAccessModeSetting _
                                                                         , True, mblnIsIncludeInactiveEmp, mblnPolicyManagementTNA, mblnDonotAttendanceinSeconds _
                                                                         , mblnFirstCheckInLastCheckOut, mblnIsHolidayConsiderOnWeekend, mblnIsDayOffConsiderOnWeekend _
                                                                         , mblnIsHolidayConsiderOnDayoff, False, -1, Nothing, "", "") = False Then

                                        If objLogin._Message.Trim.Length > 0 Then
                                            WriteLog("RealtimeLogReceived Update :- " & objLogin._Message)
                                        End If

                                    End If  ' If objLogin.Update

                                Else
                                    objLogin._checkintime = mdtLoginDate
                                    objLogin._Original_InTime = mdtLoginDate
                                    objLogin._Checkouttime = Nothing
                                    objLogin._CheckOutDevice = ""
                                    objLogin._Original_OutTime = Nothing
                                    objLogin._InOutType = 0
                                    objLogin._Workhour = 0
                                    objLogin._Holdunkid = 0
                                    objLogin._Voiddatetime = Nothing
                                    objLogin._Isvoid = False
                                    objLogin._Voidreason = ""
                                    objLogin._CheckInDevice = mstrDeviceCode

                                    If objLogin.Insert(mstrDatabaseName, 1, xYearId, xCompanyId, Now.Date, Now.Date, mstrUserAccessModeSetting _
                                                                             , True, mblnIsIncludeInactiveEmp, mblnPolicyManagementTNA, mblnDonotAttendanceinSeconds _
                                                                             , mblnFirstCheckInLastCheckOut, mblnIsHolidayConsiderOnWeekend, mblnIsDayOffConsiderOnWeekend _
                                                                             , mblnIsHolidayConsiderOnDayoff, False, -1, Nothing, "", "") = False Then

                                        If objLogin._Message.Trim.Length > 0 Then
                                            WriteLog("RealtimeLogReceived Insert :- " & objLogin._Message)
                                        End If

                                    End If  '  If objLogin.Insert

                                End If   '   If objLogin.IsEmployeeLoginExist(eZeeDate.convertDate(objLogin._Logindate.Date), "", CInt(dr("EmployeeId")), intLoginId) Then

                            End If  ' If mblnAttendanceOnDeviceInOutStatus Then

                        End If  'If drShiftTran.Length > 0

                    End If  ' If mblnFirstCheckInLastCheckOut = False Then

                End If   ' If xCompanyId > 0 AndAlso mstrDeviceCode.Trim.Length > 0 Then

            End If  '   If log <> IntPtr.Zero Then

        Catch ex As Exception
            WriteLog("RealtimeLogReceived Device Id : " & deviceID & " Error :- " & ex.Message)
        Finally
            objLogin = Nothing
            objShift = Nothing
            objEmpShift = Nothing
            objShiftTran = Nothing
        End Try
    End Sub

    Private Sub DeviceDisconnected(ByVal deviceID As UInt32)
        Dim mstrValue As String = ""
        Try

            If dctConnectedDevice.ContainsKey(deviceID) Then
                mstrValue = dctConnectedDevice(deviceID).ToString()
                dctConnectedDevice.Remove(deviceID)
                If dctDisconnectedDevice.ContainsKey(deviceID) = False AndAlso mstrValue.Trim.Length > 0 Then
                    dctDisconnectedDevice.Add(deviceID, mstrValue)
                    If mstrValue.Trim.Length > 0 AndAlso mstrTnAFailureNotificationUserIds.Trim.Length > 0 Then
                        Dim ar() As String = mstrValue.Trim.Split(CChar("|"))
                        If ar.Length > 0 Then
                            Dim mstrIP As String = ar(0).ToString()
                            Dim mintCompanyId As Integer = CInt(ar(1))
                            mstrDeviceCode = ar(2).ToString()
                            WriteLog("mintCompanyId : " & mintCompanyId.ToString())
                            WriteLog("mstrDeviceCode : " & mstrDeviceCode.ToString())
                            SendFailedNotificationToUser(mintCompanyId, mstrDeviceCode, mstrIP, "")
                        End If
                    End If
                End If
            End If
        Catch ex As Exception
            WriteLog("DeviceDisconnected Device Id : " & deviceID & " Error :- " & ex.Message)
        End Try
    End Sub

    Public Sub SendFailedNotificationToUser(ByVal xCompanyunkid As Integer, ByVal xDevicecode As String, ByVal mstrIP As String, ByVal mstrError As String)
        Dim strMessage As String = ""
        Dim objSendmail As New clsSendMail
        Try

            If mstrTnAFailureNotificationUserIds.Trim.Length > 0 Then
                Company._Object._Companyunkid = xCompanyunkid

                Dim objUser As New clsUserAddEdit
                For Each sId As String In mstrTnAFailureNotificationUserIds.Trim.Split(CChar(","))

                    If sId.Trim.Length <= 0 Then Continue For

                    objUser._Userunkid = CInt(sId)
                    Dim mstrUserName As String = objUser._Username.Trim
                    Dim mstrUserEmail As String = objUser._Email

                    strMessage = "<HTML> <BODY>"
                    strMessage &= "Dear " & CultureInfo.CurrentCulture.TextInfo.ToTitleCase(mstrUserName) & ", <BR></BR>"
                    If mstrError.Trim.Length > 0 AndAlso xDevicecode.Trim.Length <= 0 AndAlso mstrIP.Trim.Length <= 0 Then
                        strMessage &= "This is to notify you that device(s) was not able to connect." & "<BR></BR>"
                        strMessage &= "<B>" & "Error Message :" & mstrError & "</B><BR></BR>"
                    ElseIf mstrError.Trim.Length <= 0 AndAlso xDevicecode.Trim.Length > 0 AndAlso mstrIP.Trim.Length > 0 Then
                        strMessage &= "This is to notify you that device code <B>" & xDevicecode & "</B> with IP Adress <B>(" & mstrIP & ")</B> was not able to connect." & "<BR></BR>"
                    End If
                    strMessage &= "Regards."

                    Dim blnValue As Boolean = True
                    blnValue = CBool(IIf(mstrArutisignature.ToString().Length > 0, mstrArutisignature.ToString(), True))
                    If blnValue Then
                        strMessage &= "<BR></BR><BR></BR><center>""POWERED BY ARUTI HR & PAYROLL MANAGEMENT SOFTWARE""</center>"
                    End If
                    strMessage &= "</BODY></HTML>"


                    objSendmail._Message = strMessage.ToString()
                    objSendmail._Form_Name = "ArutiAttendanceTracker"
                    objSendmail._LogEmployeeUnkid = -1
                    objSendmail._OperationModeId = enLogin_Mode.DESKTOP
                    objSendmail._UserUnkid = 1
                    objSendmail._ModuleRefId = clsSendMail.enAT_VIEW_TYPE.TNA_MGT


                    Dim mstrEmailError As String = ""
                    If mstrUserEmail.Trim.Length > 0 Then
                        objSendmail._Subject = "Notification for Failure of Fingerprint Device Connection"
                        objSendmail._ToEmail = mstrUserEmail.Trim
                        mstrEmailError = objSendmail.SendMail(xCompanyunkid)
                    End If
                Next
            End If
        Catch ex As Exception
            WriteLog("SendFailedNotificationToUser : " & ex.Message)
        Finally
            objSendmail = Nothing
        End Try
    End Sub

    Private Sub WriteLog(ByVal strMessage As String)
        Dim m_strLogFile As String = ""
        Dim m_strFileName As String = "ArutiAttendance_LOG_" & Now.ToString("yyyyMMdd")
        Dim file As System.IO.StreamWriter
        m_strLogFile = System.IO.Path.Combine(My.Application.Info.DirectoryPath, m_strFileName & ".txt")
        file = New System.IO.StreamWriter(m_strLogFile, True)
        file.BaseStream.Seek(0, SeekOrigin.End)
        file.WriteLine(strMessage)
        file.Close()
    End Sub

#End Region

End Class
