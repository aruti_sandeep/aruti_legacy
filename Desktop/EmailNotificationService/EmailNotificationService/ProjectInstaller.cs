﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration.Install;
using System.Linq;
using System.ServiceProcess;
using System.Threading.Tasks;

namespace EmailNotificationService
{
    [RunInstaller(true)]
    public partial class ProjectInstaller : System.Configuration.Install.Installer
    {
        private ServiceProcessInstaller processInstaller;
        private ServiceInstaller serviceInstaller;

        public ProjectInstaller()
        {
            // Create and configure the service process installer
            processInstaller = new ServiceProcessInstaller();
            processInstaller.Account = ServiceAccount.LocalSystem;

            // Create and configure the service installer
            serviceInstaller = new ServiceInstaller();
            serviceInstaller.ServiceName = "EmailNotificationService";
            serviceInstaller.DisplayName = "Email Notification Service";
            serviceInstaller.StartType = ServiceStartMode.Automatic;

            // Add installers to the installer collection
            Installers.Add(processInstaller);
            Installers.Add(serviceInstaller);
        }

        private void serviceInstaller1_AfterInstall(object sender, InstallEventArgs e)
        {
            ServiceController sc;
            sc = new ServiceController(serviceInstaller1.ServiceName);
            sc.Start();
        }
    }
}
