﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Management;
using System.Net;
using System.Net.Mail;
using System.Net.Mime;
using System.Runtime.Serialization.Formatters.Binary;
using System.ServiceProcess;
using System.Text.RegularExpressions;
using System.Threading;

namespace EmailNotificationService
{
    public partial class EmailService : ServiceBase
    {
        private string connectionString = @"Data Source=.\apayroll;user id=aruti_sa;Initial Catalog=applicationemaillogs;Password=pRofessionalaRuti999;MultipleActiveResultSets=true;";
        private SqlConnection connection;
        private ManualResetEvent shutdownEvent = new ManualResetEvent(false);
        private Thread serviceThread;

        public EmailService()
        {
            InitializeComponent();
        }

        protected override void OnStart(string[] args)
        {
            serviceThread = new Thread(ServiceWorkerThread);
            serviceThread.Start();
        }

        protected override void OnStop()
        {
            shutdownEvent.Set();
            if (!serviceThread.Join(300))
            {
                serviceThread.Abort();
            }
            SqlDependency.Stop(connectionString);
        }

        private void ServiceWorkerThread()
        {
            try
            {
                SqlDependency.Start(connectionString);
                StartListeningForChanges();
                //shutdownEvent.WaitOne();
            }
            catch (Exception ex)
            {
                LogError($"Service worker thread encountered an error: {ex.Message}");
            }
            finally
            {
                //SqlDependency.Stop(connectionString);
            }
        }

        private void StartListeningForChanges()
        {
            //int retryCount = 3;
            //while (retryCount > 0)
            //{
            try
            {
                SqlDependency.Start(connectionString);

                using (SqlConnection connection = new SqlConnection(connectionString))
                {
                    connection.Open();
                    string query = "SELECT Id, RecipientEmail, EmailSubject, EmailContent FROM dbo.EmailQueue WITH (NOLOCK) WHERE IsProcessed = 0";

                    using (SqlCommand command = new SqlCommand(query, connection))
                    {
                        SqlDependency dependency = new SqlDependency(command);
                        dependency.OnChange += OnDatabaseChange;

                        using (SqlDataReader reader = command.ExecuteReader())
                        {
                            // Reader used to set up the dependency, no further processing needed

                        }
                    }
                }

                LogInfo("Listening for changes on the EmailQueue table.");
                //break;
            }
            catch (Exception ex)
            {
                //retryCount--;
                //LogError($"Error setting up SQL Dependency: {ex.Message}. Retries left: {retryCount}");
                LogError($"Error setting up SQL Dependency: {ex.Message}.");
                System.Threading.Thread.Sleep(1000);
            }
            //}
        }


        private void StartListeningForChanges1()
        {
            try
            {
                connection = new SqlConnection(connectionString);
                connection.Open();

                string query = "SELECT Id, RecipientEmail, EmailSubject, EmailContent FROM EmailQueue WITH (NOLOCK) WHERE IsProcessed = 0";

                using (SqlCommand command = new SqlCommand(query, connection))
                {
                    SqlDependency dependency = new SqlDependency(command);
                    dependency.OnChange += new OnChangeEventHandler(OnDatabaseChange);

                    // Execute the command to set up the dependency
                    using (SqlDataReader reader = command.ExecuteReader())
                    {

                    }
                }
            }
            catch (Exception ex)
            {
                LogError($"Error setting up SQL Dependency: {ex.Message}");
            }
        }


        private void OnDatabaseChange(object sender, SqlNotificationEventArgs e)
        {
            try
            {
                LogInfo($"Notification received. Type: {e.Type}, Source: {e.Source}, Info: {e.Info}");

                if (e.Info == SqlNotificationInfo.Insert)
                {
                    LogInfo("Change detected. Processing emails...");
                }
                else
                {
                    LogInfo($"Unhandled notification type: {e.Type}. Source: {e.Source}, Info: {e.Info}");
                }
                Thread.Sleep(5000);
                ProcessEmails();
                StartListeningForChanges();
            }
            catch (Exception ex)
            {
                LogError($"OnDatabaseChange: {ex.Message}");
            }
        }
        private void ProcessEmails()
        {
            try
            {

                DataTable table = new DataTable();

                LogInfo($"Getting Email Setups --- START");
                using (SqlConnection connection = new SqlConnection(connectionString))
                {
                    connection.Open();

                    string stmpdetailQuery = "SELECT sendername,email_type,ews_url,ews_domain, senderaddress,reference,mailserverip,mailserverport,username,password,isloginssl,iscrt_authenticated,isbypassproxy,protocolunkid,companyunkid FROM hrmsConfiguration..cfcompany_master WITH (NOLOCK) WHERE code = 'NMB' ";

                    using (SqlCommand selectCommand = new SqlCommand(stmpdetailQuery, connection))
                    {
                        using (SqlDataAdapter dataAdapter = new SqlDataAdapter(selectCommand))
                        {
                            dataAdapter.Fill(table);
                        }
                    }

                    LogInfo($"Getting Email Setups --- END : " + table.Rows.Count.ToString());


                    LogInfo($"Getting Email Data --- START");
                    string selectQuery = "SELECT Id, RecipientEmail, EmailSubject, EmailContent FROM EmailQueue WITH (NOLOCK) WHERE IsProcessed = 0";

                    using (SqlCommand selectCommand = new SqlCommand(selectQuery, connection))
                    {
                        using (SqlDataReader reader = selectCommand.ExecuteReader())
                        {
                            while (reader.Read())
                            {
                                LogInfo($"Email Data Got --- START");
                                string id = reader["Id"].ToString();
                                string recipientEmail = reader["RecipientEmail"].ToString();
                                string emailSubject = reader["EmailSubject"].ToString();
                                string emailContent = reader["EmailContent"].ToString();

                                LogInfo($"Email Data Got --- END");

                                DataTable dtAlternateView = GetEmailAlternateView(id);

                                DataTable dtAttachment = GetEmailAttachment(id);

                                LogInfo($"Attachment Row Count --- " + dtAttachment.Rows.Count);

                                LogInfo($"Sending Email --- START");
                                string reason = SendEmail(recipientEmail, emailSubject, emailContent, table, dtAttachment, dtAlternateView);
                                LogInfo($"Sending Email --- END");

                                if (reason == null) { reason = string.Empty; }


                                LogInfo($"Marking Processed --- START");
                                MarkEmailAsProcessed(id, reason);
                                LogInfo($"Marking Processed --- END");



                                try
                                {
                                    LogInfo($" Delete AlternateView Files --> START");
                                    if (dtAlternateView != null && dtAlternateView.Rows.Count > 0)
                                    {
                                        foreach (DataRow dr in dtAlternateView.Rows)
                                        {
                                            System.IO.File.Delete(System.IO.Path.GetTempPath() + dr["alternateviewguid"].ToString() + ".jpeg");
                                        }
                                    }
                                    LogInfo($" Delete AlternateView Files --> STOP");
                                }
                                catch (Exception ex)
                                {
                                    LogInfo($"Delete AlternateView Files : {ex.Message}");
                                }



                                try
                                {
                                    LogInfo($" Delete Attachment Files --> START");
                                    if (dtAttachment != null && dtAttachment.Rows.Count > 0)
                                    {
                                        foreach (DataRow dr in dtAttachment.Rows)
                                        {
                                            System.IO.File.Delete(System.IO.Path.GetTempPath() + dr["filename"].ToString());
                                        }
                                    }
                                    LogInfo($" Delete Attachment Files --> STOP");
                                }
                                catch (Exception ex)
                                {
                                    LogInfo($"Delete Attachment Files : {ex.Message}");
                                }

                            }
                        }
                    }
                    LogInfo($"Getting Email Data --- END");
                }
            }
            catch (Exception ex)
            {
                LogError($"Error processing emails: {ex.StackTrace}");
            }
        }

        private DataTable GetEmailAttachment(string id)
        {
            DataTable dtable = new DataTable();
            try
            {
                using (SqlConnection connection = new SqlConnection(connectionString))
                {
                    connection.Open();
                    string selectQuery = "SELECT Id, filename, filedata FROM mailattachment WITH (NOLOCK) WHERE ISNULL(alternateviewguid,'') = '' AND EmailQueueId = @EmailQueueId ";

                    using (SqlCommand selectCommand = new SqlCommand(selectQuery, connection))
                    {
                        selectCommand.Parameters.Clear();
                        selectCommand.Parameters.AddWithValue("EmailQueueId", id);
                        using (SqlDataAdapter dataAdapter = new SqlDataAdapter(selectCommand))
                        {
                            dataAdapter.Fill(dtable);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                LogError($"GetEmailAttachment: {ex.StackTrace}");
            }
            return dtable;
        }
        private DataTable GetEmailAlternateView(string id)
        {
            DataTable dtable = new DataTable();
            try
            {
                using (SqlConnection connection = new SqlConnection(connectionString))
                {
                    connection.Open();
                    string selectQuery = "SELECT Id, filename, filedata,alternateviewguid FROM mailattachment WITH (NOLOCK) WHERE ISNULL(alternateviewguid,'') <> '' AND EmailQueueId = @EmailQueueId";

                    using (SqlCommand selectCommand = new SqlCommand(selectQuery, connection))
                    {
                        selectCommand.Parameters.Clear();
                        selectCommand.Parameters.AddWithValue("EmailQueueId", id);
                        using (SqlDataAdapter dataAdapter = new SqlDataAdapter(selectCommand))
                        {
                            dataAdapter.Fill(dtable);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                LogError($"GetEmailAlternateView: {ex.StackTrace}");
            }
            return dtable;
        }

        private string SendEmail(string recipientEmail, string subject, string body, DataTable table, DataTable dtAttachments, DataTable dtAlternateView)
        {
            string sendingError = string.Empty;
            try
            {
                if (table != null && table.Rows.Count > 0)
                {
                    using (MailMessage mail = new MailMessage())
                    {

                        if (dtAlternateView != null && dtAlternateView.Rows.Count > 0)
                        {
                            if (body.Contains("<img "))
                            {
                                List<LinkedResource> lscr = new List<LinkedResource>();
                                AlternateView avHtml = null;
                                foreach (DataRow dr in dtAlternateView.Rows)
                                {
                                    string strPath = LoadFromDB(dr["filedata"].ToString(), System.IO.Path.GetTempPath() + dr["alternateviewguid"].ToString() + ".jpeg");
                                    LogInfo($" AlternateView Path --> " + strPath);
                                    LinkedResource inline = new LinkedResource(strPath, MediaTypeNames.Image.Jpeg);
                                    inline.ContentId = dr["alternateviewguid"].ToString();
                                    lscr.Add(inline);
                                }

                                avHtml = AlternateView.CreateAlternateViewFromString(body, null, MediaTypeNames.Text.Html);
                                for (int i = 0; i <= lscr.Count - 1; i++)
                                {
                                    avHtml.LinkedResources.Add(lscr[i]);
                                }
                                mail.AlternateViews.Add(avHtml);
                            }
                        }


                        mail.From = new MailAddress(table.Rows[0]["senderaddress"].ToString());
                        mail.To.Add(recipientEmail);
                        mail.Subject = subject;
                        mail.IsBodyHtml = true;
                        mail.Body = body;


                        if (dtAttachments != null && dtAttachments.Rows.Count > 0)
                        {
                            foreach (DataRow dr in dtAttachments.Rows)
                            {
                                string strPath = LoadFromDB(dr["filedata"].ToString(), System.IO.Path.GetTempPath() + dr["filename"].ToString());
                                mail.Attachments.Add(new System.Net.Mail.Attachment(strPath));
                                LogInfo($" Attachment Path :  {strPath} ");
                            }
                        }

                        using (SmtpClient smtp = new SmtpClient(table.Rows[0]["mailserverip"].ToString()))
                        {
                            smtp.Port = (int)table.Rows[0]["mailserverport"];
                            smtp.Credentials = new NetworkCredential(table.Rows[0]["username"].ToString(), table.Rows[0]["password"].ToString());
                            smtp.EnableSsl = (bool)table.Rows[0]["isloginssl"];

                            if ((bool)table.Rows[0]["isbypassproxy"])
                            {
                                ServicePointManager.Expect100Continue = false;
                            }

                            if ((bool)table.Rows[0]["iscrt_authenticated"])
                            {
                                ServicePointManager.ServerCertificateValidationCallback = (s, certificate, chain, sslPolicyErrors) => true;
                            }
                            if (smtp.Port == 587)
                            {
                                if ((int)table.Rows[0]["protocolunkid"] == 1)
                                {
                                    ServicePointManager.SecurityProtocol = System.Net.SecurityProtocolType.Tls;
                                }
                                else if ((int)table.Rows[0]["protocolunkid"] == 2)
                                {
                                    ServicePointManager.SecurityProtocol = System.Net.SecurityProtocolType.Ssl3;
                                }
                                else if ((int)table.Rows[0]["protocolunkid"] == 3)
                                {
                                    ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls12;
                                }
                            }

                            try
                            {
                                LogInfo($" smtp.Send :  {recipientEmail} -- START");
                                smtp.Send(mail);
                                LogInfo($" smtp.Send :  {recipientEmail} -- END");
                            }
                            catch (Exception iex)
                            {
                                sendingError = iex.Message;
                                LogInfo($"Email sendingError : {iex.Message}");
                            }

                        }
                    }
                }
                else
                {
                    LogInfo($"Email Configuration Not Found");
                }


                //using (MailMessage mail = new MailMessage())
                //{
                //    mail.From = new MailAddress("your_email@example.com");
                //    mail.To.Add(recipientEmail);
                //    mail.Subject = subject;
                //    mail.Body = body;

                //    using (SmtpClient smtp = new SmtpClient("smtp.your-email-server.com"))
                //    {
                //        smtp.Port = 587;
                //        smtp.Credentials = new NetworkCredential("your_email@example.com", "your_password");
                //        smtp.EnableSsl = true;
                //        smtp.Send(mail);
                //    }

                //    LogInfo($"Email sent to {recipientEmail}");
                //}
            }
            catch (Exception ex)
            {
                sendingError = ex.Message;
                LogError($"Failed to send email to {recipientEmail}: {ex.Message}");
            }

            return sendingError;
        }


        public string LoadFromDB(string filedata, string fullfilename)
        {
            string filepath = "";
            try
            {
                filepath = (fullfilename);
                File.WriteAllBytes(filepath, Convert.FromBase64String(filedata));
            }
            catch (Exception e)
            {
                LogError($"LoadFromDB : " + e.Message);
            }
            return filepath;
        }

        private void MarkEmailAsProcessed(string id, string reason)
        {
            try
            {
                using (SqlConnection connection = new SqlConnection(connectionString))
                {
                    connection.Open();
                    
                    string updateQuery = "UPDATE EmailQueue SET IsProcessed = 1,FailureReason = @reason,senddatetime = @senddatetime WHERE Id = @Id";

                    using (SqlCommand updateCommand = new SqlCommand(updateQuery, connection))
                    {
                        updateCommand.Parameters.Clear();
                        updateCommand.Parameters.AddWithValue("@Id", id.ToString());
                        updateCommand.Parameters.AddWithValue("@reason", reason);
                        updateCommand.Parameters.AddWithValue("@senddatetime", DateTime.Now);
                        updateCommand.ExecuteNonQuery();
                    }

                    LogInfo($"Email with ID {id} marked as processed.");
                }
            }
            catch (Exception ex)
            {
                LogError($"Failed to mark email as processed: {ex.Message}");
            }
        }


        public void LogInfo(string Message)
        {
            string m_strLogFile = "";
            string m_strFileName = "Notification_Info_LOG_" + DateTime.Now.Date.ToString("yyyyMMdd");
            System.IO.StreamWriter file;
            m_strLogFile = System.IO.Path.Combine(AppDomain.CurrentDomain.BaseDirectory + "\\", m_strFileName + ".txt");
            file = new System.IO.StreamWriter(m_strLogFile, true);
            file.BaseStream.Seek(0, SeekOrigin.End);
            string strFinalMessage = "";
            strFinalMessage = "---------------------------------------------------------------------------------------" + Environment.NewLine;
            strFinalMessage += Message + Environment.NewLine;
            strFinalMessage += "---------------------------------------------------------------------------------------" + Environment.NewLine;
            file.WriteLine(strFinalMessage);
            file.Close();
        }

        public void LogError(string Message)
        {
            string m_strLogFile = "";
            string m_strFileName = "Notification_Error_LOG_" + DateTime.Now.Date.ToString("yyyyMMdd");
            System.IO.StreamWriter file;
            m_strLogFile = System.IO.Path.Combine(AppDomain.CurrentDomain.BaseDirectory + "\\", m_strFileName + ".txt");
            file = new System.IO.StreamWriter(m_strLogFile, true);
            file.BaseStream.Seek(0, SeekOrigin.End);
            string strFinalMessage = "";
            strFinalMessage = "---------------------------------------------------------------------------------------" + Environment.NewLine;
            strFinalMessage += Message + Environment.NewLine;
            strFinalMessage += "---------------------------------------------------------------------------------------" + Environment.NewLine;
            file.WriteLine(strFinalMessage);
            file.Close();
        }

        public MailMessage DeserializeMailMessage(byte[] mailMessageBytes)
        {
            MailMessage m = new MailMessage();
            using (MemoryStream memoryStream = new MemoryStream(mailMessageBytes))
            {
                BinaryFormatter binaryFormatter = new BinaryFormatter();

                SerializableMailMessage serializedMessage = (SerializableMailMessage)binaryFormatter.Deserialize(memoryStream);

                m.From = new MailAddress(serializedMessage.From);

                foreach (string recipient in serializedMessage.To)
                    m.To.Add(new MailAddress(recipient));

                m.Subject = serializedMessage.Subject;
                m.Body = serializedMessage.Body;
                m.IsBodyHtml = serializedMessage.IsBodyHtml;

                foreach (SerializableAttachment serializedAttachment in serializedMessage.Attachments)
                {
                    var attachment = new Attachment(new MemoryStream(serializedAttachment.ContentStream), serializedAttachment.FileName);
                    m.Attachments.Add(attachment);
                }

                return m;
            }
        }

    }

    [Serializable]
    public class SerializableMailMessage
    {
        private string _from;
        private List<string> _to;
        private string _subject;
        private string _body;
        private bool _isBodyHtml;
        private List<SerializableAttachment> _attachments;

        // Public properties with Getters and Setters
        public string From
        {
            get { return _from; }
            set { _from = value; }
        }

        public List<string> To
        {
            get { return _to; }
            set { _to = value; }
        }

        public string Subject
        {
            get { return _subject; }
            set { _subject = value; }
        }

        public string Body
        {
            get { return _body; }
            set { _body = value; }
        }

        public bool IsBodyHtml
        {
            get { return _isBodyHtml; }
            set { _isBodyHtml = value; }
        }

        public List<SerializableAttachment> Attachments
        {
            get { return _attachments; }
            set { _attachments = value; }
        }

        // Constructor to convert MailMessage to SerializableMailMessage
        public SerializableMailMessage(MailMessage mailMessage)
        {
            _from = mailMessage.From.ToString();
            _to = mailMessage.To.Select(t => t.ToString()).ToList();
            _subject = mailMessage.Subject;
            _body = mailMessage.Body;
            _isBodyHtml = mailMessage.IsBodyHtml;
            _attachments = mailMessage.Attachments.Select(a => new SerializableAttachment(a)).ToList();
        }
    }

    [Serializable]
    public class SerializableAttachment
    {
        private string _fileName;
        private byte[] _contentStream;

        // Public properties with Getters and Setters
        public string FileName
        {
            get { return _fileName; }
            set { _fileName = value; }
        }

        public byte[] ContentStream
        {
            get { return _contentStream; }
            set { _contentStream = value; }
        }

        // Constructor to convert Attachment to SerializableAttachment
        public SerializableAttachment(System.Net.Mail.Attachment attachment)
        {
            _fileName = attachment.Name;
            using (MemoryStream ms = new MemoryStream())
            {
                CopyTo(attachment.ContentStream, ms);
                _contentStream = ms.ToArray();
            }
        }

        public void CopyTo(Stream source, Stream destination, int bufferSize = 81920)
        {
            if (source == null) throw new ArgumentNullException(nameof(source));
            if (destination == null) throw new ArgumentNullException(nameof(destination));
            if (bufferSize <= 0) throw new ArgumentOutOfRangeException(nameof(bufferSize));

            byte[] buffer = new byte[bufferSize];
            int bytesRead;
            do
            {
                bytesRead = source.Read(buffer, 0, bufferSize);
                if (bytesRead > 0)
                {
                    destination.Write(buffer, 0, bytesRead);
                }
            } while (bytesRead > 0);
        }
    }


}
