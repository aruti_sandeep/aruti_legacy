﻿Imports System
Imports System.Reflection
Imports System.Runtime.InteropServices

' General Information about an assembly is controlled through the following 
' set of attributes. Change these attribute values to modify the information
' associated with an assembly.

' Review the values of the assembly attributes

<Assembly: AssemblyTitle("Arutireflex")> 
<Assembly: AssemblyDescription("")> 
<Assembly: AssemblyCompany("Aruti")> 
<Assembly: AssemblyProduct("Arutireflex")> 
<Assembly: AssemblyCopyright("Copyright ©Aruti 2019")> 
<Assembly: AssemblyTrademark("")> 

<Assembly: ComVisible(False)>

'The following GUID is for the ID of the typelib if this project is exposed to COM
<Assembly: Guid("69741d57-8ef7-4998-8a93-f63c8048ff17")> 

' Version information for an assembly consists of the following four values:
'
'      Major Version
'      Minor Version 
'      Build Number
'      Revision
'
' You can specify all the values or you can default the Build and Revision Numbers 
' by using the '*' as shown below:
' <Assembly: AssemblyVersion("1.0.*")> 

<Assembly: AssemblyVersion("9.0.1.28")> 
<Assembly: AssemblyFileVersion("9.0.1.28")> 
