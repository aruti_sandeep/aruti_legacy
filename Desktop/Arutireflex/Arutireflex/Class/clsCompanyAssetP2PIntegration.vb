﻿Imports System.Runtime.Serialization
Imports Newtonsoft.Json
Imports System.Windows

Public Class clsCompanyAssetP2PIntegration

    Dim clsqueryFixedAssetRequest As New queryFixedAssetRequest

#Region "Properties"

    Public Property queryFixedAssetRequest() As queryFixedAssetRequest
        Get
            Return clsqueryFixedAssetRequest
        End Get
        Set(ByVal value As queryFixedAssetRequest)
            clsqueryFixedAssetRequest = value
        End Set
    End Property

#End Region


End Class


<DataContract()> _
Public Class queryFixedAssetRequest

    Dim mstrCompanyCode As String = Nothing
    Dim mstrEmployeeCode As String = Nothing
    Dim mstrAssetGroup As String = Nothing
    Dim mstrAssetType As String = Nothing
    Dim mstrAssetLocation As String = Nothing
    Dim mstrPersonResponsible As String = Nothing

#Region "Properties"


    <DataMember()> _
    Public Property CompanyCode() As String
        Get
            Return mstrCompanyCode
        End Get
        Set(ByVal value As String)
            mstrCompanyCode = value
        End Set
    End Property

    <DataMember()> _
    Public Property EmployeeCode() As String
        Get
            Return mstrEmployeeCode
        End Get
        Set(ByVal value As String)
            mstrEmployeeCode = value
        End Set
    End Property

    <DataMember(EmitDefaultValue:=False)> _
   Public Property AssetGroup() As String
        Get
            Return mstrAssetGroup
        End Get
        Set(ByVal value As String)
            mstrAssetGroup = value
        End Set
    End Property

    <DataMember(EmitDefaultValue:=False)> _
 Public Property AssetType() As String
        Get
            Return mstrAssetType
        End Get
        Set(ByVal value As String)
            mstrAssetType = value
        End Set
    End Property

    <DataMember(EmitDefaultValue:=False)> _
   Public Property AssetLocation() As String
        Get
            Return mstrAssetLocation
        End Get
        Set(ByVal value As String)
            mstrAssetLocation = value
        End Set
    End Property

    <DataMember(EmitDefaultValue:=False)> _
Public Property PersonResponsible() As String
        Get
            Return mstrPersonResponsible
        End Get
        Set(ByVal value As String)
            mstrPersonResponsible = value
        End Set
    End Property

#End Region

End Class
