﻿Public Class clsSpreedsheetHelper
    '''<summary>returns an empty cell when a blank cell is encountered
    '''</summary>
    Public Shared Function GetRowCells(ByVal row As DocumentFormat.OpenXml.Spreadsheet.Row) As IEnumerable(Of DocumentFormat.OpenXml.Spreadsheet.Cell)
        Dim currentCount As Integer = 0
        Dim iCells As New List(Of DocumentFormat.OpenXml.Spreadsheet.Cell)()
        For Each cell As DocumentFormat.OpenXml.Spreadsheet.Cell In row.Descendants(Of DocumentFormat.OpenXml.Spreadsheet.Cell)()
            Dim columnName As String = GetColumnName(cell.CellReference)
            Dim currentColumnIndex As Integer = ConvertColumnNameToNumber(columnName)
            Do While currentCount < currentColumnIndex
                iCells.Add(New DocumentFormat.OpenXml.Spreadsheet.Cell())
                ''Yield(New DocumentFormat.OpenXml.Spreadsheet.Cell())
                currentCount += 1
            Loop
            iCells.Add(cell)
            currentCount += 1
        Next cell
        Return iCells
    End Function

    ''' <summary>
    ''' Given a cell name, parses the specified cell to get the column name.
    ''' </summary>
    ''' <param name="cellReference">Address of the cell (ie. B2)</param>
    ''' <returns>Column Name (ie. B)</returns>
    Public Shared Function GetColumnName(ByVal cellReference As String) As String
        ' Match the column name portion of the cell name.
        Dim regex = New System.Text.RegularExpressions.Regex("[A-Za-z]+")
        Dim match = regex.Match(cellReference)

        Return match.Value
    End Function

    ''' <summary>
    ''' Given just the column name (no row index),
    ''' it will return the zero based column index.
    ''' </summary>
    ''' <param name="columnName">Column Name (ie. A or AB)</param>
    ''' <returns>Zero based index if the conversion was successful</returns>
    ''' <exception cref="ArgumentException">thrown if the given string
    ''' contains characters other than uppercase letters</exception>
    Public Shared Function ConvertColumnNameToNumber(ByVal columnName As String) As Integer
        Dim alpha = New System.Text.RegularExpressions.Regex("^[A-Z]+$")
        If Not alpha.IsMatch(columnName) Then
            Throw New ArgumentException()
        End If

        Dim colLetters() As Char = columnName.ToCharArray()
        Array.Reverse(colLetters)

        Dim convertedValue As Integer = 0
        For i As Integer = 0 To colLetters.Length - 1
            Dim letter As Char = colLetters(i)
            Dim current As Integer = If(i = 0, AscW(letter) - 65, AscW(letter) - 64) ' ASCII 'A' = 65
            convertedValue += current * CInt(Math.Truncate(Math.Pow(26, i)))
        Next i

        Return convertedValue
    End Function
End Class