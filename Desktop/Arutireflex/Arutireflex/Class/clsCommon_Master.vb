﻿Imports Arutireflex.Arutireflex
Imports System.Data.SqlClient

Public Class clsCommon_Master


#Region " Private variables "
    Dim mstrMessage As String = ""
    Private mintMasterunkid As Integer
    Private mstrCode As String = String.Empty
    Private mstrAlias As String = String.Empty
    Private mstrName As String = String.Empty
    Private mstrName1 As String = String.Empty
    Private mstrName2 As String = String.Empty
    Private mintMastertype As Integer
    Private mblnIsactive As Boolean = True
    Private mintQualificationGrp_Level As Integer
    Private mintDependant_Limit As Integer = 0
    Private mintCourseTypeId As Integer = 0
    Private mblnIsRefreeAllowed As Boolean = False
    Private mblnIsSyncWithrecruitment As Boolean = False
    Private mblnMedicalBenefit As Boolean = False
    Private mstrDescription As String = ""
    Private mblnIsMandatoryForNewEmp As Boolean = False
    Private mblnIsUseInAssetDeclaration As Boolean = False
    Private mblnIsForTrainingRequisition As Boolean = False
    Private mblnIsCRBenefit As Boolean = False
    Private mintMaxCountForCRBenefit As Integer = 0
    Private mintMinAgeLimitForCRBenefit As Integer = 0
    Private mintUserunkid As Integer = 0
    Private mintJobMstunkid As Integer = 0

#End Region

    'Pinkal (20-Dec-2024) -- Start
    'Toyota Enhancement : (A1X-2901) Toyota - Approver reminders on pending approvals for employee movements. 

#Region "Enum"

    Public Enum enCommonMaster
        ADVERTISE_CATEGORY = 1
        ALLERGIES = 2
        ASSET_CONDITION = 3
        BENEFIT_GROUP = 4
        BLOOD_GROUP = 5
        COMPLEXION = 6
        DISABILITIES = 7
        EMPLOYEMENT_TYPE = 8
        ETHNICITY = 9
        EYES_COLOR = 10
        HAIR_COLOR = 11
        IDENTITY_TYPES = 12
        INTERVIEW_TYPE = 13
        LANGUAGES = 14
        MARRIED_STATUS = 15
        MEMBERSHIP_CATEGORY = 16
        PAY_TYPE = 17
        QUALIFICATION_COURSE_GROUP = 18
        RELATIONS = 19
        RELIGION = 20
        RESULT_GROUP = 21
        SHIFT_TYPE = 22
        SKILL_CATEGORY = 23
        TITLE = 24
        TRAINING_RESOURCES = 25
        ASSET_STATUS = 26
        ASSETS = 27
        LETTER_TYPE = 28
        SALINC_REASON = 29 'Sohail (08 Nov 2011)
        TRAINING_COURSEMASTER = 30
        STRATEGIC_GOAL = 31
        SOURCES_FUNDINGS = 32
        ATTACHMENT_TYPES = 33
        VACANCY_MASTER = 34
        DISCIPLINE_COMMITTEE = 35
        OFFENCE_CATEGORY = 36
        COMMITTEE_MEMBERS_CATEGORY = 37
        APPRAISAL_ACTIONS = 38
        SECTOR_ROUTE = 39
        COMPETENCE_CATEGORIES = 40
        ASSESSMENT_SCALE_GROUP = 41
        TRANSFERS = 42
        PROMOTIONS = 43
        RECATEGORIZE = 44
        PROBATION = 45
        CONFIRMATION = 46
        SUSPENSION = 47
        TERMINATION = 48
        RE_HIRE = 49
        WORK_PERMIT = 50
        RETIREMENTS = 51
        COST_CENTER = 52
        COMPETENCE_GROUP = 53
        PROV_REGION_1 = 54
        ROAD_STREET_1 = 55
        CHIEFDOM = 56
        VILLAGE = 57
        TOWN_1 = 58
        DISC_REPONSE_TYPE = 59
        GE_APPRAISAL_ACTIONS = 60
        BSC_APPRAISAL_ACTIONS = 61
        RESIDENT_PERMIT = 62
        GRIEVANCE_TYPE = 63
        ASSET_SECTOR = 64
        ASSET_SECURITIES = 65
        TRAINING_MODE = 66
        LIABILITY_TYPE_ASSET_DECLARATION_TEMPLATE2 = 67
        GOAL_UNIT_OF_MEASURE = 68
        DEPENDANT_ACTIVE_INACTIVE_REASON = 69
        PERFORMANCE_CUSTOM_ITEM = 70
        VACANCY_SOURCE = 71
        EXEMPTION = 72
        TRAINING_REQUEST_COST_ITEM = 73
        DISCIPLINE_CASE_OPEN_REASON = 74
        DISCIPLINE_CONTRARY_TO = 75
        STAFF_TRANSFER = 76
        CLAIM_JOURNAL = 77
        CLAIM_PAYMENTMODE = 78
    End Enum

#End Region

    'Pinkal (20-Dec-2024) -- End

#Region " Properties "

    ''' <summary>
    ''' Purpose: Get Message from Class 
    ''' Modify By: Anjan
    ''' </summary>
    Public ReadOnly Property _Message() As String
        Get
            Return mstrMessage
        End Get
    End Property

    ''' <summary>
    ''' Purpose: Get or Set masterunkid
    ''' Modify By: Anjan
    ''' </summary>
    Public Property _Masterunkid() As Integer
        Get
            Return mintMasterunkid
        End Get
        Set(ByVal value As Integer)
            mintMasterunkid = Value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set code
    ''' Modify By: Anjan
    ''' </summary>
    Public Property _Code() As String
        Get
            Return mstrCode
        End Get
        Set(ByVal value As String)
            mstrCode = Value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set alias
    ''' Modify By: Anjan
    ''' </summary>
    Public Property _Alias() As String
        Get
            Return mstrAlias
        End Get
        Set(ByVal value As String)
            mstrAlias = Value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set name
    ''' Modify By: Anjan
    ''' </summary>
    Public Property _Name() As String
        Get
            Return mstrName
        End Get
        Set(ByVal value As String)
            mstrName = Value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set name1
    ''' Modify By: Anjan
    ''' </summary>
    Public Property _Name1() As String
        Get
            Return mstrName1
        End Get
        Set(ByVal value As String)
            mstrName1 = Value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set name2
    ''' Modify By: Anjan
    ''' </summary>
    Public Property _Name2() As String
        Get
            Return mstrName2
        End Get
        Set(ByVal value As String)
            mstrName2 = Value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set mastertype
    ''' Modify By: Anjan
    ''' </summary>
    Public Property _Mastertype() As Integer
        Get
            Return mintMastertype
        End Get
        Set(ByVal value As Integer)
            mintMastertype = Value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set isactive
    ''' Modify By: Anjan
    ''' </summary>
    Public Property _Isactive() As Boolean
        Get
            Return mblnIsactive
        End Get
        Set(ByVal value As Boolean)
            mblnIsactive = Value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set Qualification Group Level
    ''' Modify By: Anjan
    ''' </summary>
    Public Property _QualificationGrp_Level() As Integer
        Get
            Return mintQualificationGrp_Level
        End Get
        Set(ByVal value As Integer)
            mintQualificationGrp_Level = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set dependant_limit
    ''' Modify By: Sandeep J. Sharma
    ''' </summary>
    Public Property _Dependant_Limit() As Integer
        Get
            Return mintDependant_Limit
        End Get
        Set(ByVal value As Integer)
            mintDependant_Limit = Value
        End Set
    End Property
    
    Public Property _CouseTypeId() As Integer
        Get
            Return mintCourseTypeId
        End Get
        Set(ByVal value As Integer)
            mintCourseTypeId = value
        End Set
    End Property

    Public Property _IsRefreeAllowed() As Boolean
        Get
            Return mblnIsRefreeAllowed
        End Get
        Set(ByVal value As Boolean)
            mblnIsRefreeAllowed = value
        End Set
    End Property

    Public Property _IsMedicalBenefit()
        Get
            Return mblnMedicalBenefit
        End Get
        Set(ByVal value)
            mblnMedicalBenefit = value
        End Set
    End Property

    Public Property _IsSyncWithrecruitment() As Boolean
        Get
            Return mblnIsSyncWithrecruitment
        End Get
        Set(ByVal value As Boolean)
            mblnIsSyncWithrecruitment = value
        End Set
    End Property
  
    Public Property _Description() As String
        Get
            Return mstrDescription
        End Get
        Set(ByVal value As String)
            mstrDescription = value
        End Set
    End Property
   
    Public Property _IsMandatoryForNewEmp() As Boolean
        Get
            Return mblnIsMandatoryForNewEmp
        End Get
        Set(ByVal value As Boolean)
            mblnIsMandatoryForNewEmp = value
        End Set
    End Property
   
    Public Property _IsUseInAssetDeclaration() As Boolean
        Get
            Return mblnIsUseInAssetDeclaration
        End Get
        Set(ByVal value As Boolean)
            mblnIsUseInAssetDeclaration = value
        End Set
    End Property
   
    Public Property _IsForTrainingRequisition() As Boolean
        Get
            Return mblnIsForTrainingRequisition
        End Get
        Set(ByVal value As Boolean)
            mblnIsForTrainingRequisition = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set IsCRBenefit
    ''' Modify By: Pinkal Jariwala
    ''' </summary>
    Public Property _IsCRBenefit() As Boolean
        Get
            Return mblnIsCRBenefit
        End Get
        Set(ByVal value As Boolean)
            mblnIsCRBenefit = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set MaxCountForCRBenefit
    ''' Modify By: Pinkal Jariwala
    ''' </summary>
    Public Property _MaxCountForCRBenefit() As Integer
        Get
            Return mintMaxCountForCRBenefit
        End Get
        Set(ByVal value As Integer)
            mintMaxCountForCRBenefit = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set MinAgeLimitForCRBenefit
    ''' Modify By: Pinkal Jariwala
    ''' </summary>
    Public Property _MinAgeLimitForCRBenefit() As Integer
        Get
            Return mintMinAgeLimitForCRBenefit
        End Get
        Set(ByVal value As Integer)
            mintMinAgeLimitForCRBenefit = value
        End Set
    End Property

    Public Property _Userunkid() As Integer
        Get
            Return mintUserunkid
        End Get
        Set(ByVal value As Integer)
            mintUserunkid = value
        End Set
    End Property
   
    Public Property _JobMstunkid() As Integer
        Get
            Return mintJobMstunkid
        End Get
        Set(ByVal value As Integer)
            mintJobMstunkid = value
        End Set
    End Property

#End Region

    Public Function CommonMasterExist(ByVal sqlCn As SqlConnection, ByVal mstrDatabase As String, ByVal MasterTypeID As enCommonMaster, Optional ByVal strCode As String = "", Optional ByVal strName As String = "", Optional ByVal intCommonMstUnkid As Integer = -1) As Boolean
        Dim strQ As String = ""
        Dim mdtCommonMst As DataTable = Nothing
        Try

            strQ = "SELECT " & _
                      "  masterunkid " & _
                      ", code " & _
                      ", alias " & _
                      ", name " & _
                      ", name1 " & _
                      ", name2 " & _
                      ", mastertype " & _
                      ", qlevel " & _
                      ", isactive " & _
                      ", dependant_limit " & _
                      ", coursetypeid " & _
                      ", isrefreeallowed " & _
                      ", ISNULL(ismedicalbenefit,0) AS ismedicalbenefit " & _
                      ", ISNULL(istrainingrequisition,0) AS istrainingrequisition " & _
                      ", ISNULL(iscrbenefit,0) AS iscrbenefit " & _
                      ", ISNULL(minagelimit_crdependants,0) AS minagelimit_crdependants " & _
                      ", ISNULL(maxcnt_crdependants,0) AS maxcnt_crdependants " & _
                      " FROM " & mstrDatabase & "..cfcommon_master " & _
                      " WHERE isactive = 1 "

            If strCode.Trim.Length > 0 Then
                strQ &= " AND code = @code "
            End If

            If strName.Trim.Length > 0 Then
                strQ &= " AND name = @name "
            End If

            If MasterTypeID > 0 Then
                strQ &= " AND mastertype = @mastertypeID "
            End If

            If intCommonMstUnkid > 0 Then
                strQ &= " AND masterunkid <> @masterunkid "
            End If

            Dim sqlcmd As New SqlCommand
            sqlcmd.CommandText = strQ
            sqlcmd.Connection = sqlCn
            sqlcmd.Parameters.Clear()

            sqlcmd.Parameters.Add("@code", SqlDbType.NVarChar).Value = strCode.Trim
            sqlcmd.Parameters.Add("@name", SqlDbType.NVarChar).Value = strName.Trim
            sqlcmd.Parameters.Add("@mastertypeID", SqlDbType.Int).Value = MasterTypeID
            sqlcmd.Parameters.Add("@masterunkid", SqlDbType.Int).Value = intCommonMstUnkid

            Dim sqlDataadp As New SqlDataAdapter()
            sqlDataadp.SelectCommand = sqlcmd
            mdtCommonMst = New DataTable
            sqlDataadp.Fill(mdtCommonMst)

            Return mdtCommonMst.Rows.Count > 0

        Catch ex As Exception
            Throw New Exception("CommonMasterExist : " & ex.Message)
        End Try
    End Function

    Public Function GetList(ByVal sqlCn As SqlConnection, ByVal mstrDatabase As String, ByVal intMasterTypeID As Integer, Optional ByVal strTableName As String = "List", Optional ByVal intLanguageID As Integer = -1, Optional ByVal blnOnlyActive As Boolean = True, Optional ByVal strFilter As String = "") As DataSet
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception
        Try
            strQ = " SELECT " & _
                      "  masterunkid " & _
                      ", code " & _
                      ", alias " & _
                      ", CASE @Language WHEN 1 THEN name1 " & _
                      "                             WHEN 2 THEN name2 " & _
                      "  ELSE name END AS name " & _
                      ", mastertype " & _
                      ", ISNULL(qlevel,0) qlevel " & _
                      ", isactive " & _
                      ", ISNULL(dependant_limit,0) AS dependant_limit " & _
                      ", ISNULL(coursetypeid,0) AS coursetypeid " & _
                      ", ISNULL(isrefreeallowed,0) AS isrefreeallowed " & _
                      ", ISNULL(ismedicalbenefit,0) AS ismedicalbenefit " & _
                      ", ISNULL(issyncwithrecruitment,0) AS issyncwithrecruitment " & _
                      ", ISNULL(description,'') AS description " & _
                      ", ISNULL(ismandatoryfornewemp, 0) AS ismandatoryfornewemp " & _
                      ", ISNULL(isuseinassetdeclaration ,0) AS isuseinassetdeclaration  " & _
                      ", ISNULL(istrainingrequisition, 0) AS istrainingrequisition " & _
                      ", ISNULL(iscrbenefit, 0) AS iscrbenefit" & _
                      ", ISNULL(minagelimit_crdependants, 0) AS minagelimit_crdependants " & _
                      ", ISNULL(maxcnt_crdependants, 0) AS maxcnt_crdependants " & _
                      ", ISNULL(jobmstunkid, 0) AS jobmstunkid " & _
                      " FROM " & mstrDatabase & "..cfcommon_master " & _
                      " WHERE mastertype = @MasterTypeId "

            If blnOnlyActive Then
                strQ &= " AND isactive = 1 "
            End If

            If strFilter.Length > 0 Then
                strQ &= strFilter
            End If

            strQ &= " ORDER BY name "

            Dim sqlcmd As New SqlCommand
            sqlcmd.CommandText = strQ
            sqlcmd.Connection = sqlCn
            sqlcmd.Parameters.Clear()

            sqlcmd.Parameters.Add("@MasterTypeId", SqlDbType.Int).Value = intMasterTypeID
            sqlcmd.Parameters.Add("@Language", SqlDbType.Int).Value = intLanguageID

            Dim sqlDataadp As New SqlDataAdapter()
            sqlDataadp.SelectCommand = sqlcmd
            dsList = New DataSet()
            sqlDataadp.Fill(dsList)

            Return dsList
        Catch ex As Exception
            Throw New Exception("GetList : " & ex.Message)
            Return Nothing
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
        End Try
        Return dsList
    End Function

    Public Function Insert(ByVal sqlCn As SqlConnection, ByVal mstrDatabase As String) As Boolean
        If CommonMasterExist(sqlCn, mstrDatabase, mintMastertype, mstrCode, , -1) Then
            mstrMessage = "This Code is already defined. Please define new Code."
            Return False
        End If

        If CommonMasterExist(sqlCn, mstrDatabase, mintMastertype, , mstrName, -1) Then
            mstrMessage = "This Name is already defined. Please define new Name."
            Return False
        End If


        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception
        Dim SqlTran As SqlTransaction = Nothing
        Dim oCmd As SqlCommand = Nothing
        Try

            strQ = "INSERT INTO " & mstrDatabase & "..cfcommon_master ( " & _
                     "  code " & _
                     ", alias " & _
                     ", name " & _
                     ", name1 " & _
                     ", name2 " & _
                     ", mastertype " & _
                     ", qlevel " & _
                     ", isactive" & _
                     ", dependant_limit" & _
                     ", coursetypeid " & _
                     ", isrefreeallowed " & _
                     ", ismedicalbenefit " & _
                     ", issyncwithrecruitment " & _
                     ", description " & _
                     ", ismandatoryfornewemp " & _
                     ", isuseinassetdeclaration " & _
                     ", istrainingrequisition " & _
                     ", iscrbenefit " & _
                     ", minagelimit_crdependants " & _
                     ", maxcnt_crdependants " & _
                     ", jobmstunkid " & _
                     " ) VALUES (" & _
                     "  @code " & _
                     ", @alias " & _
                     ", @name " & _
                     ", @name1 " & _
                     ", @name2 " & _
                     ", @mastertype " & _
                     ", @qlevel " & _
                     ", @isactive" & _
                     ", @dependant_limit" & _
                     ", @coursetypeid " & _
                     ", @isrefreeallowed " & _
                     ", @ismedicalbenefit " & _
                     ", @issyncwithrecruitment " & _
                     ", @description " & _
                     ", @ismandatoryfornewemp " & _
                     ", @isuseinassetdeclaration " & _
                     ", @istrainingrequisition " & _
                     ", @iscrbenefit " & _
                     ", @minagelimit_crdependants " & _
                     ", @maxcnt_crdependants " & _
                     ", @jobmstunkid " & _
                     " ); SELECT @@identity"



            SqlTran = sqlCn.BeginTransaction    'START TRANSACTION

            oCmd = New SqlCommand(strQ, sqlCn, SqlTran)
            oCmd.Parameters.Clear()
            oCmd.Parameters.Add("@code", SqlDbType.NVarChar).Value = mstrCode.ToString()
            oCmd.Parameters.Add("@alias", SqlDbType.NVarChar).Value = mstrAlias.ToString()
            oCmd.Parameters.Add("@name", SqlDbType.NVarChar).Value = mstrName.ToString()
            oCmd.Parameters.Add("@name1", SqlDbType.NVarChar).Value = mstrName1.ToString()
            oCmd.Parameters.Add("@name2", SqlDbType.NVarChar).Value = mstrName2.ToString()
            oCmd.Parameters.Add("@mastertype", SqlDbType.Int).Value = mintMastertype.ToString()
            oCmd.Parameters.Add("@isactive", SqlDbType.Bit).Value = mblnIsactive.ToString()
            oCmd.Parameters.Add("@qlevel", SqlDbType.Int).Value = mintQualificationGrp_Level.ToString()
            oCmd.Parameters.Add("@dependant_limit", SqlDbType.Int).Value = mintDependant_Limit.ToString()
            oCmd.Parameters.Add("@coursetypeid", SqlDbType.Int).Value = mintCourseTypeId.ToString()
            oCmd.Parameters.Add("@isrefreeallowed", SqlDbType.Bit).Value = mblnIsRefreeAllowed.ToString()
            oCmd.Parameters.Add("@ismedicalbenefit", SqlDbType.Bit).Value = mblnMedicalBenefit.ToString()
            oCmd.Parameters.Add("@issyncwithrecruitment", SqlDbType.Bit).Value = mblnIsSyncWithrecruitment.ToString()
            oCmd.Parameters.Add("@description", SqlDbType.NVarChar).Value = mstrDescription.ToString()
            oCmd.Parameters.Add("@ismandatoryfornewemp", SqlDbType.Bit).Value = mblnIsMandatoryForNewEmp.ToString()
            oCmd.Parameters.Add("@isuseinassetdeclaration", SqlDbType.Bit).Value = mblnIsUseInAssetDeclaration.ToString()
            oCmd.Parameters.Add("@istrainingrequisition", SqlDbType.Bit).Value = mblnIsForTrainingRequisition.ToString()
            oCmd.Parameters.Add("@iscrbenefit", SqlDbType.Bit).Value = mblnIsCRBenefit.ToString()
            oCmd.Parameters.Add("@minagelimit_crdependants", SqlDbType.Int).Value = mintMinAgeLimitForCRBenefit.ToString()
            oCmd.Parameters.Add("@maxcnt_crdependants", SqlDbType.Int).Value = mintMaxCountForCRBenefit.ToString()
            oCmd.Parameters.Add("@jobmstunkid", SqlDbType.Int).Value = mintJobMstunkid

            mintMasterunkid = oCmd.ExecuteScalar()

            Dim objReflex As New Arutireflex
            objReflex.sqlCn = sqlCn
            If objReflex.Insert_AtLog(oCmd.Transaction, mstrDatabase, 1, "cfcommon_master", "masterunkid", mintMasterunkid, False, 1) = False Then
                Throw New Exception
            End If
            objReflex = Nothing

            SqlTran.Commit()

            Return True
        Catch ex As Exception
            SqlTran.Rollback()
            Throw New Exception("Common Mastet Insert : " & ex.Message)
            Return False
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
        End Try
    End Function

End Class
