﻿Imports System.IO
Imports System.Text
Imports System.Windows
Imports System.Windows.Documents

Public Class RtfToPlaintTextConverter
    Private Sub New()
    End Sub

    Public Shared Function RtfToPlainText(ByVal rtf As String, Optional ByVal encoding As Encoding = Nothing) As String
        Dim safeEncoding = If(encoding, System.Text.Encoding.UTF8)
        Dim doc = New FlowDocument()
        Dim range = New TextRange(doc.ContentStart, doc.ContentEnd)
        Using stream = New MemoryStream(safeEncoding.GetBytes(rtf))
            Using outStream = New MemoryStream()
                range.Load(stream, DataFormats.Rtf)
                range.Save(outStream, DataFormats.Text)
                outStream.Seek(0, SeekOrigin.Begin)
                Return safeEncoding.GetString(outStream.ToArray()).Trim()
            End Using
        End Using
    End Function

End Class
