﻿Imports System.Data.SqlClient

Public Class clsEmployee_Assets_Tran

#Region " Private variables "
    Private mstrMessage As String = ""
    Private mintAssetstranunkid As Integer
    Private mintEmployeeunkid As Integer
    Private mintAssetunkid As Integer
    Private mstrAsset_No As String = String.Empty
    Private mintConditionunkid As Integer
    Private mintStatusunkid As Integer
    Private mdtAssign_Return_Date As Date
    Private mstrRemark As String = String.Empty
    Private mintUserunkid As Integer
    Private mblnIsvoid As Boolean
    Private mintVoiduserunkid As Integer
    Private mstrVoidreason As String = String.Empty
    Private mdtVoiddatetime As Date
    Private mstrAssetserial_No As String = String.Empty
    Private mstrWebFormName As String = String.Empty
    Private mintLogEmployeeUnkid As Integer = -1
    Private mstrWebClientIP As String = String.Empty
    Private mstrWebHostName As String = String.Empty
    Private mstrAssetGroup As String = String.Empty
#End Region

#Region " Properties "

    ''' <summary>
    ''' Purpose: Get Message from Class 
    ''' Modify By: Sandeep J. Sharma
    ''' </summary>
    Public ReadOnly Property _Message() As String
        Get
            Return mstrMessage
        End Get
    End Property

    ''' <summary>
    ''' Purpose: Get or Set assetstranunkid
    ''' Modify By: Sandeep J. Sharma
    ''' </summary>
    Public Property _Assetstranunkid() As Integer
        Get
            Return mintAssetstranunkid
        End Get
        Set(ByVal value As Integer)
            mintAssetstranunkid = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set employeeunkid
    ''' Modify By: Sandeep J. Sharma
    ''' </summary>
    Public Property _Employeeunkid() As Integer
        Get
            Return mintEmployeeunkid
        End Get
        Set(ByVal value As Integer)
            mintEmployeeunkid = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set assetunkid
    ''' Modify By: Sandeep J. Sharma
    ''' </summary>
    Public Property _Assetunkid() As Integer
        Get
            Return mintAssetunkid
        End Get
        Set(ByVal value As Integer)
            mintAssetunkid = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set asset_no
    ''' Modify By: Sandeep J. Sharma
    ''' </summary>
    Public Property _Asset_No() As String
        Get
            Return mstrAsset_No
        End Get
        Set(ByVal value As String)
            mstrAsset_No = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set conditionunkid
    ''' Modify By: Sandeep J. Sharma
    ''' </summary>
    Public Property _Conditionunkid() As Integer
        Get
            Return mintConditionunkid
        End Get
        Set(ByVal value As Integer)
            mintConditionunkid = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set asset_statusunkid
    ''' Modify By: Sandeep J. Sharma
    ''' </summary>
    Public Property _Statusunkid() As Integer
        Get
            Return mintStatusunkid
        End Get
        Set(ByVal value As Integer)
            mintStatusunkid = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set assign_return_date
    ''' Modify By: Sandeep J. Sharma
    ''' </summary>
    Public Property _Assign_Return_Date() As Date
        Get
            Return mdtAssign_Return_Date
        End Get
        Set(ByVal value As Date)
            mdtAssign_Return_Date = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set remark
    ''' Modify By: Sandeep J. Sharma
    ''' </summary>
    Public Property _Remark() As String
        Get
            Return mstrRemark
        End Get
        Set(ByVal value As String)
            mstrRemark = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set userunkid
    ''' Modify By: Sandeep J. Sharma
    ''' </summary>
    Public Property _Userunkid() As Integer
        Get
            Return mintUserunkid
        End Get
        Set(ByVal value As Integer)
            mintUserunkid = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set isvoid
    ''' Modify By: Sandeep J. Sharma
    ''' </summary>
    Public Property _Isvoid() As Boolean
        Get
            Return mblnIsvoid
        End Get
        Set(ByVal value As Boolean)
            mblnIsvoid = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set voiduserunkid
    ''' Modify By: Sandeep J. Sharma
    ''' </summary>
    Public Property _Voiduserunkid() As Integer
        Get
            Return mintVoiduserunkid
        End Get
        Set(ByVal value As Integer)
            mintVoiduserunkid = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set voidreason
    ''' Modify By: Sandeep J. Sharma
    ''' </summary>
    Public Property _Voidreason() As String
        Get
            Return mstrVoidreason
        End Get
        Set(ByVal value As String)
            mstrVoidreason = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set voiddatetime
    ''' Modify By: Sandeep J. Sharma
    ''' </summary>
    Public Property _Voiddatetime() As Date
        Get
            Return mdtVoiddatetime
        End Get
        Set(ByVal value As Date)
            mdtVoiddatetime = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set assetserial_no
    ''' Modify By: Sandeep J. Sharma
    ''' </summary>
    Public Property _Assetserial_No() As String
        Get
            Return mstrAssetserial_No
        End Get
        Set(ByVal value As String)
            mstrAssetserial_No = value
        End Set
    End Property

    Public Property _WebFormName() As String
        Get
            Return mstrWebFormName
        End Get
        Set(ByVal value As String)
            mstrWebFormName = value
        End Set
    End Property

    Public WriteOnly Property _LoginEmployeeUnkid() As Integer
        Set(ByVal value As Integer)
            mintLogEmployeeUnkid = value
        End Set
    End Property

    Public WriteOnly Property _WebClientIP() As String
        Set(ByVal value As String)
            mstrWebClientIP = value
        End Set
    End Property

    Public WriteOnly Property _WebHostName() As String
        Set(ByVal value As String)
            mstrWebHostName = value
        End Set
    End Property

    Dim mstrCompanyCode As String = ""
    Public WriteOnly Property _CompanyCode() As String
        Set(ByVal value As String)
            mstrCompanyCode = value
        End Set
    End Property

    Dim mstrEmployeeCode As String = String.Empty
    Public WriteOnly Property _EmployeeCode() As String
        Set(ByVal value As String)
            mstrEmployeeCode = value
        End Set
    End Property

    Dim mstrEmployee As String = String.Empty
    Public WriteOnly Property _Employee() As String
        Set(ByVal value As String)
            mstrEmployee = value
        End Set
    End Property

    Dim mstrAssetCategory As String = String.Empty
    Public WriteOnly Property _AssetCategory() As String
        Set(ByVal value As String)
            mstrAssetCategory = value
        End Set
    End Property

    Dim mstrPostedData As String = String.Empty
    Public WriteOnly Property _PostedData() As String
        Set(ByVal value As String)
            mstrPostedData = value
        End Set
    End Property

    Dim mstrResponseData As String = String.Empty
    Public WriteOnly Property _ResponseData() As String
        Set(ByVal value As String)
            mstrResponseData = value
        End Set
    End Property

    Dim mblnIsPostedError As Boolean = False
    Public WriteOnly Property _IsPostedError() As Boolean
        Set(ByVal value As Boolean)
            mblnIsPostedError = value
        End Set
    End Property


#End Region

    Public Function isExist(ByVal sqlCn As SqlConnection, ByVal mstrDatabase As String, ByVal intEmpId As String, ByVal intAssetId As Integer, ByVal strSrNo As String, ByVal mstrCompanyAssetP2PServiceURL As String, Optional ByVal intUnkid As Integer = -1) As Boolean
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception
        Try

            strQ = "SELECT " & _
                      "  assetstranunkid " & _
                      ", employeeunkid " & _
                      ", assetunkid " & _
                      ", asset_no " & _
                      ", conditionunkid " & _
                      ", asset_statusunkid " & _
                      ", assign_return_date " & _
                      ", remark " & _
                      ", userunkid " & _
                      ", isvoid " & _
                      ", voiduserunkid " & _
                      ", voidreason " & _
                      ", ISNULL(posteddata,'') AS  posteddata " & _
                      ", ISNULL(responsedata,'') AS responsedata " & _
                      ", ISNULL(ispostederror,0) AS ispostederror " & _
                      " FROM " & mstrDatabase & "..hremployee_assets_tran " & _
                      " WHERE employeeunkid = @EmpId " & _
                      " AND assetunkid = @AssetId " & _
                      " AND ISNULL(isvoid,0) = 0 "

            If mstrCompanyAssetP2PServiceURL.Trim.Length <= 0 Then
                strQ &= " AND assetserial_no = @assetserial_no "
            Else
                strQ &= " AND asset_no = @asset_no "
            End If

            If intUnkid > 0 Then
                strQ &= " AND assetstranunkid <> @assetstranunkid"
            End If

            Dim sqlcmd As New SqlCommand
            sqlcmd.CommandText = strQ
            sqlcmd.Connection = sqlCn
            sqlcmd.Parameters.Clear()

            sqlcmd.Parameters.Add("@assetserial_no", SqlDbType.NVarChar).Value = strSrNo
            sqlcmd.Parameters.Add("@asset_no", SqlDbType.NVarChar).Value = strSrNo
            sqlcmd.Parameters.Add("@EmpId", SqlDbType.Int).Value = intEmpId
            sqlcmd.Parameters.Add("@AssetId", SqlDbType.Int).Value = intAssetId
            sqlcmd.Parameters.Add("@assetstranunkid", SqlDbType.Int).Value = intUnkid

            Dim sqlDataadp As New SqlDataAdapter()
            sqlDataadp.SelectCommand = sqlcmd
            dsList = New DataSet
            sqlDataadp.Fill(dsList)

            Return dsList.Tables(0).Rows.Count > 0
        Catch ex As Exception
            Throw New Exception("isExist : " & ex.Message)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
        End Try
    End Function

    Public Function Insert(ByVal sqlCn As SqlConnection, ByVal mstrDatabase As String, ByVal mstrCompanyAssetP2PServiceURL As String, ByVal xCompanyId As Integer) As Boolean
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception
        Dim blnFalg As Boolean = False
        Dim SqlTran As SqlTransaction = Nothing
        Dim oCmd As SqlCommand = Nothing

        If isExist(sqlCn, mstrDatabase, mintEmployeeunkid, mintAssetunkid, mstrAssetserial_No, mstrCompanyAssetP2PServiceURL) Then
            mstrMessage = "This Asset is already linked to selected employee. Please add new asset."
            Return False
        Else
            If isExist(sqlCn, mstrDatabase, mintEmployeeunkid, mintAssetunkid, mstrAsset_No, mstrCompanyAssetP2PServiceURL, mintAssetstranunkid) Then
                mstrMessage = "This Asset is already linked to selected employee. Please add new asset."
                Return False
            End If
        End If

        Try

            strQ = "INSERT INTO " & mstrDatabase & "..hremployee_assets_tran ( " & _
                      "  employeeunkid " & _
                      ", assetunkid " & _
                      ", asset_no " & _
                      ", conditionunkid " & _
                      ", asset_statusunkid " & _
                      ", assign_return_date " & _
                      ", remark " & _
                      ", userunkid " & _
                      ", isvoid " & _
                      ", voiduserunkid " & _
                      ", voidreason" & _
                      ", voiddatetime " & _
                      ", assetserial_no " & _
                      ", posteddata " & _
                      ", responsedata " & _
                      ", ispostederror " & _
                    ") VALUES (" & _
                      "  @employeeunkid " & _
                      ", @assetunkid " & _
                      ", @asset_no " & _
                      ", @conditionunkid " & _
                      ", @asset_statusunkid " & _
                      ", @assign_return_date " & _
                      ", @remark " & _
                      ", @userunkid " & _
                      ", @isvoid " & _
                      ", @voiduserunkid " & _
                      ", @voidreason" & _
                      ", @voiddatetime " & _
                      ", @assetserial_no " & _
                      ", @posteddata " & _
                      ", @responsedata " & _
                      ", @ispostederror " & _
                    "); SELECT @@identity"


            SqlTran = sqlCn.BeginTransaction    'START TRANSACTION


            oCmd = New SqlCommand(strQ, sqlCn, SqlTran)
            oCmd.Parameters.Clear()

            oCmd.Parameters.Add("@employeeunkid", SqlDbType.Int).Value = mintEmployeeunkid.ToString()
            oCmd.Parameters.Add("@assetunkid", SqlDbType.Int).Value = mintAssetunkid.ToString()
            oCmd.Parameters.Add("@asset_no", SqlDbType.NVarChar).Value = mstrAsset_No.ToString()
            oCmd.Parameters.Add("@conditionunkid", SqlDbType.Int).Value = mintConditionunkid.ToString()
            oCmd.Parameters.Add("@asset_statusunkid", SqlDbType.Int).Value = mintStatusunkid.ToString()
            oCmd.Parameters.Add("@assign_return_date", SqlDbType.DateTime).Value = mdtAssign_Return_Date
            oCmd.Parameters.Add("@remark", SqlDbType.NVarChar).Value = mstrRemark.ToString()
            oCmd.Parameters.Add("@userunkid", SqlDbType.Int).Value = mintUserunkid.ToString()
            oCmd.Parameters.Add("@isvoid", SqlDbType.Bit).Value = mblnIsvoid.ToString()
            oCmd.Parameters.Add("@voiduserunkid", SqlDbType.Int).Value = mintVoiduserunkid.ToString()
            oCmd.Parameters.Add("@voidreason", SqlDbType.NVarChar).Value = mstrVoidreason.ToString()

            If mdtVoiddatetime = Nothing Then
                oCmd.Parameters.Add("@voiddatetime", SqlDbType.DateTime).Value = DBNull.Value
            Else
                oCmd.Parameters.Add("@voiddatetime", SqlDbType.DateTime).Value = mdtVoiddatetime
            End If

            oCmd.Parameters.Add("@assetserial_no", SqlDbType.NVarChar).Value = mstrAssetserial_No.ToString()
            oCmd.Parameters.Add("@posteddata", SqlDbType.NVarChar).Value = mstrPostedData.ToString()
            oCmd.Parameters.Add("@responsedata", SqlDbType.NVarChar).Value = mstrResponseData.ToString()
            oCmd.Parameters.Add("@ispostederror", SqlDbType.Bit).Value = mblnIsPostedError.ToString()

            mintAssetstranunkid = oCmd.ExecuteScalar()

            If InsertAuditTrailForEmpAssetsTran(sqlCn, SqlTran, mstrDatabase, 1) = False Then
                SqlTran.Rollback()
                Return False
            End If

            If InsertEmployeeAssetsStatus_Tran(sqlCn, SqlTran, mstrDatabase, xCompanyId, True) = False Then
                SqlTran.Rollback()
                Return False
            End If

            SqlTran.Commit()

            Return True
        Catch ex As Exception
            SqlTran.Rollback()
            Throw New Exception("clsEmployee_Assets_Tran : Insert; Module Name: " & ex.Message)
            Return False
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
        End Try
    End Function

    Public Function Update(ByVal sqlCn As SqlConnection, ByVal mstrDatabase As String, ByVal mstrCompanyAssetP2PServiceURL As String) As Boolean

        If mstrCompanyAssetP2PServiceURL.Trim.Length <= 0 Then
            If isExist(sqlCn, mstrDatabase, mintEmployeeunkid, mintAssetunkid, mstrAssetserial_No, mstrCompanyAssetP2PServiceURL, mintAssetstranunkid) Then
                mstrMessage = "This Asset is already linked to selected employee. Please add new asset."
                Return False
            End If
        Else
            If isExist(sqlCn, mstrDatabase, mintEmployeeunkid, mintAssetunkid, mstrAsset_No, mstrCompanyAssetP2PServiceURL, mintAssetstranunkid) Then
                mstrMessage = "This Asset is already linked to selected employee. Please add new asset."
                Return False
            End If
        End If

        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception
        Dim SqlTran As SqlTransaction = Nothing
        Dim oCmd As SqlCommand = Nothing

        Try

            strQ = "UPDATE hremployee_assets_tran SET " & _
                       "  employeeunkid = @employeeunkid" & _
                       ", assetunkid = @assetunkid" & _
                       ", asset_no = @asset_no" & _
                       ", conditionunkid = @conditionunkid" & _
                       ", asset_statusunkid = @asset_statusunkid" & _
                       ", assign_return_date = @assign_return_date" & _
                       ", remark = @remark" & _
                       ", userunkid = @userunkid" & _
                       ", isvoid = @isvoid" & _
                       ", voiduserunkid = @voiduserunkid" & _
                       ", voidreason = @voidreason " & _
                       ", voiddatetime = @voiddatetime " & _
                       ", assetserial_no = @assetserial_no " & _
                       ", posteddata = @posteddata " & _
                       ", responsedata = @responsedata " & _
                       ", ispostederror = @ispostederror " & _
                       " WHERE assetstranunkid = @assetstranunkid "

            SqlTran = sqlCn.BeginTransaction    'START TRANSACTION

            oCmd = New SqlCommand(strQ, sqlCn, SqlTran)
            oCmd.Parameters.Clear()

            oCmd.Parameters.Add("@employeeunkid", SqlDbType.Int).Value = mintEmployeeunkid.ToString()
            oCmd.Parameters.Add("@assetunkid", SqlDbType.Int).Value = mintAssetunkid.ToString()
            oCmd.Parameters.Add("@asset_no", SqlDbType.NVarChar).Value = mstrAsset_No.ToString()
            oCmd.Parameters.Add("@conditionunkid", SqlDbType.Int).Value = mintConditionunkid.ToString()
            oCmd.Parameters.Add("@asset_statusunkid", SqlDbType.Int).Value = mintStatusunkid.ToString()
            oCmd.Parameters.Add("@assign_return_date", SqlDbType.DateTime).Value = mdtAssign_Return_Date
            oCmd.Parameters.Add("@remark", SqlDbType.NVarChar).Value = mstrRemark.ToString()
            oCmd.Parameters.Add("@userunkid", SqlDbType.Int).Value = mintUserunkid.ToString()
            oCmd.Parameters.Add("@isvoid", SqlDbType.Bit).Value = mblnIsvoid.ToString()
            oCmd.Parameters.Add("@voiduserunkid", SqlDbType.Int).Value = mintVoiduserunkid.ToString()
            oCmd.Parameters.Add("@voidreason", SqlDbType.NVarChar).Value = mstrVoidreason.ToString()

            If mdtVoiddatetime = Nothing Then
                oCmd.Parameters.Add("@voiddatetime", SqlDbType.DateTime).Value = DBNull.Value
            Else
                oCmd.Parameters.Add("@voiddatetime", SqlDbType.DateTime).Value = mdtVoiddatetime
            End If

            oCmd.Parameters.Add("@assetserial_no", SqlDbType.NVarChar).Value = mstrAssetserial_No.ToString()
            oCmd.Parameters.Add("@posteddata", SqlDbType.NVarChar).Value = mstrPostedData.ToString()
            oCmd.Parameters.Add("@responsedata", SqlDbType.NVarChar).Value = mstrResponseData.ToString()
            oCmd.Parameters.Add("@ispostederror", SqlDbType.Bit).Value = mblnIsPostedError.ToString()

            If InsertAuditTrailForEmpAssetsTran(sqlCn, SqlTran, mstrDatabase, 2) = False Then
                SqlTran.Rollback()
                Return False
            End If

            SqlTran.Commit()

            Return True
        Catch ex As Exception
            SqlTran.Rollback()
            Throw New Exception("clsEmployee_Assets_Tran : Update; Module Name: " & ex.Message)
            Return False
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
        End Try
    End Function

    Public Function InsertAuditTrailForEmpAssetsTran(ByVal sqlCn As SqlConnection, ByVal SqlTran As SqlTransaction, ByVal mstrDatabase As String, ByVal AuditType As Integer) As Boolean
        Dim strQ As String = ""
        Dim exForce As Exception = Nothing
        Dim oCmd As SqlCommand = Nothing
        Try

            strQ = "INSERT INTO " & mstrDatabase & "..athremployee_assets_tran ( " & _
                      "  assetstranunkid " & _
                      ", employeeunkid " & _
                      ", assetunkid " & _
                      ", asset_no " & _
                      ", conditionunkid " & _
                      ", asset_statusunkid " & _
                      ", assign_return_date " & _
                      ", remark " & _
                      ", audittype " & _
                      ", audituserunkid " & _
                      ", auditdatetime " & _
                      ", ip " & _
                      ", machine_name " & _
                      ", assetserial_no " & _
                      ", form_name " & _
                      ", module_name1 " & _
                      ", module_name2 " & _
                      ", module_name3 " & _
                      ", module_name4 " & _
                      ", module_name5 " & _
                      ", isweb " & _
                      ", loginemployeeunkid " & _
                      ", posteddata " & _
                      ", responsedata " & _
                      ", ispostederror " & _
                      ") VALUES (" & _
                      "  @assetstranunkid " & _
                      ", @employeeunkid " & _
                      ", @assetunkid " & _
                      ", @asset_no " & _
                      ", @conditionunkid " & _
                      ", @asset_statusunkid " & _
                      ", @assign_return_date" & _
                      ", @remark " & _
                      ", @audittype " & _
                      ", @audituserunkid " & _
                      ", GETDATE() " & _
                      ", @ip" & _
                      ", @machine_name " & _
                      ", @assetserial_no " & _
                      ", @form_name " & _
                      ", @module_name1 " & _
                      ", @module_name2 " & _
                      ", @module_name3 " & _
                      ", @module_name4 " & _
                      ", @module_name5 " & _
                      ", @isweb " & _
                      ", @loginemployeeunkid " & _
                      ", @posteddata " & _
                      ", @responsedata " & _
                      ", @ispostederror " & _
                      "); "


            oCmd = New SqlCommand(strQ, sqlCn, SqlTran)
            oCmd.Parameters.Clear()

            oCmd.Parameters.Add("@assetstranunkid", SqlDbType.Int).Value = mintAssetstranunkid.ToString()
            oCmd.Parameters.Add("@employeeunkid", SqlDbType.Int).Value = mintEmployeeunkid.ToString()
            oCmd.Parameters.Add("@assetunkid", SqlDbType.Int).Value = mintAssetunkid.ToString()
            oCmd.Parameters.Add("@asset_no", SqlDbType.NVarChar).Value = mstrAsset_No.ToString()
            oCmd.Parameters.Add("@conditionunkid", SqlDbType.Int).Value = mintConditionunkid.ToString()
            oCmd.Parameters.Add("@asset_statusunkid", SqlDbType.Int).Value = mintStatusunkid.ToString()
            oCmd.Parameters.Add("@assign_return_date", SqlDbType.DateTime).Value = mdtAssign_Return_Date
            oCmd.Parameters.Add("@remark", SqlDbType.NVarChar).Value = mstrRemark.ToString()
            oCmd.Parameters.Add("@audittype", SqlDbType.Int).Value = AuditType.ToString
            oCmd.Parameters.Add("@audituserunkid", SqlDbType.Int).Value = mintUserunkid
            oCmd.Parameters.Add("@ip", SqlDbType.NVarChar).Value = mstrWebClientIP
            oCmd.Parameters.Add("@machine_name", SqlDbType.NVarChar).Value = mstrWebHostName
            oCmd.Parameters.Add("@assetserial_no", SqlDbType.NVarChar).Value = mstrAssetserial_No.ToString
            oCmd.Parameters.Add("@form_name", SqlDbType.NVarChar).Value = "ArutiReflex"
            oCmd.Parameters.Add("@isweb", SqlDbType.Bit).Value = False
            oCmd.Parameters.Add("@module_name1", SqlDbType.NVarChar).Value = ""
            oCmd.Parameters.Add("@module_name2", SqlDbType.NVarChar).Value = ""
            oCmd.Parameters.Add("@module_name3", SqlDbType.NVarChar).Value = ""
            oCmd.Parameters.Add("@module_name4", SqlDbType.NVarChar).Value = ""
            oCmd.Parameters.Add("@module_name5", SqlDbType.NVarChar).Value = ""
            oCmd.Parameters.Add("@loginemployeeunkid", SqlDbType.Int).Value = 0
            oCmd.Parameters.Add("@posteddata", SqlDbType.NVarChar).Value = mstrPostedData.ToString()
            oCmd.Parameters.Add("@responsedata", SqlDbType.NVarChar).Value = mstrResponseData.ToString()
            oCmd.Parameters.Add("@ispostederror", SqlDbType.Bit).Value = mblnIsPostedError
            oCmd.ExecuteNonQuery()

            Return True
        Catch ex As Exception
            Throw New Exception("InsertAuditTrailForEmpAssetsTran : " & ex.Message)
            Return False
        End Try
    End Function

    Public Function InsertEmployeeAssetsStatus_Tran(ByVal Sqlcn As SqlConnection, ByVal sqlTran As SqlTransaction, ByVal mstrDatabase As String, ByVal xCompanyId As Integer, Optional ByVal blnFromEmpAssetInsert As Boolean = False) As Boolean
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception
        Dim blnFlag As Boolean = False
        Dim oCmd As SqlCommand = Nothing
        Try

            strQ = "INSERT INTO " & mstrDatabase & "..hremployee_assets_status_tran ( " & _
                      "  assetstranunkid " & _
                      ", status_date " & _
                      ", statusunkid " & _
                      ", remark " & _
                      ", userunkid " & _
                      ", isvoid " & _
                      ", voiddatetime " & _
                      ", voiduserunkid" & _
                    ") VALUES (" & _
                      "  @assetstranunkid " & _
                      ", @status_date " & _
                      ", @statusunkid " & _
                      ", @remark " & _
                      ", @userunkid " & _
                      ", @isvoid " & _
                      ", @voiddatetime " & _
                      ", @voiduserunkid" & _
                    "); SELECT @@identity"


            oCmd = New SqlCommand(strQ, Sqlcn, sqlTran)
            oCmd.Parameters.Clear()


            oCmd.Parameters.Add("@assetstranunkid", SqlDbType.Int).Value = mintAssetstranunkid.ToString()
            oCmd.Parameters.Add("@status_date", SqlDbType.DateTime).Value = mdtAssign_Return_Date
            oCmd.Parameters.Add("@statusunkid", SqlDbType.Int).Value = mintStatusunkid.ToString()
            oCmd.Parameters.Add("@remark", SqlDbType.NVarChar).Value = mstrRemark.ToString()
            oCmd.Parameters.Add("@userunkid", SqlDbType.Int).Value = mintUserunkid.ToString
            oCmd.Parameters.Add("@isvoid", SqlDbType.Bit).Value = mblnIsvoid.ToString

            If mdtVoiddatetime = Nothing Then
                oCmd.Parameters.Add("@voiddatetime", SqlDbType.DateTime).Value = DBNull.Value
            Else
                oCmd.Parameters.Add("@voiddatetime", SqlDbType.DateTime).Value = mdtVoiddatetime
            End If
            oCmd.Parameters.Add("@voiduserunkid", SqlDbType.Int).Value = mintVoiduserunkid.ToString()

            Dim mintEmployeeassetstatustranunkid As Integer = oCmd.ExecuteScalar()

            Return True
        Catch ex As Exception
            Throw New Exception("clsEmployee_Assets_Tran : InsertEmployeeAssetsStatus_Tran; Module Name: " & ex.Message)
            Return False
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
        End Try
    End Function


End Class
