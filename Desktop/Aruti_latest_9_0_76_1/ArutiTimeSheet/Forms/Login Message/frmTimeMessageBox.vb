Option Strict On

Imports eZeeCommonLib
Imports Aruti.Data

Public Class frmTimeMessageBox

#Region " Private Variables "
    Private ReadOnly mstrModuleName As String = "frmTimeMessageBox"
    Private mblnCancel As Boolean = True
    Friend mstrMessage As String = String.Empty
#End Region

#Region " Display Dialog "

    Public Function displayDialog(ByVal mstrEmplyeeName As String) As Boolean
        Try
            objlblMessage.Text = mstrMessage
            objlblName.Text = mstrEmplyeeName
            objlblTime.Text = Now.ToShortTimeString
            Me.ShowDialog()
            Return Not mblnCancel
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "displayDialog", mstrModuleName)
        End Try
    End Function

#End Region

#Region " Form's Events "

    Private Sub frmTimeMessageBox_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Try
            Call Set_Logo(Me, enArutiApplicatinType.Aruti_Payroll)
            tmrTimeOut.Enabled = True
            lblTimer.Text = "5"
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmTimeMessageBox_Load", mstrMessage)
        End Try
    End Sub

#End Region

#Region " Button's Events "

    Private Sub btnclose_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnclose.Click
        Me.Close()
    End Sub

#End Region

#Region " Timer's Event"

    Private Sub tmrTimeOut_Tick(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles tmrTimeOut.Tick
        Try
            If Val(lblTimer.Text) > 0 Then
                lblTimer.Text = (Val(lblTimer.Text) - 1).ToString
            Else
                Me.Close()
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "tmrTimeOut_Tick", mstrMessage)
        End Try
    End Sub

#End Region

   
End Class
