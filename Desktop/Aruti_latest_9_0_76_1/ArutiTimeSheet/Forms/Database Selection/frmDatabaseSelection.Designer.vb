﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmDatabaseSelection
    Inherits eZee.Common.eZeeForm

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmDatabaseSelection))
        Me.pnlDatabaseSelection = New System.Windows.Forms.Panel
        Me.objelLine1 = New eZee.Common.eZeeLine
        Me.btnCancel = New eZee.Common.eZeeLightButton(Me.components)
        Me.btnOpen = New eZee.Common.eZeeLightButton(Me.components)
        Me.lvDatabase = New System.Windows.Forms.ListView
        Me.colhProperty = New System.Windows.Forms.ColumnHeader
        Me.picSideImage = New System.Windows.Forms.PictureBox
        Me.eZeeHeader = New eZee.Common.eZeeHeader
        Me.pnlDatabaseSelection.SuspendLayout()
        CType(Me.picSideImage, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'pnlDatabaseSelection
        '
        Me.pnlDatabaseSelection.Controls.Add(Me.objelLine1)
        Me.pnlDatabaseSelection.Controls.Add(Me.btnCancel)
        Me.pnlDatabaseSelection.Controls.Add(Me.btnOpen)
        Me.pnlDatabaseSelection.Controls.Add(Me.lvDatabase)
        Me.pnlDatabaseSelection.Controls.Add(Me.picSideImage)
        Me.pnlDatabaseSelection.Controls.Add(Me.eZeeHeader)
        Me.pnlDatabaseSelection.Dock = System.Windows.Forms.DockStyle.Fill
        Me.pnlDatabaseSelection.Location = New System.Drawing.Point(0, 0)
        Me.pnlDatabaseSelection.Name = "pnlDatabaseSelection"
        Me.pnlDatabaseSelection.Size = New System.Drawing.Size(567, 281)
        Me.pnlDatabaseSelection.TabIndex = 1
        '
        'objelLine1
        '
        Me.objelLine1.LineOrientation = eZee.Common.eZeeLine.Orientation.Horizontal
        Me.objelLine1.Location = New System.Drawing.Point(172, 219)
        Me.objelLine1.Name = "objelLine1"
        Me.objelLine1.Size = New System.Drawing.Size(385, 12)
        Me.objelLine1.TabIndex = 17
        Me.objelLine1.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'btnCancel
        '
        Me.btnCancel.BackColor = System.Drawing.Color.White
        Me.btnCancel.BackgroundImage = CType(resources.GetObject("btnCancel.BackgroundImage"), System.Drawing.Image)
        Me.btnCancel.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.btnCancel.BorderColor = System.Drawing.Color.Empty
        Me.btnCancel.ButtonType = eZee.Common.eZeeLightButton.enButtonType.NormalButton
        Me.btnCancel.DialogResult = System.Windows.Forms.DialogResult.No
        Me.btnCancel.FlatAppearance.BorderSize = 0
        Me.btnCancel.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnCancel.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnCancel.ForeColor = System.Drawing.Color.Black
        Me.btnCancel.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.btnCancel.GradientForeColor = System.Drawing.Color.Black
        Me.btnCancel.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.btnCancel.HoverGradientForeColor = System.Drawing.Color.Black
        Me.btnCancel.Image = Global.ArutiTimeSheet.My.Resources.Resources.Close_24x24
        Me.btnCancel.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.btnCancel.Location = New System.Drawing.Point(449, 234)
        Me.btnCancel.Name = "btnCancel"
        Me.btnCancel.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.btnCancel.PressedGradientForeColor = System.Drawing.Color.Black
        Me.btnCancel.Size = New System.Drawing.Size(108, 40)
        Me.btnCancel.TabIndex = 2
        Me.btnCancel.Text = "&Close"
        Me.btnCancel.UseVisualStyleBackColor = True
        '
        'btnOpen
        '
        Me.btnOpen.BackColor = System.Drawing.Color.White
        Me.btnOpen.BackgroundImage = CType(resources.GetObject("btnOpen.BackgroundImage"), System.Drawing.Image)
        Me.btnOpen.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.btnOpen.BorderColor = System.Drawing.Color.Empty
        Me.btnOpen.ButtonType = eZee.Common.eZeeLightButton.enButtonType.NormalButton
        Me.btnOpen.DialogResult = System.Windows.Forms.DialogResult.OK
        Me.btnOpen.FlatAppearance.BorderSize = 0
        Me.btnOpen.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnOpen.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnOpen.ForeColor = System.Drawing.Color.Black
        Me.btnOpen.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.btnOpen.GradientForeColor = System.Drawing.Color.Black
        Me.btnOpen.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.btnOpen.HoverGradientForeColor = System.Drawing.Color.Black
        Me.btnOpen.Image = Global.ArutiTimeSheet.My.Resources.Resources.BackOffice_32
        Me.btnOpen.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.btnOpen.Location = New System.Drawing.Point(449, 176)
        Me.btnOpen.Name = "btnOpen"
        Me.btnOpen.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.btnOpen.PressedGradientForeColor = System.Drawing.Color.Black
        Me.btnOpen.Size = New System.Drawing.Size(108, 40)
        Me.btnOpen.TabIndex = 15
        Me.btnOpen.Text = "&Open"
        Me.btnOpen.UseCompatibleTextRendering = True
        Me.btnOpen.UseVisualStyleBackColor = False
        '
        'lvDatabase
        '
        Me.lvDatabase.Columns.AddRange(New System.Windows.Forms.ColumnHeader() {Me.colhProperty})
        Me.lvDatabase.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lvDatabase.FullRowSelect = True
        Me.lvDatabase.GridLines = True
        Me.lvDatabase.HideSelection = False
        Me.lvDatabase.Location = New System.Drawing.Point(172, 66)
        Me.lvDatabase.MultiSelect = False
        Me.lvDatabase.Name = "lvDatabase"
        Me.lvDatabase.Size = New System.Drawing.Size(271, 150)
        Me.lvDatabase.TabIndex = 14
        Me.lvDatabase.UseCompatibleStateImageBehavior = False
        Me.lvDatabase.View = System.Windows.Forms.View.Details
        '
        'colhProperty
        '
        Me.colhProperty.Tag = "colhProperty"
        Me.colhProperty.Text = "Database"
        Me.colhProperty.Width = 266
        '
        'picSideImage
        '
        Me.picSideImage.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch
        Me.picSideImage.Dock = System.Windows.Forms.DockStyle.Left
        Me.picSideImage.Image = Global.ArutiTimeSheet.My.Resources.Resources.Aruti
        Me.picSideImage.Location = New System.Drawing.Point(0, 60)
        Me.picSideImage.Name = "picSideImage"
        Me.picSideImage.Size = New System.Drawing.Size(166, 221)
        Me.picSideImage.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me.picSideImage.TabIndex = 13
        Me.picSideImage.TabStop = False
        '
        'eZeeHeader
        '
        Me.eZeeHeader.BackColor = System.Drawing.SystemColors.Control
        Me.eZeeHeader.BorderColor = System.Drawing.SystemColors.ControlDark
        Me.eZeeHeader.DescriptionForeColor = System.Drawing.SystemColors.ControlDarkDark
        Me.eZeeHeader.Dock = System.Windows.Forms.DockStyle.Top
        Me.eZeeHeader.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.eZeeHeader.GradiantStyle = eZee.Common.eZeeHeader.GradientStyle.Central
        Me.eZeeHeader.GradientColor1 = System.Drawing.SystemColors.Window
        Me.eZeeHeader.GradientColor2 = System.Drawing.SystemColors.Control
        Me.eZeeHeader.HeaderTextForeColor = System.Drawing.SystemColors.ControlText
        Me.eZeeHeader.Icon = Nothing
        Me.eZeeHeader.Location = New System.Drawing.Point(0, 0)
        Me.eZeeHeader.Message = ""
        Me.eZeeHeader.Name = "eZeeHeader"
        Me.eZeeHeader.Size = New System.Drawing.Size(567, 60)
        Me.eZeeHeader.TabIndex = 3
        Me.eZeeHeader.Title = "Select Database"
        '
        'frmDatabaseSelection
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(567, 281)
        Me.Controls.Add(Me.pnlDatabaseSelection)
        Me.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle
        Me.KeyPreview = True
        Me.MaximizeBox = False
        Me.MinimizeBox = False
        Me.Name = "frmDatabaseSelection"
        Me.ShowInTaskbar = False
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent
        Me.Text = "Database Selection"
        Me.pnlDatabaseSelection.ResumeLayout(False)
        CType(Me.picSideImage, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents btnOpen As eZee.Common.eZeeLightButton
    Friend WithEvents picSideImage As System.Windows.Forms.PictureBox
    Friend WithEvents pnlDatabaseSelection As System.Windows.Forms.Panel
    Friend WithEvents btnCancel As eZee.Common.eZeeLightButton
    Friend WithEvents lvDatabase As System.Windows.Forms.ListView
    Friend WithEvents colhProperty As System.Windows.Forms.ColumnHeader
    Friend WithEvents eZeeHeader As eZee.Common.eZeeHeader
    Friend WithEvents objelLine1 As eZee.Common.eZeeLine
End Class
