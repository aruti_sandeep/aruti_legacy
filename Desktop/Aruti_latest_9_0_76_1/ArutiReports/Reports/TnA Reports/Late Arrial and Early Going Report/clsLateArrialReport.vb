Imports eZeeCommonLib
Imports Aruti.Data
Imports System.IO
Imports System.Text
Imports ExcelWriter

Public Class clsLateArrialReport
    Inherits IReportData
    Private Shared ReadOnly mstrModuleName As String = "clsLateArrialReport"
    Private mstrReportId As String = enArutiReport.Late_Arrival_Report  '179
    Dim objDataOperation As clsDataOperation


#Region " Constructor "
    Public Sub New(ByVal intLangId As Integer, ByVal intCompanyId As Integer) 'S.SANDEEP |24-Sep-2020| -- START --END
        Me.setReportData(CInt(mstrReportId), intLangId, intCompanyId) 'S.SANDEEP |24-Sep-2020| -- START --END
        'Pinkal (14-Feb-2022) -- Start
        'Enhancement TRA : TnA Module Enhancement for TRA.
        'Call Create_OnDetailReport()
        'Pinkal (14-Feb-2022) -- End
    End Sub
#End Region

#Region " Private variables "

    Private mintReportId As Integer = 0
    Private mstrReportTypeName As String = ""
    Private mintYearId As Integer = 0
    Private mdtFromDate As DateTime = Nothing
    Private mdtToDate As DateTime = Nothing
    Private mintEmployeeId As Integer = 0
    Private mstrEmployeeName As String = ""
    Private mstrAdvance_Filter As String = String.Empty
    Private mintViewIndex As Integer = -1
    Private mstrViewByIds As String = ""
    Private mstrViewByName As String = ""
    Private mstrAnalysis_Fields As String = ""
    Private mstrAnalysis_Join As String = ""
    Private mstrAnalysis_OrderBy As String = ""
    Private mstrAnalysis_OrderBy_GName As String = ""
    Private mstrReport_GroupName As String = ""
    Private mstrExportReportPath As String = ConfigParameter._Object._ExportReportPath
    Private mblnOpenAfterExport As Boolean = ConfigParameter._Object._OpenAfterExport
    Private mstrOrderByQuery As String = ""
    Private mintUserUnkid As Integer = -1
    Private mintCompanyUnkid As Integer = -1
    Private mstrUserAccessFilter As String = String.Empty
    Private mblnIncludeAccessFilterQry As Boolean = True
    Private mblnIgnoreBlankRows As Boolean = False
    'S.SANDEEP |29-MAR-2019| -- START
    'ENHANCEMENT : 0003555 {PAPAYE}.
    Private mblnShowShiftName As Boolean = False
    'S.SANDEEP |29-MAR-2019| -- END


    'Pinkal (08-Aug-2019) -- Start
    'Enhancement [004032 - TRZ] - Working on Off days hours should not count as part of the total Base hours in Employee timesheet report.
    Private mblnDonotconsiderLeave As Boolean = False
    'Pinkal (08-Aug-2019) -- End


    'Pinkal (27-Oct-2020) -- Start
    'Enhancement St.Jude -   Working on Automatic Attendance download and send Report to Manager.
    Private mblnGenerateReportForNotification As Boolean = False
    'Pinkal (27-Oct-2020) -- End


    'Pinkal (14-Feb-2022) -- Start
    'Enhancement TRA : TnA Module Enhancement for TRA.
    Dim Rpt As CrystalDecisions.CrystalReports.Engine.ReportClass
    Private mblnEachEmployeeOnPage As Boolean = False
    Private mstrGroupName As String = ""
    Private mintLocationId As Integer = 0
    Private mstrLocation As String = ""
    'Pinkal (14-Feb-2022) -- End 


#End Region

#Region " Properties "

    Public WriteOnly Property _ReportId() As Integer
        Set(ByVal value As Integer)
            mintReportId = value
        End Set
    End Property

    Public WriteOnly Property _ReportTypeName() As String
        Set(ByVal value As String)
            mstrReportTypeName = value
        End Set
    End Property

    Public WriteOnly Property _YearId() As Integer
        Set(ByVal value As Integer)
            mintYearId = value
        End Set
    End Property

    Public WriteOnly Property _FromDate() As Date
        Set(ByVal value As Date)
            mdtFromDate = value
        End Set
    End Property

    Public WriteOnly Property _ToDate() As Date
        Set(ByVal value As Date)
            mdtToDate = value
        End Set
    End Property

    Public WriteOnly Property _EmployeeID() As Integer
        Set(ByVal value As Integer)
            mintEmployeeId = value
        End Set
    End Property

    Public WriteOnly Property _EmployeeName() As String
        Set(ByVal value As String)
            mstrEmployeeName = value
        End Set
    End Property

    Public WriteOnly Property _Advance_Filter() As String
        Set(ByVal value As String)
            mstrAdvance_Filter = value
        End Set
    End Property

    Public WriteOnly Property _ViewIndex() As Integer
        Set(ByVal value As Integer)
            mintViewIndex = value
        End Set
    End Property

    Public WriteOnly Property _ViewByIds() As String
        Set(ByVal value As String)
            mstrViewByIds = value
        End Set
    End Property

    Public WriteOnly Property _ViewByName() As String
        Set(ByVal value As String)
            mstrViewByName = value
        End Set
    End Property

    Public WriteOnly Property _Analysis_Fields() As String
        Set(ByVal value As String)
            mstrAnalysis_Fields = value
        End Set
    End Property

    Public WriteOnly Property _Analysis_Join() As String
        Set(ByVal value As String)
            mstrAnalysis_Join = value
        End Set
    End Property

    Public WriteOnly Property _Analysis_OrderBy() As String
        Set(ByVal value As String)
            mstrAnalysis_OrderBy = value
        End Set
    End Property

    Public WriteOnly Property _Analysis_OrderBy_GName() As String
        Set(ByVal value As String)
            mstrAnalysis_OrderBy_GName = value
        End Set
    End Property

    Public WriteOnly Property _Report_GroupName() As String
        Set(ByVal value As String)
            mstrReport_GroupName = value
        End Set
    End Property

    Public WriteOnly Property _ExportReportPath() As String
        Set(ByVal value As String)
            mstrExportReportPath = value
        End Set
    End Property

    Public WriteOnly Property _OpenAfterExport() As Boolean
        Set(ByVal value As Boolean)
            mblnOpenAfterExport = value
        End Set
    End Property

    Public WriteOnly Property _CompanyUnkId() As Integer
        Set(ByVal value As Integer)
            mintCompanyUnkid = value
        End Set
    End Property

    Public WriteOnly Property _UserUnkId() As Integer
        Set(ByVal value As Integer)
            mintUserUnkid = value
        End Set
    End Property

    Public WriteOnly Property _UserAccessFilter() As String
        Set(ByVal value As String)
            mstrUserAccessFilter = value
        End Set
    End Property

    Public Property _IncludeAccessFilterQry() As Boolean
        Get
            Return mblnIncludeAccessFilterQry
        End Get
        Set(ByVal value As Boolean)
            mblnIncludeAccessFilterQry = value
        End Set
    End Property

    Public Property _IgnoreBlankRows() As Boolean
        Get
            Return mblnIgnoreBlankRows
        End Get
        Set(ByVal value As Boolean)
            mblnIgnoreBlankRows = value
        End Set
    End Property

    'S.SANDEEP |29-MAR-2019| -- START
    'ENHANCEMENT : 0003555 {PAPAYE}.
    Public WriteOnly Property _ShowShiftName() As Boolean
        Set(ByVal value As Boolean)
            mblnShowShiftName = value
        End Set
    End Property
    'S.SANDEEP |29-MAR-2019| -- END

    'Pinkal (08-Aug-2019) -- Start
    'Enhancement [004032 - TRZ] - Working on Off days hours should not count as part of the total Base hours in Employee timesheet report.
    Public WriteOnly Property _DoNotConsiderLeave() As Boolean
        Set(ByVal value As Boolean)
            mblnDonotconsiderLeave = value
        End Set
    End Property
    'Pinkal (08-Aug-2019) -- End

    'Pinkal (27-Oct-2020) -- Start
    'Enhancement St.Jude -   Working on Automatic Attendance download and send Report to Manager.
    Public Property _GenerateReportForNotification() As Boolean
        Get
            Return mblnGenerateReportForNotification
        End Get
        Set(ByVal value As Boolean)
            mblnGenerateReportForNotification = value
        End Set
    End Property
    'Pinkal (27-Oct-2020) -- End


    'Pinkal (14-Feb-2022) -- Start
    'Enhancement TRA : TnA Module Enhancement for TRA.

    Public ReadOnly Property _Rpt() As CrystalDecisions.CrystalReports.Engine.ReportClass
        Get
            Return Rpt
        End Get
    End Property

    Public WriteOnly Property _LocationId() As Integer
        Set(ByVal value As Integer)
            mintLocationId = value
        End Set
    End Property

    Public WriteOnly Property _Location() As String
        Set(ByVal value As String)
            mstrLocation = value
        End Set
    End Property

    Public WriteOnly Property _GroupName() As String
        Set(ByVal value As String)
            mstrGroupName = value
        End Set
    End Property

    Public WriteOnly Property _ShowEachEmployeeOnNewPage() As Boolean
        Set(ByVal value As Boolean)
            mblnEachEmployeeOnPage = value
        End Set
    End Property

    'Pinkal (14-Feb-2022) -- End

#End Region

#Region "Public Function & Procedures "

    Public Sub SetDefaultValue()
        Try
            mintYearId = 0
            mdtFromDate = Nothing
            mdtToDate = Nothing
            mintEmployeeId = 0
            mstrEmployeeName = ""
            mblnIgnoreBlankRows = True
            mstrAdvance_Filter = ""
            mintViewIndex = -1
            mstrViewByIds = ""
            mstrViewByName = ""
            mstrAnalysis_Fields = ""
            mstrAnalysis_Join = ""
            mstrAnalysis_OrderBy = ""
            mstrAnalysis_OrderBy_GName = ""
            mstrReport_GroupName = ""
            mblnIncludeAccessFilterQry = True
            mstrOrderByQuery = ""
            'S.SANDEEP |29-MAR-2019| -- START
            'ENHANCEMENT : 0003555 {PAPAYE}.
            mblnShowShiftName = False
            'S.SANDEEP |29-MAR-2019| -- END

            'Pinkal (08-Aug-2019) -- Start
            'Enhancement [004032 - TRZ] - Working on Off days hours should not count as part of the total Base hours in Employee timesheet report.
            mblnDonotconsiderLeave = False
            'Pinkal (08-Aug-2019) -- End

            'Pinkal (27-Oct-2020) -- Start
            'Enhancement St.Jude -   Working on Automatic Attendance download and send Report to Manager.
            mblnGenerateReportForNotification = False
            'Pinkal (27-Oct-2020) -- End


            'Pinkal (14-Feb-2022) -- Start
            'Enhancement TRA : TnA Module Enhancement for TRA.
            Create_OnDetailReport()
            mblnEachEmployeeOnPage = True
            'Pinkal (14-Feb-2022) -- End

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: SetDefaultValue; Module Name: " & mstrModuleName)
        End Try
    End Sub

    Public Sub FilterTitleAndFilterQuery()
        Me._FilterQuery = ""
        Me._FilterTitle = ""

        Try

            If mdtFromDate <> Nothing Then
                Me._FilterTitle &= Language.getMessage(mstrModuleName, 3, "From Date: ") & " " & mdtFromDate.Date & " "
            End If

            If mdtToDate <> Nothing Then
                Me._FilterTitle &= Language.getMessage(mstrModuleName, 4, "To Date: ") & " " & mdtToDate.Date & " "
            End If

            If mintEmployeeId > 0 Then
                Me._FilterTitle &= Language.getMessage(mstrModuleName, 5, "Employee: ") & " " & mstrEmployeeName & " "
            End If

            If Me.OrderByQuery <> "" Then

                If mstrGroupName.ToUpper = "TANZANIA REVENUE AUTHORITY" Then
                    If mintViewIndex > 0 Then
                        mstrOrderByQuery &= "  ORDER BY GName, CONVERT(char(8),loginIntime.checkintime,112) , " & Me.OrderByQuery & " "
                    Else
                        mstrOrderByQuery &= "  ORDER BY  CONVERT(char(8),loginIntime.checkintime,112) , " & Me.OrderByQuery & " "
                    End If

                Else

                If mintViewIndex > 0 Then
                    'Pinkal (27-Oct-2020) -- Start
                    'Enhancement St.Jude -   Working on Automatic Attendance download and send Report to Manager.
                    If mblnGenerateReportForNotification = False Then
                    mstrOrderByQuery &= "  ORDER BY GName," & Me.OrderByQuery
                Else
                        mstrOrderByQuery &= "  ORDER BY GName," & Me.OrderByQuery & " , CONVERT(VARCHAR(8),tnalogin_summary.login_date,112)"
                    End If
                    'Pinkal (27-Oct-2020) -- End
                Else

                    'Pinkal (27-Oct-2020) -- Start
                    'Enhancement St.Jude -   Working on Automatic Attendance download and send Report to Manager.
                    If mblnGenerateReportForNotification = False Then
                    mstrOrderByQuery &= "  ORDER BY " & Me.OrderByQuery
                    Else
                        mstrOrderByQuery &= "  ORDER BY " & Me.OrderByQuery & " , CONVERT(VARCHAR(8),tnalogin_summary.login_date,112)"
                    End If
                    'Pinkal (27-Oct-2020) -- End


                End If

                End If

                Me._FilterTitle &= Language.getMessage(mstrModuleName, 6, " Order By : ") & " " & Me.OrderByDisplay
            End If

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: FilterTitleAndFilterQuery; Module Name: " & mstrModuleName)
        End Try
    End Sub

    Public Overrides Sub generateReport(ByVal pintReportType As Integer, Optional ByVal PrintAction As Aruti.Data.enPrintAction = Aruti.Data.enPrintAction.Preview, Optional ByVal ExportAction As Aruti.Data.enExportAction = Aruti.Data.enExportAction.None)
        Dim objRpt As CrystalDecisions.CrystalReports.Engine.ReportClass = Nothing
        Dim strReportExportFile As String = ""
        Try

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: generateReport; Module Name: " & mstrModuleName)
        End Try
    End Sub

    Public Overrides Sub generateReportNew(ByVal xDatabaseName As String, ByVal xUserUnkid As Integer, ByVal xYearUnkid As Integer, ByVal xCompanyUnkid As Integer, ByVal xPeriodStart As Date, ByVal xPeriodEnd As Date, ByVal xUserModeSetting As String, ByVal xOnlyApproved As Boolean, ByVal xExportReportPath As String, ByVal xOpenReportAfterExport As Boolean, ByVal pintReportType As Integer, Optional ByVal PrintAction As Aruti.Data.enPrintAction = Aruti.Data.enPrintAction.Preview, Optional ByVal ExportAction As Aruti.Data.enExportAction = Aruti.Data.enExportAction.None, Optional ByVal intBaseCurrencyUnkid As Integer = 0)
        Dim objRpt As CrystalDecisions.CrystalReports.Engine.ReportClass = Nothing
        Dim strReportExportFile As String = ""
        Try
            If mintCompanyUnkid <= 0 Then
                mintCompanyUnkid = Company._Object._Companyunkid
            End If

            Company._Object._Companyunkid = mintCompanyUnkid
            ConfigParameter._Object._Companyunkid = mintCompanyUnkid

            If mintUserUnkid <= 0 Then
                mintUserUnkid = User._Object._Userunkid
            End If

            User._Object._Userunkid = mintUserUnkid


            If mstrGroupName.ToUpper() = "TANZANIA REVENUE AUTHORITY" Then
                'Pinkal (28-Apr-2022) -- Start
                'Enhancement TRA : TnA Module Enhancement for TRA.
                If pintReportType = 0 Then
                    objRpt = Generate_TRA_Late_Coming_Report(xDatabaseName, xUserUnkid, xYearUnkid, xCompanyUnkid, xPeriodStart, xPeriodEnd, xUserModeSetting, xOnlyApproved)
                Else
                objRpt = Generate_TRA_Early_Departure_Report(xDatabaseName, xUserUnkid, xYearUnkid, xCompanyUnkid, xPeriodStart, xPeriodEnd, xUserModeSetting, xOnlyApproved)
            End If
                'Pinkal (28-Apr-2022) -- End
            End If

            Rpt = objRpt

            If Not IsNothing(objRpt) Then
                Call ReportExecute(objRpt, PrintAction, ExportAction, xExportReportPath, xOpenReportAfterExport)
            End If

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: generateReportNew; Module Name: " & mstrModuleName)
        End Try
    End Sub

    Public Overrides Sub setDefaultOrderBy(ByVal intReportType As Integer)
        OrderByDisplay = ""
        OrderByQuery = ""
        Try
            OrderByDisplay = iColumn_DetailReport.ColumnItem(0).DisplayName
            OrderByQuery = iColumn_DetailReport.ColumnItem(0).Name
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: setDefaultOrderBy; Module Name: " & mstrModuleName)
        End Try
    End Sub

    Public Overrides Sub setOrderBy(ByVal intReportType As Integer)
        Try
            Call OrderByExecute(iColumn_DetailReport)
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: setOrderBy; Module Name: " & mstrModuleName)
        End Try
    End Sub

#End Region

#Region " Report Generation "

    Dim iColumn_DetailReport As New IColumnCollection

    Public Property Field_OnDetailReport() As IColumnCollection
        Get
            Return iColumn_DetailReport
        End Get
        Set(ByVal value As IColumnCollection)
            iColumn_DetailReport = value
        End Set
    End Property

    Private Sub Create_OnDetailReport()
        Try
            iColumn_DetailReport.Clear()

            'Pinkal (14-Feb-2022) -- Start
            'Enhancement TRA : TnA Module Enhancement for TRA.
            If mstrGroupName.ToUpper() = "TANZANIA REVENUE AUTHORITY" Then
                iColumn_DetailReport.Add(New IColumn("ISNULL(hremployee_master.employeecode,'') + ' - ' + ISNULL(hremployee_master.firstname,'') + ' ' + ISNULL(hremployee_master.othername,'') + ' ' + ISNULL(hremployee_master.surname,'') ", Language.getMessage(mstrModuleName, 2, "Employee")))
            Else
            iColumn_DetailReport.Add(New IColumn("ISNULL(hremployee_master.employeecode, '')", Language.getMessage(mstrModuleName, 1, "Employee Code")))
            iColumn_DetailReport.Add(New IColumn("ISNULL(hremployee_master.firstname, '') + ' ' + ISNULL(hremployee_master.othername, '') + ' ' + ISNULL(hremployee_master.surname, '') ", Language.getMessage(mstrModuleName, 2, "Employee")))
            End If
            'Pinkal (14-Feb-2022) -- End

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: Create_OnDetailReport; Module Name: " & mstrModuleName)
        End Try
    End Sub

    Public Sub Generate_LateArrival_Report(ByVal xDatabaseName As String, ByVal xUserUnkid As Integer _
                                                                                     , ByVal xYearUnkid As Integer, ByVal xCompanyUnkid As Integer _
                                                                                     , ByVal xDateAsOn As Date, ByVal xUserModeSetting As String, ByVal xOnlyApproved As Boolean _
                                                                                     , ByVal xIncludeIn_ActiveEmployee As Boolean)

        Dim objRpt As CrystalDecisions.CrystalReports.Engine.ReportClass = Nothing
        Dim StrQ As String = ""
        Dim StrQFilter As String = ""
        Dim dsList As New DataSet
        Dim iTotDays As Integer = 0
        Try


            If mintCompanyUnkid <= 0 Then
                mintCompanyUnkid = Company._Object._Companyunkid
            End If

            Company._Object._Companyunkid = mintCompanyUnkid
            ConfigParameter._Object._Companyunkid = mintCompanyUnkid

            If mintUserUnkid <= 0 Then
                mintUserUnkid = User._Object._Userunkid
            End If

            User._Object._Userunkid = mintUserUnkid

            objDataOperation = New clsDataOperation

            iTotDays = DateDiff(DateInterval.Day, mdtFromDate, mdtToDate) + 1


            Dim xUACQry, xUACFiltrQry, xAdvanceJoinQry As String
            xUACQry = "" : xUACFiltrQry = "" : xAdvanceJoinQry = ""
            If mblnIncludeAccessFilterQry Then Call NewAccessLevelFilterString(xUACQry, xUACFiltrQry, xDateAsOn.Date, xOnlyApproved, xDatabaseName, xUserUnkid, xCompanyUnkid, xYearUnkid, xUserModeSetting)
            Call GetAdvanceFilterQry(xAdvanceJoinQry, xDateAsOn.Date, xDatabaseName)

            objDataOperation.ClearParameters()

            StrQ = " SELECT hremployee_master.employeeunkid , ISNULL(hremployee_master.employeecode,'') AS [Emp No.] " & _
                       " , ISNULL(cfcommon_master.name,'') AS [Employment] " & _
                       " , ISNULL(hremployee_master.firstname,'') + ' ' + ISNULL(hremployee_master.othername,'') + ' ' + ISNULL(hremployee_master.surname,'') AS [Name] "


            If mintViewIndex > 0 Then
                StrQ &= mstrAnalysis_Fields
            Else
                StrQ &= ", '' AS GName "
            End If

            StrQ &= " FROM hremployee_master " & _
                         " LEFT JOIN cfcommon_master ON cfcommon_master.masterunkid = hremployee_master.employmenttypeunkid AND cfcommon_master.mastertype= " & clsCommon_Master.enCommonMaster.EMPLOYEMENT_TYPE

            If mintViewIndex > 0 Then
                StrQ &= mstrAnalysis_Join
            End If

            If xUACQry.Trim.Length > 0 Then
                StrQ &= xUACQry
            End If

            If xAdvanceJoinQry.Trim.Length > 0 Then
                StrQ &= xAdvanceJoinQry
            End If

            StrQ &= " WHERE  1=1  "

            If mstrAdvance_Filter.Trim.Length > 0 Then
                StrQ &= " AND " & mstrAdvance_Filter
            End If

            If xUACFiltrQry.Trim.Length > 0 Then
                StrQ &= " AND " & xUACFiltrQry
            End If

            If mintEmployeeId > 0 Then
                StrQ &= " AND hremployee_master.employeeunkid = " & mintEmployeeId
            End If

            Call FilterTitleAndFilterQuery()

            StrQ &= Me._FilterQuery

            StrQ &= mstrOrderByQuery

            dsList = objDataOperation.ExecQuery(StrQ, "DataTable")

            If objDataOperation.ErrorMessage <> "" Then
                Throw New Exception(objDataOperation.ErrorNumber & ":" & objDataOperation.ErrorMessage)
            End If

            'Pinkal (08-Aug-2019) -- Start
            'Enhancement [Good Neighbours] -Working on adding Total count of Early/Arrilval Report.
            If mintReportId = 0 Then 'Late Arrival Report
                If dsList.Tables(0).Columns.Contains("TotalLateCount") = False Then
                    dsList.Tables(0).Columns.Add("TotalLateCount", System.Type.GetType("System.String"))
                    dsList.Tables(0).Columns("TotalLateCount").DefaultValue = ""
                    dsList.Tables(0).Columns("TotalLateCount").Caption = Language.getMessage(mstrModuleName, 11, "Total Late Arrival Count")
                End If
            ElseIf mintReportId = 1 Then 'Early Departure Report
                If dsList.Tables(0).Columns.Contains("TotalEarlyCount") = False Then
                    dsList.Tables(0).Columns.Add("TotalEarlyCount", System.Type.GetType("System.String"))
                    dsList.Tables(0).Columns("TotalEarlyCount").DefaultValue = ""
                    dsList.Tables(0).Columns("TotalEarlyCount").Caption = Language.getMessage(mstrModuleName, 12, "Total Early Going Count")
                End If
            End If
            'Pinkal (08-Aug-2019) -- End



            If dsList IsNot Nothing Then
                Dim intCount As Integer = 0
                For i As Integer = 0 To iTotDays - 1
                    If dsList.Tables(0).Columns.Contains(eZeeDate.convertDate(mdtFromDate.AddDays(i).Date)) = False Then
                        dsList.Tables(0).Columns.Add(eZeeDate.convertDate(mdtFromDate.AddDays(i).Date), System.Type.GetType("System.String"))
                        dsList.Tables(0).Columns(eZeeDate.convertDate(mdtFromDate.AddDays(i).Date)).Caption = mdtFromDate.AddDays(i).Date
                    Else
                        intCount += 1
                        dsList.Tables(0).Columns.Add(eZeeDate.convertDate(mdtFromDate.AddDays(i).Date) & StrDup(intCount, "*"), System.Type.GetType("System.String"))
                        dsList.Tables(0).Columns(eZeeDate.convertDate(mdtFromDate.AddDays(i).Date) & StrDup(intCount, "*")).Caption = mdtFromDate.AddDays(i).Date
                    End If
                Next

                Dim objEmpShift As New clsEmployee_Shift_Tran
                Dim objShift As New clsNewshift_master
                Dim objShiftTran As New clsshift_tran
                Dim objLogin As New clslogin_Tran

                'Pinkal (08-Aug-2019) -- Start
                'Enhancement [Good Neighbours] -Working on adding Total count of Early/Arrilval Report.
                Dim mintCount As Integer = 0
                'Pinkal (08-Aug-2019) -- End

                For i As Integer = 0 To dsList.Tables(0).Rows.Count - 1
                    Dim mintShiftID As Integer = -1

                    'Pinkal (08-Aug-2019) -- Start
                    'Enhancement [Good Neighbours] -Working on adding Total count of Early/Arrilval Report.
                    'Dim mintIndex As Integer = 5
                    'If mintViewIndex > 0 Then mintIndex = 7
                    Dim mintIndex As Integer = 6
                    If mintViewIndex > 0 Then mintIndex = 8
                    'Pinkal (08-Aug-2019) -- End

                    mintCount = 0

                    For j As Integer = mintIndex To dsList.Tables(0).Columns.Count - 1
                        mintShiftID = objEmpShift.GetEmployee_Current_ShiftId(eZeeDate.convertDate(dsList.Tables(0).Columns(j).ColumnName).Date, CInt(dsList.Tables(0).Rows(i)("employeeunkid")))
                        objShift._Shiftunkid = mintShiftID

                        'S.SANDEEP |29-MAR-2019| -- START
                        'ENHANCEMENT : 0003555 {PAPAYE}.
                        Dim StrShiftName As String = "" : StrShiftName = objShift._Shiftname
                        'S.SANDEEP |29-MAR-2019| -- END


                        'Pinkal (17-Nov-2018) -- Start
                        'Bug - PAPAYE#3102 | Unable to print report employee late arrival/early departure
                        If objShift._IsOpenShift = True Then Continue For
                        'Pinkal (17-Nov-2018) -- End

                        objShiftTran.GetShiftTran(objShift._Shiftunkid)
                        Dim drRow() As DataRow = objShiftTran._dtShiftday.Select("dayid = " & GetWeekDayNumber(WeekdayName(Weekday(eZeeDate.convertDate(dsList.Tables(0).Columns(j).ColumnName).Date), False, FirstDayOfWeek.Sunday).ToString()))
                        If drRow.Length > 0 Then
                            Dim mdtDate As DateTime = Nothing
                            objLogin._Employeeunkid = CInt(dsList.Tables(0).Rows(i)("employeeunkid"))
                            objLogin._Logindate = eZeeDate.convertDate(dsList.Tables(0).Columns(j).ColumnName).Date

                            Dim dsLogin As DataSet = objLogin.GetList("List", True)
                            Dim mdtEmpLoginDate As Date = Nothing
                            'Dim mdtEmpLoginDate As Date = IIf(IsDBNull(dsLogin.Tables(0).Compute("Min(checkintime)", "1=1")), Nothing, dsLogin.Tables(0).Compute("Min(checkintime)", "1=1"))

                            'Pinkal (08-Aug-2019) -- Start
                            'Enhancement [004032 - TRZ] - Working on Off days hours should not count as part of the total Base hours in Employee timesheet report.
                            If mblnDonotconsiderLeave Then
                                Dim objLeaveIssue As New clsleaveissue_Tran
                                If objLeaveIssue.GetIssuedDayFractionForViewer(CInt(dsList.Tables(0).Rows(i)("employeeunkid")), eZeeDate.convertDate(objLogin._Logindate)) > 0 Then Continue For
                                objLeaveIssue = Nothing
                            End If
                            'Pinkal (08-Aug-2019) -- End


                            If mintReportId = 0 Then 'Late Arrival Report
                                mdtDate = CDate(eZeeDate.convertDate(dsList.Tables(0).Columns(j).ColumnName).Date & " " & CDate(drRow(0)("starttime").ToString()).ToString("hh:mm tt"))
                                mdtEmpLoginDate = (From n In dsLogin.Tables(0).AsEnumerable() Where n.Field(Of DateTime)("checkintime") <> Nothing Select n.Field(Of DateTime)("checkintime")).DefaultIfEmpty.Min()
                                If mdtEmpLoginDate <> Nothing AndAlso mdtEmpLoginDate > mdtDate Then
                                    'S.SANDEEP |29-MAR-2019| -- START
                                    'ENHANCEMENT : 0003555 {PAPAYE}.
                                    'dsList.Tables(0).Rows(i)(j) = mdtEmpLoginDate.ToString("HH:mm")
                                    If mblnShowShiftName Then
                                        dsList.Tables(0).Rows(i)(j) = StrShiftName & "#10;" & mdtEmpLoginDate.ToString("HH:mm")
                                    Else
                                        dsList.Tables(0).Rows(i)(j) = mdtEmpLoginDate.ToString("HH:mm")
                                    End If
                                    'S.SANDEEP |29-MAR-2019| -- END


                                    'Pinkal (08-Aug-2019) -- Start
                                    'Enhancement [Good Neighbours] -Working on adding Total count of Early/Arrilval Report.
                                    mintCount += 1
                                    dsList.Tables(0).Rows(i)("TotalLateCount") = mintCount
                                    'Pinkal (08-Aug-2019) -- End

                                Else
                                    dsList.Tables(0).Rows(i)(j) = ""
                                End If

                            ElseIf mintReportId = 1 Then 'Early Departure Report
                                Dim mintShiftCountDay As Integer = DateDiff(DateInterval.Day, CDate(drRow(0)("starttime")).Date, CDate(drRow(0)("endtime")).Date)
                                mdtDate = CDate(eZeeDate.convertDate(dsList.Tables(0).Columns(j).ColumnName).AddDays(mintShiftCountDay).Date & " " & CDate(drRow(0)("endtime").ToString()).ToString("hh:mm tt"))
                                mdtEmpLoginDate = (From n In dsLogin.Tables(0).AsEnumerable() Where n.IsNull("checkouttime") = False AndAlso n.Field(Of DateTime)("checkouttime") <> Nothing Select n.Field(Of DateTime)("checkouttime")).DefaultIfEmpty.Max()
                                If mdtEmpLoginDate <> Nothing AndAlso mdtEmpLoginDate < mdtDate Then
                                    'S.SANDEEP |29-MAR-2019| -- START
                                    'ENHANCEMENT : 0003555 {PAPAYE}.
                                    'dsList.Tables(0).Rows(i)(j) = mdtEmpLoginDate.ToString("HH:mm")
                                    If mblnShowShiftName Then
                                        dsList.Tables(0).Rows(i)(j) = StrShiftName & "#10;" & mdtEmpLoginDate.ToString("HH:mm")
                                    Else
                                        dsList.Tables(0).Rows(i)(j) = mdtEmpLoginDate.ToString("HH:mm")
                                    End If
                                    'S.SANDEEP |29-MAR-2019| -- END

                                    'Pinkal (08-Aug-2019) -- Start
                                    'Enhancement [Good Neighbours] -Working on adding Total count of Early/Arrilval Report.
                                    mintCount += 1
                                    dsList.Tables(0).Rows(i)("TotalEarlyCount") = mintCount
                                    'Pinkal (08-Aug-2019) -- End

                                Else
                                    dsList.Tables(0).Rows(i)(j) = ""
                                End If
                            End If

                        End If
                    Next
                Next

            End If

            ConfigParameter._Object.GetReportSettings(CInt(mstrReportId))

            Dim strarrGroupColumns As String() = Nothing
            Dim rowsArrayHeader As New ArrayList
            Dim rowsArrayFooter As New ArrayList
            Dim row As WorksheetRow
            Dim wcell As WorksheetCell

            Dim mdtTableExcel As DataTable = dsList.Tables(0)


            If mdtTableExcel IsNot Nothing Then
                If mdtTableExcel.Columns.Contains("employeeunkid") Then mdtTableExcel.Columns.Remove("employeeunkid")
            End If

            mdtTableExcel.AcceptChanges()


            If mintViewIndex > 0 Then
                If mdtTableExcel.Columns.Contains("Id") Then
                    mdtTableExcel.Columns.Remove("Id")
                End If
                mdtTableExcel.Columns("GName").Caption = mstrReport_GroupName
                Dim strGrpCols As String() = {"GName"}
                strarrGroupColumns = strGrpCols
            Else
                If mdtTableExcel.Columns.Contains("GName") Then
                    mdtTableExcel.Columns.Remove("GName")
                End If
            End If

            If mblnIgnoreBlankRows Then
                mdtTableExcel = IgnoreEmptyRows(mdtTableExcel)
            End If


            'Pinkal (08-Aug-2019) -- Start
            'Enhancement [Good Neighbours] -Working on adding Total count of Early/Arrilval Report.

            Dim intDaysCount As Integer = 0
            'Dim Index As Integer = 3
            'If mintViewIndex > 0 Then Index = 4

            Dim Index As Integer = 4
            If mintViewIndex > 0 Then Index = 5

            For i As Integer = Index To mdtTableExcel.Columns.Count - 1
                mdtTableExcel.Columns(i).Caption = MonthName(mdtFromDate.AddDays(intDaysCount).Month, True) & "#10;" & mdtFromDate.AddDays(intDaysCount).ToString("dd") & "#10;" & WeekdayName(Weekday(mdtFromDate.AddDays(intDaysCount).Date, FirstDayOfWeek.Sunday), True, FirstDayOfWeek.Sunday)
                intDaysCount += 1
            Next

            If mintReportId = 0 Then 'Late Arrival Report
                dsList.Tables(0).Columns("TotalLateCount").SetOrdinal(dsList.Tables(0).Columns.Count - 1)
            ElseIf mintReportId = 1 Then  'Early Departure Report
                dsList.Tables(0).Columns("TotalEarlyCount").SetOrdinal(dsList.Tables(0).Columns.Count - 1)
            End If
            'Pinkal (08-Aug-2019) -- Start


            row = New WorksheetRow()

            If Me._FilterTitle.ToString.Length > 0 Then
                wcell = New WorksheetCell(Me._FilterTitle.ToString, "s10bw")
                wcell.MergeAcross = mdtTableExcel.Columns.Count - 1
                row.Cells.Add(wcell)
            End If
            rowsArrayHeader.Add(row)

            row = New WorksheetRow()
            wcell = New WorksheetCell("", "s10bw")
            row.Cells.Add(wcell)
            rowsArrayHeader.Add(row)


            row = New WorksheetRow()
            wcell = New WorksheetCell("", "s10bw")
            row.Cells.Add(wcell)
            rowsArrayFooter.Add(row)
            '--------------------


            If ConfigParameter._Object._IsShowPreparedBy = True Then

                Dim objUser As New clsUserAddEdit
                objUser._Userunkid = xUserUnkid
                row = New WorksheetRow()
                wcell = New WorksheetCell(Language.getMessage(mstrModuleName, 7, "Prepared By :") & Space(10) & objUser._Username, "s8bw")
                wcell.MergeAcross = 4
                row.Cells.Add(wcell)
                objUser = Nothing
                wcell = New WorksheetCell("", "s10bw")
                row.Cells.Add(wcell)
                rowsArrayFooter.Add(row)

                row = New WorksheetRow()
                wcell = New WorksheetCell("", "s10bw")
                row.Cells.Add(wcell)
                rowsArrayFooter.Add(row)
            End If


            If ConfigParameter._Object._IsShowCheckedBy = True Then
                row = New WorksheetRow()
                wcell = New WorksheetCell(Language.getMessage(mstrModuleName, 8, "Checked By :"), "s8bw")
                wcell.MergeAcross = 4
                row.Cells.Add(wcell)

                wcell = New WorksheetCell("", "s10bw")
                row.Cells.Add(wcell)
                rowsArrayFooter.Add(row)

                row = New WorksheetRow()
                wcell = New WorksheetCell("", "s10bw")
                row.Cells.Add(wcell)
                rowsArrayFooter.Add(row)
            End If


            If ConfigParameter._Object._IsShowApprovedBy = True Then
                row = New WorksheetRow()
                wcell = New WorksheetCell(Language.getMessage(mstrModuleName, 9, "Approved By :"), "s8bw")
                wcell.MergeAcross = 4
                row.Cells.Add(wcell)

                wcell = New WorksheetCell("", "s10bw")
                row.Cells.Add(wcell)
                rowsArrayFooter.Add(row)

                row = New WorksheetRow()
                wcell = New WorksheetCell("", "s10bw")
                row.Cells.Add(wcell)
                rowsArrayFooter.Add(row)
            End If

            If ConfigParameter._Object._IsShowReceivedBy = True Then
                row = New WorksheetRow()
                wcell = New WorksheetCell(Language.getMessage(mstrModuleName, 10, "Received By :"), "s8bw")
                wcell.MergeAcross = 4
                row.Cells.Add(wcell)
                rowsArrayFooter.Add(row)
            End If


            '--------------------

            'SET EXCEL CELL WIDTH  
            Dim intArrayColumnWidth As Integer() = Nothing
            ReDim intArrayColumnWidth(mdtTableExcel.Columns.Count - 1)
            For i As Integer = 0 To intArrayColumnWidth.Length - 1
                If i <= 2 Then
                    intArrayColumnWidth(i) = 125
                Else
                    intArrayColumnWidth(i) = 75
                End If
            Next
            'SET EXCEL CELL WIDTH


            Call ReportExecute(Nothing, enPrintAction.None, enExportAction.ExcelExtra, mstrExportReportPath, mblnOpenAfterExport, mdtTableExcel, intArrayColumnWidth, True, True, True, strarrGroupColumns, mstrReportTypeName, "", " ", , "", True, rowsArrayHeader, rowsArrayFooter, Nothing, Nothing, True)

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: Generate_LateArrival_Report; Module Name: " & mstrModuleName)
        End Try
    End Sub

    Private Function IgnoreEmptyRows(ByVal objDataReader As DataTable) As DataTable
        Dim strBuilder As New StringBuilder
        Dim blnFlag As Boolean = False
        Try

            Dim arr() As String = {"Emp No.", "Employment", "Name", "GName"}
            Dim decCols() As String = (From p In objDataReader.Columns.Cast(Of DataColumn)() Where (arr.Contains(p.ColumnName.ToString) = False) Select ("([" & p.ColumnName.ToString & "] = '' OR [" & p.ColumnName.ToString() & "] IS NULL ) AND ")).ToArray
            If decCols.Length > 0 Then
                Dim s As String = String.Join("", decCols)
                Dim r() As DataRow = objDataReader.Select(s.Substring(0, s.Length - 4))
                For Each dr As DataRow In r
                    objDataReader.Rows.Remove(dr)
                Next
            End If

            Return objDataReader

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: IgnoreEmptyRows; Module Name: " & mstrModuleName)
            Return Nothing
        End Try
    End Function


    'Pinkal (27-Oct-2020) -- Start
    'Enhancement St.Jude -   Working on Automatic Attendance download and send Report to Manager.
    Private Function Generate_DetailReport(ByVal strDatabaseName As String, _
                                           ByVal intUserUnkid As Integer, _
                                           ByVal intYearUnkid As Integer, _
                                           ByVal intCompanyUnkid As Integer, _
                                           ByVal dtPeriodStart As Date, _
                                           ByVal dtPeriodEnd As Date, _
                                           ByVal strUserModeSetting As String, _
                                           ByVal blnOnlyApproved As Boolean) As CrystalDecisions.CrystalReports.Engine.ReportClass

        Dim objRpt As CrystalDecisions.CrystalReports.Engine.ReportClass = Nothing
        Dim StrQ As String = ""
        Dim dsList As New DataSet
        Dim rpt_Data As ArutiReport.Designer.dsArutiReport
        Try

            objDataOperation = New clsDataOperation
            objDataOperation.ClearParameters()

            Dim xDateJoinQry, xDateFilterQry, xUACQry, xUACFiltrQry, xAdvanceJoinQry As String
            xDateJoinQry = "" : xDateFilterQry = "" : xUACQry = "" : xUACFiltrQry = "" : xAdvanceJoinQry = ""
            Call GetDatesFilterString(xDateJoinQry, xDateFilterQry, dtPeriodStart, dtPeriodEnd, , , strDatabaseName)
            Call NewAccessLevelFilterString(xUACQry, xUACFiltrQry, dtPeriodEnd, blnOnlyApproved, strDatabaseName, intUserUnkid, intCompanyUnkid, intYearUnkid, strUserModeSetting)
            Call GetAdvanceFilterQry(xAdvanceJoinQry, dtPeriodEnd, strDatabaseName)

            StrQ = " SELECT hremployee_master.employeeunkid  " & _
                       ", ISNULL(hremployee_master.employeecode,'') AS Empcode " & _
                       ", ISNULL(hremployee_master.firstname,'') + ' ' + ISNULL(hremployee_master.othername,'') + ' ' + ISNULL(hremployee_master.surname,'') AS Employee " & _
                       ", ISNULL(dept.name, '') AS department " & _
                       ", ISNULL(CONVERT(VARCHAR(5), tnashift_tran.starttime, 108),'') + ' - ' + ISNULL(CONVERT(VARCHAR(5), tnashift_tran.endtime, 108),'') As Shifthr " & _
                       ", loginIntime.checkintime AS Late_Intime " & _
                       ", loginOuttime.checkouttime AS Early_Goingtime "


            If mintViewIndex > 0 Then
                StrQ &= mstrAnalysis_Fields
            Else
                StrQ &= ", '' AS GName "
            End If

            StrQ &= " FROM tnalogin_summary " & _
                         " JOIN hremployee_master ON hremployee_master.employeeunkid = tnalogin_summary.employeeunkid "

            If xAdvanceJoinQry.Trim.Length > 0 Then
                StrQ &= xAdvanceJoinQry
            End If

            StrQ &= " LEFT JOIN " & _
                         "   ( " & _
                         "       SELECT " & _
                         "           jobunkid " & _
                         "           ,employeeunkid " & _
                         "           ,ROW_NUMBER()OVER(PARTITION BY employeeunkid ORDER BY effectivedate DESC) AS rno " & _
                         "       FROM hremployee_categorization_tran " & _
                         "       WHERE isvoid = 0 AND CONVERT(CHAR(8),effectivedate,112) <= '" & eZeeDate.convertDate(dtPeriodEnd) & "' " & _
                         "   ) AS Jobs ON Jobs.employeeunkid = hremployee_master.employeeunkid AND Jobs.rno = 1 " & _
                         " LEFT JOIN hrjob_master jb ON Jobs.jobunkid = jb.jobunkid " & _
                         " LEFT JOIN " & _
                         "   ( " & _
                         "       SELECT " & _
                         "           departmentunkid " & _
                         "           ,employeeunkid " & _
                         "           ,ROW_NUMBER()OVER(PARTITION BY employeeunkid ORDER BY effectivedate DESC) AS rno " & _
                         "       FROM hremployee_transfer_tran " & _
                         "       WHERE isvoid = 0 AND CONVERT(CHAR(8),effectivedate,112) <= '" & eZeeDate.convertDate(dtPeriodEnd) & "' " & _
                         "   ) AS Alloc ON Alloc.employeeunkid = hremployee_master.employeeunkid AND Alloc.rno = 1 " & _
                         " LEFT JOIN hrdepartment_master dept ON Alloc.departmentunkid = dept.departmentunkid " & _
                         " JOIN tnashift_master ON tnashift_master.shiftunkid = tnalogin_summary.shiftunkid AND tnashift_master.isactive = 1 " & _
                         " JOIN tnashift_tran ON tnashift_tran.shiftunkid = tnashift_master.shiftunkid AND tnashift_tran.dayid = datepart (dw,tnalogin_summary.login_date) - 1 " & _
                         " LEFT JOIN ( SELECT  tnalogin_tran.logindate,employeeunkid " & _
                                                      ", CASE WHEN MIN(tnalogin_tran.roundoff_intime) IS NULL THEN MIN(tnalogin_tran.checkintime) " & _
                                                      "           WHEN MIN(tnalogin_tran.checkintime) <>  MIN(tnalogin_tran.roundoff_intime) THEN  " & _
                                                      "     MIN(tnalogin_tran.roundoff_intime) " & _
                                                      "  ELSE  MIN(tnalogin_tran.roundoff_intime) END checkintime  " & _
                                                      " FROM tnalogin_Tran " & _
                                                      " WHERE CONVERT(VARCHAR(8),logindate,112) BETWEEN  '" & eZeeDate.convertDate(dtPeriodStart.Date) & "' AND '" & eZeeDate.convertDate(dtPeriodEnd.Date) & "' AND isvoid = 0 " & _
                                                      " GROUP BY logindate,employeeunkid " & _
                           " ) AS loginIntime ON loginIntime.employeeunkid = tnalogin_summary.employeeunkid " & _
                           " AND CONVERT(VARCHAR(8),loginIntime.logindate,112) = CONVERT(VARCHAR(8),tnalogin_summary.login_date,112)  AND tnalogin_summary.ltcoming_grace > 60 " & _
                           " LEFT JOIN ( SELECT tnalogin_tran.logindate,employeeunkid " & _
                           "                          , CASE WHEN Max(tnalogin_tran.roundoff_outtime) IS NULL THEN MAX(tnalogin_tran.checkouttime)  " & _
                           "                                     WHEN MAX(tnalogin_tran.checkouttime) <> ISNULL(MAX(tnalogin_tran.roundoff_outtime), '') THEN  " & _
                           "                                MAX(tnalogin_tran.roundoff_outtime) " & _
                           "                            ELSE MAX(tnalogin_tran.roundoff_outtime) END checkouttime  " & _
                           "                    FROM tnalogin_Tran " & _
                           "                    WHERE isvoid = 0 AND CONVERT(VARCHAR(8),logindate,112) BETWEEN '" & eZeeDate.convertDate(dtPeriodStart.Date) & "' AND '" & eZeeDate.convertDate(dtPeriodEnd.Date) & "' AND isvoid = 0 " & _
                           "					GROUP BY logindate,employeeunkid  " & _
                           " ) AS loginOuttime ON loginOuttime.employeeunkid = tnalogin_summary.employeeunkid  " & _
                           " AND CONVERT(VARCHAR(8),loginOuttime.logindate,112) = CONVERT(VARCHAR(8),tnalogin_summary.login_date,112)  AND tnalogin_summary.elrgoing_grace > 60 "

            If mintViewIndex > 0 Then
                StrQ &= mstrAnalysis_Join
            End If

            If xUACQry.Trim.Length > 0 Then
                StrQ &= xUACQry
            End If

            StrQ &= " WHERE  1=1  AND loginintime.checkintime IS NOT NULL OR loginOuttime.checkouttime IS NOT NULL "

            If mstrAdvance_Filter.Trim.Length > 0 Then
                StrQ &= " AND " & mstrAdvance_Filter
            End If

            If xUACFiltrQry.Trim.Length > 0 Then
                StrQ &= " AND " & xUACFiltrQry
            End If

            If mintEmployeeId > 0 Then
                StrQ &= " AND hremployee_master.employeeunkid = " & mintEmployeeId
            End If

            Call FilterTitleAndFilterQuery()

            StrQ &= Me._FilterQuery

            StrQ &= mstrOrderByQuery

            dsList = objDataOperation.ExecQuery(StrQ, "DataTable")

            If objDataOperation.ErrorMessage <> "" Then
                Throw New Exception(objDataOperation.ErrorNumber & ":" & objDataOperation.ErrorMessage)
            End If


            rpt_Data = New ArutiReport.Designer.dsArutiReport
            For Each dtRow As DataRow In dsList.Tables("DataTable").Rows
                Dim rpt_Row As DataRow = rpt_Data.Tables("ArutiTable").NewRow
                rpt_Row.Item("Column1") = dtRow.Item("Empcode").ToString().Trim()
                rpt_Row.Item("Column2") = dtRow.Item("Employee").ToString().Trim()
                rpt_Row.Item("Column3") = dtRow.Item("department").ToString().Trim()
                rpt_Row.Item("Column4") = dtRow.Item("job").ToString().Trim()
                rpt_Row.Item("Column5") = dtRow.Item("Shifthr").ToString().Trim()

                If Not IsDBNull(dtRow.Item("Late_Intime")) Then
                    rpt_Row.Item("Column6") = CDate(dtRow.Item("Late_Intime"))
                End If

                If Not IsDBNull(dtRow.Item("Early_Goingtime")) Then
                    rpt_Row.Item("Column7") = CDate(dtRow.Item("Early_Goingtime"))
                End If
                rpt_Row.Item("Column8") = dtRow.Item("GName").ToString()
                rpt_Data.Tables("ArutiTable").Rows.Add(rpt_Row)
            Next

            objRpt = New ArutiReport.Designer.rptEmpLtComing_Elgoing
            ConfigParameter._Object.GetReportSettings(CInt(mstrReportId))
            Dim arrImageRow As DataRow = Nothing
            arrImageRow = rpt_Data.Tables("ArutiImage").NewRow()


            ReportFunction.Logo_Display(objRpt, _
                                        ConfigParameter._Object._IsDisplayLogo, _
                                        ConfigParameter._Object._ShowLogoRightSide, _
                                        "arutiLogo1", _
                                        "arutiLogo2", _
                                        arrImageRow, _
                                        "txtCompanyName", _
                                        "txtReportName", _
                                        "txtFilterDescription", _
                                        ConfigParameter._Object._GetLeftMargin, _
                                        ConfigParameter._Object._GetRightMargin)

            rpt_Data.Tables("ArutiImage").Rows.Add(arrImageRow)

            If rpt_Data.Tables("ArutiTable").Rows.Count <= 0 Then
                rpt_Data.Tables("ArutiTable").Rows.Add("")
            End If

            If ConfigParameter._Object._IsShowPreparedBy = True Then
                Call ReportFunction.TextChange(objRpt, "lblPreparedBy", Language.getMessage(mstrModuleName, 7, "Prepared By :"))
                Call ReportFunction.TextChange(objRpt, "txtPreparedBy", User._Object._Username)
            Else
                Call ReportFunction.EnableSuppressSection(objRpt, "ReportFooterSection4", True)
            End If

            If ConfigParameter._Object._IsShowCheckedBy = True Then
                Call ReportFunction.TextChange(objRpt, "lblCheckedBy", Language.getMessage(mstrModuleName, 8, "Checked By :"))
            Else
                Call ReportFunction.EnableSuppressSection(objRpt, "ReportFooterSection3", True)
            End If

            If ConfigParameter._Object._IsShowApprovedBy = True Then
                Call ReportFunction.TextChange(objRpt, "lblApprovedBy", Language.getMessage(mstrModuleName, 9, "Approved By :"))
            Else
                Call ReportFunction.EnableSuppressSection(objRpt, "ReportFooterSection2", True)
            End If

            If ConfigParameter._Object._IsShowReceivedBy = True Then
                Call ReportFunction.TextChange(objRpt, "lblReceivedBy", Language.getMessage(mstrModuleName, 10, "Received By :"))
            Else
                Call ReportFunction.EnableSuppressSection(objRpt, "ReportFooterSection1", True)
            End If

            objRpt.SetDataSource(rpt_Data)


            Call ReportFunction.TextChange(objRpt, "txtECode", Language.getMessage(mstrModuleName, 1, "Employee Code"))
            Call ReportFunction.TextChange(objRpt, "txtEName", Language.getMessage(mstrModuleName, 2, "Employee"))
            Call ReportFunction.TextChange(objRpt, "txtDepartment", Language.getMessage(mstrModuleName, 13, "Department"))
            Call ReportFunction.TextChange(objRpt, "txtJob", Language.getMessage(mstrModuleName, 14, "Job"))
            Call ReportFunction.TextChange(objRpt, "txtShiftHrs", Language.getMessage(mstrModuleName, 15, "Shift Hrs"))
            Call ReportFunction.TextChange(objRpt, "txtLateInTime", Language.getMessage(mstrModuleName, 16, "Late In Time"))
            Call ReportFunction.TextChange(objRpt, "txtEarlyOuttime", Language.getMessage(mstrModuleName, 17, "Early Going Time"))

            Call ReportFunction.TextChange(objRpt, "lblPrintedBy", Language.getMessage(mstrModuleName, 18, "Printed By :"))
            Call ReportFunction.TextChange(objRpt, "lblPrintedDate", Language.getMessage(mstrModuleName, 19, "Printed Date :"))

            Call ReportFunction.TextChange(objRpt, "txtPrintedBy", Me._UserName)
            Call ReportFunction.TextChange(objRpt, "txtPrintedDate", Me._PrintDate)

            Call ReportFunction.TextChange(objRpt, "txtReportName", Me._ReportName)
            Call ReportFunction.TextChange(objRpt, "txtCompanyName", Me._CompanyName)
            Call ReportFunction.TextChange(objRpt, "txtFilterDescription", Me._FilterTitle)

            Return objRpt
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: Generate_DetailReport; Module Name: " & mstrModuleName)
            Return Nothing
        Finally

        End Try
    End Function

    Public Sub Send_ELateEarlyTiming(ByVal xDatabaseName As String, ByVal xUserUnkid As Integer, ByVal xYearUnkid As Integer _
                                                            , ByVal xCompanyUnkid As Integer, ByVal mdtStartDate As Date, ByVal mdtEndDate As Date _
                                                            , ByVal xOnlyApproved As Boolean, ByVal xUserModeSetting As String, ByVal StrFileName As String, ByVal StrFilePath As String)
        Dim objRpt As CrystalDecisions.CrystalReports.Engine.ReportClass = Nothing
        Dim strReportExportFile As String = ""
        Try
            Company._Object._Companyunkid = xCompanyUnkid
            User._Object._Userunkid = xUserUnkid
            mintCompanyUnkid = xCompanyUnkid
            mintUserUnkid = xUserUnkid

            objRpt = Generate_DetailReport(xDatabaseName, xUserUnkid, xYearUnkid, xCompanyUnkid, mdtStartDate, mdtEndDate, xUserModeSetting, xOnlyApproved)

            If Not IsNothing(objRpt) Then
                Dim oStream As New IO.MemoryStream
                oStream = objRpt.ExportToStream(CrystalDecisions.Shared.ExportFormatType.PortableDocFormat)
                Dim fs As New System.IO.FileStream(StrFilePath & "\" & StrFileName.Replace(" ", "_") & ".pdf", IO.FileMode.Create, IO.FileAccess.Write)
                Dim data As Byte() = oStream.ToArray()
                fs.Write(data, 0, data.Length)
                fs.Close()
                fs.Dispose()
            End If
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: Send_ELateEarlyTiming; Module Name: " & mstrModuleName)
        Finally
            objRpt.Dispose()
        End Try
    End Sub

    'Pinkal (27-Oct-2020) -- End



    'Pinkal (14-Feb-2022) -- Start
    'Enhancement TRA : TnA Module Enhancement for TRA.
    Public Function Generate_TRA_Early_Departure_Report(ByVal strDatabaseName As String, _
                                           ByVal intUserUnkid As Integer, _
                                           ByVal intYearUnkid As Integer, _
                                           ByVal intCompanyUnkid As Integer, _
                                           ByVal dtPeriodStart As Date, _
                                           ByVal dtPeriodEnd As Date, _
                                           ByVal strUserModeSetting As String, _
                                           ByVal blnOnlyApproved As Boolean) As CrystalDecisions.CrystalReports.Engine.ReportClass
        Dim objRpt As CrystalDecisions.CrystalReports.Engine.ReportClass = Nothing
        Dim StrQ As String = ""
        Dim dsList As New DataSet
        Dim rpt_Data As ArutiReport.Designer.dsArutiReport
        Try

            objDataOperation = New clsDataOperation
            objDataOperation.ClearParameters()

            Dim xDateJoinQry, xDateFilterQry, xUACQry, xUACFiltrQry, xAdvanceJoinQry As String
            xDateJoinQry = "" : xDateFilterQry = "" : xUACQry = "" : xUACFiltrQry = "" : xAdvanceJoinQry = ""
            Call GetDatesFilterString(xDateJoinQry, xDateFilterQry, dtPeriodStart, dtPeriodEnd, , , strDatabaseName)
            If mblnIncludeAccessFilterQry Then Call NewAccessLevelFilterString(xUACQry, xUACFiltrQry, dtPeriodEnd, blnOnlyApproved, strDatabaseName, intUserUnkid, intCompanyUnkid, intYearUnkid, strUserModeSetting)
            Call GetAdvanceFilterQry(xAdvanceJoinQry, dtPeriodEnd, strDatabaseName)

            StrQ = " SELECT hremployee_master.employeeunkid  " & _
                       ",  ROW_NUMBER() OVER (ORDER BY ISNULL(hremployee_master.employeecode, ''),CONVERT(VARCHAR(8), loginIntime.logindate, 112)) AS SrNo " & _
                       ", ISNULL(hremployee_master.employeecode,'') + ' - ' + ISNULL(hremployee_master.firstname,'') + ' ' + ISNULL(hremployee_master.othername,'') + ' ' + ISNULL(hremployee_master.surname,'') AS EmpCodeName " & _
                       ", ISNULL(dept.name, '') AS department " & _
                       ", CONVERT(VARCHAR(8),loginIntime.logindate,112) AS logindate " & _
                       ", loginIntime.InDevice " & _
                       ", loginIntime.checkintime AS InTime " & _
                       ", loginOuttime.OutDevice " & _
                       ", loginOuttime.checkouttime AS OutTime " & _
                       ",ISNULL(RIGHT('00' + CONVERT(VARCHAR(max), DATEDIFF(SECOND,RIGHT(CONVERT(VARCHAR(20),loginOuttime.checkouttime,100),8),RIGHT(CONVERT(VARCHAR(20),tnashift_tran.endtime,100),8))/ 3600), 2) " & _
                       "+ ':' + RIGHT('00'+ CONVERT(VARCHAR(2), (DATEDIFF(SECOND,RIGHT(CONVERT(VARCHAR(20),loginOuttime.checkouttime,100),8),RIGHT(CONVERT(VARCHAR(20),tnashift_tran.endtime,100),8)) % 3600 ) / 60), 2), '00:00') AS EDeparture "


            Select Case mintLocationId

                Case enAllocation.BRANCH

                    StrQ &= ", ISNULL(hrstation_master.name, '') AS location "

                Case enAllocation.DEPARTMENT_GROUP

                    StrQ &= ", ISNULL(hrdepartment_group_master.name, '') AS location "

                Case enAllocation.SECTION_GROUP

                    StrQ &= ", ISNULL(hrsectiongroup_master.name, '') AS location "

                Case enAllocation.SECTION

                    StrQ &= ", ISNULL(hrsection_master.name, '') AS location "

                Case enAllocation.UNIT_GROUP

                    StrQ &= ", ISNULL(hrunitgroup_master.name, '') AS location "

                Case enAllocation.UNIT

                    StrQ &= ", ISNULL(hrunit_master.name, '') AS location "

                Case enAllocation.TEAM

                    StrQ &= ", ISNULL(hrteam_master.name, '') AS location "

                Case enAllocation.JOB_GROUP

                    StrQ &= ", ISNULL(jg.name, '') AS location "

                Case enAllocation.JOBS

                    StrQ &= ", ISNULL(jb.job_name, '') AS location "

                Case enAllocation.CLASS_GROUP

                    StrQ &= ", ISNULL(hrclassgroup_master.name, '') AS location "

                Case enAllocation.CLASSES

                    StrQ &= ", ISNULL(hrclasses_master.name, '') AS location "

                Case enAllocation.COST_CENTER

                    StrQ &= ", ISNULL(prcostcenter_master.costcentername, '') AS location "

            End Select


            If mintViewIndex > 0 Then
                StrQ &= mstrAnalysis_Fields
            Else
                StrQ &= ", '' AS GName "
            End If

            StrQ &= " FROM tnalogin_summary " & _
                         " JOIN hremployee_master ON hremployee_master.employeeunkid = tnalogin_summary.employeeunkid "

            If xAdvanceJoinQry.Trim.Length > 0 Then
                StrQ &= xAdvanceJoinQry
            End If

            StrQ &= " LEFT JOIN " & _
                         "   ( " & _
                         "       SELECT " & _
                         "         stationunkid " & _
                         "        ,deptgroupunkid " & _
                         "        ,departmentunkid " & _
                         "        ,sectiongroupunkid " & _
                         "        ,sectionunkid " & _
                         "        ,unitgroupunkid " & _
                         "        ,unitunkid " & _
                         "        ,teamunkid " & _
                         "        ,classgroupunkid " & _
                         "        ,classunkid " & _
                         "        ,employeeunkid " & _
                         "        ,ROW_NUMBER()OVER(PARTITION BY employeeunkid ORDER BY effectivedate DESC) AS rno " & _
                         "       FROM hremployee_transfer_tran " & _
                         "       WHERE isvoid = 0 AND CONVERT(CHAR(8),effectivedate,112) <= '" & eZeeDate.convertDate(dtPeriodEnd) & "' " & _
                         "   ) AS Alloc ON Alloc.employeeunkid = hremployee_master.employeeunkid AND Alloc.rno = 1 " & _
                         " LEFT JOIN hrdepartment_master dept ON Alloc.departmentunkid = dept.departmentunkid " & _
                         " LEFT JOIN ( SELECT  tnalogin_tran.logindate,employeeunkid " & _
                                                      ", CASE WHEN MIN(tnalogin_tran.roundoff_intime) IS NULL THEN  ISNULL(RIGHT(CONVERT(VARCHAR(20), MIN(tnalogin_tran.checkintime), 100),8), '00:00') " & _
                                                      "           WHEN ISNULL(RIGHT(CONVERT(VARCHAR(20), MIN(tnalogin_tran.checkintime), 100),8), '00:00') <>  ISNULL(RIGHT(CONVERT(VARCHAR(20), MIN(tnalogin_tran.roundoff_intime), 100),8), '00:00') THEN  " & _
                                                      "      ISNULL(RIGHT(CONVERT(VARCHAR(20), MIN(tnalogin_tran.roundoff_intime), 100),8), '00:00') " & _
                                                      "  ELSE  ISNULL(RIGHT(CONVERT(VARCHAR(20), MIN(tnalogin_tran.checkintime), 100),8), '00:00') END checkintime  " & _
                                                      ", ISNULL(tnalogin_tran.checkindevice,'') AS InDevice " & _
                                                      " FROM tnalogin_Tran " & _
                                                      " WHERE CONVERT(VARCHAR(8),logindate,112) BETWEEN  '" & eZeeDate.convertDate(dtPeriodStart.Date) & "' AND '" & eZeeDate.convertDate(dtPeriodEnd.Date) & "' AND isvoid = 0 " & _
                                                      " GROUP BY logindate,employeeunkid, ISNULL(tnalogin_tran.checkindevice,'') " & _
                           " ) AS loginIntime ON loginIntime.employeeunkid = tnalogin_summary.employeeunkid " & _
                           " JOIN tnashift_master ON tnashift_master.shiftunkid = tnalogin_summary.shiftunkid AND tnashift_master.isactive = 1 " & _
                           " JOIN tnashift_tran ON tnashift_tran.shiftunkid = tnashift_master.shiftunkid AND tnashift_tran.dayid = datepart (dw,tnalogin_summary.login_date) - 1 " & _
                           " AND CONVERT(VARCHAR(8),loginIntime.logindate,112) = CONVERT(VARCHAR(8),tnalogin_summary.login_date,112)  " & _
                           " LEFT JOIN ( SELECT tnalogin_tran.logindate,employeeunkid " & _
                           "                          , CASE WHEN Max(tnalogin_tran.roundoff_outtime) IS NULL THEN ISNULL(RIGHT(CONVERT(VARCHAR(20), MAX(tnalogin_tran.checkouttime), 100),8), '00:00')  " & _
                           "                                     WHEN ISNULL(RIGHT(CONVERT(VARCHAR(20), MAX(tnalogin_tran.checkouttime), 100),8), '00:00') <> ISNULL(RIGHT(CONVERT(VARCHAR(20), MAX(tnalogin_tran.roundoff_outtime), 100),8), '00:00') THEN  " & _
                           "                                ISNULL(RIGHT(CONVERT(VARCHAR(20), MAX(tnalogin_tran.roundoff_outtime), 100),8), '00:00') " & _
                           "                            ELSE ISNULL(RIGHT(CONVERT(VARCHAR(20), MAX(tnalogin_tran.checkouttime), 100),8), '00:00') END checkouttime  " & _
                           "                          , ISNULL(tnalogin_tran.checkoutdevice,'') AS OutDevice " & _
                           "                    FROM tnalogin_Tran " & _
                           "                    WHERE isvoid = 0 AND CONVERT(VARCHAR(8),logindate,112) BETWEEN '" & eZeeDate.convertDate(dtPeriodStart.Date) & "' AND '" & eZeeDate.convertDate(dtPeriodEnd.Date) & "' AND isvoid = 0 " & _
                           "					GROUP BY logindate,employeeunkid, ISNULL(tnalogin_tran.checkoutdevice,'')  " & _
                           " ) AS loginOuttime ON loginOuttime.employeeunkid = tnalogin_summary.employeeunkid  " & _
                           " AND CONVERT(VARCHAR(8),loginOuttime.logindate,112) = CONVERT(VARCHAR(8),tnalogin_summary.login_date,112) "



            Select Case mintLocationId

                Case enAllocation.BRANCH

                    StrQ &= " LEFT JOIN hrstation_master ON hrstation_master.stationunkid = Alloc.stationunkid "

                Case enAllocation.DEPARTMENT_GROUP

                    StrQ &= " LEFT JOIN hrdepartment_group_master ON hrdepartment_group_master.deptgroupunkid = Alloc.deptgroupunkid "

                Case enAllocation.SECTION_GROUP

                    StrQ &= " LEFT JOIN hrsectiongroup_master ON hrsectiongroup_master.sectiongroupunkid = Alloc.sectiongroupunkid "

                Case enAllocation.SECTION

                    StrQ &= " LEFT JOIN hrsection_master ON hrsection_master.sectionunkid = Alloc.sectionunkid "

                Case enAllocation.UNIT_GROUP

                    StrQ &= " LEFT JOIN hrunitgroup_master ON hrunitgroup_master.unitgroupunkid = Alloc.unitgroupunkid "

                Case enAllocation.UNIT

                    StrQ &= " LEFT JOIN hrunit_master ON hrunit_master.unitunkid = Alloc.unitunkid "

                Case enAllocation.TEAM

                    StrQ &= " LEFT JOIN hrteam_master ON hrteam_master.teamunkid = Alloc.teamunkid "

                Case enAllocation.JOB_GROUP

                    StrQ &= " LEFT JOIN " & _
                                  "   ( " & _
                                  "       SELECT " & _
                                  "           jobgroupunkid " & _
                                  "           ,employeeunkid " & _
                                  "           ,ROW_NUMBER()OVER(PARTITION BY employeeunkid ORDER BY effectivedate DESC) AS rno " & _
                                  "       FROM hremployee_categorization_tran " & _
                                  "       WHERE isvoid = 0 AND CONVERT(CHAR(8),effectivedate,112) <= '" & eZeeDate.convertDate(dtPeriodEnd) & "' " & _
                                  "   ) AS Jobgrp ON Jobgrp.employeeunkid = hremployee_master.employeeunkid AND Jobgrp.rno = 1 " & _
                                  " LEFT JOIN hrjobgroup_master jg ON Jobgrp.jobgroupunkid = jg.jobgroupunkid "

                Case enAllocation.JOBS

                    StrQ &= " LEFT JOIN " & _
                                  "   ( " & _
                                  "       SELECT " & _
                                  "           jobunkid " & _
                                  "           ,employeeunkid " & _
                                  "           ,ROW_NUMBER()OVER(PARTITION BY employeeunkid ORDER BY effectivedate DESC) AS rno " & _
                                  "       FROM hremployee_categorization_tran " & _
                                  "       WHERE isvoid = 0 AND CONVERT(CHAR(8),effectivedate,112) <= '" & eZeeDate.convertDate(dtPeriodEnd) & "' " & _
                                  "   ) AS Jobs ON Jobs.employeeunkid = hremployee_master.employeeunkid AND Jobs.rno = 1 " & _
                                  " LEFT JOIN hrjob_master jb ON Jobs.jobunkid = jb.jobunkid "

                Case enAllocation.CLASS_GROUP

                    StrQ &= " LEFT JOIN hrclassgroup_master ON hrclassgroup_master.classgroupunkid = Alloc.classgroupunkid "

                Case enAllocation.CLASSES

                    StrQ &= " LEFT JOIN hrclasses_master ON hrclasses_master.classesunkid = Alloc.classunkid "

                Case enAllocation.COST_CENTER

                    StrQ &= " LEFT JOIN " & _
                                  "   ( " & _
                                  "       SELECT " & _
                                  "           cctranheadvalueid " & _
                                  "           ,employeeunkid " & _
                                  "           ,ROW_NUMBER()OVER(PARTITION BY employeeunkid ORDER BY effectivedate DESC) AS rno " & _
                                  "       FROM hremployee_cctranhead_tran " & _
                                  "       WHERE isvoid = 0 AND istransactionhead = 0 AND CONVERT(CHAR(8),effectivedate,112) <= '" & eZeeDate.convertDate(dtPeriodEnd) & "' " & _
                                  "   ) AS CC ON CC.employeeunkid = hremployee_master.employeeunkid AND CC.rno = 1 " & _
                                  " LEFT JOIN prcostcenter_master ON prcostcenter_master.costcenterunkid = CC.cctranheadvalueid "

            End Select


            If mintViewIndex > 0 Then
                StrQ &= mstrAnalysis_Join
            End If

            If xUACQry.Trim.Length > 0 Then
                StrQ &= xUACQry
            End If

            StrQ &= " WHERE  1=1  AND (loginintime.checkintime IS NOT NULL OR loginOuttime.checkouttime IS NOT NULL) AND tnalogin_summary.elrgoing_grace > 60 "

            If mstrAdvance_Filter.Trim.Length > 0 Then
                StrQ &= " AND " & mstrAdvance_Filter
            End If

            If xUACFiltrQry.Trim.Length > 0 Then
                StrQ &= " AND " & xUACFiltrQry
            End If

            If mintEmployeeId > 0 Then
                StrQ &= " AND hremployee_master.employeeunkid = " & mintEmployeeId
            End If

            Call FilterTitleAndFilterQuery()

            StrQ &= Me._FilterQuery

            StrQ &= mstrOrderByQuery

            dsList = objDataOperation.ExecQuery(StrQ, "DataTable")

            If objDataOperation.ErrorMessage <> "" Then
                Throw New Exception(objDataOperation.ErrorNumber & ":" & objDataOperation.ErrorMessage)
            End If


            rpt_Data = New ArutiReport.Designer.dsArutiReport
            For Each dtRow As DataRow In dsList.Tables("DataTable").Rows
                Dim rpt_Row As DataRow = rpt_Data.Tables("ArutiTable").NewRow
                rpt_Row.Item("Column1") = dtRow.Item("GName").ToString().Trim()
                rpt_Row.Item("Column2") = dtRow.Item("EmpCodeName").ToString().Trim()
                rpt_Row.Item("Column3") = eZeeDate.convertDate(dtRow.Item("logindate").ToString().Trim()).ToShortDateString()
                rpt_Row.Item("Column4") = dtRow.Item("location").ToString().Trim()
                rpt_Row.Item("Column5") = dtRow.Item("department").ToString().Trim()
                rpt_Row.Item("Column6") = dtRow.Item("InDevice").ToString().Trim()

                If Not IsDBNull(dtRow.Item("InTime")) Then
                    rpt_Row.Item("Column7") = dtRow.Item("InTime").ToString()
                End If

                rpt_Row.Item("Column8") = dtRow.Item("OutDevice").ToString()

                If Not IsDBNull(dtRow.Item("OutTime")) Then
                    rpt_Row.Item("Column9") = dtRow.Item("OutTime").ToString()
                End If
                rpt_Row.Item("Column10") = dtRow.Item("EDeparture").ToString()
                rpt_Data.Tables("ArutiTable").Rows.Add(rpt_Row)
            Next

            objRpt = New ArutiReport.Designer.rptTRAEarlygoing
            ConfigParameter._Object.GetReportSettings(CInt(mstrReportId))
            Dim arrImageRow As DataRow = Nothing
            arrImageRow = rpt_Data.Tables("ArutiImage").NewRow()


            ReportFunction.Logo_Display(objRpt, _
                                        ConfigParameter._Object._IsDisplayLogo, _
                                        ConfigParameter._Object._ShowLogoRightSide, _
                                        "arutiLogo1", _
                                        "arutiLogo2", _
                                        arrImageRow, _
                                        "txtCompanyName", _
                                        "txtReportName", _
                                        "txtFilterDescription", _
                                        ConfigParameter._Object._GetLeftMargin, _
                                        ConfigParameter._Object._GetRightMargin)

            rpt_Data.Tables("ArutiImage").Rows.Add(arrImageRow)

            If rpt_Data.Tables("ArutiTable").Rows.Count <= 0 Then
                rpt_Data.Tables("ArutiTable").Rows.Add("")
            End If

            If ConfigParameter._Object._IsShowPreparedBy = True Then
                Call ReportFunction.TextChange(objRpt, "lblPreparedBy", Language.getMessage(mstrModuleName, 7, "Prepared By :"))
                Call ReportFunction.TextChange(objRpt, "txtPreparedBy", User._Object._Username)
            Else
                Call ReportFunction.EnableSuppressSection(objRpt, "ReportFooterSection4", True)
            End If

            If ConfigParameter._Object._IsShowCheckedBy = True Then
                Call ReportFunction.TextChange(objRpt, "lblCheckedBy", Language.getMessage(mstrModuleName, 8, "Checked By :"))
            Else
                Call ReportFunction.EnableSuppressSection(objRpt, "ReportFooterSection3", True)
            End If

            If ConfigParameter._Object._IsShowApprovedBy = True Then
                Call ReportFunction.TextChange(objRpt, "lblApprovedBy", Language.getMessage(mstrModuleName, 9, "Approved By :"))
            Else
                Call ReportFunction.EnableSuppressSection(objRpt, "ReportFooterSection2", True)
            End If

            If ConfigParameter._Object._IsShowReceivedBy = True Then
                Call ReportFunction.TextChange(objRpt, "lblReceivedBy", Language.getMessage(mstrModuleName, 10, "Received By :"))
            Else
                Call ReportFunction.EnableSuppressSection(objRpt, "ReportFooterSection1", True)
            End If

            If mblnEachEmployeeOnPage = True Then
                ReportFunction.EnableNewPageBefore(objRpt, "GroupHeaderSection1", True)
                objRpt.DataDefinition.FormulaFields("frmlNewPage").Text = "if Previous ({ArutiTable.Column1}) <> {ArutiTable.Column1} then 0 Else 1 "
            Else
                ReportFunction.EnableNewPageBefore(objRpt, "GroupHeaderSection1", False)
                objRpt.DataDefinition.FormulaFields("frmlNewPage").Text = "0"
            End If

            objRpt.SetDataSource(rpt_Data)

            Call ReportFunction.TextChange(objRpt, "txtSrNo", Language.getMessage(mstrModuleName, 20, "Sr.No"))
            Call ReportFunction.TextChange(objRpt, "txtLoginDate", Language.getMessage(mstrModuleName, 21, "Login Date"))
            Call ReportFunction.TextChange(objRpt, "txtLocation", Language.getMessage(mstrModuleName, 22, "Location"))
            Call ReportFunction.TextChange(objRpt, "txtDepartment", Language.getMessage(mstrModuleName, 23, "Department"))
            Call ReportFunction.TextChange(objRpt, "txtInDevice", Language.getMessage(mstrModuleName, 24, "In Device"))
            Call ReportFunction.TextChange(objRpt, "txtInTime", Language.getMessage(mstrModuleName, 25, "In Time"))
            Call ReportFunction.TextChange(objRpt, "txtOutDevice", Language.getMessage(mstrModuleName, 26, "Out Device"))
            Call ReportFunction.TextChange(objRpt, "txtOuttime", Language.getMessage(mstrModuleName, 27, "Out Time"))
            Call ReportFunction.TextChange(objRpt, "txtEarlyDeparture", Language.getMessage(mstrModuleName, 28, "Early Departure"))
            Call ReportFunction.TextChange(objRpt, "txtGroupName", mstrReport_GroupName)
            Call ReportFunction.TextChange(objRpt, "txtEmployee", Language.getMessage(mstrModuleName, 5, "Employee: "))
            Call ReportFunction.TextChange(objRpt, "txtTotalEmployee", Language.getMessage(mstrModuleName, 29, "Total Employee : "))

            If mintViewIndex < 0 Then
                Call ReportFunction.EnableSuppressSection(objRpt, "GroupHeaderSection1", True)
                Call ReportFunction.EnableSuppressSection(objRpt, "GroupFooterSection1", True)
            End If


            Call ReportFunction.TextChange(objRpt, "lblPrintedBy", Language.getMessage(mstrModuleName, 18, "Printed By :"))
            Call ReportFunction.TextChange(objRpt, "lblPrintedDate", Language.getMessage(mstrModuleName, 19, "Printed Date :"))

            Call ReportFunction.TextChange(objRpt, "txtPrintedBy", Me._UserName)
            Call ReportFunction.TextChange(objRpt, "txtPrintedDate", Me._PrintDate)

            Call ReportFunction.TextChange(objRpt, "txtReportName", Me._ReportName)
            Call ReportFunction.TextChange(objRpt, "txtCompanyName", Me._CompanyName)
            Call ReportFunction.TextChange(objRpt, "txtFilterDescription", Me._FilterTitle)

            Return objRpt
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: Generate_TRA_Early_Departure_Report; Module Name: " & mstrModuleName)
            Return Nothing
        End Try
    End Function

    Public Function Generate_TRA_Late_Coming_Report(ByVal strDatabaseName As String, _
                                           ByVal intUserUnkid As Integer, _
                                           ByVal intYearUnkid As Integer, _
                                           ByVal intCompanyUnkid As Integer, _
                                           ByVal dtPeriodStart As Date, _
                                           ByVal dtPeriodEnd As Date, _
                                           ByVal strUserModeSetting As String, _
                                           ByVal blnOnlyApproved As Boolean) As CrystalDecisions.CrystalReports.Engine.ReportClass
        Dim objRpt As CrystalDecisions.CrystalReports.Engine.ReportClass = Nothing
        Dim StrQ As String = ""
        Dim dsList As New DataSet
        Dim rpt_Data As ArutiReport.Designer.dsArutiReport
        Try

            objDataOperation = New clsDataOperation
            objDataOperation.ClearParameters()

            Dim xDateJoinQry, xDateFilterQry, xUACQry, xUACFiltrQry, xAdvanceJoinQry As String
            xDateJoinQry = "" : xDateFilterQry = "" : xUACQry = "" : xUACFiltrQry = "" : xAdvanceJoinQry = ""
            Call GetDatesFilterString(xDateJoinQry, xDateFilterQry, dtPeriodStart, dtPeriodEnd, , , strDatabaseName)
            If mblnIncludeAccessFilterQry Then Call NewAccessLevelFilterString(xUACQry, xUACFiltrQry, dtPeriodEnd, blnOnlyApproved, strDatabaseName, intUserUnkid, intCompanyUnkid, intYearUnkid, strUserModeSetting)
            Call GetAdvanceFilterQry(xAdvanceJoinQry, dtPeriodEnd, strDatabaseName)

            StrQ = " SELECT hremployee_master.employeeunkid  " & _
                       ",  ROW_NUMBER() OVER (ORDER BY ISNULL(hremployee_master.employeecode, ''),CONVERT(VARCHAR(8), loginIntime.logindate, 112)) AS SrNo " & _
                       ", ISNULL(hremployee_master.employeecode,'') + ' - ' + ISNULL(hremployee_master.firstname,'') + ' ' + ISNULL(hremployee_master.othername,'') + ' ' + ISNULL(hremployee_master.surname,'') AS EmpCodeName " & _
                       ", ISNULL(dept.name, '') AS department " & _
                       ", CONVERT(VARCHAR(8),loginIntime.logindate,112) AS logindate " & _
                       ", loginIntime.InDevice " & _
                       ", loginIntime.checkintime AS InTime " & _
                       ", loginOuttime.OutDevice " & _
                       ", loginOuttime.checkouttime AS OutTime " & _
                       ",ISNULL(RIGHT('00' + CONVERT(VARCHAR(max), DATEDIFF(SECOND,RIGHT(CONVERT(VARCHAR(20),tnashift_tran.starttime,100),8),RIGHT(CONVERT(VARCHAR(20),loginIntime.checkintime,100),8))/ 3600), 2) " & _
                       "+ ':' + RIGHT('00'+ CONVERT(VARCHAR(2), (DATEDIFF(SECOND,RIGHT(CONVERT(VARCHAR(20),tnashift_tran.starttime,100),8),RIGHT(CONVERT(VARCHAR(20),loginIntime.checkintime,100),8)) % 3600 ) / 60), 2), '00:00') AS LComing "


            Select Case mintLocationId

                Case enAllocation.BRANCH

                    StrQ &= ", ISNULL(hrstation_master.name, '') AS location "

                Case enAllocation.DEPARTMENT_GROUP

                    StrQ &= ", ISNULL(hrdepartment_group_master.name, '') AS location "

                Case enAllocation.SECTION_GROUP

                    StrQ &= ", ISNULL(hrsectiongroup_master.name, '') AS location "

                Case enAllocation.SECTION

                    StrQ &= ", ISNULL(hrsection_master.name, '') AS location "

                Case enAllocation.UNIT_GROUP

                    StrQ &= ", ISNULL(hrunitgroup_master.name, '') AS location "

                Case enAllocation.UNIT

                    StrQ &= ", ISNULL(hrunit_master.name, '') AS location "

                Case enAllocation.TEAM

                    StrQ &= ", ISNULL(hrteam_master.name, '') AS location "

                Case enAllocation.JOB_GROUP

                    StrQ &= ", ISNULL(jg.name, '') AS location "

                Case enAllocation.JOBS

                    StrQ &= ", ISNULL(jb.job_name, '') AS location "

                Case enAllocation.CLASS_GROUP

                    StrQ &= ", ISNULL(hrclassgroup_master.name, '') AS location "

                Case enAllocation.CLASSES

                    StrQ &= ", ISNULL(hrclasses_master.name, '') AS location "

                Case enAllocation.COST_CENTER

                    StrQ &= ", ISNULL(prcostcenter_master.costcentername, '') AS location "

            End Select


            If mintViewIndex > 0 Then
                StrQ &= mstrAnalysis_Fields
            Else
                StrQ &= ", '' AS GName "
            End If

            StrQ &= " FROM tnalogin_summary " & _
                         " JOIN hremployee_master ON hremployee_master.employeeunkid = tnalogin_summary.employeeunkid "

            If xAdvanceJoinQry.Trim.Length > 0 Then
                StrQ &= xAdvanceJoinQry
            End If

            StrQ &= " LEFT JOIN " & _
                         "   ( " & _
                         "       SELECT " & _
                         "         stationunkid " & _
                         "        ,deptgroupunkid " & _
                         "        ,departmentunkid " & _
                         "        ,sectiongroupunkid " & _
                         "        ,sectionunkid " & _
                         "        ,unitgroupunkid " & _
                         "        ,unitunkid " & _
                         "        ,teamunkid " & _
                         "        ,classgroupunkid " & _
                         "        ,classunkid " & _
                         "        ,employeeunkid " & _
                         "        ,ROW_NUMBER()OVER(PARTITION BY employeeunkid ORDER BY effectivedate DESC) AS rno " & _
                         "       FROM hremployee_transfer_tran " & _
                         "       WHERE isvoid = 0 AND CONVERT(CHAR(8),effectivedate,112) <= '" & eZeeDate.convertDate(dtPeriodEnd) & "' " & _
                         "   ) AS Alloc ON Alloc.employeeunkid = hremployee_master.employeeunkid AND Alloc.rno = 1 " & _
                         " LEFT JOIN hrdepartment_master dept ON Alloc.departmentunkid = dept.departmentunkid " & _
                         " LEFT JOIN ( SELECT  tnalogin_tran.logindate,employeeunkid " & _
                                                      ", CASE WHEN MIN(tnalogin_tran.roundoff_intime) IS NULL THEN  ISNULL(RIGHT(CONVERT(VARCHAR(20), MIN(tnalogin_tran.checkintime), 100),8), '00:00') " & _
                                                      "           WHEN ISNULL(RIGHT(CONVERT(VARCHAR(20), MIN(tnalogin_tran.checkintime), 100),8), '00:00') <>  ISNULL(RIGHT(CONVERT(VARCHAR(20), MIN(tnalogin_tran.roundoff_intime), 100),8), '00:00') THEN  " & _
                                                      "      ISNULL(RIGHT(CONVERT(VARCHAR(20), MIN(tnalogin_tran.roundoff_intime), 100),8), '00:00') " & _
                                                      "  ELSE  ISNULL(RIGHT(CONVERT(VARCHAR(20), MIN(tnalogin_tran.checkintime), 100),8), '00:00') END checkintime  " & _
                                                      ", ISNULL(tnalogin_tran.checkindevice,'') AS InDevice " & _
                                                      " FROM tnalogin_Tran " & _
                                                      " WHERE CONVERT(VARCHAR(8),logindate,112) BETWEEN  '" & eZeeDate.convertDate(dtPeriodStart.Date) & "' AND '" & eZeeDate.convertDate(dtPeriodEnd.Date) & "' AND isvoid = 0 " & _
                                                      " GROUP BY logindate,employeeunkid, ISNULL(tnalogin_tran.checkindevice,'') " & _
                           " ) AS loginIntime ON loginIntime.employeeunkid = tnalogin_summary.employeeunkid " & _
                           " JOIN tnashift_master ON tnashift_master.shiftunkid = tnalogin_summary.shiftunkid AND tnashift_master.isactive = 1 " & _
                           " JOIN tnashift_tran ON tnashift_tran.shiftunkid = tnashift_master.shiftunkid AND tnashift_tran.dayid = datepart (dw,tnalogin_summary.login_date) - 1 " & _
                           " AND CONVERT(VARCHAR(8),loginIntime.logindate,112) = CONVERT(VARCHAR(8),tnalogin_summary.login_date,112)  " & _
                           " LEFT JOIN ( SELECT tnalogin_tran.logindate,employeeunkid " & _
                           "                          , CASE WHEN Max(tnalogin_tran.roundoff_outtime) IS NULL THEN ISNULL(RIGHT(CONVERT(VARCHAR(20), MAX(tnalogin_tran.checkouttime), 100),8), '00:00')  " & _
                           "                                     WHEN ISNULL(RIGHT(CONVERT(VARCHAR(20), MAX(tnalogin_tran.checkouttime), 100),8), '00:00') <> ISNULL(RIGHT(CONVERT(VARCHAR(20), MAX(tnalogin_tran.roundoff_outtime), 100),8), '00:00') THEN  " & _
                           "                                ISNULL(RIGHT(CONVERT(VARCHAR(20), MAX(tnalogin_tran.roundoff_outtime), 100),8), '00:00') " & _
                           "                            ELSE ISNULL(RIGHT(CONVERT(VARCHAR(20), MAX(tnalogin_tran.checkouttime), 100),8), '00:00') END checkouttime  " & _
                           "                          , ISNULL(tnalogin_tran.checkoutdevice,'') AS OutDevice " & _
                           "                    FROM tnalogin_Tran " & _
                           "                    WHERE isvoid = 0 AND CONVERT(VARCHAR(8),logindate,112) BETWEEN '" & eZeeDate.convertDate(dtPeriodStart.Date) & "' AND '" & eZeeDate.convertDate(dtPeriodEnd.Date) & "' AND isvoid = 0 " & _
                           "					GROUP BY logindate,employeeunkid, ISNULL(tnalogin_tran.checkoutdevice,'')  " & _
                           " ) AS loginOuttime ON loginOuttime.employeeunkid = tnalogin_summary.employeeunkid  " & _
                           " AND CONVERT(VARCHAR(8),loginOuttime.logindate,112) = CONVERT(VARCHAR(8),tnalogin_summary.login_date,112) "



            Select Case mintLocationId

                Case enAllocation.BRANCH

                    StrQ &= " LEFT JOIN hrstation_master ON hrstation_master.stationunkid = Alloc.stationunkid "

                Case enAllocation.DEPARTMENT_GROUP

                    StrQ &= " LEFT JOIN hrdepartment_group_master ON hrdepartment_group_master.deptgroupunkid = Alloc.deptgroupunkid "

                Case enAllocation.SECTION_GROUP

                    StrQ &= " LEFT JOIN hrsectiongroup_master ON hrsectiongroup_master.sectiongroupunkid = Alloc.sectiongroupunkid "

                Case enAllocation.SECTION

                    StrQ &= " LEFT JOIN hrsection_master ON hrsection_master.sectionunkid = Alloc.sectionunkid "

                Case enAllocation.UNIT_GROUP

                    StrQ &= " LEFT JOIN hrunitgroup_master ON hrunitgroup_master.unitgroupunkid = Alloc.unitgroupunkid "

                Case enAllocation.UNIT

                    StrQ &= " LEFT JOIN hrunit_master ON hrunit_master.unitunkid = Alloc.unitunkid "

                Case enAllocation.TEAM

                    StrQ &= " LEFT JOIN hrteam_master ON hrteam_master.teamunkid = Alloc.teamunkid "

                Case enAllocation.JOB_GROUP

                    StrQ &= " LEFT JOIN " & _
                                  "   ( " & _
                                  "       SELECT " & _
                                  "           jobgroupunkid " & _
                                  "           ,employeeunkid " & _
                                  "           ,ROW_NUMBER()OVER(PARTITION BY employeeunkid ORDER BY effectivedate DESC) AS rno " & _
                                  "       FROM hremployee_categorization_tran " & _
                                  "       WHERE isvoid = 0 AND CONVERT(CHAR(8),effectivedate,112) <= '" & eZeeDate.convertDate(dtPeriodEnd) & "' " & _
                                  "   ) AS Jobgrp ON Jobgrp.employeeunkid = hremployee_master.employeeunkid AND Jobgrp.rno = 1 " & _
                                  " LEFT JOIN hrjobgroup_master jg ON Jobgrp.jobgroupunkid = jg.jobgroupunkid "

                Case enAllocation.JOBS

                    StrQ &= " LEFT JOIN " & _
                                  "   ( " & _
                                  "       SELECT " & _
                                  "           jobunkid " & _
                                  "           ,employeeunkid " & _
                                  "           ,ROW_NUMBER()OVER(PARTITION BY employeeunkid ORDER BY effectivedate DESC) AS rno " & _
                                  "       FROM hremployee_categorization_tran " & _
                                  "       WHERE isvoid = 0 AND CONVERT(CHAR(8),effectivedate,112) <= '" & eZeeDate.convertDate(dtPeriodEnd) & "' " & _
                                  "   ) AS Jobs ON Jobs.employeeunkid = hremployee_master.employeeunkid AND Jobs.rno = 1 " & _
                                  " LEFT JOIN hrjob_master jb ON Jobs.jobunkid = jb.jobunkid "

                Case enAllocation.CLASS_GROUP

                    StrQ &= " LEFT JOIN hrclassgroup_master ON hrclassgroup_master.classgroupunkid = Alloc.classgroupunkid "

                Case enAllocation.CLASSES

                    StrQ &= " LEFT JOIN hrclasses_master ON hrclasses_master.classesunkid = Alloc.classunkid "

                Case enAllocation.COST_CENTER

                    StrQ &= " LEFT JOIN " & _
                                  "   ( " & _
                                  "       SELECT " & _
                                  "           cctranheadvalueid " & _
                                  "           ,employeeunkid " & _
                                  "           ,ROW_NUMBER()OVER(PARTITION BY employeeunkid ORDER BY effectivedate DESC) AS rno " & _
                                  "       FROM hremployee_cctranhead_tran " & _
                                  "       WHERE isvoid = 0 AND istransactionhead = 0 AND CONVERT(CHAR(8),effectivedate,112) <= '" & eZeeDate.convertDate(dtPeriodEnd) & "' " & _
                                  "   ) AS CC ON CC.employeeunkid = hremployee_master.employeeunkid AND CC.rno = 1 " & _
                                  " LEFT JOIN prcostcenter_master ON prcostcenter_master.costcenterunkid = CC.cctranheadvalueid "

            End Select


            If mintViewIndex > 0 Then
                StrQ &= mstrAnalysis_Join
            End If

            If xUACQry.Trim.Length > 0 Then
                StrQ &= xUACQry
            End If

            StrQ &= " WHERE  1=1  AND (loginintime.checkintime IS NOT NULL OR loginOuttime.checkouttime IS NOT NULL) AND tnalogin_summary.ltcoming_grace > 60 "

            If mstrAdvance_Filter.Trim.Length > 0 Then
                StrQ &= " AND " & mstrAdvance_Filter
            End If

            If xUACFiltrQry.Trim.Length > 0 Then
                StrQ &= " AND " & xUACFiltrQry
            End If

            If mintEmployeeId > 0 Then
                StrQ &= " AND hremployee_master.employeeunkid = " & mintEmployeeId
            End If

            Call FilterTitleAndFilterQuery()

            StrQ &= Me._FilterQuery

            StrQ &= mstrOrderByQuery

            dsList = objDataOperation.ExecQuery(StrQ, "DataTable")

            If objDataOperation.ErrorMessage <> "" Then
                Throw New Exception(objDataOperation.ErrorNumber & ":" & objDataOperation.ErrorMessage)
            End If


            rpt_Data = New ArutiReport.Designer.dsArutiReport
            For Each dtRow As DataRow In dsList.Tables("DataTable").Rows
                Dim rpt_Row As DataRow = rpt_Data.Tables("ArutiTable").NewRow
                rpt_Row.Item("Column1") = dtRow.Item("GName").ToString().Trim()
                rpt_Row.Item("Column2") = dtRow.Item("EmpCodeName").ToString().Trim()
                rpt_Row.Item("Column3") = eZeeDate.convertDate(dtRow.Item("logindate").ToString().Trim()).ToShortDateString()
                rpt_Row.Item("Column4") = dtRow.Item("location").ToString().Trim()
                rpt_Row.Item("Column5") = dtRow.Item("department").ToString().Trim()
                rpt_Row.Item("Column6") = dtRow.Item("InDevice").ToString().Trim()

                If Not IsDBNull(dtRow.Item("InTime")) Then
                    rpt_Row.Item("Column7") = dtRow.Item("InTime").ToString()
                End If

                rpt_Row.Item("Column8") = dtRow.Item("OutDevice").ToString()

                If Not IsDBNull(dtRow.Item("OutTime")) Then
                    rpt_Row.Item("Column9") = dtRow.Item("OutTime").ToString()
                End If
                rpt_Row.Item("Column10") = dtRow.Item("LComing").ToString()
                rpt_Data.Tables("ArutiTable").Rows.Add(rpt_Row)
            Next

            objRpt = New ArutiReport.Designer.rptTRALateComing
            ConfigParameter._Object.GetReportSettings(CInt(mstrReportId))
            Dim arrImageRow As DataRow = Nothing
            arrImageRow = rpt_Data.Tables("ArutiImage").NewRow()


            ReportFunction.Logo_Display(objRpt, _
                                        ConfigParameter._Object._IsDisplayLogo, _
                                        ConfigParameter._Object._ShowLogoRightSide, _
                                        "arutiLogo1", _
                                        "arutiLogo2", _
                                        arrImageRow, _
                                        "txtCompanyName", _
                                        "txtReportName", _
                                        "txtFilterDescription", _
                                        ConfigParameter._Object._GetLeftMargin, _
                                        ConfigParameter._Object._GetRightMargin)

            rpt_Data.Tables("ArutiImage").Rows.Add(arrImageRow)

            If rpt_Data.Tables("ArutiTable").Rows.Count <= 0 Then
                rpt_Data.Tables("ArutiTable").Rows.Add("")
            End If

            If ConfigParameter._Object._IsShowPreparedBy = True Then
                Call ReportFunction.TextChange(objRpt, "lblPreparedBy", Language.getMessage(mstrModuleName, 7, "Prepared By :"))
                Call ReportFunction.TextChange(objRpt, "txtPreparedBy", User._Object._Username)
            Else
                Call ReportFunction.EnableSuppressSection(objRpt, "ReportFooterSection4", True)
            End If

            If ConfigParameter._Object._IsShowCheckedBy = True Then
                Call ReportFunction.TextChange(objRpt, "lblCheckedBy", Language.getMessage(mstrModuleName, 8, "Checked By :"))
            Else
                Call ReportFunction.EnableSuppressSection(objRpt, "ReportFooterSection3", True)
            End If

            If ConfigParameter._Object._IsShowApprovedBy = True Then
                Call ReportFunction.TextChange(objRpt, "lblApprovedBy", Language.getMessage(mstrModuleName, 9, "Approved By :"))
            Else
                Call ReportFunction.EnableSuppressSection(objRpt, "ReportFooterSection2", True)
            End If

            If ConfigParameter._Object._IsShowReceivedBy = True Then
                Call ReportFunction.TextChange(objRpt, "lblReceivedBy", Language.getMessage(mstrModuleName, 10, "Received By :"))
            Else
                Call ReportFunction.EnableSuppressSection(objRpt, "ReportFooterSection1", True)
            End If

            If mblnEachEmployeeOnPage = True Then
                ReportFunction.EnableNewPageBefore(objRpt, "GroupHeaderSection1", True)
                objRpt.DataDefinition.FormulaFields("frmlNewPage").Text = "if Previous ({ArutiTable.Column1}) <> {ArutiTable.Column1} then 0 Else 1 "
            Else
                ReportFunction.EnableNewPageBefore(objRpt, "GroupHeaderSection1", False)
                objRpt.DataDefinition.FormulaFields("frmlNewPage").Text = "0"
            End If

            objRpt.SetDataSource(rpt_Data)

            Call ReportFunction.TextChange(objRpt, "txtSrNo", Language.getMessage(mstrModuleName, 20, "Sr.No"))
            Call ReportFunction.TextChange(objRpt, "txtLoginDate", Language.getMessage(mstrModuleName, 21, "Login Date"))
            Call ReportFunction.TextChange(objRpt, "txtLocation", Language.getMessage(mstrModuleName, 22, "Location"))
            Call ReportFunction.TextChange(objRpt, "txtDepartment", Language.getMessage(mstrModuleName, 23, "Department"))
            Call ReportFunction.TextChange(objRpt, "txtInDevice", Language.getMessage(mstrModuleName, 24, "In Device"))
            Call ReportFunction.TextChange(objRpt, "txtInTime", Language.getMessage(mstrModuleName, 25, "In Time"))
            Call ReportFunction.TextChange(objRpt, "txtOutDevice", Language.getMessage(mstrModuleName, 26, "Out Device"))
            Call ReportFunction.TextChange(objRpt, "txtOuttime", Language.getMessage(mstrModuleName, 27, "Out Time"))
            Call ReportFunction.TextChange(objRpt, "txtLateComing", Language.getMessage(mstrModuleName, 30, "Late Coming"))
            Call ReportFunction.TextChange(objRpt, "txtGroupName", mstrReport_GroupName)
            Call ReportFunction.TextChange(objRpt, "txtEmployee", Language.getMessage(mstrModuleName, 5, "Employee: "))
            Call ReportFunction.TextChange(objRpt, "txtTotalEmployee", Language.getMessage(mstrModuleName, 29, "Total Employee : "))

            If mintViewIndex < 0 Then
                Call ReportFunction.EnableSuppressSection(objRpt, "GroupHeaderSection1", True)
                Call ReportFunction.EnableSuppressSection(objRpt, "GroupFooterSection1", True)
            End If


            Call ReportFunction.TextChange(objRpt, "lblPrintedBy", Language.getMessage(mstrModuleName, 18, "Printed By :"))
            Call ReportFunction.TextChange(objRpt, "lblPrintedDate", Language.getMessage(mstrModuleName, 19, "Printed Date :"))

            Call ReportFunction.TextChange(objRpt, "txtPrintedBy", Me._UserName)
            Call ReportFunction.TextChange(objRpt, "txtPrintedDate", Me._PrintDate)

            Call ReportFunction.TextChange(objRpt, "txtReportName", Me._ReportName)
            Call ReportFunction.TextChange(objRpt, "txtCompanyName", Me._CompanyName)
            Call ReportFunction.TextChange(objRpt, "txtFilterDescription", Me._FilterTitle)

            Return objRpt
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: Generate_TRA_Late_Coming_Report; Module Name: " & mstrModuleName)
            Return Nothing
        End Try
    End Function

    'Pinkal (14-Feb-2022) -- End



#End Region


    '<Language> This Auto Generated Text Please Do Not Modify it.
#Region " Language & UI Settings "
    Public Shared Sub SetMessages()
        Try
            Language.setMessage(mstrModuleName, 1, "Employee Code")
            Language.setMessage(mstrModuleName, 2, "Employee")
            Language.setMessage(mstrModuleName, 3, "From Date:")
            Language.setMessage(mstrModuleName, 4, "To Date:")
            Language.setMessage(mstrModuleName, 5, "Employee:")
            Language.setMessage(mstrModuleName, 6, " Order By :")
            Language.setMessage(mstrModuleName, 7, "Prepared By :")
            Language.setMessage(mstrModuleName, 8, "Checked By :")
			Language.setMessage(mstrModuleName, 9, "Approved By :")
            Language.setMessage(mstrModuleName, 10, "Received By :")
			Language.setMessage(mstrModuleName, 11, "Total Late Arrival Count")
			Language.setMessage(mstrModuleName, 12, "Total Early Going Count")
			Language.setMessage(mstrModuleName, 13, "Department")
			Language.setMessage(mstrModuleName, 14, "Job")
			Language.setMessage(mstrModuleName, 15, "Shift Hrs")
			Language.setMessage(mstrModuleName, 16, "Late In Time")
			Language.setMessage(mstrModuleName, 17, "Early Going Time")
			Language.setMessage(mstrModuleName, 18, "Printed By :")
			Language.setMessage(mstrModuleName, 19, "Printed Date :")
			Language.setMessage(mstrModuleName, 20, "Sr.No")
			Language.setMessage(mstrModuleName, 21, "Login Date")
			Language.setMessage(mstrModuleName, 22, "Location")
			Language.setMessage(mstrModuleName, 23, "Department")
			Language.setMessage(mstrModuleName, 24, "In Device")
			Language.setMessage(mstrModuleName, 25, "In Time")
			Language.setMessage(mstrModuleName, 26, "Out Device")
			Language.setMessage(mstrModuleName, 27, "Out Time")
			Language.setMessage(mstrModuleName, 28, "Early Departure")
			Language.setMessage(mstrModuleName, 29, "Total Employee :")
            Language.setMessage(mstrModuleName, 30, "Late Coming")

        Catch Ex As Exception
            Throw New Exception(Ex.Message & "; Procedure Name: SetMessages; Module Name: " & mstrModuleName)
        End Try
    End Sub
#End Region 'Language & UI Settings
	'</Language>
End Class
