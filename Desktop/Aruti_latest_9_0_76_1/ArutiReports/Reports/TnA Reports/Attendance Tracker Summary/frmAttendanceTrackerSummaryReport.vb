﻿#Region " Imports "

Imports Aruti.Data
Imports eZeeCommonLib
Imports Aruti
Imports ArutiReports

#End Region

Public Class frmAttendanceTrackerSummaryReport

#Region " Private Variables "

    Private mstrModuleName As String = "frmAttendanceTrackerSummaryReport"
    Private mdtStartDate As Date = Nothing
    Private mdtEndDate As Date = Nothing
    Private mstrAdvanceFilter As String = ""
    Private objEmpAttTrkSummary As clsAttendanceTrackerSummaryReport

#End Region

#Region " Constructor "

    Public Sub New()
        objEmpAttTrkSummary = New clsAttendanceTrackerSummaryReport(User._Object._Languageunkid, Company._Object._Companyunkid)
        objEmpAttTrkSummary.SetDefaultValue()
        InitializeComponent()
    End Sub

#End Region

#Region " Private Function "

    Private Sub FillCombo()
        Dim dsList As New DataSet
        Dim objEmployee As New clsEmployee_Master
        Dim objFromPeriod As New clscommom_period_Tran
        Dim objMaster As New clsMasterData
        Try
            dsList = objEmployee.GetEmployeeList(FinancialYear._Object._DatabaseName, _
                        User._Object._Userunkid, _
                        FinancialYear._Object._YearUnkid, _
                        Company._Object._Companyunkid, _
                        eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), _
                        eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), _
                        ConfigParameter._Object._UserAccessModeSetting, _
                        True, ConfigParameter._Object._IsIncludeInactiveEmp, "Employee", True)

            With cboEmployee
                .ValueMember = "employeeunkid"
                .DisplayMember = "employeename"
                .DataSource = dsList.Tables(0)
                .SelectedValue = 0
            End With

            dsList = objFromPeriod.getListForCombo(enModuleReference.Payroll, _
                                               FinancialYear._Object._YearUnkid, _
                                               FinancialYear._Object._DatabaseName, _
                                               FinancialYear._Object._Database_Start_Date, "From_Period", True)
            With cboPeriod
                .ValueMember = "periodunkid"
                .DisplayMember = "name"
                .DataSource = dsList.Tables(0)
                .SelectedValue = 0

            End With

            dsList = objMaster.GetEAllocation_Notification("List", "", False, False)
            Dim drRow As DataRow = dsList.Tables(0).NewRow
            drRow("Id") = 0
            drRow("Name") = Language.getMessage(mstrModuleName, 103, "Select")
            dsList.Tables(0).Rows.InsertAt(drRow, 0)
            With cboAllocation
                .ValueMember = "Id"
                .DisplayMember = "Name"
                .DataSource = dsList.Tables(0)
            End With

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "FillCombo", mstrModuleName)
        Finally
            objEmployee = Nothing : objFromPeriod = Nothing : dsList.Dispose() : objMaster = Nothing
        End Try
    End Sub

    Private Sub FillLeave()
        Dim objlvType As New clsleavetype_master
        Dim dsList As New DataSet
        Try
            dsList = objlvType.GetList("List", True, True, "ISNULL(IsExcuseLeave, 0) = 1 ")

            If dsList.Tables(0).Rows.Count > 0 Then
                Dim iTable As DataTable = dsList.Tables(0).DefaultView.ToTable(True, "leavetypeunkid", "leavename").Copy()
                Dim iCol As New DataColumn
                With iCol
                    .DataType = GetType(System.String)
                    .DefaultValue = ""
                    .ColumnName = "displayvalue"
                End With
                iTable.Columns.Add(iCol)

                For Each iRow As DataRow In iTable.Rows
                    iRow("displayvalue") = iRow("leavename")
                Next
                iTable.AcceptChanges()

                dgvLeave.AutoGenerateColumns = False

                dgcolhOrgLeave.DataPropertyName = "leavename"
                dgcolhCaption.DataPropertyName = "displayvalue"
                objdgcolhLvTypId.DataPropertyName = "leavetypeunkid"

                dgvLeave.DataSource = iTable

            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "FillLeave", mstrModuleName)
        End Try
    End Sub

    Private Function SetValue() As Boolean
        Try
            objEmpAttTrkSummary.SetDefaultValue()

            If CInt(cboPeriod.SelectedValue) <= 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 100, "Sorry, Period is mandatory information. Please select period to continue."), enMsgBoxStyle.Information)
                Return False
            End If

            If dgvLeave.RowCount <= 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 200, "Sorry, Excuse Leave is not defined. Please defined excuse leave in order to continue."), enMsgBoxStyle.Information)
                Return False
            End If

            objEmpAttTrkSummary._Advance_Filter = mstrAdvanceFilter
            objEmpAttTrkSummary._AllocationId = cboAllocation.SelectedValue
            objEmpAttTrkSummary._AllocationName = cboAllocation.Text
            objEmpAttTrkSummary._ConsiderZeroAsValue = chkConsiderZeroFilter.Checked
            objEmpAttTrkSummary._DaysFrom = nudDayFrom.Value
            objEmpAttTrkSummary._DaysTo = nudDayTo.Value
            objEmpAttTrkSummary._DefaultWorkingDays = nudDaysWorked.Value
            objEmpAttTrkSummary._EmployeeID = cboEmployee.SelectedValue
            objEmpAttTrkSummary._EmployeeName = cboEmployee.Text
            objEmpAttTrkSummary._EndDate = mdtEndDate
            Dim iCaptions As New Dictionary(Of Integer, String)
            iCaptions = CType(dgvLeave.DataSource, DataTable).AsEnumerable().ToDictionary(Function(x) x.Field(Of Integer)(objdgcolhLvTypId.DataPropertyName), Function(y) y.Field(Of String)(dgcolhCaption.DataPropertyName))
            objEmpAttTrkSummary._ExcuseCaption = iCaptions
            objEmpAttTrkSummary._PeriodId = cboPeriod.SelectedValue
            objEmpAttTrkSummary._PeriodName = cboPeriod.Text
            objEmpAttTrkSummary._ShowOnlyLateArrivalHours = chkShowLateArrivalhours.Checked
            objEmpAttTrkSummary._StartDate = mdtStartDate


            Return True

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "SetValue", mstrModuleName)
        End Try
    End Function

    Private Sub ResetValue()
        Try
            cboPeriod.SelectedValue = 0
            cboEmployee.SelectedValue = 0
            mdtStartDate = Nothing
            mdtEndDate = Nothing
            mstrAdvanceFilter = String.Empty
            nudDaysWorked.Value = 20
            nudDayFrom.Value = 0
            nudDayTo.Text = ""
            chkConsiderZeroFilter.Checked = False
            chkShowLateArrivalhours.Checked = False
            GetValue()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "ResetValue", mstrModuleName)
        End Try
    End Sub

    Private Sub GetValue()
        Dim dsValue As New DataSet
        Dim objUserDefRMode As New clsUserDef_ReportMode
        Try
            dsValue = objUserDefRMode.GetList("List", enArutiReport.Attendance_Tracker_Summary_Report)
            If dsValue.Tables.Count > 0 Then
                Dim ival() As DataRow = dsValue.Tables(0).Select("reporttypeid = -111")
                If ival IsNot Nothing AndAlso ival.Length > 0 Then
                    nudDaysWorked.Value = CInt(ival(0)("transactionheadid"))
                End If
            End If

            If dsValue.Tables.Count > 0 Then
                Dim ival() As DataRow = dsValue.Tables(0).Select("reporttypeid = -222")
                If ival IsNot Nothing AndAlso ival.Length > 0 Then
                    cboAllocation.SelectedValue = CInt(ival(0)("transactionheadid"))
                End If
            End If

            If dgvLeave.Rows.Count <= 0 Then Exit Sub
            If dsValue.Tables.Count > 0 Then
                If dsValue.Tables(0).Rows.Count > 0 Then
                    For Each sRow As DataRow In dsValue.Tables(0).Rows
                        Dim gRow As DataGridViewRow = dgvLeave.Rows.Cast(Of DataGridViewRow).AsEnumerable().Where(Function(x) CInt(x.Cells(objdgcolhLvTypId.Index).Value) = CInt(sRow("reporttypeid"))).Select(Function(x) x).FirstOrDefault()
                        If gRow IsNot Nothing Then dgvLeave.Rows(gRow.Index).Cells(dgcolhCaption.Index).Value = sRow("transactionheadid")
                    Next
                End If
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "GetValue", mstrModuleName)
        End Try
    End Sub

#End Region

#Region " Form's Events "

    Private Sub frmAttendanceTrackerSummaryReport_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            Call Language.setLanguage(Me.Name)
            OtherSettings()
            eZeeHeader.Title = objEmpAttTrkSummary._ReportName
            eZeeHeader.Message = objEmpAttTrkSummary._ReportDesc
            Call FillCombo()
            Call FillLeave()
            Call GetValue()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmAttendanceTrackerSummaryReport_Load", mstrModuleName)
        End Try

    End Sub

    Private Sub frmAttendanceTrackerSummaryReport_FormClosed(ByVal sender As Object, ByVal e As System.Windows.Forms.FormClosedEventArgs) Handles Me.FormClosed
        Try
            objEmpAttTrkSummary = Nothing
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmAttendanceTrackerSummaryReport_FormClosed", mstrModuleName)
        End Try
    End Sub

    Private Sub frmAttendanceTrackerSummaryReport_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles Me.KeyPress
        Try
            Select Case e.KeyChar
                Case CChar(ChrW(CInt(Windows.Forms.Keys.Enter)))
                    SendKeys.Send("{TAB}")
                    Exit Select
            End Select
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmAttendanceTrackerSummaryReport_KeyPress", mstrModuleName)
        End Try

    End Sub

    Private Sub objbtnLanguage_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnLanguage.Click
        Dim objfrm As New frmLanguage
        Try
            If User._Object._Isrighttoleft = True Then
                objfrm.RightToLeft = Windows.Forms.RightToLeft.Yes
                objfrm.RightToLeftLayout = True
                Call Language.ctlRightToLeftlayOut(objfrm)
            End If

            Call SetMessages()

            'clsEmpBudgetTimesheetSubmissionReport.SetMessages()
            'objfrm._Other_ModuleNames = "clsEmpBudgetTimesheetSubmissionReport"
            objfrm.displayDialog(Me)

            Call Language.setLanguage(Me.Name)
            Call SetLanguage()

        Catch ex As System.Exception
            Call DisplayError.Show(-1, ex.Message, "objbtnLanguage_Click", mstrModuleName)
        Finally
            objfrm.Dispose()
            objfrm = Nothing
        End Try
    End Sub

#End Region

#Region " Buttons "

    Private Sub objbtnAdvanceFilter_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnAdvanceFilter.Click
        Dim frm As New frmAdvanceSearch
        Try
            frm._Hr_EmployeeTable_Alias = "hremployee_master"
            frm.ShowDialog()
            mstrAdvanceFilter = frm._GetFilterString
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objbtnAdvanceFilter_Click", mstrModuleName)
        Finally
            If frm IsNot Nothing Then frm.Dispose()
        End Try
    End Sub

    Private Sub objbtnSearchEmployee_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles objbtnSearchEmployee.Click
        Try
            Dim objFrm As New frmCommonSearch
            objFrm.DataSource = CType(cboEmployee.DataSource, DataTable)
            objFrm.ValueMember = cboEmployee.ValueMember
            objFrm.DisplayMember = cboEmployee.DisplayMember
            objFrm.CodeMember = "employeeCode"

            If objFrm.DisplayDialog() Then
                cboEmployee.SelectedValue = objFrm.SelectedValue
            End If
            cboEmployee.Focus()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objbtnSearchEmployee_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub btnReset_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnReset.Click
        Try
            ResetValue()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "btnReset_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub btnExport_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnExport.Click
        Try
            If SetValue() = False Then Exit Sub
            objEmpAttTrkSummary.Generate_DetailReport(FinancialYear._Object._DatabaseName, _
                                                      User._Object._Userunkid, _
                                                      FinancialYear._Object._YearUnkid, _
                                                      Company._Object._Companyunkid, _
                                                      mdtStartDate, _
                                                      mdtEndDate, _
                                                      ConfigParameter._Object._UserAccessModeSetting, True, _
                                                      ConfigParameter._Object._ExportReportPath, _
                                                      ConfigParameter._Object._OpenAfterExport, _
                                                      0, enPrintAction.None, enExportAction.ExcelExtra)
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "btnExport_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub btnClose_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnClose.Click
        Try
            Me.Close()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "btnClose_Click", mstrModuleName)
        End Try
    End Sub

#End Region

#Region " Link Button "

    Private Sub btnSaveSelection_Click(ByVal sender As System.Object, ByVal e As System.Windows.Forms.LinkLabelLinkClickedEventArgs) Handles lnkSaveSelection.LinkClicked
        Dim objUserDefRMode As New clsUserDef_ReportMode
        Dim intUnkid As Integer = -1
        Dim mblnFlag As Boolean = False
        Try
            objUserDefRMode._Reportunkid = enArutiReport.Attendance_Tracker_Summary_Report
            objUserDefRMode._Reporttypeid = -111
            objUserDefRMode._Reportmodeid = 0
            objUserDefRMode._Headtypeid = 1
            objUserDefRMode._EarningTranHeadIds = nudDaysWorked.Value.ToString()
            intUnkid = objUserDefRMode.isExist(enArutiReport.Attendance_Tracker_Summary_Report, objUserDefRMode._Reporttypeid, 0, objUserDefRMode._Headtypeid)
            objUserDefRMode._Reportmodeunkid = intUnkid

            If intUnkid <= 0 Then
                mblnFlag = objUserDefRMode.Insert()
            Else
                mblnFlag = objUserDefRMode.Update()
            End If

            objUserDefRMode._Reportunkid = enArutiReport.Attendance_Tracker_Summary_Report
            objUserDefRMode._Reporttypeid = -222
            objUserDefRMode._Reportmodeid = 0
            objUserDefRMode._Headtypeid = 1
            objUserDefRMode._EarningTranHeadIds = CInt(cboAllocation.SelectedValue)
            intUnkid = objUserDefRMode.isExist(enArutiReport.Attendance_Tracker_Summary_Report, objUserDefRMode._Reporttypeid, 0, objUserDefRMode._Headtypeid)
            objUserDefRMode._Reportmodeunkid = intUnkid

            If intUnkid <= 0 Then
                mblnFlag = objUserDefRMode.Insert()
            Else
                mblnFlag = objUserDefRMode.Update()
            End If

            For Each gRow As DataGridViewRow In dgvLeave.Rows
                objUserDefRMode._Reportunkid = enArutiReport.Attendance_Tracker_Summary_Report
                objUserDefRMode._Reporttypeid = CInt(gRow.Cells(objdgcolhLvTypId.Index).Value)
                objUserDefRMode._Reportmodeid = 0
                objUserDefRMode._Headtypeid = 1
                objUserDefRMode._EarningTranHeadIds = gRow.Cells(dgcolhCaption.Index).Value.ToString()

                intUnkid = objUserDefRMode.isExist(enArutiReport.Attendance_Tracker_Summary_Report, objUserDefRMode._Reporttypeid, 0, objUserDefRMode._Headtypeid)
                objUserDefRMode._Reportmodeunkid = intUnkid

                If intUnkid <= 0 Then
                    mblnFlag = objUserDefRMode.Insert()
                Else
                    mblnFlag = objUserDefRMode.Update()
                End If
            Next

            If mblnFlag Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 4, "Selection Saved Successfully."), enMsgBoxStyle.Information)
            Else
                eZeeMsgBox.Show(objUserDefRMode._Message)
                Exit Sub
            End If

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "lnkSave_LinkClicked", mstrModuleName)
        Finally
            objUserDefRMode = Nothing
        End Try
    End Sub

#End Region

#Region " Combobox Event(s) "

    Private Sub cboPeriod_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cboPeriod.SelectedIndexChanged
        Try
            If CInt(cboPeriod.SelectedValue) > 0 Then
                Dim objPeriod As New clscommom_period_Tran
                objPeriod._Periodunkid(FinancialYear._Object._DatabaseName) = CInt(cboPeriod.SelectedValue)
                mdtStartDate = objPeriod._TnA_StartDate.Date
                mdtEndDate = objPeriod._TnA_EndDate.Date
                objlblPDate.Text = Language.getMessage(mstrModuleName, 101, "Date Range :") & " " & mdtStartDate.ToShortDateString() & " - " & mdtEndDate.ToShortDateString()
                nudDaysWorked.Maximum = objPeriod._Constant_Days
                nudDayFrom.Maximum = objPeriod._Constant_Days
                nudDayTo.Maximum = objPeriod._Constant_Days
            Else
                mdtStartDate = Nothing
                mdtEndDate = Nothing
                objlblPDate.Text = ""
                nudDaysWorked.Maximum = 31
                nudDayFrom.Maximum = 31
                nudDayTo.Maximum = 31
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "cboPeriod_SelectedIndexChanged", mstrModuleName)
        End Try
    End Sub

#End Region

    '<Language> This Auto Generated Text Please Do Not Modify it.
#Region " Language & UI Settings "
    Private Sub OtherSettings()
        Try
            Me.SuspendLayout()

            Call SetLanguage()

            Me.gbFilterCriteria.GradientColor = GUI._eZeeContainerHeaderBackColor
            Me.gbFilterCriteria.ForeColor = GUI._eZeeContainerHeaderForeColor

            Me.gbOtherSetting.GradientColor = GUI._eZeeContainerHeaderBackColor
            Me.gbOtherSetting.ForeColor = GUI._eZeeContainerHeaderForeColor

            Me.eZeeHeader.GradientColor1 = GUI._HeaderBackColor1
            Me.eZeeHeader.GradientColor2 = GUI._HeaderBackColor2
            Me.eZeeHeader.BorderColor = GUI._HeaderBorderColor
            Me.eZeeHeader.HeaderTextForeColor = GUI._HeaderTitleFontColor
            Me.eZeeHeader.DescriptionForeColor = GUI._HeaderMessageFontColor


            Me.objbtnAdvanceFilter.GradientBackColor = GUI._ButttonBackColor
            Me.objbtnAdvanceFilter.GradientForeColor = GUI._ButttonFontColor

            Me.objbtnLanguage.GradientBackColor = GUI._ButttonBackColor
            Me.objbtnLanguage.GradientForeColor = GUI._ButttonFontColor

            Me.btnReset.GradientBackColor = GUI._ButttonBackColor
            Me.btnReset.GradientForeColor = GUI._ButttonFontColor

            Me.btnExport.GradientBackColor = GUI._ButttonBackColor
            Me.btnExport.GradientForeColor = GUI._ButttonFontColor

            Me.btnClose.GradientBackColor = GUI._ButttonBackColor
            Me.btnClose.GradientForeColor = GUI._ButttonFontColor


            Me.ResumeLayout()
        Catch Ex As Exception
            DisplayError.Show("-1", Ex.Message, "OtherSettings", mstrModuleName)
        End Try
    End Sub


    Private Sub SetLanguage()
        Try
            Me.Text = Language._Object.getCaption(Me.Name, Me.Text)

            Me.eZeeHeader.Title = Language._Object.getCaption(Me.eZeeHeader.Name & "_Title", Me.eZeeHeader.Title)
            Me.eZeeHeader.Message = Language._Object.getCaption(Me.eZeeHeader.Name & "_Message", Me.eZeeHeader.Message)
            Me.btnReset.Text = Language._Object.getCaption(Me.btnReset.Name, Me.btnReset.Text)
            Me.btnExport.Text = Language._Object.getCaption(Me.btnExport.Name, Me.btnExport.Text)
            Me.btnClose.Text = Language._Object.getCaption(Me.btnClose.Name, Me.btnClose.Text)
            Me.gbFilterCriteria.Text = Language._Object.getCaption(Me.gbFilterCriteria.Name, Me.gbFilterCriteria.Text)
            Me.lblPeriod.Text = Language._Object.getCaption(Me.lblPeriod.Name, Me.lblPeriod.Text)
            Me.lblEmployee.Text = Language._Object.getCaption(Me.lblEmployee.Name, Me.lblEmployee.Text)
            Me.chkShowLateArrivalhours.Text = Language._Object.getCaption(Me.chkShowLateArrivalhours.Name, Me.chkShowLateArrivalhours.Text)
            Me.lblDayFrom.Text = Language._Object.getCaption(Me.lblDayFrom.Name, Me.lblDayFrom.Text)
            Me.lblYTo.Text = Language._Object.getCaption(Me.lblYTo.Name, Me.lblYTo.Text)
            Me.elActDaysNoOfWorked.Text = Language._Object.getCaption(Me.elActDaysNoOfWorked.Name, Me.elActDaysNoOfWorked.Text)
            Me.lblExpetedDays.Text = Language._Object.getCaption(Me.lblExpetedDays.Name, Me.lblExpetedDays.Text)
            Me.gbOtherSetting.Text = Language._Object.getCaption(Me.gbOtherSetting.Name, Me.gbOtherSetting.Text)
            Me.lnkSaveSelection.Text = Language._Object.getCaption(Me.lnkSaveSelection.Name, Me.lnkSaveSelection.Text)
            Me.dgcolhOrgLeave.HeaderText = Language._Object.getCaption(Me.dgcolhOrgLeave.Name, Me.dgcolhOrgLeave.HeaderText)
            Me.dgcolhCaption.HeaderText = Language._Object.getCaption(Me.dgcolhCaption.Name, Me.dgcolhCaption.HeaderText)
            Me.chkConsiderZeroFilter.Text = Language._Object.getCaption(Me.chkConsiderZeroFilter.Name, Me.chkConsiderZeroFilter.Text)
            Me.lblAllocation.Text = Language._Object.getCaption(Me.lblAllocation.Name, Me.lblAllocation.Text)

        Catch Ex As Exception
            DisplayError.Show("-1", Ex.Message, "SetLanguage", mstrModuleName)
        End Try
    End Sub


    Private Sub SetMessages()
        Try
            Language.setMessage(mstrModuleName, 4, "Selection Saved Successfully.")
            Language.setMessage(mstrModuleName, 100, "Sorry, Period is mandatory information. Please select period to continue.")
            Language.setMessage(mstrModuleName, 101, "Date Range :")
            Language.setMessage(mstrModuleName, 103, "Select")
            Language.setMessage(mstrModuleName, 200, "Sorry, Excuse Leave is not defined. Please defined excuse leave in order to continue.")

        Catch Ex As Exception
            DisplayError.Show("-1", Ex.Message, "SetMessages", mstrModuleName)
        End Try
    End Sub
#End Region 'Language & UI Settings
    '</Language>
End Class