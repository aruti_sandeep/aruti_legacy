﻿'************************************************************************************************************************************
'Class Name : clsAttendanceTrackerSummaryReport.vb
'Purpose    :
'Date       : 07-Apr-2022
'Written By : Sandeep
'Modified   :
'************************************************************************************************************************************
#Region " Imports "

Imports eZeeCommonLib
Imports Aruti.Data
Imports ExcelWriter
Imports System
Imports System.Text
Imports System.IO

#End Region

Public Class clsAttendanceTrackerSummaryReport
    Inherits IReportData
    Private Shared ReadOnly mstrModuleName As String = "clsAttendanceTrackerSummaryReport"
    Private mstrReportId As String = enArutiReport.Attendance_Tracker_Summary_Report  '292
    Dim objDataOperation As clsDataOperation

#Region " Constructor "
    Public Sub New(ByVal intLangId As Integer, ByVal intCompanyId As Integer)
        Me.setReportData(CInt(mstrReportId), intLangId, intCompanyId)
        Call Create_OnDetailReport()
    End Sub
#End Region

#Region " Private variables "

    Private mintPeriodID As Integer = 0
    Private mstrPeriodName As String = ""
    Private mdtStartDate As Date = Nothing
    Private mdtEndDate As Date = Nothing
    Private mintEmployeeId As Integer = 0
    Private mstrEmployeeName As String = ""
    Private mintUserUnkid As Integer = -1
    Private mstrAdvance_Filter As String = String.Empty
    Private mblnShowOnlyLateArrivalHours As Boolean = False
    Private mintDaysFrom As Integer = -1
    Private mintDaysTo As Integer = -1
    Private mblnConsiderZeroAsValue As Boolean = False
    Private mdicExcuseCaption As Dictionary(Of Integer, String) = Nothing
    Private mintDefaultWorkingDays As Integer = 0
    Private mintAllocationId As Integer = 0
    Private mstrAllocationName As String = ""
    Private mblnIncludeAccessFilterQry As Boolean = True
    Private mDicExcuseLeaveSecs As Dictionary(Of String, String)

#End Region

#Region " Properties "

    Public WriteOnly Property _PeriodId() As Integer
        Set(ByVal value As Integer)
            mintPeriodID = value
        End Set
    End Property

    Public WriteOnly Property _PeriodName() As String
        Set(ByVal value As String)
            mstrPeriodName = value
        End Set
    End Property

    Public WriteOnly Property _StartDate() As Date
        Set(ByVal value As Date)
            mdtStartDate = value
        End Set
    End Property

    Public WriteOnly Property _EndDate() As Date
        Set(ByVal value As Date)
            mdtEndDate = value
        End Set
    End Property

    Public WriteOnly Property _EmployeeID() As Integer
        Set(ByVal value As Integer)
            mintEmployeeId = value
        End Set
    End Property

    Public WriteOnly Property _EmployeeName() As String
        Set(ByVal value As String)
            mstrEmployeeName = value
        End Set
    End Property

    Public WriteOnly Property _UserUnkId() As Integer
        Set(ByVal value As Integer)
            mintUserUnkid = value
        End Set
    End Property

    Public WriteOnly Property _Advance_Filter() As String
        Set(ByVal value As String)
            mstrAdvance_Filter = value
        End Set
    End Property

    Public WriteOnly Property _ShowOnlyLateArrivalHours() As Boolean
        Set(ByVal value As Boolean)
            mblnShowOnlyLateArrivalHours = value
        End Set
    End Property

    Public WriteOnly Property _DaysFrom() As Integer
        Set(ByVal value As Integer)
            mintDaysFrom = value
        End Set
    End Property

    Public WriteOnly Property _DaysTo() As Integer
        Set(ByVal value As Integer)
            mintDaysTo = value
        End Set
    End Property

    Public WriteOnly Property _ConsiderZeroAsValue() As Boolean
        Set(ByVal value As Boolean)
            mblnConsiderZeroAsValue = value
        End Set
    End Property

    Public WriteOnly Property _ExcuseCaption() As Dictionary(Of Integer, String)
        Set(ByVal value As Dictionary(Of Integer, String))
            mdicExcuseCaption = value
        End Set
    End Property

    Public WriteOnly Property _DefaultWorkingDays() As Integer
        Set(ByVal value As Integer)
            mintDefaultWorkingDays = value
        End Set
    End Property

    Public WriteOnly Property _AllocationId() As Integer
        Set(ByVal value As Integer)
            mintAllocationId = value
        End Set
    End Property

    Public WriteOnly Property _AllocationName() As String
        Set(ByVal value As String)
            mstrAllocationName = value
        End Set
    End Property

    Public Property _IncludeAccessFilterQry() As Boolean
        Get
            Return mblnIncludeAccessFilterQry
        End Get
        Set(ByVal value As Boolean)
            mblnIncludeAccessFilterQry = value
        End Set
    End Property

#End Region

#Region " Public Function & Procedures "

    Public Sub SetDefaultValue()
        Try
            mintPeriodID = 0
            mstrPeriodName = ""
            mdtStartDate = Nothing
            mdtEndDate = Nothing
            mintEmployeeId = 0
            mstrEmployeeName = ""
            mintUserUnkid = -1
            mstrAdvance_Filter = String.Empty
            mblnShowOnlyLateArrivalHours = False
            mintDaysFrom = -1
            mintDaysTo = -1
            mblnConsiderZeroAsValue = False
            mdicExcuseCaption = Nothing
            mintDefaultWorkingDays = 0
            mintAllocationId = 0
            mstrAllocationName = ""
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: SetDefaultValue; Module Name: " & mstrModuleName)
        End Try
    End Sub

    Public Sub FilterTitleAndFilterQuery()
        Me._FilterQuery = ""
        Me._FilterTitle = ""

        Try
            objDataOperation.AddParameter("@Days", SqlDbType.Int, eZeeDataType.INT_SIZE, mintDefaultWorkingDays)
            objDataOperation.AddParameter("@start_date", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, eZeeDate.convertDate(mdtStartDate))
            objDataOperation.AddParameter("@end_date", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, eZeeDate.convertDate(mdtEndDate))



            If mintPeriodID > 0 Then
                Me._FilterTitle &= Language.getMessage(mstrModuleName, 200, "Period: ") & " " & mstrPeriodName & " "
            End If

            If mintEmployeeId > 0 Then
                objDataOperation.AddParameter("@employeeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintEmployeeId)
                Me._FilterTitle &= Language.getMessage(mstrModuleName, 201, "Employee: ") & " " & mstrEmployeeName & " "
            End If

            If mblnShowOnlyLateArrivalHours Then
                Me._FilterQuery &= " AND ISNULL(LT.eLt,0) > 0 "
                Me._FilterTitle &= Language.getMessage(mstrModuleName, 202, "Only Late Arrival")
            End If

            If mblnConsiderZeroAsValue Then
                If mintDaysFrom <> -1 AndAlso mintDaysTo <> -1 Then
                    Me._FilterTitle &= Language.getMessage(mstrModuleName, 203, "Days From : ") & " " & mintDaysFrom.ToString() & " " & Language.getMessage(mstrModuleName, 204, "Days To : ") & " " & mintDaysTo.ToString()
                End If
            End If

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: FilterTitleAndFilterQuery; Module Name: " & mstrModuleName)
        End Try
    End Sub

    Public Overrides Sub generateReport(ByVal pintReportType As Integer, Optional ByVal PrintAction As Aruti.Data.enPrintAction = Aruti.Data.enPrintAction.Preview, Optional ByVal ExportAction As Aruti.Data.enExportAction = Aruti.Data.enExportAction.None)

    End Sub

    Public Overrides Sub generateReportNew(ByVal xDatabaseName As String, ByVal xUserUnkid As Integer, ByVal xYearUnkid As Integer _
                                                              , ByVal xCompanyUnkid As Integer, ByVal xPeriodStart As Date, ByVal xPeriodEnd As Date, ByVal xUserModeSetting As String _
                                                              , ByVal xOnlyApproved As Boolean, ByVal xExportReportPath As String, ByVal xOpenReportAfterExport As Boolean _
                                                              , ByVal pintReportType As Integer, Optional ByVal PrintAction As Aruti.Data.enPrintAction = Aruti.Data.enPrintAction.Preview _
                                                              , Optional ByVal ExportAction As Aruti.Data.enExportAction = Aruti.Data.enExportAction.None _
                                                              , Optional ByVal intBaseCurrencyUnkid As Integer = 0)
        Dim objRpt As CrystalDecisions.CrystalReports.Engine.ReportClass = Nothing
        Dim strReportExportFile As String = ""
        Try
            If Not IsNothing(objRpt) Then
                Call ReportExecute(objRpt, PrintAction, ExportAction, xExportReportPath, xOpenReportAfterExport)
            End If
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: generateReport; Module Name: " & mstrModuleName)
        End Try
    End Sub

    Public Overrides Sub setDefaultOrderBy(ByVal intReportType As Integer)

    End Sub

    Public Overrides Sub setOrderBy(ByVal intReportType As Integer)

    End Sub

    'S.SANDEEP |13-MAY-2022| -- START
    'ISSUE/ENHANCEMENT : AC2-335
    Private Sub Legends()
        Try
            Language.getMessage(mstrModuleName, 300, "KEY")
            Language.getMessage(mstrModuleName, 401, "Short of working hours")
            Language.getMessage(mstrModuleName, 302, "Arrival after core hours")
            Language.getMessage(mstrModuleName, 403, "Not Captured")
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Legends: generateReport; Module Name: " & mstrModuleName)
        End Try
    End Sub
    'S.SANDEEP |13-MAY-2022| -- END

#End Region

#Region " Report Generation "

    Dim iColumn_DetailReport As New IColumnCollection

    Public Property Field_OnDetailReport() As IColumnCollection
        Get
            Return iColumn_DetailReport
        End Get
        Set(ByVal value As IColumnCollection)
            iColumn_DetailReport = value
        End Set
    End Property

    Private Sub Create_OnDetailReport()
        Try
            iColumn_DetailReport.Clear()
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: Create_OnDetailReport; Module Name: " & mstrModuleName)
        End Try
    End Sub

    Private Function PerformSrNo(ByVal dr As DataRow) As Boolean
        Try
            dr("srn") = dr.Table.Rows.IndexOf(dr) + 1
            Return True
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: PerformComputation; Module Name: " & mstrModuleName)
        End Try
    End Function

    Private Function PerformComputation(ByVal dr As DataRow, ByVal iExLv As List(Of DataRow)) As Boolean
        Try
            Dim xTotal As Integer = 0
            If iExLv IsNot Nothing AndAlso iExLv.Count > 0 Then
                For Each lv As DataRow In iExLv
                    xTotal = xTotal + CInt(dr("lv_" & lv("leavetypeunkid")))
                Next
            End If
            dr("eActDaysWk") = CInt(dr("eActDaysWk")) + xTotal + CInt(dr("eOfficeDays"))
            'S.SANDEEP |13-MAY-2022| -- START
            'ISSUE/ENHANCEMENT : AC2-335
            'dr("eAcWkh") = dr("eActDaysWk") * dr("hrs")
            If CInt(dr("eOfficeDays")) <= 0 Then
                dr("iColrTag") = "R"
            ElseIf CInt(dr("eLtArrv")) > 0 Then
                dr("iColrTag") = "G"
            ElseIf CInt(dr("eShDays")) > 0 Then
                dr("iColrTag") = "Y"
            Else
                dr("iColrTag") = "S"
            End If
            'S.SANDEEP |13-MAY-2022| -- END

            'dr("eVarPct") = CDec(IIf(dr("eAcWkh") <= 0, 0, (dr("eExpWkh") / dr("eAcWkh")) * 100))
            Dim dblTotAcWkh As Double = 0
            For Each iCol As String In mDicExcuseLeaveSecs.Keys
                dblTotAcWkh += dr(mDicExcuseLeaveSecs(iCol))
            Next
            dblTotAcWkh += dr("eAcWks")
            dr("eAcWkh") = CDbl(CInt(((dblTotAcWkh) / 3600)).ToString() & "." & Math.Abs(CInt(((dblTotAcWkh) Mod 3600) / 60)).ToString())
            dr("eVarPct") = CDec(IIf(CDec(dr("eAcWkh")) <= 0, 0, (CDec(dr("eAcWkh")) / dr("eExpWkh")) * 100))
            'dr("eVarWkh") = dr("eExpWkh") - dr("eAcWkh")
            'dr("eVarWkh") = CDbl(CInt(((dr("eAcWks") - dr("eExpWks")) / 3600)).ToString() & "." & Math.Abs(CInt(((dr("eAcWks") - dr("eExpWks")) Mod 3600) / 60)).ToString())
            dr("eVarWkh") = CDbl(CInt(((dblTotAcWkh - dr("eExpWks")) / 3600)).ToString() & "." & Math.Abs(CInt(((dblTotAcWkh - dr("eExpWks")) Mod 3600) / 60)).ToString())
            'dr("eShDays") = IIf(dr("hrs") <= 0, 0, CInt(Math.Ceiling(Math.Abs(dr("eVarWkh")) / dr("hrs"))))
            dr("eShDays") = Rounding.BRound(IIf(dr("hrs") <= 0, 0, CDbl((Math.Abs(dr("eVarWkh")) / dr("hrs")))), 10.0).ToString("###0.0")

            Return True
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: PerformComputation; Module Name: " & mstrModuleName)
        End Try
    End Function

    Private Function PerformValueSetting(ByVal dr As DataRow, ByVal ds As DataSet) As Boolean
        Try            
            Dim tm As DataRow() = ds.Tables(0).Select("employeeunkid = '" & dr("employeeunkid").ToString() & "'")
            If tm IsNot Nothing AndAlso tm.Length > 0 Then
                For Each t As DataRow In tm
                    dr("lv_" & t("leavetypeunkid")) = t("lvcnt")
                    If CBool(t("isexcuseleave")) = True Then
                        dr("lv_sc_" & t("leavetypeunkid")) = (t("lvcnt") * dr("hrs")) * 3600
                    End If
                Next
            End If
            Return True
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: PerformComputation; Module Name: " & mstrModuleName)
        End Try
    End Function

    Public Sub Generate_DetailReport(ByVal xDatabaseName As String, ByVal xUserUnkid As Integer, ByVal xYearUnkid As Integer _
                                    , ByVal xCompanyUnkid As Integer, ByVal xPeriodStart As Date, ByVal xPeriodEnd As Date, ByVal xUserModeSetting As String _
                                    , ByVal xOnlyApproved As Boolean, ByVal xExportReportPath As String, ByVal xOpenReportAfterExport As Boolean _
                                    , ByVal pintReportType As Integer, Optional ByVal PrintAction As Aruti.Data.enPrintAction = Aruti.Data.enPrintAction.Preview _
                                    , Optional ByVal ExportAction As Aruti.Data.enExportAction = Aruti.Data.enExportAction.None _
                                    , Optional ByVal intBaseCurrencyUnkid As Integer = 0)

        Dim StrQ As String = ""
        Dim StrQFilter As String = ""
        Dim dsList As New DataSet
        Try
            mDicExcuseLeaveSecs = New Dictionary(Of String, String)

            If xUserUnkid <= 0 Then
                xUserUnkid = User._Object._Userunkid
            End If

            User._Object._Userunkid = xUserUnkid

            Dim xDateJoinQry, xDateFilterQry, xUACQry, xUACFiltrQry, xAdvanceJoinQry As String
            xDateJoinQry = "" : xDateFilterQry = "" : xUACQry = "" : xUACFiltrQry = "" : xAdvanceJoinQry = ""
            Call GetDatesFilterString(xDateJoinQry, xDateFilterQry, xPeriodEnd, xPeriodEnd, True, False, xDatabaseName)
            If mblnIncludeAccessFilterQry Then Call NewAccessLevelFilterString(xUACQry, xUACFiltrQry, xPeriodEnd, xOnlyApproved, xDatabaseName, xUserUnkid, xCompanyUnkid, xYearUnkid, xUserModeSetting)
            Call GetAdvanceFilterQry(xAdvanceJoinQry, xPeriodEnd, xDatabaseName)

            objDataOperation = New clsDataOperation
            objDataOperation.ClearParameters()

            Dim iNrLv As List(Of DataRow) = Nothing : Dim iExLv As List(Of DataRow) = Nothing
            Dim objLvType As New clsleavetype_master
            Dim dsLv As New DataSet
            dsLv = objLvType.GetList("List", True, True)

            If dsLv.Tables.Count > 0 AndAlso dsLv.Tables(0).Rows.Count > 0 Then
                If dsLv.Tables(0).AsEnumerable().Where(Function(x) x.Field(Of Boolean)("isexcuseleave") = False).Count > 0 Then
                    iNrLv = dsLv.Tables(0).AsEnumerable().Where(Function(x) x.Field(Of Boolean)("isexcuseleave") = False).Select(Function(x) x).ToList()
                End If

                If dsLv.Tables(0).AsEnumerable().Where(Function(x) x.Field(Of Boolean)("isexcuseleave") = True).Count > 0 Then
                    iExLv = dsLv.Tables(0).AsEnumerable().Where(Function(x) x.Field(Of Boolean)("isexcuseleave") = True).Select(Function(x) x).ToList()
                End If
            End If

            StrQ = "IF OBJECT_ID('tempdb..#Emplst') IS NOT NULL DROP TABLE #Emplst " & _
                   "SELECT " & _
                   "     hremployee_master.firstname + ' ' + hremployee_master.surname AS eName " & _
                   "    ,hremployee_master.employeecode AS eCode " & _
                   "    ,hremployee_master.employeeunkid " & _
                   "INTO #Emplst " & _
                   "FROM hremployee_master "
            If mstrAdvance_Filter.Trim.Length > 0 Then
                If xAdvanceJoinQry.Trim.Length > 0 Then
                    StrQ &= xAdvanceJoinQry
                End If
            End If

            If xDateJoinQry.Trim.Length > 0 Then
                StrQ &= xDateJoinQry
            End If

            If xUACQry.Trim.Length > 0 Then
                StrQ &= xUACQry
            End If

            StrQ &= " WHERE 1 = 1 "

            If mintEmployeeId > 0 Then
                StrQ &= " AND hremployee_master.employeeunkid = @employeeunkid "
            End If

            If xDateFilterQry.Trim.Length > 0 Then
                StrQ &= xDateFilterQry & " "
            End If

            If xUACFiltrQry.Trim.Length > 0 Then
                StrQ &= " AND " & xUACFiltrQry
            End If

            If mstrAdvance_Filter.Trim.Length > 0 Then
                StrQ &= " AND " & mstrAdvance_Filter
            End If

            StrQ &= "; SELECT " & _
                    "     0 AS srn " & _
                    "    ,#Emplst.eName " & _
                    "    ,#Emplst.eCode " & _
                    "    ,#Emplst.employeeunkid " & _
                    "    ,@Days AS eDays " & _
                    "    ,0 AS eActDaysWk "
            If iNrLv IsNot Nothing AndAlso iNrLv.Count > 0 Then
                For Each lv As DataRow In iNrLv
                    StrQ &= ", 0 AS lv_" & lv("leavetypeunkid").ToString
                Next
            End If
            If iExLv IsNot Nothing AndAlso iExLv.Count > 0 Then
                For Each lv As DataRow In iExLv
                    StrQ &= ", 0 AS lv_" & lv("leavetypeunkid").ToString & " " & _
                            ", 0 AS lv_sc_" & lv("leavetypeunkid").ToString
                    If mDicExcuseLeaveSecs.ContainsKey("lv_sc_" & lv("leavetypeunkid").ToString) = False Then _
                    mDicExcuseLeaveSecs.Add("lv_sc_" & lv("leavetypeunkid").ToString, "lv_sc_" & lv("leavetypeunkid").ToString)
                Next
            End If

            'S.SANDEEP |13-MAY-2022| -- START
            'ISSUE/ENHANCEMENT : AC2-335
            'StrQ &= "    ,ISNULL(PD.eP,0) AS eOfficeDays " & _
            '        "    ,(@Days * ISNULL(eSF.hrs,0)) AS eExpWkh " & _
            '        "    ,0 AS eAcWkh " & _
            '        "    ,0 AS eVarPct " & _
            '        "    ,0 AS eVarWkh " & _
            '        "    ,0 AS eShDays " & _
            '        "    ,ISNULL(eSF.chTime,'') eChTime " & _
            '        "    ,ISNULL(LT.eLt,0) eLtArrv " & _
            '        "    ,ISNULL(eSF.hrs,0) AS hrs "
            StrQ &= "    ,ISNULL(PD.eP,0) AS eOfficeDays " & _
                    "    ,(@Days * ISNULL(eSF.hrs,0)) AS eExpWkh " & _
                    "    ,ISNULL(PD.eAcWkh,0) AS eAcWkh " & _
                    "    ,CAST(0 AS DECIMAL(10,2)) AS eVarPct " & _
                    "    ,CAST(0 AS DECIMAL(10,2)) AS eVarWkh " & _
                    "    ,CAST(0 AS NVARCHAR(MAX)) AS eShDays " & _
                    "    ,ISNULL(eSF.chTime,'') eChTime " & _
                    "    ,ISNULL(LT.eLt,0) eLtArrv " & _
                    "    ,ISNULL(eSF.hrs,0) AS hrs " & _
                    "    ,ISNULL(PD.eAcWks,0) AS eAcWks " & _
                    "    ,((@Days * ISNULL(eSF.hrs,0)) * 3600) AS eExpWks "
                    
            'S.SANDEEP |13-MAY-2022| -- END

            Select Case mintAllocationId
                Case enAllocation.BRANCH
                    StrQ &= ", ISNULL(hrstation_master.name, '') AS allocation "
                Case enAllocation.DEPARTMENT_GROUP
                    StrQ &= ", ISNULL(hrdepartment_group_master.name, '') AS allocation "
                Case enAllocation.DEPARTMENT
                    StrQ &= ", ISNULL(dept.name, '') AS allocation "
                Case enAllocation.SECTION_GROUP
                    StrQ &= ", ISNULL(hrsectiongroup_master.name, '') AS allocation "
                Case enAllocation.SECTION
                    StrQ &= ", ISNULL(hrsection_master.name, '') AS allocation "
                Case enAllocation.UNIT_GROUP
                    StrQ &= ", ISNULL(hrunitgroup_master.name, '') AS allocation "
                Case enAllocation.UNIT
                    StrQ &= ", ISNULL(hrunit_master.name, '') AS allocation "
                Case enAllocation.TEAM
                    StrQ &= ", ISNULL(hrteam_master.name, '') AS allocation "
                Case enAllocation.JOB_GROUP
                    StrQ &= ", ISNULL(jg.name, '') AS allocation "
                Case enAllocation.JOBS
                    StrQ &= ", ISNULL(jb.job_name, '') AS allocation "
                Case enAllocation.CLASS_GROUP
                    StrQ &= ", ISNULL(hrclassgroup_master.name, '') AS allocation "
                Case enAllocation.CLASSES
                    StrQ &= ", ISNULL(hrclasses_master.name, '') AS allocation "
                Case enAllocation.COST_CENTER
                    StrQ &= ", ISNULL(prcostcenter_master.costcentername, '') AS allocation "
            End Select

            StrQ &= " ,'' AS iColrTag " & _
                    " FROM #Emplst " & _
                    " LEFT JOIN " & _
                    "   ( " & _
                    "       SELECT " & _
                    "         stationunkid " & _
                    "        ,deptgroupunkid " & _
                    "        ,departmentunkid " & _
                    "        ,sectiongroupunkid " & _
                    "        ,sectionunkid " & _
                    "        ,unitgroupunkid " & _
                    "        ,unitunkid " & _
                    "        ,teamunkid " & _
                    "        ,classgroupunkid " & _
                    "        ,classunkid " & _
                    "        ,employeeunkid " & _
                    "        ,ROW_NUMBER()OVER(PARTITION BY employeeunkid ORDER BY effectivedate DESC) AS rno " & _
                    "       FROM hremployee_transfer_tran " & _
                    "       WHERE isvoid = 0 AND CONVERT(CHAR(8),effectivedate,112) <= @end_date "
            If mintEmployeeId > 0 Then
                StrQ &= " AND hremployee_transfer_tran.employeeunkid = @employeeunkid "
            End If
            StrQ &= "   ) AS Alloc ON Alloc.employeeunkid = #Emplst.employeeunkid AND Alloc.rno = 1 "


            Select Case mintAllocationId
                Case enAllocation.BRANCH
                    StrQ &= " LEFT JOIN hrstation_master ON hrstation_master.stationunkid = Alloc.stationunkid "
                Case enAllocation.DEPARTMENT_GROUP
                    StrQ &= " LEFT JOIN hrdepartment_group_master ON hrdepartment_group_master.deptgroupunkid = Alloc.deptgroupunkid "
                Case enAllocation.DEPARTMENT
                    StrQ &= " LEFT JOIN hrdepartment_master dept ON dept.departmentunkid = Alloc.departmentunkid  "
                Case enAllocation.SECTION_GROUP
                    StrQ &= " LEFT JOIN hrsectiongroup_master ON hrsectiongroup_master.sectiongroupunkid = Alloc.sectiongroupunkid "
                Case enAllocation.SECTION
                    StrQ &= " LEFT JOIN hrsection_master ON hrsection_master.sectionunkid = Alloc.sectionunkid "
                Case enAllocation.UNIT_GROUP
                    StrQ &= " LEFT JOIN hrunitgroup_master ON hrunitgroup_master.unitgroupunkid = Alloc.unitgroupunkid "
                Case enAllocation.UNIT
                    StrQ &= " LEFT JOIN hrunit_master ON hrunit_master.unitunkid = Alloc.unitunkid "
                Case enAllocation.TEAM
                    StrQ &= " LEFT JOIN hrteam_master ON hrteam_master.teamunkid = Alloc.teamunkid "
                Case enAllocation.JOB_GROUP
                    StrQ &= " LEFT JOIN " & _
                                  "   ( " & _
                                  "       SELECT " & _
                                  "           jobgroupunkid " & _
                                  "           ,employeeunkid " & _
                                  "           ,ROW_NUMBER()OVER(PARTITION BY employeeunkid ORDER BY effectivedate DESC) AS rno " & _
                                  "       FROM hremployee_categorization_tran " & _
                                  "       WHERE isvoid = 0 AND CONVERT(CHAR(8),effectivedate,112) <= @end_date "
                    If mintEmployeeId > 0 Then
                        StrQ &= " AND hremployee_categorization_tran.employeeunkid = @employeeunkid "
                    End If
                    StrQ &= "   ) AS Jobgrp ON Jobgrp.employeeunkid = #Emplst.employeeunkid AND Jobgrp.rno = 1 " & _
                                  " LEFT JOIN hrjobgroup_master jg ON Jobgrp.jobgroupunkid = jg.jobgroupunkid "
                Case enAllocation.JOBS
                    StrQ &= " LEFT JOIN " & _
                                  "   ( " & _
                                  "       SELECT " & _
                                  "           jobunkid " & _
                                  "           ,employeeunkid " & _
                                  "           ,ROW_NUMBER()OVER(PARTITION BY employeeunkid ORDER BY effectivedate DESC) AS rno " & _
                                  "       FROM hremployee_categorization_tran " & _
                                  "       WHERE isvoid = 0 AND CONVERT(CHAR(8),effectivedate,112) <= @end_date "
                    If mintEmployeeId > 0 Then
                        StrQ &= " AND hremployee_categorization_tran.employeeunkid = @employeeunkid "
                    End If
                    StrQ &= "   ) AS Jobs ON Jobs.employeeunkid = #Emplst.employeeunkid AND Jobs.rno = 1 " & _
                                  " LEFT JOIN hrjob_master jb ON Jobs.jobunkid = jb.jobunkid "
                Case enAllocation.CLASS_GROUP
                    StrQ &= " LEFT JOIN hrclassgroup_master ON hrclassgroup_master.classgroupunkid = Alloc.classgroupunkid "
                Case enAllocation.CLASSES
                    StrQ &= " LEFT JOIN hrclasses_master ON hrclasses_master.classesunkid = Alloc.classunkid "
                Case enAllocation.COST_CENTER
                    StrQ &= " LEFT JOIN " & _
                                  "   ( " & _
                                  "       SELECT " & _
                                  "           cctranheadvalueid " & _
                                  "           ,employeeunkid " & _
                                  "           ,ROW_NUMBER()OVER(PARTITION BY employeeunkid ORDER BY effectivedate DESC) AS rno " & _
                                  "       FROM hremployee_cctranhead_tran " & _
                                  "       WHERE isvoid = 0 AND istransactionhead = 0 AND CONVERT(CHAR(8),effectivedate,112) <= @end_date "
                    If mintEmployeeId > 0 Then
                        StrQ &= " AND hremployee_cctranhead_tran.employeeunkid = @employeeunkid "
                    End If
                    StrQ &= "   ) AS CC ON CC.employeeunkid = #Emplst.employeeunkid AND CC.rno = 1 " & _
                                  " LEFT JOIN prcostcenter_master ON prcostcenter_master.costcenterunkid = CC.cctranheadvalueid "

            End Select

            StrQ &= "LEFT JOIN " & _
                    "( " & _
                    "    SELECT " & _
                    "         employeeunkid " & _
                    "        ,COUNT(1) AS eLt " & _
                    "    FROM tnalogin_summary " & _
                    "    WHERE ltcoming >= 60 " & _
                    "        AND CONVERT(NVARCHAR(8),login_date,112) BETWEEN @start_date AND @end_date " & _
                    "        AND isdayoff = 0 AND isholiday = 0 AND isweekend = 0 AND ispaidleave = 0 "
            If mintEmployeeId > 0 Then
                StrQ &= " AND tnalogin_summary.employeeunkid = @employeeunkid "
            End If

            'S.SANDEEP |13-MAY-2022| -- START
            'ISSUE/ENHANCEMENT : AC2-335
            'StrQ &= "    GROUP BY employeeunkid " & _
            '        ") AS LT ON LT.employeeunkid = #Emplst.employeeunkid " & _
            '        "LEFT JOIN " & _
            '        "( " & _
            '        "    SELECT " & _
            '        "         employeeunkid " & _
            '        "        ,COUNT(1) AS eP " & _
            '        "    FROM tnalogin_summary " & _
            '        "    WHERE CONVERT(NVARCHAR(8),login_date,112) BETWEEN @start_date AND @end_date " & _
            '        "    AND isunpaidleave = 0 AND total_hrs > 0 "
            StrQ &= "    GROUP BY employeeunkid " & _
                    ") AS LT ON LT.employeeunkid = #Emplst.employeeunkid " & _
                    "LEFT JOIN " & _
                    "( " & _
                    "    SELECT " & _
                    "         employeeunkid " & _
                    "        ,COUNT(1) AS eP " & _
                    "        ,SUM(total_hrs/3600) AS eAcWkhold " & _
                    "        ,ISNULL(RIGHT('00' + CONVERT(VARCHAR(max), SUM(total_hrs)/ 3600), 2) + '.' + RIGHT('00'+ CONVERT(VARCHAR(2), (SUM(total_hrs)% 3600 ) / 60), 2), '00.00') AS eAcWkh " & _
                    "        ,SUM(total_hrs) AS eAcWks " & _
                    "    FROM tnalogin_summary " & _
                    "    WHERE CONVERT(NVARCHAR(8),login_date,112) BETWEEN @start_date AND @end_date " & _
                    "    AND isunpaidleave = 0 AND total_hrs > 0 "
            'S.SANDEEP |13-MAY-2022| -- END

            If mintEmployeeId > 0 Then
                StrQ &= " AND tnalogin_summary.employeeunkid = @employeeunkid "
            End If
            StrQ &= "    GROUP BY employeeunkid " & _
                    ") AS PD ON PD.employeeunkid = #Emplst.employeeunkid " & _
                    "LEFT JOIN " & _
                    "( " & _
                    "    SELECT " & _
                    "        SF.* " & _
                    "    FROM " & _
                    "( " & _
                    "    SELECT " & _
                    "         employeeunkid " & _
                    "        ,hremployee_shift_tran.shiftunkid " & _
                    "        ,CONVERT(NVARCHAR(5),tnashift_tran.starttime,108)+' - '+CONVERT(NVARCHAR(5),tnashift_tran.endtime,108) AS chTime " & _
                    "        ,(workinghrs/3600) AS hrs " & _
                    "        ,ROW_NUMBER()OVER(PARTITION BY employeeunkid ORDER BY effectivedate DESC) AS rno " & _
                    "    FROM hremployee_shift_tran " & _
                    "        JOIN tnashift_master ON tnashift_master.shiftunkid = hremployee_shift_tran.shiftunkid " & _
                    "        JOIN tnashift_tran ON tnashift_tran.shiftunkid = tnashift_master.shiftunkid " & _
                    "    WHERE isvoid = 0 AND CONVERT(NVARCHAR(8),effectivedate,112) <= @end_date " & _
                    "    AND isweekend = 0 "
            If mintEmployeeId > 0 Then
                StrQ &= " AND hremployee_shift_tran.employeeunkid = @employeeunkid "
            End If
            StrQ &= "    ) AS SF WHERE SF.rno = 1 " & _
                    ") AS eSF ON eSF.employeeunkid = #Emplst.employeeunkid " & _
                    "WHERE 1 = 1 "

            Call FilterTitleAndFilterQuery()

            StrQ &= Me._FilterQuery

            dsList = objDataOperation.ExecQuery(StrQ, "DataTable")

            If objDataOperation.ErrorMessage <> "" Then
                Throw New Exception(objDataOperation.ErrorNumber & ":" & objDataOperation.ErrorMessage)
            End If

            StrQ = ""
            StrQ = "SELECT " & _
                   "     employeeunkid " & _
                   "    ,lvleavetype_master.leavetypeunkid " & _
                   "    ,COUNT(1) AS lvcnt " & _
                   "    ,lvleavetype_master.isexcuseleave " & _
                   "FROM lvleaveIssue_tran " & _
                   "    JOIN lvleaveIssue_master ON lvleaveIssue_master.leaveissueunkid = lvleaveIssue_tran.leaveissueunkid " & _
                   "    JOIN lvleavetype_master ON lvleavetype_master.leavetypeunkid = lvleaveIssue_master.leavetypeunkid " & _
                   "WHERE lvleaveIssue_tran.isvoid = 0 AND lvleaveIssue_master.isvoid = 0 " & _
                   "    AND CONVERT(NVARCHAR(8),leavedate,112) BETWEEN @start_date AND @end_date " & _
                   "    AND lvleavetype_master.isactive = 1 "
            If mintEmployeeId > 0 Then
                StrQ &= " AND lvleaveIssue_master.employeeunkid = @employeeunkid "
            End If
            StrQ &= "GROUP BY employeeunkid,lvleavetype_master.leavetypeunkid,lvleavetype_master.isexcuseleave " & _
                   "ORDER BY employeeunkid,leavetypeunkid "

            dsLv = objDataOperation.ExecQuery(StrQ, "DataTable")

            If objDataOperation.ErrorMessage <> "" Then
                Throw New Exception(objDataOperation.ErrorNumber & ":" & objDataOperation.ErrorMessage)
            End If

            dsList.Tables(0).AsEnumerable().ToList().ForEach(Function(x) PerformValueSetting(x, dsLv))

            dsList.Tables(0).AsEnumerable().ToList().ForEach(Function(x) PerformComputation(x, iExLv))

            dsList.Tables(0).Columns("eName").Caption = Language.getMessage(mstrModuleName, 100, "Name")
            dsList.Tables(0).Columns("eCode").Caption = Language.getMessage(mstrModuleName, 101, "Employee ID")
            dsList.Tables(0).Columns("eDays").Caption = Language.getMessage(mstrModuleName, 102, "No of working Days")
            dsList.Tables(0).Columns("eActDaysWk").Caption = Language.getMessage(mstrModuleName, 103, "Actual number of days worked")

            dsList.Tables(0).Columns("eOfficeDays").Caption = Language.getMessage(mstrModuleName, 104, "No of days  in office")
            dsList.Tables(0).Columns("eExpWkh").Caption = Language.getMessage(mstrModuleName, 105, "Expected  no of working hours")
            dsList.Tables(0).Columns("eAcWkh").Caption = Language.getMessage(mstrModuleName, 506, "Actual no. of working (hours.minutes)")
            dsList.Tables(0).Columns("eVarPct").Caption = Language.getMessage(mstrModuleName, 107, "Variance (%)")
            dsList.Tables(0).Columns("eVarWkh").Caption = Language.getMessage(mstrModuleName, 508, "Variance (in hours.minutes)")
            dsList.Tables(0).Columns("eVarWkh").ExtendedProperties.Add("iGCL", "iGCL")
            dsList.Tables(0).Columns("eShDays").Caption = Language.getMessage(mstrModuleName, 109, "Effective Excess/Short (In Days)")
            dsList.Tables(0).Columns("eChTime").Caption = Language.getMessage(mstrModuleName, 110, "Employee Chosen Time")
            dsList.Tables(0).Columns("eLtArrv").Caption = Language.getMessage(mstrModuleName, 111, "Late Arrival Post shift start time")
            dsList.Tables(0).Columns("eLtArrv").ExtendedProperties.Add("iYCL", "iYCL")

            dsList.Tables(0).Columns("srn").Caption = Language.getMessage(mstrModuleName, 112, "S.No")
            For Each l As DataRow In iNrLv
                dsList.Tables(0).Columns("lv_" & l("leavetypeunkid").ToString).Caption = l("leavetypecode").ToString
            Next
            For Each k As Integer In mdicExcuseCaption.Keys
                dsList.Tables(0).Columns("lv_" & k.ToString).Caption = mdicExcuseCaption(k)
            Next


            Dim mdtTable As DataTable = dsList.Tables(0).Clone()
            If mblnConsiderZeroAsValue Then
                If mintDaysFrom <> -1 AndAlso mintDaysTo <> -1 Then
                    mdtTable = New DataView(dsList.Tables(0), "eActDaysWk >= " & mintDaysFrom.ToString() & " AND eActDaysWk <= " & mintDaysTo.ToString(), "allocation", DataViewRowState.CurrentRows).ToTable()
                End If

            Else
                mdtTable = New DataView(dsList.Tables(0), "", "allocation", DataViewRowState.CurrentRows).ToTable()
            End If

            mdtTable.AsEnumerable().ToList().ForEach(Function(x) PerformSrNo(x))


            Dim strarrGroupColumns As String() = Nothing
            Dim rowsArrayHeader As New ArrayList
            Dim rowsArrayFooter As New ArrayList
            Dim row As WorksheetRow
            Dim wcell As WorksheetCell
            Dim strBuilder As New StringBuilder

            If mdtTable.Columns.Contains("employeeunkid") Then
                mdtTable.Columns.Remove("employeeunkid")
            End If

            If mdtTable.Columns.Contains("hrs") Then
                mdtTable.Columns.Remove("hrs")
            End If

            If mdtTable.Columns.Contains("eAcWks") Then
                mdtTable.Columns.Remove("eAcWks")
            End If

            If mdtTable.Columns.Contains("eExpWks") Then
                mdtTable.Columns.Remove("eExpWks")
            End If

            For Each iCol As String In mDicExcuseLeaveSecs.Keys
                mdtTable.Columns.Remove(mDicExcuseLeaveSecs(iCol))
            Next


            mdtTable.Columns("allocation").Caption = mstrAllocationName
            Dim strGrpCols As String() = {"allocation"}
            strarrGroupColumns = strGrpCols

            row = New WorksheetRow()
            If Me._FilterTitle.ToString.Length > 0 Then
                wcell = New WorksheetCell(Me._FilterTitle.ToString, "s10bw")
                wcell.MergeAcross = dsList.Tables(0).Columns.Count - 1
                row.Cells.Add(wcell)
            End If
            rowsArrayHeader.Add(row)

            row = New WorksheetRow()
            wcell = New WorksheetCell("", "s10bw")
            row.Cells.Add(wcell)
            rowsArrayHeader.Add(row)


            Dim intArrayColumnWidth As Integer() = Nothing
            ReDim intArrayColumnWidth(mdtTable.Columns.Count - 1)
            For i As Integer = 0 To intArrayColumnWidth.Length - 1
                If i = 0 Then
                    intArrayColumnWidth(i) = 30
                ElseIf i > 0 AndAlso i < 2 Then
                    intArrayColumnWidth(i) = 125
                Else
                    intArrayColumnWidth(i) = 60
                End If
            Next

            Call ReportExecute(Nothing, enPrintAction.None, enExportAction.ExcelExtra, xExportReportPath, xOpenReportAfterExport, mdtTable, intArrayColumnWidth, True, True, False, strarrGroupColumns, Me._ReportName, "", " ", , "", False, rowsArrayHeader, rowsArrayFooter, Nothing, Nothing, False, False)

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: Generate_DetailReport; Module Name: " & mstrModuleName)
        End Try
    End Sub

#End Region

    '<Language> This Auto Generated Text Please Do Not Modify it.
#Region " Language & UI Settings "
    Public Shared Sub SetMessages()
        Try
            Language.setMessage(mstrModuleName, 100, "Name")
            Language.setMessage(mstrModuleName, 101, "Employee ID")
            Language.setMessage(mstrModuleName, 102, "No of working Days")
            Language.setMessage(mstrModuleName, 103, "Actual number of days worked")
            Language.setMessage(mstrModuleName, 104, "No of days  in office")
            Language.setMessage(mstrModuleName, 105, "Expected  no of working hours")
            Language.setMessage(mstrModuleName, 107, "Variance (%)")
            Language.setMessage(mstrModuleName, 109, "Effective Excess/Short (In Days)")
            Language.setMessage(mstrModuleName, 110, "Employee Chosen Time")
            Language.setMessage(mstrModuleName, 111, "Late Arrival Post shift start time")
            Language.setMessage(mstrModuleName, 112, "S.No")
            Language.setMessage(mstrModuleName, 200, "Period:")
            Language.setMessage(mstrModuleName, 201, "Employee:")
            Language.setMessage(mstrModuleName, 202, "Only Late Arrival")
            Language.setMessage(mstrModuleName, 203, "Days From :")
            Language.setMessage(mstrModuleName, 204, "Days To :")
            Language.setMessage(mstrModuleName, 300, "KEY")
            Language.setMessage(mstrModuleName, 302, "Arrival after core hours")
            Language.setMessage(mstrModuleName, 401, "Short of working hours")
            Language.setMessage(mstrModuleName, 403, "Not Captured")
            Language.setMessage(mstrModuleName, 506, "Actual no. of working (hours.minute)")
            Language.setMessage(mstrModuleName, 508, "Variance (in hours.minute)")

        Catch Ex As Exception
            DisplayError.Show("-1", Ex.Message, "SetMessages", mstrModuleName)
        End Try
    End Sub
#End Region 'Language & UI Settings
    '</Language>
End Class
