Imports eZeeCommonLib
Imports Aruti.Data
Imports System.IO
Imports System.Text
Imports ExcelWriter

Public Class clsDailyAttendance_Report
    Inherits IReportData
    Private Shared ReadOnly mstrModuleName As String = "clsDailyAttendance_Report"
    Private mstrReportId As String = enArutiReport.Daily_Attendance_Checking_Report
    Dim objDataOperation As clsDataOperation

#Region " Constructor "

    Public Sub New(ByVal intLangId As Integer, ByVal intCompanyId As Integer) 'S.SANDEEP |24-Sep-2020| -- START --END
        Me.setReportData(CInt(mstrReportId), intLangId, intCompanyId) 'S.SANDEEP |24-Sep-2020| -- START --END
        Call Create_OnDetailReport()
    End Sub

#End Region

#Region " Private variables "

    Private mintEmployeeId As Integer = -1
    Private mstrEmployeeName As String = String.Empty
    Private mdtFromDate As Date = Nothing
    Private mdtToDate As Date = Nothing
    Dim dblColTot As Double()
    Private mintReportTypeId As Integer = 0
    Private mstrReportTypeName As String = ""
    'Pinkal (06-Nov-2015) -- Start
    'Enhancement - WORKING ON HYATT CHANGES DAILY ATTENDANCE CHECKING REPORT.
    'Private mstrExportReportPath As String = ConfigParameter._Object._ExportReportPath
    'Private mblnOpenAfterExport As Boolean = ConfigParameter._Object._OpenAfterExport
    Private mstrExportReportPath As String = ""
    Private mblnOpenAfterExport As Boolean = False
    'Pinkal (06-Nov-2015) -- End


    Private mintViewIndex As Integer = -1
    Private mstrViewByIds As String = String.Empty
    Private mstrViewByName As String = String.Empty
    Private mstrAnalysis_Fields As String = ""
    Private mstrAnalysis_Join As String = ""
    Private mstrReport_GroupName As String = ""
    Private mstrUserAccessFilter As String = String.Empty
    Private mstrAdvance_Filter As String = String.Empty
    Private mblnIncludeInactive As Boolean = False
    Private mblnIgnoreEmptyColumns As Boolean = False

    'Anjan [08 September 2015] -- Start
    'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
    Private mintUserUnkid As Integer = -1
    Private mintCompanyUnkid As Integer = -1
    'Anjan [08 September 2015] -- End

    'S.SANDEEP |29-MAR-2019| -- START
    'ENHANCEMENT : 0003634 {PAPAYE}.
    Private strLegendCode As String = String.Empty
    Private mstrLegendName As String = String.Empty
    'S.SANDEEP |29-MAR-2019| -- END

    'Pinkal (22-Dec-2022) -- Start
    ' (A1X-351) TRA - Daily attendance report changes to include new columns and hide other columns
    Dim mstrGroup As String = ""
    Private mintLocationId As Integer = 0
    Private mstrLocation As String = ""
    'Pinkal (22-Dec-2022) -- End

#End Region

#Region " Properties "

    Public WriteOnly Property _FromDate() As Date
        Set(ByVal value As Date)
            mdtFromDate = value
        End Set
    End Property

    Public WriteOnly Property _ToDate() As Date
        Set(ByVal value As Date)
            mdtToDate = value
        End Set
    End Property

    Public WriteOnly Property _EmployeeId() As Integer
        Set(ByVal value As Integer)
            mintEmployeeId = value
        End Set
    End Property

    Public WriteOnly Property _EmployeeName() As String
        Set(ByVal value As String)
            mstrEmployeeName = value
        End Set
    End Property

    Public WriteOnly Property _ReportTypeId() As Integer
        Set(ByVal value As Integer)
            mintReportTypeId = value
        End Set
    End Property

    Public WriteOnly Property _ReportTypeName() As String
        Set(ByVal value As String)
            mstrReportTypeName = value
        End Set
    End Property

    Public WriteOnly Property _ViewIndex() As Integer
        Set(ByVal value As Integer)
            mintViewIndex = value
        End Set
    End Property

    Public WriteOnly Property _ViewByIds() As String
        Set(ByVal value As String)
            mstrViewByIds = value
        End Set
    End Property

    Public WriteOnly Property _ViewByName() As String
        Set(ByVal value As String)
            mstrViewByName = value
        End Set
    End Property

    Public WriteOnly Property _Analysis_Fields() As String
        Set(ByVal value As String)
            mstrAnalysis_Fields = value
        End Set
    End Property

    Public WriteOnly Property _Analysis_Join() As String
        Set(ByVal value As String)
            mstrAnalysis_Join = value
        End Set
    End Property

    Public WriteOnly Property _Report_GroupName() As String
        Set(ByVal value As String)
            mstrReport_GroupName = value
        End Set
    End Property

    Public WriteOnly Property _UserAccessFilter() As String
        Set(ByVal value As String)
            mstrUserAccessFilter = value
        End Set
    End Property

    Public WriteOnly Property _Advance_Filter() As String
        Set(ByVal value As String)
            mstrAdvance_Filter = value
        End Set
    End Property

    Public WriteOnly Property _IncludeInactive() As Boolean
        Set(ByVal value As Boolean)
            mblnIncludeInactive = value
        End Set
    End Property

    Public WriteOnly Property _IgnoreEmptyColumns() As Boolean
        Set(ByVal value As Boolean)
            mblnIgnoreEmptyColumns = value
        End Set
    End Property


    'Pinkal (06-Nov-2015) -- Start
    'Enhancement - WORKING ON HYATT CHANGES DAILY ATTENDANCE CHECKING REPORT.

    Public WriteOnly Property _ExportReportPath() As String
        Set(ByVal value As String)
            mstrExportReportPath = value
        End Set
    End Property

    Public WriteOnly Property _OpenAfterExport() As Boolean
        Set(ByVal value As Boolean)
            mblnOpenAfterExport = value
        End Set
    End Property

    'Pinkal (06-Nov-2015) -- End

    'S.SANDEEP |29-MAR-2019| -- START
    'ENHANCEMENT : 0003634 {PAPAYE}.
    Public WriteOnly Property _LegendCode() As String
        Set(ByVal value As String)
            strLegendCode = value
        End Set
    End Property

    Public WriteOnly Property _LegendName() As String
        Set(ByVal value As String)
            mstrLegendName = value
        End Set
    End Property
    'S.SANDEEP |29-MAR-2019| -- END

    'Pinkal (22-Dec-2022) -- Start
    ' (A1X-351) TRA - Daily attendance report changes to include new columns and hide other columns
    Public WriteOnly Property _LocationId() As Integer
        Set(ByVal value As Integer)
            mintLocationId = value
        End Set
    End Property

    Public WriteOnly Property _Location() As String
        Set(ByVal value As String)
            mstrLocation = value
        End Set
    End Property
    'Pinkal (22-Dec-2022) -- End

#End Region

#Region "Public Function & Procedures "

    Public Sub SetDefaultValue()
        Try
            mintEmployeeId = 0
            mstrEmployeeName = ""
            mdtFromDate = Nothing
            mdtToDate = Nothing
            mintReportTypeId = 0
            mstrReportTypeName = ""
            mintViewIndex = -1
            mstrViewByIds = ""
            mstrViewByName = ""
            mstrAnalysis_Fields = ""
            mstrAnalysis_Join = ""
            mstrReport_GroupName = ""
            mstrUserAccessFilter = ""
            mstrAdvance_Filter = ""
            mblnIncludeInactive = False
            mblnIgnoreEmptyColumns = False

            'Pinkal (06-Nov-2015) -- Start
            'Enhancement - WORKING ON HYATT CHANGES DAILY ATTENDANCE CHECKING REPORT.
            mstrExportReportPath = ""
            mblnOpenAfterExport = False
            'Pinkal (06-Nov-2015) -- End

            'S.SANDEEP |29-MAR-2019| -- START
            'ENHANCEMENT : 0003634 {PAPAYE}.
            strLegendCode = ""
            mstrLegendName = String.Empty
            'S.SANDEEP |29-MAR-2019| -- END

            'Pinkal (22-Dec-2022) -- Start
            ' (A1X-351) TRA - Daily attendance report changes to include new columns and hide other columns
            mintLocationId = 0
            mstrLocation = ""
            'Pinkal (22-Dec-2022) -- End

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: SetDefaultValue; Module Name: " & mstrModuleName)
        End Try
    End Sub

    Private Sub FilterTitleAndFilterQuery()
        Me._FilterQuery = ""
        Me._FilterTitle = ""
        Try

            If mintEmployeeId > 0 Then
                objDataOperation.AddParameter("@EmpId", SqlDbType.Int, eZeeDataType.INT_SIZE, mintEmployeeId)
                Me._FilterQuery &= " WHERE logintran.employeeunkid = @EmpId "
                Me._FilterTitle &= Language.getMessage(mstrModuleName, 1, " Employee : ") & " " & mstrEmployeeName & " "
            End If

            Me._FilterTitle &= Language.getMessage(mstrModuleName, 2, "From Date : ") & " " & mdtFromDate.Date & " To " & Language.getMessage(mstrModuleName, 3, "To Date : ") & mdtToDate.Date

            If mintViewIndex > 0 Then
                Me._FilterQuery &= " ORDER BY Gname, logintran.employeecode , logintran.logindate "

            Else
                Me._FilterQuery &= " ORDER BY logintran.employeecode , logintran.logindate "
            End If

            'S.SANDEEP |29-MAR-2019| -- START
            'ENHANCEMENT : 0003634 {PAPAYE}.
            If strLegendCode <> "0" Then
                Me._FilterTitle &= " " & Language.getMessage(mstrModuleName, 31, "Legends : ") & " " & mstrLegendName & " "
            End If
            'S.SANDEEP |29-MAR-2019| -- END

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: FilterTitleAndFilterQuery; Module Name: " & mstrModuleName)
        End Try
    End Sub

    'Anjan [08 September 2015] -- Start
    'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
    Public Overloads Overrides Sub generateReport(ByVal pintReportType As Integer, Optional ByVal PrintAction As Aruti.Data.enPrintAction = Aruti.Data.enPrintAction.Preview, Optional ByVal ExportAction As Aruti.Data.enExportAction = Aruti.Data.enExportAction.None)
        'Dim objRpt As CrystalDecisions.CrystalReports.Engine.ReportClass = Nothing
        'Dim strReportExportFile As String = ""
        'Try
        '    If Not IsNothing(objRpt) Then
        '        Call ReportExecute(objRpt, PrintAction, ExportAction, ConfigParameter._Object._ExportReportPath, ConfigParameter._Object._OpenAfterExport)
        '    End If
        'Catch ex As Exception
        '    Throw New Exception(ex.Message & "; Procedure Name: generateReport; Module Name: " & mstrModuleName)
        'End Try
    End Sub
    Public Overrides Sub generateReportNew(ByVal xDatabaseName As String, ByVal xUserUnkid As Integer, ByVal xYearUnkid As Integer, ByVal xCompanyUnkid As Integer, ByVal xPeriodStart As Date, ByVal xPeriodEnd As Date, ByVal xUserModeSetting As String, ByVal xOnlyApproved As Boolean, ByVal xExportReportPath As String, ByVal xOpenReportAfterExport As Boolean, ByVal pintReportType As Integer, Optional ByVal PrintAction As Aruti.Data.enPrintAction = Aruti.Data.enPrintAction.Preview, Optional ByVal ExportAction As Aruti.Data.enExportAction = Aruti.Data.enExportAction.None, Optional ByVal xBaseCurrencyId As Integer = 0)
        Dim objRpt As CrystalDecisions.CrystalReports.Engine.ReportClass = Nothing
        Dim strReportExportFile As String = ""
        Try

            If mintCompanyUnkid <= 0 Then
                mintCompanyUnkid = Company._Object._Companyunkid
            End If

            Company._Object._Companyunkid = mintCompanyUnkid
            ConfigParameter._Object._Companyunkid = mintCompanyUnkid

            If mintUserUnkid <= 0 Then
                mintUserUnkid = User._Object._Userunkid
            End If

            User._Object._Userunkid = mintUserUnkid

            If Not IsNothing(objRpt) Then
                Call ReportExecute(objRpt, PrintAction, ExportAction, xExportReportPath, xOpenReportAfterExport)
            End If
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: generateReport; Module Name: " & mstrModuleName)
        End Try
    End Sub
    'Anjan [08 September 2015] -- End

    Public Overrides Sub setDefaultOrderBy(ByVal intReportType As Integer)
        OrderByDisplay = ""
        OrderByQuery = ""
        Try
            OrderByDisplay = iColumn_DetailReport.ColumnItem(0).DisplayName
            OrderByQuery = iColumn_DetailReport.ColumnItem(0).Name
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: setDefaultOrderBy; Module Name: " & mstrModuleName)
        End Try
    End Sub

    Public Overrides Sub setOrderBy(ByVal intReportType As Integer)
        Try
            Call OrderByExecute(iColumn_DetailReport)
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: setOrderBy; Module Name: " & mstrModuleName)
        End Try
    End Sub

#End Region

#Region " Report Generation "

    Dim iColumn_DetailReport As New IColumnCollection

    Public Property Field_OnDetailReport() As IColumnCollection
        Get
            Return iColumn_DetailReport
        End Get
        Set(ByVal value As IColumnCollection)
            iColumn_DetailReport = value
        End Set
    End Property

    Private Sub Create_OnDetailReport()
        Try
            iColumn_DetailReport.Clear()
            iColumn_DetailReport.Add(New IColumn("", ""))
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: Create_OnDetailReport; Module Name: " & mstrModuleName)
        End Try
    End Sub

    'Anjan [08 September 2015] -- Start
    'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
    'Public Sub Generate_DailyAttendanceReport()
    Public Sub Generate_DailyAttendanceReport(ByVal strDatabaseName As String, _
                                      ByVal intUserUnkid As Integer, _
                                      ByVal intYearUnkid As Integer, _
                                      ByVal intCompanyUnkid As Integer, _
                                      ByVal dtPeriodStart As Date, _
                                      ByVal dtPeriodEnd As Date, _
                                      ByVal strUserModeSetting As String, _
                                      ByVal blnOnlyApproved As Boolean, _
                                      ByVal xExportReportPath As String, ByVal xOpenReportAfterExport As Boolean)
        'Anjan [08 September 2015] -- End


        Dim dsAttendance As New DataSet
        Dim dsList As DataSet = Nothing
        Dim StrQ As String = String.Empty
        Dim mintLeaveTypeCount As Integer = 0

        Try

            'Pinkal (22-Dec-2022) -- Start
            ' (A1X-351) TRA - Daily attendance report changes to include new columns and hide other columns
            Dim objGroupMaster As New clsGroup_Master
            objGroupMaster._Groupunkid = 1
            mstrGroup = objGroupMaster._Groupname
            objGroupMaster = Nothing

            If mintCompanyUnkid <= 0 Then
                mintCompanyUnkid = Company._Object._Companyunkid
            End If

            Company._Object._Companyunkid = mintCompanyUnkid
            ConfigParameter._Object._Companyunkid = mintCompanyUnkid

            If mintUserUnkid <= 0 Then
                mintUserUnkid = User._Object._Userunkid
            End If

            User._Object._Userunkid = intUserUnkid


            'Pinkal (22-Dec-2022) -- End

            Dim dsDays As DataSet = GenerateDays()

            If dsDays IsNot Nothing Then
                dsAttendance = New DataSet
                dsAttendance.Tables.Add("Attendance")
                Dim dc As DataColumn


                dsAttendance.Tables(0).Columns.Add("SrNo", Type.GetType("System.Int32"))
                dsAttendance.Tables(0).Columns.Add("EmployeeCode", Type.GetType("System.String"))
                dsAttendance.Tables(0).Columns.Add("Employee", Type.GetType("System.String"))

                'Pinkal (22-Dec-2022) -- Start
                ' (A1X-351) TRA - Daily attendance report changes to include new columns and hide other columns.
                If mstrGroup.Trim.ToUpper() = "TANZANIA REVENUE AUTHORITY" Then
                    dsAttendance.Tables(0).Columns.Add("Location", Type.GetType("System.String"))
                    dsAttendance.Tables(0).Columns.Add("Department", Type.GetType("System.String"))
                End If
                'Pinkal (22-Dec-2022) -- End

                If mintViewIndex > 0 Then
                    dsAttendance.Tables(0).Columns.Add("Id", Type.GetType("System.Int32"))
                    dsAttendance.Tables(0).Columns.Add("GName", Type.GetType("System.String"))
                End If


                For i As Integer = 0 To dsDays.Tables(0).Rows.Count - 1
                    dc = New DataColumn(CDate(dsDays.Tables(0).Rows(i)("PeriodDate")).ToString("yyyyMMdd"))
                    dc.DataType = Type.GetType("System.String")
                    dc.Caption = dsDays.Tables(0).Rows(i)("PeriodMonth").ToString() & "#10;" & CDate(dsDays.Tables(0).Rows(i)("PeriodDate")).ToString("dd") & "#10;" & dsDays.Tables(0).Rows(i)("Periodday")
                    'S.SANDEEP |29-MAR-2019| -- START
                    'ENHANCEMENT : 0003634 {PAPAYE}.
                    dc.ExtendedProperties.Add("oDt_" & dc.ColumnName, dc.ColumnName)
                    'S.SANDEEP |29-MAR-2019| -- END
                    dsAttendance.Tables(0).Columns.Add(dc)
                Next

                'Pinkal (22-Dec-2022) -- Start
                ' (A1X-351) TRA - Daily attendance report changes to include new columns and hide other columns.

                If mstrGroup.Trim.ToUpper() <> "TANZANIA REVENUE AUTHORITY" Then

                    Dim objLeave As New clsleavetype_master
                    Dim DsLeavetype As DataSet = objLeave.GetList("LeaveType", True)

                    For i As Integer = 0 To DsLeavetype.Tables(0).Rows.Count - 1
                        dc = New DataColumn(DsLeavetype.Tables(0).Rows(i)("Leavetypecode"))
                        dc.DataType = Type.GetType("System.String")
                        'dc.Caption = DsLeavetype.Tables(0).Rows(i)("Leavetypecode") & "#10;" & DsLeavetype.Tables(0).Rows(i)("LeaveName")
                        dc.Caption = DsLeavetype.Tables(0).Rows(i)("Leavetypecode")

                        'S.SANDEEP |29-MAR-2019| -- START
                        'ENHANCEMENT : 0003634 {PAPAYE}.

                        'Pinkal (16-Apr-2019) -- Start
                        'Defect [0003748]: Legend filter not working on leave type legends.
                        'dc.ExtendedProperties.Add("lv_" & DsLeavetype.Tables(0).Rows(i)("leavetypeunkid"), "lv_" & DsLeavetype.Tables(0).Rows(i)("leavetypeunkid"))
                        dc.ExtendedProperties.Add("lv_" & DsLeavetype.Tables(0).Rows(i)("leavetypeunkid"), "lv_" & DsLeavetype.Tables(0).Rows(i)("Leavetypecode"))
                        'Pinkal (16-Apr-2019) -- End

                        'S.SANDEEP |29-MAR-2019| -- END

                        dsAttendance.Tables(0).Columns.Add(dc)
                    Next

                    mintLeaveTypeCount = DsLeavetype.Tables(0).Rows.Count

                    dc = Nothing
                    dc = New DataColumn("TotLeave")
                    dc.Caption = Language.getMessage(mstrModuleName, 17, "Total Leave")
                    dc.DataType = Type.GetType("System.String")
                    dsAttendance.Tables(0).Columns.Add(dc)

                End If
                'Pinkal (22-Dec-2022) -- End


                dc = Nothing
                dc = New DataColumn("TotAbsent")
                dc.Caption = Language.getMessage(mstrModuleName, 18, "Total Absent")
                dc.DataType = Type.GetType("System.String")
                dsAttendance.Tables(0).Columns.Add(dc)

                dc = Nothing
                dc = New DataColumn("TotPresent")
                dc.Caption = Language.getMessage(mstrModuleName, 19, "Total Present")
                dc.DataType = Type.GetType("System.String")
                dsAttendance.Tables(0).Columns.Add(dc)

                'Pinkal (22-Dec-2022) -- Start
                ' (A1X-351) TRA - Daily attendance report changes to include new columns and hide other columns.
                If mstrGroup.Trim.ToUpper() <> "TANZANIA REVENUE AUTHORITY" Then
                    dc = Nothing
                    dc = New DataColumn("TotMispunch")
                    dc.Caption = Language.getMessage(mstrModuleName, 20, "Total Mispunch")
                    dc.DataType = Type.GetType("System.String")
                    dsAttendance.Tables(0).Columns.Add(dc)
                End If
                'Pinkal (22-Dec-2022) -- End

                dc = New DataColumn("TotWorkhrs")
                dc.Caption = Language.getMessage(mstrModuleName, 21, "Total Working Hrs")
                dc.DataType = Type.GetType("System.String")
                dsAttendance.Tables(0).Columns.Add(dc)

                'Pinkal (22-Dec-2022) -- Start
                ' (A1X-351) TRA - Daily attendance report changes to include new columns and hide other columns.
                If mstrGroup.Trim.ToUpper() <> "TANZANIA REVENUE AUTHORITY" Then
                    dc = Nothing
                    dc = New DataColumn("TotShorthrs")
                    dc.Caption = Language.getMessage(mstrModuleName, 22, "Total Short Hrs")
                    dc.DataType = Type.GetType("System.String")
                    dsAttendance.Tables(0).Columns.Add(dc)

                    dc = Nothing
                    dc = New DataColumn("TotOT1")
                    dc.Caption = Language.getMessage(mstrModuleName, 23, "Total OT1")
                    dc.DataType = Type.GetType("System.String")
                    dsAttendance.Tables(0).Columns.Add(dc)

                    dc = Nothing
                    dc = New DataColumn("TotOT2")
                    dc.Caption = Language.getMessage(mstrModuleName, 24, "Total OT2")
                    dc.DataType = Type.GetType("System.String")
                    dsAttendance.Tables(0).Columns.Add(dc)

                    dc = Nothing
                    dc = New DataColumn("TotOT3")
                    dc.Caption = Language.getMessage(mstrModuleName, 25, "Total OT3")
                    dc.DataType = Type.GetType("System.String")
                    dsAttendance.Tables(0).Columns.Add(dc)

                    dc = Nothing
                    dc = New DataColumn("TotOT4")
                    dc.Caption = Language.getMessage(mstrModuleName, 26, "Total OT4")
                    dc.DataType = Type.GetType("System.String")
                    dsAttendance.Tables(0).Columns.Add(dc)

                    dc = Nothing
                    dc = New DataColumn("TotNightHrs")
                    dc.Caption = Language.getMessage(mstrModuleName, 27, "Total Night Hrs")
                    dc.DataType = Type.GetType("System.String")
                    dsAttendance.Tables(0).Columns.Add(dc)
                Else
                    dc = Nothing
                    dc = New DataColumn("TotOvertimeDays")
                    dc.Caption = Language.getMessage(mstrModuleName, 32, "Total Overtime Days")
                    dc.DataType = Type.GetType("System.String")
                    dsAttendance.Tables(0).Columns.Add(dc)
                End If
                'Pinkal (22-Dec-2022) -- End

                dsAttendance.Tables(0).Columns.Add("EmployeeId", Type.GetType("System.Int32"))
                dsAttendance.Tables(0).Columns.Add("ShiftId", Type.GetType("System.Int32"))

            End If

            objDataOperation = New clsDataOperation



            'Anjan [08 September 2015] -- Start
            'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
            'If mstrUserAccessFilter.Trim.Length <= 0 Then
            '    mstrUserAccessFilter = UserAccessLevel._AccessLevelFilterString
            'End If

            Dim xDateJoinQry, xDateFilterQry, xUACQry, xUACFiltrQry, xAdvanceJoinQry As String
            xDateJoinQry = "" : xDateFilterQry = "" : xUACQry = "" : xUACFiltrQry = "" : xAdvanceJoinQry = ""
            Call GetDatesFilterString(xDateJoinQry, xDateFilterQry, dtPeriodStart, dtPeriodEnd, , , strDatabaseName)
            Call NewAccessLevelFilterString(xUACQry, xUACFiltrQry, dtPeriodEnd, blnOnlyApproved, strDatabaseName, intUserUnkid, intCompanyUnkid, intYearUnkid, strUserModeSetting)
            Call GetAdvanceFilterQry(xAdvanceJoinQry, dtPeriodEnd, strDatabaseName)
            'Anjan [08 September 2015] -- End

            'Pinkal (22-Dec-2022) -- Start 
            ' (A1X-351) TRA - Daily attendance report changes to include new columns and hide other columns.
            Dim strAllocationName As String = String.Empty
            Dim strAllocationJoin As String = String.Empty
            Dim strEmpTransacJoin As String = String.Empty
            Dim strDeptJoin As String = String.Empty

            'Pinkal (22-Dec-2022) -- Start 
            ' (A1X-351) TRA - Daily attendance report changes to include new columns and hide other columns.
            If mstrGroup.Trim.ToUpper() = "TANZANIA REVENUE AUTHORITY" Then

                strDeptJoin = " LEFT JOIN hrdepartment_master AS disdpt on disdpt.departmentunkid = Tr.departmentunkid "

                Select Case mintLocationId
                    Case enAllocation.BRANCH
                        strAllocationName = ",ISNULL(st.name, '') "
                        strAllocationJoin = " LEFT JOIN hrstation_master st ON Tr.stationunkid = st.stationunkid "
                    Case enAllocation.DEPARTMENT_GROUP
                        strAllocationName = ",ISNULL(dgm.name, '') "
                        strAllocationJoin = " LEFT JOIN hrdepartment_group_master dgm ON dgm.deptgroupunkid = Tr.deptgroupunkid "
                    Case enAllocation.DEPARTMENT
                        strAllocationName = ",ISNULL(dm.name, '') "
                        strAllocationJoin = " LEFT JOIN hrdepartment_master dm ON Tr.departmentunkid = dm.departmentunkid "
                    Case enAllocation.SECTION_GROUP
                        strAllocationName = ",ISNULL(sgm.name, '') "
                        strAllocationJoin = " LEFT JOIN hrsectiongroup_master sgm ON Tr.sectiongroupunkid = sgm.sectiongroupunkid "
                    Case enAllocation.SECTION
                        strAllocationName = ",ISNULL(sm.name, '') "
                        strAllocationJoin = " LEFT JOIN hrsection_master sm ON Tr.sectionunkid = sm.sectionunkid "
                    Case enAllocation.UNIT_GROUP
                        strAllocationName = ",ISNULL(ugm.name, '') "
                        strAllocationJoin = " LEFT JOIN hrunitgroup_master ugm ON Tr.unitgroupunkid = ugm.unitgroupunkid "
                    Case enAllocation.UNIT
                        strAllocationName = ",ISNULL(um.name, '') "
                        strAllocationJoin = " LEFT JOIN hrunit_master um ON Tr.unitunkid = um.unitunkid "
                    Case enAllocation.TEAM
                        strAllocationName = ",ISNULL(tm.name, '') "
                        strAllocationJoin = " LEFT JOIN hrteam_master tm ON Tr.teamunkid = tm.teamunkid "
                    Case enAllocation.JOB_GROUP
                        strAllocationName = ",ISNULL(jgm.name, '') "
                        strAllocationJoin = " LEFT JOIN hrjobgroup_master jgm ON Jr.jobgroupunkid = jgm.jobgroupunkid "
                    Case enAllocation.JOBS
                        strAllocationName = ",ISNULL(jm.job_name, '') "
                        strAllocationJoin = " LEFT JOIN hrjob_master jm ON Jr.jobunkid = jm.jobunkid "
                    Case enAllocation.CLASS_GROUP
                        strAllocationName = ",ISNULL(cgm.name, '') "
                        strAllocationJoin = " LEFT JOIN hrclassgroup_master cgm ON Tr.classgroupunkid = cgm.classgroupunkid "
                    Case enAllocation.CLASSES
                        strAllocationName = ",ISNULL(clm.name, '') "
                        strAllocationJoin = " LEFT JOIN hrclasses_master clm ON Tr.classunkid = clm.classesunkid "
                    Case enAllocation.COST_CENTER
                        strAllocationName = ",ISNULL(pcm.costcentername, '') "
                        strAllocationJoin = " LEFT JOIN prcostcenter_master pcm ON Cr.costcenterunkid = pcm.costcenterunkid "
                End Select

                strEmpTransacJoin = " LEFT JOIN " & _
                                                "( " & _
                                                "   SELECT " & _
                                                "         hremployee_transfer_tran.stationunkid " & _
                                                "        ,hremployee_transfer_tran.deptgroupunkid " & _
                                                "        ,hremployee_transfer_tran.departmentunkid " & _
                                                "        ,hremployee_transfer_tran.sectiongroupunkid " & _
                                                "        ,hremployee_transfer_tran.sectionunkid " & _
                                                "        ,hremployee_transfer_tran.unitgroupunkid " & _
                                                "        ,hremployee_transfer_tran.unitunkid " & _
                                                "        ,hremployee_transfer_tran.teamunkid " & _
                                                "        ,hremployee_transfer_tran.classgroupunkid " & _
                                                "        ,hremployee_transfer_tran.classunkid " & _
                                                "        ,ROW_NUMBER()OVER(PARTITION BY hremployee_transfer_tran.employeeunkid ORDER BY hremployee_transfer_tran.effectivedate DESC) AS rno " & _
                                                "        ,hremployee_transfer_tran.employeeunkid " & _
                                                "   FROM hremployee_transfer_tran " & _
                                                "   WHERE hremployee_transfer_tran.isvoid = 0 AND CONVERT(NVARCHAR(8),effectivedate,112) <= @enddate " & _
                                               " ) AS Tr ON Tr.employeeunkid = hremployee_master.employeeunkid AND Tr.rno = 1 " & _
                                               " LEFT JOIN " & _
                                               "( " & _
                                               "   SELECT " & _
                                               "        hremployee_categorization_tran.jobgroupunkid " & _
                                               "       ,hremployee_categorization_tran.jobunkid " & _
                                               "       ,ROW_NUMBER()OVER(PARTITION BY hremployee_categorization_tran.employeeunkid ORDER BY hremployee_categorization_tran.effectivedate DESC) AS rno " & _
                                               "       ,hremployee_categorization_tran.employeeunkid " & _
                                               "   FROM hremployee_categorization_tran " & _
                                               "   WHERE hremployee_categorization_tran.isvoid = 0 AND CONVERT(NVARCHAR(8),effectivedate,112) <= @enddate " & _
                                               " ) AS Jr ON Jr.employeeunkid = hremployee_master.employeeunkid AND Jr.rno = 1 " & _
                                               " LEFT JOIN " & _
                                               "( " & _
                                               "   SELECT " & _
                                               "        hremployee_cctranhead_tran.cctranheadvalueid " & _
                                               "       ,ROW_NUMBER()OVER(PARTITION BY hremployee_cctranhead_tran.employeeunkid ORDER BY hremployee_cctranhead_tran.effectivedate DESC) AS rno " & _
                                               "       ,hremployee_cctranhead_tran.employeeunkid " & _
                                               "   FROM hremployee_cctranhead_tran " & _
                                               "   WHERE hremployee_cctranhead_tran.isvoid = 0 " & _
                                               "   AND hremployee_cctranhead_tran.istransactionhead = 0 AND CONVERT(NVARCHAR(8),effectivedate,112) <= @enddate " & _
                                               " ) AS Cr ON Cr.employeeunkid = hremployee_master.employeeunkid AND Cr.rno = 1 "

            End If
            'Pinkal (22-Dec-2022) -- End 



            'Pinkal (22-Dec-2022) -- End

            'FOR  EMPLOYEE
            StrQ = " Select employeeunkid, 0 AS SrNo, employeecode,employee, shiftunkid,  logindate, Workinghrs, " & _
                      " CONVERT(VARCHAR(MAX),( CONVERT(INT, SUBSTRING(Totworkhrs, 0,CHARINDEX(':',Totworkhrs)))* 3600 + CONVERT(INT, RIGHT(Totworkhrs, 2))* 60 - " & _
                      " CONVERT(INT, SUBSTRING(TotOvertime, 0,CHARINDEX(':',TotOvertime))) * 3600 + CONVERT(INT, RIGHT(TotOvertime, 2))* 60 ) / 3600)  " & _
                      " + ':' +  " & _
                      " RIGHT('00'+ CONVERT(VARCHAR(MAX), ( CONVERT(INT, SUBSTRING(Totworkhrs,0,CHARINDEX(':',Totworkhrs)))* 3600 + CONVERT(INT, RIGHT(Totworkhrs,2)) * 60 -" & _
                      " CONVERT(INT, SUBSTRING(TotOvertime, 0,CHARINDEX(':', TotOvertime))) * 3600 + CONVERT(INT, RIGHT(TotOvertime, 2)) ) % 3600 / 60), 2) TotWorkhrs , " & _
                      " TotOvertime,TotOT2,TotOT3,TotOT4,TotShorthrs,TotAbsent,TotNightHrs,TotMispunch  "

            'Pinkal (22-Dec-2022) -- Start 
            ' (A1X-351) TRA - Daily attendance report changes to include new columns and hide other columns.
            If mstrGroup.Trim.ToUpper() = "TANZANIA REVENUE AUTHORITY" Then
                StrQ &= ", TotOvertimeDays,Department,Location "
            End If
            'Pinkal (22-Dec-2022) -- End

            If mintViewIndex > 0 Then
                StrQ &= ",ID,GName "
            End If




            StrQ &= "FROM " & _
                          " ( " & _
                          "  SELECT  tnalogin_summary.employeeunkid ,ISNULL(hremployee_master.firstname, '') + ' ' + ISNULL(hremployee_master.surname, '') employee, tnalogin_summary.shiftunkid " & _
                          ", ISNULL(hremployee_master.employeecode,'') employeecode, CONVERT(VARCHAR(10),login_date,112) logindate  " & _
                        ", ISNULL(CONVERT(VARCHAR(MAX),total_hrs / 3600) + ':'  + RIGHT('00'+ CONVERT(VARCHAR(MAX), (total_hrs % 3600 ) / 60), 2), '00:00') Workinghrs  " & _
                          ", ISNULL(CONVERT(VARCHAR(MAX),(SELECT SUM(t.total_hrs) / 3600 FROM tnalogin_summary t WHERE t.employeeunkid = tnalogin_summary.employeeunkid  AND CONVERT(VARCHAR(10),login_date,112) BETWEEN @startdate and @enddate)) " & _
                          " + ':' +  " & _
                        " RIGHT('00' + CONVERT(VARCHAR(MAX),(SELECT (SUM(t.total_hrs) % 3600 ) / 60 FROM tnalogin_summary t WHERE t.employeeunkid = tnalogin_summary.employeeunkid  AND CONVERT(VARCHAR(10),login_date,112) BETWEEN @startdate and @enddate)), 2), '00:00') TotWorkhrs " & _
                          ", ISNULL(CONVERT(VARCHAR(MAX),(SELECT SUM(t.total_short_hrs) / 3600 FROM tnalogin_summary t WHERE t.employeeunkid = tnalogin_summary.employeeunkid  AND CONVERT(VARCHAR(10),login_date,112) BETWEEN @startdate and @enddate)) " & _
                          " + ':' +  " & _
                        " RIGHT('00' + CONVERT(VARCHAR(MAX),(SELECT (SUM(t.total_short_hrs) % 3600 ) / 60 FROM tnalogin_summary t WHERE t.employeeunkid = tnalogin_summary.employeeunkid  AND CONVERT(VARCHAR(10),login_date,112) BETWEEN @startdate and @enddate )), 2), '00:00') TotShorthrs " & _
                          ", ISNULL(CONVERT(VARCHAR(MAX),(SELECT SUM(t.total_overtime) / 3600 FROM tnalogin_summary t WHERE t.employeeunkid = tnalogin_summary.employeeunkid  AND CONVERT(VARCHAR(10),login_date,112) BETWEEN @startdate and @enddate)) " & _
                          " + ':' +  " & _
                        " RIGHT('00' + CONVERT(VARCHAR(MAX),(SELECT (SUM(t.total_overtime) % 3600 ) / 60 FROM tnalogin_summary t WHERE t.employeeunkid = tnalogin_summary.employeeunkid  AND CONVERT(VARCHAR(10),login_date,112) BETWEEN @startdate and @enddate )), 2), '00:00') TotOvertime " & _
                          ", ISNULL(CONVERT(VARCHAR(MAX),(SELECT SUM(t.ot2) / 3600 FROM tnalogin_summary t WHERE t.employeeunkid = tnalogin_summary.employeeunkid  AND CONVERT(VARCHAR(10),login_date,112) BETWEEN @startdate and @enddate)) " & _
                          " + ':' +  " & _
                        " RIGHT('00' + CONVERT(VARCHAR(MAX),(SELECT (SUM(t.ot2) % 3600 ) / 60 FROM tnalogin_summary t WHERE t.employeeunkid = tnalogin_summary.employeeunkid  AND CONVERT(VARCHAR(10),login_date,112) BETWEEN @startdate and @enddate )), 2), '00:00') TotOT2 " & _
                          ", ISNULL(CONVERT(VARCHAR(MAX),(SELECT SUM(t.ot3) / 3600 FROM tnalogin_summary t WHERE t.employeeunkid = tnalogin_summary.employeeunkid  AND CONVERT(VARCHAR(10),login_date,112) BETWEEN @startdate and @enddate)) " & _
                          " + ':' +  " & _
                        " RIGHT('00' + CONVERT(VARCHAR(MAX),(SELECT (SUM(t.ot3) % 3600 ) / 60 FROM tnalogin_summary t WHERE t.employeeunkid = tnalogin_summary.employeeunkid  AND CONVERT(VARCHAR(8),login_date,112) BETWEEN @startdate and @enddate )), 2), '00:00') TotOT3 " & _
                          ", ISNULL(CONVERT(VARCHAR(MAX),(SELECT SUM(t.ot4) / 3600 FROM tnalogin_summary t WHERE t.employeeunkid = tnalogin_summary.employeeunkid  AND CONVERT(VARCHAR(8),login_date,112) BETWEEN @startdate and @enddate)) " & _
                          " + ':' +  " & _
                        " RIGHT('00' + CONVERT(VARCHAR(MAX),(SELECT (SUM(t.ot4) % 3600 ) / 60 FROM tnalogin_summary t WHERE t.employeeunkid = tnalogin_summary.employeeunkid  AND CONVERT(VARCHAR(10),login_date,112) BETWEEN @startdate and @enddate )), 2), '00:00') TotOT4 " & _
                          ", ISNULL(CONVERT(VARCHAR(MAX),(SELECT SUM(t.total_nighthrs) / 3600 FROM tnalogin_summary t WHERE t.employeeunkid = tnalogin_summary.employeeunkid  AND CONVERT(VARCHAR(8),login_date,112) BETWEEN @startdate and @enddate)) " & _
                          " + ':' +  " & _
                        " RIGHT('00' + CONVERT(VARCHAR(MAX),(SELECT (SUM(t.total_nighthrs) % 3600 ) / 60 FROM tnalogin_summary t WHERE t.employeeunkid = tnalogin_summary.employeeunkid  AND CONVERT(VARCHAR(10),login_date,112) BETWEEN @startdate and @enddate )), 2), '00:00') TotNightHrs " & _
                          ",   (select COUNT(t.isunpaidleave) FROM tnalogin_summary t WHERE t.employeeunkid = tnalogin_summary.employeeunkid AND t.isunpaidleave = 1 AND CONVERT(VARCHAR(10),login_date,112) BETWEEN @startdate and @enddate) TotAbsent " & _
                          ",   (select COUNT(*) FROM tnalogin_tran t WHERE t.employeeunkid = tnalogin_summary.employeeunkid AND (t.checkintime is NULL OR t.checkouttime is NULL)  AND t.isvoid = 0 AND  CONVERT(VARCHAR(8),logindate,112) BETWEEN @startdate and @enddate) TotMispunch "

            'Pinkal (05-Feb-2016) -- End

            'Pinkal (22-Dec-2022) -- Start 
            ' (A1X-351) TRA - Daily attendance report changes to include new columns and hide other columns.
            If mstrGroup.Trim.ToUpper() = "TANZANIA REVENUE AUTHORITY" Then
                StrQ &= ",(SELECT COUNT(t.total_overtime)  FROM tnalogin_summary t WHERE t.total_overtime >= 60 AND t.employeeunkid = tnalogin_summary.employeeunkid AND CONVERT(VARCHAR(10),login_date,112) BETWEEN @startdate and @enddate) TotOvertimeDays " & _
                             ", ISNULL(disdpt.name,'') AS Department  "

                If strAllocationName.Trim.Length > 0 Then
                    StrQ &= " " & strAllocationName & " AS Location "
                End If
            End If
            'Pinkal (22-Dec-2022) -- End 



            If mintViewIndex > 0 Then
                StrQ &= mstrAnalysis_Fields
            Else
                StrQ &= ", 0 AS Id, '' AS GName "
            End If

            StrQ &= "  FROM hremployee_master " & _
                         "  LEFT JOIN tnalogin_summary ON tnalogin_summary.employeeunkid = hremployee_master.employeeunkid "


            'Pinkal (22-Dec-2022) -- Start
            ' (A1X-351) TRA - Daily attendance report changes to include new columns and hide other columns.
            If mstrGroup.Trim.ToUpper() = "TANZANIA REVENUE AUTHORITY" Then
                If strEmpTransacJoin.Trim.Length > 0 Then
                    StrQ &= " " & strEmpTransacJoin & " "
                End If

                If strAllocationJoin.Trim.Length > 0 Then
                    StrQ &= " " & strAllocationJoin & " "
                End If
                StrQ &= " " & strDeptJoin & " "
            End If
            'Pinkal (22-Dec-2022) -- End

            'Anjan [08 September 2015] -- Start
            'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
            If xDateJoinQry.Trim.Length > 0 Then
                StrQ &= xDateJoinQry
            End If

            If xUACQry.Trim.Length > 0 Then
                StrQ &= xUACQry
            End If

            If xAdvanceJoinQry.Trim.Length > 0 Then
                StrQ &= xAdvanceJoinQry
            End If
            'Anjan [08 September 2015] -- End



            If mintViewIndex > 0 Then
                StrQ &= mstrAnalysis_Join
            End If

            StrQ &= "  WHERE CONVERT(VARCHAR(10),login_date,112) BETWEEN @startdate and @enddate "


            'Anjan [08 September 2015] -- Start
            'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
            'If mstrAdvance_Filter.Trim.Length > 0 Then
            '    StrQ &= " AND " & mstrAdvance_Filter & " "
            'End If

            'If mblnIncludeInactive = False Then
            '    StrQ &= " AND CONVERT(CHAR(8),hremployee_master.appointeddate,112) <= @enddate " & _
            '                 " AND ISNULL(CONVERT(CHAR(8),hremployee_master.termination_from_date,112),@startdate) >= @startdate " & _
            '                 " AND ISNULL(CONVERT(CHAR(8),hremployee_master.termination_to_date,112),@startdate) >= @startdate " & _
            '                 " AND ISNULL(CONVERT(CHAR(8), hremployee_master.empl_enddate,112), @startdate) >= @startdate "
            'End If

            'If mstrUserAccessFilter.Trim.Length > 0 Then
            '    StrQ &= mstrUserAccessFilter
            'End If


            If xUACFiltrQry.Trim.Length > 0 Then
                StrQ &= " AND " & xUACFiltrQry & " "
            End If

            If mblnIncludeInactive = False Then
                If xDateFilterQry.Trim.Length > 0 Then
                    StrQ &= " " & xDateFilterQry & " "
                End If
            End If
            If mstrAdvance_Filter.Length > 0 Then
                StrQ &= " AND " & mstrAdvance_Filter & " "
            End If
            'Anjan [08 September 2015] -- End



            StrQ &= " ) AS logintran  "

            objDataOperation.ClearParameters()
            objDataOperation.AddParameter("@startdate", SqlDbType.NVarChar, eZeeDataType.DATETIME_SIZE, eZeeDate.convertDate(mdtFromDate))
            objDataOperation.AddParameter("@enddate", SqlDbType.NVarChar, eZeeDataType.DATETIME_SIZE, eZeeDate.convertDate(mdtToDate))

            FilterTitleAndFilterQuery()
            StrQ &= Me._FilterQuery

            dsList = objDataOperation.ExecQuery(StrQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                Throw New Exception(objDataOperation.ErrorNumber & ":" & objDataOperation.ErrorMessage)
            End If


            Dim mintEmp As Integer = 0
            Dim mintDeptId As Integer = 0
            Dim intCount As Integer = -1
            Dim intEmpCount As Integer = 0
            Dim intPresentCount As Integer = 0
            Dim intAbsentCount As Integer = 0
            Dim drRow As DataRow = Nothing
            Dim objLogin As New clslogin_Tran

            For k As Integer = 0 To dsList.Tables(0).Rows.Count - 1

                If mintEmp <> dsList.Tables(0).Rows(k)("employeeunkid") Then 'FOR INSERTING EMPLOYEE
                    drRow = dsAttendance.Tables(0).NewRow
                    intEmpCount += 1
                    intPresentCount = 0
                    intAbsentCount = 0
                    drRow("ShiftId") = dsList.Tables(0).Rows(k)("shiftunkid")
                    drRow("EmployeeId") = dsList.Tables(0).Rows(k)("employeeunkid")

                    If mintViewIndex > 0 Then
                        drRow("Id") = dsList.Tables(0).Rows(k)("Id")
                        drRow("GName") = dsList.Tables(0).Rows(k)("GName")
                    End If

                    drRow("SrNo") = intEmpCount
                    drRow("EmployeeCode") = dsList.Tables(0).Rows(k)("employeecode")
                    drRow("Employee") = dsList.Tables(0).Rows(k)("employee")
                    drRow("TotWorkhrs") = dsList.Tables(0).Rows(k)("TotWorkhrs").ToString()

                    'Pinkal (22-Dec-2022) -- Start
                    ' (A1X-351) TRA - Daily attendance report changes to include new columns and hide other columns.
                    If mstrGroup.Trim.ToUpper() <> "TANZANIA REVENUE AUTHORITY" Then
                        drRow("TotLeave") = "0"
                        drRow("TotShorthrs") = dsList.Tables(0).Rows(k)("TotShorthrs").ToString()
                        drRow("TotOT1") = dsList.Tables(0).Rows(k)("TotOvertime").ToString()
                        drRow("TotOT2") = dsList.Tables(0).Rows(k)("TotOT2").ToString()
                        drRow("TotOT3") = dsList.Tables(0).Rows(k)("TotOT3").ToString()
                        drRow("TotOT4") = dsList.Tables(0).Rows(k)("TotOT4").ToString()
                        drRow("TotMispunch") = dsList.Tables(0).Rows(k)("TotMispunch").ToString()
                        drRow("TotNightHrs") = dsList.Tables(0).Rows(k)("TotNightHrs").ToString()
                    Else
                        drRow("Location") = dsList.Tables(0).Rows(k)("Location").ToString()
                        drRow("Department") = dsList.Tables(0).Rows(k)("Department").ToString()
                        drRow("TotOvertimeDays") = dsList.Tables(0).Rows(k)("TotOvertimeDays").ToString()
                    End If
                    'Pinkal (22-Dec-2022) -- End


                    'Pinkal (10-Jun-2017) -- Start
                    'Enhancement - Solved Bug for AKIBA  AND FROM NOW Absent will be Calculate manually Not From Query.
                    'drRow("TotAbsent") = dsList.Tables(0).Rows(k)("TotAbsent")
                    drRow("TotAbsent") = 0
                    'Pinkal (10-Jun-2017) -- End

                    dsAttendance.Tables(0).Rows.Add(drRow)
                    dsAttendance.Tables(0).AcceptChanges()
                    mintEmp = CInt(dsList.Tables(0).Rows(k)("employeeunkid"))
                    intCount += 1
                End If

                If mintEmp = CInt(dsList.Tables(0).Rows(k)("employeeunkid")) Then 'FOR INSERTING WORKING HRS  

                    objLogin._Employeeunkid = CInt(dsList.Tables(0).Rows(k)("employeeunkid"))
                    objLogin._Logindate = eZeeDate.convertDate(dsList.Tables(0).Rows(k)("logindate"))
                    Dim mintCount As Integer = objLogin.GetList("List", True).Tables(0).Compute("COUNT(checkintime)", "1=1")

                    'Pinkal (10-Jun-2017) -- Start
                    'Enhancement - Solved Bug for AKIBA  AND FROM NOW Absent will be Calculate manually Not From Query.
                    ' If dsList.Tables(0).Rows(k)("Workinghrs").ToString() <> "00:00" AndAlso dsList.Tables(0).Rows(k)("Workinghrs").ToString() <> "0:00" Then
                    If mintCount > 0 Then
                        'S.SANDEEP |29-MAR-2019| -- START
                        'ENHANCEMENT : 0003634 {PAPAYE}.
                        'dsAttendance.Tables(0).Rows(intCount)(dsList.Tables(0).Rows(k)("logindate")) = Language.getMessage(mstrModuleName, 5, "PR")
                        'intPresentCount += 1
                        'dsAttendance.Tables(0).Rows(intCount)("TotPresent") = intPresentCount
                        If strLegendCode = "0" Then
                            dsAttendance.Tables(0).Rows(intCount)(dsList.Tables(0).Rows(k)("logindate")) = Language.getMessage(mstrModuleName, 5, "PR")
                            intPresentCount += 1
                            dsAttendance.Tables(0).Rows(intCount)("TotPresent") = intPresentCount
                        ElseIf strLegendCode = "PR" Then
                            dsAttendance.Tables(0).Rows(intCount)(dsList.Tables(0).Rows(k)("logindate")) = Language.getMessage(mstrModuleName, 5, "PR")
                            intPresentCount += 1
                            dsAttendance.Tables(0).Rows(intCount)("TotPresent") = intPresentCount
                        End If
                        'Pinkal (20-Apr-2019) -- End

                        'S.SANDEEP |29-MAR-2019| -- END
                    Else
                        'S.SANDEEP |29-MAR-2019| -- START
                        'ENHANCEMENT : 0003634 {PAPAYE}.
                        'dsAttendance.Tables(0).Rows(intCount)(dsList.Tables(0).Rows(k)("logindate")) = Language.getMessage(mstrModuleName, 6, "AB")
                        'intAbsentCount += 1
                        'dsAttendance.Tables(0).Rows(intCount)("TotAbsent") = intAbsentCount
                        If strLegendCode = "0" Then
                            dsAttendance.Tables(0).Rows(intCount)(dsList.Tables(0).Rows(k)("logindate")) = Language.getMessage(mstrModuleName, 6, "AB")
                            intAbsentCount += 1
                            dsAttendance.Tables(0).Rows(intCount)("TotAbsent") = intAbsentCount
                        ElseIf strLegendCode = "AB" Then
                            dsAttendance.Tables(0).Rows(intCount)(dsList.Tables(0).Rows(k)("logindate")) = Language.getMessage(mstrModuleName, 6, "AB")
                            intAbsentCount += 1
                            dsAttendance.Tables(0).Rows(intCount)("TotAbsent") = intAbsentCount
                        End If
                        'S.SANDEEP |29-MAR-2019| -- END
                    End If
                    'Pinkal (10-Jun-2017) -- End

                End If

            Next



            Dim objShiftTran As New clsshift_tran
            Dim objholiday As New clsemployee_holiday
            Dim objDayOFF As New clsemployee_dayoff_Tran

            'Pinkal (22-Nov-2018) -- Start
            'BUG - Solved Bug For Papaaye for fetching wrong shift.
            ' Dim objshift As New clsNewshift_master
            Dim objEmpShiftTran As New clsEmployee_Shift_Tran
            'Pinkal (22-Nov-2018) -- End


            For i As Integer = 0 To dsAttendance.Tables(0).Rows.Count - 1
                If IsDBNull(dsAttendance.Tables(0).Rows(i)("shiftid")) Then Continue For

                'Pinkal (22-Nov-2018) -- Start
                'BUG - Solved Bug For Papaaye for fetching wrong shift.
                'objshift._Shiftunkid = CInt(dsAttendance.Tables(0).Rows(i)("shiftid"))
                'Pinkal (22-Nov-2018) -- End

                For j As Integer = 0 To dsDays.Tables(0).Rows.Count - 1
                    Dim mstrDate As String = CDate(dsDays.Tables(0).Rows(j)("PeriodDate")).ToString("yyyyMMdd")
                    If dsAttendance.Tables(0).Columns(mstrDate).ToString = mstrDate Then

                        'FOR HOLIDAY
                        Dim dtHolidayName As DataTable = objholiday.GetEmployeeHoliday(CInt(dsAttendance.Tables(0).Rows(i)("EmployeeID")), eZeeDate.convertDate(mstrDate))
                        Dim mstrHoliday As String = ""
                        If dtHolidayName IsNot Nothing AndAlso dtHolidayName.Rows.Count > 0 Then
                            mstrHoliday = dtHolidayName.Rows(0)("holidayname")
                        End If

                        If mstrHoliday <> "" Then
                            Dim arHoliday As String() = mstrHoliday.Split(",")
                            mstrHoliday = arHoliday(0)
                            Dim mstrColor As String = ""
                            If arHoliday.Length > 1 Then mstrColor = arHoliday(1)
                            If mstrHoliday <> "" And (dsAttendance.Tables(0).Rows(i)(mstrDate).ToString = Language.getMessage(mstrModuleName, 6, "AB") OrElse dsAttendance.Tables(0).Rows(i)(mstrDate).ToString = "00:00" OrElse dsAttendance.Tables(0).Rows(i)(mstrDate).ToString = "0:00" OrElse dsAttendance.Tables(0).Rows(i)(mstrDate).ToString = "") Then
                                'S.SANDEEP |29-MAR-2019| -- START
                                'ENHANCEMENT : 0003634 {PAPAYE}.

                                ''Pinkal (10-Jun-2017) -- Start
                                ''Enhancement - Solved Bug for AKIBA  AND FROM NOW Absent will be Calculate manually Not From Query.
                                'If dsAttendance.Tables(0).Rows(i)(mstrDate).ToString = Language.getMessage(mstrModuleName, 6, "AB") Then
                                '    If IsDBNull(dsAttendance.Tables(0).Rows(i)("TotAbsent")) Then dsAttendance.Tables(0).Rows(i)("TotAbsent") = 0
                                '    dsAttendance.Tables(0).Rows(i)("TotAbsent") = CInt(dsAttendance.Tables(0).Rows(i)("TotAbsent")) - 1
                                'End If
                                ''Pinkal (10-Jun-2017) -- End

                                'dsAttendance.Tables(0).Rows(i)(mstrDate) = Language.getMessage(mstrModuleName, 7, "HL")

                                If strLegendCode = "0" Then
                                    If dsAttendance.Tables(0).Rows(i)(mstrDate).ToString = Language.getMessage(mstrModuleName, 6, "AB") Then
                                        If IsDBNull(dsAttendance.Tables(0).Rows(i)("TotAbsent")) Then dsAttendance.Tables(0).Rows(i)("TotAbsent") = 0
                                        dsAttendance.Tables(0).Rows(i)("TotAbsent") = CInt(dsAttendance.Tables(0).Rows(i)("TotAbsent")) - 1
                                    End If
                                    dsAttendance.Tables(0).Rows(i)(mstrDate) = Language.getMessage(mstrModuleName, 7, "HL")

                                    'Pinkal (16-Apr-2019) -- Start
                                    'Defect [0003748]: Legend filter not working on leave type legends.

                                    'ElseIf strLegendCode = "HL" Then
                                ElseIf strLegendCode.StartsWith("hl_") Then

                                    If dsAttendance.Tables(0).Rows(i)(mstrDate).ToString = Language.getMessage(mstrModuleName, 6, "AB") Then
                                        If IsDBNull(dsAttendance.Tables(0).Rows(i)("TotAbsent")) Then dsAttendance.Tables(0).Rows(i)("TotAbsent") = 0
                                        dsAttendance.Tables(0).Rows(i)("TotAbsent") = CInt(dsAttendance.Tables(0).Rows(i)("TotAbsent")) - 1
                                    End If
                                    dsAttendance.Tables(0).Rows(i)(mstrDate) = Language.getMessage(mstrModuleName, 7, "HL")

                                ElseIf strLegendCode = "AB" Then
                                    If dsAttendance.Tables(0).Rows(i)(mstrDate).ToString = Language.getMessage(mstrModuleName, 6, "AB") Then
                                        If IsDBNull(dsAttendance.Tables(0).Rows(i)("TotAbsent")) Then dsAttendance.Tables(0).Rows(i)("TotAbsent") = 0
                                        dsAttendance.Tables(0).Rows(i)("TotAbsent") = CInt(dsAttendance.Tables(0).Rows(i)("TotAbsent")) - 1
                                    End If
                                    dsAttendance.Tables(0).Rows(i)(mstrDate) = ""

                                ElseIf strLegendCode = "WE" Then
                                    If IsDBNull(dsAttendance.Tables(0).Rows(i)("TotAbsent")) Then dsAttendance.Tables(0).Rows(i)("TotAbsent") = 0
                                    dsAttendance.Tables(0).Rows(i)("TotAbsent") = CInt(dsAttendance.Tables(0).Rows(i)("TotAbsent")) - 1
                                    dsAttendance.Tables(0).Rows(i)(mstrDate) = ""
                                    'Pinkal (16-Apr-2019) -- End

                                End If
                                'S.SANDEEP |29-MAR-2019| -- END


                            End If
                        End If

                        'FOR WEEKEND

                        'Pinkal (22-Nov-2018) -- Start
                        'BUG - Solved Bug For Papaaye for fetching wrong shift.
                        'objShiftTran.GetShiftTran(objshift._Shiftunkid)
                        Dim xshiftunkid As Integer = objEmpShiftTran.GetEmployee_Current_ShiftId(eZeeDate.convertDate(mstrDate).Date, CInt(dsAttendance.Tables(0).Rows(i)("EmployeeID")))
                        objShiftTran.GetShiftTran(xshiftunkid)
                        'Pinkal (22-Nov-2018) -- End
                        Dim drWeekend() As DataRow = objShiftTran._dtShiftday.Select("dayid = " & GetWeekDayNumber(WeekdayName(Weekday(eZeeDate.convertDate(dsAttendance.Tables(0).Columns(mstrDate).ToString).Date), False, FirstDayOfWeek.Sunday).ToString()) & " AND isweekend = 1")

                        If drWeekend.Length > 0 AndAlso dsAttendance.Tables(0).Rows(i)(mstrDate).ToString <> Language.getMessage(mstrModuleName, 5, "PR") Then

                            'S.SANDEEP |29-MAR-2019| -- START
                            'ENHANCEMENT : 0003634 {PAPAYE}.
                            ''Pinkal (10-Jun-2017) -- Start
                            ''Enhancement - Solved Bug for AKIBA  AND FROM NOW Absent will be Calculate manually Not From Query.
                            'If dsAttendance.Tables(0).Rows(i)(mstrDate).ToString = Language.getMessage(mstrModuleName, 6, "AB") Then
                            '    If IsDBNull(dsAttendance.Tables(0).Rows(i)("TotAbsent")) Then dsAttendance.Tables(0).Rows(i)("TotAbsent") = 0
                            '    dsAttendance.Tables(0).Rows(i)("TotAbsent") = CInt(dsAttendance.Tables(0).Rows(i)("TotAbsent")) - 1
                            'End If
                            ''Pinkal (10-Jun-2017) -- End

                            'dsAttendance.Tables(0).Rows(i)(mstrDate) = Language.getMessage(mstrModuleName, 8, "WE")

                            If strLegendCode = "0" Then
                                If dsAttendance.Tables(0).Rows(i)(mstrDate).ToString = Language.getMessage(mstrModuleName, 6, "AB") Then
                                    If IsDBNull(dsAttendance.Tables(0).Rows(i)("TotAbsent")) Then dsAttendance.Tables(0).Rows(i)("TotAbsent") = 0
                                    dsAttendance.Tables(0).Rows(i)("TotAbsent") = CInt(dsAttendance.Tables(0).Rows(i)("TotAbsent")) - 1
                                End If
                                dsAttendance.Tables(0).Rows(i)(mstrDate) = Language.getMessage(mstrModuleName, 8, "WE")
                            ElseIf strLegendCode = "WE" Then
                                If dsAttendance.Tables(0).Rows(i)(mstrDate).ToString = Language.getMessage(mstrModuleName, 6, "AB") Then
                                    If IsDBNull(dsAttendance.Tables(0).Rows(i)("TotAbsent")) Then dsAttendance.Tables(0).Rows(i)("TotAbsent") = 0
                                    dsAttendance.Tables(0).Rows(i)("TotAbsent") = CInt(dsAttendance.Tables(0).Rows(i)("TotAbsent")) - 1
                                End If
                                dsAttendance.Tables(0).Rows(i)(mstrDate) = Language.getMessage(mstrModuleName, 8, "WE")

                                'Pinkal (16-Apr-2019) -- Start
                                'Defect [0003748]: Legend filter not working on leave type legends.
                            ElseIf strLegendCode = "AB" Then
                                If dsAttendance.Tables(0).Rows(i)(mstrDate).ToString = Language.getMessage(mstrModuleName, 6, "AB") Then
                                    If IsDBNull(dsAttendance.Tables(0).Rows(i)("TotAbsent")) Then dsAttendance.Tables(0).Rows(i)("TotAbsent") = 0
                                    dsAttendance.Tables(0).Rows(i)("TotAbsent") = CInt(dsAttendance.Tables(0).Rows(i)("TotAbsent")) - 1
                                End If
                                dsAttendance.Tables(0).Rows(i)(mstrDate) = ""

                            ElseIf strLegendCode.StartsWith("hl_") Then
                                If IsDBNull(dsAttendance.Tables(0).Rows(i)("TotAbsent")) Then dsAttendance.Tables(0).Rows(i)("TotAbsent") = 0
                                dsAttendance.Tables(0).Rows(i)("TotAbsent") = CInt(dsAttendance.Tables(0).Rows(i)("TotAbsent")) - 1
                                dsAttendance.Tables(0).Rows(i)(mstrDate) = ""

                            ElseIf strLegendCode = "OFF" Then
                                If IsDBNull(dsAttendance.Tables(0).Rows(i)("TotAbsent")) Then dsAttendance.Tables(0).Rows(i)("TotAbsent") = 0
                                dsAttendance.Tables(0).Rows(i)("TotAbsent") = CInt(dsAttendance.Tables(0).Rows(i)("TotAbsent")) - 1
                                dsAttendance.Tables(0).Rows(i)(mstrDate) = ""

                            ElseIf strLegendCode = "***" Then
                                If dsAttendance.Tables(0).Rows(i)(mstrDate).ToString = Language.getMessage(mstrModuleName, 5, "PR") Then
                                    dsAttendance.Tables(0).Rows(i)("TotPresent") = CInt(dsAttendance.Tables(0).Rows(i)("TotPresent")) - 1
                                End If
                                If dsAttendance.Tables(0).Rows(i)(mstrDate).ToString = Language.getMessage(mstrModuleName, 6, "AB") Then
                                    If IsDBNull(dsAttendance.Tables(0).Rows(i)("TotAbsent")) Then dsAttendance.Tables(0).Rows(i)("TotAbsent") = 0
                                    dsAttendance.Tables(0).Rows(i)("TotAbsent") = CInt(dsAttendance.Tables(0).Rows(i)("TotAbsent")) - 1
                                End If
                                dsAttendance.Tables(0).Rows(i)(mstrDate) = ""


                                'Pinkal (16-Apr-2019) -- End
                            End If

                            'S.SANDEEP |29-MAR-2019| -- END

                        End If

                        'FOR DAYOFF
                        If dsAttendance.Tables(0).Rows(i)(mstrDate).ToString <> Language.getMessage(mstrModuleName, 5, "PR") OrElse dsAttendance.Tables(0).Rows(i)(mstrDate).ToString <> Language.getMessage(mstrModuleName, 8, "WE") Then
                            'Pinkal (17-Jan-2017) -- Start
                            'Enhancement - Working on DAYOFF Error given by AMOS. 
                            'Dim dsDayOFF As DataSet = objDayOFF.GetList("List", "", CInt(dsAttendance.Tables(0).Rows(i)("EmployeeID")), eZeeDate.convertDate(mstrDate), eZeeDate.convertDate(mstrDate))
                            Dim dsDayOFF As DataSet = objDayOFF.GetList("List", strDatabaseName, mintUserUnkid, intYearUnkid, intCompanyUnkid, strUserModeSetting, dtPeriodEnd.Date, "", CInt(dsAttendance.Tables(0).Rows(i)("EmployeeID")), eZeeDate.convertDate(mstrDate), eZeeDate.convertDate(mstrDate))
                            'Pinkal (17-Jan-2017) -- End

                            If dsDayOFF IsNot Nothing AndAlso dsDayOFF.Tables(0).Rows.Count > 0 Then

                                'S.SANDEEP |29-MAR-2019| -- START
                                'ENHANCEMENT : 0003634 {PAPAYE}.

                                ''Pinkal (10-Jun-2017) -- Start
                                ''Enhancement - Solved Bug for AKIBA  AND FROM NOW Absent will be Calculate manually Not From Query.
                                'If dsAttendance.Tables(0).Rows(i)(mstrDate).ToString = Language.getMessage(mstrModuleName, 6, "AB") Then
                                '    If IsDBNull(dsAttendance.Tables(0).Rows(i)("TotAbsent")) Then dsAttendance.Tables(0).Rows(i)("TotAbsent") = 0
                                '    dsAttendance.Tables(0).Rows(i)("TotAbsent") = CInt(dsAttendance.Tables(0).Rows(i)("TotAbsent")) - 1
                                'End If
                                ''Pinkal (10-Jun-2017) -- End
                                'dsAttendance.Tables(0).Rows(i)(mstrDate) = Language.getMessage(mstrModuleName, 29, "OFF")

                                If strLegendCode = "0" Then
                                    If dsAttendance.Tables(0).Rows(i)(mstrDate).ToString = Language.getMessage(mstrModuleName, 6, "AB") Then
                                        If IsDBNull(dsAttendance.Tables(0).Rows(i)("TotAbsent")) Then dsAttendance.Tables(0).Rows(i)("TotAbsent") = 0
                                        dsAttendance.Tables(0).Rows(i)("TotAbsent") = CInt(dsAttendance.Tables(0).Rows(i)("TotAbsent")) - 1
                                    End If
                                    dsAttendance.Tables(0).Rows(i)(mstrDate) = Language.getMessage(mstrModuleName, 29, "OFF")
                                ElseIf strLegendCode = "OFF" Then
                                    If dsAttendance.Tables(0).Rows(i)(mstrDate).ToString = Language.getMessage(mstrModuleName, 6, "AB") Then
                                        If IsDBNull(dsAttendance.Tables(0).Rows(i)("TotAbsent")) Then dsAttendance.Tables(0).Rows(i)("TotAbsent") = 0
                                        dsAttendance.Tables(0).Rows(i)("TotAbsent") = CInt(dsAttendance.Tables(0).Rows(i)("TotAbsent")) - 1
                                    End If
                                    dsAttendance.Tables(0).Rows(i)(mstrDate) = Language.getMessage(mstrModuleName, 29, "OFF")

                                    'Pinkal (16-Apr-2019) -- Start
                                    'Defect [0003748]: Legend filter not working on leave type legends.

                                ElseIf strLegendCode = "AB" Then
                                    If dsAttendance.Tables(0).Rows(i)(mstrDate).ToString = Language.getMessage(mstrModuleName, 6, "AB") Then
                                        If IsDBNull(dsAttendance.Tables(0).Rows(i)("TotAbsent")) Then dsAttendance.Tables(0).Rows(i)("TotAbsent") = 0
                                        dsAttendance.Tables(0).Rows(i)("TotAbsent") = CInt(dsAttendance.Tables(0).Rows(i)("TotAbsent")) - 1
                                    End If
                                    dsAttendance.Tables(0).Rows(i)(mstrDate) = ""

                                ElseIf strLegendCode.StartsWith("hl_") Then
                                    If IsDBNull(dsAttendance.Tables(0).Rows(i)("TotAbsent")) Then dsAttendance.Tables(0).Rows(i)("TotAbsent") = 0
                                    dsAttendance.Tables(0).Rows(i)("TotAbsent") = CInt(dsAttendance.Tables(0).Rows(i)("TotAbsent")) - 1
                                    dsAttendance.Tables(0).Rows(i)(mstrDate) = ""

                                ElseIf strLegendCode = "WE" Then
                                    If IsDBNull(dsAttendance.Tables(0).Rows(i)("TotAbsent")) Then dsAttendance.Tables(0).Rows(i)("TotAbsent") = 0
                                    dsAttendance.Tables(0).Rows(i)("TotAbsent") = CInt(dsAttendance.Tables(0).Rows(i)("TotAbsent")) - 1
                                    dsAttendance.Tables(0).Rows(i)(mstrDate) = ""

                                ElseIf strLegendCode = "***" Then
                                    If dsAttendance.Tables(0).Rows(i)(mstrDate).ToString = Language.getMessage(mstrModuleName, 5, "PR") Then
                                        dsAttendance.Tables(0).Rows(i)("TotPresent") = CInt(dsAttendance.Tables(0).Rows(i)("TotPresent")) - 1
                                    End If
                                    If dsAttendance.Tables(0).Rows(i)(mstrDate).ToString = Language.getMessage(mstrModuleName, 6, "AB") Then
                                        If IsDBNull(dsAttendance.Tables(0).Rows(i)("TotAbsent")) Then dsAttendance.Tables(0).Rows(i)("TotAbsent") = 0
                                        dsAttendance.Tables(0).Rows(i)("TotAbsent") = CInt(dsAttendance.Tables(0).Rows(i)("TotAbsent")) - 1
                                    End If
                                    dsAttendance.Tables(0).Rows(i)(mstrDate) = ""

                                    'Pinkal (16-Apr-2019) -- End

                                End If
                                'S.SANDEEP |29-MAR-2019| -- END



                            End If
                            dsDayOFF.Clear()
                            dsDayOFF = Nothing
                        End If

                        'FOR MISPUNCH
                        objLogin._Employeeunkid = CInt(dsAttendance.Tables(0).Rows(i)("EmployeeID"))
                        objLogin._Logindate = eZeeDate.convertDate(mstrDate)
                        Dim drLogin() As DataRow = objLogin.GetList("List", True).Tables(0).Select("checkintime IS NULL or checkouttime IS NULL")
                        If drLogin.Length > 0 Then

                            'S.SANDEEP |29-MAR-2019| -- START
                            'ENHANCEMENT : 0003634 {PAPAYE}.

                            ''Pinkal (10-Jun-2017) -- Start
                            ''Enhancement - Solved Bug for AKIBA  AND FROM NOW Absent will be Calculate manually Not From Query.
                            'If dsAttendance.Tables(0).Rows(i)(mstrDate).ToString = Language.getMessage(mstrModuleName, 5, "PR") Then
                            '    dsAttendance.Tables(0).Rows(i)("TotPresent") = CInt(dsAttendance.Tables(0).Rows(i)("TotPresent")) - 1
                            'End If

                            'If dsAttendance.Tables(0).Rows(i)(mstrDate).ToString = Language.getMessage(mstrModuleName, 6, "AB") Then
                            '    If IsDBNull(dsAttendance.Tables(0).Rows(i)("TotAbsent")) Then dsAttendance.Tables(0).Rows(i)("TotAbsent") = 0
                            '    dsAttendance.Tables(0).Rows(i)("TotAbsent") = CInt(dsAttendance.Tables(0).Rows(i)("TotAbsent")) - 1
                            'End If
                            ''Pinkal (10-Jun-2017) -- End

                            'dsAttendance.Tables(0).Rows(i)(mstrDate) = "***"

                            If strLegendCode = "0" Then
                                If dsAttendance.Tables(0).Rows(i)(mstrDate).ToString = Language.getMessage(mstrModuleName, 6, "AB") Then
                                    If IsDBNull(dsAttendance.Tables(0).Rows(i)("TotAbsent")) Then dsAttendance.Tables(0).Rows(i)("TotAbsent") = 0
                                    dsAttendance.Tables(0).Rows(i)("TotAbsent") = CInt(dsAttendance.Tables(0).Rows(i)("TotAbsent")) - 1
                                End If
                                'Pinkal (22-Dec-2022) -- Start
                                ' (A1X-351) TRA - Daily attendance report changes to include new columns and hide other columns.
                                If mstrGroup.Trim.ToUpper() <> "TANZANIA REVENUE AUTHORITY" Then
                                    If dsAttendance.Tables(0).Rows(i)(mstrDate).ToString = Language.getMessage(mstrModuleName, 5, "PR") Then
                                        dsAttendance.Tables(0).Rows(i)("TotPresent") = CInt(dsAttendance.Tables(0).Rows(i)("TotPresent")) - 1
                                    End If
                                    dsAttendance.Tables(0).Rows(i)(mstrDate) = "***"
                                End If
                                'Pinkal (22-Dec-2022) -- End
                            ElseIf strLegendCode = "***" Then
                                If dsAttendance.Tables(0).Rows(i)(mstrDate).ToString = Language.getMessage(mstrModuleName, 6, "AB") Then
                                    If IsDBNull(dsAttendance.Tables(0).Rows(i)("TotAbsent")) Then dsAttendance.Tables(0).Rows(i)("TotAbsent") = 0
                                    dsAttendance.Tables(0).Rows(i)("TotAbsent") = CInt(dsAttendance.Tables(0).Rows(i)("TotAbsent")) - 1
                                End If
                                'Pinkal (22-Dec-2022) -- Start
                                ' (A1X-351) TRA - Daily attendance report changes to include new columns and hide other columns.
                                If mstrGroup.Trim.ToUpper() <> "TANZANIA REVENUE AUTHORITY" Then
                                    If dsAttendance.Tables(0).Rows(i)(mstrDate).ToString = Language.getMessage(mstrModuleName, 5, "PR") Then
                                        dsAttendance.Tables(0).Rows(i)("TotPresent") = CInt(dsAttendance.Tables(0).Rows(i)("TotPresent")) - 1
                                    End If
                                    dsAttendance.Tables(0).Rows(i)(mstrDate) = "***"
                                End If
                                'Pinkal (22-Dec-2022) -- End

                                'Pinkal (16-Apr-2019) -- Start
                                'Defect [0003748]: Legend filter not working on leave type legends.

                            ElseIf strLegendCode = "AB" Then
                                If dsAttendance.Tables(0).Rows(i)(mstrDate).ToString = Language.getMessage(mstrModuleName, 6, "AB") Then
                                    If IsDBNull(dsAttendance.Tables(0).Rows(i)("TotAbsent")) Then dsAttendance.Tables(0).Rows(i)("TotAbsent") = 0
                                    dsAttendance.Tables(0).Rows(i)("TotAbsent") = CInt(dsAttendance.Tables(0).Rows(i)("TotAbsent")) - 1
                                End If
                                dsAttendance.Tables(0).Rows(i)(mstrDate) = ""

                            ElseIf strLegendCode.StartsWith("hl_") Then
                                If IsDBNull(dsAttendance.Tables(0).Rows(i)("TotAbsent")) Then dsAttendance.Tables(0).Rows(i)("TotAbsent") = 0
                                dsAttendance.Tables(0).Rows(i)("TotAbsent") = CInt(dsAttendance.Tables(0).Rows(i)("TotAbsent")) - 1
                                dsAttendance.Tables(0).Rows(i)(mstrDate) = ""

                            ElseIf strLegendCode = "WE" Then
                                If IsDBNull(dsAttendance.Tables(0).Rows(i)("TotAbsent")) Then dsAttendance.Tables(0).Rows(i)("TotAbsent") = 0
                                dsAttendance.Tables(0).Rows(i)("TotAbsent") = CInt(dsAttendance.Tables(0).Rows(i)("TotAbsent")) - 1
                                dsAttendance.Tables(0).Rows(i)(mstrDate) = ""

                            ElseIf strLegendCode = "OFF" Then
                                If IsDBNull(dsAttendance.Tables(0).Rows(i)("TotAbsent")) Then dsAttendance.Tables(0).Rows(i)("TotAbsent") = 0
                                dsAttendance.Tables(0).Rows(i)("TotAbsent") = CInt(dsAttendance.Tables(0).Rows(i)("TotAbsent")) - 1
                                dsAttendance.Tables(0).Rows(i)(mstrDate) = ""

                                'Pinkal (16-Apr-2019) -- End

                            End If
                            'S.SANDEEP |29-MAR-2019| -- END


                        End If
                        drLogin = Nothing

                    End If
                Next
            Next

            'Pinkal (10-Jun-2017) -- Start
            'Enhancement - Solved Bug for AKIBA  AND FROM NOW Absent will be Calculate manually Not From Query.
            dsAttendance.AcceptChanges()
            'Pinkal (10-Jun-2017) -- End

            'Pinkal (22-Nov-2018) -- Start
            'BUG - Solved Bug For Papaaye for fetching wrong shift.
            ' objshift = Nothing
            objEmpShiftTran = Nothing
            'Pinkal (22-Nov-2018) -- End

            objShiftTran = Nothing
            objholiday = Nothing
            objLogin = Nothing
            objDayOFF = Nothing

            'FOR  LEAVE
            Dim dsLeave As DataSet = Nothing
            StrQ = " SELECT lvleaveIssue_master.employeeunkid,isnull(hremployee_master.firstname, '') + ISNULL(hremployee_master.surname,'') 'employee'  " & _
                      ", convert(VARCHAR(8),leavedate,112) leavedate,lvleavetype_master.leavetypeunkid,lvleavetype_master.leavename,lvleavetype_master.leavetypecode,lvleavetype_master.color " & _
                      ", ISNULL(lvleaveIssue_tran.dayfraction,0.00) AS dayfraction "

            If mintViewIndex > 0 Then
                StrQ &= mstrAnalysis_Fields
            Else
                StrQ &= ", 0 AS Id, '' AS GName "
            End If

            StrQ &= " FROM lvleaveIssue_tran " & _
                      " LEFT JOIN lvleaveIssue_master ON lvleaveIssue_tran.leaveissueunkid = lvleaveIssue_master.leaveissueunkid " & _
                      " LEFT JOIN hremployee_master ON hremployee_master.employeeunkid = lvleaveIssue_master.employeeunkid " & _
                      " JOIN lvleavetype_master ON dbo.lvleaveIssue_master.leavetypeunkid = lvleavetype_master.leavetypeunkid "


            'Anjan [08 September 2015] -- Start
            'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
            If mintViewIndex > 0 Then
                StrQ &= mstrAnalysis_Join
            End If

            If xDateJoinQry.Trim.Length > 0 Then
                StrQ &= xDateJoinQry
            End If

            If xUACQry.Trim.Length > 0 Then
                StrQ &= xUACQry
            End If
            If xAdvanceJoinQry.Trim.Length > 0 Then
                StrQ &= xAdvanceJoinQry
            End If
            'Anjan [08 September 2015] -- End


            StrQ &= " WHERE lvleaveIssue_tran.isvoid = 0 AND lvleaveIssue_master.isvoid = 0 AND CONVERT(VARCHAR(8),leavedate,112) BETWEEN @startdate and @enddate"

            If mstrAdvance_Filter.Trim.Length > 0 Then
                StrQ &= " AND " & mstrAdvance_Filter & " "
            End If


            'Anjan [08 September 2015] -- Start
            'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
            'If mblnIncludeInactive = False Then
            '    StrQ &= " AND CONVERT(CHAR(8),hremployee_master.appointeddate,112) <= @enddate " & _
            '                 " AND ISNULL(CONVERT(CHAR(8),hremployee_master.termination_from_date,112),@startdate) >= @startdate " & _
            '                 " AND ISNULL(CONVERT(CHAR(8),hremployee_master.termination_to_date,112),@startdate) >= @startdate " & _
            '                 " AND ISNULL(CONVERT(CHAR(8), hremployee_master.empl_enddate,112), @startdate) >= @startdate "
            'End If

            'If mstrUserAccessFilter.Trim.Length > 0 Then
            '    StrQ &= mstrUserAccessFilter
            'End If

            If xUACFiltrQry.Trim.Length > 0 Then
                StrQ &= " AND " & xUACFiltrQry & " "
            End If

            If mblnIncludeInactive = False Then
                If xDateFilterQry.Trim.Length > 0 Then
                    StrQ &= xDateFilterQry & " "
                End If
            End If
            'Anjan [08 September 2015] -- End





            objDataOperation.ClearParameters()
            objDataOperation.AddParameter("@startdate", SqlDbType.NVarChar, eZeeDataType.DATETIME_SIZE, eZeeDate.convertDate(mdtFromDate))
            objDataOperation.AddParameter("@enddate", SqlDbType.NVarChar, eZeeDataType.DATETIME_SIZE, eZeeDate.convertDate(mdtToDate))
            If mintEmployeeId > 0 Then
                objDataOperation.AddParameter("@EmpId", SqlDbType.Int, eZeeDataType.INT_SIZE, mintEmployeeId)
                StrQ &= "  AND lvleaveIssue_master.employeeunkid = @EmpId "
            End If

            StrQ &= " ORDER by lvleaveIssue_master.leavetypeunkid "

            dsLeave = objDataOperation.ExecQuery(StrQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                Throw New Exception(objDataOperation.ErrorNumber & ":" & objDataOperation.ErrorMessage)
            End If

            mintEmp = 0
            Dim dRow As DataRow() = Nothing

            For i As Integer = 0 To dsLeave.Tables(0).Rows.Count - 1

                If mintEmp <> CInt(dsLeave.Tables(0).Rows(i)("employeeunkid")) Then
                    dRow = dsAttendance.Tables(0).Select("employeeId=" & CInt(dsLeave.Tables(0).Rows(i)("employeeunkid")))
                    mintEmp = CInt(dsLeave.Tables(0).Rows(i)("employeeunkid"))
                End If

                If dRow IsNot Nothing AndAlso dRow.Length > 0 Then
                    If dRow(0).Table.Columns.Contains(dsLeave.Tables(0).Rows(i)("leavedate")) AndAlso (dRow(0).Table.Columns(dsLeave.Tables(0).Rows(i)("leavedate")).ToString = dsLeave.Tables(0).Rows(i)("leavedate").ToString()) Then


                        'Pinkal (10-Jun-2017) -- Start
                        'Enhancement - Solved Bug for AKIBA  AND FROM NOW Absent will be Calculate manually Not From Query.

                        'Pinkal (14-Jun-2017) -- Start
                        'Enhancement -  Solved Bug for AKIBA .
                        If IsDBNull(dRow(0)(dsLeave.Tables(0).Rows(i)("leavedate"))) Then dRow(0)(dsLeave.Tables(0).Rows(i)("leavedate")) = ""
                        'Pinkal (14-Jun-2017) -- End


                        'S.SANDEEP |29-MAR-2019| -- START
                        'ENHANCEMENT : 0003634 {PAPAYE}.
                        'If dRow(0)(dsLeave.Tables(0).Rows(i)("leavedate")) = Language.getMessage(mstrModuleName, 5, "PR") Then
                        '    dRow(0)("TotPresent") = CInt(dRow(0)("TotPresent")) - 1

                        'ElseIf dRow(0)(dsLeave.Tables(0).Rows(i)("leavedate")) = "***" Then
                        '    dRow(0)("TotMispunch") = CInt(dRow(0)("TotMispunch")) - 1

                        'ElseIf dRow(0)(dsLeave.Tables(0).Rows(i)("leavedate")) = Language.getMessage(mstrModuleName, 6, "AB") Then
                        '    dRow(0)("TotAbsent") = CInt(dRow(0)("TotAbsent")) - 1
                        'End If
                        ''Pinkal (10-Jun-2017) -- End

                        'dRow(0)(dsLeave.Tables(0).Rows(i)("leavedate")) = dsLeave.Tables(0).Rows(i)("leavetypecode").ToString()
                        'If dRow(0)(dsLeave.Tables(0).Rows(i)("Leavetypecode")).ToString = "" Then dRow(0)(dsLeave.Tables(0).Rows(i)("Leavetypecode")) = "0"
                        ''If dRow(0)(dsLeave.Tables(0).Rows(i)("Leavetypecode")).ToString = "0" Then
                        'dRow(0)(dsLeave.Tables(0).Rows(i)("Leavetypecode")) += dsLeave.Tables(0).Rows(i)("dayfraction")
                        'dRow(0)("TotLeave") = CInt(dRow(0)("TotLeave")) + CInt(dsLeave.Tables(0).Rows(i)("dayfraction"))
                        ''End If

                        If strLegendCode = "0" Then
                            If dRow(0)(dsLeave.Tables(0).Rows(i)("leavedate")) = Language.getMessage(mstrModuleName, 5, "PR") Then
                                dRow(0)("TotPresent") = CInt(dRow(0)("TotPresent")) - 1

                            ElseIf dRow(0)(dsLeave.Tables(0).Rows(i)("leavedate")) = "***" Then
                                dRow(0)("TotMispunch") = CInt(dRow(0)("TotMispunch")) - 1

                            ElseIf dRow(0)(dsLeave.Tables(0).Rows(i)("leavedate")) = Language.getMessage(mstrModuleName, 6, "AB") Then
                                dRow(0)("TotAbsent") = CInt(dRow(0)("TotAbsent")) - 1
                            End If
                            dRow(0)(dsLeave.Tables(0).Rows(i)("leavedate")) = dsLeave.Tables(0).Rows(i)("leavetypecode").ToString()
                            'Pinkal (22-Dec-2022) -- Start
                            ' (A1X-351) TRA - Daily attendance report changes to include new columns and hide other columns.
                            If mstrGroup.Trim.ToUpper() <> "TANZANIA REVENUE AUTHORITY" Then
                                If dRow(0)(dsLeave.Tables(0).Rows(i)("Leavetypecode")).ToString = "" Then dRow(0)(dsLeave.Tables(0).Rows(i)("Leavetypecode")) = "0"
                                dRow(0)(dsLeave.Tables(0).Rows(i)("Leavetypecode")) += dsLeave.Tables(0).Rows(i)("dayfraction")
                                dRow(0)("TotLeave") = CInt(dRow(0)("TotLeave")) + CInt(dsLeave.Tables(0).Rows(i)("dayfraction"))
                            End If
                            'Pinkal (22-Dec-2022) -- End


                        ElseIf strLegendCode = "PR" Then
                            'Pinkal (20-Apr-2019) -- Start
                            'Defect - Solved Defect for Papaaye Manaul Attendance Register Report for Advance Filter Issue.
                            If dRow(0)(dsLeave.Tables(0).Rows(i)("leavedate")) = Language.getMessage(mstrModuleName, 5, "PR") Then
                                dRow(0)("TotPresent") = CInt(dRow(0)("TotPresent")) - 1
                                dRow(0)(dsLeave.Tables(0).Rows(i)("leavedate")) = ""
                            End If
                            'Pinkal (20-Apr-2019) -- End
                        ElseIf strLegendCode = "AB" Then
                            'Pinkal (20-Apr-2019) -- Start
                            'Defect - Solved Defect for Papaaye Manaul Attendance Register Report for Advance Filter Issue.
                            If dRow(0)(dsLeave.Tables(0).Rows(i)("leavedate")) = Language.getMessage(mstrModuleName, 6, "AB") Then
                                dRow(0)("TotAbsent") = CInt(dRow(0)("TotAbsent")) - 1
                                dRow(0)(dsLeave.Tables(0).Rows(i)("leavedate")) = ""
                            End If
                            'Pinkal (20-Apr-2019) -- End
                        ElseIf strLegendCode = "***" Then
                            'Pinkal (20-Apr-2019) -- Start
                            'Defect - Solved Defect for Papaaye Manaul Attendance Register Report for Advance Filter Issue.
                            If dRow(0)(dsLeave.Tables(0).Rows(i)("leavedate")) = "***" Then
                                dRow(0)("TotMispunch") = CInt(dRow(0)("TotMispunch")) - 1
                                dRow(0)(dsLeave.Tables(0).Rows(i)("leavedate")) = ""
                            End If
                            'Pinkal (20-Apr-2019) -- End
                        ElseIf strLegendCode.StartsWith("lv_") Then
                            'Pinkal (16-Apr-2019) -- Start
                            'Defect [0003748]: Legend filter not working on leave type legends.
                            If dsAttendance.Tables(0).Columns(dsLeave.Tables(0).Rows(i)("leavetypecode").ToString()).ExtendedProperties.ContainsKey(strLegendCode) Then
                                dRow(0)(dsLeave.Tables(0).Rows(i)("leavedate")) = dsLeave.Tables(0).Rows(i)("leavetypecode").ToString()
                                'Pinkal (22-Dec-2022) -- Start
                                ' (A1X-351) TRA - Daily attendance report changes to include new columns and hide other columns.
                                If mstrGroup.Trim.ToUpper() <> "TANZANIA REVENUE AUTHORITY" Then
                                    If dRow(0)(dsLeave.Tables(0).Rows(i)("Leavetypecode")).ToString = "" Then dRow(0)(dsLeave.Tables(0).Rows(i)("Leavetypecode")) = "0"
                                    dRow(0)(dsLeave.Tables(0).Rows(i)("Leavetypecode")) += dsLeave.Tables(0).Rows(i)("dayfraction")
                                    dRow(0)("TotLeave") = CInt(dRow(0)("TotLeave")) + CInt(dsLeave.Tables(0).Rows(i)("dayfraction"))
                                End If
                                'Pinkal (22-Dec-2022) -- End
                            End If
                            'Pinkal (16-Apr-2019) -- End
                        End If
                        'S.SANDEEP |29-MAR-2019| -- END

                    End If
                End If

            Next



            'S.SANDEEP |29-MAR-2019| -- START
            'ENHANCEMENT : 0003634 {PAPAYE}.
            If strLegendCode <> "0" Then
                Dim drCol = dsAttendance.Tables(0).Columns.Cast(Of DataColumn)().AsEnumerable().Where(Function(x) x.ExtendedProperties("oDt_" & x.ColumnName) <> "")
                Dim dtTable As DataTable = dsAttendance.Tables(0).Clone()
                If drCol IsNot Nothing Then
                    Dim strFilterString As String = String.Empty
                    strFilterString = String.Join("] <> '' OR [", drCol.Cast(Of DataColumn).Select(Function(x) x.ColumnName).ToArray())
                    If strFilterString.Trim.Length > 0 Then
                        strFilterString = "[" & strFilterString & "] <> '' "
                        dtTable = New DataView(dsAttendance.Tables(0), strFilterString, "", DataViewRowState.CurrentRows).ToTable()
                    End If
                End If
                dsAttendance.Tables.RemoveAt(0)
                dsAttendance.Tables.Add(dtTable)
                dsAttendance.Tables(0).AsEnumerable().ToList().ForEach(Function(x) SetColValue(x, "SrNo", dsAttendance.Tables(0)))


                'Pinkal (16-Apr-2019) -- Start
                'Defect [0003748]: Legend filter not working on leave type legends.

                Dim lst As IEnumerable(Of String) = Nothing
                If strLegendCode.Trim.StartsWith("lv_") Then
                    lst = dsAttendance.Tables(0).Columns.Cast(Of DataColumn).Where(Function(x) x.ExtendedProperties.ContainsValue("lv_" & x.ColumnName) AndAlso x.ExtendedProperties.ContainsKey(strLegendCode) = False).Select(Function(x) x.ColumnName)
                Else
                    lst = dsAttendance.Tables(0).Columns.Cast(Of DataColumn).Where(Function(x) x.ExtendedProperties.ContainsValue("lv_" & x.ColumnName)).Select(Function(x) x.ColumnName)
                End If

                Dim mstrLeaveType As String = ""
                If lst IsNot Nothing AndAlso lst.Count > 0 Then
                    mstrLeaveType = String.Join(",", lst.ToArray())
                End If

                If strLegendCode = "PR" Then
                    'Pinkal (22-Dec-2022) -- Start
                    ' (A1X-351) TRA - Daily attendance report changes to include new columns and hide other columns.
                    If mstrGroup.Trim.ToUpper() <> "TANZANIA REVENUE AUTHORITY" Then
                        RemoveColumnForLegendsFilter(dsAttendance, "TotAbsent,TotMispunch,TotLeave" & IIf(mstrLeaveType.Length > 0, ", " & mstrLeaveType, ""))
                    Else
                        RemoveColumnForLegendsFilter(dsAttendance, "TotAbsent")
                    End If
                    'Pinkal (22-Dec-2022) -- End

                ElseIf strLegendCode = "AB" Then
                    'Pinkal (22-Dec-2022) -- Start
                    ' (A1X-351) TRA - Daily attendance report changes to include new columns and hide other columns.
                    If mstrGroup.Trim.ToUpper() <> "TANZANIA REVENUE AUTHORITY" Then
                        RemoveColumnForLegendsFilter(dsAttendance, "TotPresent,TotLeave,TotWorkhrs,TotShorthrs,TotOT1,TotOT2,TotOT3,TotOT4,TotMispunch,TotNightHrs" & IIf(mstrLeaveType.Length > 0, ", " & mstrLeaveType, ""))
                    Else
                        RemoveColumnForLegendsFilter(dsAttendance, "TotPresent,TotWorkhrs,TotOvertimeDays")
                    End If
                    'Pinkal (22-Dec-2022) -- End

                ElseIf strLegendCode = "***" Then
                    'Pinkal (22-Dec-2022) -- Start
                    ' (A1X-351) TRA - Daily attendance report changes to include new columns and hide other columns.
                    If mstrGroup.Trim.ToUpper() <> "TANZANIA REVENUE AUTHORITY" Then
                        RemoveColumnForLegendsFilter(dsAttendance, "TotAbsent,TotPresent,TotLeave,TotWorkhrs,TotShorthrs,TotOT1,TotOT2,TotOT3,TotOT4,TotNightHrs" & IIf(mstrLeaveType.Length > 0, ", " & mstrLeaveType, ""))
                    Else
                        RemoveColumnForLegendsFilter(dsAttendance, "TotAbsent,TotPresent,TotWorkhrs,TotOvertimeDays")
                    End If
                    'Pinkal (22-Dec-2022) -- End

                ElseIf strLegendCode = "WE" OrElse strLegendCode = "OFF" OrElse strLegendCode.Trim.StartsWith("hl_") OrElse strLegendCode.Trim.StartsWith("lv_") Then
                    'Pinkal (22-Dec-2022) -- Start
                    ' (A1X-351) TRA - Daily attendance report changes to include new columns and hide other columns.
                    If mstrGroup.Trim.ToUpper() <> "TANZANIA REVENUE AUTHORITY" Then
                        RemoveColumnForLegendsFilter(dsAttendance, "TotAbsent,TotPresent,TotLeave,TotWorkhrs,TotShorthrs,TotOT1,TotOT2,TotOT3,TotOT4,TotMispunch,TotNightHrs" & IIf(mstrLeaveType.Length > 0, ", " & mstrLeaveType, ""))
                    Else
                        RemoveColumnForLegendsFilter(dsAttendance, "TotAbsent,TotPresent,TotOvertimeDays,TotLeave,TotWorkhrs,TotShorthrs,TotOT1,TotOT2,TotOT3,TotOT4,TotMispunch,TotNightHrs" & IIf(mstrLeaveType.Length > 0, ", " & mstrLeaveType, ""))
                    End If
                    'Pinkal (22-Dec-2022) -- End
                End If

                'Pinkal (16-Apr-2019) -- End

            End If
            'S.SANDEEP |29-MAR-2019| -- END

            'Pinkal (10-Jun-2017) -- Start
            'Enhancement - Solved Bug for AKIBA  AND FROM NOW Absent will be Calculate manually Not From Query.
            dsAttendance.AcceptChanges()
            'Pinkal (10-Jun-2017) -- End

            Dim dtLegends As DataTable = GetLegends()

            Dim strarrGroupColumns As String() = Nothing
            Dim rowsArrayHeader As New ArrayList
            Dim rowsArrayFooter As New ArrayList
            Dim row As WorksheetRow
            Dim wcell As WorksheetCell

            Dim mdtTableExcel As DataTable = dsAttendance.Tables(0)

            If mdtTableExcel.Columns.Contains("EmployeeId") Then
                mdtTableExcel.Columns.Remove("EmployeeId")
            End If

            If mdtTableExcel.Columns.Contains("ShiftId") Then
                mdtTableExcel.Columns.Remove("ShiftId")
            End If

            If mintViewIndex > 0 Then
                If mdtTableExcel.Columns.Contains("Id") Then
                    mdtTableExcel.Columns.Remove("Id")
                End If

                mdtTableExcel.Columns("GName").Caption = mstrReport_GroupName
                Dim strGrpCols As String() = {"GName"}
                strarrGroupColumns = strGrpCols
            End If

            If mblnIgnoreEmptyColumns Then
                mdtTableExcel = IgnoreEmptyColumns(mdtTableExcel)
            End If

            row = New WorksheetRow()

            If Me._FilterTitle.ToString.Length > 0 Then
                wcell = New WorksheetCell(Me._FilterTitle.ToString, "s10bw")
                wcell.MergeAcross = mdtTableExcel.Columns.Count - 1
                row.Cells.Add(wcell)
            End If
            rowsArrayHeader.Add(row)


            row = New WorksheetRow()
            wcell = New WorksheetCell("", "s10bw")
            row.Cells.Add(wcell)
            rowsArrayFooter.Add(row)

            row = New WorksheetRow()
            wcell = New WorksheetCell("", "s10bw")
            row.Cells.Add(wcell)
            rowsArrayFooter.Add(row)


            row = New WorksheetRow()
            wcell = New WorksheetCell(Language.getMessage(mstrModuleName, 14, "Legends :"), "s10bw")
            wcell.MergeAcross = 5
            row.Cells.Add(wcell)
            rowsArrayFooter.Add(row)

            row = New WorksheetRow()
            wcell = New WorksheetCell("", "s10bw")
            wcell.MergeAcross = 5
            row.Cells.Add(wcell)
            rowsArrayFooter.Add(row)


            If dtLegends IsNot Nothing AndAlso dtLegends.Rows.Count > 0 Then

                For i As Integer = 0 To dtLegends.Rows.Count - 1
                    row = New WorksheetRow()
                    wcell = New WorksheetCell(Space(20) & dtLegends.Rows(i)("code").ToString() & " - " & dtLegends.Rows(i)("name").ToString(), "s10bw")
                    wcell.MergeAcross = 5
                    row.Cells.Add(wcell)
                    rowsArrayFooter.Add(row)
                Next

            End If
            '--------------------



            If ConfigParameter._Object._IsShowPreparedBy = True Then
                row = New WorksheetRow()
                wcell = New WorksheetCell(Language.getMessage(mstrModuleName, 9, "Prepared By :") & Space(10) & User._Object._Username, "s8bw")
                wcell.MergeAcross = 4
                row.Cells.Add(wcell)

                wcell = New WorksheetCell("", "s10bw")
                row.Cells.Add(wcell)
                rowsArrayFooter.Add(row)

                row = New WorksheetRow()
                wcell = New WorksheetCell("", "s10bw")
                row.Cells.Add(wcell)
                rowsArrayFooter.Add(row)
            End If



            If ConfigParameter._Object._IsShowCheckedBy = True Then
                row = New WorksheetRow()
                wcell = New WorksheetCell(Language.getMessage(mstrModuleName, 10, "Checked By :"), "s8bw")
                wcell.MergeAcross = 4
                row.Cells.Add(wcell)

                wcell = New WorksheetCell("", "s10bw")
                row.Cells.Add(wcell)
                rowsArrayFooter.Add(row)

                row = New WorksheetRow()
                wcell = New WorksheetCell("", "s10bw")
                row.Cells.Add(wcell)
                rowsArrayFooter.Add(row)
            End If


            If ConfigParameter._Object._IsShowApprovedBy = True Then
                row = New WorksheetRow()
                wcell = New WorksheetCell(Language.getMessage(mstrModuleName, 11, "Approved By"), "s8bw")
                wcell.MergeAcross = 4
                row.Cells.Add(wcell)

                wcell = New WorksheetCell("", "s10bw")
                row.Cells.Add(wcell)
                rowsArrayFooter.Add(row)

                row = New WorksheetRow()
                wcell = New WorksheetCell("", "s10bw")
                row.Cells.Add(wcell)
                rowsArrayFooter.Add(row)
            End If

            If ConfigParameter._Object._IsShowReceivedBy = True Then
                row = New WorksheetRow()
                wcell = New WorksheetCell(Language.getMessage(mstrModuleName, 12, "Received By :"), "s8bw")
                wcell.MergeAcross = 4
                row.Cells.Add(wcell)
                rowsArrayFooter.Add(row)
            End If


            ''SET EXCEL CELL WIDTH  
            Dim intArrayColumnWidth As Integer() = Nothing
            ReDim intArrayColumnWidth(mdtTableExcel.Columns.Count - 1)
            For i As Integer = 0 To intArrayColumnWidth.Length - 1
                If i = 0 Then
                    intArrayColumnWidth(i) = 40

                    'Pinkal (22-Dec-2022) -- Start
                    ' (A1X-351) TRA - Daily attendance report changes to include new columns and hide other columns.
                ElseIf i <= 2 AndAlso mstrGroup.Trim.ToUpper() <> "TANZANIA REVENUE AUTHORITY" Then
                    intArrayColumnWidth(i) = 160
                ElseIf i > 2 AndAlso i <= DateDiff(DateInterval.Day, mdtFromDate, mdtToDate.AddDays(1)) + 2 AndAlso mstrGroup.Trim.ToUpper() <> "TANZANIA REVENUE AUTHORITY" Then
                    intArrayColumnWidth(i) = 30
                ElseIf i > DateDiff(DateInterval.Day, mdtFromDate, mdtToDate.AddDays(1)) + 2 AndAlso i <= (DateDiff(DateInterval.Day, mdtFromDate, mdtToDate.AddDays(1)) + 2 + mintLeaveTypeCount) AndAlso mstrGroup.Trim.ToUpper() <> "TANZANIA REVENUE AUTHORITY" Then
                    intArrayColumnWidth(i) = 50

                ElseIf i <= 4 AndAlso mstrGroup.Trim.ToUpper() = "TANZANIA REVENUE AUTHORITY" Then
                    intArrayColumnWidth(i) = 160
                ElseIf i > 4 AndAlso i <= DateDiff(DateInterval.Day, mdtFromDate, mdtToDate.AddDays(1)) + 4 AndAlso mstrGroup.Trim.ToUpper() = "TANZANIA REVENUE AUTHORITY" Then
                    intArrayColumnWidth(i) = 30
                ElseIf i > DateDiff(DateInterval.Day, mdtFromDate, mdtToDate.AddDays(1)) + 4 AndAlso i <= (DateDiff(DateInterval.Day, mdtFromDate, mdtToDate.AddDays(1)) + 4 + mintLeaveTypeCount) AndAlso mstrGroup.Trim.ToUpper() = "TANZANIA REVENUE AUTHORITY" Then
                    intArrayColumnWidth(i) = 50
                    'Pinkal (22-Dec-2022) -- End
                Else
                    intArrayColumnWidth(i) = 90
                End If
            Next
            'SET EXCEL CELL WIDTH

            Call ReportExecute(Nothing, enPrintAction.None, enExportAction.ExcelExtra, mstrExportReportPath, mblnOpenAfterExport, mdtTableExcel, intArrayColumnWidth, True, True, False, strarrGroupColumns, Me._ReportName, "", " ", , "", False, rowsArrayHeader, rowsArrayFooter, Nothing, Nothing, False)

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: Generate_DailyAttendanceReport; Module Name: " & mstrModuleName)
        End Try
    End Sub

    Public Function GenerateDays() As DataSet
        Dim StrQ As String = String.Empty
        Dim dsList As DataSet = Nothing
        Try
            Dim PeriodDiff As Integer = DateDiff(DateInterval.Day, mdtFromDate, mdtToDate.AddDays(1))
            objDataOperation = New clsDataOperation

            StrQ = " DECLARE @myTable TABLE " & _
                   " ( " & _
                   "    PeriodMonth nvarchar(20) " & _
                   ",   PeriodDate DateTime " & _
                   ",   PeriodDay nvarchar(20) " & _
                   " ) " & _
                    " declare @StartDate datetime " & _
                    " declare @Days int " & _
                    " declare @CurrentDay int " & _
                    " set @StartDate = @start_date" & _
                    " set @Days = @PeriodDiff " & _
                    " set @CurrentDay = 0 " & _
                    " while @CurrentDay < @Days " & _
                    "   begin " & _
                    "        insert @myTable (PeriodMonth,PeriodDate,PeriodDay) values (Convert(char(3), DATENAME(MONTH,dateadd(dd, @CurrentDay, @StartDate))), dateadd(dd, @CurrentDay, @StartDate),SUBSTRING(DATEname(dw,dateadd(dd, @CurrentDay, @StartDate)),0,4)) " & _
                    "        set @CurrentDay = @CurrentDay + 1 " & _
                    "   End " & _
                    " select PeriodMonth,PeriodDate,PeriodDay from @myTable "


            objDataOperation.ClearParameters()
            objDataOperation.AddParameter("@PeriodDiff", SqlDbType.Int, eZeeDataType.INT_SIZE, PeriodDiff)
            objDataOperation.AddParameter("@start_date", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, mdtFromDate.Date)
            dsList = objDataOperation.ExecQuery(StrQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                Throw New Exception(objDataOperation.ErrorNumber & ":" & objDataOperation.ErrorMessage)
            End If

            Return dsList
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: GenerateDays; Module Name: " & mstrModuleName)
        End Try
        Return Nothing
    End Function

    'S.SANDEEP |29-MAR-2019| -- START
    'ENHANCEMENT : 0003634 {PAPAYE}.
    Private Function SetColValue(ByVal dr As DataRow, ByVal strColName As String, Optional ByVal dtTable As DataTable = Nothing) As Boolean
        If strColName.Trim.Length > 0 Then
            Select Case strColName.ToUpper
                Case "UDISPLAY"
                    dr("uDisplay") = dr("Code").ToString() & " - " & dr("Name").ToString()
                Case "SRNO"
                    dr("SrNo") = dtTable.Rows.IndexOf(dr) + 1
            End Select
        End If
        Return True
    End Function
    'S.SANDEEP |29-MAR-2019| -- END

    Public Function GetLegends() As DataTable
        Dim StrQ As String = String.Empty
        Dim dsList As DataSet = Nothing

        Try
            objDataOperation = New clsDataOperation

            'S.SANDEEP |29-MAR-2019| -- START
            'ENHANCEMENT : 0003634 {PAPAYE}.
            'StrQ = " SELECT leavetypecode Code , leaveName [Name],Convert(varchar(10),Color) Color, 0 as Holiday FROM lvleavetype_master " & _
            '       " UNION " & _
            '       " SELECT '' AS Code , holidayname [Name],Convert(varchar(10),Color) Color, 1 as Holiday FROM lvholiday_master  " & _
            '       " WHERE Convert(varchar(10),holidaydate,112) BETWEEN @startdate AND @enddate "

            StrQ = "SELECT " & _
                   "     leavetypecode AS Code " & _
                   "    ,leaveName AS [Name] " & _
                   "    ,Convert(varchar(10),Color) AS Color " & _
                   "    ,0 AS Holiday " & _
                   "    ,'lv_' + CAST(leavetypeunkid AS NVARCHAR(MAX)) AS uCode " & _
                   "FROM lvleavetype_master " & _
                   "UNION " & _
                   "SELECT " & _
                   "     '' AS Code " & _
                   "    ,holidayname AS [Name] " & _
                   "    ,Convert(varchar(10),Color) Color " & _
                   "    ,1 as Holiday " & _
                   "    ,'hl_' + CAST(holidayunkid AS NVARCHAR(MAX)) AS uCode " & _
                   "FROM lvholiday_master  " & _
                   "WHERE CONVERT(CHAR(8),holidaydate,112) BETWEEN @startdate AND @enddate "
            'S.SANDEEP |29-MAR-2019| -- END

            objDataOperation.ClearParameters()
            objDataOperation.AddParameter("@startdate", SqlDbType.NVarChar, eZeeDataType.DATETIME_SIZE, eZeeDate.convertDate(mdtFromDate))
            objDataOperation.AddParameter("@enddate", SqlDbType.NVarChar, eZeeDataType.DATETIME_SIZE, eZeeDate.convertDate(mdtToDate))
            dsList = objDataOperation.ExecQuery(StrQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                Throw New Exception(objDataOperation.ErrorNumber & ":" & objDataOperation.ErrorMessage)
            End If

            'S.SANDEEP |29-MAR-2019| -- START
            'ENHANCEMENT : 0003634 {PAPAYE}.
            dsList.Tables(0).Columns.Add("uDisplay", GetType(System.String)).DefaultValue = ""
            'S.SANDEEP |29-MAR-2019| -- END


            'Pinkal (27-Apr-2021)-- Start
            'KBC Enhancement  -  Working on Claim Retirement Enhancement.
            'If dsList IsNot Nothing AndAlso dsList.Tables(0).Rows.Count > 0 Then
            If dsList IsNot Nothing Then
                'Pinkal (27-Apr-2021) -- End


                Dim dRow() As DataRow = dsList.Tables(0).Select("Holiday = 1")
                If dRow.Length > 0 Then
                    For i As Integer = 0 To dRow.Length - 1
                        dRow(i)("Code") = Language.getMessage(mstrModuleName, 7, "HL")
                        dRow(i).AcceptChanges()
                    Next
                End If

                Dim DrRow As DataRow = dsList.Tables(0).NewRow

                DrRow("Code") = Language.getMessage(mstrModuleName, 5, "PR")
                DrRow("Name") = Language.getMessage(mstrModuleName, 15, "Present")
                DrRow("Color") = ""
                'S.SANDEEP |29-MAR-2019| -- START
                'ENHANCEMENT : 0003634 {PAPAYE}.
                DrRow("uCode") = "PR"
                'S.SANDEEP |29-MAR-2019| -- END
                dsList.Tables(0).Rows.InsertAt(DrRow, 0)

                DrRow = dsList.Tables(0).NewRow
                DrRow("Code") = Language.getMessage(mstrModuleName, 6, "AB")
                DrRow("Name") = Language.getMessage(mstrModuleName, 16, "Absent")
                DrRow("Color") = ""
                'S.SANDEEP |29-MAR-2019| -- START
                'ENHANCEMENT : 0003634 {PAPAYE}.
                DrRow("uCode") = "AB"
                'S.SANDEEP |29-MAR-2019| -- END
                dsList.Tables(0).Rows.InsertAt(DrRow, 1)

                DrRow = dsList.Tables(0).NewRow
                DrRow("Code") = Language.getMessage(mstrModuleName, 8, "WE")
                DrRow("Name") = Language.getMessage(mstrModuleName, 13, "Weekend")
                DrRow("Color") = ""
                'S.SANDEEP |29-MAR-2019| -- START
                'ENHANCEMENT : 0003634 {PAPAYE}.
                DrRow("uCode") = "WE"
                'S.SANDEEP |29-MAR-2019| -- END
                dsList.Tables(0).Rows.InsertAt(DrRow, 2)

                'Pinkal (22-Dec-2022) -- Start
                ' (A1X-351) TRA - Daily attendance report changes to include new columns and hide other columns.
                If mstrGroup.Trim.ToUpper() <> "TANZANIA REVENUE AUTHORITY" Then
                    DrRow = dsList.Tables(0).NewRow
                    DrRow("Code") = "***"
                    DrRow("Name") = Language.getMessage(mstrModuleName, 28, "Mispunch")
                    DrRow("Color") = ""
                    'S.SANDEEP |29-MAR-2019| -- START
                    'ENHANCEMENT : 0003634 {PAPAYE}.
                    DrRow("uCode") = "***"
                    'S.SANDEEP |29-MAR-2019| -- END
                    dsList.Tables(0).Rows.InsertAt(DrRow, 3)
                End If
                'Pinkal (22-Dec-2022) -- End

                DrRow = dsList.Tables(0).NewRow
                DrRow("Code") = Language.getMessage(mstrModuleName, 29, "OFF")
                DrRow("Name") = Language.getMessage(mstrModuleName, 30, "Day OFF")
                DrRow("Color") = ""
                'S.SANDEEP |29-MAR-2019| -- START
                'ENHANCEMENT : 0003634 {PAPAYE}.
                DrRow("uCode") = "OFF"
                'S.SANDEEP |29-MAR-2019| -- END
                dsList.Tables(0).Rows.InsertAt(DrRow, 4)

                'S.SANDEEP |29-MAR-2019| -- START
                'ENHANCEMENT : 0003634 {PAPAYE}.
                dsList.Tables(0).AsEnumerable().ToList().ForEach(Function(dr) SetColValue(dr, "uDisplay"))
                'S.SANDEEP |29-MAR-2019| -- END

                Return dsList.Tables(0)
            End If

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: GetLegends; Module Name: " & mstrModuleName)
        End Try
        Return Nothing
    End Function

    Private Function IgnoreEmptyColumns(ByVal objDataReader As DataTable) As DataTable
        Dim strBuilder As New StringBuilder
        Dim blnFlag As Boolean = False
        Dim intColCount As Integer = objDataReader.Columns.Count
        Dim intRowCount As Integer = objDataReader.Rows.Count
        Try

            Dim intZeroColumn As Integer = 0
            Dim arrZeroColIndx As New ArrayList

            'Report Column Caption
SetColumnCount:
            intColCount = objDataReader.Columns.Count
            For j As Integer = DateDiff(DateInterval.Day, mdtFromDate, mdtToDate.AddDays(1)) + 4 To intColCount - 1
                Dim drRow As DataRow() = Nothing
                If objDataReader.Columns(j).DataType Is Type.GetType("System.String") Then
                    drRow = objDataReader.Select("[" & objDataReader.Columns(j).ColumnName & "] = '00:00' OR [" & objDataReader.Columns(j).ColumnName & "] = '' OR [" & objDataReader.Columns(j).ColumnName & "] IS NULL  OR [" & objDataReader.Columns(j).ColumnName & "] = '0:00' ")
                    If drRow.Length = objDataReader.Rows.Count Then
                        objDataReader.Columns.RemoveAt(j)
                        GoTo SetColumnCount
                    End If
                End If
            Next

            Return objDataReader

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: IgnoreEmptyColumns; Module Name: " & mstrModuleName)
            Return Nothing
        End Try
    End Function


    'Pinkal (15-Apr-2019) -- Start
    'Enhancement - Working on Leave & CR Changes for NMB.
    Private Sub RemoveColumnForLegendsFilter(ByVal dsAttendance As DataSet, ByVal mstrColumnNames As String)
        Try
            If dsAttendance Is Nothing Then Exit Sub
            If mstrColumnNames.Length <= 0 Then Exit Sub

            Dim mstrColumns() As String = mstrColumnNames.Trim().Split(",")
            If mstrColumns.Length > 0 Then

                For Each col In mstrColumns
                    dsAttendance.Tables(0).Columns.Remove(col.Trim())
                Next
                dsAttendance.AcceptChanges()
            End If

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: RemoveColumnForLegendsFilter; Module Name: " & mstrModuleName)
        End Try
    End Sub
    'Pinkal (15-Apr-2019) -- End


#End Region

    '<Language> This Auto Generated Text Please Do Not Modify it.
#Region " Language & UI Settings "
    Public Shared Sub SetMessages()
        Try
            Language.setMessage(mstrModuleName, 1, " Employee :")
            Language.setMessage(mstrModuleName, 2, "From Date :")
            Language.setMessage(mstrModuleName, 3, "To Date :")
            Language.setMessage(mstrModuleName, 5, "PR")
            Language.setMessage(mstrModuleName, 6, "AB")
            Language.setMessage(mstrModuleName, 7, "HL")
            Language.setMessage(mstrModuleName, 8, "WE")
            Language.setMessage(mstrModuleName, 9, "Prepared By :")
            Language.setMessage(mstrModuleName, 10, "Checked By :")
            Language.setMessage(mstrModuleName, 11, "Approved By")
            Language.setMessage(mstrModuleName, 12, "Received By :")
            Language.setMessage(mstrModuleName, 13, "Weekend")
            Language.setMessage(mstrModuleName, 14, "Legends :")
            Language.setMessage(mstrModuleName, 15, "Present")
            Language.setMessage(mstrModuleName, 16, "Absent")
            Language.setMessage(mstrModuleName, 17, "Total Leave")
            Language.setMessage(mstrModuleName, 18, "Total Absent")
            Language.setMessage(mstrModuleName, 19, "Total Present")
            Language.setMessage(mstrModuleName, 20, "Total Mispunch")
            Language.setMessage(mstrModuleName, 21, "Total Working Hrs")
            Language.setMessage(mstrModuleName, 22, "Total Short Hrs")
            Language.setMessage(mstrModuleName, 23, "Total OT1")
            Language.setMessage(mstrModuleName, 24, "Total OT2")
            Language.setMessage(mstrModuleName, 25, "Total OT3")
            Language.setMessage(mstrModuleName, 26, "Total OT4")
            Language.setMessage(mstrModuleName, 27, "Total Night Hrs")
            Language.setMessage(mstrModuleName, 28, "Mispunch")
            Language.setMessage(mstrModuleName, 29, "OFF")
            Language.setMessage(mstrModuleName, 30, "Day OFF")
            Language.setMessage(mstrModuleName, 31, "Legends :")
            Language.setMessage(mstrModuleName, 32, "Total Overtime Days")

        Catch Ex As Exception
            DisplayError.Show("-1", Ex.Message, "SetMessages", mstrModuleName)
        End Try
    End Sub
#End Region 'Language & UI Settings
    '</Language>
End Class
