﻿Imports Aruti.Data
Imports eZeeCommonLib
Imports Aruti
Imports ArutiReports


Public Class frmMisconductReport
    Private mstrModuleName As String = "frmMisconductReport"
    Private objMisconductReport As clsMisconductReport
    Private mstrGroupName As String = ""

#Region " Constructor "

    Public Sub New()
        objMisconductReport = New clsMisconductReport(User._Object._Languageunkid, Company._Object._Companyunkid)
        objMisconductReport.SetDefaultValue()
        Dim objGrpMstr As New clsGroup_Master
        objGrpMstr._Groupunkid = 1
        mstrGroupName = objGrpMstr._Groupname
        objGrpMstr = Nothing
        InitializeComponent()
    End Sub

#End Region

#Region " Private Variables "

    Private mstrStringIds As String = String.Empty
    Private mstrStringName As String = String.Empty
    Private mintViewIdx As Integer = 0
    Private mstrAnalysis_Fields As String = ""
    Private mstrAnalysis_Join As String = ""
    Private mstrAnalysis_OrderBy As String = ""
    Private mstrReport_GroupName As String = ""

#End Region

#Region " Private Function "

    Public Sub FillCombo()
        Try
            Dim objEmp As New clsEmployee_Master
            Dim dsList As New DataSet


            dsList = objEmp.GetEmployeeList(FinancialYear._Object._DatabaseName, _
                                            User._Object._Userunkid, _
                                            FinancialYear._Object._YearUnkid, _
                                            Company._Object._Companyunkid, _
                                            eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), _
                                            eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), _
                                            ConfigParameter._Object._UserAccessModeSetting, _
                                            True, ConfigParameter._Object._IsIncludeInactiveEmp, "Employee", True)

            With cboEmployee
                .ValueMember = "employeeunkid"
                .DisplayMember = "employeename"
                .DataSource = dsList.Tables(0)
                .SelectedValue = 0
            End With
            objEmp = Nothing
            dsList = Nothing

            Dim objMaster As New clsMasterData
            dsList = objMaster.GetEAllocation_Notification("List", "", False, False)
            Dim dtTable As DataTable = New DataView(dsList.Tables(0), "Id <> " & enAllocation.DEPARTMENT, "", DataViewRowState.CurrentRows).ToTable()
            Dim drRow As DataRow = dtTable.NewRow
            drRow("Id") = 0
            drRow("Name") = Language.getMessage(mstrModuleName, 2, "Select")
            dtTable.Rows.InsertAt(drRow, 0)
            With cboLocation
                .ValueMember = "Id"
                .DisplayMember = "Name"
                .DataSource = dtTable
            End With
            objMaster = Nothing
            dtTable = Nothing
            dsList = Nothing

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "FillCombo", mstrModuleName)
        End Try
    End Sub

    Public Sub ResetValue()
        Try
            dtpFromDate.Value = DateTime.Now
            dtpToDate.Value = DateTime.Now
            cboEmployee.SelectedValue = 0
            mstrStringIds = ""
            mstrStringName = ""
            mintViewIdx = -1
            Dim objUserDefRMode As New clsUserDef_ReportMode
            Dim dsList As DataSet = objUserDefRMode.GetList("List", enArutiReport.EmployeeMisconductReport, 0, 0)
            If dsList.Tables("List").Rows.Count > 0 Then
                cboLocation.SelectedValue = dsList.Tables("List").Rows(0).Item("transactionheadid")
            End If
            dsList = Nothing
            objUserDefRMode = Nothing
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "ResetValue", mstrModuleName)
        End Try
    End Sub

    Public Function SetFilter() As Boolean
        Try
            objMisconductReport.SetDefaultValue()

            If (DateDiff(DateInterval.Day, dtpFromDate.Value, dtpToDate.Value) + 1) > 31 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 103, "Sorry, Date range cannot be exceed more than 31 days. Please set correct days to generate report."), enMsgBoxStyle.Information)
                Return False
            End If

            If dtpToDate.Value.Date < dtpFromDate.Value.Date Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 104, "Sorry, To Date cannot be less than from date. Please set correct days to generate report."), enMsgBoxStyle.Information)
                Return False
            End If

            Dim sMsg As String = ""
            objMisconductReport.IsAbsentProcessDone(sMsg, dtpFromDate.Value.Date, dtpToDate.Value.Date, CInt(cboEmployee.SelectedValue))
            If sMsg.Trim.Length > 0 Then
                eZeeMsgBox.Show(sMsg, enMsgBoxStyle.Information)
                Return False
            End If

            If mstrGroupName.ToUpper() = "TANZANIA REVENUE AUTHORITY" Then
                If cboLocation.SelectedIndex <= 0 Then
                    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 100, "Sorry, Location is mandatory information. Please select location to continue."), enMsgBoxStyle.Information)
                    Return False
                End If
            End If
            If ConfigParameter._Object._MisconductConsecutiveAbsentDays <= 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 101, "Sorry, Misconduct days are not set under Aruti configuration -> Report Setting. Please set Misconduct days from Aruti configuration -> Report Setting."), enMsgBoxStyle.Information)
                Return False
            End If

            objMisconductReport._DateFrom = dtpFromDate.Value
            objMisconductReport._DateTo = dtpToDate.Value

            objMisconductReport._EmployeeId = cboEmployee.SelectedValue
            objMisconductReport._EmployeeName = cboEmployee.Text

            objMisconductReport._ViewByIds = mstrStringIds
            objMisconductReport._ViewIndex = mintViewIdx
            objMisconductReport._ViewByName = mstrStringName
            objMisconductReport._Analysis_Fields = mstrAnalysis_Fields
            objMisconductReport._Analysis_Join = mstrAnalysis_Join
            objMisconductReport._Analysis_OrderBy = mstrAnalysis_OrderBy
            objMisconductReport._Report_GroupName = mstrReport_GroupName
            objMisconductReport._GroupName = mstrGroupName
            objMisconductReport._MisconductDays = ConfigParameter._Object._MisconductConsecutiveAbsentDays
            objMisconductReport._LocationId = cboLocation.SelectedValue
            objMisconductReport._Location = cboLocation.Text

            Return True
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "SetFilter", mstrModuleName)
        End Try
    End Function

#End Region

#Region " Form's Events "

    Private Sub frmMisconductReport_FormClosed(ByVal sender As Object, ByVal e As System.Windows.Forms.FormClosedEventArgs) Handles Me.FormClosed
        Try
            objMisconductReport = Nothing
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmMisconductReport_FormClosed", mstrModuleName)
        End Try

    End Sub

    Private Sub frmMisconductReport_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            Call Language.setLanguage(Me.Name)

            OtherSettings()
            Me._Title = objMisconductReport._ReportName
            Me._Message = objMisconductReport._ReportDesc

            Call FillCombo()
            Call ResetValue()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmMisconductReport_Load", mstrModuleName)
        End Try
    End Sub

    Private Sub frmMisconductReport_KeyDown(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles Me.KeyDown
        Try
            If e.Control Then
                If e.KeyCode = Windows.Forms.Keys.R Then
                    Call frmMisconductReport_Report_Click(Nothing, Nothing)
                End If
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmMisconductReport_KeyDown", mstrModuleName)
        End Try
    End Sub

    Private Sub frmMisconductReport_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles Me.KeyPress
        Try
            Select Case e.KeyChar
                Case CChar(ChrW(CInt(Windows.Forms.Keys.Enter)))
                    Windows.Forms.SendKeys.Send("{TAB}")
                    e.Handled = True
                    Exit Select
            End Select
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmMisconductReport_KeyPress", mstrModuleName)
        End Try

    End Sub

#End Region

#Region " Buttons "

    Private Sub frmMisconductReport_Report_Click(ByVal sender As Object, ByVal e As Aruti.Data.PrintButtonEventArgs) Handles Me.Report_Click
        Try
            If SetFilter() = False Then Exit Sub


            objMisconductReport.generateReportNew(FinancialYear._Object._DatabaseName, _
                                                User._Object._Userunkid, _
                                                FinancialYear._Object._YearUnkid, _
                                                Company._Object._Companyunkid, _
                                                dtpToDate.Value.Date, _
                                                dtpToDate.Value.Date, _
                                                ConfigParameter._Object._UserAccessModeSetting, True, _
                                                ConfigParameter._Object._ExportReportPath, _
                                                ConfigParameter._Object._OpenAfterExport, _
                                                0, e.Type, enExportAction.None)

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmMisconductReport_Report_Click", mstrModuleName)
        End Try

    End Sub

    Private Sub frmMisconductReport_Export_Click(ByVal sender As Object, ByVal e As Aruti.Data.PrintButtonEventArgs) Handles Me.Export_Click
        Try
            If SetFilter() = False Then Exit Sub

            objMisconductReport.generateReportNew(FinancialYear._Object._DatabaseName, _
                                                User._Object._Userunkid, _
                                                FinancialYear._Object._YearUnkid, _
                                                Company._Object._Companyunkid, _
                                                dtpToDate.Value.Date, _
                                                dtpToDate.Value.Date, _
                                                ConfigParameter._Object._UserAccessModeSetting, True, _
                                                ConfigParameter._Object._ExportReportPath, _
                                                ConfigParameter._Object._OpenAfterExport, _
                                                0, enPrintAction.None, e.Type)
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmMisconductReport_Export_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub frmMisconductReport_Reset_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Reset_Click
        Try
            Call ResetValue()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmMisconductReport_Reset_Click", mstrModuleName)
        End Try

    End Sub

    Private Sub frmMisconductReport_Cancel_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Cancel_Click
        Try
            Me.Close()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmMisconductReport_Cancel_Click", mstrModuleName)
        End Try

    End Sub

    Private Sub objbtnSearchEmployee_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnSearchEmployee.Click
        Dim frm As New frmCommonSearch
        Try
            frm.DataSource = cboEmployee.DataSource
            frm.ValueMember = cboEmployee.ValueMember
            frm.DisplayMember = cboEmployee.DisplayMember
            frm.CodeMember = "employeecode"
            If frm.DisplayDialog Then
                cboEmployee.SelectedValue = frm.SelectedValue
                cboEmployee.Focus()
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objbtnSearchEmployee_Click", mstrModuleName)
        Finally
            If frm IsNot Nothing Then frm.Dispose()
        End Try
    End Sub

    Private Sub frmMisconductReport_Language_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Language_Click
        Dim objfrm As New frmLanguage
        Try
            If User._Object._Isrighttoleft = True Then
                objfrm.RightToLeft = Windows.Forms.RightToLeft.Yes
                objfrm.RightToLeftLayout = True
                Call Language.ctlRightToLeftlayOut(objfrm)
            End If

            Call SetMessages()

            clsMisconductReport.SetMessages()
            objfrm._Other_ModuleNames = "clsMisconductReport"
            objfrm.displayDialog(Me)

            Call Language.setLanguage(Me.Name)
            Call SetLanguage()

        Catch ex As System.Exception
            Call DisplayError.Show(-1, ex.Message, "frmMisconductReport_Language_Click", mstrModuleName)
        Finally
            objfrm.Dispose()
            objfrm = Nothing
        End Try

    End Sub

#End Region

#Region " LinkLabel Event "

    Private Sub lnkSetAnalysis_LinkClicked(ByVal sender As System.Object, ByVal e As System.Windows.Forms.LinkLabelLinkClickedEventArgs) Handles lnkSetAnalysis.LinkClicked
        Dim frm As New frmViewAnalysis
        Try            
            frm.displayDialog()
            mstrStringIds = frm._ReportBy_Ids
            mstrStringName = frm._ReportBy_Name
            mintViewIdx = frm._ViewIndex
            mstrAnalysis_Fields = frm._Analysis_Fields
            mstrAnalysis_Join = frm._Analysis_Join
            mstrAnalysis_OrderBy = frm._Analysis_OrderBy
            mstrReport_GroupName = frm._Report_GroupName
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "lnkSetAnalysis_LinkClicked", mstrModuleName)
        Finally
            frm = Nothing
        End Try
    End Sub

    Private Sub lnkSave_LinkClicked(ByVal sender As System.Object, ByVal e As System.Windows.Forms.LinkLabelLinkClickedEventArgs) Handles lnkSave.LinkClicked
        Try
            Dim objUserDefRMode As New clsUserDef_ReportMode
            Try
                objUserDefRMode = New clsUserDef_ReportMode()
                objUserDefRMode._Reportunkid = enArutiReport.EmployeeMisconductReport
                objUserDefRMode._Reporttypeid = 0
                objUserDefRMode._Reportmodeid = 0
                objUserDefRMode._Headtypeid = 0

                Dim intUnkid As Integer = -1

                objUserDefRMode._EarningTranHeadIds = cboLocation.SelectedValue
                intUnkid = objUserDefRMode.isExist(enArutiReport.EmployeeMisconductReport, 0, 0, 0)

                objUserDefRMode._Reportmodeunkid = intUnkid
                If intUnkid <= 0 Then
                    objUserDefRMode.Insert()
                Else
                    objUserDefRMode.Update()
                End If
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 103, "Setting saved successfully."), enMsgBoxStyle.Information)
            Catch ex As Exception
                DisplayError.Show("-1", ex.Message, "lnkSave_LinkClicked", mstrModuleName)
            Finally
                objUserDefRMode = Nothing
            End Try
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "lnkSave_LinkClicked", mstrModuleName)
        End Try
    End Sub

#End Region

    '<Language> This Auto Generated Text Please Do Not Modify it.
#Region " Language & UI Settings "
    Private Sub OtherSettings()
        Try
            Me.SuspendLayout()
            
            Call SetLanguage()

            Me.gbFilterCriteria.GradientColor = GUI._eZeeContainerHeaderBackColor
            Me.gbFilterCriteria.ForeColor = GUI._eZeeContainerHeaderForeColor


            Me.ResumeLayout()
        Catch Ex As Exception
            DisplayError.Show("-1", Ex.Message, "OtherSettings", mstrModuleName)
        End Try
    End Sub


    Private Sub SetLanguage()
        Try
            Me.Text = Language._Object.getCaption(Me.Name, Me.Text)

            Me.gbFilterCriteria.Text = Language._Object.getCaption(Me.gbFilterCriteria.Name, Me.gbFilterCriteria.Text)
            Me.lnkSetAnalysis.Text = Language._Object.getCaption(Me.lnkSetAnalysis.Name, Me.lnkSetAnalysis.Text)
            Me.lblEmpName.Text = Language._Object.getCaption(Me.lblEmpName.Name, Me.lblEmpName.Text)
            Me.LblToDate.Text = Language._Object.getCaption(Me.LblToDate.Name, Me.LblToDate.Text)
            Me.LblFromDate.Text = Language._Object.getCaption(Me.LblFromDate.Name, Me.LblFromDate.Text)
            Me.LblLocation.Text = Language._Object.getCaption(Me.LblLocation.Name, Me.LblLocation.Text)
            Me.lnkSave.Text = Language._Object.getCaption(Me.lnkSave.Name, Me.lnkSave.Text)

        Catch Ex As Exception
            DisplayError.Show("-1", Ex.Message, "SetLanguage", mstrModuleName)
        End Try
    End Sub


    Private Sub SetMessages()
        Try
            Language.setMessage(mstrModuleName, 100, "Sorry, Location is mandatory information. Please select location to continue.")
            Language.setMessage(mstrModuleName, 101, "Sorry, Misconduct days are not set under Aruti configuration -> Report Setting. Please set Misconduct days from Aruti configuration -> Report Setting.")
            Language.setMessage(mstrModuleName, 103, "Setting saved successfully.")
            Language.setMessage(mstrModuleName, 104, "Select Allocation for Report")

        Catch Ex As Exception
            DisplayError.Show("-1", Ex.Message, "SetMessages", mstrModuleName)
        End Try
    End Sub
#End Region 'Language & UI Settings
    '</Language>
End Class