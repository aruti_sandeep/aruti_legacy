#Region " Imports "

Imports Aruti.Data
Imports eZeeCommonLib
Imports Aruti
Imports ArutiReports

#End Region

Public Class frmAttendanceSummaryReport

#Region " Private Variables "

    Private ReadOnly mstrModuleName As String = "frmAttendanceSummaryReport"
    Private objASR As clsAttendanceSummaryReport
    Private mstrStringIds As String = String.Empty
    Private mstrStringName As String = String.Empty
    Private mintViewIdx As Integer = -1
    Private mstrAnalysis_Fields As String = ""
    Private mstrAnalysis_Join As String = ""
    Private mstrAnalysis_OrderBy As String = ""
    Private mstrReport_GroupName As String = ""
    Private mstrAdvanceFilter As String = String.Empty

#End Region

#Region " Constructor "

    Public Sub New()
        objASR = New clsAttendanceSummaryReport(User._Object._Languageunkid,Company._Object._Companyunkid)
        objASR.SetDefaultValue()
        InitializeComponent()
        '_Show_AdvanceFilter = True
    End Sub

#End Region

#Region " Private Function "

    Private Sub FillCombo()
        Dim dsList As New DataSet
        Dim objEmp As New clsEmployee_Master
        Try
            'S.SANDEEP [04 JUN 2015] -- START
            'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS

            'If ConfigParameter._Object._IsIncludeInactiveEmp = True Then
            '    dsList = objEmp.GetEmployeeList("List", True)
            'Else
            '    dsList = objEmp.GetEmployeeList("List", True, True, , , , , , , , , , , , eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate))
            'End If

            dsList = objEmp.GetEmployeeList(FinancialYear._Object._DatabaseName, _
                                            User._Object._Userunkid, _
                                            FinancialYear._Object._YearUnkid, _
                                            Company._Object._Companyunkid, _
                                            eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), _
                                            eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), _
                                            ConfigParameter._Object._UserAccessModeSetting, _
                                            True, ConfigParameter._Object._IsIncludeInactiveEmp, "List", True)
            'S.SANDEEP [04 JUN 2015] -- END

            With cboEmployee
                .ValueMember = "employeeunkid"
                .DisplayMember = "employeename"
                .DataSource = dsList.Tables("List")
                .SelectedValue = 0
            End With

            Dim objLeaveType As New clsleavetype_master
            Dim iDataTable As DataTable = Nothing

            'Pinkal (1-Jul-2014) -- Start
            'Enhancement : Oman Changes [Attendance Summary Report adding Absentism & Loss of Pay Column]
            'dsList = objLeaveType.getListForCombo("List", True, 1)
            dsList = objLeaveType.getListForCombo("List", True)
            'Pinkal (1-Jul-2014) -- End


            With cboLeave1
                .ValueMember = "leavetypeunkid"
                .DisplayMember = "name"
                .DataSource = dsList.Tables("List")
                .SelectedValue = 0
            End With

            With cboLeave2
                .ValueMember = "leavetypeunkid"
                .DisplayMember = "name"
                .DataSource = dsList.Tables("List").Copy
                .SelectedValue = 0
            End With

            With cboLeave3
                .ValueMember = "leavetypeunkid"
                .DisplayMember = "name"
                .DataSource = dsList.Tables("List").Copy
                .SelectedValue = 0
            End With

            With cboLeave4
                .ValueMember = "leavetypeunkid"
                .DisplayMember = "name"
                .DataSource = dsList.Tables("List").Copy
                .SelectedValue = 0
            End With



            'Pinkal (1-Jul-2014) -- Start
            'Enhancement : Oman Changes [Attendance Summary Report adding Absentism & Loss of Pay Column]

            With cboLeave5
                .ValueMember = "leavetypeunkid"
                .DisplayMember = "name"
                .DataSource = dsList.Tables("List").Copy
                .SelectedValue = 0
            End With

            'Pinkal (1-Jul-2014) -- End

            If CInt(ConfigParameter._Object._ASR_Leave1) > 0 Then
                cboLeave1.SelectedValue = CInt(ConfigParameter._Object._ASR_Leave1)
            End If
            If CInt(ConfigParameter._Object._ASR_Leave2) > 0 Then
                cboLeave2.SelectedValue = CInt(ConfigParameter._Object._ASR_Leave2)
            End If
            If CInt(ConfigParameter._Object._ASR_Leave3) > 0 Then
                cboLeave3.SelectedValue = CInt(ConfigParameter._Object._ASR_Leave3)
            End If
            If CInt(ConfigParameter._Object._ASR_Leave4) > 0 Then
                cboLeave4.SelectedValue = CInt(ConfigParameter._Object._ASR_Leave4)
            End If


            'Pinkal (1-Jul-2014) -- Start
            'Enhancement : Oman Changes [Attendance Summary Report adding Absentism & Loss of Pay Column]
            If CInt(ConfigParameter._Object._ASR_Leave5) > 0 Then
                cboLeave5.SelectedValue = CInt(ConfigParameter._Object._ASR_Leave5)
            End If
            'Pinkal (1-Jul-2014) -- End


            'Pinkal (25-Feb-2015) -- Start
            'Enhancement - ADDING COLUMN (TOTAL HOURS) IN ATTENDANCE SUMMARY REPORT AS PER RUTTA'S REQUEST [OCEAN LINK,SWACHEN].
            chkShowTotalhrs.Checked = ConfigParameter._Object._ASR_ShowTotalHrs
            'Pinkal (25-Feb-2015) -- End

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "FillCombo", mstrModuleName)
        Finally
            objEmp = Nothing : dsList.Dispose()
        End Try
    End Sub

    Private Sub Fill_LeaveType(ByVal mstrLeaveTypeIDs As String)
        Dim dsList As New DataSet
        Dim objLeaveType As New clsleavetype_master
        Dim iDataTable As DataTable = Nothing
        Try
            RemoveHandler objchkAll.CheckedChanged, AddressOf objchkAll_CheckedChanged
            RemoveHandler lvLeaveType.ItemChecked, AddressOf lvLeaveType_ItemChecked

            dsList = objLeaveType.GetList("Leave", True, True)

            If mstrLeaveTypeIDs.Trim.Length > 0 Then
                iDataTable = New DataView(dsList.Tables("Leave"), "ispaid = 1 AND leavetypeunkid not in (" & mstrLeaveTypeIDs & ")", "leavename", DataViewRowState.CurrentRows).ToTable
            Else
                iDataTable = New DataView(dsList.Tables("Leave"), "ispaid = 1", "leavename", DataViewRowState.CurrentRows).ToTable
            End If

            lvLeaveType.Items.Clear()
            Dim iStringValue As String = String.Empty
            For Each iRow As DataRow In iDataTable.Rows
                Dim lvItem As New ListViewItem

                iStringValue = "" : iStringValue = iRow.Item("leavetypecode").ToString

                If iStringValue.Trim.Length > 0 Then
                    iStringValue &= " - " & iRow.Item("leavename").ToString
                Else
                    iStringValue &= iRow.Item("leavename").ToString
                End If

                lvItem.Text = iStringValue

                If ConfigParameter._Object._ASR_LvType.Trim.Length > 0 Then
                    Dim tagIds As Integer = -1
                    tagIds = New List(Of Integer)(ConfigParameter._Object._ASR_LvType.Split(","c).Select(Function(s) Integer.Parse(s))).IndexOf(iRow.Item("leavetypeunkid"))
                    If tagIds > -1 Then
                        lvItem.Checked = True
                    End If
                End If

                lvItem.Tag = iRow.Item("leavetypeunkid")

                lvLeaveType.Items.Add(lvItem)
            Next
            If lvLeaveType.Items.Count > 7 Then
                colhLeave.Width = 244 - 20
                ' objchkAll.Location = New Point(284, 160)
            Else
                colhLeave.Width = 244
                'objchkAll.Location = New Point(301, 160)
            End If

            AddHandler objchkAll.CheckedChanged, AddressOf objchkAll_CheckedChanged
            AddHandler lvLeaveType.ItemChecked, AddressOf lvLeaveType_ItemChecked

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "Fill_LeaveType", mstrModuleName)
        Finally
        End Try
    End Sub

    Public Sub ResetValue()
        Try
            dtpFromDate.Value = ConfigParameter._Object._CurrentDateAndTime
            dtpToDate.Value = ConfigParameter._Object._CurrentDateAndTime
            cboEmployee.SelectedValue = 0
            chkInactiveemp.Checked = False
            txtSearch.Text = ""
            mstrStringIds = ""
            mstrStringName = ""
            mintViewIdx = -1
            mstrAnalysis_Fields = ""
            mstrAnalysis_Join = ""
            mstrAnalysis_OrderBy = ""
            mstrReport_GroupName = ""
            objASR.setDefaultOrderBy(0)
            txtOrderBy.Text = objASR.OrderByDisplay
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "ResetValue", mstrModuleName)
        Finally
        End Try
    End Sub

    Public Function SetFilter() As Boolean
        Try
            objASR.SetDefaultValue()

            Dim iSelectedTag As List(Of String) = lvLeaveType.CheckedItems.Cast(Of ListViewItem)().Select(Function(x) x.Tag.ToString).ToList()
            Dim iCSV As String = String.Join(", ", iSelectedTag.ToArray())

            If iCSV.Trim.Length > 0 Then
                objASR._SpecialLeaveTypeIds = iCSV
            End If

            If CInt(cboLeave1.SelectedValue) > 0 Then
                objASR._LeaveId1 = CInt(cboLeave1.SelectedValue)
                objASR._Leave1 = cboLeave1.Text
            End If

            If CInt(cboLeave2.SelectedValue) > 0 Then
                objASR._LeaveId2 = CInt(cboLeave2.SelectedValue)
                objASR._Leave2 = cboLeave2.Text
            End If

            If CInt(cboLeave3.SelectedValue) > 0 Then
                objASR._LeaveId3 = CInt(cboLeave3.SelectedValue)
                objASR._Leave3 = cboLeave3.Text
            End If

            If CInt(cboLeave4.SelectedValue) > 0 Then
                objASR._LeaveId4 = CInt(cboLeave4.SelectedValue)
                objASR._Leave4 = cboLeave4.Text
            End If


            'Pinkal (1-Jul-2014) -- Start
            'Enhancement : Oman Changes [Attendance Summary Report adding Absentism & Loss of Pay Column]
            If CInt(cboLeave5.SelectedValue) > 0 Then
                objASR._LeaveId5 = CInt(cboLeave5.SelectedValue)
                objASR._Leave5 = cboLeave5.Text
            End If

            'Pinkal (1-Jul-2014) -- End


            'Pinkal (25-Feb-2015) -- Start
            'Enhancement - ADDING COLUMN (TOTAL HOURS) IN ATTENDANCE SUMMARY REPORT AS PER RUTTA'S REQUEST [OCEAN LINK,SWACHEN].
            objASR._ShowTotalHrs = chkShowTotalhrs.Checked
            'Pinkal (25-Feb-2015) -- End

            objASR._EmployeeId = cboEmployee.SelectedValue
            objASR._EmployeeName = cboEmployee.Text
            objASR._IncludeInactiveEmp = chkInactiveemp.Checked
            objASR._ViewByIds = mstrStringIds
            objASR._ViewIndex = mintViewIdx
            objASR._ViewByName = mstrStringName
            objASR._Analysis_Fields = mstrAnalysis_Fields
            objASR._Analysis_Join = mstrAnalysis_Join
            objASR._Analysis_OrderBy = mstrAnalysis_OrderBy
            objASR._Report_GroupName = mstrReport_GroupName
            objASR._Advance_Filter = mstrAdvanceFilter
            objASR._Date1 = dtpFromDate.Value.Date
            objASR._Date2 = dtpToDate.Value.Date

            Return True
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "SetFilter", mstrModuleName)
        Finally
        End Try
    End Function

    Public Function Validation() As Boolean
        Dim blnFlag As Boolean = True
        Try

            If (CInt(cboLeave1.SelectedValue) > 0 AndAlso CInt(cboLeave2.SelectedValue) > 0) AndAlso (CInt(cboLeave1.SelectedValue) = CInt(cboLeave2.SelectedValue)) Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 1, "This Leave Type is already mapped.Please map other Leave Type."), CType(enMsgBoxStyle.Information + enMsgBoxStyle.OkOnly, enMsgBoxStyle))
                blnFlag = False

            ElseIf (CInt(cboLeave1.SelectedValue) > 0 AndAlso CInt(cboLeave3.SelectedValue) > 0) AndAlso (CInt(cboLeave1.SelectedValue) = CInt(cboLeave3.SelectedValue)) Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 1, "This Leave Type is already mapped.Please map other Leave Type."), CType(enMsgBoxStyle.Information + enMsgBoxStyle.OkOnly, enMsgBoxStyle))
                blnFlag = False

            ElseIf (CInt(cboLeave1.SelectedValue) > 0 AndAlso CInt(cboLeave4.SelectedValue) > 0) AndAlso (CInt(cboLeave1.SelectedValue) = CInt(cboLeave4.SelectedValue)) Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 1, "This Leave Type is already mapped.Please map other Leave Type."), CType(enMsgBoxStyle.Information + enMsgBoxStyle.OkOnly, enMsgBoxStyle))
                blnFlag = False


                'Pinkal (1-Jul-2014) -- Start
                'Enhancement : Oman Changes [Attendance Summary Report adding Absentism & Loss of Pay Column]
            ElseIf (CInt(cboLeave1.SelectedValue) > 0 AndAlso CInt(cboLeave5.SelectedValue) > 0) AndAlso (CInt(cboLeave1.SelectedValue) = CInt(cboLeave5.SelectedValue)) Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 1, "This Leave Type is already mapped.Please map other Leave Type."), CType(enMsgBoxStyle.Information + enMsgBoxStyle.OkOnly, enMsgBoxStyle))
                blnFlag = False
                'Pinkal (1-Jul-2014) -- End


            ElseIf (CInt(cboLeave2.SelectedValue) > 0 AndAlso CInt(cboLeave3.SelectedValue) > 0) AndAlso (CInt(cboLeave2.SelectedValue) = CInt(cboLeave3.SelectedValue)) Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 1, "This Leave Type is already mapped.Please map other Leave Type."), CType(enMsgBoxStyle.Information + enMsgBoxStyle.OkOnly, enMsgBoxStyle))
                blnFlag = False

            ElseIf (CInt(cboLeave2.SelectedValue) > 0 AndAlso CInt(cboLeave4.SelectedValue) > 0) AndAlso (CInt(cboLeave2.SelectedValue) = CInt(cboLeave4.SelectedValue)) Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 1, "This Leave Type is already mapped.Please map other Leave Type."), CType(enMsgBoxStyle.Information + enMsgBoxStyle.OkOnly, enMsgBoxStyle))
                blnFlag = False


                'Pinkal (1-Jul-2014) -- Start
                'Enhancement : Oman Changes [Attendance Summary Report adding Absentism & Loss of Pay Column]
            ElseIf (CInt(cboLeave2.SelectedValue) > 0 AndAlso CInt(cboLeave5.SelectedValue) > 0) AndAlso (CInt(cboLeave2.SelectedValue) = CInt(cboLeave5.SelectedValue)) Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 1, "This Leave Type is already mapped.Please map other Leave Type."), CType(enMsgBoxStyle.Information + enMsgBoxStyle.OkOnly, enMsgBoxStyle))
                blnFlag = False
                'Pinkal (1-Jul-2014) -- End


            ElseIf (CInt(cboLeave3.SelectedValue) > 0 AndAlso CInt(cboLeave4.SelectedValue) > 0) AndAlso (CInt(cboLeave3.SelectedValue) = CInt(cboLeave4.SelectedValue)) Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 1, "This Leave Type is already mapped.Please map other Leave Type."), CType(enMsgBoxStyle.Information + enMsgBoxStyle.OkOnly, enMsgBoxStyle))
                blnFlag = False


                'Pinkal (1-Jul-2014) -- Start
                'Enhancement : Oman Changes [Attendance Summary Report adding Absentism & Loss of Pay Column]

            ElseIf (CInt(cboLeave3.SelectedValue) > 0 AndAlso CInt(cboLeave5.SelectedValue) > 0) AndAlso (CInt(cboLeave3.SelectedValue) = CInt(cboLeave5.SelectedValue)) Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 1, "This Leave Type is already mapped.Please map other Leave Type."), CType(enMsgBoxStyle.Information + enMsgBoxStyle.OkOnly, enMsgBoxStyle))
                blnFlag = False

            ElseIf (CInt(cboLeave4.SelectedValue) > 0 AndAlso CInt(cboLeave5.SelectedValue) > 0) AndAlso (CInt(cboLeave4.SelectedValue) = CInt(cboLeave5.SelectedValue)) Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 1, "This Leave Type is already mapped.Please map other Leave Type."), CType(enMsgBoxStyle.Information + enMsgBoxStyle.OkOnly, enMsgBoxStyle))
                blnFlag = False

                'Pinkal (1-Jul-2014) -- End


            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "Validation", mstrModuleName)
        End Try
        Return blnFlag
    End Function

#End Region

#Region " Form's Events "

    Private Sub frmAttendanceSummaryReport_FormClosed(ByVal sender As Object, ByVal e As System.Windows.Forms.FormClosedEventArgs) Handles Me.FormClosed
        Try
            objASR = Nothing
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmAttendanceSummaryReport_FormClosed", mstrModuleName)
        End Try
    End Sub

    Private Sub frmAttendanceSummaryReport_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            Call Language.setLanguage(Me.Name)
            OtherSettings()
            Me._Title = objASR._ReportName
            Me._Message = objASR._ReportDesc

            Call FillCombo()
            'Call Fill_LeaveType()
            Call ResetValue()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmAttendanceSummaryReport_Load", mstrModuleName)
        End Try
    End Sub

    Private Sub frmAttendanceSummaryReport_KeyDown(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles Me.KeyDown
        Try
            If e.Control Then
                If e.KeyCode = Windows.Forms.Keys.R Then
                    Call frmAttendanceSummaryReport_Report_Click(Nothing, Nothing)
                End If
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmAttendanceSummaryReport_KeyDown", mstrModuleName)
        End Try
    End Sub

    Private Sub frmAttendanceSummaryReport_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles Me.KeyPress
        Try
            Select Case e.KeyChar
                Case CChar(ChrW(CInt(Windows.Forms.Keys.Enter)))
                    Windows.Forms.SendKeys.Send("{TAB}")
                    e.Handled = True
                    Exit Select
            End Select
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmAttendanceSummaryReport_KeyPress", mstrModuleName)
        End Try
    End Sub

#End Region

#Region " Buttons "

    Private Sub frmAttendanceSummaryReport_Report_Click(ByVal sender As Object, ByVal e As Aruti.Data.PrintButtonEventArgs) Handles Me.Report_Click
        Try
            If Validation() Then
                If SetFilter() = False Then Exit Sub

                'Shani(24-Aug-2015) -- Start
                'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
                'objASR.generateReport(0, e.Type, enExportAction.None)
                objASR.generateReportNew(FinancialYear._Object._DatabaseName, _
                                         User._Object._Userunkid, _
                                         FinancialYear._Object._YearUnkid, _
                                         Company._Object._Companyunkid, _
                                         eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), _
                                         eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), _
                                         ConfigParameter._Object._UserAccessModeSetting, True, _
                                         ConfigParameter._Object._ExportReportPath, _
                                         ConfigParameter._Object._OpenAfterExport, _
                                         0, e.Type, enExportAction.None)
                'Shani(24-Aug-2015) -- End
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmAttendanceSummaryReport_Report_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub frmAttendanceSummaryReport_Export_Click(ByVal sender As Object, ByVal e As Aruti.Data.PrintButtonEventArgs) Handles Me.Export_Click
        Try
            If Validation() Then
                'S.SANDEEP [ 26 NOV 2013 ] -- START
                If SetFilter() = False Then Exit Sub

                'Shani(24-Aug-2015) -- Start
                'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
                'objASR.generateReport(0, enPrintAction.None, e.Type)
                objASR.generateReportNew(FinancialYear._Object._DatabaseName, _
                                         User._Object._Userunkid, _
                                         FinancialYear._Object._YearUnkid, _
                                         Company._Object._Companyunkid, _
                                         eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), _
                                         eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), _
                                         ConfigParameter._Object._UserAccessModeSetting, True, _
                                         ConfigParameter._Object._ExportReportPath, _
                                         ConfigParameter._Object._OpenAfterExport, _
                                         0, enPrintAction.None, e.Type)
                'Shani(24-Aug-2015) -- End

                'S.SANDEEP [ 26 NOV 2013 ] -- END
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmAttendanceSummaryReport_Export_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub frmAttendanceSummaryReport_Reset_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Reset_Click
        Try
            Call ResetValue()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmAttendanceSummaryReport_Reset_Click", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub frmAttendanceSummaryReport_Cancel_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Cancel_Click
        Try
            Me.Close()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmAttendanceSummaryReport_Cancel_Click", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub frmAttendanceSummaryReport_Language_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Language_Click
        Dim objfrm As New frmLanguage
        Try
            If User._Object._Isrighttoleft = True Then
                objfrm.RightToLeft = Windows.Forms.RightToLeft.Yes
                objfrm.RightToLeftLayout = True
                Call Language.ctlRightToLeftlayOut(objfrm)
            End If

            Call SetMessages()

            'clsAttendanceSummaryReport.SetMessages()
            'objfrm._Other_ModuleNames = "clsAttendanceSummaryReport"
            objfrm.displayDialog(Me)

            Call Language.setLanguage(Me.Name)
            Call SetLanguage()

        Catch ex As System.Exception
            Call DisplayError.Show(-1, ex.Message, "frmAttendanceSummaryReport_Language_Click", mstrModuleName)
        Finally
            objfrm.Dispose()
            objfrm = Nothing
        End Try
    End Sub

    Private Sub Form_AdvanceFilter_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.AdvanceFilter_Click
        Dim frm As New frmAdvanceSearch
        Try
            frm._Hr_EmployeeTable_Alias = "hremployee_master"
            frm.ShowDialog()
            mstrAdvanceFilter = frm._GetFilterString
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "Form_AdvanceFilter_Click", mstrModuleName)
        Finally
            If frm IsNot Nothing Then frm.Dispose()
        End Try
    End Sub

    Private Sub objbtnSearchEmployee_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnSearchEmployee.Click
        Dim frm As New frmCommonSearch
        Try
            frm.DataSource = CType(cboEmployee.DataSource, DataTable)
            frm.ValueMember = cboEmployee.ValueMember
            frm.DisplayMember = cboEmployee.DisplayMember
            frm.CodeMember = "employeecode"
            If frm.DisplayDialog Then
                cboEmployee.SelectedValue = frm.SelectedValue
                cboEmployee.Focus()
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objbtnSearchEmployee_Click", mstrModuleName)
        Finally
            If frm IsNot Nothing Then frm.Dispose()
        End Try
    End Sub

    Private Sub objbtnSearchLeave1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnSearchLeave1.Click
        Dim frm As New frmCommonSearch
        Try
            frm.DataSource = CType(cboLeave1.DataSource, DataTable)
            frm.ValueMember = cboLeave1.ValueMember
            frm.DisplayMember = cboLeave1.DisplayMember
            If frm.DisplayDialog Then
                cboLeave1.SelectedValue = frm.SelectedValue
                cboLeave1.Focus()
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objbtnSearchLeave1_Click", mstrModuleName)
        Finally
            If frm IsNot Nothing Then frm.Dispose()
        End Try
    End Sub

    Private Sub objbtnSearchLeave2_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnSearchLeave2.Click
        Dim frm As New frmCommonSearch
        Try
            frm.DataSource = CType(cboLeave2.DataSource, DataTable)
            frm.ValueMember = cboLeave2.ValueMember
            frm.DisplayMember = cboLeave2.DisplayMember
            If frm.DisplayDialog Then
                cboLeave2.SelectedValue = frm.SelectedValue
                cboLeave2.Focus()
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objbtnSearchLeave2_Click", mstrModuleName)
        Finally
            If frm IsNot Nothing Then frm.Dispose()
        End Try
    End Sub

    Private Sub objbtnSearchLeave3_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnSearchLeave3.Click
        Dim frm As New frmCommonSearch
        Try
            frm.DataSource = CType(cboLeave3.DataSource, DataTable)
            frm.ValueMember = cboLeave3.ValueMember
            frm.DisplayMember = cboLeave3.DisplayMember
            If frm.DisplayDialog Then
                cboLeave3.SelectedValue = frm.SelectedValue
                cboLeave3.Focus()
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objbtnSearchLeave3_Click", mstrModuleName)
        Finally
            If frm IsNot Nothing Then frm.Dispose()
        End Try
    End Sub

    Private Sub objbtnSearchLeave4_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnSearchLeave4.Click
        Dim frm As New frmCommonSearch
        Try
            frm.DataSource = CType(cboLeave4.DataSource, DataTable)
            frm.ValueMember = cboLeave4.ValueMember
            frm.DisplayMember = cboLeave4.DisplayMember
            If frm.DisplayDialog Then
                cboLeave4.SelectedValue = frm.SelectedValue
                cboLeave4.Focus()
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objbtnSearchLeave4_Click", mstrModuleName)
        Finally
            If frm IsNot Nothing Then frm.Dispose()
        End Try
    End Sub


    'Pinkal (1-Jul-2014) -- Start
    'Enhancement : Oman Changes [Attendance Summary Report adding Absentism & Loss of Pay Column]

    Private Sub objbtnSearchLeave5_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnSearchLeave5.Click
        Dim frm As New frmCommonSearch
        Try
            frm.DataSource = CType(cboLeave5.DataSource, DataTable)
            frm.ValueMember = cboLeave5.ValueMember
            frm.DisplayMember = cboLeave5.DisplayMember
            If frm.DisplayDialog Then
                cboLeave5.SelectedValue = frm.SelectedValue
                cboLeave5.Focus()
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objbtnSearchLeave5_Click", mstrModuleName)
        Finally
            If frm IsNot Nothing Then frm.Dispose()
        End Try
    End Sub

    'Pinkal (1-Jul-2014) -- End


#End Region

#Region "Dropdown Event"

    Private Sub cboLeave1_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cboLeave1.SelectedIndexChanged, cboLeave2.SelectedIndexChanged, _
                                                                                                                                                                              cboLeave3.SelectedIndexChanged, cboLeave4.SelectedIndexChanged, _
                                                                                                                                                                              cboLeave5.SelectedIndexChanged
        Try
            Dim mstrLeaveTypeIDs As String = ""

            If Validation() Then
                If CInt(cboLeave1.SelectedValue) > 0 Then
                    mstrLeaveTypeIDs = CInt(cboLeave1.SelectedValue) & ","
                End If

                If CInt(cboLeave2.SelectedValue) > 0 Then
                    mstrLeaveTypeIDs &= CInt(cboLeave2.SelectedValue) & ","
                End If

                If CInt(cboLeave3.SelectedValue) > 0 Then
                    mstrLeaveTypeIDs &= CInt(cboLeave3.SelectedValue) & ","
                End If

                If CInt(cboLeave4.SelectedValue) > 0 Then
                    mstrLeaveTypeIDs &= CInt(cboLeave4.SelectedValue) & ","
                End If


                'Pinkal (1-Jul-2014) -- Start
                'Enhancement : Oman Changes [Attendance Summary Report adding Absentism & Loss of Pay Column]
                If CInt(cboLeave5.SelectedValue) > 0 Then
                    mstrLeaveTypeIDs &= CInt(cboLeave5.SelectedValue) & ","
                End If
                'Pinkal (1-Jul-2014) -- End


                If mstrLeaveTypeIDs.ToString.Trim.Length > 0 Then
                    mstrLeaveTypeIDs = mstrLeaveTypeIDs.Trim.Substring(0, mstrLeaveTypeIDs.Trim.Length - 1)
                End If
            Else
                CType(sender, ComboBox).SelectedValue = 0
            End If

            Fill_LeaveType(mstrLeaveTypeIDs)

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "cboLeave1_SelectedIndexChanged", mstrModuleName)
        End Try
    End Sub

#End Region

#Region "LinkButton Event"

    Private Sub lnkSetAnalysis_LinkClicked(ByVal sender As System.Object, ByVal e As System.Windows.Forms.LinkLabelLinkClickedEventArgs) Handles lnkSetAnalysis.LinkClicked
        Dim frm As New frmViewAnalysis
        Try
            frm.displayDialog()
            mstrStringIds = frm._ReportBy_Ids
            mstrStringName = frm._ReportBy_Name
            mintViewIdx = frm._ViewIndex
            mstrAnalysis_Fields = frm._Analysis_Fields
            mstrAnalysis_Join = frm._Analysis_Join
            mstrAnalysis_OrderBy = frm._Analysis_OrderBy
            mstrReport_GroupName = frm._Report_GroupName
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "lnkSetAnalysis_LinkClicked", mstrModuleName)
        Finally
            frm = Nothing
        End Try
    End Sub

    Private Sub lvLeaveType_ItemChecked(ByVal sender As Object, ByVal e As System.Windows.Forms.ItemCheckedEventArgs) Handles lvLeaveType.ItemChecked
        Try
            RemoveHandler objchkAll.CheckedChanged, AddressOf objchkAll_CheckedChanged
            If lvLeaveType.CheckedItems.Count <= 0 Then
                objchkAll.CheckState = CheckState.Unchecked
            ElseIf lvLeaveType.CheckedItems.Count < lvLeaveType.Items.Count Then
                objchkAll.CheckState = CheckState.Indeterminate
            ElseIf lvLeaveType.CheckedItems.Count = lvLeaveType.Items.Count Then
                objchkAll.CheckState = CheckState.Checked
            End If
            AddHandler objchkAll.CheckedChanged, AddressOf objchkAll_CheckedChanged
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "lvLeaveType_ItemChecked", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub lnkSave_LinkClicked(ByVal sender As System.Object, ByVal e As System.Windows.Forms.LinkLabelLinkClickedEventArgs) Handles lnkSave.LinkClicked
        Try
            'If lvLeaveType.CheckedItems.Count <= 0 Then
            '    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 1, "Please check atleast one leave type in order to save as special leave."), enMsgBoxStyle.Information)
            '    Exit Sub
            'End If

            If Validation() Then

                Dim iSelectedTag As List(Of String) = lvLeaveType.CheckedItems.Cast(Of ListViewItem)().Select(Function(x) x.Tag.ToString).ToList()
                Dim iCSV As String = String.Join(", ", iSelectedTag.ToArray())

                Dim objConfig As New clsConfigOptions
                objConfig._Companyunkid = Company._Object._Companyunkid
                objConfig._ASR_Leave1 = CInt(cboLeave1.SelectedValue)
                objConfig._ASR_Leave2 = CInt(cboLeave2.SelectedValue)
                objConfig._ASR_Leave3 = CInt(cboLeave3.SelectedValue)
                objConfig._ASR_Leave4 = CInt(cboLeave4.SelectedValue)
                objConfig._ASR_LvType = iCSV


                'Pinkal (1-Jul-2014) -- Start
                'Enhancement : Oman Changes [Attendance Summary Report adding Absentism & Loss of Pay Column]
                objConfig._ASR_Leave5 = CInt(cboLeave5.SelectedValue)
                'Pinkal (1-Jul-2014) -- End


                'Pinkal (25-Feb-2015) -- Start
                'Enhancement - ADDING COLUMN (TOTAL HOURS) IN ATTENDANCE SUMMARY REPORT AS PER RUTTA'S REQUEST [OCEAN LINK,SWACHEN].
                objConfig._ASR_ShowTotalHrs = chkShowTotalhrs.Checked
                'Pinkal (25-Feb-2015) -- End


                If objConfig.updateParam() = True Then
                    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 2, "Filters saved successfully."), enMsgBoxStyle.Information)
                End If
                objConfig = Nothing
                ConfigParameter._Object.Refresh()
            End If

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "lnkSave_LinkClicked", mstrModuleName)
        Finally
        End Try
    End Sub

#End Region

#Region "CheckBox Event"

    Private Sub objchkAll_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objchkAll.CheckedChanged
        Try
            RemoveHandler lvLeaveType.ItemChecked, AddressOf lvLeaveType_ItemChecked
            For Each lvItem As ListViewItem In lvLeaveType.Items
                lvItem.Checked = objchkAll.Checked
            Next
            AddHandler lvLeaveType.ItemChecked, AddressOf lvLeaveType_ItemChecked
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objchkAll_CheckedChanged", mstrModuleName)
        Finally
        End Try
    End Sub

#End Region

#Region "TextBox Event"

    Private Sub txtSearch_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txtSearch.TextChanged
        Try
            lvLeaveType.SelectedIndices.Clear()
            Dim lvFoundItem As ListViewItem = lvLeaveType.FindItemWithText(txtSearch.Text, False, 0, True)
            If lvFoundItem IsNot Nothing Then
                lvLeaveType.TopItem = lvFoundItem
                lvFoundItem.Selected = True
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "txtSearch_TextChanged", mstrModuleName)
        End Try
    End Sub


#End Region

#Region " Controls "

    Private Sub objbtnSort_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles objbtnSort.Click
        Try
            objASR.setOrderBy(0)
            txtOrderBy.Text = objASR.OrderByDisplay
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objbtnSort_Click", mstrModuleName)
        End Try
    End Sub

#End Region

   
	'<Language> This Auto Generated Text Please Do Not Modify it.
#Region " Language & UI Settings "
	Private Sub OtherSettings()
		Try
			Me.SuspendLayout()
            Call SetLanguage()
			
			Me.gbFilterCriteria.GradientColor = GUI._eZeeContainerHeaderBackColor 
			Me.gbFilterCriteria.ForeColor = GUI._eZeeContainerHeaderForeColor 

			Me.gbSortBy.GradientColor = GUI._eZeeContainerHeaderBackColor 
			Me.gbSortBy.ForeColor = GUI._eZeeContainerHeaderForeColor 


			Me.ResumeLayout()
		Catch Ex As Exception
			DisplayError.Show("-1", ex.Message, "OtherSettings", mstrModuleName)
		End Try
	End Sub
			
			
	Private Sub SetLanguage()
		Try
			Me.Text = Language._Object.getCaption(Me.Name, Me.Text)
			
			Me.gbFilterCriteria.Text = Language._Object.getCaption(Me.gbFilterCriteria.Name, Me.gbFilterCriteria.Text)
			Me.chkInactiveemp.Text = Language._Object.getCaption(Me.chkInactiveemp.Name, Me.chkInactiveemp.Text)
			Me.lblToDate.Text = Language._Object.getCaption(Me.lblToDate.Name, Me.lblToDate.Text)
			Me.lblEmpName.Text = Language._Object.getCaption(Me.lblEmpName.Name, Me.lblEmpName.Text)
			Me.lblFromDate.Text = Language._Object.getCaption(Me.lblFromDate.Name, Me.lblFromDate.Text)
			Me.colhLeave.Text = Language._Object.getCaption(CStr(Me.colhLeave.Tag), Me.colhLeave.Text)
			Me.lnkSave.Text = Language._Object.getCaption(Me.lnkSave.Name, Me.lnkSave.Text)
			Me.gbSortBy.Text = Language._Object.getCaption(Me.gbSortBy.Name, Me.gbSortBy.Text)
			Me.lblOrderBy.Text = Language._Object.getCaption(Me.lblOrderBy.Name, Me.lblOrderBy.Text)
			Me.lnkSetAnalysis.Text = Language._Object.getCaption(Me.lnkSetAnalysis.Name, Me.lnkSetAnalysis.Text)
			Me.elLine1.Text = Language._Object.getCaption(Me.elLine1.Name, Me.elLine1.Text)
			Me.LblLeave4.Text = Language._Object.getCaption(Me.LblLeave4.Name, Me.LblLeave4.Text)
			Me.LblLeave3.Text = Language._Object.getCaption(Me.LblLeave3.Name, Me.LblLeave3.Text)
			Me.LblLeave2.Text = Language._Object.getCaption(Me.LblLeave2.Name, Me.LblLeave2.Text)
			Me.LblLeave1.Text = Language._Object.getCaption(Me.LblLeave1.Name, Me.LblLeave1.Text)
                        Me.objbtnSearchLeave5.Text = Language._Object.getCaption(Me.objbtnSearchLeave5.Name, Me.objbtnSearchLeave5.Text)
                        Me.LblLeave5.Text = Language._Object.getCaption(Me.LblLeave5.Name, Me.LblLeave5.Text)

		Catch Ex As Exception
			DisplayError.Show("-1", ex.Message, "SetLanguage", mstrModuleName)
		End Try
	End Sub
			
			
	Private Sub SetMessages()
		Try
			Language.setMessage(mstrModuleName, 1, "This Leave Type is already mapped.Please map other Leave Type.")
			Language.setMessage(mstrModuleName, 2, "Leave saved successfully.")

		Catch Ex As Exception
			DisplayError.Show("-1", ex.Message, "SetMessages", mstrModuleName)
		End Try
	End Sub
#End Region 'Language & UI Settings
	'</Language>
End Class
