Imports Aruti.Data
Imports eZeeCommonLib
Imports Aruti
Imports ArutiReports

'Last Message index= 1

Public Class frmEmpBreakTimesheetReport
    Private mstrModuleName As String = "frmEmpBreakTimesheetReport"
    Private objEmpBreakTimeSheet As clsEmpBreakTimesheetReport

#Region "Constructor"

    Public Sub New()
        objEmpBreakTimeSheet = New clsEmpBreakTimesheetReport(User._Object._Languageunkid,Company._Object._Companyunkid)
        objEmpBreakTimeSheet.SetDefaultValue()
        InitializeComponent()
        _Show_AdvanceFilter = True
    End Sub

#End Region

#Region "Private Variables"

    Private mstrStringIds As String = String.Empty
    Private mstrStringName As String = String.Empty
    Private mintViewIdx As Integer = 0
    Private mstrAnalysis_Fields As String = ""
    Private mstrAnalysis_Join As String = ""
    Private mstrAnalysis_OrderBy As String = ""
    Private mstrReport_GroupName As String = ""
    Private mstrAdvanceFilter As String = String.Empty

#End Region

#Region "Private Function"

    Public Sub FillCombo()
        Try
            Dim objEmp As New clsEmployee_Master
            Dim dsList As New DataSet

            'Shani(24-Aug-2015) -- Start
            'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
            'If ConfigParameter._Object._IsIncludeInactiveEmp = True Then
            '    dsList = objEmp.GetEmployeeList("Employee", True, False)
            'Else
            '    dsList = objEmp.GetEmployeeList("Employee", True, True, , , , , , , , , , , , eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate))
            'End If
            dsList = objEmp.GetEmployeeList(FinancialYear._Object._DatabaseName, _
                                            User._Object._Userunkid, _
                                            FinancialYear._Object._YearUnkid, _
                                            Company._Object._Companyunkid, _
                                            eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), _
                                            eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), _
                                            ConfigParameter._Object._UserAccessModeSetting, _
                                            True, ConfigParameter._Object._IsIncludeInactiveEmp, "Employee", True)
            'Shani(24-Aug-2015) -- End

            With cboEmployee
                .ValueMember = "employeeunkid"
                .DisplayMember = "employeename"
                .DataSource = dsList.Tables(0)
                .SelectedValue = 0
            End With
            objEmp = Nothing
            dsList = Nothing

            Dim objShift As New clsNewshift_master
            dsList = objShift.getListForCombo("List", True)
            With cboShift
                .ValueMember = "shiftunkid"
                .DisplayMember = "name"
                .DataSource = dsList.Tables(0)
                .SelectedValue = 0
            End With

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "FillCombo", mstrModuleName)
        End Try
    End Sub

    Public Sub ResetValue()
        Try
            cboEmployee.SelectedValue = 0
            cboShift.SelectedValue = 0
            mstrStringIds = ""
            mstrStringName = ""
            mintViewIdx = -1
            dtpFromDate.Value = ConfigParameter._Object._CurrentDateAndTime.Date
            dtpToDate.Value = ConfigParameter._Object._CurrentDateAndTime.Date
            chkInactiveemp.Checked = False
            mstrAdvanceFilter = ""
            objEmpBreakTimeSheet.setDefaultOrderBy(0)
            txtOrderBy.Text = objEmpBreakTimeSheet.OrderByDisplay
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "ResetValue", mstrModuleName)
        End Try
    End Sub

    Public Function SetFilter() As Boolean
        Try
            objEmpBreakTimeSheet.SetDefaultValue()

            objEmpBreakTimeSheet._EmpId = cboEmployee.SelectedValue
            objEmpBreakTimeSheet._EmpName = cboEmployee.Text

            If CInt(cboEmployee.SelectedValue) > 0 Then
                Dim objEmp As New clsEmployee_Master
                'S.SANDEEP [04 JUN 2015] -- START
                'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
                'objEmp._Employeeunkid = CInt(cboEmployee.SelectedValue)
                objEmp._Employeeunkid(eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate)) = CInt(cboEmployee.SelectedValue)
                'S.SANDEEP [04 JUN 2015] -- END
                objEmpBreakTimeSheet._Shiftunkid = objEmp._Shiftunkid
                objEmpBreakTimeSheet._EmpCode = objEmp._Employeecode
            End If

            objEmpBreakTimeSheet._FromDate = dtpFromDate.Value.Date
            objEmpBreakTimeSheet._ToDate = dtpToDate.Value.Date
            objEmpBreakTimeSheet._IsActive = chkInactiveemp.Checked
            objEmpBreakTimeSheet._Shiftunkid = CInt(cboShift.SelectedValue)
            objEmpBreakTimeSheet._ShiftName = cboShift.Text
            objEmpBreakTimeSheet._ViewByIds = mstrStringIds
            objEmpBreakTimeSheet._ViewIndex = mintViewIdx
            objEmpBreakTimeSheet._ViewByName = mstrStringName
            objEmpBreakTimeSheet._Analysis_Fields = mstrAnalysis_Fields
            objEmpBreakTimeSheet._Analysis_Join = mstrAnalysis_Join
            objEmpBreakTimeSheet._Analysis_OrderBy = mstrAnalysis_OrderBy
            objEmpBreakTimeSheet._Report_GroupName = mstrReport_GroupName
            objEmpBreakTimeSheet._AdvanceFilter = mstrAdvanceFilter
            objEmpBreakTimeSheet._ShowEachEmployeeOnNewPage = chkShowEachEmpOnNewPage.Checked

            'Pinkal (14-Feb-2022) -- Start
            'Enhancement TRA : TnA Module Enhancement for TRA.
            Dim objGrpMstr As New clsGroup_Master
            objGrpMstr._Groupunkid = 1
            objEmpBreakTimeSheet._GroupName = objGrpMstr._Groupname
            objGrpMstr = Nothing
            'Pinkal (14-Feb-2022) -- End

            Return True
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "SetFilter", mstrModuleName)
        End Try
    End Function

#End Region

#Region "Form's Events"

    Private Sub frmEmpBreakTimesheetReport_FormClosed(ByVal sender As Object, ByVal e As System.Windows.Forms.FormClosedEventArgs) Handles Me.FormClosed
        Try
            objEmpBreakTimeSheet = Nothing
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmEmpBreakTimesheetReport_FormClosed", mstrModuleName)
        End Try

    End Sub

    Private Sub frmEmpBreakTimesheetReport_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            Call Language.setLanguage(Me.Name)
            OtherSettings()
            Me._Title = objEmpBreakTimeSheet._ReportName
            Me._Message = objEmpBreakTimeSheet._ReportDesc
            Call FillCombo()
            Call ResetValue()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmEmpBreakTimesheetReport_Load", mstrModuleName)
        End Try
    End Sub

    Private Sub frmEmpBreakTimesheetReport_KeyDown(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles Me.KeyDown
        Try
            If e.Control Then
                If e.KeyCode = Windows.Forms.Keys.R Then
                    Call frmEmpBreakTimesheetReport_Report_Click(Nothing, Nothing)
                End If
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmEmpBreakTimesheetReport_KeyDown", mstrModuleName)
        End Try
    End Sub

    Private Sub frmEmpBreakTimesheetReport_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles Me.KeyPress
        Try
            Select Case e.KeyChar
                Case CChar(ChrW(CInt(Windows.Forms.Keys.Enter)))
                    Windows.Forms.SendKeys.Send("{TAB}")
                    e.Handled = True
                    Exit Select
            End Select
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmEmpBreakTimesheetReport_KeyPress", mstrModuleName)
        End Try

    End Sub

#End Region

#Region "Buttons"

    Private Sub frmEmpBreakTimesheetReport_Report_Click(ByVal sender As Object, ByVal e As Aruti.Data.PrintButtonEventArgs) Handles Me.Report_Click
        Try
            If SetFilter() = False Then Exit Sub

            'Shani(24-Aug-2015) -- Start
            'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
            'objEmpBreakTimeSheet.generateReport(0, e.Type, Aruti.Data.enExportAction.None)
            objEmpBreakTimeSheet.generateReportNew(FinancialYear._Object._DatabaseName, _
                                                   User._Object._Userunkid, _
                                                   FinancialYear._Object._YearUnkid, _
                                                   Company._Object._Companyunkid, _
                                                   eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), _
                                                   eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), _
                                                   ConfigParameter._Object._UserAccessModeSetting, True, _
                                                   ConfigParameter._Object._ExportReportPath, _
                                                   ConfigParameter._Object._OpenAfterExport, _
                                                   0, e.Type, Aruti.Data.enExportAction.None)
            'Shani(24-Aug-2015) -- End

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmEmpBreakTimesheetReport_Report_Click", mstrModuleName)
        End Try

    End Sub

    Private Sub frmEmpBreakTimesheetReport_Export_Click(ByVal sender As Object, ByVal e As Aruti.Data.PrintButtonEventArgs) Handles Me.Export_Click
        Try
            If SetFilter() = False Then Exit Sub

            'Shani(24-Aug-2015) -- Start
            'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
            'objEmpBreakTimeSheet.generateReport(0, enPrintAction.None, e.Type)
            objEmpBreakTimeSheet.generateReportNew(FinancialYear._Object._DatabaseName, _
                                                   User._Object._Userunkid, _
                                                   FinancialYear._Object._YearUnkid, _
                                                   Company._Object._Companyunkid, _
                                                   eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), _
                                                   eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), _
                                                   ConfigParameter._Object._UserAccessModeSetting, True, _
                                                   ConfigParameter._Object._ExportReportPath, _
                                                   ConfigParameter._Object._OpenAfterExport, _
                                                   0, enPrintAction.None, e.Type)
            'Shani(24-Aug-2015) -- End

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmEmpBreakTimesheetReport_Export_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub frmEmpBreakTimesheetReport_Reset_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Reset_Click
        Try
            Call ResetValue()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmEmpBreakTimesheetReport_Reset_Click", mstrModuleName)
        End Try

    End Sub

    Private Sub frmEmpBreakTimesheetReport_Cancel_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Cancel_Click
        Try
            Me.Close()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmEmpBreakTimesheetReport_Cancel_Click", mstrModuleName)
        End Try

    End Sub

    Private Sub Form_AdvanceFilter_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.AdvanceFilter_Click
        Dim frm As New frmAdvanceSearch
        Try
            frm._Hr_EmployeeTable_Alias = "hremployee_master"
            frm.ShowDialog()
            mstrAdvanceFilter = frm._GetFilterString
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "Form_AdvanceFilter_Click", mstrModuleName)
        Finally
            If frm IsNot Nothing Then frm.Dispose()
        End Try
    End Sub

    Private Sub objbtnSearchEmployee_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnSearchEmployee.Click
        Dim frm As New frmCommonSearch
        Try
            frm.DataSource = cboEmployee.DataSource
            frm.ValueMember = cboEmployee.ValueMember
            frm.DisplayMember = cboEmployee.DisplayMember
            frm.CodeMember = "employeecode"
            If frm.DisplayDialog Then
                cboEmployee.SelectedValue = frm.SelectedValue
                cboEmployee.Focus()
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objbtnSearchEmployee_Click", mstrModuleName)
        Finally
            If frm IsNot Nothing Then frm.Dispose()
        End Try
    End Sub

    Private Sub frmEmpBreakTimesheetReport_Language_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Language_Click
        Dim objfrm As New frmLanguage
        Try
            If User._Object._Isrighttoleft = True Then
                objfrm.RightToLeft = Windows.Forms.RightToLeft.Yes
                objfrm.RightToLeftLayout = True
                Call Language.ctlRightToLeftlayOut(objfrm)
            End If

            Call SetMessages()

            clsEmpBreakTimesheetReport.SetMessages()
            objfrm._Other_ModuleNames = "clsEmpBreakTimesheetReport"
            objfrm.displayDialog(Me)

            Call Language.setLanguage(Me.Name)
            Call SetLanguage()

        Catch ex As System.Exception
            Call DisplayError.Show(-1, ex.Message, "frmEmpBreakTimesheetReport_Language_Click", mstrModuleName)
        Finally
            objfrm.Dispose()
            objfrm = Nothing
        End Try

    End Sub

    Private Sub objbtnSearchShift_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnSearchShift.Click
        Dim frm As New frmCommonSearch
        Try
            frm.DataSource = cboShift.DataSource
            frm.ValueMember = cboShift.ValueMember
            frm.DisplayMember = cboShift.DisplayMember
            If frm.DisplayDialog Then
                cboShift.SelectedValue = frm.SelectedValue
                cboShift.Focus()
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objbtnSearchShift_Click", mstrModuleName)
        Finally
            If frm IsNot Nothing Then frm.Dispose()
        End Try
    End Sub


#End Region

#Region " Controls "

    Private Sub objbtnSort_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles objbtnSort.Click
        Try
            objEmpBreakTimeSheet.setOrderBy(0)
            txtOrderBy.Text = objEmpBreakTimeSheet.OrderByDisplay
        Catch ex As Exception
            DisplayError.Show(-1, ex.Message, "objbtnSort_Click", mstrModuleName)
        End Try
    End Sub

#End Region

#Region "LinkLabel Event"

    Private Sub lnkSetAnalysis_LinkClicked(ByVal sender As System.Object, ByVal e As System.Windows.Forms.LinkLabelLinkClickedEventArgs) Handles lnkSetAnalysis.LinkClicked 'S.SANDEEP [ 26 NOV 2013 ] -- START {Handler Added} -- END
        Dim frm As New frmViewAnalysis
        Try
            frm.displayDialog()
            mstrStringIds = frm._ReportBy_Ids
            mstrStringName = frm._ReportBy_Name
            mintViewIdx = frm._ViewIndex
            mstrAnalysis_Fields = frm._Analysis_Fields
            mstrAnalysis_Join = frm._Analysis_Join
            mstrAnalysis_OrderBy = frm._Analysis_OrderBy
            mstrReport_GroupName = frm._Report_GroupName
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "lnkSetAnalysis_LinkClicked", mstrModuleName)
        Finally
            frm = Nothing
        End Try
    End Sub

#End Region


    '<Language> This Auto Generated Text Please Do Not Modify it.
#Region " Language & UI Settings "

    Private Sub OtherSettings()
        Try
            Me.SuspendLayout()
            Call SetLanguage()

            Me.gbFilterCriteria.GradientColor = GUI._eZeeContainerHeaderBackColor
            Me.gbFilterCriteria.ForeColor = GUI._eZeeContainerHeaderForeColor

            Me.gbSortBy.GradientColor = GUI._eZeeContainerHeaderBackColor
            Me.gbSortBy.ForeColor = GUI._eZeeContainerHeaderForeColor


            Me.ResumeLayout()
        Catch Ex As Exception
            DisplayError.Show("-1", Ex.Message, "OtherSettings", mstrModuleName)
        End Try
    End Sub

    Private Sub SetLanguage()
        Try
            Me.Text = Language._Object.getCaption(Me.Name, Me.Text)

            Me.gbFilterCriteria.Text = Language._Object.getCaption(Me.gbFilterCriteria.Name, Me.gbFilterCriteria.Text)
            Me.lblEmpName.Text = Language._Object.getCaption(Me.lblEmpName.Name, Me.lblEmpName.Text)
            Me.lblFromDate.Text = Language._Object.getCaption(Me.lblFromDate.Name, Me.lblFromDate.Text)
            Me.gbSortBy.Text = Language._Object.getCaption(Me.gbSortBy.Name, Me.gbSortBy.Text)
            Me.lblOrderBy.Text = Language._Object.getCaption(Me.lblOrderBy.Name, Me.lblOrderBy.Text)
            Me.lblToDate.Text = Language._Object.getCaption(Me.lblToDate.Name, Me.lblToDate.Text)
            Me.LblShift.Text = Language._Object.getCaption(Me.LblShift.Name, Me.LblShift.Text)
            Me.lnkSetAnalysis.Text = Language._Object.getCaption(Me.lnkSetAnalysis.Name, Me.lnkSetAnalysis.Text)
            Me.chkShowEachEmpOnNewPage.Text = Language._Object.getCaption(Me.chkShowEachEmpOnNewPage.Name, Me.chkShowEachEmpOnNewPage.Text)
            Me.chkInactiveemp.Text = Language._Object.getCaption(Me.chkInactiveemp.Name, Me.chkInactiveemp.Text)

        Catch Ex As Exception
            DisplayError.Show("-1", Ex.Message, "SetLanguage", mstrModuleName)
        End Try
    End Sub

    Private Sub SetMessages()
        Try
            Language.setMessage(mstrModuleName, 2, "Filters saved successfully.")

        Catch Ex As Exception
            DisplayError.Show("-1", Ex.Message, "SetMessages", mstrModuleName)
        End Try
    End Sub
#End Region 'Language & UI Settings
    '</Language>
End Class
