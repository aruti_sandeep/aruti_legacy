Imports Aruti.Data
Imports eZeeCommonLib
Imports Aruti
Imports ArutiReports

'Last Message index= 1

Public Class frmTRAAbsenteesExcuseReport
    Private mstrModuleName As String = "frmTRAAbsenteesExcuseReport"
    Private objTRAAbsenteesExcuseReport As clsTRAAbsenteesExcuseReport

#Region "Constructor"
    Public Sub New()
        objTRAAbsenteesExcuseReport = New clsTRAAbsenteesExcuseReport(User._Object._Languageunkid, Company._Object._Companyunkid)
        objTRAAbsenteesExcuseReport.SetDefaultValue()
        InitializeComponent()
        _Show_AdvanceFilter = True
    End Sub
#End Region

#Region "Private Variables"

    Private mstrViewByIds As String = String.Empty
    Private mstrViewByName As String = String.Empty
    Private mintViewIndex As Integer = -1
    Private mstrAnalysis_Fields As String = ""
    Private mstrAnalysis_Join As String = ""
    Private mstrAnalysis_OrderBy As String = ""
    Private mstrReport_GroupName As String = ""
    Private mstrAnalysis_OrderBy_GName As String = ""
    Private mstrAdvanceFilter As String = String.Empty

#End Region

#Region "Private Function"

    Public Sub FillCombo()
        Try
            Dim objEmp As New clsEmployee_Master
            Dim dsList As New DataSet

            dsList = objEmp.GetEmployeeList(FinancialYear._Object._DatabaseName, _
                                            User._Object._Userunkid, _
                                            FinancialYear._Object._YearUnkid, _
                                            Company._Object._Companyunkid, _
                                            eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), _
                                            eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), _
                                            ConfigParameter._Object._UserAccessModeSetting, _
                                            True, ConfigParameter._Object._IsIncludeInactiveEmp, "Employee", True)

            With cboEmployee
                .ValueMember = "employeeunkid"
                .DisplayMember = "employeename"
                .DataSource = dsList.Tables(0)
                .SelectedValue = 0
            End With
            objEmp = Nothing
            dsList = Nothing


            Dim objLeaveType As New clsleavetype_master
            dsList = objLeaveType.getListForCombo("List", True, -1, FinancialYear._Object._DatabaseName, "lvleavetype_master.isexcuseleave = 1", False)
            With cboExcuse
                .ValueMember = "leavetypeunkid"
                .DisplayMember = "name"
                .DataSource = dsList.Tables(0)
                .SelectedValue = 0
            End With
            objLeaveType = Nothing
            dsList = Nothing

            Dim objMaster As New clsMasterData
            dsList = objMaster.GetEAllocation_Notification("List", "", False, False)
            Dim dtTable As DataTable = New DataView(dsList.Tables(0), "Id <> " & enAllocation.DEPARTMENT, "", DataViewRowState.CurrentRows).ToTable()
            Dim drRow As DataRow = dtTable.NewRow
            drRow("Id") = 0
            drRow("Name") = Language.getMessage(mstrModuleName, 2, "Select")
            dtTable.Rows.InsertAt(drRow, 0)
            With cboLocation
                .ValueMember = "Id"
                .DisplayMember = "Name"
                .DataSource = dtTable
            End With
            objMaster = Nothing
            dtTable = Nothing
            dsList = Nothing

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "FillCombo", mstrModuleName)
        End Try
    End Sub

    Public Sub ResetValue()
        Try
            cboEmployee.SelectedValue = 0
            cboExcuse.SelectedValue = 0
            dtpFromDate.Value = ConfigParameter._Object._CurrentDateAndTime.Date
            dtpToDate.Value = ConfigParameter._Object._CurrentDateAndTime.Date
            mintViewIndex = -1
            mstrAdvanceFilter = ""
            mstrViewByIds = ""
            mstrViewByName = ""
            mstrAnalysis_Fields = ""
            mstrAnalysis_Join = ""
            mstrAnalysis_OrderBy = ""
            mstrAnalysis_OrderBy_GName = ""
            mstrReport_GroupName = ""

            Dim objUserDefRMode As New clsUserDef_ReportMode
            Dim dsList As DataSet = objUserDefRMode.GetList("List", enArutiReport.Employee_WithOut_ClockOut_Report, 0, 0)
            If dsList.Tables("List").Rows.Count > 0 Then
                Dim ar() As String = dsList.Tables("List").Rows(0).Item("transactionheadid").ToString().Split("|")
                If ar.Length > 0 Then
                    cboLocation.SelectedValue = ar(0).ToString()
                    chkShowEachEmpOnNewPage.Checked = CBool(ar(1).ToString())
                End If
            End If
            dsList = Nothing
            objUserDefRMode = Nothing

            objTRAAbsenteesExcuseReport.setDefaultOrderBy(0)
            txtOrderBy.Text = objTRAAbsenteesExcuseReport.OrderByDisplay
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "ResetValue", mstrModuleName)
        End Try
    End Sub

    Public Function SetFilter() As Boolean
        Try
            If CInt(cboLocation.SelectedValue) <= 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 3, "Location is compulsory information.Please Select Location."), enMsgBoxStyle.Information)
                cboLocation.Focus()
                Return False
            End If

            objTRAAbsenteesExcuseReport.SetDefaultValue()

            objTRAAbsenteesExcuseReport._FromDate = dtpFromDate.Value.Date
            objTRAAbsenteesExcuseReport._ToDate = dtpToDate.Value.Date
            objTRAAbsenteesExcuseReport._EmpId = cboEmployee.SelectedValue
            objTRAAbsenteesExcuseReport._EmpName = cboEmployee.Text
            objTRAAbsenteesExcuseReport._ExcuseId = CInt(cboExcuse.SelectedValue)
            objTRAAbsenteesExcuseReport._Excuse = cboExcuse.Text
            objTRAAbsenteesExcuseReport._LocationId = cboLocation.SelectedValue
            objTRAAbsenteesExcuseReport._Location = cboLocation.Text
            objTRAAbsenteesExcuseReport._ShowEachEmployeeOnNewPage = chkShowEachEmpOnNewPage.Checked
            objTRAAbsenteesExcuseReport._ViewByIds = mstrViewByIds
            objTRAAbsenteesExcuseReport._ViewIndex = mintViewIndex
            objTRAAbsenteesExcuseReport._ViewByName = mstrViewByName
            objTRAAbsenteesExcuseReport._Analysis_Fields = mstrAnalysis_Fields
            objTRAAbsenteesExcuseReport._Analysis_Join = mstrAnalysis_Join
            objTRAAbsenteesExcuseReport._Analysis_OrderBy = mstrAnalysis_OrderBy
            objTRAAbsenteesExcuseReport._Analysis_OrderBy_GName = mstrAnalysis_OrderBy_GName
            objTRAAbsenteesExcuseReport._Report_GroupName = mstrReport_GroupName
            objTRAAbsenteesExcuseReport._AdvanceFilter = mstrAdvanceFilter
            objTRAAbsenteesExcuseReport._YearId = FinancialYear._Object._YearUnkid
            Return True
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "SetFilter", mstrModuleName)
        End Try
    End Function

#End Region

#Region "Form's Events"

    Private Sub frmTRAAbsenteesExcuseReport_FormClosed(ByVal sender As Object, ByVal e As System.Windows.Forms.FormClosedEventArgs) Handles Me.FormClosed
        Try
            objTRAAbsenteesExcuseReport = Nothing
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmTRAAbsenteesExcuseReport_FormClosed", mstrModuleName)
        End Try
    End Sub

    Private Sub frmTRAAbsenteesExcuseReport_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            Call Language.setLanguage(Me.Name)
            OtherSettings()
            Me._Title = objTRAAbsenteesExcuseReport._ReportName
            Me._Message = objTRAAbsenteesExcuseReport._ReportDesc
            Call FillCombo()
            Call ResetValue()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmTRAAbsenteesExcuseReport_Load", mstrModuleName)
        End Try
    End Sub

    Private Sub frmTRAAbsenteesExcuseReport_KeyDown(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles Me.KeyDown
        Try
            If e.Control Then
                If e.KeyCode = Windows.Forms.Keys.R Then
                    Call frmTRAAbsenteesExcuseReport_Report_Click(Nothing, Nothing)
                End If
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmTRAAbsenteesExcuseReport_KeyDown", mstrModuleName)
        End Try
    End Sub

    Private Sub frmTRAAbsenteesExcuseReport_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles Me.KeyPress
        Try
            Select Case e.KeyChar
                Case CChar(ChrW(CInt(Windows.Forms.Keys.Enter)))
                    Windows.Forms.SendKeys.Send("{TAB}")
                    e.Handled = True
                    Exit Select
            End Select
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmTRAAbsenteesExcuseReport_KeyPress", mstrModuleName)
        End Try

    End Sub

#End Region

#Region "Buttons"

    Private Sub frmTRAAbsenteesExcuseReport_Report_Click(ByVal sender As Object, ByVal e As Aruti.Data.PrintButtonEventArgs) Handles Me.Report_Click
        Try
            If SetFilter() = False Then Exit Sub

            objTRAAbsenteesExcuseReport.generateReportNew(FinancialYear._Object._DatabaseName, _
                                                   User._Object._Userunkid, _
                                                   FinancialYear._Object._YearUnkid, _
                                                   Company._Object._Companyunkid, _
                                                   eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), _
                                                   eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), _
                                                   ConfigParameter._Object._UserAccessModeSetting, True, _
                                                   ConfigParameter._Object._ExportReportPath, _
                                                   ConfigParameter._Object._OpenAfterExport, _
                                                   0, e.Type, Aruti.Data.enExportAction.None)
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmTRAAbsenteesExcuseReport_Report_Click", mstrModuleName)
        End Try

    End Sub

    Private Sub frmTRAAbsenteesExcuseReport_Export_Click(ByVal sender As Object, ByVal e As Aruti.Data.PrintButtonEventArgs) Handles Me.Export_Click
        Try
            If SetFilter() = False Then Exit Sub

            objTRAAbsenteesExcuseReport.generateReportNew(FinancialYear._Object._DatabaseName, _
                                                   User._Object._Userunkid, _
                                                   FinancialYear._Object._YearUnkid, _
                                                   Company._Object._Companyunkid, _
                                                   eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), _
                                                   eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), _
                                                   ConfigParameter._Object._UserAccessModeSetting, True, _
                                                   ConfigParameter._Object._ExportReportPath, _
                                                   ConfigParameter._Object._OpenAfterExport, _
                                                   0, enPrintAction.None, e.Type)
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmTRAAbsenteesExcuseReport_Export_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub frmTRAAbsenteesExcuseReport_Reset_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Reset_Click
        Try
            Call ResetValue()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmTRAAbsenteesExcuseReport_Reset_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub frmTRAAbsenteesExcuseReport_Cancel_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Cancel_Click
        Try
            Me.Close()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmTRAAbsenteesExcuseReport_Cancel_Click", mstrModuleName)
        End Try

    End Sub

    Private Sub Form_AdvanceFilter_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.AdvanceFilter_Click
        Dim frm As New frmAdvanceSearch
        Try
            frm._Hr_EmployeeTable_Alias = "hremployee_master"
            frm.ShowDialog()
            mstrAdvanceFilter = frm._GetFilterString
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "Form_AdvanceFilter_Click", mstrModuleName)
        Finally
            If frm IsNot Nothing Then frm.Dispose()
        End Try
    End Sub

    Private Sub objbtnSearchEmployee_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnSearchEmployee.Click
        Dim frm As New frmCommonSearch
        Try
            frm.DataSource = cboEmployee.DataSource
            frm.ValueMember = cboEmployee.ValueMember
            frm.DisplayMember = cboEmployee.DisplayMember
            frm.CodeMember = "employeecode"
            If frm.DisplayDialog Then
                cboEmployee.SelectedValue = frm.SelectedValue
                cboEmployee.Focus()
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objbtnSearchEmployee_Click", mstrModuleName)
        Finally
            If frm IsNot Nothing Then frm.Dispose()
        End Try
    End Sub

    Private Sub frmTRAAbsenteesExcuseReport_Language_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Language_Click
        Dim objfrm As New frmLanguage
        Try
            If User._Object._Isrighttoleft = True Then
                objfrm.RightToLeft = Windows.Forms.RightToLeft.Yes
                objfrm.RightToLeftLayout = True
                Call Language.ctlRightToLeftlayOut(objfrm)
            End If

            Call SetMessages()

            objTRAAbsenteesExcuseReport.SetMessages()
            objfrm._Other_ModuleNames = "objTRAAbsenteesExcuseReport"
            objfrm.displayDialog(Me)

            Call Language.setLanguage(Me.Name)
            Call SetLanguage()

        Catch ex As System.Exception
            Call DisplayError.Show(-1, ex.Message, "frmTRAAbsenteesExcuseReport_Language_Click", mstrModuleName)
        Finally
            objfrm.Dispose()
            objfrm = Nothing
        End Try

    End Sub

    Private Sub objbtnSearchExcuse_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnSearchExcuse.Click
        Dim frm As New frmCommonSearch
        Try
            frm.DataSource = cboExcuse.DataSource
            frm.ValueMember = cboExcuse.ValueMember
            frm.DisplayMember = cboLocation.DisplayMember
            If frm.DisplayDialog Then
                cboExcuse.SelectedValue = frm.SelectedValue
                cboExcuse.Focus()
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objbtnSearchExcuse_Click", mstrModuleName)
        Finally
            If frm IsNot Nothing Then frm.Dispose()
        End Try
    End Sub

    Private Sub objbtnSearchLocation_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnSearchLocation.Click
        Dim frm As New frmCommonSearch
        Try
            frm.DataSource = cboLocation.DataSource
            frm.ValueMember = cboLocation.ValueMember
            frm.DisplayMember = cboLocation.DisplayMember
            If frm.DisplayDialog Then
                cboLocation.SelectedValue = frm.SelectedValue
                cboLocation.Focus()
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objbtnSearchLocation_Click", mstrModuleName)
        Finally
            If frm IsNot Nothing Then frm.Dispose()
        End Try
    End Sub

#End Region

#Region " Controls "

    Private Sub objbtnSort_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles objbtnSort.Click
        Try
            objTRAAbsenteesExcuseReport.setOrderBy(0)
            txtOrderBy.Text = objTRAAbsenteesExcuseReport.OrderByDisplay
        Catch ex As Exception
            DisplayError.Show(-1, ex.Message, "objbtnSort_Click", mstrModuleName)
        End Try
    End Sub

#End Region

#Region "LinkLabel Event"

    Private Sub lnkSetAnalysis_LinkClicked(ByVal sender As System.Object, ByVal e As System.Windows.Forms.LinkLabelLinkClickedEventArgs) Handles lnkSetAnalysis.LinkClicked
        Dim frm As New frmViewAnalysis
        Try
            frm.displayDialog()
            mstrViewByIds = frm._ReportBy_Ids
            mstrViewByName = frm._ReportBy_Name
            mintViewIndex = frm._ViewIndex
            mstrAnalysis_Fields = frm._Analysis_Fields
            mstrAnalysis_Join = frm._Analysis_Join
            mstrAnalysis_OrderBy = frm._Analysis_OrderBy
            mstrAnalysis_OrderBy_GName = frm._Analysis_OrderBy_GName
            mstrReport_GroupName = frm._Report_GroupName
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "lnkSetAnalysis_LinkClicked", mstrModuleName)
        Finally
            frm = Nothing
        End Try
    End Sub

    Private Sub lnkSave_LinkClicked(ByVal sender As System.Object, ByVal e As System.Windows.Forms.LinkLabelLinkClickedEventArgs) Handles lnkSave.LinkClicked
        Try
            Dim objUserDefRMode As New clsUserDef_ReportMode
            Try
                objUserDefRMode = New clsUserDef_ReportMode()
                objUserDefRMode._Reportunkid = enArutiReport.Employee_Absentees_With_Excuse_Report
                objUserDefRMode._Reporttypeid = 0
                objUserDefRMode._Reportmodeid = 0
                objUserDefRMode._Headtypeid = 0

                Dim intUnkid As Integer = -1

                objUserDefRMode._EarningTranHeadIds = cboLocation.SelectedValue & "|" & chkShowEachEmpOnNewPage.Checked
                intUnkid = objUserDefRMode.isExist(enArutiReport.Employee_Absentees_With_Excuse_Report, 0, 0, 0)

                objUserDefRMode._Reportmodeunkid = intUnkid
                If intUnkid <= 0 Then
                    objUserDefRMode.Insert()
                Else
                    objUserDefRMode.Update()
                End If
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 1, "Setting saved successfully."), enMsgBoxStyle.Information)
            Catch ex As Exception
                DisplayError.Show("-1", ex.Message, "lnkSave_LinkClicked", mstrModuleName)
            Finally
                objUserDefRMode = Nothing
            End Try
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "lnkSave_LinkClicked", mstrModuleName)
        End Try
    End Sub


#End Region

	'<Language> This Auto Generated Text Please Do Not Modify it.
#Region " Language & UI Settings "
	Private Sub OtherSettings()
		Try
			Me.SuspendLayout()
            Call SetLanguage()
			
			Me.gbFilterCriteria.GradientColor = GUI._eZeeContainerHeaderBackColor 
			Me.gbFilterCriteria.ForeColor = GUI._eZeeContainerHeaderForeColor 

			Me.gbSortBy.GradientColor = GUI._eZeeContainerHeaderBackColor 
			Me.gbSortBy.ForeColor = GUI._eZeeContainerHeaderForeColor 


			Me.ResumeLayout()
		Catch Ex As Exception
			DisplayError.Show("-1", ex.Message, "OtherSettings", mstrModuleName)
		End Try
	End Sub
			
			
	Private Sub SetLanguage()
		Try
			Me.Text = Language._Object.getCaption(Me.Name, Me.Text)
			
			Me.gbFilterCriteria.Text = Language._Object.getCaption(Me.gbFilterCriteria.Name, Me.gbFilterCriteria.Text)
			Me.lblEmpName.Text = Language._Object.getCaption(Me.lblEmpName.Name, Me.lblEmpName.Text)
			Me.lblFromDate.Text = Language._Object.getCaption(Me.lblFromDate.Name, Me.lblFromDate.Text)
			Me.gbSortBy.Text = Language._Object.getCaption(Me.gbSortBy.Name, Me.gbSortBy.Text)
			Me.lblOrderBy.Text = Language._Object.getCaption(Me.lblOrderBy.Name, Me.lblOrderBy.Text)
			Me.lblToDate.Text = Language._Object.getCaption(Me.lblToDate.Name, Me.lblToDate.Text)
			Me.lnkSetAnalysis.Text = Language._Object.getCaption(Me.lnkSetAnalysis.Name, Me.lnkSetAnalysis.Text)
			Me.LblLocation.Text = Language._Object.getCaption(Me.LblLocation.Name, Me.LblLocation.Text)
			Me.chkShowEachEmpOnNewPage.Text = Language._Object.getCaption(Me.chkShowEachEmpOnNewPage.Name, Me.chkShowEachEmpOnNewPage.Text)
			Me.lnkSave.Text = Language._Object.getCaption(Me.lnkSave.Name, Me.lnkSave.Text)
			Me.LblExcuse.Text = Language._Object.getCaption(Me.LblExcuse.Name, Me.LblExcuse.Text)

		Catch Ex As Exception
			DisplayError.Show("-1", ex.Message, "SetLanguage", mstrModuleName)
		End Try
	End Sub
			
			
	Private Sub SetMessages()
		Try
			Language.setMessage(mstrModuleName, 1, "Setting saved successfully.")
			Language.setMessage(mstrModuleName, 2, "Select")
			Language.setMessage(mstrModuleName, 3, "Location is compulsory information.Please Select Location.")

		Catch Ex As Exception
			DisplayError.Show("-1", ex.Message, "SetMessages", mstrModuleName)
		End Try
	End Sub
#End Region 'Language & UI Settings
	'</Language>
End Class
