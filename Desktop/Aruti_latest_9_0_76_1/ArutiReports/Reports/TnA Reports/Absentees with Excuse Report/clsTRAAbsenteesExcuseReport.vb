'************************************************************************************************************************************
'Class Name : clsTRAEmpMispunchReport.vb
'Purpose    :
'Date       :20-Feb-2022
'Written By :Pinkal
'Modified   :
'************************************************************************************************************************************

Imports eZeeCommonLib
Imports Aruti.Data
''' <summary>
''' Purpose: Report Generation Class 
''' Developer: Pinkal
''' </summary>
''' 
Public Class clsTRAAbsenteesExcuseReport
    Inherits IReportData
    Private Shared ReadOnly mstrModuleName As String = "clsTRAAbsenteesExcuseReport"
    Private mstrReportId As String = enArutiReport.Employee_Absentees_With_Excuse_Report '286
    Dim objDataOperation As clsDataOperation

#Region " Constructor "
    Public Sub New(ByVal intLangId As Integer, ByVal intCompanyId As Integer)
        Me.setReportData(CInt(mstrReportId), intLangId, intCompanyId)
        Call Create_OnDetailReport()
    End Sub
#End Region

#Region " Private variables "

    Private mdtFromDate As DateTime = Nothing
    Private mdtToDate As DateTime = Nothing
    Private mintEmpId As Integer = 0
    Private mstrEmpName As String = ""
    Private mintExcuseId As Integer = 0
    Private mstrExcuse As String = ""
    Private mintLocationId As Integer = 0
    Private mstrLocation As String = ""
    Private mstrOrderByQuery As String = ""
    Private mintViewIndex As Integer = -1
    Private mstrViewByIds As String = String.Empty
    Private mstrViewByName As String = String.Empty
    Private mstrAnalysis_Fields As String = ""
    Private mstrAnalysis_Join As String = ""
    Private mstrAnalysis_OrderBy As String = ""
    Private mstrAnalysis_OrderBy_GName As String = ""
    Private mstrReport_GroupName As String = ""
    Private mstrAdvanceFilter As String = String.Empty
    Private mblnEachEmployeeOnPage As Boolean = False
    Dim Rpt As CrystalDecisions.CrystalReports.Engine.ReportClass
    Private mintUserUnkid As Integer = -1
    Private mintCompanyUnkid As Integer = -1
    Private mstrUserAccessFilter As String = String.Empty
    Private mintYearID As Integer = -1
    Private mblnIncludeAccessFilterQry As Boolean = True

#End Region

#Region " Properties "


    Public WriteOnly Property _FromDate() As DateTime
        Set(ByVal value As DateTime)
            mdtFromDate = value
        End Set
    End Property

    Public WriteOnly Property _ToDate() As DateTime
        Set(ByVal value As DateTime)
            mdtToDate = value
        End Set
    End Property

    Public WriteOnly Property _EmpId() As Integer
        Set(ByVal value As Integer)
            mintEmpId = value
        End Set
    End Property

    Public WriteOnly Property _EmpName() As String
        Set(ByVal value As String)
            mstrEmpName = value
        End Set
    End Property

    Public WriteOnly Property _ExcuseId() As Integer
        Set(ByVal value As Integer)
            mintExcuseId = value
        End Set
    End Property

    Public WriteOnly Property _Excuse() As String
        Set(ByVal value As String)
            mstrExcuse = value
        End Set
    End Property

    Public WriteOnly Property _LocationId() As Integer
        Set(ByVal value As Integer)
            mintLocationId = value
        End Set
    End Property

    Public WriteOnly Property _Location() As String
        Set(ByVal value As String)
            mstrLocation = value
        End Set
    End Property



    Public WriteOnly Property _ViewIndex() As Integer
        Set(ByVal value As Integer)
            mintViewIndex = value
        End Set
    End Property

    Public WriteOnly Property _ViewByIds() As String
        Set(ByVal value As String)
            mstrViewByIds = value
        End Set
    End Property

    Public WriteOnly Property _ViewByName() As String
        Set(ByVal value As String)
            mstrViewByName = value
        End Set
    End Property

    Public WriteOnly Property _Analysis_Fields() As String
        Set(ByVal value As String)
            mstrAnalysis_Fields = value
        End Set
    End Property

    Public WriteOnly Property _Analysis_Join() As String
        Set(ByVal value As String)
            mstrAnalysis_Join = value
        End Set
    End Property

    Public WriteOnly Property _Analysis_OrderBy() As String
        Set(ByVal value As String)
            mstrAnalysis_OrderBy = value
        End Set
    End Property

    Public WriteOnly Property _Analysis_OrderBy_GName() As String
        Set(ByVal value As String)
            mstrAnalysis_OrderBy_GName = value
        End Set
    End Property

    Public WriteOnly Property _Report_GroupName() As String
        Set(ByVal value As String)
            mstrReport_GroupName = value
        End Set
    End Property

    Public WriteOnly Property _AdvanceFilter() As String
        Set(ByVal value As String)
            mstrAdvanceFilter = value
        End Set
    End Property

    Public WriteOnly Property _ShowEachEmployeeOnNewPage() As Boolean
        Set(ByVal value As Boolean)
            mblnEachEmployeeOnPage = value
        End Set
    End Property

    Public ReadOnly Property _Rpt() As CrystalDecisions.CrystalReports.Engine.ReportClass
        Get
            Return Rpt
        End Get
    End Property

    Public WriteOnly Property _CompanyUnkId() As Integer
        Set(ByVal value As Integer)
            mintCompanyUnkid = value
        End Set
    End Property

    Public WriteOnly Property _UserUnkId() As Integer
        Set(ByVal value As Integer)
            mintUserUnkid = value
        End Set
    End Property

    Public WriteOnly Property _UserAccessFilter() As String
        Set(ByVal value As String)
            mstrUserAccessFilter = value
        End Set
    End Property

    Public WriteOnly Property _YearId() As Integer
        Set(ByVal value As Integer)
            mintYearID = value
        End Set
    End Property

    Public Property _IncludeAccessFilterQry() As Boolean
        Get
            Return mblnIncludeAccessFilterQry
        End Get
        Set(ByVal value As Boolean)
            mblnIncludeAccessFilterQry = value
        End Set
    End Property


#End Region

#Region "Public Function & Procedures "

    Public Sub SetDefaultValue()
        Try
            mdtFromDate = Nothing
            mdtToDate = Nothing
            mintEmpId = 0
            mstrEmpName = ""
            mintExcuseId = 0
            mstrExcuse = ""
            mintLocationId = 0
            mstrLocation = ""
            mblnEachEmployeeOnPage = True
            mstrOrderByQuery = ""
            mintViewIndex = -1
            mstrViewByIds = ""
            mstrOrderByQuery = ""
            mstrAdvanceFilter = ""
            mstrAnalysis_OrderBy_GName = ""
            mstrReport_GroupName = ""
            mblnIncludeAccessFilterQry = True
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: SetDefaultValue; Module Name: " & mstrModuleName)
        End Try
    End Sub

    Public Sub FilterTitleAndFilterQuery()
        Me._FilterQuery = ""
        Me._FilterTitle = ""

        Try
            objDataOperation.AddParameter("@FromDate", SqlDbType.NVarChar, eZeeDataType.DATETIME_SIZE, eZeeDate.convertDate(mdtFromDate))
            objDataOperation.AddParameter("@ToDate", SqlDbType.NVarChar, eZeeDataType.DATETIME_SIZE, eZeeDate.convertDate(mdtToDate))
            Me._FilterTitle &= Language.getMessage(mstrModuleName, 11, "From Date: ") & " " & mdtFromDate.ToShortDateString & " "
            Me._FilterTitle &= Language.getMessage(mstrModuleName, 12, "To Date: ") & " " & mdtToDate.ToShortDateString & " "
            Me._FilterQuery &= " AND CONVERT(VARCHAR(8), lvleaveIssue_tran.leavedate, 112) BETWEEN @FromDate AND @ToDate "


            If mintEmpId > 0 Then
                objDataOperation.AddParameter("@employeeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintEmpId)
                Me._FilterQuery &= " AND lvleaveIssue_master.employeeunkid = @employeeunkid "
                Me._FilterTitle &= Language.getMessage(mstrModuleName, 13, "Employee : ") & " " & mstrEmpName & " "
            End If

            If mintExcuseId > 0 Then
                objDataOperation.AddParameter("@leavetypeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintExcuseId)
                Me._FilterQuery &= " AND lvleavetype_master.leavetypeunkid = @leavetypeunkid "
                Me._FilterTitle &= Language.getMessage(mstrModuleName, 14, "Excuse : ") & " " & mstrExcuse & " "
            End If

            If Me.OrderByQuery <> "" Then
                mstrOrderByQuery &= "  ORDER BY " & Me.OrderByQuery
            End If

            Me._FilterTitle &= Language.getMessage(mstrModuleName, 20, " Order By : ") & " " & Me.OrderByDisplay

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: FilterTitleAndFilterQuery; Module Name: " & mstrModuleName)
        End Try
    End Sub

    Public Overrides Sub generateReport(ByVal pintReportType As Integer, Optional ByVal PrintAction As Aruti.Data.enPrintAction = Aruti.Data.enPrintAction.Preview, Optional ByVal ExportAction As Aruti.Data.enExportAction = Aruti.Data.enExportAction.None)
       
    End Sub

    Public Overrides Sub generateReportNew(ByVal xDatabaseName As String, ByVal xUserUnkid As Integer, ByVal xYearUnkid As Integer, ByVal xCompanyUnkid As Integer, ByVal xPeriodStart As Date, ByVal xPeriodEnd As Date, ByVal xUserModeSetting As String, ByVal xOnlyApproved As Boolean, ByVal xExportReportPath As String, ByVal xOpenReportAfterExport As Boolean, ByVal pintReportType As Integer, Optional ByVal PrintAction As Aruti.Data.enPrintAction = Aruti.Data.enPrintAction.Preview, Optional ByVal ExportAction As Aruti.Data.enExportAction = Aruti.Data.enExportAction.None, Optional ByVal xBaseCurrencyId As Integer = 0)
        Dim objRpt As CrystalDecisions.CrystalReports.Engine.ReportClass = Nothing
        Dim strReportExportFile As String = ""
        Try

            If mintCompanyUnkid <= 0 Then
                mintCompanyUnkid = Company._Object._Companyunkid
            End If

            Company._Object._Companyunkid = mintCompanyUnkid
            ConfigParameter._Object._Companyunkid = mintCompanyUnkid

            If mintUserUnkid <= 0 Then
                mintUserUnkid = User._Object._Userunkid
            End If

            User._Object._Userunkid = mintUserUnkid

            objRpt = Generate_DetailReport(xDatabaseName, xUserUnkid, xYearUnkid, xCompanyUnkid, xPeriodStart, xPeriodEnd, xUserModeSetting, xOnlyApproved)

            Rpt = objRpt

            If Not IsNothing(objRpt) Then
                Call ReportExecute(objRpt, PrintAction, ExportAction, xExportReportPath, xOpenReportAfterExport)
            End If
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: generateReport; Module Name: " & mstrModuleName)
        End Try
    End Sub

    Public Overrides Sub setDefaultOrderBy(ByVal intReportType As Integer)
        OrderByDisplay = ""
        OrderByQuery = ""
        Try
            OrderByDisplay = iColumn_DetailReport.ColumnItem(0).DisplayName
            OrderByQuery = iColumn_DetailReport.ColumnItem(0).Name
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: setDefaultOrderBy; Module Name: " & mstrModuleName)
        End Try
    End Sub

    Public Overrides Sub setOrderBy(ByVal intReportType As Integer)
        Try
            Call OrderByExecute(iColumn_DetailReport)
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: setOrderBy; Module Name: " & mstrModuleName)
        End Try
    End Sub

#End Region

#Region " Report Generation "

    Dim iColumn_DetailReport As New IColumnCollection

    Public Property Field_OnDetailReport() As IColumnCollection
        Get
            Return iColumn_DetailReport
        End Get
        Set(ByVal value As IColumnCollection)
            iColumn_DetailReport = value
        End Set
    End Property

    Private Sub Create_OnDetailReport()
        Try
            iColumn_DetailReport.Clear()
            iColumn_DetailReport.Add(New IColumn("ISNULL(hremployee_master.employeecode,'') + ' - ' + ISNULL(hremployee_master.firstname,'') + ' ' + ISNULL(hremployee_master.othername,'') + ' ' + ISNULL(hremployee_master.surname,'')", Language.getMessage(mstrModuleName, 1, "Employee")))
            iColumn_DetailReport.Add(New IColumn("CONVERT(VARCHAR(8), lvleaveIssue_tran.leavedate, 112)", Language.getMessage(mstrModuleName, 2, "Date")))
            iColumn_DetailReport.Add(New IColumn("ISNULL(lvleavetype_master.leavename,'')", Language.getMessage(mstrModuleName, 8, "Excuse")))
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: Create_OnDetailReport; Module Name: " & mstrModuleName)
        End Try
    End Sub

    Public Function Generate_DetailReport(ByVal strDatabaseName As String, _
                                          ByVal intUserUnkid As Integer, _
                                          ByVal intYearUnkid As Integer, _
                                          ByVal intCompanyUnkid As Integer, _
                                          ByVal dtPeriodStart As Date, _
                                          ByVal dtPeriodEnd As Date, _
                                          ByVal strUserModeSetting As String, _
                                          ByVal blnOnlyApproved As Boolean) As CrystalDecisions.CrystalReports.Engine.ReportClass
        Dim objRpt As CrystalDecisions.CrystalReports.Engine.ReportClass = Nothing
        Dim StrQ As String = ""
        Dim dsList As New DataSet
        Dim rpt_Data As ArutiReport.Designer.dsArutiReport
        Try

            objDataOperation = New clsDataOperation
            objDataOperation.ClearParameters()


            StrQ = "SET ARITHABORT ON"
            objDataOperation.ExecNonQuery(StrQ)
            If objDataOperation.ErrorMessage <> "" Then
                Throw New Exception(objDataOperation.ErrorNumber & ":" & objDataOperation.ErrorMessage)
            End If

            objDataOperation.ClearParameters()

            Dim xDateJoinQry, xDateFilterQry, xUACQry, xUACFiltrQry, xAdvanceJoinQry As String
            xDateJoinQry = "" : xDateFilterQry = "" : xUACQry = "" : xUACFiltrQry = "" : xAdvanceJoinQry = ""
            Call GetDatesFilterString(xDateJoinQry, xDateFilterQry, dtPeriodStart, dtPeriodEnd, , , strDatabaseName)
            If mblnIncludeAccessFilterQry Then Call NewAccessLevelFilterString(xUACQry, xUACFiltrQry, dtPeriodEnd, blnOnlyApproved, strDatabaseName, intUserUnkid, intCompanyUnkid, intYearUnkid, strUserModeSetting)
            Call GetAdvanceFilterQry(xAdvanceJoinQry, dtPeriodEnd, strDatabaseName)

            StrQ = " SELECT hremployee_master.employeeunkid  " & _
                       ",  ROW_NUMBER() OVER (ORDER BY ISNULL(hremployee_master.employeecode, ''),CONVERT(VARCHAR(8), lvleaveIssue_tran.leavedate, 112)) AS SrNo " & _
                       ", ISNULL(hremployee_master.employeecode,'') + ' - ' + ISNULL(hremployee_master.firstname,'') + ' ' + ISNULL(hremployee_master.othername,'') + ' ' + ISNULL(hremployee_master.surname,'') AS EmpCodeName " & _
                       ", ISNULL(dept.name, '') AS department " & _
                       ", CONVERT(VARCHAR(8), lvleaveIssue_tran.leavedate, 112) AS date " & _
                       ", ISNULL(lvleavetype_master.leavename,'') AS Excuse " & _
                       ", ISNULL(CONVERT(VARCHAR(8), lvleaveform.approve_stdate, 112),  '') AS approvestdate " & _
                       ", ISNULL(CONVERT(VARCHAR(8), lvleaveform.approve_eddate, 112),'') AS approveeddate "


            Select Case mintLocationId

                Case enAllocation.BRANCH

                    StrQ &= ", ISNULL(hrstation_master.name, '') AS location "

                Case enAllocation.DEPARTMENT_GROUP

                    StrQ &= ", ISNULL(hrdepartment_group_master.name, '') AS location "

                Case enAllocation.SECTION_GROUP

                    StrQ &= ", ISNULL(hrsectiongroup_master.name, '') AS location "

                Case enAllocation.SECTION

                    StrQ &= ", ISNULL(hrsection_master.name, '') AS location "

                Case enAllocation.UNIT_GROUP

                    StrQ &= ", ISNULL(hrunitgroup_master.name, '') AS location "

                Case enAllocation.UNIT

                    StrQ &= ", ISNULL(hrunit_master.name, '') AS location "

                Case enAllocation.TEAM

                    StrQ &= ", ISNULL(hrteam_master.name, '') AS location "

                Case enAllocation.JOB_GROUP

                    StrQ &= ", ISNULL(jg.name, '') AS location "

                Case enAllocation.JOBS

                    StrQ &= ", ISNULL(jb.job_name, '') AS location "

                Case enAllocation.CLASS_GROUP

                    StrQ &= ", ISNULL(hrclassgroup_master.name, '') AS location "

                Case enAllocation.CLASSES

                    StrQ &= ", ISNULL(hrclasses_master.name, '') AS location "

                Case enAllocation.COST_CENTER

                    StrQ &= ", ISNULL(prcostcenter_master.costcentername, '') AS location "

            End Select


            If mintViewIndex > 0 Then
                StrQ &= mstrAnalysis_Fields
            Else
                StrQ &= ", '' AS GName "
            End If

            StrQ &= " FROM lvleaveIssue_tran " & _
                         " LEFT JOIN lvleaveIssue_master ON lvleaveIssue_master.leaveissueunkid = lvleaveIssue_tran.leaveissueunkid   " & _
                         " LEFT JOIN lvleaveform ON lvleaveform.formunkid = lvleaveIssue_master.formunkid AND lvleaveform.leavetypeunkid = lvleaveIssue_master.leavetypeunkid  " & _
                         " LEFT JOIN lvleavetype_master ON lvleavetype_master.leavetypeunkid = lvleaveform.leavetypeunkid " & _
                         " LEFT JOIN hremployee_master ON hremployee_master.employeeunkid = lvleaveform.employeeunkid "

            If xAdvanceJoinQry.Trim.Length > 0 Then
                StrQ &= xAdvanceJoinQry
            End If

            StrQ &= " LEFT JOIN " & _
                         "   ( " & _
                         "       SELECT " & _
                         "         stationunkid " & _
                         "        ,deptgroupunkid " & _
                         "        ,departmentunkid " & _
                         "        ,sectiongroupunkid " & _
                         "        ,sectionunkid " & _
                         "        ,unitgroupunkid " & _
                         "        ,unitunkid " & _
                         "        ,teamunkid " & _
                         "        ,classgroupunkid " & _
                         "        ,classunkid " & _
                         "        ,employeeunkid " & _
                         "        ,ROW_NUMBER()OVER(PARTITION BY employeeunkid ORDER BY effectivedate DESC) AS rno " & _
                         "       FROM hremployee_transfer_tran " & _
                         "       WHERE isvoid = 0 AND CONVERT(CHAR(8),effectivedate,112) <= '" & eZeeDate.convertDate(dtPeriodEnd) & "' " & _
                         "   ) AS Alloc ON Alloc.employeeunkid = hremployee_master.employeeunkid AND Alloc.rno = 1 " & _
                         " LEFT JOIN hrdepartment_master dept ON Alloc.departmentunkid = dept.departmentunkid "



            Select Case mintLocationId

                Case enAllocation.BRANCH

                    StrQ &= " LEFT JOIN hrstation_master ON hrstation_master.stationunkid = Alloc.stationunkid "

                Case enAllocation.DEPARTMENT_GROUP

                    StrQ &= " LEFT JOIN hrdepartment_group_master ON hrdepartment_group_master.deptgroupunkid = Alloc.deptgroupunkid "

                Case enAllocation.SECTION_GROUP

                    StrQ &= " LEFT JOIN hrsectiongroup_master ON hrsectiongroup_master.sectiongroupunkid = Alloc.sectiongroupunkid "

                Case enAllocation.SECTION

                    StrQ &= " LEFT JOIN hrsection_master ON hrsection_master.sectionunkid = Alloc.sectionunkid "

                Case enAllocation.UNIT_GROUP

                    StrQ &= " LEFT JOIN hrunitgroup_master ON hrunitgroup_master.unitgroupunkid = Alloc.unitgroupunkid "

                Case enAllocation.UNIT

                    StrQ &= " LEFT JOIN hrunit_master ON hrunit_master.unitunkid = Alloc.unitunkid "

                Case enAllocation.TEAM

                    StrQ &= " LEFT JOIN hrteam_master ON hrteam_master.teamunkid = Alloc.teamunkid "

                Case enAllocation.JOB_GROUP

                    StrQ &= " LEFT JOIN " & _
                                  "   ( " & _
                                  "       SELECT " & _
                                  "           jobgroupunkid " & _
                                  "           ,employeeunkid " & _
                                  "           ,ROW_NUMBER()OVER(PARTITION BY employeeunkid ORDER BY effectivedate DESC) AS rno " & _
                                  "       FROM hremployee_categorization_tran " & _
                                  "       WHERE isvoid = 0 AND CONVERT(CHAR(8),effectivedate,112) <= '" & eZeeDate.convertDate(dtPeriodEnd) & "' " & _
                                  "   ) AS Jobgrp ON Jobgrp.employeeunkid = hremployee_master.employeeunkid AND Jobgrp.rno = 1 " & _
                                  " LEFT JOIN hrjobgroup_master jg ON Jobgrp.jobgroupunkid = jg.jobgroupunkid "

                Case enAllocation.JOBS

                    StrQ &= " LEFT JOIN " & _
                                  "   ( " & _
                                  "       SELECT " & _
                                  "           jobunkid " & _
                                  "           ,employeeunkid " & _
                                  "           ,ROW_NUMBER()OVER(PARTITION BY employeeunkid ORDER BY effectivedate DESC) AS rno " & _
                                  "       FROM hremployee_categorization_tran " & _
                                  "       WHERE isvoid = 0 AND CONVERT(CHAR(8),effectivedate,112) <= '" & eZeeDate.convertDate(dtPeriodEnd) & "' " & _
                                  "   ) AS Jobs ON Jobs.employeeunkid = hremployee_master.employeeunkid AND Jobs.rno = 1 " & _
                                  " LEFT JOIN hrjob_master jb ON Jobs.jobunkid = jb.jobunkid "

                Case enAllocation.CLASS_GROUP

                    StrQ &= " LEFT JOIN hrclassgroup_master ON hrclassgroup_master.classgroupunkid = Alloc.classgroupunkid "

                Case enAllocation.CLASSES

                    StrQ &= " LEFT JOIN hrclasses_master ON hrclasses_master.classesunkid = Alloc.classunkid "

                Case enAllocation.COST_CENTER

                    StrQ &= " LEFT JOIN " & _
                                  "   ( " & _
                                  "       SELECT " & _
                                  "           cctranheadvalueid " & _
                                  "           ,employeeunkid " & _
                                  "           ,ROW_NUMBER()OVER(PARTITION BY employeeunkid ORDER BY effectivedate DESC) AS rno " & _
                                  "       FROM hremployee_cctranhead_tran " & _
                                  "       WHERE isvoid = 0 AND istransactionhead = 0 AND CONVERT(CHAR(8),effectivedate,112) <= '" & eZeeDate.convertDate(dtPeriodEnd) & "' " & _
                                  "   ) AS CC ON CC.employeeunkid = hremployee_master.employeeunkid AND CC.rno = 1 " & _
                                  " LEFT JOIN prcostcenter_master ON prcostcenter_master.costcenterunkid = CC.cctranheadvalueid "

            End Select


            If mintViewIndex > 0 Then
                StrQ &= mstrAnalysis_Join
            End If

            If xDateJoinQry.Trim.Length > 0 Then
                StrQ &= xDateJoinQry
            End If

            If xUACQry.Trim.Length > 0 Then
                StrQ &= xUACQry
            End If

            StrQ &= " WHERE  lvleaveIssue_tran.isvoid = 0 AND lvleaveIssue_master.isvoid = 0 AND lvleaveform.isvoid = 0 AND lvleavetype_master.isexcuseleave =1 "

            If xUACFiltrQry.Trim.Length > 0 Then
                StrQ &= " AND " & xUACFiltrQry
            End If

            If xDateFilterQry.Trim.Length > 0 Then
                StrQ &= xDateFilterQry & " "
            End If

            If mstrAdvanceFilter.Trim.Length > 0 Then
                StrQ &= " AND " & mstrAdvanceFilter
            End If

            Call FilterTitleAndFilterQuery()

            StrQ &= Me._FilterQuery

            StrQ &= mstrOrderByQuery

            dsList = objDataOperation.ExecQuery(StrQ, "DataTable")

            If objDataOperation.ErrorMessage <> "" Then
                Throw New Exception(objDataOperation.ErrorNumber & ":" & objDataOperation.ErrorMessage)
            End If


            rpt_Data = New ArutiReport.Designer.dsArutiReport
            For Each dtRow As DataRow In dsList.Tables("DataTable").Rows
                Dim rpt_Row As DataRow = rpt_Data.Tables("ArutiTable").NewRow
                rpt_Row.Item("Column1") = dtRow.Item("GName").ToString().Trim()
                rpt_Row.Item("Column2") = dtRow.Item("EmpCodeName").ToString().Trim()
                rpt_Row.Item("Column3") = eZeeDate.convertDate(dtRow.Item("date").ToString().Trim()).ToShortDateString()
                rpt_Row.Item("Column4") = dtRow.Item("location").ToString().Trim()
                rpt_Row.Item("Column5") = dtRow.Item("department").ToString().Trim()
                rpt_Row.Item("Column6") = dtRow.Item("Excuse").ToString()
                rpt_Row.Item("Column7") = eZeeDate.convertDate(dtRow.Item("approvestdate").ToString().Trim()).ToShortDateString()
                rpt_Row.Item("Column8") = eZeeDate.convertDate(dtRow.Item("approveeddate").ToString().Trim()).ToShortDateString()
                rpt_Data.Tables("ArutiTable").Rows.Add(rpt_Row)
            Next

            objRpt = New ArutiReport.Designer.rptTRAAbsenteesExcuseReport
            ConfigParameter._Object.GetReportSettings(CInt(mstrReportId))
            Dim arrImageRow As DataRow = Nothing
            arrImageRow = rpt_Data.Tables("ArutiImage").NewRow()


            ReportFunction.Logo_Display(objRpt, _
                                        ConfigParameter._Object._IsDisplayLogo, _
                                        ConfigParameter._Object._ShowLogoRightSide, _
                                        "arutiLogo1", _
                                        "arutiLogo2", _
                                        arrImageRow, _
                                        "txtCompanyName", _
                                        "txtReportName", _
                                        "txtFilterDescription", _
                                        ConfigParameter._Object._GetLeftMargin, _
                                        ConfigParameter._Object._GetRightMargin)

            rpt_Data.Tables("ArutiImage").Rows.Add(arrImageRow)

            If rpt_Data.Tables("ArutiTable").Rows.Count <= 0 Then
                rpt_Data.Tables("ArutiTable").Rows.Add("")
            End If

            If ConfigParameter._Object._IsShowPreparedBy = True Then
                Call ReportFunction.TextChange(objRpt, "lblPreparedBy", Language.getMessage(mstrModuleName, 15, "Prepared By :"))
                Call ReportFunction.TextChange(objRpt, "txtPreparedBy", User._Object._Username)
            Else
                Call ReportFunction.EnableSuppressSection(objRpt, "ReportFooterSection4", True)
            End If

            If ConfigParameter._Object._IsShowCheckedBy = True Then
                Call ReportFunction.TextChange(objRpt, "lblCheckedBy", Language.getMessage(mstrModuleName, 16, "Checked By :"))
            Else
                Call ReportFunction.EnableSuppressSection(objRpt, "ReportFooterSection3", True)
            End If

            If ConfigParameter._Object._IsShowApprovedBy = True Then
                Call ReportFunction.TextChange(objRpt, "lblApprovedBy", Language.getMessage(mstrModuleName, 17, "Approved By :"))
            Else
                Call ReportFunction.EnableSuppressSection(objRpt, "ReportFooterSection2", True)
            End If

            If ConfigParameter._Object._IsShowReceivedBy = True Then
                Call ReportFunction.TextChange(objRpt, "lblReceivedBy", Language.getMessage(mstrModuleName, 18, "Received By :"))
            Else
                Call ReportFunction.EnableSuppressSection(objRpt, "ReportFooterSection1", True)
            End If

            If mblnEachEmployeeOnPage = True Then
                ReportFunction.EnableNewPageBefore(objRpt, "GroupHeaderSection1", True)
                objRpt.DataDefinition.FormulaFields("frmlNewPage").Text = "if Previous ({ArutiTable.Column1}) <> {ArutiTable.Column1} then 0 Else 1 "
            Else
                ReportFunction.EnableNewPageBefore(objRpt, "GroupHeaderSection1", False)
                objRpt.DataDefinition.FormulaFields("frmlNewPage").Text = "0"
            End If

            objRpt.SetDataSource(rpt_Data)

            Call ReportFunction.TextChange(objRpt, "txtSrNo", Language.getMessage(mstrModuleName, 4, "Sr.No"))
            Call ReportFunction.TextChange(objRpt, "txtDate", Language.getMessage(mstrModuleName, 5, "Date"))
            Call ReportFunction.TextChange(objRpt, "txtLocation", Language.getMessage(mstrModuleName, 6, "Location"))
            Call ReportFunction.TextChange(objRpt, "txtDepartment", Language.getMessage(mstrModuleName, 7, "Department"))
            Call ReportFunction.TextChange(objRpt, "txtExcuseLeave", Language.getMessage(mstrModuleName, 8, "Excuse"))
            Call ReportFunction.TextChange(objRpt, "txtAppStartDate", Language.getMessage(mstrModuleName, 9, "Approved Start Date"))
            Call ReportFunction.TextChange(objRpt, "txtAppEndDate", Language.getMessage(mstrModuleName, 10, "Approved End Date"))
            Call ReportFunction.TextChange(objRpt, "txtGroupName", mstrReport_GroupName)
            Call ReportFunction.TextChange(objRpt, "txtEmployee", Language.getMessage(mstrModuleName, 3, "Employee: "))
            Call ReportFunction.TextChange(objRpt, "txtTotalEmployee", Language.getMessage(mstrModuleName, 19, "Total Employee : "))

            If mintViewIndex < 0 Then
                Call ReportFunction.EnableSuppressSection(objRpt, "GroupHeaderSection1", True)
                Call ReportFunction.EnableSuppressSection(objRpt, "GroupFooterSection1", True)
            End If


            Call ReportFunction.TextChange(objRpt, "lblPrintedBy", Language.getMessage(mstrModuleName, 21, "Printed By :"))
            Call ReportFunction.TextChange(objRpt, "lblPrintedDate", Language.getMessage(mstrModuleName, 22, "Printed Date :"))

            Call ReportFunction.TextChange(objRpt, "txtPrintedBy", Me._UserName)
            Call ReportFunction.TextChange(objRpt, "txtPrintedDate", Me._PrintDate)

            Call ReportFunction.TextChange(objRpt, "txtReportName", Me._ReportName)
            Call ReportFunction.TextChange(objRpt, "txtCompanyName", Me._CompanyName)
            Call ReportFunction.TextChange(objRpt, "txtFilterDescription", Me._FilterTitle)

            Return objRpt
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: Generate_DetailReport; Module Name: " & mstrModuleName)
            Return Nothing
        End Try
    End Function

#End Region



	'<Language> This Auto Generated Text Please Do Not Modify it.
#Region " Language & UI Settings "
	Public Shared Sub SetMessages()
		Try
			Language.setMessage(mstrModuleName, 1, "Employee")
			Language.setMessage(mstrModuleName, 2, "Date")
			Language.setMessage(mstrModuleName, 3, "Employee:")
			Language.setMessage(mstrModuleName, 4, "Sr.No")
			Language.setMessage(mstrModuleName, 5, "Date")
			Language.setMessage(mstrModuleName, 6, "Location")
			Language.setMessage(mstrModuleName, 7, "Department")
			Language.setMessage(mstrModuleName, 8, "Excuse")
			Language.setMessage(mstrModuleName, 9, "Approved Start Date")
			Language.setMessage(mstrModuleName, 10, "Approved End Date")
			Language.setMessage(mstrModuleName, 11, "From Date:")
			Language.setMessage(mstrModuleName, 12, "To Date:")
			Language.setMessage(mstrModuleName, 13, "Employee :")
			Language.setMessage(mstrModuleName, 14, "Excuse :")
			Language.setMessage(mstrModuleName, 15, "Prepared By :")
			Language.setMessage(mstrModuleName, 16, "Checked By :")
			Language.setMessage(mstrModuleName, 17, "Approved By :")
			Language.setMessage(mstrModuleName, 18, "Received By :")
			Language.setMessage(mstrModuleName, 19, "Total Employee :")
			Language.setMessage(mstrModuleName, 20, " Order By :")
			Language.setMessage(mstrModuleName, 21, "Printed By :")
			Language.setMessage(mstrModuleName, 22, "Printed Date :")

		Catch Ex As Exception
			DisplayError.Show("-1", ex.Message, "SetMessages", mstrModuleName)
		End Try
	End Sub
#End Region 'Language & UI Settings
	'</Language>
End Class
