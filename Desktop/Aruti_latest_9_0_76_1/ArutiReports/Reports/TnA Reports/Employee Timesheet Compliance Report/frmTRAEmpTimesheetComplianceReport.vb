Imports Aruti.Data
Imports eZeeCommonLib
Imports Aruti
Imports ArutiReports

'Last Message index= 1

Public Class frmTRAEmpTimesheetComplianceReport
    Private mstrModuleName As String = "frmTRAEmpTimesheetComplianceReport"
    Private objTRAEmpTimesheetComplianceReport As clsTRAEmpTimesheetComplianceReport

#Region "Constructor"
    Public Sub New()
        objTRAEmpTimesheetComplianceReport = New clsTRAEmpTimesheetComplianceReport(User._Object._Languageunkid, Company._Object._Companyunkid)
        objTRAEmpTimesheetComplianceReport.SetDefaultValue()
        InitializeComponent()
    End Sub
#End Region

#Region "Private Variables"

    Private mstrViewByIds As String = String.Empty
    Private mstrViewByName As String = String.Empty
    Private mintViewIndex As Integer = -1
    Private mstrAnalysis_Fields As String = ""
    Private mstrAnalysis_Join As String = ""
    Private mstrAnalysis_OrderBy As String = ""
    Private mstrReport_GroupName As String = ""
    Private mstrAnalysis_OrderBy_GName As String = ""
    Private mstrAdvanceFilter As String = String.Empty

#End Region

#Region "Private Function"

    Public Sub FillCombo()
        Try
            Dim objEmp As New clsEmployee_Master
            Dim dsList As New DataSet

            dsList = objEmp.GetEmployeeList(FinancialYear._Object._DatabaseName, _
                                            User._Object._Userunkid, _
                                            FinancialYear._Object._YearUnkid, _
                                            Company._Object._Companyunkid, _
                                            eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), _
                                            eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), _
                                            ConfigParameter._Object._UserAccessModeSetting, _
                                            True, ConfigParameter._Object._IsIncludeInactiveEmp, "Employee", True)

            With cboEmployee
                .ValueMember = "employeeunkid"
                .DisplayMember = "employeename"
                .DataSource = dsList.Tables(0)
                .SelectedValue = 0
            End With
            objEmp = Nothing
            dsList = Nothing


            Dim objMaster As New clsMasterData
            dsList = objMaster.GetEAllocation_Notification("List", "", False, False)
            Dim dtTable As DataTable = New DataView(dsList.Tables(0), "Id <> " & enAllocation.DEPARTMENT, "", DataViewRowState.CurrentRows).ToTable()
            Dim drRow As DataRow = dtTable.NewRow
            drRow("Id") = 0
            drRow("Name") = Language.getMessage(mstrModuleName, 2, "Select")
            dtTable.Rows.InsertAt(drRow, 0)
            With cboLocation
                .ValueMember = "Id"
                .DisplayMember = "Name"
                .DataSource = dtTable
            End With
            objMaster = Nothing
            dtTable = Nothing
            dsList = Nothing

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "FillCombo", mstrModuleName)
        End Try
    End Sub

    Public Sub ResetValue()
        Try
            cboEmployee.SelectedValue = 0
            dtpFromDate.Value = ConfigParameter._Object._CurrentDateAndTime.Date
            dtpToDate.Value = ConfigParameter._Object._CurrentDateAndTime.Date
            mintViewIndex = -1
            mstrAdvanceFilter = ""
            mstrViewByIds = ""
            mstrViewByName = ""
            mstrAnalysis_Fields = ""
            mstrAnalysis_Join = ""
            mstrAnalysis_OrderBy = ""
            mstrAnalysis_OrderBy_GName = ""
            mstrReport_GroupName = ""

            Dim objUserDefRMode As New clsUserDef_ReportMode
            Dim dsList As DataSet = objUserDefRMode.GetList("List", enArutiReport.Employee_Timesheet_Compliance_Report, 0, 0)
            If dsList.Tables("List").Rows.Count > 0 Then
                Dim ar() As String = dsList.Tables("List").Rows(0).Item("transactionheadid").ToString().Split("|")
                If ar.Length > 0 Then
                    cboLocation.SelectedValue = ar(0).ToString()
                    chkIncludeWeekend.Checked = CBool(ar(1).ToString())
                    chkIncludeDayOff.Checked = CBool(ar(2).ToString())
                    chkIncludeDayOff.Checked = CBool(ar(3).ToString())
                End If
            End If
            dsList = Nothing
            objUserDefRMode = Nothing

            objTRAEmpTimesheetComplianceReport.setDefaultOrderBy(0)
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "ResetValue", mstrModuleName)
        End Try
    End Sub

    Public Function SetFilter() As Boolean
        Try
            If CInt(cboLocation.SelectedValue) <= 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 3, "Location is compulsory information.Please Select Location."), enMsgBoxStyle.Information)
                cboLocation.Focus()
                Return False
            End If

            objTRAEmpTimesheetComplianceReport.SetDefaultValue()

            objTRAEmpTimesheetComplianceReport._FromDate = dtpFromDate.Value.Date
            objTRAEmpTimesheetComplianceReport._ToDate = dtpToDate.Value.Date
            objTRAEmpTimesheetComplianceReport._EmpId = cboEmployee.SelectedValue
            objTRAEmpTimesheetComplianceReport._EmpName = cboEmployee.Text
            objTRAEmpTimesheetComplianceReport._LocationId = cboLocation.SelectedValue
            objTRAEmpTimesheetComplianceReport._Location = cboLocation.Text
            objTRAEmpTimesheetComplianceReport._IncludeWeekend = chkIncludeWeekend.Checked
            objTRAEmpTimesheetComplianceReport._IncludeHoliday = chkIncludeDayOff.Checked
            objTRAEmpTimesheetComplianceReport._IncludeDayOff = chkIncludeDayOff.Checked
            objTRAEmpTimesheetComplianceReport._ViewByIds = mstrViewByIds
            objTRAEmpTimesheetComplianceReport._ViewIndex = mintViewIndex
            objTRAEmpTimesheetComplianceReport._ViewByName = mstrViewByName
            objTRAEmpTimesheetComplianceReport._Analysis_Fields = mstrAnalysis_Fields
            objTRAEmpTimesheetComplianceReport._Analysis_Join = mstrAnalysis_Join
            objTRAEmpTimesheetComplianceReport._Analysis_OrderBy = mstrAnalysis_OrderBy
            objTRAEmpTimesheetComplianceReport._Analysis_OrderBy_GName = mstrAnalysis_OrderBy_GName
            objTRAEmpTimesheetComplianceReport._Report_GroupName = mstrReport_GroupName
            objTRAEmpTimesheetComplianceReport._AdvanceFilter = mstrAdvanceFilter
            objTRAEmpTimesheetComplianceReport._YearId = FinancialYear._Object._YearUnkid
            objTRAEmpTimesheetComplianceReport._UserUnkId = User._Object._Userunkid
            objTRAEmpTimesheetComplianceReport._UserName = User._Object._Username
            objTRAEmpTimesheetComplianceReport._CompanyUnkId = Company._Object._Companyunkid

            Return True
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "SetFilter", mstrModuleName)
        End Try
    End Function

#End Region

#Region "Form's Events"

    Private Sub frmTRAEmpTimesheetComplianceReport_FormClosed(ByVal sender As Object, ByVal e As System.Windows.Forms.FormClosedEventArgs) Handles Me.FormClosed
        Try
            objTRAEmpTimesheetComplianceReport = Nothing
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmTRAEmpTimesheetComplianceReport_FormClosed", mstrModuleName)
        End Try
    End Sub

    Private Sub frmTRAEmpTimesheetComplianceReport_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            Call Language.setLanguage(Me.Name)
            OtherSettings()
            eZeeHeader.Title = objTRAEmpTimesheetComplianceReport._ReportName
            eZeeHeader.Message = objTRAEmpTimesheetComplianceReport._ReportDesc
            Call FillCombo()
            Call ResetValue()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmTRAEmpTimesheetComplianceReport_Load", mstrModuleName)
        End Try
    End Sub

    Private Sub frmTRAEmpTimesheetComplianceReport_KeyDown(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles Me.KeyDown
        Try
            If e.Control Then
                If e.KeyCode = Windows.Forms.Keys.E Then
                    Call btnExport_Click(btnExport, e)
                End If
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmTRAEmpTimesheetComplianceReport_KeyDown", mstrModuleName)
        End Try
    End Sub

    Private Sub frmTRAEmpTimesheetComplianceReport_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles Me.KeyPress
        Try
            Select Case e.KeyChar
                Case CChar(ChrW(CInt(Windows.Forms.Keys.Enter)))
                    Windows.Forms.SendKeys.Send("{TAB}")
                    e.Handled = True
                    Exit Select
            End Select
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmTRAEmpTimesheetComplianceReport_KeyPress", mstrModuleName)
        End Try

    End Sub

#End Region

#Region "Buttons"

    Private Sub btnExport_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnExport.Click
        Try
            If SetFilter() = False Then Exit Sub

            objTRAEmpTimesheetComplianceReport.Generate_DetailReport(FinancialYear._Object._DatabaseName, User._Object._Userunkid, FinancialYear._Object._YearUnkid _
                                                                                                        , Company._Object._Companyunkid, dtpFromDate.Value.Date, dtpToDate.Value.Date _
                                                                                                        , ConfigParameter._Object._UserAccessModeSetting, True, ConfigParameter._Object._ExportReportPath _
                                                                                                        , ConfigParameter._Object._OpenAfterExport)
         
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "btnExport_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub btnReset_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnReset.Click
        Try
            Call ResetValue()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "btnReset_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub btnClose_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnClose.Click
        Try
            Me.Close()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "btnClose_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub objbtnAdvanceFilter_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles objbtnAdvanceFilter.Click
        Dim frm As New frmAdvanceSearch
        Try
            frm._Hr_EmployeeTable_Alias = "hremployee_master"
            frm.ShowDialog()
            mstrAdvanceFilter = frm._GetFilterString
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objbtnAdvanceFilter_Click", mstrModuleName)
        Finally
            If frm IsNot Nothing Then frm.Dispose()
        End Try
    End Sub

    Private Sub objbtnSearchEmployee_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnSearchEmployee.Click
        Dim frm As New frmCommonSearch
        Try
            frm.DataSource = cboEmployee.DataSource
            frm.ValueMember = cboEmployee.ValueMember
            frm.DisplayMember = cboEmployee.DisplayMember
            frm.CodeMember = "employeecode"
            If frm.DisplayDialog Then
                cboEmployee.SelectedValue = frm.SelectedValue
                cboEmployee.Focus()
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objbtnSearchEmployee_Click", mstrModuleName)
        Finally
            If frm IsNot Nothing Then frm.Dispose()
        End Try
    End Sub

    Private Sub objbtnLanguage_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles objbtnLanguage.Click
        Dim objfrm As New frmLanguage
        Try
            If User._Object._Isrighttoleft = True Then
                objfrm.RightToLeft = Windows.Forms.RightToLeft.Yes
                objfrm.RightToLeftLayout = True
                Call Language.ctlRightToLeftlayOut(objfrm)
            End If

            Call SetMessages()

            clsTRAEmpTimesheetComplianceReport.SetMessages()
            objfrm._Other_ModuleNames = "clsTRAEmpTimesheetComplianceReport"
            objfrm.displayDialog(Me)

            Call Language.setLanguage(Me.Name)
            Call SetLanguage()

        Catch ex As System.Exception
            Call DisplayError.Show(-1, ex.Message, "frmTRAEmpTimesheetComplianceReport_Language_Click", mstrModuleName)
        Finally
            objfrm.Dispose()
            objfrm = Nothing
        End Try

    End Sub

    Private Sub objbtnSearchLocation_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnSearchLocation.Click
        Dim frm As New frmCommonSearch
        Try
            frm.DataSource = cboLocation.DataSource
            frm.ValueMember = cboLocation.ValueMember
            frm.DisplayMember = cboLocation.DisplayMember
            If frm.DisplayDialog Then
                cboLocation.SelectedValue = frm.SelectedValue
                cboLocation.Focus()
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objbtnSearchLocation_Click", mstrModuleName)
        Finally
            If frm IsNot Nothing Then frm.Dispose()
        End Try
    End Sub

#End Region


#Region "LinkLabel Event"

    Private Sub lnkSetAnalysis_LinkClicked(ByVal sender As System.Object, ByVal e As System.Windows.Forms.LinkLabelLinkClickedEventArgs) Handles lnkSetAnalysis.LinkClicked
        Dim frm As New frmViewAnalysis
        Try
            frm.displayDialog()
            mstrViewByIds = frm._ReportBy_Ids
            mstrViewByName = frm._ReportBy_Name
            mintViewIndex = frm._ViewIndex
            mstrAnalysis_Fields = frm._Analysis_Fields
            mstrAnalysis_Join = frm._Analysis_Join
            mstrAnalysis_OrderBy = frm._Analysis_OrderBy
            mstrAnalysis_OrderBy_GName = frm._Analysis_OrderBy_GName
            mstrReport_GroupName = frm._Report_GroupName
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "lnkSetAnalysis_LinkClicked", mstrModuleName)
        Finally
            frm = Nothing
        End Try
    End Sub

    Private Sub lnkSave_LinkClicked(ByVal sender As System.Object, ByVal e As System.Windows.Forms.LinkLabelLinkClickedEventArgs) Handles lnkSave.LinkClicked
        Try
            Dim objUserDefRMode As New clsUserDef_ReportMode
            Try
                objUserDefRMode = New clsUserDef_ReportMode()
                objUserDefRMode._Reportunkid = enArutiReport.Employee_Timesheet_Compliance_Report
                objUserDefRMode._Reporttypeid = 0
                objUserDefRMode._Reportmodeid = 0
                objUserDefRMode._Headtypeid = 0

                Dim intUnkid As Integer = -1

                objUserDefRMode._EarningTranHeadIds = cboLocation.SelectedValue & "|" & chkIncludeWeekend.Checked & "|" & chkIncludeHoliday.Checked & "|" & chkIncludeDayOff.Checked
                intUnkid = objUserDefRMode.isExist(enArutiReport.Employee_Timesheet_Compliance_Report, 0, 0, 0)

                objUserDefRMode._Reportmodeunkid = intUnkid
                If intUnkid <= 0 Then
                    objUserDefRMode.Insert()
                Else
                    objUserDefRMode.Update()
                End If
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 1, "Setting saved successfully."), enMsgBoxStyle.Information)
            Catch ex As Exception
                DisplayError.Show("-1", ex.Message, "lnkSave_LinkClicked", mstrModuleName)
            Finally
                objUserDefRMode = Nothing
            End Try
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "lnkSave_LinkClicked", mstrModuleName)
        End Try
    End Sub


#End Region

	'<Language> This Auto Generated Text Please Do Not Modify it.
#Region " Language & UI Settings "
	Private Sub OtherSettings()
		Try
			Me.SuspendLayout()
            Call SetLanguage()
			
			Me.gbFilterCriteria.GradientColor = GUI._eZeeContainerHeaderBackColor 
			Me.gbFilterCriteria.ForeColor = GUI._eZeeContainerHeaderForeColor 

            Me.eZeeHeader.GradientColor1 = GUI._HeaderBackColor1
			Me.eZeeHeader.GradientColor2 = GUI._HeaderBackColor2 
			Me.eZeeHeader.BorderColor = GUI._HeaderBorderColor 
			Me.eZeeHeader.HeaderTextForeColor = GUI._HeaderTitleFontColor 
			Me.eZeeHeader.DescriptionForeColor = GUI._HeaderMessageFontColor 


			Me.btnReset.GradientBackColor = GUI._ButttonBackColor 
			Me.btnReset.GradientForeColor = GUI._ButttonFontColor

			Me.btnExport.GradientBackColor = GUI._ButttonBackColor 
			Me.btnExport.GradientForeColor = GUI._ButttonFontColor

			Me.btnClose.GradientBackColor = GUI._ButttonBackColor 
			Me.btnClose.GradientForeColor = GUI._ButttonFontColor

			Me.objbtnLanguage.GradientBackColor = GUI._ButttonBackColor 
			Me.objbtnLanguage.GradientForeColor = GUI._ButttonFontColor

			Me.objbtnAdvanceFilter.GradientBackColor = GUI._ButttonBackColor 
			Me.objbtnAdvanceFilter.GradientForeColor = GUI._ButttonFontColor


			Me.ResumeLayout()
		Catch Ex As Exception
			DisplayError.Show("-1", ex.Message, "OtherSettings", mstrModuleName)
		End Try
	End Sub
			
			
	Private Sub SetLanguage()
		Try
			Me.Text = Language._Object.getCaption(Me.Name, Me.Text)
			
			Me.eZeeHeader.Title = Language._Object.getCaption(Me.eZeeHeader.Name & "_Title" , Me.eZeeHeader.Title)
			Me.eZeeHeader.Message = Language._Object.getCaption(Me.eZeeHeader.Name & "_Message" , Me.eZeeHeader.Message)
			Me.btnReset.Text = Language._Object.getCaption(Me.btnReset.Name, Me.btnReset.Text)
			Me.btnExport.Text = Language._Object.getCaption(Me.btnExport.Name, Me.btnExport.Text)
			Me.btnClose.Text = Language._Object.getCaption(Me.btnClose.Name, Me.btnClose.Text)
			Me.gbFilterCriteria.Text = Language._Object.getCaption(Me.gbFilterCriteria.Name, Me.gbFilterCriteria.Text)
			Me.lblEmployee.Text = Language._Object.getCaption(Me.lblEmployee.Name, Me.lblEmployee.Text)
			Me.lblFromDate.Text = Language._Object.getCaption(Me.lblFromDate.Name, Me.lblFromDate.Text)
			Me.lblToDate.Text = Language._Object.getCaption(Me.lblToDate.Name, Me.lblToDate.Text)
			Me.lnkSetAnalysis.Text = Language._Object.getCaption(Me.lnkSetAnalysis.Name, Me.lnkSetAnalysis.Text)
			Me.chkIncludeWeekend.Text = Language._Object.getCaption(Me.chkIncludeWeekend.Name, Me.chkIncludeWeekend.Text)
			Me.chkIncludeDayOff.Text = Language._Object.getCaption(Me.chkIncludeDayOff.Name, Me.chkIncludeDayOff.Text)
			Me.LblLocation.Text = Language._Object.getCaption(Me.LblLocation.Name, Me.LblLocation.Text)
			Me.lnkSave.Text = Language._Object.getCaption(Me.lnkSave.Name, Me.lnkSave.Text)
            Me.chkIncludeHoliday.Text = Language._Object.getCaption(Me.chkIncludeHoliday.Name, Me.chkIncludeHoliday.Text)

		Catch Ex As Exception
			DisplayError.Show("-1", ex.Message, "SetLanguage", mstrModuleName)
		End Try
	End Sub
			
			
	Private Sub SetMessages()
		Try
			Language.setMessage(mstrModuleName, 1, "Setting saved successfully.")
			Language.setMessage(mstrModuleName, 2, "Select")
			Language.setMessage(mstrModuleName, 3, "Location is compulsory information.Please Select Location.")

		Catch Ex As Exception
			DisplayError.Show("-1", ex.Message, "SetMessages", mstrModuleName)
		End Try
	End Sub
#End Region 'Language & UI Settings
	'</Language>
End Class
