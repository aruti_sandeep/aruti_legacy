#Region " Imports "

Imports eZeeCommonLib
Imports Aruti.Data

#End Region

Public Class frmMedicalBillsummary

#Region " Private Variables "
    Private ReadOnly mstrModuleName As String = "frmMedicalBillsummary"
    Private objBillsummary As clsMedicalBillsummary
    Private mstrStringIds As String = String.Empty
    Private mstrStringName As String = String.Empty
    Private mintViewIdx As Integer = 0
    Private mstrAnalysis_Fields As String = ""
    Private mstrAnalysis_Join As String = ""
    Private mstrAnalysis_OrderBy As String = ""
    Private mstrReport_GroupName As String = ""
    Private dsPeriod As DataSet = Nothing
#End Region

#Region " Constructor "

    Public Sub New()
        objBillsummary = New clsMedicalBillsummary(User._Object._Languageunkid,Company._Object._Companyunkid)
        objBillsummary.SetDefaultValue()
        InitializeComponent()
    End Sub

#End Region

#Region " Private Function "

    Private Sub FillCombo()
        Try
            Dim objEmp As New clsEmployee_Master
            Dim objperiod As New clscommom_period_Tran
            Dim dsList As New DataSet
            Dim objUserdefReport As New clsUserDef_ReportMode

            'S.SANDEEP [04 JUN 2015] -- START
            'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
            'dsList = objEmp.GetEmployeeList("Emp", True, True, , , , , , , , , , , , eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate).Date, eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate).Date)
            dsList = objEmp.GetEmployeeList(FinancialYear._Object._DatabaseName, _
                                            User._Object._Userunkid, _
                                            FinancialYear._Object._YearUnkid, _
                                            Company._Object._Companyunkid, _
                                            eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), _
                                            eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), _
                                            ConfigParameter._Object._UserAccessModeSetting, _
                                            True, ConfigParameter._Object._IsIncludeInactiveEmp, "Emp", True)
            'S.SANDEEP [04 JUN 2015] -- END

            With cboEmployee
                .ValueMember = "employeeunkid"
                .DisplayMember = "employeename"
                .DataSource = dsList.Tables("Emp")
                .SelectedValue = 0
            End With
            objEmp = Nothing


            'Sohail (21 Aug 2015) -- Start
            'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
            'dsPeriod = objperiod.getListForCombo(enModuleReference.Payroll, Company._Object._YearUnkid, "Period", True)

            'Shani(11-Feb-2016) -- Start
            'Leave,TNA And Medical Reprt Convert to Active Employee Query Wise
            'dsPeriod = objperiod.getListForCombo(enModuleReference.Payroll, Company._Object._YearUnkid, FinancialYear._Object._DatabaseName, FinancialYear._Object._Database_Start_Date, "Period", True)
            dsPeriod = objperiod.getListForCombo(enModuleReference.Payroll, FinancialYear._Object._YearUnkid, FinancialYear._Object._DatabaseName, FinancialYear._Object._Database_Start_Date, "Period", True)
            'Shani(11-Feb-2016) -- End

            'Sohail (21 Aug 2015) -- End
            With cboFromPeriod
                .ValueMember = "periodunkid"
                .DisplayMember = "name"
                .DataSource = dsPeriod.Tables("Period")
                .SelectedValue = 0
            End With

            With cboToPeriod
                .ValueMember = "periodunkid"
                .DisplayMember = "name"
                .DataSource = dsPeriod.Tables("Period").Copy
                .SelectedValue = 0
            End With

            Dim objInstitute As New clsinstitute_master
            dsList = objInstitute.getListForCombo(True, "Institute", True, 1)
            With cboServiceProvider
                .ValueMember = "instituteunkid"
                .DisplayMember = "name"
                .DataSource = dsList.Tables("Institute")
                .SelectedValue = 0
            End With

            With cboReportType
                .Items.Clear()
                .Items.Add(Language.getMessage(mstrModuleName, 1, "Medical Bill Summary Employee Wise"))
                .Items.Add(Language.getMessage(mstrModuleName, 2, "Medical Bill Summary Service Provider Wise"))
                .SelectedIndex = 0
            End With

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "FillCombo", mstrModuleName)
        End Try
    End Sub

    Public Function SetFilter() As Boolean
        Try

            objBillsummary.SetDefaultValue()

            objBillsummary._ReportTypeId = CInt(cboReportType.SelectedIndex)
            objBillsummary._ReportTypeName = cboReportType.Text

            objBillsummary._FromPeriodId = CInt(cboFromPeriod.SelectedValue)
            objBillsummary._FromPeriodName = cboFromPeriod.Text

            objBillsummary._ToPeriodId = CInt(cboToPeriod.SelectedValue)
            objBillsummary._ToPeriodName = cboToPeriod.Text


            If dsPeriod IsNot Nothing And dsPeriod.Tables.Count > 0 Then

                Dim drRow() As DataRow = dsPeriod.Tables(0).Select("periodunkid >= " & CInt(cboFromPeriod.SelectedValue) & " AND periodunkid<= " & CInt(cboToPeriod.SelectedValue))

                If drRow.Length > 0 Then
                    Dim mstrPeriod As String = ""
                    For i As Integer = 0 To drRow.Length - 1
                        mstrPeriod &= drRow(i)("periodunkid").ToString() & ","
                    Next

                    If mstrPeriod.Trim.Length > 0 Then
                        mstrPeriod = mstrPeriod.Substring(0, mstrPeriod.Trim.Length - 1)
                    End If

                    objBillsummary._mstrPeriodID = mstrPeriod

                End If

            End If

            If CInt(cboReportType.SelectedIndex) = 0 Then
                objBillsummary._EmployeeId = CInt(cboEmployee.SelectedValue)
                objBillsummary._EmployeeName = cboEmployee.Text

            ElseIf CInt(cboReportType.SelectedIndex = 1) Then
                objBillsummary._ProviderId = CInt(cboServiceProvider.SelectedValue)
                objBillsummary._ProviderName = cboServiceProvider.Text

            End If

            objBillsummary._ViewByIds = mstrStringIds
            objBillsummary._ViewIndex = mintViewIdx
            objBillsummary._ViewByName = mstrStringName
            objBillsummary._Analysis_Fields = mstrAnalysis_Fields
            objBillsummary._Analysis_Join = mstrAnalysis_Join
            objBillsummary._Analysis_OrderBy = mstrAnalysis_OrderBy
            objBillsummary._Report_GroupName = mstrReport_GroupName


            'Anjan [08 September 2015] -- Start
            'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
            objBillsummary._IncludeInactiveEmp = chkInactiveemp.Checked
            'Anjan [08 September 2015] -- End


            Return True
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "SetFilter", mstrModuleName)
            Return False
        End Try
    End Function

    Private Function Validation() As Boolean
        Try
            If CInt(cboFromPeriod.SelectedValue) <= 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 3, "From Period is compulsory information.Please Select From Period."), enMsgBoxStyle.Information)
                cboFromPeriod.Select()
                Return False

            ElseIf CInt(cboToPeriod.SelectedValue) <= 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 4, "To Period is compulsory information.Please Select To Period."), enMsgBoxStyle.Information)
                cboToPeriod.Select()
                Return False
            End If

            If cboToPeriod.SelectedIndex < cboFromPeriod.SelectedIndex Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 5, " To Period cannot be less than From Period."), enMsgBoxStyle.Information)
                cboToPeriod.Focus()
                Exit Function
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "Validation", mstrModuleName)
            Return False
        End Try
        Return True
    End Function

    Private Sub ResetValue()
        Try
            objBillsummary.setDefaultOrderBy(0)
            txtOrderBy.Text = objBillsummary.OrderByDisplay
            cboEmployee.SelectedIndex = 0
            cboFromPeriod.SelectedIndex = 0
            cboReportType.SelectedIndex = 0
            cboToPeriod.SelectedIndex = 0
            cboServiceProvider.SelectedIndex = 0
            mstrStringIds = ""
            mstrStringName = ""
            mintViewIdx = -1
            mstrAnalysis_Fields = ""
            mstrAnalysis_Join = ""
            mstrAnalysis_OrderBy = ""
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "ResetValue", mstrModuleName)
        End Try
    End Sub

#End Region

#Region " Form's Events "

    Private Sub frmMedicalBillsummary_FormClosed(ByVal sender As Object, ByVal e As System.Windows.Forms.FormClosedEventArgs) Handles Me.FormClosed
        Try
            objBillsummary = Nothing
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmMedicalBillsummary_FormClosed", mstrModuleName)
        End Try
    End Sub

    Private Sub frmMedicalBillsummary_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            Call Language.setLanguage(Me.Name)
            Me._Title = objBillsummary._ReportName
            Me._Message = objBillsummary._ReportDesc
            Call FillCombo()
            Call ResetValue()

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmMedicalBillsummary_Load", mstrModuleName)
        End Try
    End Sub

    Private Sub frmMedicalBillsummary_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles Me.KeyPress
        Try
            Select Case e.KeyChar
                Case CChar(ChrW(CInt(Windows.Forms.Keys.Enter)))
                    Windows.Forms.SendKeys.Send("{TAB}")
                    e.Handled = True
                    Exit Select
            End Select
        Catch ex As Exception
            DisplayError.Show(-1, ex.Message, "frmMedicalBillsummary_KeyPress", mstrModuleName)
        End Try
    End Sub

    Private Sub frmMedicalBillsummary_Language_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Language_Click
        Dim objfrm As New frmLanguage
        Try
            If User._Object._Isrighttoleft = True Then
                objfrm.RightToLeft = Windows.Forms.RightToLeft.Yes
                objfrm.RightToLeftLayout = True
                Call Language.ctlRightToLeftlayOut(objfrm)
            End If

            Call SetMessages()

            clsMedicalBillsummary.SetMessages()
            objfrm._Other_ModuleNames = "clsMedicalBillsummary"
            objfrm.displayDialog(Me)

            Call Language.setLanguage(Me.Name)
            Call SetLanguage()

        Catch ex As System.Exception
            Call DisplayError.Show(-1, ex.Message, "frmMedicalBillsummary_Language_Click", mstrModuleName)
        Finally
            objfrm.Dispose()
            objfrm = Nothing
        End Try

    End Sub
#End Region

#Region "Buttons"

    Private Sub objbtnSearchEmployee_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnSearchEmployee.Click
        Dim objfrm As New frmCommonSearch
        Dim dtList As DataTable
        Try
            dtList = CType(cboEmployee.DataSource, DataTable)
            With cboEmployee
                objfrm.DataSource = dtList
                objfrm.ValueMember = .ValueMember
                objfrm.DisplayMember = .DisplayMember
                objfrm.CodeMember = "employeecode"
                If objfrm.DisplayDialog Then
                    .SelectedValue = objfrm.SelectedValue
                End If
                .Focus()
            End With
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objbtnSearchEmployee_Click", mstrModuleName)
        Finally
            objfrm = Nothing
        End Try
    End Sub

    Private Sub objbtnSearchFromPeriod_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnSearchFromPeriod.Click
        Dim objfrm As New frmCommonSearch
        Dim dtList As DataTable
        Try
            dtList = CType(cboFromPeriod.DataSource, DataTable)
            With cboFromPeriod
                objfrm.DataSource = dtList
                objfrm.ValueMember = .ValueMember
                objfrm.DisplayMember = .DisplayMember
                If objfrm.DisplayDialog Then
                    .SelectedValue = objfrm.SelectedValue
                End If
                .Focus()
            End With
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objbtnSearchFromPeriod_Click", mstrModuleName)
        Finally
            objfrm = Nothing
        End Try
    End Sub

    Private Sub objbtnSearchToPeriod_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnSearchToPeriod.Click
        Dim objfrm As New frmCommonSearch
        Dim dtList As DataTable
        Try
            dtList = CType(cboToPeriod.DataSource, DataTable)
            With cboToPeriod
                objfrm.DataSource = dtList
                objfrm.ValueMember = .ValueMember
                objfrm.DisplayMember = .DisplayMember
                If objfrm.DisplayDialog Then
                    .SelectedValue = objfrm.SelectedValue
                End If
                .Focus()
            End With
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objbtnSearchToPeriod_Click", mstrModuleName)
        Finally
            objfrm = Nothing
        End Try
    End Sub

    Private Sub objbtnSearchProvider_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnSearchProvider.Click
        Dim objfrm As New frmCommonSearch
        Dim dtList As DataTable
        Try
            dtList = CType(cboServiceProvider.DataSource, DataTable)
            With cboServiceProvider
                objfrm.DataSource = dtList
                objfrm.ValueMember = .ValueMember
                objfrm.DisplayMember = .DisplayMember
                If objfrm.DisplayDialog Then
                    .SelectedValue = objfrm.SelectedValue
                End If
                .Focus()
            End With
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objbtnSearchProvider_Click", mstrModuleName)
        Finally
            objfrm = Nothing
        End Try
    End Sub

    Private Sub frmMedicalBillsummary_Report_Click(ByVal sender As Object, ByVal e As Aruti.Data.PrintButtonEventArgs) Handles Me.Report_Click
        Try
            If Validation() Then
                If SetFilter() = False Then Exit Sub
                'Anjan [08 September 2015] -- Start
                'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
                'objBillsummary.generateReport(CInt(cboReportType.SelectedIndex), e.Type, enExportAction.None)
                objBillsummary.generateReportNew(FinancialYear._Object._DatabaseName, _
                                               User._Object._Userunkid, _
                                               FinancialYear._Object._YearUnkid, _
                                               Company._Object._Companyunkid, _
                                               eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), _
                                               eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), _
                                               ConfigParameter._Object._UserAccessModeSetting, _
                                               True, ConfigParameter._Object._ExportReportPath, ConfigParameter._Object._OpenAfterExport, cboReportType.SelectedIndex, e.Type, enExportAction.None, ConfigParameter._Object._Base_CurrencyId)
                'Anjan [08 September 2015] -- End)
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmMedicalBillsummary_Report_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub frmMedicalBillsummary_Export_Click(ByVal sender As Object, ByVal e As Aruti.Data.PrintButtonEventArgs) Handles Me.Export_Click
        Try
            If Validation() Then
                If SetFilter() = False Then Exit Sub

                'Anjan [08 September 2015] -- Start
                'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
                'objBillsummary.generateReport(CInt(cboReportType.SelectedIndex), enPrintAction.None, e.Type)
                objBillsummary.generateReportNew(FinancialYear._Object._DatabaseName, _
                                               User._Object._Userunkid, _
                                               FinancialYear._Object._YearUnkid, _
                                               Company._Object._Companyunkid, _
                                               eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), _
                                               eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), _
                                               ConfigParameter._Object._UserAccessModeSetting, _
                                               True, ConfigParameter._Object._ExportReportPath, ConfigParameter._Object._OpenAfterExport, cboReportType.SelectedIndex, enPrintAction.None, e.Type, ConfigParameter._Object._Base_CurrencyId)
                'Anjan [08 September 2015] -- End


            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmMedicalBillsummary_Export_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub frmMedicalBillsummary_Reset_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Reset_Click
        Try
            Call ResetValue()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmMedicalBillsummary_Reset_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub frmMedicalBillsummary_Cancel_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Cancel_Click
        Try
            Me.Close()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmMedicalBillsummary_Cancel_Click", mstrModuleName)
        End Try

    End Sub

#End Region

#Region "LinkLabel Event"

    Private Sub lnkSetAnalysis_LinkClicked(ByVal sender As System.Object, ByVal e As System.Windows.Forms.LinkLabelLinkClickedEventArgs) Handles lnkSetAnalysis.LinkClicked
        Dim frm As New frmViewAnalysis
        Try
            frm.displayDialog()
            mstrStringIds = frm._ReportBy_Ids
            mstrStringName = frm._ReportBy_Name
            mintViewIdx = frm._ViewIndex
            mstrAnalysis_Fields = frm._Analysis_Fields
            mstrAnalysis_Join = frm._Analysis_Join
            mstrAnalysis_OrderBy = frm._Analysis_OrderBy
            mstrReport_GroupName = frm._Report_GroupName
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "lnkSetAnalysis_LinkClicked", mstrModuleName)
        Finally
            frm = Nothing
        End Try
    End Sub

#End Region

#Region "Control"

    Private Sub objbtnSort_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles objbtnSort.Click
        Try
            objBillsummary.setOrderBy(CInt(cboReportType.SelectedIndex))
            txtOrderBy.Text = objBillsummary.OrderByDisplay
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objbtnSort_Click", mstrModuleName)
        End Try
    End Sub


#End Region

#Region "ComboBox Event"

    Private Sub cboReportType_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cboReportType.SelectedIndexChanged
        Try
            If CInt(cboReportType.SelectedIndex) <= 0 Then
                cboServiceProvider.SelectedIndex = 0
                cboEmployee.Enabled = True
                objbtnSearchEmployee.Enabled = True
                lnkSetAnalysis.Enabled = True
                cboServiceProvider.Enabled = False
                objbtnSearchProvider.Enabled = False
            Else
                cboEmployee.SelectedIndex = 0
                cboServiceProvider.Enabled = True
                objbtnSearchProvider.Enabled = True
                lnkSetAnalysis.Enabled = False
                cboEmployee.Enabled = False
                objbtnSearchEmployee.Enabled = False
                mstrStringIds = ""
                mstrStringName = ""
                mintViewIdx = -1
                mstrAnalysis_Fields = ""
                mstrAnalysis_Join = ""
                mstrAnalysis_OrderBy = ""
            End If
            objBillsummary.setDefaultOrderBy(CInt(cboReportType.SelectedIndex))
            txtOrderBy.Text = objBillsummary.OrderByDisplay
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "cboReportType_SelectedIndexChanged", mstrModuleName)
        End Try
    End Sub

#End Region



	'<Language> This Auto Generated Text Please Do Not Modify it.
#Region " Language & UI Settings "
	Private Sub OtherSettings()
		Try
			Me.SuspendLayout()
            Call SetLanguage()
			
			Me.gbSortBy.GradientColor = GUI._eZeeContainerHeaderBackColor 
			Me.gbSortBy.ForeColor = GUI._eZeeContainerHeaderForeColor 

			Me.gbFilterCriteria.GradientColor = GUI._eZeeContainerHeaderBackColor 
			Me.gbFilterCriteria.ForeColor = GUI._eZeeContainerHeaderForeColor 


			Me.ResumeLayout()
		Catch Ex As Exception
			DisplayError.Show("-1", ex.Message, "OtherSettings", mstrModuleName)
		End Try
	End Sub
			
			
	Private Sub SetLanguage()
		Try
			Me.Text = Language._Object.getCaption(Me.Name, Me.Text)
			
			Me.gbSortBy.Text = Language._Object.getCaption(Me.gbSortBy.Name, Me.gbSortBy.Text)
			Me.lblOrderBy.Text = Language._Object.getCaption(Me.lblOrderBy.Name, Me.lblOrderBy.Text)
			Me.gbFilterCriteria.Text = Language._Object.getCaption(Me.gbFilterCriteria.Name, Me.gbFilterCriteria.Text)
			Me.lblFromPeriod.Text = Language._Object.getCaption(Me.lblFromPeriod.Name, Me.lblFromPeriod.Text)
			Me.lnkSetAnalysis.Text = Language._Object.getCaption(Me.lnkSetAnalysis.Name, Me.lnkSetAnalysis.Text)
			Me.lblEmployee.Text = Language._Object.getCaption(Me.lblEmployee.Name, Me.lblEmployee.Text)
			Me.LblProvider.Text = Language._Object.getCaption(Me.LblProvider.Name, Me.LblProvider.Text)
			Me.LblToPeriod.Text = Language._Object.getCaption(Me.LblToPeriod.Name, Me.LblToPeriod.Text)
			Me.lblReportType.Text = Language._Object.getCaption(Me.lblReportType.Name, Me.lblReportType.Text)

		Catch Ex As Exception
			DisplayError.Show("-1", ex.Message, "SetLanguage", mstrModuleName)
		End Try
	End Sub
			
			
	Private Sub SetMessages()
		Try
			Language.setMessage(mstrModuleName, 1, "Medical Bill Summary Employee Wise")
			Language.setMessage(mstrModuleName, 2, "Medical Bill Summary Service Provider Wise")
			Language.setMessage(mstrModuleName, 3, "From Period is compulsory information.Please Select From Period.")
			Language.setMessage(mstrModuleName, 4, "To Period is compulsory information.Please Select To Period.")
			Language.setMessage(mstrModuleName, 5, " To Period cannot be less than From Period.")

		Catch Ex As Exception
			DisplayError.Show("-1", ex.Message, "SetMessages", mstrModuleName)
		End Try
	End Sub
#End Region 'Language & UI Settings
	'</Language>
End Class
