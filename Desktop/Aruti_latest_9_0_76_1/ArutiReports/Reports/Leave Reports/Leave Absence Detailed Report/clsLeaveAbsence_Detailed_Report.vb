'************************************************************************************************************************************
'Class Name : clsLeaveAbsence_Detailed_Report.vb
'Purpose    :
'Date       : 16-May-2020
'Written By :Pinkal
'Modified   :
'************************************************************************************************************************************

Imports eZeeCommonLib
Imports Aruti.Data
Imports ExcelWriter

''' <summary>
''' Purpose: Report Generation Class 
''' Developer: Pinkal
''' </summary>
''' 

Public Class clsLeaveAbsence_Detailed_Report
    Inherits IReportData
    Private Shared ReadOnly mstrModuleName As String = "clsLeaveAbsence_Detailed_Report"
    Private mstrReportId As String = enArutiReport.Leave_Absence_DetailedReport  '225
    Dim objDataOperation As clsDataOperation

#Region " Constructor "
    Public Sub New(Byval intLangId As Integer,Byval intCompanyId AS Integer) 'S.SANDEEP |24-Sep-2020| -- START --END
        Me.setReportData(CInt(mstrReportId),intLangId,intCompanyId) 'S.SANDEEP |24-Sep-2020| -- START --END
        Call Create_OnDetailReport()
    End Sub
#End Region

#Region " Private variables "

    Private mdtFromDate As DateTime = Nothing
    Private mdtToDate As DateTime = Nothing
    Private mintEmployeeId As Integer = 0
    Private mstrEmployeeName As String = ""
    Private mstrLeaveIds As String = ""
    Private mstrLeaveName As String = ""
    Private mintStatusId As Integer = 0
    Private mstrStatus As String = ""
    Private mstrOrderByQuery As String = ""
    Private mstrAdvance_Filter As String = String.Empty
    Private mintViewIndex As Integer = -1
    Private mstrViewByIds As String = ""
    Private mstrViewByName As String = ""
    Private mstrAnalysis_Fields As String = ""
    Private mstrAnalysis_Join As String = ""
    Private mstrAnalysis_OrderBy As String = ""
    Private mstrAnalysis_OrderBy_GName As String = ""
    Private mstrReport_GroupName As String = ""
    Private mblnFirstNamethenSurname As Boolean = ConfigParameter._Object._FirstNamethenSurname
    Private mstrExportReportPath As String = ConfigParameter._Object._ExportReportPath
    Private mblnOpenAfterExport As Boolean = ConfigParameter._Object._OpenAfterExport
    Private mintUserUnkid As Integer = -1
    Private mintCompanyUnkid As Integer = -1
    Private mstrUserAccessFilter As String = String.Empty
    Private mblnIncludeAccessFilterQry As Boolean = True

#End Region

#Region " Properties "

    Public WriteOnly Property _FromDate() As Date
        Set(ByVal value As Date)
            mdtFromDate = value
        End Set
    End Property

    Public WriteOnly Property _ToDate() As Date
        Set(ByVal value As Date)
            mdtToDate = value
        End Set
    End Property

    Public WriteOnly Property _EmployeeID() As Integer
        Set(ByVal value As Integer)
            mintEmployeeId = value
        End Set
    End Property

    Public WriteOnly Property _EmployeeName() As String
        Set(ByVal value As String)
            mstrEmployeeName = value
        End Set
    End Property

    Public WriteOnly Property _LeaveIds() As String
        Set(ByVal value As String)
            mstrLeaveIds = value
        End Set
    End Property

    Public WriteOnly Property _LeaveName() As String
        Set(ByVal value As String)
            mstrLeaveName = value
        End Set
    End Property

    Public WriteOnly Property _StatusId() As Integer
        Set(ByVal value As Integer)
            mintStatusId = value
        End Set
    End Property

    Public WriteOnly Property _StatusName() As String
        Set(ByVal value As String)
            mstrStatus = value
        End Set
    End Property

    Public WriteOnly Property _Advance_Filter() As String
        Set(ByVal value As String)
            mstrAdvance_Filter = value
        End Set
    End Property

    Public WriteOnly Property _ViewIndex() As Integer
        Set(ByVal value As Integer)
            mintViewIndex = value
        End Set
    End Property

    Public WriteOnly Property _ViewByIds() As String
        Set(ByVal value As String)
            mstrViewByIds = value
        End Set
    End Property

    Public WriteOnly Property _ViewByName() As String
        Set(ByVal value As String)
            mstrViewByName = value
        End Set
    End Property

    Public WriteOnly Property _Analysis_Fields() As String
        Set(ByVal value As String)
            mstrAnalysis_Fields = value
        End Set
    End Property

    Public WriteOnly Property _Analysis_Join() As String
        Set(ByVal value As String)
            mstrAnalysis_Join = value
        End Set
    End Property

    Public WriteOnly Property _Analysis_OrderBy() As String
        Set(ByVal value As String)
            mstrAnalysis_OrderBy = value
        End Set
    End Property

    Public WriteOnly Property _Analysis_OrderBy_GName() As String
        Set(ByVal value As String)
            mstrAnalysis_OrderBy_GName = value
        End Set
    End Property

    Public WriteOnly Property _Report_GroupName() As String
        Set(ByVal value As String)
            mstrReport_GroupName = value
        End Set
    End Property

    Public WriteOnly Property _ExportReportPath() As String
        Set(ByVal value As String)
            mstrExportReportPath = value
        End Set
    End Property

    Public WriteOnly Property _OpenAfterExport() As Boolean
        Set(ByVal value As Boolean)
            mblnOpenAfterExport = value
        End Set
    End Property

    Public WriteOnly Property _CompanyUnkId() As Integer
        Set(ByVal value As Integer)
            mintCompanyUnkid = value
        End Set
    End Property

    Public WriteOnly Property _UserUnkId() As Integer
        Set(ByVal value As Integer)
            mintUserUnkid = value
        End Set
    End Property

    Public WriteOnly Property _UserAccessFilter() As String
        Set(ByVal value As String)
            mstrUserAccessFilter = value
        End Set
    End Property

    Public Property _IncludeAccessFilterQry() As Boolean
        Get
            Return mblnIncludeAccessFilterQry
        End Get
        Set(ByVal value As Boolean)
            mblnIncludeAccessFilterQry = value
        End Set
    End Property


#End Region

#Region "Public Function & Procedures "

    Public Sub SetDefaultValue()
        Try
            mdtFromDate = Nothing
            mdtToDate = Nothing
            mintEmployeeId = 0
            mstrEmployeeName = ""
            mstrLeaveIds = ""
            mstrLeaveName = ""
            mintStatusId = 0
            mstrStatus = ""
            mstrAdvance_Filter = ""
            mintViewIndex = -1
            mstrViewByIds = ""
            mstrViewByName = ""
            mstrAnalysis_Fields = ""
            mstrAnalysis_Join = ""
            mstrAnalysis_OrderBy = ""
            mstrAnalysis_OrderBy_GName = ""
            mstrReport_GroupName = ""
            mstrOrderByQuery = ""
            'Pinkal (11-Sep-2020) -- Start
            'Optimzation Leave NMB:  Working on Optimzing Various modules for Garbage Colletion Issue.
            setDefaultOrderBy(0)
            'Pinkal (11-Sep-2020) -- End
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: SetDefaultValue; Module Name: " & mstrModuleName)
        End Try
    End Sub

    Public Sub FilterTitleAndFilterQuery()
        Me._FilterQuery = ""
        Me._FilterTitle = ""

        Try

            If mdtFromDate <> Nothing Then
                Me._FilterQuery &= " AND Convert(Char(8),LV.leavedate,112) >= @startdate "
                objDataOperation.AddParameter("@startdate", SqlDbType.VarChar, eZeeDataType.DATETIME_SIZE, eZeeDate.convertDate(mdtFromDate))
                Me._FilterTitle &= Language.getMessage(mstrModuleName, 18, "From Date: ") & " " & mdtFromDate.Date & " "
            End If

            If mdtToDate <> Nothing Then
                Me._FilterQuery &= " AND Convert(Char(8),LV.leavedate,112) <= @enddate "
                objDataOperation.AddParameter("@enddate", SqlDbType.VarChar, eZeeDataType.DATETIME_SIZE, eZeeDate.convertDate(mdtToDate))
                Me._FilterTitle &= Language.getMessage(mstrModuleName, 19, "To Date: ") & " " & mdtToDate.Date & " "
            End If

            If mintEmployeeId > 0 Then
                Me._FilterQuery &= " AND hremployee_master.employeeunkid = @employeeunkid "
                objDataOperation.AddParameter("@employeeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintEmployeeId)
                Me._FilterTitle &= Language.getMessage(mstrModuleName, 20, "Employee: ") & " " & mstrEmployeeName & " "
            End If

            If mstrLeaveIds.Trim.Length > 0 Then
                Me._FilterQuery &= " AND lvleavetype_master.leavetypeunkid in (" & mstrLeaveIds & ")"
                Me._FilterTitle &= Language.getMessage(mstrModuleName, 21, "Leave Type: ") & " " & mstrLeaveName & " "
            End If

            If mintStatusId > 0 Then
                Me._FilterQuery &= " AND LV.statusunkid = @statusunkid "
                objDataOperation.AddParameter("@statusunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintStatusId)
                Me._FilterTitle &= Language.getMessage(mstrModuleName, 22, "Status: ") & " " & mstrStatus & " "
            End If

            If mintViewIndex > 0 Then
                If Me.OrderByQuery <> "" Then
                    'Pinkal (11-Sep-2020) -- Start
                    'Optimzation Leave NMB:  Working on Optimzing Various modules for Garbage Colletion Issue.
                    'mstrOrderByQuery &= "ORDER BY Employeecode,LV.LeaveDate," & mstrAnalysis_OrderBy_GName
                    If mstrAnalysis_OrderBy_GName.Trim.Length > 0 Then
                        mstrOrderByQuery &= "ORDER BY   " & mstrAnalysis_OrderBy_GName & ",Employeecode,LV.LeaveDate"
                    Else
                        mstrOrderByQuery &= "ORDER BY Employeecode,LV.LeaveDate"
                    End If
                    'Pinkal (11-Sep-2020) -- End
                End If
            Else
                If Me.OrderByQuery <> "" Then
                    mstrOrderByQuery &= " ORDER BY Employeecode,LV.LeaveDate "
                End If
            End If

            '   Me._FilterTitle &= Language.getMessage(mstrModuleName, 21, " Order By : ") & " " & Me.OrderByDisplay

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: FilterTitleAndFilterQuery; Module Name: " & mstrModuleName)
        End Try
    End Sub

    Public Overrides Sub generateReport(ByVal pintReportType As Integer, Optional ByVal PrintAction As Aruti.Data.enPrintAction = Aruti.Data.enPrintAction.Preview, Optional ByVal ExportAction As Aruti.Data.enExportAction = Aruti.Data.enExportAction.None)

    End Sub

    Public Overrides Sub generateReportNew(ByVal xDatabaseName As String, ByVal xUserUnkid As Integer, ByVal xYearUnkid As Integer _
                                                              , ByVal xCompanyUnkid As Integer, ByVal xPeriodStart As Date, ByVal xPeriodEnd As Date, ByVal xUserModeSetting As String _
                                                              , ByVal xOnlyApproved As Boolean, ByVal xExportReportPath As String, ByVal xOpenReportAfterExport As Boolean _
                                                              , ByVal pintReportType As Integer, Optional ByVal PrintAction As Aruti.Data.enPrintAction = Aruti.Data.enPrintAction.Preview _
                                                              , Optional ByVal ExportAction As Aruti.Data.enExportAction = Aruti.Data.enExportAction.None _
                                                              , Optional ByVal intBaseCurrencyUnkid As Integer = 0)
        Dim objRpt As CrystalDecisions.CrystalReports.Engine.ReportClass = Nothing
        Dim strReportExportFile As String = ""
        Try
            If Not IsNothing(objRpt) Then
                Call ReportExecute(objRpt, PrintAction, ExportAction, xExportReportPath, xOpenReportAfterExport)
            End If
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: generateReport; Module Name: " & mstrModuleName)
        End Try
    End Sub

    Public Overrides Sub setDefaultOrderBy(ByVal intReportType As Integer)
        OrderByDisplay = ""
        OrderByQuery = ""
        Try
            OrderByDisplay = iColumn_DetailReport.ColumnItem(0).DisplayName
            OrderByQuery = iColumn_DetailReport.ColumnItem(0).Name
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: setDefaultOrderBy; Module Name: " & mstrModuleName)
        End Try
    End Sub

    Public Overrides Sub setOrderBy(ByVal intReportType As Integer)
        Try
            Call OrderByExecute(iColumn_DetailReport)
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: setOrderBy; Module Name: " & mstrModuleName)
        End Try
    End Sub

#End Region

#Region " Report Generation "

    Dim iColumn_DetailReport As New IColumnCollection

    Public Property Field_OnDetailReport() As IColumnCollection
        Get
            Return iColumn_DetailReport
        End Get
        Set(ByVal value As IColumnCollection)
            iColumn_DetailReport = value
        End Set
    End Property

    Private Sub Create_OnDetailReport()
        Try
            iColumn_DetailReport.Clear()
            'Pinkal (11-Sep-2020) -- Start
            'Optimzation Leave NMB:  Working on Optimzing Various modules for Garbage Colletion Issue.
            iColumn_DetailReport.Add(New IColumn("ISNULL(hremployee_master.employeecode,'')", Language.getMessage(mstrModuleName, 5, "Employee Code")))
            'Pinkal (11-Sep-2020) -- End
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: Create_OnDetailReport; Module Name: " & mstrModuleName)
        End Try
    End Sub

    Public Sub Generate_DetailReport(ByVal xDatabaseName As String, ByVal xUserUnkid As Integer, ByVal xYearUnkid As Integer, ByVal xCompanyUnkid As Integer _
                                                     , ByVal strEmployeeAsOnDate As String, ByVal xUserModeSetting As String, ByVal xOnlyApproved As Boolean)
        Dim objRpt As CrystalDecisions.CrystalReports.Engine.ReportClass = Nothing
        Dim StrQ As String = ""
        Dim dsList As New DataSet
        Try

            If mintCompanyUnkid <= 0 Then
                mintCompanyUnkid = Company._Object._Companyunkid
            End If

            Company._Object._Companyunkid = mintCompanyUnkid
            ConfigParameter._Object._Companyunkid = mintCompanyUnkid

            If mintUserUnkid <= 0 Then
                mintUserUnkid = User._Object._Userunkid
            End If

            User._Object._Userunkid = mintUserUnkid

            objDataOperation = New clsDataOperation
            objDataOperation.ClearParameters()

            Dim xUACQry, xUACFiltrQry, xAdvanceJoinQry As String
            xUACQry = "" : xUACFiltrQry = "" : xAdvanceJoinQry = ""
            If mblnIncludeAccessFilterQry Then Call NewAccessLevelFilterString(xUACQry, xUACFiltrQry, mdtToDate, xOnlyApproved, xDatabaseName, xUserUnkid, xCompanyUnkid, xYearUnkid, xUserModeSetting)
            Call GetAdvanceFilterQry(xAdvanceJoinQry, mdtToDate, xDatabaseName)


            StrQ = "SELECT hremployee_master.employeeunkid " & _
                     ", ISNULL(hremployee_master.employeecode,'') AS EmployeeCode " & _
                     ", ISNULL(hremployee_master.firstname, '') AS FirstName " & _
                     ", ISNULL(hremployee_master.othername, '') AS LastName " & _
                     ", ISNULL(dept.name,'') AS Department " & _
                     ", ISNULL(SGrp.name,'') AS SectionGroup " & _
                     ", ISNULL(ClsGrp.name,'') AS ClassGroup " & _
                     ", ISNULL(cls.name,'') AS Class " & _
                     ", ISNULL(jb.job_name,'') AS Job " & _
                     ", @startDate AS [From Date] " & _
                     ", @enddate AS [To Date] " & _
                     ", CONVERT(CHAR(8),LV.leavedate,112) AS [Leave Date] " & _
                     ", ISNULL(lvleavetype_master.leavename,'') AS [Leave Type] " & _
                     ", CASE WHEN LV.Statusunkid = 1 THEN @Approved " & _
                     "           WHEN LV.Statusunkid = 2 THEN @Pending " & _
                     "           WHEN LV.Statusunkid = 3 THEN @Rejected " & _
                     "           WHEN LV.Statusunkid = 4 THEN @Rescheduled " & _
                     "           WHEN LV.Statusunkid = 7 THEN @Issued " & _
                     "   END AS Status "

            If mintViewIndex > 0 Then
                StrQ &= mstrAnalysis_Fields
            Else
                StrQ &= ", '' AS GName "
            End If

            StrQ &= " FROM hremployee_master " & _
                         " LEFT JOIN " & _
                         " ( " & _
                         "    SELECT " & _
                         "         departmentunkid " & _
                         "        ,sectiongroupunkid " & _
                         "        ,classgroupunkid " & _
                         "        ,classunkid " & _
                         "        ,employeeunkid " & _
                         "        ,ROW_NUMBER()OVER(PARTITION BY employeeunkid ORDER BY effectivedate DESC) AS rno " & _
                         "    FROM hremployee_transfer_tran " & _
                         "    WHERE isvoid = 0 AND CONVERT(CHAR(8),effectivedate,112) <= @endDate " & _
                         "  ) AS Alloc ON Alloc.employeeunkid = hremployee_master.employeeunkid AND Alloc.rno = 1 " & _
                         " LEFT JOIN hrdepartment_master dept on dept.departmentunkid = Alloc.departmentunkid " & _
                         " LEFT JOIN hrsectiongroup_master SGrp on SGrp.sectiongroupunkid = Alloc.sectiongroupunkid " & _
                         " LEFT JOIN hrclassgroup_master ClsGrp on ClsGrp.classgroupunkid = Alloc.classgroupunkid " & _
                         " LEFT JOIN hrclasses_master cls on cls.classesunkid = Alloc.classunkid " & _
                         " LEFT JOIN " & _
                         " ( " & _
                         "    SELECT " & _
                         "         jobunkid " & _
                         "        ,employeeunkid " & _
                         "        ,ROW_NUMBER()OVER(PARTITION BY employeeunkid ORDER BY effectivedate DESC) AS rno " & _
                         "    FROM hremployee_categorization_tran " & _
                         "    WHERE isvoid = 0 AND CONVERT(CHAR(8),effectivedate,112) <= @endDate " & _
                         " ) AS Jobs ON Jobs.employeeunkid = hremployee_master.employeeunkid AND Jobs.rno = 1 " & _
                         " LEFT JOIN hrjob_master jb ON jb.jobunkid = Jobs.jobunkid " & _
                         " JOIN " & _
                         " ( " & _
                         "          SELECT " & _
                         "                      lvleaveform.formunkid " & _
                         "                     ,lvleaveform.formno " & _
                         "                     ,lvleaveform.employeeunkid " & _
                         "                     ,lvleaveform.leavetypeunkid " & _
                         "                     ,lvleaveform.statusunkid " & _
                         "                     ,lvleaveday_fraction.leavedate 'LeaveDate' " & _
                         "          FROM lvleaveform " & _
                         "          LEFT JOIN lvleaveday_fraction ON lvleaveday_fraction.formunkid = lvleaveform.formunkid And lvleaveday_fraction.isvoid = 0 AND lvleaveday_fraction.approverunkid <= 0 " & _
                         "          WHERE lvleaveform.isvoid = 0 AND lvleaveform.statusunkid IN (1, 2, 3, 4) " & _
                         "          UNION " & _
                         "           SELECT " & _
                         "                      lvleaveform.formunkid " & _
                         "                     ,lvleaveform.formno " & _
                         "                     ,lvleaveform.employeeunkid " & _
                         "                     ,lvleaveform.leavetypeunkid " & _
                         "                     ,lvleaveform.statusunkid " & _
                         "                     ,lvleaveIssue_tran.leavedate 'LeaveDate' " & _
                         "          FROM lvleaveform " & _
                         "          LEFT JOIN lvleaveIssue_master ON lvleaveIssue_master.formunkid = lvleaveform.formunkid AND lvleaveIssue_master.employeeunkid = lvleaveform.employeeunkid " & _
                         "          AND lvleaveform.leavetypeunkid = lvleaveIssue_master.leavetypeunkid AND lvleaveIssue_master.isvoid = 0 " & _
                         "          JOIN lvleaveIssue_tran ON lvleaveIssue_master.leaveissueunkid = lvleaveIssue_tran.leaveissueunkid	AND lvleaveIssue_master.isvoid = 0 " & _
                         "          WHERE lvleaveform.isvoid = 0	AND lvleaveform.statusunkid = 7 " & _
                         " )  as LV on LV.employeeunkid = hremployee_master.employeeunkid " & _
                         " LEFT JOIN lvleavetype_master ON lvleavetype_master.leavetypeunkid = LV.leavetypeunkid "


            If mintViewIndex > 0 Then
                StrQ &= mstrAnalysis_Join
            End If

            If xUACQry.Trim.Length > 0 Then
                StrQ &= xUACQry
            End If

            If xAdvanceJoinQry.Trim.Length > 0 Then
                StrQ &= xAdvanceJoinQry
            End If

            StrQ &= " WHERE  LV.statusunkid <> 6 "


            If mstrAdvance_Filter.Trim.Length > 0 Then
                StrQ &= " AND " & mstrAdvance_Filter
            End If

            If xUACFiltrQry.Trim.Length > 0 Then
                StrQ &= " AND " & xUACFiltrQry
            End If

            Call FilterTitleAndFilterQuery()

            StrQ &= Me._FilterQuery

            StrQ &= mstrOrderByQuery

            objDataOperation.AddParameter("@Approved", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage("clsMasterData", 110, "Approved"))
            objDataOperation.AddParameter("@Pending", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage("clsMasterData", 111, "Pending"))
            objDataOperation.AddParameter("@Rejected", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage("clsMasterData", 112, "Rejected"))
            objDataOperation.AddParameter("@Rescheduled", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage("clsMasterData", 113, "Re-Scheduled"))
            objDataOperation.AddParameter("@Issued", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage("clsMasterData", 276, "Issued"))

            dsList = objDataOperation.ExecQuery(StrQ, "DataTable")

            If objDataOperation.ErrorMessage <> "" Then
                Throw New Exception(objDataOperation.ErrorNumber & ":" & objDataOperation.ErrorMessage)
            End If

            ConfigParameter._Object.GetReportSettings(CInt(mstrReportId))

            Dim strarrGroupColumns As String() = Nothing
            Dim rowsArrayHeader As New ArrayList
            Dim rowsArrayFooter As New ArrayList
            Dim row As WorksheetRow
            Dim wcell As WorksheetCell

            Dim mdtTableExcel As DataTable = dsList.Tables(0).Clone


            mdtTableExcel = New DataView(dsList.Tables(0), "", "", DataViewRowState.CurrentRows).ToTable

            If mdtTableExcel.Columns.Contains("employeeunkid") Then
                mdtTableExcel.Columns.Remove("employeeunkid")
            End If

            If mintViewIndex > 0 Then
                If mdtTableExcel.Columns.Contains("Id") Then
                    mdtTableExcel.Columns.Remove("Id")
                End If
                mdtTableExcel.Columns("GName").Caption = mstrReport_GroupName
                Dim strGrpCols As String() = {"GName"}
                strarrGroupColumns = strGrpCols
            Else
                If mdtTableExcel.Columns.Contains("GName") Then
                    mdtTableExcel.Columns.Remove("GName")
                End If
            End If

            row = New WorksheetRow()

            If Me._FilterTitle.ToString.Length > 0 Then
                wcell = New WorksheetCell(Me._FilterTitle.ToString, "s10bw")
                wcell.MergeAcross = mdtTableExcel.Columns.Count - 1
                row.Cells.Add(wcell)
            End If
            rowsArrayHeader.Add(row)

            row = New WorksheetRow()
            wcell = New WorksheetCell("", "s10bw")
            row.Cells.Add(wcell)
            rowsArrayHeader.Add(row)


            row = New WorksheetRow()
            wcell = New WorksheetCell("", "s10bw")
            row.Cells.Add(wcell)
            rowsArrayFooter.Add(row)
            '--------------------

            If ConfigParameter._Object._IsShowPreparedBy = True Then
                row = New WorksheetRow()

                Dim objUser As New clsUserAddEdit
                objUser._Userunkid = xUserUnkid
                wcell = New WorksheetCell(Language.getMessage(mstrModuleName, 1, "Prepared By :") & Space(10) & objUser._Username, "s8bw")
                wcell.MergeAcross = 4
                row.Cells.Add(wcell)
                objUser = Nothing

                wcell = New WorksheetCell("", "s10bw")
                row.Cells.Add(wcell)
                rowsArrayFooter.Add(row)

                row = New WorksheetRow()
                wcell = New WorksheetCell("", "s10bw")
                row.Cells.Add(wcell)
                rowsArrayFooter.Add(row)
            End If


            If ConfigParameter._Object._IsShowCheckedBy = True Then
                row = New WorksheetRow()
                wcell = New WorksheetCell(Language.getMessage(mstrModuleName, 2, "Checked By :"), "s8bw")
                wcell.MergeAcross = 4
                row.Cells.Add(wcell)

                wcell = New WorksheetCell("", "s10bw")
                row.Cells.Add(wcell)
                rowsArrayFooter.Add(row)

                row = New WorksheetRow()
                wcell = New WorksheetCell("", "s10bw")
                row.Cells.Add(wcell)
                rowsArrayFooter.Add(row)
            End If


            If ConfigParameter._Object._IsShowApprovedBy = True Then
                row = New WorksheetRow()
                wcell = New WorksheetCell(Language.getMessage(mstrModuleName, 3, "Approved By"), "s8bw")
                wcell.MergeAcross = 4
                row.Cells.Add(wcell)

                wcell = New WorksheetCell("", "s10bw")
                row.Cells.Add(wcell)
                rowsArrayFooter.Add(row)

                row = New WorksheetRow()
                wcell = New WorksheetCell("", "s10bw")
                row.Cells.Add(wcell)
                rowsArrayFooter.Add(row)
            End If

            If ConfigParameter._Object._IsShowReceivedBy = True Then
                row = New WorksheetRow()
                wcell = New WorksheetCell(Language.getMessage(mstrModuleName, 4, "Received By :"), "s8bw")
                wcell.MergeAcross = 4
                row.Cells.Add(wcell)
                rowsArrayFooter.Add(row)
            End If


            '--------------------



            For i As Integer = 0 To mdtTableExcel.Rows.Count - 1
                If mdtTableExcel.Rows(i)("From Date").ToString.Length > 0 Then
                    mdtTableExcel.Rows(i)("From Date") = FormatDateTime(eZeeDate.convertDate(mdtTableExcel.Rows(i)("From Date").ToString()), DateFormat.ShortDate)
                End If
                If mdtTableExcel.Rows(i)("To Date").ToString.Length > 0 Then
                    mdtTableExcel.Rows(i)("To Date") = FormatDateTime(eZeeDate.convertDate(mdtTableExcel.Rows(i)("To Date").ToString()), DateFormat.ShortDate)
                End If
                If mdtTableExcel.Rows(i)("Leave Date").ToString.Length > 0 Then
                    mdtTableExcel.Rows(i)("Leave Date") = FormatDateTime(eZeeDate.convertDate(mdtTableExcel.Rows(i)("Leave Date").ToString()), DateFormat.ShortDate)
                End If
            Next


            'SET EXCEL CELL WIDTH  
            Dim intArrayColumnWidth As Integer() = Nothing
            ReDim intArrayColumnWidth(mdtTableExcel.Columns.Count - 1)
            For i As Integer = 0 To intArrayColumnWidth.Length - 1
                intArrayColumnWidth(i) = 125
            Next
            'SET EXCEL CELL WIDTH


            mdtTableExcel.Columns("EmployeeCode").Caption = Language.getMessage(mstrModuleName, 5, "Employee Code")
            mdtTableExcel.Columns("FirstName").Caption = Language.getMessage(mstrModuleName, 6, "First Name")
            mdtTableExcel.Columns("LastName").Caption = Language.getMessage(mstrModuleName, 7, "Last Name")
            mdtTableExcel.Columns("Department").Caption = Language.getMessage(mstrModuleName, 8, "Department")
            mdtTableExcel.Columns("SectionGroup").Caption = Language.getMessage(mstrModuleName, 9, "Section Group")
            mdtTableExcel.Columns("ClassGroup").Caption = Language.getMessage(mstrModuleName, 10, "Class Group")
            mdtTableExcel.Columns("Class").Caption = Language.getMessage(mstrModuleName, 11, "Class")
            mdtTableExcel.Columns("Job").Caption = Language.getMessage(mstrModuleName, 12, "Job")
            mdtTableExcel.Columns("From Date").Caption = Language.getMessage(mstrModuleName, 13, "From Date")
            mdtTableExcel.Columns("To Date").Caption = Language.getMessage(mstrModuleName, 14, "To Date")
            mdtTableExcel.Columns("Leave Date").Caption = Language.getMessage(mstrModuleName, 15, "Leave Date")
            mdtTableExcel.Columns("Leave Type").Caption = Language.getMessage(mstrModuleName, 16, "Leave Type")
            mdtTableExcel.Columns("Status").Caption = Language.getMessage(mstrModuleName, 17, "Status")

            Call ReportExecute(Nothing, enPrintAction.None, enExportAction.ExcelExtra, mstrExportReportPath, mblnOpenAfterExport, mdtTableExcel, intArrayColumnWidth, True, True, False, strarrGroupColumns, Me._ReportName, "", " ", , "", False, rowsArrayHeader, rowsArrayFooter, Nothing, Nothing, False)

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: Generate_DetailReport; Module Name: " & mstrModuleName)
        End Try
    End Sub


#End Region



    '<Language> This Auto Generated Text Please Do Not Modify it.
#Region " Language & UI Settings "
    Public Shared Sub SetMessages()
        Try
            Language.setMessage(mstrModuleName, 1, "Prepared By :")
            Language.setMessage(mstrModuleName, 2, "Checked By :")
            Language.setMessage(mstrModuleName, 3, "Approved By")
            Language.setMessage(mstrModuleName, 4, "Received By :")
            Language.setMessage(mstrModuleName, 5, "Employee Code")
            Language.setMessage(mstrModuleName, 6, "First Name")
            Language.setMessage(mstrModuleName, 7, "Last Name")
            Language.setMessage(mstrModuleName, 8, "Department")
            Language.setMessage(mstrModuleName, 9, "Section Group")
            Language.setMessage(mstrModuleName, 10, "Class Group")
            Language.setMessage(mstrModuleName, 11, "Class")
            Language.setMessage(mstrModuleName, 12, "Job")
            Language.setMessage(mstrModuleName, 13, "From Date")
            Language.setMessage(mstrModuleName, 14, "To Date")
            Language.setMessage(mstrModuleName, 15, "Leave Date")
            Language.setMessage(mstrModuleName, 16, "Leave Type")
            Language.setMessage(mstrModuleName, 17, "Status")
            Language.setMessage(mstrModuleName, 18, "From Date:")
            Language.setMessage(mstrModuleName, 19, "To Date:")
            Language.setMessage(mstrModuleName, 20, "Employee:")
            Language.setMessage(mstrModuleName, 21, "Leave Type:")
            Language.setMessage(mstrModuleName, 22, "Status:")
            Language.setMessage("clsMasterData", 110, "Approved")
            Language.setMessage("clsMasterData", 111, "Pending")
            Language.setMessage("clsMasterData", 112, "Rejected")
            Language.setMessage("clsMasterData", 113, "Re-Scheduled")
            Language.setMessage("clsMasterData", 276, "Issued")

        Catch Ex As Exception
            Throw New Exception(Ex.Message & "; Procedure Name: SetMessages; Module Name: " & mstrModuleName)
        End Try
    End Sub
#End Region 'Language & UI Settings
	'</Language>
End Class
