'************************************************************************************************************************************
'Class Name : clsLeaveLiability_Report.vb
'Purpose    :
'Date       :  08-Jul-2014
'Written By :Pinkal
'Modified   :
'************************************************************************************************************************************

Imports eZeeCommonLib
Imports Aruti.Data
Imports ExcelWriter

''' <summary>
''' Purpose: Report Generation Class 
''' Developer: Pinkal
''' </summary>
''' 
Public Class clsLeaveLiability_Report
    Inherits IReportData
    Private Shared ReadOnly mstrModuleName As String = "clsLeaveLiability_Report"
    Private mstrReportId As String = enArutiReport.Leave_Liability_Report
    Dim objDataOperation As clsDataOperation

#Region " Constructor "
    Public Sub New(Byval intLangId As Integer,Byval intCompanyId AS Integer) 'S.SANDEEP |24-Sep-2020| -- START --END
        Me.setReportData(CInt(mstrReportId),intLangId,intCompanyId) 'S.SANDEEP |24-Sep-2020| -- START --END
        Call Create_OnDetailReport()
    End Sub
#End Region

#Region " Private variables "

    Private mintEmployeeID As Integer = 0
    Private mstrEmployeeName As String = ""
    Private mintLeaveId As Integer = 0
    Private mstrLeaveName As String = ""
    Private mstrOrderByQuery As String = ""
    Private mblnIncludeInactiveEmp As Boolean = False
    Private mstrAdvance_Filter As String = String.Empty
    Private mintViewIndex As Integer = -1
    Private mstrViewByIds As String = ""
    Private mstrViewByName As String = ""
    Private mstrAnalysis_Fields As String = ""
    Private mstrAnalysis_Join As String = ""
    Private mstrAnalysis_OrderBy As String = ""
    Private mstrAnalysis_OrderBy_GName As String = ""
    Private mstrReport_GroupName As String = ""
    Private mstrExportReportPath As String = ConfigParameter._Object._ExportReportPath
    Private mblnOpenAfterExport As Boolean = ConfigParameter._Object._OpenAfterExport
    Private mintUserUnkid As Integer = -1
    Private mintCompanyUnkid As Integer = -1
    Private mstrUserAccessFilter As String = String.Empty
    Private mintLeaveBalanceSetting As Integer = -1
    Private mintYearId As Integer = -1
    Private mblnFirstNamethenSurname As Boolean = ConfigParameter._Object._FirstNamethenSurname
    Private mintOtherEarningTranId As Integer = -1
    Private mstrLeaveLiabilitySetting As String = ""
    Private mintLeaveLiabilityDays As Integer = 0

    'Pinkal (12-Jan-2019) -- Start
    'Enhancement [Ref #: 0003343] - Working on TUJIJENGE for Leave Liability Report.
    Private mblnFinClose As Boolean = False
    'Pinkal (12-Jan-2019) -- End

    'Pinkal (03-May-2019) -- Start
    'Enhancement - Working on Leave UAT Changes for NMB.
    Private mdtAsonDate As Date = Nothing
    Private mintLeaveAccrueTenureSetting As Integer = 0
    Private mintLeaveAccrueDaysAfterEachMonth As Integer = 0
    Private mdtDatabaseStartDate As Date = Nothing
    Private mdtDatabaseEndDate As Date = Nothing
    'Pinkal (03-May-2019) -- End


    'Pinkal (26-Aug-2019) -- Start
    'Enhancement NMB - Working on Leave Liability Report to include Leave BF as per NMB Requirement.
    Private mblnIncludeLeaveBF As Boolean = False
    'Pinkal (26-Aug-2019) -- End


    'Pinkal (30-Jun-2021)-- Start
    'Netiz / Scania Enhancements : Working on Leave Liability Report Enhancements.
    Private mstrOtherEarningTranHead As String = ""
    'Pinkal (30-Jun-2021) -- End


#End Region

#Region " Properties "


    Public WriteOnly Property _EmployeeId() As Integer
        Set(ByVal value As Integer)
            mintEmployeeID = value
        End Set
    End Property

    Public WriteOnly Property _Employee() As String
        Set(ByVal value As String)
            mstrEmployeeName = value
        End Set
    End Property

    Public WriteOnly Property _LeaveId() As Integer
        Set(ByVal value As Integer)
            mintLeaveId = value
        End Set
    End Property

    Public WriteOnly Property _LeaveName() As String
        Set(ByVal value As String)
            mstrLeaveName = value
        End Set
    End Property

    Public WriteOnly Property _IncludeInactiveEmp() As Boolean
        Set(ByVal value As Boolean)
            mblnIncludeInactiveEmp = value
        End Set
    End Property

    Public WriteOnly Property _Advance_Filter() As String
        Set(ByVal value As String)
            mstrAdvance_Filter = value
        End Set
    End Property

    Public WriteOnly Property _ViewIndex() As Integer
        Set(ByVal value As Integer)
            mintViewIndex = value
        End Set
    End Property

    Public WriteOnly Property _ViewByIds() As String
        Set(ByVal value As String)
            mstrViewByIds = value
        End Set
    End Property

    Public WriteOnly Property _ViewByName() As String
        Set(ByVal value As String)
            mstrViewByName = value
        End Set
    End Property

    Public WriteOnly Property _Analysis_Fields() As String
        Set(ByVal value As String)
            mstrAnalysis_Fields = value
        End Set
    End Property

    Public WriteOnly Property _Analysis_Join() As String
        Set(ByVal value As String)
            mstrAnalysis_Join = value
        End Set
    End Property

    Public WriteOnly Property _Analysis_OrderBy() As String
        Set(ByVal value As String)
            mstrAnalysis_OrderBy = value
        End Set
    End Property

    Public WriteOnly Property _Analysis_OrderBy_GName() As String
        Set(ByVal value As String)
            mstrAnalysis_OrderBy_GName = value
        End Set
    End Property

    Public WriteOnly Property _Report_GroupName() As String
        Set(ByVal value As String)
            mstrReport_GroupName = value
        End Set
    End Property

    Public WriteOnly Property _ExportReportPath() As String
        Set(ByVal value As String)
            mstrExportReportPath = value
        End Set
    End Property

    Public WriteOnly Property _OpenAfterExport() As Boolean
        Set(ByVal value As Boolean)
            mblnOpenAfterExport = value
        End Set
    End Property

    Public WriteOnly Property _CompanyUnkId() As Integer
        Set(ByVal value As Integer)
            mintCompanyUnkid = value
        End Set
    End Property

    Public WriteOnly Property _UserUnkId() As Integer
        Set(ByVal value As Integer)
            mintUserUnkid = value
        End Set
    End Property

    Public WriteOnly Property _UserAccessFilter() As String
        Set(ByVal value As String)
            mstrUserAccessFilter = value
        End Set
    End Property

    Public WriteOnly Property _LeaveBalanceSetting() As Integer
        Set(ByVal value As Integer)
            mintLeaveBalanceSetting = value
        End Set
    End Property

    Public WriteOnly Property _YearId() As Integer
        Set(ByVal value As Integer)
            mintYearId = value
        End Set
    End Property

    Public WriteOnly Property _OtherEarningTranId() As Integer
        Set(ByVal value As Integer)
            mintOtherEarningTranId = value
        End Set
    End Property


    'Pinkal (30-Jun-2021)-- Start
    'Netiz / Scania Enhancements : Working on Leave Liability Report Enhancements.
    Public WriteOnly Property _OtherEarningTranHead() As String
        Set(ByVal value As String)
            mstrOtherEarningTranHead = value
        End Set
    End Property
    'Pinkal (30-Jun-2021) -- End


    Public WriteOnly Property _LeaveLiabilitySetting() As String
        Set(ByVal value As String)
            mstrLeaveLiabilitySetting = value
        End Set
    End Property

    'Pinkal (12-Jan-2019) -- Start
    'Enhancement [Ref #: 0003343] - Working on TUJIJENGE for Leave Liability Report.
    Public WriteOnly Property _IsFinClose() As Boolean
        Set(ByVal value As Boolean)
            mblnFinClose = value
        End Set
    End Property
    'Pinkal (12-Jan-2019) -- End


    'Pinkal (03-May-2019) -- Start
    'Enhancement - Working on Leave UAT Changes for NMB.

    Public WriteOnly Property _AsonDate() As Date
        Set(ByVal value As Date)
            mdtAsonDate = value
        End Set
    End Property

    Public WriteOnly Property _LeaveAccrueTenureSetting() As Integer
        Set(ByVal value As Integer)
            mintLeaveAccrueTenureSetting = value
        End Set
    End Property

    Public WriteOnly Property _LeaveAccrueDaysAfterEachMonth() As Integer
        Set(ByVal value As Integer)
            mintLeaveAccrueDaysAfterEachMonth = value
        End Set
    End Property

    Public WriteOnly Property _DatabaseStartDate() As Date
        Set(ByVal value As Date)
            mdtDatabaseStartDate = value
        End Set
    End Property

    Public WriteOnly Property _DatabaseEndDate() As Date
        Set(ByVal value As Date)
            mdtDatabaseEndDate = value
        End Set
    End Property

    'Pinkal (03-May-2019) -- End

    'Pinkal (26-Aug-2019) -- Start
    'Enhancement NMB - Working on Leave Liability Report to include Leave BF as per NMB Requirement.

    Public WriteOnly Property _IncludeLeaveBF() As Boolean
        Set(ByVal value As Boolean)
            mblnIncludeLeaveBF = value
        End Set
    End Property

    'Pinkal (26-Aug-2019) -- End

#End Region

#Region "Public Function & Procedures "

    Public Sub SetDefaultValue()
        Try
            mintEmployeeID = 0
            mstrEmployeeName = ""
            mintLeaveId = 0
            mstrLeaveName = ""
            mstrOrderByQuery = ""
            mblnIncludeInactiveEmp = False
            mstrAdvance_Filter = ""
            mintViewIndex = -1
            mstrViewByIds = ""
            mstrViewByName = ""
            mstrAnalysis_Fields = ""
            mstrAnalysis_Join = ""
            mstrAnalysis_OrderBy = ""
            mstrAnalysis_OrderBy_GName = ""
            mstrReport_GroupName = ""
            mstrOrderByQuery = ""

            'Pinkal (12-Jan-2019) -- Start
            'Enhancement [Ref #: 0003343] - Working on TUJIJENGE for Leave Liability Report.
            mblnFinClose = False
            'Pinkal (12-Jan-2019) -- End


            'Pinkal (03-May-2019) -- Start
            'Enhancement - Working on Leave UAT Changes for NMB.
            mdtAsonDate = Nothing
            mintLeaveAccrueTenureSetting = 0
            mintLeaveAccrueDaysAfterEachMonth = 0
            mdtDatabaseStartDate = Nothing
            mdtDatabaseEndDate = Nothing
            'Pinkal (03-May-2019) -- End


            'Pinkal (26-Aug-2019) -- Start
            'Enhancement NMB - Working on Leave Liability Report to include Leave BF as per NMB Requirement.
            mblnIncludeLeaveBF = False
            'Pinkal (26-Aug-2019) -- End


        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: SetDefaultValue; Module Name: " & mstrModuleName)
        End Try
    End Sub

    Public Sub FilterTitleAndFilterQuery()
        Me._FilterQuery = ""
        Me._FilterTitle = ""

        Try

            If mintEmployeeID > 0 Then
                Me._FilterQuery &= " AND hremployee_master.employeeunkid = @employeeunkid "
                objDataOperation.AddParameter("@employeeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintEmployeeID)
                Me._FilterTitle &= Language.getMessage(mstrModuleName, 18, "Staff Name: ") & " " & mstrEmployeeName & " "
            End If

            If mintLeaveId > 0 Then
                Me._FilterTitle &= Language.getMessage(mstrModuleName, 19, "Leave Name: ") & " " & mstrLeaveName & " "
            End If


            'Pinkal (30-Jun-2021)-- Start
            'Netiz / Scania Enhancements : Working on Leave Liability Report Enhancements.
            If mintOtherEarningTranId > 0 Then
                Me._FilterTitle &= Language.getMessage(mstrModuleName, 32, "Other Earning: ") & " " & mstrOtherEarningTranHead & " "
            End If
            'Pinkal (30-Jun-2021) -- End


            If mblnIncludeInactiveEmp = False Then
                objDataOperation.AddParameter("@startdate", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, ConfigParameter._Object._EmployeeAsOnDate)
                objDataOperation.AddParameter("@enddate", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, ConfigParameter._Object._EmployeeAsOnDate)
            End If

            If Me.OrderByQuery <> "" Then
                If mintViewIndex > 0 Then
                    mstrOrderByQuery &= "  ORDER BY GName," & Me.OrderByQuery
                Else
                    mstrOrderByQuery &= "  ORDER BY " & Me.OrderByQuery
                End If

                Me._FilterTitle &= Language.getMessage(mstrModuleName, 20, " Order By : ") & " " & Me.OrderByDisplay
            End If

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: FilterTitleAndFilterQuery; Module Name: " & mstrModuleName)
        End Try
    End Sub

    Public Overrides Sub generateReport(ByVal pintReportType As Integer, Optional ByVal PrintAction As Aruti.Data.enPrintAction = Aruti.Data.enPrintAction.Preview, Optional ByVal ExportAction As Aruti.Data.enExportAction = Aruti.Data.enExportAction.None)
        'Dim objRpt As CrystalDecisions.CrystalReports.Engine.ReportClass = Nothing
        'Dim strReportExportFile As String = ""
        'Try
        '    If Not IsNothing(objRpt) Then
        '        Call ReportExecute(objRpt, PrintAction, ExportAction, " ", False)
        '    End If
        'Catch ex As Exception
        '    Throw New Exception(ex.Message & "; Procedure Name: generateReport; Module Name: " & mstrModuleName)
        'End Try
    End Sub

    Public Overrides Sub generateReportNew(ByVal xDatabaseName As String, ByVal xUserUnkid As Integer, ByVal xYearUnkid As Integer _
                                                              , ByVal xCompanyUnkid As Integer, ByVal xPeriodStart As Date, ByVal xPeriodEnd As Date _
                                                              , ByVal xUserModeSetting As String, ByVal xOnlyApproved As Boolean, ByVal xExportReportPath As String _
                                                              , ByVal xOpenReportAfterExport As Boolean, ByVal pintReportType As Integer _
                                                              , Optional ByVal PrintAction As Aruti.Data.enPrintAction = Aruti.Data.enPrintAction.Preview _
                                                              , Optional ByVal ExportAction As Aruti.Data.enExportAction = Aruti.Data.enExportAction.None, Optional ByVal intBaseCurrencyUnkid As Integer = 0)
        Dim objRpt As CrystalDecisions.CrystalReports.Engine.ReportClass = Nothing
        Dim strReportExportFile As String = ""
        Try
            If Not IsNothing(objRpt) Then
                Call ReportExecute(objRpt, PrintAction, ExportAction, " ", False)
            End If
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: generateReport; Module Name: " & mstrModuleName)
        End Try
    End Sub

    Public Overrides Sub setDefaultOrderBy(ByVal intReportType As Integer)
        OrderByDisplay = ""
        OrderByQuery = ""
        Try
            OrderByDisplay = iColumn_DetailReport.ColumnItem(0).DisplayName
            OrderByQuery = iColumn_DetailReport.ColumnItem(0).Name
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: setDefaultOrderBy; Module Name: " & mstrModuleName)
        End Try
    End Sub

    Public Overrides Sub setOrderBy(ByVal intReportType As Integer)
        Try
            Call OrderByExecute(iColumn_DetailReport)
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: setOrderBy; Module Name: " & mstrModuleName)
        End Try
    End Sub

#End Region

#Region " Report Generation "

    Dim iColumn_DetailReport As New IColumnCollection

    Public Property Field_OnDetailReport() As IColumnCollection
        Get
            Return iColumn_DetailReport
        End Get
        Set(ByVal value As IColumnCollection)
            iColumn_DetailReport = value
        End Set
    End Property

    Private Sub Create_OnDetailReport()
        Try
            iColumn_DetailReport.Clear()
            iColumn_DetailReport.Add(New IColumn("hremployee_master.employeecode", Language.getMessage(mstrModuleName, 11, "Employee ID")))

            If mblnFirstNamethenSurname = True Then
                iColumn_DetailReport.Add(New IColumn(" ISNULL(hremployee_master.firstname, '') + ' ' + ISNULL(hremployee_master.othername, '') + ' ' + ISNULL(hremployee_master.surname, '')", Language.getMessage(mstrModuleName, 12, "Staff Name")))
            Else
                iColumn_DetailReport.Add(New IColumn("ISNULL(hremployee_master.surname, '') + ' ' + ISNULL(hremployee_master.firstname, '') + ' ' + ISNULL(hremployee_master.othername, '')", Language.getMessage(mstrModuleName, 12, "Staff Name")))
            End If
            iColumn_DetailReport.Add(New IColumn("CONVERT(char(10),hremployee_master.appointeddate,103)", Language.getMessage(mstrModuleName, 14, "Hire Date")))

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: Create_OnDetailReport; Module Name: " & mstrModuleName)
        End Try
    End Sub

    'Public Sub Generate_DetailReport()
    '    Dim StrQ As String = ""
    '    Dim StrQFilter As String = ""
    '    Dim dsList As New DataSet
    '    Try
    '        Dim objMaster As New clsMasterData
    '        Dim objExchangeRate As New clsExchangeRate
    '        Dim decDecimalPlaces As Decimal = 0
    '        Dim mintPeriodId As Integer = -1

    '        objExchangeRate._ExchangeRateunkid = ConfigParameter._Object._Base_CurrencyId
    '        decDecimalPlaces = objExchangeRate._Digits_After_Decimal

    '        mintPeriodId = objMaster.getFirstPeriodID(enModuleReference.Payroll, 1)  'GET LAST OPEN PERIOD


    '        objDataOperation = New clsDataOperation
    '        objDataOperation.ClearParameters()

    '        StrQ = "SELECT hremployee_master.employeeunkid " & _
    '                  ",hremployee_master.employeecode "

    '        If mblnFirstNamethenSurname = True Then
    '            StrQ &= ", ISNULL(hremployee_master.firstname, '') + ' ' + ISNULL(hremployee_master.othername, '') + ' ' + ISNULL(hremployee_master.surname, '') AS employee "
    '        Else
    '            StrQ &= ", ISNULL(hremployee_master.surname, '') + ' ' + ISNULL(hremployee_master.firstname, '') + ' ' + ISNULL(hremployee_master.othername, '') AS employee "
    '        End If

    '        StrQ &= ", ISNULL(dept.name,'') AS Department " & _
    '                     ", CONVERT(char(10),hremployee_master.appointeddate,103) AS appointeddate " & _
    '                     ", ISNULL(A.BasicSal,0.00) AS scale " & _
    '                     ", 0.00 AS dayscale "

    '        If mintViewIndex > 0 Then
    '            StrQ &= mstrAnalysis_Fields
    '        Else
    '            StrQ &= ", '' AS GName "
    '        End If

    '        StrQ &= " from hremployee_master " & _
    '                  " JOIN hrdepartment_master dept on dept.departmentunkid = hremployee_master.departmentunkid "


    '        'Pinkal (16-Sep-2014) -- Start
    '        'Enhancement - Zanzibar Residence Leave Liability Report Changes For Basic Salary

    '        StrQ &= " LEFT JOIN  " & _
    '                     " ( Select " & _
    '                     "		 prpayrollprocess_tran.employeeunkid AS Empid " & _
    '                     "		,SUM(CAST(ISNULL(prpayrollprocess_tran.amount, 0) AS DECIMAL(36, " & decDecimalPlaces & "))) AS BasicSal " & _
    '                     "        ,cfcommon_period_tran.periodunkid AS PrdId " & _
    '                                                  "FROM      prpayrollprocess_tran " & _
    '                                                            "JOIN prtranhead_master ON prpayrollprocess_tran.tranheadunkid = prtranhead_master.tranheadunkid " & _
    '                                                            "JOIN prtnaleave_tran ON prtnaleave_tran.tnaleavetranunkid = prpayrollprocess_tran.tnaleavetranunkid " & _
    '                                                            "JOIN cfcommon_period_tran ON prtnaleave_tran.payperiodunkid = cfcommon_period_tran.periodunkid " & _
    '                   "		JOIN hremployee_master ON hremployee_master.employeeunkid = prpayrollprocess_tran.employeeunkid "

    '        StrQ &= mstrAnalysis_Join

    '        StrQ &= "	WHERE       ISNULL(prpayrollprocess_tran.isvoid, 0) = 0 " & _
    '                                                    "AND ISNULL(prtranhead_master.isvoid, 0) = 0 " & _
    '                                                    "AND ISNULL(prtnaleave_tran.isvoid, 0) = 0 " & _
    '                                "AND prpayrollprocess_tran.tranheadunkid > 0 " & _
    '                                                    "AND cfcommon_period_tran.isactive = 1 " & _
    '                                "AND cfcommon_period_tran.periodunkid IN (" & mintPeriodId & ")   "

    '        If mintOtherEarningTranId > 0 Then 'Basic Salary as Other Earning
    '            StrQ &= "AND prpayrollprocess_tran.tranheadunkid =  " & mintOtherEarningTranId
    '        Else
    '            StrQ &= "AND prtranhead_master.typeof_id = " & enTypeOf.Salary & "  "
    '        End If

    '        StrQ &= "	GROUP BY prpayrollprocess_tran.employeeunkid,cfcommon_period_tran.periodunkid )  AS A  " & _
    '                    " ON A.Empid  = hremployee_master.employeeunkid "

    '        'Pinkal (16-Sep-2014) -- End


    '        If mintViewIndex > 0 Then
    '            StrQ &= mstrAnalysis_Join
    '        End If

    '        StrQ &= " WHERE  1=1 "

    '        If mstrAdvance_Filter.Trim.Length > 0 Then
    '            StrQ &= " AND " & mstrAdvance_Filter
    '        End If

    '        If mblnIncludeInactiveEmp = False Then
    '            StrQ &= " AND CONVERT(CHAR(8),hremployee_master.appointeddate,112) <= @enddate " & _
    '                " AND ISNULL(CONVERT(CHAR(8),hremployee_master.termination_from_date,112),@startdate) >= @startdate " & _
    '                " AND ISNULL(CONVERT(CHAR(8),hremployee_master.termination_to_date,112),@startdate) >= @startdate " & _
    '                " AND ISNULL(CONVERT(CHAR(8), hremployee_master.empl_enddate,112), @startdate) >= @startdate "
    '        End If

    '        If mstrUserAccessFilter.Trim.Length <= 0 Then
    '            mstrUserAccessFilter = UserAccessLevel._AccessLevelFilterString
    '        End If

    '        If mstrUserAccessFilter.Trim.Length > 0 Then
    '            StrQ &= mstrUserAccessFilter
    '        End If

    '        Call FilterTitleAndFilterQuery()

    '        StrQ &= Me._FilterQuery

    '        StrQ &= mstrOrderByQuery

    '        dsList = objDataOperation.ExecQuery(StrQ, "DataTable")

    '        If objDataOperation.ErrorMessage <> "" Then
    '            Throw New Exception(objDataOperation.ErrorNumber & ":" & objDataOperation.ErrorMessage)
    '        End If


    '        Dim strarrGroupColumns As String() = Nothing
    '        Dim rowsArrayHeader As New ArrayList
    '        Dim rowsArrayFooter As New ArrayList
    '        Dim row As WorksheetRow
    '        Dim wcell As WorksheetCell

    '        ConfigParameter._Object.GetReportSettings(CInt(mstrReportId))


    '        Dim mdtTableExcel As DataTable = dsList.Tables(0)

    '        'For Each dtRow As DataRow In mdtTableExcel.Rows

    '        '    Dim dsScale As DataSet = objMaster.Get_Current_Scale("List", CInt(dtRow.Item("employeeunkid")), ConfigParameter._Object._CurrentDateAndTime.Date)

    '        '    If dsScale IsNot Nothing AndAlso dsScale.Tables(0).Rows.Count > 0 Then
    '        '        dtRow("Scale") = CDec(dsScale.Tables(0).Rows(0)("newscale"))
    '        '        dtRow("dayscale") = Math.Round(CDec(dsScale.Tables(0).Rows(0)("newscale")) / 26, 3)
    '        '    End If

    '        'Next


    '        If mdtTableExcel.Columns.Contains("Total Days") = False Then
    '            mdtTableExcel.Columns.Add("Total Days", Type.GetType("System.Decimal"))
    '            mdtTableExcel.Columns("Total Days").DefaultValue = 0
    '        End If

    '        If mdtTableExcel.Columns.Contains("Total Value") = False Then
    '            mdtTableExcel.Columns.Add("Total Value", Type.GetType("System.Decimal"))
    '            mdtTableExcel.Columns("Total Value").DefaultValue = 0
    '        End If

    '        Dim objLeaveType As New clsleavetype_master
    '        Dim objAccrue As New clsleavebalance_tran
    '        Dim dsLeaveType As DataSet = Nothing

    '        If mintLeaveId > 0 Then
    '            dsLeaveType = objLeaveType.GetList("List", True, True, "ispaid = 1 AND isaccrueamount = 1 AND leavetypeunkid = " & mintLeaveId)
    '        Else
    '            dsLeaveType = objLeaveType.GetList("List", True, True, "ispaid = 1 AND isaccrueamount = 1")
    '        End If

    '        Dim dcHeader As New Dictionary(Of Integer, String)
    '        Dim intColumnIndex As Integer = -1

    '        intColumnIndex = mdtTableExcel.Columns.Count - 6

    '        If dsLeaveType IsNot Nothing AndAlso dsLeaveType.Tables(0).Rows.Count > 0 Then
    '            Dim intEmpID As Integer = -1
    '            For Each dtRow As DataRow In dsLeaveType.Tables(0).Rows

    '                intColumnIndex += 1
    '                If dcHeader.ContainsKey(intColumnIndex) = False Then
    '                    dcHeader.Add(intColumnIndex, dtRow("leavename").ToString() & " " & Language.getMessage(mstrModuleName, 23, "Accrued"))
    '                End If

    '                mdtTableExcel.Columns.Add(dtRow("leavename").ToString() & " " & Language.getMessage(mstrModuleName, 1, "Days Accrued"), Type.GetType("System.Decimal"))
    '                mdtTableExcel.Columns(dtRow("leavename").ToString() & " " & Language.getMessage(mstrModuleName, 1, "Days Accrued")).DefaultValue = 0
    '                mdtTableExcel.Columns(dtRow("leavename").ToString() & " " & Language.getMessage(mstrModuleName, 1, "Days Accrued")).Caption = dtRow("leavename").ToString() & " " & Language.getMessage(mstrModuleName, 21, "Days")

    '                intColumnIndex += 1
    '                mdtTableExcel.Columns.Add(dtRow("leavename").ToString() & " " & Language.getMessage(mstrModuleName, 2, "Value Accrued"), Type.GetType("System.Decimal"))
    '                mdtTableExcel.Columns(dtRow("leavename").ToString() & " " & Language.getMessage(mstrModuleName, 2, "Value Accrued")).DefaultValue = 0
    '                mdtTableExcel.Columns(dtRow("leavename").ToString() & " " & Language.getMessage(mstrModuleName, 2, "Value Accrued")).Caption = dtRow("leavename").ToString() & " " & Language.getMessage(mstrModuleName, 22, "Value")

    '                intColumnIndex += 1
    '                If dcHeader.ContainsKey(intColumnIndex) = False Then
    '                    dcHeader.Add(intColumnIndex, dtRow("leavename").ToString() & " " & Language.getMessage(mstrModuleName, 24, "Consumed"))
    '                End If

    '                mdtTableExcel.Columns.Add(dtRow("leavename").ToString() & " " & Language.getMessage(mstrModuleName, 3, "Days Consumed"), Type.GetType("System.Decimal"))
    '                mdtTableExcel.Columns(dtRow("leavename").ToString() & " " & Language.getMessage(mstrModuleName, 3, "Days Consumed")).DefaultValue = 0
    '                mdtTableExcel.Columns(dtRow("leavename").ToString() & " " & Language.getMessage(mstrModuleName, 3, "Days Consumed")).Caption = dtRow("leavename").ToString() & " " & Language.getMessage(mstrModuleName, 21, "Days")

    '                intColumnIndex += 1
    '                mdtTableExcel.Columns.Add(dtRow("leavename").ToString() & " " & Language.getMessage(mstrModuleName, 4, "Value Consumed"), Type.GetType("System.Decimal"))
    '                mdtTableExcel.Columns(dtRow("leavename").ToString() & " " & Language.getMessage(mstrModuleName, 4, "Value Consumed")).DefaultValue = 0
    '                mdtTableExcel.Columns(dtRow("leavename").ToString() & " " & Language.getMessage(mstrModuleName, 4, "Value Consumed")).Caption = dtRow("leavename").ToString() & " " & Language.getMessage(mstrModuleName, 22, "Value")

    '                intColumnIndex += 1
    '                If dcHeader.ContainsKey(intColumnIndex) = False Then
    '                    dcHeader.Add(intColumnIndex, dtRow("leavename").ToString() & " " & Language.getMessage(mstrModuleName, 25, "Balance"))
    '                End If

    '                mdtTableExcel.Columns.Add(dtRow("leavename").ToString() & " " & Language.getMessage(mstrModuleName, 5, "Days Balance"), Type.GetType("System.Decimal"))
    '                mdtTableExcel.Columns(dtRow("leavename").ToString() & " " & Language.getMessage(mstrModuleName, 5, "Days Balance")).DefaultValue = 0
    '                mdtTableExcel.Columns(dtRow("leavename").ToString() & " " & Language.getMessage(mstrModuleName, 5, "Days Balance")).Caption = dtRow("leavename").ToString() & " " & Language.getMessage(mstrModuleName, 21, "Days")

    '                intColumnIndex += 1
    '                mdtTableExcel.Columns.Add(dtRow("leavename").ToString() & " " & Language.getMessage(mstrModuleName, 6, "Value Balance"), Type.GetType("System.Decimal"))
    '                mdtTableExcel.Columns(dtRow("leavename").ToString() & " " & Language.getMessage(mstrModuleName, 6, "Value Balance")).DefaultValue = 0
    '                mdtTableExcel.Columns(dtRow("leavename").ToString() & " " & Language.getMessage(mstrModuleName, 6, "Value Balance")).Caption = dtRow("leavename").ToString() & " " & Language.getMessage(mstrModuleName, 22, "Value")


    '                For Each dRow As DataRow In mdtTableExcel.Rows
    '                    Dim dsBalance As DataSet = Nothing
    '                    If mintLeaveBalanceSetting = enLeaveBalanceSetting.Financial_Year Then
    '                        dsBalance = objAccrue.GetEmployeeBalanceData(CInt(dtRow("leavetypeunkid")), CInt(dRow("employeeunkid")), True, mintYearId, False, False, False)
    '                    Else
    '                        dsBalance = objAccrue.GetEmployeeBalanceData(CInt(dtRow("leavetypeunkid")), CInt(dRow("employeeunkid")), True, mintYearId, True, True, False)
    '                    End If

    '                    If dsBalance IsNot Nothing AndAlso dsBalance.Tables(0).Rows.Count > 0 Then
    '                        dRow(dtRow("leavename").ToString() & " " & Language.getMessage(mstrModuleName, 1, "Days Accrued")) = Math.Round(CDec(dsBalance.Tables(0).Rows(0)("accrue_amount")), 2)
    '                        dRow(dtRow("leavename").ToString() & " " & Language.getMessage(mstrModuleName, 2, "Value Accrued")) = Math.Round(CDec(dsBalance.Tables(0).Rows(0)("accrue_amount")) * CDec(dRow("dayscale")), 2)

    '                        dRow(dtRow("leavename").ToString() & " " & Language.getMessage(mstrModuleName, 3, "Days Consumed")) = Math.Round(CDec(dsBalance.Tables(0).Rows(0)("issue_amount")), 2)
    '                        dRow(dtRow("leavename").ToString() & " " & Language.getMessage(mstrModuleName, 4, "Value Consumed")) = Math.Round(CDec(dsBalance.Tables(0).Rows(0)("issue_amount")) * CDec(dRow("dayscale")), 2)

    '                        dRow(dtRow("leavename").ToString() & " " & Language.getMessage(mstrModuleName, 5, "Days Balance")) = CDec(dRow(dtRow("leavename").ToString() & " " & Language.getMessage(mstrModuleName, 1, "Days Accrued"))) - CDec(dRow(dtRow("leavename").ToString() & " " & Language.getMessage(mstrModuleName, 3, "Days Consumed")))
    '                        dRow(dtRow("leavename").ToString() & " " & Language.getMessage(mstrModuleName, 6, "Value Balance")) = dRow(dtRow("leavename").ToString() & " " & Language.getMessage(mstrModuleName, 2, "Value Accrued")) - dRow(dtRow("leavename").ToString() & " " & Language.getMessage(mstrModuleName, 4, "Value Consumed"))

    '                        dRow("Total Days") = IIf(IsDBNull(dRow("Total Days")), 0, dRow("Total Days")) + (Math.Round(CDec(dsBalance.Tables(0).Rows(0)("accrue_amount")), 2) - Math.Round(CDec(dsBalance.Tables(0).Rows(0)("issue_amount")), 2))
    '                        dRow("Total Value") = IIf(IsDBNull(dRow("Total Value")), 0, dRow("Total Value")) + (CDec(dRow(dtRow("leavename").ToString() & " " & Language.getMessage(mstrModuleName, 2, "Value Accrued")) - dRow(dtRow("leavename").ToString() & " " & Language.getMessage(mstrModuleName, 4, "Value Consumed"))))
    '                    End If

    '                Next

    '            Next
    '            mdtTableExcel.AcceptChanges()
    '        End If



    '        If mintViewIndex > 0 Then
    '            If mdtTableExcel.Columns.Contains("Id") Then
    '                mdtTableExcel.Columns.Remove("Id")
    '            End If
    '            mdtTableExcel.Columns("GName").Caption = mstrReport_GroupName
    '            Dim strGrpCols As String() = {"GName"}
    '            strarrGroupColumns = strGrpCols
    '        End If

    '        If mdtTableExcel.Columns.Contains("employeeunkid") Then
    '            mdtTableExcel.Columns.Remove("employeeunkid")
    '        End If

    '        If mdtTableExcel.Columns.Contains("dayscale") Then
    '            mdtTableExcel.Columns.Remove("dayscale")
    '        End If

    '        If mintViewIndex <= 0 AndAlso mdtTableExcel.Columns.Contains("GName") Then
    '            mdtTableExcel.Columns.Remove("GName")
    '        End If

    '        mdtTableExcel.Columns("employeecode").Caption = Language.getMessage(mstrModuleName, 11, "Employee ID")
    '        mdtTableExcel.Columns("employee").Caption = Language.getMessage(mstrModuleName, 12, "Staff Name")
    '        mdtTableExcel.Columns("Department").Caption = Language.getMessage(mstrModuleName, 13, "Sub Department")
    '        mdtTableExcel.Columns("appointeddate").Caption = Language.getMessage(mstrModuleName, 14, "Hire Date")
    '        mdtTableExcel.Columns("scale").Caption = Language.getMessage(mstrModuleName, 15, "Basic Salary")

    '        mdtTableExcel.Columns("Total Days").SetOrdinal(mdtTableExcel.Columns.Count - 1)
    '        mdtTableExcel.Columns("Total Value").SetOrdinal(mdtTableExcel.Columns.Count - 1)
    '        mdtTableExcel.Columns("Total Days").Caption = Language.getMessage(mstrModuleName, 16, "Total Days")
    '        mdtTableExcel.Columns("Total Value").Caption = Language.getMessage(mstrModuleName, 17, "Total Balance")

    '        row = New WorksheetRow()
    '        If Me._FilterTitle.ToString.Length > 0 Then
    '            wcell = New WorksheetCell(Me._FilterTitle.ToString, "s10bw")
    '            wcell.MergeAcross = mdtTableExcel.Columns.Count - 1
    '            row.Cells.Add(wcell)
    '        End If
    '        rowsArrayHeader.Add(row)


    '        row = New WorksheetRow()
    '        Dim intCount As Integer = 0
    '        Do While intCount <= mdtTableExcel.Columns.Count - 1
    '            If dcHeader.ContainsKey(intCount) Then
    '                wcell = New WorksheetCell(dcHeader.Item(intCount), "HeaderStyle")
    '                wcell.MergeAcross = 1
    '                intCount += 1
    '            ElseIf intCount = mdtTableExcel.Columns.Count - 2 Then
    '                wcell = New WorksheetCell(Language.getMessage(mstrModuleName, 26, "Outstanding"), "HeaderStyle")
    '                wcell.MergeAcross = 1
    '                intCount += 1
    '            Else
    '                wcell = New WorksheetCell("", "HeaderStyle")
    '            End If
    '            row.Cells.Add(wcell)
    '            intCount += 1
    '        Loop
    '        rowsArrayHeader.Add(row)


    '        row = New WorksheetRow()
    '        wcell = New WorksheetCell("", "s10bw")
    '        row.Cells.Add(wcell)
    '        rowsArrayFooter.Add(row)
    '        '--------------------

    '        If ConfigParameter._Object._IsShowPreparedBy = True Then
    '            row = New WorksheetRow()
    '            wcell = New WorksheetCell(Language.getMessage(mstrModuleName, 7, "Prepared By :") & Space(10) & User._Object._Username, "s8bw")
    '            wcell.MergeAcross = 4
    '            row.Cells.Add(wcell)

    '            wcell = New WorksheetCell("", "s10bw")
    '            row.Cells.Add(wcell)
    '            rowsArrayFooter.Add(row)

    '            row = New WorksheetRow()
    '            wcell = New WorksheetCell("", "s10bw")
    '            row.Cells.Add(wcell)
    '            rowsArrayFooter.Add(row)
    '        End If


    '        If ConfigParameter._Object._IsShowCheckedBy = True Then
    '            row = New WorksheetRow()
    '            wcell = New WorksheetCell(Language.getMessage(mstrModuleName, 8, "Checked By :"), "s8bw")
    '            wcell.MergeAcross = 4
    '            row.Cells.Add(wcell)

    '            wcell = New WorksheetCell("", "s10bw")
    '            row.Cells.Add(wcell)
    '            rowsArrayFooter.Add(row)

    '            row = New WorksheetRow()
    '            wcell = New WorksheetCell("", "s10bw")
    '            row.Cells.Add(wcell)
    '            rowsArrayFooter.Add(row)
    '        End If


    '        If ConfigParameter._Object._IsShowApprovedBy = True Then
    '            row = New WorksheetRow()
    '            wcell = New WorksheetCell(Language.getMessage(mstrModuleName, 9, "Approved By"), "s8bw")
    '            wcell.MergeAcross = 4
    '            row.Cells.Add(wcell)

    '            wcell = New WorksheetCell("", "s10bw")
    '            row.Cells.Add(wcell)
    '            rowsArrayFooter.Add(row)

    '            row = New WorksheetRow()
    '            wcell = New WorksheetCell("", "s10bw")
    '            row.Cells.Add(wcell)
    '            rowsArrayFooter.Add(row)
    '        End If

    '        If ConfigParameter._Object._IsShowReceivedBy = True Then
    '            row = New WorksheetRow()
    '            wcell = New WorksheetCell(Language.getMessage(mstrModuleName, 10, "Received By :"), "s8bw")
    '            wcell.MergeAcross = 4
    '            row.Cells.Add(wcell)
    '            rowsArrayFooter.Add(row)
    '        End If

    '        '--------------------

    '        'SET EXCEL CELL WIDTH  
    '        Dim intArrayColumnWidth As Integer() = Nothing
    '        ReDim intArrayColumnWidth(mdtTableExcel.Columns.Count - 1)
    '        For i As Integer = 0 To intArrayColumnWidth.Length - 1
    '            intArrayColumnWidth(i) = 125
    '        Next
    '        'SET EXCEL CELL WIDTH






    '        Call ReportExecute(Nothing, enPrintAction.None, enExportAction.ExcelExtra, mstrExportReportPath, mblnOpenAfterExport, mdtTableExcel, intArrayColumnWidth, True, True, True, strarrGroupColumns, Me._ReportName, "", " ", , "", True, rowsArrayHeader, rowsArrayFooter, Nothing, Nothing, True)

    '    Catch ex As Exception
    '        Throw New Exception(ex.Message & "; Procedure Name: Generate_DetailReport; Module Name: " & mstrModuleName)
    '    End Try
    'End Sub


    Public Sub Generate_DetailReport(ByVal xDatabaseName As String, ByVal xUserUnkid As Integer, ByVal xYearUnkid As Integer, ByVal xCompanyUnkid As Integer _
                                                              , ByVal strEmployeeAsOnDate As String, ByVal xUserModeSetting As String, ByVal xOnlyApproved As Boolean, Optional ByVal intBaseCurrencyUnkid As Integer = 0)
        Dim StrQ As String = ""
        Dim StrQFilter As String = ""
        Dim dsList As New DataSet
        Try
            Dim objMaster As New clsMasterData
            Dim objExchangeRate As New clsExchangeRate
            Dim decDecimalPlaces As Decimal = 0
            Dim mintPeriodId As Integer = -1

            objExchangeRate._ExchangeRateunkid = intBaseCurrencyUnkid
            decDecimalPlaces = objExchangeRate._Digits_After_Decimal

            'Pinkal (12-Jan-2019) -- Start
            'Enhancement [Ref #: 0003343] - Working on TUJIJENGE for Leave Liability Report.
            'mintPeriodId = objMaster.getFirstPeriodID(enModuleReference.Payroll, xYearUnkid, 1)  'GET LAST OPEN PERIOD
            mintPeriodId = objMaster.getFirstPeriodID(enModuleReference.Payroll, xYearUnkid, 1, False, True)  'GET LAST OPEN PERIOD
            'Pinkal (12-Jan-2019) -- End


            Dim mintDays As Integer = 0
            Dim mdtStartDate As DateTime = Nothing
            Dim mdtEndDate As DateTime = Nothing
            Dim objPeriod As New clscommom_period_Tran
            objPeriod._Periodunkid(xDatabaseName) = mintPeriodId
            mdtStartDate = objPeriod._Start_Date.Date
            mdtEndDate = objPeriod._End_Date.Date
            objPeriod = Nothing

            objDataOperation = New clsDataOperation

            Dim xDateJoinQry, xDateFilterQry, xUACQry, xUACFiltrQry, xAdvanceJoinQry As String
            xDateJoinQry = "" : xDateFilterQry = "" : xUACQry = "" : xUACFiltrQry = "" : xAdvanceJoinQry = ""
            Call GetDatesFilterString(xDateJoinQry, xDateFilterQry, eZeeDate.convertDate(strEmployeeAsOnDate), eZeeDate.convertDate(strEmployeeAsOnDate), , , xDatabaseName)
            Call NewAccessLevelFilterString(xUACQry, xUACFiltrQry, eZeeDate.convertDate(strEmployeeAsOnDate), xOnlyApproved, xDatabaseName, xUserUnkid, xCompanyUnkid, xYearUnkid, xUserModeSetting)
            Call GetAdvanceFilterQry(xAdvanceJoinQry, eZeeDate.convertDate(strEmployeeAsOnDate), xDatabaseName)

            objDataOperation.ClearParameters()

            StrQ = "SELECT hremployee_master.employeeunkid " & _
                      ",hremployee_master.employeecode "

            If mblnFirstNamethenSurname = True Then
                StrQ &= ", ISNULL(hremployee_master.firstname, '') + ' ' + ISNULL(hremployee_master.othername, '') + ' ' + ISNULL(hremployee_master.surname, '') AS employee "
            Else
                StrQ &= ", ISNULL(hremployee_master.surname, '') + ' ' + ISNULL(hremployee_master.firstname, '') + ' ' + ISNULL(hremployee_master.othername, '') AS employee "
            End If

            StrQ &= ", ISNULL(dept.name,'') AS Department " & _
                         ", CONVERT(char(10),hremployee_master.appointeddate,103) AS appointeddate " & _
                         ", ISNULL(A.BasicSal,0.00) AS scale " & _
                         ", 0.00 AS dayscale "

            If mintViewIndex > 0 Then
                StrQ &= mstrAnalysis_Fields
            Else
                StrQ &= ", '' AS GName "
            End If

            StrQ &= " from hremployee_master " & _
                         " LEFT JOIN " & _
                        " ( " & _
                        "    SELECT " & _
                        "         departmentunkid " & _
                        "        ,employeeunkid " & _
                        "        ,ROW_NUMBER()OVER(PARTITION BY employeeunkid ORDER BY effectivedate DESC) AS rno " & _
                        "    FROM hremployee_transfer_tran " & _
                        "    WHERE isvoid = 0 AND CONVERT(CHAR(8),effectivedate,112) <= '" & strEmployeeAsOnDate & "' " & _
                        " ) AS Alloc ON Alloc.employeeunkid = hremployee_master.employeeunkid AND Alloc.rno = 1 " & _
                        " JOIN hrdepartment_master dept on dept.departmentunkid = Alloc.departmentunkid "

            StrQ &= " LEFT JOIN  " & _
                         " ( Select " & _
                         "		 prpayrollprocess_tran.employeeunkid AS Empid " & _
                         "		,SUM(CAST(ISNULL(prpayrollprocess_tran.amount, 0) AS DECIMAL(36, " & decDecimalPlaces & "))) AS BasicSal " & _
                         "        ,cfcommon_period_tran.periodunkid AS PrdId " & _
                                                      "FROM      prpayrollprocess_tran " & _
                                                                "JOIN prtranhead_master ON prpayrollprocess_tran.tranheadunkid = prtranhead_master.tranheadunkid " & _
                                                                "JOIN prtnaleave_tran ON prtnaleave_tran.tnaleavetranunkid = prpayrollprocess_tran.tnaleavetranunkid " & _
                                                                "JOIN cfcommon_period_tran ON prtnaleave_tran.payperiodunkid = cfcommon_period_tran.periodunkid " & _
                       "		JOIN hremployee_master ON hremployee_master.employeeunkid = prpayrollprocess_tran.employeeunkid "

            StrQ &= mstrAnalysis_Join

            If xDateJoinQry.Trim.Length > 0 Then
                StrQ &= xDateJoinQry
            End If

            If xUACQry.Trim.Length > 0 Then
                StrQ &= xUACQry
            End If

            If xAdvanceJoinQry.Trim.Length > 0 Then
                StrQ &= xAdvanceJoinQry
            End If


            StrQ &= "	WHERE       ISNULL(prpayrollprocess_tran.isvoid, 0) = 0 " & _
                                                        "AND ISNULL(prtranhead_master.isvoid, 0) = 0 " & _
                                                        "AND ISNULL(prtnaleave_tran.isvoid, 0) = 0 " & _
                                    "AND prpayrollprocess_tran.tranheadunkid > 0 " & _
                                                        "AND cfcommon_period_tran.isactive = 1 " & _
                                    "AND cfcommon_period_tran.periodunkid IN (" & mintPeriodId & ")   "

            If mintOtherEarningTranId > 0 Then 'Basic Salary as Other Earning
                StrQ &= "AND prpayrollprocess_tran.tranheadunkid =  " & mintOtherEarningTranId
            Else
                StrQ &= "AND prtranhead_master.typeof_id = " & enTypeOf.Salary & "  "
            End If

            If mstrAdvance_Filter.Trim.Length > 0 Then
                StrQ &= " AND " & mstrAdvance_Filter
            End If

            If xUACFiltrQry.Trim.Length > 0 Then
                StrQ &= " AND " & xUACFiltrQry
            End If

            If mblnIncludeInactiveEmp = False Then
                If xDateFilterQry.Trim.Length > 0 Then
                    StrQ &= xDateFilterQry
                End If
            End If

            StrQ &= "	GROUP BY prpayrollprocess_tran.employeeunkid,cfcommon_period_tran.periodunkid )  AS A  " & _
                        " ON A.Empid  = hremployee_master.employeeunkid "


            If mintViewIndex > 0 Then
                StrQ &= mstrAnalysis_Join
            End If


            If xDateJoinQry.Trim.Length > 0 Then
                StrQ &= xDateJoinQry
            End If

            If xUACQry.Trim.Length > 0 Then
                StrQ &= xUACQry
            End If

            If xAdvanceJoinQry.Trim.Length > 0 Then
                StrQ &= xAdvanceJoinQry
            End If

            StrQ &= " WHERE  1=1 "

            If mstrAdvance_Filter.Trim.Length > 0 Then
                StrQ &= " AND " & mstrAdvance_Filter
            End If

            If xUACFiltrQry.Trim.Length > 0 Then
                StrQ &= " AND " & xUACFiltrQry
            End If

            If mblnIncludeInactiveEmp = False Then
                If xDateFilterQry.Trim.Length > 0 Then
                    StrQ &= xDateFilterQry
                End If
            End If

            Call FilterTitleAndFilterQuery()

            StrQ &= Me._FilterQuery

            StrQ &= mstrOrderByQuery

            dsList = objDataOperation.ExecQuery(StrQ, "DataTable")

            If objDataOperation.ErrorMessage <> "" Then
                Throw New Exception(objDataOperation.ErrorNumber & ":" & objDataOperation.ErrorMessage)
            End If


            Dim strarrGroupColumns As String() = Nothing
            Dim rowsArrayHeader As New ArrayList
            Dim rowsArrayFooter As New ArrayList
            Dim row As WorksheetRow
            Dim wcell As WorksheetCell

            ConfigParameter._Object.GetReportSettings(CInt(mstrReportId))


            Dim mdtTableExcel As DataTable = dsList.Tables(0)

            For Each dtRow As DataRow In mdtTableExcel.Rows

                If mstrLeaveLiabilitySetting.ToString.Contains("|") = False Then
                    If mstrLeaveLiabilitySetting = CStr(enLeaveLiabilitySetting.Days_With_Weekend_Included) Then
                        mintDays = DateDiff(DateInterval.Day, mdtStartDate.Date, mdtEndDate.Date.AddDays(1))
                    ElseIf mstrLeaveLiabilitySetting = CStr(enLeaveLiabilitySetting.Days_With_Weekend_Excluded) Then
                        Dim objEmpShift As New clsEmployee_Shift_Tran
                        objEmpShift.GetWorkingDaysAndHours(xDatabaseName, xUserUnkid, xYearUnkid, xCompanyUnkid, xUserModeSetting, CInt(dtRow.Item("employeeunkid")), mdtStartDate.Date, mdtEndDate.Date, mintDays, 0)
                        objEmpShift = Nothing
                    End If
                Else
                    Dim ar() As String = mstrLeaveLiabilitySetting.ToString().Split(CChar("|"))
                    If ar.Length > 1 Then
                        If ar(1).ToString().Trim.Length > 0 Then
                            mintDays = CInt(ar(1))
                        End If
                    End If
                End If


                'Pinkal (30-Jun-2021)-- Start
                'Netiz / Scania Enhancements : Working on Leave Liability Report Enhancements.
                If mintOtherEarningTranId > 0 Then 'Basic Salary as Other Earning
                    dtRow("dayscale") = Math.Round(CDec(dtRow("Scale")) / mintDays, 3)
                Else
                Dim dsScale As DataSet = objMaster.Get_Current_Scale("List", CInt(dtRow.Item("employeeunkid")), mdtEndDate.Date)

                If dsScale IsNot Nothing AndAlso dsScale.Tables(0).Rows.Count > 0 Then
                    dtRow("Scale") = CDec(dsScale.Tables(0).Rows(0)("newscale"))
                    dtRow("dayscale") = Math.Round(CDec(dsScale.Tables(0).Rows(0)("newscale")) / mintDays, 3)
                End If
                End If
                'Pinkal (30-Jun-2021) -- End
            Next

            If mdtTableExcel.Columns.Contains("Total Days") = False Then
                mdtTableExcel.Columns.Add("Total Days", Type.GetType("System.Decimal"))
                mdtTableExcel.Columns("Total Days").DefaultValue = 0
            End If

            If mdtTableExcel.Columns.Contains("Total Value") = False Then
                mdtTableExcel.Columns.Add("Total Value", Type.GetType("System.Decimal"))
                mdtTableExcel.Columns("Total Value").DefaultValue = 0
            End If

            Dim objLeaveType As New clsleavetype_master
            Dim objAccrue As New clsleavebalance_tran
            Dim dsLeaveType As DataSet = Nothing

            If mintLeaveId > 0 Then
                dsLeaveType = objLeaveType.GetList("List", True, True, "ispaid = 1 AND isaccrueamount = 1 AND leavetypeunkid = " & mintLeaveId)
            Else
                dsLeaveType = objLeaveType.GetList("List", True, True, "ispaid = 1 AND isaccrueamount = 1")
            End If

            Dim dcHeader As New Dictionary(Of Integer, String)
            Dim intColumnIndex As Integer = -1

            intColumnIndex = mdtTableExcel.Columns.Count - 6

            If dsLeaveType IsNot Nothing AndAlso dsLeaveType.Tables(0).Rows.Count > 0 Then
                Dim intEmpID As Integer = -1
                For Each dtRow As DataRow In dsLeaveType.Tables(0).Rows

                    'Pinkal (26-Aug-2019) -- Start
                    'Enhancement NMB - Working on Leave Liability Report to include Leave BF as per NMB Requirement.
                    If mblnIncludeLeaveBF Then
                        intColumnIndex += 1
                        If dcHeader.ContainsKey(intColumnIndex) = False Then
                            dcHeader.Add(intColumnIndex, dtRow("leavename").ToString() & " " & Language.getMessage(mstrModuleName, 29, "Leave BF"))
                        End If
                        mdtTableExcel.Columns.Add(dtRow("leavename").ToString() & " " & Language.getMessage(mstrModuleName, 30, "BF Days"), Type.GetType("System.Decimal"))
                        mdtTableExcel.Columns(dtRow("leavename").ToString() & " " & Language.getMessage(mstrModuleName, 30, "BF Days")).DefaultValue = 0

                        intColumnIndex += 1
                        mdtTableExcel.Columns.Add(dtRow("leavename").ToString() & " " & Language.getMessage(mstrModuleName, 31, "BF Value"), Type.GetType("System.Decimal"))
                        mdtTableExcel.Columns(dtRow("leavename").ToString() & " " & Language.getMessage(mstrModuleName, 31, "BF Value")).DefaultValue = 0
                    End If
                    'Pinkal (26-Aug-2019) -- End


                    intColumnIndex += 1
                    If dcHeader.ContainsKey(intColumnIndex) = False Then
                        dcHeader.Add(intColumnIndex, dtRow("leavename").ToString() & " " & Language.getMessage(mstrModuleName, 23, "Accrued"))
                    End If

                    mdtTableExcel.Columns.Add(dtRow("leavename").ToString() & " " & Language.getMessage(mstrModuleName, 1, "Days Accrued"), Type.GetType("System.Decimal"))
                    mdtTableExcel.Columns(dtRow("leavename").ToString() & " " & Language.getMessage(mstrModuleName, 1, "Days Accrued")).DefaultValue = 0
                    mdtTableExcel.Columns(dtRow("leavename").ToString() & " " & Language.getMessage(mstrModuleName, 1, "Days Accrued")).Caption = dtRow("leavename").ToString() & " " & Language.getMessage(mstrModuleName, 21, "Days")

                    intColumnIndex += 1
                    mdtTableExcel.Columns.Add(dtRow("leavename").ToString() & " " & Language.getMessage(mstrModuleName, 2, "Value Accrued"), Type.GetType("System.Decimal"))
                    mdtTableExcel.Columns(dtRow("leavename").ToString() & " " & Language.getMessage(mstrModuleName, 2, "Value Accrued")).DefaultValue = 0
                    mdtTableExcel.Columns(dtRow("leavename").ToString() & " " & Language.getMessage(mstrModuleName, 2, "Value Accrued")).Caption = dtRow("leavename").ToString() & " " & Language.getMessage(mstrModuleName, 22, "Value")

                    intColumnIndex += 1
                    If dcHeader.ContainsKey(intColumnIndex) = False Then
                        dcHeader.Add(intColumnIndex, dtRow("leavename").ToString() & " " & Language.getMessage(mstrModuleName, 24, "Consumed"))
                    End If

                    mdtTableExcel.Columns.Add(dtRow("leavename").ToString() & " " & Language.getMessage(mstrModuleName, 3, "Days Consumed"), Type.GetType("System.Decimal"))
                    mdtTableExcel.Columns(dtRow("leavename").ToString() & " " & Language.getMessage(mstrModuleName, 3, "Days Consumed")).DefaultValue = 0
                    mdtTableExcel.Columns(dtRow("leavename").ToString() & " " & Language.getMessage(mstrModuleName, 3, "Days Consumed")).Caption = dtRow("leavename").ToString() & " " & Language.getMessage(mstrModuleName, 21, "Days")

                    intColumnIndex += 1
                    mdtTableExcel.Columns.Add(dtRow("leavename").ToString() & " " & Language.getMessage(mstrModuleName, 4, "Value Consumed"), Type.GetType("System.Decimal"))
                    mdtTableExcel.Columns(dtRow("leavename").ToString() & " " & Language.getMessage(mstrModuleName, 4, "Value Consumed")).DefaultValue = 0
                    mdtTableExcel.Columns(dtRow("leavename").ToString() & " " & Language.getMessage(mstrModuleName, 4, "Value Consumed")).Caption = dtRow("leavename").ToString() & " " & Language.getMessage(mstrModuleName, 22, "Value")

                    intColumnIndex += 1
                    If dcHeader.ContainsKey(intColumnIndex) = False Then
                        dcHeader.Add(intColumnIndex, dtRow("leavename").ToString() & " " & Language.getMessage(mstrModuleName, 25, "Balance"))
                    End If

                    mdtTableExcel.Columns.Add(dtRow("leavename").ToString() & " " & Language.getMessage(mstrModuleName, 5, "Days Balance"), Type.GetType("System.Decimal"))
                    mdtTableExcel.Columns(dtRow("leavename").ToString() & " " & Language.getMessage(mstrModuleName, 5, "Days Balance")).DefaultValue = 0
                    mdtTableExcel.Columns(dtRow("leavename").ToString() & " " & Language.getMessage(mstrModuleName, 5, "Days Balance")).Caption = dtRow("leavename").ToString() & " " & Language.getMessage(mstrModuleName, 21, "Days")

                    intColumnIndex += 1
                    mdtTableExcel.Columns.Add(dtRow("leavename").ToString() & " " & Language.getMessage(mstrModuleName, 6, "Value Balance"), Type.GetType("System.Decimal"))
                    mdtTableExcel.Columns(dtRow("leavename").ToString() & " " & Language.getMessage(mstrModuleName, 6, "Value Balance")).DefaultValue = 0
                    mdtTableExcel.Columns(dtRow("leavename").ToString() & " " & Language.getMessage(mstrModuleName, 6, "Value Balance")).Caption = dtRow("leavename").ToString() & " " & Language.getMessage(mstrModuleName, 22, "Value")


                    For Each dRow As DataRow In mdtTableExcel.Rows
                        Dim dsBalance As DataSet = Nothing

                        'Pinkal (12-Jan-2019) -- Start
                        'Enhancement [Ref #: 0003343] - Working on TUJIJENGE for Leave Liability Report.
                        'If mintLeaveBalanceSetting = enLeaveBalanceSetting.Financial_Year Then
                        '    dsBalance = objAccrue.GetEmployeeBalanceData(CInt(dtRow("leavetypeunkid")), CInt(dRow("employeeunkid")), True, mintYearId, False, False, False)
                        'Else
                        '    dsBalance = objAccrue.GetEmployeeBalanceData(CInt(dtRow("leavetypeunkid")), CInt(dRow("employeeunkid")), True, mintYearId, True, True, False)
                        'End If


                        'Pinkal (03-May-2019) -- Start
                        'Enhancement - Working on Leave UAT Changes for NMB.

                        Dim dsBalanceAsonDate As DataSet = Nothing
                        Dim mdecLeaveIssueAsOnDate As Decimal = 0
                        Dim mdecValueAccruedAsonDate As Decimal = 0
                        Dim mdecValueConsumedAsonDate As Decimal = 0
                        Dim objleaveIssuetran As New clsleaveissue_Tran

                        If mdtAsonDate = Nothing Then

                            If mintLeaveBalanceSetting = enLeaveBalanceSetting.Financial_Year Then
                                dsBalance = objAccrue.GetEmployeeBalanceData(CInt(dtRow("leavetypeunkid")), CInt(dRow("employeeunkid")), True, mintYearId, False, False, mblnFinClose)
                            Else
                                dsBalance = objAccrue.GetEmployeeBalanceData(CInt(dtRow("leavetypeunkid")), CInt(dRow("employeeunkid")), True, mintYearId, True, True, mblnFinClose)
                            End If

                        ElseIf mdtAsonDate <> Nothing Then

                            If mintLeaveBalanceSetting = enLeaveBalanceSetting.Financial_Year Then
                                dsBalance = objAccrue.GetLeaveBalanceInfo(mdtAsonDate, CInt(dtRow("leavetypeunkid")), CInt(dRow("employeeunkid")), mintLeaveAccrueTenureSetting _
                                                                                                , mintLeaveAccrueDaysAfterEachMonth, mdtDatabaseStartDate.Date, mdtDatabaseEndDate.Date, False, False _
                                                                                                , mintLeaveBalanceSetting, mblnFinClose, mintYearId, Nothing, "")

                                mdecLeaveIssueAsOnDate = objleaveIssuetran.GetEmployeeIssueDaysCount(CInt(dRow("employeeunkid")), CInt(dtRow("leavetypeunkid")), mintYearId, mdtAsonDate, Nothing, Nothing, Nothing, Nothing, "")

                            ElseIf mintLeaveBalanceSetting = enLeaveBalanceSetting.ELC Then
                                objAccrue._DBStartdate = mdtDatabaseStartDate.Date
                                objAccrue._DBEnddate = mdtDatabaseEndDate.Date
                                dsBalance = objAccrue.GetLeaveBalanceInfo(mdtAsonDate, CInt(dtRow("leavetypeunkid")), CInt(dRow("employeeunkid")), mintLeaveAccrueTenureSetting _
                                                                                               , mintLeaveAccrueDaysAfterEachMonth, , , True, True, mintLeaveBalanceSetting, mblnFinClose, mintYearId, Nothing, "")

                                mdecLeaveIssueAsOnDate = objleaveIssuetran.GetEmployeeIssueDaysCount(CInt(dRow("employeeunkid")), CInt(dtRow("leavetypeunkid")), mintYearId, mdtAsonDate, CDate(dsBalance.Tables(0).Rows(0)("startdate")).Date, CDate(dsBalance.Tables(0).Rows(0)("enddate")).Date, mdtStartDate, Nothing, "")
                            End If
                        End If

                        'Pinkal (03-May-2019) -- End


                        'Pinkal (12-Jan-2019) -- End

                        If dsBalance IsNot Nothing AndAlso dsBalance.Tables(0).Rows.Count > 0 Then


                            'Pinkal (26-Aug-2019) -- Start
                            'Enhancement NMB - Working on Leave Liability Report to include Leave BF as per NMB Requirement.
                            If mblnIncludeLeaveBF Then
                                dRow(dtRow("leavename").ToString() & " " & Language.getMessage(mstrModuleName, 30, "BF Days")) = Math.Round(CDec(dsBalance.Tables(0).Rows(0)("LeaveBF")), 2)
                                dRow(dtRow("leavename").ToString() & " " & Language.getMessage(mstrModuleName, 31, "BF Value")) = Math.Round(CDec(dsBalance.Tables(0).Rows(0)("LeaveBF")) * CDec(dRow("dayscale")), 2)
                            End If
                            'Pinkal (26-Aug-2019) -- End


                            'Pinkal (28-Jun-2021)-- Start
                            'Leave Liability Report Bug : Solved Leave Liability report bug.
                            If mdtAsonDate <> Nothing Then
                                dRow(dtRow("leavename").ToString() & " " & Language.getMessage(mstrModuleName, 1, "Days Accrued")) = Math.Round(CDec(dsBalance.Tables(0).Rows(0)("accrue_amount")), 2)
                                dRow(dtRow("leavename").ToString() & " " & Language.getMessage(mstrModuleName, 2, "Value Accrued")) = Math.Round(CDec(dsBalance.Tables(0).Rows(0)("accrue_amount")) * CDec(dRow("dayscale")), 2)
                            ElseIf mdtAsonDate = Nothing Then
                                If mblnIncludeLeaveBF Then
                                    dRow(dtRow("leavename").ToString() & " " & Language.getMessage(mstrModuleName, 1, "Days Accrued")) = Math.Round((CDec(dsBalance.Tables(0).Rows(0)("accrue_amount")) - CDec(dsBalance.Tables(0).Rows(0)("LeaveBF"))), 2)
                                    dRow(dtRow("leavename").ToString() & " " & Language.getMessage(mstrModuleName, 2, "Value Accrued")) = Math.Round((CDec(dsBalance.Tables(0).Rows(0)("accrue_amount")) - CDec(dsBalance.Tables(0).Rows(0)("LeaveBF"))) * CDec(dRow("dayscale")), 2)
                                Else
                            dRow(dtRow("leavename").ToString() & " " & Language.getMessage(mstrModuleName, 1, "Days Accrued")) = Math.Round(CDec(dsBalance.Tables(0).Rows(0)("accrue_amount")), 2)
                            dRow(dtRow("leavename").ToString() & " " & Language.getMessage(mstrModuleName, 2, "Value Accrued")) = Math.Round(CDec(dsBalance.Tables(0).Rows(0)("accrue_amount")) * CDec(dRow("dayscale")), 2)
                                End If
                            End If
                            'Pinkal (28-Jun-2021) -- End

                            'Pinkal (03-May-2019) -- Start
                            'Enhancement - Working on Leave UAT Changes for NMB.
                            If mdtAsonDate <> Nothing Then
                                dRow(dtRow("leavename").ToString() & " " & Language.getMessage(mstrModuleName, 3, "Days Consumed")) = Math.Round(mdecLeaveIssueAsOnDate, 2)
                                dRow(dtRow("leavename").ToString() & " " & Language.getMessage(mstrModuleName, 4, "Value Consumed")) = Math.Round(mdecLeaveIssueAsOnDate * CDec(dRow("dayscale")), 2)
                            Else
                                dRow(dtRow("leavename").ToString() & " " & Language.getMessage(mstrModuleName, 3, "Days Consumed")) = Math.Round(CDec(dsBalance.Tables(0).Rows(0)("issue_amount")), 2)
                                dRow(dtRow("leavename").ToString() & " " & Language.getMessage(mstrModuleName, 4, "Value Consumed")) = Math.Round(CDec(dsBalance.Tables(0).Rows(0)("issue_amount")) * CDec(dRow("dayscale")), 2)
                            End If
                            'Pinkal (03-May-2019) -- End


                            'Pinkal (28-Jun-2021)-- Start
                            'Leave Liability Report Bug : Solved Leave Liability report bug.
                            If mdtAsonDate <> Nothing Then
                                dRow(dtRow("leavename").ToString() & " " & Language.getMessage(mstrModuleName, 5, "Days Balance")) = CDec(dRow(dtRow("leavename").ToString() & " " & Language.getMessage(mstrModuleName, 1, "Days Accrued"))) - CDec(dRow(dtRow("leavename").ToString() & " " & Language.getMessage(mstrModuleName, 3, "Days Consumed")))
                                dRow(dtRow("leavename").ToString() & " " & Language.getMessage(mstrModuleName, 6, "Value Balance")) = dRow(dtRow("leavename").ToString() & " " & Language.getMessage(mstrModuleName, 2, "Value Accrued")) - dRow(dtRow("leavename").ToString() & " " & Language.getMessage(mstrModuleName, 4, "Value Consumed"))
                            Else
                                If mblnIncludeLeaveBF Then
                                    dRow(dtRow("leavename").ToString() & " " & Language.getMessage(mstrModuleName, 5, "Days Balance")) = CDec(dRow(dtRow("leavename").ToString() & " " & Language.getMessage(mstrModuleName, 1, "Days Accrued")) + dRow(dtRow("leavename").ToString() & " " & Language.getMessage(mstrModuleName, 30, "BF Days"))) - CDec(dRow(dtRow("leavename").ToString() & " " & Language.getMessage(mstrModuleName, 3, "Days Consumed")))
                                    dRow(dtRow("leavename").ToString() & " " & Language.getMessage(mstrModuleName, 6, "Value Balance")) = dRow(dtRow("leavename").ToString() & " " & Language.getMessage(mstrModuleName, 2, "Value Accrued")) + dRow(dtRow("leavename").ToString() & " " & Language.getMessage(mstrModuleName, 31, "BF Value")) - dRow(dtRow("leavename").ToString() & " " & Language.getMessage(mstrModuleName, 4, "Value Consumed"))
                                Else
                            dRow(dtRow("leavename").ToString() & " " & Language.getMessage(mstrModuleName, 5, "Days Balance")) = CDec(dRow(dtRow("leavename").ToString() & " " & Language.getMessage(mstrModuleName, 1, "Days Accrued"))) - CDec(dRow(dtRow("leavename").ToString() & " " & Language.getMessage(mstrModuleName, 3, "Days Consumed")))
                            dRow(dtRow("leavename").ToString() & " " & Language.getMessage(mstrModuleName, 6, "Value Balance")) = dRow(dtRow("leavename").ToString() & " " & Language.getMessage(mstrModuleName, 2, "Value Accrued")) - dRow(dtRow("leavename").ToString() & " " & Language.getMessage(mstrModuleName, 4, "Value Consumed"))
                                End If
                            End If
                            'Pinkal (28-Jun-2021) -- End

                            'Pinkal (03-May-2019) -- Start
                            'Enhancement - Working on Leave UAT Changes for NMB.
                            If mdtAsonDate <> Nothing Then

                                'Pinkal (26-Aug-2019) -- Start
                                'Enhancement NMB - Working on Leave Liability Report to include Leave BF as per NMB Requirement.
                                If mblnIncludeLeaveBF Then
                                    dRow("Total Days") = IIf(IsDBNull(dRow("Total Days")), 0, dRow("Total Days")) + ((Math.Round(CDec(dsBalance.Tables(0).Rows(0)("accrue_amount")), 2) + Math.Round(CDec(dsBalance.Tables(0).Rows(0)("LeaveBF")), 2)) - Math.Round(mdecLeaveIssueAsOnDate, 2))
                                Else
                                    dRow("Total Days") = IIf(IsDBNull(dRow("Total Days")), 0, dRow("Total Days")) + (Math.Round(CDec(dsBalance.Tables(0).Rows(0)("accrue_amount")), 2) - Math.Round(mdecLeaveIssueAsOnDate, 2))
                                End If
                                'Pinkal (26-Aug-2019) -- End
                            Else
                                'Pinkal (26-Aug-2019) -- Start
                                'Enhancement NMB - Working on Leave Liability Report to include Leave BF as per NMB Requirement.
                                If mblnIncludeLeaveBF Then
                                    'Pinkal (28-Jun-2021)-- Start
                                    'Leave Liability Report Bug : Solved Leave Liability report bug.
                                    'dRow("Total Days") = IIf(IsDBNull(dRow("Total Days")), 0, dRow("Total Days")) + ((Math.Round(CDec(dsBalance.Tables(0).Rows(0)("accrue_amount")), 2) + Math.Round(CDec(dsBalance.Tables(0).Rows(0)("LeaveBF")), 2)) - Math.Round(CDec(dsBalance.Tables(0).Rows(0)("issue_amount")), 2))
                                    dRow("Total Days") = IIf(IsDBNull(dRow("Total Days")), 0, dRow("Total Days")) + ((Math.Round(CDec(dsBalance.Tables(0).Rows(0)("accrue_amount")), 2)) - Math.Round(CDec(dsBalance.Tables(0).Rows(0)("issue_amount")), 2))
                                    'Pinkal (28-Jun-2021)-- Start
                                Else
                                    dRow("Total Days") = IIf(IsDBNull(dRow("Total Days")), 0, dRow("Total Days")) + (Math.Round(CDec(dsBalance.Tables(0).Rows(0)("accrue_amount")), 2) - Math.Round(CDec(dsBalance.Tables(0).Rows(0)("issue_amount")), 2))
                                End If
                                'Pinkal (26-Aug-2019) -- End
                            End If
                            'Pinkal (03-May-2019) -- End


                            'Pinkal (26-Aug-2019) -- Start
                            'Enhancement NMB - Working on Leave Liability Report to include Leave BF as per NMB Requirement.
                            If mblnIncludeLeaveBF Then
                                dRow("Total Value") = IIf(IsDBNull(dRow("Total Value")), 0, dRow("Total Value")) + (CDec((dRow(dtRow("leavename").ToString() & " " & Language.getMessage(mstrModuleName, 2, "Value Accrued")) + dRow(dtRow("leavename").ToString() & " " & Language.getMessage(mstrModuleName, 31, "BF Value"))) - dRow(dtRow("leavename").ToString() & " " & Language.getMessage(mstrModuleName, 4, "Value Consumed"))))
                            Else
                                dRow("Total Value") = IIf(IsDBNull(dRow("Total Value")), 0, dRow("Total Value")) + (CDec(dRow(dtRow("leavename").ToString() & " " & Language.getMessage(mstrModuleName, 2, "Value Accrued")) - dRow(dtRow("leavename").ToString() & " " & Language.getMessage(mstrModuleName, 4, "Value Consumed"))))
                            End If
                            'Pinkal (26-Aug-2019) -- End

                        End If

                    Next

                Next
                mdtTableExcel.AcceptChanges()
            End If



            If mintViewIndex > 0 Then
                If mdtTableExcel.Columns.Contains("Id") Then
                    mdtTableExcel.Columns.Remove("Id")
                End If
                mdtTableExcel.Columns("GName").Caption = mstrReport_GroupName
                Dim strGrpCols As String() = {"GName"}
                strarrGroupColumns = strGrpCols
            End If

            If mdtTableExcel.Columns.Contains("employeeunkid") Then
                mdtTableExcel.Columns.Remove("employeeunkid")
            End If

            If mdtTableExcel.Columns.Contains("dayscale") Then
                mdtTableExcel.Columns.Remove("dayscale")
            End If

            If mintViewIndex <= 0 AndAlso mdtTableExcel.Columns.Contains("GName") Then
                mdtTableExcel.Columns.Remove("GName")
            End If

            mdtTableExcel.Columns("employeecode").Caption = Language.getMessage(mstrModuleName, 11, "Employee ID")
            mdtTableExcel.Columns("employee").Caption = Language.getMessage(mstrModuleName, 12, "Staff Name")
            mdtTableExcel.Columns("Department").Caption = Language.getMessage(mstrModuleName, 13, "Sub Department")
            mdtTableExcel.Columns("appointeddate").Caption = Language.getMessage(mstrModuleName, 14, "Hire Date")


            'Pinkal (30-Jun-2021)-- Start
            'Netiz / Scania Enhancements : Working on Leave Liability Report Enhancements.
            If mintOtherEarningTranId > 0 Then
                mdtTableExcel.Columns("scale").Caption = mstrOtherEarningTranHead
            Else
            mdtTableExcel.Columns("scale").Caption = Language.getMessage(mstrModuleName, 15, "Basic Salary")
            End If
            'Pinkal (30-Jun-2021) -- End


            mdtTableExcel.Columns("Total Days").SetOrdinal(mdtTableExcel.Columns.Count - 1)
            mdtTableExcel.Columns("Total Value").SetOrdinal(mdtTableExcel.Columns.Count - 1)
            mdtTableExcel.Columns("Total Days").Caption = Language.getMessage(mstrModuleName, 16, "Total Days")
            mdtTableExcel.Columns("Total Value").Caption = Language.getMessage(mstrModuleName, 17, "Total Balance")



            row = New WorksheetRow()
            'Pinkal (03-May-2019) -- Start
            'Enhancement - Working on Leave UAT Changes for NMB.
            'wcell = New WorksheetCell(Language.getMessage(mstrModuleName, 27, "Yealy Leave Liability Days:") & " " & mintDays, "s10bw")
            wcell = New WorksheetCell(Language.getMessage(mstrModuleName, 28, "Yearly Leave Liability Days:") & " " & mintDays, "s10bw")
            'Pinkal (03-May-2019) -- End
            wcell.MergeAcross = mdtTableExcel.Columns.Count - 1
            row.Cells.Add(wcell)
            rowsArrayHeader.Add(row)

            row = New WorksheetRow()
            If Me._FilterTitle.ToString.Length > 0 Then
                wcell = New WorksheetCell(Me._FilterTitle.ToString, "s10bw")
                wcell.MergeAcross = mdtTableExcel.Columns.Count - 1
                row.Cells.Add(wcell)
            End If
            rowsArrayHeader.Add(row)


            row = New WorksheetRow()
            Dim intCount As Integer = 0
            Do While intCount <= mdtTableExcel.Columns.Count - 1
                If dcHeader.ContainsKey(intCount) Then
                    wcell = New WorksheetCell(dcHeader.Item(intCount), "HeaderStyle")
                    wcell.MergeAcross = 1
                    intCount += 1
                ElseIf intCount = mdtTableExcel.Columns.Count - 2 Then
                    wcell = New WorksheetCell(Language.getMessage(mstrModuleName, 26, "Outstanding"), "HeaderStyle")
                    wcell.MergeAcross = 1
                    intCount += 1
                Else
                    wcell = New WorksheetCell("", "HeaderStyle")
                End If
                row.Cells.Add(wcell)
                intCount += 1
            Loop
            rowsArrayHeader.Add(row)


            row = New WorksheetRow()
            wcell = New WorksheetCell("", "s10bw")
            row.Cells.Add(wcell)
            rowsArrayFooter.Add(row)
            '--------------------

            If ConfigParameter._Object._IsShowPreparedBy = True Then
                row = New WorksheetRow()
                wcell = New WorksheetCell(Language.getMessage(mstrModuleName, 7, "Prepared By :") & Space(10) & User._Object._Username, "s8bw")
                wcell.MergeAcross = 4
                row.Cells.Add(wcell)

                wcell = New WorksheetCell("", "s10bw")
                row.Cells.Add(wcell)
                rowsArrayFooter.Add(row)

                row = New WorksheetRow()
                wcell = New WorksheetCell("", "s10bw")
                row.Cells.Add(wcell)
                rowsArrayFooter.Add(row)
            End If


            If ConfigParameter._Object._IsShowCheckedBy = True Then
                row = New WorksheetRow()
                wcell = New WorksheetCell(Language.getMessage(mstrModuleName, 8, "Checked By :"), "s8bw")
                wcell.MergeAcross = 4
                row.Cells.Add(wcell)

                wcell = New WorksheetCell("", "s10bw")
                row.Cells.Add(wcell)
                rowsArrayFooter.Add(row)

                row = New WorksheetRow()
                wcell = New WorksheetCell("", "s10bw")
                row.Cells.Add(wcell)
                rowsArrayFooter.Add(row)
            End If


            If ConfigParameter._Object._IsShowApprovedBy = True Then
                row = New WorksheetRow()
                wcell = New WorksheetCell(Language.getMessage(mstrModuleName, 9, "Approved By"), "s8bw")
                wcell.MergeAcross = 4
                row.Cells.Add(wcell)

                wcell = New WorksheetCell("", "s10bw")
                row.Cells.Add(wcell)
                rowsArrayFooter.Add(row)

                row = New WorksheetRow()
                wcell = New WorksheetCell("", "s10bw")
                row.Cells.Add(wcell)
                rowsArrayFooter.Add(row)
            End If

            If ConfigParameter._Object._IsShowReceivedBy = True Then
                row = New WorksheetRow()
                wcell = New WorksheetCell(Language.getMessage(mstrModuleName, 10, "Received By :"), "s8bw")
                wcell.MergeAcross = 4
                row.Cells.Add(wcell)
                rowsArrayFooter.Add(row)
            End If

            '--------------------

            'SET EXCEL CELL WIDTH  
            Dim intArrayColumnWidth As Integer() = Nothing
            ReDim intArrayColumnWidth(mdtTableExcel.Columns.Count - 1)
            For i As Integer = 0 To intArrayColumnWidth.Length - 1
                intArrayColumnWidth(i) = 125
            Next
            'SET EXCEL CELL WIDTH


            Call ReportExecute(Nothing, enPrintAction.None, enExportAction.ExcelExtra, mstrExportReportPath, mblnOpenAfterExport, mdtTableExcel, intArrayColumnWidth, True, True, True, strarrGroupColumns, Me._ReportName, "", " ", , "", True, rowsArrayHeader, rowsArrayFooter, Nothing, Nothing, True)

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: Generate_DetailReport; Module Name: " & mstrModuleName)
        End Try
    End Sub

#End Region





    '<Language> This Auto Generated Text Please Do Not Modify it.
#Region " Language & UI Settings "
    Public Shared Sub SetMessages()
        Try
            Language.setMessage(mstrModuleName, 1, "Days Accrued")
            Language.setMessage(mstrModuleName, 2, "Value Accrued")
            Language.setMessage(mstrModuleName, 3, "Days Consumed")
            Language.setMessage(mstrModuleName, 4, "Value Consumed")
            Language.setMessage(mstrModuleName, 5, "Days Balance")
            Language.setMessage(mstrModuleName, 6, "Value Balance")
            Language.setMessage(mstrModuleName, 7, "Prepared By :")
            Language.setMessage(mstrModuleName, 8, "Checked By :")
            Language.setMessage(mstrModuleName, 9, "Approved By")
            Language.setMessage(mstrModuleName, 10, "Received By :")
            Language.setMessage(mstrModuleName, 11, "Employee ID")
            Language.setMessage(mstrModuleName, 12, "Staff Name")
            Language.setMessage(mstrModuleName, 13, "Sub Department")
            Language.setMessage(mstrModuleName, 14, "Hire Date")
            Language.setMessage(mstrModuleName, 15, "Basic Salary")
            Language.setMessage(mstrModuleName, 16, "Total Days")
            Language.setMessage(mstrModuleName, 17, "Total Balance")
            Language.setMessage(mstrModuleName, 18, "Staff Name:")
            Language.setMessage(mstrModuleName, 19, "Leave Name:")
            Language.setMessage(mstrModuleName, 20, " Order By :")
            Language.setMessage(mstrModuleName, 21, "Days")
            Language.setMessage(mstrModuleName, 22, "Value")
            Language.setMessage(mstrModuleName, 23, "Accrued")
            Language.setMessage(mstrModuleName, 24, "Consumed")
            Language.setMessage(mstrModuleName, 25, "Balance")
            Language.setMessage(mstrModuleName, 26, "Outstanding")
            Language.setMessage(mstrModuleName, 28, "Yearly Leave Liability Days:")
            Language.setMessage(mstrModuleName, 29, "Leave BF")
            Language.setMessage(mstrModuleName, 30, "BF Days")
            Language.setMessage(mstrModuleName, 31, "BF Value")
			Language.setMessage(mstrModuleName, 32, "Other Earning:")

        Catch Ex As Exception
            Throw New Exception(Ex.Message & "; Procedure Name: SetMessages; Module Name: " & mstrModuleName)
        End Try
    End Sub
#End Region 'Language & UI Settings
    '</Language>


End Class
