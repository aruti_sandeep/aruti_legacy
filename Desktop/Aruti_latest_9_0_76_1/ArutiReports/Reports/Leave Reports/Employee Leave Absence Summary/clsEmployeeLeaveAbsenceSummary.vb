'************************************************************************************************************************************
'Class Name : clsEmployeeLeaveAbsenceSummaryReport.vb
'Purpose    :
'Date       :10/9/2010
'Written By :Vimal M. Gohil
'Modified   :
'************************************************************************************************************************************

Imports eZeeCommonLib
Imports Aruti.Data

''' <summary>
''' Purpose: Report Generation Class 
''' Developer: Vimal M. Gohil
''' </summary>
Public Class clsEmployeeLeaveAbsenceSummary
    Inherits IReportData
    Private Shared ReadOnly mstrModuleName As String = "clsEmployeeLeaveAbsenceSummaryReport"
    Private mstrReportId As String = enArutiReport.EmployeeLeaveAbsenceSummary
    Dim objDataOperation As clsDataOperation

#Region " Constructor "
    Public Sub New(Byval intLangId As Integer,Byval intCompanyId AS Integer) 'S.SANDEEP |24-Sep-2020| -- START --END
        Me.setReportData(CInt(mstrReportId),intLangId,intCompanyId) 'S.SANDEEP |24-Sep-2020| -- START --END
        Call Create_OnDetailReport()
    End Sub
#End Region

#Region " Private Variables "
    Private mintReportId As Integer = 0
    Private mstrReportTypeName As String = ""

    Private mintLeaveTypeId As Integer = 0
    Private mstrLeaveTypeName As String = ""

    Private mdtStartDateFrom As DateTime = Nothing
    Private mdtStartDateTo As DateTime = Nothing

    Private mstrOrderByQuery As String = ""



    'Pinkal (11-MAY-2012) -- Start
    'Enhancement : TRA Changes
    Private mblnIncludeInactiveEmp As Boolean = False
    'Pinkal (11-MAY-2012) -- End

    'Pinkal (28-Oct-2014) -- Start
    'Enhancement -  ENHANCEMENT FOR REQUIRED BY TRA ON ONLINE LEAVE ISSUES.
    Private mstrUserAccessFilter As String = ""
    Dim Rpt As CrystalDecisions.CrystalReports.Engine.ReportClass
    Private mintUserUnkid As Integer = -1
    Private mintCompanyUnkid As Integer = -1
    'Pinkal (28-Oct-2014) -- End

#End Region

#Region " Properties "

    Public WriteOnly Property _ReportId() As Integer
        Set(ByVal value As Integer)
            mintReportId = value
        End Set
    End Property

    Public WriteOnly Property _ReportTypeName() As String
        Set(ByVal value As String)
            mstrReportTypeName = value
        End Set
    End Property

    Public WriteOnly Property _LeaveTypeId() As Integer
        Set(ByVal value As Integer)
            mintLeaveTypeId = value
        End Set
    End Property

    Public WriteOnly Property _LeaveTypeName() As String
        Set(ByVal value As String)
            mstrLeaveTypeName = value
        End Set
    End Property


    Public WriteOnly Property _StartDateFrom() As DateTime
        Set(ByVal value As DateTime)
            mdtStartDateFrom = value
        End Set
    End Property

    Public WriteOnly Property _StartDateTo() As DateTime
        Set(ByVal value As DateTime)
            mdtStartDateTo = value
        End Set
    End Property

    Public WriteOnly Property _OrderByQuery() As String
        Set(ByVal value As String)
            mstrOrderByQuery = value
        End Set
    End Property


    'Pinkal (11-MAY-2012) -- Start
    'Enhancement : TRA Changes

    Public WriteOnly Property _IncludeInactiveEmp() As Boolean
        Set(ByVal value As Boolean)
            mblnIncludeInactiveEmp = value
        End Set
    End Property

    'Pinkal (11-MAY-2012) -- End

    'Pinkal (28-Oct-2014) -- Start
    'Enhancement -  ENHANCEMENT FOR REQUIRED BY TRA ON ONLINE LEAVE ISSUES.

    Public ReadOnly Property _Rpt() As CrystalDecisions.CrystalReports.Engine.ReportClass
        Get
            Return Rpt
        End Get
    End Property

    Public WriteOnly Property _CompanyUnkId() As Integer
        Set(ByVal value As Integer)
            mintCompanyUnkid = value
        End Set
    End Property

    Public WriteOnly Property _UserUnkId() As Integer
        Set(ByVal value As Integer)
            mintUserUnkid = value
        End Set
    End Property

    Public WriteOnly Property _UserAccessFilter() As String
        Set(ByVal value As String)
            mstrUserAccessFilter = value
        End Set
    End Property

    'Pinkal (28-Oct-2014) -- End


#End Region

#Region "Public Function & Procedures "

    Public Sub SetDefaultValue()
        Try
            mintReportId = 0
            mstrReportTypeName = ""

            mintLeaveTypeId = 0
            mstrLeaveTypeName = ""

            mdtStartDateFrom = Nothing
            mdtStartDateTo = Nothing

            mstrOrderByQuery = ""



            'Pinkal (11-MAY-2012) -- Start
            'Enhancement : TRA Changes
            mblnIncludeInactiveEmp = False
            'Pinkal (11-MAY-2012) -- End


        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: SetDefaultValue; Module Name: " & mstrModuleName)
        End Try
    End Sub

    Public Sub FilterTitleAndFilterQuery()
        Me._FilterQuery = ""
        Me._FilterTitle = ""
        Try

            If Not (mdtStartDateFrom = Nothing AndAlso mdtStartDateTo = Nothing) Then
                objDataOperation.AddParameter("@DateFrom", SqlDbType.NVarChar, eZeeDataType.DATETIME_SIZE, eZeeDate.convertDate(mdtStartDateFrom))
                objDataOperation.AddParameter("@DateTo", SqlDbType.NVarChar, eZeeDataType.DATETIME_SIZE, eZeeDate.convertDate(mdtStartDateTo))
                Me._FilterTitle &= Language.getMessage(mstrModuleName, 12, " Date From : ") & " " & mdtStartDateFrom.ToShortDateString & " " & _
                                   Language.getMessage(mstrModuleName, 13, " To : ") & " " & mdtStartDateTo.ToShortDateString & " "
            End If

            If mintLeaveTypeId > 0 Then
                objDataOperation.AddParameter("@LeaveTypeId", SqlDbType.Int, eZeeDataType.INT_SIZE, mintLeaveTypeId)
                Me._FilterQuery &= " AND  LeaveId = @LeaveTypeId "
                Me._FilterTitle &= Language.getMessage(mstrModuleName, 14, " Leave Type : ") & " " & mstrLeaveTypeName & " "
            End If


            'Pinkal (11-MAY-2012) -- Start
            'Enhancement : TRA Changes
            If mblnIncludeInactiveEmp = False Then
                objDataOperation.AddParameter("@startdate", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, ConfigParameter._Object._EmployeeAsOnDate)
                objDataOperation.AddParameter("@enddate", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, ConfigParameter._Object._EmployeeAsOnDate)
            End If
            'Pinkal (11-MAY-2012) -- End


            If Me.OrderByQuery <> "" Then
                Me._FilterQuery &= " ORDER BY " & Me.OrderByQuery
                Me._FilterTitle &= Language.getMessage(mstrModuleName, 15, " Order By : ") & " " & Me.OrderByDisplay
            End If

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: FilterTitleAndFilterQuery; Module Name: " & mstrModuleName)
        End Try
    End Sub

    Public Overrides Sub generateReport(ByVal pintReportType As Integer, Optional ByVal PrintAction As Aruti.Data.enPrintAction = Aruti.Data.enPrintAction.Preview, Optional ByVal ExportAction As Aruti.Data.enExportAction = Aruti.Data.enExportAction.None)
        'Dim objRpt As CrystalDecisions.CrystalReports.Engine.ReportClass = Nothing
        'Dim strReportExportFile As String = ""
        'Try


        '    'Pinkal (28-Oct-2014) -- Start
        '    'Enhancement -  ENHANCEMENT FOR REQUIRED BY TRA ON ONLINE LEAVE ISSUES.

        '    If mintCompanyUnkid <= 0 Then
        '        mintCompanyUnkid = Company._Object._Companyunkid
        '    End If
        '    Company._Object._Companyunkid = mintCompanyUnkid
        '    ConfigParameter._Object._Companyunkid = mintCompanyUnkid


        '    If mintUserUnkid <= 0 Then
        '        mintUserUnkid = User._Object._Userunkid
        '    End If

        '    User._Object._Userunkid = mintUserUnkid

        '    objRpt = Generate_DetailReport()

        '    Rpt = objRpt

        '    'Pinkal (28-Oct-2014) -- End

        '    If Not IsNothing(objRpt) Then
        '        Call ReportExecute(objRpt, PrintAction, ExportAction, ConfigParameter._Object._ExportReportPath, ConfigParameter._Object._OpenAfterExport)
        '    End If
        'Catch ex As Exception
        '    Throw New Exception(ex.Message & "; Procedure Name: generateReport; Module Name: " & mstrModuleName)
        'End Try
    End Sub

    Public Overrides Sub generateReportNew(ByVal xDatabaseName As String, ByVal xUserUnkid As Integer, ByVal xYearUnkid As Integer, ByVal xCompanyUnkid As Integer _
                                                              , ByVal xPeriodStart As Date, ByVal xPeriodEnd As Date, ByVal xUserModeSetting As String, ByVal xOnlyApproved As Boolean _
                                                              , ByVal xExportReportPath As String, ByVal xOpenReportAfterExport As Boolean, ByVal pintReportType As Integer _
                                                              , Optional ByVal PrintAction As Aruti.Data.enPrintAction = Aruti.Data.enPrintAction.Preview _
                                                              , Optional ByVal ExportAction As Aruti.Data.enExportAction = Aruti.Data.enExportAction.None, Optional ByVal intBaseCurrencyUnkid As Integer = 0)

        Dim objRpt As CrystalDecisions.CrystalReports.Engine.ReportClass = Nothing
        Dim strReportExportFile As String = ""
        Try

            If mintCompanyUnkid <= 0 Then
                mintCompanyUnkid = Company._Object._Companyunkid
            End If
            Company._Object._Companyunkid = mintCompanyUnkid
            ConfigParameter._Object._Companyunkid = mintCompanyUnkid

            If mintUserUnkid <= 0 Then
                mintUserUnkid = User._Object._Userunkid
            End If

            User._Object._Userunkid = mintUserUnkid

            objRpt = Generate_DetailReport(xDatabaseName, xUserUnkid, xYearUnkid, xCompanyUnkid, eZeeDate.convertDate(xPeriodEnd), xUserModeSetting, xOnlyApproved)

            Rpt = objRpt

            If Not IsNothing(objRpt) Then
                Call ReportExecute(objRpt, PrintAction, ExportAction, xExportReportPath, xOpenReportAfterExport)
            End If

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: generateReportNew; Module Name: " & mstrModuleName)
        End Try

    End Sub

    Public Overrides Sub setDefaultOrderBy(ByVal intReportType As Integer)
        OrderByDisplay = ""
        OrderByQuery = ""
        Try
            OrderByDisplay = iColumn_DetailReport.ColumnItem(0).DisplayName
            OrderByQuery = iColumn_DetailReport.ColumnItem(0).Name
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: setDefaultOrderBy; Module Name: " & mstrModuleName)
        End Try
    End Sub

    Public Overrides Sub setOrderBy(ByVal intReportType As Integer)
        Try
            Call OrderByExecute(iColumn_DetailReport)
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: setOrderBy; Module Name: " & mstrModuleName)
        End Try
    End Sub

#End Region

#Region " Report Generation "
    Dim iColumn_DetailReport As New IColumnCollection

    Public Property Field_OnDetailReport() As IColumnCollection
        Get
            Return iColumn_DetailReport
        End Get
        Set(ByVal value As IColumnCollection)
            iColumn_DetailReport = value
        End Set
    End Property

    Private Sub Create_OnDetailReport()
        Try
            iColumn_DetailReport.Clear()
            iColumn_DetailReport.Add(New IColumn("DeptName", Language.getMessage(mstrModuleName, 1, "Department")))
            iColumn_DetailReport.Add(New IColumn("LeaveCode", Language.getMessage(mstrModuleName, 2, "Leave Code")))
            iColumn_DetailReport.Add(New IColumn("LeaveName", Language.getMessage(mstrModuleName, 3, "Leave Type")))
            iColumn_DetailReport.Add(New IColumn("Employees", Language.getMessage(mstrModuleName, 4, "Employees")))
            iColumn_DetailReport.Add(New IColumn("Days", Language.getMessage(mstrModuleName, 5, "Days")))
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: Create_OnDetailReport; Module Name: " & mstrModuleName)
        End Try
    End Sub


    'Pinkal (24-Aug-2015) -- Start
    'Enhancement - WORKING ON ACTIVE EMPLOYEE CONDITION.

    '    Private Function Generate_DetailReport() As CrystalDecisions.CrystalReports.Engine.ReportClass
    '        Dim objRpt As CrystalDecisions.CrystalReports.Engine.ReportClass = Nothing
    '        Dim StrQ As String = ""
    '        Dim dsList As New DataSet
    '        Dim rpt_Data As ArutiReport.Designer.dsArutiReport

    '        Try
    '            objDataOperation = New clsDataOperation

    '            StrQ = "SELECT DeptName " & _
    '                       ",LeaveCode " & _
    '                       ",LeaveName " & _
    '                       ",LeaveId " & _
    '                       ",Employees " & _
    '                       ",Days " & _
    '"FROM    ( SELECT  DeptName " & _
    '               ",LeaveCode " & _
    '               ",LeaveName " & _
    '               ",LeaveId " & _
    '               ",COUNT(DISTINCT EmpId) AS Employees " & _
    '               ",COUNT(LeaveDate) AS Days " & _
    '       "FROM    ( SELECT    hremployee_master.employeeunkid AS EmpId " & _
    '                           ",hrdepartment_master.name AS DeptName " & _
    '                           ",CONVERT(CHAR(8) ,lvleaveIssue_tran.leavedate ,112) AS LeaveDate " & _
    '                           ",lvleaveIssue_master.leavetypeunkid AS LeaveId " & _
    '                           ",lvleavetype_master.leavetypecode AS LeaveCode " & _
    '                           ",lvleavetype_master.leavename AS LeaveName " & _
    '                   "FROM      lvleaveIssue_master " & _
    '                            "JOIN lvleaveIssue_tran ON lvleaveIssue_master.leaveissueunkid = lvleaveIssue_tran.leaveissueunkid " & _
    '                            "JOIN lvleavetype_master ON lvleavetype_master.leavetypeunkid = lvleaveIssue_master.leavetypeunkid " & _
    '                            "JOIN hremployee_master ON lvleaveIssue_master.employeeunkid = hremployee_master.employeeunkid " & _
    '                            "LEFT JOIN hrdepartment_master ON hremployee_master.departmentunkid = hrdepartment_master.departmentunkid " & _
    '                  "WHERE     CONVERT(CHAR(8) ,lvleaveIssue_tran.leavedate ,112) BETWEEN @DateFrom AND @DateTo " & _
    '                            "AND lvleaveIssue_master.isvoid = 0 " & _
    '                                        "AND lvleaveIssue_tran.isvoid = 0 "



    '            Pinkal (11-MAY-2012) -- Start
    'Enhancement: TRA(Changes)
    '            If mblnIncludeInactiveEmp = False Then
    '                StrQ &= " AND CONVERT(CHAR(8),appointeddate,112) <= @enddate " & _
    '                    " AND ISNULL(CONVERT(CHAR(8),termination_from_date,112),@startdate) >= @startdate " & _
    '                    " AND ISNULL(CONVERT(CHAR(8),termination_to_date,112),@startdate) >= @startdate " & _
    '                    " AND ISNULL(CONVERT(CHAR(8), empl_enddate,112), @startdate) >= @startdate "
    '            End If
    '            Pinkal (11-MAY-2012) -- End


    '            Pinkal (28-Oct-2014) -- Start
    '            Enhancement -  ENHANCEMENT FOR REQUIRED BY TRA ON ONLINE LEAVE ISSUES.

    '            If UserAccessLevel._AccessLevel.Length > 0 Then
    '                StrQ &= UserAccessLevel._AccessLevelFilterString
    '            End If

    '            If mstrUserAccessFilter.Trim = "" Then
    '                mstrUserAccessFilter = UserAccessLevel._AccessLevelFilterString
    '            End If

    '            If mstrUserAccessFilter.Trim.Length > 0 Then
    '                mstrUserAccessFilter &= mstrUserAccessFilter.Trim
    '            End If

    '            Pinkal (28-Oct-2014) -- End


    '            StrQ &= ") AS List " & _
    '        "GROUP BY DeptName " & _
    '               ",LeaveCode " & _
    '               ",LeaveName " & _
    '               ",LeaveId " & _
    '"        ) AS ELA  " & _
    '"WHERE   1 = 1  "


    '            Vimal (30 Nov 2010) -- End




    '            Call FilterTitleAndFilterQuery()

    '            StrQ &= Me._FilterQuery

    '            StrQ &= mstrOrderByQuery

    '            dsList = objDataOperation.ExecQuery(StrQ, "DataTable")

    '            If objDataOperation.ErrorMessage <> "" Then
    '                Throw New Exception(objDataOperation.ErrorNumber & ":" & objDataOperation.ErrorMessage)
    '            End If

    '            rpt_Data = New ArutiReport.Designer.dsArutiReport

    '            For Each dtRow As DataRow In dsList.Tables("DataTable").Rows
    '                Dim rpt_Rows As DataRow
    '                rpt_Rows = rpt_Data.Tables("ArutiTable").NewRow

    '                rpt_Rows.Item("Column1") = dtRow.Item("DeptName")
    '                rpt_Rows.Item("Column2") = dtRow.Item("LeaveCode")
    '                rpt_Rows.Item("Column3") = dtRow.Item("LeaveName")
    '                rpt_Rows.Item("Column4") = Format(CDec(dtRow.Item("Employees")))
    '                rpt_Rows.Item("Column5") = Format(CDec(dtRow.Item("Days")))

    '                rpt_Data.Tables("ArutiTable").Rows.Add(rpt_Rows)
    '            Next

    '            objRpt = New ArutiReport.Designer.rptEmployeeLeaveAbsenceSummary

    '            Sandeep [ 10 FEB 2011 ] -- Start
    '            ConfigParameter._Object.GetReportSettings(CInt(mstrReportId))
    '            Dim arrImageRow As DataRow = Nothing
    '            arrImageRow = rpt_Data.Tables("ArutiImage").NewRow()

    '            ReportFunction.Logo_Display(objRpt, _
    '                                        ConfigParameter._Object._IsDisplayLogo, _
    '                                        ConfigParameter._Object._ShowLogoRightSide, _
    '                                        "arutiLogo1", _
    '                                        "arutiLogo2", _
    '                                        arrImageRow, _
    '                                        "txtCompanyName", _
    '                                        "txtReportName", _
    '                                        "txtFilterDescription", _
    '                                        ConfigParameter._Object._GetLeftMargin, _
    '                                        ConfigParameter._Object._GetRightMargin)

    '            rpt_Data.Tables("ArutiImage").Rows.Add(arrImageRow)

    '            If rpt_Data.Tables("ArutiTable").Rows.Count <= 0 Then
    '                rpt_Data.Tables("ArutiTable").Rows.Add("")
    '            End If

    '            If ConfigParameter._Object._IsShowPreparedBy = True Then
    '                Call ReportFunction.TextChange(objRpt, "lblPreparedBy", Language.getMessage(mstrModuleName, 17, "Prepared By :"))
    '                Call ReportFunction.TextChange(objRpt, "txtUPreparedBy", User._Object._Username)
    '            Else
    '                Call ReportFunction.EnableSuppressSection(objRpt, "ReportFooterSection1", True)
    '            End If

    '            If ConfigParameter._Object._IsShowCheckedBy = True Then
    '                Call ReportFunction.TextChange(objRpt, "lblCheckedBy", Language.getMessage(mstrModuleName, 18, "Checked By :"))
    '            Else
    '                Call ReportFunction.EnableSuppressSection(objRpt, "ReportFooterSection2", True)
    '            End If

    '            If ConfigParameter._Object._IsShowApprovedBy = True Then
    '                Call ReportFunction.TextChange(objRpt, "lblApprovedBy", Language.getMessage(mstrModuleName, 19, "Approved By :"))
    '            Else
    '                Call ReportFunction.EnableSuppressSection(objRpt, "ReportFooterSection3", True)
    '            End If

    '            If ConfigParameter._Object._IsShowReceivedBy = True Then
    '                Call ReportFunction.TextChange(objRpt, "lblReceivedBy", Language.getMessage(mstrModuleName, 20, "Received By :"))
    '            Else
    '                Call ReportFunction.EnableSuppressSection(objRpt, "ReportFooterSection4", True)
    '            End If
    '            Sandeep [ 10 FEB 2011 ] -- End 

    '            objRpt.SetDataSource(rpt_Data)
    '            objRpt.Subreports("rptEmployeeLeaveAbsenceSub").SetDataSource(rpt_Data)


    '            Call ReportFunction.TextChange(objRpt, "txtDeptName", Language.getMessage(mstrModuleName, 16, "Department :"))
    '            Call ReportFunction.TextChange(objRpt, "txtLeaveCode", Language.getMessage(mstrModuleName, 2, "Leave Code"))
    '            Call ReportFunction.TextChange(objRpt, "txtLeaveType", Language.getMessage(mstrModuleName, 3, "Leave Type"))
    '            Call ReportFunction.TextChange(objRpt, "txtEmployees", Language.getMessage(mstrModuleName, 4, "Employees"))
    '            Call ReportFunction.TextChange(objRpt, "txtDays", Language.getMessage(mstrModuleName, 5, "Days"))

    '            Call ReportFunction.TextChange(objRpt, "txtGroupTotal", Language.getMessage(mstrModuleName, 10, "Group Total :"))

    '            Call ReportFunction.TextChange(objRpt.Subreports("rptEmployeeLeaveAbsenceSub"), "txtSummary", Language.getMessage(mstrModuleName, 9, "Summary"))
    '            Call ReportFunction.TextChange(objRpt.Subreports("rptEmployeeLeaveAbsenceSub"), "txtLeaveCode", Language.getMessage(mstrModuleName, 2, "Leave Code"))
    '            Call ReportFunction.TextChange(objRpt.Subreports("rptEmployeeLeaveAbsenceSub"), "txtLeaveType", Language.getMessage(mstrModuleName, 3, "Leave Type"))
    '            Call ReportFunction.TextChange(objRpt.Subreports("rptEmployeeLeaveAbsenceSub"), "txtEmployees", Language.getMessage(mstrModuleName, 4, "Employees"))
    '            Call ReportFunction.TextChange(objRpt.Subreports("rptEmployeeLeaveAbsenceSub"), "txtDays", Language.getMessage(mstrModuleName, 5, "Days"))

    '            Call ReportFunction.TextChange(objRpt.Subreports("rptEmployeeLeaveAbsenceSub"), "txtGrandTotal", Language.getMessage(mstrModuleName, 11, "Grand Total :"))



    '            Call ReportFunction.TextChange(objRpt, "lblPrintedBy", Language.getMessage(mstrModuleName, 6, "Printed By :"))
    '            Call ReportFunction.TextChange(objRpt, "lblPrintedDate", Language.getMessage(mstrModuleName, 7, "Printed Date :"))


    '            Pinkal (11-MAY-2012) -- Start
    'Enhancement: TRA(Changes)
    '            Call ReportFunction.TextChange(objRpt, "lblPageNumber", Language.getMessage(mstrModuleName, 8, "Page :"))
    '            Pinkal (11-MAY-2012) -- End



    '            Call ReportFunction.TextChange(objRpt, "txtPrintedDate", Me._PrintDate)
    '            Call ReportFunction.TextChange(objRpt, "txtPrintedBy", Me._UserName)

    '            Call ReportFunction.TextChange(objRpt, "txtReportName", Me._ReportName)
    '            Call ReportFunction.TextChange(objRpt, "txtCompanyName", Me._CompanyName)
    '            Call ReportFunction.TextChange(objRpt, "txtFilterDescription", Me._FilterTitle)

    '            Return objRpt
    '        Catch ex As Exception
    '            Throw New Exception(ex.Message & "; Procedure Name: Generate_DetailReport; Module Name: " & mstrModuleName)
    '            Return Nothing
    '        End Try
    '    End Function

    Private Function Generate_DetailReport(ByVal xDatabaseName As String, ByVal xUserUnkid As Integer, ByVal xYearUnkid As Integer, ByVal xCompanyUnkid As Integer _
                                                              , ByVal strEmployeeAsOnDate As String, ByVal xUserModeSetting As String, ByVal xOnlyApproved As Boolean) As CrystalDecisions.CrystalReports.Engine.ReportClass
        Dim objRpt As CrystalDecisions.CrystalReports.Engine.ReportClass = Nothing
        Dim StrQ As String = ""
        Dim dsList As New DataSet
        Dim rpt_Data As ArutiReport.Designer.dsArutiReport

        Try
            objDataOperation = New clsDataOperation


            Dim xDateJoinQry, xDateFilterQry, xUACQry, xUACFiltrQry, xAdvanceJoinQry As String
            xDateJoinQry = "" : xDateFilterQry = "" : xUACQry = "" : xUACFiltrQry = "" : xAdvanceJoinQry = ""
            Call GetDatesFilterString(xDateJoinQry, xDateFilterQry, eZeeDate.convertDate(strEmployeeAsOnDate), eZeeDate.convertDate(strEmployeeAsOnDate), , , xDatabaseName)
            Call NewAccessLevelFilterString(xUACQry, xUACFiltrQry, eZeeDate.convertDate(strEmployeeAsOnDate), xOnlyApproved, xDatabaseName, xUserUnkid, xCompanyUnkid, xYearUnkid, xUserModeSetting)


            StrQ = "SELECT DeptName " & _
                         ",LeaveCode " & _
                         ",LeaveName " & _
                         ",LeaveId " & _
                         ",Employees " & _
                         ",Days " & _
            "FROM    ( SELECT  DeptName " & _
                           ",LeaveCode " & _
                           ",LeaveName " & _
                           ",LeaveId " & _
                           ",COUNT(DISTINCT EmpId) AS Employees " & _
                           ",COUNT(LeaveDate) AS Days " & _
                   "FROM    ( SELECT    hremployee_master.employeeunkid AS EmpId " & _
                                       ",hrdepartment_master.name AS DeptName " & _
                                       ",CONVERT(CHAR(8) ,lvleaveIssue_tran.leavedate ,112) AS LeaveDate " & _
                                       ",lvleaveIssue_master.leavetypeunkid AS LeaveId " & _
                                       ",lvleavetype_master.leavetypecode AS LeaveCode " & _
                                       ",lvleavetype_master.leavename AS LeaveName " & _
                               "FROM      lvleaveIssue_master " & _
                                        "JOIN lvleaveIssue_tran ON lvleaveIssue_master.leaveissueunkid = lvleaveIssue_tran.leaveissueunkid " & _
                                        "JOIN lvleavetype_master ON lvleavetype_master.leavetypeunkid = lvleaveIssue_master.leavetypeunkid " & _
                                        "JOIN hremployee_master ON lvleaveIssue_master.employeeunkid = hremployee_master.employeeunkid " & _
                                                       " LEFT JOIN " & _
                                                        " ( " & _
                                                        "    SELECT " & _
                                                        "         departmentunkid " & _
                                                        "        ,employeeunkid " & _
                                                        "        ,ROW_NUMBER()OVER(PARTITION BY employeeunkid ORDER BY effectivedate DESC) AS rno " & _
                                                        "    FROM hremployee_transfer_tran " & _
                                                        "    WHERE isvoid = 0 AND CONVERT(CHAR(8),effectivedate,112) <= '" & strEmployeeAsOnDate & "' " & _
                                                        " ) AS Alloc ON Alloc.employeeunkid = hremployee_master.employeeunkid AND Alloc.rno = 1 " & _
                                                        " LEFT JOIN hrdepartment_master ON hrdepartment_master.departmentunkid = Alloc.departmentunkid "


            If xDateJoinQry.Trim.Length > 0 Then
                StrQ &= xDateJoinQry
            End If

            If xUACQry.Trim.Length > 0 Then
                StrQ &= xUACQry
            End If



            StrQ &= "                                WHERE  CONVERT(CHAR(8) ,lvleaveIssue_tran.leavedate ,112) BETWEEN @DateFrom AND @DateTo " & _
                                                       " AND lvleaveIssue_master.isvoid = 0 " & _
                                                       " AND lvleaveIssue_tran.isvoid = 0 "


            If xUACFiltrQry.Trim.Length > 0 Then
                StrQ &= " AND " & xUACFiltrQry
            End If

            If mblnIncludeInactiveEmp = False Then
                If xDateFilterQry.Trim.Length > 0 Then
                    StrQ &= xDateFilterQry
                End If
            End If

            'If mblnIncludeInactiveEmp = False Then
            '    StrQ &= " AND CONVERT(CHAR(8),appointeddate,112) <= @enddate " & _
            '        " AND ISNULL(CONVERT(CHAR(8),termination_from_date,112),@startdate) >= @startdate " & _
            '        " AND ISNULL(CONVERT(CHAR(8),termination_to_date,112),@startdate) >= @startdate " & _
            '        " AND ISNULL(CONVERT(CHAR(8), empl_enddate,112), @startdate) >= @startdate "
            'End If

            'If mstrUserAccessFilter.Trim = "" Then
            '    mstrUserAccessFilter = UserAccessLevel._AccessLevelFilterString
            'End If

            'If mstrUserAccessFilter.Trim.Length > 0 Then
            '    mstrUserAccessFilter &= mstrUserAccessFilter.Trim
            'End If

            StrQ &= ") AS List " & _
                    "GROUP BY DeptName " & _
                           ",LeaveCode " & _
                           ",LeaveName " & _
                           ",LeaveId " & _
            "        ) AS ELA  " & _
            "WHERE   1 = 1  "



            Call FilterTitleAndFilterQuery()

            StrQ &= Me._FilterQuery


            StrQ &= mstrOrderByQuery

            dsList = objDataOperation.ExecQuery(StrQ, "DataTable")

            If objDataOperation.ErrorMessage <> "" Then
                Throw New Exception(objDataOperation.ErrorNumber & ":" & objDataOperation.ErrorMessage)
            End If

            rpt_Data = New ArutiReport.Designer.dsArutiReport


            For Each dtRow As DataRow In dsList.Tables("DataTable").Rows
                Dim rpt_Rows As DataRow
                rpt_Rows = rpt_Data.Tables("ArutiTable").NewRow

                rpt_Rows.Item("Column1") = dtRow.Item("DeptName")
                rpt_Rows.Item("Column2") = dtRow.Item("LeaveCode")
                rpt_Rows.Item("Column3") = dtRow.Item("LeaveName")
                rpt_Rows.Item("Column4") = Format(CDec(dtRow.Item("Employees")))
                rpt_Rows.Item("Column5") = Format(CDec(dtRow.Item("Days")))

                rpt_Data.Tables("ArutiTable").Rows.Add(rpt_Rows)
            Next

            objRpt = New ArutiReport.Designer.rptEmployeeLeaveAbsenceSummary

            'Sandeep [ 10 FEB 2011 ] -- Start
            ConfigParameter._Object.GetReportSettings(CInt(mstrReportId))
            Dim arrImageRow As DataRow = Nothing
            arrImageRow = rpt_Data.Tables("ArutiImage").NewRow()

            ReportFunction.Logo_Display(objRpt, _
                                        ConfigParameter._Object._IsDisplayLogo, _
                                        ConfigParameter._Object._ShowLogoRightSide, _
                                        "arutiLogo1", _
                                        "arutiLogo2", _
                                        arrImageRow, _
                                        "txtCompanyName", _
                                        "txtReportName", _
                                        "txtFilterDescription", _
                                        ConfigParameter._Object._GetLeftMargin, _
                                        ConfigParameter._Object._GetRightMargin)

            rpt_Data.Tables("ArutiImage").Rows.Add(arrImageRow)

            If rpt_Data.Tables("ArutiTable").Rows.Count <= 0 Then
                rpt_Data.Tables("ArutiTable").Rows.Add("")
            End If

            If ConfigParameter._Object._IsShowPreparedBy = True Then
                Call ReportFunction.TextChange(objRpt, "lblPreparedBy", Language.getMessage(mstrModuleName, 17, "Prepared By :"))
                Call ReportFunction.TextChange(objRpt, "txtUPreparedBy", User._Object._Username)
            Else
                Call ReportFunction.EnableSuppressSection(objRpt, "ReportFooterSection1", True)
            End If

            If ConfigParameter._Object._IsShowCheckedBy = True Then
                Call ReportFunction.TextChange(objRpt, "lblCheckedBy", Language.getMessage(mstrModuleName, 18, "Checked By :"))
            Else
                Call ReportFunction.EnableSuppressSection(objRpt, "ReportFooterSection2", True)
            End If

            If ConfigParameter._Object._IsShowApprovedBy = True Then
                Call ReportFunction.TextChange(objRpt, "lblApprovedBy", Language.getMessage(mstrModuleName, 19, "Approved By :"))
            Else
                Call ReportFunction.EnableSuppressSection(objRpt, "ReportFooterSection3", True)
            End If

            If ConfigParameter._Object._IsShowReceivedBy = True Then
                Call ReportFunction.TextChange(objRpt, "lblReceivedBy", Language.getMessage(mstrModuleName, 20, "Received By :"))
            Else
                Call ReportFunction.EnableSuppressSection(objRpt, "ReportFooterSection4", True)
            End If
            'Sandeep [ 10 FEB 2011 ] -- End 

            objRpt.SetDataSource(rpt_Data)
            objRpt.Subreports("rptEmployeeLeaveAbsenceSub").SetDataSource(rpt_Data)


            Call ReportFunction.TextChange(objRpt, "txtDeptName", Language.getMessage(mstrModuleName, 16, "Department :"))
            Call ReportFunction.TextChange(objRpt, "txtLeaveCode", Language.getMessage(mstrModuleName, 2, "Leave Code"))
            Call ReportFunction.TextChange(objRpt, "txtLeaveType", Language.getMessage(mstrModuleName, 3, "Leave Type"))
            Call ReportFunction.TextChange(objRpt, "txtEmployees", Language.getMessage(mstrModuleName, 4, "Employees"))
            Call ReportFunction.TextChange(objRpt, "txtDays", Language.getMessage(mstrModuleName, 5, "Days"))

            Call ReportFunction.TextChange(objRpt, "txtGroupTotal", Language.getMessage(mstrModuleName, 10, "Group Total :"))

            Call ReportFunction.TextChange(objRpt.Subreports("rptEmployeeLeaveAbsenceSub"), "txtSummary", Language.getMessage(mstrModuleName, 9, "Summary"))
            Call ReportFunction.TextChange(objRpt.Subreports("rptEmployeeLeaveAbsenceSub"), "txtLeaveCode", Language.getMessage(mstrModuleName, 2, "Leave Code"))
            Call ReportFunction.TextChange(objRpt.Subreports("rptEmployeeLeaveAbsenceSub"), "txtLeaveType", Language.getMessage(mstrModuleName, 3, "Leave Type"))
            Call ReportFunction.TextChange(objRpt.Subreports("rptEmployeeLeaveAbsenceSub"), "txtEmployees", Language.getMessage(mstrModuleName, 4, "Employees"))
            Call ReportFunction.TextChange(objRpt.Subreports("rptEmployeeLeaveAbsenceSub"), "txtDays", Language.getMessage(mstrModuleName, 5, "Days"))

            Call ReportFunction.TextChange(objRpt.Subreports("rptEmployeeLeaveAbsenceSub"), "txtGrandTotal", Language.getMessage(mstrModuleName, 11, "Grand Total :"))



            Call ReportFunction.TextChange(objRpt, "lblPrintedBy", Language.getMessage(mstrModuleName, 6, "Printed By :"))
            Call ReportFunction.TextChange(objRpt, "lblPrintedDate", Language.getMessage(mstrModuleName, 7, "Printed Date :"))


            'Pinkal (11-MAY-2012) -- Start
            'Enhancement : TRA Changes
            'Call ReportFunction.TextChange(objRpt, "lblPageNumber", Language.getMessage(mstrModuleName, 8, "Page :"))
            'Pinkal (11-MAY-2012) -- End



            Call ReportFunction.TextChange(objRpt, "txtPrintedDate", Me._PrintDate)
            Call ReportFunction.TextChange(objRpt, "txtPrintedBy", Me._UserName)

            Call ReportFunction.TextChange(objRpt, "txtReportName", Me._ReportName)
            Call ReportFunction.TextChange(objRpt, "txtCompanyName", Me._CompanyName)
            Call ReportFunction.TextChange(objRpt, "txtFilterDescription", Me._FilterTitle)

            Return objRpt
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: Generate_DetailReport; Module Name: " & mstrModuleName)
            Return Nothing
        End Try
    End Function


    'Pinkal (24-Aug-2015) -- End

#End Region


    '<Language> This Auto Generated Text Please Do Not Modify it.
#Region " Language & UI Settings "
    Public Shared Sub SetMessages()
        Try
            Language.setMessage(mstrModuleName, 1, "Department")
            Language.setMessage(mstrModuleName, 2, "Leave Code")
            Language.setMessage(mstrModuleName, 3, "Leave Type")
            Language.setMessage(mstrModuleName, 4, "Employees")
            Language.setMessage(mstrModuleName, 5, "Days")
            Language.setMessage(mstrModuleName, 6, "Printed By :")
            Language.setMessage(mstrModuleName, 7, "Printed Date :")
            Language.setMessage(mstrModuleName, 9, "Summary")
            Language.setMessage(mstrModuleName, 10, "Group Total :")
            Language.setMessage(mstrModuleName, 11, "Grand Total :")
            Language.setMessage(mstrModuleName, 12, " Date From :")
            Language.setMessage(mstrModuleName, 13, " To :")
            Language.setMessage(mstrModuleName, 14, " Leave Type :")
            Language.setMessage(mstrModuleName, 15, " Order By :")
            Language.setMessage(mstrModuleName, 16, "Department :")
            Language.setMessage(mstrModuleName, 17, "Prepared By :")
            Language.setMessage(mstrModuleName, 18, "Checked By :")
            Language.setMessage(mstrModuleName, 19, "Approved By :")
            Language.setMessage(mstrModuleName, 20, "Received By :")

        Catch Ex As Exception
            Throw New Exception(Ex.Message & "; Procedure Name: SetMessages; Module Name: " & mstrModuleName)
        End Try
    End Sub
#End Region 'Language & UI Settings
    '</Language>


   
  
End Class
