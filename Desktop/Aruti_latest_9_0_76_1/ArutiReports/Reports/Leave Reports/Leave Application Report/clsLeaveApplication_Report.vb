'************************************************************************************************************************************
'Class Name : clsLeaveApplication_Report.vb
'Purpose    :
'Date       :  24-Jun-2019
'Written By :Pinkal
'Modified   :
'************************************************************************************************************************************

Imports eZeeCommonLib
Imports Aruti.Data
Imports ExcelWriter

''' <summary>
''' Purpose: Report Generation Class 
''' Developer: Pinkal
''' </summary>
''' 
Public Class clsLeaveApplication_Report
    Inherits IReportData
    Private Shared ReadOnly mstrModuleName As String = "clsLeaveApplication_Report"
    Private mstrReportId As String = enArutiReport.Leave_Application_Report
    Dim objDataOperation As clsDataOperation

#Region " Constructor "
    Public Sub New(Byval intLangId As Integer,Byval intCompanyId AS Integer) 'S.SANDEEP |24-Sep-2020| -- START --END
        Me.setReportData(CInt(mstrReportId),intLangId,intCompanyId) 'S.SANDEEP |24-Sep-2020| -- START --END
        Call Create_OnDetailReport()
    End Sub
#End Region

#Region " Private variables "

    Private mdtApplicationFrom As Date = Nothing
    Private mdtApplicationTo As Date = Nothing
    Private mdtStartDate As Date = Nothing
    Private mdtEndDate As Date = Nothing
    Private mintEmployeeID As Integer = 0
    Private mstrEmployeeName As String = ""
    Private mintLeaveId As Integer = 0
    Private mstrLeaveName As String = ""
    Private mintStatusId As Integer = 0
    Private mstrStatus As String = ""
    Private mstrOrderByQuery As String = ""
    Private mstrAdvance_Filter As String = String.Empty
    Private mintViewIndex As Integer = -1
    Private mstrViewByIds As String = ""
    Private mstrViewByName As String = ""
    Private mstrAnalysis_Fields As String = ""
    Private mstrAnalysis_Join As String = ""
    Private mstrAnalysis_OrderBy As String = ""
    Private mstrAnalysis_OrderBy_GName As String = ""
    Private mstrReport_GroupName As String = ""
    Private mstrExportReportPath As String = ConfigParameter._Object._ExportReportPath
    Private mblnOpenAfterExport As Boolean = ConfigParameter._Object._OpenAfterExport

#End Region

#Region " Properties "

    Public WriteOnly Property _ApplicationFrom() As Date
        Set(ByVal value As Date)
            mdtApplicationFrom = value
        End Set
    End Property

    Public WriteOnly Property _ApplicationTo() As Date
        Set(ByVal value As Date)
            mdtApplicationTo = value
        End Set
    End Property

    Public WriteOnly Property _StartDate() As Date
        Set(ByVal value As Date)
            mdtStartDate = value
        End Set
    End Property

    Public WriteOnly Property _EndDate() As Date
        Set(ByVal value As Date)
            mdtEndDate = value
        End Set
    End Property

    Public WriteOnly Property _EmployeeId() As Integer
        Set(ByVal value As Integer)
            mintEmployeeID = value
        End Set
    End Property

    Public WriteOnly Property _Employee() As String
        Set(ByVal value As String)
            mstrEmployeeName = value
        End Set
    End Property

    Public WriteOnly Property _LeaveId() As Integer
        Set(ByVal value As Integer)
            mintLeaveId = value
        End Set
    End Property

    Public WriteOnly Property _LeaveName() As String
        Set(ByVal value As String)
            mstrLeaveName = value
        End Set
    End Property

    Public WriteOnly Property _statusId() As Integer
        Set(ByVal value As Integer)
            mintStatusId = value
        End Set
    End Property

    Public WriteOnly Property _Status() As String
        Set(ByVal value As String)
            mstrStatus = value
        End Set
    End Property

    Public WriteOnly Property _Advance_Filter() As String
        Set(ByVal value As String)
            mstrAdvance_Filter = value
        End Set
    End Property

    Public WriteOnly Property _ViewIndex() As Integer
        Set(ByVal value As Integer)
            mintViewIndex = value
        End Set
    End Property

    Public WriteOnly Property _ViewByIds() As String
        Set(ByVal value As String)
            mstrViewByIds = value
        End Set
    End Property

    Public WriteOnly Property _ViewByName() As String
        Set(ByVal value As String)
            mstrViewByName = value
        End Set
    End Property

    Public WriteOnly Property _Analysis_Fields() As String
        Set(ByVal value As String)
            mstrAnalysis_Fields = value
        End Set
    End Property

    Public WriteOnly Property _Analysis_Join() As String
        Set(ByVal value As String)
            mstrAnalysis_Join = value
        End Set
    End Property

    Public WriteOnly Property _Analysis_OrderBy() As String
        Set(ByVal value As String)
            mstrAnalysis_OrderBy = value
        End Set
    End Property

    Public WriteOnly Property _Analysis_OrderBy_GName() As String
        Set(ByVal value As String)
            mstrAnalysis_OrderBy_GName = value
        End Set
    End Property

    Public WriteOnly Property _Report_GroupName() As String
        Set(ByVal value As String)
            mstrReport_GroupName = value
        End Set
    End Property

    Public WriteOnly Property _ExportReportPath() As String
        Set(ByVal value As String)
            mstrExportReportPath = value
        End Set
    End Property

    Public WriteOnly Property _OpenAfterExport() As Boolean
        Set(ByVal value As Boolean)
            mblnOpenAfterExport = value
        End Set
    End Property


#End Region

#Region "Public Function & Procedures "

    Public Sub SetDefaultValue()
        Try
            mdtApplicationFrom = Nothing
            mdtApplicationTo = Nothing
            mdtStartDate = Nothing
            mdtEndDate = Nothing
            mintEmployeeID = 0
            mstrEmployeeName = ""
            mintLeaveId = 0
            mstrLeaveName = ""
            mintStatusId = 0
            mstrStatus = ""
            mstrOrderByQuery = ""
            mstrAdvance_Filter = ""
            mintViewIndex = -1
            mstrViewByIds = ""
            mstrViewByName = ""
            mstrAnalysis_Fields = ""
            mstrAnalysis_Join = ""
            mstrAnalysis_OrderBy = ""
            mstrAnalysis_OrderBy_GName = ""
            mstrReport_GroupName = ""
            mstrOrderByQuery = ""

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: SetDefaultValue; Module Name: " & mstrModuleName)
        End Try
    End Sub

    Public Sub FilterTitleAndFilterQuery()
        Me._FilterQuery = ""
        Me._FilterTitle = ""

        Try

            If mdtApplicationFrom <> Nothing Then
                Me._FilterQuery &= " AND CONVERT(CHAR(8),lvleaveform.applydate,112) >= @ApplicationFrom "
                objDataOperation.AddParameter("@ApplicationFrom", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, eZeeDate.convertDate(mdtApplicationFrom.Date))
                Me._FilterTitle &= Language.getMessage(mstrModuleName, 6, "Application From Date: ") & " " & mdtApplicationFrom.Date & " "
            End If

            If mdtApplicationTo <> Nothing Then
                Me._FilterQuery &= " AND CONVERT(CHAR(8),lvleaveform.applydate,112) <= @ApplicationTo "
                objDataOperation.AddParameter("@ApplicationTo", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, eZeeDate.convertDate(mdtApplicationTo.Date))
                Me._FilterTitle &= Language.getMessage(mstrModuleName, 7, "Application To Date: ") & " " & mdtApplicationTo.Date & " "
            End If

            If mdtStartDate <> Nothing Then
                Me._FilterQuery &= " AND CONVERT(CHAR(8),lvleaveform.startdate,112) >= @FromDate "
                objDataOperation.AddParameter("@FromDate", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, eZeeDate.convertDate(mdtStartDate.Date))
                Me._FilterTitle &= Language.getMessage(mstrModuleName, 8, "Start Date: ") & " " & mdtStartDate.Date & " "
            End If

            If mdtEndDate <> Nothing Then
                Me._FilterQuery &= " AND CONVERT(CHAR(8),lvleaveform.returndate,112) <= @ToDate "
                objDataOperation.AddParameter("@ToDate", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, eZeeDate.convertDate(mdtEndDate.Date))
                Me._FilterTitle &= Language.getMessage(mstrModuleName, 9, "End Date: ") & " " & mdtEndDate.Date & " "
            End If

            If mintEmployeeID > 0 Then
                Me._FilterQuery &= " AND lvleaveform.employeeunkid = @employeeunkid "
                objDataOperation.AddParameter("@employeeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintEmployeeID)
                Me._FilterTitle &= Language.getMessage(mstrModuleName, 10, "Employee: ") & " " & mstrEmployeeName & " "
            End If

            If mintLeaveId > 0 Then
                Me._FilterQuery &= " AND lvleaveform.leavetypeunkid = @leavetypeunkid "
                objDataOperation.AddParameter("@leavetypeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintLeaveId)
                Me._FilterTitle &= Language.getMessage(mstrModuleName, 11, "Leave: ") & " " & mstrLeaveName & " "
            End If

            If mintStatusId > 0 Then
                Me._FilterQuery &= " AND lvleaveform.statusunkid = @statusunkid "
                objDataOperation.AddParameter("@statusunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintStatusId)
                Me._FilterTitle &= Language.getMessage(mstrModuleName, 12, "Status: ") & " " & mstrStatus & " "
            End If

            If Me.OrderByQuery <> "" Then
                If mintViewIndex > 0 Then
                    mstrOrderByQuery &= "  ORDER BY GName," & Me.OrderByQuery
                Else
                    mstrOrderByQuery &= "  ORDER BY " & Me.OrderByQuery
                End If

                Me._FilterTitle &= Language.getMessage(mstrModuleName, 13, " Order By : ") & " " & Me.OrderByDisplay
            End If

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: FilterTitleAndFilterQuery; Module Name: " & mstrModuleName)
        End Try
    End Sub

    Public Overrides Sub generateReport(ByVal pintReportType As Integer, Optional ByVal PrintAction As Aruti.Data.enPrintAction = Aruti.Data.enPrintAction.Preview, Optional ByVal ExportAction As Aruti.Data.enExportAction = Aruti.Data.enExportAction.None)

    End Sub

    Public Overrides Sub generateReportNew(ByVal xDatabaseName As String, ByVal xUserUnkid As Integer, ByVal xYearUnkid As Integer _
                                                              , ByVal xCompanyUnkid As Integer, ByVal xPeriodStart As Date, ByVal xPeriodEnd As Date _
                                                              , ByVal xUserModeSetting As String, ByVal xOnlyApproved As Boolean, ByVal xExportReportPath As String _
                                                              , ByVal xOpenReportAfterExport As Boolean, ByVal pintReportType As Integer _
                                                              , Optional ByVal PrintAction As Aruti.Data.enPrintAction = Aruti.Data.enPrintAction.Preview _
                                                              , Optional ByVal ExportAction As Aruti.Data.enExportAction = Aruti.Data.enExportAction.None, Optional ByVal intBaseCurrencyUnkid As Integer = 0)
        Dim objRpt As CrystalDecisions.CrystalReports.Engine.ReportClass = Nothing
        Dim strReportExportFile As String = ""
        Try
            If Not IsNothing(objRpt) Then
                Call ReportExecute(objRpt, PrintAction, ExportAction, " ", False)
            End If
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: generateReport; Module Name: " & mstrModuleName)
        End Try
    End Sub


    Public Overrides Sub setDefaultOrderBy(ByVal intReportType As Integer)
        OrderByDisplay = ""
        OrderByQuery = ""
        Try
            OrderByDisplay = iColumn_DetailReport.ColumnItem(0).DisplayName
            OrderByQuery = iColumn_DetailReport.ColumnItem(0).Name
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: setDefaultOrderBy; Module Name: " & mstrModuleName)
        End Try
    End Sub

    Public Overrides Sub setOrderBy(ByVal intReportType As Integer)
        Try
            Call OrderByExecute(iColumn_DetailReport)
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: setOrderBy; Module Name: " & mstrModuleName)
        End Try
    End Sub

#End Region

#Region " Report Generation "

    Dim iColumn_DetailReport As New IColumnCollection

    Public Property Field_OnDetailReport() As IColumnCollection
        Get
            Return iColumn_DetailReport
        End Get
        Set(ByVal value As IColumnCollection)
            iColumn_DetailReport = value
        End Set
    End Property

    Private Sub Create_OnDetailReport()
        Try
            iColumn_DetailReport.Clear()
            iColumn_DetailReport.Add(New IColumn("hremployee_master.employeecode", Language.getMessage(mstrModuleName, 1, "Employee Code")))
            iColumn_DetailReport.Add(New IColumn(" ISNULL(hremployee_master.firstname, '') + ' ' + ISNULL(hremployee_master.othername, '') + ' ' + ISNULL(hremployee_master.surname, '')", Language.getMessage(mstrModuleName, 2, "Employee")))
            iColumn_DetailReport.Add(New IColumn("CONVERT(char(8),lvleaveform.applydate,112)", Language.getMessage(mstrModuleName, 3, "Application Date")))
            iColumn_DetailReport.Add(New IColumn("CONVERT(char(8),lvleaveform.startdate,112)", Language.getMessage(mstrModuleName, 4, "Start Date")))
            iColumn_DetailReport.Add(New IColumn("CONVERT(char(8),lvleaveform.returndate,112)", Language.getMessage(mstrModuleName, 5, "End Date")))

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: Create_OnDetailReport; Module Name: " & mstrModuleName)
        End Try
    End Sub

    Public Sub Generate_DetailReport(ByVal xDatabaseName As String, ByVal xUserUnkid As Integer, ByVal xYearUnkid As Integer, ByVal xCompanyUnkid As Integer _
                                                   , ByVal strEmployeeAsOnDate As String, ByVal xUserModeSetting As String, ByVal xOnlyApproved As Boolean)
        Dim StrQ As String = ""
        Dim StrQFilter As String = ""
        Dim dsList As New DataSet
        Try
            If xCompanyUnkid <= 0 Then
                xCompanyUnkid = Company._Object._Companyunkid
            End If

            Company._Object._Companyunkid = xCompanyUnkid
            ConfigParameter._Object._Companyunkid = xCompanyUnkid

            If xUserUnkid <= 0 Then
                xUserUnkid = User._Object._Userunkid
            End If

            User._Object._Userunkid = xUserUnkid

            objDataOperation = New clsDataOperation

            Dim xDateJoinQry, xDateFilterQry, xUACQry, xUACFiltrQry, xAdvanceJoinQry As String
            xDateJoinQry = "" : xDateFilterQry = "" : xUACQry = "" : xUACFiltrQry = "" : xAdvanceJoinQry = ""
            Call GetDatesFilterString(xDateJoinQry, xDateFilterQry, eZeeDate.convertDate(strEmployeeAsOnDate), eZeeDate.convertDate(strEmployeeAsOnDate), , , xDatabaseName)
            Call NewAccessLevelFilterString(xUACQry, xUACFiltrQry, eZeeDate.convertDate(strEmployeeAsOnDate), xOnlyApproved, xDatabaseName, xUserUnkid, xCompanyUnkid, xYearUnkid, xUserModeSetting)
            Call GetAdvanceFilterQry(xAdvanceJoinQry, eZeeDate.convertDate(strEmployeeAsOnDate), xDatabaseName)

            objDataOperation.ClearParameters()

            StrQ = "SELECT hremployee_master.employeeunkid " & _
                      ", hremployee_master.employeecode " & _
                      ", ISNULL(hremployee_master.firstname, '') + ' ' + ISNULL(hremployee_master.othername, '') + ' ' + ISNULL(hremployee_master.surname, '') AS Name " & _
                      ", CONVERT(CHAR(8),lvleaveform.applydate,112)  as applydate " & _
                      ", CONVERT(CHAR(8),lvleaveform.startdate,112) as startdate " & _
                      ", CONVERT(CHAR(8),lvleaveform.returndate,112) as returndate " & _
                      ", lvleavetype_master.leavename AS Leave " & _
                      ", CASE WHEN lvleaveform.statusunkid = 1 then @Approve " & _
                      "           WHEN lvleaveform.statusunkid = 2 then @Pending  " & _
                      "           WHEN lvleaveform.statusunkid = 3 then @Reject " & _
                      "           WHEN lvleaveform.statusunkid = 4 then @ReSchedule " & _
                      "           WHEN lvleaveform.statusunkid = 7 then @Issued end AS Status "


            If mintViewIndex > 0 Then
                StrQ &= mstrAnalysis_Fields
            Else
                StrQ &= ", '' AS GName "
            End If

            StrQ &= " FROM lvleaveform " & _
                         " LEFT JOIN hremployee_master ON hremployee_master.employeeunkid = lvleaveform.employeeunkid " & _
                         " LEFT JOIN lvleavetype_master ON lvleavetype_master.leavetypeunkid = lvleaveform.leavetypeunkid "


            'Pinkal (11-Sep-2020) -- Start
            'Optimzation Leave NMB:  Working on Optimzing Various modules for Garbage Colletion Issue.
            If xAdvanceJoinQry.Trim.Length > 0 Then
                StrQ &= xAdvanceJoinQry
            End If
            'Pinkal (11-Sep-2020) -- End

            StrQ &= mstrAnalysis_Join

            If xDateJoinQry.Trim.Length > 0 Then
                StrQ &= xDateJoinQry
            End If

            If xUACQry.Trim.Length > 0 Then
                StrQ &= xUACQry
            End If


            StrQ &= "	WHERE lvleaveform.isvoid = 0  "

            If mstrAdvance_Filter.Trim.Length > 0 Then
                StrQ &= " AND " & mstrAdvance_Filter
            End If

            If xUACFiltrQry.Trim.Length > 0 Then
                StrQ &= " AND " & xUACFiltrQry
            End If

            If xDateFilterQry.Trim.Length > 0 Then
                StrQ &= xDateFilterQry
            End If


            Call FilterTitleAndFilterQuery()

            StrQ &= Me._FilterQuery

            StrQ &= mstrOrderByQuery

            objDataOperation.AddParameter("@Approve", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage("clsMasterData", 110, "Approved"))
            objDataOperation.AddParameter("@Pending", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage("clsMasterData", 111, "Pending"))
            objDataOperation.AddParameter("@Reject", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage("clsMasterData", 112, "Rejected"))
            objDataOperation.AddParameter("@ReSchedule", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage("clsMasterData", 113, "Re-Scheduled"))
            objDataOperation.AddParameter("@Cancel", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage("clsMasterData", 115, "Cancelled"))
            objDataOperation.AddParameter("@Issued", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage("clsMasterData", 276, "Issued"))

            dsList = objDataOperation.ExecQuery(StrQ, "DataTable")

            If objDataOperation.ErrorMessage <> "" Then
                Throw New Exception(objDataOperation.ErrorNumber & ":" & objDataOperation.ErrorMessage)
            End If


            For Each dFRow As DataRow In dsList.Tables(0).Rows
                If IsDBNull(dFRow("applydate")) = False AndAlso dFRow("applydate") <> Nothing Then
                    dFRow("applydate") = eZeeDate.convertDate(dFRow("applydate").ToString()).ToShortDateString()
                End If
                If IsDBNull(dFRow("startdate")) = False AndAlso dFRow("startdate") <> Nothing Then
                    dFRow("startdate") = eZeeDate.convertDate(dFRow("startdate").ToString()).ToShortDateString()
                End If
                If IsDBNull(dFRow("returndate")) = False AndAlso dFRow("returndate") <> Nothing Then
                    dFRow("returndate") = eZeeDate.convertDate(dFRow("returndate").ToString()).ToShortDateString()
                End If
            Next

            Dim strarrGroupColumns As String() = Nothing
            Dim rowsArrayHeader As New ArrayList
            Dim rowsArrayFooter As New ArrayList
            Dim row As WorksheetRow
            Dim wcell As WorksheetCell

            ConfigParameter._Object.GetReportSettings(CInt(mstrReportId))

            Dim mdtTableExcel As DataTable = dsList.Tables(0)

            If mintViewIndex > 0 Then
                If mdtTableExcel.Columns.Contains("Id") Then
                    mdtTableExcel.Columns.Remove("Id")
                End If
                mdtTableExcel.Columns("GName").Caption = mstrReport_GroupName
                Dim strGrpCols As String() = {"GName"}
                strarrGroupColumns = strGrpCols
            End If

            If mdtTableExcel.Columns.Contains("employeeunkid") Then
                mdtTableExcel.Columns.Remove("employeeunkid")
            End If

            If mintViewIndex <= 0 AndAlso mdtTableExcel.Columns.Contains("GName") Then
                mdtTableExcel.Columns.Remove("GName")
            End If

            mdtTableExcel.Columns("employeecode").Caption = Language.getMessage(mstrModuleName, 18, "Emp Code")
            mdtTableExcel.Columns("Name").Caption = Language.getMessage(mstrModuleName, 19, "Name")
            mdtTableExcel.Columns("applydate").Caption = Language.getMessage(mstrModuleName, 20, "Application Date")
            mdtTableExcel.Columns("startdate").Caption = Language.getMessage(mstrModuleName, 21, "Start Date")
            mdtTableExcel.Columns("returndate").Caption = Language.getMessage(mstrModuleName, 22, "End Date")
            mdtTableExcel.Columns("status").Caption = Language.getMessage(mstrModuleName, 23, "Status")


            row = New WorksheetRow()
            If Me._FilterTitle.ToString.Length > 0 Then
                wcell = New WorksheetCell(Me._FilterTitle.ToString, "s10bw")
                wcell.MergeAcross = mdtTableExcel.Columns.Count - 1
                row.Cells.Add(wcell)
            End If
            rowsArrayHeader.Add(row)


            row = New WorksheetRow()
            wcell = New WorksheetCell("", "s10bw")
            row.Cells.Add(wcell)
            rowsArrayFooter.Add(row)
            '--------------------

            If ConfigParameter._Object._IsShowPreparedBy = True Then
                row = New WorksheetRow()
                wcell = New WorksheetCell(Language.getMessage(mstrModuleName, 14, "Prepared By :") & Space(10) & User._Object._Username, "s8bw")
                wcell.MergeAcross = 4
                row.Cells.Add(wcell)

                wcell = New WorksheetCell("", "s10bw")
                row.Cells.Add(wcell)
                rowsArrayFooter.Add(row)

                row = New WorksheetRow()
                wcell = New WorksheetCell("", "s10bw")
                row.Cells.Add(wcell)
                rowsArrayFooter.Add(row)
            End If


            If ConfigParameter._Object._IsShowCheckedBy = True Then
                row = New WorksheetRow()
                wcell = New WorksheetCell(Language.getMessage(mstrModuleName, 15, "Checked By :"), "s8bw")
                wcell.MergeAcross = 4
                row.Cells.Add(wcell)

                wcell = New WorksheetCell("", "s10bw")
                row.Cells.Add(wcell)
                rowsArrayFooter.Add(row)

                row = New WorksheetRow()
                wcell = New WorksheetCell("", "s10bw")
                row.Cells.Add(wcell)
                rowsArrayFooter.Add(row)
            End If


            If ConfigParameter._Object._IsShowApprovedBy = True Then
                row = New WorksheetRow()
                wcell = New WorksheetCell(Language.getMessage(mstrModuleName, 16, "Approved By"), "s8bw")
                wcell.MergeAcross = 4
                row.Cells.Add(wcell)

                wcell = New WorksheetCell("", "s10bw")
                row.Cells.Add(wcell)
                rowsArrayFooter.Add(row)

                row = New WorksheetRow()
                wcell = New WorksheetCell("", "s10bw")
                row.Cells.Add(wcell)
                rowsArrayFooter.Add(row)
            End If

            If ConfigParameter._Object._IsShowReceivedBy = True Then
                row = New WorksheetRow()
                wcell = New WorksheetCell(Language.getMessage(mstrModuleName, 17, "Received By :"), "s8bw")
                wcell.MergeAcross = 4
                row.Cells.Add(wcell)
                rowsArrayFooter.Add(row)
            End If

            '--------------------

            'SET EXCEL CELL WIDTH  
            Dim intArrayColumnWidth As Integer() = Nothing
            ReDim intArrayColumnWidth(mdtTableExcel.Columns.Count - 1)
            For i As Integer = 0 To intArrayColumnWidth.Length - 1
                If i = 0 Then
                    intArrayColumnWidth(i) = 100
                ElseIf i = 1 Then
                    intArrayColumnWidth(i) = 150
                Else
                    intArrayColumnWidth(i) = 125
                End If
            Next
            'SET EXCEL CELL WIDTH


            Call ReportExecute(Nothing, enPrintAction.None, enExportAction.ExcelExtra, mstrExportReportPath, mblnOpenAfterExport, mdtTableExcel, intArrayColumnWidth, True, True, True, strarrGroupColumns, Me._ReportName, "", " ", , "", True, rowsArrayHeader, rowsArrayFooter, Nothing, Nothing, True)

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: Generate_DetailReport; Module Name: " & mstrModuleName)
        End Try
    End Sub

#End Region





    '<Language> This Auto Generated Text Please Do Not Modify it.
#Region " Language & UI Settings "
    Public Shared Sub SetMessages()
        Try
            Language.setMessage(mstrModuleName, 1, "Employee Code")
            Language.setMessage(mstrModuleName, 2, "Employee")
            Language.setMessage(mstrModuleName, 3, "Application Date")
            Language.setMessage(mstrModuleName, 4, "Start Date")
            Language.setMessage(mstrModuleName, 5, "End Date")
            Language.setMessage(mstrModuleName, 6, "Application From Date:")
            Language.setMessage(mstrModuleName, 7, "Application To Date:")
            Language.setMessage(mstrModuleName, 8, "Start Date:")
            Language.setMessage(mstrModuleName, 9, "End Date:")
            Language.setMessage(mstrModuleName, 10, "Employee:")
            Language.setMessage(mstrModuleName, 11, "Leave:")
            Language.setMessage(mstrModuleName, 12, "Status:")
            Language.setMessage(mstrModuleName, 13, " Order By :")
            Language.setMessage(mstrModuleName, 14, "Prepared By :")
            Language.setMessage(mstrModuleName, 15, "Checked By :")
            Language.setMessage(mstrModuleName, 16, "Approved By")
            Language.setMessage(mstrModuleName, 17, "Received By :")
            Language.setMessage(mstrModuleName, 18, "Emp Code")
            Language.setMessage(mstrModuleName, 19, "Name")
            Language.setMessage(mstrModuleName, 20, "Application Date")
            Language.setMessage(mstrModuleName, 21, "Start Date")
            Language.setMessage(mstrModuleName, 22, "End Date")
            Language.setMessage(mstrModuleName, 23, "Status")
            Language.setMessage("clsMasterData", 110, "Approved")
            Language.setMessage("clsMasterData", 111, "Pending")
            Language.setMessage("clsMasterData", 112, "Rejected")
            Language.setMessage("clsMasterData", 113, "Re-Scheduled")
            Language.setMessage("clsMasterData", 115, "Cancelled")
            Language.setMessage("clsMasterData", 276, "Issued")

        Catch Ex As Exception
            Throw New Exception(Ex.Message & "; Procedure Name: SetMessages; Module Name: " & mstrModuleName)
        End Try
    End Sub
#End Region 'Language & UI Settings
	'</Language>
End Class
