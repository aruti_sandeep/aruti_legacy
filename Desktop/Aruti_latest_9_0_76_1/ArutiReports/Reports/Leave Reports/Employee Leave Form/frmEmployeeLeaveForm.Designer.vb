﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmEmployeeLeaveForm
    Inherits Aruti.Data.frmBaseReportForm

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container
        Me.gbFilterCriteria = New eZee.Common.eZeeCollapsibleContainer(Me.components)
        Me.chkDonotShowExpense = New System.Windows.Forms.CheckBox
        Me.chkIncludeDependandList = New System.Windows.Forms.CheckBox
        Me.cboLeaveForm = New System.Windows.Forms.ComboBox
        Me.objbtnSearchLeaveForm = New eZee.Common.eZeeGradientButton
        Me.objbtnSearchLeaveType = New eZee.Common.eZeeGradientButton
        Me.objbtnSearchEmployee = New eZee.Common.eZeeGradientButton
        Me.lblEmployee = New System.Windows.Forms.Label
        Me.lblLeaveForm = New System.Windows.Forms.Label
        Me.cboEmployee = New System.Windows.Forms.ComboBox
        Me.lblLeaveType = New System.Windows.Forms.Label
        Me.cboLeaveType = New System.Windows.Forms.ComboBox
        Me.gbFilterCriteria.SuspendLayout()
        Me.SuspendLayout()
        '
        'NavPanel
        '
        Me.NavPanel.Location = New System.Drawing.Point(0, 356)
        Me.NavPanel.Size = New System.Drawing.Size(542, 55)
        '
        'gbFilterCriteria
        '
        Me.gbFilterCriteria.BorderColor = System.Drawing.Color.Black
        Me.gbFilterCriteria.Checked = False
        Me.gbFilterCriteria.CollapseAllExceptThis = False
        Me.gbFilterCriteria.CollapsedHoverImage = Nothing
        Me.gbFilterCriteria.CollapsedNormalImage = Nothing
        Me.gbFilterCriteria.CollapsedPressedImage = Nothing
        Me.gbFilterCriteria.CollapseOnLoad = False
        Me.gbFilterCriteria.Controls.Add(Me.chkDonotShowExpense)
        Me.gbFilterCriteria.Controls.Add(Me.chkIncludeDependandList)
        Me.gbFilterCriteria.Controls.Add(Me.cboLeaveForm)
        Me.gbFilterCriteria.Controls.Add(Me.objbtnSearchLeaveForm)
        Me.gbFilterCriteria.Controls.Add(Me.objbtnSearchLeaveType)
        Me.gbFilterCriteria.Controls.Add(Me.objbtnSearchEmployee)
        Me.gbFilterCriteria.Controls.Add(Me.lblEmployee)
        Me.gbFilterCriteria.Controls.Add(Me.lblLeaveForm)
        Me.gbFilterCriteria.Controls.Add(Me.cboEmployee)
        Me.gbFilterCriteria.Controls.Add(Me.lblLeaveType)
        Me.gbFilterCriteria.Controls.Add(Me.cboLeaveType)
        Me.gbFilterCriteria.ExpandedHoverImage = Nothing
        Me.gbFilterCriteria.ExpandedNormalImage = Nothing
        Me.gbFilterCriteria.ExpandedPressedImage = Nothing
        Me.gbFilterCriteria.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.gbFilterCriteria.GradientColor = System.Drawing.SystemColors.ButtonFace
        Me.gbFilterCriteria.HeaderHeight = 25
        Me.gbFilterCriteria.HeaderMessage = ""
        Me.gbFilterCriteria.HeaderMessageFont = New System.Drawing.Font("Tahoma", 8.25!)
        Me.gbFilterCriteria.HeaderMessageForeColor = System.Drawing.Color.Black
        Me.gbFilterCriteria.HeightOnCollapse = 0
        Me.gbFilterCriteria.LeftTextSpace = 0
        Me.gbFilterCriteria.Location = New System.Drawing.Point(8, 66)
        Me.gbFilterCriteria.Name = "gbFilterCriteria"
        Me.gbFilterCriteria.OpenHeight = 300
        Me.gbFilterCriteria.PanelBorderStyle = System.Windows.Forms.ButtonBorderStyle.Solid
        Me.gbFilterCriteria.ShowBorder = True
        Me.gbFilterCriteria.ShowCheckBox = False
        Me.gbFilterCriteria.ShowCollapseButton = False
        Me.gbFilterCriteria.ShowDefaultBorderColor = True
        Me.gbFilterCriteria.ShowDownButton = False
        Me.gbFilterCriteria.ShowHeader = True
        Me.gbFilterCriteria.Size = New System.Drawing.Size(373, 164)
        Me.gbFilterCriteria.TabIndex = 1
        Me.gbFilterCriteria.Temp = 0
        Me.gbFilterCriteria.Text = "Filter Criteria"
        Me.gbFilterCriteria.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'chkDonotShowExpense
        '
        Me.chkDonotShowExpense.Checked = True
        Me.chkDonotShowExpense.CheckState = System.Windows.Forms.CheckState.Checked
        Me.chkDonotShowExpense.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.chkDonotShowExpense.Location = New System.Drawing.Point(89, 139)
        Me.chkDonotShowExpense.Name = "chkDonotShowExpense"
        Me.chkDonotShowExpense.Size = New System.Drawing.Size(279, 17)
        Me.chkDonotShowExpense.TabIndex = 214
        Me.chkDonotShowExpense.Text = "Do not Show Expenses Section if no Expense is Set"
        Me.chkDonotShowExpense.UseVisualStyleBackColor = True
        '
        'chkIncludeDependandList
        '
        Me.chkIncludeDependandList.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.chkIncludeDependandList.Location = New System.Drawing.Point(89, 116)
        Me.chkIncludeDependandList.Name = "chkIncludeDependandList"
        Me.chkIncludeDependandList.Size = New System.Drawing.Size(279, 17)
        Me.chkIncludeDependandList.TabIndex = 212
        Me.chkIncludeDependandList.Text = "Include Dependants List"
        Me.chkIncludeDependandList.UseVisualStyleBackColor = True
        '
        'cboLeaveForm
        '
        Me.cboLeaveForm.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboLeaveForm.DropDownWidth = 300
        Me.cboLeaveForm.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboLeaveForm.FormattingEnabled = True
        Me.cboLeaveForm.Location = New System.Drawing.Point(89, 88)
        Me.cboLeaveForm.Name = "cboLeaveForm"
        Me.cboLeaveForm.Size = New System.Drawing.Size(253, 21)
        Me.cboLeaveForm.TabIndex = 3
        '
        'objbtnSearchLeaveForm
        '
        Me.objbtnSearchLeaveForm.BackColor = System.Drawing.Color.Transparent
        Me.objbtnSearchLeaveForm.BackColor1 = System.Drawing.Color.Transparent
        Me.objbtnSearchLeaveForm.BackColor2 = System.Drawing.Color.Transparent
        Me.objbtnSearchLeaveForm.BorderNormalColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.objbtnSearchLeaveForm.BorderSelected = False
        Me.objbtnSearchLeaveForm.DialogResult = System.Windows.Forms.DialogResult.None
        Me.objbtnSearchLeaveForm.Image = Global.ArutiReports.My.Resources.Resources.Mini_Search
        Me.objbtnSearchLeaveForm.ImageAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.objbtnSearchLeaveForm.Location = New System.Drawing.Point(345, 88)
        Me.objbtnSearchLeaveForm.Name = "objbtnSearchLeaveForm"
        Me.objbtnSearchLeaveForm.Size = New System.Drawing.Size(21, 21)
        Me.objbtnSearchLeaveForm.TabIndex = 210
        '
        'objbtnSearchLeaveType
        '
        Me.objbtnSearchLeaveType.BackColor = System.Drawing.Color.Transparent
        Me.objbtnSearchLeaveType.BackColor1 = System.Drawing.Color.Transparent
        Me.objbtnSearchLeaveType.BackColor2 = System.Drawing.Color.Transparent
        Me.objbtnSearchLeaveType.BorderNormalColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.objbtnSearchLeaveType.BorderSelected = False
        Me.objbtnSearchLeaveType.DialogResult = System.Windows.Forms.DialogResult.None
        Me.objbtnSearchLeaveType.Image = Global.ArutiReports.My.Resources.Resources.Mini_Search
        Me.objbtnSearchLeaveType.ImageAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.objbtnSearchLeaveType.Location = New System.Drawing.Point(345, 33)
        Me.objbtnSearchLeaveType.Name = "objbtnSearchLeaveType"
        Me.objbtnSearchLeaveType.Size = New System.Drawing.Size(21, 21)
        Me.objbtnSearchLeaveType.TabIndex = 208
        '
        'objbtnSearchEmployee
        '
        Me.objbtnSearchEmployee.BackColor = System.Drawing.Color.Transparent
        Me.objbtnSearchEmployee.BackColor1 = System.Drawing.Color.Transparent
        Me.objbtnSearchEmployee.BackColor2 = System.Drawing.Color.Transparent
        Me.objbtnSearchEmployee.BorderNormalColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.objbtnSearchEmployee.BorderSelected = False
        Me.objbtnSearchEmployee.DialogResult = System.Windows.Forms.DialogResult.None
        Me.objbtnSearchEmployee.Image = Global.ArutiReports.My.Resources.Resources.Mini_Search
        Me.objbtnSearchEmployee.ImageAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.objbtnSearchEmployee.Location = New System.Drawing.Point(345, 60)
        Me.objbtnSearchEmployee.Name = "objbtnSearchEmployee"
        Me.objbtnSearchEmployee.Size = New System.Drawing.Size(21, 21)
        Me.objbtnSearchEmployee.TabIndex = 207
        '
        'lblEmployee
        '
        Me.lblEmployee.BackColor = System.Drawing.Color.Transparent
        Me.lblEmployee.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblEmployee.Location = New System.Drawing.Point(8, 64)
        Me.lblEmployee.Name = "lblEmployee"
        Me.lblEmployee.Size = New System.Drawing.Size(77, 13)
        Me.lblEmployee.TabIndex = 174
        Me.lblEmployee.Text = "Employee"
        '
        'lblLeaveForm
        '
        Me.lblLeaveForm.BackColor = System.Drawing.Color.Transparent
        Me.lblLeaveForm.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblLeaveForm.Location = New System.Drawing.Point(8, 92)
        Me.lblLeaveForm.Name = "lblLeaveForm"
        Me.lblLeaveForm.Size = New System.Drawing.Size(77, 13)
        Me.lblLeaveForm.TabIndex = 0
        Me.lblLeaveForm.Text = "Leave Form"
        '
        'cboEmployee
        '
        Me.cboEmployee.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboEmployee.DropDownWidth = 120
        Me.cboEmployee.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboEmployee.FormattingEnabled = True
        Me.cboEmployee.Location = New System.Drawing.Point(89, 60)
        Me.cboEmployee.Name = "cboEmployee"
        Me.cboEmployee.Size = New System.Drawing.Size(253, 21)
        Me.cboEmployee.TabIndex = 2
        '
        'lblLeaveType
        '
        Me.lblLeaveType.BackColor = System.Drawing.Color.Transparent
        Me.lblLeaveType.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblLeaveType.Location = New System.Drawing.Point(8, 37)
        Me.lblLeaveType.Name = "lblLeaveType"
        Me.lblLeaveType.Size = New System.Drawing.Size(77, 13)
        Me.lblLeaveType.TabIndex = 3
        Me.lblLeaveType.Text = "Leave Type"
        '
        'cboLeaveType
        '
        Me.cboLeaveType.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboLeaveType.DropDownWidth = 120
        Me.cboLeaveType.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboLeaveType.FormattingEnabled = True
        Me.cboLeaveType.Location = New System.Drawing.Point(89, 33)
        Me.cboLeaveType.Name = "cboLeaveType"
        Me.cboLeaveType.Size = New System.Drawing.Size(253, 21)
        Me.cboLeaveType.TabIndex = 168
        '
        'frmEmployeeLeaveForm
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(542, 411)
        Me.Controls.Add(Me.gbFilterCriteria)
        Me.Name = "frmEmployeeLeaveForm"
        Me.Text = "frmEmployeeLeaveForm"
        Me.Controls.SetChildIndex(Me.NavPanel, 0)
        Me.Controls.SetChildIndex(Me.gbFilterCriteria, 0)
        Me.gbFilterCriteria.ResumeLayout(False)
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents gbFilterCriteria As eZee.Common.eZeeCollapsibleContainer
    Private WithEvents lblLeaveType As System.Windows.Forms.Label
    Public WithEvents cboLeaveType As System.Windows.Forms.ComboBox
    Private WithEvents lblEmployee As System.Windows.Forms.Label
    Public WithEvents cboEmployee As System.Windows.Forms.ComboBox
    Private WithEvents lblLeaveForm As System.Windows.Forms.Label
    Friend WithEvents objbtnSearchLeaveType As eZee.Common.eZeeGradientButton
    Friend WithEvents objbtnSearchEmployee As eZee.Common.eZeeGradientButton
    Public WithEvents cboLeaveForm As System.Windows.Forms.ComboBox
    Friend WithEvents objbtnSearchLeaveForm As eZee.Common.eZeeGradientButton
    Friend WithEvents chkIncludeDependandList As System.Windows.Forms.CheckBox
    Friend WithEvents chkDonotShowExpense As System.Windows.Forms.CheckBox
End Class
