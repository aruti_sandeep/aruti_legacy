'************************************************************************************************************************************
'Class Name : clsEmployeeLeaveBalanceSummaryReport.vb
'Purpose    :
'Date       :02/12/2010
'Written By :Vimal M. Gohil
'Modified   :
'************************************************************************************************************************************

Imports eZeeCommonLib
Imports Aruti.Data
Imports System.Math
''' <summary>
''' Purpose: Report Generation Class 
''' Developer: Vimal M. Gohil
''' </summary>
Public Class clsEmployeeLeaveBalanceListReport
    Inherits IReportData
    Private Shared ReadOnly mstrModuleName As String = "clsEmployeeLeaveBalanceListReport"
    Private mstrReportId As String = enArutiReport.EmployeeLeaveBalanceList
    Dim objDataOperation As clsDataOperation

#Region " Constructor "

    Public Sub New(Byval intLangId As Integer,Byval intCompanyId AS Integer) 'S.SANDEEP |24-Sep-2020| -- START --END
        Me.setReportData(CInt(mstrReportId),intLangId,intCompanyId) 'S.SANDEEP |24-Sep-2020| -- START --END

        'Pinkal (09-May-2013) -- Start
        'Enhancement : TRA Changes

        Call Create_OnDetailReport()
        Create_OnDetailEmpWOAccrueReport()

        'Pinkal (09-May-2013) -- End
    End Sub

#End Region

#Region " Private variables "
    Private mintReportId As Integer = 0
    Private mstrReportTypeName As String = ""

    Private mintEmployeeId As Integer = 0
    Private mstrEmployeeName As String = ""

    Private mstrLeaveTypeIds As String = ""
    Private mdtDate As DateTime = Nothing
    Private mstrOrderByQuery As String = ""
    Private mstrLeavTypeList As String = ""
    Private mintViewIndex As Integer = -1
    Private mstrViewByIds As String = String.Empty
    Private mstrViewByName As String = String.Empty
    Dim mblnIncludeInactiveEmp As Boolean = False
    Private mstrAnalysis_Fields As String = ""
    Private mstrAnalysis_Join As String = ""
    Private mstrAnalysis_OrderBy As String = ""
    Private mstrReport_GroupName As String = ""
    Private mintLeaveBalanceSetting As Integer = -1
    Private mstrAdvance_Filter As String = String.Empty
    Dim Rpt As CrystalDecisions.CrystalReports.Engine.ReportClass
    Private mintUserUnkid As Integer = -1
    Private mintCompanyUnkid As Integer = -1
    Private mstrUserAccessFilter As String = String.Empty
    Private mblnFirstNamethenSurname As Boolean = ConfigParameter._Object._FirstNamethenSurname
    Private mblnIncludeCloseELC As Boolean = False
    Private mdtdbStartDate As DateTime = Nothing
    Private mdtdbEndDate As DateTime = Nothing
    Private mblnIncludeLeaveEncashment As Boolean = True

    'Pinkal (06-Jan-2016) -- Start
    'Enhancement - Working on Changes in SS for Leave Module.
    Private mblnIncludeAccessFilterQry As Boolean = True
    'Pinkal (06-Jan-2016) -- End


    'Pinkal (18-Nov-2016) -- Start
    'Enhancement - Working on IBA Leave Requirement as per Mr.Rutta's Guidance.
    Private mintLeaveAccrueTenureSetting As Integer = enLeaveAccrueTenureSetting.Yearly
    Private mintLeaveAccrueDaysAfterEachMonth As Integer = 0
    'Pinkal (18-Nov-2016) -- End


#End Region

#Region " Properties "
    Public WriteOnly Property _ReportId() As Integer
        Set(ByVal value As Integer)
            mintReportId = value
        End Set
    End Property

    Public WriteOnly Property _ReportTypeName() As String
        Set(ByVal value As String)
            mstrReportTypeName = value
        End Set
    End Property

    Public WriteOnly Property _EmployeeId() As Integer
        Set(ByVal value As Integer)
            mintEmployeeId = value
        End Set
    End Property

    Public WriteOnly Property _EmployeeName() As String
        Set(ByVal value As String)
            mstrEmployeeName = value
        End Set
    End Property

    Public WriteOnly Property _LeaveTypeIds() As String
        Set(ByVal value As String)
            mstrLeaveTypeIds = value
        End Set
    End Property

    Public WriteOnly Property _Date() As DateTime
        Set(ByVal value As DateTime)
            mdtDate = value
        End Set
    End Property

    Public WriteOnly Property _OrderByQuery()
        Set(ByVal value)
            mstrOrderByQuery = value
        End Set
    End Property

    Public WriteOnly Property _IncludeInactiveEmp() As Boolean
        Set(ByVal value As Boolean)
            mblnIncludeInactiveEmp = value
        End Set
    End Property

    Public WriteOnly Property _ViewIndex() As Integer
        Set(ByVal value As Integer)
            mintViewIndex = value
        End Set
    End Property

    Public WriteOnly Property _ViewByIds() As String
        Set(ByVal value As String)
            mstrViewByIds = value
        End Set
    End Property

    Public WriteOnly Property _ViewByName() As String
        Set(ByVal value As String)
            mstrViewByName = value
        End Set
    End Property

    Public WriteOnly Property _Analysis_Fields() As String
        Set(ByVal value As String)
            mstrAnalysis_Fields = value
        End Set
    End Property

    Public WriteOnly Property _Analysis_Join() As String
        Set(ByVal value As String)
            mstrAnalysis_Join = value
        End Set
    End Property

    Public WriteOnly Property _Analysis_OrderBy() As String
        Set(ByVal value As String)
            mstrAnalysis_OrderBy = value
        End Set
    End Property

    Public WriteOnly Property _Report_GroupName() As String
        Set(ByVal value As String)
            mstrReport_GroupName = value
        End Set
    End Property


    'Pinkal (06-Feb-2013) -- Start
    'Enhancement : TRA Changes

    Public WriteOnly Property _LeaveBalanceSetting() As Integer
        Set(ByVal value As Integer)
            mintLeaveBalanceSetting = value
        End Set
    End Property

    'Pinkal (06-Feb-2013) -- End

    'Pinkal (24-Feb-2013) -- Start
    'Enhancement : TRA Changes

    Public WriteOnly Property _Advance_Filter() As String
        Set(ByVal value As String)
            mstrAdvance_Filter = value
        End Set
    End Property

    'Pinkal (24-Feb-2013) -- End



    'Pinkal (01-Feb-2014) -- Start
    'Enhancement : TRA Changes

    Public ReadOnly Property _Rpt() As CrystalDecisions.CrystalReports.Engine.ReportClass
        Get
            Return Rpt
        End Get
    End Property

    Public WriteOnly Property _CompanyUnkId() As Integer
        Set(ByVal value As Integer)
            mintCompanyUnkid = value
        End Set
    End Property

    Public WriteOnly Property _UserUnkId() As Integer
        Set(ByVal value As Integer)
            mintUserUnkid = value
        End Set
    End Property

    Public WriteOnly Property _UserAccessFilter() As String
        Set(ByVal value As String)
            mstrUserAccessFilter = value
        End Set
    End Property

    'Pinkal (01-Feb-2014) -- End

    'S.SANDEEP [ 26 MAR 2014 ] -- START
    Public WriteOnly Property _FirstNamethenSurname() As Boolean
        Set(ByVal value As Boolean)
            mblnFirstNamethenSurname = value
        End Set
    End Property
    'S.SANDEEP [ 26 MAR 2014 ] -- END


    'Pinkal (21-Jul-2014) -- Start
    'Enhancement - TRA Leave Enhancement From TRA and TANAPA Urgent Fixes.Docx File Point No 1 & 16
    Public WriteOnly Property _IncludeCloseELC()
        Set(ByVal value)
            mblnIncludeCloseELC = value
        End Set
    End Property
    'Pinkal (21-Jul-2014) -- End


    'Pinkal (25-Jul-2015) -- Start
    'Enhancement :Getting Problem in showing Leave Balance in Payslip in ELC Setting For (CIT) 

    ''' <summary>
    ''' Purpose: Get or Set DBStartdate
    ''' Modify By: Pinkal 
    ''' </summary>
    ''' 
    Public Property _DBStartdate() As DateTime
        Get
            Return mdtdbStartDate
        End Get
        Set(ByVal value As DateTime)
            mdtdbStartDate = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set DBEnddate
    ''' Modify By: Pinkal
    ''' </summary>
    Public Property _DBEnddate() As DateTime
        Get
            Return mdtdbEndDate
        End Get
        Set(ByVal value As DateTime)
            mdtdbEndDate = value
        End Set
    End Property

    'Pinkal (25-Jul-2015) -- End


    'Pinkal (18-Aug-2015) -- Start
    'Enhancement - WORKING ON LEAVE ENCASHMENT DEDUCTION IN LEAVE MODULE.

    Public Property _IncludeEncashment() As Boolean
        Get
            Return mblnIncludeLeaveEncashment
        End Get
        Set(ByVal value As Boolean)
            mblnIncludeLeaveEncashment = value
        End Set
    End Property

    'Pinkal (18-Aug-2015) -- End


    'Pinkal (06-Jan-2016) -- Start
    'Enhancement - Working on Changes in SS for Leave Module.
    Public Property _IncludeAccessFilterQry() As Boolean
        Get
            Return mblnIncludeAccessFilterQry
        End Get
        Set(ByVal value As Boolean)
            mblnIncludeAccessFilterQry = value
        End Set
    End Property
    'Pinkal (06-Jan-2016) -- End

    'Pinkal (18-Nov-2016) -- Start
    'Enhancement - Working on IBA Leave Requirement as per Mr.Rutta's Guidance.

    Public WriteOnly Property _LeaveAccrueTenureSetting() As Integer
        Set(ByVal value As Integer)
            mintLeaveAccrueTenureSetting = value
        End Set
    End Property

    Public WriteOnly Property _LeaveAccrueDaysAfterEachMonth() As Integer
        Set(ByVal value As Integer)
            mintLeaveAccrueDaysAfterEachMonth = value
        End Set
    End Property

    'Pinkal (18-Nov-2016) -- End

#End Region

#Region "Public Function & Procedures "

    Public Sub SetDefaultValue()
        Try
            mintReportId = 0
            mstrReportTypeName = ""
            mintEmployeeId = 0
            mstrEmployeeName = ""
            mdtDate = Nothing
            mstrOrderByQuery = ""
            mblnIncludeInactiveEmp = False
            mintViewIndex = -1
            mstrViewByIds = ""
            mstrOrderByQuery = ""

            'Pinkal (24-Feb-2013) -- Start
            'Enhancement : TRA Changes
            mstrAdvance_Filter = ""
            'Pinkal (24-Feb-2013) -- End


            'Pinkal (21-Jul-2014) -- Start
            'Enhancement - TRA Leave Enhancement From TRA and TANAPA Urgent Fixes.Docx File Point No 1 & 16
            mblnIncludeCloseELC = False
            'Pinkal (21-Jul-2014) -- End


            'Pinkal (18-Aug-2015) -- Start
            'Enhancement - WORKING ON LEAVE ENCASHMENT DEDUCTION IN LEAVE MODULE.
            mintViewIndex = -1
            mstrViewByIds = ""
            mstrViewByName = ""
            mstrAnalysis_Fields = ""
            mstrAnalysis_Join = ""
            mstrAnalysis_OrderBy = ""
            mstrReport_GroupName = ""
            mblnIncludeLeaveEncashment = True
            'Pinkal (18-Aug-2015) -- End



            'Pinkal (06-Jan-2016) -- Start
            'Enhancement - Working on Changes in SS for Leave Module.
            mblnIncludeAccessFilterQry = True
            'Pinkal (06-Jan-2016) -- End


        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: SetDefaultValue; Module Name: " & mstrModuleName)
        End Try
    End Sub

    Public Sub FilterTitleAndFilterQuery()
        Me._FilterQuery = ""
        Me._FilterTitle = ""

        Try

            Me._FilterTitle &= Language.getMessage(mstrModuleName, 16, " As On Date : ") & " " & mdtDate.ToShortDateString & " "

            If mintEmployeeId > 0 Then
                objDataOperation.AddParameter("@EmpId", SqlDbType.Int, eZeeDataType.INT_SIZE, mintEmployeeId)
                Me._FilterQuery &= " AND hremployee_master.employeeunkid = @EmpId "
                Me._FilterTitle &= Language.getMessage(mstrModuleName, 17, " Employee : ") & " " & mstrEmployeeName & " "
            End If


            If mintReportId <= 1 Then
                mstrLeavTypeList = mstrLeaveTypeIds

                If mstrLeavTypeList.Length > 0 Then
                    Me._FilterQuery &= " AND lvleavetype_master.leavetypeunkid IN (" & mstrLeavTypeList & ") "
                End If


                'INCLUDE YEAR TO SHOW ONLY PRESENT YEAR LEAVE BALANCE 
                Me._FilterQuery &= " AND lvleavebalance_tran.yearunkid = " & FinancialYear._Object._YearUnkid
                'INCLUDE YEAR TO SHOW ONLY PRESENT YEAR LEAVE BALANCE 

            End If

            If mblnIncludeInactiveEmp = False Then
                objDataOperation.AddParameter("@startdate", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, ConfigParameter._Object._EmployeeAsOnDate)
                objDataOperation.AddParameter("@enddate", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, ConfigParameter._Object._EmployeeAsOnDate)
            End If

            If Me.OrderByQuery <> "" Then

                'Pinkal (21-Jul-2014) -- Start
                'Enhancement - TRA Leave Enhancement From TRA and TANAPA Urgent Fixes.Docx File Point No 1 & 16

                If mintReportId <= 1 Then
                    mstrOrderByQuery &= " ORDER BY " & Me.OrderByQuery & ",CONVERT(CHAR(8), lvleavebalance_tran.startdate, 112)"
                Else
                    mstrOrderByQuery &= " ORDER BY " & Me.OrderByQuery
                End If

                'Pinkal (21-Jul-2014) -- End

                Me._FilterTitle &= Language.getMessage(mstrModuleName, 18, " Order By : ") & " " & Me.OrderByDisplay
            End If

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: FilterTitleAndFilterQuery; Module Name: " & mstrModuleName)
        End Try
    End Sub

    Public Overrides Sub generateReport(ByVal pintReportType As Integer, Optional ByVal PrintAction As Aruti.Data.enPrintAction = Aruti.Data.enPrintAction.Preview, Optional ByVal ExportAction As Aruti.Data.enExportAction = Aruti.Data.enExportAction.None)
        Dim objRpt As CrystalDecisions.CrystalReports.Engine.ReportClass = Nothing
        Dim strReportExportFile As String = ""
        Try


            ''Pinkal (01-Feb-2014) -- Start
            ''Enhancement : TRA Changes

            'If mintCompanyUnkid <= 0 Then
            '    mintCompanyUnkid = Company._Object._Companyunkid
            'End If

            'Company._Object._Companyunkid = mintCompanyUnkid
            'ConfigParameter._Object._Companyunkid = mintCompanyUnkid

            'If mintUserUnkid <= 0 Then
            '    mintUserUnkid = User._Object._Userunkid
            'End If

            'User._Object._Userunkid = mintUserUnkid

            ''Pinkal (01-Feb-2014) -- End



            ''Pinkal (09-May-2013) -- Start
            ''Enhancement : TRA Changes

            'If pintReportType <= 1 Then
            '    objRpt = Generate_DetailReport()
            'Else
            '    objRpt = Generate_DetailEmpWOAccrueReport()
            'End If

            ''Pinkal (09-May-2013) -- End

            ''Pinkal (01-Feb-2014) -- Start
            ''Enhancement : TRA Changes
            'Rpt = objRpt
            ''Pinkal (01-Feb-2014) -- End

            'If Not IsNothing(objRpt) Then
            '    Call ReportExecute(objRpt, PrintAction, ExportAction, ConfigParameter._Object._ExportReportPath, ConfigParameter._Object._OpenAfterExport)
            'End If
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: generateReport; Module Name: " & mstrModuleName)
        End Try
    End Sub


    'Pinkal (24-Aug-2015) -- Start
    'Enhancement - WORKING ON ACTIVE EMPLOYEE CONDITION.

    Public Overrides Sub generateReportNew(ByVal xDatabaseName As String, ByVal xUserUnkid As Integer, ByVal xYearUnkid As Integer, ByVal xCompanyUnkid As Integer _
                                                                , ByVal xPeriodStart As Date, ByVal xPeriodEnd As Date, ByVal xUserModeSetting As String, ByVal xOnlyApproved As Boolean _
                                                                , ByVal xExportReportPath As String, ByVal xOpenReportAfterExport As Boolean, ByVal pintReportType As Integer _
                                                                , Optional ByVal PrintAction As Aruti.Data.enPrintAction = Aruti.Data.enPrintAction.Preview _
                                                                , Optional ByVal ExportAction As Aruti.Data.enExportAction = Aruti.Data.enExportAction.None _
                                                                , Optional ByVal intBaseCurrencyUnkid As Integer = 0)

        Dim objRpt As CrystalDecisions.CrystalReports.Engine.ReportClass = Nothing
        Dim strReportExportFile As String = ""
        Try
            If mintCompanyUnkid <= 0 Then
                mintCompanyUnkid = Company._Object._Companyunkid
            End If

            Company._Object._Companyunkid = mintCompanyUnkid
            ConfigParameter._Object._Companyunkid = mintCompanyUnkid

            If mintUserUnkid <= 0 Then
                mintUserUnkid = User._Object._Userunkid
            End If

            User._Object._Userunkid = mintUserUnkid

            If pintReportType <= 1 Then
                objRpt = Generate_DetailReport(xDatabaseName, xUserUnkid, xYearUnkid, xCompanyUnkid, xPeriodEnd, xUserModeSetting, xOnlyApproved)
            Else
                objRpt = Generate_DetailEmpWOAccrueReport(xDatabaseName, xUserUnkid, xYearUnkid, xCompanyUnkid, xPeriodEnd, xUserModeSetting, xOnlyApproved)
            End If

            Rpt = objRpt

            If Not IsNothing(objRpt) Then
                Call ReportExecute(objRpt, PrintAction, ExportAction, xExportReportPath, xOpenReportAfterExport)
            End If
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: generateReportNew; Module Name: " & mstrModuleName)
        End Try
    End Sub

    'Pinkal (24-Aug-2015) -- End


    Public Overrides Sub setDefaultOrderBy(ByVal intReportType As Integer)
        OrderByDisplay = ""
        OrderByQuery = ""
        Try

            'Pinkal (09-May-2013) -- Start
            'Enhancement : TRA Changes

            If intReportType <= 1 Then
                OrderByDisplay = iColumn_DetailReport.ColumnItem(0).DisplayName
                OrderByQuery = iColumn_DetailReport.ColumnItem(0).Name
            Else
                OrderByDisplay = iColumn_EmpWOAccrueReport.ColumnItem(0).DisplayName
                OrderByQuery = iColumn_EmpWOAccrueReport.ColumnItem(0).Name
            End If

            'Pinkal (09-May-2013) -- End

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: setDefaultOrderBy; Module Name: " & mstrModuleName)
        End Try
    End Sub

    Public Overrides Sub setOrderBy(ByVal intReportType As Integer)
        Try

            'Pinkal (09-May-2013) -- Start
            'Enhancement : TRA Changes

            If intReportType <= 1 Then
                Call OrderByExecute(iColumn_DetailReport)
            Else
                Call OrderByExecute(iColumn_EmpWOAccrueReport)
            End If

            'Pinkal (09-May-2013) -- End

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: setOrderBy; Module Name: " & mstrModuleName)
        End Try
    End Sub

#End Region

#Region " Report Generation "

#Region " Employee Leave Balance Employee Wise AND Leave Type Wise"

    Dim iColumn_DetailReport As New IColumnCollection

    Public Property Field_OnDetailReport() As IColumnCollection
        Get
            Return iColumn_DetailReport
        End Get
        Set(ByVal value As IColumnCollection)
            iColumn_DetailReport = value
        End Set
    End Property

    Private Sub Create_OnDetailReport()
        Try
            iColumn_DetailReport.Clear()
            iColumn_DetailReport.Add(New IColumn("EmpCode", Language.getMessage(mstrModuleName, 1, "Code")))
            iColumn_DetailReport.Add(New IColumn("EmpName", Language.getMessage(mstrModuleName, 2, "Employee")))
            iColumn_DetailReport.Add(New IColumn("LeaveTypeCode", Language.getMessage(mstrModuleName, 3, "Leave Code")))
            iColumn_DetailReport.Add(New IColumn("LeaveTypeName", Language.getMessage(mstrModuleName, 4, "Leave Type")))
            'iColumn_DetailReport.Add(New IColumn("LeaveAmountBF", Language.getMessage(mstrModuleName, 5, "Leave B/F")))
            'iColumn_DetailReport.Add(New IColumn("LastYrAccAmount", Language.getMessage(mstrModuleName, 6, "Total Accrued Year To Date")))
            'iColumn_DetailReport.Add(New IColumn("LeaveAmountBF", Language.getMessage(mstrModuleName, 7, "Total Issued Year To Date")))
            'iColumn_DetailReport.Add(New IColumn("LeaveAmountBF", Language.getMessage(mstrModuleName, 8, "Balance Year To Date")))

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: Create_OnDetailReport; Module Name: " & mstrModuleName)
        End Try
    End Sub


    'Pinkal (24-Aug-2015) -- Start
    'Enhancement - WORKING ON ACTIVE EMPLOYEE CONDITION.

    'Private Function Generate_DetailReport() As CrystalDecisions.CrystalReports.Engine.ReportClass
    '    Dim objRpt As CrystalDecisions.CrystalReports.Engine.ReportClass = Nothing
    '    Dim StrQ As String = ""
    '    Dim dsList As New DataSet
    '    Dim rpt_Data As ArutiReport.Designer.dsArutiReport
    '    Try
    '        objDataOperation = New clsDataOperation

    '        'StrQ = "SELECT  hremployee_master.employeeunkid AS EmpId  " & _
    '        '"      , hremployee_master.employeecode AS EmpCode  " & _
    '        '"      , ISNULL(hremployee_master.firstname, '') + ' '  " & _
    '        '"        + ISNULL(hremployee_master.othername, '') + ' '  " & _
    '        '"        + ISNULL(hremployee_master.surname, '') AS EmpName  " & _
    '        '"      , lvleavetype_master.leavetypeunkid AS LeaveTypeId  " & _
    '        '"      , lvleavetype_master.leavetypecode AS LeaveTypeCode " & _
    '        '"      , lvleavetype_master.leavename AS LeaveTypeName  " & _
    '        '"      , CONVERT(CHAR(8), lvleavebalance_tran.startdate, 112) AS StartDate " & _
    '        '"      , CONVERT(CHAR(8), lvleavebalance_tran.enddate, 112) AS EndDate " & _
    '        '"      , lvleavebalance_tran.uptolstyr_accrueamt AS LastYrAccAmount " & _
    '        '"      , lvleavebalance_tran.accrue_amount AS ThisYrAccAmount " & _
    '        '"      ,   CASE WHEN (lvleavebalance_tran.uptolstyr_accrueamt - lvleavebalance_tran.accrue_amount) > 0  then " & _
    '        '"                 convert(Decimal(10,2),(lvleavebalance_tran.accrue_amount - (lvleavebalance_tran.uptolstyr_accrueamt - lvleavebalance_tran.accrue_amount))) " & _
    '        '"          ELSE 0 END  AS LeaveAmountBF " & _
    '        '"FROM    lvleavebalance_tran " & _
    '        '"        JOIN hremployee_master  ON lvleavebalance_tran.employeeunkid = hremployee_master.employeeunkid  " & _
    '        '"        JOIN lvleavetype_master ON lvleavetype_master.leavetypeunkid = lvleavebalance_tran.leavetypeunkid " & _
    '        '"WHERE   hremployee_master.isactive = 1  " & _
    '        '"        AND lvleavetype_master.isactive = 1  " & _
    '        '"        AND lvleavebalance_tran.isvoid = 0 "



    '        'Pinkal (5-MAY-2012) -- Start
    '        'Enhancement : TRA Changes

    '        'StrQ = "SELECT  hremployee_master.employeeunkid AS EmpId  " & _
    '        '"      , hremployee_master.employeecode AS EmpCode  " & _
    '        '"      , ISNULL(hremployee_master.firstname, '') + ' '  " & _
    '        '"        + ISNULL(hremployee_master.othername, '') + ' '  " & _
    '        '"        + ISNULL(hremployee_master.surname, '') AS EmpName  " & _
    '        '"      , lvleavetype_master.leavetypeunkid AS LeaveTypeId  " & _
    '        '"      , lvleavetype_master.leavetypecode AS LeaveTypeCode " & _
    '        '"      , lvleavetype_master.leavename AS LeaveTypeName  " & _
    '        '"      , CONVERT(CHAR(8), lvleavebalance_tran.startdate, 112) AS StartDate " & _
    '        '"      , CONVERT(CHAR(8), lvleavebalance_tran.enddate, 112) AS EndDate " & _
    '        '"      , CAST (lvleavebalance_tran.uptolstyr_accrueamt AS DECIMAL(10,2)) AS LastYrAccAmount " & _
    '        '"      , CAST (lvleavebalance_tran.accrue_amount AS DECIMAL(10,2)) AS ThisYrAccAmount " & _
    '        '"      , ISNULL(CAST(leavebf AS DECIMAL(10,2)),0) AS LeaveAmountBF  " & _
    '        '"FROM    lvleavebalance_tran " & _
    '        '"        JOIN hremployee_master  ON lvleavebalance_tran.employeeunkid = hremployee_master.employeeunkid  " & _
    '        '"        JOIN lvleavetype_master ON lvleavetype_master.leavetypeunkid = lvleavebalance_tran.leavetypeunkid " & _
    '        '"WHERE  lvleavetype_master.isactive = 1  " & _
    '        '"        AND lvleavebalance_tran.isvoid = 0 "



    '        StrQ = "SELECT "

    '        StrQ &= " hremployee_master.employeeunkid AS EmpId  " & _
    '        "      , hremployee_master.employeecode AS EmpCode  "

    '        'S.SANDEEP [ 26 MAR 2014 ] -- START
    '        '", ISNULL(hremployee_master.firstname, '') + ' ' + ISNULL(hremployee_master.othername, '') + ' '+ ISNULL(hremployee_master.surname, ' ') AS EmpName "
    '        If mblnFirstNamethenSurname = True Then
    '            StrQ &= ", ISNULL(hremployee_master.firstname, '') + ' ' + ISNULL(hremployee_master.othername, '') + ' ' + ISNULL(hremployee_master.surname, '') AS EmpName "
    '        Else
    '            StrQ &= ", ISNULL(hremployee_master.surname, '') + ' ' + ISNULL(hremployee_master.firstname, '') + ' ' + ISNULL(hremployee_master.othername, '') AS EmpName "
    '        End If
    '        'S.SANDEEP [ 26 MAR 2014 ] -- END

    '        StrQ &= "      , lvleavetype_master.leavetypeunkid AS LeaveTypeId  " & _
    '"      , lvleavetype_master.leavetypecode AS LeaveTypeCode " & _
    '"      , lvleavetype_master.leavename AS LeaveTypeName  " & _
    '"      , CONVERT(CHAR(8), lvleavebalance_tran.startdate, 112) AS StartDate " & _
    '"      , CONVERT(CHAR(8), lvleavebalance_tran.enddate, 112) AS EndDate " & _
    '"      , CAST (lvleavebalance_tran.uptolstyr_accrueamt AS DECIMAL(10,2)) AS LastYrAccAmount " & _
    '"      , CAST (lvleavebalance_tran.accrue_amount AS DECIMAL(10,2)) AS ThisYrAccAmount " & _
    '"      , ISNULL(CAST(leavebf AS DECIMAL(10,2)),0) AS LeaveAmountBF  " & _
    '                "      , ISNULL(lvleavebalance_tran.isshortleave,0) AS isshortleave " & _
    '                "      , CAST (lvleavebalance_tran.issue_amount AS DECIMAL(10, 2)) AS ThisYrissueamt " & _
    '                "      , CAST (lvleavebalance_tran.remaining_bal AS DECIMAL(10, 2)) AS ThisYrremainingBal " & _
    '                    "      , CONVERT(CHAR(8),hremployee_master.appointeddate, 112) AS appointeddate " & _
    '                    "      , ISNULL(lvleavebalance_tran.actualamount,0) AS actualamount " & _
    '                    "      , ISNULL(lvleavebalance_tran.adj_remaining_bal,0) AS adjustment "


    '        'Pinkal (29-May-2015) -- Start 'Enhancement - LEAVE ADJUSTMENT CHANGES REQUIRED BY DENNIS.[", ISNULL(lvleavebalance_tran.actualamount,0) AS actualamount " & _ ", ISNULL(lvleavebalance_tran.adj_remaining_bal,0) AS adjustment "]

    '        'Pinkal (21-Jul-2014) -- Start
    '        'Enhancement - TRA Leave Enhancement From TRA and TANAPA Urgent Fixes.Docx File Point No 1 & 16

    '        If mintLeaveBalanceSetting = enLeaveBalanceSetting.Financial_Year Then
    '            StrQ &= ", isclose_fy "
    '        ElseIf mintLeaveBalanceSetting = enLeaveBalanceSetting.ELC Then
    '            StrQ &= ", isopenelc "
    '        End If

    '        'Pinkal (21-Jul-2014) -- End


    '        If mintViewIndex > 0 Then
    '            StrQ &= mstrAnalysis_Fields
    '        Else
    '            StrQ &= ", 0 AS Id, '' AS GName "
    '        End If


    '        StrQ &= "  FROM    lvleavebalance_tran " & _
    '"        JOIN hremployee_master  ON lvleavebalance_tran.employeeunkid = hremployee_master.employeeunkid  " & _
    '                "        JOIN lvleavetype_master ON lvleavetype_master.leavetypeunkid = lvleavebalance_tran.leavetypeunkid "

    '        StrQ &= mstrAnalysis_Join

    '        StrQ &= "WHERE  lvleavetype_master.isactive = 1  AND lvleavebalance_tran.isvoid = 0 "


    '        'Pinkal (06-Feb-2013) -- Start
    '        'Enhancement : TRA Changes

    '        StrQ &= " AND hremployee_master.isapproved = 1 "


    '        'Pinkal (15-Jul-2013) -- Start
    '        'Enhancement : TRA Changes

    '        'If mintLeaveBalanceSetting = enLeaveBalanceSetting.ELC Then
    '        '    StrQ &= " AND isopenelc = 1 AND iselc = 1 "
    '        'End If

    '        If mintLeaveBalanceSetting = enLeaveBalanceSetting.ELC Then
    '            StrQ &= " AND iselc = 1 "
    '        End If

    '        'Pinkal (15-Jul-2013) -- End

    '        'Pinkal (06-Feb-2013) -- End    


    '        'Pinkal (21-Jul-2014) -- Start
    '        'Enhancement - TRA Leave Enhancement From TRA and TANAPA Urgent Fixes.Docx File Point No 1 & 16

    '        If mblnIncludeCloseELC = False AndAlso mintLeaveBalanceSetting = enLeaveBalanceSetting.ELC Then
    '            StrQ &= " AND isopenelc  = 1 "
    '        End If

    '        'If mintLeaveBalanceSetting = enLeaveBalanceSetting.ELC Then
    '        '    StrQ &= " AND isopenelc = 1 AND iselc = 1 "
    '        'End If
    '        'Pinkal (21-Jul-2014) -- End



    '        'Pinkal (24-Feb-2013) -- Start
    '        'Enhancement : TRA Changes
    '        If mstrAdvance_Filter.Trim.Length > 0 Then
    '            StrQ &= " AND " & mstrAdvance_Filter
    '        End If
    '        'Pinkal (24-Feb-2013) -- End


    '        If mblnIncludeInactiveEmp = False Then
    '            StrQ &= " AND CONVERT(CHAR(8),hremployee_master.appointeddate,112) <= @enddate " & _
    '                " AND ISNULL(CONVERT(CHAR(8),hremployee_master.termination_from_date,112),@startdate) >= @startdate " & _
    '                " AND ISNULL(CONVERT(CHAR(8),hremployee_master.termination_to_date,112),@startdate) >= @startdate " & _
    '                " AND ISNULL(CONVERT(CHAR(8), hremployee_master.empl_enddate,112), @startdate) >= @startdate "
    '        End If



    '        'Pinkal (01-Feb-2014) -- Start
    '        'Enhancement : TRA Changes

    '        'If UserAccessLevel._AccessLevel.Length > 0 Then
    '        '    StrQ &= UserAccessLevel._AccessLevelFilterString
    '        'End If

    '        If mstrUserAccessFilter.Trim.Length <= 0 Then
    '            mstrUserAccessFilter = UserAccessLevel._AccessLevelFilterString
    '        End If

    '        If mstrUserAccessFilter.Trim.Length > 0 Then
    '            StrQ &= mstrUserAccessFilter
    '        End If

    '        'Pinkal (01-Feb-2014) -- End

    '        Call FilterTitleAndFilterQuery()

    '        StrQ &= Me._FilterQuery

    '        StrQ &= mstrOrderByQuery

    '        dsList = objDataOperation.ExecQuery(StrQ, "DataTable")

    '        If objDataOperation.ErrorMessage <> "" Then
    '            Throw New Exception(objDataOperation.ErrorNumber & ":" & objDataOperation.ErrorMessage)
    '        End If

    '        rpt_Data = New ArutiReport.Designer.dsArutiReport

    '        For Each dtRow As DataRow In dsList.Tables("DataTable").Rows
    '            Dim rpt_Rows As DataRow
    '            rpt_Rows = rpt_Data.Tables("ArutiTable").NewRow

    '            rpt_Rows.Item("Column1") = dtRow.Item("EmpId")

    '            Select Case mintReportId
    '                Case 0
    '                    rpt_Rows.Item("Column5") = dtRow.Item("LeaveTypeCode")
    '                    rpt_Rows.Item("Column4") = dtRow.Item("LeaveTypeName")

    '                    rpt_Rows.Item("Column11") = dtRow.Item("EmpCode")
    '                    rpt_Rows.Item("Column12") = dtRow.Item("EmpName")

    '                Case 1
    '                    rpt_Rows.Item("Column2") = dtRow.Item("EmpCode")
    '                    rpt_Rows.Item("Column3") = dtRow.Item("EmpName")

    '                    rpt_Rows.Item("Column11") = dtRow.Item("LeaveTypeCode")
    '                    rpt_Rows.Item("Column12") = dtRow.Item("LeaveTypeName")
    '            End Select

    '            rpt_Rows.Item("Column6") = dtRow.Item("LeaveTypeId")
    '            rpt_Rows.Item("Column7") = Math.Round(dtRow.Item("LeaveAmountBF"), 2)
    '            If IsDBNull(dtRow.Item("StartDate")) Then
    '                rpt_Rows.Item("Column13") = ""
    '            Else
    '                rpt_Rows.Item("Column13") = eZeeDate.convertDate(dtRow.Item("StartDate").ToString).ToShortDateString
    '            End If

    '            If IsDBNull(dtRow.Item("EndDate")) Then
    '                rpt_Rows.Item("Column14") = ""
    '            Else
    '                rpt_Rows.Item("Column14") = eZeeDate.convertDate(dtRow.Item("EndDate").ToString).ToShortDateString
    '            End If


    '            If CBool(dtRow.Item("isshortleave")) = False Then



    '                'Pinkal (21-Jun-2012) -- Start
    '                'Enhancement : TRA Changes
    '                Dim mstrLeaveTypeID As String = ""
    '                Dim objleavetype As New clsleavetype_master
    '                Dim dFill As DataTable = objleavetype.GetDeductdToLeaveType("List", CInt(dtRow.Item("EmpId")), True)

    '                For i As Integer = 0 To dFill.Rows.Count - 1
    '                    mstrLeaveTypeID &= dFill.Rows(i)("leavetypeunkid").ToString() & ","
    '                Next

    '                If mstrLeaveTypeID.Length > 0 And mstrLeaveTypeID.Trim <> "0" Then
    '                    mstrLeaveTypeID = mstrLeaveTypeID.Substring(0, mstrLeaveTypeID.Length - 1)
    '                End If

    '                If mstrLeaveTypeID.Trim.Length > 0 Then
    '                    mstrLeaveTypeID &= "," & dtRow.Item("LeaveTypeId").ToString()
    '                End If

    '                'Pinkal (21-Jun-2012) -- End



    '                Dim objLeaveBalance As New clsleavebalance_tran
    '                Dim dsLeaveList As New DataSet
    '                If mstrLeaveTypeID.Trim.Length > 0 Then

    '                    'Pinkal (06-Feb-2013) -- Start
    '                    'Enhancement : TRA Changes

    '                    If mintLeaveBalanceSetting = enLeaveBalanceSetting.Financial_Year Then
    '                        dsLeaveList = objLeaveBalance.GetLeaveBalanceInfo(mdtDate.Date, mstrLeaveTypeID, dtRow.Item("EmpId"))
    '                    ElseIf mintLeaveBalanceSetting = enLeaveBalanceSetting.ELC Then

    '                        'Pinkal (21-Jul-2014) -- Start
    '                        'Enhancement - TRA Leave Enhancement From TRA and TANAPA Urgent Fixes.Docx File Point No 1 & 16
    '                        'dsLeaveList = objLeaveBalance.GetLeaveBalanceInfo(mdtDate.Date, mstrLeaveTypeID, dtRow.Item("EmpId"), , , True, True)

    '                        'Pinkal (25-Jul-2015) -- Start
    '                        'Enhancement :Getting Problem in showing Leave Balance in Payslip in ELC Setting For (CIT) 
    '                        objLeaveBalance._DBStartdate = mdtdbStartDate
    '                        objLeaveBalance._DBEnddate = mdtdbEndDate
    '                        'Pinkal (25-Jul-2015) -- End

    '                        If CBool(dtRow.Item("isopenelc")) = False Then
    '                            dsLeaveList = objLeaveBalance.GetLeaveBalanceInfo(eZeeDate.convertDate(dtRow.Item("EndDate").ToString()).Date, mstrLeaveTypeID, dtRow.Item("EmpId"), , , CBool(dtRow.Item("isopenelc")), True)
    '                        Else
    '                            dsLeaveList = objLeaveBalance.GetLeaveBalanceInfo(mdtDate.Date, mstrLeaveTypeID, dtRow.Item("EmpId"), , , CBool(dtRow.Item("isopenelc")), True)
    '                        End If

    '                        'Pinkal (21-Jul-2014) -- End


    '                    End If

    '                    'Pinkal (06-Feb-2013) -- End

    '                Else


    '                    'Pinkal (06-Feb-2013) -- Start
    '                    'Enhancement : TRA Changes
    '                    If mintLeaveBalanceSetting = enLeaveBalanceSetting.Financial_Year Then
    '                        dsLeaveList = objLeaveBalance.GetLeaveBalanceInfo(mdtDate.Date, dtRow.Item("LeaveTypeId"), dtRow.Item("EmpId"), )
    '                    ElseIf mintLeaveBalanceSetting = enLeaveBalanceSetting.ELC Then

    '                        'Pinkal (21-Jul-2014) -- Start
    '                        'Enhancement - TRA Leave Enhancement From TRA and TANAPA Urgent Fixes.Docx File Point No 1 & 16
    '                        'dsLeaveList = objLeaveBalance.GetLeaveBalanceInfo(mdtDate.Date, dtRow.Item("LeaveTypeId"), dtRow.Item("EmpId"), , , True, True)

    '                        'Pinkal (25-Jul-2015) -- Start
    '                        'Enhancement :Getting Problem in showing Leave Balance in Payslip in ELC Setting For (CIT) 
    '                        objLeaveBalance._DBStartdate = mdtdbStartDate
    '                        objLeaveBalance._DBEnddate = mdtdbEndDate
    '                        'Pinkal (25-Jul-2015) -- End

    '                        If CBool(dtRow.Item("isopenelc")) = False Then
    '                            dsLeaveList = objLeaveBalance.GetLeaveBalanceInfo(eZeeDate.convertDate(dtRow.Item("EndDate").ToString()).Date, dtRow.Item("LeaveTypeId"), dtRow.Item("EmpId"), , , CBool(dtRow.Item("isopenelc")), True)
    '                        Else
    '                            dsLeaveList = objLeaveBalance.GetLeaveBalanceInfo(mdtDate.Date, dtRow.Item("LeaveTypeId"), dtRow.Item("EmpId"), , , CBool(dtRow.Item("isopenelc")), True)
    '                        End If

    '                        'Pinkal (21-Jul-2014) -- End

    '                    End If

    '                    'Pinkal (06-Feb-2013) -- End
    '                End If

    '                If dsLeaveList.Tables(0).Rows.Count > 0 Then
    '                    rpt_Rows.Item("Column8") = CDec(dsLeaveList.Tables(0).Rows(0).Item("Accrue_amount")).ToString("#0.00")
    '                    rpt_Rows.Item("Column9") = CDec(dsLeaveList.Tables(0).Rows(0).Item("Issue_amount")).ToString("#0.00")
    '                    rpt_Rows.Item("Column10") = CDec(dsLeaveList.Tables(0).Rows(0).Item("Balance")).ToString("#0.00")
    '                Else

    '                    rpt_Rows.Item("Column8") = Math.Round(0, 2)
    '                    rpt_Rows.Item("Column9") = Math.Round(0, 2)
    '                    rpt_Rows.Item("Column10") = Math.Round(0, 2)
    '                End If

    '            Else

    '                'Pinkal (29-May-2015) -- Start
    '                'Enhancement - LEAVE ADJUSTMENT CHANGES REQUIRED BY DENNIS.
    '                'rpt_Rows.Item("Column8") = CDec(dtRow.Item("ThisYrAccAmount")).ToString("#0.00")
    '                rpt_Rows.Item("Column8") = CDec(dtRow.Item("actualamount")).ToString("#0.00")
    '                'Pinkal (29-May-2015) -- End
    '                rpt_Rows.Item("Column9") = CDec(dtRow.Item("ThisYrissueamt")).ToString("#0.00")
    '                rpt_Rows.Item("Column10") = CDec(dtRow.Item("ThisYrremainingBal")).ToString("#0.00")

    '            End If

    '            rpt_Rows.Item("Column15") = dtRow.Item("GName")


    '            'Pinkal (30-Nov-2013) -- Start
    '            'Enhancement : Oman Changes
    '            rpt_Rows.Item("Column16") = eZeeDate.convertDate(dtRow.Item("appointeddate").ToString()).ToShortDateString
    '            'Pinkal (30-Nov-2013) -- End

    '            'Pinkal (29-May-2015) -- Start
    '            'Enhancement - LEAVE ADJUSTMENT CHANGES REQUIRED BY DENNIS.
    '            rpt_Rows.Item("Column17") = CDec(dtRow.Item("adjustment")).ToString("#0.00")
    '            'Pinkal (29-May-2015) -- End


    '            rpt_Data.Tables("ArutiTable").Rows.Add(rpt_Rows)
    '        Next

    '        objRpt = New ArutiReport.Designer.rptEmployeeLeaveBalanceList

    '        ConfigParameter._Object.GetReportSettings(CInt(mstrReportId))
    '        Dim arrImageRow As DataRow = Nothing
    '        arrImageRow = rpt_Data.Tables("ArutiImage").NewRow()

    '        ReportFunction.Logo_Display(objRpt, _
    '                                    ConfigParameter._Object._IsDisplayLogo, _
    '                                    ConfigParameter._Object._ShowLogoRightSide, _
    '                                    "arutiLogo1", _
    '                                    "arutiLogo2", _
    '                                    arrImageRow, _
    '                                    "txtCompanyName", _
    '                                    "txtReportName", _
    '                                    "txtFilterDescription", _
    '                                    ConfigParameter._Object._GetLeftMargin, _
    '                                    ConfigParameter._Object._GetRightMargin)

    '        rpt_Data.Tables("ArutiImage").Rows.Add(arrImageRow)

    '        If rpt_Data.Tables("ArutiTable").Rows.Count <= 0 Then
    '            rpt_Data.Tables("ArutiTable").Rows.Add("")
    '        End If

    '        If ConfigParameter._Object._IsShowPreparedBy = True Then
    '            Call ReportFunction.TextChange(objRpt, "lblPreparedBy", Language.getMessage(mstrModuleName, 23, "Prepared By :"))
    '            Call ReportFunction.TextChange(objRpt, "txtUPreparedBy", User._Object._Username)
    '        Else
    '            Call ReportFunction.EnableSuppressSection(objRpt, "ReportFooterSection1", True)
    '        End If

    '        If ConfigParameter._Object._IsShowCheckedBy = True Then
    '            Call ReportFunction.TextChange(objRpt, "lblCheckedBy", Language.getMessage(mstrModuleName, 24, "Checked By :"))
    '        Else
    '            Call ReportFunction.EnableSuppressSection(objRpt, "ReportFooterSection2", True)
    '        End If

    '        If ConfigParameter._Object._IsShowApprovedBy = True Then
    '            Call ReportFunction.TextChange(objRpt, "lblApprovedBy", Language.getMessage(mstrModuleName, 25, "Approved By :"))
    '        Else
    '            Call ReportFunction.EnableSuppressSection(objRpt, "ReportFooterSection3", True)
    '        End If

    '        If ConfigParameter._Object._IsShowReceivedBy = True Then
    '            Call ReportFunction.TextChange(objRpt, "lblReceivedBy", Language.getMessage(mstrModuleName, 26, "Received By :"))
    '        Else
    '            Call ReportFunction.EnableSuppressSection(objRpt, "ReportFooterSection4", True)
    '        End If

    '        objRpt.SetDataSource(rpt_Data)

    '        Select Case mintReportId
    '            Case 0
    '                ReportFunction.EnableSuppressSection(objRpt, "GroupHeaderSection1", True)
    '                ReportFunction.EnableSuppressSection(objRpt, "GroupFooterSection1", True)

    '                Call ReportFunction.EnableSuppress(objRpt, "txtEmpCode", True)
    '                Call ReportFunction.EnableSuppress(objRpt, "txtEmpName", True)

    '                Call ReportFunction.TextChange(objRpt, "txtLeaveTypeCode", Language.getMessage(mstrModuleName, 9, "Code :"))
    '                Call ReportFunction.TextChange(objRpt, "txtLeaveTypeName", Language.getMessage(mstrModuleName, 10, "Leave :"))

    '                Call ReportFunction.TextChange(objRpt, "txtColumn14", Language.getMessage(mstrModuleName, 12, "Emp Code"))
    '                Call ReportFunction.TextChange(objRpt, "txtColumn15", Language.getMessage(mstrModuleName, 2, "Employee"))

    '                Call ReportFunction.TextChange(objRpt, "txtGroupTotal", Language.getMessage(mstrModuleName, 19, "Group Total :"))

    '                Call ReportFunction.EnableSuppress(objRpt, "grp3Col71", True)
    '                Call ReportFunction.EnableSuppress(objRpt, "grp3Col81", True)
    '                Call ReportFunction.EnableSuppress(objRpt, "grp3Col91", True)
    '                Call ReportFunction.EnableSuppress(objRpt, "grp3Balance1", True)

    '                'Call ReportFunction.SetRptDecimal(objRpt, "grp4Col71")
    '                'Call ReportFunction.SetRptDecimal(objRpt, "grp4Col81")
    '                'Call ReportFunction.SetRptDecimal(objRpt, "grp4Col91")
    '                'Call ReportFunction.SetRptDecimal(objRpt, "grp4Balance1")

    '            Case 1
    '                ReportFunction.EnableSuppressSection(objRpt, "GroupHeaderSection2", True)
    '                ReportFunction.EnableSuppressSection(objRpt, "GroupFooterSection2", True)

    '                Call ReportFunction.EnableSuppress(objRpt, "txtLeaveTypeCode", True)
    '                Call ReportFunction.EnableSuppress(objRpt, "txtLeaveTypeName", True)

    '                Call ReportFunction.TextChange(objRpt, "txtEmpCode", Language.getMessage(mstrModuleName, 9, "Code :"))
    '                Call ReportFunction.TextChange(objRpt, "txtEmpName", Language.getMessage(mstrModuleName, 11, "Employee :"))

    '                Call ReportFunction.TextChange(objRpt, "txtColumn14", Language.getMessage(mstrModuleName, 3, "Leave Code"))
    '                Call ReportFunction.TextChange(objRpt, "txtColumn15", Language.getMessage(mstrModuleName, 4, "Leave Type"))

    '                Call ReportFunction.TextChange(objRpt, "txtGroupTotal1", Language.getMessage(mstrModuleName, 19, "Group Total :"))

    '                Call ReportFunction.EnableSuppress(objRpt, "grp4Col71", True)
    '                Call ReportFunction.EnableSuppress(objRpt, "grp4Col81", True)
    '                Call ReportFunction.EnableSuppress(objRpt, "grp4Col91", True)
    '                Call ReportFunction.EnableSuppress(objRpt, "grp4Balance1", True)

    '                'Call ReportFunction.SetRptDecimal(objRpt, "grp3Col71")
    '                'Call ReportFunction.SetRptDecimal(objRpt, "grp3Col81")
    '                'Call ReportFunction.SetRptDecimal(objRpt, "grp3Col91")
    '                'Call ReportFunction.SetRptDecimal(objRpt, "grp3Balance1")
    '        End Select

    '        Call ReportFunction.TextChange(objRpt, "txtLeaveBF", Language.getMessage(mstrModuleName, 5, "Leave B/F"))
    '        Call ReportFunction.TextChange(objRpt, "txtTotalAccruedYearToDate", Language.getMessage(mstrModuleName, 6, "Total Accrued Year To Date"))
    '        Call ReportFunction.TextChange(objRpt, "txtTotalIssuedYearToDate", Language.getMessage(mstrModuleName, 7, "Total Issued Year To Date"))
    '        Call ReportFunction.TextChange(objRpt, "txtBalance", Language.getMessage(mstrModuleName, 8, "Balance Year To Date"))
    '        Call ReportFunction.TextChange(objRpt, "txtStartDate", Language.getMessage(mstrModuleName, 21, "Start Date"))
    '        Call ReportFunction.TextChange(objRpt, "txtEndDate", Language.getMessage(mstrModuleName, 22, "End Date"))
    '        Call ReportFunction.TextChange(objRpt, "txtGrandTotal", Language.getMessage(mstrModuleName, 20, "Grand Total :"))

    '        Call ReportFunction.TextChange(objRpt, "lblPrintedBy", Language.getMessage(mstrModuleName, 13, "Printed By :"))
    '        Call ReportFunction.TextChange(objRpt, "lblPrintedDate", Language.getMessage(mstrModuleName, 14, "Printed Date :"))


    '        'Pinkal (30-Nov-2013) -- Start
    '        'Enhancement : Oman Changes
    '        Call ReportFunction.TextChange(objRpt, "txtJoiningDate", Language.getMessage(mstrModuleName, 36, "Joining Date"))
    '        'Pinkal (30-Nov-2013) -- End

    '        'Pinkal (29-May-2015) -- Start
    '        'Enhancement - LEAVE ADJUSTMENT CHANGES REQUIRED BY DENNIS.
    '        Call ReportFunction.TextChange(objRpt, "txtLeaveAdjustment", Language.getMessage(mstrModuleName, 37, "Leave Adjustment"))
    '        'Pinkal (29-May-2015) -- End


    '        Call ReportFunction.TextChange(objRpt, "txtPrintedBy", Me._UserName)
    '        Call ReportFunction.TextChange(objRpt, "txtPrintedDate", Me._PrintDate)

    '        Call ReportFunction.TextChange(objRpt, "txtReportName", Me._ReportName & " " & "[" & mstrReportTypeName & "]")
    '        Call ReportFunction.TextChange(objRpt, "txtCompanyName", Me._CompanyName)
    '        Call ReportFunction.TextChange(objRpt, "txtFilterDescription", Me._FilterTitle)

    '        If mstrReport_GroupName.Trim.Length > 0 Then

    '            Call ReportFunction.TextChange(objRpt, "txtGroupName", mstrReport_GroupName)
    '            Call ReportFunction.TextChange(objRpt, "txtAllocationTotal", mstrReport_GroupName.Trim.Replace(":", "") & Language.getMessage(mstrModuleName, 27, " Total :"))

    '        Else
    '            ReportFunction.EnableSuppressSection(objRpt, "GroupHeaderSection3", True)
    '            ReportFunction.EnableSuppressSection(objRpt, "GroupFooterSection3", True)
    '        End If



    '        Return objRpt
    '    Catch ex As Exception
    '        Throw New Exception(ex.Message & "; Procedure Name: Generate_DetailReport; Module Name: " & mstrModuleName)
    '        Return Nothing
    '    End Try
    'End Function

    Private Function Generate_DetailReport(ByVal xDatabaseName As String, ByVal xUserUnkid As Integer, ByVal xYearUnkid As Integer, ByVal xCompanyUnkid As Integer _
                                                                , ByVal xEmployeeAsonDate As Date, ByVal xUserModeSetting As String, ByVal xOnlyApproved As Boolean) As CrystalDecisions.CrystalReports.Engine.ReportClass
        Dim objRpt As CrystalDecisions.CrystalReports.Engine.ReportClass = Nothing
        Dim StrQ As String = ""
        Dim dsList As New DataSet
        Dim rpt_Data As ArutiReport.Designer.dsArutiReport
        Try
            objDataOperation = New clsDataOperation


            'Pinkal (19-Feb-2020) -- Start
            'ENHANCEMENT VOLTAMP [0004521]:  Calculate Issue Leave Based As on Date with configuration Setting.
            Dim mblnLeaveIssueAsonDateOnLeaveBalanceListReport As Boolean = False
            Dim objConfig As New clsConfigOptions
            If objConfig.GetKeyValue(xCompanyUnkid, "LeaveIssueAsonDateOnLeaveBalanceListReport", Nothing).Trim.Length > 0 Then
                mblnLeaveIssueAsonDateOnLeaveBalanceListReport = CBool(objConfig.GetKeyValue(xCompanyUnkid, "LeaveIssueAsonDateOnLeaveBalanceListReport", Nothing))
            End If
            objConfig = Nothing
            'Pinkal (19-Feb-2020) -- End




            Dim xDateJoinQry, xDateFilterQry, xUACQry, xUACFiltrQry, xAdvanceJoinQry As String
            xDateJoinQry = "" : xDateFilterQry = "" : xUACQry = "" : xUACFiltrQry = "" : xAdvanceJoinQry = ""
            Call GetDatesFilterString(xDateJoinQry, xDateFilterQry, xEmployeeAsonDate, xEmployeeAsonDate, , , xDatabaseName)
            If mblnIncludeAccessFilterQry Then Call NewAccessLevelFilterString(xUACQry, xUACFiltrQry, xEmployeeAsonDate, xOnlyApproved, xDatabaseName, xUserUnkid, xCompanyUnkid, xYearUnkid, xUserModeSetting, "")
            Call GetAdvanceFilterQry(xAdvanceJoinQry, xEmployeeAsonDate, xDatabaseName)

            StrQ = " SELECT " & _
                       " hremployee_master.employeeunkid AS EmpId  " & _
            "      , hremployee_master.employeecode AS EmpCode  "

            If mblnFirstNamethenSurname = True Then
                StrQ &= ", ISNULL(hremployee_master.firstname, '') + ' ' + ISNULL(hremployee_master.othername, '') + ' ' + ISNULL(hremployee_master.surname, '') AS EmpName "
            Else
                StrQ &= ", ISNULL(hremployee_master.surname, '') + ' ' + ISNULL(hremployee_master.firstname, '') + ' ' + ISNULL(hremployee_master.othername, '') AS EmpName "
            End If


            StrQ &= "      , lvleavetype_master.leavetypeunkid AS LeaveTypeId  " & _
                    "      , lvleavetype_master.leavetypecode AS LeaveTypeCode " & _
                    "      , lvleavetype_master.leavename AS LeaveTypeName  " & _
                    "      , CONVERT(CHAR(8), lvleavebalance_tran.startdate, 112) AS StartDate " & _
                    "      , CONVERT(CHAR(8), lvleavebalance_tran.enddate, 112) AS EndDate " & _
                    "      , CAST (lvleavebalance_tran.uptolstyr_accrueamt AS DECIMAL(10,2)) AS LastYrAccAmount " & _
                    "      , CAST (lvleavebalance_tran.accrue_amount AS DECIMAL(10,2)) AS ThisYrAccAmount " & _
                    "      , ISNULL(CAST(leavebf AS DECIMAL(10,2)),0) AS LeaveAmountBF  " & _
                    "      , ISNULL(lvleavebalance_tran.isshortleave,0) AS isshortleave " & _
                    "      , CAST (lvleavebalance_tran.issue_amount AS DECIMAL(10, 2)) AS ThisYrissueamt " & _
                    "      , CAST (lvleavebalance_tran.remaining_bal AS DECIMAL(10, 2)) AS ThisYrremainingBal " & _
                        "      , CONVERT(CHAR(8),hremployee_master.appointeddate, 112) AS appointeddate " & _
                        "      , ISNULL(lvleavebalance_tran.actualamount,0) AS actualamount " & _
                        "      , ISNULL(lvleavebalance_tran.adj_remaining_bal,0) AS adjustment "


            'Pinkal (29-May-2015) -- Start 'Enhancement - LEAVE ADJUSTMENT CHANGES REQUIRED BY DENNIS.[", ISNULL(lvleavebalance_tran.actualamount,0) AS actualamount " & _ ", ISNULL(lvleavebalance_tran.adj_remaining_bal,0) AS adjustment "]


            If mintLeaveBalanceSetting = enLeaveBalanceSetting.Financial_Year Then
                StrQ &= ", isclose_fy "
            ElseIf mintLeaveBalanceSetting = enLeaveBalanceSetting.ELC Then
                StrQ &= ", isopenelc "
            End If

            If mintViewIndex > 0 Then
                StrQ &= mstrAnalysis_Fields
            Else
                StrQ &= ", 0 AS Id, '' AS GName "
            End If


            StrQ &= "  FROM    lvleavebalance_tran " & _
                    "        JOIN hremployee_master  ON lvleavebalance_tran.employeeunkid = hremployee_master.employeeunkid  " & _
                    "        JOIN lvleavetype_master ON lvleavetype_master.leavetypeunkid = lvleavebalance_tran.leavetypeunkid "

            StrQ &= mstrAnalysis_Join


            'Pinkal (11-Sep-2020) -- Start
            'Optimzation Leave NMB:  Working on Optimzing Various modules for Garbage Colletion Issue.
            If xAdvanceJoinQry.Trim.Length > 0 Then
                StrQ &= xAdvanceJoinQry
            End If
            'Pinkal (11-Sep-2020) -- End

            If xDateJoinQry.Trim.Length > 0 Then
                StrQ &= xDateJoinQry
            End If

            If xUACQry.Trim.Length > 0 Then
                StrQ &= xUACQry
            End If

          

            StrQ &= "WHERE  lvleavetype_master.isactive = 1  AND lvleavebalance_tran.isvoid = 0 "

            StrQ &= " AND hremployee_master.isapproved = 1 "

            If mintLeaveBalanceSetting = enLeaveBalanceSetting.ELC Then
                StrQ &= " AND iselc = 1 "
            End If


            'Pinkal (21-Jul-2014) -- Start
            'Enhancement - TRA Leave Enhancement From TRA and TANAPA Urgent Fixes.Docx File Point No 1 & 16

            If mblnIncludeCloseELC = False AndAlso mintLeaveBalanceSetting = enLeaveBalanceSetting.ELC Then
                StrQ &= " AND isopenelc  = 1 "
            End If

            'If mintLeaveBalanceSetting = enLeaveBalanceSetting.ELC Then
            '    StrQ &= " AND isopenelc = 1 AND iselc = 1 "
            'End If
            'Pinkal (21-Jul-2014) -- End

            If mstrAdvance_Filter.Trim.Length > 0 Then
                StrQ &= " AND " & mstrAdvance_Filter
            End If

            If xUACFiltrQry.Trim.Length > 0 Then
                StrQ &= " AND " & xUACFiltrQry
            End If

            If mblnIncludeInactiveEmp = False Then
                If xDateFilterQry.Trim.Length > 0 Then
                    StrQ &= xDateFilterQry
                End If
            End If


            'Pinkal (24-Aug-2015) -- Start
            'Enhancement - WORKING ON ACTIVE EMPLOYEE CONDITION.

            'If mblnIncludeInactiveEmp = False Then
            '    StrQ &= " AND CONVERT(CHAR(8),hremployee_master.appointeddate,112) <= @enddate " & _
            '        " AND ISNULL(CONVERT(CHAR(8),hremployee_master.termination_from_date,112),@startdate) >= @startdate " & _
            '        " AND ISNULL(CONVERT(CHAR(8),hremployee_master.termination_to_date,112),@startdate) >= @startdate " & _
            '        " AND ISNULL(CONVERT(CHAR(8), hremployee_master.empl_enddate,112), @startdate) >= @startdate "
            'End If


            'If mstrUserAccessFilter.Trim.Length <= 0 Then
            '    mstrUserAccessFilter = UserAccessLevel._AccessLevelFilterString
            'End If

            'If mstrUserAccessFilter.Trim.Length > 0 Then
            '    StrQ &= mstrUserAccessFilter
            'End If

            'Pinkal (24-Aug-2015) -- End

            Call FilterTitleAndFilterQuery()

            StrQ &= Me._FilterQuery

            StrQ &= mstrOrderByQuery

            dsList = objDataOperation.ExecQuery(StrQ, "DataTable")

            If objDataOperation.ErrorMessage <> "" Then
                Throw New Exception(objDataOperation.ErrorNumber & ":" & objDataOperation.ErrorMessage)
            End If

            rpt_Data = New ArutiReport.Designer.dsArutiReport

            For Each dtRow As DataRow In dsList.Tables("DataTable").Rows
                Dim rpt_Rows As DataRow
                rpt_Rows = rpt_Data.Tables("ArutiTable").NewRow

                rpt_Rows.Item("Column1") = dtRow.Item("EmpId")

                Select Case mintReportId
                    Case 0
                        rpt_Rows.Item("Column5") = dtRow.Item("LeaveTypeCode")
                        rpt_Rows.Item("Column4") = dtRow.Item("LeaveTypeName")

                        rpt_Rows.Item("Column11") = dtRow.Item("EmpCode")
                        rpt_Rows.Item("Column12") = dtRow.Item("EmpName")

                    Case 1
                        rpt_Rows.Item("Column2") = dtRow.Item("EmpCode")
                        rpt_Rows.Item("Column3") = dtRow.Item("EmpName")

                        rpt_Rows.Item("Column11") = dtRow.Item("LeaveTypeCode")
                        rpt_Rows.Item("Column12") = dtRow.Item("LeaveTypeName")
                End Select

                rpt_Rows.Item("Column6") = dtRow.Item("LeaveTypeId")
                rpt_Rows.Item("Column7") = Math.Round(dtRow.Item("LeaveAmountBF"), 2)
                If IsDBNull(dtRow.Item("StartDate")) Then
                    rpt_Rows.Item("Column13") = ""
                Else
                    rpt_Rows.Item("Column13") = eZeeDate.convertDate(dtRow.Item("StartDate").ToString).ToShortDateString
                End If

                If IsDBNull(dtRow.Item("EndDate")) Then
                    rpt_Rows.Item("Column14") = ""
                Else
                    rpt_Rows.Item("Column14") = eZeeDate.convertDate(dtRow.Item("EndDate").ToString).ToShortDateString
                End If


                If CBool(dtRow.Item("isshortleave")) = False Then

                    Dim mstrLeaveTypeID As String = ""
                    Dim objleavetype As New clsleavetype_master
                    Dim dFill As DataTable = objleavetype.GetDeductdToLeaveType("List", CInt(dtRow.Item("EmpId")), True)

                    For i As Integer = 0 To dFill.Rows.Count - 1
                        mstrLeaveTypeID &= dFill.Rows(i)("leavetypeunkid").ToString() & ","
                    Next

                    If mstrLeaveTypeID.Length > 0 And mstrLeaveTypeID.Trim <> "0" Then
                        mstrLeaveTypeID = mstrLeaveTypeID.Substring(0, mstrLeaveTypeID.Length - 1)
                    End If

                    If mstrLeaveTypeID.Trim.Length > 0 Then
                        mstrLeaveTypeID &= "," & dtRow.Item("LeaveTypeId").ToString()
                    End If


                    Dim objLeaveBalance As New clsleavebalance_tran
                    Dim dsLeaveList As New DataSet
                    If mstrLeaveTypeID.Trim.Length > 0 Then

                        If mintLeaveBalanceSetting = enLeaveBalanceSetting.Financial_Year Then

                            'Pinkal (28-Jan-2019) -- Start
                            'Bug - Due to Change In Get Balance Info Method report was showing 0 for all columns due to Pacra changes did by sohail for Leave Details.
                            'dsLeaveList = objLeaveBalance.GetLeaveBalanceInfo(mdtDate.Date, mstrLeaveTypeID, dtRow.Item("EmpId"), mintLeaveAccrueTenureSetting, mintLeaveAccrueDaysAfterEachMonth, , , False, False, mintLeaveBalanceSetting,0, xYearUnkid)

                            'Pinkal (19-Feb-2020) -- Start
                            'ENHANCEMENT VOLTAMP [0004521]:  Calculate Issue Leave Based As on Date with configuration Setting.
                            'dsLeaveList = objLeaveBalance.GetLeaveBalanceInfo(mdtDate.Date, mstrLeaveTypeID, dtRow.Item("EmpId"), mintLeaveAccrueTenureSetting, mintLeaveAccrueDaysAfterEachMonth, , , False, False, mintLeaveBalanceSetting, CInt(dtRow("isclose_fy")), xYearUnkid)
                            dsLeaveList = objLeaveBalance.GetLeaveBalanceInfo(mdtDate.Date, mstrLeaveTypeID, dtRow.Item("EmpId"), mintLeaveAccrueTenureSetting, mintLeaveAccrueDaysAfterEachMonth, , , False, False, mintLeaveBalanceSetting, CInt(dtRow("isclose_fy")), xYearUnkid, Nothing, "", True, mblnLeaveIssueAsonDateOnLeaveBalanceListReport)
                            'Pinkal (19-Feb-2020) -- End

                            'Pinkal (28-Jan-2019) -- End

                        ElseIf mintLeaveBalanceSetting = enLeaveBalanceSetting.ELC Then

                            objLeaveBalance._DBStartdate = mdtdbStartDate
                            objLeaveBalance._DBEnddate = mdtdbEndDate


                            If CBool(dtRow.Item("isopenelc")) = False Then
                                'Pinkal (15-Jan-2019) -- Start
                                'Bug - SOLVED ISSUE FOR PACRA ABOUT DEDUCTION LEAVE ISSUE.
                                'dsLeaveList = objLeaveBalance.GetLeaveBalanceInfo(eZeeDate.convertDate(dtRow.Item("EndDate").ToString()).Date, mstrLeaveTypeID, dtRow.Item("EmpId"), mintLeaveAccrueTenureSetting, mintLeaveAccrueDaysAfterEachMonth, , , CBool(dtRow.Item("isopenelc")), True)

                                'Pinkal (19-Feb-2020) -- Start
                                'ENHANCEMENT VOLTAMP [0004521]:  Calculate Issue Leave Based As on Date with configuration Setting.
                                If mblnLeaveIssueAsonDateOnLeaveBalanceListReport Then
                                    dsLeaveList = objLeaveBalance.GetLeaveBalanceInfo(mdtDate.Date, mstrLeaveTypeID, dtRow.Item("EmpId"), mintLeaveAccrueTenureSetting, mintLeaveAccrueDaysAfterEachMonth, , , CBool(dtRow.Item("isopenelc")), True, mintLeaveBalanceSetting, -1, xYearUnkid, Nothing, "", True, mblnLeaveIssueAsonDateOnLeaveBalanceListReport)
                                Else
                                    dsLeaveList = objLeaveBalance.GetLeaveBalanceInfo(eZeeDate.convertDate(dtRow.Item("EndDate").ToString()).Date, mstrLeaveTypeID, dtRow.Item("EmpId"), mintLeaveAccrueTenureSetting, mintLeaveAccrueDaysAfterEachMonth, , , CBool(dtRow.Item("isopenelc")), True, mintLeaveBalanceSetting, -1, xYearUnkid, Nothing, "", True, mblnLeaveIssueAsonDateOnLeaveBalanceListReport)
                                End If
                                'Pinkal (19-Feb-2020) -- End

                                'Pinkal (15-Jan-2019) -- End
                            Else
                                'Pinkal (15-Jan-2019) -- Start
                                'Bug - SOLVED ISSUE FOR PACRA ABOUT DEDUCTION LEAVE ISSUE.
                                'dsLeaveList = objLeaveBalance.GetLeav1BalanceInfo(mdtDate.Date, mstrLeaveTypeID, dtRow.Item("EmpId"), mintLeaveAccrueTenureSetting, mintLeaveAccrueDaysAfterEachMonth, , , CBool(dtRow.Item("isopenelc")), True)

                                'Pinkal (19-Feb-2020) -- Start
                                'ENHANCEMENT VOLTAMP [0004521]:  Calculate Issue Leave Based As on Date with configuration Setting.
                                'dsLeaveList = objLeaveBalance.GetLeaveBalanceInfo(mdtDate.Date, mstrLeaveTypeID, dtRow.Item("EmpId"), mintLeaveAccrueTenureSetting, mintLeaveAccrueDaysAfterEachMonth, , , CBool(dtRow.Item("isopenelc")), True, mintLeaveBalanceSetting, -1, xYearUnkid)
                                dsLeaveList = objLeaveBalance.GetLeaveBalanceInfo(mdtDate.Date, mstrLeaveTypeID, dtRow.Item("EmpId"), mintLeaveAccrueTenureSetting, mintLeaveAccrueDaysAfterEachMonth, , , CBool(dtRow.Item("isopenelc")), True, mintLeaveBalanceSetting, -1, xYearUnkid, Nothing, "", True, mblnLeaveIssueAsonDateOnLeaveBalanceListReport)
                                'Pinkal (19-Feb-2020) -- End

                                'Pinkal (15-Jan-2019) -- End
                            End If

                            'Pinkal (18-Nov-2016) -- End

                        End If
                    Else

                        If mintLeaveBalanceSetting = enLeaveBalanceSetting.Financial_Year Then
                            'Pinkal (28-Jan-2019) -- Start
                            'Bug - Due to Change In Get Balance Info Method report was showing 0 for all columns due to Pacra changes did by sohail for Leave Details.
                            'dsLeaveList = objLeaveBalance.GetLeaveBalanceInfo(mdtDate.Date, dtRow.Item("LeaveTypeId"), dtRow.Item("EmpId"), mintLeaveAccrueTenureSetting, mintLeaveAccrueDaysAfterEachMonth, , , False, False, mintLeaveBalanceSetting, 0, xYearUnkid)

                            'Pinkal (19-Feb-2020) -- Start
                            'ENHANCEMENT VOLTAMP [0004521]:  Calculate Issue Leave Based As on Date with configuration Setting.
                            'dsLeaveList = objLeaveBalance.GetLeaveBalanceInfo(mdtDate.Date, dtRow.Item("LeaveTypeId"), dtRow.Item("EmpId"), mintLeaveAccrueTenureSetting, mintLeaveAccrueDaysAfterEachMonth, , , False, False, mintLeaveBalanceSetting, CInt(dtRow("isclose_fy")), xYearUnkid)
                            dsLeaveList = objLeaveBalance.GetLeaveBalanceInfo(mdtDate.Date, dtRow.Item("LeaveTypeId"), dtRow.Item("EmpId"), mintLeaveAccrueTenureSetting, mintLeaveAccrueDaysAfterEachMonth, , , False, False, mintLeaveBalanceSetting, CInt(dtRow("isclose_fy")), xYearUnkid, Nothing, "", True, mblnLeaveIssueAsonDateOnLeaveBalanceListReport)
                            'Pinkal (19-Feb-2020) -- End

                            'Pinkal (28-Jan-2019) -- End

                        ElseIf mintLeaveBalanceSetting = enLeaveBalanceSetting.ELC Then
                            objLeaveBalance._DBStartdate = mdtdbStartDate
                            objLeaveBalance._DBEnddate = mdtdbEndDate

                            If CBool(dtRow.Item("isopenelc")) = False Then
                                'Pinkal (19-Feb-2020) -- Start
                                'ENHANCEMENT VOLTAMP [0004521]:  Calculate Issue Leave Based As on Date with configuration Setting.
                                'dsLeaveList = objLeaveBalance.GetLeaveBalanceInfo(eZeeDate.convertDate(dtRow.Item("EndDate").ToString()).Date, dtRow.Item("LeaveTypeId"), dtRow.Item("EmpId"), mintLeaveAccrueTenureSetting, mintLeaveAccrueDaysAfterEachMonth, , , CBool(dtRow.Item("isopenelc")), True, mintLeaveBalanceSetting, 0, xYearUnkid)
                                If mblnLeaveIssueAsonDateOnLeaveBalanceListReport Then
                                    dsLeaveList = objLeaveBalance.GetLeaveBalanceInfo(mdtDate.Date, dtRow.Item("LeaveTypeId"), dtRow.Item("EmpId"), mintLeaveAccrueTenureSetting, mintLeaveAccrueDaysAfterEachMonth, , , CBool(dtRow.Item("isopenelc")), True, mintLeaveBalanceSetting, 0, xYearUnkid, Nothing, "", True, mblnLeaveIssueAsonDateOnLeaveBalanceListReport)
                                Else
                                    dsLeaveList = objLeaveBalance.GetLeaveBalanceInfo(eZeeDate.convertDate(dtRow.Item("EndDate").ToString()).Date, dtRow.Item("LeaveTypeId"), dtRow.Item("EmpId"), mintLeaveAccrueTenureSetting, mintLeaveAccrueDaysAfterEachMonth, , , CBool(dtRow.Item("isopenelc")), True, mintLeaveBalanceSetting, 0, xYearUnkid, Nothing, "", True, mblnLeaveIssueAsonDateOnLeaveBalanceListReport)
                                End If
                                'Pinkal (19-Feb-2020) -- End
                            Else
                                'Pinkal (19-Feb-2020) -- Start
                                'ENHANCEMENT VOLTAMP [0004521]:  Calculate Issue Leave Based As on Date with configuration Setting.
                                'dsLeaveList = objLeaveBalance.GetLeaveBalanceInfo(mdtDate.Date, dtRow.Item("LeaveTypeId"), dtRow.Item("EmpId"), mintLeaveAccrueTenureSetting, mintLeaveAccrueDaysAfterEachMonth, , , CBool(dtRow.Item("isopenelc")), True, mintLeaveBalanceSetting, 0, xYearUnkid)
                                dsLeaveList = objLeaveBalance.GetLeaveBalanceInfo(mdtDate.Date, dtRow.Item("LeaveTypeId"), dtRow.Item("EmpId"), mintLeaveAccrueTenureSetting, mintLeaveAccrueDaysAfterEachMonth, , , CBool(dtRow.Item("isopenelc")), True, mintLeaveBalanceSetting, 0, xYearUnkid, Nothing, "", True, mblnLeaveIssueAsonDateOnLeaveBalanceListReport)
                                'Pinkal (19-Feb-2020) -- End
                            End If

                        End If
                    End If

                    If dsLeaveList.Tables(0).Rows.Count > 0 Then
                        rpt_Rows.Item("Column8") = CDec(dsLeaveList.Tables(0).Rows(0).Item("Accrue_amount")).ToString("#0.00")
                        rpt_Rows.Item("Column9") = CDec(dsLeaveList.Tables(0).Rows(0).Item("Issue_amount")).ToString("#0.00")
                        rpt_Rows.Item("Column10") = CDec(dsLeaveList.Tables(0).Rows(0).Item("Balance")).ToString("#0.00")
                        rpt_Rows.Item("Column18") = CDec(dsLeaveList.Tables(0).Rows(0).Item("LeaveEncashment")).ToString("#0.00")
                    Else

                        rpt_Rows.Item("Column8") = Math.Round(0, 2)
                        rpt_Rows.Item("Column9") = Math.Round(0, 2)
                        rpt_Rows.Item("Column10") = Math.Round(0, 2)
                        rpt_Rows.Item("Column18") = Math.Round(0, 2)
                    End If

                Else

                    rpt_Rows.Item("Column8") = CDec(dtRow.Item("actualamount")).ToString("#0.00")

                    'Pinkal (19-Feb-2020) -- Start
                    'ENHANCEMENT VOLTAMP [0004521]:  Calculate Issue Leave Based As on Date with configuration Setting.
                    If mblnLeaveIssueAsonDateOnLeaveBalanceListReport Then
                        Dim objBalance As New clsleavebalance_tran
                        Dim dsShortLeaveList As DataSet = objBalance.GetLeaveBalanceInfo(mdtDate.Date, dtRow.Item("LeaveTypeId"), dtRow.Item("EmpId"), mintLeaveAccrueTenureSetting, mintLeaveAccrueDaysAfterEachMonth, , , False, False, mintLeaveBalanceSetting, CInt(dtRow("isclose_fy")), xYearUnkid, Nothing, "", True, mblnLeaveIssueAsonDateOnLeaveBalanceListReport)
                        objBalance = Nothing

                        If dsShortLeaveList IsNot Nothing AndAlso dsShortLeaveList.Tables(0).Rows.Count > 0 Then
                            rpt_Rows.Item("Column9") = CDec(dsShortLeaveList.Tables(0).Rows(0).Item("Issue_amount")).ToString("#0.00")
                            rpt_Rows.Item("Column10") = CDec(dsShortLeaveList.Tables(0).Rows(0).Item("Balance")).ToString("#0.00")
                        Else
                            rpt_Rows.Item("Column9") = CDec(dtRow.Item("ThisYrissueamt")).ToString("#0.00")
                            rpt_Rows.Item("Column10") = CDec(dtRow.Item("ThisYrremainingBal")).ToString("#0.00")
                        End If
                    Else
                        rpt_Rows.Item("Column9") = CDec(dtRow.Item("ThisYrissueamt")).ToString("#0.00")
                        rpt_Rows.Item("Column10") = CDec(dtRow.Item("ThisYrremainingBal")).ToString("#0.00")
                    End If
                    'Pinkal (19-Feb-2020) -- End

                    rpt_Rows.Item("Column18") = Math.Round(0, 2)

                End If

                rpt_Rows.Item("Column15") = dtRow.Item("GName")
                rpt_Rows.Item("Column16") = eZeeDate.convertDate(dtRow.Item("appointeddate").ToString()).ToShortDateString
                rpt_Rows.Item("Column17") = CDec(dtRow.Item("adjustment")).ToString("#0.00")

                rpt_Data.Tables("ArutiTable").Rows.Add(rpt_Rows)
            Next

            objRpt = New ArutiReport.Designer.rptEmployeeLeaveBalanceList

            ConfigParameter._Object.GetReportSettings(CInt(mstrReportId))
            Dim arrImageRow As DataRow = Nothing
            arrImageRow = rpt_Data.Tables("ArutiImage").NewRow()

            ReportFunction.Logo_Display(objRpt, _
                                        ConfigParameter._Object._IsDisplayLogo, _
                                        ConfigParameter._Object._ShowLogoRightSide, _
                                        "arutiLogo1", _
                                        "arutiLogo2", _
                                        arrImageRow, _
                                        "txtCompanyName", _
                                        "txtReportName", _
                                        "txtFilterDescription", _
                                        ConfigParameter._Object._GetLeftMargin, _
                                        ConfigParameter._Object._GetRightMargin)

            rpt_Data.Tables("ArutiImage").Rows.Add(arrImageRow)

            If rpt_Data.Tables("ArutiTable").Rows.Count <= 0 Then
                rpt_Data.Tables("ArutiTable").Rows.Add("")
            End If

            If ConfigParameter._Object._IsShowPreparedBy = True Then
                Call ReportFunction.TextChange(objRpt, "lblPreparedBy", Language.getMessage(mstrModuleName, 23, "Prepared By :"))
                Call ReportFunction.TextChange(objRpt, "txtUPreparedBy", User._Object._Username)
            Else
                Call ReportFunction.EnableSuppressSection(objRpt, "ReportFooterSection1", True)
            End If

            If ConfigParameter._Object._IsShowCheckedBy = True Then
                Call ReportFunction.TextChange(objRpt, "lblCheckedBy", Language.getMessage(mstrModuleName, 24, "Checked By :"))
            Else
                Call ReportFunction.EnableSuppressSection(objRpt, "ReportFooterSection2", True)
            End If

            If ConfigParameter._Object._IsShowApprovedBy = True Then
                Call ReportFunction.TextChange(objRpt, "lblApprovedBy", Language.getMessage(mstrModuleName, 25, "Approved By :"))
            Else
                Call ReportFunction.EnableSuppressSection(objRpt, "ReportFooterSection3", True)
            End If

            If ConfigParameter._Object._IsShowReceivedBy = True Then
                Call ReportFunction.TextChange(objRpt, "lblReceivedBy", Language.getMessage(mstrModuleName, 26, "Received By :"))
            Else
                Call ReportFunction.EnableSuppressSection(objRpt, "ReportFooterSection4", True)
            End If

            objRpt.SetDataSource(rpt_Data)

            Select Case mintReportId
                Case 0
                    ReportFunction.EnableSuppressSection(objRpt, "GroupHeaderSection1", True)
                    ReportFunction.EnableSuppressSection(objRpt, "GroupFooterSection1", True)

                    Call ReportFunction.EnableSuppress(objRpt, "txtEmpCode", True)
                    Call ReportFunction.EnableSuppress(objRpt, "txtEmpName", True)

                    Call ReportFunction.TextChange(objRpt, "txtLeaveTypeCode", Language.getMessage(mstrModuleName, 9, "Code :"))
                    Call ReportFunction.TextChange(objRpt, "txtLeaveTypeName", Language.getMessage(mstrModuleName, 10, "Leave :"))

                    Call ReportFunction.TextChange(objRpt, "txtColumn14", Language.getMessage(mstrModuleName, 12, "Emp Code"))
                    Call ReportFunction.TextChange(objRpt, "txtColumn15", Language.getMessage(mstrModuleName, 2, "Employee"))

                    Call ReportFunction.TextChange(objRpt, "txtGroupTotal", Language.getMessage(mstrModuleName, 19, "Group Total :"))

                    Call ReportFunction.EnableSuppress(objRpt, "grp3Col71", True)
                    Call ReportFunction.EnableSuppress(objRpt, "grp3Col81", True)
                    Call ReportFunction.EnableSuppress(objRpt, "grp3Col91", True)
                    Call ReportFunction.EnableSuppress(objRpt, "grp3Balance1", True)

                Case 1
                    ReportFunction.EnableSuppressSection(objRpt, "GroupHeaderSection2", True)
                    ReportFunction.EnableSuppressSection(objRpt, "GroupFooterSection2", True)

                    Call ReportFunction.EnableSuppress(objRpt, "txtLeaveTypeCode", True)
                    Call ReportFunction.EnableSuppress(objRpt, "txtLeaveTypeName", True)

                    Call ReportFunction.TextChange(objRpt, "txtEmpCode", Language.getMessage(mstrModuleName, 9, "Code :"))
                    Call ReportFunction.TextChange(objRpt, "txtEmpName", Language.getMessage(mstrModuleName, 11, "Employee :"))

                    Call ReportFunction.TextChange(objRpt, "txtColumn14", Language.getMessage(mstrModuleName, 3, "Leave Code"))
                    Call ReportFunction.TextChange(objRpt, "txtColumn15", Language.getMessage(mstrModuleName, 4, "Leave Type"))

                    Call ReportFunction.TextChange(objRpt, "txtGroupTotal1", Language.getMessage(mstrModuleName, 19, "Group Total :"))

                    Call ReportFunction.EnableSuppress(objRpt, "grp4Col71", True)
                    Call ReportFunction.EnableSuppress(objRpt, "grp4Col81", True)
                    Call ReportFunction.EnableSuppress(objRpt, "grp4Col91", True)
                    Call ReportFunction.EnableSuppress(objRpt, "grp4Balance1", True)

            End Select

            Call ReportFunction.TextChange(objRpt, "txtLeaveBF", Language.getMessage(mstrModuleName, 5, "Leave B/F"))
            Call ReportFunction.TextChange(objRpt, "txtTotalAccruedYearToDate", Language.getMessage(mstrModuleName, 6, "Total Accrued Year To Date"))
            Call ReportFunction.TextChange(objRpt, "txtTotalIssuedYearToDate", Language.getMessage(mstrModuleName, 7, "Total Issued Year To Date"))
            Call ReportFunction.TextChange(objRpt, "txtBalance", Language.getMessage(mstrModuleName, 8, "Balance Year To Date"))
            Call ReportFunction.TextChange(objRpt, "txtStartDate", Language.getMessage(mstrModuleName, 21, "Start Date"))
            Call ReportFunction.TextChange(objRpt, "txtEndDate", Language.getMessage(mstrModuleName, 22, "End Date"))
            Call ReportFunction.TextChange(objRpt, "txtGrandTotal", Language.getMessage(mstrModuleName, 20, "Grand Total :"))
            Call ReportFunction.TextChange(objRpt, "lblPrintedBy", Language.getMessage(mstrModuleName, 13, "Printed By :"))
            Call ReportFunction.TextChange(objRpt, "lblPrintedDate", Language.getMessage(mstrModuleName, 14, "Printed Date :"))
            Call ReportFunction.TextChange(objRpt, "txtJoiningDate", Language.getMessage(mstrModuleName, 36, "Joining Date"))

            'Pinkal (29-May-2015) -- Start
            'Enhancement - LEAVE ADJUSTMENT CHANGES REQUIRED BY DENNIS.
            Call ReportFunction.TextChange(objRpt, "txtLeaveAdjustment", Language.getMessage(mstrModuleName, 37, "Leave Adjustment"))
            'Pinkal (29-May-2015) -- End


            'Pinkal (18-Aug-2015) -- Start
            'Enhancement - WORKING ON LEAVE ENCASHMENT DEDUCTION IN LEAVE MODULE.
            Call ReportFunction.TextChange(objRpt, "txtEncashment", Language.getMessage(mstrModuleName, 38, "Encashment"))


            If mblnIncludeLeaveEncashment = False Then
                Call ReportFunction.EnableSuppress(objRpt, "txtEncashment", True)
                Call ReportFunction.EnableSuppress(objRpt, "Column181", True)
                Call ReportFunction.EnableSuppress(objRpt, "grp4Col181", True)
                Call ReportFunction.EnableSuppress(objRpt, "grp3Col181", True)
                Call ReportFunction.EnableSuppress(objRpt, "grp15Col181", True)
                Call ReportFunction.EnableSuppress(objRpt, "txtCol181", True)
            End If

            'Pinkal (18-Aug-2015) -- End


            Call ReportFunction.TextChange(objRpt, "txtPrintedBy", Me._UserName)
            Call ReportFunction.TextChange(objRpt, "txtPrintedDate", Me._PrintDate)
            Call ReportFunction.TextChange(objRpt, "txtReportName", Me._ReportName & " " & "[" & mstrReportTypeName & "]")
            Call ReportFunction.TextChange(objRpt, "txtCompanyName", Me._CompanyName)
            Call ReportFunction.TextChange(objRpt, "txtFilterDescription", Me._FilterTitle)

            If mstrReport_GroupName.Trim.Length > 0 Then
                Call ReportFunction.TextChange(objRpt, "txtGroupName", mstrReport_GroupName)
                Call ReportFunction.TextChange(objRpt, "txtAllocationTotal", mstrReport_GroupName.Trim.Replace(":", "") & Language.getMessage(mstrModuleName, 27, " Total :"))
            Else
                ReportFunction.EnableSuppressSection(objRpt, "GroupHeaderSection3", True)
                ReportFunction.EnableSuppressSection(objRpt, "GroupFooterSection3", True)
            End If

            Return objRpt
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: Generate_DetailReport; Module Name: " & mstrModuleName)
            Return Nothing
        End Try
    End Function

    'Pinkal (24-Aug-2015) -- End

#End Region

#Region "Employee Without Leave Accrue"

    Dim iColumn_EmpWOAccrueReport As New IColumnCollection

    Public Property Field_OnDetailEmpWOAccrueReport() As IColumnCollection
        Get
            Return iColumn_EmpWOAccrueReport
        End Get
        Set(ByVal value As IColumnCollection)
            iColumn_EmpWOAccrueReport = value
        End Set
    End Property

    Private Sub Create_OnDetailEmpWOAccrueReport()
        Try
            iColumn_EmpWOAccrueReport.Clear()
            iColumn_EmpWOAccrueReport.Add(New IColumn("EmpCode", Language.getMessage(mstrModuleName, 1, "Code")))
            iColumn_EmpWOAccrueReport.Add(New IColumn("EmpName", Language.getMessage(mstrModuleName, 2, "Employee")))
            iColumn_EmpWOAccrueReport.Add(New IColumn("Department", Language.getMessage(mstrModuleName, 33, "Department")))
            iColumn_EmpWOAccrueReport.Add(New IColumn("Job", Language.getMessage(mstrModuleName, 34, "Job")))
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: Create_OnDetailEmpWOAccrueReport; Module Name: " & mstrModuleName)
        End Try
    End Sub


    'Pinkal (24-Aug-2015) -- Start
    'Enhancement - WORKING ON ACTIVE EMPLOYEE CONDITION.

    'Private Function Generate_DetailEmpWOAccrueReport() As CrystalDecisions.CrystalReports.Engine.ReportClass
    '    Dim objRpt As CrystalDecisions.CrystalReports.Engine.ReportClass = Nothing
    '    Dim StrQ As String = ""
    '    Dim dsList As New DataSet
    '    Dim dsLeaveType As DataSet = Nothing
    '    Dim rpt_Data As ArutiReport.Designer.dsArutiReport
    '    Try
    '        objDataOperation = New clsDataOperation


    '        StrQ = " SELECT  hremployee_master.employeeunkid AS EmpId " & _
    '                   ", hremployee_master.employeecode AS EmpCode " & _
    '                   ", ISNULL(hremployee_master.firstname, '') + ' ' + ISNULL(hremployee_master.othername, '') + ' ' + ISNULL(hremployee_master.surname, '') AS EmpName " & _
    '                   ", gender " & _
    '                   ",ISNULL(Dept.name,'') AS Department " & _
    '                   ",ISNULL(Job.job_name,'') AS Job "



    '        If mintViewIndex > 0 Then
    '            StrQ &= mstrAnalysis_Fields
    '        Else
    '            StrQ &= ", 0 AS Id, '' AS GName "
    '        End If


    '        StrQ &= " FROM hremployee_master " & _
    '                     " JOIN hrjob_master AS Job ON hremployee_master.jobunkid = Job.jobunkid " & _
    '                     " JOIN hrdepartment_master AS Dept ON hremployee_master.departmentunkid = Dept.departmentunkid "

    '        StrQ &= mstrAnalysis_Join

    '        StrQ &= " WHERE  hremployee_master.isapproved = 1  "

    '        If mstrAdvance_Filter.Trim.Length > 0 Then
    '            StrQ &= " AND " & mstrAdvance_Filter
    '        End If

    '        If mblnIncludeInactiveEmp = False Then
    '            StrQ &= " AND CONVERT(CHAR(8),hremployee_master.appointeddate,112) <= @enddate " & _
    '                " AND ISNULL(CONVERT(CHAR(8),hremployee_master.termination_from_date,112),@startdate) >= @startdate " & _
    '                " AND ISNULL(CONVERT(CHAR(8),hremployee_master.termination_to_date,112),@startdate) >= @startdate " & _
    '                " AND ISNULL(CONVERT(CHAR(8), hremployee_master.empl_enddate,112), @startdate) >= @startdate "
    '        End If



    '        'Pinkal (01-Feb-2014) -- Start
    '        'Enhancement : TRA Changes

    '        'If UserAccessLevel._AccessLevel.Length > 0 Then
    '        '    StrQ &= UserAccessLevel._AccessLevelFilterString
    '        'End If

    '        If mstrUserAccessFilter.Trim.Length <= 0 Then
    '            mstrUserAccessFilter = UserAccessLevel._AccessLevelFilterString
    '        End If

    '        If mstrUserAccessFilter.Trim.Length > 0 Then
    '            StrQ &= mstrUserAccessFilter
    '        End If

    '        'Pinkal (01-Feb-2014) -- End

    '        Call FilterTitleAndFilterQuery()

    '        StrQ &= Me._FilterQuery

    '        StrQ &= mstrOrderByQuery

    '        dsList = objDataOperation.ExecQuery(StrQ, "DataTable")

    '        If objDataOperation.ErrorMessage <> "" Then
    '            Throw New Exception(objDataOperation.ErrorNumber & ":" & objDataOperation.ErrorMessage)
    '        End If

    '        rpt_Data = New ArutiReport.Designer.dsArutiReport

    '        Dim objLeaveBal As New clsleavebalance_tran
    '        Dim objLeaveType As New clsleavetype_master
    '        dsLeaveType = objLeaveType.GetList("List", True)
    '        Dim dtLeaveType As DataTable = New DataView(dsLeaveType.Tables(0), "leavetypeunkid in (" & mstrLeaveTypeIds & ")", "", DataViewRowState.CurrentRows).ToTable

    '        For Each dtRow As DataRow In dsList.Tables("DataTable").Rows
    '            Dim rpt_Rows As DataRow

    '            For i As Integer = 0 To dtLeaveType.Rows.Count - 1

    '                If CInt(dtLeaveType.Rows(i)("gender")) > 0 AndAlso CInt(dtRow.Item("gender")) <> CInt(dtLeaveType.Rows(i)("gender")) Then Continue For

    '                Dim dtTable As DataTable = objLeaveBal.GetEmployeeBalanceForLeaveType(CInt(dtRow.Item("EmpId")), CInt(dtLeaveType.Rows(i)("leavetypeunkid")), FinancialYear._Object._YearUnkid)

    '                If dtTable IsNot Nothing AndAlso dtTable.Rows.Count <= 0 Then
    '                    rpt_Rows = rpt_Data.Tables("ArutiTable").NewRow
    '                    rpt_Rows.Item("Column1") = dtRow.Item("GName")
    '                    rpt_Rows.Item("Column2") = dtLeaveType.Rows(i)("leavetypecode").ToString()
    '                    rpt_Rows.Item("Column3") = dtLeaveType.Rows(i)("leavename").ToString()
    '                    rpt_Rows.Item("Column4") = dtRow.Item("EmpCode")
    '                    rpt_Rows.Item("Column5") = dtRow.Item("EmpName")
    '                    rpt_Rows.Item("Column6") = dtRow.Item("Department")
    '                    rpt_Rows.Item("Column7") = dtRow.Item("Job")
    '                    rpt_Rows.Item("Column8") = dtRow.Item("EmpId")
    '                    rpt_Data.Tables("ArutiTable").Rows.Add(rpt_Rows)
    '                End If

    '            Next

    '        Next

    '        objRpt = New ArutiReport.Designer.rptEmpWOLeaveAccrueReport

    '        ConfigParameter._Object.GetReportSettings(CInt(mstrReportId))
    '        Dim arrImageRow As DataRow = Nothing
    '        arrImageRow = rpt_Data.Tables("ArutiImage").NewRow()

    '        ReportFunction.Logo_Display(objRpt, _
    '                                    ConfigParameter._Object._IsDisplayLogo, _
    '                                    ConfigParameter._Object._ShowLogoRightSide, _
    '                                    "arutiLogo1", _
    '                                    "arutiLogo2", _
    '                                    arrImageRow, _
    '                                    "txtCompanyName", _
    '                                    "txtReportName", _
    '                                    "txtFilterDescription", _
    '                                    ConfigParameter._Object._GetLeftMargin, _
    '                                    ConfigParameter._Object._GetRightMargin)

    '        rpt_Data.Tables("ArutiImage").Rows.Add(arrImageRow)

    '        If rpt_Data.Tables("ArutiTable").Rows.Count <= 0 Then
    '            rpt_Data.Tables("ArutiTable").Rows.Add("")
    '        End If

    '        If ConfigParameter._Object._IsShowPreparedBy = True Then
    '            Call ReportFunction.TextChange(objRpt, "lblPreparedBy", Language.getMessage(mstrModuleName, 23, "Prepared By :"))
    '            Call ReportFunction.TextChange(objRpt, "txtUPreparedBy", User._Object._Username)
    '        Else
    '            Call ReportFunction.EnableSuppressSection(objRpt, "ReportFooterSection1", True)
    '        End If

    '        If ConfigParameter._Object._IsShowCheckedBy = True Then
    '            Call ReportFunction.TextChange(objRpt, "lblCheckedBy", Language.getMessage(mstrModuleName, 24, "Checked By :"))
    '        Else
    '            Call ReportFunction.EnableSuppressSection(objRpt, "ReportFooterSection2", True)
    '        End If

    '        If ConfigParameter._Object._IsShowApprovedBy = True Then
    '            Call ReportFunction.TextChange(objRpt, "lblApprovedBy", Language.getMessage(mstrModuleName, 25, "Approved By :"))
    '        Else
    '            Call ReportFunction.EnableSuppressSection(objRpt, "ReportFooterSection3", True)
    '        End If

    '        If ConfigParameter._Object._IsShowReceivedBy = True Then
    '            Call ReportFunction.TextChange(objRpt, "lblReceivedBy", Language.getMessage(mstrModuleName, 26, "Received By :"))
    '        Else
    '            Call ReportFunction.EnableSuppressSection(objRpt, "ReportFooterSection4", True)
    '        End If

    '        objRpt.SetDataSource(rpt_Data)

    '        Call ReportFunction.TextChange(objRpt, "txtEmpCode", Language.getMessage(mstrModuleName, 1, "Code"))
    '        Call ReportFunction.TextChange(objRpt, "txtEmpName", Language.getMessage(mstrModuleName, 2, "Employee"))
    '        Call ReportFunction.TextChange(objRpt, "txtDepartment", Language.getMessage(mstrModuleName, 28, "Department"))
    '        Call ReportFunction.TextChange(objRpt, "txtJob", Language.getMessage(mstrModuleName, 29, "Job Title"))
    '        Call ReportFunction.TextChange(objRpt, "txtLeaveCode", Language.getMessage(mstrModuleName, 30, "Leave Code :"))
    '        Call ReportFunction.TextChange(objRpt, "txtLeaveType", Language.getMessage(mstrModuleName, 31, "Leave Type :"))
    '        Call ReportFunction.TextChange(objRpt, "txtSubCount", Language.getMessage(mstrModuleName, 32, "Sub Count : "))

    '        Call ReportFunction.TextChange(objRpt, "lblPrintedBy", Language.getMessage(mstrModuleName, 13, "Printed By :"))
    '        Call ReportFunction.TextChange(objRpt, "lblPrintedDate", Language.getMessage(mstrModuleName, 14, "Printed Date :"))

    '        Call ReportFunction.TextChange(objRpt, "txtPrintedBy", Me._UserName)
    '        Call ReportFunction.TextChange(objRpt, "txtPrintedDate", Me._PrintDate)
    '        Call ReportFunction.TextChange(objRpt, "txtReportName", mstrReportTypeName)
    '        Call ReportFunction.TextChange(objRpt, "txtCompanyName", Me._CompanyName)
    '        Call ReportFunction.TextChange(objRpt, "txtFilterDescription", Me._FilterTitle)

    '        If mstrReport_GroupName.Trim.Length > 0 Then
    '            Call ReportFunction.TextChange(objRpt, "txtGroupName", mstrReport_GroupName)
    '            Call ReportFunction.TextChange(objRpt, "txtAllocationCount", mstrReport_GroupName.Trim.Replace(":", "") & Language.getMessage(mstrModuleName, 35, " Count :"))
    '        End If

    '        Return objRpt
    '    Catch ex As Exception
    '        Throw New Exception(ex.Message & "; Procedure Name: Generate_DetailEmpWOAccrueReport; Module Name: " & mstrModuleName)
    '        Return Nothing
    '    End Try
    'End Function

    Private Function Generate_DetailEmpWOAccrueReport(ByVal xDatabaseName As String, ByVal xUserUnkid As Integer, ByVal xYearUnkid As Integer, ByVal xCompanyUnkid As Integer _
                                                                , ByVal xEmployeeAsonDate As Date, ByVal xUserModeSetting As String, ByVal xOnlyApproved As Boolean) As CrystalDecisions.CrystalReports.Engine.ReportClass
        Dim objRpt As CrystalDecisions.CrystalReports.Engine.ReportClass = Nothing
        Dim StrQ As String = ""
        Dim dsList As New DataSet
        Dim dsLeaveType As DataSet = Nothing
        Dim rpt_Data As ArutiReport.Designer.dsArutiReport
        Try
            objDataOperation = New clsDataOperation


            Dim xDateJoinQry, xDateFilterQry, xUACQry, xUACFiltrQry, xAdvanceJoinQry As String
            xDateJoinQry = "" : xDateFilterQry = "" : xUACQry = "" : xUACFiltrQry = "" : xAdvanceJoinQry = ""
            Call GetDatesFilterString(xDateJoinQry, xDateFilterQry, xEmployeeAsonDate, xEmployeeAsonDate, , , xDatabaseName)
            If mblnIncludeAccessFilterQry Then Call NewAccessLevelFilterString(xUACQry, xUACFiltrQry, xEmployeeAsonDate, xOnlyApproved, xDatabaseName, xUserUnkid, xCompanyUnkid, xYearUnkid, xUserModeSetting)
            Call GetAdvanceFilterQry(xAdvanceJoinQry, xEmployeeAsonDate, xDatabaseName)


            StrQ = " SELECT  hremployee_master.employeeunkid AS EmpId " & _
                       ", hremployee_master.employeecode AS EmpCode " & _
                       ", ISNULL(hremployee_master.firstname, '') + ' ' + ISNULL(hremployee_master.othername, '') + ' ' + ISNULL(hremployee_master.surname, '') AS EmpName " & _
                       ", gender " & _
                       ",ISNULL(Dept.name,'') AS Department " & _
                       ",ISNULL(Job.job_name,'') AS Job "



            If mintViewIndex > 0 Then
                StrQ &= mstrAnalysis_Fields
            Else
                StrQ &= ", 0 AS Id, '' AS GName "
            End If


            StrQ &= " FROM hremployee_master " & _
                         " JOIN " & _
                         " ( " & _
                         "         SELECT " & _
                         "         jobunkid " & _
                         "        ,employeeunkid " & _
                         "        ,ROW_NUMBER()OVER(PARTITION BY employeeunkid ORDER BY effectivedate DESC) AS rno " & _
                         "    FROM hremployee_categorization_tran " & _
                         "    WHERE isvoid = 0 AND CONVERT(CHAR(8),effectivedate,112) <= '" & eZeeDate.convertDate(xEmployeeAsonDate) & "' " & _
                         " ) AS Jobs ON Jobs.employeeunkid = hremployee_master.employeeunkid AND Jobs.rno = 1 " & _
                         " JOIN hrjob_master AS Job ON Job.jobunkid = Jobs.jobunkid " & _
                         " JOIN " & _
                                                        " ( " & _
                                                        "    SELECT " & _
                                                        "         departmentunkid " & _
                                                        "        ,employeeunkid " & _
                                                        "        ,ROW_NUMBER()OVER(PARTITION BY employeeunkid ORDER BY effectivedate DESC) AS rno " & _
                                                        "    FROM hremployee_transfer_tran " & _
                                                        "    WHERE isvoid = 0 AND CONVERT(CHAR(8),effectivedate,112) <= '" & eZeeDate.convertDate(xEmployeeAsonDate) & "' " & _
                                                        " ) AS Alloc ON Alloc.employeeunkid = hremployee_master.employeeunkid AND Alloc.rno = 1 " & _
                        " JOIN hrdepartment_master AS Dept ON Dept.departmentunkid = Alloc.departmentunkid "



            StrQ &= mstrAnalysis_Join

            If xDateJoinQry.Trim.Length > 0 Then
                StrQ &= xDateJoinQry
            End If

            If xUACQry.Trim.Length > 0 Then
                StrQ &= xUACQry
            End If

            If xAdvanceJoinQry.Trim.Length > 0 Then
                StrQ &= xAdvanceJoinQry
            End If

            StrQ &= " WHERE  hremployee_master.isapproved = 1  "

            If mstrAdvance_Filter.Trim.Length > 0 Then
                StrQ &= " AND " & mstrAdvance_Filter
            End If

            If xUACFiltrQry.Trim.Length > 0 Then
                StrQ &= " AND " & xUACFiltrQry
            End If

            If mblnIncludeInactiveEmp = False Then
                If xDateFilterQry.Trim.Length > 0 Then
                    StrQ &= xDateFilterQry
                End If
            End If


            'If mblnIncludeInactiveEmp = False Then
            '    StrQ &= " AND CONVERT(CHAR(8),hremployee_master.appointeddate,112) <= @enddate " & _
            '        " AND ISNULL(CONVERT(CHAR(8),hremployee_master.termination_from_date,112),@startdate) >= @startdate " & _
            '        " AND ISNULL(CONVERT(CHAR(8),hremployee_master.termination_to_date,112),@startdate) >= @startdate " & _
            '        " AND ISNULL(CONVERT(CHAR(8), hremployee_master.empl_enddate,112), @startdate) >= @startdate "
            'End If



            'Pinkal (01-Feb-2014) -- Start
            'Enhancement : TRA Changes

            'If UserAccessLevel._AccessLevel.Length > 0 Then
            '    StrQ &= UserAccessLevel._AccessLevelFilterString
            'End If

            'If mstrUserAccessFilter.Trim.Length <= 0 Then
            '    mstrUserAccessFilter = UserAccessLevel._AccessLevelFilterString
            'End If

            'If mstrUserAccessFilter.Trim.Length > 0 Then
            '    StrQ &= mstrUserAccessFilter
            'End If

            'Pinkal (01-Feb-2014) -- End

            Call FilterTitleAndFilterQuery()

            StrQ &= Me._FilterQuery

            StrQ &= mstrOrderByQuery

            dsList = objDataOperation.ExecQuery(StrQ, "DataTable")

            If objDataOperation.ErrorMessage <> "" Then
                Throw New Exception(objDataOperation.ErrorNumber & ":" & objDataOperation.ErrorMessage)
            End If

            rpt_Data = New ArutiReport.Designer.dsArutiReport

            Dim objLeaveBal As New clsleavebalance_tran
            Dim objLeaveType As New clsleavetype_master
            dsLeaveType = objLeaveType.GetList("List", True)
            Dim dtLeaveType As DataTable = New DataView(dsLeaveType.Tables(0), "leavetypeunkid in (" & mstrLeaveTypeIds & ")", "", DataViewRowState.CurrentRows).ToTable

            For Each dtRow As DataRow In dsList.Tables("DataTable").Rows
                Dim rpt_Rows As DataRow

                For i As Integer = 0 To dtLeaveType.Rows.Count - 1

                    If CInt(dtLeaveType.Rows(i)("gender")) > 0 AndAlso CInt(dtRow.Item("gender")) <> CInt(dtLeaveType.Rows(i)("gender")) Then Continue For

                    Dim dtTable As DataTable = objLeaveBal.GetEmployeeBalanceForLeaveType(CInt(dtRow.Item("EmpId")), CInt(dtLeaveType.Rows(i)("leavetypeunkid")), FinancialYear._Object._YearUnkid)

                    If dtTable IsNot Nothing AndAlso dtTable.Rows.Count <= 0 Then
                        rpt_Rows = rpt_Data.Tables("ArutiTable").NewRow
                        rpt_Rows.Item("Column1") = dtRow.Item("GName")
                        rpt_Rows.Item("Column2") = dtLeaveType.Rows(i)("leavetypecode").ToString()
                        rpt_Rows.Item("Column3") = dtLeaveType.Rows(i)("leavename").ToString()
                        rpt_Rows.Item("Column4") = dtRow.Item("EmpCode")
                        rpt_Rows.Item("Column5") = dtRow.Item("EmpName")
                        rpt_Rows.Item("Column6") = dtRow.Item("Department")
                        rpt_Rows.Item("Column7") = dtRow.Item("Job")
                        rpt_Rows.Item("Column8") = dtRow.Item("EmpId")
                        rpt_Data.Tables("ArutiTable").Rows.Add(rpt_Rows)
                    End If

                Next

            Next

            objRpt = New ArutiReport.Designer.rptEmpWOLeaveAccrueReport

            ConfigParameter._Object.GetReportSettings(CInt(mstrReportId))
            Dim arrImageRow As DataRow = Nothing
            arrImageRow = rpt_Data.Tables("ArutiImage").NewRow()

            ReportFunction.Logo_Display(objRpt, _
                                        ConfigParameter._Object._IsDisplayLogo, _
                                        ConfigParameter._Object._ShowLogoRightSide, _
                                        "arutiLogo1", _
                                        "arutiLogo2", _
                                        arrImageRow, _
                                        "txtCompanyName", _
                                        "txtReportName", _
                                        "txtFilterDescription", _
                                        ConfigParameter._Object._GetLeftMargin, _
                                        ConfigParameter._Object._GetRightMargin)

            rpt_Data.Tables("ArutiImage").Rows.Add(arrImageRow)

            If rpt_Data.Tables("ArutiTable").Rows.Count <= 0 Then
                rpt_Data.Tables("ArutiTable").Rows.Add("")
            End If

            If ConfigParameter._Object._IsShowPreparedBy = True Then
                Call ReportFunction.TextChange(objRpt, "lblPreparedBy", Language.getMessage(mstrModuleName, 23, "Prepared By :"))
                Call ReportFunction.TextChange(objRpt, "txtUPreparedBy", User._Object._Username)
            Else
                Call ReportFunction.EnableSuppressSection(objRpt, "ReportFooterSection1", True)
            End If

            If ConfigParameter._Object._IsShowCheckedBy = True Then
                Call ReportFunction.TextChange(objRpt, "lblCheckedBy", Language.getMessage(mstrModuleName, 24, "Checked By :"))
            Else
                Call ReportFunction.EnableSuppressSection(objRpt, "ReportFooterSection2", True)
            End If

            If ConfigParameter._Object._IsShowApprovedBy = True Then
                Call ReportFunction.TextChange(objRpt, "lblApprovedBy", Language.getMessage(mstrModuleName, 25, "Approved By :"))
            Else
                Call ReportFunction.EnableSuppressSection(objRpt, "ReportFooterSection3", True)
            End If

            If ConfigParameter._Object._IsShowReceivedBy = True Then
                Call ReportFunction.TextChange(objRpt, "lblReceivedBy", Language.getMessage(mstrModuleName, 26, "Received By :"))
            Else
                Call ReportFunction.EnableSuppressSection(objRpt, "ReportFooterSection4", True)
            End If

            objRpt.SetDataSource(rpt_Data)

            Call ReportFunction.TextChange(objRpt, "txtEmpCode", Language.getMessage(mstrModuleName, 1, "Code"))
            Call ReportFunction.TextChange(objRpt, "txtEmpName", Language.getMessage(mstrModuleName, 2, "Employee"))
            Call ReportFunction.TextChange(objRpt, "txtDepartment", Language.getMessage(mstrModuleName, 28, "Department"))
            Call ReportFunction.TextChange(objRpt, "txtJob", Language.getMessage(mstrModuleName, 29, "Job Title"))
            Call ReportFunction.TextChange(objRpt, "txtLeaveCode", Language.getMessage(mstrModuleName, 30, "Leave Code :"))
            Call ReportFunction.TextChange(objRpt, "txtLeaveType", Language.getMessage(mstrModuleName, 31, "Leave Type :"))
            Call ReportFunction.TextChange(objRpt, "txtSubCount", Language.getMessage(mstrModuleName, 32, "Sub Count : "))

            Call ReportFunction.TextChange(objRpt, "lblPrintedBy", Language.getMessage(mstrModuleName, 13, "Printed By :"))
            Call ReportFunction.TextChange(objRpt, "lblPrintedDate", Language.getMessage(mstrModuleName, 14, "Printed Date :"))

            Call ReportFunction.TextChange(objRpt, "txtPrintedBy", Me._UserName)
            Call ReportFunction.TextChange(objRpt, "txtPrintedDate", Me._PrintDate)
            Call ReportFunction.TextChange(objRpt, "txtReportName", mstrReportTypeName)
            Call ReportFunction.TextChange(objRpt, "txtCompanyName", Me._CompanyName)
            Call ReportFunction.TextChange(objRpt, "txtFilterDescription", Me._FilterTitle)

            If mstrReport_GroupName.Trim.Length > 0 Then
                Call ReportFunction.TextChange(objRpt, "txtGroupName", mstrReport_GroupName)
                Call ReportFunction.TextChange(objRpt, "txtAllocationCount", mstrReport_GroupName.Trim.Replace(":", "") & Language.getMessage(mstrModuleName, 35, " Count :"))
            End If

            Return objRpt
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: Generate_DetailEmpWOAccrueReport; Module Name: " & mstrModuleName)
            Return Nothing
        End Try
    End Function

    'Pinkal (24-Aug-2015) -- End

#End Region


#End Region


    '<Language> This Auto Generated Text Please Do Not Modify it.
#Region " Language & UI Settings "
    Public Shared Sub SetMessages()
        Try
            Language.setMessage(mstrModuleName, 1, "Code")
            Language.setMessage(mstrModuleName, 2, "Employee")
            Language.setMessage(mstrModuleName, 3, "Leave Code")
            Language.setMessage(mstrModuleName, 4, "Leave Type")
            Language.setMessage(mstrModuleName, 5, "Leave B/F")
            Language.setMessage(mstrModuleName, 6, "Total Accrued Year To Date")
            Language.setMessage(mstrModuleName, 7, "Total Issued Year To Date")
            Language.setMessage(mstrModuleName, 8, "Balance Year To Date")
            Language.setMessage(mstrModuleName, 9, "Code :")
            Language.setMessage(mstrModuleName, 10, "Leave :")
            Language.setMessage(mstrModuleName, 11, "Employee :")
            Language.setMessage(mstrModuleName, 12, "Emp Code")
            Language.setMessage(mstrModuleName, 13, "Printed By :")
            Language.setMessage(mstrModuleName, 14, "Printed Date :")
            Language.setMessage(mstrModuleName, 16, " As On Date :")
            Language.setMessage(mstrModuleName, 17, " Employee :")
            Language.setMessage(mstrModuleName, 18, " Order By :")
            Language.setMessage(mstrModuleName, 19, "Group Total :")
            Language.setMessage(mstrModuleName, 20, "Grand Total :")
            Language.setMessage(mstrModuleName, 21, "Start Date")
            Language.setMessage(mstrModuleName, 22, "End Date")
            Language.setMessage(mstrModuleName, 23, "Prepared By :")
            Language.setMessage(mstrModuleName, 24, "Checked By :")
            Language.setMessage(mstrModuleName, 25, "Approved By :")
            Language.setMessage(mstrModuleName, 26, "Received By :")
            Language.setMessage(mstrModuleName, 27, " Total :")
            Language.setMessage(mstrModuleName, 28, "Department")
            Language.setMessage(mstrModuleName, 29, "Job Title")
            Language.setMessage(mstrModuleName, 30, "Leave Code :")
            Language.setMessage(mstrModuleName, 31, "Leave Type :")
            Language.setMessage(mstrModuleName, 32, "Sub Count :")
            Language.setMessage(mstrModuleName, 33, "Department")
            Language.setMessage(mstrModuleName, 34, "Job")
            Language.setMessage(mstrModuleName, 35, " Count :")
            Language.setMessage(mstrModuleName, 36, "Joining Date")
            Language.setMessage(mstrModuleName, 37, "Leave Adjustment")
            Language.setMessage(mstrModuleName, 38, "Encashment")

        Catch Ex As Exception
            Throw New Exception(Ex.Message & "; Procedure Name: SetMessages; Module Name: " & mstrModuleName)
        End Try
    End Sub
#End Region 'Language & UI Settings
    '</Language>


End Class
