﻿'************************************************************************************************************************************
'Class Name : clsSHIFReport.vb
'Purpose    :
'Date       : 11/11/2024
'Written By : Hemant.
'Modified   :
'************************************************************************************************************************************

#Region " Imports "

Imports eZeeCommonLib
Imports Aruti.Data
Imports ExcelWriter
Imports System.Text
Imports System.IO

#End Region

''' <summary>
''' Purpose: Report Generation Class 
''' Developer: Hemant.
''' </summary>
Public Class clsSHIFReport
    Inherits IReportData
    Private Shared ReadOnly mstrModuleName As String = "clsSHIFReport"
    Private mstrReportId As String = enArutiReport.SHIF_Report
    Dim objDataOperation As clsDataOperation

#Region " Constructor "

    Public Sub New(ByVal intLangId As Integer, ByVal intCompanyId As Integer)
        Me.setReportData(CInt(mstrReportId), intLangId, intCompanyId)
        Call Create_OnDetailReport()
    End Sub

#End Region

#Region " Private variables "

    Private mintReportId As Integer = 0
    Private mstrReportTypeName As String = ""
    Private mintPeriodId As Integer = -1
    Private mstrPeriodName As String = String.Empty
    Private mintMembershipId As Integer = 1
    Private mstrMembershipName As String = String.Empty
    Private mintNonPayrollMembershipId As Integer = -1
    Private mstrNonPayrollMembershipName As String = String.Empty
    Private mintNHIFMembershipId As Integer = -1
    Private mstrNHIFMembershipName As String = String.Empty
    Private mintContributionAmountHeadId As Integer = 1
    Private mstrContributionAmountHeadName As String = String.Empty

    Private mblnFirstNamethenSurname As Boolean = ConfigParameter._Object._FirstNamethenSurname
    Private mstrAnalysis_Fields As String = ""
    Private mstrAnalysis_Join As String = ""
    Private mstrAnalysis_OrderBy As String = ""
    Private mintViewIndex As Integer = -1
    Private mstrViewByIds As String = String.Empty
    Private mstrViewByName As String = String.Empty
    Private mstrReport_GroupName As String = ""
    Private mintReportTypeId As Integer = 0

    Private mstrExportedFileName As String = String.Empty
#End Region

#Region " Properties "

    Public WriteOnly Property _ReportId() As Integer
        Set(ByVal value As Integer)
            mintReportId = value
        End Set
    End Property

    Public WriteOnly Property _ReportTypeName() As String
        Set(ByVal value As String)
            mstrReportTypeName = value
        End Set
    End Property

    Public WriteOnly Property _PeriodId() As Integer
        Set(ByVal value As Integer)
            mintPeriodId = value
        End Set
    End Property

    Public WriteOnly Property _PeriodName() As String
        Set(ByVal value As String)
            mstrPeriodName = value
        End Set
    End Property

    Public WriteOnly Property _MembershipId() As Integer
        Set(ByVal value As Integer)
            mintMembershipId = value
        End Set
    End Property

    Public WriteOnly Property _MembershipName() As String
        Set(ByVal value As String)
            mstrMembershipName = value
        End Set
    End Property

    Public WriteOnly Property _NonPayrollMembershipId() As Integer
        Set(ByVal value As Integer)
            mintNonPayrollMembershipId = value
        End Set
    End Property

    Public WriteOnly Property _NonPayrollMembershipName() As String
        Set(ByVal value As String)
            mstrNonPayrollMembershipName = value
        End Set
    End Property

    Public WriteOnly Property _NHIFMembershipId() As Integer
        Set(ByVal value As Integer)
            mintNHIFMembershipId = value
        End Set
    End Property

    Public WriteOnly Property _NHIFMembershipName() As String
        Set(ByVal value As String)
            mstrNHIFMembershipName = value
        End Set
    End Property


    Public WriteOnly Property _ContributionAmountHeadId() As Integer
        Set(ByVal value As Integer)
            mintContributionAmountHeadId = value
        End Set
    End Property

    Public WriteOnly Property _ContributionAmountHeadName() As String
        Set(ByVal value As String)
            mstrContributionAmountHeadName = value
        End Set
    End Property


    Public WriteOnly Property _FirstNamethenSurname() As Boolean
        Set(ByVal value As Boolean)
            mblnFirstNamethenSurname = value
        End Set
    End Property

    Public WriteOnly Property _Analysis_Fields() As String
        Set(ByVal value As String)
            mstrAnalysis_Fields = value
        End Set
    End Property

    Public WriteOnly Property _Analysis_Join() As String
        Set(ByVal value As String)
            mstrAnalysis_Join = value
        End Set
    End Property

    Public WriteOnly Property _Analysis_OrderBy() As String
        Set(ByVal value As String)
            mstrAnalysis_OrderBy = value
        End Set
    End Property

    Public WriteOnly Property _ViewIndex() As Integer
        Set(ByVal value As Integer)
            mintViewIndex = value
        End Set
    End Property

    Public WriteOnly Property _ViewByIds() As String
        Set(ByVal value As String)
            mstrViewByIds = value
        End Set
    End Property

    Public WriteOnly Property _ViewByName() As String
        Set(ByVal value As String)
            mstrViewByName = value
        End Set
    End Property

    Public WriteOnly Property _Report_GroupName() As String
        Set(ByVal value As String)
            mstrReport_GroupName = value
        End Set
    End Property

    Public WriteOnly Property _ReportTypeId() As Integer
        Set(ByVal value As Integer)
            mintReportTypeId = value
        End Set
    End Property

    Public ReadOnly Property _ExportedFileName() As String
        Get
            Return mstrExportedFileName
        End Get
    End Property

#End Region

#Region "Public Function & Procedures "

    Public Sub SetDefaultValue()
        Try
            mintReportId = -1
            mstrReportTypeName = ""
            mintPeriodId = 0
            mstrPeriodName = ""
            mintMembershipId = 0
            mstrMembershipName = ""
            mintNonPayrollMembershipId = 0
            mstrNonPayrollMembershipName = ""
            mintNHIFMembershipId = 0
            mstrNHIFMembershipName = ""
            mintContributionAmountHeadId = 0
            mstrContributionAmountHeadName = ""
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: SetDefaultValue; Module Name: " & mstrModuleName)
        End Try
    End Sub

    Private Sub FilterTitleAndFilterQuery()
        Me._FilterQuery = ""
        Me._FilterTitle = ""
        Try

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: FilterTitleAndFilterQuery; Module Name: " & mstrModuleName)
        End Try
    End Sub

    Public Overloads Overrides Sub generateReport(ByVal pintReportType As Integer, Optional ByVal PrintAction As Aruti.Data.enPrintAction = Aruti.Data.enPrintAction.Preview, Optional ByVal ExportAction As Aruti.Data.enExportAction = Aruti.Data.enExportAction.None)

    End Sub

    Public Overrides Sub generateReportNew(ByVal xDatabaseName As String, ByVal xUserUnkid As Integer, ByVal xYearUnkid As Integer, ByVal xCompanyUnkid As Integer, ByVal xPeriodStart As Date, ByVal xPeriodEnd As Date, ByVal xUserModeSetting As String, ByVal xOnlyApproved As Boolean, ByVal xExportReportPath As String, ByVal xOpenReportAfterExport As Boolean, ByVal pintReportType As Integer, Optional ByVal PrintAction As enPrintAction = enPrintAction.Preview, Optional ByVal ExportAction As enExportAction = enExportAction.None, Optional ByVal xBaseCurrencyId As Integer = 0)
        Try

        Catch ex As Exception

        End Try
    End Sub

    Public Overrides Sub setDefaultOrderBy(ByVal intReportType As Integer)
        OrderByDisplay = ""
        OrderByQuery = ""
        Try

            OrderByDisplay = iColumn_DetailReport.ColumnItem(0).DisplayName
            OrderByQuery = iColumn_DetailReport.ColumnItem(0).Name

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: setDefaultOrderBy; Module Name: " & mstrModuleName)
        End Try
    End Sub

    Public Overrides Sub setOrderBy(ByVal intReportType As Integer)
        Try

            Call OrderByExecute(iColumn_DetailReport)
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: setOrderBy; Module Name: " & mstrModuleName)
        End Try
    End Sub

#End Region

#Region " Report Generation "

    Dim iColumn_DetailReport As New IColumnCollection

    Public Property Field_OnDetailReport() As IColumnCollection
        Get
            Return iColumn_DetailReport
        End Get
        Set(ByVal value As IColumnCollection)
            iColumn_DetailReport = value
        End Set
    End Property

    Private Sub Create_OnDetailReport()
        Try
            iColumn_DetailReport.Clear()
            iColumn_DetailReport.Add(New IColumn("", ""))
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: Create_OnDetailReport; Module Name: " & mstrModuleName)
        End Try
    End Sub

    Public Function Generate_DetailReport(ByVal strDatabaseName As String, _
                                           ByVal intUserUnkid As Integer, _
                                           ByVal intYearUnkid As Integer, _
                                           ByVal intCompanyUnkid As Integer, _
                                           ByVal dtPeriodStart As Date, _
                                           ByVal dtPeriodEnd As Date, _
                                           ByVal strUserModeSetting As String, _
                                           ByVal blnOnlyApproved As Boolean, _
                                           ByVal intBaseCurrencyId As Integer, _
                                           ByVal strFmtCurrency As String, _
                                           ByVal xExportReportPath As String, _
                                           ByVal xOpenReportAfterExport As Boolean _
                                           ) As Boolean

        Dim StrQ As String = ""
        Dim dsList As New DataSet
        Dim exForce As Exception
        Dim mdtTableExcel As New DataTable
        Dim dtCol As DataColumn

        Try
            objDataOperation = New clsDataOperation

            Dim dtFinalTable As DataTable
            dtFinalTable = New DataTable("SHIF")

            dtCol = New DataColumn("EmployeeCode", System.Type.GetType("System.String"))
            dtCol.Caption = Language.getMessage(mstrModuleName, 1, "PAYROLL NUMBER")
            dtCol.DefaultValue = ""
            dtFinalTable.Columns.Add(dtCol)

            dtCol = New DataColumn("FirstName", System.Type.GetType("System.String"))
            dtCol.Caption = Language.getMessage(mstrModuleName, 2, "FIRSTNAME")
            dtCol.DefaultValue = ""
            dtFinalTable.Columns.Add(dtCol)

            dtCol = New DataColumn("LastName", System.Type.GetType("System.String"))
            dtCol.Caption = Language.getMessage(mstrModuleName, 3, "LASTNAME")
            dtCol.DefaultValue = ""
            dtFinalTable.Columns.Add(dtCol)

            dtCol = New DataColumn("IDNo", System.Type.GetType("System.String"))
            dtCol.Caption = Language.getMessage(mstrModuleName, 4, "ID NO")
            dtCol.DefaultValue = ""
            dtFinalTable.Columns.Add(dtCol)

            dtCol = New DataColumn("KRA_PIN", System.Type.GetType("System.String"))
            dtCol.Caption = Language.getMessage(mstrModuleName, 5, "KRA PIN")
            dtCol.DefaultValue = ""
            dtFinalTable.Columns.Add(dtCol)

            dtCol = New DataColumn("NHIFNo", System.Type.GetType("System.String"))
            dtCol.Caption = Language.getMessage(mstrModuleName, 6, "NHIF NO")
            dtCol.DefaultValue = ""
            dtFinalTable.Columns.Add(dtCol)

            dtCol = New DataColumn("Amount", System.Type.GetType("System.Decimal"))
            dtCol.Caption = Language.getMessage(mstrModuleName, 7, "CONTRIBUTION AMOUNT")
            dtCol.DefaultValue = 0
            dtFinalTable.Columns.Add(dtCol)

            dtCol = New DataColumn("Mobile", System.Type.GetType("System.String"))
            dtCol.Caption = Language.getMessage(mstrModuleName, 8, "PHONE")
            dtCol.DefaultValue = ""
            dtFinalTable.Columns.Add(dtCol)


            Dim objExchangeRate As New clsExchangeRate
            Dim decDecimalPlaces As Decimal = 0
            objExchangeRate._ExchangeRateunkid = intBaseCurrencyId
            decDecimalPlaces = objExchangeRate._Digits_After_Decimal

            Dim xUACQry, xUACFiltrQry As String
            xUACQry = "" : xUACFiltrQry = ""
            Call NewAccessLevelFilterString(xUACQry, xUACFiltrQry, dtPeriodEnd, blnOnlyApproved, strDatabaseName, intUserUnkid, intCompanyUnkid, intYearUnkid, strUserModeSetting)


            StrQ = "SELECT hremployee_master.employeeunkid " & _
                        ", CONVERT(CHAR(8), hremployee_master.appointeddate, 112) AS appointeddate " & _
                        ", hremployee_master.employeecode " & _
                        ", ISNULL(hremployee_master.firstname, '')  AS firstname " & _
                        ", ISNULL(hremployee_master.surname, '') AS surname " & _
                        ", ISNULL(hremployee_master.present_mobile, '') AS mobile "
            

            StrQ &= "INTO #TableEmp " & _
                    "FROM hremployee_master "

            StrQ &= mstrAnalysis_Join

            If xUACQry.Trim.Length > 0 Then
                StrQ &= xUACQry
            End If

            StrQ &= " WHERE 1 = 1 "

            If xUACFiltrQry.Trim.Length > 0 Then
                StrQ &= " AND " & xUACFiltrQry
            End If

            StrQ &= ";SELECT A.employeeunkid " & _
                        ", #TableEmp.employeecode " & _
                        ", #TableEmp.firstname " & _
                        ", #TableEmp.surname " & _
                        ", MAX(A.MembershipNo) AS MembershipNo " & _
                        ", MAX(A.OtherMembershipNo) AS OtherMembershipNo " & _
                        ", MAX(A.NhifNo) AS NhifNo " & _
                        ", SUM(A.amount) AS amount " & _
                        ", #TableEmp.mobile "

            StrQ &= "FROM " & _
                   "( " & _
                       "SELECT #TableEmp.employeeunkid " & _
                            ", ISNULL(hremployee_meminfo_tran.membershipno, '') AS MembershipNo " & _
                            ", '' AS OtherMembershipNo " & _
                            ", '' AS NhifNo " & _
                            ", 0 AS amount "

            StrQ &= "FROM #TableEmp " & _
                            "LEFT JOIN hremployee_meminfo_tran " & _
                                "ON hremployee_meminfo_tran.employeeunkid = #TableEmp.employeeunkid " & _
                                   "AND ISNULL(hremployee_meminfo_tran.isdeleted, 0) = 0 " & _
                            "LEFT JOIN hrmembership_master " & _
                                "ON hremployee_meminfo_tran.membershipunkid = hrmembership_master.membershipunkid " & _
                        "WHERE hremployee_meminfo_tran.membershipunkid = @MemId "


            StrQ &= "UNION ALL " & _
                           "SELECT #TableEmp.employeeunkid " & _
                                 ", '' AS MembershipNo " & _
                                 ", ISNULL(hremployee_meminfo_tran.membershipno, '') AS MeOtherMembershipNomshipNo " & _
                                 ", '' AS NhifNo " & _
                                 ", 0 AS amount "

            StrQ &= "FROM #TableEmp " & _
                               "LEFT JOIN hremployee_meminfo_tran " & _
                                   "ON hremployee_meminfo_tran.employeeunkid = #TableEmp.employeeunkid " & _
                                      "AND ISNULL(hremployee_meminfo_tran.isdeleted, 0) = 0 " & _
                               "LEFT JOIN hrmembership_master " & _
                                   "ON hremployee_meminfo_tran.membershipunkid = hrmembership_master.membershipunkid " & _
                            "WHERE hremployee_meminfo_tran.membershipunkid = @OtherMemId "

            StrQ &= "UNION ALL " & _
                          "SELECT #TableEmp.employeeunkid " & _
                                ", '' AS MembershipNo " & _
                                ", '' AS MeOtherMembershipNomshipNo " & _
                                ", ISNULL(hremployee_meminfo_tran.membershipno, '') AS NhifNo " & _
                                ", 0 AS amount "

            StrQ &= "FROM #TableEmp " & _
                               "LEFT JOIN hremployee_meminfo_tran " & _
                                   "ON hremployee_meminfo_tran.employeeunkid = #TableEmp.employeeunkid " & _
                                      "AND ISNULL(hremployee_meminfo_tran.isdeleted, 0) = 0 " & _
                               "LEFT JOIN hrmembership_master " & _
                                   "ON hremployee_meminfo_tran.membershipunkid = hrmembership_master.membershipunkid " & _
                            "WHERE hremployee_meminfo_tran.membershipunkid = @NhifMemId "


            StrQ &= "UNION ALL " & _
                        "SELECT prpayrollprocess_tran.employeeunkid " & _
                             ", '' AS MembershipNo " & _
                             ", '' AS OtherMembershipNo " & _
                             ", '' AS NhifNo " & _
                             ", SUM(ISNULL(CAST(prpayrollprocess_tran.amount AS DECIMAL(36, " & decDecimalPlaces & ")), 0)) AS amount "

            StrQ &= "FROM prpayrollprocess_tran " & _
                            "JOIN #TableEmp " & _
                                "ON prpayrollprocess_tran.employeeunkid = #TableEmp.employeeunkid " & _
                            "JOIN prtnaleave_tran " & _
                                "ON prpayrollprocess_tran.tnaleavetranunkid = prtnaleave_tran.tnaleavetranunkid " & _
                            "JOIN prtranhead_master " & _
                                "ON prpayrollprocess_tran.tranheadunkid = prtranhead_master.tranheadunkid " & _
                        "WHERE prpayrollprocess_tran.isvoid = 0 " & _
                              "AND prtnaleave_tran.isvoid = 0 " & _
                              "AND prtranhead_master.isvoid = 0 " & _
                              "AND prtnaleave_tran.payperiodunkid = @PeriodId " & _
                              "AND prtranhead_master.tranheadunkid =  @tranheadunkid " & _
                              "AND prpayrollprocess_tran.amount <> 0 "


            StrQ &= "GROUP BY prpayrollprocess_tran.employeeunkid " & _
                               ", prtnaleave_tran.payperiodunkid "

            StrQ &= ") AS A " & _
                  "JOIN #TableEmp " & _
                              "ON A.employeeunkid = #TableEmp.employeeunkid " & _
                  "GROUP BY A.employeeunkid " & _
                          ", #TableEmp.employeecode " & _
                          ", #TableEmp.firstname " & _
                          ", #TableEmp.surname " & _
                          ", #TableEmp.mobile "

            StrQ &= " DROP TABLE #TableEmp "


            objDataOperation.AddParameter("@MemId", SqlDbType.Int, eZeeDataType.INT_SIZE, mintMembershipId)
            objDataOperation.AddParameter("@OtherMemId", SqlDbType.Int, eZeeDataType.INT_SIZE, mintNonPayrollMembershipId)
            objDataOperation.AddParameter("@NhifMemId", SqlDbType.Int, eZeeDataType.INT_SIZE, mintNHIFMembershipId)
            objDataOperation.AddParameter("@PeriodId", SqlDbType.Int, eZeeDataType.INT_SIZE, mintPeriodId)
            objDataOperation.AddParameter("@tranheadunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintContributionAmountHeadId)


            dsList = objDataOperation.ExecQuery(StrQ, "DataTable")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & " " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            Dim rpt_Row As DataRow = Nothing
            Dim drRow As DataRow
            Dim intRowCount As Integer = dsList.Tables(0).Rows.Count
            For ii As Integer = 0 To intRowCount - 1
                drRow = dsList.Tables(0).Rows(ii)
                rpt_Row = dtFinalTable.NewRow

                rpt_Row.Item("EmployeeCode") = drRow.Item("EmployeeCode")
                rpt_Row.Item("FirstName") = drRow.Item("FirstName")
                rpt_Row.Item("LastName") = drRow.Item("SurName")
                rpt_Row.Item("IDNo") = drRow.Item("MembershipNo")
                rpt_Row.Item("KRA_PIN") = drRow.Item("OtherMembershipNo")
                rpt_Row.Item("NHIFNo") = drRow.Item("NHIFNo")
                rpt_Row.Item("Amount") = drRow.Item("Amount")
                rpt_Row.Item("Mobile") = drRow.Item("Mobile")


                dtFinalTable.Rows.Add(rpt_Row)

            Next

            If dtFinalTable IsNot Nothing AndAlso dtFinalTable.Rows.Count <= 0 Then
                Dim dRow As DataRow = dtFinalTable.NewRow
                dRow.Item("EmployeeCode") = ""
                dRow.Item("FirstName") = ""
                dRow.Item("LastName") = ""
                dRow.Item("IDNo") = ""
                dRow.Item("KRA_PIN") = ""
                dRow.Item("NHIFNo") = ""
                dRow.Item("Amount") = ""
                dRow.Item("Mobile") = ""
                dtFinalTable.Rows.Add(dRow)
            End If

            dtFinalTable.AcceptChanges()

            mdtTableExcel = dtFinalTable

            Using package As New OfficeOpenXml.ExcelPackage()
                Dim worksheet As OfficeOpenXml.ExcelWorksheet = package.Workbook.Worksheets.Add("Data")
                Dim xTable As DataTable = mdtTableExcel.Copy
                For Each iCol As DataColumn In xTable.Columns
                    If iCol.Ordinal <> xTable.Columns.Count - 1 Then
                        xTable.Columns(iCol.Ordinal).ColumnName = iCol.Caption.Trim()
                    End If
                Next

                worksheet.Cells("A1").LoadFromDataTable(xTable, True)
                worksheet.Cells("A1:XFD").AutoFilter = False
                worksheet.Cells(2, 1, xTable.Rows.Count + 1, xTable.Columns.Count - 2).Style.Numberformat.Format = "@"
                worksheet.Cells(2, xTable.Columns.Count - 1, xTable.Rows.Count + 1, xTable.Columns.Count - 1).Style.Numberformat.Format = "0"
                worksheet.Cells(2, xTable.Columns.Count, xTable.Rows.Count + 1, xTable.Columns.Count).Style.Numberformat.Format = "0"

                worksheet.Cells.AutoFitColumns(0)

                If Me._ReportName.Contains("/") = True Then
                    Me._ReportName = Me._ReportName.Replace("/", "_")
                End If

                If Me._ReportName.Contains("\") Then
                    Me._ReportName = Me._ReportName.Replace("\", "_")
                End If

                If Me._ReportName.Contains(":") Then
                    Me._ReportName = Me._ReportName.Replace(":", "_")
                End If
                Dim strExportFileName As String = ""
                strExportFileName = Me._ReportName.Trim.Replace(" ", "_") & "_" & Now.Date.ToString("yyyyMMdd") & Format(Now, "hhmmss")

                xExportReportPath = xExportReportPath & "\" & strExportFileName & ".xlsx"

                Dim fi As IO.FileInfo = New IO.FileInfo(xExportReportPath)
                package.SaveAs(fi)

                mstrExportedFileName = strExportFileName & ".xlsx"

            End Using

            If IO.File.Exists(xExportReportPath) Then
                If xOpenReportAfterExport Then
                    Process.Start(xExportReportPath)
                End If
            End If


        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: Generate_DetailReport; Module Name: " & mstrModuleName)
        End Try
    End Function



#End Region
	'<Language> This Auto Generated Text Please Do Not Modify it.
#Region " Language & UI Settings "
	Public Shared Sub SetMessages()
		Try
			Language.setMessage(mstrModuleName, 1, "PAYROLL NUMBER")
			Language.setMessage(mstrModuleName, 2, "FIRSTNAME")
			Language.setMessage(mstrModuleName, 3, "LASTNAME")
			Language.setMessage(mstrModuleName, 4, "ID NO")
			Language.setMessage(mstrModuleName, 5, "KRA PIN")
			Language.setMessage(mstrModuleName, 6, "NHIF NO")
			Language.setMessage(mstrModuleName, 7, "CONTRIBUTION AMOUNT")
			Language.setMessage(mstrModuleName, 8, "PHONE")
			
		Catch Ex As Exception
            DisplayError.Show("-1", Ex.Message, "SetMessages", mstrModuleName)
		End Try
	End Sub
#End Region 'Language & UI Settings
	'</Language>
End Class
