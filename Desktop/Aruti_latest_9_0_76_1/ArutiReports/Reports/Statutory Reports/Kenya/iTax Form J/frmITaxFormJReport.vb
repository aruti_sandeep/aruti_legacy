'************************************************************************************************************************************
'Class Name : frmITaxFormJReport.vb
'Purpose    : 
'Written By : Sohail
'Modified   : 
'************************************************************************************************************************************

#Region " Imports "

Imports Aruti.Data
Imports eZeeCommonLib
Imports Aruti
Imports ArutiReports

#End Region

Public Class frmITaxFormJReport

#Region " Private Variables "

    Private ReadOnly mstrModuleName As String = "frmITaxFormJReport"
    Private objiTaxFormJ As clsITaxFormJReport
    Private mstrSchemeName As String = ""

    Private mstrStringIds As String = String.Empty
    Private mstrStringName As String = String.Empty
    Private mintViewIdx As Integer = 0
    Private mstrAnalysis_Fields As String = ""
    Private mstrAnalysis_Join As String = ""
    Private mstrAnalysis_OrderBy As String = ""
    Private mstrReport_GroupName As String = ""
    Private mstrAnalysis_OrderBy_GroupName As String = ""

    Private mdtPeriodEndDate As DateTime
    Private mintFirstPeriod As Integer = 0
    'Nilay (10-Oct-2015) -- Start
    'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
    Private mstrBaseCurrSign As String = ""
    Private mintBaseCurrId As Integer = 0
    'Nilay (10-Oct-2015) -- End

#End Region

#Region " Contructor "

    Public Sub New()
        objiTaxFormJ = New clsITaxFormJReport(User._Object._Languageunkid,Company._Object._Companyunkid)
        objiTaxFormJ.SetDefaultValue()
        InitializeComponent()
    End Sub

#End Region

#Region " Private Enum "
    Private Enum enHeadTypeId
        Membership = 1
    End Enum
#End Region

#Region " Private Function "

    Private Sub FillCombo()
        Dim objMember As New clsmembership_master
        Dim objEmp As New clsEmployee_Master
        Dim objperiod As New clscommom_period_Tran
        Dim objLoan As New clsLoan_Scheme
        Dim objMaster As New clsMasterData
        Dim dsCombos As New DataSet


        Try
            'Nilay (18-Nov-2015) -- Start
            'mintFirstPeriod = objMaster.getFirstPeriodID(enModuleReference.Payroll, 1, FinancialYear._Object._YearUnkid, False)
            mintFirstPeriod = objMaster.getFirstPeriodID(enModuleReference.Payroll, FinancialYear._Object._YearUnkid, 1, False)
            'Nilay (18-Nov-2015) -- End

            'Nilay (03-Nov-2015) -- Start
            'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
            'dsCombos = objperiod.getListForCombo(enModuleReference.Payroll, FinancialYear._Object._YearUnkid, "Period", True, 0, , , FinancialYear._Object._DatabaseName)
            dsCombos = objperiod.getListForCombo(enModuleReference.Payroll, _
                                                 FinancialYear._Object._YearUnkid, _
                                                 FinancialYear._Object._DatabaseName, _
                                                 FinancialYear._Object._Database_Start_Date, _
                                                 "Period", True)
            'Nilay (03-Nov-2015) -- End

            With cboPeriod
                .ValueMember = "periodunkid"
                .DisplayMember = "name"
                .DataSource = dsCombos.Tables("Period")
                .SelectedValue = mintFirstPeriod
            End With

            'S.SANDEEP [04 JUN 2015] -- START
            'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
            'dsCombos = objEmp.GetEmployeeList("Emp", True, False)
            dsCombos = objEmp.GetEmployeeList(FinancialYear._Object._DatabaseName, _
                                              User._Object._Userunkid, _
                                              FinancialYear._Object._YearUnkid, _
                                              Company._Object._Companyunkid, _
                                              eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), _
                                              eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), _
                                              ConfigParameter._Object._UserAccessModeSetting, _
                                              True, True, "Emp", True)
            'S.SANDEEP [04 JUN 2015] -- END

            With cboEmployee
                .ValueMember = "employeeunkid"
                .DisplayMember = "employeename"
                .DataSource = dsCombos.Tables(0)
                .SelectedValue = 0
            End With

            dsCombos = objMember.getListForCombo("Membership", True)
            With cboMembership
                .ValueMember = "membershipunkid"
                .DisplayMember = "name"
                .DataSource = dsCombos.Tables(0)
                .SelectedValue = 0
            End With

            'Nilay (10-Oct-2015) -- Start
            'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
            Dim objCurrency As New clsExchangeRate
            dsCombos = objCurrency.getComboList("Currency")
            With cboCurrency
                .ValueMember = "countryunkid"
                .DisplayMember = "currency_sign"
                .DataSource = dsCombos.Tables("Currency")
                .SelectedValue = 0
            End With

            Dim dtTable As DataTable = New DataView(dsCombos.Tables("Currency"), "isbasecurrency = 1 ", "", DataViewRowState.CurrentRows).ToTable
            If dtTable.Rows.Count > 0 Then
                mstrBaseCurrSign = dtTable.Rows(0).Item("currency_sign").ToString
                mintBaseCurrId = CInt(dtTable.Rows(0).Item("countryunkid"))
            Else
                mstrBaseCurrSign = ""
                mintBaseCurrId = 0
            End If
            If mintBaseCurrId > 0 Then
                cboCurrency.SelectedValue = mintBaseCurrId
            Else
                If cboCurrency.Items.Count > 0 Then cboCurrency.SelectedIndex = 0
            End If
            'Nilay (10-Oct-2015) -- End

            lvLoan.Items.Clear()
            Dim lvItem As ListViewItem
            dsCombos = objLoan.getComboList(False, "Loan")
            For Each dsRow As DataRow In dsCombos.Tables("Loan").Rows
                lvItem = New ListViewItem

                lvItem.Text = ""
                lvItem.Tag = CInt(dsRow.Item("loanschemeunkid"))

                lvItem.SubItems.Add(dsRow.Item("Code").ToString)

                lvItem.SubItems.Add(dsRow.Item("name").ToString)

                RemoveHandler lvLoan.ItemChecked, AddressOf lvLoan_ItemChecked
                lvLoan.Items.Add(lvItem)
                AddHandler lvLoan.ItemChecked, AddressOf lvLoan_ItemChecked
            Next

            If lvLoan.Items.Count > 9 Then
                colhName.Width = 130 - 18
            Else
                colhName.Width = 130
            End If

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "FillCombo", mstrModuleName)
        Finally
            objMember = Nothing
            objEmp = Nothing
            objperiod = Nothing
            objLoan = Nothing
            dsCombos.Dispose()
        End Try
    End Sub

    Private Sub ResetValue()
        Try
            cboPeriod.SelectedValue = mintFirstPeriod
            cboEmployee.SelectedValue = 0
            cboMembership.SelectedValue = 0


            Call CheckAll(lvLoan, False)

            mstrStringIds = String.Empty
            mstrStringName = String.Empty
            mintViewIdx = 0
            mstrAnalysis_Fields = ""
            mstrAnalysis_Join = ""
            mstrAnalysis_OrderBy = ""
            mstrReport_GroupName = ""
            mstrAnalysis_OrderBy_GroupName = ""

            Call GetValue()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "ResetValue", mstrModuleName)
        End Try
    End Sub

    Private Sub GetValue()
        Dim objUserDefRMode As New clsUserDef_ReportMode
        Dim dsList As DataSet
        Try
            dsList = objUserDefRMode.GetList("List", enArutiReport.ITax_Form_J_Report)

            If dsList.Tables("List").Rows.Count > 0 Then

                For Each dsRow As DataRow In dsList.Tables("List").Rows
                    Select Case CInt(dsRow.Item("headtypeid"))

                        Case enHeadTypeId.Membership
                            cboMembership.SelectedValue = CInt(dsRow.Item("transactionheadid"))


                    End Select
                Next

            End If

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "GetValue", mstrModuleName)
        Finally
            objUserDefRMode = Nothing
        End Try
    End Sub

    Private Function SetFilter(Optional ByVal blnSaveSelection As Boolean = False) As Boolean
        Try
            objiTaxFormJ.SetDefaultValue()

            mstrSchemeName = String.Empty

            If blnSaveSelection = False Then
                If CInt(cboPeriod.SelectedValue) <= 0 Then
                    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 1, "Effective Period is mandatory information. Please select Effective Period."), enMsgBoxStyle.Information)
                    cboPeriod.Focus()
                    Return False
                ElseIf lvLoan.CheckedItems.Count <= 0 Then
                    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 2, "Please check Periods to view report."), enMsgBoxStyle.Information)
                    lvLoan.Focus()
                    Return False
                End If
            End If

            'If gbBasicSalaryOtherEarning.Checked = True AndAlso CInt(cboOtherEarning.SelectedValue) <= 0 Then
            '    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 3, "Please select transaction head."), enMsgBoxStyle.Information)
            '    cboOtherEarning.Focus()
            '    Return False
            'End If

            objiTaxFormJ._PeriodId = CInt(cboPeriod.SelectedValue)
            objiTaxFormJ._PeriodName = cboPeriod.Text

            If CInt(cboMembership.SelectedValue) > 0 Then
                objiTaxFormJ._MembershipId = cboMembership.SelectedValue
                objiTaxFormJ._MembershipName = cboMembership.Text
            End If

            If CInt(cboEmployee.SelectedValue) > 0 Then
                objiTaxFormJ._EmployeeId = CInt(cboEmployee.SelectedValue)
                objiTaxFormJ._EmployeeName = cboEmployee.Text
            End If

            'If CInt(cboHouseAllow.SelectedValue) > 0 Then
            '    objiTaxFormJ._ColumnFTranId = CInt(cboHouseAllow.SelectedValue)
            'End If

            'If CInt(cboTransAllow.SelectedValue) > 0 Then
            '    objiTaxFormJ._ColumnGTranId = CInt(cboTransAllow.SelectedValue)
            'End If

            'If CInt(cboLeavePay.SelectedValue) > 0 Then
            '    objiTaxFormJ._ColumnHTranId = CInt(cboLeavePay.SelectedValue)
            'End If

            'If CInt(cboOverTimeAllow.SelectedValue) > 0 Then
            '    objiTaxFormJ._ColumnITranId = CInt(cboOverTimeAllow.SelectedValue)
            'End If

            'If CInt(cboDirectorFee.SelectedValue) > 0 Then
            '    objiTaxFormJ._ColumnJTranId = CInt(cboDirectorFee.SelectedValue)
            'End If

            'If CInt(cboLumpSum.SelectedValue) > 0 Then
            '    objiTaxFormJ._ColumnKTranId = CInt(cboLumpSum.SelectedValue)
            'End If

            'If CInt(cboActualContribution.SelectedValue) > 0 Then
            '    objiTaxFormJ._ColumnYTranId = CInt(cboActualContribution.SelectedValue)
            'End If

            'If CInt(cboMonthlyRelief.SelectedValue) > 0 Then
            '    objiTaxFormJ._ColumnAFTranId = CInt(cboMonthlyRelief.SelectedValue)
            'End If


            'If gbBasicSalaryOtherEarning.Checked = True Then
            '    objiTaxFormJ._OtherEarningTranId = CInt(cboOtherEarning.SelectedValue)
            'Else
            '    objiTaxFormJ._OtherEarningTranId = 0
            'End If

            objiTaxFormJ._Analysis_OrderBy_GName = mstrAnalysis_OrderBy_GroupName

            Dim mstrSchemeIds As String = ""
            Dim allId As List(Of String) = (From lv In lvLoan.CheckedItems.Cast(Of ListViewItem)() Select (lv.Tag.ToString)).ToList
            Dim allName As List(Of String) = (From lv In lvLoan.CheckedItems.Cast(Of ListViewItem)() Select (lv.SubItems(colhName.Index).Text)).ToList
            mstrSchemeIds = String.Join(",", allId.ToArray)
            mstrSchemeName = String.Join(",", allName.ToArray)

            objiTaxFormJ._SchemeListIDs = mstrSchemeIds
            objiTaxFormJ._SchemeName = mstrSchemeName

            objiTaxFormJ._ViewByIds = mstrStringIds
            objiTaxFormJ._ViewIndex = mintViewIdx
            objiTaxFormJ._ViewByName = mstrStringName
            objiTaxFormJ._Analysis_Fields = mstrAnalysis_Fields
            objiTaxFormJ._Analysis_Join = mstrAnalysis_Join
            objiTaxFormJ._Analysis_OrderBy = mstrAnalysis_OrderBy
            objiTaxFormJ._Report_GroupName = mstrReport_GroupName
            'Nilay (10-Oct-2015) -- Start
            'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
            objiTaxFormJ._CurrencyId = CInt(cboCurrency.SelectedValue)
            'Nilay (10-Oct-2015) -- End

            Return True

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "SetFilter", mstrModuleName)
        End Try
    End Function

    Private Sub CheckAll(ByVal LV As ListView, ByVal blnOperation As Boolean)
        Try
            For Each LvItem As ListViewItem In LV.Items
                LvItem.Checked = blnOperation
            Next
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "CheckAll", mstrModuleName)
        End Try
    End Sub

#End Region

#Region " Forms "

    Private Sub frmITaxFormJReport_FormClosed(ByVal sender As Object, ByVal e As System.Windows.Forms.FormClosedEventArgs) Handles Me.FormClosed
        Try
            objiTaxFormJ = Nothing
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmITaxFormJReport_FormClosed", mstrModuleName)
        End Try
    End Sub

    Private Sub frmITaxFormJReport_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            Call Language.setLanguage(Me.Name)


            Call OtherSettings()


            Call FillCombo()
            Call ResetValue()

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmITaxFormJReport_Load", mstrModuleName)
        End Try
    End Sub

    Private Sub Form_KeyDown(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles Me.KeyDown
        Try
            If e.Control Then
                If e.KeyCode = Windows.Forms.Keys.E Then
                    Call btnExport_Click(sender, e)
                End If
            End If
        Catch ex As Exception
            DisplayError.Show(-1, ex.Message, "Form_KeyDown", mstrModuleName)
        End Try
    End Sub

    Private Sub Form_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles Me.KeyPress
        Try
            Select Case e.KeyChar
                Case CChar(ChrW(CInt(Windows.Forms.Keys.Enter)))
                    Windows.Forms.SendKeys.Send("{TAB}")
                    e.Handled = True
                    Exit Select
            End Select
        Catch ex As Exception
            DisplayError.Show(-1, ex.Message, "Form_KeyPress", mstrModuleName)
        End Try
    End Sub

#End Region

#Region " Buttons "

    Private Sub btnClose_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnClose.Click
        Try
            Me.Close()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "btnClose_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub btnExport_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnExport.Click
        Try
            If Not SetFilter() Then Exit Sub

            SaveDialog.FileName = ""
            SaveDialog.Filter = "CSV File|*.csv"
            SaveDialog.FilterIndex = 0
            SaveDialog.ShowDialog()
            If SaveDialog.FileName = "" Then
                Exit Try
            End If

            'Nilay (10-Oct-2015) -- Start
            'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
            'If objiTaxFormJ.Generate_PayrollReport(FinancialYear._Object._YearUnkid, UserAccessLevel._AccessLevelFilterString, FinancialYear._Object._Database_Start_Date, FinancialYear._Object._Database_End_Date, SaveDialog.FileName) = True Then
            '    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 3, "Data Exported Successfuly."), enMsgBoxStyle.Information)
            'End If

            Dim dtPeriodStartDate As Date
            Dim dtPeriodEndDate As Date
            If CInt(cboPeriod.SelectedValue) <= 0 Then
                dtPeriodStartDate = eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate)
                dtPeriodEndDate = eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate)
            Else
                Dim objPeriod As New clscommom_period_Tran
                objPeriod._Periodunkid(FinancialYear._Object._DatabaseName) = CInt(cboPeriod.SelectedValue)
                dtPeriodStartDate = objPeriod._Start_Date
                dtPeriodEndDate = objPeriod._End_Date
                objPeriod = Nothing
            End If

            'Sohail (03 Aug 2019) -- Start
            'PAYTECH KENYA issue # 0004019 - 76.1 - Inaccurate data on payroll reports i.e payroll summary report When an employee is moved between departments/branches, payroll report captures only the latest transfer records regardless of the period when payroll was done. System should pick the allocation based on period of transfer without overriding the former transfer.
            objiTaxFormJ._Analysis_Join = mstrAnalysis_Join.Replace(ConfigParameter._Object._EmployeeAsOnDate, eZeeDate.convertDate(dtPeriodEndDate))
            'Sohail (03 Aug 2019) -- End

            If objiTaxFormJ.Generate_PayrollReport(FinancialYear._Object._DatabaseName, _
                                                   User._Object._Userunkid, _
                                                   FinancialYear._Object._YearUnkid, _
                                                   Company._Object._Companyunkid, _
                                                   dtPeriodStartDate, _
                                                   dtPeriodEndDate, _
                                                   ConfigParameter._Object._UserAccessModeSetting, _
                                                   True, _
                                                   SaveDialog.FileName) = True Then

                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 3, "Data Exported Successfuly."), enMsgBoxStyle.Information)
            End If
            'objiTaxFormJ.generateReportNew(FinancialYear._Object._DatabaseName, _
            '                               User._Object._Userunkid, _
            '                               FinancialYear._Object._YearUnkid, _
            '                               Company._Object._Companyunkid, _
            '                               dtPeriodStartDate, _
            '                               dtPeriodEndDate, _
            '                               ConfigParameter._Object._UserAccessModeSetting, _
            '                               True, _
            '                               ConfigParameter._Object._ExportReportPath, _
            '                               ConfigParameter._Object._OpenAfterExport, _
            '                               0, _
            '                               enPrintAction.None, e.Type)

            'Nilay (10-Oct-2015) -- End

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "btnExport_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub btnReset_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnReset.Click
        Try
            Call ResetValue()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "btnReset_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub objbtnSearchEmployee_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnSearchEmployee.Click
        Dim frm As New frmCommonSearch
        Try
            frm.DataSource = cboEmployee.DataSource
            frm.ValueMember = cboEmployee.ValueMember
            frm.DisplayMember = cboEmployee.DisplayMember
            frm.CodeMember = "employeecode"
            If frm.DisplayDialog Then
                cboEmployee.SelectedValue = frm.SelectedValue
                cboEmployee.Focus()
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objbtnSearchEmployee_Click", mstrModuleName)
        Finally
            If frm IsNot Nothing Then frm.Dispose()
        End Try
    End Sub

    Private Sub objbtnLanguage_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnLanguage.Click
        Dim objfrm As New frmLanguage
        Try
            If User._Object._Isrighttoleft = True Then
                objfrm.RightToLeft = Windows.Forms.RightToLeft.Yes
                objfrm.RightToLeftLayout = True
                Call Language.ctlRightToLeftlayOut(objfrm)
            End If

            Call SetMessages()

            clsITaxFormJReport.SetMessages()
            objfrm._Other_ModuleNames = "clsITaxFormJReport"
            objfrm.displayDialog(Me)

            Call Language.setLanguage(Me.Name)
            Call SetLanguage()

        Catch ex As System.Exception
            Call DisplayError.Show(-1, ex.Message, "Form_Language_Click", mstrModuleName)
        Finally
            objfrm.Dispose()
            objfrm = Nothing
        End Try
    End Sub

    Private Sub btnSaveSelection_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSaveSelection.Click
        Dim objUserDefRMode As New clsUserDef_ReportMode
        Try
            If SetFilter(True) = False Then Exit Try

            For intHeadType As Integer = 1 To 10
                objUserDefRMode = New clsUserDef_ReportMode()

                Dim intUnkid As Integer = -1

                objUserDefRMode._Reportunkid = enArutiReport.ITax_Form_J_Report
                objUserDefRMode._Reporttypeid = 0
                objUserDefRMode._Reportmodeid = 0

                Select Case intHeadType


                    Case enHeadTypeId.Membership
                        objUserDefRMode._Headtypeid = intHeadType
                        objUserDefRMode._EarningTranHeadIds = cboMembership.SelectedValue.ToString

                        intUnkid = objUserDefRMode.isExist(enArutiReport.ITax_Form_J_Report, 0, 0, intHeadType)


                End Select

                objUserDefRMode._Reportmodeunkid = intUnkid

                If intUnkid <= 0 Then
                    objUserDefRMode.Insert()
                Else
                    objUserDefRMode.Update()
                End If

            Next

            eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 4, "Selection Saved Successfully"), enMsgBoxStyle.Information)

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "btnSaveSelection_Click", mstrModuleName)
        Finally
            objUserDefRMode = Nothing
        End Try
    End Sub

#End Region

#Region " Combobox's Events "

    Private Sub cboPeriod_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cboPeriod.SelectedIndexChanged
        Dim objperiod As New clscommom_period_Tran
        Dim objExcessSlan As New clsTranheadInexcessslabTran
        Dim dsCombos As DataSet
        Dim dtTable As DataTable
        Dim strNextSlabFilter As String = ""

        Try
            'Sohail (21 Aug 2015) -- Start
            'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
            'dsCombos = objperiod.getListForCombo(enModuleReference.Payroll, FinancialYear._Object._YearUnkid, "Period", False)
            dsCombos = objperiod.getListForCombo(enModuleReference.Payroll, FinancialYear._Object._YearUnkid, FinancialYear._Object._DatabaseName, FinancialYear._Object._Database_Start_Date, "Period", False)
            'Sohail (21 Aug 2015) -- End

            If CInt(cboPeriod.SelectedValue) > 0 Then
                'Sohail (21 Aug 2015) -- Start
                'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
                'objperiod._Periodunkid = CInt(cboPeriod.SelectedValue)
                objperiod._Periodunkid(FinancialYear._Object._DatabaseName) = CInt(cboPeriod.SelectedValue)
                'Sohail (21 Aug 2015) -- End

                Dim ds As DataSet = objExcessSlan.GetSlabPeriodList(enModuleReference.Payroll, 0, "Slab", True, 0)
                Dim dt As DataTable = New DataView(ds.Tables("Slab"), "end_date > '" & eZeeDate.convertDate(objperiod._End_Date) & "' ", "", DataViewRowState.CurrentRows).ToTable
                If dt.Rows.Count > 0 Then
                    strNextSlabFilter = " AND end_date < '" & dt.Rows(0).Item("end_date").ToString & "' "
                End If

                dtTable = New DataView(dsCombos.Tables("Period"), "end_date >= '" & eZeeDate.convertDate(objperiod._End_Date) & "' " & strNextSlabFilter & " ", "", DataViewRowState.CurrentRows).ToTable
            Else
                dtTable = New DataView(dsCombos.Tables("Period")).ToTable
                dtTable.Rows.Clear()
            End If

            'lvLoan.Items.Clear()
            'Dim lvItem As ListViewItem
            'For Each dtRow As DataRow In dtTable.Rows
            '    lvItem = New ListViewItem
            '    lvItem.Text = ""
            '    lvItem.SubItems.Add(dtRow.Item("name").ToString)
            '    lvItem.Tag = dtRow.Item("periodunkid")
            '    lvItem.SubItems(colhName.Index).Tag = dtRow.Item("end_date").ToString
            '    lvLoan.Items.Add(lvItem)
            '    lvItem = Nothing
            'Next
            'lvLoan.GridLines = False

            'If lvLoan.Items.Count > 6 Then
            '    colhName.Width = 205 - 18
            'Else
            '    colhName.Width = 205
            'End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "cboSlabEffectivePeriod_SelectedIndexChanged", mstrModuleName)
        End Try
    End Sub

    'Private Sub cboColumnF_Validating(ByVal sender As Object, ByVal e As System.ComponentModel.CancelEventArgs) Handles cboHouseAllow.Validating, cboTransAllow.Validating, _
    '                                                                                                                    cboLeavePay.Validating, cboOverTimeAllow.Validating, _
    '                                                                                                                    cboDirectorFee.Validating, cboLumpSum.Validating
    '    Try
    '        Dim cbo As ComboBox = CType(sender, ComboBox)

    '        If CInt(cbo.SelectedValue) > 0 Then
    '            Dim lst As IEnumerable(Of ComboBox) = gbFilterCriteria.Controls.OfType(Of ComboBox)().Where(Function(t) t.Name <> cbo.Name AndAlso t.Name <> cboEmployee.Name AndAlso t.Name <> cboMembership.Name AndAlso t.Name <> cboSlabEffectivePeriod.Name AndAlso CInt(t.SelectedValue) = CInt(cbo.SelectedValue))
    '            If lst.Count > 0 Then
    '                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 5, "Sorry, This transaction head is already mapped."))
    '                cbo.SelectedValue = 0
    '                e.Cancel = True
    '            End If
    '        End If
    '    Catch ex As Exception
    '        DisplayError.Show("-1", ex.Message, "cboColumnF_Validating", mstrModuleName)
    '    End Try
    'End Sub

#End Region

#Region " Controls "

    Private Sub objchkAllPeriod_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objchkAllPeriod.CheckedChanged
        Try
            Call CheckAll(lvLoan, objchkAllPeriod.Checked)
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objchkAllPeriod_CheckedChanged", mstrModuleName)
        End Try
    End Sub

    Private Sub lvLoan_ItemChecked(ByVal sender As Object, ByVal e As System.Windows.Forms.ItemCheckedEventArgs) Handles lvLoan.ItemChecked
        Try
            RemoveHandler objchkAllPeriod.CheckedChanged, AddressOf objchkAllPeriod_CheckedChanged
            If lvLoan.CheckedItems.Count <= 0 Then
                objchkAllPeriod.CheckState = CheckState.Unchecked
            ElseIf lvLoan.CheckedItems.Count < lvLoan.Items.Count Then
                objchkAllPeriod.CheckState = CheckState.Indeterminate
            ElseIf lvLoan.CheckedItems.Count = lvLoan.Items.Count Then
                objchkAllPeriod.CheckState = CheckState.Checked
            End If
            AddHandler objchkAllPeriod.CheckedChanged, AddressOf objchkAllPeriod_CheckedChanged
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "lvLoan_ItemChecked", mstrModuleName)
        End Try
    End Sub

    Private Sub lnkSetAnalysis_LinkClicked(ByVal sender As System.Object, ByVal e As System.Windows.Forms.LinkLabelLinkClickedEventArgs) Handles lnkSetAnalysis.LinkClicked
        Dim frm As New frmViewAnalysis
        Try
            frm.displayDialog()
            mstrStringIds = frm._ReportBy_Ids
            mstrStringName = frm._ReportBy_Name
            mintViewIdx = frm._ViewIndex

            mstrAnalysis_Fields = frm._Analysis_Fields
            mstrAnalysis_Join = frm._Analysis_Join
            mstrAnalysis_OrderBy = frm._Analysis_OrderBy
            mstrReport_GroupName = frm._Report_GroupName
            mstrAnalysis_OrderBy_GroupName = frm._Analysis_OrderBy_GName
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "", mstrModuleName)
        Finally
            frm = Nothing
        End Try
    End Sub

    

    

#End Region


	'<Language> This Auto Generated Text Please Do Not Modify it.
#Region " Language & UI Settings "
	Private Sub OtherSettings()
		Try
			Me.SuspendLayout()
			
			Call SetLanguage()
			
			Me.gbFilterCriteria.GradientColor = GUI._eZeeContainerHeaderBackColor 
			Me.gbFilterCriteria.ForeColor = GUI._eZeeContainerHeaderForeColor 


			
			Me.eZeeHeader.GradientColor1 = GUI._HeaderBackColor1 
			Me.eZeeHeader.GradientColor2 = GUI._HeaderBackColor2 
			Me.eZeeHeader.BorderColor = GUI._HeaderBorderColor 
			Me.eZeeHeader.HeaderTextForeColor = GUI._HeaderTitleFontColor 
			Me.eZeeHeader.DescriptionForeColor = GUI._HeaderMessageFontColor 


			Me.btnSaveSelection.GradientBackColor = GUI._ButttonBackColor 
			Me.btnSaveSelection.GradientForeColor = GUI._ButttonFontColor

			Me.btnReset.GradientBackColor = GUI._ButttonBackColor 
			Me.btnReset.GradientForeColor = GUI._ButttonFontColor

			Me.btnExport.GradientBackColor = GUI._ButttonBackColor 
			Me.btnExport.GradientForeColor = GUI._ButttonFontColor

			Me.btnClose.GradientBackColor = GUI._ButttonBackColor 
			Me.btnClose.GradientForeColor = GUI._ButttonFontColor

			Me.objbtnLanguage.GradientBackColor = GUI._ButttonBackColor 
			Me.objbtnLanguage.GradientForeColor = GUI._ButttonFontColor


			Me.ResumeLayout()
		Catch Ex As Exception
			DisplayError.Show("-1", ex.Message, "OtherSettings", mstrModuleName)
		End Try
	End Sub
			
			
	Private Sub SetLanguage()
		Try
			Me.Text = Language._Object.getCaption(Me.Name, Me.Text)
			
			Me.gbFilterCriteria.Text = Language._Object.getCaption(Me.gbFilterCriteria.Name, Me.gbFilterCriteria.Text)
			Me.btnSaveSelection.Text = Language._Object.getCaption(Me.btnSaveSelection.Name, Me.btnSaveSelection.Text)
			Me.lblMembership.Text = Language._Object.getCaption(Me.lblMembership.Name, Me.lblMembership.Text)
			Me.lnkSetAnalysis.Text = Language._Object.getCaption(Me.lnkSetAnalysis.Name, Me.lnkSetAnalysis.Text)
			Me.colhCode.Text = Language._Object.getCaption(CStr(Me.colhCode.Tag), Me.colhCode.Text)
			Me.colhName.Text = Language._Object.getCaption(CStr(Me.colhName.Tag), Me.colhName.Text)
			Me.lblPeriod.Text = Language._Object.getCaption(Me.lblPeriod.Name, Me.lblPeriod.Text)
			Me.lblEmployee.Text = Language._Object.getCaption(Me.lblEmployee.Name, Me.lblEmployee.Text)
			Me.btnReset.Text = Language._Object.getCaption(Me.btnReset.Name, Me.btnReset.Text)
			Me.btnExport.Text = Language._Object.getCaption(Me.btnExport.Name, Me.btnExport.Text)
			Me.btnClose.Text = Language._Object.getCaption(Me.btnClose.Name, Me.btnClose.Text)
			Me.eZeeHeader.Title = Language._Object.getCaption(Me.eZeeHeader.Name & "_Title" , Me.eZeeHeader.Title)
			Me.eZeeHeader.Message = Language._Object.getCaption(Me.eZeeHeader.Name & "_Message" , Me.eZeeHeader.Message)

		Catch Ex As Exception
			DisplayError.Show("-1", ex.Message, "SetLanguage", mstrModuleName)
		End Try
	End Sub
			
			
	Private Sub SetMessages()
		Try
			Language.setMessage(mstrModuleName, 1, "Effective Period is mandatory information. Please select Effective Period.")
			Language.setMessage(mstrModuleName, 2, "Please check Periods to view report.")
			Language.setMessage(mstrModuleName, 3, "Data Exported Successfuly.")
			Language.setMessage(mstrModuleName, 4, "Selection Saved Successfully")

		Catch Ex As Exception
			DisplayError.Show("-1", ex.Message, "SetMessages", mstrModuleName)
		End Try
	End Sub
#End Region 'Language & UI Settings
	'</Language>

   
End Class
