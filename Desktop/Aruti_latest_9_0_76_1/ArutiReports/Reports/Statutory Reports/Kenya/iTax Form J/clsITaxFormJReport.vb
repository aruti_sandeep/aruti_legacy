'************************************************************************************************************************************
'Class Name : clsITaxFormJReport.vb
'Purpose    :
'Date       :30/10/2015
'Written By :Sohail
'Modified   :
'************************************************************************************************************************************

Imports eZeeCommonLib
Imports Aruti.Data
Imports System.IO
Imports System.Text

''' <summary>
''' Purpose: Report Generation Class 
''' Developer: Sohail
''' </summary>
Public Class clsITaxFormJReport
    Inherits IReportData
    Private Shared ReadOnly mstrModuleName As String = "clsITaxFormJReport"
    Private mstrReportId As String = enArutiReport.ITax_Form_J_Report
    Dim objDataOperation As clsDataOperation

#Region " Constructor "

    Public Sub New(Byval intLangId As Integer,Byval intCompanyId AS Integer) 'S.SANDEEP |24-Sep-2020| -- START --END
        Me.setReportData(CInt(mstrReportId),intLangId,intCompanyId) 'S.SANDEEP |24-Sep-2020| -- START --END
        Call Create_OnDetailReport()
    End Sub

#End Region

#Region " Private variables "
    Private mintPeriodId As Integer = 0
    Private mstrPeriodName As String = ""
    Private mintEmployeeId As Integer = -1
    Private mstrEmployeeName As String = String.Empty
    Private mintSchemeId As Integer = 0
    Private mstrSchemeName As String = ""
    Private mstrSchemeListIDs As String = String.Empty
    Private mintMembershipId As Integer = 0
    Private mstrMembershipName As String = String.Empty
    Private mblnIsActive As Boolean = True

    Private mdtPeriodStartDate As Date
    Private mdtPeriodEndDate As Date

    Private mblnFirstNamethenSurname As Boolean = ConfigParameter._Object._FirstNamethenSurname

    'Private mintColumnFTranId As Integer = 0
    'Private mintColumnGTranId As Integer = 0
    'Private mintColumnHTranId As Integer = 0
    'Private mintColumnITranId As Integer = 0
    'Private mintColumnJTranId As Integer = 0
    'Private mintColumnKTranId As Integer = 0
    'Private mintOtherEarningTranId As Integer = 0
   
    'Private mintColumnYTranId As Integer = 0
    'Private mintColumnAFTranId As Integer = 0

    Private mintViewIndex As Integer = -1
    Private mstrViewByIds As String = String.Empty
    Private mstrViewByName As String = String.Empty
    Private mstrAnalysis_Fields As String = ""
    Private mstrAnalysis_Join As String = ""
    Private mstrAnalysis_OrderBy As String = ""
    Private mstrReport_GroupName As String = ""
    Private mstrAnalysis_OrderBy_GroupName As String = ""
    'Nilay (10-Oct-2015) -- Start
    'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
    Private mintCurrencyID As Integer = -1
    Private mstrBaseCurrencySign As String = ""
    'Nilay (10-Oct-2015) -- End

#End Region

#Region " Properties "

    Public WriteOnly Property _PeriodId() As Integer
        Set(ByVal value As Integer)
            mintPeriodId = value
        End Set
    End Property

    Public WriteOnly Property _PeriodName() As String
        Set(ByVal value As String)
            mstrPeriodName = value
        End Set
    End Property
    Public WriteOnly Property _EmployeeId() As Integer
        Set(ByVal value As Integer)
            mintEmployeeId = value
        End Set
    End Property

    Public WriteOnly Property _EmployeeName() As String
        Set(ByVal value As String)
            mstrEmployeeName = value
        End Set
    End Property

    Public WriteOnly Property _SchemeId() As Integer
        Set(ByVal value As Integer)
            mintSchemeId = value
        End Set
    End Property

    Public WriteOnly Property _SchemeName() As String
        Set(ByVal value As String)
            mstrSchemeName = value
        End Set
    End Property

    Public WriteOnly Property _SchemeListIDs() As String
        Set(ByVal value As String)
            mstrSchemeListIDs = value
        End Set
    End Property

    Public WriteOnly Property _MembershipId() As Integer
        Set(ByVal value As Integer)
            mintMembershipId = value
        End Set
    End Property

    Public WriteOnly Property _MembershipName() As String
        Set(ByVal value As String)
            mstrMembershipName = value
        End Set
    End Property

    Public WriteOnly Property _PeriodStartDate() As Date
        Set(ByVal value As Date)
            mdtPeriodStartDate = value
        End Set
    End Property

    Public WriteOnly Property _PeriodEndDate() As Date
        Set(ByVal value As Date)
            mdtPeriodEndDate = value
        End Set
    End Property

    Public WriteOnly Property _IsActive() As Boolean
        Set(ByVal value As Boolean)
            mblnIsActive = value
        End Set
    End Property

    'Public WriteOnly Property _ColumnFTranId() As Integer
    '    Set(ByVal value As Integer)
    '        mintColumnFTranId = value
    '    End Set
    'End Property

    'Public WriteOnly Property _ColumnGTranId() As Integer
    '    Set(ByVal value As Integer)
    '        mintColumnGTranId = value
    '    End Set
    'End Property

    'Public WriteOnly Property _ColumnHTranId() As Integer
    '    Set(ByVal value As Integer)
    '        mintColumnHTranId = value
    '    End Set
    'End Property

    'Public WriteOnly Property _ColumnITranId() As Integer
    '    Set(ByVal value As Integer)
    '        mintColumnITranId = value
    '    End Set
    'End Property

    'Public WriteOnly Property _ColumnJTranId() As Integer
    '    Set(ByVal value As Integer)
    '        mintColumnJTranId = value
    '    End Set
    'End Property

    'Public WriteOnly Property _ColumnKTranId() As Integer
    '    Set(ByVal value As Integer)
    '        mintColumnKTranId = value
    '    End Set
    'End Property

    'Public WriteOnly Property _OtherEarningTranId() As Integer
    '    Set(ByVal value As Integer)
    '        mintOtherEarningTranId = value
    '    End Set
    'End Property

    'Public WriteOnly Property _ColumnYTranId() As Integer
    '    Set(ByVal value As Integer)
    '        mintColumnYTranId = value
    '    End Set
    'End Property

    'Public WriteOnly Property _ColumnAFTranId() As Integer
    '    Set(ByVal value As Integer)
    '        mintColumnAFTranId = value
    '    End Set
    'End Property

    Public WriteOnly Property _ViewIndex() As Integer
        Set(ByVal value As Integer)
            mintViewIndex = value
        End Set
    End Property

    Public WriteOnly Property _ViewByIds() As String
        Set(ByVal value As String)
            mstrViewByIds = value
        End Set
    End Property

    Public WriteOnly Property _ViewByName() As String
        Set(ByVal value As String)
            mstrViewByName = value
        End Set
    End Property

    Public WriteOnly Property _Analysis_Fields() As String
        Set(ByVal value As String)
            mstrAnalysis_Fields = value
        End Set
    End Property

    Public WriteOnly Property _Analysis_Join() As String
        Set(ByVal value As String)
            mstrAnalysis_Join = value
        End Set
    End Property

    Public WriteOnly Property _Analysis_OrderBy() As String
        Set(ByVal value As String)
            mstrAnalysis_OrderBy = value
        End Set
    End Property

    Public WriteOnly Property _Report_GroupName() As String
        Set(ByVal value As String)
            mstrReport_GroupName = value
        End Set
    End Property

    Public WriteOnly Property _Analysis_OrderBy_GName() As String
        Set(ByVal value As String)
            mstrAnalysis_OrderBy_GroupName = value
        End Set
    End Property

    'Nilay (10-Oct-2015) -- Start
    'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
    Public WriteOnly Property _CurrencyId() As Integer
        Set(ByVal value As Integer)
            mintCurrencyID = value
        End Set
    End Property

    Public WriteOnly Property _BaseCurrencySign() As String
        Set(ByVal value As String)
            mstrBaseCurrencySign = value
        End Set
    End Property
    'Nilay (10-Oct-2015) -- End

#End Region

#Region " Public Function & Procedures "

    Public Sub SetDefaultValue()
        Try
            mintEmployeeId = 0
            mstrEmployeeName = ""
            mintPeriodId = 0
            mstrPeriodName = ""
            mintSchemeId = 0
            mstrSchemeName = ""
            mblnIsActive = True
            mstrSchemeListIDs = ""
            mintMembershipId = 0
            mstrMembershipName = ""
            'mintOtherEarningTranId = 0
            'mintColumnFTranId = 0
            'mintColumnGTranId = 0
            'mintColumnHTranId = 0
            'mintColumnITranId = 0
            'mintColumnJTranId = 0
            'mintColumnKTranId = 0
            'mintColumnYTranId = 0
            'mintColumnAFTranId = 0

            mintViewIndex = -1
            mstrViewByIds = ""
            mstrViewByName = ""
            mstrAnalysis_Fields = ""
            mstrAnalysis_Join = ""
            mstrAnalysis_OrderBy = ""
            mstrReport_GroupName = ""
            mstrAnalysis_OrderBy_GroupName = ""

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: SetDefaultValue; Module Name: " & mstrModuleName)
        End Try
    End Sub

    Private Sub FilterTitleAndFilterQuery()
        Me._FilterQuery = ""
        Me._FilterTitle = ""
        Try
            objDataOperation.AddParameter("@membershipunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintMembershipId)
            'objDataOperation.AddParameter("@column_f_id", SqlDbType.Int, eZeeDataType.INT_SIZE, mintColumnFTranId)
            'objDataOperation.AddParameter("@column_g_id", SqlDbType.Int, eZeeDataType.INT_SIZE, mintColumnGTranId)
            'objDataOperation.AddParameter("@column_h_id", SqlDbType.Int, eZeeDataType.INT_SIZE, mintColumnHTranId)
            'objDataOperation.AddParameter("@column_i_id", SqlDbType.Int, eZeeDataType.INT_SIZE, mintColumnITranId)
            'objDataOperation.AddParameter("@column_j_id", SqlDbType.Int, eZeeDataType.INT_SIZE, mintColumnJTranId)
            'objDataOperation.AddParameter("@column_k_id", SqlDbType.Int, eZeeDataType.INT_SIZE, mintColumnKTranId)
            'objDataOperation.AddParameter("@column_y_id", SqlDbType.Int, eZeeDataType.INT_SIZE, mintColumnYTranId)
            'objDataOperation.AddParameter("@column_af_id", SqlDbType.Int, eZeeDataType.INT_SIZE, mintColumnAFTranId)

            If mintEmployeeId > 0 Then
                objDataOperation.AddParameter("@EmpId", SqlDbType.Int, eZeeDataType.INT_SIZE, mintEmployeeId)
                Me._FilterQuery &= " AND empid = @EmpId "
            End If

            'Nilay (10-Oct-2015) -- Start
            'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
            If mintPeriodId > 0 Then
                objDataOperation.AddParameter("@PeriodId", SqlDbType.Int, eZeeDataType.INT_SIZE, mintPeriodId)
                Me._FilterTitle &= Language.getMessage(mstrModuleName, 3, " Period : ") & " " & mstrPeriodName & " "
            End If
            'Nilay (10-Oct-2015) -- End

            'If mintOtherEarningTranId > 0 Then
            '    objDataOperation.AddParameter("@OtherEarningTranId", SqlDbType.Int, eZeeDataType.INT_SIZE, mintOtherEarningTranId)
            'End If

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: FilterTitleAndFilterQuery; Module Name: " & mstrModuleName)
        End Try
    End Sub

    'Nilay (10-Oct-2015) -- Start
    'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
    Public Overloads Overrides Sub generateReport(ByVal pintReportType As Integer, Optional ByVal PrintAction As Aruti.Data.enPrintAction = Aruti.Data.enPrintAction.Preview, Optional ByVal ExportAction As Aruti.Data.enExportAction = Aruti.Data.enExportAction.None)
        'Dim objRpt As CrystalDecisions.CrystalReports.Engine.ReportClass = Nothing
        'Dim strReportExportFile As String = ""
        Try
            'If Not IsNothing(objRpt) Then
            '    Call ReportExecute(objRpt, PrintAction, ExportAction, ConfigParameter._Object._ExportReportPath, ConfigParameter._Object._OpenAfterExport)
            'End If
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: generateReport; Module Name: " & mstrModuleName)
        End Try
    End Sub

    Public Overrides Sub generateReportNew(ByVal xDatabaseName As String, _
                                           ByVal xUserUnkid As Integer, _
                                           ByVal xYearUnkid As Integer, _
                                           ByVal xCompanyUnkid As Integer, _
                                           ByVal xPeriodStart As Date, _
                                           ByVal xPeriodEnd As Date, _
                                           ByVal xUserModeSetting As String, _
                                           ByVal xOnlyApproved As Boolean, _
                                           ByVal xExportReportPath As String, _
                                           ByVal xOpenReportAfterExport As Boolean, _
                                           ByVal pintReportType As Integer, _
                                           Optional ByVal PrintAction As enPrintAction = enPrintAction.Preview, _
                                           Optional ByVal ExportAction As enExportAction = enExportAction.None, _
                                           Optional ByVal xBaseCurrencyId As Integer = 0)

        Dim objRpt As CrystalDecisions.CrystalReports.Engine.ReportClass = Nothing
        Dim strReportExportFile As String = ""
        Try
            If Not IsNothing(objRpt) Then
                Call ReportExecute(objRpt, PrintAction, ExportAction, ConfigParameter._Object._ExportReportPath, ConfigParameter._Object._OpenAfterExport)
            End If
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: generateReportNew; Module Name: " & mstrModuleName)
        End Try
    End Sub
    'Nilay (10-Oct-2015) -- End

    Public Overrides Sub setDefaultOrderBy(ByVal intReportType As Integer)
        OrderByDisplay = ""
        OrderByQuery = ""
        Try
            OrderByDisplay = iColumn_DetailReport.ColumnItem(0).DisplayName
            OrderByQuery = iColumn_DetailReport.ColumnItem(0).Name
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: setDefaultOrderBy; Module Name: " & mstrModuleName)
        End Try
    End Sub

    Public Overrides Sub setOrderBy(ByVal intReportType As Integer)
        Try
            Call OrderByExecute(iColumn_DetailReport)
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: setOrderBy; Module Name: " & mstrModuleName)
        End Try
    End Sub

#End Region

#Region " Report Generation "

    Dim iColumn_DetailReport As New IColumnCollection

    Public Property Field_OnDetailReport() As IColumnCollection
        Get
            Return iColumn_DetailReport
        End Get
        Set(ByVal value As IColumnCollection)
            iColumn_DetailReport = value
        End Set
    End Property

    Private Sub Create_OnDetailReport()
        Try
            iColumn_DetailReport.Clear()
            iColumn_DetailReport.Add(New IColumn("ISNULL(hremployee_master.employeecode,'')", Language.getMessage(mstrModuleName, 1, "Employee Code")))

            iColumn_DetailReport.Add(New IColumn("ISNULL(hremployee_master.firstname, '') + ' ' + ISNULL(hremployee_master.othername, '') + ' '+ ISNULL(hremployee_master.surname, ' ')", Language.getMessage(mstrModuleName, 2, "Employee Name")))

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: Create_OnDetailReport; Module Name: " & mstrModuleName)
        End Try
    End Sub

    'Sohail (30 Oct 2015) -- Start
    'Enhancement - New Statutory Report I-TAX Form J Report and option for Prescribed Quarterly Interest Rate on Period Master for Kenya.
    Public Function Generate_PayrollReport(ByVal xDatabaseName As String, _
                                           ByVal xUserUnkid As Integer, _
                                           ByVal xYearUnkid As Integer, _
                                           ByVal xCompanyUnkid As Integer, _
                                           ByVal xPeriodStart As Date, _
                                           ByVal xPeriodEnd As Date, _
                                           ByVal xUserModeSetting As String, _
                                           ByVal xOnlyApproved As Boolean, _
                                           ByVal xExportReportPath As String) As Boolean

        Dim objRpt As CrystalDecisions.CrystalReports.Engine.ReportClass = Nothing
        Dim StrQ As String = ""
        Dim strFilter As String = ""
        Dim strAllocationJoin As String = ""
        Dim dsList As New DataSet
        'Dim rpt_Data As ArutiReport.Designer.dsArutiReport
        Dim strBuilder As New StringBuilder

        Try

            Dim xDateJoinQry, xDateFilterQry, xUACQry, xUACFiltrQry, xAdvanceJoinQry As String
            xDateJoinQry = "" : xDateFilterQry = "" : xUACQry = "" : xUACFiltrQry = "" : xAdvanceJoinQry = ""
            Call GetDatesFilterString(xDateJoinQry, xDateFilterQry, xPeriodStart, xPeriodEnd, , , xDatabaseName)
            Call NewAccessLevelFilterString(xUACQry, xUACFiltrQry, xPeriodEnd, xOnlyApproved, xDatabaseName, xUserUnkid, xCompanyUnkid, xYearUnkid, xUserModeSetting)
            Call GetAdvanceFilterQry(xAdvanceJoinQry, xPeriodEnd, xDatabaseName)

            Dim strExtraReportFilterValue As String = ""

            'Nilay (18-Nov-2015) -- Start
            'Dim intFirstOpenPeriodID As Integer = (New clsMasterData).getFirstPeriodID(enModuleReference.Payroll, 1, xYearUnkid, , True)
            Dim intFirstOpenPeriodID As Integer = (New clsMasterData).getFirstPeriodID(enModuleReference.Payroll, xYearUnkid, 1, , True)
            'Nilay (18-Nov-2015) -- End

            objDataOperation = New clsDataOperation

            strFilter &= "AND lnloan_balance_tran.transaction_periodunkid <> '" & intFirstOpenPeriodID & "' "

            If mstrSchemeListIDs.Trim <> "" Then
                strFilter &= " AND lnloan_advance_tran.loanschemeunkid IN (" & mstrSchemeListIDs & ") "
            End If

            If mintEmployeeId > 0 Then
                strFilter &= " AND prpayrollprocess_tran.employeeunkid = '" & mintEmployeeId & "' "
                strExtraReportFilterValue &= " AND hremployee_master.employeeunkid = '" & mintEmployeeId & "' "
            End If

            'If mstrAdvance_Filter.Trim.Length > 0 Then
            '    strFilter &= " AND " & mstrAdvance_Filter
            '    strExtraReportFilterValue &= " AND " & mstrAdvance_Filter
            'End If

            If mblnIsActive = False Then
                If xDateFilterQry.Trim.Length > 0 Then
                    strFilter &= xDateFilterQry
                End If
            End If

            'If mintBranchId > 0 Then
            '    strFilter &= " AND Alloc.stationunkid = '" & mintBranchId & "' "
            '    strExtraReportFilterValue &= " AND Alloc.stationunkid = '" & mintBranchId & "' "
            'End If

            strAllocationJoin &= "LEFT JOIN " & _
                                 "( " & _
                                 "      SELECT " & _
                                 "           stationunkid " & _
                                 "          ,employeeunkid " & _
                                 "          ,ROW_NUMBER()OVER(PARTITION BY employeeunkid ORDER BY effectivedate DESC) AS rno " & _
                                 "      FROM hremployee_transfer_tran " & _
                                 "      WHERE isvoid = 0 AND CONVERT(CHAR(8),effectivedate,112) <= '" & eZeeDate.convertDate(xPeriodEnd) & "' " & _
                                 ") AS Alloc ON Alloc.employeeunkid = hremployee_master.employeeunkid AND Alloc.rno = 1 "

            If xUACFiltrQry.Trim.Length > 0 Then
                strFilter &= " AND " & xUACFiltrQry
                strExtraReportFilterValue &= " AND " & xUACFiltrQry
            End If

            If mintPeriodId > 0 Then
                strFilter &= "AND lnloan_balance_tran.transaction_periodunkid = @PeriodId "
            End If

            StrQ = "SELECT  Loan.transaction_periodunkid  " & _
                   "    , Loan.period_name AS TranPeriod_Name " & _
                   "    , Loan.end_date AS TranPeriod_end_date " & _
                   "    , Loan.employeeunkid " & _
                   "    , Loan.EmpCode " & _
                   "    , Loan.EmpName " & _
                   "    , Loan.loanadvancetranunkid " & _
                   "    , Loan.loanschemeunkid " & _
                   "    , Loan.loanvoucher_no " & _
                   "    , Loan.LoanSchemeCode " & _
                   "    , Loan.LoanSchemeName " & _
                   "    , BF.loanbalancetranunkid " & _
                   "    , Loan.interest_rate " & _
                   "    , BF.bf_balance " & _
                   "    , BF.bf_balancePaidCurrency " & _
                   "    , Loan.amount " & _
                   "    , Loan.amountPaidCurrency " & _
                   "    , Loan.repayment_amount " & _
                   "    , Loan.repayment_amountPaidCurrency " & _
                   "    , Loan.topup_amount " & _
                   "    , Loan.topup_amountPaidCurrency " & _
                   "    , Loan.principal_amount " & _
                   "    , Loan.principal_amountPaidCurrency " & _
                   "    , Loan.interest_amount " & _
                   "    , Loan.interest_amountPaidCurrency " & _
                   "    , CF.cf_balance " & _
                   "    , CF.cf_balancePaidCurrency " & _
                   "    , BF.isonhold " & _
                   "    , Currency.countryunkid " & _
                   "    , Currency.currency_sign " & _
                   "    , ISNULL(M.membershipno,'') AS MembershipNo " & _
                   "    , prescribed_interest_rate " & _
                   "FROM " & _
                   "( " & _
                   "        SELECT  lnloan_balance_tran.transaction_periodunkid  " & _
                   "                , cfcommon_period_tran.period_name " & _
                   "                , CONVERT(CHAR(8), cfcommon_period_tran.end_date, 112) AS end_date " & _
                   "                , lnloan_balance_tran.employeeunkid " & _
                   "                , hremployee_master.employeecode AS EmpCode " & _
                   "                , lnloan_balance_tran.loanadvancetranunkid " & _
                   "                , lnloan_balance_tran.loanschemeunkid " & _
                   "                , lnloan_advance_tran.loanvoucher_no " & _
                   "                , lnloan_scheme_master.code AS LoanSchemeCode " & _
                   "                , lnloan_scheme_master.name AS LoanSchemeName " & _
                   "                , SUM(lnloan_balance_tran.amount) AS amount " & _
                   "                , SUM(lnloan_balance_tran.amountPaidCurrency) AS amountPaidCurrency " & _
                   "                , SUM(lnloan_balance_tran.repayment_amount) AS repayment_amount " & _
                   "                , SUM(lnloan_balance_tran.repayment_amountPaidCurrency) AS repayment_amountPaidCurrency " & _
                   "                , SUM(lnloan_balance_tran.topup_amount) AS topup_amount " & _
                   "                , SUM(lnloan_balance_tran.topup_amountPaidCurrency) AS topup_amountPaidCurrency " & _
                   "                , SUM(lnloan_balance_tran.principal_amount) AS principal_amount " & _
                   "                , SUM(lnloan_balance_tran.principal_amountPaidCurrency) AS principal_amountPaidCurrency " & _
                   "                , SUM(lnloan_balance_tran.interest_amount) AS interest_amount " & _
                   "                , SUM(lnloan_balance_tran.interest_amountPaidCurrency) AS interest_amountPaidCurrency " & _
                   "                , lnloan_advance_tran.countryunkid " & _
                   "                , lnloan_advance_tran.exchange_rate " & _
                   "                , cfcommon_period_tran.prescribed_interest_rate "

            If mblnFirstNamethenSurname = False Then
                StrQ &= "           , ISNULL(hremployee_master.surname, '') + ' ' + ISNULL(hremployee_master.firstname, '') + ' ' + ISNULL(hremployee_master.othername, '') AS EmpName "
            Else
                StrQ &= "           , ISNULL(hremployee_master.firstname, '') + ' ' + ISNULL(hremployee_master.othername, '') + ' ' + ISNULL(hremployee_master.surname, '') AS EmpName "
            End If

            StrQ &= "               , lnloan_balance_tran.interest_rate "

            StrQ &= "       FROM    lnloan_balance_tran " & _
                    "               LEFT JOIN lnloan_advance_tran ON lnloan_advance_tran.loanadvancetranunkid = lnloan_balance_tran.loanadvancetranunkid " & _
                    "               LEFT JOIN hremployee_master ON lnloan_advance_tran.employeeunkid = hremployee_master.employeeunkid " & _
                    "               LEFT JOIN lnloan_scheme_master ON lnloan_balance_tran.loanschemeunkid = lnloan_scheme_master.loanschemeunkid " & _
                    "               LEFT JOIN cfcommon_period_tran ON cfcommon_period_tran.periodunkid = lnloan_balance_tran.transaction_periodunkid "

            StrQ &= strAllocationJoin & " "

            If xDateJoinQry.Trim.Length > 0 Then
                StrQ &= xDateJoinQry
            End If

            If xUACQry.Trim.Length > 0 Then
                StrQ &= xUACQry
            End If

            If xAdvanceJoinQry.Trim.Length > 0 Then
                StrQ &= xAdvanceJoinQry
            End If

            StrQ &= "       WHERE   lnloan_balance_tran.isvoid = 0 " & _
                    "               AND lnloan_advance_tran.isvoid = 0 " & _
                    "               AND lnloan_scheme_master.isactive = 1 " & _
                    "               AND cfcommon_period_tran.isactive = 1 " & _
                    "               AND cfcommon_period_tran.modulerefid = " & enModuleReference.Payroll & " " & _
                    "               AND ISNULL(lnloan_balance_tran.isbrought_forward, 0) = 0 " & _
                                       strFilter.Replace("prpayrollprocess_tran", "lnloan_balance_tran")


            StrQ &= "       GROUP BY  cfcommon_period_tran.end_date " & _
                    "               , cfcommon_period_tran.period_name " & _
                    "               , lnloan_balance_tran.transaction_periodunkid " & _
                    "               , lnloan_balance_tran.employeeunkid " & _
                    "               , hremployee_master.employeecode " & _
                    "               , lnloan_balance_tran.loanadvancetranunkid " & _
                    "               , lnloan_balance_tran.loanschemeunkid " & _
                    "               , lnloan_advance_tran.loanvoucher_no " & _
                    "               , lnloan_scheme_master.code " & _
                    "               , lnloan_scheme_master.name " & _
                    "               , lnloan_advance_tran.countryunkid " & _
                    "               , lnloan_advance_tran.exchange_rate " & _
                    "               , cfcommon_period_tran.prescribed_interest_rate "

            If mblnFirstNamethenSurname = False Then
                StrQ &= "           , ISNULL(hremployee_master.surname, '') + ' ' + ISNULL(hremployee_master.firstname, '') + ' ' + ISNULL(hremployee_master.othername, '') "
            Else
                StrQ &= "           , ISNULL(hremployee_master.firstname, '') + ' ' + ISNULL(hremployee_master.othername, '') + ' ' + ISNULL(hremployee_master.surname, '') "
            End If

            'If mblnShowInterestInformation = True Then
            StrQ &= "               , lnloan_balance_tran.interest_rate "
            'End If

            StrQ &= ")  AS Loan " & _
                    "   LEFT JOIN ( " & _
                    "               SELECT    A.transaction_periodunkid  " & _
                    "                       , A.employeeunkid " & _
                    "                       , A.loanadvancetranunkid " & _
                    "                       , A.loanbalancetranunkid " & _
                    "                       , A.bf_balance " & _
                    "                       , A.bf_balancePaidCurrency " & _
                    "                       , A.interest_rate " & _
                    "                       , A.isonhold " & _
                    "                       , A.countryunkid " & _
                    "                       , A.exchange_rate " & _
                    "               FROM ( " & _
                    "                       SELECT  lnloan_balance_tran.transaction_periodunkid  " & _
                    "                             , lnloan_balance_tran.employeeunkid " & _
                    "                             , lnloan_balance_tran.loanadvancetranunkid " & _
                    "                             , lnloan_balance_tran.loanbalancetranunkid " & _
                    "                             , lnloan_balance_tran.bf_balance " & _
                    "                             , lnloan_balance_tran.bf_balancePaidCurrency AS bf_balancePaidCurrency " & _
                    "                             , lnloan_balance_tran.isonhold " & _
                    "                             , lnloan_advance_tran.countryunkid " & _
                    "                             , lnloan_advance_tran.exchange_rate "

            'If mblnShowInterestInformation = True Then
            StrQ &= "                             , lnloan_balance_tran.interest_rate " & _
                    "                             , DENSE_RANK() OVER ( PARTITION BY lnloan_balance_tran.transaction_periodunkid, lnloan_balance_tran.loanadvancetranunkid, lnloan_balance_tran.interest_rate ORDER BY lnloan_balance_tran.transactiondate, lnloan_balance_tran.loanbalancetranunkid ) AS BF_ROWNO "
            'Else
            'StrQ &= "                                  , CAST(0 AS FLOAT) AS interest_rate " & _
            '                                          ", DENSE_RANK() OVER ( PARTITION BY lnloan_balance_tran.transaction_periodunkid, lnloan_balance_tran.loanadvancetranunkid ORDER BY lnloan_balance_tran.transactiondate, lnloan_balance_tran.loanbalancetranunkid ) AS BF_ROWNO "
            'End If


            StrQ &= "                       FROM    lnloan_balance_tran " & _
                    "                            LEFT JOIN lnloan_advance_tran ON lnloan_advance_tran.loanadvancetranunkid = lnloan_balance_tran.loanadvancetranunkid " & _
                    "                            LEFT JOIN hremployee_master ON lnloan_advance_tran.employeeunkid = hremployee_master.employeeunkid "

            StrQ &= strAllocationJoin & " "


            If xDateJoinQry.Trim.Length > 0 Then
                StrQ &= xDateJoinQry
            End If

            If xUACQry.Trim.Length > 0 Then
                StrQ &= xUACQry
            End If

            If xAdvanceJoinQry.Trim.Length > 0 Then
                StrQ &= xAdvanceJoinQry
            End If

            StrQ &= "                       WHERE   lnloan_balance_tran.isvoid = 0 " & _
                    "                            AND lnloan_advance_tran.isvoid = 0 " & _
                    "                            AND ISNULL(lnloan_balance_tran.isbrought_forward, 0) = 0 " & _
                                                 strFilter.Replace("prpayrollprocess_tran", "lnloan_balance_tran") & _
                    "                    ) AS A " & _
                    "               WHERE   A.BF_ROWNO = 1 " & _
                    "             ) AS BF ON BF.transaction_periodunkid = Loan.transaction_periodunkid " & _
                    "             AND BF.loanadvancetranunkid = Loan.loanadvancetranunkid " & _
                    "             AND BF.interest_rate = Loan.interest_rate "

            StrQ &= "   LEFT JOIN ( " & _
                    "               SELECT  A.transaction_periodunkid  " & _
                    "                     , A.employeeunkid " & _
                    "                     , A.loanadvancetranunkid " & _
                    "                     , A.loanbalancetranunkid " & _
                    "                     , A.cf_balance " & _
                    "                     , A.cf_balancePaidCurrency " & _
                    "                     , A.interest_rate " & _
                    "                     , A.isonhold " & _
                    "                     , A.countryunkid " & _
                    "                     , A.exchange_rate " & _
                    "               FROM ( " & _
                    "                       SELECT lnloan_balance_tran.transaction_periodunkid  " & _
                    "                            , lnloan_balance_tran.employeeunkid " & _
                    "                            , lnloan_balance_tran.loanadvancetranunkid " & _
                    "                            , lnloan_balance_tran.loanbalancetranunkid " & _
                    "                            , lnloan_balance_tran.cf_balance AS cf_balance " & _
                    "                            , lnloan_balance_tran.cf_balancePaidCurrency AS cf_balancePaidCurrency " & _
                    "                            , lnloan_balance_tran.isonhold " & _
                    "                            , lnloan_advance_tran.countryunkid " & _
                    "                            , lnloan_advance_tran.exchange_rate "

            'If mblnShowInterestInformation = True Then
            StrQ &= "                            , lnloan_balance_tran.interest_rate " & _
                    "                            , DENSE_RANK() OVER ( PARTITION BY lnloan_balance_tran.transaction_periodunkid, lnloan_balance_tran.loanadvancetranunkid, lnloan_balance_tran.interest_rate ORDER BY lnloan_balance_tran.transactiondate DESC, lnloan_balance_tran.loanbalancetranunkid DESC ) AS CF_ROWNO "
            'Else
            'StrQ &= "                                  , CAST(0 AS FLOAT) AS interest_rate " & _
            '                                          ", DENSE_RANK() OVER ( PARTITION BY lnloan_balance_tran.transaction_periodunkid, lnloan_balance_tran.loanadvancetranunkid ORDER BY lnloan_balance_tran.transactiondate DESC, lnloan_balance_tran.loanbalancetranunkid DESC ) AS CF_ROWNO "
            'End If

            StrQ &= "                       FROM    lnloan_balance_tran " & _
                    "                            LEFT JOIN lnloan_advance_tran ON lnloan_advance_tran.loanadvancetranunkid = lnloan_balance_tran.loanadvancetranunkid " & _
                    "                            LEFT JOIN hremployee_master ON lnloan_advance_tran.employeeunkid = hremployee_master.employeeunkid "

            StrQ &= strAllocationJoin & " "

            If xDateJoinQry.Trim.Length > 0 Then
                StrQ &= xDateJoinQry
            End If

            If xUACQry.Trim.Length > 0 Then
                StrQ &= xUACQry
            End If

            If xAdvanceJoinQry.Trim.Length > 0 Then
                StrQ &= xAdvanceJoinQry
            End If

            StrQ &= "                       WHERE    lnloan_balance_tran.isvoid = 0 " & _
                    "                            AND lnloan_advance_tran.isvoid = 0 " & _
                    "                            AND ISNULL(lnloan_balance_tran.isbrought_forward, 0) = 0 " & _
                                                 strFilter.Replace("prpayrollprocess_tran", "lnloan_balance_tran") & _
                    "                    ) AS A " & _
                    "                    WHERE   A.CF_ROWNO = 1 " & _
                    "             ) AS CF ON CF.transaction_periodunkid = Loan.transaction_periodunkid " & _
                    "             AND CF.loanadvancetranunkid = Loan.loanadvancetranunkid " & _
                    "             AND CF.interest_rate = Loan.interest_rate "

            StrQ &= "   LEFT JOIN ( " & _
                    "               SELECT  DISTINCT " & _
                    "                       cfexchange_rate.countryunkid  " & _
                    "                     , cfexchange_rate.currency_name " & _
                    "                     , cfexchange_rate.currency_sign " & _
                    "               FROM    cfexchange_rate " & _
                    "               WHERE   cfexchange_rate.isactive = 1 " & _
                    "              ) AS Currency ON Loan.countryunkid = Currency.countryunkid " & _
                    "   LEFT JOIN ( " & _
                    "               SELECT  employeeunkid " & _
                    "                     , membershipno = ISNULL(STUFF(( SELECT '; ' + membershipno " & _
                    "                                                     FROM hremployee_master " & _
                    "                                                       LEFT JOIN hremployee_meminfo_tran ON hremployee_master.employeeunkid = hremployee_meminfo_tran.employeeunkid " & _
                    "                                                     WHERE membershipno IS NOT NULL " & _
                    "                                                           AND ISNULL(hremployee_meminfo_tran.isdeleted,0) = 0 " & _
                    "                                                           AND hremployee_master.employeeunkid = a.employeeunkid "

            If mintMembershipId > 0 Then
                StrQ &= " AND membershipunkid = @membershipunkid "
            End If

            StrQ &= "                                                     FOR " & _
                    "                                                       XML PATH('')), 1, 2, ''), '') " & _
                    "               FROM hremployee_master a " & _
                    "               GROUP BY employeeunkid " & _
                    "             ) AS M ON M.employeeunkid = Loan.employeeunkid "

            StrQ &= "WHERE 1 = 1 "

            'If mblnShowOnHold = False Then
            '    StrQ &= "AND BF.isonhold = 0 "
            'End If

            If mintCurrencyID > 0 Then
                StrQ &= "AND Loan.countryunkid = " & mintCurrencyID & " "
            End If

            Call FilterTitleAndFilterQuery()

            dsList = objDataOperation.ExecQuery(StrQ, "DataTable")

            If objDataOperation.ErrorMessage <> "" Then
                Throw New Exception(objDataOperation.ErrorNumber & ":" & objDataOperation.ErrorMessage)
            End If

            Dim objPeriod As New clscommom_period_Tran
            objPeriod._Periodunkid(xDatabaseName) = intFirstOpenPeriodID
            If intFirstOpenPeriodID > 0 AndAlso (mintPeriodId <= 0 OrElse xPeriodEnd = objPeriod._End_Date) Then
                Dim strFS As String = ""
                'If mblnShowOnHold = False Then
                'strFS &= " AND isonhold = 0 "
                'End If

                If mintCurrencyID > 0 Then
                    strFS &= " AND countryunkid = " & mintCurrencyID & " "
                End If

                If strFS.Trim <> "" Then strFS = strFS.Substring(4)

                Dim objLoan As New clsLoan_Advance

                'Nilay (25-Mar-2016) -- Start
                'Dim dsLoan As DataSet = objLoan.Calculate_LoanBalanceInfo("Loan", objPeriod._End_Date.AddDays(1), IIf(mintEmployeeId > 0, mintEmployeeId.ToString, "").ToString, _
                '                                                         0, objPeriod._Start_Date, True, False, mstrSchemeListIDs, True, True, mblnFirstNamethenSurname, True, , _
                '                                                         strExtraReportFilterValue, xAdvanceJoinQry, strAllocationJoin)

                'Nilay (13-Oct-2016) -- Start
                'Enhancement : Membership wise option - Show Basic Salary on Statutory Report and ignore that option on configuration
                'Dim dsLoan As DataSet = objLoan.Calculate_LoanBalanceInfo(xDatabaseName, xUserUnkid, xYearUnkid, xCompanyUnkid, _
                '                                                          xPeriodStart, xPeriodEnd, xUserModeSetting, xOnlyApproved, "Loan", objPeriod._End_Date.AddDays(1), _
                '                                                          IIf(mintEmployeeId > 0, mintEmployeeId.ToString, "").ToString, 0, _
                '                                                          objPeriod._Start_Date, True, False, mstrSchemeListIDs, True, True, mblnFirstNamethenSurname, True, , _
                '                                                          strExtraReportFilterValue, xAdvanceJoinQry, strAllocationJoin)
                Dim dsLoan As DataSet = objLoan.Calculate_LoanBalanceInfo(xDatabaseName, xUserUnkid, xYearUnkid, xCompanyUnkid, _
                                                                          xPeriodStart, xPeriodEnd, xUserModeSetting, xOnlyApproved, "Loan", objPeriod._End_Date.AddDays(1), _
                                                                          IIf(mintEmployeeId > 0, mintEmployeeId.ToString, "").ToString, 0, _
                                                                          objPeriod._Start_Date, True, False, mstrSchemeListIDs, True, True, mblnFirstNamethenSurname, True, , _
                                                                          strExtraReportFilterValue, xAdvanceJoinQry)
                'Nilay (13-Oct-2016) -- End

                'Nilay (25-Mar-2016) -- End




                Dim dtLoan As DataTable = New DataView(dsLoan.Tables("Loan"), strFS, "", DataViewRowState.CurrentRows).ToTable

                'dtLoan.Columns.Add("MembershipNo", System.Type.GetType("System.String")).Expression = "MembershipNo"
                'dtLoan.Columns.Add("prescribed_interest_rate", System.Type.GetType("System.Decimal")).Expression = "PrescribedInterestRate"

                If objPeriod._Statusid = 1 Then
                    dtLoan.Columns.Add("amount", System.Type.GetType("System.Decimal")).Expression = "TotalMonthlyDeduction + TotPMTAmount"
                    dtLoan.Columns.Add("amountPaidCurrency", System.Type.GetType("System.Decimal")).Expression = "TotalMonthlyDeductionPaidCurrency + TotPMTAmountPaidCurrency"
                    dtLoan.Columns.Add("principal_amount", System.Type.GetType("System.Decimal")).Expression = "TotalMonthlyPrincipalAmt + TotPrincipalAmount"
                    dtLoan.Columns.Add("principal_amountPaidCurrency", System.Type.GetType("System.Decimal")).Expression = "TotalMonthlyPrincipalAmtPaidCurrency + TotPrincipalAmountPaidCurrency"
                    dtLoan.Columns.Add("interest_amount", System.Type.GetType("System.Decimal")).Expression = "TotalMonthlyInterest + TotInterestAmount"
                    dtLoan.Columns.Add("interest_amountPaidCurrency", System.Type.GetType("System.Decimal")).Expression = "TotalMonthlyInterestPaidCurrency + TotInterestAmountPaidCurrency"
                    dtLoan.Columns.Add("cf_balance", System.Type.GetType("System.Decimal")).Expression = "LastProjectedBalance"
                    dtLoan.Columns.Add("cf_balancePaidCurrency", System.Type.GetType("System.Decimal")).Expression = "LastProjectedBalancePaidCurrency"
                Else
                    dtLoan.Columns.Add("amount", System.Type.GetType("System.Decimal")).Expression = "TotalMonthlyDeduction"
                    dtLoan.Columns.Add("amountPaidCurrency", System.Type.GetType("System.Decimal")).Expression = "TotalMonthlyDeductionPaidCurrency"
                    dtLoan.Columns.Add("principal_amount", System.Type.GetType("System.Decimal")).Expression = "TotalMonthlyPrincipalAmt"
                    dtLoan.Columns.Add("principal_amountPaidCurrency", System.Type.GetType("System.Decimal")).Expression = "TotalMonthlyPrincipalAmtPaidCurrency"
                    dtLoan.Columns.Add("interest_amount", System.Type.GetType("System.Decimal")).Expression = "TotalMonthlyInterest"
                    dtLoan.Columns.Add("interest_amountPaidCurrency", System.Type.GetType("System.Decimal")).Expression = "TotalMonthlyInterestPaidCurrency"
                    dtLoan.Columns.Add("cf_balance", System.Type.GetType("System.Decimal")).Expression = "LastProjectedBalance + TotPMTAmount"
                    dtLoan.Columns.Add("cf_balancePaidCurrency", System.Type.GetType("System.Decimal")).Expression = "LastProjectedBalancePaidCurrency + TotPMTAmountPaidCurrency"
                End If

                Dim intColIndex = -1

                intColIndex += 1
                dtLoan.Columns("MembershipNo").ColumnName = dsList.Tables("DataTable").Columns("MembershipNo").ColumnName
                dtLoan.Columns("MembershipNo").SetOrdinal(intColIndex)

                intColIndex += 1
                dtLoan.Columns("prescribed_interest_rate").ColumnName = dsList.Tables("DataTable").Columns("prescribed_interest_rate").ColumnName
                dtLoan.Columns("prescribed_interest_rate").SetOrdinal(intColIndex)

                intColIndex += 1
                dtLoan.Columns("CurrMonthPeriodUnkId").ColumnName = dsList.Tables("DataTable").Columns("transaction_periodunkid").ColumnName
                dtLoan.Columns("transaction_periodunkid").SetOrdinal(intColIndex)

                intColIndex += 1
                dtLoan.Columns("CurrMonthPeriodName").ColumnName = dsList.Tables("DataTable").Columns("TranPeriod_Name").ColumnName
                dtLoan.Columns("TranPeriod_Name").SetOrdinal(intColIndex)

                intColIndex += 1
                dtLoan.Columns("CurrMonthPeriod_end_date").ColumnName = dsList.Tables("DataTable").Columns("TranPeriod_end_date").ColumnName
                dtLoan.Columns("TranPeriod_end_date").SetOrdinal(intColIndex)

                intColIndex += 1
                dtLoan.Columns("employeeunkid").ColumnName = dsList.Tables("DataTable").Columns("employeeunkid").ColumnName
                dtLoan.Columns("employeeunkid").SetOrdinal(intColIndex)

                intColIndex += 1
                dtLoan.Columns("EmpCode").ColumnName = dsList.Tables("DataTable").Columns("EmpCode").ColumnName
                dtLoan.Columns("EmpCode").SetOrdinal(intColIndex)

                intColIndex += 1
                dtLoan.Columns("EmpName").ColumnName = dsList.Tables("DataTable").Columns("EmpName").ColumnName
                dtLoan.Columns("EmpName").SetOrdinal(intColIndex)

                intColIndex += 1
                dtLoan.Columns("loanadvancetranunkid").ColumnName = dsList.Tables("DataTable").Columns("loanadvancetranunkid").ColumnName
                dtLoan.Columns("loanadvancetranunkid").SetOrdinal(intColIndex)

                intColIndex += 1
                dtLoan.Columns("loanschemeunkid").ColumnName = dsList.Tables("DataTable").Columns("loanschemeunkid").ColumnName
                dtLoan.Columns("loanschemeunkid").SetOrdinal(intColIndex)

                intColIndex += 1
                dtLoan.Columns("loanvoucher_no").ColumnName = dsList.Tables("DataTable").Columns("loanvoucher_no").ColumnName
                dtLoan.Columns("loanvoucher_no").SetOrdinal(intColIndex)

                intColIndex += 1
                dtLoan.Columns("LoanSchemeCode").ColumnName = dsList.Tables("DataTable").Columns("LoanSchemeCode").ColumnName
                dtLoan.Columns("LoanSchemeCode").SetOrdinal(intColIndex)

                intColIndex += 1
                dtLoan.Columns("LoanSchemeName").ColumnName = dsList.Tables("DataTable").Columns("LoanSchemeName").ColumnName
                dtLoan.Columns("LoanSchemeName").SetOrdinal(intColIndex)

                intColIndex += 1
                dtLoan.Columns("interest_rate").ColumnName = dtLoan.Columns("interest_rate").ColumnName
                dtLoan.Columns("interest_rate").SetOrdinal(intColIndex)

                intColIndex += 1
                dtLoan.Columns("BalanceAmount").ColumnName = dsList.Tables("DataTable").Columns("bf_balance").ColumnName
                dtLoan.Columns("bf_balance").SetOrdinal(intColIndex)

                intColIndex += 1
                dtLoan.Columns("BalanceAmountPaidCurrency").ColumnName = dsList.Tables("DataTable").Columns("bf_balancePaidCurrency").ColumnName
                dtLoan.Columns("bf_balancePaidCurrency").SetOrdinal(intColIndex)

                intColIndex += 1
                'dtLoan.Columns("TotalMonthlyDeduction").ColumnName = dsList.Tables("DataTable").Columns("amount").ColumnName
                dtLoan.Columns("amount").SetOrdinal(intColIndex)

                intColIndex += 1
                'dtLoan.Columns("TotalMonthlyDeductionPaidCurrency").ColumnName = dsList.Tables("DataTable").Columns("amountPaidCurrency").ColumnName
                dtLoan.Columns("amountPaidCurrency").SetOrdinal(intColIndex)

                intColIndex += 1
                dtLoan.Columns("TotalMonthlyRepayment").ColumnName = dsList.Tables("DataTable").Columns("repayment_amount").ColumnName
                dtLoan.Columns("repayment_amount").SetOrdinal(intColIndex)

                intColIndex += 1
                dtLoan.Columns("TotalMonthlyRepaymentPaidCurrency").ColumnName = dsList.Tables("DataTable").Columns("repayment_amountPaidCurrency").ColumnName
                dtLoan.Columns("repayment_amountPaidCurrency").SetOrdinal(intColIndex)

                intColIndex += 1
                dtLoan.Columns("TotalMonthlyTopup").ColumnName = dsList.Tables("DataTable").Columns("topup_amount").ColumnName
                dtLoan.Columns("topup_amount").SetOrdinal(intColIndex)

                intColIndex += 1
                dtLoan.Columns("TotalMonthlyTopupPaidCurrency").ColumnName = dsList.Tables("DataTable").Columns("topup_amountPaidCurrency").ColumnName
                dtLoan.Columns("topup_amountPaidCurrency").SetOrdinal(intColIndex)

                intColIndex += 1
                'dtLoan.Columns("TotPrincipalAmount").ColumnName = dsList.Tables("DataTable").Columns("principal_amount").ColumnName
                dtLoan.Columns("principal_amount").SetOrdinal(intColIndex)

                intColIndex += 1
                'dtLoan.Columns("TotPrincipalAmountPaidCurrency").ColumnName = dsList.Tables("DataTable").Columns("principal_amountPaidCurrency").ColumnName
                dtLoan.Columns("principal_amountPaidCurrency").SetOrdinal(intColIndex)

                intColIndex += 1
                'dtLoan.Columns("TotInterestAmount").ColumnName = dsList.Tables("DataTable").Columns("interest_amount").ColumnName
                dtLoan.Columns("interest_amount").SetOrdinal(intColIndex)

                intColIndex += 1
                'dtLoan.Columns("TotInterestAmountPaidCurrency").ColumnName = dsList.Tables("DataTable").Columns("interest_amountPaidCurrency").ColumnName
                dtLoan.Columns("interest_amountPaidCurrency").SetOrdinal(intColIndex)

                intColIndex += 1
                'dtLoan.Columns("LastProjectedBalance").ColumnName = dsList.Tables("DataTable").Columns("cf_balance").ColumnName
                dtLoan.Columns("cf_balance").SetOrdinal(intColIndex)

                intColIndex += 1
                'dtLoan.Columns("LastProjectedBalancePaidCurrency").ColumnName = dsList.Tables("DataTable").Columns("cf_balancePaidCurrency").ColumnName
                dtLoan.Columns("cf_balancePaidCurrency").SetOrdinal(intColIndex)

                intColIndex += 1
                dtLoan.Columns("isonhold").ColumnName = dsList.Tables("DataTable").Columns("isonhold").ColumnName
                dtLoan.Columns("isonhold").SetOrdinal(intColIndex)

                intColIndex += 1
                dtLoan.Columns("countryunkid").ColumnName = dsList.Tables("DataTable").Columns("countryunkid").ColumnName
                dtLoan.Columns("countryunkid").SetOrdinal(intColIndex)

                intColIndex += 1
                dtLoan.Columns("currency_sign").ColumnName = dsList.Tables("DataTable").Columns("currency_sign").ColumnName
                dtLoan.Columns("currency_sign").SetOrdinal(intColIndex)

                '*** Expression Columns
                intColIndex += 1
                dtLoan.Columns("TotalMonthlyDeduction").SetOrdinal(intColIndex)

                intColIndex += 1
                dtLoan.Columns("TotalMonthlyDeductionPaidCurrency").SetOrdinal(intColIndex)

                intColIndex += 1
                dtLoan.Columns("TotPMTAmount").SetOrdinal(intColIndex)

                intColIndex += 1
                dtLoan.Columns("TotPMTAmountPaidCurrency").SetOrdinal(intColIndex)

                intColIndex += 1
                dtLoan.Columns("TotPrincipalAmount").SetOrdinal(intColIndex)

                intColIndex += 1
                dtLoan.Columns("TotPrincipalAmountPaidCurrency").SetOrdinal(intColIndex)

                intColIndex += 1
                dtLoan.Columns("TotalMonthlyPrincipalAmt").SetOrdinal(intColIndex)

                intColIndex += 1
                dtLoan.Columns("TotalMonthlyPrincipalAmtPaidCurrency").SetOrdinal(intColIndex)

                intColIndex += 1
                dtLoan.Columns("TotInterestAmount").SetOrdinal(intColIndex)

                intColIndex += 1
                dtLoan.Columns("TotInterestAmountPaidCurrency").SetOrdinal(intColIndex)

                intColIndex += 1
                dtLoan.Columns("TotalMonthlyInterest").SetOrdinal(intColIndex)

                intColIndex += 1
                dtLoan.Columns("TotalMonthlyInterestPaidCurrency").SetOrdinal(intColIndex)

                intColIndex += 1
                dtLoan.Columns("LastProjectedBalance").SetOrdinal(intColIndex)

                intColIndex += 1
                dtLoan.Columns("LastProjectedBalancePaidCurrency").SetOrdinal(intColIndex)


                For i As Integer = intColIndex + 1 To dtLoan.Columns.Count - 1
                    dtLoan.Columns.RemoveAt(intColIndex + 1)
                Next

                dsList.Tables("DataTable").Merge(dtLoan)

            End If

            For Each dtRow As DataRow In dsList.Tables("DataTable").Rows
                strBuilder.Append(dtRow.Item("MembershipNo").ToString) 'Column A
                strBuilder.Append("," & dtRow.Item("EmpName").ToString & "") 'Column B
                strBuilder.Append("," & dtRow.Item("EmpCode").ToString & "") 'Column C
                strBuilder.Append("," & Format(dtRow.Item("bf_balancePaidCurrency"), GUI.fmtCurrency).Replace(",", "") & "") 'Column D
                strBuilder.Append("," & Format(dtRow.Item("cf_balancePaidCurrency"), GUI.fmtCurrency).Replace(",", "") & "") 'Column E
                strBuilder.Append("," & Format(dtRow.Item("interest_rate"), GUI.fmtCurrency).Replace(",", "") & "") 'Column F
                strBuilder.Append(",") 'Column G
                'strBuilder.Append("," & Format(dtRow.Item("prescribed_interest_rate"), GUI.fmtCurrency).Replace(",", "") & "") 'Column H
                strBuilder.Append("," & Format(dtRow.Item("prescribed_interest_rate"), "###############0.000000").Replace(",", "") & "") 'Column H

                strBuilder.Append(vbCrLf)
            Next


            If SaveTextFile(xExportReportPath, strBuilder) Then
                Return True
            Else
                Return False
            End If

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: Generate_PayrollReport; Module Name: " & mstrModuleName)
        End Try
    End Function

    'Public Function Generate_PayrollReport(ByVal mintYearunkid As Integer _
    '                                       , ByVal mstr_AccessLevelFilterString As String _
    '                                       , ByVal mdtDatabaseStartDate As Date _
    '                                       , ByVal mdtDatabaseEndDate As Date _
    '                                       , ByVal strExportPath As String) As Boolean
    '    Dim dsList As DataSet
    '    Dim exForce As Exception
    '    Dim StrQ As String = String.Empty
    '    Dim StrInnerQ As String = String.Empty
    '    Dim objActivity As New clsActivity_Master
    '    Dim strBuilder As New StringBuilder
    '    Try



    '        Dim intFirstOpenPeriodID As Integer = (New clsMasterData).getFirstPeriodID(enModuleReference.Payroll, 1, mintYearunkid)
    '        'If mstrUserAccessLevel.Trim = "" Then mstrUserAccessLevel = UserAccessLevel._AccessLevel
    '        If mstr_AccessLevelFilterString.Trim = "" Then mstr_AccessLevelFilterString = UserAccessLevel._AccessLevelFilterString

    '        objDataOperation = New clsDataOperation
    '        objDataOperation.ClearParameters()

    '        StrQ = "CREATE TABLE #tbl " & _
    '                 "( " & _
    '                   "unkid INT IDENTITY(1, 1) " & _
    '                 ", loanschemeunkid INT " & _
    '                 ", periodunkid INT " & _
    '                 ", loanadvancetranunkid INT " & _
    '                 ", employeeunkid INT " & _
    '                 ", payrollprocesstranunkid INT " & _
    '                 ", paymenttranunkid INT " & _
    '                 ", loanbf_amount DECIMAL(36, 6) " & _
    '                 ", amount DECIMAL(36, 6) " & _
    '                 ", enddate DATETIME " & _
    '                 ") " & _
    '             " " & _
    '             "INSERT  INTO #tbl " & _
    '                     "( loanschemeunkid " & _
    '                     ", periodunkid " & _
    '                     ", loanadvancetranunkid " & _
    '                     ", employeeunkid " & _
    '                     ", payrollprocesstranunkid " & _
    '                     ", paymenttranunkid " & _
    '                     ", loanbf_amount " & _
    '                     ", amount " & _
    '                     ", enddate " & _
    '                     ") " & _
    '                     "SELECT  lnloan_scheme_master.loanschemeunkid " & _
    '                           ", TableLoan.payperiodunkid " & _
    '                           ", TableLoan.loanadvancetranunkid " & _
    '                           ", TableLoan.employeeunkid " & _
    '                           ", TableLoan.payrollprocesstranunkid " & _
    '                           ", TableLoan.paymenttranunkid " & _
    '                           ", ISNULL(TableBF.cf_amount, lnloan_advance_tran.bf_amount) AS bf_amount " & _
    '                           ", TableLoan.amount " & _
    '                           ", TableLoan.enddate " & _
    '                     "FROM    ( SELECT    prtnaleave_tran.payperiodunkid " & _
    '                                       ", prpayrollprocess_tran.employeeunkid " & _
    '                                       ", prpayrollprocess_tran.loanadvancetranunkid AS loanadvancetranunkid " & _
    '                                       ", prpayrollprocess_tran.payrollprocesstranunkid " & _
    '                                       ", 0 AS paymenttranunkid " & _
    '                                       ", prpayrollprocess_tran.amount " & _
    '                                       ", cfcommon_period_tran.end_date AS enddate " & _
    '                               "FROM      prpayrollprocess_tran " & _
    '                                         "JOIN prtnaleave_tran ON prpayrollprocess_tran.tnaleavetranunkid = prtnaleave_tran.tnaleavetranunkid " & _
    '                                         "JOIN lnloan_advance_tran ON prpayrollprocess_tran.loanadvancetranunkid = lnloan_advance_tran.loanadvancetranunkid " & _
    '                                         "INNER JOIN cfcommon_period_tran ON prtnaleave_tran.payperiodunkid = cfcommon_period_tran.periodunkid " & _
    '                               "WHERE     ISNULL(prpayrollprocess_tran.isvoid, 0) = 0 " & _
    '                                         "AND ISNULL(prtnaleave_tran.isvoid, 0) = 0 " & _
    '                                         "AND ISNULL(lnloan_advance_tran.isvoid, 0) = 0 " & _
    '                                         "AND cfcommon_period_tran.isactive = 1 " & _
    '                                         "AND prpayrollprocess_tran.loanadvancetranunkid > 0 " & _
    '                                         "AND lnloan_advance_tran.isloan = 1 " & _
    '                                         "AND cfcommon_period_tran.modulerefid = " & enModuleReference.Payroll & " " & _
    '                                         "AND cfcommon_period_tran.periodunkid = @FirstOpenPeriodId " & _
    '                                         "AND lnloan_advance_tran.bf_amount IS NOT NULL "

    '        If mintSchemeId > 0 Then
    '            StrQ &= "                   AND lnloan_advance_tran.loanschemeunkid = @SchemeId "
    '        End If

    '        If mstrSchemeListIDs.Trim <> "" Then
    '            StrQ &= "                   AND lnloan_advance_tran.loanschemeunkid IN (" & mstrSchemeListIDs & ") "
    '        End If

    '        If mintEmployeeId > 0 Then
    '            StrQ &= "                   AND prpayrollprocess_tran.employeeunkid = @EmpId "
    '        End If

    '        StrQ &= "                      UNION ALL " & _
    '                              "SELECT    Payment.periodunkid " & _
    '                                      ", employeeunkid " & _
    '                                      ", loanadvancetranunkid " & _
    '                                      ", 0 AS payrollprocesstranunkid " & _
    '                                      ", Payment.paymenttranunkid " & _
    '                                      ", amount " & _
    '                                      ", cfcommon_period_tran.end_date AS enddate " & _
    '                              "FROM      ( SELECT    ( SELECT    C.periodunkid " & _
    '                                                      "FROM      cfcommon_period_tran " & _
    '                                                                "AS C " & _
    '                                                      "WHERE     CONVERT(CHAR(8), prpayment_tran.paymentdate, 112) BETWEEN C.start_date AND C.end_date " & _
    '                                                                "AND c.modulerefid = " & enModuleReference.Payroll & " " & _
    '                                                                "AND c.isactive = 1 " & _
    '                                                    ") AS periodunkid " & _
    '                                                  ", prpayment_tran.paymenttranunkid " & _
    '                                  ", prpayment_tran.employeeunkid " & _
    '                                  ", prpayment_tran.referencetranunkid AS loanadvancetranunkid " & _
    '                                                  ", prpayment_tran.amount " & _
    '                                          "FROM      prpayment_tran " & _
    '                                                    "JOIN lnloan_advance_tran ON lnloan_advance_tran.loanadvancetranunkid = prpayment_tran.referencetranunkid " & _
    '                          "WHERE     ISNULL(prpayment_tran.isvoid, 0) = 0 " & _
    '                                                    "AND prpayment_tran.referenceid = " & clsPayment_tran.enPaymentRefId.LOAN & " " & _
    '                                                    "AND prpayment_tran.paytypeid = " & clsPayment_tran.enPayTypeId.RECEIVED & " " & _
    '                                                    "AND prpayment_tran.paymentdate BETWEEN @db_start_date AND @db_end_date "

    '        If mintSchemeId > 0 Then
    '            StrQ &= "                               AND lnloan_advance_tran.loanschemeunkid = @SchemeId "
    '        End If

    '        If mstrSchemeListIDs.Trim <> "" Then
    '            StrQ &= "                               AND lnloan_advance_tran.loanschemeunkid IN (" & mstrSchemeListIDs & ") "
    '        End If

    '        If mintEmployeeId > 0 Then
    '            StrQ &= "                               AND prpayment_tran.employeeunkid = @EmpId "
    '        End If

    '        StrQ &= "                       ) AS Payment " & _
    '                                        "JOIN cfcommon_period_tran ON Payment.periodunkid = cfcommon_period_tran.periodunkid " & _
    '                                                                          "AND cfcommon_period_tran.periodunkid = @FirstOpenPeriodId " & _
    '                            ") AS TableLoan " & _
    '                        "INNER JOIN lnloan_advance_tran ON TableLoan.loanadvancetranunkid = lnloan_advance_tran.loanadvancetranunkid " & _
    '                        "INNER JOIN lnloan_scheme_master ON lnloan_advance_tran.loanschemeunkid = lnloan_scheme_master.loanschemeunkid " & _
    '                            "LEFT JOIN ( SELECT  loanadvancetranunkid " & _
    '                                              ", MIN(cf_amount) AS cf_amount " & _
    '                                        "FROM    lnloan_balance_tran " & _
    '                                        "WHERE   ISNULL(isvoid, 0) = 0  "
    '        'Sohail (26 Aug 2013) - [WHERE   ISNULL(isvoid, 0) = 0 ]

    '        If mintSchemeId > 0 Then
    '            StrQ &= "                               AND lnloan_balance_tran.loanschemeunkid = @SchemeId "
    '        End If

    '        If mstrSchemeListIDs.Trim <> "" Then
    '            StrQ &= "                               AND lnloan_balance_tran.loanschemeunkid IN (" & mstrSchemeListIDs & ") "
    '        End If

    '        If mintEmployeeId > 0 Then
    '            StrQ &= "                               AND lnloan_balance_tran.employeeunkid = @EmpId "
    '        End If

    '        StrQ &= "GROUP BY loanadvancetranunkid " & _
    '                                      ") AS TableBF ON TableBF.loanadvancetranunkid = TableLoan.loanadvancetranunkid " & _
    '                    "ORDER BY TableLoan.loanadvancetranunkid " & _
    '                           ", TableLoan.enddate " & _
    '            " " & _
    '            "SELECT  a.loanschemeunkid AS LoanSchemeId " & _
    '                  ", lnloan_scheme_master.code AS LoanCode " & _
    '                  ", lnloan_scheme_master.name AS LoanSchemeName " & _
    '                  ", a.periodunkid AS PeriodId " & _
    '                     ", cfcommon_period_tran.period_name AS PeriodName " & _
    '                  ", CONVERT(CHAR(8), a.end_date, 112) AS PeriodEndDate " & _
    '                  ", a.loanadvancetranunkid AS LoanAdvanceTranUnkId " & _
    '                  ", a.employeeunkid AS EmpId " & _
    '                       ", hremployee_master.employeecode AS EmpCode " & _
    '                   ", ISNULL(M.membershipno,'') AS MembershipNo "

    '        If mblnFirstNamethenSurname = False Then
    '            StrQ &= ", ISNULL(hremployee_master.surname, '') + ' ' + ISNULL(hremployee_master.firstname, '') + ' ' + ISNULL(hremployee_master.othername, '') AS EmpName "
    '        Else
    '            StrQ &= ", ISNULL(hremployee_master.firstname, '') + ' ' + ISNULL(hremployee_master.othername, '') + ' ' + ISNULL(hremployee_master.surname, '') AS EmpName "
    '        End If


    '        StrQ &= ", a.bf_amount AS LoanAmountBF " & _
    '                  ", a.amount AS DeductionAmount " & _
    '                  ", a.cf_amount AS LoanAmountCF " & _
    '                  ", lnloan_advance_tran.interest_rate AS InterestRate " & _
    '                  ", lnloan_advance_tran.interest_amount AS InterestAmount " & _
    '                  ", ( a.amount * lnloan_advance_tran.interest_amount ) / lnloan_advance_tran.net_amount AS InterestDeduction " & _
    '                  ", cfcommon_period_tran.prescribed_interest_rate " & _
    '                  ", CASE ISNULL(receipt.voucherno, " & _
    '                                "ISNULL(prglobalvoc_master.globalvocno, lnloan_advance_tran.loanvoucher_no)) " & _
    '                      "WHEN '' THEN prpayment_tran.voucherno ELSE ISNULL(receipt.voucherno, ISNULL(prglobalvoc_master.globalvocno, lnloan_advance_tran.loanvoucher_no)) " & _
    '                    "END AS loanvoucher_no " & _
    '                  ", CASE WHEN receipt.voucherno IS NULL THEN 0 ELSE 1 END AS IsReceipt " & _
    '                  ", a.onhold " & _
    '            "FROM    ( SELECT    lnloan_balance_tran.loanschemeunkid " & _
    '                              ", lnloan_balance_tran.periodunkid " & _
    '                              ", lnloan_balance_tran.end_date " & _
    '                              ", lnloan_balance_tran.loanadvancetranunkid " & _
    '                              ", lnloan_balance_tran.employeeunkid " & _
    '                              ", lnloan_balance_tran.payrollprocesstranunkid " & _
    '                              ", lnloan_balance_tran.paymenttranunkid " & _
    '                              ", lnloan_balance_tran.bf_amount " & _
    '                              ", lnloan_balance_tran.amount " & _
    '                              ", lnloan_balance_tran.cf_amount " & _
    '                              ", 0 AS onhold " & _
    '                      "FROM      lnloan_balance_tran " & _
    '                      "WHERE   ISNULL(lnloan_balance_tran.isvoid, 0) = 0 "

    '        'If mblnShowOnHold = True Then

    '        '    StrQ &= "UNION ALL " & _
    '        '                  "SELECT  C.loanschemeunkid  " & _
    '        '                          ", C.periodunkid " & _
    '        '                          ", C.end_date " & _
    '        '                          ", C.loanadvancetranunkid " & _
    '        '                          ", C.employeeunkid " & _
    '        '                          ", C.payrollprocesstranunkid " & _
    '        '                          ", C.paymenttranunkid " & _
    '        '                          ", C.bf_amount " & _
    '        '                          ", C.amount " & _
    '        '                          ", C.cf_amount " & _
    '        '                          ", 1 AS onhold " & _
    '        '                    "FROM    ( SELECT    lnloan_advance_tran.loanschemeunkid  " & _
    '        '                                      ", cfcommon_period_tran.periodunkid " & _
    '        '                                      ", cfcommon_period_tran.end_date " & _
    '        '                                      ", lnloan_status_tran.loanadvancetranunkid " & _
    '        '                                      ", lnloan_advance_tran.employeeunkid " & _
    '        '                                      ", 0 AS payrollprocesstranunkid " & _
    '        '                                      ", 0 AS paymenttranunkid " & _
    '        '                                      ", ISNULL(lnloan_balance_tran.bf_amount, lnloan_advance_tran.bf_amount) AS bf_amount " & _
    '        '                                      ", 0 AS amount " & _
    '        '                                      ", ISNULL(lnloan_balance_tran.cf_amount, lnloan_advance_tran.balance_amount) AS cf_amount " & _
    '        '                                      ", DENSE_RANK() OVER ( PARTITION BY lnloan_status_tran.loanadvancetranunkid ORDER BY lnloan_balance_tran.end_date DESC ) AS ROWNO " & _
    '        '                              "FROM      lnloan_status_tran " & _
    '        '                                        "LEFT JOIN lnloan_advance_tran ON lnloan_advance_tran.loanadvancetranunkid = lnloan_status_tran.loanadvancetranunkid " & _
    '        '                                        "LEFT JOIN cfcommon_period_tran ON lnloan_status_tran.status_date BETWEEN cfcommon_period_tran.start_date AND cfcommon_period_tran.end_date " & _
    '        '                                        "LEFT JOIN lnloan_balance_tran ON lnloan_balance_tran.loanadvancetranunkid = lnloan_status_tran.loanadvancetranunkid " & _
    '        '                                                                         "AND lnloan_balance_tran.end_date <= lnloan_status_tran.status_date " & _
    '        '                              "WHERE     lnloan_status_tran.isvoid = 0 " & _
    '        '                                        "AND lnloan_advance_tran.isvoid = 0 " & _
    '        '                                        "AND lnloan_balance_tran.isvoid = 0 " & _
    '        '                                        "AND cfcommon_period_tran.modulerefid = " & enModuleReference.Payroll & " " & _
    '        '                                        "AND lnloan_status_tran.statusunkid = 2 "

    '        '    If mstrSchemeListIDs.Trim <> "" Then
    '        '        StrQ &= "                   AND lnloan_advance_tran.loanschemeunkid IN (" & mstrSchemeListIDs & ") "
    '        '    End If

    '        '    If mintEmployeeId > 0 Then
    '        '        StrQ &= "                   AND lnloan_advance_tran.employeeunkid = @EmpId "
    '        '    End If

    '        '    StrQ &= "       ) AS C " & _
    '        '                   "WHERE   C.ROWNO = 1 "

    '        'End If

    '        StrQ &= "                      UNION ALL " & _
    '                      "SELECT    a.loanschemeunkid " & _
    '                              ", a.periodunkid " & _
    '                              ", a.enddate " & _
    '                              ", a.loanadvancetranunkid " & _
    '                              ", a.employeeunkid " & _
    '                              ", a.payrollprocesstranunkid " & _
    '                              ", a.paymenttranunkid " & _
    '                              ", a.loanbf_amount - SUM(b.amount) + a.amount AS BFAmount " & _
    '                              ", a.amount AS Amount " & _
    '                              ", a.loanbf_amount - SUM(b.amount) AS CFAmount " & _
    '                                  ", 0 AS onhold " & _
    '                      "FROM      #tbl a " & _
    '                                "JOIN #tbl AS b ON a.employeeunkid = b.employeeunkid " & _
    '                                                  "AND a.loanadvancetranunkid = b.loanadvancetranunkid " & _
    '                                                  "AND b.unkid <= a.unkid " & _
    '                      "GROUP BY  a.loanadvancetranunkid " & _
    '                              ", a.loanschemeunkid " & _
    '                              ", a.employeeunkid " & _
    '                              ", a.loanbf_amount " & _
    '                              ", a.enddate " & _
    '                              ", a.periodunkid " & _
    '                              ", a.payrollprocesstranunkid " & _
    '                              ", a.paymenttranunkid " & _
    '                              ", a.amount " & _
    '                    ") AS a " & _
    '                    "JOIN cfcommon_period_tran ON a.periodunkid = cfcommon_period_tran.periodunkid " & _
    '                    "JOIN hremployee_master ON hremployee_master.employeeunkid = a.employeeunkid " & _
    '                    "LEFT JOIN lnloan_scheme_master ON a.loanschemeunkid = lnloan_scheme_master.loanschemeunkid " & _
    '                    "LEFT JOIN lnloan_advance_tran ON lnloan_advance_tran.loanadvancetranunkid = a.loanadvancetranunkid " & _
    '                    "LEFT JOIN prpayrollprocess_tran ON prpayrollprocess_tran.payrollprocesstranunkid = a.payrollprocesstranunkid " & _
    '                    "LEFT JOIN prpayment_tran ON prpayment_tran.referencetranunkid = prpayrollprocess_tran.tnaleavetranunkid " & _
    '                                                    "AND a.payrollprocesstranunkid > 0 " & _
    '                                                    "AND ISNULL(prpayment_tran.isvoid, 0) = 0 " & _
    '                                                    "AND prpayment_tran.referenceid IN ( " & clsPayment_tran.enPaymentRefId.PAYSLIP & " ) " & _
    '                    "LEFT JOIN prglobalvoc_master ON prpayment_tran.globalvocno = prglobalvoc_master.globalvocunkid " & _
    '                    "LEFT JOIN prpayment_tran AS receipt ON receipt.paymenttranunkid = a.paymenttranunkid " & _
    '                                                               "AND a.paymenttranunkid > 0 " & _
    '                                                               "AND ISNULL(receipt.isvoid, 0) = 0 " & _
    '                                                               "AND receipt.referenceid IN ( " & clsPayment_tran.enPaymentRefId.LOAN & " ) " & _
    '                                                               "AND receipt.isreceipt = 1 " & _
    '                    "LEFT JOIN ( SELECT  employeeunkid " & _
    '                                          ", membershipno = ISNULL(STUFF(( SELECT    '; ' + membershipno " & _
    '                                                                          "FROM      hremployee_master AS b " & _
    '                                                                                    "LEFT JOIN hremployee_meminfo_tran ON b.employeeunkid = hremployee_meminfo_tran.employeeunkid " & _
    '                                                                          "WHERE     membershipno IS NOT NULL " & _
    '                                                                                    "AND ISNULL(hremployee_meminfo_tran.isdeleted,0) = 0 " & _
    '                                                                                    "AND b.employeeunkid = a.employeeunkid "

    '        If mintMembershipId > 0 Then
    '            StrQ &= "                                                                AND membershipunkid = @membershipunkid "
    '        End If

    '        If UserAccessLevel._AccessLevel.Length > 0 Then
    '            StrQ &= UserAccessLevel._AccessLevelFilterString.Replace("hremployee_master", "b")
    '        End If


    '        StrQ &= "                                                        FOR " & _
    '                                                                          "XML PATH('')), 1, 2, ''), '') " & _
    '                                   "FROM    hremployee_master a " & _
    '                                  "GROUP BY employeeunkid " & _
    '                                ") AS M ON M.employeeunkid = hremployee_master.employeeunkid " & _
    '                    "WHERE  cfcommon_period_tran.isactive = 1 " & _
    '                    "AND cfcommon_period_tran.modulerefid = " & enModuleReference.Payroll & " " & _
    '                    "AND lnloan_scheme_master.isactive = 1 " & _
    '                    "AND ISNULL(lnloan_advance_tran.isvoid, 0) = 0 " & _
    '                    "AND ISNULL(prpayrollprocess_tran.isvoid, 0) = 0 "

    '        'If mstrAdvance_Filter.Trim.Length > 0 Then
    '        '    StrQ &= " AND " & mstrAdvance_Filter
    '        'End If

    '        If mblnIsActive = False Then
    '            StrQ &= " AND CONVERT(CHAR(8),hremployee_master.appointeddate,112) <= @enddate " & _
    '             " AND ISNULL(CONVERT(CHAR(8),hremployee_master.termination_from_date,112),@startdate) >= @startdate " & _
    '             " AND ISNULL(CONVERT(CHAR(8),hremployee_master.termination_to_date,112),@startdate) >= @startdate" & _
    '                 " AND ISNULL(CONVERT(CHAR(8),hremployee_master.empl_enddate,112), @startdate) >= @startdate "
    '        End If

    '        'If mintBranchId > 0 Then
    '        '    StrQ &= " AND hremployee_master.stationunkid = @BranchId "
    '        'End If

    '        If mstr_AccessLevelFilterString.Length > 0 Then
    '            StrQ &= mstr_AccessLevelFilterString
    '        End If

    '        'If mintPeriodId > 0 AndAlso mblnShowInterestInformation = False Then
    '        '    StrQ &= "AND a.periodunkid = @PeriodId "
    '        'End If

    '        Call FilterTitleAndFilterQuery()

    '        StrQ &= Me._FilterQuery

    '        StrQ &= "ORDER BY a.loanadvancetranunkid " & _
    '                   ", CONVERT(CHAR(8), a.end_date, 112) " & _
    '                   ", a.BF_Amount DESC "

    '        StrQ &= "DROP TABLE #tbl "


    '        objDataOperation.AddParameter("@FirstOpenPeriodId", SqlDbType.Int, eZeeDataType.INT_SIZE, intFirstOpenPeriodID)
    '        'Sohail (10 Jun 2013) -- Start
    '        'TRA - ENHANCEMENT
    '        'objDataOperation.AddParameter("@db_start_date", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, eZeeDate.convertDate(FinancialYear._Object._Database_Start_Date))
    '        'objDataOperation.AddParameter("@db_end_date", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, eZeeDate.convertDate(FinancialYear._Object._Database_End_Date))
    '        objDataOperation.AddParameter("@db_start_date", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, eZeeDate.convertDate(IIf(mdtDatabaseStartDate <> Nothing, mdtDatabaseStartDate, FinancialYear._Object._Database_Start_Date)))
    '        objDataOperation.AddParameter("@db_end_date", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, eZeeDate.convertDate(IIf(mdtDatabaseEndDate <> Nothing, mdtDatabaseEndDate, FinancialYear._Object._Database_End_Date)))
    '        'Sohail (10 Jun 2013) -- End

    '        dsList = objDataOperation.ExecQuery(StrQ, "DataTable")

    '        If objDataOperation.ErrorMessage <> "" Then
    '            Throw New Exception(objDataOperation.ErrorNumber & ":" & objDataOperation.ErrorMessage)
    '        End If


    '        For Each dtRow As DataRow In dsList.Tables("DataTable").Rows
    '            strBuilder.Append(dtRow.Item("MembershipNo").ToString) 'Column A
    '            strBuilder.Append("," & dtRow.Item("EmpName").ToString & "") 'Column B
    '            strBuilder.Append("," & dtRow.Item("EmpCode").ToString & "") 'Column C
    '            strBuilder.Append("," & Format(dtRow.Item("LoanAmountBF"), GUI.fmtCurrency).Replace(",", "") & "") 'Column D
    '            strBuilder.Append("," & Format(dtRow.Item("LoanAmountCF"), GUI.fmtCurrency).Replace(",", "") & "") 'Column E
    '            strBuilder.Append("," & Format(dtRow.Item("InterestRate"), GUI.fmtCurrency).Replace(",", "") & "") 'Column F
    '            strBuilder.Append(",") 'Column G
    '            strBuilder.Append("," & Format(dtRow.Item("prescribed_interest_rate"), GUI.fmtCurrency).Replace(",", "") & "") 'Column H

    '            strBuilder.Append(vbCrLf)
    '        Next


    '        If SaveTextFile(strExportPath, strBuilder) Then
    '            Return True
    '        Else
    '            Return False
    '        End If


    '    Catch ex As Exception
    '        Throw New Exception(ex.Message & "; Procedure Name: Generate_PayrollReport; Module Name: " & mstrModuleName)
    '    End Try
    'End Function
    ' Sohail (30 Oct 2015) -- End



    Private Function SaveTextFile(ByVal fpath As String, ByVal sb As System.Text.StringBuilder) As Boolean
        Dim fsFile As New FileStream(fpath, FileMode.Create, FileAccess.Write)
        Dim strWriter As New StreamWriter(fsFile)
        Try

            With strWriter
                .BaseStream.Seek(0, SeekOrigin.End)
                .WriteLine(sb)
                .Close()
            End With
            Return True
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: SaveExcelfile; Module Name: " & mstrModuleName)
            Return False
        Finally
            sb = Nothing
            strWriter = Nothing
            fsFile = Nothing
        End Try
    End Function
#End Region



    '<Language> This Auto Generated Text Please Do Not Modify it.
#Region " Language & UI Settings "
    Public Shared Sub SetMessages()
        Try
            Language.setMessage(mstrModuleName, 1, "Employee Code")
            Language.setMessage(mstrModuleName, 2, "Employee Name")

        Catch Ex As Exception
            Throw New Exception(Ex.Message & "; Procedure Name: SetMessages; Module Name: " & mstrModuleName)
        End Try
    End Sub
#End Region 'Language & UI Settings
    '</Language>
End Class
