'************************************************************************************************************************************
'Class Name : clsP11Report.vb
'Purpose    :
'Date       :12/03/2014
'Written By :Sohail
'Modified   :
'************************************************************************************************************************************

#Region " Imports "

Imports eZeeCommonLib
Imports Aruti.Data

#End Region

''' <summary>
''' Purpose: Report Generation Class 
''' Developer: Sohail
''' </summary>
Public Class clsP10Report
    Inherits IReportData
    Private Shared ReadOnly mstrModuleName As String = "clsP10Report"
    Private mstrReportId As String = enArutiReport.P10_Report

    Dim objDataOperation As clsDataOperation


#Region " Constructor "

    Public Sub New(Byval intLangId As Integer,Byval intCompanyId AS Integer) 'S.SANDEEP |24-Sep-2020| -- START --END
        Me.setReportData(CInt(mstrReportId),intLangId,intCompanyId) 'S.SANDEEP |24-Sep-2020| -- START --END
        Call Create_OnDetailReport()
    End Sub

#End Region

#Region " Private Variables "

    'For Report
    Private mintEmpId As Integer = 0
    Private mstrEmpName As String = ""
    Private mstrPeriodIds As String = ""
    Private mstrPeriodNames As String = String.Empty
    Private mintPAYEHeadId As Integer = 0
    Private mstrPAYEHeadName As String = ""
    Private mintAuditTaxHeadId As Integer = 0
    Private mstrAuditTaxHeadName As String = ""
    Private mintFringeBenefitHeadId As Integer = 0
    Private mstrFringeBenefiHeadName As String = ""

    Private mblnIncludeInactiveEmp As Boolean = True

    'For Analysis By
    Private mstrAnalysis_Fields As String = ""
    Private mstrAnalysis_Join As String = ""
    Private mstrAnalysis_OrderBy As String = ""
    Private mintViewIndex As Integer = -1
    Private mstrViewByIds As String = String.Empty
    Private mstrViewByName As String = String.Empty
    Private mstrReport_GroupName As String = ""

#End Region

#Region " Properties "

    Public WriteOnly Property _EmpId() As Integer
        Set(ByVal value As Integer)
            mintEmpId = value
        End Set
    End Property

    Public WriteOnly Property _EmpName() As String
        Set(ByVal value As String)
            mstrEmpName = value
        End Set
    End Property

    Public WriteOnly Property _PAYEHeadId() As Integer
        Set(ByVal value As Integer)
            mintPAYEHeadId = value
        End Set
    End Property

    Public WriteOnly Property _PAYEHeadName() As String
        Set(ByVal value As String)
            mstrPAYEHeadName = value
        End Set
    End Property

    Public WriteOnly Property _AuditTaxHeadId() As Integer
        Set(ByVal value As Integer)
            mintAuditTaxHeadId = value
        End Set
    End Property

    Public WriteOnly Property _AuditTaxHeadName() As String
        Set(ByVal value As String)
            mstrAuditTaxHeadName = value
        End Set
    End Property

    Public WriteOnly Property _FringeBenefitHeadId() As Integer
        Set(ByVal value As Integer)
            mintFringeBenefitHeadId = value
        End Set
    End Property

    Public WriteOnly Property _FringeBenefitHeadName() As String
        Set(ByVal value As String)
            mstrFringeBenefiHeadName = value
        End Set
    End Property



    Public WriteOnly Property _PeriodIds() As String
        Set(ByVal value As String)
            mstrPeriodIds = value
        End Set
    End Property

    Public WriteOnly Property _IncludeInactiveEmp() As Boolean
        Set(ByVal value As Boolean)
            mblnIncludeInactiveEmp = value
        End Set
    End Property

    Public WriteOnly Property _PeriodNames() As String
        Set(ByVal value As String)
            mstrPeriodNames = value
        End Set
    End Property

    Public WriteOnly Property _ViewIndex() As Integer
        Set(ByVal value As Integer)
            mintViewIndex = value
        End Set
    End Property

    Public WriteOnly Property _ViewByIds() As String
        Set(ByVal value As String)
            mstrViewByIds = value
        End Set
    End Property

    Public WriteOnly Property _ViewByName() As String
        Set(ByVal value As String)
            mstrViewByName = value
        End Set
    End Property

    Public WriteOnly Property _Analysis_Fields() As String
        Set(ByVal value As String)
            mstrAnalysis_Fields = value
        End Set
    End Property

    Public WriteOnly Property _Analysis_Join() As String
        Set(ByVal value As String)
            mstrAnalysis_Join = value
        End Set
    End Property

    Public WriteOnly Property _Analysis_OrderBy() As String
        Set(ByVal value As String)
            mstrAnalysis_OrderBy = value
        End Set
    End Property

    Public WriteOnly Property _Report_GroupName() As String
        Set(ByVal value As String)
            mstrReport_GroupName = value
        End Set
    End Property

#End Region

#Region "Public Function & Procedures "

    Public Sub SetDefaultValue()
        Try

            mintEmpId = 0
            mstrEmpName = ""
            mstrPeriodIds = ""
            mintPAYEHeadId = 0
            mstrPAYEHeadName = ""
            mintAuditTaxHeadId = 0
            mstrAuditTaxHeadName = ""
            mintFringeBenefitHeadId = 0
            mstrFringeBenefiHeadName = ""


            mblnIncludeInactiveEmp = True


            mstrAnalysis_Fields = ""
            mstrAnalysis_Join = ""
            mstrAnalysis_OrderBy = ""
            mintViewIndex = -1
            mstrViewByIds = ""
            mstrReport_GroupName = ""

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: SetDefaultValue; Module Name: " & mstrModuleName)
        End Try
    End Sub

    Private Sub FilterTitleAndFilterQuery()
        Me._FilterQuery = ""
        Me._FilterTitle = ""
        Try
            objDataOperation.AddParameter("@payeheadid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintPAYEHeadId)
            objDataOperation.AddParameter("@audittaxheadid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintAuditTaxHeadId)
            objDataOperation.AddParameter("@fringebenefitheadid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintFringeBenefitHeadId)
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: FilterTitleAndFilterQuery; Module Name: " & mstrModuleName)
        End Try
    End Sub

    Public Overrides Sub generateReport(ByVal pintReportType As Integer, Optional ByVal PrintAction As Aruti.Data.enPrintAction = Aruti.Data.enPrintAction.Preview, Optional ByVal ExportAction As Aruti.Data.enExportAction = Aruti.Data.enExportAction.None)
        'Dim objRpt As CrystalDecisions.CrystalReports.Engine.ReportClass = Nothing
        'Dim strReportExportFile As String = ""

        'Try

        '    objRpt = Generate_DetailReport()

        '    If Not IsNothing(objRpt) Then
        '        Call ReportExecute(objRpt, PrintAction, ExportAction, ConfigParameter._Object._ExportReportPath, ConfigParameter._Object._OpenAfterExport)
        '    End If
        'Catch ex As Exception
        '    Throw New Exception(ex.Message & "; Procedure Name: generateReport; Module Name: " & mstrModuleName)
        'End Try
    End Sub

    'S.SANDEEP [04 JUN 2015] -- START
    'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
    Public Overrides Sub generateReportNew(ByVal xDatabaseName As String, ByVal xUserUnkid As Integer, ByVal xYearUnkid As Integer, ByVal xCompanyUnkid As Integer, ByVal xPeriodStart As Date, ByVal xPeriodEnd As Date, ByVal xUserModeSetting As String, ByVal xOnlyApproved As Boolean, ByVal xExportReportPath As String, ByVal xOpenReportAfterExport As Boolean, ByVal pintReportType As Integer, Optional ByVal PrintAction As enPrintAction = enPrintAction.Preview, Optional ByVal ExportAction As enExportAction = enExportAction.None, Optional ByVal xBaseCurrencyId As Integer = 0)
        Dim objRpt As CrystalDecisions.CrystalReports.Engine.ReportClass = Nothing
        Dim strReportExportFile As String = ""
        Try
            objRpt = Generate_DetailReport(xDatabaseName, xUserUnkid, xYearUnkid, xCompanyUnkid, xPeriodEnd, xUserModeSetting, xOnlyApproved, xBaseCurrencyId)

            If Not IsNothing(objRpt) Then
                Call ReportExecute(objRpt, PrintAction, ExportAction, xExportReportPath, xOpenReportAfterExport)
            End If
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: generateReportNew; Module Name: " & mstrModuleName)
        Finally
        End Try
    End Sub
    'S.SANDEEP [04 JUN 2015] -- END

    Public Overrides Sub setDefaultOrderBy(ByVal intReportType As Integer)
        OrderByDisplay = ""
        OrderByQuery = ""
        Try

            OrderByDisplay = iColumn_DetailReport.ColumnItem(0).DisplayName
            OrderByQuery = iColumn_DetailReport.ColumnItem(0).Name

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: setDefaultOrderBy; Module Name: " & mstrModuleName)
        End Try
    End Sub

    Public Overrides Sub setOrderBy(ByVal intReportType As Integer)
        Try
            Call OrderByExecute(iColumn_DetailReport)
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: setOrderBy; Module Name: " & mstrModuleName)
        End Try
    End Sub
#End Region

#Region " Report Generation "
    Dim iColumn_DetailReport As New IColumnCollection

    Public Property Field_OnDetailReport() As IColumnCollection
        Get
            Return iColumn_DetailReport
        End Get
        Set(ByVal value As IColumnCollection)
            iColumn_DetailReport = value
        End Set
    End Property

    Private Sub Create_OnDetailReport()
        Try
            iColumn_DetailReport.Clear()
            iColumn_DetailReport.Add(New IColumn("", ""))
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: Create_OnDetailReport; Module Name: " & mstrModuleName)
        End Try
    End Sub

    'S.SANDEEP [04 JUN 2015] -- START
    'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
    Private Function Generate_DetailReport(ByVal strDatabaseName As String, _
                                           ByVal intUserUnkid As Integer, _
                                           ByVal intYearUnkid As Integer, _
                                           ByVal intCompanyUnkid As Integer, _
                                           ByVal dtPeriodEnd As Date, _
                                           ByVal strUserModeSetting As String, _
                                           ByVal blnOnlyApproved As Boolean, _
                                           ByVal intBaseCurrencyId As Integer) As CrystalDecisions.CrystalReports.Engine.ReportClass
        'Private Function Generate_DetailReport() As CrystalDecisions.CrystalReports.Engine.ReportClass
        'S.SANDEEP [04 JUN 2015] -- END

        Dim objRpt As CrystalDecisions.CrystalReports.Engine.ReportClass = Nothing
        Dim StrQ As String = ""
        Dim dsList As New DataSet
        Dim exForce As Exception
        Dim rpt_Data As ArutiReport.Designer.dsArutiReport
        Dim strBaseCurrencySign As String
        Dim decTotalPaye As Decimal = 0

        Try
            objDataOperation = New clsDataOperation

            Dim objExchangeRate As New clsExchangeRate
            Dim decDecimalPlaces As Decimal = 0

            'S.SANDEEP [04 JUN 2015] -- START
            'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
            'objExchangeRate._ExchangeRateunkid = ConfigParameter._Object._Base_CurrencyId
            objExchangeRate._ExchangeRateunkid = intBaseCurrencyId
            'S.SANDEEP [04 JUN 2015] -- END

            'Sohail (29 Mar 2017) -- Start
            'Issue - 65.2 - Amount not matching with Payroll Report.
            'decDecimalPlaces = objExchangeRate._Digits_After_Decimal
            'Sohail (29 Nov 2017) -- Start
            'SUMATRA – issue # 0001666: Discrepancy in various reports in 70.1.
            'decDecimalPlaces = 6
            decDecimalPlaces = objExchangeRate._Digits_After_Decimal
            'Sohail (29 Nov 2017) -- End
            'Sohail (29 Mar 2017) -- End
            strBaseCurrencySign = objExchangeRate._Currency_Sign

            'S.SANDEEP [04 JUN 2015] -- START
            'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
            Dim xUACQry, xUACFiltrQry As String : xUACQry = "" : xUACFiltrQry = ""
            Call NewAccessLevelFilterString(xUACQry, xUACFiltrQry, dtPeriodEnd, blnOnlyApproved, strDatabaseName, intUserUnkid, intCompanyUnkid, intYearUnkid, strUserModeSetting)
            'S.SANDEEP [04 JUN 2015] -- END

            StrQ = "SELECT  A.periodunkid " & _
                          ", cfcommon_period_tran.period_name " & _
                          ", PAYE " & _
                          ", AuditTax " & _
                          ", FringeBenefit " & _
                    "FROM    ( SELECT    ISNULL(Period.payperiodunkid, 0) AS periodunkid " & _
                                      ", SUM(ISNULL(Period.PAYE, 0)) AS PAYE " & _
                                      ", SUM(ISNULL(Period.AuditTax, 0)) AS AuditTax " & _
                                      ", SUM(ISNULL(Period.FringeBenefit, 0)) AS FringeBenefit " & _
                              "FROM      ( SELECT    payperiodunkid " & _
                                                  ", CAST(amount AS DECIMAL(36, " & decDecimalPlaces & ")) AS PAYE " & _
                                                  ", 0 AS AuditTax " & _
                                                  ", 0 AS FringeBenefit " & _
                                          "FROM      prpayrollprocess_tran " & _
                                                    "LEFT JOIN prtnaleave_tran ON prpayrollprocess_tran.tnaleavetranunkid = prtnaleave_tran.tnaleavetranunkid " & _
                                                    "LEFT JOIN hremployee_master ON prtnaleave_tran.employeeunkid = hremployee_master.employeeunkid " & _
                                                    "LEFT JOIN prtranhead_master ON prtranhead_master.tranheadunkid = prpayrollprocess_tran.tranheadunkid " & _
                                                                                  "AND prpayrollprocess_tran.tranheadunkid > 0 "

            StrQ &= mstrAnalysis_Join

            'S.SANDEEP [04 JUN 2015] -- START
            'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
            If xUACQry.Trim.Length > 0 Then
                StrQ &= xUACQry
            End If
            'S.SANDEEP [04 JUN 2015] -- END

            StrQ &= "                      WHERE     prpayrollprocess_tran.isvoid = 0 " & _
                                                    "AND prtnaleave_tran.isvoid = 0 " & _
                                                    "AND prtnaleave_tran.payperiodunkid IN (" & mstrPeriodIds & ") " & _
                                                    "AND prtranhead_master.tranheadunkid = @payeheadid "

            If mintEmpId > 0 Then
                StrQ &= "                  AND prpayrollprocess_tran.employeeunkid = @employeeunkid "
            End If

            'S.SANDEEP [04 JUN 2015] -- START
            'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS

            If xUACFiltrQry.Trim.Length > 0 Then
                StrQ &= " AND " & xUACFiltrQry
            End If

            'S.SANDEEP [04 JUN 2015] -- END

            StrQ &= "                      UNION ALL " & _
                                          "SELECT    payperiodunkid " & _
                                                  ", 0 AS PAYE " & _
                                                  ", CAST(amount AS DECIMAL(36, " & decDecimalPlaces & ")) AS AuditTax " & _
                                                  ", 0 AS FringeBenefit " & _
                                          "FROM      prpayrollprocess_tran " & _
                                                    "LEFT JOIN prtnaleave_tran ON prpayrollprocess_tran.tnaleavetranunkid = prtnaleave_tran.tnaleavetranunkid " & _
                                                    "LEFT JOIN hremployee_master ON prtnaleave_tran.employeeunkid = hremployee_master.employeeunkid " & _
                                                    "LEFT JOIN prtranhead_master ON prtranhead_master.tranheadunkid = prpayrollprocess_tran.tranheadunkid " & _
                                                                                  "AND prpayrollprocess_tran.tranheadunkid > 0 "

            StrQ &= mstrAnalysis_Join

            'S.SANDEEP [04 JUN 2015] -- START
            'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
            If xUACQry.Trim.Length > 0 Then
                StrQ &= xUACQry
            End If
            'S.SANDEEP [04 JUN 2015] -- END

            StrQ &= "                      WHERE     prpayrollprocess_tran.isvoid = 0 " & _
                                                    "AND prtnaleave_tran.isvoid = 0 " & _
                                                    "AND prtnaleave_tran.payperiodunkid IN (" & mstrPeriodIds & ") " & _
                                                    "AND prtranhead_master.tranheadunkid = @audittaxheadid "
            If mintEmpId > 0 Then
                StrQ &= "                  AND prpayrollprocess_tran.employeeunkid = @employeeunkid "
            End If

            'S.SANDEEP [04 JUN 2015] -- START
            'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS

            If xUACFiltrQry.Trim.Length > 0 Then
                StrQ &= " AND " & xUACFiltrQry
            End If

            'S.SANDEEP [04 JUN 2015] -- END

            StrQ &= "                      UNION ALL " & _
                                          "SELECT    payperiodunkid " & _
                                                  ", 0 AS PAYE " & _
                                                  ", 0 AS AuditTax " & _
                                                  ", CAST(amount AS DECIMAL(36, " & decDecimalPlaces & ")) AS FringeBenefit " & _
                                          "FROM      prpayrollprocess_tran " & _
                                                    "LEFT JOIN prtnaleave_tran ON prpayrollprocess_tran.tnaleavetranunkid = prtnaleave_tran.tnaleavetranunkid " & _
                                                    "LEFT JOIN hremployee_master ON prtnaleave_tran.employeeunkid = hremployee_master.employeeunkid " & _
                                                    "LEFT JOIN prtranhead_master ON prtranhead_master.tranheadunkid = prpayrollprocess_tran.tranheadunkid " & _
                                                                                  "AND prpayrollprocess_tran.tranheadunkid > 0 "

            StrQ &= mstrAnalysis_Join

            'S.SANDEEP [04 JUN 2015] -- START
            'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
            If xUACQry.Trim.Length > 0 Then
                StrQ &= xUACQry
            End If
            'S.SANDEEP [04 JUN 2015] -- END

            StrQ &= "                      WHERE     prpayrollprocess_tran.isvoid = 0 " & _
                                                    "AND prtnaleave_tran.isvoid = 0 " & _
                                                    "AND prtnaleave_tran.payperiodunkid IN (" & mstrPeriodIds & ") " & _
                                                    "AND prtranhead_master.tranheadunkid = @fringebenefitheadid "

            If mintEmpId > 0 Then
                StrQ &= "                  AND prpayrollprocess_tran.employeeunkid = @employeeunkid "
            End If

            'S.SANDEEP [04 JUN 2015] -- START
            'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS

            If xUACFiltrQry.Trim.Length > 0 Then
                StrQ &= " AND " & xUACFiltrQry
            End If

            'S.SANDEEP [04 JUN 2015] -- END

            StrQ &= "                    ) AS Period " & _
                              "GROUP BY  Period.payperiodunkid " & _
                            ") AS A " & _
                            "LEFT JOIN cfcommon_period_tran ON A.periodunkid = cfcommon_period_tran.periodunkid "


            Call FilterTitleAndFilterQuery()
            StrQ &= Me._FilterQuery


            If mintEmpId > 0 Then
                objDataOperation.AddParameter("@employeeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintEmpId)
            End If

            dsList = objDataOperation.ExecQuery(StrQ, "DataTable")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & " : " & objDataOperation.ErrorMessage)
                Throw exForce
            End If


            rpt_Data = New ArutiReport.Designer.dsArutiReport

            Dim rpt_Row As DataRow = Nothing
            Dim decTotPAYE As Decimal = 0
            Dim decTotAuditTax As Decimal = 0
            Dim decTotFringeBenefit As Decimal = 0

            For Each dtRow As DataRow In dsList.Tables("DataTable").Rows

                rpt_Row = rpt_Data.Tables("ArutiTable").NewRow

                rpt_Row.Item("Column1") = dtRow.Item("period_name").ToString
                rpt_Row.Item("Column2") = Format(CDec(dtRow.Item("PAYE")), GUI.fmtCurrency)
                rpt_Row.Item("Column3") = Format(CDec(dtRow.Item("AuditTax")), GUI.fmtCurrency)
                rpt_Row.Item("Column4") = Format(CDec(dtRow.Item("FringeBenefit")), GUI.fmtCurrency)
                rpt_Row.Item("Column5") = ""


                decTotPAYE += CDec(rpt_Row.Item("Column2"))
                decTotAuditTax += CDec(rpt_Row.Item("Column3"))
                decTotFringeBenefit += CDec(rpt_Row.Item("Column4"))

                rpt_Row.Item("Column21") = Format(decTotPAYE, GUI.fmtCurrency)
                rpt_Row.Item("Column22") = Format(decTotAuditTax, GUI.fmtCurrency)
                rpt_Row.Item("Column23") = Format(decTotFringeBenefit, GUI.fmtCurrency)

                rpt_Data.Tables("ArutiTable").Rows.Add(rpt_Row)
            Next

            objRpt = New ArutiReport.Designer.rptKY_P10

            objRpt.SetDataSource(rpt_Data)

            ReportFunction.TextChange(objRpt, "lblCaption1", Language.getMessage(mstrModuleName, 1, "ISO 9001:2008 CERTIFIED"))
            ReportFunction.TextChange(objRpt, "lblAppendix", Language.getMessage(mstrModuleName, 2, "APPENDIX 5"))
            ReportFunction.TextChange(objRpt, "lblCaption2", Language.getMessage(mstrModuleName, 3, "P.10"))
            ReportFunction.TextChange(objRpt, "lblCaption3", Language.getMessage(mstrModuleName, 4, "P.A.Y.E - EMPLOYER'S CERTIFICATE"))
            ReportFunction.TextChange(objRpt, "lblCaption4", Language.getMessage(mstrModuleName, 5, "YEAR"))
            ReportFunction.TextChange(objRpt, "txtYear", FinancialYear._Object._FinancialYear_Name)
            ReportFunction.TextChange(objRpt, "lblEmployerName", Language.getMessage(mstrModuleName, 6, "NAME OF EMPLOYER "))
            ReportFunction.TextChange(objRpt, "txtEmployerName", Company._Object._Name)
            ReportFunction.TextChange(objRpt, "lblEmployerPin", Language.getMessage(mstrModuleName, 7, "EMPLOYER'S PIN"))
            ReportFunction.TextChange(objRpt, "txtEmplrPIN", Company._Object._Tinno)
            ReportFunction.TextChange(objRpt, "lblSAC", Language.getMessage(mstrModuleName, 8, "To Senior Assistant Commissioner"))
            ReportFunction.TextChange(objRpt, "txtSAC", Language.getMessage(mstrModuleName, 9, ""))
            ReportFunction.TextChange(objRpt, "lblText2", Language.getMessage(mstrModuleName, 10, "We/I forward herewith "))
            ReportFunction.TextChange(objRpt, "txtForward", Company._Object._Name)
            ReportFunction.TextChange(objRpt, "lblText3", Language.getMessage(mstrModuleName, 11, "Tax Deduction Cards (P9A/P9B) showing the total tax deducted"))
            ReportFunction.TextChange(objRpt, "lblText4", Language.getMessage(mstrModuleName, 12, "(as listed on P.10A) amounting to") & " " & strBaseCurrencySign)
            ReportFunction.TextChange(objRpt, "txtAmount", Format(decTotPAYE, GUI.fmtCurrency))
            ReportFunction.TextChange(objRpt, "lblText5", Language.getMessage(mstrModuleName, 13, "This total tax has been paid as follows:-"))

            ReportFunction.TextChange(objRpt, "lblMonth", Language.getMessage(mstrModuleName, 14, "MONTH"))
            ReportFunction.TextChange(objRpt, "lblPTax", Language.getMessage(mstrModuleName, 15, "PAYE TAX"))
            ReportFunction.TextChange(objRpt, "lblAuditTax", Language.getMessage(mstrModuleName, 16, "AUDIT TAX, INTEREST/PENALTY"))
            ReportFunction.TextChange(objRpt, "lblBenefit", Language.getMessage(mstrModuleName, 17, "FRINGE BENEFIT TAX"))
            ReportFunction.TextChange(objRpt, "lblDatePaid", Language.getMessage(mstrModuleName, 18, "DATE PAID (PER RECEIVING BAN STAMP)"))
            ReportFunction.TextChange(objRpt, "lblTotalTax", Language.getMessage(mstrModuleName, 19, "TOTAL TAX") & "  " & strBaseCurrencySign)
            ReportFunction.TextChange(objRpt, "lblNote", Language.getMessage(mstrModuleName, 20, "NOTE:-"))
            ReportFunction.TextChange(objRpt, "lblPoint1", Language.getMessage(mstrModuleName, 21, "(1) Attach Photostat copies of "))
            ReportFunction.TextChange(objRpt, "lblPoint1_1", Language.getMessage(mstrModuleName, 22, "All the Pay-In Credit Slips (P11s)"))
            ReportFunction.TextChange(objRpt, "lblPoint1_2", Language.getMessage(mstrModuleName, 23, "for the year."))
            ReportFunction.TextChange(objRpt, "lblPoint2", Language.getMessage(mstrModuleName, 24, "(2) Complete this certificate in triplicate and send the top two copies with the enclosures to your Income Tax Office"))
            ReportFunction.TextChange(objRpt, "lblPoint2_1", Language.getMessage(mstrModuleName, 25, "not later than 28 FEBRUARY."))
            ReportFunction.TextChange(objRpt, "lblPoint3", Language.getMessage(mstrModuleName, 26, "(3) Provide Statistical information required by Central Bureau of Statistics.(See overleaf)."))
            ReportFunction.TextChange(objRpt, "lblPoint4", Language.getMessage(mstrModuleName, 27, "We/I certifiy that the particulars entered above are correct."))
            ReportFunction.TextChange(objRpt, "lblPostalAddress", Language.getMessage(mstrModuleName, 28, "ADDRESS"))
            ReportFunction.TextChange(objRpt, "txtAddress", Company._Object._Address1 & " " & Company._Object._Address2 & " " & Company._Object._City_Name & " " & Company._Object._State_Name & " " & Company._Object._Country_Name & " " & Company._Object._Post_Code_No)
            ReportFunction.TextChange(objRpt, "lblSignature", Language.getMessage(mstrModuleName, 29, "SIGNATURE"))
            ReportFunction.TextChange(objRpt, "txtSignature", "")
            ReportFunction.TextChange(objRpt, "lblDate", Language.getMessage(mstrModuleName, 30, "DATE"))
            ReportFunction.TextChange(objRpt, "txtDate", "")

            Return objRpt

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: Generate_DetailReport; Module Name: " & mstrModuleName)
            Return Nothing
        End Try
    End Function
#End Region

    '<Language> This Auto Generated Text Please Do Not Modify it.
#Region " Language & UI Settings "
    Public Shared Sub SetMessages()
        Try
            Language.setMessage(mstrModuleName, 1, "ISO 9001:2008 CERTIFIED")
            Language.setMessage(mstrModuleName, 2, "APPENDIX 5")
            Language.setMessage(mstrModuleName, 3, "P.10")
            Language.setMessage(mstrModuleName, 4, "P.A.Y.E - EMPLOYER'S CERTIFICATE")
            Language.setMessage(mstrModuleName, 5, "YEAR")
            Language.setMessage(mstrModuleName, 6, "NAME OF EMPLOYER")
            Language.setMessage(mstrModuleName, 7, "EMPLOYER'S PIN")
            Language.setMessage(mstrModuleName, 8, "To Senior Assistant Commissioner")
            Language.setMessage(mstrModuleName, 9, "")
            Language.setMessage(mstrModuleName, 10, "We/I forward herewith")
            Language.setMessage(mstrModuleName, 11, "Tax Deduction Cards (P9A/P9B) showing the total tax deducted")
            Language.setMessage(mstrModuleName, 12, "(as listed on P.10A) amounting to")
            Language.setMessage(mstrModuleName, 13, "This total tax has been paid as follows:-")
            Language.setMessage(mstrModuleName, 14, "MONTH")
            Language.setMessage(mstrModuleName, 15, "PAYE TAX")
            Language.setMessage(mstrModuleName, 16, "AUDIT TAX, INTEREST/PENALTY")
            Language.setMessage(mstrModuleName, 17, "FRINGE BENEFIT TAX")
            Language.setMessage(mstrModuleName, 18, "DATE PAID (PER RECEIVING BAN STAMP)")
            Language.setMessage(mstrModuleName, 19, "TOTAL TAX")
            Language.setMessage(mstrModuleName, 20, "NOTE:-")
            Language.setMessage(mstrModuleName, 21, "(1) Attach Photostat copies of")
            Language.setMessage(mstrModuleName, 22, "All the Pay-In Credit Slips (P11s)")
            Language.setMessage(mstrModuleName, 23, "for the year.")
            Language.setMessage(mstrModuleName, 24, "(2) Complete this certificate in triplicate and send the top two copies with the enclosures to your Income Tax Office")
            Language.setMessage(mstrModuleName, 25, "not later than 28 FEBRUARY.")
            Language.setMessage(mstrModuleName, 26, "(3) Provide Statistical information required by Central Bureau of Statistics.(See overleaf).")
            Language.setMessage(mstrModuleName, 27, "We/I certifiy that the particulars entered above are correct.")
            Language.setMessage(mstrModuleName, 28, "ADDRESS")
            Language.setMessage(mstrModuleName, 29, "SIGNATURE")
            Language.setMessage(mstrModuleName, 30, "DATE")

        Catch Ex As Exception
            Throw New Exception(Ex.Message & "; Procedure Name: SetMessages; Module Name: " & mstrModuleName)
        End Try
    End Sub
#End Region 'Language & UI Settings
    '</Language>

End Class
