﻿'************************************************************************************************************************************
'Class Name : clsNITAReport.vb
'Purpose    :
'Date       :29/11/2021
'Written By : Sohail.
'Modified   :
'************************************************************************************************************************************

#Region " Imports "

Imports eZeeCommonLib
Imports Aruti.Data
Imports ExcelWriter
Imports System.Text
Imports System.IO

#End Region

''' <summary>
''' Purpose: Report Generation Class 
''' Developer: Sohail.
''' </summary>
Public Class clsNITAReport
    Inherits IReportData
    Private Shared ReadOnly mstrModuleName As String = "clsNITAReport"
    Private mstrReportId As String = enArutiReport.NITA_Report
    Dim objDataOperation As clsDataOperation

#Region " Constructor "

    Public Sub New(ByVal intLangId As Integer, ByVal intCompanyId As Integer)
        Me.setReportData(CInt(mstrReportId), intLangId, intCompanyId)
        Call Create_OnDetailReport()
    End Sub

#End Region

#Region " Private variables "

    Private mintReportId As Integer = 0
    Private mstrReportTypeName As String = ""
    Private mintPeriodId As Integer = -1
    Private mstrPeriodName As String = String.Empty
    Private mintMembershipId As Integer = 1
    Private mstrMembershipName As String = String.Empty
    Private mintNonPayrollMembershipId As Integer = -1
    Private mstrNonPayrollMembershipName As String = String.Empty
    Private mintNITADeductionHeadId As Integer = 1
    Private mstrNITADeductionHeadName As String = String.Empty

    Private mblnFirstNamethenSurname As Boolean = ConfigParameter._Object._FirstNamethenSurname
    Private mstrAnalysis_Fields As String = ""
    Private mstrAnalysis_Join As String = ""
    Private mstrAnalysis_OrderBy As String = ""
    Private mintViewIndex As Integer = -1
    Private mstrViewByIds As String = String.Empty
    Private mstrViewByName As String = String.Empty
    Private mstrReport_GroupName As String = ""
    Private mintReportTypeId As Integer = 0
#End Region

#Region " Properties "

    Public WriteOnly Property _ReportId() As Integer
        Set(ByVal value As Integer)
            mintReportId = value
        End Set
    End Property

    Public WriteOnly Property _ReportTypeName() As String
        Set(ByVal value As String)
            mstrReportTypeName = value
        End Set
    End Property

    Public WriteOnly Property _PeriodId() As Integer
        Set(ByVal value As Integer)
            mintPeriodId = value
        End Set
    End Property

    Public WriteOnly Property _PeriodName() As String
        Set(ByVal value As String)
            mstrPeriodName = value
        End Set
    End Property

    Public WriteOnly Property _MembershipId() As Integer
        Set(ByVal value As Integer)
            mintMembershipId = value
        End Set
    End Property

    Public WriteOnly Property _MembershipName() As String
        Set(ByVal value As String)
            mstrMembershipName = value
        End Set
    End Property

    Public WriteOnly Property _NonPayrollMembershipId() As Integer
        Set(ByVal value As Integer)
            mintNonPayrollMembershipId = value
        End Set
    End Property

    Public WriteOnly Property _NonPayrollMembershipName() As String
        Set(ByVal value As String)
            mstrNonPayrollMembershipName = value
        End Set
    End Property

    Public WriteOnly Property _NITADeductionHeadId() As Integer
        Set(ByVal value As Integer)
            mintNITADeductionHeadId = value
        End Set
    End Property

    Public WriteOnly Property _NITADeductionHeadName() As String
        Set(ByVal value As String)
            mstrNITADeductionHeadName = value
        End Set
    End Property


    Public WriteOnly Property _FirstNamethenSurname() As Boolean
        Set(ByVal value As Boolean)
            mblnFirstNamethenSurname = value
        End Set
    End Property

    Public WriteOnly Property _Analysis_Fields() As String
        Set(ByVal value As String)
            mstrAnalysis_Fields = value
        End Set
    End Property

    Public WriteOnly Property _Analysis_Join() As String
        Set(ByVal value As String)
            mstrAnalysis_Join = value
        End Set
    End Property

    Public WriteOnly Property _Analysis_OrderBy() As String
        Set(ByVal value As String)
            mstrAnalysis_OrderBy = value
        End Set
    End Property

    Public WriteOnly Property _ViewIndex() As Integer
        Set(ByVal value As Integer)
            mintViewIndex = value
        End Set
    End Property

    Public WriteOnly Property _ViewByIds() As String
        Set(ByVal value As String)
            mstrViewByIds = value
        End Set
    End Property

    Public WriteOnly Property _ViewByName() As String
        Set(ByVal value As String)
            mstrViewByName = value
        End Set
    End Property

    Public WriteOnly Property _Report_GroupName() As String
        Set(ByVal value As String)
            mstrReport_GroupName = value
        End Set
    End Property

    Public WriteOnly Property _ReportTypeId() As Integer
        Set(ByVal value As Integer)
            mintReportTypeId = value
        End Set
    End Property

#End Region

#Region "Public Function & Procedures "

    Public Sub SetDefaultValue()
        Try
            mintReportId = -1
            mstrReportTypeName = ""
            mintPeriodId = 0
            mstrPeriodName = ""
            mintMembershipId = 0
            mstrMembershipName = ""
            mintNonPayrollMembershipId = 0
            mstrNonPayrollMembershipName = ""
            mintNITADeductionHeadId = 0
            mstrNITADeductionHeadName = ""
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: SetDefaultValue; Module Name: " & mstrModuleName)
        End Try
    End Sub

    Private Sub FilterTitleAndFilterQuery()
        Me._FilterQuery = ""
        Me._FilterTitle = ""
        Try

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: FilterTitleAndFilterQuery; Module Name: " & mstrModuleName)
        End Try
    End Sub

    Public Overloads Overrides Sub generateReport(ByVal pintReportType As Integer, Optional ByVal PrintAction As Aruti.Data.enPrintAction = Aruti.Data.enPrintAction.Preview, Optional ByVal ExportAction As Aruti.Data.enExportAction = Aruti.Data.enExportAction.None)

    End Sub

    Public Overrides Sub generateReportNew(ByVal xDatabaseName As String, ByVal xUserUnkid As Integer, ByVal xYearUnkid As Integer, ByVal xCompanyUnkid As Integer, ByVal xPeriodStart As Date, ByVal xPeriodEnd As Date, ByVal xUserModeSetting As String, ByVal xOnlyApproved As Boolean, ByVal xExportReportPath As String, ByVal xOpenReportAfterExport As Boolean, ByVal pintReportType As Integer, Optional ByVal PrintAction As enPrintAction = enPrintAction.Preview, Optional ByVal ExportAction As enExportAction = enExportAction.None, Optional ByVal xBaseCurrencyId As Integer = 0)
        Try

        Catch ex As Exception

        End Try
    End Sub

    Public Overrides Sub setDefaultOrderBy(ByVal intReportType As Integer)
        OrderByDisplay = ""
        OrderByQuery = ""
        Try

            OrderByDisplay = iColumn_DetailReport.ColumnItem(0).DisplayName
            OrderByQuery = iColumn_DetailReport.ColumnItem(0).Name

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: setDefaultOrderBy; Module Name: " & mstrModuleName)
        End Try
    End Sub

    Public Overrides Sub setOrderBy(ByVal intReportType As Integer)
        Try

            Call OrderByExecute(iColumn_DetailReport)
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: setOrderBy; Module Name: " & mstrModuleName)
        End Try
    End Sub

#End Region

#Region " Report Generation "

    Dim iColumn_DetailReport As New IColumnCollection

    Public Property Field_OnDetailReport() As IColumnCollection
        Get
            Return iColumn_DetailReport
        End Get
        Set(ByVal value As IColumnCollection)
            iColumn_DetailReport = value
        End Set
    End Property

    Private Sub Create_OnDetailReport()
        Try
            iColumn_DetailReport.Clear()
            iColumn_DetailReport.Add(New IColumn("", ""))
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: Create_OnDetailReport; Module Name: " & mstrModuleName)
        End Try
    End Sub

    Public Function Generate_DetailReport(ByVal strDatabaseName As String, _
                                           ByVal intUserUnkid As Integer, _
                                           ByVal intYearUnkid As Integer, _
                                           ByVal intCompanyUnkid As Integer, _
                                           ByVal dtPeriodStart As Date, _
                                           ByVal dtPeriodEnd As Date, _
                                           ByVal strUserModeSetting As String, _
                                           ByVal blnOnlyApproved As Boolean, _
                                           ByVal intBaseCurrencyId As Integer, _
                                           ByVal strFmtCurrency As String, _
                                           ByVal strExportPath As String _
                                           ) As Boolean

        Dim StrQ As String = ""
        Dim dsList As New DataSet
        Dim exForce As Exception
        Dim strBuilder As New StringBuilder

        Try
            objDataOperation = New clsDataOperation

            Dim objExchangeRate As New clsExchangeRate
            Dim decDecimalPlaces As Decimal = 0
            objExchangeRate._ExchangeRateunkid = intBaseCurrencyId
            decDecimalPlaces = objExchangeRate._Digits_After_Decimal

            Dim xUACQry, xUACFiltrQry As String
            xUACQry = "" : xUACFiltrQry = ""
            Call NewAccessLevelFilterString(xUACQry, xUACFiltrQry, dtPeriodEnd, blnOnlyApproved, strDatabaseName, intUserUnkid, intCompanyUnkid, intYearUnkid, strUserModeSetting)


            StrQ = "SELECT hremployee_master.employeeunkid " & _
                        ", CONVERT(CHAR(8), hremployee_master.appointeddate, 112) AS appointeddate " & _
                        ", hremployee_master.employeecode "

            If mblnFirstNamethenSurname = False Then
                StrQ &= ", ISNULL(hremployee_master.surname,'') + ' ' + ISNULL(hremployee_master.firstname,'') + ' ' + ISNULL(hremployee_master.othername,'') AS employeename "
            Else
                StrQ &= ", ISNULL(hremployee_master.firstname,'') + ' ' + ISNULL(hremployee_master.othername,'') + ' ' + ISNULL(hremployee_master.surname,'') AS employeename "
            End If

            StrQ &= "INTO #TableEmp " & _
                    "FROM hremployee_master "

            StrQ &= mstrAnalysis_Join

            If xUACQry.Trim.Length > 0 Then
                StrQ &= xUACQry
            End If

            StrQ &= " WHERE 1 = 1 "

            If xUACFiltrQry.Trim.Length > 0 Then
                StrQ &= " AND " & xUACFiltrQry
            End If

            StrQ &= ";SELECT A.employeeunkid " & _
                        ", #TableEmp.employeecode " & _
                        ", #TableEmp.employeename " & _
                        ", MAX(A.MembershipNo) AS MembershipNo " & _
                        ", MAX(A.OtherMembershipNo) AS OtherMembershipNo " & _
                        ", SUM(A.amount) AS amount "

            StrQ &= "FROM " & _
                   "( " & _
                       "SELECT #TableEmp.employeeunkid " & _
                            ", ISNULL(hremployee_meminfo_tran.membershipno, '') AS MembershipNo " & _
                            ", '' AS OtherMembershipNo " & _
                            ", 0 AS amount "

            StrQ &= "FROM #TableEmp " & _
                            "LEFT JOIN hremployee_meminfo_tran " & _
                                "ON hremployee_meminfo_tran.employeeunkid = #TableEmp.employeeunkid " & _
                                   "AND ISNULL(hremployee_meminfo_tran.isdeleted, 0) = 0 " & _
                            "LEFT JOIN hrmembership_master " & _
                                "ON hremployee_meminfo_tran.membershipunkid = hrmembership_master.membershipunkid " & _
                        "WHERE hremployee_meminfo_tran.membershipunkid = @MemId "


            StrQ &= "UNION ALL " & _
                           "SELECT #TableEmp.employeeunkid " & _
                                 ", '' AS MembershipNo " & _
                                 ", ISNULL(hremployee_meminfo_tran.membershipno, '') AS MeOtherMembershipNomshipNo " & _
                                 ", 0 AS amount "

            StrQ &= "FROM #TableEmp " & _
                               "LEFT JOIN hremployee_meminfo_tran " & _
                                   "ON hremployee_meminfo_tran.employeeunkid = #TableEmp.employeeunkid " & _
                                      "AND ISNULL(hremployee_meminfo_tran.isdeleted, 0) = 0 " & _
                               "LEFT JOIN hrmembership_master " & _
                                   "ON hremployee_meminfo_tran.membershipunkid = hrmembership_master.membershipunkid " & _
                            "WHERE hremployee_meminfo_tran.membershipunkid = @OtherMemId "


            StrQ &= "UNION ALL " & _
                        "SELECT prpayrollprocess_tran.employeeunkid " & _
                             ", '' AS MembershipNo " & _
                             ", '' AS OtherMembershipNo " & _
                             ", SUM(ISNULL(CAST(prpayrollprocess_tran.amount AS DECIMAL(36, " & decDecimalPlaces & ")), 0)) AS amount "

            StrQ &= "FROM prpayrollprocess_tran " & _
                            "JOIN #TableEmp " & _
                                "ON prpayrollprocess_tran.employeeunkid = #TableEmp.employeeunkid " & _
                            "JOIN prtnaleave_tran " & _
                                "ON prpayrollprocess_tran.tnaleavetranunkid = prtnaleave_tran.tnaleavetranunkid " & _
                            "JOIN prtranhead_master " & _
                                "ON prpayrollprocess_tran.tranheadunkid = prtranhead_master.tranheadunkid " & _
                        "WHERE prpayrollprocess_tran.isvoid = 0 " & _
                              "AND prtnaleave_tran.isvoid = 0 " & _
                              "AND prtranhead_master.isvoid = 0 " & _
                              "AND prtnaleave_tran.payperiodunkid = @PeriodId " & _
                              "AND prtranhead_master.tranheadunkid =  @tranheadunkid " & _
                              "AND prpayrollprocess_tran.amount <> 0 "


            StrQ &= "GROUP BY prpayrollprocess_tran.employeeunkid " & _
                               ", prtnaleave_tran.payperiodunkid "

            StrQ &= ") AS A " & _
                  "JOIN #TableEmp " & _
                              "ON A.employeeunkid = #TableEmp.employeeunkid " & _
                  "GROUP BY A.employeeunkid " & _
                          ", #TableEmp.employeecode " & _
                          ", #TableEmp.employeename "

            StrQ &= " DROP TABLE #TableEmp "


            objDataOperation.AddParameter("@MemId", SqlDbType.Int, eZeeDataType.INT_SIZE, mintMembershipId)
            objDataOperation.AddParameter("@OtherMemId", SqlDbType.Int, eZeeDataType.INT_SIZE, mintNonPayrollMembershipId)
            objDataOperation.AddParameter("@PeriodId", SqlDbType.Int, eZeeDataType.INT_SIZE, mintPeriodId)
            objDataOperation.AddParameter("@tranheadunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintNITADeductionHeadId)


            dsList = objDataOperation.ExecQuery(StrQ, "DataTable")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & " " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            Dim mdtData As DataTable = New DataView(dsList.Tables(0), "", "employeename, Amount DESC", DataViewRowState.CurrentRows).ToTable


            For Each dtRow As DataRow In dsList.Tables("DataTable").Rows
                If CDec(dtRow.Item("amount")) = 0 Then Continue For

                strBuilder.Append(dtRow.Item("MembershipNo").ToString) 'Column A
                strBuilder.Append("," & dtRow.Item("employeename").ToString & "") 'Column B
                strBuilder.Append("," & dtRow.Item("OtherMembershipNo").ToString & "") 'Column C
                strBuilder.Append("," & Format(CDec(dtRow.Item("amount")), strFmtCurrency).Replace(",", "") & "") 'Column D

                strBuilder.Append(vbCrLf)
            Next


            If SaveTextFile(strExportPath, strBuilder) Then
                Return True
            Else
                Return False
            End If


        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: Generate_DetailReport; Module Name: " & mstrModuleName)
        End Try
    End Function

    Private Function SaveTextFile(ByVal fpath As String, ByVal sb As System.Text.StringBuilder) As Boolean
        Dim fsFile As New FileStream(fpath, FileMode.Create, FileAccess.Write)
        Dim strWriter As New StreamWriter(fsFile)
        Try

            With strWriter
                .BaseStream.Seek(0, SeekOrigin.End)
                .WriteLine(sb)
                .Close()
            End With
            Return True
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: SaveExcelfile; Module Name: " & mstrModuleName)
            Return False
        Finally
            sb = Nothing
            strWriter = Nothing
            fsFile = Nothing
        End Try
    End Function

#End Region

End Class
