﻿#Region " Imports "

Imports eZeeCommonLib
Imports Aruti.Data
Imports System.Text.RegularExpressions
Imports System.Text

#End Region
Public Class frmHousingLevyFormMReport

#Region " Private Variables "

    Private ReadOnly mstrModuleName As String = "frmHousingLevyFormMReport"
    Private objHousingLevyFormMReport As clsHousingLevyFormMReport
    Private mstrStringIds As String = String.Empty
    Private mstrStringName As String = String.Empty
    Private mintViewIdx As Integer = 0
    Private mstrAnalysis_Fields As String = ""
    Private mstrAnalysis_Join As String = ""
    Private mstrReport_GroupName As String = ""
    Private eReport As enArutiReport
    Private mintFirstOpenPeriod As Integer = 0
    Private mstrBaseCurrSign As String = ""
    Private mintBaseCurrId As Integer = 0
    Private mstrAdvanceFilter As String = String.Empty

#End Region

#Region " Contructor "

    Public Sub New()
        objHousingLevyFormMReport = New clsHousingLevyFormMReport(User._Object._Languageunkid, Company._Object._Companyunkid)
        objHousingLevyFormMReport.SetDefaultValue()
        InitializeComponent()
    End Sub

#End Region

#Region " Private Enum "
    Private Enum enHeadTypeId
        Identity_Number = 1
        KRA_PIN = 2
        Gross_Pay_Formula = 3
    End Enum
#End Region

#Region " Private Function "

    Private Sub FillCombo()
        Dim objMember As New clsmembership_master
        Dim dsCombos As New DataSet
        Dim objperiod As New clscommom_period_Tran
        Dim objHead As New clsTransactionHead
        Dim objMaster As New clsMasterData
        Try
            dsCombos = objMember.getListForCombo("Membership", True)
            With cboIdentityNumber
                .ValueMember = "membershipunkid"
                .DisplayMember = "name"
                .DataSource = dsCombos.Tables(0)
                .SelectedValue = 0
            End With

            With cboKRAPIN
                .ValueMember = "membershipunkid"
                .DisplayMember = "name"
                .DataSource = dsCombos.Tables(0).Copy
                .SelectedValue = 0
            End With

            mintFirstOpenPeriod = objMaster.getFirstPeriodID(enModuleReference.Payroll, FinancialYear._Object._YearUnkid, 1)

            dsCombos = objperiod.getListForCombo(enModuleReference.Payroll, FinancialYear._Object._YearUnkid, FinancialYear._Object._DatabaseName, FinancialYear._Object._Database_Start_Date, "Period", True)
            With cboPeriod
                .ValueMember = "periodunkid"
                .DisplayMember = "name"
                .DataSource = dsCombos.Tables(0)
                .SelectedValue = mintFirstOpenPeriod
            End With

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "FillCombo", mstrModuleName)
        Finally
            objMember = Nothing
            objperiod = Nothing
            objHead = Nothing
            objMaster = Nothing
            dsCombos.Dispose()
        End Try
    End Sub

    Private Sub ResetValue()
        Try
            cboKRAPIN.SelectedValue = 0
            cboPeriod.SelectedValue = mintFirstOpenPeriod
            cboIdentityNumber.SelectedValue = 0
            txtGrossPayFormula.Text = ""

            mstrStringIds = ""
            mstrStringName = ""
            mintViewIdx = -1
            mstrAnalysis_Fields = ""
            mstrAnalysis_Join = ""
            mstrReport_GroupName = ""
            mstrAdvanceFilter = ""


            Call GetValue()

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "ResetValue", mstrModuleName)
        End Try
    End Sub

    Private Sub GetValue()
        Dim objUserDefRMode As New clsUserDef_ReportMode
        Dim dsList As DataSet
        Try
            dsList = objUserDefRMode.GetList("List", enArutiReport.Affordable_Housing_Levy_Form_M_Report)

            If dsList.Tables("List").Rows.Count > 0 Then

                For Each dsRow As DataRow In dsList.Tables("List").Rows
                    Select Case CInt(dsRow.Item("headtypeid"))

                        Case enHeadTypeId.Identity_Number
                            cboIdentityNumber.SelectedValue = CInt(dsRow.Item("transactionheadid"))

                        Case enHeadTypeId.KRA_PIN
                            cboKRAPIN.SelectedValue = CInt(dsRow.Item("transactionheadid"))

                        Case enHeadTypeId.Gross_Pay_Formula
                            txtGrossPayFormula.Text = CStr(dsRow.Item("transactionheadid"))

                    End Select
                Next

            End If

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "GetValue", mstrModuleName)
        Finally
            objUserDefRMode = Nothing
        End Try
    End Sub

    Private Function SetFilter() As Boolean
        Try
            Call objHousingLevyFormMReport.SetDefaultValue()

            If CInt(cboPeriod.SelectedValue) <= 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 1, "Period is mandatory information. Please select Period to continue."), enMsgBoxStyle.Information)
                cboPeriod.Focus()
                Exit Function
            ElseIf CInt(cboIdentityNumber.SelectedValue) <= 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 2, "Identity Number is mandatory information. Please select Identity Number to continue."), enMsgBoxStyle.Information)
                cboIdentityNumber.Focus()
                Exit Function
            ElseIf CInt(cboKRAPIN.SelectedValue) <= 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 3, "KRA PIN Membership is mandatory information. Please select KRA PIN Membership to continue."), enMsgBoxStyle.Information)
                cboKRAPIN.Focus()
                Exit Function
            ElseIf txtGrossPayFormula.Text.Trim.Length <= 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 4, "Gross Pay Formula is mandatory information. Please enter Gross Pay Formula to continue."), enMsgBoxStyle.Information)
                txtGrossPayFormula.Focus()
                Exit Function
            End If

            If txtGrossPayFormula.Text.Trim.Length > 0 Then
                Dim objDHead As New Dictionary(Of Integer, String)
                Dim objHead As New clsTransactionHead
                Dim dsHead As DataSet = objHead.getComboList(FinancialYear._Object._DatabaseName, "List", False, , , , True, , , , True, , , True)
                Dim strPattern As String = "([^#]*#[^#]*)#"
                Dim strReplacement As String = "$1)"
                Dim strResult As String = Regex.Replace(txtGrossPayFormula.Text, strPattern, strReplacement)
                Dim arr As MatchCollection = Regex.Matches(strResult, "(?<=\#)[^\)]*(?=\))", RegexOptions.IgnoreCase)
                For Each s As RegularExpressions.Match In arr
                    Dim scode As String = s.Groups(0).ToString
                    If scode.Trim <> "" Then
                        Dim dr() As DataRow = dsHead.Tables(0).Select("code = '" & scode & "' ")
                        If dr.Length > 0 Then
                            If objDHead.ContainsKey(CInt(dr(0).Item("tranheadunkid"))) = False Then
                                objDHead.Add(CInt(dr(0).Item("tranheadunkid")), scode)
                            End If
                        End If
                    End If
                Next
                If objDHead.Count <= 0 Then
                    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 5, " At least one Transaction Head is mandatory information in Gross Pay Formula. Please enter Transaction Head in Gross Pay Formula to continue."), enMsgBoxStyle.Information)
                    txtGrossPayFormula.Focus()
                    Exit Function
                End If
            End If

            objHousingLevyFormMReport._PeriodId = CInt(cboPeriod.SelectedValue)
            objHousingLevyFormMReport._PeriodName = cboPeriod.Text

            objHousingLevyFormMReport._IdentityNumberId = CInt(cboIdentityNumber.SelectedValue)
            objHousingLevyFormMReport._IdentityNumberName = cboIdentityNumber.Text

            objHousingLevyFormMReport._KRAPINId = CInt(cboKRAPIN.SelectedValue)
            objHousingLevyFormMReport._KRAPINName = cboKRAPIN.Text

            objHousingLevyFormMReport._GrossPayFormula = IIf(txtGrossPayFormula.Text.Trim.Length <= 0, "0", txtGrossPayFormula.Text)

            objHousingLevyFormMReport._ViewByIds = mstrStringIds
            objHousingLevyFormMReport._ViewIndex = mintViewIdx
            objHousingLevyFormMReport._ViewByName = mstrStringName
            objHousingLevyFormMReport._Analysis_Fields = mstrAnalysis_Fields
            objHousingLevyFormMReport._Analysis_Join = mstrAnalysis_Join
            objHousingLevyFormMReport._Report_GroupName = mstrReport_GroupName


            objHousingLevyFormMReport._FirstNamethenSurname = ConfigParameter._Object._FirstNamethenSurname

            Return True
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "SetFilter", mstrModuleName)
        End Try
    End Function

#End Region

#Region " Forms "

    Private Sub frmHousingLevyFormMReport_FormClosed(ByVal sender As Object, ByVal e As System.Windows.Forms.FormClosedEventArgs) Handles Me.FormClosed
        Try
            objHousingLevyFormMReport = Nothing
        Catch ex As Exception
            DisplayError.Show(-1, ex.Message, "frmHousingLevyFormMReport_FormClosed", mstrModuleName)
        End Try
    End Sub

    Private Sub frmHousingLevyFormMReport_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            Call Language.setLanguage(Me.Name)

            Me.eZeeHeader.Title = objHousingLevyFormMReport._ReportName
            Me.eZeeHeader.Message = objHousingLevyFormMReport._ReportDesc

            Call OtherSettings()

            Call FillCombo()
            Call ResetValue()

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmHousingLevyFormMReport_Load", mstrModuleName)
        End Try
    End Sub

    Private Sub Form_KeyDown(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles Me.KeyDown
        Try
            If e.Control Then
                If e.KeyCode = Windows.Forms.Keys.R Then

                End If
            End If
        Catch ex As Exception
            DisplayError.Show(-1, ex.Message, "Form_KeyDown", mstrModuleName)
        End Try
    End Sub

    Private Sub Form_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles Me.KeyPress
        Try
            Select Case e.KeyChar
                Case CChar(ChrW(CInt(Windows.Forms.Keys.Enter)))
                    Windows.Forms.SendKeys.Send("{TAB}")
                    e.Handled = True
                    Exit Select
            End Select
        Catch ex As Exception
            DisplayError.Show(-1, ex.Message, "Form_KeyPress", mstrModuleName)
        End Try
    End Sub

#End Region

#Region " Buttons "

    Private Sub btnClose_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnClose.Click
        Try
            Me.Close()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "btnClose_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub btnExport_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnExport.Click
        Try
            If Not SetFilter() Then Exit Sub

            SaveDialog.FileName = ""
            SaveDialog.Filter = "CSV File|*.csv"
            SaveDialog.FilterIndex = 0
            SaveDialog.ShowDialog()
            If SaveDialog.FileName = "" Then
                Exit Try
            End If


            Dim objPeriod As New clscommom_period_Tran
            objPeriod._Periodunkid(FinancialYear._Object._DatabaseName) = CInt(cboPeriod.SelectedValue)

            objHousingLevyFormMReport._Analysis_Join = mstrAnalysis_Join.Replace(ConfigParameter._Object._EmployeeAsOnDate, objPeriod._End_Date)


            If objHousingLevyFormMReport.Generate_DetailReport(FinancialYear._Object._DatabaseName, _
                                             User._Object._Userunkid, _
                                             FinancialYear._Object._YearUnkid, _
                                             Company._Object._Companyunkid, _
                                             objPeriod._Start_Date, _
                                             objPeriod._End_Date, _
                                             ConfigParameter._Object._UserAccessModeSetting, _
                                             True, _
                                             ConfigParameter._Object._Base_CurrencyId, _
                                             GUI.fmtCurrency, _
                                             SaveDialog.FileName _
                                             ) = True Then

                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 7, "Data Exported Successfuly."), enMsgBoxStyle.Information)
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "btnExport_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub btnReset_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnReset.Click
        Try
            Call ResetValue()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "btnReset_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub objbtnLanguage_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnLanguage.Click
        Dim objfrm As New frmLanguage
        Try
            If User._Object._Isrighttoleft = True Then
                objfrm.RightToLeft = Windows.Forms.RightToLeft.Yes
                objfrm.RightToLeftLayout = True
                Call Language.ctlRightToLeftlayOut(objfrm)
            End If

            Call SetMessages()

            'clsHousingLevyFormMReport.SetMessages()
            objfrm._Other_ModuleNames = "clsHousingLevyFormMReport"
            objfrm.displayDialog(Me)

            Call Language.setLanguage(Me.Name)
            Call SetLanguage()

        Catch ex As System.Exception
            Call DisplayError.Show(-1, ex.Message, "objbtnLanguage_Click", mstrModuleName)
        Finally
            objfrm.Dispose()
            objfrm = Nothing
        End Try
    End Sub

    Private Sub btnSaveSelection_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSaveSelection.Click
        Dim objUserDefRMode As New clsUserDef_ReportMode
        Try
            If SetFilter() = False Then Exit Try

            For intHeadType As Integer = 1 To GetEnumHighestValue(Of enHeadTypeId)()
                objUserDefRMode = New clsUserDef_ReportMode()

                Dim intUnkid As Integer = -1

                objUserDefRMode._Reportunkid = enArutiReport.Affordable_Housing_Levy_Form_M_Report
                objUserDefRMode._Reporttypeid = 0
                objUserDefRMode._Reportmodeid = 0

                Select Case intHeadType

                    Case enHeadTypeId.Identity_Number
                        objUserDefRMode._Headtypeid = intHeadType
                        objUserDefRMode._EarningTranHeadIds = cboIdentityNumber.SelectedValue.ToString

                        intUnkid = objUserDefRMode.isExist(enArutiReport.Affordable_Housing_Levy_Form_M_Report, 0, 0, intHeadType)

                    Case enHeadTypeId.KRA_PIN
                        objUserDefRMode._Headtypeid = intHeadType
                        objUserDefRMode._EarningTranHeadIds = cboKRAPIN.SelectedValue.ToString

                        intUnkid = objUserDefRMode.isExist(enArutiReport.Affordable_Housing_Levy_Form_M_Report, 0, 0, intHeadType)

                    Case enHeadTypeId.Gross_Pay_Formula
                        objUserDefRMode._Headtypeid = intHeadType
                        objUserDefRMode._EarningTranHeadIds = txtGrossPayFormula.Text

                        intUnkid = objUserDefRMode.isExist(enArutiReport.Affordable_Housing_Levy_Form_M_Report, 0, 0, intHeadType)

                End Select

                objUserDefRMode._Reportmodeunkid = intUnkid

                If intUnkid <= 0 Then
                    objUserDefRMode.Insert()
                Else
                    objUserDefRMode.Update()
                End If

            Next

            eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 6, "Selection Saved Successfully"), enMsgBoxStyle.Information)

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "btnSaveSelection_Click", mstrModuleName)
        Finally
            objUserDefRMode = Nothing
        End Try
    End Sub

#End Region


	'<Language> This Auto Generated Text Please Do Not Modify it.
#Region " Language & UI Settings "
	Private Sub OtherSettings()
		Try
			Me.SuspendLayout()
			
			Call SetLanguage()
			
			Me.gbMandatoryInfo.GradientColor = GUI._eZeeContainerHeaderBackColor
			Me.gbMandatoryInfo.ForeColor = GUI._eZeeContainerHeaderForeColor

            Me.eZeeHeader.GradientColor1 = GUI._HeaderBackColor1
			Me.eZeeHeader.GradientColor2 = GUI._HeaderBackColor2
			Me.eZeeHeader.BorderColor = GUI._HeaderBorderColor
			Me.eZeeHeader.HeaderTextForeColor = GUI._HeaderTitleFontColor
			Me.eZeeHeader.DescriptionForeColor = GUI._HeaderMessageFontColor

			Me.btnSaveSelection.GradientBackColor = GUI._ButttonBackColor
			Me.btnSaveSelection.GradientForeColor = GUI._ButttonFontColor

			Me.objbtnLanguage.GradientBackColor = GUI._ButttonBackColor
			Me.objbtnLanguage.GradientForeColor = GUI._ButttonFontColor

			Me.btnClose.GradientBackColor = GUI._ButttonBackColor
			Me.btnClose.GradientForeColor = GUI._ButttonFontColor

			Me.btnReset.GradientBackColor = GUI._ButttonBackColor
			Me.btnReset.GradientForeColor = GUI._ButttonFontColor

			Me.btnExport.GradientBackColor = GUI._ButttonBackColor
			Me.btnExport.GradientForeColor = GUI._ButttonFontColor

			Me.ResumeLayout()
		Catch Ex As Exception
            DisplayError.Show("-1", Ex.Message, "OtherSettings", mstrModuleName)
		End Try
	End Sub
			
			
	Private Sub SetLanguage()
		Try
			Me.Text = Language._Object.getCaption(Me.Name, Me.Text)
			
			Me.lblGrossPayFormula.Text = Language._Object.getCaption(Me.lblGrossPayFormula.Name, Me.lblGrossPayFormula.Text)
			Me.eZeeHeader.Title = Language._Object.getCaption(Me.eZeeHeader.Name & "_Title" , Me.eZeeHeader.Title)
			Me.eZeeHeader.Message = Language._Object.getCaption(Me.eZeeHeader.Name & "_Message" , Me.eZeeHeader.Message)
			Me.gbMandatoryInfo.Text = Language._Object.getCaption(Me.gbMandatoryInfo.Name, Me.gbMandatoryInfo.Text)
			Me.btnSaveSelection.Text = Language._Object.getCaption(Me.btnSaveSelection.Name, Me.btnSaveSelection.Text)
			Me.lblKRAPIN.Text = Language._Object.getCaption(Me.lblKRAPIN.Name, Me.lblKRAPIN.Text)
			Me.LblCurrencyRate.Text = Language._Object.getCaption(Me.LblCurrencyRate.Name, Me.LblCurrencyRate.Text)
			Me.lblIdentityNumber.Text = Language._Object.getCaption(Me.lblIdentityNumber.Name, Me.lblIdentityNumber.Text)
			Me.lblPeriod.Text = Language._Object.getCaption(Me.lblPeriod.Name, Me.lblPeriod.Text)
			Me.btnClose.Text = Language._Object.getCaption(Me.btnClose.Name, Me.btnClose.Text)
			Me.btnReset.Text = Language._Object.getCaption(Me.btnReset.Name, Me.btnReset.Text)
			Me.btnExport.Text = Language._Object.getCaption(Me.btnExport.Name, Me.btnExport.Text)
			
		Catch Ex As Exception
            DisplayError.Show("-1", Ex.Message, "SetLanguage", mstrModuleName)
		End Try
	End Sub
			
			
	Public Sub SetMessages()
		Try
			Language.setMessage(mstrModuleName, 1, "Period is mandatory information. Please select Period to continue.")
			Language.setMessage(mstrModuleName, 2, "Identity Number is mandatory information. Please select Identity Number to continue.")
			Language.setMessage(mstrModuleName, 3, "KRA PIN Membership is mandatory information. Please select KRA PIN Membership to continue.")
			Language.setMessage(mstrModuleName, 4, "Gross Pay Formula is mandatory information. Please enter Gross Pay Formula to continue.")
			Language.setMessage(mstrModuleName, 5, " At least one Transaction Head is mandatory information in Gross Pay Formula. Please enter Transaction Head in Gross Pay Formula to continue.")
			Language.setMessage(mstrModuleName, 6, "Selection Saved Successfully")
			Language.setMessage(mstrModuleName, 7, "Data Exported Successfuly.")
			
		Catch Ex As Exception
            DisplayError.Show("-1", Ex.Message, "SetMessages", mstrModuleName)
		End Try
	End Sub
#End Region 'Language & UI Settings
	'</Language>

 
End Class