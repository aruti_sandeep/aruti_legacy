'************************************************************************************************************************************
'Class Name : clsTaxReport.vb
'Purpose    :
'Date       :18/05/2013
'Written By :Sohail
'Modified   :
'************************************************************************************************************************************

#Region " Imports "

Imports eZeeCommonLib
Imports Aruti.Data

#End Region

''' <summary>
''' Purpose: Report Generation Class 
''' Developer: Sohail
''' </summary>
Public Class clsTaxReport
    Inherits IReportData

    Private Shared ReadOnly mstrModuleName As String = "clsTaxReport"
    Private mstrReportId As String = enArutiReport.Tax_Report

    Dim objDataOperation As clsDataOperation

#Region " Constructor "

    Public Sub New(Byval intLangId As Integer,Byval intCompanyId AS Integer) 'S.SANDEEP |24-Sep-2020| -- START --END
        Me.setReportData(CInt(mstrReportId),intLangId,intCompanyId) 'S.SANDEEP |24-Sep-2020| -- START --END
        Call Create_OnDetailReport()
    End Sub

#End Region

#Region " Private Variables "

    'For Report
    Private mintEmpId As Integer = 0
    Private mstrEmpName As String = ""
    Private mintHousingTranId As Integer = 0
    Private mstrStatutoryIds As String = ""
    Private mstrPeriodIds As String = ""
    Private mstrPeriodNames As String = String.Empty
    Private mintPeriodCount As Integer = -1
    Private mintPayeHeadId As Integer = 0
    Private mstrPayeHeadName As String = ""
    Private mintMembershipId As Integer = 0
    Private mstrMembershipName As String = ""
    Private mintOtherEarningTranId As Integer = 0 'Sohail (28 Aug 2013)
    'Sohail (29 Mar 2014) -- Start
    'ENHANCEMENT - Show Payroll No. and Employee TIN No on Tax Report.
    Private mblnShowPayrollNo As Boolean = False
    Private mblnShowEmployeeTINNo As Boolean = False
    'Sohail (29 Mar 2014) -- End

    Private mblnIncludeInactiveEmp As Boolean = True
    Private mdtSlabDate As Date
    'Sohail (23 Dec 2019) -- Start
    'AKF Enhancement # 0004231: Advanced Filter is required for NSSF Form and Tax Report.
    Private mstrAdvanceFilter As String = String.Empty
    'Sohail (23 Dec 2019) -- End

    'For Analysis By
    Private mstrAnalysis_Fields As String = ""
    Private mstrAnalysis_Join As String = ""
    Private mstrAnalysis_OrderBy As String = ""
    Private mintViewIndex As Integer = -1
    Private mstrViewByIds As String = String.Empty
    Private mstrViewByName As String = String.Empty
    Private mstrReport_GroupName As String = ""

#End Region

#Region " Properties "

    Public WriteOnly Property _EmpId() As Integer
        Set(ByVal value As Integer)
            mintEmpId = value
        End Set
    End Property

    Public WriteOnly Property _EmpName() As String
        Set(ByVal value As String)
            mstrEmpName = value
        End Set
    End Property

    Public WriteOnly Property _StatutoryIds() As String
        Set(ByVal value As String)
            mstrStatutoryIds = value
        End Set
    End Property

    Public WriteOnly Property _PayeHeadId() As Integer
        Set(ByVal value As Integer)
            mintPayeHeadId = value
        End Set
    End Property

    Public WriteOnly Property _PayeHeadName() As String
        Set(ByVal value As String)
            mstrPayeHeadName = value
        End Set
    End Property

    Public WriteOnly Property _MembershipId() As Integer
        Set(ByVal value As Integer)
            mintMembershipId = value
        End Set
    End Property

    Public WriteOnly Property _MembershipName() As String
        Set(ByVal value As String)
            mstrMembershipName = value
        End Set
    End Property

    Public WriteOnly Property _PeriodIds() As String
        Set(ByVal value As String)
            mstrPeriodIds = value
        End Set
    End Property

    'Sohail (28 Aug 2013) -- Start
    'TRA - ENHANCEMENT
    Public WriteOnly Property _OtherEarningTranId() As Integer
        Set(ByVal value As Integer)
            mintOtherEarningTranId = value
        End Set
    End Property
    'Sohail (28 Aug 2013) -- End

    Public WriteOnly Property _IncludeInactiveEmp() As Boolean
        Set(ByVal value As Boolean)
            mblnIncludeInactiveEmp = value
        End Set
    End Property

    Public WriteOnly Property _PeriodNames() As String
        Set(ByVal value As String)
            mstrPeriodNames = value
        End Set
    End Property

    Public WriteOnly Property _ViewIndex() As Integer
        Set(ByVal value As Integer)
            mintViewIndex = value
        End Set
    End Property

    Public WriteOnly Property _ViewByIds() As String
        Set(ByVal value As String)
            mstrViewByIds = value
        End Set
    End Property

    Public WriteOnly Property _ViewByName() As String
        Set(ByVal value As String)
            mstrViewByName = value
        End Set
    End Property

    Public WriteOnly Property _Analysis_Fields() As String
        Set(ByVal value As String)
            mstrAnalysis_Fields = value
        End Set
    End Property

    Public WriteOnly Property _Analysis_Join() As String
        Set(ByVal value As String)
            mstrAnalysis_Join = value
        End Set
    End Property

    Public WriteOnly Property _Analysis_OrderBy() As String
        Set(ByVal value As String)
            mstrAnalysis_OrderBy = value
        End Set
    End Property

    Public WriteOnly Property _Report_GroupName() As String
        Set(ByVal value As String)
            mstrReport_GroupName = value
        End Set
    End Property

    Public WriteOnly Property _PeriodEndDate() As DateTime
        Set(ByVal value As DateTime)
            mdtSlabDate = value
        End Set
    End Property

    'Sohail (29 Mar 2014) -- Start
    'ENHANCEMENT - Show Payroll No. and Employee TIN No on Tax Report.
    Public WriteOnly Property _ShowPayrollNo() As Boolean
        Set(ByVal value As Boolean)
            mblnShowPayrollNo = value
        End Set
    End Property

    Public WriteOnly Property _ShowEmployeeTINNo() As Boolean
        Set(ByVal value As Boolean)
            mblnShowEmployeeTINNo = value
        End Set
    End Property
    'Sohail (29 Mar 2014) -- End

    'Sohail (23 Dec 2019) -- Start
    'AKF Enhancement # 0004231: Advanced Filter is required for NSSF Form and Tax Report.
    Public WriteOnly Property _AdvanceFilter() As String
        Set(ByVal value As String)
            mstrAdvanceFilter = value
        End Set
    End Property
    'Sohail (23 Dec 2019) -- End

#End Region

#Region "Public Function & Procedures "

    Public Sub SetDefaultValue()
        Try

            mintEmpId = 0
            mstrEmpName = ""
            mintHousingTranId = 0
            mstrStatutoryIds = ""
            mstrPeriodIds = ""
            mintPayeHeadId = 0
            mstrPayeHeadName = ""
            mintMembershipId = 0
            mstrMembershipName = ""
            mintOtherEarningTranId = 0 'Sohail (28 Aug 2013)
            'Sohail (29 Mar 2014) -- Start
            'ENHANCEMENT - Show Payroll No. and Employee TIN No on Tax Report.
            mblnShowPayrollNo = False
            mblnShowEmployeeTINNo = False
            'Sohail (29 Mar 2014) -- End

            mblnIncludeInactiveEmp = True


            mstrAnalysis_Fields = ""
            mstrAnalysis_Join = ""
            mstrAnalysis_OrderBy = ""
            mintViewIndex = -1
            mstrViewByIds = ""
            mstrReport_GroupName = ""

            'Sohail (23 Dec 2019) -- Start
            'AKF Enhancement # 0004231: Advanced Filter is required for NSSF Form and Tax Report.
            mstrAdvanceFilter = ""
            'Sohail (23 Dec 2019) -- End
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: SetDefaultValue; Module Name: " & mstrModuleName)
        End Try
    End Sub

    Private Sub FilterTitleAndFilterQuery()
        Me._FilterQuery = ""
        Me._FilterTitle = ""
        Try
            objDataOperation.AddParameter("@payetranheadunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintPayeHeadId)

            If mintMembershipId > 0 Then
                'Me._FilterQuery = "AND hrmembership_master.membershipunkid = @membershipunkid " 'Sohail (14 Jun 2013)
                Me._FilterTitle = "Membership : " & mstrMembershipName

                objDataOperation.AddParameter("@membershipunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintMembershipId)
            End If

            'Sohail (28 Aug 2013) -- Start
            'TRA - ENHANCEMENT
            If mintOtherEarningTranId > 0 Then
                objDataOperation.AddParameter("@OtherEarningTranId", SqlDbType.Int, eZeeDataType.INT_SIZE, mintOtherEarningTranId)
            End If
            'Sohail (28 Aug 2013) -- End

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: FilterTitleAndFilterQuery; Module Name: " & mstrModuleName)
        End Try
    End Sub

    Public Overrides Sub generateReport(ByVal pintReportType As Integer, Optional ByVal PrintAction As Aruti.Data.enPrintAction = Aruti.Data.enPrintAction.Preview, Optional ByVal ExportAction As Aruti.Data.enExportAction = Aruti.Data.enExportAction.None)
        'Dim objRpt As CrystalDecisions.CrystalReports.Engine.ReportClass = Nothing
        'Dim strReportExportFile As String = ""

        'Try

        '    objRpt = Generate_DetailReport()

        '    If Not IsNothing(objRpt) Then
        '        Call ReportExecute(objRpt, PrintAction, ExportAction, ConfigParameter._Object._ExportReportPath, ConfigParameter._Object._OpenAfterExport)
        '    End If
        'Catch ex As Exception
        '    Throw New Exception(ex.Message & "; Procedure Name: generateReport; Module Name: " & mstrModuleName)
        'End Try
    End Sub

    'S.SANDEEP [04 JUN 2015] -- START
    'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
    Public Overrides Sub generateReportNew(ByVal xDatabaseName As String, ByVal xUserUnkid As Integer, ByVal xYearUnkid As Integer, ByVal xCompanyUnkid As Integer, ByVal xPeriodStart As Date, ByVal xPeriodEnd As Date, ByVal xUserModeSetting As String, ByVal xOnlyApproved As Boolean, ByVal xExportReportPath As String, ByVal xOpenReportAfterExport As Boolean, ByVal pintReportType As Integer, Optional ByVal PrintAction As enPrintAction = enPrintAction.Preview, Optional ByVal ExportAction As enExportAction = enExportAction.None, Optional ByVal xBaseCurrencyId As Integer = 0)
        Dim objRpt As CrystalDecisions.CrystalReports.Engine.ReportClass = Nothing
        Dim strReportExportFile As String = ""
        Try
            objRpt = Generate_DetailReport(xDatabaseName, xUserUnkid, xYearUnkid, xCompanyUnkid, xPeriodEnd, xUserModeSetting, xOnlyApproved, xBaseCurrencyId)

            If Not IsNothing(objRpt) Then
                Call ReportExecute(objRpt, PrintAction, ExportAction, xExportReportPath, xOpenReportAfterExport)
            End If
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: generateReportNew; Module Name: " & mstrModuleName)
        Finally
        End Try
    End Sub
    'S.SANDEEP [04 JUN 2015] -- END

    Public Overrides Sub setDefaultOrderBy(ByVal intReportType As Integer)
        OrderByDisplay = ""
        OrderByQuery = ""
        Try

            OrderByDisplay = iColumn_DetailReport.ColumnItem(0).DisplayName
            OrderByQuery = iColumn_DetailReport.ColumnItem(0).Name

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: setDefaultOrderBy; Module Name: " & mstrModuleName)
        End Try
    End Sub

    Public Overrides Sub setOrderBy(ByVal intReportType As Integer)
        Try
            Call OrderByExecute(iColumn_DetailReport)
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: setOrderBy; Module Name: " & mstrModuleName)
        End Try
    End Sub
#End Region


#Region " Report Generation "
    Dim iColumn_DetailReport As New IColumnCollection

    Public Property Field_OnDetailReport() As IColumnCollection
        Get
            Return iColumn_DetailReport
        End Get
        Set(ByVal value As IColumnCollection)
            iColumn_DetailReport = value
        End Set
    End Property

    Private Sub Create_OnDetailReport()
        Try
            iColumn_DetailReport.Clear()
            iColumn_DetailReport.Add(New IColumn("", ""))
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: Create_OnDetailReport; Module Name: " & mstrModuleName)
        End Try
    End Sub


    'S.SANDEEP [04 JUN 2015] -- START
    'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
    Private Function Generate_DetailReport(ByVal strDatabaseName As String, _
                                           ByVal intUserUnkid As Integer, _
                                           ByVal intYearUnkid As Integer, _
                                           ByVal intCompanyUnkid As Integer, _
                                           ByVal dtPeriodEnd As Date, _
                                           ByVal strUserModeSetting As String, _
                                           ByVal blnOnlyApproved As Boolean, _
                                           ByVal intBaseCurrencyId As Integer) As CrystalDecisions.CrystalReports.Engine.ReportClass
        'Private Function Generate_DetailReport() As CrystalDecisions.CrystalReports.Engine.ReportClass
        'S.SANDEEP [04 JUN 2015] -- END
        Dim objRpt As CrystalDecisions.CrystalReports.Engine.ReportClass = Nothing
        Dim StrQ As String = ""
        Dim dsList As New DataSet
        Dim exForce As Exception
        Dim rpt_Data As ArutiReport.Designer.dsArutiReport
        Dim strBaseCurrencySign As String

        Try
            objDataOperation = New clsDataOperation

            Dim objExchangeRate As New clsExchangeRate
            Dim decDecimalPlaces As Decimal = 0

            'S.SANDEEP [04 JUN 2015] -- START
            'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
            'objExchangeRate._ExchangeRateunkid = ConfigParameter._Object._Base_CurrencyId
            objExchangeRate._ExchangeRateunkid = intBaseCurrencyId
            'S.SANDEEP [04 JUN 2015] -- END

            'Sohail (29 Mar 2017) -- Start
            'Issue - 65.2 - Amount not matching with Payroll Report.
            'decDecimalPlaces = objExchangeRate._Digits_After_Decimal
            'Sohail (29 Nov 2017) -- Start
            'SUMATRA – issue # 0001666: Discrepancy in various reports in 70.1.
            'decDecimalPlaces = 6
            decDecimalPlaces = objExchangeRate._Digits_After_Decimal
            'Sohail (29 Nov 2017) -- End
            'Sohail (29 Mar 2017) -- End
            strBaseCurrencySign = objExchangeRate._Currency_Sign

            'S.SANDEEP [04 JUN 2015] -- START
            'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
            Dim xUACQry, xUACFiltrQry As String : xUACQry = "" : xUACFiltrQry = ""
            Call NewAccessLevelFilterString(xUACQry, xUACFiltrQry, dtPeriodEnd, blnOnlyApproved, strDatabaseName, intUserUnkid, intCompanyUnkid, intYearUnkid, strUserModeSetting)
            'S.SANDEEP [04 JUN 2015] -- END
            'Sohail (23 Dec 2019) -- Start
            'AKF Enhancement # 0004231: Advanced Filter is required for NSSF Form and Tax Report.
            Dim xAdvanceJoinQry As String = ""
            Call GetAdvanceFilterQry(xAdvanceJoinQry, dtPeriodEnd, strDatabaseName)
            'Sohail (23 Dec 2019) -- End

            StrQ = "SELECT   basicpay AS basicpay " & _
                          ", noncashbenefit AS noncashbenefit " & _
                          ", allowancebenefit AS allowancebenefit " & _
                          ", basicpay + noncashbenefit + ( allowancebenefit ) AS Grosspay " & _
                          ", ( basicpay + noncashbenefit + ( allowancebenefit ) ) - deduction AS taxable " & _
                          ", deduction AS deduction " & _
                          ", taxpayable AS taxpaid " & _
                          ", empid AS empid " & _
                          ", hremployee_master.employeecode AS empcode " & _
                          ", ISNULL(hremployee_meminfo_tran.membershipno, '') AS membershipno "

            If ConfigParameter._Object._FirstNamethenSurname = True Then
                StrQ &= ", ISNULL(firstname, '') + ' ' + ISNULL(othername, '') + ' ' + ISNULL(surname, '') AS empname "
            Else
                StrQ &= ", ISNULL(surname, '') + ' ' + ISNULL(firstname, '') + ' ' + ISNULL(othername, '') AS empname "
            End If


            StrQ &= "FROM    ( SELECT    SUM(ISNULL(PAYE.basicsalary, 0)) AS basicpay " & _
                                      ", SUM(ISNULL(PAYE.allowance, 0)) AS allowancebenefit " & _
                                      ", SUM(ISNULL(PAYE.noncashbenefit, 0)) AS noncashbenefit " & _
                                      ", SUM(ISNULL(PAYE.deduction, 0)) AS deduction " & _
                                      ", SUM(ISNULL(PAYE.taxpayable, 0)) AS taxpayable " & _
                                      ", SUM(ISNULL(PAYE.paye, 0)) AS taxpaid " & _
                                      ", paye.empid AS empid " & _
                              "FROM      ( SELECT    CAST(ISNULL(prpayrollprocess_tran.amount, 0) AS DECIMAL(36, " & decDecimalPlaces & ")) AS basicsalary " & _
                                                  ", 0 AS allowance " & _
                                                  ", 0 AS noncashbenefit " & _
                                                  ", 0 AS deduction " & _
                                                  ", 0 AS taxpayable " & _
                                                  ", 0 AS paye " & _
                                                  ", prpayrollprocess_tran.employeeunkid AS empid " & _
                                          "FROM      prpayrollprocess_tran " & _
                                                    "JOIN prtnaleave_tran ON prpayrollprocess_tran.tnaleavetranunkid = prtnaleave_tran.tnaleavetranunkid " & _
                                                    "JOIN hremployee_master ON prpayrollprocess_tran.employeeunkid = hremployee_master.employeeunkid " & _
                                                    "LEFT JOIN prtranhead_master ON prtranhead_master.tranheadunkid = prpayrollprocess_tran.tranheadunkid "
            'Sohail (23 Dec 2019) -- Start
            'AKF Enhancement # 0004231: Advanced Filter is required for NSSF Form and Tax Report.
            'StrQ &= mstrAnalysis_Join
            If mstrAdvanceFilter.Trim.Length > 0 AndAlso xAdvanceJoinQry.Trim.Length > 0 Then
                StrQ &= xAdvanceJoinQry
            End If
            'Sohail (23 Dec 2019) -- End

            'S.SANDEEP [04 JUN 2015] -- START
            'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
            If xUACQry.Trim.Length > 0 Then
                StrQ &= xUACQry
            End If
            'S.SANDEEP [04 JUN 2015] -- END

            StrQ &= "                      WHERE     ISNULL(prpayrollprocess_tran.isvoid, 0) = 0 " & _
                                                    "AND ISNULL(prtnaleave_tran.isvoid, 0) = 0 " & _
                                                    "AND prpayrollprocess_tran.tranheadunkid > 0 " & _
                                                    "AND prtnaleave_tran.payperiodunkid IN (" & mstrPeriodIds & ") "

            If mintOtherEarningTranId > 0 Then 'Basic Salary as Other Earning
                StrQ &= "AND prpayrollprocess_tran.tranheadunkid = @OtherEarningTranId  "
            Else
                StrQ &= "AND prtranhead_master.typeof_id = " & enTypeOf.Salary & "  "
            End If

            If mintEmpId > 0 Then
                StrQ &= "                            AND prpayrollprocess_tran.employeeunkid = @employeeunkid "
            End If

            'S.SANDEEP [04 JUN 2015] -- START
            'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
            'If UserAccessLevel._AccessLevel.Length > 0 Then
            '    StrQ &= UserAccessLevel._AccessLevelFilterString
            'End If

            If xUACFiltrQry.Trim.Length > 0 Then
                StrQ &= " AND " & xUACFiltrQry
            End If

            'S.SANDEEP [04 JUN 2015] -- END

            'Sohail (23 Dec 2019) -- Start
            'AKF Enhancement # 0004231: Advanced Filter is required for NSSF Form and Tax Report.
            If mstrAdvanceFilter.Length > 0 AndAlso mstrAdvanceFilter.Contains("ADF.") = True Then
                StrQ &= "AND " & mstrAdvanceFilter
            End If
            'Sohail (23 Dec 2019) -- End

            StrQ &= "                      UNION ALL " & _
                                          "SELECT    0 AS basicsalary " & _
                                                  ", 0 AS allowance " & _
                                                  ", CAST(ISNULL(prpayrollprocess_tran.amount, 0) AS DECIMAL(36, " & decDecimalPlaces & ")) AS noncashbenefit " & _
                                                  ", 0 AS deduction " & _
                                                  ", 0 AS taxpayable " & _
                                                  ", 0 AS paye " & _
                                                  ", prpayrollprocess_tran.employeeunkid AS empid " & _
                                          "FROM      prpayrollprocess_tran " & _
                                                    "JOIN prtnaleave_tran ON prpayrollprocess_tran.tnaleavetranunkid = prtnaleave_tran.tnaleavetranunkid " & _
                                                    "JOIN hremployee_master ON prpayrollprocess_tran.employeeunkid = hremployee_master.employeeunkid " & _
                                                    "LEFT JOIN prtranhead_master ON prtranhead_master.tranheadunkid = prpayrollprocess_tran.tranheadunkid "

            'Sohail (23 Dec 2019) -- Start
            'AKF Enhancement # 0004231: Advanced Filter is required for NSSF Form and Tax Report.
            'StrQ &= mstrAnalysis_Join
            If mstrAdvanceFilter.Trim.Length > 0 AndAlso xAdvanceJoinQry.Trim.Length > 0 Then
                StrQ &= xAdvanceJoinQry
            End If
            'Sohail (23 Dec 2019) -- End

            'S.SANDEEP [04 JUN 2015] -- START
            'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
            If xUACQry.Trim.Length > 0 Then
                StrQ &= xUACQry
            End If
            'S.SANDEEP [04 JUN 2015] -- END

            StrQ &= "                      WHERE     ISNULL(prpayrollprocess_tran.isvoid, 0) = 0 " & _
                                                    "AND ISNULL(prtnaleave_tran.isvoid, 0) = 0 " & _
                                                    "AND prtranhead_master.isnoncashbenefit = 1 " & _
                                                    "AND prtnaleave_tran.payperiodunkid IN (" & mstrPeriodIds & ") "

            If mintEmpId > 0 Then
                StrQ &= "                            AND prpayrollprocess_tran.employeeunkid = @employeeunkid "
            End If

            'S.SANDEEP [04 JUN 2015] -- START
            'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
            'If UserAccessLevel._AccessLevel.Length > 0 Then
            '    StrQ &= UserAccessLevel._AccessLevelFilterString
            'End If

            If xUACFiltrQry.Trim.Length > 0 Then
                StrQ &= " AND " & xUACFiltrQry
            End If

            'S.SANDEEP [04 JUN 2015] -- END

            'Sohail (23 Dec 2019) -- Start
            'AKF Enhancement # 0004231: Advanced Filter is required for NSSF Form and Tax Report.
            If mstrAdvanceFilter.Length > 0 AndAlso mstrAdvanceFilter.Contains("ADF.") = True Then
                StrQ &= "AND " & mstrAdvanceFilter
            End If
            'Sohail (23 Dec 2019) -- End

            'Sohail (09 Dec 2017) -- Start
            'SUPPORT 1722 - PACRA | STATUTORY REPORTS SHOWING LESSER GROSS PAY in 70.1.
            'StrQ &= "                      UNION ALL " & _
            '                              "SELECT    0 AS basicsalary " & _
            '                                      ", CAST(ISNULL(prpayrollprocess_tran.amount, 0) AS DECIMAL(36, " & decDecimalPlaces & ")) AS allowance " & _
            '                                      ", 0 AS noncashbenefit " & _
            '                                      ", 0 AS deduction " & _
            '                                      ", 0 AS taxpayable " & _
            '                                      ", 0 AS paye " & _
            '                                      ", prpayrollprocess_tran.employeeunkid AS empid " & _
            '                              "FROM      prpayrollprocess_tran " & _
            '                                        "JOIN prtnaleave_tran ON prpayrollprocess_tran.tnaleavetranunkid = prtnaleave_tran.tnaleavetranunkid " & _
            '                                        "JOIN hremployee_master ON prpayrollprocess_tran.employeeunkid = hremployee_master.employeeunkid " & _
            '                                        "LEFT JOIN prtranhead_master ON prtranhead_master.tranheadunkid = prpayrollprocess_tran.tranheadunkid "
            StrQ &= "                      UNION ALL " & _
                                          "SELECT    0 AS basicsalary " & _
                                                  ", CAST(ISNULL(prpayrollprocess_tran.amount, 0) AS DECIMAL(36, " & decDecimalPlaces & ")) AS allowance " & _
                                                  ", 0 AS noncashbenefit " & _
                                                  ", 0 AS deduction " & _
                                                  ", 0 AS taxpayable " & _
                                                  ", 0 AS paye " & _
                                                  ", prpayrollprocess_tran.employeeunkid AS empid " & _
                                          "FROM      prpayrollprocess_tran " & _
                                                    "JOIN prtnaleave_tran ON prpayrollprocess_tran.tnaleavetranunkid = prtnaleave_tran.tnaleavetranunkid " & _
                                                    "JOIN hremployee_master ON prpayrollprocess_tran.employeeunkid = hremployee_master.employeeunkid " & _
                                                    "LEFT JOIN prtranhead_master ON prtranhead_master.tranheadunkid = prpayrollprocess_tran.tranheadunkid " & _
                                                        " AND prtranhead_master.isvoid = 0 " & _
                                                    " LEFT JOIN cmclaim_process_tran ON prpayrollprocess_tran.crprocesstranunkid = cmclaim_process_tran.crprocesstranunkid " & _
                                                        " AND cmclaim_process_tran.isvoid = 0 " & _
                                                    " LEFT JOIN cmexpense_master ON cmclaim_process_tran.expenseunkid = cmexpense_master.expenseunkid " & _
                                                        " AND cmexpense_master.isactive = 1 " & _
                                                    "LEFT JOIN cmretire_process_tran ON cmretire_process_tran.crretirementprocessunkid = prpayrollprocess_tran.crretirementprocessunkid " & _
                                                    "  AND cmretire_process_tran.isvoid = 0 " & _
                                                    "LEFT JOIN cmexpense_master AS crretireexpense ON cmretire_process_tran.expenseunkid = crretireexpense.expenseunkid " & _
                                                    "  AND crretireexpense.isactive = 1 "
            'Sohail (09 Jun 2021) - [crretirementprocessunkid]
            'Sohail (09 Dec 2017) -- End

            'Sohail (23 Dec 2019) -- Start
            'AKF Enhancement # 0004231: Advanced Filter is required for NSSF Form and Tax Report.
            'StrQ &= mstrAnalysis_Join
            If mstrAdvanceFilter.Trim.Length > 0 AndAlso xAdvanceJoinQry.Trim.Length > 0 Then
                StrQ &= xAdvanceJoinQry
            End If
            'Sohail (23 Dec 2019) -- End

            'S.SANDEEP [04 JUN 2015] -- START
            'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
            If xUACQry.Trim.Length > 0 Then
                StrQ &= xUACQry
            End If
            'S.SANDEEP [04 JUN 2015] -- END

            'Sohail (09 Dec 2017) -- Start
            'SUPPORT 1722 - PACRA | STATUTORY REPORTS SHOWING LESSER GROSS PAY in 70.1.
            'StrQ &= "                      WHERE     ISNULL(prpayrollprocess_tran.isvoid, 0) = 0 " & _
            '                                        "AND ISNULL(prtnaleave_tran.isvoid, 0) = 0 " & _
            '                                        "AND prtranhead_master.trnheadtype_id = " & enTranHeadType.EarningForEmployees & " " & _
            '                                        "AND prtranhead_master.istaxable = 1 " & _
            '                                        "AND prtranhead_master.typeof_id NOT IN ( " & enTypeOf.Salary & " ) " & _
            '                                        "AND prtnaleave_tran.payperiodunkid IN (" & mstrPeriodIds & ") " & _
            '                                        "AND NOT ( prtranhead_master.isnoncashbenefit = 1 ) "
            StrQ &= "                      WHERE     prpayrollprocess_tran.isvoid = 0 " & _
                                                    "AND prtnaleave_tran.isvoid = 0 " & _
                                                    "AND prtnaleave_tran.payperiodunkid IN (" & mstrPeriodIds & ") "
            'Sohail (09 Dec 2017) -- End

            If mintOtherEarningTranId > 0 Then 'Basic Salary as Other Earning
                'Sohail (09 Dec 2017) -- Start
                'SUPPORT 1722 - PACRA | STATUTORY REPORTS SHOWING LESSER GROSS PAY in 70.1.
                'StrQ &= "AND NOT ( prtranhead_master.tranheadunkid = @OtherEarningTranId AND istaxable = 1 ) "
                StrQ &= "AND ((prtranhead_master.trnheadtype_id = " & CInt(enTranHeadType.EarningForEmployees) & " AND prtranhead_master.istaxable = 1 AND prtranhead_master.typeof_id NOT IN ( " & enTypeOf.Salary & " ) AND NOT ( prtranhead_master.isnoncashbenefit = 1 ) AND NOT ( prtranhead_master.tranheadunkid = @OtherEarningTranId AND istaxable = 1 ) ) OR (cmexpense_master.trnheadtype_id = " & CInt(enTranHeadType.EarningForEmployees) & ") OR (prpayrollprocess_tran.crretirementprocessunkid > 0 AND prpayrollprocess_tran.add_deduct IN (1)) ) "
                'Sohail (09 Dec 2017) -- End
            Else
                'Sohail (09 Dec 2017) -- Start
                'SUPPORT 1722 - PACRA | STATUTORY REPORTS SHOWING LESSER GROSS PAY in 70.1.
                'StrQ &= "AND NOT ( prtranhead_master.typeof_id = " & enTypeOf.Salary & " AND istaxable = 1 ) "
                StrQ &= "AND ((prtranhead_master.trnheadtype_id = " & CInt(enTranHeadType.EarningForEmployees) & " AND prtranhead_master.istaxable = 1 AND prtranhead_master.typeof_id NOT IN ( " & enTypeOf.Salary & " ) AND NOT ( prtranhead_master.isnoncashbenefit = 1 ) AND NOT ( prtranhead_master.typeof_id = " & enTypeOf.Salary & " AND istaxable = 1 ) ) OR (cmexpense_master.trnheadtype_id = " & CInt(enTranHeadType.EarningForEmployees) & ") OR (prpayrollprocess_tran.crretirementprocessunkid > 0 AND prpayrollprocess_tran.add_deduct IN (1)) ) "
                'Sohail (09 Dec 2017) -- End
            End If


            If mintEmpId > 0 Then
                StrQ &= "                            AND prpayrollprocess_tran.employeeunkid = @employeeunkid "
            End If


            'S.SANDEEP [04 JUN 2015] -- START
            'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS

            ''Pinkal (15-Oct-2014) -- Start
            ''Enhancement -  CHANGES FOR AKFTZ - PUTTING USER ACCESS LEVEL IN ALL STATUTORY REPORTS AS PER MR.RUTTA' S COMMENT

            'If UserAccessLevel._AccessLevel.Length > 0 Then
            '    StrQ &= UserAccessLevel._AccessLevelFilterString
            'End If
            ''Pinkal (15-Oct-2014) -- End

            If xUACFiltrQry.Trim.Length > 0 Then
                StrQ &= " AND " & xUACFiltrQry
            End If

            'S.SANDEEP [04 JUN 2015] -- END

            'Sohail (23 Dec 2019) -- Start
            'AKF Enhancement # 0004231: Advanced Filter is required for NSSF Form and Tax Report.
            If mstrAdvanceFilter.Length > 0 AndAlso mstrAdvanceFilter.Contains("ADF.") = True Then
                StrQ &= "AND " & mstrAdvanceFilter
            End If
            'Sohail (23 Dec 2019) -- End

            StrQ &= "                      UNION ALL " & _
                                          "SELECT    0 AS basicsalary " & _
                                                  ", 0 AS allowance " & _
                                                  ", 0 AS noncashbenefit " & _
                                                  ", CAST(ISNULL(prpayrollprocess_tran.amount, 0) AS DECIMAL(36, " & decDecimalPlaces & ")) AS deduction " & _
                                                  ", 0 AS taxpayable " & _
                                                  ", 0 AS paye " & _
                                                  ", prpayrollprocess_tran.employeeunkid AS empid " & _
                                          "FROM      prpayrollprocess_tran " & _
                                                    "JOIN prtnaleave_tran ON prpayrollprocess_tran.tnaleavetranunkid = prtnaleave_tran.tnaleavetranunkid " & _
                                                    "JOIN hremployee_master ON prpayrollprocess_tran.employeeunkid = hremployee_master.employeeunkid " & _
                                                    "LEFT JOIN prtranhead_master ON prtranhead_master.tranheadunkid = prpayrollprocess_tran.tranheadunkid "

            'Sohail (23 Dec 2019) -- Start
            'AKF Enhancement # 0004231: Advanced Filter is required for NSSF Form and Tax Report.
            'StrQ &= mstrAnalysis_Join
            If mstrAdvanceFilter.Trim.Length > 0 AndAlso xAdvanceJoinQry.Trim.Length > 0 Then
                StrQ &= xAdvanceJoinQry
            End If
            'Sohail (23 Dec 2019) -- End

            'S.SANDEEP [04 JUN 2015] -- START
            'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
            If xUACQry.Trim.Length > 0 Then
                StrQ &= xUACQry
            End If
            'S.SANDEEP [04 JUN 2015] -- END

            StrQ &= "                      WHERE     ISNULL(prpayrollprocess_tran.isvoid, 0) = 0 " & _
                                                    "AND ISNULL(prtnaleave_tran.isvoid, 0) = 0 " & _
                                                    "AND prtranhead_master.tranheadunkid IN (" & mstrStatutoryIds & ") " & _
                                                    "AND prtranhead_master.trnheadtype_id = " & enTranHeadType.EmployeesStatutoryDeductions & " " & _
                                                    "AND prtnaleave_tran.payperiodunkid IN (" & mstrPeriodIds & ") "

            If mintEmpId > 0 Then
                StrQ &= "                            AND prpayrollprocess_tran.employeeunkid = @employeeunkid "
            End If

            'S.SANDEEP [04 JUN 2015] -- START
            'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
            'If UserAccessLevel._AccessLevel.Length > 0 Then
            '    StrQ &= UserAccessLevel._AccessLevelFilterString
            'End If

            If xUACFiltrQry.Trim.Length > 0 Then
                StrQ &= " AND " & xUACFiltrQry
            End If

            'S.SANDEEP [04 JUN 2015] -- END

            'Sohail (23 Dec 2019) -- Start
            'AKF Enhancement # 0004231: Advanced Filter is required for NSSF Form and Tax Report.
            If mstrAdvanceFilter.Length > 0 AndAlso mstrAdvanceFilter.Contains("ADF.") = True Then
                StrQ &= "AND " & mstrAdvanceFilter
            End If
            'Sohail (23 Dec 2019) -- End

            StrQ &= "                      UNION ALL " & _
                                          "SELECT    0 AS basicsalary " & _
                                                  ", 0 AS allowance " & _
                                                  ", 0 AS noncashbenefit " & _
                                                  ", 0 AS deduction " & _
                                                  ", CAST(ISNULL(prpayrollprocess_tran.amount, 0) AS DECIMAL(36, " & decDecimalPlaces & ")) AS taxpayable " & _
                                                  ", 0 AS paye " & _
                                                  ", prpayrollprocess_tran.employeeunkid AS empid " & _
                                          "FROM      prpayrollprocess_tran " & _
                                                    "JOIN prtnaleave_tran ON prpayrollprocess_tran.tnaleavetranunkid = prtnaleave_tran.tnaleavetranunkid " & _
                                                    "JOIN hremployee_master ON prpayrollprocess_tran.employeeunkid = hremployee_master.employeeunkid " & _
                                                    "LEFT JOIN prtranhead_master ON prtranhead_master.tranheadunkid = prpayrollprocess_tran.tranheadunkid "

            'Sohail (23 Dec 2019) -- Start
            'AKF Enhancement # 0004231: Advanced Filter is required for NSSF Form and Tax Report.
            'StrQ &= mstrAnalysis_Join
            If mstrAdvanceFilter.Trim.Length > 0 AndAlso xAdvanceJoinQry.Trim.Length > 0 Then
                StrQ &= xAdvanceJoinQry
            End If
            'Sohail (23 Dec 2019) -- End

            'S.SANDEEP [04 JUN 2015] -- START
            'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
            If xUACQry.Trim.Length > 0 Then
                StrQ &= xUACQry
            End If
            'S.SANDEEP [04 JUN 2015] -- END

            StrQ &= "                      WHERE     ISNULL(prpayrollprocess_tran.isvoid, 0) = 0 " & _
                                                    "AND ISNULL(prtnaleave_tran.isvoid, 0) = 0 " & _
                                                    "AND prtranhead_master.typeof_id = " & enTypeOf.Taxes & " " & _
                                                    "AND prtnaleave_tran.payperiodunkid IN (" & mstrPeriodIds & ") "

            If mintEmpId > 0 Then
                StrQ &= "                            AND prpayrollprocess_tran.employeeunkid = @employeeunkid "
            End If

            'S.SANDEEP [04 JUN 2015] -- START
            'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
            'If UserAccessLevel._AccessLevel.Length > 0 Then
            '    StrQ &= UserAccessLevel._AccessLevelFilterString
            'End If

            If xUACFiltrQry.Trim.Length > 0 Then
                StrQ &= " AND " & xUACFiltrQry
            End If

            'S.SANDEEP [04 JUN 2015] -- END

            'Sohail (23 Dec 2019) -- Start
            'AKF Enhancement # 0004231: Advanced Filter is required for NSSF Form and Tax Report.
            If mstrAdvanceFilter.Length > 0 AndAlso mstrAdvanceFilter.Contains("ADF.") = True Then
                StrQ &= "AND " & mstrAdvanceFilter
            End If
            'Sohail (23 Dec 2019) -- End

            StrQ &= "                      ) AS PAYE " & _
                              "WHERE     1 = 1 " & _
                              "GROUP BY  empid " & _
                              "HAVING    SUM(ISNULL(PAYE.taxpayable, 0)) <> 0 " & _
                            ") AS A " & _
                            "LEFT JOIN hremployee_master ON hremployee_master.employeeunkid = A.empid " & _
                            "LEFT JOIN hremployee_meminfo_tran ON hremployee_master.employeeunkid = hremployee_meminfo_tran.employeeunkid "

            If mintMembershipId > 0 Then
                StrQ &= "AND hremployee_meminfo_tran.membershipunkid = @membershipunkid "
            End If

            StrQ &= "LEFT JOIN hrmembership_master ON hremployee_meminfo_tran.membershipunkid = hrmembership_master.membershipunkid " & _
                    "WHERE   1 = 1 " & _
                    "AND ISNULL(hremployee_meminfo_tran.isdeleted, 0) = 0 "


            Call FilterTitleAndFilterQuery()
            StrQ &= Me._FilterQuery

            If ConfigParameter._Object._FirstNamethenSurname = True Then
                StrQ &= " ORDER BY  ISNULL(hremployee_master.firstname, '') + ' ' + ISNULL(hremployee_master.othername, '') + ' ' + ISNULL(hremployee_master.surname, '') "
            Else
                StrQ &= " ORDER BY  ISNULL(hremployee_master.surname, '') + ' ' + ISNULL(hremployee_master.firstname, '') + ' ' + ISNULL(hremployee_master.othername, '')  "
            End If

            If mintEmpId > 0 Then
                objDataOperation.AddParameter("@employeeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintEmpId)
            End If

            dsList = objDataOperation.ExecQuery(StrQ, "DataTable")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & " : " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            Dim dtTemp() As DataRow = dsList.Tables(0).Select("basicpay = 0 AND noncashbenefit = 0 AND allowancebenefit = 0 AND Grosspay = 0 AND deduction = 0 AND taxable = 0 AND taxpaid = 0")

            If dtTemp.Length > 0 Then
                For i As Integer = 0 To dtTemp.Length - 1
                    dsList.Tables(0).Rows.Remove(dtTemp(i))
                Next
                dsList.Tables(0).AcceptChanges()
            End If


            rpt_Data = New ArutiReport.Designer.dsArutiReport

            Dim rpt_Row As DataRow = Nothing
            Dim intCnt As Integer = 1
            Dim intPrevEmp As Integer = 0

            Dim decTotBasic, decTotNcb, decTotAllowance, decTotGross, decTotDeduction, decTotTaxable, decTotTaxDue As Decimal
            decTotBasic = 0 : decTotNcb = 0 : decTotAllowance = 0 : decTotGross = 0 : decTotDeduction = 0 : decTotTaxable = 0 : decTotTaxDue = 0

            For Each dtRow As DataRow In dsList.Tables("DataTable").Rows

                If intPrevEmp = CInt(dtRow.Item("empid")) Then '*** To prevent Duplicate entries if employee has more than one Membership Assigned
                    intPrevEmp = CInt(dtRow.Item("empid"))
                    Continue For
                End If


                rpt_Row = rpt_Data.Tables("ArutiTable").NewRow

                rpt_Row.Item("Column1") = intCnt
                rpt_Row.Item("Column2") = dtRow.Item("empname")
                rpt_Row.Item("Column3") = dtRow.Item("empcode")
                'Sohail (14 Jun 2013) -- Start
                'TRA - ENHANCEMENT
                'rpt_Row.Item("Column4") = dtRow.Item("membershipname")
                rpt_Row.Item("Column4") = dtRow.Item("membershipno")
                'Sohail (14 Jun 2013) -- End
                'rpt_Row.Item("Column5") = dtRow.Item("city")

                rpt_Row.Item("Column6") = Format(CDec(dtRow.Item("basicpay")), GUI.fmtCurrency)
                rpt_Row.Item("Column7") = Format(CDec(dtRow.Item("noncashbenefit")), GUI.fmtCurrency)
                rpt_Row.Item("Column8") = Format(CDec(dtRow.Item("allowancebenefit")), GUI.fmtCurrency)
                rpt_Row.Item("Column9") = Format(CDec(dtRow.Item("Grosspay")), GUI.fmtCurrency)
                rpt_Row.Item("Column10") = Format(CDec(dtRow.Item("deduction")), GUI.fmtCurrency)
                rpt_Row.Item("Column11") = Format(CDec(dtRow.Item("taxable")), GUI.fmtCurrency)
                rpt_Row.Item("Column12") = Format(CDec(dtRow.Item("taxpaid")), GUI.fmtCurrency)

                decTotBasic = decTotBasic + CDec(dtRow.Item("basicpay"))
                decTotNcb = decTotNcb + CDec(dtRow.Item("noncashbenefit"))
                decTotAllowance = decTotAllowance + CDec(dtRow.Item("allowancebenefit"))
                decTotGross = decTotGross + CDec(dtRow.Item("Grosspay"))
                decTotDeduction = decTotDeduction + CDec(dtRow.Item("deduction"))
                decTotTaxable = decTotTaxable + CDec(dtRow.Item("taxable"))
                decTotTaxDue = decTotTaxDue + CDec(dtRow.Item("taxpaid"))

                intCnt += 1
                intPrevEmp = CInt(dtRow.Item("empid"))
                rpt_Data.Tables("ArutiTable").Rows.Add(rpt_Row)
            Next

            objRpt = New ArutiReport.Designer.rptTaxReport

            objRpt.SetDataSource(rpt_Data)

            ReportFunction.TextChange(objRpt.Subreports("rptEmployerDetail"), "lblTRA", Language.getMessage(mstrModuleName, 37, "TANZANIA REVENUE AUTHORITY")) 'Start From 37
            ReportFunction.TextChange(objRpt.Subreports("rptEmployerDetail"), "lblPAYE", Language.getMessage(mstrModuleName, 38, "P.A.Y.E."))
            ReportFunction.TextChange(objRpt.Subreports("rptEmployerDetail"), "lblReportName", Language.getMessage(mstrModuleName, 39, "STATEMENT AND PAYMENT OF TAX WITHHELD"))
            ReportFunction.TextChange(objRpt.Subreports("rptEmployerDetail"), "lblYear", Language.getMessage(mstrModuleName, 40, "YEAR:"))
            ReportFunction.TextChange(objRpt.Subreports("rptEmployerDetail"), "txtYear", FinancialYear._Object._FinancialYear_Name)
            ReportFunction.TextChange(objRpt.Subreports("rptEmployerDetail"), "lblTIN", Language.getMessage(mstrModuleName, 41, "TIN:"))
            ReportFunction.TextChange(objRpt.Subreports("rptEmployerDetail"), "txtTIN", Company._Object._Tinno)
            ReportFunction.TextChange(objRpt.Subreports("rptEmployerDetail"), "lblPeriod", Language.getMessage(mstrModuleName, 42, "Period: (Please tick the appropriate box) "))
            ReportFunction.TextChange(objRpt.Subreports("rptEmployerDetail"), "lblPeriod1", Language.getMessage(mstrModuleName, 43, "From 1 January to 30 June "))
            ReportFunction.TextChange(objRpt.Subreports("rptEmployerDetail"), "lblPeriod2", Language.getMessage(mstrModuleName, 44, "From 1 January to 31 December "))

            If mstrPeriodNames.Trim.Length > 0 Then
                ReportFunction.TextChange(objRpt.Subreports("rptEmployerDetail"), "txtPeriodNames", Language.getMessage(mstrModuleName, 81, "Periods : (") & mstrPeriodNames & Language.getMessage(mstrModuleName, 82, ")"))
            Else
                ReportFunction.TextChange(objRpt.Subreports("rptEmployerDetail"), "txtPeriodNames", "")
            End If


            ReportFunction.TextChange(objRpt.Subreports("rptEmployerDetail"), "lblEmployer", Language.getMessage(mstrModuleName, 45, "Name of Employer:"))
            ReportFunction.TextChange(objRpt.Subreports("rptEmployerDetail"), "txtEmployer", Company._Object._Name)

            ReportFunction.TextChange(objRpt.Subreports("rptEmployerDetail"), "lblPostalAddress", Language.getMessage(mstrModuleName, 46, "Postal Address:"))
            ReportFunction.TextChange(objRpt.Subreports("rptEmployerDetail"), "lblPoBox", Language.getMessage(mstrModuleName, 47, "P. O. Box"))
            Dim objZip As New clszipcode_master
            objZip._Zipcodeunkid = Company._Object._Postalunkid
            ReportFunction.TextChange(objRpt.Subreports("rptEmployerDetail"), "txtPoBox", objZip._Zipcode_No)
            objZip = Nothing
            ReportFunction.TextChange(objRpt.Subreports("rptEmployerDetail"), "lblPostalCity", Language.getMessage(mstrModuleName, 48, "Postal City"))
            Dim objCity As New clscity_master
            objCity._Cityunkid = Company._Object._Cityunkid
            ReportFunction.TextChange(objRpt.Subreports("rptEmployerDetail"), "txtPostalCity", objCity._Name)
            objCity = Nothing
            ReportFunction.TextChange(objRpt.Subreports("rptEmployerDetail"), "lblContactNumbers", Language.getMessage(mstrModuleName, 49, "Contact Numbers:"))
            ReportFunction.TextChange(objRpt.Subreports("rptEmployerDetail"), "lblPhone", Language.getMessage(mstrModuleName, 50, "Phone number"))
            ReportFunction.TextChange(objRpt.Subreports("rptEmployerDetail"), "txtPhone1", Company._Object._Phone1)
            ReportFunction.TextChange(objRpt.Subreports("rptEmployerDetail"), "lblSecondPhone", Language.getMessage(mstrModuleName, 51, "Second Phone"))
            ReportFunction.TextChange(objRpt.Subreports("rptEmployerDetail"), "txtSecondPhone", Company._Object._Phone2)
            ReportFunction.TextChange(objRpt.Subreports("rptEmployerDetail"), "lblThirdPhone", Language.getMessage(mstrModuleName, 52, "Third Phone"))
            ReportFunction.TextChange(objRpt.Subreports("rptEmployerDetail"), "txtThirdphone", Company._Object._Phone3)
            ReportFunction.TextChange(objRpt.Subreports("rptEmployerDetail"), "lblFax", Language.getMessage(mstrModuleName, 53, " Fax number"))
            ReportFunction.TextChange(objRpt.Subreports("rptEmployerDetail"), "txtFax", Company._Object._Fax)
            ReportFunction.TextChange(objRpt.Subreports("rptEmployerDetail"), "lblEmail", Language.getMessage(mstrModuleName, 54, "E-mail address:"))
            ReportFunction.TextChange(objRpt.Subreports("rptEmployerDetail"), "txtEmail", Company._Object._Email)
            ReportFunction.TextChange(objRpt.Subreports("rptEmployerDetail"), "lblPhysical", Language.getMessage(mstrModuleName, 55, "Physical Address:"))
            ReportFunction.TextChange(objRpt.Subreports("rptEmployerDetail"), "lblPlot", Language.getMessage(mstrModuleName, 56, "Plot Number"))
            ReportFunction.TextChange(objRpt.Subreports("rptEmployerDetail"), "txtPlot", Company._Object._Reg1_Value)
            ReportFunction.TextChange(objRpt.Subreports("rptEmployerDetail"), "lblBlock", Language.getMessage(mstrModuleName, 57, "Block Number"))
            ReportFunction.TextChange(objRpt.Subreports("rptEmployerDetail"), "txtBlock", Company._Object._Reg2_Value)
            ReportFunction.TextChange(objRpt.Subreports("rptEmployerDetail"), "lblStreet", Language.getMessage(mstrModuleName, 58, "Street/Location"))
            ReportFunction.TextChange(objRpt.Subreports("rptEmployerDetail"), "txtStreet", Company._Object._Address1 & " " & Company._Object._Address2)
            ReportFunction.TextChange(objRpt.Subreports("rptEmployerDetail"), "lblBranch", Language.getMessage(mstrModuleName, 59, "Name of Branch"))
            Dim objBranch As New clsbankbranch_master
            objBranch._Branchunkid = Company._Object._Branchunkid
            ReportFunction.TextChange(objRpt.Subreports("rptEmployerDetail"), "txtBranch", objBranch._Branchname)
            objBranch = Nothing
            ReportFunction.TextChange(objRpt.Subreports("rptEmployerDetail"), "lblPaymentStatement", Language.getMessage(mstrModuleName, 60, "ITX215.01.E -  P.A.Y.E.-Statement"))


            ReportFunction.TextChange(objRpt, "txtReportName", Language.getMessage(mstrModuleName, 61, "TAX Report"))
            ReportFunction.TextChange(objRpt, "txtCompanyName", Company._Object._Name)
            ReportFunction.TextChange(objRpt, "txtAddress", Company._Object._Address1 & " " & Company._Object._Address2 & " " & Company._Object._City_Name & " " & Company._Object._State_Name & " " & Company._Object._Post_Code_No)
            ReportFunction.TextChange(objRpt, "txtCountry", Company._Object._Country_Name)
            ReportFunction.TextChange(objRpt, "lblDate", Language.getMessage(mstrModuleName, 62, "Date :"))
            ReportFunction.TextChange(objRpt, "txtDate", ConfigParameter._Object._CurrentDateAndTime.ToShortDateString)
            ReportFunction.TextChange(objRpt, "lblReturnPeriods", Language.getMessage(mstrModuleName, 68, "Return Periods :"))
            ReportFunction.TextChange(objRpt, "txtReturnPeriods", mstrPeriodNames)
            ReportFunction.TextChange(objRpt, "lblCurrency", Language.getMessage(mstrModuleName, 77, "Currency :"))
            ReportFunction.TextChange(objRpt, "txtCurrency", strBaseCurrencySign)
            ReportFunction.TextChange(objRpt, "lblVRN", Language.getMessage(mstrModuleName, 78, "VRN :"))
            ReportFunction.TextChange(objRpt, "txtVRN", Company._Object._Registerdno)


            ReportFunction.TextChange(objRpt, "lblTIN", Language.getMessage(mstrModuleName, 63, "TIN :"))
            ReportFunction.TextChange(objRpt, "txtTIN", Company._Object._Tinno)
            ReportFunction.TextChange(objRpt, "txtSrNo", Language.getMessage(mstrModuleName, 64, "S/No."))
            ReportFunction.TextChange(objRpt, "txtEmpName", Language.getMessage(mstrModuleName, 65, "Name Of Employee"))
            'Sohail (29 Mar 2014) -- Start
            'ENHANCEMENT - Show Payroll No. and Employee TIN No on Tax Report.
            'ReportFunction.TextChange(objRpt, "txtPayroll", Language.getMessage(mstrModuleName, 66, "Payroll No."))
            'ReportFunction.TextChange(objRpt, "txtTINNo", Language.getMessage(mstrModuleName, 67, "Employee TIN No"))
            If mblnShowPayrollNo = True Then
                ReportFunction.TextChange(objRpt, "txtPayroll", Language.getMessage(mstrModuleName, 66, "Payroll No."))
            Else
                ReportFunction.TextChange(objRpt, "txtPayroll", "")
                ReportFunction.EnableSuppress(objRpt, "txtPayroll", True)
                ReportFunction.EnableSuppress(objRpt, "Column31", True)
            End If
            If mblnShowEmployeeTINNo = True Then
                ReportFunction.TextChange(objRpt, "txtTINNo", Language.getMessage(mstrModuleName, 67, "Employee TIN No"))
            Else
                ReportFunction.TextChange(objRpt, "txtTINNo", "")
                ReportFunction.EnableSuppress(objRpt, "txtTINNo", True)
                ReportFunction.EnableSuppress(objRpt, "Column41", True)
            End If
            If mblnShowPayrollNo = False AndAlso mblnShowEmployeeTINNo = False Then
                objRpt.ReportDefinition.ReportObjects("txtEmpName").Left = objRpt.ReportDefinition.ReportObjects("txtPayroll").Left
                objRpt.ReportDefinition.ReportObjects("txtEmpName").Width = objRpt.ReportDefinition.ReportObjects("txtEmpName").Width + objRpt.ReportDefinition.ReportObjects("txtPayroll").Width + objRpt.ReportDefinition.ReportObjects("txtTINNo").Width
                objRpt.ReportDefinition.ReportObjects("Column21").Left = objRpt.ReportDefinition.ReportObjects("Column31").Left
                objRpt.ReportDefinition.ReportObjects("Column21").Width = objRpt.ReportDefinition.ReportObjects("Column21").Width + objRpt.ReportDefinition.ReportObjects("Column31").Width + objRpt.ReportDefinition.ReportObjects("Column41").Width
            ElseIf mblnShowEmployeeTINNo = False Then
                objRpt.ReportDefinition.ReportObjects("txtEmpName").Width = objRpt.ReportDefinition.ReportObjects("txtEmpName").Width + objRpt.ReportDefinition.ReportObjects("txtTINNo").Width
                objRpt.ReportDefinition.ReportObjects("Column21").Width = objRpt.ReportDefinition.ReportObjects("Column21").Width + objRpt.ReportDefinition.ReportObjects("Column41").Width
            ElseIf mblnShowPayrollNo = False Then
                objRpt.ReportDefinition.ReportObjects("txtEmpName").Left = objRpt.ReportDefinition.ReportObjects("txtPayroll").Left
                objRpt.ReportDefinition.ReportObjects("txtEmpName").Width = objRpt.ReportDefinition.ReportObjects("txtEmpName").Width + objRpt.ReportDefinition.ReportObjects("txtPayroll").Width
                objRpt.ReportDefinition.ReportObjects("Column21").Left = objRpt.ReportDefinition.ReportObjects("Column31").Left
                objRpt.ReportDefinition.ReportObjects("Column21").Width = objRpt.ReportDefinition.ReportObjects("Column21").Width + objRpt.ReportDefinition.ReportObjects("Column31").Width
            End If
            'objRpt.ReportDefinition.ReportObjects("txtReportMode").Left = objRpt.ReportDefinition.ReportObjects("txtCompanyName").Left
            'Sohail (29 Mar 2014) -- End
            ReportFunction.TextChange(objRpt, "txtPay", Language.getMessage(mstrModuleName, 69, "Basic Salary"))
            ReportFunction.TextChange(objRpt, "txtNonCash", Language.getMessage(mstrModuleName, 70, "Non Cash Benefit"))
            ReportFunction.TextChange(objRpt, "txtAllBenefit", Language.getMessage(mstrModuleName, 71, "Other Taxable Allowances"))
            ReportFunction.TextChange(objRpt, "txtGrossPay", Language.getMessage(mstrModuleName, 72, "Gross Pay"))
            ReportFunction.TextChange(objRpt, "txtDeduction", Language.getMessage(mstrModuleName, 73, "Deductions"))
            ReportFunction.TextChange(objRpt, "txtTaxable", Language.getMessage(mstrModuleName, 74, "Taxable Pay"))
            ReportFunction.TextChange(objRpt, "txtTaxDue", Language.getMessage(mstrModuleName, 75, "Tax Paid"))
            ReportFunction.TextChange(objRpt, "txtGrandTotal", Language.getMessage(mstrModuleName, 76, "Total"))


            Call ReportFunction.TextChange(objRpt, "txtTotal6", Format(decTotBasic, GUI.fmtCurrency))
            Call ReportFunction.TextChange(objRpt, "txtTotal7", Format(decTotNcb, GUI.fmtCurrency))
            Call ReportFunction.TextChange(objRpt, "txtTotal8", Format(decTotAllowance, GUI.fmtCurrency))
            Call ReportFunction.TextChange(objRpt, "txtTotal9", Format(decTotGross, GUI.fmtCurrency))
            Call ReportFunction.TextChange(objRpt, "txtTotal10", Format(decTotDeduction, GUI.fmtCurrency))
            Call ReportFunction.TextChange(objRpt, "txtTotal11", Format(decTotTaxable, GUI.fmtCurrency))
            Call ReportFunction.TextChange(objRpt, "txtTotal12", Format(decTotTaxDue, GUI.fmtCurrency))

            Return objRpt

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: Generate_DetailReport; Module Name: " & mstrModuleName)
            Return Nothing
        End Try
    End Function
#End Region

    'Sohail (23 Apr 2015) -- Start
    'Enhancement - Tax Report columns calculattion should be same PaYE Withheld P9 Report. (Email From: Anatory Rutta, Subject: TAx Report, Sent: Thursday, April 23, 2015 12:58 PM)
#Region " Report b4 2015-04-23 Tax Report columns calculattion should be same PaYE Withheld P9 Report "
    'Private Function Generate_DetailReport() As CrystalDecisions.CrystalReports.Engine.ReportClass
    '    Dim objRpt As CrystalDecisions.CrystalReports.Engine.ReportClass = Nothing
    '    Dim StrQ As String = ""
    '    Dim dsList As New DataSet
    '    Dim exForce As Exception
    '    Dim rpt_Data As ArutiReport.Designer.dsArutiReport
    '    Dim strBaseCurrencySign As String

    '    Try
    '        objDataOperation = New clsDataOperation

    '        Dim objExchangeRate As New clsExchangeRate
    '        Dim decDecimalPlaces As Decimal = 0
    '        objExchangeRate._ExchangeRateunkid = ConfigParameter._Object._Base_CurrencyId
    '        decDecimalPlaces = objExchangeRate._Digits_After_Decimal
    '        strBaseCurrencySign = objExchangeRate._Currency_Sign

    '        StrQ = "SELECT   basicpay AS basicpay " & _
    '                      ", noncashbenefit AS noncashbenefit " & _
    '                      ", CASE WHEN allowancebenefit <= 0 THEN 0 " & _
    '                             "WHEN ( basicpay + noncashbenefit ) > allowancebenefit " & _
    '                             "THEN ( basicpay + noncashbenefit ) - allowancebenefit " & _
    '                             "ELSE allowancebenefit - ( basicpay + noncashbenefit ) " & _
    '                        "END AS allowancebenefit " & _
    '                      ", basicpay + noncashbenefit + ( CASE WHEN allowancebenefit <= 0 THEN 0 " & _
    '                                                    "WHEN ( basicpay + noncashbenefit ) > allowancebenefit " & _
    '                                                    "THEN ( basicpay + noncashbenefit ) - allowancebenefit " & _
    '                                                    "ELSE allowancebenefit - ( basicpay + noncashbenefit ) " & _
    '                                               "END ) AS Grosspay " & _
    '                      ", ( basicpay + noncashbenefit + ( CASE WHEN allowancebenefit <= 0 THEN 0 " & _
    '                                                      "WHEN ( basicpay + noncashbenefit ) > allowancebenefit " & _
    '                                                      "THEN ( basicpay + noncashbenefit ) - allowancebenefit " & _
    '                                                      "ELSE allowancebenefit - ( basicpay + noncashbenefit ) " & _
    '                                                 "END ) ) - deduction AS taxable " & _
    '                      ", deduction AS deduction " & _
    '                      ", taxpayable AS taxpayable " & _
    '                      ", taxpaid AS taxpaid " & _
    '                      ", empid AS empid " & _
    '                      ", hremployee_master.employeecode AS empcode "

    '        'Anjan [25 Mar 2014] -- Start
    '        'ENHANCEMENT : Requested by Rutta
    '        If ConfigParameter._Object._FirstNamethenSurname = True Then
    '            StrQ &= ", ISNULL(firstname, '') + ' ' + ISNULL(othername, '') + ' ' + ISNULL(surname, '') AS empname "
    '        Else
    '            StrQ &= ", ISNULL(surname, '') + ' ' + ISNULL(firstname, '') + ' ' + ISNULL(othername, '') AS empname "
    '        End If


    '        'Anjan [25 Mar 2014 ] -- End



    '        StrQ &= ", ISNULL(hremployee_meminfo_tran.membershipno, '') AS membershipno " & _
    '                "FROM    ( SELECT    SUM(ISNULL(PAYE.basicsalary, 0)) AS basicpay " & _
    '                                  ", SUM(ISNULL(PAYE.allowance, 0)) AS allowancebenefit " & _
    '                                  ", SUM(ISNULL(PAYE.noncashbenefit, 0)) AS noncashbenefit " & _
    '                                  ", SUM(ISNULL(PAYE.deduction, 0)) AS deduction " & _
    '                                  ", SUM(ISNULL(PAYE.taxpayable, 0)) AS taxpayable " & _
    '                                  ", SUM(ISNULL(PAYE.paye, 0)) AS taxpaid " & _
    '                                  ", paye.empid AS empid " & _
    '                          "FROM      ( SELECT    CAST(ISNULL(prpayrollprocess_tran.amount, 0) AS DECIMAL(36, " & decDecimalPlaces & ")) AS basicsalary " & _
    '                                              ", 0 AS allowance " & _
    '                                              ", 0 AS noncashbenefit " & _
    '                                              ", 0 AS deduction " & _
    '                                              ", 0 AS taxpayable " & _
    '                                              ", 0 AS paye " & _
    '                                              ", prpayrollprocess_tran.employeeunkid AS empid " & _
    '                                      "FROM      prpayrollprocess_tran " & _
    '                                                "JOIN prtnaleave_tran ON prpayrollprocess_tran.tnaleavetranunkid = prtnaleave_tran.tnaleavetranunkid " & _
    '                                                "JOIN hremployee_master ON prpayrollprocess_tran.employeeunkid = hremployee_master.employeeunkid " & _
    '                                                "LEFT JOIN prtranhead_master ON prtranhead_master.tranheadunkid = prpayrollprocess_tran.tranheadunkid "
    '        'Sohail (14 Jun 2013) -- ", ISNULL(hrmembership_master.membershipname, '') AS membershipname " & _

    '        StrQ &= mstrAnalysis_Join

    '        'Sohail (28 Aug 2013) -- Start
    '        'TRA - ENHANCEMENT
    '        'StrQ &= "                      WHERE     ISNULL(prpayrollprocess_tran.isvoid, 0) = 0 " & _
    '        '                                        "AND ISNULL(prtnaleave_tran.isvoid, 0) = 0 " & _
    '        '                                        "AND prtranhead_master.typeof_id = " & enTypeOf.Salary & " " & _
    '        '                                        "AND prtnaleave_tran.payperiodunkid IN (" & mstrPeriodIds & ") "
    '        StrQ &= "                      WHERE     ISNULL(prpayrollprocess_tran.isvoid, 0) = 0 " & _
    '                                                "AND ISNULL(prtnaleave_tran.isvoid, 0) = 0 " & _
    '                                                "AND prpayrollprocess_tran.tranheadunkid > 0 " & _
    '                                                "AND prtnaleave_tran.payperiodunkid IN (" & mstrPeriodIds & ") "

    '        If mintOtherEarningTranId > 0 Then 'Basic Salary as Other Earning
    '            StrQ &= "AND prpayrollprocess_tran.tranheadunkid = @OtherEarningTranId  "
    '        Else
    '            StrQ &= "AND prtranhead_master.typeof_id = " & enTypeOf.Salary & "  "
    '        End If
    '        'Sohail (28 Aug 2013) -- End

    '        If mintEmpId > 0 Then
    '            StrQ &= "                            AND prpayrollprocess_tran.employeeunkid = @employeeunkid "
    '        End If

    '        'Pinkal (15-Oct-2014) -- Start
    '        'Enhancement -  CHANGES FOR AKFTZ - PUTTING USER ACCESS LEVEL IN ALL STATUTORY REPORTS AS PER MR.RUTTA' S COMMENT

    '        If UserAccessLevel._AccessLevel.Length > 0 Then
    '            StrQ &= UserAccessLevel._AccessLevelFilterString
    '        End If
    '        'Pinkal (15-Oct-2014) -- End

    '        StrQ &= "                      UNION ALL " & _
    '                                      "SELECT    0 AS basicsalary " & _
    '                                              ", 0 AS allowance " & _
    '                                              ", CAST(ISNULL(prpayrollprocess_tran.amount, 0) AS DECIMAL(36, " & decDecimalPlaces & ")) AS noncashbenefit " & _
    '                                              ", 0 AS deduction " & _
    '                                              ", 0 AS taxpayable " & _
    '                                              ", 0 AS paye " & _
    '                                              ", prpayrollprocess_tran.employeeunkid AS empid " & _
    '                                      "FROM      prpayrollprocess_tran " & _
    '                                                "JOIN prtnaleave_tran ON prpayrollprocess_tran.tnaleavetranunkid = prtnaleave_tran.tnaleavetranunkid " & _
    '                                                "JOIN hremployee_master ON prpayrollprocess_tran.employeeunkid = hremployee_master.employeeunkid " & _
    '                                                "LEFT JOIN prtranhead_master ON prtranhead_master.tranheadunkid = prpayrollprocess_tran.tranheadunkid "

    '        StrQ &= mstrAnalysis_Join

    '        StrQ &= "                      WHERE     ISNULL(prpayrollprocess_tran.isvoid, 0) = 0 " & _
    '                                                "AND ISNULL(prtnaleave_tran.isvoid, 0) = 0 " & _
    '                                                "AND prtranhead_master.isnoncashbenefit = 1 " & _
    '                                                "AND prtnaleave_tran.payperiodunkid IN (" & mstrPeriodIds & ") "
    '        'Sohail (14 Jun 2013) -- ["AND prtranhead_master.istaxable = 1 " & _] (Now isnoncashbenefit is under informational head)

    '        If mintEmpId > 0 Then
    '            StrQ &= "                            AND prpayrollprocess_tran.employeeunkid = @employeeunkid "
    '        End If

    '        'Pinkal (15-Oct-2014) -- Start
    '        'Enhancement -  CHANGES FOR AKFTZ - PUTTING USER ACCESS LEVEL IN ALL STATUTORY REPORTS AS PER MR.RUTTA' S COMMENT

    '        If UserAccessLevel._AccessLevel.Length > 0 Then
    '            StrQ &= UserAccessLevel._AccessLevelFilterString
    '        End If
    '        'Pinkal (15-Oct-2014) -- End

    '        StrQ &= "                      UNION ALL " & _
    '                                      "SELECT    0 AS basicsalary " & _
    '                                              ", CAST(ISNULL(prpayrollprocess_tran.amount, 0) AS DECIMAL(36, " & decDecimalPlaces & ")) AS allowance " & _
    '                                              ", 0 AS noncashbenefit " & _
    '                                              ", 0 AS deduction " & _
    '                                              ", 0 AS taxpayable " & _
    '                                              ", 0 AS paye " & _
    '                                              ", prpayrollprocess_tran.employeeunkid AS empid " & _
    '                                      "FROM      prpayrollprocess_tran " & _
    '                                                "JOIN prtnaleave_tran ON prpayrollprocess_tran.tnaleavetranunkid = prtnaleave_tran.tnaleavetranunkid " & _
    '                                                "JOIN hremployee_master ON prpayrollprocess_tran.employeeunkid = hremployee_master.employeeunkid " & _
    '                                                "LEFT JOIN prtranhead_master ON prtranhead_master.tranheadunkid = prpayrollprocess_tran.tranheadunkid "

    '        StrQ &= mstrAnalysis_Join

    '        StrQ &= "                      WHERE     ISNULL(prpayrollprocess_tran.isvoid, 0) = 0 " & _
    '                                                "AND ISNULL(prtnaleave_tran.isvoid, 0) = 0 " & _
    '                                                "AND prtranhead_master.trnheadtype_id = " & enTranHeadType.EarningForEmployees & " " & _
    '                                                "AND prtranhead_master.istaxable = 1 " & _
    '                                                "AND prtranhead_master.typeof_id NOT IN ( " & enTypeOf.Salary & " ) " & _
    '                                                "AND prtnaleave_tran.payperiodunkid IN (" & mstrPeriodIds & ") "
    '        'Sohail (14 Jun 2013) -- [ "AND prtranhead_master.isnoncashbenefit = 0 " & _] (Now isnoncashbenefit is under informational head)

    '        If mintEmpId > 0 Then
    '            StrQ &= "                            AND prpayrollprocess_tran.employeeunkid = @employeeunkid "
    '        End If

    '        'Pinkal (15-Oct-2014) -- Start
    '        'Enhancement -  CHANGES FOR AKFTZ - PUTTING USER ACCESS LEVEL IN ALL STATUTORY REPORTS AS PER MR.RUTTA' S COMMENT

    '        If UserAccessLevel._AccessLevel.Length > 0 Then
    '            StrQ &= UserAccessLevel._AccessLevelFilterString
    '        End If
    '        'Pinkal (15-Oct-2014) -- End

    '        StrQ &= "                      UNION ALL " & _
    '                                      "SELECT    0 AS basicsalary " & _
    '                                              ", 0 AS allowance " & _
    '                                              ", 0 AS noncashbenefit " & _
    '                                              ", CAST(ISNULL(prpayrollprocess_tran.amount, 0) AS DECIMAL(36, " & decDecimalPlaces & ")) AS deduction " & _
    '                                              ", 0 AS taxpayable " & _
    '                                              ", 0 AS paye " & _
    '                                              ", prpayrollprocess_tran.employeeunkid AS empid " & _
    '                                      "FROM      prpayrollprocess_tran " & _
    '                                                "JOIN prtnaleave_tran ON prpayrollprocess_tran.tnaleavetranunkid = prtnaleave_tran.tnaleavetranunkid " & _
    '                                                "JOIN hremployee_master ON prpayrollprocess_tran.employeeunkid = hremployee_master.employeeunkid " & _
    '                                                "LEFT JOIN prtranhead_master ON prtranhead_master.tranheadunkid = prpayrollprocess_tran.tranheadunkid "

    '        StrQ &= mstrAnalysis_Join

    '        StrQ &= "                      WHERE     ISNULL(prpayrollprocess_tran.isvoid, 0) = 0 " & _
    '                                                "AND ISNULL(prtnaleave_tran.isvoid, 0) = 0 " & _
    '                                                "AND prtranhead_master.tranheadunkid IN (" & mstrStatutoryIds & ") " & _
    '                                                "AND prtranhead_master.trnheadtype_id = " & enTranHeadType.EmployeesStatutoryDeductions & " " & _
    '                                                "AND prtnaleave_tran.payperiodunkid IN (" & mstrPeriodIds & ") "

    '        If mintEmpId > 0 Then
    '            StrQ &= "                            AND prpayrollprocess_tran.employeeunkid = @employeeunkid "
    '        End If

    '        'Pinkal (15-Oct-2014) -- Start
    '        'Enhancement -  CHANGES FOR AKFTZ - PUTTING USER ACCESS LEVEL IN ALL STATUTORY REPORTS AS PER MR.RUTTA' S COMMENT

    '        If UserAccessLevel._AccessLevel.Length > 0 Then
    '            StrQ &= UserAccessLevel._AccessLevelFilterString
    '        End If
    '        'Pinkal (15-Oct-2014) -- End

    '        StrQ &= "                      UNION ALL " & _
    '                                      "SELECT    0 AS basicsalary " & _
    '                                              ", 0 AS allowance " & _
    '                                              ", 0 AS noncashbenefit " & _
    '                                              ", 0 AS deduction " & _
    '                                              ", CAST(ISNULL(prpayrollprocess_tran.amount, 0) AS DECIMAL(36, " & decDecimalPlaces & ")) AS taxpayable " & _
    '                                              ", 0 AS paye " & _
    '                                              ", prpayrollprocess_tran.employeeunkid AS empid " & _
    '                                      "FROM      prpayrollprocess_tran " & _
    '                                                "JOIN prtnaleave_tran ON prpayrollprocess_tran.tnaleavetranunkid = prtnaleave_tran.tnaleavetranunkid " & _
    '                                                "JOIN hremployee_master ON prpayrollprocess_tran.employeeunkid = hremployee_master.employeeunkid " & _
    '                                                "LEFT JOIN prtranhead_master ON prtranhead_master.tranheadunkid = prpayrollprocess_tran.tranheadunkid "

    '        StrQ &= mstrAnalysis_Join

    '        StrQ &= "                      WHERE     ISNULL(prpayrollprocess_tran.isvoid, 0) = 0 " & _
    '                                                "AND ISNULL(prtnaleave_tran.isvoid, 0) = 0 " & _
    '                                                "AND prtranhead_master.typeof_id = " & enTypeOf.Taxes & " " & _
    '                                                "AND prtnaleave_tran.payperiodunkid IN (" & mstrPeriodIds & ") "

    '        If mintEmpId > 0 Then
    '            StrQ &= "                            AND prpayrollprocess_tran.employeeunkid = @employeeunkid "
    '        End If

    '        'Pinkal (15-Oct-2014) -- Start
    '        'Enhancement -  CHANGES FOR AKFTZ - PUTTING USER ACCESS LEVEL IN ALL STATUTORY REPORTS AS PER MR.RUTTA' S COMMENT

    '        If UserAccessLevel._AccessLevel.Length > 0 Then
    '            StrQ &= UserAccessLevel._AccessLevelFilterString
    '        End If
    '        'Pinkal (15-Oct-2014) -- End

    '        StrQ &= "                      UNION ALL " & _
    '                                      "SELECT    0 AS basicsalary " & _
    '                                              ", 0 AS allowance " & _
    '                                              ", 0 AS noncashbenefit " & _
    '                                              ", 0 AS deduction " & _
    '                                              ", 0 AS taxpayable " & _
    '                                              ", CAST(ISNULL(prpayrollprocess_tran.amount, 0) AS DECIMAL(36, " & decDecimalPlaces & ")) AS paye " & _
    '                                              ", prpayrollprocess_tran.employeeunkid AS empid " & _
    '                                      "FROM      prpayrollprocess_tran " & _
    '                                                "JOIN prtnaleave_tran ON prpayrollprocess_tran.tnaleavetranunkid = prtnaleave_tran.tnaleavetranunkid " & _
    '                                                "JOIN hremployee_master ON prpayrollprocess_tran.employeeunkid = hremployee_master.employeeunkid " & _
    '                                                "LEFT JOIN prtranhead_master ON prtranhead_master.tranheadunkid = prpayrollprocess_tran.tranheadunkid "

    '        StrQ &= mstrAnalysis_Join

    '        StrQ &= "                      WHERE     ISNULL(prpayrollprocess_tran.isvoid, 0) = 0 " & _
    '                                                "AND ISNULL(prtnaleave_tran.isvoid, 0) = 0 " & _
    '                                                "AND prtranhead_master.tranheadunkid = @payetranheadunkid " & _
    '                                                "AND prtnaleave_tran.payperiodunkid IN (" & mstrPeriodIds & ") "

    '        If mintEmpId > 0 Then
    '            StrQ &= "                            AND prpayrollprocess_tran.employeeunkid = @employeeunkid "
    '        End If

    '        'Pinkal (15-Oct-2014) -- Start
    '        'Enhancement -  CHANGES FOR AKFTZ - PUTTING USER ACCESS LEVEL IN ALL STATUTORY REPORTS AS PER MR.RUTTA' S COMMENT

    '        If UserAccessLevel._AccessLevel.Length > 0 Then
    '            StrQ &= UserAccessLevel._AccessLevelFilterString
    '        End If
    '        'Pinkal (15-Oct-2014) -- End

    '        StrQ &= "                      ) AS PAYE " & _
    '                          "WHERE     1 = 1 " & _
    '                          "GROUP BY  empid " & _
    '                          "HAVING    SUM(ISNULL(PAYE.paye, 0)) <> 0 " & _
    '                        ") AS A " & _
    '                        "LEFT JOIN hremployee_master ON hremployee_master.employeeunkid = A.empid " & _
    '                        "LEFT JOIN hremployee_meminfo_tran ON hremployee_master.employeeunkid = hremployee_meminfo_tran.employeeunkid "

    '        'Sohail (14 Jun 2013) -- Start
    '        'TRA - ENHANCEMENT
    '        If mintMembershipId > 0 Then
    '            StrQ &= "AND hremployee_meminfo_tran.membershipunkid = @membershipunkid "
    '        End If
    '        'Sohail (14 Jun 2013) -- End

    '        StrQ &= "LEFT JOIN hrmembership_master ON hremployee_meminfo_tran.membershipunkid = hrmembership_master.membershipunkid " & _
    '                "WHERE   1 = 1 " & _
    '                "AND ISNULL(hremployee_meminfo_tran.isdeleted, 0) = 0 "
    '        'Sohail (14 Jun 2013) - [AND ISNULL(hremployee_meminfo_tran.isdeleted, 0) = 0,   ]




    '        Call FilterTitleAndFilterQuery()
    '        StrQ &= Me._FilterQuery

    '        'Anjan [25 Mar 2014] -- Start
    '        'ENHANCEMENT : Requested by Rutta
    '        If ConfigParameter._Object._FirstNamethenSurname = True Then
    '            StrQ &= " ORDER BY  ISNULL(hremployee_master.firstname, '') + ' ' + ISNULL(hremployee_master.othername, '') + ' ' + ISNULL(hremployee_master.surname, '') "
    '        Else
    '            StrQ &= " ORDER BY  ISNULL(hremployee_master.surname, '') + ' ' + ISNULL(hremployee_master.firstname, '') + ' ' + ISNULL(hremployee_master.othername, '')  "
    '        End If

    '        'Anjan [25 Mar 2014 ] -- End


    '        If mintEmpId > 0 Then
    '            objDataOperation.AddParameter("@employeeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintEmpId)
    '        End If

    '        dsList = objDataOperation.ExecQuery(StrQ, "DataTable")

    '        If objDataOperation.ErrorMessage <> "" Then
    '            exForce = New Exception(objDataOperation.ErrorNumber & " : " & objDataOperation.ErrorMessage)
    '            Throw exForce
    '        End If

    '        Dim dtTemp() As DataRow = dsList.Tables(0).Select("basicpay = 0 AND noncashbenefit = 0 AND allowancebenefit = 0 AND Grosspay = 0 AND deduction = 0 AND taxable = 0 AND taxpaid = 0")

    '        If dtTemp.Length > 0 Then
    '            For i As Integer = 0 To dtTemp.Length - 1
    '                dsList.Tables(0).Rows.Remove(dtTemp(i))
    '            Next
    '            dsList.Tables(0).AcceptChanges()
    '        End If


    '        rpt_Data = New ArutiReport.Designer.dsArutiReport

    '        Dim rpt_Row As DataRow = Nothing
    '        Dim intCnt As Integer = 1
    '        Dim intPrevEmp As Integer = 0

    '        Dim decTotBasic, decTotNcb, decTotAllowance, decTotGross, decTotDeduction, decTotTaxable, decTotTaxDue As Decimal
    '        decTotBasic = 0 : decTotNcb = 0 : decTotAllowance = 0 : decTotGross = 0 : decTotDeduction = 0 : decTotTaxable = 0 : decTotTaxDue = 0

    '        For Each dtRow As DataRow In dsList.Tables("DataTable").Rows

    '            If intPrevEmp = CInt(dtRow.Item("empid")) Then '*** To prevent Duplicate entries if employee has more than one Membership Assigned
    '                intPrevEmp = CInt(dtRow.Item("empid"))
    '                Continue For
    '            End If


    '            rpt_Row = rpt_Data.Tables("ArutiTable").NewRow

    '            rpt_Row.Item("Column1") = intCnt
    '            rpt_Row.Item("Column2") = dtRow.Item("empname")
    '            rpt_Row.Item("Column3") = dtRow.Item("empcode")
    '            'Sohail (14 Jun 2013) -- Start
    '            'TRA - ENHANCEMENT
    '            'rpt_Row.Item("Column4") = dtRow.Item("membershipname")
    '            rpt_Row.Item("Column4") = dtRow.Item("membershipno")
    '            'Sohail (14 Jun 2013) -- End
    '            'rpt_Row.Item("Column5") = dtRow.Item("city")

    '            rpt_Row.Item("Column6") = Format(CDec(dtRow.Item("basicpay")), GUI.fmtCurrency)
    '            rpt_Row.Item("Column7") = Format(CDec(dtRow.Item("noncashbenefit")), GUI.fmtCurrency)
    '            rpt_Row.Item("Column8") = Format(CDec(dtRow.Item("allowancebenefit")), GUI.fmtCurrency)
    '            rpt_Row.Item("Column9") = Format(CDec(dtRow.Item("Grosspay")), GUI.fmtCurrency)
    '            rpt_Row.Item("Column10") = Format(CDec(dtRow.Item("deduction")), GUI.fmtCurrency)
    '            rpt_Row.Item("Column11") = Format(CDec(dtRow.Item("taxable")), GUI.fmtCurrency)
    '            rpt_Row.Item("Column12") = Format(CDec(dtRow.Item("taxpaid")), GUI.fmtCurrency)

    '            decTotBasic = decTotBasic + CDec(dtRow.Item("basicpay"))
    '            decTotNcb = decTotNcb + CDec(dtRow.Item("noncashbenefit"))
    '            decTotAllowance = decTotAllowance + CDec(dtRow.Item("allowancebenefit"))
    '            decTotGross = decTotGross + CDec(dtRow.Item("Grosspay"))
    '            decTotDeduction = decTotDeduction + CDec(dtRow.Item("deduction"))
    '            decTotTaxable = decTotTaxable + CDec(dtRow.Item("taxable"))
    '            decTotTaxDue = decTotTaxDue + CDec(dtRow.Item("taxpaid"))

    '            intCnt += 1
    '            intPrevEmp = CInt(dtRow.Item("empid"))
    '            rpt_Data.Tables("ArutiTable").Rows.Add(rpt_Row)
    '        Next

    '        objRpt = New ArutiReport.Designer.rptTaxReport

    '        objRpt.SetDataSource(rpt_Data)

    '        ReportFunction.TextChange(objRpt.Subreports("rptEmployerDetail"), "lblTRA", Language.getMessage(mstrModuleName, 37, "TANZANIA REVENUE AUTHORITY")) 'Start From 37
    '        ReportFunction.TextChange(objRpt.Subreports("rptEmployerDetail"), "lblPAYE", Language.getMessage(mstrModuleName, 38, "P.A.Y.E."))
    '        ReportFunction.TextChange(objRpt.Subreports("rptEmployerDetail"), "lblReportName", Language.getMessage(mstrModuleName, 39, "STATEMENT AND PAYMENT OF TAX WITHHELD"))
    '        ReportFunction.TextChange(objRpt.Subreports("rptEmployerDetail"), "lblYear", Language.getMessage(mstrModuleName, 40, "YEAR:"))
    '        ReportFunction.TextChange(objRpt.Subreports("rptEmployerDetail"), "txtYear", FinancialYear._Object._FinancialYear_Name)
    '        ReportFunction.TextChange(objRpt.Subreports("rptEmployerDetail"), "lblTIN", Language.getMessage(mstrModuleName, 41, "TIN:"))
    '        ReportFunction.TextChange(objRpt.Subreports("rptEmployerDetail"), "txtTIN", Company._Object._Tinno)
    '        ReportFunction.TextChange(objRpt.Subreports("rptEmployerDetail"), "lblPeriod", Language.getMessage(mstrModuleName, 42, "Period: (Please tick the appropriate box) "))
    '        ReportFunction.TextChange(objRpt.Subreports("rptEmployerDetail"), "lblPeriod1", Language.getMessage(mstrModuleName, 43, "From 1 January to 30 June "))
    '        ReportFunction.TextChange(objRpt.Subreports("rptEmployerDetail"), "lblPeriod2", Language.getMessage(mstrModuleName, 44, "From 1 January to 31 December "))

    '        If mstrPeriodNames.Trim.Length > 0 Then
    '            ReportFunction.TextChange(objRpt.Subreports("rptEmployerDetail"), "txtPeriodNames", Language.getMessage(mstrModuleName, 81, "Periods : (") & mstrPeriodNames & Language.getMessage(mstrModuleName, 82, ")"))
    '        Else
    '            ReportFunction.TextChange(objRpt.Subreports("rptEmployerDetail"), "txtPeriodNames", "")
    '        End If


    '        ReportFunction.TextChange(objRpt.Subreports("rptEmployerDetail"), "lblEmployer", Language.getMessage(mstrModuleName, 45, "Name of Employer:"))
    '        ReportFunction.TextChange(objRpt.Subreports("rptEmployerDetail"), "txtEmployer", Company._Object._Name)

    '        ReportFunction.TextChange(objRpt.Subreports("rptEmployerDetail"), "lblPostalAddress", Language.getMessage(mstrModuleName, 46, "Postal Address:"))
    '        ReportFunction.TextChange(objRpt.Subreports("rptEmployerDetail"), "lblPoBox", Language.getMessage(mstrModuleName, 47, "P. O. Box"))
    '        Dim objZip As New clszipcode_master
    '        objZip._Zipcodeunkid = Company._Object._Postalunkid
    '        ReportFunction.TextChange(objRpt.Subreports("rptEmployerDetail"), "txtPoBox", objZip._Zipcode_No)
    '        objZip = Nothing
    '        ReportFunction.TextChange(objRpt.Subreports("rptEmployerDetail"), "lblPostalCity", Language.getMessage(mstrModuleName, 48, "Postal City"))
    '        Dim objCity As New clscity_master
    '        objCity._Cityunkid = Company._Object._Cityunkid
    '        ReportFunction.TextChange(objRpt.Subreports("rptEmployerDetail"), "txtPostalCity", objCity._Name)
    '        objCity = Nothing
    '        ReportFunction.TextChange(objRpt.Subreports("rptEmployerDetail"), "lblContactNumbers", Language.getMessage(mstrModuleName, 49, "Contact Numbers:"))
    '        ReportFunction.TextChange(objRpt.Subreports("rptEmployerDetail"), "lblPhone", Language.getMessage(mstrModuleName, 50, "Phone number"))
    '        ReportFunction.TextChange(objRpt.Subreports("rptEmployerDetail"), "txtPhone1", Company._Object._Phone1)
    '        ReportFunction.TextChange(objRpt.Subreports("rptEmployerDetail"), "lblSecondPhone", Language.getMessage(mstrModuleName, 51, "Second Phone"))
    '        ReportFunction.TextChange(objRpt.Subreports("rptEmployerDetail"), "txtSecondPhone", Company._Object._Phone2)
    '        ReportFunction.TextChange(objRpt.Subreports("rptEmployerDetail"), "lblThirdPhone", Language.getMessage(mstrModuleName, 52, "Third Phone"))
    '        ReportFunction.TextChange(objRpt.Subreports("rptEmployerDetail"), "txtThirdphone", Company._Object._Phone3)
    '        ReportFunction.TextChange(objRpt.Subreports("rptEmployerDetail"), "lblFax", Language.getMessage(mstrModuleName, 53, " Fax number"))
    '        ReportFunction.TextChange(objRpt.Subreports("rptEmployerDetail"), "txtFax", Company._Object._Fax)
    '        ReportFunction.TextChange(objRpt.Subreports("rptEmployerDetail"), "lblEmail", Language.getMessage(mstrModuleName, 54, "E-mail address:"))
    '        ReportFunction.TextChange(objRpt.Subreports("rptEmployerDetail"), "txtEmail", Company._Object._Email)
    '        ReportFunction.TextChange(objRpt.Subreports("rptEmployerDetail"), "lblPhysical", Language.getMessage(mstrModuleName, 55, "Physical Address:"))
    '        ReportFunction.TextChange(objRpt.Subreports("rptEmployerDetail"), "lblPlot", Language.getMessage(mstrModuleName, 56, "Plot Number"))
    '        ReportFunction.TextChange(objRpt.Subreports("rptEmployerDetail"), "txtPlot", Company._Object._Reg1_Value)
    '        ReportFunction.TextChange(objRpt.Subreports("rptEmployerDetail"), "lblBlock", Language.getMessage(mstrModuleName, 57, "Block Number"))
    '        ReportFunction.TextChange(objRpt.Subreports("rptEmployerDetail"), "txtBlock", Company._Object._Reg2_Value)
    '        ReportFunction.TextChange(objRpt.Subreports("rptEmployerDetail"), "lblStreet", Language.getMessage(mstrModuleName, 58, "Street/Location"))
    '        ReportFunction.TextChange(objRpt.Subreports("rptEmployerDetail"), "txtStreet", Company._Object._Address1 & " " & Company._Object._Address2)
    '        ReportFunction.TextChange(objRpt.Subreports("rptEmployerDetail"), "lblBranch", Language.getMessage(mstrModuleName, 59, "Name of Branch"))
    '        Dim objBranch As New clsbankbranch_master
    '        objBranch._Branchunkid = Company._Object._Branchunkid
    '        ReportFunction.TextChange(objRpt.Subreports("rptEmployerDetail"), "txtBranch", objBranch._Branchname)
    '        objBranch = Nothing
    '        ReportFunction.TextChange(objRpt.Subreports("rptEmployerDetail"), "lblPaymentStatement", Language.getMessage(mstrModuleName, 60, "ITX215.01.E -  P.A.Y.E.-Statement"))


    '        ReportFunction.TextChange(objRpt, "txtReportName", Language.getMessage(mstrModuleName, 61, "TAX Report"))
    '        ReportFunction.TextChange(objRpt, "txtCompanyName", Company._Object._Name)
    '        ReportFunction.TextChange(objRpt, "txtAddress", Company._Object._Address1 & " " & Company._Object._Address2 & " " & Company._Object._City_Name & " " & Company._Object._State_Name & " " & Company._Object._Post_Code_No)
    '        ReportFunction.TextChange(objRpt, "txtCountry", Company._Object._Country_Name)
    '        ReportFunction.TextChange(objRpt, "lblDate", Language.getMessage(mstrModuleName, 62, "Date :"))
    '        ReportFunction.TextChange(objRpt, "txtDate", ConfigParameter._Object._CurrentDateAndTime.ToShortDateString)
    '        ReportFunction.TextChange(objRpt, "lblReturnPeriods", Language.getMessage(mstrModuleName, 68, "Return Periods :"))
    '        ReportFunction.TextChange(objRpt, "txtReturnPeriods", mstrPeriodNames)
    '        ReportFunction.TextChange(objRpt, "lblCurrency", Language.getMessage(mstrModuleName, 77, "Currency :"))
    '        ReportFunction.TextChange(objRpt, "txtCurrency", strBaseCurrencySign)
    '        ReportFunction.TextChange(objRpt, "lblVRN", Language.getMessage(mstrModuleName, 78, "VRN :"))
    '        ReportFunction.TextChange(objRpt, "txtVRN", Company._Object._Registerdno)


    '        ReportFunction.TextChange(objRpt, "lblTIN", Language.getMessage(mstrModuleName, 63, "TIN :"))
    '        ReportFunction.TextChange(objRpt, "txtTIN", Company._Object._Tinno)
    '        ReportFunction.TextChange(objRpt, "txtSrNo", Language.getMessage(mstrModuleName, 64, "S/No."))
    '        ReportFunction.TextChange(objRpt, "txtEmpName", Language.getMessage(mstrModuleName, 65, "Name Of Employee"))
    '        'Sohail (29 Mar 2014) -- Start
    '        'ENHANCEMENT - Show Payroll No. and Employee TIN No on Tax Report.
    '        'ReportFunction.TextChange(objRpt, "txtPayroll", Language.getMessage(mstrModuleName, 66, "Payroll No."))
    '        'ReportFunction.TextChange(objRpt, "txtTINNo", Language.getMessage(mstrModuleName, 67, "Employee TIN No"))
    '        If mblnShowPayrollNo = True Then
    '            ReportFunction.TextChange(objRpt, "txtPayroll", Language.getMessage(mstrModuleName, 66, "Payroll No."))
    '        Else
    '            ReportFunction.TextChange(objRpt, "txtPayroll", "")
    '            ReportFunction.EnableSuppress(objRpt, "txtPayroll", True)
    '            ReportFunction.EnableSuppress(objRpt, "Column31", True)
    '        End If
    '        If mblnShowEmployeeTINNo = True Then
    '            ReportFunction.TextChange(objRpt, "txtTINNo", Language.getMessage(mstrModuleName, 67, "Employee TIN No"))
    '        Else
    '            ReportFunction.TextChange(objRpt, "txtTINNo", "")
    '            ReportFunction.EnableSuppress(objRpt, "txtTINNo", True)
    '            ReportFunction.EnableSuppress(objRpt, "Column41", True)
    '        End If
    '        If mblnShowPayrollNo = False AndAlso mblnShowEmployeeTINNo = False Then
    '            objRpt.ReportDefinition.ReportObjects("txtEmpName").Left = objRpt.ReportDefinition.ReportObjects("txtPayroll").Left
    '            objRpt.ReportDefinition.ReportObjects("txtEmpName").Width = objRpt.ReportDefinition.ReportObjects("txtEmpName").Width + objRpt.ReportDefinition.ReportObjects("txtPayroll").Width + objRpt.ReportDefinition.ReportObjects("txtTINNo").Width
    '            objRpt.ReportDefinition.ReportObjects("Column21").Left = objRpt.ReportDefinition.ReportObjects("Column31").Left
    '            objRpt.ReportDefinition.ReportObjects("Column21").Width = objRpt.ReportDefinition.ReportObjects("Column21").Width + objRpt.ReportDefinition.ReportObjects("Column31").Width + objRpt.ReportDefinition.ReportObjects("Column41").Width
    '        ElseIf mblnShowEmployeeTINNo = False Then
    '            objRpt.ReportDefinition.ReportObjects("txtEmpName").Width = objRpt.ReportDefinition.ReportObjects("txtEmpName").Width + objRpt.ReportDefinition.ReportObjects("txtTINNo").Width
    '            objRpt.ReportDefinition.ReportObjects("Column21").Width = objRpt.ReportDefinition.ReportObjects("Column21").Width + objRpt.ReportDefinition.ReportObjects("Column41").Width
    '        ElseIf mblnShowPayrollNo = False Then
    '            objRpt.ReportDefinition.ReportObjects("txtEmpName").Left = objRpt.ReportDefinition.ReportObjects("txtPayroll").Left
    '            objRpt.ReportDefinition.ReportObjects("txtEmpName").Width = objRpt.ReportDefinition.ReportObjects("txtEmpName").Width + objRpt.ReportDefinition.ReportObjects("txtPayroll").Width
    '            objRpt.ReportDefinition.ReportObjects("Column21").Left = objRpt.ReportDefinition.ReportObjects("Column31").Left
    '            objRpt.ReportDefinition.ReportObjects("Column21").Width = objRpt.ReportDefinition.ReportObjects("Column21").Width + objRpt.ReportDefinition.ReportObjects("Column31").Width
    '        End If
    '        'objRpt.ReportDefinition.ReportObjects("txtReportMode").Left = objRpt.ReportDefinition.ReportObjects("txtCompanyName").Left
    '        'Sohail (29 Mar 2014) -- End
    '        ReportFunction.TextChange(objRpt, "txtPay", Language.getMessage(mstrModuleName, 69, "Basic Salary"))
    '        ReportFunction.TextChange(objRpt, "txtNonCash", Language.getMessage(mstrModuleName, 70, "Non Cash Benefit"))
    '        ReportFunction.TextChange(objRpt, "txtAllBenefit", Language.getMessage(mstrModuleName, 71, "Other Taxable Allowances"))
    '        ReportFunction.TextChange(objRpt, "txtGrossPay", Language.getMessage(mstrModuleName, 72, "Gross Pay"))
    '        ReportFunction.TextChange(objRpt, "txtDeduction", Language.getMessage(mstrModuleName, 73, "Deductions"))
    '        ReportFunction.TextChange(objRpt, "txtTaxable", Language.getMessage(mstrModuleName, 74, "Taxable Pay"))
    '        ReportFunction.TextChange(objRpt, "txtTaxDue", Language.getMessage(mstrModuleName, 75, "Tax Paid"))
    '        ReportFunction.TextChange(objRpt, "txtGrandTotal", Language.getMessage(mstrModuleName, 76, "Total"))


    '        Call ReportFunction.TextChange(objRpt, "txtTotal6", Format(decTotBasic, GUI.fmtCurrency))
    '        Call ReportFunction.TextChange(objRpt, "txtTotal7", Format(decTotNcb, GUI.fmtCurrency))
    '        Call ReportFunction.TextChange(objRpt, "txtTotal8", Format(decTotAllowance, GUI.fmtCurrency))
    '        Call ReportFunction.TextChange(objRpt, "txtTotal9", Format(decTotGross, GUI.fmtCurrency))
    '        Call ReportFunction.TextChange(objRpt, "txtTotal10", Format(decTotDeduction, GUI.fmtCurrency))
    '        Call ReportFunction.TextChange(objRpt, "txtTotal11", Format(decTotTaxable, GUI.fmtCurrency))
    '        Call ReportFunction.TextChange(objRpt, "txtTotal12", Format(decTotTaxDue, GUI.fmtCurrency))

    '        Return objRpt

    '    Catch ex As Exception
    '        Throw New Exception(ex.Message & "; Procedure Name: Generate_DetailReport; Module Name: " & mstrModuleName)
    '        Return Nothing
    '    End Try
    'End Function
#End Region
    'Sohail (23 Apr 2015) -- End


    '<Language> This Auto Generated Text Please Do Not Modify it.
#Region " Language & UI Settings "
    Public Shared Sub SetMessages()
        Try
            Language.setMessage(mstrModuleName, 37, "TANZANIA REVENUE AUTHORITY")
            Language.setMessage(mstrModuleName, 38, "P.A.Y.E.")
            Language.setMessage(mstrModuleName, 39, "STATEMENT AND PAYMENT OF TAX WITHHELD")
            Language.setMessage(mstrModuleName, 40, "YEAR:")
            Language.setMessage(mstrModuleName, 41, "TIN:")
            Language.setMessage(mstrModuleName, 42, "Period: (Please tick the appropriate box)")
            Language.setMessage(mstrModuleName, 43, "From 1 January to 30 June")
            Language.setMessage(mstrModuleName, 44, "From 1 January to 31 December")
            Language.setMessage(mstrModuleName, 45, "Name of Employer:")
            Language.setMessage(mstrModuleName, 46, "Postal Address:")
            Language.setMessage(mstrModuleName, 47, "P. O. Box")
            Language.setMessage(mstrModuleName, 48, "Postal City")
            Language.setMessage(mstrModuleName, 49, "Contact Numbers:")
            Language.setMessage(mstrModuleName, 50, "Phone number")
            Language.setMessage(mstrModuleName, 51, "Second Phone")
            Language.setMessage(mstrModuleName, 52, "Third Phone")
            Language.setMessage(mstrModuleName, 53, " Fax number")
            Language.setMessage(mstrModuleName, 54, "E-mail address:")
            Language.setMessage(mstrModuleName, 55, "Physical Address:")
            Language.setMessage(mstrModuleName, 56, "Plot Number")
            Language.setMessage(mstrModuleName, 57, "Block Number")
            Language.setMessage(mstrModuleName, 58, "Street/Location")
            Language.setMessage(mstrModuleName, 59, "Name of Branch")
            Language.setMessage(mstrModuleName, 60, "ITX215.01.E -  P.A.Y.E.-Statement")
            Language.setMessage(mstrModuleName, 61, "TAX Report")
            Language.setMessage(mstrModuleName, 62, "Date :")
            Language.setMessage(mstrModuleName, 63, "TIN :")
            Language.setMessage(mstrModuleName, 64, "S/No.")
            Language.setMessage(mstrModuleName, 65, "Name Of Employee")
            Language.setMessage(mstrModuleName, 66, "Payroll No.")
            Language.setMessage(mstrModuleName, 67, "Employee TIN No")
            Language.setMessage(mstrModuleName, 68, "Return Periods :")
            Language.setMessage(mstrModuleName, 69, "Basic Salary")
            Language.setMessage(mstrModuleName, 70, "Non Cash Benefit")
            Language.setMessage(mstrModuleName, 71, "Other Taxable Allowances")
            Language.setMessage(mstrModuleName, 72, "Gross Pay")
            Language.setMessage(mstrModuleName, 73, "Deductions")
            Language.setMessage(mstrModuleName, 74, "Taxable Pay")
            Language.setMessage(mstrModuleName, 75, "Tax Paid")
            Language.setMessage(mstrModuleName, 76, "Total")
            Language.setMessage(mstrModuleName, 77, "Currency :")
            Language.setMessage(mstrModuleName, 78, "VRN :")
            Language.setMessage(mstrModuleName, 81, "Periods : (")
            Language.setMessage(mstrModuleName, 82, "")

        Catch Ex As Exception
            Throw New Exception(Ex.Message & "; Procedure Name: SetMessages; Module Name: " & mstrModuleName)
        End Try
    End Sub
#End Region 'Language & UI Settings
    '</Language>


End Class
