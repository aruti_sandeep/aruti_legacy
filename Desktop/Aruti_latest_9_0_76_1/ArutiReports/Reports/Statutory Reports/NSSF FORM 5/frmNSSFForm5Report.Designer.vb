﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmNSSFForm5Report
    Inherits Aruti.Data.frmBaseReportForm

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmNSSFForm5Report))
        Me.gbMandatoryInfo = New eZee.Common.eZeeCollapsibleContainer(Me.components)
        Me.lnkExportDataxlsx = New System.Windows.Forms.LinkLabel
        Me.btnSaveSelection = New eZee.Common.eZeeLightButton(Me.components)
        Me.cboIdentity = New System.Windows.Forms.ComboBox
        Me.lblIdentity = New System.Windows.Forms.Label
        Me.LblKRAPin = New System.Windows.Forms.Label
        Me.cboKRAPIN = New System.Windows.Forms.ComboBox
        Me.dtpDate = New System.Windows.Forms.DateTimePicker
        Me.lblAsOnDate = New System.Windows.Forms.Label
        Me.objCheckAll = New System.Windows.Forms.CheckBox
        Me.lblPeriodList = New System.Windows.Forms.Label
        Me.lvPeriodList = New System.Windows.Forms.ListView
        Me.objcolhCheck = New System.Windows.Forms.ColumnHeader
        Me.colhPeriodName = New System.Windows.Forms.ColumnHeader
        Me.objcolhPeriodStart = New System.Windows.Forms.ColumnHeader
        Me.chkIgnoreZero = New System.Windows.Forms.CheckBox
        Me.lblVOLHead = New System.Windows.Forms.Label
        Me.cboVOLHead = New System.Windows.Forms.ComboBox
        Me.lblIncludeMembership = New System.Windows.Forms.Label
        Me.cboIncludeMembership = New System.Windows.Forms.ComboBox
        Me.objlblExRate = New System.Windows.Forms.Label
        Me.LblCurrency = New System.Windows.Forms.Label
        Me.cboCurrency = New System.Windows.Forms.ComboBox
        Me.cboEmpHeadType = New System.Windows.Forms.ComboBox
        Me.chkShowBasicSal = New System.Windows.Forms.CheckBox
        Me.lblCompanyHeadType = New System.Windows.Forms.Label
        Me.lnkAdvanceFilter = New System.Windows.Forms.LinkLabel
        Me.lblEmpContriHeadType = New System.Windows.Forms.Label
        Me.lblMembership = New System.Windows.Forms.Label
        Me.cboCoHeadType = New System.Windows.Forms.ComboBox
        Me.cboMembership = New System.Windows.Forms.ComboBox
        Me.lblPeriod = New System.Windows.Forms.Label
        Me.cboPeriod = New System.Windows.Forms.ComboBox
        Me.gbBasicSalaryOtherEarning = New eZee.Common.eZeeCollapsibleContainer(Me.components)
        Me.pnlAdvanceTaken = New System.Windows.Forms.Panel
        Me.objbtnSearchOtherEarning = New eZee.Common.eZeeGradientButton
        Me.cboOtherEarning = New System.Windows.Forms.ComboBox
        Me.lblOtherEarning = New System.Windows.Forms.Label
        Me.gbMandatoryInfo.SuspendLayout()
        Me.gbBasicSalaryOtherEarning.SuspendLayout()
        Me.pnlAdvanceTaken.SuspendLayout()
        Me.SuspendLayout()
        '
        'NavPanel
        '
        Me.NavPanel.Location = New System.Drawing.Point(0, 528)
        Me.NavPanel.Size = New System.Drawing.Size(765, 55)
        '
        'gbMandatoryInfo
        '
        Me.gbMandatoryInfo.BorderColor = System.Drawing.Color.Black
        Me.gbMandatoryInfo.Checked = False
        Me.gbMandatoryInfo.CollapseAllExceptThis = False
        Me.gbMandatoryInfo.CollapsedHoverImage = Nothing
        Me.gbMandatoryInfo.CollapsedNormalImage = Nothing
        Me.gbMandatoryInfo.CollapsedPressedImage = Nothing
        Me.gbMandatoryInfo.CollapseOnLoad = False
        Me.gbMandatoryInfo.Controls.Add(Me.lnkExportDataxlsx)
        Me.gbMandatoryInfo.Controls.Add(Me.btnSaveSelection)
        Me.gbMandatoryInfo.Controls.Add(Me.cboIdentity)
        Me.gbMandatoryInfo.Controls.Add(Me.lblIdentity)
        Me.gbMandatoryInfo.Controls.Add(Me.LblKRAPin)
        Me.gbMandatoryInfo.Controls.Add(Me.cboKRAPIN)
        Me.gbMandatoryInfo.Controls.Add(Me.dtpDate)
        Me.gbMandatoryInfo.Controls.Add(Me.lblAsOnDate)
        Me.gbMandatoryInfo.Controls.Add(Me.objCheckAll)
        Me.gbMandatoryInfo.Controls.Add(Me.lblPeriodList)
        Me.gbMandatoryInfo.Controls.Add(Me.lvPeriodList)
        Me.gbMandatoryInfo.Controls.Add(Me.chkIgnoreZero)
        Me.gbMandatoryInfo.Controls.Add(Me.lblVOLHead)
        Me.gbMandatoryInfo.Controls.Add(Me.cboVOLHead)
        Me.gbMandatoryInfo.Controls.Add(Me.lblIncludeMembership)
        Me.gbMandatoryInfo.Controls.Add(Me.cboIncludeMembership)
        Me.gbMandatoryInfo.Controls.Add(Me.objlblExRate)
        Me.gbMandatoryInfo.Controls.Add(Me.LblCurrency)
        Me.gbMandatoryInfo.Controls.Add(Me.cboCurrency)
        Me.gbMandatoryInfo.Controls.Add(Me.cboEmpHeadType)
        Me.gbMandatoryInfo.Controls.Add(Me.chkShowBasicSal)
        Me.gbMandatoryInfo.Controls.Add(Me.lblCompanyHeadType)
        Me.gbMandatoryInfo.Controls.Add(Me.lnkAdvanceFilter)
        Me.gbMandatoryInfo.Controls.Add(Me.lblEmpContriHeadType)
        Me.gbMandatoryInfo.Controls.Add(Me.lblMembership)
        Me.gbMandatoryInfo.Controls.Add(Me.cboCoHeadType)
        Me.gbMandatoryInfo.Controls.Add(Me.cboMembership)
        Me.gbMandatoryInfo.Controls.Add(Me.lblPeriod)
        Me.gbMandatoryInfo.Controls.Add(Me.cboPeriod)
        Me.gbMandatoryInfo.ExpandedHoverImage = Nothing
        Me.gbMandatoryInfo.ExpandedNormalImage = Nothing
        Me.gbMandatoryInfo.ExpandedPressedImage = Nothing
        Me.gbMandatoryInfo.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.gbMandatoryInfo.GradientColor = System.Drawing.SystemColors.ButtonFace
        Me.gbMandatoryInfo.HeaderHeight = 25
        Me.gbMandatoryInfo.HeaderMessage = ""
        Me.gbMandatoryInfo.HeaderMessageFont = New System.Drawing.Font("Tahoma", 8.25!)
        Me.gbMandatoryInfo.HeaderMessageForeColor = System.Drawing.Color.Black
        Me.gbMandatoryInfo.HeightOnCollapse = 0
        Me.gbMandatoryInfo.LeftTextSpace = 0
        Me.gbMandatoryInfo.Location = New System.Drawing.Point(12, 66)
        Me.gbMandatoryInfo.Name = "gbMandatoryInfo"
        Me.gbMandatoryInfo.OpenHeight = 300
        Me.gbMandatoryInfo.PanelBorderStyle = System.Windows.Forms.ButtonBorderStyle.Solid
        Me.gbMandatoryInfo.ShowBorder = True
        Me.gbMandatoryInfo.ShowCheckBox = False
        Me.gbMandatoryInfo.ShowCollapseButton = False
        Me.gbMandatoryInfo.ShowDefaultBorderColor = True
        Me.gbMandatoryInfo.ShowDownButton = False
        Me.gbMandatoryInfo.ShowHeader = True
        Me.gbMandatoryInfo.Size = New System.Drawing.Size(455, 380)
        Me.gbMandatoryInfo.TabIndex = 0
        Me.gbMandatoryInfo.Temp = 0
        Me.gbMandatoryInfo.Text = "Mandatory Information"
        Me.gbMandatoryInfo.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'lnkExportDataxlsx
        '
        Me.lnkExportDataxlsx.BackColor = System.Drawing.Color.Transparent
        Me.lnkExportDataxlsx.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lnkExportDataxlsx.LinkBehavior = System.Windows.Forms.LinkBehavior.NeverUnderline
        Me.lnkExportDataxlsx.Location = New System.Drawing.Point(224, 350)
        Me.lnkExportDataxlsx.Name = "lnkExportDataxlsx"
        Me.lnkExportDataxlsx.Size = New System.Drawing.Size(204, 17)
        Me.lnkExportDataxlsx.TabIndex = 113
        Me.lnkExportDataxlsx.TabStop = True
        Me.lnkExportDataxlsx.Text = "Export in xlsx format (Data Only)"
        Me.lnkExportDataxlsx.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.lnkExportDataxlsx.Visible = False
        '
        'btnSaveSelection
        '
        Me.btnSaveSelection.BackColor = System.Drawing.Color.White
        Me.btnSaveSelection.BackgroundImage = CType(resources.GetObject("btnSaveSelection.BackgroundImage"), System.Drawing.Image)
        Me.btnSaveSelection.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.btnSaveSelection.BorderColor = System.Drawing.Color.Empty
        Me.btnSaveSelection.ButtonType = eZee.Common.eZeeLightButton.enButtonType.NormalButton
        Me.btnSaveSelection.FlatAppearance.BorderSize = 0
        Me.btnSaveSelection.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnSaveSelection.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnSaveSelection.ForeColor = System.Drawing.Color.Black
        Me.btnSaveSelection.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.btnSaveSelection.GradientForeColor = System.Drawing.Color.Black
        Me.btnSaveSelection.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.btnSaveSelection.HoverGradientForeColor = System.Drawing.Color.Black
        Me.btnSaveSelection.Location = New System.Drawing.Point(107, 343)
        Me.btnSaveSelection.Name = "btnSaveSelection"
        Me.btnSaveSelection.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.btnSaveSelection.PressedGradientForeColor = System.Drawing.Color.Black
        Me.btnSaveSelection.Size = New System.Drawing.Size(111, 30)
        Me.btnSaveSelection.TabIndex = 109
        Me.btnSaveSelection.Text = "&Save Selection"
        Me.btnSaveSelection.UseVisualStyleBackColor = True
        '
        'cboIdentity
        '
        Me.cboIdentity.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboIdentity.DropDownWidth = 180
        Me.cboIdentity.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboIdentity.FormattingEnabled = True
        Me.cboIdentity.Location = New System.Drawing.Point(326, 61)
        Me.cboIdentity.Name = "cboIdentity"
        Me.cboIdentity.Size = New System.Drawing.Size(118, 21)
        Me.cboIdentity.TabIndex = 107
        '
        'lblIdentity
        '
        Me.lblIdentity.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblIdentity.Location = New System.Drawing.Point(233, 59)
        Me.lblIdentity.Name = "lblIdentity"
        Me.lblIdentity.Size = New System.Drawing.Size(90, 21)
        Me.lblIdentity.TabIndex = 106
        Me.lblIdentity.Text = "Identity"
        Me.lblIdentity.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'LblKRAPin
        '
        Me.LblKRAPin.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LblKRAPin.Location = New System.Drawing.Point(233, 90)
        Me.LblKRAPin.Name = "LblKRAPin"
        Me.LblKRAPin.Size = New System.Drawing.Size(87, 15)
        Me.LblKRAPin.TabIndex = 101
        Me.LblKRAPin.Text = "KRA PIN"
        Me.LblKRAPin.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'cboKRAPIN
        '
        Me.cboKRAPIN.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboKRAPIN.DropDownWidth = 180
        Me.cboKRAPIN.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboKRAPIN.FormattingEnabled = True
        Me.cboKRAPIN.Location = New System.Drawing.Point(326, 87)
        Me.cboKRAPIN.Name = "cboKRAPIN"
        Me.cboKRAPIN.Size = New System.Drawing.Size(118, 21)
        Me.cboKRAPIN.TabIndex = 100
        '
        'dtpDate
        '
        Me.dtpDate.CalendarFont = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.dtpDate.Checked = False
        Me.dtpDate.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.dtpDate.Format = System.Windows.Forms.DateTimePickerFormat.[Short]
        Me.dtpDate.Location = New System.Drawing.Point(109, 87)
        Me.dtpDate.Name = "dtpDate"
        Me.dtpDate.Size = New System.Drawing.Size(118, 21)
        Me.dtpDate.TabIndex = 2
        '
        'lblAsOnDate
        '
        Me.lblAsOnDate.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblAsOnDate.Location = New System.Drawing.Point(9, 89)
        Me.lblAsOnDate.Name = "lblAsOnDate"
        Me.lblAsOnDate.Size = New System.Drawing.Size(94, 16)
        Me.lblAsOnDate.TabIndex = 98
        Me.lblAsOnDate.Text = "Curr. As On Date"
        Me.lblAsOnDate.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'objCheckAll
        '
        Me.objCheckAll.AutoSize = True
        Me.objCheckAll.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.objCheckAll.Location = New System.Drawing.Point(117, 147)
        Me.objCheckAll.Name = "objCheckAll"
        Me.objCheckAll.Size = New System.Drawing.Size(15, 14)
        Me.objCheckAll.TabIndex = 95
        Me.objCheckAll.UseVisualStyleBackColor = True
        '
        'lblPeriodList
        '
        Me.lblPeriodList.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblPeriodList.Location = New System.Drawing.Point(9, 145)
        Me.lblPeriodList.Name = "lblPeriodList"
        Me.lblPeriodList.Size = New System.Drawing.Size(94, 16)
        Me.lblPeriodList.TabIndex = 94
        Me.lblPeriodList.Text = "Period List"
        Me.lblPeriodList.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'lvPeriodList
        '
        Me.lvPeriodList.CheckBoxes = True
        Me.lvPeriodList.Columns.AddRange(New System.Windows.Forms.ColumnHeader() {Me.objcolhCheck, Me.colhPeriodName, Me.objcolhPeriodStart})
        Me.lvPeriodList.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lvPeriodList.FullRowSelect = True
        Me.lvPeriodList.HeaderStyle = System.Windows.Forms.ColumnHeaderStyle.Nonclickable
        Me.lvPeriodList.Location = New System.Drawing.Point(109, 141)
        Me.lvPeriodList.Name = "lvPeriodList"
        Me.lvPeriodList.Size = New System.Drawing.Size(335, 173)
        Me.lvPeriodList.TabIndex = 5
        Me.lvPeriodList.UseCompatibleStateImageBehavior = False
        Me.lvPeriodList.View = System.Windows.Forms.View.Details
        '
        'objcolhCheck
        '
        Me.objcolhCheck.Tag = "objcolhCheck"
        Me.objcolhCheck.Text = ""
        Me.objcolhCheck.Width = 25
        '
        'colhPeriodName
        '
        Me.colhPeriodName.Tag = "colhPeriodName"
        Me.colhPeriodName.Text = "Period"
        Me.colhPeriodName.Width = 295
        '
        'objcolhPeriodStart
        '
        Me.objcolhPeriodStart.Tag = "objcolhPeriodStart"
        Me.objcolhPeriodStart.Text = "objcolhPeriodStart"
        Me.objcolhPeriodStart.Width = 0
        '
        'chkIgnoreZero
        '
        Me.chkIgnoreZero.Checked = True
        Me.chkIgnoreZero.CheckState = System.Windows.Forms.CheckState.Checked
        Me.chkIgnoreZero.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.chkIgnoreZero.Location = New System.Drawing.Point(107, 320)
        Me.chkIgnoreZero.Name = "chkIgnoreZero"
        Me.chkIgnoreZero.Size = New System.Drawing.Size(205, 17)
        Me.chkIgnoreZero.TabIndex = 6
        Me.chkIgnoreZero.Text = "Ignore Zero"
        Me.chkIgnoreZero.UseVisualStyleBackColor = True
        '
        'lblVOLHead
        '
        Me.lblVOLHead.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblVOLHead.Location = New System.Drawing.Point(233, 117)
        Me.lblVOLHead.Name = "lblVOLHead"
        Me.lblVOLHead.Size = New System.Drawing.Size(87, 15)
        Me.lblVOLHead.TabIndex = 88
        Me.lblVOLHead.Text = "VOL Head"
        Me.lblVOLHead.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'cboVOLHead
        '
        Me.cboVOLHead.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboVOLHead.DropDownWidth = 180
        Me.cboVOLHead.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboVOLHead.FormattingEnabled = True
        Me.cboVOLHead.Location = New System.Drawing.Point(326, 114)
        Me.cboVOLHead.Name = "cboVOLHead"
        Me.cboVOLHead.Size = New System.Drawing.Size(118, 21)
        Me.cboVOLHead.TabIndex = 4
        '
        'lblIncludeMembership
        '
        Me.lblIncludeMembership.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblIncludeMembership.Location = New System.Drawing.Point(9, 110)
        Me.lblIncludeMembership.Name = "lblIncludeMembership"
        Me.lblIncludeMembership.Size = New System.Drawing.Size(94, 26)
        Me.lblIncludeMembership.TabIndex = 86
        Me.lblIncludeMembership.Text = "Include Membership"
        Me.lblIncludeMembership.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'cboIncludeMembership
        '
        Me.cboIncludeMembership.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboIncludeMembership.DropDownWidth = 230
        Me.cboIncludeMembership.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboIncludeMembership.FormattingEnabled = True
        Me.cboIncludeMembership.Location = New System.Drawing.Point(109, 114)
        Me.cboIncludeMembership.Name = "cboIncludeMembership"
        Me.cboIncludeMembership.Size = New System.Drawing.Size(118, 21)
        Me.cboIncludeMembership.TabIndex = 3
        '
        'objlblExRate
        '
        Me.objlblExRate.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.objlblExRate.Location = New System.Drawing.Point(235, 63)
        Me.objlblExRate.Name = "objlblExRate"
        Me.objlblExRate.Size = New System.Drawing.Size(207, 15)
        Me.objlblExRate.TabIndex = 79
        Me.objlblExRate.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'LblCurrency
        '
        Me.LblCurrency.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LblCurrency.Location = New System.Drawing.Point(9, 62)
        Me.LblCurrency.Name = "LblCurrency"
        Me.LblCurrency.Size = New System.Drawing.Size(94, 15)
        Me.LblCurrency.TabIndex = 75
        Me.LblCurrency.Text = "Currency"
        Me.LblCurrency.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'cboCurrency
        '
        Me.cboCurrency.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboCurrency.DropDownWidth = 180
        Me.cboCurrency.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboCurrency.FormattingEnabled = True
        Me.cboCurrency.Location = New System.Drawing.Point(109, 60)
        Me.cboCurrency.Name = "cboCurrency"
        Me.cboCurrency.Size = New System.Drawing.Size(118, 21)
        Me.cboCurrency.TabIndex = 1
        '
        'cboEmpHeadType
        '
        Me.cboEmpHeadType.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboEmpHeadType.DropDownWidth = 180
        Me.cboEmpHeadType.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboEmpHeadType.FormattingEnabled = True
        Me.cboEmpHeadType.Location = New System.Drawing.Point(107, 380)
        Me.cboEmpHeadType.Name = "cboEmpHeadType"
        Me.cboEmpHeadType.Size = New System.Drawing.Size(118, 21)
        Me.cboEmpHeadType.TabIndex = 59
        Me.cboEmpHeadType.Visible = False
        '
        'chkShowBasicSal
        '
        Me.chkShowBasicSal.Checked = True
        Me.chkShowBasicSal.CheckState = System.Windows.Forms.CheckState.Checked
        Me.chkShowBasicSal.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.chkShowBasicSal.Location = New System.Drawing.Point(107, 379)
        Me.chkShowBasicSal.Name = "chkShowBasicSal"
        Me.chkShowBasicSal.Size = New System.Drawing.Size(205, 17)
        Me.chkShowBasicSal.TabIndex = 73
        Me.chkShowBasicSal.Text = "Show Basic Salary"
        Me.chkShowBasicSal.UseVisualStyleBackColor = True
        Me.chkShowBasicSal.Visible = False
        '
        'lblCompanyHeadType
        '
        Me.lblCompanyHeadType.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblCompanyHeadType.Location = New System.Drawing.Point(231, 379)
        Me.lblCompanyHeadType.Name = "lblCompanyHeadType"
        Me.lblCompanyHeadType.Size = New System.Drawing.Size(87, 15)
        Me.lblCompanyHeadType.TabIndex = 62
        Me.lblCompanyHeadType.Text = "Co. Contribution"
        Me.lblCompanyHeadType.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.lblCompanyHeadType.Visible = False
        '
        'lnkAdvanceFilter
        '
        Me.lnkAdvanceFilter.BackColor = System.Drawing.Color.Transparent
        Me.lnkAdvanceFilter.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lnkAdvanceFilter.Location = New System.Drawing.Point(357, 4)
        Me.lnkAdvanceFilter.Name = "lnkAdvanceFilter"
        Me.lnkAdvanceFilter.Size = New System.Drawing.Size(94, 17)
        Me.lnkAdvanceFilter.TabIndex = 69
        Me.lnkAdvanceFilter.TabStop = True
        Me.lnkAdvanceFilter.Text = "Advance Filter"
        Me.lnkAdvanceFilter.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'lblEmpContriHeadType
        '
        Me.lblEmpContriHeadType.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblEmpContriHeadType.Location = New System.Drawing.Point(7, 378)
        Me.lblEmpContriHeadType.Name = "lblEmpContriHeadType"
        Me.lblEmpContriHeadType.Size = New System.Drawing.Size(94, 15)
        Me.lblEmpContriHeadType.TabIndex = 60
        Me.lblEmpContriHeadType.Text = "Emp. Contribution"
        Me.lblEmpContriHeadType.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.lblEmpContriHeadType.Visible = False
        '
        'lblMembership
        '
        Me.lblMembership.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblMembership.Location = New System.Drawing.Point(9, 36)
        Me.lblMembership.Name = "lblMembership"
        Me.lblMembership.Size = New System.Drawing.Size(94, 15)
        Me.lblMembership.TabIndex = 64
        Me.lblMembership.Text = "Membership"
        Me.lblMembership.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'cboCoHeadType
        '
        Me.cboCoHeadType.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboCoHeadType.DropDownWidth = 180
        Me.cboCoHeadType.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboCoHeadType.FormattingEnabled = True
        Me.cboCoHeadType.Location = New System.Drawing.Point(324, 376)
        Me.cboCoHeadType.Name = "cboCoHeadType"
        Me.cboCoHeadType.Size = New System.Drawing.Size(118, 21)
        Me.cboCoHeadType.TabIndex = 61
        Me.cboCoHeadType.Visible = False
        '
        'cboMembership
        '
        Me.cboMembership.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboMembership.DropDownWidth = 180
        Me.cboMembership.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboMembership.FormattingEnabled = True
        Me.cboMembership.Location = New System.Drawing.Point(109, 33)
        Me.cboMembership.Name = "cboMembership"
        Me.cboMembership.Size = New System.Drawing.Size(118, 21)
        Me.cboMembership.TabIndex = 0
        '
        'lblPeriod
        '
        Me.lblPeriod.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblPeriod.Location = New System.Drawing.Point(7, 368)
        Me.lblPeriod.Name = "lblPeriod"
        Me.lblPeriod.Size = New System.Drawing.Size(94, 15)
        Me.lblPeriod.TabIndex = 57
        Me.lblPeriod.Text = "Period"
        Me.lblPeriod.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.lblPeriod.Visible = False
        '
        'cboPeriod
        '
        Me.cboPeriod.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboPeriod.DropDownWidth = 180
        Me.cboPeriod.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboPeriod.FormattingEnabled = True
        Me.cboPeriod.Location = New System.Drawing.Point(107, 407)
        Me.cboPeriod.Name = "cboPeriod"
        Me.cboPeriod.Size = New System.Drawing.Size(118, 21)
        Me.cboPeriod.TabIndex = 58
        Me.cboPeriod.Visible = False
        '
        'gbBasicSalaryOtherEarning
        '
        Me.gbBasicSalaryOtherEarning.BorderColor = System.Drawing.Color.Black
        Me.gbBasicSalaryOtherEarning.Checked = False
        Me.gbBasicSalaryOtherEarning.CollapseAllExceptThis = False
        Me.gbBasicSalaryOtherEarning.CollapsedHoverImage = Nothing
        Me.gbBasicSalaryOtherEarning.CollapsedNormalImage = Nothing
        Me.gbBasicSalaryOtherEarning.CollapsedPressedImage = Nothing
        Me.gbBasicSalaryOtherEarning.CollapseOnLoad = False
        Me.gbBasicSalaryOtherEarning.Controls.Add(Me.pnlAdvanceTaken)
        Me.gbBasicSalaryOtherEarning.ExpandedHoverImage = Nothing
        Me.gbBasicSalaryOtherEarning.ExpandedNormalImage = Nothing
        Me.gbBasicSalaryOtherEarning.ExpandedPressedImage = Nothing
        Me.gbBasicSalaryOtherEarning.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.gbBasicSalaryOtherEarning.GradientColor = System.Drawing.SystemColors.ButtonFace
        Me.gbBasicSalaryOtherEarning.HeaderHeight = 25
        Me.gbBasicSalaryOtherEarning.HeaderMessage = ""
        Me.gbBasicSalaryOtherEarning.HeaderMessageFont = New System.Drawing.Font("Tahoma", 8.25!)
        Me.gbBasicSalaryOtherEarning.HeaderMessageForeColor = System.Drawing.Color.Black
        Me.gbBasicSalaryOtherEarning.HeightOnCollapse = 0
        Me.gbBasicSalaryOtherEarning.LeftTextSpace = 0
        Me.gbBasicSalaryOtherEarning.Location = New System.Drawing.Point(12, 452)
        Me.gbBasicSalaryOtherEarning.Name = "gbBasicSalaryOtherEarning"
        Me.gbBasicSalaryOtherEarning.OpenHeight = 300
        Me.gbBasicSalaryOtherEarning.PanelBorderStyle = System.Windows.Forms.ButtonBorderStyle.Solid
        Me.gbBasicSalaryOtherEarning.ShowBorder = True
        Me.gbBasicSalaryOtherEarning.ShowCheckBox = True
        Me.gbBasicSalaryOtherEarning.ShowCollapseButton = False
        Me.gbBasicSalaryOtherEarning.ShowDefaultBorderColor = True
        Me.gbBasicSalaryOtherEarning.ShowDownButton = False
        Me.gbBasicSalaryOtherEarning.ShowHeader = True
        Me.gbBasicSalaryOtherEarning.Size = New System.Drawing.Size(455, 61)
        Me.gbBasicSalaryOtherEarning.TabIndex = 1
        Me.gbBasicSalaryOtherEarning.Temp = 0
        Me.gbBasicSalaryOtherEarning.Text = "Basic Salary As Other Earning"
        Me.gbBasicSalaryOtherEarning.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'pnlAdvanceTaken
        '
        Me.pnlAdvanceTaken.Controls.Add(Me.objbtnSearchOtherEarning)
        Me.pnlAdvanceTaken.Controls.Add(Me.cboOtherEarning)
        Me.pnlAdvanceTaken.Controls.Add(Me.lblOtherEarning)
        Me.pnlAdvanceTaken.Location = New System.Drawing.Point(2, 26)
        Me.pnlAdvanceTaken.Name = "pnlAdvanceTaken"
        Me.pnlAdvanceTaken.Size = New System.Drawing.Size(450, 33)
        Me.pnlAdvanceTaken.TabIndex = 259
        '
        'objbtnSearchOtherEarning
        '
        Me.objbtnSearchOtherEarning.BackColor = System.Drawing.Color.Transparent
        Me.objbtnSearchOtherEarning.BackColor1 = System.Drawing.Color.Transparent
        Me.objbtnSearchOtherEarning.BackColor2 = System.Drawing.Color.Transparent
        Me.objbtnSearchOtherEarning.BorderNormalColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.objbtnSearchOtherEarning.BorderSelected = False
        Me.objbtnSearchOtherEarning.DialogResult = System.Windows.Forms.DialogResult.None
        Me.objbtnSearchOtherEarning.Image = Global.ArutiReports.My.Resources.Resources.Mini_Search
        Me.objbtnSearchOtherEarning.ImageAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.objbtnSearchOtherEarning.Location = New System.Drawing.Point(289, 3)
        Me.objbtnSearchOtherEarning.Name = "objbtnSearchOtherEarning"
        Me.objbtnSearchOtherEarning.Size = New System.Drawing.Size(21, 21)
        Me.objbtnSearchOtherEarning.TabIndex = 57
        '
        'cboOtherEarning
        '
        Me.cboOtherEarning.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboOtherEarning.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboOtherEarning.FormattingEnabled = True
        Me.cboOtherEarning.Location = New System.Drawing.Point(97, 3)
        Me.cboOtherEarning.Name = "cboOtherEarning"
        Me.cboOtherEarning.Size = New System.Drawing.Size(186, 21)
        Me.cboOtherEarning.TabIndex = 0
        '
        'lblOtherEarning
        '
        Me.lblOtherEarning.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblOtherEarning.Location = New System.Drawing.Point(8, 8)
        Me.lblOtherEarning.Name = "lblOtherEarning"
        Me.lblOtherEarning.Size = New System.Drawing.Size(83, 16)
        Me.lblOtherEarning.TabIndex = 0
        Me.lblOtherEarning.Text = "Other Earning"
        Me.lblOtherEarning.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'frmNSSFForm5Report
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(765, 583)
        Me.Controls.Add(Me.gbBasicSalaryOtherEarning)
        Me.Controls.Add(Me.gbMandatoryInfo)
        Me.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.MaximizeBox = False
        Me.MinimizeBox = False
        Me.Name = "frmNSSFForm5Report"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent
        Me.Controls.SetChildIndex(Me.NavPanel, 0)
        Me.Controls.SetChildIndex(Me.gbMandatoryInfo, 0)
        Me.Controls.SetChildIndex(Me.gbBasicSalaryOtherEarning, 0)
        Me.gbMandatoryInfo.ResumeLayout(False)
        Me.gbMandatoryInfo.PerformLayout()
        Me.gbBasicSalaryOtherEarning.ResumeLayout(False)
        Me.pnlAdvanceTaken.ResumeLayout(False)
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents gbMandatoryInfo As eZee.Common.eZeeCollapsibleContainer
    Friend WithEvents cboEmpHeadType As System.Windows.Forms.ComboBox
    Friend WithEvents cboCoHeadType As System.Windows.Forms.ComboBox
    Friend WithEvents lblEmpContriHeadType As System.Windows.Forms.Label
    Friend WithEvents lblCompanyHeadType As System.Windows.Forms.Label
    Friend WithEvents lblPeriod As System.Windows.Forms.Label
    Friend WithEvents cboPeriod As System.Windows.Forms.ComboBox
    Friend WithEvents lblMembership As System.Windows.Forms.Label
    Friend WithEvents cboMembership As System.Windows.Forms.ComboBox
    Friend WithEvents lnkAdvanceFilter As System.Windows.Forms.LinkLabel
    Friend WithEvents chkShowBasicSal As System.Windows.Forms.CheckBox
    Friend WithEvents LblCurrency As System.Windows.Forms.Label
    Friend WithEvents cboCurrency As System.Windows.Forms.ComboBox
    Friend WithEvents objlblExRate As System.Windows.Forms.Label
    Friend WithEvents lblIncludeMembership As System.Windows.Forms.Label
    Friend WithEvents cboIncludeMembership As System.Windows.Forms.ComboBox
    Friend WithEvents lblVOLHead As System.Windows.Forms.Label
    Friend WithEvents cboVOLHead As System.Windows.Forms.ComboBox
    Friend WithEvents gbBasicSalaryOtherEarning As eZee.Common.eZeeCollapsibleContainer
    Friend WithEvents pnlAdvanceTaken As System.Windows.Forms.Panel
    Friend WithEvents objbtnSearchOtherEarning As eZee.Common.eZeeGradientButton
    Friend WithEvents cboOtherEarning As System.Windows.Forms.ComboBox
    Friend WithEvents lblOtherEarning As System.Windows.Forms.Label
    Friend WithEvents chkIgnoreZero As System.Windows.Forms.CheckBox
    Friend WithEvents objCheckAll As System.Windows.Forms.CheckBox
    Friend WithEvents lblPeriodList As System.Windows.Forms.Label
    Friend WithEvents lvPeriodList As System.Windows.Forms.ListView
    Friend WithEvents objcolhCheck As System.Windows.Forms.ColumnHeader
    Friend WithEvents colhPeriodName As System.Windows.Forms.ColumnHeader
    Friend WithEvents dtpDate As System.Windows.Forms.DateTimePicker
    Friend WithEvents lblAsOnDate As System.Windows.Forms.Label
    Friend WithEvents LblKRAPin As System.Windows.Forms.Label
    Friend WithEvents cboKRAPIN As System.Windows.Forms.ComboBox
    Friend WithEvents objcolhPeriodStart As System.Windows.Forms.ColumnHeader
    Friend WithEvents cboIdentity As System.Windows.Forms.ComboBox
    Friend WithEvents lblIdentity As System.Windows.Forms.Label
    Friend WithEvents btnSaveSelection As eZee.Common.eZeeLightButton
    Friend WithEvents lnkExportDataxlsx As System.Windows.Forms.LinkLabel
End Class
