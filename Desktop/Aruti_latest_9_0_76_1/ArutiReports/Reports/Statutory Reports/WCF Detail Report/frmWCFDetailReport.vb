﻿#Region " Imports "

Imports eZeeCommonLib
Imports Aruti.Data

#End Region
Public Class frmWCFDetailReport

#Region " Private Variables "

    Private ReadOnly mstrModuleName As String = "frmWCFDetailReport"
    Private objWCFDetailReport As clsWCFDetailReport
    Private mintFirstOpenPeriod As Integer = 0
    Private mstrBaseCurrSign As String = ""
    Private mintBaseCurrId As Integer = 0
    Private mintPaidCurrencyId As Integer = 0
    Private mdecBaseExRate As Decimal = 0
    Private mdecPaidExRate As Decimal = 0
    Private eReport As enArutiReport
#End Region

#Region " Private Enum "
    Private Enum enHeadTypeId
        Currency = 1
        Basic_Pay = 2
        Gross_Pay_Formula = 3
        WCF = 4
        National_Id = 5
    End Enum
#End Region

#Region " Form's Events "
    Private Sub frmWCFDetailReport_FormClosed(ByVal sender As Object, ByVal e As System.Windows.Forms.FormClosedEventArgs) Handles Me.FormClosed
        Try
            objWCFDetailReport = Nothing
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmWCFDetailReport_FormClosed", mstrModuleName)
        End Try
    End Sub

    Private Sub frmWCFDetailReport_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            Call Language.setLanguage(Me.Name)
            OtherSettings()
            Call FillCombo()
            Call ResetValue()

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmWCFDetailReport_Load", mstrModuleName)
        End Try
    End Sub

    Private Sub frmWCFDetailReport_KeyDown(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles Me.KeyDown
        Try
            If e.Control Then
                If e.KeyCode = Windows.Forms.Keys.R Then
                End If
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmWCFDetailReport_KeyDown", mstrModuleName)
        End Try
    End Sub

    Private Sub frmWCFDetailReport_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles Me.KeyPress
        Try
            Select Case e.KeyChar
                Case CChar(ChrW(CInt(Windows.Forms.Keys.Enter)))
                    SendKeys.Send("{TAB}")
                    Exit Select
            End Select
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmWCFDetailReport_KeyPress", mstrModuleName)
        End Try

    End Sub

    Private Sub objbtnLanguage_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnLanguage.Click
        Dim objfrm As New frmLanguage
        Try
            If User._Object._Isrighttoleft = True Then
                objfrm.RightToLeft = Windows.Forms.RightToLeft.Yes
                objfrm.RightToLeftLayout = True
                Call Language.ctlRightToLeftlayOut(objfrm)
            End If

            Call SetMessages()

            clsExecutedTrainingsReport.SetMessages()
            objfrm._Other_ModuleNames = "clsWCFDetailReport"
            objfrm.displayDialog(Me)

            Call Language.setLanguage(Me.Name)
            Call SetLanguage()

        Catch ex As System.Exception
            Call DisplayError.Show(-1, ex.Message, "objbtnLanguage_Click", mstrModuleName)
        Finally
            objfrm.Dispose()
            objfrm = Nothing
        End Try
    End Sub
#End Region

#Region " Private Function "

    Private Sub FillCombo()
        Dim objMaster As New clsMasterData
        Dim objCMaster As New clsCommon_Master
        Dim objTransactionHead As New clsTransactionHead
        Dim objperiod As New clscommom_period_Tran
        Dim dsCombos As New DataSet

        Try
            mintFirstOpenPeriod = objMaster.getFirstPeriodID(enModuleReference.Payroll, FinancialYear._Object._YearUnkid, 1)

            dsCombos = objperiod.getListForCombo(enModuleReference.Payroll, FinancialYear._Object._YearUnkid, FinancialYear._Object._DatabaseName, FinancialYear._Object._Database_Start_Date, "Period", True)

            With cboPeriod
                .ValueMember = "periodunkid"
                .DisplayMember = "name"
                .DataSource = dsCombos.Tables(0)
                .SelectedValue = mintFirstOpenPeriod
            End With

            dsCombos = objCMaster.getComboList(clsCommon_Master.enCommonMaster.IDENTITY_TYPES, True, "List")
            With cboWCF
                .ValueMember = "masterunkid"
                .DisplayMember = "name"
                .DataSource = dsCombos.Tables("List")
                .SelectedValue = 0
            End With

            With cboNationalID
                .ValueMember = "masterunkid"
                .DisplayMember = "name"
                .DataSource = dsCombos.Tables("List").Copy
                .SelectedValue = 0
            End With

            dsCombos = objTransactionHead.getComboList(FinancialYear._Object._DatabaseName, "Head", True, , , , , )
            Dim dt As DataTable = New DataView(dsCombos.Tables("Head"), "", "", DataViewRowState.CurrentRows).ToTable
            With cboBasicPay
                .ValueMember = "tranheadunkid"
                .DisplayMember = "Name"
                .DataSource = dt
                .SelectedValue = 0
            End With

            Dim objCurrency As New clsExchangeRate
            dsCombos = objCurrency.getComboList("Currency", True)
            cboCurrency.ValueMember = "countryunkid"
            cboCurrency.DisplayMember = "currency_sign"
            cboCurrency.DataSource = dsCombos.Tables("Currency")

            Dim dtTable As DataTable = New DataView(dsCombos.Tables("Currency"), "isbasecurrency = 1 ", "", DataViewRowState.CurrentRows).ToTable
            If dtTable.Rows.Count > 0 Then
                mstrBaseCurrSign = dtTable.Rows(0).Item("currency_sign").ToString
                mintBaseCurrId = CInt(dtTable.Rows(0).Item("exchangerateunkid"))
            Else
                mstrBaseCurrSign = ""
                mintBaseCurrId = 0
            End If

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "FillCombo", mstrModuleName)
        Finally
            objCMaster = Nothing
            objTransactionHead = Nothing
        End Try
    End Sub

    Private Sub ResetValue()
        Try
            cboPeriod.SelectedValue = mintFirstOpenPeriod
            cboWCF.SelectedValue = 0
            cboBasicPay.SelectedIndex = 0
            cboNationalID.SelectedIndex = 0
            mintPaidCurrencyId = 0
            mdecBaseExRate = 0
            mdecPaidExRate = 0
            txtGrossPayFormula.Text = ""
            objWCFDetailReport.setDefaultOrderBy(0)
            Call GetValue()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "ResetValue", mstrModuleName)
        End Try
    End Sub

    Public Function SetFilter() As Boolean
        Try
            If CInt(cboPeriod.SelectedValue) <= 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 1, "Period is mandatory information. Please select Period to continue."), enMsgBoxStyle.Information)
                cboPeriod.Focus()
                Return False
            End If

            objWCFDetailReport.SetDefaultValue()

            objWCFDetailReport._PeriodId = CInt(cboPeriod.SelectedValue)
            objWCFDetailReport._PeriodName = cboPeriod.Text

            If (mintBaseCurrId = mintPaidCurrencyId) Or mintPaidCurrencyId <= 0 Then
                mintPaidCurrencyId = mintBaseCurrId
            End If
            objWCFDetailReport._CountryId = CInt(cboCurrency.SelectedValue)
            objWCFDetailReport._BaseCurrencyId = mintBaseCurrId
            objWCFDetailReport._PaidCurrencyId = mintPaidCurrencyId
            If mdecBaseExRate > 0 AndAlso mdecPaidExRate Then
                objWCFDetailReport._ConversionRate = mdecPaidExRate / mdecBaseExRate
            End If

            objWCFDetailReport._WCFId = cboWCF.SelectedValue
            objWCFDetailReport._WCFName = cboWCF.Text
            objWCFDetailReport._BasicPayHeadId = cboBasicPay.SelectedValue
            objWCFDetailReport._IdentityId = cboNationalID.SelectedValue
            objWCFDetailReport._IdentityName = cboNationalID.Text

            objWCFDetailReport._GrossPayFormula = IIf(txtGrossPayFormula.Text.Trim.Length <= 0, "0", txtGrossPayFormula.Text)
            Return True
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "SetFilter", mstrModuleName)
        End Try
    End Function

    Private Sub GetValue()
        Dim objUserDefRMode As New clsUserDef_ReportMode
        Dim dsList As DataSet
        Try
            dsList = objUserDefRMode.GetList("List", enArutiReport.WCF_Detail_Report)

            If dsList.Tables("List").Rows.Count > 0 Then

                For Each dsRow As DataRow In dsList.Tables("List").Rows
                    Select Case CInt(dsRow.Item("headtypeid"))

                        Case enHeadTypeId.Currency
                            cboCurrency.SelectedValue = CInt(dsRow.Item("transactionheadid"))


                        Case enHeadTypeId.WCF
                            cboWCF.SelectedValue = CInt(dsRow.Item("transactionheadid"))

                        Case enHeadTypeId.Basic_Pay
                            cboBasicPay.SelectedValue = CInt(dsRow.Item("transactionheadid"))

                        Case enHeadTypeId.Gross_Pay_Formula
                            txtGrossPayFormula.Text = dsRow.Item("transactionheadid").ToString

                        Case enHeadTypeId.National_Id
                            cboNationalID.SelectedValue = CInt(dsRow.Item("transactionheadid"))


                    End Select
                Next

            End If

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "GetValue", mstrModuleName)
        Finally
            objUserDefRMode = Nothing
        End Try
    End Sub

#End Region

#Region " Constructor "
    Public Sub New(ByVal menReport As enArutiReport)
        eReport = menReport

        objWCFDetailReport = New clsWCFDetailReport(User._Object._Languageunkid, Company._Object._Companyunkid)
        objWCFDetailReport.SetDefaultValue()
        InitializeComponent()
    End Sub
#End Region

#Region " Buttons Events "
    Private Sub btnClose_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnClose.Click
        Try
            Me.Close()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "btnClose_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub btnReset_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnReset.Click
        Try
            Call ResetValue()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "btnReset_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub btnExport_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnExport.Click
        Try

            If SetFilter() = False Then Exit Sub

            objWCFDetailReport.Generate_DetailReport(FinancialYear._Object._DatabaseName, _
                                                     User._Object._Userunkid, _
                                                     FinancialYear._Object._YearUnkid, _
                                                     Company._Object._Companyunkid, _
                                                     eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate.ToString), _
                                                     eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate.ToString), _
                                                     ConfigParameter._Object._UserAccessModeSetting, _
                                                     True, False, ConfigParameter._Object._ExportReportPath, _
                                                     ConfigParameter._Object._OpenAfterExport, _
                                                     0, enExportAction.None)



        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "btnExport_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub btnSaveSelection_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSaveSelection.Click
        Dim objUserDefRMode As New clsUserDef_ReportMode
        Try
            If SetFilter() = False Then Exit Try

            For intHeadType As Integer = 1 To 5
                objUserDefRMode = New clsUserDef_ReportMode()

                Dim intUnkid As Integer = -1

                objUserDefRMode._Reportunkid = enArutiReport.WCF_Detail_Report
                objUserDefRMode._Reporttypeid = 0
                objUserDefRMode._Reportmodeid = 0

                Select Case intHeadType

                    Case enHeadTypeId.Currency
                        objUserDefRMode._Headtypeid = intHeadType
                        objUserDefRMode._EarningTranHeadIds = cboCurrency.SelectedValue.ToString

                        intUnkid = objUserDefRMode.isExist(eReport, 0, 0, intHeadType)

                    Case enHeadTypeId.WCF
                        objUserDefRMode._Headtypeid = intHeadType
                        objUserDefRMode._EarningTranHeadIds = cboWCF.SelectedValue.ToString

                        intUnkid = objUserDefRMode.isExist(eReport, 0, 0, intHeadType)

                    Case enHeadTypeId.Basic_Pay
                        objUserDefRMode._Headtypeid = intHeadType
                        objUserDefRMode._EarningTranHeadIds = cboBasicPay.SelectedValue.ToString

                        intUnkid = objUserDefRMode.isExist(eReport, 0, 0, intHeadType)

                    Case enHeadTypeId.Gross_Pay_Formula
                        objUserDefRMode._Headtypeid = intHeadType
                        objUserDefRMode._EarningTranHeadIds = txtGrossPayFormula.Text.Trim

                        intUnkid = objUserDefRMode.isExist(eReport, 0, 0, intHeadType)

                    Case enHeadTypeId.National_Id
                        objUserDefRMode._Headtypeid = intHeadType
                        objUserDefRMode._EarningTranHeadIds = cboNationalID.SelectedValue.ToString

                        intUnkid = objUserDefRMode.isExist(eReport, 0, 0, intHeadType)


                End Select

                objUserDefRMode._Reportmodeunkid = intUnkid

                If intUnkid <= 0 Then
                    objUserDefRMode.Insert()
                Else
                    objUserDefRMode.Update()
                End If

            Next

            eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 6, "Selection Saved Successfully"), enMsgBoxStyle.Information)

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "btnSaveSelection_Click", mstrModuleName)
        Finally
            objUserDefRMode = Nothing
        End Try
    End Sub
#End Region

#Region "ComboBox Event"
    Private Sub cboCurrency_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cboCurrency.SelectedIndexChanged
        Try
            objlblExRate.Text = ""
            mdecBaseExRate = 0
            mdecPaidExRate = 0
            If CInt(cboCurrency.SelectedValue) > 0 Then
                Dim objExRate As New clsExchangeRate
                Dim objPeriod As New clscommom_period_Tran
                objPeriod._Periodunkid(FinancialYear._Object._DatabaseName) = CInt(cboPeriod.SelectedValue)
                Dim ds As DataSet = objExRate.GetList("ExRate", True, , , CInt(cboCurrency.SelectedValue), True, objPeriod._End_Date.Date, True)
                If ds.Tables("ExRate").Rows.Count > 0 Then
                    mdecBaseExRate = CDec(ds.Tables("ExRate").Rows(0).Item("exchange_rate1"))
                    mdecPaidExRate = CDec(ds.Tables("ExRate").Rows(0).Item("exchange_rate2"))
                    mintPaidCurrencyId = CInt(ds.Tables("ExRate").Rows(0).Item("exchangerateunkid"))
                    objlblExRate.Text = Format(mdecBaseExRate, GUI.fmtCurrency) & " " & mstrBaseCurrSign & " = " & mdecPaidExRate.ToString & " " & ds.Tables("ExRate").Rows(0).Item("currency_sign").ToString & " "
                End If
            End If

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "cboCurrency_SelectedIndexChanged", mstrModuleName)
        End Try
    End Sub

    Private Sub cboPeriod_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cboPeriod.SelectedIndexChanged, cboCurrency.SelectedIndexChanged
        Try
            If CInt(cboPeriod.SelectedValue) > 0 AndAlso CInt(cboCurrency.SelectedValue) > 0 Then
                Dim objExRate As New clsExchangeRate
                Dim objPeriod As New clscommom_period_Tran
                objPeriod._Periodunkid(FinancialYear._Object._DatabaseName) = CInt(cboPeriod.SelectedValue)
                Dim dsList As DataSet = Nothing
                dsList = objExRate.GetList("ExRate", True, , , CInt(cboCurrency.SelectedValue), True, objPeriod._End_Date.Date, True)
                Dim dtTable As DataTable = New DataView(dsList.Tables("ExRate")).ToTable

                If dtTable.Rows.Count > 0 Then
                    mdecBaseExRate = CDec(dtTable.Rows(0).Item("exchange_rate1"))
                    mdecPaidExRate = CDec(dtTable.Rows(0).Item("exchange_rate2"))
                    mintPaidCurrencyId = CInt(dtTable.Rows(0).Item("exchangerateunkid"))
                    objlblExRate.Text = Format(mdecBaseExRate, GUI.fmtCurrency) & " " & mstrBaseCurrSign & " = " & mdecPaidExRate.ToString & " " & dtTable.Rows(0).Item("currency_sign").ToString & " "
                Else
                    mdecBaseExRate = 0
                    mdecPaidExRate = 0
                    mintPaidCurrencyId = 0
                    objlblExRate.Text = ""
                End If
            Else
                mdecBaseExRate = 0
                mdecPaidExRate = 0
                mintPaidCurrencyId = 0
                objlblExRate.Text = ""
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "cboPeriod_SelectedIndexChanged", mstrModuleName)
        End Try
    End Sub
#End Region

	'<Language> This Auto Generated Text Please Do Not Modify it.
#Region " Language & UI Settings "
	Private Sub OtherSettings()
		Try
			Me.SuspendLayout()
			
			Call SetLanguage()
			
			Me.gbFilterCriteria.GradientColor = GUI._eZeeContainerHeaderBackColor
			Me.gbFilterCriteria.ForeColor = GUI._eZeeContainerHeaderForeColor

            Me.eZeeHeader.GradientColor1 = GUI._HeaderBackColor1
			Me.eZeeHeader.GradientColor2 = GUI._HeaderBackColor2
			Me.eZeeHeader.BorderColor = GUI._HeaderBorderColor
			Me.eZeeHeader.HeaderTextForeColor = GUI._HeaderTitleFontColor
			Me.eZeeHeader.DescriptionForeColor = GUI._HeaderMessageFontColor

			Me.btnReset.GradientBackColor = GUI._ButttonBackColor
			Me.btnReset.GradientForeColor = GUI._ButttonFontColor

			Me.btnExport.GradientBackColor = GUI._ButttonBackColor
			Me.btnExport.GradientForeColor = GUI._ButttonFontColor

			Me.btnClose.GradientBackColor = GUI._ButttonBackColor
			Me.btnClose.GradientForeColor = GUI._ButttonFontColor

			Me.objbtnLanguage.GradientBackColor = GUI._ButttonBackColor
			Me.objbtnLanguage.GradientForeColor = GUI._ButttonFontColor

			Me.ResumeLayout()
		Catch Ex As Exception
            DisplayError.Show("-1", Ex.Message, "OtherSettings", mstrModuleName)
		End Try
	End Sub
			
			
	Private Sub SetLanguage()
		Try
			Me.Text = Language._Object.getCaption(Me.Name, Me.Text)
			
			Me.btnReset.Text = Language._Object.getCaption(Me.btnReset.Name, Me.btnReset.Text)
			Me.btnExport.Text = Language._Object.getCaption(Me.btnExport.Name, Me.btnExport.Text)
			Me.btnClose.Text = Language._Object.getCaption(Me.btnClose.Name, Me.btnClose.Text)
			Me.gbFilterCriteria.Text = Language._Object.getCaption(Me.gbFilterCriteria.Name, Me.gbFilterCriteria.Text)
			Me.lblBasicPay.Text = Language._Object.getCaption(Me.lblBasicPay.Name, Me.lblBasicPay.Text)
			Me.eZeeHeader.Title = Language._Object.getCaption(Me.eZeeHeader.Name & "_Title" , Me.eZeeHeader.Title)
			Me.eZeeHeader.Message = Language._Object.getCaption(Me.eZeeHeader.Name & "_Message" , Me.eZeeHeader.Message)
			Me.lblWCF.Text = Language._Object.getCaption(Me.lblWCF.Name, Me.lblWCF.Text)
            Me.lblNationalID.Text = Language._Object.getCaption(Me.lblNationalID.Name, Me.lblNationalID.Text)
			Me.lblPeriod.Text = Language._Object.getCaption(Me.lblPeriod.Name, Me.lblPeriod.Text)
			
		Catch Ex As Exception
            DisplayError.Show("-1", Ex.Message, "SetLanguage", mstrModuleName)
		End Try
	End Sub
			
			
	Public Sub SetMessages()
		Try
			Language.setMessage(mstrModuleName, 1, "Period is mandatory information. Please select Period to continue.")
			
		Catch Ex As Exception
            DisplayError.Show("-1", Ex.Message, "SetMessages", mstrModuleName)
		End Try
	End Sub
#End Region 'Language & UI Settings
	'</Language>

    
End Class