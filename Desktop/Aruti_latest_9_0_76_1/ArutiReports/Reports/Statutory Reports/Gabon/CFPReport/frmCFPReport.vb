﻿'************************************************************************************************************************************
'Class Name : frmCFPReport.vb
'Purpose    : 
'Written By : Sohail
'Modified   : 
'************************************************************************************************************************************

#Region " Imports "

Imports eZeeCommonLib
Imports Aruti.Data

#End Region

Public Class frmCFPReport

#Region " Private Variables "

    Private ReadOnly mstrModuleName As String = "frmCFPReport"
    Private objCFPReport As clsCFPReport
    Private mintFirstPeriodId As Integer = 0
    Private mintLastPeriodId As Integer = 0

    Private mstrStringIds As String = String.Empty
    Private mstrStringName As String = String.Empty
    Private mintViewIdx As Integer = 0
    Private mstrAnalysis_Fields As String = ""
    Private mstrAnalysis_Join As String = ""
    Private mstrAnalysis_OrderBy As String = ""
    Private mstrReport_GroupName As String = ""

    Private mdtPeriodEndDate As DateTime

#End Region

#Region " Contructor "

    Public Sub New()
        objCFPReport = New clsCFPReport(User._Object._Languageunkid, Company._Object._Companyunkid)
        objCFPReport.SetDefaultValue()
        InitializeComponent()
        _Show_ExcelExtra_Menu = True
    End Sub

#End Region

#Region " Private Enum "
    Public Enum enHeadTypeId
        BasicSalary = 1
        CashBenefit = 2
        CNSSContribution = 3
        OtherBenefit = 4
        BaseCFP = 5
        Rate = 6
    End Enum
#End Region

#Region " Private Function "

    Private Sub FillCombo()
        Dim objPeriod As New clscommom_period_Tran
        Dim objTranHead As New clsTransactionHead
        Dim objMaster As New clsMasterData

        Dim objMember As New clsmembership_master


        Dim dsCombos As DataSet

        Try


            mintFirstPeriodId = objMaster.getFirstPeriodID(enModuleReference.Payroll, FinancialYear._Object._YearUnkid, 1)

            dsCombos = objPeriod.getListForCombo(enModuleReference.Payroll, FinancialYear._Object._YearUnkid, FinancialYear._Object._DatabaseName, FinancialYear._Object._Database_Start_Date, "Period", True)


            With cboPeriod
                .ValueMember = "periodunkid"
                .DisplayMember = "name"
                .DataSource = dsCombos.Tables(0)
                .SelectedValue = mintFirstPeriodId
            End With

            dsCombos = objTranHead.getComboList(FinancialYear._Object._DatabaseName, "Heads", True, , , , , , )
            With cboBasicSalary
                .ValueMember = "tranheadunkid"
                .DisplayMember = "name"
                .DataSource = dsCombos.Tables(0)
                .SelectedValue = 0
            End With

            With cboCashBenefit
                .ValueMember = "tranheadunkid"
                .DisplayMember = "name"
                .DataSource = dsCombos.Tables(0).Copy
                .SelectedValue = 0
            End With

            With cboCNSSContribution
                .ValueMember = "tranheadunkid"
                .DisplayMember = "name"
                .DataSource = dsCombos.Tables(0).Copy
                .SelectedValue = 0
            End With

            With cboOtherBenefit
                .ValueMember = "tranheadunkid"
                .DisplayMember = "name"
                .DataSource = dsCombos.Tables(0).Copy
                .SelectedValue = 0
            End With

            With cboBaseCFP
                .ValueMember = "tranheadunkid"
                .DisplayMember = "name"
                .DataSource = dsCombos.Tables(0).Copy
                .SelectedValue = 0
            End With


        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "FillCombo", mstrModuleName)
        Finally
            objPeriod = Nothing
            objTranHead = Nothing
            objMaster = Nothing
        End Try
    End Sub

    Private Sub ResetValue()
        Try
            cboPeriod.SelectedValue = mintFirstPeriodId
            cboBasicSalary.SelectedValue = 0
            cboCashBenefit.SelectedValue = 0
            cboCNSSContribution.SelectedValue = 0
            cboOtherBenefit.SelectedValue = 0
            cboBaseCFP.SelectedValue = 0
            txtRate.Decimal = 0


            mstrStringIds = String.Empty
            mstrStringName = String.Empty
            mintViewIdx = 0
            mstrAnalysis_Fields = ""
            mstrAnalysis_Join = ""
            mstrAnalysis_OrderBy = ""
            mstrReport_GroupName = ""

            Call GetValue()

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "ResetValue", mstrModuleName)
        End Try
    End Sub

    Private Sub GetValue()
        Dim objUserDefRMode As New clsUserDef_ReportMode
        Dim dsList As DataSet
        Try
            dsList = objUserDefRMode.GetList("List", enArutiReport.CFP_Report)

            If dsList.Tables("List").Rows.Count > 0 Then

                For Each dsRow As DataRow In dsList.Tables("List").Rows
                    Select Case CInt(dsRow.Item("headtypeid"))

                        Case enHeadTypeId.BasicSalary
                            cboBasicSalary.SelectedValue = CInt(dsRow.Item("transactionheadid"))

                        Case enHeadTypeId.CashBenefit
                            cboCashBenefit.SelectedValue = CInt(dsRow.Item("transactionheadid"))

                        Case enHeadTypeId.CNSSContribution
                            cboCNSSContribution.SelectedValue = CInt(dsRow.Item("transactionheadid"))

                        Case enHeadTypeId.OtherBenefit
                            cboOtherBenefit.SelectedValue = CInt(dsRow.Item("transactionheadid"))

                        Case enHeadTypeId.BaseCFP
                            cboBaseCFP.SelectedValue = CInt(dsRow.Item("transactionheadid"))

                        Case enHeadTypeId.Rate
                            txtRate.Decimal = CDec(dsRow.Item("transactionheadid"))


                    End Select
                Next

            End If

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "GetValue", mstrModuleName)
        Finally
            objUserDefRMode = Nothing
        End Try
    End Sub

    Private Function SetFilter() As Boolean
        Dim objCompany As New clsCompany_Master
        Dim dsList As DataSet
        Dim intPrevYearID As Integer = 0
        Dim mdicYearDBName As New Dictionary(Of Integer, String)
        'Sohail (18 Jan 2017) -- End
        Try
            objCFPReport.SetDefaultValue()


            If CInt(cboPeriod.SelectedValue) <= 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 1, "From Period is mandatory information. Please select From Period."), enMsgBoxStyle.Information)
                cboPeriod.Focus()
                Return False

            ElseIf CInt(cboBasicSalary.SelectedValue) <= 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 2, "Please select head for Basic Salary."), enMsgBoxStyle.Information)
                cboBasicSalary.Focus()
                Return False
            ElseIf CInt(cboCashBenefit.SelectedValue) <= 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 3, "Please select head for Cash Benefit."), enMsgBoxStyle.Information)
                cboCashBenefit.Focus()
                Return False
            ElseIf CInt(cboCNSSContribution.SelectedValue) <= 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 4, "Please select head for CNSS Contribution."), enMsgBoxStyle.Information)
                cboCNSSContribution.Focus()
                Return False
            ElseIf CInt(cboOtherBenefit.SelectedValue) <= 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 5, "Please select head for Other Benefit."), enMsgBoxStyle.Information)
                cboOtherBenefit.Focus()
                Return False
            ElseIf CInt(cboBaseCFP.SelectedValue) <= 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 6, "Please select head for Base CFP."), enMsgBoxStyle.Information)
                cboBaseCFP.Focus()
                Return False
            ElseIf CDec(txtRate.Decimal) <= 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 7, "Please enter Rate."), enMsgBoxStyle.Information)
                txtRate.Focus()
                Return False

            End If



            Dim i As Integer = 0

            Dim strPreriodIds As String = cboPeriod.SelectedValue.ToString
            Dim mstrPeriodsName As String = cboPeriod.Text

            objCFPReport._PeriodIds = strPreriodIds
            objCFPReport._PeriodNames = mstrPeriodsName

            objCFPReport._LastPeriodYearName = eZeeDate.convertDate(CType(cboPeriod.SelectedItem, DataRowView).Item("end_date").ToString).Year.ToString

            objCFPReport._BasicSalaryHeadId = CInt(cboBasicSalary.SelectedValue)
            objCFPReport._BasicSalaryHeadName = cboBasicSalary.Text

            objCFPReport._CashBenefitHeadId = CInt(cboCashBenefit.SelectedValue)
            objCFPReport._CashBenefitHeadName = cboCashBenefit.Text

            objCFPReport._CNSSContributionHeadId = CInt(cboCNSSContribution.SelectedValue)
            objCFPReport._CNSSContributionHeadName = cboCNSSContribution.Text

            objCFPReport._OtherBenefitHeadId = CInt(cboOtherBenefit.SelectedValue)
            objCFPReport._OtherBenefitHeadName = cboOtherBenefit.Text

            objCFPReport._BaseCFPHeadId = CInt(cboBaseCFP.SelectedValue)
            objCFPReport._BaseCFPHeadName = cboBaseCFP.Text

            objCFPReport._Rate = txtRate.Decimal


            objCFPReport._ViewByIds = mstrStringIds
            objCFPReport._ViewIndex = mintViewIdx
            objCFPReport._ViewByName = mstrStringName
            objCFPReport._Analysis_Fields = mstrAnalysis_Fields
            objCFPReport._Analysis_Join = mstrAnalysis_Join
            objCFPReport._Analysis_OrderBy = mstrAnalysis_OrderBy
            objCFPReport._Report_GroupName = mstrReport_GroupName


            Return True

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "SetFilter", mstrModuleName)
        End Try
    End Function

#End Region

#Region " Forms "

    Private Sub frmCFPReport_FormClosed(ByVal sender As Object, ByVal e As System.Windows.Forms.FormClosedEventArgs) Handles Me.FormClosed
        Try
            objCFPReport = Nothing
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmCFPReport_FormClosed", mstrModuleName)
        End Try
    End Sub

    Private Sub frmCFPReport_KeyDown(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles Me.KeyDown
        Try
            If e.KeyCode = Keys.Return Then
                Windows.Forms.SendKeys.Send("{TAB}")
                e.Handled = True
            ElseIf e.Control AndAlso e.KeyCode = Keys.R Then
                Call frmCFPReport_Report_Click(Me, New Aruti.Data.PrintButtonEventArgs(enPrintAction.Preview))
            End If

        Catch ex As Exception
            DisplayError.Show(-1, ex.Message, "frmCFPReport_KeyDown", mstrModuleName)
        End Try
    End Sub

    Private Sub frmCFPReport_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            Call Language.setLanguage(Me.Name)

            Call OtherSettings()

            Me._Title = objCFPReport._ReportName
            Me._Message = objCFPReport._ReportDesc

            Call FillCombo()
            Call ResetValue()

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmCFPReport_Load", mstrModuleName)
        End Try
    End Sub

    Private Sub frmCFPReport_Language_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Language_Click
        Dim objfrm As New frmLanguage
        Try
            If User._Object._Isrighttoleft = True Then
                objfrm.RightToLeft = Windows.Forms.RightToLeft.Yes
                objfrm.RightToLeftLayout = True
                Call Language.ctlRightToLeftlayOut(objfrm)
            End If

            Call SetMessages()

            clsCFPReport.SetMessages()
            objfrm._Other_ModuleNames = "clsCFPReport"
            objfrm.displayDialog(Me)

            Call Language.setLanguage(Me.Name)
            Call SetLanguage()

        Catch ex As System.Exception
            Call DisplayError.Show(-1, ex.Message, "frmCFPReport_Language_Click", mstrModuleName)
        Finally
            objfrm.Dispose()
            objfrm = Nothing
        End Try
    End Sub
#End Region

#Region " Buttons "

    Private Sub frmCFPReport_Report_Click(ByVal sender As Object, ByVal e As Aruti.Data.PrintButtonEventArgs) Handles Me.Report_Click
        Try
            If Not SetFilter() Then Exit Sub

            Dim objPeriod As New clscommom_period_Tran
            objPeriod._Periodunkid(FinancialYear._Object._DatabaseName) = CInt(cboPeriod.SelectedValue)

            objCFPReport._Analysis_Join = mstrAnalysis_Join.Replace(ConfigParameter._Object._EmployeeAsOnDate, eZeeDate.convertDate(objPeriod._End_Date))

            objCFPReport.generateReportNew(FinancialYear._Object._DatabaseName, _
                                           User._Object._Userunkid, _
                                           FinancialYear._Object._YearUnkid, _
                                           Company._Object._Companyunkid, _
                                           objPeriod._Start_Date, _
                                           objPeriod._End_Date, _
                                           ConfigParameter._Object._UserAccessModeSetting, _
                                           True, ConfigParameter._Object._ExportReportPath, _
                                           ConfigParameter._Object._OpenAfterExport, 0, enPrintAction.Preview, enExportAction.None, ConfigParameter._Object._Base_CurrencyId)

        Catch ex As Exception
            DisplayError.Show(-1, ex.Message, "frmCFPReport_Report_Click", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub frmCFPReport_Export_Click(ByVal sender As Object, ByVal e As Aruti.Data.PrintButtonEventArgs) Handles Me.Export_Click
        Try
            If Not SetFilter() Then Exit Sub

            Dim objPeriod As New clscommom_period_Tran
            objPeriod._Periodunkid(FinancialYear._Object._DatabaseName) = CInt(cboPeriod.SelectedValue)

            objCFPReport._Analysis_Join = mstrAnalysis_Join.Replace(ConfigParameter._Object._EmployeeAsOnDate, eZeeDate.convertDate(objPeriod._End_Date))

            objCFPReport.generateReportNew(FinancialYear._Object._DatabaseName, _
                                           User._Object._Userunkid, _
                                           FinancialYear._Object._YearUnkid, _
                                           Company._Object._Companyunkid, _
                                           objPeriod._Start_Date, _
                                           objPeriod._End_Date, _
                                           ConfigParameter._Object._UserAccessModeSetting, _
                                           True, ConfigParameter._Object._ExportReportPath, _
                                           ConfigParameter._Object._OpenAfterExport, 0, enPrintAction.None, e.Type, ConfigParameter._Object._Base_CurrencyId)

        Catch ex As Exception
            DisplayError.Show(-1, ex.Message, "frmCFPReport_Export_Click", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub btnSaveSelection_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSaveSelection.Click
        Dim objUserDefRMode As New clsUserDef_ReportMode
        Try

            For intHeadType As Integer = 1 To GetEnumHighestValue(Of enHeadTypeId)()
                objUserDefRMode = New clsUserDef_ReportMode()

                Dim intUnkid As Integer = -1

                objUserDefRMode._Reportunkid = enArutiReport.CFP_Report
                objUserDefRMode._Reporttypeid = 0
                objUserDefRMode._Reportmodeid = 0

                Select Case intHeadType

                    Case enHeadTypeId.BasicSalary
                        objUserDefRMode._Headtypeid = intHeadType
                        objUserDefRMode._EarningTranHeadIds = cboBasicSalary.SelectedValue.ToString

                        intUnkid = objUserDefRMode.isExist(enArutiReport.CFP_Report, 0, 0, intHeadType)

                    Case enHeadTypeId.CashBenefit
                        objUserDefRMode._Headtypeid = intHeadType
                        objUserDefRMode._EarningTranHeadIds = cboCashBenefit.SelectedValue.ToString

                        intUnkid = objUserDefRMode.isExist(enArutiReport.CFP_Report, 0, 0, intHeadType)

                    Case enHeadTypeId.CNSSContribution
                        objUserDefRMode._Headtypeid = intHeadType
                        objUserDefRMode._EarningTranHeadIds = cboCNSSContribution.SelectedValue.ToString

                        intUnkid = objUserDefRMode.isExist(enArutiReport.CFP_Report, 0, 0, intHeadType)

                    Case enHeadTypeId.OtherBenefit
                        objUserDefRMode._Headtypeid = intHeadType
                        objUserDefRMode._EarningTranHeadIds = cboOtherBenefit.SelectedValue.ToString

                        intUnkid = objUserDefRMode.isExist(enArutiReport.CFP_Report, 0, 0, intHeadType)

                    Case enHeadTypeId.BaseCFP
                        objUserDefRMode._Headtypeid = intHeadType
                        objUserDefRMode._EarningTranHeadIds = cboBaseCFP.SelectedValue.ToString

                        intUnkid = objUserDefRMode.isExist(enArutiReport.CFP_Report, 0, 0, intHeadType)

                    Case enHeadTypeId.Rate
                        objUserDefRMode._Headtypeid = intHeadType
                        objUserDefRMode._EarningTranHeadIds = txtRate.Decimal.ToString

                        intUnkid = objUserDefRMode.isExist(enArutiReport.CFP_Report, 0, 0, intHeadType)


                End Select

                objUserDefRMode._Reportmodeunkid = intUnkid

                If intUnkid <= 0 Then
                    objUserDefRMode.Insert()
                Else
                    objUserDefRMode.Update()
                End If

            Next

            eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 8, "Selection Saved Successfully"), enMsgBoxStyle.Information)

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "btnSaveSelection_Click", mstrModuleName)
        Finally
            objUserDefRMode = Nothing
        End Try
    End Sub

    Private Sub frmCFPReport_Reset_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Reset_Click
        Try
            Call ResetValue()

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmCFPReport_Reset_Click", mstrModuleName)
        End Try
    End Sub

#End Region

#Region " Combobox' Events "

    Private Sub cboBasicSalary_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles cboBasicSalary.KeyPress, _
                                                                                                                            cboCashBenefit.KeyPress, _
                                                                                                                            cboCNSSContribution.KeyPress, _
                                                                                                                            cboOtherBenefit.KeyPress, _
                                                                                                                            cboBaseCFP.KeyPress

        Try
            Dim cbo As ComboBox = CType(sender, ComboBox)
            If (CInt(AscW(e.KeyChar)) >= 65 AndAlso CInt(AscW(e.KeyChar)) <= 90) Or (CInt(AscW(e.KeyChar)) >= 97 AndAlso CInt(AscW(e.KeyChar)) <= 122) Or (CInt(AscW(e.KeyChar)) >= 47 AndAlso CInt(AscW(e.KeyChar)) <= 57) Then
                Dim frm As New frmCommonSearch
                With frm
                    .ValueMember = cbo.ValueMember
                    .DisplayMember = cbo.DisplayMember
                    .DataSource = CType(cbo.DataSource, DataTable)
                    .CodeMember = "code"

                End With

                Dim c As Char = Convert.ToChar(e.KeyChar)
                frm.TypedText = c.ToString

                If frm.DisplayDialog Then
                    cbo.SelectedValue = frm.SelectedValue
                    cbo.Tag = frm.SelectedAlias
                    e.KeyChar = ChrW(Keys.ShiftKey)
                Else
                    cbo.Text = ""
                    cbo.Tag = ""
                End If
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, CType(sender, ComboBox).Name & "_KeyPress", mstrModuleName)
        End Try
    End Sub

    'Private Sub cboBasicSalary_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cboBasicSalary.SelectedIndexChanged, _
    '                                                                                                                    cboCashBenefit.SelectedIndexChanged, _
    '                                                                                                                    cboCNSSContribution.SelectedIndexChanged, _
    '                                                                                                                    cboOtherBenefit.SelectedIndexChanged, _
    '                                                                                                                    cboBaseCFP.SelectedIndexChanged

    '    Try

    '    Catch ex As Exception
    '        DisplayError.Show("-1", ex.Message, "", mstrModuleName)
    '    End Try
    'End Sub

    Private Sub cboBasicSalary_Validating(ByVal sender As Object, ByVal e As System.ComponentModel.CancelEventArgs) Handles cboBasicSalary.Validating, _
                                                                                                                        cboCashBenefit.Validating, _
                                                                                                                        cboCNSSContribution.Validating, _
                                                                                                                        cboOtherBenefit.Validating, _
                                                                                                                        cboBaseCFP.Validating

        Try
            Dim cbo As ComboBox = CType(sender, ComboBox)

            If CInt(cbo.SelectedValue) > 0 Then
                Dim lst As IEnumerable(Of ComboBox) = gbFilterCriteria.Controls.OfType(Of ComboBox)().Where(Function(t) t.Name <> cbo.Name AndAlso t.Name <> cboPeriod.Name AndAlso CInt(t.SelectedValue) = CInt(cbo.SelectedValue))
                If lst.Count > 0 Then
                    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 9, "Sorry, This transaction head is already mapped."))
                    cbo.SelectedValue = 0
                    e.Cancel = True
                End If
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, CType(sender, ComboBox).Name & "_Validating", mstrModuleName)
        End Try
    End Sub
#End Region

#Region " Link Button Events "

    Private Sub lnkSetAnalysis_LinkClicked(ByVal sender As System.Object, ByVal e As System.Windows.Forms.LinkLabelLinkClickedEventArgs) Handles lnkSetAnalysis.LinkClicked
        Dim frm As New frmViewAnalysis
        Try
            frm.displayDialog()
            mstrStringIds = frm._ReportBy_Ids
            mstrStringName = frm._ReportBy_Name
            mintViewIdx = frm._ViewIndex
            mstrAnalysis_Fields = frm._Analysis_Fields
            mstrAnalysis_Join = frm._Analysis_Join
            mstrReport_GroupName = frm._Report_GroupName
            mstrAnalysis_OrderBy = frm._Analysis_OrderBy_GName
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "lnkSetAnalysis_LinkClicked", mstrModuleName)
        Finally
            frm = Nothing
        End Try
    End Sub

#End Region

	'<Language> This Auto Generated Text Please Do Not Modify it.
#Region " Language & UI Settings "
	Private Sub OtherSettings()
		Try
			Me.SuspendLayout()
			 
			Call SetLanguage()
			
			Me.gbFilterCriteria.GradientColor = GUI._eZeeContainerHeaderBackColor 
			Me.gbFilterCriteria.ForeColor = GUI._eZeeContainerHeaderForeColor 


			Me.btnSaveSelection.GradientBackColor = GUI._ButttonBackColor 
			Me.btnSaveSelection.GradientForeColor = GUI._ButttonFontColor


			Me.ResumeLayout()
		Catch Ex As Exception
			DisplayError.Show("-1", ex.Message, "OtherSettings", mstrModuleName)
		End Try
	End Sub
			
			
	Private Sub SetLanguage()
		Try
			Me.Text = Language._Object.getCaption(Me.Name, Me.Text)
			
			Me.gbFilterCriteria.Text = Language._Object.getCaption(Me.gbFilterCriteria.Name, Me.gbFilterCriteria.Text)
			Me.lnkSetAnalysis.Text = Language._Object.getCaption(Me.lnkSetAnalysis.Name, Me.lnkSetAnalysis.Text)
			Me.lblBaseCFP.Text = Language._Object.getCaption(Me.lblBaseCFP.Name, Me.lblBaseCFP.Text)
			Me.btnSaveSelection.Text = Language._Object.getCaption(Me.btnSaveSelection.Name, Me.btnSaveSelection.Text)
			Me.lblOtherBenefit.Text = Language._Object.getCaption(Me.lblOtherBenefit.Name, Me.lblOtherBenefit.Text)
			Me.lblCNSSContribution.Text = Language._Object.getCaption(Me.lblCNSSContribution.Name, Me.lblCNSSContribution.Text)
			Me.lblCashBenefit.Text = Language._Object.getCaption(Me.lblCashBenefit.Name, Me.lblCashBenefit.Text)
			Me.lblPeriod.Text = Language._Object.getCaption(Me.lblPeriod.Name, Me.lblPeriod.Text)
			Me.lblBasicSalary.Text = Language._Object.getCaption(Me.lblBasicSalary.Name, Me.lblBasicSalary.Text)
			Me.lblRate.Text = Language._Object.getCaption(Me.lblRate.Name, Me.lblRate.Text)

		Catch Ex As Exception
			DisplayError.Show("-1", ex.Message, "SetLanguage", mstrModuleName)
		End Try
	End Sub
			
			
	Private Sub SetMessages()
		Try
			Language.setMessage(mstrModuleName, 1, "From Period is mandatory information. Please select From Period.")
			Language.setMessage(mstrModuleName, 2, "Please select head for Basic Salary.")
			Language.setMessage(mstrModuleName, 3, "Please select head for Cash Benefit.")
			Language.setMessage(mstrModuleName, 4, "Please select head for CNSS Contribution.")
			Language.setMessage(mstrModuleName, 5, "Please select head for Other Benefit.")
			Language.setMessage(mstrModuleName, 6, "Please select head for Base CFP.")
			Language.setMessage(mstrModuleName, 7, "Please enter Rate.")
			Language.setMessage(mstrModuleName, 8, "Selection Saved Successfully")
			Language.setMessage(mstrModuleName, 9, "Sorry, This transaction head is already mapped.")

		Catch Ex As Exception
			DisplayError.Show("-1", ex.Message, "SetMessages", mstrModuleName)
		End Try
	End Sub
#End Region 'Language & UI Settings
	'</Language>
End Class