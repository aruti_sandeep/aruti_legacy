﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmID22DECLARATIONReport
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmID22DECLARATIONReport))
        Me.eZeeHeader = New eZee.Common.eZeeHeader
        Me.EZeeFooter1 = New eZee.Common.eZeeFooter
        Me.objbtnAdvanceFilter = New eZee.Common.eZeeLightButton(Me.components)
        Me.btnReset = New eZee.Common.eZeeLightButton(Me.components)
        Me.btnExport = New eZee.Common.eZeeLightButton(Me.components)
        Me.btnClose = New eZee.Common.eZeeLightButton(Me.components)
        Me.objbtnLanguage = New eZee.Common.eZeeLightButton(Me.components)
        Me.gbMandatoryInfo = New eZee.Common.eZeeCollapsibleContainer(Me.components)
        Me.objbtnRSFormula = New eZee.Common.eZeeGradientButton
        Me.LblRSFormula = New System.Windows.Forms.Label
        Me.txtRSFormula = New System.Windows.Forms.TextBox
        Me.objbtnKeywordsINF = New eZee.Common.eZeeGradientButton
        Me.lblIndemnitesNonImposablesFormula = New System.Windows.Forms.Label
        Me.txtIndemnitesNonImposablesFormula = New System.Windows.Forms.TextBox
        Me.objbtnKeywordsFNF = New eZee.Common.eZeeGradientButton
        Me.lblFNHFormula = New System.Windows.Forms.Label
        Me.txtFNHFormula = New System.Windows.Forms.TextBox
        Me.objbtnKeywordsIRF = New eZee.Common.eZeeGradientButton
        Me.lblIRPPFormula = New System.Windows.Forms.Label
        Me.txtIRPPFormula = New System.Windows.Forms.TextBox
        Me.objbtnKeywordsTCF = New eZee.Common.eZeeGradientButton
        Me.lblTCSFormula = New System.Windows.Forms.Label
        Me.txtTCSFormula = New System.Windows.Forms.TextBox
        Me.objbtnKeywordsSBF = New eZee.Common.eZeeGradientButton
        Me.lblSalairesBrutCongesFormula = New System.Windows.Forms.Label
        Me.txtSalairesBrutCongesFormula = New System.Windows.Forms.TextBox
        Me.objbtnKeywordsIIF = New eZee.Common.eZeeGradientButton
        Me.lblIndemnitesImposablesFormula = New System.Windows.Forms.Label
        Me.txtIndemnitesImposablesFormula = New System.Windows.Forms.TextBox
        Me.objbtnKeywordsNF = New eZee.Common.eZeeGradientButton
        Me.lblNourritureFormula = New System.Windows.Forms.Label
        Me.txtNourritureFormula = New System.Windows.Forms.TextBox
        Me.lblMembership = New System.Windows.Forms.Label
        Me.cboMembership = New System.Windows.Forms.ComboBox
        Me.objbtnKeywordsLDF = New eZee.Common.eZeeGradientButton
        Me.lblLogementDomesticiteFormula = New System.Windows.Forms.Label
        Me.txtLogementDomesticiteFormula = New System.Windows.Forms.TextBox
        Me.objbtnKeywordsSPF = New eZee.Common.eZeeGradientButton
        Me.lblSalairesBrutPresenceFormula = New System.Windows.Forms.Label
        Me.txtSalairesBrutPresenceFormula = New System.Windows.Forms.TextBox
        Me.btnSaveSelection = New eZee.Common.eZeeLightButton(Me.components)
        Me.lblToPeriod = New System.Windows.Forms.Label
        Me.cboToPeriod = New System.Windows.Forms.ComboBox
        Me.LblCurrencyRate = New System.Windows.Forms.Label
        Me.LblCurrency = New System.Windows.Forms.Label
        Me.cboCurrency = New System.Windows.Forms.ComboBox
        Me.lnkSetAnalysis = New System.Windows.Forms.LinkLabel
        Me.lblFromPeriod = New System.Windows.Forms.Label
        Me.cboFromPeriod = New System.Windows.Forms.ComboBox
        Me.EZeeFooter1.SuspendLayout()
        Me.gbMandatoryInfo.SuspendLayout()
        Me.SuspendLayout()
        '
        'eZeeHeader
        '
        Me.eZeeHeader.BackColor = System.Drawing.SystemColors.Control
        Me.eZeeHeader.BorderColor = System.Drawing.SystemColors.ControlDark
        Me.eZeeHeader.DescriptionForeColor = System.Drawing.SystemColors.ControlDarkDark
        Me.eZeeHeader.Dock = System.Windows.Forms.DockStyle.Top
        Me.eZeeHeader.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.eZeeHeader.GradiantStyle = eZee.Common.eZeeHeader.GradientStyle.Central
        Me.eZeeHeader.GradientColor1 = System.Drawing.SystemColors.Window
        Me.eZeeHeader.GradientColor2 = System.Drawing.SystemColors.Control
        Me.eZeeHeader.HeaderTextForeColor = System.Drawing.SystemColors.ControlText
        Me.eZeeHeader.Icon = Nothing
        Me.eZeeHeader.Location = New System.Drawing.Point(0, 0)
        Me.eZeeHeader.Message = ""
        Me.eZeeHeader.Name = "eZeeHeader"
        Me.eZeeHeader.Size = New System.Drawing.Size(807, 60)
        Me.eZeeHeader.TabIndex = 72
        Me.eZeeHeader.Title = ""
        '
        'EZeeFooter1
        '
        Me.EZeeFooter1.BorderColor = System.Drawing.Color.Silver
        Me.EZeeFooter1.Controls.Add(Me.objbtnAdvanceFilter)
        Me.EZeeFooter1.Controls.Add(Me.btnReset)
        Me.EZeeFooter1.Controls.Add(Me.btnExport)
        Me.EZeeFooter1.Controls.Add(Me.btnClose)
        Me.EZeeFooter1.Controls.Add(Me.objbtnLanguage)
        Me.EZeeFooter1.Dock = System.Windows.Forms.DockStyle.Bottom
        Me.EZeeFooter1.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.EZeeFooter1.GradiantStyle = eZee.Common.eZeeFooter.GradientStyle.Up
        Me.EZeeFooter1.GradientColor1 = System.Drawing.SystemColors.Control
        Me.EZeeFooter1.GradientColor2 = System.Drawing.SystemColors.Control
        Me.EZeeFooter1.Location = New System.Drawing.Point(0, 597)
        Me.EZeeFooter1.Name = "EZeeFooter1"
        Me.EZeeFooter1.Size = New System.Drawing.Size(807, 55)
        Me.EZeeFooter1.TabIndex = 73
        '
        'objbtnAdvanceFilter
        '
        Me.objbtnAdvanceFilter.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.objbtnAdvanceFilter.BackColor = System.Drawing.Color.White
        Me.objbtnAdvanceFilter.BackgroundImage = CType(resources.GetObject("objbtnAdvanceFilter.BackgroundImage"), System.Drawing.Image)
        Me.objbtnAdvanceFilter.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.objbtnAdvanceFilter.BorderColor = System.Drawing.Color.Empty
        Me.objbtnAdvanceFilter.ButtonType = eZee.Common.eZeeLightButton.enButtonType.NormalButton
        Me.objbtnAdvanceFilter.FlatAppearance.BorderSize = 0
        Me.objbtnAdvanceFilter.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.objbtnAdvanceFilter.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.objbtnAdvanceFilter.ForeColor = System.Drawing.Color.Black
        Me.objbtnAdvanceFilter.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.objbtnAdvanceFilter.GradientForeColor = System.Drawing.Color.Black
        Me.objbtnAdvanceFilter.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.objbtnAdvanceFilter.HoverGradientForeColor = System.Drawing.Color.Black
        Me.objbtnAdvanceFilter.Location = New System.Drawing.Point(411, 13)
        Me.objbtnAdvanceFilter.Name = "objbtnAdvanceFilter"
        Me.objbtnAdvanceFilter.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.objbtnAdvanceFilter.PressedGradientForeColor = System.Drawing.Color.Black
        Me.objbtnAdvanceFilter.Size = New System.Drawing.Size(97, 30)
        Me.objbtnAdvanceFilter.TabIndex = 6
        Me.objbtnAdvanceFilter.Text = "Adv. Filter"
        Me.objbtnAdvanceFilter.UseVisualStyleBackColor = False
        Me.objbtnAdvanceFilter.Visible = False
        '
        'btnReset
        '
        Me.btnReset.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnReset.BackColor = System.Drawing.Color.White
        Me.btnReset.BackgroundImage = CType(resources.GetObject("btnReset.BackgroundImage"), System.Drawing.Image)
        Me.btnReset.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.btnReset.BorderColor = System.Drawing.Color.Empty
        Me.btnReset.ButtonType = eZee.Common.eZeeLightButton.enButtonType.NormalButton
        Me.btnReset.FlatAppearance.BorderSize = 0
        Me.btnReset.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnReset.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnReset.ForeColor = System.Drawing.Color.Black
        Me.btnReset.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.btnReset.GradientForeColor = System.Drawing.Color.Black
        Me.btnReset.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.btnReset.HoverGradientForeColor = System.Drawing.Color.Black
        Me.btnReset.Location = New System.Drawing.Point(514, 13)
        Me.btnReset.Name = "btnReset"
        Me.btnReset.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.btnReset.PressedGradientForeColor = System.Drawing.Color.Black
        Me.btnReset.Size = New System.Drawing.Size(90, 30)
        Me.btnReset.TabIndex = 1
        Me.btnReset.Text = "&Reset"
        Me.btnReset.UseVisualStyleBackColor = True
        '
        'btnExport
        '
        Me.btnExport.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnExport.BackColor = System.Drawing.Color.White
        Me.btnExport.BackgroundImage = CType(resources.GetObject("btnExport.BackgroundImage"), System.Drawing.Image)
        Me.btnExport.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.btnExport.BorderColor = System.Drawing.Color.Empty
        Me.btnExport.ButtonType = eZee.Common.eZeeLightButton.enButtonType.NormalButton
        Me.btnExport.FlatAppearance.BorderSize = 0
        Me.btnExport.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnExport.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnExport.ForeColor = System.Drawing.Color.Black
        Me.btnExport.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.btnExport.GradientForeColor = System.Drawing.Color.Black
        Me.btnExport.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.btnExport.HoverGradientForeColor = System.Drawing.Color.Black
        Me.btnExport.Location = New System.Drawing.Point(610, 13)
        Me.btnExport.Name = "btnExport"
        Me.btnExport.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.btnExport.PressedGradientForeColor = System.Drawing.Color.Black
        Me.btnExport.Size = New System.Drawing.Size(90, 30)
        Me.btnExport.TabIndex = 2
        Me.btnExport.Text = "&Export"
        Me.btnExport.UseVisualStyleBackColor = True
        '
        'btnClose
        '
        Me.btnClose.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnClose.BackColor = System.Drawing.Color.White
        Me.btnClose.BackgroundImage = CType(resources.GetObject("btnClose.BackgroundImage"), System.Drawing.Image)
        Me.btnClose.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.btnClose.BorderColor = System.Drawing.Color.Empty
        Me.btnClose.ButtonType = eZee.Common.eZeeLightButton.enButtonType.NormalButton
        Me.btnClose.FlatAppearance.BorderSize = 0
        Me.btnClose.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnClose.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnClose.ForeColor = System.Drawing.Color.Black
        Me.btnClose.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.btnClose.GradientForeColor = System.Drawing.Color.Black
        Me.btnClose.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.btnClose.HoverGradientForeColor = System.Drawing.Color.Black
        Me.btnClose.Location = New System.Drawing.Point(706, 13)
        Me.btnClose.Name = "btnClose"
        Me.btnClose.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.btnClose.PressedGradientForeColor = System.Drawing.Color.Black
        Me.btnClose.Size = New System.Drawing.Size(90, 30)
        Me.btnClose.TabIndex = 3
        Me.btnClose.Text = "&Close"
        Me.btnClose.UseVisualStyleBackColor = True
        '
        'objbtnLanguage
        '
        Me.objbtnLanguage.BackColor = System.Drawing.Color.White
        Me.objbtnLanguage.BackgroundImage = CType(resources.GetObject("objbtnLanguage.BackgroundImage"), System.Drawing.Image)
        Me.objbtnLanguage.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.objbtnLanguage.BorderColor = System.Drawing.Color.Empty
        Me.objbtnLanguage.ButtonType = eZee.Common.eZeeLightButton.enButtonType.NormalButton
        Me.objbtnLanguage.FlatAppearance.BorderSize = 0
        Me.objbtnLanguage.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.objbtnLanguage.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.objbtnLanguage.ForeColor = System.Drawing.Color.Black
        Me.objbtnLanguage.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.objbtnLanguage.GradientForeColor = System.Drawing.Color.Black
        Me.objbtnLanguage.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.objbtnLanguage.HoverGradientForeColor = System.Drawing.Color.Black
        Me.objbtnLanguage.Image = Global.ArutiReports.My.Resources.Resources.FONT_TXT
        Me.objbtnLanguage.Location = New System.Drawing.Point(9, 13)
        Me.objbtnLanguage.Name = "objbtnLanguage"
        Me.objbtnLanguage.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.objbtnLanguage.PressedGradientForeColor = System.Drawing.Color.Black
        Me.objbtnLanguage.Size = New System.Drawing.Size(30, 30)
        Me.objbtnLanguage.TabIndex = 0
        Me.objbtnLanguage.UseVisualStyleBackColor = False
        '
        'gbMandatoryInfo
        '
        Me.gbMandatoryInfo.BorderColor = System.Drawing.Color.Black
        Me.gbMandatoryInfo.Checked = False
        Me.gbMandatoryInfo.CollapseAllExceptThis = False
        Me.gbMandatoryInfo.CollapsedHoverImage = Nothing
        Me.gbMandatoryInfo.CollapsedNormalImage = Nothing
        Me.gbMandatoryInfo.CollapsedPressedImage = Nothing
        Me.gbMandatoryInfo.CollapseOnLoad = False
        Me.gbMandatoryInfo.Controls.Add(Me.objbtnRSFormula)
        Me.gbMandatoryInfo.Controls.Add(Me.LblRSFormula)
        Me.gbMandatoryInfo.Controls.Add(Me.txtRSFormula)
        Me.gbMandatoryInfo.Controls.Add(Me.objbtnKeywordsINF)
        Me.gbMandatoryInfo.Controls.Add(Me.lblIndemnitesNonImposablesFormula)
        Me.gbMandatoryInfo.Controls.Add(Me.txtIndemnitesNonImposablesFormula)
        Me.gbMandatoryInfo.Controls.Add(Me.objbtnKeywordsFNF)
        Me.gbMandatoryInfo.Controls.Add(Me.lblFNHFormula)
        Me.gbMandatoryInfo.Controls.Add(Me.txtFNHFormula)
        Me.gbMandatoryInfo.Controls.Add(Me.objbtnKeywordsIRF)
        Me.gbMandatoryInfo.Controls.Add(Me.lblIRPPFormula)
        Me.gbMandatoryInfo.Controls.Add(Me.txtIRPPFormula)
        Me.gbMandatoryInfo.Controls.Add(Me.objbtnKeywordsTCF)
        Me.gbMandatoryInfo.Controls.Add(Me.lblTCSFormula)
        Me.gbMandatoryInfo.Controls.Add(Me.txtTCSFormula)
        Me.gbMandatoryInfo.Controls.Add(Me.objbtnKeywordsSBF)
        Me.gbMandatoryInfo.Controls.Add(Me.lblSalairesBrutCongesFormula)
        Me.gbMandatoryInfo.Controls.Add(Me.txtSalairesBrutCongesFormula)
        Me.gbMandatoryInfo.Controls.Add(Me.objbtnKeywordsIIF)
        Me.gbMandatoryInfo.Controls.Add(Me.lblIndemnitesImposablesFormula)
        Me.gbMandatoryInfo.Controls.Add(Me.txtIndemnitesImposablesFormula)
        Me.gbMandatoryInfo.Controls.Add(Me.objbtnKeywordsNF)
        Me.gbMandatoryInfo.Controls.Add(Me.lblNourritureFormula)
        Me.gbMandatoryInfo.Controls.Add(Me.txtNourritureFormula)
        Me.gbMandatoryInfo.Controls.Add(Me.lblMembership)
        Me.gbMandatoryInfo.Controls.Add(Me.cboMembership)
        Me.gbMandatoryInfo.Controls.Add(Me.objbtnKeywordsLDF)
        Me.gbMandatoryInfo.Controls.Add(Me.lblLogementDomesticiteFormula)
        Me.gbMandatoryInfo.Controls.Add(Me.txtLogementDomesticiteFormula)
        Me.gbMandatoryInfo.Controls.Add(Me.objbtnKeywordsSPF)
        Me.gbMandatoryInfo.Controls.Add(Me.lblSalairesBrutPresenceFormula)
        Me.gbMandatoryInfo.Controls.Add(Me.txtSalairesBrutPresenceFormula)
        Me.gbMandatoryInfo.Controls.Add(Me.btnSaveSelection)
        Me.gbMandatoryInfo.Controls.Add(Me.lblToPeriod)
        Me.gbMandatoryInfo.Controls.Add(Me.cboToPeriod)
        Me.gbMandatoryInfo.Controls.Add(Me.LblCurrencyRate)
        Me.gbMandatoryInfo.Controls.Add(Me.LblCurrency)
        Me.gbMandatoryInfo.Controls.Add(Me.cboCurrency)
        Me.gbMandatoryInfo.Controls.Add(Me.lnkSetAnalysis)
        Me.gbMandatoryInfo.Controls.Add(Me.lblFromPeriod)
        Me.gbMandatoryInfo.Controls.Add(Me.cboFromPeriod)
        Me.gbMandatoryInfo.ExpandedHoverImage = Nothing
        Me.gbMandatoryInfo.ExpandedNormalImage = Nothing
        Me.gbMandatoryInfo.ExpandedPressedImage = Nothing
        Me.gbMandatoryInfo.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.gbMandatoryInfo.GradientColor = System.Drawing.SystemColors.ButtonFace
        Me.gbMandatoryInfo.HeaderHeight = 25
        Me.gbMandatoryInfo.HeaderMessage = ""
        Me.gbMandatoryInfo.HeaderMessageFont = New System.Drawing.Font("Tahoma", 8.25!)
        Me.gbMandatoryInfo.HeaderMessageForeColor = System.Drawing.Color.Black
        Me.gbMandatoryInfo.HeightOnCollapse = 0
        Me.gbMandatoryInfo.LeftTextSpace = 0
        Me.gbMandatoryInfo.Location = New System.Drawing.Point(12, 66)
        Me.gbMandatoryInfo.Name = "gbMandatoryInfo"
        Me.gbMandatoryInfo.OpenHeight = 300
        Me.gbMandatoryInfo.PanelBorderStyle = System.Windows.Forms.ButtonBorderStyle.Solid
        Me.gbMandatoryInfo.ShowBorder = True
        Me.gbMandatoryInfo.ShowCheckBox = False
        Me.gbMandatoryInfo.ShowCollapseButton = False
        Me.gbMandatoryInfo.ShowDefaultBorderColor = True
        Me.gbMandatoryInfo.ShowDownButton = False
        Me.gbMandatoryInfo.ShowHeader = True
        Me.gbMandatoryInfo.Size = New System.Drawing.Size(496, 522)
        Me.gbMandatoryInfo.TabIndex = 0
        Me.gbMandatoryInfo.Temp = 0
        Me.gbMandatoryInfo.Text = "Mandatory Information"
        Me.gbMandatoryInfo.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'objbtnRSFormula
        '
        Me.objbtnRSFormula.BackColor = System.Drawing.Color.Transparent
        Me.objbtnRSFormula.BackColor1 = System.Drawing.Color.Transparent
        Me.objbtnRSFormula.BackColor2 = System.Drawing.Color.Transparent
        Me.objbtnRSFormula.BorderNormalColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.objbtnRSFormula.BorderSelected = False
        Me.objbtnRSFormula.DialogResult = System.Windows.Forms.DialogResult.None
        Me.objbtnRSFormula.Image = Global.ArutiReports.My.Resources.Resources.Info_icons
        Me.objbtnRSFormula.ImageAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.objbtnRSFormula.Location = New System.Drawing.Point(459, 437)
        Me.objbtnRSFormula.Name = "objbtnRSFormula"
        Me.objbtnRSFormula.Size = New System.Drawing.Size(21, 21)
        Me.objbtnRSFormula.TabIndex = 205
        '
        'LblRSFormula
        '
        Me.LblRSFormula.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LblRSFormula.Location = New System.Drawing.Point(9, 432)
        Me.LblRSFormula.Name = "LblRSFormula"
        Me.LblRSFormula.Size = New System.Drawing.Size(158, 34)
        Me.LblRSFormula.TabIndex = 204
        Me.LblRSFormula.Text = "RS"
        Me.LblRSFormula.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'txtRSFormula
        '
        Me.txtRSFormula.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtRSFormula.Location = New System.Drawing.Point(173, 437)
        Me.txtRSFormula.Name = "txtRSFormula"
        Me.txtRSFormula.Size = New System.Drawing.Size(280, 21)
        Me.txtRSFormula.TabIndex = 12
        '
        'objbtnKeywordsINF
        '
        Me.objbtnKeywordsINF.BackColor = System.Drawing.Color.Transparent
        Me.objbtnKeywordsINF.BackColor1 = System.Drawing.Color.Transparent
        Me.objbtnKeywordsINF.BackColor2 = System.Drawing.Color.Transparent
        Me.objbtnKeywordsINF.BorderNormalColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.objbtnKeywordsINF.BorderSelected = False
        Me.objbtnKeywordsINF.DialogResult = System.Windows.Forms.DialogResult.None
        Me.objbtnKeywordsINF.Image = Global.ArutiReports.My.Resources.Resources.Info_icons
        Me.objbtnKeywordsINF.ImageAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.objbtnKeywordsINF.Location = New System.Drawing.Point(459, 400)
        Me.objbtnKeywordsINF.Name = "objbtnKeywordsINF"
        Me.objbtnKeywordsINF.Size = New System.Drawing.Size(21, 21)
        Me.objbtnKeywordsINF.TabIndex = 201
        '
        'lblIndemnitesNonImposablesFormula
        '
        Me.lblIndemnitesNonImposablesFormula.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblIndemnitesNonImposablesFormula.Location = New System.Drawing.Point(9, 395)
        Me.lblIndemnitesNonImposablesFormula.Name = "lblIndemnitesNonImposablesFormula"
        Me.lblIndemnitesNonImposablesFormula.Size = New System.Drawing.Size(158, 34)
        Me.lblIndemnitesNonImposablesFormula.TabIndex = 200
        Me.lblIndemnitesNonImposablesFormula.Text = "Indemnités Non Imposables Formula"
        Me.lblIndemnitesNonImposablesFormula.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'txtIndemnitesNonImposablesFormula
        '
        Me.txtIndemnitesNonImposablesFormula.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtIndemnitesNonImposablesFormula.Location = New System.Drawing.Point(173, 400)
        Me.txtIndemnitesNonImposablesFormula.Name = "txtIndemnitesNonImposablesFormula"
        Me.txtIndemnitesNonImposablesFormula.Size = New System.Drawing.Size(280, 21)
        Me.txtIndemnitesNonImposablesFormula.TabIndex = 11
        '
        'objbtnKeywordsFNF
        '
        Me.objbtnKeywordsFNF.BackColor = System.Drawing.Color.Transparent
        Me.objbtnKeywordsFNF.BackColor1 = System.Drawing.Color.Transparent
        Me.objbtnKeywordsFNF.BackColor2 = System.Drawing.Color.Transparent
        Me.objbtnKeywordsFNF.BorderNormalColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.objbtnKeywordsFNF.BorderSelected = False
        Me.objbtnKeywordsFNF.DialogResult = System.Windows.Forms.DialogResult.None
        Me.objbtnKeywordsFNF.Image = Global.ArutiReports.My.Resources.Resources.Info_icons
        Me.objbtnKeywordsFNF.ImageAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.objbtnKeywordsFNF.Location = New System.Drawing.Point(459, 366)
        Me.objbtnKeywordsFNF.Name = "objbtnKeywordsFNF"
        Me.objbtnKeywordsFNF.Size = New System.Drawing.Size(21, 21)
        Me.objbtnKeywordsFNF.TabIndex = 198
        '
        'lblFNHFormula
        '
        Me.lblFNHFormula.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblFNHFormula.Location = New System.Drawing.Point(9, 361)
        Me.lblFNHFormula.Name = "lblFNHFormula"
        Me.lblFNHFormula.Size = New System.Drawing.Size(158, 34)
        Me.lblFNHFormula.TabIndex = 197
        Me.lblFNHFormula.Text = "FNH Formula"
        Me.lblFNHFormula.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'txtFNHFormula
        '
        Me.txtFNHFormula.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtFNHFormula.Location = New System.Drawing.Point(173, 366)
        Me.txtFNHFormula.Name = "txtFNHFormula"
        Me.txtFNHFormula.Size = New System.Drawing.Size(280, 21)
        Me.txtFNHFormula.TabIndex = 10
        '
        'objbtnKeywordsIRF
        '
        Me.objbtnKeywordsIRF.BackColor = System.Drawing.Color.Transparent
        Me.objbtnKeywordsIRF.BackColor1 = System.Drawing.Color.Transparent
        Me.objbtnKeywordsIRF.BackColor2 = System.Drawing.Color.Transparent
        Me.objbtnKeywordsIRF.BorderNormalColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.objbtnKeywordsIRF.BorderSelected = False
        Me.objbtnKeywordsIRF.DialogResult = System.Windows.Forms.DialogResult.None
        Me.objbtnKeywordsIRF.Image = Global.ArutiReports.My.Resources.Resources.Info_icons
        Me.objbtnKeywordsIRF.ImageAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.objbtnKeywordsIRF.Location = New System.Drawing.Point(459, 329)
        Me.objbtnKeywordsIRF.Name = "objbtnKeywordsIRF"
        Me.objbtnKeywordsIRF.Size = New System.Drawing.Size(21, 21)
        Me.objbtnKeywordsIRF.TabIndex = 195
        '
        'lblIRPPFormula
        '
        Me.lblIRPPFormula.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblIRPPFormula.Location = New System.Drawing.Point(9, 324)
        Me.lblIRPPFormula.Name = "lblIRPPFormula"
        Me.lblIRPPFormula.Size = New System.Drawing.Size(158, 34)
        Me.lblIRPPFormula.TabIndex = 194
        Me.lblIRPPFormula.Text = "IRPP Formula"
        Me.lblIRPPFormula.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'txtIRPPFormula
        '
        Me.txtIRPPFormula.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtIRPPFormula.Location = New System.Drawing.Point(173, 329)
        Me.txtIRPPFormula.Name = "txtIRPPFormula"
        Me.txtIRPPFormula.Size = New System.Drawing.Size(280, 21)
        Me.txtIRPPFormula.TabIndex = 9
        '
        'objbtnKeywordsTCF
        '
        Me.objbtnKeywordsTCF.BackColor = System.Drawing.Color.Transparent
        Me.objbtnKeywordsTCF.BackColor1 = System.Drawing.Color.Transparent
        Me.objbtnKeywordsTCF.BackColor2 = System.Drawing.Color.Transparent
        Me.objbtnKeywordsTCF.BorderNormalColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.objbtnKeywordsTCF.BorderSelected = False
        Me.objbtnKeywordsTCF.DialogResult = System.Windows.Forms.DialogResult.None
        Me.objbtnKeywordsTCF.Image = Global.ArutiReports.My.Resources.Resources.Info_icons
        Me.objbtnKeywordsTCF.ImageAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.objbtnKeywordsTCF.Location = New System.Drawing.Point(459, 293)
        Me.objbtnKeywordsTCF.Name = "objbtnKeywordsTCF"
        Me.objbtnKeywordsTCF.Size = New System.Drawing.Size(21, 21)
        Me.objbtnKeywordsTCF.TabIndex = 192
        '
        'lblTCSFormula
        '
        Me.lblTCSFormula.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblTCSFormula.Location = New System.Drawing.Point(9, 288)
        Me.lblTCSFormula.Name = "lblTCSFormula"
        Me.lblTCSFormula.Size = New System.Drawing.Size(158, 34)
        Me.lblTCSFormula.TabIndex = 191
        Me.lblTCSFormula.Text = "TCS Formula"
        Me.lblTCSFormula.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'txtTCSFormula
        '
        Me.txtTCSFormula.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtTCSFormula.Location = New System.Drawing.Point(173, 293)
        Me.txtTCSFormula.Name = "txtTCSFormula"
        Me.txtTCSFormula.Size = New System.Drawing.Size(280, 21)
        Me.txtTCSFormula.TabIndex = 8
        '
        'objbtnKeywordsSBF
        '
        Me.objbtnKeywordsSBF.BackColor = System.Drawing.Color.Transparent
        Me.objbtnKeywordsSBF.BackColor1 = System.Drawing.Color.Transparent
        Me.objbtnKeywordsSBF.BackColor2 = System.Drawing.Color.Transparent
        Me.objbtnKeywordsSBF.BorderNormalColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.objbtnKeywordsSBF.BorderSelected = False
        Me.objbtnKeywordsSBF.DialogResult = System.Windows.Forms.DialogResult.None
        Me.objbtnKeywordsSBF.Image = Global.ArutiReports.My.Resources.Resources.Info_icons
        Me.objbtnKeywordsSBF.ImageAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.objbtnKeywordsSBF.Location = New System.Drawing.Point(459, 259)
        Me.objbtnKeywordsSBF.Name = "objbtnKeywordsSBF"
        Me.objbtnKeywordsSBF.Size = New System.Drawing.Size(21, 21)
        Me.objbtnKeywordsSBF.TabIndex = 186
        '
        'lblSalairesBrutCongesFormula
        '
        Me.lblSalairesBrutCongesFormula.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblSalairesBrutCongesFormula.Location = New System.Drawing.Point(9, 254)
        Me.lblSalairesBrutCongesFormula.Name = "lblSalairesBrutCongesFormula"
        Me.lblSalairesBrutCongesFormula.Size = New System.Drawing.Size(158, 34)
        Me.lblSalairesBrutCongesFormula.TabIndex = 185
        Me.lblSalairesBrutCongesFormula.Text = "Salaires Brut Congés Formula"
        Me.lblSalairesBrutCongesFormula.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'txtSalairesBrutCongesFormula
        '
        Me.txtSalairesBrutCongesFormula.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtSalairesBrutCongesFormula.Location = New System.Drawing.Point(173, 259)
        Me.txtSalairesBrutCongesFormula.Name = "txtSalairesBrutCongesFormula"
        Me.txtSalairesBrutCongesFormula.Size = New System.Drawing.Size(280, 21)
        Me.txtSalairesBrutCongesFormula.TabIndex = 7
        '
        'objbtnKeywordsIIF
        '
        Me.objbtnKeywordsIIF.BackColor = System.Drawing.Color.Transparent
        Me.objbtnKeywordsIIF.BackColor1 = System.Drawing.Color.Transparent
        Me.objbtnKeywordsIIF.BackColor2 = System.Drawing.Color.Transparent
        Me.objbtnKeywordsIIF.BorderNormalColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.objbtnKeywordsIIF.BorderSelected = False
        Me.objbtnKeywordsIIF.DialogResult = System.Windows.Forms.DialogResult.None
        Me.objbtnKeywordsIIF.Image = Global.ArutiReports.My.Resources.Resources.Info_icons
        Me.objbtnKeywordsIIF.ImageAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.objbtnKeywordsIIF.Location = New System.Drawing.Point(459, 222)
        Me.objbtnKeywordsIIF.Name = "objbtnKeywordsIIF"
        Me.objbtnKeywordsIIF.Size = New System.Drawing.Size(21, 21)
        Me.objbtnKeywordsIIF.TabIndex = 183
        '
        'lblIndemnitesImposablesFormula
        '
        Me.lblIndemnitesImposablesFormula.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblIndemnitesImposablesFormula.Location = New System.Drawing.Point(9, 217)
        Me.lblIndemnitesImposablesFormula.Name = "lblIndemnitesImposablesFormula"
        Me.lblIndemnitesImposablesFormula.Size = New System.Drawing.Size(158, 34)
        Me.lblIndemnitesImposablesFormula.TabIndex = 182
        Me.lblIndemnitesImposablesFormula.Text = "Indemnités Imposables Formula"
        Me.lblIndemnitesImposablesFormula.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'txtIndemnitesImposablesFormula
        '
        Me.txtIndemnitesImposablesFormula.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtIndemnitesImposablesFormula.Location = New System.Drawing.Point(173, 222)
        Me.txtIndemnitesImposablesFormula.Name = "txtIndemnitesImposablesFormula"
        Me.txtIndemnitesImposablesFormula.Size = New System.Drawing.Size(280, 21)
        Me.txtIndemnitesImposablesFormula.TabIndex = 6
        '
        'objbtnKeywordsNF
        '
        Me.objbtnKeywordsNF.BackColor = System.Drawing.Color.Transparent
        Me.objbtnKeywordsNF.BackColor1 = System.Drawing.Color.Transparent
        Me.objbtnKeywordsNF.BackColor2 = System.Drawing.Color.Transparent
        Me.objbtnKeywordsNF.BorderNormalColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.objbtnKeywordsNF.BorderSelected = False
        Me.objbtnKeywordsNF.DialogResult = System.Windows.Forms.DialogResult.None
        Me.objbtnKeywordsNF.Image = Global.ArutiReports.My.Resources.Resources.Info_icons
        Me.objbtnKeywordsNF.ImageAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.objbtnKeywordsNF.Location = New System.Drawing.Point(459, 186)
        Me.objbtnKeywordsNF.Name = "objbtnKeywordsNF"
        Me.objbtnKeywordsNF.Size = New System.Drawing.Size(21, 21)
        Me.objbtnKeywordsNF.TabIndex = 180
        '
        'lblNourritureFormula
        '
        Me.lblNourritureFormula.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblNourritureFormula.Location = New System.Drawing.Point(9, 181)
        Me.lblNourritureFormula.Name = "lblNourritureFormula"
        Me.lblNourritureFormula.Size = New System.Drawing.Size(158, 34)
        Me.lblNourritureFormula.TabIndex = 179
        Me.lblNourritureFormula.Text = "Nourriture Formula"
        Me.lblNourritureFormula.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'txtNourritureFormula
        '
        Me.txtNourritureFormula.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtNourritureFormula.Location = New System.Drawing.Point(173, 186)
        Me.txtNourritureFormula.Name = "txtNourritureFormula"
        Me.txtNourritureFormula.Size = New System.Drawing.Size(280, 21)
        Me.txtNourritureFormula.TabIndex = 5
        '
        'lblMembership
        '
        Me.lblMembership.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblMembership.Location = New System.Drawing.Point(12, 89)
        Me.lblMembership.Name = "lblMembership"
        Me.lblMembership.Size = New System.Drawing.Size(155, 15)
        Me.lblMembership.TabIndex = 175
        Me.lblMembership.Text = "Membership"
        Me.lblMembership.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'cboMembership
        '
        Me.cboMembership.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboMembership.DropDownWidth = 180
        Me.cboMembership.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboMembership.FormattingEnabled = True
        Me.cboMembership.Location = New System.Drawing.Point(173, 87)
        Me.cboMembership.Name = "cboMembership"
        Me.cboMembership.Size = New System.Drawing.Size(126, 21)
        Me.cboMembership.TabIndex = 2
        '
        'objbtnKeywordsLDF
        '
        Me.objbtnKeywordsLDF.BackColor = System.Drawing.Color.Transparent
        Me.objbtnKeywordsLDF.BackColor1 = System.Drawing.Color.Transparent
        Me.objbtnKeywordsLDF.BackColor2 = System.Drawing.Color.Transparent
        Me.objbtnKeywordsLDF.BorderNormalColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.objbtnKeywordsLDF.BorderSelected = False
        Me.objbtnKeywordsLDF.DialogResult = System.Windows.Forms.DialogResult.None
        Me.objbtnKeywordsLDF.Image = Global.ArutiReports.My.Resources.Resources.Info_icons
        Me.objbtnKeywordsLDF.ImageAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.objbtnKeywordsLDF.Location = New System.Drawing.Point(459, 150)
        Me.objbtnKeywordsLDF.Name = "objbtnKeywordsLDF"
        Me.objbtnKeywordsLDF.Size = New System.Drawing.Size(21, 21)
        Me.objbtnKeywordsLDF.TabIndex = 173
        '
        'lblLogementDomesticiteFormula
        '
        Me.lblLogementDomesticiteFormula.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblLogementDomesticiteFormula.Location = New System.Drawing.Point(9, 145)
        Me.lblLogementDomesticiteFormula.Name = "lblLogementDomesticiteFormula"
        Me.lblLogementDomesticiteFormula.Size = New System.Drawing.Size(158, 34)
        Me.lblLogementDomesticiteFormula.TabIndex = 172
        Me.lblLogementDomesticiteFormula.Text = "Logement Domesticité Formula"
        Me.lblLogementDomesticiteFormula.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'txtLogementDomesticiteFormula
        '
        Me.txtLogementDomesticiteFormula.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtLogementDomesticiteFormula.Location = New System.Drawing.Point(173, 150)
        Me.txtLogementDomesticiteFormula.Name = "txtLogementDomesticiteFormula"
        Me.txtLogementDomesticiteFormula.Size = New System.Drawing.Size(280, 21)
        Me.txtLogementDomesticiteFormula.TabIndex = 4
        '
        'objbtnKeywordsSPF
        '
        Me.objbtnKeywordsSPF.BackColor = System.Drawing.Color.Transparent
        Me.objbtnKeywordsSPF.BackColor1 = System.Drawing.Color.Transparent
        Me.objbtnKeywordsSPF.BackColor2 = System.Drawing.Color.Transparent
        Me.objbtnKeywordsSPF.BorderNormalColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.objbtnKeywordsSPF.BorderSelected = False
        Me.objbtnKeywordsSPF.DialogResult = System.Windows.Forms.DialogResult.None
        Me.objbtnKeywordsSPF.Image = Global.ArutiReports.My.Resources.Resources.Info_icons
        Me.objbtnKeywordsSPF.ImageAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.objbtnKeywordsSPF.Location = New System.Drawing.Point(459, 114)
        Me.objbtnKeywordsSPF.Name = "objbtnKeywordsSPF"
        Me.objbtnKeywordsSPF.Size = New System.Drawing.Size(21, 21)
        Me.objbtnKeywordsSPF.TabIndex = 166
        '
        'lblSalairesBrutPresenceFormula
        '
        Me.lblSalairesBrutPresenceFormula.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblSalairesBrutPresenceFormula.Location = New System.Drawing.Point(9, 109)
        Me.lblSalairesBrutPresenceFormula.Name = "lblSalairesBrutPresenceFormula"
        Me.lblSalairesBrutPresenceFormula.Size = New System.Drawing.Size(158, 34)
        Me.lblSalairesBrutPresenceFormula.TabIndex = 165
        Me.lblSalairesBrutPresenceFormula.Text = "Salaires Brut Présence Formula"
        Me.lblSalairesBrutPresenceFormula.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'txtSalairesBrutPresenceFormula
        '
        Me.txtSalairesBrutPresenceFormula.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtSalairesBrutPresenceFormula.Location = New System.Drawing.Point(173, 114)
        Me.txtSalairesBrutPresenceFormula.Name = "txtSalairesBrutPresenceFormula"
        Me.txtSalairesBrutPresenceFormula.Size = New System.Drawing.Size(280, 21)
        Me.txtSalairesBrutPresenceFormula.TabIndex = 3
        '
        'btnSaveSelection
        '
        Me.btnSaveSelection.BackColor = System.Drawing.Color.White
        Me.btnSaveSelection.BackgroundImage = CType(resources.GetObject("btnSaveSelection.BackgroundImage"), System.Drawing.Image)
        Me.btnSaveSelection.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.btnSaveSelection.BorderColor = System.Drawing.Color.Empty
        Me.btnSaveSelection.ButtonType = eZee.Common.eZeeLightButton.enButtonType.NormalButton
        Me.btnSaveSelection.FlatAppearance.BorderSize = 0
        Me.btnSaveSelection.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnSaveSelection.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnSaveSelection.ForeColor = System.Drawing.Color.Black
        Me.btnSaveSelection.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.btnSaveSelection.GradientForeColor = System.Drawing.Color.Black
        Me.btnSaveSelection.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.btnSaveSelection.HoverGradientForeColor = System.Drawing.Color.Black
        Me.btnSaveSelection.Location = New System.Drawing.Point(192, 481)
        Me.btnSaveSelection.Name = "btnSaveSelection"
        Me.btnSaveSelection.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.btnSaveSelection.PressedGradientForeColor = System.Drawing.Color.Black
        Me.btnSaveSelection.Size = New System.Drawing.Size(111, 30)
        Me.btnSaveSelection.TabIndex = 13
        Me.btnSaveSelection.Text = "&Save Selection"
        Me.btnSaveSelection.UseVisualStyleBackColor = True
        '
        'lblToPeriod
        '
        Me.lblToPeriod.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblToPeriod.Location = New System.Drawing.Point(9, 62)
        Me.lblToPeriod.Name = "lblToPeriod"
        Me.lblToPeriod.Size = New System.Drawing.Size(158, 17)
        Me.lblToPeriod.TabIndex = 86
        Me.lblToPeriod.Text = "To Period"
        Me.lblToPeriod.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'cboToPeriod
        '
        Me.cboToPeriod.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboToPeriod.DropDownWidth = 230
        Me.cboToPeriod.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboToPeriod.FormattingEnabled = True
        Me.cboToPeriod.Location = New System.Drawing.Point(173, 60)
        Me.cboToPeriod.Name = "cboToPeriod"
        Me.cboToPeriod.Size = New System.Drawing.Size(126, 21)
        Me.cboToPeriod.TabIndex = 1
        '
        'LblCurrencyRate
        '
        Me.LblCurrencyRate.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LblCurrencyRate.Location = New System.Drawing.Point(255, 474)
        Me.LblCurrencyRate.Name = "LblCurrencyRate"
        Me.LblCurrencyRate.Size = New System.Drawing.Size(188, 15)
        Me.LblCurrencyRate.TabIndex = 79
        Me.LblCurrencyRate.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'LblCurrency
        '
        Me.LblCurrency.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LblCurrency.Location = New System.Drawing.Point(301, 492)
        Me.LblCurrency.Name = "LblCurrency"
        Me.LblCurrency.Size = New System.Drawing.Size(60, 15)
        Me.LblCurrency.TabIndex = 75
        Me.LblCurrency.Text = "Currency"
        Me.LblCurrency.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.LblCurrency.Visible = False
        '
        'cboCurrency
        '
        Me.cboCurrency.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboCurrency.DropDownWidth = 180
        Me.cboCurrency.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboCurrency.FormattingEnabled = True
        Me.cboCurrency.Location = New System.Drawing.Point(367, 492)
        Me.cboCurrency.Name = "cboCurrency"
        Me.cboCurrency.Size = New System.Drawing.Size(126, 21)
        Me.cboCurrency.TabIndex = 76
        Me.cboCurrency.Visible = False
        '
        'lnkSetAnalysis
        '
        Me.lnkSetAnalysis.BackColor = System.Drawing.Color.Transparent
        Me.lnkSetAnalysis.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lnkSetAnalysis.Location = New System.Drawing.Point(357, 4)
        Me.lnkSetAnalysis.Name = "lnkSetAnalysis"
        Me.lnkSetAnalysis.Size = New System.Drawing.Size(94, 17)
        Me.lnkSetAnalysis.TabIndex = 69
        Me.lnkSetAnalysis.TabStop = True
        Me.lnkSetAnalysis.Text = "Analysis By"
        Me.lnkSetAnalysis.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.lnkSetAnalysis.Visible = False
        '
        'lblFromPeriod
        '
        Me.lblFromPeriod.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblFromPeriod.Location = New System.Drawing.Point(9, 35)
        Me.lblFromPeriod.Name = "lblFromPeriod"
        Me.lblFromPeriod.Size = New System.Drawing.Size(158, 15)
        Me.lblFromPeriod.TabIndex = 64
        Me.lblFromPeriod.Text = "From Period"
        Me.lblFromPeriod.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'cboFromPeriod
        '
        Me.cboFromPeriod.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboFromPeriod.DropDownWidth = 180
        Me.cboFromPeriod.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboFromPeriod.FormattingEnabled = True
        Me.cboFromPeriod.Location = New System.Drawing.Point(173, 33)
        Me.cboFromPeriod.Name = "cboFromPeriod"
        Me.cboFromPeriod.Size = New System.Drawing.Size(126, 21)
        Me.cboFromPeriod.TabIndex = 0
        '
        'frmID22DECLARATIONReport
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(807, 652)
        Me.Controls.Add(Me.gbMandatoryInfo)
        Me.Controls.Add(Me.eZeeHeader)
        Me.Controls.Add(Me.EZeeFooter1)
        Me.Font = New System.Drawing.Font("Tahoma", 8.25!)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None
        Me.MaximizeBox = False
        Me.MinimizeBox = False
        Me.Name = "frmID22DECLARATIONReport"
        Me.ShowInTaskbar = False
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent
        Me.Text = "ID21 DECLARATION ET TRAITEMENT DES SALAIRES"
        Me.EZeeFooter1.ResumeLayout(False)
        Me.gbMandatoryInfo.ResumeLayout(False)
        Me.gbMandatoryInfo.PerformLayout()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents eZeeHeader As eZee.Common.eZeeHeader
    Friend WithEvents EZeeFooter1 As eZee.Common.eZeeFooter
    Friend WithEvents objbtnAdvanceFilter As eZee.Common.eZeeLightButton
    Friend WithEvents btnReset As eZee.Common.eZeeLightButton
    Friend WithEvents btnExport As eZee.Common.eZeeLightButton
    Friend WithEvents btnClose As eZee.Common.eZeeLightButton
    Friend WithEvents objbtnLanguage As eZee.Common.eZeeLightButton
    Friend WithEvents gbMandatoryInfo As eZee.Common.eZeeCollapsibleContainer
    Friend WithEvents objbtnKeywordsLDF As eZee.Common.eZeeGradientButton
    Friend WithEvents lblLogementDomesticiteFormula As System.Windows.Forms.Label
    Friend WithEvents txtLogementDomesticiteFormula As System.Windows.Forms.TextBox
    Friend WithEvents objbtnKeywordsSPF As eZee.Common.eZeeGradientButton
    Friend WithEvents lblSalairesBrutPresenceFormula As System.Windows.Forms.Label
    Friend WithEvents txtSalairesBrutPresenceFormula As System.Windows.Forms.TextBox
    Friend WithEvents btnSaveSelection As eZee.Common.eZeeLightButton
    Friend WithEvents lblToPeriod As System.Windows.Forms.Label
    Friend WithEvents cboToPeriod As System.Windows.Forms.ComboBox
    Friend WithEvents LblCurrencyRate As System.Windows.Forms.Label
    Friend WithEvents LblCurrency As System.Windows.Forms.Label
    Friend WithEvents cboCurrency As System.Windows.Forms.ComboBox
    Friend WithEvents lnkSetAnalysis As System.Windows.Forms.LinkLabel
    Friend WithEvents lblFromPeriod As System.Windows.Forms.Label
    Friend WithEvents cboFromPeriod As System.Windows.Forms.ComboBox
    Friend WithEvents lblMembership As System.Windows.Forms.Label
    Friend WithEvents cboMembership As System.Windows.Forms.ComboBox
    Friend WithEvents objbtnKeywordsNF As eZee.Common.eZeeGradientButton
    Friend WithEvents lblNourritureFormula As System.Windows.Forms.Label
    Friend WithEvents txtNourritureFormula As System.Windows.Forms.TextBox
    Friend WithEvents objbtnKeywordsSBF As eZee.Common.eZeeGradientButton
    Friend WithEvents lblSalairesBrutCongesFormula As System.Windows.Forms.Label
    Friend WithEvents txtSalairesBrutCongesFormula As System.Windows.Forms.TextBox
    Friend WithEvents objbtnKeywordsIIF As eZee.Common.eZeeGradientButton
    Friend WithEvents lblIndemnitesImposablesFormula As System.Windows.Forms.Label
    Friend WithEvents txtIndemnitesImposablesFormula As System.Windows.Forms.TextBox
    Friend WithEvents objbtnKeywordsTCF As eZee.Common.eZeeGradientButton
    Friend WithEvents lblTCSFormula As System.Windows.Forms.Label
    Friend WithEvents txtTCSFormula As System.Windows.Forms.TextBox
    Friend WithEvents objbtnKeywordsFNF As eZee.Common.eZeeGradientButton
    Friend WithEvents lblFNHFormula As System.Windows.Forms.Label
    Friend WithEvents txtFNHFormula As System.Windows.Forms.TextBox
    Friend WithEvents objbtnKeywordsIRF As eZee.Common.eZeeGradientButton
    Friend WithEvents lblIRPPFormula As System.Windows.Forms.Label
    Friend WithEvents txtIRPPFormula As System.Windows.Forms.TextBox
    Friend WithEvents objbtnKeywordsINF As eZee.Common.eZeeGradientButton
    Friend WithEvents lblIndemnitesNonImposablesFormula As System.Windows.Forms.Label
    Friend WithEvents txtIndemnitesNonImposablesFormula As System.Windows.Forms.TextBox
    Friend WithEvents objbtnRSFormula As eZee.Common.eZeeGradientButton
    Friend WithEvents LblRSFormula As System.Windows.Forms.Label
    Friend WithEvents txtRSFormula As System.Windows.Forms.TextBox
End Class
