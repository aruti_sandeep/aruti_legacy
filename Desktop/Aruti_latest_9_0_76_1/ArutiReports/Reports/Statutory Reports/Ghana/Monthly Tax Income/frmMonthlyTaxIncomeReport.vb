#Region " Imports "
Imports eZeeCommonLib
Imports Aruti.Data
#End Region

Public Class frmMonthlyTaxIncomeReport

    Private ReadOnly mstrModuleName As String = "frmMonthlyTaxIncomeReport"
    Private objTaxIncome As clsMonthlyTaxIncomeReport

    Private mdecEx_Rate As Decimal = 0
    Private mstrCurr_Sign As String = String.Empty
    Private mstrBaseCurrSign As String = String.Empty
    Private mintBaseCurrId As Integer = 0
    Private mstrCurrency_Rate As String = String.Empty
    Private mstrAdvanceFilter As String = String.Empty
    Private mintFirstOpenPeriod As Integer = 0

    Private mstrStringIds As String = ""
    Private mstrStringName As String = ""
    Private mintViewIdx As Integer = 0
    Private mstrAnalysis_Fields As String = ""
    Private mstrAnalysis_Join As String = ""
    Private mstrAnalysis_OrderBy As String = ""
    Private mstrReport_GroupName As String = ""
    'S.SANDEEP [04 JUN 2015] -- START
    'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
    Private mDicDataBaseYearId As Dictionary(Of String, Integer)
    Private mDicDataBaseEndDate As Dictionary(Of String, String)
    'S.SANDEEP [04 JUN 2015] -- END


#Region " Constructor "

    Public Sub New()
        objTaxIncome = New clsMonthlyTaxIncomeReport(User._Object._Languageunkid,Company._Object._Companyunkid)
        objTaxIncome.SetDefaultValue()
        InitializeComponent()
    End Sub

#End Region

#Region " Private Enum "
    Private Enum enHeadTypeId
        EmpDBBNSSS = 1
        EmpOPS = 2
        EmpPPS = 3
        TINNo = 4
        PAYE = 5
        Employee = 6
        Other_Earning = 7
    End Enum
#End Region

#Region " Private Function "

    Private Sub FillCombo()
        Dim objPeriod As New clscommom_period_Tran
        Dim objMaster As New clsMasterData
        Dim objMember As New clsmembership_master
        Dim objTranHead As New clsTransactionHead
        Dim dsCombo As DataSet

        Try

            'S.SANDEEP [04 JUN 2015] -- START
            'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
            'mintFirstOpenPeriod = objMaster.getFirstPeriodID(enModuleReference.Payroll, 1, FinancialYear._Object._YearUnkid)
            mintFirstOpenPeriod = objMaster.getFirstPeriodID(enModuleReference.Payroll, FinancialYear._Object._YearUnkid, 1)
            'S.SANDEEP [04 JUN 2015] -- END

            'Sohail (06 Jan 2015) -- Start
            'Ghana Enhancement - From and To YTD period on Monthly Income Tax Report.
            'dsCombo = objPeriod.getListForCombo(enModuleReference.Payroll, FinancialYear._Object._YearUnkid, "Period", True)
            'Sohail (21 Aug 2015) -- Start
            'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
            'dsCombo = objPeriod.getListForCombo(enModuleReference.Payroll, 0, "Period", True)
            dsCombo = objPeriod.getListForCombo(enModuleReference.Payroll, 0, FinancialYear._Object._DatabaseName, FinancialYear._Object._Database_Start_Date, "Period", True)
            'Sohail (21 Aug 2015) -- End
            'Sohail (06 Jan 2015) -- End
            With cboPeriod
                .ValueMember = "periodunkid"
                .DisplayMember = "name"
                .DataSource = dsCombo.Tables(0)
                .SelectedValue = mintFirstOpenPeriod
            End With

            'Sohail (06 Jan 2015) -- Start
            'Ghana Enhancement - From and To YTD period on Monthly Income Tax Report.
            With cboToPeriod
                .ValueMember = "periodunkid"
                .DisplayMember = "name"
                .DataSource = dsCombo.Tables(0).Copy
                .SelectedValue = mintFirstOpenPeriod
            End With
            'Sohail (06 Jan 2015) -- End


            dsCombo = objMember.getListForCombo("Membership", True, , 1)
            With cboEmpDBBNSSS
                .ValueMember = "membershipunkid"
                .DisplayMember = "name"
                .DataSource = dsCombo.Tables(0)
                .SelectedValue = 0
            End With

            With cboEmpOPS
                .ValueMember = "membershipunkid"
                .DisplayMember = "name"
                .DataSource = dsCombo.Tables(0).Copy
                .SelectedValue = 0
            End With

            With cboEmpPPS
                .ValueMember = "membershipunkid"
                .DisplayMember = "name"
                .DataSource = dsCombo.Tables(0).Copy
                .SelectedValue = 0
            End With

            dsCombo = objMember.getListForCombo("Membership", True, , 2)
            With cboTINNo
                .ValueMember = "membershipunkid"
                .DisplayMember = "name"
                .DataSource = dsCombo.Tables("Membership")
                .SelectedValue = 0
            End With

            'Sohail (21 Aug 2015) -- Start
            'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
            'dsCombo = objTranHead.getComboList("Heads", False, enTranHeadType.EmployeesStatutoryDeductions, , enTypeOf.Taxes)
            dsCombo = objTranHead.getComboList(FinancialYear._Object._DatabaseName, "Heads", False, enTranHeadType.EmployeesStatutoryDeductions, , enTypeOf.Taxes)
            'Sohail (21 Aug 2015) -- End
            With cboPAYE
                .ValueMember = "tranheadunkid"
                .DisplayMember = "name"
                .DataSource = dsCombo.Tables(0)
                If dsCombo.Tables(0).Rows.Count > 0 Then .SelectedIndex = 0
            End With

            'Sohail (21 Aug 2015) -- Start
            'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
            'dsCombo = objTranHead.getComboList("OtherEarning", True, , , enTypeOf.Other_Earnings)

            'Sohail (16 Mar 2016) -- Start
            'Enhancement - Include Informational Heads in Basic Salary As Other Earning option in all Statutory Reports.
            'dsCombo = objTranHead.getComboList(FinancialYear._Object._DatabaseName, "OtherEarning", True, , , enTypeOf.Other_Earnings)

            'Anjan [26 September 2016] -- Start
            'ENHANCEMENT : Removing other earnings and including all earnings on all statutory reports as per Rutta's request.
            'dsCombo = objTranHead.getComboList(FinancialYear._Object._DatabaseName, "OtherEarning", True, , , , , , "typeof_id = " & enTypeOf.Other_Earnings & " OR trnheadtype_id = " & enTranHeadType.Informational & " ")
            dsCombo = objTranHead.getComboList(FinancialYear._Object._DatabaseName, "OtherEarning", True, , , , , , "trnheadtype_id = " & enTranHeadType.EarningForEmployees & " OR trnheadtype_id = " & enTranHeadType.Informational & " ")
            'Anjan [26 September 2016] -- End

            'Sohail (16 Mar 2016) -- End
            'Sohail (21 Aug 2015) -- End
            With cboOtherEarning
                .ValueMember = "tranheadunkid"
                .DisplayMember = "name"
                .DataSource = dsCombo.Tables("OtherEarning")
                .SelectedValue = 0
            End With

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "FillCombo", mstrModuleName)
        Finally
            objPeriod = Nothing
            objMaster = Nothing
            objMember = Nothing
            objTranHead = Nothing
        End Try
    End Sub

    Private Sub ResetValue()
        Try
            cboPeriod.SelectedValue = mintFirstOpenPeriod
            cboToPeriod.SelectedValue = mintFirstOpenPeriod 'Sohail (06 Jan 2015)
            cboEmployee.SelectedValue = 0
            cboEmpDBBNSSS.SelectedValue = 0
            cboEmpOPS.SelectedValue = 0
            cboEmpPPS.SelectedValue = 0
            cboTINNo.SelectedValue = 0
            If cboPAYE.Items.Count > 0 Then cboPAYE.SelectedIndex = 0
            cboOtherEarning.SelectedValue = 0
            gbBasicSalaryOtherEarning.Checked = ConfigParameter._Object._SetBasicSalaryAsOtherEarningOnStatutoryReport


            mstrStringIds = String.Empty
            mstrStringName = String.Empty
            mintViewIdx = 0
            mstrAnalysis_Fields = ""
            mstrAnalysis_Join = ""
            mstrAnalysis_OrderBy = ""
            mstrReport_GroupName = ""

            Call GetValue()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "ResetValue", mstrModuleName)
        End Try
    End Sub

    Private Sub GetValue()
        Dim objUserDefRMode As New clsUserDef_ReportMode
        Dim dsList As DataSet
        Try
            dsList = objUserDefRMode.GetList("List", enArutiReport.Monthly_Tax_Income_Report)

            If dsList.Tables("List").Rows.Count > 0 Then

                For Each dsRow As DataRow In dsList.Tables("List").Rows
                    Select Case CInt(dsRow.Item("headtypeid"))

                        Case enHeadTypeId.EmpDBBNSSS
                            cboEmpDBBNSSS.SelectedValue = CInt(dsRow.Item("transactionheadid"))


                        Case enHeadTypeId.EmpOPS
                            cboEmpOPS.SelectedValue = CInt(dsRow.Item("transactionheadid"))

                        Case enHeadTypeId.EmpPPS
                            cboEmpPPS.SelectedValue = CInt(dsRow.Item("transactionheadid"))

                        Case enHeadTypeId.TINNo
                            cboTINNo.SelectedValue = CInt(dsRow.Item("transactionheadid"))

                        Case enHeadTypeId.PAYE
                            cboPAYE.SelectedValue = CInt(dsRow.Item("transactionheadid"))

                        Case enHeadTypeId.Employee
                            cboEmployee.SelectedValue = CInt(dsRow.Item("transactionheadid"))

                        Case enHeadTypeId.Other_Earning
                            cboOtherEarning.SelectedValue = CInt(dsRow.Item("transactionheadid"))
                            If CInt(dsRow.Item("transactionheadid")) > 0 Then
                                gbBasicSalaryOtherEarning.Checked = True
                            Else
                                gbBasicSalaryOtherEarning.Checked = False
                            End If
                    End Select
                Next

            End If

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "GetValue", mstrModuleName)
        Finally
            objUserDefRMode = Nothing
        End Try
    End Sub

    Private Function SetFilter() As Boolean
        'Sohail (06 Jan 2015) -- Start
        'Ghana Enhancement - From and To YTD period on Monthly Income Tax Report.
        Dim objPeriod As New clscommom_period_Tran
        Dim strPeriodIDs As String = ""
        Dim strPeriodNAMEs As String = ""
        Dim arrDatabaseName As New ArrayList
        Dim i As Integer = 0
        Dim intPrevYearID As Integer = 0
        'Sohail (06 Jan 2015) -- End
        Try
            objTaxIncome.SetDefaultValue()

            If CInt(cboPeriod.SelectedValue) <= 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 1, "Period is mandatory information. Please select Period."), enMsgBoxStyle.Information)
                cboPeriod.Focus()
                Return False
                'Sohail (06 Jan 2015) -- Start
                'Ghana Enhancement - From and To YTD period on Monthly Income Tax Report.
            ElseIf CInt(cboToPeriod.SelectedValue) <= 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 10, "To Period is mandatory information. Please select Period."), enMsgBoxStyle.Information)
                cboToPeriod.Focus()
                Return False
            ElseIf cboToPeriod.SelectedIndex < cboPeriod.SelectedIndex Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 11, " To Period cannot be less than From Period"), enMsgBoxStyle.Information)
                cboToPeriod.Focus()
                Exit Function
                'Sohail (06 Jan 2015) -- End
            ElseIf CInt(cboEmpDBBNSSS.SelectedValue) <= 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 2, "Please select Employee D.B.B.N.S.S.S."), enMsgBoxStyle.Information)
                cboEmpDBBNSSS.Focus()
                Return False
                'ElseIf CInt(cboEmpOPS.SelectedValue) <= 0 Then
                '    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 3, "Please select Employee O.P.S."), enMsgBoxStyle.Information)
                '    cboEmpOPS.Focus()
                '    Return False
                'ElseIf CInt(cboEmpPPS.SelectedValue) <= 0 Then
                '    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 4, "Please select Employee P.P.S."), enMsgBoxStyle.Information)
                '    cboEmpPPS.Focus()
                '    Return False
                'ElseIf CInt(cboTINNo.SelectedValue) <= 0 Then
                '    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 5, "Please select TIN No."), enMsgBoxStyle.Information)
                '    cboTINNo.Focus()
                '    Return False
            ElseIf cboPAYE.Items.Count <= 0 OrElse CInt(cboPAYE.SelectedValue) <= 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 6, "Please select PAYE head."), enMsgBoxStyle.Information)
                cboPAYE.Focus()
                Return False
            End If

            If gbBasicSalaryOtherEarning.Checked = True AndAlso CInt(cboOtherEarning.SelectedValue) <= 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 7, "Please select transaction head."), enMsgBoxStyle.Information)
                cboOtherEarning.Focus()
                Return False
            End If

            'Sohail (06 Jan 2015) -- Start
            'Ghana Enhancement - From and To YTD period on Monthly Income Tax Report.
            'objTaxIncome._PeriodId = CInt(cboPeriod.SelectedValue)
            'objTaxIncome._PeriodName = cboPeriod.Text
            Dim strDBIds As String = objPeriod.GetDISTINCTDatabaseNAMEs(enModuleReference.Payroll, eZeeDate.convertDate(CType(cboPeriod.SelectedItem, DataRowView).Item("start_date")), eZeeDate.convertDate(CType(cboToPeriod.SelectedItem, DataRowView).Item("end_date")))
            arrDatabaseName.AddRange(strDBIds.Split(","))
            For Each dsRow As DataRow In CType(cboPeriod.DataSource, DataTable).Rows
                If i >= cboPeriod.SelectedIndex AndAlso i <= cboToPeriod.SelectedIndex Then
                    strPeriodIDs &= ", " & dsRow.Item("periodunkid").ToString
                    strPeriodNAMEs &= ", " & dsRow.Item("name").ToString
                End If
                i += 1
            Next
            If strPeriodIDs.Trim <> "" Then strPeriodIDs = strPeriodIDs.Substring(2)
            If strPeriodNAMEs.Trim <> "" Then strPeriodNAMEs = strPeriodNAMEs.Substring(2)
            If arrDatabaseName.Count <= 0 Then Return False


            'S.SANDEEP [04 JUN 2015] -- START
            'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS

            mDicDataBaseYearId = New Dictionary(Of String, Integer)
            mDicDataBaseEndDate = New Dictionary(Of String, String)

            Dim objCompany As New clsCompany_Master
            Dim dsDB As DataSet = objCompany.GetFinancialYearList(Company._Object._Companyunkid, , "Database", 0)

            mDicDataBaseYearId = dsDB.Tables(0).AsEnumerable().ToDictionary(Of String, Integer)(Function(x) x.Field(Of String)("database_name"), Function(y) y.Field(Of Integer)("yearunkid"))
            mDicDataBaseEndDate = dsDB.Tables(0).AsEnumerable().ToDictionary(Of String, String)(Function(x) x.Field(Of String)("database_name"), Function(x) x.Field(Of String)("end_date"))

            objTaxIncome._DicDataBaseEndDate = mDicDataBaseEndDate
            objTaxIncome._DicDataBaseYearId = mDicDataBaseYearId

            objCompany = Nothing
            'S.SANDEEP [04 JUN 2015] -- END


            objTaxIncome._PeriodIds = strPeriodIDs
            objTaxIncome._PeriodNames = strPeriodNAMEs
            objTaxIncome._Arr_DatabaseName = arrDatabaseName
            objTaxIncome._CurrDatabaseName = FinancialYear._Object._DatabaseName
            'Sohail (06 Jan 2015) -- End

            If CInt(cboEmpDBBNSSS.SelectedValue) > 0 Then
                objTaxIncome._DBBNSMembershipId = cboEmpDBBNSSS.SelectedValue
                objTaxIncome._DBBNSMembershipName = cboEmpDBBNSSS.Text
            End If

            If CInt(cboEmpOPS.SelectedValue) > 0 Then
                objTaxIncome._OPSMembershipId = cboEmpOPS.SelectedValue
                objTaxIncome._OPSMembershipName = cboEmpOPS.Text
            End If

            If CInt(cboEmpPPS.SelectedValue) > 0 Then
                objTaxIncome._PPSMembershipId = cboEmpPPS.SelectedValue
                objTaxIncome._PPSMembershipName = cboEmpPPS.Text
            End If

            If CInt(cboTINNo.SelectedValue) > 0 Then
                objTaxIncome._TINNoMembershipId = cboTINNo.SelectedValue
                objTaxIncome._TINNoMembershipName = cboTINNo.Text
            End If

            If CInt(cboPAYE.SelectedValue) > 0 Then
                objTaxIncome._PAYEHeadId = CInt(cboPAYE.SelectedValue)
                objTaxIncome._PAYEHeadName = cboPAYE.Text
            End If

            If CInt(cboEmployee.SelectedValue) > 0 Then
                objTaxIncome._EmpId = cboEmployee.SelectedValue
                objTaxIncome._EmpName = cboEmployee.Text
            End If




            If gbBasicSalaryOtherEarning.Checked = True Then
                objTaxIncome._OtherEarningTranId = CInt(cboOtherEarning.SelectedValue)
            Else
                objTaxIncome._OtherEarningTranId = 0
            End If

            objTaxIncome._ViewByIds = mstrStringIds
            objTaxIncome._ViewIndex = mintViewIdx
            objTaxIncome._ViewByName = mstrStringName
            objTaxIncome._Analysis_Fields = mstrAnalysis_Fields
            objTaxIncome._Analysis_Join = mstrAnalysis_Join
            objTaxIncome._Analysis_OrderBy = mstrAnalysis_OrderBy
            objTaxIncome._Report_GroupName = mstrReport_GroupName

            'Sohail (03 Aug 2019) -- Start
            'PAYTECH KENYA issue # 0004019 - 76.1 - Inaccurate data on payroll reports i.e payroll summary report When an employee is moved between departments/branches, payroll report captures only the latest transfer records regardless of the period when payroll was done. System should pick the allocation based on period of transfer without overriding the former transfer.
            objTaxIncome._EmpAsOnDate = eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate)
            'Sohail (03 Aug 2019) -- End

            Return True

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "SetFilter", mstrModuleName)
        End Try
    End Function

#End Region

#Region " Form's Events "
    Private Sub frmMonthlyTaxIncomeReport_FormClosed(ByVal sender As Object, ByVal e As System.Windows.Forms.FormClosedEventArgs) Handles Me.FormClosed
        Try
            objTaxIncome = Nothing
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmMonthlyTaxIncomeReport_FormClosed", mstrModuleName)
        End Try
    End Sub

    Private Sub frmMonthlyTaxIncomeReport_KeyDown(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles Me.KeyDown
        Try
            If e.KeyCode = Keys.Return Then
                Windows.Forms.SendKeys.Send("{TAB}")
                e.Handled = True
            ElseIf e.Control AndAlso e.KeyCode = Keys.E Then
                Call btnExport.PerformClick()
            End If

        Catch ex As Exception
            DisplayError.Show(-1, ex.Message, "frmMonthlyTaxIncomeReport_KeyDown", mstrModuleName)
        End Try
    End Sub

    Private Sub frmMonthlyTaxIncomeReport_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            Call Language.setLanguage(Me.Name)

            Call OtherSettings()


            Call FillCombo()
            Call ResetValue()

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmMonthlyTaxIncomeReport_Load", mstrModuleName)
        End Try
    End Sub
#End Region

#Region " Buttons "
    Private Sub btnClose_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnClose.Click
        Try
            Me.Close()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "btnClose_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub btnExport_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnExport.Click
        Try
            If SetFilter() = False Then Exit Try
            'S.SANDEEP [04 JUN 2015] -- START
            'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
            'objTaxIncome.Generate_DetailReport()
            objTaxIncome.Generate_DetailReport(User._Object._Userunkid, _
                                               FinancialYear._Object._YearUnkid, _
                                               Company._Object._Companyunkid, _
                                               eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), _
                                               ConfigParameter._Object._UserAccessModeSetting, _
                                               True, ConfigParameter._Object._Base_CurrencyId)
            'S.SANDEEP [04 JUN 2015] -- END
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "btnExport_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub btnReset_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnReset.Click
        Try
            Call ResetValue()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "btnReset_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub objbtnSearchEmployee_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnSearchEmployee.Click
        Dim frm As New frmCommonSearch
        Try
            frm.DataSource = cboEmployee.DataSource
            frm.ValueMember = cboEmployee.ValueMember
            frm.DisplayMember = cboEmployee.DisplayMember
            frm.CodeMember = "employeecode"
            If frm.DisplayDialog Then
                cboEmployee.SelectedValue = frm.SelectedValue
                cboEmployee.Focus()
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objbtnSearchEmployee_Click", mstrModuleName)
        Finally
            If frm IsNot Nothing Then frm.Dispose()
        End Try
    End Sub

    Private Sub btnSaveSelection_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSaveSelection.Click
        Dim objUserDefRMode As New clsUserDef_ReportMode
        Try
            If SetFilter() = False Then Exit Try

            For intHeadType As Integer = 1 To 7
                objUserDefRMode = New clsUserDef_ReportMode()

                Dim intUnkid As Integer = -1

                objUserDefRMode._Reportunkid = enArutiReport.Monthly_Tax_Income_Report
                objUserDefRMode._Reporttypeid = 0
                objUserDefRMode._Reportmodeid = 0

                Select Case intHeadType

                    Case enHeadTypeId.EmpDBBNSSS
                        objUserDefRMode._Headtypeid = intHeadType
                        objUserDefRMode._EarningTranHeadIds = cboEmpDBBNSSS.SelectedValue.ToString

                        intUnkid = objUserDefRMode.isExist(enArutiReport.Monthly_Tax_Income_Report, 0, 0, intHeadType)

                    Case enHeadTypeId.EmpOPS
                        objUserDefRMode._Headtypeid = intHeadType
                        objUserDefRMode._EarningTranHeadIds = cboEmpOPS.SelectedValue.ToString

                        intUnkid = objUserDefRMode.isExist(enArutiReport.Monthly_Tax_Income_Report, 0, 0, intHeadType)

                    Case enHeadTypeId.EmpPPS
                        objUserDefRMode._Headtypeid = intHeadType
                        objUserDefRMode._EarningTranHeadIds = cboEmpPPS.SelectedValue.ToString

                        intUnkid = objUserDefRMode.isExist(enArutiReport.Monthly_Tax_Income_Report, 0, 0, intHeadType)

                    Case enHeadTypeId.TINNo
                        objUserDefRMode._Headtypeid = intHeadType
                        objUserDefRMode._EarningTranHeadIds = cboTINNo.SelectedValue.ToString

                        intUnkid = objUserDefRMode.isExist(enArutiReport.Monthly_Tax_Income_Report, 0, 0, intHeadType)

                    Case enHeadTypeId.PAYE
                        objUserDefRMode._Headtypeid = intHeadType
                        objUserDefRMode._EarningTranHeadIds = cboPAYE.SelectedValue.ToString

                        intUnkid = objUserDefRMode.isExist(enArutiReport.Monthly_Tax_Income_Report, 0, 0, intHeadType)

                    Case enHeadTypeId.Employee
                        objUserDefRMode._Headtypeid = intHeadType
                        objUserDefRMode._EarningTranHeadIds = cboEmployee.SelectedValue.ToString

                        intUnkid = objUserDefRMode.isExist(enArutiReport.Monthly_Tax_Income_Report, 0, 0, intHeadType)

                    Case enHeadTypeId.Other_Earning
                        objUserDefRMode._Headtypeid = intHeadType
                        objUserDefRMode._EarningTranHeadIds = cboOtherEarning.SelectedValue.ToString

                        intUnkid = objUserDefRMode.isExist(enArutiReport.Monthly_Tax_Income_Report, 0, 0, intHeadType)

                End Select

                objUserDefRMode._Reportmodeunkid = intUnkid

                If intUnkid <= 0 Then
                    objUserDefRMode.Insert()
                Else
                    objUserDefRMode.Update()
                End If

            Next

            eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 9, "Selection Saved Successfully"), enMsgBoxStyle.Information)

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "btnSaveSelection_Click", mstrModuleName)
        Finally
            objUserDefRMode = Nothing
        End Try
    End Sub

    Private Sub objbtnLanguage_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnLanguage.Click
        Dim objfrm As New frmLanguage
        Try
            If User._Object._Isrighttoleft = True Then
                objfrm.RightToLeft = Windows.Forms.RightToLeft.Yes
                objfrm.RightToLeftLayout = True
                Call Language.ctlRightToLeftlayOut(objfrm)
            End If

            Call SetMessages()

            clsMonthlyTaxIncomeReport.SetMessages()
            objfrm._Other_ModuleNames = "clsMonthlyTaxIncomeReport"
            objfrm.displayDialog(Me)

            Call Language.setLanguage(Me.Name)
            Call SetLanguage()

        Catch ex As System.Exception
            Call DisplayError.Show(-1, ex.Message, "Form_Language_Click", mstrModuleName)
        Finally
            objfrm.Dispose()
            objfrm = Nothing
        End Try
    End Sub
#End Region

#Region " Combobox Events "
    Private Sub cboPeriod_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cboPeriod.SelectedIndexChanged
        Dim objEmp As New clsEmployee_Master
        Dim dsList As DataSet
        Try
            'S.SANDEEP [04 JUN 2015] -- START
            'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
            'dsList = objEmp.GetEmployeeList("Emp", True, True, , , , , , , , , , , , eZeeDate.convertDate(CType(cboPeriod.SelectedItem, DataRowView).Item("start_date").ToString), eZeeDate.convertDate(CType(cboPeriod.SelectedItem, DataRowView).Item("end_date").ToString))
            dsList = objEmp.GetEmployeeList(FinancialYear._Object._DatabaseName, _
                                            User._Object._Userunkid, _
                                            FinancialYear._Object._YearUnkid, _
                                            Company._Object._Companyunkid, _
                                            eZeeDate.convertDate(CType(cboPeriod.SelectedItem, DataRowView).Item("start_date").ToString), _
                                            eZeeDate.convertDate(CType(cboPeriod.SelectedItem, DataRowView).Item("end_date").ToString), _
                                            ConfigParameter._Object._UserAccessModeSetting, _
                                            True, False, "Emp", True)
            'S.SANDEEP [04 JUN 2015] -- END
            With cboEmployee
                .ValueMember = "employeeunkid"
                .DisplayMember = "employeename"
                .DataSource = dsList.Tables("Emp")
                .SelectedValue = 0
            End With
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "cboPeriod_SelectedIndexChanged", mstrModuleName)
        Finally
            objEmp = Nothing
        End Try
    End Sub

    Private Sub cboEmpDBBNSSS_Validating(ByVal sender As Object, ByVal e As System.ComponentModel.CancelEventArgs) Handles cboEmpDBBNSSS.Validating, _
                                                                                                                           cboEmpOPS.Validating, _
                                                                                                                           cboEmpPPS.Validating

        Try
            Dim cbo As ComboBox = CType(sender, ComboBox)
            If CInt(cbo.SelectedValue) > 0 Then
                Dim lst As IEnumerable(Of ComboBox) = (From c In gbMandatoryInfo.Controls.OfType(Of ComboBox)().Where(Function(t) t.Name <> cbo.Name AndAlso t.Name <> cboPeriod.Name AndAlso t.Name <> cboEmployee.Name AndAlso t.Name <> cboTINNo.Name AndAlso t.Name <> cboPAYE.Name AndAlso t.Name <> cboOtherEarning.Name AndAlso CInt(t.SelectedValue) = CInt(cbo.SelectedValue)))
                If lst.Count > 0 Then
                    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 8, "Sorry, This Membership is already selected."))
                    cbo.SelectedValue = 0
                    e.Cancel = True
                End If
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "cboEmpDBBNSSS_Validating", mstrModuleName)
        End Try
    End Sub
#End Region

#Region " Other Control's Events "
    Private Sub lnkAnalysisBy_LinkClicked(ByVal sender As System.Object, ByVal e As System.Windows.Forms.LinkLabelLinkClickedEventArgs) Handles lnkAnalysisBy.LinkClicked
        Dim frm As New frmViewAnalysis
        Try
            frm.displayDialog()
            mstrStringIds = frm._ReportBy_Ids
            mstrStringName = frm._ReportBy_Name
            mintViewIdx = frm._ViewIndex
            mstrAnalysis_Fields = frm._Analysis_Fields
            mstrAnalysis_Join = frm._Analysis_Join
            mstrAnalysis_OrderBy = frm._Analysis_OrderBy
            mstrReport_GroupName = frm._Report_GroupName
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "lnkAnalysisBy_LinkClicked", mstrModuleName)
        Finally
            frm = Nothing
        End Try
    End Sub

    Private Sub objbtnSearchOtherEarning_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnSearchOtherEarning.Click
        Dim frm As New frmCommonSearch
        Try
            frm.DataSource = cboOtherEarning.DataSource
            frm.ValueMember = cboOtherEarning.ValueMember
            frm.DisplayMember = cboOtherEarning.DisplayMember
            frm.CodeMember = "code"
            If frm.DisplayDialog Then
                cboOtherEarning.SelectedValue = frm.SelectedValue
                cboOtherEarning.Focus()
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objbtnSearchOtherEarning_Click", mstrModuleName)
        Finally
            frm = Nothing
        End Try
    End Sub
#End Region

	'<Language> This Auto Generated Text Please Do Not Modify it.
#Region " Language & UI Settings "
	Private Sub OtherSettings()
		Try
			Me.SuspendLayout()
			
			Call SetLanguage()
			
			Me.gbMandatoryInfo.GradientColor = GUI._eZeeContainerHeaderBackColor 
			Me.gbMandatoryInfo.ForeColor = GUI._eZeeContainerHeaderForeColor 

			Me.gbBasicSalaryOtherEarning.GradientColor = GUI._eZeeContainerHeaderBackColor 
			Me.gbBasicSalaryOtherEarning.ForeColor = GUI._eZeeContainerHeaderForeColor 


			
			Me.eZeeHeader.GradientColor1 = GUI._HeaderBackColor1 
			Me.eZeeHeader.GradientColor2 = GUI._HeaderBackColor2 
			Me.eZeeHeader.BorderColor = GUI._HeaderBorderColor 
			Me.eZeeHeader.HeaderTextForeColor = GUI._HeaderTitleFontColor 
			Me.eZeeHeader.DescriptionForeColor = GUI._HeaderMessageFontColor 


			Me.btnReset.GradientBackColor = GUI._ButttonBackColor 
			Me.btnReset.GradientForeColor = GUI._ButttonFontColor

			Me.btnExport.GradientBackColor = GUI._ButttonBackColor 
			Me.btnExport.GradientForeColor = GUI._ButttonFontColor

			Me.btnClose.GradientBackColor = GUI._ButttonBackColor 
			Me.btnClose.GradientForeColor = GUI._ButttonFontColor

			Me.objbtnLanguage.GradientBackColor = GUI._ButttonBackColor 
			Me.objbtnLanguage.GradientForeColor = GUI._ButttonFontColor

			Me.btnSaveSelection.GradientBackColor = GUI._ButttonBackColor 
			Me.btnSaveSelection.GradientForeColor = GUI._ButttonFontColor


			Me.ResumeLayout()
		Catch Ex As Exception
			DisplayError.Show("-1", ex.Message, "OtherSettings", mstrModuleName)
		End Try
	End Sub
			
			
	Private Sub SetLanguage()
		Try
			Me.Text = Language._Object.getCaption(Me.Name, Me.Text)
			
			Me.btnReset.Text = Language._Object.getCaption(Me.btnReset.Name, Me.btnReset.Text)
			Me.btnExport.Text = Language._Object.getCaption(Me.btnExport.Name, Me.btnExport.Text)
			Me.btnClose.Text = Language._Object.getCaption(Me.btnClose.Name, Me.btnClose.Text)
			Me.eZeeHeader.Title = Language._Object.getCaption(Me.eZeeHeader.Name & "_Title" , Me.eZeeHeader.Title)
			Me.eZeeHeader.Message = Language._Object.getCaption(Me.eZeeHeader.Name & "_Message" , Me.eZeeHeader.Message)
			Me.gbMandatoryInfo.Text = Language._Object.getCaption(Me.gbMandatoryInfo.Name, Me.gbMandatoryInfo.Text)
			Me.LblCurrencyRate.Text = Language._Object.getCaption(Me.LblCurrencyRate.Name, Me.LblCurrencyRate.Text)
			Me.lblEmpDBNSSS.Text = Language._Object.getCaption(Me.lblEmpDBNSSS.Name, Me.lblEmpDBNSSS.Text)
			Me.lnkAnalysisBy.Text = Language._Object.getCaption(Me.lnkAnalysisBy.Name, Me.lnkAnalysisBy.Text)
			Me.lblEmpOPS.Text = Language._Object.getCaption(Me.lblEmpOPS.Name, Me.lblEmpOPS.Text)
			Me.lblEmpPPS.Text = Language._Object.getCaption(Me.lblEmpPPS.Name, Me.lblEmpPPS.Text)
			Me.lblPeriod.Text = Language._Object.getCaption(Me.lblPeriod.Name, Me.lblPeriod.Text)
			Me.gbBasicSalaryOtherEarning.Text = Language._Object.getCaption(Me.gbBasicSalaryOtherEarning.Name, Me.gbBasicSalaryOtherEarning.Text)
			Me.lblOtherEarning.Text = Language._Object.getCaption(Me.lblOtherEarning.Name, Me.lblOtherEarning.Text)
			Me.lblTINNo.Text = Language._Object.getCaption(Me.lblTINNo.Name, Me.lblTINNo.Text)
			Me.lnOtherInformation.Text = Language._Object.getCaption(Me.lnOtherInformation.Name, Me.lnOtherInformation.Text)
			Me.lblEmployee.Text = Language._Object.getCaption(Me.lblEmployee.Name, Me.lblEmployee.Text)
			Me.lblPAYE.Text = Language._Object.getCaption(Me.lblPAYE.Name, Me.lblPAYE.Text)
			Me.btnSaveSelection.Text = Language._Object.getCaption(Me.btnSaveSelection.Name, Me.btnSaveSelection.Text)
			Me.lblToPeriod.Text = Language._Object.getCaption(Me.lblToPeriod.Name, Me.lblToPeriod.Text)

		Catch Ex As Exception
			DisplayError.Show("-1", ex.Message, "SetLanguage", mstrModuleName)
		End Try
	End Sub
			
			
	Private Sub SetMessages()
		Try
			Language.setMessage(mstrModuleName, 1, "Period is mandatory information. Please select Period.")
            Language.setMessage(mstrModuleName, 2, "Please select Employee D.B.B.N.S.S.S.")
            Language.setMessage(mstrModuleName, 3, "Please select Employee O.P.S.")
            Language.setMessage(mstrModuleName, 4, "Please select Employee P.P.S.")
            Language.setMessage(mstrModuleName, 5, "Please select TIN No.")
			Language.setMessage(mstrModuleName, 6, "Please select PAYE head.")
			Language.setMessage(mstrModuleName, 7, "Please select transaction head.")
			Language.setMessage(mstrModuleName, 8, "Sorry, This Membership is already selected.")
			Language.setMessage(mstrModuleName, 9, "Selection Saved Successfully")
			Language.setMessage(mstrModuleName, 10, "To Period is mandatory information. Please select Period.")
			Language.setMessage(mstrModuleName, 11, " To Period cannot be less than From Period")

		Catch Ex As Exception
			DisplayError.Show("-1", ex.Message, "SetMessages", mstrModuleName)
		End Try
	End Sub
#End Region 'Language & UI Settings
	'</Language>
End Class
