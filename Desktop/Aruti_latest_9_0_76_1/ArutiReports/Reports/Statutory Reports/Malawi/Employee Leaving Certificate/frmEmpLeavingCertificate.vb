#Region " Imports "

Imports eZeeCommonLib
Imports Aruti.Data

#End Region


Public Class frmEmpLeavingCertificate

#Region " Private Variables "

    Private ReadOnly mstrModuleName As String = "frmEmpLeavingCertificate"
    Private objEmpLeaving As clsEmpLeavingCertificate
    Private mdtPeriodEndDate As DateTime = Nothing
    Private mintYearUnkId As Integer = Nothing
    Private mstrDatabaseName As String = ""
    Private mstrSearchText As String = ""

#End Region

#Region " Contructor "

    Public Sub New()
        objEmpLeaving = New clsEmpLeavingCertificate(User._Object._Languageunkid,Company._Object._Companyunkid)
        objEmpLeaving.SetDefaultValue()
        InitializeComponent()
    End Sub

#End Region

#Region " Private Function "

    Private Sub FillCombo()
        Dim dsCombos As DataSet
        Try
            Dim objEmp As New clsEmployee_Master

            Dim dsList As DataSet = objEmp.GetEmployeeList(FinancialYear._Object._DatabaseName, _
                                                  User._Object._Userunkid, _
                                                  FinancialYear._Object._YearUnkid, _
                                                  Company._Object._Companyunkid, _
                                                  eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), _
                                                  eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), _
                                                  ConfigParameter._Object._UserAccessModeSetting, _
                                                  True, True, "Emp", True)

            dsCombos = objEmp.GetEmployeeList(FinancialYear._Object._DatabaseName, _
                                                User._Object._Userunkid, _
                                                FinancialYear._Object._YearUnkid, _
                                                Company._Object._Companyunkid, _
                                                eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), _
                                                eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), _
                                                ConfigParameter._Object._UserAccessModeSetting, _
                                                True, False, "Emp", False)

            Dim dtList As DataTable = ( _
                                            From t1 In dsList.Tables(0).AsEnumerable() _
                                            Group Join t2 In dsCombos.Tables(0).AsEnumerable() On t1.Field(Of Integer)("employeeunkid") Equals t2.Field(Of Integer)("employeeunkid") Into tg = Group _
                                            From tcheck In tg.DefaultIfEmpty() _
                                            Where tcheck Is Nothing _
                                            Select t1).CopyToDataTable()

            If dtList Is Nothing Then
                dtList = CType(dsList.Tables(0), DataTable).Select("employeeunkid = 0").CopyToDataTable()
            End If

            With cboEmployee
                .ValueMember = "employeeunkid"
                .DisplayMember = "employeename"
                .DataSource = dtList
                .SelectedValue = 0
            End With
            objEmp = Nothing


            Dim objTranHead As New clsTransactionHead
            dsCombos = objTranHead.getComboList(FinancialYear._Object._DatabaseName, "Taxes", True, enTranHeadType.EmployeesStatutoryDeductions, enCalcType.AsComputedOnWithINEXCESSOFTaxSlab, enTypeOf.Taxes)
            With cboPAYE
                .ValueMember = "tranheadunkid"
                .DisplayMember = "name"
                .DataSource = dsCombos.Tables(0)
                .SelectedValue = 0
            End With
            objTranHead = Nothing

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "FillCombo", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub ResetValue()
        Try
            cboEmployee.SelectedValue = 0
            cboMembership.SelectedValue = 0
            cboPAYE.SelectedValue = 0

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "ResetValue", mstrModuleName)
        End Try
    End Sub

    Private Function SetFilter() As Boolean
        Try
            objEmpLeaving.SetDefaultValue()

            If CInt(cboEmployee.SelectedValue) <= 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 1, "Employee is mandatory information. Please select Employee."), enMsgBoxStyle.Information)
                cboEmployee.Focus()
                Return False
            ElseIf CInt(cboMembership.SelectedValue) <= 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 2, "Membership is mandatory information. Please select Membership."), enMsgBoxStyle.Information)
                cboMembership.Focus()
                Return False
            ElseIf CInt(cboPAYE.SelectedValue) <= 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 3, "Slab Transaction Head is mandatory information. Please select Slab Transaction Head."), enMsgBoxStyle.Information)
                cboPAYE.Focus()
                Return False
            End If


            objEmpLeaving._EmpId = CInt(cboEmployee.SelectedValue)
            objEmpLeaving._EmpName = cboEmployee.Text
            objEmpLeaving._MembershipId = CInt(cboMembership.SelectedValue)
            objEmpLeaving._MembershipName = cboMembership.Text
            objEmpLeaving._MembershipNo = cboMembership.Tag.ToString()
            objEmpLeaving._PayeHeadID = CInt(cboPAYE.SelectedValue)
            objEmpLeaving._PayeHeadName = cboPAYE.Text

            Dim objMaster As New clsMasterData
            Dim mintPeriodId As Integer = objMaster.getCurrentPeriodID(enModuleReference.Payroll, ConfigParameter._Object._CurrentDateAndTime.Date, FinancialYear._Object._YearUnkid)
            objMaster = Nothing

            Dim objPeriod As New clscommom_period_Tran
            objPeriod._Periodunkid(FinancialYear._Object._DatabaseName) = mintPeriodId
            objEmpLeaving._PeriodId = mintPeriodId
            objEmpLeaving._Period = objPeriod._Period_Name
            objEmpLeaving._PeriodStartDate = objPeriod._Start_Date.Date
            objEmpLeaving._PeriodEndDate = objPeriod._End_Date.Date
            mdtPeriodEndDate = objPeriod._End_Date.Date
            objPeriod = Nothing


            Return True
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "SetFilter", mstrModuleName)
        End Try
    End Function

#End Region

#Region " Forms "

    Private Sub frmEmpLeavingCertificate_FormClosed(ByVal sender As Object, ByVal e As System.Windows.Forms.FormClosedEventArgs) Handles Me.FormClosed
        Try
            objEmpLeaving = Nothing
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmEmpLeavingCertificate_FormClosed", mstrModuleName)
        End Try
    End Sub

    Private Sub frmEmpLeavingCertificate_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            Call Language.setLanguage(Me.Name)

            Call OtherSettings()

            Me._Title = objEmpLeaving._ReportName
            Me._Message = objEmpLeaving._ReportDesc

            Call FillCombo()
            Call ResetValue()

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmEmpLeavingCertificate_Load", mstrModuleName)
        End Try
    End Sub

    Private Sub Form_KeyDown(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles Me.KeyDown
        Try
            If e.Control Then
                If e.KeyCode = Windows.Forms.Keys.R Then
                    Call frmEmpLeavingCertificate_Report_Click(Me, New Aruti.Data.PrintButtonEventArgs(enPrintAction.Preview))
                End If
            End If
        Catch ex As Exception
            DisplayError.Show(-1, ex.Message, "Form_KeyDown", mstrModuleName)
        End Try
    End Sub

    Private Sub Form_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles Me.KeyPress
        Try
            Select Case e.KeyChar
                Case CChar(ChrW(CInt(Windows.Forms.Keys.Enter)))
                    Windows.Forms.SendKeys.Send("{TAB}")
                    e.Handled = True
                    Exit Select
            End Select
        Catch ex As Exception
            DisplayError.Show(-1, ex.Message, "Form_KeyPress", mstrModuleName)
        End Try
    End Sub

#End Region

#Region " Buttons "

    Private Sub frmEmpLeavingCertificate_Report_Click(ByVal sender As Object, ByVal e As PrintButtonEventArgs) Handles Me.Report_Click
        Try
            If Not SetFilter() Then Exit Sub

            objEmpLeaving.generateReportNew(FinancialYear._Object._DatabaseName, _
                                         User._Object._Userunkid, _
                                         FinancialYear._Object._YearUnkid, _
                                         Company._Object._Companyunkid, _
                                         eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate).Date, _
                                         eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate).Date, _
                                         ConfigParameter._Object._UserAccessModeSetting, _
                                         True, ConfigParameter._Object._ExportReportPath, _
                                         ConfigParameter._Object._OpenAfterExport, 0, e.Type, enExportAction.None, ConfigParameter._Object._Base_CurrencyId)
        Catch ex As Exception
            DisplayError.Show(-1, ex.Message, "frmEmpLeavingCertificate_Report_Click", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub frmEmpLeavingCertificate_Export_Click(ByVal sender As System.Object, ByVal e As PrintButtonEventArgs) Handles Me.Export_Click
        Try
            If Not SetFilter() Then Exit Sub
            objEmpLeaving.generateReportNew(FinancialYear._Object._DatabaseName, _
                                         User._Object._Userunkid, _
                                         FinancialYear._Object._YearUnkid, _
                                         Company._Object._Companyunkid, _
                                          eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate).Date, _
                                         eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate).Date, _
                                         ConfigParameter._Object._UserAccessModeSetting, _
                                         True, ConfigParameter._Object._ExportReportPath, _
                                         ConfigParameter._Object._OpenAfterExport, 0, enPrintAction.None, e.Type, ConfigParameter._Object._Base_CurrencyId)
        Catch ex As Exception
            DisplayError.Show(-1, ex.Message, "frmEmpLeavingCertificate_Export_Click", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub frmEmpLeavingCertificate_Reset_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Reset_Click
        Try
            Call ResetValue()
        Catch ex As Exception
            DisplayError.Show(-1, ex.Message, "frmEmpLeavingCertificate_Reset_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub frmEmpLeavingCertificate_Cancel_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Cancel_Click
        Try
            Me.Close()
        Catch ex As Exception
            DisplayError.Show(-1, ex.Message, "frmEmpLeavingCertificate_Cancel_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub objbtnSearchEmployee_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnSearchEmployee.Click
        Dim frm As New frmCommonSearch
        Try
            frm.DataSource = cboEmployee.DataSource
            frm.ValueMember = cboEmployee.ValueMember
            frm.DisplayMember = cboEmployee.DisplayMember
            frm.CodeMember = "employeecode"
            If frm.DisplayDialog Then
                cboEmployee.SelectedValue = frm.SelectedValue
                cboEmployee.Focus()
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objbtnSearchEmployee_Click", mstrModuleName)
        Finally
            frm = Nothing
        End Try
    End Sub

    Private Sub objbtnSearchMembeship_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnSearchMembeship.Click
        Dim frm As New frmCommonSearch
        Try
            frm.DataSource = cboMembership.DataSource
            frm.ValueMember = cboMembership.ValueMember
            frm.DisplayMember = cboMembership.DisplayMember
            If frm.DisplayDialog Then
                cboMembership.SelectedValue = frm.SelectedValue
                cboMembership.Focus()
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objbtnSearchMembeship_Click", mstrModuleName)
        Finally
            frm = Nothing
        End Try
    End Sub

    Private Sub objbtnSearchPAYE_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnSearchPAYE.Click
        Dim frm As New frmCommonSearch
        Try
            frm.DataSource = cboPAYE.DataSource
            frm.ValueMember = cboPAYE.ValueMember
            frm.DisplayMember = cboPAYE.DisplayMember
            If frm.DisplayDialog Then
                cboPAYE.SelectedValue = frm.SelectedValue
                cboPAYE.Focus()
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objbtnSearchPAYE_Click", mstrModuleName)
        Finally
            frm = Nothing
        End Try
    End Sub

    Private Sub Form_Language_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Me.Language_Click
        Dim objfrm As New frmLanguage
        Try
            If User._Object._Isrighttoleft = True Then
                objfrm.RightToLeft = Windows.Forms.RightToLeft.Yes
                objfrm.RightToLeftLayout = True
                Call Language.ctlRightToLeftlayOut(objfrm)
            End If

            Call SetMessages()

            clsEmpLeavingCertificate.SetMessages()
            objfrm._Other_ModuleNames = "clsEmpLeavingCertificate"
            objfrm.displayDialog(Me)

            Call Language.setLanguage(Me.Name)
            Call SetLanguage()

        Catch ex As System.Exception
            Call DisplayError.Show(-1, ex.Message, "Form_Language_Click", mstrModuleName)
        Finally
            objfrm.Dispose()
            objfrm = Nothing
        End Try
    End Sub

#End Region

#Region "Combobox Events"

    Private Sub cboEmployee_SelectedValueChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cboEmployee.SelectedValueChanged
        Try
            Dim objEmpMem As New clsMembershipTran
            objEmpMem._EmployeeUnkid = CInt(cboEmployee.SelectedValue)
            Dim dtTable As DataTable = objEmpMem._DataList

            Dim drRow As DataRow = dtTable.NewRow()
            drRow("membershipunkid") = 0
            drRow("membership") = Language.getMessage(mstrModuleName, 1, "Select")
            dtTable.Rows.InsertAt(drRow, 0)


            With cboMembership
                .ValueMember = "membershipunkid"
                .DisplayMember = "membership"
                .DataSource = dtTable
                .SelectedValue = 0
            End With

            objEmpMem = Nothing
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "cboEmployee_SelectedValueChanged", mstrModuleName)
        End Try
    End Sub

    Private Sub cboMembership_SelectedValueChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cboMembership.SelectedValueChanged
        Try
            cboMembership.Tag = Nothing
            Dim drRow As DataRowView = CType(cboMembership.SelectedItem, DataRowView)
            If drRow IsNot Nothing Then
                cboMembership.Tag = drRow("membershipno").ToString()
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "cboMembership_SelectedValueChanged", mstrModuleName)
        End Try
    End Sub

#End Region

    '<Language> This Auto Generated Text Please Do Not Modify it.
#Region " Language & UI Settings "
    Private Sub OtherSettings()
        Try
            Me.SuspendLayout()
            Call SetLanguage()

            Me.gbFilterCriteria.GradientColor = GUI._eZeeContainerHeaderBackColor
            Me.gbFilterCriteria.ForeColor = GUI._eZeeContainerHeaderForeColor


            Me.ResumeLayout()
        Catch Ex As Exception
            DisplayError.Show("-1", Ex.Message, "OtherSettings", mstrModuleName)
        End Try
    End Sub


    Private Sub SetLanguage()
        Try
            Me.Text = Language._Object.getCaption(Me.Name, Me.Text)

            Me.lblEmployee.Text = Language._Object.getCaption(Me.lblEmployee.Name, Me.lblEmployee.Text)
            Me.lblMembership.Text = Language._Object.getCaption(Me.lblMembership.Name, Me.lblMembership.Text)
            Me.gbFilterCriteria.Text = Language._Object.getCaption(Me.gbFilterCriteria.Name, Me.gbFilterCriteria.Text)
            Me.lblPAYE.Text = Language._Object.getCaption(Me.lblPAYE.Name, Me.lblPAYE.Text)

        Catch Ex As Exception
            DisplayError.Show("-1", Ex.Message, "SetLanguage", mstrModuleName)
        End Try
    End Sub


    Private Sub SetMessages()
        Try
            Language.setMessage(mstrModuleName, 1, "Employee is mandatory information. Please select Employee.")
            Language.setMessage(mstrModuleName, 2, "Membership is mandatory information. Please select Membership.")
            Language.setMessage(mstrModuleName, 3, "Slab Transaction Head is mandatory information. Please select Slab Transaction Head.")
            Language.setMessage(mstrModuleName, 4, "Type to Search")

        Catch Ex As Exception
            DisplayError.Show("-1", Ex.Message, "SetMessages", mstrModuleName)
        End Try
    End Sub
#End Region 'Language & UI Settings
    '</Language>


End Class
