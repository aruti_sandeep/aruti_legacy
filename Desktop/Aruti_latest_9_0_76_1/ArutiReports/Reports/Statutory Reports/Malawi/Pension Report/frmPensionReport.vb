﻿#Region " Imports "

Imports Aruti.Data
Imports eZeeCommonLib

#End Region

Public Class frmPensionReport

#Region " Private Variables "

    Private ReadOnly mstrModuleName As String = "frmPensionReport"
    Private objPensionReport As clsPensionReport
    Private mintFirstOpenPeriod As Integer = 0

#End Region

#Region " Contructor "

    Public Sub New()

        objPensionReport = New clsPensionReport(User._Object._Languageunkid, Company._Object._Companyunkid)
        objPensionReport.SetDefaultValue()
        InitializeComponent()
    End Sub

#End Region

#Region " Private Enum "
    Private Enum enHeadTypeId
        Membership = 1
        EmployerContribution = 2
        GroupLifeAssurance = 3
        AdministrationFees = 4
        FuneralCover = 5
    End Enum
#End Region

#Region " Form's Event(s) "

    Private Sub frmPensionReport_FormClosed(ByVal sender As Object, ByVal e As System.Windows.Forms.FormClosedEventArgs) Handles Me.FormClosed
        Try
            objPensionReport = Nothing
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmPensionReport_FormClosed", mstrModuleName)
        End Try
    End Sub

    Private Sub frmPensionReport_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            Call Language.setLanguage(Me.Name)

            eZeeHeader.Title = objPensionReport._ReportName
            eZeeHeader.Message = objPensionReport._ReportDesc

            OtherSettings()
            Call FillCombo()
            Call ResetValue()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmPensionReport_Load", mstrModuleName)
        End Try
    End Sub

    Private Sub Form_KeyDown(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles Me.KeyDown
        Try
            If e.Control Then
                If e.KeyCode = Windows.Forms.Keys.E Then

                End If
            End If
        Catch ex As Exception
            DisplayError.Show(-1, ex.Message, "Form_KeyDown", mstrModuleName)
        End Try
    End Sub

    Private Sub Form_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles Me.KeyPress
        Try
            Select Case e.KeyChar
                Case CChar(ChrW(CInt(Windows.Forms.Keys.Enter)))
                    Windows.Forms.SendKeys.Send("{TAB}")
                    e.Handled = True
                    Exit Select
            End Select
        Catch ex As Exception
            DisplayError.Show(-1, ex.Message, "Form_KeyPress", mstrModuleName)
        End Try
    End Sub

#End Region

#Region " Private Function "

    Private Sub FillCombo()
        Dim objEmp As New clsEmployee_Master
        Dim objMaster As New clsMasterData
        Dim objperiod As New clscommom_period_Tran
        Dim objMember As New clsmembership_master
        Dim objHead As New clsTransactionHead
        Dim dsList As New DataSet
        Try
            dsList = objEmp.GetEmployeeList(FinancialYear._Object._DatabaseName, _
                                           User._Object._Userunkid, _
                                           FinancialYear._Object._YearUnkid, _
                                           Company._Object._Companyunkid, _
                                           eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), _
                                           eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), _
                                           ConfigParameter._Object._UserAccessModeSetting, _
                                          True, True, "Emp", True)
            With cboEmployee
                .ValueMember = "employeeunkid"
                .DisplayMember = "employeename"
                .DataSource = dsList.Tables("Emp")
                .SelectedValue = 0
            End With

            mintFirstOpenPeriod = objMaster.getFirstPeriodID(enModuleReference.Payroll, FinancialYear._Object._YearUnkid, 1)
            dsList = objperiod.getListForCombo(enModuleReference.Payroll, FinancialYear._Object._YearUnkid, FinancialYear._Object._DatabaseName, FinancialYear._Object._Database_Start_Date, "Period", True)
            With cboPeriod
                .ValueMember = "periodunkid"
                .DisplayMember = "name"
                .DataSource = dsList.Tables(0)
                .SelectedValue = mintFirstOpenPeriod
            End With

            dsList = objMember.getListForCombo("Membership", True, , 1)
            With cboMembership
                .ValueMember = "membershipunkid"
                .DisplayMember = "name"
                .DataSource = dsList.Tables(0)
                .SelectedValue = 0
            End With


            dsList = objHead.getComboList(FinancialYear._Object._DatabaseName, "Head", True)
            With cboEmployerContribution
                .ValueMember = "tranheadunkid"
                .DisplayMember = "Name"
                .DataSource = dsList.Tables("Head").Copy()
                .SelectedValue = 0
            End With

            With cboGroupLifeAssurance
                .ValueMember = "tranheadunkid"
                .DisplayMember = "Name"
                .DataSource = dsList.Tables("Head").Copy()
                .SelectedValue = 0
            End With

            With cboAdministrationFees
                .ValueMember = "tranheadunkid"
                .DisplayMember = "Name"
                .DataSource = dsList.Tables("Head").Copy()
                .SelectedValue = 0
            End With

            With cboFuneralCover
                .ValueMember = "tranheadunkid"
                .DisplayMember = "Name"
                .DataSource = dsList.Tables("Head").Copy()
                .SelectedValue = 0
            End With


        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "FillCombo", mstrModuleName)
        Finally
            objEmp = Nothing : objMaster = Nothing : objperiod = Nothing
            objMember = Nothing : objHead = Nothing : dsList.Dispose()
        End Try
    End Sub

    Private Sub ResetValue()
        Try
            cboPeriod.SelectedValue = mintFirstOpenPeriod
            cboEmployee.SelectedValue = 0

            Call GetValue()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "ResetValue", mstrModuleName)
        End Try
    End Sub

    Private Sub GetValue()
        Dim objUserDefRMode As New clsUserDef_ReportMode
        Dim dsList As DataSet
        Try
            dsList = objUserDefRMode.GetList("List", enArutiReport.Pension_Report)

            If dsList.Tables("List").Rows.Count > 0 Then

                For Each dsRow As DataRow In dsList.Tables("List").Rows
                    Select Case CInt(dsRow.Item("headtypeid"))

                        Case enHeadTypeId.Membership
                            cboMembership.SelectedValue = CInt(dsRow.Item("transactionheadid"))
                        Case enHeadTypeId.EmployerContribution
                            cboEmployerContribution.SelectedValue = CInt(dsRow.Item("transactionheadid"))
                        Case enHeadTypeId.GroupLifeAssurance
                            cboGroupLifeAssurance.SelectedValue = CInt(dsRow.Item("transactionheadid"))
                        Case enHeadTypeId.AdministrationFees
                            cboAdministrationFees.SelectedValue = CInt(dsRow.Item("transactionheadid"))
                        Case enHeadTypeId.FuneralCover
                            cboFuneralCover.SelectedValue = CInt(dsRow.Item("transactionheadid"))

                    End Select
                Next

            End If

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "GetValue", mstrModuleName)
        Finally
            objUserDefRMode = Nothing
        End Try
    End Sub

    Private Function SetFilter() As Boolean
        Try

            Call objPensionReport.SetDefaultValue()

            If CInt(cboMembership.SelectedValue) <= 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 1, "Membership is mandatory information. Please select Membership to continue."), enMsgBoxStyle.Information)
                cboMembership.Focus()
                Exit Function
            End If

            If CInt(cboPeriod.SelectedValue) <= 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 2, "Period is mandatory information. Please select Period to continue."), enMsgBoxStyle.Information)
                cboPeriod.Focus()
                Exit Function
            End If

            If CInt(cboMembership.SelectedValue) > 0 Then
                objPensionReport._MembershipId = cboMembership.SelectedValue
                objPensionReport._MembershipName = cboMembership.Text
            End If

            If CInt(cboPeriod.SelectedValue) > 0 Then
                objPensionReport._PeriodId = cboPeriod.SelectedValue
                objPensionReport._PeriodName = cboPeriod.Text
            End If

            objPensionReport._EmployeeId = CInt(cboEmployee.SelectedValue)
            objPensionReport._EmployeeName = cboEmployee.Text

            objPensionReport._EmployerContributionHeadId = CInt(cboEmployerContribution.SelectedValue)
            objPensionReport._GroupLifeAssurancenHeadId = CInt(cboGroupLifeAssurance.SelectedValue)
            objPensionReport._AdministrationFeesHeadId = CInt(cboAdministrationFees.SelectedValue)
            objPensionReport._FuneralCoverHeadId = CInt(cboFuneralCover.SelectedValue)

            Return True
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "SetFilter", mstrModuleName)
        End Try
    End Function

#End Region

#Region " Buttons "

    Private Sub objbtnSearchEmployee_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnSearchEmployee.Click
        Dim frm As New frmCommonSearch
        Try
            frm.DataSource = cboEmployee.DataSource
            frm.ValueMember = cboEmployee.ValueMember
            frm.DisplayMember = cboEmployee.DisplayMember
            frm.CodeMember = "employeecode"
            If frm.DisplayDialog Then
                cboEmployee.SelectedValue = frm.SelectedValue
                cboEmployee.Focus()
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objbtnSearchEmployee_Click", mstrModuleName)
        Finally
            If frm IsNot Nothing Then frm.Dispose()
        End Try
    End Sub

    Private Sub objbtnLanguage_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnLanguage.Click
        Dim objfrm As New frmLanguage
        Try
            If User._Object._Isrighttoleft = True Then
                objfrm.RightToLeft = Windows.Forms.RightToLeft.Yes
                objfrm.RightToLeftLayout = True
                Call Language.ctlRightToLeftlayOut(objfrm)
            End If

            Call SetMessages()

            clsPayrollReport.SetMessages()
            objfrm._Other_ModuleNames = "clsPensionReport"
            objfrm.displayDialog(Me)

            Call Language.setLanguage(Me.Name)
            Call SetLanguage()

        Catch ex As System.Exception
            Call DisplayError.Show(-1, ex.Message, "Form_Language_Click", mstrModuleName)
        Finally
            objfrm.Dispose()
            objfrm = Nothing
        End Try
    End Sub

    Private Sub btnSaveSelection_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSaveSelection.Click
        Dim objUserDefRMode As New clsUserDef_ReportMode
        Try
            If SetFilter() = False Then Exit Try

            For intHeadType As Integer = 1 To GetEnumHighestValue(Of enHeadTypeId)()
                objUserDefRMode = New clsUserDef_ReportMode()

                Dim intUnkid As Integer = -1

                objUserDefRMode._Reportunkid = enArutiReport.Pension_Report
                objUserDefRMode._Reporttypeid = 0
                objUserDefRMode._Reportmodeid = 0

                Select Case intHeadType

                    Case enHeadTypeId.Membership
                        objUserDefRMode._Headtypeid = intHeadType
                        objUserDefRMode._EarningTranHeadIds = cboMembership.SelectedValue.ToString
                    Case enHeadTypeId.EmployerContribution
                        objUserDefRMode._Headtypeid = intHeadType
                        objUserDefRMode._EarningTranHeadIds = cboEmployerContribution.SelectedValue.ToString
                    Case enHeadTypeId.GroupLifeAssurance
                        objUserDefRMode._Headtypeid = intHeadType
                        objUserDefRMode._EarningTranHeadIds = cboGroupLifeAssurance.SelectedValue.ToString
                    Case enHeadTypeId.AdministrationFees
                        objUserDefRMode._Headtypeid = intHeadType
                        objUserDefRMode._EarningTranHeadIds = cboAdministrationFees.SelectedValue.ToString
                    Case enHeadTypeId.FuneralCover
                        objUserDefRMode._Headtypeid = intHeadType
                        objUserDefRMode._EarningTranHeadIds = cboFuneralCover.SelectedValue.ToString

                        intUnkid = objUserDefRMode.isExist(enArutiReport.Pension_Report, 0, 0, intHeadType)

                End Select

                objUserDefRMode._Reportmodeunkid = intUnkid

                If intUnkid <= 0 Then
                    objUserDefRMode.Insert()
                Else
                    objUserDefRMode.Update()
                End If

            Next

            eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 4, "Selection Saved Successfully"), enMsgBoxStyle.Information)

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "btnSaveSelection_Click", mstrModuleName)
        Finally
            objUserDefRMode = Nothing
        End Try
    End Sub

    Private Sub btnClose_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnClose.Click
        Try
            Me.Close()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "btnClose_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub btnReset_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnReset.Click
        Try
            Call ResetValue()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "btnReset_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub btnExport_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnExport.Click
        Try
            If SetFilter() = False Then Exit Sub

            objPensionReport.Generate_DetailReport(FinancialYear._Object._DatabaseName, _
                                                   User._Object._Userunkid, _
                                                   FinancialYear._Object._YearUnkid, _
                                                   Company._Object._Companyunkid, _
                                                   eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate.ToString), _
                                                   eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate.ToString), _
                                                   ConfigParameter._Object._UserAccessModeSetting, _
                                                   True, False, ConfigParameter._Object._ExportReportPath, _
                                                   ConfigParameter._Object._OpenAfterExport, _
                                                   0, enExportAction.None)
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "btnExport_Click", mstrModuleName)
        End Try
    End Sub


#End Region


    
	'<Language> This Auto Generated Text Please Do Not Modify it.
#Region " Language & UI Settings "
	Private Sub OtherSettings()
		Try
			Me.SuspendLayout()

            Call SetLanguage()
			
			Me.gbFilterCriteria.GradientColor = GUI._eZeeContainerHeaderBackColor
			Me.gbFilterCriteria.ForeColor = GUI._eZeeContainerHeaderForeColor

            Me.eZeeHeader.GradientColor1 = GUI._HeaderBackColor1
			Me.eZeeHeader.GradientColor2 = GUI._HeaderBackColor2
			Me.eZeeHeader.BorderColor = GUI._HeaderBorderColor
			Me.eZeeHeader.HeaderTextForeColor = GUI._HeaderTitleFontColor
			Me.eZeeHeader.DescriptionForeColor = GUI._HeaderMessageFontColor

			Me.btnReset.GradientBackColor = GUI._ButttonBackColor
			Me.btnReset.GradientForeColor = GUI._ButttonFontColor

			Me.btnExport.GradientBackColor = GUI._ButttonBackColor
			Me.btnExport.GradientForeColor = GUI._ButttonFontColor

			Me.btnClose.GradientBackColor = GUI._ButttonBackColor
			Me.btnClose.GradientForeColor = GUI._ButttonFontColor

			Me.objbtnLanguage.GradientBackColor = GUI._ButttonBackColor
			Me.objbtnLanguage.GradientForeColor = GUI._ButttonFontColor

			Me.btnSaveSelection.GradientBackColor = GUI._ButttonBackColor
			Me.btnSaveSelection.GradientForeColor = GUI._ButttonFontColor

			Me.ResumeLayout()
		Catch Ex As Exception
            DisplayError.Show("-1", Ex.Message, "OtherSettings", mstrModuleName)
		End Try
	End Sub
			
			
	Private Sub SetLanguage()
		Try
			Me.Text = Language._Object.getCaption(Me.Name, Me.Text)
			
			Me.eZeeHeader.Title = Language._Object.getCaption(Me.eZeeHeader.Name & "_Title" , Me.eZeeHeader.Title)
			Me.eZeeHeader.Message = Language._Object.getCaption(Me.eZeeHeader.Name & "_Message" , Me.eZeeHeader.Message)
			Me.btnReset.Text = Language._Object.getCaption(Me.btnReset.Name, Me.btnReset.Text)
			Me.btnExport.Text = Language._Object.getCaption(Me.btnExport.Name, Me.btnExport.Text)
			Me.btnClose.Text = Language._Object.getCaption(Me.btnClose.Name, Me.btnClose.Text)
			Me.gbFilterCriteria.Text = Language._Object.getCaption(Me.gbFilterCriteria.Name, Me.gbFilterCriteria.Text)
			Me.btnSaveSelection.Text = Language._Object.getCaption(Me.btnSaveSelection.Name, Me.btnSaveSelection.Text)
			Me.lblPeriod.Text = Language._Object.getCaption(Me.lblPeriod.Name, Me.lblPeriod.Text)
			Me.lblEmployee.Text = Language._Object.getCaption(Me.lblEmployee.Name, Me.lblEmployee.Text)
			Me.lblMembership.Text = Language._Object.getCaption(Me.lblMembership.Name, Me.lblMembership.Text)
			Me.lblFuneralCover.Text = Language._Object.getCaption(Me.lblFuneralCover.Name, Me.lblFuneralCover.Text)
			Me.lblGroupLifeAssurance.Text = Language._Object.getCaption(Me.lblGroupLifeAssurance.Name, Me.lblGroupLifeAssurance.Text)
			Me.lblAdministrationFees.Text = Language._Object.getCaption(Me.lblAdministrationFees.Name, Me.lblAdministrationFees.Text)
			Me.lblEmployerContribution.Text = Language._Object.getCaption(Me.lblEmployerContribution.Name, Me.lblEmployerContribution.Text)
			
		Catch Ex As Exception
            DisplayError.Show("-1", Ex.Message, "SetLanguage", mstrModuleName)
		End Try
	End Sub
			
			
	Public Sub SetMessages()
		Try
			Language.setMessage(mstrModuleName, 1, "Membership is mandatory information. Please select Membership to continue.")
			Language.setMessage(mstrModuleName, 2, "Period is mandatory information. Please select Period to continue.")
			Language.setMessage(mstrModuleName, 4, "Selection Saved Successfully")
			
		Catch Ex As Exception
            DisplayError.Show("-1", Ex.Message, "SetMessages", mstrModuleName)
		End Try
	End Sub
#End Region 'Language & UI Settings
	'</Language>
End Class