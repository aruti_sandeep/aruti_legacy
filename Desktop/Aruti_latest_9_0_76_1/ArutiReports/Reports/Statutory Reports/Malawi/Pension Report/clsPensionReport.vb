﻿'************************************************************************************************************************************
'Class Name   : clsPensionReport.vb
'Purpose      :
'Created Date : 24 Dec 2024
'Written By   : Hemant Morker
'Modified     :
'************************************************************************************************************************************
#Region " Imports "

Imports Aruti.Data
Imports eZeeCommonLib

#End Region

Public Class clsPensionReport
    Inherits IReportData
    Private Shared ReadOnly mstrModuleName As String = "clsPensionReport"
    Private mstrReportId As String = enArutiReport.Pension_Report
    Dim objDataOperation As clsDataOperation

#Region " Constructor "

    Public Sub New(ByVal intLangId As Integer, ByVal intCompanyId As Integer)
        Me.setReportData(CInt(mstrReportId), intLangId, intCompanyId)
        Call Create_OnDetailReport()
    End Sub

#End Region

#Region " Private variables "

    Private mintMembershipId As Integer = 1
    Private mstrMembershipName As String = String.Empty

    Private mintPeriodId As Integer = -1
    Private mstrPeriodName As String = String.Empty

    Private mintEmployeeId As Integer = -1
    Private mstrEmployeeName As String = String.Empty

    Private mintEmployerContributionHeadId As Integer = -1
    Private mintGroupLifeAssurancenHeadId As Integer = -1
    Private mintAdministrationFeesHeadId As Integer = -1
    Private mintFuneralCoverHeadId As Integer = -1

    Private mintBaseCurrId As Integer = 0
    Private mintPaidCurrencyId As Integer = 0
    Private mstrAnalysis_Join As String = ""
    Private mstrAdvance_Filter As String = ""

    Private mstrExportedFileName As String = String.Empty

#End Region

#Region " Properties "

    Public WriteOnly Property _MembershipId() As Integer
        Set(ByVal value As Integer)
            mintMembershipId = value
        End Set
    End Property

    Public WriteOnly Property _MembershipName() As String
        Set(ByVal value As String)
            mstrMembershipName = value
        End Set
    End Property

    Public WriteOnly Property _PeriodId() As Integer
        Set(ByVal value As Integer)
            mintPeriodId = value
        End Set
    End Property

    Public WriteOnly Property _PeriodName() As String
        Set(ByVal value As String)
            mstrPeriodName = value
        End Set
    End Property

    Public WriteOnly Property _EmployeeId() As Integer
        Set(ByVal value As Integer)
            mintEmployeeId = value
        End Set
    End Property

    Public WriteOnly Property _EmployeeName() As String
        Set(ByVal value As String)
            mstrEmployeeName = value
        End Set
    End Property

    Public WriteOnly Property _EmployerContributionHeadId() As Integer
        Set(ByVal value As Integer)
            mintEmployerContributionHeadId = value
        End Set
    End Property

    Public WriteOnly Property _GroupLifeAssurancenHeadId() As Integer
        Set(ByVal value As Integer)
            mintGroupLifeAssurancenHeadId = value
        End Set
    End Property

    Public WriteOnly Property _AdministrationFeesHeadId() As Integer
        Set(ByVal value As Integer)
            mintAdministrationFeesHeadId = value
        End Set
    End Property

    Public WriteOnly Property _FuneralCoverHeadId() As Integer
        Set(ByVal value As Integer)
            mintFuneralCoverHeadId = value
        End Set
    End Property

    Public WriteOnly Property _BaseCurrencyId() As Integer
        Set(ByVal value As Integer)
            mintBaseCurrId = value
        End Set
    End Property

    Public WriteOnly Property _PaidCurrencyId() As Integer
        Set(ByVal value As Integer)
            mintPaidCurrencyId = value
        End Set
    End Property

    Public WriteOnly Property _Analysis_Join() As String
        Set(ByVal value As String)
            mstrAnalysis_Join = value
        End Set
    End Property

    Public WriteOnly Property _Advance_Filter() As String
        Set(ByVal value As String)
            mstrAdvance_Filter = value
        End Set
    End Property

    Public ReadOnly Property _ExportedFileName() As String
        Get
            Return mstrExportedFileName
        End Get
    End Property


#End Region

#Region "Public Function & Procedures "
    Public Sub SetDefaultValue()
        Try
            mintPeriodId = -1
            mstrPeriodName = String.Empty
            mintMembershipId = 0
            mstrMembershipName = String.Empty
            mintEmployeeId = -1
            mstrEmployeeName = String.Empty

            mintEmployerContributionHeadId = 0
            mintGroupLifeAssurancenHeadId = 0
            mintFuneralCoverHeadId = 0
            mintAdministrationFeesHeadId = 0


        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: SetDefaultValue; Module Name: " & mstrModuleName)
        End Try
    End Sub

    Public Overrides Sub generateReport(ByVal pintReportType As Integer, Optional ByVal PrintAction As Aruti.Data.enPrintAction = Aruti.Data.enPrintAction.Preview, Optional ByVal ExportAction As Aruti.Data.enExportAction = Aruti.Data.enExportAction.None)

    End Sub

    Public Overrides Sub generateReportNew(ByVal xDatabaseName As String, ByVal xUserUnkid As Integer, ByVal xYearUnkid As Integer, ByVal xCompanyUnkid As Integer, ByVal xPeriodStart As Date, ByVal xPeriodEnd As Date, ByVal xUserModeSetting As String, ByVal xOnlyApproved As Boolean, ByVal xExportReportPath As String, ByVal xOpenReportAfterExport As Boolean, ByVal pintReportType As Integer, Optional ByVal PrintAction As Aruti.Data.enPrintAction = Aruti.Data.enPrintAction.Preview, Optional ByVal ExportAction As Aruti.Data.enExportAction = Aruti.Data.enExportAction.None, Optional ByVal intBaseCurrencyUnkid As Integer = 0)
        
    End Sub

    Public Overrides Sub setDefaultOrderBy(ByVal intReportType As Integer)

    End Sub

    Public Overrides Sub setOrderBy(ByVal intReportType As Integer)

    End Sub
#End Region

#Region " Report Generation "

    Dim iColumn_DetailReport As New IColumnCollection

    Private Sub Create_OnDetailReport()
        Try
            iColumn_DetailReport.Clear()
            iColumn_DetailReport.Add(New IColumn("", ""))
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: Create_OnDetailReport; Module Name: " & mstrModuleName)
        End Try
    End Sub

    Public Sub Generate_DetailReport(ByVal xDatabaseName As String, _
                                          ByVal xUserUnkid As Integer, _
                                          ByVal xYearUnkid As Integer, _
                                          ByVal xCompanyUnkid As Integer, _
                                          ByVal xPeriodStart As Date, _
                                          ByVal xPeriodEnd As Date, _
                                          ByVal xUserModeSetting As String, _
                                          ByVal xOnlyApproved As Boolean, _
                                          ByVal xIncludeIn_ActiveEmployee As Boolean, _
                                          ByVal xExportReportPath As String, _
                                          ByVal xOpenReportAfterExport As Boolean, _
                                          Optional ByVal PrintAction As Aruti.Data.enPrintAction = Aruti.Data.enPrintAction.Preview, _
                                          Optional ByVal ExportAction As Aruti.Data.enExportAction = Aruti.Data.enExportAction.None, _
                                          Optional ByVal intBaseCurrencyUnkid As Integer = 0 _
                                          )
        Dim dtCol As DataColumn
        Dim dtFinalTable As DataTable
        Dim dsList As New DataSet
        Dim StrQ As String = ""
        Dim StrEmpQ As String = ""
        Dim mdtTableExcel As New DataTable
        Dim exForce As Exception

        Try


            objDataOperation = New clsDataOperation
            dtFinalTable = New DataTable("Pension")

            dtCol = New DataColumn("MembershipNo", System.Type.GetType("System.String"))
            dtCol.Caption = Language.getMessage(mstrModuleName, 1, "PENSION NUMBER")
            dtCol.DefaultValue = ""
            dtFinalTable.Columns.Add(dtCol)

            dtCol = New DataColumn("Sr.No", System.Type.GetType("System.Int32"))
            dtCol.Caption = Language.getMessage(mstrModuleName, 2, "SERIAL NUMBER ")
            dtCol.DefaultValue = 0
            dtFinalTable.Columns.Add(dtCol)

            dtCol = New DataColumn("Surname", System.Type.GetType("System.String"))
            dtCol.Caption = Language.getMessage(mstrModuleName, 3, "SURNAME")
            dtCol.DefaultValue = ""
            dtFinalTable.Columns.Add(dtCol)

            dtCol = New DataColumn("FirstName", System.Type.GetType("System.String"))
            dtCol.Caption = Language.getMessage(mstrModuleName, 4, "FIRST NAME (S)")
            dtCol.DefaultValue = ""
            dtFinalTable.Columns.Add(dtCol)

            dtCol = New DataColumn("Gender", System.Type.GetType("System.String"))
            dtCol.Caption = Language.getMessage(mstrModuleName, 5, "GENDER")
            dtCol.DefaultValue = ""
            dtFinalTable.Columns.Add(dtCol)

            dtCol = New DataColumn("DOB", System.Type.GetType("System.String"))
            dtCol.Caption = Language.getMessage(mstrModuleName, 6, "DATE OF BIRTH (dd-mmm-yyyy)")
            dtCol.DefaultValue = ""
            dtFinalTable.Columns.Add(dtCol)

            dtCol = New DataColumn("MebershipJoinedDate", System.Type.GetType("System.String"))
            dtCol.Caption = Language.getMessage(mstrModuleName, 7, "DATE JOINED MEMBERSHIP (dd-mm-yyyy)")
            dtCol.DefaultValue = ""
            dtFinalTable.Columns.Add(dtCol)

            dtCol = New DataColumn("MobileNo", System.Type.GetType("System.String"))
            dtCol.Caption = Language.getMessage(mstrModuleName, 8, "MOBILE PHONE NUMBER ")
            dtCol.DefaultValue = ""
            dtFinalTable.Columns.Add(dtCol)

            dtCol = New DataColumn("BasicSalary", System.Type.GetType("System.Decimal"))
            dtCol.Caption = Language.getMessage(mstrModuleName, 9, "BASIC SALARY")
            dtCol.DefaultValue = 0
            dtFinalTable.Columns.Add(dtCol)

            dtCol = New DataColumn("EmployeeContribution", System.Type.GetType("System.Decimal"))
            dtCol.Caption = Language.getMessage(mstrModuleName, 10, "EMPLOYEE CONTRIBUTION (5.00%)")
            dtCol.DefaultValue = 0
            dtFinalTable.Columns.Add(dtCol)

            dtCol = New DataColumn("EmployerContribution", System.Type.GetType("System.Decimal"))
            dtCol.Caption = Language.getMessage(mstrModuleName, 11, "Employer Contribution (10.00%)")
            dtCol.DefaultValue = 0
            dtFinalTable.Columns.Add(dtCol)

            dtCol = New DataColumn("GroupLifeAssurance", System.Type.GetType("System.Decimal"))
            dtCol.Caption = Language.getMessage(mstrModuleName, 12, "Group Life Assurance (0.63%) ")
            dtCol.DefaultValue = 0
            dtFinalTable.Columns.Add(dtCol)

            dtCol = New DataColumn("AdministrationFees", System.Type.GetType("System.Decimal"))
            dtCol.Caption = Language.getMessage(mstrModuleName, 13, "Administration Fees (0.07%)")
            dtCol.DefaultValue = 0
            dtFinalTable.Columns.Add(dtCol)

            dtCol = New DataColumn("FuneralCover", System.Type.GetType("System.Decimal"))
            dtCol.Caption = Language.getMessage(mstrModuleName, 14, "Funeral Cover (0.10%)")
            dtCol.DefaultValue = 0
            dtFinalTable.Columns.Add(dtCol)

            dtCol = New DataColumn("TotalEmployerContribution", System.Type.GetType("System.Decimal"))
            dtCol.Caption = Language.getMessage(mstrModuleName, 15, "TOTAL Employer's Contribution")
            dtCol.DefaultValue = 0
            dtFinalTable.Columns.Add(dtCol)

            dtCol = New DataColumn("GrandTotal", System.Type.GetType("System.Decimal"))
            dtCol.Caption = Language.getMessage(mstrModuleName, 16, "GRAND TOTAL")
            dtCol.DefaultValue = 0
            dtFinalTable.Columns.Add(dtCol)


            Dim objExchangeRate As New clsExchangeRate
            Dim decDecimalPlaces As Decimal = 0

            objExchangeRate._ExchangeRateunkid = 1  ' intBaseCurrencyId
            decDecimalPlaces = objExchangeRate._Digits_After_Decimal

            Dim xUACQry, xUACFiltrQry As String : xUACQry = "" : xUACFiltrQry = ""
            Call NewAccessLevelFilterString(xUACQry, xUACFiltrQry, xPeriodEnd, xOnlyApproved, xDatabaseName, xUserUnkid, xCompanyUnkid, xYearUnkid, xUserModeSetting)

            StrEmpQ = "SELECT   hremployee_master.employeeunkid " & _
                      ", hremployee_master.employeecode AS employeecode " & _
                      ", ISNULL(hremployee_master.surname, '') SURNAME " & _
                      ", ISNULL(hremployee_master.firstname, '') AS FIRSTNAME " & _
                      ", hremployee_master.birthdate " & _
                      ", ISNULL(CASE WHEN gender = 1 THEN @Male WHEN gender = 2 THEN @Female END, '') AS 'Gender' " & _
                      ", hremployee_master.present_mobile "

            StrEmpQ &= "INTO #TableEmp "
            StrEmpQ &= "FROM hremployee_master "

            StrEmpQ &= mstrAnalysis_Join


            If xUACQry.Trim.Length > 0 Then
                StrEmpQ &= xUACQry
            End If

            StrEmpQ &= "WHERE 1 = 1 "

            If mintEmployeeId > 0 Then
                StrEmpQ &= "	AND hremployee_master.employeeunkid = @EmpId "
            End If

            If xUACFiltrQry.Trim.Length > 0 Then
                StrEmpQ &= " AND " & xUACFiltrQry
            End If

            StrQ = StrEmpQ

            StrQ &= "SELECT " & _
                      "  ISNULL(E.MEM_NO, '') AS MEM_NO " & _
                      ", ISNULL(#TableEmp.surname, '') SURNAME " & _
                      ", ISNULL(#TableEmp.firstname, '') AS FIRSTNAME " & _
                      ", ISNULL(#TableEmp.Gender, '') AS GENDER " & _
                      ", CAST(ISNULL(#TableEmp.birthdate, '') AS NVARCHAR(MAX)) AS DOB " & _
                      ", CAST(ISNULL(E.ISSUE_DATE, '') AS NVARCHAR(MAX)) AS MEM_ISSUE_DATE " & _
                      ", ISNULL(#TableEmp.present_mobile, '') AS MOBILE " & _
                      ", ISNULL(E.EAmount, 0) AS EMP_SHARE " & _
                      ", #TableEmp.employeeunkid AS EmpId " & _
                      ", #TableEmp.employeecode AS employeecode " & _
                      ", NGross.BasicSalary " & _
                      ", NGross.EmployerContribution " & _
                      ", NGross.GroupLifeAssurance " & _
                      ", NGross.AdministrationFees " & _
                      ", NGross.FuneralCover " & _
                      ", NGross.EmployerContribution + NGross.GroupLifeAssurance + NGross.AdministrationFees + NGross.FuneralCover AS TotalEmployerContribution " & _
                      ", E.EAmount + NGross.EmployerContribution + NGross.GroupLifeAssurance + NGross.AdministrationFees + NGross.FuneralCover AS GrandTotal " & _
                  "FROM #TableEmp "

            StrQ &= "JOIN " & _
                   "( " & _
                   "	SELECT " & _
                   "		 #TableEmp.employeeunkid AS EmpId " & _
                   "        , ISNULL(hremployee_meminfo_tran.membershipno, '') AS MEM_NO " & _
                   "        , ISNULL(hrmembership_master.employer_membershipno, '') AS EMPLOYER_MEM_NO " & _
                   "        , ISNULL(hremployee_meminfo_tran.issue_date, '') AS ISSUE_DATE " & _
                   "		,SUM(ISNULL(CAST(prpayrollprocess_tran.amount AS DECIMAL(36," & decDecimalPlaces & ")), 0)) AS EAmount " & _
                   "	FROM prpayrollprocess_tran " & _
                   "		JOIN #TableEmp ON prpayrollprocess_tran.employeeunkid = #TableEmp.employeeunkid " & _
                   "			JOIN hremployee_meminfo_tran ON prpayrollprocess_tran.membershiptranunkid = hremployee_meminfo_tran.membershiptranunkid AND ISNULL(hremployee_meminfo_tran.isdeleted,0) = 0 " & _
                   "			LEFT JOIN hrmembership_master ON hremployee_meminfo_tran.membershipunkid = hrmembership_master.membershipunkid AND prpayrollprocess_tran.tranheadunkid = hrmembership_master.emptranheadunkid " & _
                   "			JOIN prtnaleave_tran ON prpayrollprocess_tran.tnaleavetranunkid = prtnaleave_tran.tnaleavetranunkid " & _
                   "			LEFT JOIN cfcommon_period_tran ON prtnaleave_tran.payperiodunkid = cfcommon_period_tran.periodunkid AND ISNULL(cfcommon_period_tran.isactive,0) = 0 " & _
                   "		WHERE prpayrollprocess_tran.isvoid = 0 " & _
                   "			AND prtnaleave_tran.isvoid = 0 " & _
                   "			AND prtnaleave_tran.payperiodunkid = @PeriodId AND hrmembership_master.membershipunkid = @MemId " & _
                   "       GROUP BY #TableEmp.employeeunkid, ISNULL(hremployee_meminfo_tran.membershipno, ''), ISNULL(hrmembership_master.employer_membershipno, ''), ISNULL(hremployee_meminfo_tran.issue_date, '') " & _
                   ") AS E ON E.EmpId = #TableEmp.employeeunkid " & _
                   "LEFT JOIN " & _
                   "( " & _
                   "	SELECT " & _
                   "	     Payroll.Empid " & _
                   "	    ,SUM(Payroll.BasicSalary) AS BasicSalary " & _
                   "	    ,SUM(Payroll.EmployerContribution) AS EmployerContribution " & _
                   "	    ,SUM(Payroll.GroupLifeAssurance) AS GroupLifeAssurance " & _
                   "	    ,SUM(Payroll.AdministrationFees) AS AdministrationFees " & _
                   "	    ,SUM(Payroll.FuneralCover) AS FuneralCover " & _
                   "	FROM " & _
                   "	( " & _
                   "		SELECT " & _
                   "			 prpayrollprocess_tran.employeeunkid AS Empid " & _
                   "			,CAST(ISNULL(prpayrollprocess_tran.amount, 0) AS DECIMAL(36," & decDecimalPlaces & ")) AS BasicSalary " & _
                   "			,0 AS EmployerContribution " & _
                   "			,0 AS GroupLifeAssurance " & _
                   "			,0 AS AdministrationFees " & _
                   "			,0 AS FuneralCover " & _
                   "		FROM prpayrollprocess_tran " & _
                   "			JOIN #TableEmp ON prpayrollprocess_tran.employeeunkid = #TableEmp.employeeunkid "
            StrQ &= "			JOIN prtnaleave_tran ON prtnaleave_tran.tnaleavetranunkid = prpayrollprocess_tran.tnaleavetranunkid " & _
                    "           JOIN prtranhead_master ON prtranhead_master.tranheadunkid = prpayrollprocess_tran.tranheadunkid " & _
                    "		WHERE prtranhead_master.typeof_id = " & enTypeOf.Salary & " " & _
                    "			AND prpayrollprocess_tran.isvoid = 0 " & _
                    "			AND prtranhead_master.isvoid = 0 " & _
                    "			AND prtnaleave_tran.isvoid = 0 AND prtnaleave_tran.payperiodunkid = @PeriodId "

            'Employer Contribution
            StrQ &= "                   UNION ALL " & _
                                   "SELECT  prpayrollprocess_tran.employeeunkid AS Empid  " & _
                                            ",0 AS BasicSalary " & _
                                            ",CAST(ISNULL(prpayrollprocess_tran.amount, 0) AS DECIMAL(36, " & decDecimalPlaces & ")) AS EmployerContribution " & _
                                            ",0 AS GroupLifeAssurance " & _
                                            ",0 AS AdministrationFees " & _
                                            ",0 AS FuneralCover " & _
                                   "FROM   prpayrollprocess_tran " & _
                                           "LEFT JOIN prtnaleave_tran ON prpayrollprocess_tran.tnaleavetranunkid = prtnaleave_tran.tnaleavetranunkid " & _
                                           "LEFT JOIN hremployee_master ON prpayrollprocess_tran.employeeunkid = hremployee_master.employeeunkid " & _
                                           "LEFT JOIN cfcommon_period_tran ON prtnaleave_tran.payperiodunkid = cfcommon_period_tran.periodunkid " & _
                                                   "AND ISNULL(cfcommon_period_tran.isactive,0) = 1 "

            StrQ &= mstrAnalysis_Join

            If xUACQry.Trim.Length > 0 Then
                StrQ &= xUACQry
            End If

            StrQ &= "                   WHERE   prpayrollprocess_tran.isvoid = 0 " & _
                                                "AND prtnaleave_tran.isvoid = 0 " & _
                                                "AND prtnaleave_tran.payperiodunkid = " & mintPeriodId & _
                                                "AND prpayrollprocess_tran.tranheadunkid = @EmployerContributionHeadunkid "

            If xUACFiltrQry.Trim.Length > 0 Then
                StrQ &= " AND " & xUACFiltrQry
            End If

            'Group Life Assurance
            StrQ &= "                   UNION ALL " & _
                                   "SELECT  prpayrollprocess_tran.employeeunkid AS Empid  " & _
                                            ",0 AS BasicSalary " & _
                                            ",0 AS EmployerContribution " & _
                                            ",CAST(ISNULL(prpayrollprocess_tran.amount, 0) AS DECIMAL(36, " & decDecimalPlaces & ")) AS GroupLifeAssurance " & _
                                            ",0 AS AdministrationFees " & _
                                            ",0 AS FuneralCover " & _
                                   "FROM   prpayrollprocess_tran " & _
                                           "LEFT JOIN prtnaleave_tran ON prpayrollprocess_tran.tnaleavetranunkid = prtnaleave_tran.tnaleavetranunkid " & _
                                           "LEFT JOIN hremployee_master ON prpayrollprocess_tran.employeeunkid = hremployee_master.employeeunkid " & _
                                           "LEFT JOIN cfcommon_period_tran ON prtnaleave_tran.payperiodunkid = cfcommon_period_tran.periodunkid " & _
                                                   "AND ISNULL(cfcommon_period_tran.isactive,0) = 1 "

            StrQ &= mstrAnalysis_Join

            If xUACQry.Trim.Length > 0 Then
                StrQ &= xUACQry
            End If

            StrQ &= "                   WHERE   prpayrollprocess_tran.isvoid = 0 " & _
                                                "AND prtnaleave_tran.isvoid = 0 " & _
                                                "AND prtnaleave_tran.payperiodunkid = " & mintPeriodId & _
                                                "AND prpayrollprocess_tran.tranheadunkid = @GroupLifeAssuranceHeadunkid "

            If xUACFiltrQry.Trim.Length > 0 Then
                StrQ &= " AND " & xUACFiltrQry
            End If

            'Administration Fees
            StrQ &= "                   UNION ALL " & _
                                   "SELECT  prpayrollprocess_tran.employeeunkid AS Empid  " & _
                                            ",0 AS BasicSalary " & _
                                            ",0 AS EmployerContribution " & _
                                            ",0 AS GroupLifeAssurance " & _
                                            ",CAST(ISNULL(prpayrollprocess_tran.amount, 0) AS DECIMAL(36, " & decDecimalPlaces & ")) AS AdministrationFees " & _
                                            ",0 AS FuneralCover " & _
                                   "FROM   prpayrollprocess_tran " & _
                                           "LEFT JOIN prtnaleave_tran ON prpayrollprocess_tran.tnaleavetranunkid = prtnaleave_tran.tnaleavetranunkid " & _
                                           "LEFT JOIN hremployee_master ON prpayrollprocess_tran.employeeunkid = hremployee_master.employeeunkid " & _
                                           "LEFT JOIN cfcommon_period_tran ON prtnaleave_tran.payperiodunkid = cfcommon_period_tran.periodunkid " & _
                                                   "AND ISNULL(cfcommon_period_tran.isactive,0) = 1 "

            StrQ &= mstrAnalysis_Join

            If xUACQry.Trim.Length > 0 Then
                StrQ &= xUACQry
            End If

            StrQ &= "                   WHERE   prpayrollprocess_tran.isvoid = 0 " & _
                                                "AND prtnaleave_tran.isvoid = 0 " & _
                                                "AND prtnaleave_tran.payperiodunkid = " & mintPeriodId & _
                                                "AND prpayrollprocess_tran.tranheadunkid = @AdministrationFeesHeadunkid "

            If xUACFiltrQry.Trim.Length > 0 Then
                StrQ &= " AND " & xUACFiltrQry
            End If

            'Funeral Cover
            StrQ &= "                   UNION ALL " & _
                                   "SELECT  prpayrollprocess_tran.employeeunkid AS Empid  " & _
                                            ",0 AS BasicSalary " & _
                                            ",0 AS EmployerContribution " & _
                                            ",0 AS GroupLifeAssurance " & _
                                            ",0 AS AdministrationFees " & _
                                            ",CAST(ISNULL(prpayrollprocess_tran.amount, 0) AS DECIMAL(36, " & decDecimalPlaces & ")) AS FuneralCover " & _
                                   "FROM   prpayrollprocess_tran " & _
                                           "LEFT JOIN prtnaleave_tran ON prpayrollprocess_tran.tnaleavetranunkid = prtnaleave_tran.tnaleavetranunkid " & _
                                           "LEFT JOIN hremployee_master ON prpayrollprocess_tran.employeeunkid = hremployee_master.employeeunkid " & _
                                           "LEFT JOIN cfcommon_period_tran ON prtnaleave_tran.payperiodunkid = cfcommon_period_tran.periodunkid " & _
                                                   "AND ISNULL(cfcommon_period_tran.isactive,0) = 1 "

            StrQ &= mstrAnalysis_Join

            If xUACQry.Trim.Length > 0 Then
                StrQ &= xUACQry
            End If

            StrQ &= "                   WHERE   prpayrollprocess_tran.isvoid = 0 " & _
                                                "AND prtnaleave_tran.isvoid = 0 " & _
                                                "AND prtnaleave_tran.payperiodunkid = " & mintPeriodId & _
                                                "AND prpayrollprocess_tran.tranheadunkid = @FuneralCoverHeadunkid "

            If xUACFiltrQry.Trim.Length > 0 Then
                StrQ &= " AND " & xUACFiltrQry
            End If

            StrQ &= "		) AS Payroll " & _
                    "		GROUP BY Payroll.Empid " & _
                    "	) AS NGross ON NGross.Empid = #TableEmp.employeeunkid "

            StrQ &= " WHERE 1 = 1 "

            StrQ &= "ORDER BY #TableEmp.employeecode "

            StrQ &= "DROP TABLE #TableEmp "

            objDataOperation.AddParameter("@MemId", SqlDbType.Int, eZeeDataType.INT_SIZE, mintMembershipId)
            objDataOperation.AddParameter("@PeriodId", SqlDbType.Int, eZeeDataType.INT_SIZE, mintPeriodId)
            objDataOperation.AddParameter("@male", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage("clsMasterData", 73, "Male"))
            objDataOperation.AddParameter("@female", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage("clsMasterData", 74, "Female"))
            objDataOperation.AddParameter("@EmpId", SqlDbType.Int, eZeeDataType.INT_SIZE, mintEmployeeId)
            objDataOperation.AddParameter("@EmployerContributionHeadunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintEmployerContributionHeadId)
            objDataOperation.AddParameter("@GroupLifeAssuranceHeadunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintGroupLifeAssurancenHeadId)
            objDataOperation.AddParameter("@AdministrationFeesHeadunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintAdministrationFeesHeadId)
            objDataOperation.AddParameter("@FuneralCoverHeadunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintFuneralCoverHeadId)

            dsList = objDataOperation.ExecQuery(StrQ, "DataTable")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & " " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            Dim rpt_Row As DataRow = Nothing
            Dim drRow As DataRow
            Dim intRowCount As Integer = dsList.Tables(0).Rows.Count
            Dim intCount As Integer = 1
            For ii As Integer = 0 To intRowCount - 1
                drRow = dsList.Tables(0).Rows(ii)
                rpt_Row = dtFinalTable.NewRow

                rpt_Row.Item("MembershipNo") = drRow.Item("MEM_NO")
                rpt_Row.Item("Sr.No") = intCount
                rpt_Row.Item("Surname") = drRow.Item("SURNAME")
                rpt_Row.Item("FirstName") = drRow.Item("FIRSTNAME")
                rpt_Row.Item("Gender") = drRow.Item("GENDER")
                If Not IsDBNull(drRow.Item("DOB")) Then
                    rpt_Row.Item("DOB") = CDate(drRow.Item("DOB")).ToShortDateString
                End If
                If Not IsDBNull(drRow.Item("MEM_ISSUE_DATE")) AndAlso eZeeDate.convertDate(CDate(drRow.Item("MEM_ISSUE_DATE"))) <> "19000101" Then
                    rpt_Row.Item("MebershipJoinedDate") = Format(CDate(drRow.Item("MEM_ISSUE_DATE")), "dd-MM-yyyy")
                End If
                rpt_Row.Item("MobileNo") = drRow.Item("MOBILE")
                rpt_Row.Item("BasicSalary") = drRow.Item("BasicSalary")
                rpt_Row.Item("EmployeeContribution") = drRow.Item("EMP_SHARE")
                rpt_Row.Item("EmployerContribution") = drRow.Item("EmployerContribution")
                rpt_Row.Item("GroupLifeAssurance") = drRow.Item("GroupLifeAssurance")
                rpt_Row.Item("AdministrationFees") = drRow.Item("AdministrationFees")
                rpt_Row.Item("FuneralCover") = drRow.Item("FuneralCover")
                rpt_Row.Item("TotalEmployerContribution") = drRow.Item("TotalEmployerContribution")
                rpt_Row.Item("GrandTotal") = drRow.Item("GrandTotal")

                dtFinalTable.Rows.Add(rpt_Row)
                intCount = intCount + 1

            Next

            If dtFinalTable IsNot Nothing AndAlso dtFinalTable.Rows.Count <= 0 Then
                Dim dRow As DataRow = dtFinalTable.NewRow

                dtFinalTable.Rows.Add(dRow)
            End If

            mdtTableExcel = dtFinalTable

            Using package As New OfficeOpenXml.ExcelPackage()
                Dim worksheet As OfficeOpenXml.ExcelWorksheet = package.Workbook.Worksheets.Add("Data")
                Dim xTable As DataTable = mdtTableExcel.Copy
                For Each iCol As DataColumn In xTable.Columns
                    If iCol.Ordinal <> xTable.Columns.Count - 1 Then
                        xTable.Columns(iCol.Ordinal).ColumnName = iCol.Caption.Trim()
                    End If
                Next
                Dim intRowNo As Integer = 2
                worksheet.Cells(intRowNo, 5, intRowNo, 6).Merge = True
                worksheet.Cells(intRowNo, 5).Value = "Name of Fund :" & " " & mstrMembershipName
                worksheet.Cells(intRowNo, 5).Style.Font.Size = 14
                worksheet.Cells(intRowNo, 5).Style.Font.Bold = True
                worksheet.Cells(intRowNo, 5).Style.HorizontalAlignment = OfficeOpenXml.Style.ExcelHorizontalAlignment.Center
                worksheet.Cells(intRowNo, 8, intRowNo, 10).Merge = True
                worksheet.Cells(intRowNo, 8).Value = Me._CompanyName
                worksheet.Cells(intRowNo, 8).Style.Font.Size = 16
                worksheet.Cells(intRowNo, 8).Style.Font.Bold = True
                worksheet.Cells(intRowNo, 8).Style.HorizontalAlignment = OfficeOpenXml.Style.ExcelHorizontalAlignment.Center
                worksheet.Cells(intRowNo, 11, intRowNo, 12).Merge = True
                worksheet.Cells(intRowNo, 11).Value = "Fund Code : 226"
                worksheet.Cells(intRowNo, 11).Style.Font.Size = 14
                worksheet.Cells(intRowNo, 11).Style.Font.Bold = True
                worksheet.Cells(intRowNo, 11).Style.HorizontalAlignment = OfficeOpenXml.Style.ExcelHorizontalAlignment.Center
                worksheet.Cells(intRowNo, 13, intRowNo, 14).Merge = True
                worksheet.Cells(intRowNo, 13).Value = "MONTH:" & " " & mstrPeriodName
                worksheet.Cells(intRowNo, 13).Style.Font.Size = 14
                worksheet.Cells(intRowNo, 13).Style.Font.Bold = True
                worksheet.Cells(intRowNo, 13).Style.HorizontalAlignment = OfficeOpenXml.Style.ExcelHorizontalAlignment.Center

                intRowNo = intRowNo + 1

                intRowNo = intRowNo + 1
                worksheet.Cells("A" & intRowNo).LoadFromDataTable(xTable, True)
                worksheet.Cells("A" & intRowNo & ":XFD").AutoFilter = False
                worksheet.Cells(intRowNo, 1, intRowNo, xTable.Columns.Count).Style.Font.Bold = True

                intRowNo = intRowNo + 1
                worksheet.Cells(intRowNo, 9, intRowNo + xTable.Rows.Count + 1, xTable.Columns.Count).Style.Numberformat.Format = GUI.fmtCurrency

                intRowNo = intRowNo + xTable.Rows.Count
                worksheet.Cells(intRowNo, 1, intRowNo, 8).Merge = True
                worksheet.Cells(intRowNo, 1).Value = "Grand Total >>>"
                worksheet.Cells(intRowNo, 1).Style.Font.Bold = True
                worksheet.Cells(intRowNo, 1).Style.HorizontalAlignment = OfficeOpenXml.Style.ExcelHorizontalAlignment.Center

                worksheet.Cells(intRowNo, 9, intRowNo, xTable.Columns.Count).Style.Numberformat.Format = GUI.fmtCurrency
                Dim decTotalBasicSalary As Decimal = CDec(dtFinalTable.Compute("SUM(BasicSalary)", "1=1"))
                worksheet.Cells(intRowNo, 9).Value = decTotalBasicSalary
                worksheet.Cells(intRowNo, 9).Style.Font.Bold = True

                Dim decTotalEmployeeContribution As Decimal = CDec(dtFinalTable.Compute("SUM(EmployeeContribution)", "1=1"))
                worksheet.Cells(intRowNo, 10).Value = decTotalEmployeeContribution
                worksheet.Cells(intRowNo, 10).Style.Font.Bold = True

                Dim decTotalEmployerContribution As Decimal = CDec(dtFinalTable.Compute("SUM(EmployerContribution)", "1=1"))
                worksheet.Cells(intRowNo, 11).Value = decTotalEmployerContribution
                worksheet.Cells(intRowNo, 11).Style.Font.Bold = True

                Dim decTotalGroupLifeAssurance As Decimal = CDec(dtFinalTable.Compute("SUM(GroupLifeAssurance)", "1=1"))
                worksheet.Cells(intRowNo, 12).Value = decTotalGroupLifeAssurance
                worksheet.Cells(intRowNo, 12).Style.Font.Bold = True

                Dim decTotalAdministrationFees As Decimal = CDec(dtFinalTable.Compute("SUM(AdministrationFees)", "1=1"))
                worksheet.Cells(intRowNo, 13).Value = decTotalAdministrationFees
                worksheet.Cells(intRowNo, 13).Style.Font.Bold = True

                Dim decTotalFuneralCover As Decimal = CDec(dtFinalTable.Compute("SUM(FuneralCover)", "1=1"))
                worksheet.Cells(intRowNo, 14).Value = decTotalFuneralCover
                worksheet.Cells(intRowNo, 14).Style.Font.Bold = True

                Dim decTotalOfTotalEmployerContribution As Decimal = CDec(dtFinalTable.Compute("SUM(TotalEmployerContribution)", "1=1"))
                worksheet.Cells(intRowNo, 15).Value = decTotalOfTotalEmployerContribution
                worksheet.Cells(intRowNo, 15).Style.Font.Bold = True

                Dim decTotalOfGrandTotal As Decimal = CDec(dtFinalTable.Compute("SUM(GrandTotal)", "1=1"))
                worksheet.Cells(intRowNo, 16).Value = decTotalOfGrandTotal
                worksheet.Cells(intRowNo, 16).Style.Font.Bold = True

                intRowNo = intRowNo + 1

                worksheet.Cells(intRowNo, 9, intRowNo + 1, 16).Style.Numberformat.Format = GUI.fmtCurrency
                Dim decVATAmount As Decimal = decTotalAdministrationFees * 16.5 / 100
                worksheet.Cells(intRowNo, 9, intRowNo, 12).Merge = True
                worksheet.Cells(intRowNo, 9).Value = "VAT on Administration Fee @16.50%"
                worksheet.Cells(intRowNo, 13).Value = decVATAmount
                worksheet.Cells(intRowNo, 16).Value = decVATAmount

                intRowNo = intRowNo + 1

                worksheet.Cells(intRowNo, 9, intRowNo, 12).Merge = True
                worksheet.Cells(intRowNo, 9).Value = "Grand Total >>>"
                worksheet.Cells(intRowNo, 9).Style.Font.Bold = True
                worksheet.Cells(intRowNo, 16).Value = decTotalOfGrandTotal + decVATAmount
                worksheet.Cells(intRowNo, 16).Style.Font.Bold = True

                worksheet.Cells.AutoFitColumns(0)

                If Me._ReportName.Contains("/") = True Then
                    Me._ReportName = Me._ReportName.Replace("/", "_")
                End If

                If Me._ReportName.Contains("\") Then
                    Me._ReportName = Me._ReportName.Replace("\", "_")
                End If

                If Me._ReportName.Contains(":") Then
                    Me._ReportName = Me._ReportName.Replace(":", "_")
                End If
                Dim strExportFileName As String = ""
                strExportFileName = Me._ReportName.Trim.Replace(" ", "_") & "_" & Now.Date.ToString("yyyyMMdd") & Format(Now, "hhmmss")

                xExportReportPath = xExportReportPath & "\" & strExportFileName & ".xlsx"

                Dim fi As IO.FileInfo = New IO.FileInfo(xExportReportPath)
                package.SaveAs(fi)

                mstrExportedFileName = strExportFileName & ".xlsx"

            End Using

            If IO.File.Exists(xExportReportPath) Then
                If xOpenReportAfterExport Then
                    Process.Start(xExportReportPath)
                End If
            End If

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: Generate_DetailReport; Module Name: " & mstrModuleName)
        End Try
    End Sub

#End Region

End Class
