#Region " Imports "

Imports eZeeCommonLib
Imports Aruti.Data
Imports OfficeOpenXml

#End Region

Public Class clsStatementAndPaymentTaxReport
    Inherits IReportData

    Private Shared ReadOnly mstrModuleName As String = "clsStatementAndPaymentTaxReport"
    Private mstrReportId As String = enArutiReport.Statement_And_Payment_Of_Tax_Withheld_For_Employees
    Dim objDataOperation As clsDataOperation

#Region " Constructor "
    Public Sub New(ByVal intLangId As Integer, ByVal intCompanyId As Integer) 'S.SANDEEP |24-Sep-2020| -- START --END
        Me.setReportData(CInt(mstrReportId), intLangId, intCompanyId) 'S.SANDEEP |24-Sep-2020| -- START --END
    End Sub
#End Region

#Region " Private variables "

    Private mintEmployeeId As Integer = 0
    Private mstrEmployeeName As String = String.Empty
    Private mintIdentityId As Integer = 0
    Private mstrIdentityName As String = String.Empty
    Private mintPrimaryId As Integer = 0
    Private mintSecondaryId As Integer = 0
    Private mintDirectorsId As Integer = 0
    Private mintResidentalId As Integer = 0
    Private mstrResidentalName As String = String.Empty
    Private mblnFirstNamethenSurname As Boolean = ConfigParameter._Object._FirstNamethenSurname
    Private mintMembershipId As Integer = 1
    Private mstrMembershipName As String = String.Empty
    Private mintBasicPayHeadId As Integer = -1
    Private mintGrosPayHeadId As Integer = -1
    Private mstrDeductionHeadIds As String = String.Empty
    Private mintTaxableAmountHeadId As Integer = -1
    Private mintTaxPayableHeadId As Integer = -1
    Private mintPeriodId As Integer = -1
    Private mstrPeriodNames As String = String.Empty
    Private mintReportId As Integer = 0
    Private mblnIncludeInactiveEmp As Boolean = False
    Private mintAllocationId As Integer = -1


    Private menExportAction As enExportAction
    Private mdtTableExcel As DataTable

    'Private mstrAnalysis_Fields As String = ""
    'Private mstrAnalysis_Join As String = ""
    'Private mstrAnalysis_OrderBy As String = ""
    'Private mintViewIndex As Integer = -1
    'Private mstrViewByIds As String = String.Empty
    'Private mstrViewByName As String = String.Empty

    Private mstrAdvanceFilter As String = String.Empty



    Dim Rpt As CrystalDecisions.CrystalReports.Engine.ReportClass

    'S.SANDEEP |28-JUL-2021| -- START
    Private mblnShowEmployeeCode As Boolean = False
    'S.SANDEEP |28-JUL-2021| -- END

#End Region

#Region " Properties "

    Public WriteOnly Property _EmployeeId() As Integer
        Set(ByVal value As Integer)
            mintEmployeeId = value
        End Set
    End Property

    Public WriteOnly Property _EmployeeName() As String
        Set(ByVal value As String)
            mstrEmployeeName = value
        End Set
    End Property


    Public WriteOnly Property _IdentityId() As Integer
        Set(ByVal value As Integer)
            mintIdentityId = value
        End Set
    End Property

    Public WriteOnly Property _IdentityName() As String
        Set(ByVal value As String)
            mstrIdentityName = value
        End Set
    End Property

    Public WriteOnly Property _MembershipId() As Integer
        Set(ByVal value As Integer)
            mintMembershipId = value
        End Set
    End Property

    Public WriteOnly Property _MembershipName() As String
        Set(ByVal value As String)
            mstrMembershipName = value
        End Set
    End Property

    Public WriteOnly Property _PrimaryId() As Integer
        Set(ByVal value As Integer)
            mintPrimaryId = value
        End Set
    End Property

    Public WriteOnly Property _SecondaryId() As Integer
        Set(ByVal value As Integer)
            mintSecondaryId = value
        End Set
    End Property

    Public WriteOnly Property _DirectorsId() As Integer
        Set(ByVal value As Integer)
            mintDirectorsId = value
        End Set
    End Property

    Public WriteOnly Property _ResidentalId() As Integer
        Set(ByVal value As Integer)
            mintResidentalId = value
        End Set
    End Property

    Public WriteOnly Property _ResidentalName() As String
        Set(ByVal value As String)
            mstrResidentalName = value
        End Set
    End Property

    Public WriteOnly Property _BasicPayHeadId() As Integer
        Set(ByVal value As Integer)
            mintBasicPayHeadId = value
        End Set
    End Property

    Public WriteOnly Property _GrosPayHeadId() As Integer
        Set(ByVal value As Integer)
            mintGrosPayHeadId = value
        End Set
    End Property

    Public WriteOnly Property _DeductionHeadId() As String
        Set(ByVal value As String)
            mstrDeductionHeadIds = value
        End Set
    End Property

    Public WriteOnly Property _TaxableAmountHeadId() As Integer
        Set(ByVal value As Integer)
            mintTaxableAmountHeadId = value
        End Set
    End Property

    Public WriteOnly Property _TaxPayableHeadId() As Integer
        Set(ByVal value As Integer)
            mintTaxPayableHeadId = value
        End Set
    End Property

    Public WriteOnly Property _Period_Id() As Integer
        Set(ByVal value As Integer)
            mintPeriodId = value
        End Set
    End Property

    Public WriteOnly Property _Period_Names() As String
        Set(ByVal value As String)
            mstrPeriodNames = value
        End Set
    End Property

    Public WriteOnly Property _IncludeInactiveEmp() As Boolean
        Set(ByVal value As Boolean)
            mblnIncludeInactiveEmp = value
        End Set
    End Property

    'Public WriteOnly Property _Analysis_Fields() As String
    '    Set(ByVal value As String)
    '        mstrAnalysis_Fields = value
    '    End Set
    'End Property

    'Public WriteOnly Property _Analysis_Join() As String
    '    Set(ByVal value As String)
    '        mstrAnalysis_Join = value
    '    End Set
    'End Property

    'Public WriteOnly Property _Analysis_OrderBy() As String
    '    Set(ByVal value As String)
    '        mstrAnalysis_OrderBy = value
    '    End Set
    'End Property

    'Public WriteOnly Property _ViewIndex() As Integer
    '    Set(ByVal value As Integer)
    '        mintViewIndex = value
    '    End Set
    'End Property

    'Public WriteOnly Property _ViewByIds() As String
    '    Set(ByVal value As String)
    '        mstrViewByIds = value
    '    End Set
    'End Property

    'Public WriteOnly Property _ViewByName() As String
    '    Set(ByVal value As String)
    '        mstrViewByName = value
    '    End Set
    'End Property

    'Public WriteOnly Property _Report_GroupName() As String
    '    Set(ByVal value As String)
    '        mstrReport_GroupName = value
    '    End Set
    'End Property

    Public WriteOnly Property _ReportId() As Integer
        Set(ByVal value As Integer)
            mintReportId = value
        End Set
    End Property

    Public WriteOnly Property _FirstNamethenSurname() As Boolean
        Set(ByVal value As Boolean)
            mblnFirstNamethenSurname = value
        End Set
    End Property

    Public WriteOnly Property _AllocationId() As Integer
        Set(ByVal value As Integer)
            mintAllocationId = value
        End Set
    End Property

    Public WriteOnly Property _AdvanceFilter() As String
        Set(ByVal value As String)
            mstrAdvanceFilter = value
        End Set
    End Property

    'S.SANDEEP |28-JUL-2021| -- START
    Public WriteOnly Property _ShowEmployeeCode() As Boolean
        Set(ByVal value As Boolean)
            mblnShowEmployeeCode = value
        End Set
    End Property
    'S.SANDEEP |28-JUL-2021| -- END

#End Region

#Region "Public Function & Procedures "

    Public Sub SetDefaultValue()
        Try

            mintEmployeeId = 0
            mstrEmployeeName = ""

            mintIdentityId = 0
            mstrIdentityName = ""
            mintPrimaryId = 0
            mintSecondaryId = 0
            mintDirectorsId = 0
            mintResidentalId = 0
            mstrResidentalName = ""

            mintMembershipId = 0
            mstrMembershipName = ""

            mintPeriodId = -1
            mstrPeriodNames = ""

            mintBasicPayHeadId = -1
            mintGrosPayHeadId = -1
            mstrDeductionHeadIds = ""
            mintTaxableAmountHeadId = -1
            mintTaxPayableHeadId = -1

            mblnIncludeInactiveEmp = True

            mintReportId = -1

            'mstrAnalysis_Fields = ""
            'mstrAnalysis_Join = ""
            'mstrAnalysis_OrderBy = ""
            'mintViewIndex = -1
            'mstrViewByIds = ""

            mstrAdvanceFilter = ""

            'S.SANDEEP |28-JUL-2021| -- START
            mblnShowEmployeeCode = False
            'S.SANDEEP |28-JUL-2021| -- END

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: SetDefaultValue; Module Name: " & mstrModuleName)
        End Try
    End Sub

    Private Sub FilterTitleAndFilterQuery()
        Me._FilterQuery = ""
        Me._FilterTitle = ""
        Try

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: FilterTitleAndFilterQuery; Module Name: " & mstrModuleName)
        End Try
    End Sub

    Public Overloads Overrides Sub generateReport(ByVal pintReportType As Integer, Optional ByVal PrintAction As Aruti.Data.enPrintAction = Aruti.Data.enPrintAction.Preview, Optional ByVal ExportAction As Aruti.Data.enExportAction = Aruti.Data.enExportAction.None)

    End Sub

    Public Overrides Sub generateReportNew(ByVal xDatabaseName As String, ByVal xUserUnkid As Integer, ByVal xYearUnkid As Integer, ByVal xCompanyUnkid As Integer, ByVal xPeriodStart As Date, ByVal xPeriodEnd As Date, ByVal xUserModeSetting As String, ByVal xOnlyApproved As Boolean, ByVal xExportReportPath As String, ByVal xOpenReportAfterExport As Boolean, ByVal pintReportType As Integer, Optional ByVal PrintAction As enPrintAction = enPrintAction.Preview, Optional ByVal ExportAction As enExportAction = enExportAction.None, Optional ByVal xBaseCurrencyId As Integer = 0)
        Dim objRpt As CrystalDecisions.CrystalReports.Engine.ReportClass = Nothing
        Dim strReportExportFile As String = ""
        Try
            menExportAction = ExportAction
            mdtTableExcel = Nothing
            Dim objCompany As New clsCompany_Master
            objCompany._Companyunkid = xCompanyUnkid

            'S.SANDEEP |28-JUL-2021| -- START
            'objRpt = Generate_DetailReport(xDatabaseName, xUserUnkid, xYearUnkid, xCompanyUnkid, xPeriodStart, xPeriodEnd, xUserModeSetting, xOnlyApproved, xBaseCurrencyId)
            objRpt = Generate_DetailReport(xDatabaseName, xUserUnkid, xYearUnkid, xCompanyUnkid, xPeriodStart, xPeriodEnd, xUserModeSetting, xOnlyApproved, xBaseCurrencyId, ExportAction)
            'S.SANDEEP |28-JUL-2021| -- END


            Rpt = objRpt

            If Not IsNothing(objRpt) Then
                'S.SANDEEP |28-JUL-2021| -- START
                'Call ReportExecute(objRpt, PrintAction, ExportAction, xExportReportPath, xOpenReportAfterExport)
                If ExportAction <> enExportAction.ExcelXLSM Then
                Call ReportExecute(objRpt, PrintAction, ExportAction, xExportReportPath, xOpenReportAfterExport)
                Else
                    Dim iMainFile = Me._ReportName
                    If iMainFile.Contains("/") = True Then
                        iMainFile = iMainFile.Replace("/", "_")
                    End If

                    If iMainFile.Contains("\") Then
                        iMainFile = iMainFile.Replace("\", "_")
                    End If

                    If iMainFile.Contains(":") Then
                        iMainFile = iMainFile.Replace(":", "_")
                    End If
                    Dim strExportFileName As String = ""
                    strExportFileName = iMainFile.Trim.Replace(" ", "_") & "_" & Now.Date.ToString("yyyyMMdd") & Format(Now, "hhmmss")
                    xExportReportPath = xExportReportPath & "\" & strExportFileName & ".xlsm"

                    'Pinkal (02-Apr-2024) -- Start
                    'PAYE Changes For Statutory Reports.
                    'Dim strCols As String() = New String() {"Column87", "Column88", "Column4", "Column5", "Column6", "Column7", "Column81", "Column82", "Column83", "Column84", "Column85", "Column86"}
                    Dim strCols As String() = New String() {"Column87", "Column88", "Column5", "Column4", "Column7", "Column81", "Column82", "Column84", "Column15"}
                    'Pinkal (02-Apr-2024) -- End


                    mdtTableExcel = mdtTableExcel.DefaultView.ToTable(False, strCols)
                    For cidx As Integer = 0 To strCols.Length - 1
                        mdtTableExcel.Columns(strCols.GetValue(cidx).ToString()).SetOrdinal(cidx)
                    Next

                    If mdtTableExcel.Rows.Count <= 0 Then mdtTableExcel.Rows.Add(mdtTableExcel.NewRow())

                    Dim ifile As String = "payetemplate.xlsm"
                    Dim ipath As String = My.Computer.FileSystem.SpecialDirectories.Temp
                    If IO.File.Exists(IO.Path.Combine(ipath, ifile)) Then
                        Dim fsrc As IO.FileInfo = New IO.FileInfo(IO.Path.Combine(ipath, ifile))
                        Using src As ExcelPackage = New ExcelPackage(fsrc)

                            'src.Load(New IO.FileStream(fsrc.FullName, IO.FileMode.Open))

                            For Each worksheet As ExcelWorksheet In src.Workbook.Worksheets

                                'Pinkal (02-Apr-2024) -- Start
                                'PAYE Changes For Statutory Reports.
                                'If worksheet.Name.ToUpper() = "PAYE_RETURN_DETAILS" Then
                                'If objCompany._Tinno.Trim.Length > 0 Then
                                '    worksheet.Cells("B3").Value = objCompany._Tinno
                                'Else
                                '    worksheet.Cells("B3").Value = 0
                                'End If
                                If worksheet.Name.ToUpper() = "INSTRUCTIONS" Then

                                    'ElseIf worksheet.Name.ToUpper() = "PAYE" Then
                                ElseIf worksheet.Name.ToUpper() = "PAYE_EMPLOYEE" Then
                                    'Pinkal (02-Apr-2024) -- End

                                    Dim ht As Double = worksheet.Row(1).Height

                                    'Pinkal (02-Apr-2024) -- Start
                                    'PAYE Changes For Statutory Reports.
                                    'worksheet.InsertRow(5, mdtTableExcel.Rows.Count - 1, 5 + mdtTableExcel.Rows.Count - 1)
                                    'worksheet.Cells("B5").LoadFromDataTable(mdtTableExcel, False)
                                    'worksheet.Cells("C5:C" & 5 + mdtTableExcel.Rows.Count + 1 & "").Style.Numberformat.Format = "###-###-###"

                                    If objCompany._Tinno.Trim.Length > 0 Then
                                        worksheet.Cells("D1").Value = objCompany._Tinno
                                    End If

                                    worksheet.InsertRow(4, mdtTableExcel.Rows.Count - 1, 4 + mdtTableExcel.Rows.Count - 1)
                                    worksheet.Cells("B4").LoadFromDataTable(mdtTableExcel, False)
                                    worksheet.Cells("C4:C" & 5 + mdtTableExcel.Rows.Count + 1 & "").Style.Numberformat.Format = "###-###-###"
                                    'Pinkal (02-Apr-2024) -- End



                                    Dim istart As ExcelCellAddress = worksheet.Dimension.Start
                                    Dim iend As ExcelCellAddress = worksheet.Dimension.End

                                    For row As Integer = istart.Row To iend.Row
                                        worksheet.Row(row).Height = ht
                                    Next
                                    Dim intEndIndex As Integer = 0
                                    For row As Integer = istart.Row To iend.Row
                                        If row > 4 Then
                                            If worksheet.Cells("B" & row).Value Is Nothing Then Continue For

                                            If worksheet.Cells("B" & row).Value.ToString().ToUpper() = "TOTAL" Then
                                                intEndIndex = row
                                            End If
                                            If intEndIndex > 0 Then Exit For
                                            'Pinkal (02-Apr-2024) -- Start
                                            'PAYE Changes For Statutory Reports.
                                            'worksheet.Cells("J" & row).Formula = "=(H" & row & " + I" & row & ")"
                                            'worksheet.Cells("L" & row).Formula = "=(J" & row & " - K" & row & ")"
                                            'Pinkal (02-Apr-2024) -- End
                                        End If
                                    Next

                                    'worksheet.Cells("H" & mdtTableExcel.Rows.Count + 2).Formula = "H2+H" & mdtTableExcel.Rows.Count + 1
                                    'worksheet.Cells("I" & mdtTableExcel.Rows.Count + 2).Formula = "I2+I" & mdtTableExcel.Rows.Count + 1
                                    'worksheet.Cells("J" & mdtTableExcel.Rows.Count + 2).Formula = "J2+J" & mdtTableExcel.Rows.Count + 1
                                    'worksheet.Cells("K" & mdtTableExcel.Rows.Count + 2).Formula = "K2+K" & mdtTableExcel.Rows.Count + 1
                                    'worksheet.Cells("L" & mdtTableExcel.Rows.Count + 2).Formula = "L2+L" & mdtTableExcel.Rows.Count + 1

                                    'Pinkal (02-Apr-2024) -- Start
                                    'PAYE Changes For Statutory Reports.
                                    'worksheet.Cells("M" & intEndIndex).Formula = String.Format("Sum({0})", New ExcelAddress("M5:M" & (intEndIndex - 1)).Address)
                                    'worksheet.Cells("H5:H" & intEndIndex).Style.Numberformat.Format = GUI.fmtCurrency
                                    'worksheet.Cells("I5:I" & intEndIndex).Style.Numberformat.Format = GUI.fmtCurrency
                                    worksheet.Cells("G3:G" & intEndIndex).Style.Numberformat.Format = GUI.fmtCurrency
                                    worksheet.Cells("H3:H" & intEndIndex).Style.Numberformat.Format = GUI.fmtCurrency
                                    worksheet.Cells("I3:I" & intEndIndex).Style.Numberformat.Format = GUI.fmtCurrency
                                    'Pinkal (02-Apr-2024) -- End




                                    'Pinkal (02-Apr-2024) -- Start
                                    'PAYE Changes For Statutory Reports.
                                    'worksheet.Cells("J5:J" & intEndIndex).Style.Numberformat.Format = GUI.fmtCurrency
                                    'worksheet.Cells("K5:K" & intEndIndex).Style.Numberformat.Format = GUI.fmtCurrency
                                    'worksheet.Cells("L5:L" & intEndIndex).Style.Numberformat.Format = GUI.fmtCurrency
                                    'worksheet.Cells("M5:M" & intEndIndex).Style.Numberformat.Format = GUI.fmtCurrency
                                    'Pinkal (02-Apr-2024) -- End


                                    'worksheet.Cells("H" & mdtTableExcel.Rows.Count + 2).Style.VerticalAlignment = Style.ExcelVerticalAlignment.Center
                                    'worksheet.Cells("I" & mdtTableExcel.Rows.Count + 2).Style.VerticalAlignment = Style.ExcelVerticalAlignment.Center
                                    'worksheet.Cells("J" & mdtTableExcel.Rows.Count + 2).Style.VerticalAlignment = Style.ExcelVerticalAlignment.Center
                                    'worksheet.Cells("K" & mdtTableExcel.Rows.Count + 2).Style.VerticalAlignment = Style.ExcelVerticalAlignment.Center
                                    'worksheet.Cells("L" & mdtTableExcel.Rows.Count + 2).Style.VerticalAlignment = Style.ExcelVerticalAlignment.Center

                                    'Pinkal (02-Apr-2024) -- Start
                                    'PAYE Changes For Statutory Reports.
                                    'worksheet.Cells("M" & intEndIndex).Style.VerticalAlignment = Style.ExcelVerticalAlignment.Center
                                    'Pinkal (02-Apr-2024) -- End



                                End If
                                'worksheet.Protection.IsProtected = True
                                'worksheet.Protection.SetPassword("Tra2020@efiling")
                            Next
                            'src.Workbook.Protection.LockStructure = True
                            'src.Workbook.Protection.SetPassword("Tra2020@efiling")
                            Dim f As IO.FileInfo = New IO.FileInfo(xExportReportPath)
                            src.SaveAs(f)
                        End Using
                    End If

                    If IO.File.Exists(xExportReportPath) Then
                        If xOpenReportAfterExport Then
                            Process.Start(xExportReportPath)
                        End If
                    End If
                    If IO.File.Exists(IO.Path.Combine(ipath, ifile)) Then
                        IO.File.Delete(IO.Path.Combine(ipath, ifile))
                    End If
                End If
                'S.SANDEEP |28-JUL-2021| -- END
            End If

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: generateReportNew; Module Name: " & mstrModuleName)
        Finally
        End Try
    End Sub

    Public Overrides Sub setDefaultOrderBy(ByVal intReportType As Integer)
        OrderByDisplay = ""
        OrderByQuery = ""
        Try

            OrderByDisplay = iColumn_DetailReport.ColumnItem(0).DisplayName
            OrderByQuery = iColumn_DetailReport.ColumnItem(0).Name

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: setDefaultOrderBy; Module Name: " & mstrModuleName)
        End Try
    End Sub

    Public Overrides Sub setOrderBy(ByVal intReportType As Integer)
        Try

            Call OrderByExecute(iColumn_DetailReport)
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: setOrderBy; Module Name: " & mstrModuleName)
        End Try
    End Sub

#End Region

#Region " Report Generation "

    Dim iColumn_DetailReport As New IColumnCollection

    Public Property Field_OnDetailReport() As IColumnCollection
        Get
            Return iColumn_DetailReport
        End Get
        Set(ByVal value As IColumnCollection)
            iColumn_DetailReport = value
        End Set
    End Property

    Private Sub Create_OnDetailReport()
        Try
            iColumn_DetailReport.Clear()
            iColumn_DetailReport.Add(New IColumn("", ""))
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: Create_OnDetailReport; Module Name: " & mstrModuleName)
        End Try
    End Sub

    Private Function Generate_DetailReport(ByVal strDatabaseName As String, _
                                           ByVal intUserUnkid As Integer, _
                                           ByVal intYearUnkid As Integer, _
                                           ByVal intCompanyUnkid As Integer, _
                                           ByVal dtPeriodStart As Date, _
                                           ByVal dtPeriodEnd As Date, _
                                           ByVal strUserModeSetting As String, _
                                           ByVal blnOnlyApproved As Boolean, _
                                           ByVal intBaseCurrencyId As Integer, _
                                           ByVal ExportAction As enExportAction) As CrystalDecisions.CrystalReports.Engine.ReportClass 'S.SANDEEP |28-JUL-2021| -- START {ByVal ExportAction As enExportAction} -- END

        Dim objRpt As CrystalDecisions.CrystalReports.Engine.ReportClass = Nothing
        Dim StrQ As String = ""
        Dim dsList As New DataSet
        Dim dsBasGross As New DataSet
        Dim exForce As Exception
        Dim rpt_Data As ArutiReport.Designer.dsArutiReport
        Try
            objDataOperation = New clsDataOperation

            Dim objExchangeRate As New clsExchangeRate
            Dim decDecimalPlaces As Decimal = 0

            objExchangeRate._ExchangeRateunkid = intBaseCurrencyId
            decDecimalPlaces = objExchangeRate._Digits_After_Decimal

            Dim xUACQry, xUACFiltrQry As String : xUACQry = "" : xUACFiltrQry = ""

            Dim xDateJoinQry, xDateFilterQry, xAdvanceJoinQry As String
            xDateJoinQry = "" : xDateFilterQry = "" : xUACQry = "" : xUACFiltrQry = "" : xAdvanceJoinQry = ""
            If mblnIncludeInactiveEmp = False Then
                Call GetDatesFilterString(xDateJoinQry, xDateFilterQry, dtPeriodStart, dtPeriodEnd, , , strDatabaseName)
            End If
            Call NewAccessLevelFilterString(xUACQry, xUACFiltrQry, dtPeriodEnd, blnOnlyApproved, strDatabaseName, intUserUnkid, intCompanyUnkid, intYearUnkid, strUserModeSetting)

            Call GetAdvanceFilterQry(xAdvanceJoinQry, dtPeriodEnd, strDatabaseName)



            objDataOperation.ClearParameters()
            StrQ &= "SELECT  hremployee_master.employeeunkid,hremployee_master.employeecode AS employeecode "
            StrQ &= ",hremployee_master.employmenttypeunkid,hremployee_master.paytypeunkid "
            If mblnFirstNamethenSurname = True Then
            StrQ &= ",     ISNULL(hremployee_master.firstname,'')+' '+ISNULL(hremployee_master.othername,'')+' '+ISNULL(hremployee_master.surname,'') AS EName "
            Else
            StrQ &= ",     ISNULL(hremployee_master.surname,'')+' '+ISNULL(hremployee_master.firstname,'')+' '+ISNULL(hremployee_master.othername,'') AS EName "
            End If

            'Pinkal (02-Apr-2024) -- Start
            'PAYE Changes For Statutory Reports.

            StrQ &= " ,ADF.stationunkid " & _
                         " ,ADF.deptgroupunkid " & _
                         " ,ADF.departmentunkid " & _
                         " ,ADF.sectiongroupunkid " & _
                         " ,ADF.unitgroupunkid " & _
                         " ,ADF.unitunkid " & _
                         " ,ADF.teamunkid " & _
                         " ,ADF.classgroupunkid " & _
                         " ,ADF.classunkid "
            'Pinkal (02-Apr-2024) -- End


            StrQ &= "INTO #tblEmp "
            StrQ &= "FROM hremployee_master "

            'StrQ &= "LEFT JOIN " & _
            '                "( " & _
            '                "    SELECT " & _
            '                "         stationunkid " & _
            '                "        ,deptgroupunkid " & _
            '                "        ,departmentunkid " & _
            '                "        ,sectiongroupunkid " & _
            '                "        ,sectionunkid " & _
            '                "        ,unitgroupunkid " & _
            '                "        ,unitunkid " & _
            '                "        ,teamunkid " & _
            '                "        ,classgroupunkid " & _
            '                "        ,classunkid " & _
            '                "        ,employeeunkid " & _
            '                "        ,ROW_NUMBER()OVER(PARTITION BY employeeunkid ORDER BY effectivedate DESC) as Rno " & _
            '                "    FROM hremployee_transfer_tran " & _
            '                "    WHERE isvoid = 0 AND CONVERT(CHAR(8),effectivedate,112) <= '" & eZeeDate.convertDate(dtPeriodEnd) & "' " & _
            '                ") AS T ON T.employeeunkid = hremployee_master.employeeunkid AND T.Rno = 1 "



            'Pinkal (02-Apr-2024) -- Start
            'PAYE Changes For Statutory Reports.
            If xUACQry.Trim.Length > 0 Then
                StrQ &= xUACQry
            End If

            If xAdvanceJoinQry.Trim.Length > 0 Then
                StrQ &= xAdvanceJoinQry
            End If

            If xDateJoinQry.Trim.Length > 0 Then
                StrQ &= xDateJoinQry
            End If

            'Pinkal (02-Apr-2024) -- End

            'StrQ &= mstrAnalysis_Join

            StrQ &= "WHERE 1 = 1 "

            If mstrAdvanceFilter.Trim <> "" Then
                StrQ &= " AND " & mstrAdvanceFilter & " "
            End If

            If mintEmployeeId > 0 Then
                StrQ &= "	AND hremployee_master.employeeunkid = @employeeunkid "
            End If

            If xUACFiltrQry.Trim.Length > 0 Then
                StrQ &= " AND " & xUACFiltrQry
            End If

            If mblnIncludeInactiveEmp = False Then
                If xDateFilterQry.Trim.Length > 0 Then
                    StrQ &= xDateFilterQry
                End If
            End If

            'S.SANDEEP |28-JUL-2021| -- START
            'StrQ &= "SELECT " & _
            '                       "#tblEmp.employeecode " & _
            '                       ",ISNULL(hremployee_meminfo_tran.membershipno, '') AS SocialSecurityNo "

            StrQ &= "SELECT DISTINCT " & _
                       "#tblEmp.employeecode " & _
                       ",ISNULL(hremployee_meminfo_tran.membershipno, '') AS SocialSecurityNo "
            'S.SANDEEP |28-JUL-2021| -- END

            If (mintIdentityId > 0) Then
                StrQ &= ",ISNULL(hremployee_idinfo_tran.identity_no, '') AS Id_Num "
            Else
                StrQ &= ",'' AS Id_Num "
            End If

            StrQ &= ",#tblEmp.employeeunkid as EmpId" & _
                       ",A.BasePay " & _
                       ",a.Deduction " & _
                       ",a.GrossAmount " & _
                       ",ISNULL(A.GrossAmount, 0) - ISNULL(a.BasePay, 0) AS OtherAllowances " & _
                       ",a.TaxableAmount " & _
                       ",a.TaxPayable " & _
                       ",#tblEmp.employmenttypeunkid " & _
                       ",PayType.name as ResidentalStatus " & _
                       ",#tblEmp.EName "

            'Pinkal (02-Apr-2024) -- Start
            'PAYE Changes For Statutory Reports.
            StrQ &= " ,ISNULL(hrstation_master.address1,'') AS Location "
            'Pinkal (02-Apr-2024) -- End

            '",Alloc.stationunkid " & _
            '",Alloc.deptgroupunkid " & _
            '",Alloc.departmentunkid " & _
            '",Alloc.sectiongroupunkid " & _
            '",Alloc.sectionunkid " & _
            '",Alloc.unitgroupunkid " & _
            '",Alloc.unitunkid " & _
            '",Alloc.teamunkid " & _
            '",jobs.jobgroupunkid " & _
            '",jobs.jobunkid " & _

            StrQ &= "FROM    ( SELECT " & _
                                            "PAYE.empid AS EmpId " & _
                                                     ",ISNULL(SUM(PAYE.BasePay), 0) AS BasePay " & _
                                                     ",ISNULL(SUM(PAYE.Deduction ), 0) AS Deduction " & _
                                                     ",ISNULL(SUM(PAYE.GrossAmount ), 0) AS GrossAmount " & _
                                                     ",ISNULL(SUM(PAYE.TaxableAmount ), 0) AS TaxableAmount " & _
                                                     ",ISNULL(SUM(PAYE.TaxPayable ), 0) AS TaxPayable " & _
                                                     "FROM      ( SELECT  #tblEmp.employeeunkid AS EmpId " & _
                                                     ",0 AS BasePay " & _
                                                     ",0 AS Deduction " & _
                                                     ",0 AS GrossAmount " & _
                                                     ",0 AS TaxableAmount " & _
                                                     ",0 AS TaxPayable " & _
                                                     "FROM prpayrollprocess_tran " & _
                                                   "LEFT JOIN prtnaleave_tran ON prpayrollprocess_tran.tnaleavetranunkid = prtnaleave_tran.tnaleavetranunkid " & _
                                                   " JOIN #tblEmp ON prpayrollprocess_tran.employeeunkid = #tblEmp.employeeunkid " & _
                                                   "JOIN hremployee_meminfo_tran ON prpayrollprocess_tran.membershiptranunkid = hremployee_meminfo_tran.membershiptranunkid " & _
                                                           "AND hremployee_meminfo_tran.employeeunkid = #tblEmp.employeeunkid " & _
                                                           "AND ISNULL(hremployee_meminfo_tran.isdeleted, 0) = 0 " & _
                                                   "LEFT JOIN hrmembership_master ON hremployee_meminfo_tran.membershipunkid = hrmembership_master.membershipunkid " & _
                                                           "AND prpayrollprocess_tran.tranheadunkid = hrmembership_master.emptranheadunkid " & _
                                                   "LEFT JOIN cfcommon_period_tran ON prtnaleave_tran.payperiodunkid = cfcommon_period_tran.periodunkid " & _
                                                           "AND ISNULL(cfcommon_period_tran.isactive,0) = 1 "


            StrQ &= "                   WHERE   prpayrollprocess_tran.isvoid = 0 " & _
                                                "AND prtnaleave_tran.isvoid = 0 " & _
                                                "AND prtnaleave_tran.payperiodunkid IN (" & mintPeriodId & ") " & _
                                                "AND hrmembership_master.membershipunkid = @MemId "


            StrQ &= "                   UNION ALL " & _
                                        "SELECT  #tblEmp.employeeunkid AS EmpId " & _
                                           ",0 AS BasePay " & _
                                           ",0 AS Deduction " & _
                                           ",0 AS GrossAmount " & _
                                           ",0 AS TaxableAmount " & _
                                           ",0 AS TaxPayable " & _
                                        "FROM   prpayrollprocess_tran " & _
                                                "LEFT JOIN prtnaleave_tran ON prpayrollprocess_tran.tnaleavetranunkid = prtnaleave_tran.tnaleavetranunkid " & _
                                                "JOIN #tblEmp ON prpayrollprocess_tran.employeeunkid = #tblEmp.employeeunkid " & _
                                                "JOIN hremployee_meminfo_tran ON prpayrollprocess_tran.membershiptranunkid = hremployee_meminfo_tran.membershiptranunkid " & _
                                                        "AND hremployee_meminfo_tran.employeeunkid = #tblEmp.employeeunkid " & _
                                                        "AND ISNULL(hremployee_meminfo_tran.isdeleted, 0) = 0 " & _
                                                "LEFT JOIN hrmembership_master ON hremployee_meminfo_tran.membershipunkid = hrmembership_master.membershipunkid " & _
                                                        "AND prpayrollprocess_tran.tranheadunkid = hrmembership_master.cotranheadunkid " & _
                                                "LEFT JOIN cfcommon_period_tran ON prtnaleave_tran.payperiodunkid = cfcommon_period_tran.periodunkid " & _
                                                        "AND ISNULL(cfcommon_period_tran.isactive,0) = 1 "




            StrQ &= "                   WHERE   prpayrollprocess_tran.isvoid = 0 " & _
                                                "AND prtnaleave_tran.isvoid = 0 " & _
                                                "AND prtnaleave_tran.payperiodunkid IN (" & mintPeriodId & ") " & _
                                                "AND hrmembership_master.membershipunkid = @MemId "

           

            'for Basic Pay


            StrQ &= "                   UNION ALL " & _
                                       "SELECT  #tblEmp.employeeunkid AS EmpId " & _
                                                     ", ISNULL(CAST(prpayrollprocess_tran.amount AS DECIMAL(36, " & decDecimalPlaces & ")), 0) AS BasePay " & _
                                                     ",0 AS Deduction " & _
                                                     ",0 AS GrossAmount " & _
                                                     ",0 AS TaxableAmount " & _
                                                     ",0 AS TaxPayable " & _
                                       "FROM   prpayrollprocess_tran " & _
                                               "LEFT JOIN prtnaleave_tran ON prpayrollprocess_tran.tnaleavetranunkid = prtnaleave_tran.tnaleavetranunkid " & _
                                               " JOIN #tblEmp ON prpayrollprocess_tran.employeeunkid = #tblEmp.employeeunkid " & _
                                               "LEFT JOIN cfcommon_period_tran ON prtnaleave_tran.payperiodunkid = cfcommon_period_tran.periodunkid " & _
                                                       "AND ISNULL(cfcommon_period_tran.isactive,0) = 1 "



            StrQ &= "                   WHERE   prpayrollprocess_tran.isvoid = 0 " & _
                                                "AND prtnaleave_tran.isvoid = 0 " & _
                                                "AND prtnaleave_tran.payperiodunkid IN (" & mintPeriodId & ") " & _
                                                "AND prpayrollprocess_tran.tranheadunkid = @BasePay "
            'for Deduction Pay



            StrQ &= "      UNION ALL " & _
                           "SELECT  #tblEmp.employeeunkid AS EmpId " & _
                                    ", 0 AS BasePay " & _
                                    ", ISNULL(CAST(prpayrollprocess_tran.amount AS DECIMAL(36, " & decDecimalPlaces & ")), 0) AS Deduction " & _
                                    ",0 AS GrossAmount " & _
                                    ",0 AS TaxableAmount " & _
                                    ",0 AS TaxPayable " & _
                           "FROM   prpayrollprocess_tran " & _
                                   "LEFT JOIN prtnaleave_tran ON prpayrollprocess_tran.tnaleavetranunkid = prtnaleave_tran.tnaleavetranunkid " & _
                                   " JOIN #tblEmp ON prpayrollprocess_tran.employeeunkid = #tblEmp.employeeunkid " & _
                                   "LEFT JOIN cfcommon_period_tran ON prtnaleave_tran.payperiodunkid = cfcommon_period_tran.periodunkid " & _
                                           "AND ISNULL(cfcommon_period_tran.isactive,0) = 1 "




            StrQ &= "                   WHERE   prpayrollprocess_tran.isvoid = 0 " & _
                                                "AND prtnaleave_tran.isvoid = 0 " & _
                                                "AND prtnaleave_tran.payperiodunkid IN (" & mintPeriodId & ") " & _
                                                "AND prpayrollprocess_tran.tranheadunkid IN (" & mstrDeductionHeadIds & ") "

            'for Gross Amount

            StrQ &= "                   UNION ALL " & _
                                       "SELECT  #tblEmp.employeeunkid AS EmpId " & _
                                                     ",0 AS BasePay " & _
                                                     ",0 AS Deduction " & _
                                                     ", ISNULL(CAST(prpayrollprocess_tran.amount AS DECIMAL(36, " & decDecimalPlaces & ")), 0) AS GrossAmount " & _
                                                     ",0 AS TaxableAmount " & _
                                                     ",0 AS TaxPayable " & _
                                       "FROM   prpayrollprocess_tran " & _
                                               "LEFT JOIN prtnaleave_tran ON prpayrollprocess_tran.tnaleavetranunkid = prtnaleave_tran.tnaleavetranunkid " & _
                                               "JOIN #tblEmp ON prpayrollprocess_tran.employeeunkid = #tblEmp.employeeunkid " & _
                                               "LEFT JOIN cfcommon_period_tran ON prtnaleave_tran.payperiodunkid = cfcommon_period_tran.periodunkid " & _
                                                       "AND ISNULL(cfcommon_period_tran.isactive,0) = 1 "



            StrQ &= "                   WHERE   prpayrollprocess_tran.isvoid = 0 " & _
                                                "AND prtnaleave_tran.isvoid = 0 " & _
                                                "AND prtnaleave_tran.payperiodunkid IN (" & mintPeriodId & ") " & _
                                                "AND prpayrollprocess_tran.add_deduct = 1"


            'For taxable  Payment

            StrQ &= "                   UNION ALL " & _
                           "SELECT  #tblEmp.employeeunkid AS EmpId " & _
                                    ", 0 AS BasePay " & _
                                    ", 0 AS Deduction " & _
                                    ", 0 AS GrossAmount " & _
                                    ", ISNULL(CAST(prpayrollprocess_tran.amount AS DECIMAL(36, " & decDecimalPlaces & ")), 0) AS TaxableAmount " & _
                                    ",0 AS TaxPayable " & _
                           "FROM   prpayrollprocess_tran " & _
                                   "LEFT JOIN prtnaleave_tran ON prpayrollprocess_tran.tnaleavetranunkid = prtnaleave_tran.tnaleavetranunkid " & _
                                   "JOIN #tblEmp ON prpayrollprocess_tran.employeeunkid = #tblEmp.employeeunkid " & _
                                   "LEFT JOIN cfcommon_period_tran ON prtnaleave_tran.payperiodunkid = cfcommon_period_tran.periodunkid " & _
                                           "AND ISNULL(cfcommon_period_tran.isactive,0) = 1 "





            StrQ &= "                   WHERE   prpayrollprocess_tran.isvoid = 0 " & _
                                                "AND prtnaleave_tran.isvoid = 0 " & _
                                                "AND prtnaleave_tran.payperiodunkid IN (" & mintPeriodId & ") " & _
                                                "AND prpayrollprocess_tran.tranheadunkid = @TaxableAmount "
           

            'For Taxable Amount

            StrQ &= "               UNION ALL " & _
                                    "SELECT  #tblEmp.employeeunkid AS EmpId " & _
                                    ", 0 AS BasePay " & _
                                    ", 0 AS Deduction " & _
                                    ", 0 AS GrossAmount " & _
                                    ", 0 AS TaxableAmount " & _
                                    ", ISNULL(CAST(prpayrollprocess_tran.amount AS DECIMAL(36, " & decDecimalPlaces & ")), 0) AS TaxPayable " & _
                           "FROM   prpayrollprocess_tran " & _
                                   "LEFT JOIN prtnaleave_tran ON prpayrollprocess_tran.tnaleavetranunkid = prtnaleave_tran.tnaleavetranunkid " & _
                                   "JOIN #tblEmp ON prpayrollprocess_tran.employeeunkid = #tblEmp.employeeunkid " & _
                                   "LEFT JOIN cfcommon_period_tran ON prtnaleave_tran.payperiodunkid = cfcommon_period_tran.periodunkid " & _
                                           "AND ISNULL(cfcommon_period_tran.isactive,0) = 1 "



           

            StrQ &= "                   WHERE   prpayrollprocess_tran.isvoid = 0 " & _
                                                "AND prtnaleave_tran.isvoid = 0 " & _
                                                "AND prtnaleave_tran.payperiodunkid IN (" & mintPeriodId & ") " & _
                                                "AND prpayrollprocess_tran.tranheadunkid = @TaxPayable "

            StrQ &= "	        ) AS PAYE " & _
                                "WHERE 1 = 1 " & _
                                "GROUP BY  empid "

            StrQ &= ") AS A " & _
                       "JOIN #tblEmp ON #tblEmp.employeeunkid = A.empid "
            '"    LEFT JOIN " & _
            '"        ( " & _
            '"            SELECT " & _
            '"                stationunkid " & _
            '"                ,deptgroupunkid " & _
            '"                ,departmentunkid " & _
            '"                ,sectiongroupunkid " & _
            '"                ,sectionunkid " & _
            '"                ,unitgroupunkid " & _
            '"                ,unitunkid " & _
            '"                ,teamunkid " & _
            '"          	    ,classgroupunkid " & _
            '"          	    ,classunkid " & _
            '"                ,employeeunkid " & _
            '"                ,ROW_NUMBER()OVER(PARTITION BY employeeunkid ORDER BY effectivedate DESC) AS rno " & _
            '"            FROM hremployee_transfer_tran " & _
            '"            WHERE isvoid = 0 AND CONVERT(CHAR(8),effectivedate,112) <= '" & eZeeDate.convertDate(dtPeriodEnd) & "' " & _
            '"        ) AS Alloc ON Alloc.employeeunkid = #tblEmp.employeeunkid AND Alloc.rno = 1 " & _
            '"    LEFT JOIN hrstation_master AS SM ON SM.stationunkid = Alloc.stationunkid " & _
            '"    LEFT JOIN hrdepartment_group_master AS DGM ON DGM.deptgroupunkid = Alloc.deptgroupunkid " & _
            '"    LEFT JOIN hrdepartment_master AS DM ON Alloc.departmentunkid = DM.departmentunkid " & _
            '"    LEFT JOIN hrsectiongroup_master  AS SECGM ON SECGM.sectiongroupunkid  = Alloc.sectiongroupunkid " & _
            '"    LEFT JOIN hrsection_master AS SECM ON SECM.sectionunkid  = Alloc.sectionunkid " & _
            '"    LEFT JOIN hrunitgroup_master AS UGM ON UGM.unitgroupunkid  = Alloc.unitgroupunkid " & _
            '"    LEFT JOIN hrunit_master AS UM ON UM.unitunkid = Alloc.unitunkid " & _
            '"    LEFT JOIN hrteam_master AS TM ON TM.teamunkid = Alloc.teamunkid " & _
            '"    LEFT JOIN hrclassgroup_master AS CGM ON CGM.classgroupunkid = Alloc.classgroupunkid " & _
            '"    LEFT JOIN hrclasses_master AS CM ON CM.classesunkid = Alloc.classunkid " & _
            '"    LEFT JOIN " & _
            '"        ( " & _
            '"            SELECT " & _
            '"                jobunkid " & _
            '"             	,jobgroupunkid " & _
            '"                ,employeeunkid " & _
            '"                ,ROW_NUMBER()OVER(PARTITION BY employeeunkid ORDER BY effectivedate DESC) AS rno " & _
            '"            FROM hremployee_categorization_tran " & _
            '"            WHERE isvoid = 0 AND CONVERT(CHAR(8),effectivedate,112) <= '" & eZeeDate.convertDate(dtPeriodEnd) & "' " & _
            '"        ) AS Jobs ON Jobs.employeeunkid = #tblEmp.employeeunkid AND Jobs.rno = 1 " & _
            '"    JOIN hrjob_master AS JM ON Jobs.jobunkid = JM.jobunkid " & _
            '"    JOIN hrjobgroup_master AS JGM ON Jobs.jobgroupunkid = JGM.jobgroupunkid " & _


            'Pinkal (02-Apr-2024) -- Start
            'PAYE Changes For Statutory Reports.
            StrQ &= " LEFT JOIN hrstation_master ON hrstation_master.stationunkid = #tblEmp.stationunkid "
            'Pinkal (02-Apr-2024) -- End


            StrQ &= "JOIN hremployee_meminfo_tran ON #tblEmp.employeeunkid = hremployee_meminfo_tran.employeeunkid " & _
                       "    AND ISNULL(hremployee_meminfo_tran.isdeleted,0) = 0 "

            If (mintIdentityId > 0) Then
                StrQ &= "LEFT JOIN hremployee_idinfo_tran ON #tblEmp.employeeunkid = hremployee_idinfo_tran.employeeunkid " & _
                        " AND hremployee_idinfo_tran.idtypeunkid = @idtypeunkid "
                objDataOperation.AddParameter("@idtypeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintIdentityId)
                'Else
                '    StrQ &= " AND ISNULL(hremployee_idinfo_tran.isdefault, 1) = 1 "
            End If

            StrQ &= "LEFT JOIN hrmembership_master ON hremployee_meminfo_tran.membershipunkid = hrmembership_master.membershipunkid " & _
                       "LEFT JOIN cfcommon_master AS PayType ON PayType.masterunkid = #tblEmp.paytypeunkid " & _
                       "    AND PayType.mastertype = '" & clsCommon_Master.enCommonMaster.PAY_TYPE & "' " & _
                       "WHERE hrmembership_master.membershipunkid = @MemId "



            objDataOperation.AddParameter("@MemId", SqlDbType.Int, eZeeDataType.INT_SIZE, mintMembershipId)
            objDataOperation.AddParameter("@BasePay", SqlDbType.Int, eZeeDataType.INT_SIZE, mintBasicPayHeadId)
            'objDataOperation.AddParameter("@Deduction", SqlDbType.NVarChar, eZeeDataType.DESC_SIZE, mstrDeductionHeadIds)
            objDataOperation.AddParameter("@TaxableAmount", SqlDbType.Int, eZeeDataType.INT_SIZE, mintTaxableAmountHeadId)
            objDataOperation.AddParameter("@TaxPayable", SqlDbType.Int, eZeeDataType.INT_SIZE, mintTaxPayableHeadId)

            If mintEmployeeId > 0 Then
                objDataOperation.AddParameter("@employeeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintEmployeeId)
            End If


           
            StrQ &= " ORDER BY employeecode "
            StrQ &= " Drop Table #tblEmp "





            dsList = objDataOperation.ExecQuery(StrQ, "DataTable")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & " " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            rpt_Data = New ArutiReport.Designer.dsArutiReport


            Dim iCnt As Integer = 0
            Dim StrTitle As String = ""
            Dim StrAddress As String = ""

            Dim decBasePayTotal As Decimal = 0
            Dim decDeductionTotal As Decimal = 0
            Dim decOtherTotal As Decimal = 0
            Dim decTaxableTotal As Decimal = 0
            Dim decTaxPayTotal As Decimal = 0
            Dim decGrossAmountTotal As Decimal = 0


            For Each dtRow As DataRow In dsList.Tables("DataTable").Rows

                Dim rpt_Row As DataRow
                rpt_Row = rpt_Data.Tables("ArutiTable").NewRow
                iCnt += 1
                rpt_Row("Column2") = iCnt.ToString
                'S.SANDEEP |28-JUL-2021| -- START
                rpt_Row("Column87") = iCnt
                'S.SANDEEP |28-JUL-2021| -- END
                rpt_Row("Column3") = dtRow.Item("Id_Num")
                'S.SANDEEP |28-JUL-2021| -- START
                Try
                    rpt_Row("Column88") = CInt(dtRow.Item("Id_Num"))
                Catch ex As Exception
                    rpt_Row("Column88") = 0
                End Try
                'S.SANDEEP |28-JUL-2021| -- END
                Dim intEmployeeMentId As Integer = 0
                Select Case mintAllocationId
                    Case enAllocation.BRANCH
                        intEmployeeMentId = dtRow.Item("stationunkid")

                    Case enAllocation.DEPARTMENT_GROUP
                        intEmployeeMentId = dtRow.Item("deptgroupunkid")

                    Case enAllocation.DEPARTMENT
                        intEmployeeMentId = dtRow.Item("departmentunkid")


                    Case enAllocation.SECTION_GROUP
                        intEmployeeMentId = dtRow.Item("sectiongroupunkid")

                    Case enAllocation.SECTION
                        intEmployeeMentId = dtRow.Item("sectionunkid")

                    Case enAllocation.UNIT_GROUP
                        intEmployeeMentId = dtRow.Item("unitgroupunkid")

                    Case enAllocation.UNIT
                        intEmployeeMentId = dtRow.Item("unitunkid")

                    Case enAllocation.TEAM
                        intEmployeeMentId = dtRow.Item("teamunkid")

                    Case enAllocation.JOB_GROUP
                        intEmployeeMentId = dtRow.Item("jobgroupunkid")

                    Case enAllocation.JOBS
                        intEmployeeMentId = dtRow.Item("jobunkid")

                    Case enAllocation.CLASS_GROUP
                        intEmployeeMentId = dtRow.Item("classgroupunkid")

                    Case enAllocation.CLASSES

                        intEmployeeMentId = dtRow.Item("classunkid")
                    Case Else
                        intEmployeeMentId = dtRow.Item("employmenttypeunkid")
                End Select


                Select Case intEmployeeMentId
                    Case mintPrimaryId
                        rpt_Row("Column4") = Language.getMessage(mstrModuleName, 20, "Primary")

                    Case mintSecondaryId
                        rpt_Row("Column4") = Language.getMessage(mstrModuleName, 21, "Secondary")

                    Case mintDirectorsId
                        rpt_Row("Column4") = Language.getMessage(mstrModuleName, 22, "Directors Fee")

                    Case Else
                        rpt_Row("Column4") = Language.getMessage(mstrModuleName, 20, "Primary")
                End Select

                rpt_Row("Column5") = dtRow.Item("ResidentalStatus")
                rpt_Row("Column6") = dtRow.Item("EName")
                rpt_Row("Column7") = dtRow.Item("SocialSecurityNo")
                rpt_Row("Column8") = Format(CDec(dtRow.Item("BasePay")), GUI.fmtCurrency)
                'S.SANDEEP |28-JUL-2021| -- START
                rpt_Row("Column81") = CDec(dtRow.Item("BasePay"))
                'S.SANDEEP |28-JUL-2021| -- END
                rpt_Row("Column9") = Format(CDec(dtRow.Item("OtherAllowances")), GUI.fmtCurrency)
                'S.SANDEEP |28-JUL-2021| -- START
                rpt_Row("Column82") = CDec(dtRow.Item("OtherAllowances"))
                'S.SANDEEP |28-JUL-2021| -- END
                rpt_Row("Column10") = Format(CDec(dtRow.Item("GrossAmount")), GUI.fmtCurrency)
                'S.SANDEEP |28-JUL-2021| -- START
                rpt_Row("Column83") = CDec(dtRow.Item("GrossAmount"))
                'S.SANDEEP |28-JUL-2021| -- END
                rpt_Row("Column11") = Format(CDec(dtRow.Item("Deduction")), GUI.fmtCurrency)
                'S.SANDEEP |28-JUL-2021| -- START
                rpt_Row("Column84") = CDec(dtRow.Item("Deduction"))
                'S.SANDEEP |28-JUL-2021| -- END
                rpt_Row("Column12") = Format(CDec(dtRow.Item("TaxableAmount")), GUI.fmtCurrency)
                'S.SANDEEP |28-JUL-2021| -- START
                rpt_Row("Column85") = CDec(dtRow.Item("TaxableAmount"))
                'S.SANDEEP |28-JUL-2021| -- END
                rpt_Row("Column13") = Format(CDec(dtRow.Item("TaxPayable")), GUI.fmtCurrency)
                'S.SANDEEP |28-JUL-2021| -- START
                rpt_Row("Column86") = CDec(dtRow.Item("TaxPayable"))
                'S.SANDEEP |28-JUL-2021| -- END
                rpt_Row("Column14") = dtRow.Item("employeecode")

                'Pinkal (02-Apr-2024) -- Start
                'PAYE Changes For Statutory Reports.
                rpt_Row("Column15") = dtRow.Item("Location").ToString()
                'Pinkal (02-Apr-2024) -- End


                decBasePayTotal += Format(CDec(dtRow.Item("BasePay")), GUI.fmtCurrency)
                decDeductionTotal += Format(CDec(dtRow.Item("Deduction")), GUI.fmtCurrency)
                decOtherTotal += Format(CDec(dtRow.Item("OtherAllowances")), GUI.fmtCurrency)
                decTaxableTotal += Format(CDec(dtRow.Item("TaxableAmount")), GUI.fmtCurrency)
                decTaxPayTotal += Format(CDec(dtRow.Item("TaxPayable")), GUI.fmtCurrency)
                decGrossAmountTotal += Format(CDec(dtRow.Item("GrossAmount")), GUI.fmtCurrency)
                rpt_Data.Tables("ArutiTable").Rows.Add(rpt_Row)
            Next

            Dim objCMaster As New clsMasterData
            Dim dsCountry As New DataSet
            dsCountry = objCMaster.getCountryList("List", False, Company._Object._Countryunkid)
            If dsCountry.Tables(0).Rows.Count > 0 Then
                StrAddress &= dsCountry.Tables(0).Rows(0)("country_name") & " " & Company._Object._Address2
            End If
            dsCountry.Dispose()

            objRpt = New ArutiReport.Designer.rptStatementAndPaymentTax
            objRpt.SetDataSource(rpt_Data)

            'S.SANDEEP |28-JUL-2021| -- START
            If ExportAction = enExportAction.ExcelXLSM Then
                mdtTableExcel = rpt_Data.Tables("ArutiTable")
            End If
            'S.SANDEEP |28-JUL-2021| -- END

            Call ReportFunction.TextChange(objRpt, "lblReportname", _ReportName)

            Call ReportFunction.TextChange(objRpt, "lblMonth", Language.getMessage(mstrModuleName, 1, "Month"))
            Call ReportFunction.TextChange(objRpt, "txtmonth", mstrPeriodNames)

            Call ReportFunction.TextChange(objRpt, "lblYear", Language.getMessage(mstrModuleName, 2, "Year"))
            Call ReportFunction.TextChange(objRpt, "txtYear", FinancialYear._Object._FinancialYear_Name)

            Call ReportFunction.TextChange(objRpt, "lblLocation", Language.getMessage(mstrModuleName, 3, "Select Location"))
            Call ReportFunction.TextChange(objRpt, "txtLocation", StrAddress)

            Call ReportFunction.TextChange(objRpt, "lblHeader", Language.getMessage(mstrModuleName, 4, "Statement Of Tax Withheld  for Employees Form For Tanzania Zanzibar"))
            Call ReportFunction.TextChange(objRpt, "lblNote", Language.getMessage(mstrModuleName, 5, "Note: This Return is submitted under the provision of Section 84 of the income Tax Act Cap 332. You are hereby required to submit the return and make payment within 7 days after end  of the month to which it refers."))
            Call ReportFunction.TextChange(objRpt, "lblformname", Language.getMessage(mstrModuleName, 6, "FORM ITX 219.03.E"))

            Call ReportFunction.TextChange(objRpt, "lblSrno", Language.getMessage(mstrModuleName, 7, "S.N"))
            Call ReportFunction.TextChange(objRpt, "lblEmployeeTin", Language.getMessage(mstrModuleName, 8, "Employee's Tin"))
            Call ReportFunction.TextChange(objRpt, "lbltypeofemp", Language.getMessage(mstrModuleName, 9, "Type Of Employeement"))
            Call ReportFunction.TextChange(objRpt, "lblresidentalstatus", Language.getMessage(mstrModuleName, 10, "Residental Status"))
            Call ReportFunction.TextChange(objRpt, "lblsocialsecurityno", Language.getMessage(mstrModuleName, 12, "Social Security Number"))
            Call ReportFunction.TextChange(objRpt, "lblbasicpay", Language.getMessage(mstrModuleName, 13, "Basic Pay"))
            Call ReportFunction.TextChange(objRpt, "lblotherallowances", Language.getMessage(mstrModuleName, 14, "Other Allowances and Benefits"))
            Call ReportFunction.TextChange(objRpt, "lblgrossamt", Language.getMessage(mstrModuleName, 15, "Gross Amount"))
            Call ReportFunction.TextChange(objRpt, "lbldeduction", Language.getMessage(mstrModuleName, 16, "Deductions"))
            Call ReportFunction.TextChange(objRpt, "lbltaxableamt", Language.getMessage(mstrModuleName, 17, "Taxable Amount"))
            Call ReportFunction.TextChange(objRpt, "lblempname", Language.getMessage(mstrModuleName, 11, "Name Of Employee"))
            Call ReportFunction.TextChange(objRpt, "lbltaxpayable", Language.getMessage(mstrModuleName, 18, "Tax Payable"))
            Call ReportFunction.TextChange(objRpt, "lblTotal", Language.getMessage(mstrModuleName, 19, "Total"))
            Call ReportFunction.TextChange(objRpt, "lblEmpCode", Language.getMessage(mstrModuleName, 23, "Employee Code"))

            Call ReportFunction.TextChange(objRpt, "txtBasicPay", Format(decBasePayTotal, GUI.fmtCurrency))
            Call ReportFunction.TextChange(objRpt, "txtDeductionTotal", Format(decDeductionTotal, GUI.fmtCurrency))
            Call ReportFunction.TextChange(objRpt, "txtOtherAllowanceTotal", Format(decOtherTotal, GUI.fmtCurrency))
            Call ReportFunction.TextChange(objRpt, "txtTaxableAmountTotal", Format(decTaxableTotal, GUI.fmtCurrency))
            Call ReportFunction.TextChange(objRpt, "txtTaxPayableTotal", Format(decTaxPayTotal, GUI.fmtCurrency))
            Call ReportFunction.TextChange(objRpt, "txtGrossTotal", Format(decGrossAmountTotal, GUI.fmtCurrency))


            'S.SANDEEP |28-JUL-2021| -- START
            If mblnShowEmployeeCode = False Then
                Call ReportFunction.EnableSuppress(objRpt, "lblEmpCode", True)
                Call ReportFunction.EnableSuppress(objRpt, "Column141", True)
                Call ReportFunction.EnableSuppress(objRpt, "Line12", True)
                Call ReportFunction.EnableSuppress(objRpt, "Line14", True)
                Call ReportFunction.SetColumnAt(objRpt, "lblempname", objRpt.ReportDefinition.ReportObjects("lblEmpCode").Left, objRpt.ReportDefinition.ReportObjects("lblEmpCode").Width + objRpt.ReportDefinition.ReportObjects("lblempname").Width)
                Call ReportFunction.SetColumnAt(objRpt, "Column61", objRpt.ReportDefinition.ReportObjects("Column141").Left, objRpt.ReportDefinition.ReportObjects("Column141").Width + objRpt.ReportDefinition.ReportObjects("Column61").Width)
            End If
            'S.SANDEEP |28-JUL-2021| -- END

            Return objRpt

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: Generate_DetailReport; Module Name: " & mstrModuleName)
            Return Nothing
        End Try
    End Function

#End Region
	'<Language> This Auto Generated Text Please Do Not Modify it.
#Region " Language & UI Settings "
	Public Shared Sub SetMessages()
		Try
			Language.setMessage(mstrModuleName, 1, "Month")
			Language.setMessage(mstrModuleName, 2, "Year")
			Language.setMessage(mstrModuleName, 3, "Select Location")
			Language.setMessage(mstrModuleName, 4, "Statement Of Tax Withheld  for Employees Form For Tanzania Zanzibar")
			Language.setMessage(mstrModuleName, 5, "Note: This Return is submitted under the provision of Section 84 of the income Tax Act Cap 332. You are hereby required to submit the return and make payment within 7 days after end  of the month to which it refers.")
			Language.setMessage(mstrModuleName, 6, "FORM ITX 219.03.E")
			Language.setMessage(mstrModuleName, 7, "S.N")
			Language.setMessage(mstrModuleName, 8, "Employee's Tin")
			Language.setMessage(mstrModuleName, 9, "Type Of Employeement")
			Language.setMessage(mstrModuleName, 10, "Residental Status")
			Language.setMessage(mstrModuleName, 11, "Name Of Employee")
			Language.setMessage(mstrModuleName, 12, "Social Security Number")
			Language.setMessage(mstrModuleName, 13, "Basic Pay")
			Language.setMessage(mstrModuleName, 14, "Other Allowances and Benefits")
			Language.setMessage(mstrModuleName, 15, "Gross Amount")
			Language.setMessage(mstrModuleName, 16, "Deductions")
			Language.setMessage(mstrModuleName, 17, "Taxable Amount")
			Language.setMessage(mstrModuleName, 18, "Tax Payable")
			Language.setMessage(mstrModuleName, 19, "Total")
			Language.setMessage(mstrModuleName, 20, "Primary")
			Language.setMessage(mstrModuleName, 21, "Secondary")
			Language.setMessage(mstrModuleName, 22, "Directors Fee")
			Language.setMessage(mstrModuleName, 23, "Employee Code")

		Catch Ex As Exception
			DisplayError.Show("-1", ex.Message, "SetMessages", mstrModuleName)
		End Try
	End Sub
#End Region 'Language & UI Settings
	'</Language>
End Class
