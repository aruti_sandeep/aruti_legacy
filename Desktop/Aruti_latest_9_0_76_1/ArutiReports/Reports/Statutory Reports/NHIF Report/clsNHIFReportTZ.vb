'************************************************************************************************************************************
'Class Name   : clsNHIFReportTZ.vb
'Purpose      :
'Created Date : 01 Jan 2018
'Written By   : Rana Varsha.
'Modified     :
'************************************************************************************************************************************
#Region " Imports "

Imports eZeeCommonLib
Imports Aruti.Data
Imports ExcelWriter

#End Region
Public Class clsNHIFReportTZ
    Inherits IReportData
    Private Shared ReadOnly mstrModuleName As String = "clsNHIFReportTZ"
    Private mstrReportId As String = enArutiReport.NHIF_Report_TZ
    Dim objDataOperation As clsDataOperation

#Region " Constructor "

    Public Sub New(Byval intLangId As Integer,Byval intCompanyId AS Integer) 'S.SANDEEP |24-Sep-2020| -- START --END
        Me.setReportData(CInt(mstrReportId),intLangId,intCompanyId) 'S.SANDEEP |24-Sep-2020| -- START --END
        Call Create_OnDetailReport()
    End Sub

#End Region

#Region " Private variables "

    Private mintMembershipId As Integer = 1
    Private mstrMembershipName As String = String.Empty
    Private mintNonPayrollMembershipId As Integer = -1
    Private mstrNonPayrollMembershipName As String = String.Empty
    Private mintPeriodId As Integer = -1
    Private mstrPeriodName As String = String.Empty
    Private mbIncludeInactiveEmp As Boolean = True
    Private mintReportId As Integer = 0
    Private mstrReportTypeName As String = ""
    Private mstrAnalysis_Fields As String = ""
    Private mstrAnalysis_Join As String = ""
    Private mstrAnalysis_OrderBy As String = ""
    Private mintViewIndex As Integer = -1
    Private mstrViewByIds As String = String.Empty
    Private mstrViewByName As String = String.Empty
    Private mstrReport_GroupName As String = ""
    Private mstrExchangeRate As String = ""
    Private mintCountryId As Integer = 0
    Private mintBaseCurrId As Integer = 0
    Private mintPaidCurrencyId As Integer = 0
    Private mdecConversionRate As Decimal = 0
    Private mstrAdvance_Filter As String = ""
    Private menExportAction As enExportAction
    Private mdtTableExcel As DataTable
    Private mstrAddress As String

    Private mintOtherEarningTranId As Integer = 0
    Private mblnShowBasicSalary As Boolean = False

    Private mblnShowRemark As Boolean = False
    Private mblnIgnoreZero As Boolean = False
    Private mstrPeriodCode As String = String.Empty
    Private mblnShowReportNameOnReport As Boolean = False
    Private mblnFirstNamethenSurname As Boolean = ConfigParameter._Object._FirstNamethenSurname
    'Sohail (10 Jul 2019) -- Start
    'Good Neighbours Enhancement - Support Issue Id # 3875 - 76.1 - Put link for NHIF EFT and it should generate report without report header of report name address and company name and add "EmployerNo" column..
    Private mstrCurrencyName As String = ""
    'Sohail (10 Jul 2019) -- End

#End Region

#Region " Properties "

    Public WriteOnly Property _MembershipId() As Integer
        Set(ByVal value As Integer)
            mintMembershipId = value
        End Set
    End Property

    Public WriteOnly Property _MembershipName() As String
        Set(ByVal value As String)
            mstrMembershipName = value
        End Set
    End Property

    Public WriteOnly Property _NonPayrollMembershipId() As Integer
        Set(ByVal value As Integer)
            mintNonPayrollMembershipId = value
        End Set
    End Property

    Public WriteOnly Property _NonPayrollMembershipName() As String
        Set(ByVal value As String)
            mstrNonPayrollMembershipName = value
        End Set
    End Property

    Public WriteOnly Property _PeriodId() As Integer
        Set(ByVal value As Integer)
            mintPeriodId = value
        End Set
    End Property

    Public WriteOnly Property _PeriodName() As String
        Set(ByVal value As String)
            mstrPeriodName = value
        End Set
    End Property

    Public WriteOnly Property _IncludeInactiveEmp() As Boolean
        Set(ByVal value As Boolean)
            mbIncludeInactiveEmp = value
        End Set
    End Property

    Public WriteOnly Property _Analysis_Fields() As String
        Set(ByVal value As String)
            mstrAnalysis_Fields = value
        End Set
    End Property

    Public WriteOnly Property _Analysis_Join() As String
        Set(ByVal value As String)
            mstrAnalysis_Join = value
        End Set
    End Property

    Public WriteOnly Property _Analysis_OrderBy() As String
        Set(ByVal value As String)
            mstrAnalysis_OrderBy = value
        End Set
    End Property

    Public WriteOnly Property _ViewIndex() As Integer
        Set(ByVal value As Integer)
            mintViewIndex = value
        End Set
    End Property

    Public WriteOnly Property _ViewByIds() As String
        Set(ByVal value As String)
            mstrViewByIds = value
        End Set
    End Property

    Public WriteOnly Property _ViewByName() As String
        Set(ByVal value As String)
            mstrViewByName = value
        End Set
    End Property

    Public WriteOnly Property _Report_GroupName() As String
        Set(ByVal value As String)
            mstrReport_GroupName = value
        End Set
    End Property

    Public WriteOnly Property _ReportId() As Integer
        Set(ByVal value As Integer)
            mintReportId = value
        End Set
    End Property

    Public WriteOnly Property _ReportTypeName() As String
        Set(ByVal value As String)
            mstrReportTypeName = value
        End Set
    End Property

    Public WriteOnly Property _CountryId() As Integer
        Set(ByVal value As Integer)
            mintCountryId = value
        End Set
    End Property

    Public WriteOnly Property _BaseCurrencyId() As Integer
        Set(ByVal value As Integer)
            mintBaseCurrId = value
        End Set
    End Property

    Public WriteOnly Property _PaidCurrencyId() As Integer
        Set(ByVal value As Integer)
            mintPaidCurrencyId = value
        End Set
    End Property

    Public WriteOnly Property _ConversionRate() As Decimal
        Set(ByVal value As Decimal)
            mdecConversionRate = value
        End Set
    End Property

    Public WriteOnly Property _ExchangeRate() As String
        Set(ByVal value As String)
            mstrExchangeRate = value
        End Set
    End Property

    Public WriteOnly Property _Advance_Filter() As String
        Set(ByVal value As String)
            mstrAdvance_Filter = value
        End Set
    End Property
    Public WriteOnly Property _ShowRemark() As Boolean
        Set(ByVal value As Boolean)
            mblnShowRemark = value
        End Set
    End Property
    Public WriteOnly Property _IgnoreZero() As Boolean
        Set(ByVal value As Boolean)
            mblnIgnoreZero = value
        End Set
    End Property
    Public WriteOnly Property _PeriodCode() As String
        Set(ByVal value As String)
            mstrPeriodCode = value
        End Set
    End Property

    Public WriteOnly Property _ShowReportNameOnReport() As Boolean
        Set(ByVal value As Boolean)
            mblnShowReportNameOnReport = value
        End Set
    End Property

    Public WriteOnly Property _FirstNamethenSurname() As Boolean
        Set(ByVal value As Boolean)
            mblnFirstNamethenSurname = value
        End Set
    End Property
    Public WriteOnly Property _OtherEarningTranId() As Integer
        Set(ByVal value As Integer)
            mintOtherEarningTranId = value
        End Set
    End Property
    Public WriteOnly Property _ShowBasicSalary() As Boolean
        Set(ByVal value As Boolean)
            mblnShowBasicSalary = value
        End Set
    End Property

    'Sohail (10 Jul 2019) -- Start
    'Good Neighbours Enhancement - Support Issue Id # 3875 - 76.1 - Put link for NHIF EFT and it should generate report without report header of report name address and company name and add "EmployerNo" column..
    Public WriteOnly Property _CurrencyName() As String
        Set(ByVal value As String)
            mstrCurrencyName = value
        End Set
    End Property
    'Sohail (10 Jul 2019) -- End

#End Region

#Region "Public Function & Procedures "

    Public Sub SetDefaultValue()
        Try
            mintMembershipId = 0
            mstrMembershipName = ""
            mintNonPayrollMembershipId = 0
            mstrNonPayrollMembershipName = ""
            mintPeriodId = 0
            mstrPeriodName = ""
            mbIncludeInactiveEmp = True
            mintReportId = -1
            mstrReportTypeName = ""
            mstrAnalysis_Fields = ""
            mstrAnalysis_Join = ""
            mstrAnalysis_OrderBy = ""
            mintViewIndex = -1
            mstrViewByIds = ""
            mstrReport_GroupName = ""
            mintCountryId = 0
            mintBaseCurrId = 0
            mintPaidCurrencyId = 0
            mdecConversionRate = 0
            mstrExchangeRate = ""
            mstrPeriodCode = ""
            mblnShowReportNameOnReport = False
            'Sohail (10 Jul 2019) -- Start
            'Good Neighbours Enhancement - Support Issue Id # 3875 - 76.1 - Put link for NHIF EFT and it should generate report without report header of report name address and company name and add "EmployerNo" column..
            mstrCurrencyName = String.Empty
            'Sohail (10 Jul 2019) -- End

            mintOtherEarningTranId = 0 
            mblnShowBasicSalary = False
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: SetDefaultValue; Module Name: " & mstrModuleName)
        End Try
    End Sub

    Private Sub FilterTitleAndFilterQuery()
        Me._FilterQuery = ""
        Me._FilterTitle = ""
        Try

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: FilterTitleAndFilterQuery; Module Name: " & mstrModuleName)
        End Try
    End Sub

    Public Overloads Overrides Sub generateReport(ByVal pintReportType As Integer, Optional ByVal PrintAction As Aruti.Data.enPrintAction = Aruti.Data.enPrintAction.Preview, Optional ByVal ExportAction As Aruti.Data.enExportAction = Aruti.Data.enExportAction.None)

    End Sub

    Public Overrides Sub generateReportNew(ByVal xDatabaseName As String, ByVal xUserUnkid As Integer, ByVal xYearUnkid As Integer, ByVal xCompanyUnkid As Integer, ByVal xPeriodStart As Date, ByVal xPeriodEnd As Date, ByVal xUserModeSetting As String, ByVal xOnlyApproved As Boolean, ByVal xExportReportPath As String, ByVal xOpenReportAfterExport As Boolean, ByVal pintReportType As Integer, Optional ByVal PrintAction As enPrintAction = enPrintAction.Preview, Optional ByVal ExportAction As enExportAction = enExportAction.None, Optional ByVal xBaseCurrencyId As Integer = 0)
        Dim objRpt As CrystalDecisions.CrystalReports.Engine.ReportClass = Nothing
        Dim strReportExportFile As String = ""
        Try
            menExportAction = ExportAction
            mdtTableExcel = Nothing

            objRpt = Generate_DetailReport(xDatabaseName, xUserUnkid, xYearUnkid, xCompanyUnkid, xPeriodEnd, xUserModeSetting, xOnlyApproved, xBaseCurrencyId)


            If Not IsNothing(objRpt) Then
                Dim intArrayColumnWidth As Integer() = {130, 100, 100, 110, 110, 110, 120, 150}
                Dim rowsArrayHeader As New ArrayList
                Call ReportExecute(objRpt, PrintAction, ExportAction, xExportReportPath, xOpenReportAfterExport, mdtTableExcel, intArrayColumnWidth, True, True, True, Nothing, mstrReportTypeName, "", " ", Nothing, "", True, rowsArrayHeader, Nothing, Nothing, Nothing, Nothing)
            End If

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: generateReportNew; Module Name: " & mstrModuleName)
        Finally
        End Try
    End Sub

    Public Overrides Sub setDefaultOrderBy(ByVal intReportType As Integer)
        OrderByDisplay = ""
        OrderByQuery = ""
        Try

            OrderByDisplay = iColumn_DetailReport.ColumnItem(0).DisplayName
            OrderByQuery = iColumn_DetailReport.ColumnItem(0).Name

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: setDefaultOrderBy; Module Name: " & mstrModuleName)
        End Try
    End Sub

    Public Overrides Sub setOrderBy(ByVal intReportType As Integer)
        Try

            Call OrderByExecute(iColumn_DetailReport)
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: setOrderBy; Module Name: " & mstrModuleName)
        End Try
    End Sub

#End Region

#Region " Report Generation "

    Dim iColumn_DetailReport As New IColumnCollection

    Public Property Field_OnDetailReport() As IColumnCollection
        Get
            Return iColumn_DetailReport
        End Get
        Set(ByVal value As IColumnCollection)
            iColumn_DetailReport = value
        End Set
    End Property

    Private Sub Create_OnDetailReport()
        Try
            iColumn_DetailReport.Clear()
            iColumn_DetailReport.Add(New IColumn("", ""))
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: Create_OnDetailReport; Module Name: " & mstrModuleName)
        End Try
    End Sub

    Private Function Generate_DetailReport(ByVal strDatabaseName As String, _
                                           ByVal intUserUnkid As Integer, _
                                           ByVal intYearUnkid As Integer, _
                                           ByVal intCompanyUnkid As Integer, _
                                           ByVal dtPeriodEnd As Date, _
                                           ByVal strUserModeSetting As String, _
                                           ByVal blnOnlyApproved As Boolean, _
                                           ByVal intBaseCurrencyId As Integer) As CrystalDecisions.CrystalReports.Engine.ReportClass
        Dim objRpt As CrystalDecisions.CrystalReports.Engine.ReportClass = Nothing
        Dim StrQ As String = ""
        Dim dsList As New DataSet
        Dim dsBasGross As New DataSet
        Dim exForce As Exception
        Dim rpt_Data As ArutiReport.Designer.dsArutiReport
        Try
            objDataOperation = New clsDataOperation

            Dim objExchangeRate As New clsExchangeRate
            Dim decDecimalPlaces As Decimal = 0

            objExchangeRate._ExchangeRateunkid = intBaseCurrencyId
            decDecimalPlaces = objExchangeRate._Digits_After_Decimal
            Dim strENameQ As String = ""
            If mblnFirstNamethenSurname = True Then
                strENameQ &= ", ISNULL(hremployee_master.firstname,'')+' '+ISNULL(hremployee_master.othername,'')+' '+ISNULL(hremployee_master.surname,'') AS EName " & _
                             ", ISNULL(hremployee_master.firstname,'') AS firstname " & _
                             ", ISNULL(hremployee_master.othername,'') AS middlename " & _
                             ", ISNULL(hremployee_master.surname,'') AS surname "
            Else
                strENameQ &= ", ISNULL(hremployee_master.surname,'')+' '+ISNULL(hremployee_master.firstname,'')+' '+ISNULL(hremployee_master.othername,'') AS EName " & _
                             ", ISNULL(hremployee_master.firstname,'') AS firstname " & _
                             ", ISNULL(hremployee_master.othername,'') AS middlename " & _
                             ", ISNULL(hremployee_master.surname,'') AS surname "
            End If
            Dim xUACQry, xUACFiltrQry As String : xUACQry = "" : xUACFiltrQry = ""
            Call NewAccessLevelFilterString(xUACQry, xUACFiltrQry, dtPeriodEnd, blnOnlyApproved, strDatabaseName, intUserUnkid, intCompanyUnkid, intYearUnkid, strUserModeSetting)

            If mintBaseCurrId = mintPaidCurrencyId Then

                StrQ = "SELECT hremployee_master.employeecode AS employeecode " & _
                             strENameQ & _
                             ", ISNULL(hremployee_meminfo_tran.membershipno,'') AS MemshipNo " & _
                             ", ISNULL(CAST(prpayrollprocess_tran.amount AS DECIMAL(36, " & decDecimalPlaces & ")),0) AS empcontribution " & _
                             ", ISNULL(CAmount,0) AS employercontribution " & _
                             ", ISNULL(CAST(prpayrollprocess_tran.amount AS DECIMAL(36, " & decDecimalPlaces & ")),0)+ISNULL(CAmount,0) AS Amount " & _
                             ", ISNULL(cfcommon_period_tran.period_name,'') AS PeriodName " & _
                             ", hremployee_master.employeeunkid AS EmpId " & _
                             ", ISNULL(cfcommon_master.name, '') AS MemshipCategory " & _
                             ", '' AS OtherMembership " & _
                             ", '' AS Remarks " & _
                             ", 0 As NonPayrollMembership " & _
                       "FROM    prpayrollprocess_tran " & _
                               "JOIN hremployee_master ON prpayrollprocess_tran.employeeunkid = hremployee_master.employeeunkid " & _
                               "JOIN hremployee_meminfo_tran ON prpayrollprocess_tran.membershiptranunkid = hremployee_meminfo_tran.membershiptranunkid " & _
                                                "AND hremployee_meminfo_tran.employeeunkid = hremployee_master.employeeunkid " & _
                                                "AND ISNULL(hremployee_meminfo_tran.isdeleted,0) = 0" & _
                               "LEFT JOIN hrmembership_master ON hremployee_meminfo_tran.membershipunkid = hrmembership_master.membershipunkid " & _
                                                "AND prpayrollprocess_tran.tranheadunkid = hrmembership_master.emptranheadunkid " & _
                               "JOIN prtnaleave_tran ON prpayrollprocess_tran.tnaleavetranunkid = prtnaleave_tran.tnaleavetranunkid " & _
                               "LEFT JOIN cfcommon_period_tran ON prtnaleave_tran.payperiodunkid = cfcommon_period_tran.periodunkid " & _
                                                "AND ISNULL(cfcommon_period_tran.isactive,0) = 1 " & _
                               "LEFT JOIN cfcommon_master ON cfcommon_master.masterunkid = hrmembership_master.membershipcategoryunkid " & _
                                                "AND cfcommon_master.mastertype = " & clsCommon_Master.enCommonMaster.MEMBERSHIP_CATEGORY & " "

                StrQ &= mstrAnalysis_Join
                If xUACQry.Trim.Length > 0 Then
                    StrQ &= xUACQry
                End If
                StrQ &= "       LEFT JOIN ( SELECT  hremployee_master.employeeunkid AS EmpId " & _
                                                 ", ISNULL(CAST(prpayrollprocess_tran.amount AS DECIMAL(36, " & decDecimalPlaces & ")),0) AS CAmount " & _
                                           "FROM    prpayrollprocess_tran " & _
                                                   "JOIN hremployee_master ON prpayrollprocess_tran.employeeunkid = hremployee_master.employeeunkid " & _
                                                   "JOIN hremployee_meminfo_tran ON prpayrollprocess_tran.membershiptranunkid = hremployee_meminfo_tran.membershiptranunkid " & _
                                                                    "AND hremployee_meminfo_tran.employeeunkid = hremployee_master.employeeunkid " & _
                                                                    "AND ISNULL(hremployee_meminfo_tran.isdeleted,0) = 0 " & _
                                                   "LEFT JOIN hrmembership_master ON hremployee_meminfo_tran.membershipunkid = hrmembership_master.membershipunkid " & _
                                                                    "AND prpayrollprocess_tran.tranheadunkid = hrmembership_master.cotranheadunkid " & _
                                                   "JOIN prtnaleave_tran ON prpayrollprocess_tran.tnaleavetranunkid = prtnaleave_tran.tnaleavetranunkid " & _
                                                   "LEFT JOIN cfcommon_period_tran ON prtnaleave_tran.payperiodunkid = cfcommon_period_tran.periodunkid " & _
                                                                    "AND ISNULL(cfcommon_period_tran.isactive,0) = 0 "

                StrQ &= mstrAnalysis_Join
                If xUACQry.Trim.Length > 0 Then
                    StrQ &= xUACQry
                End If

                StrQ &= "                   WHERE ISNULL(prpayrollprocess_tran.isvoid,0) = 0 " & _
                                           "AND ISNULL(prtnaleave_tran.isvoid,0) = 0 " & _
                                           "AND prtnaleave_tran.payperiodunkid = @PeriodId " & _
                                           "AND hrmembership_master.membershipunkid = @MemId "

                If xUACFiltrQry.Trim.Length > 0 Then
                    StrQ &= " AND " & xUACFiltrQry
                End If
                StrQ &= "           ) AS C ON C.EmpId = hremployee_master.employeeunkid " & _
                                        "WHERE ISNULL(prpayrollprocess_tran.isvoid,0) = 0 " & _
                                        "AND ISNULL(prtnaleave_tran.isvoid, 0) = 0 " & _
                                        "AND prtnaleave_tran.payperiodunkid = @PeriodId " & _
                                        "AND hrmembership_master.membershipunkid = @MemId "

                If mstrAdvance_Filter.Trim.Length > 0 Then
                    StrQ &= " AND " & mstrAdvance_Filter
                End If

                If xUACFiltrQry.Trim.Length > 0 Then
                    StrQ &= " AND " & xUACFiltrQry
                End If


                If mintNonPayrollMembershipId > 0 Then

                    StrQ &= "UNION ALL " & _
                                "SELECT hremployee_master.employeecode AS employeecode " & _
                                      strENameQ & _
                                      ",'' AS MemshipNo " & _
                                      ", 0 AS empcontribution " & _
                                      ", 0 AS employercontribution " & _
                                      ", 0 AS Amount " & _
                                      ", '' AS PeriodName " & _
                                      ", hremployee_master.employeeunkid AS EmpId " & _
                                      ", '' AS MemshipCategory " & _
                                      ",ISNULL(hremployee_meminfo_tran.membershipno,'') AS OtherMembership " & _
                                      ", '' AS Remarks " & _
                                      ", 1 AS NonPayrollMembership  " & _
                                      "FROM    hremployee_master " & _
                                        "JOIN hremployee_meminfo_tran ON hremployee_meminfo_tran.employeeunkid = hremployee_master.employeeunkid " & _
                                                        "AND ISNULL(hremployee_meminfo_tran.isdeleted,0) = 0 " & _
                                        "LEFT JOIN hrmembership_master ON hremployee_meminfo_tran.membershipunkid = hrmembership_master.membershipunkid "

                    StrQ &= mstrAnalysis_Join

                    If xUACQry.Trim.Length > 0 Then
                        StrQ &= xUACQry
                    End If

                    StrQ &= "WHERE   hrmembership_master.membershipunkid IN ( " & mintNonPayrollMembershipId.ToString & " ) "

                    If mstrAdvance_Filter.Trim.Length > 0 Then
                        StrQ &= " AND " & mstrAdvance_Filter
                    End If

                    If xUACFiltrQry.Trim.Length > 0 Then
                        StrQ &= " AND " & xUACFiltrQry
                    End If

                End If

                StrQ &= "ORDER BY employeecode "

            ElseIf mintBaseCurrId <> mintPaidCurrencyId Then

                StrQ = "SELECT hremployee_master.employeecode AS employeecode " & _
                             strENameQ & _
                             ", ISNULL(hremployee_meminfo_tran.membershipno,'') AS MemshipNo " & _
                             ", ( prpayment_tran.expaidrate / prpayment_tran.baseexchangerate ) *  ISNULL(CAST(prpayrollprocess_tran.amount AS DECIMAL(36, " & decDecimalPlaces & ")),0) AS empcontribution " & _
                             ", ( prpayment_tran.expaidrate / prpayment_tran.baseexchangerate ) *  ISNULL(CAmount,0) AS employercontribution " & _
                             ", ( prpayment_tran.expaidrate / prpayment_tran.baseexchangerate ) *  (ISNULL(CAST(prpayrollprocess_tran.amount AS DECIMAL(36, " & decDecimalPlaces & ")),0) +  ISNULL(CAmount,0)) AS Amount " & _
                             ", ISNULL(cfcommon_period_tran.period_name,'') AS PeriodName " & _
                             ", hremployee_master.employeeunkid AS EmpId " & _
                             ", ISNULL(cfcommon_master.name, '') AS MemshipCategory " & _
                             ", '' AS OtherMembership " & _
                             ", '' AS Remarks " & _
                              ", 0 As NonPayrollMembership " & _
                       "FROM    prpayrollprocess_tran " & _
                               "JOIN hremployee_master ON prpayrollprocess_tran.employeeunkid = hremployee_master.employeeunkid " & _
                               "JOIN hremployee_meminfo_tran ON prpayrollprocess_tran.membershiptranunkid = hremployee_meminfo_tran.membershiptranunkid " & _
                                                "AND hremployee_meminfo_tran.employeeunkid = hremployee_master.employeeunkid " & _
                                                "AND ISNULL(hremployee_meminfo_tran.isdeleted,0) = 0" & _
                               "LEFT JOIN hrmembership_master ON hremployee_meminfo_tran.membershipunkid = hrmembership_master.membershipunkid " & _
                                                "AND prpayrollprocess_tran.tranheadunkid = hrmembership_master.emptranheadunkid " & _
                               "JOIN prtnaleave_tran ON prpayrollprocess_tran.tnaleavetranunkid = prtnaleave_tran.tnaleavetranunkid " & _
                               "LEFT JOIN cfcommon_period_tran ON prtnaleave_tran.payperiodunkid = cfcommon_period_tran.periodunkid " & _
                                                "AND ISNULL(cfcommon_period_tran.isactive,0) = 1 " & _
                               "LEFT JOIN cfcommon_master ON cfcommon_master.masterunkid = hrmembership_master.membershipcategoryunkid " & _
                                                "AND cfcommon_master.mastertype = " & clsCommon_Master.enCommonMaster.MEMBERSHIP_CATEGORY & " " & _
                               "JOIN prpayment_tran ON prpayment_tran.periodunkid = prtnaleave_tran.payperiodunkid " & _
                                                "AND prtnaleave_tran.employeeunkid = prpayment_tran.employeeunkid " & _
                                                "AND ISNULL(prpayment_tran.isvoid, 0) = 0 " & _
                                                "AND prpayment_tran.countryunkid = " & mintCountryId & " "

                StrQ &= " AND prpayment_tran.referenceid = " & clsPayment_tran.enPaymentRefId.PAYSLIP & " " & _
                            " AND prpayment_tran.paytypeid = " & clsPayment_tran.enPayTypeId.PAYMENT & " "



                StrQ &= mstrAnalysis_Join

                If xUACQry.Trim.Length > 0 Then
                    StrQ &= xUACQry
                End If
                StrQ &= "   LEFT JOIN ( SELECT  hremployee_master.employeeunkid AS EmpId " & _
                                             ", ISNULL(CAST(prpayrollprocess_tran.amount AS DECIMAL(36, " & decDecimalPlaces & ")),0) AS CAmount " & _
                                       "FROM    prpayrollprocess_tran " & _
                                               "JOIN hremployee_master ON prpayrollprocess_tran.employeeunkid = hremployee_master.employeeunkid " & _
                                               "JOIN hremployee_meminfo_tran ON prpayrollprocess_tran.membershiptranunkid = hremployee_meminfo_tran.membershiptranunkid " & _
                                                                "AND hremployee_meminfo_tran.employeeunkid = hremployee_master.employeeunkid " & _
                                                                "AND ISNULL(hremployee_meminfo_tran.isdeleted,0) = 0 " & _
                                               "LEFT JOIN hrmembership_master ON hremployee_meminfo_tran.membershipunkid = hrmembership_master.membershipunkid " & _
                                                                "AND prpayrollprocess_tran.tranheadunkid = hrmembership_master.cotranheadunkid " & _
                                               "JOIN prtnaleave_tran ON prpayrollprocess_tran.tnaleavetranunkid = prtnaleave_tran.tnaleavetranunkid " & _
                                               "LEFT JOIN cfcommon_period_tran ON prtnaleave_tran.payperiodunkid = cfcommon_period_tran.periodunkid " & _
                                                                "AND ISNULL(cfcommon_period_tran.isactive,0) = 0 "

                StrQ &= mstrAnalysis_Join

                If xUACQry.Trim.Length > 0 Then
                    StrQ &= xUACQry
                End If
                StrQ &= "               WHERE   ISNULL(prpayrollprocess_tran.isvoid,0) = 0 " & _
                                               "AND ISNULL(prtnaleave_tran.isvoid,0) = 0 " & _
                                               "AND prtnaleave_tran.payperiodunkid = @PeriodId " & _
                                               "AND hrmembership_master.membershipunkid = @MemId "

                If xUACFiltrQry.Trim.Length > 0 Then
                    StrQ &= " AND " & xUACFiltrQry
                End If
                StrQ &= "              ) AS C ON C.EmpId = hremployee_master.employeeunkid " & _
                                    "WHERE   ISNULL(prpayrollprocess_tran.isvoid,0) = 0 " & _
                                    "AND ISNULL(prtnaleave_tran.isvoid, 0) = 0 " & _
                                    "AND prtnaleave_tran.payperiodunkid = @PeriodId " & _
                                    "AND hrmembership_master.membershipunkid = @MemId " & _
                                    "AND prpayment_tran.periodunkid = @PeriodId "

                If mstrAdvance_Filter.Trim.Length > 0 Then
                    StrQ &= " AND " & mstrAdvance_Filter
                End If

                If xUACFiltrQry.Trim.Length > 0 Then
                    StrQ &= " AND " & xUACFiltrQry
                End If

                StrQ &= "UNION ALL " & _
                        "SELECT hremployee_master.employeecode AS employeecode " & _
                              strENameQ & _
                              ", ISNULL(hremployee_meminfo_tran.membershipno,'') AS MemshipNo " & _
                              ", " & mdecConversionRate & " *  ISNULL(CAST(prpayrollprocess_tran.amount AS DECIMAL(36, " & decDecimalPlaces & ")),0) AS empcontribution " & _
                              ", " & mdecConversionRate & " *  ISNULL(CAmount,0) AS employercontribution " & _
                              ", " & mdecConversionRate & " *  (ISNULL(CAST(prpayrollprocess_tran.amount AS DECIMAL(36, " & decDecimalPlaces & ")),0)+  ISNULL(CAmount,0)) AS Amount " & _
                              ", ISNULL(cfcommon_period_tran.period_name,'') AS PeriodName " & _
                              ", hremployee_master.employeeunkid AS EmpId " & _
                              ", ISNULL(cfcommon_master.name, '') AS MemshipCategory " & _
                              ", '' AS OtherMembership " & _
                              ", '' AS Remarks " & _
                               ", 0 As NonPayrollMembership " & _
                             "FROM    prpayrollprocess_tran " & _
                                "JOIN hremployee_master ON prpayrollprocess_tran.employeeunkid = hremployee_master.employeeunkid " & _
                                "JOIN hremployee_meminfo_tran ON prpayrollprocess_tran.membershiptranunkid = hremployee_meminfo_tran.membershiptranunkid " & _
                                                "AND hremployee_meminfo_tran.employeeunkid = hremployee_master.employeeunkid " & _
                                                "AND ISNULL(hremployee_meminfo_tran.isdeleted,0) = 0" & _
                                "LEFT JOIN hrmembership_master ON hremployee_meminfo_tran.membershipunkid = hrmembership_master.membershipunkid " & _
                                "AND prpayrollprocess_tran.tranheadunkid = hrmembership_master.emptranheadunkid " & _
                                "JOIN prtnaleave_tran ON prpayrollprocess_tran.tnaleavetranunkid = prtnaleave_tran.tnaleavetranunkid " & _
                                "LEFT JOIN cfcommon_period_tran ON prtnaleave_tran.payperiodunkid = cfcommon_period_tran.periodunkid " & _
                                                "AND ISNULL(cfcommon_period_tran.isactive,0) = 1 " & _
                                "LEFT JOIN cfcommon_master ON cfcommon_master.masterunkid = hrmembership_master.membershipcategoryunkid " & _
                                                "AND cfcommon_master.mastertype = " & clsCommon_Master.enCommonMaster.MEMBERSHIP_CATEGORY & " " & _
                                "JOIN prpayment_tran ON prpayment_tran.periodunkid = prtnaleave_tran.payperiodunkid " & _
                                                "AND prtnaleave_tran.employeeunkid = prpayment_tran.employeeunkid " & _
                                                "AND ISNULL(prpayment_tran.isvoid, 0) = 0 AND prpayment_tran.countryunkid <> " & mintCountryId & " "
                StrQ &= " AND prpayment_tran.referenceid = " & clsPayment_tran.enPaymentRefId.PAYSLIP & " " & _
                            " AND prpayment_tran.paytypeid = " & clsPayment_tran.enPayTypeId.PAYMENT & " "

                StrQ &= mstrAnalysis_Join
                If xUACQry.Trim.Length > 0 Then
                    StrQ &= xUACQry
                End If
                StrQ &= "        LEFT JOIN ( SELECT  hremployee_master.employeeunkid AS EmpId " & _
                                                  ", ISNULL(CAST(prpayrollprocess_tran.amount AS DECIMAL(36, " & decDecimalPlaces & ")),0) AS CAmount " & _
                                            "FROM    prpayrollprocess_tran " & _
                                                     "JOIN hremployee_master ON prpayrollprocess_tran.employeeunkid = hremployee_master.employeeunkid " & _
                                                     "JOIN hremployee_meminfo_tran ON prpayrollprocess_tran.membershiptranunkid = hremployee_meminfo_tran.membershiptranunkid " & _
                                                                    "AND hremployee_meminfo_tran.employeeunkid = hremployee_master.employeeunkid " & _
                                                                    "AND ISNULL(hremployee_meminfo_tran.isdeleted,0) = 0 " & _
                                                     "LEFT JOIN hrmembership_master ON hremployee_meminfo_tran.membershipunkid = hrmembership_master.membershipunkid " & _
                                                                    "AND prpayrollprocess_tran.tranheadunkid = hrmembership_master.cotranheadunkid " & _
                                                     "JOIN prtnaleave_tran ON prpayrollprocess_tran.tnaleavetranunkid = prtnaleave_tran.tnaleavetranunkid " & _
                                                     "LEFT JOIN cfcommon_period_tran ON prtnaleave_tran.payperiodunkid = cfcommon_period_tran.periodunkid " & _
                                                                    "AND ISNULL(cfcommon_period_tran.isactive,0) = 0 "

                StrQ &= mstrAnalysis_Join

                If xUACQry.Trim.Length > 0 Then
                    StrQ &= xUACQry
                End If


                StrQ &= "                    WHERE   ISNULL(prpayrollprocess_tran.isvoid,0) = 0 " & _
                                            "AND ISNULL(prtnaleave_tran.isvoid,0) = 0 " & _
                                            "AND prtnaleave_tran.payperiodunkid = @PeriodId " & _
                                            "AND hrmembership_master.membershipunkid = @MemId "
                If xUACFiltrQry.Trim.Length > 0 Then
                    StrQ &= " AND " & xUACFiltrQry
                End If


                StrQ &= "            ) AS C ON C.EmpId = hremployee_master.employeeunkid " & _
                                         "WHERE   ISNULL(prpayrollprocess_tran.isvoid,0) = 0 " & _
                                         "AND ISNULL(prtnaleave_tran.isvoid, 0) = 0 " & _
                                         "AND prtnaleave_tran.payperiodunkid = @PeriodId " & _
                                         "AND hrmembership_master.membershipunkid = @MemId " & _
                                         "AND prpayment_tran.periodunkid = @PeriodId "

                If mstrAdvance_Filter.Trim.Length > 0 Then
                    StrQ &= " AND " & mstrAdvance_Filter
                End If

                If xUACFiltrQry.Trim.Length > 0 Then
                    StrQ &= " AND " & xUACFiltrQry
                End If

                StrQ &= "UNION ALL " & _
                        "SELECT hremployee_master.employeecode AS employeecode " & _
                              strENameQ & _
                              ", ISNULL(hremployee_meminfo_tran.membershipno,'') AS MemshipNo " & _
                              ", " & mdecConversionRate & " *  ISNULL(CAST(prpayrollprocess_tran.amount AS DECIMAL(36, " & decDecimalPlaces & ")),0) AS empcontribution " & _
                              ", " & mdecConversionRate & " *  ISNULL(CAmount,0) AS employercontribution " & _
                              ", " & mdecConversionRate & " *  (ISNULL(CAST(prpayrollprocess_tran.amount AS DECIMAL(36, " & decDecimalPlaces & ")),0) +   ISNULL(CAmount,0))  AS Amount " & _
                              ", ISNULL(cfcommon_period_tran.period_name,'') AS PeriodName " & _
                              ", hremployee_master.employeeunkid AS EmpId " & _
                              ", ISNULL(cfcommon_master.name, '') AS MemshipCategory " & _
                              ", '' AS OtherMembership " & _
                              ", '' AS Remarks " & _
                               ", 0 As NonPayrollMembership " & _
                        "FROM    prpayrollprocess_tran " & _
                                "JOIN hremployee_master ON prpayrollprocess_tran.employeeunkid = hremployee_master.employeeunkid " & _
                                "JOIN hremployee_meminfo_tran ON prpayrollprocess_tran.membershiptranunkid = hremployee_meminfo_tran.membershiptranunkid " & _
                                                "AND hremployee_meminfo_tran.employeeunkid = hremployee_master.employeeunkid " & _
                                                "AND ISNULL(hremployee_meminfo_tran.isdeleted,0) = 0" & _
                                "LEFT JOIN hrmembership_master ON hremployee_meminfo_tran.membershipunkid = hrmembership_master.membershipunkid " & _
                                                "AND prpayrollprocess_tran.tranheadunkid = hrmembership_master.emptranheadunkid " & _
                                "JOIN prtnaleave_tran ON prpayrollprocess_tran.tnaleavetranunkid = prtnaleave_tran.tnaleavetranunkid " & _
                                "LEFT JOIN cfcommon_period_tran ON prtnaleave_tran.payperiodunkid = cfcommon_period_tran.periodunkid " & _
                                                "AND ISNULL(cfcommon_period_tran.isactive,0) = 1 " & _
                                "LEFT JOIN cfcommon_master ON cfcommon_master.masterunkid = hrmembership_master.membershipcategoryunkid " & _
                                                "AND cfcommon_master.mastertype = " & clsCommon_Master.enCommonMaster.MEMBERSHIP_CATEGORY & " " & _
                                "LEFT JOIN prpayment_tran ON prpayment_tran.periodunkid = prtnaleave_tran.payperiodunkid " & _
                                                "AND prtnaleave_tran.employeeunkid = prpayment_tran.employeeunkid " & _
                                                "AND ISNULL(prpayment_tran.isvoid, 0) = 0 "
                StrQ &= " AND prpayment_tran.referenceid = " & clsPayment_tran.enPaymentRefId.PAYSLIP & " " & _
                            " AND prpayment_tran.paytypeid = " & clsPayment_tran.enPayTypeId.PAYMENT & " "

                StrQ &= mstrAnalysis_Join
                If xUACQry.Trim.Length > 0 Then
                    StrQ &= xUACQry
                End If

                StrQ &= "        LEFT JOIN ( SELECT  hremployee_master.employeeunkid AS EmpId " & _
                                                  ", ISNULL(CAST(prpayrollprocess_tran.amount AS DECIMAL(36, " & decDecimalPlaces & ")),0) AS CAmount " & _
                                            "FROM    prpayrollprocess_tran " & _
                                                    "JOIN hremployee_master ON prpayrollprocess_tran.employeeunkid = hremployee_master.employeeunkid " & _
                                                    "JOIN hremployee_meminfo_tran ON prpayrollprocess_tran.membershiptranunkid = hremployee_meminfo_tran.membershiptranunkid " & _
                                                                    "AND hremployee_meminfo_tran.employeeunkid = hremployee_master.employeeunkid " & _
                                                                    "AND ISNULL(hremployee_meminfo_tran.isdeleted,0) = 0 " & _
                                                    "LEFT JOIN hrmembership_master ON hremployee_meminfo_tran.membershipunkid = hrmembership_master.membershipunkid " & _
                                                                    "AND prpayrollprocess_tran.tranheadunkid = hrmembership_master.cotranheadunkid " & _
                                                    "JOIN prtnaleave_tran ON prpayrollprocess_tran.tnaleavetranunkid = prtnaleave_tran.tnaleavetranunkid " & _
                                                    "LEFT JOIN cfcommon_period_tran ON prtnaleave_tran.payperiodunkid = cfcommon_period_tran.periodunkid " & _
                                                                    "AND ISNULL(cfcommon_period_tran.isactive,0) = 0 "

                StrQ &= mstrAnalysis_Join
                If xUACQry.Trim.Length > 0 Then
                    StrQ &= xUACQry
                End If

                StrQ &= "                    WHERE   ISNULL(prpayrollprocess_tran.isvoid,0) = 0 " & _
                                                    "AND ISNULL(prtnaleave_tran.isvoid,0) = 0 " & _
                                                    "AND prtnaleave_tran.payperiodunkid = @PeriodId " & _
                                                    "AND hrmembership_master.membershipunkid = @MemId "



                If xUACFiltrQry.Trim.Length > 0 Then
                    StrQ &= " AND " & xUACFiltrQry
                End If


                StrQ &= "              ) AS C ON C.EmpId = hremployee_master.employeeunkid " & _
                                          " WHERE   ISNULL(prpayrollprocess_tran.isvoid,0) = 0 " & _
                                          " AND ISNULL(prtnaleave_tran.isvoid, 0) = 0 " & _
                                          " AND prtnaleave_tran.payperiodunkid = @PeriodId " & _
                                          " AND hrmembership_master.membershipunkid = @MemId " & _
                                          " AND prpayment_tran.periodunkid IS NULL "

                If mstrAdvance_Filter.Trim.Length > 0 Then
                    StrQ &= " AND " & mstrAdvance_Filter
                End If

                If xUACFiltrQry.Trim.Length > 0 Then
                    StrQ &= " AND " & xUACFiltrQry
                End If

                If mintNonPayrollMembershipId > 0 Then

                    StrQ &= "UNION ALL " & _
                                "SELECT hremployee_master.employeecode AS employeecode " & _
                                     strENameQ & _
                                      ",'' AS MemshipNo " & _
                                      ", 0 AS empcontribution " & _
                                      ", 0 AS employercontribution " & _
                                      ", 0 AS Amount " & _
                                      ", '' AS PeriodName " & _
                                      ", hremployee_master.employeeunkid AS EmpId " & _
                                      ", '' AS MemshipCategory " & _
                                      ",ISNULL(hremployee_meminfo_tran.membershipno,'') AS OtherMembership " & _
                                      ", '' AS Remarks " & _
                                      ", 1 AS NonPayrollMembership  " & _
                                      "FROM    hremployee_master " & _
                                        "JOIN hremployee_meminfo_tran ON hremployee_meminfo_tran.employeeunkid = hremployee_master.employeeunkid " & _
                                                        "AND ISNULL(hremployee_meminfo_tran.isdeleted,0) = 0 " & _
                                        "LEFT JOIN hrmembership_master ON hremployee_meminfo_tran.membershipunkid = hrmembership_master.membershipunkid "
                    StrQ &= mstrAnalysis_Join
                    If xUACQry.Trim.Length > 0 Then
                        StrQ &= xUACQry
                    End If
                    StrQ &= "WHERE   hrmembership_master.membershipunkid IN ( " & mintNonPayrollMembershipId.ToString & " ) "

                    If mstrAdvance_Filter.Trim.Length > 0 Then
                        StrQ &= " AND " & mstrAdvance_Filter
                    End If
                    If xUACFiltrQry.Trim.Length > 0 Then
                        StrQ &= " AND " & xUACFiltrQry
                    End If

                End If

                StrQ &= "ORDER BY employeecode "

            End If

            objDataOperation.AddParameter("@MemId", SqlDbType.Int, eZeeDataType.INT_SIZE, mintMembershipId)
            objDataOperation.AddParameter("@PeriodId", SqlDbType.Int, eZeeDataType.INT_SIZE, mintPeriodId)


            dsList = objDataOperation.ExecQuery(StrQ, "DataTable")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & " " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            If mintBaseCurrId = mintPaidCurrencyId Then

                StrQ = "SELECT " & _
                        "	 A.Empid " & _
                        "	,BasicSal " & _
                        "	,GrossPay " & _
                        "FROM " & _
                        "( " & _
                        "	SELECT " & _
                        "		 prpayrollprocess_tran.employeeunkid AS Empid " & _
                        "		,SUM(CAST(ISNULL(prpayrollprocess_tran.amount, 0) AS DECIMAL(36, " & decDecimalPlaces & "))) AS BasicSal "

                If mintViewIndex > 0 Then
                    StrQ &= mstrAnalysis_Fields
                Else
                    StrQ &= ", 0 AS Id, '' AS GName "
                End If

                StrQ &= "	FROM prpayrollprocess_tran " & _
                        "		JOIN prtranhead_master ON prpayrollprocess_tran.tranheadunkid = prtranhead_master.tranheadunkid " & _
                        "		JOIN prtnaleave_tran ON prtnaleave_tran.tnaleavetranunkid = prpayrollprocess_tran.tnaleavetranunkid " & _
                        "		JOIN cfcommon_period_tran ON prtnaleave_tran.payperiodunkid = cfcommon_period_tran.periodunkid " & _
                        "		JOIN hremployee_master ON hremployee_master.employeeunkid = prpayrollprocess_tran.employeeunkid "

                StrQ &= mstrAnalysis_Join
                If xUACQry.Trim.Length > 0 Then
                    StrQ &= xUACQry
                End If

                StrQ &= "WHERE    ISNULL(prpayrollprocess_tran.isvoid, 0) = 0 " & _
                    "		AND ISNULL(prtranhead_master.isvoid, 0) = 0 " & _
                    "		AND ISNULL(prtnaleave_tran.isvoid, 0) = 0 " & _
                                 "AND prpayrollprocess_tran.tranheadunkid > 0 " & _
                    "		AND cfcommon_period_tran.isactive = 1 " & _
                                 "AND cfcommon_period_tran.periodunkid = @PeriodId "

                'Sohail (22 Jul 2019) -- Start
                'Good Neighbours Enhancement # - 76.1 - Pick Basic Salary as Other Earning if option is ticked on head master in NHIF Report.
                'If mintOtherEarningTranId > 0 Then 'Basic Salary as Other Earning
                '    StrQ &= "AND prpayrollprocess_tran.tranheadunkid = @OtherEarningTranId  "
                'Else
                '    StrQ &= "AND prtranhead_master.typeof_id = " & enTypeOf.Salary & "  "
                'End If
                StrQ &= "AND ((prtranhead_master.typeof_id = " & enTypeOf.Salary & " and prtranhead_master.trnheadtype_id = " & enTranHeadType.EarningForEmployees & ") OR prtranhead_master.isbasicsalaryasotherearning  = 1) "
                'Sohail (22 Jul 2019) -- End                

                If xUACFiltrQry.Trim.Length > 0 Then
                    StrQ &= " AND " & xUACFiltrQry
                End If

                StrQ &= "GROUP BY prpayrollprocess_tran.employeeunkid "
                If mintViewIndex > 0 Then
                    StrQ &= mstrAnalysis_Fields.Replace("AS Id", "").Replace("AS GName", "")
                End If

                StrQ &= ") AS A " & _
                        "JOIN " & _
                        "( " & _
                        "	SELECT " & _
                        "		 prpayrollprocess_tran.employeeunkid AS Empid " & _
                        "		,SUM(CAST(ISNULL(prpayrollprocess_tran.amount, 0) AS DECIMAL(36, " & decDecimalPlaces & "))) AS GrossPay "

                If mintViewIndex > 0 Then
                    StrQ &= mstrAnalysis_Fields
                Else
                    StrQ &= ", 0 AS Id, '' AS GName "
                End If

                StrQ &= "	FROM prpayrollprocess_tran " & _
                        "		JOIN prtranhead_master ON prpayrollprocess_tran.tranheadunkid = prtranhead_master.tranheadunkid " & _
                        "		JOIN prtnaleave_tran ON prtnaleave_tran.tnaleavetranunkid = prpayrollprocess_tran.tnaleavetranunkid " & _
                        "		JOIN cfcommon_period_tran ON prtnaleave_tran.payperiodunkid = cfcommon_period_tran.periodunkid " & _
                        "		JOIN hremployee_master ON hremployee_master.employeeunkid = prpayrollprocess_tran.employeeunkid "

                StrQ &= mstrAnalysis_Join

                If xUACQry.Trim.Length > 0 Then
                    StrQ &= xUACQry
                End If

                StrQ &= "	WHERE prtranhead_master.trnheadtype_id = 1 " & _
                        "		AND ISNULL(prpayrollprocess_tran.isvoid,0) = 0 " & _
                        "		AND ISNULL(prtranhead_master.isvoid, 0) = 0 " & _
                        "		AND ISNULL(prtnaleave_tran.isvoid, 0) = 0 " & _
                        "		AND cfcommon_period_tran.isactive = 1 " & _
                        "		AND cfcommon_period_tran.periodunkid = @PeriodId "

                If xUACFiltrQry.Trim.Length > 0 Then
                    StrQ &= " AND " & xUACFiltrQry
                End If

                StrQ &= "	GROUP BY prpayrollprocess_tran.employeeunkid "


                If mintViewIndex > 0 Then
                    StrQ &= mstrAnalysis_Fields.Replace("AS Id", "").Replace("AS GName", "")
                End If

            ElseIf mintBaseCurrId <> mintPaidCurrencyId Then

                StrQ = "SELECT " & _
                     "	 A.Empid " & _
                     "	,BasicSal " & _
                     "	,GrossPay " & _
                     "FROM " & _
                     "( " & _
                     "	SELECT " & _
                     "		 prpayrollprocess_tran.employeeunkid AS Empid " & _
                     "		,( prpayment_tran.expaidrate / prpayment_tran.baseexchangerate ) * SUM(CAST(ISNULL(prpayrollprocess_tran.amount, 0) AS DECIMAL(36, " & decDecimalPlaces & "))) AS BasicSal "

                If mintViewIndex > 0 Then
                    StrQ &= mstrAnalysis_Fields
                Else
                    StrQ &= ", 0 AS Id, '' AS GName "
                End If

                StrQ &= "   FROM prpayrollprocess_tran " & _
                             "   JOIN prtranhead_master ON prpayrollprocess_tran.tranheadunkid = prtranhead_master.tranheadunkid " & _
                             "   JOIN prtnaleave_tran ON prtnaleave_tran.tnaleavetranunkid = prpayrollprocess_tran.tnaleavetranunkid  " & _
                             "   JOIN cfcommon_period_tran ON prtnaleave_tran.payperiodunkid = cfcommon_period_tran.periodunkid " & _
                             "   JOIN hremployee_master ON hremployee_master.employeeunkid = prpayrollprocess_tran.employeeunkid " & _
                             "   JOIN prpayment_tran ON prpayrollprocess_tran.employeeunkid = prpayment_tran.employeeunkid AND prpayment_tran.isvoid = 0" & _
                             "   AND prtnaleave_tran.payperiodunkid = prpayment_tran.periodunkid AND prpayment_tran.countryunkid = " & mintCountryId


                StrQ &= " AND prpayment_tran.referenceid = " & clsPayment_tran.enPaymentRefId.PAYSLIP & " " & _
                            " AND prpayment_tran.paytypeid = " & clsPayment_tran.enPayTypeId.PAYMENT & " "


                StrQ &= mstrAnalysis_Join

                If xUACQry.Trim.Length > 0 Then
                    StrQ &= xUACQry
                End If
                StrQ &= "  WHERE     ISNULL(prpayrollprocess_tran.isvoid, 0) = 0 " & _
                             "  AND ISNULL(prtranhead_master.isvoid, 0) = 0 AND ISNULL(prtnaleave_tran.isvoid, 0) = 0 " & _
                                    "AND prpayrollprocess_tran.tranheadunkid > 0 " & _
                             "  AND cfcommon_period_tran.isactive = 1 AND cfcommon_period_tran.periodunkid = @PeriodId " & _
                                    "AND prpayment_tran.periodunkid = @PeriodId "

                'Sohail (22 Jul 2019) -- Start
                'Good Neighbours Enhancement # - 76.1 - Pick Basic Salary as Other Earning if option is ticked on head master in NHIF Report.
                'If mintOtherEarningTranId > 0 Then 'Basic Salary as Other Earning
                '    StrQ &= "AND prpayrollprocess_tran.tranheadunkid = @OtherEarningTranId  "
                'Else
                '    StrQ &= "AND prtranhead_master.typeof_id = " & enTypeOf.Salary & "  "
                'End If
                StrQ &= "AND ((prtranhead_master.typeof_id = " & enTypeOf.Salary & " and prtranhead_master.trnheadtype_id = " & enTranHeadType.EarningForEmployees & ") OR prtranhead_master.isbasicsalaryasotherearning  = 1) "
                'Sohail (22 Jul 2019) -- End

                If xUACFiltrQry.Trim.Length > 0 Then
                    StrQ &= " AND " & xUACFiltrQry
                End If


                StrQ &= "GROUP BY  prpayrollprocess_tran.employeeunkid,prpayment_tran.expaidrate,prpayment_tran.baseexchangerate "

                If mintViewIndex > 0 Then
                    StrQ &= mstrAnalysis_Fields.Replace("AS Id", "").Replace("AS GName", "")
                End If

                StrQ &= "  UNION ALL " & _
                            "  SELECT prpayrollprocess_tran.employeeunkid AS Empid " & _
                            ", " & mdecConversionRate & " * SUM(CAST(ISNULL(prpayrollprocess_tran.amount, 0) AS DECIMAL(36," & decDecimalPlaces & "))) AS BasicSal "

                If mintViewIndex > 0 Then
                    StrQ &= mstrAnalysis_Fields
                Else
                    StrQ &= ", 0 AS Id, '' AS GName "
                End If

                StrQ &= "   FROM  prpayrollprocess_tran " & _
                             "   JOIN prtranhead_master ON prpayrollprocess_tran.tranheadunkid = prtranhead_master.tranheadunkid " & _
                             "   JOIN prtnaleave_tran ON prtnaleave_tran.tnaleavetranunkid = prpayrollprocess_tran.tnaleavetranunkid " & _
                             "   JOIN cfcommon_period_tran ON prtnaleave_tran.payperiodunkid = cfcommon_period_tran.periodunkid " & _
                             "   JOIN hremployee_master ON hremployee_master.employeeunkid = prpayrollprocess_tran.employeeunkid " & _
                             "   JOIN prpayment_tran ON prpayrollprocess_tran.employeeunkid = prpayment_tran.employeeunkid   AND prpayment_tran.isvoid = 0 " & _
                             "   AND prtnaleave_tran.payperiodunkid = prpayment_tran.periodunkid AND prpayment_tran.countryunkid <> " & mintCountryId

                StrQ &= " AND prpayment_tran.referenceid = " & clsPayment_tran.enPaymentRefId.PAYSLIP & " " & _
                            " AND prpayment_tran.paytypeid = " & clsPayment_tran.enPayTypeId.PAYMENT & " "

                StrQ &= mstrAnalysis_Join
                If xUACQry.Trim.Length > 0 Then
                    StrQ &= xUACQry
                End If
                StrQ &= "    WHERE    ISNULL(prpayrollprocess_tran.isvoid, 0) = 0 " & _
                             "    AND ISNULL(prtranhead_master.isvoid, 0) = 0  AND ISNULL(prtnaleave_tran.isvoid, 0) = 0 " & _
                                     "AND prpayrollprocess_tran.tranheadunkid > 0 " & _
                             "    AND cfcommon_period_tran.isactive = 1 AND cfcommon_period_tran.periodunkid = @PeriodId " & _
                                     "AND prpayment_tran.periodunkid = @PeriodId "

                'Sohail (22 Jul 2019) -- Start
                'Good Neighbours Enhancement # - 76.1 - Pick Basic Salary as Other Earning if option is ticked on head master in NHIF Report.
                'If mintOtherEarningTranId > 0 Then 'Basic Salary as Other Earning
                '    StrQ &= "AND prpayrollprocess_tran.tranheadunkid = @OtherEarningTranId  "
                'Else
                '    StrQ &= "AND prtranhead_master.typeof_id = " & enTypeOf.Salary & "  "
                'End If
                StrQ &= "AND ((prtranhead_master.typeof_id = " & enTypeOf.Salary & " and prtranhead_master.trnheadtype_id = " & enTranHeadType.EarningForEmployees & ") OR prtranhead_master.isbasicsalaryasotherearning  = 1) "
                'Sohail (22 Jul 2019) -- End

                If xUACFiltrQry.Trim.Length > 0 Then
                    StrQ &= " AND " & xUACFiltrQry
                End If

                StrQ &= "GROUP BY  prpayrollprocess_tran.employeeunkid "

                If mintViewIndex > 0 Then
                    StrQ &= mstrAnalysis_Fields.Replace("AS Id", "").Replace("AS GName", "")
                End If

                StrQ &= "     UNION ALL " & _
                             "     SELECT prpayrollprocess_tran.employeeunkid AS Empid " & _
                             "     ," & mdecConversionRate & " * SUM(CAST(ISNULL(prpayrollprocess_tran.amount, 0) AS DECIMAL(36," & decDecimalPlaces & "))) AS BasicSal"


                If mintViewIndex > 0 Then
                    StrQ &= mstrAnalysis_Fields
                Else
                    StrQ &= ", 0 AS Id, '' AS GName "
                End If

                StrQ &= "    FROM prpayrollprocess_tran " & _
                             "    JOIN prtranhead_master ON prpayrollprocess_tran.tranheadunkid = prtranhead_master.tranheadunkid " & _
                             "    JOIN prtnaleave_tran ON prtnaleave_tran.tnaleavetranunkid = prpayrollprocess_tran.tnaleavetranunkid " & _
                             "    JOIN cfcommon_period_tran ON prtnaleave_tran.payperiodunkid = cfcommon_period_tran.periodunkid " & _
                             "    JOIN hremployee_master ON hremployee_master.employeeunkid = prpayrollprocess_tran.employeeunkid " & _
                             "    LEFT JOIN prpayment_tran ON prpayrollprocess_tran.employeeunkid = prpayment_tran.employeeunkid  AND prpayment_tran.isvoid = 0 " & _
                             "    AND prtnaleave_tran.payperiodunkid = prpayment_tran.periodunkid "

                StrQ &= " AND prpayment_tran.referenceid = " & clsPayment_tran.enPaymentRefId.PAYSLIP & " " & _
                            " AND prpayment_tran.paytypeid = " & clsPayment_tran.enPayTypeId.PAYMENT & " "

                StrQ &= mstrAnalysis_Join
                If xUACQry.Trim.Length > 0 Then
                    StrQ &= xUACQry
                End If
                StrQ &= "  WHERE     ISNULL(prpayrollprocess_tran.isvoid, 0) = 0 " & _
                             "  AND ISNULL(prtranhead_master.isvoid, 0) = 0 AND ISNULL(prtnaleave_tran.isvoid, 0) = 0 " & _
                                    "AND prpayrollprocess_tran.tranheadunkid > 0 " & _
                             "  AND cfcommon_period_tran.isactive = 1 AND cfcommon_period_tran.periodunkid = @PeriodId " & _
                                    "AND prpayment_tran.paymenttranunkid IS NULL "

                'Sohail (22 Jul 2019) -- Start
                'Good Neighbours Enhancement # - 76.1 - Pick Basic Salary as Other Earning if option is ticked on head master in NHIF Report.
                'If mintOtherEarningTranId > 0 Then 'Basic Salary as Other Earning
                '    StrQ &= "AND prpayrollprocess_tran.tranheadunkid = @OtherEarningTranId  "
                'Else
                '    StrQ &= "AND prtranhead_master.typeof_id = " & enTypeOf.Salary & "  "
                'End If
                StrQ &= "AND ((prtranhead_master.typeof_id = " & enTypeOf.Salary & " and prtranhead_master.trnheadtype_id = " & enTranHeadType.EarningForEmployees & ") OR prtranhead_master.isbasicsalaryasotherearning  = 1) "
                'Sohail (22 Jul 2019) -- End

                If xUACFiltrQry.Trim.Length > 0 Then
                    StrQ &= " AND " & xUACFiltrQry
                End If

                StrQ &= "GROUP BY  prpayrollprocess_tran.employeeunkid"

                If mintViewIndex > 0 Then
                    StrQ &= mstrAnalysis_Fields.Replace("AS Id", "").Replace("AS GName", "")
                End If

                StrQ &= ") AS A " & _
                             "JOIN " & _
                             "  ( " & _
                             "           SELECT   prpayrollprocess_tran.employeeunkid AS Empid " & _
                             "          ,( prpayment_tran.expaidrate / prpayment_tran.baseexchangerate ) * SUM(CAST(ISNULL(prpayrollprocess_tran.amount, 0) AS DECIMAL(36," & decDecimalPlaces & "))) AS GrossPay "

                If mintViewIndex > 0 Then
                    StrQ &= mstrAnalysis_Fields
                Else
                    StrQ &= ", 0 AS Id, '' AS GName "
                End If

                StrQ &= "          FROM prpayrollprocess_tran " & _
                            "           JOIN prtranhead_master ON prpayrollprocess_tran.tranheadunkid = prtranhead_master.tranheadunkid " & _
                            "           JOIN prtnaleave_tran ON prtnaleave_tran.tnaleavetranunkid = prpayrollprocess_tran.tnaleavetranunkid " & _
                            "           JOIN cfcommon_period_tran ON prtnaleave_tran.payperiodunkid = cfcommon_period_tran.periodunkid " & _
                            "           JOIN hremployee_master ON hremployee_master.employeeunkid = prpayrollprocess_tran.employeeunkid " & _
                            "           JOIN prpayment_tran ON prpayment_tran.employeeunkid = prpayrollprocess_tran.employeeunkid  AND prpayment_tran.isvoid = 0 " & _
                            "           AND prpayment_tran.periodunkid = prtnaleave_tran.payperiodunkid AND prpayment_tran.countryunkid = " & mintCountryId

                StrQ &= " AND prpayment_tran.referenceid = " & clsPayment_tran.enPaymentRefId.PAYSLIP & " " & _
                            " AND prpayment_tran.paytypeid = " & clsPayment_tran.enPayTypeId.PAYMENT & " "


                StrQ &= mstrAnalysis_Join

                If xUACQry.Trim.Length > 0 Then
                    StrQ &= xUACQry
                End If

                StrQ &= "          WHERE  prtranhead_master.trnheadtype_id =1 AND ISNULL(prpayrollprocess_tran.isvoid, 0) = 0 " & _
                            "           AND ISNULL(prtranhead_master.isvoid, 0) = 0 AND ISNULL(prtnaleave_tran.isvoid, 0) = 0 " & _
                             "          AND cfcommon_period_tran.isactive = 1 AND cfcommon_period_tran.periodunkid = @PeriodId " & _
                             "          AND prpayment_tran.periodunkid= @PeriodId "

                If xUACFiltrQry.Trim.Length > 0 Then
                    StrQ &= " AND " & xUACFiltrQry
                End If

                StrQ &= "          GROUP BY prpayrollprocess_tran.employeeunkid,prpayment_tran.expaidrate,prpayment_tran.baseexchangerate "

                If mintViewIndex > 0 Then
                    StrQ &= mstrAnalysis_Fields.Replace("AS Id", "").Replace("AS GName", "")
                End If

                StrQ &= "          UNION ALL " & _
                             "          SELECT   prpayrollprocess_tran.employeeunkid AS Empid " & _
                             ",         " & mdecConversionRate & " * SUM(CAST(ISNULL(prpayrollprocess_tran.amount, 0) AS DECIMAL(36," & decDecimalPlaces & "))) AS GrossPay "

                If mintViewIndex > 0 Then
                    StrQ &= mstrAnalysis_Fields
                Else
                    StrQ &= ", 0 AS Id, '' AS GName "
                End If

                StrQ &= "             FROM prpayrollprocess_tran " & _
                             "             JOIN prtranhead_master ON prpayrollprocess_tran.tranheadunkid = prtranhead_master.tranheadunkid " & _
                             "             JOIN prtnaleave_tran ON prtnaleave_tran.tnaleavetranunkid = prpayrollprocess_tran.tnaleavetranunkid " & _
                             "             JOIN cfcommon_period_tran ON prtnaleave_tran.payperiodunkid = cfcommon_period_tran.periodunkid " & _
                             "             JOIN hremployee_master ON hremployee_master.employeeunkid = prpayrollprocess_tran.employeeunkid " & _
                             "             JOIN prpayment_tran ON prpayment_tran.employeeunkid = prpayrollprocess_tran.employeeunkid  AND prpayment_tran.isvoid = 0 " & _
                             "             AND prpayment_tran.periodunkid = prtnaleave_tran.payperiodunkid AND prpayment_tran.countryunkid <> " & mintCountryId

                StrQ &= " AND prpayment_tran.referenceid = " & clsPayment_tran.enPaymentRefId.PAYSLIP & " " & _
                            " AND prpayment_tran.paytypeid = " & clsPayment_tran.enPayTypeId.PAYMENT & " "

                StrQ &= mstrAnalysis_Join
                If xUACQry.Trim.Length > 0 Then
                    StrQ &= xUACQry
                End If
                StrQ &= "            WHERE prtranhead_master.trnheadtype_id = 1 AND ISNULL(prpayrollprocess_tran.isvoid, 0) = 0 " & _
                             "            AND ISNULL(prtranhead_master.isvoid, 0) = 0  AND ISNULL(prtnaleave_tran.isvoid, 0) = 0 " & _
                             "            AND cfcommon_period_tran.isactive = 1 AND cfcommon_period_tran.periodunkid = @PeriodId " & _
                             "            AND prpayment_tran.periodunkid= @PeriodId  "


                If xUACFiltrQry.Trim.Length > 0 Then
                    StrQ &= " AND " & xUACFiltrQry
                End If


                StrQ &= "            GROUP BY prpayrollprocess_tran.employeeunkid "

                If mintViewIndex > 0 Then
                    StrQ &= mstrAnalysis_Fields.Replace("AS Id", "").Replace("AS GName", "")
                End If

                StrQ &= "          UNION ALL " & _
                             "          SELECT   prpayrollprocess_tran.employeeunkid AS Empid " & _
                             ",         " & mdecConversionRate & " * SUM(CAST(ISNULL(prpayrollprocess_tran.amount, 0) AS DECIMAL(36," & decDecimalPlaces & "))) AS GrossPay "

                If mintViewIndex > 0 Then
                    StrQ &= mstrAnalysis_Fields
                Else
                    StrQ &= ", 0 AS Id, '' AS GName "
                End If

                StrQ &= "          FROM prpayrollprocess_tran " & _
                             "          JOIN prtranhead_master ON prpayrollprocess_tran.tranheadunkid = prtranhead_master.tranheadunkid " & _
                             "          JOIN prtnaleave_tran ON prtnaleave_tran.tnaleavetranunkid = prpayrollprocess_tran.tnaleavetranunkid " & _
                            "           JOIN cfcommon_period_tran ON prtnaleave_tran.payperiodunkid = cfcommon_period_tran.periodunkid " & _
                            "           JOIN hremployee_master ON hremployee_master.employeeunkid = prpayrollprocess_tran.employeeunkid " & _
                            "           LEFT JOIN prpayment_tran ON prpayment_tran.employeeunkid = prpayrollprocess_tran.employeeunkid  AND prpayment_tran.isvoid = 0 " & _
                            "           AND prpayment_tran.periodunkid = prtnaleave_tran.payperiodunkid "

                StrQ &= " AND prpayment_tran.referenceid = " & clsPayment_tran.enPaymentRefId.PAYSLIP & " " & _
                            " AND prpayment_tran.paytypeid = " & clsPayment_tran.enPayTypeId.PAYMENT & " "

                StrQ &= mstrAnalysis_Join
                If xUACQry.Trim.Length > 0 Then
                    StrQ &= xUACQry
                End If

                StrQ &= "           WHERE prtranhead_master.trnheadtype_id = 1 AND ISNULL(prpayrollprocess_tran.isvoid, 0) = 0 " & _
                             "           AND ISNULL(prtranhead_master.isvoid, 0) = 0 AND ISNULL(prtnaleave_tran.isvoid, 0) = 0 " & _
                             "           AND cfcommon_period_tran.isactive = 1 AND cfcommon_period_tran.periodunkid = @PeriodId " & _
                             "           AND prpayment_tran.paymenttranunkid IS NULL  "


                If xUACFiltrQry.Trim.Length > 0 Then
                    StrQ &= " AND " & xUACFiltrQry
                End If

                StrQ &= "           GROUP BY prpayrollprocess_tran.employeeunkid "

                If mintViewIndex > 0 Then
                    StrQ &= mstrAnalysis_Fields.Replace("AS Id", "").Replace("AS GName", "")
                End If

            End If

            StrQ &= ") AS G ON G.Empid = A.Empid "
            dsBasGross = objDataOperation.ExecQuery(StrQ, "DataTable")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & " " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            Dim mdtData As DataTable = New DataView(dsList.Tables(0), "", "employeecode, Amount DESC", DataViewRowState.CurrentRows).ToTable

            Dim StrECode As String = String.Empty
            Dim StrPrevECode As String = String.Empty
            Dim rpt_Row As DataRow = Nothing

            rpt_Data = New ArutiReport.Designer.dsArutiReport


            Dim decColumn4Total, decColumn81Total As Decimal
            decColumn4Total = 0 : decColumn81Total = 0
            Dim iCnt As Integer = 1

            For Each dtRow As DataRow In dsList.Tables("DataTable").Rows
                StrECode = dtRow.Item("employeecode").ToString.Trim

                If StrPrevECode <> StrECode Then
                    If mblnIgnoreZero = True Then
                        If CDec(dtRow.Item("Amount")) = 0 Then Continue For
                    End If
                    rpt_Row = rpt_Data.Tables("ArutiTable").NewRow
                    iCnt += 1
                Else
                    rpt_Row = rpt_Data.Tables("ArutiTable").Rows(rpt_Data.Tables("ArutiTable").Rows.Count - 1)
                    rpt_Row.AcceptChanges()
                    StrPrevECode = StrECode
                    Continue For
                End If
                If dtRow.Item("MemshipNo").ToString.Trim = "0" Then
                    rpt_Row.Item("Column3") = ""
                Else
                    rpt_Row.Item("Column3") = dtRow.Item("MemshipNo")
                End If
                rpt_Row.Item("Column1") = dtRow.Item("employeecode").ToString
                rpt_Row.Item("Column7") = dtRow.Item("firstname").ToString
                rpt_Row.Item("Column5") = dtRow.Item("middlename").ToString
                rpt_Row.Item("Column6") = dtRow.Item("surname").ToString

                rpt_Data.Tables("ArutiTable").Rows.Add(rpt_Row)
                StrPrevECode = StrECode
                Dim dtTemp() As DataRow = dsBasGross.Tables("DataTable").Select("Empid = '" & dtRow.Item("Empid") & "'")
                If dtTemp.Length > 0 Then
                    If mblnShowBasicSalary = True Then
                        rpt_Row.Item("Column4") = Format(CDec(dtTemp(0).Item("BasicSal")), GUI.fmtCurrency)
                        rpt_Row.Item("Column82") = Format(CDec(dtTemp(0).Item("BasicSal")), GUI.fmtCurrency)
                        decColumn4Total = decColumn4Total + CDec(dtTemp(0).Item("BasicSal"))
                    Else
                        rpt_Row.Item("Column4") = Format(CDec(dtTemp(0).Item("GrossPay")), GUI.fmtCurrency)
                        rpt_Row.Item("Column82") = Format(CDec(dtTemp(0).Item("GrossPay")), GUI.fmtCurrency)
                        decColumn4Total = decColumn4Total + CDec(dtTemp(0).Item("GrossPay"))
                    End If
                End If
                rpt_Row.Item("Column8") = Format(CDec(dtRow.Item("Amount")), GUI.fmtCurrency)
                rpt_Row.Item("Column81") = Format(CDec(dtRow.Item("Amount")), GUI.fmtCurrency)
                decColumn81Total = decColumn81Total + CDec(dtRow.Item("Amount"))
                iCnt += 1
            Next

            mstrAddress = Company._Object._Address1 & " " & Company._Object._Address2
            mstrAddress &= " " & Company._Object._City_Name & " " & Company._Object._State_Name & " " & Company._Object._Post_Code_No & " " & Company._Object._Country_Name

            objRpt = New ArutiReport.Designer.rptNHIFReportTZ
            objRpt.SetDataSource(rpt_Data)
            objRpt.DataDefinition.FormulaFields("frmlAddress").Text = "'" & Company._Object._Name & "<BR>" & mstrAddress & "'"
            Call ReportFunction.TextChange(objRpt, "txtReportName", Me._ReportName)
            Call ReportFunction.TextChange(objRpt, "txtAddress", mstrAddress)
            Call ReportFunction.TextChange(objRpt, "txtNHIFNo", Language.getMessage(mstrModuleName, 1, "Membership No"))
            Call ReportFunction.TextChange(objRpt, "txtEmpCode", Language.getMessage(mstrModuleName, 2, "PF Number"))
            Call ReportFunction.TextChange(objRpt, "txtChequeNo", Language.getMessage(mstrModuleName, 3, "Cheque No"))
            Call ReportFunction.TextChange(objRpt, "txtFirstName", Language.getMessage(mstrModuleName, 4, "First Name"))
            Call ReportFunction.TextChange(objRpt, "txtMiddleName", Language.getMessage(mstrModuleName, 5, "Middle Name"))
            Call ReportFunction.TextChange(objRpt, "txtLastName", Language.getMessage(mstrModuleName, 6, "Last Name"))
            Call ReportFunction.TextChange(objRpt, "txtBasicSalary", Language.getMessage(mstrModuleName, 7, "Basic Salary"))
            Call ReportFunction.TextChange(objRpt, "txtAmount", Language.getMessage(mstrModuleName, 8, "Amount Contributed"))
            Call ReportFunction.TextChange(objRpt, "txtTotalBasicSalary", Format(decColumn4Total, GUI.fmtCurrency))
            Call ReportFunction.TextChange(objRpt, "txtTotalAmount", Format(decColumn81Total, GUI.fmtCurrency))
            Call ReportFunction.TextChange(objRpt, "txtTotal", Language.getMessage(mstrModuleName, 9, "Total"))


            Dim intColIndex As Integer = -1
            If menExportAction = enExportAction.ExcelExtra Then
                mdtTableExcel = rpt_Data.Tables(0)
                mdtTableExcel.Columns("Column3").Caption = Language.getMessage(mstrModuleName, 1, "Membership No")

                intColIndex += 1
                mdtTableExcel.Columns("Column3").SetOrdinal(intColIndex)

                mdtTableExcel.Columns("Column1").Caption = Language.getMessage(mstrModuleName, 2, "PF Number")
                intColIndex += 1
                mdtTableExcel.Columns("Column1").SetOrdinal(intColIndex)

                mdtTableExcel.Columns("Column9").Caption = Language.getMessage(mstrModuleName, 3, "Cheque No")
                intColIndex += 1
                mdtTableExcel.Columns("Column9").SetOrdinal(intColIndex)


                mdtTableExcel.Columns("Column7").Caption = Language.getMessage(mstrModuleName, 4, "First Name")
                intColIndex += 1
                mdtTableExcel.Columns("Column7").SetOrdinal(intColIndex)

                mdtTableExcel.Columns("Column5").Caption = Language.getMessage(mstrModuleName, 5, "Middle Name")
                intColIndex += 1
                mdtTableExcel.Columns("Column5").SetOrdinal(intColIndex)

                mdtTableExcel.Columns("Column6").Caption = Language.getMessage(mstrModuleName, 6, "Last Name")
                intColIndex += 1
                mdtTableExcel.Columns("Column6").SetOrdinal(intColIndex)

                mdtTableExcel.Columns("Column82").Caption = Language.getMessage(mstrModuleName, 7, "Basic Salary")
                intColIndex += 1
                mdtTableExcel.Columns("Column82").SetOrdinal(intColIndex)

                mdtTableExcel.Columns("Column81").Caption = Language.getMessage(mstrModuleName, 8, "Amount Contributed")
                intColIndex += 1
                mdtTableExcel.Columns("Column81").SetOrdinal(intColIndex)

                For i = intColIndex + 1 To mdtTableExcel.Columns.Count - 1
                    mdtTableExcel.Columns.RemoveAt(intColIndex + 1)
                Next

            End If

            Return objRpt

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: Generate_DetailReport; Module Name: " & mstrModuleName)
            Return Nothing
        End Try
    End Function

    'Sohail (10 Jul 2019) -- Start
    'Good Neighbours Enhancement - Support Issue Id # 3875 - 76.1 - Put link for NHIF EFT and it should generate report without report header of report name address and company name and add "EmployerNo" column..
    Public Function Generate_EFT_NHIF_Report(ByVal strDatabaseName As String _
                                             , ByVal intUserUnkid As Integer _
                                             , ByVal intYearUnkid As Integer _
                                             , ByVal intCompanyUnkid As Integer _
                                             , ByVal dtPeriodEnd As Date _
                                             , ByVal strUserModeSetting As String _
                                             , ByVal blnOnlyApproved As Boolean _
                                             , ByVal intBaseCurrencyId As Integer _
                                             , ByVal strExportPath As String _
                                             , ByVal xOpenReportAfterExport As Boolean _
                                             ) As Boolean
        Dim StrQ As String = ""
        Dim dsList As New DataSet
        Dim dsBasGross As New DataSet
        Dim exForce As Exception
        Dim rpt_Data As ArutiReport.Designer.dsArutiReport
        Try
            objDataOperation = New clsDataOperation

            Dim objExchangeRate As New clsExchangeRate
            Dim decDecimalPlaces As Decimal = 0

            objExchangeRate._ExchangeRateunkid = intBaseCurrencyId
            decDecimalPlaces = objExchangeRate._Digits_After_Decimal
            Dim strENameQ As String = ""
            If mblnFirstNamethenSurname = True Then
                strENameQ &= ", ISNULL(hremployee_master.firstname,'')+' '+ISNULL(hremployee_master.othername,'')+' '+ISNULL(hremployee_master.surname,'') AS EName " & _
                             ", ISNULL(hremployee_master.firstname,'') AS firstname " & _
                             ", ISNULL(hremployee_master.othername,'') AS middlename " & _
                             ", ISNULL(hremployee_master.surname,'') AS surname "
            Else
                strENameQ &= ", ISNULL(hremployee_master.surname,'')+' '+ISNULL(hremployee_master.firstname,'')+' '+ISNULL(hremployee_master.othername,'') AS EName " & _
                             ", ISNULL(hremployee_master.firstname,'') AS firstname " & _
                             ", ISNULL(hremployee_master.othername,'') AS middlename " & _
                             ", ISNULL(hremployee_master.surname,'') AS surname "
            End If
            Dim xUACQry, xUACFiltrQry As String : xUACQry = "" : xUACFiltrQry = ""
            Call NewAccessLevelFilterString(xUACQry, xUACFiltrQry, dtPeriodEnd, blnOnlyApproved, strDatabaseName, intUserUnkid, intCompanyUnkid, intYearUnkid, strUserModeSetting)

            If mintBaseCurrId = mintPaidCurrencyId Then

                StrQ = "SELECT hremployee_master.employeecode AS employeecode " & _
                             strENameQ & _
                             ", ISNULL(hremployee_meminfo_tran.membershipno,'') AS MemshipNo " & _
                             ", ISNULL(hrmembership_master.employer_membershipno, '') AS EmployerMemshipNo " & _
                             ", ISNULL(CAST(prpayrollprocess_tran.amount AS DECIMAL(36, " & decDecimalPlaces & ")),0) AS empcontribution " & _
                             ", ISNULL(CAmount,0) AS employercontribution " & _
                             ", ISNULL(CAST(prpayrollprocess_tran.amount AS DECIMAL(36, " & decDecimalPlaces & ")),0)+ISNULL(CAmount,0) AS Amount " & _
                             ", ISNULL(cfcommon_period_tran.period_name,'') AS PeriodName " & _
                             ", hremployee_master.employeeunkid AS EmpId " & _
                             ", ISNULL(cfcommon_master.name, '') AS MemshipCategory " & _
                             ", '' AS OtherMembership " & _
                             ", '' AS Remarks " & _
                             ", 0 As NonPayrollMembership " & _
                       "FROM    prpayrollprocess_tran " & _
                               "JOIN hremployee_master ON prpayrollprocess_tran.employeeunkid = hremployee_master.employeeunkid " & _
                               "JOIN hremployee_meminfo_tran ON prpayrollprocess_tran.membershiptranunkid = hremployee_meminfo_tran.membershiptranunkid " & _
                                                "AND hremployee_meminfo_tran.employeeunkid = hremployee_master.employeeunkid " & _
                                                "AND ISNULL(hremployee_meminfo_tran.isdeleted,0) = 0" & _
                               "LEFT JOIN hrmembership_master ON hremployee_meminfo_tran.membershipunkid = hrmembership_master.membershipunkid " & _
                                                "AND prpayrollprocess_tran.tranheadunkid = hrmembership_master.emptranheadunkid " & _
                               "JOIN prtnaleave_tran ON prpayrollprocess_tran.tnaleavetranunkid = prtnaleave_tran.tnaleavetranunkid " & _
                               "LEFT JOIN cfcommon_period_tran ON prtnaleave_tran.payperiodunkid = cfcommon_period_tran.periodunkid " & _
                                                "AND ISNULL(cfcommon_period_tran.isactive,0) = 1 " & _
                               "LEFT JOIN cfcommon_master ON cfcommon_master.masterunkid = hrmembership_master.membershipcategoryunkid " & _
                                                "AND cfcommon_master.mastertype = " & clsCommon_Master.enCommonMaster.MEMBERSHIP_CATEGORY & " "
                'Sohail (10 Jul 2019) - [EmployerMemshipNo]

                StrQ &= mstrAnalysis_Join
                If xUACQry.Trim.Length > 0 Then
                    StrQ &= xUACQry
                End If
                StrQ &= "       LEFT JOIN ( SELECT  hremployee_master.employeeunkid AS EmpId " & _
                                                 ", ISNULL(CAST(prpayrollprocess_tran.amount AS DECIMAL(36, " & decDecimalPlaces & ")),0) AS CAmount " & _
                                           "FROM    prpayrollprocess_tran " & _
                                                   "JOIN hremployee_master ON prpayrollprocess_tran.employeeunkid = hremployee_master.employeeunkid " & _
                                                   "JOIN hremployee_meminfo_tran ON prpayrollprocess_tran.membershiptranunkid = hremployee_meminfo_tran.membershiptranunkid " & _
                                                                    "AND hremployee_meminfo_tran.employeeunkid = hremployee_master.employeeunkid " & _
                                                                    "AND ISNULL(hremployee_meminfo_tran.isdeleted,0) = 0 " & _
                                                   "LEFT JOIN hrmembership_master ON hremployee_meminfo_tran.membershipunkid = hrmembership_master.membershipunkid " & _
                                                                    "AND prpayrollprocess_tran.tranheadunkid = hrmembership_master.cotranheadunkid " & _
                                                   "JOIN prtnaleave_tran ON prpayrollprocess_tran.tnaleavetranunkid = prtnaleave_tran.tnaleavetranunkid " & _
                                                   "LEFT JOIN cfcommon_period_tran ON prtnaleave_tran.payperiodunkid = cfcommon_period_tran.periodunkid " & _
                                                                    "AND ISNULL(cfcommon_period_tran.isactive,0) = 0 "

                StrQ &= mstrAnalysis_Join
                If xUACQry.Trim.Length > 0 Then
                    StrQ &= xUACQry
                End If

                StrQ &= "                   WHERE ISNULL(prpayrollprocess_tran.isvoid,0) = 0 " & _
                                           "AND ISNULL(prtnaleave_tran.isvoid,0) = 0 " & _
                                           "AND prtnaleave_tran.payperiodunkid = @PeriodId " & _
                                           "AND hrmembership_master.membershipunkid = @MemId "

                If xUACFiltrQry.Trim.Length > 0 Then
                    StrQ &= " AND " & xUACFiltrQry
                End If
                StrQ &= "           ) AS C ON C.EmpId = hremployee_master.employeeunkid " & _
                                        "WHERE ISNULL(prpayrollprocess_tran.isvoid,0) = 0 " & _
                                        "AND ISNULL(prtnaleave_tran.isvoid, 0) = 0 " & _
                                        "AND prtnaleave_tran.payperiodunkid = @PeriodId " & _
                                        "AND hrmembership_master.membershipunkid = @MemId "

                If mstrAdvance_Filter.Trim.Length > 0 Then
                    StrQ &= " AND " & mstrAdvance_Filter
                End If

                If xUACFiltrQry.Trim.Length > 0 Then
                    StrQ &= " AND " & xUACFiltrQry
                End If


                If mintNonPayrollMembershipId > 0 Then

                    StrQ &= "UNION ALL " & _
                                "SELECT hremployee_master.employeecode AS employeecode " & _
                                      strENameQ & _
                                      ",'' AS MemshipNo " & _
                                      ", '' AS EmployerMemshipNo " & _
                                      ", 0 AS empcontribution " & _
                                      ", 0 AS employercontribution " & _
                                      ", 0 AS Amount " & _
                                      ", '' AS PeriodName " & _
                                      ", hremployee_master.employeeunkid AS EmpId " & _
                                      ", '' AS MemshipCategory " & _
                                      ",ISNULL(hremployee_meminfo_tran.membershipno,'') AS OtherMembership " & _
                                      ", '' AS Remarks " & _
                                      ", 1 AS NonPayrollMembership  " & _
                                      "FROM    hremployee_master " & _
                                        "JOIN hremployee_meminfo_tran ON hremployee_meminfo_tran.employeeunkid = hremployee_master.employeeunkid " & _
                                                        "AND ISNULL(hremployee_meminfo_tran.isdeleted,0) = 0 " & _
                                        "LEFT JOIN hrmembership_master ON hremployee_meminfo_tran.membershipunkid = hrmembership_master.membershipunkid "
                    'Sohail (10 Jul 2019) - [EmployerMemshipNo]

                    StrQ &= mstrAnalysis_Join

                    If xUACQry.Trim.Length > 0 Then
                        StrQ &= xUACQry
                    End If

                    StrQ &= "WHERE   hrmembership_master.membershipunkid IN ( " & mintNonPayrollMembershipId.ToString & " ) "

                    If mstrAdvance_Filter.Trim.Length > 0 Then
                        StrQ &= " AND " & mstrAdvance_Filter
                    End If

                    If xUACFiltrQry.Trim.Length > 0 Then
                        StrQ &= " AND " & xUACFiltrQry
                    End If

                End If

                StrQ &= "ORDER BY employeecode "

            ElseIf mintBaseCurrId <> mintPaidCurrencyId Then

                StrQ = "SELECT hremployee_master.employeecode AS employeecode " & _
                             strENameQ & _
                             ", ISNULL(hremployee_meminfo_tran.membershipno,'') AS MemshipNo " & _
                             ", ISNULL(hrmembership_master.employer_membershipno, '') AS EmployerMemshipNo " & _
                             ", ( prpayment_tran.expaidrate / prpayment_tran.baseexchangerate ) *  ISNULL(CAST(prpayrollprocess_tran.amount AS DECIMAL(36, " & decDecimalPlaces & ")),0) AS empcontribution " & _
                             ", ( prpayment_tran.expaidrate / prpayment_tran.baseexchangerate ) *  ISNULL(CAmount,0) AS employercontribution " & _
                             ", ( prpayment_tran.expaidrate / prpayment_tran.baseexchangerate ) *  (ISNULL(CAST(prpayrollprocess_tran.amount AS DECIMAL(36, " & decDecimalPlaces & ")),0) +  ISNULL(CAmount,0)) AS Amount " & _
                             ", ISNULL(cfcommon_period_tran.period_name,'') AS PeriodName " & _
                             ", hremployee_master.employeeunkid AS EmpId " & _
                             ", ISNULL(cfcommon_master.name, '') AS MemshipCategory " & _
                             ", '' AS OtherMembership " & _
                             ", '' AS Remarks " & _
                              ", 0 As NonPayrollMembership " & _
                       "FROM    prpayrollprocess_tran " & _
                               "JOIN hremployee_master ON prpayrollprocess_tran.employeeunkid = hremployee_master.employeeunkid " & _
                               "JOIN hremployee_meminfo_tran ON prpayrollprocess_tran.membershiptranunkid = hremployee_meminfo_tran.membershiptranunkid " & _
                                                "AND hremployee_meminfo_tran.employeeunkid = hremployee_master.employeeunkid " & _
                                                "AND ISNULL(hremployee_meminfo_tran.isdeleted,0) = 0" & _
                               "LEFT JOIN hrmembership_master ON hremployee_meminfo_tran.membershipunkid = hrmembership_master.membershipunkid " & _
                                                "AND prpayrollprocess_tran.tranheadunkid = hrmembership_master.emptranheadunkid " & _
                               "JOIN prtnaleave_tran ON prpayrollprocess_tran.tnaleavetranunkid = prtnaleave_tran.tnaleavetranunkid " & _
                               "LEFT JOIN cfcommon_period_tran ON prtnaleave_tran.payperiodunkid = cfcommon_period_tran.periodunkid " & _
                                                "AND ISNULL(cfcommon_period_tran.isactive,0) = 1 " & _
                               "LEFT JOIN cfcommon_master ON cfcommon_master.masterunkid = hrmembership_master.membershipcategoryunkid " & _
                                                "AND cfcommon_master.mastertype = " & clsCommon_Master.enCommonMaster.MEMBERSHIP_CATEGORY & " " & _
                               "JOIN prpayment_tran ON prpayment_tran.periodunkid = prtnaleave_tran.payperiodunkid " & _
                                                "AND prtnaleave_tran.employeeunkid = prpayment_tran.employeeunkid " & _
                                                "AND ISNULL(prpayment_tran.isvoid, 0) = 0 " & _
                                                "AND prpayment_tran.countryunkid = " & mintCountryId & " "
                'Sohail (10 Jul 2019) - [EmployerMemshipNo]

                StrQ &= " AND prpayment_tran.referenceid = " & clsPayment_tran.enPaymentRefId.PAYSLIP & " " & _
                            " AND prpayment_tran.paytypeid = " & clsPayment_tran.enPayTypeId.PAYMENT & " "



                StrQ &= mstrAnalysis_Join

                If xUACQry.Trim.Length > 0 Then
                    StrQ &= xUACQry
                End If
                StrQ &= "   LEFT JOIN ( SELECT  hremployee_master.employeeunkid AS EmpId " & _
                                             ", ISNULL(CAST(prpayrollprocess_tran.amount AS DECIMAL(36, " & decDecimalPlaces & ")),0) AS CAmount " & _
                                       "FROM    prpayrollprocess_tran " & _
                                               "JOIN hremployee_master ON prpayrollprocess_tran.employeeunkid = hremployee_master.employeeunkid " & _
                                               "JOIN hremployee_meminfo_tran ON prpayrollprocess_tran.membershiptranunkid = hremployee_meminfo_tran.membershiptranunkid " & _
                                                                "AND hremployee_meminfo_tran.employeeunkid = hremployee_master.employeeunkid " & _
                                                                "AND ISNULL(hremployee_meminfo_tran.isdeleted,0) = 0 " & _
                                               "LEFT JOIN hrmembership_master ON hremployee_meminfo_tran.membershipunkid = hrmembership_master.membershipunkid " & _
                                                                "AND prpayrollprocess_tran.tranheadunkid = hrmembership_master.cotranheadunkid " & _
                                               "JOIN prtnaleave_tran ON prpayrollprocess_tran.tnaleavetranunkid = prtnaleave_tran.tnaleavetranunkid " & _
                                               "LEFT JOIN cfcommon_period_tran ON prtnaleave_tran.payperiodunkid = cfcommon_period_tran.periodunkid " & _
                                                                "AND ISNULL(cfcommon_period_tran.isactive,0) = 0 "

                StrQ &= mstrAnalysis_Join

                If xUACQry.Trim.Length > 0 Then
                    StrQ &= xUACQry
                End If
                StrQ &= "               WHERE   ISNULL(prpayrollprocess_tran.isvoid,0) = 0 " & _
                                               "AND ISNULL(prtnaleave_tran.isvoid,0) = 0 " & _
                                               "AND prtnaleave_tran.payperiodunkid = @PeriodId " & _
                                               "AND hrmembership_master.membershipunkid = @MemId "

                If xUACFiltrQry.Trim.Length > 0 Then
                    StrQ &= " AND " & xUACFiltrQry
                End If
                StrQ &= "              ) AS C ON C.EmpId = hremployee_master.employeeunkid " & _
                                    "WHERE   ISNULL(prpayrollprocess_tran.isvoid,0) = 0 " & _
                                    "AND ISNULL(prtnaleave_tran.isvoid, 0) = 0 " & _
                                    "AND prtnaleave_tran.payperiodunkid = @PeriodId " & _
                                    "AND hrmembership_master.membershipunkid = @MemId " & _
                                    "AND prpayment_tran.periodunkid = @PeriodId "

                If mstrAdvance_Filter.Trim.Length > 0 Then
                    StrQ &= " AND " & mstrAdvance_Filter
                End If

                If xUACFiltrQry.Trim.Length > 0 Then
                    StrQ &= " AND " & xUACFiltrQry
                End If

                StrQ &= "UNION ALL " & _
                        "SELECT hremployee_master.employeecode AS employeecode " & _
                              strENameQ & _
                              ", ISNULL(hremployee_meminfo_tran.membershipno,'') AS MemshipNo " & _
                              ", ISNULL(hrmembership_master.employer_membershipno, '') AS EmployerMemshipNo " & _
                              ", " & mdecConversionRate & " *  ISNULL(CAST(prpayrollprocess_tran.amount AS DECIMAL(36, " & decDecimalPlaces & ")),0) AS empcontribution " & _
                              ", " & mdecConversionRate & " *  ISNULL(CAmount,0) AS employercontribution " & _
                              ", " & mdecConversionRate & " *  (ISNULL(CAST(prpayrollprocess_tran.amount AS DECIMAL(36, " & decDecimalPlaces & ")),0)+  ISNULL(CAmount,0)) AS Amount " & _
                              ", ISNULL(cfcommon_period_tran.period_name,'') AS PeriodName " & _
                              ", hremployee_master.employeeunkid AS EmpId " & _
                              ", ISNULL(cfcommon_master.name, '') AS MemshipCategory " & _
                              ", '' AS OtherMembership " & _
                              ", '' AS Remarks " & _
                               ", 0 As NonPayrollMembership " & _
                             "FROM    prpayrollprocess_tran " & _
                                "JOIN hremployee_master ON prpayrollprocess_tran.employeeunkid = hremployee_master.employeeunkid " & _
                                "JOIN hremployee_meminfo_tran ON prpayrollprocess_tran.membershiptranunkid = hremployee_meminfo_tran.membershiptranunkid " & _
                                                "AND hremployee_meminfo_tran.employeeunkid = hremployee_master.employeeunkid " & _
                                                "AND ISNULL(hremployee_meminfo_tran.isdeleted,0) = 0" & _
                                "LEFT JOIN hrmembership_master ON hremployee_meminfo_tran.membershipunkid = hrmembership_master.membershipunkid " & _
                                "AND prpayrollprocess_tran.tranheadunkid = hrmembership_master.emptranheadunkid " & _
                                "JOIN prtnaleave_tran ON prpayrollprocess_tran.tnaleavetranunkid = prtnaleave_tran.tnaleavetranunkid " & _
                                "LEFT JOIN cfcommon_period_tran ON prtnaleave_tran.payperiodunkid = cfcommon_period_tran.periodunkid " & _
                                                "AND ISNULL(cfcommon_period_tran.isactive,0) = 1 " & _
                                "LEFT JOIN cfcommon_master ON cfcommon_master.masterunkid = hrmembership_master.membershipcategoryunkid " & _
                                                "AND cfcommon_master.mastertype = " & clsCommon_Master.enCommonMaster.MEMBERSHIP_CATEGORY & " " & _
                                "JOIN prpayment_tran ON prpayment_tran.periodunkid = prtnaleave_tran.payperiodunkid " & _
                                                "AND prtnaleave_tran.employeeunkid = prpayment_tran.employeeunkid " & _
                                                "AND ISNULL(prpayment_tran.isvoid, 0) = 0 AND prpayment_tran.countryunkid <> " & mintCountryId & " "
                'Sohail (10 Jul 2019) - [EmployerMemshipNo]
                StrQ &= " AND prpayment_tran.referenceid = " & clsPayment_tran.enPaymentRefId.PAYSLIP & " " & _
                            " AND prpayment_tran.paytypeid = " & clsPayment_tran.enPayTypeId.PAYMENT & " "

                StrQ &= mstrAnalysis_Join
                If xUACQry.Trim.Length > 0 Then
                    StrQ &= xUACQry
                End If
                StrQ &= "        LEFT JOIN ( SELECT  hremployee_master.employeeunkid AS EmpId " & _
                                                  ", ISNULL(CAST(prpayrollprocess_tran.amount AS DECIMAL(36, " & decDecimalPlaces & ")),0) AS CAmount " & _
                                            "FROM    prpayrollprocess_tran " & _
                                                     "JOIN hremployee_master ON prpayrollprocess_tran.employeeunkid = hremployee_master.employeeunkid " & _
                                                     "JOIN hremployee_meminfo_tran ON prpayrollprocess_tran.membershiptranunkid = hremployee_meminfo_tran.membershiptranunkid " & _
                                                                    "AND hremployee_meminfo_tran.employeeunkid = hremployee_master.employeeunkid " & _
                                                                    "AND ISNULL(hremployee_meminfo_tran.isdeleted,0) = 0 " & _
                                                     "LEFT JOIN hrmembership_master ON hremployee_meminfo_tran.membershipunkid = hrmembership_master.membershipunkid " & _
                                                                    "AND prpayrollprocess_tran.tranheadunkid = hrmembership_master.cotranheadunkid " & _
                                                     "JOIN prtnaleave_tran ON prpayrollprocess_tran.tnaleavetranunkid = prtnaleave_tran.tnaleavetranunkid " & _
                                                     "LEFT JOIN cfcommon_period_tran ON prtnaleave_tran.payperiodunkid = cfcommon_period_tran.periodunkid " & _
                                                                    "AND ISNULL(cfcommon_period_tran.isactive,0) = 0 "

                StrQ &= mstrAnalysis_Join

                If xUACQry.Trim.Length > 0 Then
                    StrQ &= xUACQry
                End If


                StrQ &= "                    WHERE   ISNULL(prpayrollprocess_tran.isvoid,0) = 0 " & _
                                            "AND ISNULL(prtnaleave_tran.isvoid,0) = 0 " & _
                                            "AND prtnaleave_tran.payperiodunkid = @PeriodId " & _
                                            "AND hrmembership_master.membershipunkid = @MemId "
                If xUACFiltrQry.Trim.Length > 0 Then
                    StrQ &= " AND " & xUACFiltrQry
                End If


                StrQ &= "            ) AS C ON C.EmpId = hremployee_master.employeeunkid " & _
                                         "WHERE   ISNULL(prpayrollprocess_tran.isvoid,0) = 0 " & _
                                         "AND ISNULL(prtnaleave_tran.isvoid, 0) = 0 " & _
                                         "AND prtnaleave_tran.payperiodunkid = @PeriodId " & _
                                         "AND hrmembership_master.membershipunkid = @MemId " & _
                                         "AND prpayment_tran.periodunkid = @PeriodId "

                If mstrAdvance_Filter.Trim.Length > 0 Then
                    StrQ &= " AND " & mstrAdvance_Filter
                End If

                If xUACFiltrQry.Trim.Length > 0 Then
                    StrQ &= " AND " & xUACFiltrQry
                End If

                StrQ &= "UNION ALL " & _
                        "SELECT hremployee_master.employeecode AS employeecode " & _
                              strENameQ & _
                              ", ISNULL(hremployee_meminfo_tran.membershipno,'') AS MemshipNo " & _
                              ", ISNULL(hrmembership_master.employer_membershipno, '') AS EmployerMemshipNo " & _
                              ", " & mdecConversionRate & " *  ISNULL(CAST(prpayrollprocess_tran.amount AS DECIMAL(36, " & decDecimalPlaces & ")),0) AS empcontribution " & _
                              ", " & mdecConversionRate & " *  ISNULL(CAmount,0) AS employercontribution " & _
                              ", " & mdecConversionRate & " *  (ISNULL(CAST(prpayrollprocess_tran.amount AS DECIMAL(36, " & decDecimalPlaces & ")),0) +   ISNULL(CAmount,0))  AS Amount " & _
                              ", ISNULL(cfcommon_period_tran.period_name,'') AS PeriodName " & _
                              ", hremployee_master.employeeunkid AS EmpId " & _
                              ", ISNULL(cfcommon_master.name, '') AS MemshipCategory " & _
                              ", '' AS OtherMembership " & _
                              ", '' AS Remarks " & _
                               ", 0 As NonPayrollMembership " & _
                        "FROM    prpayrollprocess_tran " & _
                                "JOIN hremployee_master ON prpayrollprocess_tran.employeeunkid = hremployee_master.employeeunkid " & _
                                "JOIN hremployee_meminfo_tran ON prpayrollprocess_tran.membershiptranunkid = hremployee_meminfo_tran.membershiptranunkid " & _
                                                "AND hremployee_meminfo_tran.employeeunkid = hremployee_master.employeeunkid " & _
                                                "AND ISNULL(hremployee_meminfo_tran.isdeleted,0) = 0" & _
                                "LEFT JOIN hrmembership_master ON hremployee_meminfo_tran.membershipunkid = hrmembership_master.membershipunkid " & _
                                                "AND prpayrollprocess_tran.tranheadunkid = hrmembership_master.emptranheadunkid " & _
                                "JOIN prtnaleave_tran ON prpayrollprocess_tran.tnaleavetranunkid = prtnaleave_tran.tnaleavetranunkid " & _
                                "LEFT JOIN cfcommon_period_tran ON prtnaleave_tran.payperiodunkid = cfcommon_period_tran.periodunkid " & _
                                                "AND ISNULL(cfcommon_period_tran.isactive,0) = 1 " & _
                                "LEFT JOIN cfcommon_master ON cfcommon_master.masterunkid = hrmembership_master.membershipcategoryunkid " & _
                                                "AND cfcommon_master.mastertype = " & clsCommon_Master.enCommonMaster.MEMBERSHIP_CATEGORY & " " & _
                                "LEFT JOIN prpayment_tran ON prpayment_tran.periodunkid = prtnaleave_tran.payperiodunkid " & _
                                                "AND prtnaleave_tran.employeeunkid = prpayment_tran.employeeunkid " & _
                                                "AND ISNULL(prpayment_tran.isvoid, 0) = 0 "
                'Sohail (10 Jul 2019) - [EmployerMemshipNo]
                StrQ &= " AND prpayment_tran.referenceid = " & clsPayment_tran.enPaymentRefId.PAYSLIP & " " & _
                            " AND prpayment_tran.paytypeid = " & clsPayment_tran.enPayTypeId.PAYMENT & " "

                StrQ &= mstrAnalysis_Join
                If xUACQry.Trim.Length > 0 Then
                    StrQ &= xUACQry
                End If

                StrQ &= "        LEFT JOIN ( SELECT  hremployee_master.employeeunkid AS EmpId " & _
                                                  ", ISNULL(CAST(prpayrollprocess_tran.amount AS DECIMAL(36, " & decDecimalPlaces & ")),0) AS CAmount " & _
                                            "FROM    prpayrollprocess_tran " & _
                                                    "JOIN hremployee_master ON prpayrollprocess_tran.employeeunkid = hremployee_master.employeeunkid " & _
                                                    "JOIN hremployee_meminfo_tran ON prpayrollprocess_tran.membershiptranunkid = hremployee_meminfo_tran.membershiptranunkid " & _
                                                                    "AND hremployee_meminfo_tran.employeeunkid = hremployee_master.employeeunkid " & _
                                                                    "AND ISNULL(hremployee_meminfo_tran.isdeleted,0) = 0 " & _
                                                    "LEFT JOIN hrmembership_master ON hremployee_meminfo_tran.membershipunkid = hrmembership_master.membershipunkid " & _
                                                                    "AND prpayrollprocess_tran.tranheadunkid = hrmembership_master.cotranheadunkid " & _
                                                    "JOIN prtnaleave_tran ON prpayrollprocess_tran.tnaleavetranunkid = prtnaleave_tran.tnaleavetranunkid " & _
                                                    "LEFT JOIN cfcommon_period_tran ON prtnaleave_tran.payperiodunkid = cfcommon_period_tran.periodunkid " & _
                                                                    "AND ISNULL(cfcommon_period_tran.isactive,0) = 0 "

                StrQ &= mstrAnalysis_Join
                If xUACQry.Trim.Length > 0 Then
                    StrQ &= xUACQry
                End If

                StrQ &= "                    WHERE   ISNULL(prpayrollprocess_tran.isvoid,0) = 0 " & _
                                                    "AND ISNULL(prtnaleave_tran.isvoid,0) = 0 " & _
                                                    "AND prtnaleave_tran.payperiodunkid = @PeriodId " & _
                                                    "AND hrmembership_master.membershipunkid = @MemId "



                If xUACFiltrQry.Trim.Length > 0 Then
                    StrQ &= " AND " & xUACFiltrQry
                End If


                StrQ &= "              ) AS C ON C.EmpId = hremployee_master.employeeunkid " & _
                                          " WHERE   ISNULL(prpayrollprocess_tran.isvoid,0) = 0 " & _
                                          " AND ISNULL(prtnaleave_tran.isvoid, 0) = 0 " & _
                                          " AND prtnaleave_tran.payperiodunkid = @PeriodId " & _
                                          " AND hrmembership_master.membershipunkid = @MemId " & _
                                          " AND prpayment_tran.periodunkid IS NULL "

                If mstrAdvance_Filter.Trim.Length > 0 Then
                    StrQ &= " AND " & mstrAdvance_Filter
                End If

                If xUACFiltrQry.Trim.Length > 0 Then
                    StrQ &= " AND " & xUACFiltrQry
                End If

                If mintNonPayrollMembershipId > 0 Then

                    StrQ &= "UNION ALL " & _
                                "SELECT hremployee_master.employeecode AS employeecode " & _
                                     strENameQ & _
                                      ",'' AS MemshipNo " & _
                                      ", '' AS EmployerMemshipNo " & _
                                      ", 0 AS empcontribution " & _
                                      ", 0 AS employercontribution " & _
                                      ", 0 AS Amount " & _
                                      ", '' AS PeriodName " & _
                                      ", hremployee_master.employeeunkid AS EmpId " & _
                                      ", '' AS MemshipCategory " & _
                                      ",ISNULL(hremployee_meminfo_tran.membershipno,'') AS OtherMembership " & _
                                      ", '' AS Remarks " & _
                                      ", 1 AS NonPayrollMembership  " & _
                                      "FROM    hremployee_master " & _
                                        "JOIN hremployee_meminfo_tran ON hremployee_meminfo_tran.employeeunkid = hremployee_master.employeeunkid " & _
                                                        "AND ISNULL(hremployee_meminfo_tran.isdeleted,0) = 0 " & _
                                        "LEFT JOIN hrmembership_master ON hremployee_meminfo_tran.membershipunkid = hrmembership_master.membershipunkid "
                    'Sohail (10 Jul 2019) - [EmployerMemshipNo]
                    StrQ &= mstrAnalysis_Join
                    If xUACQry.Trim.Length > 0 Then
                        StrQ &= xUACQry
                    End If
                    StrQ &= "WHERE   hrmembership_master.membershipunkid IN ( " & mintNonPayrollMembershipId.ToString & " ) "

                    If mstrAdvance_Filter.Trim.Length > 0 Then
                        StrQ &= " AND " & mstrAdvance_Filter
                    End If
                    If xUACFiltrQry.Trim.Length > 0 Then
                        StrQ &= " AND " & xUACFiltrQry
                    End If

                End If

                StrQ &= "ORDER BY employeecode "

            End If

            objDataOperation.AddParameter("@MemId", SqlDbType.Int, eZeeDataType.INT_SIZE, mintMembershipId)
            objDataOperation.AddParameter("@PeriodId", SqlDbType.Int, eZeeDataType.INT_SIZE, mintPeriodId)


            dsList = objDataOperation.ExecQuery(StrQ, "DataTable")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & " " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            If mintBaseCurrId = mintPaidCurrencyId Then

                StrQ = "SELECT " & _
                        "	 A.Empid " & _
                        "	,BasicSal " & _
                        "	,GrossPay " & _
                        "FROM " & _
                        "( " & _
                        "	SELECT " & _
                        "		 prpayrollprocess_tran.employeeunkid AS Empid " & _
                        "		,SUM(CAST(ISNULL(prpayrollprocess_tran.amount, 0) AS DECIMAL(36, " & decDecimalPlaces & "))) AS BasicSal "

                If mintViewIndex > 0 Then
                    StrQ &= mstrAnalysis_Fields
                Else
                    StrQ &= ", 0 AS Id, '' AS GName "
                End If

                StrQ &= "	FROM prpayrollprocess_tran " & _
                        "		JOIN prtranhead_master ON prpayrollprocess_tran.tranheadunkid = prtranhead_master.tranheadunkid " & _
                        "		JOIN prtnaleave_tran ON prtnaleave_tran.tnaleavetranunkid = prpayrollprocess_tran.tnaleavetranunkid " & _
                        "		JOIN cfcommon_period_tran ON prtnaleave_tran.payperiodunkid = cfcommon_period_tran.periodunkid " & _
                        "		JOIN hremployee_master ON hremployee_master.employeeunkid = prpayrollprocess_tran.employeeunkid "

                StrQ &= mstrAnalysis_Join
                If xUACQry.Trim.Length > 0 Then
                    StrQ &= xUACQry
                End If

                StrQ &= "WHERE    ISNULL(prpayrollprocess_tran.isvoid, 0) = 0 " & _
                    "		AND ISNULL(prtranhead_master.isvoid, 0) = 0 " & _
                    "		AND ISNULL(prtnaleave_tran.isvoid, 0) = 0 " & _
                                 "AND prpayrollprocess_tran.tranheadunkid > 0 " & _
                    "		AND cfcommon_period_tran.isactive = 1 " & _
                                 "AND cfcommon_period_tran.periodunkid = @PeriodId "

                'Sohail (22 Jul 2019) -- Start
                'Good Neighbours Enhancement # - 76.1 - Pick Basic Salary as Other Earning if option is ticked on head master in NHIF Report.
                'If mintOtherEarningTranId > 0 Then 'Basic Salary as Other Earning
                '    StrQ &= "AND prpayrollprocess_tran.tranheadunkid = @OtherEarningTranId  "
                'Else
                '    StrQ &= "AND prtranhead_master.typeof_id = " & enTypeOf.Salary & "  "
                'End If
                StrQ &= "AND ((prtranhead_master.typeof_id = " & enTypeOf.Salary & " and prtranhead_master.trnheadtype_id = " & enTranHeadType.EarningForEmployees & ") OR prtranhead_master.isbasicsalaryasotherearning  = 1) "
                'Sohail (22 Jul 2019) -- End

                If xUACFiltrQry.Trim.Length > 0 Then
                    StrQ &= " AND " & xUACFiltrQry
                End If

                StrQ &= "GROUP BY prpayrollprocess_tran.employeeunkid "
                If mintViewIndex > 0 Then
                    StrQ &= mstrAnalysis_Fields.Replace("AS Id", "").Replace("AS GName", "")
                End If

                StrQ &= ") AS A " & _
                        "JOIN " & _
                        "( " & _
                        "	SELECT " & _
                        "		 prpayrollprocess_tran.employeeunkid AS Empid " & _
                        "		,SUM(CAST(ISNULL(prpayrollprocess_tran.amount, 0) AS DECIMAL(36, " & decDecimalPlaces & "))) AS GrossPay "

                If mintViewIndex > 0 Then
                    StrQ &= mstrAnalysis_Fields
                Else
                    StrQ &= ", 0 AS Id, '' AS GName "
                End If

                StrQ &= "	FROM prpayrollprocess_tran " & _
                        "		JOIN prtranhead_master ON prpayrollprocess_tran.tranheadunkid = prtranhead_master.tranheadunkid " & _
                        "		JOIN prtnaleave_tran ON prtnaleave_tran.tnaleavetranunkid = prpayrollprocess_tran.tnaleavetranunkid " & _
                        "		JOIN cfcommon_period_tran ON prtnaleave_tran.payperiodunkid = cfcommon_period_tran.periodunkid " & _
                        "		JOIN hremployee_master ON hremployee_master.employeeunkid = prpayrollprocess_tran.employeeunkid "

                StrQ &= mstrAnalysis_Join

                If xUACQry.Trim.Length > 0 Then
                    StrQ &= xUACQry
                End If

                StrQ &= "	WHERE prtranhead_master.trnheadtype_id = 1 " & _
                        "		AND ISNULL(prpayrollprocess_tran.isvoid,0) = 0 " & _
                        "		AND ISNULL(prtranhead_master.isvoid, 0) = 0 " & _
                        "		AND ISNULL(prtnaleave_tran.isvoid, 0) = 0 " & _
                        "		AND cfcommon_period_tran.isactive = 1 " & _
                        "		AND cfcommon_period_tran.periodunkid = @PeriodId "

                If xUACFiltrQry.Trim.Length > 0 Then
                    StrQ &= " AND " & xUACFiltrQry
                End If

                StrQ &= "	GROUP BY prpayrollprocess_tran.employeeunkid "


                If mintViewIndex > 0 Then
                    StrQ &= mstrAnalysis_Fields.Replace("AS Id", "").Replace("AS GName", "")
                End If

            ElseIf mintBaseCurrId <> mintPaidCurrencyId Then

                StrQ = "SELECT " & _
                     "	 A.Empid " & _
                     "	,BasicSal " & _
                     "	,GrossPay " & _
                     "FROM " & _
                     "( " & _
                     "	SELECT " & _
                     "		 prpayrollprocess_tran.employeeunkid AS Empid " & _
                     "		,( prpayment_tran.expaidrate / prpayment_tran.baseexchangerate ) * SUM(CAST(ISNULL(prpayrollprocess_tran.amount, 0) AS DECIMAL(36, " & decDecimalPlaces & "))) AS BasicSal "

                If mintViewIndex > 0 Then
                    StrQ &= mstrAnalysis_Fields
                Else
                    StrQ &= ", 0 AS Id, '' AS GName "
                End If

                StrQ &= "   FROM prpayrollprocess_tran " & _
                             "   JOIN prtranhead_master ON prpayrollprocess_tran.tranheadunkid = prtranhead_master.tranheadunkid " & _
                             "   JOIN prtnaleave_tran ON prtnaleave_tran.tnaleavetranunkid = prpayrollprocess_tran.tnaleavetranunkid  " & _
                             "   JOIN cfcommon_period_tran ON prtnaleave_tran.payperiodunkid = cfcommon_period_tran.periodunkid " & _
                             "   JOIN hremployee_master ON hremployee_master.employeeunkid = prpayrollprocess_tran.employeeunkid " & _
                             "   JOIN prpayment_tran ON prpayrollprocess_tran.employeeunkid = prpayment_tran.employeeunkid AND prpayment_tran.isvoid = 0" & _
                             "   AND prtnaleave_tran.payperiodunkid = prpayment_tran.periodunkid AND prpayment_tran.countryunkid = " & mintCountryId


                StrQ &= " AND prpayment_tran.referenceid = " & clsPayment_tran.enPaymentRefId.PAYSLIP & " " & _
                            " AND prpayment_tran.paytypeid = " & clsPayment_tran.enPayTypeId.PAYMENT & " "


                StrQ &= mstrAnalysis_Join

                If xUACQry.Trim.Length > 0 Then
                    StrQ &= xUACQry
                End If
                StrQ &= "  WHERE     ISNULL(prpayrollprocess_tran.isvoid, 0) = 0 " & _
                             "  AND ISNULL(prtranhead_master.isvoid, 0) = 0 AND ISNULL(prtnaleave_tran.isvoid, 0) = 0 " & _
                                    "AND prpayrollprocess_tran.tranheadunkid > 0 " & _
                             "  AND cfcommon_period_tran.isactive = 1 AND cfcommon_period_tran.periodunkid = @PeriodId " & _
                                    "AND prpayment_tran.periodunkid = @PeriodId "

                'Sohail (22 Jul 2019) -- Start
                'Good Neighbours Enhancement # - 76.1 - Pick Basic Salary as Other Earning if option is ticked on head master in NHIF Report.
                'If mintOtherEarningTranId > 0 Then 'Basic Salary as Other Earning
                '    StrQ &= "AND prpayrollprocess_tran.tranheadunkid = @OtherEarningTranId  "
                'Else
                '    StrQ &= "AND prtranhead_master.typeof_id = " & enTypeOf.Salary & "  "
                'End If
                StrQ &= "AND ((prtranhead_master.typeof_id = " & enTypeOf.Salary & " and prtranhead_master.trnheadtype_id = " & enTranHeadType.EarningForEmployees & ") OR prtranhead_master.isbasicsalaryasotherearning  = 1) "
                'Sohail (22 Jul 2019) -- End

                If xUACFiltrQry.Trim.Length > 0 Then
                    StrQ &= " AND " & xUACFiltrQry
                End If


                StrQ &= "GROUP BY  prpayrollprocess_tran.employeeunkid,prpayment_tran.expaidrate,prpayment_tran.baseexchangerate "

                If mintViewIndex > 0 Then
                    StrQ &= mstrAnalysis_Fields.Replace("AS Id", "").Replace("AS GName", "")
                End If

                StrQ &= "  UNION ALL " & _
                            "  SELECT prpayrollprocess_tran.employeeunkid AS Empid " & _
                            ", " & mdecConversionRate & " * SUM(CAST(ISNULL(prpayrollprocess_tran.amount, 0) AS DECIMAL(36," & decDecimalPlaces & "))) AS BasicSal "

                If mintViewIndex > 0 Then
                    StrQ &= mstrAnalysis_Fields
                Else
                    StrQ &= ", 0 AS Id, '' AS GName "
                End If

                StrQ &= "   FROM  prpayrollprocess_tran " & _
                             "   JOIN prtranhead_master ON prpayrollprocess_tran.tranheadunkid = prtranhead_master.tranheadunkid " & _
                             "   JOIN prtnaleave_tran ON prtnaleave_tran.tnaleavetranunkid = prpayrollprocess_tran.tnaleavetranunkid " & _
                             "   JOIN cfcommon_period_tran ON prtnaleave_tran.payperiodunkid = cfcommon_period_tran.periodunkid " & _
                             "   JOIN hremployee_master ON hremployee_master.employeeunkid = prpayrollprocess_tran.employeeunkid " & _
                             "   JOIN prpayment_tran ON prpayrollprocess_tran.employeeunkid = prpayment_tran.employeeunkid   AND prpayment_tran.isvoid = 0 " & _
                             "   AND prtnaleave_tran.payperiodunkid = prpayment_tran.periodunkid AND prpayment_tran.countryunkid <> " & mintCountryId

                StrQ &= " AND prpayment_tran.referenceid = " & clsPayment_tran.enPaymentRefId.PAYSLIP & " " & _
                            " AND prpayment_tran.paytypeid = " & clsPayment_tran.enPayTypeId.PAYMENT & " "

                StrQ &= mstrAnalysis_Join
                If xUACQry.Trim.Length > 0 Then
                    StrQ &= xUACQry
                End If
                StrQ &= "    WHERE    ISNULL(prpayrollprocess_tran.isvoid, 0) = 0 " & _
                             "    AND ISNULL(prtranhead_master.isvoid, 0) = 0  AND ISNULL(prtnaleave_tran.isvoid, 0) = 0 " & _
                                     "AND prpayrollprocess_tran.tranheadunkid > 0 " & _
                             "    AND cfcommon_period_tran.isactive = 1 AND cfcommon_period_tran.periodunkid = @PeriodId " & _
                                     "AND prpayment_tran.periodunkid = @PeriodId "

                'Sohail (22 Jul 2019) -- Start
                'Good Neighbours Enhancement # - 76.1 - Pick Basic Salary as Other Earning if option is ticked on head master in NHIF Report.
                'If mintOtherEarningTranId > 0 Then 'Basic Salary as Other Earning
                '    StrQ &= "AND prpayrollprocess_tran.tranheadunkid = @OtherEarningTranId  "
                'Else
                '    StrQ &= "AND prtranhead_master.typeof_id = " & enTypeOf.Salary & "  "
                'End If
                StrQ &= "AND ((prtranhead_master.typeof_id = " & enTypeOf.Salary & " and prtranhead_master.trnheadtype_id = " & enTranHeadType.EarningForEmployees & ") OR prtranhead_master.isbasicsalaryasotherearning  = 1) "
                'Sohail (22 Jul 2019) -- End

                If xUACFiltrQry.Trim.Length > 0 Then
                    StrQ &= " AND " & xUACFiltrQry
                End If

                StrQ &= "GROUP BY  prpayrollprocess_tran.employeeunkid "

                If mintViewIndex > 0 Then
                    StrQ &= mstrAnalysis_Fields.Replace("AS Id", "").Replace("AS GName", "")
                End If

                StrQ &= "     UNION ALL " & _
                             "     SELECT prpayrollprocess_tran.employeeunkid AS Empid " & _
                             "     ," & mdecConversionRate & " * SUM(CAST(ISNULL(prpayrollprocess_tran.amount, 0) AS DECIMAL(36," & decDecimalPlaces & "))) AS BasicSal"


                If mintViewIndex > 0 Then
                    StrQ &= mstrAnalysis_Fields
                Else
                    StrQ &= ", 0 AS Id, '' AS GName "
                End If

                StrQ &= "    FROM prpayrollprocess_tran " & _
                             "    JOIN prtranhead_master ON prpayrollprocess_tran.tranheadunkid = prtranhead_master.tranheadunkid " & _
                             "    JOIN prtnaleave_tran ON prtnaleave_tran.tnaleavetranunkid = prpayrollprocess_tran.tnaleavetranunkid " & _
                             "    JOIN cfcommon_period_tran ON prtnaleave_tran.payperiodunkid = cfcommon_period_tran.periodunkid " & _
                             "    JOIN hremployee_master ON hremployee_master.employeeunkid = prpayrollprocess_tran.employeeunkid " & _
                             "    LEFT JOIN prpayment_tran ON prpayrollprocess_tran.employeeunkid = prpayment_tran.employeeunkid  AND prpayment_tran.isvoid = 0 " & _
                             "    AND prtnaleave_tran.payperiodunkid = prpayment_tran.periodunkid "

                StrQ &= " AND prpayment_tran.referenceid = " & clsPayment_tran.enPaymentRefId.PAYSLIP & " " & _
                            " AND prpayment_tran.paytypeid = " & clsPayment_tran.enPayTypeId.PAYMENT & " "

                StrQ &= mstrAnalysis_Join
                If xUACQry.Trim.Length > 0 Then
                    StrQ &= xUACQry
                End If
                StrQ &= "  WHERE     ISNULL(prpayrollprocess_tran.isvoid, 0) = 0 " & _
                             "  AND ISNULL(prtranhead_master.isvoid, 0) = 0 AND ISNULL(prtnaleave_tran.isvoid, 0) = 0 " & _
                                    "AND prpayrollprocess_tran.tranheadunkid > 0 " & _
                             "  AND cfcommon_period_tran.isactive = 1 AND cfcommon_period_tran.periodunkid = @PeriodId " & _
                                    "AND prpayment_tran.paymenttranunkid IS NULL "

                'Sohail (22 Jul 2019) -- Start
                'Good Neighbours Enhancement # - 76.1 - Pick Basic Salary as Other Earning if option is ticked on head master in NHIF Report.
                'If mintOtherEarningTranId > 0 Then 'Basic Salary as Other Earning
                '    StrQ &= "AND prpayrollprocess_tran.tranheadunkid = @OtherEarningTranId  "
                'Else
                '    StrQ &= "AND prtranhead_master.typeof_id = " & enTypeOf.Salary & "  "
                'End If
                StrQ &= "AND ((prtranhead_master.typeof_id = " & enTypeOf.Salary & " and prtranhead_master.trnheadtype_id = " & enTranHeadType.EarningForEmployees & ") OR prtranhead_master.isbasicsalaryasotherearning  = 1) "
                'Sohail (22 Jul 2019) -- End

                If xUACFiltrQry.Trim.Length > 0 Then
                    StrQ &= " AND " & xUACFiltrQry
                End If

                StrQ &= "GROUP BY  prpayrollprocess_tran.employeeunkid"

                If mintViewIndex > 0 Then
                    StrQ &= mstrAnalysis_Fields.Replace("AS Id", "").Replace("AS GName", "")
                End If

                StrQ &= ") AS A " & _
                             "JOIN " & _
                             "  ( " & _
                             "           SELECT   prpayrollprocess_tran.employeeunkid AS Empid " & _
                             "          ,( prpayment_tran.expaidrate / prpayment_tran.baseexchangerate ) * SUM(CAST(ISNULL(prpayrollprocess_tran.amount, 0) AS DECIMAL(36," & decDecimalPlaces & "))) AS GrossPay "

                If mintViewIndex > 0 Then
                    StrQ &= mstrAnalysis_Fields
                Else
                    StrQ &= ", 0 AS Id, '' AS GName "
                End If

                StrQ &= "          FROM prpayrollprocess_tran " & _
                            "           JOIN prtranhead_master ON prpayrollprocess_tran.tranheadunkid = prtranhead_master.tranheadunkid " & _
                            "           JOIN prtnaleave_tran ON prtnaleave_tran.tnaleavetranunkid = prpayrollprocess_tran.tnaleavetranunkid " & _
                            "           JOIN cfcommon_period_tran ON prtnaleave_tran.payperiodunkid = cfcommon_period_tran.periodunkid " & _
                            "           JOIN hremployee_master ON hremployee_master.employeeunkid = prpayrollprocess_tran.employeeunkid " & _
                            "           JOIN prpayment_tran ON prpayment_tran.employeeunkid = prpayrollprocess_tran.employeeunkid  AND prpayment_tran.isvoid = 0 " & _
                            "           AND prpayment_tran.periodunkid = prtnaleave_tran.payperiodunkid AND prpayment_tran.countryunkid = " & mintCountryId

                StrQ &= " AND prpayment_tran.referenceid = " & clsPayment_tran.enPaymentRefId.PAYSLIP & " " & _
                            " AND prpayment_tran.paytypeid = " & clsPayment_tran.enPayTypeId.PAYMENT & " "


                StrQ &= mstrAnalysis_Join

                If xUACQry.Trim.Length > 0 Then
                    StrQ &= xUACQry
                End If

                StrQ &= "          WHERE  prtranhead_master.trnheadtype_id =1 AND ISNULL(prpayrollprocess_tran.isvoid, 0) = 0 " & _
                            "           AND ISNULL(prtranhead_master.isvoid, 0) = 0 AND ISNULL(prtnaleave_tran.isvoid, 0) = 0 " & _
                             "          AND cfcommon_period_tran.isactive = 1 AND cfcommon_period_tran.periodunkid = @PeriodId " & _
                             "          AND prpayment_tran.periodunkid= @PeriodId "

                If xUACFiltrQry.Trim.Length > 0 Then
                    StrQ &= " AND " & xUACFiltrQry
                End If

                StrQ &= "          GROUP BY prpayrollprocess_tran.employeeunkid,prpayment_tran.expaidrate,prpayment_tran.baseexchangerate "

                If mintViewIndex > 0 Then
                    StrQ &= mstrAnalysis_Fields.Replace("AS Id", "").Replace("AS GName", "")
                End If

                StrQ &= "          UNION ALL " & _
                             "          SELECT   prpayrollprocess_tran.employeeunkid AS Empid " & _
                             ",         " & mdecConversionRate & " * SUM(CAST(ISNULL(prpayrollprocess_tran.amount, 0) AS DECIMAL(36," & decDecimalPlaces & "))) AS GrossPay "

                If mintViewIndex > 0 Then
                    StrQ &= mstrAnalysis_Fields
                Else
                    StrQ &= ", 0 AS Id, '' AS GName "
                End If

                StrQ &= "             FROM prpayrollprocess_tran " & _
                             "             JOIN prtranhead_master ON prpayrollprocess_tran.tranheadunkid = prtranhead_master.tranheadunkid " & _
                             "             JOIN prtnaleave_tran ON prtnaleave_tran.tnaleavetranunkid = prpayrollprocess_tran.tnaleavetranunkid " & _
                             "             JOIN cfcommon_period_tran ON prtnaleave_tran.payperiodunkid = cfcommon_period_tran.periodunkid " & _
                             "             JOIN hremployee_master ON hremployee_master.employeeunkid = prpayrollprocess_tran.employeeunkid " & _
                             "             JOIN prpayment_tran ON prpayment_tran.employeeunkid = prpayrollprocess_tran.employeeunkid  AND prpayment_tran.isvoid = 0 " & _
                             "             AND prpayment_tran.periodunkid = prtnaleave_tran.payperiodunkid AND prpayment_tran.countryunkid <> " & mintCountryId

                StrQ &= " AND prpayment_tran.referenceid = " & clsPayment_tran.enPaymentRefId.PAYSLIP & " " & _
                            " AND prpayment_tran.paytypeid = " & clsPayment_tran.enPayTypeId.PAYMENT & " "

                StrQ &= mstrAnalysis_Join
                If xUACQry.Trim.Length > 0 Then
                    StrQ &= xUACQry
                End If
                StrQ &= "            WHERE prtranhead_master.trnheadtype_id = 1 AND ISNULL(prpayrollprocess_tran.isvoid, 0) = 0 " & _
                             "            AND ISNULL(prtranhead_master.isvoid, 0) = 0  AND ISNULL(prtnaleave_tran.isvoid, 0) = 0 " & _
                             "            AND cfcommon_period_tran.isactive = 1 AND cfcommon_period_tran.periodunkid = @PeriodId " & _
                             "            AND prpayment_tran.periodunkid= @PeriodId  "


                If xUACFiltrQry.Trim.Length > 0 Then
                    StrQ &= " AND " & xUACFiltrQry
                End If


                StrQ &= "            GROUP BY prpayrollprocess_tran.employeeunkid "

                If mintViewIndex > 0 Then
                    StrQ &= mstrAnalysis_Fields.Replace("AS Id", "").Replace("AS GName", "")
                End If

                StrQ &= "          UNION ALL " & _
                             "          SELECT   prpayrollprocess_tran.employeeunkid AS Empid " & _
                             ",         " & mdecConversionRate & " * SUM(CAST(ISNULL(prpayrollprocess_tran.amount, 0) AS DECIMAL(36," & decDecimalPlaces & "))) AS GrossPay "

                If mintViewIndex > 0 Then
                    StrQ &= mstrAnalysis_Fields
                Else
                    StrQ &= ", 0 AS Id, '' AS GName "
                End If

                StrQ &= "          FROM prpayrollprocess_tran " & _
                             "          JOIN prtranhead_master ON prpayrollprocess_tran.tranheadunkid = prtranhead_master.tranheadunkid " & _
                             "          JOIN prtnaleave_tran ON prtnaleave_tran.tnaleavetranunkid = prpayrollprocess_tran.tnaleavetranunkid " & _
                            "           JOIN cfcommon_period_tran ON prtnaleave_tran.payperiodunkid = cfcommon_period_tran.periodunkid " & _
                            "           JOIN hremployee_master ON hremployee_master.employeeunkid = prpayrollprocess_tran.employeeunkid " & _
                            "           LEFT JOIN prpayment_tran ON prpayment_tran.employeeunkid = prpayrollprocess_tran.employeeunkid  AND prpayment_tran.isvoid = 0 " & _
                            "           AND prpayment_tran.periodunkid = prtnaleave_tran.payperiodunkid "

                StrQ &= " AND prpayment_tran.referenceid = " & clsPayment_tran.enPaymentRefId.PAYSLIP & " " & _
                            " AND prpayment_tran.paytypeid = " & clsPayment_tran.enPayTypeId.PAYMENT & " "

                StrQ &= mstrAnalysis_Join
                If xUACQry.Trim.Length > 0 Then
                    StrQ &= xUACQry
                End If

                StrQ &= "           WHERE prtranhead_master.trnheadtype_id = 1 AND ISNULL(prpayrollprocess_tran.isvoid, 0) = 0 " & _
                             "           AND ISNULL(prtranhead_master.isvoid, 0) = 0 AND ISNULL(prtnaleave_tran.isvoid, 0) = 0 " & _
                             "           AND cfcommon_period_tran.isactive = 1 AND cfcommon_period_tran.periodunkid = @PeriodId " & _
                             "           AND prpayment_tran.paymenttranunkid IS NULL  "


                If xUACFiltrQry.Trim.Length > 0 Then
                    StrQ &= " AND " & xUACFiltrQry
                End If

                StrQ &= "           GROUP BY prpayrollprocess_tran.employeeunkid "

                If mintViewIndex > 0 Then
                    StrQ &= mstrAnalysis_Fields.Replace("AS Id", "").Replace("AS GName", "")
                End If

            End If

            StrQ &= ") AS G ON G.Empid = A.Empid "
            dsBasGross = objDataOperation.ExecQuery(StrQ, "DataTable")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & " " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            Dim mdtData As DataTable = New DataView(dsList.Tables(0), "", "employeecode, Amount DESC", DataViewRowState.CurrentRows).ToTable

            Dim StrECode As String = String.Empty
            Dim StrPrevECode As String = String.Empty
            Dim rpt_Row As DataRow = Nothing

            rpt_Data = New ArutiReport.Designer.dsArutiReport


            Dim decColumn4Total, decColumn81Total As Decimal
            decColumn4Total = 0 : decColumn81Total = 0
            Dim iCnt As Integer = 1

            For Each dtRow As DataRow In dsList.Tables("DataTable").Rows
                StrECode = dtRow.Item("employeecode").ToString.Trim

                If StrPrevECode <> StrECode Then
                    If mblnIgnoreZero = True Then
                        If CDec(dtRow.Item("Amount")) = 0 Then Continue For
                    End If
                    rpt_Row = rpt_Data.Tables("ArutiTable").NewRow
                    iCnt += 1
                Else
                    rpt_Row = rpt_Data.Tables("ArutiTable").Rows(rpt_Data.Tables("ArutiTable").Rows.Count - 1)
                    rpt_Row.AcceptChanges()
                    StrPrevECode = StrECode
                    Continue For
                End If
                If dtRow.Item("MemshipNo").ToString.Trim = "0" Then
                    rpt_Row.Item("Column3") = ""
                Else
                    rpt_Row.Item("Column3") = dtRow.Item("MemshipNo")
                End If
                rpt_Row.Item("Column1") = dtRow.Item("employeecode").ToString
                rpt_Row.Item("Column7") = dtRow.Item("firstname").ToString
                rpt_Row.Item("Column5") = dtRow.Item("middlename").ToString
                rpt_Row.Item("Column6") = dtRow.Item("surname").ToString
                'Sohail (10 Jul 2019) -- Start
                'Good Neighbours Enhancement - Support Issue Id # 3875 - 76.1 - Put link for NHIF EFT and it should generate report without report header of report name address and company name and add "EmployerNo" column..
                rpt_Row.Item("Column10") = dtRow.Item("EmployerMemshipNo").ToString
                'Sohail (10 Jul 2019) -- End

                rpt_Data.Tables("ArutiTable").Rows.Add(rpt_Row)
                StrPrevECode = StrECode
                Dim dtTemp() As DataRow = dsBasGross.Tables("DataTable").Select("Empid = '" & dtRow.Item("Empid") & "'")
                If dtTemp.Length > 0 Then
                    If mblnShowBasicSalary = True Then
                        rpt_Row.Item("Column4") = Format(CDec(dtTemp(0).Item("BasicSal")), GUI.fmtCurrency)
                        rpt_Row.Item("Column82") = Format(CDec(dtTemp(0).Item("BasicSal")), GUI.fmtCurrency)
                        decColumn4Total = decColumn4Total + CDec(dtTemp(0).Item("BasicSal"))
                    Else
                        rpt_Row.Item("Column4") = Format(CDec(dtTemp(0).Item("GrossPay")), GUI.fmtCurrency)
                        rpt_Row.Item("Column82") = Format(CDec(dtTemp(0).Item("GrossPay")), GUI.fmtCurrency)
                        decColumn4Total = decColumn4Total + CDec(dtTemp(0).Item("GrossPay"))
                    End If
                End If
                rpt_Row.Item("Column8") = Format(CDec(dtRow.Item("Amount")), GUI.fmtCurrency)
                rpt_Row.Item("Column81") = Format(CDec(dtRow.Item("Amount")), GUI.fmtCurrency)
                decColumn81Total = decColumn81Total + CDec(dtRow.Item("Amount"))
                iCnt += 1
            Next

            mstrAddress = Company._Object._Address1 & " " & Company._Object._Address2
            mstrAddress &= " " & Company._Object._City_Name & " " & Company._Object._State_Name & " " & Company._Object._Post_Code_No & " " & Company._Object._Country_Name


            Dim intColIndex As Integer = -1
            mdtTableExcel = rpt_Data.Tables(0)
            mdtTableExcel.Columns("Column3").Caption = Language.getMessage(mstrModuleName, 10, "MembershipNo")

            intColIndex += 1
            mdtTableExcel.Columns("Column3").SetOrdinal(intColIndex)

            mdtTableExcel.Columns("Column1").Caption = Language.getMessage(mstrModuleName, 11, "PFNumber")
            intColIndex += 1
            mdtTableExcel.Columns("Column1").SetOrdinal(intColIndex)

            mdtTableExcel.Columns("Column9").Caption = Language.getMessage(mstrModuleName, 13, "ChequeNo")
            intColIndex += 1
            mdtTableExcel.Columns("Column9").SetOrdinal(intColIndex)


            mdtTableExcel.Columns("Column7").Caption = Language.getMessage(mstrModuleName, 14, "FirstName")
            intColIndex += 1
            mdtTableExcel.Columns("Column7").SetOrdinal(intColIndex)

            mdtTableExcel.Columns("Column5").Caption = Language.getMessage(mstrModuleName, 15, "MiddleName")
            intColIndex += 1
            mdtTableExcel.Columns("Column5").SetOrdinal(intColIndex)

            mdtTableExcel.Columns("Column6").Caption = Language.getMessage(mstrModuleName, 16, "LastName")
            intColIndex += 1
            mdtTableExcel.Columns("Column6").SetOrdinal(intColIndex)

            mdtTableExcel.Columns("Column82").Caption = Language.getMessage(mstrModuleName, 17, "BasicSalary")
            intColIndex += 1
            mdtTableExcel.Columns("Column82").SetOrdinal(intColIndex)

            mdtTableExcel.Columns("Column81").Caption = Language.getMessage(mstrModuleName, 18, "AmountContributed")
            intColIndex += 1
            mdtTableExcel.Columns("Column81").SetOrdinal(intColIndex)

            mdtTableExcel.Columns("Column10").Caption = Language.getMessage(mstrModuleName, 19, "EmployerNo")
            intColIndex += 1
            mdtTableExcel.Columns("Column10").SetOrdinal(intColIndex)

            For i = intColIndex + 1 To mdtTableExcel.Columns.Count - 1
                mdtTableExcel.Columns.RemoveAt(intColIndex + 1)
            Next

            Dim intArrayColWidth(mdtTableExcel.Columns.Count - 1) As Integer
            For i As Integer = 0 To intArrayColWidth.Length - 1
                intArrayColWidth(i) = 130
            Next

            mdtTableExcel.ExtendedProperties.Add("HeaderStyle", "HeaderStyleWC")

            Call ReportExecute(Nothing, enPrintAction.None, enExportAction.ExcelExtra, strExportPath, xOpenReportAfterExport, mdtTableExcel, intArrayColWidth, False, True, False, , "EFT_NHIF_(" & mstrCurrencyName & ")", , " ", , , False, Nothing, , , , False)

            Return True

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: Generate_EFT_NHIF_Report; Module Name: " & mstrModuleName)
            Return Nothing
        End Try
    End Function
    'Sohail (10 Jul 2019) -- End

#End Region

    '<Language> This Auto Generated Text Please Do Not Modify it.
#Region " Language & UI Settings "
    Public Shared Sub SetMessages()
        Try
            Language.setMessage(mstrModuleName, 1, "Membership No")
            Language.setMessage(mstrModuleName, 2, "PF Number")
            Language.setMessage(mstrModuleName, 3, "Cheque No")
            Language.setMessage(mstrModuleName, 4, "First Name")
            Language.setMessage(mstrModuleName, 5, "Middle Name")
            Language.setMessage(mstrModuleName, 6, "Last Name")
            Language.setMessage(mstrModuleName, 7, "Basic Salary")
            Language.setMessage(mstrModuleName, 8, "Amount Contributed")
            Language.setMessage(mstrModuleName, 9, "Total")

        Catch Ex As Exception
            Throw New Exception(Ex.Message & "; Procedure Name: SetMessages; Module Name: " & mstrModuleName)
        End Try
    End Sub
#End Region 'Language & UI Settings
    '</Language>
End Class
