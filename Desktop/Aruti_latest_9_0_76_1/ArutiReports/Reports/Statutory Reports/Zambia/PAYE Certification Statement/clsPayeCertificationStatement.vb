﻿'************************************************************************************************************************************
'Class Name : clsPayeCertificationStatement.vb
'Purpose    :
'Date       :28-03-2022
'Written By :Hemant
'Modified   :
'************************************************************************************************************************************

#Region " Imports "

Imports eZeeCommonLib
Imports Aruti.Data
Imports System
Imports System.Text.RegularExpressions
Imports System.Text

#End Region

''' <summary>
''' Purpose: Report Generation Class 
''' Developer: Hemant
''' </summary>
Public Class clsPayeCertificationStatement
    Inherits IReportData
    Private Shared ReadOnly mstrModuleName As String = "clsPayeCertificationStatement"
    Private mstrReportId As String = enArutiReport.PAYE_Certification_Statement

    Dim objDataOperation As clsDataOperation

    Dim Rpt As CrystalDecisions.CrystalReports.Engine.ReportClass

#Region " Constructor "

    Public Sub New(ByVal intLangId As Integer, ByVal intCompanyId As Integer)
        Me.setReportData(CInt(mstrReportId), intLangId, intCompanyId)
        Call Create_OnDetailReport()
    End Sub

#End Region

#Region " Structure "

    Public Structure STC_FYYear
        Dim YearUnkid As Integer
        Dim DatabaseName As String
        Dim EndDate As Date
    End Structure

#End Region

#Region " Private Variables "

    'For Report
    Private mintEmpId As Integer = 0
    Private mstrEmpName As String = ""
    Private mstrPeriodIds As String = ""
    Private mstrPeriodNames As String = String.Empty
    Private mintGrossTaxableHeadId As Integer = 0
    Private mstrGrossTaxableHeadName As String = ""
    Private mintFreePayHeadId As Integer = 0
    Private mstrFreePayHeadName As String = ""
    Private mintTotalTaxPaidHeadId As Integer = 0
    Private mstrTotalTaxPaidHeadName As String = ""
    Private mintNAPSAHeadId As Integer = 0
    Private mstrNAPSAHeadName As String = ""
    Private mintToPeriodId As Integer = 0
    Private mstrToPeriodName As String = ""
    
    Private mblnIncludeInactiveEmp As Boolean = True
    Private mSTCFYYear As New List(Of STC_FYYear)
    Private mstrFirstPeriodYearName As String = ""

    'For Analysis By
    Private mstrAnalysis_Fields As String = ""
    Private mstrAnalysis_Join As String = ""
    Private mstrAnalysis_OrderBy As String = ""
    Private mintViewIndex As Integer = -1
    Private mstrViewByIds As String = String.Empty
    Private mstrViewByName As String = String.Empty
    Private mstrReport_GroupName As String = ""

    Private mblnApplyUserAccessFilter As Boolean = True
    'Hemant (22 Dec 2022) -- Start
    'ENHANCEMENT(Zambia) : A1X-441 - PAYE Certification Statement to include employee TPIN
    Private mintTPINId As Integer = 0
    Private mstrTPINName As String = ""
    'Hemant (22 Dec 2022) -- End    
#End Region

#Region " Properties "

    Public WriteOnly Property _EmpId() As Integer
        Set(ByVal value As Integer)
            mintEmpId = value
        End Set
    End Property

    Public WriteOnly Property _EmpName() As String
        Set(ByVal value As String)
            mstrEmpName = value
        End Set
    End Property

    Public WriteOnly Property _GrossTaxableHeadId() As Integer
        Set(ByVal value As Integer)
            mintGrossTaxableHeadId = value
        End Set
    End Property

    Public WriteOnly Property _GrossTaxableHeadName() As String
        Set(ByVal value As String)
            mstrGrossTaxableHeadName = value
        End Set
    End Property

    Public WriteOnly Property _FreePayHeadId() As Integer
        Set(ByVal value As Integer)
            mintFreePayHeadId = value
        End Set
    End Property

    Public WriteOnly Property _FreePayHeadName() As String
        Set(ByVal value As String)
            mstrFreePayHeadName = value
        End Set
    End Property

    Public WriteOnly Property _TotalTaxPaidHeadId() As Integer
        Set(ByVal value As Integer)
            mintTotalTaxPaidHeadId = value
        End Set
    End Property

    Public WriteOnly Property _TotalTaxPaidHeadName() As String
        Set(ByVal value As String)
            mstrTotalTaxPaidHeadName = value
        End Set
    End Property

    Public WriteOnly Property _NAPSAHeadId() As Integer
        Set(ByVal value As Integer)
            mintNAPSAHeadId = value
        End Set
    End Property

    Public WriteOnly Property _NAPSAHeadName() As String
        Set(ByVal value As String)
            mstrNAPSAHeadName = value
        End Set
    End Property

    'Hemant (22 Dec 2022) -- Start
    'ENHANCEMENT(Zambia) : A1X-441 - PAYE Certification Statement to include employee TPIN
    Public WriteOnly Property _TPINId() As Integer
        Set(ByVal value As Integer)
            mintTPINId = value
        End Set
    End Property

    Public WriteOnly Property _TPINName() As String
        Set(ByVal value As String)
            mstrTPINName = value
        End Set
    End Property
    'Hemant (22 Dec 2022) -- End

    Public WriteOnly Property _STC_FYYear() As List(Of STC_FYYear)
        Set(ByVal value As List(Of STC_FYYear))
            mSTCFYYear = value
        End Set
    End Property

    Public WriteOnly Property _PeriodIds() As String
        Set(ByVal value As String)
            mstrPeriodIds = value
        End Set
    End Property

    Public WriteOnly Property _PeriodNames() As String
        Set(ByVal value As String)
            mstrPeriodNames = value
        End Set
    End Property

    Public WriteOnly Property _ToPeriodId() As Integer
        Set(ByVal value As Integer)
            mintToPeriodId = value
        End Set
    End Property

    Public WriteOnly Property _ToPeriodName() As String
        Set(ByVal value As String)
            mstrToPeriodName = value
        End Set
    End Property

    Public WriteOnly Property _IncludeInactiveEmp() As Boolean
        Set(ByVal value As Boolean)
            mblnIncludeInactiveEmp = value
        End Set
    End Property

    Public WriteOnly Property _ViewIndex() As Integer
        Set(ByVal value As Integer)
            mintViewIndex = value
        End Set
    End Property

    Public WriteOnly Property _ViewByIds() As String
        Set(ByVal value As String)
            mstrViewByIds = value
        End Set
    End Property

    Public WriteOnly Property _ViewByName() As String
        Set(ByVal value As String)
            mstrViewByName = value
        End Set
    End Property

    Public WriteOnly Property _Analysis_Fields() As String
        Set(ByVal value As String)
            mstrAnalysis_Fields = value
        End Set
    End Property

    Public WriteOnly Property _Analysis_Join() As String
        Set(ByVal value As String)
            mstrAnalysis_Join = value
        End Set
    End Property

    Public WriteOnly Property _Analysis_OrderBy() As String
        Set(ByVal value As String)
            mstrAnalysis_OrderBy = value
        End Set
    End Property

    Public WriteOnly Property _Report_GroupName() As String
        Set(ByVal value As String)
            mstrReport_GroupName = value
        End Set
    End Property

    Public WriteOnly Property _FirstPeriodYearName() As String
        Set(ByVal value As String)
            mstrFirstPeriodYearName = value
        End Set
    End Property


    Public WriteOnly Property _ApplyUserAccessFilter() As Boolean
        Set(ByVal value As Boolean)
            mblnApplyUserAccessFilter = value
        End Set
    End Property

    Public ReadOnly Property _Rpt() As CrystalDecisions.CrystalReports.Engine.ReportClass
        Get
            Return Rpt
        End Get
    End Property


#End Region

#Region "Public Function & Procedures "

    Public Sub SetDefaultValue()
        Try

            mintEmpId = 0
            mstrEmpName = ""
            mstrPeriodIds = ""
            mintGrossTaxableHeadId = 0
            mstrGrossTaxableHeadName = ""
            mintFreePayHeadId = 0
            mstrFreePayHeadName = ""
            mintTotalTaxPaidHeadId = 0
            mstrTotalTaxPaidHeadName = ""
            mintNAPSAHeadId = 0
            mstrNAPSAHeadName = ""
            'Hemant (22 Dec 2022) -- Start
            'ENHANCEMENT(Zambia) : A1X-441 - PAYE Certification Statement to include employee TPIN
            mintTPINId = 0
            mstrTPINName = ""
            'Hemant (22 Dec 2022) -- End	
            mintToPeriodId = 0
            mstrToPeriodName = ""
           
            mSTCFYYear.Clear()

            mblnIncludeInactiveEmp = True

            mstrFirstPeriodYearName = ""


            mstrAnalysis_Fields = ""
            mstrAnalysis_Join = ""
            mstrAnalysis_OrderBy = ""
            mintViewIndex = -1
            mstrViewByIds = ""
            mstrReport_GroupName = ""

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: SetDefaultValue; Module Name: " & mstrModuleName)
        End Try
    End Sub

    Private Sub FilterTitleAndFilterQuery()
        Me._FilterQuery = ""
        Me._FilterTitle = ""
        Try
            

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: FilterTitleAndFilterQuery; Module Name: " & mstrModuleName)
        End Try
    End Sub

    Public Overrides Sub generateReport(ByVal pintReportType As Integer, Optional ByVal PrintAction As Aruti.Data.enPrintAction = Aruti.Data.enPrintAction.Preview, Optional ByVal ExportAction As Aruti.Data.enExportAction = Aruti.Data.enExportAction.None)
        'Dim objRpt As CrystalDecisions.CrystalReports.Engine.ReportClass = Nothing
        'Dim strReportExportFile As String = ""

        'Try

        '    objRpt = Generate_DetailReport()

        '    If Not IsNothing(objRpt) Then
        '        Call ReportExecute(objRpt, PrintAction, ExportAction, ConfigParameter._Object._ExportReportPath, ConfigParameter._Object._OpenAfterExport)
        '    End If
        'Catch ex As Exception
        '    Throw New Exception(ex.Message & "; Procedure Name: generateReport; Module Name: " & mstrModuleName)
        'End Try
    End Sub

    Public Overrides Sub generateReportNew(ByVal xDatabaseName As String, ByVal xUserUnkid As Integer, ByVal xYearUnkid As Integer, ByVal xCompanyUnkid As Integer, ByVal xPeriodStart As Date, ByVal xPeriodEnd As Date, ByVal xUserModeSetting As String, ByVal xOnlyApproved As Boolean, ByVal xExportReportPath As String, ByVal xOpenReportAfterExport As Boolean, ByVal pintReportType As Integer, Optional ByVal PrintAction As enPrintAction = enPrintAction.Preview, Optional ByVal ExportAction As enExportAction = enExportAction.None, Optional ByVal xBaseCurrencyId As Integer = 0)
        Dim objRpt As CrystalDecisions.CrystalReports.Engine.ReportClass = Nothing
        Dim strReportExportFile As String = ""
        Dim objCompany As New clsCompany_Master
        Dim objConfig As New clsConfigOptions
        Try
            objCompany._Companyunkid = xCompanyUnkid
            objConfig._Companyunkid = xCompanyUnkid
            objRpt = Generate_DetailReport(xDatabaseName, xUserUnkid, xYearUnkid, xCompanyUnkid, xPeriodEnd, xUserModeSetting, xOnlyApproved, xBaseCurrencyId, objCompany._Name, objCompany._Tinno, objConfig._CurrencyFormat)

            Rpt = objRpt

            If Not IsNothing(objRpt) Then
                Call ReportExecute(objRpt, PrintAction, ExportAction, xExportReportPath, xOpenReportAfterExport)
            End If
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: generateReportNew; Module Name: " & mstrModuleName)
        Finally
        End Try
    End Sub


    Public Overrides Sub setDefaultOrderBy(ByVal intReportType As Integer)
        OrderByDisplay = ""
        OrderByQuery = ""
        Try

            OrderByDisplay = iColumn_DetailReport.ColumnItem(0).DisplayName
            OrderByQuery = iColumn_DetailReport.ColumnItem(0).Name

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: setDefaultOrderBy; Module Name: " & mstrModuleName)
        End Try
    End Sub

    Public Overrides Sub setOrderBy(ByVal intReportType As Integer)
        Try
            Call OrderByExecute(iColumn_DetailReport)
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: setOrderBy; Module Name: " & mstrModuleName)
        End Try
    End Sub
#End Region

#Region " Report Generation "
    Dim iColumn_DetailReport As New IColumnCollection

    Public Property Field_OnDetailReport() As IColumnCollection
        Get
            Return iColumn_DetailReport
        End Get
        Set(ByVal value As IColumnCollection)
            iColumn_DetailReport = value
        End Set
    End Property

    Private Sub Create_OnDetailReport()
        Try
            iColumn_DetailReport.Clear()
            iColumn_DetailReport.Add(New IColumn("", ""))
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: Create_OnDetailReport; Module Name: " & mstrModuleName)
        End Try
    End Sub


    Private Function Generate_DetailReport(ByVal strDatabaseName As String _
                                           , ByVal intUserUnkid As Integer _
                                           , ByVal intYearUnkid As Integer _
                                           , ByVal intCompanyUnkid As Integer _
                                           , ByVal dtPeriodEnd As Date _
                                           , ByVal strUserModeSetting As String _
                                           , ByVal blnOnlyApproved As Boolean _
                                           , ByVal intBaseCurrencyId As Integer _
                                           , ByVal strCompanyName As String _
                                           , ByVal strCompanyTinNo As String _
                                           , ByVal strFmtCurrency As String _
                                           ) As CrystalDecisions.CrystalReports.Engine.ReportClass

        Dim objRpt As CrystalDecisions.CrystalReports.Engine.ReportClass = Nothing
        Dim StrQ As String = ""
        Dim dsList As New DataSet
        Dim exForce As Exception
        Dim rpt_Data As ArutiReport.Designer.dsArutiReport
        Dim strBaseCurrencySign As String
        Dim decTotalPaye As Decimal = 0

        Try
            objDataOperation = New clsDataOperation

            Dim objExchangeRate As New clsExchangeRate
            Dim decDecimalPlaces As Decimal = 0

            objExchangeRate._ExchangeRateunkid = intBaseCurrencyId
            
            decDecimalPlaces = objExchangeRate._Digits_After_Decimal
            
            strBaseCurrencySign = objExchangeRate._Currency_Sign

            Dim xUACQry, xUACFiltrQry As String

           
            For Each key In mSTCFYYear

                xUACQry = "" : xUACFiltrQry = ""

                If mblnApplyUserAccessFilter = True Then
                    Call NewAccessLevelFilterString(xUACQry, xUACFiltrQry, key.EndDate, blnOnlyApproved, key.DatabaseName, intUserUnkid, intCompanyUnkid, key.YearUnkid, strUserModeSetting)
                End If

                StrQ &= "SELECT hremployee_master.employeeunkid " & _
                                ", hremployee_master.appointeddate " & _
                        "INTO #" & key.DatabaseName & " " & _
                        "FROM hremployee_master "

                StrQ &= "LEFT JOIN " & _
                            "( " & _
                                "SELECT hremployee_transfer_tran.employeeunkid " & _
                                     ", hremployee_transfer_tran.departmentunkid AS allocationtranunkid " & _
                                     ", ISNULL(hrdepartment_master.code, '') AS DeptCode " & _
                                     ", ISNULL(hrdepartment_master.name, '') AS DeptName " & _
                                     ", ROW_NUMBER() OVER (PARTITION BY hremployee_transfer_tran.employeeunkid ORDER BY hremployee_transfer_tran.effectivedate DESC) AS ROWNO " & _
                                "FROM hremployee_transfer_tran " & _
                                    "JOIN hremployee_master ON hremployee_master.employeeunkid = hremployee_transfer_tran.employeeunkid " & _
                                    "LEFT JOIN hrdepartment_master ON hremployee_transfer_tran.departmentunkid = hrdepartment_master.departmentunkid " & _
                                "WHERE hremployee_transfer_tran.isvoid = 0 " & _
                                      "AND CONVERT(CHAR(8), hremployee_transfer_tran.effectivedate, 112) <= '" & eZeeDate.convertDate(key.EndDate) & "' " & _
                            ") AS Transfer ON Transfer.employeeunkid = hremployee_master.employeeunkid "

                StrQ &= "LEFT JOIN " & _
                            "( " & _
                                "SELECT hremployee_categorization_tran.employeeunkid " & _
                                     ", hremployee_categorization_tran.jobunkid AS allocationtranunkid " & _
                                     ", ISNULL(hrjob_master.job_code, '') AS JobCode " & _
                                     ", ISNULL(hrjob_master.job_name, '') AS JobName " & _
                                     ", ROW_NUMBER() OVER (PARTITION BY hremployee_categorization_tran.employeeunkid ORDER BY hremployee_categorization_tran.effectivedate DESC) AS ROWNO " & _
                                "FROM hremployee_categorization_tran " & _
                                    "JOIN hremployee_master ON hremployee_master.employeeunkid = hremployee_categorization_tran.employeeunkid " & _
                                    "LEFT JOIN hrjob_master ON hremployee_categorization_tran.jobunkid = hrjob_master.jobunkid " & _
                                "WHERE hremployee_categorization_tran.isvoid = 0 " & _
                                      "AND CONVERT(CHAR(8), hremployee_categorization_tran.effectivedate, 112) <= '" & eZeeDate.convertDate(key.EndDate) & "' " & _
                            ") AS Recate ON Recate.employeeunkid = hremployee_master.employeeunkid "

                StrQ &= mstrAnalysis_Join

                If xUACQry.Trim.Length > 0 Then
                    StrQ &= xUACQry
                End If

                StrQ &= " WHERE 1 = 1 " & _
                        " AND Transfer.ROWNO = 1 " & _
                        " AND Recate.ROWNO = 1 "

                If mintEmpId > 0 Then
                    StrQ &= "                  AND hremployee_master.employeeunkid = @employeeunkid "
                End If

                If xUACFiltrQry.Trim.Length > 0 Then
                    StrQ &= " AND " & xUACFiltrQry
                End If

                StrQ &= " "
            Next

            StrQ &= "SELECT   A.employeeunkid " & _
                          ", employeecode " & _
                          ", firstname " & _
                          ", othername " & _
                          ", surname " & _
                          ", employeename " & _
                          ", CONVERT(CHAR(8), appointeddate, 112) AS appointeddate " & _
                          ", GrossTaxable " & _
                          ", FreePay " & _
                          ", TotalTaxPaid " & _
                          ", Napsa " & _
                          ", identity_no " & _
                          ", TPIN "
            'Hemant (22 Dec 2022) -- [TPIN]
            

            StrQ &= "FROM    ( "

            Dim i As Integer = -1
            Dim strCurrDatabaseName As String = ""
            Dim StrInnerQ As String = ""
            For Each key In mSTCFYYear
                strCurrDatabaseName = key.DatabaseName
                i += 1

                If i > 0 Then
                    StrInnerQ &= " UNION ALL "
                End If

                StrInnerQ &= " " & _
                             "SELECT     ISNULL(Period.employeeunkid, 0) AS employeeunkid " & _
                                      ", hremployee_master.employeecode " & _
                                      ", ISNULL(hremployee_master.firstname, '') AS firstname " & _
                                      ", ISNULL(hremployee_master.othername, '') AS othername " & _
                                      ", ISNULL(hremployee_master.surname, '') AS surname " & _
                                      ", ISNULL(hremployee_master.firstname, '') + ' ' + ISNULL(hremployee_master.othername, '') + ' ' + ISNULL(hremployee_master.surname, '') AS employeename " & _
                                      ", Period.appointeddate " & _
                                      ", SUM(ISNULL(Period.GrossTaxable, 0)) AS GrossTaxable " & _
                                      ", SUM(ISNULL(Period.FreePay, 0)) AS FreePay " & _
                                      ", SUM(ISNULL(Period.TotalTaxPaid, 0)) AS TotalTaxPaid " & _
                                      ", SUM(ISNULL(Period.Napsa, 0)) AS Napsa " & _
                                      ", ISNULL(C.identity_no, '') AS identity_no " & _
                                      ", ISNULL(D.TPIN, '') AS TPIN "
                'Hemant (22 Dec 2022) -- [TPIN]

                StrInnerQ &= "FROM      ( SELECT     prpayrollprocess_tran.employeeunkid " & _
                                                  ", CAST(amount AS DECIMAL(36, " & decDecimalPlaces & ")) AS GrossTaxable " & _
                                                  ", 0 AS FreePay " & _
                                                  ", 0 AS TotalTaxPaid " & _
                                                  ", 0 AS Napsa " & _
                                                  ", #" & strCurrDatabaseName & ".appointeddate "


                StrInnerQ &= "FROM      #" & strCurrDatabaseName & " " & _
                                                   "LEFT JOIN " & strCurrDatabaseName & "..prpayrollprocess_tran ON #" & strCurrDatabaseName & ".employeeunkid = prpayrollprocess_tran.employeeunkid " & _
                                                   "LEFT JOIN " & strCurrDatabaseName & "..prtnaleave_tran ON prpayrollprocess_tran.tnaleavetranunkid = prtnaleave_tran.tnaleavetranunkid " & _
                                                   "LEFT JOIN " & strCurrDatabaseName & "..prtranhead_master ON prtranhead_master.tranheadunkid = prpayrollprocess_tran.tranheadunkid " & _
                                                                                  "AND prpayrollprocess_tran.tranheadunkid > 0 "

                StrInnerQ &= "                      WHERE     prpayrollprocess_tran.isvoid = 0 " & _
                                                    "AND prtnaleave_tran.isvoid = 0 " & _
                                                    "AND prtnaleave_tran.payperiodunkid IN (" & mstrPeriodIds & ") " & _
                                                    "AND prtranhead_master.tranheadunkid = @grosstaxableid "

                StrInnerQ &= "                      UNION ALL " & _
                                          "SELECT     prpayrollprocess_tran.employeeunkid " & _
                                                  ", 0 AS GrossTaxable " & _
                                                  ", CAST(amount AS DECIMAL(36, " & decDecimalPlaces & ")) AS FreePay " & _
                                                  ", 0 AS TotalTaxPaid " & _
                                                  ", 0 AS Napsa " & _
                                                  ", #" & strCurrDatabaseName & ".appointeddate "


                StrInnerQ &= "FROM      #" & strCurrDatabaseName & " " & _
                                                        "LEFT JOIN " & strCurrDatabaseName & "..prpayrollprocess_tran ON #" & strCurrDatabaseName & ".employeeunkid = prpayrollprocess_tran.employeeunkid " & _
                                                        "LEFT JOIN " & strCurrDatabaseName & "..prtnaleave_tran ON prpayrollprocess_tran.tnaleavetranunkid = prtnaleave_tran.tnaleavetranunkid " & _
                                                        "LEFT JOIN " & strCurrDatabaseName & "..prtranhead_master ON prtranhead_master.tranheadunkid = prpayrollprocess_tran.tranheadunkid " & _
                                                                                  "AND prpayrollprocess_tran.tranheadunkid > 0 "

                StrInnerQ &= "                      WHERE     prpayrollprocess_tran.isvoid = 0 " & _
                                                    "AND prtnaleave_tran.isvoid = 0 " & _
                                                    "AND prtnaleave_tran.payperiodunkid IN (" & mstrPeriodIds & ") " & _
                                                    "AND prtranhead_master.tranheadunkid = @freepayheadid "

                StrInnerQ &= "                      UNION ALL " & _
                                          "SELECT     prpayrollprocess_tran.employeeunkid " & _
                                                  ", 0 AS GrossTaxable " & _
                                                  ", 0 AS FreePay " & _
                                                  ", CAST(amount AS DECIMAL(36, " & decDecimalPlaces & ")) AS TotalTaxPaid " & _
                                                  ", 0 AS Napsa " & _
                                                  ", #" & strCurrDatabaseName & ".appointeddate "


                StrInnerQ &= "FROM      #" & strCurrDatabaseName & " " & _
                                                        "LEFT JOIN " & strCurrDatabaseName & "..prpayrollprocess_tran ON #" & strCurrDatabaseName & ".employeeunkid = prpayrollprocess_tran.employeeunkid " & _
                                                        "LEFT JOIN " & strCurrDatabaseName & "..prtnaleave_tran ON prpayrollprocess_tran.tnaleavetranunkid = prtnaleave_tran.tnaleavetranunkid " & _
                                                        "LEFT JOIN " & strCurrDatabaseName & "..prtranhead_master ON prtranhead_master.tranheadunkid = prpayrollprocess_tran.tranheadunkid " & _
                                                                                  "AND prpayrollprocess_tran.tranheadunkid > 0 "

                StrInnerQ &= "                      WHERE     prpayrollprocess_tran.isvoid = 0 " & _
                                                    "AND prtnaleave_tran.isvoid = 0 " & _
                                                    "AND prtnaleave_tran.payperiodunkid IN (" & mstrPeriodIds & ") " & _
                                                    "AND prtranhead_master.tranheadunkid = @totaltaxpaidheadid "

                StrInnerQ &= "                      UNION ALL " & _
                                        "SELECT     prpayrollprocess_tran.employeeunkid " & _
                                                ", 0 AS GrossTaxable " & _
                                                ", 0 AS FreePay " & _
                                                ", 0 AS TotalTaxPaid " & _
                                                ", CAST(amount AS DECIMAL(36, " & decDecimalPlaces & ")) AS Napsa " & _
                                                ", #" & strCurrDatabaseName & ".appointeddate "


                StrInnerQ &= "FROM      #" & strCurrDatabaseName & " " & _
                                                        "LEFT JOIN " & strCurrDatabaseName & "..prpayrollprocess_tran ON #" & strCurrDatabaseName & ".employeeunkid = prpayrollprocess_tran.employeeunkid " & _
                                                        "LEFT JOIN " & strCurrDatabaseName & "..prtnaleave_tran ON prpayrollprocess_tran.tnaleavetranunkid = prtnaleave_tran.tnaleavetranunkid " & _
                                                        "LEFT JOIN " & strCurrDatabaseName & "..prtranhead_master ON prtranhead_master.tranheadunkid = prpayrollprocess_tran.tranheadunkid " & _
                                                                                  "AND prpayrollprocess_tran.tranheadunkid > 0 "

                StrInnerQ &= "                      WHERE     prpayrollprocess_tran.isvoid = 0 " & _
                                                    "AND prtnaleave_tran.isvoid = 0 " & _
                                                    "AND prtnaleave_tran.payperiodunkid IN (" & mstrPeriodIds & ") " & _
                                                    "AND prtranhead_master.tranheadunkid = @napsaheadid "


                
                StrInnerQ &= "                    ) AS Period " & _
                                    "LEFT JOIN " & strCurrDatabaseName & "..hremployee_master ON Period.employeeunkid = hremployee_master.employeeunkid " 

                StrInnerQ &= "       LEFT JOIN ( SELECT  DISTINCT #" & strCurrDatabaseName & ".employeeunkid AS EmpId " & _
                                                ", ISNULL(hremployee_idinfo_tran.identity_no, '') AS identity_no " & _
                                                  "FROM    #" & strCurrDatabaseName & " " & _
                                                          "LEFT JOIN " & strCurrDatabaseName & "..hremployee_idinfo_tran ON #" & strCurrDatabaseName & ".employeeunkid = hremployee_idinfo_tran.employeeunkid " & _
                                                          "/*JOIN " & strCurrDatabaseName & "..hremployee_master ON hremployee_idinfo_tran.employeeunkid = hremployee_master.employeeunkid*/ " & _
                                                          "LEFT JOIN " & strCurrDatabaseName & "..cfcommon_master ON cfcommon_master.masterunkid = hremployee_idinfo_tran.idtypeunkid "


                StrInnerQ &= "                   WHERE hremployee_idinfo_tran.isdefault = 1 "


                StrInnerQ &= "           ) AS C ON C.EmpId = hremployee_master.employeeunkid "

                'Hemant (22 Dec 2022) -- Start
                'ENHANCEMENT(Zambia) : A1X-441 - PAYE Certification Statement to include employee TPIN
                StrInnerQ &= "       LEFT JOIN ( SELECT  DISTINCT #" & strCurrDatabaseName & ".employeeunkid AS EmpId " & _
                                               ", ISNULL(hremployee_idinfo_tran.identity_no, '') AS TPIN " & _
                                                 "FROM    #" & strCurrDatabaseName & " " & _
                                                         "LEFT JOIN " & strCurrDatabaseName & "..hremployee_idinfo_tran ON #" & strCurrDatabaseName & ".employeeunkid = hremployee_idinfo_tran.employeeunkid "

                StrInnerQ &= "                   WHERE hremployee_idinfo_tran.idtypeunkid = @tpinid "


                StrInnerQ &= "           ) AS D ON D.EmpId = hremployee_master.employeeunkid "
                'Hemant (22 Dec 2022) -- End
                StrInnerQ &= "GROUP BY  Period.employeeunkid " & _
                                          ", hremployee_master.employeecode " & _
                                          ", ISNULL(hremployee_master.firstname, '') " & _
                                          ", ISNULL(hremployee_master.othername, '') " & _
                                          ", ISNULL(hremployee_master.surname, '') " & _
                                          ", ISNULL(hremployee_master.firstname, '') + ' ' + ISNULL(hremployee_master.othername, '') + ' ' + ISNULL(hremployee_master.surname, '') " & _
                                          ", Period.appointeddate " & _
                                          ", ISNULL(C.identity_no, '') " & _
                                          ", ISNULL(D.TPIN, '') "
                'Hemant (22 Dec 2022) -- [D.TPIN]

            Next

            Call FilterTitleAndFilterQuery()

            StrQ &= StrInnerQ
            StrQ &= ") AS A "

            StrQ &= " ORDER BY employeename "


            For Each key In mSTCFYYear
                StrQ &= " DROP TABLE #" & key.DatabaseName & " "
            Next

            If mintEmpId > 0 Then
                objDataOperation.AddParameter("@employeeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintEmpId)
            End If

            objDataOperation.AddParameter("@grosstaxableid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintGrossTaxableHeadId)
            objDataOperation.AddParameter("@freepayheadid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintFreePayHeadId)
            objDataOperation.AddParameter("@totaltaxpaidheadid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintTotalTaxPaidHeadId)
            objDataOperation.AddParameter("@napsaheadid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintNAPSAHeadId)
            'Hemant (22 Dec 2022) -- Start
            'ENHANCEMENT(Zambia) : A1X-441 - PAYE Certification Statement to include employee TPIN
            objDataOperation.AddParameter("@tpinid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintTPINId)
            'Hemant (22 Dec 2022) -- End

            dsList = objDataOperation.ExecQuery(StrQ, "DataTable")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & " : " & objDataOperation.ErrorMessage)
                Throw exForce
            End If


            rpt_Data = New ArutiReport.Designer.dsArutiReport

            Dim rpt_Row As DataRow = Nothing
            Dim intPrevEmpId As Integer = 0
            Dim intCurrEmpId As Integer = 0
            Dim decTotGrossTaxable As Decimal = 0
            Dim decTotFreePay As Decimal = 0
            Dim decTotTotalTaxPaid As Decimal = 0
            Dim decTotTotNapsa As Decimal = 0

            For Each dtRow As DataRow In dsList.Tables("DataTable").Rows
                intCurrEmpId = CInt(dtRow.Item("employeeunkid"))

                If intPrevEmpId <> intCurrEmpId Then
                    decTotGrossTaxable = 0
                    decTotFreePay = 0
                    decTotTotalTaxPaid = 0
                    decTotTotNapsa = 0
                End If

                rpt_Row = rpt_Data.Tables("ArutiTable").NewRow

                rpt_Row.Item("Column1") = mstrToPeriodName
                rpt_Row.Item("Column2") = Format(CDec(dtRow.Item("GrossTaxable")), strFmtCurrency)
               
                rpt_Row.Item("Column3") = Format(CDec(dtRow.Item("FreePay")), strFmtCurrency)
                rpt_Row.Item("Column4") = Format(CDec(dtRow.Item("TotalTaxPaid")), strFmtCurrency)
                rpt_Row.Item("Column5") = Format(CDec(dtRow.Item("Napsa")), strFmtCurrency)
                rpt_Row.Item("Column6") = Format(0.0, strFmtCurrency)

                rpt_Row.Item("Column12") = eZeeDate.convertDate(dtRow.Item("appointeddate").ToString).ToShortDateString
                'rpt_Row.Item("Column14") = dtRow.Item("period_name").ToString
                rpt_Row.Item("Column15") = dtRow.Item("employeeunkid").ToString
                rpt_Row.Item("Column16") = dtRow.Item("employeecode").ToString
                rpt_Row.Item("Column17") = dtRow.Item("employeename").ToString
                rpt_Row.Item("Column18") = dtRow.Item("firstname").ToString & " " & dtRow.Item("othername").ToString
                rpt_Row.Item("Column19") = dtRow.Item("surname").ToString
                rpt_Row.Item("Column20") = dtRow.Item("identity_no").ToString
                

                decTotGrossTaxable += CDec(rpt_Row.Item("Column2"))
                decTotFreePay += CDec(rpt_Row.Item("Column3"))
                decTotTotalTaxPaid += CDec(rpt_Row.Item("Column4"))
                decTotTotNapsa += CDec(rpt_Row.Item("Column5"))
                
                rpt_Row.Item("Column21") = Format(decTotGrossTaxable, strFmtCurrency)
                rpt_Row.Item("Column22") = Format(decTotFreePay, strFmtCurrency)
                rpt_Row.Item("Column23") = Format(decTotTotalTaxPaid, strFmtCurrency)
                rpt_Row.Item("Column24") = Format(decTotTotNapsa, strFmtCurrency)
                
                'Hemant (22 Dec 2022) -- Start
                'ENHANCEMENT(Zambia) : A1X-441 - PAYE Certification Statement to include employee TPIN
                rpt_Row.Item("Column25") = dtRow.Item("TPIN").ToString
                'Hemant (22 Dec 2022) -- End
                intPrevEmpId = intCurrEmpId

                rpt_Data.Tables("ArutiTable").Rows.Add(rpt_Row)
            Next

            objRpt = New ArutiReport.Designer.rptPayeCertification

            objRpt.SetDataSource(rpt_Data)

            ReportFunction.TextChange(objRpt, "lblCaption1", Language.getMessage(mstrModuleName, 1, "PAYE CERTIFICATION STATEMENT AS AT:"))
            ReportFunction.TextChange(objRpt, "lblNonTaxableAllowances", Language.getMessage(mstrModuleName, 2, "NON TAXABLE ALLOWANCES"))
            ReportFunction.TextChange(objRpt, "lblNatRegNo", Language.getMessage(mstrModuleName, 3, "NAT. REG. NO:"))
            ReportFunction.TextChange(objRpt, "lblPreviousEmployment", Language.getMessage(mstrModuleName, 4, "PREVIOUS EMPLOYMENT"))
            ReportFunction.TextChange(objRpt, "lblName", Language.getMessage(mstrModuleName, 5, "NAME"))
            ReportFunction.TextChange(objRpt, "lblEmployee", Language.getMessage(mstrModuleName, 6, "EMPLOYEE:"))
            ReportFunction.TextChange(objRpt, "lblThisEmployment", Language.getMessage(mstrModuleName, 7, "THIS EMPLOYMENT"))
            ReportFunction.TextChange(objRpt, "lblEmployedOn", Language.getMessage(mstrModuleName, 8, "EMPLOYED ON:"))
            ReportFunction.TextChange(objRpt, "lblRefNo", Language.getMessage(mstrModuleName, 9, "REF NO:"))
            ReportFunction.TextChange(objRpt, "txtRefNo", strCompanyTinNo)
            ReportFunction.TextChange(objRpt, "lblEmployerName", Language.getMessage(mstrModuleName, 10, "EMPLOYER:"))
            ReportFunction.TextChange(objRpt, "txtEmployerName", strCompanyName)


            ReportFunction.TextChange(objRpt, "lblTotalNonTaxableDeductions", Language.getMessage(mstrModuleName, 11, "TOTAL NON TAXABLE DEDUCTIONS:"))
            ReportFunction.TextChange(objRpt, "lblGrossTaxable", Language.getMessage(mstrModuleName, 12, "GROSS TAXABLE:"))
            ReportFunction.TextChange(objRpt, "lblFreePay", Language.getMessage(mstrModuleName, 13, "FREE PAY:"))
            ReportFunction.TextChange(objRpt, "lblTotalTaxPaid", Language.getMessage(mstrModuleName, 14, "TOTAL TAX PAID:"))
            ReportFunction.TextChange(objRpt, "lblNapsa", Language.getMessage(mstrModuleName, 15, "NAPSA"))

            ReportFunction.TextChange(objRpt, "lblSignature", Language.getMessage(mstrModuleName, 16, "SIGNATURE:"))
            ReportFunction.TextChange(objRpt, "lblDate", Language.getMessage(mstrModuleName, 17, "DATE:"))
            ReportFunction.TextChange(objRpt, "lblITFP22", Language.getMessage(mstrModuleName, 18, "ITF/P22"))
            'Hemant (22 Dec 2022) -- Start
            'ENHANCEMENT(Zambia) : A1X-441 - PAYE Certification Statement to include employee TPIN
            ReportFunction.TextChange(objRpt, "lbltpin", Language.getMessage(mstrModuleName, 19, "TPIN:"))
            'Hemant (22 Dec 2022) -- End

            Return objRpt

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: Generate_DetailReport; Module Name: " & mstrModuleName)
            Return Nothing
        End Try
    End Function

#End Region


	'<Language> This Auto Generated Text Please Do Not Modify it.
#Region " Language & UI Settings "
	Public Shared Sub SetMessages()
		Try
			Language.setMessage(mstrModuleName, 1, "PAYE CERTIFICATION STATEMENT AS AT:")
			Language.setMessage(mstrModuleName, 2, "NON TAXABLE ALLOWANCES")
			Language.setMessage(mstrModuleName, 3, "NAT. REG. NO:")
			Language.setMessage(mstrModuleName, 4, "PREVIOUS EMPLOYMENT")
			Language.setMessage(mstrModuleName, 5, "NAME")
			Language.setMessage(mstrModuleName, 6, "EMPLOYEE:")
			Language.setMessage(mstrModuleName, 7, "THIS EMPLOYMENT")
			Language.setMessage(mstrModuleName, 8, "EMPLOYED ON:")
			Language.setMessage(mstrModuleName, 9, "REF NO:")
			Language.setMessage(mstrModuleName, 10, "EMPLOYER:")
			Language.setMessage(mstrModuleName, 11, "TOTAL NON TAXABLE DEDUCTIONS:")
			Language.setMessage(mstrModuleName, 12, "GROSS TAXABLE:")
			Language.setMessage(mstrModuleName, 13, "FREE PAY:")
			Language.setMessage(mstrModuleName, 14, "TOTAL TAX PAID:")
			Language.setMessage(mstrModuleName, 15, "NAPSA")
			Language.setMessage(mstrModuleName, 16, "SIGNATURE:")
			Language.setMessage(mstrModuleName, 17, "DATE:")
			Language.setMessage(mstrModuleName, 18, "ITF/P22")
            Language.setMessage(mstrModuleName, 19, "TPIN:")
			
		Catch Ex As Exception
            DisplayError.Show("-1", Ex.Message, "SetMessages", mstrModuleName)
		End Try
	End Sub
#End Region 'Language & UI Settings
	'</Language>
End Class
