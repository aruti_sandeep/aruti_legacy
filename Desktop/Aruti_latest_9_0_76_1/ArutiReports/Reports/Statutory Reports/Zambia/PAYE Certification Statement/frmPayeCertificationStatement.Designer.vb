﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmPayeCertificationStatement
    Inherits Aruti.Data.frmBaseReportForm

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmPayeCertificationStatement))
        Me.gbFilterCriteria = New eZee.Common.eZeeCollapsibleContainer(Me.components)
        Me.cboNAPSA = New System.Windows.Forms.ComboBox
        Me.lblNAPSA = New System.Windows.Forms.Label
        Me.lnkSetAnalysis = New System.Windows.Forms.LinkLabel
        Me.cboTotalTaxPaid = New System.Windows.Forms.ComboBox
        Me.lblTotalTaxPaid = New System.Windows.Forms.Label
        Me.btnSaveSelection = New eZee.Common.eZeeLightButton(Me.components)
        Me.cboFreePay = New System.Windows.Forms.ComboBox
        Me.lblFreePay = New System.Windows.Forms.Label
        Me.cboToPeriod = New System.Windows.Forms.ComboBox
        Me.lblToPeriod = New System.Windows.Forms.Label
        Me.cboFromPeriod = New System.Windows.Forms.ComboBox
        Me.lblFromPeriod = New System.Windows.Forms.Label
        Me.lblGrossTaxable = New System.Windows.Forms.Label
        Me.cboGrossTaxable = New System.Windows.Forms.ComboBox
        Me.cboEmployee = New System.Windows.Forms.ComboBox
        Me.lblEmployee = New System.Windows.Forms.Label
        Me.lblTPIN = New System.Windows.Forms.Label
        Me.cboTPIN = New System.Windows.Forms.ComboBox
        Me.gbFilterCriteria.SuspendLayout()
        Me.SuspendLayout()
        '
        'NavPanel
        '
        Me.NavPanel.Location = New System.Drawing.Point(0, 434)
        Me.NavPanel.Size = New System.Drawing.Size(711, 55)
        '
        'gbFilterCriteria
        '
        Me.gbFilterCriteria.BorderColor = System.Drawing.Color.Black
        Me.gbFilterCriteria.Checked = False
        Me.gbFilterCriteria.CollapseAllExceptThis = False
        Me.gbFilterCriteria.CollapsedHoverImage = Nothing
        Me.gbFilterCriteria.CollapsedNormalImage = Nothing
        Me.gbFilterCriteria.CollapsedPressedImage = Nothing
        Me.gbFilterCriteria.CollapseOnLoad = False
        Me.gbFilterCriteria.Controls.Add(Me.cboTPIN)
        Me.gbFilterCriteria.Controls.Add(Me.lblTPIN)
        Me.gbFilterCriteria.Controls.Add(Me.cboNAPSA)
        Me.gbFilterCriteria.Controls.Add(Me.lblNAPSA)
        Me.gbFilterCriteria.Controls.Add(Me.lnkSetAnalysis)
        Me.gbFilterCriteria.Controls.Add(Me.cboTotalTaxPaid)
        Me.gbFilterCriteria.Controls.Add(Me.lblTotalTaxPaid)
        Me.gbFilterCriteria.Controls.Add(Me.btnSaveSelection)
        Me.gbFilterCriteria.Controls.Add(Me.cboFreePay)
        Me.gbFilterCriteria.Controls.Add(Me.lblFreePay)
        Me.gbFilterCriteria.Controls.Add(Me.cboToPeriod)
        Me.gbFilterCriteria.Controls.Add(Me.lblToPeriod)
        Me.gbFilterCriteria.Controls.Add(Me.cboFromPeriod)
        Me.gbFilterCriteria.Controls.Add(Me.lblFromPeriod)
        Me.gbFilterCriteria.Controls.Add(Me.lblGrossTaxable)
        Me.gbFilterCriteria.Controls.Add(Me.cboGrossTaxable)
        Me.gbFilterCriteria.Controls.Add(Me.cboEmployee)
        Me.gbFilterCriteria.Controls.Add(Me.lblEmployee)
        Me.gbFilterCriteria.ExpandedHoverImage = Nothing
        Me.gbFilterCriteria.ExpandedNormalImage = Nothing
        Me.gbFilterCriteria.ExpandedPressedImage = Nothing
        Me.gbFilterCriteria.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.gbFilterCriteria.GradientColor = System.Drawing.SystemColors.ButtonFace
        Me.gbFilterCriteria.HeaderHeight = 25
        Me.gbFilterCriteria.HeaderMessage = ""
        Me.gbFilterCriteria.HeaderMessageFont = New System.Drawing.Font("Tahoma", 8.25!)
        Me.gbFilterCriteria.HeaderMessageForeColor = System.Drawing.Color.Black
        Me.gbFilterCriteria.HeightOnCollapse = 0
        Me.gbFilterCriteria.LeftTextSpace = 0
        Me.gbFilterCriteria.Location = New System.Drawing.Point(12, 66)
        Me.gbFilterCriteria.Name = "gbFilterCriteria"
        Me.gbFilterCriteria.OpenHeight = 300
        Me.gbFilterCriteria.PanelBorderStyle = System.Windows.Forms.ButtonBorderStyle.Solid
        Me.gbFilterCriteria.ShowBorder = True
        Me.gbFilterCriteria.ShowCheckBox = False
        Me.gbFilterCriteria.ShowCollapseButton = False
        Me.gbFilterCriteria.ShowDefaultBorderColor = True
        Me.gbFilterCriteria.ShowDownButton = False
        Me.gbFilterCriteria.ShowHeader = True
        Me.gbFilterCriteria.Size = New System.Drawing.Size(643, 190)
        Me.gbFilterCriteria.TabIndex = 0
        Me.gbFilterCriteria.Temp = 0
        Me.gbFilterCriteria.Text = "Filter Criteria"
        Me.gbFilterCriteria.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'cboNAPSA
        '
        Me.cboNAPSA.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboNAPSA.FormattingEnabled = True
        Me.cboNAPSA.Location = New System.Drawing.Point(447, 89)
        Me.cboNAPSA.Name = "cboNAPSA"
        Me.cboNAPSA.Size = New System.Drawing.Size(167, 21)
        Me.cboNAPSA.TabIndex = 74
        '
        'lblNAPSA
        '
        Me.lblNAPSA.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblNAPSA.Location = New System.Drawing.Point(319, 91)
        Me.lblNAPSA.Name = "lblNAPSA"
        Me.lblNAPSA.Size = New System.Drawing.Size(122, 15)
        Me.lblNAPSA.TabIndex = 75
        Me.lblNAPSA.Text = "NAPSA"
        Me.lblNAPSA.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'lnkSetAnalysis
        '
        Me.lnkSetAnalysis.BackColor = System.Drawing.Color.Transparent
        Me.lnkSetAnalysis.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lnkSetAnalysis.Location = New System.Drawing.Point(535, 4)
        Me.lnkSetAnalysis.Name = "lnkSetAnalysis"
        Me.lnkSetAnalysis.Size = New System.Drawing.Size(94, 17)
        Me.lnkSetAnalysis.TabIndex = 72
        Me.lnkSetAnalysis.TabStop = True
        Me.lnkSetAnalysis.Text = "Analysis By"
        Me.lnkSetAnalysis.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.lnkSetAnalysis.Visible = False
        '
        'cboTotalTaxPaid
        '
        Me.cboTotalTaxPaid.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboTotalTaxPaid.FormattingEnabled = True
        Me.cboTotalTaxPaid.Location = New System.Drawing.Point(131, 87)
        Me.cboTotalTaxPaid.Name = "cboTotalTaxPaid"
        Me.cboTotalTaxPaid.Size = New System.Drawing.Size(167, 21)
        Me.cboTotalTaxPaid.TabIndex = 4
        '
        'lblTotalTaxPaid
        '
        Me.lblTotalTaxPaid.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblTotalTaxPaid.Location = New System.Drawing.Point(8, 89)
        Me.lblTotalTaxPaid.Name = "lblTotalTaxPaid"
        Me.lblTotalTaxPaid.Size = New System.Drawing.Size(117, 15)
        Me.lblTotalTaxPaid.TabIndex = 10
        Me.lblTotalTaxPaid.Text = "Total Tax Paid"
        Me.lblTotalTaxPaid.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'btnSaveSelection
        '
        Me.btnSaveSelection.BackColor = System.Drawing.Color.White
        Me.btnSaveSelection.BackgroundImage = CType(resources.GetObject("btnSaveSelection.BackgroundImage"), System.Drawing.Image)
        Me.btnSaveSelection.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.btnSaveSelection.BorderColor = System.Drawing.Color.Empty
        Me.btnSaveSelection.ButtonType = eZee.Common.eZeeLightButton.enButtonType.NormalButton
        Me.btnSaveSelection.FlatAppearance.BorderSize = 0
        Me.btnSaveSelection.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnSaveSelection.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnSaveSelection.ForeColor = System.Drawing.Color.Black
        Me.btnSaveSelection.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.btnSaveSelection.GradientForeColor = System.Drawing.Color.Black
        Me.btnSaveSelection.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.btnSaveSelection.HoverGradientForeColor = System.Drawing.Color.Black
        Me.btnSaveSelection.Location = New System.Drawing.Point(131, 145)
        Me.btnSaveSelection.Name = "btnSaveSelection"
        Me.btnSaveSelection.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.btnSaveSelection.PressedGradientForeColor = System.Drawing.Color.Black
        Me.btnSaveSelection.Size = New System.Drawing.Size(111, 30)
        Me.btnSaveSelection.TabIndex = 6
        Me.btnSaveSelection.Text = "&Save Selection"
        Me.btnSaveSelection.UseVisualStyleBackColor = True
        '
        'cboFreePay
        '
        Me.cboFreePay.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboFreePay.FormattingEnabled = True
        Me.cboFreePay.Location = New System.Drawing.Point(447, 60)
        Me.cboFreePay.Name = "cboFreePay"
        Me.cboFreePay.Size = New System.Drawing.Size(167, 21)
        Me.cboFreePay.TabIndex = 3
        '
        'lblFreePay
        '
        Me.lblFreePay.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblFreePay.Location = New System.Drawing.Point(319, 62)
        Me.lblFreePay.Name = "lblFreePay"
        Me.lblFreePay.Size = New System.Drawing.Size(122, 15)
        Me.lblFreePay.TabIndex = 7
        Me.lblFreePay.Text = "Free Pay"
        Me.lblFreePay.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'cboToPeriod
        '
        Me.cboToPeriod.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboToPeriod.FormattingEnabled = True
        Me.cboToPeriod.Location = New System.Drawing.Point(447, 33)
        Me.cboToPeriod.Name = "cboToPeriod"
        Me.cboToPeriod.Size = New System.Drawing.Size(167, 21)
        Me.cboToPeriod.TabIndex = 1
        '
        'lblToPeriod
        '
        Me.lblToPeriod.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblToPeriod.Location = New System.Drawing.Point(319, 36)
        Me.lblToPeriod.Name = "lblToPeriod"
        Me.lblToPeriod.Size = New System.Drawing.Size(122, 15)
        Me.lblToPeriod.TabIndex = 2
        Me.lblToPeriod.Text = "To Period"
        Me.lblToPeriod.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'cboFromPeriod
        '
        Me.cboFromPeriod.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboFromPeriod.FormattingEnabled = True
        Me.cboFromPeriod.Location = New System.Drawing.Point(131, 33)
        Me.cboFromPeriod.Name = "cboFromPeriod"
        Me.cboFromPeriod.Size = New System.Drawing.Size(167, 21)
        Me.cboFromPeriod.TabIndex = 0
        '
        'lblFromPeriod
        '
        Me.lblFromPeriod.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblFromPeriod.Location = New System.Drawing.Point(8, 36)
        Me.lblFromPeriod.Name = "lblFromPeriod"
        Me.lblFromPeriod.Size = New System.Drawing.Size(117, 15)
        Me.lblFromPeriod.TabIndex = 0
        Me.lblFromPeriod.Text = "From Period"
        Me.lblFromPeriod.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'lblGrossTaxable
        '
        Me.lblGrossTaxable.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblGrossTaxable.Location = New System.Drawing.Point(8, 62)
        Me.lblGrossTaxable.Name = "lblGrossTaxable"
        Me.lblGrossTaxable.Size = New System.Drawing.Size(117, 15)
        Me.lblGrossTaxable.TabIndex = 4
        Me.lblGrossTaxable.Text = "Gross Taxable"
        Me.lblGrossTaxable.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'cboGrossTaxable
        '
        Me.cboGrossTaxable.DropDownWidth = 180
        Me.cboGrossTaxable.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboGrossTaxable.FormattingEnabled = True
        Me.cboGrossTaxable.Location = New System.Drawing.Point(131, 60)
        Me.cboGrossTaxable.Name = "cboGrossTaxable"
        Me.cboGrossTaxable.Size = New System.Drawing.Size(167, 21)
        Me.cboGrossTaxable.TabIndex = 2
        '
        'cboEmployee
        '
        Me.cboEmployee.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboEmployee.FormattingEnabled = True
        Me.cboEmployee.Location = New System.Drawing.Point(131, 114)
        Me.cboEmployee.Name = "cboEmployee"
        Me.cboEmployee.Size = New System.Drawing.Size(167, 21)
        Me.cboEmployee.TabIndex = 5
        '
        'lblEmployee
        '
        Me.lblEmployee.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblEmployee.Location = New System.Drawing.Point(8, 120)
        Me.lblEmployee.Name = "lblEmployee"
        Me.lblEmployee.Size = New System.Drawing.Size(117, 15)
        Me.lblEmployee.TabIndex = 29
        Me.lblEmployee.Text = "Employee"
        Me.lblEmployee.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'lblTPIN
        '
        Me.lblTPIN.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblTPIN.Location = New System.Drawing.Point(319, 120)
        Me.lblTPIN.Name = "lblTPIN"
        Me.lblTPIN.Size = New System.Drawing.Size(104, 15)
        Me.lblTPIN.TabIndex = 244
        Me.lblTPIN.Text = "TPIN"
        Me.lblTPIN.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'cboTPIN
        '
        Me.cboTPIN.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboTPIN.FormattingEnabled = True
        Me.cboTPIN.Location = New System.Drawing.Point(447, 120)
        Me.cboTPIN.Name = "cboTPIN"
        Me.cboTPIN.Size = New System.Drawing.Size(167, 21)
        Me.cboTPIN.TabIndex = 245
        '
        'frmPayeCertificationStatement
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(711, 489)
        Me.Controls.Add(Me.gbFilterCriteria)
        Me.Name = "frmPayeCertificationStatement"
        Me.Text = "frmPayeCertificationStatement"
        Me.Controls.SetChildIndex(Me.gbFilterCriteria, 0)
        Me.Controls.SetChildIndex(Me.NavPanel, 0)
        Me.gbFilterCriteria.ResumeLayout(False)
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents gbFilterCriteria As eZee.Common.eZeeCollapsibleContainer
    Friend WithEvents lnkSetAnalysis As System.Windows.Forms.LinkLabel
    Friend WithEvents btnSaveSelection As eZee.Common.eZeeLightButton
    Friend WithEvents cboTotalTaxPaid As System.Windows.Forms.ComboBox
    Friend WithEvents lblTotalTaxPaid As System.Windows.Forms.Label
    Friend WithEvents cboFreePay As System.Windows.Forms.ComboBox
    Friend WithEvents lblFreePay As System.Windows.Forms.Label
    Friend WithEvents cboToPeriod As System.Windows.Forms.ComboBox
    Friend WithEvents lblToPeriod As System.Windows.Forms.Label
    Friend WithEvents cboFromPeriod As System.Windows.Forms.ComboBox
    Friend WithEvents lblFromPeriod As System.Windows.Forms.Label
    Friend WithEvents lblGrossTaxable As System.Windows.Forms.Label
    Friend WithEvents cboGrossTaxable As System.Windows.Forms.ComboBox
    Friend WithEvents cboEmployee As System.Windows.Forms.ComboBox
    Friend WithEvents lblEmployee As System.Windows.Forms.Label
    Friend WithEvents cboNAPSA As System.Windows.Forms.ComboBox
    Friend WithEvents lblNAPSA As System.Windows.Forms.Label
    Friend WithEvents cboTPIN As System.Windows.Forms.ComboBox
    Friend WithEvents lblTPIN As System.Windows.Forms.Label
End Class
