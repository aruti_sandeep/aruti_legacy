#Region " Imports "

Imports eZeeCommonLib
Imports Aruti.Data

#End Region

Public Class frmPayrollReport

#Region " Private Variables "

    Private ReadOnly mstrModuleName As String = "frmPayrollReport"
    Private objPayroll As clsPayrollReport

    'Sohail (26 Nov 2011) -- Start
    Private mstrStringIds As String = ""
    Private mstrStringName As String = ""
    Private mintViewIdx As Integer = 0
    Private mstrAnalysis_Fields As String = ""
    Private mstrAnalysis_Join As String = ""
    Private mstrAnalysis_OrderBy As String = ""
    Private mstrReport_GroupName As String = ""
    'Sohail (26 Nov 2011) -- End


    'S.SANDEEP [ 11 SEP 2012 ] -- START
    'ENHANCEMENT : TRA CHANGES
    Private mdecEx_Rate As Decimal = 0
    Private mstrCurr_Sign As String = String.Empty
    'S.SANDEEP [ 11 SEP 2012 ] -- END

    'Sohail (16 Mar 2013) -- Start
    'TRA - ENHANCEMENT
    Private mstrBaseCurrSign As String = String.Empty
    Private mintBaseCurrId As Integer = 0
    Private mstrCurrency_Rate As String = String.Empty
    'Sohail (16 Mar 2013) -- End

    'Sohail (08 Dec 2012) -- Start
    'TRA - ENHANCEMENT
    Private mstrFromDatabaseName As String
    Private mstrToDatabaseName As String
    Private mintFirstOpenPeriod As Integer = 0
    'Sohail (08 Dec 2012) -- End

    'Pinkal (27-Feb-2013) -- Start
    'Enhancement : TRA Changes
    Private mstrAdvanceFilter As String = String.Empty
    'Pinkal (27-Feb-2013) -- End

    'Shani [ 22 NOV 2014 ] -- START
    ''FDRC Enhancemwnt - Provide Transaction Head Type filter on Custom Payroll Report.
    Private marrCustomPayrollReportHeadsIds As New ArrayList
    'Shani [ 22 NOV 2014 ] -- END

#End Region

#Region " Constructor "

    Public Sub New()
        objPayroll = New clsPayrollReport(User._Object._Languageunkid,Company._Object._Companyunkid)
        objPayroll.SetDefaultValue()
        InitializeComponent()
    End Sub

#End Region

#Region " Private Function "

    Private Sub FillCombo()
        'Sohail (10 Jul 2019) -- Start
        'Good Neighbours Enhancement - Support Issue Id # 3876 - 76.1 - Put membership number in custom payroll report -& separate employee name as First Name, Middle Name and Last Name.
        Dim objMembership As New clsmembership_master
        'Sohail (10 Jul 2019) -- End
        Try
            Dim objEmp As New clsEmployee_Master
            Dim objperiod As New clscommom_period_Tran
            'S.SANDEEP [ 17 AUG 2011 ] -- START
            'ENHANCEMENT : BRANCH ( STATION )INCLUSION IN FILTER
            Dim objBranch As New clsStation
            'S.SANDEEP [ 17 AUG 2011 ] -- END 

            'S.SANDEEP [ 11 SEP 2012 ] -- START
            'ENHANCEMENT : TRA CHANGES
            Dim objExRate As New clsExchangeRate
            'S.SANDEEP [ 11 SEP 2012 ] -- END

            Dim objMaster As New clsMasterData 'Sohail (08 Dec 2012)

            'Pinkal (31-Mar-2023) -- Start
            '[JIRA] (A1X-676) Sissalana - Custom Payroll Report Modification to include Employee Identity info.
            Dim objIdentity As New clsCommon_Master
            'Pinkal (31-Mar-2023) -- End


            Dim dsList As New DataSet


            'Pinkal (24-Jun-2011) -- Start
            'ISSUE : INCLUSION OF INACTIVE EMPLOYEE
            'dsList = objEmp.GetEmployeeList("Emp", True, True)

            'Anjan [10 June 2015] -- Start
            'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
            'dsList = objEmp.GetEmployeeList("Emp", True, False)

            'Nilay (10-Feb-2016) -- Start
            'dsList = objEmp.GetEmployeeList(FinancialYear._Object._DatabaseName, _
            '                              User._Object._Userunkid, _
            '                              FinancialYear._Object._YearUnkid, _
            '                              Company._Object._Companyunkid, _
            '                              eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), _
            '                              eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), _
            '                              ConfigParameter._Object._UserAccessModeSetting, _
            '                              True, ConfigParameter._Object._IsIncludeInactiveEmp, "Emp", True)

            dsList = objEmp.GetEmployeeList(FinancialYear._Object._DatabaseName, _
                                           User._Object._Userunkid, _
                                           FinancialYear._Object._YearUnkid, _
                                           Company._Object._Companyunkid, _
                                           eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), _
                                           eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), _
                                           ConfigParameter._Object._UserAccessModeSetting, _
                                          True, True, "Emp", True)
            'Nilay (10-Feb-2016) -- End

            'Anjan [10 June 2015] -- End

            'Pinkal (24-Jun-2011) -- End

            With cboEmployee
                .ValueMember = "employeeunkid"
                .DisplayMember = "employeename"
                .DataSource = dsList.Tables("Emp")
                .SelectedValue = 0
            End With
            objEmp = Nothing

            'Sohail (08 Dec 2012) -- Start
            'TRA - ENHANCEMENT - Allow Personal Salary Calculation Report to Closed FY
            'dsList = objperiod.getListForCombo(enModuleReference.Payroll, FinancialYear._Object._YearUnkid, "Period", True)

            'S.SANDEEP [04 JUN 2015] -- START
            'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
            'mintFirstOpenPeriod = objMaster.getFirstPeriodID(enModuleReference.Payroll, 1, FinancialYear._Object._YearUnkid)
            mintFirstOpenPeriod = objMaster.getFirstPeriodID(enModuleReference.Payroll, FinancialYear._Object._YearUnkid, 1)
            'S.SANDEEP [04 JUN 2015] -- END

            'Sohail (21 Aug 2015) -- Start
            'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
            'dsList = objperiod.getListForCombo(enModuleReference.Payroll, 0, "Period", True)
            dsList = objperiod.getListForCombo(enModuleReference.Payroll, 0, FinancialYear._Object._DatabaseName, FinancialYear._Object._Database_Start_Date, "Period", True)
            'Sohail (21 Aug 2015) -- End
            'Sohail (08 Dec 2012) -- End
            With cboPeriod
                .ValueMember = "periodunkid"
                .DisplayMember = "name"
                .DataSource = dsList.Tables(0)
                'Sohail (08 Dec 2012) -- Start
                'TRA - ENHANCEMENT
                '.SelectedValue = 0
                .SelectedValue = mintFirstOpenPeriod
                'Sohail (08 Dec 2012) -- End
            End With
            'Sohail (05 Apr 2012) -- Start
            'TRA - ENHANCEMENT

            'Sohail (08 Dec 2012) -- Start
            'TRA - ENHANCEMENT - Allow Personal Salary Calculation Report to Closed FY
            'dsList = objperiod.getListForCombo(enModuleReference.Payroll, FinancialYear._Object._YearUnkid, "Period", True)
            'Sohail (21 Aug 2015) -- Start
            'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
            'dsList = objperiod.getListForCombo(enModuleReference.Payroll, 0, "Period", True)
            dsList = objperiod.getListForCombo(enModuleReference.Payroll, 0, FinancialYear._Object._DatabaseName, FinancialYear._Object._Database_Start_Date, "Period", True)
            'Sohail (21 Aug 2015) -- End
            'Sohail (08 Dec 2012) -- End
            With cboToPeriod
                .ValueMember = "periodunkid"
                .DisplayMember = "name"
                .DataSource = dsList.Tables(0)
                'Sohail (08 Dec 2012) -- Start
                'TRA - ENHANCEMENT
                '.SelectedValue = 0
                .SelectedValue = mintFirstOpenPeriod
                'Sohail (08 Dec 2012) -- End
            End With
            'Sohail (05 Apr 2012) -- End
            objperiod = Nothing

            'S.SANDEEP [ 17 AUG 2011 ] -- START
            'ENHANCEMENT : BRANCH ( STATION )INCLUSION IN FILTER
            dsList = objBranch.getComboList("List", True)
            With cboBranch
                .ValueMember = "stationunkid"
                .DisplayMember = "name"
                .DataSource = dsList.Tables("List")
                .SelectedValue = 0
            End With
            'S.SANDEEP [ 17 AUG 2011 ] -- END 

            'Sohail (05 Apr 2012) -- Start
            'TRA - ENHANCEMENT
            With cboReportType
                .Items.Clear()
                .Items.Add(Language.getMessage(mstrModuleName, 1, "Payroll Report"))
                .Items.Add(Language.getMessage(mstrModuleName, 2, "Personal Salary Calculation Report"))
                .Items.Add(Language.getMessage(mstrModuleName, 9, "Employee Payroll Summary Report")) 'Sohail (01 Sep 2014)
                'S.SANDEEP [ 29 SEP 2014 ] -- START
                .Items.Add(Language.getMessage(mstrModuleName, 10, "Custom Payroll Report"))
                'S.SANDEEP [ 29 SEP 2014 ] -- END
                'Sohail (20 Apr 2015) -- Start
                'KVTC Enhancement - New Report Custom Payroll Report Template 2 in Payroll Report. (Email From: Rutta Anatory, Subject: KVTC, Sent: Monday, March 02, 2015 1:13 PM)
                .Items.Add(Language.getMessage(mstrModuleName, 14, "Custom Payroll Report Template 2"))
                'Sohail (20 Apr 2015) -- End
                .SelectedIndex = 0
            End With
            'Sohail (05 Apr 2012) -- End

            'S.SANDEEP [ 11 SEP 2012 ] -- START
            'ENHANCEMENT : TRA CHANGES
            dsList = objExRate.getComboList("ExRate", True)
            'Sohail (16 Mar 2013) -- Start
            'TRA - ENHANCEMENT
            If dsList.Tables(0).Rows.Count > 0 Then
                Dim dtTable As DataTable = New DataView(dsList.Tables("ExRate"), "isbasecurrency = 1 ", "", DataViewRowState.CurrentRows).ToTable
                If dtTable.Rows.Count > 0 Then
                    mstrBaseCurrSign = dtTable.Rows(0).Item("currency_sign").ToString
                    mintBaseCurrId = CInt(dtTable.Rows(0).Item("exchangerateunkid"))
                End If
            End If
            'Sohail (16 Mar 2013) -- End

            With cboCurrency
                .ValueMember = "countryunkid"
                .DisplayMember = "currency_sign"
                .DataSource = dsList.Tables("ExRate")
                .SelectedValue = 0
            End With
            'S.SANDEEP [ 11 SEP 2012 ] -- END


            'Shani [ 22 NOV 2014 ] -- START
            'FDRC Enhancemwnt - Provide Transaction Head Type filter on Custom Payroll Report.
            dsList = objMaster.getComboListForHeadType("HeadType")
            'Sohail (03 Feb 2016) -- Start
            'Enhancement - Show Loans and Savings in Separate Columns option in Payroll Report.
            Dim dr As DataRow = dsList.Tables("HeadType").NewRow
            dr.Item("Id") = -1
            dr.Item("Name") = Language.getMessage(mstrModuleName, 15, "Loan")
            dsList.Tables("HeadType").Rows.Add(dr)
            dr = dsList.Tables("HeadType").NewRow
            dr.Item("Id") = -2
            dr.Item("Name") = Language.getMessage(mstrModuleName, 16, "Advance")
            dsList.Tables("HeadType").Rows.Add(dr)
            dr = dsList.Tables("HeadType").NewRow
            dr.Item("Id") = -3
            dr.Item("Name") = Language.getMessage(mstrModuleName, 17, "Savings")
            dsList.Tables("HeadType").Rows.Add(dr)
            'Sohail (03 Feb 2016) -- End
            With cboTrnHeadType
                .ValueMember = "Id"
                .DisplayMember = "Name"
                .DataSource = dsList.Tables("HeadType")
                .SelectedValue = 0
            End With
            'Shani [ 22 NOV 2014 ] -- END

            'Sohail (10 Jul 2019) -- Start
            'Good Neighbours Enhancement - Support Issue Id # 3876 - 76.1 - Put membership number in custom payroll report -& separate employee name as First Name, Middle Name and Last Name.
            dsList = objMembership.getListForCombo("List", True)
            With cboMembership
                .ValueMember = "membershipunkid"
                .DisplayMember = "name"
                .DataSource = dsList.Tables(0)
                .SelectedValue = 0
            End With
            'Sohail (10 Jul 2019) -- End

            'Pinkal (31-Mar-2023) -- Start
            '[JIRA] (A1X-676) Sissalana - Custom Payroll Report Modification to include Employee Identity info.
            dsList = objIdentity.getComboList(clsCommon_Master.enCommonMaster.IDENTITY_TYPES, True, "List")
            With cboIdentity
                .ValueMember = "masterunkid"
                .DisplayMember = "name"
                .DataSource = dsList.Tables(0)
                .SelectedValue = 0
            End With
            'Pinkal (31-Mar-2023) -- End


            dsList = Nothing

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "FillCombo", mstrModuleName)
            'Sohail (10 Jul 2019) -- Start
            'Good Neighbours Enhancement - Support Issue Id # 3876 - 76.1 - Put membership number in custom payroll report -& separate employee name as First Name, Middle Name and Last Name.
        Finally
            objMembership = Nothing
            'Sohail (10 Jul 2019) -- End
        End Try
    End Sub

    'Sohail (29 Apr 2014) -- Start
    'Enhancement - Export Excel Report with Company Logo 
    Private Sub FillList()
        Dim objTranHead As New clsTransactionHead
        Dim dsList As DataSet
        Try
            Dim lvItem As ListViewItem
            lvTranHead.Items.Clear()
            Dim lvArray As New List(Of ListViewItem)
            lvTranHead.BeginUpdate()

            'Sohail (21 Aug 2015) -- Start
            'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
            'dsList = objTranHead.getComboList("Heads", False, , , , , , "(typeof_id IN (" & enTypeOf.Allowance & "," & enTypeOf.Other_Earnings & ") OR trnheadtype_id  IN (" & enTranHeadType.DeductionForEmployee & "))")
            dsList = objTranHead.getComboList(FinancialYear._Object._DatabaseName, "Heads", False, , , , , , "(typeof_id IN (" & enTypeOf.Allowance & "," & enTypeOf.Other_Earnings & ") OR trnheadtype_id  IN (" & enTranHeadType.DeductionForEmployee & "))")
            'Sohail (21 Aug 2015) -- End

            For Each dsRow As DataRow In dsList.Tables("Heads").Rows
                lvItem = New ListViewItem

                lvItem.Text = ""
                lvItem.Tag = CInt(dsRow.Item("tranheadunkid"))

                lvItem.SubItems.Add(dsRow.Item("code").ToString)
                lvItem.SubItems.Add(dsRow.Item("name").ToString)

                lvArray.Add(lvItem)
            Next

            RemoveHandler lvTranHead.ItemChecked, AddressOf lvTranHead_ItemChecked
            lvTranHead.Items.AddRange(lvArray.ToArray)
            AddHandler lvTranHead.ItemChecked, AddressOf lvTranHead_ItemChecked

            If lvTranHead.Items.Count > 10 Then
                colhName.Width = 200 - 18
            Else
                colhName.Width = 200
            End If
            lvArray = Nothing
            lvTranHead.EndUpdate()

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "FillList", mstrModuleName)
        End Try
    End Sub

    Private Sub CheckAllHeads(ByVal blnCheckAll As Boolean)
        Try
            For Each lvItem As ListViewItem In lvTranHead.Items
                RemoveHandler lvTranHead.ItemChecked, AddressOf lvTranHead_ItemChecked 'Sohail (03 Nov 2010)
                lvItem.Checked = blnCheckAll
                AddHandler lvTranHead.ItemChecked, AddressOf lvTranHead_ItemChecked 'Sohail (03 Nov 2010)
            Next
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "CheckAllHeads", mstrModuleName)
        End Try
    End Sub
    'Sohail (29 Apr 2014) -- End

    Private Sub ResetValue()
        Try

            'Sohail (07 Aug 2013) -- Start
            'TRA - ENHANCEMENT
            objPayroll.setDefaultOrderBy(0)
            txtOrderBy.Text = objPayroll.OrderByDisplay
            'Sohail (07 Aug 2013) -- End

            cboEmployee.SelectedValue = 0
            'Sohail (08 Dec 2012) -- Start
            'TRA - ENHANCEMENT
            'cboPeriod.SelectedValue = 0
            cboPeriod.SelectedValue = mintFirstOpenPeriod
            'Sohail (08 Dec 2012) -- End

            'S.SANDEEP [ 17 AUG 2011 ] -- START
            'ENHANCEMENT : BRANCH ( STATION )INCLUSION IN FILTER
            cboBranch.SelectedValue = 0
            'S.SANDEEP [ 17 AUG 2011 ] -- END 


            'Pinkal (24-Jun-2011) -- Start
            'ISSUE : INCLUSION OF INACTIVE EMPLOYEE
            chkInactiveemp.Checked = False
            'Pinkal (24-Jun-2011) -- End

            'Sohail (26 Nov 2011) -- Start
            mstrStringIds = ""
            mstrStringName = ""
            mintViewIdx = -1
            mstrAnalysis_Fields = ""
            mstrAnalysis_Join = ""
            mstrAnalysis_OrderBy = ""
            mstrReport_GroupName = ""
            'Sohail (26 Nov 2011) -- End


            'Pinkal (05-Mar-2012) -- Start
            'Enhancement : TRA Changes
            chkIgnorezeroHead.Checked = True
            'Pinkal (05-Mar-2012) -- End

            'Sohail (05 Apr 2012) -- Start
            'TRA - ENHANCEMENT
            cboReportType.SelectedIndex = 0
            cboToPeriod.SelectedValue = 0
            'Sohail (05 Apr 2012) -- End

            'S.SANDEEP [ 11 SEP 2012 ] -- START
            'ENHANCEMENT : TRA CHANGES
            lblExRate.Text = ""
            mdecEx_Rate = 1
            mstrCurr_Sign = String.Empty
            'S.SANDEEP [ 11 SEP 2012 ] -- END
            mstrCurrency_Rate = "" 'Sohail (16 Mar 2013)

            'Pinkal (27-Feb-2013) -- Start
            'Enhancement : TRA Changes
            mstrAdvanceFilter = ""
            'Pinkal (27-Feb-2013) -- End

            objchkSelectAll.Checked = True 'Sohail (29 Apr 2014)
            chkShowPaymentDetails.Checked = False 'Sohail (20 Aug 2014)
            chkIncludeEmployerContribution.Checked = False 'Sohail (12 Sep 2014)
            'Sohail (03 Feb 2016) -- Start
            'Enhancement - Show Loans and Savings in Separate Columns option in Payroll Report.
            chkShowLoansInSeparateColumns.Checked = False
            chkShowSavingsInSeparateColumns.Checked = False
            'Sohail (03 Feb 2016) -- End

            'Hemant (31 Aug 2018) -- Start
            'Enhancement : Changes for Including Columns of Claims & Request Head in Custom Payroll Report
            chkShowCRInSeparateColumns.Checked = False
            'Hemant (31 Aug 2018)) -- End

            'Nilay (12-Oct-2016) -- Start
            'CCK Enhancements -  Salary Journal - To include cost center codes in custom payroll report
            txtCostCenterCode.Text = ""
            'Nilay (12-Oct-2016) -- End

            'Sohail (10 Jul 2019) -- Start
            'Good Neighbours Enhancement - Support Issue Id # 3876 - 76.1 - Put membership number in custom payroll report -& separate employee name as First Name, Middle Name and Last Name.
            cboMembership.SelectedValue = 0
            chkShowEmpNameinSeperateColumn.Checked = False
            'Sohail (10 Jul 2019) -- End

            'Pinkal (31-Mar-2023) -- Start
            '[JIRA] (A1X-676) Sissalana - Custom Payroll Report Modification to include Employee Identity info.
            cboIdentity.SelectedValue = 0
            'Pinkal (31-Mar-2023) -- End

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "ResetValue", mstrModuleName)
        End Try
    End Sub

    'S.SANDEEP [ 29 SEP 2014 ] -- START
    Private Sub FillCustomHeads()
        Dim objTranHead As New clsTransactionHead
        Dim dsList As DataSet = Nothing
        Try
            Dim dtTable As New DataTable
            Dim lvItem As ListViewItem
            'Shani [ 22 NOV 2014 ] -- START
            'FDRC Enhancemwnt - Provide Transaction Head Type filter on Custom Payroll Report.
            'lvTranHead.Items.Clear()
            lvCustomTranHead.Items.Clear()
            'Shani [ 22 NOV 2014 ] -- END

            'Sohail (20 Apr 2015) -- Start
            'KVTC Enhancement - New Report Custom Payroll Report Template 2 in Payroll Report. (Email From: Rutta Anatory, Subject: KVTC, Sent: Monday, March 02, 2015 1:13 PM)
            ConfigParameter._Object.Refresh()
            marrCustomPayrollReportHeadsIds.Clear()
            If cboReportType.SelectedIndex = 4 Then 'Custom Payroll Report Template 2 
                If ConfigParameter._Object._CustomPayrollReportTemplate2HeadsIds.Trim <> "" Then
                    marrCustomPayrollReportHeadsIds.AddRange(ConfigParameter._Object._CustomPayrollReportTemplate2HeadsIds.Split(","))
                End If
            Else 'Custom Payroll Report
                If ConfigParameter._Object._CustomPayrollReportHeadsIds.Trim <> "" Then
                    marrCustomPayrollReportHeadsIds.AddRange(ConfigParameter._Object._CustomPayrollReportHeadsIds.Split(","))
                End If
            End If
            'Sohail (20 Apr 2015) -- End

            Dim lvArray As New List(Of ListViewItem)
            lvCustomTranHead.BeginUpdate()
            'Shani [ 22 NOV 2014 ] -- START
            'FDRC Enhancemwnt - Provide Transaction Head Type filter on Custom Payroll Report.
            'Dim xHeads() As String = ConfigParameter._Object._CustomPayrollReportHeadsIds.Split(",")
            'If ConfigParameter._Object._CustomPayrollReportHeadsIds.Trim.Length > 0 Then
            '    Dim xFilter As String = String.Empty
            '    For Each xVal As Integer In ConfigParameter._Object._CustomPayrollReportHeadsIds.Split(",")
            '        xFilter = "" : xFilter = "prtranhead_master.tranheadunkid = '" & xVal & "' "
            '        dsList = objTranHead.getComboList("Heads", False, , , , , , xFilter)
            '        dtTable.Merge(dsList.Tables(0), True)
            '    Next
            '    xFilter = "prtranhead_master.tranheadunkid NOT IN(" & ConfigParameter._Object._CustomPayrollReportHeadsIds.Trim & ")"
            '    dsList = objTranHead.getComboList("Heads", False, , , , , , xFilter)
            '    dtTable.Merge(dsList.Tables(0), True)
            'Else
            '    dsList = objTranHead.getComboList("Heads", False)
            '    dtTable.Merge(dsList.Tables(0), True)
            'End If
            Dim ds As DataSet
            'Sohail (03 Feb 2016) -- Start
            'Enhancement - Show Loans and Savings in Separate Columns option in Payroll Report.
            Dim objLoan As New clsLoan_Scheme
            Dim objSaving As New clsSavingScheme
            'Hemant (31 Aug 2018) -- Start
            'Enhancement : Changes for Including Columns of Claims & Request Head in Custom Payroll Report
            Dim objExpense As New clsExpense_Master
            'Hemant (31 Aug 2018)) -- End
            Dim dsLoanIN As DataSet = Nothing
            Dim dsLoanNotIN As DataSet = Nothing
            Dim dsSavingIN As DataSet = Nothing
            Dim dsSavingNotIN As DataSet = Nothing
            'Hemant (31 Aug 2018) -- Start
            'Enhancement : Changes for Including Columns of Claims & Request Head in Custom Payroll Report
            Dim dsCRNotIN As DataSet = Nothing
            Dim dsCRIN As DataSet = Nothing
            'Hemant (31 Aug 2018)) -- End
            
            Dim arrHead() As String = (From p As String In marrCustomPayrollReportHeadsIds Where (IsNumeric(p)) Select (p.ToString)).ToArray
            Dim arrLoan() As String = (From p As String In marrCustomPayrollReportHeadsIds Where (p.StartsWith("Loan")) Select (p.Substring(4).ToString)).ToArray
            Dim arrAdvance() As String = (From p As String In marrCustomPayrollReportHeadsIds Where (p.StartsWith("Advance")) Select (p.Substring(7).ToString)).ToArray
            Dim arrSaving() As String = (From p As String In marrCustomPayrollReportHeadsIds Where (p.StartsWith("Saving")) Select (p.Substring(6).ToString)).ToArray

            'Hemant (31 Aug 2018) -- Start
            'Enhancement : Changes for Including Columns of Claims & Request Head in Custom Payroll Report
            Dim arrCR() As String = (From p As String In marrCustomPayrollReportHeadsIds Where (p.StartsWith("CR")) Select (p.Substring(8).ToString)).ToArray
            'Hemant (31 Aug 2018)) -- End


            Dim aLoan As New Dictionary(Of String, DataRow)
            Dim aAdvance As New Dictionary(Of String, DataRow)
            Dim aSaving As New Dictionary(Of String, DataRow)
            'Hemant (31 Aug 2018) -- Start
            'Enhancement : Changes for Including Columns of Claims & Request Head in Custom Payroll Report
            Dim aCR As New Dictionary(Of String, DataRow)
            'Hemant (31 Aug 2018)) -- End


            'Sohail (03 Feb 2016) -- End
            'Sohail (20 Apr 2015) -- Start
            'KVTC Enhancement - New Report Custom Payroll Report Template 2 in Payroll Report. (Email From: Rutta Anatory, Subject: KVTC, Sent: Monday, March 02, 2015 1:13 PM)
            'If marrCustomPayrollReportHeadsIds.Count > 0 Then
            '    dsList = objTranHead.getComboList("Heads", False, CInt(cboTrnHeadType.SelectedValue), , , , , "prtranhead_master.tranheadunkid NOT IN(" & String.Join(",", marrCustomPayrollReportHeadsIds.ToArray(Type.GetType("System.String"))) & ")")
            '    ds = objTranHead.getComboList("Heads", False, , , , , , "prtranhead_master.tranheadunkid IN(" & String.Join(",", marrCustomPayrollReportHeadsIds.ToArray(Type.GetType("System.String"))) & ")")
            'Else
            '    dsList = objTranHead.getComboList("Heads", False, CInt(cboTrnHeadType.SelectedValue), , , , , )
            '    ds = objTranHead.getComboList("Heads", False, , , , , , )
            'End If
            'Sohail (03 Feb 2016) -- Start
            'Enhancement - Show Loans and Savings in Separate Columns option in Payroll Report.
            'If marrCustomPayrollReportHeadsIds.Count > 0 Then

            'S.SANDEEP [04 JUN 2015] -- START
            'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
            'dsList = objTranHead.getComboList("Heads", False, CInt(cboTrnHeadType.SelectedValue), , , , , "prtranhead_master.tranheadunkid NOT IN(" & String.Join(",", marrCustomPayrollReportHeadsIds.ToArray(Type.GetType("System.String"))) & ")", , True, True)
            'ds = objTranHead.getComboList("Heads", False, , , , , , "prtranhead_master.tranheadunkid IN(" & String.Join(",", marrCustomPayrollReportHeadsIds.ToArray(Type.GetType("System.String"))) & ")", , True, True)

            'dsList = objTranHead.getComboList(FinancialYear._Object._DatabaseName, "Heads", False, CInt(cboTrnHeadType.SelectedValue), , , , , "prtranhead_master.tranheadunkid NOT IN(" & String.Join(",", marrCustomPayrollReportHeadsIds.ToArray(Type.GetType("System.String"))) & ")", True, True)
            'ds = objTranHead.getComboList(FinancialYear._Object._DatabaseName, "Heads", False, , , , , , "prtranhead_master.tranheadunkid IN(" & String.Join(",", marrCustomPayrollReportHeadsIds.ToArray(Type.GetType("System.String"))) & ")", True, True)
            'S.SANDEEP [04 JUN 2015] -- END

            'Else

            'S.SANDEEP [04 JUN 2015] -- START
            'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
            'dsList = objTranHead.getComboList("Heads", False, CInt(cboTrnHeadType.SelectedValue), , , , , , , True, True)
            'ds = objTranHead.getComboList("Heads", False, , , , , , , , True, True)

            'dsList = objTranHead.getComboList(FinancialYear._Object._DatabaseName, "Heads", False, CInt(cboTrnHeadType.SelectedValue), , , , , , True, True)
            'ds = objTranHead.getComboList(FinancialYear._Object._DatabaseName, "Heads", False, , , , , , , True, True)
            'S.SANDEEP [04 JUN 2015] -- END
            'End If
            Dim strNonHeadFilter As String = ""
            If CInt(cboTrnHeadType.SelectedValue) < 0 Then
                strNonHeadFilter = " AND 1 = 2 "
            End If
            'Transaction Heads
            If arrHead.Length > 0 Then
                'Sohail (18 Apr 2016) -- Start
                'Enhancement - 59.1 - NET PAY ROUNDING ADJUSTMENT head for the difference of actual Net Pay and Rounded Net Pay for HERON Portico.
                'dsList = objTranHead.getComboList(FinancialYear._Object._DatabaseName, "Heads", False, CInt(cboTrnHeadType.SelectedValue), , , , , "prtranhead_master.tranheadunkid NOT IN(" & String.Join(",", arrHead) & ")" & strNonHeadFilter, True, True)
                'ds = objTranHead.getComboList(FinancialYear._Object._DatabaseName, "Heads", False, , , , , , "prtranhead_master.tranheadunkid IN(" & String.Join(",", arrHead) & ")", True, True)
                dsList = objTranHead.getComboList(FinancialYear._Object._DatabaseName, "Heads", False, CInt(cboTrnHeadType.SelectedValue), , , , , "prtranhead_master.tranheadunkid NOT IN(" & String.Join(",", arrHead) & ")" & strNonHeadFilter, True, True, True, , , , True)
                'Hemant (03 Feb 2023) -- [blnAddLoanHeads:=True]
                ds = objTranHead.getComboList(FinancialYear._Object._DatabaseName, "Heads", False, , , , , , "prtranhead_master.tranheadunkid IN(" & String.Join(",", arrHead) & ")", True, True, True, , , , True)
                'Hemant (03 Feb 2023) -- [blnAddLoanHeads:=True]
                'Sohail (18 Apr 2016) -- End
            Else
                'Sohail (18 Apr 2016) -- Start
                'Enhancement - 59.1 - NET PAY ROUNDING ADJUSTMENT head for the difference of actual Net Pay and Rounded Net Pay for HERON Portico.
                'dsList = objTranHead.getComboList(FinancialYear._Object._DatabaseName, "Heads", False, CInt(cboTrnHeadType.SelectedValue), , , , , If(strNonHeadFilter.Trim <> "", strNonHeadFilter.Substring(4), ""), True, True)
                'ds = objTranHead.getComboList(FinancialYear._Object._DatabaseName, "Heads", False, , , , , , , True, True)
                dsList = objTranHead.getComboList(FinancialYear._Object._DatabaseName, "Heads", False, CInt(cboTrnHeadType.SelectedValue), , , , , If(strNonHeadFilter.Trim <> "", strNonHeadFilter.Substring(4), ""), True, True, True, , , , True)
                'Hemant (03 Feb 2023) -- [blnAddLoanHeads:=True]
                ds = objTranHead.getComboList(FinancialYear._Object._DatabaseName, "Heads", False, , , , , , , True, True, True, , , , True)
                'Hemant (03 Feb 2023) -- [blnAddLoanHeads:=True]
                'Sohail (18 Apr 2016) -- End
            End If
            Dim a As Dictionary(Of String, DataRow) = (From p In ds.Tables(0).AsEnumerable Select New With {.ID = p.Item("tranheadunkid").ToString, .DATAROW = p}).ToDictionary(Function(x) x.ID, Function(y) y.DATAROW)

            'Loan
            If arrLoan.Length > 0 Then
                dsLoanNotIN = objLoan.getComboList(False, "Loan", , "lnloan_scheme_master.loanschemeunkid NOT IN (" & String.Join(",", arrLoan) & ") ")
                dsLoanIN = objLoan.getComboList(False, "Loan", , "lnloan_scheme_master.loanschemeunkid IN (" & String.Join(",", arrLoan) & ") ")
                dsLoanNotIN.Tables(0).Columns("loanschemeunkid").ColumnName = "tranheadunkid"
                dsLoanNotIN.Tables(0).Columns("Code").ColumnName = "code"

                dsLoanIN.Tables(0).Columns("loanschemeunkid").ColumnName = "tranheadunkid"
                dsLoanIN.Tables(0).Columns("Code").ColumnName = "code"

                aLoan = (From p In dsLoanIN.Tables(0).AsEnumerable Select New With {.ID = p.Item("tranheadunkid").ToString, .DATAROW = p}).ToDictionary(Function(x) "Loan" + x.ID, Function(y) y.DATAROW)
                a = a.Union(aLoan).ToDictionary(Function(x) x.Key, Function(y) y.Value)
            ElseIf CInt(cboTrnHeadType.SelectedValue) = 0 OrElse CInt(cboTrnHeadType.SelectedValue) = -1 Then
                dsLoanNotIN = objLoan.getComboList(False, "Loan")
                dsLoanNotIN.Tables(0).Columns("loanschemeunkid").ColumnName = "tranheadunkid"
                dsLoanNotIN.Tables(0).Columns("Code").ColumnName = "code"
            End If

            'Saving
            If arrSaving.Length > 0 Then
                dsSavingNotIN = objSaving.getComboList(False, "Saving", , "svsavingscheme_master.savingschemeunkid NOT IN (" & String.Join(",", arrSaving) & ") ")
                dsSavingIN = objSaving.getComboList(False, "Saving", , "svsavingscheme_master.savingschemeunkid  IN (" & String.Join(",", arrSaving) & ") ")
                dsSavingNotIN.Tables(0).Columns("savingschemeunkid").ColumnName = "tranheadunkid"

                dsSavingIN.Tables(0).Columns("savingschemeunkid").ColumnName = "tranheadunkid"

                aSaving = (From p In dsSavingIN.Tables(0).AsEnumerable Select New With {.ID = p.Item("tranheadunkid").ToString, .DATAROW = p}).ToDictionary(Function(x) "Saving" + x.ID, Function(y) y.DATAROW)
                a = a.Union(aSaving).ToDictionary(Function(x) x.Key, Function(y) y.Value)
            ElseIf CInt(cboTrnHeadType.SelectedValue) = 0 OrElse CInt(cboTrnHeadType.SelectedValue) = -3 Then
                dsSavingNotIN = objSaving.getComboList(False, "Saving")
                dsSavingNotIN.Tables(0).Columns("savingschemeunkid").ColumnName = "tranheadunkid"
            End If

            'Hemant (31 Aug 2018) -- Start
            'Enhancement : Changes for Including Columns of Claims & Request Head in Custom Payroll Report
            'Claim & Request
            If arrCR.Length > 0 Then
                dsCRNotIN = objExpense.getComboList(-1, False, "CR", "cmclaim_request_master.crmasterunkid NOT IN (" & String.Join(",", arrCR) & ") ")
                dsCRIN = objExpense.getComboList(-1, False, "CR", , "cmclaim_request_master.crmasterunkid  IN (" & String.Join(",", arrCR) & ") ")
                dsCRNotIN.Tables(0).Columns("crmasterunkid").ColumnName = "tranheadunkid"

                dsCRIN.Tables(0).Columns("crmasterunkid").ColumnName = "tranheadunkid"

                aCR = (From p In dsCRNotIN.Tables(0).AsEnumerable Select New With {.ID = p.Item("tranheadunkid").ToString, .DATAROW = p}).ToDictionary(Function(x) "CR" + x.ID, Function(y) y.DATAROW)
                a = a.Union(aCR).ToDictionary(Function(x) x.Key, Function(y) y.Value)
            ElseIf CInt(cboTrnHeadType.SelectedValue) = 0 OrElse CInt(cboTrnHeadType.SelectedValue) = -3 Then
                dsCRNotIN = objExpense.getComboList(-1, False, "CR")
                dsCRNotIN.Tables(0).Columns("id").ColumnName = "tranheadunkid"
            End If
            'Hemant (31 Aug 2018)) -- End            

            If dsLoanNotIN IsNot Nothing Then
                Dim dc As New DataColumn("trnheadtype_id", System.Type.GetType("System.Int32"))
                dc.DefaultValue = -1
                dsLoanNotIN.Tables(0).Columns.Add(dc)
                dsLoanNotIN.Tables(0).Columns("code").SetOrdinal(1)
            End If

            If dsLoanIN IsNot Nothing Then
                Dim dc1 As New DataColumn("trnheadtype_id", System.Type.GetType("System.Int32"))
                dc1.DefaultValue = -1
                dsLoanIN.Tables(0).Columns.Add(dc1)
                dsLoanIN.Tables(0).Columns("code").SetOrdinal(1)
            End If

            If dsSavingNotIN IsNot Nothing Then
                Dim dc3 As New DataColumn("trnheadtype_id", System.Type.GetType("System.Int32"))
                dc3.DefaultValue = -3
                dsSavingNotIN.Tables(0).Columns.Add(dc3)
                dsSavingNotIN.Tables(0).Columns("code").SetOrdinal(1)
            End If

            If dsSavingIN IsNot Nothing Then
                Dim dc4 As New DataColumn("trnheadtype_id", System.Type.GetType("System.Int32"))
                dc4.DefaultValue = -3
                dsSavingIN.Tables(0).Columns.Add(dc4)
                dsSavingIN.Tables(0).Columns("code").SetOrdinal(1)
            End If

            'Hemant (31 Aug 2018) -- Start
            'Enhancement : Changes for Including Columns of Claims & Request Head in Custom Payroll Report
            If dsCRNotIN IsNot Nothing Then
                Dim dc5 As New DataColumn("trnheadtype_id", System.Type.GetType("System.Int32"))
                dc5.DefaultValue = -4
                dsCRNotIN.Tables(0).Columns.Add(dc5)
                dsCRNotIN.Tables(0).Columns("code").SetOrdinal(1)
            End If

            If dsCRIN IsNot Nothing Then
                Dim dc6 As New DataColumn("trnheadtype_id", System.Type.GetType("System.Int32"))
                dc6.DefaultValue = -4
                dsCRIN.Tables(0).Columns.Add(dc6)
                dsCRIN.Tables(0).Columns("code").SetOrdinal(1)
            End If
            'Hemant (31 Aug 2018)) -- End

            'Sohail (03 Feb 2016) -- End
            'Sohail (20 Apr 2015) -- End
            'Dim a As Dictionary(Of Integer, DataRow) = (From p In ds.Tables(0).AsEnumerable Select New With {.ID = CInt(p.Item("tranheadunkid")), .DATAROW = p}).ToDictionary(Function(x) CInt(x.ID), Function(y) y.DATAROW) 'Sohail (03 Feb 2016)

            dtTable.Merge(dsList.Tables(0), True)
            'Sohail (03 Feb 2016) -- Start
            'Enhancement - Show Loans and Savings in Separate Columns option in Payroll Report.
            If dsLoanNotIN IsNot Nothing Then
                If CInt(cboTrnHeadType.SelectedValue) = 0 OrElse CInt(cboTrnHeadType.SelectedValue) = -1 Then
                    dtTable.Merge(dsLoanNotIN.Tables(0), True)
                End If
            End If

            If dsSavingNotIN IsNot Nothing Then
                If CInt(cboTrnHeadType.SelectedValue) = 0 OrElse CInt(cboTrnHeadType.SelectedValue) = -3 Then
                    dtTable.Merge(dsSavingNotIN.Tables(0), True)
                End If
            End If

            'Hemant (31 Aug 2018) -- Start
            'Enhancement : Changes for Including Columns of Claims & Request Head in Custom Payroll Report
            If dsCRNotIN IsNot Nothing Then
                If CInt(cboTrnHeadType.SelectedValue) = 0 OrElse CInt(cboTrnHeadType.SelectedValue) = -4 Then
                    dtTable.Merge(dsCRNotIN.Tables(0), True)
                End If
            End If
            'Hemant (31 Aug 2018)) -- End

            'Advance
            Dim dr_Advance As DataRow = dtTable.NewRow
            dr_Advance.Item("tranheadunkid") = 1
            dr_Advance.Item("code") = Language.getMessage(mstrModuleName, 16, "Advance")
            dr_Advance.Item("name") = Language.getMessage(mstrModuleName, 16, "Advance")
            dr_Advance.Item("trnheadtype_id") = -2

            If arrAdvance.Length > 0 Then
                a.Add("Advance1", dr_Advance)
            ElseIf CInt(cboTrnHeadType.SelectedValue) = 0 OrElse CInt(cboTrnHeadType.SelectedValue) = -2 Then
                dtTable.Rows.Add(dr_Advance)
            End If

            dtTable = New DataView(dtTable, "", "name", DataViewRowState.CurrentRows).ToTable
            'Sohail (03 Feb 2016) -- End
            Dim intPos As Integer = 0
            For Each itm In marrCustomPayrollReportHeadsIds
                'Sohail (13 Jan 2020) -- Start
                'NMB Issue # : The given key was not present in the dicionary.
                If a.ContainsKey(itm.ToString) = False Then Continue For
                'Sohail (13 Jan 2020) -- End
                Dim dr As DataRow = dtTable.NewRow
                'Sohail (03 Feb 2016) -- Start
                'Enhancement - Show Loans and Savings in Separate Columns option in Payroll Report.
                'dr.ItemArray = a.Item(CInt(itm)).ItemArray
                dr.ItemArray = a.Item(itm).ItemArray
                'Sohail (03 Feb 2016) -- End
                dtTable.Rows.InsertAt(dr, intPos)
                intPos += 1
            Next
            'For Each dsRow As DataRow In ds.Tables(0).Rows
            '    Dim dr As DataRow = dtTable.NewRow
            '    dr.ItemArray = dsRow.ItemArray
            '    dtTable.Rows.InsertAt(dr, intPos)
            '    intPos += 1
            'Next
            'Shani [ 22 NOV 2014 ] -- END

            For Each dsRow As DataRow In dtTable.Rows
                lvItem = New ListViewItem

                lvItem.Text = ""
                lvItem.Tag = CInt(dsRow.Item("tranheadunkid"))

                lvItem.SubItems.Add(dsRow.Item("code").ToString)
                lvItem.SubItems.Add(dsRow.Item("name").ToString)
                'Sohail (03 Feb 2016) -- Start
                'Enhancement - Show Loans and Savings in Separate Columns option in Payroll Report.
                lvItem.SubItems.Add(dsRow.Item("trnheadtype_id").ToString)
                'Sohail (03 Feb 2016) -- End
                'Shani [ 22 NOV 2014 ] -- START
                'FDRC Enhancemwnt - Provide Transaction Head Type filter on Custom Payroll Report.
                'If xHeads IsNot Nothing Then
                '    If Array.IndexOf(xHeads, dsRow.Item("tranheadunkid").ToString) >= 0 Then
                '        lvItem.Checked = True
                '    End If
                'Else
                '    lvItem.Checked = True
                'End If
                'Sohail (03 Feb 2016) -- Start
                'Enhancement - Show Loans and Savings in Separate Columns option in Payroll Report.
                'If marrCustomPayrollReportHeadsIds.Contains(dsRow.Item("tranheadunkid").ToString) = True Then
                '   lvItem.Checked = True
                If arrHead.Contains(dsRow.Item("tranheadunkid").ToString) = True AndAlso CInt(dsRow.Item("trnheadtype_id")) > 0 Then 'Transaction heads
                    lvItem.Checked = True
                ElseIf arrLoan.Contains(dsRow.Item("tranheadunkid").ToString) = True AndAlso CInt(dsRow.Item("trnheadtype_id")) = -1 Then 'Loan
                    lvItem.Checked = True
                ElseIf arrAdvance.Contains(dsRow.Item("tranheadunkid").ToString) = True AndAlso CInt(dsRow.Item("trnheadtype_id")) = -2 Then 'Advance
                    lvItem.Checked = True
                ElseIf arrSaving.Contains(dsRow.Item("tranheadunkid").ToString) = True AndAlso CInt(dsRow.Item("trnheadtype_id")) = -3 Then 'Saving
                    lvItem.Checked = True
                    'Sohail (03 Feb 2016) -- End
                Else
                    lvItem.Checked = False
                End If
                'Shani [ 22 NOV 2014 ] -- END
                lvArray.Add(lvItem)
            Next

            RemoveHandler lvCustomTranHead.ItemChecked, AddressOf lvCutomTranHead_ItemChecked
            lvCustomTranHead.Items.AddRange(lvArray.ToArray)
            AddHandler lvCustomTranHead.ItemChecked, AddressOf lvCutomTranHead_ItemChecked

            RemoveHandler objchkCheckAll.CheckedChanged, AddressOf objchkCheckAll_CheckedChanged
            If lvCustomTranHead.CheckedItems.Count = lvCustomTranHead.Items.Count Then
                objchkCheckAll.CheckState = CheckState.Checked
            ElseIf lvCustomTranHead.CheckedItems.Count < lvCustomTranHead.Items.Count Then
                objchkCheckAll.CheckState = CheckState.Indeterminate
            ElseIf lvCustomTranHead.CheckedItems.Count <= 0 Then
                objchkCheckAll.CheckState = CheckState.Unchecked
            End If
            AddHandler objchkCheckAll.CheckedChanged, AddressOf objchkCheckAll_CheckedChanged

            If lvCustomTranHead.Items.Count > 15 Then
                colhCName.Width = 240 - 20
            Else
                colhCName.Width = 240
            End If
            lvArray = Nothing
            'lvCustomTranHead.EndUpdate() 'Sohail (03 Feb 2016)

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "FillCustomHeads", mstrModuleName)
            'Sohail (03 Feb 2016) -- Start
            'Enhancement - Show Loans and Savings in Separate Columns option in Payroll Report.
        Finally
            lvCustomTranHead.EndUpdate()
            'Sohail (03 Feb 2016) -- End
        End Try
    End Sub

    Private Sub Fill_Allocation()
        Try
            RemoveHandler objchkAllocation.CheckedChanged, AddressOf objchkAllocation_CheckedChanged
            RemoveHandler lvAllocation_Hierarchy.ItemChecked, AddressOf lvAllocation_Hierarchy_ItemChecked

            Dim dsList As New DataSet
            Dim dtTable As New DataTable
            'Sohail (20 Apr 2015) -- Start
            'KVTC Enhancement - New Report Custom Payroll Report Template 2 in Payroll Report. (Email From: Rutta Anatory, Subject: KVTC, Sent: Monday, March 02, 2015 1:13 PM)
            'Dim xAlloc() As String = ConfigParameter._Object._CustomPayrollReportAllocIds.Split(",")
            'If ConfigParameter._Object._CustomPayrollReportAllocIds.Trim.Length > 0 Then
            '    Dim xFilter As String = String.Empty
            '    For Each xVal As Integer In ConfigParameter._Object._CustomPayrollReportAllocIds.Split(",")
            '        xFilter = "" : xFilter = "Id = '" & xVal & "' "
            '        dsList = objPayroll.GetAllocationList(xFilter)
            '        dtTable.Merge(dsList.Tables(0), True)
            '    Next
            '    xFilter = "Id NOT IN(" & ConfigParameter._Object._CustomPayrollReportAllocIds.Trim & ")"
            '    dsList = objPayroll.GetAllocationList(xFilter)
            '    dtTable.Merge(dsList.Tables(0), True)
            'Else
            '    dsList = objPayroll.GetAllocationList()
            '    dtTable.Merge(dsList.Tables(0), True)
            'End If
            Dim marrAlloc As New ArrayList
            If cboReportType.SelectedIndex = 4 Then 'Custom Payroll Report Template 2
                If ConfigParameter._Object._CustomPayrollReportTemplate2AllocIds.Trim <> "" Then
                    marrAlloc.AddRange(ConfigParameter._Object._CustomPayrollReportTemplate2AllocIds.Split(","))
                End If
            Else 'Custom Payroll Report
                If ConfigParameter._Object._CustomPayrollReportAllocIds.Trim <> "" Then
                    marrAlloc.AddRange(ConfigParameter._Object._CustomPayrollReportAllocIds.Split(","))
                End If
            End If

            If marrAlloc.Count > 0 Then
                dsList = objPayroll.GetAllocationList("Id NOT IN(" & String.Join(",", marrAlloc.ToArray(Type.GetType("System.String"))) & ")")
                dtTable.Merge(dsList.Tables(0), True)

                dsList = objPayroll.GetAllocationList("Id IN (" & String.Join(",", marrAlloc.ToArray(Type.GetType("System.String"))) & ")")
                Dim intPos As Integer = 0
                For Each dRow As DataRow In dsList.Tables(0).Rows
                    Dim dr As DataRow = dtTable.NewRow
                    dr.ItemArray = dRow.ItemArray
                    dtTable.Rows.InsertAt(dr, intPos)
                    intPos += 1
                Next
            Else
                dsList = objPayroll.GetAllocationList()
                dtTable.Merge(dsList.Tables(0), True)
            End If
            'Sohail (20 Apr 2015) -- End

            lvAllocation_Hierarchy.Items.Clear()

            For Each dRow As DataRow In dtTable.Rows
                Dim lvItem As New ListViewItem

                lvItem.Text = ""
                lvItem.SubItems.Add(dRow.Item("Name").ToString)
                lvItem.Tag = dRow.Item("Id").ToString

                'Sohail (20 Apr 2015) -- Start
                'KVTC Enhancement - New Report Custom Payroll Report Template 2 in Payroll Report. (Email From: Rutta Anatory, Subject: KVTC, Sent: Monday, March 02, 2015 1:13 PM)
                'If xAlloc IsNot Nothing Then
                '    If Array.IndexOf(xAlloc, dRow.Item("Id").ToString) >= 0 Then
                '        lvItem.Checked = True
                '    End If
                'End If
                If marrAlloc.Contains(dRow.Item("Id").ToString) = True Then
                    lvItem.Checked = True
                Else
                    lvItem.Checked = False
                End If
                'Sohail (20 Apr 2015) -- End

                lvAllocation_Hierarchy.Items.Add(lvItem)
            Next

            If lvAllocation_Hierarchy.Items.Count > 5 Then
                colhAllocation.Width = 300 - 20
            Else
                colhAllocation.Width = 300
            End If

            If lvAllocation_Hierarchy.CheckedItems.Count = lvAllocation_Hierarchy.Items.Count Then
                objchkAllocation.CheckState = CheckState.Checked
            ElseIf lvAllocation_Hierarchy.CheckedItems.Count < lvAllocation_Hierarchy.Items.Count Then
                objchkAllocation.CheckState = CheckState.Indeterminate
            ElseIf lvAllocation_Hierarchy.CheckedItems.Count <= 0 Then
                objchkAllocation.CheckState = CheckState.Unchecked
            End If

            AddHandler objchkAllocation.CheckedChanged, AddressOf objchkAllocation_CheckedChanged
            AddHandler lvAllocation_Hierarchy.ItemChecked, AddressOf lvAllocation_Hierarchy_ItemChecked

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "Fill_Allocation", mstrModuleName)
        Finally
        End Try
    End Sub
    'S.SANDEEP [ 29 SEP 2014 ] -- END

#End Region

#Region " Form's Events "

    Private Sub frmPayrollReport_FormClosed(ByVal sender As Object, ByVal e As System.Windows.Forms.FormClosedEventArgs) Handles Me.FormClosed
        Try
            objPayroll = Nothing
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmPayrollReport_FormClosed", mstrModuleName)
        End Try
    End Sub

    Private Sub frmPayrollReport_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            Call Language.setLanguage(Me.Name)

            'Pinkal (03-Sep-2012) -- Start
            'Enhancement : TRA Changes
            OtherSettings()
            'Pinkal (03-Sep-2012) -- End

            'Shani [ 22 NOV 2014 ] -- START
            'FDRC Enhancemwnt - Provide Transaction Head Type filter on Custom Payroll Report.
            marrCustomPayrollReportHeadsIds.Clear()
            If ConfigParameter._Object._CustomPayrollReportHeadsIds.Trim <> "" Then
                marrCustomPayrollReportHeadsIds.AddRange(ConfigParameter._Object._CustomPayrollReportHeadsIds.Split(","))
            End If
            'Shani [ 22 NOV 2014 ] -- END

            chkIgnorezeroHead.Checked = True
            Call FillCombo()
            Call FillList() 'Sohail (29 Apr 2014)
            Call ResetValue()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmPayrollReport_Load", mstrModuleName)
        End Try
    End Sub

    Private Sub Form_KeyDown(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles Me.KeyDown
        Try
            If e.Control Then
                If e.KeyCode = Windows.Forms.Keys.E Then
                    Call btnExport_Click(sender, e)
                End If
            End If
        Catch ex As Exception
            DisplayError.Show(-1, ex.Message, "Form_KeyDown", mstrModuleName)
        End Try
    End Sub

    Private Sub Form_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles Me.KeyPress
        Try
            Select Case e.KeyChar
                Case CChar(ChrW(CInt(Windows.Forms.Keys.Enter)))
                    Windows.Forms.SendKeys.Send("{TAB}")
                    e.Handled = True
                    Exit Select
            End Select
        Catch ex As Exception
            DisplayError.Show(-1, ex.Message, "Form_KeyPress", mstrModuleName)
        End Try
    End Sub

#End Region

#Region " Buttons "

    Private Sub btnClose_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnClose.Click
        Try
            Me.Close()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "btnClose_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub btnExport_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnExport.Click
        'Sohail (20 Aug 2014) -- Start
        'Enhancement - Show Payment Details on Payroll Report and ED Spreadsheet Report.
        Dim objCompany As New clsCompany_Master
        Dim dsList As DataSet
        Dim arrDatabaseName As New ArrayList
        Dim intPrevYearID As Integer = 0
        'Sohail (20 Aug 2014) -- End
        'Sohail (21 Aug 2015) -- Start
        'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
        Dim mdtPeriod_Start As Date
        Dim mdtPeriod_End As Date
        'Sohail (21 Aug 2015) -- End
        Dim mdicYearDBName As New Dictionary(Of Integer, String) 'Sohail (21 Aug 2015)
        Try
            If cboPeriod.SelectedValue <= 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 4, "Period is compulsory infomation. Please select period to continue."), enMsgBoxStyle.Information)
                cboPeriod.Focus()
                Exit Sub
            End If


            'Anjan [27 February 2015] -- Start
            'Issue : Aga Khan wanted to export reports on local computer and not on cloud server.
            'Sohail (25 Mar 2013) -- Start
            'TRA - ENHANCEMENT
            'If ConfigParameter._Object._ExportReportPath = "" Then
            '    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 8, "Please set the Export Report Path form Aruti Configuration -> Option -> Paths."), enMsgBoxStyle.Information)
            '    Exit Sub
            'End If
            'Sohail (25 Mar 2013) -- End
            'Anjan [27 February 2015] -- End



            'Sohail (05 Apr 2012) -- Start
            'TRA - ENHANCEMENT
            'Sohail (01 Sep 2014) -- Start
            'Enhancement - New Report Type Employee Payroll Summary in Payroll Report.
            'If cboReportType.SelectedIndex = 1 Then
            If cboReportType.SelectedIndex = 1 OrElse cboReportType.SelectedIndex = 2 Then
                'Sohail (01 Sep 2014) -- End
                'Sohail (01 Sep 2014) -- Start
                'Enhancement - New Report Type Employee Payroll Summary in Payroll Report.
                'If CInt(cboEmployee.SelectedValue) <= 0 Then
                If cboReportType.SelectedIndex = 1 AndAlso CInt(cboEmployee.SelectedValue) <= 0 Then
                    'Sohail (01 Sep 2014) -- End
                    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 5, "Employee is compulsory infomation. Please select Employee to continue."), enMsgBoxStyle.Information)
                    cboEmployee.Focus()
                    Exit Sub
                ElseIf CInt(cboToPeriod.SelectedValue) <= 0 Then
                    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 4, "Period is compulsory infomation. Please select period to continue."), enMsgBoxStyle.Information)
                    cboToPeriod.Focus()
                    Exit Sub
                ElseIf cboToPeriod.SelectedIndex < cboPeriod.SelectedIndex Then
                    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 3, " To Period cannot be less than From Period"), enMsgBoxStyle.Information)
                    cboToPeriod.Focus()
                    Exit Sub
                End If
            End If
            'Sohail (05 Apr 2012) -- End


            'S.SANDEEP [ 11 SEP 2012 ] -- START
            'ENHANCEMENT : TRA CHANGES
            If mdecEx_Rate <= 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 7, "Sorry, No exchange rate defined for this currency for the period selected."), enMsgBoxStyle.Information)
                cboCurrency.Focus()
                Exit Sub
            End If
            'S.SANDEEP [ 11 SEP 2012 ] -- END


            objPayroll.SetDefaultValue()

            'Sohail (05 Apr 2012) -- Start
            'TRA - ENHANCEMENT
            objPayroll._ReportId = cboReportType.SelectedIndex
            objPayroll._ReportTypeName = cboReportType.Text
            'Sohail (08 Dec 2012) -- Start
            'TRA - ENHANCEMENT
            'objPayroll._ToPeriodId = CInt(cboToPeriod.SelectedValue)
            'objPayroll._ToPeriodName = cboToPeriod.Text
            'Sohail (01 Sep 2014) -- Start
            'Enhancement - New Report Type Employee Payroll Summary in Payroll Report.
            'If cboReportType.SelectedIndex = 1 Then 'Personal Salary Calculation Report
            If cboReportType.SelectedIndex = 1 OrElse cboReportType.SelectedIndex = 2 Then 'Personal Salary Calculation Report OR Employee Payroll Summary
                'Sohail (01 Sep 2014) -- End
                objPayroll._ToPeriodId = CInt(cboToPeriod.SelectedValue)
                objPayroll._ToPeriodName = cboToPeriod.Text
            Else
                objPayroll._ToPeriodId = 0
                objPayroll._ToPeriodName = ""
            End If
            'Sohail (08 Dec 2012) -- End

            Dim i As Integer = 0
            Dim strList As String = cboPeriod.SelectedValue.ToString
            'Sohail (20 Aug 2014) -- Start
            'Enhancement - Show Payment Details on Payroll Report and ED Spreadsheet Report.
            dsList = objCompany.GetFinancialYearList(Company._Object._Companyunkid, , "Database", CInt(CType(cboPeriod.SelectedItem, DataRowView).Item("yearunkid")))
            If dsList.Tables("Database").Rows.Count > 0 Then
                arrDatabaseName.Add(dsList.Tables("Database").Rows(0).Item("database_name").ToString)
                intPrevYearID = CInt(CType(cboPeriod.SelectedItem, DataRowView).Item("yearunkid"))
                'Sohail (21 Aug 2015) -- Start
                'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
                If mdicYearDBName.ContainsKey(intPrevYearID) = False Then
                    mdicYearDBName.Add(intPrevYearID, dsList.Tables("Database").Rows(0).Item("database_name").ToString)
                End If
                'Sohail (21 Aug 2015) -- End
            End If
            'Sohail (20 Aug 2014) -- End
            'For Each cbItem As DataRowView In cboPeriod.Items
            '    If i >= cboPeriod.SelectedIndex AndAlso i <= cboToPeriod.SelectedIndex Then
            '        strList &= ", " & cbItem.
            '    End If
            'Next
            'Sohail (01 Sep 2014) -- Start
            'Enhancement - New Report Type Employee Payroll Summary in Payroll Report.
            'If cboReportType.SelectedIndex = 1 Then 'Personal Salary Calculation Report
            If cboReportType.SelectedIndex = 1 OrElse cboReportType.SelectedIndex = 2 Then 'Personal Salary Calculation Report OR Employee Payroll Summary
                'Sohail (01 Sep 2014) -- End
                For Each dsRow As DataRow In CType(cboPeriod.DataSource, DataTable).Rows
                    If i > cboPeriod.SelectedIndex AndAlso i <= cboToPeriod.SelectedIndex Then
                        strList &= ", " & dsRow.Item("periodunkid").ToString
                        'Sohail (20 Aug 2014) -- Start
                        'Enhancement - Show Payment Details on Payroll Report and ED Spreadsheet Report.
                        If intPrevYearID <> CInt(dsRow.Item("yearunkid")) Then
                            dsList = objCompany.GetFinancialYearList(Company._Object._Companyunkid, , "Database", CInt(dsRow.Item("yearunkid")))
                            If dsList.Tables("Database").Rows.Count > 0 Then
                                arrDatabaseName.Add(dsList.Tables("Database").Rows(0).Item("database_name").ToString)
                                'Sohail (21 Aug 2015) -- Start
                                'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
                                If mdicYearDBName.ContainsKey(CInt(dsRow.Item("yearunkid"))) = False Then
                                    mdicYearDBName.Add(CInt(dsRow.Item("yearunkid")), dsList.Tables("Database").Rows(0).Item("database_name").ToString)
                                End If
                                'Sohail (21 Aug 2015) -- End
                            End If
                        End If
                        intPrevYearID = CInt(dsRow.Item("yearunkid"))
                        'Sohail (20 Aug 2014) -- End
                    End If
                    i += 1
                Next
            End If

            objPayroll._PeriodIdList = strList
            'Sohail (20 Aug 2014) -- Start
            'Enhancement - Show Payment Details on Payroll Report and ED Spreadsheet Report.
            objPayroll._Arr_DatabaseName = arrDatabaseName
            objPayroll._ShowPaymentDetails = chkShowPaymentDetails.Checked
            'Sohail (20 Aug 2014) -- End
            'Sohail (05 Apr 2012) -- End
            objPayroll._IncludeEmployerContribution = chkIncludeEmployerContribution.Checked 'Sohail (12 Sep 2014)

            objPayroll._EmployeeId = cboEmployee.SelectedValue
            objPayroll._EmployeeName = cboEmployee.Text

            objPayroll._PeriodId = cboPeriod.SelectedValue
            objPayroll._PeriodName = cboPeriod.Text


            'Pinkal (24-Jun-2011) -- Start
            'ISSUE : INCLUSION OF INACTIVE EMPLOYEE
            objPayroll._IsActive = chkInactiveemp.Checked
            'Pinkal (24-Jun-2011) -- End


            'S.SANDEEP [ 17 AUG 2011 ] -- START
            'ENHANCEMENT : BRANCH ( STATION )INCLUSION IN FILTER
            objPayroll._BranchId = cboBranch.SelectedValue
            objPayroll._Branch_Name = cboBranch.Text
            'S.SANDEEP [ 17 AUG 2011 ] -- END 

            'Sohail (26 Nov 2011) -- Start
            objPayroll._ViewByIds = mstrStringIds
            objPayroll._ViewIndex = mintViewIdx
            objPayroll._ViewByName = mstrStringName
            objPayroll._Analysis_Fields = mstrAnalysis_Fields
            objPayroll._Analysis_Join = mstrAnalysis_Join
            objPayroll._Analysis_OrderBy = mstrAnalysis_OrderBy
            objPayroll._Report_GroupName = mstrReport_GroupName
            'Sohail (26 Nov 2011) -- End


            'Pinkal (05-Mar-2012) -- Start
            'Enhancement : TRA Changes
            objPayroll._IgnoreZeroHeads = chkIgnorezeroHead.Checked
            'Pinkal (05-Mar-2012) -- End

            'Sohail (16 Apr 2012) -- Start
            'TRA - ENHANCEMENT
            Dim objPeriod As New clscommom_period_Tran
            'Sohail (21 Aug 2015) -- Start
            'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
            'objPeriod._Periodunkid = CInt(cboPeriod.SelectedValue)
            objPeriod._Periodunkid(FinancialYear._Object._DatabaseName) = CInt(cboPeriod.SelectedValue)
            'Sohail (21 Aug 2015) -- End
            objPayroll._PeriodStartDate = objPeriod._Start_Date
            'Sohail (21 Aug 2015) -- Start
            'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
            mdtPeriod_Start = objPeriod._Start_Date
            'Sohail (21 Aug 2015) -- End

            'Sohail (01 Sep 2014) -- Start
            'Enhancement - New Report Type Employee Payroll Summary in Payroll Report.
            'If cboReportType.SelectedIndex = 1 Then 'Personal Salary Calculation Report
            If cboReportType.SelectedIndex = 1 OrElse cboReportType.SelectedIndex = 2 Then 'Personal Salary Calculation Report
                'Sohail (01 Sep 2014) -- End
                'Sohail (21 Aug 2015) -- Start
                'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
                'objPeriod._Periodunkid = CInt(cboToPeriod.SelectedValue)
                objPeriod._Periodunkid(FinancialYear._Object._DatabaseName) = CInt(cboToPeriod.SelectedValue)
                'Sohail (21 Aug 2015) -- End
            End If
            objPayroll._PeriodEndDate = objPeriod._End_Date
            'Sohail (16 Apr 2012) -- End
            'Sohail (21 Aug 2015) -- Start
            'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
            mdtPeriod_End = objPeriod._End_Date
            'Sohail (21 Aug 2015) -- End

            'Sohail (03 Feb 2016) -- Start
            'Enhancement - Show Loans and Savings in Separate Columns option in Payroll Report.
            objPayroll._ShowLoansInSeparateColumns = chkShowLoansInSeparateColumns.Checked
            objPayroll._ShowSavingsInSeparateColumns = chkShowSavingsInSeparateColumns.Checked
            'Sohail (03 Feb 2016) -- End

            'Hemant (31 Aug 2018) -- Start
            'Enhancement : Changes for Including Columns of Claims & Request Head in Custom Payroll Report
            objPayroll._ShowCRInSeparateColumns = chkShowCRInSeparateColumns.Checked
            'Hemant (31 Aug 2018)) -- End

            'Sohail (29 Apr 2014) -- Start
            'Enhancement - Export Excel Report with Company Logo 
            Dim lstUnSelected As List(Of String) = (From lvItem In lvTranHead.Items.Cast(Of ListViewItem)() Where lvItem.Checked = False Select (lvItem.Tag.ToString)).ToList
            Dim strIDs As String = ""
            If lstUnSelected.Count > 0 Then
                strIDs = String.Join(",", lstUnSelected.ToArray)
            End If
            objPayroll._UnSelectedHeadIDs = strIDs
            'Sohail (29 Apr 2014) -- End

            'S.SANDEEP [ 11 SEP 2012 ] -- START
            'ENHANCEMENT : TRA CHANGES
            objPayroll._Ex_Rate = mdecEx_Rate
            objPayroll._Currency_Sign = mstrCurr_Sign
            'S.SANDEEP [ 11 SEP 2012 ] -- END
            objPayroll._Currency_Rate = mstrCurrency_Rate

            'Sohail (08 Dec 2012) -- Start
            'TRA - ENHANCEMENT
            objPayroll._FromDatabaseName = mstrFromDatabaseName
            'Sohail (01 Sep 2014) -- Start
            'Enhancement - New Report Type Employee Payroll Summary in Payroll Report.
            'If cboReportType.SelectedIndex = 1 Then 'Personal Salary Calculation Report
            If cboReportType.SelectedIndex = 1 OrElse cboReportType.SelectedIndex = 2 Then 'Personal Salary Calculation Report
                'Sohail (01 Sep 2014) -- End
                objPayroll._ToDatabaseName = mstrToDatabaseName
            Else
                objPayroll._ToDatabaseName = ""
            End If
            'Sohail (08 Dec 2012) -- End

            'Pinkal (27-Feb-2013) -- Start
            'Enhancement : TRA Changes
            objPayroll._Advance_Filter = mstrAdvanceFilter
            'Pinkal (27-Feb-2013) -- End

            'Sohail (13 Feb 2016) -- Start
            'Enhancement - Report was not coming from ESS in 58.1 due to UserUnkId = -1 passed in NewAccessLevelFilter method.
            objPayroll._ApplyUserAccessFilter = True
            'Sohail ((13 Feb 2016) -- End

            'Nilay (12-Oct-2016) -- Start
            'CCK Enhancements -  Salary Journal - To include cost center codes in custom payroll report
            If cboReportType.SelectedIndex = 4 Then
                objPayroll._CostCenterCode = txtCostCenterCode.Text.Trim
            End If
            'Nilay (12-Oct-2016) -- End

            'Sohail (10 Jul 2019) -- Start
            'Good Neighbours Enhancement - Support Issue Id # 3876 - 76.1 - Put membership number in custom payroll report -& separate employee name as First Name, Middle Name and Last Name.
            objPayroll._ShowEmpNameInSeprateColumns = chkShowEmpNameinSeperateColumn.Checked
            objPayroll._MembershipUnkId = CInt(cboMembership.SelectedValue)
            objPayroll._MembershipName = cboMembership.Text
            'Sohail (10 Jul 2019) -- End

            'Pinkal (31-Mar-2023) -- Start
            '[JIRA] (A1X-676) Sissalana - Custom Payroll Report Modification to include Employee Identity info.
            objPayroll._IdentityId = CInt(cboIdentity.SelectedValue)
            objPayroll._IndentityName = cboIdentity.Text
            'Pinkal (31-Mar-2023) -- End


            'Sohail (03 Aug 2019) -- Start
            'PAYTECH KENYA issue # 0004019 - 76.1 - Inaccurate data on payroll reports i.e payroll summary report When an employee is moved between departments/branches, payroll report captures only the latest transfer records regardless of the period when payroll was done. System should pick the allocation based on period of transfer without overriding the former transfer..
            objPayroll._Analysis_Join = mstrAnalysis_Join.Replace(ConfigParameter._Object._EmployeeAsOnDate, eZeeDate.convertDate(mdtPeriod_End))
            'Sohail (03 Aug 2019) -- End

            'S.SANDEEP [ 29 SEP 2014 ] -- START
            ''Sohail (29 Apr 2014) -- Start
            ''Enhancement - Export Excel Report with Company Logo 
            ''objPayroll.Generate_PayrollReport()
            'objPayroll.Generate_PayrollReport(True)
            ''Sohail (29 Apr 2014) -- End
            'Sohail (20 Apr 2015) -- Start
            'KVTC Enhancement - New Report Custom Payroll Report Template 2 in Payroll Report. (Email From: Rutta Anatory, Subject: KVTC, Sent: Monday, March 02, 2015 1:13 PM)
            'If cboReportType.SelectedIndex = 3 Then
            If cboReportType.SelectedIndex = 3 OrElse cboReportType.SelectedIndex = 4 Then
                'Sohail (20 Apr 2015) -- End
                If lvCustomTranHead.CheckedItems.Count <= 0 Then
                    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 12, "Sorry, Please select atleast one transaction head in order to generate report."), enMsgBoxStyle.Information)
                    Exit Sub
                End If

                Dim xCSVHeads As String = String.Empty
                'Sohail (03 Feb 2016) -- Start
                'Enhancement - Show Loans and Savings in Separate Columns option in Payroll Report.
                'xCSVHeads = String.Join(",", lvCustomTranHead.CheckedItems.Cast(Of ListViewItem)().[Select](Function(x) x.Tag.ToString()).ToArray())
                'Sohail (29 Mar 2017) -- Start
                'CCK Enhancement - 65.2 - On Custom payroll report, loans are showing after the TOTAL Deduction column. They should show before the total deduction column.
                'xCSVHeads = String.Join(",", (From p In lvCustomTranHead.CheckedItems.Cast(Of ListViewItem)() Where (CInt(p.SubItems(colhCHeadTypeID.Index).Text) > 0) Select (p.Tag.ToString)).ToArray _
                '                             .Union _
                '                             (From p In lvCustomTranHead.CheckedItems.Cast(Of ListViewItem)() Where (CInt(p.SubItems(colhCHeadTypeID.Index).Text) = -1) Select ("Loan" + p.Tag.ToString)).ToArray _
                '                             .Union _
                '                             (From p In lvCustomTranHead.CheckedItems.Cast(Of ListViewItem)() Where (CInt(p.SubItems(colhCHeadTypeID.Index).Text) = -2) Select ("Advance" + p.Tag.ToString)).ToArray _
                '                             .Union _
                '                             (From p In lvCustomTranHead.CheckedItems.Cast(Of ListViewItem)() Where (CInt(p.SubItems(colhCHeadTypeID.Index).Text) = -3) Select ("Saving" + p.Tag.ToString)).ToArray)
                xCSVHeads = String.Join(",", (From p In lvCustomTranHead.CheckedItems.Cast(Of ListViewItem)() Select (If(CInt(p.SubItems(colhCHeadTypeID.Index).Text) > 0, p.Tag.ToString, If(CInt(p.SubItems(colhCHeadTypeID.Index).Text) = -1, "Loan" + p.Tag.ToString,  If(CInt(p.SubItems(colhCHeadTypeID.Index).Text) = -2, "Advance" + p.Tag.ToString,  If(CInt(p.SubItems(colhCHeadTypeID.Index).Text) = -3, "Saving" + p.Tag.ToString,  If(CInt(p.SubItems(colhCHeadTypeID.Index).Text) = -4, "CR" + p.Tag.ToString,"").ToString).ToString).ToString).ToString).ToString)).ToArray)
                'Sohail (29 Mar 2017) -- End
                'Sohail (03 Feb 2016) -- End
                objPayroll._CustomReportHeadsIds = xCSVHeads

                Dim xAllocVal As New Dictionary(Of Integer, String)
                xAllocVal = lvAllocation_Hierarchy.CheckedItems.Cast(Of ListViewItem).ToDictionary(Function(x) CInt(x.Tag), Function(x) x.SubItems(colhAllocation.Index).Text)
                objPayroll._AllocationDetails = xAllocVal
                'Sohail (21 Aug 2015) -- Start
                'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
                'objPayroll.Generate_CustomPayrollHeadsReport()
                objPayroll.Generate_CustomPayrollHeadsReport(mdicYearDBName, User._Object._Userunkid, Company._Object._Companyunkid, mdtPeriod_Start, _
                                                             mdtPeriod_End, ConfigParameter._Object._UserAccessModeSetting, True, chkInactiveemp.Checked, _
                                                             True, ConfigParameter._Object._Base_CurrencyId, ConfigParameter._Object._SetPayslipPaymentApproval, _
                                                             ConfigParameter._Object._CurrencyFormat, ConfigParameter._Object._ExportReportPath, _
                                                             ConfigParameter._Object._OpenAfterExport)
                'Sohail (21 Aug 2015) -- End
            Else
                'Sohail (21 Aug 2015) -- Start
                'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
                'objPayroll.Generate_PayrollReport(True)
                'Sohail (26 Dec 2015) -- Start
                'Enhancement - Show Company Logo option on Payroll Report as SUm function was not working at FDRC for HTML as their french language in MS EXCEL.
                'objPayroll.Generate_PayrollReport(mdicYearDBName, User._Object._Userunkid, Company._Object._Companyunkid, mdtPeriod_Start, mdtPeriod_End, ConfigParameter._Object._UserAccessModeSetting, True, chkInactiveemp.Checked, True, ConfigParameter._Object._Base_CurrencyId, ConfigParameter._Object._SetPayslipPaymentApproval, ConfigParameter._Object._CurrencyFormat, ConfigParameter._Object._ExportReportPath, ConfigParameter._Object._OpenAfterExport, True)
                'Sohail (01 Mar 2016) -- Start
                'Enhancement - Rounding issue for Net Pay on Payroll Report, not matching with Payslip Report.
                'objPayroll.Generate_PayrollReport(mdicYearDBName, User._Object._Userunkid, Company._Object._Companyunkid, mdtPeriod_Start, mdtPeriod_End, ConfigParameter._Object._UserAccessModeSetting, True, chkInactiveemp.Checked, True, ConfigParameter._Object._Base_CurrencyId, ConfigParameter._Object._SetPayslipPaymentApproval, ConfigParameter._Object._CurrencyFormat, ConfigParameter._Object._ExportReportPath, ConfigParameter._Object._OpenAfterExport, chkShowCompanyLogoOnReport.Checked)
                objPayroll.Generate_PayrollReport(mdicYearDBName, User._Object._Userunkid, Company._Object._Companyunkid, mdtPeriod_Start, mdtPeriod_End, ConfigParameter._Object._UserAccessModeSetting, True, chkInactiveemp.Checked, True, ConfigParameter._Object._Base_CurrencyId, ConfigParameter._Object._SetPayslipPaymentApproval, ConfigParameter._Object._CurrencyFormat, ConfigParameter._Object._ExportReportPath, ConfigParameter._Object._OpenAfterExport, chkShowCompanyLogoOnReport.Checked, ConfigParameter._Object._RoundOff_Type)
                'Sohail (01 Mar 2016) -- End
                'Sohail (26 Dec 2015) -- End
                'Sohail (21 Aug 2015) -- End
            End If
            'S.SANDEEP [ 29 SEP 2014 ] -- END



        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "btnExport_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub btnReset_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnReset.Click
        Try
            Call ResetValue()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "btnReset_Click", mstrModuleName)
        End Try
    End Sub

    'Sohail (08 Nov 2011) -- Start
    Private Sub objbtnSearchEmployee_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnSearchEmployee.Click
        Dim frm As New frmCommonSearch
        Try
            frm.DataSource = cboEmployee.DataSource
            frm.ValueMember = cboEmployee.ValueMember
            frm.DisplayMember = cboEmployee.DisplayMember
            frm.CodeMember = "employeecode"
            If frm.DisplayDialog Then
                cboEmployee.SelectedValue = frm.SelectedValue
                cboEmployee.Focus()
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objbtnSearchEmployee_Click", mstrModuleName)
        Finally
            If frm IsNot Nothing Then frm.Dispose()
        End Try
    End Sub
    'Sohail (08 Nov 2011) -- End


    'Pinkal (03-Sep-2012) -- Start
    'Enhancement : TRA Changes

    Private Sub objbtnLanguage_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnLanguage.Click
        Dim objfrm As New frmLanguage
        Try
            If User._Object._Isrighttoleft = True Then
                objfrm.RightToLeft = Windows.Forms.RightToLeft.Yes
                objfrm.RightToLeftLayout = True
                Call Language.ctlRightToLeftlayOut(objfrm)
            End If

            Call SetMessages()

            clsPayrollReport.SetMessages()
            objfrm._Other_ModuleNames = "clsPayrollReport"
            objfrm.displayDialog(Me)

            Call Language.setLanguage(Me.Name)
            Call SetLanguage()

        Catch ex As System.Exception
            Call DisplayError.Show(-1, ex.Message, "Form_Language_Click", mstrModuleName)
        Finally
            objfrm.Dispose()
            objfrm = Nothing
        End Try
    End Sub


    'Pinkal (03-Sep-2012) -- End

    'Pinkal (27-Feb-2013) -- Start
    'Enhancement : TRA Changes

    Private Sub objbtnAdvanceFilter_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnAdvanceFilter.Click
        Dim frm As New frmAdvanceSearch
        Try
            frm._Hr_EmployeeTable_Alias = "hremployee_master"
            frm.ShowDialog()
            mstrAdvanceFilter = frm._GetFilterString
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "Form_AdvanceFilter_Click", mstrModuleName)
        Finally
            If frm IsNot Nothing Then frm.Dispose()
        End Try
    End Sub

    'Pinkal (27-Feb-2013) -- End

    'S.SANDEEP [ 20 SEP 2014 ] -- START
    Private Sub objbtnUp_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnUp.Click
        Try
            If lvCustomTranHead.SelectedItems.Count > 0 Then
                Dim SelIndex As Integer = lvCustomTranHead.SelectedIndices.Item(0)
                If SelIndex = 0 Then Exit Sub
                Dim tItem As ListViewItem
                tItem = lvCustomTranHead.Items(SelIndex - 1)
                lvCustomTranHead.Items(SelIndex - 1) = lvCustomTranHead.Items(SelIndex).Clone
                lvCustomTranHead.Items(SelIndex) = tItem.Clone
                lvCustomTranHead.Items(SelIndex - 1).Selected = True
                lvCustomTranHead.Items(SelIndex - 1).EnsureVisible()
            End If
            lvCustomTranHead.Focus()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objbtnUp_Click", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub objbtnDown_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnDown.Click
        Try
            If lvCustomTranHead.SelectedItems.Count > 0 Then
                Dim SelIndex As Integer = lvCustomTranHead.SelectedIndices.Item(0)
                If SelIndex = lvCustomTranHead.Items.Count - 1 Then Exit Sub
                Dim tItem As ListViewItem
                tItem = lvCustomTranHead.Items(SelIndex + 1)
                lvCustomTranHead.Items(SelIndex + 1) = lvCustomTranHead.Items(SelIndex).Clone
                lvCustomTranHead.Items(SelIndex) = tItem.Clone
                lvCustomTranHead.Items(SelIndex + 1).Selected = True
                lvCustomTranHead.Items(SelIndex + 1).EnsureVisible()
            End If
            lvCustomTranHead.Focus()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objbtnDown_Click", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub objbtnTop_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnTop.Click
        Try
            If lvAllocation_Hierarchy.SelectedItems.Count > 0 Then
                Dim SelIndex As Integer = lvAllocation_Hierarchy.SelectedIndices.Item(0)
                If SelIndex = 0 Then Exit Sub
                Dim tItem As ListViewItem
                tItem = lvAllocation_Hierarchy.Items(SelIndex - 1)
                lvAllocation_Hierarchy.Items(SelIndex - 1) = lvAllocation_Hierarchy.Items(SelIndex).Clone
                lvAllocation_Hierarchy.Items(SelIndex) = tItem.Clone
                lvAllocation_Hierarchy.Items(SelIndex - 1).Selected = True
                lvAllocation_Hierarchy.Items(SelIndex - 1).EnsureVisible()
            End If
            lvAllocation_Hierarchy.Focus()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objbtnTop_Click", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub objbtnBottom_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnBottom.Click
        Try
            If lvAllocation_Hierarchy.SelectedItems.Count > 0 Then
                Dim SelIndex As Integer = lvAllocation_Hierarchy.SelectedIndices.Item(0)
                If SelIndex = lvAllocation_Hierarchy.Items.Count - 1 Then Exit Sub
                Dim tItem As ListViewItem
                tItem = lvAllocation_Hierarchy.Items(SelIndex + 1)
                lvAllocation_Hierarchy.Items(SelIndex + 1) = lvAllocation_Hierarchy.Items(SelIndex).Clone
                lvAllocation_Hierarchy.Items(SelIndex) = tItem.Clone
                lvAllocation_Hierarchy.Items(SelIndex + 1).Selected = True
                lvAllocation_Hierarchy.Items(SelIndex + 1).EnsureVisible()
            End If
            lvAllocation_Hierarchy.Focus()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objbtnBottom_Click", mstrModuleName)
        Finally
        End Try
    End Sub
    'S.SANDEEP [ 20 SEP 2014 ] -- END

#End Region

    'Sohail (29 Apr 2014) -- Start
    'Enhancement - Export Excel Report with Company Logo 
#Region " Listview Events "

    Private Sub lvTranHead_ItemChecked(ByVal sender As Object, ByVal e As System.Windows.Forms.ItemCheckedEventArgs) Handles lvTranHead.ItemChecked
        Try
            If lvTranHead.CheckedItems.Count <= 0 Then
                RemoveHandler objchkSelectAll.CheckedChanged, AddressOf chkSelectAll_CheckedChanged
                objchkSelectAll.CheckState = CheckState.Unchecked
                AddHandler objchkSelectAll.CheckedChanged, AddressOf chkSelectAll_CheckedChanged
            ElseIf lvTranHead.CheckedItems.Count < lvTranHead.Items.Count Then
                RemoveHandler objchkSelectAll.CheckedChanged, AddressOf chkSelectAll_CheckedChanged
                objchkSelectAll.CheckState = CheckState.Indeterminate
                AddHandler objchkSelectAll.CheckedChanged, AddressOf chkSelectAll_CheckedChanged
            ElseIf lvTranHead.CheckedItems.Count = lvTranHead.Items.Count Then
                RemoveHandler objchkSelectAll.CheckedChanged, AddressOf chkSelectAll_CheckedChanged
                objchkSelectAll.CheckState = CheckState.Checked
                AddHandler objchkSelectAll.CheckedChanged, AddressOf chkSelectAll_CheckedChanged
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "lvTranHead_ItemChecked", mstrModuleName)
        End Try
    End Sub

    'S.SANDEEP [ 29 SEP 2014 ] -- START
    Private Sub lvCutomTranHead_ItemChecked(ByVal sender As Object, ByVal e As System.Windows.Forms.ItemCheckedEventArgs) Handles lvCustomTranHead.ItemChecked
        Try
            RemoveHandler objchkCheckAll.CheckedChanged, AddressOf objchkCheckAll_CheckedChanged
            If lvCustomTranHead.CheckedItems.Count <= 0 Then
                objchkCheckAll.CheckState = CheckState.Unchecked
            ElseIf lvCustomTranHead.CheckedItems.Count < lvCustomTranHead.Items.Count Then
                objchkCheckAll.CheckState = CheckState.Indeterminate
            ElseIf lvCustomTranHead.CheckedItems.Count = lvCustomTranHead.Items.Count Then
                objchkCheckAll.CheckState = CheckState.Checked
            End If

            'Shani [ 22 NOV 2014 ] -- START
            'FDRC Enhancemwnt - Provide Transaction Head Type filter on Custom Payroll Report.
            marrCustomPayrollReportHeadsIds.Clear()
            'Sohail (03 Feb 2016) -- Start
            'Enhancement - Show Loans and Savings in Separate Columns option in Payroll Report.
            'marrCustomPayrollReportHeadsIds.AddRange((From p In lvCustomTranHead.CheckedItems.Cast(Of ListViewItem)() Select (p.Tag.ToString)).ToArray)
            marrCustomPayrollReportHeadsIds.AddRange((From p In lvCustomTranHead.CheckedItems.Cast(Of ListViewItem)() Where (CInt(p.SubItems(colhCHeadTypeID.Index).Text) > 0) Select (p.Tag.ToString)).ToArray)
            marrCustomPayrollReportHeadsIds.AddRange((From p In lvCustomTranHead.CheckedItems.Cast(Of ListViewItem)() Where (CInt(p.SubItems(colhCHeadTypeID.Index).Text) = -1) Select ("Loan" + p.Tag.ToString)).ToArray)
            marrCustomPayrollReportHeadsIds.AddRange((From p In lvCustomTranHead.CheckedItems.Cast(Of ListViewItem)() Where (CInt(p.SubItems(colhCHeadTypeID.Index).Text) = -2) Select ("Advance" + p.Tag.ToString)).ToArray)
            marrCustomPayrollReportHeadsIds.AddRange((From p In lvCustomTranHead.CheckedItems.Cast(Of ListViewItem)() Where (CInt(p.SubItems(colhCHeadTypeID.Index).Text) = -3) Select ("Saving" + p.Tag.ToString)).ToArray)
            'Sohail (03 Feb 2016) -- End
            'Shani [ 22 NOV 2014 ] -- END

            AddHandler objchkCheckAll.CheckedChanged, AddressOf objchkCheckAll_CheckedChanged
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "lvCutomTranHead_ItemChecked", mstrModuleName)
        End Try
    End Sub

    Private Sub lvAllocation_Hierarchy_ItemChecked(ByVal sender As Object, ByVal e As System.Windows.Forms.ItemCheckedEventArgs) Handles lvAllocation_Hierarchy.ItemChecked
        Try
            RemoveHandler objchkAllocation.CheckedChanged, AddressOf objchkAllocation_CheckedChanged
            If lvAllocation_Hierarchy.CheckedItems.Count <= 0 Then
                objchkAllocation.CheckState = CheckState.Unchecked
            ElseIf lvAllocation_Hierarchy.CheckedItems.Count < lvAllocation_Hierarchy.Items.Count Then
                objchkAllocation.CheckState = CheckState.Indeterminate
            ElseIf lvAllocation_Hierarchy.CheckedItems.Count = lvAllocation_Hierarchy.Items.Count Then
                objchkAllocation.CheckState = CheckState.Checked
            End If
            AddHandler objchkAllocation.CheckedChanged, AddressOf objchkAllocation_CheckedChanged
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "lvAllocation_Hierarchy_ItemChecked", mstrModuleName)
        Finally
        End Try
    End Sub
    'S.SANDEEP [ 29 SEP 2014 ] -- END

#End Region

#Region " CheckBox's Events "
    Private Sub chkSelectAll_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objchkSelectAll.CheckedChanged
        Try
            Call CheckAllHeads(objchkSelectAll.Checked)
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "chkSelectAll_CheckedChanged", mstrModuleName)
        End Try
    End Sub

    'S.SANDEEP [ 29 SEP 2014 ] -- START
    Private Sub objchkCheckAll_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objchkCheckAll.CheckedChanged
        Try
            RemoveHandler lvCustomTranHead.ItemChecked, AddressOf lvCutomTranHead_ItemChecked
            For Each lvItem As ListViewItem In lvCustomTranHead.Items
                lvItem.Checked = objchkCheckAll.CheckState
            Next
            'Shani [ 22 NOV 2014 ] -- START
            'FDRC Enhancemwnt - Provide Transaction Head Type filter on Custom Payroll Report.
            marrCustomPayrollReportHeadsIds.Clear()
            'Sohail (03 Feb 2016) -- Start
            'Enhancement - Show Loans and Savings in Separate Columns option in Payroll Report.
            'marrCustomPayrollReportHeadsIds.AddRange((From p In lvCustomTranHead.CheckedItems.Cast(Of ListViewItem)() Select (p.Tag.ToString)).ToArray)
            marrCustomPayrollReportHeadsIds.AddRange((From p In lvCustomTranHead.CheckedItems.Cast(Of ListViewItem)() Where (CInt(p.SubItems(colhCHeadTypeID.Index).Text) > 0) Select (p.Tag.ToString)).ToArray)
            marrCustomPayrollReportHeadsIds.AddRange((From p In lvCustomTranHead.CheckedItems.Cast(Of ListViewItem)() Where (CInt(p.SubItems(colhCHeadTypeID.Index).Text) = -1) Select ("Loan" + p.Tag.ToString)).ToArray)
            marrCustomPayrollReportHeadsIds.AddRange((From p In lvCustomTranHead.CheckedItems.Cast(Of ListViewItem)() Where (CInt(p.SubItems(colhCHeadTypeID.Index).Text) = -2) Select ("Advance" + p.Tag.ToString)).ToArray)
            marrCustomPayrollReportHeadsIds.AddRange((From p In lvCustomTranHead.CheckedItems.Cast(Of ListViewItem)() Where (CInt(p.SubItems(colhCHeadTypeID.Index).Text) = -3) Select ("Saving" + p.Tag.ToString)).ToArray)
            'Sohail (03 Feb 2016) -- End
            'Shani [ 22 NOV 2014 ] -- END
            AddHandler lvCustomTranHead.ItemChecked, AddressOf lvCutomTranHead_ItemChecked
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objchkCheckAll_CheckedChanged", mstrModuleName)
        End Try
    End Sub

    Private Sub objchkAllocation_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objchkAllocation.CheckedChanged
        Try
            RemoveHandler lvAllocation_Hierarchy.ItemChecked, AddressOf lvAllocation_Hierarchy_ItemChecked
            For Each lvItem As ListViewItem In lvAllocation_Hierarchy.Items
                lvItem.Checked = objchkAllocation.CheckState
            Next
            AddHandler lvAllocation_Hierarchy.ItemChecked, AddressOf lvAllocation_Hierarchy_ItemChecked
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objchkAllocation_CheckedChanged", mstrModuleName)
        Finally
        End Try
    End Sub
    'S.SANDEEP [ 29 SEP 2014 ] -- END

#End Region
    'Sohail (29 Apr 2014) -- End

    'Sohail (26 Nov 2011) -- Start
#Region " Other Control's Events "
    Private Sub lnkAnalysisBy_LinkClicked(ByVal sender As System.Object, ByVal e As System.Windows.Forms.LinkLabelLinkClickedEventArgs) Handles lnkAnalysisBy.LinkClicked
        Dim frm As New frmViewAnalysis
        Try
            frm.displayDialog()
            mstrStringIds = frm._ReportBy_Ids
            mstrStringName = frm._ReportBy_Name
            mintViewIdx = frm._ViewIndex
            'Sohail (26 Nov 2011) -- Start
            mstrAnalysis_Fields = frm._Analysis_Fields
            mstrAnalysis_Join = frm._Analysis_Join
            mstrAnalysis_OrderBy = frm._Analysis_OrderBy
            mstrReport_GroupName = frm._Report_GroupName
            'Sohail (26 Nov 2011) -- End
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "lnkAnalysisBy_LinkClicked", mstrModuleName)
        Finally
            frm = Nothing
        End Try
    End Sub

    'Sohail (05 Apr 2012) -- Start
    'TRA - ENHANCEMENT
    Private Sub cboReportType_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cboReportType.SelectedIndexChanged
        Try
            'Sohail (01 Sep 2014) -- Start
            'Enhancement - New Report Type Employee Payroll Summary in Payroll Report.
            chkShowPaymentDetails.Visible = True
            'Sohail (01 Sep 2014) -- End

            'Sohail (26 Dec 2015) -- Start
            'Enhancement - Show Company Logo option on Payroll Report as SUm function was not working at FDRC for HTML as their french language in MS EXCEL.
            chkShowCompanyLogoOnReport.Visible = True
            'Sohail (26 Dec 2015) -- End

            'S.SANDEEP [ 29 SEP 2014 ] -- START
            gbCustomSetting.Visible = False
            lvAllocation_Hierarchy.Items.Clear()
            lvCustomTranHead.Items.Clear()
            'S.SANDEEP [ 29 SEP 2014 ] -- END

            'Sohail (10 Jul 2019) -- Start
            'Good Neighbours Enhancement - Support Issue Id # 3876 - 76.1 - Put membership number in custom payroll report -& separate employee name as First Name, Middle Name and Last Name.
            lblMembership.Visible = False
            cboMembership.Visible = False
            chkShowEmpNameinSeperateColumn.Visible = False
            'Sohail (10 Jul 2019) -- End

            'Pinkal (31-Mar-2023) -- Start
            '[JIRA] (A1X-676) Sissalana - Custom Payroll Report Modification to include Employee Identity info.
            lblIdentity.Visible = False
            cboIdentity.Visible = False
            'Pinkal (31-Mar-2023) -- End

            pnlTranHead.Enabled = True : txtSearchHead.Enabled = True

            Select Case cboReportType.SelectedIndex
                Case 0 'Payroll Report
                    lblPeriod.Text = "Period"
                    lblToPeriod.Visible = False
                    cboToPeriod.Visible = False

                    'S.SANDEEP [ 11 SEP 2012 ] -- START
                    'ENHANCEMENT : TRA CHANGES
                    lblCurrency.Visible = True
                    cboCurrency.Visible = True
                    cboCurrency.SelectedValue = 0
                    lblExRate.Text = ""
                    lblExRate.Visible = True
                    'S.SANDEEP [ 11 SEP 2012 ] -- END

                    'Nilay (12-Oct-2016) -- Start
                    'CCK Enhancements -  Salary Journal - To include cost center codes in custom payroll report
                    lblCostCenterCode.Visible = False
                    txtCostCenterCode.Text = ""
                    txtCostCenterCode.Visible = False
                    'Nilay (12-Oct-2016) -- End

                    'Sohail (01 Sep 2014) -- Start
                    'Enhancement - New Report Type Employee Payroll Summary in Payroll Report.
                    'Case 1 'Personal Salary Calculation
                Case 1, 2 'Personal Salary Calculation OR Employee Payroll Summary
                    'Sohail (01 Sep 2014) -- End

                    lblPeriod.Text = "From Period"
                    lblToPeriod.Visible = True
                    cboToPeriod.Visible = True

                    'S.SANDEEP [ 11 SEP 2012 ] -- START
                    'ENHANCEMENT : TRA CHANGES
                    lblCurrency.Visible = False
                    cboCurrency.Visible = False
                    cboCurrency.SelectedValue = 0
                    lblExRate.Text = ""
                    lblExRate.Visible = False
                    'S.SANDEEP [ 11 SEP 2012 ] -- END

                    'Sohail (01 Sep 2014) -- Start
                    'Enhancement - New Report Type Employee Payroll Summary in Payroll Report.
                    If cboReportType.SelectedIndex = 2 Then
                        chkShowPaymentDetails.Checked = False
                        chkShowPaymentDetails.Visible = False
                    End If
                    'Sohail (01 Sep 2014) -- End

                    'Nilay (12-Oct-2016) -- Start
                    'CCK Enhancements -  Salary Journal - To include cost center codes in custom payroll report
                    lblCostCenterCode.Visible = False
                    txtCostCenterCode.Text = ""
                    txtCostCenterCode.Visible = False
                    'Nilay (12-Oct-2016) -- End

                    'S.SANDEEP [ 29 SEP 2014 ] -- START
                Case 3  'Custom Payroll Report
                    lblPeriod.Text = "Period"
                    lblToPeriod.Visible = False
                    cboToPeriod.Visible = False
                    'Shani [ 22 NOV 2014 ] -- START
                    'FDRC Enhancemwnt - Provide Transaction Head Type filter on Custom Payroll Report.
                    cboTrnHeadType.SelectedValue = 0
                    'Shani [ 22 NOV 2014 ] -- END

                    pnlTranHead.Enabled = False : txtSearchHead.Enabled = False
                    gbCustomSetting.Visible = True

                    lblCurrency.Visible = True
                    cboCurrency.Visible = True
                    cboCurrency.SelectedValue = 0
                    lblExRate.Text = ""
                    lblExRate.Visible = True
                    Call Fill_Allocation()
                    Call FillCustomHeads()
                    chkIncludeEmployerContribution.CheckState = CheckState.Unchecked
                    chkIncludeEmployerContribution.Enabled = False
                    'S.SANDEEP [ 29 SEP 2014 ] -- END
                    'Sohail (26 Dec 2015) -- Start
                    'Enhancement - Show Company Logo option on Payroll Report as SUm function was not working at FDRC for HTML as their french language in MS EXCEL.
                    chkShowCompanyLogoOnReport.Checked = False
                    chkShowCompanyLogoOnReport.Visible = False
                    'Sohail (26 Dec 2015) -- End

                    'Nilay (12-Oct-2016) -- Start
                    'CCK Enhancements -  Salary Journal - To include cost center codes in custom payroll report
                    lblCostCenterCode.Visible = False
                    txtCostCenterCode.Text = ""
                    txtCostCenterCode.Visible = False
                    'Nilay (12-Oct-2016) -- End

                    'Sohail (10 Jul 2019) -- Start
                    'Good Neighbours Enhancement - Support Issue Id # 3876 - 76.1 - Put membership number in custom payroll report -& separate employee name as First Name, Middle Name and Last Name.
                    lblMembership.Visible = True
                    cboMembership.Visible = True
                    chkShowEmpNameinSeperateColumn.Visible = True
                    'Sohail (10 Jul 2019) -- End

                    'Pinkal (31-Mar-2023) -- Start
                    '[JIRA] (A1X-676) Sissalana - Custom Payroll Report Modification to include Employee Identity info.
                    lblIdentity.Visible = True
                    cboIdentity.Visible = True
                    'Pinkal (31-Mar-2023) -- End


                    'Sohail (20 Apr 2015) -- Start
                    'KVTC Enhancement - New Report Custom Payroll Report Template 2 in Payroll Report. (Email From: Rutta Anatory, Subject: KVTC, Sent: Monday, March 02, 2015 1:13 PM)
                Case 4
                    lblPeriod.Text = "Period"
                    lblToPeriod.Visible = False
                    cboToPeriod.Visible = False
                    cboTrnHeadType.SelectedValue = 0
                    pnlTranHead.Enabled = False : txtSearchHead.Enabled = False
                    gbCustomSetting.Visible = True
                    lblCurrency.Visible = True
                    cboCurrency.Visible = True
                    cboCurrency.SelectedValue = 0
                    lblExRate.Text = ""
                    lblExRate.Visible = True
                    Call Fill_Allocation()
                    Call FillCustomHeads()
                    chkIncludeEmployerContribution.CheckState = CheckState.Unchecked
                    chkIncludeEmployerContribution.Enabled = False
                    'Sohail (26 Dec 2015) -- Start
                    'Enhancement - Show Company Logo option on Payroll Report as SUm function was not working at FDRC for HTML as their french language in MS EXCEL.
                    chkShowCompanyLogoOnReport.Checked = False
                    chkShowCompanyLogoOnReport.Visible = False
                    'Sohail (26 Dec 2015) -- End
                    'Sohail (20 Apr 2015) -- End

                    'Nilay (12-Oct-2016) -- Start
                    'CCK Enhancements -  Salary Journal - To include cost center codes in custom payroll report
                    lblCostCenterCode.Visible = True
                    txtCostCenterCode.Text = ""
                    txtCostCenterCode.Visible = True
                    'Nilay (12-Oct-2016) -- End

                    'Sohail (10 Jul 2019) -- Start
                    'Good Neighbours Enhancement - Support Issue Id # 3876 - 76.1 - Put membership number in custom payroll report -& separate employee name as First Name, Middle Name and Last Name.
                    lblMembership.Visible = True
                    cboMembership.Visible = True
                    chkShowEmpNameinSeperateColumn.Visible = True
                    'Sohail (10 Jul 2019) -- End

                    'Pinkal (31-Mar-2023) -- Start
                    '[JIRA] (A1X-676) Sissalana - Custom Payroll Report Modification to include Employee Identity info.
                    lblIdentity.Visible = True
                    cboIdentity.Visible = True
                    'Pinkal (31-Mar-2023) -- End

            End Select
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "cboReportType_SelectedIndexChanged", mstrModuleName)
        End Try
    End Sub
    'Sohail (05 Apr 2012) -- End


    'S.SANDEEP [ 11 SEP 2012 ] -- START
    'ENHANCEMENT : TRA CHANGES
    Private Sub cboCurrency_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cboCurrency.SelectedIndexChanged _
                                                                                                            , cboPeriod.SelectedIndexChanged
        'Sohail (08 Dec 2012) -- Start
        'TRA - ENHANCEMENT
        Dim objPeriod As New clscommom_period_Tran
        Dim objCompany As New clsCompany_Master
        Dim dsList As DataSet
        'Sohail (08 Dec 2012) -- End
        Try
            If cboCurrency.Visible = True Then
                If CInt(cboCurrency.SelectedValue) > 0 Then
                    Dim objExRate As New clsExchangeRate
                    'Sohail (08 Dec 2012) -- Start
                    'TRA - ENHANCEMENT
                    'Dim dsList As DataSet
                    'Dim objPeriod As New clscommom_period_Tran 
                    'Sohail (08 Dec 2012) -- End
                    'Sohail (21 Aug 2015) -- Start
                    'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
                    'objPeriod._Periodunkid = CInt(cboPeriod.SelectedValue)
                    objPeriod._Periodunkid(FinancialYear._Object._DatabaseName) = CInt(cboPeriod.SelectedValue)
                    'Sohail (21 Aug 2015) -- End
                    dsList = objExRate.GetList("ExRate", True, , , CInt(cboCurrency.SelectedValue), True, objPeriod._End_Date.Date, True)
                    If dsList.Tables("ExRate").Rows.Count > 0 Then
                        mdecEx_Rate = CDec(dsList.Tables("ExRate").Rows(0)("exchange_rate"))
                        mstrCurr_Sign = dsList.Tables("ExRate").Rows(0)("currency_sign")
                        lblExRate.Text = Language.getMessage(mstrModuleName, 6, "Exchange Rate :") & " " & CDbl(mdecEx_Rate) & " "
                        mstrCurrency_Rate = Format(CDec(dsList.Tables("ExRate").Rows(0).Item("exchange_rate1")), GUI.fmtCurrency) & " " & mstrBaseCurrSign & " = " & CDec(dsList.Tables("ExRate").Rows(0).Item("exchange_rate2")).ToString & " " & mstrCurr_Sign & " " 'Sohail (16 Mar 2013)
                    Else
                        lblExRate.Text = "" : mdecEx_Rate = 0 : mstrCurr_Sign = ""
                    End If
                Else
                    mdecEx_Rate = 1 : lblExRate.Text = "" : mstrCurr_Sign = String.Empty
                    mstrCurrency_Rate = "" 'Sohail (16 Mar 2013)
                End If
            End If

            'Sohail (08 Dec 2012) -- Start
            'TRA - ENHANCEMENT
            'Sohail (21 Aug 2015) -- Start
            'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
            'objPeriod._Periodunkid = CInt(cboPeriod.SelectedValue)
            objPeriod._Periodunkid(FinancialYear._Object._DatabaseName) = CInt(cboPeriod.SelectedValue)
            'Sohail (21 Aug 2015) -- End
            dsList = objCompany.GetFinancialYearList(Company._Object._Companyunkid, , "Database", objPeriod._Yearunkid)
            If dsList.Tables("Database").Rows.Count > 0 Then
                mstrFromDatabaseName = dsList.Tables("Database").Rows(0).Item("database_name").ToString
            Else
                mstrFromDatabaseName = ""
            End If
            'Sohail (08 Dec 2012) -- End

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "cboCurrency_SelectedIndexChanged", mstrModuleName)
            'Sohail (08 Dec 2012) -- Start
            'TRA - ENHANCEMENT
        Finally
            objPeriod = Nothing
            objCompany = Nothing
            'Sohail (08 Dec 2012) -- End
        End Try

    End Sub
    'S.SANDEEP [ 11 SEP 2012 ] -- END

    'Sohail (08 Dec 2012) -- Start
    'TRA - ENHANCEMENT
    Private Sub cboToPeriod_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cboToPeriod.SelectedIndexChanged
        Dim objPeriod As New clscommom_period_Tran
        Dim objCompany As New clsCompany_Master
        Dim dsList As DataSet
        Try
            'Sohail (21 Aug 2015) -- Start
            'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
            'objPeriod._Periodunkid = CInt(cboToPeriod.SelectedValue)
            objPeriod._Periodunkid(FinancialYear._Object._DatabaseName) = CInt(cboToPeriod.SelectedValue)
            'Sohail (21 Aug 2015) -- End
            dsList = objCompany.GetFinancialYearList(Company._Object._Companyunkid, , "Database", objPeriod._Yearunkid)
            If dsList.Tables("Database").Rows.Count > 0 Then
                mstrToDatabaseName = dsList.Tables("Database").Rows(0).Item("database_name").ToString
            Else
                mstrToDatabaseName = ""
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "cboToPeriod_SelectedIndexChanged", mstrModuleName)
        Finally
            objPeriod = Nothing
            objCompany = Nothing
        End Try
    End Sub
    'Sohail (08 Dec 2012) -- End

    'Sohail (07 Aug 2013) -- Start
    'TRA - ENHANCEMENT
    Private Sub objbtnSort_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnSort.Click
        Try
            objPayroll.setOrderBy(0)
            txtOrderBy.Text = objPayroll.OrderByDisplay
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objbtnSort_Click", mstrModuleName)
        End Try
    End Sub
    'Sohail (07 Aug 2013) -- End

    'Sohail (29 Apr 2014) -- Start
    'Enhancement - Export Excel Report with Company Logo 
    Private Sub txtSearchHead_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txtSearchHead.TextChanged
        Try
            If lvTranHead.Items.Count <= 0 Then Exit Sub
            lvTranHead.SelectedIndices.Clear()
            Dim lvFoundItem As ListViewItem = lvTranHead.FindItemWithText(txtSearchHead.Text, True, 0, True)
            If lvFoundItem IsNot Nothing Then
                lvTranHead.TopItem = lvFoundItem
                lvFoundItem.Selected = True
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "txtSearchEmp_TextChanged", mstrModuleName)
        End Try
    End Sub
    'Sohail (29 Apr 2014) -- End


    'S.SANDEEP [ 29 SEP 2014 ] -- START
    Private Sub txtSearchCHeads_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txtSearchCHeads.TextChanged
        Try
            If lvCustomTranHead.Items.Count <= 0 Then Exit Sub
            lvCustomTranHead.SelectedIndices.Clear()
            Dim lvFoundItem As ListViewItem = lvCustomTranHead.FindItemWithText(txtSearchCHeads.Text, True, 0, True)
            If lvFoundItem IsNot Nothing Then
                lvCustomTranHead.TopItem = lvFoundItem
                lvFoundItem.Selected = True
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "txtSearchCHeads_TextChanged", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub lnkSave_LinkClicked(ByVal sender As System.Object, ByVal e As System.Windows.Forms.LinkLabelLinkClickedEventArgs) Handles lnkSave.LinkClicked
        Try
            Dim objConfig As New clsConfigOptions
            objConfig._Companyunkid = Company._Object._Companyunkid

            Dim xCheckedItems As String = String.Empty
            If lvCustomTranHead.CheckedItems.Count > 0 Then
                'Sohail (03 Feb 2016) -- Start
                'Enhancement - Show Loans and Savings in Separate Columns option in Payroll Report.
                'xCheckedItems = String.Join(",", lvCustomTranHead.CheckedItems.Cast(Of ListViewItem)().[Select](Function(x) x.Tag.ToString()).ToArray())
                'Sohail (29 Mar 2017) -- Start
                'CCK Enhancement - 65.2 - On Custom payroll report, loans are showing after the TOTAL Deduction column. They should show before the total deduction column.
                'xCheckedItems = String.Join(",", (From p In lvCustomTranHead.CheckedItems.Cast(Of ListViewItem)() Where (CInt(p.SubItems(colhCHeadTypeID.Index).Text) > 0) Select (p.Tag.ToString)).ToArray _
                '                                 .Union _
                '                                 (From p In lvCustomTranHead.CheckedItems.Cast(Of ListViewItem)() Where (CInt(p.SubItems(colhCHeadTypeID.Index).Text) = -1) Select ("Loan" + p.Tag.ToString)).ToArray _
                '                                 .Union _
                '                                 (From p In lvCustomTranHead.CheckedItems.Cast(Of ListViewItem)() Where (CInt(p.SubItems(colhCHeadTypeID.Index).Text) = -2) Select ("Advance" + p.Tag.ToString)).ToArray _
                '                                 .Union _
                '                                 (From p In lvCustomTranHead.CheckedItems.Cast(Of ListViewItem)() Where (CInt(p.SubItems(colhCHeadTypeID.Index).Text) = -3) Select ("Saving" + p.Tag.ToString)).ToArray)
                xCheckedItems = String.Join(",", (From p In lvCustomTranHead.CheckedItems.Cast(Of ListViewItem)() Select (If(CInt(p.SubItems(colhCHeadTypeID.Index).Text) > 0, p.Tag.ToString, If(CInt(p.SubItems(colhCHeadTypeID.Index).Text) = -1, "Loan" + p.Tag.ToString, If(CInt(p.SubItems(colhCHeadTypeID.Index).Text) = -2, "Advance" + p.Tag.ToString, If(CInt(p.SubItems(colhCHeadTypeID.Index).Text) = -3, "Saving" + p.Tag.ToString, "").ToString).ToString).ToString).ToString)).ToArray)
                'Sohail (29 Mar 2017) -- End                                                 
                'Sohail (03 Feb 2016) -- End
                If xCheckedItems.Trim.Length > 0 Then
                    'Sohail (20 Apr 2015) -- Start
                    'KVTC Enhancement - New Report Custom Payroll Report Template 2 in Payroll Report. (Email From: Rutta Anatory, Subject: KVTC, Sent: Monday, March 02, 2015 1:13 PM)
                    'objConfig._CustomPayrollReportHeadsIds = xCheckedItems
                    If cboReportType.SelectedIndex = 4 Then 'Custom Payroll Report Template 2
                        objConfig._CustomPayrollReportTemplate2HeadsIds = xCheckedItems
                    Else 'Custom Payroll Report
                        objConfig._CustomPayrollReportHeadsIds = xCheckedItems
                    End If
                    'Sohail (20 Apr 2015) -- End
                End If
            End If
            xCheckedItems = ""
            If lvAllocation_Hierarchy.CheckedItems.Count > 0 Then
                xCheckedItems = String.Join(",", lvAllocation_Hierarchy.CheckedItems.Cast(Of ListViewItem)().[Select](Function(x) x.Tag.ToString()).ToArray())
                If xCheckedItems.Trim.Length > 0 Then
                    'Sohail (20 Apr 2015) -- Start
                    'KVTC Enhancement - New Report Custom Payroll Report Template 2 in Payroll Report. (Email From: Rutta Anatory, Subject: KVTC, Sent: Monday, March 02, 2015 1:13 PM)
                    'objConfig._CustomPayrollReportAllocIds = xCheckedItems
                    If cboReportType.SelectedIndex = 4 Then 'Custom Payroll Report Template 2
                        objConfig._CustomPayrollReportTemplate2AllocIds = xCheckedItems
                    Else 'Custom Payroll Report
                        objConfig._CustomPayrollReportAllocIds = xCheckedItems
                    End If
                    'Sohail (20 Apr 2015) -- End
                End If
            End If
            objConfig.updateParam()
            ConfigParameter._Object.Refresh()
            eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 13, "Settings saved successfully."), enMsgBoxStyle.Information)
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "lnkSave_LinkClicked", mstrModuleName)
        Finally
        End Try
    End Sub
    'S.SANDEEP [ 29 SEP 2014 ] -- END


    'Shani [ 22 NOV 2014 ] -- START
    'FDRC Enhancemwnt - Provide Transaction Head Type filter on Custom Payroll Report.
    Private Sub cboTrnHeadType_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cboTrnHeadType.SelectedIndexChanged
        Try
            Call FillCustomHeads()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "cboTrnHeadType_SelectedIndexChanged", mstrModuleName)
        End Try
    End Sub
    'Shani [ 22 NOV 2014 ] -- END

#End Region


    '<Language> This Auto Generated Text Please Do Not Modify it.
#Region " Language & UI Settings "
    Private Sub OtherSettings()
        Try
            Me.SuspendLayout()
            Call SetLanguage()

            Me.gbFilterCriteria.GradientColor = GUI._eZeeContainerHeaderBackColor
            Me.gbFilterCriteria.ForeColor = GUI._eZeeContainerHeaderForeColor

            Me.gbSortBy.GradientColor = GUI._eZeeContainerHeaderBackColor
            Me.gbSortBy.ForeColor = GUI._eZeeContainerHeaderForeColor

            Me.gbCustomSetting.GradientColor = GUI._eZeeContainerHeaderBackColor
            Me.gbCustomSetting.ForeColor = GUI._eZeeContainerHeaderForeColor


         
            Me.eZeeHeader.GradientColor1 = GUI._HeaderBackColor1
            Me.eZeeHeader.GradientColor2 = GUI._HeaderBackColor2
            Me.eZeeHeader.BorderColor = GUI._HeaderBorderColor
            Me.eZeeHeader.HeaderTextForeColor = GUI._HeaderTitleFontColor
            Me.eZeeHeader.DescriptionForeColor = GUI._HeaderMessageFontColor


            Me.objLanguage.GradientBackColor = GUI._ButttonBackColor
            Me.objLanguage.GradientForeColor = GUI._ButttonFontColor

            Me.objbtnLanguage.GradientBackColor = GUI._ButttonBackColor
            Me.objbtnLanguage.GradientForeColor = GUI._ButttonFontColor

            Me.btnClose.GradientBackColor = GUI._ButttonBackColor
            Me.btnClose.GradientForeColor = GUI._ButttonFontColor

            Me.btnReset.GradientBackColor = GUI._ButttonBackColor
            Me.btnReset.GradientForeColor = GUI._ButttonFontColor

            Me.btnExport.GradientBackColor = GUI._ButttonBackColor
            Me.btnExport.GradientForeColor = GUI._ButttonFontColor

            Me.objbtnAdvanceFilter.GradientBackColor = GUI._ButttonBackColor
            Me.objbtnAdvanceFilter.GradientForeColor = GUI._ButttonFontColor

            Me.objbtnUp.GradientBackColor = GUI._ButttonBackColor
            Me.objbtnUp.GradientForeColor = GUI._ButttonFontColor

            Me.objbtnDown.GradientBackColor = GUI._ButttonBackColor
            Me.objbtnDown.GradientForeColor = GUI._ButttonFontColor

            Me.objbtnTop.GradientBackColor = GUI._ButttonBackColor
            Me.objbtnTop.GradientForeColor = GUI._ButttonFontColor

            Me.objbtnBottom.GradientBackColor = GUI._ButttonBackColor
            Me.objbtnBottom.GradientForeColor = GUI._ButttonFontColor


            Me.ResumeLayout()
        Catch Ex As Exception
            DisplayError.Show("-1", Ex.Message, "OtherSettings", mstrModuleName)
        End Try
    End Sub


    Private Sub SetLanguage()
        Try
            Me.Text = Language._Object.getCaption(Me.Name, Me.Text)

            Me.gbFilterCriteria.Text = Language._Object.getCaption(Me.gbFilterCriteria.Name, Me.gbFilterCriteria.Text)
            Me.lblEmployee.Text = Language._Object.getCaption(Me.lblEmployee.Name, Me.lblEmployee.Text)
            Me.eZeeHeader.Title = Language._Object.getCaption(Me.eZeeHeader.Name & "_Title", Me.eZeeHeader.Title)
            Me.eZeeHeader.Message = Language._Object.getCaption(Me.eZeeHeader.Name & "_Message", Me.eZeeHeader.Message)
            Me.btnClose.Text = Language._Object.getCaption(Me.btnClose.Name, Me.btnClose.Text)
            Me.btnReset.Text = Language._Object.getCaption(Me.btnReset.Name, Me.btnReset.Text)
            Me.btnExport.Text = Language._Object.getCaption(Me.btnExport.Name, Me.btnExport.Text)
            Me.lblPeriod.Text = Language._Object.getCaption(Me.lblPeriod.Name, Me.lblPeriod.Text)
            Me.chkInactiveemp.Text = Language._Object.getCaption(Me.chkInactiveemp.Name, Me.chkInactiveemp.Text)
            Me.lblBranch.Text = Language._Object.getCaption(Me.lblBranch.Name, Me.lblBranch.Text)
            Me.lnkAnalysisBy.Text = Language._Object.getCaption(Me.lnkAnalysisBy.Name, Me.lnkAnalysisBy.Text)
            Me.chkIgnorezeroHead.Text = Language._Object.getCaption(Me.chkIgnorezeroHead.Name, Me.chkIgnorezeroHead.Text)
            Me.lblReportType.Text = Language._Object.getCaption(Me.lblReportType.Name, Me.lblReportType.Text)
            Me.lblToPeriod.Text = Language._Object.getCaption(Me.lblToPeriod.Name, Me.lblToPeriod.Text)
            Me.lblCurrency.Text = Language._Object.getCaption(Me.lblCurrency.Name, Me.lblCurrency.Text)
            Me.lblExRate.Text = Language._Object.getCaption(Me.lblExRate.Name, Me.lblExRate.Text)
            Me.gbSortBy.Text = Language._Object.getCaption(Me.gbSortBy.Name, Me.gbSortBy.Text)
            Me.lblOrderBy.Text = Language._Object.getCaption(Me.lblOrderBy.Name, Me.lblOrderBy.Text)
            Me.colhCheck.Text = Language._Object.getCaption(CStr(Me.colhCheck.Tag), Me.colhCheck.Text)
            Me.colhCode.Text = Language._Object.getCaption(CStr(Me.colhCode.Tag), Me.colhCode.Text)
            Me.colhName.Text = Language._Object.getCaption(CStr(Me.colhName.Tag), Me.colhName.Text)
            Me.lblSearchHead.Text = Language._Object.getCaption(Me.lblSearchHead.Name, Me.lblSearchHead.Text)
            Me.lblIncludeHeads.Text = Language._Object.getCaption(Me.lblIncludeHeads.Name, Me.lblIncludeHeads.Text)
            Me.chkShowPaymentDetails.Text = Language._Object.getCaption(Me.chkShowPaymentDetails.Name, Me.chkShowPaymentDetails.Text)
            Me.chkIncludeEmployerContribution.Text = Language._Object.getCaption(Me.chkIncludeEmployerContribution.Name, Me.chkIncludeEmployerContribution.Text)
            Me.gbCustomSetting.Text = Language._Object.getCaption(Me.gbCustomSetting.Name, Me.gbCustomSetting.Text)
            Me.lnkSave.Text = Language._Object.getCaption(Me.lnkSave.Name, Me.lnkSave.Text)
            Me.colhCCode.Text = Language._Object.getCaption(CStr(Me.colhCCode.Tag), Me.colhCCode.Text)
            Me.colhCName.Text = Language._Object.getCaption(CStr(Me.colhCName.Tag), Me.colhCName.Text)
            Me.colhAllocation.Text = Language._Object.getCaption(CStr(Me.colhAllocation.Tag), Me.colhAllocation.Text)
            Me.lblTrnHeadType.Text = Language._Object.getCaption(Me.lblTrnHeadType.Name, Me.lblTrnHeadType.Text)
            Me.chkShowCompanyLogoOnReport.Text = Language._Object.getCaption(Me.chkShowCompanyLogoOnReport.Name, Me.chkShowCompanyLogoOnReport.Text)
            Me.chkShowLoansInSeparateColumns.Text = Language._Object.getCaption(Me.chkShowLoansInSeparateColumns.Name, Me.chkShowLoansInSeparateColumns.Text)
            Me.chkShowSavingsInSeparateColumns.Text = Language._Object.getCaption(Me.chkShowSavingsInSeparateColumns.Name, Me.chkShowSavingsInSeparateColumns.Text)
            Me.colhCHeadTypeID.Text = Language._Object.getCaption(CStr(Me.colhCHeadTypeID.Tag), Me.colhCHeadTypeID.Text)
            Me.lblCostCenterCode.Text = Language._Object.getCaption(Me.lblCostCenterCode.Name, Me.lblCostCenterCode.Text)
            Me.chkShowCRInSeparateColumns.Text = Language._Object.getCaption(Me.chkShowCRInSeparateColumns.Name, Me.chkShowCRInSeparateColumns.Text)
			Me.lblMembership.Text = Language._Object.getCaption(Me.lblMembership.Name, Me.lblMembership.Text)
			Me.chkShowEmpNameinSeperateColumn.Text = Language._Object.getCaption(Me.chkShowEmpNameinSeperateColumn.Name, Me.chkShowEmpNameinSeperateColumn.Text)
            Me.lblIdentity.Text = Language._Object.getCaption(Me.lblIdentity.Name, Me.lblIdentity.Text)

        Catch Ex As Exception
            DisplayError.Show("-1", Ex.Message, "SetLanguage", mstrModuleName)
        End Try
    End Sub


    Private Sub SetMessages()
        Try
            Language.setMessage(mstrModuleName, 1, "Payroll Report")
            Language.setMessage(mstrModuleName, 2, "Personal Salary Calculation Report")
            Language.setMessage(mstrModuleName, 3, " To Period cannot be less than From Period")
            Language.setMessage(mstrModuleName, 4, "Period is compulsory infomation. Please select period to continue.")
            Language.setMessage(mstrModuleName, 5, "Employee is compulsory infomation. Please select Employee to continue.")
            Language.setMessage(mstrModuleName, 6, "Exchange Rate :")
            Language.setMessage(mstrModuleName, 7, "Sorry, No exchange rate defined for this currency for the period selected.")
            Language.setMessage(mstrModuleName, 9, "Employee Payroll Summary Report")
            Language.setMessage(mstrModuleName, 10, "Custom Payroll Report")
            Language.setMessage(mstrModuleName, 12, "Sorry, Please select atleast one transaction head in order to generate report.")
            Language.setMessage(mstrModuleName, 13, "Settings saved successfully.")
            Language.setMessage(mstrModuleName, 14, "Custom Payroll Report Template 2")
            Language.setMessage(mstrModuleName, 15, "Loan")
            Language.setMessage(mstrModuleName, 16, "Advance")
            Language.setMessage(mstrModuleName, 17, "Savings")

        Catch Ex As Exception
            DisplayError.Show("-1", Ex.Message, "SetMessages", mstrModuleName)
        End Try
    End Sub
#End Region 'Language & UI Settings
    '</Language>
End Class

