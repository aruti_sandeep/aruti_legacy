﻿#Region " Imports "

Imports eZeeCommonLib
Imports Aruti.Data

#End Region

Public Class frmPayrollEmployeeListReport

#Region " Private Variables "

    Private ReadOnly mstrModuleName As String = "frmPayrollEmployeeListReport"
    Private objPayrollEmployeeListReport As clsPayrollEmployeeListReport
    Private mdtPayPeriodStartDate As DateTime
    Private mdtPayPeriodEndDate As DateTime
    Private mintFirstOpenPeriod As Integer = 0
    Private mstrCustomAllocIds As String = String.Empty
    Private eReport As enArutiReport
    Private mstrAdvanceFilter As String = String.Empty
#End Region

#Region " Private Enum "
    Private Enum enHeadTypeId
        CustomAllocIds = 1
    End Enum
#End Region

#Region " Constructor "
    Public Sub New(ByVal menReport As enArutiReport)
        eReport = menReport
        objPayrollEmployeeListReport = New clsPayrollEmployeeListReport(User._Object._Languageunkid, Company._Object._Companyunkid)
        objPayrollEmployeeListReport.SetDefaultValue()
        InitializeComponent()
    End Sub
#End Region

#Region " Form's Events "
    Private Sub frmPayrollEmployeeListReport_FormClosed(ByVal sender As Object, ByVal e As System.Windows.Forms.FormClosedEventArgs) Handles Me.FormClosed
        Try
            objPayrollEmployeeListReport = Nothing
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmPayrollEmployeeListReport_FormClosed", mstrModuleName)
        End Try
    End Sub

    Private Sub frmPayrollEmployeeListReport_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            Call Language.setLanguage(Me.Name)
            OtherSettings()
            Call FillCombo()
            Call ResetValue()
            Call Fill_Allocation()

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmPayrollEmployeeListReport_Load", mstrModuleName)
        End Try
    End Sub

    Private Sub frmPayrollEmployeeListReport_KeyDown(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles Me.KeyDown
        Try
            If e.Control Then
                If e.KeyCode = Windows.Forms.Keys.R Then
                End If
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmPayrollEmployeeListReport_KeyDown", mstrModuleName)
        End Try
    End Sub

    Private Sub frmPayrollEmployeeListReport_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles Me.KeyPress
        Try
            Select Case e.KeyChar
                Case CChar(ChrW(CInt(Windows.Forms.Keys.Enter)))
                    SendKeys.Send("{TAB}")
                    Exit Select
            End Select
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmPayrollEmployeeListReport_KeyPress", mstrModuleName)
        End Try

    End Sub

    Private Sub objbtnLanguage_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnLanguage.Click
        Dim objfrm As New frmLanguage
        Try
            If User._Object._Isrighttoleft = True Then
                objfrm.RightToLeft = Windows.Forms.RightToLeft.Yes
                objfrm.RightToLeftLayout = True
                Call Language.ctlRightToLeftlayOut(objfrm)
            End If

            Call SetMessages()

            clsPayrollEmployeeListReport.SetMessages()
            objfrm._Other_ModuleNames = "clsPayrollEmployeeListReport"
            objfrm.displayDialog(Me)

            Call Language.setLanguage(Me.Name)
            Call SetLanguage()

        Catch ex As System.Exception
            Call DisplayError.Show(-1, ex.Message, "objbtnLanguage_Click", mstrModuleName)
        Finally
            objfrm.Dispose()
            objfrm = Nothing
        End Try
    End Sub
#End Region

#Region " Private Function "

    Private Sub FillCombo()
        Dim dsCombo As New DataSet
        Dim objMaster As New clsMasterData
        Dim objperiod As New clscommom_period_Tran

        Try
            mintFirstOpenPeriod = objMaster.getFirstPeriodID(enModuleReference.Payroll, FinancialYear._Object._YearUnkid, 1)
            dsCombo = objperiod.getListForCombo(enModuleReference.Payroll, 0, FinancialYear._Object._DatabaseName, FinancialYear._Object._Database_Start_Date, "Period", True)
            With cboPeriod
                .ValueMember = "periodunkid"
                .DisplayMember = "name"
                .DataSource = dsCombo.Tables(0)
                .SelectedValue = mintFirstOpenPeriod
            End With

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "FillCombo", mstrModuleName)
        Finally
            dsCombo.Dispose() : objMaster = Nothing
            objperiod = Nothing
        End Try
    End Sub

    Private Sub Fill_Allocation()
        Dim objPayroll As New clsPayrollReport(User._Object._Languageunkid, Company._Object._Companyunkid)
        Try
            RemoveHandler objchkAllocation.CheckedChanged, AddressOf objchkAllocation_CheckedChanged
            RemoveHandler lvAllocation_Hierarchy.ItemChecked, AddressOf lvAllocation_Hierarchy_ItemChecked

            Dim dsList As New DataSet
            Dim dtTable As New DataTable

            Dim marrAlloc As New ArrayList

            If mstrCustomAllocIds.Trim <> "" Then
                marrAlloc.AddRange(mstrCustomAllocIds.Split(","))
            End If

            If marrAlloc.Count > 0 Then
                dsList = objPayrollEmployeeListReport.GetAllocationList("Id NOT IN(" & String.Join(",", marrAlloc.ToArray(Type.GetType("System.String"))) & ")")
                dtTable.Merge(dsList.Tables(0), True)

                dsList = objPayrollEmployeeListReport.GetAllocationList("Id IN (" & String.Join(",", marrAlloc.ToArray(Type.GetType("System.String"))) & ")")
                Dim intPos As Integer = 0
                For Each dRow As DataRow In dsList.Tables(0).Rows
                    Dim dr As DataRow = dtTable.NewRow
                    dr.ItemArray = dRow.ItemArray
                    dtTable.Rows.InsertAt(dr, intPos)
                    intPos += 1
                Next
            Else
                dsList = objPayrollEmployeeListReport.GetAllocationList()
                dtTable.Merge(dsList.Tables(0), True)
            End If

            lvAllocation_Hierarchy.Items.Clear()

            For Each dRow As DataRow In dtTable.Rows
                Dim lvItem As New ListViewItem

                lvItem.Text = ""
                lvItem.SubItems.Add(dRow.Item("Name").ToString)
                lvItem.Tag = dRow.Item("Id").ToString

                If marrAlloc.Contains(dRow.Item("Id").ToString) = True Then
                    lvItem.Checked = True
                Else
                    lvItem.Checked = False
                End If

                lvAllocation_Hierarchy.Items.Add(lvItem)
            Next

            If lvAllocation_Hierarchy.Items.Count > 5 Then
                colhAllocation.Width = 300 - 20
            Else
                colhAllocation.Width = 300
            End If

            If lvAllocation_Hierarchy.CheckedItems.Count = lvAllocation_Hierarchy.Items.Count Then
                objchkAllocation.CheckState = CheckState.Checked
            ElseIf lvAllocation_Hierarchy.CheckedItems.Count < lvAllocation_Hierarchy.Items.Count Then
                objchkAllocation.CheckState = CheckState.Indeterminate
            ElseIf lvAllocation_Hierarchy.CheckedItems.Count <= 0 Then
                objchkAllocation.CheckState = CheckState.Unchecked
            End If

            AddHandler objchkAllocation.CheckedChanged, AddressOf objchkAllocation_CheckedChanged
            AddHandler lvAllocation_Hierarchy.ItemChecked, AddressOf lvAllocation_Hierarchy_ItemChecked

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "Fill_Allocation", mstrModuleName)
        Finally
            objPayroll = Nothing
        End Try
    End Sub

    Private Sub ResetValue()
        Try
            cboPeriod.SelectedValue = mintFirstOpenPeriod
            mstrAdvanceFilter = ""
            objPayrollEmployeeListReport.setDefaultOrderBy(0)
            Call GetValue()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "ResetValue", mstrModuleName)
        End Try
    End Sub

    Public Function SetFilter() As Boolean
        Try
            If CInt(cboPeriod.SelectedIndex) = 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 1, "Please Select Period."), enMsgBoxStyle.Information)
                cboPeriod.Focus()
                Exit Function
            End If

            objPayrollEmployeeListReport.SetDefaultValue()

            objPayrollEmployeeListReport._PeriodId = cboPeriod.SelectedValue
            objPayrollEmployeeListReport._PeriodName = cboPeriod.Text

            Dim xAllocVal As New Dictionary(Of Integer, String)
            xAllocVal = lvAllocation_Hierarchy.CheckedItems.Cast(Of ListViewItem).ToDictionary(Function(x) CInt(x.Tag), Function(x) x.SubItems(colhAllocation.Index).Text)
            objPayrollEmployeeListReport._AllocationDetails = xAllocVal

            Return True
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "SetFilter", mstrModuleName)
        End Try
    End Function

    Private Sub GetValue()
        Dim objUserDefRMode As New clsUserDef_ReportMode
        Dim dsList As DataSet
        Try
            dsList = objUserDefRMode.GetList("List", enArutiReport.Payroll_Employee_List_Report)

            If dsList.Tables("List").Rows.Count > 0 Then

                For Each dsRow As DataRow In dsList.Tables("List").Rows
                    Select Case CInt(dsRow.Item("headtypeid"))

                        Case enHeadTypeId.CustomAllocIds
                            mstrCustomAllocIds = CStr(dsRow.Item("transactionheadid"))

                    End Select
                Next

            End If

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "GetValue", mstrModuleName)
        Finally
            objUserDefRMode = Nothing
        End Try
    End Sub

#End Region

#Region " Buttons Events "

    Private Sub btnExport_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnExport.Click
        Try

            If SetFilter() = False Then Exit Sub

            Dim objPeriod As New clscommom_period_Tran
            objPeriod._Periodunkid(FinancialYear._Object._DatabaseName) = CInt(cboPeriod.SelectedValue)


            objPayrollEmployeeListReport.Generate_DetailReport(FinancialYear._Object._DatabaseName, _
                                                                User._Object._Userunkid, _
                                                                FinancialYear._Object._YearUnkid, _
                                                                Company._Object._Companyunkid, _
                                                                objPeriod._Start_Date, _
                                                                objPeriod._End_Date, _
                                                                ConfigParameter._Object._UserAccessModeSetting, _
                                                                True, False, ConfigParameter._Object._ExportReportPath, _
                                                                ConfigParameter._Object._OpenAfterExport, _
                                                                0, enExportAction.None, , True, True, False, _
                                                                mstrAdvanceFilter)



        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "btnExport_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub btnReset_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnReset.Click
        Try
            Call ResetValue()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "btnReset_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub btnClose_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnClose.Click
        Try
            Me.Close()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "btnClose_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub objbtnTop_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnTop.Click
        Try
            If lvAllocation_Hierarchy.SelectedItems.Count > 0 Then
                Dim SelIndex As Integer = lvAllocation_Hierarchy.SelectedIndices.Item(0)
                If SelIndex = 0 Then Exit Sub
                Dim tItem As ListViewItem
                tItem = lvAllocation_Hierarchy.Items(SelIndex - 1)
                lvAllocation_Hierarchy.Items(SelIndex - 1) = lvAllocation_Hierarchy.Items(SelIndex).Clone
                lvAllocation_Hierarchy.Items(SelIndex) = tItem.Clone
                lvAllocation_Hierarchy.Items(SelIndex - 1).Selected = True
                lvAllocation_Hierarchy.Items(SelIndex - 1).EnsureVisible()
            End If
            lvAllocation_Hierarchy.Focus()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objbtnTop_Click", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub objbtnBottom_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnBottom.Click
        Try
            If lvAllocation_Hierarchy.SelectedItems.Count > 0 Then
                Dim SelIndex As Integer = lvAllocation_Hierarchy.SelectedIndices.Item(0)
                If SelIndex = lvAllocation_Hierarchy.Items.Count - 1 Then Exit Sub
                Dim tItem As ListViewItem
                tItem = lvAllocation_Hierarchy.Items(SelIndex + 1)
                lvAllocation_Hierarchy.Items(SelIndex + 1) = lvAllocation_Hierarchy.Items(SelIndex).Clone
                lvAllocation_Hierarchy.Items(SelIndex) = tItem.Clone
                lvAllocation_Hierarchy.Items(SelIndex + 1).Selected = True
                lvAllocation_Hierarchy.Items(SelIndex + 1).EnsureVisible()
            End If
            lvAllocation_Hierarchy.Focus()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objbtnBottom_Click", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub objbtnAdvanceFilter_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnAdvanceFilter.Click
        Dim frm As New frmAdvanceSearch
        Try
            frm._Hr_EmployeeTable_Alias = "hremployee_master"
            frm.ShowDialog()
            mstrAdvanceFilter = frm._GetFilterString
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "Form_AdvanceFilter_Click", mstrModuleName)
        Finally
            If frm IsNot Nothing Then frm.Dispose()
        End Try
    End Sub

#End Region

#Region "Combobox Events"
    Private Sub cboPeriod_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cboPeriod.SelectedIndexChanged
        Dim objPeriod As New clscommom_period_Tran
        Try
            objPeriod._Periodunkid(FinancialYear._Object._DatabaseName) = CInt(cboPeriod.SelectedValue)

            mdtPayPeriodStartDate = objPeriod._Start_Date
            mdtPayPeriodEndDate = objPeriod._End_Date

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "cboPeriod_SelectedIndexChanged", mstrModuleName)
        Finally
            objPeriod = Nothing
        End Try
    End Sub
#End Region

#Region " CheckBox's Events "

    Private Sub objchkAllocation_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objchkAllocation.CheckedChanged
        Try
            RemoveHandler lvAllocation_Hierarchy.ItemChecked, AddressOf lvAllocation_Hierarchy_ItemChecked
            For Each lvItem As ListViewItem In lvAllocation_Hierarchy.Items
                lvItem.Checked = objchkAllocation.CheckState
            Next
            AddHandler lvAllocation_Hierarchy.ItemChecked, AddressOf lvAllocation_Hierarchy_ItemChecked
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objchkAllocation_CheckedChanged", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub lvAllocation_Hierarchy_ItemChecked(ByVal sender As Object, ByVal e As System.Windows.Forms.ItemCheckedEventArgs) Handles lvAllocation_Hierarchy.ItemChecked
        Try
            RemoveHandler objchkAllocation.CheckedChanged, AddressOf objchkAllocation_CheckedChanged
            If lvAllocation_Hierarchy.CheckedItems.Count <= 0 Then
                objchkAllocation.CheckState = CheckState.Unchecked
            ElseIf lvAllocation_Hierarchy.CheckedItems.Count < lvAllocation_Hierarchy.Items.Count Then
                objchkAllocation.CheckState = CheckState.Indeterminate
            ElseIf lvAllocation_Hierarchy.CheckedItems.Count = lvAllocation_Hierarchy.Items.Count Then
                objchkAllocation.CheckState = CheckState.Checked
            End If
            AddHandler objchkAllocation.CheckedChanged, AddressOf objchkAllocation_CheckedChanged
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "lvAllocation_Hierarchy_ItemChecked", mstrModuleName)
        Finally
        End Try
    End Sub
#End Region

    Private Sub lnkSave_LinkClicked(ByVal sender As System.Object, ByVal e As System.Windows.Forms.LinkLabelLinkClickedEventArgs) Handles lnkSave.LinkClicked
        Dim objUserDefRMode As New clsUserDef_ReportMode
        Try
            If SetFilter() = False Then Exit Try

            For intHeadType As Integer = 1 To 1
                objUserDefRMode = New clsUserDef_ReportMode()

                Dim intUnkid As Integer = -1

                objUserDefRMode._Reportunkid = enArutiReport.Payroll_Employee_List_Report
                objUserDefRMode._Reporttypeid = 0
                objUserDefRMode._Reportmodeid = 0

                Select Case intHeadType

                    Case enHeadTypeId.CustomAllocIds
                        objUserDefRMode._Headtypeid = intHeadType
                        Dim xCheckedItems As String = String.Empty
                        If lvAllocation_Hierarchy.CheckedItems.Count > 0 Then
                            xCheckedItems = String.Join(",", lvAllocation_Hierarchy.CheckedItems.Cast(Of ListViewItem)().[Select](Function(x) x.Tag.ToString()).ToArray())
                            If xCheckedItems.Trim.Length > 0 Then
                                objUserDefRMode._EarningTranHeadIds = xCheckedItems
                            End If
                        End If
                        intUnkid = objUserDefRMode.isExist(eReport, 0, 0, intHeadType)

                End Select

                objUserDefRMode._Reportmodeunkid = intUnkid

                If intUnkid <= 0 Then
                    objUserDefRMode.Insert()
                Else
                    objUserDefRMode.Update()
                End If

            Next

            eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 2, "Selection Saved Successfully"), enMsgBoxStyle.Information)
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "lnkSave_LinkClicked", mstrModuleName)
        Finally
        End Try
    End Sub




	'<Language> This Auto Generated Text Please Do Not Modify it.
#Region " Language & UI Settings "
	Private Sub OtherSettings()
		Try
			Me.SuspendLayout()
			
			Call SetLanguage()
			
			Me.gbFilterCriteria.GradientColor = GUI._eZeeContainerHeaderBackColor
			Me.gbFilterCriteria.ForeColor = GUI._eZeeContainerHeaderForeColor

			Me.gbCustomSetting.GradientColor = GUI._eZeeContainerHeaderBackColor
			Me.gbCustomSetting.ForeColor = GUI._eZeeContainerHeaderForeColor

            Me.eZeeHeader.GradientColor1 = GUI._HeaderBackColor1
			Me.eZeeHeader.GradientColor2 = GUI._HeaderBackColor2
			Me.eZeeHeader.BorderColor = GUI._HeaderBorderColor
			Me.eZeeHeader.HeaderTextForeColor = GUI._HeaderTitleFontColor
			Me.eZeeHeader.DescriptionForeColor = GUI._HeaderMessageFontColor

			Me.objbtnLanguage.GradientBackColor = GUI._ButttonBackColor
			Me.objbtnLanguage.GradientForeColor = GUI._ButttonFontColor

			Me.btnClose.GradientBackColor = GUI._ButttonBackColor
			Me.btnClose.GradientForeColor = GUI._ButttonFontColor

			Me.btnExport.GradientBackColor = GUI._ButttonBackColor
			Me.btnExport.GradientForeColor = GUI._ButttonFontColor

			Me.btnReset.GradientBackColor = GUI._ButttonBackColor
			Me.btnReset.GradientForeColor = GUI._ButttonFontColor

			Me.objbtnTop.GradientBackColor = GUI._ButttonBackColor
			Me.objbtnTop.GradientForeColor = GUI._ButttonFontColor

			Me.objbtnBottom.GradientBackColor = GUI._ButttonBackColor
			Me.objbtnBottom.GradientForeColor = GUI._ButttonFontColor

			Me.objbtnAdvanceFilter.GradientBackColor = GUI._ButttonBackColor
			Me.objbtnAdvanceFilter.GradientForeColor = GUI._ButttonFontColor

			Me.ResumeLayout()
		Catch Ex As Exception
            DisplayError.Show("-1", Ex.Message, "OtherSettings", mstrModuleName)
		End Try
	End Sub
			
			
	Private Sub SetLanguage()
		Try
			Me.Text = Language._Object.getCaption(Me.Name, Me.Text)
			
			Me.gbFilterCriteria.Text = Language._Object.getCaption(Me.gbFilterCriteria.Name, Me.gbFilterCriteria.Text)
			Me.lblPeriod.Text = Language._Object.getCaption(Me.lblPeriod.Name, Me.lblPeriod.Text)
			Me.btnClose.Text = Language._Object.getCaption(Me.btnClose.Name, Me.btnClose.Text)
			Me.btnExport.Text = Language._Object.getCaption(Me.btnExport.Name, Me.btnExport.Text)
			Me.eZeeHeader.Title = Language._Object.getCaption(Me.eZeeHeader.Name & "_Title" , Me.eZeeHeader.Title)
			Me.eZeeHeader.Message = Language._Object.getCaption(Me.eZeeHeader.Name & "_Message" , Me.eZeeHeader.Message)
			Me.btnReset.Text = Language._Object.getCaption(Me.btnReset.Name, Me.btnReset.Text)
			Me.gbCustomSetting.Text = Language._Object.getCaption(Me.gbCustomSetting.Name, Me.gbCustomSetting.Text)
			Me.colhAllocation.Text = Language._Object.getCaption(CStr(Me.colhAllocation.Tag), Me.colhAllocation.Text)
			Me.lnkSave.Text = Language._Object.getCaption(Me.lnkSave.Name, Me.lnkSave.Text)
			
		Catch Ex As Exception
            DisplayError.Show("-1", Ex.Message, "SetLanguage", mstrModuleName)
		End Try
	End Sub
			
			
	Public Sub SetMessages()
		Try
            Language.setMessage(mstrModuleName, 1, "Please Select Period")
			Language.setMessage(mstrModuleName, 2, "Selection Saved Successfully")
			
		Catch Ex As Exception
            DisplayError.Show("-1", Ex.Message, "SetMessages", mstrModuleName)
		End Try
	End Sub
#End Region 'Language & UI Settings
	'</Language>
End Class