﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmPayrollEmployeeListReport
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmPayrollEmployeeListReport))
        Me.gbFilterCriteria = New eZee.Common.eZeeCollapsibleContainer(Me.components)
        Me.cboPeriod = New System.Windows.Forms.ComboBox
        Me.lblPeriod = New System.Windows.Forms.Label
        Me.eZeeHeader = New eZee.Common.eZeeHeader
        Me.EZeeFooter1 = New eZee.Common.eZeeFooter
        Me.btnReset = New eZee.Common.eZeeLightButton(Me.components)
        Me.btnExport = New eZee.Common.eZeeLightButton(Me.components)
        Me.btnClose = New eZee.Common.eZeeLightButton(Me.components)
        Me.objbtnLanguage = New eZee.Common.eZeeLightButton(Me.components)
        Me.gbCustomSetting = New eZee.Common.eZeeCollapsibleContainer(Me.components)
        Me.objbtnTop = New eZee.Common.eZeeLightButton(Me.components)
        Me.objbtnBottom = New eZee.Common.eZeeLightButton(Me.components)
        Me.objchkAllocation = New System.Windows.Forms.CheckBox
        Me.lvAllocation_Hierarchy = New eZee.Common.eZeeListView(Me.components)
        Me.objcolhCheckAll = New System.Windows.Forms.ColumnHeader
        Me.colhAllocation = New System.Windows.Forms.ColumnHeader
        Me.lnkSave = New System.Windows.Forms.LinkLabel
        Me.objbtnAdvanceFilter = New eZee.Common.eZeeLightButton(Me.components)
        Me.gbFilterCriteria.SuspendLayout()
        Me.EZeeFooter1.SuspendLayout()
        Me.gbCustomSetting.SuspendLayout()
        Me.SuspendLayout()
        '
        'gbFilterCriteria
        '
        Me.gbFilterCriteria.BorderColor = System.Drawing.Color.Black
        Me.gbFilterCriteria.Checked = False
        Me.gbFilterCriteria.CollapseAllExceptThis = False
        Me.gbFilterCriteria.CollapsedHoverImage = Nothing
        Me.gbFilterCriteria.CollapsedNormalImage = Nothing
        Me.gbFilterCriteria.CollapsedPressedImage = Nothing
        Me.gbFilterCriteria.CollapseOnLoad = False
        Me.gbFilterCriteria.Controls.Add(Me.cboPeriod)
        Me.gbFilterCriteria.Controls.Add(Me.lblPeriod)
        Me.gbFilterCriteria.ExpandedHoverImage = Nothing
        Me.gbFilterCriteria.ExpandedNormalImage = Nothing
        Me.gbFilterCriteria.ExpandedPressedImage = Nothing
        Me.gbFilterCriteria.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.gbFilterCriteria.GradientColor = System.Drawing.SystemColors.ButtonFace
        Me.gbFilterCriteria.HeaderHeight = 25
        Me.gbFilterCriteria.HeaderMessage = ""
        Me.gbFilterCriteria.HeaderMessageFont = New System.Drawing.Font("Microsoft Sans Serif", 8.25!)
        Me.gbFilterCriteria.HeaderMessageForeColor = System.Drawing.Color.Black
        Me.gbFilterCriteria.HeightOnCollapse = 0
        Me.gbFilterCriteria.LeftTextSpace = 0
        Me.gbFilterCriteria.Location = New System.Drawing.Point(15, 75)
        Me.gbFilterCriteria.Name = "gbFilterCriteria"
        Me.gbFilterCriteria.OpenHeight = 300
        Me.gbFilterCriteria.PanelBorderStyle = System.Windows.Forms.ButtonBorderStyle.Solid
        Me.gbFilterCriteria.ShowBorder = True
        Me.gbFilterCriteria.ShowCheckBox = False
        Me.gbFilterCriteria.ShowCollapseButton = False
        Me.gbFilterCriteria.ShowDefaultBorderColor = True
        Me.gbFilterCriteria.ShowDownButton = False
        Me.gbFilterCriteria.ShowHeader = True
        Me.gbFilterCriteria.Size = New System.Drawing.Size(421, 73)
        Me.gbFilterCriteria.TabIndex = 64
        Me.gbFilterCriteria.Temp = 0
        Me.gbFilterCriteria.Text = "Filter Criteria"
        Me.gbFilterCriteria.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'cboPeriod
        '
        Me.cboPeriod.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboPeriod.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboPeriod.FormattingEnabled = True
        Me.cboPeriod.Location = New System.Drawing.Point(127, 31)
        Me.cboPeriod.Name = "cboPeriod"
        Me.cboPeriod.Size = New System.Drawing.Size(254, 21)
        Me.cboPeriod.TabIndex = 4
        '
        'lblPeriod
        '
        Me.lblPeriod.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblPeriod.Location = New System.Drawing.Point(14, 34)
        Me.lblPeriod.Name = "lblPeriod"
        Me.lblPeriod.Size = New System.Drawing.Size(108, 15)
        Me.lblPeriod.TabIndex = 3
        Me.lblPeriod.Text = "Period"
        Me.lblPeriod.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'eZeeHeader
        '
        Me.eZeeHeader.BackColor = System.Drawing.SystemColors.Control
        Me.eZeeHeader.BorderColor = System.Drawing.SystemColors.ControlDark
        Me.eZeeHeader.DescriptionForeColor = System.Drawing.SystemColors.ControlDarkDark
        Me.eZeeHeader.Dock = System.Windows.Forms.DockStyle.Top
        Me.eZeeHeader.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.eZeeHeader.GradiantStyle = eZee.Common.eZeeHeader.GradientStyle.Central
        Me.eZeeHeader.GradientColor1 = System.Drawing.SystemColors.Window
        Me.eZeeHeader.GradientColor2 = System.Drawing.SystemColors.Control
        Me.eZeeHeader.HeaderTextForeColor = System.Drawing.SystemColors.ControlText
        Me.eZeeHeader.Icon = Nothing
        Me.eZeeHeader.Location = New System.Drawing.Point(0, 0)
        Me.eZeeHeader.Message = ""
        Me.eZeeHeader.Name = "eZeeHeader"
        Me.eZeeHeader.Size = New System.Drawing.Size(634, 60)
        Me.eZeeHeader.TabIndex = 63
        Me.eZeeHeader.Title = "Payroll Employee List Report"
        '
        'EZeeFooter1
        '
        Me.EZeeFooter1.BorderColor = System.Drawing.Color.Silver
        Me.EZeeFooter1.Controls.Add(Me.objbtnAdvanceFilter)
        Me.EZeeFooter1.Controls.Add(Me.btnReset)
        Me.EZeeFooter1.Controls.Add(Me.btnExport)
        Me.EZeeFooter1.Controls.Add(Me.btnClose)
        Me.EZeeFooter1.Controls.Add(Me.objbtnLanguage)
        Me.EZeeFooter1.Dock = System.Windows.Forms.DockStyle.Bottom
        Me.EZeeFooter1.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.EZeeFooter1.GradiantStyle = eZee.Common.eZeeFooter.GradientStyle.Up
        Me.EZeeFooter1.GradientColor1 = System.Drawing.SystemColors.Control
        Me.EZeeFooter1.GradientColor2 = System.Drawing.SystemColors.Control
        Me.EZeeFooter1.Location = New System.Drawing.Point(0, 397)
        Me.EZeeFooter1.Name = "EZeeFooter1"
        Me.EZeeFooter1.Size = New System.Drawing.Size(634, 55)
        Me.EZeeFooter1.TabIndex = 62
        '
        'btnReset
        '
        Me.btnReset.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnReset.BackColor = System.Drawing.Color.White
        Me.btnReset.BackgroundImage = CType(resources.GetObject("btnReset.BackgroundImage"), System.Drawing.Image)
        Me.btnReset.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.btnReset.BorderColor = System.Drawing.Color.Empty
        Me.btnReset.ButtonType = eZee.Common.eZeeLightButton.enButtonType.NormalButton
        Me.btnReset.FlatAppearance.BorderSize = 0
        Me.btnReset.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnReset.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnReset.ForeColor = System.Drawing.Color.Black
        Me.btnReset.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.btnReset.GradientForeColor = System.Drawing.Color.Black
        Me.btnReset.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.btnReset.HoverGradientForeColor = System.Drawing.Color.Black
        Me.btnReset.Location = New System.Drawing.Point(341, 13)
        Me.btnReset.Name = "btnReset"
        Me.btnReset.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.btnReset.PressedGradientForeColor = System.Drawing.Color.Black
        Me.btnReset.Size = New System.Drawing.Size(90, 30)
        Me.btnReset.TabIndex = 34
        Me.btnReset.Text = "&Reset"
        Me.btnReset.UseVisualStyleBackColor = True
        '
        'btnExport
        '
        Me.btnExport.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnExport.BackColor = System.Drawing.Color.White
        Me.btnExport.BackgroundImage = CType(resources.GetObject("btnExport.BackgroundImage"), System.Drawing.Image)
        Me.btnExport.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.btnExport.BorderColor = System.Drawing.Color.Empty
        Me.btnExport.ButtonType = eZee.Common.eZeeLightButton.enButtonType.NormalButton
        Me.btnExport.FlatAppearance.BorderSize = 0
        Me.btnExport.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnExport.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnExport.ForeColor = System.Drawing.Color.Black
        Me.btnExport.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.btnExport.GradientForeColor = System.Drawing.Color.Black
        Me.btnExport.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.btnExport.HoverGradientForeColor = System.Drawing.Color.Black
        Me.btnExport.Location = New System.Drawing.Point(437, 13)
        Me.btnExport.Name = "btnExport"
        Me.btnExport.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.btnExport.PressedGradientForeColor = System.Drawing.Color.Black
        Me.btnExport.Size = New System.Drawing.Size(90, 30)
        Me.btnExport.TabIndex = 33
        Me.btnExport.Text = "&Export"
        Me.btnExport.UseVisualStyleBackColor = True
        '
        'btnClose
        '
        Me.btnClose.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnClose.BackColor = System.Drawing.Color.White
        Me.btnClose.BackgroundImage = CType(resources.GetObject("btnClose.BackgroundImage"), System.Drawing.Image)
        Me.btnClose.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.btnClose.BorderColor = System.Drawing.Color.Empty
        Me.btnClose.ButtonType = eZee.Common.eZeeLightButton.enButtonType.NormalButton
        Me.btnClose.FlatAppearance.BorderSize = 0
        Me.btnClose.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnClose.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnClose.ForeColor = System.Drawing.Color.Black
        Me.btnClose.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.btnClose.GradientForeColor = System.Drawing.Color.Black
        Me.btnClose.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.btnClose.HoverGradientForeColor = System.Drawing.Color.Black
        Me.btnClose.Location = New System.Drawing.Point(533, 13)
        Me.btnClose.Name = "btnClose"
        Me.btnClose.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.btnClose.PressedGradientForeColor = System.Drawing.Color.Black
        Me.btnClose.Size = New System.Drawing.Size(90, 30)
        Me.btnClose.TabIndex = 32
        Me.btnClose.Text = "&Close"
        Me.btnClose.UseVisualStyleBackColor = True
        '
        'objbtnLanguage
        '
        Me.objbtnLanguage.BackColor = System.Drawing.Color.White
        Me.objbtnLanguage.BackgroundImage = CType(resources.GetObject("objbtnLanguage.BackgroundImage"), System.Drawing.Image)
        Me.objbtnLanguage.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.objbtnLanguage.BorderColor = System.Drawing.Color.Empty
        Me.objbtnLanguage.ButtonType = eZee.Common.eZeeLightButton.enButtonType.NormalButton
        Me.objbtnLanguage.FlatAppearance.BorderSize = 0
        Me.objbtnLanguage.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.objbtnLanguage.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.objbtnLanguage.ForeColor = System.Drawing.Color.Black
        Me.objbtnLanguage.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.objbtnLanguage.GradientForeColor = System.Drawing.Color.Black
        Me.objbtnLanguage.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.objbtnLanguage.HoverGradientForeColor = System.Drawing.Color.Black
        Me.objbtnLanguage.Image = Global.ArutiReports.My.Resources.Resources.FONT_TXT
        Me.objbtnLanguage.Location = New System.Drawing.Point(9, 13)
        Me.objbtnLanguage.Name = "objbtnLanguage"
        Me.objbtnLanguage.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.objbtnLanguage.PressedGradientForeColor = System.Drawing.Color.Black
        Me.objbtnLanguage.Size = New System.Drawing.Size(30, 30)
        Me.objbtnLanguage.TabIndex = 32
        Me.objbtnLanguage.UseVisualStyleBackColor = False
        '
        'gbCustomSetting
        '
        Me.gbCustomSetting.BorderColor = System.Drawing.Color.Black
        Me.gbCustomSetting.Checked = False
        Me.gbCustomSetting.CollapseAllExceptThis = False
        Me.gbCustomSetting.CollapsedHoverImage = Nothing
        Me.gbCustomSetting.CollapsedNormalImage = Nothing
        Me.gbCustomSetting.CollapsedPressedImage = Nothing
        Me.gbCustomSetting.CollapseOnLoad = False
        Me.gbCustomSetting.Controls.Add(Me.objbtnTop)
        Me.gbCustomSetting.Controls.Add(Me.objbtnBottom)
        Me.gbCustomSetting.Controls.Add(Me.objchkAllocation)
        Me.gbCustomSetting.Controls.Add(Me.lvAllocation_Hierarchy)
        Me.gbCustomSetting.Controls.Add(Me.lnkSave)
        Me.gbCustomSetting.ExpandedHoverImage = Nothing
        Me.gbCustomSetting.ExpandedNormalImage = Nothing
        Me.gbCustomSetting.ExpandedPressedImage = Nothing
        Me.gbCustomSetting.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.gbCustomSetting.GradientColor = System.Drawing.SystemColors.ButtonFace
        Me.gbCustomSetting.HeaderHeight = 25
        Me.gbCustomSetting.HeaderMessage = ""
        Me.gbCustomSetting.HeaderMessageFont = New System.Drawing.Font("Microsoft Sans Serif", 8.25!)
        Me.gbCustomSetting.HeaderMessageForeColor = System.Drawing.Color.Black
        Me.gbCustomSetting.HeightOnCollapse = 0
        Me.gbCustomSetting.LeftTextSpace = 0
        Me.gbCustomSetting.Location = New System.Drawing.Point(15, 154)
        Me.gbCustomSetting.Name = "gbCustomSetting"
        Me.gbCustomSetting.OpenHeight = 300
        Me.gbCustomSetting.PanelBorderStyle = System.Windows.Forms.ButtonBorderStyle.Solid
        Me.gbCustomSetting.ShowBorder = True
        Me.gbCustomSetting.ShowCheckBox = False
        Me.gbCustomSetting.ShowCollapseButton = False
        Me.gbCustomSetting.ShowDefaultBorderColor = True
        Me.gbCustomSetting.ShowDownButton = False
        Me.gbCustomSetting.ShowHeader = True
        Me.gbCustomSetting.Size = New System.Drawing.Size(421, 232)
        Me.gbCustomSetting.TabIndex = 65
        Me.gbCustomSetting.Temp = 0
        Me.gbCustomSetting.Text = "Custom Settings"
        Me.gbCustomSetting.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'objbtnTop
        '
        Me.objbtnTop.BackColor = System.Drawing.Color.White
        Me.objbtnTop.BackgroundImage = CType(resources.GetObject("objbtnTop.BackgroundImage"), System.Drawing.Image)
        Me.objbtnTop.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.objbtnTop.BorderColor = System.Drawing.Color.Empty
        Me.objbtnTop.ButtonType = eZee.Common.eZeeLightButton.enButtonType.NormalButton
        Me.objbtnTop.FlatAppearance.BorderSize = 0
        Me.objbtnTop.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.objbtnTop.ForeColor = System.Drawing.Color.Black
        Me.objbtnTop.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.objbtnTop.GradientForeColor = System.Drawing.Color.Black
        Me.objbtnTop.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.objbtnTop.HoverGradientForeColor = System.Drawing.Color.Black
        Me.objbtnTop.Image = Global.ArutiReports.My.Resources.Resources.MoveUp_16
        Me.objbtnTop.Location = New System.Drawing.Point(383, 26)
        Me.objbtnTop.Name = "objbtnTop"
        Me.objbtnTop.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.objbtnTop.PressedGradientForeColor = System.Drawing.Color.Black
        Me.objbtnTop.Size = New System.Drawing.Size(32, 30)
        Me.objbtnTop.TabIndex = 228
        Me.objbtnTop.UseVisualStyleBackColor = False
        '
        'objbtnBottom
        '
        Me.objbtnBottom.BackColor = System.Drawing.Color.White
        Me.objbtnBottom.BackgroundImage = CType(resources.GetObject("objbtnBottom.BackgroundImage"), System.Drawing.Image)
        Me.objbtnBottom.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.objbtnBottom.BorderColor = System.Drawing.Color.Empty
        Me.objbtnBottom.ButtonType = eZee.Common.eZeeLightButton.enButtonType.NormalButton
        Me.objbtnBottom.FlatAppearance.BorderSize = 0
        Me.objbtnBottom.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.objbtnBottom.ForeColor = System.Drawing.Color.Black
        Me.objbtnBottom.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.objbtnBottom.GradientForeColor = System.Drawing.Color.Black
        Me.objbtnBottom.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.objbtnBottom.HoverGradientForeColor = System.Drawing.Color.Black
        Me.objbtnBottom.Image = Global.ArutiReports.My.Resources.Resources.MoveDown_16
        Me.objbtnBottom.Location = New System.Drawing.Point(383, 57)
        Me.objbtnBottom.Name = "objbtnBottom"
        Me.objbtnBottom.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.objbtnBottom.PressedGradientForeColor = System.Drawing.Color.Black
        Me.objbtnBottom.Size = New System.Drawing.Size(32, 30)
        Me.objbtnBottom.TabIndex = 229
        Me.objbtnBottom.UseVisualStyleBackColor = False
        '
        'objchkAllocation
        '
        Me.objchkAllocation.AutoSize = True
        Me.objchkAllocation.Location = New System.Drawing.Point(8, 32)
        Me.objchkAllocation.Name = "objchkAllocation"
        Me.objchkAllocation.Size = New System.Drawing.Size(15, 14)
        Me.objchkAllocation.TabIndex = 22
        Me.objchkAllocation.UseVisualStyleBackColor = True
        '
        'lvAllocation_Hierarchy
        '
        Me.lvAllocation_Hierarchy.BackColorOnChecked = False
        Me.lvAllocation_Hierarchy.CheckBoxes = True
        Me.lvAllocation_Hierarchy.ColumnHeaders = Nothing
        Me.lvAllocation_Hierarchy.Columns.AddRange(New System.Windows.Forms.ColumnHeader() {Me.objcolhCheckAll, Me.colhAllocation})
        Me.lvAllocation_Hierarchy.CompulsoryColumns = ""
        Me.lvAllocation_Hierarchy.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lvAllocation_Hierarchy.FullRowSelect = True
        Me.lvAllocation_Hierarchy.GridLines = True
        Me.lvAllocation_Hierarchy.GroupingColumn = Nothing
        Me.lvAllocation_Hierarchy.HeaderStyle = System.Windows.Forms.ColumnHeaderStyle.Nonclickable
        Me.lvAllocation_Hierarchy.HideSelection = False
        Me.lvAllocation_Hierarchy.Location = New System.Drawing.Point(2, 26)
        Me.lvAllocation_Hierarchy.MinColumnWidth = 50
        Me.lvAllocation_Hierarchy.MultiSelect = False
        Me.lvAllocation_Hierarchy.Name = "lvAllocation_Hierarchy"
        Me.lvAllocation_Hierarchy.OptionalColumns = ""
        Me.lvAllocation_Hierarchy.ShowMoreItem = False
        Me.lvAllocation_Hierarchy.ShowSaveItem = False
        Me.lvAllocation_Hierarchy.ShowSelectAll = True
        Me.lvAllocation_Hierarchy.ShowSizeAllColumnsToFit = True
        Me.lvAllocation_Hierarchy.Size = New System.Drawing.Size(379, 198)
        Me.lvAllocation_Hierarchy.Sortable = True
        Me.lvAllocation_Hierarchy.TabIndex = 13
        Me.lvAllocation_Hierarchy.UseCompatibleStateImageBehavior = False
        Me.lvAllocation_Hierarchy.View = System.Windows.Forms.View.Details
        '
        'objcolhCheckAll
        '
        Me.objcolhCheckAll.Tag = "objcolhCheckAll"
        Me.objcolhCheckAll.Text = ""
        Me.objcolhCheckAll.Width = 25
        '
        'colhAllocation
        '
        Me.colhAllocation.Tag = "colhAllocation"
        Me.colhAllocation.Text = "Allocations/Other Details"
        Me.colhAllocation.Width = 300
        '
        'lnkSave
        '
        Me.lnkSave.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.lnkSave.BackColor = System.Drawing.Color.Transparent
        Me.lnkSave.LinkBehavior = System.Windows.Forms.LinkBehavior.NeverUnderline
        Me.lnkSave.Location = New System.Drawing.Point(277, 2)
        Me.lnkSave.Name = "lnkSave"
        Me.lnkSave.Size = New System.Drawing.Size(141, 21)
        Me.lnkSave.TabIndex = 1
        Me.lnkSave.TabStop = True
        Me.lnkSave.Text = "Save Settings"
        Me.lnkSave.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'objbtnAdvanceFilter
        '
        Me.objbtnAdvanceFilter.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.objbtnAdvanceFilter.BackColor = System.Drawing.Color.White
        Me.objbtnAdvanceFilter.BackgroundImage = CType(resources.GetObject("objbtnAdvanceFilter.BackgroundImage"), System.Drawing.Image)
        Me.objbtnAdvanceFilter.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.objbtnAdvanceFilter.BorderColor = System.Drawing.Color.Empty
        Me.objbtnAdvanceFilter.ButtonType = eZee.Common.eZeeLightButton.enButtonType.NormalButton
        Me.objbtnAdvanceFilter.FlatAppearance.BorderSize = 0
        Me.objbtnAdvanceFilter.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.objbtnAdvanceFilter.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.objbtnAdvanceFilter.ForeColor = System.Drawing.Color.Black
        Me.objbtnAdvanceFilter.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.objbtnAdvanceFilter.GradientForeColor = System.Drawing.Color.Black
        Me.objbtnAdvanceFilter.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.objbtnAdvanceFilter.HoverGradientForeColor = System.Drawing.Color.Black
        Me.objbtnAdvanceFilter.Location = New System.Drawing.Point(238, 13)
        Me.objbtnAdvanceFilter.Name = "objbtnAdvanceFilter"
        Me.objbtnAdvanceFilter.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.objbtnAdvanceFilter.PressedGradientForeColor = System.Drawing.Color.Black
        Me.objbtnAdvanceFilter.Size = New System.Drawing.Size(97, 30)
        Me.objbtnAdvanceFilter.TabIndex = 37
        Me.objbtnAdvanceFilter.Text = "Adv. Filter"
        Me.objbtnAdvanceFilter.UseVisualStyleBackColor = False
        '
        'frmPayrollEmployeeListReport
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(634, 452)
        Me.Controls.Add(Me.gbCustomSetting)
        Me.Controls.Add(Me.gbFilterCriteria)
        Me.Controls.Add(Me.eZeeHeader)
        Me.Controls.Add(Me.EZeeFooter1)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None
        Me.Name = "frmPayrollEmployeeListReport"
        Me.ShowInTaskbar = False
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent
        Me.Text = "Payroll Employee List Report"
        Me.gbFilterCriteria.ResumeLayout(False)
        Me.EZeeFooter1.ResumeLayout(False)
        Me.gbCustomSetting.ResumeLayout(False)
        Me.gbCustomSetting.PerformLayout()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents gbFilterCriteria As eZee.Common.eZeeCollapsibleContainer
    Friend WithEvents cboPeriod As System.Windows.Forms.ComboBox
    Friend WithEvents lblPeriod As System.Windows.Forms.Label
    Friend WithEvents objbtnLanguage As eZee.Common.eZeeLightButton
    Friend WithEvents btnClose As eZee.Common.eZeeLightButton
    Friend WithEvents btnExport As eZee.Common.eZeeLightButton
    Friend WithEvents eZeeHeader As eZee.Common.eZeeHeader
    Friend WithEvents EZeeFooter1 As eZee.Common.eZeeFooter
    Friend WithEvents btnReset As eZee.Common.eZeeLightButton
    Friend WithEvents gbCustomSetting As eZee.Common.eZeeCollapsibleContainer
    Friend WithEvents objbtnTop As eZee.Common.eZeeLightButton
    Friend WithEvents objbtnBottom As eZee.Common.eZeeLightButton
    Friend WithEvents objchkAllocation As System.Windows.Forms.CheckBox
    Friend WithEvents lvAllocation_Hierarchy As eZee.Common.eZeeListView
    Friend WithEvents objcolhCheckAll As System.Windows.Forms.ColumnHeader
    Friend WithEvents colhAllocation As System.Windows.Forms.ColumnHeader
    Friend WithEvents lnkSave As System.Windows.Forms.LinkLabel
    Friend WithEvents objbtnAdvanceFilter As eZee.Common.eZeeLightButton
End Class
